/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#ifndef __VTSS_IPMC_MVR_SERIALIZER_HXX__
#define __VTSS_IPMC_MVR_SERIALIZER_HXX__

#include "vtss_appl_serialize.hxx"
#include "vtss/appl/ipmc_mvr.h"
#include "vtss/appl/types.hxx"

/*****************************************************************************
    Data type serializer
*****************************************************************************/

/*****************************************************************************
    Enumerator serializer
*****************************************************************************/
extern vtss_enum_descriptor_t ipmc_mvr_intfMode_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ipmc_mvr_intf_mode_t,
                         "IpmcMvrVlanInterfaceMode",
                         ipmc_mvr_intfMode_txt,
                         "This enumeration indicates the MVR VLAN interface's operational mode.");

extern vtss_enum_descriptor_t ipmc_mvr_vlanTag_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ipmc_intf_vtag_t,
                         "IpmcMvrVlanInterfaceTagging",
                         ipmc_mvr_vlanTag_txt,
                         "This enumeration indicates the VLAN tagging for IPMC control frames in MVR.");

extern vtss_enum_descriptor_t ipmc_mvr_portRole_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ipmc_mvr_port_role_t,
                         "IpmcMvrVlanInterfacePortRole",
                         ipmc_mvr_portRole_txt,
                         "This enumeration indicates the MVR port's operational role.");

extern vtss_enum_descriptor_t ipmc_mvr_querierType_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ipmc_querier_states_t,
                         "IpmcMvrInterfaceQuerierStatus",
                         ipmc_mvr_querierType_txt,
                         "This enumeration indicates the Querier status for MVR VLAN interface.");

extern vtss_enum_descriptor_t ipmc_mvr_sfmType_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ipmc_sfm_mode_t,
                         "IpmcMvrGroupSrcListGroupFilterMode",
                         ipmc_mvr_sfmType_txt,
                         "This enumeration indicates the group filter mode for an IPMC group address.");

extern vtss_enum_descriptor_t ipmc_mvr_srclistType_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mvr_action_t,
                         "IpmcMvrGroupSrcListSourceType",
                         ipmc_mvr_srclistType_txt,
                         "This enumeration indicates the source filtering type from IPMC.");

mesa_rc vtss_appl_ipmc_mvr_control_statistics_dummy_get(
        vtss_ifindex_t *const act_value);

/*****************************************************************************
    Index serializer
*****************************************************************************/
VTSS_SNMP_TAG_SERIALIZE(ipmc_mvr_port_ifindex_idx, vtss_ifindex_t, a, s) {
    a.add_leaf(
        vtss::AsInterfaceIndex(s.inner),
        vtss::tag::Name("PortIndex"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(0),
        vtss::tag::Description("Logical interface number of the physical port.")
    );
}

VTSS_SNMP_TAG_SERIALIZE(ipmc_mvr_vlan_ifindex_idx, vtss_ifindex_t, a, s) {
    a.add_leaf(
        vtss::AsInterfaceIndex(s.inner),
        vtss::tag::Name("IfIndex"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(0),
        vtss::tag::Description("Logical interface number of the VLAN interface.")
    );
}

VTSS_SNMP_TAG_SERIALIZE(ipmc_mvr_ipv4_grp_addr_idx, mesa_ipv4_t, a, s) {
    a.add_leaf(
        vtss::AsIpv4(s.inner),
        vtss::tag::Name("GroupAddress"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(0),
        vtss::tag::Description("Assigned IPv4 multicast address.")
    );
}

VTSS_SNMP_TAG_SERIALIZE(ipmc_mvr_ipv4_src_addr_idx, mesa_ipv4_t, a, s) {
    a.add_leaf(
        vtss::AsIpv4(s.inner),
        vtss::tag::Name("HostAddress"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(0),
        vtss::tag::Description("Assigned IPv4 source address.")
    );
}

VTSS_SNMP_TAG_SERIALIZE(ipmc_mvr_ipv6_grp_addr_idx, mesa_ipv6_t, a, s ) {
    a.add_leaf(
        s.inner,
        vtss::tag::Name("GroupAddress"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(0),
        vtss::tag::Description("Assigned IPv6 multicast address.")
    );
}

VTSS_SNMP_TAG_SERIALIZE(ipmc_mvr_ipv6_src_addr_idx, mesa_ipv6_t, a, s ) {
    a.add_leaf(
        s.inner,
        vtss::tag::Name("HostAddress"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(0),
        vtss::tag::Description("Assigned IPv6 source address.")
    );
}

/*****************************************************************************
    Data serializer
*****************************************************************************/
template<typename T>
void serialize(T &a, vtss_appl_ipmc_mvr_global_t &s)
{
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_mvr_global_t"));
    int ix = 0;

    m.add_leaf(
        vtss::AsBool(s.mode),
        vtss::tag::Name("AdminState"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the IPMC MVR global functionality.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_mvr_port_conf_t &s)
{
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_mvr_port_conf_t"));
    int ix = 0;

    m.add_leaf(
        vtss::AsBool(s.do_immediate_leave),
        vtss::tag::Name("DoImmediateLeave"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the MVR immediate leave functionality.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_mvr_interface_t &s)
{
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_mvr_interface_t"));
    int ix = 0;

    m.add_leaf(
        vtss::AsDisplayString(s.name.n, sizeof(s.name.n)),
        vtss::tag::Name("Name"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("MVR Name is an optional attribute to indicate the name "
            "of the specific MVR VLAN that user could easily associate the MVR VLAN purpose "
            "with its name.")
    );

    m.add_leaf(
        vtss::AsIpv4(s.igmp_querier_address),
        vtss::tag::Name("IgmpQuerierAddress"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The static IPv4 source address of the specific MVR interface "
            "for seding IGMP Query message with respect to IGMP Querier election.")
    );

    m.add_leaf(
        s.mode,
        vtss::tag::Name("Mode"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("In Dynamic mode, MVR allows dynamic MVR membership reports on "
            "source ports. In Compatible mode, MVR membership reports are forbidden on source "
            "ports.")
    );

    m.add_leaf(
        s.tagging,
        vtss::tag::Name("Tagging"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Specify whether the traversed IGMP/MLD control frames will be "
            "sent as Untagged or Tagged with MVR VID.")
    );

    m.add_leaf(
        s.priority,
        vtss::tag::Name("Priority"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Specify how the traversed IGMP/MLD control frames will be sent "
            "in prioritized manner in VLAN tag.")
    );

    m.add_leaf(
        s.last_listener_query_interval,
        vtss::tag::Name("LastListenerQueryInt"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting Last Listener/Member Query Interval is used to "
            "control IGMP protocol stack for fast aging mechanism. It defines the maximum time "
            "to wait for IGMP/MLD report memberships on a port before removing the port from "
            "multicast group membership. The value is in units of tenths of a seconds. The range "
            "is from 0 to 31744.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.channel_profile.n, sizeof(s.channel_profile.n)),
        vtss::tag::Name("ChannelProfile"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The profile used for the channel filtering condition in the specific "
            "MVR VLAN. Profile selected for designated interface channel is not allowed to have "
            "overlapped permit group address by comparing with other MVR VLAN interface's channel.")
    );

    m.add_leaf(
        vtss::AsBool(s.querier_election),
        vtss::tag::Name("QuerierElection"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the capability to run IGMP Querier election per-VLAN basis.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_mvr_intf_port_t &s)
{
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_mvr_intf_port_t"));
    int ix = 0;

    m.add_leaf(
        s.role,
        vtss::tag::Name("Role"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Configure a MVR port of the designated MVR VLAN as one of the "
            "following roles: Inactive, Source or Receiver. An inactive port does not participate "
            "MVR operations. Configure uplink ports that receive and send multicast data as "
            "a source port, and multicast subscribers cannot be directly connected to source ports. "
            "Configure a port as a receiver port if it is a subscriber port and should only receive "
            "multicast data, and a receiver port does not receive data unless it becomes a member of "
            "the multicast group by issuing IGMP/MLD control messages. Be Caution: MVR source ports "
            "are not recommended to be overlapped with management VLAN ports.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_mvr_grp_adrs_cnt_t &s)
{
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_mvr_grp_adrs_cnt_t"));
    int ix = 0;

    m.add_leaf(
        s.mvr_igmp_grps,
        vtss::tag::Name("FromIgmp"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote the total number of registered "
            "multicast group address from IGMP learning in MVR.")
    );

    m.add_leaf(
        s.mvr_mld_grps,
        vtss::tag::Name("FromMld"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote the total number of registered "
            "multicast group address from MLD learning in MVR.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_mvr_intf_ipv4_status_t &s)
{
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_mvr_intf_ipv4_status_t"));
    int ix = 0;

    m.add_leaf(
        s.querier_state,
        vtss::tag::Name("QuerierStatus"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The IGMP Querier status of the specific VLAN interface.")
    );

    m.add_leaf(
        vtss::AsIpv4(s.active_querier_address),
        vtss::tag::Name("ActiveQuerierAddress"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The active IGMP Querier address on the specific VLAN interface.")
    );

    m.add_leaf(
        s.querier_up_time,
        vtss::tag::Name("QuerierUptime"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the in operation timer for the specific "
            "interface act as a IGMP Querier.")
    );

    m.add_leaf(
        s.query_interval,
        vtss::tag::Name("QueryInterval"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used to control IGMP protocol stack "
            "as stated in RFC-3376 8.2.")
    );

    m.add_leaf(
        s.startup_query_count,
        vtss::tag::Name("StartupQueryCount"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used to control IGMP protocol stack "
            "as stated in RFC-3376 8.7.")
    );

    m.add_leaf(
        s.querier_expiry_time,
        vtss::tag::Name("QuerierExpiryTime"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used to control IGMP protocol stack "
            "as stated in RFC-3376 8.5.")
    );

    m.add_leaf(
        s.counter_tx_query,
        vtss::tag::Name("CounterTxQuery"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "IGMP interface for transmitting IGMP Query control frames.")
    );

    m.add_leaf(
        s.counter_tx_specific_query,
        vtss::tag::Name("CounterTxSpecificQuery"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "IGMP interface for transmitting IGMP Specific Query control frames.")
    );

    m.add_leaf(
        s.counter_rx_query,
        vtss::tag::Name("CounterRxQuery"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "IGMP interface for receiving IGMP Query control frames.")
    );

    m.add_leaf(
        s.counter_rx_v1_join,
        vtss::tag::Name("CounterRxV1Join"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "IGMP interface for receiving IGMPv1 Join control frames.")
    );

    m.add_leaf(
        s.counter_rx_v2_join,
        vtss::tag::Name("CounterRxV2Join"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "IGMP interface for receiving IGMPv2 Join control frames.")
    );

    m.add_leaf(
        s.counter_rx_v2_leave,
        vtss::tag::Name("CounterRxV2Leave"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "IGMP interface for receiving IGMPv2 Leave control frames.")
    );

    m.add_leaf(
        s.counter_rx_v3_join,
        vtss::tag::Name("CounterRxV3Join"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "IGMP interface for receiving IGMPv3 Join control frames.")
    );

    m.add_leaf(
        s.counter_rx_errors,
        vtss::tag::Name("CounterRxErrors"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "IGMP interface for receiving invalid IGMP control frames.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_mvr_intf_ipv6_status_t &s)
{
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_mvr_intf_ipv6_status_t"));
    int ix = 0;

    m.add_leaf(
        s.querier_state,
        vtss::tag::Name("QuerierStatus"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The MLD Querier status of the specific VLAN interface.")
    );

    m.add_leaf(
        s.active_querier_address,
        vtss::tag::Name("ActiveQuerierAddress"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The active MLD Querier address on the specific VLAN interface.")
    );

    m.add_leaf(
        s.querier_up_time,
        vtss::tag::Name("QuerierUptime"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the in operation timer for the specific "
            "interface act as a MLD Querier.")
    );

    m.add_leaf(
        s.query_interval,
        vtss::tag::Name("QueryInterval"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used to control MLD protocol stack "
            "as stated in RFC-3810 9.2.")
    );

    m.add_leaf(
        s.startup_query_count,
        vtss::tag::Name("StartupQueryCount"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used to control MLD protocol stack "
            "as stated in RFC-3810 9.7.")
    );

    m.add_leaf(
        s.querier_expiry_time,
        vtss::tag::Name("QuerierExpiryTime"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used to control MLD protocol stack "
            "as stated in RFC-3810 9.5.")
    );

    m.add_leaf(
        s.counter_tx_query,
        vtss::tag::Name("CounterTxQuery"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "MLD interface for transmitting MLD Query control frames.")
    );

    m.add_leaf(
        s.counter_tx_specific_query,
        vtss::tag::Name("CounterTxSpecificQuery"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "MLD interface for transmitting MLD Specific Query control frames.")
    );

    m.add_leaf(
        s.counter_rx_query,
        vtss::tag::Name("CounterRxQuery"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "MLD interface for receiving MLD Query control frames.")
    );

    m.add_leaf(
        s.counter_rx_v1_report,
        vtss::tag::Name("CounterRxV1Report"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "MLD interface for receiving MLDv1 Report control frames.")
    );

    m.add_leaf(
        s.counter_rx_v1_done,
        vtss::tag::Name("CounterRxV1Done"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "MLD interface for receiving MLDv1 Done control frames.")
    );

    m.add_leaf(
        s.counter_rx_v2_report,
        vtss::tag::Name("CounterRxV2Report"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "MLD interface for receiving MLDv2 Report control frames.")
    );

    m.add_leaf(
        s.counter_rx_errors,
        vtss::tag::Name("CounterRxErrors"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "MLD interface for receiving invalid MLD control frames.")
    );
}


template<typename T>
void serialize(T &a, vtss_appl_ipmc_mvr_grp_address_t &s)
{
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_mvr_grp_address_t"));
    int                     ix = 0;
    vtss::PortListStackable &list = (vtss::PortListStackable &)s.member_ports;

    m.add_leaf(
        list,
        vtss::tag::Name("MemberPorts"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote the memberships of the registered "
            "multicast group address from MVR.")
    );

    m.add_leaf(
        vtss::AsBool(s.hardware_switch),
        vtss::tag::Name("HardwareSwitch"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote whether the multicast traffic "
            "destined to the registered group address could be forwarding by switch "
            "hardware or not.")
    );
}



template<typename T>
void serialize(T &a, vtss_appl_ipmc_mvr_grp_srclist_t &s)
{
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_mvr_grp_srclist_t"));
    int ix = 0;

    m.add_leaf(
        s.group_filter_mode,
        vtss::tag::Name("GroupFilterMode"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote the source filtering mode of "
            "the specific registered multicast group address from MVR.")
    );

    m.add_leaf(
        s.filter_timer,
        vtss::tag::Name("FilterTimer"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to count down the timer for the specific "
            "multicast group's filtering mode transition.")
    );

    m.add_leaf(
        s.source_type,
        vtss::tag::Name("SourceType"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote the filtering type of the specific "
            "source address in multicasting to the registered group address.")
    );

    m.add_leaf(
        s.source_timer,
        vtss::tag::Name("SourceTimer"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to count down the timer for purging the specific "
            "source address from the registered multicast group's source list.")
    );

    m.add_leaf(
        vtss::AsBool(s.hardware_switch),
        vtss::tag::Name("HardwareFilter"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote whether the traffic destined to "
            "the multicast group address from the specific source address could be "
            "forwarding by switch hardware or not.")
    );
}


namespace vtss {
namespace appl {
namespace mvr {
namespace interfaces {
struct IpmcMvrGlobalsConfig {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_appl_ipmc_mvr_global_t *>
    > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ipmc_mvr_global_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_mvr_system_config_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ipmc_mvr_system_config_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MVR);
};

struct IpmcMvrPortConfigTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_mvr_port_conf_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for managing extra MVR helper features per port basis.";

    static constexpr const char *index_description =
        "Each port has a set of parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_mvr_port_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ipmc_mvr_port_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_mvr_port_config_get);
  // The iterator vtss_appl_iterator_ifindex_front_port_exist works for Lakeinari, but I
  // am not sure it is the right choise in case of stacking. When the right generic iterator
  // has been found, this should be used for all platforms
    VTSS_EXPOSE_ITR_PTR(vtss_appl_iterator_ifindex_front_port_exist);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ipmc_mvr_port_config_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MVR);
};

struct IpmcMvrIgmpVlanConfigTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_mvr_interface_t *>
    > P;

    static constexpr uint32_t snmpRowEditorOid = 100;
    static constexpr const char *table_description =
        "This is a table for managing MVR VLAN interface entries.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_mvr_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ipmc_mvr_interface_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_mvr_vlan_config_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_mvr_vlan_general_itr);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ipmc_mvr_vlan_config_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_ipmc_mvr_vlan_config_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_ipmc_mvr_vlan_config_del);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MVR);
};

struct IpmcMvrVlanPortConfigTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_mvr_intf_port_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for managing MVR port roles of a specific MVR VLAN interface.";

    static constexpr const char *index_description =
        "Each port has a set of parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_mvr_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, ipmc_mvr_port_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_ipmc_mvr_intf_port_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_mvr_intf_port_config_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_mvr_intf_port_itr);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ipmc_mvr_intf_port_config_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MVR);
};

struct IpmcMvrStatusGrpCnt {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_appl_ipmc_mvr_grp_adrs_cnt_t *>
    > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ipmc_mvr_grp_adrs_cnt_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_mvr_grp_adr_cnt_get);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MVR);
};

struct IpmcMvrIgmpVlanStatusTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_mvr_intf_ipv4_status_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for displaying the per VLAN interface status in MVR from IGMP protocol.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_mvr_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ipmc_mvr_intf_ipv4_status_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_mvr_igmp_vlan_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_mvr_vlan_general_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MVR);
};

struct IpmcMvrIgmpGrpadrsTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamKey<mesa_ipv4_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_mvr_grp_address_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for displaying the registered IPv4 multicast group address status in IGMP from MVR.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_mvr_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(mesa_ipv4_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, ipmc_mvr_ipv4_grp_addr_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_ipmc_mvr_grp_address_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_mvr_igmp_group_address_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_mvr_igmp_group_address_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MVR);
};

struct IpmcMvrIgmpSrclistTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamKey<mesa_ipv4_t *>,
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamKey<mesa_ipv4_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_mvr_grp_srclist_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for displaying the address SFM (a.k.a Source List Multicast) status in source list of the registered IPv4 multicast group in IGMP from MVR.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_mvr_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(mesa_ipv4_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, ipmc_mvr_ipv4_grp_addr_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3));
        serialize(h, ipmc_mvr_port_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_4(mesa_ipv4_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(4));
        serialize(h, ipmc_mvr_ipv4_src_addr_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_5(vtss_appl_ipmc_mvr_grp_srclist_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(5));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_mvr_igmp_group_srclist_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_mvr_igmp_group_srclist_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MVR);
};

struct IpmcMvrMldVlanStatusTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_mvr_intf_ipv6_status_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for displaying the per VLAN interface status in MVR from MLD protocol.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_mvr_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ipmc_mvr_intf_ipv6_status_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_mvr_mld_vlan_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_mvr_vlan_general_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MVR);
};

struct IpmcMvrMldGrpadrsTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamKey<mesa_ipv6_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_mvr_grp_address_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for displaying the registered IPv6 multicast group address status in MLD from MVR.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_mvr_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(mesa_ipv6_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, ipmc_mvr_ipv6_grp_addr_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_ipmc_mvr_grp_address_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_mvr_mld_group_address_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_mvr_mld_group_address_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MVR);
};

struct IpmcMvrMldSrclistTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamKey<mesa_ipv6_t *>,
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamKey<mesa_ipv6_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_mvr_grp_srclist_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for displaying the address SFM (a.k.a Source List Multicast) status in source list of the registered IPv6 multicast group in MLD from MVR.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_mvr_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(mesa_ipv6_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, ipmc_mvr_ipv6_grp_addr_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3));
        serialize(h, ipmc_mvr_port_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_4(mesa_ipv6_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(4));
        serialize(h, ipmc_mvr_ipv6_src_addr_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_5(vtss_appl_ipmc_mvr_grp_srclist_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(5));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_mvr_mld_group_srclist_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_mvr_mld_group_srclist_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MVR);
};

struct IpmcMvrControlClearStatistics {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_ifindex_t *>
    > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_mvr_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_mvr_control_statistics_dummy_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ipmc_mvr_statistics_clear_by_vlan_act);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MVR);
};
}  // namespace interfaces
}  // namespace mvr
}  // namespace appl
}  // namespace vtss

#endif /* __VTSS_IPMC_MVR_SERIALIZER_HXX__ */
