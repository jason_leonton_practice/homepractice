/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "mvr_serializer.hxx"

vtss_enum_descriptor_t ipmc_mvr_intfMode_txt[] {
    {VTSS_APPL_IPMC_MVR_INTF_MODE_DYNAMIC,      "dynamic"},
    {VTSS_APPL_IPMC_MVR_INTF_MODE_COMPATIBLE,   "compatible"},
    {0, 0}
};

vtss_enum_descriptor_t ipmc_mvr_vlanTag_txt[] {
    {VTSS_APPL_IPMC_INTF_UNTAG,                 "untagged"},
    {VTSS_APPL_IPMC_INTF_TAGED,                 "tagging"},
    {0, 0}
};

vtss_enum_descriptor_t ipmc_mvr_portRole_txt[] {
    {VTSS_APPL_IPMC_MVR_PORT_ROLE_INACT,        "inactive"},
    {VTSS_APPL_IPMC_MVR_PORT_ROLE_SOURCE,       "source"},
    {VTSS_APPL_IPMC_MVR_PORT_ROLE_RECEIVER,     "receiver"},
    {0, 0}
};

vtss_enum_descriptor_t ipmc_mvr_querierType_txt[] {
    {VTSS_APPL_IPMC_QUERIER_DISABLED,           "disabled"},
    {VTSS_APPL_IPMC_QUERIER_INIT,               "initial"},
    {VTSS_APPL_IPMC_QUERIER_IDLE,               "idle"},
    {VTSS_APPL_IPMC_QUERIER_ACTIVE,             "active"},
    {0, 0}
};

vtss_enum_descriptor_t ipmc_mvr_sfmType_txt[] {
    {VTSS_APPL_IPMC_SF_MODE_EXCLUDE,            "exclude"},
    {VTSS_APPL_IPMC_SF_MODE_INCLUDE,            "include"},
    {0, 0}
};

vtss_enum_descriptor_t ipmc_mvr_srclistType_txt[] {
    {VTSS_APPL_MVR_ACTION_DENY,                 "deny"},
    {VTSS_APPL_MVR_ACTION_PERMIT,               "permit"},
    {0, 0}
};

mesa_rc vtss_appl_ipmc_mvr_control_statistics_dummy_get(vtss_ifindex_t *const act_value) {
    if (act_value) {
        *act_value = VTSS_IFINDEX_VLAN_OFFSET;
    }

    return VTSS_RC_OK;
}

