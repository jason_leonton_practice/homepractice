/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _MVR_API_H_
#define _MVR_API_H_

#include "mvr_conf.h"

#ifdef __cplusplus
extern "C" {
#endif
/* Initialize MVR module */
mesa_rc mvr_init(vtss_init_data_t *data);

/* Set MVR Global Mode */
mesa_rc mvr_mgmt_set_mode(BOOL *mode);

/* Get MVR Global Mode */
mesa_rc mvr_mgmt_get_mode(BOOL *mode);

/* Set MVR VLAN Interface */
mesa_rc mvr_mgmt_set_intf_entry(vtss_isid_t isid_in, ipmc_operation_action_t action, mvr_mgmt_interface_t *entry);

/* Validate MVR VLAN Channel */
mesa_rc mvr_mgmt_validate_intf_channel(void);

/* Get MVR VLAN Interface By Name(String) */
mesa_rc mvr_mgmt_get_intf_entry_by_name(vtss_isid_t isid, mvr_mgmt_interface_t *entry);

/* Get-Next MVR VLAN Interface By Name(String) */
mesa_rc mvr_mgmt_get_next_intf_entry_by_name(vtss_isid_t isid, mvr_mgmt_interface_t *entry);

/* Get MVR VLAN Interface */
mesa_rc mvr_mgmt_get_intf_entry(vtss_isid_t isid, mvr_mgmt_interface_t *entry);

/* Get-Next MVR VLAN Interface */
mesa_rc mvr_mgmt_get_next_intf_entry(vtss_isid_t isid, mvr_mgmt_interface_t *entry);

/* Get Local MVR VLAN Interface */
mesa_rc mvr_mgmt_local_interface_get(mvr_local_interface_t *entry);

/* Get-Next Local MVR VLAN Interface */
mesa_rc mvr_mgmt_local_interface_get_next(mvr_local_interface_t *entry);

/* Get MVR VLAN Operational Status */
mesa_rc mvr_mgmt_get_intf_info(vtss_isid_t isid, ipmc_ip_version_t version, ipmc_prot_intf_entry_param_t *entry);

/* Get-Next MVR VLAN Operational Status */
mesa_rc mvr_mgmt_get_next_intf_info(vtss_isid_t isid, ipmc_ip_version_t *version, ipmc_prot_intf_entry_param_t *entry);

/* GetNext IPMC Interface(VLAN) Group Info */
mesa_rc mvr_mgmt_get_next_intf_group(vtss_isid_t isid,
                                     ipmc_ip_version_t *ipmc_version,
                                     u16 *vid,
                                     ipmc_prot_intf_group_entry_t *intf_group_entry);

/* Get MVR Group Info */
mesa_rc
mvr_mgmt_group_info_get(
    const vtss_isid_t               sidx,
    const ipmc_ip_version_t         verx,
    const mesa_vid_t                *const vidx,
    const mesa_ipv6_t               *const grpx,
    ipmc_prot_intf_group_entry_t    *const intf_group_entry
);

/* GetNext MVR Group Info */
mesa_rc
mvr_mgmt_group_info_get_next(
    const vtss_isid_t               sidx,
    const ipmc_ip_version_t         verx,
    const mesa_vid_t                *const vidx,
    const mesa_ipv6_t               *const grpx,
    ipmc_prot_intf_group_entry_t    *const intf_group_entry
);

/* GetNext SourceList Entry per Group */
mesa_rc
mvr_mgmt_get_next_group_srclist(
    const vtss_isid_t               isid,
    const ipmc_ip_version_t         ipmc_version,
    const mesa_vid_t                vid,
    const mesa_ipv6_t               *const addr,
    ipmc_prot_group_srclist_t       *const group_srclist_entry
);

/* Set MVR Fast leave Ports */
mesa_rc mvr_mgmt_set_fast_leave_port(vtss_isid_t isid, mvr_conf_fast_leave_t *fast_leave_port);

/* Get MVR Fast leave Ports */
mesa_rc mvr_mgmt_get_fast_leave_port(vtss_isid_t isid, mvr_conf_fast_leave_t *fast_leave_port);

/* Clear MVR Statistics Counters */
mesa_rc mvr_mgmt_clear_stat_counter(vtss_isid_t isid, mesa_vid_t vid);

/* Obsoleted API for MVID */
mesa_rc mvr_mgmt_get_mvid(u16 *obs_mvid);

/* Thread manipulation */
mesa_rc mvr_mgmt_sm_thread_suspend(void);
mesa_rc mvr_mgmt_sm_thread_resume(void);

#ifdef __cplusplus
}
#endif
#endif /* _MVR_API_H_ */
