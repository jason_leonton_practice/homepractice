/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "main.h"
#include "msg_api.h"
#include "port_api.h"
#include "critd_api.h"
#include "vlan_api.h"
#include "mgmt_api.h"
#include "mvr.h"
#include "msg_api.h"
#include "mvr_api.h"
#include "vtss/appl/ipmc_mvr.h"
#ifdef VTSS_SW_OPTION_ICFG
#include "mvr_icfg.h"
#endif /* VTSS_SW_OPTION_ICFG */
#include "misc_api.h"
#include "vtss_timer_api.h"
#include "ipmc_trace.h"


#define VTSS_ALLOC_MODULE_ID        VTSS_MODULE_ID_MVR

#define MVR_VLAN_TYPE               VTSS_APPL_VLAN_USER_MVR   /* VTSS_APPL_VLAN_USER_STATIC */
#define MVR_THREAD_EXE_SUPP         0

#define MVR_THREAD_SECOND_DEF       1000        /* 1000 msec = 1 second */
#define MVR_THREAD_TICK_TIME        1000        /* 1000 msec */
#define MVR_THREAD_TIME_UNIT_BASE   (MVR_THREAD_SECOND_DEF / MVR_THREAD_TICK_TIME)
#define MVR_THREAD_TIME_MISC_BASE   (MVR_THREAD_TIME_UNIT_BASE * 5)
#define MVR_THREAD_START_DELAY_CNT  15
#define MVR_THREAD_MIN_DELAY_CNT    3

#define MVR_EVENT_ANY               0xFFFFFFFF  /* Any possible bit */
#define MVR_EVENT_SM_TIME_WAKEUP    0x00000001
#define MVR_EVENT_SWITCH_ADD        0x00000010
#define MVR_EVENT_SWITCH_DEL        0x00000020
#define MVR_EVENT_PKT_HANDLER       0x00000100
#define MVR_EVENT_VLAN_PORT_CHG     0x00001000
#define MVR_EVENT_MASTER_UP         0x00010000
#define MVR_EVENT_MASTER_DOWN       0x00020000
#define MVR_EVENT_THREAD_RESUME     0x10000000
#define MVR_EVENT_THREAD_SUSPEND    0x20000000


/* MVR Event Values */
#define MVR_EVENT_VALUE_INIT        0x0
#define MVR_EVENT_VALUE_SW_ADD      0x00000001
#define MVR_EVENT_VALUE_SW_DEL      0x00000010

/* Thread variables */
/* MVR Timer Thread */
static vtss_handle_t                mvr_sm_thread_handle;
static vtss_thread_t                mvr_sm_thread_block;

static struct {
    critd_t                         crit;
    critd_t                         get_crit;

    mvr_configuration_t             mvr_conf;

    vtss_flag_t                     vlan_entry_flags;
    ipmc_prot_intf_entry_param_t    interface_entry;
    ipmc_prot_intf_group_entry_t    intf_group_entry;
    ipmc_prot_group_srclist_t       group_srclist_entry;

    vtss_flag_t                     grp_entry_flags;
    vtss_flag_t                     grp_nxt_entry_flags;
    ipmc_prot_intf_group_entry_t    group_entry;
    ipmc_prot_intf_group_entry_t    group_next_entry;

    /* Request semaphore */
    struct {
        vtss_sem_t   sem;
    } request;

    /* Reply semaphore */
    struct {
        vtss_sem_t   sem;
    } reply;

    /* MSG Buffer */
    u8                  *msg[MVR_MSG_MAX_ID];
    u32                 msize[MVR_MSG_MAX_ID];
} mvr_running;

#if VTSS_TRACE_ENABLED

static vtss_trace_reg_t mvr_trace_reg = {
    VTSS_MODULE_ID_MVR, "MVR", "Multicast VLAN"
};

static vtss_trace_grp_t mvr_trace_grps[TRACE_GRP_CNT] = {
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CRIT */ {
        "crit",
        "Critical regions",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_RX */ {
        "rx",
        "Packet receive flow",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_TX */ {
        "tx",
        "Packet transmit glow",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_QUERIER_STATE */ {
        "querier",
        "Querier state",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    }
};

#endif /* VTSS_TRACE_ENABLED */

#if VTSS_TRACE_ENABLED

#define MVR_CRIT_ENTER() critd_enter(&mvr_running.crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE,  __FILE__, __LINE__)
#define MVR_CRIT_EXIT()  critd_exit(&mvr_running.crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE,  __FILE__, __LINE__)

#define MVR_GET_CRIT_ENTER() critd_enter(&mvr_running.get_crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE,  __FILE__, __LINE__)
#define MVR_GET_CRIT_EXIT()  critd_exit(&mvr_running.get_crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE,  __FILE__, __LINE__)

#else

#define MVR_CRIT_ENTER() critd_enter(&mvr_running.crit)
#define MVR_CRIT_EXIT()  critd_exit(&mvr_running.crit)

#define MVR_GET_CRIT_ENTER() critd_enter(&mvr_running.get_crit)
#define MVR_GET_CRIT_EXIT()  critd_exit(&mvr_running.get_crit)

#endif /* VTSS_TRACE_ENABLED */

#ifdef VTSS_SW_OPTION_PACKET
static mesa_packet_rx_info_t    ipmc_mvr_rx_info;
#endif /* VTSS_SW_OPTION_PACKET */
static u8                       ipmcmvr_frame[IPMC_LIB_PKT_BUF_SZ];
static ipmc_lib_log_t           ipmcmvr_rx_packet_log;
static i8                       ipmcmvr_rx_pktlog_msg[IPMC_LIB_LOG_MSG_SZ_VAL];
static BOOL ipmcmvr_rx_packet_callback(void *contxt, const uchar *const frm, const mesa_packet_rx_info_t *const rx_info, BOOL next_ipmc)
{
    mesa_rc             rc = VTSS_OK;
    ipmc_ip_eth_hdr     *etherHdr;
    ipmc_ip_version_t   version = IPMC_IP_VERSION_INIT;
    ipmc_port_bfs_t     ipmcmvr_fwd_map;
    u16                 ingress_vid;
    BOOL                ingress_chk = TRUE; // Should be set 'FALSE' when checking error
    mesa_port_list_t    ingress_member;
    vtss_mvr_intf_tx_t  pcp[MVR_NUM_OF_INTF_PER_VERSION];
    mesa_glag_no_t      glag_no;
    u32                 i, local_port_cnt;
    BOOL                global_profile_state = FALSE;

    T_DG_PORT(TRACE_GRP_RX, rx_info->port_no,
              "Enter: vid = %d, glag_no=%u, %s Frame with length %d received",
              rx_info->tag.vid, rx_info->glag_no,
              ipmc_lib_frm_tagtype_txt(rx_info->tag_type, IPMC_TXT_CASE_CAPITAL),
              rx_info->length);
    T_D_HEX(frm, rx_info->length);

    glag_no = rx_info->glag_no;

    memset(&ipmcmvr_fwd_map, 0x0, sizeof(ipmc_port_bfs_t));
    memset(ingress_member, 0x0, sizeof(ingress_member));
    etherHdr = (ipmc_ip_eth_hdr *) frm;
    if (ntohs(etherHdr->type) == IP_MULTICAST_V6_ETHER_TYPE) {
        version = IPMC_IP_VERSION_MLD;
    } else {
        if (ntohs(etherHdr->type) == IP_MULTICAST_V4_ETHER_TYPE) {
            version = IPMC_IP_VERSION_IGMP;
        }
    }

    /* Simply checking for the global profile state */
    (void) ipmc_lib_profile_state_get(&global_profile_state);
    if (!global_profile_state) {
        rc = IPMC_ERROR_PKT_INGRESS_FILTER;
        ingress_chk = FALSE;
    }

    /* (VLAN) Ingress Filtering */
    memset(pcp, 0x0, sizeof(pcp));
    ingress_vid = rx_info->tag.vid;
    if (ingress_vid == VTSS_APPL_IPMC_VID_NULL ||
        mesa_vlan_port_members_get(NULL, ingress_vid, &ingress_member) != VTSS_RC_OK ||
        !ingress_member[rx_info->port_no]) {
        rc = IPMC_ERROR_PKT_INGRESS_FILTER;
        ingress_chk = FALSE;
    }


    local_port_cnt = ipmc_lib_get_system_local_port_cnt();

#ifdef VTSS_SW_OPTION_PACKET
    if (ingress_chk) {
        char    mvrBufPort[MGMT_PORT_BUF_SIZE];

        MVR_CRIT_ENTER();
        memcpy(&ipmc_mvr_rx_info, rx_info, sizeof(ipmc_mvr_rx_info));
        ipmc_lib_packet_strip_vtag(frm, &ipmcmvr_frame[0], rx_info->tag_type, &ipmc_mvr_rx_info);

        rc = RX_ipmcmvr(version, ingress_vid, contxt, &ipmcmvr_frame[0], &ipmc_mvr_rx_info, &ipmcmvr_fwd_map, pcp);
        MVR_CRIT_EXIT();

        /* MVR determines the forwarding map by its own database. */
        for (i = 0; i < local_port_cnt; i++) {
            if (i != rx_info->port_no) {
                ingress_member[i] = VTSS_PORT_BF_GET(ipmcmvr_fwd_map.member_ports, i);
            } else {
                ingress_member[i] = FALSE;
            }

            VTSS_PORT_BF_SET(ipmcmvr_fwd_map.member_ports,
                             i,
                             ingress_member[i]);
        }

        T_DG(TRACE_GRP_RX, "ipmcmvr_fwd_map(%s) is %s", error_txt(rc), mgmt_iport_list2txt(ingress_member, mvrBufPort));
    }
#endif /* VTSS_SW_OPTION_PACKET */

    switch (rc) {
    case IPMC_ERROR_PKT_INGRESS_FILTER:
    case IPMC_ERROR_PKT_CHECKSUM:
    case IPMC_ERROR_PKT_FORMAT:
        /* silently ignored */
        T_DG_PORT(TRACE_GRP_RX, rx_info->port_no, "Exit: %s", error_txt(rc));
        return FALSE;
    case IPMC_ERROR_PKT_CONTENT:
    case IPMC_ERROR_PKT_ADDRESS:
    case IPMC_ERROR_PKT_VERSION:
    case IPMC_ERROR_PKT_RESERVED:
        /* we shall flood this one */
        vtss_mvr_calculate_dst_ports(FALSE, FALSE, ingress_vid, rx_info->port_no, &ipmcmvr_fwd_map, version);

        MVR_CRIT_ENTER();
        memset(&ipmcmvr_rx_packet_log, 0x0, sizeof(ipmc_lib_log_t));
        ipmcmvr_rx_packet_log.vid = ingress_vid;
        ipmcmvr_rx_packet_log.port = rx_info->port_no;
        ipmcmvr_rx_packet_log.version = version;
        ipmcmvr_rx_packet_log.event.message.data = ipmcmvr_rx_pktlog_msg;

        memset(ipmcmvr_rx_pktlog_msg, 0x0, sizeof(ipmcmvr_rx_pktlog_msg));
        if (rc == IPMC_ERROR_PKT_CONTENT) {
            sprintf(ipmcmvr_rx_pktlog_msg, "(IPMC_ERROR_PKT_CONTENT)->RX frame with bad content!");
        }
        if (rc == IPMC_ERROR_PKT_RESERVED) {
            sprintf(ipmcmvr_rx_pktlog_msg, "(IPMC_ERROR_PKT_RESERVED)->RX reserved group address registration!");
        }
        if (rc == IPMC_ERROR_PKT_ADDRESS) {
            sprintf(ipmcmvr_rx_pktlog_msg, "(IPMC_ERROR_PKT_ADDRESS)->RX frame with bad address!");
        }
        if (rc == IPMC_ERROR_PKT_VERSION) {
            sprintf(ipmcmvr_rx_pktlog_msg, "(IPMC_ERROR_PKT_ADDRESS)->RX frame with bad version!");
        }
        IPMC_LIB_LOG_MSG(&ipmcmvr_rx_packet_log, IPMC_SEVERITY_Warning);
        MVR_CRIT_EXIT();
        break;
    default:
        break;
    }


    if (IPMC_LIB_BFS_HAS_MEMBER(ipmcmvr_fwd_map.member_ports)) {
        BOOL    src_fast_leave;

        MVR_CRIT_ENTER();
        src_fast_leave = vtss_mvr_get_fast_leave_ports(rx_info->port_no);
        MVR_CRIT_EXIT();

        if (ingress_chk &&
            ((rc == VTSS_OK) ||
             (!next_ipmc &&
              ((rc == IPMC_ERROR_PKT_IS_QUERY) ||
               (rc == IPMC_ERROR_PKT_GROUP_FILTER) ||
               (rc == IPMC_ERROR_PKT_GROUP_NOT_FOUND))))) {
            for (i = 0; i < MVR_NUM_OF_INTF_PER_VERSION; i++) {
                if (!pcp[i].vid) {
                    continue;
                }

                T_DG_PORT(TRACE_GRP_TX, rx_info->port_no, "vid = %d", pcp[i].vid);
                if (ipmc_lib_packet_tx(&ipmcmvr_fwd_map,
                                       (pcp[i].vtag == IPMC_INTF_UNTAG) ? TRUE : FALSE,
                                       src_fast_leave,
                                       rx_info->port_no,
                                       pcp[i].src_type,
                                       pcp[i].vid,
                                       rx_info->tag.dei,
                                       pcp[i].priority,
                                       glag_no,
                                       frm,
                                       rx_info->length) != VTSS_OK) {
                    T_W("Failure in ipmc_lib_packet_tx");
                }
            }
        } else {
            if (!next_ipmc) {
                if (ipmc_lib_packet_tx(&ipmcmvr_fwd_map,
                                       FALSE,
                                       src_fast_leave,
                                       rx_info->port_no,
                                       IPMC_PKT_SRC_MVR,
                                       rx_info->tag.vid,
                                       rx_info->tag.dei,
                                       rx_info->tag.pcp,
                                       glag_no,
                                       frm,
                                       rx_info->length) != VTSS_OK) {
                    T_W("Failure in ipmc_lib_packet_tx");
                }
            }
        }
    }

    if (rc != VTSS_OK) {
        if (rc == IPMC_ERROR_PKT_IS_QUERY) {
            T_DG_PORT(TRACE_GRP_RX, rx_info->port_no, "Exit: %s", error_txt(rc));
            return FALSE;
        } else {
            if ((rc == IPMC_ERROR_PKT_GROUP_FILTER) ||
                (rc == IPMC_ERROR_PKT_GROUP_NOT_FOUND)) {
                if (next_ipmc) {
                    T_DG_PORT(TRACE_GRP_RX, rx_info->port_no, "Exit: %s", error_txt(rc));
                    return FALSE;
                } else {
                    T_DG_PORT(TRACE_GRP_RX, rx_info->port_no, "Exit: %s", error_txt(rc));
                    return TRUE;
                }
            } else {
                T_DG_PORT(TRACE_GRP_RX, rx_info->port_no, "Exit: %s", error_txt(rc));
                return FALSE;
            }
        }
    }

    /* MVR has done the processing; Others shouldn't take care of it. */
    T_DG_PORT(TRACE_GRP_RX, rx_info->port_no, "%s", "Exit");
    return TRUE;
}


/*lint -esym(459, mvr_sm_events) */
static vtss_flag_t         mvr_sm_events;
static vtss::Timer         mvr_sm_timer;
static u32                 mvr_switch_event_value[VTSS_ISID_END];

static void mvr_sm_event_set(vtss_flag_value_t flag)
{
    vtss_flag_setbits(&mvr_sm_events, flag);
}

static void mvr_sm_timer_isr(struct vtss::Timer *timer)
{
    mvr_sm_event_set(MVR_EVENT_SM_TIME_WAKEUP);
}

static void mvr_conf_reset_intf_role(vtss_isid_t isid, mesa_vid_t vid, mvr_conf_intf_role_t *role, BOOL clear)
{
    port_iter_t             pit;
    u32                     i;
    mvr_conf_port_role_t    *port_role;

    if (!role) {
        return;
    }

    for (i = 0; i < MVR_VLAN_MAX; i++) {
        port_role = &role->intf[i];
        if ((vid != MVR_VID_VOID) && (port_role->vid != vid)) {
            continue;
        }

        if (clear) {
            memset(port_role, 0x0, sizeof(mvr_conf_port_role_t));
        }

        if (msg_switch_is_master()) {
            (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        } else {
            (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        }
        while (port_iter_getnext(&pit)) {
            port_role->ports[pit.iport] = MVR_CONF_DEF_PORT_ROLE;
        }
    }
}

static void mvr_conf_reset_intf(mvr_conf_intf_entry_t *intf, BOOL clear)
{
    if (!intf) {
        return;
    }

    if (clear) {
        memset(intf, 0x0, sizeof(mvr_conf_intf_entry_t));
    }

    intf->mode = MVR_CONF_DEF_INTF_MODE;
    intf->vtag = MVR_CONF_DEF_INTF_VTAG;
    intf->querier_status = VTSS_IPMCMVR_DISABLED;
    intf->querier4_address = MVR_CONF_DEF_INTF_ADRS4;
    intf->priority = MVR_CONF_DEF_INTF_PRIO;
    intf->profile_index = MVR_CONF_DEF_INTF_PROFILE;
    intf->protocol_status = VTSS_IPMCMVR_ENABLED;
    intf->compatibility = IPMC_PARAM_DEF_COMPAT;
    intf->robustness_variable = IPMC_PARAM_DEF_RV;
    intf->query_interval = IPMC_PARAM_DEF_QI;
    intf->query_response_interval = IPMC_PARAM_DEF_QRI;
    intf->last_listener_query_interval = MVR_CONF_DEF_INTF_LLQI;
    intf->unsolicited_report_interval = IPMC_PARAM_DEF_URI;
}

static void mvr_conf_default(void)
{
    int                     i;
    switch_iter_t           sit;
    vtss_isid_t             j;
    mvr_conf_global_t       *global;
    mvr_conf_intf_entry_t   *intf;
    mvr_conf_intf_role_t    *intf_role;
    mvr_configuration_t     *conf;

    MVR_CRIT_ENTER();

    conf = &mvr_running.mvr_conf;

    /* Use default configuration */
    memset(conf, 0x0, sizeof(mvr_configuration_t));

    global = &conf->mvr_conf_global;
    global->mvr_state = MVR_CONF_DEF_GLOBAL_MODE;

    for (i = 0; i < MVR_VLAN_MAX; i++) {
        intf = &conf->mvr_conf_vlan[i];
        mvr_conf_reset_intf(intf, FALSE);
    }

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        j = sit.isid;

        intf_role = &conf->mvr_conf_role[ipmc_lib_isid_convert(TRUE, j)];
        mvr_conf_reset_intf_role(j, MVR_VID_VOID, intf_role, FALSE);
    }
    intf_role = &conf->mvr_conf_role[VTSS_ISID_LOCAL];
    mvr_conf_reset_intf_role(VTSS_ISID_LOCAL, MVR_VID_VOID, intf_role, FALSE);
    MVR_CRIT_EXIT();
}

static void _mvr_set_querying_enable(ipmc_intf_entry_t *ipmc_intf, BOOL new_mode)
{
    if (ipmc_intf == NULL) {
        return;
    }

    if (ipmc_intf->param.querier.querier_enabled != new_mode) {
        T_DG(TRACE_GRP_QUERIER_STATE, "IPMCVer-%d, vid = %d, old_election_mode = %d, new_election_mode = %d",
             ipmc_intf->ipmc_version, ipmc_intf->param.vid, ipmc_intf->param.querier.querier_enabled, new_mode);
        ipmc_intf->param.querier.querier_enabled = new_mode;
        ipmc_intf->param.querier.state = IPMC_QUERIER_IDLE;
        IPMC_LIB_ADRS_SET(&ipmc_intf->param.active_querier, 0x0);

        if (new_mode) {
            ipmc_intf->param.querier.state = IPMC_QUERIER_INIT;
            ipmc_intf->param.querier.timeout = 0;

            ipmc_intf->param.querier.QuerierUpTime = 0;
            ipmc_intf->param.querier.OtherQuerierTimeOut = 0;

            ipmc_intf->param.querier.StartUpItv = IPMC_TIMER_SQI(ipmc_intf);
            ipmc_intf->param.querier.StartUpCnt = IPMC_TIMER_SQC(ipmc_intf);
            ipmc_intf->param.querier.LastQryCnt = IPMC_TIMER_LLQC(ipmc_intf);
        } else {
            ipmc_intf->param.querier.timeout = IPMC_TIMER_QI(ipmc_intf);

            ipmc_intf->param.querier.OtherQuerierTimeOut = IPMC_TIMER_OQPT(ipmc_intf);
        }

        ipmc_intf->param.querier.proxy_query_timeout = 0x0;
    }
}

static void _mvr_mgmt_transit_intf(vtss_isid_t isid_in,
                                   vtss_isid_t isid,
                                   vtss_mvr_interface_t *intf,
                                   mvr_mgmt_interface_t *entry,
                                   ipmc_ip_version_t version)
{
    port_iter_t                     pit;
    u32                             idx;
    ipmc_intf_entry_t               *p;
    ipmc_prot_intf_entry_param_t    *param;
    mvr_conf_intf_entry_t           *iif;
    mvr_conf_intf_role_t            *pif;
    mvr_conf_port_role_t            *ptr;

    if (!intf || !entry) {
        return;
    }

    p = &intf->basic;
    param = &p->param;
    iif = &entry->intf;

    p->ipmc_version = version;
    param->vid = entry->vid;
    if (vtss_mvr_interface_get(intf) != VTSS_OK) {
        memset(intf, 0x0, sizeof(vtss_mvr_interface_t));
    }

    p->ipmc_version = version;
    param->vid = entry->vid;
    memcpy(intf->name, iif->name, MVR_NAME_MAX_LEN);
    intf->name[MVR_NAME_MAX_LEN - 1] = 0;
    intf->mode = iif->mode;
    intf->vtag = iif->vtag;
    intf->priority = iif->priority;
    intf->profile_index = iif->profile_index;
    param->querier.QuerierAdrs4 = iif->querier4_address;
    param->querier.RobustVari = MVR_QUERIER_ROBUST_VARIABLE;
    param->querier.QueryIntvl = MVR_QUERIER_QUERY_INTERVAL;
    param->querier.MaxResTime = MVR_QUERIER_MAX_RESP_TIME;
    param->querier.LastQryItv = iif->last_listener_query_interval;
    param->querier.LastQryCnt = param->querier.RobustVari;
    param->querier.UnsolicitR = MVR_QUERIER_UNSOLICIT_REPORT;
    param->cfg_compatibility = IPMC_PARAM_DEF_COMPAT;
    param->rtr_compatibility.mode = VTSS_IPMC_COMPAT_MODE_SFM;
    param->hst_compatibility.mode = VTSS_IPMC_COMPAT_MODE_SFM;

    // Update querier state when Query election mode is changed
    _mvr_set_querying_enable(p, iif->querier_status);
    param->querier.querier_enabled = iif->querier_status;

    if (isid_in != VTSS_ISID_GLOBAL) {
        ptr = &entry->role;
        if (msg_switch_is_master()) {
            (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        } else {
            (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        }
        while (port_iter_getnext(&pit)) {
            idx = pit.iport;

            intf->ports[idx] = ptr->ports[idx];

            if (intf->ports[idx] != MVR_PORT_ROLE_INACT) {
                VTSS_PORT_BF_SET(p->vlan_ports, idx, TRUE);
            }
        }
    } else {
        if (msg_switch_is_master()) {
            pif = &mvr_running.mvr_conf.mvr_conf_role[ipmc_lib_isid_convert(TRUE, isid)];
            for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
                ptr = &pif->intf[idx];

                if (ptr->valid && (ptr->vid == param->vid)) {
                    (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
                    while (port_iter_getnext(&pit)) {
                        idx = pit.iport;

                        intf->ports[idx] = ptr->ports[idx];

                        if (intf->ports[idx] != MVR_PORT_ROLE_INACT) {
                            VTSS_PORT_BF_SET(p->vlan_ports, idx, TRUE);
                        }
                    }

                    break;
                }
            }
        }
    }
}

static BOOL mvr_get_port_role_entry(vtss_isid_t isid_in, mvr_conf_port_role_t *entry)
{
    u16                     idx;
    switch_iter_t           sit;
    vtss_isid_t             isid;
    mvr_conf_intf_role_t    *p;
    mvr_conf_port_role_t    *q;

    if (!entry || (entry->vid > MVR_VID_MAX)) {
        return FALSE;
    }

    if (isid_in == VTSS_ISID_LOCAL) {
        p = &mvr_running.mvr_conf.mvr_conf_role[VTSS_ISID_LOCAL];
        for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
            q = &p->intf[idx];

            if (q->valid && (q->vid == entry->vid)) {
                memcpy(entry, q, sizeof(mvr_conf_port_role_t));
                return TRUE;
            }
        }

        return FALSE;
    }

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if (isid != isid_in) {
            continue;
        }

        p = &mvr_running.mvr_conf.mvr_conf_role[ipmc_lib_isid_convert(TRUE, isid)];
        for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
            q = &p->intf[idx];

            if (q->valid && (q->vid == entry->vid)) {
                memcpy(entry, q, sizeof(mvr_conf_port_role_t));
                return TRUE;
            }
        }
    }

    return FALSE;
}

static void _mvr_stacking_reset_port_type(vtss_isid_t isid)
{
    port_iter_t                 pit;
    u32                         idx;
    vtss_appl_vlan_port_conf_t  *vlan_port_conf;

    if (!msg_switch_is_master() ||
        !IPMC_MEM_SYSTEM_MTAKE(vlan_port_conf, sizeof(vtss_appl_vlan_port_conf_t))) {
        return;
    }

    (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
    while (port_iter_getnext(&pit)) {
        mesa_rc rc;

        idx = pit.iport;

        if (vlan_mgmt_port_conf_get(isid, idx, vlan_port_conf, MVR_VLAN_TYPE, TRUE) == VTSS_RC_OK) {
            /*
                to un-override VLAN port settings,
                all we need to do is to clear the #flags member
            */
            vlan_port_conf->hybrid.flags = 0;
            if ((rc = vlan_mgmt_port_conf_set(isid, idx, vlan_port_conf, MVR_VLAN_TYPE)) != VTSS_RC_OK) {
                T_D("Failure in _mvr_stacking_reset_port_type on isid %d port %u. Error: %s", isid, idx, error_txt(rc));
            }
        }
    }

    IPMC_MEM_SYSTEM_MGIVE(vlan_port_conf);
}

static BOOL _mvr_stacking_set_port_type(vtss_isid_t isid, vtss_mvr_interface_t *entry)
{
    port_iter_t                 pit;
    u32                         i, idx;
    mesa_vid_t                  uvid, chk_vid;
    BOOL                        flag;
    vtss_appl_vlan_port_conf_t  *vlan_port_conf, *vlan_port_chk;
    mvr_conf_intf_role_t        *p;
    mvr_conf_port_role_t        *q, chk_role;
    mesa_rc                     rc;

    if (!entry) {
        return FALSE;
    }

    if (!msg_switch_is_master() || (isid == VTSS_ISID_GLOBAL)) {
        return TRUE;
    }

    if (!IPMC_MEM_SYSTEM_MTAKE(vlan_port_conf, sizeof(vtss_appl_vlan_port_conf_t))) {
        return FALSE;
    }
    if (!IPMC_MEM_SYSTEM_MTAKE(vlan_port_chk, sizeof(vtss_appl_vlan_port_conf_t))) {
        IPMC_MEM_SYSTEM_MGIVE(vlan_port_conf);
        return FALSE;
    }

    chk_vid = entry->basic.param.vid;
    memset(&chk_role, 0x0, sizeof(mvr_conf_port_role_t));
    chk_role.vid = chk_vid;
    MVR_CRIT_ENTER();
    if (!mvr_get_port_role_entry(isid, &chk_role)) {
        MVR_CRIT_EXIT();

        IPMC_MEM_SYSTEM_MGIVE(vlan_port_chk);
        IPMC_MEM_SYSTEM_MGIVE(vlan_port_conf);

        return FALSE;
    }
    MVR_CRIT_EXIT();

    (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
    while (port_iter_getnext(&pit)) {
        idx = pit.iport;

        if ((rc = vlan_mgmt_port_conf_get(isid, idx, vlan_port_conf, MVR_VLAN_TYPE, TRUE)) != VTSS_RC_OK) {
            T_D("Failure on isid %d port %u. Error: %s", isid, idx, error_txt(rc));
            continue;
        }

        if (chk_role.ports[idx] == MVR_PORT_ROLE_INACT) {
            u8 active_cnt = 0;

            uvid = 0;
            flag = FALSE;   /* flag to denote source port */

            MVR_CRIT_ENTER();
            p = &mvr_running.mvr_conf.mvr_conf_role[ipmc_lib_isid_convert(TRUE, isid)];
            for (i = 0; i < MVR_VLAN_MAX; i++) {
                q = &p->intf[i];

                if (q->valid && (q->vid != chk_vid)) {
                    if (q->ports[idx] != MVR_PORT_ROLE_INACT) {
                        ++active_cnt;
                        uvid = q->vid;

                        /* pre-defined: same role in a port */
                        if (q->ports[idx] == MVR_PORT_ROLE_SOURC) {
                            flag = TRUE;
                        }
                    }
                }
            }
            MVR_CRIT_EXIT();

            if (active_cnt) {
                if (active_cnt == 1) {
                    vlan_port_conf->hybrid.untagged_vid = uvid;
                }
                if (flag) {
                    /* BZ#16729: Source ports should always send with tagged */
                    vlan_port_conf->hybrid.tx_tag_type = VTSS_APPL_VLAN_TX_TAG_TYPE_TAG_ALL;
                }
            } else {
                /*
                    to un-override VLAN port settings,
                    all we need to do is to clear the #flags member
                */
                vlan_port_conf->hybrid.flags = 0;
            }

            T_D("Set isid-%d port-%u pvid=%u/uvid=%u/frm_t=%d/ing_fltr=%s/tx_tag_t=%d/port_t=%d/flag=%u",
                isid,
                idx,
                vlan_port_conf->hybrid.pvid,
                vlan_port_conf->hybrid.untagged_vid,
                vlan_port_conf->hybrid.frame_type,
                vlan_port_conf->hybrid.ingress_filter ? "TRUE" : "FALSE",
                vlan_port_conf->hybrid.tx_tag_type,
                vlan_port_conf->hybrid.port_type,
                vlan_port_conf->hybrid.flags);
            if ((rc = vlan_mgmt_port_conf_set(isid, idx, vlan_port_conf, MVR_VLAN_TYPE)) != VTSS_RC_OK) {
                T_D("Failure in MVR_PORT_ROLE_INACT on isid %d port %u (%s)", isid, idx, error_txt(rc));
            }

            continue;
        }

        uvid = entry->basic.param.vid;
        flag = FALSE;   /* For overlapped UVID checking */

        MVR_CRIT_ENTER();
        p = &mvr_running.mvr_conf.mvr_conf_role[ipmc_lib_isid_convert(TRUE, isid)];
        for (i = 0; i < MVR_VLAN_MAX; i++) {
            q = &p->intf[i];

            if (q->valid && (q->vid != chk_vid)) {
                if (q->ports[idx] == chk_role.ports[idx]) {
                    flag = TRUE;
                    break;
                }
            }
        }
        MVR_CRIT_EXIT();

        if (flag) {
            if (vlan_mgmt_port_conf_get(isid, idx, vlan_port_chk, VTSS_APPL_VLAN_USER_STATIC, TRUE) == VTSS_RC_OK) {
                uvid = vlan_port_chk->hybrid.pvid;
            } else {
                uvid = VTSS_APPL_VLAN_ID_DEFAULT;
            }
        }

        vlan_port_conf->hybrid.flags |= (VTSS_APPL_VLAN_PORT_FLAGS_AWARE | VTSS_APPL_VLAN_PORT_FLAGS_RX_TAG_TYPE | VTSS_APPL_VLAN_PORT_FLAGS_TX_TAG_TYPE);
        vlan_port_conf->hybrid.untagged_vid = uvid;
        vlan_port_conf->hybrid.port_type = VTSS_APPL_VLAN_PORT_TYPE_C;
        vlan_port_conf->hybrid.frame_type = MESA_VLAN_FRAME_ALL;
        if (chk_role.ports[idx] == MVR_PORT_ROLE_SOURC) {
            /* Sourcer Ports */
            /* BZ#16729: Source ports should always send with tagged */
            vlan_port_conf->hybrid.tx_tag_type = VTSS_APPL_VLAN_TX_TAG_TYPE_TAG_ALL;
        } else {
            /* Receiver Ports */
            vlan_port_conf->hybrid.tx_tag_type = VTSS_APPL_VLAN_TX_TAG_TYPE_UNTAG_ALL;
        }

        T_D("Set isid-%d port-%u pvid=%u/uvid=%u/frm_t=%d/ing_fltr=%s/tx_tag_t=%d/port_t=%d/flag=%u",
            isid,
            idx,
            vlan_port_conf->hybrid.pvid,
            vlan_port_conf->hybrid.untagged_vid,
            vlan_port_conf->hybrid.frame_type,
            vlan_port_conf->hybrid.ingress_filter ? "TRUE" : "FALSE",
            vlan_port_conf->hybrid.tx_tag_type,
            vlan_port_conf->hybrid.port_type,
            vlan_port_conf->hybrid.flags);
        if ((rc = vlan_mgmt_port_conf_set(isid, idx, vlan_port_conf, MVR_VLAN_TYPE)) != VTSS_RC_OK) {
            T_D("Failure(%s) on isid %d port %u (%s)", (chk_role.ports[idx] == MVR_PORT_ROLE_SOURC) ? "SRC" : "RCV", isid, idx, error_txt(rc));
        }
    }

    IPMC_MEM_SYSTEM_MGIVE(vlan_port_chk);
    IPMC_MEM_SYSTEM_MGIVE(vlan_port_conf);

    return TRUE;
}

static BOOL _mvr_stacking_refine_port_type(vtss_isid_t isid)
{
    port_iter_t                 pit;
    u32                         i, idx;
    u16                         uvid, active_cnt;
    BOOL                        flag;
    vtss_appl_vlan_port_conf_t  *vlan_port_conf;
    mvr_conf_intf_role_t        *p;
    mvr_conf_port_role_t        *q;
    mesa_rc                     rc;

    if (!msg_switch_is_master() || (isid == VTSS_ISID_GLOBAL) ||
        !IPMC_MEM_SYSTEM_MTAKE(vlan_port_conf, sizeof(vtss_appl_vlan_port_conf_t))) {
        return TRUE;
    }

    (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
    while (port_iter_getnext(&pit)) {
        idx = pit.iport;

        if ((rc = vlan_mgmt_port_conf_get(isid, idx, vlan_port_conf, MVR_VLAN_TYPE, TRUE)) != VTSS_RC_OK) {
            T_D("Failure on isid %d port %u. Error: %s", isid, idx, error_txt(rc));
            continue;
        }

        uvid = active_cnt = 0;
        flag = FALSE;   /* flag to denote source port */
        MVR_CRIT_ENTER();
        p = &mvr_running.mvr_conf.mvr_conf_role[ipmc_lib_isid_convert(TRUE, isid)];
        for (i = 0; i < MVR_VLAN_MAX; i++) {
            q = &p->intf[i];

            if (!q->valid) {
                continue;
            }

            if (q->ports[idx] != MVR_PORT_ROLE_INACT) {
                ++active_cnt;
                uvid = q->vid;

                /* pre-defined: same role in a port */
                if (q->ports[idx] == MVR_PORT_ROLE_SOURC) {
                    flag = TRUE;
                }
            }
        }
        MVR_CRIT_EXIT();

        if (active_cnt) {
            if (active_cnt == 1) {
                vlan_port_conf->hybrid.untagged_vid = uvid;
            }
            if (flag) {
                /* BZ#16729: Source ports should always send with tagged */
                vlan_port_conf->hybrid.tx_tag_type = VTSS_APPL_VLAN_TX_TAG_TYPE_TAG_ALL;
            }
        } else {
            /*
                to un-override VLAN port settings,
                all we need to do is to clear the #flags member
            */
            vlan_port_conf->hybrid.flags = 0;
        }

        T_D("Refine isid-%d port-%u pvid=%u/uvid=%u/frm_t=%d/ing_fltr=%s/tx_tag_t=%d/port_t=%d/flag=%u",
            isid,
            idx,
            vlan_port_conf->hybrid.pvid,
            vlan_port_conf->hybrid.untagged_vid,
            vlan_port_conf->hybrid.frame_type,
            vlan_port_conf->hybrid.ingress_filter ? "TRUE" : "FALSE",
            vlan_port_conf->hybrid.tx_tag_type,
            vlan_port_conf->hybrid.port_type,
            vlan_port_conf->hybrid.flags);
        if ((rc = vlan_mgmt_port_conf_set(isid, idx, vlan_port_conf, MVR_VLAN_TYPE)) != VTSS_RC_OK) {
            T_D("Failure in setting isid %d port %u (%s)", isid, idx, error_txt(rc));
        }
    }

    IPMC_MEM_SYSTEM_MGIVE(vlan_port_conf);
    return TRUE;
}

static const char *mvr_msg_id_txt(mvr_msg_id_t msg_id)
{
    const char    *txt;

    switch (msg_id) {
    case MVR_MSG_ID_GLOBAL_SET_REQ:
        txt = "MVR_MSG_ID_GLOBAL_SET_REQ";

        break;
    case MVR_MSG_ID_GLOBAL_PURGE_REQ:
        txt = "MVR_MSG_ID_GLOBAL_PURGE_REQ";

        break;
    case MVR_MSG_ID_VLAN_ENTRY_SET_REQ:
        txt = "MVR_MSG_ID_VLAN_ENTRY_SET_REQ";

        break;
    case MVR_MSG_ID_VLAN_ENTRY_GET_REQ:
        txt = "MVR_MSG_ID_VLAN_ENTRY_GET_REQ";

        break;
    case MVR_MSG_ID_VLAN_ENTRY_GET_REP:
        txt = "MVR_MSG_ID_VLAN_ENTRY_GET_REP";

        break;
    case MVR_MSG_ID_VLAN_GROUP_ENTRY_WALK_REQ:
        txt = "MVR_MSG_ID_VLAN_GROUP_ENTRY_WALK_REQ";

        break;
    case MVR_MSG_ID_VLAN_GROUP_ENTRY_WALK_REP:
        txt = "MVR_MSG_ID_VLAN_GROUP_ENTRY_WALK_REP";

        break;
    case MVR_MSG_ID_GROUP_ENTRY_GET_REQ:
        txt = "MVR_MSG_ID_GROUP_ENTRY_GET_REQ";

        break;
    case MVR_MSG_ID_GROUP_ENTRY_GET_REP:
        txt = "MVR_MSG_ID_GROUP_ENTRY_GET_REP";

        break;
    case MVR_MSG_ID_GROUP_ENTRY_GETNEXT_REQ:
        txt = "MVR_MSG_ID_GROUP_ENTRY_GETNEXT_REQ";

        break;
    case MVR_MSG_ID_GROUP_ENTRY_GETNEXT_REP:
        txt = "MVR_MSG_ID_GROUP_ENTRY_GETNEXT_REP";

        break;
    case MVR_MSG_ID_GROUP_SRCLIST_WALK_REQ:
        txt = "MVR_MSG_ID_GROUP_SRCLIST_WALK_REQ";

        break;
    case MVR_MSG_ID_GROUP_SRCLIST_WALK_REP:
        txt = "MVR_MSG_ID_GROUP_SRCLIST_WALK_REP";

        break;
    case MVR_MSG_ID_PORT_FAST_LEAVE_SET_REQ:
        txt = "MVR_MSG_ID_PORT_FAST_LEAVE_SET_REQ";

        break;
    case MVR_MSG_ID_STAT_COUNTER_CLEAR_REQ:
        txt = "MVR_MSG_ID_STAT_COUNTER_CLEAR_REQ";

        break;
    case MVR_MSG_ID_STP_PORT_CHANGE_REQ:
        txt = "MVR_MSG_ID_STP_PORT_CHANGE_REQ";

        break;
    default:
        txt = "?";

        break;
    }

    return txt;
}

/* Allocate request/reply buffer */
static void mvr_msg_alloc(mvr_msg_id_t msg_id, mvr_msg_buf_t *buf, BOOL from_get, BOOL request)
{
    u32 msg_size;

    T_I("msg-%s (%d)", mvr_msg_id_txt(msg_id), msg_id);

    if (from_get) {
        MVR_GET_CRIT_ENTER();
    } else {
        MVR_CRIT_ENTER();
    }
    buf->sem = (request ? &mvr_running.request.sem : &mvr_running.reply.sem);
    buf->msg = mvr_running.msg[msg_id];
    msg_size = mvr_running.msize[msg_id];
    if (from_get) {
        MVR_GET_CRIT_EXIT();
    } else {
        MVR_CRIT_EXIT();
    }

    vtss_sem_wait(buf->sem);
    T_I("msg_id: %d->%s(%s-LOCK)", msg_id, mvr_msg_id_txt(msg_id), request ? "REQ" : "REP");
    memset(buf->msg, 0x0, msg_size);
}

static void mvr_msg_tx_done(void *contxt, void *msg, msg_tx_rc_t rc)
{
    mvr_msg_id_t    msg_id = *(mvr_msg_id_t *)msg;

    vtss_sem_post((vtss_sem_t *)contxt);
    T_I("msg_id: %d->%s-UNLOCK", msg_id, mvr_msg_id_txt(msg_id));
}

static void mvr_msg_tx(mvr_msg_buf_t *buf, vtss_isid_t isid, size_t len)
{
    mvr_msg_id_t    msg_id = *(mvr_msg_id_t *)buf->msg;

    T_D("msg_id: %d->%s, len: %zd, isid: %d", msg_id, mvr_msg_id_txt(msg_id), len, isid);
    msg_tx_adv(buf->sem, mvr_msg_tx_done, MSG_TX_OPT_DONT_FREE, VTSS_MODULE_ID_MVR, isid, buf->msg, len);
}

static vtss_mvr_interface_t mvr_intf4msg;
static BOOL mvr_msg_rx(void *contxt, const void *const rx_msg, size_t len, vtss_module_id_t modid, u32 isid)
{
    mvr_msg_id_t      msg_id = *(mvr_msg_id_t *)rx_msg;
    int               i;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    T_D("msg_id: %d->%s, len: %zd, isid: %u", msg_id, mvr_msg_id_txt(msg_id), len, isid);

    switch (msg_id) {
    case MVR_MSG_ID_GLOBAL_SET_REQ: {
        mvr_msg_global_set_req_t    *msg;

        msg = (mvr_msg_global_set_req_t *)rx_msg;
        T_D("Receiving MVR_MSG_ID_GLOBAL_SET_REQ");

        if (msg->global_setting.mvr_state == VTSS_IPMCMVR_ENABLED) {
            BOOL                    next;
            vtss_mvr_interface_t    mvrif;

            memset(&mvrif, 0x0, sizeof(vtss_mvr_interface_t));
            MVR_CRIT_ENTER();
            next = (vtss_mvr_interface_get_next(&mvrif) == VTSS_OK);
            MVR_CRIT_EXIT();
            while (next) {
                if (_mvr_stacking_set_port_type(msg->isid, &mvrif) != TRUE) {
                    T_D("MVR set VLAN-%u tagging failed in isid-%d", mvrif.basic.param.vid, msg->isid);
                }

                MVR_CRIT_ENTER();
                next = (vtss_mvr_interface_get_next(&mvrif) == VTSS_OK);
                MVR_CRIT_EXIT();
            }

            MVR_CRIT_ENTER();
            mvr_running.mvr_conf.mvr_conf_global.mvr_state = TRUE;
            vtss_mvr_set_mode(TRUE);
            MVR_CRIT_EXIT();
        } else {
            _mvr_stacking_reset_port_type(msg->isid);

            MVR_CRIT_ENTER();
            mvr_running.mvr_conf.mvr_conf_global.mvr_state = FALSE;
            vtss_mvr_set_mode(FALSE);
            MVR_CRIT_EXIT();
        }
        mvr_sm_event_set(MVR_EVENT_PKT_HANDLER);

        break;
    }
    case MVR_MSG_ID_GLOBAL_PURGE_REQ: {
        mvr_msg_purge_req_t *msg;

        msg = (mvr_msg_purge_req_t *)rx_msg;
        T_D("Receiving MVR_MSG_ID_GLOBAL_PURGE_REQ");

        MVR_CRIT_ENTER();
        (void)vtss_mvr_purge(msg->isid, FALSE, FALSE);
        MVR_CRIT_EXIT();

        break;
    }
    case MVR_MSG_ID_VLAN_ENTRY_SET_REQ: {
        mvr_msg_vlan_set_req_t  *msg;
        vtss_mvr_interface_t    *mvrif;
        port_iter_t             pit;
        char                    mvrBufPort[MGMT_PORT_BUF_SIZE];
        mesa_port_list_t        mvrFwdMap;

        msg = (mvr_msg_vlan_set_req_t *)rx_msg;

        memset(mvrFwdMap, 0x0, sizeof(mvrFwdMap));
        if (msg_switch_is_master()) {
            (void) port_iter_init(&pit, NULL, msg->isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        } else {
            (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        }
        while (port_iter_getnext(&pit)) {
            if (msg->vlan_entry.role.ports[pit.iport] != MVR_PORT_ROLE_INACT) {
                mvrFwdMap[pit.iport] = TRUE;
            }
        }

        T_D("Receiving MVR_MSG_ID_VLAN_ENTRY_SET_REQ(%u) with Ports=%s",
            msg->vlan_entry.vid,
            mgmt_iport_list2txt(mvrFwdMap, mvrBufPort));

        MVR_CRIT_ENTER();
        mvrif = &mvr_intf4msg;
        _mvr_mgmt_transit_intf(VTSS_ISID_LOCAL, msg->isid, mvrif, &msg->vlan_entry, msg->version);
        if (vtss_mvr_interface_set(msg->action, mvrif) != VTSS_OK) {
            T_D("MVR_MSG_ID_VLAN_ENTRY_SET_REQ(%s-%u) Action:%d Failed!",
                ipmc_lib_version_txt(msg->version, IPMC_TXT_CASE_UPPER),
                mvrif->basic.param.vid,
                msg->action);
        }
        MVR_CRIT_EXIT();

        break;
    }
    case MVR_MSG_ID_VLAN_ENTRY_GET_REQ: {
        mvr_msg_vlan_entry_get_req_t    *msg;
        mvr_msg_vlan_entry_get_rep_t    *msg_rep;
        mvr_msg_buf_t                   buf;
        vtss_mvr_interface_t            *intf;
        ipmc_intf_entry_t               *entry;
        BOOL                            valid;

        msg = (mvr_msg_vlan_entry_get_req_t *)rx_msg;
        T_D("Receiving MVR_MSG_ID_VLAN_ENTRY_GET_REQ (VID:%d/VER:%d)", msg->vid, msg->version);

        MVR_CRIT_ENTER();
        intf = &mvr_intf4msg;
        memset(intf, 0x0, sizeof(vtss_mvr_interface_t));
        entry = &intf->basic;
        entry->ipmc_version =  msg->version;
        entry->param.vid = msg->vid;

        valid = vtss_mvr_interface_get(intf) == VTSS_OK;
        MVR_CRIT_EXIT();

        mvr_msg_alloc(MVR_MSG_ID_VLAN_ENTRY_GET_REP, &buf, FALSE, FALSE);
        msg_rep = (mvr_msg_vlan_entry_get_rep_t *)buf.msg;

        msg_rep->msg_id = MVR_MSG_ID_VLAN_ENTRY_GET_REP;
        if (valid) {
            memcpy(&msg_rep->interface_entry, &entry->param, sizeof(ipmc_prot_intf_entry_param_t));
        } else {
            msg_rep->interface_entry.vid = 0;
            T_D("no such vlan (%d)", msg->vid);
        }

        mvr_msg_tx(&buf, isid, sizeof(*msg_rep));

        break;
    }
    case MVR_MSG_ID_VLAN_ENTRY_GET_REP: {
        mvr_msg_vlan_entry_get_rep_t    *msg;

        msg = (mvr_msg_vlan_entry_get_rep_t *)rx_msg;
        T_D("Receiving MVR_MSG_ID_VLAN_ENTRY_GET_REP");

        MVR_CRIT_ENTER();
        memcpy(&mvr_running.interface_entry, &msg->interface_entry, sizeof(ipmc_prot_intf_entry_param_t));
        vtss_flag_setbits(&mvr_running.vlan_entry_flags, 1 << isid);
        MVR_CRIT_EXIT();

        break;
    }
    case MVR_MSG_ID_VLAN_GROUP_ENTRY_WALK_REQ: {
        mvr_msg_vlan_entry_get_req_t        *msg;
        mvr_msg_vlan_group_entry_get_rep_t  *msg_rep;
        mvr_msg_buf_t                       buf;
        ipmc_group_entry_t                  grp;
        BOOL                                valid;

        msg = (mvr_msg_vlan_entry_get_req_t *)rx_msg;
        T_D("Receiving MVR_MSG_ID_VLAN_GROUP_ENTRY_WALK_REQ");

        grp.ipmc_version = msg->version;
        grp.vid = msg->vid;
        memcpy(&grp.group_addr, &msg->group_addr, sizeof(mesa_ipv6_t));
        MVR_CRIT_ENTER();
        valid = vtss_mvr_intf_group_get_next(&grp);
        MVR_CRIT_EXIT();

        mvr_msg_alloc(MVR_MSG_ID_VLAN_GROUP_ENTRY_WALK_REP, &buf, FALSE, FALSE);
        msg_rep = (mvr_msg_vlan_group_entry_get_rep_t *)buf.msg;

        msg_rep->msg_id = MVR_MSG_ID_VLAN_GROUP_ENTRY_WALK_REP;
        msg_rep->intf_group_entry.valid = FALSE;
        if (valid) {
            msg_rep->intf_group_entry.ipmc_version = grp.ipmc_version;
            msg_rep->intf_group_entry.vid = grp.vid;
            memcpy(&msg_rep->intf_group_entry.group_addr, &grp.group_addr, sizeof(mesa_ipv6_t));
            memcpy(&msg_rep->intf_group_entry.db, &grp.info->db, sizeof(ipmc_group_db_t));

            if (!msg_switch_is_master()) {
                msg_rep->intf_group_entry.db.ipmc_sf_do_forward_srclist = NULL;
                msg_rep->intf_group_entry.db.ipmc_sf_do_not_forward_srclist = NULL;
                IPMC_FLTR_TIMER_DELTA_GET(&msg_rep->intf_group_entry.db);
            }

            msg_rep->intf_group_entry.valid = TRUE;
        } else {
            T_D("no more group entry in vlan-%d", msg->vid);
        }
        mvr_msg_tx(&buf, isid, sizeof(*msg_rep));

        break;
    }
    case MVR_MSG_ID_VLAN_GROUP_ENTRY_WALK_REP: {
        mvr_msg_vlan_group_entry_get_rep_t  *msg;

        msg = (mvr_msg_vlan_group_entry_get_rep_t *)rx_msg;
        T_D("Receiving MVR_MSG_ID_VLAN_GROUP_ENTRY_WALK_REP");

        MVR_CRIT_ENTER();
        memcpy(&mvr_running.intf_group_entry, &msg->intf_group_entry, sizeof(ipmc_prot_intf_group_entry_t));
        vtss_flag_setbits(&mvr_running.vlan_entry_flags, 1 << isid);
        MVR_CRIT_EXIT();

        break;
    }
    case MVR_MSG_ID_GROUP_ENTRY_GET_REQ:
    case MVR_MSG_ID_GROUP_ENTRY_GETNEXT_REQ: {
        mvr_msg_group_entry_itr_req_t   *msg;
        mvr_msg_group_entry_itr_rep_t   *msg_rep;
        mvr_msg_buf_t                   buf;
        ipmc_group_entry_t              grp;
        BOOL                            valid = FALSE;

        msg = (mvr_msg_group_entry_itr_req_t *)rx_msg;

        grp.ipmc_version = msg->version;
        grp.vid = msg->vid;
        IPMC_LIB_ADRS_CPY(&grp.group_addr, &msg->group_addr);
        if (msg_id == MVR_MSG_ID_GROUP_ENTRY_GET_REQ) {
            mvr_msg_alloc(MVR_MSG_ID_GROUP_ENTRY_GET_REP, &buf, FALSE, FALSE);
        } else {
            mvr_msg_alloc(MVR_MSG_ID_GROUP_ENTRY_GETNEXT_REP, &buf, FALSE, FALSE);
        }

        MVR_CRIT_ENTER();
        if (msg_id == MVR_MSG_ID_GROUP_ENTRY_GET_REQ) {
            if (vtss_mvr_group_entry_get(&msg->version, &msg->vid, &msg->group_addr, &grp) == VTSS_RC_OK) {
                valid = TRUE;
            }
        } else {
            if (vtss_mvr_group_entry_get_next(&msg->version, &msg->vid, &msg->group_addr, &grp) == VTSS_RC_OK) {
                if (grp.ipmc_version == msg->version) {
                    valid = TRUE;
                }
            }
        }
        MVR_CRIT_EXIT();

        msg_rep = (mvr_msg_group_entry_itr_rep_t *)buf.msg;
        if (msg_id == MVR_MSG_ID_GROUP_ENTRY_GET_REQ) {
            msg_rep->msg_id = MVR_MSG_ID_GROUP_ENTRY_GET_REP;
        } else {
            msg_rep->msg_id = MVR_MSG_ID_GROUP_ENTRY_GETNEXT_REP;
        }

        msg_rep->group_entry.valid = valid;
        if (valid) {
            msg_rep->group_entry.ipmc_version = grp.ipmc_version;
            msg_rep->group_entry.vid = grp.vid;
            IPMC_LIB_ADRS_CPY(&msg_rep->group_entry.group_addr, &grp.group_addr);
            memcpy(&msg_rep->group_entry.db, &grp.info->db, sizeof(ipmc_group_db_t));

            if (!msg_switch_is_master()) {
                msg_rep->group_entry.db.ipmc_sf_do_forward_srclist = NULL;
                msg_rep->group_entry.db.ipmc_sf_do_not_forward_srclist = NULL;
                IPMC_FLTR_TIMER_DELTA_GET(&msg_rep->group_entry.db);
            }
        } else {
            T_D("no more group entry in vlan-%u", msg->vid);
        }
        mvr_msg_tx(&buf, isid, sizeof(*msg_rep));

        break;
    }
    case MVR_MSG_ID_GROUP_ENTRY_GET_REP:
    case MVR_MSG_ID_GROUP_ENTRY_GETNEXT_REP: {
        mvr_msg_group_entry_itr_rep_t   *msg;

        msg = (mvr_msg_group_entry_itr_rep_t *)rx_msg;

        MVR_CRIT_ENTER();
        if (msg_id == MVR_MSG_ID_GROUP_ENTRY_GET_REP) {
            memcpy(&mvr_running.group_entry, &msg->group_entry, sizeof(ipmc_prot_intf_group_entry_t));
            vtss_flag_setbits(&mvr_running.grp_entry_flags, 1 << isid);
        } else {
            memcpy(&mvr_running.group_next_entry, &msg->group_entry, sizeof(ipmc_prot_intf_group_entry_t));
            vtss_flag_setbits(&mvr_running.grp_nxt_entry_flags, 1 << isid);
        }
        MVR_CRIT_EXIT();

        break;
    }
    case MVR_MSG_ID_GROUP_SRCLIST_WALK_REQ: {
        mvr_msg_vlan_entry_get_req_t    *msg;
        mvr_msg_group_srclist_get_rep_t *msg_rep;
        mvr_msg_buf_t                   buf;
        ipmc_group_entry_t              grp;
        BOOL                            valid;

        msg = (mvr_msg_vlan_entry_get_req_t *)rx_msg;
        T_D("Receiving MVR_MSG_ID_GROUP_SRCLIST_WALK_REQ");

        grp.ipmc_version = msg->version;
        grp.vid = msg->vid;
        memcpy(&grp.group_addr, &msg->group_addr, sizeof(mesa_ipv6_t));
        MVR_CRIT_ENTER();
        valid = vtss_mvr_intf_group_get(&grp);
        MVR_CRIT_EXIT();

        mvr_msg_alloc(MVR_MSG_ID_GROUP_SRCLIST_WALK_REP, &buf, FALSE, FALSE);
        msg_rep = (mvr_msg_group_srclist_get_rep_t *)buf.msg;

        msg_rep->msg_id = MVR_MSG_ID_GROUP_SRCLIST_WALK_REP;
        msg_rep->group_srclist_entry.valid = FALSE;
        if (valid) {
            ipmc_db_ctrl_hdr_t  *srclist;

            if (msg->srclist_type) {
                srclist = grp.info->db.ipmc_sf_do_forward_srclist;
            } else {
                srclist = grp.info->db.ipmc_sf_do_not_forward_srclist;
            }

            if (srclist &&
                ((msg_rep->group_srclist_entry.cntr = IPMC_LIB_DB_GET_COUNT(srclist)) != 0)) {
                ipmc_sfm_srclist_t  entry;

                memcpy(&entry.src_ip, &msg->srclist_addr, sizeof(mesa_ipv6_t));
                if (ipmc_lib_srclist_buf_get_next(srclist, &entry)) {
                    msg_rep->group_srclist_entry.type = msg->srclist_type;
                    memcpy(&msg_rep->group_srclist_entry.srclist, &entry, sizeof(ipmc_sfm_srclist_t));
                    IPMC_SRCT_TIMER_DELTA_GET(&msg_rep->group_srclist_entry.srclist);

                    msg_rep->group_srclist_entry.valid = TRUE;
                }
            }
        }
        mvr_msg_tx(&buf, isid, sizeof(*msg_rep));

        break;
    }
    case MVR_MSG_ID_GROUP_SRCLIST_WALK_REP: {
        mvr_msg_group_srclist_get_rep_t *msg;

        msg = (mvr_msg_group_srclist_get_rep_t *)rx_msg;
        T_D("Receiving MVR_MSG_ID_GROUP_SRCLIST_WALK_REP");

        MVR_CRIT_ENTER();
        memcpy(&mvr_running.group_srclist_entry, &msg->group_srclist_entry, sizeof(ipmc_prot_group_srclist_t));
        vtss_flag_setbits(&mvr_running.vlan_entry_flags, 1 << isid);
        MVR_CRIT_EXIT();

        break;
    }
    case MVR_MSG_ID_PORT_FAST_LEAVE_SET_REQ: {
        mvr_msg_port_fast_leave_set_req_t   *msg;
        port_iter_t                         pit;
        ipmc_port_bfs_t                     fast_leave_mask;

        msg = (mvr_msg_port_fast_leave_set_req_t *)rx_msg;
        T_D("Receiving MVR_MSG_ID_PORT_FAST_LEAVE_SET_REQ");

        VTSS_PORT_BF_CLR(fast_leave_mask.member_ports);
        if (msg_switch_is_master()) {
            (void) port_iter_init(&pit, NULL, msg->isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        } else {
            (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        }
        while (port_iter_getnext(&pit)) {
            i = pit.iport;

            VTSS_PORT_BF_SET(fast_leave_mask.member_ports,
                             i,
                             VTSS_PORT_BF_GET(msg->fast_leave.ports, i));
        }

        MVR_CRIT_ENTER();
        vtss_mvr_set_fast_leave_ports(&fast_leave_mask);
        MVR_CRIT_EXIT();

        break;
    }
    case MVR_MSG_ID_STAT_COUNTER_CLEAR_REQ: {
        mvr_msg_stat_counter_clear_req_t    *msg;

        msg = (mvr_msg_stat_counter_clear_req_t *)rx_msg;
        T_D("Receiving MVR_MSG_ID_STAT_COUNTER_CLEAR_REQ");

        MVR_CRIT_ENTER();
        vtss_mvr_clear_stat_counter(msg->vid);
        MVR_CRIT_EXIT();

        break;
    }
    case MVR_MSG_ID_STP_PORT_CHANGE_REQ: {
        mvr_msg_stp_port_change_set_req_t   *msg;
        ipmc_port_bfs_t                     router_port_mask;

        msg = (mvr_msg_stp_port_change_set_req_t *) rx_msg;
        T_D("Receiving MVR_MSG_ID_STP_PORT_CHANGE_REQ");

        VTSS_PORT_BF_CLR(router_port_mask.member_ports);

        MVR_CRIT_ENTER();
        /* We should filter sendout process if this port belongs to dynamic router port. */
        ipmc_lib_get_discovered_router_port_mask(IPMC_IP_VERSION_ALL, &router_port_mask);
        if (!VTSS_PORT_BF_GET(router_port_mask.member_ports, msg->port)) {
            vtss_mvr_stp_port_state_change_handle(IPMC_IP_VERSION_ALL, msg->port, msg->new_state);
        }
        MVR_CRIT_EXIT();

        break;
    }
    default:
        T_E("unknown message ID: %d", msg_id);

        break;
    }

    T_D("mvr_msg_rx(%u) consumes ID%u:%u ticks", (u32)msg_id, isid, (u32)(vtss_current_time() - exe_time_base));

    return TRUE;
}

static mesa_rc mvr_stacking_register(void)
{
    msg_rx_filter_t filter;

    memset(&filter, 0, sizeof(filter));
    filter.cb = mvr_msg_rx;
    filter.modid = VTSS_MODULE_ID_MVR;
    return msg_rx_filter_register(&filter);
}

/* Set STACK IPMC MODE */
static mesa_rc mvr_stacking_set_mode(vtss_isid_t isid_add)
{
    mvr_msg_buf_t               buf;
    mvr_msg_global_set_req_t    *msg;
    switch_iter_t               sit;
    vtss_isid_t                 isid;
    vtss_mvr_interface_t        mvrif;
    BOOL                        next, apply_mode;

    if (!msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    T_D("enter, isid: %d", isid_add);

    MVR_CRIT_ENTER();
    apply_mode = mvr_running.mvr_conf.mvr_conf_global.mvr_state;
    MVR_CRIT_EXIT();

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if ((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) {
            continue;
        }

        if (ipmc_lib_isid_is_local(isid)) {
            if (apply_mode == VTSS_IPMCMVR_ENABLED) {
                memset(&mvrif, 0x0, sizeof(vtss_mvr_interface_t));
                MVR_CRIT_ENTER();
                next = (vtss_mvr_interface_get_next(&mvrif) == VTSS_OK);
                MVR_CRIT_EXIT();
                while (next) {
                    if (_mvr_stacking_set_port_type(isid, &mvrif) != TRUE) {
                        T_D("MVR set VLAN-%u tagging failed in isid-%d", mvrif.basic.param.vid, isid);
                    }

                    MVR_CRIT_ENTER();
                    next = (vtss_mvr_interface_get_next(&mvrif) == VTSS_OK);
                    MVR_CRIT_EXIT();
                }

                MVR_CRIT_ENTER();
                vtss_mvr_set_mode(TRUE);
                MVR_CRIT_EXIT();
            } else {
                _mvr_stacking_reset_port_type(isid);

                MVR_CRIT_ENTER();
                vtss_mvr_set_mode(FALSE);
                MVR_CRIT_EXIT();
            }
            mvr_sm_event_set(MVR_EVENT_PKT_HANDLER);

            continue;
        } else {
            if (apply_mode == VTSS_IPMCMVR_ENABLED) {
                memset(&mvrif, 0x0, sizeof(vtss_mvr_interface_t));
                MVR_CRIT_ENTER();
                next = (vtss_mvr_interface_get_next(&mvrif) == VTSS_OK);
                MVR_CRIT_EXIT();
                while (next) {
                    if (_mvr_stacking_set_port_type(isid, &mvrif) != TRUE) {
                        T_D("MVR set VLAN-%u tagging failed in isid-%d", mvrif.basic.param.vid, isid);
                    }

                    MVR_CRIT_ENTER();
                    next = (vtss_mvr_interface_get_next(&mvrif) == VTSS_OK);
                    MVR_CRIT_EXIT();
                }
            } else {
                _mvr_stacking_reset_port_type(isid);
            }
        }

        mvr_msg_alloc(MVR_MSG_ID_GLOBAL_SET_REQ, &buf, FALSE, TRUE);
        msg = (mvr_msg_global_set_req_t *)buf.msg;

        msg->global_setting.mvr_state = apply_mode;
        msg->isid = isid;
        msg->msg_id = MVR_MSG_ID_GLOBAL_SET_REQ;
        mvr_msg_tx(&buf, isid, sizeof(*msg));
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

/* Set STACK IPMC FAST LEAVE PORT */
static mesa_rc mvr_stacking_set_fastleave_port(vtss_isid_t isid_add)
{
    mvr_msg_buf_t                       buf;
    mvr_msg_port_fast_leave_set_req_t   *msg;
    switch_iter_t                       sit;
    vtss_isid_t                         isid;
    port_iter_t                         pit;
    mvr_conf_fast_leave_t               *fast_leave;
    ipmc_port_bfs_t                     fastleave_setting;
    u32                                 i, temp_port_mask;

    if (!msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    T_D("enter, isid: %d", isid_add);

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if ((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) {
            continue;
        }

        if (ipmc_lib_isid_is_local(isid)) {
            VTSS_PORT_BF_CLR(fastleave_setting.member_ports);
            MVR_CRIT_ENTER();
            fast_leave = &mvr_running.mvr_conf.mvr_conf_fast_leave[ipmc_lib_isid_convert(TRUE, isid)];
            (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
            while (port_iter_getnext(&pit)) {
                i = pit.iport;

                VTSS_PORT_BF_SET(fastleave_setting.member_ports,
                                 i,
                                 VTSS_PORT_BF_GET(fast_leave->ports, i));
            }

            vtss_mvr_set_fast_leave_ports(&fastleave_setting);
            MVR_CRIT_EXIT();

            continue;
        }

        mvr_msg_alloc(MVR_MSG_ID_PORT_FAST_LEAVE_SET_REQ, &buf, FALSE, TRUE);
        msg = (mvr_msg_port_fast_leave_set_req_t *)buf.msg;

        MVR_CRIT_ENTER();
        memcpy(&msg->fast_leave,
               &mvr_running.mvr_conf.mvr_conf_fast_leave[ipmc_lib_isid_convert(TRUE, isid)],
               sizeof(mvr_conf_fast_leave_t));
        MVR_CRIT_EXIT();

        temp_port_mask = 0;
        (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        while (port_iter_getnext(&pit)) {
            i = pit.iport;

            if (VTSS_PORT_BF_GET(msg->fast_leave.ports, i)) {
                temp_port_mask |= (1 << i);
            }
        }

        msg->msg_id = MVR_MSG_ID_PORT_FAST_LEAVE_SET_REQ;
        msg->isid = isid;
        mvr_msg_tx(&buf, isid, sizeof(*msg));

        T_D("(TX) (ISID = %d)temp_port_mask = 0x%x", isid_add, temp_port_mask);
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

/* Send request and wait for response */
static vtss_flag_value_t mvr_stacking_send_req(const vtss_isid_t       isid,
                                               const ipmc_ip_version_t ipmc_version,
                                               const mesa_vid_t        vid,
                                               const mesa_ipv6_t       *const group_addr,
                                               const mesa_ipv6_t       *const srclist,
                                               const mvr_msg_id_t      msg_id,
                                               const BOOL              type)
{
    vtss_flag_t       *flags;
    mvr_msg_buf_t     buf;
    vtss_flag_value_t flag, retVal;
    vtss_tick_count_t time_wait;

    T_D("enter(isid: %d)", isid);
    retVal = 0;
    flag = (1 << isid);
    mvr_msg_alloc(msg_id, &buf, TRUE, TRUE);

    if (msg_id == MVR_MSG_ID_GROUP_ENTRY_GET_REQ ||
        msg_id == MVR_MSG_ID_GROUP_ENTRY_GETNEXT_REQ) {
        mvr_msg_group_entry_itr_req_t   *msg_grpx = (mvr_msg_group_entry_itr_req_t *)buf.msg;

        msg_grpx->msg_id = msg_id;
        msg_grpx->version = ipmc_version;
        msg_grpx->vid = vid;
        if (group_addr) {
            memcpy(&msg_grpx->group_addr, group_addr, sizeof(mesa_ipv6_t));
        }

        MVR_GET_CRIT_ENTER();
        if (msg_id == MVR_MSG_ID_GROUP_ENTRY_GETNEXT_REQ) {
            flags = &mvr_running.grp_nxt_entry_flags;
        } else {
            flags = &mvr_running.grp_entry_flags;
        }
        vtss_flag_maskbits(flags, ~flag);
        MVR_GET_CRIT_EXIT();

        mvr_msg_tx(&buf, isid, sizeof(*msg_grpx));
    } else {
        mvr_msg_vlan_entry_get_req_t    *msg_vlan = (mvr_msg_vlan_entry_get_req_t *)buf.msg;

        msg_vlan->msg_id = msg_id;
        msg_vlan->version = ipmc_version;
        /* Default: MVR_MSG_ID_VLAN_ENTRY_GET_REQ */
        msg_vlan->vid = vid;
        if (msg_id == MVR_MSG_ID_VLAN_GROUP_ENTRY_WALK_REQ) {
            if (group_addr) {
                memcpy(&msg_vlan->group_addr, group_addr, sizeof(mesa_ipv6_t));
            }
        }
        if (msg_id == MVR_MSG_ID_GROUP_SRCLIST_WALK_REQ) {
            if (group_addr) {
                memcpy(&msg_vlan->group_addr, group_addr, sizeof(mesa_ipv6_t));
            }
            if (srclist) {
                memcpy(&msg_vlan->srclist_addr, srclist, sizeof(mesa_ipv6_t));
            }
            msg_vlan->srclist_type = type;
        }

        MVR_GET_CRIT_ENTER();
        flags = &mvr_running.vlan_entry_flags;
        vtss_flag_maskbits(flags, ~flag);
        MVR_GET_CRIT_EXIT();

        mvr_msg_tx(&buf, isid, sizeof(*msg_vlan));
    }

    time_wait = vtss_current_time() + VTSS_OS_MSEC2TICK(MVR_REQ_TIMEOUT);
    MVR_GET_CRIT_ENTER();
    if (!(vtss_flag_timed_wait(flags, flag, VTSS_FLAG_WAITMODE_OR, time_wait) & flag)) {
        retVal = 1;
    }
    MVR_GET_CRIT_EXIT();

    return retVal;
}

static mesa_rc mvr_stacking_set_intf_entry(vtss_isid_t isid,
                                           ipmc_operation_action_t action,
                                           vtss_mvr_interface_t *entry,
                                           ipmc_ip_version_t ipmc_version)
{
    mvr_msg_buf_t                buf;
    mvr_msg_vlan_set_req_t       *msg;
    vtss_appl_vlan_entry_t       vlan_entry;
    port_iter_t                  pit;
    char                         mvrBufPort[MGMT_PORT_BUF_SIZE];
    mesa_port_list_t             mvrFwdMap;
    u32                          idx;
    ipmc_intf_entry_t            *intf;
    ipmc_prot_intf_entry_param_t *param;

    if (!msg_switch_is_master() || !entry) {
        return VTSS_RC_ERROR;
    }

    T_D("enter, isid: %d", isid);

    intf = &entry->basic;
    param = &intf->param;
    intf->ipmc_version = ipmc_version;

    if (action == IPMC_OP_DEL) {
        memset(&vlan_entry, 0x0, sizeof(vtss_appl_vlan_entry_t));
        if (vtss_appl_vlan_get(isid, param->vid, &vlan_entry, FALSE, VTSS_APPL_VLAN_USER_ALL) == VTSS_OK) {
            if (vlan_mgmt_vlan_del(isid, param->vid, MVR_VLAN_TYPE) != VTSS_OK) {
                T_D("MVR delete VLAN-%u failed in isid-%d", param->vid, isid);
            } else {
                /* Refine specific MVR VLAN Tagging */
                if (_mvr_stacking_refine_port_type(isid) != TRUE) {
                    T_D("MVR refine VLAN-%u tagging failed in isid-%d", param->vid, isid);
                }
            }
        }
    } else {
        memset(&vlan_entry, 0x0, sizeof(vtss_appl_vlan_entry_t));
        vlan_entry.vid = param->vid;
        (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
        while (port_iter_getnext(&pit)) {
            idx = pit.iport;

            if (entry->ports[idx] != MVR_PORT_ROLE_INACT) {
                vlan_entry.ports[idx] = 1;
            }
        }

        if (vlan_mgmt_vlan_add(isid, &vlan_entry, MVR_VLAN_TYPE) != VTSS_OK) {
            T_D("MVR add VLAN-%u failed in isid-%d", param->vid, isid);
        } else {
            /* Set MVR VLAN Tagging */
            if (_mvr_stacking_set_port_type(isid, entry) != TRUE) {
                T_D("MVR set VLAN-%u tagging failed in isid-%d", param->vid, isid);
            }
        }
    }

    if (ipmc_lib_isid_is_local(isid)) {
        MVR_CRIT_ENTER();
        if (vtss_mvr_interface_set(action, entry) != VTSS_OK) {
            T_D("MVR_MSG_ID_VLAN_ENTRY_SET_REQ Failed!");
        }
        MVR_CRIT_EXIT();
    } else {
        mvr_mgmt_interface_t    *mvrif;
        mvr_conf_intf_entry_t   *p;
        mvr_conf_port_role_t    *q;

        T_D("MVR_MSG_ID_VLAN_ENTRY_SET_REQ SndTo(%d) with VID:%u", isid, param->vid);

        mvr_msg_alloc(MVR_MSG_ID_VLAN_ENTRY_SET_REQ, &buf, FALSE, TRUE);
        msg = (mvr_msg_vlan_set_req_t *)buf.msg;

        msg->msg_id = MVR_MSG_ID_VLAN_ENTRY_SET_REQ;
        msg->isid = isid;
        msg->action = action;
        msg->version = ipmc_version;
        mvrif = &msg->vlan_entry;
        mvrif->vid = param->vid;

        p = &mvrif->intf;
        q = &mvrif->role;
        p->vid = q->vid = mvrif->vid;

        p->valid = TRUE;
        memcpy(p->name, entry->name, MVR_NAME_MAX_LEN);
        p->name[MVR_NAME_MAX_LEN - 1] = 0;
        p->mode = entry->mode;
        p->vtag = entry->vtag;
        p->querier_status = param->querier.querier_enabled;
        p->querier4_address = param->querier.QuerierAdrs4;
        p->priority = entry->priority;
        p->profile_index = entry->profile_index;

        p->compatibility = param->cfg_compatibility;
        p->robustness_variable = param->querier.RobustVari;
        p->query_interval = param->querier.QueryIntvl;
        p->query_response_interval = param->querier.MaxResTime;
        p->last_listener_query_interval = param->querier.LastQryItv;
        p->unsolicited_report_interval = param->querier.UnsolicitR;

        memset(mvrFwdMap, 0x0, sizeof(mvrFwdMap));
        (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        while (port_iter_getnext(&pit)) {
            idx = pit.iport;

            q->ports[idx] = entry->ports[idx];
            if (q->ports[idx] != MVR_PORT_ROLE_INACT) {
                mvrFwdMap[idx] = TRUE;
            }
        }
        q->valid = TRUE;

        T_D("SendTo ISID-%d with VID=%u Ports=%s",
            isid, q->vid,
            mgmt_iport_list2txt(mvrFwdMap, mvrBufPort));

        mvr_msg_tx(&buf, isid, sizeof(*msg));
    }

    T_D("exit, isid: %d", isid);

    return VTSS_OK;
}

static vtss_mvr_interface_t mvr_intf4get;
static mesa_rc
mvr_stacking_get_intf_entry(
    const vtss_isid_t               isid_add,
    const ipmc_ip_version_t         ipmc_version,
    ipmc_prot_intf_entry_param_t    *const intf_param
)
{
    mesa_rc rc = VTSS_RC_ERROR;

    if (!msg_switch_is_master() || !intf_param) {
        T_D("Invalid Input!");
        return rc;
    }

    T_D("enter->isid:%u/ver:%d/vid:%u", isid_add, ipmc_version, intf_param->vid);

    if (isid_add == VTSS_ISID_GLOBAL ||
        isid_add == VTSS_ISID_LOCAL ||
        ipmc_lib_isid_is_local(isid_add)) {
        vtss_mvr_interface_t    *entry;
        ipmc_intf_entry_t       *intf;

        MVR_GET_CRIT_ENTER();
        entry = &mvr_intf4get;
        memset(entry, 0x0, sizeof(vtss_mvr_interface_t));
        intf = &entry->basic;
        intf->ipmc_version = ipmc_version;
        intf->param.vid = intf_param->vid;

        if ((rc = vtss_mvr_interface_get(entry)) == VTSS_RC_OK) {
            memcpy(intf_param, &intf->param, sizeof(ipmc_prot_intf_entry_param_t));
        } else {
            intf_param->vid = 0;
        }
        MVR_GET_CRIT_EXIT();
    } else {
        if (IPMC_LIB_ISID_EXIST(isid_add)) {
            mesa_ipv6_t zero_addr;

            MVR_GET_CRIT_ENTER();
            ipmc_lib_get_all_zero_ipv6_addr(&zero_addr);
            memset(&mvr_running.interface_entry, 0x0, sizeof(ipmc_prot_intf_entry_param_t));
            MVR_GET_CRIT_EXIT();

            if (mvr_stacking_send_req(isid_add, ipmc_version,
                                      intf_param->vid, &zero_addr, NULL,
                                      MVR_MSG_ID_VLAN_ENTRY_GET_REQ,
                                      FALSE)) {
                T_D("timeout, MVR_MSG_ID_VLAN_ENTRY_GET_REQ(isid=%d)", isid_add);
            } else {
                MVR_GET_CRIT_ENTER();
                memcpy(intf_param, &mvr_running.interface_entry, sizeof(ipmc_prot_intf_entry_param_t));
                MVR_GET_CRIT_EXIT();

                rc = (intf_param->vid != 0) ? VTSS_RC_OK : VTSS_RC_ERROR;
            }
        }
    }

    T_D("exit(%s)->isid:%u/vid:%u", rc == VTSS_RC_OK ? "OK" : "NG", isid_add, intf_param->vid);
    return rc;
}

static mesa_rc
mvr_stacking_get_next_group_srclist_entry(
    const vtss_isid_t               isid_add,
    const ipmc_ip_version_t         ipmc_version,
    const mesa_vid_t                vid,
    const mesa_ipv6_t               *const addr,
    ipmc_prot_group_srclist_t       *const srclist
)
{
    char    buf1[40], buf2[40];
    mesa_rc rc = VTSS_RC_ERROR;

    if (!msg_switch_is_master() || !addr || !srclist) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    memset(buf1, 0x0, sizeof(buf1));
    memset(buf2, 0x0, sizeof(buf2));
    T_D("enter->isid:%u/ver:%d/vid:%u/grp=%s/src=[%s]%s",
        isid_add,
        ipmc_version,
        vid,
        misc_ipv6_txt(addr, buf1),
        srclist->type ? "ALLOW" : "BLOCK",
        misc_ipv6_txt(&srclist->srclist.src_ip, buf2));

    if (isid_add == VTSS_ISID_GLOBAL ||
        isid_add == VTSS_ISID_LOCAL ||
        ipmc_lib_isid_is_local(isid_add)) {
        ipmc_group_entry_t  grp;
        BOOL                valid;

        grp.ipmc_version = ipmc_version;
        grp.vid = vid;
        memcpy(&grp.group_addr, addr, sizeof(mesa_ipv6_t));
        MVR_GET_CRIT_ENTER();
        valid = vtss_mvr_intf_group_get(&grp);
        MVR_GET_CRIT_EXIT();

        srclist->valid = FALSE;
        if (valid) {
            ipmc_db_ctrl_hdr_t  *slist;

            if (srclist->type) {
                slist = grp.info->db.ipmc_sf_do_forward_srclist;
            } else {
                slist = grp.info->db.ipmc_sf_do_not_forward_srclist;
            }

            if (slist &&
                ((srclist->cntr = IPMC_LIB_DB_GET_COUNT(slist)) != 0)) {
                ipmc_sfm_srclist_t  entry;

                memcpy(&entry.src_ip, &srclist->srclist.src_ip, sizeof(mesa_ipv6_t));
                if (ipmc_lib_srclist_buf_get_next(slist, &entry)) {
                    /* srclist->type remains the same */
                    memcpy(&srclist->srclist, &entry, sizeof(ipmc_sfm_srclist_t));
                    srclist->valid = TRUE;

                    rc = VTSS_RC_OK;
                }
            }
        }
    } else {
        if (IPMC_LIB_ISID_EXIST(isid_add)) {
            MVR_GET_CRIT_ENTER();
            memset(&mvr_running.group_srclist_entry, 0x0, sizeof(ipmc_prot_group_srclist_t));
            MVR_GET_CRIT_EXIT();

            if (mvr_stacking_send_req(isid_add, ipmc_version,
                                      vid, addr, &srclist->srclist.src_ip,
                                      MVR_MSG_ID_GROUP_SRCLIST_WALK_REQ,
                                      srclist->type)) {
                T_D("timeout, MVR_MSG_ID_GROUP_SRCLIST_WALK_REQ(isid=%d)", isid_add);
            } else {
                MVR_GET_CRIT_ENTER();
                memcpy(srclist, &mvr_running.group_srclist_entry, sizeof(ipmc_prot_group_srclist_t));
                MVR_GET_CRIT_EXIT();

                rc = srclist->valid ? VTSS_RC_OK : VTSS_RC_ERROR;
            }
        }
    }

    memset(buf1, 0x0, sizeof(buf1));
    memset(buf2, 0x0, sizeof(buf2));
    T_D("exit(%s)->isid:%u/ver:%d/vid:%u/grp=%s/src=[%s]%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        isid_add,
        ipmc_version,
        vid,
        misc_ipv6_txt(addr, buf1),
        srclist->type ? "ALLOW" : "BLOCK",
        misc_ipv6_txt(&srclist->srclist.src_ip, buf2));
    return rc;
}

static mesa_rc
mvr_stacking_get_next_intf_group_entry(
    const vtss_isid_t               isid_add,
    const ipmc_ip_version_t         ipmc_version,
    const mesa_vid_t                vid,
    ipmc_prot_intf_group_entry_t    *const intf_group
)
{
    char    buf[40];
    mesa_rc rc = VTSS_RC_ERROR;

    if (!msg_switch_is_master() || !intf_group) {
        T_D("Invalid Input!");
        return rc;
    }

    memset(buf, 0x0, sizeof(buf));
    T_D("enter->isid:%u/ver:%d/vid:%u/grp=%s",
        isid_add,
        ipmc_version,
        vid,
        misc_ipv6_txt(&intf_group->group_addr, buf));

    if (isid_add == VTSS_ISID_GLOBAL ||
        isid_add == VTSS_ISID_LOCAL ||
        ipmc_lib_isid_is_local(isid_add)) {
        ipmc_group_entry_t  grp;

        grp.ipmc_version = ipmc_version;
        grp.vid = vid;
        memcpy(&grp.group_addr, &intf_group->group_addr, sizeof(mesa_ipv6_t));
        intf_group->valid = FALSE;
        MVR_GET_CRIT_ENTER();
        intf_group->valid = vtss_mvr_intf_group_get_next(&grp);
        MVR_GET_CRIT_EXIT();

        if (intf_group->valid) {
            intf_group->ipmc_version = grp.ipmc_version;
            intf_group->vid = grp.vid;
            memcpy(&intf_group->group_addr, &grp.group_addr, sizeof(mesa_ipv6_t));
            memcpy(&intf_group->db, &grp.info->db, sizeof(ipmc_group_db_t));

            rc = VTSS_RC_OK;
        }
    } else {
        if (IPMC_LIB_ISID_EXIST(isid_add)) {
            MVR_GET_CRIT_ENTER();
            memset(&mvr_running.intf_group_entry, 0x0, sizeof(ipmc_prot_intf_group_entry_t));
            MVR_GET_CRIT_EXIT();

            if (mvr_stacking_send_req(isid_add, ipmc_version,
                                      vid, &intf_group->group_addr, NULL,
                                      MVR_MSG_ID_VLAN_GROUP_ENTRY_WALK_REQ,
                                      FALSE)) {
                T_D("timeout, MVR_MSG_ID_VLAN_GROUP_ENTRY_WALK_REQ(isid=%d)", isid_add);
            } else {
                MVR_GET_CRIT_ENTER();
                memcpy(intf_group, &mvr_running.intf_group_entry, sizeof(ipmc_prot_intf_group_entry_t));
                MVR_GET_CRIT_EXIT();

                rc = intf_group->valid ? VTSS_RC_OK : VTSS_RC_ERROR;
            }
        }
    }

    memset(buf, 0x0, sizeof(buf));
    T_D("exit(%s)->isid:%u/vid:%d/group_address=%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        isid_add,
        vid,
        misc_ipv6_txt(&intf_group->group_addr, buf));
    return rc;
}

static BOOL mvr_util_ipv6_address_zero(const mesa_ipv6_t *const adrx)
{
    mesa_ipv6_t adrs;

    if (!adrx) {
        return FALSE;
    }

    IPMC_LIB_ADRS_CPY(&adrs, adrx);
    return ipmc_lib_isaddr6_all_zero(&adrs);
}

static mesa_rc mvr_stacking_group_entry_get(const vtss_isid_t            sidx,
                                            const ipmc_ip_version_t      verx,
                                            const mesa_vid_t             *const vidx,
                                            const mesa_ipv6_t            *const grpx,
                                            ipmc_prot_intf_group_entry_t *const entry)
{
    char              buf[40];
    vtss_tick_count_t exe_time_base;
    ipmc_ip_version_t version;
    mesa_rc           rc = VTSS_RC_ERROR;

    if (!msg_switch_is_master() ||
        !vidx || !grpx || !entry) {
        T_D("Invalid Input!");
        return rc;
    }
    exe_time_base = vtss_current_time();

    memset(buf, 0x0, sizeof(buf));
    T_D("enter->isid:%d/ver:%d/vid:%u/grp=%s",
        sidx,
        verx,
        *vidx,
        misc_ipv6_txt(grpx, buf));

    version = verx;
    if ((sidx == VTSS_ISID_GLOBAL) ||
        (sidx == VTSS_ISID_LOCAL) ||
        ipmc_lib_isid_is_local(sidx)) {
        ipmc_group_entry_t  grp;

        entry->valid = FALSE;
        grp.ipmc_version = version;
        grp.vid = *vidx;
        IPMC_LIB_ADRS_CPY(&grp.group_addr, grpx);
        MVR_GET_CRIT_ENTER();
        if (vtss_mvr_group_entry_get(&version, vidx, grpx, &grp) == VTSS_RC_OK) {
            entry->valid = TRUE;
        }
        MVR_GET_CRIT_EXIT();

        if (entry->valid) {
            entry->ipmc_version = grp.ipmc_version;
            entry->vid = grp.vid;
            IPMC_LIB_ADRS_CPY(&entry->group_addr, &grp.group_addr);
            memcpy(&entry->db, &grp.info->db, sizeof(ipmc_group_db_t));

            if (!msg_switch_is_master()) {
                entry->db.ipmc_sf_do_forward_srclist = NULL;
                entry->db.ipmc_sf_do_not_forward_srclist = NULL;
            }

            rc = VTSS_RC_OK;
        }
    } else {
        if (IPMC_LIB_ISID_EXIST(sidx)) {
            MVR_GET_CRIT_ENTER();
            memset(&mvr_running.group_entry, 0x0, sizeof(ipmc_prot_intf_group_entry_t));
            MVR_GET_CRIT_EXIT();

            if (mvr_stacking_send_req(sidx, version, *vidx,
                                      grpx, NULL,
                                      MVR_MSG_ID_GROUP_ENTRY_GET_REQ,
                                      FALSE)) {
                T_D("timeout, MVR_MSG_ID_GROUP_ENTRY_GET_REQ(isid=%d)", sidx);
            } else {
                MVR_GET_CRIT_ENTER();
                memcpy(entry, &mvr_running.group_entry, sizeof(ipmc_prot_intf_group_entry_t));
                MVR_GET_CRIT_EXIT();

                rc = entry->valid ? VTSS_RC_OK : VTSS_RC_ERROR;
            }
        }
    }

    memset(buf, 0x0, sizeof(buf));
    T_D("exit(%s:%u-Ticks-GOT)->isid:%d/vid:%u/grp=%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        (u32)(vtss_current_time() - exe_time_base),
        sidx,
        entry->vid,
        misc_ipv6_txt(&entry->group_addr, buf));
    return rc;
}

static mesa_rc mvr_stacking_group_entry_get_next(const vtss_isid_t            sidx,
                                                 const ipmc_ip_version_t      verx,
                                                 const mesa_vid_t             *const vidx,
                                                 const mesa_ipv6_t            *const grpx,
                                                 ipmc_prot_intf_group_entry_t *const entry)
{
    char              buf[40];
    vtss_tick_count_t exe_time_base;
    ipmc_ip_version_t version;
    mesa_rc           rc = VTSS_RC_ERROR;

    if (!msg_switch_is_master() ||
        !vidx || !grpx || !entry) {
        T_D("Invalid Input!");
        return rc;
    }
    exe_time_base = vtss_current_time();

    memset(buf, 0x0, sizeof(buf));
    T_D("enter->isid:%d/ver:%d/vid:%u/grp=%s",
        sidx,
        verx,
        *vidx,
        misc_ipv6_txt(grpx, buf));

    version = verx;
    if ((sidx == VTSS_ISID_GLOBAL) ||
        (sidx == VTSS_ISID_LOCAL) ||
        ipmc_lib_isid_is_local(sidx)) {
        ipmc_group_entry_t  grp;

        entry->valid = FALSE;
        grp.ipmc_version = version;
        grp.vid = *vidx;
        IPMC_LIB_ADRS_CPY(&grp.group_addr, grpx);
        MVR_GET_CRIT_ENTER();
        if (vtss_mvr_group_entry_get_next(&version, vidx, grpx, &grp) == VTSS_RC_OK) {
            if (grp.ipmc_version == version) {
                entry->valid = TRUE;
            }
        }
        MVR_GET_CRIT_EXIT();

        if (entry->valid) {
            entry->ipmc_version = grp.ipmc_version;
            entry->vid = grp.vid;
            IPMC_LIB_ADRS_CPY(&entry->group_addr, &grp.group_addr);
            memcpy(&entry->db, &grp.info->db, sizeof(ipmc_group_db_t));

            if (!msg_switch_is_master()) {
                entry->db.ipmc_sf_do_forward_srclist = NULL;
                entry->db.ipmc_sf_do_not_forward_srclist = NULL;
            }

            rc = VTSS_RC_OK;
        }
    } else {
        if (IPMC_LIB_ISID_EXIST(sidx)) {
            MVR_GET_CRIT_ENTER();
            memset(&mvr_running.group_next_entry, 0x0, sizeof(ipmc_prot_intf_group_entry_t));
            MVR_GET_CRIT_EXIT();

            if (mvr_stacking_send_req(sidx, version, *vidx,
                                      grpx, NULL,
                                      MVR_MSG_ID_GROUP_ENTRY_GETNEXT_REQ,
                                      FALSE)) {
                T_D("timeout, MVR_MSG_ID_GROUP_ENTRY_GETNEXT_REQ(isid=%d)", sidx);
            } else {
                MVR_GET_CRIT_ENTER();
                memcpy(entry, &mvr_running.group_next_entry, sizeof(ipmc_prot_intf_group_entry_t));
                MVR_GET_CRIT_EXIT();

                rc = entry->valid ? VTSS_RC_OK : VTSS_RC_ERROR;
            }
        }
    }

    memset(buf, 0x0, sizeof(buf));
    T_D("exit(%s:%u-Ticks-GOT)->isid:%d/vid:%u/grp=%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        (u32)(vtss_current_time() - exe_time_base),
        sidx,
        entry->vid,
        misc_ipv6_txt(&entry->group_addr, buf));
    return rc;
}

/* STACK IPMC clear STAT Counter */
static mesa_rc mvr_stacking_clear_statistics(vtss_isid_t isid_add, mesa_vid_t vid)
{
    mvr_msg_buf_t                       buf;
    mvr_msg_stat_counter_clear_req_t    *msg;
    switch_iter_t                       sit;
    vtss_isid_t                         isid;

    T_D("enter, isid: %d", isid_add);

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if ((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) {
            continue;
        }

        mvr_msg_alloc(MVR_MSG_ID_STAT_COUNTER_CLEAR_REQ, &buf, FALSE, TRUE);
        msg = (mvr_msg_stat_counter_clear_req_t *)buf.msg;

        msg->msg_id = MVR_MSG_ID_STAT_COUNTER_CLEAR_REQ;
        msg->isid = isid;
        msg->vid = vid;
        mvr_msg_tx(&buf, isid, sizeof(*msg));
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

static mesa_rc mvr_stacking_sync_mgmt_conf(vtss_isid_t isid_add, ipmc_lib_mgmt_info_t *sys_mgmt)
{
    mvr_msg_sys_mgmt_set_req_t  buf, *msg = &buf;
    switch_iter_t               sit;
    vtss_isid_t                 isid;
    ipmc_mgmt_ipif_t            *ifp;
    u32                         ifx;
    ipmc_lib_mgmt_info_t        info, *ipmc_info = &info;

    if (!msg_switch_is_master() || !sys_mgmt) {
        return VTSS_RC_ERROR;
    }

    T_I("SYNC(isid: %d):MAC[%s]", isid_add, misc_mac2str(sys_mgmt->mac_addr));

    /* Fill out message */
    IPMC_MGMT_MAC_ADR_GET(sys_mgmt, msg->mgmt_mac);
    msg->intf_cnt = 0;
    for (ifx = 0; ifx < VTSS_IPMC_MGMT_IPIF_MAX_CNT; ifx++) {
        if (IPMC_MGMT_IPIF_VALID(sys_mgmt, ifx)) {
            T_I("SYNC(%u) INTF-ADR-%u(VID:%u) is %s",
                (msg->intf_cnt + 1),
                IPMC_MGMT_IPIF_ADRS4(sys_mgmt, ifx),
                IPMC_MGMT_IPIF_IDVLN(sys_mgmt, ifx),
                IPMC_MGMT_IPIF_STATE(sys_mgmt, ifx) ? "Up" : "Down");

            ifp = &msg->ip_addr[msg->intf_cnt++];
            ipmc_mgmt_intf_vidx(ifp) = IPMC_MGMT_IPIF_IDVLN(sys_mgmt, ifx);
            ipmc_mgmt_intf_live(ifp) = IPMC_MGMT_IPIF_VALID(sys_mgmt, ifx);
            ipmc_mgmt_intf_adr4(ifp) = IPMC_MGMT_IPIF_ADRS4(sys_mgmt, ifx);
            ipmc_mgmt_intf_opst(ifp) = IPMC_MGMT_IPIF_STATE(sys_mgmt, ifx);
        }
    }

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if (((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) ||
            ipmc_lib_isid_is_local(isid)) {
            continue;
        }

        /* Receive message */
        IPMC_MGMT_SYSTEM_CHANGE(ipmc_info) = TRUE;
        IPMC_MGMT_MAC_ADR_SET(ipmc_info, msg->mgmt_mac);
        for (ifx = 0; ifx < msg->intf_cnt; ifx++) {
            ifp = &msg->ip_addr[ifx];
            if (ipmc_mgmt_intf_live(ifp)) {
                T_I("RCV(%u) INTF-ADR-%u(VID:%u)/%s",
                    (ifx + 1),
                    ipmc_mgmt_intf_adr4(ifp),
                    ipmc_mgmt_intf_vidx(ifp),
                    ipmc_mgmt_intf_opst(ifp) ? "Up" : "Down");

                IPMC_MGMT_IPIF_IDVLN(ipmc_info, ifx) = ipmc_mgmt_intf_vidx(ifp);
                IPMC_MGMT_IPIF_VALID(ipmc_info, ifx) = ipmc_mgmt_intf_live(ifp);
                IPMC_MGMT_IPIF_ADRS4(ipmc_info, ifx) = ipmc_mgmt_intf_adr4(ifp);
                IPMC_MGMT_IPIF_STATE(ipmc_info, ifx) = ipmc_mgmt_intf_opst(ifp);
            }
        }

        if (!ipmc_lib_system_mgmt_info_set(ipmc_info)) {
            T_D("Failure in ipmc_lib_system_mgmt_info_set()");
        }
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

static BOOL mvr_get_is_vlan_conf_full(void)
{
    u16                     idx;
    mvr_conf_intf_entry_t   *entry;

    for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
        entry = &mvr_running.mvr_conf.mvr_conf_vlan[idx];

        if (!entry->valid) {
            return FALSE;
        }
    }

    return TRUE;
}

static BOOL mvr_get_vlan_conf_entry(mvr_conf_intf_entry_t *entry)
{
    u16                     idx;
    mvr_conf_intf_entry_t   *p;

    if (!entry || (entry->vid > MVR_VID_MAX)) {
        return FALSE;
    }

    for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
        p = &mvr_running.mvr_conf.mvr_conf_vlan[idx];

        if (p->valid && (p->vid == entry->vid)) {
            memcpy(entry, p, sizeof(mvr_conf_intf_entry_t));
            return TRUE;
        }
    }

    return FALSE;
}

static BOOL mvr_get_next_vlan_conf_entry(mvr_conf_intf_entry_t *entry)
{
    u16                     idx;
    mvr_conf_intf_entry_t   *p, *q;

    if (!entry || (entry->vid > MVR_VID_MAX)) {
        return FALSE;
    }

    q = NULL;
    for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
        p = &mvr_running.mvr_conf.mvr_conf_vlan[idx];

        if (!p->valid) {
            continue;
        }

        if (p->vid > entry->vid) {
            if (!q) {
                q = p;
            } else {
                if (p->vid < q->vid) {
                    q = p;
                }
            }
        }
    }

    if (q != NULL) {
        memcpy(entry, q, sizeof(mvr_conf_intf_entry_t));
        return TRUE;
    } else {
        return FALSE;
    }
}

static i32 _mvr_intf_name_vid_cmp_func(void *elm1, void *elm2)
{
    mvr_conf_intf_entry_t   *element1, *element2;
    i8                      name1[MVR_NAME_MAX_LEN], name2[MVR_NAME_MAX_LEN];

    if (!elm1 || !elm2) {
        T_W("MVR_ASSERT(_mvr_intf_name_vid_cmp_func)");
        for (;;) {}
    }

    element1 = (mvr_conf_intf_entry_t *)elm1;
    element2 = (mvr_conf_intf_entry_t *)elm2;
    memset(name1, 0x0, sizeof(name1));
    strncpy(name1, element1->name, strlen(element1->name));
    memset(name2, 0x0, sizeof(name2));
    strncpy(name2, element2->name, strlen(element2->name));

    if (strncmp(name1, name2, MVR_NAME_MAX_LEN) > 0) {
        return 1;
    } else if (strncmp(name1, name2, MVR_NAME_MAX_LEN) < 0) {
        return -1;
    } else if (element1->vid > element2->vid) {
        return 1;
    } else if (element1->vid < element2->vid) {
        return -1;
    } else {
        return 0;
    }
}

static BOOL mvr_get_vlan_conf_entry_by_name(mvr_conf_intf_entry_t *entry)
{
    u16                     idx;
    int                     len;
    mvr_conf_intf_entry_t   *p;

    if (!entry) {
        return FALSE;
    }

    len = strlen(entry->name);
    for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
        p = &mvr_running.mvr_conf.mvr_conf_vlan[idx];

        if (!p->valid) {
            continue;
        }

        if (len != strlen(p->name)) {
            continue;
        }

        if (!strncmp(entry->name, p->name, len)) {
            memcpy(entry, p, sizeof(mvr_conf_intf_entry_t));
            return TRUE;
        }
    }

    return FALSE;
}

static BOOL mvr_get_next_vlan_conf_entry_by_name(mvr_conf_intf_entry_t *entry)
{
    u16                     idx;
    mvr_conf_intf_entry_t   *p, *q;

    if (!entry) {
        return FALSE;
    }

    q = NULL;
    for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
        p = &mvr_running.mvr_conf.mvr_conf_vlan[idx];

        if (!p->valid) {
            continue;
        }

        if (_mvr_intf_name_vid_cmp_func((void *)p, (void *)entry) > 0) {
            if (!q) {
                q = p;
            } else {
                if (_mvr_intf_name_vid_cmp_func((void *)p, (void *)q) < 0) {
                    q = p;
                }
            }
        }
    }

    if (q != NULL) {
        memcpy(entry, q, sizeof(mvr_conf_intf_entry_t));
        return TRUE;
    } else {
        return FALSE;
    }
}

/* Obsoleted API for MVID */
mesa_rc mvr_mgmt_get_mvid(u16 *obs_mvid)
{
    mvr_conf_intf_entry_t   entry;

    if (!obs_mvid) {
        return VTSS_RC_ERROR;
    }

    memset(&entry, 0x0, sizeof(mvr_conf_intf_entry_t));
    MVR_CRIT_ENTER();
    if (mvr_get_next_vlan_conf_entry(&entry) != VTSS_OK) {
        MVR_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }
    MVR_CRIT_EXIT();

    *obs_mvid = entry.vid;
    return VTSS_OK;
}

static BOOL _mvr_validate_profile_overlap(u16 mvid, u32 pdx, mesa_rc *rc)
{
    u32                         vdx;
    BOOL                        flag, first_permit, tmp;
    mvr_conf_intf_entry_t       intf;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *pf;
    ipmc_lib_rule_t             *rule;
    ipmc_profile_rule_t         *ptr, *bgn, *end, *chk, buf;

    if (!rc) {
        return FALSE;
    }

    if (!IPMC_MEM_PROFILE_MTAKE(pfm)) {
        *rc = IPMC_ERROR_MEMORY_NG;
        return FALSE;
    }

    *rc = VTSS_OK;
    if (!pdx) {
        IPMC_MEM_PROFILE_MGIVE(pfm);
        return TRUE;
    }

    pf = &pfm->profile;
    pf->data.index = pdx;
    if (!ipmc_lib_fltr_profile_get(pf, FALSE)) {
        *rc = IPMC_ERROR_ENTRY_NOT_FOUND;
        IPMC_MEM_PROFILE_MGIVE(pfm);
        return FALSE;
    }

    flag = TRUE;
    first_permit = FALSE;
    bgn = end = ptr = NULL;
    while ((ptr = ipmc_lib_profile_tree_get_next(pdx, ptr, &tmp)) != NULL) {
        if ((rule = ptr->rule) == NULL) {
            continue;
        }

        if (bgn) {
            first_permit = FALSE;

            if (bgn->version != ptr->version) {
                end = NULL;
                bgn = ptr;

                continue;
            }

            if (rule->action == IPMC_ACTION_PERMIT) {
                end = ptr;
            } else {
                end = NULL;
                bgn = ptr;
            }
        } else {
            bgn = ptr;
            if (rule->action == IPMC_ACTION_PERMIT) {
                first_permit = TRUE;
                end = ptr;
            }
        }

        if (!end) {
            continue;
        }

        memset(&intf, 0x0, sizeof(mvr_conf_intf_entry_t));
        while (mvr_get_next_vlan_conf_entry(&intf)) {
            if (intf.vid == mvid) {
                continue;
            }
            if (intf.profile_index == MVR_CONF_DEF_INTF_PROFILE) {
                continue;
            }

            if (intf.profile_index == pdx) {
                flag = FALSE;
                break;
            }

            pf = &pfm->profile;
            memset(pf, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
            pf->data.index = intf.profile_index;
            if ((pf = ipmc_lib_fltr_profile_get(pf, FALSE)) != NULL) {
                vdx = pf->data.index;

                memcpy(&buf, bgn, sizeof(ipmc_profile_rule_t));
                if (first_permit) {
                    IPMC_LIB_ADRS_SET(&buf.grp_adr, 0x0);
                }
                chk = &buf;
                if ((chk = ipmc_lib_profile_tree_get_next(vdx, chk, &tmp)) != NULL) {
                    if (chk->version != bgn->version) {
                        continue;
                    }

                    if ((rule = chk->rule) != NULL) {
                        if (rule->action == IPMC_ACTION_PERMIT) {
                            flag = FALSE;
                            break;
                        }
                    }

                    memcpy(&buf, end, sizeof(ipmc_profile_rule_t));
                    buf.vir_idx = 0x0;
                    chk = &buf;
                    if ((chk = ipmc_lib_profile_tree_get_next(vdx, chk, &tmp)) != NULL) {
                        if (chk->version != end->version) {
                            continue;
                        }

                        if ((rule = chk->rule) != NULL) {
                            if (rule->action == IPMC_ACTION_PERMIT) {
                                flag = FALSE;
                                break;
                            }
                        }
                    }
                }
            }
        }

        if (!flag) {
            break;
        } else {
            bgn = end;
            end = NULL;
        }
    }

    if (!flag) {
        *rc = IPMC_ERROR_ENTRY_OVERLAPPED;
    }

    IPMC_MEM_PROFILE_MGIVE(pfm);
    return flag;
}

static BOOL _mvr_sanity_chk_interface_entry(vtss_isid_t isid,
                                            mvr_mgmt_interface_t *entry,
                                            ipmc_operation_action_t action,
                                            mesa_rc *rc)
{
    u16                     vid;
    port_iter_t             pit;
    u32                     idx;
    mvr_conf_intf_entry_t   chk, *p;
    mvr_conf_port_role_t    *q;

    if (!entry || !rc) {
        return FALSE;
    }

    *rc = VTSS_OK;
    vid = entry->vid;
    if (vid < MVR_VID_MIN || vid > MVR_VID_MAX) {
        *rc = IPMC_ERROR_PARM;
        return FALSE;
    }

    chk.vid = vid;
    MVR_CRIT_ENTER();
    if (mvr_get_vlan_conf_entry(&chk)) {
        if (action == IPMC_OP_ADD) {
            *rc = IPMC_ERROR_ENTRY_INVALID;
        }
    } else {
        if (action == IPMC_OP_ADD) {
            if (mvr_get_is_vlan_conf_full()) {
                *rc = IPMC_ERROR_TABLE_IS_FULL;
            }
        } else {
            *rc = IPMC_ERROR_VLAN_NOT_FOUND;
        }
    }

    if (*rc == VTSS_OK && strlen(entry->intf.name)) {
        memset(chk.name, 0x0, MVR_NAME_MAX_LEN);
        strncpy(chk.name, entry->intf.name, strlen(entry->intf.name));
        if (mvr_get_vlan_conf_entry_by_name(&chk) &&
            chk.vid != vid) {
            *rc = IPMC_ERROR_ENTRY_NAME_DUPLICATED;
        }
    }
    MVR_CRIT_EXIT();

    if (*rc != VTSS_OK) {
        return FALSE;
    }

    if (action == IPMC_OP_ADD) {
        vtss_appl_vlan_entry_t vlan_entry;

        vlan_entry.vid = vid;
        if (vtss_appl_vlan_get(isid, vid, &vlan_entry, FALSE, VTSS_APPL_VLAN_USER_ALL) == VTSS_OK) {
            *rc = IPMC_ERROR_VLAN_ACTIVE;
        }
    }

    p = &entry->intf;
    q = &entry->role;

    if ((p->mode != MVR_INTF_MODE_DYNA) &&
        (p->mode != MVR_INTF_MODE_COMP) &&
        (p->mode != MVR_INTF_MODE_INIT)) {
        *rc = IPMC_ERROR_PARM;
        return FALSE;
    }

    if ((p->vtag != IPMC_INTF_UNTAG) &&
        (p->vtag != IPMC_INTF_TAGED)) {
        *rc = IPMC_ERROR_PARM;
        return FALSE;
    }
    if (p->priority > IPMC_PARAM_MAX_PRIORITY) {
        *rc = IPMC_ERROR_PARM;
        return FALSE;
    }

    if (p->querier4_address) {
        u8  adrc = (p->querier4_address >> 24) & 0xFF;

        if ((adrc == 127) || (adrc > 223)) {
            *rc = IPMC_ERROR_PARM;
            return FALSE;
        }
    }

    if (p->last_listener_query_interval > 31744) {
        *rc = IPMC_ERROR_PARM;
        return FALSE;
    }

    (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
    while (port_iter_getnext(&pit)) {
        idx = pit.iport;

        if ((q->ports[idx] != MVR_PORT_ROLE_INACT) &&
            (q->ports[idx] != MVR_PORT_ROLE_SOURC) &&
            (q->ports[idx] != MVR_PORT_ROLE_RECVR)) {
            *rc = IPMC_ERROR_PARM;
            return FALSE;
        }

        if (q->ports[idx] != MVR_PORT_ROLE_INACT) {
            BOOL                    next;
            vtss_mvr_interface_t    mvrif;

            memset(&mvrif, 0x0, sizeof(vtss_mvr_interface_t));
            MVR_CRIT_ENTER();
            next = (vtss_mvr_interface_get_next(&mvrif) == VTSS_OK);
            MVR_CRIT_EXIT();
            while (next) {
                if ((entry->vid != mvrif.basic.param.vid) &&
                    (mvrif.ports[idx] != MVR_PORT_ROLE_INACT)) {
                    if (q->ports[idx] != mvrif.ports[idx]) {
                        *rc = IPMC_ERROR_PARM;
                        return FALSE;
                    }
                }

                MVR_CRIT_ENTER();
                next = (vtss_mvr_interface_get_next(&mvrif) == VTSS_OK);
                MVR_CRIT_EXIT();
            }
        }
    }

    return TRUE;
}

/* Only mvr_mgmt_set_intf_entry can access this func */
static BOOL _mvr_mgmt_set_intf_role(vtss_isid_t isid, ipmc_operation_action_t action, mvr_conf_port_role_t *entry)
{
    port_iter_t             pit;
    u32                     idx;
    mvr_conf_port_role_t    *p;

    switch (action) {
    case IPMC_OP_ADD:
        for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
            p = &mvr_running.mvr_conf.mvr_conf_role[ipmc_lib_isid_convert(TRUE, isid)].intf[idx];

            if (!p->valid) {
                memcpy(p, entry, sizeof(mvr_conf_port_role_t));
                p->valid = TRUE;

                if (ipmc_lib_isid_is_local(isid)) {
                    memcpy(&mvr_running.mvr_conf.mvr_conf_role[VTSS_ISID_LOCAL].intf[idx],
                           p,
                           sizeof(mvr_conf_port_role_t));
                }

                return TRUE;
            }
        }

        break;
    case IPMC_OP_DEL:
        for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
            p = &mvr_running.mvr_conf.mvr_conf_role[ipmc_lib_isid_convert(TRUE, isid)].intf[idx];

            if (!p->valid || (p->vid != entry->vid)) {
                continue;
            }

            memset(p, 0x0, sizeof(mvr_conf_port_role_t));
            /* p->valid = FALSE; */
            (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
            while (port_iter_getnext(&pit)) {
                p->ports[pit.iport] = MVR_CONF_DEF_PORT_ROLE;
            }

            if (ipmc_lib_isid_is_local(isid)) {
                memcpy(&mvr_running.mvr_conf.mvr_conf_role[VTSS_ISID_LOCAL].intf[idx],
                       p,
                       sizeof(mvr_conf_port_role_t));
            }

            return TRUE;
        }

        break;
    case IPMC_OP_UPD:
    case IPMC_OP_SET:
        for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
            p = &mvr_running.mvr_conf.mvr_conf_role[ipmc_lib_isid_convert(TRUE, isid)].intf[idx];

            if (!p->valid || (p->vid != entry->vid)) {
                continue;
            }

            if (memcmp(p, entry, sizeof(mvr_conf_port_role_t))) {
                memcpy(p, entry, sizeof(mvr_conf_port_role_t));
            }

            p->valid = TRUE;

            if (ipmc_lib_isid_is_local(isid)) {
                memcpy(&mvr_running.mvr_conf.mvr_conf_role[VTSS_ISID_LOCAL].intf[idx],
                       p,
                       sizeof(mvr_conf_port_role_t));
            }

            return TRUE;
        }

        break;
    default:

        break;
    }

    return FALSE;
}

static BOOL _mvr_mgmt_set_intf_intf(ipmc_operation_action_t action, mvr_conf_intf_entry_t *entry)
{
    switch_iter_t           sit;
    u32                     idx, i;
    mvr_conf_intf_entry_t   *p;
    mvr_conf_intf_role_t    *q;

    switch (action) {
    case IPMC_OP_ADD:
        for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
            p = &mvr_running.mvr_conf.mvr_conf_vlan[idx];

            if (!p->valid) {
                u32 roleIdx;

                memcpy(p, entry, sizeof(mvr_conf_intf_entry_t));
                p->valid = TRUE;

                (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
                while (switch_iter_getnext(&sit)) {
                    i = sit.isid;

                    q = &mvr_running.mvr_conf.mvr_conf_role[ipmc_lib_isid_convert(TRUE, i)];
                    mvr_conf_reset_intf_role(i, (mesa_vid_t)entry->vid, q, TRUE);
                    for (roleIdx = 0; roleIdx < MVR_VLAN_MAX; roleIdx++) {
                        if (q->intf[roleIdx].valid) {
                            continue;
                        }

                        q->intf[roleIdx].valid = TRUE;
                        q->intf[roleIdx].vid = entry->vid;
                        break;
                    }

                    if (ipmc_lib_isid_is_local(i)) {
                        q = &mvr_running.mvr_conf.mvr_conf_role[VTSS_ISID_LOCAL];
                        mvr_conf_reset_intf_role(VTSS_ISID_LOCAL, (mesa_vid_t)entry->vid, q, TRUE);
                        for (roleIdx = 0; roleIdx < MVR_VLAN_MAX; roleIdx++) {
                            if (q->intf[roleIdx].valid) {
                                continue;
                            }

                            q->intf[roleIdx].valid = TRUE;
                            q->intf[roleIdx].vid = entry->vid;
                            break;
                        }
                    }
                }

                return TRUE;
            }
        }

        break;
    case IPMC_OP_DEL:
        for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
            p = &mvr_running.mvr_conf.mvr_conf_vlan[idx];

            if (!p->valid || (p->vid != entry->vid)) {
                continue;
            }

            (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
            while (switch_iter_getnext(&sit)) {
                i = sit.isid;

                q = &mvr_running.mvr_conf.mvr_conf_role[ipmc_lib_isid_convert(TRUE, i)];
                mvr_conf_reset_intf_role(i, (mesa_vid_t)entry->vid, q, TRUE);

                if (ipmc_lib_isid_is_local(i)) {
                    q = &mvr_running.mvr_conf.mvr_conf_role[VTSS_ISID_LOCAL];
                    mvr_conf_reset_intf_role(VTSS_ISID_LOCAL, (mesa_vid_t)entry->vid, q, TRUE);
                }
            }
            mvr_conf_reset_intf(p, TRUE);
            p->valid = FALSE;

            return TRUE;
        }

        break;
    case IPMC_OP_UPD:
    case IPMC_OP_SET:
        for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
            p = &mvr_running.mvr_conf.mvr_conf_vlan[idx];

            if (!p->valid || (p->vid != entry->vid)) {
                continue;
            }

            if (memcmp(p, entry, sizeof(mvr_conf_intf_entry_t))) {
                memset(p->name, 0x0, sizeof(p->name));
                memcpy(p, entry, sizeof(mvr_conf_intf_entry_t));
                p->valid = TRUE;
                return TRUE;
            }

            break;
        }

        break;
    default:

        break;
    }

    return FALSE;
}

static vtss_mvr_interface_t mvr_intf4set;

/* Set MVR VLAN Interface */
mesa_rc mvr_mgmt_set_intf_entry(vtss_isid_t isid_in, ipmc_operation_action_t action, mvr_mgmt_interface_t *entry)
{
    mesa_rc               sanity_rc;
    switch_iter_t         sit;
    vtss_isid_t           isid;
    BOOL                  flag;
    mvr_conf_intf_entry_t *p, bkup;
    vtss_mvr_interface_t  *ifp;
    vtss_tick_count_t     exe_time_base;

    if (!entry || !msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    exe_time_base = vtss_current_time();
    sanity_rc = VTSS_RC_ERROR;
    if (isid_in == VTSS_ISID_GLOBAL) {
        (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
        while (switch_iter_getnext(&sit)) {
            if (!_mvr_sanity_chk_interface_entry(sit.isid, entry, action, &sanity_rc)) {
                return sanity_rc;
            }
        }
    } else {
        if (!_mvr_sanity_chk_interface_entry(isid_in, entry, action, &sanity_rc)) {
            return sanity_rc;
        }
    }

    MVR_CRIT_ENTER();
    if (action != IPMC_OP_DEL) {
        if (!_mvr_validate_profile_overlap(entry->vid, entry->intf.profile_index, &sanity_rc)) {
            MVR_CRIT_EXIT();
            return sanity_rc;
        }
    }

    if (action == IPMC_OP_ADD) {
        p = &entry->intf;
        memcpy(&bkup, p, sizeof(mvr_conf_intf_entry_t));
        mvr_conf_reset_intf(p, FALSE);

        p->mode = bkup.mode;
        p->vtag = bkup.vtag;
        p->querier_status = bkup.querier_status;
        p->querier4_address = bkup.querier4_address;
        p->priority = bkup.priority;
        p->profile_index = bkup.profile_index;
        p->last_listener_query_interval = bkup.last_listener_query_interval;
    }

    entry->intf.vid = entry->role.vid = entry->vid;
    flag = _mvr_mgmt_set_intf_intf(action, &entry->intf);
    if (isid_in != VTSS_ISID_GLOBAL) {
        (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
        while (switch_iter_getnext(&sit)) {
            isid = sit.isid;

            if (isid_in != isid) {
                continue;
            }

            flag |= _mvr_mgmt_set_intf_role(isid, action, &entry->role);
        }
    }

    ifp = &mvr_intf4set;
    MVR_CRIT_EXIT();

    if (!flag) {
        return VTSS_OK;
    }

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if ((isid_in != VTSS_ISID_GLOBAL) && (isid_in != isid)) {
            continue;
        }

        MVR_CRIT_ENTER();
        _mvr_mgmt_transit_intf(isid_in, isid, ifp, entry, IPMC_IP_VERSION_IGMP);
        MVR_CRIT_EXIT();
        (void) mvr_stacking_set_intf_entry(isid, action, ifp, IPMC_IP_VERSION_IGMP);
        MVR_CRIT_ENTER();
        _mvr_mgmt_transit_intf(isid_in, isid, ifp, entry, IPMC_IP_VERSION_MLD);
        MVR_CRIT_EXIT();
        (void) mvr_stacking_set_intf_entry(isid, action, ifp, IPMC_IP_VERSION_MLD);
    }

    T_D("mvr_mgmt_set_intf_entry(%u) consumes %u ticks", entry->vid, (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

/* Validate MVR VLAN Channel */
mesa_rc mvr_mgmt_validate_intf_channel(void)
{
    u16                     idx;
    mesa_rc                 ret;
    mvr_conf_intf_entry_t   *p;

    ret = VTSS_OK;
    MVR_CRIT_ENTER();
    if (mvr_running.mvr_conf.mvr_conf_global.mvr_state) {
        for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
            p = &mvr_running.mvr_conf.mvr_conf_vlan[idx];

            if (!p->valid || !p->profile_index) {
                continue;
            }

            if (!_mvr_validate_profile_overlap(p->vid, p->profile_index, &ret)) {
                break;
            }
        }
    }
    MVR_CRIT_EXIT();

    return ret;
}

/* Get MVR VLAN Interface By Name(String) */
mesa_rc mvr_mgmt_get_intf_entry_by_name(vtss_isid_t isid, mvr_mgmt_interface_t *entry)
{
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!entry) {
        return VTSS_RC_ERROR;
    }

    if (strlen(entry->intf.name) >= MVR_NAME_MAX_LEN) {
        return VTSS_RC_ERROR;
    }

    MVR_CRIT_ENTER();
    if (mvr_get_vlan_conf_entry_by_name(&entry->intf)) {
        vtss_isid_t isid_get = isid;

        if (isid_get == VTSS_ISID_GLOBAL) {
            isid_get = VTSS_ISID_LOCAL;
        }

        entry->role.vid = entry->intf.vid;
        if (!mvr_get_port_role_entry(isid_get, &entry->role)) {
            MVR_CRIT_EXIT();
            return VTSS_RC_ERROR;
        }

        entry->vid = entry->intf.vid;
    } else {
        MVR_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }
    MVR_CRIT_EXIT();

    T_D("mvr_mgmt_get_intf_entry_by_name consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

/* Get-Next MVR VLAN Interface By Name(String) */
mesa_rc mvr_mgmt_get_next_intf_entry_by_name(vtss_isid_t isid, mvr_mgmt_interface_t *entry)
{
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!entry) {
        return VTSS_RC_ERROR;
    }

    if (strlen(entry->intf.name) >= MVR_NAME_MAX_LEN) {
        return VTSS_RC_ERROR;
    }

    MVR_CRIT_ENTER();
    if (mvr_get_next_vlan_conf_entry_by_name(&entry->intf)) {
        vtss_isid_t isid_get = isid;

        if (isid_get == VTSS_ISID_GLOBAL) {
            isid_get = VTSS_ISID_LOCAL;
        }

        entry->role.vid = entry->intf.vid;
        if (!mvr_get_port_role_entry(isid_get, &entry->role)) {
            MVR_CRIT_EXIT();
            return VTSS_RC_ERROR;
        }

        entry->vid = entry->intf.vid;
    } else {
        MVR_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }
    MVR_CRIT_EXIT();

    T_D("mvr_mgmt_get_next_intf_entry_by_name consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

/* Get MVR VLAN Interface */
mesa_rc mvr_mgmt_get_intf_entry(vtss_isid_t isid, mvr_mgmt_interface_t *entry)
{
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!entry) {
        return VTSS_RC_ERROR;
    }

    entry->intf.vid = entry->vid;
    MVR_CRIT_ENTER();
    if (mvr_get_vlan_conf_entry(&entry->intf)) {
        vtss_isid_t isid_get = isid;

        if (isid_get == VTSS_ISID_GLOBAL) {
            isid_get = VTSS_ISID_LOCAL;
        }

        entry->role.vid = entry->intf.vid;
        if (!mvr_get_port_role_entry(isid_get, &entry->role)) {
            T_D("mvr_get_port_role_entry cannot get port role for VID %u", entry->role.vid);
            MVR_CRIT_EXIT();
            return VTSS_RC_ERROR;
        }

        entry->vid = entry->intf.vid;
    } else {
        MVR_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }
    MVR_CRIT_EXIT();

    T_D("mvr_mgmt_get_intf_entry consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

/* Get-Next MVR VLAN Interface */
mesa_rc mvr_mgmt_get_next_intf_entry(vtss_isid_t isid, mvr_mgmt_interface_t *entry)
{
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!entry) {
        return VTSS_RC_ERROR;
    }

    entry->intf.vid = entry->vid;
    MVR_CRIT_ENTER();
    if (mvr_get_next_vlan_conf_entry(&entry->intf)) {
        vtss_isid_t isid_get = isid;

        if (isid_get == VTSS_ISID_GLOBAL) {
            isid_get = VTSS_ISID_LOCAL;
        }

        entry->role.vid = entry->intf.vid;
        if (!mvr_get_port_role_entry(isid_get, &entry->role)) {
            MVR_CRIT_EXIT();
            return VTSS_RC_ERROR;
        }

        entry->vid = entry->intf.vid;
    } else {
        MVR_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }
    MVR_CRIT_EXIT();

    T_D("mvr_mgmt_get_next_intf_entry consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

static void _mvr_mgmt_revert_intf(mvr_local_interface_t *entry, vtss_mvr_interface_t *mvrif)
{
    ipmc_intf_entry_t               *itfce;
    ipmc_prot_intf_entry_param_t    *param;
    mvr_conf_intf_entry_t           *p;
    ipmc_querier_sm_t               *q;

    if (!entry || !mvrif) {
        return;
    }

    itfce = &mvrif->basic;
    param = &itfce->param;
    p = &entry->intf;
    q = &param->querier;
    entry->version = itfce->ipmc_version;
    p->vid = entry->vid = param->vid;

    memcpy(p->name, mvrif->name, sizeof(p->name));
    p->mode = mvrif->mode;
    p->vtag = mvrif->vtag;
    p->querier_status = q->querier_enabled;
    p->querier4_address = q->QuerierAdrs4;
    p->priority = mvrif->priority;
    p->profile_index = mvrif->profile_index;
    p->protocol_status = itfce->op_state;
    p->compatibility = param->cfg_compatibility;
    p->robustness_variable = q->RobustVari;
    p->query_interval = q->QueryIntvl;
    p->query_response_interval = q->MaxResTime;
    p->last_listener_query_interval = q->LastQryItv;
    p->unsolicited_report_interval = q->UnsolicitR;

    memcpy(entry->role_ports, mvrif->ports, sizeof(entry->role_ports));
    memcpy(entry->vlan_ports, itfce->vlan_ports, sizeof(entry->vlan_ports));
}

/* Get Local MVR VLAN Interface */
mesa_rc mvr_mgmt_local_interface_get(mvr_local_interface_t *entry)
{
    mesa_rc                 retVal;
    vtss_mvr_interface_t    mvrif;
    ipmc_intf_entry_t       *intf;

    if (!entry) {
        return VTSS_RC_ERROR;
    }

    memset(&mvrif, 0x0, sizeof(vtss_mvr_interface_t));
    intf = &mvrif.basic;
    intf->param.vid = entry->vid;
    intf->ipmc_version = entry->version;
    MVR_CRIT_ENTER();
    retVal = vtss_mvr_interface_get(&mvrif);
    MVR_CRIT_EXIT();

    if (retVal == VTSS_OK) {
        _mvr_mgmt_revert_intf(entry, &mvrif);
    }

    return retVal;
}

/* Get-Next Local MVR VLAN Interface */
mesa_rc mvr_mgmt_local_interface_get_next(mvr_local_interface_t *entry)
{
    mesa_rc                 retVal;
    vtss_mvr_interface_t    mvrif;
    ipmc_intf_entry_t       *intf;

    if (!entry) {
        return VTSS_RC_ERROR;
    }

    memset(&mvrif, 0x0, sizeof(vtss_mvr_interface_t));
    intf = &mvrif.basic;
    intf->param.vid = entry->vid;
    intf->ipmc_version = entry->version;
    MVR_CRIT_ENTER();
    retVal = vtss_mvr_interface_get_next(&mvrif);
    MVR_CRIT_EXIT();

    if (retVal == VTSS_OK) {
        _mvr_mgmt_revert_intf(entry, &mvrif);
    }

    return retVal;
}

/* Get MVR VLAN Operational Status */
mesa_rc mvr_mgmt_get_intf_info(vtss_isid_t isid, ipmc_ip_version_t version, ipmc_prot_intf_entry_param_t *entry)
{
    if (!entry) {
        return VTSS_RC_ERROR;
    }

    return mvr_stacking_get_intf_entry(isid, version, entry);
}

/* Get-Next MVR VLAN Operational Status */
mesa_rc mvr_mgmt_get_next_intf_info(vtss_isid_t isid, ipmc_ip_version_t *version, ipmc_prot_intf_entry_param_t *entry)
{
    BOOL                    flag;
    mvr_conf_intf_entry_t   intf;

    if (!version || !entry) {
        return VTSS_RC_ERROR;
    }

    memset(&intf, 0x0, sizeof(mvr_conf_intf_entry_t));
    if (*version == IPMC_IP_VERSION_ALL) {
        intf.vid = entry->vid;
        MVR_CRIT_ENTER();
        flag = mvr_get_next_vlan_conf_entry(&intf);
        MVR_CRIT_EXIT();
        if (!flag) {
            return VTSS_RC_ERROR;
        }

        *version = IPMC_IP_VERSION_IGMP;
        entry->vid = intf.vid;
    } else {
        if (*version == IPMC_IP_VERSION_IGMP) {
            *version = IPMC_IP_VERSION_MLD;
        } else {
            intf.vid = entry->vid;
            MVR_CRIT_ENTER();
            flag = mvr_get_next_vlan_conf_entry(&intf);
            MVR_CRIT_EXIT();
            if (!flag) {
                return VTSS_RC_ERROR;
            }

            *version = IPMC_IP_VERSION_IGMP;
            entry->vid = intf.vid;
        }
    }

    return mvr_stacking_get_intf_entry(isid, *version, entry);
}

mesa_rc
mvr_mgmt_get_next_group_srclist(
    const vtss_isid_t               isid,
    const ipmc_ip_version_t         ipmc_version,
    const mesa_vid_t                vid,
    const mesa_ipv6_t               *const addr,
    ipmc_prot_group_srclist_t       *const group_srclist_entry
)
{
    mesa_rc rc = VTSS_OK;

    if (!addr || !group_srclist_entry) {
        return VTSS_RC_ERROR;
    }

    if ((rc = mvr_stacking_get_next_group_srclist_entry(isid, ipmc_version, vid, addr, group_srclist_entry)) == VTSS_OK) {
        if (group_srclist_entry->valid == FALSE) {
            rc = VTSS_RC_ERROR;
        }
    }

    return rc;
}

mesa_rc mvr_mgmt_get_next_intf_group(vtss_isid_t isid,
                                     ipmc_ip_version_t *ipmc_version,
                                     u16 *vid,
                                     ipmc_prot_intf_group_entry_t *intf_group_entry)
{
    mesa_rc rc = VTSS_OK;

    if (!intf_group_entry || !ipmc_version || !vid) {
        return VTSS_RC_ERROR;
    }

    if ((rc = mvr_stacking_get_next_intf_group_entry(isid, *ipmc_version, *vid, intf_group_entry)) == VTSS_OK) {
        if (intf_group_entry->valid == FALSE) {
            rc = VTSS_RC_ERROR;
        } else {
            *vid = intf_group_entry->vid;
            *ipmc_version = intf_group_entry->ipmc_version;
        }
    }

    return rc;
}

/* Get MVR Group Info */
mesa_rc
mvr_mgmt_group_info_get(
    const vtss_isid_t               sidx,
    const ipmc_ip_version_t         verx,
    const mesa_vid_t                *const vidx,
    const mesa_ipv6_t               *const grpx,
    ipmc_prot_intf_group_entry_t    *const intf_group_entry
)
{
    return mvr_stacking_group_entry_get(sidx, verx, vidx, grpx, intf_group_entry);
}

/* GetNext MVR Group Info */
mesa_rc
mvr_mgmt_group_info_get_next(
    const vtss_isid_t               sidx,
    const ipmc_ip_version_t         verx,
    const mesa_vid_t                *const vidx,
    const mesa_ipv6_t               *const grpx,
    ipmc_prot_intf_group_entry_t    *const intf_group_entry
)
{
    return mvr_stacking_group_entry_get_next(sidx, verx, vidx, grpx, intf_group_entry);
}

mesa_rc mvr_mgmt_set_fast_leave_port(vtss_isid_t isid, mvr_conf_fast_leave_t *fast_leave_port)
{
    port_iter_t           pit;
    u32                   i;
    BOOL                  apply_flag = FALSE;
    mvr_conf_fast_leave_t *fast_leave;
    vtss_tick_count_t     exe_time_base = vtss_current_time();

    if (!fast_leave_port) {
        return VTSS_RC_ERROR;
    }

    if (!msg_switch_is_master()) {
        return VTSS_OK;
    }

    MVR_CRIT_ENTER();
    fast_leave = &mvr_running.mvr_conf.mvr_conf_fast_leave[ipmc_lib_isid_convert(TRUE, isid)];

    (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
    while (port_iter_getnext(&pit)) {
        i = pit.iport;

        if (VTSS_PORT_BF_GET(fast_leave_port->ports, i) !=
            VTSS_PORT_BF_GET(fast_leave->ports, i)) {
            apply_flag = TRUE;
            break;
        }
    }

    if (!apply_flag) {
        MVR_CRIT_EXIT();
        return VTSS_OK;
    } else {
        (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        while (port_iter_getnext(&pit)) {
            i = pit.iport;

            VTSS_PORT_BF_SET(fast_leave->ports,
                             i,
                             VTSS_PORT_BF_GET(fast_leave_port->ports, i));
        }
    }
    MVR_CRIT_EXIT();

    (void) mvr_stacking_set_fastleave_port(isid);

    T_D("mvr_mgmt_set_fast_leave_port consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc mvr_mgmt_get_fast_leave_port(vtss_isid_t isid, mvr_conf_fast_leave_t *fast_leave_port)
{
    port_iter_t           pit;
    mvr_conf_fast_leave_t *fast_leave;
    vtss_tick_count_t     exe_time_base = vtss_current_time();

    if (!fast_leave_port) {
        return VTSS_RC_ERROR;
    }

    MVR_CRIT_ENTER();
    fast_leave = &mvr_running.mvr_conf.mvr_conf_fast_leave[ipmc_lib_isid_convert(TRUE, isid)];

    (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
    memset(fast_leave_port, 0x0, sizeof(mvr_conf_fast_leave_t));
    while (port_iter_getnext(&pit)) {
        VTSS_PORT_BF_SET(fast_leave_port->ports,
                         pit.iport,
                         VTSS_PORT_BF_GET(fast_leave->ports, pit.iport));
    }
    MVR_CRIT_EXIT();

    T_D("mvr_mgmt_get_fast_leave_port consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc mvr_mgmt_set_mode(BOOL *mode)
{
    vtss_tick_count_t exe_time_base = vtss_current_time();

    MVR_CRIT_ENTER();
    if (mvr_running.mvr_conf.mvr_conf_global.mvr_state == *mode) {
        MVR_CRIT_EXIT();
        return VTSS_OK;
    }

    mvr_running.mvr_conf.mvr_conf_global.mvr_state = *mode;

    MVR_CRIT_EXIT();

    (void) mvr_stacking_set_mode(VTSS_ISID_GLOBAL);

    T_D("mvr_mgmt_set_mode consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc mvr_mgmt_get_mode(BOOL *mode)
{
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!mode) {
        return VTSS_RC_ERROR;
    }

    MVR_CRIT_ENTER();
    *mode = mvr_running.mvr_conf.mvr_conf_global.mvr_state;
    MVR_CRIT_EXIT();

    T_D("mvr_mgmt_get_mode consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc mvr_mgmt_clear_stat_counter(vtss_isid_t isid, mesa_vid_t vid)
{
    mesa_rc           rc;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    rc = mvr_stacking_clear_statistics(isid, vid);

    T_D("mvr_mgmt_clear_stat_counter consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));

    return rc;
}

static BOOL mvr_port_status[VTSS_ISID_END][VTSS_MAX_PORTS_LEGACY_CONSTANT_USE_CAPARRAY_INSTEAD], mvrReady = FALSE;
/* Port state callback function  This function is called if a GLOBAL port state change occur.  */
static void mvr_gport_state_change_cb(vtss_isid_t isid, mesa_port_no_t port_no, port_info_t *info)
{
    T_D("enter: Port(%u/Stack:%s)->%s", port_no, info->stack ? "TRUE" : "FALSE", info->link ? "UP" : "DOWN");

    MVR_CRIT_ENTER();
    if (msg_switch_is_master() && !info->stack) {
        if (info->link) {
            mvr_port_status[ipmc_lib_isid_convert(TRUE, isid)][port_no] = TRUE;
        } else {
            mvr_port_status[ipmc_lib_isid_convert(TRUE, isid)][port_no] = FALSE;
        }

        if (ipmc_lib_isid_is_local(isid)) {
            mvr_port_status[VTSS_ISID_LOCAL][port_no] = mvr_port_status[ipmc_lib_isid_convert(TRUE, isid)][port_no];
        }
    }
    MVR_CRIT_EXIT();

    T_D("exit: Port(%u/Stack:%s)->%s", port_no, info->stack ? "TRUE" : "FALSE", info->link ? "UP" : "DOWN");
}

void mvr_port_state_change_cb(mesa_port_no_t port_no, port_info_t *info)
{
    vtss_tick_count_t exe_time_base = vtss_current_time();

    MVR_CRIT_ENTER();
    vtss_mvr_port_state_change_handle(port_no, info);
    MVR_CRIT_EXIT();

    T_D("mvr_port_state_change_cb consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));
}

void mvr_stp_port_state_change_callback(vtss_common_port_t l2port, vtss_common_stpstate_t new_state)
{
    switch_iter_t                     sit;
    vtss_isid_t                       isid, l2_isid;
    mesa_port_no_t                    iport;
    mvr_msg_buf_t                     buf;
    mvr_msg_stp_port_change_set_req_t *msg;
    vtss_tick_count_t                 exe_time_base = vtss_current_time();

    /* Only process it when master and forwarding state */
    if (!msg_switch_is_master() || (new_state != VTSS_COMMON_STPSTATE_FORWARDING)) {
        return;
    }

    /* Continue only when MVR administration enabled. */
    MVR_CRIT_ENTER();
    if (!mvr_running.mvr_conf.mvr_conf_global.mvr_state) {
        MVR_CRIT_EXIT();
        return;
    }
    MVR_CRIT_EXIT();

    isid = VTSS_ISID_GLOBAL;
    T_D("Convert %s to isid and iport", l2port2str(l2port));
    if (!l2port2port(l2port, &isid, &iport)) {
        T_D("mvr_stp_port_state_change_callback(%s): Ignore non-STP L2 logical interface", l2port2str(l2port));
        return;
    }

    if (!IPMC_LIB_ISID_VALID(isid)) {
        T_D("Failed to get valid ISID(%d) on %s", isid, l2port2str(l2port));
        return;
    }
    if (!IPMC_LIB_ISID_CHECK(isid)) {
        T_D("Failed to translate ISID(%d) on %s", isid, l2port2str(l2port));
        return;
    }
    l2_isid = isid;

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;
        if (!IPMC_LIB_ISID_PASS(l2_isid, isid)) {
            continue;
        }

        mvr_msg_alloc(MVR_MSG_ID_STP_PORT_CHANGE_REQ, &buf, FALSE, TRUE);
        msg = (mvr_msg_stp_port_change_set_req_t *) buf.msg;

        msg->msg_id = MVR_MSG_ID_STP_PORT_CHANGE_REQ;
        msg->isid = isid;
        msg->port = iport;
        msg->new_state = new_state;
        mvr_msg_tx(&buf, isid, sizeof(*msg));
    }

    T_D("mvr_stp_port_state_change_callback consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));
}

typedef struct {
    BOOL st;
    u8   chg[VTSS_PORT_BF_SIZE];
    u16  pvid[VTSS_MAX_PORTS_LEGACY_CONSTANT_USE_CAPARRAY_INSTEAD];
} mvr_vlan_port_chg_t;
static mvr_vlan_port_chg_t  mvr_vlan_port_chg_buf[VTSS_ISID_END];

static void mvr_vlan_port_change_callback(vtss_isid_t isid, mesa_port_no_t iport, const vtss_appl_vlan_port_detailed_conf_t *new_conf)
{
    vtss_isid_t isidx;

    /*
        If called back on the master, there is no guarantee that the change has actually taken place in H/W.
        If called back on the local switch, changes are already pushed to H/W.

        When called back on the local switch #isid is VTSS_ISID_LOCAL.
        When called back on the master, #isid is a legal ISID (VTSS_ISID_START ~ VTSS_ISID_END).

        MVR chooses "called back on the master" since VLAN should be treated as centralized.
    */

    if (!mvrReady || !new_conf || !msg_switch_is_master() ||
        ((isidx = ipmc_lib_isid_convert(TRUE, isid)) == VTSS_ISID_UNKNOWN)) {
        return;
    }

    MVR_CRIT_ENTER();
    mvr_vlan_port_chg_buf[isidx].st = TRUE;
    VTSS_PORT_BF_SET(mvr_vlan_port_chg_buf[isidx].chg, iport, TRUE);
    mvr_vlan_port_chg_buf[isidx].pvid[iport] = new_conf->pvid;
    MVR_CRIT_EXIT();

    mvr_sm_event_set(MVR_EVENT_VLAN_PORT_CHG);
}

static void _mvr_vlan_port_change_handler(void)
{
    switch_iter_t              sit;
    vtss_isid_t                isid;
    port_iter_t                pit;
    mesa_port_no_t             iport;
    u8                         i, active_cnt;
    mesa_vid_t                 *uvid_chg;
    vtss_appl_vlan_port_conf_t *vlan_port_conf;
    mvr_conf_intf_role_t       *p;
    mvr_conf_port_role_t       *q;
    mesa_rc                    rc;
    vtss_tick_count_t          exe_time_base;

    if (!msg_switch_is_master() ||
        !IPMC_MEM_SYSTEM_MTAKE(vlan_port_conf, sizeof(vtss_appl_vlan_port_conf_t))) {
        return;
    }
    if (!IPMC_MEM_SYSTEM_MTAKE(uvid_chg, sizeof(mesa_vid_t) * VTSS_MAX_PORTS_LEGACY_CONSTANT_USE_CAPARRAY_INSTEAD)) {
        IPMC_MEM_SYSTEM_MGIVE(vlan_port_conf);
        return;
    }

    exe_time_base = vtss_current_time();

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        /* always Read&Clear mvr_vlan_port_chg_buf[isid] */
        MVR_CRIT_ENTER();
        if (!mvr_vlan_port_chg_buf[isid].st) {
            memset(&mvr_vlan_port_chg_buf[isid], 0x0, sizeof(mvr_vlan_port_chg_t));
            MVR_CRIT_EXIT();
            continue;
        }

        /* prepare the changed uvid buffer for each isid */
        memset(uvid_chg, VTSS_IPMC_VID_NULL, sizeof(mesa_vid_t) * VTSS_MAX_PORTS_LEGACY_CONSTANT_USE_CAPARRAY_INSTEAD);
        (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
        while (port_iter_getnext(&pit)) {
            iport = pit.iport;
            if (VTSS_PORT_BF_GET(mvr_vlan_port_chg_buf[isid].chg, iport)) {
                active_cnt = 0;
                p = &mvr_running.mvr_conf.mvr_conf_role[ipmc_lib_isid_convert(TRUE, isid)];
                for (i = 0; i < MVR_VLAN_MAX; i++) {
                    q = &p->intf[i];
                    if (!q->valid) {
                        continue;
                    }

                    if (q->ports[iport] == MVR_PORT_ROLE_SOURC) {
                        ++active_cnt;
                    }
                }

                if (active_cnt > 1) {
                    uvid_chg[iport] = mvr_vlan_port_chg_buf[isid].pvid[iport];
                }
            }
        }

        memset(&mvr_vlan_port_chg_buf[isid], 0x0, sizeof(mvr_vlan_port_chg_t));
        MVR_CRIT_EXIT();

        (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
        while (port_iter_getnext(&pit)) {
            iport = pit.iport;
            if ((uvid_chg[iport] == VTSS_IPMC_VID_NULL) ||
                (vlan_mgmt_port_conf_get(isid, iport, vlan_port_conf, MVR_VLAN_TYPE, TRUE) != VTSS_RC_OK)) {
                continue;
            }

            if (vlan_port_conf->hybrid.untagged_vid != uvid_chg[iport]) {
                vlan_port_conf->hybrid.untagged_vid  = uvid_chg[iport];
                if ((rc = vlan_mgmt_port_conf_set(isid, iport, vlan_port_conf, MVR_VLAN_TYPE)) != VTSS_RC_OK) {
                    T_D("Failure in setting VLAN port conf on isid %u port %u (%s)", isid, iport, error_txt(rc));
                }
            }
        }
    }

    IPMC_MEM_SYSTEM_MGIVE(uvid_chg);
    IPMC_MEM_SYSTEM_MGIVE(vlan_port_conf);
    T_D("_mvr_vlan_port_change_handler consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));
}

static void _mvr_vlan_warning_handler(mesa_vid_t vid)
{
    switch_iter_t          sit;
    vtss_isid_t            isid;
    port_iter_t            pit;
    u32                    idx, iport;
    BOOL                   got_warn;
    vtss_appl_vlan_entry_t vlan_conf;
    mvr_conf_intf_role_t   *q;
    mvr_conf_port_role_t   *r;

    if (!msg_switch_is_master()) {
        return;
    }

    got_warn = FALSE;
    (void)switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;
        vlan_conf.vid = vid;

        if (vtss_appl_vlan_get(isid, vid, &vlan_conf, FALSE, VTSS_APPL_VLAN_USER_STATIC) != VTSS_RC_OK) {
            continue;
        }

        /* skip stacking ports */
        (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
        while (port_iter_getnext(&pit)) {
            iport = pit.iport;
            if (vlan_conf.ports[iport] == 0) {
                continue;
            }

            /* Warn if this VLAN port is overlapped with same MVR VID source port */
            MVR_CRIT_ENTER();
            q = &mvr_running.mvr_conf.mvr_conf_role[ipmc_lib_isid_convert(TRUE, isid)];
            for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
                r = &q->intf[idx];

                if (r->valid && (r->vid == vid)) {
                    if (r->ports[iport] == MVR_PORT_ROLE_SOURC) {
                        got_warn = TRUE;
                    }

                    break;
                }
            }
            MVR_CRIT_EXIT();

            if (got_warn) {
                break;
            }
        }

        if (got_warn) {
            T_W("Please adjust the management VLAN ports overlapped with MVR source ports!");
            break;
        }
    }
}

static BOOL mvr_sm_thread_suspend = FALSE;
static BOOL mvr_sm_thread_done_init = FALSE;

static void mvr_sm_thread(vtss_addrword_t data)
{
    vtss_flag_value_t    events;
    vtss_tick_count_t    exe_time_base;
    BOOL                 i_am_suspend, sync_mgmt_flag, sync_mgmt_done, mvr_link = FALSE;
    switch_iter_t        sit;
    port_iter_t          pit;
    u32                  idx, ticks = 0, ticks_overflow = 0, delay_cnt = 0;
    ipmc_lib_mgmt_info_t info, *sys_mgmt = &info;
    vtss_mvr_interface_t *sm_thread_intf, *sm_thread_intf_bak;
#if MVR_THREAD_EXE_SUPP
    vtss_tick_count_t    last_exe_time;
#endif /* MVR_THREAD_EXE_SUPP */

    T_D("enter mvr_sm_thread");

    // Wait until INIT_CMD_INIT is complete.
    msg_wait(MSG_WAIT_UNTIL_INIT_DONE, VTSS_MODULE_ID_MVR);

    MVR_CRIT_ENTER();
    vtss_flag_init(&mvr_running.vlan_entry_flags);
    vtss_flag_init(&mvr_running.grp_entry_flags);
    vtss_flag_init(&mvr_running.grp_nxt_entry_flags);

    /* Initialize running data structure */
    vtss_mvr_init();
    /* Initialize MSG-RX */
    (void) mvr_stacking_register();
    MVR_CRIT_EXIT();

    /* wait for IP task/stack is ready */
    T_I("mvr_sm_thread init delay start");
    while (!mvr_link && (delay_cnt < MVR_THREAD_START_DELAY_CNT)) {

        if (delay_cnt > MVR_THREAD_MIN_DELAY_CNT) {
            (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
            while (port_iter_getnext(&pit)) {
                if (mvr_port_status[VTSS_ISID_LOCAL][pit.iport]) {
                    mvr_link = TRUE;
                    break;
                }
            }
        }

        if (!mvr_link) {
            VTSS_OS_MSLEEP(MVR_THREAD_SECOND_DEF);
            delay_cnt++;
        }
    }
    T_I("mvr_sm_thread init delay done");

    /* Initialize Periodical Wakeup Timer  */
    mvr_sm_timer.set_repeat(true);
    mvr_sm_timer.set_period(vtss::milliseconds(MVR_THREAD_TICK_TIME));
    mvr_sm_timer.callback = mvr_sm_timer_isr;
    mvr_sm_timer.modid = VTSS_MODULE_ID_MVR;
    if (vtss_timer_start(&mvr_sm_timer) != VTSS_RC_OK) {
        T_D("vtss_timer_start failed");
    }

    if (!IPMC_MEM_SYSTEM_MTAKE(sm_thread_intf, sizeof(vtss_mvr_interface_t))) {
        T_E("MVR sm_thread_intf allocation failure!");
    }
    sm_thread_intf_bak = sm_thread_intf;

    T_D("mvr_sm_thread start");
    mvrReady = TRUE;
    sync_mgmt_flag = TRUE;
    MVR_CRIT_ENTER();
    i_am_suspend = mvr_sm_thread_suspend;
    mvr_sm_thread_done_init = mvrReady;
    MVR_CRIT_EXIT();

#if MVR_THREAD_EXE_SUPP
    last_exe_time = vtss_current_time();
#endif /* MVR_THREAD_EXE_SUPP */
    while (mvrReady && sm_thread_intf && sys_mgmt) {
        events = vtss_flag_wait(&mvr_sm_events, MVR_EVENT_ANY,
                                VTSS_FLAG_WAITMODE_OR_CLR);

        if (events & MVR_EVENT_MASTER_UP) {
            sync_mgmt_flag = TRUE;
        }
        if (events & MVR_EVENT_MASTER_DOWN) {
            sync_mgmt_flag = FALSE;
        }

        if (events & MVR_EVENT_PKT_HANDLER) {
            BOOL    mvr_mode;

            MVR_CRIT_ENTER();
            mvr_mode = mvr_running.mvr_conf.mvr_conf_global.mvr_state;
            MVR_CRIT_EXIT();

            if (mvr_mode) {
                (void) ipmc_lib_packet_register(IPMC_OWNER_MVR, ipmcmvr_rx_packet_callback);
            } else {
                (void) ipmc_lib_packet_unregister(IPMC_OWNER_MVR);
            }
        }

        if (events & MVR_EVENT_SWITCH_DEL) {
            vtss_isid_t isid_del;

            T_D("MVR_EVENT_SWITCH_DEL");

            exe_time_base = vtss_current_time();

            MVR_CRIT_ENTER();

            (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID);
            while (switch_iter_getnext(&sit)) {
                isid_del = sit.isid;

                if (!(mvr_switch_event_value[ipmc_lib_isid_convert(TRUE, isid_del)] & MVR_EVENT_VALUE_SW_DEL)) {
                    continue;
                }

                mvr_switch_event_value[ipmc_lib_isid_convert(TRUE, isid_del)] &= ~MVR_EVENT_VALUE_SW_DEL;
                if (ipmc_lib_isid_is_local(isid_del)) {
                    mvr_switch_event_value[VTSS_ISID_LOCAL] &= ~MVR_EVENT_VALUE_SW_DEL;
                }
            }

            MVR_CRIT_EXIT();

            T_D("MVR_EVENT_SWITCH_DEL consumes %u ticks per time", (u32)(vtss_current_time() - exe_time_base));
        }

        if (events & MVR_EVENT_SWITCH_ADD) {
            vtss_isid_t             isid_add;
            mvr_msg_buf_t           msgbuf;
            mvr_msg_purge_req_t     *purgemsg;
            u32                     isid_set, i;
            mvr_conf_intf_entry_t   *p;
            mvr_conf_port_role_t    *q;
            mesa_rc                 chk_rc;
            ipmc_operation_action_t action;
            ipmc_intf_entry_t       *ifp;
            mvr_mgmt_interface_t    interface;

            T_D("MVR_EVENT_SWITCH_ADD");

            exe_time_base = vtss_current_time();

            sync_mgmt_done = FALSE;
            if (!ipmc_lib_system_mgmt_info_cpy(sys_mgmt)) {
                sync_mgmt_done = TRUE;  /* Not Ready Yet! */
            }

            (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID);
            while (switch_iter_getnext(&sit)) {
                isid_add = sit.isid;

                MVR_CRIT_ENTER();
                isid_set = mvr_switch_event_value[ipmc_lib_isid_convert(TRUE, isid_add)] & MVR_EVENT_VALUE_SW_ADD;
                MVR_CRIT_EXIT();

                if (isid_set == 0) {
                    continue;
                }

                T_D("MVR_EVENT_SWITCH_ADD (%d/%u)",
                    isid_add,
                    mvr_switch_event_value[ipmc_lib_isid_convert(TRUE, isid_add)]);

                _mvr_stacking_reset_port_type(isid_add);
                /* Initial MVR Protocol Database */

                if (ipmc_lib_isid_is_local(isid_add)) {
                    MVR_CRIT_ENTER();
                    (void)vtss_mvr_purge(isid_add, FALSE, FALSE);
                    MVR_CRIT_EXIT();
                } else {
                    mvr_msg_alloc(MVR_MSG_ID_GLOBAL_PURGE_REQ, &msgbuf, FALSE, TRUE);
                    purgemsg = (mvr_msg_purge_req_t *)msgbuf.msg;

                    purgemsg->isid = isid_add;
                    purgemsg->msg_id = MVR_MSG_ID_GLOBAL_PURGE_REQ;
                    mvr_msg_tx(&msgbuf, isid_add, sizeof(*purgemsg));
                }

                (void) mvr_stacking_set_fastleave_port(isid_add);

                for (idx = 0; idx < MVR_VLAN_MAX; idx++) {
                    MVR_CRIT_ENTER();
                    p = &mvr_running.mvr_conf.mvr_conf_vlan[idx];
                    if (!p->valid) {
                        MVR_CRIT_EXIT();
                        continue;
                    }

                    memcpy(&interface.intf, p, sizeof(mvr_conf_intf_entry_t));
                    memset(&interface.role, 0x0, sizeof(mvr_conf_port_role_t));
                    for (i = 0; i < MVR_VLAN_MAX; i++) {
                        q = &mvr_running.mvr_conf.mvr_conf_role[ipmc_lib_isid_convert(TRUE, isid_add)].intf[i];
                        if (q->valid && (q->vid == p->vid)) {
                            memcpy(&interface.role, q, sizeof(mvr_conf_port_role_t));
                            break;
                        }
                    }
                    interface.vid = p->vid;
                    MVR_CRIT_EXIT();

                    ifp = &sm_thread_intf->basic;
                    ifp->ipmc_version = IPMC_IP_VERSION_IGMP;
                    ifp->param.vid = interface.vid;
                    if (ipmc_lib_isid_is_local(isid_add)) {
                        MVR_CRIT_ENTER();
                        chk_rc = vtss_mvr_interface_get(sm_thread_intf);
                        MVR_CRIT_EXIT();
                    } else {
                        chk_rc = VTSS_RC_ERROR;
                    }

                    if (chk_rc != VTSS_OK) {
                        action = IPMC_OP_ADD;
                    } else {
                        action = IPMC_OP_UPD;
                    }

                    MVR_CRIT_ENTER();
                    _mvr_mgmt_transit_intf(isid_add, isid_add, sm_thread_intf, &interface, IPMC_IP_VERSION_IGMP);
                    MVR_CRIT_EXIT();
                    (void) mvr_stacking_set_intf_entry(isid_add, action, sm_thread_intf, IPMC_IP_VERSION_IGMP);

                    MVR_CRIT_ENTER();
                    _mvr_mgmt_transit_intf(isid_add, isid_add, sm_thread_intf, &interface, IPMC_IP_VERSION_MLD);
                    MVR_CRIT_EXIT();
                    (void) mvr_stacking_set_intf_entry(isid_add, action, sm_thread_intf, IPMC_IP_VERSION_MLD);
                }

                (void) mvr_stacking_set_mode(isid_add);

                if (!sync_mgmt_done) {
                    (void) mvr_stacking_sync_mgmt_conf(isid_add, sys_mgmt);
                }

                MVR_CRIT_ENTER();
                mvr_switch_event_value[ipmc_lib_isid_convert(TRUE, isid_add)] &= ~MVR_EVENT_VALUE_SW_ADD;
                if (ipmc_lib_isid_is_local(isid_add)) {
                    mvr_switch_event_value[VTSS_ISID_LOCAL] &= ~MVR_EVENT_VALUE_SW_ADD;
                }
                MVR_CRIT_EXIT();
            }

            T_D("MVR_EVENT_SWITCH_ADD consumes %u ticks per time", (u32)(vtss_current_time() - exe_time_base));
        }

        if (events & MVR_EVENT_THREAD_SUSPEND) {
            MVR_CRIT_ENTER();
            i_am_suspend = mvr_sm_thread_suspend = TRUE;
            MVR_CRIT_EXIT();
        }

        if (events & MVR_EVENT_THREAD_RESUME) {
            MVR_CRIT_ENTER();
            i_am_suspend = mvr_sm_thread_suspend = FALSE;
            MVR_CRIT_EXIT();
        }

        if (i_am_suspend) {
            if (events & MVR_EVENT_SM_TIME_WAKEUP) {
                (void) ipmc_lib_calc_thread_tick(&ticks, 1, MVR_THREAD_TIME_UNIT_BASE, &ticks_overflow);
            }

            if (events & MVR_EVENT_VLAN_PORT_CHG) {
                _mvr_vlan_port_change_handler();
            }

            sm_thread_intf = sm_thread_intf_bak;

            continue;
        }

        sync_mgmt_done = FALSE;
        if (events & MVR_EVENT_SM_TIME_WAKEUP) {
            u8  exe_round = 1;
#if MVR_THREAD_EXE_SUPP
            u32 supply_ticks = 0, diff_exe_time = ipmc_lib_diff_u32_wrap_around((u32)last_exe_time, (u32)vtss_current_time());

            if (VTSS_OS_TICK2MSEC(diff_exe_time) > 2 * MVR_THREAD_TICK_TIME) {
                supply_ticks = VTSS_OS_TICK2MSEC(diff_exe_time) / MVR_THREAD_TICK_TIME - 1;
            }

            if (supply_ticks > 0) {
                exe_round += ipmc_lib_calc_thread_tick(&ticks, supply_ticks, MVR_THREAD_TIME_UNIT_BASE, &ticks_overflow);
            }

            last_exe_time = vtss_current_time();
#endif /* MVR_THREAD_EXE_SUPP */

            if (msg_switch_is_master() && ipmc_lib_system_mgmt_info_chg(sys_mgmt)) {
                sync_mgmt_done = (mvr_stacking_sync_mgmt_conf(VTSS_ISID_GLOBAL, sys_mgmt) == VTSS_OK);
            }

            if (!(ticks % MVR_THREAD_TIME_UNIT_BASE)) {
                for (; exe_round > 0; exe_round--) {
                    if (msg_switch_is_master() && sync_mgmt_flag) {
                        if (!sync_mgmt_done) {
                            if (ipmc_lib_system_mgmt_info_cpy(sys_mgmt)) {
                                sync_mgmt_flag = (mvr_stacking_sync_mgmt_conf(VTSS_ISID_GLOBAL, sys_mgmt) != VTSS_OK);
                            }
                        } else {
                            sync_mgmt_flag = FALSE;
                        }
                    }

                    MVR_CRIT_ENTER();
                    T_N("mvr_sm_thread: %dMSEC-TICKS(%u | %u)", MVR_THREAD_TICK_TIME, ticks_overflow, ticks);
                    exe_time_base = vtss_current_time();

                    vtss_mvr_tick_gen();

                    if (mvr_running.mvr_conf.mvr_conf_global.mvr_state) {
                        memset(sm_thread_intf, 0x0, sizeof(vtss_mvr_interface_t));
                        while (vtss_mvr_interface_get_next(sm_thread_intf) == VTSS_OK) {
                            vtss_mvr_tick_intf_tmr(&sm_thread_intf->basic);
                            if (vtss_mvr_interface_set(IPMC_OP_SET, sm_thread_intf) != VTSS_OK) {
                                T_D("vtss_mvr_interface_set(IPMC_OP_SET) Failed!");
                            }
                        }

                        vtss_mvr_tick_intf_rxmt();
                        vtss_mvr_tick_group_tmr();
                    }

                    T_N("MVR_EVENT_SM_TIME_WAKEUP consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));
                    MVR_CRIT_EXIT();
                }
            }

            if (!(ticks % MVR_THREAD_TIME_MISC_BASE)) {
                BOOL                next;
                ipmc_mgmt_ipif_t    sys_intf;

                memset(sm_thread_intf, 0x0, sizeof(vtss_mvr_interface_t));
                MVR_CRIT_ENTER();
                next = (vtss_mvr_interface_get_next(sm_thread_intf) == VTSS_OK);
                MVR_CRIT_EXIT();
                while (next) {
                    if (ipmc_lib_get_system_mgmt_intf(&sm_thread_intf->basic, &sys_intf)) {
                        _mvr_vlan_warning_handler(ipmc_mgmt_intf_vidx(&sys_intf));
                    }

                    MVR_CRIT_ENTER();
                    next = (vtss_mvr_interface_get_next(sm_thread_intf) == VTSS_OK);
                    MVR_CRIT_EXIT();
                }
            }

            (void) ipmc_lib_calc_thread_tick(&ticks, 1, MVR_THREAD_TIME_UNIT_BASE, &ticks_overflow);
        }

        if (events & MVR_EVENT_VLAN_PORT_CHG) {
            _mvr_vlan_port_change_handler();
        }

        sm_thread_intf = sm_thread_intf_bak;
    }

    IPMC_MEM_SYSTEM_MGIVE(sm_thread_intf_bak);
    mvrReady = FALSE;
    T_W("exit mvr_sm_thread");
}

mesa_rc mvr_mgmt_sm_thread_resume(void)
{
    BOOL    i_can_go;

    MVR_CRIT_ENTER();
    i_can_go = mvr_sm_thread_suspend;
    MVR_CRIT_EXIT();

    if (!i_can_go) {
        return VTSS_RC_OK;
    }

    i_can_go = FALSE;
    while (!i_can_go) {
        MVR_CRIT_ENTER();
        i_can_go = mvr_sm_thread_done_init;
        MVR_CRIT_EXIT();

        if (!i_can_go) {
            VTSS_OS_MSLEEP(333);
        }
    }

    mvr_sm_event_set(MVR_EVENT_THREAD_RESUME);
    return VTSS_RC_OK;
}

mesa_rc mvr_mgmt_sm_thread_suspend(void)
{
    BOOL    i_can_go;

    MVR_CRIT_ENTER();
    i_can_go = mvr_sm_thread_suspend;
    MVR_CRIT_EXIT();

    if (i_can_go) {
        return VTSS_RC_OK;
    }

    while (!i_can_go) {
        MVR_CRIT_ENTER();
        i_can_go = mvr_sm_thread_done_init;
        MVR_CRIT_EXIT();

        if (!i_can_go) {
            VTSS_OS_MSLEEP(333);
        }
    }

    mvr_sm_event_set(MVR_EVENT_THREAD_SUSPEND);
    i_can_go = FALSE;
    while (!i_can_go) {
        MVR_CRIT_ENTER();
        i_can_go = mvr_sm_thread_suspend;
        MVR_CRIT_EXIT();

        if (!i_can_go) {
            VTSS_OS_MSLEEP(333);
        }
    }

    return VTSS_RC_OK;
}

/****************************************************************************/
/*  Initialization functions                                                */
/****************************************************************************/
#ifdef VTSS_SW_OPTION_PRIVATE_MIB
VTSS_PRE_DECLS void ipmc_mvr_mib_init(void);
#endif /* VTSS_SW_OPTION_PRIVATE_MIB */

#ifdef VTSS_SW_OPTION_JSON_RPC
VTSS_PRE_DECLS void vtss_appl_mvr_json_init(void);
#endif

extern "C" int mvr_icli_cmd_register();

mesa_rc mvr_init(vtss_init_data_t *data)
{
    mvr_msg_id_t      idx;
    vtss_isid_t       isid = data->isid;
    BOOL              run_time_action, mvr_mode;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    switch (data->cmd) {
    case INIT_CMD_EARLY_INIT:
        /* Initialize and register trace ressources */
        VTSS_TRACE_REG_INIT(&mvr_trace_reg, mvr_trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&mvr_trace_reg);
        break;

    case INIT_CMD_INIT:
        for (idx = MVR_MSG_ID_GLOBAL_SET_REQ; idx < MVR_MSG_MAX_ID; idx++) {
            switch (idx) {
            case MVR_MSG_ID_GLOBAL_SET_REQ:
                mvr_running.msize[idx] = sizeof(mvr_msg_global_set_req_t);
                break;
            case MVR_MSG_ID_GLOBAL_PURGE_REQ:
                mvr_running.msize[idx] = sizeof(mvr_msg_purge_req_t);
                break;
            case MVR_MSG_ID_VLAN_ENTRY_GET_REQ:
            case MVR_MSG_ID_VLAN_GROUP_ENTRY_WALK_REQ:
            case MVR_MSG_ID_GROUP_SRCLIST_WALK_REQ:
                mvr_running.msize[idx] = sizeof(mvr_msg_vlan_entry_get_req_t);
                break;
            case MVR_MSG_ID_GROUP_ENTRY_GET_REQ:
            case MVR_MSG_ID_GROUP_ENTRY_GETNEXT_REQ:
                mvr_running.msize[idx] = sizeof(mvr_msg_group_entry_itr_req_t);
                break;
            case MVR_MSG_ID_VLAN_ENTRY_GET_REP:
                mvr_running.msize[idx] = sizeof(mvr_msg_vlan_entry_get_rep_t);
                break;
            case MVR_MSG_ID_VLAN_GROUP_ENTRY_WALK_REP:
                mvr_running.msize[idx] = sizeof(mvr_msg_vlan_group_entry_get_rep_t);
                break;
            case MVR_MSG_ID_GROUP_ENTRY_GET_REP:
            case MVR_MSG_ID_GROUP_ENTRY_GETNEXT_REP:
                mvr_running.msize[idx] = sizeof(mvr_msg_group_entry_itr_rep_t);
                break;
            case MVR_MSG_ID_GROUP_SRCLIST_WALK_REP:
                mvr_running.msize[idx] = sizeof(mvr_msg_group_srclist_get_rep_t);
                break;
            case MVR_MSG_ID_PORT_FAST_LEAVE_SET_REQ:
                mvr_running.msize[idx] = sizeof(mvr_msg_port_fast_leave_set_req_t);
                break;
            case MVR_MSG_ID_STAT_COUNTER_CLEAR_REQ:
                mvr_running.msize[idx] = sizeof(mvr_msg_stat_counter_clear_req_t);
                break;
            case MVR_MSG_ID_STP_PORT_CHANGE_REQ:
                mvr_running.msize[idx] = sizeof(mvr_msg_stp_port_change_set_req_t);
                break;
            case MVR_MSG_ID_VLAN_ENTRY_SET_REQ:
            default:
                /* Give the MAX */
                mvr_running.msize[idx] = sizeof(mvr_msg_vlan_set_req_t);
                break;
            }

            VTSS_MALLOC_CAST(mvr_running.msg[idx], mvr_running.msize[idx]);
            if (mvr_running.msg[idx] == NULL) {
                T_W("MVR_ASSERT(INIT_CMD_INIT)");
                for (;;) {}
            }
        }

        /* Create semaphore for critical regions */
        critd_init(&mvr_running.crit, "MVR_global.crit", VTSS_MODULE_ID_MVR, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
        MVR_CRIT_EXIT();

        critd_init(&mvr_running.get_crit, "MVR_global.get_crit", VTSS_MODULE_ID_MVR, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
        MVR_GET_CRIT_EXIT();

        (void)ipmc_lib_common_init();
        /* Initialized RX semaphore */
        (void)ipmc_lib_packet_init();

#ifdef VTSS_SW_OPTION_ICFG
        if (ipmc_mvr_icfg_init() != VTSS_OK) {
            T_E("ipmc_mvr_icfg_init failed!");
        }
#endif /* VTSS_SW_OPTION_ICFG */

#ifdef VTSS_SW_OPTION_PRIVATE_MIB
        ipmc_mvr_mib_init();    /* Register IPMC MVR private mib */
#endif /* VTSS_SW_OPTION_PRIVATE_MIB */
#ifdef VTSS_SW_OPTION_JSON_RPC
        vtss_appl_mvr_json_init();
#endif
        mvr_icli_cmd_register();

        /* Initialize message buffers */
        vtss_sem_init(&mvr_running.request.sem, 1);
        vtss_sem_init(&mvr_running.reply.sem, 1);

        memset(mvr_switch_event_value, 0x0, sizeof(mvr_switch_event_value));

        /* Initialize MVR-EVENT groups */
        vtss_flag_init(&mvr_sm_events);
#if 0 /* etliang */
        vtss_flag_init(&mvr_db_events);
#endif /* etliang */

        memset(mvr_port_status, 0x0, sizeof(mvr_port_status));

        memset(mvr_vlan_port_chg_buf, 0x0, sizeof(mvr_vlan_port_chg_buf));
        /* Register for VLAN port configuration changes */
        vlan_port_conf_change_register(VTSS_MODULE_ID_MVR, mvr_vlan_port_change_callback, TRUE);

        vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
                           mvr_sm_thread,
                           0,
                           "IPMC_MVR",
                           nullptr,
                           0,
                           &mvr_sm_thread_handle,
                           &mvr_sm_thread_block);

        T_I("MVR-INIT_CMD_INIT consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));

        break;
    case INIT_CMD_START:
        T_I("START: ISID->%d", isid);

        (void)ipmc_lib_packet_resume();
        (void) port_change_register(VTSS_MODULE_ID_MVR, mvr_port_state_change_cb);
        (void) l2_stp_state_change_register(mvr_stp_port_state_change_callback);
        /* Register for Port GLOBAL change callback */
        (void) port_global_change_register(VTSS_MODULE_ID_MVR, mvr_gport_state_change_cb);

        T_D("MVR-INIT_CMD_START consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

        break;
    case INIT_CMD_CONF_DEF:
        T_I("CONF_DEF: ISID->%d", isid);

        MVR_CRIT_ENTER();
        run_time_action = mvrReady;
        mvr_mode = mvr_running.mvr_conf.mvr_conf_global.mvr_state;
        MVR_CRIT_EXIT();

        if (msg_switch_is_master()) {
            if (run_time_action) {
                switch_iter_t          sit;
                mvr_msg_buf_t          msgbuf;
                mvr_msg_purge_req_t    *purgemsg;
                BOOL                   flag;
                vtss_appl_vlan_entry_t vlan_entry;
                mvr_conf_intf_entry_t  intf;

                memset(&intf, 0x0, sizeof(mvr_conf_intf_entry_t));
                MVR_CRIT_ENTER();
                flag = mvr_get_next_vlan_conf_entry(&intf);
                MVR_CRIT_EXIT();
                while (flag) {
                    memset(&vlan_entry, 0x0, sizeof(vtss_appl_vlan_entry_t));
                    if (vtss_appl_vlan_get(VTSS_ISID_GLOBAL, intf.vid, &vlan_entry, FALSE, VTSS_APPL_VLAN_USER_ALL) == VTSS_OK) {
                        (void)vlan_mgmt_vlan_del(VTSS_ISID_GLOBAL, intf.vid, MVR_VLAN_TYPE);
                    }

                    MVR_CRIT_ENTER();
                    flag = mvr_get_next_vlan_conf_entry(&intf);
                    MVR_CRIT_EXIT();
                }

                if ((isid != VTSS_ISID_GLOBAL) && !ipmc_lib_isid_is_local(isid)) {
                    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID);
                    while (switch_iter_getnext(&sit)) {
                        if (sit.isid != isid) {
                            continue;
                        }

                        _mvr_stacking_reset_port_type(isid);

                        /* Initial MVR Protocol Database */
                        mvr_msg_alloc(MVR_MSG_ID_GLOBAL_PURGE_REQ, &msgbuf, FALSE, TRUE);
                        purgemsg = (mvr_msg_purge_req_t *)msgbuf.msg;

                        purgemsg->isid = isid;
                        purgemsg->msg_id = MVR_MSG_ID_GLOBAL_PURGE_REQ;
                        mvr_msg_tx(&msgbuf, isid, sizeof(*purgemsg));
                    }
                }

                if (ipmc_lib_isid_is_local(isid)) {
                    _mvr_stacking_reset_port_type(isid);
                }

                if (isid == VTSS_ISID_GLOBAL) {
                    MVR_CRIT_ENTER();
                    (void)vtss_mvr_purge(VTSS_ISID_LOCAL, FALSE, FALSE);
                    MVR_CRIT_EXIT();

                    /* Reset stack configuration */
                    mvr_conf_default();

                    if (mvr_mode) {
                        mvr_sm_event_set(MVR_EVENT_PKT_HANDLER);
                    }
                }
            } else {
                /* Reset stack configuration */
                mvr_conf_default();

                if (mvr_mode) {
                    mvr_sm_event_set(MVR_EVENT_PKT_HANDLER);
                }
            }
        } else {
            /* Reset stack configuration */
            mvr_conf_default();

            if (mvr_mode) {
                mvr_sm_event_set(MVR_EVENT_PKT_HANDLER);
            }
        }

        T_D("MVR-INIT_CMD_CONF_DEF consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

        break;
    case INIT_CMD_MASTER_UP:
        T_I("MASTER_UP: ISID->%d", isid);

        /* Read configuration */
        mvr_conf_default();
        mvr_sm_event_set(MVR_EVENT_MASTER_UP);

        T_D("MVR-INIT_CMD_MASTER_UP consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));

        break;
    case INIT_CMD_MASTER_DOWN:
        T_I("MASTER_DOWN: ISID->%d", isid);

        mvr_sm_event_set(MVR_EVENT_MASTER_DOWN);

        T_D("MVR-INIT_CMD_MASTER_DOWN consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

        break;
    case INIT_CMD_SWITCH_ADD:
        T_I("SWITCH_ADD: ISID->%d", isid);

        MVR_CRIT_ENTER();
        mvr_switch_event_value[ipmc_lib_isid_convert(TRUE, isid)] |= MVR_EVENT_VALUE_SW_ADD;
        if (ipmc_lib_isid_is_local(isid)) {
            mvr_switch_event_value[VTSS_ISID_LOCAL] |= MVR_EVENT_VALUE_SW_ADD;
        }
        MVR_CRIT_EXIT();

        mvr_sm_event_set(MVR_EVENT_SWITCH_ADD);


        T_D("MVR-INIT_CMD_SWITCH_ADD consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

        break;
    case INIT_CMD_SWITCH_DEL:
        T_I("SWITCH_DEL: ISID->%d", isid);

        MVR_CRIT_ENTER();
        mvr_switch_event_value[ipmc_lib_isid_convert(TRUE, isid)] |= MVR_EVENT_VALUE_SW_DEL;
        if (ipmc_lib_isid_is_local(isid)) {
            mvr_switch_event_value[VTSS_ISID_LOCAL] |= MVR_EVENT_VALUE_SW_DEL;
        }
        MVR_CRIT_EXIT();

        mvr_sm_event_set(MVR_EVENT_SWITCH_DEL);

        T_D("MVR-INIT_CMD_SWITCH_DEL consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

        break;
    default:
        break;
    }

    return 0;
}

/*****************************************************************************
    Public API section for IPMC MVR
    from vtss_appl/include/vtss/appl/ipmc_mvr.h
*****************************************************************************/
#include "vtss_common_iterator.hxx"

static mesa_rc
_vtss_appl_ipmc_mvr_vlan_status_get(
    const ipmc_ip_version_t                     version,
    const vtss_ifindex_t                        *const ifidx,
    void                                        *const entry
)
{
    vtss_ifindex_elm_t              ife;
    mvr_mgmt_interface_t            mvrif;
    ipmc_prot_intf_entry_param_t    intf;
    BOOL                            ctrl;
    mesa_rc                         rc = VTSS_RC_ERROR;

    if (!ifidx || !entry ||
        vtss_ifindex_decompose(*ifidx, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid Input!");
        return rc;
    }

    memset(&mvrif, 0x0, sizeof(mvr_mgmt_interface_t));
    mvrif.vid = (mesa_vid_t)ife.ordinal;
    if (mvr_mgmt_get_intf_entry(VTSS_ISID_GLOBAL, &mvrif) != VTSS_RC_OK) {
        T_D("No such VIDX %u in MVR", mvrif.vid);
        return rc;
    }

    memset(&intf, 0x0, sizeof(ipmc_prot_intf_entry_param_t));
    if ((rc = mvr_mgmt_get_mode(&ctrl)) == VTSS_RC_OK) {
        intf.vid = mvrif.vid;
    }
    if (rc == VTSS_RC_OK &&
        (rc = mvr_mgmt_get_intf_info(VTSS_ISID_GLOBAL, version, &intf)) == VTSS_RC_OK) {
        ipmc_querier_sm_t                       *qrier = &intf.querier;
        ipmc_statistics_t                       *stats = &intf.stats;
        vtss_appl_ipmc_mvr_intf_ipv4_status_t   *p = NULL;
        vtss_appl_ipmc_mvr_intf_ipv6_status_t   *q = NULL;
        vtss_appl_ipmc_querier_states_t         qs = VTSS_APPL_IPMC_QUERIER_ACTIVE;

        if (version == IPMC_IP_VERSION_IGMP) {
            p = (vtss_appl_ipmc_mvr_intf_ipv4_status_t *)entry;
        } else {
            q = (vtss_appl_ipmc_mvr_intf_ipv6_status_t *)entry;
        }

        if (p) {
            memset(p, 0x0, sizeof(vtss_appl_ipmc_mvr_intf_ipv4_status_t));
            if (ctrl) {
                mesa_ipv4_t qadr;

                switch (qrier->state) {
                case IPMC_QUERIER_IDLE:
                    qs = VTSS_APPL_IPMC_QUERIER_IDLE;
                    p->querier_expiry_time = qrier->OtherQuerierTimeOut;
                    break;
                case IPMC_QUERIER_INIT:
                    qs = VTSS_APPL_IPMC_QUERIER_INIT;
                    p->query_interval = qrier->timeout;
                    if (qrier->RobustVari < qrier->StartUpCnt) {
                        p->startup_query_count = qrier->RobustVari;
                    } else {
                        p->startup_query_count = qrier->RobustVari - qrier->StartUpCnt;
                    }
                    break;
                default:    /* VTSS_APPL_IPMC_QUERIER_ACTIVE */
                    p->querier_up_time = qrier->QuerierUpTime;
                    p->query_interval = qrier->timeout;
                    break;
                }
                p->querier_state = qs;
                IPMC_LIB_ADRS_6TO4_SET(intf.active_querier, qadr);
                p->active_querier_address = ntohl(qadr);
                p->counter_tx_query = qrier->ipmc_queries_sent;
                p->counter_tx_specific_query = qrier->group_queries_sent;
                p->counter_rx_query = stats->igmp_queries;
                p->counter_rx_v1_join = stats->igmp_v1_membership_join;
                p->counter_rx_v2_join = stats->igmp_v2_membership_join;
                p->counter_rx_v2_leave = stats->igmp_v2_membership_leave;
                p->counter_rx_v3_join = stats->igmp_v3_membership_join;
                p->counter_rx_errors = stats->igmp_error_pkt;
            } else {
                p->querier_state = VTSS_APPL_IPMC_QUERIER_DISABLED;
            }
        }

        if (q) {
            memset(q, 0x0, sizeof(vtss_appl_ipmc_mvr_intf_ipv6_status_t));
            if (ctrl) {
                switch (qrier->state) {
                case IPMC_QUERIER_IDLE:
                    qs = VTSS_APPL_IPMC_QUERIER_IDLE;
                    q->querier_expiry_time = qrier->OtherQuerierTimeOut;
                    break;
                case IPMC_QUERIER_INIT:
                    qs = VTSS_APPL_IPMC_QUERIER_INIT;
                    q->query_interval = qrier->timeout;
                    if (qrier->RobustVari < qrier->StartUpCnt) {
                        q->startup_query_count = qrier->RobustVari;
                    } else {
                        q->startup_query_count = qrier->RobustVari - qrier->StartUpCnt;
                    }
                    break;
                default:    /* VTSS_APPL_IPMC_QUERIER_ACTIVE */
                    q->querier_up_time = qrier->QuerierUpTime;
                    q->query_interval = qrier->timeout;
                    break;
                }
                q->querier_state = qs;
                IPMC_LIB_ADRS_CPY(&q->active_querier_address, &intf.active_querier);
                q->counter_tx_query = qrier->ipmc_queries_sent;
                q->counter_tx_specific_query = qrier->group_queries_sent;
                q->counter_rx_query = stats->mld_queries;
                q->counter_rx_v1_report = stats->mld_v1_membership_report;
                q->counter_rx_v1_done = stats->mld_v1_membership_done;
                q->counter_rx_v2_report = stats->mld_v2_membership_report;
                q->counter_rx_errors = stats->mld_error_pkt;
            } else {
                q->querier_state = VTSS_APPL_IPMC_QUERIER_DISABLED;
            }
        }
    }

    return rc;
}

static mesa_rc
_vtss_appl_ipmc_mvr_group_address_itr(
    const ipmc_ip_version_t                     version,
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const void                                  *const grps_prev,
    void                                        *const grps_next
)
{
    mesa_vid_t                      vidx;
    mesa_ipv6_t                     grpx;
    ipmc_prot_intf_group_entry_t    intf_group_entry;
    i8                              adrString[40];
    mesa_rc                         rc = VTSS_RC_ERROR;

    if (!ifid_next || !grps_next) {
        T_D("Invalid Input!");
        return rc;
    }

    vidx = VTSS_APPL_IPMC_VID_NULL;
    if (ifid_prev && *ifid_prev >= VTSS_IFINDEX_VLAN_OFFSET) {
        vtss_ifindex_elm_t  ife;

        T_D("Get VID from given IfIndex");
        if (vtss_ifindex_decompose(*ifid_prev, &ife) != VTSS_RC_OK ||
            ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
            T_D("Failed to decompose VLAN-IfIndex %u!\n\r", VTSS_IFINDEX_PRINTF_ARG(*ifid_prev));
            return rc;
        }

        vidx = (mesa_vid_t)ife.ordinal;
    }

    IPMC_LIB_ADRS_SET(&grpx, 0x0);
    if (ifid_prev && grps_prev != NULL) {
        if (version == IPMC_IP_VERSION_IGMP) {
            mesa_ipv4_t grp4;

            grp4 = htonl(*((mesa_ipv4_t *)grps_prev));
            IPMC_LIB_ADRS_4TO6_SET(grp4, grpx);
        }
        if (version == IPMC_IP_VERSION_MLD) {
            IPMC_LIB_ADRS_CPY(&grpx, ((mesa_ipv6_t *)grps_prev));
        }
    }

    memset(&intf_group_entry, 0x0, sizeof(ipmc_prot_intf_group_entry_t));
    rc = mvr_mgmt_group_info_get_next(VTSS_ISID_GLOBAL, version, &vidx, &grpx, &intf_group_entry);

    memset(adrString, 0x0, sizeof(adrString));
    T_I("%sFound next VIDX-%u / GRPS-%s",
        rc == VTSS_RC_OK ? "" : "Not",
        intf_group_entry.vid,
        misc_ipv6_txt(&intf_group_entry.group_addr, adrString));
    if (rc == VTSS_RC_OK) {
        /* convert VIDX to IfIndex */
        if (vtss_ifindex_from_vlan(intf_group_entry.vid, ifid_next) != VTSS_RC_OK) {
            T_D("Failed to convert IfIndex from %u!\n\r", vidx);
            return VTSS_RC_ERROR;
        }
        if (version == IPMC_IP_VERSION_IGMP) {
            IPMC_LIB_ADRS_6TO4_SET(intf_group_entry.group_addr, (*((mesa_ipv4_t *)grps_next)));
            *((mesa_ipv4_t *)grps_next) = ntohl(*((mesa_ipv4_t *)grps_next));
        }
        if (version == IPMC_IP_VERSION_MLD) {
            IPMC_LIB_ADRS_CPY(((mesa_ipv6_t *)grps_next), &intf_group_entry.group_addr);
        }
    }

    return rc;
}

/* Refer to MAC sample */
static BOOL mvr_portlist_index_set(u32 i, vtss_port_list_stackable_t *pl)
{
    if (i >= 8 * 128) {
        return false;
    }

    u8 val = 1;
    u32 idx_bit = i & 7;
    u32 idx = i >> 3;
    val <<= idx_bit;
    pl->data[idx] |= val;
    return true;
}

static BOOL mvr_portlist_index_clear(u32 i, vtss_port_list_stackable_t *pl)
{
    if (i >= 8 * 128) {
        return false;
    }

    u8 val = 1;
    u32 idx_bit = i & 7;
    u32 idx = i >> 3;
    val <<= idx_bit;
    pl->data[idx] &= (~val);
    return true;
}

static u32 mvr_isid_port_to_index(vtss_isid_t i,  mesa_port_no_t p)
{
    VTSS_ASSERT(VTSS_PORT_NO_START == 0);
    VTSS_ASSERT(VTSS_ISID_START == 1);
    VTSS_ASSERT(mesa_port_cnt(nullptr) - 1 <= 128);
    return (i - 1) * 128 + iport2uport(p);
}
/* MAC sample END */

static mesa_rc
_vtss_appl_ipmc_mvr_group_address_get(
    const ipmc_ip_version_t                     version,
    const mesa_vid_t                            *const vidx,
    const mesa_ipv6_t                           *const grpx,
    vtss_appl_ipmc_mvr_grp_address_t            *const entry
)
{
    switch_iter_t                   sit;
    port_iter_t                     pit;
    ipmc_prot_intf_group_entry_t    intf_group_entry;
    vtss_isid_t                     isid;
    mesa_port_no_t                  iport;
    mesa_rc                         rc = VTSS_RC_ERROR;

    if (!vidx || !grpx || !entry) {
        T_D("Invalid Input!");
        return rc;
    }

    memset(&entry->member_ports, 0x0, sizeof(vtss_port_list_stackable_t));

    if ((rc = mvr_mgmt_group_info_get(VTSS_ISID_GLOBAL, version, vidx, grpx, &intf_group_entry)) == VTSS_RC_OK) {
        ipmc_group_db_t *grp_db = &intf_group_entry.db;

        entry->hardware_switch = grp_db->asm_in_hw;

        (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
        while (switch_iter_getnext(&sit)) {
            isid = sit.isid;
            if (mvr_mgmt_group_info_get(isid, version, vidx, grpx, &intf_group_entry) != VTSS_RC_OK) {
                continue;
            }

            (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_FRONT);
            while (port_iter_getnext(&pit)) {
                iport = pit.iport;
                if (VTSS_PORT_BF_GET(grp_db->port_mask, iport)) {
                    (void) mvr_portlist_index_set(mvr_isid_port_to_index(isid, iport), &entry->member_ports);
                } else {
                    (void) mvr_portlist_index_clear(mvr_isid_port_to_index(isid, iport), &entry->member_ports);
                }
            }
        }
    }

    return rc;
}

static mesa_rc
_vtss_appl_ipmc_mvr_group_srclist_itr(
    const ipmc_ip_version_t                     version,
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const void                                  *const grps_prev,
    void                                        *const grps_next,
    const vtss_ifindex_t                        *const port_prev,
    vtss_ifindex_t                              *const port_next,
    const void                                  *const adrs_prev,
    void                                        *const adrs_next
)
{
    ipmc_prot_intf_group_entry_t    intf_group_entry;
    ipmc_group_db_t                 *grp_db;
    ipmc_prot_group_srclist_t       group_srclist_entry;
    ipmc_sfm_srclist_t              *sfm_src;
    mesa_vid_t                      vidx;
    vtss_isid_t                     sidx, isid;
    mesa_port_no_t                  prtx, iport;
    mesa_ipv6_t                     grpx, adrx, *adrp;
    vtss_ifindex_elm_t              ife;
    mesa_ipv4_t                     ipa4;
    BOOL                            fnd;
    switch_iter_t                   sit;
    port_iter_t                     pit;
    i8                              grpString[40], adrString[40];
    mesa_rc                         rc = VTSS_RC_ERROR;

    if (!ifid_next || !grps_next ||
        !port_next || !adrs_next) {
        T_D("Invalid Input!");
        return rc;
    }

    vidx = VTSS_APPL_IPMC_VID_NULL;
    IPMC_LIB_ADRS_SET(&grpx, 0x0);
    sidx = VTSS_ISID_GLOBAL;
    prtx = VTSS_PORT_NO_START;
    IPMC_LIB_ADRS_SET(&adrx, 0x0);
    adrp = NULL;
    memset(&intf_group_entry, 0x0, sizeof(ipmc_prot_intf_group_entry_t));
    grp_db = &intf_group_entry.db;
    fnd = FALSE;
    if (ifid_prev && *ifid_prev >= VTSS_IFINDEX_VLAN_OFFSET) {
        T_D("Get VID from given IfIndex");
        if (vtss_ifindex_decompose(*ifid_prev, &ife) != VTSS_RC_OK ||
            ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
            T_D("Failed to decompose VLAN-IfIndex %u!\n\r", VTSS_IFINDEX_PRINTF_ARG(*ifid_prev));
            return rc;
        }
        vidx = (mesa_vid_t)ife.ordinal;

        if (grps_prev != NULL) {
            if (version == IPMC_IP_VERSION_IGMP) {
                ipa4 = htonl(*((mesa_ipv4_t *)grps_prev));
                IPMC_LIB_ADRS_4TO6_SET(ipa4, grpx);
            }
            if (version == IPMC_IP_VERSION_MLD) {
                IPMC_LIB_ADRS_CPY(&grpx, ((mesa_ipv6_t *)grps_prev));
            }
        }

        if ((rc = mvr_mgmt_group_info_get(VTSS_ISID_GLOBAL, version, &vidx, &grpx, &intf_group_entry)) == VTSS_RC_OK) {
            if (port_prev) {
                T_D("Get ISID/PORT from given IfIndex");
                if (vtss_ifindex_decompose(*port_prev, &ife) != VTSS_RC_OK ||
                    ife.iftype != VTSS_IFINDEX_TYPE_PORT) {
                    T_D("Failed to decompose PORT-IfIndex %u!\n\r", VTSS_IFINDEX_PRINTF_ARG(*port_prev));
                    rc = mvr_mgmt_group_info_get_next(VTSS_ISID_GLOBAL, version, &vidx, &grpx, &intf_group_entry);
                } else {
                    sidx = ife.isid;
                    prtx = (mesa_vid_t)ife.ordinal;
                }
            }

            if (rc == VTSS_RC_OK && sidx != VTSS_ISID_GLOBAL &&
                mvr_mgmt_group_info_get(sidx, version, &vidx, &grpx, &intf_group_entry) == VTSS_RC_OK) {
                if (VTSS_PORT_BF_GET(grp_db->port_mask, prtx) &&
                    IPMC_LIB_GRP_PORT_DO_SFM(grp_db, prtx)) {
                    if (adrs_prev != NULL) {
                        if (version == IPMC_IP_VERSION_IGMP) {
                            ipa4 = htonl(*((mesa_ipv4_t *)adrs_prev));
                            IPMC_LIB_ADRS_4TO6_SET(ipa4, adrx);
                        }
                        if (version == IPMC_IP_VERSION_MLD) {
                            IPMC_LIB_ADRS_CPY(&adrx, ((mesa_ipv6_t *)adrs_prev));
                        }
                        adrp = &adrx;
                    }
                }
            }
        } else {
            rc = mvr_mgmt_group_info_get_next(VTSS_ISID_GLOBAL, version, &vidx, &grpx, &intf_group_entry);
        }
    } else {
        rc = mvr_mgmt_group_info_get_next(VTSS_ISID_GLOBAL, version, &vidx, &grpx, &intf_group_entry);
    }

    memset(&group_srclist_entry, 0x0, sizeof(ipmc_prot_group_srclist_t));
    sfm_src = &group_srclist_entry.srclist;
    while (!fnd && rc == VTSS_RC_OK) {
        vidx = intf_group_entry.vid;
        IPMC_LIB_ADRS_CPY(&grpx, &intf_group_entry.group_addr);

        if (sidx == VTSS_ISID_GLOBAL) {
            (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
            while (!fnd && switch_iter_getnext(&sit)) {
                isid = sit.isid;
                if (mvr_mgmt_group_info_get(isid, version, &vidx, &grpx, &intf_group_entry) != VTSS_RC_OK) {
                    continue;
                }

                (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_FRONT);
                while (port_iter_getnext(&pit)) {
                    iport = pit.iport;
                    if (VTSS_PORT_BF_GET(grp_db->port_mask, iport) &&
                        IPMC_LIB_GRP_PORT_DO_SFM(grp_db, iport)) {
                        memset(&group_srclist_entry, 0x0, sizeof(ipmc_prot_group_srclist_t));
                        group_srclist_entry.type = IPMC_LIB_GRP_PORT_SFM_EX(grp_db, iport) ? FALSE : TRUE;
                        while (mvr_mgmt_get_next_group_srclist(isid, version, vidx,
                                                               &grpx,
                                                               &group_srclist_entry) == VTSS_OK) {
                            if (VTSS_PORT_BF_GET(sfm_src->port_mask, iport)) {
                                IPMC_LIB_ADRS_CPY(&adrx, &sfm_src->src_ip);
                                fnd = TRUE;
                                break;
                            }
                        }

                        if (!fnd) { /* Source is None */
                            IPMC_LIB_ADRS_SET(&adrx, 0x0);
                        }

                        sidx = isid;
                        prtx = iport;
                        fnd = TRUE;
                        break;
                    }
                }
            }
        } else {
            if (mvr_mgmt_group_info_get(sidx, version, &vidx, &grpx, &intf_group_entry) == VTSS_RC_OK) {
                if (adrp && mvr_util_ipv6_address_zero(adrp)) {
                    if (++prtx >= mesa_port_cnt(nullptr)) {
                        prtx = mesa_port_cnt(nullptr);
                    }
                    adrp = NULL;
                }

                (void) port_iter_init(&pit, NULL, sidx, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_FRONT);
                while (prtx < mesa_port_cnt(nullptr) && port_iter_getnext(&pit)) {
                    if ((iport = pit.iport) < prtx) {
                        continue;
                    }

                    if (VTSS_PORT_BF_GET(grp_db->port_mask, iport) &&
                        IPMC_LIB_GRP_PORT_DO_SFM(grp_db, iport)) {
                        memset(&group_srclist_entry, 0x0, sizeof(ipmc_prot_group_srclist_t));
                        group_srclist_entry.type = IPMC_LIB_GRP_PORT_SFM_EX(grp_db, iport) ? FALSE : TRUE;
                        if (adrp) {
                            IPMC_LIB_ADRS_CPY(&sfm_src->src_ip, adrp);
                        }
                        while (mvr_mgmt_get_next_group_srclist(sidx, version, vidx,
                                                               &grpx,
                                                               &group_srclist_entry) == VTSS_OK) {
                            if (VTSS_PORT_BF_GET(sfm_src->port_mask, iport)) {
                                IPMC_LIB_ADRS_CPY(&adrx, &sfm_src->src_ip);
                                fnd = TRUE;
                                break;
                            }
                        }

                        if (!fnd) { /* Source is None */
                            IPMC_LIB_ADRS_SET(&adrx, 0x0);
                        }

                        prtx = iport;
                        fnd = TRUE;
                        break;
                    }

                    if (!fnd && iport == prtx) {
                        adrp = NULL;
                    }
                }
            }

            (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
            while (!fnd && switch_iter_getnext(&sit)) {
                isid = sit.isid;
                if (isid <= sidx) {
                    continue;
                }
                if (mvr_mgmt_group_info_get(isid, version, &vidx, &grpx, &intf_group_entry) != VTSS_RC_OK) {
                    continue;
                }

                (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_FRONT);
                while (port_iter_getnext(&pit)) {
                    iport = pit.iport;
                    if (VTSS_PORT_BF_GET(grp_db->port_mask, iport) &&
                        IPMC_LIB_GRP_PORT_DO_SFM(grp_db, iport)) {
                        memset(&group_srclist_entry, 0x0, sizeof(ipmc_prot_group_srclist_t));
                        group_srclist_entry.type = IPMC_LIB_GRP_PORT_SFM_EX(grp_db, iport) ? FALSE : TRUE;
                        while (mvr_mgmt_get_next_group_srclist(isid, version, vidx,
                                                               &grpx,
                                                               &group_srclist_entry) == VTSS_OK) {
                            if (VTSS_PORT_BF_GET(sfm_src->port_mask, iport)) {
                                IPMC_LIB_ADRS_CPY(&adrx, &sfm_src->src_ip);
                                fnd = TRUE;
                                break;
                            }
                        }

                        if (!fnd) { /* Source is None */
                            IPMC_LIB_ADRS_SET(&adrx, 0x0);
                        }

                        sidx = isid;
                        prtx = iport;
                        fnd = TRUE;
                        break;
                    }
                }
            }

            if (!fnd) {
                sidx = VTSS_ISID_GLOBAL;
                prtx = VTSS_PORT_NO_START;
            }
        }

        if (!fnd) {
            rc = mvr_mgmt_group_info_get_next(VTSS_ISID_GLOBAL, version, &vidx, &grpx, &intf_group_entry);
        }
    }

    memset(grpString, 0x0, sizeof(adrString));
    memset(adrString, 0x0, sizeof(adrString));
    T_I("%sFound next VIDX-%u / GRPS-%s / PORT-%u:%u / ADDR-%s",
        fnd ? "" : "Not",
        vidx,
        misc_ipv6_txt(&grpx, grpString),
        sidx, prtx,
        misc_ipv6_txt(&adrx, adrString));
    if (fnd) {
        vtss_ifindex_t  idxv, idxp;

        /* convert VIDX to IfIndex */
        if (vtss_ifindex_from_vlan(vidx, &idxv) != VTSS_RC_OK) {
            T_D("Failed to convert VLAN-IfIndex from %u!\n\r", vidx);
            return VTSS_RC_ERROR;
        }
        /* convert SIDX/PRTX to IfIndex */
        if (vtss_ifindex_from_port(sidx, prtx, &idxp) != VTSS_RC_OK) {
            T_D("Failed to convert PORT-IfIndex from %u/%u!\n\r", sidx, prtx);
            return VTSS_RC_ERROR;
        }

        *ifid_next = idxv;
        *port_next = idxp;
        if (version == IPMC_IP_VERSION_IGMP) {
            IPMC_LIB_ADRS_6TO4_SET(grpx, (*((mesa_ipv4_t *)grps_next)));
            *((mesa_ipv4_t *)grps_next) = ntohl(*((mesa_ipv4_t *)grps_next));
            IPMC_LIB_ADRS_6TO4_SET(adrx, (*((mesa_ipv4_t *)adrs_next)));
            *((mesa_ipv4_t *)adrs_next) = ntohl(*((mesa_ipv4_t *)adrs_next));
        }
        if (version == IPMC_IP_VERSION_MLD) {
            IPMC_LIB_ADRS_CPY(((mesa_ipv6_t *)grps_next), &grpx);
            IPMC_LIB_ADRS_CPY(((mesa_ipv6_t *)adrs_next), &adrx);
        }

        return VTSS_RC_OK;
    } else {
        return VTSS_RC_ERROR;
    }
}

static mesa_rc
_vtss_appl_ipmc_mvr_group_srclist_get(
    const ipmc_ip_version_t                     version,
    const mesa_vid_t                            *const vidx,
    const mesa_ipv6_t                           *const grpx,
    const vtss_isid_t                           *const sidx,
    const mesa_port_no_t                        *const prtx,
    const mesa_ipv6_t                           *const adrx,
    vtss_appl_ipmc_mvr_grp_srclist_t            *const entry
)
{
    ipmc_prot_intf_group_entry_t    intf_group_entry;
    mesa_rc                         rc = VTSS_RC_ERROR;

    if (!vidx || !grpx || !sidx ||
        !prtx || !adrx || !entry) {
        T_D("Invalid Input!");
        return rc;
    }

    if ((rc = mvr_mgmt_group_info_get(*sidx, version, vidx, grpx, &intf_group_entry)) == VTSS_RC_OK) {
        ipmc_group_db_t             *grp_db = &intf_group_entry.db;
        ipmc_prot_group_srclist_t   group_srclist_entry;

        if (VTSS_PORT_BF_GET(grp_db->port_mask, *prtx) &&
            IPMC_LIB_GRP_PORT_DO_SFM(grp_db, *prtx)) {
            i32                 fto, sto;
            BOOL                sfm, hws = FALSE;
            ipmc_sfm_srclist_t  *sfm_src = &group_srclist_entry.srclist;

            memset(&group_srclist_entry, 0x0, sizeof(ipmc_prot_group_srclist_t));
            if (IPMC_LIB_GRP_PORT_SFM_EX(grp_db, *prtx)) {
                fto = ipmc_lib_mgmt_calc_delta_time(*sidx, &grp_db->tmr.delta_time.v[*prtx]);
                group_srclist_entry.type = FALSE;
            } else {
                fto = -1;
                group_srclist_entry.type = TRUE;
            }
            sfm = group_srclist_entry.type;

            sto = -1;
            if (mvr_util_ipv6_address_zero(adrx)) {
                hws = TRUE;
                while (mvr_mgmt_get_next_group_srclist(*sidx, version, *vidx,
                                                       grpx,
                                                       &group_srclist_entry) == VTSS_OK) {
                    if (VTSS_PORT_BF_GET(sfm_src->port_mask, *prtx)) {
                        hws = FALSE;
                        rc = VTSS_RC_ERROR;
                        break;
                    }
                }
            } else {
                rc = VTSS_RC_ERROR;
                while (mvr_mgmt_get_next_group_srclist(*sidx, version, *vidx,
                                                       grpx,
                                                       &group_srclist_entry) == VTSS_OK) {
                    if (IPMC_LIB_ADRS_CMP(adrx, &sfm_src->src_ip)) {
                        continue;
                    }

                    if (VTSS_PORT_BF_GET(sfm_src->port_mask, *prtx)) {
                        sto = ipmc_lib_mgmt_calc_delta_time(*sidx, &sfm_src->tmr.delta_time.v[*prtx]);
                        hws = sfm_src->sfm_in_hw;
                        rc = VTSS_RC_OK;
                        break;
                    }
                }
            }

            if (rc == VTSS_RC_OK) {
                entry->group_filter_mode = (fto < 0) ? VTSS_APPL_IPMC_SF_MODE_INCLUDE : VTSS_APPL_IPMC_SF_MODE_EXCLUDE;
                entry->filter_timer = (fto > 0) ? (u32)fto : 0;
                entry->source_type = sfm ? VTSS_APPL_MVR_ACTION_PERMIT : VTSS_APPL_MVR_ACTION_DENY;
                entry->source_timer = (sto > 0) ? (u32)sto : 0;
                entry->hardware_switch = hws;
            }
        } else {
            rc = VTSS_RC_ERROR;
        }
    }

    return rc;
}

/**
 * \brief Get IPMC MVR Global Parameters
 *
 * To read current system parameters in IPMC MVR.
 *
 * \param conf      [OUT]    The IPMC MVR system configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_system_config_get(
    vtss_appl_ipmc_mvr_global_t                 *const conf
)
{
    mesa_rc rc = VTSS_RC_ERROR;

    if (!conf) {
        T_D("Invalid Input!");
        return rc;
    }

    rc = mvr_mgmt_get_mode(&conf->mode);
    T_D("GET(%s)-%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        conf->mode ? "Enabled" : "Disabled");

    return rc;
}

/**
 * \brief Set IPMC MVR Global Parameters
 *
 * To modify current system parameters in IPMC MVR.
 *
 * \param conf      [IN]     The IPMC MVR system configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_system_config_set(
    const vtss_appl_ipmc_mvr_global_t           *const conf
)
{
    BOOL    mvr_mode;
    mesa_rc rc = VTSS_RC_ERROR;

    if (!conf) {
        T_D("Invalid Input!");
        return rc;
    }

    if (mvr_mgmt_get_mode(&mvr_mode) == VTSS_RC_OK) {
        mvr_mode = conf->mode;
        rc = mvr_mgmt_set_mode(&mvr_mode);
    }

    T_D("SET(%s)-%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        conf->mode ? "Enabled" : "Disabled");

    return rc;
}

/**
 * \brief Iterator for retrieving IPMC MVR port information key/index
 *
 * To walk information (configuration and status) index of the port in IPMC MVR.
 *
 * \param prev      [IN]    Interface index to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_general_port_itr(
    const vtss_ifindex_t                        *const prev,
    vtss_ifindex_t                              *const next
)
{
    return vtss_appl_iterator_ifindex_front_port_exist(prev, next);
}

/**
 * \brief Get IPMC MVR specific port configuration
 *
 * To get configuration of the specific port in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 *
 * \param entry     [OUT]   The current configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_port_config_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_mvr_port_conf_t              *const entry
)
{
    vtss_ifindex_elm_t      ife;
    vtss_isid_t             isid;
    u32                     ptx;
    mvr_conf_fast_leave_t   mvr_port_fstlve;
    mesa_rc                 rc = VTSS_RC_ERROR;

    /* get isid/iport from given IfIndex */
    if (!entry || !ifindex ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_PORT ||
        (ptx = ife.ordinal) >= mesa_port_cnt(nullptr)) {
        T_D("Invalid Input!");
        return rc;
    }

    isid = ife.isid;
    T_D("ISID:%u PORT:%u", isid, ptx);

    if ((rc = mvr_mgmt_get_fast_leave_port(isid, &mvr_port_fstlve)) == VTSS_RC_OK) {
        entry->do_immediate_leave = VTSS_PORT_BF_GET(mvr_port_fstlve.ports, ptx);
    }

    T_D("GET(%s:%d:%u)-%sDoFstLve",
        rc == VTSS_RC_OK ? "OK" : "NG",
        isid, ptx,
        entry->do_immediate_leave ? "" : "Not");

    return rc;
}

/**
 * \brief Set IPMC MVR specific port configuration
 *
 * To modify configuration of the specific port in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 * \param entry     [IN]    The revised configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_port_config_set(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_appl_ipmc_mvr_port_conf_t        *const entry
)
{
    vtss_ifindex_elm_t      ife;
    vtss_isid_t             isid;
    u32                     ptx;
    mvr_conf_fast_leave_t   mvr_port_fstlve;
    mesa_rc                 rc = VTSS_RC_ERROR;

    /* get isid/iport from given IfIndex */
    if (!entry || !ifindex ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_PORT ||
        (ptx = ife.ordinal) >= mesa_port_cnt(nullptr)) {
        T_D("Invalid Input!");
        return rc;
    }

    isid = ife.isid;
    T_D("ISID:%u PORT:%u", isid, ptx);

    if ((rc = mvr_mgmt_get_fast_leave_port(isid, &mvr_port_fstlve)) == VTSS_RC_OK) {
        VTSS_PORT_BF_SET(mvr_port_fstlve.ports, ptx, entry->do_immediate_leave);
        rc = mvr_mgmt_set_fast_leave_port(isid, &mvr_port_fstlve);
    }

    T_D("SET(%s:%d:%u)-%sDoFstLve",
        rc == VTSS_RC_OK ? "OK" : "NG",
        isid, ptx,
        entry->do_immediate_leave ? "" : "Not");

    return rc;
}

/**
 * \brief Iterator for retrieving IPMC MVR VLAN information key/index
 *
 * To walk information (configuration and status) index of the VLAN in IPMC MVR.
 *
 * \param prev      [IN]    Interface index to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_vlan_general_itr(
    const vtss_ifindex_t                        *const prev,
    vtss_ifindex_t                              *const next
)
{
    mvr_mgmt_interface_t    mvrif;

    if (!next) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    memset(&mvrif, 0x0, sizeof(mvr_mgmt_interface_t));
    if (prev && *prev >= VTSS_IFINDEX_VLAN_OFFSET) {
        vtss_ifindex_elm_t  ife;

        /* get next */
        T_D("Find next IfIndex");

        /* get VID from given IfIndex */
        if (vtss_ifindex_decompose(*prev, &ife) != VTSS_RC_OK ||
            ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
            T_D("Failed to decompose IfIndex %u!\n\r", VTSS_IFINDEX_PRINTF_ARG(*prev));
            return VTSS_RC_ERROR;
        }

        mvrif.vid = (mesa_vid_t)ife.ordinal;
    } else {
        /* get first */
        T_D("Find first IfIndex");
    }

    if (mvr_mgmt_get_next_intf_entry(VTSS_ISID_GLOBAL, &mvrif) != VTSS_RC_OK) {
        T_D("No next VIDX w.r.t %u", mvrif.vid);
        return VTSS_RC_ERROR;
    }

    T_I("Found next VIDX-%u", mvrif.vid);
    /* convert VIDX to IfIndex */
    if (vtss_ifindex_from_vlan(mvrif.vid, next) != VTSS_RC_OK) {
        T_D("Failed to convert IfIndex from %u!\n\r", mvrif.vid);
        return VTSS_RC_ERROR;
    }

    return VTSS_RC_OK;
}

/**
 * \brief Get IPMC MVR specific VLAN configuration
 *
 * To get configuration of the specific VLAN in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The current configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_vlan_config_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_mvr_interface_t              *const entry
)
{
    vtss_ifindex_elm_t      ife;
    mvr_mgmt_interface_t    mvrif;
    ipmc_lib_profile_mem_t  *pfm;
    i8                      adrString[40];
    mesa_rc                 rc = VTSS_RC_ERROR;

    if (!ifindex || !entry ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid Input!");
        return rc;
    }

    memset(&mvrif, 0x0, sizeof(mvr_mgmt_interface_t));
    mvrif.vid = (mesa_vid_t)ife.ordinal;
    if (mvr_mgmt_get_intf_entry(VTSS_ISID_GLOBAL, &mvrif) != VTSS_RC_OK) {
        T_D("No such VIDX %u in MVR", mvrif.vid);
        return rc;
    }

    if (IPMC_MEM_PROFILE_MTAKE(pfm)) {
        mvr_conf_intf_entry_t       *mvif = &mvrif.intf;
        ipmc_lib_grp_fltr_profile_t *fltr_profile = &pfm->profile;

        memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
        if (mvif->profile_index) {
            fltr_profile->data.index = mvif->profile_index;
            rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, FALSE);
        } else {
            rc = VTSS_RC_OK;
        }

        if (rc == VTSS_RC_OK) {
            memset(entry, 0x0, sizeof(vtss_appl_ipmc_mvr_interface_t));
            if (strlen(mvif->name) > 0) {
                strncpy(entry->name.n, mvif->name, strlen(mvif->name));
            }
            entry->querier_election = mvif->querier_status;
            entry->igmp_querier_address = mvif->querier4_address;
            entry->mode = mvif->mode;
            entry->tagging = mvif->vtag;
            entry->priority = mvif->priority;
            entry->last_listener_query_interval = mvif->last_listener_query_interval;
            if (strlen(fltr_profile->data.name) > 0) {
                strncpy(entry->channel_profile.n, fltr_profile->data.name, strlen(fltr_profile->data.name));
            }
        }

        IPMC_MEM_PROFILE_MGIVE(pfm);
    }

    memset(adrString, 0x0, sizeof(adrString));
    T_D("GET(%s)-Name:%s/QAdr:%s/Mode:%d/VTag:%d/VPri:%u/LLQI:%u/ChPf:%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        entry->name.n, misc_ipv4_txt(entry->igmp_querier_address, adrString),
        entry->mode, entry->tagging, entry->priority, entry->last_listener_query_interval,
        entry->channel_profile.n);

    return rc;
}

/**
 * \brief Set IPMC MVR specific VLAN configuration
 *
 * To modify configuration of the specific VLAN in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param entry     [IN]    The revised configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_vlan_config_set(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_appl_ipmc_mvr_interface_t        *const entry
)
{
    vtss_ifindex_elm_t      ife;
    mvr_mgmt_interface_t    mvrif;
    ipmc_lib_profile_mem_t  *pfm;
    i8                      adrString[40];
    mesa_rc                 rc = VTSS_RC_ERROR;

    if (!ifindex || !entry ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid Input!");
        return rc;
    }

    if (entry->mode != VTSS_APPL_IPMC_MVR_INTF_MODE_DYNAMIC &&
        entry->mode != VTSS_APPL_IPMC_MVR_INTF_MODE_COMPATIBLE) {
        T_D("Invalid Data!");
        return rc;
    }
    if (entry->tagging != VTSS_APPL_IPMC_INTF_UNTAG &&
        entry->tagging != VTSS_APPL_IPMC_INTF_TAGED) {
        T_D("Invalid Data!");
        return rc;
    }

    memset(&mvrif, 0x0, sizeof(mvr_mgmt_interface_t));
    mvrif.vid = (mesa_vid_t)ife.ordinal;
    if (mvr_mgmt_get_intf_entry(VTSS_ISID_GLOBAL, &mvrif) != VTSS_RC_OK) {
        T_D("No such VIDX %u in MVR", mvrif.vid);
        return rc;
    }

    if (IPMC_MEM_PROFILE_MTAKE(pfm)) {
        mvr_conf_intf_entry_t       *mvif = &mvrif.intf;
        ipmc_lib_grp_fltr_profile_t *fltr_profile = &pfm->profile;

        memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
        if (strlen(entry->channel_profile.n) > 0) {
            strncpy(fltr_profile->data.name, entry->channel_profile.n, strlen(entry->channel_profile.n));
            rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE);
        } else {
            rc = VTSS_RC_OK;
        }

        if (rc == VTSS_RC_OK) {
            memset(mvif->name, 0x0, sizeof(mvif->name));
            if (strlen(entry->name.n) > 0) {
                strncpy(mvif->name, entry->name.n, strlen(entry->name.n));
            }
            mvif->querier_status = entry->querier_election;
            mvif->querier4_address = entry->igmp_querier_address;
            mvif->mode = entry->mode;
            mvif->vtag = entry->tagging;
            mvif->priority = entry->priority;
            mvif->last_listener_query_interval = entry->last_listener_query_interval;
            if (strlen(entry->channel_profile.n) > 0) {
                mvif->profile_index = fltr_profile->data.index;
            } else {
                mvif->profile_index = MVR_CONF_DEF_INTF_PROFILE;
            }

            rc = mvr_mgmt_set_intf_entry(VTSS_ISID_GLOBAL, IPMC_OP_SET, &mvrif);
        }

        IPMC_MEM_PROFILE_MGIVE(pfm);
    }

    memset(adrString, 0x0, sizeof(adrString));
    T_D("SET(%s)-Name:%s/QAdr:%s/Mode:%d/VTag:%d/VPri:%u/LLQI:%u/ChPf:%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        entry->name.n, misc_ipv4_txt(entry->igmp_querier_address, adrString),
        entry->mode, entry->tagging, entry->priority, entry->last_listener_query_interval,
        entry->channel_profile.n);

    return rc;
}

/**
 * \brief Delete IPMC MVR specific VLAN configuration
 *
 * To delete configuration of the specific VLAN in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_vlan_config_del(
    const vtss_ifindex_t                        *const ifindex
)
{
    vtss_ifindex_elm_t      ife;
    mvr_mgmt_interface_t    mvrif;
    mvr_conf_intf_entry_t   *mvif;
    i8                      adrString[40];
    mesa_rc                 rc = VTSS_RC_ERROR;

    if (!ifindex ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid Input!");
        return rc;
    }

    memset(&mvrif, 0x0, sizeof(mvr_mgmt_interface_t));
    mvrif.vid = (mesa_vid_t)ife.ordinal;
    if (mvr_mgmt_get_intf_entry(VTSS_ISID_GLOBAL, &mvrif) != VTSS_RC_OK) {
        T_D("No such VIDX %u in MVR", mvrif.vid);
        return rc;
    }

    mvif = &mvrif.intf;
    rc = mvr_mgmt_set_intf_entry(VTSS_ISID_GLOBAL, IPMC_OP_DEL, &mvrif);

    memset(adrString, 0x0, sizeof(adrString));
    T_D("DEL(%s)-Name:%s/QAdr:%s/Mode:%d/VTag:%d/VPri:%u/LLQI:%u/ChPf:%u",
        rc == VTSS_RC_OK ? "OK" : "NG",
        mvif->name, misc_ipv4_txt(mvif->querier4_address, adrString),
        mvif->mode, mvif->vtag, mvif->priority, mvif->last_listener_query_interval,
        mvif->profile_index);

    return rc;
}

/**
 * \brief Add IPMC MVR specific VLAN configuration
 *
 * To add configuration of the specific VLAN in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param entry     [IN]    The new configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_vlan_config_add(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_appl_ipmc_mvr_interface_t        *const entry
)
{
    vtss_ifindex_elm_t      ife;
    mvr_mgmt_interface_t    mvrif;
    ipmc_lib_profile_mem_t  *pfm;
    i8                      adrString[40];
    mesa_rc                 rc = VTSS_RC_ERROR;

    if (!ifindex || !entry ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid Input!");
        return rc;
    }

    if (entry->mode != VTSS_APPL_IPMC_MVR_INTF_MODE_DYNAMIC &&
        entry->mode != VTSS_APPL_IPMC_MVR_INTF_MODE_COMPATIBLE) {
        T_D("Invalid Data!");
        return rc;
    }
    if (entry->tagging != VTSS_APPL_IPMC_INTF_UNTAG &&
        entry->tagging != VTSS_APPL_IPMC_INTF_TAGED) {
        T_D("Invalid Data!");
        return rc;
    }

    memset(&mvrif, 0x0, sizeof(mvr_mgmt_interface_t));
    mvrif.vid = (mesa_vid_t)ife.ordinal;
    if (mvr_mgmt_get_intf_entry(VTSS_ISID_GLOBAL, &mvrif) == VTSS_RC_OK) {
        T_D("Existing VIDX %u in MVR", mvrif.vid);
        return rc;
    }

    if (IPMC_MEM_PROFILE_MTAKE(pfm)) {
        mvr_conf_intf_entry_t       *mvif = &mvrif.intf;
        ipmc_lib_grp_fltr_profile_t *fltr_profile = &pfm->profile;

        memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
        if (strlen(entry->channel_profile.n) > 0) {
            strncpy(fltr_profile->data.name, entry->channel_profile.n, strlen(entry->channel_profile.n));
            rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE);
        } else {
            rc = VTSS_RC_OK;
        }

        if (rc == VTSS_RC_OK) {
            memset(mvif->name, 0x0, sizeof(mvif->name));
            if (strlen(entry->name.n) > 0) {
                strncpy(mvif->name, entry->name.n, strlen(entry->name.n));
            }
            mvif->querier_status = entry->querier_election;
            mvif->querier4_address = entry->igmp_querier_address;
            mvif->mode = entry->mode;
            mvif->vtag = entry->tagging;
            mvif->priority = entry->priority;
            mvif->last_listener_query_interval = entry->last_listener_query_interval;
            if (strlen(entry->channel_profile.n) > 0) {
                mvif->profile_index = fltr_profile->data.index;
            } else {
                mvif->profile_index = MVR_CONF_DEF_INTF_PROFILE;
            }

            rc = mvr_mgmt_set_intf_entry(VTSS_ISID_GLOBAL, IPMC_OP_ADD, &mvrif);
        }

        IPMC_MEM_PROFILE_MGIVE(pfm);
    }

    memset(adrString, 0x0, sizeof(adrString));
    T_D("ADD(%s)-Name:%s/QAdr:%s/Mode:%d/VTag:%d/VPri:%u/LLQI:%u/ChPf:%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        entry->name.n, misc_ipv4_txt(entry->igmp_querier_address, adrString),
        entry->mode, entry->tagging, entry->priority, entry->last_listener_query_interval,
        entry->channel_profile.n);

    return rc;
}

/**
 * \brief Iterator for retrieving IPMC MVR per VLAN's port information key/index
 *
 * To walk information (configuration and status) index of the port in a IPMC MVR VLAN.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param port_prev [IN]    Ifindex of port to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param port_next [OUT]   The key/index of port's Ifindex should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_intf_port_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const vtss_ifindex_t                        *const port_prev,
    vtss_ifindex_t                              *const port_next
)
{
    mvr_mgmt_interface_t    mvrif;
    mesa_vid_t              vidx;
    vtss_isid_t             sidx;
    mesa_port_no_t          prtx;
    vtss_ifindex_t          idxv, idxp;

    if (!ifid_next || !port_next) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    vidx = VTSS_APPL_IPMC_VID_NULL;
    sidx = VTSS_ISID_GLOBAL;
    prtx = VTSS_PORT_NO_START;
    memset(&mvrif, 0x0, sizeof(mvr_mgmt_interface_t));
    if (ifid_prev && *ifid_prev >= VTSS_IFINDEX_VLAN_OFFSET) {
        vtss_ifindex_elm_t  ife;

        /* get VID from given IfIndex */
        if (vtss_ifindex_decompose(*ifid_prev, &ife) != VTSS_RC_OK ||
            ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
            T_D("Failed to decompose VLAN IfIndex %u!\n\r", VTSS_IFINDEX_PRINTF_ARG(*ifid_prev));
            return VTSS_RC_ERROR;
        }

        mvrif.vid = (mesa_vid_t)ife.ordinal;
        if (mvr_mgmt_get_intf_entry(VTSS_ISID_GLOBAL, &mvrif) != VTSS_RC_OK) {
            if (mvr_mgmt_get_next_intf_entry(VTSS_ISID_GLOBAL, &mvrif) != VTSS_RC_OK) {
                T_D("No next VIDX w.r.t %u", mvrif.vid);
                return VTSS_RC_ERROR;
            }
        } else {
            if (port_prev) {
                /* get isid/iport from given IfIndex */
                if (vtss_ifindex_decompose(*port_prev, &ife) != VTSS_RC_OK ||
                    ife.iftype != VTSS_IFINDEX_TYPE_PORT) {
                    T_D("Failed to decompose PORT IfIndex %u!\n\r", VTSS_IFINDEX_PRINTF_ARG(*port_prev));
                    if (mvr_mgmt_get_next_intf_entry(VTSS_ISID_GLOBAL, &mvrif) != VTSS_RC_OK) {
                        T_D("No next VIDX w.r.t %u", mvrif.vid);
                        return VTSS_RC_ERROR;
                    }
                } else {
                    sidx = ife.usid;
                    prtx = ife.ordinal;
                }
            }
        }

        vidx = mvrif.vid;
    } else {
        /* get first */
        T_D("Find first IfIndex");
        if (mvr_mgmt_get_next_intf_entry(VTSS_ISID_GLOBAL, &mvrif) != VTSS_RC_OK) {
            T_D("No next VIDX w.r.t %u", mvrif.vid);
            return VTSS_RC_ERROR;
        }

        vidx = mvrif.vid;
    }

    if (sidx == VTSS_ISID_GLOBAL) {
        if (!ipmc_lib_mgmt_port_ifindex_get_next(NULL, NULL, &sidx, &prtx)) {
            T_D("No first IfIndex found!\n\r");
            return VTSS_RC_ERROR;
        }
    } else {
        vtss_isid_t     usid = sidx;
        mesa_port_no_t  uport = prtx;

        if (!ipmc_lib_mgmt_port_ifindex_get_next(&usid, &uport, &sidx, &prtx)) {
            if (mvr_mgmt_get_next_intf_entry(VTSS_ISID_GLOBAL, &mvrif) != VTSS_RC_OK) {
                T_D("No next VIDX w.r.t %u", mvrif.vid);
                return VTSS_RC_ERROR;
            } else {
                vidx = mvrif.vid;
                if (!ipmc_lib_mgmt_port_ifindex_get_next(NULL, NULL, &sidx, &prtx)) {
                    T_D("No first IfIndex found!\n\r");
                    return VTSS_RC_ERROR;
                }
            }
        }
    }

    T_I("Found next VIDX-%u/SIDX-%u/PRTX-%u", vidx, sidx, prtx);
    /* convert VIDX to VLAN IfIndex */
    if (vtss_ifindex_from_vlan(vidx, &idxv) != VTSS_RC_OK) {
        T_D("Failed to convert VLAN IfIndex from %u!\n\r", vidx);
        return VTSS_RC_ERROR;
    }
    /* convert SIDX/PRTX to PORT IfIndex */
    if (vtss_ifindex_from_port(sidx, prtx, &idxp) != VTSS_RC_OK) {
        T_D("Failed to convert PORT IfIndex from %u/%u!\n\r", sidx, prtx);
        return VTSS_RC_ERROR;
    }

    *ifid_next = idxv;
    *port_next = idxp;

    return VTSS_RC_OK;
}

/**
 * \brief Get IPMC MVR VLAN's specific port configuration
 *
 * To get configuration of the specific port in an IPMC MVR VLAN.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param stkport   [IN]    (key) Interface index - the logical interface index of the physical port.
 *
 * \param entry     [OUT]   The current configuration of the port in a MVR VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_intf_port_config_get(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_ifindex_t                        *const stkport,
    vtss_appl_ipmc_mvr_intf_port_t              *const entry
)
{
    vtss_ifindex_elm_t      ife;
    mvr_mgmt_interface_t    mvrif;
    mesa_vid_t              vidx;
    vtss_isid_t             sidx;
    mesa_port_no_t          prtx;
    mesa_rc                 rc = VTSS_RC_ERROR;

    if (!ifindex || !stkport || !entry) {
        T_D("Invalid Input!");
        return rc;
    }

    if (vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Failed to decompose VLAN IfIndex %u!\n\r", VTSS_IFINDEX_PRINTF_ARG(*ifindex));
        return rc;
    }
    vidx = (mesa_vid_t)ife.ordinal;
    if (vtss_ifindex_decompose(*stkport, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_PORT ||
        (prtx = ife.ordinal) >= mesa_port_cnt(nullptr)) {
        T_D("Failed to decompose PORT IfIndex %u!\n\r", VTSS_IFINDEX_PRINTF_ARG(*stkport));
        return rc;
    }
    sidx = ife.isid;

    memset(&mvrif, 0x0, sizeof(mvr_mgmt_interface_t));
    mvrif.vid = vidx;
    if ((rc = mvr_mgmt_get_intf_entry(sidx, &mvrif)) == VTSS_RC_OK) {
        entry->role = mvrif.role.ports[prtx];
    }

    T_D("GET(%s)-VIDX:%u/SIDX:%u/PRTX:%u/ROLE:%u",
        rc == VTSS_RC_OK ? "OK" : "NG",
        vidx, sidx, prtx, entry->role);

    return rc;
}

/**
 * \brief Set IPMC MVR VLAN's specific port configuration
 *
 * To modify configuration of the specific port in an IPMC MVR VLAN.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param stkport   [IN]    (key) Interface index - the logical interface index of the physical port.
 * \param entry     [IN]    The revised configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_intf_port_config_set(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_ifindex_t                        *const stkport,
    const vtss_appl_ipmc_mvr_intf_port_t        *const entry
)
{
    vtss_ifindex_elm_t      ife;
    mvr_mgmt_interface_t    mvrif;
    mesa_vid_t              vidx;
    vtss_isid_t             sidx;
    mesa_port_no_t          prtx;
    mesa_rc                 rc = VTSS_RC_ERROR;

    if (!ifindex || !stkport || !entry) {
        T_D("Invalid Input!");
        return rc;
    }

    if (entry->role != VTSS_APPL_IPMC_MVR_PORT_ROLE_INACT &&
        entry->role != VTSS_APPL_IPMC_MVR_PORT_ROLE_SOURCE &&
        entry->role != VTSS_APPL_IPMC_MVR_PORT_ROLE_RECEIVER) {
        T_D("Invalid Data!");
        return rc;
    }

    if (vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Failed to decompose VLAN IfIndex %u!\n\r", VTSS_IFINDEX_PRINTF_ARG(*ifindex));
        return rc;
    }
    vidx = (mesa_vid_t)ife.ordinal;
    if (vtss_ifindex_decompose(*stkport, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_PORT ||
        (prtx = ife.ordinal) >= mesa_port_cnt(nullptr)) {
        T_D("Failed to decompose PORT IfIndex %u!\n\r", VTSS_IFINDEX_PRINTF_ARG(*stkport));
        return rc;
    }
    sidx = ife.isid;

    memset(&mvrif, 0x0, sizeof(mvr_mgmt_interface_t));
    mvrif.vid = vidx;
    if ((rc = mvr_mgmt_get_intf_entry(sidx, &mvrif)) == VTSS_RC_OK) {
        mvrif.role.ports[prtx] = entry->role;
        rc = mvr_mgmt_set_intf_entry(sidx, IPMC_OP_SET, &mvrif);
    }

    T_D("SET(%s)-VIDX:%u/SIDX:%u/PRTX:%u/ROLE:%u",
        rc == VTSS_RC_OK ? "OK" : "NG",
        vidx, sidx, prtx, entry->role);

    return rc;
}

/**
 * \brief Get current IPMC MVR multicast group registration count
 *
 * To read the entry count of registered multicast group address from IPMC MVR.
 *
 * \param grp_cnt   [OUT]   The current registered multicast group address count.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_grp_adr_cnt_get(
    vtss_appl_ipmc_mvr_grp_adrs_cnt_t           *const grp_cnt
)
{
    vtss_isid_t                     sidx;
    ipmc_ip_version_t               verx;
    mesa_vid_t                      vidx;
    mesa_ipv6_t                     grpx;
    ipmc_prot_intf_group_entry_t    intf_group_entry;

    if (!grp_cnt) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    memset(grp_cnt, 0x0, sizeof(vtss_appl_ipmc_mvr_grp_adrs_cnt_t));

    sidx = VTSS_ISID_GLOBAL;
    verx = IPMC_IP_VERSION_IGMP;
    vidx = VTSS_APPL_IPMC_VID_NULL;
    ipmc_lib_get_all_zero_ipv6_addr(&grpx);
    memset(&intf_group_entry, 0x0, sizeof(ipmc_prot_intf_group_entry_t));
    while (mvr_mgmt_group_info_get_next(sidx, verx, &vidx, &grpx, &intf_group_entry) == VTSS_RC_OK) {
        if (intf_group_entry.ipmc_version == IPMC_IP_VERSION_IGMP) {
            ++grp_cnt->mvr_igmp_grps;
        }
        if (intf_group_entry.ipmc_version == IPMC_IP_VERSION_MLD) {
            ++grp_cnt->mvr_mld_grps;
        }

        verx = intf_group_entry.ipmc_version;
        vidx = intf_group_entry.vid;
        IPMC_LIB_ADRS_CPY(&grpx, &intf_group_entry.group_addr);
    }

    T_D("GET IGMP_GRP_CNT-%u / MLD_GRP_CNT-%u",
        grp_cnt->mvr_igmp_grps,
        grp_cnt->mvr_mld_grps);

    return VTSS_RC_OK;
}

/**
 * \brief Get IPMC MVR specific VLAN interface's IGMP status
 *
 * To get interface's IGMP status of the specific VLAN in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The current configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_igmp_vlan_status_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_mvr_intf_ipv4_status_t       *const entry
)
{
    T_D("enter(ifindex:%d)", ifindex ? vtss_ifindex_cast_to_u32(*ifindex) : -1);
    return _vtss_appl_ipmc_mvr_vlan_status_get(IPMC_IP_VERSION_IGMP, ifindex, (void *)entry);
}


/**
 * \brief Iterator for retrieving IPMC MVR IPv4 group address table key/index
 *
 * To walk indexes of the IPv4 group address table in IPMC MVR.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param grps_prev [IN]    IPv4 Address of multicast group to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param grps_next [OUT]   The key/index of multicast group address should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_igmp_group_address_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const mesa_ipv4_t                           *const grps_prev,
    mesa_ipv4_t                                 *const grps_next
)
{
    T_D("enter(MVR-IGMP-GRPADDR)");
    return _vtss_appl_ipmc_mvr_group_address_itr(IPMC_IP_VERSION_IGMP,
                                                 ifid_prev,
                                                 ifid_next,
                                                 (void *)grps_prev,
                                                 (void *)grps_next);
}

/**
 * \brief Get IPMC MVR specific registered IPv4 multicast address on a VLAN
 *
 * To get registered IPv4 multicast group address data of the specific VLAN in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param grpadrs   [IN]    (key) IPv4 multicast group address.
 *
 * \param entry     [OUT]   The current status of the registered multicast group address.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_igmp_group_address_get(
    const vtss_ifindex_t                        *const ifindex,
    const mesa_ipv4_t                           *const grpadrs,
    vtss_appl_ipmc_mvr_grp_address_t            *const entry
)
{
    vtss_ifindex_elm_t  ife;
    mesa_vid_t          vidx;
    mesa_ipv6_t         grpx;
    mesa_ipv4_t         ipa4;
    i8                  grpString[40];
    mesa_rc             rc = VTSS_RC_ERROR;

    /* get VID from given IfIndex */
    if (!entry || !ifindex || !grpadrs ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid Input!");
        return rc;
    }

    vidx = (mesa_vid_t)ife.ordinal;
    IPMC_LIB_ADRS_SET(&grpx, 0x0);
    ipa4 = htonl(*grpadrs);
    IPMC_LIB_ADRS_4TO6_SET(ipa4, grpx);
    rc = _vtss_appl_ipmc_mvr_group_address_get(IPMC_IP_VERSION_IGMP, &vidx, &grpx, entry);

    memset(grpString, 0x0, sizeof(grpString));
    T_D("GET(%s)-VIDX:%u/GRPX:%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        vidx, misc_ipv6_txt(&grpx, grpString));

    return rc;
}

/**
 * \brief Iterator for retrieving IPMC MVR IPv4 group source list table key/index
 *
 * To walk indexes of the IPv4 group source list table in IPMC MVR.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param grps_prev [IN]    IPv4 Address of multicast group to be used for indexing determination.
 * \param port_prev [IN]    Ifindex of port to be used for indexing determination.
 * \param adrs_prev [IN]    IPv4 Address of multicasting source to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param grps_next [OUT]   The key/index of multicast group address should be used for the GET operation.
 * \param port_next [OUT]   The key/index of port's Ifindex should be used for the GET operation.
 * \param adrs_next [OUT]   The key/index of multicasting source address should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_igmp_group_srclist_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const mesa_ipv4_t                           *const grps_prev,
    mesa_ipv4_t                                 *const grps_next,
    const vtss_ifindex_t                        *const port_prev,
    vtss_ifindex_t                              *const port_next,
    const mesa_ipv4_t                           *const adrs_prev,
    mesa_ipv4_t                                 *const adrs_next
)
{
    T_D("enter(MVR-IGMP-SRCLIST)");
    return _vtss_appl_ipmc_mvr_group_srclist_itr(IPMC_IP_VERSION_IGMP,
                                                 ifid_prev,
                                                 ifid_next,
                                                 (void *)grps_prev,
                                                 (void *)grps_next,
                                                 port_prev,
                                                 port_next,
                                                 (void *)adrs_prev,
                                                 (void *)adrs_next);
}

/**
 * \brief Get IPMC MVR specific registered IPv4 multicast address and its source list entry
 *
 * To get registered IPv4 multicast group source list data of the specific VLAN and port in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param grpadrs   [IN]    (key) IPv4 multicast group address.
 * \param stkport   [IN]    (key) Interface index - the logical interface index of the port.
 * \param srcadrs   [IN]    (key) Multicasting IPv4 source address.
 *
 * \param entry     [OUT]   The current status of the registered IPv4 multicast group source list entry.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_igmp_group_srclist_get(
    const vtss_ifindex_t                        *const ifindex,
    const mesa_ipv4_t                           *const grpadrs,
    const vtss_ifindex_t                        *const stkport,
    const mesa_ipv4_t                           *const srcadrs,
    vtss_appl_ipmc_mvr_grp_srclist_t            *const entry
)
{
    vtss_ifindex_elm_t  ife;
    mesa_vid_t          vidx;
    mesa_ipv6_t         grpx, adrx;
    vtss_isid_t         sidx;
    mesa_port_no_t      prtx;
    mesa_ipv4_t         ipa4;
    i8                  grpString[40], adrString[40];
    mesa_rc             rc = VTSS_RC_ERROR;

    /* get VID from given IfIndex */
    if (!entry || !ifindex || !grpadrs || !stkport || !srcadrs ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid Input!");
        return rc;
    }
    vidx = (mesa_vid_t)ife.ordinal;
    /* get ISID/PORT from given IfIndex */
    if (vtss_ifindex_decompose(*stkport, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_PORT ||
        (prtx = ife.ordinal) >= mesa_port_cnt(nullptr)) {
        T_D("Invalid Input!");
        return rc;
    }
    sidx = ife.isid;

    IPMC_LIB_ADRS_SET(&grpx, 0x0);
    ipa4 = htonl(*grpadrs);
    IPMC_LIB_ADRS_4TO6_SET(ipa4, grpx);
    IPMC_LIB_ADRS_SET(&adrx, 0x0);
    ipa4 = htonl(*srcadrs);
    IPMC_LIB_ADRS_4TO6_SET(ipa4, adrx);
    rc = _vtss_appl_ipmc_mvr_group_srclist_get(IPMC_IP_VERSION_IGMP, &vidx, &grpx, &sidx, &prtx, &adrx, entry);

    memset(grpString, 0x0, sizeof(grpString));
    memset(adrString, 0x0, sizeof(adrString));
    T_D("GET(%s)-VIDX:%u/GRPX:%s/SIDX:%d/PRTX:%d/ADRX:%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        vidx, misc_ipv6_txt(&grpx, grpString),
        sidx, prtx, misc_ipv6_txt(&adrx, adrString));

    return rc;
}

/**
 * \brief Get IPMC MVR specific VLAN interface's MLD status
 *
 * To get interface's MLD status of the specific VLAN in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The current configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_mld_vlan_status_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_mvr_intf_ipv6_status_t       *const entry
)
{
    T_D("enter(ifindex:%d)", ifindex ? vtss_ifindex_cast_to_u32(*ifindex) : -1);
    return _vtss_appl_ipmc_mvr_vlan_status_get(IPMC_IP_VERSION_MLD, ifindex, (void *)entry);
}

/**
 * \brief Iterator for retrieving IPMC MVR IPv6 group address table key/index
 *
 * To walk indexes of the IPv4 group address table in IPMC MVR.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param grps_prev [IN]    IPv6 Address of multicast group to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param grps_next [OUT]   The key/index of multicast group address should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_mld_group_address_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const mesa_ipv6_t                           *const grps_prev,
    mesa_ipv6_t                                 *const grps_next
)
{
    T_D("enter(MVR-MLD-GRPADDR)");
    return _vtss_appl_ipmc_mvr_group_address_itr(IPMC_IP_VERSION_MLD,
                                                 ifid_prev,
                                                 ifid_next,
                                                 (void *)grps_prev,
                                                 (void *)grps_next);
}

/**
 * \brief Get IPMC MVR specific registered IPv6 multicast address on a VLAN
 *
 * To get registered IPv6 multicast group address data of the specific VLAN in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param grpadrs   [IN]    (key) IPv6 multicast group address.
 *
 * \param entry     [OUT]   The current status of the registered multicast group address.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_mld_group_address_get(
    const vtss_ifindex_t                        *const ifindex,
    const mesa_ipv6_t                           *const grpadrs,
    vtss_appl_ipmc_mvr_grp_address_t            *const entry
)
{
    vtss_ifindex_elm_t  ife;
    mesa_vid_t          vidx;
    mesa_ipv6_t         grpx;
    i8                  grpString[40];
    mesa_rc             rc = VTSS_RC_ERROR;

    /* get VID from given IfIndex */
    if (!entry || !ifindex || !grpadrs ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid Input!");
        return rc;
    }

    vidx = (mesa_vid_t)ife.ordinal;
    IPMC_LIB_ADRS_CPY(&grpx, grpadrs);
    rc = _vtss_appl_ipmc_mvr_group_address_get(IPMC_IP_VERSION_MLD, &vidx, &grpx, entry);

    memset(grpString, 0x0, sizeof(grpString));
    T_D("GET(%s)-VIDX:%u/GRPX:%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        vidx, misc_ipv6_txt(&grpx, grpString));

    return rc;
}

/**
 * \brief Iterator for retrieving IPMC MVR IPv6 group source list table key/index
 *
 * To walk indexes of the IPv6 group source list table in IPMC MVR.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param grps_prev [IN]    IPv6 Address of multicast group to be used for indexing determination.
 * \param port_prev [IN]    Ifindex of port to be used for indexing determination.
 * \param adrs_prev [IN]    IPv6 Address of multicasting source to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param grps_next [OUT]   The key/index of multicast group address should be used for the GET operation.
 * \param port_next [OUT]   The key/index of port's Ifindex should be used for the GET operation.
 * \param adrs_next [OUT]   The key/index of multicasting source address should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_mld_group_srclist_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const mesa_ipv6_t                           *const grps_prev,
    mesa_ipv6_t                                 *const grps_next,
    const vtss_ifindex_t                        *const port_prev,
    vtss_ifindex_t                              *const port_next,
    const mesa_ipv6_t                           *const adrs_prev,
    mesa_ipv6_t                                 *const adrs_next
)
{
    T_D("enter(MVR-MLD-SRCLIST)");
    return _vtss_appl_ipmc_mvr_group_srclist_itr(IPMC_IP_VERSION_MLD,
                                                 ifid_prev,
                                                 ifid_next,
                                                 (void *)grps_prev,
                                                 (void *)grps_next,
                                                 port_prev,
                                                 port_next,
                                                 (void *)adrs_prev,
                                                 (void *)adrs_next);
}

/**
 * \brief Get IPMC MVR specific registered IPv6 multicast address and its source list entry
 *
 * To get registered IPv6 multicast group source list data of the specific VLAN and port in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param grpadrs   [IN]    (key) IPv6 multicast group address.
 * \param stkport   [IN]    (key) Interface index - the logical interface index of the port.
 * \param srcadrs   [IN]    (key) Multicasting IPv6 source address.
 *
 * \param entry     [OUT]   The current status of the registered multicast group source list entry.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_mld_group_srclist_get(
    const vtss_ifindex_t                        *const ifindex,
    const mesa_ipv6_t                           *const grpadrs,
    const vtss_ifindex_t                        *const stkport,
    const mesa_ipv6_t                           *const srcadrs,
    vtss_appl_ipmc_mvr_grp_srclist_t            *const entry
)
{
    vtss_ifindex_elm_t  ife;
    mesa_vid_t          vidx;
    mesa_ipv6_t         grpx, adrx;
    vtss_isid_t         sidx;
    mesa_port_no_t      prtx;
    i8                  grpString[40], adrString[40];
    mesa_rc             rc = VTSS_RC_ERROR;

    /* get VID from given IfIndex */
    if (!entry || !ifindex || !grpadrs || !stkport || !srcadrs ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid Input!");
        return rc;
    }
    vidx = (mesa_vid_t)ife.ordinal;
    /* get ISID/PORT from given IfIndex */
    if (vtss_ifindex_decompose(*stkport, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_PORT ||
        (prtx = ife.ordinal) >= mesa_port_cnt(nullptr)) {
        T_D("Invalid Input!");
        return rc;
    }
    sidx = ife.isid;

    IPMC_LIB_ADRS_CPY(&grpx, grpadrs);
    IPMC_LIB_ADRS_CPY(&adrx, srcadrs);
    rc = _vtss_appl_ipmc_mvr_group_srclist_get(IPMC_IP_VERSION_MLD, &vidx, &grpx, &sidx, &prtx, &adrx, entry);

    memset(grpString, 0x0, sizeof(grpString));
    memset(adrString, 0x0, sizeof(adrString));
    T_D("GET(%s)-VIDX:%u/GRPX:%s/SIDX:%d/PRTX:%d/ADRX:%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        vidx, misc_ipv6_txt(&grpx, grpString),
        sidx, prtx, misc_ipv6_txt(&adrx, adrString));

    return rc;
}

/**
 * \brief IPMC MVR Control ACTION for clearing statistics
 *
 * To clear the statistics based on the given VLAN ifIndex of MVR interface.
 *
 * \param ifindex   [IN]    Interface index - the logical interface index of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_statistics_clear_by_vlan_act(
    const vtss_ifindex_t                        *const ifindex
)
{
    vtss_ifindex_elm_t      ife;
    mvr_mgmt_interface_t    mvrif;

    if (!ifindex) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    if (*ifindex == VTSS_IFINDEX_VLAN_OFFSET) {
        return VTSS_RC_OK;
    }

    if (vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid IfIndex!");
        return VTSS_RC_ERROR;
    }

    memset(&mvrif, 0x0, sizeof(mvr_mgmt_interface_t));
    mvrif.vid = (mesa_vid_t)ife.ordinal;
    if (mvr_mgmt_get_intf_entry(VTSS_ISID_GLOBAL, &mvrif) != VTSS_RC_OK) {
        T_D("No such VIDX %u in MVR", mvrif.vid);
        return VTSS_RC_ERROR;
    }

    return mvr_mgmt_clear_stat_counter(VTSS_ISID_GLOBAL, mvrif.vid);
}
