/*
 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
*/

#include "mvr_serializer.hxx"

VTSS_MIB_MODULE("ipmcMvrMib", "IPMC-MVR", ipmc_mvr_mib_init, VTSS_MODULE_ID_MVR,
                root, h) {
    h.add_history_element("201407010000Z", "Initial version");
    h.add_history_element("201708240000Z", "Added Querier election cofigured option");
    h.description("This is a private version of the IPMC MVR MIB");
}

using namespace vtss;
using namespace expose::snmp;

#define NS(VAR, P, ID, NAME) static NamespaceNode VAR(&P, OidElement(ID, NAME))
NS(ipmc_mvr_objects, root, 1, "ipmcMvrMibObjects");
NS(ipmc_mvr_config, ipmc_mvr_objects, 2, "ipmcMvrConfig");
NS(ipmc_mvr_status, ipmc_mvr_objects, 3, "ipmcMvrStatus");
NS(ipmc_mvr_control, ipmc_mvr_objects, 4, "ipmcMvrControl");

namespace vtss {
namespace appl {
namespace mvr {
namespace interfaces {
static StructRW2<IpmcMvrGlobalsConfig> ipmc_mvr_globals_config(
        &ipmc_mvr_config, vtss::expose::snmp::OidElement(1, "ipmcMvrConfigGlobals"));
static TableReadWrite2<IpmcMvrPortConfigTable> ipmc_mvr_port_config_table(
        &ipmc_mvr_config, vtss::expose::snmp::OidElement(2, "ipmcMvrConfigPortTable"));
static TableReadWriteAddDelete2<IpmcMvrIgmpVlanConfigTable> ipmc_mvr_igmp_vlan_config_table(
        &ipmc_mvr_config, vtss::expose::snmp::OidElement(3, "ipmcMvrConfigInterfaceTable"), vtss::expose::snmp::OidElement(4, "ipmcMvrConfigInterfaceTableRowEditor"));
static TableReadWrite2<IpmcMvrVlanPortConfigTable> ipmc_mvr_vlan_port_config_table(
        &ipmc_mvr_config, vtss::expose::snmp::OidElement(5, "ipmcMvrConfigVlanPortTable"));
static StructRO2<IpmcMvrStatusGrpCnt> ipmc_mvr_status_grp_cnt(
        &ipmc_mvr_status, vtss::expose::snmp::OidElement(1, "ipmcMvrStatusGroupAddressCount"));
static TableReadOnly2<IpmcMvrIgmpVlanStatusTable> ipmc_mvr_igmp_vlan_status_table(
        &ipmc_mvr_status, vtss::expose::snmp::OidElement(2, "ipmcMvrStatusIgmpVlanTable"));
static TableReadOnly2<IpmcMvrIgmpGrpadrsTable> ipmc_mvr_igmp_grpadrs_table(
        &ipmc_mvr_status, vtss::expose::snmp::OidElement(3, "ipmcMvrStatusIgmpGroupAddressTable"));
static TableReadOnly2<IpmcMvrIgmpSrclistTable> ipmc_mvr_igmp_srclist_table(
        &ipmc_mvr_status, vtss::expose::snmp::OidElement(4, "ipmcMvrStatusIgmpGroupSrcListTable"));
static TableReadOnly2<IpmcMvrMldVlanStatusTable> ipmc_mvr_mld_vlan_status_table(
        &ipmc_mvr_status, vtss::expose::snmp::OidElement(5, "ipmcMvrStatusMldVlanTable"));
static TableReadOnly2<IpmcMvrMldGrpadrsTable> ipmc_mvr_mld_grpadrs_table(
        &ipmc_mvr_status, vtss::expose::snmp::OidElement(6, "ipmcMvrStatusMldGroupAddressTable"));
static TableReadOnly2<IpmcMvrMldSrclistTable> ipmc_mvr_mld_srclist_table(
        &ipmc_mvr_status, vtss::expose::snmp::OidElement(7, "ipmcMvrStatusMldGroupSrcListTable"));
static StructRW2<IpmcMvrControlClearStatistics> ipmc_mvr_control_clear_statistics(
        &ipmc_mvr_control, vtss::expose::snmp::OidElement(1, "ipmcMvrControlStatisticsClear"));
}  // namespace interfaces
}  // namespace mvr
}  // namespace appl
}  // namespace vtss

