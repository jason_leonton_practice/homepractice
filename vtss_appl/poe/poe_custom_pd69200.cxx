/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

****************************************************************************/
// PoE chip PD69200 from MicroSemi. The user guide that have been used
// for this implementation is :
//  Microsemi - Serial Communication Protocol User Guide - Revision 6.1
/****************************************************************************/

/*lint -esym(459,i2c_rd_data_rdy_interrupt_function)*/


#include "critd_api.h"
#include "poe.h"
#include "poe_custom_pd69200_api.h"
#include "interrupt_api.h"
#include "poe_custom_api.h"
#include "misc_api.h"
#include "poe_trace.h"
#include "poe_board.h"
#include "vtss_api_if_api.h"
using namespace std;
#include <map>
#include <iostream>
#include <fstream>

// Section 4.1. in PD69200 user guide - key definitions
# define COMMAND_KEY   0x00
# define PROGRAM_KEY   0x01
# define REQUEST_KEY   0x02
# define TELEMETRY_KEY 0x03
# define REPORT_KEY    0x52

#define PD_BUFFER_SIZE 15     // All microSemi's access are 15 bytes

// The sequence number is updated by the transmit function, but we have to insert a dummy seq number when building the data structure.
#define DUMMY_SEQ_NUM 0x00
#define SEQ_NUM_DO_NOT_CHK 0xFF
#define DUMMY_BYTE 0x4E

// A channel is the "port" number from a PoE controllers point of view. If we only
// have one controller a channel is the same as the internal port number, but if
// we have multiple controllers then the channel number must start from 0 for each controller.
// On boards with physically swapped ports (currently Ocelot PoE board), this should still be
// the simple modulo mapping, and the PD69200 should be programmed with a port-matrix that does
// the channel-to-port mapping, since it is the channel number that determines the port-priority
// within each of the 3 user-priorities.
static uchar pd69200_iport2channel(mesa_port_no_t iport)
{
    return iport % POE_PORTS_PER_CONTROLLER;
}

// Some configurations are not needed per port, but per controller.
// In order to ease the application code, we allow the application
// to make a loop over all ports, but will only apply the
// configuration for one port. Any port could be used but we use port 0.
// pd69200_is_base_port can be used to make sure that only one port
// is used to do configuration that are global for the controller.
// Return: TRUE is this is the base port for the controller, else FALSE.
static BOOL pd69200_is_base_port(mesa_port_no_t iport)
{
    return iport % POE_PORTS_PER_CONTROLLER == 0;
}

// We need to know if chip detection is needed for each port.
// Since multiple ports share the same controller at the same i2c device,
// we use a map of "founds", which is going to be updated whenever a
// i2c device is open successfully
static std::map <string, poe_chip_found_t> pd69200_chip_found_map;

// Update map with PoE chip found for a port.
// In: iport - Internal port number
//     chip_found - New chip found value
static void pd69200_chip_found_set(mesa_port_no_t iport, poe_chip_found_t chip_found)
{
    // Get the i2c device name
    poe_custom_entry_t poe_hw_conf;
    poe_hw_conf = poe_custom_get_hw_config(iport, &poe_hw_conf);

    pd69200_chip_found_map[poe_hw_conf.i2c_device_name[PD69200]] = chip_found;
}

// Get if PoE chip is found for a given port.
// In: iport - Internal port number
// Return - chip_found value.
static poe_chip_found_t pd69200_chip_found_get(mesa_port_no_t iport)
{
    // Get the i2c device name
    poe_custom_entry_t poe_hw_conf;
    poe_hw_conf = poe_custom_get_hw_config(iport, &poe_hw_conf);

    // if the device name not found the map of found i2c devices, we need to do chip detection.
    if (pd69200_chip_found_map.find(poe_hw_conf.i2c_device_name[PD69200]) == pd69200_chip_found_map.end()) {
        pd69200_chip_found_map[poe_hw_conf.i2c_device_name[PD69200]] = DETECTION_NEEDED;
    }

    return pd69200_chip_found_map[poe_hw_conf.i2c_device_name[PD69200]];
}

// PD power banking according to Power Banks Definition table in section 4.5.8 in user guide.
# define BACKUP_POWER_BANK 0
# define PRIMARY_POWER_BANK 0
# define ALL_POWER_BANKS 0

#define POE_INTERRUPT_FLAG 0x2

static BOOL ten_sec_i2c_reset_enabled  = FALSE; // Ok not to semaphore protect. Only set from one place.
static BOOL firmware_upgrade_in_progress = FALSE; // Set when doing firmware upgrade

int pd69200_wait_time = 50; // Wait 400 ms according to the synchonization descriped in section 7, but from e-mail from Shmulik this can be set to 50 ms.
static poe_chip_found_t pd69200_do_detection_(mesa_port_no_t iport);

// Debug function for printing the I2C buffer
// In : buf - Pointer to the I2C data
//      len - The number of data bytes
//      trace - TRUE to print using the trace system, else standard out printing.
static void print_buffer (uchar *buf, uchar len, int line, BOOL trace = TRUE)
{
    int i;
    char str_buf[100];
    char str_buf_hex[100];

    strcpy(&str_buf[0], "");
    for (i = 0; i < len ; i++) {
        sprintf(str_buf_hex, "0x%02X ", buf[i]);
        strcat(&str_buf[0], &str_buf_hex[0]);
    }

    if (trace) {
        T_IG(VTSS_TRACE_GRP_CUSTOM, "Line:%d - %s, i2c_wait_time:%d", line, &str_buf[0], pd69200_wait_time);
    } else {
        cout << "Read back:" << str_buf << "\n";
    }
}


// Open i2c driver (if not already open)
// In : iport - internal port number
//      i2c device id.
static mesa_rc open_i2c_dev(mesa_port_no_t iport, int *i2c_dev)
{
    // We keep a map of i2c devices we have already open for access.
    static std::map <string, int> i2c_dev_map;

    // Get the i2c device name
    poe_custom_entry_t poe_hw_conf;
    poe_hw_conf = poe_custom_get_hw_config(iport, &poe_hw_conf); // Any port will do, since the i2c device is the same for all ports

    // Only open if not already
    if (i2c_dev_map.find(poe_hw_conf.i2c_device_name[PD69200]) == i2c_dev_map.end()) {
        T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "I2C device name:%s", poe_hw_conf.i2c_device_name[PD69200]);

        int i2c_base;
        *i2c_dev = vtss_i2c_dev_open(poe_hw_conf.i2c_device_name[PD69200], NULL, &i2c_base);

        if (*i2c_dev == -1) {
            return VTSS_RC_ERROR;
        }

        // Open correctly - store the id
        i2c_dev_map[poe_hw_conf.i2c_device_name[PD69200]] = *i2c_dev;
    }

    // Return the i2c_dev id
    *i2c_dev = i2c_dev_map[poe_hw_conf.i2c_device_name[PD69200]];
    T_NG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "I2C device name:%s, id:%d", poe_hw_conf.i2c_device_name[PD69200], *i2c_dev);
    return VTSS_RC_OK;
}

// Something went wrong, we should do synchronization. We do not do as described in section 7 in the userguide. Instead
// microSemi has suggested that we simply wait more than 10 sec. After 10 sec. the I2C buffer is flushed, and we do not have
// to do a reset of the PoE chip, which microSemi didn't like. We only do the wait once.
// The above suggestion from microsemi didn't work, so the workaround below is used instead.
static void pd69200_sync(mesa_port_no_t iport)
{
    uchar data[PD_BUFFER_SIZE];
    u8 timeout;

    T_IG(VTSS_TRACE_GRP_CUSTOM, "Syncing");
    if (ten_sec_i2c_reset_enabled) {
        VTSS_MSLEEP(10000); // Wait at least 10 sec in order to reset i2c system. See Section 8.2 MASK Registers list in "PD69200 Serial Communication Protocol User Guide"
    }
    VTSS_MSLEEP(1000);
    int i2c_dev;
    if (open_i2c_dev(iport, &i2c_dev) != VTSS_RC_OK) {
        return;
    }

    // Do dummy reads to insure that the i2c is in fact empty now.
    for (timeout = PD_BUFFER_SIZE * 4; timeout > 0; timeout --) {
        (void) vtss_i2c_dev_rd(i2c_dev, &data[0], 1);
        T_DG(VTSS_TRACE_GRP_CUSTOM, "Data:0x%X", data[0]);
        if (data[0] == REPORT_KEY || data[0] == TELEMETRY_KEY) {
            (void) pd69200_rd(iport, &data[1], PD_BUFFER_SIZE - 1);
            T_IG(VTSS_TRACE_GRP_CUSTOM, "Sync done");
            print_buffer(&data[0], PD_BUFFER_SIZE, __LINE__);
            break;
        }
    }
    T_RG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "%s", "Did not find and Report or Telemetry Key");
    VTSS_MSLEEP(1000);
}

// The sequence number is also named ECHO in the user guide. Two consecutive
// messages must not contain the same sequunce number. See section 4.1 - ECHO
// in the user guide.
// In : reset_seq_num - Set to TRUE in order to reset the sequence number to 0.
static char get_seq_num(BOOL reset_seq_num)
{
    static uchar seq_num = 0;

    if (reset_seq_num) {
        seq_num = 0;
    } else if (seq_num == SEQ_NUM_DO_NOT_CHK) {
        seq_num = 0;
    } else {
        seq_num++;
    }
    return seq_num;
}

// Updates the check sum for the command. See section 4.1-CHECKSUM in the user guide
// In : buf - pointer to the I2C data
static void pd69200_update_check_sum (uchar *buf)
{
    int buf_index = 0;
    unsigned int sum = 0;

    // Find the check for the data (skip the last 2 bytes - They are the check sum bytes going to be replaced.
    for (buf_index = 0 ; buf_index < PD_BUFFER_SIZE - 2; buf_index++) {
        sum += buf[buf_index]; // Sum up the checksum
    }

    // Convert from integer to 2 bytes.
    buf[13] = sum >> 8;
    buf[14] = sum & 0xFF;

    T_NG(VTSS_TRACE_GRP_CUSTOM, "Check sum = %u, buf[13] = %u, buf[14] = %u", sum, buf[13], buf[14]);
}

// Check if the check sum is correct. See Section 4.1 - CHECKSUM in the user guide
// In : buf - pointer to the I2C data
BOOL pd69200_check_sum_ok(mesa_port_no_t iport, uchar *buf)
{
    int buf_index = 0;
    unsigned int sum = 0;

    // -2 because the Sum bytes shall not be part of the checksum
    for (buf_index = 0 ; buf_index < PD_BUFFER_SIZE - 2; buf_index++) {
        sum += buf[buf_index]; // Sum up the checksum
    }


    // Do the check of the checksum
    if (( buf[13] != (sum >> 8 & 0xFF)) && (buf[14] != (sum & 0xFF)) ) {
        T_WG(VTSS_TRACE_GRP_CUSTOM, "pd6920 I2C check sum error");
        print_buffer(&buf[0], PD_BUFFER_SIZE, __LINE__);
        pd69200_sync(iport);
        return FALSE;
    } else {
        return TRUE;
    }
}

// Report key - See section 4.6 in the user guide
// expected_seq_num - The expected sequence number for the report
static BOOL report_key_ok(mesa_port_no_t iport, u8 expected_seq_num)
{
    mesa_rc rc;
    BOOL report_key_ok_v = TRUE;

    uchar buf[PD_BUFFER_SIZE];

    rc = pd69200_rd(iport, buf, PD_BUFFER_SIZE);
    if (rc == VTSS_RC_MISC_I2C_REBOOT_IN_PROGRESS) {
        return TRUE;
    } else if (rc != VTSS_RC_OK) {
        return FALSE;
    }
    T_NG(VTSS_TRACE_GRP_CUSTOM, "Entering check_report_key");

    // First make sure that the checksum is correct
    if (pd69200_check_sum_ok(iport, &buf[0])) {
        if (buf[0] != REPORT_KEY) {
            T_WG(VTSS_TRACE_GRP_CUSTOM, "Report Key error - Data isn't a report key buf");
            print_buffer(&buf[0], PD_BUFFER_SIZE, __LINE__);
            report_key_ok_v = FALSE;
        } else if (buf[2] == 0x00 && buf[3] == 0x00) {
            // Command received/correctly executed
            report_key_ok_v = TRUE;
        } else if  (buf[2] == 0xFF && buf[3] == 0xFF && buf[4] == 0xFF && buf[5] == 0xFF) {
            T_WG(VTSS_TRACE_GRP_CUSTOM, "Report key error - Command received / wrong checksum ");
            report_key_ok_v = FALSE;
        } else if  (buf[2] > 0x0  && buf[2] < 0x80) {
            T_WG(VTSS_TRACE_GRP_CUSTOM, "Report key error - Failed execution / conflict in subject bytes");
            report_key_ok_v = FALSE;
        } else if  (buf[2] > 0x80  && buf[2] < 0x90) {
            T_WG(VTSS_TRACE_GRP_CUSTOM, "Report key error - Failed execution / Wrong data byte value");
            report_key_ok_v = FALSE;
        } else if  (buf[2] == 0xFF  && buf[3] == 0xFF) {
            T_WG(VTSS_TRACE_GRP_CUSTOM, "Report key error - Failed execution / Undefined key value");
            report_key_ok_v = FALSE;
        } else {
            report_key_ok_v = TRUE;
        }
    } else {
        T_WG(VTSS_TRACE_GRP_CUSTOM, "Report key error - Checksum error");
        report_key_ok_v = FALSE;
    }

    if (!report_key_ok_v) {
        pd69200_sync(iport);
    }

    return report_key_ok_v;
}


// Function that reads controller response ( reponds upon request ), and check the key and checksum.
// Returns 0 in case of error else 1.
static BOOL get_controller_response (mesa_port_no_t iport, uchar *buf, uchar expected_seq_num)
{
    mesa_rc rc;
    BOOL result ;

    // Try and read the data (Step A)
    rc = pd69200_rd(iport, buf, PD_BUFFER_SIZE);
    if (rc == VTSS_RC_MISC_I2C_REBOOT_IN_PROGRESS) {
        memset(buf, 0x0, PD_BUFFER_SIZE);
        return TRUE;
    } else if (rc != VTSS_RC_OK) {
        memset(buf, 0x0, PD_BUFFER_SIZE);
        return FALSE;
    }

    T_RG(VTSS_TRACE_GRP_CUSTOM, "get_controller_response");

    // Check checksum
    if (!pd69200_check_sum_ok(iport, &buf[0]) || buf[0] != TELEMETRY_KEY) {

        result = FALSE; // Communication error.
        if (expected_seq_num != SEQ_NUM_DO_NOT_CHK && expected_seq_num != buf[1]) {
            T_IG(VTSS_TRACE_GRP_CUSTOM, "Seq number errorr: expect_seq:%d, seq:%d", expected_seq_num, buf[1]);
            print_buffer(&buf[0], PD_BUFFER_SIZE, __LINE__);
            pd69200_sync(iport);
            return FALSE;
        } else if (!pd69200_check_sum_ok(iport, &buf[0])) {
            T_WG(VTSS_TRACE_GRP_CUSTOM, "Checksum error");
        } else if (buf[0] == REPORT_KEY) {
            T_WG(VTSS_TRACE_GRP_CUSTOM, "Unexpected Report key - ignoring");
            return TRUE;
        } else {
            T_WG(VTSS_TRACE_GRP_CUSTOM, "Telemetry error");
            print_buffer(&buf[0], PD_BUFFER_SIZE, __LINE__);
        }

    } else {
        result =  TRUE; // Everything is OK
    }

    return result;
}

// Do the real I2C transmitting. Returns FALSE is the trasmit went wrong.
static BOOL is_i2c_tx_ok(mesa_port_no_t iport, uchar *buf)
{
    buf[1] = get_seq_num(FALSE);

    // Update the checksum
    pd69200_update_check_sum(buf);

    int bytes_tranmitted = pd69200_wr(iport, buf, PD_BUFFER_SIZE);

    // Check that all bytes were transmitted
    if (bytes_tranmitted != PD_BUFFER_SIZE) {
        T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "I2C communication error with PoE controller, bytes transmitted = %u", bytes_tranmitted);
        print_buffer(&buf[0], PD_BUFFER_SIZE, __LINE__);
        return FALSE;
    } else {
        return TRUE; // Ok - Everthing went well.
    }
}

// Transmit the command
// Return TRUE in case that everything went well
// If the transmitted data is a request key the data is return in the buf pointer.
static BOOL pd69200_i2c_tx(mesa_port_no_t iport, uchar *buf, u32 line)
{
    uchar buf_copy[PD_BUFFER_SIZE];
    memcpy(&buf_copy[0], buf, PD_BUFFER_SIZE);

    T_NG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "Entering pd69200_i2c_tx - line:%d", line);

    BOOL tx_ok = FALSE; // Bool for determine if the transmit went well, or if we shall try and do a reset of the PoE chip.

    if (firmware_upgrade_in_progress) {
        return TRUE;
    }

    // Do the I2C transmission and check report key.
    if (is_i2c_tx_ok(iport, &buf_copy[0])) {

        // Section 4.6 in PD69200/G user guide - check report in case of command or program
        if ((buf_copy[0] == COMMAND_KEY || buf_copy[0] == PROGRAM_KEY)) {
            T_RG(VTSS_TRACE_GRP_CUSTOM, "Check report key");
            if (report_key_ok(iport , buf_copy[1])) {
                T_NG(VTSS_TRACE_GRP_CUSTOM, "REPORT_KEY OK");
                tx_ok = TRUE;
            } else {
                T_WG(VTSS_TRACE_GRP_CUSTOM, "Report key error");
                print_buffer(&buf_copy[0], PD_BUFFER_SIZE, __LINE__);
            }
        } else if (buf_copy[0] == REQUEST_KEY) {
            T_RG(VTSS_TRACE_GRP_CUSTOM, "REQUEST_KEY");
            tx_ok = get_controller_response(iport, &buf[0], buf_copy[1]);
        } else {
            T_WG(VTSS_TRACE_GRP_CUSTOM, "ELSE");
            tx_ok = TRUE;
        }
    } else {
        T_WG(VTSS_TRACE_GRP_CUSTOM, "tx error");
    }
    poe_halt_at_i2c_err(!tx_ok);


    if (!tx_ok) {
        if (pd69200_chip_found_get(iport) == DETECTION_NEEDED) {
            T_IG(VTSS_TRACE_GRP_CUSTOM, "I2C communication error - PoE chip not found");
            pd69200_chip_found_set(iport, NOT_FOUND);
        } else {
            T_IG(VTSS_TRACE_GRP_CUSTOM, "I2C communication error - re-detecting PoE chip-set");
            pd69200_chip_found_set(iport, pd69200_do_detection_(iport));
        }
    }

    T_RG(VTSS_TRACE_GRP_CUSTOM, "tx_ok:%d", tx_ok);
    return tx_ok;
}

// Apply the port mapping
static void pd69200_program_global_matrix(mesa_port_no_t iport)
{
    // Transmit the command
    uchar buf[PD_BUFFER_SIZE] = {COMMAND_KEY,
                                 DUMMY_SEQ_NUM,
                                 0x07,
                                 0x43,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE
                                };
    (void) pd69200_i2c_tx(iport, &buf[0], __LINE__);
}

// If we end up mapping two channels to the same port the "pd69200_program_global_matrix"
// will not be executed (no errors will indicate this). To avoid this we start be mapping all channels
// to a non-existing port (Ports above  48)
static void pd69200_temp_matrix_init(mesa_port_no_t iport)
{
    for (uchar i = 0; i < 48; i++) {
        uchar buf[PD_BUFFER_SIZE] = {COMMAND_KEY,
                                     DUMMY_SEQ_NUM,
                                     0x05,
                                     0x43,
                                     i,
                                     (uchar)(i + 49),
                                     255,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE
                                    };
        (void) pd69200_i2c_tx(iport, &buf[0], __LINE__);
    }
}

// Setup port mapping
static void pd69200_temp_matrix_set(mesa_port_no_t iport)
{
    if (!port_custom_table[iport].poe_support) {
        return;
    }

    // Transmit the command
    uchar iport2 = pd69200_iport2channel(iport);
    uchar channel = port_custom_table[iport].poe_chip_port;

    uchar buf[PD_BUFFER_SIZE] = {COMMAND_KEY,
                                 DUMMY_SEQ_NUM,
                                 0x05,
                                 0x43,
                                 channel,
                                 iport2,
                                 255,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE
                                };
    (void) pd69200_i2c_tx(iport, &buf[0], __LINE__);
    T_IG(VTSS_TRACE_GRP_CUSTOM, "Map Channel:%d to port:%d", channel, iport);
}


// Work-around for now.
static CapArray<u16, MESA_CAP_PORT_CNT> pse_allocated_power;
static CapArray<u16, MESA_CAP_PORT_CNT> pd_requested_power;

//
// The PD69200 commands -- See MicroSemi PD69200/G user guide
//
void pd69200_pse_data_get(mesa_port_no_t iport, poe_pse_data_t *pse_data)
{
    uchar channel = pd69200_iport2channel(iport);

    // Send request to get status
    uchar buf[PD_BUFFER_SIZE]  = {REQUEST_KEY,
                                  DUMMY_SEQ_NUM,
                                  0x05,
                                  0xA8,
                                  channel,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE
                                 };

    memset(pse_data, 0, sizeof(poe_pse_data_t));

    // Get response -- Check that response is valid
    if (pd69200_i2c_tx(iport, &buf[0], __LINE__)) {
        pse_data->pse_allocated_power = (buf[2] << 8) + buf[3];
        pse_data->pd_requested_power  = (buf[4] << 8) + buf[5];
        pse_data->pse_power_type      =  buf[6];
        pse_data->power_class         =  buf[7];
        pse_data->pse_power_pair      =  buf[8];
        pse_data->mdi_power_status    =  buf[9];
        pse_data->cable_len           =  buf[10];
        pse_data->power_indicator     =  (buf[11] << 8) + buf[12];

        // Issue that "Get Port Layer2 LLDP PSE Data" always return zero for allocated and requested power. This is a work-around
        pse_data->pse_allocated_power = pse_allocated_power[channel];
        pse_data->pd_requested_power = pd_requested_power[channel];

        T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, channel, "PSE allocated power:%d", pse_data->pse_allocated_power);
        T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, channel, "PD requested power:%d", pse_data->pd_requested_power);
        T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, channel, "PSE power type:0x%X", pse_data->pse_power_type);
        T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, channel,  "PSE indicator :0x%X", pse_data->power_indicator);
    } else {
        T_EG_PORT(VTSS_TRACE_GRP_CUSTOM, channel, "%s", "Could not get LLDP PSE data.");
    }
}


void pd69200_pd_data_set(mesa_port_no_t iport, poe_pd_data_t *pd_data)
{
    uchar channel = pd69200_iport2channel(iport);

    // Transmit the command
    uchar buf[PD_BUFFER_SIZE] = {COMMAND_KEY,
                                 DUMMY_SEQ_NUM,
                                 0x05,
                                 0xA6,
                                 channel,
                                 pd_data->type,
                                 (uchar) ((pd_data->pd_requested_power >> 8) & 0xFF),
                                 (uchar) (pd_data->pd_requested_power & 0xFF),
                                 (uchar) ((pd_data->pse_allocated_power >> 8) & 0xFF),
                                 (uchar) (pd_data->pse_allocated_power & 0xFF),
                                 pd_data->cable_len,
                                 pd_data->execute_lldp,
                                 DUMMY_BYTE
                                };

    (void) pd69200_i2c_tx(iport, &buf[0], __LINE__);

    // Issue that "Get Port Layer2 LLDP PSE Data" always return zero for allocated and requested power. This is a work-around
    pse_allocated_power[channel] = pd_data->pse_allocated_power;
    pd_requested_power[channel]  = pd_data->pd_requested_power;

    T_DG_PORT(VTSS_TRACE_GRP_CUSTOM, channel, "Requested Power:%d", pd_data->pd_requested_power);
    T_DG_PORT(VTSS_TRACE_GRP_CUSTOM, channel, "Allocated Power:%d", pd_data->pse_allocated_power);
    T_DG_PORT(VTSS_TRACE_GRP_CUSTOM, channel, "Cable Len:%d", pd_data->cable_len);
    T_DG_PORT(VTSS_TRACE_GRP_CUSTOM, channel, "Execute:%d", pd_data->execute_lldp);
}

void pd69200_individual_mask_set(mesa_port_no_t iport, u8 mask, u8 val)
{
    T_DG(VTSS_TRACE_GRP_CUSTOM, "Mask:%d, val:%d", mask, val);
    uchar buf[PD_BUFFER_SIZE] = {COMMAND_KEY,
                                 DUMMY_SEQ_NUM,
                                 0x07,
                                 0x56,
                                 mask,
                                 val,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE
                                };

    (void) pd69200_i2c_tx(iport, &buf[0], __LINE__);
}

static void pd69200_10sec_reset_enable(mesa_port_no_t iport)
{
    // From mail
    // The 10 sec is a new feature, which we have implemented recently in our firmware for the old generation ICs (PD69200), and in the first firmware for the new generation ICs (690xx, the one you have).
    // It is not updated yet in the document, it is just mentioned in the release note for the firmware (of the old generation ICs).
    // Please see section 3 in the attached release note.
    // You can set it according to section 4.5.20
    // The mask key is 0x1B (this mask is not shared with our customers).
    // 0=disable
    // 1= enable

    T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "%s", "Entering pd69200_10sec_reset_enable");
    uchar buf[PD_BUFFER_SIZE] = {COMMAND_KEY,
                                 DUMMY_SEQ_NUM,
                                 0x07,
                                 0x56,
                                 0x1B,
                                 0x01,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE
                                };
    (void) pd69200_i2c_tx(iport, &buf[0], __LINE__);
    ten_sec_i2c_reset_enabled = TRUE;
}

// Section 4.7.7
static int pd69200_get_active_power_bank(mesa_port_no_t iport)
{
    uchar buf[PD_BUFFER_SIZE] = {REQUEST_KEY,
                                 DUMMY_SEQ_NUM,
                                 0x07,
                                 0x0B,
                                 0x17,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE
                                };

    if (pd69200_i2c_tx(iport, &buf[0], __LINE__)) {
        T_DG(VTSS_TRACE_GRP_CUSTOM, "Active power bank = %d, power limit = 0x%X , 0x%X", buf[9], buf[10], buf[11]);
        return buf[9];
    } else {
        T_WG(VTSS_TRACE_GRP_CUSTOM, "Response error - setting active bank to 0");
        print_buffer(&buf[0], PD_BUFFER_SIZE, __LINE__);
        return 0;
    }
}


// Section 4.7.5 - Getting system status
static BOOL pd69200_get_system_status(mesa_port_no_t iport)
{
    uchar buf[PD_BUFFER_SIZE] = {REQUEST_KEY,
                                 DUMMY_SEQ_NUM,
                                 0x07,
                                 0x3D,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE
                                };

    if (pd69200_i2c_tx(iport, &buf[0], __LINE__)) {
        T_IG(VTSS_TRACE_GRP_CUSTOM, "Got system status");
        return TRUE;
    } else {
        T_IG(VTSS_TRACE_GRP_CUSTOM, "Didn't get system status");
        return FALSE;
    }
}

poe_power_source_t pd69200_get_power_source (void)
{
    if (vtss_board_type() == VTSS_BOARD_OCELOT_REF) {
        /* On Ocelot PCB120, 0xB is DC_A, 0xD is DC_B */
        if (0xD == pd69200_get_active_power_bank(0)) {
            return BACKUP;
        } else {
            return PRIMARY;
        }
    } else {
        return PRIMARY;
    }
}

// Section 4.5.3
void pd69200_save_system_settings(mesa_port_no_t iport)
{
    T_DG(VTSS_TRACE_GRP_CUSTOM, "pd69200_save_system_settings - Section 4.5.3");
    uchar buf[PD_BUFFER_SIZE] = {PROGRAM_KEY,
                                 DUMMY_SEQ_NUM,
                                 0x06,
                                 0x0F,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE
                                };
    (void) pd69200_i2c_tx(iport, &buf[0], __LINE__);
}

void pd69200_set_power_bank(mesa_port_no_t iport, int power_limit, uchar bank)
{
    //  pd69200_set_power_banks, Section 4.5.8 in user guide
    int max_shutdown_voltage = 570; // Set max. voltage for the primary supply to 57.0 Volt.
    int min_shutdown_voltage = 420; // Set min. voltage for the primary supply to 42.0 Volt.

    uchar guard_band = 0x2; // We set guard to 2W in order to get priority to take over as soon as possible.

    bank = pd69200_get_active_power_bank(iport);
    T_DG(VTSS_TRACE_GRP_CUSTOM, "bank:%d  new power_limit:0x%X, guard_band:0x%X", bank,  power_limit, guard_band);

    // Split integers into a high and a low byte
    uchar power_limit_l = power_limit & 0xFF;
    uchar power_limit_h = power_limit >> 8 & 0xFF;

    uchar max_shutdown_voltage_l = max_shutdown_voltage & 0xFF;
    uchar max_shutdown_voltage_h = max_shutdown_voltage >> 8 & 0xFF;

    uchar min_shutdown_voltage_l = min_shutdown_voltage & 0xFF;
    uchar min_shutdown_voltage_h = min_shutdown_voltage >> 8 & 0xFF;

    // Transmit the command
    uchar buf[PD_BUFFER_SIZE] = {COMMAND_KEY,
                                 DUMMY_SEQ_NUM,
                                 0x07,
                                 0x0B,
                                 0x57,
                                 bank,
                                 power_limit_h,
                                 power_limit_l,
                                 max_shutdown_voltage_h,
                                 max_shutdown_voltage_l,
                                 min_shutdown_voltage_h,
                                 min_shutdown_voltage_l,
                                 guard_band,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE
                                };

    print_buffer(&buf[0], PD_BUFFER_SIZE, __LINE__);

    if ((vtss_board_type() == VTSS_BOARD_JAGUAR2_CU48_REF) && (iport == 24)) {
        (void) pd69200_i2c_tx(iport, &buf[0], __LINE__);
        pd69200_save_system_settings(iport); // I think we have to save system settings at this point - See Section 4.5.8 - "Save System Settings".
    } else {
        (void) pd69200_i2c_tx(0, &buf[0], __LINE__);
        pd69200_save_system_settings(0); // I think we have to save system settings at this point - See Section 4.5.8 - "Save System Settings".
    }
}

// Section 4.5.8 in user guide
void pd69200_set_power_banks(mesa_port_no_t iport, int primary_power_limit, int backup_power_limit)
{

    if (!pd69200_is_base_port(iport)) {
        return;
    }

    static BOOL init_done = FALSE;
    static int last_primary_power_limit = 0;
    static int last_backup_power_limit = 0;

    if (!init_done || primary_power_limit != last_primary_power_limit || backup_power_limit != last_backup_power_limit) {
        last_primary_power_limit = primary_power_limit;
        last_backup_power_limit  = backup_power_limit;
        init_done = TRUE;
//      pd69200_set_power_bank(backup_power_limit, BACKUP_POWER_BANK);
//      pd69200_set_power_bank(primary_power_limit, PRIMARY_POWER_BANK);
        if ((vtss_board_type() == VTSS_BOARD_JAGUAR2_CU48_REF) && (iport == 0)) {
            pd69200_set_power_bank(iport, primary_power_limit, ALL_POWER_BANKS);
            pd69200_set_power_bank(iport + POE_PORTS_PER_CONTROLLER, primary_power_limit, ALL_POWER_BANKS);
        } else {
            pd69200_set_power_bank(iport, primary_power_limit, ALL_POWER_BANKS); // If both primary and backup is available the it is the primary supply that is used.
        }
    }
}


// Section 4.5.13 in user guide
void pd69200_set_enable_disable_channel(mesa_port_no_t iport, uchar enable, vtss_appl_poe_port_mode_t mode)
{
    uchar channel = pd69200_iport2channel(iport);

    uchar af_mode;
    // UG section 4.5.13 - AF Mask (PD69000 only): 0 - only IEEE802.3af operation; N - stay with the last mode (IEEE802.3af or IEEE802.3at).
    if (mode == VTSS_APPL_POE_MODE_POE) {
        af_mode = 0; // AF mode
    } else {
        af_mode = 1;  // N = 1 according to mail from micsroSemi.
    }

    T_DG(VTSS_TRACE_GRP_CUSTOM, "pd69200_set_enable_disable_channel - Section 4.5.13 in user guide, channel:%d, enable:%d", channel, enable);
    uchar buf[PD_BUFFER_SIZE] = {COMMAND_KEY,
                                 DUMMY_SEQ_NUM,
                                 0x05,
                                 0x0C,
                                 channel,
                                 enable,
                                 af_mode,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE
                                };
    (void) pd69200_i2c_tx(iport, &buf[0], __LINE__);
}

void pd69200_set_power_limit_channel(mesa_port_no_t iport, uint ppl )
{
    uchar channel = pd69200_iport2channel(iport);

    // We uses deci Watts while pd6920 uses mili Watts, so we have to do a convertion
    ppl = ppl  * 100;
    ppl = ppl * 1.05; // According to the PoE team the chipset added 5% to power when uses "automatically" mode, so doing the same when
    // we use fixed limits
    uchar ppl_l, ppl_h;

    // Section 4.5.14 in user guide
    uint max_port_power = poe_custom_get_port_power_max(iport) * 100; // Getting max port power in milli watt
    if (ppl > max_port_power) {
        ppl = max_port_power;
    }
    // convert from integer to two bytes
    ppl_l = ppl & 0xFF;
    ppl_h = ppl >> 8 & 0xFF;

    T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "ppl:%d, ppl_l:%d, ppl_h:%d", ppl, ppl_l, ppl_h);

    uchar buf2[PD_BUFFER_SIZE] = {COMMAND_KEY,
                                  DUMMY_SEQ_NUM,
                                  0x05,
                                  0x0B,
                                  channel,
                                  ppl_h,
                                  ppl_l,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE
                                 };
    (void) pd69200_i2c_tx(iport, &buf2[0], __LINE__);
    // Section 4.5.15 in user guide

    uchar buf[PD_BUFFER_SIZE] = {COMMAND_KEY,
                                 DUMMY_SEQ_NUM,
                                 0x05,
                                 0xA2,
                                 channel,
                                 ppl_h,
                                 ppl_l,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE
                                };
    (void) pd69200_i2c_tx(iport, &buf[0], __LINE__);
}

//  Section 4.5.16 in PD69200 user guide
void pd69200_set_port_priority(mesa_port_no_t iport, vtss_appl_poe_port_power_priority_t priority )
{
    uchar channel = pd69200_iport2channel(iport);

    static BOOL init_done = FALSE;
    static CapArray<vtss_appl_poe_port_power_priority_t, MESA_CAP_PORT_CNT> last_priority;

    if (!init_done) {
        last_priority.clear();
        init_done = TRUE;
    }

    if (last_priority[channel] != priority) {
        last_priority[channel] = priority;

        T_DG_PORT(VTSS_TRACE_GRP_CUSTOM, channel, "prio:%d, channel:%d", priority, channel);
        uchar pd69200_prio = 3; // Default to low priority

        // Type conversion -- See section 4.5.16 in PD69200 user guide
        switch (priority) {
        case VTSS_APPL_POE_PORT_POWER_PRIORITY_LOW :
            pd69200_prio = 3;
            break;
        case VTSS_APPL_POE_PORT_POWER_PRIORITY_HIGH:
            pd69200_prio = 2;
            break;
        case VTSS_APPL_POE_PORT_POWER_PRIORITY_CRITICAL:
            pd69200_prio = 1;
            break;
        default:
            T_WG(VTSS_TRACE_GRP_CUSTOM, "Unkown priority");
        }

        uchar buf[PD_BUFFER_SIZE] = {COMMAND_KEY,
                                     DUMMY_SEQ_NUM,
                                     0x05,
                                     0x0A,
                                     channel,
                                     pd69200_prio,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE
                                    };
        (void) pd69200_i2c_tx(iport, &buf[0], __LINE__);
    }
}

// Section 4.7.5 in user guide
// Reads system status without requesting it. Needed after reset - see section 4.1.
BOOL pd69200_rd_system_status_ok(mesa_port_no_t iport)
{
    uchar buf[PD_BUFFER_SIZE];

    // From section 4.1.1 -
    // As part of rebooting, a System Status Telemetry message will be transmitted back to the
    // Host within *TWAKEUP.  *TWAKEUP = 300msec typical depending on system architecture.
    VTSS_MSLEEP(300);

    // Get response -- Check that response is valid
    if (get_controller_response(iport, &buf[0], 0xFF)) {
        char cpu_status1     = buf[3];
        char cpu_status2     = buf[4];
//        char factory_default = buf[5];
//        char gie             = buf[6];
//        char private_label   = buf[7];
//        char device_fail     = buf[9];
//        char temp_disconnect = buf[10];
//        char temp_alarm      = buf[11];
//        int  interrupt       = buf[12] << 8 & buf[13];

        if (cpu_status1 == 0x1) {
            T_WG(VTSS_TRACE_GRP_CUSTOM, "PoE controller firmware download is required");
        }

        if (cpu_status2 == 0x1) {
            T_WG(VTSS_TRACE_GRP_CUSTOM, "PoE controller memory error");
        }

        return TRUE;

    } else {
        T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "System data read failure, 0x%X 0x%X 0x%X", buf[0], buf[1], buf[2]);
        print_buffer(&buf[0], PD_BUFFER_SIZE, __LINE__);
        return FALSE;
    }
}

void pd69200_restore_factory_default(mesa_port_no_t iport, BOOL trace = TRUE)
{
    T_DG(VTSS_TRACE_GRP_CUSTOM, "pd69200_restore_factory_default - Section 4.1.2");
    uchar read_buf[PD_BUFFER_SIZE];
    uchar buf[PD_BUFFER_SIZE] = {PROGRAM_KEY,
                                 DUMMY_SEQ_NUM,
                                 0x2D,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE
                                };
    if (!is_i2c_tx_ok(iport, &buf[0])) {
        T_WG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "%s", "Transmission error.");
        // Still try reading the response normally, just in case a transmission error was only seen on the host-side.
    }
    VTSS_MSLEEP(100); // Wait 100ms (4.1.2 note 1)

    if (pd69200_rd(iport, &read_buf[0], PD_BUFFER_SIZE) != VTSS_RC_OK) {
        T_WG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "%s", "Could not read response.");
    } else {
        print_buffer(&read_buf[0], PD_BUFFER_SIZE, __LINE__, trace);
    }
}

// Section 4.5.1 in user guide
// Returns TRUE if reset were done correct else FALSE
void pd69200_reset_command (mesa_port_no_t iport)
{
    if (!firmware_upgrade_in_progress) {
        T_IG(VTSS_TRACE_GRP_CUSTOM, "pd69200_reset_command");

        // Empty i2c buffer.
        uchar dummny;
        auto i = 20;
        while (i--) {
            pd69200_rd(iport, &dummny, 1);
        }


        uchar buf[PD_BUFFER_SIZE] = {COMMAND_KEY,
                                     DUMMY_SEQ_NUM,
                                     0x07,
                                     0x55,
                                     0x00,
                                     0x55,
                                     0x00,
                                     0x55,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE,
                                     DUMMY_BYTE
                                    };

        (void) is_i2c_tx_ok(iport, &buf[0]);

        // After reset we look for the REPORT KEY to show up
        BOOL found = FALSE;
        i = 50; // We do read maximum 50 bytes before giving up.
        while (i--) {
            if (pd69200_rd(iport, buf, 1) != VTSS_RC_OK) {
                T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "Could not read reset response. i:%d", i);
                found = FALSE;
                break;
            }

            if (buf[0] == REPORT_KEY) {
                found = TRUE;
                break;
            }

            T_DG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "Wating... %d", i);
            VTSS_MSLEEP(100); // OK, not an expected byte, give PoE controller a little more time to come up, before checking again.
        }

        if (found) {
            T_IG(VTSS_TRACE_GRP_CUSTOM, "Report key found after reset command");
            // Read rest of the response in order to clear buffer
            if (pd69200_rd(iport, &buf[1], PD_BUFFER_SIZE - 1) != VTSS_RC_OK) {
                found = FALSE;
                T_IG(VTSS_TRACE_GRP_CUSTOM, "Rest of response not OK.");
            }
            print_buffer(buf, PD_BUFFER_SIZE, __LINE__);
        } else {
            T_IG(VTSS_TRACE_GRP_CUSTOM, "Report key NOT found after reset command");
        }

        (void) get_seq_num(TRUE); // Reset the sequence number
    }
}

// section 4.7.17 in user guide
void pd69200_get_port_measurement(mesa_port_no_t iport, poe_status_t *poe_status)
{
    uchar channel = pd69200_iport2channel(iport);
    int current_used;
    int power_used;
    T_RG(VTSS_TRACE_GRP_CUSTOM, "Entering pd69200_get_port_measurement");

    // Send request to get status
    uchar buf[PD_BUFFER_SIZE]  = {REQUEST_KEY,
                                  DUMMY_SEQ_NUM,
                                  0x05,
                                  0x25,
                                  channel,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE
                                 };

    // Get response -- Check that response is valid
    if (pd69200_i2c_tx(iport, &buf[0], __LINE__)) {
        power_used = (buf[6] << 8) + buf[7];
        current_used = (buf[4] << 8) + buf[5];
    } else {
        T_WG(VTSS_TRACE_GRP_CUSTOM, "Data did not contain valid data.");
        power_used = 0;
        current_used = 0;
    };

    // Update the status structure
    poe_status->power_used[iport] = power_used / 100; // We use deci watts while PD69200 uses mili watts ( That why we divides with 100 )-
    poe_status->current_used[iport] = current_used;

    T_NG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "power_used = %u, current_used = %u", power_used, current_used);
}

// Get software version - Section 4.7.1 in user guide
poe_firmware_version_t pd69200_get_sw_version(mesa_port_no_t iport)
{
    poe_firmware_version_t version;

    // Send request to get sw version
    uchar buf[PD_BUFFER_SIZE]  = {REQUEST_KEY,
                                  DUMMY_SEQ_NUM,
                                  0x07,
                                  0x1E,
                                  0x21,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE
                                 };

    T_DG(VTSS_TRACE_GRP_CUSTOM, "pd69200_get_sw_version");

    // Get response -- Check that response is valid
    if (!pd69200_i2c_tx(iport, &buf[0], __LINE__)) {
        T_DG(VTSS_TRACE_GRP_CUSTOM, "Data did not contain valid data.");
    };

    version.hw_ver = buf[2];
    version.sw_ver = (buf[5] << 8) + buf[6];
    version.param  = buf[7];
    version.internal_sw_ver = (buf[9] << 8) + buf[10];

    T_DG(VTSS_TRACE_GRP_CUSTOM, "HW Ver.:%d, Prod:%d, sw ver:%d, build:%d, internal sw ver:%d, Asic Patch Num:%d",
         buf[2], buf[4], (buf[5] << 8) + buf[6], buf[8], (buf[9] << 8) + buf[10], (buf[11] << 8) + buf[12]);

    return version;
}

//  Power management - Section 4.5.9
void pd69200_set_pm_method(mesa_port_no_t iport, vtss_appl_poe_management_mode_t power_mgmt_mode)
{
    if (!pd69200_is_base_port(iport)) {
        return;
    }

    T_DG(VTSS_TRACE_GRP_CUSTOM, "pd69200_set_pm_method - Section 4.5.9");
    uchar pm1 = 0x00;
    uchar pm2 = 0x00;
    uchar pm3 = 0x00;
    uchar layer2_mode = 0; // Default not layer2 mode - See table in section 8.2

    switch (power_mgmt_mode) {
    case VTSS_APPL_POE_CLASS_CONSUMP :
        // See table in section 4.5.9 in PD69200 user guide
        pm1 = 0x00;
        pm2 = 0x01;
        pm3 = 0x00;
        break;

    case VTSS_APPL_POE_CLASS_RESERVED :
        // See table in section 4.5.9 in PD69200 user guide
        pm1 = 0x04;
        pm2 = 0x01;
        pm3 = 0x00;
        break;

    case VTSS_APPL_POE_ALLOCATED_RESERVED:
        // The actually Max Reserved power is done at the user interface level

        // See table in section 4.5.9 in PD69200 user guide
        pm1 = 0x00;
        pm2 = 0x00;
        pm3 = 0x00;

        break;

    case VTSS_APPL_POE_ALLOCATED_CONSUMP:
        // See table in section 4.5.9 in PD69200 user guide
        pm1 = 0x00;
        pm2 = 0x00;
        pm3 = 0x00;

        break;

    case VTSS_APPL_POE_LLDPMED_CONSUMP:
        // See table in section 4.5.9 in PD69200 user guide
        pm1 = 0x00;
        pm2 = 0x00;
        pm3 = 0x00;
        layer2_mode = 1; // Layer2 mode - See table in section 8.2
        break;

    case VTSS_APPL_POE_LLDPMED_RESERVED:
        // The actually Max Reserved power is done in LLDP module
        pm1 = 0x04;
        pm2 = 0x00;
        pm3 = 0x00;
        layer2_mode = 1; // Layer2 mode - See table in section 8.2
        break;
    default:
        T_WG(VTSS_TRACE_GRP_CUSTOM, "Unknown managedment mode");
    }

    pd69200_individual_mask_set(iport, 0x2E, layer2_mode); // Enable layer 2 in case we are in any LLDP mode.

    T_NG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "Power management pm1:%d, pm2:%d, pm3:%d", pm1, pm2, pm3);
    // Send the Power management command -See Section 4.5.9 in PD69200 user guide
    uchar buf[PD_BUFFER_SIZE] = {COMMAND_KEY,
                                 DUMMY_SEQ_NUM,
                                 0x07,
                                 0x0B,
                                 0x5F,
                                 pm1,
                                 pm2,
                                 pm3,
                                 0x00,
                                 0x00,
                                 0x00,
                                 0x00,
                                 0x00,
                                 0x00,
                                 0x00
                                };
    (void) pd69200_i2c_tx(iport, &buf[0], __LINE__);
}


//  Set system masks - Section 4.5.10
static void pd69200_set_system_masks (mesa_port_no_t iport, uchar mask)
{
    mask |= 1 << 2; // maskbit2 must be 1 according to section 4.5.10.
    // Send the system masks command.
    uchar buf[PD_BUFFER_SIZE] = {COMMAND_KEY,
                                 DUMMY_SEQ_NUM,
                                 0x07,
                                 0x2B,
                                 mask,
                                 0x00,
                                 0x00,
                                 0x00,
                                 0x00,
                                 0x00,
                                 0x00,
                                 0x00,
                                 0x00,
                                 0x00,
                                 0x00
                                };

    T_DG(VTSS_TRACE_GRP_CUSTOM, "mask:%d", mask);
    (void) pd69200_i2c_tx(iport, &buf[0], __LINE__);
}

// section 4.7.6 in user guide - Getting systems mask
// In/Out - Pointer to where to put the mask result.
static void pd69200_get_system_mask(mesa_port_no_t iport, char *mask)
{
    T_DG(VTSS_TRACE_GRP_CUSTOM, "pd69200_get_system_mask");

    // Send request to get status
    uchar buf[PD_BUFFER_SIZE]  = {REQUEST_KEY,
                                  DUMMY_SEQ_NUM,
                                  0x07,
                                  0x2B,
                                  0x00,
                                  0x00,
                                  0x00,
                                  0x00,
                                  0x00,
                                  0x00,
                                  0x00,
                                  0x00,
                                  0x00,
                                  0x00,
                                  0x00
                                 };

    // Get response -- Check that response is valid
    if (pd69200_i2c_tx(iport, &buf[0], __LINE__)) {
        *mask = buf[2];
    } else {
        T_WG(VTSS_TRACE_GRP_CUSTOM, "Did not get system mask data.");
        *mask = 0;
    };
    T_DG(VTSS_TRACE_GRP_CUSTOM, "mask:%d", *mask);
}

// Debug function for doing register access, and print out the response
void pd69200_debug_access(mesa_port_no_t iport, uchar key, uchar subject, uchar subject1, uchar subject2,
                          uchar data0, uchar data1, uchar data2, uchar data3, uchar data4, uchar data5, uchar data6, uchar data7)
{
    // Send request
    uchar buf[PD_BUFFER_SIZE]  = {key,
                                  DUMMY_SEQ_NUM,
                                  subject,
                                  subject1,
                                  subject2,
                                  data0,
                                  data1,
                                  data2,
                                  data3,
                                  data4,
                                  data5,
                                  data6,
                                  data7
                                 };

    // Get response -- Check that response is valid
    if (pd69200_i2c_tx(iport, &buf[0], __LINE__)) {
        print_buffer(buf, PD_BUFFER_SIZE, __LINE__, FALSE);
    } else {
        T_WG(VTSS_TRACE_GRP_CUSTOM, "Could not transmit.");
    };
}

// Function for setting the legacy capacitor detection mode
//      enable         : True - Enable legacy capacitor detection
void pd69200_capacitor_detection_set(mesa_port_no_t iport, BOOL enable)
{
    char mask;

    pd69200_get_system_mask(iport, &mask);

    T_DG(VTSS_TRACE_GRP_CUSTOM, "mask:%d", mask);
    if (enable) {
        // Enable Cap detection
        pd69200_set_system_masks(iport, mask | 0x2); // Bit 1 is cap detection- High is enable  (Section 4.5.10)

        //pd69200_individual_mask_set(0x1, 0x1);
    } else {
        // Disable Cap detection
        pd69200_set_system_masks(iport, mask & ~0x2); // Bit 1 is cap detection- Low is disable  (Section 4.5.10)
        //pd69200_individual_mask_set(0x1, 0x0);
    }
}

// Function for getting the legacy capacitor detection mode
BOOL pd69200_capacitor_detection_get(mesa_port_no_t iport)
{
    char mask = 0;

    pd69200_get_system_mask(iport, &mask);

    T_IG(VTSS_TRACE_GRP_CUSTOM, "mask:%d", mask);
    // capacitor detection is bit 1 (See UG - section 4.7.6.)
    if (mask & 0x2) {
        return TRUE;
    } else {
        return FALSE;
    }
}

static mesa_port_list_t new_pd_detected;
static CapArray<vtss_appl_poe_status_type_t, MESA_CAP_PORT_CNT> prev_status;

BOOL pd69200_new_pd_detected_get(mesa_port_no_t iport, BOOL clear)
{
    BOOL current_val = new_pd_detected[iport];
    if (clear) {
        new_pd_detected[iport] = FALSE;
    }
    return current_val;
}

//  Getting status for a single port using "Get Single Port Status"
//  In : iport - The port for which to get status (starting from 0)
//  Out : port_status - pointer to where to put the status for the port (or NULL)
//  Out : class_v     - pointer to where to put the class info for the port (or NULL)
void pd69200_port_status_get (mesa_port_no_t iport, vtss_appl_poe_status_type_t *port_status, char *class_v)
{
    vtss_appl_poe_status_type_t port_status_v;

    uchar channel = pd69200_iport2channel(iport);

    // Send request to get status
    uchar buf[PD_BUFFER_SIZE]  = {REQUEST_KEY,
                                  DUMMY_SEQ_NUM,
                                  0x05,
                                  0x0E,
                                  channel,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE,
                                  DUMMY_BYTE
                                 };

    // Get response -- Check that response is valid
    if (pd69200_i2c_tx(iport, buf, __LINE__)) {

        if (port_status) {
            // See table 4 in the user guide for understanding the conversion - we do not support all status values.
            switch (buf[3]) {
            case 0x0:
            case 0x1:
                port_status_v = VTSS_APPL_POE_PD_ON;
                break;

            case 0x1A:
                port_status_v = VTSS_APPL_POE_PD_OFF;
                break;

            case 0x1B: // Interim state, detection going on
                if ((prev_status[iport] == VTSS_APPL_POE_PD_OFF) || (prev_status[iport] == VTSS_APPL_POE_PD_ON)) {
                    port_status_v = VTSS_APPL_POE_NO_PD_DETECTED;
                } else {
                    port_status_v = prev_status[iport];
                }
                break;

            case 0x1C: // We have seen "False Positives" when conencted to another switch.
            case 0x1E: // We heen seen "False Positives" when conencted to another switch.
                port_status_v = VTSS_APPL_POE_NO_PD_DETECTED;
                break;

            case 0x1F:
                port_status_v = VTSS_APPL_POE_PD_OVERLOAD;
                break;

            case 0x20:
            case 0x3C:
                port_status_v = VTSS_APPL_POE_POWER_BUDGET_EXCEEDED;
                break;

            case 0x02:
            case 0x03:
            case 0x04:
            case 0x06:
            case 0x07:
            case 0x08:
            case 0x0C:
            case 0x11:
            case 0x12:
            case 0x1D:
            case 0x21:
            case 0x24:
            case 0x25:
            case 0x26:
            case 0x2B:
            case 0x2C:
            case 0x2D:
            case 0x2E:
            case 0x2F:
            case 0x31:
            case 0x32:
            case 0x33:
            case 0x34:
            case 0x35:
            case 0x36:
            case 0x37:
            case 0x38:
            case 0x39:
            case 0x3A:
            case 0x3D:
            case 0x3E:
            case 0x3F:
            case 0x43:
            case 0x44:
            case 0x45:
            case 0x46:
            case 0x47:
                T_RG(VTSS_TRACE_GRP_CUSTOM, "Port status: 0x%X", buf[3] );
                port_status_v = VTSS_APPL_POE_UNKNOWN_STATE;
                break;

            default:
                // This shall never happen all states should be covered.
                T_WG(VTSS_TRACE_GRP_CUSTOM, "Unknown port status: 0x%X", buf[3] );
                port_status_v = VTSS_APPL_POE_PD_OFF;
                break;

            }

            if ((port_status_v == VTSS_APPL_POE_PD_ON) && (prev_status[iport] != VTSS_APPL_POE_PD_ON)) {
                new_pd_detected[iport] = TRUE;
            }

            T_IG_PORT(VTSS_TRACE_GRP_STATUS, iport, "buf[3]:0x%02x, status:%d, prev:%d, detect:%d", buf[3], port_status_v, prev_status[iport], new_pd_detected.get(iport));
            *port_status = prev_status[iport] = port_status_v;
        }

        if (class_v) {
            *class_v = buf[6];
        }

    } else {
        T_WG(VTSS_TRACE_GRP_CUSTOM, "Data did not contain valid data.");
    }
}

static mesa_rc pd69200_clear_device_buffer(mesa_port_no_t iport);

static poe_chip_found_t pd69200_do_detection_(mesa_port_no_t iport)
{

    if (!port_custom_table[iport].poe_support ||
        pd69200_clear_device_buffer(iport) != MESA_RC_OK) {
        return NOT_FOUND;
    }

    (void) pd69200_reset_command(iport);

    poe_chip_found_t chip_found = NOT_FOUND;

    if (pd69200_rd_system_status_ok(iport)) {
        T_IG(VTSS_TRACE_GRP_CUSTOM, "Chip found by check_startup_response");
        chip_found = FOUND;
    }

    pd69200_sync(iport); // For some reason we have seen that the I2C buffer isn't empty after reset, so we empty it (pd69200_sync is doing that)

    if (chip_found == NOT_FOUND) {
        if (pd69200_get_system_status(iport)) {
            chip_found = FOUND;
            T_IG(VTSS_TRACE_GRP_CUSTOM, "Chip found by Calling pd69200_get_system_status");
        }
    }

    T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "Chip found:%d", chip_found);
    if (chip_found == FOUND) {
        pd69200_10sec_reset_enable(iport); // Setup the chipset to reset the i2c circuit in the poe chipset if there have 10 sec with no access
    }
    return chip_found;
}

// Function that returns 1 is a PoE PD69200 chip set is found, else 0.
// The chip is detected by reading the sw version. If the I2C access fails
// we say that the pd69200 chip isn't available
int pd69200_is_chip_available(mesa_port_no_t iport)
{
    T_RG(VTSS_TRACE_GRP_CUSTOM, "Support:%d, firmware:%d, found:%d", port_custom_table[iport].poe_support, firmware_upgrade_in_progress, pd69200_chip_found_get(iport));
    if (!port_custom_table[iport].poe_support || firmware_upgrade_in_progress) {
        return FALSE;
    }

    if (pd69200_chip_found_get(iport) == FOUND) {
        return TRUE;
    }
    return FALSE;
}

// For auto-detection of PD69200 chipset.
void pd69200_do_detection(mesa_port_no_t iport)
{
    // We only want to try to detect the chipset once.
    if (pd69200_chip_found_get(iport) == DETECTION_NEEDED) {
        pd69200_chip_found_set(iport, pd69200_do_detection_(iport));
    }
}

// Function for writing data from the MicroSemi micro-controller.
// IN  : Data - Pointer to data to write
//     : Size - Number of bytes to write.
int pd69200_wr(mesa_port_no_t iport, uchar *data, char size)
{
    poe_custom_entry_t poe_hw_conf;

    T_NG(VTSS_TRACE_GRP_CUSTOM, "WRITE");
    T_RG_HEX(VTSS_TRACE_GRP_CUSTOM, data, size);
    poe_hw_conf = poe_custom_get_hw_config(0, &poe_hw_conf);

    int i2c_dev;
    if (open_i2c_dev(iport, &i2c_dev) != VTSS_RC_OK) {
        return 0;
    }

    if (vtss_i2c_dev_wr(i2c_dev, data, size) != VTSS_RC_OK) {
        T_WG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "I2C write problem - size:%d", size);
        poe_halt_at_i2c_err(TRUE);
        size = 0;
    }

    return size;
}

// Function for reading data from the MicroSemi micro-controller.
// IN/OUT : Data - Pointer to where to put the read data
// IN     : Size - Number of bytes to read.
mesa_rc pd69200_rd(mesa_port_no_t iport, uchar *data, char size)
{
    mesa_rc rc = VTSS_RC_OK;
    VTSS_OS_MSLEEP(pd69200_wait_time);
    poe_custom_entry_t poe_hw_conf;
    poe_hw_conf = poe_custom_get_hw_config(0, &poe_hw_conf);

    //    int i2c_dev;
    int i2c_dev;
    VTSS_RC(open_i2c_dev(iport, &i2c_dev));
    rc =  vtss_i2c_dev_rd(i2c_dev, data, size);

    T_NG(VTSS_TRACE_GRP_CUSTOM, "READ");
    T_RG_HEX(VTSS_TRACE_GRP_CUSTOM, data, size);
    if (rc == VTSS_RC_ERROR) {
        T_WG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "I2C read problem, rc:0x%X", rc);
        poe_halt_at_i2c_err(TRUE);
    }
    return rc;
}

// Set up the channel-to-port mapping matrix on all PD69200 chips found.
void pd69200_setup_port_matrix(void)
{
    for (auto iport = 0; iport < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); iport++) {
        if ((pd69200_chip_found_get(iport) == FOUND) && (pd69200_is_base_port(iport))) {
            pd69200_temp_matrix_init(iport);
        }
    }
    for (auto iport = 0; iport < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); iport++) {
        if (pd69200_chip_found_get(iport) == FOUND) {
            pd69200_temp_matrix_set(iport);
        }
    }
    for (auto iport = 0; iport < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); iport++) {
        if ((pd69200_chip_found_get(iport) == FOUND) && (pd69200_is_base_port(iport))) {
            pd69200_program_global_matrix(iport);
        }
    }
}

// Things that is needed after a boot
void pd69200_poe_init(void)
{
    poe_pd_data_t pd_data;
    memset(&pd_data, 0, sizeof(pd_data));
    new_pd_detected.clear_all();
    for (auto iport = 0; iport < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); iport++) {
        if (pd69200_chip_found_get(iport) == FOUND) {
            if (pd69200_is_base_port(iport)) {
                pd69200_restore_factory_default(iport);
                //pd69200_program_global_matrix(iport);
                pd69200_set_system_masks(iport, 0x6); // Turn off lowest priority port, when a higher priority has a PD connected,see section 4.5.10
                //pd69200_temp_matrix_init(iport);
            }
            //pd69200_temp_matrix_set(iport);
            pd69200_pd_data_set(iport, &pd_data);
        }
    }
    pd69200_setup_port_matrix();
}

//***************************************************************************************************************
//  Code below is used to update the PoE chipset firmware. It is based upon microSemi TN-140_06-0024-081 document
// **************************************************************************************************************
// Send the enter command as described in sw conf guide section 5.2
static mesa_rc sendEnter(mesa_port_no_t iport)
{
    const u8 enter[] = { (u8)'E', (u8)'N', (u8)'T', (u8)'R' };

    T_IG(VTSS_TRACE_GRP_CUSTOM, "Sending ENTR");

    (void) pd69200_wr(iport,  (uchar *) &enter[0], 1);
    VTSS_MSLEEP(10); // Section 5.2 - Keep at least 10 ms delay between each transmitted character.

    (void) pd69200_wr(iport, (uchar *) &enter[1], 1);
    VTSS_MSLEEP(10); // Section 5.2 - Keep at least 10 ms delay between each transmitted character.

    (void) pd69200_wr(iport, (uchar *) &enter[2], 1);
    VTSS_MSLEEP(10); // Section 5.2 - Keep at least 10 ms delay between each transmitted character.

    (void) pd69200_wr(iport, (uchar *) &enter[3], 1);

    return VTSS_RC_OK;
}

// Checking if PoE firmware is valid (Dor selecting the firmware upgrade process)
static BOOL is_firmware_valid(mesa_port_no_t iport)
{
    uchar buf[PD_BUFFER_SIZE] = {REQUEST_KEY,
                                 DUMMY_SEQ_NUM,
                                 0x07,
                                 0x3D,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE,
                                 DUMMY_BYTE
                                };
    if (pd69200_i2c_tx(iport, &buf[0], __LINE__)) {
        // See section 4.1.6 "Get System Status", CPU status
        T_IG(VTSS_TRACE_GRP_CUSTOM, "CPU status:0x%X", buf[2]);
        if (buf[2] & 0x2) { // From sw conf guide - Bit1 = '1', indicates that firmware download is required. (Telemetry structure change 4.1.6.3)
            return FALSE;
        }
        return TRUE;
    }
    return FALSE;
}

static mesa_rc firmwareDownloadStep1(mesa_port_no_t iport)
{
    // From sw conf guide
    // 5.4 - I2C Download Process
    // Send 'Get System Status' message (in single byte) to check firmware validity (CPU status-1 Bit 1 = 1 indicates invalid firmware).
    // If valid firmware is detected, perform the downloading process as described in Section 5.1.
    // If invalid firmware is detected, perform the downloading process as described in Section 5.2.
    BOOL valid = is_firmware_valid(iport);

    if (valid) {
        // Section 5.1
        u8 download[15] = { 0x01, 0x01, 0xFF, 0x99, 0x15, 0x16, 0x16, 0x99, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x74 };
        T_IG(VTSS_TRACE_GRP_CUSTOM, "Start download");
        if (!pd69200_i2c_tx(iport, &download[0], __LINE__)) {
            T_IG(VTSS_TRACE_GRP_CUSTOM, "download problem");
            return VTSS_RC_ERROR;
        }
    } else {
        // Section 5.2
        sendEnter(iport);
    }

    firmware_upgrade_in_progress = TRUE; // Signal to PoE code to stop access to the PoE chipset.
    pd69200_chip_found_set(iport,  NOT_FOUND);

    return VTSS_RC_OK;
}

#define LINE_SIZE_MAX 100 // PoE firmware file line max size in bytes


static mesa_rc firmwareVersionGet (const char *microsemi_firmware, poe_firmware_version_t &version_get)
{
    const string swNum = "Software Number:";
    string str(microsemi_firmware);
    std::string::size_type pos = str.find(swNum, '\n');
    std::string ver_str = str.substr(pos + swNum.length(), str.find('\n'));

    if (pos == std::string::npos) {
        return VTSS_APPL_POE_ERROR_FIRMWARE_VER;
    }

    version_get.sw_ver = std::stoi(ver_str);
    T_DG(VTSS_TRACE_GRP_CUSTOM, "version_get:%d", version_get.sw_ver);

    const string param = "Param Number:";
    pos = str.find(param, '\n');
    std::string param_str = str.substr(pos + param.length(), str.find('\n'));
    version_get.param     = std::stoi(param_str);
    return VTSS_RC_OK;
}

static BOOL is_firmware_new(mesa_port_no_t iport, const char *microsemi_firmware)
{
    poe_firmware_version_t firm_ver;
    if (firmwareVersionGet(microsemi_firmware, firm_ver) != VTSS_RC_OK) {
        return TRUE; // Could not determine version, so act as it is new.
    }

    poe_firmware_version_t current_ver = pd69200_get_sw_version(iport);
    T_IG(VTSS_TRACE_GRP_CUSTOM, "firm_ver:%d.%d, current_ver:%d.%d", firm_ver.sw_ver, firm_ver.param, current_ver.sw_ver, current_ver.param);
    if (firm_ver.sw_ver == current_ver.sw_ver && firm_ver.param == current_ver.param) {
        return FALSE;
    }

    return TRUE;
}

// Function for doing the firmware update
mesa_rc pd69200_DownloadFirmwareFunc(mesa_port_no_t iport, const char *microsemi_firmware, size_t firmware_size)
{
    if ((pd69200_chip_found_get(iport) != FOUND) || !is_firmware_new(iport, microsemi_firmware)) {
        T_IG(VTSS_TRACE_GRP_CUSTOM, "Not New");
        return VTSS_APPL_POE_ERROR_FIRMWARE_VER_NOT_NEW;
    }
    T_IG(VTSS_TRACE_GRP_CUSTOM, "New Firmware found");

    u32 byte_cnt = 0;
    u8 progress = 0;
    u8 prev_progress = 255;
    char *byteArr;

    u8 byte_Arr[15];

    T_IG(VTSS_TRACE_GRP_CUSTOM, "Starting PoE firmware update");
    VTSS_RC(firmwareDownloadStep1(iport));

    VTSS_MSLEEP(100); // read TPE\r\n - Section 5.1 - step 2 - response may take 100 ms
    pd69200_rd(iport, &byte_Arr[0], 5); // read TPE\r\n - Section 5.1 - step 2

    // Section 5.1 - step 3
    T_IG(VTSS_TRACE_GRP_CUSTOM, " Sending 'E' - erasing memory - Section 5.1 - step 3");
    byte_Arr[0] = ((u8)'E');
    (void) pd69200_wr(iport, (uchar *) &byte_Arr[0], 1);
    pd69200_rd(iport, &byte_Arr[0], 5);// read TOE\r\n - Section 5.1 - step 3;

    T_IG(VTSS_TRACE_GRP_CUSTOM, "Erasure may last up to 5 seconds. - Section 5.1 - step 4");
    VTSS_MSLEEP(5000);
    pd69200_rd(iport, &byte_Arr[0], 4); // read TE\r\n - Section 5.1 - step 4
    VTSS_MSLEEP(100);
    pd69200_rd(iport, &byte_Arr[0], 5); // read TPE\r\n - Section 5.1 - step 4
    VTSS_MSLEEP(100);

    byte_Arr[0] = ((u8)'P'); // Sending 'P' - program memory - Section 5.1 - step 5
    (void) pd69200_wr(iport, &byte_Arr[0], 1);
    pd69200_rd(iport, &byte_Arr[0], 5); // read TOP\r\n - Section 5.1 - step 5

    // If greater than 127 then we have reached end of file
    while ((byte_cnt <= firmware_size) && (*microsemi_firmware <= 127)) {
        byteArr = (char *)microsemi_firmware;

        // Print out progress
        progress = (byte_cnt * 100) / firmware_size;
        if (progress != prev_progress) {
            T_IG(VTSS_TRACE_GRP_CUSTOM, "PoE Firmware update progress:%d %%", progress);
            prev_progress = progress;
        }

        if (byteArr[0] == 10) {
            T_DG(VTSS_TRACE_GRP_CUSTOM, "Skipping LF (Line Feed)");
            microsemi_firmware++;
            byteArr =  (char *)microsemi_firmware;
        }

        if (byteArr[0] != 'S') {
            T_E("Line should start with S byteArr[0]:0x%X", byteArr[0]);
            break;
        }

        if (byteArr[1] == 0x30) { //skip line that starts with "S0"
            T_DG(VTSS_TRACE_GRP_CUSTOM, "Skipping lines starting with S0");

            // If greater than 127 then we have reached end of file
            while (*microsemi_firmware != '\n' && *microsemi_firmware < 127 && byte_cnt <= firmware_size) {
                byte_cnt++;
                T_NG(VTSS_TRACE_GRP_CUSTOM, "Skipping S0 lines microsemi_firmware:0x%X, byte_cnt:%u", *microsemi_firmware, byte_cnt);
                microsemi_firmware++;
            }

            // Lines Starting with S3 or S7
        } else if (byteArr[1] == 0x33 || byteArr[1] == 0x37) {
            T_DG(VTSS_TRACE_GRP_CUSTOM, "Lines Starting with S3 or S7 - Section 5.1 - step 6");

            u8 line[LINE_SIZE_MAX];
            u8 line_index = 0;
            // If greater than 127 then we have reached end of file
            while (*microsemi_firmware != '\n' && *microsemi_firmware < 127 && byte_cnt <= firmware_size) {
                line[line_index++] = *microsemi_firmware;
                microsemi_firmware++;
                byte_cnt++;
            }
            microsemi_firmware++; // For the "\n"

            T_DG(VTSS_TRACE_GRP_CUSTOM, "Writing new line - byte_cnt:%u", byte_cnt);
            line[line_index++] = (u8)'\r';
            line[line_index] = (u8)'\n';
            (void) pd69200_wr(iport, (uchar *)&line[0], line_index); // Write the line - Section 5.1 - step 6

            pd69200_rd(iport, (uchar *)&byteArr[0], 4); // reading T*\r\n or TP\r\n - Section 5.1 - step 6

            if (byteArr[0] == 0x54 && byteArr[1] == 0x2a) { // if read T*\r\n
                T_DG(VTSS_TRACE_GRP_CUSTOM, "NOT END of File");
            } else if (byteArr[0] == 0x54 && byteArr[1] == 0x50) { // if read TP\r\n
                T_IG(VTSS_TRACE_GRP_CUSTOM, "END of File");     // Reset - Section 5.1 - step 7
                break;//sLine = null; //end of file
            } else {
                T_IG(VTSS_TRACE_GRP_CUSTOM, "Waiting 100ms");
                VTSS_MSLEEP(100);
            }

        } else {
            // Unknown Sx number
            T_E("Unknown start value S:%d", byteArr[1]);
            break;
        }
    }

    // Wait 400ms - Section 5.1 - step 8
    VTSS_MSLEEP(400);

    // Reset - Section 5.1 - step 9
    u8 rst[] = { (u8)'R', (u8)'S', (u8)'T'};
    (void) pd69200_wr(iport, (uchar *) &rst[0], 3);

    VTSS_MSLEEP(10000);
    pd69200_sync(iport);
    firmware_upgrade_in_progress = FALSE;
    pd69200_chip_found_set(iport, DETECTION_NEEDED);
    pd69200_do_detection(iport);
    T_IG(VTSS_TRACE_GRP_CUSTOM, "Upgrade done");

    return VTSS_RC_OK;
}

static mesa_rc pd69200_download_firmware(mesa_port_no_t iport, bool valid, const char *microsemi_firmware, size_t firmware_size)
{
    mesa_rc rc = MESA_RC_ERROR;
    u8      buf[PD_BUFFER_SIZE];
    u32     byte_cnt = 0;
    u8      progress = 0;
    u8      prev_progress = 255;
    const   char *byteArr;
    u8      line[LINE_SIZE_MAX];
    u8      line_index;

    if (valid) { // Valid firmware exists
        // Section 5.1
        u8 download[] = { 0x01, 0x01, 0xFF, 0x99, 0x15, 0x16, 0x16, 0x99, 0x4E, 0x4E, 0x4E, 0x4E, 0x4E, 0x03, 0xFA };
        T_IG(VTSS_TRACE_GRP_CUSTOM, "Valid firmware - start download");
        if (!pd69200_i2c_tx(iport, download, __LINE__)) {
            T_IG(VTSS_TRACE_GRP_CUSTOM, "download problem");
            return VTSS_RC_ERROR;
        }
    } else { // Invalid or non-existing firmware
        // Section 5.2
        T_IG(VTSS_TRACE_GRP_CUSTOM, "Invalid firmware - start download");
        sendEnter(iport);
    }

    firmware_upgrade_in_progress = TRUE; // Signal to PoE code to stop access to the PoE chipset.

    VTSS_MSLEEP(100); // read TPE\r\n - Section 5.1 - step 2 - response may take 100 ms
    pd69200_rd(iport, buf, 5); // read TPE\r\n - Section 5.1 - step 2
    if ((buf[0] != 'T') || (buf[1] != 'P') || (buf[2] != 'E')) {
        T_IG(VTSS_TRACE_GRP_CUSTOM, "No TPE!");
        goto out;
    }

    T_DG(VTSS_TRACE_GRP_CUSTOM, " Sending 'E' - erasing memory - Section 5.1 - step 3");
    buf[0] = 'E';
    (void) pd69200_wr(iport, buf, 1);
    VTSS_MSLEEP(100);
    pd69200_rd(iport, buf, 5);// read TOE\r\n - Section 5.1 - step 3;
    if ((buf[0] != 'T') || (buf[1] != 'O') || (buf[2] != 'E')) {
        T_IG(VTSS_TRACE_GRP_CUSTOM, "No TOE!");
        goto out;
    }

    T_DG(VTSS_TRACE_GRP_CUSTOM, "Erasure may last up to 5 seconds. - Section 5.1 - step 4");
    VTSS_MSLEEP(5000);

    pd69200_rd(iport, buf, 4); // read TE\r\n - Section 5.1 - step 4
    if ((buf[0] != 'T') || (buf[1] != 'E')) {
        T_DG(VTSS_TRACE_GRP_CUSTOM, "No TE!");
        goto out;
    }
    VTSS_MSLEEP(100);

    pd69200_rd(iport, buf, 5); // read TPE\r\n - Section 5.1 - step 4
    if ((buf[0] != 'T') || (buf[1] != 'P') || (buf[2] != 'E')) {
        T_IG(VTSS_TRACE_GRP_CUSTOM, "No TPE!");
        goto out;
    }

    T_DG(VTSS_TRACE_GRP_CUSTOM, " Sending 'P' - program memory - Section 5.1 - step 5");
    buf[0] = 'P';
    (void) pd69200_wr(iport, buf, 1);
    VTSS_MSLEEP(100);
    pd69200_rd(iport, buf, 5); // read TOP\r\n - Section 5.1 - step 5
    if ((buf[0] != 'T') || (buf[1] != 'O') || (buf[2] != 'P')) {
        T_IG(VTSS_TRACE_GRP_CUSTOM, "No TOP");
        goto out;
    }

    // If greater than 127 then we have reached end of file
    while ((byte_cnt <= firmware_size) && (*microsemi_firmware <= 127)) {
        byteArr = microsemi_firmware;

        // Print out progress
        progress = (byte_cnt * 100) / firmware_size;
        if (progress != prev_progress) {
            T_IG(VTSS_TRACE_GRP_CUSTOM, "PoE Firmware update progress:%d %%", progress);
            prev_progress = progress;
        }

        if (byteArr[0] == 10) {
            T_DG(VTSS_TRACE_GRP_CUSTOM, "Skipping LF (Line Feed)");
            microsemi_firmware++;
            byteArr = microsemi_firmware;
        }

        if (byteArr[0] != 'S') {
            T_EG(VTSS_TRACE_GRP_CUSTOM, "Line should start with S byteArr[0]:0x%X", byteArr[0]);
            break;
        }

        if (byteArr[1] == '0') { //skip line that starts with "S0"
            T_NG(VTSS_TRACE_GRP_CUSTOM, "Skipping lines starting with S0");

            // If greater than 127 then we have reached end of file
            while (*microsemi_firmware != '\n' && *microsemi_firmware < 127 && byte_cnt <= firmware_size) {
                byte_cnt++;
                T_NG(VTSS_TRACE_GRP_CUSTOM, "Skipping S0 lines microsemi_firmware:0x%X, byte_cnt:%u", *microsemi_firmware, byte_cnt);
                microsemi_firmware++;
            }

            // Lines Starting with S3 or S7
        } else if (byteArr[1] == '3' || byteArr[1] == '7') {
            T_NG(VTSS_TRACE_GRP_CUSTOM, "Lines Starting with S3 or S7 - Section 5.1 - step 6");

            line_index = 0;
            // If greater than 127 then we have reached end of file
            while (*microsemi_firmware != '\n' && *microsemi_firmware < 127 && byte_cnt <= firmware_size) {
                line[line_index++] = *microsemi_firmware;
                microsemi_firmware++;
                byte_cnt++;
            }
            microsemi_firmware++; // For the "\n"

            T_NG(VTSS_TRACE_GRP_CUSTOM, "Writing new line - byte_cnt:%u", byte_cnt);
            line[line_index++] = '\n';

            (void) pd69200_wr(iport, line, line_index); // Write the line - Section 5.1 - step 6
            T_NG_HEX(VTSS_TRACE_GRP_CUSTOM, line, line_index);

            pd69200_rd(iport, buf, 4); // reading T*\r\n or TP\r\n - Section 5.1 - step 6

            if (buf[0] == 'T' && buf[1] == '*') { // if read T*\r\n
                T_NG(VTSS_TRACE_GRP_CUSTOM, "NOT END of File");
            } else if (buf[0] == 'T' && buf[1] == 'P') { // if read TP\r\n
                T_NG(VTSS_TRACE_GRP_CUSTOM, "END of File");     // Reset - Section 5.1 - step 7
                break; //end of file
            } else {
                T_NG(VTSS_TRACE_GRP_CUSTOM, "Waiting 100ms");
                VTSS_MSLEEP(100);
            }

        } else {
            // Unknown Sx number
            T_EG(VTSS_TRACE_GRP_CUSTOM, "Unknown start value S:%d", byteArr[1]);
            break;
        }
    }

    // Wait 400ms - Section 5.1 - step 8
    VTSS_MSLEEP(400);

    // Reset - Section 5.1 - step 9
    buf[0] = 'R';
    buf[1] = 'S';
    buf[2] = 'T';
    (void) pd69200_wr(iport, buf, 3);

    VTSS_MSLEEP(10000);
    rc = MESA_RC_OK; // Download succeeded

out:
    firmware_upgrade_in_progress = FALSE;
    return rc;
}

static mesa_rc pd69200_get_and_download_firmware(mesa_port_no_t iport, bool valid)
{
    mesa_rc rc = VTSS_APPL_POE_ERROR_FIRMWARE;
    int     size = 0;
    char   *buffer = NULL;

    if ((VTSS_MALLOC_CAST(buffer, POE_FIRMWARE_SIZE_MAX)) == NULL) {
        T_EG(VTSS_TRACE_GRP_CUSTOM, "Memory allocation issue.");
        return MESA_RC_ERROR;
    }

    std::ifstream file("/etc/mscc/poe/firmware/mscc_firmware.txt");
    if (file.is_open()) {
        while (!file.eof() && (size < POE_FIRMWARE_SIZE_MAX)) {
            file.get(buffer[size++]); //reading one character from file to array
        }
        file.close();
    } else {
        T_EG(VTSS_TRACE_GRP_CUSTOM, "Cannot open firmware file.");
        goto out;
    }

    rc = pd69200_download_firmware(iport, valid, buffer, size);
    T_IG(VTSS_TRACE_GRP_CUSTOM, "size:%d, rc:%d", size, rc);
    T_IG_HEX(VTSS_TRACE_GRP_CUSTOM, (u8 *)(buffer + size - 10), 20);

out:
    VTSS_FREE(buffer);
    return rc;
}

static mesa_rc pd69200_clear_device_buffer(mesa_port_no_t iport)
{
    u8      buf[PD_BUFFER_SIZE];
    mesa_rc rc = MESA_RC_OK;

    if (get_controller_response(iport, &buf[0], 0xFF)) {
        T_IG(VTSS_TRACE_GRP_CUSTOM, "Controller response:");
        T_IG_HEX(VTSS_TRACE_GRP_CUSTOM, buf, PD_BUFFER_SIZE);
        if ((buf[0] == TELEMETRY_KEY) && (buf[1] == 0xff)) { // This is the self transmitted System Status Telemetry
            if (buf[2] & 2) { // Firmware download required
                T_IG(VTSS_TRACE_GRP_CUSTOM, "Invalid PoE firmware detected!");
                rc = pd69200_get_and_download_firmware(iport, FALSE);
            } else if ((buf[2] & 1) || (buf[3] & 1)) {
                T_EG(VTSS_TRACE_GRP_CUSTOM, "PoE controller or memory error - non recoverable!");
                rc = MESA_RC_ERROR;
            } else {
                T_IG(VTSS_TRACE_GRP_CUSTOM, "Valid PoE firmware detected!");
#if 0
                T_IG(VTSS_TRACE_GRP_CUSTOM, "Forcing PoE firmware download - for debug only!");
                rc = pd69200_get_and_download_firmware(iport, TRUE);
#endif
            }
        }
    }
    return rc;
}
/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
