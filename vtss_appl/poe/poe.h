/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _VTSS_POE_H_
#define _VTSS_POE_H_

#include "poe_api.h"
/**********************************************************************
 * PoE Messages
 **********************************************************************/
typedef enum {
    POE_MSG_ID_GET_STAT_REQ,          /* Master's request to get status from a slave */
    POE_MSG_ID_GET_STAT_REP,          /* Slave's reply upon a get status */
    POE_MSG_ID_GET_PD_CLASSES_REP,    /* Slave's reply upon a get PD classes  */
    POE_MSG_ID_GET_PD_CLASSES_REQ,    /* Master's request to get PD classes from a slave */
} poe_msg_id_t;

#define POE_UNDETERMINED_CLASS -1

// Request message
typedef struct {
    // Message ID
    poe_msg_id_t msg_id;

    union {
        /* POE_MSG_ID_CONF_SET_REQ */

        /* POE_MSG_ID_GET_STAT_REQ: No data */

        /* POE_MSG_ID_GET_PD_CLASSES_REQ: No data */
    } req;
} poe_msg_req_t;

// Reply message
typedef struct {
    // Message ID
    poe_msg_id_t msg_id;

    union {
        /* POE_MSG_ID_CONF_STAT_REP */
        // poe_status_t status; // deleted as it was not used

        /* POE_MSG_ID_GET_PD_CLASSES_REP */
        char         classes[VTSS_MAX_PORTS_LEGACY_CONSTANT_USE_CAPARRAY_INSTEAD];
    } rep;
} poe_msg_rep_t;

typedef struct {
    int dev;
    int base;
} poe_device_t;

// For containing software and hardware version
typedef struct {
    u16 sw_ver;
    u8  hw_ver;
    u8  param;
    u16 internal_sw_ver;
} poe_firmware_version_t;
#define MSCC_POE_FORCE_FIRMWARE_UPDATE 1

// bitset for PoE status led
#define POE_LED_POE_OFF     (1 << 0)
#define POE_LED_POE_ON      (1 << 1)
#define POE_LED_POE_DENIED  (1 << 2)
#define POE_LED_POE_ERROR   (1 << 3)

#endif //_VTSS_POE_H_
/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
