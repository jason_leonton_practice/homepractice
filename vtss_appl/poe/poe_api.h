/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _VTSS_APPL_POE_API_H_
#define _VTSS_APPL_POE_API_H_
#include "port_api.h"
#include "vtss/appl/poe.h"
#include "poe_types.h"

//Default priority configuration
#define POE_PRIORITY_DEFAULT   VTSS_APPL_POE_PORT_POWER_PRIORITY_LOW
//Define power management configuration
#if 0  // 20180329 Harvey modify, Leonton spec.
#define POE_MGMT_MODE_DEFAULT  VTSS_APPL_POE_LLDPMED_CONSUMP
#else
#define POE_MGMT_MODE_DEFAULT  VTSS_APPL_POE_CLASS_CONSUMP
#endif
// Default mode configuration
#define POE_MODE_DEFAULT       VTSS_APPL_POE_MODE_POE_PLUS
// Default maximum power configuration for a port in allocation mode.
// It is a problem if the total power of all ports exceeds the
// amount of power the power supply can deliver (See bugzilla#22307), because
// the individual power limits from start-config is not applied once the power
// supply limit is exceeded.
// The solution is to set the default value to 0. In that way we never exceeds
// the power supply limit when booting.
#if 0  // 20180326 Harvey modify, Leonton spec.
#define POE_MAX_POWER_DEFAULT  0
#else
#define POE_MAX_POWER_DEFAULT  300  // DeciWatts
#endif


//Default ping alive configuration
#define PING_ALIVE_ADDRESS_DEFAULT   0
#define PING_ALIVE_INTERVAL_DEFAULT  60

//Default poe schedule configuration
#define WEEKDAY_MAX_NUM              7
#define POE_SCHEDULE_MAX_NUM         32
#define POE_SCHEDULE_STATE_DEFAULT   0
#define POE_SCHEDULE_ID_DEFAULT      0

typedef enum {
    ACTUAL,
    REQUESTED
} poe_mgmt_mode_t;

typedef enum {
    NOT_FOUND,
    FOUND,
    DETECTION_NEEDED
} poe_chip_found_t;

typedef enum {
    PING_ALIVE_STATE,
    PING_ALIVE_ADDRESS,
    PING_ALIVE_INTERVAL,
    POE_SCHED_ID,
    POE_SCHED_MODE
} lntn_poe_type_t;

typedef enum {
    SCHEDULE_DISABLE,
    SCHEDULE_ON,
    SCHEDULE_OFF
} sched_mode_t;

typedef struct {
	CapArray<BOOL, MESA_CAP_PORT_CNT> changed;
	CapArray<BOOL, MESA_CAP_PORT_CNT> state;
	CapArray<u16, MESA_CAP_PORT_CNT> interval;
	CapArray<mesa_ipv4_t, MESA_CAP_PORT_CNT> address;
} ping_alive_local_t;

typedef struct {
	CapArray<u8, MESA_CAP_PORT_CNT> sched_id;
	CapArray<sched_mode_t, MESA_CAP_PORT_CNT> mode;
} sched_port_local_t;

typedef struct {
    BOOL status; //0: Inactive, 1: Active
    // days since Sunday - [0..6]
    int  start_hour[WEEKDAY_MAX_NUM];
    int  start_min[WEEKDAY_MAX_NUM];
    int  end_hour[WEEKDAY_MAX_NUM];
    int  end_min[WEEKDAY_MAX_NUM];
} sched_time_t;

typedef struct {
    sched_time_t   sched_time[POE_SCHEDULE_MAX_NUM];
} sched_time_local_t;

/**********************************************************************
 ** Configuration structs
 **********************************************************************/


/**********************************************************************
** Wrapper-methods for hardware-access (For LLDP-Poe control).
**********************************************************************/
poe_entry_t poe_hw_config_get(mesa_port_no_t port_idx, poe_entry_t *hw_conf);
void poe_pse_data_get(mesa_port_no_t port_index, poe_pse_data_t *pse_data);
void poe_pd_data_set(mesa_port_no_t port_index, poe_pd_data_t *pd_data);
BOOL poe_new_pd_detected_get(mesa_port_no_t port_index, BOOL clear);

u16  poe_port_max_power_get(mesa_port_no_t port_index);// Returns Maximum power supported for a port (in DeciWatts)
char *poe_firmware_info_get(mesa_port_no_t port_index, char *info);

/**********************************************************************
 ** PoE functions
 **********************************************************************/
const char *poe_error_txt(mesa_rc rc); // Return printable string for the given return code

// Function returning the maximum power the a port can be configured to.
u16 poe_max_power_mode_dependent(mesa_port_no_t iport, vtss_appl_poe_port_mode_t poe_mode);

/* Initialize module */
mesa_rc poe_init(vtss_init_data_t *data);

// Function that returns the current configuration.
void poe_config_get(poe_conf_t *conf);

// Function that can set the current configuration.
mesa_rc poe_config_set(poe_conf_t *new_conf);

// Function for getting current status for all ports
void poe_mgmt_get_status(poe_status_t *status);

// Function Converting a integer to a string with one digit. E.g. 102 becomes 10.2.
char *one_digi_float2str(int val, char *string_ptr);

const char *poe_status2str(vtss_appl_poe_status_type_t status, mesa_port_no_t iport, poe_conf_t *conf);

char *poe_class2str(const poe_status_t *status, mesa_port_no_t iport, char *class_str);

BOOL poe_mgmt_is_backup_power_supported(void);
poe_power_source_t poe_mgmt_get_power_source(void); // Function that returns which power source the host is using right now,

// Function for getting a list with which PoE chipset there is associated to a given port.
// In/out : poe_chip_found - pointer to array to return the list.
void poe_mgmt_is_chip_found(poe_chipset_t *poe_chip_found);

// Debug function for doing PoE register reads
void poe_mgmt_reg_rd(mesa_port_no_t iport, u16 addr, u16 *data);

// Debug function for doing PoE register writes
void poe_mgmt_reg_wr(mesa_port_no_t iport, u16 addr, u16 data);


// Debug function for when debugging i2c issues
void poe_halt_at_i2c_err_ena(); // Enabling halting at i2c errors.
void poe_halt_at_i2c_err(BOOL i2c_err); // Do the halting if an i2c error happens - Input - TRUE is an i2c error has been detected.

// Debug function
void poe_mgmt_capacitor_detection_set(mesa_port_no_t iport, BOOL enable);
BOOL poe_mgmt_capacitor_detection_get(mesa_port_no_t iport);
mesa_rc poe_mgmt_firmware_update(mesa_port_no_t iport, const char *firmware, size_t firmware_size);
char *poe_mgmt_firmware_info_get(mesa_port_no_t iport, char *firmware_string);
void poe_debug_access(mesa_port_no_t iport, uchar key, uchar subject, uchar subject1, uchar subject2,
                      uchar data0, uchar data1, uchar data2, uchar data3, uchar data4, uchar data5, uchar data6, uchar data7);
void poe_restore_factory_default(mesa_port_no_t iport);

// Function for getting which PoE chipset is found from outside the poe.c file (In order to do semaphore protection)
poe_chipset_t poe_is_chip_found(mesa_port_no_t iport);

// Returns TRUE if the PoE chips are detected and initialized, else FALSE.
BOOL is_poe_ready(void);

BOOL poe_is_any_chip_found(void);

#define POE_FIRMWARE_SIZE_MAX 2000000 // Maximum size of the PoE firmware in bytes (file mscc_firmware.txt)
mesa_rc poe_do_firmware_upgrade(mesa_port_no_t iport, const char *url, int &tftp_err, BOOL has_built_in);

// Lntn PoE Ping Alive for getting/setting local value
void ping_alive_get_local_config(ping_alive_local_t *conf);
void ping_alive_set_local_config(ping_alive_local_t *new_conf);
void sched_port_get_local_config(sched_port_local_t *conf);
void sched_port_set_local_config(sched_port_local_t *new_conf);
void sched_time_get_local_config(sched_time_local_t *conf);
void sched_time_set_local_config(sched_time_local_t *new_conf);

#endif  /* _VTSS_APPL_POE_API_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
