/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
****************************************************************************/

#include "poe_custom_api.h"
#include "poe.h"

#ifdef __cplusplus
extern "C" {
#endif
void pd69200_reset_command (void);
void pd69200_set_enable_disable_channel(mesa_port_no_t iport, uchar enable, vtss_appl_poe_port_mode_t mode);
void pd69200_set_pm_method(mesa_port_no_t iport, vtss_appl_poe_management_mode_t power_mgmt_mode);
void pd69200_set_power_limit_channel(mesa_port_no_t iport, uint ppl );
void pd69200_set_port_priority(mesa_port_no_t iport, vtss_appl_poe_port_power_priority_t priority);
void pd69200_get_port_measurement(mesa_port_no_t iport, poe_status_t *poe_status);
void pd69200_set_power_banks(mesa_port_no_t iport, int primary_power_limit, int backup_power_limit);
int pd69200_get_port_power_limit(int iport);
void pd69200_poe_init(void);
int pd69200_wr(mesa_port_no_t iport, uchar *data, char size);
mesa_rc pd69200_rd(mesa_port_no_t iport, uchar *data, char size);
void pd69200_port_status_get (mesa_port_no_t iport, vtss_appl_poe_status_type_t *port_status, char *class_v);

poe_power_source_t pd69200_get_power_source (void);
void pd69200_pse_data_get(mesa_port_no_t iport, poe_pse_data_t *pse_data);
void pd69200_pd_data_set(mesa_port_no_t iport, poe_pd_data_t *pd_data);

// Function that returns 1 is a PoE chip set is found, else 0.
int pd69200_is_chip_available(mesa_port_no_t iport);

void pd69200_do_detection(mesa_port_no_t iport);

// Function for getting the legacy capacitor detection mode
BOOL pd69200_capacitor_detection_get(mesa_port_no_t iport);

// Function for setting the legacy capacitor detection mode
//      enable         : True - Enable legacy capacitor detection
void pd69200_capacitor_detection_set(mesa_port_no_t iport, BOOL enable);

// Get software version - Section 4.7.1 in user guide
poe_firmware_version_t pd69200_get_sw_version(mesa_port_no_t iport);

// Function for updating PoE chipset firmware
mesa_rc pd69200_DownloadFirmwareFunc(mesa_port_no_t iport, const char *microsemi_firmware, size_t firmware_size);

void pd69200_individual_mask_set(mesa_port_no_t iport, u8 mask, u8 value);

BOOL pd69200_new_pd_detected_get(mesa_port_no_t iport, BOOL clear);

void pd69200_debug_access(mesa_port_no_t iport, uchar key, uchar subject, uchar subject1, uchar subject2,
                          uchar data0, uchar data1, uchar data2, uchar data3, uchar data4, uchar data5, uchar data6, uchar data7);
void pd69200_restore_factory_default(mesa_port_no_t iport, BOOL trace);
#ifdef __cplusplus
}
#endif
/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
