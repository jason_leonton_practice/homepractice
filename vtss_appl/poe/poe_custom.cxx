/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
**********************************************************************
*/

//**********************************************************************
//  This file contains functions that are general for the PoE system.
//  Depending upon which PoE chip set the hardware uses each function
//  calls the corresponding function for that chip set.
//**********************************************************************
#include "critd_api.h"
#include "misc_api.h"    // I2C
#include "poe.h"
#include "poe_custom_api.h"
#include "poe_custom_pd69200_api.h"
#include "poe_custom_si3452_api.h"
#include "poe_custom_slus787_api.h"
#include "poe_custom_ltc4271_api.h"
#include "poe_board.h"
#include "vtss/appl/poe.h"
#include "poe_trace.h"

#ifdef VTSS_SW_OPTION_SYSLOG
#include "syslog_api.h"
#endif

static BOOL init_done = FALSE;  // OK - That is not semaphore protect. Only set one place.
static BOOL firmware_upgrade_in_progress = FALSE; // Set when doing firmware upgrade

/****************************************************************************/
/*  Semaphore and trace                                                     */
/****************************************************************************/
static critd_t crit_custom;  // Critial region for the shared custom poe chipset.
#define POE_CUSTOM_CRIT_SCOPE() VtssPoECustomCritdGuard __lock_guard__(__LINE__)

#if (VTSS_TRACE_ENABLED)
struct VtssPoECustomCritdGuard {
    VtssPoECustomCritdGuard(int line) : line_(line)
    {
        critd_enter(&crit_custom, VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE,  __FUNCTION__, line);
    }

    ~VtssPoECustomCritdGuard()
    {
        critd_exit(&crit_custom, VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE,  __FUNCTION__, line_);
    }
    int line_;
};
#define POE_CUSTOM_CRIT_ENTER() T_RG(VTSS_TRACE_GRP_CRIT, "Enter"); critd_enter(&crit_custom, VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define POE_CUSTOM_CRIT_EXIT() T_RG(VTSS_TRACE_GRP_CRIT, "Exit"); critd_exit(&crit_custom, VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#else
struct VtssPoECustomCritdGuard {
    VtssPoECustomCritdGuard()
    {
        critd_enter(&crit_custom);
    }

    ~VtssPoECustomCritdGuard()
    {
        critd_exit(&crit_custom, 0);
    }
};
#define POE_CUSTOM_CRIT_ENTER() critd_enter(&crit_custom)
#define POE_CUSTOM_CRIT_EXIT() critd_exit(&crit_custom)
#endif

//**********************************************************************
//* Generic functions that is called by the host module
//**********************************************************************
static poe_chipset_t poe_custom_is_chip_found_(mesa_port_no_t iport, u32 line)
{
    T_RG(VTSS_TRACE_GRP_CUSTOM, "line:%d", line);
    // Get PoE hardware board configuration
    poe_custom_entry_t poe_hw_conf;

    if (!MESA_CAP(MEBA_CAP_POE)) {
        T_RG(VTSS_TRACE_GRP_CUSTOM, "No PoE Board feature");
        return NO_POE_CHIPSET_FOUND;
    }

    poe_hw_conf = poe_custom_get_hw_config(iport, &poe_hw_conf);

    if (!poe_hw_conf.available) {
        T_RG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "%s", "PoE not supported");
        return NO_POE_CHIPSET_FOUND;
    }

#ifdef VTSS_HW_CUSTOM_POE_CHIP_SI3452_SUPPORT
    if (si3452_is_chip_available(iport)) {
        return SI3452;
    }
#endif


#ifdef VTSS_HW_CUSTOM_POE_CHIP_SLUS787_SUPPORT
    if (slus787_is_chip_available(iport)) {
        return SLUS787;
    }
#endif

#ifdef VTSS_HW_CUSTOM_POE_CHIP_PD69200_SUPPORT
    if (pd69200_is_chip_available(iport)) {
        T_RG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "%s", "pd69200_is_chip_available");
        return PD69200;
    } else {
        T_RG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "%s", "NOT pd69200_is_chip_available");
    }
#endif

#ifdef VTSS_HW_CUSTOM_POE_CHIP_LTC4271_SUPPORT
    if (ltc4271_is_chip_available(iport)) {
        T_RG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "%s", "ltc4271_is_chip_available");
        return LTC4271;
    } else {
        T_RG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "%s", "NOT ltc4271_is_chip_available");
    }
#endif
    return NO_POE_CHIPSET_FOUND;
}

// Function that returns which PoE chip set is found (PoE chip sets are auto detected).
// If not PoE chip is found, NO_POE_CHIPSET_FOUND is returned.
poe_chipset_t poe_custom_is_chip_found(mesa_port_no_t iport)
{
    T_RG(VTSS_TRACE_GRP_CUSTOM, "init_done:%d", init_done);
    if (!init_done) {
        return NO_POE_CHIPSET_FOUND;
    }
    return poe_custom_is_chip_found_(iport, __LINE__);
}

// Function that tells wether the PoE chip does the power management.
BOOL poe_custom_chip_does_power_management()
{
    return (poe_custom_is_chip_found(0) == PD69200);
}

// Disable PoE
static void poe_disable(void)
{
    T_IG(VTSS_TRACE_GRP_CUSTOM, "board enable");
    // Software disable all ports before we hardware enable PoE
    for (auto iport = 0 ; iport < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); iport++) {
        if (poe_custom_is_chip_found_(iport, __LINE__) == PD69200) {
            pd69200_set_enable_disable_channel(iport, FALSE, VTSS_APPL_POE_MODE_DISABLED);
        } else if (poe_custom_is_chip_found_(iport, __LINE__) == SI3452) {
            si3452_poe_enable(iport, FALSE);
        } else if (poe_custom_is_chip_found_(iport, __LINE__) == SLUS787) {
            slus787_poe_enable(iport, FALSE);
        } else if (poe_custom_is_chip_found_(iport, __LINE__) == LTC4271) {
            ltc4271_poe_enable(iport, FALSE);
        }
    }
    poe_board_hw_enable();
}

void poe_custom_init_chips(void)
{
    mesa_port_no_t iport;
    POE_CUSTOM_CRIT_SCOPE();

    // BOOL found = FALSE;
    if (firmware_upgrade_in_progress) {
        return;
    }
#ifdef VTSS_HW_CUSTOM_POE_CHIP_SI3452_SUPPORT
    for (iport = 0 ; iport < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); iport++) {
        T_IG(VTSS_TRACE_GRP_CUSTOM, "%s", "detecting, SI");
        if (si3452_is_chip_available(iport)) {
            found = TRUE; // At least one found
        }
    }
#endif

#ifdef VTSS_HW_CUSTOM_POE_CHIP_SLUS787_SUPPORT
    if (!found) {
        for (iport = 0 ; iport < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); iport++) {
            T_IG(VTSS_TRACE_GRP_CUSTOM, "%s", "detecting TI");
            if (slus787_is_chip_available(iport)) {
                found = TRUE; // At least one found
            }
        }
    }
#endif

#ifdef VTSS_HW_CUSTOM_POE_CHIP_PD69200_SUPPORT
    if (!found) {
        T_IG(VTSS_TRACE_GRP_CUSTOM, "%s", "detecting PD69200");
        for (iport = 0 ; iport < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); iport++) {
            pd69200_do_detection(iport);
        }
    }
#endif

    for (iport = 0 ; iport < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); iport++) {
        if (poe_custom_is_chip_found_(iport, __LINE__) == PD69200) {
            pd69200_poe_init(); // Initialize before we hardware enable PoE
            poe_disable(); // Hardware enable PoE ports
            pd69200_individual_mask_set(iport, 0x1E, 0); // Disable i2c ready interrupt
            break; // At the moment this is all handled in the PD69200 driver.
        } else if (poe_custom_is_chip_found_(iport, __LINE__) == SI3452) {
            si3452_poe_init(iport);
        } else if (poe_custom_is_chip_found_(iport, __LINE__) == SLUS787) {
            slus787_poe_init(iport);
        } else if (poe_custom_is_chip_found_(iport, __LINE__) == LTC4271) {
            ltc4271_poe_init(iport);
        }
        vtss_thread_yield(); // Make sure that other threads get access to the i2c controller.
    }
}

// Function that shall be called after reset / board boot.
void poe_custom_init(void)
{
    T_IG(VTSS_TRACE_GRP_CUSTOM, "Initializing");
    if (init_done) {
        T_IG(VTSS_TRACE_GRP_CUSTOM, "PoE init already done");
        return;
    }

    T_IG(VTSS_TRACE_GRP_CUSTOM, "Doing PoE init");
    critd_init(&crit_custom, "PoE crit custom", VTSS_MODULE_ID_POE, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
    // It take loooong time to download and program firmware, so allow indefinite lock of this mutex.
    crit_custom.max_lock_time = -1;

    POE_CUSTOM_CRIT_EXIT();
    if (MESA_CAP(MEBA_CAP_POE)) {
        poe_custom_init_chips();
    }
    init_done = TRUE;
    T_IG(VTSS_TRACE_GRP_CUSTOM, "PoE init done");
}

// Debug function for updating the PoE chipset firmware - For MSCC PoE chips only
static mesa_rc poe_download_firmware(mesa_port_no_t iport, const char *firmware, size_t firmware_size)
{
    T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "%s", "Enter");
#ifdef VTSS_SW_OPTION_SYSLOG
    S_I("PoE: Firmware upgrade started");
#endif /* VTSS_SW_OPTION_SYSLOG */
    mesa_rc rc = pd69200_DownloadFirmwareFunc(iport, firmware, firmware_size);

    if (rc == VTSS_RC_OK) {
        poe_disable(); // For some reason the hardware pin needs to be re-enabled after firmware upgrade.
    }

#ifdef VTSS_SW_OPTION_SYSLOG
    switch (rc) {
    case VTSS_RC_OK:
        S_I("PoE: Firmware upgrade done");
        break;
    case VTSS_APPL_POE_ERROR_FIRMWARE_VER_NOT_NEW:
        S_I("PoE: No Firmware upgrade needed");
        break;
    default:
        S_I("PoE: Firmware failed");
        break;
    }
#endif /* VTSS_SW_OPTION_SYSLOG */
    T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "%s", "Exit");
    return rc;
}

// Debug function for updating the PoE chipset firmware
mesa_rc poe_mgmt_firmware_update(mesa_port_no_t iport, const char *firmware, size_t firmware_size)
{
    BOOL fw_update_pre;

    T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "%s", "Enter");
    POE_CUSTOM_CRIT_ENTER();
    fw_update_pre = firmware_upgrade_in_progress;
    firmware_upgrade_in_progress = TRUE;
    POE_CUSTOM_CRIT_EXIT();
    if (fw_update_pre == TRUE) {
        T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "%s", "Exit - update already in progress.");
        return VTSS_RC_OK;
    }

    mesa_rc rc = poe_download_firmware(iport, firmware, firmware_size);

    POE_CUSTOM_CRIT_ENTER();
    firmware_upgrade_in_progress = FALSE;
    POE_CUSTOM_CRIT_EXIT();

    T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "%s", "Exit");
    return rc;
}

// Enable or disable a port
void poe_custom_port_enable(mesa_port_no_t iport, BOOL enable, vtss_appl_poe_port_mode_t mode)
{
    T_NG_PORT(VTSS_TRACE_GRP_CUSTOM, iport, "Enable:%d", enable);
    POE_CUSTOM_CRIT_SCOPE();
    if (firmware_upgrade_in_progress) {
        return;
    }
    if (poe_custom_is_chip_found_(iport, __LINE__) == PD69200) {
        pd69200_set_enable_disable_channel(iport, enable, mode);
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == SI3452) {
        si3452_poe_enable(iport, enable);
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == SLUS787) {
        slus787_poe_enable(iport, enable);
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == LTC4271) {
        ltc4271_poe_enable(iport, enable);
    }
}

// Power management
void poe_custom_pm(mesa_port_no_t iport, vtss_appl_poe_management_mode_t power_mgmt_mode)
{
    POE_CUSTOM_CRIT_SCOPE();
    if ((firmware_upgrade_in_progress == FALSE) && (poe_custom_is_chip_found_(iport, __LINE__) == PD69200)) {
        pd69200_set_pm_method(iport, power_mgmt_mode);
    }
}

// Set max. power per port in deci watts
void poe_custom_set_port_max_power(mesa_port_no_t iport, int max_port_power, vtss_appl_poe_status_type_t port_status)
{
    POE_CUSTOM_CRIT_SCOPE();
    if (firmware_upgrade_in_progress) {
        return;
    }
    static BOOL first = TRUE;
    static CapArray<int, MESA_CAP_PORT_CNT> last_max_port_power;
    static CapArray<vtss_appl_poe_status_type_t, MESA_CAP_PORT_CNT> last_port_status;
    BOOL skip_check = FALSE;

    int i;
    // Initialize last_max_port_power first time we enter this function.
    if (first) {
        first = FALSE;
        for (i = 0 ; i < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); i++) {
            last_max_port_power[i] = 0;    // Set to a value the will force registers to be updated
            last_port_status[i] = VTSS_APPL_POE_UNKNOWN_STATE;
        }
    }

    if (last_port_status[(int) iport] != VTSS_APPL_POE_PD_ON && port_status == VTSS_APPL_POE_PD_ON) {
        skip_check = TRUE;
    }

    last_port_status[(int) iport] = port_status;

    // Since access to PoE registers can take quite a bit of time, we only updates the registers when
    // the max_port_power has changed.
    if (!skip_check && last_max_port_power[(int) iport] == max_port_power) {
        return ;
    }

    last_max_port_power[(int) iport] = max_port_power;

    // Make sure that we don't configure max power to more then the chip set supports.
    if (max_port_power > poe_custom_get_port_power_max(iport)) {
        max_port_power = poe_custom_get_port_power_max(iport);
    }

    if (poe_custom_is_chip_found_(iport, __LINE__) == PD69200) {
        pd69200_set_power_limit_channel(iport, max_port_power);
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == SI3452) {
        si3452_set_power_limit_channel(iport, max_port_power);
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == SLUS787) {
        slus787_set_power_limit_channel(iport, max_port_power);
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == LTC4271) {
        ltc4271_set_power_limit_channel(iport, max_port_power);
    }
}

// Configuration of the port priority
void poe_custom_set_port_priority(mesa_port_no_t iport, vtss_appl_poe_port_power_priority_t priority)
{
    POE_CUSTOM_CRIT_SCOPE();
    if ((firmware_upgrade_in_progress == FALSE) && (poe_custom_is_chip_found_(iport, __LINE__) == PD69200)) {
        pd69200_set_port_priority(iport, priority);
    }
}

// Returns Maximum power supported for a port (in deciWatts)
u16 poe_custom_get_port_power_max(mesa_port_no_t iport)
{
    return 300; // PoE+ maximum power.
}

// Get which power supply source the switch is running on.
poe_power_source_t poe_custom_get_power_source(void)
{
    return PRIMARY;
}

// Function that return TRUE if backup power supply is supported.
BOOL poe_custom_is_backup_power_supported (void)
{
    return FALSE;
}

// Maximum power for the power supply.
void poe_custom_set_power_supply(mesa_port_no_t iport, int primary_max_power, int backup_max_power)
{
    POE_CUSTOM_CRIT_SCOPE();
    if ((firmware_upgrade_in_progress == FALSE) && (poe_custom_is_chip_found_(iport, __LINE__) == PD69200)) {
        T_NG(VTSS_TRACE_GRP_CUSTOM, "Setting Power Banks for PD69200");
        pd69200_set_power_banks(iport, primary_max_power, backup_max_power);
    }
}

// Get status ( Real time current and power cunsumption )
void poe_custom_get_status(poe_status_t *poe_status)
{
    POE_CUSTOM_CRIT_SCOPE();
    if (firmware_upgrade_in_progress) {
        return;
    }
    mesa_port_no_t iport;
    for (iport = 0 ; iport < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); iport++) {
        if (poe_custom_is_chip_found_(iport, __LINE__) == PD69200) {
            pd69200_get_port_measurement(iport, poe_status); // Gets power consumption and current
        } else if (poe_custom_is_chip_found_(iport, __LINE__) == SI3452) {
            si3452_get_port_measurement(poe_status, iport);
        } else if (poe_custom_is_chip_found_(iport, __LINE__) == SLUS787) {
            slus787_get_port_measurement(poe_status, iport);
        } else if (poe_custom_is_chip_found_(iport, __LINE__) == LTC4271) {
            ltc4271_get_port_measurement(poe_status, iport);
        }
    }
}

// Updates the power reserved fields in the poe_status with the power given by the class.
void poe_custom_get_all_ports_classes(char *classes)
{
    POE_CUSTOM_CRIT_SCOPE();
    if (firmware_upgrade_in_progress) {
        return;
    }
    mesa_port_no_t iport;
    for (iport = 0 ; iport < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); iport++) {
        if (poe_custom_is_chip_found_(iport, __LINE__) == PD69200) {
            char class_v;
            pd69200_port_status_get(iport, NULL, &class_v);
            classes[iport] = class_v;
        } else if (poe_custom_is_chip_found_(iport, __LINE__) == SI3452) {
            si3452_get_all_port_class(classes, iport);
        } else if (poe_custom_is_chip_found_(iport, __LINE__) == SLUS787) {
            slus787_get_all_port_class(classes, iport);
        } else if (poe_custom_is_chip_found_(iport, __LINE__) == LTC4271) {
            ltc4271_get_all_port_class(classes, iport);
        }
    }
}

// Function for getting port status for a single port.
// In - iport - The internal port number for which to get status
// Out - port_status - Pointer to where to put the status
void poe_custom_port_status_get(mesa_port_no_t iport, vtss_appl_poe_status_type_t *port_status)
{
    POE_CUSTOM_CRIT_SCOPE();
    if (firmware_upgrade_in_progress) {
        return;
    }

    if (poe_custom_is_chip_found_(iport, __LINE__) == PD69200) {
        pd69200_port_status_get(iport, port_status, NULL);
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == SI3452) {
        si3452_port_status_get(port_status, iport);
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == SLUS787) {
        slus787_port_status_get(port_status, iport);
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == LTC4271) {
        ltc4271_port_status_get(port_status, iport);
    } else {
        // Well, no PoE chip sets found at all......
        *port_status = VTSS_APPL_POE_NOT_SUPPORTED;
    }
}

void poe_custom_pse_data_get(mesa_port_no_t iport, poe_pse_data_t *pse_data)
{
    POE_CUSTOM_CRIT_SCOPE();
    if ((firmware_upgrade_in_progress == FALSE) && (poe_custom_is_chip_found_(iport, __LINE__) == PD69200)) {
        T_IG(VTSS_TRACE_GRP_CUSTOM, "poe_custom_pd_data_get");
        pd69200_pse_data_get(iport, pse_data);
    }
}

void poe_custom_pd_data_set(mesa_port_no_t iport, poe_pd_data_t *pd_data)
{
    POE_CUSTOM_CRIT_SCOPE();
    if ((firmware_upgrade_in_progress == FALSE) && (poe_custom_is_chip_found_(iport, __LINE__) == PD69200)) {
        T_IG(VTSS_TRACE_GRP_CUSTOM, "poe_custom_pd_data_set");
        pd69200_pd_data_set(iport, pd_data);
    }
}

BOOL poe_custom_new_pd_detected_get(mesa_port_no_t iport, BOOL clear)
{
    POE_CUSTOM_CRIT_SCOPE();
    if ((firmware_upgrade_in_progress == FALSE) && (poe_custom_is_chip_found_(iport, __LINE__) == PD69200)) {
        return pd69200_new_pd_detected_get(iport, clear);
    }
    return FALSE;
}

mesa_rc poe_custom_powerin_status_get(vtss_appl_poe_powerin_status_t *const powerin_status)
{
    if (poe_custom_chip_does_power_management()) {
        poe_power_source_t power_status = pd69200_get_power_source();
        if (power_status == BACKUP) {
            powerin_status->pwr_in_status1 = FALSE;
            powerin_status->pwr_in_status2 = TRUE;
        } else {
            powerin_status->pwr_in_status1 = TRUE;
            powerin_status->pwr_in_status2 = FALSE;
        }
    } else {
        mesa_sgpio_port_data_t data[MESA_SGPIO_PORTS];
        VTSS_RC(mesa_sgpio_read(NULL, 0, 0, data));
        powerin_status->pwr_in_status1 = data[2].value[0];
        powerin_status->pwr_in_status2 = data[3].value[0];
    }
    return VTSS_RC_OK;
}

void poe_custom_vee_update(void)
{
    for (auto iport = 0 ; iport < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); iport++) {
        if (poe_custom_is_chip_found_(iport, __LINE__) == LTC4271) {
            ltc4271_update_vee(iport);
            return;
        }
    }
}


//
// Debug functions
//

// Debug function for converting PoE chipset to a printable text string.
// In : poe_chipset - The PoE chipset type.
//      buf        - Pointer to a buffer that can contain the printable Poe chipset string.
char *poe_chipset2txt(poe_chipset_t poe_chipset, char *buf)
{
    strcpy (buf, "-"); // Default to no chipset.

    if (poe_chipset == PD69200) {
        strcpy(buf, "MicroSemi PD69300");
    }

    if (poe_chipset == SI3452) {
        strcpy(buf, "SilliconLabs 3452");
    }

    if (poe_chipset == SLUS787) {
        strcpy(buf, "Texas Instruments slus787");
    }

    if (poe_chipset == LTC4271) {
        strcpy(buf, "Linear Tech LTC4271");
    }

    return buf;
}

// Debug function for writing PoE chipset registers
// In : iport - The port at which the PoE chipset is matched to.
//      addr       - The PoE chipset register address
//      Data       - The data to be written to the PoE chipset.
void poe_custom_wr(mesa_port_no_t iport, u16 addr, u16 data)
{
    POE_CUSTOM_CRIT_SCOPE();
    if (firmware_upgrade_in_progress) {
        return;
    }
    if (poe_custom_is_chip_found_(iport, __LINE__) == PD69200) {
//        (void) pd69200_wr(data, addr);
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == SLUS787) {
        slus787_device_wr(iport, addr, data);
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == SI3452) {
        si3452_device_wr(iport, addr, data);
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == LTC4271) {
        ltc4271_device_wr(iport, addr, data);
    }
}

// Debug function for reading PoE chipset registers
// In : iport - The port at which the PoE chipset is matched to.
//      addr       - The PoE chipset register address
// Out: Data       - Pointer to the read result
void poe_custom_rd(mesa_port_no_t iport, u16 addr, u16 *data)
{
    POE_CUSTOM_CRIT_SCOPE();
    if (firmware_upgrade_in_progress) {
        return;
    }
    u8 data_u8 = 0;
    if (poe_custom_is_chip_found_(iport, __LINE__) == PD69200) {
        // TBD - not supported pt.
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == SLUS787) {
        slus787_device_rd(iport, (char) addr, &data_u8, 1);
        *data = (u16) data_u8;
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == SI3452) {
        si3452_device_rd(iport, (char) addr, &data_u8);
        *data = (u16) data_u8;
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == LTC4271) {
        ltc4271_device_rd(iport, (char) addr, &data_u8);
        *data = (u16) data_u8;
    }
}

// Debug function for setting the legacy capacitor detection
// In : iport - The port at which the PoE chipset is matched to.
//      enable     - True to enable legacy capacitor detection. False to disable.
void poe_custom_capacitor_detection_set(mesa_port_no_t iport, BOOL enable)
{
    POE_CUSTOM_CRIT_SCOPE();
    static int error = 1;

    if ((firmware_upgrade_in_progress == FALSE) && (poe_custom_is_chip_found_(iport, __LINE__) == PD69200)) {
        pd69200_capacitor_detection_set(iport, enable);
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == LTC4271) {
        ltc4271_capacitor_detection_set(enable);
    } else if (error && error--) {
        T_D("Capacitor detect setting not support for this chipset");
    }
}

// Debug function for getting current state of the legacy capacitor detection
// In : iport - The port at which the PoE chipset is matched to.
//      enable     - True to enable legacy capacitor detection. False to disable.
BOOL poe_custom_capacitor_detection_get(mesa_port_no_t iport)
{
    POE_CUSTOM_CRIT_SCOPE();
    static int error = 1;

    if ((firmware_upgrade_in_progress == FALSE) && (poe_custom_is_chip_found_(iport, __LINE__) == PD69200)) {
        return pd69200_capacitor_detection_get(iport);
    } else if (poe_custom_is_chip_found_(iport, __LINE__) == LTC4271) {
        return ltc4271_capacitor_detection_get();
    } else if (error && error--) {
        T_D("Capacitor detect setting not support for this chipset");
        return FALSE;
    }
    return FALSE;
}

// For doing PD69200 access.
void poe_debug_access(mesa_port_no_t iport, uchar key, uchar subject, uchar subject1, uchar subject2,
                      uchar data0, uchar data1, uchar data2, uchar data3, uchar data4, uchar data5, uchar data6, uchar data7)
{
    POE_CUSTOM_CRIT_SCOPE();
    if (firmware_upgrade_in_progress) {
        return;
    }
    T_NG(VTSS_TRACE_GRP_CUSTOM, "debug access key:0x%X", key);
    pd69200_debug_access(iport, key, subject, subject1, subject2, data0, data1, data2, data3, data4, data5, data6, data7);
}

void poe_restore_factory_default(mesa_port_no_t iport)
{
    POE_CUSTOM_CRIT_SCOPE();
    pd69200_restore_factory_default(iport, FALSE);
}

// Debug function for getting firmware information
// In : iport - The port at which the PoE chipset is matched to.
// Out : info_txt - Pointer to printable string with firmware information
char *poe_custom_firmware_info_get(mesa_port_no_t iport, char *info)
{
    POE_CUSTOM_CRIT_SCOPE();
    if ((firmware_upgrade_in_progress == FALSE) && (poe_custom_is_chip_found_(iport, __LINE__) == PD69200)) {
        poe_firmware_version_t ver = pd69200_get_sw_version(iport);
        sprintf(info, "SW:%d, HW:%d, Internal:%d, param:%d", ver.sw_ver, ver.hw_ver, ver.internal_sw_ver, ver.param);
    } else {
        strcpy(info, "-");
    }
    return info;
}

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
