/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

#include "web_api.h"
#include "poe_api.h"
/* =================
 * Trace definitions
 * -------------- */
#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>

#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_WEB

#define VTSS_TRACE_GRP_DEFAULT 0
#define VTSS_TRACE_GRP_POE     1
#define TRACE_GRP_CNT          2

#ifdef VTSS_SW_OPTION_PRIV_LVL
#include "vtss_privilege_web_api.h"
#endif

#include <vtss_trace_api.h>
#include "ip_legacy.h"

/****************************************************************************/
/* Provide power supply max and min values for the web help pages.          */
/****************************************************************************/
#define WEB_BUF_LEN 512

static size_t poe_lib_config_js(char **base_ptr, char **cur_ptr, size_t *length)
{
    char buf[WEB_BUF_LEN];
    (void) snprintf(buf, WEB_BUF_LEN,
                    "var configPoESupplyMin = %d;\n"
                    "var configPoESupplyMax = %d;\n",
                    vtss_appl_poe_supply_min_get(),
                    vtss_appl_poe_supply_max_get());
    return webCommonBufferHandler(base_ptr, cur_ptr, length, buf);
}
web_lib_config_js_tab_entry(poe_lib_config_js);

/****************************************************************************/
/*  Web Handler  functions                                                  */
/****************************************************************************/
//
// Power Over Ethernet POE handler
//
static i32 handler_config_poe(CYG_HTTPD_STATE *p)
{
    vtss_isid_t     isid = web_retrieve_request_sid(p); /* Includes USID = ISID */
    char            form_name[24];
    int             hidden_uport;
    port_iter_t     pit;
    mesa_port_no_t  iport, form_index;


    if (redirectUnmanagedOrInvalid(p, isid) || (isid != VTSS_ISID_START)) { /* Redirect unmanaged/invalid access to handler */
        return -1;
    }

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_POE)) {
        return -1;
    }
#endif


    CapArray<poe_chipset_t, MESA_CAP_PORT_CNT> poe_chip_found;
    poe_mgmt_is_chip_found(&poe_chip_found[0]);

    T_N("Web Accessing PoE configuration");

    poe_conf_t poe_local_conf;
    poe_config_get(&poe_local_conf);

    //
    // Setting new configuration
    //
    if (p->method == CYG_HTTPD_METHOD_POST) {
        T_N("Web Setting PoE configuration");
        const char *id;
        size_t len;
        BOOL mgmt_mode_reserved;
        id = cyg_httpd_form_varable_string(p, "mode_group", &len);
        if (id != NULL && len == strlen("ActualConsumption") && memcmp(id, "ActualConsumption", len) == 0) {
            mgmt_mode_reserved = FALSE;
        } else {
            mgmt_mode_reserved = TRUE;
        }

        id = cyg_httpd_form_varable_string(p, "base_group", &len);
        // Get management configuration
        if (id != NULL && len == strlen("Class") && memcmp(id, "Class", len) == 0) {
            T_D("Class radio button was set");
            if (mgmt_mode_reserved) {
                poe_local_conf.power_mgmt_mode = VTSS_APPL_POE_CLASS_RESERVED;
            } else {
                poe_local_conf.power_mgmt_mode = VTSS_APPL_POE_CLASS_CONSUMP;
            }


        } else if (id != NULL && len == strlen("Allocation") && memcmp(id, "Allocation", len) == 0) {
            T_D("Allocated radio button was set");
            if (mgmt_mode_reserved) {
                poe_local_conf.power_mgmt_mode = VTSS_APPL_POE_ALLOCATED_RESERVED;
            } else {
                poe_local_conf.power_mgmt_mode = VTSS_APPL_POE_ALLOCATED_CONSUMP;
            }
        } else if (id != NULL && len == strlen("LLDP-Med") && memcmp(id, "LLDP-Med", len) == 0) {
            T_D("LLDP-Med radio button was set");
            if (mgmt_mode_reserved) {
                poe_local_conf.power_mgmt_mode = VTSS_APPL_POE_LLDPMED_RESERVED;
            } else {
                poe_local_conf.power_mgmt_mode = VTSS_APPL_POE_LLDPMED_CONSUMP;
            }
        } else {
            T_E ("Unknown radio button - defaulting management mode to CLASS");
            poe_local_conf.power_mgmt_mode = VTSS_APPL_POE_CLASS_CONSUMP;
        }

        //
        // Parameters that are valid for all switches
        //

        // Power supplies
        poe_local_conf.primary_power_supply    = httpd_form_get_value_int(p, "PrimaryPowerSupply", 0, vtss_appl_poe_supply_max_get());
        if (poe_mgmt_is_backup_power_supported()) {
            poe_local_conf.backup_power_supply = httpd_form_get_value_int(p, "BackupPowerSupply",  0, vtss_appl_poe_supply_max_get());
        }


        // Priority, PoE enable and  Max. Port Power
        for (form_index = 0; form_index < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); form_index++) {
            // Because some ports might not support PoE, a hidden web form shows the real port number for the corresponding
            // web form. We do only continue if the hidden port number is found.
            sprintf(form_name, "hidden_portno_%u", form_index);
            if (cyg_httpd_form_varable_int(p, form_name, &hidden_uport)) {
                iport = uport2iport(hidden_uport);

                // PoE mode
                sprintf(form_name, "hidden_poe_mode_%u", form_index); // Set form name
                poe_local_conf.poe_mode[iport] = (vtss_appl_poe_port_mode_t)httpd_form_get_value_int(p, &form_name[0], 0, 2);

                // Max power.
                sprintf(form_name, "hidden_max_power_%u", form_index);
                poe_local_conf.max_port_power[iport] = httpd_form_get_value_int(p, &form_name[0], 0, poe_port_max_power_get(iport));

                // Priority
                sprintf(form_name, "hidden_priority_%u", form_index); // Set form name
                poe_local_conf.priority[iport] = (vtss_appl_poe_port_power_priority_t)httpd_form_get_value_int(p, &form_name[0], 0, 3);
                T_D_PORT(iport, "poe_local_conf.poe_mode=%d, poe_local_conf.max_port_power=%d, poe_local_conf.priority=%d",
                         poe_local_conf.poe_mode[iport], poe_local_conf.max_port_power[iport], poe_local_conf.priority[iport]);
            }
        }

        // Capacitor Detection
#ifdef LNTN_SW_POE_LEGACY_PD
        id = cyg_httpd_form_varable_string(p, "cap_detect_group", &len);

        poe_local_conf.cap_detect = (id  != NULL && len == strlen("en") &&
                                     memcmp(id, "en", len) == 0) ?
                                    POE_CAP_DETECT_ENABLED : POE_CAP_DETECT_DISABLED;
#endif
        // Write back the configuration
        poe_config_set(&poe_local_conf);

        redirect(p, "/poe_config.htm");

    } else {

        //
        // Getting the configuration.
        //
        T_N("Web Getting PoE");
        int ct;

        cyg_httpd_start_chunked("html");
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d/%d/%d|%d/%d/%d/%d/%d/%d|",
                      poe_local_conf.power_mgmt_mode,
#ifdef LNTN_SW_POE_LEGACY_PD
// signal that LEGACY PD mode is supported
                      1,
#else
// signal that LEGACY PD mode is NOT supported
                      0,
#endif
                      poe_local_conf.cap_detect,
                      poe_local_conf.primary_power_supply,
                      poe_local_conf.backup_power_supply,
                      poe_mgmt_is_backup_power_supported(),// Hardware supports backup power supply. Signal that to the web interface
                      vtss_appl_poe_supply_min_get(),
                      vtss_appl_poe_supply_max_get(),
#ifdef VTSS_SW_OPTION_LLDP
// signal that LLDP is supported
                      1
#else
// signal that LLDP is NOT supported
                      0
#endif

                     );

        cyg_httpd_write_chunked(p->outbuffer, ct);

        if (port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_FRONT) == VTSS_OK) {
            while (port_iter_getnext(&pit)) {
                // Pass configuration to Web
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%d/%d/%d/%d,",
                              pit.uport,
                              poe_local_conf.priority[pit.iport],
                              poe_local_conf.max_port_power[pit.iport],
                              poe_local_conf.poe_mode[pit.iport],
                              poe_chip_found[pit.iport] != NO_POE_CHIPSET_FOUND);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            } // End for loop
        }
        T_N("cyg_httpd_write_chunked -> %s", p->outbuffer);
        cyg_httpd_end_chunked();

    }
    return -1; // Do not further search the file system.
}

// Status
static i32 handler_status_poe(CYG_HTTPD_STATE *p)
{
    vtss_isid_t    isid = web_retrieve_request_sid(p); /* Includes USID = ISID */
    port_iter_t    pit;
    char           err_msg[100];
    char           class_str[10];
    strcpy(err_msg, ""); // No errors so far :)

    if (redirectUnmanagedOrInvalid(p, isid) || (isid != VTSS_ISID_START)) { /* Redirect unmanaged/invalid access to handler */
        return -1;
    }

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_POE)) {
        return -1;
    }
#endif

    CapArray<poe_chipset_t, MESA_CAP_PORT_CNT> poe_chip_found;
    poe_mgmt_is_chip_found(&poe_chip_found[0]);

    T_D("Web Accessing PoE Status");
    poe_status_t          poe_status;

    // Get the status fields.
    poe_mgmt_get_status(&poe_status);

    poe_conf_t poe_local_conf;
    poe_config_get(&poe_local_conf);

    //
    // Getting the status
    //
    T_D("Web Getting PoE Status");
    int ct;

    cyg_httpd_start_chunked("html");

    // Update with error message.
    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s!",
                  err_msg);

    cyg_httpd_write_chunked(p->outbuffer, ct);



    if (port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_FRONT) == VTSS_OK) {
        while (port_iter_getnext(&pit)) {
            T_D("PoE - %u/%d/%d/%d/%d",
                pit.uport,
                poe_status.power_allocated[pit.iport],
                poe_status.power_used[pit.iport],
                poe_local_conf.priority[pit.iport],
                poe_status.current_used[pit.iport]
               );

            // Pass configuration to Web
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%d/%d/%d/%d/%s/%s/%d|",
                          pit.uport,
                          poe_status.power_requested[pit.iport],
                          poe_status.power_used[pit.iport],
                          poe_local_conf.priority[pit.iport],
                          poe_status.current_used[pit.iport],
                          poe_status2str(poe_status.port_status[pit.iport], pit.iport, &poe_local_conf),
                          poe_class2str(&poe_status, pit.iport, &class_str[0]),
                          poe_status.power_allocated[pit.iport]);
            cyg_httpd_write_chunked(p->outbuffer, ct);
        }
    }

    T_D("cyg_httpd_write_chunked -> %s", p->outbuffer);
    cyg_httpd_end_chunked();
    return -1; // Do not further search the file system.
}

static i32 handler_conf_ping_alive(CYG_HTTPD_STATE *p)
{
    vtss_isid_t sid = web_retrieve_request_sid(p); /* Includes USID = ISID */
    port_iter_t pit;
    int ct, iport;
    BOOL state_changed, interval_changed;
    BOOL state;
    int interval;
    mesa_ipv4_t address;
    ping_alive_local_t ping_alive_local;
    CapArray<poe_chipset_t, MESA_CAP_PORT_CNT> poe_chip_found;

    if (redirectUnmanagedOrInvalid(p, sid)) { /* Redirect unmanaged/invalid access to handler */
        return -1;
    }
#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_POE)) {
        return -1;
    }
#endif

    poe_mgmt_is_chip_found(&poe_chip_found[0]);

    // web set
    if (p->method == CYG_HTTPD_METHOD_POST) {

        ping_alive_get_local_config(&ping_alive_local);

        for (iport = 0; iport < MESA_CAP(MESA_CAP_PORT_CNT); iport++) {

            state_changed = 0;
            interval_changed = 0;
            // Because some ports might not support PoE, a hidden web form shows the real port number for the corresponding
            // web form. We do only continue if the hidden port number is found.
            if (poe_chip_found[iport] != NO_POE_CHIPSET_FOUND) {

                //IPv4
                if (web_parse_ipv4_fmt(p, &address, "ipv4_%d", iport) == VTSS_OK){
                    ping_alive_local.address[iport] = address;
                }

                //Interval
                if (cyg_httpd_form_variable_int_fmt(p, &interval, "interval_%d", iport) == TRUE) {
                    interval_changed = abs(ping_alive_local.interval[iport] - interval) > 0 ? TRUE : FALSE;
                    ping_alive_local.interval[iport] = interval;
                }

                //State
                state = cyg_httpd_form_variable_check_fmt(p, "enable_%d", iport);
                state_changed = abs(ping_alive_local.state[iport] - state) > 0 ? TRUE : FALSE;
                ping_alive_local.state[iport] = state;

                ping_alive_local.changed[iport] = (state_changed || interval_changed);
            }
        }
        // Set new configuration
        ping_alive_set_local_config(&ping_alive_local);

        redirect(p, "/poe_ping_alive.htm");

    } else {  //web get
        cyg_httpd_start_chunked("html");

        ping_alive_get_local_config(&ping_alive_local);
        /* Ports */
        (void)port_iter_init(&pit, NULL, sid,  PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);

        while (port_iter_getnext(&pit)) {
            int iport = pit.iport, uport = pit.uport;
            i8 icmp_dest[253];
            address = ping_alive_local.address[iport];

            sprintf(icmp_dest, "%u.%u.%u.%u",
                (address & 0xff000000) >> 24,
                (address & 0x00ff0000) >> 16,
                (address & 0x0000ff00) >>  8,
                (address & 0x000000ff) >>  0);

            ct = snprintf(p->outbuffer, sizeof(p->outbuffer),"%d/%d/%s/%d/%d|",
                uport,
                ping_alive_local.state[iport],
                icmp_dest,
                ping_alive_local.interval[iport],
                poe_chip_found[iport] != NO_POE_CHIPSET_FOUND?0:1);
                cyg_httpd_write_chunked(p->outbuffer, ct);
        }

        cyg_httpd_end_chunked();
    }
    return -1; // Do not further search the file system.
}

static i32 handler_conf_poe_schedule(CYG_HTTPD_STATE *p)
{
    int ct, index, sched_id, mode, sched;
    port_iter_t pit;
    CapArray<poe_chipset_t, MESA_CAP_PORT_CNT> poe_chip_found;
    sched_port_local_t sched_port_local;
    sched_time_local_t sched_time_local;
    vtss_isid_t sid = web_retrieve_request_sid(p); /* Includes USID = ISID */

    if (redirectUnmanagedOrInvalid(p, sid)) { /* Redirect unmanaged/invalid access to handler */
        return -1;
    }
#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_POE)) {
        return -1;
    }
#endif

    poe_mgmt_is_chip_found(&poe_chip_found[0]);

    // web set
    if (p->method == CYG_HTTPD_METHOD_POST) {

        sched_port_get_local_config(&sched_port_local);
        sched_time_get_local_config(&sched_time_local);

        for (index = 0; index < MESA_CAP(MESA_CAP_PORT_CNT); index++) {
            if (poe_chip_found[index] != NO_POE_CHIPSET_FOUND) {
                if (cyg_httpd_form_variable_int_fmt(p, &mode, "mode_%d", index)) {
                    sched_port_local.mode[index] = (sched_mode_t)mode;
                }

                if (cyg_httpd_form_variable_int_fmt(p, &sched_id, "id_%d", index)) {
                    sched_port_local.sched_id[index] = sched_id;
                }
            }
        }

        for (index = 1; index <= POE_SCHEDULE_MAX_NUM; index++) {
            if (cyg_httpd_form_variable_int_fmt(p, &sched, "sched_add_%d", index)) {
                sched_time_local.sched_time[sched - 1].status = 1;
            }

            if (cyg_httpd_form_variable_check_fmt(p, "delete_%d", index)) {
                memset(&(sched_time_local.sched_time[index - 1]), 0, sizeof(sched_time_t));
            }
        }
        // // Set new configuration
        sched_port_set_local_config(&sched_port_local);
        sched_time_set_local_config(&sched_time_local);

        redirect(p, "/poe_schedule.htm");

    } else {  // web get
        cyg_httpd_start_chunked("html");

        sched_port_get_local_config(&sched_port_local);
        sched_time_get_local_config(&sched_time_local);

        /* Ports */
        (void)port_iter_init(&pit, NULL, sid,  PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
        while (port_iter_getnext(&pit)) {
            int iport = pit.iport, uport = pit.uport;

            ct = snprintf(p->outbuffer, sizeof(p->outbuffer),"%d/%d/%d/%d|",
                                        uport,
                                        sched_port_local.mode[iport],
                                        sched_port_local.sched_id[iport],
                                        poe_chip_found[iport] != NO_POE_CHIPSET_FOUND?0:1);
            cyg_httpd_write_chunked(p->outbuffer, ct);
        }
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer),",");
        cyg_httpd_write_chunked(p->outbuffer, ct);

        for (index = 0; index < POE_SCHEDULE_MAX_NUM; index++) {
            if (sched_time_local.sched_time[index].status == TRUE) {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer),"%d/",index+1);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            }
        }

        cyg_httpd_end_chunked();
    }

    return -1; // Do not further search the file system.
}

static i32 handler_conf_poe_schedule_time(CYG_HTTPD_STATE *p)
{
    int week_day_num = 7;
    int ct, index, day, sched_id;
    char buf[10];
    size_t len;
    const char        *var_string;
    const char        *startBuf;
    const char        *endBuf;
    sched_time_local_t sched_time_local;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_POE)) {
        return -1;
    }
#endif

    // web set
    if (p->method == CYG_HTTPD_METHOD_POST) {
        sched_time_get_local_config(&sched_time_local);

        cyg_httpd_form_varable_int(p, "id", &sched_id);

        for (day = 0; day < week_day_num; day ++) {

            //Start Time
            len = 0;
            memset(buf, 0, sizeof(buf));
            startBuf = cyg_httpd_form_variable_str_fmt(p, &len, "start_%d", day);
            if (cgi_unescape(startBuf, buf, len, sizeof(buf)) == FALSE) {
                T_D("cgi_unescape( %s )\n", startBuf);
                redirect(p, "/poe_schedule.htm");
                return -1;
            }
            sscanf(buf, "%d:%d", &(sched_time_local.sched_time[sched_id - 1].start_hour[day]),
                                    &(sched_time_local.sched_time[sched_id - 1].start_min[day]));

            //End Time
            len = 0;
            memset(buf, 0, sizeof(buf));
            endBuf = cyg_httpd_form_variable_str_fmt(p, &len, "end_%d", day);
            if (cgi_unescape(endBuf, buf, len, sizeof(buf)) == FALSE) {
                T_D("cgi_unescape( %s )\n", endBuf);
                redirect(p, "/poe_schedule.htm");
                return -1;
            }
            sscanf(buf, "%d:%d", &(sched_time_local.sched_time[sched_id - 1].end_hour[day]),
                                    &(sched_time_local.sched_time[sched_id - 1].end_min[day]));

        }
        sched_time_set_local_config(&sched_time_local);

        redirect(p, "/poe_schedule.htm");

    } else {  //web get
        cyg_httpd_start_chunked("html");

        sched_time_get_local_config(&sched_time_local);

        len = 0;
        var_string = cyg_httpd_form_varable_string(p, "id", &len);
        if ( var_string == NULL || len == 0 ) {
            T_D("fail to get schedule id\n");
            redirect(p, "/poe_schedule.htm");
            return -1;
        }

        sprintf(buf,"%s",var_string);

        for (index = 0; index < POE_SCHEDULE_MAX_NUM; index++) {
            if (sched_time_local.sched_time[index].status == TRUE) {
                //schedule id
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer),"%d/",index+1);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            }
        }
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer),"|");
        cyg_httpd_write_chunked(p->outbuffer, ct);


        sched_id = atoi(buf);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer),"%d/",sched_id);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        //schedule time
        for (day = 0; day < WEEKDAY_MAX_NUM; day++) {
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer),"%02d:%02d/%02d:%02d/",
            sched_time_local.sched_time[sched_id - 1].start_hour[day],
            sched_time_local.sched_time[sched_id - 1].start_min[day],
            sched_time_local.sched_time[sched_id - 1].end_hour[day],
            sched_time_local.sched_time[sched_id - 1].end_min[day]);

            cyg_httpd_write_chunked(p->outbuffer, ct);
        }
            cyg_httpd_end_chunked();
    }
    return -1; // Do not further search the file system.
}

/****************************************************************************/
/*  HTTPD Table Entries                                                     */
/****************************************************************************/

CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_config_poe, "/config/poe_config", handler_config_poe);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_status_poe, "/stat/poe_status", handler_status_poe);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_config_poe_ping_alive,     "/config/poe_ping_alive", handler_conf_ping_alive);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_config_poe_schedule,       "/config/poe_schedule", handler_conf_poe_schedule);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_config_poe_schedule_time,  "/config/poe_schedule_time", handler_conf_poe_schedule_time);


/****************************************************************************/
/*  End of file.                                                            */
/****************************************************************************/
