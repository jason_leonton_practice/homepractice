/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _VTSS_APPL_POE_TYPES_H_
#define _VTSS_APPL_POE_TYPES_H_

typedef enum {
    POE_CAP_DETECT_DISABLED,
    POE_CAP_DETECT_ENABLED
} poe_cap_detect_t; // Capacitor detection enabled (or not)

typedef enum {
    UNKNOWN,
    PRIMARY,
    BACKUP,
    RESERVED
} poe_power_source_t; // power source, see TIA-1057 table 16 or IEEE 802.3at table 33-22 (bits 5:4)

/**********************************************************************
* Struct for defining PoE capabilites for each port
**********************************************************************/
typedef struct {
    BOOL available;    /* True is PoE is available for the port  */
    u8   pse_pairs_control_ability;  // pethPsePortPowerPairsControlAbility, rfc3621
    u8   pse_power_pair;             // pethPsePortPowerPairs, rfc3621
} poe_entry_t;

// Definitions of PoE chipset we support. Note: NO_POE_CHIPSET_FOUND MUST be the last.
typedef enum {
    PD69200,
    SI3452,
    SLUS787,
    LTC4271,  // Leonton uses the chip
    POE_CHIPSET_DETECTION, // PoE chipset detection in progress
    NO_POE_CHIPSET_FOUND
} poe_chipset_t;

typedef struct {
    u16 pse_allocated_power;
    u16 pd_requested_power;
    u8  pse_power_type;
    u8  power_class;
    u8  pse_power_pair;
    u8  mdi_power_status;
    u8  cable_len;
    u16 power_indicator;
} poe_pse_data_t;

typedef struct {
    u8  type;
    u16 pd_requested_power;
    u16 pse_allocated_power;
    u8  cable_len;
    u8  execute_lldp;
} poe_pd_data_t;

/**********************************************************************
 ** Status structs
 **********************************************************************/

/* PoE status for the switch */
typedef struct {
    // The power for reserved by the PD
    CapArray<int, MESA_CAP_PORT_CNT> power_allocated;
    // The power for requested by the PD
    CapArray<int, MESA_CAP_PORT_CNT> power_requested;
    // The power that the PD is using right now
    CapArray<int, MESA_CAP_PORT_CNT> power_used;
    // The current the PD is using right now
    CapArray<int, MESA_CAP_PORT_CNT> current_used;
    // Port status - Is on/off and why.
    CapArray<vtss_appl_poe_status_type_t, MESA_CAP_PORT_CNT> port_status;
    // The class of the PD is using right now
    CapArray<int, MESA_CAP_PORT_CNT> pd_class;
    // List with the poe chip found for corresponding port.
    CapArray<poe_chipset_t, MESA_CAP_PORT_CNT> poe_chip_found;

    // Total Power used currently
    u32                         total_power_used;

    // Total current used
    u32                         total_current_used;

    // ThecTotal amount of power reserved by the PDs
    u32                         total_power_reserved;
} poe_status_t;

/**********************************************************************
 ** Configuration structs
 **********************************************************************/

/* Configuration for the local switch */
typedef struct {
    /* The Max power that the primary power supply can give.Given with one digit.
       E.g. 209 equals 20.9W */
    int                                  primary_power_supply ;
    /* The Max power that the backup (normally a battery) power supply can give.
    Given with one digit. E.g. 209 equals 20.9W */
    int                                  backup_power_supply  ;
    /* Priority for each port*/
    CapArray<vtss_appl_poe_port_power_priority_t, MESA_CAP_PORT_CNT> priority ;
    /* Maximum power allowed for each port ( Set by user )*/
    CapArray<int, MESA_CAP_PORT_CNT> max_port_power;
    /* It is possible to select mode for each port individually.*/
    CapArray<vtss_appl_poe_port_mode_t, MESA_CAP_PORT_CNT> poe_mode;

    poe_cap_detect_t   cap_detect;      // Capacitor detection enabled (or not)

    /* Some customers requires some spare power for powering the system. E.g. they like to configure a power supply of 180W, but only give 170 W for PoE. */
    u16 system_power_reserve;

    vtss_appl_poe_management_mode_t power_mgmt_mode; // Power management mode
} poe_conf_t;

inline bool operator==(const poe_conf_t &a, const poe_conf_t &b)
{
#define TRY_CMP(X) if (a.X != b.X) return false
    TRY_CMP(primary_power_supply);
    TRY_CMP(backup_power_supply);
    TRY_CMP(priority);
    TRY_CMP(max_port_power);
    TRY_CMP(poe_mode);
    TRY_CMP(cap_detect);
    TRY_CMP(system_power_reserve);
    TRY_CMP(power_mgmt_mode);
#undef TRY_CMP
    return true;
}

inline bool operator!=(const poe_conf_t &a, const poe_conf_t &b)
{
    return !(a == b);
}

#endif  /* _VTSS_APPL_POE_TYPES_H_ */
