/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
*/
#ifndef __POE_SERIALIZER_HXX__
#define __POE_SERIALIZER_HXX__

#include "vtss/basics/snmp.hxx"
#include "vtss_appl_serialize.hxx"
#include "vtss/appl/poe.h"
#include "vtss/appl/interface.h"
#include "vtss_appl_formatting_tags.hxx"
#include "vtss_common_iterator.hxx"

extern const vtss_enum_descriptor_t vtss_appl_poe_management_mode_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_poe_management_mode_t, "poeMgmtModeType",
                         vtss_appl_poe_management_mode_txt,
                         "This enumeration defines the types of PoE management mode.");

extern const vtss_enum_descriptor_t vtss_appl_poe_port_mode_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_poe_port_mode_t, "poeModeType",
                         vtss_appl_poe_port_mode_txt,
                         "This enumeration defines the types of port PoE mode.");

extern const vtss_enum_descriptor_t vtss_appl_poe_status_type_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_poe_status_type_t, "poeStatusType",
                         vtss_appl_poe_status_type_txt,
                         "This enumeration defines the feature status.");

extern const vtss_enum_descriptor_t vtss_appl_poe_port_power_priority_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_poe_port_power_priority_t, "poePowerPriorityType",
                         vtss_appl_poe_port_power_priority_txt,
                         "This enumeration defines the port power priority.");

extern const vtss_enum_descriptor_t vtss_appl_poe_led_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_poe_led_t, "poeLedColor",
                         vtss_appl_poe_led_txt,
                         "This enumeration defines the led color.");

/*****************************************************************************
  - MIB row/table indexes serializer
*****************************************************************************/
VTSS_SNMP_TAG_SERIALIZE(POE_ifindex_index, vtss_ifindex_t, a, s) {
    a.add_leaf(vtss::AsInterfaceIndex(s.inner), vtss::tag::Name("IfIndex"),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(0),
               vtss::tag::Description("Logical interface number."));
}

VTSS_SNMP_TAG_SERIALIZE(POE_usid_index, vtss_usid_t, a, s) {
    a.add_leaf(s.inner, vtss::tag::Name("SwitchId"),
               vtss::expose::snmp::RangeSpec<uint32_t>(1, 16),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(0),
               vtss::tag::Description("User Switch Id."));
}

template <typename T>
void serialize(T &a, vtss_appl_poe_port_capabilities_t &s) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_poe_port_capabilities_t"));
    int ix = 0;

    m.add_leaf(vtss::AsBool(s.poe_capable), vtss::tag::Name("PoE"),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "Indicates whether interface is PoE capable or not."));
}

template <typename T>
void serialize(T &a, vtss_appl_poe_global_conf_t &s) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_poe_global_conf_t"));
    int ix = 0;
    m.add_leaf(s.mgmt_mode, vtss::tag::Name("ManagementMode"),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                     "PoE management modes, six PoE management modes are supported. "
                     "CLASS_RESERVED: Maximum port power determined by class, "
                     "and power is managed according to power consumption. "
                     "CLASS_CONSUMP: Maximum port power determined by class, "
                     "and power is managed according to reserved power. "
                     "ALLOCATED_RESERVED: Maximum port power determined by allocation, "
                     "and power is managed according to power consumption. "
                     "ALLOCATED_CONSUMP: Maximum port power determined by allocation, "
                     "and power is managed according to reserved power. "
                     "LLDPMED_RESERVED: Maximum port power determined by LLDP Media protocol, "
                     "and power is managed according to reserved power. "
                     "LLDPMED_CONSUMP: Maximum port power determined by LLDP Media protocol, "
                     "and power is managed according to power consumption."));
}

template <typename T>
void serialize(T &a, vtss_appl_poe_conf_t &s) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_poe_conf_t"));
    int ix = 0;
    m.add_leaf(s.primary_power_supply, vtss::tag::Name("MaxPower"),
               vtss::expose::snmp::RangeSpec<uint32_t>(0, vtss_appl_poe_supply_max_get()),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                   "The Maximum power(in Watt) "
                   "that the power sourcing equipment(such as switch) can deliver."));

#ifdef LNTN_SW_POE_LEGACY_PD
    m.add_leaf(vtss::AsBool(s.capacitor_detect), vtss::tag::Name("CapacitorDetection"),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                   "Indicates whether switch capacitor detection feature is enabled or not."));
#endif
}

template <typename T>
void serialize(T &a, vtss_appl_poe_port_conf_t &s) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_poe_port_conf_t"));
    int ix = 0;
    m.add_leaf(s.mode, vtss::tag::Name("Mode"),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                   "Set PoE mode or disable PoE feature, two PoE modes are supported. "
                   "POE: Enables PoE based on IEEE 802.3af standard, "
                   "and provides power up to 15.4W(154 deciwatt) of DC power to powered device. When changing to standard mode the MaxPower is automatically adjust to 15.4 W, if it currently exceeds 15.4 W. "
                   "POE_PLUS: Enabled PoE based on IEEE 802.3at standard, "
                   "and provides power up to 30W(300 deciwatt) of DC power to powered device."));

    m.add_leaf(s.priority, vtss::tag::Name("Priority"),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                   "Set port power priority. "
                   "Priority determines the order in which the interfaces will receive power. "
                   "Interfaces with a higher priority will receive power before interfaces with a lower priority. "
                   "PRIORITY_LOW means lowest priority. "
                   "PRIORITY_HIGH means medium priority. "
                   "PRIORITY_CRITICAL means highest priority."));

    m.add_leaf(s.max_port_power, vtss::tag::Name("MaxPower"),
               vtss::expose::snmp::RangeSpec<uint32_t>(0, 300),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                   "Maximum port power(in deciwatt) that can be delivered to remote device. "
                   "Maximum allowed values from 0 to 154 deciwatt in POE standard mode, "
                   "and from 0 to 300 deciwatt in POE plus mode."));

}

template <typename T>
void serialize(T &a, vtss_appl_poe_port_status_t &s) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_poe_port_status_t"));
    int ix = 0;
    m.add_leaf(s.pd_class, vtss::tag::Name("PDClass"),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                   "Powered device(PD) negotiates a power class with sourcing equipment(PSE) "
                   "during the time of initial connection, each class have a maximum supported power. "
                   "Class assigned to PD is based on PD electrical characteristics. "
                   "Value -1 means either PD attached to the interface can not advertise its power class "
                   "or no PD detected or PoE is not supported or "
                   "PoE feature is disabled or unsupported PD class(classes 0-4 is supported)."));

    m.add_leaf(s.port_status, vtss::tag::Name("CurrentState"),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                   "Indicate port status. "
                   "notSupported: PoE not supported"
                   "budgetExceeded: PoE is turned OFF due to power budget exceeded on PSE. "
                   "noPoweredDeviceDetected: No PD detected. "
                   "poweredDeviceOn: PSE supplying power to PD through PoE. "
                   "poweredDeviceOverloaded: PD consumes more power than the maximum limit configured on the PSE port."
                   "poweredDeviceOff: PD is powered down"
                   "unknownState: PD state unknown"
                   "disabled: PoE is disabled for the interface"
                   "disabledInterfaceShutdown: PD is powered down due to interface shut-down"));

    m.add_leaf(s.power_consume, vtss::tag::Name("PowerConsumption"),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                   "Indicates the power(in deciwatt) that the PD is consuming right now."));

    m.add_leaf(s.power_requested, vtss::tag::Name("PowerReserved"),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                   "Indicates the power(in deciwatt) that the PD has requested/reserved."));

    m.add_leaf(s.current_consume, vtss::tag::Name("CurrentConsumption"),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                   "Indicates the current(in mA) that the PD is consuming right now."));
}

template <typename T>
void serialize(T &a, vtss_appl_poe_powerin_status_t &s) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_poe_powerin_status_t"));
    int ix = 0;
    m.add_leaf(
        vtss::AsBool(s.pwr_in_status1),
        vtss::tag::Name("PowerInStatus1"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description(
            "The current status of 1st power supplies (ON/OFF)."
            "true  means power supply is available (ON)"
            "false means power supply is not available (OFF)"
                               ));

    m.add_leaf(
        vtss::AsBool(s.pwr_in_status2),
        vtss::tag::Name("PowerInStatus2"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description(
            "The current status of 2nd power supplies (ON/OFF)."
            "true  means power supply is available (ON)"
            "false means power supply is not available (OFF)"
                               ));

    m.add_leaf(s.total_power_used, vtss::tag::Name("TotalPowerConsumption"),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                   "Indicates the total power(in deciwatt) that is consumed right now."));

    m.add_leaf(s.total_current_used, vtss::tag::Name("TotalCurrentConsumption"),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                   "Indicates the total current power(in mA) that is consumed right now."));

    m.add_leaf(s.total_power_reserved, vtss::tag::Name("TotalPowerReserved"),
               vtss::expose::snmp::Status::Current, vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Indicates the total power reserved by the PDs."));
}

template <typename T>
void serialize(T &a, vtss_appl_poe_powerin_led_t &s) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_poe_powerin_led_t"));
    int ix = 0;
    m.add_leaf(
        s.pwr_led1,
        vtss::tag::Name("PowerInLed1"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description(
            "The current led color for power supply 1"
                               )
               );
    m.add_leaf(
        s.pwr_led2,
        vtss::tag::Name("PowerInLed2"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description(
            "The current led color for power supply 2"
                               )
               );
}

template <typename T>
void serialize(T &a, vtss_appl_poe_status_led_t &s) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_poe_status_led_t"));
    int ix = 0;
    m.add_leaf(
        s.status_led,
        vtss::tag::Name("StatusLed"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description(
            "The current led color indicating PoE status"
                               )
               );
}

namespace vtss {
namespace appl {
namespace poe {
namespace interfaces {
struct poeGlobalConfigEntry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamVal<vtss_appl_poe_global_conf_t *>> P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_poe_global_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }
    VTSS_EXPOSE_GET_PTR(vtss_appl_poe_global_conf_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_poe_global_conf_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_POE);
};

struct poeSwitchConfigEntry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamKey<vtss_usid_t>,
            vtss::expose::ParamVal<vtss_appl_poe_conf_t *>> P;

    static constexpr const char *table_description =
            "This is a table to configure PoE configurations for a switch.";

    static constexpr const char *index_description =
            "Each switch has a set of PoE configurable parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_usid_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, POE_usid_index(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_poe_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_poe_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_iterator_switch);
    VTSS_EXPOSE_SET_PTR(vtss_appl_poe_conf_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_POE);
};

struct poeInterfaceConfigEntry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamKey<vtss_ifindex_t>,
            vtss::expose::ParamVal<vtss_appl_poe_port_conf_t *>> P;

    static constexpr const char *table_description =
            "This is a table to configure PoE configurations for a specific "
            "interface.";

    static constexpr const char *index_description =
            "Each interface has a set of PoE configurable parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, POE_ifindex_index(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_poe_port_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_poe_port_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_iterator_ifindex_front_port_exist);
    VTSS_EXPOSE_SET_PTR(vtss_appl_poe_port_conf_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_POE);
};

struct poeInterfaceStatusEntry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamKey<vtss_ifindex_t>,
            vtss::expose::ParamVal<vtss_appl_poe_port_status_t *>> P;

    static constexpr const char *table_description =
            "This is a table to Power over Ethernet interface status";

    static constexpr const char *index_description =
            "Each interface has a set of status parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, POE_ifindex_index(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_poe_port_status_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_poe_port_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_iterator_ifindex_front_port_exist);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_POE);
};

struct poeInterfaceCapabilitiesEntry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamKey<vtss_ifindex_t>,
            vtss::expose::ParamVal<vtss_appl_poe_port_capabilities_t *>> P;

    static constexpr const char *table_description =
            "This is a table to interface capabilities";

    static constexpr const char *index_description =
            "Each interface has a set of capability parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, POE_ifindex_index(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_poe_port_capabilities_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_poe_port_capabilities_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_iterator_ifindex_front_port_exist);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_POE);
};

struct poeInterfacePowerInStatusEntry {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_appl_poe_powerin_status_t *>
        > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_poe_powerin_status_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_poe_powerin_status_get);
};

struct poeInterfacePowerLedStatusEntry {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_appl_poe_powerin_led_t *>
        > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_poe_powerin_led_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_poe_powerin_led_get);
};

struct poeInterfaceStatusLedEntry {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_appl_poe_status_led_t *>
        > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_poe_status_led_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_poe_status_led_get);
};

}  // namespace interfaces
}  // namespace poe
}  // namespace appl
}  // namespace vtss
#endif
