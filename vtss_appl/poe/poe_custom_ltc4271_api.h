/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable format
 (e.g. HEX file) and only in or with products utilizing the Microsemi switch and
 PHY products.  The source code of the software may not be disclosed, transmitted
 or distributed without the prior written permission of Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all ownership,
 copyright, trade secret and proprietary rights in the software and its source code,
 including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL WARRANTIES
 OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES ARE EXPRESS,
 IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION, WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND NON-INFRINGEMENT.

*/
/************************************************************-*- mode: C -*-*/
/*                                                                          */
/*           Copyright (C) 2007 Vitesse Semiconductor Corporation           */
/*                           All Rights Reserved.                           */
/*                                                                          */
/****************************************************************************/
/*                                                                          */
/*                            Copyright Notice:                             */
/*                                                                          */
/*  This document contains confidential and proprietary information.        */
/*  Reproduction or usage of this document, in part or whole, by any means, */
/*  electrical, mechanical, optical, chemical or otherwise is prohibited,   */
/*  without written permission from Vitesse Semiconductor Corporation.      */
/*                                                                          */
/*  The information contained herein is protected by Danish and             */
/*  international copyright laws.                                           */
/*                                                                          */
/****************************************************************************/
/*                                                                          */
/*  <DESCRIBE FILE CONTENTS HERE>                                           */
/*                                                                          */
/****************************************************************************/
/****************************************************************************/

#include "poe_custom_api.h"
#include "poe.h"
#include <vtss/api/types.h>

// Define how many ports each PoE chip controls.
#define PORTS_PER_POE_CHIP 4 //4 ports per chip - See Table 1 - SI3452 register map.


#ifdef __cplusplus
extern "C" {
#endif
int ltc4271_is_chip_available(mesa_port_no_t iport);
void ltc4271_poe_enable(mesa_port_no_t iport, BOOL enable);
void ltc4271_poe_init(mesa_port_no_t iport);
void ltc4271_get_port_measurement(poe_status_t *poe_status, mesa_port_no_t iport);
void ltc4271_port_status_get(vtss_appl_poe_status_type_t *port_status, mesa_port_no_t iport);
void ltc4271_get_all_port_class(char *classes, mesa_port_no_t iport);
void ltc4271_set_power_limit_channel(mesa_port_no_t iport, int max_port_power) ;
void ltc4271_device_wr(mesa_port_no_t iport, uchar reg_addr, char data);
void ltc4271_device_rd(mesa_port_no_t iport, uchar reg_addr, uchar *data);
void ltc4271_reset_command();
void ltc4271_capacitor_detection_set(BOOL enable);
uchar ltc4271_capacitor_detection_get();
void ltc4271_pse_interrupt_enable(vtss_port_no_t port_index, BOOL enable);
void ltc4271_update_vee(mesa_port_no_t port_index);
#ifdef __cplusplus
}
#endif
/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
