/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#ifdef VTSS_SW_OPTION_POE

#include "poe_api.h"
#include "poe.h" // For trace
#include "poe_trace.h"

#include "icli_api.h"
#include "icli_porting_util.h"
#include "poe_icli_functions.h"

#include "icfg_api.h" // For vtss_icfg_query_request_t
#include "misc_api.h" // for uport2iport
#include "msg_api.h" // For msg_switch_exists
#include "mgmt_api.h" //mgmt_str_float2long
#include "topo_api.h"   //topo_usid2isid(), topo_isid2usid()
#include "vtss_tftp_api.h"

/***************************************************************************/
/*  Type defines                                                           */
/***************************************************************************/
// Type used for selecting which poe icli configuration to update for the common function.
typedef enum {VTSS_POE_ICLI_MODE,
              VTSS_POE_ICLI_PRIORITY,
              VTSS_POE_ICLI_POWER_LIMIT
             } vtss_poe_icli_conf_t; // Which configuration do to

// Used to passed the PoE configuration value for the corresponding configuration type.
typedef union {
  // Mode
  struct {
    BOOL poe;
    BOOL poe_plus;
  } mode;

  // Priority
  struct {
    BOOL low;
    BOOL high;
    BOOL critical;
  } priority;

  // Power limit
  char *power_limit_value;
} poe_conf_value_t;

typedef struct {
    BOOL           state;
    u32            interval;
    mesa_ipv4_t    address;
} ping_alive_value_t;

typedef struct {
    u8             sched_id;
    sched_mode_t   mode;
} poe_sched_value_t;

// PoE power supply leds
extern vtss_appl_poe_led_t power1_led_color;
extern vtss_appl_poe_led_t power2_led_color;

// PoE status led
extern vtss_appl_poe_led_t status_led_color;

// Used to passed the PoE configuration value for the corresponding configuration type.
/***************************************************************************/
/*  Internal functions                                                     */
/****************************************************************************/
// Checking if PoE is supported for a least one of the interface currently selected
// IN : session_id - Current session id
// Return TRUE if at least one interface supporting PoE is found within the currently selected interfaces, else FALSE
static BOOL poe_icli_supported(u32 session_id)
{
  icli_variable_value_t       value;
  icli_stack_port_range_t     *plist;

  if (ICLI_MODE_PARA_GET(NULL, &value) != ICLI_RC_OK) {
    ICLI_PRINTF("%% Fail to get mode para\n");
    return FALSE;
  }

  CapArray<poe_chipset_t, MESA_CAP_PORT_CNT> poe_chip_found;

  plist = &(value.u.u_port_type_list); // port list

  if (plist->cnt >= ICLI_RANGE_LIST_CNT) {
    T_E("Something wrong with plist, defaulting to all ports. plist->cnt:%d", plist->cnt);
    plist = NULL;
  }

  // In case that the PoE chipset is not detected yet, we simply allow to setup PoE. This is needed because startup config is applied before the
  // PoE chipset is detected.
  if (!is_poe_ready()) {
    return TRUE;
  }

  port_iter_t pit;
  poe_mgmt_is_chip_found(&poe_chip_found[0]); // Get a list with which PoE chip there is associated to the port.

  VTSS_RC(icli_port_iter_init(&pit, VTSS_ISID_START, PORT_ITER_FLAGS_ALL));
  while (icli_port_iter_getnext(&pit, plist)) {
    T_NG_PORT(VTSS_TRACE_GRP_ICLI, pit.iport, "found:%d", poe_chip_found[pit.iport]);

    if (poe_chip_found[pit.iport] != NO_POE_CHIPSET_FOUND) {
      T_NG(VTSS_TRACE_GRP_ICLI, "found");
      return TRUE;
    }
  }

  T_NG(VTSS_TRACE_GRP_ICLI, "Not found");
  return FALSE;
}

// Function for checking is if PoE is supported for a specific port. If PoE isn't supported for the port, a printout is done.
// In : session_id - session_id of ICLI_PRINTF
//      iport - Internal port
//      uport - User port number
//      isid  - Internal switch id.
// Return: TRUE if PoE chipset is found for the iport, else FALSE
static BOOL is_poe_supported(i32 session_id, mesa_port_no_t iport, mesa_port_no_t uport, vtss_usid_t usid, poe_chipset_t *poe_chip_found)
{
  if (poe_chip_found[iport] == NO_POE_CHIPSET_FOUND) {
    T_DG_PORT(VTSS_TRACE_GRP_ICLI, iport, "PoE NOT supported:%d", poe_chip_found[iport]);
    icli_print_port_info_txt(session_id, usid, uport); // Print interface
    ICLI_PRINTF("does not have PoE support\n");
    return FALSE;
  } else {
    T_DG_PORT(VTSS_TRACE_GRP_ICLI, iport, "PoE supported:%d", poe_chip_found[iport]);
    return TRUE;
  }
}

static BOOL is_poe_feature(i32 session_id)
{
  if (!MESA_CAP(MEBA_CAP_POE)) {
    ICLI_PRINTF("Not support PoE feature\n");
    return FALSE;
  } else {
    T_D("Is PoE feature");
    return TRUE;
  }
}

// Help function for printing out status
// In : session_id - Session_Id for ICLI_PRINTF
//      debug  - Set to TRUE in order to get more PoE information printed
//      has_interface - TRUE if user has specified a specific interface
//      list - List of interfaces (ports)
static void poe_status(i32 session_id, const switch_iter_t *sit, BOOL debug, BOOL has_interface, icli_stack_port_range_t *plist)
{
  port_iter_t        pit;
  poe_status_t       status;
  char               txt_string[50];
  char               txt_string1[50];
  char               class_string[10];
  char               buf[250];

  // Header
  sprintf(buf, "%-23s %-9s %-15s %-41s %-16s %-19s", "Interface", "PD Class", "Power Reserved", "Port Status", "Power Used [W]", "Current Used [mA]");
  icli_table_header(session_id, buf); // Print header

  if (sit->isid != VTSS_ISID_START) {
    return;
  }
  poe_mgmt_get_status(&status); // Update the status fields

  poe_conf_t poe_conf;
  poe_config_get(&poe_conf);

  CapArray<poe_chipset_t, MESA_CAP_PORT_CNT> poe_chip_found;
  poe_mgmt_is_chip_found(&poe_chip_found[0]); // Get a list with which PoE chip there is associated to the port.

  // Loop through all front ports
  if (icli_port_iter_init(&pit, sit->isid, PORT_ITER_FLAGS_FRONT | PORT_ITER_SORT_ORDER_IPORT) == VTSS_RC_OK) {
    while (icli_port_iter_getnext(&pit, plist)) {
      if (is_poe_supported(session_id, pit.iport, pit.uport, sit->usid, &poe_chip_found[0])) {
        (void) icli_port_info_txt(sit->usid, pit.uport, buf);  // Get interface as printable string
        ICLI_PRINTF("%-23s %-9s %-15s %-41s %-16s %-19d \n",
                    &buf[0], // interface
                    poe_class2str(&status, pit.iport, &class_string[0]),
                    one_digi_float2str(status.power_requested[pit.iport], &txt_string1[0]),
                    poe_status2str(status.port_status[pit.iport], pit.iport, &poe_conf),
                    one_digi_float2str(status.power_used[pit.iport], &txt_string[0]),
                    status.current_used[pit.iport]);
      }
    }
  }

  ICLI_PRINTF("%-23s %-9s %-15s %-41s %-16s %-19d \n", "Total", "-", one_digi_float2str(status.total_power_reserved, &txt_string1[0]) ,
              "-", one_digi_float2str(status.total_power_used, &txt_string[0]), status.total_current_used);
}

// Help function for setting poe mode
// In : has_poe - TRUE is PoE port power shall be poe mode
//      has_poe_plus - TRUE if PoE port power shall be poe+ mode.
//      iport - Port in question
//      no - TRUE if mode shall be set to default.
//      poe_conf - Pointer to the current configuration.
static void poe_icli_mode_conf(const poe_conf_value_t *poe_conf_value, mesa_port_no_t iport, BOOL no, poe_conf_t *poe_conf)
{
  T_DG_PORT(VTSS_TRACE_GRP_ICLI, iport, "poe:%d, poe_plus:%d, no:%d", poe_conf_value->mode.poe, poe_conf_value->mode.poe_plus, no);
  // Update mode
  if (poe_conf_value->mode.poe) {
    poe_conf->poe_mode[iport] = VTSS_APPL_POE_MODE_POE;
  } else if (poe_conf_value->mode.poe_plus) {
    poe_conf->poe_mode[iport] = VTSS_APPL_POE_MODE_POE_PLUS;
  } else if (no) {
    poe_conf->poe_mode[iport] = VTSS_APPL_POE_MODE_DISABLED;
  }
}

// Help function for setting power limit
// In : Session_Id - session_id for ICLI_PRINTF
//      iport - Internal port in question
//      uport - User port id
//      value_str - new value (as string)
//      no - TRUE if mode shall be set to default.
//      poe_conf - Pointer to the current configuration.

static void poe_icli_power_limit_conf(i32 session_id, const port_iter_t *pit, vtss_usid_t usid, char *original_value_str, poe_conf_t *poe_conf, BOOL no)
{
  u16 port_power;
  char txt_string[50];
  char interface_str[50];
  long value;

  (void) icli_port_info_txt(usid, pit->uport, interface_str);  // Get interface as printable string

  // Take a copy of the original_value_str value string, because the mgmt_str_float2long is modifying the string.
  char value_str[100];
  misc_strncpyz(value_str, original_value_str, 100);

  if (no) {
    value = POE_MAX_POWER_DEFAULT;
  } else {
    // Convert from string to long. We don't check for valid range, that is done later.
    if (mgmt_str_float2long(value_str, &value, 0, 2147483647, 1) != VTSS_RC_OK) {
      T_DG_PORT(VTSS_TRACE_GRP_ICLI, pit->iport, "usid:%d, original_value_str:%s, value_str:%s", usid, original_value_str, value_str);
      ICLI_PRINTF("\"%s\" is an invalid power limit value for %s\n", original_value_str, interface_str);
      return;
    }
  }

  T_D("Setting max port power to %ld, poe_max_power_mode_dependent = %d",
      value, poe_max_power_mode_dependent(pit->iport, poe_conf->poe_mode[pit->iport]));

  // check for valid range
  if (value > poe_max_power_mode_dependent(pit->iport, poe_conf->poe_mode[pit->iport])) {
    port_power = poe_max_power_mode_dependent(pit->iport, poe_conf->poe_mode[pit->iport]);
    mgmt_long2str_float(&txt_string[0], port_power, 1);
    ICLI_PRINTF("Maximum allowed power (for the current mode) for %s is limited to %s W\n", interface_str, txt_string);
  } else {
    port_power = value;
  }

  poe_conf->max_port_power[pit->iport] = port_power;
}

// Function for configuring PoE priority
// IN - has_low - TRUE is priority
static void poe_icli_priority_conf(const poe_conf_value_t *poe_conf_value, mesa_port_no_t iport, BOOL no, poe_conf_t *poe_conf)
{
  // Update mode
  if (poe_conf_value->priority.low) {
    poe_conf->priority[iport] = VTSS_APPL_POE_PORT_POWER_PRIORITY_LOW;
  } else if (poe_conf_value->priority.high) {
    poe_conf->priority[iport] = VTSS_APPL_POE_PORT_POWER_PRIORITY_HIGH;
  } else if (poe_conf_value->priority.critical) {
    poe_conf->priority[iport] = VTSS_APPL_POE_PORT_POWER_PRIORITY_CRITICAL;
  } else if (no) {
    poe_conf->priority[iport] = POE_PRIORITY_DEFAULT;
  }
}

static void poe_check_local_set(i32 session_id, poe_conf_t *poe_conf)
{
  if (poe_config_set(poe_conf) == VTSS_APPL_POE_ERROR_PORT_POWER_EXCEEDED) {
    ICLI_PRINTF("Allocated power exceeds the amount of power that the power supply can provide\n");
  }
}


// Common function used for setting interface configurations.
// In : session_id - Session_Id for ICLI_PRINTF
//      poe_conf_type - Selecting which configuration to update.
//      poe_conf_value - New configuration value.
//      plist - List of interfaces to update
//      no - TRUE if user wants to set configuration to default
static void poe_icli_common(i32 session_id, vtss_poe_icli_conf_t poe_conf_type,
                            const poe_conf_value_t *poe_conf_value,
                            icli_stack_port_range_t *plist, BOOL no)
{
  poe_conf_t poe_conf;
  u8 isid_cnt = 0;

  // Just making sure that we don't access NULL pointer.
  if (plist == NULL) {
    T_E("plist was unexpected NULL");
    return;
  }
  T_IG(VTSS_TRACE_GRP_ICLI, "cnt:%d", plist->cnt);

  if (plist->switch_range[isid_cnt].isid == VTSS_ISID_START) {
    // Get current configuration
    poe_config_get(&poe_conf);

    CapArray<poe_chipset_t, MESA_CAP_PORT_CNT> poe_chip_found;
    poe_mgmt_is_chip_found(&poe_chip_found[0]); // Get a list with which PoE chip there is associated to the port.

    port_iter_t        pit;
    if (icli_port_iter_init(&pit, plist->switch_range[isid_cnt].isid, PORT_ITER_FLAGS_FRONT | PORT_ITER_SORT_ORDER_IPORT) == VTSS_RC_OK) {
      T_DG(VTSS_TRACE_GRP_ICLI, "isid:%d", plist->switch_range[isid_cnt].isid);
      while (icli_port_iter_getnext(&pit, plist)) {
        T_DG_PORT(VTSS_TRACE_GRP_ICLI, pit.iport, "poe_conf_type:%d", poe_conf_type);
        // Ignore if PoE isn't supported for this port.
        if (is_poe_supported(session_id, pit.iport, pit.uport, plist->switch_range[isid_cnt].usid, &poe_chip_found[0])) {
          switch  (poe_conf_type) {
          case VTSS_POE_ICLI_MODE:
            poe_icli_mode_conf(poe_conf_value, pit.iport, no, &poe_conf);
            break;
          case VTSS_POE_ICLI_PRIORITY:
            poe_icli_priority_conf(poe_conf_value, pit.iport, no, &poe_conf);
            break;

          case VTSS_POE_ICLI_POWER_LIMIT:
            poe_icli_power_limit_conf(session_id, &pit, plist->switch_range[isid_cnt].isid, poe_conf_value->power_limit_value, &poe_conf, no);
            break;

          default:
            T_E("Unknown poe_conf_type:%d", poe_conf_type);
          }
        }
      }
      // Set new configuration
      poe_check_local_set(session_id, &poe_conf);
    }
  }
}

// Function for configuring PoE power supply
//      value - New power supply value
//      no    - TRUE to restore to default
static void poe_icli_power_supply_set(i32 session_id, u32 value, BOOL no)
{

  // Get current configuration
  poe_conf_t poe_conf;

  poe_config_get(&poe_conf);

  if (no) {
    poe_conf.primary_power_supply = vtss_appl_poe_supply_default_get();
  } else {
    if (value > vtss_appl_poe_supply_max_get()) {
      ICLI_PRINTF("Maximum allowed power supply is limited to %u W\n", vtss_appl_poe_supply_max_get());
    } else {
      poe_conf.primary_power_supply = value;
    }
  }
  // Set new configuration
  poe_check_local_set(session_id, &poe_conf);
}

// Configuring System Reserve Power
// IN - isid  - isid for the switch to configure
//      value - New system power reserve
//      no    - TRUE to restore to default
static void poe_icli_system_reserve_power_set(i32 session_id, vtss_isid_t isid, u32 value, BOOL no)
{
  // Get current configuration
  poe_conf_t poe_conf;
  if (isid != VTSS_ISID_START) {
    return;
  }
  poe_config_get(&poe_conf);

  if (no) {
    poe_conf.system_power_reserve = VTSS_APPL_POE_SYSTEM_RESERVE_POWER_DEFAULT;
  } else {
    poe_conf.system_power_reserve = value;
  }
  // Set new configuration
  poe_check_local_set(session_id, &poe_conf);
}

// Function for configuring PoE power supply
// IN - isid  - isid for the switch to configure
//      value - New power supply value
//      no    - TRUE to restore to default
void poe_icli_cap_detect_set(i32 session_id, BOOL no)
{
  // Get current configuration
  poe_conf_t poe_conf;

  poe_config_get(&poe_conf);

  if (no) {
    poe_conf.cap_detect = POE_CAP_DETECT_DISABLED;
    T_IG(VTSS_TRACE_GRP_ICLI, "PoE Cap det disable");
  } else {
    T_IG(VTSS_TRACE_GRP_ICLI, "PoE Cap det enable");
    poe_conf.cap_detect = POE_CAP_DETECT_ENABLED;
  }
  // Set new configuration
  poe_check_local_set(session_id, &poe_conf);
}

/***************************************************************************/
/*  Functions called by iCLI                                                */
/****************************************************************************/
// Check if PoE is supported at all
BOOL poe_icli_runtime_supported(u32                session_id,
                                icli_runtime_ask_t ask,
                                icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
    T_D("MESA_CAP(MEBA_CAP_POE):0x%X", MESA_CAP(MEBA_CAP_POE));
    if (MESA_CAP(MEBA_CAP_POE)) {
      runtime->present = TRUE;
    } else {
      runtime->present = FALSE;
    }
    return TRUE;
  default:
    return FALSE;
  }
}

// Checks if PoE is supported for interfaces
BOOL poe_icli_interface_runtime_supported(u32                session_id,
                                          icli_runtime_ask_t ask,
                                          icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
    T_D("MESA_CAP(MEBA_CAP_POE):0x%X", MESA_CAP(MEBA_CAP_POE));
    if (MESA_CAP(MEBA_CAP_POE)) {
      runtime->present = poe_icli_supported(session_id);
    } else {
      runtime->present = FALSE;
    }
    return TRUE;
  default:
    return FALSE;
  }
}

// See poe_icli_functions.h
void poe_icli_power_supply(i32 session_id, u32 value, BOOL no, vtss_poe_icli_power_conf_t power_type)
{
  vtss_isid_t isid = VTSS_ISID_START;
  switch (power_type) {
  case VTSS_POE_ICLI_POWER_SUPPLY:
    poe_icli_power_supply_set(session_id, value, no);
    break;
  case VTSS_POE_ICLI_SYSTEM_RESERVE_POWER:
    poe_icli_system_reserve_power_set(session_id, isid, value, no);
    break;
  }
}

// See poe_icli_functions.h
void poe_icli_management_mode(i32 session_id, BOOL has_class_consumption, BOOL has_class_reserved_power, BOOL has_allocation_consumption, BOOL has_alllocation_reserved_power, BOOL has_lldp_consumption, BOOL has_lldp_reserved_power, BOOL no)
{
  poe_conf_t poe_conf;
  poe_config_get(&poe_conf);

  if (has_class_consumption) {
    poe_conf.power_mgmt_mode = VTSS_APPL_POE_CLASS_CONSUMP;
  } else if (has_class_reserved_power) {
    poe_conf.power_mgmt_mode = VTSS_APPL_POE_CLASS_RESERVED;
  } else if (has_alllocation_reserved_power) {
    poe_conf.power_mgmt_mode = VTSS_APPL_POE_ALLOCATED_RESERVED;
  } else if (has_allocation_consumption) {
    poe_conf.power_mgmt_mode = VTSS_APPL_POE_ALLOCATED_CONSUMP;
  } else if (has_lldp_reserved_power) {
    poe_conf.power_mgmt_mode = VTSS_APPL_POE_LLDPMED_RESERVED;
  } else if (has_lldp_consumption) {
    poe_conf.power_mgmt_mode = VTSS_APPL_POE_LLDPMED_CONSUMP;
  }

  if (no) {
    poe_conf.power_mgmt_mode = POE_MGMT_MODE_DEFAULT;
  }

  if (poe_config_set(&poe_conf) == VTSS_APPL_POE_ERROR_PORT_POWER_EXCEEDED) {
    ICLI_PRINTF("Allocated power exceeds the amount of power that the power supply can provide\n");
  }
}

// See poe_icli_functions.h
void poe_icli_priority(i32 session_id, BOOL has_low, BOOL has_high, BOOL has_critical, icli_stack_port_range_t *plist, BOOL no)
{
  T_DG(VTSS_TRACE_GRP_ICLI, "no:%d, has_high:%d, has_low:%d", no, has_high, has_low);
  poe_conf_value_t poe_conf_value;
  poe_conf_value.priority.low = has_low;
  poe_conf_value.priority.high = has_high;
  poe_conf_value.priority.critical = has_critical;

  poe_icli_common(session_id, VTSS_POE_ICLI_PRIORITY, &poe_conf_value, plist, no);
}

// See poe_icli_functions.h
mesa_rc poe_icli_mode(i32 session_id, BOOL has_poe, BOOL has_poe_plus, icli_stack_port_range_t *plist, BOOL no)
{
  poe_conf_value_t poe_conf_value;
  poe_conf_value.mode.poe = has_poe;
  poe_conf_value.mode.poe_plus = has_poe_plus;

  T_DG(VTSS_TRACE_GRP_ICLI, "has_poe:%d, has_poe_plus:%d, no:%d", has_poe, has_poe_plus, no);
  poe_icli_common(session_id, VTSS_POE_ICLI_MODE, &poe_conf_value, plist, no);
  return VTSS_RC_OK;
}

// See poe_icli_functions.h
void poe_icli_halt_at_i2c_err(i32 session_id)
{
  poe_halt_at_i2c_err(TRUE);
}

// See poe_icli_functions.h
// See poe_icli_functions.h
void poe_icli_power_limit(i32 session_id, char *value, icli_stack_port_range_t *plist, BOOL no)
{
  T_DG(VTSS_TRACE_GRP_ICLI, "no:%d", no);
  poe_conf_value_t poe_conf_value;
  poe_conf_value.power_limit_value = value;
  poe_icli_common(session_id, VTSS_POE_ICLI_POWER_LIMIT, &poe_conf_value, plist, no);
}

// See poe_icli_functions.h
void poe_icli_show(i32 session_id, BOOL has_interface, icli_stack_port_range_t *list)
{
  switch_iter_t sit;

  if (icli_switch_iter_init(&sit) != VTSS_RC_OK) {
    return;
  }
  while (icli_switch_iter_getnext(&sit, list)) {
    if (has_interface || msg_switch_exists(sit.isid)) {
      poe_status(session_id, &sit, FALSE, has_interface, list);
    }
  }
}

/* Get the current power supply status */
void poe_icli_get_power_in_status(i32 session_id)
{
  BOOL pwr_in_status1;
  BOOL pwr_in_status2;
  mesa_sgpio_port_data_t  data[MESA_SGPIO_PORTS];

  if ( mesa_sgpio_read(NULL, 0, 0, data) == VTSS_RC_OK ) {
    pwr_in_status1 = data[2].value[0];
    pwr_in_status2 = data[3].value[0];
    ICLI_PRINTF("%10s|%10s\n", "PowerIn", "Status");
    ICLI_PRINTF("%10s|%10s\n", "1", (pwr_in_status1) ? "ON" : "OFF");
    ICLI_PRINTF("%10s|%10s\n", "2", (pwr_in_status2) ? "ON" : "OFF");
  } else {
    ICLI_PRINTF("Failed to get power supply status");
  }
}

const char *color2str(vtss_appl_poe_led_t led)
{
  switch (led) {
  case VTSS_APPL_POE_LED_OFF: {
    return "Off";
  }
  case VTSS_APPL_POE_LED_GREEN: {
    return "Green";
  }
  case VTSS_APPL_POE_LED_RED: {
    return "Red";
  }
  case VTSS_APPL_POE_LED_BLINK_RED: {
    return "BlinkRed";
  }
  default:
    return "Unknown";
  }
}

/* Get the current led color of power supplies */
void poe_icli_get_power_in_led(i32 session_id)
{
  ICLI_PRINTF("%10s|%10s\n", "PowerIn", "Led");
  ICLI_PRINTF("%10s|%10s\n", "1", color2str(power1_led_color));
  ICLI_PRINTF("%10s|%10s\n", "2", color2str(power2_led_color));
}

/* Get the current led color indicating PoE status */
void poe_icli_get_status_led(i32 session_id)
{
  ICLI_PRINTF("%10s|%10s\n", "PoEStatus", "Led");
  ICLI_PRINTF("%10s|%10s\n", "1", color2str(status_led_color));
}

// Debug function for during PoE frimware upgrade for MSCC chips.
mesa_rc poe_icli_firmware_upgrade(u32 session_id, mesa_port_no_t iport, const char *url, BOOL has_built_in)
{
  ICLI_PRINTF("PoE firmware upgrade starting -- Do not power off\n");
  int tftp_err;
  mesa_rc rc = poe_do_firmware_upgrade(iport, url, tftp_err, has_built_in);
  if (rc != VTSS_RC_OK) {
    if (!has_built_in && rc != VTSS_APPL_POE_ERROR_FIRMWARE_VER_NOT_NEW) {
      ICLI_PRINTF("%% Load error: %s\n", vtss_tftp_err2str(tftp_err));
    }
  } else {
    ICLI_PRINTF("PoE firmware upgrade done -- OK to power off\n");
  }
  return rc;
}


/***************************************************************************/
/* ICFG (Show running)                                                     */
/***************************************************************************/
#ifdef VTSS_SW_OPTION_ICFG

// Function called by ICFG.
static mesa_rc poe_icfg_global_conf(const vtss_icfg_query_request_t *req,
                                    vtss_icfg_query_result_t *result)
{
  mesa_port_no_t   iport;
  poe_conf_t       poe_conf;
  int sched_index;
  int week_day;
  char *week_arr[7] = {"sun", "mon", "tue", "wed", "thu", "fri", "sat"};
  ping_alive_local_t ping_alive_local;
  sched_port_local_t sched_port_local;
  sched_time_local_t sched_time_local;

  if (!poe_is_any_chip_found()) {
    T_DG(VTSS_TRACE_GRP_ICLI, "%s", "PoE NOT supported");
    return VTSS_RC_OK;
  }
  switch (req->cmd_mode) {
  case ICLI_CMD_MODE_GLOBAL_CONFIG:
    poe_config_get(&poe_conf);

    //
    // Power management mode
    //
    if (req->all_defaults ||
        poe_conf.power_mgmt_mode != POE_MGMT_MODE_DEFAULT) {

      VTSS_RC(vtss_icfg_printf(result, "poe management mode %s\n",
                               poe_conf.power_mgmt_mode == VTSS_APPL_POE_CLASS_CONSUMP
                               ? "class-consumption" :
                               poe_conf.power_mgmt_mode == VTSS_APPL_POE_CLASS_RESERVED
                               ? "class-reserved-power" :
                               poe_conf.power_mgmt_mode == VTSS_APPL_POE_ALLOCATED_CONSUMP
                               ? "allocation-consumption" :
                               poe_conf.power_mgmt_mode == VTSS_APPL_POE_ALLOCATED_RESERVED
                               ? "allocation-reserved-power" :
                               poe_conf.power_mgmt_mode == VTSS_APPL_POE_LLDPMED_CONSUMP
                               ? "lldp-consumption" :
                               poe_conf.power_mgmt_mode == VTSS_APPL_POE_LLDPMED_RESERVED
                               ? "lldp-reserved-power" :
                               "Unknown PoE management mode"));
    }

    //
    // Power supply
    //
    if (req->all_defaults ||
        poe_conf.primary_power_supply != vtss_appl_poe_supply_default_get()) {
      VTSS_RC(vtss_icfg_printf(result, "poe supply %d\n", poe_conf.primary_power_supply));
    }

    //
    // Capacitor Detection (customization)
    //
#ifdef LNTN_SW_POE_LEGACY_PD
    if (req->all_defaults ||
        poe_conf.cap_detect != POE_CAP_DETECT_DISABLED) {

      if (poe_conf.cap_detect == POE_CAP_DETECT_DISABLED) {
        VTSS_RC(vtss_icfg_printf(result, "no poe capacitor-detect\n"));
      } else {
        VTSS_RC(vtss_icfg_printf(result, "poe capacitor-detect\n"));
      }
    }
#endif

    //
    // Schedule
    //
    sched_time_get_local_config(&sched_time_local);

    for(sched_index = 0; sched_index < POE_SCHEDULE_MAX_NUM; sched_index++) {
        if(req->all_defaults || sched_time_local.sched_time[sched_index].status != POE_SCHEDULE_STATE_DEFAULT) {
            for(week_day = 0; week_day < WEEKDAY_MAX_NUM; week_day++) {

                if(sched_time_local.sched_time[sched_index].start_hour[week_day]
                    || sched_time_local.sched_time[sched_index].start_min[week_day]
                    || sched_time_local.sched_time[sched_index].end_hour[week_day]
                    || sched_time_local.sched_time[sched_index].end_min[week_day] ) {

                VTSS_RC(vtss_icfg_printf(result, "poe schedule-id %d weekday %s start-time %02d:%02d end-time %02d:%02d\n",
                    sched_index + 1,
                    week_arr[week_day],
                    sched_time_local.sched_time[sched_index].start_hour[week_day],
                    sched_time_local.sched_time[sched_index].start_min[week_day],
                    sched_time_local.sched_time[sched_index].end_hour[week_day],
                    sched_time_local.sched_time[sched_index].end_min[week_day]));
                }

            }
        }
    }
    break;
  //
  // Interface configurations
  //
  case ICLI_CMD_MODE_INTERFACE_PORT_LIST:

    iport = uport2iport(req->instance_id.port.begin_uport);

    if (msg_switch_configurable(req->instance_id.port.isid) && (req->instance_id.port.isid == VTSS_ISID_START)) {
      CapArray<poe_chipset_t, MESA_CAP_PORT_CNT> poe_chip_found;
      poe_mgmt_is_chip_found(&poe_chip_found[0]); // Get a list with which PoE chip there is associated to the port.
      if (poe_chip_found[iport] != NO_POE_CHIPSET_FOUND) {

        // Get current configuration
        poe_config_get(&poe_conf);

        //
        // Mode
        //
        if (req->all_defaults ||
            poe_conf.poe_mode[iport] != POE_MODE_DEFAULT) {
          if (poe_conf.poe_mode[iport] == VTSS_APPL_POE_MODE_DISABLED) {
            // No command
            VTSS_RC(vtss_icfg_printf(result, " no poe mode\n"));
          } else {
            // Normal command
            VTSS_RC(vtss_icfg_printf(result, " poe mode %s\n",
                                     poe_conf.poe_mode[iport] == VTSS_APPL_POE_MODE_POE_PLUS ? "plus" :
                                     poe_conf.poe_mode[iport] == VTSS_APPL_POE_MODE_POE ? "standard" :
                                     "Unknown PoE mode"));
          }
        }

        //
        // Priority
        //
        if (req->all_defaults ||
            poe_conf.priority[iport] != POE_PRIORITY_DEFAULT) {

          // Normal command
          VTSS_RC(vtss_icfg_printf(result, " poe priority %s\n",
                                   poe_conf.priority[iport] == VTSS_APPL_POE_PORT_POWER_PRIORITY_LOW
                                   ? "low" :
                                   poe_conf.priority[iport] == VTSS_APPL_POE_PORT_POWER_PRIORITY_HIGH
                                   ? "high" :
                                   poe_conf.priority[iport] == VTSS_APPL_POE_PORT_POWER_PRIORITY_CRITICAL
                                   ? "critical" :
                                   "Unknown PoE priority"));
        }

        //
        // Maximum power
        //
        if (req->all_defaults ||
            poe_conf.max_port_power[iport] != POE_MAX_POWER_DEFAULT) {

          // We always print the value, so the users can see the default value.
          char txt_string[50];
          mgmt_long2str_float(&txt_string[0], poe_conf.max_port_power[iport], 1);
          VTSS_RC(vtss_icfg_printf(result, " poe power limit %s\n", txt_string));
        }

        //
        // Ping-alive and schedule
        //
        ping_alive_get_local_config(&ping_alive_local);
        sched_port_get_local_config(&sched_port_local);

        // State
        if(req->all_defaults || ping_alive_local.state[iport] == TRUE) {
            VTSS_RC(vtss_icfg_printf(result, " poe ping-alive\n"));
        }

        // Interval
        if(req->all_defaults || ping_alive_local.interval[iport] != PING_ALIVE_INTERVAL_DEFAULT){
            VTSS_RC(vtss_icfg_printf(result, " poe ping-alive interval %u\n", ping_alive_local.interval[iport]));
        }

        // Address
        if(req->all_defaults || ping_alive_local.address[iport] != PING_ALIVE_ADDRESS_DEFAULT){
            i8 addr[253];
            (void) icli_ipv4_to_str(ping_alive_local.address[iport], addr);
            VTSS_RC(vtss_icfg_printf(result, " poe ping-alive address %s\n", addr));
        }

        // Schedule Mode
        if(req->all_defaults || sched_port_local.mode[iport] != SCHEDULE_DISABLE){
            VTSS_RC(vtss_icfg_printf(result, " poe schedule mode %s\n",
                                        sched_port_local.mode[iport] == SCHEDULE_ON? "on":"off"));
        }

        // Schedule ID
        if(req->all_defaults || sched_port_local.sched_id[iport] != POE_SCHEDULE_ID_DEFAULT){
            VTSS_RC(vtss_icfg_printf(result, " poe port schedule-id %d\n",
                                                sched_port_local.sched_id[iport]));
        }
      }
    }
    break;
  default:
    //Not needed for PoE
    break;
  }

  return VTSS_RC_OK;
}

/* ICFG Initialization function */
mesa_rc poe_icfg_init(void)
{
  VTSS_RC(vtss_icfg_query_register(VTSS_ICFG_POE_GLOBAL_CONF, "poe", poe_icfg_global_conf));
  VTSS_RC(vtss_icfg_query_register(VTSS_ICFG_POE_PORT_CONF, "poe", poe_icfg_global_conf));
  return VTSS_RC_OK;
}

#endif // VTSS_SW_OPTION_ICFG

/****************************************************************************/
/*                              LNTN PoE                                    */
/****************************************************************************/
static void ping_alive_info(i32 session_id, const switch_iter_t *sit, BOOL debug, BOOL has_interface, icli_stack_port_range_t *plist)
{
    char            address[253];
    char            buf[250];
    port_iter_t     pit;

    memset(address, 0, sizeof(address));
    memset(buf, 0, sizeof(buf));

    // Header
    snprintf(buf, sizeof(buf), "%-23s %-11s %-25s %-16s", "Interface", "State", "IP Address", "Interval [sec]");
    icli_table_header(session_id, buf); // Print header

    ping_alive_local_t ping_alive_local;

    ping_alive_get_local_config(&ping_alive_local);

    poe_chipset_t poe_chip_found[MESA_CAP_PORT_CNT];
    poe_mgmt_is_chip_found(&poe_chip_found[0]); // Get a list with which PoE chip there is associated to the port.

    // Loop through all front ports
    if (icli_port_iter_init(&pit, sit->isid, PORT_ITER_FLAGS_FRONT | PORT_ITER_SORT_ORDER_IPORT) == VTSS_RC_OK) {
        while (icli_port_iter_getnext(&pit, plist)) {
            if (is_poe_supported(session_id, pit.iport, pit.uport, sit->usid, &poe_chip_found[0])) {
                (void) icli_port_info_txt(sit->usid, pit.uport, buf);  // Get interface as printable string
                (void) icli_ipv4_to_str(ping_alive_local.address[pit.iport], address);

                ICLI_PRINTF("%-23s %-11s %-25s %-16u \n",
                &buf[0], // interface
                ping_alive_local.state[pit.iport] ? "Enable" : "Disable",
                address,
                ping_alive_local.interval[pit.iport]);
            }
        }
    }
}

static void poe_sched_info(i32 session_id, const switch_iter_t *sit, BOOL debug, BOOL has_interface, icli_stack_port_range_t *plist)
{
    char            id[5];
    char            buf[250];
    port_iter_t     pit;
    sched_port_local_t sched_port_local;

    memset(id, 0, sizeof(id));
    memset(buf, 0, sizeof(buf));

    // Header
    snprintf(buf, sizeof(buf), "%-23s %-15s %-13s", "Interface", "Mode", "Schedule ID");
    icli_table_header(session_id, buf); // Print header

    sched_port_get_local_config(&sched_port_local);

    poe_chipset_t poe_chip_found[MESA_CAP_PORT_CNT];
    poe_mgmt_is_chip_found(&poe_chip_found[0]); // Get a list with which PoE chip there is associated to the port.

    // Loop through all front ports
    if (icli_port_iter_init(&pit, sit->isid, PORT_ITER_FLAGS_FRONT | PORT_ITER_SORT_ORDER_IPORT) == VTSS_RC_OK) {
        while (icli_port_iter_getnext(&pit, plist)) {
            if (is_poe_supported(session_id, pit.iport, pit.uport, sit->usid, &poe_chip_found[0])) {
                (void) icli_port_info_txt(sit->usid, pit.uport, buf);  // Get interface as printable string

                snprintf(id, sizeof(id), "%d", sched_port_local.sched_id[pit.iport]);

                ICLI_PRINTF("%-23s %-15s %-13s \n",
                &buf[0], // interface
                sched_port_local.mode[pit.iport] == 0 ? "Disable" :
                sched_port_local.mode[pit.iport] == 1 ? "Schedule On" : "Schedule Off",
                sched_port_local.sched_id[pit.iport] == 0 ? "" : id);
            }
        }
    }
}

static void ping_alive_state_conf(const ping_alive_value_t *ping_alive_value, mesa_port_no_t iport, ping_alive_local_t *ping_alive_local)
{
    BOOL changed;

    changed = abs(ping_alive_value->state - ping_alive_local->state[iport]) ? TRUE : FALSE;
    ping_alive_local->changed[iport] = changed;
    ping_alive_local->state[iport]   = ping_alive_value->state;
}

static void ping_alive_interval_conf(const ping_alive_value_t *ping_alive_value, mesa_port_no_t iport, ping_alive_local_t *ping_alive_local)
{
    BOOL changed;

    changed = abs(ping_alive_value->interval - ping_alive_local->interval[iport]) ? TRUE : FALSE;
    ping_alive_local->changed[iport]  = changed;
    ping_alive_local->interval[iport] = ping_alive_value->interval;
}

static void ping_alive_address_conf(const ping_alive_value_t *ping_alive_value, mesa_port_no_t iport, ping_alive_local_t *ping_alive_local)
{
    ping_alive_local->address[iport] = ping_alive_value->address;
}

static void ping_alive_icli_common(i32 session_id, icli_stack_port_range_t *plist,
                            const ping_alive_value_t *ping_alive_value,
                            lntn_poe_type_t lntn_poe_type)
{
    u8 isid_cnt;
    ping_alive_local_t ping_alive_local;

    // Just making sure that we don't access NULL pointer.
    if (plist == NULL) {
        T_E("plist was unexpected NULL");
        return;
    }

    for (isid_cnt = 0; isid_cnt < plist->cnt; isid_cnt++) {
        // Get current configuration
        ping_alive_get_local_config(&ping_alive_local);

        poe_chipset_t poe_chip_found[MESA_CAP_PORT_CNT];
        poe_mgmt_is_chip_found(&poe_chip_found[0]); // Get a list with which PoE chip there is associated to the port.

        port_iter_t        pit;
        if (icli_port_iter_init(&pit, plist->switch_range[isid_cnt].isid, PORT_ITER_FLAGS_FRONT | PORT_ITER_SORT_ORDER_IPORT) == VTSS_RC_OK) {
            T_DG(VTSS_TRACE_GRP_ICLI, "isid:%d", plist->switch_range[isid_cnt].isid);
            while (icli_port_iter_getnext(&pit, plist)) {
                T_DG_PORT(VTSS_TRACE_GRP_ICLI, pit.iport, "ping_alive_conf_type:%d", lntn_poe_type);
                // Ignore if PoE isn't supported for this port.
                if (is_poe_supported(session_id, pit.iport, pit.uport, plist->switch_range[isid_cnt].usid, &poe_chip_found[0])) {
                    switch  (lntn_poe_type) {
                        case PING_ALIVE_STATE:
                            ping_alive_state_conf(ping_alive_value, pit.iport, &ping_alive_local);
                            break;
                        case PING_ALIVE_INTERVAL:
                            ping_alive_interval_conf(ping_alive_value, pit.iport, &ping_alive_local);
                            break;
                        case PING_ALIVE_ADDRESS:
                            ping_alive_address_conf(ping_alive_value, pit.iport, &ping_alive_local);
                            break;
                        default:
                            T_E("Unknown lntn_poe_type:%d", lntn_poe_type);
                    }
                }
            }
            // Set new configuration
            ping_alive_set_local_config(&ping_alive_local);
        }
    }
}

static void schedule_port_icli_common(i32 session_id, icli_stack_port_range_t *plist,
                            const poe_sched_value_t *poe_sched_value,
                            lntn_poe_type_t lntn_poe_type)
{
    sched_port_local_t sched_port_local;
    u8 isid_cnt;

    // Just making sure that we don't access NULL pointer.
    if (plist == NULL) {
        T_E("plist was unexpected NULL");
        return;
    }

    for (isid_cnt = 0; isid_cnt < plist->cnt; isid_cnt++) {
        // Get current configuration
        sched_port_get_local_config(&sched_port_local);

        poe_chipset_t poe_chip_found[MESA_CAP_PORT_CNT];
        poe_mgmt_is_chip_found(&poe_chip_found[0]); // Get a list with which PoE chip there is associated to the port.

        port_iter_t        pit;
        if (icli_port_iter_init(&pit, plist->switch_range[isid_cnt].isid, PORT_ITER_FLAGS_FRONT | PORT_ITER_SORT_ORDER_IPORT) == VTSS_RC_OK) {
            T_DG(VTSS_TRACE_GRP_ICLI, "isid:%d", plist->switch_range[isid_cnt].isid);
            while (icli_port_iter_getnext(&pit, plist)) {
                T_DG_PORT(VTSS_TRACE_GRP_ICLI, pit.iport, "poe_sched_type:%d", lntn_poe_type);
                // Ignore if PoE isn't supported for this port.
                if (is_poe_supported(session_id, pit.iport, pit.uport, plist->switch_range[isid_cnt].usid, &poe_chip_found[0])) {
                    switch  (lntn_poe_type) {
                        case POE_SCHED_ID:
                            sched_port_local.sched_id[pit.iport] = poe_sched_value->sched_id;
                            break;
                        case POE_SCHED_MODE:
                            sched_port_local.mode[pit.iport] = poe_sched_value->mode;
                            break;
                        default:
                            T_E("Unknown poe_sched_type:%d", lntn_poe_type);
                    }
                }
            }
            // Set new configuration
            sched_port_set_local_config(&sched_port_local);
        }
    }
}

void ping_alive_icli_state(i32 session_id, icli_stack_port_range_t *plist, BOOL has_enable)
{
    ping_alive_value_t ping_alive_value;
    ping_alive_value.state = has_enable;

    T_DG(VTSS_TRACE_GRP_ICLI, "Ping alive state: %d", has_enable);
    ping_alive_icli_common(session_id, plist, &ping_alive_value, PING_ALIVE_STATE);
}

void ping_alive_icli_interval(i32 session_id, icli_stack_port_range_t *plist, u32 interval)
{
    ping_alive_value_t ping_alive_value;
    ping_alive_value.interval = interval;

    T_DG(VTSS_TRACE_GRP_ICLI, "Ping alive interval: %d", interval);
    ping_alive_icli_common(session_id, plist, &ping_alive_value, PING_ALIVE_INTERVAL);
}

void ping_alive_icli_address(i32 session_id, icli_stack_port_range_t *plist, mesa_ipv4_t address)
{
    char icmp_dest[253];
    ping_alive_value_t ping_alive_value;
    ping_alive_value.address = address;

    memset(icmp_dest, 0, sizeof(icmp_dest));

    (void) icli_ipv4_to_str(address, icmp_dest);

    T_DG(VTSS_TRACE_GRP_ICLI, "Ping alive address: %s", icmp_dest);
    ping_alive_icli_common(session_id, plist, &ping_alive_value, PING_ALIVE_ADDRESS);
}

void ping_alive_icli_show(i32 session_id, BOOL has_interface, icli_stack_port_range_t *list)
{
    switch_iter_t sit;

    if (icli_switch_iter_init(&sit) != VTSS_RC_OK) {
        return;
    }

    while (icli_switch_iter_getnext(&sit, list)) {
        if (has_interface || msg_switch_exists(sit.isid)) {
            ping_alive_info(session_id, &sit, FALSE, has_interface, list);
        }
    }
}

void poe_schedule_icli_id(i32 session_id, icli_stack_port_range_t *plist, u8 sched_id)
{
    poe_sched_value_t poe_sched_value;
    poe_sched_value.sched_id = sched_id;

    T_DG(VTSS_TRACE_GRP_ICLI, "PoE schedule id: %d", sched_id);
    schedule_port_icli_common(session_id, plist, &poe_sched_value, POE_SCHED_ID);
}

void poe_schedule_icli_mode(i32 session_id, icli_stack_port_range_t *plist, BOOL has_on, BOOL has_off)
{
    poe_sched_value_t poe_sched_value;

    if (has_on) {
        poe_sched_value.mode = SCHEDULE_ON;
    } else if (has_off) {
        poe_sched_value.mode = SCHEDULE_OFF;
    } else {
        poe_sched_value.mode = SCHEDULE_DISABLE;
    }

    T_DG(VTSS_TRACE_GRP_ICLI, "has_on: %d, has_off:%d", has_on, has_off);
    schedule_port_icli_common(session_id, plist, &poe_sched_value, POE_SCHED_MODE);
}

void poe_schedule_icli_time(i32 session_id, u8 sched_id, u8 week_list, icli_time_t start_time, icli_time_t end_time, BOOL no)
{
    u8 day;
     sched_time_local_t sched_time_local;

    // 20180207 Harvey add, Non-PoE model returns error message
    if (!is_poe_feature(session_id)) {
        return;
    }

    sched_time_get_local_config(&sched_time_local);

    if (no) { //no == TRUE: mean reset to default
        memset(&(sched_time_local.sched_time[sched_id - 1]), 0, sizeof(sched_time_t));
    } else {
        sched_time_local.sched_time[sched_id - 1].status = 1; // Active

        for (day = 0; day < WEEKDAY_MAX_NUM; day++) {
            if(week_list & VTSS_BIT(day)) {
                sched_time_local.sched_time[sched_id - 1].start_hour[day] = start_time.hour;
                sched_time_local.sched_time[sched_id - 1].start_min[day]  = start_time.min;
                sched_time_local.sched_time[sched_id - 1].end_hour[day]   = end_time.hour;
                sched_time_local.sched_time[sched_id - 1].end_min[day]    = end_time.min;
            }
        }
    }

    T_DG(VTSS_TRACE_GRP_ICLI, "Start Time %d:%d, End Time %d:%d", start_time.hour, start_time.min, end_time.hour, end_time.min);

    // Set new configuration
    sched_time_set_local_config(&sched_time_local);
}

void poe_schedule_icli_show(i32 session_id, BOOL has_interface, icli_stack_port_range_t *list)
{
    switch_iter_t sit;

    if (icli_switch_iter_init(&sit) != VTSS_RC_OK) {
        return;
    }

    while (icli_switch_iter_getnext(&sit, list)) {
        if (has_interface || msg_switch_exists(sit.isid)) {
            poe_sched_info(session_id, &sit, FALSE, has_interface, list);
        }
    }
}

void poe_schedule_id_icli_show(i32 session_id, u8 sched_id)
{
    int   day, index;
    char* weeks[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    sched_time_local_t sched_time_local;

    sched_time_get_local_config(&sched_time_local);

    if (sched_id != 0) {
        if (sched_time_local.sched_time[sched_id - 1].status == 1) {
            ICLI_PRINTF("\nSchedule ID:  %-3u \n", sched_id);
            ICLI_PRINTF("Status:  Active\n");
            ICLI_PRINTF("Weekday       StartTime    EndTime \r\n");
            ICLI_PRINTF("------------  -----------  --------\r\n");
            for (day = 0; day < WEEKDAY_MAX_NUM; day++) {
                ICLI_PRINTF("%-14s",weeks[day]);
                ICLI_PRINTF("%02u:%02u",sched_time_local.sched_time[sched_id - 1].start_hour[day]
                                        ,sched_time_local.sched_time[sched_id - 1].start_min[day]);
                ICLI_PRINTF("        %02u:%02u\r\n",sched_time_local.sched_time[sched_id - 1].end_hour[day]
                                        ,sched_time_local.sched_time[sched_id - 1].end_min[day]);
            }
            ICLI_PRINTF("\n");
        } else {
            ICLI_PRINTF("Schedule ID %u have yet to configure.\n", sched_id);
        }
    } else {
        //Show all active schedule
        for (index = 0; index < POE_SCHEDULE_MAX_NUM; index++) {
            if (sched_time_local.sched_time[index].status == 1) {
                ICLI_PRINTF("\nSchedule ID:  %-3u \n", index+1);
                ICLI_PRINTF("Status:  Active\n");
                ICLI_PRINTF("Weekday       StartTime    EndTime \r\n");
                ICLI_PRINTF("------------  -----------  --------\r\n");
                for (day = 0; day < WEEKDAY_MAX_NUM; day++) {
                    ICLI_PRINTF("%-14s",weeks[day]);
                    ICLI_PRINTF("%02u:%02u",sched_time_local.sched_time[index].start_hour[day]
                                            ,sched_time_local.sched_time[index].start_min[day]);
                    ICLI_PRINTF("        %02u:%02u\r\n",sched_time_local.sched_time[index].end_hour[day]
                                            ,sched_time_local.sched_time[index].end_min[day]);
                }
                ICLI_PRINTF("\n");
            }
        }
    }
}

#endif //VTSS_SW_OPTION_POE
