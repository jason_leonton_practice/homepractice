/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
/****************************************************************************/
// PoE chip LTC4271 from SilliconLabs. We having got a real data sheet at the time of writing,
// so all refernces are done to the word docment we have got from SilliconLabs.
// The docuemnt can be found at L:\R_D\Components\Silicon Laboratories\ltc4271 e
/****************************************************************************/

#include "critd_api.h"
#include "poe.h"
#include "poe_custom_api.h"
#include "misc_api.h"
#include "poe_custom_ltc4271_api.h"
#include "poe_trace.h"
#include "conf_api.h"

//
// Define the LTC4271 register map.
//
const uchar LTC4271_REG_STATPWR     = (0x10);
const uchar LTC4271_REG_OPMOD       = (0x12);
const uchar LTC4271_REG_DETENA      = (0x14);
const uchar LTC4271_REG_MCONF       = (0x17);
const uchar LTC4271_REG_DETPB       = (0x18);
const uchar LTC4271_REG_PWRPB       = (0x19);
const uchar LTC4271_REG_RSTPB       = (0x1A);
const uchar LTC4271_REG_DEVID       = (0x1B);
const uchar LTC4271_REG_VEELSB      = (0x2E);
const uchar LTC4271_REG_VEEMSB      = (0x2F);

const uchar LTC4271_REG_STATP_BASE  = (0x0C);
#define LTC4271_REG_STATP(x) (x + LTC4271_REG_STATP_BASE)

const uchar LTC4271_REG_CUT_BASE  = (0x47);
#define LTC4271_REG_CUT(x) ((x * 5) + LTC4271_REG_CUT_BASE)

const uchar LTC4271_REG_HPMD_BASE  = (0x46);
#define LTC4271_REG_HPMD(x) ((x * 5) + LTC4271_REG_HPMD_BASE)

const uchar LTC4271_REG_IP_LSB_BASE  = (0x30);
#define LTC4271_REG_IP_LSB(x) ((x * 4) + LTC4271_REG_IP_LSB_BASE)
const uchar LTC4271_REG_IP_MSB_BASE  = (0x31);
#define LTC4271_REG_IP_MSB(x) ((x * 4) + LTC4271_REG_IP_MSB_BASE)

const uchar LTC4271_DEVICE_ID       = (0x64);

#define LTC4271_READ_VEE_TOLERANCE                  (600)   // 600mV
#define LTC4271_READ_CURRENT_TOLERANCE_STAGE1       (17)    // 17mA
#define LTC4271_READ_CURRENT_TOLERANCE_STAGE2       (21)    // 21mA
#define LTC4271_READ_CURRENT_TOLERANCE_STAGE3       (25)    // 25mA

static CapArray<poe_chip_found_t, MESA_CAP_PORT_CNT> ltc4271_chip_found;

static CapArray<vtss_i2c_adp_t, MESA_CAP_PORT_CNT> ltc4271_i2c_dev;

static int g_vee = 0;

// Open i2c driver (if not already open)
static mesa_rc open_i2c_dev(mesa_port_no_t port_index)
{
    static mesa_port_list_t already_open;

    T_R("Entering");
    if (already_open[port_index]) {
        return VTSS_RC_OK;
    }

    already_open[port_index] = TRUE;

    poe_custom_entry_t poe_hw_conf;
    poe_hw_conf = poe_custom_get_hw_config(port_index, &poe_hw_conf); // Any port will do, since the i2c device is the same for all ports

    ltc4271_i2c_dev[port_index].i2c_bus = vtss_i2c_dev_open(poe_hw_conf.i2c_device_name[LTC4271], NULL, & ltc4271_i2c_dev[port_index].i2c_addr);
    if ( ltc4271_i2c_dev[port_index].i2c_bus == -1) {
        return VTSS_RC_ERROR;
    }
    return VTSS_RC_OK;
}

typedef struct {
    int i2c_bus;
    int i2c_addr;
    int curr_mux_id;
} PCA9544_MUX_t;

static void ltc4271_switch_poe_mux(mesa_port_no_t port_index)
{
    static PCA9544_MUX_t mux = {
        .i2c_bus = -1,
        .i2c_addr = -1,
        .curr_mux_id = -1
    };

    lntn_global_conf_t lntn_conf = lntn_conf_get();
    lntn_port_conf_t *port_conf = &lntn_conf.port_conf[port_index];
    u8 retry_cnt;

    T_DG(VTSS_TRACE_GRP_CUSTOM, "Switch port[%d] mux id is %d", port_index, port_conf->poe.mux_id);

    // open i2c device once
    if(mux.i2c_bus == -1) {
        mux.i2c_bus = vtss_i2c_dev_open("pca9544_0", NULL, &mux.i2c_addr);
        if (mux.i2c_bus == -1) {
            T_WG(VTSS_TRACE_GRP_CUSTOM, "Open I2C device 'pca9544_0' failed");
            return;
        } else {
            T_IG(VTSS_TRACE_GRP_CUSTOM, "Got pca9544 i2c_bus[%d]", mux.i2c_bus);
        }
    }

    // already switch the same channel
    if(mux.curr_mux_id == port_conf->poe.mux_id)
        return;

    for (retry_cnt = 1; retry_cnt <= I2C_RETRIES_MAX; retry_cnt++) {
        // refer to PCA9544 datasheet, the value [4..7] can switch channel [0..3]
        uchar buf = (port_conf->poe.mux_id == 0)? 5: 6;

        // If no bytes were transmitted we need to do redetection of the PoE chipset
        if (vtss_i2c_dev_wr(mux.i2c_bus, &buf, 1) != VTSS_RC_OK) {
            if (retry_cnt == I2C_RETRIES_MAX) {
                T_WG(VTSS_TRACE_GRP_CUSTOM, "I2C TX problem, PCA9544 cannot access");
            } else {
                T_DG(VTSS_TRACE_GRP_CUSTOM, "TX retry cnt = %d", retry_cnt);
            }
        } else {
            // keep current mux id to avoid the multiplex would be switched frequently
            mux.curr_mux_id = port_conf->poe.mux_id;
            break; // OK - Access went well. No need to re-send.
        }
    }

    VTSS_OS_MSLEEP(10);
}

// Do the i2c write.
void ltc4271_device_wr(mesa_port_no_t port_index, uchar reg_addr, char data)
{
    u8 retry_cnt;
    // Get PoE hardware board configuration
    poe_custom_entry_t poe_hw_conf;
    poe_hw_conf = poe_custom_get_hw_config(port_index, &poe_hw_conf);

    // switch mux by i2c
    ltc4271_switch_poe_mux(port_index);

    if (open_i2c_dev(port_index) != VTSS_RC_OK) {
        T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "%s", "i2c not open");
        return;
    }

    if (poe_hw_conf.available) {
        for (retry_cnt = 1; retry_cnt <= I2C_RETRIES_MAX; retry_cnt++) {
            uchar buf[2];
            buf[0] = reg_addr;
            buf[1] = data;

            // If no bytes were transmitted we need to do redetection of the PoE chipset
            if (vtss_i2c_dev_wr(ltc4271_i2c_dev[port_index].i2c_bus, &buf[0], 2) != VTSS_RC_OK) {
                if (retry_cnt == I2C_RETRIES_MAX) {
                    T_WG(VTSS_TRACE_GRP_CUSTOM, "I2C TX problem, Re-detecting PoE Chip set");
                    ltc4271_chip_found[port_index] = DETECTION_NEEDED;
                } else {
                    T_DG(VTSS_TRACE_GRP_CUSTOM, "TX retry cnt = %d", retry_cnt);
                }
            } else {
                break; // OK - Access went well. No need to re-send.
            }
        }
    }
    T_R("Write Exit");
}

// Do the i2c read, the result is passed by pointer.
void ltc4271_device_rd(mesa_port_no_t port_index, uchar reg_addr, uchar *data)
{
    u8 retry_cnt;
    // Get PoE hardware board configuration
    poe_custom_entry_t poe_hw_conf;
    poe_hw_conf = poe_custom_get_hw_config(port_index, &poe_hw_conf);

    // switch mux by i2c
    ltc4271_switch_poe_mux(port_index);

    if (open_i2c_dev(port_index) != VTSS_RC_OK) {
        T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "%s", "i2c not open");
        return;
    }

    if (poe_hw_conf.available) {
        for (retry_cnt = 1; retry_cnt <= I2C_RETRIES_MAX; retry_cnt++) {
            // If no bytes were received we need to do re-detection of the PoE chipset
            if (vtss_i2c_dev_wr_rd(ltc4271_i2c_dev[port_index].i2c_bus, ltc4271_i2c_dev[port_index].i2c_addr, &reg_addr, 1, data, 1) == VTSS_RC_OK) {
                break; // OK - Data was read correctly
            } else {
                if (retry_cnt == I2C_RETRIES_MAX) {
                    ltc4271_chip_found[port_index] = DETECTION_NEEDED;
                    T_WG(VTSS_TRACE_GRP_CUSTOM, "I2C RX problem, Re-detecting PoE Chip set");
                    break;
                } else {
                    T_DG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "RX retry cnt = %d", retry_cnt);
                    continue; // Access failed - retry.
                }
            }
        }
    }
    T_R("Read Exit");
}


// Returns the Vee voltage in mV
void ltc4271_update_vee(mesa_port_no_t port_index)
{
    int   vee = 0;
    uchar vee_lsb, vee_msb;

    T_RG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "Entering ltc4271_update_vee");

    // Read the vee.
    ltc4271_device_rd(port_index, LTC4271_REG_VEELSB, &vee_lsb);
    ltc4271_device_rd(port_index, LTC4271_REG_VEEMSB, &vee_msb);

    vee = (((vee_msb & 0x3F) << 8) + vee_lsb) * 5.835;  // Vee in mV units - see table 3:"device command summary"
    vee -= LTC4271_READ_VEE_TOLERANCE;  // Reserved tolance

    // Avoid frequent updates to vee.
    if (g_vee != 0 && (abs(g_vee - vee) < 500)) {   // 500 mV
        T_DG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "g_vee = %u", g_vee);
        return;
    }

    g_vee = vee;

    T_DG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "port_index = %d, vee_msb = %d, vee_lsb=%d, vee = %u", port_index, vee_msb, vee_lsb, vee);
}


// read the port status
char ltc4271_read_port_status (mesa_port_no_t port_index)
{
    lntn_global_conf_t lntn_conf = lntn_conf_get();
    lntn_port_conf_t *port_conf = &lntn_conf.port_conf[port_index];
    uchar reg_value = 0;

    T_NG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "Entering ltc4271_read_port_status");
    ltc4271_device_rd(port_index, LTC4271_REG_STATP(port_conf->poe.chip_port), &reg_value);
    return reg_value;
}

// read the port powered status
char ltc4271_read_port_powered_status (vtss_port_no_t port_index)
{
    lntn_global_conf_t lntn_conf = lntn_conf_get();
    lntn_port_conf_t *port_conf = &lntn_conf.port_conf[port_index];
    uchar reg_value = 0;
    T_RG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "Entering ltc4271_read_port_powered_status");

    ltc4271_device_rd(port_index, LTC4271_REG_STATPWR, &reg_value);
    return (((reg_value >> port_conf->poe.chip_port) & 0x11 ) == 0x11)?1:0;
}

// Get status for all ports
void ltc4271_port_status_get(vtss_appl_poe_status_type_t *port_status, mesa_port_no_t port_index)
{
    char pwr_enable_status;
    char detection;
    char port_status_reg;
    poe_custom_entry_t poe_hw_conf;

    //Detection result
    port_status_reg = ltc4271_read_port_status(port_index);

    //power state

    // Extract status - See Table 1:ltc4271 register map.
    pwr_enable_status = ltc4271_read_port_powered_status(port_index);
    detection   = port_status_reg & 0x7;

    // Get PoE hardware board configuration
    poe_hw_conf = poe_custom_get_hw_config(port_index, &poe_hw_conf);


    T_DG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "status = 0x%X, detection = 0x%X", port_status_reg, detection);
    if (poe_hw_conf.available == false) {
        // PoE chip not mounted for this port
        *port_status = VTSS_APPL_POE_NOT_SUPPORTED;
    } else if (detection == 4) { // 4 = Detection Good - See table 2 in documentation
        if (pwr_enable_status) {
            T_NG(VTSS_TRACE_GRP_CUSTOM, "Setting port status to PD_ON Port_index = %u", port_index);
            *port_status = VTSS_APPL_POE_PD_ON;
        } else {
            T_NG(VTSS_TRACE_GRP_CUSTOM, "Setting port status to PD_OFF Port_index = %u", port_index);
            *port_status = VTSS_APPL_POE_PD_OFF;
        }
    } else {
        T_DG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "Setting port status to NO_PD_DETECTED");
        // See Table 2 register defintions.
        *port_status = VTSS_APPL_POE_NO_PD_DETECTED;
    }
}


// Function for setting the max power for a port.
void ltc4271_set_power_limit_channel(mesa_port_no_t port_index, int max_port_power)
{
    lntn_global_conf_t lntn_conf = lntn_conf_get();
    lntn_port_conf_t *port_conf = &lntn_conf.port_conf[port_index];
    int vee = g_vee / 1000 ;  // Convert to Volt

    char icut;
    if (vee == 0) {
        icut = 0; // Avoid divide by zero.
    } else {
        /*
         *  @param: max_port_power(1 = 0.1W)
         *  @param: vee(1 = 1V)
         *  @param: icut(1 = 18.75mA)
         *  P = max_port_power / 10
         *  I = icut * 18.75 / 1000
         *  V = vee
         *
         *  I(A) = P(W) / V(V)
         *  icut * 18.75 / 1000 = (max_port_power / 10) / vee
         *  icut = (1000 / 18.75) * (max_port_power / 10) / vee
         *       = (1000 / 18.75 / 10) * max_port_power / vee
         *       = 5.333 * max_port_power / vee
         */
        icut = (5.333 * max_port_power) / vee ;

        /* Provide enough current */
        icut += 3;
    }
    icut |= 0xC0;  // RSENSE = 0.25Ω, cutrng=1, 1 LSB = 18.75mA

    ltc4271_device_wr(port_index, LTC4271_REG_CUT(port_conf->poe.chip_port), icut);

    T_DG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "vee = %d, max_port_power = %d,icut set to 0x%x", vee, max_port_power, icut);
}

// Determine the class for each port. Passed back via pointer
void ltc4271_get_all_port_class(char *classes, mesa_port_no_t port_index)
{
    int class_reg = ltc4271_read_port_status(port_index);
    // See ltc4271 documentation Table 1.
    classes[port_index] = (class_reg >> 4) & 0x7;

    T_NG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "classes[port_index] = %d, class_reg = 0x%X", classes[port_index], class_reg);
}

// Updates the poe status with current and power consumption
void ltc4271_get_port_measurement(poe_status_t *poe_status, mesa_port_no_t port_index)
{
    lntn_global_conf_t lntn_conf = lntn_conf_get();
    lntn_port_conf_t *port_conf = &lntn_conf.port_conf[port_index];
    uchar current_used_lsb = 0, current_used_msb = 0;
    int power_used;
    int current_used = 0;
    int vee;
    uchar powered_status;

    T_NG(VTSS_TRACE_GRP_CUSTOM, "Entering ltc4271_get_port_measurement");

    powered_status = ltc4271_read_port_powered_status(port_index);
    T_RG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "ltc4271_poe_port_ok = %d", powered_status);

    if (powered_status) {
        ltc4271_device_rd(port_index, LTC4271_REG_IP_LSB(port_conf->poe.chip_port), &current_used_lsb);
        ltc4271_device_rd(port_index, LTC4271_REG_IP_MSB(port_conf->poe.chip_port), &current_used_msb);

        current_used = (((current_used_msb & 0x3F) << 8) + current_used_lsb) * 0.122;  // 1 LSB = 122.07μA

        /* According to the experimental results,
         * the tolerance of the LTC4271 chip read current needs to be compensated.
         */
        if (current_used <= 250) {                                  // 250mA
            current_used = (current_used < LTC4271_READ_CURRENT_TOLERANCE_STAGE1) ?
                            0 : (current_used - LTC4271_READ_CURRENT_TOLERANCE_STAGE1);
        } else if (current_used > 250 && current_used <= 480) {     // 480mA
            current_used -= LTC4271_READ_CURRENT_TOLERANCE_STAGE2;
        } else {
            current_used -= LTC4271_READ_CURRENT_TOLERANCE_STAGE3;
        }

        // Get the vee value ( in milli-volt )
        vee = g_vee / 1000 ;  // Convert to Volt
        T_DG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "current_msb = %d, current_lsb=%d, current_used = %u, vee = %d",
                  current_used_msb, current_used_lsb, current_used, vee);
        /*
         *  @param: power_used(1 = 0.1W)
         *  @param: vee(1 = 1V)
         *  @param: current_used(1 = 1mA)
         *  P = power_used / 10
         *  I = current_used / 1000
         *  V = vee
         *
         *  P(W) = I(A) * V(V)
         *  power_used / 10 = (current_used / 1000) * vee
         *                  = current_used * vee / 1000
         *  power_used = current_used * vee / 100
         */
        power_used = (vee * current_used / 100);    // DeciWatts
        T_DG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "power_used = %d", power_used);
    } else {
        vee = 0;
        current_used = 0;
        power_used = 0;
    }

    // Update the status structure
    poe_status -> power_used[port_index]   = power_used;
    poe_status -> current_used[port_index] = current_used;

    if (current_used > 1000) {
        T_DG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "power_used = %u, current_used = %u, vee = %d",
                  power_used, poe_status -> current_used[port_index], vee);
    }
}

// Set port mode ( Manual / Semiauto or auto )
static void ltc4271_set_port_mode (mesa_port_no_t port_index, char mode)
{
    //mode: 0=Shutdown; 1=Manual; 2=Semi-auto; 3=AUTO pin
    lntn_global_conf_t lntn_conf = lntn_conf_get();
    lntn_port_conf_t *port_conf = &lntn_conf.port_conf[port_index];
    uchar reg_value;

    ltc4271_device_rd(port_index, LTC4271_REG_OPMOD, &reg_value);
    reg_value &= ~(0x3 << (2 * port_conf->poe.chip_port));
    reg_value |= mode << (2 * port_conf->poe.chip_port);
    ltc4271_device_wr(port_index, LTC4271_REG_OPMOD, reg_value);

    if (mode == 0x0) {
        ltc4271_device_wr(port_index, LTC4271_REG_PWRPB, 0x10 << port_conf->poe.chip_port);
    } else {
        ltc4271_device_rd(port_index, LTC4271_REG_DETENA, &reg_value);
        reg_value |= 0x11 << port_conf->poe.chip_port;
        ltc4271_device_wr(port_index, LTC4271_REG_DETENA, reg_value);
        ltc4271_device_wr(port_index, LTC4271_REG_DETPB, 0x1 << port_conf->poe.chip_port);
    }
}

// Things that is needed after a reset
void ltc4271_poe_init(mesa_port_no_t port_index)
{
    T_NG(VTSS_TRACE_GRP_CUSTOM, "Entering ltc4271_poe_init");
    ltc4271_set_port_mode(port_index, 3); // Set to auto mode
}

// Enables or disable poe for a port
void ltc4271_poe_enable(mesa_port_no_t port_index, BOOL enable)
{
    // The enabling / disabling of the PoE ports s´dones't work due to some SillionLabs bugs.
    // This is a work around for now
    if (enable) {
        ltc4271_set_port_mode(port_index, 3); // Set mode to auto
        T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "Enable");
    } else {
        ltc4271_set_port_mode(port_index, 0); // Set mode to shutdown
        VTSS_MSLEEP(50);
        ltc4271_set_port_mode(port_index, 2); // Set mode to semi-auto
        T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "Disable");
    }
}

// Function that returns 1 is a PoE chip set is found, else 0.
int ltc4271_is_chip_available(mesa_port_no_t port_index)
{
    uchar hw_rev  = 0;
    static BOOL init_done = FALSE;
    int i;

    // Initialize chip found array.
    if (!init_done) {
        for (i = 0 ; i < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); i++) {
            ltc4271_chip_found[i] = DETECTION_NEEDED;
        }
        init_done = TRUE;
    }

    // We only want to try to detect the ltc4271 chipset once.
    if (ltc4271_chip_found[port_index] == DETECTION_NEEDED) {
        // We detects the chipset by reading the hardware revison register.
        ltc4271_device_rd(port_index, LTC4271_REG_DEVID, &hw_rev);

        if (hw_rev == LTC4271_DEVICE_ID ) {
            ltc4271_chip_found[port_index] = FOUND;
            T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "PoE ltc4271 chipset found, hw_rev = 0x%X", hw_rev);
        } else {
            ltc4271_chip_found[port_index] = NOT_FOUND;
            T_IG_PORT(VTSS_TRACE_GRP_CUSTOM, port_index, "PoE ltc4271 chipset NOT found, hw_rev = 0x%X", hw_rev);
        }
    }

    if (ltc4271_chip_found[port_index] == FOUND) {
        return 1;
    } else {
        return 0;
    }
}

static void ltc4271_set_legacy_mode(vtss_port_no_t port_index, BOOL enable)
{
    lntn_global_conf_t lntn_conf = lntn_conf_get();
    lntn_port_conf_t *port_conf = &lntn_conf.port_conf[port_index];
    uchar reg_value = 0;
    ltc4271_device_rd(port_index, LTC4271_REG_HPMD(port_conf->poe.chip_port), &reg_value);

    if (enable) {
        reg_value |= 0x2;
    } else {
        reg_value &= ~(0x2);
    }

    ltc4271_device_wr(port_index, LTC4271_REG_HPMD(port_conf->poe.chip_port), reg_value);
}

static void ltc4271_get_legacy_mode(vtss_port_no_t port_index, BOOL *enable)
{
    lntn_global_conf_t lntn_conf = lntn_conf_get();
    lntn_port_conf_t *port_conf = &lntn_conf.port_conf[port_index];
    uchar reg_value = 0;

    ltc4271_device_rd(port_index, LTC4271_REG_HPMD(port_conf->poe.chip_port), &reg_value);

    *enable = (reg_value & 0x2) ? true : false;
}

void ltc4271_capacitor_detection_set(BOOL enable)
{
    vtss_port_no_t port_index;

    for (port_index = 0 ; port_index < MESA_CAP(VTSS_APPL_CAP_POE_PORT_CNT); port_index++) {
        ltc4271_set_legacy_mode(port_index, enable);
    }
}

BOOL ltc4271_capacitor_detection_get()
{
    BOOL enable;

    ltc4271_get_legacy_mode(0, &enable);

    return enable;
}

// For mptest use
void ltc4271_pse_interrupt_enable(vtss_port_no_t port_index, BOOL enable)
{
    uchar reg_value = 0;

    ltc4271_device_rd(port_index, LTC4271_REG_MCONF, &reg_value);

    if (enable) {
        reg_value |= 0x80;
    } else {
        reg_value &= ~(0x80);
    }

    ltc4271_device_wr(port_index, LTC4271_REG_MCONF, reg_value);
}

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
