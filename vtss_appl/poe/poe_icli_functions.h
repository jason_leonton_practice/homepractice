/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

#ifndef _VTSS_ICLI_POE_H_
#define _VTSS_ICLI_POE_H_

#include "icli_api.h" // for icli_stack_port_range_t

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \file
 * \brief PoE iCLI functions
 * \details This header file describes PoE iCLI functions
 */



/**
 * \brief Function for displaying PoE status
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param has_interface [IN] TRUE if the user want to display a specific interface (port)
 * \param list [IN] port list of which interfaces to display PoE status.
 * \return None.
 **/
void poe_icli_show(i32 session_id, BOOL has_interface, icli_stack_port_range_t *list);


/**
 * \brief Function for configuring PoE mode
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param has_poe [IN] TRUE if the user want to set PoE to PoE mode (15.4W).
 * \param has_poe_plus [IN] TRUE if the user want to set PoE to PoE mode (30W).
 * \param plist [IN] List of interfaces to configure.
 * \param no [IN] TRUE is user want to set mode to default value
 * \return VTSS_RC_OK if mode was set correctly, else error code.
 **/
mesa_rc poe_icli_mode(i32 session_id, BOOL has_poe, BOOL has_poe_plus, icli_stack_port_range_t *plist, BOOL no);

/**
 * \brief Function for configuring PoE priority
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param has_low [IN] TRUE if the user want to priority to low.
 * \param has_high [IN] TRUE if the user want to priority to high.
 * \param has_critical [IN] TRUE if the user want to priority to critical.
 * \param plist [IN] List of interfaces to configure.
 * \param no [IN] TRUE is user want to set mode to default value
 * \return None.
 **/
void poe_icli_priority(i32 session_id, BOOL has_low, BOOL has_high, BOOL has_critical, icli_stack_port_range_t *plist, BOOL no);

/**
 * \brief Function for configuring PoE management mode
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param has_* [IN] TRUE for the mode that the user want.
 * \param no [IN] TRUE is user want to set mode to default value
 * \return None.
 **/
void poe_icli_management_mode(i32 session_id, BOOL has_class_consumption, BOOL has_class_reserved_power, BOOL has_allocation_consumption, BOOL has_alllocation_reserved_power, BOOL has_lldp_consumption, BOOL has_lldp_reserved_power, BOOL no);


/**
 * \brief Function for configuring PoE capacitor detection
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param no [IN] TRUE is user want to set mode to default value
 * \return None.
 **/
void poe_icli_cap_detect_set(i32 session_id, BOOL no);

/**
 * \brief Function for configuring power limit for port in allocation mode.
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param value [IN] New value
 * \param plist [IN] List of interfaces to configure.
 * \param no [IN] TRUE is user want to set mode to default value
 * \return None.
 **/
void poe_icli_power_limit(i32 session_id, char *value, icli_stack_port_range_t *plist, BOOL no);

/** Selecting which power to configure.*/
typedef enum {VTSS_POE_ICLI_POWER_SUPPLY,
              VTSS_POE_ICLI_SYSTEM_RESERVE_POWER
             }
vtss_poe_icli_power_conf_t;

/**
 * \brief Function for configuring maximum power for the power supply
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param has_sid[IN] TRUE if user has specified a specific sid
 * \param usid[IN] User switch ID.
 * \param no [IN] TRUE is user want to set mode to default value
 * \return None.
 **/
void poe_icli_power_supply(i32 session_id, u32 value, BOOL no, vtss_poe_icli_power_conf_t power_type);

/**
 * \brief Function for getting the current power supply status
 *
 * \param session_id [IN] The session id used by iCLI print.
 *
 * \return None.
 **/
void poe_icli_get_power_in_status(i32 session_id);

#if 0
}
#endif
/**
 * \brief Function for getting the current led color of power supplies
 *
 * \param session_id [IN] The session id used by iCLI print.
 *
 * \return None.
 */
void poe_icli_get_power_in_led(i32 session_id);

/**
 * \brief Function for getting the current led color indicating PoE status
 *
 * \param session_id [IN] The session id used by iCLI print.
 *
 * \return None.
 */
void poe_icli_get_status_led(i32 session_id);

/**
 * \brief Function for initializing ICFG
 **/
mesa_rc poe_icfg_init(void);

/**
 * \brief Function for at runtime getting information if PoE is supported for the currently selected interfaces
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param ask [IN]  Asking
 * \param runtime [IN]  Pointer to where to put the "answer"
 * \return TRUE if runtime contains valid information.
 **/
BOOL poe_icli_interface_runtime_supported(u32                session_id,
                                          icli_runtime_ask_t ask,
                                          icli_runtime_t     *runtime);


/**
 * \brief Function for at runtime getting information if PoE is supported at all.
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param ask [IN]  Asking
 * \param runtime [IN]  Pointer to where to put the "answer"
 * \return TRUE if runtime contains valid information.
 **/
BOOL poe_icli_runtime_supported(u32                session_id,
                                icli_runtime_ask_t ask,
                                icli_runtime_t     *runtime);


mesa_rc poe_icli_firmware_upgrade(u32 session_id, mesa_port_no_t iport, const char *path, BOOL has_built_in);


/****************************************************************************/
/*                              LNTN PoE                                    */
/****************************************************************************/

/**
 * \brief Function for configuring Ping Alive State
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param plist [IN] List of interfaces to configure.
 * \param has_enable [IN] TRUE if the user want to set ping alive state .
 * \return None.
 **/
void ping_alive_icli_state(i32 session_id, icli_stack_port_range_t *plist, BOOL has_enable);

/**
 * \brief Function for configuring Ping Alive Interval
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param plist [IN] List of interfaces to configure.
 * \param interval [IN] The ping interval value.
 * \return None.
 **/
void ping_alive_icli_interval(i32 session_id, icli_stack_port_range_t *plist, u32 interval);

/**
 * \brief Function for configuring Ping Alive Address
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param plist [IN] List of interfaces to configure.
  * \param address [IN] The ping IP address.
 * \return None.
 **/
void ping_alive_icli_address(i32 session_id, icli_stack_port_range_t *plist, mesa_ipv4_t address);

/**
 * \brief Function for displaying PoE Ping Alive status
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param has_interface [IN] TRUE if the user want to display a specific interface (port)
 * \param list [IN] port list of which interfaces to display PoE Ping Alive status.
 * \return None.
 **/
void ping_alive_icli_show(i32 session_id, BOOL has_interface, icli_stack_port_range_t *list);

/**
 * \brief Function for configuring PoE Schedule ID.
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param schedule_id [IN] Schedule ID
 * \param plist [IN] List of interfaces to configure.
 * \param no [IN] TRUE is user want to set mode to default value
 * \return None.
 **/
void poe_schedule_icli_id(i32 session_id, icli_stack_port_range_t *plist, u8 sched_id);

/**
 * \brief Function for configuring PoE Schedule Mode
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param plist [IN] List of interfaces to configure.
 * \param has_on [IN] TRUE if the user want to set PoE schedule-on mode.
 * \param has_off [IN] TRUE if the user want to set PoE schedule-off mode.
 * \return None.
 **/
void poe_schedule_icli_mode(i32 session_id, icli_stack_port_range_t *plist, BOOL has_on, BOOL has_off);

/**
 * \brief Function for configuring PoE Schedule ID time.
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param schedule_id [IN] Schedule ID
 * \param week_list [IN] Weekday list.
 * \param start_time [IN] Start time for poe schedule.
 * \param end_time [IN] End time for poe schedule.
 * \return None.
 **/
void poe_schedule_icli_time(i32 session_id, u8 sched_id, u8 week_list, icli_time_t start_time, icli_time_t end_time, BOOL no);

/**
 * \brief Function for displaying PoE Ping Alive status
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param has_interface [IN] TRUE if the user want to display a specific interface (port)
 * \param list [IN] port list of which interfaces to display PoE Ping Alive status.
 * \param no [IN] TRUE is user want to set mode to default value
 * \return None.
 **/
void poe_schedule_icli_show(i32 session_id, BOOL has_interface, icli_stack_port_range_t *list);

/**
 * \brief Function for displaying PoE Schedule status
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param schedule_id [IN] Schedule ID.
 * \return None.
 **/
void poe_schedule_id_icli_show(i32 session_id, u8 sched_id);


#ifdef __cplusplus
}
#endif

#endif // _VTSS_ICLI_POE_H_
