/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
/*lint -esym(457,lldp_remote_set_requested_power) */ // Ok - LLDP is writing and PoE is reading. It doesn't matter if the read is missed for one cycle.
/****************************************************************************
PoE ( Power Over Ethernet ) is used to control external PoE chips. For more
information see the Design Spec (DS 0153)
*****************************************************************************/

#include "critd_api.h"
#include "poe.h"
#ifdef VTSS_SW_OPTION_LLDP
#include "lldp_sm.h"
#include "lldp_remote.h"
#include "lldp_api.h"
#include "lldp_basic_types.h"
#endif //  VTSS_SW_OPTION_LLDP
#include "poe_custom_api.h"
#include "misc_api.h"
#include "interrupt_api.h"
#include "poe_trace.h"
#include "../port/port_serializer.hxx"
#ifdef VTSS_SW_OPTION_ICFG
#include "poe_icli_functions.h" // For poe_icfg_init
#endif

#ifdef VTSS_SW_OPTION_WEB
#include "web_api.h"
#endif /* VTSS_SW_OPTION_WEB */

#include "vtss_common_iterator.hxx"


#if defined(VTSS_SW_OPTION_SYSLOG)
#include "syslog_api.h"
#endif  // VTSS_SW_OPTION_SYSLOG

#include "led_api.h"
#include "lock.hxx"

#if defined(VTSS_SW_OPTION_LED_STATUS)
#include "vtss/appl/led.h"
#endif /* VTSS_SW_OPTION_LED_STATUS */
#include "vtss_tftp_api.h"

#include "conf_api.h"  // For conf_mgmt_pwrmaxbgt_get
#include "port_api.h"  // For port_status->poe_pg setting
#include "vtss_timer_api.h"
#include "daylight_saving_api.h"
#include "netinet/ip.h"
#include "netinet/ip_icmp.h"
#include "sysutil_api.h"  // For system_get_tz_off

/****************************************************************************/
/*  TRACE system                                                            */
/****************************************************************************/

#if (VTSS_TRACE_ENABLED)
static vtss_trace_reg_t trace_reg = {
    VTSS_TRACE_MODULE_ID, "poe", "Power Over Ethernet"
};

static vtss_trace_grp_t trace_grps[VTSS_TRACE_GRP_CNT] = {
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_CRIT */ {
        "crit",
        "Critical regions ",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_MGMT */ {
        "pow_mgmt",
        "Power Management",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_CONF */ {
        "conf",
        "Configuration",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_STATUS */ {
        "status",
        "Status",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_CUSTOM */ {
        "custom",
        "PoE Chip set",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_ICLI */ {
        "iCLI",
        "iCLI",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PING */ {
        "ping",
        "ping",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
};
#define POE_CRIT_ENTER()         T_RG(VTSS_TRACE_GRP_CRIT, "Enter"); critd_enter(        &crit,        VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define POE_CRIT_EXIT()          T_RG(VTSS_TRACE_GRP_CRIT, "Exit"); critd_exit(         &crit,        VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define POE_CRIT_STATUS_ENTER()  T_RG(VTSS_TRACE_GRP_CRIT, "Enter"); critd_enter(&crit_status, VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define POE_CRIT_STATUS_EXIT()   T_RG(VTSS_TRACE_GRP_CRIT, "Exit"); critd_exit(&crit_status,  VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)

// LNTN PoE
#define PING_ALIVE_CRIT_ENTER()         critd_enter(        &crit_ping_alive,        VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define PING_ALIVE_CRIT_EXIT()          critd_exit(         &crit_ping_alive,        VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define POE_SCHED_CRIT_ENTER()          critd_enter(        &crit_poe_sched,         VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define POE_SCHED_CRIT_EXIT()           critd_exit(         &crit_poe_sched,         VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)

#else
#define POE_CRIT_ENTER()         critd_enter(        &crit)
#define POE_CRIT_EXIT()          critd_exit(         &crit)
#define POE_CRIT_STATUS_ENTER()  critd_enter(        &crit_status)
#define POE_CRIT_STATUS_EXIT()   critd_exit(         &crit_status)

// LNTN PoE
#define PING_ALIVE_CRIT_ENTER()         critd_enter(        &crit_ping_alive)
#define PING_ALIVE_CRIT_EXIT()          critd_exit(         &crit_ping_alive)
#define POE_SCHED_CRIT_ENTER()          critd_enter(        &crit_poe_sched)
#define POE_SCHED_CRIT_EXIT()           critd_exit(         &crit_poe_sched)

#endif /* VTSS_TRACE_ENABLED */

/****************************************************************************/
/*  Pre-defined functions */
/****************************************************************************/
static void update_registers(poe_conf_t *new_conf);
/****************************************************************************/
/*  Global variables */
/****************************************************************************/
// Variable to signaling the PoE chipset has been initialized. It is OK that it is not semaphore protected, since it is only supposed to be set one place.
static BOOL poe_init_done = FALSE;
static BOOL switch_add = FALSE;
// Message
#define POE_TIMEOUT (vtss_current_time() + VTSS_OS_MSEC2TICK(20000)) /* Wait for timeout (20 seconds) or synch. flag to be set ( I2C might be blocked by other modules/threads ). */

// Configuration
static poe_conf_t poe_conf;   // Current configuration.

static mesa_port_list_t power_budget_exceeded; // If we do manual power management ( which is
// needed in some modes) this indicates if the port is forced off,
// due to that the power budget has been exceeded
static mesa_port_list_t pd_overload; //

/* Critical region protection */
static critd_t crit;  // Critical region for global variables
static critd_t crit_status;  // Critial region for the poe_status.
static critd_t crit_ping_alive;
static critd_t crit_poe_sched;

// When a port is turned off due to that the port uses too much power, we have to turn it on again
// sometime to see if it still uses too much power.
// This is done with this counter. When the the counter hs counted down to zero, the port is turn
// on again.
static CapArray<char, MESA_CAP_PORT_CNT> retry_cnt;
const  char retry_cnt_max = 5; // Specifies the maximum number of seconds (Depending upon the sleep in poe_thread function) to wait before tuning on the port

// PoE power supply leds
vtss_appl_poe_led_t power1_led_color = VTSS_APPL_POE_LED_OFF;  // by default
vtss_appl_poe_led_t power2_led_color = VTSS_APPL_POE_LED_OFF;  // by default

// PoE status led
vtss_appl_poe_led_t status_led_color = VTSS_APPL_POE_LED_OFF;  // by default
vtss_appl_poe_led_t prev_status_led_color = VTSS_APPL_POE_LED_OFF;  // by default

// LNTN PoE global struct
static ping_alive_local_t ping_alive_local;
static sched_port_local_t sched_port_local;
static sched_time_local_t sched_time_local;
static CapArray<vtss::Timer, MESA_CAP_PORT_CNT> ping_alive_timer_local;
static CapArray<bool, MESA_CAP_PORT_CNT> sched_active;

#define POE_PING_PACKET_SIZE (128)

/*************************************************************************
** Misc Functions
*************************************************************************/
//
// Converts error to printable text
//
// In : rc - The error type
//
// Retrun : Error text
//
const char *poe_error_txt(mesa_rc rc)
{
    switch (rc) {
    case VTSS_APPL_POE_ERROR_NULL_POINTER:
        return "Unexpected reference to NULL pointer.";

    case VTSS_APPL_POE_ERROR_UNKNOWN_BOARD:
        return "Unknown board type.";

    case VTSS_APPL_POE_ERROR_NOT_MASTER:
        return "Unknown board type.";

    case VTSS_APPL_POE_ERROR_PRIM_SUPPLY_RANGE:
        return "Primary power supply value out of range.";

    case VTSS_APPL_POE_ERROR_CONF_ERROR:
        return "Internal error - Configuration could not be done.";

    case VTSS_APPL_POE_ERROR_PORT_POWER_EXCEEDED:
        return "Port power exceeded.";

    case VTSS_APPL_POE_ERROR_NOT_SUPPORTED:
        return "Port is not supporting PoE.";

    case VTSS_APPL_POE_ERROR_FIRMWARE:
        return "PoE firmware download failed.";

    case VTSS_APPL_POE_ERROR_DETECT:
        return "PoE chip detection still in progress.";

    case VTSS_APPL_POE_ERROR_FIRMWARE_VER:
        return "PoE firmware version not found.";

    case VTSS_APPL_POE_ERROR_FIRMWARE_VER_NOT_NEW:
        return "Already contains this PoE firmware version";

    case VTSS_RC_OK:
        return "";
    }

    T_I("rc:%d", rc);
    return "Unknown PoE error";
}


#include <iostream>
#include <fstream>
#include <string>

// Doing firmware upgrade.
// url - Only valid if "has_built_in" if false. String with tftp path to new firmware file
// tftp_err - Only valid if "has_built_in" if false. tftp error code, to be used with vtss_tftp_err2str
// has_built_in - TRUE to use the built in firmware placed in /etc/mscc/poe/firmware/mscc_firmware.txt. FALSE to use the URL to find the firmware
mesa_rc poe_do_firmware_upgrade(mesa_port_no_t iport, const char *url, int &tftp_err, BOOL has_built_in)
{
    mesa_rc rc = VTSS_RC_OK;
    int size = 0;
    char *buffer = NULL;

    if ((VTSS_MALLOC_CAST(buffer, POE_FIRMWARE_SIZE_MAX)) == NULL) {
        T_E("Memory allocation issue");
        rc = VTSS_APPL_POE_ERROR_FIRMWARE;
        goto out;
    }

    if (has_built_in) {
        size = 0;
        std::ifstream file ("/etc/mscc/poe/firmware/mscc_firmware.txt");
        if (file.is_open()) {
            while (!file.eof() && size < POE_FIRMWARE_SIZE_MAX) {
                file.get(buffer[size++]); //reading one character from file to array
            }
            file.close();
        } else {
            rc = VTSS_APPL_POE_ERROR_FIRMWARE;
            goto out;
        }
    } else {
        // Get tftp data
        misc_url_parts_t url_parts;
        misc_url_parts_init(&url_parts, MISC_URL_PROTOCOL_TFTP  |
                            MISC_URL_PROTOCOL_FTP   |
                            MISC_URL_PROTOCOL_HTTP  |
                            MISC_URL_PROTOCOL_HTTPS |
                            MISC_URL_PROTOCOL_FILE);

        if (!misc_url_decompose(url, &url_parts)) {
            T_I("Invalid url");
            rc = VTSS_APPL_POE_ERROR_FIRMWARE;
            goto out;
        }

        if ((size = vtss_tftp_get(url_parts.path, url_parts.host, url_parts.port,
                                  (char *) buffer, POE_FIRMWARE_SIZE_MAX, TRUE, &tftp_err)) <= 0) {
            rc = VTSS_APPL_POE_ERROR_FIRMWARE;
            goto out;
        }

    }

    T_N("%s", buffer);

    rc = poe_mgmt_firmware_update(iport, buffer, size);
    T_I("size:%d, rc:%d", size, rc);

out:
    // Free resource
    if (buffer) {
        VTSS_FREE(buffer);
    }

    if (rc == VTSS_RC_OK) {
        poe_custom_init_chips(); // PoE chip has been reset during the upgrade process. We need to re-initialize the chip.
    }
    return rc;
}
/*****************************************************************/
// Description: Converting a integer to a string with one digit. E.g. 102 becomes 10.2. *
// Output: Returns pointer to string
/*****************************************************************/

char *one_digi_float2str(int val, char *max_power_val_str)
{
    char digi[2] = "";

    // Convert the integer to a string
    sprintf(max_power_val_str, "%d", val);

    int str_len = strlen(max_power_val_str);

    // get the last charater in the string
    digi[0] =  max_power_val_str[str_len - 1];
    digi[1] =  '\0';

    // Remove the last digi in the string
    max_power_val_str[str_len - 1] = '\0';


    if (str_len == 1) {
        // If the integer only were one digi then add "0." in front. E.g. 4 becomes 0.4
        strcat(max_power_val_str, "0.");
    } else {
        // Add the "dot" to the string
        strcat(max_power_val_str, ".");
    }

    // Add the digi to the string
    strcat(max_power_val_str, &digi[0]);


    // return the string
    return max_power_val_str;
}



// Function that converts from class into power in deci watts
static int class2power(char class_, mesa_port_no_t iport, vtss_appl_poe_port_mode_t mode)
{
    // Classes is defined in IEEE 802.3at table 33-6.
    switch (class_) {
    case 0 :
    case 3 :
        return 154; // 15.4 watts
    case 1 :
        return 40; // 4.0 watts
    case 2 :
        return 70; // 7.0 watts
    case 4 :
    default:
        if (mode == VTSS_APPL_POE_MODE_POE_PLUS) {
            return 300; // Maximum for PoE+
        } else {
            return 154; // Only allow PoE standard.
        }
    }
}

// Return if a port is in shut-down by user.
static BOOL is_port_shutdown(mesa_port_no_t iport)
{
    vtss_ifindex_t ifindex;
    vtss_appl_port_conf_t port_conf;

    // Make sure that we don't get out-of-bounds
    if (iport >= port_isid_port_count(VTSS_ISID_LOCAL)) {
        T_N("PORT:%d, count:%d", iport, port_isid_port_count(VTSS_ISID_LOCAL));
        return TRUE;
    }

    VTSS_RC_ERR_PRINT(vtss_ifindex_from_port(VTSS_ISID_START, iport, &ifindex));
    VTSS_RC_ERR_PRINT(vtss_appl_port_conf_get(ifindex, &port_conf));

    if (!port_conf.admin.enable) {
        return TRUE;
    }

    return FALSE;
}

// Getting local mode configuration when taking into account that port can
// be shut-down by the port module.
static vtss_appl_poe_port_mode_t poe_mode_get(mesa_port_no_t iport)
{
    POE_CRIT_ENTER(); // Protect poe_conf
    vtss_appl_poe_port_mode_t status = poe_conf.poe_mode[iport];
    POE_CRIT_EXIT();
    T_RG_PORT(VTSS_TRACE_GRP_STATUS, iport, "Status:0x%X", status);
    if (is_port_shutdown(iport)) {
        status = VTSS_APPL_POE_MODE_DISABLED;
    }
    T_RG_PORT(VTSS_TRACE_GRP_STATUS, iport, "Status:0x%X", status);
    return status;
}

// Function that returns the maximum power allowed for a port (depending upon the mode)
// In : iport - Port starting from 0. The port at which to get the maximum power
//      poe_mode - The current mode for iport.
// return  : The maximum allowed power for the port.
u16 poe_max_power_mode_dependent(mesa_port_no_t iport, vtss_appl_poe_port_mode_t poe_mode)
{
    u16 max_power = 0;

    switch (poe_mode) {
    case VTSS_APPL_POE_MODE_POE:
        // 15.4 W is Maximum for PoE
        max_power = 154;
        break;
    case VTSS_APPL_POE_MODE_POE_PLUS:
        // 30W is Maximum for PoE+
        max_power = 300;
        break;
    case VTSS_APPL_POE_MODE_DISABLED:
        max_power = 300;
        break;
    default:
        max_power = 300;
        break;
    }

    // Make sure that we doesn't configure more power than the PoE chip set can deliver.
    if (max_power < poe_custom_get_port_power_max(iport)) {
        return max_power;
    } else {
        return poe_custom_get_port_power_max(iport);
    }
}

static int poe_find_allocated_power(int requested_power, mesa_port_no_t iport)
{
    int max_power;

    // Set max power according to class.
    max_power = poe_max_power_mode_dependent(iport, poe_mode_get(iport));

    if (poe_mode_get(iport) == VTSS_APPL_POE_MODE_DISABLED) {
        // If the port is disabled then allocate  0W
        max_power = 0;
    }

    // If the request power is less than the class allow then only allocate the requested power.
    if (requested_power < max_power) {
        max_power = requested_power;
    }

    return max_power;
}

// Maximum power for the power supply.
void poe_power_supply_set(mesa_port_no_t iport, int primary_max_power, int backup_max_power)
{
    T_RG(VTSS_TRACE_GRP_CUSTOM, "Entering poe_power_supply_set");

    if (primary_max_power <  vtss_appl_poe_supply_min_get()) {
        primary_max_power = vtss_appl_poe_supply_min_get();
    }

    if (backup_max_power < vtss_appl_poe_supply_min_get()) {
        backup_max_power = vtss_appl_poe_supply_min_get();
    }

    if (primary_max_power > vtss_appl_poe_supply_max_get()) {
        primary_max_power = vtss_appl_poe_supply_max_get();
    }

    if (backup_max_power > vtss_appl_poe_supply_max_get()) {
        backup_max_power = vtss_appl_poe_supply_max_get();
    }

    if (vtss_board_type() == VTSS_BOARD_JAGUAR2_CU48_REF) {
        primary_max_power /= 2;
        backup_max_power  /= 2;
    }
    poe_custom_set_power_supply(iport, primary_max_power, backup_max_power);
}

// Function for updating a local copy of the PoE status.
// IN :  local_poe_status - Pointer to the new status, or Pointer to where to put the current status. Depends upon the get parameter-.
//       get _ True to get the last status, FALSE to update status.
static void poe_status_set_get(poe_status_t *local_poe_status, BOOL get)
{
    // The status from PoE is updated every sec in order to be able to respond upon changes. To get fast access to the status from management
    // we keep a local copy of the status which we give back fast.
    static poe_status_t poe_status;
    T_R("Enter poe_status_get get:%d", get);
    POE_CRIT_STATUS_ENTER();
    if (get) {
        *local_poe_status = poe_status;
    } else {
        poe_status = *local_poe_status;
    }
    POE_CRIT_STATUS_EXIT();
    T_NG(VTSS_TRACE_GRP_STATUS, "Total power:%d, total_current:%d, total reserved:%d, get:%d",
         poe_status.total_power_used, poe_status.total_current_used, poe_status.total_power_reserved, get);
}

/**
 * \brief Function for Analyze PoE port status for updating led later
 *
 * \param poe_status [IN]  Current poe status
 * \param port_index [IN]  port number
 * \param poe_led    [OUT] update poe led bitset
 *
 * \return void
 **/
static void vtss_appl_poe_led_analyze(poe_status_t *poe_status,
                                      mesa_port_no_t port_index,
                                      u8 *poe_led)
{
    switch (poe_status->port_status[port_index]) {
    case VTSS_APPL_POE_POWER_BUDGET_EXCEEDED :
    case VTSS_APPL_POE_PD_OVERLOAD : {
        *poe_led |= POE_LED_POE_DENIED;
        break;
    }
    case VTSS_APPL_POE_PD_ON : {
        *poe_led |= POE_LED_POE_ON;
        break;
    }
    case VTSS_APPL_POE_DISABLED :
    case VTSS_APPL_POE_NO_PD_DETECTED :
    case VTSS_APPL_POE_PD_OFF: {
        *poe_led |= POE_LED_POE_OFF;
        break;
    }
    default :
        // do not change led
        break;
    }
}

/**
 * \brief Function for setting up PoE status leds, Power supply leds
 *
 * \param port_bit [IN] sgpio mapping number, defined in
 * vtss_appl/board/port_custom_lu26.c
 *
 * \param color [IN] The color to be set
 *
 * \return VTSS_RC_OK  if operation is succeeded.
 **/
mesa_rc vtss_appl_poe_led_set(u8 port_bit,
                              vtss_appl_poe_led_t color)
{
#if defined(THIS_IS_UBELEIVABLE_STUPID_CODE_NOT_EVEN_TAKING_BOARD_TYPE_INTO_ACCOUNT)
// Macros for port_bit
#define VTSS_POE_POE_POWER_LED1 16
#define VTSS_POE_POE_POWER_LED2 17
#define VTSS_POE_POE_STATUS_LED 18
    mesa_sgpio_conf_t conf;
    static vtss_appl_poe_led_t pre_color = VTSS_APPL_POE_LED_OFF;  // for blinking led

    /* Make sure that the SGPIO initialization has been done */
    if (vtss_board_type() == VTSS_BOARD_UNKNOWN) {
        return VTSS_APPL_POE_ERROR_UNKNOWN_BOARD;
    }

    vtss_appl_api_lock_unlock api_lock;       /* Protect SGIO conf */
    VTSS_RC(mesa_sgpio_conf_get(NULL, 0, 0, &conf));

    /* Pre-process blink red case */
    if (color == VTSS_APPL_POE_LED_BLINK_RED) {
        color = (pre_color == VTSS_APPL_POE_LED_OFF) ?
                VTSS_APPL_POE_LED_RED : VTSS_APPL_POE_LED_OFF;
        pre_color = color;
    }

    switch (color) {
    case VTSS_APPL_POE_LED_GREEN:
        conf.port_conf[port_bit].mode[0] = MESA_SGPIO_MODE_ON;
        conf.port_conf[port_bit].mode[1] = MESA_SGPIO_MODE_OFF;
        break;
    case VTSS_APPL_POE_LED_RED:
        conf.port_conf[port_bit].mode[0] = MESA_SGPIO_MODE_OFF;
        conf.port_conf[port_bit].mode[1] = MESA_SGPIO_MODE_ON;
        break;
    default:
        conf.port_conf[port_bit].mode[0] = MESA_SGPIO_MODE_OFF;
        conf.port_conf[port_bit].mode[1] = MESA_SGPIO_MODE_OFF;
        break;
    }
    return mesa_sgpio_conf_set(NULL, 0, 0, &conf);
#else
    return MESA_RC_OK;
#endif
}

/**
 * \brief Function for setting poe power supply leds
 * Green - Power is present on the associated circuit, system is operating
 *         normally.
 * Off   - Power is not present on the associated circuit, or the system is not
 *         powered up.
 * Red   - Power is not present on the associated circuit, and the power supply
 *         alarm is configured.
 *
 * \return VTSS_RC_OK if operation succeeded.
 **/
static mesa_rc vtss_appl_poe_pwr_led_set(void)
{
    vtss_appl_poe_powerin_status_t pwr_st;

    VTSS_RC(vtss_appl_poe_powerin_status_get(&pwr_st));

#if defined(VTSS_POE_POE_POWER_LED1)
    VTSS_RC(vtss_appl_poe_led_set(VTSS_POE_POE_POWER_LED1,
                                  pwr_st.pwr_in_status1 ? VTSS_APPL_POE_LED_GREEN : VTSS_APPL_POE_LED_OFF));
#endif /* VTSS_SW_OPTION_LED_STATUS */

    // led has been set ok, update the global variable
    power1_led_color = pwr_st.pwr_in_status1 ? VTSS_APPL_POE_LED_GREEN : VTSS_APPL_POE_LED_OFF;

#if defined(VTSS_POE_POE_POWER_LED2)
    VTSS_RC(vtss_appl_poe_led_set(VTSS_POE_POE_POWER_LED2,
                                  pwr_st.pwr_in_status2 ? VTSS_APPL_POE_LED_GREEN : VTSS_APPL_POE_LED_OFF));
#endif /* VTSS_SW_OPTION_LED_STATUS */

    // led has been set ok, update the global variable
    power2_led_color = pwr_st.pwr_in_status2 ? VTSS_APPL_POE_LED_GREEN : VTSS_APPL_POE_LED_OFF;

#if defined(VTSS_SW_OPTION_LED_STATUS)
    led_system_led_state_set(SYSTEM_LED_ID_DCA, pwr_st.pwr_in_status1 ? VTSS_APPL_LED_CTRL_DCA_NORMAL : VTSS_APPL_LED_CTRL_DCA_NOT_PRESENT, FALSE);
    led_system_led_state_set(SYSTEM_LED_ID_DCB, pwr_st.pwr_in_status2 ? VTSS_APPL_LED_CTRL_DCB_NORMAL : VTSS_APPL_LED_CTRL_DCB_NOT_PRESENT, FALSE);
#endif /* VTSS_SW_OPTION_LED_STATUS */

    meba_status_led_set(board_instance, MEBA_LED_TYPE_DC_A, pwr_st.pwr_in_status1 ? MEBA_LED_COLOR_GREEN : MEBA_LED_COLOR_OFF);
    meba_status_led_set(board_instance, MEBA_LED_TYPE_DC_B, pwr_st.pwr_in_status2 ? MEBA_LED_COLOR_GREEN : MEBA_LED_COLOR_OFF);

    /* TODO: Handle Red status */
    return VTSS_RC_OK;
}

/**
 * \brief Function for getting the status of the 2 power leds on the PoE board.
 *
 * \param pwr_led1 [IN/OUT] Led status for power supply 1.
 *
 * \param pwr_led2 [IN/OUT] Led status for power supply 2.
 *
 * \return VTSS_RC_OK  if operation is succeeded.
 **/
mesa_rc vtss_appl_poe_powerin_led_get(vtss_appl_poe_powerin_led_t *const pwr_led)
{
    vtss_appl_poe_powerin_status_t pwr_st;

    VTSS_RC(vtss_appl_poe_powerin_status_get(&pwr_st));

    pwr_led->pwr_led1 = pwr_st.pwr_in_status1 ? VTSS_APPL_POE_LED_GREEN : VTSS_APPL_POE_LED_OFF;
    pwr_led->pwr_led2 = pwr_st.pwr_in_status2 ? VTSS_APPL_POE_LED_GREEN : VTSS_APPL_POE_LED_OFF;

    return VTSS_RC_OK;
}

#if defined(VTSS_SW_OPTION_SYSLOG)
static const char *led_color2str(vtss_appl_poe_led_t color)
{
    switch (color) {
    case VTSS_APPL_POE_LED_RED: {
        return "Red";
    }
    case VTSS_APPL_POE_LED_GREEN: {
        return "Green";
    }
    case VTSS_APPL_POE_LED_BLINK_RED: {
        return "BlinkRed";
    }
    case VTSS_APPL_POE_LED_OFF: {
        return "Off";
    }
    default:
        return "Unknown";
    }
}
#endif /* defined(VTSS_SW_OPTION_SYSLOG) */

/**
 * \brief Function for setting PoE status led color
 *
 * \param bitset [IN] bitset for setting the PoE status led.
 *
 * \return VTSS_RC_OK if operation is succeeded.
 */
mesa_rc vtss_appl_poe_status_led_set(const u8 bitset)
{
    if (bitset & POE_LED_POE_ERROR) {
        meba_status_led_set(board_instance, MEBA_LED_TYPE_POE, MEBA_LED_COLOR_RED);
    } else if (bitset & POE_LED_POE_DENIED) {
        meba_status_led_set(board_instance, MEBA_LED_TYPE_POE, MEBA_LED_COLOR_YELLOW);
    } else if (bitset & POE_LED_POE_ON) {
        meba_status_led_set(board_instance, MEBA_LED_TYPE_POE, MEBA_LED_COLOR_GREEN);
    } else {
        meba_status_led_set(board_instance, MEBA_LED_TYPE_POE, MEBA_LED_COLOR_OFF);
    }
#if defined(VTSS_SW_OPTION_LED_STATUS)
    u32 state;
    if (bitset & POE_LED_POE_ERROR) {
        state = VTSS_APPL_LED_CTRL_POE_FAULT;
        status_led_color = VTSS_APPL_POE_LED_RED;
    } else if (bitset & POE_LED_POE_DENIED) {
        state = VTSS_APPL_LED_CTRL_POE_EXCEED;
        status_led_color = VTSS_APPL_POE_LED_BLINK_RED;
    } else if (bitset & POE_LED_POE_ON) {
        state = VTSS_APPL_LED_CTRL_POE_ON;
        status_led_color = VTSS_APPL_POE_LED_GREEN;
    } else if (bitset & POE_LED_POE_OFF) {
        state = VTSS_APPL_LED_CTRL_POE_OFF;
        status_led_color = VTSS_APPL_POE_LED_OFF;
    } else {
        T_WG(VTSS_TRACE_GRP_STATUS, "Unknown PoE LED Status %u!", bitset);
        state = VTSS_APPL_LED_CTRL_POE_OFF;
        status_led_color = VTSS_APPL_POE_LED_OFF;
    }

    // set led color
    if (led_system_led_state_set(SYSTEM_LED_ID_POE, state, FALSE) == VTSS_RC_OK) {

        T_NG(VTSS_TRACE_GRP_STATUS, "poe status led bitset: %u, color: %d\n",
             bitset,
             (int)status_led_color);

#if defined(VTSS_SW_OPTION_SYSLOG)
        // check if color has changed
        if (status_led_color != prev_status_led_color) {
            S_I("POE STATUS LED: Changed from %s to %s.",
                led_color2str(prev_status_led_color),
                led_color2str(status_led_color)
               );
        }
#endif  // VTSS_SW_OPTION_SYSLOG

        // update previous color
        prev_status_led_color = status_led_color;
    } else {
        // return error if set LED failed
        return VTSS_RC_ERROR;
    }
#else
    if (bitset & POE_LED_POE_ERROR) {
        status_led_color = VTSS_APPL_POE_LED_RED;
    } else if (bitset & POE_LED_POE_DENIED) {
        status_led_color = VTSS_APPL_POE_LED_BLINK_RED;
    } else if (bitset & POE_LED_POE_ON) {
        status_led_color = VTSS_APPL_POE_LED_GREEN;
    } else if (bitset & POE_LED_POE_OFF) {
        status_led_color = VTSS_APPL_POE_LED_OFF;
    } else {
        T_WG(VTSS_TRACE_GRP_STATUS, "Unknown PoE LED Status %u!", bitset);
        status_led_color = VTSS_APPL_POE_LED_OFF;
    }
    T_NG(VTSS_TRACE_GRP_STATUS, "poe status led bitset: %u, color: %d\n",
         bitset,
         (int)status_led_color);

#if defined(VTSS_SW_OPTION_SYSLOG)
    // check if color has changed
    if (status_led_color != prev_status_led_color) {
        S_I("POE STATUS LED: Changed from %s to %s.",
            led_color2str(prev_status_led_color),
            led_color2str(status_led_color)
           );
    }
#endif  // VTSS_SW_OPTION_SYSLOG

    // update previous color
    prev_status_led_color = status_led_color;

    // set led color via sgpio interface
#if defined(VTSS_POE_POE_STATUS_LED)
    return vtss_appl_poe_led_set(VTSS_POE_POE_STATUS_LED, status_led_color);
#endif
#endif /* VTSS_SW_OPTION_LED_STATUS */
    return VTSS_RC_OK;
}

/**
 * \brief Function for getting PoE status led color
 *
 * \param status_led [IN/OUT] The current color of the PoE status led.
 *
 * \return VTSS_RC_OK if operation succeeded.
 **/
mesa_rc vtss_appl_poe_status_led_get(vtss_appl_poe_status_led_t *status_led)
{
    status_led->status_led = status_led_color;
    return VTSS_RC_OK;
}

bool is_media_type_sfp(mesa_port_no_t iport)
{

    vtss_ifindex_t ifindex;
    meba_port_cap_t cap;
    vtss_appl_port_media_t media = VTSS_APPL_PORT_MEDIA_CU;
    VTSS_RC(vtss_ifindex_from_port(VTSS_ISID_LOCAL, iport, &ifindex));
    cap = port_isid_port_cap(VTSS_ISID_LOCAL, iport); // get port capabilities
    vtss_appl_port_conf_t port_conf;
    VTSS_RC(vtss_appl_port_conf_get(ifindex, &port_conf));

    if (cap & (MEBA_PORT_CAP_DUAL_SFP_DETECT | MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER)) {
        if (port_conf.dual_media_fiber_speed == MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED) {
            // cu only
            media = VTSS_APPL_PORT_MEDIA_CU;
        } else if (port_conf.dual_media_fiber_speed == MESA_SPEED_FIBER_AUTO) {
            // Both SFP and cu (default)
            media = VTSS_APPL_PORT_MEDIA_DUAL;
        } else {
            // sfp only
            media = VTSS_APPL_PORT_MEDIA_SFP;
        }
    }

    return (media == VTSS_APPL_PORT_MEDIA_SFP);
}

static void lntn_poe_led_update(mesa_port_no_t port_index, poe_status_t *poe_status)
{
    static CapArray<char, MESA_CAP_PORT_CNT> led_on;

    switch (poe_status->port_status[port_index]) {
        case VTSS_APPL_POE_POWER_BUDGET_EXCEEDED :
        case VTSS_APPL_POE_PD_OVERLOAD :
        case VTSS_APPL_POE_DISABLED :
        case VTSS_APPL_POE_NO_PD_DETECTED :
        case VTSS_APPL_POE_PD_OFF:
            if (led_on[port_index] == 1) {
                T_DG(VTSS_TRACE_GRP_STATUS, "To do: PoE Port[%d] led turn off.", port_index);
                led_on[port_index] = 0;
                port_set_poe_pg(port_index, FALSE);

                // Bugfix: PoE LED is updated too slowly
                meba_mptest_port_led_set(board_instance, port_index, MEBA_LED_COLOR_OFF);
            }
            break;

        case VTSS_APPL_POE_PD_ON :
            if (led_on[port_index] == 0) {
                T_DG(VTSS_TRACE_GRP_STATUS, "To do: PoE Port[%d] led turn on.", port_index);
                led_on[port_index] = 1;
                port_set_poe_pg(port_index, TRUE);

                // Bugfix: PoE LED is updated too slowly
                meba_mptest_port_led_set(board_instance, port_index, MEBA_LED_COLOR_YELLOW);
            }
            break;

        default :
            // do not change led
            break;
    }
}

// Getting and updating poe status
static void poe_status_update(void)
{
    poe_status_t poe_status;
    mesa_port_no_t port_index;
    CapArray<char, MESA_CAP_PORT_CNT> classes;
    u8 poe_led = POE_LED_POE_OFF;  // bitset for PoE led status
    T_NG(VTSS_TRACE_GRP_STATUS, "Getting PoE poe_status" );

    // Update Power and current used fields
    poe_custom_get_status(&poe_status);

    // Get the class for all PDs connected.
    poe_custom_get_all_ports_classes(&classes[0]);

    // Set PoE two power supply leds
    vtss_appl_poe_pwr_led_set();

    poe_status.total_power_used = 0;
    poe_status.total_current_used = 0;
    poe_status.total_power_reserved =  0;

    for (port_index = 0 ; port_index < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); port_index++) {

        if (is_media_type_sfp(port_index)) {
            poe_status.port_status[port_index] = VTSS_APPL_POE_NOT_SUPPORTED;
            poe_status.poe_chip_found[port_index] = NO_POE_CHIPSET_FOUND;
        } else {
            // Update the port_status fields
            poe_custom_port_status_get(port_index, &poe_status.port_status[port_index]);
            poe_status.poe_chip_found[port_index] = poe_custom_is_chip_found(port_index);
        }
        T_RG_PORT(VTSS_TRACE_GRP_STATUS, port_index, "Found:%d", poe_status.poe_chip_found[port_index]);

        // Leonton PoE led
        lntn_poe_led_update(port_index, &poe_status);

        // Make sure that power used and current used have valid value even when no PoE chip is not found.
        if (poe_status.poe_chip_found[port_index] == NO_POE_CHIPSET_FOUND ||
            poe_status.port_status[port_index]    == VTSS_APPL_POE_NOT_SUPPORTED ||
            poe_status.port_status[port_index]    == VTSS_APPL_POE_NO_PD_DETECTED ||
            poe_mode_get(port_index)   == VTSS_APPL_POE_MODE_DISABLED ||
            poe_status.port_status[port_index]    == VTSS_APPL_POE_UNKNOWN_STATE) {

            T_DG_PORT(VTSS_TRACE_GRP_STATUS, port_index, "Resetting status for port with not PoE chipset, %d", poe_mode_get(port_index));
            poe_status.power_used[port_index] = 0;
            poe_status.current_used[port_index] = 0;
            poe_status.pd_class[port_index] = POE_UNDETERMINED_CLASS;
            if (poe_conf.power_mgmt_mode == VTSS_APPL_POE_ALLOCATED_RESERVED || poe_conf.power_mgmt_mode == VTSS_APPL_POE_ALLOCATED_CONSUMP) {
                // When in allocated mode, show the configured power even when no PD is present.
                poe_status.power_requested[port_index] = poe_conf.max_port_power[port_index];
                T_NG_PORT(VTSS_TRACE_GRP_MGMT, port_index, "power_requested = %d", poe_status.power_requested[port_index]);
            } else {
                poe_status.power_requested[port_index] = 0;
            }
            poe_status.power_allocated[port_index] =  0;

            // We have seen that some PoE Chipsets don't give PoE disable back when the port is powered down,
            // so we force the status in order to make sure that status is shown correct.
            if (poe_status.port_status[port_index] != VTSS_APPL_POE_NOT_SUPPORTED) {
                if (is_port_shutdown(port_index)) {
                    T_DG_PORT (VTSS_TRACE_GRP_STATUS, port_index, "%s", "Interface shutdown");
                    poe_status.port_status[port_index] = VTSS_APPL_POE_DISABLED_INTERFACE_SHUTDOWN;
                } else if (poe_mode_get(port_index) == VTSS_APPL_POE_MODE_DISABLED) {
                    poe_status.port_status[port_index] = VTSS_APPL_POE_DISABLED;
                }
            }

            continue; // continue to next port
        }

        // Check PoE port status for led update
        vtss_appl_poe_led_analyze(&poe_status, port_index, &poe_led);

        // Set reqeusted power either by LLDP, class or reserved.
        if (poe_conf.power_mgmt_mode == VTSS_APPL_POE_LLDPMED_RESERVED ||
            poe_conf.power_mgmt_mode == VTSS_APPL_POE_LLDPMED_CONSUMP) {
#ifdef VTSS_SW_OPTION_LLDP
            if (lldp_remote_lldp_is_info_valid(port_index)) {
                poe_status.power_requested[port_index] = lldp_remote_get_requested_power(port_index, poe_mode_get(port_index));
                T_DG_PORT(VTSS_TRACE_GRP_STATUS, port_index, "lldp_remote_lldp_is_info_valid, %d", poe_status.power_requested[port_index]);
            } else {
                poe_status.power_requested[port_index] = class2power(classes[port_index], port_index, poe_mode_get(port_index));
            }
#endif // VTSS_SW_OPTION_LLDP
        } else if (poe_conf.power_mgmt_mode == VTSS_APPL_POE_CLASS_RESERVED ||
                   poe_conf.power_mgmt_mode == VTSS_APPL_POE_CLASS_CONSUMP) {
            poe_status.power_requested[port_index] =  class2power(classes[port_index], port_index, poe_mode_get(port_index));
            T_DG_PORT(VTSS_TRACE_GRP_MGMT, port_index, "power_requested = %d (via class:%d)", poe_status.power_requested[port_index], classes[port_index]);
        } else {
            POE_CRIT_ENTER(); // Protect poe_conf
            poe_status.power_requested[port_index] = poe_conf.max_port_power[port_index];
            T_NG_PORT(VTSS_TRACE_GRP_MGMT, port_index, "power_requested = %d", poe_conf.max_port_power[port_index]);
            POE_CRIT_EXIT(); // Protect poe_conf
        }

        // Work around because the pd69200 PoE card sets some ports error state when no PD is connected.
        // This is still being investigated.
        if (poe_status.port_status[port_index] == VTSS_APPL_POE_UNKNOWN_STATE) {
            poe_status.power_requested[port_index] = 0;
        }

        // If PoE is disabled for a port then it shall of course request 0 W.
        if (poe_mode_get(port_index) == VTSS_APPL_POE_MODE_DISABLED ||
            poe_custom_is_chip_found(port_index) == NO_POE_CHIPSET_FOUND  ||
            poe_status.port_status[port_index] == VTSS_APPL_POE_NO_PD_DETECTED) {
            poe_status.power_requested[port_index] = 0;
        }

        T_DG_PORT(VTSS_TRACE_GRP_STATUS, port_index, "Port status = %d, Requested Power = %d,",
                  poe_status. port_status[port_index], poe_status.power_requested[port_index]);


        // Change port status if the port is forced off if the power budget is exceeded.
        if (power_budget_exceeded[port_index]) {
            T_DG_PORT(VTSS_TRACE_GRP_STATUS, port_index, "Forcing status to power budget exceeded, %d", power_budget_exceeded.get(port_index));
            poe_status.port_status[port_index] = VTSS_APPL_POE_POWER_BUDGET_EXCEEDED;
        }

        T_DG_PORT(VTSS_TRACE_GRP_STATUS, port_index, "Pd overload, %d", pd_overload.get(port_index));
        if (pd_overload[port_index]) {
            poe_status.port_status[port_index] = VTSS_APPL_POE_PD_OVERLOAD;
        }


        // Update power allocated fields.
        if (poe_status.port_status[port_index] == VTSS_APPL_POE_PD_ON) {
            poe_status.power_allocated[port_index] =  poe_find_allocated_power(poe_status.power_requested[port_index], port_index);
        } else {
            poe_status.power_allocated[port_index] =  0;
        }
        poe_status.pd_class[port_index] = classes[port_index]; // Set the class field (used by Web).

        T_DG_PORT(VTSS_TRACE_GRP_STATUS, port_index, "Class = %d, power allocated = %d, power requested = %d",
                  classes[port_index], poe_status.power_allocated[port_index], poe_status.power_requested[port_index]);

        // Signal to LLDP module to transmit LLDP information as soon as possible.
#ifdef VTSS_SW_OPTION_LLDP
        if (poe_custom_new_pd_detected_get(port_index, FALSE)) {
            (void) lldp_poe_fast_start(port_index, FALSE);
        }
#endif
        poe_status.total_power_used     += poe_status.power_used[port_index];
        poe_status.total_current_used   += poe_status.current_used[port_index];
        poe_status.total_power_reserved += poe_status.power_requested[port_index];
        T_NG_PORT(VTSS_TRACE_GRP_STATUS, port_index, "Total power:%d, total_current:%d, total reserved:%d",
                  poe_status.total_power_used, poe_status.total_current_used, poe_status.total_power_reserved);

    }

    // Set the color for PoE status led
    vtss_appl_poe_status_led_set(poe_led);

    poe_status_set_get(&poe_status, FALSE); // Update the status
}

//
// Debug functions for debugging i2c issues
//
static BOOL poe_halt_at_i2c_err_v = FALSE;
void poe_halt_at_i2c_err_ena(void)
{
    poe_halt_at_i2c_err_v = TRUE;
}

void poe_halt_at_i2c_err(BOOL i2c_err)
{
    if (i2c_err && poe_halt_at_i2c_err_v) {
        T_E("Halting due to i2c error");
        POE_CRIT_ENTER();
        while (1) {
            // Loop forever in order to capture the last i2c access with a probe.
        }
    }
}
/*************************************************************************
** Message functions
*************************************************************************/

/* Update configuration */
static void poe_update_conf(poe_conf_t *new_conf)
{
    // Tell each port to retry to turn on the PD at once, else the user is wondering why it takes some time from
    // he has given the command to it is executed.
    POE_CRIT_ENTER();
    for (mesa_port_no_t port_index = 0 ; port_index < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); port_index++ ) {
        retry_cnt[port_index] = 0;
    }
    POE_CRIT_EXIT();
    // update_registers(new_conf); // Update the Chipset with the new configuration.
}

// Function that enables/disables power for a PD depending upon power budget and pd_overload.
// In : None (power_budget_exceeded and pd_overload are global variables)
// Return : None.
static void poe_update_pd_state(const poe_status_t *poe_status)
{
    mesa_port_no_t port_index;
    poe_conf_t configuration;

    // Only update the PD state when PoE initialization is done.
    if (poe_init_done) {
        for (port_index = 0 ; port_index < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); port_index++ ) {
            POE_CRIT_ENTER(); // Protect poe_conf
            configuration = poe_conf;
            POE_CRIT_EXIT(); // Protect poe_conf
            T_IG_PORT(VTSS_TRACE_GRP_MGMT, port_index, "power_budget_exceeded:%d, pd_overload:%d, status:%d",
                      power_budget_exceeded.get(port_index), pd_overload.get(port_index), poe_status->port_status[port_index]);

            // Turn of in case power is exceeded. We keep the PD off until it is detected in order to make sure that
            // power budget calculation is done before powering on a PD.
            if (power_budget_exceeded[port_index] || pd_overload[port_index]) {
                poe_custom_port_enable(port_index, FALSE, configuration.poe_mode[port_index]); // Turn Off the PD
                T_IG_PORT(VTSS_TRACE_GRP_MGMT, port_index, "poe_mode:%d", configuration.poe_mode[port_index]);
            } else {
                if (sched_active[port_index]) {
                    // Skip update, update from poe_schedule_update
                    continue;
                }
                poe_custom_port_enable(port_index,
                                       configuration.poe_mode[port_index] != VTSS_APPL_POE_MODE_DISABLED,
                                       configuration.poe_mode[port_index]); // Turn On the PD (unless PoE is disabled for this port)
                T_IG_PORT(VTSS_TRACE_GRP_MGMT, port_index, "poe_mode:%d", configuration.poe_mode[port_index]);
            }
        }
    }
}

/*********************************************************************
** Power management
*********************************************************************/

// Function that sorts a port list in order according to the ports priority
// Port with highest priority is first in the list.
static void poe_sort_after_prio(int *port_list)
{
    int port_index;
    POE_CRIT_ENTER();
    for (port_index = 0 ; port_index < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); port_index++ ) {
        if (poe_conf.priority[port_index] == VTSS_APPL_POE_PORT_POWER_PRIORITY_CRITICAL) {
            *port_list = port_index;
            port_list ++;
        }
    }

    for (port_index = 0 ; port_index < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); port_index++ ) {
        if (poe_conf.priority[port_index] == VTSS_APPL_POE_PORT_POWER_PRIORITY_HIGH) {
            *port_list = port_index;
            port_list ++;
        }
    }

    for (port_index = 0 ; port_index < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); port_index++ ) {
        if (poe_conf.priority[port_index] == VTSS_APPL_POE_PORT_POWER_PRIORITY_LOW) {
            *port_list = port_index;
            port_list ++;
        }
    }

    POE_CRIT_EXIT();
}


// Function for doing power management of the individual ports.
static void poe_port_power_mgmt (const poe_status_t *poe_status)
{
    mesa_port_no_t port_index;

    for (port_index = 0; port_index < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); port_index ++ ) {
        if (retry_cnt[port_index] == 0) {
            int power_used = poe_status->power_used[port_index];
            T_DG_PORT(VTSS_TRACE_GRP_MGMT, port_index, "Power used= %u, Power allocated= %d",
                      power_used, poe_status->power_allocated[port_index]);

            // If the port doesn't have a PD connected there is no need to check it.
            if (poe_status->port_status[port_index] != VTSS_APPL_POE_NO_PD_DETECTED && (poe_mode_get(port_index) != VTSS_APPL_POE_MODE_DISABLED)) {
                // Turn port off in case that the allocated power exceeds the  max power set by user.
                if (power_used > poe_status->power_allocated[port_index]) {
                    if (poe_mode_get(port_index) != VTSS_APPL_POE_MODE_DISABLED) {
                        pd_overload[port_index]  = TRUE;
                        retry_cnt[port_index] = rand() % retry_cnt_max; // Set random time before "reenabling" poe
                    }
                } else {
                    pd_overload[port_index]  = FALSE;
                }
            } else {
                pd_overload[port_index]  = FALSE;
            }
        }
    }
}


// Function for doing power management
static void poe_power_mgmt (poe_mgmt_mode_t mode)
{
    poe_status_t poe_status;
    poe_status_set_get(&poe_status, TRUE);
    T_DG(VTSS_TRACE_GRP_MGMT, "Enter poe_power_mgmt, mode :%d", mode);
    CapArray<int, MESA_CAP_PORT_CNT> port_list;
    mesa_port_list_t port_on;

    POE_CRIT_ENTER();
    if (!poe_init_done) {
        POE_CRIT_EXIT();
        return;
    }
    POE_CRIT_EXIT();

    int total_power = 0;
    int power       = 0;
    int  port_list_index, port_index; // Must be signed because -1 is used to exit a for loop.

    poe_sort_after_prio(&port_list[0]); // Sort the port list with highest priority first.

    ushort power_source_value;
    POE_CRIT_ENTER(); // Protect poe_conf
    if (poe_mgmt_get_power_source() == PRIMARY) {
        power_source_value = poe_conf.primary_power_supply - poe_conf.system_power_reserve;
    } else {
        power_source_value = poe_conf.backup_power_supply - poe_conf.system_power_reserve;
    }
    POE_CRIT_EXIT(); // Protect poe_conf

    // Turn on ports starting with the highest priority, until the total power exceeds the power suppply
    for (port_list_index = 0; port_list_index < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); port_list_index ++ ) {
        port_index = port_list[port_list_index]; // Get port with highest priority.

        // Make sure that we don't get out of bounds (Should never happen).
        if (port_index >= MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT)) {
            T_E("port index out of range - %d",  port_index);
            continue;
        }

        // No need to go on if the port doesn't support PoE
        if (poe_status.port_status[port_index] == VTSS_APPL_POE_NOT_SUPPORTED) {
            continue;
        }

        T_DG_PORT(VTSS_TRACE_GRP_MGMT, (mesa_port_no_t)port_index, "poe_status.port_status =%d, power_source_value:%d",
                  poe_status.port_status[port_index], power_source_value);

        if ((mode == REQUESTED && poe_status.port_status[port_index] != VTSS_APPL_POE_NO_PD_DETECTED ) ||
            (mode == ACTUAL && poe_status.port_status[port_index] == VTSS_APPL_POE_PD_ON)) {

            if (mode == ACTUAL) {
                power = poe_status.power_used[port_index];
            } else {
                power = poe_status.power_requested[port_index];
            }

            total_power += power; // Add the request power to the power budget.

            // Determine is more power is requested than the power supply can deliever.
            // Multiply with 10 because PD's power is in deciwatts while power supply is in watts
            if (total_power > (power_source_value * 10)) {
                port_on[port_index] = FALSE; // Turn port off to lower the power requested
                total_power -= power;    // Ok - Port turn off. Don't use the reuqested power for this port.
                T_DG_PORT(VTSS_TRACE_GRP_MGMT, (mesa_port_no_t)port_index, "Total_power = %u,power = %u, port off", total_power, power);
            } else {
                port_on[port_index] = TRUE;
                T_DG_PORT(VTSS_TRACE_GRP_MGMT, (mesa_port_no_t)port_index, "Total_power = %u, port on", total_power);
            }

        } else {
            port_on[port_index] = TRUE;
        }

        // Count down retry counter for determine when to try to turn on the PoE for the port agian.
        if (retry_cnt[port_index] > 0 && retry_cnt[port_index] <= retry_cnt_max) {
            retry_cnt[port_index] --;
            T_DG_PORT(VTSS_TRACE_GRP_MGMT, (mesa_port_no_t)port_index, "retry_cnt = %d", retry_cnt[port_index]);
        } else {
            retry_cnt[port_index] = 0;
        }

        if (port_on[port_index]) {
            // The port was turned off because it used to much power last time it was turned on.
            // Wait until retry counter has counted down before turning on the port again.
            if (retry_cnt[port_index] == 0) {
                T_DG_PORT(VTSS_TRACE_GRP_MGMT, (mesa_port_no_t)port_index, "Turned on, power allocated:", poe_status.power_allocated[port_index]);
                if (poe_status.power_allocated[port_index] > 0 ) {
                    poe_custom_set_port_max_power(port_index, poe_status.power_allocated[port_index], poe_status.port_status[port_index]);
                }
                POE_CRIT_ENTER();
                power_budget_exceeded[port_index]  = FALSE;
                POE_CRIT_EXIT();
            }
        } else {
            // OK - Port turned off.
            retry_cnt[port_index] = rand() % retry_cnt_max;
            T_DG_PORT(VTSS_TRACE_GRP_MGMT, (mesa_port_no_t)port_index, "Forced off, retry_cnt[port_index]:%d", retry_cnt[port_index]);
            if (poe_mode_get(port_index) != VTSS_APPL_POE_MODE_DISABLED) {
                POE_CRIT_ENTER(); // Protect power_budget_exceeded
                power_budget_exceeded[port_index]  = TRUE;
                POE_CRIT_EXIT();
            }
        }
    }
    poe_update_pd_state(&poe_status); // Update PD enable/disable state
    poe_port_power_mgmt(&poe_status);
}




// Function for updating the hardware with the current configuration.
static void update_registers(poe_conf_t *new_conf)
{
    if (!poe_init_done) {
        return;
    }
    T_R("Update chip registers");
    mesa_port_no_t port_index;
    CapArray<char, MESA_CAP_PORT_CNT> classes;

    static poe_conf_t configuration;

    POE_CRIT_ENTER(); // protect poe_conf
    // Take a local copy for this function, because it in case that the I2C communication fails can take quite a
    // while before we returns from the configuration functions used below. If this case we can run into a thread deadlock
    // if we are still in a critical section.
    configuration = *new_conf;
    POE_CRIT_EXIT();

    T_IG(VTSS_TRACE_GRP_MGMT, "Power supply:%d, System Reserve:%d", configuration.primary_power_supply, configuration.system_power_reserve);
    if (!poe_custom_chip_does_power_management()) {
        //We do the power management
        if (configuration.power_mgmt_mode == VTSS_APPL_POE_LLDPMED_RESERVED ||
            configuration.power_mgmt_mode == VTSS_APPL_POE_CLASS_RESERVED ||
            configuration.power_mgmt_mode == VTSS_APPL_POE_ALLOCATED_RESERVED ) {
            poe_power_mgmt(REQUESTED);
        } else {
            poe_power_mgmt(ACTUAL);
        }
    } else {
        // Enable/Disable PoE
        for (port_index = 0 ; port_index < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); port_index++ ) {
            poe_power_supply_set(port_index, configuration.primary_power_supply - configuration.system_power_reserve,
                                 configuration.backup_power_supply - configuration.system_power_reserve);

            poe_custom_pm(port_index, configuration.power_mgmt_mode); // Update the power management registers
            poe_custom_port_enable(port_index,
                                   poe_mode_get(port_index) != VTSS_APPL_POE_MODE_DISABLED,
                                   configuration.poe_mode[port_index]); // Turn On the PD (unless PoE is disabled for this port)
        }
    }

    if (configuration.power_mgmt_mode == VTSS_APPL_POE_LLDPMED_CONSUMP ||
        configuration.power_mgmt_mode == VTSS_APPL_POE_LLDPMED_RESERVED ||
        configuration.power_mgmt_mode == VTSS_APPL_POE_CLASS_CONSUMP ||
        configuration.power_mgmt_mode == VTSS_APPL_POE_CLASS_RESERVED) {
        poe_custom_get_all_ports_classes(&classes[0]);
    }

    poe_custom_capacitor_detection_set(0, configuration.cap_detect ? TRUE : FALSE);

    // Setup power management for the chip set
    for (port_index = 0 ; port_index < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); port_index++ ) {
        vtss_appl_poe_status_type_t port_status;

        // If the only "faillure" is, that no PD nothing has been plugged into the port, the dynamic modes need to default to max allowed power.
        BOOL no_pd_detected = FALSE;
        poe_custom_port_status_get(port_index, &port_status);
        if (port_status == VTSS_APPL_POE_UNKNOWN_STATE || port_status == VTSS_APPL_POE_NO_PD_DETECTED) {
            no_pd_detected = TRUE;
        }

        // port priority +  port power + Port enable/disable
        T_NG_PORT(VTSS_TRACE_GRP_MGMT, port_index, "prio:%d", configuration.priority[port_index]);
        poe_custom_set_port_priority(port_index, configuration.priority[port_index]);

        if (configuration.power_mgmt_mode == VTSS_APPL_POE_LLDPMED_CONSUMP
            || configuration.power_mgmt_mode == VTSS_APPL_POE_LLDPMED_RESERVED ) {
            if (no_pd_detected) {
                poe_custom_set_port_max_power(port_index, poe_max_power_mode_dependent(port_index, poe_mode_get(port_index)), port_status);
            } else {
#ifdef VTSS_SW_OPTION_LLDP
                if (lldp_remote_lldp_is_info_valid(port_index)) {
                    lldp_u16_t requested_power = lldp_remote_get_requested_power(port_index, poe_mode_get(port_index));
                    T_RG_PORT(VTSS_TRACE_GRP_MGMT, port_index, "Max power set via lldp, power = %d",
                              requested_power);

                    poe_custom_set_port_max_power(port_index, requested_power, port_status);
                } else {
                    poe_custom_set_port_max_power(port_index, class2power(classes[port_index], port_index, poe_mode_get(port_index)), port_status);
                }
#endif // VTSS_SW_OPTION_LLDP
            }
        } else if (configuration.power_mgmt_mode == VTSS_APPL_POE_CLASS_CONSUMP
                   || configuration.power_mgmt_mode == VTSS_APPL_POE_CLASS_RESERVED ) {
            if (no_pd_detected) {
                poe_custom_set_port_max_power(port_index, poe_max_power_mode_dependent(port_index, poe_mode_get(port_index)), port_status);
            } else {
                poe_custom_set_port_max_power(port_index, class2power(classes[port_index], port_index, poe_mode_get(port_index)), port_status);
            }
        } else {
            T_RG_PORT(VTSS_TRACE_GRP_MGMT, port_index, "Max power set, power = %d", configuration.max_port_power[port_index]);
#if 1       // BugFix #1527: Auto pin mode will cause PoE allocation mode to be abnormal
            if (no_pd_detected) {
                poe_custom_set_port_max_power(port_index, poe_max_power_mode_dependent(port_index, poe_mode_get(port_index)), port_status);
            } else {
                poe_custom_set_port_max_power(port_index, configuration.max_port_power[port_index], port_status);
            }
#else
            poe_custom_set_port_max_power(port_index, configuration.max_port_power[port_index]);
#endif
        }
    }
}

/**********************************************************************
** Configuration
**********************************************************************/
static void poe_conf_default_set(void)
{
    uint              port_index;

    T_RG(VTSS_TRACE_GRP_CONF, "Entering");

    POE_CRIT_ENTER();

    // Set default configuration
    T_NG(VTSS_TRACE_GRP_CONF, "Restoring to default value");

    // First reset whole structure...
    vtss_clear(poe_conf);

    // Set the individual fields that need to be something other than 0
    poe_conf.primary_power_supply = vtss_appl_poe_supply_default_get();
    poe_conf.system_power_reserve = VTSS_APPL_POE_SYSTEM_RESERVE_POWER_DEFAULT;
    poe_conf.backup_power_supply = vtss_appl_poe_supply_default_get();
    poe_conf.power_mgmt_mode = POE_MGMT_MODE_DEFAULT;

    // Set default PoE for all ports
    for (port_index = 0; port_index < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); port_index++) {
        poe_conf.max_port_power[port_index] = POE_MAX_POWER_DEFAULT;
        poe_conf.poe_mode[port_index] = POE_MODE_DEFAULT;
        poe_conf.priority[port_index] = POE_PRIORITY_DEFAULT;
    }

    POE_CRIT_EXIT();
}


// Function for getting which PoE chipset is found from outside this file (In order to do semaphore protection)
poe_chipset_t poe_is_chip_found(mesa_port_no_t port_index)
{
    poe_chipset_t chipset;
    chipset = poe_custom_is_chip_found(port_index);
    return chipset;
}

// Return TRUE if any PoE chipset is found.
BOOL poe_is_any_chip_found(void)
{
    for (auto i = 0; i < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); i++) {
        if (poe_is_chip_found(i) != NO_POE_CHIPSET_FOUND) {
            return TRUE;
        }
    }
    return FALSE;
}


/**********************************************************************
** LNTN PoE
**********************************************************************/

static unsigned short checksum(void *addr, int count)
{
    int sum = 0;
    unsigned short *source = (unsigned short *) addr;

    while (count > 1)  {
        sum += *source++;
        count -= 2;
    }

    if (count > 0) {
        unsigned short tmp = 0;
        *(unsigned char *) (&tmp) = * (unsigned char *) source;
        sum += tmp;
    }

    while (sum >> 16) {
        sum = (sum & 0xffff) + (sum >> 16);
    }

    return ~sum;
}

typedef enum {
    POE_PING_STATUS_ALIVE,
    POE_PING_STATUS_SOCKET_TIMEOUT,
    POE_PING_STATUS_SOCKET_ERROR,
    POE_PING_STATUS_ICMP_PACKAGE_ERROR,
    POE_PING_STATUS_ICMP_TYPE_ERROR,
    POE_PING_STATUS_ICMP_ID_ERROR,
    POE_PING_STATUS_MAX
} poe_ping_status_t;

/* Ping alive internal function */
static poe_ping_status_t ping_alive_socket_unpack(char *packet, unsigned int packet_len, int pid)
{
    unsigned int iphdrlen;
    struct ip *ip;
    struct icmp *icmp;

    T_RG(VTSS_TRACE_GRP_PING, "Entering ping_alive_socket_unpack");

    ip = (struct ip *)packet;
    iphdrlen = ip->ip_hl << 2;
    icmp = (struct icmp *)(packet + iphdrlen);

    if ((packet_len -= iphdrlen) < 8) {
        return POE_PING_STATUS_ICMP_PACKAGE_ERROR;
    }
    if (icmp->icmp_type != ICMP_ECHOREPLY) {
        return POE_PING_STATUS_ICMP_TYPE_ERROR;
    }
    if (icmp->icmp_id != pid) {
        return POE_PING_STATUS_ICMP_ID_ERROR;
    }

    return POE_PING_STATUS_ALIVE;
}

static poe_ping_status_t ping_alive_check_status(int fd, int pid)
{
    char packet[POE_PING_PACKET_SIZE];
    int ret = 0;
    fd_set set;
    struct sockaddr_in sktaddr;
    struct timeval timeout = {0 ,1000};  // 1000 Microseconds
    unsigned int packet_len;
    socklen_t sktaddr_len;

    T_RG(VTSS_TRACE_GRP_PING, "Entering ping_alive_check_status");

    FD_ZERO(&set);
    FD_SET(fd, &set);
    fcntl(fd, F_SETFL, O_NONBLOCK);
    sktaddr_len = sizeof(struct sockaddr_in);

    if ((ret = select(fd + 1, &set, NULL, NULL, &timeout)) > 0) {
        if (FD_ISSET(fd, &set) ) {
            if((packet_len = recvfrom(fd, packet, sizeof(packet), 0, ((struct sockaddr *)(&sktaddr)), &sktaddr_len))) {
                return ping_alive_socket_unpack(packet, packet_len, pid);
            }
        }
    }

    if (ret == 0) {
        T_DG(VTSS_TRACE_GRP_PING, "POE_PING_STATUS_SOCKET_TIMEOUT");
        return POE_PING_STATUS_SOCKET_TIMEOUT;
    }

    T_DG(VTSS_TRACE_GRP_PING, "POE_PING_STATUS_SOCKET_ERROR");
    return POE_PING_STATUS_SOCKET_ERROR;
}

static int ping_alive_socket_init(int protocol)
{
    int socket_fd = 0;
    int buf_size = 50*1024;

    T_RG(VTSS_TRACE_GRP_PING, "Entering ping_alive_socket_init");

    socket_fd = socket(PF_INET, SOCK_RAW, protocol);
    if (socket_fd < 0) {
        T_DG(VTSS_TRACE_GRP_PING, "socket_fd < 0");
        return -1;
    }

    if (setsockopt(socket_fd, SOL_SOCKET, SO_RCVBUF, &buf_size, sizeof(buf_size)) != 0) {
        T_DG(VTSS_TRACE_GRP_PING, "Set RCVBUF error");
        return -2;
    }
    if (fcntl(socket_fd, F_SETFL, O_NONBLOCK) != 0) {
        T_DG(VTSS_TRACE_GRP_PING, "Request nonblocking I/O");
        return -3;
    }

    return socket_fd;
}

static bool ping_alive_socket_send(int socket_fd, struct sockaddr_in *addr, int pid)
{
    struct icmp icmphdr;

    T_RG(VTSS_TRACE_GRP_PING, "Entering ping_alive_socket_send");

    memset(&icmphdr, 0, sizeof(icmphdr));
    icmphdr.icmp_type = ICMP_ECHO;
    icmphdr.icmp_id = pid;
    icmphdr.icmp_seq = 1;
    icmphdr.icmp_code = 0;
    icmphdr.icmp_cksum = checksum(&icmphdr, sizeof(icmphdr));

    if (sendto(socket_fd, &icmphdr, sizeof(icmphdr), 0, (struct sockaddr*)addr, sizeof(*addr)) <= 0 ) {
        T_DG(VTSS_TRACE_GRP_PING, "sendto error[%s]", strerror(errno));
        return FALSE;
    }

    return TRUE;
}

static bool poe_ping_test(i8 *dest_host)
{
    int socket_fd = 0;
    int ping_cnt = 3;
    int pid = -1;
    poe_ping_status_t       alive = POE_PING_STATUS_SOCKET_ERROR;
    struct sockaddr_in      ping_addr;
    struct protoent *proto = NULL;

    pid = getpid();
    proto = getprotobyname("ICMP");

    memset(&ping_addr, 0, sizeof(ping_addr));
    ping_addr.sin_family = AF_INET;
    ping_addr.sin_port = 0;
    ping_addr.sin_addr.s_addr = inet_addr(dest_host);

    T_IG(VTSS_TRACE_GRP_PING, "dest_host[%s]", dest_host);

    if ((socket_fd = ping_alive_socket_init(proto->p_proto)) < 0) {
        T_IG(VTSS_TRACE_GRP_PING, "ping_alive_socket_init error[%d]", socket_fd);
    }

    for (; ping_cnt > 0; ping_cnt--) {
        if (!(ping_alive_socket_send(socket_fd, &ping_addr, pid))) {
            continue;
        }

        alive = ping_alive_check_status(socket_fd, pid);
        if (alive == POE_PING_STATUS_ALIVE) {
            break;
        }
    }

    if (alive == POE_PING_STATUS_ALIVE) {
        T_IG(VTSS_TRACE_GRP_PING, "alive");
        return TRUE;
    } else {
        T_IG(VTSS_TRACE_GRP_PING, "Ping fail[%d]", alive);
        return FALSE;
    }

    close(socket_fd);
}

static void poe_mode_restart(uint port_index, vtss_appl_poe_port_mode_t last_mode)
{
    /* Restart PoE Mode */
    // 1). Disable PoE
    // 2). Wait for 3 second
    // 3). Restart PoE with last mode
    poe_custom_port_enable(port_index, FALSE, VTSS_APPL_POE_MODE_DISABLED);
    VTSS_OS_MSLEEP(3000); //3s
    poe_custom_port_enable(port_index, TRUE, last_mode);
}

static void ping_alive_timer_start(uint port_index)
{
    vtss::Timer *timer = &ping_alive_timer_local[port_index];

    T_DG(VTSS_TRACE_GRP_PING, "port_index[%d], interval[%d]", port_index, ping_alive_local.interval[port_index]);
    timer->set_period(vtss::seconds(ping_alive_local.interval[port_index]));
    timer->set_repeat(TRUE);

    if (vtss_timer_start(timer) != VTSS_RC_OK) {
        T_DG(VTSS_TRACE_GRP_PING, "Unable to start timer for port %u", port_index + 1);
    }
}

static void ping_alive_timer_stop(uint port_index)
{
    vtss::Timer *timer = &ping_alive_timer_local[port_index];

    if (vtss_timer_cancel(timer) != VTSS_RC_OK) {
        T_DG(VTSS_TRACE_GRP_PING, "Unable to stop timer for port %u", port_index + 1);
    }
}

static void ping_alive_timer_cb(vtss::Timer *timer)
{
    T_RG(VTSS_TRACE_GRP_PING, "Entering Ping Alive Callback");
    int port_index = (int)timer->user_data;
    i8 dest_host[VTSS_APPL_SYSUTIL_INPUT_DOMAIN_NAME_LEN];
    vtss_appl_poe_status_type_t port_status;

    memset(dest_host, 0, sizeof(dest_host));

    (void) icli_ipv4_to_str(ping_alive_local.address[port_index], dest_host);
    T_DG(VTSS_TRACE_GRP_PING, "Port[%d], Ping alive address %s.", port_index ,dest_host);

    // Update the port_status fields
    poe_custom_port_status_get(port_index, &port_status);
    /* Check List  1). PoE Mode != Disable,
                   2). PoE Status == PD ON,
                   3). Ping Alive State == Enable,
                   4). ing Alive Address != 0.0.0.0
    */
    T_DG(VTSS_TRACE_GRP_PING, "poe_mode[%d], port_status[%d], state[%d]",
        poe_conf.poe_mode[port_index], port_status, ping_alive_local.state[port_index]);
    if (poe_conf.poe_mode[port_index] != VTSS_APPL_POE_MODE_DISABLED
        && port_status == VTSS_APPL_POE_PD_ON
        && ping_alive_local.address[port_index] != PING_ALIVE_ADDRESS_DEFAULT
        && ping_alive_local.state[port_index]) {

        if (poe_ping_test(dest_host) == FALSE) {
            T_IG(VTSS_TRACE_GRP_PING, "Ping %s get fail, restart poe.", dest_host);
            poe_mode_restart(port_index, poe_conf.poe_mode[port_index]);
        }
    }
}

/* PoE schedule internal function */
static void get_current_time(time_t time, struct tm **current_time)
{
    if(time >= 0) {
    int       tz_off;
#ifdef VTSS_SW_OPTION_SYSUTIL
        tz_off = system_get_tz_off();
        time += (tz_off * 60);  /* Adjust for TZ minutes => seconds */
#ifdef VTSS_SW_OPTION_DAYLIGHT_SAVING
        time += (time_dst_get_offset() * 60); /* Correct for DST */
#endif  // VTSS_SW_OPTION_DAYLIGHT_SAVING
            *current_time = localtime(&time);
#else   // VTSS_SW_OPTION_SYSUTIL
            *current_time = localtime(&time);
#endif  // VTSS_SW_OPTION_SYSUTIL
        }
}

static void get_sched_time(uint sched_id, u8 current_day, icli_time_t *start, icli_time_t *end)
{
    start->hour = sched_time_local.sched_time[sched_id - 1].start_hour[current_day];
    start->min  = sched_time_local.sched_time[sched_id - 1].start_min[current_day];
    end->hour   = sched_time_local.sched_time[sched_id - 1].end_hour[current_day];
    end->min    = sched_time_local.sched_time[sched_id - 1].end_min[current_day];
}

static bool poe_schedule_need_update(uint port_index)
{
    vtss_appl_poe_status_type_t poe_status;
    uint sched_id = sched_port_local.sched_id[port_index];

    poe_custom_port_status_get(port_index, &poe_status);

    // Specification definition
    if (poe_conf.poe_mode[port_index] == VTSS_APPL_POE_MODE_DISABLED) {
        return false;
    }

    // Check schedule update conditions
    // 1. PoE port has setting schedule id
    // 2. PoE port has setting schedule mode
    // 3. Schedule id has been set
    if (sched_id != POE_SCHEDULE_ID_DEFAULT &&
        sched_port_local.mode[port_index] != SCHEDULE_DISABLE &&
        sched_time_local.sched_time[sched_id - 1].status != POE_SCHEDULE_STATE_DEFAULT) {
        return true;
    }

    return false;
}

static void poe_schedule_update(void)
{
    uint port_index, sched_id;
    //Current Time
    u8  current_day = 0;  // days since Sunday - [0..6]
    u32 current_time = 0; //sec
    struct tm *current = NULL;
    //Schedule Time
    icli_time_t start, end;
    u32 sched_start = 0, sched_end = 0; //sec
    sched_mode_t sched_mode;
    vtss_appl_poe_port_mode_t poe_mode;

    get_current_time(time(NULL), &current);
    current_day  = current->tm_wday;
    current_time = ((current->tm_hour*3600) + (current->tm_min*60) + current->tm_sec);

    for (port_index = 0; port_index < MESA_CAP(MESA_CAP_PORT_CNT); port_index++) {

        if (poe_conf.poe_mode[port_index] != VTSS_APPL_POE_MODE_DISABLED &&
            poe_schedule_need_update(port_index)) {
            sched_active[port_index] = true;
        } else {
            sched_active[port_index] = false;
            continue;   // Skip update, update from poe_update_pd_state
        }

        sched_mode  = sched_port_local.mode[port_index];
        sched_id    = sched_port_local.sched_id[port_index];

        memset(&start, 0, sizeof(icli_time_t));
        memset(&end  , 0, sizeof(icli_time_t));
        get_sched_time(sched_id, current_day, &start, &end);

        sched_start = (start.hour * 3600 + start.min * 60);
        sched_end   = (end.hour   * 3600 + end.min   * 60);

        if ((sched_start <= current_time) && (current_time <= sched_end)) {
            switch(sched_mode) {
                case SCHEDULE_ON:
                    poe_mode = poe_conf.poe_mode[port_index];
                    poe_custom_port_enable(port_index, TRUE, poe_mode); //power on
                    break;
                case SCHEDULE_OFF:
                    poe_mode = VTSS_APPL_POE_MODE_DISABLED;
                    poe_custom_port_enable(port_index, FALSE, poe_mode); //power off
                    break;
                case SCHEDULE_DISABLE:
                    break;
            }
        } else {
            switch(sched_mode) {
                case SCHEDULE_ON:
                    poe_mode = VTSS_APPL_POE_MODE_DISABLED;
                    poe_custom_port_enable(port_index, FALSE, poe_mode); //power off
                    break;
                case SCHEDULE_OFF:
                    poe_mode = poe_conf.poe_mode[port_index];
                    poe_custom_port_enable(port_index, TRUE, poe_mode); //power on
                    break;
                case SCHEDULE_DISABLE:
                    break;
            }
        }
    }
}

/* Lntn PoE default setting */
static void lntn_poe_default_set(void)
{
    uint              port_index;

    T_RG(VTSS_TRACE_GRP_CONF, "Entering LNTN PoE default setting");

    // Set default configuration
    T_NG(VTSS_TRACE_GRP_CONF, "Restoring to LNTN PoE default value");

    PING_ALIVE_CRIT_ENTER();
    // First reset whole structure...
    memset(&ping_alive_local, 0, sizeof(ping_alive_local_t));

    for (port_index = 0; port_index < MESA_CAP(MESA_CAP_PORT_CNT); port_index++) {

        ping_alive_local.interval[port_index] = PING_ALIVE_INTERVAL_DEFAULT;

        //Timer Init
        vtss::Timer *timer = &ping_alive_timer_local[port_index];
        timer->callback    = ping_alive_timer_cb;
        timer->modid       = VTSS_MODULE_ID_POE;
        timer->user_data   = (void *)(port_index);
        ping_alive_timer_stop(port_index);
    }
    PING_ALIVE_CRIT_EXIT();

    POE_SCHED_CRIT_ENTER();
    memset(&sched_port_local, 0, sizeof(sched_port_local_t));
    memset(&sched_time_local, 0, sizeof(sched_time_local_t));
    memset(&sched_active,     0, sizeof(sched_active));
    POE_SCHED_CRIT_EXIT();

    T_RG(VTSS_TRACE_GRP_CONF, "Entering LNTN PoE default setting");
}

void ping_alive_get_local_config(ping_alive_local_t *conf)
{
    PING_ALIVE_CRIT_ENTER();
    *conf = ping_alive_local;
    PING_ALIVE_CRIT_EXIT();
}

void ping_alive_set_local_config(ping_alive_local_t *new_conf)
{
    uint    port_index;

    PING_ALIVE_CRIT_ENTER();
    ping_alive_local = *new_conf;
    PING_ALIVE_CRIT_EXIT();

    //Check the ping alive state and handle timer
    for (port_index = 0; port_index < MESA_CAP(MESA_CAP_PORT_CNT); port_index++) {
        T_DG(VTSS_TRACE_GRP_PING, "port_index[%d], ping_alive_local.state[%d], ping_alive_local.changed[%d]"
            , port_index, ping_alive_local.state[port_index], ping_alive_local.changed[port_index]);
        if (ping_alive_local.state[port_index] == TRUE
            && ping_alive_local.changed[port_index] == TRUE) {
            ping_alive_timer_start(port_index);

            PING_ALIVE_CRIT_ENTER();
            ping_alive_local.changed[port_index] = 0;
            PING_ALIVE_CRIT_EXIT();
        } else if (ping_alive_local.state[port_index] == FALSE
                    && ping_alive_local.changed[port_index] == TRUE){
            ping_alive_timer_stop(port_index);

            PING_ALIVE_CRIT_ENTER();
            ping_alive_local.changed[port_index] = 0;
            PING_ALIVE_CRIT_EXIT();
        }
    }
}

void sched_port_get_local_config(sched_port_local_t *conf)
{
    POE_SCHED_CRIT_ENTER();
    *conf = sched_port_local;
    POE_SCHED_CRIT_EXIT();
}

void sched_port_set_local_config(sched_port_local_t *new_conf)
{
    POE_SCHED_CRIT_ENTER();
    sched_port_local = *new_conf;
    POE_SCHED_CRIT_EXIT();
}

void sched_time_get_local_config(sched_time_local_t *conf)
{
    POE_SCHED_CRIT_ENTER();
    *conf = sched_time_local;
    POE_SCHED_CRIT_EXIT();
}

void sched_time_set_local_config(sched_time_local_t *new_conf)
{
    POE_SCHED_CRIT_ENTER();
    sched_time_local = *new_conf;
    POE_SCHED_CRIT_EXIT();
}


/**********************************************************************
** Thread
**********************************************************************/

static vtss_handle_t poe_thread_handle;
static vtss_thread_t poe_thread_block;
static vtss::Lock    thread_lock;

static void poe_thread(vtss_addrword_t data)
{
    /* Do the board-initialize, and wait 2 seconds to allow for the the PD69200 and the i2c bus to get up and running.
     * For some reason, not doing it here (or in the port module), but only in main/vtss_api_if.cxx makes the PD69200
     * communication fail, unless there is an SFP-module inserted during boot.
     */
    meba_reset(board_instance, MEBA_BOARD_INITIALIZE);
    VTSS_OS_MSLEEP(2000);

    poe_custom_init(); // Detecting PoE chip set. Has to be called even on PoE less boards to initialise the PoE crit.
    if (!MESA_CAP(MEBA_CAP_POE)) {
        T_I("No PoE Board feature");
        return; // Stop thread if no PoE chipsets is found.
    }

#if 0  // 20180329 Harvey mark, Leonton has no specification
    T_I("Firmware upgrade");
    int dummy;
    mesa_rc rc;
    for (auto iport = 0; iport < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); iport++) {
        rc = poe_do_firmware_upgrade(iport, NULL, dummy, TRUE);

        // Note - Firmware is only upgraded in the case that it doesn't match the built in version
        if ( rc != VTSS_RC_OK && rc != VTSS_APPL_POE_ERROR_FIRMWARE_VER_NOT_NEW) {
            T_E("%s", error_txt(rc));
        }
    }
#endif

    while (!switch_add) {
        T_I("Waiting for switch add");
        VTSS_OS_MSLEEP(1000);
    }

    T_I("Entering poe_thread");

    if (!poe_is_any_chip_found()) {
        poe_init_done = TRUE; // Signal that PoE chipset has been initialized.
        return; // Stop thread if no PoE chipsets is found.
    }

    for (;;) {
        thread_lock.wait();
        // Do thread stuff
        poe_custom_vee_update();
        update_registers(&poe_conf);
        poe_status_update();

        POE_CRIT_ENTER();
        poe_init_done = TRUE; // Signal that PoE chipset has been initialized.
        POE_CRIT_EXIT();

        POE_SCHED_CRIT_ENTER();
        poe_schedule_update();
        POE_SCHED_CRIT_EXIT();
        VTSS_OS_MSLEEP(500); // Allow other threads to get access
    }
}

/**********************************************************************
** Wrapper-methods for hardware-access.
**********************************************************************/
poe_entry_t poe_hw_config_get(mesa_port_no_t port_idx, poe_entry_t *hw_conf)
{
    poe_custom_entry_t poe_hw_conf;
    poe_hw_conf = poe_custom_get_hw_config(port_idx, &poe_hw_conf);

    hw_conf->available = poe_hw_conf.available;
    hw_conf->pse_pairs_control_ability = poe_hw_conf.pse_pairs_control_ability;
    hw_conf->pse_power_pair = poe_hw_conf.pse_power_pair;
    return *hw_conf;
}

void poe_pse_data_get(mesa_port_no_t port_index, poe_pse_data_t *pse_data)
{
    poe_custom_pse_data_get(port_index, pse_data);
}

void poe_pd_data_set(mesa_port_no_t port_index, poe_pd_data_t *pd_data)
{
    poe_custom_pd_data_set(port_index, pd_data);
}

BOOL poe_new_pd_detected_get(mesa_port_no_t port_index, BOOL clear)
{
    return poe_custom_new_pd_detected_get(port_index, clear);
}

u16  poe_port_max_power_get(mesa_port_no_t port_index)
{
    return poe_custom_get_port_power_max(port_index);
}

char *poe_firmware_info_get(mesa_port_no_t port_index, char *info)
{
    return poe_custom_firmware_info_get(port_index, info);
}

/**********************************************************************
** Management functions
**********************************************************************/
// Returns TRUE if the PoE chips are detected and initialized, else FALSE.
BOOL is_poe_ready(void)
{
    T_NG(VTSS_TRACE_GRP_STATUS, "poe_init_done:%d", poe_init_done);
    return poe_init_done;
}

// Debug function for getting the PoE chipset firmware
char *poe_mgmt_firmware_info_get(mesa_port_no_t port_index, char *firmware_string)
{
    firmware_string = poe_custom_firmware_info_get(port_index, firmware_string);
    return firmware_string;
}

// Debug function for setting legacy capacitor detection (Critical region protected) (***NO stacking support)
// In : port_index : Port number starting from 0
//      enable : TRUE to enable legacy capacitor detection
void poe_mgmt_capacitor_detection_set(mesa_port_no_t port_index, BOOL enable)
{
    poe_custom_capacitor_detection_set(port_index, enable);
}

// Debug function for getting legacy capacitor detection (Critical region protected) (***NO stacking support)
// In : port_index : Port number starting from 0
BOOL poe_mgmt_capacitor_detection_get(mesa_port_no_t port_index)
{
    BOOL enable;
    enable = poe_custom_capacitor_detection_get(port_index);
    return enable;
}


// Reading PoE chipset register (Critical region protected) (***NO stacking support)
void poe_mgmt_reg_rd(mesa_port_no_t port_index, u16 addr, u16 *data)
{
    poe_custom_rd(port_index, addr, data);
}

// Write PoE chipset register (Critical region protected) (***NO stacking support)
void poe_mgmt_reg_wr(mesa_port_no_t port_index, u16 addr, u16 data)
{
    poe_custom_wr(port_index, addr, data);
}

BOOL poe_mgmt_is_backup_power_supported(void)
{
    return poe_custom_is_backup_power_supported();
}

poe_power_source_t poe_mgmt_get_power_source(void)
{
    return poe_custom_get_power_source();
}

// Function for getting if PoE chipset is found (Critical region protected)
void poe_mgmt_is_chip_found(poe_chipset_t *chip_found)
{
    poe_status_t local_poe_status;
    poe_mgmt_get_status(&local_poe_status);
    memcpy(chip_found, local_poe_status.poe_chip_found.data(), local_poe_status.poe_chip_found.mem_size());
}

// Checking if max power per port is OK and that they don't exceed the maximum power that the power supply can provide.
static mesa_rc poe_is_max_power_ok(poe_conf_t *new_conf)
{
    mesa_port_no_t port_index;
    // u16 total_power = 0;
    CapArray<poe_chipset_t, MESA_CAP_PORT_CNT> poe_chip_found;
    poe_mgmt_is_chip_found(&poe_chip_found[0]);

    for (port_index = 0 ; port_index < MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT); port_index++ ) {
        if (new_conf->poe_mode[port_index] == VTSS_APPL_POE_MODE_POE &&  new_conf->max_port_power[port_index] > 154 ) {
            new_conf->max_port_power[port_index] = 154; // Auto adjust to maximum allowed power for standard mode.
            T_IG_PORT(VTSS_TRACE_GRP_CONF, port_index, "%s", "Maximum port power must not exceed 15.4W when in mode is set to PoE");
        }

#if 1  // BugFix, #1528: [PoE]Don't need this condition
    }
#else
        if (poe_chip_found[port_index] != NO_POE_CHIPSET_FOUND) {
            total_power += new_conf->max_port_power[port_index];
            T_IG_PORT(VTSS_TRACE_GRP_CONF, port_index, "max power:%d, total_power:%d", new_conf->max_port_power[port_index], total_power);
        }
    }

    T_IG(VTSS_TRACE_GRP_CONF, "total_power:%d, mode:%d, power_supply:%d, power_mgmt_mode:%d", total_power, poe_conf.power_mgmt_mode, new_conf->primary_power_supply, new_conf->power_mgmt_mode);
    if (total_power > (new_conf->primary_power_supply * 10)) {
        T_IG(VTSS_TRACE_GRP_CONF, "Exceeded");
        return VTSS_APPL_POE_ERROR_PORT_POWER_EXCEEDED;
    }
#endif

    return VTSS_RC_OK;
}

// Function that returns the current configuration.
void poe_config_get(poe_conf_t *conf)
{
    T_NG(VTSS_TRACE_GRP_CONF, "Getting local conf");
    POE_CRIT_ENTER();
    *conf = poe_conf; // Return this switch configuration
    POE_CRIT_EXIT();
}

// Function that can set the current configuration.
mesa_rc poe_config_set(poe_conf_t *new_conf)
{
    T_DG(VTSS_TRACE_GRP_CONF, "Setting local conf");

    // Check new configuration
    VTSS_RC(poe_is_max_power_ok(new_conf));

    // Update new configuration, update the switch in question
    POE_CRIT_ENTER();
    poe_conf = *new_conf;
    POE_CRIT_EXIT();
    poe_update_conf(new_conf);
    return VTSS_RC_OK;
}

// Getting status for mangement
void poe_mgmt_get_status(poe_status_t *local_poe_status)
{
    if (!poe_init_done) {
        vtss_clear(*local_poe_status);
        return;
    }
    poe_status_set_get(local_poe_status, TRUE);
}

/**
 * Set PoE global configuration.
 * conf  [IN]: PoE global configurable parameters
 * return VTSS_OK if the operation succeeded.
 */
mesa_rc vtss_appl_poe_global_conf_set(const vtss_appl_poe_global_conf_t  *const conf)
{
    poe_conf_t tmp_poe_conf;

    if (conf == NULL) {
        T_I("Input parameter is NULL");
        return VTSS_APPL_POE_ERROR_NULL_POINTER;
    }

    // Get current conf
    poe_config_get(&tmp_poe_conf);

    // Update fields
    tmp_poe_conf.power_mgmt_mode = conf->mgmt_mode;

    // Set conf
    poe_config_set(&tmp_poe_conf);
    return VTSS_RC_OK;
}
/**
 * Get PoE global configuration..
 * conf  [OUT]: PoE global configurable parameters
 * return VTSS_OK if the operation succeeded.
 */
mesa_rc vtss_appl_poe_global_conf_get(vtss_appl_poe_global_conf_t  *const conf)
{
    poe_conf_t tmp_poe_conf;
    if (conf == NULL) {
        T_I("Input parameter is NULL");
        return VTSS_APPL_POE_ERROR_NULL_POINTER;
    }
    //Get config
    poe_config_get(&tmp_poe_conf);

    // Update fields
    conf->mgmt_mode = tmp_poe_conf.power_mgmt_mode;
    return VTSS_RC_OK;
}

mesa_rc vtss_appl_poe_conf_set(vtss_usid_t usid, const vtss_appl_poe_conf_t  *const conf)
{
    poe_conf_t tmp_poe_conf;

    if ((conf == NULL) || (usid != VTSS_USID_START)) {
        T_I("Input parameter is NULL");
        return VTSS_APPL_POE_ERROR_NULL_POINTER;
    }

    // Get current conf
    poe_config_get(&tmp_poe_conf);

    // Update fields
    tmp_poe_conf.primary_power_supply = conf->primary_power_supply;
    tmp_poe_conf.cap_detect = (conf->capacitor_detect ? POE_CAP_DETECT_ENABLED : POE_CAP_DETECT_DISABLED);

    // Set conf
    poe_config_set(&tmp_poe_conf);
    return VTSS_RC_OK;
}

mesa_rc vtss_appl_poe_conf_get(vtss_usid_t usid, vtss_appl_poe_conf_t *const conf)
{
    poe_conf_t tmp_poe_conf;
    if ((conf == NULL) || (usid != VTSS_USID_START)) {
        T_I("Input parameter is NULL");
        return VTSS_APPL_POE_ERROR_NULL_POINTER;
    }
    //Get config
    poe_config_get(&tmp_poe_conf);

    // Update fields
    conf->primary_power_supply = tmp_poe_conf.primary_power_supply;
    conf->capacitor_detect = tmp_poe_conf.cap_detect;
    return VTSS_RC_OK;
}

/**
 * Set PoE port configuration.
 * ifIndex  [IN]: Interface index
 * conf     [IN]: PoE port configurable parameters
 * return VTSS_OK if the operation succeeded.
 */
mesa_rc vtss_appl_poe_port_conf_set(vtss_ifindex_t                   ifIndex,
                                    const vtss_appl_poe_port_conf_t  *const conf)
{
    CapArray<poe_chipset_t, MESA_CAP_PORT_CNT> poe_chip_found;
    vtss_ifindex_elm_t  ife;
    poe_conf_t          tmp_conf;

    if (conf == NULL) {
        T_I("Input parameter is NULL");
        return VTSS_APPL_POE_ERROR_NULL_POINTER;
    }
    VTSS_RC(vtss_appl_ifindex_port_configurable(ifIndex, &ife))
    if (ife.isid != VTSS_ISID_START) {
        return VTSS_APPL_POE_ERROR_NOT_SUPPORTED;
    }
    poe_mgmt_is_chip_found(&poe_chip_found[0]);

    poe_config_get(&tmp_conf);

    if (poe_chip_found[ife.ordinal] != NO_POE_CHIPSET_FOUND) {
        if ((conf->mode == VTSS_APPL_POE_MODE_POE) && (conf->max_port_power > 154 || conf->max_port_power < 0)) {
            T_I("maximum port power must not exceed 154 deciwatt when mode is set to PoE (ifIndex %u)", VTSS_IFINDEX_PRINTF_ARG(ifIndex));
            return VTSS_APPL_POE_ERROR_PORT_POWER_EXCEEDED;
        }
        if ((conf->mode == VTSS_APPL_POE_MODE_POE_PLUS) && (conf->max_port_power > 300 || conf->max_port_power < 0)) {
            T_I("maximum port power must not exceed 300 deciwatt when mode is set to PoE plus (ifIndex %u)", VTSS_IFINDEX_PRINTF_ARG(ifIndex));
            return VTSS_APPL_POE_ERROR_PORT_POWER_EXCEEDED;
        }
        tmp_conf.poe_mode[ife.ordinal]       = conf->mode;
        tmp_conf.priority[ife.ordinal]       = conf->priority;
        tmp_conf.max_port_power[ife.ordinal] = conf->max_port_power;
        //Set configuration
        VTSS_RC(poe_config_set(&tmp_conf));
    } else {
        T_D("Interface: %u does not have PoE support", ife.ordinal);
        return VTSS_APPL_POE_ERROR_NOT_SUPPORTED;
    }
    return VTSS_RC_OK;
}
/**
 * Get PoE port configuration.
 * ifIndex   [IN]: Interface index
 * conf     [OUT]: PoE port configurable parameters
 * return VTSS_OK if the operation succeeded.
 */
mesa_rc vtss_appl_poe_port_conf_get(vtss_ifindex_t            ifIndex,
                                    vtss_appl_poe_port_conf_t *const conf)
{
    CapArray<poe_chipset_t, MESA_CAP_PORT_CNT> poe_chip_found;
    vtss_ifindex_elm_t  ife;
    poe_conf_t          tmp_conf;

    if (conf == NULL) {
        T_I("Input parameter is NULL");
        return VTSS_APPL_POE_ERROR_NULL_POINTER;
    }
    VTSS_RC(vtss_appl_ifindex_port_configurable(ifIndex, &ife))
    if (ife.isid != VTSS_ISID_START) {
        return VTSS_APPL_POE_ERROR_NOT_SUPPORTED;
    }

    poe_mgmt_is_chip_found(&poe_chip_found[0]);

    if (poe_chip_found[ife.ordinal] != NO_POE_CHIPSET_FOUND) {
        poe_config_get(&tmp_conf);
        conf->mode           = tmp_conf.poe_mode[ife.ordinal];
        conf->priority       = tmp_conf.priority[ife.ordinal];
        conf->max_port_power = tmp_conf.max_port_power[ife.ordinal];
    } else {
        T_D("Interface: %u does not have PoE support", ife.ordinal);
        return VTSS_APPL_POE_ERROR_NOT_SUPPORTED;
    }
    return VTSS_RC_OK;
}
/**
 * Get PoE port status
 * ifIndex     [IN]: Interface index
 * status     [OUT]: Port status
 * return VTSS_OK if the operation succeeded.
 */
mesa_rc vtss_appl_poe_port_status_get(vtss_ifindex_t              ifIndex,
                                      vtss_appl_poe_port_status_t *const status)
{
    poe_status_t        local_status;
    CapArray<poe_chipset_t, MESA_CAP_PORT_CNT> poe_chip_found;
    vtss_ifindex_elm_t  ife;
    vtss_appl_poe_status_type_t   port_status;

    /* Check illegal parameters */
    if (status == NULL) {
        T_I("Input parameter is NULL");
        return VTSS_APPL_POE_ERROR_NULL_POINTER;
    }
    VTSS_RC(vtss_appl_ifindex_port_configurable(ifIndex, &ife))
    if (ife.isid != VTSS_ISID_START) {
        return VTSS_APPL_POE_ERROR_NOT_SUPPORTED;
    }
    poe_mgmt_get_status(&local_status);

    poe_mgmt_is_chip_found(&poe_chip_found[0]);

    if (poe_chip_found[ife.ordinal] != NO_POE_CHIPSET_FOUND) {
        status->power_consume      = local_status.power_used[ife.ordinal];
        status->current_consume    = local_status.current_used[ife.ordinal];
        status->power_requested    = local_status.power_requested[ife.ordinal];
        port_status             = local_status.port_status[ife.ordinal];
        status->port_status     = port_status;

        if (port_status != VTSS_APPL_POE_PD_ON
            || local_status.pd_class[ife.ordinal] > 4) { // Only classes 0-4 is defined in the standard
            status->pd_class        = POE_UNDETERMINED_CLASS;
        } else {
            status->pd_class        = local_status.pd_class[ife.ordinal];
        }
    } else {
        T_D("Interface: %u does not have PoE support", ife.ordinal);
        return VTSS_APPL_POE_ERROR_NOT_SUPPORTED;
    }
    return VTSS_RC_OK;
}
/**
 * Get PoE port capabilities
 * ifIndex       [IN]: Interface index
 * capabilities [OUT]: PoE platform specific port capabilities
 * return VTSS_OK if the operation succeeded.
 */
mesa_rc vtss_appl_poe_port_capabilities_get(vtss_ifindex_t                    ifIndex,
                                            vtss_appl_poe_port_capabilities_t *const capabilities)
{
    vtss_ifindex_elm_t  ife;
    CapArray<poe_chipset_t, MESA_CAP_PORT_CNT> poe_chip_found;

    /* Check illegal parameters */
    if (capabilities == NULL) {
        T_I("Input parameter is NULL");
        return VTSS_APPL_POE_ERROR_NULL_POINTER;
    }
    VTSS_RC(vtss_appl_ifindex_port_configurable(ifIndex, &ife))
    if (ife.isid != VTSS_ISID_START) {
        return VTSS_APPL_POE_ERROR_NOT_SUPPORTED;
    }
    poe_mgmt_is_chip_found(&poe_chip_found[0]);

    if (poe_chip_found[ife.ordinal] == NO_POE_CHIPSET_FOUND) {
        capabilities->poe_capable = false;
    } else {
        capabilities->poe_capable = true;
    }
    return VTSS_RC_OK;
}

/**
 * \brief Function for getting the current power supply status
 *
 * \param pwr_in_status [IN,OUT] The status of the power supply.
 *
 * \return VTSS_OK if the operation succeeded.
 **/
mesa_rc vtss_appl_poe_powerin_status_get(vtss_appl_poe_powerin_status_t *const powerin_status)
{
    poe_status_t local_status;
    poe_custom_powerin_status_get(powerin_status);

    poe_mgmt_get_status(&local_status);
    powerin_status->total_current_used   = local_status.total_current_used;
    powerin_status->total_power_used     = local_status.total_power_used;
    powerin_status->total_power_reserved = local_status.total_power_reserved;
    return VTSS_RC_OK;
}

// Function converting the port status to a printable string.
const char *poe_status2str(vtss_appl_poe_status_type_t status, mesa_port_no_t port_index, poe_conf_t *conf)
{
    if (!poe_init_done) {
        return "Detecting PoE chipset";
    } else if (status ==  VTSS_APPL_POE_NOT_SUPPORTED) {
        return "PoE not available - No PoE chip found";
    } else if (conf->poe_mode[port_index] == VTSS_APPL_POE_MODE_DISABLED) {
        return "PoE turned OFF - PoE disabled";
    } else {
        T_NG_PORT(VTSS_TRACE_GRP_STATUS, port_index, "port_status:%d", status);
        switch (status) {
        case VTSS_APPL_POE_DISABLED :
            return "PoE turned OFF - PoE disabled";

        case VTSS_APPL_POE_POWER_BUDGET_EXCEEDED:
            return "PoE turned OFF - Power budget exceeded";

        case VTSS_APPL_POE_UNKNOWN_STATE:
        case VTSS_APPL_POE_NO_PD_DETECTED:
            return "No PD detected";

        case VTSS_APPL_POE_PD_ON:
            return "PoE turned ON";

        case VTSS_APPL_POE_PD_OVERLOAD:
            return "PoE turned OFF - PD overload";

        case VTSS_APPL_POE_PD_OFF:
            return "PoE turned OFF";

        case VTSS_APPL_POE_DISABLED_INTERFACE_SHUTDOWN:
            return "PoE turned OFF - Interface shutdown";

        default :
            // This should never happen.
            T_I("Unknown PoE status:%d", status);
            return "PoE state unknown";
        }
    }
}

// Function converting the class to a printable string.
char *poe_class2str(const poe_status_t *status, mesa_port_no_t port_index, char *class_str)
{
    T_DG_PORT(VTSS_TRACE_GRP_STATUS, port_index, "port_status:%d", status->port_status[port_index]);
    if (status->port_status[port_index] != VTSS_APPL_POE_PD_ON ||
        status->pd_class[port_index] < 0 ||
        status->pd_class[port_index] > 4) { // Only classes 0-4 is defined in the standard.
        strcpy(&class_str[0], "-");
    } else {
        sprintf(class_str, "%d", status->pd_class[port_index]);
    }

    return class_str;
}

/****************************************************************************/
/*  Initialization functions                                                */
/****************************************************************************/
#ifdef VTSS_SW_OPTION_PRIVATE_MIB
/* Initialize private mib */
VTSS_PRE_DECLS void poe_mib_init(void);
#endif
#ifdef VTSS_SW_OPTION_JSON_RPC
VTSS_PRE_DECLS void vtss_appl_poe_json_init(void);
#endif
extern "C" int poe_icli_cmd_register();

/* Initialize module */
mesa_rc poe_init(vtss_init_data_t *data)
{
    switch (data->cmd) {
    case INIT_CMD_EARLY_INIT:
        /* Initialize and register trace resources */
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, VTSS_TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);
        break;

    case INIT_CMD_INIT:
        T_I("INIT_CMD_INIT");

        critd_init(&crit, "PoE crit", VTSS_MODULE_ID_POE, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
#ifdef VTSS_SW_OPTION_PRIVATE_MIB
        /* Register private mib */
        poe_mib_init();
#endif
#ifdef VTSS_SW_OPTION_JSON_RPC
        vtss_appl_poe_json_init();
#endif
        poe_icli_cmd_register();
#ifdef VTSS_SW_OPTION_WEB
        if (!MESA_CAP(MEBA_CAP_POE)) {
            /* Hide POE pages if we are not POE-capable */
            web_page_disable("poe_menu");
            web_page_disable("poe_status.htm");
            match_disable_in_json("configuration_poe");
            match_disable_in_json("poe_status");
#if defined VTSS_SW_OPTION_LLDP
            web_page_disable("lldp_poe_neighbors.htm");
            match_disable_in_json("lldp_poe_neighbors");
#endif  /* VTSS_SW_OPTION_LLDP */
        }
#endif /* VTSS_SW_OPTION_WEB */

        POE_CRIT_EXIT();
        critd_init(&crit_status, "PoE crit status", VTSS_MODULE_ID_POE, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
        POE_CRIT_STATUS_EXIT();

        critd_init(&crit_ping_alive, "PoE Ping Alive crit", VTSS_MODULE_ID_POE, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
        PING_ALIVE_CRIT_EXIT();

        critd_init(&crit_poe_sched, "PoE Schedule crit", VTSS_MODULE_ID_POE, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
        POE_SCHED_CRIT_EXIT();

        poe_conf_default_set();
        lntn_poe_default_set();
#ifdef VTSS_SW_OPTION_ICFG
        if (poe_icfg_init() != VTSS_RC_OK) {
            T_E("ICFG not initialized correctly");
        }
#endif

        thread_lock.lock(false);
        vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
                           poe_thread,
                           0,
                           "POE",
                           nullptr,
                           0,
                           &poe_thread_handle,
                           &poe_thread_block);

        T_I("INIT_CMD_INIT");
        break;
    case INIT_CMD_START:
        T_I("INIT_CMD_START");
        T_I("INIT_CMD_START LEAVE");
        break;
    case INIT_CMD_CONF_DEF:
        T_I("RESTORE TO DEFAULT");
        poe_conf_default_set(); // Restore to default.
        lntn_poe_default_set();
        poe_update_conf(&poe_conf);
        T_I("INIT_CMD_CONF_DEF LEAVE");
        break;
    case INIT_CMD_MASTER_UP:
        T_I("MASTER UP");
        break;
    case INIT_CMD_MASTER_DOWN:
        T_I("MASTER DOWN");
        break;
    case INIT_CMD_SWITCH_ADD:
        switch_add = TRUE;
        T_I("INIT_CMD_SWITCH_ADD");
        poe_update_conf(&poe_conf);
        T_I("INIT_CMD_SWITCH_ADD POE DONE");
        break;
    case INIT_CMD_SWITCH_DEL:
        T_I("INIT_CMD_SWITCH_DEL");
        break;
    case INIT_CMD_SUSPEND_RESUME:
        POE_CRIT_ENTER();
        thread_lock.lock(!data->resume);
        POE_CRIT_EXIT();
        break;

    default:
        break;
    }

    return 0;

}

u32 vtss_appl_poe_get_device_max_budget(void)
{
    int pwrmaxbgt = 0;

    if (conf_mgmt_pwrmaxbgt_get(&pwrmaxbgt) == 0) {
        return pwrmaxbgt;
    } else {
        return 0;
    }
}

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
