/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _SYSLOG_H_
#define _SYSLOG_H_

#include <time.h>
#include "critd_api.h"
#include "vtss/appl/syslog.h"
#include "syslog_api.h"

/* ================================================================= *
 *  syslog configuration blocks
 * ================================================================= */

/* Block versions
   1: Initial version
   2: Added syslog server */
#define SYSLOG_CONF_BLK_VERSION     2

/* syslog configuration block */
typedef struct {
    u32                            version;   /* Block version */
    vtss_appl_syslog_server_conf_t conf;      /* syslog configuration */
} syslog_conf_blk_t;

#define SYSLOG_FLASH_HDR_COOKIE  0x474F4C53   /* Cookie 'SLOG'(for "Syslog") */
#define SYSLOG_FLASH_HDR_VERSION 1            /* Configuration version number */

#define SYSLOG_FLASH_ENTRY_COOKIE 0x59544E53  /* Cookie 'SNTY' (for "Syslog Entry") */
#define SYSLOG_FLASH_ENTRY_VERSION 1          /* Entry version number */

#define SYSLOG_FLASH_UNINIT_FLASH_VALUE_LONG 0xFFFFFFFF

/****************************************************************************/
/****************************************************************************/
typedef struct {
    u32 size;    // Header size in bytes
    u32 cookie;  // Header cookie
    u32 version; // Header version number
} SL_flash_hdr_t;

/****************************************************************************/
/****************************************************************************/
typedef struct {
    u32                    size;    // Entry size in bytes
    u32                    cookie;  // Entry cookie
    u32                    version; // Entry version number
    time_t                 time;    // Time of saving.
    syslog_cat_t           cat;     // Category (either of the SYSLOG_CAT_xxx constants)
    vtss_appl_syslog_lvl_t lvl;     // Level (either of the SYSLOG_LVL_xxx constants)
    // Here follows the NULL-terminated message
} SL_flash_entry_t;

/*---- RAM System Log ------------------------------------------------------*/

/* Stack message IDs */
typedef enum {
    SL_MSG_ID_ENTRY_GET_REQ,    /* Get entry request */
    SL_MSG_ID_ENTRY_GET_REP,    /* Get entry reply */
    SL_MSG_ID_STAT_GET_REQ,     /* Get statistics request */
    SL_MSG_ID_STAT_GET_REP,     /* Get statistics reply */
    SL_MSG_ID_CLR_REQ,          /* Clear request (no reply) */
    SL_MSG_ID_CONF_SET_REQ      /* syslog configuration set request (no reply) */
} SL_msg_id_t;

/* Stack request message */
typedef struct {
    SL_msg_id_t msg_id; /* Message ID */
    union {
        /* SL_MSG_ID_ENTRY_GET_REQ */
        struct {
            BOOL                    next;
            ulong                   id;
            vtss_appl_syslog_lvl_t  lvl;
            vtss_module_id_t        mid;
        } entry_get;

        /* SL_MSG_ID_STAT_GET_REQ: No data */

        /* SL_MSG_ID_CLR_REQ */
        struct {
            vtss_appl_syslog_lvl_t lvl;
        } entry_clear;

        /* SL_MSG_ID_CONF_SET_REQ */
        struct {
            vtss_appl_syslog_server_conf_t conf;
        } conf_set;
    } data;
} SL_msg_req_t;

/* Stack reply message */
typedef struct {
    SL_msg_id_t msg_id; /* Message ID */
    union {
        /* SL_MSG_ID_ENTRY_GET_REP */
        struct {
            BOOL               found;
            syslog_ram_entry_t entry;
        } entry_get;

        /* SL_MSG_ID_STAT_GET_REP */
        struct {
            syslog_ram_stat_t stat;
        } stat_get;
    } data;
} SL_msg_rep_t;

#define SYSLOG_RAM_SIZE (1024*1024)

/* RAM entry */
typedef struct SL_ram_entry_t {
    struct SL_ram_entry_t *next;
    ulong                   id;                         /* Message ID */
    vtss_appl_syslog_lvl_t  lvl;                        /* Level */
    vtss_module_id_t        mid;                        /* Module ID */
    time_t                  time;                       /* Time stamp */
    char                    msg[SYSLOG_RAM_MSG_MAX];    /* Message */
} SL_ram_entry_t;

/* Variables for RAM system log */
typedef struct {
    uchar             log[SYSLOG_RAM_SIZE];
    syslog_ram_stat_t stat[VTSS_ISID_END];
    vtss_flag_t        stat_flags;
    SL_ram_entry_t    *first;       /* First entry in list */
    SL_ram_entry_t    *last;        /* Last entry in list */
    ulong             current_id;   /* current ID */

    /* Request buffer */
    void *request;

    /* Reply buffer */
    void *reply;

    /* Management reply buffer */
    struct {
        vtss_sem_t         sem;
        vtss_flag_t        flags;
        BOOL               found;
        syslog_ram_entry_t *entry;
    } mgmt_reply;
} SL_ram_t;

/* ================================================================= *
 *  syslog global structure
 * ================================================================= */

/* syslog global structure */
typedef struct {
    critd_t                 crit;
    vtss_appl_syslog_server_conf_t conf;
    vtss_flag_t             conf_flags;
    vtss_mtimer_t           conf_timer[VTSS_ISID_END];
    ulong                   send_msg_id[VTSS_ISID_END];     /* Record message ID that already send to syslog server */
    time_t                  current_time;
} syslog_global_t;

/*--------------------------------------------------------------------------*/

#endif /* _SYSLOG_H_ */
