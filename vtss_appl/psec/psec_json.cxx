/*
 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
*/

#include "psec_serializer.hxx"
#include "vtss/basics/expose/json.hxx"
#include "json_rpc_api.hxx"
using namespace vtss;
using namespace vtss::json;
using namespace vtss::expose::json;
using namespace vtss::appl::psec;

namespace vtss {
void json_node_add(Node *node);
}  // namespace vtss

#define NS(N, P, D) static vtss::expose::json::NamespaceNode N(&P, D);
static NamespaceNode PSEC_JSON_node_psec("portSecurity");
extern "C" void vtss_appl_psec_json_init(void) { json_node_add(&PSEC_JSON_node_psec); }

NS(PSEC_JSON_node_config,            PSEC_JSON_node_psec,    "config");
NS(PSEC_JSON_node_status,            PSEC_JSON_node_psec,    "status");
NS(PSEC_JSON_node_status_global,     PSEC_JSON_node_status,  "global");
NS(PSEC_JSON_node_status_interface,  PSEC_JSON_node_status,  "interface");
NS(PSEC_JSON_node_control,           PSEC_JSON_node_psec,    "control");
NS(PSEC_JSON_node_control_global,    PSEC_JSON_node_control, "global");
NS(PSEC_JSON_node_control_interface, PSEC_JSON_node_control, "interface");

namespace vtss {
namespace appl {
namespace psec {

static StructReadOnly            <Capabilities>                capabilities                 (&PSEC_JSON_node_psec,              "capabilities");
static StructReadWrite           <ConfigGlobal>                config_global                (&PSEC_JSON_node_config,            "global");
static TableReadWrite            <ConfigInterface>             config_interface             (&PSEC_JSON_node_config,            "interface");
static StructReadOnly            <StatusGlobal>                status_global                (&PSEC_JSON_node_status_global,     "main");
static StructReadOnlyNotification<StatusGlobalNotification>    status_global_notification   (&PSEC_JSON_node_status_global,     "notification", &psec_global_notification_status);
static TableReadOnlyNoNS         <StatusInterface>             status_interface             (&PSEC_JSON_node_status_interface);
static TableReadOnlyNotification <StatusInterfaceNotification> status_interface_notification(&PSEC_JSON_node_status_interface,  "notification", &psec_interface_notification_status);
static TableReadOnly             <StatusInterfaceMac>          status_interface_mac         (&PSEC_JSON_node_status_interface,  "mac");
static StructWriteOnly           <ControlGlobalMacClear>       control_global_mac_clear     (&PSEC_JSON_node_control_global,    "mac_clear");

}  // namespace psec
}  // namespace appl
}  // namespace vtss

