/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _VTSS_DDMI_H_
#define _VTSS_DDMI_H_

/* ================================================================= *
 *  Trace definitions
 * ================================================================= */

#include "ddmi_api.h"
#include "vtss_module_id.h"

#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>

#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_DDMI

#define VTSS_TRACE_GRP_DEFAULT      0
#define TRACE_GRP_CRIT              1
#define TRACE_GRP_CNT               2

#include <vtss_trace_api.h>

/* ================================================================= *
 *  DDMI configuration blocks
 * ================================================================= */

/* Block versions */
#define DDMI_CONF_BLK_VERSION         1
#define DDMI_PORT_CONF_BLK_VERSION    1

/* ================================================================= *
 *  DDMI stack messages
 * ================================================================= */

/* DDMI messages IDs */
typedef enum {
    DDMI_MSG_ID_PORT_STATUS_REQ,    /* DDMI port status get request */
    DDMI_MSG_ID_PORT_STATUS_RSP    /* DDMI port status get reply */
} ddmi_msg_id_t;


/* Request message */
typedef struct {
    /* Message ID */
    ddmi_msg_id_t    msg_id;
    /* Request data, depending on message ID */
    union {
        struct {
            ddmi_port_info_t    ports[MESA_CAP_PORT_CNT];
        } port_info;
    } data;
} ddmi_msg_t;

/* DDMI message buffer */
typedef struct {
    vtss_sem_t *sem; /* Semaphore */
    void       *msg; /* Message */
} ddmi_msg_buf_t;

/* ================================================================= *
 *  DDMI global structure
 * ================================================================= */

/* DDMI global structure */
typedef struct {
    critd_t     crit;
    critd_t     cb_crit;
    vtss_flag_t conf_flags;
    ddmi_conf_t conf;
} ddmi_global_t;

/* ================================================================= *
 *  DDMI registration table
 * ================================================================= */

#define DDMI_STATE_CHANGE_REG_MAX 24

/* Port change registration table */
typedef struct {
    int               count;
    ddmi_state_change_reg_t reg[DDMI_STATE_CHANGE_REG_MAX];
} ddmi_state_change_table_t;


#endif /* _VTSS_DDMI_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
