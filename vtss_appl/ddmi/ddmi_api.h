/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _DDMI_API_H_
#define _DDMI_API_H_

#include "mscc/ethernet/switch/api.h" /* For mesa_rc, mesa_vid_t, etc. */
#include "port_api.h"
#include "vtss/appl/ddmi.h"

/**
 * \file ddmi_api.h
 * \brief This file defines the APIs for the DDMI module
 */
#ifdef __cplusplus
extern "C" {
#endif

/**
 * DDMI management enabled/disabled
 */
#define DDMI_MGMT_ENABLED         (1)       /**< Enable option  */
#define DDMI_MGMT_DISABLED        (0)       /**< Disable option */


/**
 * Default DDMI configuration
 */
#define DDMI_MGMT_DEFAULT_MODE                DDMI_MGMT_ENABLED
#define DDMI_MGMT_DEFAULT_PORT_MODE           DDMI_MGMT_DISABLED

#define DDMI_CAP_A0_SUPPORTED          1
#define DDMI_CAP_A2_SUPPORTED          2
#define DDMI_CAP_SFP_DETECTED          4
/**
 * \brief API Error Return Codes (mesa_rc)
 */
enum {
    DDMI_ERROR_MUST_BE_MASTER = MODULE_ERROR_START(VTSS_MODULE_ID_DDMI),    /**< Operation is only allowed on the master switch.                  */
    DDMI_ERROR_ISID,                                                              /**< isid parameter is invalid.                                       */
    DDMI_ERROR_INV_PARAM,                                                         /**< Invalid parameter.                                               */
    DDMI_ERROR_READ_FAIL,
    DDMI_ERROR_I2C_BUSY,
    DDMI_ERROR_TIMEOUT,
};

/**
 * \brief DDMI port configuration.
 */
typedef struct {
    BOOL    port_mode[MESA_CAP_PORT_CNT];       /**< Enable auto detect mode or configure manual.   */
} ddmi_port_conf_t;

typedef struct {
    i8  vendor_name[VTSS_APPL_DDMI_STR_MAX_LEN + 1]; /* 20-35 */
    i8  vendor_pn[VTSS_APPL_DDMI_STR_MAX_LEN + 1];   /* 40-55 */
    i8  vendor_sn[VTSS_APPL_DDMI_STR_MAX_LEN + 1];   /* 68-83 */
    i8  vendor_rev[VTSS_APPL_DDMI_STR_MAX_LEN + 1];  /* 56-59 */
    i8  date_code[VTSS_APPL_DDMI_STR_MAX_LEN + 1];   /* 84-91 */
    vtss_appl_sfp_tranceiver_t  mode;                /* 3-13  */
    u8  type;                                        /* 92    */
} ddmi_a0_t;

typedef struct {
    i8  temp_hi_alarm[VTSS_APPL_DDMI_STR_MAX_LEN + 1];      /* 00-01 */
    i8  temp_lo_alarm[VTSS_APPL_DDMI_STR_MAX_LEN + 1];      /* 02-03 */
    i8  temp_hi_warn[VTSS_APPL_DDMI_STR_MAX_LEN + 1];       /* 04-05 */
    i8  temp_lo_warn[VTSS_APPL_DDMI_STR_MAX_LEN + 1];       /* 06-07 */
    i8  voltage_hi_alarm[VTSS_APPL_DDMI_STR_MAX_LEN + 1];   /* 08-09 */
    i8  voltage_lo_alarm[VTSS_APPL_DDMI_STR_MAX_LEN + 1];   /* 10-11 */
    i8  voltage_hi_warn[VTSS_APPL_DDMI_STR_MAX_LEN + 1];    /* 12-13 */
    i8  voltage_lo_warn[VTSS_APPL_DDMI_STR_MAX_LEN + 1];    /* 14-15 */
    i8  bias_hi_alarm[VTSS_APPL_DDMI_STR_MAX_LEN + 1];      /* 16-17 */
    i8  bias_lo_alarm[VTSS_APPL_DDMI_STR_MAX_LEN + 1];      /* 18-19 */
    i8  bias_hi_warn[VTSS_APPL_DDMI_STR_MAX_LEN + 1];       /* 20-21 */
    i8  bias_lo_warn[VTSS_APPL_DDMI_STR_MAX_LEN + 1];       /* 22-23 */
    i8  tx_pwr_hi_alarm[VTSS_APPL_DDMI_STR_MAX_LEN + 1];    /* 24-25 */
    i8  tx_pwr_lo_alarm[VTSS_APPL_DDMI_STR_MAX_LEN + 1];    /* 26-27 */
    i8  tx_pwr_hi_warn[VTSS_APPL_DDMI_STR_MAX_LEN + 1];     /* 28-29 */
    i8  tx_pwr_lo_warn[VTSS_APPL_DDMI_STR_MAX_LEN + 1];     /* 30-31 */
    i8  rx_pwr_hi_alarm[VTSS_APPL_DDMI_STR_MAX_LEN + 1];    /* 32-33 */
    i8  rx_pwr_lo_alarm[VTSS_APPL_DDMI_STR_MAX_LEN + 1];    /* 34-35 */
    i8  rx_pwr_hi_warn[VTSS_APPL_DDMI_STR_MAX_LEN + 1];     /* 36-37 */
    i8  rx_pwr_lo_warn[VTSS_APPL_DDMI_STR_MAX_LEN + 1];     /* 38-39 */
    i8  temp_current[VTSS_APPL_DDMI_STR_MAX_LEN + 1];       /* 96-97 */
    i8  voltage_current[VTSS_APPL_DDMI_STR_MAX_LEN + 1];    /* 98-99 */
    i8  bias_current[VTSS_APPL_DDMI_STR_MAX_LEN + 1];       /* 100-101 */
    i8  tx_pwr_current[VTSS_APPL_DDMI_STR_MAX_LEN + 1];     /* 102-103 */
    i8  rx_pwr_current[VTSS_APPL_DDMI_STR_MAX_LEN + 1];     /* 104-105 */
} ddmi_a2_t;

typedef struct {
    u32 mode;       /* DDMI global mode */
} ddmi_conf_t;

/**
 * \brief DDMI Information.
 */
typedef struct {
    u8          cap;
    ddmi_a0_t   ddmi_a0;
    ddmi_a2_t   ddmi_a2;
} ddmi_port_info_t;

/* DDMI state change callback */
typedef void (*ddmi_state_change_callback_t)(mesa_port_no_t port_no, vtss_appl_ddmi_event_state_t state[]);

/* DDMI state change callback registration */
mesa_rc ddmi_state_change_register(vtss_module_id_t module_id, ddmi_state_change_callback_t callback);

/* DDMI state change registration */
typedef struct {
    ddmi_state_change_callback_t callback;   /* User callback function */
    vtss_module_id_t       module_id;  /* Module ID */
} ddmi_state_change_reg_t;


/**
  * \brief Retrieve an error string based on a return code
  *        from one of the DDMI API functions.
  *
  * \param rc [IN]: Error code that must be in the DDMI_ERROR_ddmi range.
  */
const char *ddmi_error_txt(mesa_rc rc);

const char *ddmi_mode_txt(vtss_appl_sfp_tranceiver_t  mode);
/**
  * \brief Get the global DDMI configuration.
  *
  * \param glbl_cfg [OUT]: Pointer to structure that receives
  *                        the current configuration.
  *
  * \return
  *    VTSS_OK on success.\n
  *    DDMI_ERROR_INV_PARAM if glbl_cfg is NULL.\n
  *    DDMI_ERROR_MUST_BE_MASTER if called on a slave switch.\n
  */
mesa_rc ddmi_port_info_get(vtss_isid_t isid, mesa_port_no_t port_no, ddmi_port_info_t *info);

/**
  * \brief Initialize the DDMI module
  *
  * \param data [IN]: Initial data point.
  *
  * \return
  *    VTSS_OK.
  */
mesa_rc ddmi_init(vtss_init_data_t *data);

void ddmi_mgmt_conf_get_default(ddmi_conf_t *conf);
mesa_rc ddmi_mgmt_conf_get(ddmi_conf_t *glbl_cfg);
mesa_rc ddmi_mgmt_conf_set(ddmi_conf_t *glbl_cfg);
void ddmi_mgmt_conf_get_default(ddmi_conf_t *conf);

mesa_rc debug_ddmi_mgmt_state_set(vtss_isid_t isid, mesa_port_no_t port_no, vtss_appl_ddmi_event_type_t type, vtss_appl_ddmi_event_state_t state);
BOOL ddmi_sfp_support_detected();

#ifdef __cplusplus
}
#endif

#endif /* _DDMI_API_H_ */


// ***************************************************************************
//
//  End of file.
//
// ***************************************************************************
