/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#include "ddmi_serializer.hxx"
#include "vtss/appl/ddmi.h"

vtss_enum_descriptor_t vtss_appl_ddmi_event_type_txt[] {
    {VTSS_APPL_DDMI_EVENT_TYPE_TEMPERATURE,   "temperature"},
    {VTSS_APPL_DDMI_EVENT_TYPE_VOLTAGE,       "voltage"},
    {VTSS_APPL_DDMI_EVENT_TYPE_BIAS,          "bias"},
    {VTSS_APPL_DDMI_EVENT_TYPE_TX_POWER,      "txPower"},
    {VTSS_APPL_DDMI_EVENT_TYPE_RX_POWER,      "rxPower"},
    {0, 0},
};
vtss_enum_descriptor_t vtss_appl_ddmi_event_state_txt[] {
    {VTSS_APPL_DDMI_EVENT_STATE_REGULAR,     "regular"},   
    {VTSS_APPL_DDMI_EVENT_STATE_HI_WARN,     "highWarn"},
    {VTSS_APPL_DDMI_EVENT_STATE_LO_WARN,     "lowWarn"},
    {VTSS_APPL_DDMI_EVENT_STATE_HI_ALARM,    "highAlarm"},
    {VTSS_APPL_DDMI_EVENT_STATE_LO_ALARM,    "lowAlarm"},
    {0, 0},
};


static vtss::expose::snmp::IteratorComposeRange<vtss_appl_ddmi_event_type_t> DDMI_event_type_itr(VTSS_APPL_DDMI_EVENT_TYPE_TEMPERATURE, VTSS_APPL_DDMI_EVENT_TYPE_RX_POWER);

/**
 * \brief Iterator for retrieving DDMI event index.
 *
 * Retrieve the 'next' index of the DDMI event table
 * according to the given 'prev'.
 *
 * \param ifx_prev  [IN]  Pointer of the ifindex of DDMI event table
 * \param type_prev [OUT] Pointer of the next ifindex of DDMI event table
 * \param ifx_next  [IN]  Pointer of the type of DDMI event table
 * \param type_next [OUT] Pointer of the next type of DDMI event table
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_ddmi_event_itr(
    const vtss_ifindex_t                    *const ifx_prev,
    vtss_ifindex_t                          *const ifx_next,
    const vtss_appl_ddmi_event_type_t       *const type_prev,
    vtss_appl_ddmi_event_type_t             *const type_next
)
{
    vtss::IteratorComposeN<vtss_ifindex_t, vtss_appl_ddmi_event_type_t> itr(
        vtss_appl_iterator_ifindex_front_port,
        DDMI_event_type_itr);

    return itr(ifx_prev, ifx_next, type_prev, type_next);
}

/**
 * \brief Get event status
 *
 * \param ifx_idx  [IN]  ifindex of DDMI event table
 * \param type_idx [IN]  type of DDMI event table
 * \param entry    [OUT] event status
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_ddmi_event_get(
    const vtss_ifindex_t  ifx_idx,
    const vtss_appl_ddmi_event_type_t  type_idx,
    vtss_appl_ddmi_event_entry_t        *const entry
)
{
    return ddmi_status_update.get(ifx_idx, type_idx, entry);
}

