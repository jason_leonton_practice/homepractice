/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "main.h"
#include "conf_api.h"
#include "msg_api.h"
#include "critd_api.h"
#include "vtss_api_if_api.h"
#include "misc_api.h"
#include "ddmi_api.h"
#include "ddmi.h"
#if defined(VTSS_SW_OPTION_ICFG)
#include "ddmi_icfg.h"
#endif /* VTSS_SW_OPTION_ICFG */
#if defined(VTSS_SW_OPTION_SYSLOG)
#include "syslog_api.h"
#endif /* VTSS_SW_OPTION_ICFG */
#include "port_api.h"

// For public header
#include "vtss_common_iterator.hxx"
#include "ifIndex_api.h"
#include "vtss/basics/expose/table-status.hxx" // For vtss::expose::TableStatus
#include "vtss/basics/memcmp-operator.hxx"  // For VTSS_BASICS_MEMCMP_OPERATOR

#define VTSS_ALLOC_MODULE_ID VTSS_TRACE_MODULE_ID

/* ================================================================= *
 *  DDMI module private data
 * ================================================================= */

typedef enum {
    DDMI_STATE_REGULAR = VTSS_APPL_DDMI_EVENT_STATE_REGULAR,
    DDMI_STATE_HI_WARN,
    DDMI_STATE_LO_WARN,
    DDMI_STATE_HI_ALARM,
    DDMI_STATE_LO_ALARM,
    DDMI_STATE_END = 0xffffffff
} ddmi_state_t;

typedef struct {
    uint port;                  /* Port_no or aggr_id */
    ddmi_port_info_t info;
    BOOL detected_done;
    ddmi_state_t state[5];
} ddmi_port_state_t;

typedef struct {
    u32 last_refresh;
} ddmi_switch_state_t;

static struct {
    vtss_usid_t usid;           /* My usid */
    u32         port_count;
    ddmi_switch_state_t switches[VTSS_ISID_END];
    ddmi_port_state_t   ports[MESA_CAP_PORT_CNT];
    ddmi_state_change_table_t register_table;
} ddmi_state;

static struct {
    ddmi_port_info_t ports[MESA_CAP_PORT_CNT];
} ddmi_master_state[VTSS_ISID_END];

typedef struct {
    int int_part;
    unsigned int frac_part;
} fraction_t;

#define TEMP_MULTIPLIER 1000
#define VOL_MULTIPLIER  10000
#define BIAS_MULTIPLIER  1000


static char *DDMI_16_bit_signed_2_complement_value_2_str(u8 *input, char *str, size_t str_size, int multiplier, fraction_t *frac_out);
static char *DDMI_16_bit_unsigned_2_complement_value_2_str(u8 *input, char *str, size_t str_size, int multiplier, fraction_t *frac_out);
static char *DDMI_16_bit_unsigned_value_2_str(u8 *input, char *str, size_t str_size, int multiplier, fraction_t *frac_out);
static i8 *temp_cb_2_str(u8 *input, char *str, size_t str_size, int multiplier, fraction_t *frac_out);
static char *txBias_cb_2_str(unsigned char *input, char *str, size_t str_size, int multiplier, fraction_t *frac_out);
static char *DDMI_offset_2_str(u8 *input, char *str, size_t str_size, int multiplier, fraction_t *frac_out);
static char *DDMI_ieee754_2_str(u8 *input, char *str, size_t str_size, int multiplier, fraction_t *frac_out);

typedef char    *(ddmi_cb_t) (u8 *input, char *str, size_t str_size, int multiplier, fraction_t *frac_out);
static struct ddmi_data_s {
    u8      offset;
    u8      len;
    i8      name[24];
    ddmi_cb_t   *ddmi_cb_fn;
} ddmi_table[] = {
    {0,     2,  "Temp High Alarm",              temp_cb_2_str},
    {2,     2,  "Temp Low Alarm",               temp_cb_2_str},
    {4,     2,  "Temp High Warning",            temp_cb_2_str},
    {6,     2,  "Temp Low Warning",             temp_cb_2_str},
    {8,     2,  "Voltage High Alarm",           DDMI_16_bit_unsigned_value_2_str},
    {10,    2,  "Voltage Low Alarm",            DDMI_16_bit_unsigned_value_2_str},
    {12,    2,  "Voltage High Warning",         DDMI_16_bit_unsigned_value_2_str},
    {14,    2,  "Voltage Low Warning",          DDMI_16_bit_unsigned_value_2_str},
    {16,    2,  "Bias High Alarm",              txBias_cb_2_str},
    {18,    2,  "Bias Low Alarm",               txBias_cb_2_str},
    {20,    2,  "Bias High Warning",            txBias_cb_2_str},
    {22,    2,  "Bias Low Warning",             txBias_cb_2_str},
    {24,    2,  "TX Power High Alarm",          DDMI_16_bit_unsigned_value_2_str},
    {26,    2,  "TX Power Low Alarm",           DDMI_16_bit_unsigned_value_2_str},
    {28,    2,  "TX Power High Warning",        DDMI_16_bit_unsigned_value_2_str},
    {30,    2,  "TX Power Low Warning",         DDMI_16_bit_unsigned_value_2_str},
    {32,    2,  "RX Power High Alarm",          DDMI_16_bit_unsigned_value_2_str},
    {34,    2,  "RX Power Low Alarm",           DDMI_16_bit_unsigned_value_2_str},
    {36,    2,  "RX Power High Warning",        DDMI_16_bit_unsigned_value_2_str},
    {38,    2,  "RX Power Low Warning",         DDMI_16_bit_unsigned_value_2_str},
    /*    {40, 16, "Unallocated"}, */
    {56,    4,  "Rx_PWR(4)",                    DDMI_ieee754_2_str},
    {60,    4,  "Rx_PWR(3)",                    DDMI_ieee754_2_str},
    {64,    4,  "Rx_PWR(2)",                    DDMI_ieee754_2_str},
    {68,    4,  "Rx_PWR(1)",                    DDMI_ieee754_2_str},
    {72,    4,  "Rx_PWR(0)",                    DDMI_ieee754_2_str},
    {76,    2,  "Tx_I(Slope)",                  DDMI_16_bit_unsigned_2_complement_value_2_str},
    {78,    2,  "Tx_I(Offset)",                 DDMI_offset_2_str},
    {80,    2,  "Tx_PWR(Slope)",                DDMI_16_bit_unsigned_2_complement_value_2_str},
    {82,    2,  "Tx_PWR(Offset)",               DDMI_offset_2_str},
    {84,    2,  "T(Slope)",                     DDMI_16_bit_unsigned_2_complement_value_2_str},
    {86,    2,  "T(Offset)",                    DDMI_offset_2_str},
    {88,    2,  "V(Slope)",                     DDMI_16_bit_unsigned_2_complement_value_2_str},
    {90,    2,  "V(Offset)",                    DDMI_offset_2_str},
    /* Converted analog values. calibrated 16 bit data. */
    {96,    2,  "Temperature",                  temp_cb_2_str},
    {98,    2,  "Vcc",                          DDMI_16_bit_unsigned_value_2_str},
    {100,   2,  "TX Bias",                      txBias_cb_2_str},
    {102,   2,  "TX Power",                     DDMI_16_bit_unsigned_value_2_str},
    {104,   2,  "RX Power",                     DDMI_16_bit_unsigned_value_2_str},
    /* Reserve Optional Alarm and Warning Flag Bits. */
    {112,   2,  "Alarm Flag Bits",              NULL},
    {116,   2,  "Warning Flag Bits",            NULL},
};


#define DDMI_TABLE_COUNT (sizeof(ddmi_table) / sizeof(struct ddmi_data_s))
/* Thread variables */
static vtss_flag_t ddmi_status_flags;

/* Private global structure */
static ddmi_global_t DDMI_global;

#if (VTSS_TRACE_ENABLED)
static vtss_trace_reg_t DDMI_trace_reg = {
    VTSS_TRACE_MODULE_ID,
    "ddmi",
    "DDMI"
};

static vtss_trace_grp_t DDMI_trace_grps[TRACE_GRP_CNT] = {
    [VTSS_TRACE_GRP_DEFAULT] = {
        "default",
        "Default",
        VTSS_TRACE_LVL_WARNING,
    },
    [TRACE_GRP_CRIT] = {
        "crit",
        "Critical regions",
        VTSS_TRACE_LVL_ERROR,
    }
};
#define DDMI_CRIT_ENTER() critd_enter(&DDMI_global.crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define DDMI_CRIT_EXIT()  critd_exit( &DDMI_global.crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define DDMI_CB_CRIT_ENTER() critd_enter(&DDMI_global.cb_crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define DDMI_CB_CRIT_EXIT()  critd_exit( &DDMI_global.cb_crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define DDMI_CRIT_ASSERT_LOCKED() critd_assert_locked(&DDMI_global.crit,    TRACE_GRP_CRIT,    __FILE__, __LINE__)
#else
#define DDMI_CRIT_ENTER() critd_enter(&DDMI_global.crit)
#define DDMI_CRIT_EXIT()  critd_exit( &DDMI_global.crit)
#define DDMI_CB_CRIT_ENTER() critd_enter(&DDMI_global.cb_crit)
#define DDMI_CB_CRIT_EXIT()  critd_exit( &DDMI_global.cb_crit)
#define DDMI_CRIT_ASSERT_LOCKED() critd_assert_locked(&DDMI_global.crit)
#endif /* VTSS_TRACE_ENABLED */

/* Thread variables */
static vtss_handle_t DDMI_thread_handle;
static vtss_thread_t DDMI_thread_block;

/* JSON notifications  */
VTSS_BASICS_MEMCMP_OPERATOR(vtss_appl_ddmi_event_entry_t);
vtss::expose::TableStatus <
vtss::expose::ParamKey<vtss_ifindex_t >,
     vtss::expose::ParamKey<vtss_appl_ddmi_event_type_t >,
     vtss::expose::ParamVal<vtss_appl_ddmi_event_entry_t *>
     > ddmi_status_update;

/****************************************************************************/
/*  Various local functions                                                 */
/****************************************************************************/

/****************************************************************************/
/*  Stack/switch functions                                                  */
/****************************************************************************/

#if (VTSS_TRACE_LVL_MIN <= VTSS_TRACE_LVL_DEBUG)
/* DDMI error text */
static const char *DDMI_msg_id_txt(ddmi_msg_id_t msg_id)
{
    const char *txt;

    switch (msg_id) {
    case DDMI_MSG_ID_PORT_STATUS_REQ:
        txt = "DDMI_MSG_ID_PORT_STATUS_REQ";
        break;
    case DDMI_MSG_ID_PORT_STATUS_RSP:
        txt = "DDMI_MSG_ID_PORT_STATUS_RSP";
        break;
    default:
        txt = "?";
        break;
    }
    return txt;
}
#endif /* VTSS_TRACE_LVL_DEBUG */

/* Allocate request buffer */
static ddmi_msg_t *ddmi_msg_alloc(ddmi_msg_id_t msg_id)
{
    ddmi_msg_t *msg = (ddmi_msg_t *)VTSS_MALLOC(sizeof(ddmi_msg_t));
    if (msg) {
        msg->msg_id = msg_id;
    }
    T_N("msg type %d => %p", msg_id, msg);
    return msg;
}

static BOOL DDMI_msg_rx(void *contxt, const void *const rx_msg, const size_t len, const vtss_module_id_t modid, const u32 isid)
{
    ddmi_msg_t *msg = (ddmi_msg_t *)rx_msg;
    ddmi_msg_id_t msg_id = *(ddmi_msg_id_t *)rx_msg;

    T_D("msg_id: %d, %s, len: %zd, isid: %d", msg_id, DDMI_msg_id_txt(msg_id), len, isid);

    switch (msg_id) {
    case DDMI_MSG_ID_PORT_STATUS_REQ: {
        ddmi_msg_t *rep = ddmi_msg_alloc(DDMI_MSG_ID_PORT_STATUS_RSP);
        if (rep) {
            port_iter_t pit;
            (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL,
                                  PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
            DDMI_CRIT_ENTER();
            while (port_iter_getnext(&pit)) {
                rep->data.port_info.ports[pit.iport] = ddmi_state.ports[pit.iport].info;
            }
            DDMI_CRIT_EXIT();
            msg_tx(VTSS_MODULE_ID_DDMI, isid, rep, sizeof(*rep));
        } else {
            T_E("msg_alloc failed");
        }
        break;
    }
    case DDMI_MSG_ID_PORT_STATUS_RSP: {
        if (msg_switch_is_master()) {
            port_iter_t pit;
            (void) port_iter_init(&pit, NULL, isid,
                                  PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
            DDMI_CRIT_ENTER();
            while (port_iter_getnext(&pit)) {
                ddmi_master_state[isid].ports[pit.iport] = msg->data.port_info.ports[pit.iport];
            }
            ddmi_state.switches[isid].last_refresh = time(NULL);
            vtss_flag_setbits(&ddmi_status_flags, 1 << isid);
            DDMI_CRIT_EXIT();
        } else {
            T_W("Skipping on non-master");
        }
        break;
    }
    default:
        T_W("unknown message ID: %d", msg_id);
        break;
    }
    return TRUE;
}

static mesa_rc DDMI_stack_register(void)
{
    msg_rx_filter_t filter;

    memset(&filter, 0, sizeof(filter));
    filter.cb = DDMI_msg_rx;
    filter.modid = VTSS_MODULE_ID_DDMI;
    return msg_rx_filter_register(&filter);
}

static void trim_trailing_space(i8 *s)
{
    i8  *c;

    if (strlen(s) == 0) {
        return;
    }

    c = s + strlen(s) - 1;

    while ((*c) == ' ') {
        *c = 0;
        --c;
    }
}

static mesa_rc read_ddmi_a0(mesa_port_no_t port_no, ddmi_port_info_t *port_info)
{
    ddmi_a0_t   a0_info;
    u8          transeiver_code[13];
    u8          date_code[8];
    mesa_rc rc = VTSS_RC_OK;

    memset(&a0_info, 0, sizeof(a0_info));
    memset(transeiver_code, 0, sizeof(transeiver_code));
    memset(date_code, 0, sizeof(date_code));

    if ( meba_sfp_i2c_xfer(board_instance, port_no, false, 0x50, 20, (u8 *)(a0_info.vendor_name), VTSS_APPL_DDMI_STR_MAX_LEN, FALSE) != VTSS_RC_OK ||
         meba_sfp_i2c_xfer(board_instance, port_no, false, 0x50, 40, (u8 *)(a0_info.vendor_pn), VTSS_APPL_DDMI_STR_MAX_LEN, FALSE) != VTSS_RC_OK ||
         meba_sfp_i2c_xfer(board_instance, port_no, false, 0x50, 68, (u8 *)(a0_info.vendor_sn), VTSS_APPL_DDMI_STR_MAX_LEN, FALSE) != VTSS_RC_OK ||
         meba_sfp_i2c_xfer(board_instance, port_no, false, 0x50, 56, (u8 *)(a0_info.vendor_rev), 4, FALSE) != VTSS_RC_OK ||
         meba_sfp_i2c_xfer(board_instance, port_no, false, 0x50, 0,  transeiver_code, 13, FALSE) != VTSS_RC_OK ||
         meba_sfp_i2c_xfer(board_instance, port_no, false, 0x50, 84, date_code, 8, FALSE) != VTSS_RC_OK ||
         meba_sfp_i2c_xfer(board_instance, port_no, false, 0x50, 92, &a0_info.type, 1, FALSE) != VTSS_RC_OK ) {
        rc = DDMI_ERROR_READ_FAIL;
    }


    if ( rc == VTSS_RC_OK ) {
        /* yyyy-mm-ddXX */
        a0_info.date_code[0] = '2';
        a0_info.date_code[1] = '0';
        memcpy(&a0_info.date_code[2], date_code, 2);
        a0_info.date_code[4] = '-';
        memcpy(&a0_info.date_code[5], &date_code[2], 2);
        a0_info.date_code[7] = '-';
        memcpy(&a0_info.date_code[8], &date_code[4], 4);
        a0_info.date_code[12] = 0;
        /* Is it a 1000BASE-X module? */
        if (transeiver_code[6] == 0x1 || transeiver_code[6] == 0x2 || transeiver_code[6] == 0x4) {
            if (transeiver_code[6] == 0x1) {
                a0_info.mode = VTSS_APPL_PORT_SFP_1000BASE_SX;
            } else if (transeiver_code[6] == 0x2) {
                a0_info.mode = VTSS_APPL_PORT_SFP_1000BASE_LX;
            } else {
                a0_info.mode = VTSS_APPL_PORT_SFP_1000BASE_CX;
            }
        } else if (transeiver_code[6] == 0x08) {
            /* Is it a CU_SFP module? */
            a0_info.mode = VTSS_APPL_PORT_SFP_1000BASE_T;
        } else if (transeiver_code[6] == 0x10 || transeiver_code[6] == 0x20 ) {
            if ( transeiver_code[6] == 0x10 ) {
                a0_info.mode = VTSS_APPL_PORT_SFP_100BASE_LX;
            } else {
                a0_info.mode = VTSS_APPL_PORT_SFP_100FX;
            }
        } else if (transeiver_code[6] == 0x40) {
            if (transeiver_code[12] >= 1 && transeiver_code[12] < 10) {
                a0_info.mode = VTSS_APPL_PORT_SFP_100BASE_BX10;
            } else if (transeiver_code[12] >= 10 && transeiver_code[12] < 25) {
                a0_info.mode = VTSS_APPL_PORT_SFP_1000BASE_BX10;
            } else {
                a0_info.mode = VTSS_APPL_PORT_SFP_NONE;
            }
        } else if (transeiver_code[6] == 0) {
            /* It does not support Ethernet Compliance Codes. Find the max supported signaling rate */
            if (transeiver_code[12] >= 1 && transeiver_code[12] < 10) {
                a0_info.mode = VTSS_APPL_PORT_SFP_100FX;
            } else if (transeiver_code[12] >= 10 && transeiver_code[12] < 25) {
                a0_info.mode = VTSS_APPL_PORT_SFP_1000BASE_X;
            } else if (transeiver_code[12] >= 25 && transeiver_code[12] < 50) {
                a0_info.mode = VTSS_APPL_PORT_SFP_2G5;
            } else if (transeiver_code[12] >= 50 && transeiver_code[12] < 100)  {
                a0_info.mode = VTSS_APPL_PORT_SFP_5G;
            } else if (transeiver_code[12] >= 100) {
                a0_info.mode = VTSS_APPL_PORT_SFP_10G;
            } else {
                a0_info.mode = VTSS_APPL_PORT_SFP_NONE;
            }
        } else {
            a0_info.mode = VTSS_APPL_PORT_SFP_NONE;
        }

        trim_trailing_space(a0_info.vendor_name);
        trim_trailing_space(a0_info.vendor_pn);
        trim_trailing_space(a0_info.vendor_sn);
        trim_trailing_space(a0_info.vendor_rev);
        trim_trailing_space(a0_info.date_code);

        port_info->ddmi_a0 = a0_info;
        if (a0_info.type & 0x40) {
            port_info->cap |= DDMI_CAP_A2_SUPPORTED;
        } else {
            port_info->cap &= ~DDMI_CAP_A2_SUPPORTED;
        }

    }

    return rc;
}

#define txPwr2str                   vol2str
#define rxPwr2str                   vol2str
#define external_txPwr2str          external_vol2str

static char *DDMI_16_bit_signed_2_complement_value_2_str(u8 *input, char *str, size_t str_size, int multiplier, fraction_t *frac_out)
{
    fraction_t frac;
    int tmp;
    char sign[] = "";
    int i = 0;
    tmp = (*input ) * multiplier + (*(input + 1)) * multiplier / 256;
    frac.int_part = tmp / multiplier;

    if ((tmp - frac.int_part * multiplier) >= 0) {
        frac.frac_part = (tmp - frac.int_part * multiplier);
    } else {
        frac.frac_part = (frac.int_part * multiplier - tmp);
        sign[0] = '-';
    }

    tmp = multiplier;

    (void) snprintf(str, str_size, "%s%d.", 0 == frac.int_part ? sign : "", frac.int_part);

    tmp = frac.frac_part;

    for ( i = multiplier / 10; i >= 1; i /= 10 ) {
        (void) snprintf(str, str_size, "%s%d", str, tmp / i);
        tmp %= i;
    }

    if (frac_out) {
        *frac_out = frac;
    }

    return str;
}

static char *DDMI_16_bit_unsigned_2_complement_value_2_str(u8 *input, char *str, size_t str_size, int multiplier, fraction_t *frac_out)
{
    fraction_t frac;
    unsigned tmp;
    int i = 0;
    tmp = (*input ) * multiplier + (*(input + 1)) * multiplier / 256;
    frac.int_part = tmp / multiplier;

    frac.frac_part = (tmp - frac.int_part * multiplier);

    tmp = multiplier;

    (void) snprintf(str, str_size, "%d.", frac.int_part);

    tmp = frac.frac_part;

    for ( i = multiplier / 10; i >= 1; i /= 10 ) {
        (void) snprintf(str, str_size, "%s%d", str, tmp / i);
        tmp %= i;
    }

    if (frac_out) {
        *frac_out = frac;
    }

    return str;
}

static char *DDMI_16_bit_unsigned_value_2_str(u8 *input, char *str, size_t str_size, int multiplier, fraction_t *frac_out)
{
    fraction_t frac;
    unsigned int tmp;
    int i = 0;
    tmp = (*input << 8 | *(input + 1));
    frac.int_part = tmp / multiplier;
    frac.frac_part = tmp - frac.int_part * multiplier;
    (void)snprintf(str, str_size, "%d.", frac.int_part);

    tmp = frac.frac_part;
    for ( i = multiplier / 10; i >= 1; i /= 10 ) {
        (void) snprintf(str, str_size, "%s%d", str, tmp / i);
        tmp %= i;
    }

    if (frac_out) {
        *frac_out = frac;
    }

    return str;
}

static char *DDMI_offset_2_str(u8 *input, char *str, size_t str_size, int multiplier, fraction_t *frac_out)
{
    int tmp;
    tmp = (*input << 8 | *(input + 1));
    (void)snprintf(str, str_size, "%d", tmp);

    return str;
}

static int DDMI_ieee754_get(u8 *input, int multiplier, u32 *result, i8 *e, u32 *m)
{
    i8          exponent;
    u32         mantissa = 0;
    int         i = 0;

    exponent = (input[0] << 1 | input[1] >> 7);
    mantissa = (input[1] & 0x7f) << 1 | input[2] >> 7;
    if (exponent == 0 && mantissa == 0) {
        *result = 0;
        return 0;
    } else if (exponent == 0) {
        /* un-normalize */
        return -1;
    } else if (exponent == 255 && mantissa == 0) {
        /* infinite */
        return -2;
    } else if (exponent == 255) {
        /* NaN */
        return -3;
    }

    exponent -= 127;

    *result = 1 * multiplier + mantissa * multiplier / 256;
    if (m) {
        *m = *result;
    }

    if (e) {
        *e = exponent;
    }

    if (exponent >= 0) {
        for (i = 0; i < exponent; i++) {
            *result *= 2;
        }
    } else {
        for (i = 0; i > exponent; i-- ) {
            *result /= 2;
        }
    }
    return 0;
}

static char *DDMI_ieee754_2_str(u8 *input, char *str, size_t str_size, int multiplier, fraction_t *frac_out)
{
    int         i, rc;
    u32         tmp, m;
    fraction_t  frac;
    i8          e;

    if ( (rc = DDMI_ieee754_get(input, multiplier, &tmp, &e, &m)) != 0) {
        if (rc == -1) {
            (void)snprintf(str, str_size, "un-normalize");
        } else if (rc == -2) {
            (void)snprintf(str, str_size, "infinite");
        } else if (rc == -3) {
            (void)snprintf(str, str_size, "NaN");
        } else {
            (void)snprintf(str, str_size, "unknonw error");
        }
    }

    frac.int_part = tmp / multiplier;
    frac.frac_part = (tmp - frac.int_part * multiplier);

    (void)snprintf(str, str_size, "%d.", frac.int_part);

    tmp = frac.frac_part;
    for ( i = multiplier / 10; i >= 1; i /= 10 ) {
        (void) snprintf(str, str_size, "%s%d", str, tmp / i);
        tmp %= i;
    }

    snprintf(str, str_size, "%s(%d%s%d%s%d)", str, m, "/", multiplier, "e", e);
    if (frac_out) {
        *frac_out = frac;
    }

    return str;
}

static i8 *temp_cb_2_str(u8 *input, char *str, size_t str_size, int multiplier, fraction_t *frac_out)
{
    return DDMI_16_bit_signed_2_complement_value_2_str(input, str, str_size, TEMP_MULTIPLIER, frac_out);
}

static void temp2str(char *input, char *str, size_t str_size, fraction_t *frac_out)
{
    fraction_t frac;
    int tmp;
    char sign[] = "";
    tmp = (*input ) * TEMP_MULTIPLIER + (*(input + 1)) * TEMP_MULTIPLIER / 256;
    T_D("0x%02x%02x", input[0], input[1]);
    frac.int_part = tmp / TEMP_MULTIPLIER;

    if ((tmp - frac.int_part * TEMP_MULTIPLIER) >= 0) {
        frac.frac_part = (tmp - frac.int_part * TEMP_MULTIPLIER);
    } else {
        frac.frac_part = (frac.int_part * TEMP_MULTIPLIER - tmp);
        sign[0] = '-';
    }
    (void) snprintf(str, str_size, "%s%d.%03d", 0 == frac.int_part ? sign : "", frac.int_part, frac.frac_part);
    if (frac_out) {
        sscanf(str, "%d.%03d", &frac_out->int_part, &frac_out->frac_part);
    }

    trim_trailing_space(str);
}

static void external_temp2str(u8 *data, u8 *slope, u8 *offset, char *str, size_t str_size, fraction_t *frac_out)
{
    fraction_t frac;
    int tmp_1, tmp_3, result;
    unsigned int tmp_2;
    char sign[] = "";
    tmp_1 = (*data) * TEMP_MULTIPLIER + (*(data + 1)) * TEMP_MULTIPLIER / 256;
    tmp_2 = (*slope) * TEMP_MULTIPLIER + *(slope + 1) * TEMP_MULTIPLIER / 256;
    tmp_3 = ((*offset << 8) | *(offset + 1)) * TEMP_MULTIPLIER;
    result = tmp_1 * tmp_2 / TEMP_MULTIPLIER + tmp_3;
    frac.int_part = result / TEMP_MULTIPLIER;

    if ((result - frac.int_part * TEMP_MULTIPLIER) >= 0) {
        frac.frac_part = (result - frac.int_part * TEMP_MULTIPLIER);
    } else {
        frac.frac_part = (frac.int_part * TEMP_MULTIPLIER - result);
        sign[0] = '-';
    }
    (void) snprintf(str, str_size, "%s%d.%03d", 0 == frac.int_part ? sign : "", frac.int_part, frac.frac_part);
    if (frac_out) {
        sscanf(str, "%d.%03d", &frac_out->int_part, &frac_out->frac_part);
    }

    trim_trailing_space(str);
}

static void vol2str(unsigned char *input, char *str, size_t str_size, fraction_t *frac_out)
{
    fraction_t frac;
    unsigned int tmp;
    tmp = (*input << 8 | *(input + 1));
    T_D("0x%02x%02x", input[0], input[1]);
//    printf("tmp = %d, \n", tmp);
    frac.int_part = tmp / VOL_MULTIPLIER;
    frac.frac_part = tmp - frac.int_part * VOL_MULTIPLIER;
    (void)snprintf(str, str_size, "%d.%04d", frac.int_part, frac.frac_part);
    if (frac_out) {
        sscanf(str, "%d.%04d", &frac_out->int_part, &frac_out->frac_part);
    }

    trim_trailing_space(str);
}

static void external_vol2str(u8 *data, u8 *slope, u8 *offset, char *str, size_t str_size, fraction_t *frac_out)
{
    fraction_t frac;
    int tmp_1, tmp_3, result;
    unsigned int tmp_2;
    char sign[] = "";
    tmp_1 = (*data << 8 | *(data + 1));
    tmp_2 = (*slope) * VOL_MULTIPLIER + *(slope + 1) * VOL_MULTIPLIER / 256;
    tmp_3 = ((*offset << 8) | *(offset + 1)) * VOL_MULTIPLIER;
    result = (tmp_1 * tmp_2 + tmp_3) / VOL_MULTIPLIER;
    frac.int_part = result / VOL_MULTIPLIER;

    if ((result - frac.int_part * VOL_MULTIPLIER) >= 0) {
        frac.frac_part = (result - frac.int_part * VOL_MULTIPLIER);
    } else {
        frac.frac_part = (frac.int_part * VOL_MULTIPLIER - result);
        sign[0] = '-';
    }
    (void) snprintf(str, str_size, "%s%d.%04d", 0 == frac.int_part ? sign : "", frac.int_part, frac.frac_part);
    if (frac_out) {
        sscanf(str, "%d.%04d", &frac_out->int_part, &frac_out->frac_part);
    }

    trim_trailing_space(str);
}

static i8 *txBias_cb_2_str(unsigned char *input, char *str, size_t str_size, int multiplier, fraction_t *frac_out)
{
    fraction_t frac;
    unsigned int tmp;
    tmp = (*input << 8 | *(input + 1)) * 2;
    frac.int_part = tmp / BIAS_MULTIPLIER;
    frac.frac_part = (tmp - frac.int_part * BIAS_MULTIPLIER);
    (void)snprintf(str, str_size, "%d.%03d", frac.int_part, frac.frac_part);
    if (frac_out) {
        sscanf(str, "%d.%03d", &frac_out->int_part, &frac_out->frac_part);
    }
    trim_trailing_space(str);
    return str;
}

static void external_txBias2str(u8 *data, u8 *slope, u8 *offset, char *str, size_t str_size, fraction_t *frac_out)
{
    fraction_t frac;
    int tmp_1, tmp_2, tmp_3, result;
    char sign[] = "";
    tmp_1 = (*data << 8 | *(data + 1));
    tmp_2 = (*slope) * BIAS_MULTIPLIER + *(slope + 1) * BIAS_MULTIPLIER / 256;
    tmp_3 = ((*offset << 8) | *(offset + 1)) * BIAS_MULTIPLIER;
    result = (tmp_1 * tmp_2 + tmp_3) * 2 / BIAS_MULTIPLIER;
    frac.int_part = result / BIAS_MULTIPLIER;

    if ((result - frac.int_part * BIAS_MULTIPLIER) >= 0) {
        frac.frac_part = (result - frac.int_part * BIAS_MULTIPLIER);
    } else {
        frac.frac_part = (frac.int_part * BIAS_MULTIPLIER - result);
        sign[0] = '-';
    }
    (void) snprintf(str, str_size, "%s%d.%04d", 0 == frac.int_part ? sign : "", frac.int_part, frac.frac_part);
    if (frac_out) {
        sscanf(str, "%d.%04d", &frac_out->int_part, &frac_out->frac_part);
    }

    trim_trailing_space(str);
}

static void txBias2str(unsigned char *input, char *str, size_t str_size, fraction_t *frac_out)
{
    fraction_t frac;
    unsigned int tmp;
    tmp = (*input << 8 | *(input + 1)) * 2;
    T_D("input : 0x%02x%02x", input[0], input[1]);
    T_D("output : (0x%02x)0x%02x%02x", tmp, tmp >> 8 & 0xff, tmp & 0xff);
    frac.int_part = tmp / BIAS_MULTIPLIER;
    frac.frac_part = (tmp - frac.int_part * BIAS_MULTIPLIER);
    (void)snprintf(str, str_size, "%d.%03d", frac.int_part, frac.frac_part);
    if (frac_out) {
        sscanf(str, "%d.%03d", &frac_out->int_part, &frac_out->frac_part);
    }

    trim_trailing_space(str);
}

static void external_rxPwr2str(u8 *data, u8 *pwr, char *str, size_t str_size, fraction_t *frac_out)
{
    fraction_t frac;
    u32 pwr_4, pwr_3, pwr_2, pwr_1, pwr_0;
    u32 input = (*data << 8 | *(data + 1));
    u32 result;

    DDMI_ieee754_get (pwr, VOL_MULTIPLIER, &pwr_4,      NULL, NULL);
    DDMI_ieee754_get (pwr + 4, VOL_MULTIPLIER, &pwr_3,  NULL, NULL);
    DDMI_ieee754_get (pwr + 8, VOL_MULTIPLIER, &pwr_2,  NULL, NULL);
    DDMI_ieee754_get (pwr + 12, VOL_MULTIPLIER, &pwr_1, NULL, NULL);
    DDMI_ieee754_get (pwr + 16, VOL_MULTIPLIER, &pwr_0, NULL, NULL);
    T_D("pwr_4 = %u, pwr_3 = %u, pwr_2 = %u, pwr_1 = %u, pwr_0 = %u, input = %u",
        pwr_4, pwr_3, pwr_2, pwr_1, pwr_0, input);
    result = pwr_0 +
             pwr_1 * input + pwr_2 * input * input +
             pwr_3 * input * input * input +
             pwr_4 * input * input * input * input;

    result = result / VOL_MULTIPLIER;
    frac.int_part = result / VOL_MULTIPLIER;
    frac.frac_part = (result - frac.int_part * VOL_MULTIPLIER);
    (void)snprintf(str, str_size, "%d.%04d", frac.int_part, frac.frac_part);
    if (frac_out) {
        sscanf(str, "%d.%03d", &frac_out->int_part, &frac_out->frac_part);
    }

    trim_trailing_space(str);

}
#define IS_LARGER(v1, v2) \
    (((v1)->int_part > (v2)->int_part) || (((v1)->int_part == (v2)->int_part) && (((v1)->int_part >=0 && ((v1)->frac_part > (v2)->frac_part))|| \
        ((v1)->int_part < 0 && ((v1)->frac_part < (v2)->frac_part)))))

#define IS_LESSER(v1, v2) \
    (((v1)->int_part < (v2)->int_part) || (((v1)->int_part == (v2)->int_part) && (((v1)->int_part >= 0 && ((v1)->frac_part < (v2)->frac_part))|| \
        ((v1)->int_part < 0 && ((v1)->frac_part > (v2)->frac_part)))))

static ddmi_state_t getState(i8 *current, fraction_t *val)
{
    fraction_t *hi_alarm_val = &val[0];
    fraction_t *lo_alarm_val = &val[1];
    fraction_t *hi_warn_val = &val[2];
    fraction_t *lo_warn_val = &val[3];
    fraction_t *current_val = &val[4];

    if ( IS_LESSER(current_val, hi_warn_val) && IS_LARGER(current_val, lo_warn_val) ) {
        return DDMI_STATE_REGULAR;
    } else if (!IS_LESSER(current_val, hi_warn_val) && IS_LESSER(current_val, hi_alarm_val)) {
        strcat(current, " +");
        return DDMI_STATE_HI_WARN;
    } else if (!IS_LARGER(current_val, lo_warn_val) && IS_LARGER(current_val, lo_alarm_val)) {
        strcat(current, " -");
        return DDMI_STATE_LO_WARN;
    } else if (!IS_LESSER(current_val, hi_alarm_val)) {
        strcat(current, " ++");
        return DDMI_STATE_HI_ALARM;
    } else if (!IS_LARGER(current_val, lo_alarm_val)) {
        strcat(current, " --");
        return DDMI_STATE_LO_ALARM;
    }
    return DDMI_STATE_END;
}

static const char *state_txt(ddmi_state_t state)
{
    switch (state) {
    case DDMI_STATE_REGULAR:
        return "REGULAR";
    case DDMI_STATE_HI_WARN:
        return "HI WARN";
    case DDMI_STATE_LO_WARN:
        return "LO WARN";
    case DDMI_STATE_HI_ALARM:
        return "HI ALARM";
    case DDMI_STATE_LO_ALARM:
        return "LO ALARM";
    case DDMI_STATE_END:
        return "REMOVE";
    default:
        return "ERROR";
    }

}

static void DDMI_status_update(vtss_isid_t isid, mesa_port_no_t port_no, vtss_appl_ddmi_event_type_t type, vtss_appl_ddmi_event_entry_t *entry)
{
    vtss_ifindex_t ifindex;
    if (vtss_ifindex_from_port(isid, port_no, &ifindex) != VTSS_RC_OK && entry) {
        memset(entry, 0, sizeof(*entry));
        T_W("vtss_ifindex_from_port fail(isid = %d, port_no = %d)", isid, port_no);
    }

    if (entry) {
        ddmi_status_update.set(ifindex, type, entry);
    } else {
        ddmi_status_update.del(ifindex, type);
    }

}

static mesa_rc read_ddmi_a2(mesa_port_no_t port_no, ddmi_port_info_t *port_info, ddmi_state_t state[], BOOL *state_change_flag)
{
    u8     sfp_phy[255], *ptr = NULL;
    ddmi_a2_t a2_info;
    i8 data_str[32 + 1], *p;
    ddmi_state_t new_state;
    mesa_rc rc = VTSS_RC_OK;
    fraction_t frac[5];
    i32 i = 0;
    i32 j = 0;
    i8 buf[128];
    vtss_appl_ddmi_event_entry_t entry;
#if 0
    u8 test_hi_a[] =    {0x9a, 0xd3},
                        test_lo_a[] =    {0x03, 0x15},
                                         test_hi_w[] =    {0x4d, 0xe5},
                                                          test_lo_w[] =    {0x06, 0x2e},
                                                                           test_cur[] =     {0xb7, 0x20};
    u8 test_external[] = {
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x32, 0xde, 0x76, 0xcf,
        0x3d, 0x00, 0x68, 0x18,
        0x3f, 0x84, 0xd8, 0xa9,
    };
#endif

    memset(&a2_info, 0, sizeof(a2_info));

    if ( meba_sfp_i2c_xfer(board_instance, port_no, false, 0x51, 0, sfp_phy, 255, FALSE) != VTSS_RC_OK ) {
        rc = DDMI_ERROR_READ_FAIL;
    }

#if 0
    memcpy(&sfp_phy[32], test_hi_a,   sizeof(test_hi_a));
    memcpy(&sfp_phy[34], test_lo_a,   sizeof(test_lo_a));
    memcpy(&sfp_phy[36], test_hi_w,   sizeof(test_hi_w));
    memcpy(&sfp_phy[38], test_lo_w,   sizeof(test_lo_w));
    memcpy(&sfp_phy[104], test_cur,  sizeof(test_cur));
    memcpy(&sfp_phy[56], test_external, sizeof(test_external));
#endif

    for (i = 0; i < DDMI_TABLE_COUNT; i++) {
        memset(buf, 0, sizeof(buf));
        memset(data_str, 0, sizeof(data_str));
        for (j = 0; j < ddmi_table[i].len; j++) {
            snprintf(buf, sizeof(buf), "%s%02x", buf, sfp_phy[ddmi_table[i].offset + j]);
        }
        ptr = &sfp_phy[ddmi_table[i].offset];
        T_D("%-24s : 0x%-16s(%s)", ddmi_table[i].name, buf, ddmi_table[i].ddmi_cb_fn ? ddmi_table[i].ddmi_cb_fn(ptr, data_str, sizeof(data_str), 10000, NULL) : "NULL");
    }

    if ((port_info->ddmi_a0.type & 0x20)) {
        temp2str( (i8 *)&sfp_phy[0],  a2_info.temp_hi_alarm, sizeof(a2_info.temp_hi_alarm), &frac[0]);
        temp2str( (i8 *)&sfp_phy[2],  a2_info.temp_lo_alarm, sizeof(a2_info.temp_lo_alarm), &frac[1]);
        temp2str( (i8 *)&sfp_phy[4],  a2_info.temp_hi_warn, sizeof(a2_info.temp_hi_warn), &frac[2]);
        temp2str( (i8 *)&sfp_phy[6],  a2_info.temp_lo_warn, sizeof(a2_info.temp_lo_warn), &frac[3]);
        temp2str( (i8 *)&sfp_phy[96], a2_info.temp_current, sizeof(a2_info.temp_current), &frac[4]);
        if ( (new_state = getState(a2_info.temp_current, frac )) != state[0]) {
            p = &buf[0];
            p += snprintf(p, (&buf[0] + sizeof(buf) - p), "DDMI-TEMPERATURE_CHANGED: DoM temperature changed to %s on ", state_txt(new_state));
#if defined(VTSS_SW_OPTION_SYSLOG)
            p += snprintf(p, (&buf[0] + sizeof(buf) - p), "Interface %s", SYSLOG_PORT_INFO_REPLACE_KEYWORD);
            S_PORT_I(VTSS_ISID_LOCAL, port_no, "%s", buf);
#endif /* VTSS_SW_OPTION_SYSLOG */
            T_D("%s", buf);

            state[0] = new_state;
            *state_change_flag = TRUE;
            memset(&entry, 0, sizeof(entry));
            entry.state = (vtss_appl_ddmi_event_state_t)new_state;
            temp2str( (i8 *)&sfp_phy[96], entry.current, sizeof(entry.current), NULL);
            memcpy(entry.high_alarm_threshold, a2_info.temp_hi_alarm, sizeof(a2_info.temp_hi_alarm));
            memcpy(entry.low_alarm_threshold, a2_info.temp_lo_alarm, sizeof(a2_info.temp_lo_alarm));
            memcpy(entry.high_warn_threshold, a2_info.temp_hi_warn, sizeof(a2_info.temp_hi_warn));
            memcpy(entry.low_warn_threshold, a2_info.temp_lo_warn, sizeof(a2_info.temp_lo_warn));
            DDMI_status_update(VTSS_ISID_LOCAL, port_no, VTSS_APPL_DDMI_EVENT_TYPE_TEMPERATURE, &entry);
        }

        vol2str( &sfp_phy[8],  a2_info.voltage_hi_alarm, sizeof(a2_info.voltage_hi_alarm), &frac[0]);
        vol2str( &sfp_phy[10], a2_info.voltage_lo_alarm, sizeof(a2_info.voltage_lo_alarm), &frac[1]);
        vol2str( &sfp_phy[12], a2_info.voltage_hi_warn, sizeof(a2_info.voltage_hi_warn), &frac[2]);
        vol2str( &sfp_phy[14], a2_info.voltage_lo_warn, sizeof(a2_info.voltage_lo_warn), &frac[3]);
        vol2str( &sfp_phy[98], a2_info.voltage_current, sizeof(a2_info.voltage_current), &frac[4]);
        if ( (new_state = getState(a2_info.voltage_current, frac )) != state[1]) {
            p = &buf[0];
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "DDMI-VOLTAGE_CHANGED: DoM voltage changed to %s on ", state_txt(new_state));
#if defined(VTSS_SW_OPTION_SYSLOG)
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "Interface %s", SYSLOG_PORT_INFO_REPLACE_KEYWORD);
            S_PORT_I(VTSS_ISID_LOCAL, port_no, "%s", buf);
#endif /* VTSS_SW_OPTION_SYSLOG */
            T_D("%s", buf);

            state[1] = new_state;
            *state_change_flag = TRUE;
            memset(&entry, 0, sizeof(entry));
            entry.state = (vtss_appl_ddmi_event_state_t)new_state;
            vol2str( &sfp_phy[98], entry.current, sizeof(entry.current), NULL);
            memcpy(entry.high_alarm_threshold, a2_info.voltage_hi_alarm, sizeof(a2_info.voltage_hi_alarm));
            memcpy(entry.low_alarm_threshold, a2_info.voltage_lo_alarm, sizeof(a2_info.voltage_lo_alarm));
            memcpy(entry.high_warn_threshold, a2_info.voltage_hi_warn, sizeof(a2_info.voltage_hi_warn));
            memcpy(entry.low_warn_threshold, a2_info.voltage_lo_warn, sizeof(a2_info.voltage_lo_warn));
            DDMI_status_update(VTSS_ISID_LOCAL, port_no, VTSS_APPL_DDMI_EVENT_TYPE_VOLTAGE, &entry);
        }

        txBias2str( &sfp_phy[16],  a2_info.bias_hi_alarm, sizeof(a2_info.bias_hi_alarm), &frac[0]);
        txBias2str( &sfp_phy[18],  a2_info.bias_lo_alarm, sizeof(a2_info.bias_lo_alarm), &frac[1]);
        txBias2str( &sfp_phy[20],  a2_info.bias_hi_warn, sizeof(a2_info.bias_hi_warn), &frac[2]);
        txBias2str( &sfp_phy[22],  a2_info.bias_lo_warn, sizeof(a2_info.bias_lo_warn), &frac[3]);
        txBias2str( &sfp_phy[100], a2_info.bias_current, sizeof(a2_info.bias_current), &frac[4]);
        if ( (new_state = getState(a2_info.bias_current, frac )) != state[2]) {
            p = &buf[0];
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "DDMI-BIAS_CHANGED: DoM Bias changed to %s on ", state_txt(new_state));
#if defined(VTSS_SW_OPTION_SYSLOG)
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "Interface %s", SYSLOG_PORT_INFO_REPLACE_KEYWORD);
            S_PORT_I(VTSS_ISID_LOCAL, port_no, "%s", buf);
#endif /* VTSS_SW_OPTION_SYSLOG */
            T_D("%s", buf);

            state[2] = new_state;
            *state_change_flag = TRUE;
            entry.state = (vtss_appl_ddmi_event_state_t)new_state;
            txBias2str( &sfp_phy[100], entry.current, sizeof(entry.current), NULL);
            memcpy(entry.high_alarm_threshold, a2_info.bias_hi_alarm, sizeof(a2_info.bias_hi_alarm));
            memcpy(entry.low_alarm_threshold, a2_info.bias_lo_alarm, sizeof(a2_info.bias_lo_alarm));
            memcpy(entry.high_warn_threshold, a2_info.bias_hi_warn, sizeof(a2_info.bias_hi_warn));
            memcpy(entry.low_warn_threshold, a2_info.bias_lo_warn, sizeof(a2_info.bias_lo_warn));
            DDMI_status_update(VTSS_ISID_LOCAL, port_no, VTSS_APPL_DDMI_EVENT_TYPE_BIAS, &entry);
        }

        txPwr2str( &sfp_phy[24],  a2_info.tx_pwr_hi_alarm, sizeof(a2_info.tx_pwr_hi_alarm), &frac[0]);
        txPwr2str( &sfp_phy[26],  a2_info.tx_pwr_lo_alarm, sizeof(a2_info.tx_pwr_lo_alarm), &frac[1]);
        txPwr2str( &sfp_phy[28],  a2_info.tx_pwr_hi_warn, sizeof(a2_info.tx_pwr_hi_warn), &frac[2]);
        txPwr2str( &sfp_phy[30],  a2_info.tx_pwr_lo_warn, sizeof(a2_info.tx_pwr_lo_warn), &frac[3]);
        txPwr2str( &sfp_phy[102], a2_info.tx_pwr_current, sizeof(a2_info.tx_pwr_current), &frac[4]);
        if ( (new_state = getState(a2_info.tx_pwr_current, frac )) != state[3]) {
            p = &buf[0];
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "DDMI-TX_POWER_CHANGED: DoM Tx Power changed to %s on ", state_txt(new_state));
#if defined(VTSS_SW_OPTION_SYSLOG)
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "Interface %s", SYSLOG_PORT_INFO_REPLACE_KEYWORD);
            S_PORT_I(VTSS_ISID_LOCAL, port_no, "%s", buf);
#endif /* VTSS_SW_OPTION_SYSLOG */
            T_D("%s", buf);

            state[3] = new_state;
            *state_change_flag = TRUE;
            entry.state = (vtss_appl_ddmi_event_state_t)new_state;
            txPwr2str( &sfp_phy[102], entry.current, sizeof(entry.current), NULL);
            memcpy(entry.high_alarm_threshold, a2_info.tx_pwr_hi_alarm, sizeof(a2_info.tx_pwr_hi_alarm));
            memcpy(entry.low_alarm_threshold, a2_info.tx_pwr_lo_alarm, sizeof(a2_info.tx_pwr_lo_alarm));
            memcpy(entry.high_warn_threshold, a2_info.tx_pwr_hi_warn, sizeof(a2_info.tx_pwr_hi_warn));
            memcpy(entry.low_warn_threshold, a2_info.tx_pwr_lo_warn, sizeof(a2_info.tx_pwr_lo_warn));
            DDMI_status_update(VTSS_ISID_LOCAL, port_no, VTSS_APPL_DDMI_EVENT_TYPE_TX_POWER, &entry);
        }

        rxPwr2str( &sfp_phy[32], a2_info.rx_pwr_hi_alarm, sizeof(a2_info.rx_pwr_hi_alarm), &frac[0]);
        rxPwr2str( &sfp_phy[34], a2_info.rx_pwr_lo_alarm, sizeof(a2_info.rx_pwr_lo_alarm), &frac[1]);
        rxPwr2str( &sfp_phy[36], a2_info.rx_pwr_hi_warn, sizeof(a2_info.rx_pwr_hi_warn), &frac[2]);
        rxPwr2str( &sfp_phy[38], a2_info.rx_pwr_lo_warn, sizeof(a2_info.rx_pwr_lo_warn), &frac[3]);
        rxPwr2str( &sfp_phy[104], a2_info.rx_pwr_current, sizeof(a2_info.rx_pwr_current), &frac[4]);
        if ( (new_state = getState(a2_info.rx_pwr_current, frac )) != state[4]) {
            p = &buf[0];
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "DDMI-RX_POWER_CHANGED: DoM Rx Power changed to %s on ", state_txt(new_state));
#if defined(VTSS_SW_OPTION_SYSLOG)
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "Interface %s", SYSLOG_PORT_INFO_REPLACE_KEYWORD);
            S_PORT_I(VTSS_ISID_LOCAL, port_no, "%s", buf);
#endif /* VTSS_SW_OPTION_SYSLOG */
            T_D("%s", buf);

            state[4] = new_state;
            *state_change_flag = TRUE;
            entry.state = (vtss_appl_ddmi_event_state_t)new_state;
            rxPwr2str( &sfp_phy[104], entry.current, sizeof(entry.current), NULL);
            memcpy(entry.high_alarm_threshold, a2_info.rx_pwr_hi_alarm, sizeof(a2_info.rx_pwr_hi_alarm));
            memcpy(entry.low_alarm_threshold, a2_info.rx_pwr_lo_alarm, sizeof(a2_info.rx_pwr_lo_alarm));
            memcpy(entry.high_warn_threshold, a2_info.rx_pwr_hi_warn, sizeof(a2_info.rx_pwr_hi_warn));
            memcpy(entry.low_warn_threshold, a2_info.rx_pwr_lo_warn, sizeof(a2_info.rx_pwr_lo_warn));
            DDMI_status_update(VTSS_ISID_LOCAL, port_no, VTSS_APPL_DDMI_EVENT_TYPE_RX_POWER, &entry);
        }
    } else if ((port_info->ddmi_a0.type & 0x10)) {
        external_temp2str(&sfp_phy[0], &sfp_phy[84], &sfp_phy[86], a2_info.temp_hi_alarm, sizeof(a2_info.temp_hi_alarm), &frac[0]);
        external_temp2str(&sfp_phy[2], &sfp_phy[84], &sfp_phy[86], a2_info.temp_lo_alarm, sizeof(a2_info.temp_lo_alarm), &frac[1]);
        external_temp2str(&sfp_phy[4], &sfp_phy[84], &sfp_phy[86], a2_info.temp_hi_warn, sizeof(a2_info.temp_hi_warn), &frac[2]);
        external_temp2str(&sfp_phy[6], &sfp_phy[84], &sfp_phy[86], a2_info.temp_lo_warn, sizeof(a2_info.temp_lo_warn), &frac[3]);
        external_temp2str(&sfp_phy[96], &sfp_phy[84], &sfp_phy[86], a2_info.temp_current, sizeof(a2_info.temp_current), &frac[4]);
        if ( (new_state = getState(a2_info.temp_current, frac )) != state[0]) {
            p = &buf[0];
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "DDMI-TEMPERATURE_CHANGED: DoM temperature changed to %s on ", state_txt(new_state));
#if defined(VTSS_SW_OPTION_SYSLOG)
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "Interface %s", SYSLOG_PORT_INFO_REPLACE_KEYWORD);
            S_PORT_I(VTSS_ISID_LOCAL, port_no, "%s", buf);
#endif /* VTSS_SW_OPTION_SYSLOG */
            T_D("%s", buf);

            state[0] = new_state;
            *state_change_flag = TRUE;
            memset(&entry, 0, sizeof(entry));
            entry.state = (vtss_appl_ddmi_event_state_t)new_state;
            temp2str( (i8 *)&sfp_phy[96], entry.current, sizeof(entry.current), NULL);
            memcpy(entry.high_alarm_threshold, a2_info.temp_hi_alarm, sizeof(a2_info.temp_hi_alarm));
            memcpy(entry.low_alarm_threshold, a2_info.temp_lo_alarm, sizeof(a2_info.temp_lo_alarm));
            memcpy(entry.high_warn_threshold, a2_info.temp_hi_warn, sizeof(a2_info.temp_hi_warn));
            memcpy(entry.low_warn_threshold, a2_info.temp_lo_warn, sizeof(a2_info.temp_lo_warn));
            DDMI_status_update(VTSS_ISID_LOCAL, port_no, VTSS_APPL_DDMI_EVENT_TYPE_TEMPERATURE, &entry);
        }

        external_vol2str( &sfp_phy[8],  &sfp_phy[88], &sfp_phy[90], a2_info.voltage_hi_alarm, sizeof(a2_info.voltage_hi_alarm), &frac[0]);
        external_vol2str( &sfp_phy[10], &sfp_phy[88], &sfp_phy[90], a2_info.voltage_lo_alarm, sizeof(a2_info.voltage_lo_alarm), &frac[1]);
        external_vol2str( &sfp_phy[12], &sfp_phy[88], &sfp_phy[90], a2_info.voltage_hi_warn, sizeof(a2_info.voltage_hi_warn), &frac[2]);
        external_vol2str( &sfp_phy[14], &sfp_phy[88], &sfp_phy[90], a2_info.voltage_lo_warn, sizeof(a2_info.voltage_lo_warn), &frac[3]);
        external_vol2str( &sfp_phy[98], &sfp_phy[88], &sfp_phy[90], a2_info.voltage_current, sizeof(a2_info.voltage_current), &frac[4]);
        if ( (new_state = getState(a2_info.voltage_current, frac )) != state[1]) {
            p = &buf[0];
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "DDMI-VOLTAGE_CHANGED: DoM voltage changed to %s on ", state_txt(new_state));
#if defined(VTSS_SW_OPTION_SYSLOG)
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "Interface %s", SYSLOG_PORT_INFO_REPLACE_KEYWORD);
            S_PORT_I(VTSS_ISID_LOCAL, port_no, "%s", buf);
#endif /* VTSS_SW_OPTION_SYSLOG */
            T_D("%s", buf);

            state[1] = new_state;
            *state_change_flag = TRUE;
            memset(&entry, 0, sizeof(entry));
            entry.state = (vtss_appl_ddmi_event_state_t)new_state;
            vol2str( &sfp_phy[98], entry.current, sizeof(entry.current), NULL);
            memcpy(entry.high_alarm_threshold, a2_info.voltage_hi_alarm, sizeof(a2_info.voltage_hi_alarm));
            memcpy(entry.low_alarm_threshold, a2_info.voltage_lo_alarm, sizeof(a2_info.voltage_lo_alarm));
            memcpy(entry.high_warn_threshold, a2_info.voltage_hi_warn, sizeof(a2_info.voltage_hi_warn));
            memcpy(entry.low_warn_threshold, a2_info.voltage_lo_warn, sizeof(a2_info.voltage_lo_warn));
            DDMI_status_update(VTSS_ISID_LOCAL, port_no, VTSS_APPL_DDMI_EVENT_TYPE_VOLTAGE, &entry);
        }

        external_txBias2str( &sfp_phy[16], &sfp_phy[76], &sfp_phy[78], a2_info.bias_hi_alarm, sizeof(a2_info.bias_hi_alarm), &frac[0]);
        external_txBias2str( &sfp_phy[18], &sfp_phy[76], &sfp_phy[78], a2_info.bias_lo_alarm, sizeof(a2_info.bias_lo_alarm), &frac[1]);
        external_txBias2str( &sfp_phy[20], &sfp_phy[76], &sfp_phy[78], a2_info.bias_hi_warn, sizeof(a2_info.bias_hi_warn), &frac[2]);
        external_txBias2str( &sfp_phy[22], &sfp_phy[76], &sfp_phy[78], a2_info.bias_lo_warn, sizeof(a2_info.bias_lo_warn), &frac[3]);
        external_txBias2str( &sfp_phy[100], &sfp_phy[76], &sfp_phy[78], a2_info.bias_current, sizeof(a2_info.bias_current), &frac[4]);
        if ( (new_state = getState(a2_info.bias_current, frac )) != state[2]) {
            p = &buf[0];
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "DDMI-BIAS_CHANGED: DoM Bias changed to %s on ", state_txt(new_state));
#if defined(VTSS_SW_OPTION_SYSLOG)
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "Interface %s", SYSLOG_PORT_INFO_REPLACE_KEYWORD);
            S_PORT_I(VTSS_ISID_LOCAL, port_no, "%s", buf);
#endif /* VTSS_SW_OPTION_SYSLOG */
            T_D("%s", buf);

            state[2] = new_state;
            *state_change_flag = TRUE;
            entry.state = (vtss_appl_ddmi_event_state_t)new_state;
            txBias2str( &sfp_phy[100], entry.current, sizeof(entry.current), NULL);
            memcpy(entry.high_alarm_threshold, a2_info.bias_hi_alarm, sizeof(a2_info.bias_hi_alarm));
            memcpy(entry.low_alarm_threshold, a2_info.bias_lo_alarm, sizeof(a2_info.bias_lo_alarm));
            memcpy(entry.high_warn_threshold, a2_info.bias_hi_warn, sizeof(a2_info.bias_hi_warn));
            memcpy(entry.low_warn_threshold, a2_info.bias_lo_warn, sizeof(a2_info.bias_lo_warn));
            DDMI_status_update(VTSS_ISID_LOCAL, port_no, VTSS_APPL_DDMI_EVENT_TYPE_BIAS, &entry);
        }

        external_txPwr2str( &sfp_phy[24], &sfp_phy[80], &sfp_phy[82], a2_info.tx_pwr_hi_alarm, sizeof(a2_info.tx_pwr_hi_alarm), &frac[0]);
        external_txPwr2str( &sfp_phy[26], &sfp_phy[80], &sfp_phy[82], a2_info.tx_pwr_lo_alarm, sizeof(a2_info.tx_pwr_lo_alarm), &frac[1]);
        external_txPwr2str( &sfp_phy[28], &sfp_phy[80], &sfp_phy[82], a2_info.tx_pwr_hi_warn, sizeof(a2_info.tx_pwr_hi_warn), &frac[2]);
        external_txPwr2str( &sfp_phy[30], &sfp_phy[80], &sfp_phy[82], a2_info.tx_pwr_lo_warn, sizeof(a2_info.tx_pwr_lo_warn), &frac[3]);
        external_txPwr2str( &sfp_phy[102], &sfp_phy[80], &sfp_phy[82], a2_info.tx_pwr_current, sizeof(a2_info.tx_pwr_current), &frac[4]);
        if ( (new_state = getState(a2_info.tx_pwr_current, frac )) != state[3]) {
            p = &buf[0];
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "DDMI-TX_POWER_CHANGED: DoM Tx Power changed to %s on ", state_txt(new_state));
#if defined(VTSS_SW_OPTION_SYSLOG)
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "Interface %s", SYSLOG_PORT_INFO_REPLACE_KEYWORD);
            S_PORT_I(VTSS_ISID_LOCAL, port_no, "%s", buf);
#endif /* VTSS_SW_OPTION_SYSLOG */
            T_D("%s", buf);

            state[3] = new_state;
            *state_change_flag = TRUE;
            entry.state = (vtss_appl_ddmi_event_state_t)new_state;
            txPwr2str( &sfp_phy[102], entry.current, sizeof(entry.current), NULL);
            memcpy(entry.high_alarm_threshold, a2_info.tx_pwr_hi_alarm, sizeof(a2_info.tx_pwr_hi_alarm));
            memcpy(entry.low_alarm_threshold, a2_info.tx_pwr_lo_alarm, sizeof(a2_info.tx_pwr_lo_alarm));
            memcpy(entry.high_warn_threshold, a2_info.tx_pwr_hi_warn, sizeof(a2_info.tx_pwr_hi_warn));
            memcpy(entry.low_warn_threshold, a2_info.tx_pwr_lo_warn, sizeof(a2_info.tx_pwr_lo_warn));
            DDMI_status_update(VTSS_ISID_LOCAL, port_no, VTSS_APPL_DDMI_EVENT_TYPE_TX_POWER, &entry);
        }

        external_rxPwr2str( &sfp_phy[32], &sfp_phy[56], a2_info.rx_pwr_hi_alarm, sizeof(a2_info.rx_pwr_hi_alarm), &frac[0]);
        external_rxPwr2str( &sfp_phy[34], &sfp_phy[56], a2_info.rx_pwr_lo_alarm, sizeof(a2_info.rx_pwr_lo_alarm), &frac[1]);
        external_rxPwr2str( &sfp_phy[36], &sfp_phy[56], a2_info.rx_pwr_hi_warn, sizeof(a2_info.rx_pwr_hi_warn), &frac[2]);
        external_rxPwr2str( &sfp_phy[38], &sfp_phy[56], a2_info.rx_pwr_lo_warn, sizeof(a2_info.rx_pwr_lo_warn), &frac[3]);
        external_rxPwr2str( &sfp_phy[104], &sfp_phy[56], a2_info.rx_pwr_current, sizeof(a2_info.rx_pwr_current), &frac[4]);
        if ( (new_state = getState(a2_info.rx_pwr_current, frac )) != state[4]) {
            p = &buf[0];
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "DDMI-RX_POWER_CHANGED: DoM Rx Power changed to %s on ", state_txt(new_state));
#if defined(VTSS_SW_OPTION_SYSLOG)
            p += snprintf(p, &buf[0] + sizeof(buf) - p, "Interface %s", SYSLOG_PORT_INFO_REPLACE_KEYWORD);
            S_PORT_I(VTSS_ISID_LOCAL, port_no, "%s", buf);
#endif /* VTSS_SW_OPTION_SYSLOG */
            T_D("%s", buf);

            state[4] = new_state;
            *state_change_flag = TRUE;
            entry.state = (vtss_appl_ddmi_event_state_t)new_state;
            rxPwr2str( &sfp_phy[104], entry.current, sizeof(entry.current), NULL);
            memcpy(entry.high_alarm_threshold, a2_info.rx_pwr_hi_alarm, sizeof(a2_info.rx_pwr_hi_alarm));
            memcpy(entry.low_alarm_threshold, a2_info.rx_pwr_lo_alarm, sizeof(a2_info.rx_pwr_lo_alarm));
            memcpy(entry.high_warn_threshold, a2_info.rx_pwr_hi_warn, sizeof(a2_info.rx_pwr_hi_warn));
            memcpy(entry.low_warn_threshold, a2_info.rx_pwr_lo_warn, sizeof(a2_info.rx_pwr_lo_warn));
            DDMI_status_update(VTSS_ISID_LOCAL, port_no, VTSS_APPL_DDMI_EVENT_TYPE_RX_POWER, &entry);
        }

    } else {
        rc = VTSS_RC_ERROR;
    }

    if ( VTSS_RC_OK == rc ) {
        port_info->ddmi_a2 = a2_info;
    }
    return rc;
}

/****************************************************************************/
/*  Management functions                                                    */
/****************************************************************************/

/* DDMI error text */
const char *ddmi_error_txt(mesa_rc rc)
{
    switch (rc) {
    case DDMI_ERROR_MUST_BE_MASTER:
        return "Operation only valid on master switch";
    case DDMI_ERROR_ISID:
        return "Invalid Switch ID";
    case DDMI_ERROR_INV_PARAM:
        return "Invalid parameter supplied to function";
    case DDMI_ERROR_READ_FAIL:
        return "Read DDMI data failed";
    case DDMI_ERROR_I2C_BUSY:
        return "I2C Busy";
    default:
        return "DDMI: Unknown error code";
    }
}

/* DDMI error text */
const char *ddmi_mode_txt(vtss_appl_sfp_tranceiver_t  mode)
{
    switch (mode) {
    case VTSS_APPL_PORT_SFP_NONE:
        return "NONE";
    case VTSS_APPL_PORT_SFP_NOT_SUPPORTED:
        return "NOT_SUPPORTED";
    case VTSS_APPL_PORT_SFP_100FX:
        return "100BASE_FX";
    case VTSS_APPL_PORT_SFP_100BASE_LX:
        return "100BASE_LX";
    case VTSS_APPL_PORT_SFP_100BASE_ZX:
        return "100BASE_ZX";
    case VTSS_APPL_PORT_SFP_100BASE_BX10:
        return "100BASE_BX10";
    case VTSS_APPL_PORT_SFP_100BASE_T:
        return "100BASE_T";
    case VTSS_APPL_PORT_SFP_1000BASE_BX10:
        return "1000BASE_BX10";
    case VTSS_APPL_PORT_SFP_1000BASE_T:
        return "1000BASE_T";
    case VTSS_APPL_PORT_SFP_1000BASE_CX:
        return "1000BASE_CX";
    case VTSS_APPL_PORT_SFP_1000BASE_SX:
        return "1000BASE_SX";
    case VTSS_APPL_PORT_SFP_1000BASE_LX:
        return "1000BASE_LX";
    case VTSS_APPL_PORT_SFP_1000BASE_X:
        return "1000BASE_X";
    case VTSS_APPL_PORT_SFP_2G5:
        return "2G5";
    case VTSS_APPL_PORT_SFP_5G:
        return "5G";
    case VTSS_APPL_PORT_SFP_10G:
        return "10G";
    default:
        return "DDMI: Unknown error code";
    }
}

/* Get Loop_Protect port info  */
mesa_rc ddmi_port_info_get(vtss_isid_t isid,
                           mesa_port_no_t port_no,
                           ddmi_port_info_t *info)
{
    mesa_rc rc = VTSS_RC_ERROR;
    T_N("enter, sid %d, port_no: %u", isid, port_no);


    if (isid >= VTSS_ISID_END || port_no >= mesa_port_cnt(0)) {
        return DDMI_ERROR_INV_PARAM;
    }

    if (!msg_switch_is_master()) {
        T_W("Not master");
        rc = DDMI_ERROR_MUST_BE_MASTER;
    } else if (!msg_switch_exists(isid)) {
        T_D("isid: %d not exist", isid);
        T_D("exit, isid: %d", isid);
        rc = DDMI_ERROR_ISID;
    } else {
        DDMI_CRIT_ENTER();
        if (ddmi_state.switches[isid].last_refresh != time(NULL)) {
            ddmi_msg_t *msg = ddmi_msg_alloc(DDMI_MSG_ID_PORT_STATUS_REQ);
            if (msg) {
                vtss_flag_value_t flag = (1 << isid);
                vtss_flag_t *flags = &ddmi_status_flags;
                vtss_tick_count_t wtime = vtss_current_time() + VTSS_OS_MSEC2TICK(3000);
                msg_tx(VTSS_MODULE_ID_DDMI, isid, msg, sizeof(*msg));
                vtss_flag_maskbits(flags, ~flag);
                DDMI_CRIT_EXIT();
                if (vtss_flag_timed_wait(flags, flag, VTSS_FLAG_WAITMODE_OR, wtime) & flag) {
                    DDMI_CRIT_ENTER();
                    *info = ddmi_master_state[isid].ports[port_no];
                    rc = VTSS_OK;
                } else {
                    DDMI_CRIT_ENTER();
                    rc = DDMI_ERROR_TIMEOUT;
                }
            }
        } else {
            /* Up-to date info */
            *info = ddmi_master_state[isid].ports[port_no];
            rc = VTSS_OK;
        }
        /* the DDMI information is flushed when DDMI is disabled, but we need to keep the capability */
        if (DDMI_global.conf.mode != DDMI_MGMT_ENABLED) {
            memset (info, 0, sizeof(*info));
            info->cap = ddmi_master_state[isid].ports[port_no].cap & ~DDMI_CAP_SFP_DETECTED;
        }
        DDMI_CRIT_EXIT();
    }

    T_N("exit");
    return rc;
}


static mesa_rc ddmi_module_invalid(vtss_module_id_t module_id)
{
    if (module_id < VTSS_MODULE_ID_NONE)
        return 0;

    T_E("invalid module_id: %d", module_id);
    return 1;
}

/* ddmi state change registration */
mesa_rc ddmi_state_change_register(vtss_module_id_t module_id, ddmi_state_change_callback_t callback)
{
    mesa_rc             rc = VTSS_OK;
    ddmi_state_change_table_t *table;
    ddmi_state_change_reg_t   *reg;

    if (ddmi_module_invalid(module_id)){
        return VTSS_RC_ERROR;
    }
    DDMI_CB_CRIT_ENTER();
    table = &ddmi_state.register_table;
    if (table->count < DDMI_STATE_CHANGE_REG_MAX) {
        reg = &ddmi_state.register_table.reg[table->count];
        reg->callback = callback;
        reg->module_id = module_id;
        table->count++;
    } else {
        T_E("ddmi state change table full");
        rc = VTSS_RC_OK;
    }
    DDMI_CB_CRIT_EXIT();

    return rc;
}

/* DDMI state change event callbacks */
static void ddmi_state_change_event(mesa_port_no_t port_no,  vtss_appl_ddmi_event_state_t state[])
{
    int i;
    ddmi_state_change_reg_t *reg;

    DDMI_CRIT_EXIT();
    DDMI_CB_CRIT_ENTER();
    for (i = 0; i < DDMI_STATE_CHANGE_REG_MAX; i++) {
        reg = &ddmi_state.register_table.reg[i];
        if (i < ddmi_state.register_table.count) {
            T_N("callback, i: %d (%s), port_no: %u",  i, vtss_module_names[reg->module_id], port_no);
            reg->callback(port_no, state);
        }
    }
    DDMI_CB_CRIT_EXIT();

    /* Enter critical region again */
    DDMI_CRIT_ENTER();

}


/****************************************************************************
 * Module thread
 ****************************************************************************/
/* We create a new thread to do it for instead of in 'Init Modules' thread.
   That we don't need wait a long time in 'Init Modules' thread. */
static void DDMI_thread(vtss_addrword_t data)
{
    mesa_port_no_t     port_no, detect_port = VTSS_PORT_NO_START;
    ddmi_port_state_t  *ddmi_port_ptr = &ddmi_state.ports[0];
    mesa_port_list_t   sfp_status_new, sfp_status_old;
    BOOL               first_sfp = TRUE, changed = FALSE, state_change_flag = FALSE;
    mesa_rc            rc;
    u32                mode;

    vtss_clear(sfp_status_new);
    vtss_clear(sfp_status_old);
    while (1) {
        DDMI_CRIT_ENTER();
        mode = DDMI_global.conf.mode;
        DDMI_CRIT_EXIT();
        if (mode == DDMI_MGMT_ENABLED) {
            for (port_no = VTSS_PORT_NO_START, ddmi_port_ptr = &ddmi_state.ports[port_no]; port_no < ddmi_state.port_count; port_no++, ddmi_port_ptr++) {
                DDMI_CRIT_ENTER();
                if ( !(ddmi_port_ptr->info.cap & DDMI_CAP_A0_SUPPORTED) ) {
                    DDMI_CRIT_EXIT();
                    continue;
                }
                if ( TRUE == first_sfp ) {
                    first_sfp = FALSE;
                    detect_port = port_no;
                }

                if (detect_port == port_no) {
                    sfp_status_old = sfp_status_new;
                    if (meba_sfp_insertion_status_get(board_instance, &sfp_status_new) != VTSS_RC_OK) {
                        T_E("Could not perform a SFP module detect");
                    }
                }

                if (sfp_status_old[port_no] != sfp_status_new[port_no]) {
                    char buf[128], *p = &buf[0];
                    p += snprintf(p, sizeof(buf), "DDMI-MODULE_INSERT_REMOVE: %s SFP module on ", sfp_status_new[port_no] ? "Inserted" : "Removed");
#if defined(VTSS_SW_OPTION_SYSLOG)
                    p += snprintf(p, p - &buf[127] + 1, "Interface %s", SYSLOG_PORT_INFO_REPLACE_KEYWORD);
                    S_PORT_I(VTSS_ISID_LOCAL, port_no, "%s", buf);
#endif /* VTSS_SW_OPTION_SYSLOG */
                    T_D("%s", buf);

                    if (sfp_status_new[port_no]) {
                        ddmi_port_ptr->info.cap |= DDMI_CAP_SFP_DETECTED;
                    } else {
                        ddmi_port_ptr->info.cap &= ~(DDMI_CAP_SFP_DETECTED | DDMI_CAP_A2_SUPPORTED);
                        ddmi_port_ptr->state[0] = DDMI_STATE_END;
                        ddmi_port_ptr->state[1] = DDMI_STATE_END;
                        ddmi_port_ptr->state[2] = DDMI_STATE_END;
                        ddmi_port_ptr->state[3] = DDMI_STATE_END;
                        ddmi_port_ptr->state[4] = DDMI_STATE_END;
                        DDMI_status_update(VTSS_ISID_LOCAL, port_no, VTSS_APPL_DDMI_EVENT_TYPE_TEMPERATURE , NULL);
                        DDMI_status_update(VTSS_ISID_LOCAL, port_no, VTSS_APPL_DDMI_EVENT_TYPE_VOLTAGE, NULL);
                        DDMI_status_update(VTSS_ISID_LOCAL, port_no, VTSS_APPL_DDMI_EVENT_TYPE_BIAS, NULL);
                        DDMI_status_update(VTSS_ISID_LOCAL, port_no, VTSS_APPL_DDMI_EVENT_TYPE_TX_POWER, NULL);
                        DDMI_status_update(VTSS_ISID_LOCAL, port_no, VTSS_APPL_DDMI_EVENT_TYPE_RX_POWER, NULL);

                        ddmi_state_change_event(port_no,  (vtss_appl_ddmi_event_state_t*) &(ddmi_port_ptr->state));
                    }
                    changed = TRUE;
                } else {
                    changed = FALSE;
                }

                //            port_sfp_i2c_lock();
                if ( (changed || !ddmi_port_ptr->detected_done) && (ddmi_port_ptr->info.cap & DDMI_CAP_SFP_DETECTED) ) {
                    rc = read_ddmi_a0(port_no, &ddmi_port_ptr->info);
                    if (VTSS_RC_OK == rc) {
                        T_D("vendor name = %s", ddmi_port_ptr->info.ddmi_a0.vendor_name);        /* 20-35    */
                        T_D("vendor pn = %s", ddmi_port_ptr->info.ddmi_a0.vendor_pn);          /* 40-55    */
                        T_D("vendor sn = %s", ddmi_port_ptr->info.ddmi_a0.vendor_sn);          /* 68-83    */
                        T_D("vendor rev = %s", ddmi_port_ptr->info.ddmi_a0.vendor_rev);         /* 56-59    */
                        T_D("vendor code = %s", ddmi_mode_txt(ddmi_port_ptr->info.ddmi_a0.mode));      /* 3-10    */
                        T_D("dete code = %s", ddmi_port_ptr->info.ddmi_a0.date_code);      /* 84-91    */
                        T_D("vendor type = %x", ddmi_port_ptr->info.ddmi_a0.type);                    /* 92    */
                        ddmi_port_ptr->detected_done = TRUE;
                    } else {
                        //                    T_E("%s", ddmi_error_txt(rc));
                        memset(&ddmi_port_ptr->info.ddmi_a0, 0, sizeof(ddmi_port_ptr->info.ddmi_a0));
                        memset(&ddmi_port_ptr->info.ddmi_a2, 0, sizeof(ddmi_port_ptr->info.ddmi_a2));
                        ddmi_port_ptr->detected_done = FALSE;
                        goto loop_end;
                    }
                }

                if (ddmi_port_ptr->info.cap & DDMI_CAP_A2_SUPPORTED) {
                    rc = read_ddmi_a2(port_no, &ddmi_port_ptr->info, ddmi_port_ptr->state ,&state_change_flag);

                    if(state_change_flag==TRUE){
                        ddmi_state_change_event(port_no,  (vtss_appl_ddmi_event_state_t*) &(ddmi_port_ptr->state));
                        state_change_flag=FALSE;
                    }

                    if (VTSS_RC_OK == rc) {
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.temp_hi_alarm);
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.temp_lo_alarm);
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.temp_hi_warn );
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.temp_lo_warn );

                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.voltage_hi_alarm);
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.voltage_lo_alarm);
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.voltage_hi_warn );
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.voltage_lo_warn );

                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.bias_hi_alarm);
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.bias_lo_alarm);
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.bias_hi_warn );
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.bias_lo_warn );

                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.tx_pwr_hi_alarm);
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.tx_pwr_lo_alarm);
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.tx_pwr_hi_warn );
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.tx_pwr_lo_warn );

                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.rx_pwr_hi_alarm);
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.rx_pwr_lo_alarm);
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.rx_pwr_hi_warn );
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.rx_pwr_lo_warn );

                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.temp_current );
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.voltage_current );
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.bias_current );
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.rx_pwr_current );
                        T_D( "%s", ddmi_port_ptr->info.ddmi_a2.tx_pwr_current );
                    } else {
                        //                    T_E("%s", ddmi_error_txt(rc));
                        memset(&ddmi_port_ptr->info.ddmi_a2, 0, sizeof(ddmi_port_ptr->info.ddmi_a2));
                        goto loop_end;
                    }

                }
loop_end:
                //            port_sfp_i2c_unlock();
                DDMI_CRIT_EXIT();
            }
        }
        VTSS_OS_MSLEEP(1000);
    }
}


/****************************************************************************/
/*  Initialization functions                                                */
/****************************************************************************/

/* Module start */
static void DDMI_start(BOOL init)
{
    ddmi_port_state_t   *ddmi_port_ptr;
    int                 i;

    T_D("enter, init: %d", init);

    if (init) {

        /* Initialize ISID information for all switches */
        ddmi_state.port_count = MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT);
        ddmi_state.register_table.count=0;
        for (i = 0, ddmi_port_ptr = &ddmi_state.ports[i]; i < (int)ddmi_state.port_count; i++, ddmi_port_ptr++) {
            ddmi_port_ptr->port = i;
            if (port_custom_table[i].cap & (MEBA_PORT_CAP_SFP_DETECT | MEBA_PORT_CAP_DUAL_SFP_DETECT) ) {
                ddmi_port_ptr->info.cap = DDMI_CAP_A0_SUPPORTED;
                ddmi_port_ptr->detected_done = TRUE;
                ddmi_port_ptr->state[0] = DDMI_STATE_END;
                ddmi_port_ptr->state[1] = DDMI_STATE_END;
                ddmi_port_ptr->state[2] = DDMI_STATE_END;
                ddmi_port_ptr->state[3] = DDMI_STATE_END;
                ddmi_port_ptr->state[4] = DDMI_STATE_END;
            }
        }

        /* Create semaphore for critical regions */
        critd_init(&DDMI_global.crit, "DDMI_global.crit", VTSS_MODULE_ID_DDMI, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
        critd_init(&DDMI_global.cb_crit, "DDMI_global.cb_crit", VTSS_MODULE_ID_DDMI, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
        vtss_flag_init(&ddmi_status_flags);
        DDMI_CRIT_EXIT();
        DDMI_CB_CRIT_EXIT();
    } else {
        /* Register for stack messages */
        (void) DDMI_stack_register();
    }
    T_D("exit");
}

#ifdef VTSS_SW_OPTION_PRIVATE_MIB
/* Initialize our private mib */
VTSS_PRE_DECLS void ddmi_mib_init(void);
#endif

#ifdef VTSS_SW_OPTION_JSON_RPC
VTSS_PRE_DECLS void vtss_appl_ddmi_json_init(void);
#endif

extern "C" int ddmi_icli_cmd_register();

/* Initialize module */
mesa_rc ddmi_init(vtss_init_data_t *data)
{
    mesa_rc rc = VTSS_OK;
    vtss_isid_t isid = data->isid;

    if (data->cmd == INIT_CMD_EARLY_INIT) {
        /* Initialize and register trace ressources */
        VTSS_TRACE_REG_INIT(&DDMI_trace_reg, DDMI_trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&DDMI_trace_reg);
    }

    T_D("enter, cmd: %d, isid: %u, flags: 0x%x", data->cmd, data->isid, data->flags);

    switch (data->cmd) {
    case INIT_CMD_INIT:
        T_D("INIT");
        DDMI_start(1);
#if defined(VTSS_SW_OPTION_ICFG)
        if ((rc = ddmi_icfg_init()) != VTSS_OK) {
            T_D("Calling ddmi_icfg_init() failed rc = %s", error_txt(rc));
        }
#endif /* VTSS_SW_OPTION_ICFG */

#ifdef VTSS_SW_OPTION_PRIVATE_MIB
        /* Register private mib */
        ddmi_mib_init();
#endif

#ifdef VTSS_SW_OPTION_JSON_RPC
        vtss_appl_ddmi_json_init();
#endif
        ddmi_icli_cmd_register();

        break;

    case INIT_CMD_START:
        T_D("START");
        DDMI_start(0);
        /* Create and start DDMI thread */
        vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
                           DDMI_thread,
                           0,
                           "DDMI_main",
                           NULL,
                           0,
                           &DDMI_thread_handle,
                           &DDMI_thread_block);
        break;

    case INIT_CMD_CONF_DEF:
        T_D("CONF_DEF, isid: %d", isid);
        if (isid == VTSS_ISID_LOCAL) {
            /* Reset local configuration */
        } else if (isid == VTSS_ISID_GLOBAL) {
            /* Reset stack configuration */
            DDMI_CRIT_ENTER();
            DDMI_global.conf.mode = DDMI_MGMT_DEFAULT_MODE;
            DDMI_CRIT_EXIT();
        } else if (VTSS_ISID_LEGAL(isid)) {
            /* Reset switch configuration */
        }
        break;

    case INIT_CMD_MASTER_UP: {
        T_D("MASTER_UP");
        /* Initialize global mode to default setting (no apply) */
        DDMI_CRIT_ENTER();
        DDMI_global.conf.mode = DDMI_MGMT_DEFAULT_MODE;
        DDMI_CRIT_EXIT();
        /* Read stack and switch configuration */
        break;
    }

    case INIT_CMD_MASTER_DOWN:
        T_D("MASTER_DOWN");
        break;

    case INIT_CMD_SWITCH_ADD:
        /* apply configuration if needed */
        T_D("SWITCH_ADD, isid: %d", isid);
        /* Apply all configuration to switch */
        break;

    case INIT_CMD_SWITCH_DEL:
        T_D("SWITCH_DEL, isid: %d", isid);
        break;

    default:
        break;
    }

    T_D("exit");
    return rc;
}

/* Get DDMI defaults */
void ddmi_mgmt_conf_get_default(ddmi_conf_t *conf)
{
    DDMI_CRIT_ENTER();
    conf->mode = DDMI_MGMT_DEFAULT_MODE;
    DDMI_CRIT_EXIT();
}


/**
  * \brief Get the global DDMI configuration.
  *
  * \param glbl_cfg [OUT]: Pointer to structure that receives
  *                        the current configuration.
  *
  * \return
  *    VTSS_OK on success.\n
  *    DDMI_ERROR_INV_PARAM if glbl_cfg is NULL.\n
  *    DDMI_ERROR_MUST_BE_MASTER if called on a slave switch.\n
  */
mesa_rc ddmi_mgmt_conf_get(ddmi_conf_t *glbl_cfg)
{
    if (glbl_cfg == NULL) {
        T_W("not master");
        return DDMI_ERROR_INV_PARAM;
    }
    if (!msg_switch_is_master()) {
        T_D("not master");
        return DDMI_ERROR_MUST_BE_MASTER;
    }

    DDMI_CRIT_ENTER();
    *glbl_cfg = DDMI_global.conf;
    DDMI_CRIT_EXIT();

    return VTSS_OK;
}

/**
  * \brief Set the global DDMI configuration.
  *
  * \param glbl_cfg [IN]: Pointer to structure that contains the
  *                       global configuration to apply to the
  *                       voice VLAN module.
  *
  * \return
  *    VTSS_OK on success.\n
  *    DDMI_ERROR_INV_PARAM if glbl_cfg is NULL or parameters error.\n
  *    DDMI_ERROR_MUST_BE_MASTER if called on a slave switch.\n
  */
mesa_rc ddmi_mgmt_conf_set(ddmi_conf_t *glbl_cfg)
{
    mesa_rc rc = VTSS_OK;

    T_D("enter, mode: %d", glbl_cfg->mode);

    if (glbl_cfg == NULL) {
        T_D("exit");
        T_W("not master");
        return DDMI_ERROR_INV_PARAM;
    }
    if (!msg_switch_is_master()) {
        T_D("exit");
        T_W("not master");
        return DDMI_ERROR_MUST_BE_MASTER;
    }

    DDMI_CRIT_ENTER();
    DDMI_global.conf = *glbl_cfg;
    DDMI_CRIT_EXIT();

    T_D("exit");
    return rc;
}

/**
  * \brief Set the global DDMI configuration.
  *
  * \param glbl_cfg [IN]: Pointer to structure that contains the
  *                       global configuration to apply to the
  *                       voice VLAN module.
  *
  * \return
  *    VTSS_OK on success.\n
  *    DDMI_ERROR_INV_PARAM if glbl_cfg is NULL or parameters error.\n
  *    DDMI_ERROR_MUST_BE_MASTER if called on a slave switch.\n
  */
mesa_rc debug_ddmi_mgmt_state_set(vtss_isid_t isid, mesa_port_no_t port_no, vtss_appl_ddmi_event_type_t type, vtss_appl_ddmi_event_state_t state)
{
    mesa_rc             rc = VTSS_OK;
    ddmi_port_state_t   *ddmi_port_ptr = &ddmi_state.ports[port_no];
    vtss_appl_ddmi_event_entry_t entry;

    if (type > VTSS_APPL_DDMI_EVENT_TYPE_RX_POWER) {
        return VTSS_RC_ERROR;
    }

    if (state > VTSS_APPL_DDMI_EVENT_STATE_LO_ALARM) {
        return VTSS_RC_ERROR;
    }

    if ((ddmi_port_ptr->info.cap & DDMI_CAP_A2_SUPPORTED) == 0) {
        return VTSS_RC_ERROR;
    }

    DDMI_CRIT_ENTER();

    ddmi_port_ptr->state[type] = (ddmi_state_t)state;

    memset(&entry, 0, sizeof(entry));
    entry.state = state;
    DDMI_status_update(VTSS_ISID_LOCAL, port_no, type, &entry);
    DDMI_CRIT_EXIT();

    T_D("exit");
    return rc;
}

BOOL ddmi_sfp_support_detected()
{
    if (lntn_conf_mgmt_sfp_count_get()>0) {
        return TRUE;
    } else {
        return FALSE;
    }
    return FALSE;
}

/*
==============================================================================

    Public APIs in vtss_appl\include\vtss\appl\ddmi.h

==============================================================================
*/
/**
 * \brief Get global configuration.
 *
 * \param globals [OUT] Global configuration.
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_ddmi_config_globals_get(
    vtss_appl_ddmi_config_globals_t     *const globals
)
{
    ddmi_conf_t     conf;

    /* check parameter */
    if (globals == NULL) {
        T_W("globals == NULL\n");
        return VTSS_RC_ERROR;
    }

    /* get */
    if (ddmi_mgmt_conf_get(&conf) != VTSS_RC_OK) {
        return VTSS_RC_ERROR;
    }

    globals->mode = (conf.mode == DDMI_MGMT_ENABLED);

    return VTSS_RC_OK;
}

/**
 * \brief Set global configuration.
 *
 * \param globals [IN] Global configuration.
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_ddmi_config_globals_set(
    const vtss_appl_ddmi_config_globals_t   *const globals
)
{
    ddmi_conf_t     conf;

    /* check parameter */
    if (globals == NULL) {
        T_W("globals == NULL\n");
        return VTSS_RC_ERROR;
    }

    if (globals->mode != TRUE && globals->mode != FALSE) {
        T_W("globals->mode = %u\n", globals->mode);
        return VTSS_RC_ERROR;
    }

    /* get */
    if (ddmi_mgmt_conf_get(&conf) != VTSS_RC_OK) {
        return VTSS_RC_ERROR;
    }

    conf.mode = (globals->mode) ? DDMI_MGMT_ENABLED : DDMI_MGMT_DISABLED;

    /* set */
    if (ddmi_mgmt_conf_set(&conf) != VTSS_RC_OK) {
        return VTSS_RC_ERROR;
    }

    return VTSS_RC_OK;
}

/**
 * \brief Iterate function of interface status table
 *
 * \param prev_ifindex [IN]  ifindex of previous port.
 * \param next_ifindex [OUT] ifindex of next port.
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_ddmi_status_interface_entry_itr(
    const vtss_ifindex_t    *const prev_ifindex,
    vtss_ifindex_t          *const next_ifindex
)
{
    return vtss_appl_iterator_ifindex_front_port(prev_ifindex, next_ifindex);
}

/**
 * \brief Get port status
 *
 * \param ifindex [IN]  ifindex of port
 * \param entry   [OUT] Port status
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_ddmi_status_interface_entry_get(
    vtss_ifindex_t                              ifindex,
    vtss_appl_ddmi_status_interface_entry_t     *const entry
)
{
    vtss_ifindex_elm_t      ife;
    ddmi_port_info_t        info;

    /* check parameter */
    if (entry == NULL) {
        T_W("entry == NULL\n");
        return VTSS_RC_ERROR;
    }

    /* get isid/iport from ifindex and validate them */
    if (vtss_appl_ifindex_port_configurable(ifindex, &ife) != VTSS_RC_OK) {
        return VTSS_RC_ERROR;
    }

    /* reset all */
    memset(entry, 0, sizeof(vtss_appl_ddmi_status_interface_entry_t));

    /* get */
    if (ddmi_port_info_get(ife.isid, ife.ordinal, &info) != VTSS_OK) {
        T_W("ddmi_port_info_get(%u, %u)\n", ife.isid, ife.ordinal);
        return VTSS_RC_ERROR;
    }

    // A0 supported?
    if ((info.cap & DDMI_CAP_A0_SUPPORTED) == 0) {
        return VTSS_RC_OK;
    }
    entry->a0_supported = TRUE;

    // SFP detected?
    if ((info.cap & DDMI_CAP_SFP_DETECTED) == 0) {
        return VTSS_RC_OK;
    }
    entry->a0_sfp_detected = TRUE;

    // Transceiver status information
    strncpy(entry->a0_vendor,        info.ddmi_a0.vendor_name, VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a0_part_number,   info.ddmi_a0.vendor_pn,   VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a0_serial_number, info.ddmi_a0.vendor_sn,   VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a0_revision,      info.ddmi_a0.vendor_rev,  VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a0_date_code,     info.ddmi_a0.date_code,   VTSS_APPL_DDMI_STR_MAX_LEN);

    entry->a0_sfp_type = info.ddmi_a0.mode;

    // A2 supported?
    if ((info.cap & DDMI_CAP_A2_SUPPORTED) == 0) {
        return VTSS_RC_OK;
    }
    entry->a2_supported = TRUE;

    // DDMI status information
    strncpy(entry->a2_temp_current,              info.ddmi_a2.temp_current,  VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_temp_high_alarm_threshold, info.ddmi_a2.temp_hi_alarm, VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_temp_low_alarm_threshold,  info.ddmi_a2.temp_lo_alarm, VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_temp_high_warn_threshold,  info.ddmi_a2.temp_hi_warn,  VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_temp_low_warn_threshold,   info.ddmi_a2.temp_lo_warn,  VTSS_APPL_DDMI_STR_MAX_LEN);

    strncpy(entry->a2_voltage_current,              info.ddmi_a2.voltage_current,  VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_voltage_high_alarm_threshold, info.ddmi_a2.voltage_hi_alarm, VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_voltage_low_alarm_threshold,  info.ddmi_a2.voltage_lo_alarm, VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_voltage_high_warn_threshold,  info.ddmi_a2.voltage_hi_warn,  VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_voltage_low_warn_threshold,   info.ddmi_a2.voltage_lo_warn,  VTSS_APPL_DDMI_STR_MAX_LEN);

    strncpy(entry->a2_tx_bias_current,              info.ddmi_a2.bias_current,  VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_tx_bias_high_alarm_threshold, info.ddmi_a2.bias_hi_alarm, VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_tx_bias_low_alarm_threshold,  info.ddmi_a2.bias_lo_alarm, VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_tx_bias_high_warn_threshold,  info.ddmi_a2.bias_hi_warn,  VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_tx_bias_low_warn_threshold,   info.ddmi_a2.bias_lo_warn,  VTSS_APPL_DDMI_STR_MAX_LEN);

    strncpy(entry->a2_tx_power_current,              info.ddmi_a2.tx_pwr_current,  VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_tx_power_high_alarm_threshold, info.ddmi_a2.tx_pwr_hi_alarm, VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_tx_power_low_alarm_threshold,  info.ddmi_a2.tx_pwr_lo_alarm, VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_tx_power_high_warn_threshold,  info.ddmi_a2.tx_pwr_hi_warn,  VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_tx_power_low_warn_threshold,   info.ddmi_a2.tx_pwr_lo_warn,  VTSS_APPL_DDMI_STR_MAX_LEN);

    strncpy(entry->a2_rx_power_current,              info.ddmi_a2.rx_pwr_current,  VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_rx_power_high_alarm_threshold, info.ddmi_a2.rx_pwr_hi_alarm, VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_rx_power_low_alarm_threshold,  info.ddmi_a2.rx_pwr_lo_alarm, VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_rx_power_high_warn_threshold,  info.ddmi_a2.rx_pwr_hi_warn,  VTSS_APPL_DDMI_STR_MAX_LEN);
    strncpy(entry->a2_rx_power_low_warn_threshold,   info.ddmi_a2.rx_pwr_lo_warn,  VTSS_APPL_DDMI_STR_MAX_LEN);

    return VTSS_RC_OK;
}

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
