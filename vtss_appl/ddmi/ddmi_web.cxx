/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

#include "web_api.h"
#ifdef VTSS_SW_OPTION_PRIV_LVL
#include "vtss_privilege_api.h"
#include "vtss_privilege_web_api.h"
#endif

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_DDMI
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_DDMI

/* =================
 * Trace definitions
 * -------------- */
#include <vtss_module_id.h>
/* ============== */
#include "port_api.h"
#include "ddmi_api.h"

static i32 handler_ddmi_overview(CYG_HTTPD_STATE *p)
{
    int         ct;
    vtss_isid_t         sid = web_retrieve_request_sid(p); /* Includes USID = ISID */
    BOOL                first = TRUE;
    port_iter_t         pit;
    ddmi_port_info_t    info;
    ddmi_a0_t           *a0_info = &info.ddmi_a0;
    ddmi_conf_t         conf;


#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_DDMI)) {
        return -1;
    }
#endif

    ( void ) ddmi_mgmt_conf_get(&conf);
    // Format:
    /* 1/ZyXEL/SFP-SX-D/S111132000061/V1.0/2011-08-10/1000BASE-SX|2/ZyXEL/SFP-SX-D/S111132000061/V1.0/2011-08-10/1000BASE-SX|3/ZyXEL/SFP-SX-D/S111132000061/V1.0/2011-08-10/1000BASE-SX|4/-/-/-/-/-/- */

    cyg_httpd_start_chunked("html");

    (void)port_iter_init(&pit, NULL, sid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
    while (port_iter_getnext(&pit)) {
        if ( VTSS_RC_OK != ddmi_port_info_get(sid, pit.iport, &info) || !(info.cap & DDMI_CAP_A0_SUPPORTED) ) {
            continue;
        }

        if ( TRUE != first) {
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|");
            cyg_httpd_write_chunked(p->outbuffer, ct);
        }

        if (info.cap & DDMI_CAP_SFP_DETECTED && DDMI_MGMT_ENABLED == conf.mode ) {
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%s/%s/%s/%s/%s/%s",
                          pit.uport,
                          a0_info->vendor_name   ,
                          a0_info->vendor_pn   ,
                          a0_info->vendor_sn   ,
                          a0_info->vendor_rev,
                          a0_info->date_code,
                          ddmi_mode_txt(a0_info->mode));
        } else {
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/-/-/-/-/-/-",
                          pit.uport);

        }
        cyg_httpd_write_chunked(p->outbuffer, ct);
        if ( TRUE == first ) {
            first = FALSE;
        }
    }

    cyg_httpd_end_chunked();

    return -1; // Do not further search the file system.
}

#define GET_SID(_port) _port/1000
#define GET_PORTID(_port) _port%1000

static i32 handler_ddmi_detailed(CYG_HTTPD_STATE *p)
{
    int                 ct, val;
    vtss_isid_t         sid = web_retrieve_request_sid(p); /* Includes USID = ISID */
    BOOL                first = TRUE;
    port_iter_t         pit;
    ddmi_port_info_t    info;
    ddmi_a0_t           *a0_info = &info.ddmi_a0;
    ddmi_a2_t           *a2_info = &info.ddmi_a2;
    vtss_uport_no_t     selected_uport = 0;
    mesa_port_no_t      iport = 0;
    ddmi_conf_t         conf;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_DDMI)) {
        return -1;
    }
#endif

    if (cyg_httpd_form_varable_int(p, "port", &val)) {
        selected_uport = val;
    }

    ( void ) ddmi_mgmt_conf_get(&conf);
    // Format:
    /* 8,12,14|id/ZyXEL/SFP-SX-D/S111132000061/V1.0/2011-08-10/1000BASE-SX|1/2/3/4/5|18/2/1/4/5|65535/2/4/4/2|1/2/3/4/5|18/2/1/4/5 */

    cyg_httpd_start_chunked("html");

    (void)port_iter_init(&pit, NULL, sid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
    while (port_iter_getnext(&pit)) {
        if ( VTSS_RC_OK != ddmi_port_info_get(sid, pit.iport, &info) || !(info.cap & DDMI_CAP_A0_SUPPORTED) ) {
            continue;
        }

        if ( TRUE != first) {
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), ",");
            cyg_httpd_write_chunked(p->outbuffer, ct);
        }

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u", pit.uport);
        cyg_httpd_write_chunked(p->outbuffer, ct);
        if ( TRUE == first ) {
            iport = pit.iport;
            first = FALSE;
        }
    }

    if (selected_uport) {
        iport = uport2iport(selected_uport);
    }
    if ( FALSE == first) {
        (void) ddmi_port_info_get(sid, iport, &info);
    } else {
        cyg_httpd_end_chunked();
        return -1;
    }
    if ( info.cap & DDMI_CAP_SFP_DETECTED && DDMI_MGMT_ENABLED == conf.mode ) {
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%u/%s/%s/%s/%s/%s/%s",
                      pit.uport,
                      a0_info->vendor_name ,
                      a0_info->vendor_pn   ,
                      a0_info->vendor_sn   ,
                      a0_info->vendor_rev,
                      a0_info->date_code,
                      ddmi_mode_txt(a0_info->mode));
    } else {
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%u/-/-/-/-/-/-",
                      pit.uport);

    }
    cyg_httpd_write_chunked(p->outbuffer, ct);

    if ( info.cap & DDMI_CAP_A2_SUPPORTED && DDMI_MGMT_ENABLED == conf.mode) {
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%s/%s/%s/%s/%s",
                      a2_info->temp_current,
                      a2_info->temp_hi_alarm ,
                      a2_info->temp_hi_warn  ,
                      a2_info->temp_lo_warn ,
                      a2_info->temp_lo_alarm);
        cyg_httpd_write_chunked(p->outbuffer, ct);
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%s/%s/%s/%s/%s",
                      a2_info->voltage_current,
                      a2_info->voltage_hi_alarm ,
                      a2_info->voltage_hi_warn  ,
                      a2_info->voltage_lo_warn ,
                      a2_info->voltage_lo_alarm);
        cyg_httpd_write_chunked(p->outbuffer, ct);
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%s/%s/%s/%s/%s",
                      a2_info->bias_current,
                      a2_info->bias_hi_alarm ,
                      a2_info->bias_hi_warn  ,
                      a2_info->bias_lo_warn ,
                      a2_info->bias_lo_alarm);
        cyg_httpd_write_chunked(p->outbuffer, ct);
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%s/%s/%s/%s/%s",
                      a2_info->tx_pwr_current,
                      a2_info->tx_pwr_hi_alarm ,
                      a2_info->tx_pwr_hi_warn  ,
                      a2_info->tx_pwr_lo_warn ,
                      a2_info->tx_pwr_lo_alarm);
        cyg_httpd_write_chunked(p->outbuffer, ct);
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%s/%s/%s/%s/%s",
                      a2_info->rx_pwr_current,
                      a2_info->rx_pwr_hi_alarm ,
                      a2_info->rx_pwr_hi_warn  ,
                      a2_info->rx_pwr_lo_warn ,
                      a2_info->rx_pwr_lo_alarm);
        cyg_httpd_write_chunked(p->outbuffer, ct);
    } else {
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|-/-/-/-/-");
        cyg_httpd_write_chunked(p->outbuffer, ct);
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|-/-/-/-/-");
        cyg_httpd_write_chunked(p->outbuffer, ct);
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|-/-/-/-/-");
        cyg_httpd_write_chunked(p->outbuffer, ct);
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|-/-/-/-/-");
        cyg_httpd_write_chunked(p->outbuffer, ct);
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|-/-/-/-/-");
        cyg_httpd_write_chunked(p->outbuffer, ct);

    }
    cyg_httpd_end_chunked();

    return -1; // Do not further search the file system.
}

static i32 handler_config_ddmi(CYG_HTTPD_STATE *p)
{
    int        ct;
    ddmi_conf_t ddmi_conf, newconf;
    int        var_value;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_DDMI)) {
        return -1;
    }
#endif

    if (p->method == CYG_HTTPD_METHOD_POST) {
        /* store form data */
        if (ddmi_mgmt_conf_get(&ddmi_conf) == VTSS_OK) {
            newconf = ddmi_conf;

            //ddmi_mode
            if (cyg_httpd_form_varable_int(p, "ddmi_mode", &var_value)) {
                newconf.mode = var_value;
            }

            if (memcmp(&newconf, &ddmi_conf, sizeof(newconf)) != 0) {
                T_D("Calling ddmi_mgmt_conf_set()");
                if (ddmi_mgmt_conf_set(&newconf) < 0) {
                    T_E("ddmi_mgmt_conf_set(): failed");
                }
            }
        }
        if (strcmp(web_ver(p), "2.0") == 0) {
            send_custom_textplain_resp(p, 200, "");
        } else {
            redirect(p, "/ddmi_config.htm");
        }
    } else {                    /* CYG_HTTPD_METHOD_GET (+HEAD) */
        (void)cyg_httpd_start_chunked("html");

        /* get form data
           Format: [ddmi_mode]
        */
        if (ddmi_mgmt_conf_get(&ddmi_conf) == VTSS_OK) {
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d", ddmi_conf.mode);
            (void)cyg_httpd_write_chunked(p->outbuffer, ct);
        }
        (void)cyg_httpd_end_chunked();
    }

    return -1; // Do not further search the file system.
}

/*  Module JS lib config routine                                            */
/****************************************************************************/

/****************************************************************************/
/*  JS lib config table entry                                               */
/****************************************************************************/

CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_ddmi_overview, "/stat/ddmi_overview", handler_ddmi_overview);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_ddmi_detail, "/stat/ddmi_detailed", handler_ddmi_detailed);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_config_ddmi, "/config/ddmi", handler_config_ddmi);

