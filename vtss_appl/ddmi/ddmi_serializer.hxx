/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#ifndef __VTSS_DDMI_SERIALIZER_HXX__
#define __VTSS_DDMI_SERIALIZER_HXX__

#include "vtss_appl_serialize.hxx"
#include "vtss/appl/ddmi.h"
#include "vtss_appl_formatting_tags.hxx" // for AsInterfaceIndex
#include "vtss/appl/port.h"

extern vtss::expose::TableStatus <
vtss::expose::ParamKey<vtss_ifindex_t >,
vtss::expose::ParamKey<vtss_appl_ddmi_event_type_t >,
     vtss::expose::ParamVal<vtss_appl_ddmi_event_entry_t *>
     > ddmi_status_update;

/*****************************************************************************
    Enum serializer
*****************************************************************************/
extern vtss_enum_descriptor_t vtss_appl_ddmi_event_type_txt[];
VTSS_XXXX_SERIALIZE_ENUM(
    vtss_appl_ddmi_event_type_t,
    "mirrorSessionType",
    vtss_appl_ddmi_event_type_txt,
    "This enumeration defines the session type in mirror function.");

extern vtss_enum_descriptor_t vtss_appl_ddmi_event_state_txt[];
VTSS_XXXX_SERIALIZE_ENUM(
    vtss_appl_ddmi_event_state_t,
    "mirrorSessionType",
    vtss_appl_ddmi_event_state_txt,
    "This enumeration defines the session type in mirror function.");

/*****************************************************************************
    Index serializer
*****************************************************************************/
VTSS_SNMP_TAG_SERIALIZE(vtss_ddmi_ifindex_index, vtss_ifindex_t, a, s) {
    a.add_leaf(
        vtss::AsInterfaceIndex(s.inner),
        vtss::tag::Name("IfIndex"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(0),
        vtss::tag::Description("Logical interface number of physical port.")
    );
}

VTSS_SNMP_TAG_SERIALIZE(vtss_appl_ddmi_event_type, vtss_appl_ddmi_event_type_t, a, s) {
    a.add_leaf(
        s.inner,
        vtss::tag::Name("Type"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(0),
        vtss::tag::Description("Current value when the event occurs.")
    );

}

/*****************************************************************************
    Data serializer
*****************************************************************************/
template<typename T>
void serialize(T &a, vtss_appl_ddmi_config_globals_t &s)
{
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ddmi_config_globals_t"));

    int ix = 0;

    m.add_leaf(
        vtss::AsBool(s.mode),
        vtss::tag::Name("Mode"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Global operation mode of DDMI. "
            "true is to enable the functions of DDMI and false is to disable it.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ddmi_status_interface_entry_t &s)
{
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ddmi_status_interface_entry_t"));

    int ix = 0;

    m.add_leaf(
        vtss::AsBool(s.a0_supported),
        vtss::tag::Name("A0Supported"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Support transceiver status information or not. "
            "true is to supported and false is not supported.")
    );

    m.add_leaf(
        vtss::AsBool(s.a0_sfp_detected),
        vtss::tag::Name("A0SfpDetected"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("SFP module is detected or not. "
            "true is to detected and false is not detected.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a0_vendor, sizeof(s.a0_vendor)),
        vtss::tag::Name("A0Vendor"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Vendor name.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a0_part_number, sizeof(s.a0_part_number)),
        vtss::tag::Name("A0PartNumber"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Part number.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a0_serial_number, sizeof(s.a0_serial_number)),
        vtss::tag::Name("A0SerialNumber"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Serial number.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a0_revision, sizeof(s.a0_revision)),
        vtss::tag::Name("A0Revision"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Revision.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a0_date_code, sizeof(s.a0_date_code)),
        vtss::tag::Name("A0DateCode"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Date Code.")
    );

    m.add_leaf(
        s.a0_sfp_type,
        vtss::tag::Name("A0SfpType"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("SFP type. "
            "none(0) means no SFP module. "
            "notSupported(1) means SFP module is not supported. "
            "sfp100Fx(2) means SFP 100BASE-FX. "
            "sfp100BaseLx(3) means SFP 100BASE-LX. "
            "sfp100BaseBx10(4) means SFP 100BASE-BX10. "
            "sfp100BaseT(5) means SFP 100BASE-T. "
            "sfp1000BaseBx10(6) means SFP 1000BASE-BX10. "
            "sfp1000BaseT(7) means SFP 1000BASE-T. "
            "sfp1000BaseCx(8) means SFP 1000BASE-CX. "
            "sfp1000BaseSx(9) means SFP 1000BASE-SX. "
            "sfp1000BaseLx(10) means SFP 1000BASE-LX. "
            "sfp1000BaseX(11) means SFP 1000BASE-X. "
            "sfp2G5(12) means SFP 2.5G. "
            "sfp5G(13) means SFP 5G. "
            "sfp10G(14) means SFP 10G.")
    );

    /* Separating A0 and A2 data to extend in future */
    ix = 1000;

    m.add_leaf(
        vtss::AsBool(s.a2_supported),
        vtss::tag::Name("A2Supported"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Support DDMI status information or not. "
            "true is to supported and false is not supported.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_temp_current, sizeof(s.a2_temp_current)),
        vtss::tag::Name("A2CurrentTemperature"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Current temperature in Celcius.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_temp_high_alarm_threshold, sizeof(s.a2_temp_high_alarm_threshold)),
        vtss::tag::Name("A2TemperatureHighAlarmThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Temperature high alarm threshold in Celcius.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_temp_low_alarm_threshold, sizeof(s.a2_temp_low_alarm_threshold)),
        vtss::tag::Name("A2TemperatureLowAlarmThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Temperature low alarm threshold in Celcius.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_temp_high_warn_threshold, sizeof(s.a2_temp_high_warn_threshold)),
        vtss::tag::Name("A2TemperatureHighWarnThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Temperature high warning threshold in Celcius.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_temp_low_warn_threshold, sizeof(s.a2_temp_low_warn_threshold)),
        vtss::tag::Name("A2TemperatureLowWarnThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Temperature low warning threshold in Celcius.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_voltage_current, sizeof(s.a2_voltage_current)),
        vtss::tag::Name("A2CurrentVoltage"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Current voltage in Volt.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_voltage_high_alarm_threshold, sizeof(s.a2_voltage_high_alarm_threshold)),
        vtss::tag::Name("A2VoltageHighAlarmThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Voltage high alarm threshold in Volt.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_voltage_low_alarm_threshold, sizeof(s.a2_voltage_low_alarm_threshold)),
        vtss::tag::Name("A2VoltageLowAlarmThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Voltage low alarm threshold in Volt.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_voltage_high_warn_threshold, sizeof(s.a2_voltage_high_warn_threshold)),
        vtss::tag::Name("A2VoltageHighWarnThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Voltage high warning threshold in Volt.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_voltage_low_warn_threshold, sizeof(s.a2_voltage_low_warn_threshold)),
        vtss::tag::Name("A2VoltageLowWarnThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Voltage low warning threshold in Volt.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_tx_bias_current, sizeof(s.a2_tx_bias_current)),
        vtss::tag::Name("A2CurrentTxBias"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Current Tx bias in mA.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_tx_bias_high_alarm_threshold, sizeof(s.a2_tx_bias_high_alarm_threshold)),
        vtss::tag::Name("A2TxBiasHighAlarmThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Tx bias high alarm threshold in mA.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_tx_bias_low_alarm_threshold, sizeof(s.a2_tx_bias_low_alarm_threshold)),
        vtss::tag::Name("A2TxBiasLowAlarmThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Tx bias low alarm threshold in mA.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_tx_bias_high_warn_threshold, sizeof(s.a2_tx_bias_high_warn_threshold)),
        vtss::tag::Name("A2TxBiasHighWarnThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Tx bias high warning threshold in mA.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_tx_bias_low_warn_threshold, sizeof(s.a2_tx_bias_low_warn_threshold)),
        vtss::tag::Name("A2TxBiasLowWarnThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Tx bias low warning threshold in mA.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_tx_power_current, sizeof(s.a2_tx_power_current)),
        vtss::tag::Name("A2CurrentTxPower"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Current Tx power in mW.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_tx_power_high_alarm_threshold, sizeof(s.a2_tx_power_high_alarm_threshold)),
        vtss::tag::Name("A2TxPowerHighAlarmThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Tx power high alarm threshold in mW.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_tx_power_low_alarm_threshold, sizeof(s.a2_tx_power_low_alarm_threshold)),
        vtss::tag::Name("A2TxPowerLowAlarmThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Tx power low alarm threshold in mW.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_tx_power_high_warn_threshold, sizeof(s.a2_tx_power_high_warn_threshold)),
        vtss::tag::Name("A2TxPowerHighWarnThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Tx power high warning threshold in mW.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_tx_power_low_warn_threshold, sizeof(s.a2_tx_power_low_warn_threshold)),
        vtss::tag::Name("A2TxPowerLowWarnThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Tx power low warning threshold in mW.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_rx_power_current, sizeof(s.a2_rx_power_current)),
        vtss::tag::Name("A2CurrentRxPower"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Current Rx power in mW.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_rx_power_high_alarm_threshold, sizeof(s.a2_rx_power_high_alarm_threshold)),
        vtss::tag::Name("A2RxPowerHighAlarmThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Rx power high alarm threshold in mW.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_rx_power_low_alarm_threshold, sizeof(s.a2_rx_power_low_alarm_threshold)),
        vtss::tag::Name("A2RxPowerLowAlarmThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Rx power low alarm threshold in mW.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_rx_power_high_warn_threshold, sizeof(s.a2_rx_power_high_warn_threshold)),
        vtss::tag::Name("A2RxPowerHighWarnThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Rx power high warning threshold in mW.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.a2_rx_power_low_warn_threshold, sizeof(s.a2_rx_power_low_warn_threshold)),
        vtss::tag::Name("A2RxPowerLowWarnThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Rx power low warning threshold in mW.")
    );
}

#if 0
VTSS_SNMP_TAG_SERIALIZE(vtss_appl_ddmi_event_index, vtss_appl_ddmi_event_index_t, a, s)
{
    int ix = 0;

    a.add_leaf(
        vtss::AsInterfaceIndex(s.inner.ifindex),
        vtss::tag::Name("IfIndex"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(0),
        vtss::tag::Description("Logical interface number of physical port.")
    );

    a.add_leaf(
        s.inner.type,
        vtss::tag::Name("Type"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Current value when the event occurs.")
    );
}
#endif


template<typename T>
void serialize(T &a, vtss_appl_ddmi_event_entry_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ddmi_event_entry_t"));
    int ix = 0;

    m.add_leaf(
        s.state,
        vtss::tag::Name("State"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Current Rx power in mV.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.current, sizeof(s.current)),
        vtss::tag::Name("Current"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The current value when the event occurs.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.high_alarm_threshold, sizeof(s.high_alarm_threshold)),
        vtss::tag::Name("HighAlarmThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The High alarm threshold when the event occurs.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.low_alarm_threshold, sizeof(s.low_alarm_threshold)),
        vtss::tag::Name("LowAlarmThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The Low alarm threshold when the event occurs.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.high_warn_threshold, sizeof(s.high_warn_threshold)),
        vtss::tag::Name("HighWarnThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The high warning threshold when the event occurs.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.low_warn_threshold, sizeof(s.low_warn_threshold)),
        vtss::tag::Name("LowWarnThreshold"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The low warning threshold when the event occurs.")
    );
}

namespace vtss {
namespace appl {
namespace ddmi {
namespace interfaces {
struct ddmiConfigGlobalsLeaf {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_appl_ddmi_config_globals_t *>
    > P;

    static constexpr const char *index_description =
        "Each entry has a set of parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ddmi_config_globals_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ddmi_config_globals_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ddmi_config_globals_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_DDMI);
};

struct ddmiStatusInterfaceEntry {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t>,
        vtss::expose::ParamVal<vtss_appl_ddmi_status_interface_entry_t *>
    > P;

    static constexpr const char *table_description =
        "This is a DDMI status table of port interface.";

    static constexpr const char *index_description =
        "Each entry has a set of DDMI status.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, vtss_ddmi_ifindex_index(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ddmi_status_interface_entry_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ddmi_status_interface_entry_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ddmi_status_interface_entry_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_DDMI);
};

struct DdmiStatusTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t>,
        vtss::expose::ParamKey<vtss_appl_ddmi_event_type_t>,
        vtss::expose::ParamVal<vtss_appl_ddmi_event_entry_t *>
    > P;

    static constexpr const char *table_description =
        "The table is DDMI status table. The index is ifindex and event type.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, vtss_ddmi_ifindex_index(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ddmi_event_type_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, vtss_appl_ddmi_event_type(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_ddmi_event_entry_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ddmi_event_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ddmi_event_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_DDMI);
};

}  // namespace interfaces
}  // namespace ddmi
}  // namespace appl
}  // namespace vtss


#endif /* __VTSS_DDMI_SERIALIZER_HXX__ */
