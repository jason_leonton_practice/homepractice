/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

/**
* \file
* \brief Public IPMC Library Hader File
* \details This header file describes IPMC Library constant and enum
*/

#ifndef _VTSS_APPL_IPMC_LIB_PUBLIC_H_
#define _VTSS_APPL_IPMC_LIB_PUBLIC_H_

#ifdef __cplusplus
extern "C" {
#endif

#define VTSS_APPL_IPMC_DISABLE              0x0     /**< Disable IPMC related setting */
#define VTSS_APPL_IPMC_ENABLE               0x1     /**< Enable IPMC related setting */

#define VTSS_APPL_IPMC_TRUE                 0x1     /**< True condition for IPMC */
#define VTSS_APPL_IPMC_FALSE                0x0     /**< False condition for IPMC */

#define VTSS_APPL_IPMC_VID_NULL             0x0     /**< Empty VLAN ID used in IPMC */
#define VTSS_APPL_IPMC_VID_MAX              0xFFF   /**< Maximum VLAN ID used in IPMC */
#define VTSS_APPL_IPMC_VID_ALL              0x1FFF  /**< A value to present all the IPMC VLAN */
#define VTSS_APPL_IPMC_VID_VOID             0xFFFF  /**< A value to present the IPMC VLAN is not yet ready */

#define VTSS_APPL_IPMC_NAME_STRING_MAX_LEN  16      /**< Maximum number of characters used for IPMC name */
#define VTSS_APPL_IPMC_DESC_STRING_MAX_LEN  64      /**< Maximum number of characters used for IPMC description */
#define VTSS_APPL_IPMC_NAME_MAX_LEN         (VTSS_APPL_IPMC_NAME_STRING_MAX_LEN + 1)    /**< String length for IPMC name */
#define VTSS_APPL_IPMC_DESC_MAX_LEN         (VTSS_APPL_IPMC_DESC_STRING_MAX_LEN + 1)    /**< String length for IPMC description */
#define VTSS_APPL_IPMC_MVR_NAME_MAX_LEN     (VTSS_APPL_IPMC_NAME_STRING_MAX_LEN + 1)    /**< String length for MVR name */

/*! \brief IPMC profiling action type */
typedef enum {
    VTSS_APPL_IPMC_ACTION_DENY = 0,         /**< IPMC will perform deny operation against matched condition */
    VTSS_APPL_IPMC_ACTION_PERMIT            /**< IPMC will perform permit operation against matched condition */
} vtss_appl_ipmc_action_t;

/*! \brief IPMC version compatiblity type */
typedef enum {
    VTSS_APPL_IPMC_COMPATIBILITY_AUTO = 0,  /**< IPMC compatibility is automatically determined */
    VTSS_APPL_IPMC_COMPATIBILITY_OLD,       /**< IPMC compatibility is forced as IGMPv1 */
    VTSS_APPL_IPMC_COMPATIBILITY_GEN,       /**< IPMC compatibility is forced as IGMPv2/MLDv1 */
    VTSS_APPL_IPMC_COMPATIBILITY_SFM        /**< IPMC compatibility is forced as IGMPv3/MLDv2 */
} vtss_appl_ipmc_compatibility_t;

/*! \brief IPMC Querier state type */
typedef enum {
    VTSS_APPL_IPMC_QUERIER_DISABLED = -1,   /**< Interface Querier is in disabled state */
    VTSS_APPL_IPMC_QUERIER_INIT,            /**< Interface Querier is in initialization state */
    VTSS_APPL_IPMC_QUERIER_IDLE,            /**< Interface Querier is in inactive state */
    VTSS_APPL_IPMC_QUERIER_ACTIVE           /**< Interface Querier is in active state */
} vtss_appl_ipmc_querier_states_t;

/*! \brief IPMC Source Filtering Multicast mode */
typedef enum {
    VTSS_APPL_IPMC_SF_MODE_EXCLUDE = 0,     /**< Source Filtering Mode is Exclude */
    VTSS_APPL_IPMC_SF_MODE_INCLUDE,         /**< Source Filtering Mode is Include */
    VTSS_APPL_IPMC_SF_MODE_NONE             /**< Source Filtering Mode is NONE */
} vtss_appl_ipmc_sfm_mode_t;

/*! \brief IPMC interface VLAN tag type */
typedef enum {
    VTSS_APPL_IPMC_INTF_UNTAG = 0,          /**< IPMC control frames will be sent as untagged */
    VTSS_APPL_IPMC_INTF_TAGED               /**< IPMC control frames will be sent as tagged */
} vtss_appl_ipmc_intf_vtag_t;

/*! \brief IPMC MVR interface operational mode */
typedef enum {
    VTSS_APPL_IPMC_MVR_INTF_MODE_INIT = -1, /**< IPMC MVR interface is in initialized mode */
    VTSS_APPL_IPMC_MVR_INTF_MODE_DYNAMIC,   /**< IPMC MVR interface works in dynamic mode */
    VTSS_APPL_IPMC_MVR_INTF_MODE_COMPATIBLE /**< IPMC MVR interface works in compatible mode */
} vtss_appl_ipmc_mvr_intf_mode_t;

/*! \brief IPMC MVR port operational role */
typedef enum {
    VTSS_APPL_IPMC_MVR_PORT_ROLE_INACT = 0, /**< IPMC MVR port is inactive */
    VTSS_APPL_IPMC_MVR_PORT_ROLE_SOURCE,    /**< IPMC MVR port acts as a source port */
    VTSS_APPL_IPMC_MVR_PORT_ROLE_RECEIVER,  /**< IPMC MVR port acts as a receiver port */
    VTSS_APPL_IPMC_MVR_PORT_ROLE_STACKING   /**< IPMC MVR port is a stacking port */
} vtss_appl_ipmc_mvr_port_role_t;

#ifdef __cplusplus
}
#endif

#endif  /* _VTSS_APPL_IPMC_LIB_PUBLIC_H_ */
