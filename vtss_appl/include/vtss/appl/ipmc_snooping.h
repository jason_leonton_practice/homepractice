/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

/**
* \file
* \brief Public IPMC Snooping API
* \details This header file describes IPMC Snooping control functions and types.
*/

#ifndef _VTSS_APPL_IPMC_SNOOPING_H_
#define _VTSS_APPL_IPMC_SNOOPING_H_

#include <vtss/appl/interface.h>
#include <vtss/appl/types.h>
#include <vtss/appl/ipmc_lib_public.h>
#include <vtss/appl/ipmc_profile.h>

#ifdef __cplusplus
extern "C" {
#endif

/*! \brief IGMP filtering action type */
typedef enum {
    VTSS_APPL_IPMC_ACTION4_DENY = 0,        /**< IGMP will perform deny operation against matched source address */
    VTSS_APPL_IPMC_ACTION4_PERMIT           /**< IGMP will perform permit operation against matched source address */
} vtss_appl_ipmc_action4_t;

/*! \brief MLD filtering action type */
typedef enum {
    VTSS_APPL_IPMC_ACTION6_DENY = 0,        /**< MLD will perform deny operation against matched source address */
    VTSS_APPL_IPMC_ACTION6_PERMIT           /**< MLD will perform permit operation against matched source address */
} vtss_appl_ipmc_action6_t;

/*! \brief IGMP router status type */
typedef enum {
    VTSS_APPL_IPMC_MROUTER4_NONE = 0,       /* No IGMP router is determined */
    VTSS_APPL_IPMC_MROUTER4_STATIC,         /* Static IGMP router is configured */
    VTSS_APPL_IPMC_MROUTER4_DYNAMIC,        /* Dynamic IGMP router is detected */
    VTSS_APPL_IPMC_MROUTER4_BOTH            /* Both static and dynamic IGMP router is determined */
} vtss_appl_ipmc_mrtr4_status_t;

/*! \brief MLD router status type */
typedef enum {
    VTSS_APPL_IPMC_MROUTER6_NONE = 0,       /* No MLD router is determined */
    VTSS_APPL_IPMC_MROUTER6_STATIC,         /* Static MLD router is configured */
    VTSS_APPL_IPMC_MROUTER6_DYNAMIC,        /* Dynamic MLD router is detected */
    VTSS_APPL_IPMC_MROUTER6_BOTH            /* Both static and dynamic MLD router is determined */
} vtss_appl_ipmc_mrtr6_status_t;

/*! \brief IGMP version compatiblity type */
typedef enum {
    VTSS_APPL_IPMC_COMPATI4_AUTO = 0,       /**< IGMP compatibility is automatically determined */
    VTSS_APPL_IPMC_COMPATI4_V1,             /**< IGMP compatibility is forced as IGMPv1 */
    VTSS_APPL_IPMC_COMPATI4_V2,             /**< IGMP compatibility is forced as IGMPv2 */
    VTSS_APPL_IPMC_COMPATI4_V3              /**< IGMP compatibility is forced as IGMPv3 */
} vtss_appl_ipmc_compati4_t;

/*! \brief MLD version compatiblity type */
typedef enum {
    VTSS_APPL_IPMC_COMPATI6_AUTO = 0,       /**< MLD compatibility is automatically determined */
    VTSS_APPL_IPMC_COMPATI6_V1,             /**< MLD compatibility is forced as MLDv1 */
    VTSS_APPL_IPMC_COMPATI6_V2              /**< MLD compatibility is forced as MLDv2 */
} vtss_appl_ipmc_compati6_t;

/*! \brief IGMP Querier state type */
typedef enum {
    VTSS_APPL_IPMC_QUERIER4_DISABLED = 0,   /**< IGMP Interface is currently disabled */
    VTSS_APPL_IPMC_QUERIER4_INIT,           /**< IGMP Interface Querier is in initialization state */
    VTSS_APPL_IPMC_QUERIER4_IDLE,           /**< IGMP Interface Querier is in inactive state */
    VTSS_APPL_IPMC_QUERIER4_ACTIVE          /**< IGMP Interface Querier is in active state */
} vtss_appl_ipmc_qry4_states_t;

/*! \brief MLD Querier state type */
typedef enum {
    VTSS_APPL_IPMC_QUERIER6_DISABLED = 0,   /**< MLD Interface is currently disabled */
    VTSS_APPL_IPMC_QUERIER6_INIT,           /**< MLD Interface Querier is in initialization state */
    VTSS_APPL_IPMC_QUERIER6_IDLE,           /**< MLD Interface Querier is in inactive state */
    VTSS_APPL_IPMC_QUERIER6_ACTIVE          /**< MLD Interface Querier is in active state */
} vtss_appl_ipmc_qry6_states_t;

/*! \brief IGMP Source Filtering Multicast mode */
typedef enum {
    VTSS_APPL_IPMC_SF4_MODE_EXCLUDE = 0,    /**< IGMP Source Filtering Mode is Exclude */
    VTSS_APPL_IPMC_SF4_MODE_INCLUDE,        /**< IGMP Source Filtering Mode is Include */
    VTSS_APPL_IPMC_SF4_MODE_NONE            /**< IGMP Source Filtering Mode is NONE */
} vtss_appl_ipmc_sfm4_mode_t;

/*! \brief MLD Source Filtering Multicast mode */
typedef enum {
    VTSS_APPL_IPMC_SF6_MODE_EXCLUDE = 0,    /**< MLD Source Filtering Mode is Exclude */
    VTSS_APPL_IPMC_SF6_MODE_INCLUDE,        /**< MLD Source Filtering Mode is Include */
    VTSS_APPL_IPMC_SF6_MODE_NONE            /**< MLD Source Filtering Mode is NONE */
} vtss_appl_ipmc_sfm6_mode_t;

/**
 * \brief IPMC Snooping IGMP global configuration
 * The configuration is the system configuration that can manage
 * the IGMP Snooping functions.
 */
typedef struct {
    /**
     * \brief Global administrative mode, TRUE is to enable IGMP snooping
     * function in the system and FALSE is to disable it.
     */
    mesa_bool_t                            mode;

    /**
     * \brief Unregistered IPv4 multicast flooding mode, TRUE is to enable
     * the flooding of IPv4 multicast traffic destined to unregistered group
     * address and FALSE is to disable it.
     */
    mesa_bool_t                            unregistered_flooding;

#ifdef VTSS_SW_OPTION_SMB_IPMC
    /**
     * \brief ssm_address is used to denote IPv4 prefix for multicast addresses
     * that follow SSM service model in group registration.
     */
    mesa_ipv4_t                     ssm_address;

    /**
     * \brief ssm_address is used to denote prefix length for IPv4 multicast
     * addresses that follow SSM service model in group registration.
     */
    uint32_t                             ssm_mask_len;

    /**
     * \brief IGMP proxy administrative mode, TRUE is to enable IGMP proxy
     * function in the system and FALSE is to disable it.
     */
    mesa_bool_t                            proxy;

    /**
     * \brief IGMP proxy for leave adminstrative mode, TRUE is to enable
     * IGMP proxy for leve function in the system and FALSE is to disable it.
     */
    mesa_bool_t                            leave_proxy;
#endif /* VTSS_SW_OPTION_SMB_IPMC */
} vtss_appl_ipmc_snp_ipv4_global_t;

#ifdef VTSS_SW_OPTION_SMB_IPMC
/**
 * \brief IPMC Snooping MLD global configuration
 * The configuration is the system configuration that can manage
 * the MLD Snooping functions.
 */
typedef struct {
    /**
     * \brief Global administrative mode, TRUE is to enable MLD snooping
     * function in the system and FALSE is to disable it.
     */
    mesa_bool_t                            mode;

    /**
     * \brief Unregistered IPv6 multicast flooding mode, TRUE is to enable
     * the flooding of IPv6 multicast traffic destined to unregistered group
     * address and FALSE is to disable it.
     */
    mesa_bool_t                            unregistered_flooding;

    /**
     * \brief ssm_address is used to denote IPv6 prefix for multicast addresses
     * that follow SSM service model in group registration.
     */
    mesa_ipv6_t                     ssm_address;

    /**
     * \brief ssm_address is used to denote prefix length for IPv6 multicast
     * addresses that follow SSM service model in group registration.
     */
    uint32_t                             ssm_mask_len;

    /**
     * \brief MLD proxy administrative mode, TRUE is to enable MLD proxy
     * function in the system and FALSE is to disable it.
     */
    mesa_bool_t                            proxy;

    /**
     * \brief MLD proxy for leave adminstrative mode, TRUE is to enable
     * MLD proxy for leve function in the system and FALSE is to disable it.
     */
    mesa_bool_t                            leave_proxy;
} vtss_appl_ipmc_snp_ipv6_global_t;
#endif /* VTSS_SW_OPTION_SMB_IPMC */

/**
 * \brief IGMP Snooping port configuration
 * The configuration is the per-port basis configuration that can enhance
 * the IGMP Snooping functions.
 */
typedef struct {
    /**
     * \brief as_router_port is used to denote the specific port to be a
     * static router port for IGMP snooping.
     */
    mesa_bool_t                            as_router_port;

    /**
     * \brief do_fast_leave is used to denote the specific port to block the
     * multicast traffic upon receiving the leave/done IGMP control messages.
     */
    mesa_bool_t                            do_fast_leave;

#ifdef VTSS_SW_OPTION_SMB_IPMC
    /**
     * \brief throttling_number is used to denote the maximum amount of multicast
     * group registration on a specific port.
     */
    int32_t                             throttling_number;

    /**
     * \brief filtering_profile is used to assign the IPMC profile for filtering
     * multicast group registration on a specific port.
     */
    vtss_appl_ipmc_name_index_t     filtering_profile;
#endif /* VTSS_SW_OPTION_SMB_IPMC */
} vtss_appl_ipmc_snp4_port_conf_t;

/**
 * \brief IPMC Snooping IGMP interface configuration
 * The configuration is the interface configuration for running
 * the IGMP Snooping functions.
 */
typedef struct {
    /**
     * \brief Interface administrative mode, TRUE is to enable IGMP snooping
     * function for the interface and FALSE is to disable it.
     */
    mesa_bool_t                            admin_state;

    /**
     * \brief querier_election is used to denote whether the specific IGMP
     * snooping interface joins the Querier election or not.
     */
    mesa_bool_t                            querier_election;

    /**
     * \brief querier_address is used to denote the static IPv4 source address
     * of the specific IGMP interface for seding IGMP Query message with respect
     * to IGMP Querier election.
     */
    mesa_ipv4_t                     querier_address;

#ifdef VTSS_SW_OPTION_SMB_IPMC
    /**
     * \brief compatibility is used to denote the way for version compatility
     * determination in IGMP snooping.
     */
    vtss_appl_ipmc_compati4_t       compatibility;

    /**
     * \brief priority is used for prioritizing the IGMP control frames to be
     * sent for IGMP snooping.
     */
    uint8_t                              priority;

    /**
     * \brief robustness_variable presents the "Robustness Variable", as
     * stated in RFC-3376 8.1, for an IGMP interface running IGMP protocol
     * while snooping IGMP control frames.
     */
    uint32_t                             robustness_variable;

    /**
     * \brief query_interval presents the "Query Interval", as stated in
     * RFC-3376 8.2, for an IGMP interface running IGMP protocol while
     * snooping IGMP control frames.
     */
    uint32_t                             query_interval;

    /**
     * \brief query_response_interval presents the "Query Response Interval",
     * as stated in RFC-3376 8.3, for an IGMP interface running IGMP protocol
     * while snooping IGMP control frames.
     */
    uint32_t                             query_response_interval;

    /**
     * \brief last_listener_query_interval presents the "Last Member Query
     * Interval", as stated in RFC-3376 8.8, for an IGMP interface running
     * IGMP protocol while snooping IGMP control frames.
     */
    uint32_t                             last_listener_query_interval;

    /**
     * \brief unsolicited_report_interval presents the "Unsolicited Report
     * Interval", as stated in RFC-3376 8.11, for an IGMP interface running
     * IGMP protocol while snooping IGMP control frames.
     */
    uint32_t                             unsolicited_report_interval;
#endif /* VTSS_SW_OPTION_SMB_IPMC */
} vtss_appl_ipmc_snp_ipv4_interface_t;

#ifdef VTSS_SW_OPTION_SMB_IPMC
/**
 * \brief MLD Snooping port configuration
 * The configuration is the per-port basis configuration that can enhance
 * the MLD Snooping functions.
 */
typedef struct {
    /**
     * \brief as_router_port is used to denote the specific port to be a
     * static router port for MLD snooping.
     */
    mesa_bool_t                            as_router_port;

    /**
     * \brief do_fast_leave is used to denote the specific port to block the
     * multicast traffic upon receiving the leave/done MLD control messages.
     */
    mesa_bool_t                            do_fast_leave;

    /**
     * \brief throttling_number is used to denote the maximum amount of multicast
     * group registration on a specific port.
     */
    int32_t                             throttling_number;

    /**
     * \brief filtering_profile is used to assign the IPMC profile for filtering
     * multicast group registration on a specific port.
     */
    vtss_appl_ipmc_name_index_t     filtering_profile;
} vtss_appl_ipmc_snp6_port_conf_t;

/**
 * \brief IPMC Snooping MLD interface configuration
 * The configuration is the interface configuration for running
 * the MLD Snooping functions.
 */
typedef struct {
    /**
     * \brief Interface administrative mode, TRUE is to enable MLD snooping
     * function for the interface and FALSE is to disable it.
     */
    mesa_bool_t                            admin_state;

    /**
     * \brief querier_election is used to denote whether the specific MLD
     * snooping interface joins the Querier election or not.
     */
    mesa_bool_t                            querier_election;

    /**
     * \brief compatibility is used to denote the way for version compatility
     * determination in MLD snooping.
     */
    vtss_appl_ipmc_compati6_t       compatibility;

    /**
     * \brief priority is used for prioritizing the MLD control frames to be
     * sent for MLD snooping.
     */
    uint8_t                              priority;

    /**
     * \brief robustness_variable presents the "Robustness Variable", as
     * stated in RFC-3810 9.1, for an MLD interface running MLD protocol
     * while snooping MLD control frames.
     */
    uint32_t                             robustness_variable;

    /**
     * \brief query_interval presents the "Query Interval", as stated in
     * RFC-3810 9.2, for an MLD interface running MLD protocol while
     * snooping MLD control frames.
     */
    uint32_t                             query_interval;

    /**
     * \brief query_response_interval presents the "Query Response Interval",
     * as stated in RFC-3810 9.3, for an MLD interface running MLD protocol
     * while snooping MLD control frames.
     */
    uint32_t                             query_response_interval;

    /**
     * \brief last_listener_query_interval presents the "Last Listener Query
     * Interval", as stated in RFC-3810 9.8, for an MLD interface running
     * MLD protocol while snooping MLD control frames.
     */
    uint32_t                             last_listener_query_interval;

    /**
     * \brief unsolicited_report_interval presents the "Unsolicited Report
     * Interval", as stated in RFC-3810 9.11, for an MLD interface running
     * MLD protocol while snooping MLD control frames.
     */
    uint32_t                             unsolicited_report_interval;
} vtss_appl_ipmc_snp_ipv6_interface_t;
#endif /* VTSS_SW_OPTION_SMB_IPMC */

/**
 * \brief IGMP Snooping mrouter status (per port)
 * The status is the per-port basis information that reflects
 * the current multicast router state in IGMP Snooping functions.
 */
typedef struct {

    /**
     * \brief status is used to denote the current multicast router
     * state on a specific port.
     */
    vtss_appl_ipmc_mrtr4_status_t   status;
} vtss_appl_ipmc_snp4_mrouter_t;

/**
 * \brief IPMC Snooping IGMP interface status
 * The IGMP interface parameters, timers, counters, and protocol status from
 * the running IGMP Snooping functions.
 */
typedef struct {
    /**
     * \brief Interface Querier state is used to denote the current IGMP
     * interface's Querier state based on Querier election defined in protocol.
     */
    vtss_appl_ipmc_qry4_states_t    querier_state;

    /**
     * \brief active_querier_address is used to denote the IPv4 address of the
     * active Querier for a running IGMP interface.
     */
    mesa_ipv4_t                     active_querier_address;

    /**
     * \brief querier_up_time presents the in operation timer for the specific
     * interface act as a IGMP Querier.
     */
    uint32_t                             querier_up_time;

    /**
     * \brief query_interval presents the "Query Interval", as stated in
     * RFC-3376 8.2, for an IGMP interface running IGMP protocol.
     */
    uint32_t                             query_interval;

    /**
     * \brief startup_query_count presents the "Startup Query Count", as stated
     * in RFC-3376 8.7, for an IGMP interface running IGMP protocol.
     */
    uint32_t                             startup_query_count;

    /**
     * \brief querier_expiry_time presents the "Other Querier Present Interval",
     * as stated in RFC-3376 8.5, for an IGMP interface running IGMP protocol.
     */
    uint32_t                             querier_expiry_time;

    /**
     * \brief querier_version presents the current IGMP version that the IGMP
     * interface should behave in running IGMP protocol as a router.
     */
    uint8_t                              querier_version;

    /**
     * \brief querier_present_timeout presents the "Older Version Querier Present
     * Timeout", as stated in RFC-3376 8.12, for an IGMP interface running IGMP
     * protocol.
     */
    uint32_t                             querier_present_timeout;

    /**
     * \brief host_version presents the current IGMP version that the IGMP
     * interface should behave in running IGMP protocol as a host.
     */
    uint8_t                              host_version;

    /**
     * \brief querier_present_timeout presents the "Older Host Present Interval",
     * as stated in RFC-3376 8.13, for an IGMP interface running IGMP protocol.
     */
    uint32_t                             host_present_timeout;

    /**
     * \brief counter_tx_query presents the current packet count on the specific
     * IGMP interface for transmitting IGMP Query control frames.
     */
    uint32_t                             counter_tx_query;

    /**
     * \brief counter_tx_specific_query presents the current packet count on the
     * specific IGMP interface for transmitting IGMP Specific Query control frames.
     */
    uint32_t                             counter_tx_specific_query;

    /**
     * \brief counter_rx_query presents the current packet count on the specific
     * IGMP interface for receiving IGMP Query control frames.
     */
    uint32_t                             counter_rx_query;

    /**
     * \brief counter_rx_v1_join presents the current packet count on the specific
     * IGMP interface for receiving IGMPv1 Join control frames.
     */
    uint32_t                             counter_rx_v1_join;

    /**
     * \brief counter_rx_v2_join presents the current packet count on the specific
     * IGMP interface for receiving IGMPv2 Join control frames.
     */
    uint32_t                             counter_rx_v2_join;

    /**
     * \brief counter_rx_v2_leave presents the current packet count on the specific
     * IGMP interface for receiving IGMPv2 Leave control frames.
     */
    uint32_t                             counter_rx_v2_leave;

    /**
     * \brief counter_rx_v3_join presents the current packet count on the specific
     * IGMP interface for receiving IGMPv3 Join control frames.
     */
    uint32_t                             counter_rx_v3_join;

    /**
     * \brief counter_rx_errors presents the current packet count on the specific
     * IGMP interface for receiving invalid IGMP control frames.
     */
    uint32_t                             counter_rx_errors;
} vtss_appl_ipmc_snp_ipv4_intf_status_t;

#ifdef VTSS_SW_OPTION_SMB_IPMC
/**
 * \brief MLD Snooping mrouter status (per port)
 * The status is the per-port basis information that reflects
 * the current multicast router state in MLD Snooping functions.
 */
typedef struct {

    /**
     * \brief status is used to denote the current multicast router
     * state on a specific port.
     */
    vtss_appl_ipmc_mrtr6_status_t   status;
} vtss_appl_ipmc_snp6_mrouter_t;

/**
 * \brief IPMC Snooping MLD interface status
 * The MLD interface parameters, timers, counters, and protocol status from
 * the running MLD Snooping functions.
 */
typedef struct {
    /**
     * \brief Interface Querier state is used to denote the current MLD
     * interface's Querier state based on Querier election defined in protocol.
     */
    vtss_appl_ipmc_qry6_states_t    querier_state;

    /**
     * \brief active_querier_address is used to denote the IPv6 address of the
     * active Querier for a running MLD interface.
     */
    mesa_ipv6_t                     active_querier_address;

    /**
     * \brief querier_up_time presents the in operation timer for the specific
     * interface act as a MLD Querier.
     */
    uint32_t                             querier_up_time;

    /**
     * \brief query_interval presents the "Query Interval", as stated in
     * RFC-3810 9.2, for a MLD interface running MLD protocol.
     */
    uint32_t                             query_interval;

    /**
     * \brief startup_query_count presents the "Startup Query Count", as stated
     * in RFC-3810 9.7, for a MLD interface running MLD protocol.
     */
    uint32_t                             startup_query_count;

    /**
     * \brief querier_expiry_time presents the "Other Querier Present Interval",
     * as stated in RFC-3810 9.5, for a MLD interface running MLD protocol.
     */
    uint32_t                             querier_expiry_time;

    /**
     * \brief querier_version presents the current MLD version that the MLD
     * interface should behave in running MLD protocol as a router.
     */
    uint8_t                              querier_version;

    /**
     * \brief querier_present_timeout presents the "Older Version Querier Present
     * Timeout", as stated in RFC-3810 9.12, for a MLD interface running MLD
     * protocol.
     */
    uint32_t                             querier_present_timeout;

    /**
     * \brief host_version presents the current MLD version that the MLD
     * interface should behave in running MLD protocol as a host.
     */
    uint8_t                              host_version;

    /**
     * \brief querier_present_timeout presents the "Older Host Present Interval",
     * as stated in RFC-3810 9.13, for a MLD interface running MLD protocol.
     */
    uint32_t                             host_present_timeout;

    /**
     * \brief counter_tx_query presents the current packet count on the specific
     * MLD interface for transmitting MLD Query control frames.
     */
    uint32_t                             counter_tx_query;

    /**
     * \brief counter_tx_specific_query presents the current packet count on the
     * specific MLD interface for transmitting MLD Specific Query control frames.
     */
    uint32_t                             counter_tx_specific_query;

    /**
     * \brief counter_rx_query presents the current packet count on the specific
     * MLD interface for receiving MLD Query control frames.
     */
    uint32_t                             counter_rx_query;

    /**
     * \brief counter_rx_v1_report presents the current packet count on the specific
     * MLD interface for receiving MLDv1 Report control frames.
     */
    uint32_t                             counter_rx_v1_report;

    /**
     * \brief counter_rx_v1_done presents the current packet count on the specific
     * MLD interface for receiving MLDv1 Done control frames.
     */
    uint32_t                             counter_rx_v1_done;

    /**
     * \brief counter_rx_v2_report presents the current packet count on the specific
     * MLD interface for receiving MLDv2 Report control frames.
     */
    uint32_t                             counter_rx_v2_report;

    /**
     * \brief counter_rx_errors presents the current packet count on the specific
     * MLD interface for receiving invalid MLD control frames.
     */
    uint32_t                             counter_rx_errors;
} vtss_appl_ipmc_snp_ipv6_intf_status_t;
#endif /* VTSS_SW_OPTION_SMB_IPMC */

/**
 * \brief IPMC Snooping group address registration count
 * The multicast group address registration count from IPMC Snooping.
 */
typedef struct {
    /**
     * \brief igmp_snp_grps is used to denote the total number of registered
     * multicast group address from IGMP snooping.
     */
    uint32_t                             igmp_snp_grps;

#ifdef VTSS_SW_OPTION_SMB_IPMC
    /**
     * \brief mld_snp_grps is used to denote the total number of registered
     * multicast group address from MLD snooping.
     */
    uint32_t                             mld_snp_grps;
#endif /* VTSS_SW_OPTION_SMB_IPMC */
} vtss_appl_ipmc_snp_grp_adrs_cnt_t;

/**
 * \brief IGMP Snooping group address registration status
 * The multicast group address registration status from IGMP Snooping.
 */
typedef struct {
    /**
     * \brief member_ports is used to denote the memberships of the registered
     * multicast group address from IGMP snooping.
     */
    vtss_port_list_stackable_t      member_ports;

    /**
     * \brief hardware_switch is used to denote whether the multicast traffic
     * destined to the registered group address could be forwarding by switch
     * hardware or not.
     */
    mesa_bool_t                            hardware_switch;
} vtss_appl_ipmc_snp_grp4_address_t;

#ifdef VTSS_SW_OPTION_SMB_IPMC
/**
 * \brief MLD Snooping group address registration status
 * The multicast group address registration status from MLD Snooping.
 */
typedef struct {
    /**
     * \brief member_ports is used to denote the memberships of the registered
     * multicast group address from MLD snooping.
     */
    vtss_port_list_stackable_t      member_ports;

    /**
     * \brief hardware_switch is used to denote whether the multicast traffic
     * destined to the registered group address could be forwarding by switch
     * hardware or not.
     */
    mesa_bool_t                            hardware_switch;
} vtss_appl_ipmc_snp_grp6_address_t;

/**
 * \brief IGMP Snooping source list status in group address registration
 * The multicast group address with source list registration status from
 * IGMP Snooping.
 */
typedef struct {
    /**
     * \brief group_filter_mode is used to denote the source filtering mode
     * of the specific registered multicast group address from IGMP snooping.
     */
    vtss_appl_ipmc_sfm4_mode_t      group_filter_mode;

    /**
     * \brief filter_timer is used to count down the timer for the specific multicast
     * group's filtering mode transition.
     */
    uint32_t                             filter_timer;

    /**
     * \brief source_type is used to denote the filtering type of the specific source
     * address in multicasting to the registered group address.
     */
    vtss_appl_ipmc_action4_t        source_type;

    /**
     * \brief source_timer is used to count down the timer for purging the specific
     * source address from the registered multicast group's source list.
     */
    uint32_t                             source_timer;

    /**
     * \brief hardware_switch is used to denote whether the traffic destined to
     * the multicast group address from the specific source address could be
     * forwarding by switch hardware or not.
     */
    mesa_bool_t                            hardware_switch;
} vtss_appl_ipmc_snp_grp4_srclist_t;

/**
 * \brief MLD Snooping source list status in group address registration
 * The multicast group address with source list registration status from
 * MLD Snooping.
 */
typedef struct {
    /**
     * \brief group_filter_mode is used to denote the source filtering mode
     * of the specific registered multicast group address from MLD snooping.
     */
    vtss_appl_ipmc_sfm6_mode_t      group_filter_mode;

    /**
     * \brief filter_timer is used to count down the timer for the specific multicast
     * group's filtering mode transition.
     */
    uint32_t                             filter_timer;

    /**
     * \brief source_type is used to denote the filtering type of the specific source
     * address in multicasting to the registered group address.
     */
    vtss_appl_ipmc_action6_t        source_type;

    /**
     * \brief source_timer is used to count down the timer for purging the specific
     * source address from the registered multicast group's source list.
     */
    uint32_t                             source_timer;

    /**
     * \brief hardware_switch is used to denote whether the traffic destined to
     * the multicast group address from the specific source address could be
     * forwarding by switch hardware or not.
     */
    mesa_bool_t                            hardware_switch;
} vtss_appl_ipmc_snp_grp6_srclist_t;
#endif /* VTSS_SW_OPTION_SMB_IPMC */

/**
 * \brief Get IGMP Snooping Global Parameters
 *
 * To read current system parameters in IGMP snooping.
 *
 * \param conf      [OUT]    The IGMP snooping system configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_system_config_get(
    vtss_appl_ipmc_snp_ipv4_global_t            *const conf
);

/**
 * \brief Set IGMP Snooping Global Parameters
 *
 * To modify current system parameters in IGMP snooping.
 *
 * \param conf      [IN]     The IGMP snooping system configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_system_config_set(
    const vtss_appl_ipmc_snp_ipv4_global_t      *const conf
);

/**
 * \brief Get IGMP Snooping global default parameters
 *
 * To get default system parameters in IGMP snooping.
 *
 * \param conf      [OUT]    The IGMP snooping system default configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc vtss_appl_ipmc_snp_igmp_system_config_default(
    vtss_appl_ipmc_snp_ipv4_global_t            *const conf
);

/**
 * \brief Iterator for retrieving IPMC Snooping port information key/index
 *
 * To walk information (configuration and status) index of the port in IPMC snooping.
 *
 * \param prev      [IN]    Interface index to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_general_port_itr(
    const vtss_ifindex_t                        *const prev,
    vtss_ifindex_t                              *const next
);

/**
 * \brief Get IGMP Snooping specific port configuration
 *
 * To get configuration of the specific port in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 *
 * \param entry     [OUT]   The current configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_port_config_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_snp4_port_conf_t             *const entry
);

/**
 * \brief Set IGMP Snooping specific port configuration
 *
 * To modify configuration of the specific port in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 * \param entry     [IN]    The revised configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_port_config_set(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_appl_ipmc_snp4_port_conf_t       *const entry
);

/**
 * \brief Get IGMP Snooping specific port default configuration
 *
 * To get default configuration of the specific port in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 *
 * \param entry     [OUT]   The default configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc vtss_appl_ipmc_snp_igmp_port_config_default(
    vtss_ifindex_t                              *const ifindex,
    vtss_appl_ipmc_snp4_port_conf_t             *const entry
);

/**
 * \brief Get IGMP Snooping specific port's mrouter status
 *
 * To get mrouter status of the specific port in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 *
 * \param entry     [OUT]   The current mrouter status of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_port_mrouter_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_snp4_mrouter_t               *const entry
);

/**
 * \brief Iterator for retrieving IGMP Snooping VLAN information key/index
 *
 * To walk information (configuration and status) index of the VLAN in IGMP snooping.
 *
 * \param prev      [IN]    Interface index to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_vlan_general_itr(
    const vtss_ifindex_t                        *const prev,
    vtss_ifindex_t                              *const next
);

/**
 * \brief Get IGMP Snooping specific VLAN configuration
 *
 * To get configuration of the specific VLAN in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The current configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_vlan_config_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_snp_ipv4_interface_t         *const entry
);

/**
 * \brief Set IGMP Snooping specific VLAN configuration
 *
 * To modify configuration of the specific VLAN in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param entry     [IN]    The revised configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_vlan_config_set(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_appl_ipmc_snp_ipv4_interface_t   *const entry
);

/**
 * \brief Get IGMP Snooping specific VLAN default configuration
 *
 * To get default configuration of the specific VLAN in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The default configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc vtss_appl_ipmc_snp_igmp_vlan_config_default(
    vtss_ifindex_t                              *const ifindex,
    vtss_appl_ipmc_snp_ipv4_interface_t         *const entry
);

/**
 * \brief Get IGMP Snooping specific VLAN interface's status
 *
 * To get interface's status of the specific VLAN in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The current configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_vlan_status_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_snp_ipv4_intf_status_t       *const entry
);

/**
 * \brief Get current IPMC Snooping multicast group registration count
 *
 * To read the entry count of registered multicast group address from IPMC snooping.
 *
 * \param grp_cnt   [OUT]   The current registered multicast group address count.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_grp_adr_cnt_get(
    vtss_appl_ipmc_snp_grp_adrs_cnt_t           *const grp_cnt
);

/**
 * \brief Iterator for retrieving IGMP Snooping group address table key/index
 *
 * To walk indexes of the group address table in IGMP snooping.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param grps_prev [IN]    IPv4 Address of multicast group to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param grps_next [OUT]   The key/index of multicast group address should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_group_address_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const mesa_ipv4_t                           *const grps_prev,
    mesa_ipv4_t                                 *const grps_next
);

/**
 * \brief Get IGMP Snooping specific registered multicast address on a VLAN
 *
 * To get registered multicast group address data of the specific VLAN in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param grpadrs   [IN]    (key) Multicast group address.
 *
 * \param entry     [OUT]   The current status of the registered multicast group address.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_group_address_get(
    const vtss_ifindex_t                        *const ifindex,
    const mesa_ipv4_t                           *const grpadrs,
    vtss_appl_ipmc_snp_grp4_address_t           *const entry
);

#ifdef VTSS_SW_OPTION_SMB_IPMC
/**
 * \brief Iterator for retrieving IGMP Snooping group source list table key/index
 *
 * To walk indexes of the group source list table in IGMP snooping.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param grps_prev [IN]    IPv4 Address of multicast group to be used for indexing determination.
 * \param port_prev [IN]    Ifindex of port to be used for indexing determination.
 * \param adrs_prev [IN]    IPv4 Address of multicasting source to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param grps_next [OUT]   The key/index of multicast group address should be used for the GET operation.
 * \param port_next [OUT]   The key/index of port's Ifindex should be used for the GET operation.
 * \param adrs_next [OUT]   The key/index of multicasting source address should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_group_srclist_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const mesa_ipv4_t                           *const grps_prev,
    mesa_ipv4_t                                 *const grps_next,
    const vtss_ifindex_t                        *const port_prev,
    vtss_ifindex_t                              *const port_next,
    const mesa_ipv4_t                           *const adrs_prev,
    mesa_ipv4_t                                 *const adrs_next
);

/**
 * \brief Get IGMP Snooping specific registered multicast address and its source list entry
 *
 * To get registered multicast group source list data of the specific VLAN and port in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param grpadrs   [IN]    (key) Multicast group address.
 * \param stkport   [IN]    (key) Interface index - the logical interface index of the port.
 * \param srcadrs   [IN]    (key) Multicasting source address.
 *
 * \param entry     [OUT]   The current status of the registered multicast group source list entry.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_group_srclist_get(
    const vtss_ifindex_t                        *const ifindex,
    const mesa_ipv4_t                           *const grpadrs,
    const vtss_ifindex_t                        *const stkport,
    const mesa_ipv4_t                           *const srcadrs,
    vtss_appl_ipmc_snp_grp4_srclist_t           *const entry
);
#endif /* VTSS_SW_OPTION_SMB_IPMC */

/**
 * \brief IGMP Snooping Control ACTION for clearing statistics
 *
 * To clear the statistics based on the given VLAN ifIndex of IGMP interface.
 *
 * \param ifindex   [IN]    Interface index - the logical interface index of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_statistics_clear_by_vlan_act(
    const vtss_ifindex_t                        *const ifindex
);

#ifdef VTSS_SW_OPTION_SMB_IPMC
/**
 * \brief Get MLD Snooping Global Parameters
 *
 * To read current system parameters in MLD snooping.
 *
 * \param conf      [OUT]    The MLD snooping system configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_system_config_get(
    vtss_appl_ipmc_snp_ipv6_global_t            *const conf
);

/**
 * \brief Set MLD Snooping Global Parameters
 *
 * To modify current system parameters in MLD snooping.
 *
 * \param conf      [IN]     The MLD snooping system configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_system_config_set(
    const vtss_appl_ipmc_snp_ipv6_global_t      *const conf
);

/**
 * \brief Get MLD Snooping global default parameters
 *
 * To get default system parameters in MLD snooping.
 *
 * \param conf      [OUT]    The MLD snooping system default configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc vtss_appl_ipmc_snp_mld_system_config_default(
    vtss_appl_ipmc_snp_ipv6_global_t            *const conf
);

/**
 * \brief Get MLD Snooping specific port configuration
 *
 * To get configuration of the specific port in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 *
 * \param entry     [OUT]   The current configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_port_config_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_snp6_port_conf_t             *const entry
);

/**
 * \brief Set MLD Snooping specific port configuration
 *
 * To modify configuration of the specific port in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 * \param entry     [IN]    The revised configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_port_config_set(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_appl_ipmc_snp6_port_conf_t       *const entry
);

/**
 * \brief Get MLD Snooping specific port default configuration
 *
 * To get default configuration of the specific port in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 *
 * \param entry     [OUT]   The default configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc vtss_appl_ipmc_snp_mld_port_config_default(
    vtss_ifindex_t                              *const ifindex,
    vtss_appl_ipmc_snp6_port_conf_t             *const entry
);

/**
 * \brief Get MLD Snooping specific port's mrouter status
 *
 * To get mrouter status of the specific port in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 *
 * \param entry     [OUT]   The current mrouter status of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_port_mrouter_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_snp6_mrouter_t               *const entry
);

/**
 * \brief Iterator for retrieving MLD Snooping VLAN information key/index
 *
 * To walk information (configuration and status) index of the VLAN in MLD snooping.
 *
 * \param prev      [IN]    Interface index to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_vlan_general_itr(
    const vtss_ifindex_t                        *const prev,
    vtss_ifindex_t                              *const next
);

/**
 * \brief Get MLD Snooping specific VLAN configuration
 *
 * To get configuration of the specific VLAN in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The current configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_vlan_config_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_snp_ipv6_interface_t         *const entry
);

/**
 * \brief Set MLD Snooping specific VLAN configuration
 *
 * To modify configuration of the specific VLAN in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param entry     [IN]    The revised configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_vlan_config_set(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_appl_ipmc_snp_ipv6_interface_t   *const entry
);

/**
 * \brief Get MLD Snooping specific VLAN default configuration
 *
 * To get default configuration of the specific VLAN in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The default configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc vtss_appl_ipmc_snp_mld_vlan_config_default(
    vtss_ifindex_t                              *const ifindex,
    vtss_appl_ipmc_snp_ipv6_interface_t         *const entry
);

/**
 * \brief Get MLD Snooping specific VLAN interface's status
 *
 * To get interface's status of the specific VLAN in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The current configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_vlan_status_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_snp_ipv6_intf_status_t       *const entry
);

/**
 * \brief Iterator for retrieving MLD Snooping group address table key/index
 *
 * To walk indexes of the group address table in MLD snooping.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param grps_prev [IN]    IPv6 Address of multicast group to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param grps_next [OUT]   The key/index of multicast group address should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_group_address_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const mesa_ipv6_t                           *const grps_prev,
    mesa_ipv6_t                                 *const grps_next
);

/**
 * \brief Get MLD Snooping specific registered multicast address on a VLAN
 *
 * To get registered multicast group address data of the specific VLAN in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param grpadrs   [IN]    (key) Multicast group address.
 *
 * \param entry     [OUT]   The current status of the registered multicast group address.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_group_address_get(
    const vtss_ifindex_t                        *const ifindex,
    const mesa_ipv6_t                           *const grpadrs,
    vtss_appl_ipmc_snp_grp6_address_t           *const entry
);

/**
 * \brief Iterator for retrieving MLD Snooping group source list table key/index
 *
 * To walk indexes of the group source list table in MLD snooping.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param grps_prev [IN]    IPv6 Address of multicast group to be used for indexing determination.
 * \param port_prev [IN]    Ifindex of port to be used for indexing determination.
 * \param adrs_prev [IN]    IPv6 Address of multicasting source to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param grps_next [OUT]   The key/index of multicast group address should be used for the GET operation.
 * \param port_next [OUT]   The key/index of port's Ifindex should be used for the GET operation.
 * \param adrs_next [OUT]   The key/index of multicasting source address should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_group_srclist_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const mesa_ipv6_t                           *const grps_prev,
    mesa_ipv6_t                                 *const grps_next,
    const vtss_ifindex_t                        *const port_prev,
    vtss_ifindex_t                              *const port_next,
    const mesa_ipv6_t                           *const adrs_prev,
    mesa_ipv6_t                                 *const adrs_next
);

/**
 * \brief Get MLD Snooping specific registered multicast address and its source list entry
 *
 * To get registered multicast group source list data of the specific VLAN and port in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param grpadrs   [IN]    (key) Multicast group address.
 * \param stkport   [IN]    (key) Interface index - the logical interface index of the port.
 * \param srcadrs   [IN]    (key) Multicasting source address.
 *
 * \param entry     [OUT]   The current status of the registered multicast group source list entry.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_group_srclist_get(
    const vtss_ifindex_t                        *const ifindex,
    const mesa_ipv6_t                           *const grpadrs,
    const vtss_ifindex_t                        *const stkport,
    const mesa_ipv6_t                           *const srcadrs,
    vtss_appl_ipmc_snp_grp6_srclist_t           *const entry
);

/**
 * \brief MLD Snooping Control ACTION for clearing statistics
 *
 * To clear the statistics based on the given VLAN ifIndex of MLD interface.
 *
 * \param ifindex   [IN]    Interface index - the logical interface index of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_statistics_clear_by_vlan_act(
    const vtss_ifindex_t                        *const ifindex
);
#endif /* VTSS_SW_OPTION_SMB_IPMC */

#ifdef __cplusplus
}
#endif

#endif  /* _VTSS_APPL_IPMC_SNOOPING_H_ */
