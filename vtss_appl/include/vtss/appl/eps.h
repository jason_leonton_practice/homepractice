/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 

*/

/**
* \file eps.h
* \brief Public EPS (ELPS) API
* \details This header file describes the EPS (Ethernet Linear Protection Switching) control functions and types.
*/

#ifndef _VTSS_APPL_EPS_H_
#define _VTSS_APPL_EPS_H_

#include <vtss/appl/interface.h>
#include <mscc/ethernet/switch/api/types.h>
#include <vtss/appl/module_id.h>
#include <vtss/appl/mep.h>

#ifdef __cplusplus
extern "C" {
#endif

#define VTSS_APPL_EPS_CREATED_MAX                              100     /**< Max. number of created EPS instances                                   */
#define VTSS_APPL_EPS_WTR_MAX                                  720     /**< Max. WTR timer value in seconds - 12 * 60 sec.                         */
#define VTSS_APPL_EPS_HOFF_OFF                                   0     /**< Hold Off timer OFF value - '0' means no hold off timing                */
#define VTSS_APPL_EPS_HOFF_MAX                                 100     /**< Max. Hold off timer value in 100ms - 100 * 100 ms.                     */
#define VTSS_APPL_EPS_MEP_MAX         VTSS_APPL_MEP_INSTANCE_MAX-1     /**< Max. MEP relation value                                                */
#define VTSS_APPL_EPS_MEP_INST_INVALID  VTSS_APPL_MEP_INSTANCE_MAX     /**< Invalid MEP indication. This is returned when EPS has no MEP relations */

/**
 * \brief Instance capability structure.
 *
 * This structure is used to contain the instance capability.
 *
 */
typedef struct {
    uint32_t created_max;      /**< Maximum number of created EPS instances                                 */
    uint32_t wtr_max;          /**< Maximum WTR timer value                                                 */
    uint32_t hoff_off;         /**< Hold Off timer OFF value                                                */
    uint32_t hoff_max;         /**< Maximum Hold off timer value                                            */
    uint32_t mep_max;          /**< Maximum MEP relation value                                              */
    uint32_t mep_invalid;      /**< Invalid MEP indication. This is returned when EPS has no MEP relations  */
} vtss_appl_eps_capabilities_t;          

/**
 * Definition of error return codes.
 * See also eps_error_txt() in platform/eps.c.
 */
enum {
    VTSS_APPL_EPS_RC_NOT_CREATED =
    MODULE_ERROR_START(VTSS_MODULE_ID_EPS),          /**< EPS instance is not created                                        */
    VTSS_APPL_EPS_RC_CREATED,                        /**< EPS instance is already created                                    */
    VTSS_APPL_EPS_RC_INVALID_PARAMETER,              /**< Invalid parameter                                                  */
    VTSS_APPL_EPS_RC_NOT_CONFIGURED,                 /**< EPS instance is not yet configured - still default configuration   */
    VTSS_APPL_EPS_RC_ARCHITECTURE,                   /**< Invalid architecture for this domain                               */
    VTSS_APPL_EPS_RC_W_P_FLOW_EQUAL,                 /**< The working and protection flows are equal                         */
    VTSS_APPL_EPS_RC_WORKING_USED,                   /**< The working flow is used by other EPS instance                     */
    VTSS_APPL_EPS_RC_PROTECTING_USED,                /**< The protecting flow is used by other EPS instance                  */
    VTSS_APPL_EPS_RC_W_P_SSF_MEP_EQUAL,              /**< Working MEP and protecting SF MEP is same instance                 */
    VTSS_APPL_EPS_RC_INVALID_APS_MEP,                /**< Invalid APS MEP instance                                           */
    VTSS_APPL_EPS_RC_INVALID_W_MEP,                  /**< Invalid working SF MEP instance                                    */
    VTSS_APPL_EPS_RC_INVALID_P_MEP,                  /**< Invalid protection SF MEP instance                                 */
    VTSS_APPL_EPS_RC_INVALID_COMMAND,                /**< Invalid command for this configuration                             */
    VTSS_APPL_EPS_RC_INVALID_WTR,                    /**< Invalid WTR timer                                                  */
    VTSS_APPL_EPS_RC_INVALID_HOLD_OFF                /**< Invalid hold off timer                                             */
};

/**
 *  The protection group domain
*/
typedef enum
{
    VTSS_APPL_EPS_PORT,          /**< Domain is Port           */
    VTSS_APPL_EPS_EVC,           /**< Domain is EVC            */
    VTSS_APPL_EPS_MPLS_TUNNEL,   /**< Domain is MPLS-TP tunnel */
    VTSS_APPL_EPS_MPLS_PW,       /**< Domain is MPLS-TP PW     */
} vtss_appl_eps_domain_t;


/**
 *  The protection group architecture
*/
typedef enum
{
    VTSS_APPL_EPS_ARCHITECTURE_1P1,  /**< Architecture is 1+1 */
    VTSS_APPL_EPS_ARCHITECTURE_1F1   /**< Architecture is 1:1 */
} vtss_appl_eps_architecture_t;

/**
 * \brief Instance create structure.
 *
 * This structure is used to create (enable) an EPS instance. This structure cannot be changed after create.
 *
 */
typedef struct
{
    vtss_appl_eps_domain_t          domain;             /**< Domain.                                     */
    vtss_appl_eps_architecture_t    architecture;       /**< Architecture.                               */
    vtss_ifindex_t                  w_flow;             /**< Working flow instance number.               */
    vtss_ifindex_t                  p_flow;             /**< Working flow instance number.               */
} vtss_appl_eps_create_param_t;

/**
 *  The 1+1 protection group switching model
*/
typedef enum
{
    VTSS_APPL_EPS_UNIDIRECTIONAL,    /**< Unidirectional 1+1 switching */
    VTSS_APPL_EPS_BIDIRECTIONAL      /**< Bidirectional 1+1 switching */
} vtss_appl_eps_directional_t;

/**
 * \brief Instance configuration structure.
 *
 * This structure is used to configure an created EPS instance.
 * After create this structure is set to default and can be change subsequently.
 *
 */
typedef struct
{
    vtss_appl_eps_directional_t directional;        /**< Directional. Only for 1+1                                                */
    mesa_bool_t                        aps;                /**< APS enabled if true. Only for 1+1                                        */
    mesa_bool_t                        revertive;          /**< Revertive operation enabled if true.                                     */
    uint32_t                         restore_timer;      /**< Wait to restore timer in seconds - max. VTSS_APPL_EPS_WTR_MAX - min. 1   */
    uint32_t                         hold_off_timer;     /**< Hold off timer in 100 ms - max. VTSS_APPL_EPS_HOFF_MAX                   */
} vtss_appl_eps_conf_t;

/**
 * \brief Instance MEP configuration structure.
 *
 * This structure is used to set or get the MEP instances related to an created EPS instance.
 * Calling vtss_appl_eps_mep_get() the following MEP instance numbers will contain VTSS_APPL_EPS_MEP_INST_INVALID
 * if no MEP relation has has been configured yet.
 *
 */
typedef struct
{
    uint32_t  w_mep;      /**< Working MEP instance number - max. VTSS_APPL_EPS_MEP_MAX - min. 0     */
    uint32_t  p_mep;      /**< Protecting MEP instance number - max. VTSS_APPL_EPS_MEP_MAX - min. 0  */
    uint32_t  aps_mep;    /**< APS MEP instance number - max. VTSS_APPL_EPS_MEP_MAX - min. 0         */
} vtss_appl_eps_mep_t;

/**
 *  The possible protection group commands.
*/
typedef enum
{
    VTSS_APPL_EPS_COMMAND_NONE,              /**< Any active command is cleared                                     */
    VTSS_APPL_EPS_COMMAND_CLEAR,             /**< Any active command is cleared - changed to NONE after CLEAR       */
    VTSS_APPL_EPS_COMMAND_LOCK_OUT,          /**< end-to-end lock out of protection                                 */
    VTSS_APPL_EPS_COMMAND_FORCED_SWITCH,     /**< Forced switch to protection                                       */
    VTSS_APPL_EPS_COMMAND_MANUAL_SWITCH_P,   /**< Manual switch to protection                                       */
    VTSS_APPL_EPS_COMMAND_MANUAL_SWITCH_W,   /**< Manual switch to working                                          */
    VTSS_APPL_EPS_COMMAND_EXERCISE,          /**< Exercise of APS protocol                                          */
    VTSS_APPL_EPS_COMMAND_FREEZE,            /**< Local Freeze of protection group                                  */
    VTSS_APPL_EPS_COMMAND_LOCK_OUT_LOCAL     /**< Local lock out of protection                                      */
} vtss_appl_eps_command_t;

/**
 * \brief Instance command structure.
 *
 * This structure is used to set or get the protection group command.
 *
 */
typedef struct
{
    vtss_appl_eps_command_t  command;      /**< protection group command.  */
} vtss_appl_eps_command_conf_t;

/**
 *  The possible protection group states as described in G.8031 Annex A.
*/
typedef enum
{
    VTSS_APPL_EPS_PROT_STATE_DISABLED,          /**< This is an invalid state - no state has been calculated yet */
    VTSS_APPL_EPS_PROT_STATE_NO_REQUEST_W,      /**< This state is according to G.8031 Annex A */
    VTSS_APPL_EPS_PROT_STATE_NO_REQUEST_P,      /**< This state is according to G.8031 Annex A */
    VTSS_APPL_EPS_PROT_STATE_LOCKOUT,           /**< This state is according to G.8031 Annex A */
    VTSS_APPL_EPS_PROT_STATE_FORCED_SWITCH,     /**< This state is according to G.8031 Annex A */
    VTSS_APPL_EPS_PROT_STATE_SIGNAL_FAIL_W,     /**< This state is according to G.8031 Annex A */
    VTSS_APPL_EPS_PROT_STATE_SIGNAL_FAIL_P,     /**< This state is according to G.8031 Annex A */
    VTSS_APPL_EPS_PROT_STATE_MANUAL_SWITCH_W,   /**< This state is according to G.8031 Annex A */
    VTSS_APPL_EPS_PROT_STATE_MANUAL_SWITCH_P,   /**< This state is according to G.8031 Annex A */
    VTSS_APPL_EPS_PROT_STATE_WAIT_TO_RESTORE,   /**< This state is according to G.8031 Annex A */
    VTSS_APPL_EPS_PROT_STATE_EXERCISE_W,        /**< This state is according to G.8031 Annex A */
    VTSS_APPL_EPS_PROT_STATE_EXERCISE_P,        /**< This state is according to G.8031 Annex A */
    VTSS_APPL_EPS_PROT_STATE_REVERSE_REQUEST_W, /**< This state is according to G.8031 Annex A */
    VTSS_APPL_EPS_PROT_STATE_REVERSE_REQUEST_P, /**< This state is according to G.8031 Annex A */
    VTSS_APPL_EPS_PROT_STATE_DO_NOT_REVERT      /**< This state is according to G.8031 Annex A */
} vtss_appl_eps_prot_state_t;

/**
 *  The possible working or protecting flow defect state.
*/
typedef enum
{
    VTSS_APPL_EPS_DEFECT_STATE_OK,      /**< The Working/protecting flow defect state is OK - not active        */
    VTSS_APPL_EPS_DEFECT_STATE_SD,      /**< The Working/protecting flow defect state is Signal Degrade active  */
    VTSS_APPL_EPS_DEFECT_STATE_SF       /**< The Working/protecting flow defect state is Signal Fail active     */
} vtss_appl_eps_defect_state_t;

/**
 *  The possible transmitted or received APS request according to G.8031 Table 11-1.
*/
typedef enum
{
    VTSS_APPL_EPS_REQUEST_NR,       /**< The Transmitted/Received request is according to G.8031 Table 11-1 */
    VTSS_APPL_EPS_REQUEST_DNR,      /**< The Transmitted/Received request is according to G.8031 Table 11-1 */
    VTSS_APPL_EPS_REQUEST_RR,       /**< The Transmitted/Received request is according to G.8031 Table 11-1 */
    VTSS_APPL_EPS_REQUEST_EXER,     /**< The Transmitted/Received request is according to G.8031 Table 11-1 */
    VTSS_APPL_EPS_REQUEST_WTR,      /**< The Transmitted/Received request is according to G.8031 Table 11-1 */
    VTSS_APPL_EPS_REQUEST_MS_W,     /**< The Transmitted/Received request is according to G.8031 Table 11-1 */
    VTSS_APPL_EPS_REQUEST_MS_P,     /**< The Transmitted/Received request is according to G.8031 Table 11-1 */
    VTSS_APPL_EPS_REQUEST_SD,       /**< The Transmitted/Received request is according to G.8031 Table 11-1 */
    VTSS_APPL_EPS_REQUEST_SF_W,     /**< The Transmitted/Received request is according to G.8031 Table 11-1 */
    VTSS_APPL_EPS_REQUEST_FS,       /**< The Transmitted/Received request is according to G.8031 Table 11-1 */
    VTSS_APPL_EPS_REQUEST_SF_P,     /**< The Transmitted/Received request is according to G.8031 Table 11-1 */
    VTSS_APPL_EPS_REQUEST_LO        /**< The Transmitted/Received request is according to G.8031 Table 11-1 */
} vtss_appl_eps_request_t;

/**
 * \brief APS information structure.
 *
 * This structure is used to contain transmitted or received APS information.
 *
 */
typedef struct
{
    vtss_appl_eps_request_t     request;    /**< Transmitted/Received request type according to G.8031 table 11-1      */
    uint32_t                         re_signal;  /**< Transmitted/Received requested signal according to G.8031 figure 11-2 */
    uint32_t                         br_signal;  /**< Transmitted/Received bridged signal according to G.8031 figure 11-2   */
}vtss_appl_eps_aps_info_t;

/**
 * \brief Instance status data structure.
 *
 * This structure is used to retrieve instance status information.
 */
typedef struct
{
    vtss_appl_eps_prot_state_t       protection_state;   /**< Protection switch state              */
    vtss_appl_eps_defect_state_t     w_state;            /**< Working flow state                   */
    vtss_appl_eps_defect_state_t     p_state;            /**< Protecting flow state                */
    vtss_appl_eps_aps_info_t         tx_aps;             /**< Transmitting APS request             */
    vtss_appl_eps_aps_info_t         rx_aps;             /**< Receiving APS request                */
    mesa_bool_t                             dFop_pm;            /**< APS B bit mismatch state             */
    mesa_bool_t                             dFop_cm;            /**< APS configuration mismatch state     */
    mesa_bool_t                             dFop_nr;            /**< APS protection incomplete state      */
    mesa_bool_t                             dFop_NoAps;         /**< APS not received                     */
} vtss_appl_eps_state_t;

/**
 * \brief Instance default structure.
 *
 * This structure is used to contain configuration default values.
 *
 */
typedef struct
{
    vtss_appl_eps_create_param_t   param;   /**< Instance create default parameters */
    vtss_appl_eps_conf_t           config;  /**< Instance configuration default     */
    vtss_appl_eps_command_t        command; /**< Command default                    */
} vtss_appl_eps_default_conf_t;

/**
 * \brief Default GET. (vtss_appl_eps_default_conf_t)
 *
 * This returns the instance default configuration values
 *
 * \param conf [OUT]      configuration.
 *
 * \return  VTSS_APPL_EPS_RC_OK
 */
void vtss_appl_eps_default_conf_get(vtss_appl_eps_default_conf_t  *const conf);

/**
 * \brief Capabilities GET. (vtss_appl_eps_capabilities_t)
 *
 * This returns the instance capabilities
 *
 * \param conf [OUT]      configuration.
 *
 * \return  VTSS_APPL_EPS_RC_OK
 */
mesa_rc vtss_appl_eps_capabilities_get(vtss_appl_eps_capabilities_t *const conf);

/**
 * \brief EPS instance ADD. (vtss_appl_eps_create_param_t)
 *
 * This add (enable) a new EPS instance with the given instance number and parameters. The instance must
 * not already be enabled. After instance add, this parameters cannot be changed.
 * After instance add, the vtss_appl_eps_conf_t is set to default values that can be changed using vtss_appl_eps_conf_set().
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      Create parameters.
 *
 * \return  VTSS_APPL_EPS_RC_CREATED
 *          VTSS_APPL_EPS_RC_INVALID_PARAMETER
 *          VTSS_APPL_EPS_RC_ARCHITECTURE
 *          VTSS_APPL_EPS_RC_W_P_EQUAL
 *          VTSS_APPL_EPS_RC_WORKING_USED
 *          VTSS_APPL_EPS_RC_PROTECTING_USED
 *          VTSS_APPL_EPS_RC_OK
 */
mesa_rc vtss_appl_eps_create_conf_add(const uint32_t                          instance,
                                      const vtss_appl_eps_create_param_t *const conf);

/**
 * \brief EPS instance DELETE.
 *
 * This delete an enabled instance with the given instance number.
 *
 * \param instance [IN]  Instance number.
 *
 * \return  VTSS_APPL_EPS_RC_NOT_CREATED
 *          VTSS_APPL_EPS_RC_INVALID_PARAMETER
 *          VTSS_APPL_EPS_RC_OK
 */
mesa_rc vtss_appl_eps_create_conf_delete(const uint32_t     instance);

/**
 * \brief EPS Instance parameters GET. (vtss_appl_eps_create_param_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    Create parameters.
 *
 * \return  VTSS_APPL_EPS_RC_INVALID_PARAMETER
 *          VTSS_APPL_EPS_RC_NOT_CREATED
 *          VTSS_APPL_EPS_RC_NOT_CONFIGURED
 *          VTSS_APPL_EPS_RC_OK
 */
mesa_rc vtss_appl_eps_create_conf_get(uint32_t                           instance,
                                      vtss_appl_eps_create_param_t  *const conf);

/**
 * \brief EPS Instance parameters SET. (vtss_appl_eps_create_param_t)
 *
 * This function will always return an error as parameters cannot be changed on a created instance.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     Create parameters.
 *
 * \return  VTSS_APPL_EPS_RC_ERROR
 */
mesa_rc vtss_appl_eps_create_conf_set(uint32_t                                 instance,
                                      const vtss_appl_eps_create_param_t  *const conf);

/**
 * \brief Instance iterator.
 *
 * This returns the next enabled instance number. When the end is reached VTSS_APPL_EPS_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search start with the lowest possible instance number. 
 *
 * \param instance      [IN]   Instance number.
 * \param next_instance [OUT]  Next enabled instance.
 *
 * \return  VTSS_APPL_EPS_RC_OK
 *          VTSS_APPL_EPS_RC_ERROR
 */
mesa_rc vtss_appl_eps_create_conf_iter(const uint32_t    *const instance,
                                       uint32_t          *const next_instance);

/**
 * \brief EPS Instance configuration SET. (vtss_appl_eps_conf_t)
 *
 * This configuration is set to default during Instance ADD (vtss_appl_eps_create_conf_add())
 * but can be change subsequently through this function.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     Configuration.
 *
 * \return  VTSS_APPL_EPS_RC_INVALID_PARAMETER
 *          VTSS_APPL_EPS_RC_NOT_CREATED
 *          VTSS_APPL_EPS_RC_OK
 */
mesa_rc vtss_appl_eps_conf_set(const uint32_t                   instance,
                               const vtss_appl_eps_conf_t  *const conf);

/**
 * \brief EPS Instance configuration GET. (vtss_appl_eps_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    Configuration.
 *
 * \return  VTSS_APPL_EPS_RC_INVALID_PARAMETER
 *          VTSS_APPL_EPS_RC_NOT_CREATED
 *          VTSS_APPL_EPS_RC_NOT_CONFIGURED
 *          VTSS_APPL_EPS_RC_OK
 */
mesa_rc vtss_appl_eps_conf_get(const uint32_t                      instance,
                               vtss_appl_eps_conf_t           *const conf);

/**
 * \brief EPS Instance MEP configuration SET. (vtss_appl_eps_mep_t)
 *
 * The EPS instance needs to be related to MEP instances for protocol information transport and Signal Fail calculation.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]      MEP Configuration.
 *
 * \return  VTSS_APPL_EPS_RC_INVALID_PARAMETER
 *          VTSS_APPL_EPS_RC_NOT_CREATED
 *          VTSS_APPL_EPS_RC_W_P_SSF_MEP_EQUAL
 *          VTSS_APPL_EPS_RC_INVALID_APS_MEP
 *          VTSS_APPL_EPS_RC_INVALID_W_MEP
 *          VTSS_APPL_EPS_RC_INVALID_P_MEP
 *          VTSS_APPL_EPS_RC_INVALID_APS_MEP
 *          VTSS_APPL_EPS_RC_OK
 */
mesa_rc vtss_appl_eps_mep_conf_set(const uint32_t                    instance,
                                   const vtss_appl_eps_mep_t    *const conf);

/**
 * \brief EPS Instance MEP configuration GET. (vtss_appl_eps_mep_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    MEP Configuration.
 *
 * \return  VTSS_APPL_EPS_RC_INVALID_PARAMETER
 *          VTSS_APPL_EPS_RC_NOT_CREATED
 *          VTSS_APPL_EPS_RC_NOT_CONFIGURED
 *          VTSS_APPL_EPS_RC_OK
 */
mesa_rc vtss_appl_eps_mep_conf_get(const uint32_t              instance,
                                   vtss_appl_eps_mep_t    *const conf);

/**
 * \brief EPS Instance protection group command SET. (vtss_appl_eps_command_conf_t)
 *
 * A command is issued on the protection group.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     Protection group command.
 *
 * \return  VTSS_APPL_EPS_RC_INVALID_PARAMETER
 *          VTSS_APPL_EPS_RC_NOT_CREATED
 *          VTSS_APPL_EPS_RC_NOT_CONFIGURED
 *          VTSS_APPL_EPS_RC_OK
 */
mesa_rc vtss_appl_eps_command_conf_set(const uint32_t                           instance,
                                       const vtss_appl_eps_command_conf_t  *const conf);

/**
 * \brief EPS Instance protection group command GET. (vtss_appl_eps_command_conf_t)
 *
 * \param instance [IN]  Instance number.
 * \param conf [OUT]     Protection group command.
 *
 * \return  VTSS_APPL_EPS_RC_INVALID_PARAMETER
 *          VTSS_APPL_EPS_RC_NOT_CREATED
 */
mesa_rc vtss_appl_eps_command_conf_get(const uint32_t                       instance,
                                       vtss_appl_eps_command_conf_t    *const conf);

/**
 * \brief EPS Instance status SET. (vtss_appl_eps_state_t)
 *
 * \param instance [IN] Instance number.
 * \param status [OUT]  Protection group status.
 *
 * \return  VTSS_APPL_EPS_RC_INVALID_PARAMETER
 *          VTSS_APPL_EPS_RC_NOT_CREATED
 */
mesa_rc vtss_appl_eps_status_get(const uint32_t                instance,
                                 vtss_appl_eps_state_t    *const status);

#ifdef __cplusplus
}
#endif
#endif /* _EPS_API_H_ */
