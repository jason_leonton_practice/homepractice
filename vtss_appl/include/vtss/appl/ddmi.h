/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

/**
 * \file
 * \brief Public API of DDMI
 * \details DDMI is an acronym for Digital Diagnostics Monitoring Interface.
 * It provides an enhanced digital diagnostic monitoring interface for optical
 * transceivers which allows real time access to device operating parameters.
 */

#ifndef _VTSS_APPL_DDMI_H_
#define _VTSS_APPL_DDMI_H_

#include <vtss/appl/types.h>
#include <vtss/appl/interface.h>
#include <vtss/appl/port.h> /* For vtss_appl_sfp_tranceiver_t */

#ifdef __cplusplus
extern "C" {
#endif

/** Maximum string length */
#define VTSS_APPL_DDMI_STR_MAX_LEN      16

/**
 * \brief Global configurations
 */
typedef struct {
    mesa_bool_t    mode; /*!< enable/disable DDMI function */
} vtss_appl_ddmi_config_globals_t;

/**
 * \brief Interface status table
 */
typedef struct {
    /** Support transceiver status information or not.
      * TRUE is supported,
      * FALSE is not supported */
    mesa_bool_t a0_supported;

    /** SFP module is detected or not.
      * TRUE is detected,
      * FALSE is not detected */
    mesa_bool_t a0_sfp_detected;

    char a0_vendor[VTSS_APPL_DDMI_STR_MAX_LEN + 1];        /*!< Vendor name   */
    char a0_part_number[VTSS_APPL_DDMI_STR_MAX_LEN + 1];   /*!< Part number   */
    char a0_serial_number[VTSS_APPL_DDMI_STR_MAX_LEN + 1]; /*!< Serial number */
    char a0_revision[VTSS_APPL_DDMI_STR_MAX_LEN + 1];      /*!< Revision      */
    char a0_date_code[VTSS_APPL_DDMI_STR_MAX_LEN + 1];     /*!< Date code     */

    vtss_appl_sfp_tranceiver_t  a0_sfp_type;    /*!< SFP type */

    /** Support DDMI status information or not.
      * TRUE is supported,
      * FALSE is not supported */
    mesa_bool_t a2_supported;

    char a2_temp_current[VTSS_APPL_DDMI_STR_MAX_LEN + 1];              /*!< Current temperature                */
    char a2_temp_high_alarm_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1]; /*!< Temperature high alarm threshold   */
    char a2_temp_low_alarm_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];  /*!< Temperature low alarm threshold    */
    char a2_temp_high_warn_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];  /*!< Temperature high warning threshold */
    char a2_temp_low_warn_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];   /*!< Temperature low warning threshold  */

    char a2_voltage_current[VTSS_APPL_DDMI_STR_MAX_LEN + 1];              /*!< Current voltage                */
    char a2_voltage_high_alarm_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1]; /*!< Voltage high alarm threshold   */
    char a2_voltage_low_alarm_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];  /*!< Voltage low alarm threshold    */
    char a2_voltage_high_warn_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];  /*!< Voltage high warning threshold */
    char a2_voltage_low_warn_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];   /*!< Voltage low warning threshold  */

    char a2_tx_bias_current[VTSS_APPL_DDMI_STR_MAX_LEN + 1];              /*!< Current Tx bias                */
    char a2_tx_bias_high_alarm_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1]; /*!< Tx bias high alarm threshold   */
    char a2_tx_bias_low_alarm_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];  /*!< Tx bias low alarm threshold    */
    char a2_tx_bias_high_warn_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];  /*!< Tx bias high warning threshold */
    char a2_tx_bias_low_warn_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];   /*!< Tx bias low warning threshold  */

    char a2_tx_power_current[VTSS_APPL_DDMI_STR_MAX_LEN + 1];              /*!< Current Tx power                */
    char a2_tx_power_high_alarm_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1]; /*!< Tx power high alarm threshold   */
    char a2_tx_power_low_alarm_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];  /*!< Tx power low alarm threshold    */
    char a2_tx_power_high_warn_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];  /*!< Tx power high warning threshold */
    char a2_tx_power_low_warn_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];   /*!< Tx power low warning threshold  */

    char a2_rx_power_current[VTSS_APPL_DDMI_STR_MAX_LEN + 1];              /*!< Current Rx power                */
    char a2_rx_power_high_alarm_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1]; /*!< Rx power high alarm threshold   */
    char a2_rx_power_low_alarm_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];  /*!< Rx power low alarm threshold    */
    char a2_rx_power_high_warn_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];  /*!< Rx power high warning threshold */
    char a2_rx_power_low_warn_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];   /*!< Rx power low warning threshold  */
} vtss_appl_ddmi_status_interface_entry_t;

/**
 * \brief DDMI event type
 */
typedef enum {
    VTSS_APPL_DDMI_EVENT_TYPE_TEMPERATURE,   /*!< Temperature event   */
    VTSS_APPL_DDMI_EVENT_TYPE_VOLTAGE,       /*!< Voltage event       */
    VTSS_APPL_DDMI_EVENT_TYPE_BIAS,          /*!< Bias event          */
    VTSS_APPL_DDMI_EVENT_TYPE_TX_POWER,      /*!< Tx Power event      */
    VTSS_APPL_DDMI_EVENT_TYPE_RX_POWER,      /*!< Rx Power event      */
    VTSS_APPL_DDMI_EVENT_TYPE_TOTAL,
} vtss_appl_ddmi_event_type_t;

/**
 * \brief DDMI event state
 */
typedef enum {
    VTSS_APPL_DDMI_EVENT_STATE_REGULAR = 0,  /*!< Change to regular state        */
    VTSS_APPL_DDMI_EVENT_STATE_HI_WARN,      /*!< Change to high warning state   */
    VTSS_APPL_DDMI_EVENT_STATE_LO_WARN,      /*!< Change to low warning state    */
    VTSS_APPL_DDMI_EVENT_STATE_HI_ALARM,     /*!< Change to high alarm state     */
    VTSS_APPL_DDMI_EVENT_STATE_LO_ALARM,     /*!< Change to low alarm state      */
} vtss_appl_ddmi_event_state_t;

/**
 * \brief DDMI event entry
 */
typedef struct {
    vtss_appl_ddmi_event_state_t state;                         /*!< Event state            */  
    char current[VTSS_APPL_DDMI_STR_MAX_LEN + 1];               /*!< Current value          */
    char high_alarm_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];  /*!< high alarm threshold   */
    char low_alarm_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];   /*!< low alarm threshold    */
    char high_warn_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];   /*!< high warning threshold */
    char low_warn_threshold[VTSS_APPL_DDMI_STR_MAX_LEN + 1];    /*!< low warning threshold  */
} vtss_appl_ddmi_event_entry_t;

/*
==============================================================================

    Public APIs

==============================================================================
*/
/**
 * \brief Get global configuration.
 *
 * \param globals [OUT] Global configuration.
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_ddmi_config_globals_get(
    vtss_appl_ddmi_config_globals_t     *const globals
);

/**
 * \brief Set global configuration.
 *
 * \param globals [IN] Global configuration.
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_ddmi_config_globals_set(
    const vtss_appl_ddmi_config_globals_t   *const globals
);

/**
 * \brief Iterate function of interface status table
 *
 * \param prev_ifindex [IN]  ifindex of previous port.
 * \param next_ifindex [OUT] ifindex of next port.
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_ddmi_status_interface_entry_itr(
    const vtss_ifindex_t    *const prev_ifindex,
    vtss_ifindex_t          *const next_ifindex
);

/**
 * \brief Get port status
 *
 * \param ifindex [IN]  ifindex of port
 * \param entry   [OUT] Port status
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_ddmi_status_interface_entry_get(
    vtss_ifindex_t                              ifindex,
    vtss_appl_ddmi_status_interface_entry_t     *const entry
);

/**
 * \brief Iterator for retrieving DDMI event index.
 *
 * Retrieve the 'next' index of the DDMI event table
 * according to the given 'prev'.
 *
 * \param ifx_prev  [IN]  Pointer of the ifindex of DDMI event table
 * \param type_prev [OUT] Pointer of the next ifindex of DDMI event table
 * \param ifx_next  [IN]  Pointer of the type of DDMI event table
 * \param type_next [OUT] Pointer of the next type of DDMI event table
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_ddmi_event_itr(
    const vtss_ifindex_t                    *const ifx_prev,
    vtss_ifindex_t                          *const ifx_next,
    const vtss_appl_ddmi_event_type_t       *const type_prev,
    vtss_appl_ddmi_event_type_t             *const type_next
);

/**
 * \brief Get event status
 *
 * \param ifx_idx  [IN]  ifindex of DDMI event table
 * \param type_idx [IN]  type of DDMI event table
 * \param entry    [OUT] event status
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_ddmi_event_get(
    const vtss_ifindex_t  ifx_idx,
    const vtss_appl_ddmi_event_type_t  type_idx,
    vtss_appl_ddmi_event_entry_t        *const entry
);

#ifdef __cplusplus
}
#endif

#endif  /* _VTSS_APPL_DDMI_H_ */
