/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

/**
 * \file
 * \brief Public Power Over Ethernet(PoE) APIs.
 * \details This header file describes public power over Ethernet APIs.
 *          PoE is used to provide electrical power over standard Ethernet cable.
 *          This allows a single cable to provide both data connection and electrical power to devices.
 *          Device which supply power known as power sourcing equipment(PSE)
 *          and device which consume power known as powered device(PD).
 */

#ifndef __VTSS_APPL_POE_H__
#define __VTSS_APPL_POE_H__

#include <vtss/appl/types.h>
#include <vtss/appl/module_id.h>
#include <vtss/appl/interface.h>
#include <vtss/basics/enum-descriptor.h>    // For vtss_enum_descriptor_t

#ifdef __cplusplus
extern "C" {
#endif

/** Maximum supported switch power supply (in Watts ) */
#if 0
#define VTSS_APPL_POE_POWER_SUPPLY_MAX 180
#else
#define VTSS_APPL_POE_POWER_SUPPLY_MAX          (vtss_appl_poe_get_device_max_budget())
#define VTSS_APPL_POE_POWER_SUPPLY_MAX_FOR_CLI  (240)
#endif

/** Minimum supported switch power supply (in Watts ) */
#define VTSS_APPL_POE_POWER_SUPPLY_MIN 1

/** Default power amount reserved for the system ( in Watts )*/
#define VTSS_APPL_POE_SYSTEM_RESERVE_POWER_DEFAULT 0

/** Definition of error return codes return by the function in case of malfunctioning behavior*/
enum {
    /** Invalid switch id*/
    VTSS_APPL_POE_ERROR_ISID =  MODULE_ERROR_START(VTSS_MODULE_ID_POE),
    VTSS_APPL_POE_ERROR_NULL_POINTER,                /**< Unexpected reference to NULL pointer.*/
    VTSS_APPL_POE_ERROR_UNKNOWN_BOARD,               /**< The board type is unknown.*/
    VTSS_APPL_POE_ERROR_NOT_MASTER,                  /**< Must be master switch in the stack.*/
    VTSS_APPL_POE_ERROR_PRIM_SUPPLY_RANGE,           /**< Primary power supply value out of range.*/
    VTSS_APPL_POE_ERROR_CONF_ERROR,                  /**< Internal error - Configuration could not be done.*/
    VTSS_APPL_POE_ERROR_PORT_POWER_EXCEEDED,         /**< Maximum power for a port is exceeded.*/
    VTSS_APPL_POE_ERROR_NOT_SUPPORTED,               /**< Port is not supporting PoE.*/
    VTSS_APPL_POE_ERROR_FIRMWARE,                    /**< PoE firmware download failed.*/
    VTSS_APPL_POE_ERROR_FIRMWARE_VER,                /**< PoE firmware version not found.*/
    VTSS_APPL_POE_ERROR_FIRMWARE_VER_NOT_NEW,        /**< PoE firmware version not a new version.*/
    VTSS_APPL_POE_ERROR_DETECT,                      /**< PoE chip detection still in progress.*/
};

/**
 * \brief PoE Port status type
 */
typedef enum {
    /** PoE feature is not supported. */
    VTSS_APPL_POE_NOT_SUPPORTED,
    /** PoE is turned OFF due to power budget exceeded on PSE. */
    VTSS_APPL_POE_POWER_BUDGET_EXCEEDED,
    /** No PD detected. */
    VTSS_APPL_POE_NO_PD_DETECTED,
    /** PSE supplying power to PD through PoE. */
    VTSS_APPL_POE_PD_ON,
    /** PD device is switched OFF. */
    VTSS_APPL_POE_PD_OFF,
    /** PD consumes more power than the maximum limit configured on the PSE port.*/
    VTSS_APPL_POE_PD_OVERLOAD,
    /** Unknown state. */
    VTSS_APPL_POE_UNKNOWN_STATE,
    /** PoE feature is disabled on PSE. */
    VTSS_APPL_POE_DISABLED,
    /** PoE disabled due to interface shutdown*/
    VTSS_APPL_POE_DISABLED_INTERFACE_SHUTDOWN
} vtss_appl_poe_status_type_t;

/**
 * \brief PoE Port status
 */
typedef struct {
    /** Powered device(PD) negotiates a power class with sourcing equipment(PSE)
        during the time of initial connection, each class have a maximum supported power.
        Class assigned to PD is based on PD electrical characteristics.
        Value -1 means the PD attached to the port cannot advertise its power class.
    */
    int                         pd_class;
    /** Port PoE status. */
    vtss_appl_poe_status_type_t port_status;
    /** The power(in deciwatt) that the PD is consuming right now. */
    int                         power_consume;
    /** The current(in mA) that the PD is consuming right now. */
    int                         current_consume;

    /** The power requested / reserved by the PD. */
    int power_requested;
} vtss_appl_poe_port_status_t;

/**
 * \brief Types of PoE power management mode
 */
typedef enum {
    /** Maximum port power determined by class,
        and power is managed according to power consumption.
     */
    VTSS_APPL_POE_CLASS_RESERVED,
    /** Maximum port power determined by class,
        and power is managed according to reserved power.
     */
    VTSS_APPL_POE_CLASS_CONSUMP,
    /** Maximum port power determined by allocated,
        and power is managed according to power consumption.
     */
    VTSS_APPL_POE_ALLOCATED_RESERVED,
    /** Maximum port power determined by allocated,
        and power is managed according to reserved power.
     */
    VTSS_APPL_POE_ALLOCATED_CONSUMP,
    /** Maximum port power determined by LLDP Media protocol,
        and power is managed according to reserved power.
    */
    VTSS_APPL_POE_LLDPMED_RESERVED,
    /** Maximum port power determined by LLDP Media protocol,
        and power is managed according to power consumption.
     */
    VTSS_APPL_POE_LLDPMED_CONSUMP
} vtss_appl_poe_management_mode_t;

/**
 * \brief PoE global configuration.
 */
typedef struct {
    /** PoE power management mode. */
    vtss_appl_poe_management_mode_t mgmt_mode;
} vtss_appl_poe_global_conf_t;

/**
 * \brief PoE switch configuration.
 */
typedef struct {
    /** The Maximum power(in watt) that the power sourcing equipment can deliver. */
    int primary_power_supply ;
    /** Used to control capacitor detection feature. */
    mesa_bool_t capacitor_detect;
} vtss_appl_poe_conf_t;

/**
 * \brief Types of port PoE mode
 */
typedef enum {
    /** PoE functionality is disabled. */
    VTSS_APPL_POE_MODE_DISABLED,
    /** Enables PoE based on IEEE 802.3af standard,
        and provides power up to 15.4W(or 154 deciwatt) of DC power to powered device.
     */
    VTSS_APPL_POE_MODE_POE,
    /** Enabled PoE based on IEEE 802.3at standard,
        and provides power up to 30W(or 300 deciwatt) of DC power to powered device.
     */
    VTSS_APPL_POE_MODE_POE_PLUS
} vtss_appl_poe_port_mode_t;

/**
 * \brief Types of port power priority.
 * \details The port power priority, which determines the order in which the port will receive power.
 *          Ports with a higher priority will receive power before ports with a lower priority.
 */
typedef enum {
    /** Least port power priority. */
    VTSS_APPL_POE_PORT_POWER_PRIORITY_LOW,
    /** Medium port power priority. */
    VTSS_APPL_POE_PORT_POWER_PRIORITY_HIGH,
    /** Highest port power priority. */
    VTSS_APPL_POE_PORT_POWER_PRIORITY_CRITICAL
} vtss_appl_poe_port_power_priority_t;

/**
 *  \brief PoE port configurable parameters.
 */
typedef struct {
    /** Indicate whether PoE is configured as VTSS_APPL_POE_MODE_POE or
        VTSS_APPL_POE_MODE_POE_PLUS on a port.
    */
    vtss_appl_poe_port_mode_t           mode;
    /** Indicate port power priority. */
    vtss_appl_poe_port_power_priority_t priority;
    /** Maximum port power(in deciwatt) that can be delivered to remote device.
        Maximum allowed values from 0 to 154 deciwatt in POE standard mode,
        and from 0 to 300 deciwatt in POE plus mode.
    */
    int                                 max_port_power;
} vtss_appl_poe_port_conf_t;

/**
 *   \brief PoE platform specific port capabilities.
 */
typedef struct {
    /** Indicate whether port is PoE capable or not. */
    mesa_bool_t  poe_capable;
} vtss_appl_poe_port_capabilities_t;

/**
 * \brief PoE power supply status
 */
typedef struct {
    mesa_bool_t pwr_in_status1;      /**< first power supply status */
    mesa_bool_t pwr_in_status2;      /**< second power supply status */
    uint32_t total_power_used;     /**< Total Power in DeciWatt currently used */
    uint32_t total_power_reserved; /**< Total amount of power reserved by the PDs */
    uint32_t total_current_used;   /**< Total amount of current in mA currently used */
} vtss_appl_poe_powerin_status_t;

/**
 * \brief PoE power led status
 */
typedef enum {
    VTSS_APPL_POE_LED_NULL,        /* Unkown status*/
    VTSS_APPL_POE_LED_OFF,         /* LED Off */
    VTSS_APPL_POE_LED_GREEN,       /* LED Green */
    VTSS_APPL_POE_LED_RED,         /* LED Red */
    VTSS_APPL_POE_LED_BLINK_RED,   /* LED Blink Red */
} vtss_appl_poe_led_t;

/**
 * \brief PoE led status of two power supplies
 */
typedef struct {
    vtss_appl_poe_led_t pwr_led1;  /**< led color for the 1st power supply */
    vtss_appl_poe_led_t pwr_led2;  /**< led color for the 2nd power supply */
} vtss_appl_poe_powerin_led_t;

/**
 * \brief PoE status led
 */
typedef struct {
    vtss_appl_poe_led_t status_led;  /**< led color indicating PoE status */
} vtss_appl_poe_status_led_t;

/**
 * \brief Get PoE maximum power supply.
 *
 * \return Maximum power supply.
 */
 uint32_t vtss_appl_poe_supply_max_get(void);

/**
 * \brief Get PoE minimum power supply.
 *
 * \return Minimum power supply.
 */
 uint32_t vtss_appl_poe_supply_min_get(void);

/**
 * \brief Get PoE default power supply.
 *
 * \return Default power supply.
 */
 uint32_t vtss_appl_poe_supply_default_get(void);

/**
 * \brief Set PoE global configuration.
 *
 * \param conf  [IN]: PoE global configurable parameters
 *
 * \return VTSS_OK if the operation succeeded.
 */
 mesa_rc vtss_appl_poe_global_conf_set(const vtss_appl_poe_global_conf_t  *const conf);

/**
 * \brief Get PoE global configuration..
 *
 * \param conf  [OUT]: PoE global configurable parameters
 *
 * \return VTSS_OK if the operation succeeded.
 */
 mesa_rc vtss_appl_poe_global_conf_get(vtss_appl_poe_global_conf_t  *const conf);

 /**
 * \brief Set PoE switch configuration.
 *
 * \param usid  [IN]: Switch id
 * \param conf  [IN]: PoE configurable parameters
 *
 * \return VTSS_OK if the operation succeeded.
 */
 mesa_rc vtss_appl_poe_conf_set(vtss_usid_t                 usid,
                                const vtss_appl_poe_conf_t  *const conf);

/**
 * \brief Get PoE switch configuration.
 *
 * \param usid   [IN]:  Switch id
 * \param conf  [OUT]: PoE switch configurable parameters
 *
 * \return VTSS_OK if the operation succeeded.
 */
 mesa_rc vtss_appl_poe_conf_get(vtss_usid_t           usid,
                                vtss_appl_poe_conf_t  *const conf);

/**
 * \brief Set PoE port configuration.
 *
 * \param ifIndex  [IN]: Interface index
 * \param conf     [IN]: PoE port configurable parameters
 *
 * \return VTSS_OK if the operation succeeded.
 */
 mesa_rc vtss_appl_poe_port_conf_set(vtss_ifindex_t                   ifIndex,
                                     const vtss_appl_poe_port_conf_t  *const conf);

/**
 * \brief Get PoE port configuration.
 *
 * \param ifIndex   [IN]: Interface index
 * \param conf     [OUT]: PoE port configurable parameters
 *
 * \return VTSS_OK if the operation succeeded.
 */
 mesa_rc vtss_appl_poe_port_conf_get(vtss_ifindex_t            ifIndex,
                                     vtss_appl_poe_port_conf_t *const conf);


/**
 * \brief Get PoE port status
 *
 * \param ifIndex     [IN]: Interface index
 * \param status     [OUT]: Port status
 *
 * \return VTSS_OK if the operation succeeded.
 */
 mesa_rc vtss_appl_poe_port_status_get(vtss_ifindex_t              ifIndex,
                                       vtss_appl_poe_port_status_t *const status);

/**
 * \brief Get PoE port capabilities
 *
 * \param ifIndex       [IN]: Interface index
 * \param capabilities [OUT]: PoE platform specific port capabilities
 *
 * \return VTSS_OK if the operation succeeded.
 */
 mesa_rc vtss_appl_poe_port_capabilities_get(vtss_ifindex_t                    ifIndex,
                                             vtss_appl_poe_port_capabilities_t *const capabilities);

/**
 * \brief Function for getting the current power supply status
 *
 * \param powerin_status [IN,OUT] The status of the power supply.
 *
 * \return VTSS_OK if the operation succeeded.
 **/
 mesa_rc vtss_appl_poe_powerin_status_get(vtss_appl_poe_powerin_status_t *const powerin_status);

/**
 * \brief Function for getting the status of the 2 power leds on the PoE board.
 *
 * \param pwr_led [IN/OUT] Led status for power supply.
 *
 * \return VTSS_RC_OK  if operation is succeeded.
 **/
mesa_rc vtss_appl_poe_powerin_led_get(vtss_appl_poe_powerin_led_t *const pwr_led);

/**
 * \brief Function for getting PoE status led color
 *
 * \param status_led [IN/OUT] The current color of the PoE status led.
 *
 * \return VTSS_RC_OK if operation succeeded.
 **/
mesa_rc vtss_appl_poe_status_led_get(vtss_appl_poe_status_led_t *status_led);


//20180326 retrieve max power budget
u32 vtss_appl_poe_get_device_max_budget(void);

#ifdef __cplusplus
}  // extern "C"
#endif
#endif  // __VTSS_APPL_POE_H__
