/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

/**
* \file
* \brief Public Performance Monitor API
* \details This header file describes Performance Monitor (abbreviated as 'PM')
* control functions and types.
* PM feature enables monitoring Maintenance Entity Point (abbreviated as 'MEP')
* statistics with Loss Measurement (abbreviated as 'LM') and Delay Measurement
* (abbreviated as 'DM').
* In addition, Ethernet Virtual Connection (abbreviated as 'EVC') statistics are
* also able to be tracked by PM.
* PM is used for gathering the MEP statistics (DM and LM) and EVC statistics
* periodically. The data can be saved into the local flash or remote storage
* automatically via configuring the transfer scheduler.
* According to this data, user can then analyze the MEP and EVC traffic if any
* fault occurs in the past and present.
*/

#ifndef _VTSS_APPL_PERFORMANCE_MONITOR_H_
#define _VTSS_APPL_PERFORMANCE_MONITOR_H_

#include <vtss/appl/types.h>
#include <vtss/appl/module_id.h>
#include <vtss/appl/interface.h>
#include <vtss/appl/mep.h>
#include <vtss/appl/evc.h>

#ifdef __cplusplus
extern "C" {
#endif

#define VTSS_APPL_PM_STRING_MAX_LEN     64                                      /**< Maximum number of characters used for PM string. */
#define VTSS_APPL_PM_MAX_URL_LEN        (VTSS_APPL_PM_STRING_MAX_LEN + 1)       /**< Maximum string length for server directory URL used in PM. */
#define VTSS_APPL_PM_MAX_TIME_STR_LEN   (VTSS_APPL_PM_STRING_MAX_LEN + 1)       /**< Maximum string length for date and time string used in PM. */

/*! \brief PM API return code. */
typedef enum {
    VTSS_APPL_PM_ERROR_GENERAL = MODULE_ERROR_START(VTSS_MODULE_ID_PERF_MON),   /**< General PM operation error. */
    VTSS_APPL_PM_ERROR_PARAM,                                                   /**< Illegal parameter. */
    VTSS_APPL_PM_ERROR_ENTRY_NOT_FOUND,                                         /**< Entry not found. */
    VTSS_APPL_PM_ERROR_ENTRY_ALREADY_EXISTS,                                    /**< Entry already exists. */
    VTSS_APPL_PM_ERROR_MEMORY,                                                  /**< Memory access failure. */
    VTSS_APPL_PM_ERROR_COUNTERS_CLEAR_FAILURE,                                  /**< Counters clear failure. */
    VTSS_APPL_PM_ERROR_CREATE_LM_FLASH_BLOCK,                                   /**< Create LM Report Block error. */
    VTSS_APPL_PM_ERROR_CREATE_DM_FLASH_BLOCK,                                   /**< Create DM Report Block error. */
    VTSS_APPL_PM_ERROR_CREATE_DM_BIN_FLASH_BLOCK,                               /**< Create DM BIN Report Block error. */
    VTSS_APPL_PM_ERROR_CREATE_EVC_FLASH_BLOCK,                                  /**< Create EVC Report Block error. */
    VTSS_APPL_PM_ERROR_LOAD_LM_FLASH_BLOCK,                                     /**< Load LM Report Block error. */
    VTSS_APPL_PM_ERROR_LOAD_DM_FLASH_BLOCK,                                     /**< Load DM Report Block error. */
    VTSS_APPL_PM_ERROR_LOAD_DM_BIN_FLASH_BLOCK,                                 /**< Load DM BIN Report Block error. */
    VTSS_APPL_PM_ERROR_LOAD_EVC_FLASH_BLOCK                                     /**< Load EVC Report Block error. */
} vtss_appl_pm_error_t;

/*! \brief PM transfer interval mode. */
typedef enum {
    VTSS_APPL_PM_TRANSFER_NONE = 0,                 /**< None of PM result will be sent. */
    VTSS_APPL_PM_TRANSFER_ALL,                      /**< PM result from all available intervals will be sent. */
    VTSS_APPL_PM_TRANSFER_NEW,                      /**< PM result from new intervals since last transfer will be sent. */
    VTSS_APPL_PM_TRANSFER_FIXED_AMOUNT              /**< PM result from a fixed number of intervals will be sent. */
} vtss_appl_pm_transfer_mode_t;

/*! \brief The port type for EVC statistics. */
typedef enum {
    VTSS_APPL_PM_EVC_PORT_TYPE_NNI = 0,             /**< NNI port type. */
    VTSS_APPL_PM_EVC_PORT_TYPE_UNI,                 /**< UNI port type. */
} vtss_appl_pm_evc_port_type_t;

/**
 * \brief The PM global configuration.
 * This configuration is the global setting that can manage the PM functions.
 */
typedef struct {
    /**
     * \brief Administrative control for PM's LM function,
     * TRUE is to enable monitoring LM and FALSE is to disable it.
     */
    mesa_bool_t                                lm_admin_state;
    /**
     * \brief Administrative control for storing PM's LM result,
     * TRUE is to enable storing LM statistics and FALSE is to disable it.
     */
    mesa_bool_t                                lm_storage_state;
    /**
     * \brief Interval for tracking LM, in minute.
     * The configurable value ranges from 1 to 60.
     */
    uint8_t                                  lm_interval;
    /**
     * \brief Administrative control for PM's DM function,
     * TRUE is to enable monitoring DM and FALSE is to disable it.
     */
    mesa_bool_t                                dm_admin_state;
    /**
     * \brief Administrative control for storing PM's DM result,
     * TRUE is to enable storing DM statistics and FALSE is to disable it.
     */
    mesa_bool_t                                dm_storage_state;
    /**
     * \brief Administrative control for storing PM's DM bucket result,
     * TRUE is to enable storing DM bucket statistics and FALSE is to disable it.
     */
    mesa_bool_t                                dm_bin_storage_state;
    /**
     * \brief Interval for tracking DM, in minute.
     * The configurable value ranges from 1 to 60.
     */
    uint8_t                                  dm_interval;
    /**
     * \brief Administrative control for PM's EVC monitoring function,
     * TRUE is to enable monitoring EVC and FALSE is to disable it.
     */
    mesa_bool_t                                evc_admin_state;
    /**
     * \brief Administrative control for storing PM's EVC monitoring result,
     * TRUE is to enable storing EVC statistics and FALSE is to disable it.
     */
    mesa_bool_t                                evc_storage_state;
    /**
     * \brief Interval for tracking EVC, in minute.
     * The configurable value ranges from 1 to 60.
     */
    uint8_t                                  evc_interval;
} vtss_appl_pm_global_conf_t;

/**
 * \brief The PM transfer configuration.
 * This configuration is the setting that manages sending PM's result.
 */
typedef struct {
    /**
     * \brief Administrative control for PM's transfer function,
     * TRUE is to enable sending PM's monitoring result and FALSE is to disable it.
     */
    mesa_bool_t                                admin_state;
    /**
     * \brief The URL for the server directory that keeps PM's monitoring result.
     */
    char                                server_url[VTSS_APPL_PM_MAX_URL_LEN];
    /**
     * \brief Transfer mode, based on selected interval type, for sending PM results.
     * When VTSS_APPL_PM_TRANSFER_NONE is set, none of PM result will be sent.
     * When VTSS_APPL_PM_TRANSFER_ALL is set, PM result from all available
     * intervals will be sent.
     * When VTSS_APPL_PM_TRANSFER_NEW is set, PM result from new intervals
     * since last transfer will be sent.
     * When VTSS_APPL_PM_TRANSFER_FIXED_AMOUNT is set, PM result from a
     * fixed number of intervals will be sent.
     */
    vtss_appl_pm_transfer_mode_t        transfer_interval_mode;
    /**
     * \brief PM result from this fixed number of intervals will be sent.
     * It is working only when 'transfer_interval_mode' is VTSS_APPL_PM_TRANSFER_FIXED_AMOUNT.
     * The configurable value ranges from 1 to 96.
     */
    uint8_t                                  fixed_interval_amount;
    /**
     * \brief Administrative control to retry sending PM result from last incompleted transmission,
     * TRUE is to enable retrying the incompleted PM result transfer and FALSE is to disable it.
     */
    mesa_bool_t                                retry_incomplete_transmission;
} vtss_appl_pm_transfer_conf_t;

/**
 * \brief The PM transfer schedule configuration.
 * This configuration is the setting that manages PM transfer schedule.
 */
typedef struct {
    /**
     * \brief Administrative control for managing the transfer schedule,
     * TRUE is to enable specific time for transfer schedule and FALSE is to disable it.
     */
    mesa_bool_t                                do_transfer;
} vtss_appl_pm_transfer_schedule_t;

/**
 * \brief The PM transfer schedule offset configuration.
 * This configuration is the setting that manages PM transfer schedule offset.
 */
typedef struct {
    /**
     * \brief The scheduled transfer time offset by minutes.
     * The configurable value ranges from 0 to 15.
     */
    uint8_t                                  transfer_minute_offset;
    /**
     * \brief The scheduled transfer time random offset by seconds.
     * It provides a range for choosing a random second offset.
     * The configurable value ranges from 0 to 900.
     */
    uint16_t                                 transfer_random_second_offset;
} vtss_appl_pm_transfer_offset_t;

/**
 * \brief The index of PM LM/DM/EVC statistics tables.
 */
typedef struct {
    /**
     * \brief The measurement interval ID for the performance monitor data sets.
     */
    uint32_t                                 measurement_interval_id;
    /**
     * \brief The entry ID for the performance monitor data sets.
     */
    uint32_t                                 entry_id;
} vtss_appl_pm_stats_index_t;

/**
 * \brief The index of PM LM MEP peer statistics tables.
 */
typedef struct {
    /**
     * \brief The measurement interval ID for the performance monitor data sets.
     */
    uint32_t                            measurement_interval_id;
    /**
     * \brief The entry ID for the performance monitor data sets.
     */
    uint32_t                            entry_id;

    /**
     * \brief The MEP peer ID for the performance monitor data sets.
     */
    uint32_t                            peer_id;
} vtss_appl_pm_stats_peer_index_t;

/**
 * \brief Information about a peer MEP in the PM LM statistics tables.
 */
typedef struct {

    /**
     * \brief An expected MEP identifier in the received CCM.
     */
    uint16_t                            mep_peer_identifier;
    /**
     * \brief MAC address to be used when unicast is selected with
     * the peer MEP. Also, this MAC is used to create HW checking
     * of receiving CCM PDU from this MEP.
     */
    mesa_mac_t                          mep_peer_mac_addr;

} vtss_appl_pm_mep_peer_info_t;

/**
 * \brief The MEP information in LM and DM statistics data.
 */
typedef struct {
    /**
     * \brief The MEP instance for the performance monitor data sets.
     */
    uint16_t                            mep_instance;
    /**
     * \brief The MEP is related to this flow depends on 'mep_domain'.
     */
    vtss_ifindex_t                      mep_flow_instance;
    /**
     * \brief The MEP flow name.
     */
    char                                mep_flow_name[VTSS_APPL_EVC_NAME_MAX_LEN];
     /**
     * \brief An outer C/S-tag or the subscriber VID that identifies
     * the subscriber flow in EVC where the MIP is active.
     */
    mesa_vid_t                          mep_tagged_vid;

    /**
     * \brief Upstream or downstream that needs to be monitored.
     */
    vtss_appl_mep_direction_t           mep_direction;
    /**
     * \brief The MEG level of this MEP.
     */
    uint8_t                             mep_level;
    /**
     * \brief The priority to be inserted as PCP bits in TAG (if any).
     */
    mesa_prio_t                         priority;
    /**
     * \brief Two bytes CCM MEP ID in the transmitted CCM.
     */
    uint16_t                            mep_identifier;
    /**
     * \brief MAC address of this MEP which can be used by other MEP
     * when unicast is selected.
     */
    mesa_mac_t                          mep_mac_addr;
    /**
     * \brief The residence port for the MEP.
     */
    vtss_ifindex_t                      residence_port;
    /**
     * \brief An expected MEP identifier in the received CCM.
     */
    uint16_t                            mep_peer_identifier;
    /**
     * \brief MAC address to be used when unicast is selected with
     * the peer MEP. Also, this MAC is used to create HW checking
     * of receiving CCM PDU from this MEP.
     */
    mesa_mac_t                          mep_peer_mac_addr;
    /**
     * \brief Number of peers for the MEP.
     */
    uint32_t                            peer_count;
    /**
     * \brief Peer information for the MEP.
     */
    vtss_appl_pm_mep_peer_info_t        peer_info[VTSS_APPL_MEP_PEER_MAX];

} vtss_appl_pm_mep_info_t;

/**
 * \brief PM LM peer MEP statistics.
 */
typedef struct {

    /**
     * \brief The number of frame transmitted.
     */
    uint32_t                                 tx_count;
    /**
     * \brief The number of frame received.
     */
    uint32_t                                 rx_count;
    /**
     * \brief The near end loss count.
     */
    uint32_t                                 near_end_loss_count;
    /**
     * \brief The near end loss ratio.
     */
    uint32_t                                 near_end_loss_rate;
    /**
     * \brief The far end loss count.
     */
    uint32_t                                 far_end_loss_count;
    /**
     * \brief The far end loss ratio.
     */
    uint32_t                                 far_end_loss_rate;

} vtss_appl_pm_lm_peer_stats_data_t;

/**
 * \brief The LM statistics data.
 * It provides the PM traffic statistics regarding packet loss.
 */
typedef struct {
    /**
     * \brief The MEP information.
     */
    vtss_appl_pm_mep_info_t             mep_info;
    /**
     * \brief Frame rate of CCM/LMM PDU which is the inverse of
     * transmission period as described in Y.1731.
     */
    vtss_appl_mep_rate_t                rate;

    /**
     * \brief The MEP peer statistics
     */
    vtss_appl_pm_lm_peer_stats_data_t   peer_stats[VTSS_APPL_MEP_PEER_MAX];

} vtss_appl_pm_lm_stats_data_t;

/**
 * \brief The PM data provides the performance monitor traffic statistics for the selected measurement interval ID and entry ID.
 */
typedef struct {
    vtss_appl_pm_mep_info_t             mep_info;                           /**< The MEP information. */
    uint8_t                             tx_rate;                            /**< The gap between transmitting 1DM/DMM PDU in 10ms. The range is 10 to 65535. */
    vtss_appl_mep_dm_tunit_t            measurement_unit;                   /**< The time resolution. */
    uint32_t                            tx_cnt;                             /**< The number of frame transmitted. */
    uint32_t                            rx_cnt;                             /**< The number of frame received. */
    uint32_t                            far_to_near_avg_delay;              /**< The far to near average delay. */
    uint32_t                            far_to_near_avg_delay_variation;    /**< The far to near average delay variation. */
    uint32_t                            far_to_near_max_delay;              /**< The maximum far to near delay. */
    uint32_t                            far_to_near_max_delay_variation;    /**< The maximum far to near delay variation. */
    uint32_t                            far_to_near_min_delay;              /**< The minimum far to near delay. */
    uint32_t                            far_to_near_min_delay_variation;    /**< The minimum far to near delay variation. */
    uint32_t                            near_to_far_avg_delay;              /**< The near to far average delay. */
    uint32_t                            near_to_far_avg_delay_variation;    /**< The near to far average delay variation. */
    uint32_t                            near_to_far_max_delay;              /**< The maximum near to far delay. */
    uint32_t                            near_to_far_max_delay_variation;    /**< The maximum near to far delay variation. */
    uint32_t                            near_to_far_min_delay;              /**< The minimum near to far delay. */
    uint32_t                            near_to_far_min_delay_variation;    /**< The minimum near to far delay variation. */
    uint32_t                            two_way_avg_delay;                  /**< The two-way average delay. */
    uint32_t                            two_way_avg_delay_variation;        /**< The two-way average delay variation. */
    uint32_t                            two_way_max_delay;                  /**< The maximum two-way delay.*/
    uint32_t                            two_way_max_delay_variation;        /**< The maximum two-way delay variation.*/
    uint32_t                            two_way_min_delay;                  /**< The minimum two-way delay.*/
    uint32_t                            two_way_min_delay_variation;        /**< The minimum two-way delay variation.*/
} vtss_appl_pm_dm_stats_data_t;

/**
 * \brief The index of PM DM bin statistics table.
 */
typedef struct {
    vtss_appl_pm_stats_index_t          statidx;            /**< The index for the DM statistics. */
    uint32_t                            type;               /**< The type for DM bin statistics. */
    uint32_t                            direction;          /**< The direction for DM bin statistics. */
    uint32_t                            bin_id;             /**< The bin ID. */
} vtss_appl_pm_dm_bin_stats_index_t;

/**
 * \brief The PM DM bin data provides the counter for the delay frames.
 * The DM bin is a counter that stores the number of delay measurements falling within a specified range,
 * during a measurement interval. The range and the used bin number are configured in MEP module. The default
 * delay threshold is 5000 us, and the default used bin is 3. If the measurement threshold is 5000 us and
 * the total number of Measurement Bins is four, the meaning of the value in the bins are:
 \verbatim
         Bin        Threshold                   Range
         bin0       0 us            0 us <= measurement < 5,000 us
         bin1       5,000 us        5,000 us <= measurement < 10,000 us
         bin2       10,000 us       10,000 us <= measurement < 15,000 us
         bin3       15,000 us       15,000 us <= measurement < infinite us
 \endverbatim
 */
typedef struct {
    uint32_t                            hit_count;          /**< The hit count for the DM bin */
} vtss_appl_pm_dm_bin_stats_data_t;

/**
 * \brief The EVC data provides the performance monitor traffic statistics for the selected measurement interval ID and entry ID.
 */
typedef struct {
    vtss_appl_pm_evc_port_type_t        port_type;          /**< The port type for the EVC. */
    vtss_ifindex_t                      port;               /**< The interface ID for the EVC. */
    mesa_prio_t                         cos;                /**< The COS queue ID on the interface, VTSS_PRIO_NO_NONE indicates the port is NNI port. */
    uint16_t                            evc_instance;       /**< The EVC instance for the performance monitor data sets. */
    uint64_t                            rx_green;           /**< The number of green frames received. */
    uint64_t                            rx_yellow;          /**< The number of yellow frames received. */
    uint64_t                            rx_red;             /**< The number of red frames received. */
    uint64_t                            rx_discard;         /**< The number of frames discarded in the ingress queue system. */
    uint64_t                            tx_green;           /**< The number of green frames transmitted. */
    uint64_t                            tx_yellow;          /**< The number of yellow frames transmitted. */
    uint64_t                            tx_discard;         /**< The number of frames discarded in the egress queue system. */
    uint64_t                            rx_green_b;         /**< The number of green bytes received. */
    uint64_t                            rx_yellow_b;        /**< The number of yellow bytes received. */
    uint64_t                            rx_red_b;           /**< The number of red bytes received. */
    uint64_t                            rx_discard_b;       /**< The number of bytes discarded in the ingress queue system. */
    uint64_t                            tx_green_b;         /**< The number of green bytes transmitted. */
    uint64_t                            tx_yellow_b;        /**< The number of yellow bytes transmitted. */
    uint64_t                            tx_discard_b;       /**< The number of bytes discarded in the egress queue system. */
} vtss_appl_pm_evc_stats_data_t;

/**
 * \brief The index of PM interval information table.
 */
typedef struct {
    /**
     * \brief The type of measurement.
     */
    uint32_t                                 information_type;
    /**
     * \brief The identifier of measurement interval.
     */
    uint32_t                                 interval_id;
} vtss_appl_pm_intvl_info_index_t;

/**
 * \brief The PM Interval Information.
 * It provides the PM measurement interval time information.
 */
typedef struct {
    /**
     * \brief The date and time when PM starts.
     */
    char                                start_time[VTSS_APPL_PM_MAX_TIME_STR_LEN];
    /**
     * \brief The date and time when PM stops.
     */
    char                                end_time[VTSS_APPL_PM_MAX_TIME_STR_LEN];
    /**
     * \brief The elapsed time ticks for the specific monitoring type and
     * the corresponding interval identifier.
     */
    mesa_timestamp_t                    elapsed_time;
} vtss_appl_pm_interval_info_t;

/**
 * \brief The PM global control actions.
 * It provides the PM actions to clear the statistics.
 */
typedef struct {
    /**
     * \brief Clear the LM statistics.
     * Set the value TRUE to clear LM statistics.
     */
    mesa_bool_t                                lm_delete_stats_all;
    /**
     * Clear the DM statistics.
     * Set the value TRUE to clear DM statistics.
     */
    mesa_bool_t                                dm_delete_stats_all;
    /**
     * Clear the EVC statistics.
     * Set the value TRUE to clear EVC statistics.
     */
    mesa_bool_t                                evc_delete_stats_all;
} vtss_appl_pm_control_globals_t;

/**
 * \brief Get PM global default configuration.
 *
 * Get default configuration of the PM global setting.
 *
 * \param entry     [OUT]   The default configuration of the PM global setting.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_global_config_default(
    vtss_appl_pm_global_conf_t              *const entry
);

/**
 * \brief Get PM global configuration.
 *
 * Get configuration of the PM global setting.
 *
 * \param entry     [OUT]   The current configuration of the PM global setting.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_global_config_get(
    vtss_appl_pm_global_conf_t              *const entry
);

/**
 * \brief Set PM global configuration.
 *
 * Modify configuration of the PM global setting.
 *
 * \param entry     [IN]    The revised configuration of the PM global setting.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_global_config_set(
    const vtss_appl_pm_global_conf_t        *const entry
);

/**
 * \brief Get PM transfer default configuration.
 *
 * Get default configuration of the PM transfer setting.
 *
 * \param entry     [OUT]   The default configuration of the PM transfer setting.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_transfer_default(
    vtss_appl_pm_transfer_conf_t            *const entry
);

/**
 * \brief Get PM transfer configuration.
 *
 * Get configuration of the PM transfer setting.
 *
 * \param entry     [OUT]   The current configuration of the PM transfer setting.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_transfer_config_get(
    vtss_appl_pm_transfer_conf_t            *const entry
);

/**
 * \brief Set PM transfer configuration.
 *
 * Modify configuration of the PM transfer setting.
 *
 * \param entry     [IN]    The revised configuration of the PM transfer setting.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_transfer_config_set(
    const vtss_appl_pm_transfer_conf_t      *const entry
);

/**
 * \brief Iterator for retrieving PM hourly scheduled transfer configuration table index.
 *
 * Retrieve the 'next' configuration index of the PM hourly scheduled transfer configuration
 * table according to the given 'prev'.
 *
 * \param prev      [IN]    Pointer of hour index to be used for index determination.
 *                          '0' is the first and a valid hour value to be used for indexing.
 *
 * \param next      [OUT]   The next index should be used for the table entry.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return Error code.      VTSS_RC_OK for success operation and the value in 'next' is valid,
 *                          other error code means that no "next" index and its corresponding
 *                          entry exists, and the end has been reached.
 */
mesa_rc vtss_appl_pm_schedule_hour_itr(
    const uint32_t                               *const prev,
    uint32_t                                     *const next
);

/**
 * \brief Get PM hourly scheduled transfer default configuration.
 *
 * Get default configuration of the PM hourly scheduled transfer.
 *
 * \param timeidx   [OUT]   The hour value to be used for PM hourly scheduled transfer.
 * \param entry     [OUT]   The default configuration of the PM hourly scheduled transfer.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_schedule_hour_default(
    uint32_t                                *const timeidx,
    vtss_appl_pm_transfer_schedule_t        *const entry
);

/**
 * \brief Get PM specific hourly scheduled transfer configuration.
 *
 * Get configuration of the specific PM hourly scheduled transfer.
 *
 * \param timeidx   [IN]    (key) Hour value to be used of PM hourly scheduled transfer.
 *
 * \param entry     [OUT]   The current configuration of the specific PM hourly scheduled transfer.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_schedule_hour_get(
    const uint32_t                          *const timeidx,
    vtss_appl_pm_transfer_schedule_t        *const entry
);

/**
 * \brief Set PM specific hourly scheduled transfer configuration.
 *
 * Modify configuration of the specific PM hourly scheduled transfer.
 *
 * \param timeidx   [IN]    (key) Hour value to be used of PM hourly scheduled transfer.
 * \param entry     [IN]    The revised configuration of the specific PM hourly scheduled transfer.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_schedule_hour_set(
    const uint32_t                          *const timeidx,
    const vtss_appl_pm_transfer_schedule_t  *const entry
);

/**
 * \brief Iterator for retrieving PM quarterly scheduled transfer configuration table index.
 *
 * Retrieve the 'next' configuration index of the PM quarterly scheduled transfer configuration
 * table according to the given 'prev'.
 *
 * \param prev      [IN]    Pointer of quarter index to be used for index determination.
 *                          '0' is the first and a valid quarter value to be used for indexing.
 *
 * \param next      [OUT]   The next index should be used for the table entry.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return Error code.      VTSS_RC_OK for success operation and the value in 'next' is valid,
 *                          other error code means that no "next" index and its corresponding
 *                          entry exists, and the end has been reached.
 */
mesa_rc vtss_appl_pm_schedule_quarter_itr(
    const uint32_t                          *const prev,
    uint32_t                                *const next
);

/**
 * \brief Get PM quarterly scheduled transfer default configuration.
 *
 * Get default configuration of the PM quarterly scheduled transfer.
 *
 * \param timeidx   [OUT]   The quarter value to be used for PM quarterly scheduled transfer.
 * \param entry     [OUT]   The default configuration of the PM quarterly scheduled transfer.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_schedule_quarter_default(
    uint32_t                                *const timeidx,
    vtss_appl_pm_transfer_schedule_t        *const entry
);

/**
 * \brief Get PM specific quarterly scheduled transfer configuration.
 *
 * Get configuration of the specific PM quarterly scheduled transfer.
 *
 * \param timeidx   [IN]    (key) Quarter value to be used of PM quarterly scheduled transfer.
 *
 * \param entry     [OUT]   The current configuration of the specific PM quarterly scheduled transfer.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_schedule_quarter_get(
    const uint32_t                          *const timeidx,
    vtss_appl_pm_transfer_schedule_t        *const entry
);

/**
 * \brief Set PM specific quarterly scheduled transfer configuration.
 *
 * Modify configuration of the specific PM quarterly scheduled transfer.
 *
 * \param timeidx   [IN]    (key) Quarter value to be used of PM quarterly scheduled transfer.
 * \param entry     [IN]    The revised configuration of the specific PM quarterly scheduled transfer.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_schedule_quarter_set(
    const uint32_t                          *const timeidx,
    const vtss_appl_pm_transfer_schedule_t  *const entry
);

/**
 * \brief Get PM transfer schedule offset default configuration.
 *
 * Get default configuration of the PM transfer schedule offset setting.
 *
 * \param entry     [OUT]   The default configuration of the PM transfer schedule offset setting.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_transfer_offset_default(
    vtss_appl_pm_transfer_offset_t          *const entry
);

/**
 * \brief Get PM transfer schedule offset configuration.
 *
 * Get configuration of the PM transfer schedule offset setting.
 *
 * \param entry     [OUT]   The current configuration of the PM transfer schedule offset setting.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_transfer_offset_get(
    vtss_appl_pm_transfer_offset_t          *const entry
);

/**
 * \brief Set PM transfer schedule offset configuration.
 *
 * Modify configuration of the PM transfer schedule offset setting.
 *
 * \param entry     [IN]    The revised configuration of the PM transfer schedule offset setting.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_transfer_offset_set(
    const vtss_appl_pm_transfer_offset_t    *const entry
);

/**
 * \brief Iterator for retrieving PM LM statistics table index.
 *
 * Retrieve the 'next' statistics index of the PM LM statistics table
 * according to the given 'prev'.
 *
 * \param prev      [IN]    Pointer of LM statistics index to be used for index determination.
 *                          LM statistics index is composed of the measurement interval ID and
 *                          the entry ID. Measurement interval ID has higher priority in index
 *                          determination.
 *
 * \param next      [OUT]   The next index should be used for the table entry.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return Error code.      VTSS_RC_OK for success operation and the value in 'next' is valid,
 *                          other error code means that no "next" index and its corresponding
 *                          entry exists, and the end has been reached.
 */
mesa_rc vtss_appl_pm_lm_stats_itr(
    const vtss_appl_pm_stats_index_t        *const prev,
    vtss_appl_pm_stats_index_t              *const next
);

/**
 * \brief Iterator for retrieving PM LM statistics peer table index.
 *
 * Retrieve the 'next' statistics index of the PM LM peer statistics table
 * according to the given 'prev'.
 *
 * \param prev      [IN]    Pointer of LM statistics index to be used for index determination.
 *                          LM statistics index is composed of the measurement interval ID and
 *                          the entry ID. Measurement interval ID has higher priority in index
 *                          determination.
 *
 * \param next      [OUT]   The next index should be used for the table entry.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return Error code.      VTSS_RC_OK for success operation and the value in 'next' is valid,
 *                          other error code means that no "next" index and its corresponding
 *                          entry exists, and the end has been reached.
 */
mesa_rc vtss_appl_pm_lm_stats_peer_itr(
    const vtss_appl_pm_stats_peer_index_t   *const prev,
    vtss_appl_pm_stats_peer_index_t         *const next
);

/**
 * \brief Get PM specific LM statistics table entry.
 *
 * Get data of the specific LM statistics.
 *
 * \param statidx   [IN]    (key) Measurement interval ID and entry ID of PM LM statistics.
 *
 * \param data      [OUT]   The statistics of the specific PM LM.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_lm_stats_get(
    const vtss_appl_pm_stats_index_t        *const statidx,
    vtss_appl_pm_lm_stats_data_t            *const data
);

/**
 * \brief Get PM specific LM peer statistics table entry.
 *
 * Get data of the specific LM peer statistics.
 *
 * \param statidx   [IN]    (key) Measurement interval ID and entry ID of PM LM statistics.
 *
 * \param data      [OUT]   The statistics of the specific PM LM.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_lm_stats_peer_get(
    const vtss_appl_pm_stats_peer_index_t   *const statidx,
    vtss_appl_pm_lm_stats_data_t            *const data
);

/**
 * \brief Iterate function of DM statistics table.
 *
 * Retrieve the 'next' configuration index of the PM DM statistics table according to the given 'prev'.
 *
 * \param prev      [IN]    Pointer of DM statics table's index which is composed of measurement
 *                          interval ID and entry ID to be used for index determination.
 *                          The NULL pointer is used for getting the first entry.
 * \param next      [OUT]   The next index should be used for the table entry.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_dm_stats_itr(
    const vtss_appl_pm_stats_index_t        *const prev,
    vtss_appl_pm_stats_index_t              *const next
);

/**
 * \brief Get function of DM statistics table.
 *
 * To get the DM data set.
 *
 * \param statidx   [IN]    (key) The key which is composed of measurement interval ID and entry ID.
 * \param data      [OUT]   The statistics of the specific entry.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_dm_stats_get(
    const vtss_appl_pm_stats_index_t        *const statidx,
    vtss_appl_pm_dm_stats_data_t            *const data
);

/**
 * \brief Iterate function of DM bin statistics table.
 *
 * Retrieve the 'next' configuration index of the PM DM bin statistics table according to the given 'prev'.
 *
 * \param prev      [IN]    Pointer of DM bin statics table's index.
 *                          The NULL pointer is used for getting the first entry.
 * \param next      [OUT]   The next index should be used for the table entry.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_dm_bin_stats_itr(
    const vtss_appl_pm_dm_bin_stats_index_t *const prev,
    vtss_appl_pm_dm_bin_stats_index_t       *const next
);

/**
 * \brief Get function of DM bin statistics table.
 *
 * To get the DM bin data set.
 *
 * \param statidx   [IN]    (key) The key which is used for getting the DM bin statistics data.
 * \param data      [OUT]   The statistics of the specific entry.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_dm_bin_stats_get(
    const vtss_appl_pm_dm_bin_stats_index_t *const statidx,
    vtss_appl_pm_dm_bin_stats_data_t        *const data
);

/**
 * \brief Iterate function of EVC statistics table.
 *
 * Retrieve the 'next' configuration index of the PM EVC statistics table according to the given 'prev'.
 *
 * \param prev      [IN]    Pointer of EVC statics table's index which is composed of measurement
 *                          interval ID and entry ID to be used for index determination.
 *                          The NULL pointer is used for getting the first entry.
 * \param next      [OUT]   The next index should be used for the table entry.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_evc_stats_itr(
    const vtss_appl_pm_stats_index_t        *const prev,
    vtss_appl_pm_stats_index_t              *const next
);

/**
 * \brief Get function of EVC statistics table.
 *
 * To get the EVC data set. The data set show NNI port traffic statistics for the selected EVC.
 * It also shows counters for UNI ports of ECEs mapping.
 *
 * \param statidx   [IN]    (key) The key which is composed of measurement interval ID and entry ID.
 * \param data      [OUT]   The statistics of the specific entry.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_evc_stats_get(
    const vtss_appl_pm_stats_index_t        *const statidx,
    vtss_appl_pm_evc_stats_data_t           *const data
);

/**
 * \brief Iterator for retrieving PM interval information table index.
 *
 * Retrieve the 'next' index of the PM interval information table
 * according to the given 'prev'.
 *
 * \param prev      [IN]    Pointer of interval information index to be used for index determination.
 *                          Interval information index is composed of the measurement type and the
 *                          interval ID. Measurement type has higher priority in index determination.
 *
 * \param next      [OUT]   The next index should be used for the table entry.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return Error code.      VTSS_RC_OK for success operation and the value in 'next' is valid,
 *                          other error code means that no "next" index and its corresponding
 *                          entry exists, and the end has been reached.
 */
mesa_rc vtss_appl_pm_interval_info_itr(
    const vtss_appl_pm_intvl_info_index_t   *const prev,
    vtss_appl_pm_intvl_info_index_t         *const next
);

/**
 * \brief Get PM specific interval information table entry.
 *
 * Get time data of the specific interval identifier with respect to a monitoring type.
 *
 * \param infoidx   [IN]    (key) Measurement type and interval ID of PM interval information.
 *
 * \param data      [OUT]   The time data of the specific PM interval information.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_interval_info_get(
    const vtss_appl_pm_intvl_info_index_t   *const infoidx,
    vtss_appl_pm_interval_info_t            *const data
);

/**
 * \brief Set PM global control actions.
 *
 * \param control [IN]      Global control actions for clearing PM LM/DM/EVC statistics.
 *
 * \return Error code.      VTSS_RC_OK for success operation, otherwise for operation failure.
 */
mesa_rc vtss_appl_pm_control_globals_set(
    const vtss_appl_pm_control_globals_t    *const control
);

#ifdef __cplusplus
}
#endif

#endif  /* _VTSS_APPL_PERFORMANCE_MONITOR_H_ */
