/*

 Vitesse MPLS-TP software.

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

/**
* \file
* \brief Public MPLS-TP API
* \details This header file describes the MPLS-TP control functions and types.
*          The API can be used to configure and monitor MPLS-TP links, tunnel endpoints,
*          LSP cross-connects and Pseudo-Wires.
*          MPLS-TP links are the logical interfaces on which MPLS frames can be sent and received.
*          An MPLS-TP link is associated with a physical port and points out a "next hop" MPLS router in the network.
*          An MPLS-TP tunnel endpoint defines the endpoint of an MPLS-TP tunnel through the network. The tunnel can be
*          attached directly to an MPLS-TP link or attached to another MPLS-TP tunnel endpoint (nested tunels).
*          An MPLS-TP Pseudo-Wire (PW) is a special type of MPLS-TP tunnel that can be attached to an EVC and also attached
*          to an MPLS-TP link or MPLS-TP tunnel. In this way, Ethernet frames from the EVC can be encapsulated/decapsulated in MPLS and forwarded over MPLS-TP.
*          An MPLS-TP LSP cross-connect is used to build an MPLS wire service for forwarding MPLS frames through a device.
*          The MPLS-TP LSP cross-connect has a forward and reverse part which defines the two forwarding directions.
*          Both of these can be attached to an MPLS-TP link or MPLS-TP tunnel.
*          A typical network setup has an EVC in device A attached to an MPLS-TP PW which may be attached to an MPLS-TP tunnel
*          which may be attached to another MPLS-TP tunnel which is then attached to an MPLS-TP link. The MPLS-TP link connects
*          via the network to a device B which can have an MPLS-TP LSP cross-connect that forwards the MPLS traffic out via an MPLS-TP
*          link or new tunnel. Finally at some point the traffic arrives in a device that terminates the MPLS-TP tunnel(s) and MPLS-TP PW.
*          The PW is attached to an EVC and the resulting Ethernet frame is forwarded according to the EVC setup.
*          MPLS-TP links, tunnel endpoints, LSP cross-connects and Pseudo-Wires can be created and deleted independently,
*          but the operational status of a given instance is not "up" until all layers have been properly configured.
*          This MPLS-TP implementation supports the following RFC's: RFC 5654, RFC 5586,
*          RFC 5921 and RFC 6370.
*/

#ifndef _VTSS_APPL_MPLS_TP_H_
#define _VTSS_APPL_MPLS_TP_H_

#include <mscc/ethernet/switch/api/types.h>
#include <vtss/appl/module_id.h>
#include <vtss/appl/interface.h>
#include <vtss/basics/enum-descriptor.h>

#ifdef __cplusplus
extern "C" {
#endif

#define VTSS_APPL_MPLSTP_TUNNEL_NAME_MAX     32          /**< Max length of MPLS-TP tunnel name. */

/** \brief Define for no MPLS label value. */
#define VTSS_APPL_MPLSTP_LABEL_NONE          0xffffffff  /**< No MPLS label value. */

/** \brief Define for HQOS ID value none. */
#define VTSS_APPL_MPLSTP_HQOS_ID_NONE        VTSS_HQOS_ID_NONE

/** \brief MPLS-TP counters. */
typedef mesa_mpls_counters_t vtss_appl_mplstp_counters_t;

/** \brief Frame VLAN tagging. */
typedef enum {
    VTSS_APPL_MPLSTP_TAGTYPE_UNTAGGED = 0,     /**< Frame is untagged. */
    VTSS_APPL_MPLSTP_TAGTYPE_CTAGGED  = 1,     /**< Frame is C-tagged. */
    VTSS_APPL_MPLSTP_TAGTYPE_STAGGED  = 2      /**< Frame is S-tagged. */
} vtss_appl_mplstp_tagtype_t;

/** \brief VLAN tagging type texts. */
extern const vtss_enum_descriptor_t vtss_appl_mplstp_tagtype_txt[];

/** \brief DiffServ tunnel modes for TC and TTL. */
typedef enum {
    VTSS_APPL_MPLSTP_TUNNEL_MODE_PIPE,         /**< Pipe mode.                    */
    VTSS_APPL_MPLSTP_TUNNEL_MODE_SHORT_PIPE,   /**< Short Pipe mode.              */
    VTSS_APPL_MPLSTP_TUNNEL_MODE_UNIFORM,      /**< Uniform mode.                 */
    VTSS_APPL_MPLSTP_TUNNEL_MODE_UNDEF         /**< Undefined (use global value). */
} vtss_appl_mplstp_tunnel_mode_t;

/** \brief MPLS-TP DiffServ tunnel mode texts. */
extern const vtss_enum_descriptor_t vtss_appl_mplstp_tunnel_mode_txt[];

/** \brief MPLS-TP entity state. */
typedef enum {
  VTSS_APPL_MPLSTP_STATE_UNCONF,               /**< MPLS-TP entity is not fully configured.                                       */
  VTSS_APPL_MPLSTP_STATE_CONF,                 /**< MPLS-TP entity is sufficiently configured, but has not acquired HW resources. */
  VTSS_APPL_MPLSTP_STATE_UP,                   /**< MPLS-TP entity has acquired HW resources and port is up.                      */
  VTSS_APPL_MPLSTP_STATE_DOWN                  /**< MPLS-TP entity has acquired HW resources, but port is down.                   */
} vtss_appl_mplstp_state_t;

/** \brief MPLS-TP entity state texts. */
extern const vtss_enum_descriptor_t vtss_appl_mplstp_state_txt[];

/** \brief MPLS-TP PW OAM VCCV type. */
typedef enum {
  VTSS_APPL_MPLSTP_OAM_NONE,                   /**< No MPLS-TP OAM.                                                    */
  VTSS_APPL_MPLSTP_OAM_VCCV1,                  /**< MPLS-TP OAM VCCV type 1: Control word / ACH only.                  */
  VTSS_APPL_MPLSTP_OAM_VCCV2,                  /**< MPLS-TP OAM VCCV type 2: Alert label, optional control word / ACH. */
  VTSS_APPL_MPLSTP_OAM_VCCV3,                  /**< MPLS-TP OAM VCCV type 3: TTL=1, optional control word / ACH.       */
  VTSS_APPL_MPLSTP_OAM_VCCV4,                  /**< MPLS-TP OAM VCCV type 3: GAL + ACH.                                */
} vtss_appl_mplstp_oam_vccv_t;

/** \brief MPLS-TP PW OAM VCCV type texts. */
extern const vtss_enum_descriptor_t vtss_appl_mplstp_oam_vccv_txt[];

/**
 * \brief MPLS-TP global parameters structure.
 *
 * This structure is used to configure global MPLS-TP parameters. These values may be
 * overridden by local settings.
 *
 */
typedef struct {
    vtss_appl_mplstp_tunnel_mode_t tunnel_mode;              /**< DiffServ tunnel modes for TC and TTL. */
    uint32_t                       global_id;                /**< Global ID (for OAM).                  */
    uint32_t                       node_id;                  /**< Node ID (for OAM).                    */
    char                           icc_carrier_code[7];      /**< ITU-T Carrier Code (ICC).             */
} vtss_appl_mplstp_global_t;

/**
 * \brief MPLS-TP link interface structure.
 *
 * This structure is used to configure an MPLS-TP link interface.
 *
 */
typedef struct {
    vtss_ifindex_t             port;                   /**< Interface index for physical port.           */
    mesa_mac_t                 tx_mac;                 /**< MAC address of peer (next hop) MPLS-TP node. */
    mesa_mac_t                 rx_mac;                 /**< MAC address of this MPLS-TP interface.       */
    vtss_appl_mplstp_tagtype_t tag_type;               /**< VLAN tag type.                               */
    mesa_vid_t                 vid;                    /**< VLAN ID, if tag_type isn't untagged.         */
    mesa_tagprio_t             pcp;                    /**< VLAN PCP; only relevant if tagged.           */
    mesa_dei_t                 dei;                    /**< VLAN DEI; only relevant if tagged.           */
    uint32_t                   src_node_id;            /**< Source node ID (for OAM).                    */
    uint32_t                   src_global_id;          /**< Source global ID (for OAM).                  */
    uint32_t                   dst_node_id;            /**< Destination node ID (for OAM).               */
    uint32_t                   dst_global_id;          /**< Destination global ID (for OAM).             */
    uint32_t                   dst_if_num;             /**< Destination interface number (for OAM).      */
    mesa_bool_t                src_node_id_valid;      /**< Source node ID valid (for OAM).              */
    mesa_bool_t                src_global_id_valid;    /**< Source global ID valid (for OAM).            */
    mesa_bool_t                dst_global_id_valid;    /**< Destination global ID valid (for OAM).       */
} vtss_appl_mplstp_link_t;

/**
 * \brief MPLS-TP link interface status structure.
 *
 * This structure is used for MPLS-TP link interface status.
 *
 */
typedef struct {
    mesa_bool_t oam_active;     /**< TRUE if OAM is configured AND any HW has been successfully allocated. */
} vtss_appl_mplstp_link_status_t;

/**
 * \brief MPLS-TP tunnel endpoint configuration structure.
 *
 * This structure is used to configure an MPLS-TP tunnel endpoint.
 *
 */
typedef struct {
    char                           name[VTSS_APPL_MPLSTP_TUNNEL_NAME_MAX]; /**< Tunnel name.                                                                        */
    vtss_appl_mplstp_tunnel_mode_t tunnel_mode;                            /**< DiffServ tunnel modes for TC and TTL.                                               */
    uint32_t                       src_node_id;                            /**< Source node ID (for OAM).                                                           */
    uint32_t                       src_global_id;                          /**< Source global ID (for OAM).                                                         */
    uint32_t                       dst_node_id;                            /**< Destination node ID (for OAM).                                                      */
    uint32_t                       dst_global_id;                          /**< Destination global ID (for OAM).                                                    */
    uint32_t                       dst_tunnel_tp_num;                      /**< Destination tunnel ID number (for OAM).                                             */
    uint16_t                       src_tunnel_tp_num;                      /**< Source tunnel ID number (if SPME, for OAM).                                         */
    uint16_t                       src_lsp_num;                            /**< Source LSP number (if SPME, for OAM).                                               */
    uint16_t                       dst_lsp_num;                            /**< Destination LSP number (if SPME, for OAM).                                          */
    mesa_bool_t                    is_spme;                                /**< TRUE if tunnel endpoint is an SPME (OAM).                                           */
    mesa_bool_t                    src_node_id_valid;                      /**< Source node ID valid (for OAM).                                                     */
    mesa_bool_t                    src_global_id_valid;                    /**< Source global ID valid (for OAM).                                                   */
    mesa_bool_t                    dst_global_id_valid;                    /**< Destination global ID valid (for OAM).                                              */
    uint32_t                       in_label;                               /**< Ingress MPLS label value or VTSS_APPL_MPLSTP_LABEL_NONE.                            */
    uint32_t                       out_label;                              /**< Egress MPLS label value or VTSS_APPL_MPLSTP_LABEL_NONE.                             */
    vtss_ifindex_t                 attach_num;                             /**< MPLS-TP link interface or MPLS-TP tunnel endpoint interface that tunnel endpoint
                                                                                is attached to.                                                                     */
    uint8_t                        tc;                                     /**< Traffic class used on egress unless overruled by UNIFORM tunnel mode.               */
    uint8_t                        ttl;                                    /**< Time-to-live used on egress unless overruled by UNIFORM tunnel mode.                */
    uint8_t                        in_cos_map_id;                          /**< Ingress Class of Service Map ID or 0 for no mapping.                                */
    uint8_t                        out_cos_map_id;                         /**< Egress Class of Service Map ID or 0 for no mapping.                                 */
    mesa_bool_t                    is_l_lsp;                               /**< TRUE for L-LSP, FALSE for E-LSP. Note that for L-LSP, the Ingress Class of Service
                                                                                Map only maps the ingress Traffic Class to Drop Precedence. The Class of Service is
                                                                                given by the l_lsp_cos field.                                                       */
    uint8_t                        l_lsp_cos;                              /**< Ingress Class of Service for L-LSP (is_l_lsp == TRUE).                              */
} vtss_appl_mplstp_tunnel_t;

/**
 * \brief MPLS-TP tunnel endpoint status structure.
 *
 * This structure is used for MPLS-TP tunnel endpoint status.
 *
 */
typedef struct {
    vtss_appl_mplstp_state_t state;      /**< MPLS-TP tunnel endpoint state.                                                                                */
    mesa_bool_t              oam_active; /**< TRUE if OAM is configured AND valid given the current label stack AND any HW has been successfully allocated. */
    mesa_bool_t              w_active;   /**< TRUE if working tunnel is active in a protection setup, FALSE if protection tunnel is active. Always TRUE for
                                              non-protection case.                                                                                            */
    uint32_t                 in_label;   /**< Ingress MPLS label value or VTSS_APPL_MPLSTP_LABEL_NONE.                                                      */
    uint32_t                 out_label;  /**< Egress MPLS label value or VTSS_APPL_MPLSTP_LABEL_NONE.                                                       */
} vtss_appl_mplstp_tunnel_status_t;


/**
 * \brief MPLS-TP LSP cross-connect direction specific configuration.
 *
 * This structure holds forward/reverse direction specific configuration values for an MPLS-TP LSP cross-connect.
 *
 */
typedef struct {
        uint32_t       in_label;             /**< Ingress MPLS label value or VTSS_APPL_MPLSTP_LABEL_NONE.          */
        uint32_t       out_label;            /**< Egress MPLS label value or VTSS_APPL_MPLSTP_LABEL_NONE.           */
        vtss_ifindex_t attach_num;           /**< Egress MPLS-TP link interface or MPLS-TP tunnel endpoint number
                                                  for forward or reverse part of LSP cross-connect.                 */
        uint8_t        in_cos_map_id;        /**< Ingress Class of Service Map ID or 0 for no mapping for forward
                                                  or reverse part of LSP cross-connect.                             */
        uint8_t        out_cos_map_id;       /**< Egress Class of Service Map ID or 0 for no mapping for forward
                                                  or reverse part of LSP cross-connect.                               */
        mesa_bool_t    is_l_lsp;             /**< TRUE for L-LSP, FALSE for E-LSP. Note that for L-LSP, the
                                                  Ingress Class of Service Map only maps the ingress Traffic Class
                                                  to Drop Precedence. The Class of Service is given by the
                                                  l_lsp_cos field.                                                  */
        uint8_t        l_lsp_cos;            /**< Ingress Class of Service for L-LSP (is_l_lsp == TRUE).            */
        mesa_hqos_id_t hqos_id;              /**< HQoS id for forward or reverse direction or HQOS_ID_NONE for none.*/
} vtss_appl_mplstp_lsp_uni_t;


/**
 * \brief MPLS-TP LSP cross-connect structure.
 *
 * This structure is used to configure an MPLS-TP LSP cross-connect.
 *
 */
typedef struct {
    char                       name[VTSS_APPL_MPLSTP_TUNNEL_NAME_MAX]; /**< Tunnel name.                                                      */
    uint32_t                   src_node_id;                            /**< Source node ID (for OAM).                                         */
    uint32_t                   src_global_id;                          /**< Source global ID (for OAM).                                       */
    uint32_t                   dst_node_id;                            /**< Destination node ID (for OAM).                                    */
    uint32_t                   dst_global_id;                          /**< Destination global ID (for OAM).                                  */
    uint16_t                   src_tunnel_tp_num;                      /**< Source tunnel ID number (for OAM).                                */
    uint16_t                   dst_tunnel_tp_num;                      /**< Destination tunnel ID number (for OAM).                           */
    uint16_t                   src_lsp_num;                            /**< Source LSP number (for OAM).                                      */
    uint16_t                   dst_lsp_num;                            /**< Destination LSP number (for OAM).                                 */
    mesa_bool_t                src_node_id_valid;                      /**< Source node ID valid (for OAM).                                   */
    mesa_bool_t                src_global_id_valid;                    /**< Source global ID valid (for OAM).                                 */
    mesa_bool_t                dst_global_id_valid;                    /**< Destination global ID valid (for OAM).                            */
    vtss_appl_mplstp_lsp_uni_t forward_lsp;                            /**< Forward direction specific values.                                */
    vtss_appl_mplstp_lsp_uni_t reverse_lsp;                            /**< Reverse direction specific values.                                */
} vtss_appl_mplstp_lsp_t;

/**
 * \brief MPLS-TP LSP cross-connect status structure.
 *
 * This structure is used for MPLS-TP LSP cross-connect status.
 *
 */
typedef struct {
    vtss_appl_mplstp_state_t state;      /**< MPLS-TP LSP cross-connect state. */
    mesa_bool_t              oam_active; /**< TRUE if OAM is configured AND valid given the current label stack AND any HW has been successfully allocated. */
} vtss_appl_mplstp_lsp_status_t;

/**
 * \brief MPLS-TP Pseudo-Wire (PW) structure.
 *
 * This structure is used to configure an MPLS-TP PW.
 *
 */
typedef struct {
    uint32_t                       in_label;                 /**< PW local/ingress MPLS label value or VTSS_APPL_MPLSTP_LABEL_NONE.     */
    uint32_t                       out_label;                /**< PW remote/egress MPLS label value or VTSS_APPL_MPLSTP_LABEL_NONE.     */
    uint32_t                       cw;                       /**< PW control word value to use on egress.                               */
    vtss_appl_mplstp_tunnel_mode_t tunnel_mode;              /**< DiffServ tunnel modes for TC and TTL.                                 */
    uint8_t                        tc;                       /**< Traffic class used on egress unless overruled by UNIFORM tunnel mode. */
    uint8_t                        ttl;                      /**< Time-to-live used on egress unless overruled by UNIFORM tunnel mode.  */
    uint8_t                        in_cos_map_id;            /**< Ingress Class of Service Map ID or 0 for no mapping.                  */
    uint8_t                        out_cos_map_id;           /**< Egress Class of Service Map ID or 0 for no mapping.                   */
    mesa_bool_t                    use_cw;                   /**< TRUE: Use control word on egress and ingress, FALSE: Do not use.      */
    mesa_bool_t                    is_l_lsp;                 /**< TRUE for L-LSP, FALSE for E-LSP. Note that for L-LSP, the
                                                                  Ingress Class of Service Map only maps the ingress Traffic Class
                                                                  to Drop Precedence. The Class of Service is given by the l_lsp_cos
                                                                  field.                                                                  */
    uint8_t                        l_lsp_cos;                /**< Ingress Class of Service for L-LSP (is_l_lsp == TRUE).                */
    vtss_ifindex_t                 attach_num;               /**< MPLS-TP link interface or MPLS-TP tunnel endpoint interface that PW
                                                                  is attached to.                                                         */
    vtss_ifindex_t                 stitch_pw_num;            /**< MPLS-TP PW interface to stitch this PW with, 0 for no stitch.         */
    vtss_appl_mplstp_oam_vccv_t    vccv_type;                /**< MPLS-TP PW OAM VCCV type.                                             */
    mesa_hqos_id_t                 hqos_id;                  /**< HQoS id or HQOS_ID_NONE for none.                                     */
    uint32_t                       src_node_id;              /**< Source node ID (for OAM).                                             */
    uint32_t                       src_global_id;            /**< Source global ID (for OAM).                                           */
    uint32_t                       src_ac_id;                /**< Source AC_ID (for OAM).                                               */
    uint32_t                       dst_node_id;              /**< Destination node ID (for OAM).                                        */
    uint32_t                       dst_global_id;            /**< Destination global ID (for OAM).                                      */
    uint32_t                       dst_ac_id;                /**< Destination AC_ID (for OAM).                                          */
    uint8_t                        src_agi_value[8];         /**< Source AGI Value (for OAM).                                           */
    uint8_t                        dst_agi_value[8];         /**< Destination AGI Value (for OAM).                                      */
    uint8_t                        src_agi_type;             /**< Source AGI Type (for OAM).                                            */
    uint8_t                        src_agi_length;           /**< Source AGI Length 0-7 (for OAM).                                      */
    uint8_t                        dst_agi_type;             /**< Destination AGI Type (for OAM).                                       */
    uint8_t                        dst_agi_length;           /**< Destination AGI Length 0-7 (for OAM).                                 */
    mesa_bool_t                    src_node_id_valid;        /**< Source node ID valid (for OAM).                                       */
    mesa_bool_t                    src_global_id_valid;      /**< Source global ID valid (for OAM).                                     */
    mesa_bool_t                    dst_global_id_valid;      /**< Destination global ID valid (for OAM).                                */
} vtss_appl_mplstp_pw_t;

/**
 * \brief MPLS-TP PW status structure.
 *
 * This structure is used for MPLS-TP PW status.
 *
 */
typedef struct {
    vtss_appl_mplstp_state_t state;      /**< MPLS-TP PW state. */
    mesa_bool_t              oam_active; /**< TRUE if OAM is configured AND valid given the current label stack AND any HW has been successfully allocated. */
} vtss_appl_mplstp_pw_status_t;

/**
 * \brief MPLS-TP Class of Service Map structure.
 *
 * This structure is used to configure an MPLS-TP Class of Service Map table entry.
 *
 */
typedef struct {
    uint8_t in_tc_to_cos[8];                        /**< Ingress Traffic Class to Class of Service mapping.                    */
    uint8_t in_tc_to_dp[8];                         /**< Ingress Traffic Class to Drop Precedence mapping.                     */
    uint8_t out_cos_dp_to_tc[2][8];                 /**< Egress Class of Service and Drop Precedence to Traffic Class mapping. */
} vtss_appl_mplstp_cosmap_t;


/**
 * \brief MPLS-TP control data structure.
 *
 * This structure is used to do control of MPLS-TP tunnels, PWs and LSP cross-connects.
 */
typedef struct {
    mesa_bool_t    clear;         /**< clear statistics. */
} vtss_appl_mplstp_control_t;


/**
 * \brief Instance capability structure.
 *
 * This structure is used to contain the instance capability.
 *
 */
typedef struct {
    uint32_t     max_links;              /**< Max number of MPLS-TP link interfaces.       */
    uint32_t     max_tunnels;            /**< Max number of MPLS-TP tunnel endpoints.      */
    uint32_t     max_pw;                 /**< Max number of MPLS-TP PW endpoints.          */
    uint32_t     max_lsp_xc;             /**< Max number of MPLS-TP LSP cross-connects.    */
    uint32_t     max_cos_map;            /**< Max number of MPLS-TP Class of Service Maps. */
    uint32_t     max_tunnel_name_len;    /**< Max length of MPLS-TP tunnel name.           */
} vtss_appl_mplstp_capabilities_t;

/**
 * Definition of error return codes.
 * See also vtss_appl_mplstp_error_txt.
 */
enum {
    VTSS_APPL_MPLSTP_RC_OK = MESA_RC_OK,       /**< All went well.                            */
    VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER = MODULE_ERROR_START(VTSS_MODULE_ID_MPLS_TP), /**< Invalid parameter.                                               */
    VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER_ATTACH_NUM_NONE,                              /**< Invalid parameter - Attach inteface number cannot be equal None. */
    VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER_VCCV_TYPE_NONE,                               /**< Invalid parameter - PW VCCV Type cannot be equal None.           */
    VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER_IN_LABEL_DISABLED,                            /**< Invalid parameter - Ingress label cannot be disabled.            */
    VTSS_APPL_MPLSTP_RC_ALLOC_FAIL,            /**< Allocation failure.                       */
    VTSS_APPL_MPLSTP_RC_LOW_LEVEL_FAIL,        /**< Lower level API failure.                  */
    VTSS_APPL_MPLSTP_RC_INVALID_TPLINKNUM,     /**< Invalid MPLS-TP link interface number.    */
    VTSS_APPL_MPLSTP_RC_INVALID_TUNNELTPNUM,   /**< Invalid MPLS-TP tunnel endpoint number.   */
    VTSS_APPL_MPLSTP_RC_INVALID_LSPNUM,        /**< Invalid MPLS-TP LSP cross-connect number. */
    VTSS_APPL_MPLSTP_RC_INVALID_PWNUM,         /**< Invalid MPLS-TP PW number.                */
    VTSS_APPL_MPLSTP_RC_IFC_UNAVAILABLE,       /**< Port is not available for MPLS-TP links.  */
    VTSS_APPL_MPLSTP_RC_NOT_FOUND,             /**< Not found.                                */
    VTSS_APPL_MPLSTP_RC_LABEL_STACK_EXCEEDED,  /**< Max. label stack depth exceeded.          */
    VTSS_APPL_MPLSTP_RC_INVALID_MEP_INST,      /**< Other OAM/MEP instance already attached.  */
};


/**
 * \brief Convert error return code to text.
 *
 * This function returns a text string for an error return code.
 *
 * \param error [IN] Error return code.
 *
 * \return  Text string with error message.
 */
const char *vtss_appl_mplstp_error_txt(mesa_rc error);

/**
 * \brief Set MPLS-TP global configuration parameters.
 *
 * This function sets MPLS-TP global configuration parameters.
 *
 * \param conf [IN] Configuration.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mplstp_global_set(const vtss_appl_mplstp_global_t *const conf);

/**
 * \brief Get MPLS-TP global configuration parameters.
 *
 * This function gets MPLS-TP global configuration parameters.
 *
 * \param conf [IN] Configuration.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 */
mesa_rc vtss_appl_mplstp_global_get(vtss_appl_mplstp_global_t *const conf);

/**
 * \brief Add or update MPLS-TP link interface.
 *
 * This function adds a new MPLS-TP link interface or updates the parameters of an
 * existing MPLS-TP link interface.
 *
 * \param tp_link_num [IN] MPLS-TP link interface index.
 * \param conf [IN]        Configuration.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_ALLOC_FAIL
 *          VTSS_APPL_MPLSTP_RC_LOW_LEVEL_FAIL
 *          VTSS_APPL_MPLSTP_RC_PORT_UNAVAILABLE
 */
mesa_rc vtss_appl_mplstp_link_add_update(vtss_ifindex_t                       tp_link_num,
                                         const vtss_appl_mplstp_link_t *const conf);

/**
 * \brief Delete MPLS-TP link interface.
 *
 * This function deletes an existing MPLS-TP link interface.
 *
 * \param tp_link_num [IN] MPLS-TP link interface.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_INVALID_TPLINKNUM
 *          VTSS_APPL_MPLSTP_RC_LOW_LEVEL_FAIL
 */
mesa_rc vtss_appl_mplstp_link_del(vtss_ifindex_t tp_link_num);

/**
 * \brief Get MPLS-TP link interface configuration.
 *
 * This function gets MPLS-TP link interface configuration parameters that
 * have previously been set.
 *
 * \param tp_link_num [IN] MPLS-TP link interface.
 * \param conf [OUT]       Configuration.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_INVALID_TPLINKNUM
 */
mesa_rc vtss_appl_mplstp_link_conf_get(vtss_ifindex_t                 tp_link_num,
                                       vtss_appl_mplstp_link_t *const conf);

/**
 * \brief Get MPLS-TP link interface status.
 *
 * This function gets MPLS-TP link interface status.
 *
 * \param tp_link_num [IN] MPLS-TP link interface.
 * \param status [OUT]     Status.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_INVALID_TPLINKNUM
 *          VTSS_APPL_MPLSTP_RC_LOW_LEVEL_FAIL
 */
mesa_rc vtss_appl_mplstp_link_status_get(vtss_ifindex_t                        tp_link_num,
                                         vtss_appl_mplstp_link_status_t *const status);

/**
 * \brief Get counters for MPLS-TP link interface.
 *
 * This function gets counters for an MPLS-TP link interface.
 *
 * \param tp_link_num [IN] MPLS-TP link interface.
 * \param counters [OUT]   Counters.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_TPLINKNUM
 */
mesa_rc vtss_appl_mplstp_link_counters_get(vtss_ifindex_t                     tp_link_num,
                                           vtss_appl_mplstp_counters_t *const counters);

/**
 * \brief Add or update MPLS-TP tunnel endpoint.
 *
 * This function adds a new MPLS-TP tunnel endpoint or updates the parameters of an
 * existing MPLS-TP tunnel endpoint.
 *
 * \param tunnel_tp_num [IN] MPLS-TP tunnel endpoint interface index.
 * \param conf [IN]          Configuration.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_ALLOC_FAIL
 *          VTSS_APPL_MPLSTP_RC_LOW_LEVEL_FAIL
 */
mesa_rc vtss_appl_mplstp_tunnel_add_update(vtss_ifindex_t                         tunnel_tp_num,
                                           const vtss_appl_mplstp_tunnel_t *const conf);

/**
 * \brief Delete MPLS-TP tunnel endpoint.
 *
 * This function deletes an existing MPLS-TP tunnel endpoint interface index.
 *
 * \param tunnel_tp_num [IN] MPLS-TP tunnel endpoint number.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_INVALID_TUNNELTPNUM
 *          VTSS_APPL_MPLSTP_RC_LOW_LEVEL_FAIL
 */
mesa_rc vtss_appl_mplstp_tunnel_del(vtss_ifindex_t tunnel_tp_num);

/**
 * \brief Get MPLS-TP tunnel endpoint configuration.
 *
 * This function gets MPLS-TP tunnel endpoint configuration parameters that
 * have previously been set.
 *
 * \param tunnel_tp_num [IN] MPLS-TP tunnel endpoint interface index.
 * \param conf [OUT]         Configuration.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_INVALID_TUNNELTPNUM
 */
mesa_rc vtss_appl_mplstp_tunnel_conf_get(vtss_ifindex_t                   tunnel_tp_num,
                                         vtss_appl_mplstp_tunnel_t *const conf);

/**
 * \brief Get MPLS-TP tunnel endpoint status.
 *
 * This function gets MPLS-TP tunnel endpoint status.
 *
 * \param tunnel_tp_num [IN] MPLS-TP tunnel endpoint number interface index.
 * \param status [OUT]       Status.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_INVALID_TUNNELTPNUM
 *          VTSS_APPL_MPLSTP_RC_LOW_LEVEL_FAIL
 */
mesa_rc vtss_appl_mplstp_tunnel_status_get(vtss_ifindex_t                          tunnel_tp_num,
                                           vtss_appl_mplstp_tunnel_status_t *const status);

/**
 * \brief Get counters for MPLS-TP tunnel endpoint.
 *
 * This function gets counters for an MPLS-TP tunnel endpoint.
 *
 * \param tunnel_tp_num [IN] MPLS-TP tunnel endpoint interface index.
 * \param counters [OUT]     Counters.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_TUNNELTPNUM
 */
mesa_rc vtss_appl_mplstp_tunnel_counters_get(vtss_ifindex_t                     tunnel_tp_num,
                                             vtss_appl_mplstp_counters_t *const counters);

/**
 * \brief Add or update MPLS-TP LSP cross-connect.
 *
 * This function adds a new MPLS-TP LSP cross-connect or updates the parameters of an
 * existing MPLS-TP LSP cross-connect.
 *
 * \param lsp_num [IN] MPLS-TP LSP cross-connect number.
 * \param conf [IN]    Configuration.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_ALLOC_FAIL
 *          VTSS_APPL_MPLSTP_RC_LOW_LEVEL_FAIL
 */
mesa_rc vtss_appl_mplstp_lsp_add_update(uint32_t lsp_num,
                                        const vtss_appl_mplstp_lsp_t *const conf);

/**
 * \brief Delete MPLS-TP LSP cross-connect.
 *
 * This function deletes an existing MPLS-TP LSP cross-connect.
 *
 * \param lsp_num [IN] MPLS-TP LSP cross-connect number.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_INVALID_LSPNUM
 *          VTSS_APPL_MPLSTP_RC_LOW_LEVEL_FAIL
 */
mesa_rc vtss_appl_mplstp_lsp_del(uint32_t lsp_num);

/**
 * \brief Get MPLS-TP LSP cross-connect configuration.
 *
 * This function gets MPLS-TP LSP cross-connect configuration parameters that
 * have previously been set.
 *
 * \param lsp_num [IN] MPLS-TP LSP cross-connect number.
 * \param conf [OUT]   Configuration.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_INVALID_LSPNUM
 */
mesa_rc vtss_appl_mplstp_lsp_conf_get(uint32_t lsp_num,
                                      vtss_appl_mplstp_lsp_t *const conf);

/**
 * \brief Get MPLS-TP LSP cross-connect status.
 *
 * This function gets MPLS-TP LSP cross-connect status.
 *
 * \param lsp_num [IN] MPLS-TP LSP cross-connect number.
 * \param status [OUT] Status.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_INVALID_LSPNUM
 *          VTSS_APPL_MPLSTP_RC_LOW_LEVEL_FAIL
 */
mesa_rc vtss_appl_mplstp_lsp_status_get(uint32_t lsp_num,
                                        vtss_appl_mplstp_lsp_status_t *const status);

/**
 * \brief Get counters for MPLS-TP LSP cross-connect.
 *
 * This function gets counters for an MPLS-TP LSP cross-connect.
 *
 * \param lsp_num [IN]       MPLS-TP LSP cross-connect number.
 * \param fwd_counters [OUT] Counters for forward direction.
 * \param rev_counters [OUT] Counters for reverse direction.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_LSPNUM
 */
mesa_rc vtss_appl_mplstp_lsp_counters_get(uint32_t lsp_num,
                                          vtss_appl_mplstp_counters_t *const fwd_counters,
                                          vtss_appl_mplstp_counters_t *const rev_counters);

/**
 * \brief Add or update MPLS-TP PW.
 *
 * This function adds a new MPLS-TP PW or updates the parameters of an
 * existing MPLS-TP PW.
 *
 * \param pw_num [IN] MPLS-TP PW interface index.
 * \param conf [IN]   Configuration.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_ALLOC_FAIL
 *          VTSS_APPL_MPLSTP_RC_LOW_LEVEL_FAIL
 */
mesa_rc vtss_appl_mplstp_pw_add_update(vtss_ifindex_t                     pw_num,
                                       const vtss_appl_mplstp_pw_t *const conf);

/**
 * \brief Delete MPLS-TP PW.
 *
 * This function deletes an existing MPLS-TP PW.
 *
 * \param pw_num [IN] MPLS-TP PW interface index.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_INVALID_PWNUM
 *          VTSS_APPL_MPLSTP_RC_LOW_LEVEL_FAIL
 */
mesa_rc vtss_appl_mplstp_pw_del(vtss_ifindex_t pw_num);

/**
 * \brief Get MPLS-TP PW configuration.
 *
 * This function gets MPLS-TP PW configuration parameters that
 * have previously been set.
 *
 * \param pw_num [IN] MPLS-TP PW interface index.
 * \param conf [OUT]  Configuration.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_INVALID_PWNUM
 */
mesa_rc vtss_appl_mplstp_pw_conf_get(vtss_ifindex_t               pw_num,
                                     vtss_appl_mplstp_pw_t *const conf);

/**
 * \brief Get MPLS-TP PW status.
 *
 * This function gets MPLS-TP PW status.
 *
 * \param pw_num [IN]  MPLS-TP PW interface index.
 * \param status [OUT] Status.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_INVALID_PWNUM
 *          VTSS_APPL_MPLSTP_RC_LOW_LEVEL_FAIL
 */
mesa_rc vtss_appl_mplstp_pw_status_get(vtss_ifindex_t                      pw_num,
                                       vtss_appl_mplstp_pw_status_t *const status);

/**
 * \brief Get counters for MPLS-TP PW.
 *
 * This function gets counters for an MPLS-TP PW.
 *
 * \param pw_num [IN]    MPLS-TP PW interface index.
 * \param counters [OUT] Counters.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PWNUM
 */
mesa_rc vtss_appl_mplstp_pw_counters_get(vtss_ifindex_t                     pw_num,
                                         vtss_appl_mplstp_counters_t *const counters);

/**
 * \brief Add or update MPLS-TP Class of Service Map table entry.
 *
 * This function adds a new MPLS-TP COS Map or updates the parameters of an
 * existing MPLS-TP COS Map.
 *
 * \param cos_map_id [IN] MPLS-TP Class of Service Map ID.
 * \param conf [IN]       Configuration.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_LOW_LEVEL_FAIL
 */
mesa_rc vtss_appl_mplstp_cosmap_add_update(uint32_t cos_map_id,
                                           const vtss_appl_mplstp_cosmap_t *const conf);

/**
 * \brief Delete MPLS-TP Class of Service Map table entry.
 *
 * This function deletes an existing MPLS-TP Class of Service Map table entry.
 *
 * \param cos_map_id [IN] MPLS-TP Class of Service Map ID.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MPLSTP_RC_LOW_LEVEL_FAIL
 */
mesa_rc vtss_appl_mplstp_cosmap_del(uint32_t cos_map_id);

/**
 * \brief Get MPLS-TP Class of Service Map table entry.
 *
 * This function gets MPLS-TP COS Map parameters that
 * have previously been set.
 *
 * \param cos_map_id [IN] MPLS-TP Class of Service Map ID.
 * \param conf [OUT]      Configuration.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mplstp_cosmap_conf_get(uint32_t cos_map_id,
                                         vtss_appl_mplstp_cosmap_t *const conf);

/**
 * \brief Get MPLS-TP capabilities status.
 *
 * This function gets MPLS-TP capabilities status.
 *
 * \param cap [OUT] Capabilities
 *
 * \return  VTSS_RC_OK
 */
mesa_rc vtss_appl_mplstp_capabilities_get(vtss_appl_mplstp_capabilities_t *const cap);

/**
 * \brief Iterate link interface number.
 *
 * This function iterates MPLS-TP link interface number.
 *
 * \param  in  [IN] Current link interface number.
 * \param out [OUT] Next link interface number
 *
 * \return  VTSS_RC_OK
 *          VTSS_RC_ERROR
 */
mesa_rc vtss_appl_mplstp_link_itr(const vtss_ifindex_t *const in, vtss_ifindex_t *const out);

/**
 * \brief Initialize link configuration structure with default values.
 *
 * This function initializes link configuration structure with default values.
 *
 * \param key [OUT] Default key value.
 * \param out [OUT] Default link values.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 */
mesa_rc vtss_appl_mplstp_link_conf_default(vtss_ifindex_t *key, vtss_appl_mplstp_link_t *const out);

/**
 * \brief Iterate tunnel endpoint number.
 *
 * This function iterates MPLS-TP tunnel endpoint number.
 *
 * \param  in  [IN] Current tunnel endpoint interface index.
 * \param out [OUT] Next tunnel endpoint interface index.
 *
 * \return  VTSS_RC_OK
 *          VTSS_RC_ERROR
 */
mesa_rc vtss_appl_mplstp_tunnel_itr(const vtss_ifindex_t *const in, vtss_ifindex_t *const out);

/**
 * \brief Initialize tunnel configuration structure with default values.
 *
 * This function initialize tunnel configuration structure with default values
 *
 * \param key [OUT] Default key value.
 * \param out [OUT] Default tunnel values.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 */
mesa_rc vtss_appl_mplstp_tunnel_conf_default(vtss_ifindex_t *key, vtss_appl_mplstp_tunnel_t *const out);

/**
 * \brief Iterate LSP cross-connect number.
 *
 * This function iterates LSP cross-connect number.
 *
 * \param in  [IN]  Current LSP cross-connect number.
 * \param out [OUT] Next LSP cross-connect number.
 *
 * \return  VTSS_RC_OK
 *          VTSS_RC_ERROR
 */
mesa_rc vtss_appl_mplstp_lsp_itr(const uint32_t *const in, uint32_t *const out);

/**
 * \brief Initialize LSP cross-connect configuration structure with default values.
 *
 * This function initialize LSP cross-connect configuration structure with default values
 *
 * \param key [OUT] Default key value.
 * \param out [OUT] Default LSP cross-connect values.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 */
mesa_rc vtss_appl_mplstp_lsp_conf_default(uint32_t *key, vtss_appl_mplstp_lsp_t *const out);

/**
 * \brief Iterate PW id.
 *
 * This function iterates MPLS-TP PW interface index.
 *
 * \param in  [IN]  Current MPLS-TP PW interface index.
 * \param out [OUT] Next MPLS-TP PW interface index.
 *
 * \return  VTSS_RC_OK
 *          VTSS_RC_ERROR
 */
mesa_rc vtss_appl_mplstp_pw_itr(const vtss_ifindex_t *const in, vtss_ifindex_t *const out);

/**
 * \brief Initialize PW configuration structure with default values.
 *
 * This function initialize PW configuration structure with default values
 *
 * \param key [OUT] Default key value.
 * \param out [OUT] Default PW values.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 */
mesa_rc vtss_appl_mplstp_pw_conf_default(vtss_ifindex_t *key, vtss_appl_mplstp_pw_t *const out);

/**
 * \brief Iterate MPLS-TP COS Map ID.
 *
 * This function iterates MPLS-TP COS Map ID.
 *
 * \param in  [IN]  Current COS Map ID.
 * \param out [OUT] Next COS Map Id.
 *
 * \return  VTSS_RC_OK
 *          VTSS_RC_ERROR
 */
mesa_rc vtss_appl_mplstp_cosmap_itr(const uint32_t *const in, uint32_t *const out);

/**
 * \brief Initialise MPLS-TP COS Map configuration structure with default values.
 *
 * This function initializes the MPLS-TP COS Map configuration structure with default values
 *
 * \param key [OUT] Default key value.
 * \param out [OUT] Default COS Map values.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 */
mesa_rc vtss_appl_mplstp_cosmap_conf_default(uint32_t *key, vtss_appl_mplstp_cosmap_t *const out);

/**
 * \brief MPLS-TP control GET for tunnel and PW.
 *
 * \param ifindex [IN]  MPLS-TP tunnel or PW interface index.
 * \param control [OUT] Control.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mplstp_control_get(const vtss_ifindex_t       ifindex,
                                     vtss_appl_mplstp_control_t *const control);

/**
 * \brief MPLS-TP control SET for tunnel and PW.
 *
 * \param ifindex [IN] MPLS-TP tunnel or PW interface index.
 * \param control [IN] Control.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mplstp_control_set(const vtss_ifindex_t             ifindex,
                                     const vtss_appl_mplstp_control_t *const control);

/**
 * \brief Iterate MPLS-TP control for tunnel and PW.
 *
 * This function iterates MPLS-TP control interfaces.
 *
 * \param in  [IN]  Current MPLS-TP control interface index.
 * \param out [OUT] Next MPLS-TP control interface index.
 *
 * \return  VTSS_RC_OK
 *          VTSS_RC_ERROR
 */
mesa_rc vtss_appl_mplstp_control_itr(const vtss_ifindex_t *const in, vtss_ifindex_t *const out);

/**
 * \brief MPLS-TP control GET for LSP cross-connect.
 *
 * \param lsp_num [IN]  MPLS-TP LSP cross-connect number.
 * \param control [OUT] Control.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mplstp_lsp_control_get(const uint32_t lsp_num,
                                         vtss_appl_mplstp_control_t *const control);

/**
 * \brief MPLS-TP control SET for LSP cross-connect.
 *
 * \param lsp_num [IN] MPLS-TP LSP cross-connect number.
 * \param control [IN] Control.
 *
 * \return  VTSS_APPL_MPLSTP_RC_OK
 *          VTSS_APPL_MPLSTP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mplstp_lsp_control_set(const uint32_t lsp_num,
                                         const vtss_appl_mplstp_control_t *const control);


#ifdef __cplusplus
}
#endif
#endif /* _VTSS_APPL_MPLS_TP_H_ */
