/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

/**
 * \file
 * \brief Public OSPF API
 * \details This header file describes OSPF control functions and types.
 */

#ifndef _VTSS_APPL_OSPF_H_
#define _VTSS_APPL_OSPF_H_

#include <mscc/ethernet/switch/api/types.h>  // For type declarations
#include <vtss/appl/module_id.h>             // For MODULE_ERROR_START()
#include <vtss/appl/interface.h>             // For vtss_ifindex_t
#include <vtss/basics/map.hxx>
#include <vtss/basics/vector.hxx>

//----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//** OSPF type declaration
//----------------------------------------------------------------------------

/** \brief The data type of OSPF instance ID. */
typedef uint32_t vtss_appl_ospf_id_t;

/** \brief The data type of OSPF area ID. */
typedef mesa_ipv4_t vtss_appl_ospf_area_id_t;

/** \brief The data type of OSPF router ID. */
typedef mesa_ipv4_t vtss_appl_ospf_router_id_t;

/** \brief The data type of OSPF priority value. */
typedef uint32_t vtss_appl_ospf_priority_t;

/** \brief The data type of OSPF cost value. */
typedef uint32_t vtss_appl_ospf_cost_t;

/** \brief The data type of OSPF metric value. */
typedef uint32_t vtss_appl_ospf_metric_t;

/** \brief The message digest key ID. */
typedef uint8_t vtss_appl_ospf_md_key_id_t;

//----------------------------------------------------------------------------
//** OSPF module error codes
//----------------------------------------------------------------------------

/** \brief FRR error return codes (mesa_rc) */
enum {
    /** Generic error code */
    VTSS_APPL_FRR_ERROR_GEN = MODULE_ERROR_START(VTSS_MODULE_ID_FRR),

    /** Illegal parameter */
    VTSS_APPL_FRR_ERROR_PARM,

    /** The operation is not supported */
    VTSS_APPL_FRR_ERROR_NOT_SUPPORT,

    /** Internal framework access error */
    VTSS_APPL_FRR_ERROR_INTERNAL_ACCESS,

    /** Entry non-existing */
    VTSS_APPL_FRR_ERROR_ENTRY_NOT_FOUND,

    /** Entry already existing */
    VTSS_APPL_FRR_ERROR_ENTRY_ALREADY_EXISTING,

    /** Invalid router ID */
    VTSS_APPL_FRR_OSPF_ERROR_INVALID_ROUTER_ID,

    /** Address range overlap */
    VTSS_APPL_FRR_ERROR_ADDR_RANGE_OVERLAP,

    /** The OSPF router ID change doesn't take effect */
    VTSS_APPL_FRR_OSPF_ERROR_ROUTER_ID_CHANGE_NOT_TAKE_EFFECT,

    /** The OSPF area ID change doesn't take effect */
    VTSS_APPL_FRR_OSPF_ERROR_AREA_ID_CHANGE_NOT_TAKE_EFFECT,

    /** The VLAN interface does not exist */
    VTSS_APPL_FRR_OSPF_ERROR_VLAN_INTF_NOT_EXIST,

    /** Backbone can not be configured as stub area */
    VTSS_APPL_FRR_OSPF_ERROR_STUB_AREA_NOT_FOR_BACKBONE,

    /* This area contains virtual link, can not be configured as stub area */
    VTSS_APPL_FRR_OSPF_ERROR_STUB_AREA_NOT_FOR_VIRTUAL_LINK,

    /** The password/key is too long */
    VTSS_APPL_FRR_OSPF_ERROR_AUTH_KEY_TOO_LONG,

    /** The password/key is invalid */
    VTSS_APPL_FRR_OSPF_ERROR_AUTH_KEY_INVALID,

    /** Backbone area can not be configured as virtual link */
    VTSS_APPL_FRR_OSPF_ERROR_VIRTUAL_LINK_NOT_ON_BACKBONE,

    /** Virtual link can not be configured in stub area */
    VTSS_APPL_FRR_OSPF_ERROR_VIRTUAL_LINK_NOT_ON_STUB,

    /** Area range not-advertise and cost can not be set at the same time */
    VTSS_APPL_FRR_OSPF_ERROR_AREA_RANGE_COST_CONFLICT,

    /** Area range network address can't be default */
    VTSS_APPL_FRR_OSPF_ERROR_AREA_RANGE_NETWORK_DEFAULT
};

//----------------------------------------------------------------------------
//** OSPF capabilities
//----------------------------------------------------------------------------

/**
 * \brief OSPF capabilities
 */
typedef struct {
    /** Maximum instance ID */
    uint32_t     instance_id_max;
} vtss_appl_ospf_capabilities_t;

/**
 * \brief Get OSPF capabilities to see what supported or not
 * \param cap [OUT] OSPF capabilities
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_capabilities_get(vtss_appl_ospf_capabilities_t *const cap);

//----------------------------------------------------------------------------
//** OSPF instance configuration
//----------------------------------------------------------------------------

#define VTSS_APPL_OSPF_INSTANCE_ID_MAX      (1) /**< Maximum ID of OSPF instance. */
#define VTSS_APPL_OSPF_INSTANCE_ID_START    (1) /**< Valid starting ID of OSPF instance. */

/**
 * \brief Add the OSPF instance.
 * \param id [IN] OSPF instance ID.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_add(const vtss_appl_ospf_id_t id);

/**
 * \brief Delete the OSPF instance.
 * \param id [IN] OSPF instance ID.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_del(const vtss_appl_ospf_id_t id);

/**
 * \brief Get the OSPF instance which the OSPF routing process is enabled.
 * \param id [IN] OSPF instance ID.
 * \return Error code.  VTSS_RC_OK means that OSPF routing process is enabled
 *                      on the instance ID.
 *                      VTSS_RC_ERROR means that the instance ID is not created
 *                      and OSPF routing process is disabled.
 */
mesa_rc vtss_appl_ospf_get(const vtss_appl_ospf_id_t id);

/**
 * \brief Iterate through all OSPF instances.
 * \param current_id [IN]   Pointer to the current instance ID. Use null pointer
 *                          to get the first instance ID.
 * \param next_id    [OUT]  Pointer to the next instance ID
 * \return Error code.      VTSS_RC_OK means that the next instance ID is valid
 *                          and the value is saved in 'out' parameter.
 *                          VTSS_RC_ERROR means that the next instance ID is
 *                          non-existing.
 */
mesa_rc vtss_appl_ospf_inst_itr(
        const vtss_appl_ospf_id_t   *const current_id,
        vtss_appl_ospf_id_t         *const next_id);

/**
 * \brief OSPF control global options.
 */
typedef struct {
    /** Reload OSPF process */
    mesa_bool_t reload_process;
} vtss_appl_ospf_control_globals_t;

/**
 * \brief Set OSPF control of global options.
 * \param control [in] Pointer to the control global options.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_control_globals(
    const vtss_appl_ospf_control_globals_t *const control);

//----------------------------------------------------------------------------
//** OSPF router configuration/status
//----------------------------------------------------------------------------
/** The maximum metric value. (0x00FFFFFE) */
#define VTSS_APPL_OSPF_METRIC_MAX   (16777214)

/** \brief The data structure for the OSPF router ID. */
typedef struct {
     /** Indicate the 'ospf_router_id' argument is a specific configured value
      * or not. */
    mesa_bool_t is_specific_id;

    /** The OSPF router ID. The value is used only when 'is_specific_id'
     * argument is true. */
    vtss_appl_ospf_router_id_t id;
} vtss_appl_ospf_router_id_conf_t;

/** \brief The OSPF redistributed metric type. */
typedef enum {
    /** No redistributed metric type is set. */
    VTSS_APPL_OSPF_REDIST_METRIC_TYPE_NONE,

    /** The specified metric value is used. */
    VTSS_APPL_OSPF_REDIST_METRIC_TYPE_SPECIFIED,

    /** External link type 1. */
    VTSS_APPL_OSPF_REDIST_METRIC_TYPE_1,

    /** External link type 2. */
    VTSS_APPL_OSPF_REDIST_METRIC_TYPE_2,

    /** The maximum of the redistributed metric type. */
    VTSS_APPL_OSPF_REDIST_METRIC_TYPE_COUNT
} vtss_appl_ospf_redist_metric_type_t;

/** \brief The OSPF redistributed protocol type. */
enum {
    /** The OSPF redistributed protocol type for the connected interfaces. */
    VTSS_APPL_OSPF_REDIST_PROTOCOL_CONNECTED,

    /** The OSPF redistributed protocol type for the static routes. */
    VTSS_APPL_OSPF_REDIST_PROTOCOL_STATIC,

    /** The maximum of the OSPF route redistributed protocol type. */
    VTSS_APPL_OSPF_REDIST_PROTOCOL_COUNT
};

/** \brief The data structure for the OSPF router ID. */
typedef struct {
     /** The OSPF redistributed metric type. */
    vtss_appl_ospf_redist_metric_type_t type;

    /** User specified metric value for the external routes.
      * The field is significant only when arugment 'type' is
      * VTSS_APPL_OSPF_ROUTE_METRIC_SPECIFIED */
    vtss_appl_ospf_metric_t metric;
} vtss_appl_ospf_redist_conf_t;

/** \brief The data structure for the OSPF router configuration. */
typedef struct {
    /** Configure the OSPF router ID. */
    vtss_appl_ospf_router_id_conf_t router_id;

    /** Configure all interfaces as passive-interface by default. */
    mesa_bool_t default_passive_interface;

    /** Indicate the 'def_metric' argument is a specific configured value
      * or not. */
    mesa_bool_t is_specific_def_metric;

    /** User specified default metric value for the OSPF routing protocol.
      * The field is significant only when the arugment 'is_specific_def_metric'
      * is TRUE */
    vtss_appl_ospf_metric_t def_metric;

    /** Configure OSPF route redistribution. */
    vtss_appl_ospf_redist_conf_t redist_conf[VTSS_APPL_OSPF_REDIST_PROTOCOL_COUNT];
} vtss_appl_ospf_router_conf_t;

/** \brief The data structure for the OSPF router interface configuration. */
typedef struct {
    /** Enable the interface as OSPF passive-interface. */
    mesa_bool_t passive_enabled;
} vtss_appl_ospf_router_intf_conf_t;

/** \brief The data structure for the OSPF router status. */
typedef struct {
    /** The OSPF router ID. */
    vtss_appl_ospf_router_id_t  ospf_router_id;

    /** Delay time (in seconds)of SPF calculations. */
    uint32_t    spf_delay;

    /** Minimum hold time (in milliseconds) between consecutive SPF calculations. */
    uint32_t    spf_holdtime;

    /** Maximum wait time (in milliseconds) between consecutive SPF calculations. */
    uint32_t    spf_max_waittime;

    /** Time (in milliseconds) that has passed between the start of the SPF
      algorithm execution and the current time. */
    uint64_t    last_executed_spf_ts;

    /** Minimum interval (in seconds) between link-state advertisements. */
    uint32_t    min_lsa_interval;

    /** Maximum arrival time (in milliseconds) of link-state advertisements. */
    uint32_t    min_lsa_arrival;

    /** Number of external link-state advertisements. */
    uint32_t    external_lsa_count;

    /** Number of external link-state checksum. */
    uint32_t    external_lsa_checksum;

    /** Number of areas attached to the router. */
    uint32_t    attached_area_count;
} vtss_appl_ospf_router_status_t;

/**
 * \brief Get the OSPF router configuration.
 * \param id   [IN] OSPF instance ID.
 * \param conf [OUT] OSPF router configuration.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_router_conf_get(
        const vtss_appl_ospf_id_t       id,
        vtss_appl_ospf_router_conf_t    *const conf);

/**
 * \brief Set the OSPF router configuration.
 * \param id   [IN] OSPF instance ID.
 * \param conf [IN] OSPF router configuration.
 * \return Error code.
 * VTSS_APPL_FRR_OSPF_ERROR_ROUTER_ID_CHANGE_NOT_TAKE_EFFECT means that router
 * ID change doesn't take effect immediately. The new setting will be applied
 * after OSPF process restarting.
 */
mesa_rc vtss_appl_ospf_router_conf_set(
        const vtss_appl_ospf_id_t           id,
        const vtss_appl_ospf_router_conf_t  *const conf);

/**
 * \brief Get the OSPF router interface configuration.
 * \param id      [IN] OSPF instance ID.
 * \param ifindex [IN]  The index of VLAN interface.
 * \param conf    [OUT] OSPF router interface configuration.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_router_intf_conf_get(
        const vtss_appl_ospf_id_t           id,
        const vtss_ifindex_t                ifindex,
        vtss_appl_ospf_router_intf_conf_t   *const conf);

/**
 * \brief Set the OSPF router interface configuration.
 * \param id      [IN] OSPF instance ID.
 * \param ifindex [IN] The index of VLAN interface.
 * \param conf    [IN] OSPF router interface configuration.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_router_intf_conf_set(
        const vtss_appl_ospf_id_t               id,
        const vtss_ifindex_t                    ifindex,
        const vtss_appl_ospf_router_intf_conf_t *const conf);

/**
 * \brief Iterate through all OSPF router interfaces.
 * \param current_id      [IN]  Current OSPF ID
 * \param next_id         [OUT] Next OSPF ID
 * \param current_ifindex [IN]  Current ifIndex
 * \param next_ifindex    [OUT] Next ifIndex
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_router_intf_conf_itr(
        const vtss_appl_ospf_id_t   *const current_id,
        vtss_appl_ospf_id_t         *const next_id,
        const vtss_ifindex_t        *const current_ifindex,
        vtss_ifindex_t              *const next_ifindex);

/**
 * \brief Get the OSPF router configuration.
 * \param id     [IN] OSPF instance ID.
 * \param status [OUT] Status for 'id'.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_router_status_get(
        const vtss_appl_ospf_id_t       id,
        vtss_appl_ospf_router_status_t  *const status);

//----------------------------------------------------------------------------
//** OSPF network area configuration/status
//----------------------------------------------------------------------------
/** \brief The authentication type. */
typedef enum {
    /** Simple password authentication. */
    VTSS_APPL_OSPF_AUTH_TYPE_SIMPLE_PASSWORD,

    /** MD5 digest authentication. */
    VTSS_APPL_OSPF_AUTH_TYPE_MD5,

    /** NULL authentication. */
    VTSS_APPL_OSPF_AUTH_TYPE_NULL,

    /** Area authentication. This type is only used for interface
     * authentication. When the interface is configured to this type.
     * It refers to area authentication onfiguratrion.
     * If the area authenticaton is disabled, the behavior is the same as NULL
     * authentication.
     */
    VTSS_APPL_OSPF_AUTH_TYPE_AREA_CFG,

    /** The maximum of the authentication type which is used for validation. */
    VTSS_APPL_OSPF_AUTH_TYPE_COUNT
} vtss_appl_ospf_auth_type_t;

/** \brief The authentication type for the OSPF area. */
typedef enum {
    /** Normal area. */
    VTSS_APPL_OSPF_AREA_NORMAL,
    /** Stub area. */
    VTSS_APPL_OSPF_AREA_STUB,
    /** Totally stub area. */
    VTSS_APPL_OSPF_AREA_TOTALLY_STUB,
    /** The maximum of the area type. */
    VTSS_APPL_OSPF_AREA_COUNT
} vtss_appl_ospf_area_type_t;

/** \brief The data structure for the OSPF area status. */
typedef struct {
    /** To indicate if it's backbone area or not. */
    mesa_bool_t is_backbone;

    /** To indicate the area type. */
    vtss_appl_ospf_area_type_t area_type;

    /** Number of active interfaces attached in the area. */
    uint32_t attached_intf_active_count;

    /** The authentication status for the area. */
    vtss_appl_ospf_auth_type_t  auth_type;

    /** Number of times SPF algorithm has been executed for the particular area. */
    uint32_t spf_executed_count;

    /** Number of the total LSAs for the particular area. */
    uint32_t lsa_count;

    /** Number of the router-LSAs(Type-1) of a given type for the particular area. */
    uint32_t router_lsa_count;

    /** The the router-LSAs(Type-1) checksum. */
    uint32_t router_lsa_checksum;

    /** Number of the network-LSAs(Type-2) of a given type for the particular area. */
    uint32_t network_lsa_count;

    /** The the network-LSAs(Type-2) checksum. */
    uint32_t network_lsa_checksum;

    /** Number of the summary-LSAs(Type-3) of a given type for the particular area. */
    uint32_t summary_lsa_count;

    /** The the summary-LSAs(Type-3) checksum. */
    uint32_t summary_lsa_checksum;

    /** Number of the ASBR-summary-LSAs(Type-4) of a given type for the particular area. */
    uint32_t asbr_summary_lsa_count;

    /** The the ASBR-summary-LSAs(Type-4) checksum. */
    uint32_t asbr_summary_lsa_checksum;
} vtss_appl_ospf_area_status_t;

/**
 * \brief Get the OSPF area configuration.
 * \param id      [IN]  OSPF instance ID.
 * \param network [IN]  OSPF area network.
 * \param area_id [OUT] OSPF area ID.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_area_conf_get(
        const vtss_appl_ospf_id_t   id,
        const mesa_ipv4_network_t   *const network,
        vtss_appl_ospf_area_id_t    *const area_id);

/**
 * \brief Add/set the OSPF area configuration.
 * \param id      [IN] OSPF instance ID.
 * \param network [IN] OSPF area network.
 * \param area_id [IN] OSPF area ID.
 * \return Error code.
 * VTSS_APPL_FRR_OSPF_ERROR_AREA_ID_CHANGE_NOT_TAKE_EFFECT means that area ID
 * change doesn't take effect.
 */
mesa_rc vtss_appl_ospf_area_conf_add(
        const vtss_appl_ospf_id_t       id,
        const mesa_ipv4_network_t       *const network,
        const vtss_appl_ospf_area_id_t  *const area_id);

/**
 * \brief Delete the OSPF area configuration.
 * \param id      [IN] OSPF instance ID.
 * \param network [IN] OSPF area network.
 * \return Error code.
 * VTSS_APPL_FRR_OSPF_ERROR_AREA_ID_CHANGE_NOT_TAKE_EFFECT means that area ID
 * change doesn't take effect.
 */
mesa_rc vtss_appl_ospf_area_conf_del(
        const vtss_appl_ospf_id_t   id,
        const mesa_ipv4_network_t   *const network);

/**
 * \brief Iterate the OSPF areas
 * \param cur_id       [IN]  Current OSPF ID
 * \param next_id      [OUT] Next OSPF ID
 * \param cur_network  [IN]  Current area network
 * \param next_network [OUT] Next area network
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_area_conf_itr(
        const vtss_appl_ospf_id_t   *const cur_id,
        vtss_appl_ospf_id_t         *const next_id,
        const mesa_ipv4_network_t   *const cur_network,
        mesa_ipv4_network_t         *const next_network);

/**
 * \brief Iterate through the OSPF area status.
 * \param cur_id       [IN]  Current OSPF ID
 * \param next_id      [OUT] Next OSPF ID
 * \param cur_area_id  [IN]  Current area ID
 * \param next_area_id [OUT] Next area ID
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_area_status_itr(
    const vtss_appl_ospf_id_t       *const cur_id,
    vtss_appl_ospf_id_t             *const next_id,
    const vtss_appl_ospf_area_id_t  *const cur_area_id,
    vtss_appl_ospf_area_id_t        *const next_area_id);

/**
 * \brief Get the OSPF area status.
 * \param id        [IN] OSPF instance ID.
 * \param area      [IN] OSPF area key.
 * \param status    [OUT] OSPF area val.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_area_status_get(
        const vtss_appl_ospf_id_t         id,
        const mesa_ipv4_t                 area,
        vtss_appl_ospf_area_status_t      *const status);

//----------------------------------------------------------------------------
//** OSPF routing entry
//----------------------------------------------------------------------------

/** \brief The protocol of the route. */
typedef enum {
    /** The route is created by DHCP. */
    VTSS_APPL_ROUTE_PROTO_DHCP,
    /** The destination network is connected directly. */
    VTSS_APPL_ROUTE_PROTO_CONNECTED,
    /** The route is created by user.  */
    VTSS_APPL_ROUTE_PROTO_STATIC,
    /** The route is created by OSPF. */
    VTSS_APPL_ROUTE_PROTO_OSPF,
    /** The maximum of the protocol type which is used for validation. */
    VTSS_APPL_ROUTE_PROTO_COUNT,
} vtss_appl_route_protocol_t;

/** \brief The data structure for the IPv4 OSPF route key. */
typedef struct {
    /**  Network to route */
    mesa_ipv4_network_t network;
    /** The protocol of the route. */
    vtss_appl_route_protocol_t protocol;
    /** IP address of next-hop router. */
    mesa_ipv4_t         nexthop;
} vtss_appl_route_ipv4_key_t;

/** \brief The data structure for the IPv4 OSPF route status. */
typedef struct {
    /** It's used to indicate if this entry is applied to FIB or not. */
    mesa_bool_t         selected;
    /** The metric of the route. */
    uint32_t            metric;
    /** The distance of the route. */
    uint8_t             distance;
    /** Number of seconds since the route was created. */
    uint32_t            uptime;
    /** The interface where the ip packet is outgoing. */
    vtss_ifindex_t      ifindex;
    /** It's used to indicate if the destination network is reachable or not. */
    mesa_bool_t         active;
} vtss_appl_route_ipv4_status_t;

/**
 * \brief Iterate through the routes in the routing table.
 * \param in [IN] Pointer to current route. Provide a null pointer to get the
 *                first interface.
 * \param out [OUT] Next route (relative to the value provided in 'in').
 * \return Error code.
 */
mesa_rc vtss_appl_route_ipv4_status_itr(
        const vtss_appl_route_ipv4_key_t  *const in,
        vtss_appl_route_ipv4_key_t        *const out);

/**
 * \brief Get status for a specific route.
 * \param key   [IN] Route to query.
 * \param status [OUT] Status for 'key'.
 * \return Error code.
 */
mesa_rc vtss_appl_route_ipv4_status_get(
        const vtss_appl_route_ipv4_key_t        *const key,
        vtss_appl_route_ipv4_status_t     *const status);


/**
 * \brief Get status for all route.
 * \param ipv4_routes [IN] An empty container.
 * \param ipv4_routes [OUT] An container with all IP roeutes.
 * \return Error code.
 */
mesa_rc vtss_appl_route_ipv4_status_get_all(
		vtss::Map<vtss_appl_route_ipv4_key_t,
		vtss_appl_route_ipv4_status_t> &ipv4_routes);

//----------------------------------------------------------------------------
//** OSPF authentication
//----------------------------------------------------------------------------
/**< The precedence of message digest key starting number. */
#define VTSS_APPL_OSPF_MD_KEY_PRECEDENCE_START (1)

/** The minimum length of simple password. */
#define VTSS_APPL_OSPF_AUTH_SIMPLE_KEY_MIN_LEN (1)

/** The maximum length of simple password. */
#define VTSS_APPL_OSPF_AUTH_SIMPLE_KEY_MAX_LEN (8)

/** The minimum length of digest password. */
#define VTSS_APPL_OSPF_AUTH_DIGEST_KEY_MIN_LEN (1)

/** The maximum length of digest password. */
#define VTSS_APPL_OSPF_AUTH_DIGEST_KEY_MAX_LEN (16)

/** The minimum ID of digest key. */
#define VTSS_APPL_OSPF_AUTH_DIGEST_KEY_ID_MIN  (1)

/** The maximum ID of digest key. */
#define VTSS_APPL_OSPF_AUTH_DIGEST_KEY_ID_MAX  (255)

/** The formula of converting the plain text length to encrypted data length. */
#define VTSS_APPL_OSPF_AUTH_ENCRYPTED_KEY_LEN(x) \
    ((16 + ((x) / 16 + 1) * 16 + 32) * 2)

/** Maximum length of encrypted simple password.  */
#define VTSS_APPL_OSPF_AUTH_ENCRYPTED_SIMPLE_KEY_LEN \
    VTSS_APPL_OSPF_AUTH_ENCRYPTED_KEY_LEN(VTSS_APPL_OSPF_AUTH_SIMPLE_KEY_MAX_LEN)

/** Maximum length of encrypted digest password.  */
#define VTSS_APPL_OSPF_AUTH_ENCRYPTED_DIGEST_KEY_LEN \
    VTSS_APPL_OSPF_AUTH_ENCRYPTED_KEY_LEN(VTSS_APPL_OSPF_AUTH_DIGEST_KEY_MAX_LEN)

/** \brief The data structure for the authentication configuration. */
typedef struct {
    /** The authentication type. */
    vtss_appl_ospf_auth_type_t  auth_type;
    /** Set 'true' to indicate the simple password is encrypted by AES256,
     *  otherwise it's unencrypted.
     */
    mesa_bool_t        is_encrypted;
    /** The simple password. */
    char                        auth_key[VTSS_APPL_OSPF_AUTH_ENCRYPTED_SIMPLE_KEY_LEN + 1];
} vtss_appl_ospf_auth_conf_t;


/** \brief The data structure for the digest key configuration. */
typedef struct {
    /** Set 'true' to indicate the key is encrypted by AES256,
     *  otherwise it's unencrypted.
     */
    mesa_bool_t        is_encrypted;
    /** The digest key. */
    char        digest_key[VTSS_APPL_OSPF_AUTH_ENCRYPTED_DIGEST_KEY_LEN + 1];
} vtss_appl_ospf_auth_digest_key_t;

//----------------------------------------------------------------------------
//** OSPF authentication: VLAN interface mode
//----------------------------------------------------------------------------
/**
 * \brief Add the digest key in the specific interface.
 * \param ifindex       [IN] The index of VLAN interface.
 * \param key_id        [IN] The key ID.
 * \param digest_key    [IN] The digest key.
 * \return Error code.
 *  VTSS_APPL_FRR_OSPF_ERROR_AUTH_KEY_TOO_LONG means the password
 *  is too long.
 *  VTSS_APPL_FRR_OSPF_ERROR_AUTH_KEY_INVALID means the key is not a valid
 *  key for AES256.
 */
mesa_rc vtss_appl_ospf_intf_auth_digest_key_add(
        const vtss_ifindex_t                    ifindex,
        const vtss_appl_ospf_md_key_id_t        key_id,
        const vtss_appl_ospf_auth_digest_key_t  *const digest_key);

/**
 * \brief Get the digest key in the specific interface.
 * \param ifindex       [IN]  The index of VLAN interface.
 * \param key_id        [IN]  The key ID.
 * \param digest_key    [OUT] The digest key.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_intf_auth_digest_key_get(
        const vtss_ifindex_t                ifindex,
        const vtss_appl_ospf_md_key_id_t    key_id,
        vtss_appl_ospf_auth_digest_key_t    *const digest_key);

/**
 * \brief Delete a digest key in the specific interface.
 * \param ifindex       [IN]  The index of VLAN interface.
 * \param key_id        [IN] The key ID.
 * \return Error code.
 *  backbone area.
 */
mesa_rc vtss_appl_ospf_intf_auth_digest_key_del(
        const vtss_ifindex_t                ifindex,
        const vtss_appl_ospf_md_key_id_t    key_id);

/**
 * \brief Iterate the digest key.
 * \param current_ifindex [IN]  The current ifIndex.
 * \param next_ifindex    [OUT] The next ifIndex.
 * \param current_key_id  [IN]  The current key ID.
 * \param next_key_id     [OUT] The next key ID.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_intf_auth_digest_key_itr(
        const vtss_ifindex_t                *const current_ifindex,
        vtss_ifindex_t                      *const next_ifindex,
        const vtss_appl_ospf_md_key_id_t    *const current_key_id,
        vtss_appl_ospf_md_key_id_t          *const next_key_id);

/**
 * \brief Iterate the digest key by the precedence.
 * \param current_ifindex [IN]  The current ifIndex.
 * \param next_ifindex    [OUT] The next ifIndex.
 * \param current_pre_id  [IN]  The precedence ID.
 * \param next_pre_id     [OUT] The next precedence ID.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_intf_md_key_precedence_itr(
        const vtss_ifindex_t                *const current_ifindex,
        vtss_ifindex_t                      *const next_ifindex,
        const uint32_t                      *const current_pre_id,
        uint32_t                            *const next_pre_id);

/**
 * \brief Get the digest key by the precedence.
 * \param ifindex [IN]  The current ifIndex.
 * \param pre_id  [IN]  The precedence ID.
 * \param key_id  [OUT] The key ID.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_intf_md_key_precedence_get(
        const vtss_ifindex_t               ifindex,
        const uint32_t                     pre_id,
        vtss_appl_ospf_md_key_id_t         *const key_id);
//----------------------------------------------------------------------------
//** OSPF authentication: router configuration mode
//----------------------------------------------------------------------------
/**
 * \brief Add the authentication configuration in the specific area.
 * \param id        [IN] OSPF instance ID.
 * \param area_id   [IN] OSPF area ID.
 * \param auth_type [IN] The authentication type.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_area_auth_conf_add(
        const vtss_appl_ospf_id_t           id,
        const vtss_appl_ospf_area_id_t      area_id,
        const vtss_appl_ospf_auth_type_t    auth_type);

/**
 * \brief Set the authentication configuration in the specific area.
 * \param id        [IN] OSPF instance ID.
 * \param area_id   [IN] OSPF area ID.
 * \param auth_type [IN] The authentication type.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_area_auth_conf_set(
        const vtss_appl_ospf_id_t           id,
        const vtss_appl_ospf_area_id_t      area_id,
        const vtss_appl_ospf_auth_type_t    auth_type);

/**
 * \brief Get the authentication configuration in the specific area.
 * \param id        [IN]  OSPF instance ID.
 * \param area_id   [IN]  OSPF area ID.
 * \param auth_type [OUT] The authentication type.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_area_auth_conf_get(
        const vtss_appl_ospf_id_t       id,
        const vtss_appl_ospf_area_id_t  area_id,
        vtss_appl_ospf_auth_type_t      *const auth_type);

/**
 * \brief Delete the authentication configuration in the specific area.
 * \param id      [IN]  OSPF instance ID.
 * \param area_id [IN]  OSPF area ID.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_area_auth_conf_del(
        const vtss_appl_ospf_id_t       id,
        const vtss_appl_ospf_area_id_t  area_id);

/**
 * \brief Iterate the specific area with authentication configuration.
 * \param current_id      [IN]  Current OSPF ID
 * \param next_id         [OUT] Next OSPF ID
 * \param current_area_id [IN]  Current area ID
 * \param next_area_id    [OUT] Next area ID
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_area_auth_conf_itr(
        const vtss_appl_ospf_id_t       *const current_id,
        vtss_appl_ospf_id_t             *const next_id,
        const vtss_appl_ospf_area_id_t  *const current_area_id,
        vtss_appl_ospf_area_id_t        *const next_area_id);

//----------------------------------------------------------------------------
//** OSPF area range
//----------------------------------------------------------------------------
/** \brief The data structure for the OSPF area range configuration. */
typedef struct {
    /** When the value is 'true', the ABR can summarize intra area paths from
     *  the address range in one summary-LSA(Type-3) advertised to other areas.
     *  When the value is 'false', the ABR does not advertised the summary-LSA
     *  (Type-3) for the address range. */
    mesa_bool_t is_advertised;

    /** Indicate the 'cost' argument is a specific configured value
      * or not. */
    mesa_bool_t is_specific_cost;

    /** User specified cost (or metric) for this summary route. */
    vtss_appl_ospf_cost_t cost;
} vtss_appl_ospf_area_range_conf_t;

/**
 * \brief Get the OSPF area range configuration.
 * \param id      [IN] OSPF instance ID.
 * \param area_id [IN] OSPF area ID.
 * \param network [IN] OSPF area range network.
 * \param conf    [IN] OSPF area range configuration.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_area_range_conf_get(
        const vtss_appl_ospf_id_t           id,
        const vtss_appl_ospf_area_id_t      area_id,
        const mesa_ipv4_network_t           network,
        vtss_appl_ospf_area_range_conf_t    *const conf);

/**
 * \brief Set the OSPF area range configuration.
 * \param id      [IN] OSPF instance ID.
 * \param area_id [IN] OSPF area ID.
 * \param network [IN] OSPF area range network.
 * \param conf    [IN] OSPF area range configuration.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_area_range_conf_set(
        const vtss_appl_ospf_id_t               id,
        const vtss_appl_ospf_area_id_t          area_id,
        const mesa_ipv4_network_t               network,
        const vtss_appl_ospf_area_range_conf_t  *const conf);

/**
 * \brief Add the OSPF area range configuration.
 * \param id      [IN] OSPF instance ID.
 * \param area_id [IN] OSPF area ID.
 * \param network [IN] OSPF area range network.
 * \param conf    [IN] OSPF area range configuration.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_area_range_conf_add(
        const vtss_appl_ospf_id_t               id,
        const vtss_appl_ospf_area_id_t          area_id,
        const mesa_ipv4_network_t               network,
        const vtss_appl_ospf_area_range_conf_t  *const conf);

/**
 * \brief Delete the OSPF area range configuration.
 * \param id      [IN] OSPF instance ID.
 * \param area_id [IN] OSPF area ID.
 * \param network [IN] OSPF area range network.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_area_range_conf_del(
        const vtss_appl_ospf_id_t       id,
        const vtss_appl_ospf_area_id_t  area_id,
        const mesa_ipv4_network_t       network);

/**
 * \brief Iterate the OSPF area ranges
 * \param current_id      [IN]  Current OSPF ID
 * \param next_id         [OUT] Next OSPF ID
 * \param current_area_id [IN]  Current area ID
 * \param next_area_id    [OUT] Next area ID
 * \param current_network [IN]  Current network address
 * \param next_network    [OUT] Next network address
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_area_range_conf_itr(
        const vtss_appl_ospf_id_t       *const current_id,
        vtss_appl_ospf_id_t             *const next_id,
        const vtss_appl_ospf_area_id_t  *const current_area_id,
        vtss_appl_ospf_area_id_t        *const next_area_id,
        const mesa_ipv4_network_t       *const current_network,
        mesa_ipv4_network_t             *const next_network);

//----------------------------------------------------------------------------
//** OSPF virtual link
//----------------------------------------------------------------------------
/** \brief The data structure of OSPF virtual link configuration. */
typedef struct {
    /** The time interval (in seconds) between hello packets. */
    uint32_t    hello_interval;

    /** The time interval (in seconds) between hello packets. */
    uint32_t    dead_interval;

    /** The time interval (in seconds) between link-state advertisement
     *  (LSA) retransmissions for adjacencies. */
    uint32_t    retransmit_interval;

    /** The authentication type. */
    vtss_appl_ospf_auth_type_t  auth_type;

    /** Set 'true' to indicate the simple password is encrypted by AES256,
     *  otherwise it's unencrypted. */
    mesa_bool_t is_encrypted;

    /** The simple password. */
    char        simple_pwd[VTSS_APPL_OSPF_AUTH_ENCRYPTED_SIMPLE_KEY_LEN + 1];
} vtss_appl_ospf_vlink_conf_t;

/**
 * \brief Add a virtual link in the specific area.
 * \param id        [IN] OSPF instance ID.
 * \param area_id   [IN] The area ID of the configuration.
 * \param router_id [IN] The destination router id of virtual link.
 * \param conf      [IN] The virtual link configuration for adding.
 * \return Error code.
 *  VTSS_APPL_FRR_OSPF_ERROR_VIRTUAL_LINK_NOT_ON_BACKBONE means the area is
 *  backbone area.
 */
mesa_rc vtss_appl_ospf_vlink_conf_add(
        const vtss_appl_ospf_id_t           id,
        const vtss_appl_ospf_area_id_t      area_id,
        const vtss_appl_ospf_router_id_t    router_id,
        const vtss_appl_ospf_vlink_conf_t   *const conf);

/**
 * \brief Set the configuration for a virtual link.
 * \param id        [IN] OSPF instance ID.
 * \param area_id   [IN] The area ID of the configuration.
 * \param router_id [IN] The destination router id of virtual link.
 * \param conf      [IN] The virtual link configuration for setting.
 * \return Error code.
 *  VTSS_APPL_FRR_OSPF_ERROR_VIRTUAL_LINK_NOT_ON_BACKBONE means the area is
 *  backbone area.
 */
mesa_rc vtss_appl_ospf_vlink_conf_set(
        const vtss_appl_ospf_id_t           id,
        const vtss_appl_ospf_area_id_t      area_id,
        const vtss_appl_ospf_router_id_t    router_id,
        const vtss_appl_ospf_vlink_conf_t   *const conf);

/**
 * \brief Get the configuration for a virtual link.
 * \param id        [IN]  OSPF instance ID.
 * \param area_id   [IN]  The area ID of the configuration.
 * \param router_id [IN]  The destination router id of virtual link.
 * \param conf      [OUT] The virtual link configuration.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_vlink_conf_get(
        const vtss_appl_ospf_id_t           id,
        const vtss_appl_ospf_area_id_t      area_id,
        const vtss_appl_ospf_router_id_t    router_id,
        vtss_appl_ospf_vlink_conf_t         *const conf);

/**
 * \brief Delete a specific virtual link.
 * \param id        [IN] OSPF instance ID.
 * \param area_id   [IN] The area ID of the configuration.
 * \param router_id [IN] The destination router id of virtual link.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_vlink_conf_del(
        const vtss_appl_ospf_id_t           id,
        const vtss_appl_ospf_area_id_t      area_id,
        const vtss_appl_ospf_router_id_t    router_id);

/**
 * \brief Iterate through all OSPF virtual links.
 * \param current_id        [IN]  Current OSPF ID
 * \param next_id           [OUT] Next OSPF ID
 * \param current_area_id   [IN]  Current Area
 * \param next_area_id      [OUT] Next Area
 * \param current_router_id [IN]  Current Destination
 * \param next_router_id    [OUT] Next Destination
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_vlink_itr(
        const vtss_appl_ospf_id_t           *const current_id,
        vtss_appl_ospf_id_t                 *const next_id,
        const vtss_appl_ospf_area_id_t      *const current_area_id,
        vtss_appl_ospf_area_id_t            *const next_area_id,
        const vtss_appl_ospf_router_id_t    *const current_router_id,
        vtss_appl_ospf_router_id_t          *const next_router_id);

//----------------------------------------------------------------------------
//** OSPF virtual link authentication: message digest key
//----------------------------------------------------------------------------
/**
 * \brief Get the message digest key for the specific virtual link.
 * \param id        [IN]  OSPF instance ID.
 * \param area_id   [IN]  OSPF area ID.
 * \param router_id [IN]  OSPF router ID.
 * \param key_id    [IN]  The message digest key ID.
 * \param md_key    [OUT] The message digest key configuration.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_vlink_md_key_conf_get(
        const vtss_appl_ospf_id_t           id,
        const vtss_appl_ospf_area_id_t      area_id,
        const vtss_appl_ospf_router_id_t    router_id,
        const vtss_appl_ospf_md_key_id_t    key_id,
        vtss_appl_ospf_auth_digest_key_t    *const md_key);

/**
 * \brief Add the message digest key for the specific virtual link.
 * \param id        [IN] OSPF instance ID.
 * \param area_id   [IN] OSPF area ID.
 * \param router_id [IN] OSPF router ID.
 * \param key_id    [IN] The message digest key ID.
 * \param md_key    [IN] The message digest key configuration.
 * \return Error code.
 *  VTSS_APPL_FRR_OSPF_ERROR_AUTH_KEY_TOO_LONG means the password
 *  is too long.
 *  VTSS_APPL_FRR_OSPF_ERROR_AUTH_KEY_INVALID means the key is not a valid
 *  key for AES256.
 */
mesa_rc vtss_appl_ospf_vlink_md_key_conf_add(
        const vtss_appl_ospf_id_t               id,
        const vtss_appl_ospf_area_id_t          area_id,
        const vtss_appl_ospf_router_id_t        router_id,
        const vtss_appl_ospf_md_key_id_t        key_id,
        const vtss_appl_ospf_auth_digest_key_t  *const md_key);

/**
 * \brief Delete a message digest key for the specific virtual link.
 * \param id        [IN] OSPF instance ID.
 * \param area_id   [IN] OSPF area ID.
 * \param router_id [IN] OSPF router ID.
 * \param key_id    [IN] The message digest key ID.
 * \return Error code.
 *  backbone area.
 */
mesa_rc vtss_appl_ospf_vlink_md_key_conf_del(
        const vtss_appl_ospf_id_t           id,
        const vtss_appl_ospf_area_id_t      area_id,
        const vtss_appl_ospf_router_id_t    router_id,
        const vtss_appl_ospf_md_key_id_t    key_id);

/**
 * \brief Iterate the message digest key for the specific virtual link.
 * \param current_id        [IN]  Current OSPF ID
 * \param next_id           [OUT] Next OSPF ID
 * \param current_area_id   [IN]  Current area ID
 * \param next_area_id      [OUT] Next area ID
 * \param current_router_id [IN]  Current router ID
 * \param next_router_id    [OUT] Next router ID
 * \param current_key_id    [IN]  The current message digest key ID.
 * \param next_key_id       [OUT] The next message digest key ID.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_vlink_md_key_itr(
        const vtss_appl_ospf_id_t           *const current_id,
        vtss_appl_ospf_id_t                 *const next_id,
        const vtss_appl_ospf_area_id_t      *const current_area_id,
        vtss_appl_ospf_area_id_t            *const next_area_id,
        const mesa_ipv4_t                   *const current_router_id,
        mesa_ipv4_t                         *const next_router_id,
        const vtss_appl_ospf_md_key_id_t    *const current_key_id,
        vtss_appl_ospf_md_key_id_t          *const next_key_id);

/**
 * \brief Get the message digest key precedence for the specific virtual link.
 * \param id         [IN]  OSPF instance ID.
 * \param area_id    [IN]  OSPF area ID.
 * \param router_id  [IN]  OSPF router ID.
 * \param precedence [IN]  The message digest key precedence.
 * \param key_id     [OUT] The message digest key ID.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_vlink_md_key_precedence_get(
        const vtss_appl_ospf_id_t           id,
        const vtss_appl_ospf_area_id_t      area_id,
        const vtss_appl_ospf_router_id_t    router_id,
        const uint32_t                      precedence,
        vtss_appl_ospf_md_key_id_t          *const key_id);

/**
 * \brief Iterate the message digest key precedence for the specific virtual link.
 * \param current_id         [IN]  Current OSPF ID
 * \param next_id            [OUT] Next OSPF ID
 * \param current_area_id    [IN]  Current area ID
 * \param next_area_id       [OUT] Next area ID
 * \param current_router_id  [IN]  Current router ID
 * \param next_router_id     [OUT] Next router ID
 * \param current_precedence [IN]  The current message digest key precedence.
 * \param next_precedence    [OUT] The next message digest key precedence.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_vlink_md_key_precedence_itr(
        const vtss_appl_ospf_id_t           *const current_id,
        vtss_appl_ospf_id_t                 *const next_id,
        const vtss_appl_ospf_area_id_t      *const current_area_id,
        vtss_appl_ospf_area_id_t            *const next_area_id,
        const vtss_appl_ospf_router_id_t    *const current_router_id,
        vtss_appl_ospf_router_id_t          *const next_router_id,
        const uint32_t                      *const current_precedence,
        uint32_t                            *const next_precedence);

//----------------------------------------------------------------------------
//** OSPF stub area
//----------------------------------------------------------------------------

/** \brief The data structure of stub area configuration. */
typedef struct {
    /** Set 'true' to configure the area as totally stub
     *  area, otherwise to set it as stub area.
     */
    mesa_bool_t no_summary;
} vtss_appl_ospf_stub_area_conf_t;

/**
 * \brief Add the specific area in the stub areas.
 * \param id      [IN] OSPF instance ID.
 * \param area_id [IN] The area ID of the stub area configuration.
 * \param conf    [IN] The stub area configuration for adding.
 * \return Error code.
 *  VTSS_APPL_FRR_OSPF_ERROR_STUB_AREA_NOT_FOR_BACKBONE means the area is
 *  backbone area.
 */
mesa_rc vtss_appl_ospf_stub_area_conf_add(
        const vtss_appl_ospf_id_t               id,
        const vtss_appl_ospf_area_id_t          area_id,
        const vtss_appl_ospf_stub_area_conf_t   *const conf);

/**
 * \brief Set the configuration for a specific stub area.
 * \param id      [IN] OSPF instance ID.
 * \param area_id [IN] The area ID of the stub area configuration.
 * \param conf    [IN] The stub area configuration for setting.
 * \return Error code.
 *  VTSS_APPL_FRR_OSPF_ERROR_STUB_AREA_NOT_FOR_BACKBONE means the area is
 *  backbone area.
 */
mesa_rc vtss_appl_ospf_stub_area_conf_set(
        const vtss_appl_ospf_id_t               id,
        const vtss_appl_ospf_area_id_t          area_id,
        const vtss_appl_ospf_stub_area_conf_t   *const conf);

/**
 * \brief Get the configuration for a specific stub area.
 * \param id      [IN] OSPF instance ID.
 * \param area_id [IN]  The area ID of the stub area configuration.
 * \param conf    [OUT] The stub area configuration.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_stub_area_conf_get(
        const vtss_appl_ospf_id_t               id,
        const vtss_appl_ospf_area_id_t          area_id,
        vtss_appl_ospf_stub_area_conf_t         *const conf);

/**
 * \brief Delete a specific stub area.
 * \param id      [IN] OSPF instance ID.
 * \param area_id [IN] The area ID of the stub area configuration.
 * \return Error code.
 *  VTSS_APPL_FRR_OSPF_ERROR_STUB_AREA_NOT_FOR_BACKBONE means the area is
 *  backbone area.
 */
mesa_rc vtss_appl_ospf_stub_area_conf_del(
        const vtss_appl_ospf_id_t               id,
        const vtss_appl_ospf_area_id_t          area_id);

/**
 * \brief Iterate the stub areas.
 * \param current_id      [IN]  Current OSPF ID
 * \param next_id         [OUT] Next OSPF ID
 * \param current_area_id [IN]  Current area ID
 * \param next_area_id    [OUT] Next area ID
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_stub_area_conf_itr(
        const vtss_appl_ospf_id_t       *const current_id,
        vtss_appl_ospf_id_t             *const next_id,
        const vtss_appl_ospf_area_id_t  *const current_area_id,
        vtss_appl_ospf_area_id_t        *const next_area_id);

//----------------------------------------------------------------------------
//** OSPF interface parameter tuning
//----------------------------------------------------------------------------

/** \brief The data structure for the OSPF VLAN interface configuration. */
typedef struct {
    /** User specified router priority for the interface. */
    vtss_appl_ospf_priority_t priority;

    /** Link state metric for the interface. It used for Shortest Path First
     * (SPF) calculation. */
    /** Indicate the 'cost' argument is a specific configured value or not. */
    mesa_bool_t is_specific_cost;
    /** User specified cost for the interface. */
    vtss_appl_ospf_cost_t cost;

    /** Enable the feature of fast hello packets or not. */
    mesa_bool_t is_fast_hello_enabled;

    /** It specifies how many Hello packets will be sent per second.
     *  It is significant only when the paramter 'is_fast_hello_enabled' is
     *  true */
    uint32_t fast_hello_packets;

    /** The time interval (in seconds) between hello packets.
     *  The value is set to 1 when the paramter 'is_fast_hello_enabled' is
     *  true */
    uint32_t dead_interval;

    /** The time interval (in seconds) between hello packets. */
    uint32_t hello_interval;

    /** The time interval (in seconds) between between link-state advertisement
     *  (LSA) retransmissions for adjacencies. */
    uint32_t retransmit_interval;

    /** The interface authentication type. */
    vtss_appl_ospf_auth_type_t  auth_type;

    /** Set 'true' to indicate the simple password is encrypted,
     *  otherwise it's unencrypted.
     */
    mesa_bool_t        is_encrypted;

    /** The simple password for interface authentication. Set null string to
     * delete the key.
     */
    char               auth_key[VTSS_APPL_OSPF_AUTH_ENCRYPTED_SIMPLE_KEY_LEN + 1];
} vtss_appl_ospf_intf_conf_t;

/**
 * \brief Get the OSPF VLAN interface configuration.
 * \param ifindex [IN]  The index of VLAN interface.
 * \param conf    [OUT] OSPF VLAN interface configuration.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_intf_conf_get(
        const vtss_ifindex_t        ifindex,
        vtss_appl_ospf_intf_conf_t  *const conf);

/**
 * \brief Set the OSPF VLAN interface configuration.
 * \param ifindex [IN] The index of VLAN interface.
 * \param conf    [IN] OSPF VLAN interface configuration.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_intf_conf_set(
        const vtss_ifindex_t                ifindex,
        const vtss_appl_ospf_intf_conf_t    *const conf);

/**
 * \brief Iterate through all OSPF VLAN interfaces.
 * \param current_ifindex [IN]  Current ifIndex
 * \param next_ifindex    [OUT] Next ifIndex
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_intf_conf_itr(
        const vtss_ifindex_t    *const current_ifindex,
        vtss_ifindex_t          *const next_ifindex);

//----------------------------------------------------------------------------
//** OSPF interface status
//----------------------------------------------------------------------------

/** \brief The link state of the OSPF interface. */
typedef enum {
    /** Down state */
    VTSS_APPL_OSPF_INTERFACE_DOWN = 1,

    /** lookback interface */
    VTSS_APPL_OSPF_INTERFACE_LOOPBACK = 2,

    /** Waiting state */
    VTSS_APPL_OSPF_INTERFACE_WAITING = 3,

    /** Point-To-Point interface */
    VTSS_APPL_OSPF_INTERFACE_POINT2POINT = 4,

    /** Select as DR other router */
    VTSS_APPL_OSPF_INTERFACE_DR_OTHER = 5,

    /** Select as BDR router */
    VTSS_APPL_OSPF_INTERFACE_BDR = 6,

    /** Select as DR router */
    VTSS_APPL_OSPF_INTERFACE_DR = 7,

    /** Unknown state */
    VTSS_APPL_OSPF_INTERFACE_UNKNOWN

} vtss_appl_ospf_interface_state_t;

/** \brief The data structure for the OSPF interface status. */
typedef struct {
    /** It's used to indicate if the interface is up or down */
    mesa_bool_t                         status;

    /** Indicate if the interface is passive interface */
    mesa_bool_t                         is_passive;

    /** IP address and prefix length */
    mesa_ipv4_network_t                 network;

    /** Area ID */
    vtss_appl_ospf_area_id_t            area_id;

    /** The OSPF router ID. */
    vtss_appl_ospf_router_id_t          router_id;

    /** The cost of the interface. */
    vtss_appl_ospf_cost_t               cost;

    /** define the state of the link */
    vtss_appl_ospf_interface_state_t    state;

    /** The OSPF priority */
    uint8_t                             priority;

    /** The router ID of DR. */
    mesa_ipv4_t                         dr_id;

    /** The IP address of DR. */
    mesa_ipv4_t                         dr_addr;

    /** The router ID of BDR. */
    mesa_ipv4_t                         bdr_id;

    /** The IP address of BDR. */
    mesa_ipv4_t                         bdr_addr;

    /** The IP address of Virtual link peer. */
    mesa_ipv4_t                         vlink_peer_addr;

    /** Hello timer, the unit of time is the second. */
    uint32_t                            hello_time;

    /** Dead timer, the unit of time is the second. */
    uint32_t                            dead_time;

    /** Retransmit timer, the unit of time is the second. */
    uint32_t                            retransmit_time;

    /** Hello due timer, the unit of time is the second. */
    uint32_t                            hello_due_time;

    /** Neighbor count. */
    uint32_t                            neighbor_count;

    /** Adjacent neighbor count */
    uint32_t                            adj_neighbor_count;

    /** Transmit Delay */
    uint32_t                            transmit_delay;
} vtss_appl_ospf_interface_status_t;

/**
 * \brief Iterator through the interface in the ospf
 *
 * \param prev      [IN]    Ifindex to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          Currently CLI and Web use this iterator.
 * \return VTSS_OK if the operation is successful.
 */
mesa_rc vtss_appl_ospf_interface_itr(
    const vtss_ifindex_t        *const prev,
    vtss_ifindex_t              *const next
);

/**
 * \brief Iterator2 through the interface in the ospf
 *
 * When IN is NULL, assign the first index.
 * When IN is not NULL, assign the next index according to the given IN value.
 * Currently MIB uses this iterator.
 *
 * \param current_addr    [IN]   Current interface address.
 * \param current_ifidx   [IN]   Current interface ifindex.
 * \param next_addr       [OUT]  Next interface address.
 * \param next_ifidx      [OUT]  Next interface ifindex.
 * \return VTSS_OK if the operation is successful.
 */
mesa_rc vtss_appl_ospf_interface_itr2(
        const mesa_ipv4_t *const current_addr,
        mesa_ipv4_t *const next_addr,
        const vtss_ifindex_t *const current_ifidx,
        vtss_ifindex_t *const next_ifidx);


/**
 * \brief Get status for a specific interface.
 * \param ifindex   [IN] Ifindex to query.
 * \param status    [OUT] Status for 'key'.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_interface_status_get(
        const vtss_ifindex_t                   ifindex,
        vtss_appl_ospf_interface_status_t      *const status);

/**
 * \brief Get status for all route.
 * \param interface [IN] An empty container.
 * \param interface [OUT] An container with all interface status.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_interface_status_get_all(
        vtss::Map<vtss_ifindex_t,
        vtss_appl_ospf_interface_status_t> &interface);

//----------------------------------------------------------------------------
//** OSPF neighbor status
//----------------------------------------------------------------------------
/**
 * \brief The OSPF options field is present in OSPF Hello packets, which
 *        enables OSPF routers to support (or not support) optional capabilities,
 *        and to communicate their capability level to other OSPF routers.
 *
 *        RFC5250 provides a description of each capability. See
 *        https://tools.ietf.org/html/rfc5250#appendix-A for a detail
 *        information.
 */
enum {
    VTSS_APPL_OSPF_OPTION_FIELD_MT       = (1 << 0), /**< Control MT-bit  */
    VTSS_APPL_OSPF_OPTION_FIELD_E        = (1 << 1), /**< Control E-bit  */
    VTSS_APPL_OSPF_OPTION_FIELD_MC       = (1 << 2), /**< Control MC-bit */
    VTSS_APPL_OSPF_OPTION_FIELD_NP       = (1 << 3), /**< Control NP-bit */
    VTSS_APPL_OSPF_OPTION_FIELD_EA       = (1 << 4), /**< Control EA-bit */
    VTSS_APPL_OSPF_OPTION_FIELD_DC       = (1 << 5), /**< Control DC-bit */
    VTSS_APPL_OSPF_OPTION_FIELD_O        = (1 << 6), /**< Control O-bit */
    VTSS_APPL_OSPF_OPTION_FIELD_DN       = (1 << 7)  /**< Control DN-bit */
};

/** \brief The neighbor state of the OSPF. */
typedef enum {
    VTSS_APPL_OSPF_NEIGHBOR_STATE_DEPENDUPON = 0,   /**< Depend Upon state */
    VTSS_APPL_OSPF_NEIGHBOR_STATE_DELETED = 1,      /**< Deleted state */
    VTSS_APPL_OSPF_NEIGHBOR_STATE_DOWN = 2,         /**< Down state */
    VTSS_APPL_OSPF_NEIGHBOR_STATE_ATTEMPT = 3,      /**< Attempt state */
    VTSS_APPL_OSPF_NEIGHBOR_STATE_INIT = 4,         /**< Init state */
    VTSS_APPL_OSPF_NEIGHBOR_STATE_2WAY = 5,         /**< 2-Way state */
    VTSS_APPL_OSPF_NEIGHBOR_STATE_EXSTART = 6,      /**< ExStart state */
    VTSS_APPL_OSPF_NEIGHBOR_STATE_EXCHANGE = 7,     /**< Exchange state */
    VTSS_APPL_OSPF_NEIGHBOR_STATE_LOADING = 8,      /**< Loading state */
    VTSS_APPL_OSPF_NEIGHBOR_STATE_FULL = 9,         /**< Full state */
    VTSS_APPL_OSPF_NEIGHBOR_STATE_UNKNOWN           /**< Unknown state */
} vtss_appl_ospf_neighbor_state_t;

/** \brief The data structure for the OSPF neighbor information. */
typedef struct {
    /** The IP address. */
    mesa_ipv4_t                         ip_addr;
    /** Neighbor's router ID. */
    vtss_appl_ospf_router_id_t          neighbor_id;
    /** Area ID */
    vtss_appl_ospf_area_id_t            area_id;
    /** Interface index */
    vtss_ifindex_t                      ifindex;
    /** The OSPF neighbor priority */
    uint8_t                             priority;
    /** Neighbor state */
    vtss_appl_ospf_neighbor_state_t     state;
    /** The router ID of DR. */
    mesa_ipv4_t                         dr_id;
    /** The IP address of DR. */
    mesa_ipv4_t                         dr_addr;
    /** The router ID of BDR. */
    mesa_ipv4_t                         bdr_id;
    /** The IP address of BDR. */
    mesa_ipv4_t                         bdr_addr;
    /** The option field which is present in OSPF hello packets */
    uint8_t                             options;
    /** Dead timer. */
    uint32_t                            dead_time;
    /** Transit Area ID */
    vtss_appl_ospf_area_id_t            transit_id;
} vtss_appl_ospf_neighbor_status_t;

/** \brief The data structure combining the OSPF neighbor's keys and status for
  * get_all container. */
typedef struct {
        /** OSPF instance ID. */
        vtss_appl_ospf_id_t              id;
        /** Neighbor id to query. */
        vtss_appl_ospf_router_id_t       neighbor_id;
        /** Neighbor IP to query. */
        mesa_ipv4_t                      neighbor_ip;
        /** Neighbor ifindex to query. */
        vtss_ifindex_t                   neighbor_ifidx;
        /** The OSPF neighbor information. */
        vtss_appl_ospf_neighbor_status_t status;
} vtss_appl_ospf_neighbor_data_t;

/**
 * \brief Get neighbor ID by neighbor IP address.
 * \param ip_addr   [IN] Neighbor IP address to query.
 * \return the Neighbor router ID or zero address if not found.
 */
vtss_appl_ospf_router_id_t vtss_appl_ospf_nbr_lookup_id_by_addr(const mesa_ipv4_t ip_addr);


/**
 * \brief Iterate through the neighbor entry.
 *  This funciton is used by standard MIB
 * \param current_id      [IN]  Current OSPF ID
 * \param next_id         [OUT] Next OSPF ID
 * \param current_nip     [IN]  Pointer to current neighbor IP.
 * \param next_nip        [OUT] Next neighbor IP.
 * \param current_ifidx   [IN]  Pointer to current neighbor ifindex.
 * \param next_ifidx      [OUT] Next neighbor ifindex.
 *                Provide a null pointer to get the first neighbor.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_neighbor_status_itr(
        const vtss_appl_ospf_id_t *const current_id,
        vtss_appl_ospf_id_t       *const next_id,
        const mesa_ipv4_t         *const current_nip,
        mesa_ipv4_t               *const next_nip,
        const vtss_ifindex_t      *const current_ifidx,
        vtss_ifindex_t            *const next_ifidx);

/**
 * \brief Iterate through the neighbor entry.
 *  This funciton is used by CLI and JSON/Private MIB
 * \param current_id      [IN]  Current OSPF ID
 * \param next_id         [OUT] Next OSPF ID
 * \param current_nid     [IN]  Pointer to current neighbor's router id.
 * \param next_nid        [OUT] Next neighbor's router id.
 * \param current_nip     [IN]  Pointer to current neighbor IP.
 * \param next_nip        [OUT] Next to next neighbor ip.
 * \param current_ifidx   [IN]  Pointer to current neighbor ifindex.
 * \param next_ifidx      [OUT] Next to next neighbor ifindex.
 *                Provide a null pointer to get the first neighbor.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_neighbor_status_itr2(
        const vtss_appl_ospf_id_t        *const current_id,
        vtss_appl_ospf_id_t              *const next_id,
        const vtss_appl_ospf_router_id_t *const current_nid,
        vtss_appl_ospf_router_id_t       *const next_nid,
        const mesa_ipv4_t                *const current_nip,
        mesa_ipv4_t                      *const next_nip,
        const vtss_ifindex_t             *const current_ifidx,
        vtss_ifindex_t                   *const next_ifidx);

/**
 * \brief Iterate through the neighbor entry.
 *  This funciton is used by Standard MIB - ospfVirtNbrTable (key: Transit Area and Router ID)
 * \param current_id                [IN]  Current OSPF ID.
 * \param next_id                   [OUT] Next OSPF ID.
 * \param current_transit_area_id   [IN]  Pointer to current transit area id.
 * \param next_transit_area_id      [OUT] Next entry transit area id.
 * \param current_nid               [IN]  Pointer to current neighbor id.
 * \param next_nid                  [OUT] Next entry neighbor id.
 * \param current_nip               [IN]  Pointer to current neighbor IP.
 * \param next_nip                  [OUT] Next entry neighbor IP.
 * \param current_ifidx             [IN]  Pointer to current neighbor ifindex.
 * \param next_ifidx                [OUT] Next entry neighbor ifindex.
 *                Provide a null pointer to get the first neighbor.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_neighbor_status_itr3(
        const vtss_appl_ospf_id_t *const current_id,
        vtss_appl_ospf_id_t *const next_id,
        const vtss_appl_ospf_area_id_t *const current_transit_area_id,
        vtss_appl_ospf_area_id_t *const next_transit_area_id,
        const vtss_appl_ospf_router_id_t *const current_nid,
        vtss_appl_ospf_router_id_t *const next_nid,
        const mesa_ipv4_t *const current_nip, mesa_ipv4_t *const next_nip,
        const vtss_ifindex_t *const current_ifidx,
        vtss_ifindex_t *const next_ifidx);

/** The search doesn't match neighbor's router ID.  */
#define VTSS_APPL_OSPF_DONTCARE_NID  (0)

/**
 * \brief Get status for a neighbor information.
 * \param id             [IN]  OSPF instance ID.
 * \param neighbor_id    [IN]  Neighbor's router ID.
 *  If the neighbor_id is VTSS_APPL_OSPF_DONTCARE_NID,
 *  it doesn't match the neighbor ID
 * \param neighbor_ip    [IN]  Neighbor's IP.
 * \param neighbor_ifidx [IN]  Neighbor's ifindex.
 * \param status         [OUT] Neighbor Status.
 * \return Error code.
 */
mesa_rc vtss_appl_ospf_neighbor_status_get(
        const vtss_appl_ospf_id_t           id,
        const vtss_appl_ospf_router_id_t    neighbor_id,
        const mesa_ipv4_t                   neighbor_ip,
        const vtss_ifindex_t                neighbor_ifidx,
        vtss_appl_ospf_neighbor_status_t    *const status);

/**
 * \brief Get status for all neighbor information.
 * \param neighbors [OUT] An container with all neighbor.
 * \return Error code.
  */
mesa_rc vtss_appl_ospf_neighbor_status_get_all(
        vtss::Vector<vtss_appl_ospf_neighbor_data_t>
        &neighbors);


#ifdef __cplusplus
}
#endif

#endif /* _VTSS_APPL_OSPF_H_ */
