/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

/**
 * \file
 * \brief Public HQoS API
 * \details This header file describes Hierarchical Quality of Service (HQoS) control functions and types.
 *
 * HQoS is enabled on a port when the port scheduling mode is set to hierarchical.
 *
 * HQoS parameters are configured using HQoS IDs.
 *
 * Traffic can be mapped to HQoS IDs by using the corresponding modules.
 */

#ifndef _VTSS_APPL_HQOS_H_
#define _VTSS_APPL_HQOS_H_

#include <mscc/ethernet/switch/api/types.h>
#include <vtss/appl/interface.h>
#include <vtss/basics/enum-descriptor.h>
#include <vtss/appl/qos.h>

extern const vtss_enum_descriptor_t hqos_sch_mode_txt[]; /**< Enum descriptor text */

/** \brief HQoS port configuration */
typedef struct {
    mesa_hqos_sch_mode_t sch_mode; /**< Scheduling mode */
} vtss_appl_hqos_port_conf_t;

/** \brief HQoS ID configuration */
typedef struct {
    uint8_t                          dwrr_cnt;      /**< Number of queues using DWRR */
    mesa_bool_t                        shaper_enable; /**< Enable shaper */
    mesa_bitrate_t              shaper_rate;   /**< Shaper rate in kbps */
    vtss_appl_qos_shaper_mode_t shaper_mode;   /*!< Shaper mode. Line or data rate. */
    mesa_bitrate_t              min_rate;      /**< Guaranteed bandwidth in kbps (set to 0 to disable) */
} vtss_appl_hqos_conf_t;


/** \brief HQoS ID queue configuration */
typedef struct {
    mesa_bool_t                        shaper_enable; /**< Enable shaper */
    mesa_bitrate_t              shaper_rate;   /**< Shaper rate in kbps */
    vtss_appl_qos_shaper_mode_t shaper_mode;   /*!< Shaper mode. Line or data rate. */
    mesa_pct_t                  queue_pct;     /**< Queue weight/percentage */
} vtss_appl_hqos_queue_conf_t;

#ifdef __cplusplus
extern "C" {
#endif

/****************************************************************************
 * Port configuration
 ****************************************************************************/

/**
 * \brief Get HQoS interface configuration.
 *
 * \param ifindex [IN]   The interface index.
 * \param conf    [OUT]  The interface configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_hqos_interface_conf_get(const vtss_ifindex_t ifindex,
                                          vtss_appl_hqos_port_conf_t *const conf);

/**
 * \brief Set HQoS interface configuration.
 *
 * \param ifindex [IN]  The interface index.
 * \param conf    [IN]  The interface configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_hqos_interface_conf_set(const vtss_ifindex_t ifindex,
                                          const vtss_appl_hqos_port_conf_t *const conf);

/****************************************************************************
 * (Port, HQoS) configuration
 ****************************************************************************/

/**
 * \brief Iterator for HQoS configuration
 *
 * \param prev_ifindex [IN]   The previous interface index or NULL.
 * \param next_ifindex [IN]   The next interface index.
 * \param prev_hqos_id [IN]   The previous HQoS ID or NULL.
 * \param next_hqos_id [OUT]  The next HQoS ID.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_hqos_itr(const vtss_ifindex_t *const prev_ifindex, vtss_ifindex_t *const next_ifindex,
                           const mesa_hqos_id_t *const prev_hqos_id, mesa_hqos_id_t *const next_hqos_id);

/**
 * \brief Get HQoS ID configuration.
 *
 * \param ifindex [IN]   The interface index.
 * \param hqos_id [IN]   The HQoS ID.
 * \param conf    [OUT]  The HQoS ID configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_hqos_conf_get(const vtss_ifindex_t ifindex,
                                const mesa_hqos_id_t hqos_id,
                                vtss_appl_hqos_conf_t *const conf);

/**
 * \brief Set HQoS ID configuration.
 *
 * \param ifindex [IN]  The interface index.
 * \param hqos_id [IN]  The HQoS ID.
 * \param conf    [IN]  The HQoS ID configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_hqos_conf_set(const vtss_ifindex_t ifindex,
                                const mesa_hqos_id_t hqos_id,
                                const vtss_appl_hqos_conf_t *const conf);

/**
 * \brief Delete HQoS ID configuration.
 *
 * \param ifindex [IN]  The interface index.
 * \param hqos_id [IN]  The HQoS ID.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_hqos_conf_del(const vtss_ifindex_t ifindex,
                                const mesa_hqos_id_t hqos_id);

/****************************************************************************
 * (Port, HQoS, Queue) configuration
 ****************************************************************************/

/**
 * \brief Iterator for HQoS ID and queue.
 *
 * \param prev_ifindex [IN]   The previous interface index or NULL.
 * \param next_ifindex [IN]   The next interface index.
 * \param prev_hqos_id [IN]   The previous HQoS ID or NULL.
 * \param next_hqos_id [OUT]  The next HQoS ID.
 * \param prev_queue   [IN]   The previous queue or NULL.
 * \param next_queue   [OUT]  The next queue.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_hqos_queue_itr(const vtss_ifindex_t *const prev_ifindex, vtss_ifindex_t *const next_ifindex,
                                 const mesa_hqos_id_t *const prev_hqos_id, mesa_hqos_id_t *const next_hqos_id,
                                 const mesa_prio_t *const prev_queue, mesa_prio_t *const next_queue);

/**
 * \brief Get HQoS ID queue configuration.
 *
 * \param ifindex [IN]   The interface index.
 * \param hqos_id [IN]   The HQoS ID.
 * \param queue   [IN]   The queue.
 * \param conf    [OUT]  The HQoS ID queue configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_hqos_queue_conf_get(const vtss_ifindex_t ifindex,
                                      const mesa_hqos_id_t hqos_id,
                                      const mesa_prio_t queue,
                                      vtss_appl_hqos_queue_conf_t *const conf);

/**
 * \brief Set HQoS ID queue configuration.
 *
 * \param ifindex [IN]   The interface index.
 * \param hqos_id [IN]   The HQoS ID.
 * \param queue   [IN]   The queue.
 * \param conf    [OUT]  The HQoS ID queue configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_hqos_queue_conf_set(const vtss_ifindex_t ifindex,
                                      const mesa_hqos_id_t hqos_id,
                                      const mesa_prio_t queue,
                                      const vtss_appl_hqos_queue_conf_t *const conf);

#ifdef __cplusplus
}
#endif

#endif /* _VTSS_APPL_HQOS_H_ */
