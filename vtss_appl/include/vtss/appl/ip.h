/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

/**
 * \file
 * \brief Public IP API
 * \details This header file describes IP control functions and types
 */

#ifndef _VTSS_APPL_IP_H_
#define _VTSS_APPL_IP_H_

#include <mscc/ethernet/switch/api/types.h>
#include <vtss/appl/interface.h>
#include <vtss/basics/enum-descriptor.h>
#include <vtss/basics/enum_macros.hxx>

#define VTSS_DHCP_OPTION_CODE_TIME_SERVER        4 /**< DHCP option code for time server */
#define VTSS_DHCP_OPTION_CODE_DOMAIN_NAME_SERVER 6 /**< DHCP option code for name server */

#ifdef __cplusplus
extern "C" {
#endif

/** \brief A collections of capability properties of the IP module. */
typedef struct {
    /** The device has IPv4 host capabilities. */
    mesa_bool_t has_ipv4_host_capabilities;

    /** The device has IPv6 host capabilities. */
    mesa_bool_t has_ipv6_host_capabilities;

    /** The device has IPv4 unicast routing capabilities. */
    mesa_bool_t has_ipv4_unicast_routing_capabilities;

    /** The device has IPv4 unicast hardware accelerated routing capabilities.
     * */
    mesa_bool_t has_ipv4_unicast_hw_routing_capabilities;

    /** The device has IPv6 unicast routing capabilities. */
    mesa_bool_t has_ipv6_unicast_routing_capabilities;

    /** The device has IPv6 unicast hardware accelerated routing capabilities.
     * */
    mesa_bool_t has_ipv6_unicast_hw_routing_capabilities;

    /** The maximum number of IP interfaces supported by the device. */
    uint32_t  max_number_of_ip_interfaces;

    /** The maximum number of static configured IP routes (shared for IPv4 and
     * IPv6) */
    uint32_t max_number_of_static_routes;

    /** The amount of hardware LPM entries. */
    uint32_t number_of_lpm_hardware_entries;
} vtss_appl_ip_capabilities_t;

/** \brief Global IP configuration. */
typedef struct {
    /** Enable routing for all interfaces. */
    mesa_bool_t enable_routing;
} vtss_appl_ip_global_param_t;


/** \brief Routing status flags */
typedef enum {
    /** Route is up. */
    VTSS_APPL_IP_ROUTE_STATUS_FLAG_UP      = 0x1,

    /** The target is a host. */
    VTSS_APPL_IP_ROUTE_STATUS_FLAG_HOST    = 0x2,

    /** The target is a gateway. */
    VTSS_APPL_IP_ROUTE_STATUS_FLAG_GATEWAY = 0x4,

    /** Host or net is unreachable. */
    VTSS_APPL_IP_ROUTE_STATUS_FLAG_REJECT  = 0x8,

    /** Route is installed in HW and HW routing is performed for non-management
     * traffic. */
    VTSS_APPL_IP_ROUTE_STATUS_FLAG_HW_RT   = 0x10,
} vtss_appl_ip_route_status_flags_t;


/** \brief Route owner */
typedef enum {
    /** Invalid. */
    VTSS_APPL_IP_ROUTE_OWNER_INVALID,

    /** Route has been installed by a routing protocol. */
    VTSS_APPL_IP_ROUTE_OWNER_DYNAMIC_USER,

    /** Route has been installed as part of the user configuration. */
    VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER,

    /** Route has been installed by a DHCP client. */
    VTSS_APPL_IP_ROUTE_OWNER_DHCP,
} vtss_appl_ip_route_owner_t;

/** \brief Flags to confiure the DHCPv4 client */
typedef enum {
    /** This value may be used to clear all flags. */
    VTSS_APPL_IP_DHCP_FLAG_NONE = 0x0,

    /** When transmitting the discover and request frames, option 43 (vendor
     * specific information) will be requested in option 55 (Parameter Request
     * List). */
    VTSS_APPL_IP_DHCP_FLAG_OPTION_43 = 0x1,

    /** When transmitting the discover and request frames option 60 (vendor
     * class identifier) will be included. The content of option 60 is
     * implementation depended. */
    VTSS_APPL_IP_DHCP_FLAG_OPTION_60 = 0x2,

    /** When transmitting the discover and request frames, option 67 (boot file
     * name) will be requested in option 55 (Parameter Request List). */
    VTSS_APPL_IP_DHCP_FLAG_OPTION_67 = 0x4,
} vtss_appl_ip_dhcp_flag_t;

#define VTSS_APPL_IP_DHCP_CLIENT_ID_MIN_LENGTH  (1)     /**< Minimum length of client identifier using the DHCP client. */
#if VTSS_APPL_IP_DHCP_CLIENT_ID_MIN_LENGTH < 1
#error "The DHCP Client-identifier must have one octet or more (Refer to RFC 2132 9.14. Client-identifier)" 
#endif
#define VTSS_APPL_IP_DHCP_CLIENT_ID_MAX_LENGTH  (32)    /**< Maximum length of client identifier using the DHCP client. */
#define VTSS_APPL_IP_DHCP_HOSTNAME_MAX_LENGTH   (64)    /**< Maximum length of hostname using the DHCP client. (including the null character) */

/** \brief Parameters to confiure the DHCPv4 client ID field */
typedef enum {
   VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_AUTO = 0,  /**< Default option (for backward compatible purpose), the value is hostname-sysmac[3]-sysmac[4]-sysmac[5] */
   VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_IFMAC,     /**< The client identifier is refer to interface MAC address */
   VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_ASCII,     /**< The client identifier is ASCII string */
   VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_HEX        /**< The client identifier is HEX value */
} vtss_appl_ip_dhcpc_client_id_type_t;

/** \brief Parameters to confiure the DHCPv4 client ID field */
typedef struct {
    /** Configure the type of DHCPv4 client ID. */
    vtss_appl_ip_dhcpc_client_id_type_t type;

    /** The interface's MAC address is taken when id_type = IFMAC */
    vtss_ifindex_t  if_mac;

    /** A unique ASCII string is taken when id_type = ASCII */
    char            ascii[VTSS_APPL_IP_DHCP_CLIENT_ID_MAX_LENGTH];

    /** A unique hexadecimal value is taken when id_type = HEX, one byte hex value is presented as two octets string(*2), one more octet for null character(+1) */
    char            hex[VTSS_APPL_IP_DHCP_CLIENT_ID_MAX_LENGTH * 2 + 1];
} vtss_appl_ip_dhcp_client_id_t;

/** \brief Parameters to confiure the DHCPv4 client */
typedef struct {
    /** Configure the behaivural of the DHCP client. */
    vtss_appl_ip_dhcp_flag_t   dhcpc_flags;

    /** Configure the hostname in the DHCP option 12 field. */
    char hostname[VTSS_APPL_IP_DHCP_HOSTNAME_MAX_LENGTH];

    /** Configure the client identiifier in the DHCP option 61 field.
     *  The interface specify which interface's MAC address is taken. */
    vtss_appl_ip_dhcp_client_id_t client_id;
} vtss_appl_ip_dhcp_param_t;

/** \brief IPv4 interface configuration. */
typedef struct {
    /** Activate the IPv4 protocol stack. */
    mesa_bool_t                       active;

    /** Configure the interface using the dhcp client. */
    mesa_bool_t                       dhcpc;

    /** Configure the parameters of the DHCP client. */
    vtss_appl_ip_dhcp_param_t  dhcpc_params;

    /** Static configured IP address and netmask. If the IPv4 protocol stack is
     *  enabled and the dhcp client is not enabled, then this address is used.
     *  If the dhcp client is enabled, then this address may be used as
     *  fallback. */
    mesa_ipv4_network_t        network;

    /** The dhcp client can be configured with a fallback timer. If no address
     *  has been provided before timeout, then the address configured in the
     * 'network' will be used as fallback. To disable the fallback mechanism
     *  configured this value to zero, and the dhcp client will quiring for an
     *  address. */
    uint32_t                        fallback_timeout;
} vtss_appl_ip_ipv4_conf_t;

/** \brief IPv4 route configuration */
typedef struct {
    /** The distance value for this route */
    uint32_t distance;
} vtss_appl_ip_ipv4_route_conf_t;

/** \brief IPv6 interface configuration. */
typedef struct {
    /** Activate the IPv6 protocol stack. */
    mesa_bool_t                active;

    /** Static configured IP address and netmask. */
    mesa_ipv6_network_t network;
} vtss_appl_ip_ipv6_conf_t;


/** \brief IPv6 route configuration */
typedef struct {
    /** Network, prefix and next-hop addresses. */
    mesa_ipv6_uc_t route;

    /** If the next-hop address in 'route' is a link local address, the
     * interface if the next-hop address must be specified here. Otherwise, this
     * must be set to zero.*/
    vtss_ifindex_t interface;
} vtss_appl_ip_ipv6_route_conf_t;

/** \brief DHCP IPc4 client state. The states INIT, SELECTING, REQUESTING,
 * REBINDING, BOUND and RENEW correspond to the states of same name show in
 * rfc2131 figure 5. */
typedef enum {
    /** The DHCP client has been stopped. */
    VTSS_APPL_IP_DHCP4C_STATE_STOPPED,

    /** The DHCP client is being initialized. */
    VTSS_APPL_IP_DHCP4C_STATE_INIT,

    /** The DHCP client has send a discover, and is now collecting offers. */
    VTSS_APPL_IP_DHCP4C_STATE_SELECTING,

    /** The DHCP client either accepts or reject the offers, and depending on
     * the action it moves on to INIT or BOUND. */
    VTSS_APPL_IP_DHCP4C_STATE_REQUESTING,

    /** The DHCP client is in the process of rebinding after a renew broadcast
     * request has been send. */
    VTSS_APPL_IP_DHCP4C_STATE_REBINDING,

    /** The DHCP client hold a valid and accepted offer from a DHCP server. */
    VTSS_APPL_IP_DHCP4C_STATE_BOUND,

    /** The offer is about to timeout, and DHCP client will send a unicast renew
     * request. */
    VTSS_APPL_IP_DHCP4C_STATE_RENEWING,

    /** The DHCP client has given up, and will not try query for DHCP servers
     * any more, instead the interface has been configured with a fallback
     * address. */
    VTSS_APPL_IP_DHCP4C_STATE_FALLBACK,

    /** The DHCP client hold a valid and accepted offer from a DHCP server, but
     * it is not applied yet as the ARP check is still not completed. */
    VTSS_APPL_IP_DHCP4C_STATE_BOUND_ARP_CHECK,
} vtss_appl_ip_dhcp4c_state_t;

/** \brief Neighbour (ARP) flags. */
typedef enum {
    /** The neighbour entry is valid. */
    VTSS_APPL_IP_NEIGHBOUR_FLAG_VALID      = 0x1,

    /** The neighbour entry is marked as permanent. */
    VTSS_APPL_IP_NEIGHBOUR_FLAG_PERMANENT  = 0x2,

    /** The neighbour is a router. */
    VTSS_APPL_IP_NEIGHBOUR_FLAG_ROUTER     = 0x4,

    /** The neighbour is present in the LPM table of a hardware router. */
    VTSS_APPL_IP_NEIGHBOUR_FLAG_HARDWARE   = 0x8,
} vtss_appl_ip_neighbour_flags_t;

/** \brief IPv6 neighbour state. */
typedef enum {
    /** TODO, Charles please update this. */
    VTSS_APPL_IP_IPV6_NEIGHBOUR_STATE_NOSTATE = 0,

    /** TODO, Charles please update this. */
    VTSS_APPL_IP_IPV6_NEIGHBOUR_STATE_INCOMPLETE = 1,

    /** TODO, Charles please update this. */
    VTSS_APPL_IP_IPV6_NEIGHBOUR_STATE_REACHABLE = 2,

    /** TODO, Charles please update this. */
    VTSS_APPL_IP_IPV6_NEIGHBOUR_STATE_STALE = 3,

    /** TODO, Charles please update this. */
    VTSS_APPL_IP_IPV6_NEIGHBOUR_STATE_DELAY = 4,

    /** TODO, Charles please update this. */
    VTSS_APPL_IP_IPV6_NEIGHBOUR_STATE_PROBE = 5,

    /** TODO, Charles please update this. */
    VTSS_APPL_IP_IPV6_NEIGHBOUR_STATE_ERROR = 100,
} vtss_appl_ip_ipv6_neighbour_state_t;


/** \brief Enum descriptor text */
extern const vtss_enum_descriptor_t vtss_appl_ip_dhcp4c_state_txt[];

/** \brief A utility function to translate a vtss_appl_ip_dhcp4c_state_t into text.
 * \param s [IN] vtss_appl_ip_dhcp4c_state_t
 * \return Text representation of s
 * */
const char *vtss_appl_ip_dhcp4c_state_to_txt(vtss_appl_ip_dhcp4c_state_t s);

/** \brief Selected fields from the DHCP offer PDU. */
typedef struct {
    /** IPv4 address and network (the offer given). */
    mesa_ipv4_network_t ip;

    /** MAC-address of the DHCP server. */
    mesa_mac_t  server_mac;

    /** IP-address of the DHCP server. */
    mesa_ipv4_t server_ip;

    /** Tells of the 'server_ip' field is valid. */
    mesa_bool_t        has_server_ip;

    /** Default gateway. */
    mesa_ipv4_t default_gateway;

    /** Tells of the 'default_gateway' field is valid. */
    mesa_bool_t        has_default_gateway;

    /** Domain name server. */
    mesa_ipv4_t domain_name_server;

    /** Tells of the 'domain_name_server' field is valid. */
    mesa_bool_t        has_domain_name_server;
} vtss_appl_ip_dhcp_fields_t;

/** \brief DHCP client state information. */
typedef struct {
    /** State of the DHCP clients state machine. */
    vtss_appl_ip_dhcp4c_state_t state;

    /** DHCP client server address. Only valid if state is
     * DHCP4C_STATE_REBINDING, DHCP4C_STATE_BOUND or DHCP4C_STATE_RENEWING. */
    mesa_ipv4_t server_ip;

} vtss_appl_ip_dhcp_client_status_t;

/** \brief Interface lower-layer flags */
typedef enum {
    /** No flags */
    VTSS_APPL_IP_IF_LINK_FLAG_NONE      =  0x0,

    /** Interface is up. */
    VTSS_APPL_IP_IF_LINK_FLAG_UP        =  0x1,

    /** Interface is capable of transmitting broadcast traffic. */
    VTSS_APPL_IP_IF_LINK_FLAG_BROADCAST =  0x2,

    /** Interface is a loop-back interface. */
    VTSS_APPL_IP_IF_LINK_FLAG_LOOPBACK  =  0x4,

    /** Interface is running (according to the operating system). */
    VTSS_APPL_IP_IF_LINK_FLAG_RUNNING   =  0x8,

    /** Indicates if the interface will answer to ARP requests. */
    VTSS_APPL_IP_IF_LINK_FLAG_NOARP     = 0x10,

    /** Indicates if the interface is in promisc mode. */
    VTSS_APPL_IP_IF_LINK_FLAG_PROMISC   = 0x20,

    /** Indicates if the interface supports multicast traffic. */
    VTSS_APPL_IP_IF_LINK_FLAG_MULTICAST = 0x40,

    /** Indicates the IPv6 Router Advertisement Managed flag. */
    VTSS_APPL_IP_IF_LINK_FLAG_IPV6_RA_MANAGED = 0x80,

    /** Indicates the IPv6 Router Advertisement Other flag. */
    VTSS_APPL_IP_IF_LINK_FLAG_IPV6_RA_OTHER = 0x100,
} vtss_appl_ip_if_link_flag_t;

/** \brief IPv6 IP address flags. */
typedef enum {
    /** None value - usefull for initializing. */
    VTSS_APPL_IP_IF_IPV6_FLAG_NONE       =   0x0,

    /** Indicates if the address is an any-cast address. */
    VTSS_APPL_IP_IF_IPV6_FLAG_ANYCAST    =   0x1,

    /** An address whose uniqueness on a link is being verified, prior to its
     * assignment to an interface.  A tentative address is not considered
     * assigned to an interface in the usual sense. */
    VTSS_APPL_IP_IF_IPV6_FLAG_TENTATIVE  =   0x2,

    /** Indicates the address duplication is detected by Duplicate Address
     * Detection (a.k.a. DAD).
     *
     * If the address is a link-local address formed from an interface
     * identifier based on the hardware address, which is supposed to be
     * uniquely assigned (e.g., EUI-64 for an Ethernet interface), IP operation
     * on the interface SHOULD be disabled. */
    VTSS_APPL_IP_IF_IPV6_FLAG_DUPLICATED =   0x4,

    /** Indicates this address is ready to be detached from the link (IPv6
     * network). */
    VTSS_APPL_IP_IF_IPV6_FLAG_DETACHED   =   0x8,

    /** An address assigned to an interface whose use is discouraged, but not
     * forbidden.  A deprecated address should no longer be used as a source
     * address in new communications, but packets sent from or to deprecated
     * addresses are delivered as expected.
     *
     * A deprecated address may continue to be used as a source address in
     * communications where switching to a preferred address causes hardship to
     * a specific upper-layer activity (e.g., an existing TCP connection) */
    VTSS_APPL_IP_IF_IPV6_FLAG_DEPRECATED =  0x10,

    /** Indicates this address does not perform Duplicate Address Detection
     * (a.k.a. DAD). */
    VTSS_APPL_IP_IF_IPV6_FLAG_NODAD      =  0x20,

    /** Indicates this address is capable of being retrieved by stateless
     * address Autoconfiguration. */
    VTSS_APPL_IP_IF_IPV6_FLAG_AUTOCONF   =  0x40,

    /** Indicates this address is a temporary address.  A temporary address is
     * used to reduce the prospect of a user identity being permanently tied to
     * an IPv6 address portion.  A IPv6 node may create temporary addresses with
     * interface identifiers based on time-varying random bit strings and
     * relatively short lifetimes (hours to days).  After that, they are
     * replaced with new addresses. */
    VTSS_APPL_IP_IF_IPV6_FLAG_TEMPORARY  =  0x80,

    /** Indicates this address is a DHCPv6 address. */
    VTSS_APPL_IP_IF_IPV6_FLAG_DHCP       = 0x100,

    /** Indicates this address is a static managed address. */
    VTSS_APPL_IP_IF_IPV6_FLAG_STATIC     = 0x200,

    /** Indicates this address is active. */
    VTSS_APPL_IP_IF_IPV6_FLAG_UP         = 0x10000,

    /** Indicates this address is working. */
    VTSS_APPL_IP_IF_IPV6_FLAG_RUNNING    = 0x80000
} vtss_appl_ip_if_ipv6_flag_t;

/** \brief Interface lower-layer status. */
typedef struct {
    /** Interface index used by the underlaying operating system. */
    uint32_t                 os_if_index;

    /** Maximum Transmission Unit size of the interface. */
    uint32_t                 mtu;

    /** MAC-address of the interface. */
    mesa_mac_t          mac;

    /** L2 broadcast address */
    mesa_mac_t          bcast;

    /** Interface flags. */
    vtss_appl_ip_if_link_flag_t flags;
} vtss_appl_ip_if_status_link_t;

/** \brief Interface lower-layer layer statistics. */
typedef struct {
    /** Number of packets received on the interface. */
    uint64_t in_packets;

    /** Number of packets send on the interface. */
    uint64_t out_packets;

    /** Number of bytes received on the interface. */
    uint64_t in_bytes;

    /** Number of packets send on the interface. */
    uint64_t out_bytes;

    /** Number multicast packets received on the interface. */
    uint64_t in_multicasts;

    /** Number of multicast packets transmitted on the interface. */
    uint64_t out_multicasts;

    /** Number of broadcast packets received on the interface. */
    uint64_t in_broadcasts;

    /** Number of packets transmitted on the interface. */
    uint64_t out_broadcasts;
} vtss_appl_ip_if_status_link_stat_t;

/** \brief IPv4 IP address information. */
typedef struct {
    /** Broadcast address used by the interface. */
    mesa_ipv4_t                 broadcast;

    /** Maximum re-assembly size. */
    uint32_t                         reasm_max_size;

    /** Arp retransmit time. */
    uint32_t                         arp_retransmit_time;
} vtss_appl_ip_if_ipv4_info_t;

/** \brief Interface IPv4 status. */
typedef struct {
    /** The actually IP address and prefix size used by the interface. */
    mesa_ipv4_network_t         net;

    /** IPv4 address information. */
    vtss_appl_ip_if_ipv4_info_t info;
} vtss_appl_ip_if_status_ipv4_t;

/** \brief Status for IPv4 Address Conflict Detection. */
typedef struct {
    /** IP address */
    mesa_ipv4_t    sip;

    /** Source MAC address of conflicting node. */
    mesa_mac_t     smac;
} vtss_appl_ip_ipv4_acd_status_key_t;

/** \brief Status for IPv4 Address Conflict Detection. */
typedef struct {
    /** IP Interface index where the conflict was detected. */
    vtss_ifindex_t interface;

    /** Interface index of port where the conflict was detected. */
    vtss_ifindex_t interface_port;
} vtss_appl_ip_ipv4_acd_status_t;

/** \brief IP statistics as defined by RFC 4293. */
typedef struct {
    /** The total number of input IP datagrams received, including those
     * received in error.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 InReceives;

    /** The total number of input IP datagrams received, including those
     * received in error.  This object counts the same datagrams as InReceives,
     * but allows for larger values.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint64_t                 HCInReceives;

    /** The total number of octets received in input IP datagrams, including
     * those received in error.  Octets from datagrams counted in InReceives
     * MUST be counted here.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 InOctets;

    /** The total number of octets received in input IP datagrams, including
     * those received in error.  This object counts the same octets as InOctets,
     * but allows for larger value.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint64_t                 HCInOctets;

    /** The number of input IP datagrams discarded due to errors in their IP
     * headers, including version number mismatch, other format errors, hop
     * count exceeded, errors discovered in processing their IP options, etc.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 InHdrErrors;

    /** The number of input IP datagrams discarded because no route could be
     * found to transmit them to their destination.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 InNoRoutes;

    /** The number of input IP datagrams discarded because the IP address in
     * their IP header's destination field was not a valid address to be
     * received at this entity.  This count includes invalid addresses (e.g.,
     * ::0).  For entities that are not IP routers and therefore do not forward
     * datagrams, this counter includes datagrams discarded because the
     * destination address was not a local address.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 InAddrErrors;

    /** The number of locally-addressed IP datagrams received successfully but
     * discarded because of an unknown or unsupported protocol.
     *
     * When tracking interface statistics, the counter of the interface to which
     * these datagrams were addressed is incremented.  This interface might not
     * be the same as the input interface for some of the datagrams.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 InUnknownProtos;

    /** The number of input IP datagrams discarded because the datagram frame
     * didn't carry enough data.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 InTruncatedPkts;

    /** The number of input datagrams for which this entity was not their final
     * IP destination and for which this entity attempted to find a route to
     * forward them to that final destination.
     *
     * When tracking interface statistics, the counter of the incoming interface
     * is incremented for each datagram.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 InForwDatagrams;

    /** The number of input datagrams for which this entity was not their final
     * IP destination and for which this entity attempted to find a route to
     * forward them to that final destination.  This object counts the same
     * packets as InForwDatagrams, but allows for larger values.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint64_t                 HCInForwDatagrams;

    /** The number of IP fragments received that needed to be reassembled at
     * this interface.
     *
     * When tracking interface statistics, the counter of the interface to which
     * these fragments were addressed is incremented.  This interface might not
     * be the same as the input interface for some of the fragments.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 ReasmReqds;

    /** The number of IP datagrams successfully reassembled.
     *
     * When tracking interface statistics, the counter of the interface to which
     * these datagrams were addressed is incremented.  This interface might not
     * be the same as the input interface for some of the datagrams.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 ReasmOKs;

    /** The number of failures detected by the IP re-assembly algorithm (for
     * whatever reason: timed out, errors, etc.).  Note that this is not
     * necessarily a count of discarded IP fragments since some algorithms
     * (notably the algorithm in RFC 815) can lose track of the number of
     * fragments by combining them as they are received.
     *
     * When tracking interface statistics, the counter of the interface to which
     * these fragments were addressed is incremented.  This interface might not
     * be the same as the input interface for some of the fragments.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 ReasmFails;

    /** The number of input IP datagrams for which no problems were encountered
     * to prevent their continued processing, but were discarded (e.g., for lack
     * of buffer space).  Note that this counter does not include any datagrams
     * discarded while awaiting re-assembly.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 InDiscards;

    /** The total number of datagrams successfully delivered to IP
     * user-protocols (including ICMP).
     *
     * When tracking interface statistics, the counter of the interface to which
     * these datagrams were addressed is incremented.  This interface might not
     * be the same as the input interface for some of the datagrams.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 InDelivers;

    /** The total number of datagrams successfully delivered to IP
     * user-protocols (including ICMP).  This object counts the same packets as
     * ipSystemStatsInDelivers, but allows for larger values.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint64_t                 HCInDelivers;

    /** The total number of IP datagrams that local IP user-protocols (including
     * ICMP) supplied to IP in requests for transmission.  Note that this
     * counter does not include any datagrams counted in OutForwDatagrams.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 OutRequests;

    /** The total number of IP datagrams that local IP user-protocols (including
     * ICMP) supplied to IP in requests for transmission.  This object counts
     * the same packets as OutRequests, but allows for larger values.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint64_t                 HCOutRequests;

    /** The number of locally generated IP datagrams discarded because no route
     * could be found to transmit them to their destination.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 OutNoRoutes;

    /** The number of datagrams for which this entity was not their final IP
     * destination and for which it was successful in finding a path to their
     * final destination.
     *
     * When tracking interface statistics, the counter of the outgoing interface
     * is incremented for a successfully forwarded datagram.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 OutForwDatagrams;

    /** The number of datagrams for which this entity was not their final IP
     * destination and for which it was successful in finding a path to their
     * final destination.  This object counts the same packets as
     * OutForwDatagrams, but allows for larger values.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint64_t                 HCOutForwDatagrams;

    /** The number of output IP datagrams for which no problem was encountered
     * to prevent their transmission to their destination, but were discarded
     * (e.g., for lack of buffer space).  Note that this counter would include
     * datagrams counted in OutForwDatagrams if any such datagrams met this
     * (discretionary) discard criterion.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 OutDiscards;

    /** The number of IP datagrams that would require fragmentation in order to
     * be transmitted.
     *
     * When tracking interface statistics, the counter of the outgoing interface
     * is incremented for a successfully fragmented datagram.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 OutFragReqds;

    /** The number of IP datagrams that have been successfully fragmented.
     *
     * When tracking interface statistics, the counter of the outgoing interface
     * is incremented for a successfully fragmented datagram.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 OutFragOKs;

    /** The number of IP datagrams that have been discarded because they needed
     * to be fragmented but could not be.  This includes IPv4 packets that have
     * the DF bit set and IPv6 packets that are being forwarded and exceed the
     * outgoing link MTU.
     *
     * When tracking interface statistics, the counter of the outgoing interface
     * is incremented for an unsuccessfully fragmented datagram.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 OutFragFails;

    /** The number of output datagram fragments that have been generated as a
     * result of IP fragmentation.
     *
     * When tracking interface statistics, the counter of the outgoing interface
     * is incremented for a successfully fragmented datagram.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 OutFragCreates;

    /** The total number of IP datagrams that this entity supplied to the lower
     * layers for transmission.  This includes datagrams generated locally and
     * those forwarded by this entity.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 OutTransmits;

    /** The total number of IP datagrams that this entity supplied to the lower
     * layers for transmission.  This object counts the same datagrams as
     * OutTransmits, but allows for larger values.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint64_t                 HCOutTransmits;

    /** The total number of octets in IP datagrams delivered to the lower layers
     * for transmission.  Octets from datagrams counted in OutTransmits MUST be
     * counted here.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 OutOctets;

    /** The total number of octets in IP datagrams delivered to the lower layers
     * for transmission.  This objects counts the same octets as OutOctets, but
     * allows for larger values.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint64_t                 HCOutOctets;

    /** The number of IP multicast datagrams received.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 InMcastPkts;

    /** The number of IP multicast datagrams received.  This object counts the
     * same datagrams as InMcastPkts but allows for larger values.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint64_t                 HCInMcastPkts;

    /** The total number of octets received in IP multicast datagrams.  Octets
     * from datagrams counted in InMcastPkts MUST be counted here.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 InMcastOctets;

    /** The total number of octets received in IP multicast datagrams.  This
     * object counts the same octets as InMcastOctets, but allows for larger
     * values.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint64_t                 HCInMcastOctets;

    /** The number of IP multicast datagrams transmitted.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 OutMcastPkts;

    /** The number of IP multicast datagrams transmitted.  This object counts
     * the same datagrams as OutMcastPkts, but allows for larger values.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint64_t                 HCOutMcastPkts;

    /** The total number of octets transmitted in IP multicast datagrams.
     * Octets from datagrams counted in OutMcastPkts MUST be counted here.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 OutMcastOctets;

    /** The total number of octets transmitted in IP multicast datagrams.  This
     * object counts the same octets as OutMcastOctets, but allows for larger
     * values.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint64_t                 HCOutMcastOctets;

    /** The number of IP broadcast datagrams received.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 InBcastPkts;

    /** The number of IP broadcast datagrams received.  This object counts the
     * same datagrams as InBcastPkts but allows for larger values.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint64_t                 HCInBcastPkts;

    /** The number of IP broadcast datagrams transmitted.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint32_t                 OutBcastPkts;

    /** The number of IP broadcast datagrams transmitted.  This object counts
     * the same datagrams as OutBcastPkts, but allows for larger values.
     *
     * Discontinuities in the value of this counter can occur at
     * re-initialization of the system, and at other times as indicated by the
     * value of DiscontinuityTime. */
    uint64_t                 HCOutBcastPkts;

    /** The value of sysUpTime on the most recent occasion at which any one or
     * more of this entry's counters suffered a discontinuity.
     *
     * If no such discontinuities have occurred since the last re-initialization
     * of the IP stack, then this object contains a zero value. */
    mesa_timestamp_t    DiscontinuityTime;

    /** The minimum reasonable polling interval for this entry.  This object
     * provides an indication of the minimum amount of time required to update
     * the counters in this entry. */
    uint32_t                 RefreshRate;

    /** Indicates if the InReceives field is valid */
    uint8_t                  InReceivesValid;

    /** Indicates if the HCInReceives field is valid */
    uint8_t                  HCInReceivesValid;

    /** Indicates if the InOctets field is valid */
    uint8_t                  InOctetsValid;

    /** Indicates if the HCInOctets field is valid */
    uint8_t                  HCInOctetsValid;

    /** Indicates if the InHdrErrors field is valid */
    uint8_t                  InHdrErrorsValid;

    /** Indicates if the InNoRoutes field is valid */
    uint8_t                  InNoRoutesValid;

    /** Indicates if the InAddrErrors field is valid */
    uint8_t                  InAddrErrorsValid;

    /** Indicates if the InUnknownProtos field is valid */
    uint8_t                  InUnknownProtosValid;

    /** Indicates if the InTruncatedPkts field is valid */
    uint8_t                  InTruncatedPktsValid;

    /** Indicates if the InForwDatagrams field is valid */
    uint8_t                  InForwDatagramsValid;

    /** Indicates if the HCInForwDatagrams field is valid */
    uint8_t                  HCInForwDatagramsValid;

    /** Indicates if the ReasmReqds field is valid */
    uint8_t                  ReasmReqdsValid;

    /** Indicates if the ReasmOKs field is valid */
    uint8_t                  ReasmOKsValid;

    /** Indicates if the ReasmFails field is valid */
    uint8_t                  ReasmFailsValid;

    /** Indicates if the InDiscards field is valid */
    uint8_t                  InDiscardsValid;

    /** Indicates if the InDelivers field is valid */
    uint8_t                  InDeliversValid;

    /** Indicates if the HCInDelivers field is valid */
    uint8_t                  HCInDeliversValid;

    /** Indicates if the OutRequests field is valid */
    uint8_t                  OutRequestsValid;

    /** Indicates if the HCOutRequests field is valid */
    uint8_t                  HCOutRequestsValid;

    /** Indicates if the OutNoRoutes field is valid */
    uint8_t                  OutNoRoutesValid;

    /** Indicates if the OutForwDatagrams field is valid */
    uint8_t                  OutForwDatagramsValid;

    /** Indicates if the HCOutForwDatagrams field is valid */
    uint8_t                  HCOutForwDatagramsValid;

    /** Indicates if the OutDiscards field is valid */
    uint8_t                  OutDiscardsValid;

    /** Indicates if the OutFragReqds field is valid */
    uint8_t                  OutFragReqdsValid;

    /** Indicates if the OutFragOKs field is valid */
    uint8_t                  OutFragOKsValid;

    /** Indicates if the OutFragFails field is valid */
    uint8_t                  OutFragFailsValid;

    /** Indicates if the OutFragCreates field is valid */
    uint8_t                  OutFragCreatesValid;

    /** Indicates if the OutTransmits field is valid */
    uint8_t                  OutTransmitsValid;

    /** Indicates if the HCOutTransmits field is valid */
    uint8_t                  HCOutTransmitsValid;

    /** Indicates if the OutOctets field is valid */
    uint8_t                  OutOctetsValid;

    /** Indicates if the HCOutOctets field is valid */
    uint8_t                  HCOutOctetsValid;

    /** Indicates if the InMcastPkts field is valid */
    uint8_t                  InMcastPktsValid;

    /** Indicates if the HCInMcastPkts field is valid */
    uint8_t                  HCInMcastPktsValid;

    /** Indicates if the InMcastOctets field is valid */
    uint8_t                  InMcastOctetsValid;

    /** Indicates if the HCInMcastOctets field is valid */
    uint8_t                  HCInMcastOctetsValid;

    /** Indicates if the OutMcastPkts field is valid */
    uint8_t                  OutMcastPktsValid;

    /** Indicates if the HCOutMcastPkts field is valid */
    uint8_t                  HCOutMcastPktsValid;

    /** Indicates if the OutMcastOctets field is valid */
    uint8_t                  OutMcastOctetsValid;

    /** Indicates if the HCOutMcastOctets field is valid */
    uint8_t                  HCOutMcastOctetsValid;

    /** Indicates if the InBcastPkts field is valid */
    uint8_t                  InBcastPktsValid;

    /** Indicates if the HCInBcastPkts field is valid */
    uint8_t                  HCInBcastPktsValid;

    /** Indicates if the OutBcastPkts field is valid */
    uint8_t                  OutBcastPktsValid;

    /** Indicates if the HCOutBcastPkts field is valid */
    uint8_t                  HCOutBcastPktsValid;

    /** Indicates if the DiscontinuityTime field is valid */
    uint8_t                  DiscontinuityTimeValid;

    /** Indicates if the RefreshRate field is valid */
    uint8_t                  RefreshRateValid;
} vtss_appl_ip_if_status_ip_stat_t;

/** \brief IPv6 IP address information. */
typedef struct {
    /** IPv6 address flags. */
    vtss_appl_ip_if_ipv6_flag_t flags;

    /** Interface index used by the underlaying operating system. */
    uint32_t                         os_if_index;
} vtss_appl_ip_if_ipv6_info_t;

/** \brief Interface IPv6 status. */
typedef struct {
    /** IPv6 address and network. */
    mesa_ipv6_network_t         net;

    /** IPv6 address information. */
    vtss_appl_ip_if_ipv6_info_t info;
} vtss_appl_ip_if_status_ipv6_t;


/** \brief IP route status. */
typedef struct {
    /** Next hop interface. */
    vtss_ifindex_t             next_hop_interface;

    /** Route flags. */
    vtss_appl_ip_route_status_flags_t flags;

    /** Owners of the route. */
    uint32_t                        owners;

    /** See ipDefaultRouterLifetime in RFC4293. */
    int                        lifetime;

    /** See ipDefaultRouterPreference in RFC4293 */
    int                        preference;
} vtss_appl_ip_route_status_t;

/** \brief IPv4 route and status. */
typedef struct {
    /** IPv4 route. */
    mesa_ipv4_uc_t                 key;

    /** Statues associated with the route. */
    vtss_appl_ip_route_status_t    val;
} vtss_appl_ip_ipv4_route_key_status_t;

/** \brief IPv6 route and status. */
typedef struct {
    /** IPv6 route. */
    vtss_appl_ip_ipv6_route_conf_t key;

    /** Status associated with the route. */
    vtss_appl_ip_route_status_t    val;
} vtss_appl_ip_ipv6_route_key_status_t;

/** \brief A collection of global actions triggers. */
typedef struct {
    /** Clear the IPv4 neighbour table. */
    mesa_bool_t ipv4_neighbour_table_clear;

    /** Clear the IPv6 neighbour table. */
    mesa_bool_t ipv6_neighbour_table_clear;

    /** Clear the IPv4 system statistics. */
    mesa_bool_t ipv4_system_statistics_clear;

    /** Clear the IPv6 system statistics. */
    mesa_bool_t ipv6_system_statistics_clear;

    /** Clear the IPv4 ACD status table. */
    mesa_bool_t ipv4_acd_status_clear;
} vtss_appl_ip_global_actions_t;

/** \brief Alias. See 'vtss_appl_ip_dhcp_client_status_t' */
typedef vtss_appl_ip_dhcp_client_status_t vtss_appl_ip_if_status_dhcp4c_t;

/** \brief IPv4 neighbour status. */
typedef struct {
    /** MAC address of the neighbour. */
    mesa_mac_t     mac_address;

    /** Interface index of the interface where the neighbour can be reached. */
    vtss_ifindex_t interface;

    /** Neighbour flags. */
    vtss_appl_ip_neighbour_flags_t flags;
} vtss_appl_ip_ipv4_neighbour_status_t;

/** \brief IPv4 neighbour IP address and status information. */
typedef struct {
    /** IPv4 address of the neighbour */
    mesa_ipv4_t ip_address;

    /** Neighbour status information. */
    vtss_appl_ip_ipv4_neighbour_status_t status;
} vtss_appl_ip_ipv4_neighbour_status_pair_t;

/** \brief IPv6 neighbour status key. */
typedef struct {
    /** IP address of the IPv6 host in the neighbour table. */
    mesa_ipv6_t ip_address;

    /** If the host is a link-local then the interface where the host can be
     * reached must be specified here. Otherwise, this must be set to zero. */
    vtss_ifindex_t interface;
} vtss_appl_ip_ipv6_neighbour_status_key_t;

/** \brief IPv6 neighbour status value. */
typedef struct {
    /** MAC address of the neighbour. */
    mesa_mac_t     mac_address;

    /** Interface index of the interface where the neighbour can be reached. */
    vtss_ifindex_t interface;

    /** Neighbour flags. */
    vtss_appl_ip_neighbour_flags_t flags;

    /** IPv6 neighbour state. */
    vtss_appl_ip_ipv6_neighbour_state_t state;
} vtss_appl_ip_ipv6_neighbour_status_value_t;

/** \brief IPv6 neighbour status pair. */
typedef struct {
    /** IPv6 address of the neighbour. */
    vtss_appl_ip_ipv6_neighbour_status_key_t key;

    /** IPv6 status information. */
    vtss_appl_ip_ipv6_neighbour_status_value_t status;
} vtss_appl_ip_ipv6_neighbour_status_pair_t;


/* Global configuration ---------------------------------------------------- */


/**
 * \brief Get the capabilities of the device.
 * \param c [OUT] Buffer to receive the result in.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_global_capabilities_get(vtss_appl_ip_capabilities_t *c);

/**
 * \brief Set global IP settings.
 * \param p [IN] Global parameters
 * \return Error code.
 */
mesa_rc vtss_appl_ip_global_param_set(
        const vtss_appl_ip_global_param_t *const p);

/**
 * \brief Get global IP settings.
 * \param p [OUT] Global parameters
 * \return Error code.
 */
mesa_rc vtss_appl_ip_global_param_get(
        vtss_appl_ip_global_param_t       *const p);

/**
 * \brief Iterate through all IP interfaces.
 * \param in [IN]   Pointer to current interface index. Provide a null pointer
 *                  to get the first interface.
 * \param out [OUT] Next interface index (relative to the value provided in
 *                  'in').
 * \return Error code. VTSS_RC_OK means that the value in out is valid,
 *                     VTSS_RC_ERROR means that no "next" interface index exists
 *                     and the end has been reached.
 */
mesa_rc vtss_appl_ip_if_conf_itr(
        const vtss_ifindex_t            *const in,
        vtss_ifindex_t                  *const out);

/**
 * \brief Iterate through all IPv4 interfaces status.
 * \param ifidx_in [IN]   Pointer to current interface index. Provide a null pointer
 *                        to get the first instance of IPv4 interface status.
 * \param ifidx_out [OUT] Next interface index (relative to the value provided in 'ifidx_in').
 * \param addr_in [IN]    Pointer to current interface address. Provide a null pointer
 *                        to get the first instance of IPv4 interface status.
 * \param addr_out [OUT]  Next interface address (relative to the value provided in 'addr_in').
 * \return Error code. VTSS_RC_OK means that the values in 'out' are valid,
 *                     VTSS_RC_ERROR means that no "next" interface index and its corresponding
 *                     IPv4 network address exists and the end has been reached.
 */
mesa_rc vtss_appl_ip_if_status_ipv4_itr(const vtss_ifindex_t      *ifidx_in,
                                        vtss_ifindex_t            *ifidx_out,
                                        const mesa_ipv4_network_t *addr_in,
                                        mesa_ipv4_network_t       *addr_out);

/**
 * \brief Iterate through all IPv6 interfaces status.
 * \param ifidx_in [IN]   Pointer to current interface index. Provide a null pointer
 *                        to get the first instance of IPv6 interface status.
 * \param ifidx_out [OUT] Next interface index (relative to the value provided in 'ifidx_in').
 * \param addr_in [IN]    Pointer to current interface address. Provide a null pointer
 *                        to get the first instance of IPv6 interface status.
 * \param addr_out [OUT]  Next interface address (relative to the value provided in 'addr_in').
 * \return Error code. VTSS_RC_OK means that the values in 'out' are valid,
 *                     VTSS_RC_ERROR means that no "next" interface index and its corresponding
 *                     IPv6 network address exists and the end has been reached.
 */
mesa_rc vtss_appl_ip_if_status_ipv6_itr(const vtss_ifindex_t      *ifidx_in,
                                        vtss_ifindex_t            *ifidx_out,
                                        const mesa_ipv6_network_t *addr_in,
                                        mesa_ipv6_network_t       *addr_out);

/**
 * \brief Clear IP system statistics counters.
 * \param version [IN] IPv4 or Ipv6
 * \return Error code.
 */
mesa_rc vtss_appl_ip_system_statistics_clear(mesa_ip_type_t version);

/**
 * \brief Global IP controls.
 * \param a [IN] struct of actions.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_global_controls(
        const vtss_appl_ip_global_actions_t *const a);

/**
 * \brief Clear the neighbour table of a given type.
 * \param type [IN] Chose IPv4 or IPv6 table.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_neighbour_clear(mesa_ip_type_t type);

/* Interface functions ----------------------------------------------------- */

/**
 * \brief Get ip interface configurations.
 * Currently no general configuration is associated with an IP interface (IPv4
 * and IPv6 configuration is handled else where), and this function can therefor
 * be considered as a dummy. It will return VTSS_RC_OK if the interface exists,
 * and otherwise return VTSS_RC_ERROR.
 * \param ifidx [IN] Interface index.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_if_conf_get(vtss_ifindex_t ifidx);

/**
 * \brief Add/update an IP interface.
 * If an IP interface does not exists for the given interface index, then create
 * one, otherwise do nothing. Return VTSS_RC_OK if the IP interface already
 * exists or was created successfully, otherwise an error is returned.
 * \param ifidx [IN] Interface index.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_if_conf_set(vtss_ifindex_t ifidx);

/**
 * \brief Delete an existing IP interface.
 * \param ifidx [IN] Interface index.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_if_conf_del(vtss_ifindex_t ifidx);

/**
 * \brief Query a specific interface for link-layer status information.
 * \param ifidx [IN] Interface index to query.
 * \param s [OUT]    Buffer to receive the status information in.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_if_status_link(
        vtss_ifindex_t ifidx,
        vtss_appl_ip_if_status_link_t *const s);

/**
 * \brief Query a specific interface for IPv4 status information.
 * \param ifidx [IN]  Interface index to query.
 * \param addr  [IN]  Interface address to query.
 * \param s     [OUT] Buffer to receive the status information in.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_if_status_ipv4(vtss_ifindex_t               ifidx,
                                    const mesa_ipv4_network_t   *addr,
                                    vtss_appl_ip_if_ipv4_info_t *const s);


/**
 * \brief Query a specific interface for DHCP (IPv4) client status information.
 * \param ifidx [IN] Interface index to query.
 * \param s [OUT] buffer to receive the status information in.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_if_status_dhcpc_v4(
        vtss_ifindex_t ifidx,
        vtss_appl_ip_if_status_dhcp4c_t *const s);

/**
 * \brief Query a specific interface for IPv6 status information.
 * \param ifidx [IN]  Interface index to query.
 * \param addr  [IN]  Interface address to query.
 * \param s     [OUT] Buffer to receive the status information in.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_if_status_ipv6(vtss_ifindex_t               ifidx,
                                    const mesa_ipv6_network_t   *addr,
                                    vtss_appl_ip_if_ipv6_info_t *const s);

/**
 * \brief Query a specific interface for lower-layer statistics.
 * \param ifidx [IN] Interface index to query.
 * \param s [OUT] buffer to receive the status information in.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_if_statistics_link(
        vtss_ifindex_t ifidx,
        vtss_appl_ip_if_status_link_stat_t *const s);

/**
 * \brief Query a specific interface for IPv4 statistics.
 * \param ifidx [IN] Interface index to query.
 * \param s [OUT] buffer to receive the status information in.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_if_statistics_ipv4(
        vtss_ifindex_t ifidx,
        vtss_appl_ip_if_status_ip_stat_t *const s);

/**
 * \brief Query a specific interface for IPv6 statistics.
 * \param ifidx [IN] Interface index to query.
 * \param s [OUT] buffer to receive the status information in.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_if_statistics_ipv6(
        vtss_ifindex_t ifidx,
        vtss_appl_ip_if_status_ip_stat_t *const s);

/**
 * \brief Clear interface counters.
 * \param ifidx [IN] Interface index.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_if_statistics_link_clear(vtss_ifindex_t ifidx);

/**
 * \brief Clear interface IPv4 counters.
 * \param ifidx [IN] Interface index.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_if_statistics_ipv4_clear(vtss_ifindex_t ifidx);

/**
 * \brief Clear interface IPv6 counters.
 * \param ifidx [IN] Interface index.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_if_statistics_ipv6_clear(vtss_ifindex_t ifidx);


/* IPv4 functions ---------------------------------------------------------- */

/**
 * \brief Get system IPv4 statistics.
 * \param s [OUT] Output parameter.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_system_statistics_ipv4_get(
        vtss_appl_ip_if_status_ip_stat_t *const s);

/**
 * \brief Get the IPv4 configuration for a given IP interface.
 * \param ifidx [IN] Interface index.
 * \param conf [OUT] IPv4 configuration.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv4_conf_get(
        vtss_ifindex_t                  ifidx,
        vtss_appl_ip_ipv4_conf_t       *const conf);

/**
 * \brief Set/update the IPv4 configuration for a given IP interface.
 * \param ifidx [IN] Interface index.
 * \param conf [IN]  IPv4 configuration.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv4_conf_set(
        vtss_ifindex_t                  ifidx,
        const vtss_appl_ip_ipv4_conf_t *const conf);

/**
 * \brief Iterate through all IPv4 routes.
 * \param in [IN]   Pointer to current route. Provide a null pointer to get the
 *                  first interface.
 * \param out [OUT] Next route (relative to the value provided in 'in').
 * \return Error code. VTSS_RC_OK means that the value in out is valid,
 *                     VTSS_RC_ERROR means that no "next" interface index exists
 *                     and the end has been reached.
 */
mesa_rc vtss_appl_ip_ipv4_route_conf_itr(
        const mesa_ipv4_uc_t  *const in,
        mesa_ipv4_uc_t        *const out);

/**
 * \brief Test if a route exists.
 * \param rt [IN] Route to lookup.
 * \param conf [OUT] IPv4 route configuration.
 * \return Error code. VTSS_RC_OK if the route exists, otherwise VTSS_RC_ERROR.
 */
mesa_rc vtss_appl_ip_ipv4_route_conf_get(
        const mesa_ipv4_uc_t  *const rt,
        vtss_appl_ip_ipv4_route_conf_t *const conf);

/**
 * \brief Add a new route.
 * \param rt [IN] Route to add.
 * \param conf [IN] IPv4 route configuration.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv4_route_conf_set(
        const mesa_ipv4_uc_t  *const rt,
        const vtss_appl_ip_ipv4_route_conf_t *const conf);

/**
 * \brief Delete an existing route.
 * \param rt [IN] Route to delete.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv4_route_conf_del(
        const mesa_ipv4_uc_t  *const rt);

/**
 * \brief Iterate through the routes in the routing table.
 * \param in [IN] Pointer to current route. Provide a null pointer to get the
 *                first interface.
 * \param out [OUT] Next route (relative to the value provided in 'in').
 * \return Error code. VTSS_RC_OK means that the value in out is valid,
 *         VTSS_RC_ERROR means that no "next" interface index exists and the end
 *         has been reached.
 */
mesa_rc vtss_appl_ip_ipv4_route_status_itr(
        const mesa_ipv4_uc_t  *const in,
        mesa_ipv4_uc_t        *const out);

/**
 * \brief Get status for a specific route.
 * \param rt [IN] Route to query.
 * \param st [OUT] Status for 'rt'.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv4_route_status_get(
        const mesa_ipv4_uc_t        *const rt,
        vtss_appl_ip_route_status_t *const st);

/**
 * \brief Get status for all routes.
 * \param max [IN] Max number of routes which may be written to '*list'.
 * \param list [OUT] The output buffer where the result is written.
 * \param cnt [OUT] Number of routes written in '*list'.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv4_route_status_get_list(
        uint32_t                                   max,
        vtss_appl_ip_ipv4_route_key_status_t *list,
        uint32_t                                  *const cnt);

/**
 * \brief Restart the DHCP process.
 * \param ifidx [IN] Interface to restart the DHCP process on.
 * \param action [IN] True means restart, false is ignored.
 * \return Return code.
 */
mesa_rc vtss_appl_ip_ipv4_dhcp_client_control_restart(
        vtss_ifindex_t                        ifidx,
        mesa_bool_t                                  action);

/**
 * \brief Get neighbour status for a given IPv4 address.
 * \param ip [IN] IP address to get status for.
 * \param status [OUT] Neighbour status for the given IPv4 address.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv4_neighbour_status_get(
        mesa_ipv4_t                           ip,
        vtss_appl_ip_ipv4_neighbour_status_t *const status);

/**
 * \brief Iterator to iterate through all the IPv4 neighbours.
 * \param in [IN] Pointer to the current route. Provide a NULL pointer to get
 *                the first IPv4 address present in the neighbour table.
 * \param out [ONT] Next IPv4 address present in the neighbour table.
 * \return VTSS_RC_OK if a next IP address was found, otherwise VTSS_RC_ERROR is
 *         returned to signal end of table.
 */
mesa_rc vtss_appl_ip_ipv4_neighbour_status_itr(
        const mesa_ipv4_t                    *const in,
        mesa_ipv4_t                          *const out);

/**
 * \brief Get the entire Neighbour table.
 * \param max [IN] The amount of entries which may be written to the status
 *                 array.
 * \param cnt [OUT] The amount of entries which was written to the status array.
 * \param status [OUT] A pointer to a buffer which can hold the result.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv4_neighbour_status_get_list(
        uint32_t                                        max,
        uint32_t                                       *cnt,
        vtss_appl_ip_ipv4_neighbour_status_pair_t *status);

/**
 * \brief Iterator to iterate through all the IPv4 Address Conflict entries.
 *
 * \param in  [IN]  The current entry key or NULL.
 * \param out [OUT] The next entry key.
 *
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv4_acd_status_itr(
        const vtss_appl_ip_ipv4_acd_status_key_t *const in,
        vtss_appl_ip_ipv4_acd_status_key_t       *const out);

/**
 * \brief Get IPv4 Address Conflict entry.
 *
 * \param key    [IN]  Entry key.
 * \param status [OUT] Entry status.
 *
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv4_acd_status_get(
        const vtss_appl_ip_ipv4_acd_status_key_t *const key,
        vtss_appl_ip_ipv4_acd_status_t *const status);

/* IPv6 functions ---------------------------------------------------------- */

/**
 * \brief Get system IPv6 statistics.
 * \param s [OUT] Output parameter.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_system_statistics_ipv6_get(
        vtss_appl_ip_if_status_ip_stat_t *const s);

/**
 * \brief Get the IPv6 configuration for a given IP interface.
 * \param ifidx [IN] Interface index.
 * \param conf [OUT] IPv6 configuration.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv6_conf_get(
        vtss_ifindex_t                  ifidx,
        vtss_appl_ip_ipv6_conf_t       *const conf);

/**
 * \brief Set/update the IPv6 configuration for a given IP interface.
 * \param ifidx [IN] Interface index.
 * \param conf [IN]  IPv6 configuration.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv6_conf_set(
        vtss_ifindex_t                  ifidx,
        const vtss_appl_ip_ipv6_conf_t *const conf);

/**
 * \brief Iterate through all the configured IPv6 routes.
 * \param in [IN] Previous route, or a null pointer to get the first route.
 * \param out [OUT] Next route.
 * \return Error code. An error is returned to signal end-of-table.
 */
mesa_rc vtss_appl_ip_ipv6_route_conf_itr(
        const vtss_appl_ip_ipv6_route_conf_t  *const in,
        vtss_appl_ip_ipv6_route_conf_t        *const out);

/**
 * \brief Test is a given IPv6 route exists
 * \param rt [IN] Route to query.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv6_route_conf_get(
        const vtss_appl_ip_ipv6_route_conf_t  *const rt);

/**
 * \brief Add a new IPv6 route.
 * \param rt [IN] Route to add.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv6_route_conf_set(
        const vtss_appl_ip_ipv6_route_conf_t  *const rt);

/**
 * \brief Delete an existing route.
 * \param rt [IN] Route to delete.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv6_route_conf_del(
        const vtss_appl_ip_ipv6_route_conf_t  *const rt);

/**
 * \brief Iterate through the routes in the routing table.
 * \param in [IN] Pointer to current route. Provide a null pointer to get the
 *                first interface.
 * \param out [OUT] Next route (relative to the value provided in 'in').
 * \return Error code. VTSS_RC_OK means that the value in out is valid,
 *                     VTSS_RC_ERROR means that no "next" interface index exists
 *                     and the end has been reached.
 */
mesa_rc vtss_appl_ip_ipv6_route_status_itr(
        const vtss_appl_ip_ipv6_route_conf_t *const in,
        vtss_appl_ip_ipv6_route_conf_t       *const out);

/**
 * \brief Get status for a specific route.
 * \param rt [IN] Route to query.
 * \param st [OUT] Status for 'rt'.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv6_route_status_get(
        const vtss_appl_ip_ipv6_route_conf_t *const rt,
        vtss_appl_ip_route_status_t          *const st);

/**
 * \brief Get status for all routes.
 * \param max [IN] Max number of routes which may be written to '*list'.
 * \param list [OUT] The output buffer where the result is written.
 * \param cnt [OUT] Number of routes written in '*list'.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv6_route_status_get_list(
        uint32_t                                   max,
        vtss_appl_ip_ipv6_route_key_status_t *list,
        uint32_t                                  *const cnt);

/**
 * \brief Get neighbour status for a given IPv6 address.
 * \param ip [IN] IP address to get status for.
 * \param status [OUT] Neighbour status for the given IPv6 address.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv6_neighbour_status_get(
        const vtss_appl_ip_ipv6_neighbour_status_key_t *const ip,
        vtss_appl_ip_ipv6_neighbour_status_value_t     *const status);

/**
 * \brief Iterator to iterate through all the IPv6 neighbours.
 * \param in [IN] Pointer to the current route. Provide a NULL pointer to get
 *                the first IPv6 address present in the neighbour table.
 * \param out [ONT] Next IPv6 address present in the neighbour table.
 * \return VTSS_RC_OK if a next IP address was found, otherwise VTSS_RC_ERROR is
 *         returned to signal end of table.
 */
mesa_rc vtss_appl_ip_ipv6_neighbour_status_itr(
        const vtss_appl_ip_ipv6_neighbour_status_key_t *const in,
        vtss_appl_ip_ipv6_neighbour_status_key_t       *const out);

/**
 * \brief Get the entire Neighbour table.
 * \param max [IN] The amount of entries which may be written to the status
 *                 array.
 * \param cnt [OUT] The amount of entries which was written to the status array.
 * \param status [OUT] A pointer to a buffer which can hold the result.
 * \return Error code.
 */
mesa_rc vtss_appl_ip_ipv6_neighbour_status_get_list(
        uint32_t                                        max,
        uint32_t                                       *cnt,
        vtss_appl_ip_ipv6_neighbour_status_pair_t *status);

#ifdef __cplusplus
}
#endif

VTSS_ENUM_BITWISE(vtss_appl_ip_route_status_flags_t); /**< Operators for route status flags */
VTSS_ENUM_BITWISE(vtss_appl_ip_if_ipv6_flag_t);       /**< Operators for IPv6 address flags */

#endif  // _VTSS_APPL_IP_H_
