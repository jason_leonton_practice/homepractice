/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

/**
* \file
* \brief Public IPMC MVR API
* \details This header file describes IPMC MVR control functions and types.
*/

#ifndef _VTSS_APPL_IPMC_MVR_H_
#define _VTSS_APPL_IPMC_MVR_H_

#include <vtss/appl/interface.h>
#include <vtss/appl/types.h>
#include <vtss/appl/ipmc_lib_public.h>
#include <vtss/appl/ipmc_profile.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef VTSS_SW_OPTION_SMB_IPMC
/*! \brief MVR filtering action type */
typedef enum {
    VTSS_APPL_MVR_ACTION_DENY = 0,  /**< MVR will perform deny operation against matched source address */
    VTSS_APPL_MVR_ACTION_PERMIT     /**< MVR will perform permit operation against matched source address */
} vtss_appl_mvr_action_t;

/**
 * \brief IPMC MVR global configuration
 * The configuration is the system configuration that can manage
 * the IPMC MVR functions.
 */
typedef struct {
    /**
     * \brief Global administrative mode, TRUE is to enable IPMC MVR
     * function in the system and FALSE is to disable it.
     */
    mesa_bool_t                            mode;
} vtss_appl_ipmc_mvr_global_t;

/**
 * \brief IPMC MVR port configuration
 * The configuration is the per-port basis configuration that can enhance
 * the IPMC MVR functions.
 */
typedef struct {
    /**
     * \brief do_immediate_leave is used to denote the specific port to block
     * the multicast traffic upon receiving the leave/done IPMC control messages.
     */
    mesa_bool_t                            do_immediate_leave;
} vtss_appl_ipmc_mvr_port_conf_t;

/**
 * \brief IPMC MVR interface configuration
 * The configuration is the interface configuration for running
 * the IPMC MVR functions.
 */
typedef struct {
    /**
     * \brief name is used to assign the name of MVR interface that
     * user could easily associate the MVR VLAN with its purpose.
     */
    vtss_appl_ipmc_name_index_t     name;

    /**
     * \brief querier_election is used to denote whether the specific MVR
     * interface joins the Querier election or not.
     */
    mesa_bool_t                     querier_election;

    /**
     * \brief igmp_querier_address is used to denote the static IPv4 source
     * address of the specific MVR interface for seding IGMP Query message
     * with respect to IGMP Querier election.
     */
    mesa_ipv4_t                     igmp_querier_address;

    /**
     * \brief mode is used to denote the way how MVR is working for IPMC
     * group learning from MVR sources.
     */
    vtss_appl_ipmc_mvr_intf_mode_t  mode;

    /**
     * \brief tagging is used for controlling the VLAN tagging of IPMC
     * control frames to be sent in MVR VLAN.
     */
    vtss_appl_ipmc_intf_vtag_t      tagging;

    /**
     * \brief priority is used for prioritizing the IPMC control frames to be
     * sent for MVR.
     */
    uint8_t                              priority;

    /**
     * \brief last_listener_query_interval presents the last listener/member
     * query interval for MVR interface running IPMC protocol while snooping
     * IPMC control frames.
     */
    uint32_t                             last_listener_query_interval;

    /**
     * \brief channel_profile is used to assign the IPMC profile for filtering
     * multicast group registration for this MVR interface.
     */
    vtss_appl_ipmc_name_index_t     channel_profile;
} vtss_appl_ipmc_mvr_interface_t;

/**
 * \brief IPMC MVR per-interface port configuration
 * The configuration is the port configuration for a MVR interface in running
 * IPMC MVR functions.
 */
typedef struct {
    /**
     * \brief role presents functionality that a specific port should take for
     * a specific MVR VLAN's operations.
     */
    vtss_appl_ipmc_mvr_port_role_t  role;
} vtss_appl_ipmc_mvr_intf_port_t;

/**
 * \brief IPMC MVR group address registration count
 * The multicast group address registration count from IPMC MVR.
 */
typedef struct {
    /**
     * \brief mvr_igmp_grps is used to denote the total number of registered
     * IGMP multicast group address from IPMC MVR.
     */
    uint32_t                             mvr_igmp_grps;

    /**
     * \brief mvr_mld_grps is used to denote the total number of registered
     * MLD multicast group address from IPMC MVR.
     */
    uint32_t                             mvr_mld_grps;
} vtss_appl_ipmc_mvr_grp_adrs_cnt_t;

/**
 * \brief IPMC MVR interface status for IGMP
 * The IPMC MVR interface parameters, timers, counters, and protocol status
 * from the running IPMC MVR IPv4 functions.
 */
typedef struct {
    /**
     * \brief Interface Querier state is used to denote the current MVR
     * interface's Querier state based on Querier election defined in protocol.
     */
    vtss_appl_ipmc_querier_states_t querier_state;

    /**
     * \brief active_querier_address is used to denote the IPv4 address of the
     * active IGMP Querier for a running MVR interface.
     */
    mesa_ipv4_t                     active_querier_address;

    /**
     * \brief querier_up_time presents the in operation timer for the specific
     * interface act as an IGMP Querier.
     */
    uint32_t                             querier_up_time;

    /**
     * \brief query_interval presents the "Query Interval", as stated in
     * RFC-3376 8.2, for a MVR interface running IGMP protocol.
     */
    uint32_t                             query_interval;

    /**
     * \brief startup_query_count presents the "Startup Query Count", as stated
     * in RFC-3376 8.7, for a MVR interface running IGMP protocol.
     */
    uint32_t                             startup_query_count;

    /**
     * \brief querier_expiry_time presents the "Other Querier Present Interval",
     * as stated in RFC-3376 8.5, for a MVR interface running IGMP protocol.
     */
    uint32_t                             querier_expiry_time;

    /**
     * \brief counter_tx_query presents the current packet count on the specific
     * MVR interface for transmitting IGMP Query control frames.
     */
    uint32_t                             counter_tx_query;

    /**
     * \brief counter_tx_specific_query presents the current packet count on the
     * specific MVR interface for transmitting IGMP Specific Query control frames.
     */
    uint32_t                             counter_tx_specific_query;

    /**
     * \brief counter_rx_query presents the current packet count on the specific
     * MVR interface for receiving IGMP Query control frames.
     */
    uint32_t                             counter_rx_query;

    /**
     * \brief counter_rx_v1_join presents the current packet count on the specific
     * MVR interface for receiving IGMPv1 Join control frames.
     */
    uint32_t                             counter_rx_v1_join;

    /**
     * \brief counter_rx_v2_join presents the current packet count on the specific
     * MVR interface for receiving IGMPv2 Join control frames.
     */
    uint32_t                             counter_rx_v2_join;

    /**
     * \brief counter_rx_v2_leave presents the current packet count on the specific
     * MVR interface for receiving IGMPv2 Leave control frames.
     */
    uint32_t                             counter_rx_v2_leave;

    /**
     * \brief counter_rx_v3_join presents the current packet count on the specific
     * MVR interface for receiving IGMPv3 Join control frames.
     */
    uint32_t                             counter_rx_v3_join;

    /**
     * \brief counter_rx_errors presents the current packet count on the specific
     * MVR interface for receiving invalid IGMP control frames.
     */
    uint32_t                             counter_rx_errors;
} vtss_appl_ipmc_mvr_intf_ipv4_status_t;

/**
 * \brief IPMC MVR interface status for MLD
 * The IPMC MVR interface parameters, timers, counters, and protocol status
 * from the running IPMC MVR IPv6 functions.
 */
typedef struct {
    /**
     * \brief Interface Querier state is used to denote the current MVR
     * interface's MLD Querier state based on Querier election defined in protocol.
     */
    vtss_appl_ipmc_querier_states_t querier_state;

    /**
     * \brief active_querier_address is used to denote the IPv6 address of the
     * active MLD Querier for a running MVR interface.
     */
    mesa_ipv6_t                     active_querier_address;

    /**
     * \brief querier_up_time presents the in operation timer for the specific
     * interface act as a MLD Querier.
     */
    uint32_t                             querier_up_time;

    /**
     * \brief query_interval presents the "Query Interval", as stated in
     * RFC-3810 9.2, for a MVR interface running MLD protocol.
     */
    uint32_t                             query_interval;

    /**
     * \brief startup_query_count presents the "Startup Query Count", as stated
     * in RFC-3810 9.7, for a MVR interface running MLD protocol.
     */
    uint32_t                             startup_query_count;

    /**
     * \brief querier_expiry_time presents the "Other Querier Present Interval",
     * as stated in RFC-3810 9.5, for a MVR interface running MLD protocol.
     */
    uint32_t                             querier_expiry_time;

    /**
     * \brief counter_tx_query presents the current packet count on the specific
     * MVR interface for transmitting MLD Query control frames.
     */
    uint32_t                             counter_tx_query;

    /**
     * \brief counter_tx_specific_query presents the current packet count on the
     * specific MVR interface for transmitting MLD Specific Query control frames.
     */
    uint32_t                             counter_tx_specific_query;

    /**
     * \brief counter_rx_query presents the current packet count on the specific
     * MVR interface for receiving MLD Query control frames.
     */
    uint32_t                             counter_rx_query;

    /**
     * \brief counter_rx_v1_report presents the current packet count on the specific
     * MVR interface for receiving MLDv1 Report control frames.
     */
    uint32_t                             counter_rx_v1_report;

    /**
     * \brief counter_rx_v1_done presents the current packet count on the specific
     * MVR interface for receiving MLDv1 Done control frames.
     */
    uint32_t                             counter_rx_v1_done;

    /**
     * \brief counter_rx_v2_report presents the current packet count on the specific
     * MVR interface for receiving MLDv2 Report control frames.
     */
    uint32_t                             counter_rx_v2_report;

    /**
     * \brief counter_rx_errors presents the current packet count on the specific
     * MVR interface for receiving invalid MLD control frames.
     */
    uint32_t                             counter_rx_errors;
} vtss_appl_ipmc_mvr_intf_ipv6_status_t;

/**
 * \brief IPMC MVR group address registration status
 * The IPMC multicast group address registration status from IPMC MVR.
 */
typedef struct {
    /**
     * \brief member_ports is used to denote the memberships of the registered
     * IPMC multicast group address from IPMC MVR.
     */
    vtss_port_list_stackable_t      member_ports;

    /**
     * \brief hardware_switch is used to denote whether the IPMC multicast
     * traffic destined to the registered group address could be forwarding
     * by switch hardware or not.
     */
    mesa_bool_t                            hardware_switch;
} vtss_appl_ipmc_mvr_grp_address_t;

/**
 * \brief IPMC MVR source list status in group address registration
 * The IPMC multicast group address with source list registration status
 * from IPMC MVR.
 */
typedef struct {
    /**
     * \brief group_filter_mode is used to denote the source filtering mode
     * of the specific registered multicast group address from IPMC MVR.
     */
    vtss_appl_ipmc_sfm_mode_t       group_filter_mode;

    /**
     * \brief filter_timer is used to count down the timer for the specific multicast
     * group's filtering mode transition.
     */
    uint32_t                             filter_timer;

    /**
     * \brief source_type is used to denote the filtering type of the specific source
     * address in multicasting to the registered group address.
     */
    vtss_appl_mvr_action_t          source_type;

    /**
     * \brief source_timer is used to count down the timer for purging the specific
     * source address from the registered multicast group's source list.
     */
    uint32_t                             source_timer;

    /**
     * \brief hardware_switch is used to denote whether the traffic destined to
     * the multicast group address from the specific source address could be
     * forwarding by switch hardware or not.
     */
    mesa_bool_t                            hardware_switch;
} vtss_appl_ipmc_mvr_grp_srclist_t;

/**
 * \brief Get IPMC MVR Global Parameters
 *
 * To read current system parameters in IPMC MVR.
 *
 * \param conf      [OUT]    The IPMC MVR system configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_system_config_get(
    vtss_appl_ipmc_mvr_global_t                 *const conf
);

/**
 * \brief Set IPMC MVR Global Parameters
 *
 * To modify current system parameters in IPMC MVR.
 *
 * \param conf      [IN]     The IPMC MVR system configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_system_config_set(
    const vtss_appl_ipmc_mvr_global_t           *const conf
);

/**
 * \brief Iterator for retrieving IPMC MVR port information key/index
 *
 * To walk information (configuration and status) index of the port in IPMC MVR.
 *
 * \param prev      [IN]    Interface index to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_general_port_itr(
    const vtss_ifindex_t                        *const prev,
    vtss_ifindex_t                              *const next
);

/**
 * \brief Get IPMC MVR specific port configuration
 *
 * To get configuration of the specific port in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 *
 * \param entry     [OUT]   The current configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_port_config_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_mvr_port_conf_t              *const entry
);

/**
 * \brief Set IPMC MVR specific port configuration
 *
 * To modify configuration of the specific port in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 * \param entry     [IN]    The revised configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_port_config_set(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_appl_ipmc_mvr_port_conf_t        *const entry
);

/**
 * \brief Iterator for retrieving IPMC MVR VLAN information key/index
 *
 * To walk information (configuration and status) index of the VLAN in IPMC MVR.
 *
 * \param prev      [IN]    Interface index to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_vlan_general_itr(
    const vtss_ifindex_t                        *const prev,
    vtss_ifindex_t                              *const next
);

/**
 * \brief Get IPMC MVR specific VLAN configuration
 *
 * To get configuration of the specific VLAN in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The current configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_vlan_config_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_mvr_interface_t              *const entry
);

/**
 * \brief Set IPMC MVR specific VLAN configuration
 *
 * To modify configuration of the specific VLAN in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param entry     [IN]    The revised configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_vlan_config_set(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_appl_ipmc_mvr_interface_t        *const entry
);

/**
 * \brief Delete IPMC MVR specific VLAN configuration
 *
 * To delete configuration of the specific VLAN in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_vlan_config_del(
    const vtss_ifindex_t                        *const ifindex
);

/**
 * \brief Add IPMC MVR specific VLAN configuration
 *
 * To add configuration of the specific VLAN in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param entry     [IN]    The new configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_vlan_config_add(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_appl_ipmc_mvr_interface_t        *const entry
);

/**
 * \brief Iterator for retrieving IPMC MVR per VLAN's port information key/index
 *
 * To walk information (configuration and status) index of the port in a IPMC MVR VLAN.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param port_prev [IN]    Ifindex of port to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param port_next [OUT]   The key/index of port's Ifindex should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_intf_port_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const vtss_ifindex_t                        *const port_prev,
    vtss_ifindex_t                              *const port_next
);

/**
 * \brief Get IPMC MVR VLAN's specific port configuration
 *
 * To get configuration of the specific port in an IPMC MVR VLAN.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param stkport   [IN]    (key) Interface index - the logical interface index of the physical port.
 *
 * \param entry     [OUT]   The current configuration of the port in a MVR VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_intf_port_config_get(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_ifindex_t                        *const stkport,
    vtss_appl_ipmc_mvr_intf_port_t              *const entry
);

/**
 * \brief Set IPMC MVR VLAN's specific port configuration
 *
 * To modify configuration of the specific port in an IPMC MVR VLAN.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param stkport   [IN]    (key) Interface index - the logical interface index of the physical port.
 * \param entry     [IN]    The revised configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_intf_port_config_set(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_ifindex_t                        *const stkport,
    const vtss_appl_ipmc_mvr_intf_port_t        *const entry
);

/**
 * \brief Get current IPMC MVR multicast group registration count
 *
 * To read the entry count of registered multicast group address from IPMC MVR.
 *
 * \param grp_cnt   [OUT]   The current registered multicast group address count.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_grp_adr_cnt_get(
    vtss_appl_ipmc_mvr_grp_adrs_cnt_t           *const grp_cnt
);

/**
 * \brief Get IPMC MVR specific VLAN interface's IGMP status
 *
 * To get interface's IGMP status of the specific VLAN in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The current configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_igmp_vlan_status_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_mvr_intf_ipv4_status_t       *const entry
);

/**
 * \brief Iterator for retrieving IPMC MVR IPv4 group address table key/index
 *
 * To walk indexes of the IPv4 group address table in IPMC MVR.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param grps_prev [IN]    IPv4 Address of multicast group to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param grps_next [OUT]   The key/index of multicast group address should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_igmp_group_address_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const mesa_ipv4_t                           *const grps_prev,
    mesa_ipv4_t                                 *const grps_next
);

/**
 * \brief Get IPMC MVR specific registered IPv4 multicast address on a VLAN
 *
 * To get registered IPv4 multicast group address data of the specific VLAN in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param grpadrs   [IN]    (key) IPv4 multicast group address.
 *
 * \param entry     [OUT]   The current status of the registered multicast group address.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_igmp_group_address_get(
    const vtss_ifindex_t                        *const ifindex,
    const mesa_ipv4_t                           *const grpadrs,
    vtss_appl_ipmc_mvr_grp_address_t            *const entry
);

/**
 * \brief Iterator for retrieving IPMC MVR IPv4 group source list table key/index
 *
 * To walk indexes of the IPv4 group source list table in IPMC MVR.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param grps_prev [IN]    IPv4 Address of multicast group to be used for indexing determination.
 * \param port_prev [IN]    Ifindex of port to be used for indexing determination.
 * \param adrs_prev [IN]    IPv4 Address of multicasting source to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param grps_next [OUT]   The key/index of multicast group address should be used for the GET operation.
 * \param port_next [OUT]   The key/index of port's Ifindex should be used for the GET operation.
 * \param adrs_next [OUT]   The key/index of multicasting source address should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_igmp_group_srclist_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const mesa_ipv4_t                           *const grps_prev,
    mesa_ipv4_t                                 *const grps_next,
    const vtss_ifindex_t                        *const port_prev,
    vtss_ifindex_t                              *const port_next,
    const mesa_ipv4_t                           *const adrs_prev,
    mesa_ipv4_t                                 *const adrs_next
);

/**
 * \brief Get IPMC MVR specific registered IPv4 multicast address and its source list entry
 *
 * To get registered IPv4 multicast group source list data of the specific VLAN and port in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param grpadrs   [IN]    (key) IPv4 multicast group address.
 * \param stkport   [IN]    (key) Interface index - the logical interface index of the port.
 * \param srcadrs   [IN]    (key) Multicasting IPv4 source address.
 *
 * \param entry     [OUT]   The current status of the registered IPv4 multicast group source list entry.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_igmp_group_srclist_get(
    const vtss_ifindex_t                        *const ifindex,
    const mesa_ipv4_t                           *const grpadrs,
    const vtss_ifindex_t                        *const stkport,
    const mesa_ipv4_t                           *const srcadrs,
    vtss_appl_ipmc_mvr_grp_srclist_t            *const entry
);

/**
 * \brief Get IPMC MVR specific VLAN interface's MLD status
 *
 * To get interface's MLD status of the specific VLAN in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The current configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_mld_vlan_status_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_mvr_intf_ipv6_status_t       *const entry
);

/**
 * \brief Iterator for retrieving IPMC MVR IPv6 group address table key/index
 *
 * To walk indexes of the IPv4 group address table in IPMC MVR.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param grps_prev [IN]    IPv6 Address of multicast group to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param grps_next [OUT]   The key/index of multicast group address should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_mld_group_address_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const mesa_ipv6_t                           *const grps_prev,
    mesa_ipv6_t                                 *const grps_next
);

/**
 * \brief Get IPMC MVR specific registered IPv6 multicast address on a VLAN
 *
 * To get registered IPv6 multicast group address data of the specific VLAN in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param grpadrs   [IN]    (key) IPv6 multicast group address.
 *
 * \param entry     [OUT]   The current status of the registered multicast group address.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_mld_group_address_get(
    const vtss_ifindex_t                        *const ifindex,
    const mesa_ipv6_t                           *const grpadrs,
    vtss_appl_ipmc_mvr_grp_address_t            *const entry
);

/**
 * \brief Iterator for retrieving IPMC MVR IPv6 group source list table key/index
 *
 * To walk indexes of the IPv6 group source list table in IPMC MVR.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param grps_prev [IN]    IPv6 Address of multicast group to be used for indexing determination.
 * \param port_prev [IN]    Ifindex of port to be used for indexing determination.
 * \param adrs_prev [IN]    IPv6 Address of multicasting source to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param grps_next [OUT]   The key/index of multicast group address should be used for the GET operation.
 * \param port_next [OUT]   The key/index of port's Ifindex should be used for the GET operation.
 * \param adrs_next [OUT]   The key/index of multicasting source address should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_mld_group_srclist_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const mesa_ipv6_t                           *const grps_prev,
    mesa_ipv6_t                                 *const grps_next,
    const vtss_ifindex_t                        *const port_prev,
    vtss_ifindex_t                              *const port_next,
    const mesa_ipv6_t                           *const adrs_prev,
    mesa_ipv6_t                                 *const adrs_next
);

/**
 * \brief Get IPMC MVR specific registered IPv6 multicast address and its source list entry
 *
 * To get registered IPv6 multicast group source list data of the specific VLAN and port in IPMC MVR.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param grpadrs   [IN]    (key) IPv6 multicast group address.
 * \param stkport   [IN]    (key) Interface index - the logical interface index of the port.
 * \param srcadrs   [IN]    (key) Multicasting IPv6 source address.
 *
 * \param entry     [OUT]   The current status of the registered multicast group source list entry.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_mld_group_srclist_get(
    const vtss_ifindex_t                        *const ifindex,
    const mesa_ipv6_t                           *const grpadrs,
    const vtss_ifindex_t                        *const stkport,
    const mesa_ipv6_t                           *const srcadrs,
    vtss_appl_ipmc_mvr_grp_srclist_t            *const entry
);

/**
 * \brief IPMC MVR Control ACTION for clearing statistics
 *
 * To clear the statistics based on the given VLAN ifIndex of MVR interface.
 *
 * \param ifindex   [IN]    Interface index - the logical interface index of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_mvr_statistics_clear_by_vlan_act(
    const vtss_ifindex_t                        *const ifindex
);
#endif /* VTSS_SW_OPTION_SMB_IPMC */

#ifdef __cplusplus
}
#endif

#endif  /* _VTSS_APPL_IPMC_MVR_H_ */
