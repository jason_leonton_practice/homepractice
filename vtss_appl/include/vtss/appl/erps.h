/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

/**
 * \file
 * \brief Public ERPS API
 * \details This header file describes ERPS control functions and types
 */

#ifndef _VTSS_APPL_ERPS_H_
#define _VTSS_APPL_ERPS_H_

#include <mscc/ethernet/switch/api.h>
#include <vtss/appl/interface.h>
#include <vtss/appl/vlan.h>
#include <vtss/appl/module_id.h>
#include <vtss/basics/enum-descriptor.h>

#ifdef __cplusplus
extern "C" {
#endif

/**  The length of the NODE ID, ITUT-G.8032(V2), Section 10.1.10 */
#define VTSS_APPL_ERPS_MAX_NODE_ID_LEN                  6

/**  Maximum number of configured ERPS groups */
#define VTSS_APPL_ERPS_MAX_PROTECTION_GROUPS            64

/**  Maximum number of protected VLANS per ERPS group */
#define VTSS_APPL_ERPS_PROTECTED_VLANS_MAX              63

/**  Error codes that can be generated by this module */   
enum {
    VTSS_APPL_ERPS_RC_OK = MESA_RC_OK,              // Note: Important identity, don't change
    VTSS_APPL_ERPS_ERROR_PG_CREATION_FAILED = MODULE_ERROR_START(VTSS_MODULE_ID_ERPS),
    VTSS_APPL_ERPS_ERROR_GROUP_NOT_EXISTS,
    VTSS_APPL_ERPS_ERROR_GROUP_ALREADY_EXISTS,
    VTSS_APPL_ERPS_ERROR_INVALID_PGID,
    VTSS_APPL_ERPS_ERROR_CANNOT_SET_RPL_OWNER_FOR_ACTIVE_GROUP,
    VTSS_APPL_ERPS_ERROR_PG_IN_INACTIVE_STATE,
    VTSS_APPL_ERPS_ERROR_NODE_ALREADY_RPL_OWNER,
    VTSS_APPL_ERPS_ERROR_NODE_NON_RPL,
    VTSS_APPL_ERPS_ERROR_SETTING_RPL_BLOCK,
    VTSS_APPL_ERPS_ERROR_VLANS_CANNOT_BE_ADDED,
    VTSS_APPL_ERPS_ERROR_VLAN_ALREADY_PARTOF_GROUP,
    VTSS_APPL_ERPS_ERROR_VLANS_CANNOT_BE_DELETED,
    VTSS_APPL_ERPS_ERROR_CONTROL_VID_PART_OF_ANOTHER_GROUP,
    VTSS_APPL_ERPS_ERROR_INVALID_REMOTE_EVENT,
    VTSS_APPL_ERPS_ERROR_INVALID_LOCAL_EVENT,
    VTSS_APPL_ERPS_ERROR_EAST_AND_WEST_ARE_SAME,
    VTSS_APPL_ERPS_ERROR_INCORRECT_ERPS_PDU_SIZE,
    VTSS_APPL_ERPS_ERROR_RAPS_PARSING_FAILED,
    VTSS_APPL_ERPS_ERROR_FAILED_IN_SETTING_VLANMAP,
    VTSS_APPL_ERPS_ERROR_MOVING_TO_NEXT_STATE,
    VTSS_APPL_ERPS_ERROR_CANNOT_ASSOCIATE_GROUP,
    VTSS_APPL_ERPS_ERROR_ALREADY_NEIGHBOUR_FOR_THISGROUP,
    VTSS_APPL_ERPS_ERROR_GROUP_IN_ACTIVE_MODE,
    VTSS_APPL_ERPS_ERROR_GIVEN_PORT_NOT_EAST_OR_WEST,
    VTSS_APPL_ERPS_ERROR_MAXIMUM_PVIDS_REACHED,
    VTSS_APPL_ERPS_ERROR_RAPS_ENABLE_FORWARDING_FAILED,
    VTSS_APPL_ERPS_ERROR_RAPS_DISABLE_FORWARDING_FAILED,
    VTSS_APPL_ERPS_ERROR_RAPS_DISABLE_RAPS_TX_FAILED,
    VTSS_APPL_ERPS_ERROR_RAPS_ENABLE_RAPS_TX_FAILED,
    VTSS_APPL_ERPS_ERROR_SF_DERIGISTER_FAILED,
    VTSS_APPL_ERPS_ERROR_SF_RIGISTER_FAILED,
    VTSS_APPL_ERPS_ERROR_MOVING_TO_FORWARDING,
    VTSS_APPL_ERPS_ERROR_MOVING_TO_DISCARDING,
    VTSS_APPL_ERPS_ERROR_CANTBE_IC_NODE,
    VTSS_APPL_ERPS_ERROR_NOT_AN_INTERCONNECTED_NODE,
    VTSS_APPL_ERPS_ERROR_CANNOTBE_INTERCONNECTED_SUB_NODE,
    VTSS_APPL_ERPS_ERROR_PRIORITY_LOGIC_FAILED,
    VTSS_APPL_ERPS_ERROR
};

/**  Enum describing the ERPS ring type  */
typedef enum {
    /** ERPS Major ring. ITUT-G.8032(V2), Section 3.2.5 */
    VTSS_APPL_ERPS_RING_TYPE_MAJOR = 1, 

    /** ERPS Sub ring.   ITUT-G.8032(V2), Section 3.2.11 */
    VTSS_APPL_ERPS_RING_TYPE_SUB
} vtss_appl_erps_ring_type_t;

/**  The ring type in a text format  */
extern const vtss_enum_descriptor_t vtss_appl_erps_ring_type_txt[];

/** Enum describing the administrator commands. ITUT-G.8032(V2), Section 8 */
typedef enum {
    /** In the absence of failure this cmd forces a block on the ring port */
    VTSS_APPL_ERPS_ADMIN_CMD_MANUAL_SWITCH = 1,

    /** This cmd forces a block on the ring port */
    VTSS_APPL_ERPS_ADMIN_CMD_FORCED_SWITCH,

    /** Clears forced or manual switch, and triggers reversion */
    VTSS_APPL_ERPS_ADMIN_CMD_CLEAR
} vtss_appl_erps_admin_cmd_t;

/**  The admin command in a text format  */
extern const vtss_enum_descriptor_t vtss_appl_erps_admin_cmd_txt[];

/** Enum describing the ERPS version */
typedef enum {
    /** ERPS version 1 */
    VTSS_APPL_ERPS_VERSION_V1 = 1, 

    /** ERPS version 2 */
    VTSS_APPL_ERPS_VERSION_V2
} vtss_appl_erps_version_t;

/**  The ERPS version in a text format  */
extern const vtss_enum_descriptor_t vtss_appl_erps_version_txt[];

/** Enum describing the RPL role of the port. ITUT-G.8032(V2), Section 3.2.8 */
typedef enum {
    /** The port is not assigned a role */
    VTSS_APPL_ERPS_RPL_MODE_NONE = 1,

    /** The port is a RPL owner. ITUT-G.8032(V2), Section 3.2.10*/
    VTSS_APPL_ERPS_RPL_MODE_OWNER,

    /** The port is a RPL neighbour. ITUT-G.8032(V2), Section 3.2.9 */
    VTSS_APPL_ERPS_RPL_MODE_NEIGHBOUR,
} vtss_appl_erps_rpl_mode_t;

/**  The ERPS RPL mode in a text format  */
extern const vtss_enum_descriptor_t vtss_appl_erps_rpl_mode_txt[];

/** Enum describing the state of the ring link. ITUT-G.8032(V2), Section 7.1 */
typedef enum {
    /** The ring link is in non-failed mode */
    VTSS_APPL_ERPS_PORT_STATE_OK = 1,

    /** The ring link is in signal-failed mode */
    VTSS_APPL_ERPS_PORT_STATE_SF
} vtss_appl_erps_port_state_t;

/**  The ERPS port state in a text format  */
extern const vtss_enum_descriptor_t vtss_appl_erps_port_state_txt[];

/** Enum specifying the ring protection state. ITUT-G.8032(V2), Table 10-2 */
typedef enum {
    /** State machine initalization */
    VTSS_APPL_ERPS_STATE_NONE = 1,

    /** No failures detected, no commands active */
    VTSS_APPL_ERPS_STATE_IDLE,

    /** The ring protection is in protected state, the RPL is active */
    VTSS_APPL_ERPS_STATE_PROTECTED,

    /** The ring protection is forced switch mode */
    VTSS_APPL_ERPS_STATE_FORCED_SWITCH,

    /** The ring protection is manual switch mode */
    VTSS_APPL_ERPS_STATE_MANUAL_SWITCH,

    /** The ring protection is pending mode */
    VTSS_APPL_ERPS_STATE_PENDING
} vtss_appl_erps_protection_state_t;

/**  The ERPS port protection state in a text format  */
extern const vtss_enum_descriptor_t vtss_appl_erps_protection_state_txt[];

/** Enum indicating port 0 or port 1 in the ERPS  */
typedef enum {
    /** Port 0 */
    VTSS_APPL_ERPS_PORT_0 = 1,

    /** Port 1 */
    VTSS_APPL_ERPS_PORT_1
} vtss_appl_erps_port_t;

/**  The ERPS port id in a text format  */
extern const vtss_enum_descriptor_t vtss_appl_erps_port_txt[];

/** Read only enum indicating the request/state of the ERPS node */
typedef enum {
    /** State machine initalization */
    VTSS_APPL_ERPS_REQUEST_STATE_NONE = 1,

    /** The request state is in manual switch state */
    VTSS_APPL_ERPS_REQUEST_STATE_MANUAL_SWITCH,

    /**  The request state is signal fail mode */
    VTSS_APPL_ERPS_REQUEST_STATE_SIGNAL_FAIL,

    /**  The request state is forced switch mode */
    VTSS_APPL_ERPS_REQUEST_STATE_FORCED_SWITCH,

    /**  The request state is event mode */
    VTSS_APPL_ERPS_REQUEST_STATE_EVENT
} vtss_appl_erps_request_state_t;

/** The ERPS request state in a text format  */
extern const vtss_enum_descriptor_t vtss_appl_erps_request_state_txt[];

/** Enum indicating a ERPS control command */
typedef enum {
    /**  No command issued */
    VTSS_APPL_ERPS_CONTROL_CMD_NONE = 0,

    /**  Forced switch mode */
    VTSS_APPL_ERPS_CONTROL_CMD_ADMIN_CMD_FORCED_SWITCH = 1,

    /**  Manaual switch mode */
    VTSS_APPL_ERPS_CONTROL_CMD_ADMIN_CMD_MANUAL_SWITCH,

    /**  Clear state command */
    VTSS_APPL_ERPS_CONTROL_CMD_ADMIN_CMD_CLEAR,

    /**  Clear the statistics */
    VTSS_APPL_ERPS_CONTROL_CMD_STATISTICS_CLEAR
} vtss_appl_erps_control_cmd_t;

/** The ERPS control command in a text format  */
extern const vtss_enum_descriptor_t vtss_appl_erps_control_cmd_txt[];

/** ERPS statistics */
typedef struct {
    uint64_t raps_sent;          /**< Number of transmitted RAPS PDUs */
    uint64_t raps_rcvd;          /**< Number of received RAPS PDUs */
    uint64_t raps_rx_dropped;    /**< Number of received RAPS PDUs that were dropped */
    uint64_t local_sf;           /**< Number of local SignalFail clear events */
    uint64_t local_sf_cleared;   /**< Number of local SignalFail clear events */
    uint64_t remote_sf;          /**< Number of remote SignalFail events */
    uint64_t event_nr;           /**< Number of NoRequest events */
    uint64_t remote_ms;          /**< Number of remote ManualSwitch events */
    uint64_t local_ms;           /**< Number of local ManualSwitch events */
    uint64_t remote_fs;          /**< Number of remote ForcedSwitch events */
    uint64_t local_fs;           /**< Number of local ForcedSwitch events */
    uint64_t admin_cleared;      /**< Number of AdminClear events */
} vtss_appl_erps_statistics_t;


/** This struct contains status per EPRS group */
typedef struct {
    /** Specifies whether ERPS group is currently active or not */
    mesa_bool_t                              active;

    /** Specifies current ERPS group protection state */
    vtss_appl_erps_protection_state_t state;

    /** Specifies whether the RPL is currently blocked or not. */
    mesa_bool_t                              rpl_blocked;

    /** Specifies the remaining wait-time before restoring ring */
    uint32_t                               wtr_remaining_time;

    /** Specifies the currently active administrative command. */
    vtss_appl_erps_admin_cmd_t        admin_cmd;

    /** Specifies whether the FailureOfProtocol alarm is currently active or not  */
    mesa_bool_t                              fop_alarm;

    /** Specifies whether R-APS transmission is currently active */
    mesa_bool_t                              tx;

    /** Specifies the currently transmitted request/state. Only relevant when Tx is active */
    vtss_appl_erps_request_state_t    tx_req;

    /** Specifies the currently transmitted RPL Blocked flag. Only relevant when Tx is active */
    mesa_bool_t                              tx_rb;

    /** Specifies the currently transmitted Do Not Flush flag. Only relevant when Tx is active */
    mesa_bool_t                              tx_dnf;

    /** Specifies the currently transmitted Blocked Port Reference. Only relevant when Tx is active */
    vtss_appl_erps_port_t             tx_bpr;

    /** Specifies whether ring Port 0 is blocked or not */
    mesa_bool_t                              port0_blocked;

    /** Specifies the state of ring Port 0 */
    vtss_appl_erps_port_state_t       port0_state;

    /** Specifies if ring Port0 receives R-APS */
    mesa_bool_t                              port0_rx;

    /** Specifies the most recently received request/state on ring Port 0. Only relevant when Rx is active */
    vtss_appl_erps_request_state_t    port0_rx_req;

    /** Specifies the most recently received RPL Blocked flag on ring Port 0. Only relevant when Rx is active */
    mesa_bool_t                              port0_rx_rb;

    /** Specifies the most recently received Do Not Flush flag on ring Port 0. Only relevant when Rx is active */
    mesa_bool_t                              port0_rx_dnf;

    /** Specifies the most recently received Blocked Port Reference on ring Port 0. Only relevant when Rx is active */
    vtss_appl_erps_port_t             port0_rx_bpr;

    /** Specifies the most recently received node ID on ring Port 0. Only relevant when Rx is active */
    uint8_t                                port0_rx_node_id[VTSS_APPL_ERPS_MAX_NODE_ID_LEN];

    /** Specifies whether ring Port 1 is blocked or not */      
    mesa_bool_t                              port1_blocked;

    /** Specifies the state of ring Port 1 */
    vtss_appl_erps_port_state_t       port1_state;

    /** Specifies if ring Port1 receives R-APS */
    mesa_bool_t                              port1_rx;

    /** Specifies the most recently received request/state on ring Port 1. Only relevant when Rx is active */
    vtss_appl_erps_request_state_t    port1_rx_req;

    /** Specifies the most recently received RPL Blocked flag on ring Port 1. Only relevant when Rx is active */
    mesa_bool_t                              port1_rx_rb;

    /** Specifies the most recently received Do Not Flush flag on ring Port 1. Only relevant when Rx is active */
    mesa_bool_t                              port1_rx_dnf;

    /** Specifies the most recently received Blocked Port Reference on ring Port 1. Only relevant when Rx is active */
    vtss_appl_erps_port_t             port1_rx_bpr;

    /** Specifies the most recently received node ID on ring Port 1. Only relevant when Rx is active */
    uint8_t                                port1_rx_node_id[VTSS_APPL_ERPS_MAX_NODE_ID_LEN];
} vtss_appl_erps_status_t;


/** This struct contains configuration of an ERPS group */
typedef struct {
    /** Type of ring. Can only be set once for a ring instance 
     * if ring-type is interconnected sub-ring, then east_port contains sub-ring link 
     * if the given command is FS or MS, east_port contains the port, which needs to be blocked */
    vtss_appl_erps_ring_type_t    ring_type;

    /** ifindex of ring protection port 0. Can only be set once for a ring instance */
    vtss_ifindex_t                port0;

    /** ifindex of ring protection Port 1. For interconnected sub-rings this value must be zero. Can only be set once for a ring instance */
    vtss_ifindex_t                port1;

    /** For sub-ring: zero = not interconnected; > zero = index of major ring group.
        For major ring: zero = not interconnected; > zero = is interconnected (i.e flag-like semantics).
        Can only be set once for a ring instance.")); */
    uint32_t                           interconnect_major_ring_id;

    /** Whether to use a virtual channel. Can only be set once for a ring instance */
    mesa_bool_t                          virtual_channel;

    /** Index of SignalFail MEP for Port 0. Zero if not set. To clear an ERPS group's MEP association, set this to zero; it affects all MEP indices */
    uint32_t                           port0_sf_mep_id;

    /** Index of SignalFail MEP for Port 1. Zero if not set. Must be zero for interconnected sub-rings. */
    uint32_t                           port1_sf_mep_id;

    /** Index of APS MEP for Port 0. Zero if not set. */
    uint32_t                           port0_aps_mep_id;

    /** Index of APS MEP for Port 1. Zero if not set. Must be zero for interconnected sub-rings with virtual channel. */
    uint32_t                           port1_aps_mep_id;

    /** Hold off time in ms. Value is rounded down to 100ms precision. Valid range is 0-10000 ms.  */
    uint32_t                           hold_off_time;

    /** Wait-to-Restore time in ms. Valid range is 60000-720000 ms (1-12 minutes). */
    uint32_t                           wtr_time;

    /** Guard time in ms. Valid range is 10-2000 ms. */
    uint32_t                           guard_time;

    /** Ring Protection Link mode. */
    vtss_appl_erps_rpl_mode_t     rpl_mode;

    /** RPL port. Only valid if the ERPS group is RPL owner or neighbour. TRUE == port0; FALSE == port1. */
    vtss_appl_erps_port_t         rpl_port;           

    /** Fourth quarter of bit-array indicating whether a VLAN is protected by this ring instance ('1') or not ('0'). */
    uint8_t                            protected_vlans[VTSS_APPL_VLAN_BITMASK_LEN_BYTES];

    /** Revertive (true) or Non-revertive (false) mode. */
    mesa_bool_t                          revertive;

    /** ERPS protocol version. */
    vtss_appl_erps_version_t      version;

    /** Whether to enable Topology Change propagation; only valid for an interconnected node. */
    mesa_bool_t                          topology_change;
} vtss_appl_erps_conf_t;

/** The struct represents the capabilities of an ERPS group.*/
typedef struct {
   /** Maximum number of configured ERPS groups.*/
    uint32_t     max_groups;

   /** Maximum number of protected VLANS per ERPS group. */
    uint32_t     max_vlans_per_group;
} vtss_appl_erps_capabilities_t;

/** The struct represents dynamic control elements in an ERPS group.*/
typedef struct {
    /** Maximum number of configured ERPS groups. */
    vtss_appl_erps_control_cmd_t cmd;

    /** Maximum number of protected VLANS per ERPS group. */
    vtss_appl_erps_port_t        port;
} vtss_appl_erps_control_t;


/**
 * \brief Get the ERPS capabilities.
 *
 * \param *const            [OUT]  ERPS cap struct to be updated
 *
 * \return VTSS_RC_OK if the operation succeeded.
 *
 **/
mesa_rc vtss_appl_erps_capabilities_get(vtss_appl_erps_capabilities_t *const);


/**
 * \brief Set the ERPS group configuration.
 *
 * \param group_id           [IN]   ERPS group id
 *
 * \param *const             [IN]   ERPS configuration struct to be applied
 *
 * \return VTSS_RC_OK if the operation succeeded.
 **/
mesa_rc vtss_appl_erps_conf_set(uint32_t group_id, const vtss_appl_erps_conf_t *const);


/**
 * \brief Get the ERPS group configuration.
 *
 * \param group_id           [IN]   ERPS group id
 *
 * \param *const             [OUT]  ERPS configuration struct to be updated
 *
 * \return VTSS_RC_OK if the operation succeeded.
 **/
mesa_rc vtss_appl_erps_conf_get(uint32_t group_id, vtss_appl_erps_conf_t *const);


/**
 * \brief Iterate function for the ERPS groups.
 * This function will iterate through all active ERPS groups.
 *
 * Use NULL pointer to get the first group.
 *
 * \param in            [IN]  previous group.
 * \param out           [OUT] next group.
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_erps_conf_itr(const uint32_t *const in, uint32_t *const out);


/**
 * \brief Delete an ERPS group.
 *
 * \param group_id   [IN]   Group id of the ERPS group to be deleted
 *
 * \return VTSS_RC_OK if the operation succeeded.
 **/
mesa_rc vtss_appl_erps_conf_del(uint32_t group_id);


/**
 * \brief Default an ERPS conf struct
 *
 * \param *        [IN] pointer to an uint32_t. Only used to share syntax with other vtss_appl_erps_* functions
 *
 * \param *const   [OUT] pointer to an defaulted vtss_appl_erps_conf_t struct
 *
 * \return VTSS_RC_OK if the operation succeeded.
 **/
mesa_rc vtss_appl_erps_conf_default(uint32_t *, vtss_appl_erps_conf_t *const);


/**
 * \brief Get the status if an ERPS group
 *
 * \param group_id      [IN] The ERPS group id
 *
 * \param *const        [OUT] The ERPS status struct to be updated
 *
 * \return VTSS_RC_OK if the operation succeeded.
 **/
mesa_rc vtss_appl_erps_status_get(uint32_t group_id, vtss_appl_erps_status_t *const);

/**
 * \brief Iterate function for the ERPS groups.
 * This function will iterate through all active ERPS groups.
 *
 * Use NULL pointer to get the first group.
 *
 * \param in            [IN]  previous group.
 * \param out           [OUT] next group.
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_erps_status_itr(const uint32_t *const in, uint32_t *const out);

/**
 * \brief Get the ERPS group statistics
 *
 * \param group_id      [IN] The ERPS group id
 *
 * \param *const        [OUT] The updated ERPS statistics struct
 *
 * \return VTSS_RC_OK if the operation succeeded.
 **/
mesa_rc vtss_appl_erps_statistics_get(uint32_t group_id, vtss_appl_erps_statistics_t *const);

/**
 * \brief Iterate function for the ERPS groups.
 * This function will iterate through all active ERPS groups.
 *
 * Use NULL pointer to get the first group.
 *
 * \param in            [IN]  previous group.
 * \param out           [OUT] next group.
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_erps_statistics_itr(const uint32_t *const in, uint32_t *const out);

/**
 * \brief Set the ERPS control configuration.
 *
 * \param group_id      [IN]   ERPS group id
 *
 * \param *const        [IN]   The ERPS control struct to be applied.
 *
 * \return VTSS_RC_OK if the operation succeeded.
 **/
mesa_rc vtss_appl_erps_control_set(uint32_t group_id, const vtss_appl_erps_control_t *const);

/**
 * \brief Get the ERPS control configuration.
 *
 * \param group_id      [IN]   ERPS group id
 *
 * \param *const        [OUT]   The ERPS control struct to be updated
 *
 * \return VTSS_RC_OK if the operation succeeded.
 **/
mesa_rc vtss_appl_erps_control_get(uint32_t group_id, vtss_appl_erps_control_t *const);

/**
 * \brief Iterate function for the ERPS groups.
 * This function will iterate through all active ERPS groups.
 *
 * Use NULL pointer to get the first group.
 *
 * \param in            [IN]  previous group.
 * \param out           [OUT] next group.
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_erps_control_itr(const uint32_t *const in, uint32_t *const out);



void lntn_erps_led_notify(void);

#ifdef __cplusplus
}
#endif
#endif  // _VTSS_APPL_ERPS_H_
