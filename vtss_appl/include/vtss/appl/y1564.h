/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

/**
 * \file
 * \brief Y.1564 API
 * \details This header file describes public functions applicable to Y.1564 control.
 */

#ifndef _VTSS_APPL_Y1564_H_
#define _VTSS_APPL_Y1564_H_

#include <mscc/ethernet/switch/api/types.h>      /* For various vtss_XXX types         */
#include <vtss/appl/defines.h>   /* For VTSS_APPL_RFC3339_TIME_STR_LEN */
#include <vtss/appl/interface.h> /* For vtss_ifindex_t                 */

#define VTSS_APPL_Y1564_PROFILE_NAME_LEN      33 /**< Maximum length (including null-termination) of profile name string.        */
#define VTSS_APPL_Y1564_PROFILE_DSCR_LEN     129 /**< Maximum length (including null-termination) of profile description string. */
#define VTSS_APPL_Y1564_REPORT_NAME_LEN       33 /**< Maximum length (including null-termination) of test report name.           */
#define VTSS_APPL_Y1564_REPORT_DSCR_LEN      129 /**< Maximum length (including null-termination) of report description string.  */
#define VTSS_APPL_Y1564_REPORT_PATH_LEN      129 /**< Maximum length (including null-termination) of the report path.            */
#define VTSS_APPL_Y1564_ECE_TEST_CNT_MAX       8 /**< The maximum number of ECEs per EVC that can be tested simultaneously. */

/**
 * Manifest constants for the Y.1564 module.
 */
typedef struct {
    /**
     * Number of profiles that can be defined and saved to non-volatile memory.
     */
    uint32_t profile_cnt;

    /**
     * Number of reports that can be saved to non-volatile memory.
     */
    uint32_t report_cnt;

    /**
     * Maximum number of concurrently running tests.
     */
    uint32_t concurrently_executing_max;

    /**
     * Maximum number of ECEs per EVC that can be tested simultaneously.
     */
    uint32_t ece_test_cnt_max;

    /**
     * Maximum number of frames in an EMIX.
     */
    uint32_t emix_max;
} vtss_appl_y1564_capabilities_t;

/**
 * Test report status.
 */
typedef enum {
    VTSS_APPL_Y1564_TEST_STATUS_INACTIVE,   /**< Current test has never been executed. */
    VTSS_APPL_Y1564_TEST_STATUS_EXECUTING,  /**< Test is currently executing.          */
    VTSS_APPL_Y1564_TEST_STATUS_CANCELLING, /**< Test is under cancellation.           */
    VTSS_APPL_Y1564_TEST_STATUS_CANCELLED,  /**< Test was cancelled by user.           */
    VTSS_APPL_Y1564_TEST_STATUS_PASSED,     /**< Test terminated successfully.         */
    VTSS_APPL_Y1564_TEST_STATUS_FAILED,     /**< Test failed.                          */
} vtss_appl_y1564_test_status_t;

/**
 * Service Acceptance Criteria.
 */
typedef struct {
    /**
     * Acceptable Frame Loss Ratio in permille.
     * Valid range is [0; 1000] permille. Set to 1000 to disable this check.
     */
    uint32_t flr_permille;

    /**
     * Acceptable Frame Transfer Delay in milliseconds.
     * Valid range is [0; 10,000] msecs. Set to 0 to disable this check.
     */
    uint32_t ftd_msecs;

    /**
     * Acceptable Frame Delay Variation in milliseconds.
     * Valid range is [0; 10,000] msecs. Set to 0 to disable this check.
     */
    uint32_t fdv_msecs;
} vtss_appl_y1564_sac_t;

/**
 * Which kind of traffic should be used as test traffic?
 */
typedef enum {
    /**
     * Transmit Y.1731 OAM frames as test traffic.
     * Whether it's LBM or TST frames depends on #vtss_appl_y1564_profile_t::dual_ended.
     * If single-ended, TST frames are transmitted, otherwise LBM frames are
     * transmitted.
     */
    VTSS_APPL_Y1564_TRAFFIC_TYPE_Y1731_OAM,

    /**
     * Transmit simulated traffic (non-OAM) that hits the ECEs under
     * test.
     */
    VTSS_APPL_Y1564_TRAFFIC_TYPE_SIMULATED_CUSTOMER,

    /**
     * This must come last.
     */
    VTSS_APPL_Y1564_TRAFFIC_TYPE_CNT
} vtss_appl_y1564_traffic_type_t;

/**
 * Common structure to hold configured test duration
 * and delay measurement intervals.
 */
typedef struct {
    /**
     * Enable this test.
     */
    mesa_bool_t enable;

    /**
     * Duration (in seconds) of a test.
     * This is the duration per step for test cases
     * that use #step_cnt. The total duration is therefore
     * #duration_secs * #step_cnt seconds.
     */
    uint32_t duration_secs;

    /**
     * Delay measurement interval (in msecs) of a test.
     * Set to 0 to disable FTD and FDV measurements.
     */
    uint32_t dm_interval_msecs;

    /**
     * This is only used when executing CIR Configuration Test.
     * It specifies the number of steps to reach CIR. With, e.g., a value of 4,
     * the EVC will be tested at 25%, 50%, 75% and 100% of CIR.
     * A non-step-load test (A.1) can be achieved by setting #step_cnt to 1.
     * Valid values [1; 1000].
     */
    uint32_t step_cnt;

} vtss_appl_y1564_profile_test_conf_t;

/**
 * This structure holds parameters common to all test types.
 *
 * Table that illustrates how #dual_ended, #traffic_type,
 * and #dst_oam_aware combinations affect the output.
 *
 * | Ended  | DST OAM aware | Traffic Type || Tx Traffic Type | Tx DM Type | DST must
 * ------------------------------------------------------------------------------------------------------------------------------------------------------------
 * | Single | No            | OAM          || TST             | 1DM        | Domain-aware loop. A dumb SMAC/DMAC swap may be enough if symmetric encapsulation.
 * | Single | No            | Cust         || Cust            | 1DM        | Domain-aware loop. A dumb SMAC/DMAC swap may be enough if symmetric encapsulation.
 * | Single | Yes           | OAM          || LBM             | DMM        | Respond with LBR and DMR
 * | Single | Yes           | Cust         || Cust            | DMM        | Domain-aware loop of customer traffic and respond to DMM with DMR.
 * | Dual   | No            | OAM          || TST             | \<none>    | Terminate and count TST. Can't do DM.
 * | Dual   | No            | Cust         || Cust            | \<none>    | Terminate and count Cust. Can't do DM.
 * | Dual   | Yes           | OAM          || TST             | 1DM        | Terminate TST and 1DM.
 * | Dual   | Yes           | Cust         || Cust            | 1DM        | Terminate Cust and 1DM.
 */
typedef struct {
    /**
     * Profile name used for identification of a given profile.
     * Defaults to empty string. Two profiles can't have the same name.
     * An empty name is illegal. Only isgraph() characters - except for
     * '/', '\"', '<', '>', and '\'' - are allowed.
     */
    char name[VTSS_APPL_Y1564_PROFILE_NAME_LEN];

    /**
     * Profile description. Only isprint() characters are allowed.
     */
    char dscr[VTSS_APPL_Y1564_PROFILE_DSCR_LEN];

    /**
     * Single- or dual-ended?
     * If FALSE, DST is expected to loop the traffic according
     * to #traffic_type and #dst_oam_aware.
     * SRC reports both transmitted and received frames.
     *
     * If TRUE, DST is terminating the traffic, and the SRC
     * only reports transmitted frames.
     */
    mesa_bool_t dual_ended;

    /**
     * TRUE if DST is OAM aware.
     *
     * See table above for a description of how #traffic_type,
     * #dual_ended and #dst_oam_aware work together.
     */
    mesa_bool_t dst_oam_aware;

    /**
     * Type of traffic to inject. See enum for an explanation.
     */
    vtss_appl_y1564_traffic_type_t traffic_type;

    /**
     * The MEG level (0 - 7) used in the generated OAM frames (TST/LBM
     * if applicable and 1DM/DMM).
     */
    uint32_t meg_level;

    /**
     * Sequence of letters that indicate the frame sizes that one
     * flow is made up of. The frame sizes are at the UNI, so
     * possible encapsulation added at the NNI is not included.
     *
     * The string may contain the following letters:
     *   a:   64 byte frames
     *   b:  128 byte frames
     *   c:  256 byte frames
     *   d:  512 byte frames
     *   e: 1024 byte frames
     *   f: 1280 byte frames
     *   g: 1518 byte frames
     *   h: MTU byte frames, where MTU is what the UNI port is currently configured to.
     *   u: User-defined. See #user_defined_frame_size.
     *
     * The string must be NULL-terminated ('\0') after at most
     * vtss_appl_y1564_capabilities_t::emix_max characters.
     *
     * On architectures with EMIX support, vtss_appl_y1564_capabilities_t::emix_max
     * is > 1, and any combination of the above letters may
     * be specified, default being as many of the characters "abceg" that there
     * is room for in the emix. This sequence indicates frame sizes of 64, 128,
     * 256, 1024, and 1518 bytes. The same size may be used more than once.
     *
     * On architectures without EMIX support, vtss_appl_y1564_capabilities_t::emix_max
     * is 1, restricting the size to a single. Default in that case is
     * "d" corresponding to 512 byte frames.
     *
     * Implementation note: The 254 bytes is the max DisplayString in SNMP, and is
     * chosen to have the MIB being forward compatible with "any" EMIX size.
     */
    char emix[254];

    /**
     * If #emix includes 'u', this is the user-specified frame
     * size, which must be in range [64; VTSS_MAX_FRAME_LENGTH_MAX - 4].
     */
    uint32_t user_defined_frame_size;

    /**
     * The time (in milliseconds with a granularity of 100) to wait after
     * each trial for the system to settle before
     * acquiring counters and other statistics from hardware.
     */
    uint32_t dwell_time_msecs;

    /**
     * Service Acceptance Criteria.
     */
    vtss_appl_y1564_sac_t sac;

    /**
     * Profile configuration for the CIR configuration test.
     */
    vtss_appl_y1564_profile_test_conf_t cir_test;

    /**
     * Profile configuration for the EIR configuration test.
     */
    vtss_appl_y1564_profile_test_conf_t eir_test;

    /**
     * Profile configuration for the Traffic Policing test.
     */
    vtss_appl_y1564_profile_test_conf_t pol_test;

    /**
     * Profile configuration for the Performance test.
     */
    vtss_appl_y1564_profile_test_conf_t prf_test;

} vtss_appl_y1564_profile_t;

/**
 * Structure that defines additional test attributes
 * related to one ECE.
 */
typedef struct {
    /**
     * The ID of the ECE to test. As described
     * in #vtss_appl_y1564_test_data_t::ece_list, this can be
     * set to 0 to terminate the list.
     * If the first entry is set to 0,
     * it is assumed that all ECEs of an EVC should be
     * tested, and all of them will get defaults for
     * the remaining parameters of this structure.
     *
     * Implementation details:
     *  This is a one-based ECE ID. Internally, the EVC module
     *  also works with one-based ECE IDs, but it didn't have to be like that.
     *  This is why we don't use the mesa_ece_id_t type.
     */
    uint32_t ece_id;

    /**
     * If the ECE under test can be reached from multiple UNI ports,
     * this one points out the one to be used.
     *
     * Leave at VTSS_IFINDEX_NONE to have the Y.1564 module auto-pick
     * a port. This is useful if you know that the ECE only can
     * be reached from one single UNI, or you don't care which
     * UNI to use, as long as the ECE can be reached through the
     * chosen UNI.
     *
     * If set to VTSS_IFINDEX_NONE and it turns out that
     * the ECE can be reached from multiple UNIs, the
     * software will pick the lowest numbered UNI.
     *
     * If set to a UNI different from VTSS_IFINDEX_NONE, and the
     * ECE is not reachable from that UNI, the test will fail.
     *
     * Only VTSS_IFINDEX_NONE and port ifindex type are
     * allowed. Other interface indices will result in
     * an error.
     */
    vtss_ifindex_t uni;

    /**
     * The VLAN ID used in the generated frames.
     *
     * This field is only used in cases where the ECE matches
     * multiple VLAN IDs or both tagged and untagged traffic.
     * In cases where an ECE match w.r.t. VLAN tagging can only
     * occur in one single, unambiguous way, this field is not
     * used and can be set to anything.
     *
     * If set to 4096:
     *   The software will insert a VLAN tag with a VLAN ID
     *   corresponding to the lowest non-zero VLAN, in the range
     *   that the ECE matches.
     *   VLAN ID 0 is only picked if its range only matches this
     *   particular.
     *   The above applies if the ECE matches tagged only as well
     *   as tagged and untagged traffic.
     *   If the ECE only matches untagged traffic, no VLAN tag
     *   will be inserted in the frame.
     *
     * If set to 4097:
     *   An error will be returned if the ECE does not match
     *   untagged traffic. Otherwise the test frames will be
     *   generated without tag.
     *
     * If set to 0:
     *   An error will be returned if the ECE does not match
     *   priority tagged traffic. Otherwise the test frames will
     *   be generated with a priority tag.
     *
     * If set to a value in range [VTSS_APPL_VLAN_ID_MIN; VTSS_APPL_VLAN_ID_MAX]:
     *   If the ECE does not match the particular VID, an error
     *   will be returned. Otherwise the VID will be used in
     *   the generated frames.
     *
     * If set to any other value, an error will be returned.
     */
    mesa_vid_t vid;

    /**
     * The PCP value to use in the generated frames.
     *
     * It is only used when #vtss_appl_y1564_profile_t::traffic_type is set
     * to VTSS_APPL_Y1564_TRAFFIC_TYPE_SIMULATED_CUSTOMER. In the Y.1731 OAM
     * case, the requested CoS is used to determine which PCP to
     * use in the tag for the OAM traffic.
     *
     * Also, the value is only used when an outer tag is required
     * in the frame.
     *
     * If set to 8:
     *   The software will insert the lowest PCP value in the range
     *   that the ECE matches on.
     *
     * If set to a value in range [VTSS_PCP_START; VTSS_PCP_END[:
     *   The software will use this value, if the ECE matches on it.
     *   Otherwise an error will be returned.
     *
     * If set to any other value, an error will be returned.
     */
    mesa_tagprio_t pcp;

    /**
     * The DEI value to use in the generated frames.
     *
     * It is only used when #vtss_appl_y1564_profile_t::traffic_type is set
     * to VTSS_APPL_Y1564_TRAFFIC_TYPE_SIMULATED_CUSTOMER. In the Y.1731 OAM
     * case, the requested CoS along with the policer's color mode,
     * is used to determine which PCP to use in the tag for the OAM
     * traffic.
     *
     * Independent of the value of #dei, the following holds true:
     * If the policer is color-blind, only one flow will be generated.
     * If the policer is color-aware, one or two flows will be generated.
     *
     * The meaning of the "color of the flow", is not necessarily what the UNI
     * ingress C-tag's DEI value is set to, because it may change in one of
     * the ingress steps. The "color of the flow" is what color (drop
     * precedence) a given flow will have just before it enters the policer.
     *
     * Ingress steps that may affect the color of the flow:
     * 1) The ECE may match a given value of the C-tag's DEI, which by itself
     *    limits the possible flows to a single, independent of the policer's
     *    color awareness.
     * 2) The ECE may enforce all frames that hit it a given color. Doing so
     *    will also limit the number of possible flows to one.
     * 3) The UNI ingress port may be tag-aware, and if so, a given (PCP, DEI)
     *    tuple maps to a given DP. It could be that the end-user has configured
     *    e.g. PCP = x and DEI = 0 to have drop precedence 1, which means that
     *    a yellow flow with DEI = 0 will be generated.
     *
     * The Y.1564 software first makes this evaluation and figures out which DEI
     * (if any) to use for green flows and which DEI to use for yellow flows.
     * After this, it applies the user's wishes based on #dei.
     * If user sets #dei to 2 (which means Auto), then nothing else is done, so
     * the one or two generated flows will be active.
     * If user sets #dei to 0 or 1, only the flow corresponding to that DEI value
     * will be active. If this rules out all flows, an error will be returned.
     *
     * It also means that if the user sets DEI to a value that rules out green
     * flows on a color-aware policer, the CIR Configuration and Performance
     * tests will be skipped.
     *
     * In general, if a color-aware policer cannot be hit with a green flow,
     * given configuration and test input parameters, the CIR Configuration and
     * Performance tests will be skipped.
     *
     * It is perfectly fine to run CIR Configuration and Performance tests on
     * a color-blind policer, even if the ingress flow is yellow when it hits
     * the policer. It just means that UNI Egress checks must be skipped for
     * that flow, because the traffic on NNI is yellow, and the Y.1564
     * recommendation forbids failing on yellow traffic.
     */
    uint32_t dei; // Can't use a mesa_dei_t, because that's a mesa_bool_t, and we need three values.

    /**
     * The DSCP value to use in the generated frames.
     *
     * It is only used when #vtss_appl_y1564_profile_t::traffic_type is set
     * to VTSS_APPL_Y1564_TRAFFIC_TYPE_SIMULATED_CUSTOMER.
     *
     * Also, it is only used when the ECE is configured to match
     * IPv4 or IPv6 frames.
     *
     * If set to 64:
     *   The software will insert the lowest DSCP value in the range
     *   that the ECE matches on.
     *
     * If set to a value in range [0; 63]:
     *   The software will use this value, if the ECE matches on it.
     *   Otherwise an error will be returned.
     *
     * If set to any other value, an error will be returned.
     */
    mesa_dscp_t dscp;

} vtss_appl_y1564_ece_test_data_t;

/**
 * Structure that holds the test data required to execute a test.
 */
typedef struct {
    /**
     * Report name used for identification of the test data.
     * Defaults to empty string. Two test data report names can't have the same name.
     * An empty name is illegal. Only isgraph() characters - except for
     * '/', '\"', '<', '>', and '\'' - are allowed.
     */
    char name[VTSS_APPL_Y1564_REPORT_NAME_LEN];

    /**
     * Description of report. Only isprint() characters are allowed.
     */
    char dscr[VTSS_APPL_Y1564_REPORT_DSCR_LEN];

    /**
     * Name of profile to use when performing the test.
     */
    char profile_name[VTSS_APPL_Y1564_PROFILE_NAME_LEN];

    /**
     * ID that uniquely identifies the EVC under test.
     *
     * Implementation details:
     *  This is the EVC ID as seen from the user's perspective, that is, 1-based.
     *  It is automatically converted to an internal EVC ID, that is, 0-based.
     */
    uint16_t evc_id;

    /**
     * The destination MAC address used in the generated frames.
     *
     * It must be a non-NULL, unicast MAC address.
     *
     * The MAC address should be the MAC address of the peer of the EVC
     * under test (remote NNI residence port's MAC address).
     *
     * Since all generated frames will use this MAC address as
     * DMAC, it is not possible to test ECEs with customer
     * simulated traffic, if the ECE dictates a particular DMAC address.
     */
    mesa_mac_t peer_mac;

    /**
     * The EVC may be made up of multiple ECEs.
     * One can test either all ECEs (the whole EVC) or
     * a subset of the ECEs mapping to the EVC.
     * #ece_list lists the ECEs to be tested along with
     * additional test attributes.
     *
     * Leave first entry at 0 to test all ECEs carried in the EVC.
     * The list is terminated (if not full) by an entry having a value equal to 0.
     *
     * If #vtss_appl_y1564_profile_t::traffic_type is set to VTSS_APPL_Y1564_TRAFFIC_TYPE_Y1731_OAM,
     * up to 8 ECEs can be tested at a time, provided they map to different
     * CoSs.
     *
     * If #vtss_appl_y1564_profile_t::traffic_type is set to VTSS_APPL_Y1564_TRAFFIC_TYPE_SIMULATED_CUSTOMER,
     * then VTSS_APPL_Y1564_ECE_TEST_CNT_MAX ECEs can be tested simultaneously, and all
     * entries in #ece_list may be defined.
     *
     * If the first entry is set to 0 and the EVC carries more ECEs than the traffic type supports,
     * then the test will fail.
     */
    vtss_appl_y1564_ece_test_data_t ece_list[VTSS_APPL_Y1564_ECE_TEST_CNT_MAX];

} vtss_appl_y1564_test_data_t;

/**
 * This structure holds meta info about a report.
 */
typedef struct {
    /**
     * Name of report. Only isgraph() characters - except for '/', '\"', '<'
     * '>', and '\'' - are allowed.
     */
    char name[VTSS_APPL_Y1564_REPORT_NAME_LEN];

    /**
     * Description of report. Only isprint() characters are allowed.
     */
    char dscr[VTSS_APPL_Y1564_REPORT_DSCR_LEN];

    /**
     * Time of creation of this report.
     */
    char creation_time[VTSS_APPL_RFC3339_TIME_STR_LEN];

    /**
     * Execution status of this report.
     */
    vtss_appl_y1564_test_status_t status;

    /**
     * This is the file system location to retrieve this report.
     *
     * If, for instance, HTTP is enabled on the device, then the report can be
     * fetched at the address formed by concatenating the device's HTTP address
     * with \p path.
     * For example, if the device is at address 192.168.1.2, the report can be
     * retrieved using the result of
     *    concat("http://192.168.1.2", uri_encode(path));
     *
     * Notice the uri_encode() call above. It is required if the report name
     * should contain special characters in order for - here - HTTP to work
     * correctly.
     */
    char path[VTSS_APPL_Y1564_REPORT_PATH_LEN];

} vtss_appl_y1564_report_info_t;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get manifest constants for the Y.1564 module.
 *
 * \param cap [OUT] Pointer to structure receiving constants.
 *
 * \return VTSS_RC_OK on success, anything else on error.
 */
mesa_rc vtss_appl_y1564_capabilities_get(vtss_appl_y1564_capabilities_t *cap);

/**
 * Get names of existing profiles.
 *
 * Repeatedly call this function to get all profiles.
 * The first time, set name[0] to '\0', and leave it at its
 * previous value subsequent times.
 * As long as the function returns a non-zero length in the
 * name argument, more profiles are defined. Once the function
 * sets name[0] to '\0', the last profile is obtained.
 *
 * \param name [INOUT] See description above. Length must be at least VTSS_APPL_Y1564_PROFILE_NAME_LEN bytes.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_y1564_profile_names_get(char *name);

/**
 * Get an existing profile or fill-in a default one.
 *
 * If the name argument is NULL or has length == 0, the profile argument
 * is filled with a default profile.
 *
 * If the name argument has length > 0, the profile argument is filled
 * with the corresponding profile data.
 *
 * \param name    [IN]  If NULL or has length == 0, default profile, otherwise the name of profile to retrieve.
 * \param profile [OUT] Pointer receiving profile.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_y1564_profile_get(const char *const name, vtss_appl_y1564_profile_t *profile);

/**
 * Set a profile.
 *
 * Use this function to create, update, or delete a profile.
 *
 * Setting profile->name to an empty string and old_name argument to the name of the
 * profile to delete, effectively deletes it, i.e. sets all fields to defaults.
 *
 * Setting profile->common.name to a non-empty string and old_name argument to the name of an
 * existing profile, will update the existing profile.
 *
 * Setting profile->common.name to a non-empty string and old_name to NULL or letting
 * it have length == 0, will create a new profile.
 *
 * \param old_name [IN] If updating or deleting an existing, must be a valid, existing profile name. Otherwise (creating a new profile), it must be NULL or have length == 0.
 * \param profile  [IN] Pointer to profile data.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_y1564_profile_set(const char *const old_name, vtss_appl_y1564_profile_t *profile);

/**
 * Get names of a non-executed tests.
 *
 * Repeatedly call this function to get all names of current test data entries.
 * Up to vtss_appl_y1564_capabilities_t::currently_executing_max such
 * entries may exist. The test data is volatile and will therefore not
 * survive a reboot.
 *
 * To iterate, do this:
 * The first time, set name[0] to '\0', and leave it at its previous value subsequent times.
 * As long as the function returns a non-zero length in the
 * name argument, more test data entries are defined.
 * Once the function sets name[0] to '\0', the last report has already been
 * obtained.
 *
 * \param name [INOUT] See description above. Length must be at least VTSS_APPL_Y1564_REPORT_NAME_LEN bytes.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_y1564_test_data_names_get(char *name);

/**
 * Get test data for a given not-yet executed test.
 *
 * If the report_name argument is NULL or has length == 0, the test_data argument
 * is filled with default test data.
 *
 * If the report_name argument has length > 0, the test_data argument is filled
 * with the corresponding test data.
 *
 * \param report_name [IN]  Name of resulting test report (at most VTSS_APPL_Y1564_REPORT_NAME_LEN chars long incl. NULL termination).
 * \param test_data   [OUT] Input data to the test.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_y1564_test_data_get(const char *const report_name, vtss_appl_y1564_test_data_t *test_data);

/**
 * Set test data.
 *
 * Use this function to create, update, or delete test data.
 *
 * Test data must be set up prior to executing a test with vtss_appl_y1564_test_start().
 *
 * Setting test_data->name to an empty string and old_name argument to the name of the
 * test data entry to delete, effectively deletes it, i.e. sets all fields to defaults.
 *
 * Setting test_data->name to a non-empty string and old_name argument to the name of an
 * existing test data entry, will update the existing test data entry.
 *
 * Setting test_data->name to a non-empty string and old_name to NULL or letting
 * it have length == 0, will create a new test data entry.
 *
 * \param old_name  [IN] If updating or deleting an existing, must be a valid, existing test data entry name. Otherwise (creating a new test data entry), it must be NULL or have length == 0.
 * \param test_data [IN] Pointer to test data.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_y1564_test_data_set(const char *const old_name, vtss_appl_y1564_test_data_t *test_data);

/**
 * Start executing a test indicated by report_name.
 *
 * \p report_name must point to a test data entry already set up with calls
 * to vtss_appl_y1564_test_data_set().
 *
 * The test either stops by itself or may be force-stopped with a call to
 * vtss_appl_y1564_test_stop().
 *
 * Once this function has been called, a snap shot of the test data set up
 * with vtss_appl_y1564_test_data_set() will be taken and the entry will
 * be removed from the test data table.
 *
 * \param report_name [IN] Name of test report
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_y1564_test_start(const char *const report_name);

/**
 * Stop executing the test indicated by report_name.
 *
 * \param report_name [IN] Name of test report currently in progress.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_y1564_test_stop(const char *const report_name);

/**
 * Get names and info of existing reports.
 *
 * Repeatedly call this function to get info and names of all reports.
 * The first time you invoke the function, set info.name[0] == '\0'.
 * Subsequent times, leave it as it was upon return.
 * Stop when the function returns info.name[0] == '\0'.
 *
 * Reports are returned in chronological order - oldest first.
 *
 * \param info [INOUT] Pointer to a structure receiving info about a given report. See description above on how to control the .name member.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_y1564_report_info_get(vtss_appl_y1564_report_info_t *info);

/**
 * Delete test report.
 *
 * \param name [IN] Name of report to delete
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_y1564_report_delete(const char *const name);

/**
 * Get a report as clear text.
 *
 * The function allocates a string of a suitable size (with VTSS_MALLOC()) and
 * prints the report into it as clear text. The length of the string can be found
 * with strlen(report_as_txt).
 *
 * Iff the function call succeeds (return value == VTSS_RC_OK), the pointer
 * to the report must be freed with a call to VTSS_FREE().
 *
 * \param report_name   [IN]  Name of report to get as text.
 * \param report_as_txt [OUT] Pointer to a char pointer receiving the VTSS_MALLOC()ed string.
 * \param status        [OUT] Status of report. May be NULL if status not needed.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_y1564_report_as_txt(const char *report_name, char **report_as_txt, vtss_appl_y1564_test_status_t *status);

#ifdef __cplusplus
}
#endif

#endif /* _VTSS_APPL_Y1564_H_ */

