/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

/**
 * \file
 * \brief Public EVC API
 * \details This header file describes EVC control functions and types.
 */

#ifndef _VTSS_APPL_EVC_H_
#define _VTSS_APPL_EVC_H_

#include <mscc/ethernet/switch/api/types.h>
#include <vtss/appl/interface.h>
#include <vtss/appl/vcap_types.h>
#include <vtss/appl/module_id.h>

extern const vtss_enum_descriptor_t evc_l2cp_type_txt[];      /**< Enum descriptor text */
extern const vtss_enum_descriptor_t evc_ece_rule_txt[];       /**< Enum descriptor text */
extern const vtss_enum_descriptor_t evc_ece_tx_lookup_txt[];  /**< Enum descriptor text */
extern const vtss_enum_descriptor_t evc_ece_pcp_mode_txt[];   /**< Enum descriptor text */
extern const vtss_enum_descriptor_t evc_ece_dei_mode_txt[];   /**< Enum descriptor text */
extern const vtss_enum_descriptor_t evc_ece_l2cp_mode_txt[];  /**< Enum descriptor text */
extern const vtss_enum_descriptor_t evc_ece_l2cp_dmac_txt[];  /**< Enum descriptor text */
extern const vtss_enum_descriptor_t evc_inner_tag_type_txt[]; /**< Enum descriptor text */
extern const vtss_enum_descriptor_t evc_policer_op_txt[];     /**< Enum descriptor text */
extern const vtss_enum_descriptor_t evc_port_l2cp_mode_txt[]; /**< Enum descriptor text */
extern const vtss_enum_descriptor_t evc_policer_type_txt[];   /**< Enum descriptor text */
extern const vtss_enum_descriptor_t evc_policer_mode_txt[];   /**< Enum descriptor text */
extern const vtss_enum_descriptor_t evc_ece_type_txt[];       /**< Enum descriptor text */
extern const vtss_enum_descriptor_t evc_ece_dir_txt[];        /**< Enum descriptor text */
extern const vtss_enum_descriptor_t evc_ece_pop_tag_txt[];    /**< Enum descriptor text */
extern const vtss_enum_descriptor_t evc_port_role_txt[];      /**< Enum descriptor text */

/**
 * Maximum length - including terminating NULL - of a EVC name
 */
#define VTSS_APPL_EVC_NAME_MAX_LEN  33

/** \brief EVC capabilities per port */
typedef struct {
    mesa_bool_t has_dei_colouring; /**< Port DEI colouring */
    mesa_bool_t has_inner_tag;     /**< Port inner tag matching */
    mesa_bool_t has_dmac_dip;      /**< Port DMAC/DIP matching */
    mesa_bool_t has_key_type;      /**< Port key type */
} vtss_appl_evc_cap_port_t; /**< Port capabilities */

/** \brief EVC capabilities per policer */
typedef struct {
    uint32_t  max;              /**< Maximum number of policers */
    mesa_bool_t has_color_blind;  /**< Policer color-blind operation */
    mesa_bool_t has_evc_policers; /**< EVC policers */
} vtss_appl_evc_cap_policer_t; /**< Policer capabilities */

/** \brief EVC capabilities per EVC */
typedef struct {
    uint32_t  max;           /**< Maximum number of EVC rules */
    mesa_bool_t has_nni_list;  /**< EVC NNI port list */
    mesa_bool_t has_policer;   /**< EVC policer mapped from EVC */
    mesa_bool_t has_uni_vid;   /**< EVC UNI VID */
    mesa_bool_t has_it_add;    /**< EVC inner tag adding */
    mesa_bool_t has_mpls;      /**< EVC MPLS pseudo-wire */
    mesa_bool_t has_etree;     /**< E-Tree */
    mesa_bool_t has_leaf_list; /**< E-Tree leaf port list */
    mesa_bool_t has_rx_map;    /**< Ingress QoS mapping */
    mesa_bool_t has_tx_map;    /**< Egress QoS mapping */
    mesa_bool_t has_name;    /**< EVC name */
} vtss_appl_evc_cap_evc_t;

/** \brief EVC capabilities per ECE */
typedef struct {
    uint32_t  max;              /**< Maximum number of ECE rules */
    mesa_bool_t has_adv_lookup;   /**< ECE advanced lookup */
    mesa_bool_t has_dmac_type;    /**< ECE DMAC type matching */
    mesa_bool_t has_dmac;         /**< ECE DMAC matching */
    mesa_bool_t has_smac;         /**< ECE SMAC matching */
    mesa_bool_t has_inner_tag;    /**< ECE inner tag matching */
    mesa_bool_t has_etype;        /**< ECE EType matching */
    mesa_bool_t has_llc;          /**< ECE LLC matching */
    mesa_bool_t has_snap;         /**< ECE SNAP matching */
    mesa_bool_t has_l2cp;         /**< ECE L2CP matching and forwarding */
    mesa_bool_t has_fragment;     /**< ECE IPv4 fragment matching */
    mesa_bool_t has_ip_proto;     /**< ECE IPv4/IPv6 protocol matching */
    mesa_bool_t has_sip;          /**< ECE IPv4/IPv6 SIP matching */
    mesa_bool_t has_dip;          /**< ECE IPv4/IPv6 DIP matching */
    mesa_bool_t has_sport;        /**< ECE IPv4/IPv6 SPORT matching */
    mesa_bool_t has_dport;        /**< ECE IPv4/IPv6 DPORT matching */
    mesa_bool_t has_rule;         /**< ECE rule type */
    mesa_bool_t has_tx_lookup;    /**< ECE Tx lookup */
    mesa_bool_t has_ot_add;       /**< ECE outer tag add */
    mesa_bool_t has_ot_vid;       /**< ECE outer tag VID add */
    mesa_bool_t has_pcp_mode;     /**< ECE PCP mode */
    mesa_bool_t has_pcp_dei_pres; /**< ECE PCP/DEI preservation */
    mesa_bool_t has_dei_mode;     /**< ECE DEI mode */
    mesa_bool_t has_it_add;       /**< ECE inner tag add */
    mesa_bool_t has_policer;      /**< EVC policer mapped from ECE */
    mesa_bool_t has_prio;         /**< ECE priority mapping */
    mesa_bool_t has_dp;           /**< ECE DP mapping */
    mesa_bool_t has_rx_map;       /**< ECE ingress QoS mapping */
} vtss_appl_evc_cap_ece_t;

/** \brief EVC capabilities per EVC and port */
typedef struct {
    mesa_bool_t has_hqos;          /**< EVC and port HQOS mapping */
    mesa_bool_t has_counters;      /**< Counters */
    mesa_bool_t has_role;          /**< Port role */
    mesa_bool_t has_l2cp_profile;  /**< L2CP profile */
    mesa_bool_t has_encap_id;      /**< Encapsulation ID */
} vtss_appl_evc_cap_evc_port_t;

/** \brief EVC capabilities per port and L2CP */
typedef struct {
    mesa_bool_t has_cosid; /**< COSID mapping */
} vtss_appl_evc_cap_port_l2cp_t;

/** \brief EVC capabilities per EVC and port and COSID */
typedef struct {
    mesa_bool_t has_policers; /**< COSID policers */
    mesa_bool_t has_counters; /**< COSID counters */
} vtss_appl_evc_cap_evc_port_cosid_t;

/** \brief EVC capabilities */
typedef struct {
    vtss_appl_evc_cap_port_t           port;           /**< Port capabilities */
    vtss_appl_evc_cap_policer_t        policer;        /**< Policer capabilities */
    vtss_appl_evc_cap_evc_t            evc;            /**< EVC capabilities */
    vtss_appl_evc_cap_ece_t            ece;            /**< ECE capabilities */
    vtss_appl_evc_cap_evc_port_t       evc_port;       /**< EVC and port capabilities */
    vtss_appl_evc_cap_port_l2cp_t      port_l2cp;      /**< EVC and L2CP capabilities */
    vtss_appl_evc_cap_evc_port_cosid_t evc_port_cosid; /**< EVC and L2CP capabilities */
} vtss_appl_evc_capabilities_t;

/****************************************************************************
 * Port configuration
 ****************************************************************************/

/** \brief EVC port configuration */
typedef struct {
    mesa_bool_t                 dei_colouring; /**< NNI: Enable colouring of DEI, valid if port.has_dei_colouring is true */
    mesa_bool_t                 inner_tag;     /**< NNI: Enable inner tag (default outer tag), valid if port.has_inner_tag is true */
    mesa_bool_t                 dmac_dip;      /**< Enable DMAC/DIP matching for basic lookup, valid if port.has_dmac_dip is true */
    mesa_vcap_key_type_t key_type;      /**< Key type for basic lookup, valid if port.has_key_type is true */
    mesa_vcap_key_type_t key_type_adv;  /**< Key type for advanced lookup, valid if port.has_key_type is true */
    mesa_bool_t                 dmac_dip_adv;  /**< Enable DMAC/DIP matching for advanced lookup, valid if port.has_key_type is true */
} vtss_appl_evc_port_conf_t;

/****************************************************************************
 * (Port, L2CP) configuration
 ****************************************************************************/

/** \brief L2CP ID, 0-15 (BPDU) and 16-31 (GARP) */
typedef uint32_t vtss_appl_l2cp_id_t;

/** \brief Port L2CP mode */
typedef enum {
    VTSS_APPL_EVC_PORT_L2CP_MODE_FORWARD, /**< Forward */
    VTSS_APPL_EVC_PORT_L2CP_MODE_DISCARD, /**< Discard */
    VTSS_APPL_EVC_PORT_L2CP_MODE_PEER     /**< Peer */
} vtss_appl_evc_port_l2cp_mode_t;

/** \brief EVC port L2CP configuration */
typedef struct {
    vtss_appl_evc_port_l2cp_mode_t mode;         /**< L2CP mode */
    mesa_bool_t                           cosid_enable; /**< COSID enable */
    mesa_cosid_t                   cosid;        /**< COSID value */
} vtss_appl_evc_port_l2cp_conf_t;

/****************************************************************************
* (EVC, Port) configuration
****************************************************************************/

/** \brief EVC port role */
typedef enum {
    VTSS_APPL_EVC_PORT_ROLE_DISABLED, /**< Port not active for EVC */
    VTSS_APPL_EVC_PORT_ROLE_NNI,      /**< Port is NNI for EVC */
    VTSS_APPL_EVC_PORT_ROLE_ROOT,     /**< Port is root UNI for EVC */
    VTSS_APPL_EVC_PORT_ROLE_LEAF,     /**< Port is leaf UNI for EVC */
} vtss_appl_evc_port_role_t;

/** \brief EVC encapsulation ID */
typedef uint16_t vtss_appl_evc_encap_id_t;

/** \brief EVC L2CP profile number */
typedef uint8_t vtss_appl_evc_l2cp_profile_t;

/** \brief EVC port configuration */
typedef struct {
    vtss_appl_evc_port_role_t    role;         /**< Port role */
    mesa_bool_t                         encap_enable; /**< Enable encapsulation */
    vtss_appl_evc_encap_id_t     encap_id;     /**< Encapsulation ID */
    mesa_bool_t                         l2cp_enable;  /**< Enable L2CP profile */
    vtss_appl_evc_l2cp_profile_t l2cp_profile; /**< L2CP profile */
    mesa_hqos_id_t               hqos_id;      /**< HQoS ID, valid if evc_port.has_hqos is true */
} vtss_appl_evc_port_evc_conf_t;

/****************************************************************************
 * Policer configuration
 ****************************************************************************/

/** \brief EVC policer mode */
typedef enum {
    VTSS_APPL_EVC_POLICER_MODE_COUPLED,
    VTSS_APPL_EVC_POLICER_MODE_AWARE,
    VTSS_APPL_EVC_POLICER_MODE_BLIND
} vtss_appl_evc_policer_mode_t;

/** \brief EVC policer configuration */
typedef struct {
    mesa_bool_t                         enable;    /**< Enable/disable policer */
    mesa_policer_type_t          type;      /**< Policer type */
    vtss_appl_evc_policer_mode_t mode;      /**< Policer mode */
    mesa_bool_t                         line_rate; /**< Line rate policing (default is data rate policing) */
    mesa_bitrate_t               cir;       /**< Committed Information Rate */
    mesa_burst_level_t           cbs;       /**< Committed Burst Size */
    mesa_bitrate_t               eir;       /**< Excess Information Rate */
    mesa_burst_level_t           ebs;       /**< Excess Burst Size */
} vtss_appl_evc_policer_conf_t;

/****************************************************************************
 * EVC configuration
 ****************************************************************************/

/** \brief EVC policer operation */
typedef enum {
    VTSS_APPL_EVC_POLICER_OP_NORMAL,  /**< Use Policer ID */
    VTSS_APPL_EVC_POLICER_OP_NONE,    /**< Use disabled policer */
    VTSS_APPL_EVC_POLICER_OP_DISCARD, /**< Use discard policer */
    VTSS_APPL_EVC_POLICER_OP_EVC,     /**< Use EVC policer (for ECE only) */
} vtss_appl_evc_policer_op_t;

/** \brief EVC/ECE inner tag type */
typedef enum {
    VTSS_APPL_EVC_INNER_TAG_NONE,    /**< No inner tag */
    VTSS_APPL_EVC_INNER_TAG_C,       /**< Inner tag is C-tag */
    VTSS_APPL_EVC_INNER_TAG_S,       /**< Inner tag is S-tag */
    VTSS_APPL_EVC_INNER_TAG_S_CUSTOM /**< Inner tag is S-custom tag */
} vtss_appl_evc_inner_tag_type_t;

/** \brief EVC inner tag */
typedef struct {
    vtss_appl_evc_inner_tag_type_t type;             /**< Tag type */
    mesa_bool_t                           vid_tunnel;       /**< VLAN ID mode */
    mesa_vid_t                     vid;              /**< VLAN ID  */
    mesa_bool_t                           pcp_dei_preserve; /**< Preserved or explicit PCP/DEI values */
    mesa_tagprio_t                 pcp;              /**< PCP value */
    mesa_dei_t                     dei;              /**< DEI value */
} vtss_appl_evc_inner_tag_t;

/** \brief EVC configuration */
typedef struct {
    mesa_vid_t                   ivid;       /**< Internal VID */
    mesa_vid_t                   vid;        /**< NNI VID of outer tag */
    mesa_port_list_t             nni_list;   /**< NNI configuration */
    mesa_vid_t                   leaf_ivid;  /**< Leaf internal VID */
    mesa_vid_t                   leaf_vid;   /**< Leaf VID of outer tag */
    mesa_port_list_t             leaf_list;  /**< Leaf configuration */
    mesa_bool_t                  learning;   /**< Enable/disable learning */
    vtss_appl_evc_policer_op_t   policer_op; /**< Policer operator, valid if evc.has_policer is true */
    mesa_evc_policer_id_t        policer_id; /**< Policer ID, valid if evc.has_policer is true */
    mesa_vid_t                   uni_vid;    /**< UNI VID of outer tag (MESA_ECE_DIR_NNI_TO_UNI only), valid if evc.has_uni_vid is true */
    vtss_appl_evc_inner_tag_t    inner_tag;  /**< Inner tag (optional), valid if evc.has_it_add is true */
    mesa_qos_ingress_map_id_t    rx_map;     /**< Ingress QoS map */
    mesa_qos_egress_map_id_t     tx_map;     /**< Egress QoS map */
    mesa_bool_t                  conflict;   /**< Status, resource conflict */
    char                         name[VTSS_APPL_EVC_NAME_MAX_LEN]; /**< EVC name */
} vtss_appl_evc_conf_t;

/** \brief PW configuration */
typedef struct {
    mesa_bool_t    split_horizon; /**< TRUE == this PW obeys split horizon; FALSE == it does not */
    vtss_ifindex_t ifindex;       /**< MPLS-TP Pseudo-Wire interface index. Zero == unused */
} vtss_appl_evc_mpls_tp_pw_conf_t;

/****************************************************************************
 * ECE configuration
 ****************************************************************************/

/** \brief ECE MAC information */
typedef struct {
    vtss_appl_vcap_dmac_type_t dmac_type; /**< DMAC match type, valid if ece.has_dmac_type is true */
    vtss_appl_vcap_mac_t       dmac;      /**< DMAC, valid if ece.has_dmac is true */
    vtss_appl_vcap_mac_t       smac;      /**< SMAC, valid if ece.has_smac is true */
} vtss_appl_evc_ece_mac_t;

/** \brief ECE tag information */
typedef struct {
    vtss_appl_vcap_vlan_tag_type_t type; /**< Tag type */
    vtss_appl_vcap_range_t         vid;  /**< VLAN ID */
    vtss_appl_vcap_vlan_pri_type_t pcp;  /**< PCP */
    mesa_vcap_bit_t                dei;  /**< DEI */
} vtss_appl_evc_ece_tag_t;

/** \brief ECE frame type */
typedef enum {
    VTSS_APPL_EVC_ECE_TYPE_ANY,   /**< Any frame type */
    VTSS_APPL_EVC_ECE_TYPE_ETYPE, /**< Ethernet Type, valid if ece.has_etype is true */
    VTSS_APPL_EVC_ECE_TYPE_LLC,   /**< LLC, valid if ece.has_llc is true */
    VTSS_APPL_EVC_ECE_TYPE_SNAP,  /**< SNAP, valid if ece.has_snap is true */
    VTSS_APPL_EVC_ECE_TYPE_L2CP,  /**< L2CP, valid if ece.has_l2cp is true */
    VTSS_APPL_EVC_ECE_TYPE_IPV4,  /**< IPv4 */
    VTSS_APPL_EVC_ECE_TYPE_IPV6   /**< IPv6 */
} vtss_appl_evc_ece_type_t;

/** \brief ECE Ethernet Type information */
typedef struct {
    vtss_appl_vcap_uint16_t etype; /**< Ethernet Type */
    vtss_appl_vcap_uint16_t data;  /**< MAC data */
} vtss_appl_evc_ece_frame_etype_t;

/** \brief ECE LLC information */
typedef struct {
    mesa_vcap_u8_t          dsap;    /**< DSAP */
    mesa_vcap_u8_t          ssap;    /**< SSAP */
    mesa_vcap_u8_t          control; /**< LLC Control */
    vtss_appl_vcap_uint16_t pid;     /**< Protocol ID */
} vtss_appl_evc_ece_frame_llc_t;

/** \brief ECE SNAP information */
typedef struct {
    vtss_appl_vcap_uint32_t oui; /**< OUI (24 bits) */
    vtss_appl_vcap_uint16_t pid; /**< Protocol ID */
} vtss_appl_evc_ece_frame_snap_t;

/** \brief ECE IPv4 information */
typedef struct {
    vtss_appl_vcap_range_t dscp;     /**< DSCP field (6 bit) */
    mesa_vcap_bit_t        fragment; /**< Fragment, valid if ece.has_fragment is true */
    mesa_vcap_u8_t         proto;    /**< Protocol, valid if ece.has_ip_proto is true */
    mesa_vcap_ip_t         sip;      /**< Source IP address, valid if ece.has_sip is true */
    mesa_vcap_ip_t         dip;      /**< Destination IP address, valid if ece.has_dip is true */
    vtss_appl_vcap_range_t sport;    /**< UDP/TCP: Source port, valid if ece.has_sport is true */
    vtss_appl_vcap_range_t dport;    /**< UDP/TCP: Destination port, valid if ece.has_dport is true */
} vtss_appl_evc_ece_frame_ipv4_t;

/** \brief ECE IPv6 information */
typedef struct {
    vtss_appl_vcap_range_t  dscp;  /**< DSCP field (6 bit) */
    mesa_vcap_u8_t          proto; /**< Protocol, valid if ece.has_ip_proto is true */
    vtss_appl_vcap_ipv6_t   sip;   /**< Source IP address, valid if ece.has_sip is true */
    vtss_appl_vcap_ipv6_t   dip;   /**< Destination IP address, valid if ece.has_dip is true */
    vtss_appl_vcap_range_t  sport; /**< UDP/TCP: Source port, valid if ece.has_sport is true */
    vtss_appl_vcap_range_t  dport; /**< UDP/TCP: Destination port, valid if ece.has_dport is true */
} vtss_appl_evc_ece_frame_ipv6_t;

/** \brief EVC L2CP type */
typedef enum {
    VTSS_APPL_EVC_L2CP_NONE,
    VTSS_APPL_EVC_L2CP_STP,
    VTSS_APPL_EVC_L2CP_PAUSE,
    VTSS_APPL_EVC_L2CP_LACP,
    VTSS_APPL_EVC_L2CP_LAMP,
    VTSS_APPL_EVC_L2CP_LOAM,
    VTSS_APPL_EVC_L2CP_DOT1X,
    VTSS_APPL_EVC_L2CP_ELMI,
    VTSS_APPL_EVC_L2CP_PB,
    VTSS_APPL_EVC_L2CP_PB_GVRP,
    VTSS_APPL_EVC_L2CP_LLDP,
    VTSS_APPL_EVC_L2CP_GMRP,
    VTSS_APPL_EVC_L2CP_GVRP,
    VTSS_APPL_EVC_L2CP_ULD,
    VTSS_APPL_EVC_L2CP_PAGP,
    VTSS_APPL_EVC_L2CP_PVST,
    VTSS_APPL_EVC_L2CP_CISCO_VLAN,
    VTSS_APPL_EVC_L2CP_CDP,
    VTSS_APPL_EVC_L2CP_VTP,
    VTSS_APPL_EVC_L2CP_DTP,
    VTSS_APPL_EVC_L2CP_CISCO_STP,
    VTSS_APPL_EVC_L2CP_CISCO_CFM,

    VTSS_APPL_EVC_L2CP_CNT /* Number of protocols */
} vtss_appl_evc_l2cp_t;

/** \brief ECE L2CP information */
typedef struct {
    vtss_appl_evc_l2cp_t proto; /**< L2CP protocol */
} vtss_appl_evc_ece_frame_l2cp_t;

/** \brief ECE frame fields */
typedef struct {
    vtss_appl_evc_ece_frame_etype_t etype; /**< VTSS_APPL_EVC_ECE_TYPE_ETYPE, valid if ece.has_etype is TRUE */
    vtss_appl_evc_ece_frame_llc_t   llc;   /**< VTSS_APPL_EVC_ECE_TYPE_LLC, valid if ece.has_llc is TRUE */
    vtss_appl_evc_ece_frame_snap_t  snap;  /**< VTSS_APPL_EVC_ECE_TYPE_SNAP, valid if ece.has_snap is TRUE */
    vtss_appl_evc_ece_frame_l2cp_t  l2cp;  /**< VTSS_APPL_EVC_ECE_TYPE_L2CP, valid if ece.has_l2cp is true */
    vtss_appl_evc_ece_frame_ipv4_t  ipv4;  /**< VTSS_APPL_EVC_ECE_TYPE_IPV4 */
    vtss_appl_evc_ece_frame_ipv6_t  ipv6;  /**< VTSS_APPL_EVC_ECE_TYPE_IPV6 */
} vtss_appl_evc_ece_frame_t; /**< Frame type specific data */

/** \brief ECE key */
typedef struct {
    mesa_port_list_t          uni_list;                        /**< UNI port list */
    vtss_appl_evc_ece_mac_t   mac;                             /**< MAC header */
    vtss_appl_evc_ece_tag_t   tag;                             /**< Tag */
    vtss_appl_evc_ece_tag_t   inner_tag;                       /**< Inner tag, valid if ece.has_inner_tag is true */
    vtss_appl_evc_ece_type_t  type;                            /**< Frame type */
    mesa_bool_t                      adv_lookup;                      /**< Advanced lookup, valid if ece.has_adv_lookup is true */
    vtss_appl_evc_ece_frame_t frame;                           /**< Frame type specific data */
} vtss_appl_evc_ece_key_t;

/** \brief ECE rule types */
typedef enum {
    VTSS_APPL_ECE_RULE_BOTH, /**< Ingress and egress rules */
    VTSS_APPL_ECE_RULE_RX,   /**< Ingress rules */
    VTSS_APPL_ECE_RULE_TX    /**< Egress rules */
} vtss_appl_ece_rule_t;

/** \brief ECE egress lookup types */
typedef enum {
    VTSS_APPL_ECE_TX_LOOKUP_VID,     /**< VID lookup */
    VTSS_APPL_ECE_TX_LOOKUP_VID_PCP, /**< (VID, PCP) lookup */
    VTSS_APPL_ECE_TX_LOOKUP_ISDX     /**< ISDX lookup */
} vtss_appl_ece_tx_lookup_t;

/** \brief PCP mode */
typedef enum {
    VTSS_APPL_ECE_PCP_MODE_CLASSIFIED, /**< Classified PCP */
    VTSS_APPL_ECE_PCP_MODE_FIXED,      /**< Fixed PCP */
    VTSS_APPL_ECE_PCP_MODE_MAPPED      /**< PCP based on mapped (QOS, DP) */
} vtss_appl_ece_pcp_mode_t;

/** \brief DEI mode */
typedef enum {
    VTSS_APPL_ECE_DEI_MODE_CLASSIFIED, /**< Classified DEI */
    VTSS_APPL_ECE_DEI_MODE_FIXED,      /**< Fixed DEI */
    VTSS_APPL_ECE_DEI_MODE_DP          /**< DP-based DEI */
} vtss_appl_ece_dei_mode_t;

/** \brief ECE outer tag */
typedef struct {
    mesa_bool_t                     enable;           /**< Enable tag (MESA_ECE_DIR_NNI_TO_UNI only) */
    mesa_vid_t               vid;              /**< VLAN ID (MESA_ECE_DIR_NNI_TO_UNI only), valid if ece.has_ot_vid is true */
    vtss_appl_ece_pcp_mode_t pcp_mode;         /**< PCP mode, valid if ece.has_pcp_mode is true */
    mesa_bool_t                     pcp_dei_preserve; /**< Preserved or explicit PCP/DEI value, valid if ece.has_pcp_dei_pres is true */
    mesa_tagprio_t           pcp;              /**< PCP value */
    vtss_appl_ece_dei_mode_t dei_mode;         /**< DEI mode, valid if ece.has_dei_mode is true */
    mesa_dei_t               dei;              /**< DEI value (ignored if colouring enabled) */
} vtss_appl_evc_ece_outer_tag_t;

/** \brief ECE inner tag */
typedef struct {
    vtss_appl_evc_inner_tag_type_t type;             /**< Tag type */
    mesa_vid_t                     vid;              /**< VLAN ID  */
    vtss_appl_ece_pcp_mode_t       pcp_mode;         /**< PCP mode, valid if ece.has_pcp_mode is true */
    mesa_bool_t                           pcp_dei_preserve; /**< Preserved or explicit PCP/DEI values, valid if ece.has_pcp_dei_pres is true */
    mesa_tagprio_t                 pcp;              /**< PCP value */
    vtss_appl_ece_dei_mode_t       dei_mode;         /**< DEI mode, valid if ece.has_dei_mode is true */
    mesa_dei_t                     dei;              /**< DEI value */
} vtss_appl_evc_ece_inner_tag_t;

/** \brief L2CP forward mode */
typedef enum {
    VTSS_APPL_EVC_L2CP_MODE_FORWARD, /**< Forward normally */
    VTSS_APPL_EVC_L2CP_MODE_DISCARD, /**< Discard frames */
    VTSS_APPL_EVC_L2CP_MODE_PEER,    /**< Redirect frames to CPU */
    VTSS_APPL_EVC_L2CP_MODE_TUNNEL   /**< Forward and replace DMAC */
} vtss_appl_evc_l2cp_mode_t;

/** \brief L2CP tunnel DMAC */
typedef enum {
    VTSS_APPL_EVC_L2CP_DMAC_CUSTOM,  /**< Custom DMAC used for tunnel forwarding */
    VTSS_APPL_EVC_L2CP_DMAC_CISCO    /**< Cisco DMAC used for tunnel forwarding */
} vtss_appl_evc_l2cp_dmac_t;

/** \brief ECE action */
typedef struct {
    mesa_ece_dir_t                dir;         /**< Traffic direction */
    vtss_appl_ece_rule_t          rule;        /**< Rule type, valid if ece.has_rule is true */
    vtss_appl_ece_tx_lookup_t     tx_lookup;   /**< Egress lookup type, valid if ece.has_tx_lookup is true */
    mesa_ece_pop_tag_t            pop_tag;     /**< Ingress VLAN popping */
    vtss_appl_evc_ece_outer_tag_t outer_tag;   /**< Egress outer VLAN tag (always present) */
    vtss_appl_evc_ece_inner_tag_t inner_tag;   /**< Egress inner VLAN tag (optional) */
    vtss_appl_evc_policer_op_t    policer_op;  /**< Policer operator, valid if ece.has_policer is true */
    mesa_evc_policer_id_t         policer_id;  /**< Policer ID, valid if ece.has_policer is true */
    mesa_evc_id_t                 evc_id;      /**< EVC ID */
    mesa_acl_policy_no_t          policy_no;   /**< ACL policy number */
    mesa_bool_t                          prio_enable; /**< Enable priority classification, valid if ece.has_prio is true */
    mesa_prio_t                   prio;        /**< Priority (QoS class), valid if ece.has_prio is true */
    mesa_bool_t                          dp_enable;   /**< Enable DP classification, valid if ece.has_dp is true */
    mesa_dp_level_t               dp;          /**< Drop precedence, valid if ece.has_dp is true */
    vtss_appl_evc_l2cp_mode_t     l2cp_mode;   /**< L2CP forward mode, valid if ece.has_l2cp is true */
    vtss_appl_evc_l2cp_dmac_t     l2cp_dmac;   /**< L2CP tunnel DMAC, valid if ece.has_l2cp is true */
    mesa_qos_ingress_map_id_t     rx_map;      /**< Ingress QoS map */
} vtss_appl_evc_ece_action_t;

/** \brief ECE configuration */
typedef struct {
    mesa_ece_id_t              id;       /**< ECE ID */
    mesa_ece_id_t              id_next;  /**< Next ECE ID or VTSS_ECE_ID_LAST */
    vtss_appl_evc_ece_key_t    key;      /**< ECE key */
    vtss_appl_evc_ece_action_t action;   /**< ECE action */
    mesa_bool_t                       conflict; /**< Status, resource conflict */
} vtss_appl_evc_ece_conf_t;

/****************************************************************************
 * EVC encapsulation configuration
 ****************************************************************************/

/** \brief EVC encapsulation configuration */
typedef struct {
    mesa_vid_t vid;    /**< VLAN ID */
    uint16_t        tx_map; /**< Egress map ID */
} vtss_appl_evc_encap_conf_t;

/****************************************************************************
 * L2CP profile configuration
 ****************************************************************************/

/** \brief L2CP profile configuration */
typedef struct {
    vtss_appl_evc_port_l2cp_mode_t mode;         /**< L2CP mode */
    mesa_bool_t                           cosid_enable; /**< COSID enable */
    mesa_cosid_t                   cosid;        /**< COSID value */
} vtss_appl_evc_l2cp_conf_t;

/****************************************************************************
 * EVC/ECE counters
 ****************************************************************************/

/** \brief EVC/ECE statistics, valid if evc_port.has_counters/ece_port.has_counters is true */
typedef struct {
    mesa_counter_pair_t rx_green;   /**< Rx green frames/bytes */
    mesa_counter_pair_t rx_yellow;  /**< Rx yellow frames/bytes */
    mesa_counter_pair_t rx_red;     /**< Rx red frames/bytes */
    mesa_counter_pair_t rx_discard; /**< Rx discarded frames/bytes */
    mesa_counter_pair_t tx_discard; /**< Tx discarded frames/bytes */
    mesa_counter_pair_t tx_green;   /**< Tx green frames/bytes */
    mesa_counter_pair_t tx_yellow;  /**< Tx yellow frames/bytes */
} vtss_appl_evc_counters_t;

/** \brief EVC control */
typedef struct {
    mesa_bool_t clear; /**< Clear counters */
} vtss_appl_evc_control_t;

#ifdef __cplusplus
extern "C" {
#endif

/****************************************************************************
 * Capabilities
 ****************************************************************************/

/**
 * \brief Get EVC capabilities.
 *
 * \param cap [OUT]  The EVC capabilities.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_capabilities_get(vtss_appl_evc_capabilities_t *const cap);

/****************************************************************************
 * Port configuration
 ****************************************************************************/

/**
 * \brief Get EVC interface configuration.
 *
 * \param ifindex [IN]  The interface index.
 * \param conf [OUT]    The interface configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_interface_conf_get(const vtss_ifindex_t ifindex,
                                         vtss_appl_evc_port_conf_t *const conf);

/**
 * \brief Set EVC interface configuration.
 *
 * \param ifindex [IN] The interface index.
 * \param conf [IN]    The interface configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_interface_conf_set(const vtss_ifindex_t ifindex,
                                         const vtss_appl_evc_port_conf_t *const conf);

/****************************************************************************
 * (Port, L2CP) configuration
 ****************************************************************************/

/**
 * \brief Iterator for interface and L2CP ID.
 *
 * \param prev_ifindex [IN]   The previous interface index.
 * \param next_ifindex [OUT]  The next interface index.
 * \param prev_l2cp_id [IN]   The previous L2CP ID.
 * \param next_l2cp_id [OUT]  The next L2CP ID.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_interface_l2cp_itr(const vtss_ifindex_t *prev_ifindex, vtss_ifindex_t *next_ifindex,
                                         const vtss_appl_l2cp_id_t *prev_l2cp_id, vtss_appl_l2cp_id_t *next_l2cp_id);

/**
 * \brief Get interface L2CP configuration.
 *
 * \param ifindex [IN]  The interface index.
 * \param l2cp_id [IN]  The L2CP ID.
 * \param conf [OUT]    The L2CP configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_interface_l2cp_conf_get(const vtss_ifindex_t ifindex,
                                              const vtss_appl_l2cp_id_t l2cp_id,
                                              vtss_appl_evc_port_l2cp_conf_t *const conf);

/**
 * \brief Set interface L2CP configuration.
 *
 * \param ifindex [IN]  The interface index.
 * \param l2cp_id [IN]  The L2CP ID.
 * \param conf [IN]     The L2CP configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_interface_l2cp_conf_set(const vtss_ifindex_t ifindex,
                                              const vtss_appl_l2cp_id_t l2cp_id,
                                              const vtss_appl_evc_port_l2cp_conf_t *const conf);

/****************************************************************************
 * (EVC, Port) configuration
 ****************************************************************************/

/**
 * \brief Iterator for EVC ID and interface.
 *
 * \param prev_evc_id [IN]    The previous EVC ID.
 * \param next_evc_id [OUT]   The next EVC ID.
 * \param prev_ifindex [IN]   The previous interface index.
 * \param next_ifindex [OUT]  The next interface index.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_interface_evc_itr(const mesa_evc_id_t *prev_evc_id, mesa_evc_id_t *next_evc_id,
                                        const vtss_ifindex_t *prev_ifindex, vtss_ifindex_t *next_ifindex);

/**
 * \brief Get EVC interface configuration.
 *
 * \param evc_id [IN]   The EVC ID.
 * \param ifindex [IN]  The interface index.
 * \param conf [OUT]    The EVC configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_interface_evc_conf_get(const mesa_evc_id_t evc_id,
                                             const vtss_ifindex_t ifindex,
                                             vtss_appl_evc_port_evc_conf_t *const conf);

/**
 * \brief Set EVC interface configuration.
 *
 * \param evc_id [IN]   The EVC ID.
 * \param ifindex [IN]  The interface index.
 * \param conf [OUT]    The EVC configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_interface_evc_conf_set(const mesa_evc_id_t evc_id,
                                             const vtss_ifindex_t ifindex,
                                             const vtss_appl_evc_port_evc_conf_t *const conf);

/****************************************************************************
 * Policer configuration
 ****************************************************************************/

/**
 * \brief Iterator for EVC policer.
 *
 * \param prev_policer_id [IN]   The previous EVC policer ID.
 * \param next_policer_id [OUT]  The next EVC policer ID.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_policer_itr(const mesa_evc_policer_id_t *prev_policer_id,
                                  mesa_evc_policer_id_t *next_policer_id);

/**
 * \brief Get EVC policer configuration.
 *
 * \param policer_id [IN]  The EVC policer ID.
 * \param conf [OUT]       The EVC policer configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_policer_conf_get(const mesa_evc_policer_id_t policer_id,
                                       vtss_appl_evc_policer_conf_t *const conf);

/**
 * \brief Set EVC policer configuration.
 *
 * \param policer_id [IN]  The EVC policer ID.
 * \param conf [IN]        The EVC policer configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_policer_conf_set(const mesa_evc_policer_id_t policer_id,
                                       const vtss_appl_evc_policer_conf_t *const conf);

/****************************************************************************
 * EVC configuration
 ****************************************************************************/

/**
 * \brief Iterator for EVC configuration.
 *
 * \param prev_evc_id [IN]   The previous EVC ID or NULL.
 * \param next_evc_id [OUT]  The next EVC ID.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_conf_itr(const mesa_evc_id_t *const prev_evc_id,
                               mesa_evc_id_t       *const next_evc_id);

/**
 * \brief Get EVC configuration.
 *
 * \param evc_id [IN]   The EVC ID.
 * \param conf   [OUT]  The EVC configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_conf_get(const mesa_evc_id_t evc_id,
                               vtss_appl_evc_conf_t *const conf);

/**
 * \brief Set EVC configuration.
 *
 * \param evc_id [IN]  The EVC ID.
 * \param conf   [IN]  The EVC configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_conf_set(const mesa_evc_id_t evc_id,
                               const vtss_appl_evc_conf_t *const conf);

/**
 * \brief Delete EVC.
 *
 * \param evc_id [IN]  The EVC ID.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_conf_del(const mesa_evc_id_t evc_id);

/**
 * \brief Default EVC configuration.
 *
 * \param evc_id [OUT]  The EVC ID.
 * \param conf   [OUT]  The EVC configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_conf_def(mesa_evc_id_t *evc_id,
                               vtss_appl_evc_conf_t *const conf);

/****************************************************************************
 * EVC/MPLS configuration
 ****************************************************************************/

/**
 * \brief Iterator for EVC MPLS PW configuration.
 *
 * \param prev_evc_id     [IN]   The previous EVC ID or NULL.
 * \param next_evc_id     [OUT]  The next EVC ID.
 * \param prev_pw_ifindex [IN]   The previous PW interface index or NULL.
 * \param next_pw_ifindex [OUT]  The next PW interface index.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_mpls_pw_conf_itr(const mesa_evc_id_t  *const prev_evc_id,
                                       mesa_evc_id_t  *const next_evc_id,
                                       const vtss_ifindex_t *const prev_pw_ifindex,
                                       vtss_ifindex_t *const next_pw_ifindex);

/**
 * \brief Get EVC MPLS PW configuration.
 *
 * \param evc_id     [IN]   The EVC ID.
 * \param pw_ifindex [IN]   The PW interface index.
 * \param conf       [OUT]  The EVC/MPLS configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_mpls_pw_conf_get(const mesa_evc_id_t  evc_id,
                                       const vtss_ifindex_t pw_ifindex,
                                       vtss_appl_evc_mpls_tp_pw_conf_t *const conf);

/**
 * \brief Set EVC MPLS PW configuration.
 *
 * \param evc_id     [IN]  The EVC ID.
 * \param pw_ifindex [IN]  The PW interface index.
 * \param conf       [IN]  The EVC/MPLS configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_mpls_pw_conf_set(const mesa_evc_id_t  evc_id,
                                       const vtss_ifindex_t pw_ifindex,
                                       const vtss_appl_evc_mpls_tp_pw_conf_t *const conf);

/**
 * \brief Add EVC MPLS PW configuration.
 *
 * \param evc_id     [IN]  The EVC ID.
 * \param pw_ifindex [IN]  The PW interface index.
 * \param conf       [IN]  The EVC/MPLS configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_mpls_pw_conf_add(const mesa_evc_id_t  evc_id,
                                       const vtss_ifindex_t pw_ifindex,
                                       const vtss_appl_evc_mpls_tp_pw_conf_t *const conf);

/**
 * \brief Delete MPLS PW from EVC.
 *
 * \param evc_id     [IN]  The EVC ID.
 * \param pw_ifindex [IN]  The PW interface index.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_mpls_pw_conf_del(const mesa_evc_id_t  evc_id,
                                       const vtss_ifindex_t pw_ifindex);

/**
 * \brief Default EVC MPLS PW configuration.
 *
 * \param evc_id     [OUT]  The EVC ID.
 * \param pw_ifindex [OUT]  The PW interface index.
 * \param conf       [OUT]  The EVC/MPLS configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_mpls_pw_conf_default(mesa_evc_id_t  *const evc_id,
                                           vtss_ifindex_t *const pw_ifindex,
                                           vtss_appl_evc_mpls_tp_pw_conf_t *const conf);

/****************************************************************************
 * ECE configuration
 ****************************************************************************/

/**
 * \brief Iterator for ECE configuration based on ECE ID numbers.
 *
 * \param prev_ece_id [IN]   The previous ECE ID or NULL.
 * \param next_ece_id [OUT]  The next ECE ID.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_ece_conf_itr(const mesa_ece_id_t *const prev_ece_id,
                                   mesa_ece_id_t       *const next_ece_id);

/**
 * \brief Get ECE configuration.
 *
 * \param ece_id [IN]   The ECE ID.
 * \param conf   [OUT]  The ECE configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_ece_conf_get(const mesa_ece_id_t ece_id,
                                   vtss_appl_evc_ece_conf_t *const conf);

/**
 * \brief Set ECE configuration.
 *
 * \param ece_id [IN]  The ECE ID.
 * \param conf   [IN]  The ECE configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_ece_conf_set(const mesa_ece_id_t ece_id,
                                   const vtss_appl_evc_ece_conf_t *const conf);

/**
 * \brief Delete ECE.
 *
 * \param ece_id [IN]  The ECE ID.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_ece_conf_del(const mesa_ece_id_t ece_id);

/**
 * \brief Iterator for ECE configuration based on ECE precedence.
 *        The ECE precedence is a 0-based index calculated based on the order of ECEs.
 *
 * \param prev_index [IN]   The previous index.
 * \param next_index [OUT]  The next index.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_ece_precedence_itr(const uint32_t *prev_index, uint32_t *const next_index);


/**
 * \brief Get ECE configuration for precedence index
 *
 * \param ndx [IN]    The precedence index.
 * \param conf [OUT]  The ECE configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_ece_precedence_get(const uint32_t ndx, vtss_appl_evc_ece_conf_t *const conf);

/****************************************************************************
 * EVC encapsulation configuration
 ****************************************************************************/

/**
 * \brief Iterator for EVC encapsulations.
 *
 * \param prev_encap_id [IN]   The previous EVC encapsulation ID.
 * \param next_encap_id [OUT]  The next EVC encapsulation ID.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_encap_itr(const vtss_appl_evc_encap_id_t *prev_encap_id,
                                vtss_appl_evc_encap_id_t *next_encap_id);

/**
 * \brief Get EVC encapsulation configuration.
 *
 * \param id   [IN]   The encapsulation ID.
 * \param conf [OUT]  The encapulation configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_encap_conf_get(const vtss_appl_evc_encap_id_t id,
                                     vtss_appl_evc_encap_conf_t *const conf);

/**
 * \brief Set EVC encapsulation configuration.
 *
 * \param id   [IN]  The encapsulation ID.
 * \param conf [IN]  The encapulation configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_encap_conf_set(const vtss_appl_evc_encap_id_t   id,
                                     const vtss_appl_evc_encap_conf_t *const conf);

/****************************************************************************
 * L2CP profile configuration
 ****************************************************************************/

/**
 * \brief Iterator for L2CP profiles.
 *
 * \param prev_profile_id [IN]   The previous L2CP profile ID.
 * \param next_profile_id [OUT]  The next L2CP profile ID.
 * \param prev_l2cp_id    [IN]   The previous L2CP ID.
 * \param next_l2cp_id    [OUT]  The next L2CP ID.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_l2cp_itr(const vtss_appl_evc_l2cp_profile_t *prev_profile_id,
                               vtss_appl_evc_l2cp_profile_t *next_profile_id,
                               const vtss_appl_l2cp_id_t *prev_l2cp_id,
                               vtss_appl_l2cp_id_t *next_l2cp_id);

/**
 * \brief Get L2CP profile configuration.
 *
 * \param profile [IN]   The L2CP profile.
 * \param id      [IN]   The L2CP ID.
 * \param conf    [OUT]  The ECE configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_l2cp_conf_get(const vtss_appl_evc_l2cp_profile_t profile,
                                    const vtss_appl_l2cp_id_t          id,
                                    vtss_appl_evc_l2cp_conf_t          *const conf);

/**
 * \brief Set L2CP profile configuration.
 *
 * \param profile [IN]  The L2CP profile.
 * \param id      [IN]  The L2CP ID.
 * \param conf    [IN]  The ECE configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_evc_l2cp_conf_set(const vtss_appl_evc_l2cp_profile_t profile,
                                    const vtss_appl_l2cp_id_t          id,
                                    const vtss_appl_evc_l2cp_conf_t    *const conf);

/****************************************************************************
 * (EVC, port, COSID) policer configuration
 ****************************************************************************/

/**
 * \brief Iterator for EVC COSID policer/statistics.
 *
 * \param prev_evc_id  [IN]   The previous EVC ID.
 * \param next_evc_id  [OUT]  The next EVC ID.
 * \param prev_ifindex [IN]   The previous interface index.
 * \param next_ifindex [OUT]  The next interface index.
 * \param prev_cosid   [IN]   The previous interface index.
 * \param next_cosid   [OUT]  The next interface index.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_cosid_itr(const mesa_evc_id_t  *prev_evc_id,
                                mesa_evc_id_t        *next_evc_id,
                                const vtss_ifindex_t *prev_ifindex,
                                vtss_ifindex_t       *next_ifindex,
                                const mesa_cosid_t   *prev_cosid,
                                mesa_cosid_t         *next_cosid);

/**
 * \brief Get EVC COSID policer configuration.
 *
 * \param evc_id [IN]   The EVC ID.
 * \param ifindex [IN]  The interface index.
 * \param cosid [IN]    The COSID.
 * \param conf [OUT]    The EVC COSID policer configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_cosid_policer_conf_get(const mesa_evc_id_t evc_id,
                                             const vtss_ifindex_t ifindex,
                                             const mesa_cosid_t cosid,
                                             vtss_appl_evc_policer_conf_t *const conf);

/**
 * \brief Set EVC COSID policer configuration.
 *
 * \param evc_id [IN]   The EVC ID.
 * \param ifindex [IN]  The interface index.
 * \param cosid [IN]    The COSID.
 * \param conf [IN]     The EVC COSID policer configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_cosid_policer_conf_set(const mesa_evc_id_t evc_id,
                                             const vtss_ifindex_t ifindex,
                                             const mesa_cosid_t cosid,
                                             const vtss_appl_evc_policer_conf_t *const conf);

/****************************************************************************
 * (EVC, port) statistics
 ****************************************************************************/

/**
 * \brief Iterator for EVC ID and interface.
 *
 * \param prev_evc_id [IN]    The previous EVC ID.
 * \param next_evc_id [OUT]   The next EVC ID.
 * \param prev_ifindex [IN]   The previous interface index.
 * \param next_ifindex [OUT]  The next interface index.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_interface_counters_itr(const mesa_evc_id_t *prev_evc_id, mesa_evc_id_t *next_evc_id,
                                             const vtss_ifindex_t *prev_ifindex, vtss_ifindex_t *next_ifindex);

/**
 * \brief Get EVC counters for interface.
 *
 * \param evc_id [IN]   The EVC ID.
 * \param ifindex [IN]  The interface index.
 * \param counters [OUT]   The EVC configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_interface_counters_get(const mesa_evc_id_t evc_id,
                                             const vtss_ifindex_t ifindex,
                                             vtss_appl_evc_counters_t *const counters);

/**
 * \brief Get EVC control for interface.
 *
 * \param evc_id [IN]    The EVC ID.
 * \param ifindex [IN]   The interface index.
 * \param control [OUT]  The EVC control structure.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_interface_control_get(const mesa_evc_id_t evc_id,
                                            const vtss_ifindex_t ifindex,
                                            vtss_appl_evc_control_t *const control);

/**
 * \brief Set EVC control for interface.
 *
 * \param evc_id [IN]   The EVC ID.
 * \param ifindex [IN]  The interface index.
 * \param control [IN]  The EVC control structure.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_interface_control_set(const mesa_evc_id_t evc_id,
                                            const vtss_ifindex_t ifindex,
                                            const vtss_appl_evc_control_t *const control);

/****************************************************************************
 * (ECE, port) statistics
 ****************************************************************************/

/**
 * \brief Iterator for ECE ID and interface.
 *
 * \param prev_index [IN]     The previous index.
 * \param next_index [OUT]    The next index.
 * \param prev_ifindex [IN]   The previous interface index.
 * \param next_ifindex [OUT]  The next interface index.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_ece_interface_counters_itr(const mesa_ece_id_t *prev_index, mesa_ece_id_t *next_index,
                                                 const vtss_ifindex_t *prev_ifindex, vtss_ifindex_t *next_ifindex);

/**
 * \brief Get ECE counters for interface.
 *
 * \param ndx [IN]        The ECE index.
 * \param ifindex [IN]    The interface index.
 * \param counters [OUT]  The ECE configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_ece_interface_counters_get(const uint32_t ndx,
                                                 const vtss_ifindex_t ifindex,
                                                 vtss_appl_evc_counters_t *const counters);
/**
 * \brief Get ECE control for interface.
 *
 * \param ndx [IN]       The ECE index.
 * \param ifindex [IN]   The interface index.
 * \param control [OUT]  The EVC control structure.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_ece_interface_control_get(const uint32_t ndx,
                                                const vtss_ifindex_t ifindex,
                                                vtss_appl_evc_control_t *const control);

/**
 * \brief Set ECE control for interface.
 *
 * \param ndx [IN]      The EVC ID.
 * \param ifindex [IN]  The interface index.
 * \param control [IN]  The EVC control structure.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_ece_interface_control_set(const uint32_t ndx,
                                                const vtss_ifindex_t ifindex,
                                                const vtss_appl_evc_control_t *const control);

/****************************************************************************
 * (EVC, port, COSID) statistics
 ****************************************************************************/

/**
 * \brief Get EVC counters for interface and COSID.
 *
 * \param evc_id [IN]     The EVC ID.
 * \param ifindex [IN]    The interface index.
 * \param cosid [IN]      The COSID.
 * \param counters [OUT]  The EVC configuration.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_cosid_counters_get(const mesa_evc_id_t evc_id,
                                         const vtss_ifindex_t ifindex,
                                         const mesa_cosid_t cosid,
                                         vtss_appl_evc_counters_t *const counters);

/**
 * \brief Get EVC control for interface and COSID.
 *
 * \param evc_id [IN]    The EVC ID.
 * \param ifindex [IN]   The interface index.
 * \param cosid [IN]     The COSID.
 * \param control [OUT]  The EVC control structure.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_cosid_control_get(const mesa_evc_id_t evc_id,
                                        const vtss_ifindex_t ifindex,
                                        const mesa_cosid_t cosid,
                                        vtss_appl_evc_control_t *const control);

/**
 * \brief Set EVC control for interface and COSID.
 *
 * \param evc_id [IN]   The EVC ID.
 * \param ifindex [IN]  The interface index.
 * \param cosid [IN]    The COSID.
 * \param control [IN]  The EVC control structure.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_cosid_control_set(const mesa_evc_id_t evc_id,
                                        const vtss_ifindex_t ifindex,
                                        const mesa_cosid_t cosid,
                                        const vtss_appl_evc_control_t *const control);
/**
 * \brief Initialize the vtss_appl_evc_control_t structure with default values.
 *
 * \param evc_id [OUT]   The EVC ID.
 * \param ifindex [OUT]  The interface index.
 * \param cosid [OUT]    The COSID.
 * \param control [OUT]  The EVC control structure.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_evc_cosid_control_def(mesa_evc_id_t *evc_id,
                                        vtss_ifindex_t *ifindex,
                                        mesa_cosid_t *cosid,
                                        vtss_appl_evc_control_t *control);

#ifdef __cplusplus
}
#endif

#endif /* _VTSS_APPL_EVC_H_ */
