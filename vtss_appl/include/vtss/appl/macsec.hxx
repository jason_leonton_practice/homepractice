/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

/**
* \file
* \brief Public MACSEC API
* \details This header file describes MACSEC control functions and types.
*/

#ifndef _VTSS_APPL_MACSEC_H_
#define _VTSS_APPL_MACSEC_H_

#include "mscc/ethernet/switch/api.h"

#define MESA_MACSEC_MAX_SA (64)
#define MESA_MACSEC_MAX_SC (MESA_MACSEC_MAX_SA/2)

/** \brief No. of  Tx SC and Rx SC Information */
typedef struct {
    uint8_t no_txsc;                       /**< No. of Tx SCs configured */
    uint8_t txsc_id;                       /**< Configured Tx SC ids */
    mesa_macsec_sci_t tx_sci;              /**< Tx SCI */
    mesa_sc_inst_count_t txsc_inst_count;  /**< Tx SC Instances */

    uint8_t no_rxsc;                       /**< No. of Rx SCs configured */
    uint8_t rxsc_id[MESA_MACSEC_MAX_SC];   /**< Configured Rx SC ids */
    mesa_macsec_sci_t rx_sci[MESA_MACSEC_MAX_SC];             /**< Rx SCIs */
    mesa_sc_inst_count_t rxsc_inst_count[MESA_MACSEC_MAX_SC]; /**< Rx SCs Instances */
} macsec_secy_inst_count_t;

/** \brief No. of  SecYs, Virtual Port Information */
typedef struct {
    uint8_t no_secy;                       /**< No. of SecYs configured */
    uint8_t secy_vport[MESA_MACSEC_MAX_SC]; /**< Configured SecY virtual port */
    macsec_secy_inst_count_t secy_inst_count[MESA_MACSEC_MAX_SC]; /**< SecY Instances */
} macsec_inst_count_t;

namespace vtss {
namespace appl {
namespace macsec {

mesa_rc macsec_inst_count_get(const mesa_inst_t      inst,
                              const mesa_port_no_t   port_no,
                              macsec_inst_count_t   *count);

} // namespace vtss
} // namespace appl
} // namespace macsec

/** List of functions to export */
#define MODULE_MACSEC_API        \
    X(macsec_inst_count_get)

/** Setup module parameters */
#define PP_MODULE_NAME macsec
/** Setup module parameters */
#define PP_MODULE_METHOD_LIST MODULE_MACSEC_API
#include "vtss/appl/optional_modules_create.hxx"

/**
 * \brief Function to check if module is enabled.
 * \return bool
 * */
inline bool macsec_module_enabled() { return vtss_appl_module_macsec; }

#endif /* _VTSS_APPL_MACSEC_H_ */
