/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

/**
 * \file
 * \brief RFC2544 API
 * \details This header file describes public functions applicable to RFC2544 control.
 */

#ifndef _VTSS_APPL_RFC2544_H_
#define _VTSS_APPL_RFC2544_H_

#include <mscc/ethernet/switch/api/types.h>      /* For various vtss_XXX types         */
#include <vtss/appl/defines.h>   /* For VTSS_APPL_RFC3339_TIME_STR_LEN */
#include <vtss/appl/interface.h> /* For vtss_ifindex_t                 */

#define VTSS_APPL_RFC2544_PROFILE_NAME_LEN  33 /**< Maximum length (including null-termination) of profile name string.        */
#define VTSS_APPL_RFC2544_PROFILE_DSCR_LEN 129 /**< Maximum length (including null-termination) of profile description string. */
#define VTSS_APPL_RFC2544_REPORT_NAME_LEN   33 /**< Maximum length (including null-termination) of test report name.           */
#define VTSS_APPL_RFC2544_REPORT_DSCR_LEN  129 /**< Maximum length (including null-termination) of report description string.  */
#define VTSS_APPL_RFC2544_REPORT_PATH_LEN  129 /**< Maximum length (including null-termination) of the report path.            */

/**
 * Manifest constants for the RFC2544 module.
 */
typedef struct {
    /**
     * Number of profiles that can be defined and saved to non-volatile memory.
     */
    uint32_t profile_cnt;

    /**
     * Number of reports that can be saved to non-volatile memory.
     */
    uint32_t report_cnt;

    /**
     * Maximum number of concurrently running tests.
     */
    uint32_t concurrently_executing_max;
} vtss_appl_rfc2544_capabilities_t;

/**
 * Structure for specifying which frame sizes to run the test with.
 * At least one must be selected.
 */
typedef struct {
    /**
     * Set to TRUE to enable test with 64-byte frames
     */
    mesa_bool_t _64;

    /**
     * Set to TRUE to enable test with 128-byte frames
     */
    mesa_bool_t _128;

    /**
     * Set to TRUE to enable test with 256-byte frames
     */
    mesa_bool_t _256;

    /**
     * Set to TRUE to enable test with 512-byte frames
     */
    mesa_bool_t _512;

    /**
     * Set to TRUE to enable test with 1024-byte frames
     */
    mesa_bool_t _1024;

    /**
     * Set to TRUE to enable test with 1280-byte frames
     */
    mesa_bool_t _1280;

    /**
     * Set to TRUE to enable test with 1518-byte frames
     */
    mesa_bool_t _1518;

    /**
     * Set to TRUE to enable test with 2000-byte frames
     */
    mesa_bool_t _2000;

    /**
     * Set to TRUE to enable test with 9600-byte frames
     */
    mesa_bool_t _9600;
} vtss_appl_rfc2544_frame_sizes_t;

/**
 * Test report status.
 */
typedef enum {
    VTSS_APPL_RFC2544_TEST_STATUS_INACTIVE,   /**< Current test has never been executed. */
    VTSS_APPL_RFC2544_TEST_STATUS_EXECUTING,  /**< Test is currently executing.          */
    VTSS_APPL_RFC2544_TEST_STATUS_CANCELLING, /**< Test is under cancellation.           */
    VTSS_APPL_RFC2544_TEST_STATUS_CANCELLED,  /**< Test was cancelled by user.           */
    VTSS_APPL_RFC2544_TEST_STATUS_PASSED,     /**< Test terminated successfully.         */
    VTSS_APPL_RFC2544_TEST_STATUS_FAILED,     /**< Test failed.                          */
    VTSS_APPL_RFC2544_TEST_STATUS_FAILING,    /**< Test has currently been set to fail.  */
} vtss_appl_rfc2544_test_status_t;

/**
 * This structure holds parameters common to all test types.
 */
typedef struct {

    /**
     * Profile name used for identification of a given profile.
     * Defaults to empty string. Two profiles can't have the same name.
     * An empty name is illegal. Only isgraph() characters - except for
     * '/', '\"', '<', '>', and '\'' - are allowed.
     */
    char name[VTSS_APPL_RFC2544_PROFILE_NAME_LEN];

    /**
     * Profile description. Only isprint() characters are allowed.
     */
    char dscr[VTSS_APPL_RFC2544_PROFILE_DSCR_LEN];

    /**
     * TRUE if DST is OAM aware.
     *
     * If DST is OAM aware, this module will send LBM and expects
     * LBR frames back. Also, it will send DMM frames and expects
     * DMR frames back for delay measurements.
     *
     * If DST is not OAM aware, this module will send TST and expects
     * TST frames back. Also, it will send 1DM frames and expects
     * 1DM frames back for delay measurements.
     */
    mesa_bool_t dst_oam_aware;

    /**
     * For VLAN-based downmeps, a tag is required.
     * This composite allows for specifying PCP, DEI, and VID.
     * If vlan_tag.vid == 0, this is interpreted as a port-based profile.
     */
    mesa_vlan_tag_t vlan_tag;

    /**
     * The interface on which generated traffic egresses. This must
     * be of type port.
     */
    vtss_ifindex_t egress_interface;

    /**
     * The MEG level (0 - 7) used in the generated OAM frames.
     */
    uint32_t meg_level;

    /**
     * The destination MAC address used in the generated frames, unless
     * overridden when the test is started.
     *
     * The source MAC address will be determined by the egress port.
     *
     * The MAC address must be a non-zero unicast MAC address.
     */
    mesa_mac_t dmac;

    /**
     * Selected frame sizes to run the selected test suites with.
     */
    vtss_appl_rfc2544_frame_sizes_t selected_frame_sizes;

    /**
     * The time (in milliseconds) to wait after each trial for the system to settle before
     * acquiring counters and other statistics from hardware.
     * The dwell time must be divisible by 1000.
     */
    uint32_t dwell_time_msecs;

    /**
     * When TRUE, generated OAM frames are sequence numbered, and looped
     * frames are checked for correct order upon reception.
     */
    mesa_bool_t sequence_number_check;

} vtss_appl_rfc2544_profile_common_t;

/**
 * Parameters related to througput test.
 */
typedef struct {
    /**
     * Controls whether the test is enabled or not.
     */
    mesa_bool_t enabled;

    /**
     * Time - in seconds - to run one trial.
     */
    uint32_t trial_duration_secs;

    /**
     * Minimum rate measured in permille of the line rate.
     */
    uint32_t rate_min_permille;

    /**
     * Maximum rate measured in permille of the line rate.
     */
    uint32_t rate_max_permille;

    /**
     * The step to change rate between trials, measured in permille of the line rate.
     */
    uint32_t rate_step_permille;

    /**
     * Allowable frame loss (in permille) for the test to be considered passed.
     */
    uint32_t pass_criterion_permille;

} vtss_appl_rfc2544_profile_throughput_t;

/**
 * Parameters related to latency test.
 */
typedef struct {
    /**
     * Controls whether the test is enabled or not.
     */
    mesa_bool_t enabled;

    /**
     * Time - in seconds - to run one trial.
     */
    uint32_t trial_duration_secs;

    /**
     * Delay measurement interval, that is, the time (in milliseconds) between
     * transmission of a Y.1731 1DM/DMM test frame.
     * The interval must be divisible by 1000.
     */
    uint32_t dm_interval_msecs;

    /**
     * Allowable frame loss (in permille) for the test to be considered passed.
     */
    uint32_t pass_criterion_permille;

} vtss_appl_rfc2544_profile_latency_t;

/**
 * Parameters related to frame loss test.
 */
typedef struct {
    /**
     * Controls whether the test is enabled or not.
     */
    mesa_bool_t enabled;

    /**
     * Time - in seconds - to run one trial.
     */
    uint32_t trial_duration_secs;

    /**
     * Minimum rate measured in permille of the line rate.
     */
    uint32_t rate_min_permille;

    /**
     * Maximum rate (start rate) measured in permille of the line rate.
     */
    uint32_t rate_max_permille;

    /**
     * The size with which rate is reduced per trial, measured in permille of the line rate.
     */
    uint32_t rate_step_permille;

} vtss_appl_rfc2544_profile_frame_loss_t;

/**
 * Parameters related to back-to-back test.
 */
typedef struct {
    /**
     * Controls whether the test is enabled or not.
     */
    mesa_bool_t enabled;

    /**
     * Time - in milliseconds - to run one trial.
     */
    uint32_t trial_duration_msecs;

    /**
     * Number of times the trial will be executed.
     */
    uint32_t trial_cnt;

} vtss_appl_rfc2544_profile_back_to_back_t;

/**
 * This structure holds parameters related to one test suite profile.
 */
typedef struct {

    /**
     * Parameters common to all test types.
     */
    vtss_appl_rfc2544_profile_common_t common;

    /**
     * Throughput test parameters.
     */
    vtss_appl_rfc2544_profile_throughput_t throughput;

    /**
     * Latency test parameters.
     */
    vtss_appl_rfc2544_profile_latency_t latency;

    /**
     * Frame loss test parameters.
     */
    vtss_appl_rfc2544_profile_frame_loss_t frame_loss;

    /**
     * back-to-back test parameters.
     */
    vtss_appl_rfc2544_profile_back_to_back_t back_to_back;

} vtss_appl_rfc2544_profile_t;

/**
 * This structure holds meta info about a report.
 */
typedef struct {
    /**
     * Name of report. Only isgraph() characters - except for '/', '\"', '<',
     * '>', and '\'' - are allowed.
     */
    char name[VTSS_APPL_RFC2544_REPORT_NAME_LEN];

    /**
     * Description of report. Only isprint() characters are allowed.
     */
    char dscr[VTSS_APPL_RFC2544_REPORT_DSCR_LEN];

    /**
     * Time of creation of this report.
     */
    char creation_time[VTSS_APPL_RFC3339_TIME_STR_LEN];

    /**
     * Execution status of this report.
     */
    vtss_appl_rfc2544_test_status_t status;

    /**
     * This is the file system location to retrieve this report.
     *
     * If, for instance, HTTP is enabled on the device, then the report can be
     * fetched at the address formed by concatenating the device's HTTP address
     * with \p path.
     * For example, if the device is at address 192.168.1.2, the report can be
     * retrieved using the result of
     *    concat("http://192.168.1.2", uri_encode(path));
     *
     * Notice the uri_encode() call above. It is required if the report name
     * should contain special characters in order for - here - HTTP to work
     * correctly.
     */
    char path[VTSS_APPL_RFC2544_REPORT_PATH_LEN];

} vtss_appl_rfc2544_report_info_t;

/**
 * This structure holds data required to start a test.
 */
typedef struct {
    /**
     * Report name used for identification of the test data.
     * Defaults to empty string. Two test data report names can't have the same name.
     * An empty name is illegal. Only isgraph() characters - except for
     * '/', '\"', '<', '>', and '\'' - are allowed.
     */
    char name[VTSS_APPL_RFC2544_REPORT_NAME_LEN];

    /**
     * Description of report. Only isprint() characters are allowed.
     */
    char dscr[VTSS_APPL_RFC2544_REPORT_DSCR_LEN];

    /**
     * Name of profile to use when performing the test.
     */
    char profile_name[VTSS_APPL_RFC2544_PROFILE_NAME_LEN];

    /**
     * The destination MAC address found in the profile
     * is used as DMAC in the generated frames, unless
     * overridden with this field.
     * In order to override, the \p dmac must be a non-zero
     * unicast MAC address.
     * In order not to override, set the \p dmac to all-zeros.
     */
    mesa_mac_t dmac;

} vtss_appl_rfc2544_test_data_t;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get manifest constants for the RFC2544 module.
 *
 * \param cap [OUT] Pointer to structure receiving constants.
 *
 * \return VTSS_RC_OK on success, anything else on error.
 */
mesa_rc vtss_appl_rfc2544_capabilities_get(vtss_appl_rfc2544_capabilities_t *cap);

/**
 * Get names of existing profiles.
 *
 * Repeatedly call this function to get all profiles.
 * The first time, set name[0] to '\0', and leave it at its
 * previous value subsequent times.
 * As long as the function returns a non-zero length in the
 * name argument, more profiles are defined. Once the function
 * sets name[0] to '\0', the last profile is obtained.
 *
 * \param name [INOUT] See description above. Length must be at least VTSS_APPL_RFC2544_PROFILE_NAME_LEN bytes.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_rfc2544_profile_names_get(char *name);

/**
 * Get an existing profile or fill-in a default one.
 *
 * If the name argument is NULL or has length == 0, the profile argument
 * is filled with a default profile.
 *
 * If the name argument has length > 0, the profile argument is filled
 * with the corresponding profile data.
 *
 * \param name    [IN]  If NULL or has length == 0, default profile, otherwise the name of profile to retrieve.
 * \param profile [OUT] Pointer receiving profile.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_rfc2544_profile_get(const char *const name, vtss_appl_rfc2544_profile_t *profile);

/**
 * Set a profile.
 *
 * Use this function to create, update, or delete a profile.
 *
 * Setting profile->common.name to an empty string and old_name argument to the name of the
 * profile to delete, effectively deletes it, i.e. sets all fields to defaults.
 *
 * Setting profile->common.name to a non-empty string and old_name argument to the name of an
 * existing profile, will update the existing profile.
 *
 * Setting profile->common.name to a non-empty string and old_name to NULL or letting
 * it have length == 0, will create a new profile.
 *
 * \param old_name [IN] If updating or deleting an existing, must be a valid, existing profile name. Otherwise (creating a new profile), it must be NULL or have length == 0.
 * \param profile  [IN] Pointer to profile data.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_rfc2544_profile_set(const char *const old_name, vtss_appl_rfc2544_profile_t *profile);

/**
 * Get names of a non-executed tests.
 *
 * Repeatedly call this function to get all names of current test data entries.
 * Up to vtss_appl_rfc2544_capabilities_t::currently_executing_max such
 * entries may exist. The test data is volatile and will therefore not
 * survive a reboot.
 *
 * To iterate, do this:
 * The first time, set name[0] to '\0', and leave it at its previous value subsequent times.
 * As long as the function returns a non-zero length in the
 * name argument, more test data entries are defined.
 * Once the function sets name[0] to '\0', the last report has already been
 * obtained.
 *
 * \param name [INOUT] See description above. Length must be at least VTSS_APPL_RFC2544_REPORT_NAME_LEN bytes.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_rfc2544_test_data_names_get(char *name);

/**
 * Get test data for a given not-yet executed test.
 *
 * If the report_name argument is NULL or has length == 0, the test_data argument
 * is filled with default test data.
 *
 * If the report_name argument has length > 0, the test_data argument is filled
 * with the corresponding test data.
 *
 * \param report_name [IN]  Name of resulting test report (at most VTSS_APPL_RFC2544_REPORT_NAME_LEN chars long incl. NULL termination).
 * \param test_data   [OUT] Input data to the test.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_rfc2544_test_data_get(const char *const report_name, vtss_appl_rfc2544_test_data_t *test_data);

/**
 * Set test data.
 *
 * Use this function to create, update, or delete test data.
 *
 * Test data must be set up prior to executing a test with vtss_appl_rfc2544_test_start().
 *
 * Setting test_data->name to an empty string and old_name argument to the name of the
 * test data entry to delete, effectively deletes it, i.e. sets all fields to defaults.
 *
 * Setting test_data->name to a non-empty string and old_name argument to the name of an
 * existing test data entry, will update the existing test data entry.
 *
 * Setting test_data->name to a non-empty string and old_name to NULL or letting
 * it have length == 0, will create a new test data entry.
 *
 * \param old_name  [IN] If updating or deleting an existing, must be a valid, existing test data entry name. Otherwise (creating a new test data entry), it must be NULL or have length == 0.
 * \param test_data [IN] Pointer to test data.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_rfc2544_test_data_set(const char *const old_name, vtss_appl_rfc2544_test_data_t *test_data);

/**
 * Start executing a test indicated by report_name.
 *
 * \p report_name must point to a test data entry already set up with calls
 * to vtss_appl_rfc2544_test_data_set().
 *
 * The test either stops by itself or may be force-stopped with a call to
 * vtss_appl_rfc2544_test_stop().
 *
 * Once this function has been called, a snap shot of the test data set up
 * with vtss_appl_rfc2544_test_data_set() will be taken and the entry will
 * be removed from the test data table.
 *
 * \param report_name [IN] Name of test report
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_rfc2544_test_start(const char *const report_name);

/**
 * Stop executing the test indicated by report_name.
 *
 * \param report_name [IN] Name of test report currently in progress.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_rfc2544_test_stop(const char *const report_name);

/**
 * Get names and info of existing reports.
 *
 * Repeatedly call this function to get info and names of all reports.
 * The first time you invoke the function, set info.name[0] == '\0'.
 * Subsequent times, leave it as it was upon return.
 * Stop when the function returns info.name[0] == '\0'.
 *
 * Reports are returned in chronological order - oldest first.
 *
 * \param info [INOUT] Pointer to a structure receiving info about a given report. See description above on how to control the .name member.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_rfc2544_report_info_get(vtss_appl_rfc2544_report_info_t *info);

/**
 * Delete test report.
 *
 * \param name [IN] Name of report to delete
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_rfc2544_report_delete(const char *const name);

/**
 * Get a report as clear text.
 *
 * The function allocates a string of a suitable size (with VTSS_MALLOC()) and
 * prints the report into it as clear text. The length of the string can be found
 * with strlen(report_as_txt).
 *
 * Iff the function call succeeds (return value == VTSS_RC_OK), the pointer
 * to the report must be freed with a call to VTSS_FREE().
 *
 * \param report_name   [IN] Name of report to get as text.
 * \param report_as_txt [OUT] Pointer to a char pointer receiving the VTSS_MALLOC()ed string.
 *
 * \return VTSS_RC_OK on success. Anything else on error. Use error_txt() to convert to string.
 */
mesa_rc vtss_appl_rfc2544_report_as_txt(const char *report_name, char **report_as_txt);

#ifdef __cplusplus
}
#endif

#endif /* _VTSS_APPL_RFC2544_H_ */

