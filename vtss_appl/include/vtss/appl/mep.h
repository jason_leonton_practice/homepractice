
/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/


/**
* \file
* \brief Public MEP (OAM) API
* \details This header file describes the MEP (OAM) control functions and types.
*/

#ifndef _VTSS_APPL_MEP_H_
#define _VTSS_APPL_MEP_H_

#include <vtss/appl/interface.h>
#include <mscc/ethernet/switch/api/types.h>
#include <vtss/appl/module_id.h>

#define VTSS_APPL_MEP_PEER_MAX                            10 /**< Highest max number of peer MEPs for all supported platforms  */
#define VTSS_APPL_MEP_TRANSACTION_MAX                      5 /**< Max number of Link Trace transaction */
#define VTSS_APPL_MEP_REPLY_MAX                            5 /**< Max number of reply in a transaction */
#define VTSS_APPL_MEP_MEG_CODE_LENGTH                     17 /**< Max length of Both Maintenance Domain Name and MEG-ID include a NULL termination */
#define VTSS_APPL_MEP_OUI_SIZE                             3 /**< The OS TLV OUI size */
#define VTSS_APPL_MEP_RT_REPLY_MAX                        16 /**< Max number of G.8113.2 Route Trace replies */
#define VTSS_APPL_MEP_DM_BINNING_MAX                      10 /**< Max number of Measurement Bins */
#define VTSS_APPL_MEP_DM_BINS_FD_MAX                      10 /**< Max number of Measurement Bins for FD */
#define VTSS_APPL_MEP_DM_BINS_IFDV_MAX                    10 /**< Max number of Measurement Bins for IFDV */

/**
 * \brief Instance capability structure.
 *
 * This structure is used to contain the instance capability.
 *
 */
typedef struct {
    uint32_t instance_max;           /**< Max number of MEP instance */
    uint32_t peer_max;               /**< Max number of peer MEP    */
    uint32_t transaction_max;        /**< Max number of Link Trace transaction */
    uint32_t reply_max;              /**< Max number of reply in a transaction */
    uint32_t client_flows_max;       /**< Max number of client flows */
    uint32_t mac_length;             /**< MAC length */
    uint32_t mep_id_max;             /**< MAX value of a MEP ID */
    uint32_t meg_code_length;        /**< MAX length of Both Maintenance Domain Name and MEG-ID include a NULL termination */
    uint32_t dm_interval_min;        /**< MIN DM transmission interval  */
    uint32_t dm_interval_max;        /**< MAX DM transmission interval */
    uint32_t dm_lastn_min;           /**< MIN number of DM LastN for average calculation */
    uint32_t dm_lastn_max;           /**< MAX number of DM LastN for average calculation */
    uint32_t dm_bin_max;             /**< MAX number of DM BINs */
    uint32_t dm_bin_fd_max;          /**< MAX number of DM FD BINs */
    uint32_t dm_bin_fd_min;          /**< MIN number of DM FD BINs */
    uint32_t dm_bin_ifdv_max;        /**< MAX number of DM IFDV BINs */
    uint32_t dm_bin_ifdv_min;        /**< MIN number of DM IFDV BINs */
    uint32_t lbm_size_max;           /**< The maximum LBM frame size */
    uint32_t lbm_size_min;           /**< The minimum LBM frame size */
    uint32_t lbm_mpls_size_max;      /**< The maximum MPLS LBM frame size */
    uint32_t slm_size_max;           /**< The maximum SLM frame size */
    uint32_t slm_size_min;           /**< The minimum SLM frame size */
    uint32_t tst_size_max;           /**< The maximum TST frame size */
    uint32_t tst_size_min;           /**< The minimum TST frame size */
    uint32_t tst_mpls_size_max;      /**< The maximum MPLS TST frame size */
    uint32_t tst_rate_max;           /**< The maximum TST bit rate */
    uint32_t test_flow_max;          /**< The maximum Test flows (LB and TST) per priority */
    uint32_t client_prio_highest;    /**< The highest possible priority is requested */
    mesa_bool_t has_itu_g8113_2;     /**< The device has MPLS BFD capabilities */
    mesa_bool_t has_voe_support;     /**< The device has HW based OAM engine support */
    uint32_t lb_to_send_infinite;    /**< Send LBM infinite - test behaviour */
    uint32_t bfd_auth_key_max;       /**< Max number of BFD Authentication Keys */
    uint32_t rt_reply_max;           /**< Max number of RT replies */
    mesa_bool_t has_evc_pag;         /**< vtss_appl_mep_conf_t.evc_pag */
    mesa_bool_t has_evc_qos;         /**< vtss_appl_mep_conf_t.evc_qos */
    mesa_bool_t has_flow_count;      /**< vtss_appl_mep_lm_conf_t.flow_count */
    mesa_bool_t has_oam_count;       /**< vtss_appl_mep_lm_conf_t.oam_count */
    mesa_bool_t has_oper_state;      /**< vtss_appl_mep_state_t.oper_state */
    mesa_bool_t has_oam_sw_support;  /**< If true SW based OAM engine is supported on this system */
} vtss_appl_mep_capabilities_t;

/**
 * Definition of error return codes.
 * See also mep_error_txt() in platform/mep.c.
 */
enum {
    VTSS_APPL_MEP_RC_APS_PROT_CONNECTED =
    MODULE_ERROR_START(VTSS_MODULE_ID_MEP),   /**< Mismatch between assosiated protection type and APS type                                   */
    VTSS_APPL_MEP_RC_INVALID_PARAMETER,       /**< Invalid parameter                                                                          */
    VTSS_APPL_MEP_RC_NOT_ENABLED,             /**< MEP instance is not enabled                                                                */
    VTSS_APPL_MEP_RC_CAST,                    /**< LM and CC sharing SW generated CCM must have same casting (multi/uni)                      */
    VTSS_APPL_MEP_RC_RATE_INTERVAL,           /**< The selected frame rate or interval is invalid                                             */
    VTSS_APPL_MEP_RC_PEER_CNT,                /**< Invalid number of peer's for this configuration                                            */
    VTSS_APPL_MEP_RC_PEER_ID,                 /**< Invalid peer MEP ID                                                                        */
    VTSS_APPL_MEP_RC_MIP,                     /**< Not allowed on a MIP                                                                       */
    VTSS_APPL_MEP_RC_INVALID_EVC,             /**< EVC flow was found invalid                                                                 */
    VTSS_APPL_MEP_RC_INVALID_PRIO,            /**< Priority was found invalid                                                                 */
    VTSS_APPL_MEP_RC_APS_UP,                  /**< APS not allowed on EVC UP MEP                                                              */
    VTSS_APPL_MEP_RC_APS_DOMAIN,              /**< R-APS not allowed in this domain                                                           */
    VTSS_APPL_MEP_RC_INVALID_VID,             /**< The VID is invalid or VLAN is not created for this VID or attribut 'VID' is illegal in this domain */
    VTSS_APPL_MEP_RC_INVALID_COS_ID,          /**< Invalid COS ID (priority) for this EVC                                                     */
    VTSS_APPL_MEP_RC_NO_VOE,                  /**< No VOE available                                                                           */
    VTSS_APPL_MEP_RC_NO_TIMESTAMP_DATA,       /**< There is no DMR time-stamp data available                                                  */
    VTSS_APPL_MEP_RC_PEER_MAC,                /**< Peer Unicast MAC must be known to do HW based CCM on SW MEP                                */
    VTSS_APPL_MEP_RC_INVALID_INSTANCE,        /**< Invalid MEP instance ID                                                                    */
    VTSS_APPL_MEP_RC_INVALID_MEG,             /**< Invalid MEG-ID or IEEE Name                                                                */
    VTSS_APPL_MEP_RC_PROP_SUPPORT,            /**< Proprietary DM is not supported                                                            */
    VTSS_APPL_MEP_RC_VOLATILE,                /**< This is an OK return code, just indicating that the MEP is volatile (not saved to flash)   */
    VTSS_APPL_MEP_RC_VLAN_SUPPORT,            /**< VLAN domain is not supported                                                               */
    VTSS_APPL_MEP_RC_CLIENT_MAX_LEVEL,        /**< The MEP is on MAX level (7) - it is not possible to have a client on higher level          */
    VTSS_APPL_MEP_RC_INVALID_CLIENT_LEVEL,    /**< The client level is invalid                                                                */
    VTSS_APPL_MEP_RC_MIP_SUPPORT,             /**< This MIP is not supported                                                                  */
    VTSS_APPL_MEP_RC_INVALID_MAC,             /**< On this platform the selected MAC is invalid                                               */
    VTSS_APPL_MEP_RC_CHANGE_PARAMETER,        /**< Some parameters are not allowed to change on an enabled instance                           */
    VTSS_APPL_MEP_RC_NO_LOOP_PORT,            /**< There is no loop port - this is required to create Up-MEP                                  */
    VTSS_APPL_MEP_RC_INVALID_PORT,            /**< The residence port is not valid in this domain or for this MEP type                        */
    VTSS_APPL_MEP_RC_INVALID_HW_CCM,          /**< On Jaguar all MEP in same classified VID must have same HW CCM state                       */
    VTSS_APPL_MEP_RC_LST,                     /**< Link State Tracking cannot be enabled as either CC or CC-TLV is not enabled                */
    VTSS_APPL_MEP_RC_NNI_PORT,                /**< The MEP must be configured on a EVC NNI port                                               */
    VTSS_APPL_MEP_RC_TEST,                    /**< TST and LBM cannot be enabled at the same time                                             */
    VTSS_APPL_MEP_RC_E_TREE,                  /**< Invalid MEP configuration for E-TREE EVC                                                   */
    VTSS_APPL_MEP_RC_INVALID_MEP_CONFIG,      /**< Invalid MEP configuration. Like Port Domain Up-MEP                                         */
    VTSS_APPL_MEP_RC_INVALID_LEVEL,           /**< Invalid MEG Level                                                                          */
    VTSS_APPL_MEP_RC_INVALID_MEP,             /**< Invalid MEP-ID                                                                             */
    VTSS_APPL_MEP_RC_NO_MIP,                  /**< No MIP available                                                                           */
    VTSS_APPL_MEP_RC_INVALID_DOMAIN,          /**< The domain is not supported                                                                */
    VTSS_APPL_MEP_RC_RESOURCE_CONF_FAILED,    /**< Resource configuration failed                                                              */
    VTSS_APPL_MEP_RC_HW_SUPPORT_REQUIRED,     /**< HW (VOP/MIP) support is required                                                           */
    VTSS_APPL_MEP_RC_UP_SUPPORT,              /**< UP MEP/MIP is not supported in this domain                                                 */
    VTSS_APPL_MEP_RC_DOWN_MAX,                /**< MAX number of Down-MEPs is exceeded in this flow                                           */
    VTSS_APPL_MEP_RC_LAST_N,                  /**< The "last N" numer of delay measurement for avarage calculation is invalid                 */
    VTSS_APPL_MEP_RC_T_UNIT,                  /**< The "Time unit" of delay measurement is invalid                                            */
    VTSS_APPL_MEP_RC_OVERFLOW,                /**< The "Overflow Action" of delay measurement is invalid                                      */
    VTSS_APPL_MEP_RC_FD_BIN,                  /**< The number of "FD Bins" is invalid                                                         */
    VTSS_APPL_MEP_RC_IFDV_BIN,                /**< The number of "IFDV Bins" is invalid                                                       */
    VTSS_APPL_MEP_RC_M_THRESHOLD,             /**< The binning measurement threshold is invalid                                               */
    VTSS_APPL_MEP_RC_MEP_MAX,                 /**< Max number of MEPs has been exceeded. Like too many Down or Up-MEPs on same port           */
    VTSS_APPL_MEP_RC_NOT_CREATED,             /**< MEP instance has not been created                                                          */
    VTSS_APPL_MEP_RC_INVALID_WORK_PORT,       /**< Invalid working port ID                                                                    */
    VTSS_APPL_MEP_RC_MIP_MAX,                 /**< Max number of MIPs has been exceeded. Like too many Down or Up-MIPs on same port           */
    VTSS_APPL_MEP_RC_CC_BFD_CONFLICT,         /**< CC and BFC cannot be enabled at the same time                                              */
    VTSS_APPL_MEP_RC_STATE_NOT_UP,            /**< The MEP operational state is not up                                                        */
    VTSS_APPL_MEP_RC_INVALID_VOE_CHANGE,      /**< VOE support cannot be added/deleted on an enabled MEP instance                             */
    VTSS_APPL_MEP_RC_TTL_ZERO,                /**< TTL==0 not allowed                                                                         */
    VTSS_APPL_MEP_RC_LM_DUAL_ENDED_RATE_EXCEED_CC_RATE, /**< LM_DUAL_ENDED rate cannot be higher than CC rate                                 */
    VTSS_APPL_MEP_RC_OUT_OF_TX_RESOURCE,      /**< There are no free transmission resources                                                   */
    VTSS_APPL_MEP_RC_TX_ERROR,                /**< Transmitting OAM frame failed due to system error                                          */
    VTSS_APPL_MEP_RC_UP_CLIENT,               /**< Up-MEP is not allowed to do client injection                                               */
    VTSS_APPL_MEP_RC_INVALID_LB_FRM_SIZE,     /**< The framesize for LB function is invalid                                                   */
    VTSS_APPL_MEP_RC_INVALID_TST_FRM_SIZE,    /**< The framesize for TST function is invalid                                                  */
    VTSS_APPL_MEP_RC_TCAM_CONFIGURATION,      /**< The TCAM configuration failed. This could be due to lack of ACL resources                  */
    VTSS_APPL_MEP_RC_INTERNAL,                /**< Internal error. Some internal configuration fail.                                          */
    VTSS_APPL_MEP_RC_VOE_CONFIG_ERROR,        /**< Too configured VOE based MEPs on a port in a flow or VOE based MEP on wrong level.         */
    VTSS_APPL_MEP_RC_MEAS_INTERVAL,           /**< Invalid LM measurement interval                                                            */
    VTSS_APPL_MEP_RC_INVALID_BIN_NUMBER,      /**< Invalid MEP DM bin number                                                                  */
    VTSS_APPL_MEP_RC_INVALID_VOE,             /**< This instance cannot be VOE based. See help on VOE selection                               */
    VTSS_APPL_MEP_RC_VOE_MPLS_ERROR,          /**< VOE is not supported on MEP MPLS domains.                                                  */
    VTSS_APPL_MEP_RC_EVC_VID_SUPPORT,         /**< EVC subscriber MEP not supported                                                           */
};

/**
 *  The instance mode
*/
typedef enum {
    VTSS_APPL_MEP_MEP,       /**< Instance is a MEP */
    VTSS_APPL_MEP_MIP        /**< Instance is a MIP */
} vtss_appl_mep_mode_t;

/**
 *  The instance direction
*/
typedef enum {
    VTSS_APPL_MEP_DOWN,      /**< Instance is Down MEP/MIP  */
    VTSS_APPL_MEP_UP         /**< Instance is Up MEP/MIP    */
} vtss_appl_mep_direction_t;

/**
 *  The instance expected and transmitted MEG ID format
*/
typedef enum {
    VTSS_APPL_MEP_ITU_ICC,    /**< MEG-ID is ITU ICC format as described in Y.1731 ANNEX A.1                                                                    */
    VTSS_APPL_MEP_IEEE_STR,   /**< MEG-ID is IEEE Domain Name format as described in 802.1ag 21.6.5 - Domain format '4' and Short format '2' (Character string) */
    VTSS_APPL_MEP_ITU_CC_ICC  /**< MEG-ID is ITU CC and ICC format as described in Y.1731 ANNEX A.2                                                             */
} vtss_appl_mep_format_t;

/**
 * \brief Instance configuration data structure.
 *
 * This structure is used to do basic instance configuration.
 * The following elements cannot be changed in an instance that is already enabled. They will be set when the instance is changed from disable to enable (create):
 *      mode - direction - domain - flow - port - mac.
 */
typedef struct {
    mesa_bool_t                 enable;                               /**< Instance is enabled/disabled                                  */
    vtss_appl_mep_mode_t        mode;                                 /**< MEP or MIP mode                                               */
    vtss_appl_mep_direction_t   direction;                            /**< Up-MEP or Down-MEP direction                                  */
    vtss_ifindex_t              flow;                                 /**< Flow (EVC - VLAN) instance                                    */
    vtss_ifindex_t              port;                                 /**< Residence port                                                */
    uint32_t                    level;                                /**< MEG level. Range 0 to 7                                       */
    uint16_t                    vid;                                  /**< VID used for tagged Port domain OAM                           */
    mesa_bool_t                 voe;                                  /**< This should utilize HW based OAM engine if possible           */
    mesa_mac_t                  mac;                                  /**< MEP Unicast MAC. If 'all zero' the residence port MAC is used */

    vtss_appl_mep_format_t      format;                               /**< MEG-ID format.                                                */
    char                        name[VTSS_APPL_MEP_MEG_CODE_LENGTH];  /**< IEEE Maintenance Domain Name.          (string)               */
    char                        meg[VTSS_APPL_MEP_MEG_CODE_LENGTH];   /**< Unique MEG ID.                         (string)               */
    uint32_t                    mep;                                  /**< MEP id of this instance                                       */

    uint32_t                    peer_count;                           /**< Number of peer MEP's. 0 to vtss_appl_mep_PEER_MAX             */
    uint16_t                    peer_mep[VTSS_APPL_MEP_PEER_MAX];     /**< MEP id of peer MEP's                                          */
    mesa_mac_t                  peer_mac[VTSS_APPL_MEP_PEER_MAX];     /**< Peer unicast MAC                                              */

    mesa_bool_t                 out_of_service;                       /**< Up-MEP is out of service. On Serval-1 a VOE based Up-MEP is set OOS and the UNI is looping - it now has functionality as a Down-MEP. */
    uint32_t                    evc_pag;                              /**< EVC generated policy PAG value. On Jaguar this is used as IS2 key. For Up-MEP (DS1076) this is used when creating MCE (IS1) entries */
    uint32_t                    evc_qos;                              /**< EVC QoS value. Only used on Caracal for getting queue frame counters. For Up-MEP (DS1076) this is used when creating MCE (IS1) entries */
} vtss_appl_mep_conf_t;

/**
 * \brief Instance peer configuration data structure.
 *
 * This structure is used to do instance peer configuration.
 * The peer MEP configuration can be done separately through this structure and the related functional interface.
 * The Instance peer configuration is also accessible through the vtss_appl_mep_conf_t structure in its raw form.
 */
typedef struct {
    mesa_mac_t   mac;          /**< The unicast MAC of the referenced Peer MEP ID */
} vtss_appl_mep_peer_conf_t;

/**
 * \brief Instance syslog structure.
 *
 * This structure is used to do instance syslog configuration.
 */
typedef struct {
    mesa_bool_t   enable;          /**< Syslog entry creation is enabled if true */
} vtss_appl_mep_syslog_conf_t;

/**
 *  The instance operational state. This is not VTSS_APPL_MEP_OPER_UP if instance for
 *  some reason is not possible to create physically.
*/
typedef enum {
    VTSS_APPL_MEP_OPER_UP,              /**< Operational state is UP */
    VTSS_APPL_MEP_OPER_DOWN,            /**< Operational state is DOWN */
    VTSS_APPL_MEP_OPER_INVALID_CONFIG,  /**< Operational state is DOWN due to invalid configuration*/
    VTSS_APPL_MEP_OPER_HW_OAM,          /**< Operational state is DOWN due to failing OAM supporting HW resources */
    VTSS_APPL_MEP_OPER_MCE              /**< Operational state is DOWN due to failing MCE resources */
} vtss_appl_mep_oper_state_t;

/**
 * \brief Instance status data structure.
 *
 * This structure is used to retrieve instance status information.
 */
typedef struct {
    mesa_bool_t                       cLevel;        /**< Incorrect CCM level received                                    */
    mesa_bool_t                       cMeg;          /**< Incorrect CCM MEG id received                                   */
    mesa_bool_t                       cMep;          /**< Incorrect CCM MEP id received                                   */
    mesa_bool_t                       cSsf;          /**< SSF state                                                       */
// These members are no more accessible in this struct - vtss_appl_mep_peer_state_t should be used
//    mesa_bool_t    cLoc[VTSS_APPL_MEP_PEER_MAX];     /**< CCM LOC state from all peer MEP                                */
//    mesa_bool_t    cRdi[VTSS_APPL_MEP_PEER_MAX];     /**< CCM RDI state from all peer MEP                                */
//    mesa_bool_t    cPeriod[VTSS_APPL_MEP_PEER_MAX];  /**< CCM Period state from all peer MEP                             */
//    mesa_bool_t    cPrio[VTSS_APPL_MEP_PEER_MAX];    /**< CCM Priority state from all peer MEP                           */
    mesa_bool_t                       cAis;          /**< AIS state. AIS is received.                                     */
    mesa_bool_t                       cLck;          /**< Locked State. LCK is received.                                  */
    mesa_bool_t                       cLoop;         /**< Loop is detected. CCM is received with own MEP ID and SMAC      */
    mesa_bool_t                       cConfig;       /**< Configuration error detected. CCM is received with own MEP ID   */
    mesa_bool_t                       cDeg;          /**< Signal Degraded                                                 */

    mesa_bool_t                       aTsf;          /**< Trail Signal fail consequent action is calculated               */
    mesa_bool_t                       aTsd;          /**< Trail Signal degrade consequent action is calculated            */
    mesa_bool_t                       aBlk;          /**< Block consequent action is calculated                           */

    vtss_appl_mep_oper_state_t oper_state;    /**< Operational state                                               */
} vtss_appl_mep_state_t;

/**
 *  Port Status TLV according to 802.1ag
*/
typedef struct {
    uint8_t  value;                          /**< The Value content */
} vtss_appl_mep_ps_tlv_t;

/**
 *  Interface Status TLV according to 802.1ag
*/
typedef struct {
    uint8_t  value;                          /**< The Value content */
} vtss_appl_mep_is_tlv_t;

/**
 *  Organization-Specific TLV according to 802.1ag
*/
typedef struct {
    uint8_t  oui[VTSS_APPL_MEP_OUI_SIZE];    /**< The OUI content */
    uint8_t  subtype;                        /**< The sub-type content */
    uint8_t  value;                          /**< The Value content */
} vtss_appl_mep_os_tlv_t;

/**
 * \brief TLV configuration data structure.
 *
 * This structure is used to configure transmitted TLV.
 */
typedef struct {
    vtss_appl_mep_os_tlv_t  os_tlv;     /**< Transmitted Organizational Specific TLV content */
} vtss_appl_mep_tlv_conf_t;

/**
 * \brief TLV state data structure.
 *
 * This structure is used to retrieve TLV status information.
 */
typedef struct {
    vtss_appl_mep_ps_tlv_t  ps_tlv;               /**< Last received Port Status TLV content             */
    vtss_appl_mep_is_tlv_t  is_tlv;               /**< Last received Interface Status TLV content        */
    vtss_appl_mep_os_tlv_t  os_tlv;               /**< Last received Organizational Specific TLV content */
    mesa_bool_t             ps_received;          /**< Last received CCM contained PS TLV                */
    mesa_bool_t             is_received;          /**< Last received CCM contained IS TLV                */
    mesa_bool_t             os_received;          /**< Last received CCM contained OS TLV                */
} vtss_appl_mep_peer_cc_state_t;

/**
 * \brief CC state data structure.
 *
 * This structure is used to retrieve CC status information.
 */
typedef struct {
    uint64_t                valid_counter;                                /**< Valid CCM received                                                         */
    uint64_t                invalid_counter;                              /**< Invalid CCM received                                                       */
    uint64_t                oo_counter;                                   /**< Counted received Out of Order sequence numbers (VOE capability)            */
    uint32_t                unexp_level;                                  /**< Last received invalid level value                                          */
    uint32_t                unexp_period;                                 /**< Last received invalid period value                                         */
    uint32_t                unexp_prio;                                   /**< Last received invalid priority value                                       */
    uint32_t                unexp_mep;                                    /**< Last received unexpected MEP id                                            */
    char                    unexp_name[VTSS_APPL_MEP_MEG_CODE_LENGTH];    /**< Last received unexpected Domain Name or ITU Carrier Code (ICC). (string)   */
    char                    unexp_meg[VTSS_APPL_MEP_MEG_CODE_LENGTH];     /**< Last received unexpected Unique MEG ID Code.                    (string)   */
    vtss_appl_mep_ps_tlv_t  ps_tlv[VTSS_APPL_MEP_PEER_MAX];               /**< Last received Port Status TLV content                                      */
    vtss_appl_mep_is_tlv_t  is_tlv[VTSS_APPL_MEP_PEER_MAX];               /**< Last received Interface Status TLV content                                 */
    vtss_appl_mep_os_tlv_t  os_tlv[VTSS_APPL_MEP_PEER_MAX];               /**< Last received Organizational Specific TLV content                          */
    mesa_bool_t             ps_received[VTSS_APPL_MEP_PEER_MAX];          /**< Last received CCM contained PS TLV                                         */
    mesa_bool_t             is_received[VTSS_APPL_MEP_PEER_MAX];          /**< Last received CCM contained IS TLV                                         */
    mesa_bool_t             os_received[VTSS_APPL_MEP_PEER_MAX];          /**< Last received CCM contained OS TLV                                         */
} vtss_appl_mep_cc_state_t;


/**
 * \brief Instance peer status data structure.
 *
 * This structure is used to get instance peer status.
 * The peer MEP status can be retrieved separately through this structure and the related functional interface.
 * The Instance peer status is also accessible through the vtss_appl_mep_state_t structure.
 */
typedef struct {
    mesa_bool_t    cLoc;       /**< CCM LOC state from peer MEP       */
    mesa_bool_t    cRdi;       /**< CCM RDI state from peer MEP       */
    mesa_bool_t    cPeriod;    /**< CCM Period state from peer MEP    */
    mesa_bool_t    cPrio;      /**< CCM Priority state from peer MEP  */
} vtss_appl_mep_peer_state_t;

/**
 * \brief Instance Performance Monitoring configuration data structure.
 *
 * This structure is used to do PM configuration of an instance.
 * When PM is enabled in an instance, any enabled LM or DM will contribute to the Performance Monitoring LM or DM statistics.
 */
typedef struct {
    mesa_bool_t    enable;         /**< Delivering of PM (LM and DM) data to the PM Data Set is enabled */
} vtss_appl_mep_pm_conf_t;

/**
 * \brief Instance Link State Tracking data structure.
 *
 * This structure is used to do LST configuration of an instance.
 * When LST is enabled in an instance, Local SF or received 'isDown' in CCM Interface Status TLV, will bring down the residence port
 */
typedef struct {
    mesa_bool_t    enable;         /**< LST functionality is enabled or disabled */
} vtss_appl_mep_lst_conf_t;

/**
 *  CCM - LM - AIS - LCK transmission period
*/
typedef enum {
    VTSS_APPL_MEP_RATE_INV,   /**< Invalid                                  */
    VTSS_APPL_MEP_RATE_300S,  /**< 300 frames per second                    */
    VTSS_APPL_MEP_RATE_100S,  /**< 100 frames per second                    */
    VTSS_APPL_MEP_RATE_10S,   /**< 10 frames per second                     */
    VTSS_APPL_MEP_RATE_1S,    /**< 1 frames per second                      */
    VTSS_APPL_MEP_RATE_6M,    /**< 6 frames per minute                      */
    VTSS_APPL_MEP_RATE_1M,    /**< 1 frames per minute                      */
    VTSS_APPL_MEP_RATE_6H,    /**< 6 frames per hour                        */
} vtss_appl_mep_rate_t;

/**
 * \brief Continuity Check configuration data structure.
 *
 * This structure is used to do CC configuration of an instance.
 * When enabled both transmission and LOC detection is enabled
 */
typedef struct {
    mesa_bool_t            enable;       /**< CCM transmission and LOC detection is enabled if true                      */
    uint32_t               prio;         /**< Priority of the transmitted CCM. Range 0 to 7                              */
    vtss_appl_mep_rate_t   rate;         /**< CCM transmission and expected reception rate                               */
    mesa_bool_t            tlv_enable;   /**< Enable/disable insertion of TLV in CCM PDU. OS, PS and IS TLV are inserted */
} vtss_appl_mep_cc_conf_t;

/**
 *  LM and DM mode
*/
typedef enum {
    VTSS_APPL_MEP_SINGEL_ENDED,  /**< Single ended LM based on LMM/LMR or DM based on DMM/DMR   */
    VTSS_APPL_MEP_DUAL_ENDED     /**< Dual ended LM based on CCM or DM based on 1DM             */
} vtss_appl_mep_ended_t;

/**
 *  Destination MAC type
*/
typedef enum {
    VTSS_APPL_MEP_UNICAST,      /**< Unicast destination address   */
    VTSS_APPL_MEP_MULTICAST     /**< Multicast destination address */
} vtss_appl_mep_cast_t;

/**
 *  Loss Measurement OAM frame counting type
*/
typedef enum {
    VTSS_APPL_MEP_OAM_COUNT_Y1731,      /**< Loss Measurement is counting OAM frames as service frames as described in Y1731. */
    VTSS_APPL_MEP_OAM_COUNT_NONE,       /**< Loss Measurement is NOT counting OAM frames as service frames.                   */
    VTSS_APPL_MEP_OAM_COUNT_ALL         /**< Loss Measurement is counting all OAM frames as service frames.                   */
} vtss_appl_mep_oam_count;

/**
 * MEP counter clear direction
 */
typedef enum {
    VTSS_APPL_MEP_CLEAR_DIR_BOTH,       /**< Clear counters for both directions (TX and RX).                                  */
    VTSS_APPL_MEP_CLEAR_DIR_TX,         /**< Clear counters for TX direction only.                                            */
    VTSS_APPL_MEP_CLEAR_DIR_RX          /**< Clear counters for RX direction only.                                            */
} vtss_appl_mep_clear_dir;

/**
 * \brief Loss Measurement configuration data structure.
 *
 * This structure is used to do LM configuration of an instance.
 * The LM can be either single ended or dual ended.
 * When LM is activated, LM PDU are transmitted and LM results are calculated.
 * Reception of LMM and conversion into LMR is always enabled.
 * The MEP can only have one Peer-MEP.
 * The following elements cannot be changed when LM is already enabled. They will be set when the LM is changed from disable to enable (create):
 *      ended.
 */
typedef struct {
    mesa_bool_t                     enable;                 /**< This enables transmission of LM PDU and calculation of loss when CCM/LMR/SLR is received.                                             */
    mesa_bool_t                     enable_rx;              /**< This enables reception and processing of received LMM/SLM/1SL. When 'enable' is TRUE this is implicitly assumed to be TRUE as well.   */
    mesa_bool_t                     synthetic;              /**< TRUE => LM based on synthetic frames (SLM/1SL). FALSE => LM based on service frames (LMM/CCM)                                         */
    mesa_bool_t                     dei;                    /**< DEI of the transmitted LM PDU (CCM/LMM/SLM/1SL)                                                                                       */
    uint32_t                        prio;                   /**< Priority of the transmitted CCM/LMM. Range 0 to 7                    */
    vtss_appl_mep_cast_t            cast;                   /**< Uni/Multicast selection - on CCM based LM only multicast is possible */
    uint32_t                        mep;                    /**< Peer Mep to receive LM - only used if unicast. Not used in case of only one peer Mep (default). Max capabilities.mep_id_max */
    vtss_appl_mep_rate_t            rate;                   /**< PDU transmission rate                                                */
    uint32_t                        size;                   /**< Size of SLM/1SL frame to send - max. capabilities.slm_size_max bytes - min. capabilities.slm_size_min                               */
    vtss_appl_mep_ended_t           ended;                  /**< Dual/single ended selection. CCM or LMM/LMR based.                   */
    uint32_t                        flr_interval;           /**< Frame loss ratio interval (number of measurements).                  */
    uint32_t                        meas_interval;          /**< Synthetic LM measurement interval. In case of non synthetic this is forced to LM PDU transmission interval. In case of synthetic this is a configurable number milliseconds. This must be a multiply of the LM PDU transmission interval */
    mesa_bool_t                     flow_count;             /**< Traffic (service frames) are counted per flow - all priority in one  */
    vtss_appl_mep_oam_count         oam_count;              /**< Configure how OAM frames are counted                                 */
    uint32_t                        loss_th;                /**< Frame loss threshold. The loss threshold count is incremented if a loss measurement is above this threshold.                        */
    uint32_t                        los_int_cnt_holddown;   /**< Holddown timer in seconds for JSON notification updates for near_los_int_cnt and far_los_int_cnt                                    */
    uint32_t                        los_th_cnt_holddown;    /**< Holddown timer in seconds for JSON notification updates for near_los_th_cnt and far_los_th_cnt                                      */
    uint32_t                        slm_test_id;            /**< SLM PDU Test ID value                                                                                                                 */
} vtss_appl_mep_lm_conf_t;

/**
 * \brief Loss Measurement status data structure.
 *
 * This structure is used to retrieve LM status information.
 */
typedef struct {
    uint32_t    tx_counter;         /**< Transmitted PDU (LMM - CCM) containing counters                         */
    uint32_t    rx_counter;         /**< Received PDU (LMM - CCM) containing counters                            */
    int32_t     near_los_tot_cnt;   /**< Near end total loss counter                                             */
    int32_t     far_los_tot_cnt;    /**< Far end total loss counter                                              */
    uint32_t    near_flr_ave_int;   /**< Near end interval frame loss ratio in per mille                         */
    uint32_t    far_flr_ave_int;    /**< Far end interval frame loss ratio in per mille                          */
    uint32_t    near_los_int_cnt;   /**< Number of measurements that has contributed to near end loss (since last clear) */
    uint32_t    far_los_int_cnt;    /**< Number of measurements that has contributed to far end loss (since last clear) */
    uint32_t    near_los_th_cnt;    /**< Near end frame loss threshold count                                     */
    uint32_t    far_los_th_cnt;     /**< Far end frame loss threshold count                                      */
    uint32_t    near_flr_ave_total; /**< Near end total frame loss ratio in per mille (average since last clear) */
    uint32_t    far_flr_ave_total;  /**< Far end total frame loss ratio in per mille (average since last clear)  */
    uint32_t    near_flr_min;       /**< Near end minimum frame loss ratio in per mille                          */
    uint32_t    far_flr_min;        /**< Far end minimum frame loss ratio in per mille                           */
    uint32_t    near_flr_max;       /**< Near end maximum frame loss ratio in per mille                          */
    uint32_t    far_flr_max;        /**< Far end maximum frame loss ratio in per mille                           */

    uint32_t    flr_interval_elapsed;  /**< Number of FLR intervals elapsed. The interval FLR is calculated when interval is elapsed */

} vtss_appl_mep_lm_state_t;


/**
 * \brief Instance Loss Measurement control data structure.
 *
 * This structure is used to do control of instance LM.
 */
typedef struct {
    mesa_bool_t    clear;         /**< clear any LM results */
} vtss_appl_mep_lm_control_t;

/**
 *  Loss Measurement Availability State
*/
typedef enum {
    VTSS_APPL_MEP_OAM_LM_AVAIL,                      /**< Loss Measurement Availability State is Available.   */
    VTSS_APPL_MEP_OAM_LM_UNAVAIL,                    /**< Loss Measurement Availability State is Unavailable. */
} vtss_appl_mep_lm_avail_state;

/**
 * \brief Loss Measurement Availability configuration data structure.
 *
 * This structure is used to do LM Availability configuration of an instance.
 */
typedef struct {
    mesa_bool_t  enable;     /**< Enabled if true                                                       */
    uint32_t     flr_th;     /**< Availability FLR Threshold in per mille                               */
    uint32_t     interval;   /**< Availability interval - number of measurements same availability in order to change Availability state */
    mesa_bool_t  main;       /**< Availability Maintenance                                              */
} vtss_appl_mep_lm_avail_conf_t;

/**
 * \brief Loss Measurement Availability status data structure.
 *
 * This structure is used to retrieve LM Availability status information for a peer instance.
 */
typedef struct {
    vtss_appl_mep_lm_avail_state near_state;        /**< Near end Availability State                         */
    vtss_appl_mep_lm_avail_state far_state;         /**< Far end Availability State                          */
    uint32_t                     near_cnt;          /**< Near end Availability Count                         */
    uint32_t                     far_cnt;           /**< Far end Availability Count                          */
    uint32_t                     near_un_cnt;       /**< Near end Unavailability count                       */
    uint32_t                     far_un_cnt;        /**< Far end Unavailability count                        */
    uint32_t                     near_avail_cnt;    /**< Near end sliding windows availability count       */
    uint32_t                     far_avail_cnt;     /**< Far end sliding windows availability count        */
} vtss_appl_mep_lm_avail_state_t;

/**
 * \brief Loss Measurement High Loss Interval configuration data structure.
 *
 * This structure is used to do LM High Loss Interval configuration of an instance.
 */
typedef struct {
    mesa_bool_t enable;      /**< Enabled if true                                                  */
    uint32_t  flr_th;        /**< High Loss Interval FLR Threshold in per mille                    */
    uint32_t  con_int;       /**< High Loss Interval consecutive interval (number of measurements) */
    uint32_t  cnt_holddown;  /**< Holddown timer in seconds for JSON notification updates for HLI near_cnt and far_cnt */
} vtss_appl_mep_lm_hli_conf_t;

/**
 * \brief Loss Measurement High Loss Interval status data structure.
 *
 * This structure is used to retrieve LM High Loss Interval status information.
 */
typedef struct {
    uint32_t near_cnt;      /**< Near end High Loss Interval count (number of measurements where Availability State is available and FLR is above HLI FLR threshold */
    uint32_t far_cnt;       /**< Far end High Loss Interval count (number of measurements where Availability State is available and FLR is above HLI FLR threshold */
    uint32_t near_con_cnt;  /**< Near end High Loss Interval consecutive count                      */
    uint32_t far_con_cnt;   /**< Far end High Loss Interval consecutive count                       */
} vtss_appl_mep_lm_hli_state_t;

/**
 * \brief Loss Measurement Signal Degraded configuration data structure.
 *
 * This structure is used to do LM Signal Degraded configuration of an instance.
 */
typedef struct {
    mesa_bool_t  enable;   /**< Enabled if true                                                                  */
    uint32_t     tx_min;   /**< Minimum number of frames that must be transmitted in a measurement before FLR is tested against the SDEG FLR threshold */
    uint32_t     flr_th;   /**< Signal Degraded FLR threshold in per mille                                       */
    uint32_t     bad_th;   /**< Number of consecutive bad interval measurements required to set degrade state    */
    uint32_t     good_th;  /**< Number of consecutive good interval measurements required to clear degrade state */
} vtss_appl_mep_lm_sdeg_conf_t;

/**
 * \brief Loss Measurement Notification status data structure.
 *
 * This structure is used to retrieve LM Notification status information.
 */
typedef struct {
    uint32_t                      peer_index;         /**< Peer MEP index                                                */
    vtss_appl_mep_lm_avail_state  near_state;         /**< Near end Availability State                                   */
    vtss_appl_mep_lm_avail_state  far_state;          /**< Far end Availability State                                    */
    uint32_t                      near_los_int_cnt;   /**< Number of measurements that has contributed to near end loss (since last clear) */
    uint32_t                      far_los_int_cnt;    /**< Number of measurements that has contributed to far end loss (since last clear) */
    uint32_t                      near_los_th_cnt;    /**< Near end frame loss threshold count                           */
    uint32_t                      far_los_th_cnt;     /**< Far end frame loss threshold count                            */
} vtss_appl_mep_lm_notif_state_t;

/**
 *  Different ways of calculation
*/
typedef enum {
    VTSS_APPL_MEP_RDTRP,    /**< Round-trip DM. The calculated delay is including the far-end residence time    */
    VTSS_APPL_MEP_FLOW      /**< Flow DM. The calculated delay is excluding the far-end residence time          */
} vtss_appl_mep_dm_calcway_t;

/**
 *  The resolution of the DM results
*/
typedef enum {
    VTSS_APPL_MEP_US,   /**< Delay is calculated in micro second    */
    VTSS_APPL_MEP_NS    /**< Delay is calculated in nano second     */
} vtss_appl_mep_dm_tunit_t;

/**
 *  Action taken when internal delay counter overflow
*/
typedef enum {
    VTSS_APPL_MEP_DISABLE,      /**< When internal total delay counter overflow, DM is disabled automatically               */
    VTSS_APPL_MEP_CONTINUE      /**< When internal total delay counter overflow, counter is reset and DM continues running  */
} vtss_appl_mep_dm_act_t;

/**
 * \brief Delay Measurement configuration data structure.
 *
 * This structure is used to do DM configuration of an instance.
 * The DM can be either single ended or dual ended.
 * When DM is activated, DM PDU are transmitted and DM results are calculated.
 * Reception of DMM and conversion into DMR is always enabled. Reception of 1DM is always enabled.
 * The following elements cannot be changed when DM is already enabled. They will be set when the DM is changed from disable to enable (create):
 *      ended.
 */
typedef struct {
/* Modify/add this for DM */
    mesa_bool_t                 enable;            /**< Enabled if true                                                                                                                           */
    uint32_t                    prio;              /**< Priority for the DM. Range 0 to 7                                                                                                         */
    vtss_appl_mep_cast_t        cast;              /**< Uni/multicast selection                                                                                                                   */
    uint32_t                    mep;               /**< Peer MEP to receive DMM/1DM - only used if unicast.                                                                                       */
    vtss_appl_mep_ended_t       ended;             /**< Dual/single ended selection. 1DM or DMM/DMR based                                                                                         */
    vtss_appl_mep_dm_calcway_t  calcway;           /**< Round trip or flow calculation way selection                                                                                              */
    uint32_t                    interval;          /**< Interval between 1DM/DMM in 10 ms. capabilities.dm_interval_min to capabilities.dm_interval_max                                           */
    uint32_t                    lastn;             /**< The number of last N measurements used to calculate average delay and variation. capabilities.dm_lastn_min to capabilities.dm_lastn_max   */
    vtss_appl_mep_dm_tunit_t    tunit;             /**< Calculation time unit                                                                                                                     */
    vtss_appl_mep_dm_act_t      overflow_act;      /**< Action when internal total delay counter overflow                                                                                         */
    mesa_bool_t                 synchronized;      /**< Near-end and far-end is real-time synchronized. One-way DM calculation on receiving DMR                                                   */
    mesa_bool_t                 proprietary;       /**< DM based on Vitesse proprietary follow-up PDU                                                                                             */
    uint16_t                    num_of_bin_fd;     /**< Configurable number of FD Measurement Bins / per Measurement Interval. default:3, min:2, max:10                                           */
    uint16_t                    num_of_bin_ifdv;   /**< Configurable number of IFDV Measurement Bins / per Measurement Interval. default:2, min:2, max:10                                         */
    uint32_t                    m_threshold;       /**< Configurable the measurement threshold for each Measurement Bin. default: 5000                                                            */
} vtss_appl_mep_dm_conf_t;

/**
 * \brief Delay Measurement status data structure.
 *
 * This structure is used to retrieve DM status information.
 */
typedef struct {
    uint32_t                    tx_cnt;                                     /**< Transmitted DMM/1DM frames */
    uint32_t                    rx_cnt;                                     /**< Received DMR/1DM frames */
    uint32_t                    rx_tout_cnt;                                /**< Received DMR time-out. After transmission of DMM, the DMR is expected to be received within 1 sec. */
    uint32_t                    rx_err_cnt;                                 /**< Received error counter. It is considered an error if a delay is negative or above 1 sec. */
    uint32_t                    ovrflw_cnt;                                 /**< The internal total delay counter overflow counter. */
    uint32_t                    avg_delay;                                  /**< The total average delay */
    uint32_t                    avg_n_delay;                                /**< The last N average delay */
    uint32_t                    avg_delay_var;                              /**< The total average delay variation */
    uint32_t                    avg_n_delay_var;                            /**< The last N average delay variation */
    uint32_t                    min_delay;                                  /**< The minimum delay measured */
    uint32_t                    max_delay;                                  /**< The maximum delay measured */
    uint32_t                    min_delay_var;                              /**< The minimum delay variation measured */
    uint32_t                    max_delay_var;                              /**< The maximum delay variation measured */
    vtss_appl_mep_dm_tunit_t    tunit;                                      /**< Time resolution */
    uint32_t                    fd_bin[VTSS_APPL_MEP_DM_BINS_FD_MAX];       /**< binning for FD */
    uint32_t                    ifdv_bin[VTSS_APPL_MEP_DM_BINS_IFDV_MAX];   /**< binning for IFDV */
} vtss_appl_mep_dm_state_t;

/**
 * \brief FD Bin
 *
 * This structure is used to do FD Bin status of an instance.
 *
 * Note: This structure is deprecated. Use the vtss_appl_mep_dm_bin_value_t instead.
 */
typedef struct {
    uint32_t   fd_bin;    /**< binning element for FD */
} vtss_appl_mep_dm_fd_bin_state_t;

/**
 * \brief IFDV Bin
 *
 * This structure is used to do FD Bin status of an instance.
 *
 * Note: This structure is deprecated. Use the vtss_appl_mep_dm_bin_value_t instead.
 */
typedef struct {
    uint32_t   ifdv_bin;    /**< binning element for IFDV */
} vtss_appl_mep_dm_ifdv_bin_state_t;

/**
 * \brief Delay Measurement bin value data structure.
 *
 * This structure is used to retrieve DM FD and IFDV bin values for a single bin.
 */
typedef struct {
    uint32_t   two_way;       /**< Two-way bin value */
    uint32_t   one_way_fn;    /**< One-way far-to-near bin value */
    uint32_t   one_way_nf;    /**< One-way near-to-far bin value */
} vtss_appl_mep_dm_bin_value_t;

/**
 * \brief Instance Delay Measurement control data structure.
 *
 * This structure is used to do control of instance DM.
 */
typedef struct {
    mesa_bool_t    clear;         /**< clear any DM results */
} vtss_appl_mep_dm_control_t;

/**
 *  The APS handling type
*/
typedef enum {
    VTSS_APPL_MEP_INV_APS,  /**< Invalid/undefined      */
    VTSS_APPL_MEP_L_APS,    /**< Linear protection APS  */
    VTSS_APPL_MEP_R_APS     /**< Ring protection APS    */
} vtss_appl_mep_aps_type_t;

/**
 * \brief Automatic Protection Switching configuration data structure.
 *
 * This structure is used to do APS configuration of an instance.
 * The MEP can handle either Linear or Ring protection APS PDU.
 */
typedef struct {
    mesa_bool_t                enable;         /**< Enabled if true                       */
    uint32_t                   prio;           /**< Priority for the APS. Range 0 to 7.   */
    vtss_appl_mep_aps_type_t   type;           /**< Type of APS                           */
    vtss_appl_mep_cast_t       cast;           /**< Uni/multicast selection               */
    uint32_t                   raps_octet;     /**< Last octet in the R-APS multicast DA  */
} vtss_appl_mep_aps_conf_t;

/**
 * \brief Link Trace configuration data structure.
 *
 * This structure is used to do LT configuration of an instance.
 * When Link Trace is enabled five transactions are initiated (LTM transmission) with five seconds interval.
 * The transaction ID will be incremented for each transaction.
 * All replies will be collected for each transaction.
 * Any elements in an enabled LT cannot be changed
 * LT will automatically disable when transactions are completed.
 */
typedef struct {
    mesa_bool_t   enable;         /**< Enabled if true                                                                              */
    uint32_t      prio;           /**< Priority for the LTM                                                                         */
    uint32_t      mep;            /**< Target Peer Mep to receive LTM - only used if 'mac' is 'all zero'                            */
    mesa_mac_t    mac;            /**< Unicast MAC address to receive LTM - has to be used to send LTM to MIP                       */
    uint32_t      ttl;            /**< Time To Live                                                                                 */
    uint32_t      transactions;   /**< Number of requested transactions - transmitted LTM. range 1 to VTSS_APPL_MEP_TRANSACTION_MAX */
} vtss_appl_mep_lt_conf_t;

/**
 *  The Relay action
*/
typedef enum {
    VTSS_APPL_MEP_RELAY_UNKNOWN,    /**< Unknown relay action                   */
    VTSS_APPL_MEP_RELAY_HIT,        /**< Relay based on match of target MAC     */
    VTSS_APPL_MEP_RELAY_FDB,        /**< Relay based on hit in FDB              */
    VTSS_APPL_MEP_RELAY_MPDB,       /**< Relay based on hit in MIP CCM database */
} vtss_appl_mep_relay_act_t;

/**
 * \brief Transaction reply structure. There will be one of this in vtss_appl_mep_lt_transaction_t for each reply received in the transaction.
 *
 */
typedef struct {
    vtss_appl_mep_mode_t        mode;              /**< The reply is done by a MEP or a MIP            */
    vtss_appl_mep_direction_t   direction;         /**< The reply is done by a UP or a Down instance   */
    uint8_t                     ttl;               /**< The reply TTL value                            */
    mesa_bool_t                 forwarded;         /**< The LTM was forwarded                          */
    vtss_appl_mep_relay_act_t   relay_action;      /**< The relay action                               */
    mesa_mac_t                  last_egress_mac;   /**< The Last Egress MAC for this reply             */
    mesa_mac_t                  next_egress_mac;   /**< The Next Egress MAC of this reply              */
} vtss_appl_mep_lt_reply_t;

/**
 * \brief Transaction replies structure containing the replies for a transaction. There will be one of this in vtss_appl_mep_lt_state_t for each transaction.
 */
typedef struct {
    uint32_t                   transaction_id;                   /**< Transaction ID                         */
    uint32_t                   reply_cnt;                        /**< Number of replies for this transaction */
    vtss_appl_mep_lt_reply_t   reply[VTSS_APPL_MEP_REPLY_MAX];   /**< Transaction replies                    */
} vtss_appl_mep_lt_transaction_t;

/**
 * \brief Link Trace status data structure.
 *
 * This structure is used to retrieve LT status information.
 */
typedef struct {
    uint32_t                         transaction_cnt;                            /**< Number of transactions         */
    vtss_appl_mep_lt_transaction_t   transaction[VTSS_APPL_MEP_TRANSACTION_MAX]; /**< Replies for each transaction   */
} vtss_appl_mep_lt_state_t;

/**
 * \brief Loop Back configuration data structure.
 *
 * This structure is used to do LB configuration of an instance.
 * When Loop Back is enabled LBM frames are transmitted with the specified interval and replies are received.
 * Data TLV with pattern 0xAA is inserted to achieve configured 'size'
 * The 'size' of the LBM frame is including SMAC+DMAC+ETHERTYPE+CRC = 16 bytes.
 * When LBM transmission is not configured 'infinite' the the LBR source MAC is collected.
 * Reception of LBM and conversion into LBR is always enabled.
 * LB will automatically disable when all LBM are transmitted - unless configured 'infinite'.
 * Any elements in an enabled LB cannot be changed
 */
typedef struct {
    mesa_bool_t           enable;    /**< Enabled if true                                                                                      */
    mesa_bool_t           dei;       /**< DEI for the LBM                                                                                      */
    uint32_t              prio;      /**< Priority for the LBM. Range 0 to 7.                                                                  */
    vtss_appl_mep_cast_t  cast;      /**< Uni/multicast selection                                                                              */
    uint32_t              mep;       /**< Peer Mep to receive LBM - only used if unicast and 'mac' is 'all zero'                               */
    mesa_mac_t            mac;       /**< Unicast MAC address to receive LBM - has to be used to send LBM to MIP                               */
    uint32_t              to_send;   /**< Number of LBM to send. capabilities.lb_to_send_infinite => test behaviour. Requires VOE              */
    uint32_t              size;      /**< Size of LBM frame to send - max. capabilities.lbm_size_max bytes - min. capabilities.lbm_size_min    */
    uint32_t              interval;  /**< Frame interval. In 10 ms. if to_send != capabilities.lb_to_send_infinite. Max 1 s.                   */
                                     /**<                 In 1 us. if to_send == capabilities.lb_to_send_infinite. Max 10 ms.                  */
    uint8_t               ttl;       /**< Time-To-Live value used for an MPLS-TP OAM LBM PDU (1-255).                                          */
} vtss_appl_mep_lb_conf_t;

/**
 * \brief The LBM reply structure. There will be one of this in vtss_appl_MEP_lb_state_t for each Peer-MEP LBR received.
 */
typedef struct {
    mesa_mac_t  mac;            /**< LBR source MAC - this is only relevant if not 'infinite' LBM   */
    uint64_t    lbr_received;   /**< Number LBR received from MAC                                   */
    uint64_t    out_of_order;   /**< The OOO counter                                                */
    uint8_t     mep_mip_id[16]; /**< The replying ICC-based MEP/MIP ID (subType 0x02 or 0x03)       */
} vtss_appl_mep_lb_reply_t;

/**
 * \brief Loop Back status data structure.
 *
 * This structure is used to retrieve LB status information.
 * The 'reply' array is only containing Peer MEP information, if not 'infinite' LBM transmission.
 * In case of 'infinite' LBM transmission there will be one 'reply' that contains the number of received LBR and the OOO counter
 */
typedef struct {
    uint32_t                   transaction_id;                 /**< This is the transaction id that is used to send the next LBM                   */
    uint64_t                   lbm_transmitted;                /**< The number of transmitted LBM                                                  */
    uint32_t                   reply_cnt;                      /**< The number of replying MAC. In case of 'infinite' LBM there is only one reply  */
    vtss_appl_mep_lb_reply_t   reply[VTSS_APPL_MEP_PEER_MAX];  /**< The replies                                                                    */
} vtss_appl_mep_lb_state_t;

/**
 *  The TST PDU Data TLV pattern
*/
typedef enum {
    VTSS_APPL_MEP_PATTERN_ALL_ZERO,  /**< All zero data pattern    */
    VTSS_APPL_MEP_PATTERN_ALL_ONE,   /**< All zero data pattern    */
    VTSS_APPL_MEP_PATTERN_0XAA,      /**< 10101010 data pattern    */
} vtss_appl_mep_pattern_t;

/**
 * \brief Test Signal configuration data structure.
 *
 * This structure is used to do TST configuration of an instance.
 * When Test Signal transmission is enabled TST frames are transmitted with the specified bit rate.
 * Data TLV with configured 'pattern' is inserted to achieve configured 'size'.
 * The 'size' of the TST frame is including SMAC+DMAC+ETHERTYPE+CRC = 16 bytes.
 * When Test Signal reception is enabled received TST frames are counted and bit rate calculated.
 * Test TLV is not supported.
 */
typedef struct {
    mesa_bool_t              enable;     /**< Enable/disable transmission of TST PDU                                                                     */
    mesa_bool_t              enable_rx;  /**< Enable/disable reception and analyse of TST PDU                                                            */
    uint32_t                 prio;       /**< Priority for the TST frame. Range 0 to 7.                                                                  */
    mesa_bool_t              dei;        /**< DEI for the TST frame                                                                                      */
    uint32_t                 mep;        /**< Peer MEP to receive TST.                                                                                   */
    uint32_t                 rate;       /**< The transmission data bit rate in Kbps - max capabilities.tst_rate_max - min 1                             */
                                         /**< In VLAN and EVC domain the Layer 2 frame size is used when calculating the frame rate                      */
                                         /**< In Port domain preamble and inter frame gap is added to Layer 2 frame size when calculating the frame rate */
    uint32_t                 size;       /**< Size of TST frame to send - max. capabilities.tst_size_max bytes - min. capabilities.tst_size_min          */
    vtss_appl_mep_pattern_t  pattern;    /**< Pattern in TST frame Data TLV                                                                              */
    mesa_bool_t              sequence;   /**< Sequence number will be inserted - not checked in receiver on Caracal and Jaguar                           */
} vtss_appl_mep_tst_conf_t;

/**
 *  The Drop Precedence indication
*/
typedef enum {
    VTSS_APPL_MEP_DP_GREEN,      /**< Drop Precedence is green    */
    VTSS_APPL_MEP_DP_YELLOW,     /**< Drop Precedence is yellow   */
} vtss_appl_mep_dp_t;

/**
 * \brief Test Signal transmission configuration data structure. (obsoleting vtss_appl_mep_tst_conf_t)
 *
 * This structure is used to do Test transmission configuration per priority.
 * When Test transmission is enabled TST/LBM frames are transmitted at the specified size and bit rate.
 * 'NULL' Test TLV or optional Data TLV with configured 'pattern' is inserted to achieve configured 'size'.
 * The 'rate' is not used for G.8021 LB 'series' behaviour. See vtss_appl_mep_lb_common_conf_t
*/
typedef struct {
    mesa_bool_t              enable;   /**< Enable/disable Test frame transmission                                                                         */
    vtss_appl_mep_dp_t       dp;       /**< Drop Precedence for the Test frame.                                                                            */
    uint32_t                 size;     /**< Size of Test frame to send - max. capabilities.tst_size_max bytes - min. capabilities.tst_size_min             */
                                       /**< This is the Layer 2 frame size including SMAC+DMAC+ETHERTYPE+CRC                                               */
    uint32_t                 rate;     /**< The transmission data bit rate in Kbps - max capabilities.tst_rate_max - min 1                                 */
                                       /**< In VLAN and EVC domain the Layer 2 frame size is used when calculating the frame rate                          */
                                       /**< In Port domain preamble and inter frame gap is added to Layer 2 frame size when calculating the frame rate     */
    vtss_appl_mep_pattern_t  pattern;  /**< Pattern used to build Data TLV in Test frame.                                                                  */
    uint8_t                  ttl;      /**< MPLS-TP TTL value                                                                                              */
} vtss_appl_mep_test_prio_conf_t;

/**
 * \brief Test Signal common configuration data structure. (obsoleting vtss_appl_mep_tst_conf_t)
 *
 * This structure is used to do the TST configuration that is common for all possible priorities.
 * When Test Signal reception is enabled received TST frames are counted and bit rate calculated.
 */
typedef struct {
    mesa_bool_t   enable_rx;   /**< Enable/disable reception and analyse of TST PDU                                    */
    uint32_t      mep;         /**< Peer Mep to receive TST                                                            */
    mesa_bool_t   sequence;    /**< Sequence number will be inserted - not checked in receiver on Caracal and Jaguar   */
} vtss_appl_mep_tst_common_conf_t;

/**
 * \brief Loop Back common configuration data structure. (obsoleting vtss_appl_mep_lb_conf_t)
 *
 * This structure is used to do the Loop Back configuration that is common for all possible priorities.
 */
typedef struct {
    uint32_t              mep;       /**< Peer Mep to receive LBM - only used if unicast and 'mac' is 'all zero'                            */
    mesa_mac_t            mac;       /**< Unicast MAC address to receive LBM - has to be used to send LBM to MIP                            */
    vtss_appl_mep_cast_t  cast;      /**< Uni/multicast selection                                                                           */
    mesa_bool_t           sequence;  /**< Sequence number will be inserted - not checked in receiver on Caracal and Jaguar                  */
    uint32_t              to_send;   /**< Number of LBM to send. Not capabilities.lb_to_send_infinite enable G.8021 'series behaviour'. capabilities.lb_to_send_infinite enables G.8021 'test' behaviour.  */
    uint32_t              interval;  /**< Frame interval. G.8021 'series' behaviour frame interval                                          */
} vtss_appl_mep_lb_common_conf_t;

/**
 * \brief Delay Measurement transmission configuration data structure. (obsoleting vtss_appl_mep_dm_conf_t)
 *
 * This structure is used to do Delay Measurement configuration per priority.
*/
typedef struct {
    mesa_bool_t   enable;   /**< Enable/disable Delay frame transmission  */
} vtss_appl_mep_dm_prio_conf_t;

/**
 * \brief Delay Measurement common configuration data structure. (obsoleting vtss_appl_mep_dm_conf_t)
 *
 * This structure is used to do the Delay Measurement configuration that is common for all possible priorities.
 */
typedef struct {
    vtss_appl_mep_cast_t        cast;              /**< Uni/multicast selection                                                                                                                   */
    uint32_t                    mep;               /**< Peer MEP to receive DMM/1DM - only used if unicast.                                                                                       */
    vtss_appl_mep_ended_t       ended;             /**< Dual/single ended selection. 1DM or DMM/DMR based                                                                                         */
    vtss_appl_mep_dm_calcway_t  calcway;           /**< Round trip or flow calculation way selection                                                                                              */
    uint32_t                    interval;          /**< Interval between 1DM/DMM in 10 ms. capabilities.dm_interval_min to capabilities.dm_interval_max                                           */
    uint32_t                    lastn;             /**< The number of last N measurements used to calculate average delay and variation. capabilities.dm_lastn_min to capabilities.dm_lastn_max   */
    vtss_appl_mep_dm_tunit_t    tunit;             /**< Calculation time unit                                                                                                                     */
    vtss_appl_mep_dm_act_t      overflow_act;      /**< Action when internal total delay counter overflow                                                                                         */
    mesa_bool_t                 synchronized;      /**< Near-end and far-end is real-time synchronized. One-way DM calculation on receiving DMR                                                   */
    mesa_bool_t                 proprietary;       /**< DM based on Vitesse proprietary follow-up PDU                                                                                             */
    uint16_t                    num_of_bin_fd;     /**< Configurable number of FD Measurement Bins / per Measurement Interval. default:3, min:2, max:10                                           */
    uint16_t                    num_of_bin_ifdv;   /**< Configurable number of IFDV Measurement Bins / per Measurement Interval. default:2, min:2, max:10                                         */
    uint32_t                    m_threshold;       /**< Configurable the measurement threshold for each Measurement Bin. default: 5000                                                            */
} vtss_appl_mep_dm_common_conf_t;

/**
 * \brief Test Signal status data structure.
 *
 * This structure is used to retrieve TST status information.
 */
typedef struct {
    uint64_t    tx_counter;  /**< Transmitted TST frames counter                             */
    uint64_t    rx_counter;  /**< Received TST frames counter                                */
    uint64_t    oo_counter;  /**< Out of Order counter                                       */
    uint32_t    rx_rate;     /**< Receive bit rate in Kbit/s                                 */
    uint32_t    time;        /**< Test time in seconds. The elapsed time since last clear    */
} vtss_appl_mep_tst_state_t;

/**
 * \brief Test status data structure.
 *
 * This structure is used to retrieve TEST status information for one priority and colour.
 * Counters are based of TST/LBM-LBR PDU counting
 */
typedef struct {
    uint64_t    tx_counter;  /**< Transmitted TEST frames counter   */
    uint64_t    rx_counter;  /**< Received TEST frames counter      */
} vtss_appl_mep_test_prio_state_t;

/** * \brief Instance Test Signal control data structure.
 *
 * This structure is used to do control of instance TST.
 */
typedef struct {
    mesa_bool_t    clear;         /**< clear any TST results */
} vtss_appl_mep_tst_control_t;

/**
 * \brief Client flow configuration data structure.
 *
 * This structure is used to do Client flow configuration of an instance.
 */
typedef struct {
    uint8_t  ais_prio;      /**< AIS Priority (EVC COS-ID) 0-7. capabilities.client_prio_highest indicate highest possible is requested */
    uint8_t  lck_prio;      /**< LCK Priority (EVC COS-ID) 0-7. capabilities.client_prio_highest indicate highest possible is requested */
    uint8_t  level;         /**< Client flow level                                                                                      */
} vtss_appl_mep_client_flow_conf_t;

/**
 * \brief Alarm Indication Signal configuration data structure.
 *
 * This structure is used to do AIS configuration of an instance.
 * When AIS is enabled AIS frames are transmitted when aAIS becomes true as described in G.8021.
 * AIS is transmitted in the Client flows configured.
 */
typedef struct {
    mesa_bool_t            enable;      /**< AIS is enabled                                                                                       */
    mesa_bool_t            protection;  /**< The first three AIS frames are transmitted as fast as possible                                       */
    vtss_appl_mep_rate_t   rate;        /**< Transmission rate of the transmitted AIS. VTSS_APPL_MEP_RATE_1S and VTSS_APPL_MEP_RATE_1M is allowed */
} vtss_appl_mep_ais_conf_t;

/**
 * \brief Locked Signal configuration data structure.
 *
 * This structure is used to do LCK configuration of an instance.
 * When LCK is enabled LCK frames are transmitted with the configured period.
 * LCK is transmitted in the Client flows configured.
 */
typedef struct {
    mesa_bool_t            enable;    /**< LCK is enabled                            */
    vtss_appl_mep_rate_t   rate;      /**< Transmission rate of the transmitted LCK  */
} vtss_appl_mep_lck_conf_t;

/**
 *  BFD session state
 */
typedef enum {
    VTSS_APPL_MEP_BFD_STATE_ADMIN_DOWN = 0,
    VTSS_APPL_MEP_BFD_STATE_DOWN       = 1,
    VTSS_APPL_MEP_BFD_STATE_INIT       = 2,
    VTSS_APPL_MEP_BFD_STATE_UP         = 3
} vtss_appl_mep_bfd_session_state_t;

/**
 *  BFD diagnostic codes
 */
typedef enum {
    VTSS_APPL_MEP_BFD_DIAG_NONE                     = 0,
    VTSS_APPL_MEP_BFD_DIAG_DETECT_EXPIRED           = 1,
    VTSS_APPL_MEP_BFD_DIAG_ECHO_FAILED              = 2,
    VTSS_APPL_MEP_BFD_DIAG_REMOTE_DOWN              = 3,
    VTSS_APPL_MEP_BFD_DIAG_FWD_PLANE_RESET          = 4,
    VTSS_APPL_MEP_BFD_DIAG_PATH_DOWN                = 5,
    VTSS_APPL_MEP_BFD_DIAG_CONCAT_PATH_DOWN         = 6,
    VTSS_APPL_MEP_BFD_DIAG_ADMIN_DOWN               = 7,
    VTSS_APPL_MEP_BFD_DIAG_REVERSE_CONCAT_PATH_DOWN = 8,
    VTSS_APPL_MEP_BFD_DIAG_MISCONNECTIVITY          = 9
} vtss_appl_mep_bfd_diag_t;

/**
 *  BFD authentication
 */
typedef enum {
    VTSS_APPL_MEP_BFD_AUTH_DISABLED              = 0,
    VTSS_APPL_MEP_BFD_AUTH_SIMPLE_PWD            = 1,
    VTSS_APPL_MEP_BFD_AUTH_KEYED_MD5             = 2,
    VTSS_APPL_MEP_BFD_AUTH_METICULOUS_KEYED_MD5  = 3,
    VTSS_APPL_MEP_BFD_AUTH_KEYED_SHA1            = 4,
    VTSS_APPL_MEP_BFD_AUTH_METICULOUS_KEYED_SHA1 = 5
} vtss_appl_mep_bfd_auth_type_t;

/**
 * \brief ITU G.8113.2 BFD CC/CV configuration data structure.
 *
 * This structure is used to do BFD CC/CV configuration of an instance.
 * When enabled both transmission and LOC detection is enabled
 */
typedef struct {
    mesa_bool_t  enable;            /**< CC/CV transmission and LOC detection is enabled if true               */
    mesa_bool_t  is_independent;    /**< Independent mode if true, coordinated if false                        */
    mesa_bool_t  cc_only;           /**< Only transmit CC frames (no CV frames) if true                        */
    mesa_bool_t  tx_auth_enabled;   /**< TRUE: Authentication enabled for transmission of CC/CV frames to peer */
    mesa_bool_t  rx_auth_enabled;   /**< TRUE: Authentication enabled for reception of CC/CV frames from peer  */
    uint8_t      tx_auth_key_id;    /**< Authentication used for transmission of CC/CV frames to peer          */
    uint32_t     cc_period;         /**< Desired CC transmission period in us, 1 us - 60 sec                   */
    uint32_t     rx_flow;           /**< Rx flow number (MPLS-TP link, MPLS-TP tunnel, MPLS-TP LSP or
                                         MPLS-TP Pseudo-Wire) for independent mode                               */
} vtss_appl_mep_g8113_2_bfd_conf_t;

/**
 * \brief ITU G.8113.2 BFD CC/CV Authentication Key configuration data structure.
 *
 * This structure is used to configure Authentication Keys used with ITU G.8113.2 BFD CC/CV.
 */
typedef struct {
    vtss_appl_mep_bfd_auth_type_t  key_type;          /**< Key type - vtss_appl_mep_bfd_auth_type_t                                  */
    uint8_t                        key_len;           /**< Key length in bytes. 1-16 for Simple Password, 16 for MD5 and 20 for SHA1 */
    uint8_t                        key[20];           /**< Key                                                                       */
} vtss_appl_mep_g8113_2_bfd_auth_conf_t;

/**
 * \brief ITU G.8113.2 BFD CC/CV state data structure.
 *
 * This structure is used to retrieve BFD CC/CV status information.
 */
typedef struct {
    uint64_t                          cc_tx_cnt;                /**< BFD CC transmitted                                                         */
    uint64_t                          valid_cc_rx_cnt;          /**< Valid BFD CC received                                                      */
    uint64_t                          invalid_cc_rx_cnt;        /**< Invalid BFD CC received                                                    */
    uint64_t                          cv_tx_cnt;                /**< BFD CV transmitted                                                         */
    uint64_t                          valid_cv_rx_cnt;          /**< Valid BFD CV received                                                      */
    uint64_t                          invalid_cv_rx_cnt;        /**< Invalid BFD CV received                                                    */
    uint32_t                          my_discr[2];              /**< Selected random "My Discriminator" value (source/sink), non-zero           */
    vtss_appl_mep_bfd_session_state_t session_state[2];         /**< Local BFD session state (for coordinated mode only index 0 is valid, for
                                                                     independent mode index 0 is sink, 1 is source.                             */
    vtss_appl_mep_bfd_diag_t          local_diag;               /**< Local diagnostic code                                                      */
    vtss_appl_mep_bfd_session_state_t remote_session_state;     /**< BFD session state received from remote end                                 */
    vtss_appl_mep_bfd_diag_t          remote_diag;              /**< Diagnostic code received from remote end                                   */
    uint32_t                          remote_discr[2];          /**< Discriminator value received from remote end (0: source, 1: sink)          */
    uint32_t                          remote_min_rx;            /**< Required Min RX interval value received from remote end                    */
    uint8_t                           remote_flags;             /**< Flags received from remote end                                             */

    uint32_t                          unexp_discr;              /**< Last received unexpected discriminator value (should be my configured)     */
    uint8_t                           exp_src_mep_id_tlv[28];   /**< Expected source MEP ID TLV                                                 */
    uint8_t                           unexp_src_mep_id_tlv[28]; /**< Last received unexpected source MEP ID TLV                                 */
} vtss_appl_mep_g8113_2_bfd_state_t;

/** * \brief Instance BFD control data structure.
 *
 * This structure is used to do control of instance BFD.
 */
typedef struct {
    mesa_bool_t    clear;         /**< clear any BFD statistics */
} vtss_appl_mep_bfd_control_t;

/**
 *  Route Trace Pad TLV type
 */
typedef enum {
    VTSS_APPL_MEP_RT_PAD_TLV_TYPE_NONE = 0,  /**< No Pad TLV            */
    VTSS_APPL_MEP_RT_PAD_TLV_TYPE_DROP = 1,  /**< Discard Pad TLV on Rx */
    VTSS_APPL_MEP_RT_PAD_TLV_TYPE_COPY = 2   /**< Copy Pad TLV to reply */
} vtss_appl_mep_rt_pad_tlv_type_t;

/**
 * \brief ITU G.8113.2 Route Trace configuration data structure.
 *
 * This structure is used to do RT configuration of an instance.
 */
typedef struct {
    mesa_bool_t                      enable;          /**< Enabled if true                                */
    uint8_t                          tc;              /**< Traffic class for the RT request               */
    mesa_bool_t                      src_id_tlv;      /**< Include Source ID TLV in the RT request        */
    mesa_bool_t                      dst_id_tlv;      /**< Include Destination ID TLV in the RT request   */
    vtss_appl_mep_rt_pad_tlv_type_t  pad_tlv_type;    /**< Pad TLV type                                   */
    uint16_t                         pad_tlv_len;     /**< Pad TLV data length                            */
    uint8_t                          flags;           /**< Flags for RT request                           */
    uint8_t                          max_ttl;         /**< Max. TTL for RT request                        */
} vtss_appl_mep_rt_conf_t;

/**
 *  Route Trace Return Codes (ITU G.8113.2 / IETF RFC 4379)
 */
typedef enum {
    VTSS_APPL_MEP_RT_RETCODE_NONE                = 0,   /**< No return code                                                               */
    VTSS_APPL_MEP_RT_RETCODE_MALFORMED           = 1,   /**< Malformed echo request received                                              */
    VTSS_APPL_MEP_RT_RETCODE_TLV_ERR             = 2,   /**< One or more of the TLVs was not understood                                   */
    VTSS_APPL_MEP_RT_RETCODE_IS_EGRESS           = 3,   /**< Replying router is an egress for the FEC at stack-depth                      */
    VTSS_APPL_MEP_RT_RETCODE_NO_MAPPING          = 4,   /**< Replying router has no mapping for the FEC at stack-depth                    */
    VTSS_APPL_MEP_RT_RETCODE_DOWN_MAP_MISMATCH   = 5,   /**< Downstream Mapping Mismatch                                                  */
    VTSS_APPL_MEP_RT_RETCODE_UP_IF_UNKNOWN       = 6,   /**< Upstream Interface Index Unknown                                             */
    VTSS_APPL_MEP_RT_RETCODE_LABEL_SWITCH        = 8,   /**< Label switched at stack-depth                                                */
    VTSS_APPL_MEP_RT_RETCODE_LABEL_SWITCH_NO_FWD = 9,   /**< Label switched but no MPLS forwarding at stack-depth                         */
    VTSS_APPL_MEP_RT_RETCODE_MAPPING_WRONG       = 10,  /**< Mapping for this FEC is not the given label at stack-depth                   */
    VTSS_APPL_MEP_RT_RETCODE_NO_LABEL            = 11,  /**< No label entry at stack-depth                                                */
    VTSS_APPL_MEP_RT_RETCODE_PROTOCOL_ERR        = 12,  /**< Protocol not associated with interface at FEC stack-depth                    */
    VTSS_APPL_MEP_RT_RETCODE_PREMATURE           = 13   /**< Premature termination of ping due to label stack shrinking to a single label */
} vtss_appl_mep_rt_ret_code_t;

/**
 * \brief ITU G.8113.2 Route Trace reply data structure.
 *
 * This structure is used to store RT reply information. There will be one of this in vtss_appl_mep_rt_status_t for each reply received for the request.
 *
 */
typedef struct {
    uint32_t                     seq_no;             /**< Sequence number from reply                                      */
    vtss_appl_mep_rt_ret_code_t  ret_code;           /**< Return code from reply                                          */
    uint8_t                      ret_subcode;        /**< Return subcode from reply                                       */
    uint32_t                     rtt_sec;            /**< Round Trip Time (seconds)                                       */
    uint32_t                     rtt_usec;           /**< Round Trip Time (microseconds)                                  */
    uint32_t                     ts_sent_sec;        /**< Timestamp sent (sender, seconds)                                */
    uint32_t                     ts_sent_usec;       /**< Timestamp sent (sender, microseconds)                           */
    uint32_t                     ts_received_sec;    /**< Timestamp received (receiver, seconds)                          */
    uint32_t                     ts_received_usec;   /**< Timestamp received (receiver, microseconds)                     */
    uint32_t                     src_global_id;      /**< Source global ID from received FEC or source ID TLV in RT reply */
    uint32_t                     src_node_id;        /**< Source node ID from received FEC or source ID TLV in RT reply   */
    uint32_t                     fec_src_value;      /**< For a reply from a PW MEP this is the source AC-ID from the
                                                          received FEC in the RT reply. For a Tunnel MEP or LSP MIP bit
                                                          31:16 is the source tunnel number and bit 15:0 is the LSP number
                                                          from the received FEC in the RT reply.                          */
    uint32_t                     rx_label_cnt;       /**< Number of label values in rx_labels[]                           */
    uint32_t                     rx_if_num;          /**< Receiver local interface index                                  */
    uint32_t                     rx_labels[8];       /**< Label Stack of RT request as seen by receiver                   */
} vtss_appl_mep_rt_reply_t;

/**
 * \brief ITU G.8113.2 Route Trace status structure containing the replies for an RT request.
 *
 */
typedef struct {
    uint32_t                    seq_no;                            /**< Sequence number of first RT request */
    uint32_t                    request_cnt;                       /**< Number of RT requests sent          */
    uint32_t                    reply_cnt;                         /**< Number of replies received          */
    vtss_appl_mep_rt_reply_t    reply[VTSS_APPL_MEP_RT_REPLY_MAX]; /**< RT replies                          */
} vtss_appl_mep_rt_status_t;

/**
 * \brief Instance default structure.
 *
 * This structure is used to contain configuration default values.
 *
 */
typedef struct
{
    vtss_appl_mep_conf_t              config;             /**< Instance configuration default */
    vtss_appl_mep_peer_conf_t         peer_conf;          /**< Peer configuration default */
    vtss_appl_mep_pm_conf_t           pm_conf;            /**< Performance Monitoring default */
    vtss_appl_mep_lst_conf_t          lst_conf;           /**< Link State Tracking default */
    vtss_appl_mep_syslog_conf_t       syslog_conf;        /**< Syslog generation configuration default */
    vtss_appl_mep_tlv_conf_t          tlv_conf;           /**< Transmitted TLV configuration default */
    vtss_appl_mep_cc_conf_t           cc_conf;            /**< Continuity Check default */
    vtss_appl_mep_lm_conf_t           lm_conf;            /**< Loss Measurement default */
    vtss_appl_mep_lm_avail_conf_t     lm_avail_conf;      /**< Loss Measurement Availability default */
    vtss_appl_mep_lm_hli_conf_t       lm_hli_conf;        /**< Loss Measurement High Loss Interval default */
    vtss_appl_mep_lm_sdeg_conf_t      lm_sdeg_conf;       /**< Loss Measurement Signal Degrade default */
    vtss_appl_mep_dm_conf_t           dm_conf;            /**< Delay Measurement default */
    vtss_appl_mep_aps_conf_t          aps_conf;           /**< Automatic Protection Switching default */
    vtss_appl_mep_lt_conf_t           lt_conf;            /**< Link Trace default */
    vtss_appl_mep_lb_conf_t           lb_conf;            /**< Loop Back default */
    vtss_appl_mep_ais_conf_t          ais_conf;           /**< Alarm Indication Signal default */
    vtss_appl_mep_lck_conf_t          lck_conf;           /**< Locked Signal default */
    vtss_appl_mep_tst_conf_t          tst_conf;           /**< Test Signal default */
    vtss_appl_mep_tst_common_conf_t   tst_common_conf;    /**< Test Signal common default */
    vtss_appl_mep_lb_common_conf_t    lb_common_conf;     /**< Loop Back common default */
    vtss_appl_mep_test_prio_conf_t    test_prio_conf;     /**< Test priority default */
    vtss_appl_mep_dm_common_conf_t    dm_common_conf;     /**< Delay Measurement common default */
    vtss_appl_mep_dm_prio_conf_t      dm_prio_conf;       /**< Delay Measurement priority default */
    vtss_appl_mep_client_flow_conf_t  client_flow_conf;   /**< Client flow configuration default */
    vtss_appl_mep_g8113_2_bfd_conf_t  bfd_conf;           /**< BFD CC/CV default */
    vtss_appl_mep_rt_conf_t           rt_conf;            /**< Route Trace default */
} vtss_appl_mep_default_conf_t;

#if defined(MEP_IMPL_G1)
namespace mep_g1 {
#elif defined(MEP_IMPL_G2)
namespace mep_g2 {
#endif
/**
 * \brief Transmitted TLV configuration SET. (vtss_appl_mep_tlv_conf_t)
 *
 * This configuration is not per MEP instance.
 *
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mep_tlv_conf_set(const vtss_appl_mep_tlv_conf_t    *const conf);

/**
 * \brief Transmitted TLV configuration GET. (vtss_appl_mep_tlv_conf_t)
 *
 * This configuration is not per MEP instance.
 *
 * \param conf [OUT]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mep_tlv_conf_get(vtss_appl_mep_tlv_conf_t    *const conf);

/**
 * \brief Default GET. (vtss_appl_mep_default_conf_t)
 *
 * This returns the instance default configuration values
 *
 * \param conf [OUT]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 */
void vtss_appl_mep_default_conf_get(vtss_appl_mep_default_conf_t  *const conf);

/**
 * \brief Capabilities GET. (vtss_appl_mep_capabilities_t)
 *
 * This returns the instance capabilities
 *
 * \param conf [OUT]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 */
mesa_rc vtss_appl_mep_capabilities_get(vtss_appl_mep_capabilities_t *const conf);

/**
 * \brief instance basic configuration SET. (7)
 *
 * This function can enable or disable an instance, or change some parameters on an already enabled instance.
 * The following parameters cannot be changed in a enabled instance:
 * mode - direction - domain - flow - port.
 * An instance must be enabled in order to enable any other functionalities like CC - LM - DM.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MEP_RC_CHANGE_PARAMETER
 *          VTSS_APPL_MEP_RC_INVALID_MAC
 *          VTSS_APPL_MEP_RC_VOLATILE
 *          VTSS_APPL_MEP_RC_MIP_SUPPORT
 *          VTSS_APPL_MEP_RC_INVALID_VID
 *          VTSS_APPL_MEP_RC_PEER_ID
 *          VTSS_APPL_MEP_RC_PEER_CNT
 *          VTSS_APPL_MEP_RC_INVALID_MEG
 *          VTSS_APPL_MEP_RC_INVALID_EVC
 *          VTSS_APPL_MEP_RC_VLAN_SUPPORT
 *          VTSS_APPL_MEP_RC_INVALID_VID
 *          VTSS_APPL_MEP_RC_NO_VOE.
 */
mesa_rc vtss_appl_mep_instance_conf_set(const uint32_t                     instance,
                                        const vtss_appl_mep_conf_t    *const conf);

/**
 * \brief Instance basic configuration GET. (vtss_appl_mep_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_VOLATILE
 */
mesa_rc vtss_appl_mep_instance_conf_get(const uint32_t               instance,
                                        vtss_appl_mep_conf_t    *const conf);

/**
 * \brief Instance ADD. (vtss_appl_mep_conf_t)
 *
 * This add (enable) a new instance with the given instance number and configuration. The instance must
 * not already be enabled.
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_instance_conf_add(const uint32_t                    instance,
                                        const vtss_appl_mep_conf_t   *const conf);

/**
 * \brief Instance DELETE.
 *
 * This delete an enabled instance with the given instance number.
 *
 * \param instance [IN]        Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_instance_conf_delete(const uint32_t  instance);

/**
 * \brief Instance iterator.
 *
 * This returns the next enabled instance number. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance      [IN]   Instance number.
 * \param next_instance [OUT]  Next enabled instance.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_instance_conf_iter(const uint32_t    *const instance,
                                         uint32_t          *const next_instance);

/**
 * \brief instance Peer MEP configuration SET. (vtss_appl_mep_peer_conf_t)
 *
 * This set the configuration of a selected Peer MEP ID. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 * \param peer_id [IN]   Peer MEP ID.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_instance_peer_conf_set(const uint32_t                        instance,
                                             const uint32_t                        peer_id,
                                             const vtss_appl_mep_peer_conf_t  *const conf);

/**
 * \brief Instance Peer MEP configuration GET. (vtss_appl_mep_peer_conf_t)
 *
 * This get the configuration of a selected Peer MEP ID
 *
 * \param instance [IN]  Instance number.
 * \param peer_id [IN]   Peer MEP ID.
 * \param conf [OUT]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_instance_peer_conf_get(const uint32_t                  instance,
                                             const uint32_t                  peer_id,
                                             vtss_appl_mep_peer_conf_t  *const conf);


/**
 * \brief Instance peer MEP configuration ADD. (vtss_appl_mep_peer_conf_t)
 *
 * This add to an instance a new Peer MEP with the given ID and configuration. The instance must be enabled.
 * The Peer MEP ID must not already exist.
 *
 * \param instance [IN]  Instance number.
 * \param peer_id [IN]    Peer MEP ID.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_instance_peer_conf_add(const uint32_t                        instance,
                                             const uint32_t                        peer_id,
                                             const vtss_appl_mep_peer_conf_t  *const conf);

/**
 * \brief Instance peer MEP configuration DELETE. (vtss_appl_mep_peer_conf_t)
 *
 * This delete from an instance a existing Peer MEP with the given ID. The instance must be enabled.
 * The Peer MEP ID must exist.
 *
 * \param instance [IN]  Instance number.
 * \param peer_id [IN]    Peer MEP ID.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_instance_peer_conf_delete(const uint32_t  instance,
                                                const uint32_t  peer_id);

/**
 * \brief Instance Peer MEP ID iterator.
 *
 * This returns the next configured Peer MEP ID for any enabled instance. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance'.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 * The search for the next Peer MEP ID will start with the input 'peer_id' + 1.
 * If the input 'peer_id' pointer is NULL, the search starts with the lowest possible peer MEP ID.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  instance where next Peer MEP ID was found.
 * \param peer_id [IN]         Start Peer MEP ID.
 * \param next_peer_id [OUT]   Next Peer MEP ID.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_instance_peer_conf_iter(const uint32_t    *const instance,
                                              uint32_t          *const next_instance,
                                              const uint32_t    *const peer_id,
                                              uint32_t          *const next_peer_id);
/**
 * \brief Syslog configuration SET. (vtss_appl_mep_syslog_conf_t)
 *
 * This enables or disables an instance to generate syslog entries. The instance must be enabled.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 */
mesa_rc vtss_appl_mep_syslog_conf_set(const uint32_t                            instance,
                                      const vtss_appl_mep_syslog_conf_t    *const conf);

/**
 * \brief Syslog configuration GET. (vtss_appl_mep_syslog_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_syslog_conf_get(const uint32_t                     instance,
                                      vtss_appl_mep_syslog_conf_t   *const conf);

/**
 * \brief Syslog iterator.
 *
 * This returns the next instance number with enabled Syslog. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 *
 * \param instance [IN]        Instance number.
 * \param next_instance [OUT]  Next instance with enabled PM.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_syslog_conf_iter(const uint32_t    *const instance,
                                       uint32_t          *const next_instance);

/**
 * \brief instance status GET. (vtss_appl_mep_state_t)
 *
 * \param instance [IN] Instance number.
 * \param status [OUT]  Status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_instance_status_get(const uint32_t              instance,
                                          vtss_appl_mep_state_t  *const status);

/**
 * \brief Instance iterator.
 *
 * This returns the next enabled instance number in the MEP status object. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance      [IN]   Instance number.
 * \param next_instance [OUT]  Next enabled instance.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_instance_status_iter(const uint32_t    *const instance,
                                           uint32_t          *const next_instance);

/**
 * \brief Instance Peer MEP status GET. (vtss_appl_mep_peer_state_t)
 *
 * This get the status of a selected Peer MEP ID
 *
 * \param instance [IN]  Instance number.
 * \param peer_id [IN]    Peer MEP ID.
 * \param status [OUT]   status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_instance_peer_status_get(const uint32_t                   instance,
                                               const uint32_t                   peer_id,
                                               vtss_appl_mep_peer_state_t  *const status);

/**
 * \brief Instance Peer MEP ID iterator.
 *
 * This returns the next configured Peer MEP ID for any enabled instance in the peer MEP status object. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance'.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 * The search for the next Peer MEP ID will start with the input 'peer_id' + 1.
 * If the input 'peer_id' pointer is NULL, the search starts with the lowest possible peer MEP ID.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  instance where next Peer MEP ID was found.
 * \param peer_id [IN]         Start Peer MEP ID.
 * \param next_peer_id [OUT]   Next Peer MEP ID.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_instance_peer_status_iter(const uint32_t  *const instance,
                                                uint32_t        *const next_instance,
                                                const uint32_t  *const peer_id,
                                                uint32_t        *const next_peer_id);

/**
 * \brief CC status GET. (vtss_appl_mep_cc_state_t)
 *
 * \param instance [IN]  Instance number.
 * \param status [OUT]   CC status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_cc_status_get(const uint32_t                  instance,
                                    vtss_appl_mep_cc_state_t   *const status);

/**
 * \brief Peer MEP CC status GET. (vtss_appl_mep_peer_cc_state_t)
 *
 * This get the CC status of a selected Peer MEP ID
 *
 * \param instance [IN]  Instance number.
 * \param peer_id [IN]   Peer MEP ID.
 * \param status [OUT]   status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_cc_peer_status_get(const uint32_t                      instance,
                                         const uint32_t                      peer_id,
                                         vtss_appl_mep_peer_cc_state_t  *const status);

/**
 * \brief Performance Monitoring configuration SET. (vtss_appl_mep_pm_conf_t)
 *
 * This enables or disables an instance to contribute to a Performance Monitoring session. The instance must be enabled.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 */
mesa_rc vtss_appl_mep_pm_conf_set(const uint32_t                         instance,
                                  const vtss_appl_mep_pm_conf_t    *const conf);

/**
 * \brief Performance Monitoring configuration GET. (vtss_appl_mep_pm_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_pm_conf_get(const uint32_t                  instance,
                                  vtss_appl_mep_pm_conf_t   *const conf);

/**
 * \brief Performance Monitoring iterator.
 *
 * This returns the next instance number with enabled PM. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 *
 * \param instance [IN]        Instance number.
 * \param next_instance [OUT]  Next instance with enabled PM.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_pm_conf_iter(const uint32_t    *const instance,
                                   uint32_t          *const next_instance);

/**
 * \brief Link State Tracking configuration SET. (vtss_appl_mep_lst_conf_t)
 *
 * This enables or disables an instance to do Link State Tracking. The instance must be enabled.
 * When LST is enabled in an instance, Local SF or received 'isDown' in CCM Interface Status TLV, will bring down the residence port
 * Only possible in Up-MEP and when CCM TLV is enabled.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 */
mesa_rc vtss_appl_mep_lst_conf_set(const uint32_t                         instance,
                                   const vtss_appl_mep_lst_conf_t    *const conf);

/**
 * \brief Link State Tracking configuration GET. (vtss_appl_mep_lst_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_lst_conf_get(const uint32_t                  instance,
                                  vtss_appl_mep_lst_conf_t   *const conf);

/**
 * \brief Link State Tracking iterator.
 *
 * This returns the next instance number with enabled LST. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 *
 * \param instance [IN]        Instance number.
 * \param next_instance [OUT]  Next instance with enabled LST.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lst_conf_iter(const uint32_t    *const instance,
                                    uint32_t          *const next_instance);

/**
 * \brief Continuity Check configuration SET. (vtss_appl_mep_cc_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * This enables or disables a Continuity Check session. The instance must be enabled.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MEP_RC_PEER_CNT
 *          VTSS_APPL_MEP_RC_PEER_MAC
 *          VTSS_APPL_MEP_RC_INVALID_COS_ID
 */
mesa_rc vtss_appl_mep_cc_conf_set(const uint32_t                         instance,
                                  const vtss_appl_mep_cc_conf_t    *const conf);

/**
 * \brief Continuity Check configuration GET. (vtss_appl_mep_cc_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_cc_conf_get(const uint32_t                 instance,
                                  vtss_appl_mep_cc_conf_t   *const conf);

/**
 * \brief Instance Continuity Check configuration ADD. (vtss_appl_mep_cc_conf_t)
 *
 * This add to an instance an enabled CC configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_cc_conf_add(const uint32_t                       instance,
                                  const vtss_appl_mep_cc_conf_t  *const conf);

/**
 * \brief instance Continuity Check configuration DELETE. (vtss_appl_mep_cc_conf_t)
 *
 * This delete from an instance an enabled CC. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_cc_conf_delete(const uint32_t  instance);

/**
 * \brief instance Continuity Check configuration iterator.
 *
 * This returns the next enabled instance with enabled CC. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next instance with enabled CC.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_cc_conf_iter(const uint32_t    *const instance,
                                   uint32_t          *const next_instance);

/**
 * \brief Loss Measurement configuration SET. (vtss_appl_mep_lm_conf_t)
 *
 * This enables or disables a Loss Measurement session. The instance must be enabled.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MEP_RC_PEER_CNT
 *          VTSS_APPL_MEP_RC_PEER_MAC
 *          VTSS_APPL_MEP_RC_INVALID_COS_ID
 */
mesa_rc vtss_appl_mep_lm_conf_set(const uint32_t                       instance,
                                  const vtss_appl_mep_lm_conf_t  *const conf);

/**
 * \brief Loss Measurement configuration GET. (vtss_appl_mep_lm_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_lm_conf_get(const uint32_t                 instance,
                                  vtss_appl_mep_lm_conf_t  *const conf);

/**
 * \brief Instance Loss Measurement configuration ADD. (vtss_appl_mep_lm_conf_t)
 *
 * This add to an instance an enabled LM configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lm_conf_add(const uint32_t                       instance,
                                  const vtss_appl_mep_lm_conf_t  *const conf);

/**
 * \brief instance Loss Measurement configuration DELETE. (vtss_appl_mep_lm_conf_t)
 *
 * This delete from an instance an enabled LM. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lm_conf_delete(const uint32_t  instance);

/**
 * \brief instance Loss Measurement configuration iterator.
 *
 * This returns the next enabled instance with enabled LM. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next instance with enabled LM.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lm_conf_iter(const uint32_t    *const instance,
                                   uint32_t          *const next_instance);

/**
 * \brief Loss Measurement status GET. (vtss_appl_mep_lm_state_t)
 *
 * This is returning LM status from Service Frame based LM when only one peer MEP is configured. This is for backward compatibility reasons.
 * The vtss_appl_mep_instance_peer_lm_status_get() function can return LM status for a specific peer MEP.
 *
 * \param instance [IN] Instance number.
 * \param status [OUT]  Status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_lm_status_get(const uint32_t                   instance,
                                    vtss_appl_mep_lm_state_t    *const status);

/**
 * \brief Instance Peer MEP LM status GET. (vtss_appl_mep_lm_state_t)
 *
 * This get the LM status of a selected Peer MEP ID
 *
 * \param instance [IN]  Instance number.
 * \param peer_id [IN]   Peer MEP ID.
 * \param status [OUT]   status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_instance_peer_lm_status_get(const uint32_t            instance,
                                                  const uint32_t            peer_id,
                                                  vtss_appl_mep_lm_state_t  *const status);

/**
 * \brief Loss Measurement status CLEAR.
 *
 * \param instance [IN] Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 */
mesa_rc vtss_appl_mep_lm_status_clear(const uint32_t    instance);

/**
 * \brief Loss Measurement status CLEAR with direction.
 *
 * \param instance [IN] Instance number.
 * \param direction [IN] Counter direction to clear.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 */
mesa_rc vtss_appl_mep_lm_status_clear_dir(const uint32_t          instance,
                                          vtss_appl_mep_clear_dir direction);

/**
 * \brief Loss Measurement control GET.
 *
 * \param instance [IN] Instance number.
 * \param control [OUT] Control.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 */
mesa_rc vtss_appl_mep_lm_control_get(const uint32_t                   instance,
                                     vtss_appl_mep_lm_control_t  *const control);

/**
 * \brief Loss Measurement control SET.
 *
 * \param instance [IN] Instance number.
 * \param control [IN]  Control.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 */
mesa_rc vtss_appl_mep_lm_control_set(const uint32_t                         instance,
                                     const vtss_appl_mep_lm_control_t  *const control);

/**
 * \brief Loss Measurement Notification status GET. (vtss_appl_mep_lm_notif_state_t)
 *
 * \param instance [IN] Instance number.
 * \param status [OUT]  Status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_lm_notif_status_get(const uint32_t                       instance,
                                          vtss_appl_mep_lm_notif_state_t  *const status);

/**
 * \brief Loss Measurement Notification iterator.
 *
 * This returns the next instance number of Loss Measurement Notification status. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 *
 * \param instance [IN]        Instance number.
 * \param next_instance [OUT]  Next instance with LM Notification.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lm_notif_status_iter(const uint32_t    *const instance,
                                           uint32_t          *const next_instance);

/**
 * \brief Loss Measurement Availability configuration SET. (vtss_appl_mep_lm_avail_conf_t)
 *
 * This configures Loss Measurement Availability for a session. The instance must be enabled.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mep_lm_avail_conf_set(const uint32_t                            instance,
                                        const vtss_appl_mep_lm_avail_conf_t  *const conf);

/**
 * \brief Loss Measurement Availability configuration GET. (vtss_appl_mep_lm_avail_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_lm_avail_conf_get(const uint32_t                      instance,
                                        vtss_appl_mep_lm_avail_conf_t  *const conf);

/**
 * \brief Instance Loss Measurement Availability configuration ADD. (vtss_appl_mep_lm_avail_conf_t)
 *
 * This add to an instance an enabled LM Availability configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lm_avail_conf_add(const uint32_t                            instance,
                                        const vtss_appl_mep_lm_avail_conf_t  *const conf);

/**
 * \brief instance Loss Measurement Availability configuration DELETE. (vtss_appl_mep_lm_avail_conf_t)
 *
 * This deletes from an instance an enabled LM Availability configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lm_avail_conf_delete(const uint32_t  instance);

/**
 * \brief instance Loss Measurement Availability configuration iterator.
 *
 * This returns the next enabled instance with enabled LM Availability. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next instance with enabled LM.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lm_avail_conf_iter(const uint32_t    *const instance,
                                         uint32_t          *const next_instance);

/**
 * \brief Loss Measurement Availability status GET. (vtss_appl_mep_lm_avail_state_t)
 *
 * \param instance [IN] Instance number.
 * \param peer_instance [IN] Peer instance number.
 * \param status [OUT]  Status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_lm_avail_status_get(const uint32_t                 instance,
                                          const uint32_t                 peer_instance,
                                          vtss_appl_mep_lm_avail_state_t *const status);

/**
 * \brief Loss Measurement High Loss Interval configuration SET. (vtss_appl_mep_lm_hli_conf_t)
 *
 * This configures Loss Measurement High Loss Interval for a session. The instance must be enabled.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mep_lm_hli_conf_set(const uint32_t                            instance,
                                      const vtss_appl_mep_lm_hli_conf_t  *const conf);

/**
 * \brief Loss Measurement High Loss Interval configuration GET. (vtss_appl_mep_lm_hli_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_lm_hli_conf_get(const uint32_t                      instance,
                                      vtss_appl_mep_lm_hli_conf_t  *const conf);

/**
 * \brief Instance Loss Measurement High Loss Interval configuration ADD. (vtss_appl_mep_lm_hli_conf_t)
 *
 * This add to an instance an enabled LM HLI configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lm_hli_conf_add(const uint32_t                            instance,
                                      const vtss_appl_mep_lm_hli_conf_t  *const conf);

/**
 * \brief instance Loss Measurement High Loss Interval configuration DELETE. (vtss_appl_mep_lm_hli_conf_t)
 *
 * This deletes from an instance an enabled LM HLI configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lm_hli_conf_delete(const uint32_t  instance);

/**
 * \brief instance Loss Measurement High Loss Interval configuration iterator.
 *
 * This returns the next enabled instance with enabled LM HLI. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next instance with enabled LM.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lm_hli_conf_iter(const uint32_t    *const instance,
                                       uint32_t          *const next_instance);

/**
 * \brief Loss Measurement High Loss Interval status GET. (vtss_appl_mep_lm_hli_state_t)
 *
 * \param instance [IN] Instance number.
 * \param peer_id [IN]  Peer MEP ID.
 * \param status [OUT]  Status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_lm_hli_status_get(const uint32_t               instance,
                                        const uint32_t               peer_id,
                                        vtss_appl_mep_lm_hli_state_t *const status);

/**
 * \brief Loss Measurement High Loss Interval status iterator.
 *
 * This returns the next instance number of Loss Measurement High Loss Interval status. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 *
 * \param instance [IN]        Instance number.
 * \param next_instance [OUT]  Next instance with LM HLI status.
 * \param peer [IN]            Peer MEP ID.
 * \param next_peer [OUT]      Next peer MEP ID.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lm_hli_status_iter(const uint32_t    *const instance,
                                         uint32_t          *const next_instance,
                                         const uint32_t    *const peer,
                                         uint32_t          *const next_peer);

/**
 * \brief Loss Measurement Signal Degrade configuration SET. (vtss_appl_mep_lm_sdeg_conf_t)
 *
 * This configures Loss Measurement Signal Degrade for a session. The instance must be enabled.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mep_lm_sdeg_conf_set(const uint32_t                           instance,
                                       const vtss_appl_mep_lm_sdeg_conf_t  *const conf);

/**
 * \brief Loss Measurement Signal Degrade configuration GET. (vtss_appl_mep_lm_sdeg_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_lm_sdeg_conf_get(const uint32_t                     instance,
                                       vtss_appl_mep_lm_sdeg_conf_t  *const conf);

/**
 * \brief Instance Loss Measurement Signal Degrade configuration ADD. (vtss_appl_mep_lm_sdeg_conf_t)
 *
 * This add to an instance an enabled LM SDEG configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lm_sdeg_conf_add(const uint32_t                           instance,
                                       const vtss_appl_mep_lm_sdeg_conf_t  *const conf);

/**
 * \brief instance Loss Measurement Signal Degrade configuration DELETE. (vtss_appl_mep_lm_sdeg_conf_t)
 *
 * This deletes from an instance an enabled LM SDEG configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lm_sdeg_conf_delete(const uint32_t  instance);

/**
 * \brief instance Loss Measurement Signal Degrade configuration iterator.
 *
 * This returns the next enabled instance with enabled LM SDEG. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next instance with enabled LM.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lm_sdeg_conf_iter(const uint32_t    *const instance,
                                        uint32_t          *const next_instance);

/**
 * \brief Delay Measurement configuration SET. (vtss_appl_mep_dm_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * This enables or disables a Delay Measurement session. The instance must be enabled.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MEP_RC_PEER_CNT
 *          VTSS_APPL_MEP_RC_PEER_ID
 *          VTSS_APPL_MEP_RC_PEER_MAC
 *          VTSS_APPL_MEP_RC_PROP_SUPPORT
 *          VTSS_APPL_MEP_RC_INVALID_COS_ID
 */
mesa_rc vtss_appl_mep_dm_conf_set(const uint32_t                       instance,
                                  const vtss_appl_mep_dm_conf_t  *const conf);

/**
 * \brief Delay Measurement configuration GET. (vtss_appl_mep_dm_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_dm_conf_get(const uint32_t                 instance,
                                  vtss_appl_mep_dm_conf_t  *const conf);

/**
 * \brief Instance Delay Measurement configuration ADD. (vtss_appl_mep_dm_conf_t)
 *
 * This add to an instance an enabled LM configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_dm_conf_add(const uint32_t                       instance,
                                  const vtss_appl_mep_dm_conf_t  *const conf);

/**
 * \brief instance Delay Measurement configuration DELETE. (vtss_appl_mep_dm_conf_t)
 *
 * This delete from an instance an enabled DM. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_dm_conf_delete(const uint32_t  instance);

/**
 * \brief instance Delay Measurement configuration iterator.
 *
 * This returns the next enabled instance with enabled DM. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next instance with enabled DM.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_dm_conf_iter(const uint32_t    *const instance,
                                   uint32_t          *const next_instance);

/**
 * \brief Delay Measurement status GET. (vtss_appl_mep_dm_state_t)
 *
 * \param instance [IN]                 Instance number.
 * \param tw_status [OUT]               Two-way status.
 * \param ow_status_far_to_near [OUT]   One-way far to near status.
 * \param ow_status_near_to_far [OUT]   One-way near to far status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_dm_status_get(const uint32_t                   instance,
                                    vtss_appl_mep_dm_state_t    *const tw_status,
                                    vtss_appl_mep_dm_state_t    *const ow_status_far_to_near,
                                    vtss_appl_mep_dm_state_t    *const ow_status_near_to_far);

/**
 * \brief Delay Measurement FD bin status GET. (vtss_appl_mep_dm_fd_bin_state_t)
 *
 * Note: This function is deprecated. Use the vtss_appl_mep_dm_fd_bins_get instead.
 *
 * \param instance [IN]  Instance ID.
 * \param index          FD Bin Table index
 * \param status [OUT]   Instance value.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_INVALID_BIN_INDEX
 */
mesa_rc vtss_appl_mep_dm_fd_bin_status_get(const uint32_t                          instance,
                                           const uint32_t                          index,
                                           vtss_appl_mep_dm_fd_bin_state_t    *const status);

/**
 * \brief Delay Measurement FD bin status GET. (vtss_appl_mep_dm_fd_bin_state_t) iterator.
 *
 * Note: This function is deprecated. Use the vtss_appl_mep_dm_fd_bins_iter instead.
 *
 * \param instance [IN]       Instance ID.
 * \param next_instance [OUT] Next instance ID.
 * \param index [IN]          FD Bin Table index
 * \param next_index [OUT]    Next FD Bin Table index.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_dm_fd_bin_status_iter(const uint32_t  *const instance,
                                            uint32_t        *const next_instance,
                                            const uint32_t  *const index,
                                            uint32_t        *const next_index);
 /**
 * \brief Delay Measurement IFDV bin status GET. (vtss_appl_mep_dm_ifdv_bin_state_t)
  *
  * Note: This function is deprecated. Use the vtss_appl_mep_dm_ifdv_bins_get instead.
 *
 * \param instance [IN]  Instance ID.
 * \param index [IN]     IFDV Bin Table index
 * \param status [OUT]   Instance value.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_INVALID_BIN_INDEX
 */
mesa_rc vtss_appl_mep_dm_ifdv_bin_status_get(const uint32_t                          instance,
                                             const uint32_t                          index,
                                             vtss_appl_mep_dm_ifdv_bin_state_t  *const status);

/**
 * \brief Delay Measurement IFDV bin status GET. (vtss_appl_mep_dm_ifdv_bin_state_t) iterator.
 *
 * Note: This function is deprecated. Use the vtss_appl_mep_dm_ifdv_bins_iter instead.
 *
 * \param instance [IN]       Instance ID.
 * \param next_instance [OUT] Next instance ID.
 * \param index [IN]          IFDV Bin Table index
 * \param next_index [OUT]    Next IFDV Bin Table index.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_dm_ifdv_bin_status_iter(const uint32_t  *const instance,
                                              uint32_t        *const next_instance,
                                              const uint32_t  *const index,
                                              uint32_t        *const next_index);

/**
 * \brief Delay Measurement Frame Delay bin GET. (vtss_appl_mep_dm_bin_value_t)
 *
 * \param instance [IN]     Instance number.
 * \param bin_number [IN]   bin number (1 .. capabilities.dm_bin_max).
 * \param tw_bin [OUT]      Corresponding two-way bin value (deprecated - contains same value as two_way value in bin_value structure).
 * \param bin_value [OUT]   Corresponding bin value.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_dm_fd_bins_get(const uint32_t                     instance,
                                     const uint32_t                     bin_number,
                                     vtss_appl_mep_dm_fd_bin_state_t    *const tw_bin,
                                     vtss_appl_mep_dm_bin_value_t       *const bin_value);

/**
 * \brief MEP DM Frame Delay Bin number iterator.
 *
 * This returns the next MEP DM Frame Delay bin number for any enabled instance.
 * When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance'.
 * If the input 'instance' pointer is NULL, the search start with the lowest possible instance number.
 * The search for the next bin number will start with the input 'bin_number' + 1.
 * If the input 'bin_number' pointer is NULL, the search start with the lowest possible bin_number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next enabled instance.
 * \param bin_number [IN]      Start bin number.
 * \param next_bin_number [OUT] Next bin number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_dm_fd_bins_iter(const uint32_t    *const instance,
                                      uint32_t          *const next_instance,
                                      const uint32_t    *const bin_number,
                                      uint32_t          *const next_bin_number);

/**
 * \brief Delay Measurement Inter Frame Delay Variation bin GET. (vtss_appl_mep_dm_bin_value_t)
 *
 * \param instance [IN]     Instance number.
 * \param bin_number [IN]   bin number (1 .. capabilities.dm_bin_max).
 * \param tw_bin [OUT]      Corresponding two-way bin value (deprecated - contains same value as two_way value in bin_value structure).
 * \param bin_value [OUT]   Corresponding bin value.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_dm_ifdv_bins_get(const uint32_t                       instance,
                                       const uint32_t                       bin_number,
                                       vtss_appl_mep_dm_ifdv_bin_state_t    *const tw_bin,
                                       vtss_appl_mep_dm_bin_value_t         *const bin_value);

/**
 * \brief MEP DM Inter Frame Delay Variation Bin number iterator.
 *
 * This returns the next MEP DM Inter Frame Delay Variation bin number for any enabled instance.
 * When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance'.
 * If the input 'instance' pointer is NULL, the search start with the lowest possible instance number.
 * The search for the next bin number will start with the input 'bin_number' + 1.
 * If the input 'bin_number' pointer is NULL, the search start with the lowest possible bin_number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next enabled instance.
 * \param bin_number [IN]      Start bin number.
 * \param next_bin_number [OUT] Next bin number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_dm_ifdv_bins_iter(const uint32_t    *const instance,
                                        uint32_t          *const next_instance,
                                        const uint32_t    *const bin_number,
                                        uint32_t          *const next_bin_number);

/**
 * \brief Delay Measurement Two Way status GET. (vtss_appl_mep_dm_state_t)
 *
 * \param instance [IN]  Instance number.
 * \param status [OUT]   Two-way status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_dm_tw_status_get(const uint32_t                   instance,
                                       vtss_appl_mep_dm_state_t    *const status);

/**
 * \brief Delay Measurement One Way Near to Far status GET. (vtss_appl_mep_dm_state_t)
 *
 * \param instance [IN]  Instance number.
 * \param status [OUT]   Two-way status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_dm_ownf_status_get(const uint32_t                   instance,
                                         vtss_appl_mep_dm_state_t    *const status);

/**
 * \brief Delay Measurement One Way Far to Near status GET. (vtss_appl_mep_dm_state_t)
 *
 * \param instance [IN]  Instance number.
 * \param status [OUT]   Two-way status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_dm_owfn_status_get(const uint32_t                   instance,
                                         vtss_appl_mep_dm_state_t    *const status);

/**
 * \brief Delay Measurement status CLEAR.
 *
 * \param instance [IN] Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 */
mesa_rc vtss_appl_mep_dm_status_clear(const uint32_t    instance);

/**
 * \brief Delay Measurement control GET.
 *
 * \param instance [IN] Instance number.
 * \param control [OUT] Control.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 */
mesa_rc vtss_appl_mep_dm_control_get(const uint32_t                   instance,
                                     vtss_appl_mep_dm_control_t  *const control);

/**
 * \brief Delay Measurement control SET.
 *
 * \param instance [IN] Instance number.
 * \param control [IN]  Control.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 */
mesa_rc vtss_appl_mep_dm_control_set(const uint32_t                         instance,
                                     const vtss_appl_mep_dm_control_t  *const control);






/**
 * \brief Delay Measurement common configuration GET. (vtss_appl_mep_dm_common_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_dm_common_conf_get(const uint32_t                       instance,
                                         vtss_appl_mep_dm_common_conf_t  *const conf);

/**
 * \brief Delay Measurement common configuration SET. (vtss_appl_mep_dm_common_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_dm_common_conf_set(const uint32_t                             instance,
                                         const vtss_appl_mep_dm_common_conf_t  *const conf);

/**
 * \brief Delay Measurement transmission configuration GET. (vtss_appl_mep_dm_prio_conf_t)
 *
 * This will get the Delay Measurement configuration per Priority.
 *
 * \param instance [IN] Instance number.
 * \param prio [IN]     Priority. Range 0 to 7
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_dm_prio_conf_get(const uint32_t                    instance,
                                       const uint32_t                    prio,
                                       vtss_appl_mep_dm_prio_conf_t *const conf);

/**
 * \brief Delay Measurement transmission configuration SET. (vtss_appl_mep_dm_prio_conf_t)
 *
 * This will set the Delay Measurement configuration per Priority.
 *
 * \param instance [IN] Instance number.
 * \param prio [IN]     Priority. Range 0 to 7
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_dm_prio_conf_set(const uint32_t                          instance,
                                       const uint32_t                          prio,
                                       const vtss_appl_mep_dm_prio_conf_t *const conf);

/**
 * \brief Delay Measurement status per priority GET. (vtss_appl_mep_dm_state_t)
 *
 * \param instance [IN]                 Instance number.
 * \param prio [IN]                     Priority. Range 0 to 7
 * \param tw_status [OUT]               Two-way status.
 * \param ow_status_far_to_near [OUT]   One-way far to near status.
 * \param ow_status_near_to_far [OUT]   One-way near to far status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_dm_prio_status_get(const uint32_t                   instance,
                                         const uint32_t                   prio,
                                         vtss_appl_mep_dm_state_t    *const tw_status,
                                         vtss_appl_mep_dm_state_t    *const ow_status_far_to_near,
                                         vtss_appl_mep_dm_state_t    *const ow_status_near_to_far);







/**
 * \brief Automatic Protection Switching configuration SET. (vtss_appl_mep_aps_conf_t)
 *
 * This enables or disables a Automatic Protection Switching session. The instance must be enabled.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 *          VTSS_APPL_MEP_RC_APS_DOMAIN
 *          VTSS_APPL_MEP_RC_INVALID_VID
 *          VTSS_APPL_MEP_RC_APS_UP
 */
mesa_rc vtss_appl_mep_aps_conf_set(const uint32_t                        instance,
                                   const vtss_appl_mep_aps_conf_t  *const conf);

/**
 * \brief Automatic Protection Switching configuration GET. (vtss_appl_mep_aps_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_aps_conf_get(const uint32_t                  instance,
                                   vtss_appl_mep_aps_conf_t  *const conf);

/**
 * \brief Instance Automatic Protection Switching configuration ADD. (vtss_appl_mep_aps_conf_t)
 *
 * This add to an instance an enabled APS configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_aps_conf_add(const uint32_t                       instance,
                                   const vtss_appl_mep_aps_conf_t  *const conf);


/**
 * \brief instance Automatic Protection Switching configuration DELETE. (vtss_appl_mep_aps_conf_t)
 *
 * This delete from an instance an enabled APS. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_aps_conf_delete(const uint32_t  instance);

/**
 * \brief instance Automatic Protection Switching configuration iterator.
 *
 * This returns the next enabled instance with enabled APS. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next instance with enabled APS.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_aps_conf_iter(const uint32_t    *const instance,
                                    uint32_t          *const next_instance);

/**
 * \brief Link Trace configuration SET. (vtss_appl_mep_lt_conf_t)
 *
 * This enables or disables a Link Trace session. The instance must be enabled.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 *          VTSS_APPL_MEP_RC_PEER_CNT
 *          VTSS_APPL_MEP_RC_PEER_ID
 *          VTSS_APPL_MEP_RC_PEER_MAC
 *          VTSS_APPL_MEP_RC_INVALID_COS_ID
 */
mesa_rc vtss_appl_mep_lt_conf_set(const uint32_t                       instance,
                                  const vtss_appl_mep_lt_conf_t  *const conf);

/**
 * \brief Link Trace configuration GET. (vtss_appl_mep_lt_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_lt_conf_get(const uint32_t                 instance,
                                  vtss_appl_mep_lt_conf_t  *const conf);

/**
 * \brief Instance Link Trace configuration ADD. (vtss_appl_mep_lt_conf_t)
 *
 * This add to an instance an enabled LT configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lt_conf_add(const uint32_t                      instance,
                                  const vtss_appl_mep_lt_conf_t  *const conf);

/**
 * \brief instance Link Trace configuration DELETE. (vtss_appl_mep_lt_conf_t)
 *
 * This delete from an instance an enabled LT. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lt_conf_delete(const uint32_t  instance);

/**
 * \brief instance Link Trace configuration iterator.
 *
 * This returns the next enabled instance with enabled LT. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next instance with enabled LT.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lt_conf_iter(const uint32_t    *const instance,
                                   uint32_t          *const next_instance);

/**
 * \brief Link Trace status GET. (vtss_appl_mep_lt_state_t)
 *
 * \param instance [IN] Instance number.
 * \param status [OUT]  Status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_lt_status_get(const uint32_t                  instance,
                                    vtss_appl_mep_lt_state_t   *const status);

/**
 * \brief Link Trace Transaction status GET. (vtss_appl_mep_lt_transaction_t)
 *
 * Returns the last LT transaction.
 *
 * \param instance [IN] Instance number.
 * \param status [OUT]  Status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_lt_transaction_status_get(const uint32_t                        instance,
                                                vtss_appl_mep_lt_transaction_t   *const status);

/**
 * \brief Link Trace Transaction iterator.
 *
 * This returns the next enabled instance with any LT transactions. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance'.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next instance with LT transactions.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lt_transaction_status_iter(const uint32_t    *const instance,
                                                 uint32_t          *const next_instance);

/**
 * \brief Link Trace Reply status GET. (vtss_appl_mep_lt_reply_t)
 *
 * This get the status of a selected LT reply id
 *
 * \param instance [IN]  Instance number.
 * \param reply_id [IN]  LT Reply id.
 * \param status [OUT]   status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lt_reply_status_get(const uint32_t                 instance,
                                          const uint32_t                 reply_id,
                                          vtss_appl_mep_lt_reply_t  *const status);

/**
 * \brief Link Trace reply status iterator.
 *
 * This returns the next Link Trace Reply id for any enabled instance. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance'.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 * The search for the next LT Reply id will start with the input 'reply_id' + 1.
 * If the input 'reply_id' pointer is NULL, the search starts with the lowest possible LT Reply id.
 *
 * \param instance [IN]         Start instance number.
 * \param next_instance [OUT]   instance where next LT Reply id was found.
 * \param reply_id [IN]         Start LT Reply id.
 * \param next_reply_id [OUT]   Next LT Reply id.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lt_reply_status_iter(const uint32_t    *const instance,
                                           uint32_t          *const next_instance,
                                           const uint32_t    *const reply_id,
                                           uint32_t          *const next_reply_id);

/**
 * \brief Loop Back configuration SET. (vtss_appl_mep_lb_conf_t)
 *
 * This enables or disables a Loop Back session. The instance must be enabled.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MEP_RC_PEER_CNT
 *          VTSS_APPL_MEP_RC_PEER_ID
 *          VTSS_APPL_MEP_RC_PEER_MAC
 *          VTSS_APPL_MEP_RC_INVALID_COS_ID
 */
mesa_rc vtss_appl_mep_lb_conf_set(const uint32_t                       instance,
                                  const vtss_appl_mep_lb_conf_t  *const conf);

/**
 * \brief Loop Back configuration GET. (vtss_appl_mep_lb_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_lb_conf_get(const uint32_t                 instance,
                                  vtss_appl_mep_lb_conf_t  *const conf);


/**
 * \brief Instance Loop Back configuration ADD. (vtss_appl_mep_lb_conf_t)
 *
 * This add to an instance an enabled LB configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lb_conf_add(const uint32_t                      instance,
                                  const vtss_appl_mep_lb_conf_t  *const conf);

/**
 * \brief instance Loop Back configuration DELETE. (vtss_appl_mep_lb_conf_t)
 *
 * This delete from an instance an enabled LB. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lb_conf_delete(const uint32_t  instance);

/**
 * \brief instance Loop Back configuration iterator.
 *
 * This returns the next enabled instance with enabled LB. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next instance with enabled LB.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lb_conf_iter(const uint32_t    *const instance,
                                   uint32_t          *const next_instance);

/**
 * \brief Loop Back status GET. (vtss_appl_mep_lb_state_t)
 *
 * \param instance [IN] Instance number.
 * \param status [OUT]  Status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_lb_status_get(const uint32_t             instance,
                                    vtss_appl_mep_lb_state_t   *const status);

/**
 * \brief Test Signal status CLEAR.
 *
 * \param instance [IN] Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 */
mesa_rc vtss_appl_mep_lb_status_clear(const uint32_t    instance);

/**
 * \brief Loop Back Reply status GET. (vtss_appl_mep_lb_reply_t)
 *
 * This get the status of a selected LB reply id
 *
 * \param instance [IN]  Instance number.
 * \param reply_id [IN]  LB Reply id.
 * \param status [OUT]   status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lb_reply_status_get(const uint32_t            instance,
                                          const uint32_t            reply_id,
                                          vtss_appl_mep_lb_reply_t  *const status);

/**
 * \brief Loop Back reply status iterator.
 *
 * This returns the next Loop Back Reply id for any enabled instance. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance'.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 * The search for the next LB Reply id will start with the input 'reply_id' + 1.
 * If the input 'reply_id' pointer is NULL, the search starts with the lowest possible LB Reply id.
 *
 * \param instance [IN]         Start instance number.
 * \param next_instance [OUT]   instance where next LB Reply id was found.
 * \param reply_id [IN]         Start LB Reply id.
 * \param next_reply_id [OUT]   Next LB Reply id.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lb_reply_status_iter(const uint32_t    *const instance,
                                           uint32_t          *const next_instance,
                                           const uint32_t    *const reply_id,
                                           uint32_t          *const next_reply_id);

/**
 * \brief Loop Back common configuration GET. (vtss_appl_mep_lb_common_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_lb_common_conf_get(const uint32_t                  instance,
                                         vtss_appl_mep_lb_common_conf_t  *const conf);

/**
 * \brief Loop Back common configuration SET. (vtss_appl_mep_lb_common_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_lb_common_conf_set(const uint32_t                        instance,
                                         const vtss_appl_mep_lb_common_conf_t  *const conf);

/**
 * \brief Loop Back transmission configuration GET. (vtss_appl_mep_test_prio_conf_t)
 *
 * This will get the Loop Back TX configuration per Prio and index.
 *
 * \param instance [IN] Instance number.
 * \param prio [IN]     Priority. Range 0 to 7
 * \param test_idx [IN] Per priority Test index. Range 0 to 1. capabilities.test_flow_max
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_lb_prio_conf_get(const uint32_t                  instance,
                                       const uint32_t                  prio,
                                       const uint32_t                  test_idx,
                                       vtss_appl_mep_test_prio_conf_t  *const conf);

/**
 * \brief Loop Back transmission configuration SET. (vtss_appl_mep_test_prio_conf_t)
 *
 * This will set the Loop Back TX configuration per Prio and index.
 *
 * \param instance [IN] Instance number.
 * \param prio [IN]     Priority. Range 0 to 7
 * \param test_idx [IN] Per priority Test index. Range 0 to 1. capabilities.test_flow_max
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_lb_prio_conf_set(const uint32_t                        instance,
                                       const uint32_t                        prio,
                                       const uint32_t                        test_idx,
                                       const vtss_appl_mep_test_prio_conf_t  *const conf);

/**
 * \brief Loop Back status per priority GET. (vtss_appl_mep_lb_state_t)
 *
 * \param instance [IN]   Instance number.
 * \param prio [IN]       Priority. Range 0 to 7
 * \param status [OUT]    status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_lb_prio_status_get(const uint32_t             instance,
                                         const uint32_t             prio,
                                         vtss_appl_mep_lb_state_t   *const status);

/**
 * \brief Alarm Indication Signal configuration SET. (vtss_appl_mep_ais_conf_t)
 *
 * This enables or disables a Alarm Indication Signal session. The instance must be enabled.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mep_ais_conf_set(const uint32_t instance,
                                   const vtss_appl_mep_ais_conf_t *const conf);

/**
 * \brief Alarm Indication Signal configuration GET. (vtss_appl_mep_ais_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_ais_conf_get(const uint32_t instance,
                                   vtss_appl_mep_ais_conf_t *const conf);

/**
 * \brief Alarm Indication Signal configuration ADD. (vtss_appl_mep_ais_conf_t)
 *
 * This add to an instance an enabled AIS configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_ais_conf_add(const uint32_t                       instance,
                                   const vtss_appl_mep_ais_conf_t  *const conf);

/**
 * \brief instance Alarm Indication Signal configuration DELETE. (vtss_appl_mep_ais_conf_t)
 *
 * This delete from an instance an enabled AIS. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_ais_conf_delete(const uint32_t  instance);

/**
 * \brief instance Alarm Indication Signal configuration iterator.
 *
 * This returns the next enabled instance with enabled AIS. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next instance with enabled AIS.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_ais_conf_iter(const uint32_t    *const instance,
                                    uint32_t          *const next_instance);

/**
 * \brief Locked Signal configuration SET. (vtss_appl_mep_lck_conf_t)
 *
 * This enables or disables a Locked Signal session. The instance must be enabled.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mep_lck_conf_set(const uint32_t instance,
                                   const vtss_appl_mep_lck_conf_t *const conf);

/**
 * \brief Locked Signal configuration GET. (vtss_appl_mep_lck_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_lck_conf_get(const uint32_t instance,
                                   vtss_appl_mep_lck_conf_t *const conf);

/**
 * \brief Locked Signal configuration ADD. (vtss_appl_mep_lck_conf_t)
 *
 * This add to an instance an enabled LCK configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lck_conf_add(const uint32_t                       instance,
                                   const vtss_appl_mep_lck_conf_t  *const conf);

/**
 * \brief instance Locked Signal configuration DELETE. (vtss_appl_mep_lck_conf_t)
 *
 * This delete from an instance an enabled LCK. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lck_conf_delete(const uint32_t  instance);

/**
 * \brief instance Locked Signal configuration iterator.
 *
 * This returns the next enabled instance with enabled LCK. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next instance with enabled LCK.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_lck_conf_iter(const uint32_t    *const instance,
                                    uint32_t          *const next_instance);

/**
 * \brief Test Signal configuration SET. (vtss_appl_mep_tst_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * This enables or disables a Test Signal session. The instance must be enabled.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MEP_RC_PEER_ID
 *          VTSS_APPL_MEP_RC_PEER_MAC
 *          VTSS_APPL_MEP_RC_INVALID_COS_ID
 */
mesa_rc vtss_appl_mep_tst_conf_set(const uint32_t instance,
                                   const vtss_appl_mep_tst_conf_t *const conf);

/**
 * \brief Test Signal configuration GET. (vtss_appl_mep_tst_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_tst_conf_get(const uint32_t instance,
                                   vtss_appl_mep_tst_conf_t *const conf);


/**
 * \brief Test Signal configuration ADD. (vtss_appl_mep_tst_conf_t)
 *
 * This add to an instance an enabled TST configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_tst_conf_add(const uint32_t                       instance,
                                   const vtss_appl_mep_tst_conf_t  *const conf);

/**
 * \brief instance Test Signal configuration DELETE. (vtss_appl_mep_tst_conf_t)
 *
 * This delete from an instance an enabled TST. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_tst_conf_delete(const uint32_t  instance);

/**
 * \brief instance Test Signal configuration iterator.
 *
 * This returns the next enabled instance with enabled TST. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next instance with enabled TST.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_tst_conf_iter(const uint32_t    *const instance,
                                    uint32_t          *const next_instance);

/**
 * \brief Test Signal status GET. (vtss_appl_mep_tst_state_t)
 *
 * \param instance [IN] Instance number.
 * \param status [OUT]  Status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_tst_status_get(const uint32_t                   instance,
                                     vtss_appl_mep_tst_state_t   *const status);


/**
 * \brief Test Signal status CLEAR.
 *
 * \param instance [IN] Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 */
mesa_rc vtss_appl_mep_tst_status_clear(const uint32_t    instance);

/**
 * \brief Test Signal control GET.
 *
 * \param instance [IN] Instance number.
 * \param control [OUT] Control.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 */
mesa_rc vtss_appl_mep_tst_control_get(const uint32_t                    instance,
                                      vtss_appl_mep_tst_control_t  *const control);

/**
 * \brief Test Signal Measurement control SET.
 *
 * \param instance [IN] Instance number.
 * \param control [IN]  Control.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 */
mesa_rc vtss_appl_mep_tst_control_set(const uint32_t                          instance,
                                      const vtss_appl_mep_tst_control_t  *const control);

/**
 * \brief Test Signal common configuration GET. (vtss_appl_mep_tst_common_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_tst_common_conf_get(const uint32_t                        instance,
                                          vtss_appl_mep_tst_common_conf_t  *const conf);

/**
 * \brief Test Signal common configuration SET. (vtss_appl_mep_tst_common_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_tst_common_conf_set(const uint32_t                              instance,
                                          const vtss_appl_mep_tst_common_conf_t  *const conf);

/**
 * \brief Test Signal transmission configuration GET. (vtss_appl_mep_test_prio_conf_t)
 *
 * This will get the Test TX configuration per Prio and index.
 *
 * \param instance [IN] Instance number.
 * \param prio [IN]     Priority. Range 0 to 7
 * \param test_idx [IN] Per priority Test index. Range 0 to 1. capabilities.test_flow_max
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_tst_prio_conf_get(const uint32_t                      instance,
                                        const uint32_t                      prio,
                                        const uint32_t                      test_idx,
                                        vtss_appl_mep_test_prio_conf_t *const conf);

/**
 * \brief Test Signal transmission configuration SET. (vtss_appl_mep_test_prio_conf_t)
 *
 * This will set the Test TX configuration per Prio and index.
 *
 * \param instance [IN] Instance number.
 * \param prio [IN]     Priority. Range 0 to 7
 * \param test_idx [IN] Per priority Test index. Range 0 to 1. capabilities.test_flow_max
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_tst_prio_conf_set(const uint32_t                            instance,
                                        const uint32_t                            prio,
                                        const uint32_t                            test_idx,
                                        const vtss_appl_mep_test_prio_conf_t *const conf);

/**
 * \brief Test Signal status per priority GET. (vtss_appl_mep_tst_state_t)
 *
 * \param instance [IN]   Instance number.
 * \param prio [IN]       Priority. Range 0 to 7
 * \param status [OUT]    status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 */
mesa_rc vtss_appl_mep_tst_prio_status_get(const uint32_t                   instance,
                                          const uint32_t                   prio,
                                          vtss_appl_mep_tst_state_t   *const status);

/**
 * \brief instance Client Flow configuration SET. (vtss_appl_mep_client_flow_conf_t)
 *
 * This sets the configuration of a selected Client Flow
 *
 * \param instance [IN]   Instance number.
 * \param flow [IN]       Client flow ifindex.
 * \param conf [IN]       configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_client_flow_conf_set(const uint32_t                               instance,
                                           const vtss_ifindex_t                    flow,
                                           const vtss_appl_mep_client_flow_conf_t  *const conf);

/**
 * \brief Instance Client Flow configuration GET. (vtss_appl_mep_client_flow_conf_t)
 *
 * This gets the configuration of a selected Client Flow
 *
 * \param instance [IN]   Instance number.
 * \param flow [IN]       Client flow ifindex.
 * \param conf [OUT]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_client_flow_conf_get(const uint32_t                         instance,
                                           const vtss_ifindex_t              flow,
                                           vtss_appl_mep_client_flow_conf_t  *const conf);


/**
 * \brief Instance Client flow configuration ADD. (vtss_appl_mep_client_flow_conf_t)
 *
 * This adds to an instance a new Client Flow with the given configuration. The instance must be enabled.
 * The flow index must not already exist. Max capabilities.client_flows_max is allowed.
 *
 * \param instance [IN]   Instance number.
 * \param flow [IN]       Client flow ifindex.
 * \param conf [IN]       configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_client_flow_conf_add(const uint32_t                               instance,
                                           const vtss_ifindex_t                    flow,
                                           const vtss_appl_mep_client_flow_conf_t  *const conf);

/**
 * \brief Instance Client Flow configuration DELETE. (vtss_appl_mep_client_flow_conf_t)
 *
 * This deletes from an instance a existing Client Flow with the given index. The instance must be enabled.
 * The flow index must exist.
 *
 * \param instance [IN]  Instance number.
 * \param flow [IN]      Client flow ifindex.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_client_flow_conf_delete(const uint32_t             instance,
                                              const vtss_ifindex_t  flow);

/**
 * \brief Instance Client Flow id iterator.
 *
 * This returns the next configured Client Flow for any enabled instance. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance'.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 * The search for the next Client Flow will start with the input 'flow_index' + 1.
 * If the input 'flow_index' pointer is NULL, the search starts with the lowest possible Client Flow index.
 *
 * \param instance [IN]         Start instance number.
 * \param next_instance [OUT]   instance where next Client Flow was found.
 * \param flow [IN]             Start Client Flow ifindex.
 * \param next_flow [OUT]       Next Client Flow ifindex.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_client_flow_conf_iter(const uint32_t               *const instance,
                                            uint32_t                     *const next_instance,
                                            const vtss_ifindex_t    *const flow,
                                            vtss_ifindex_t          *const next_flow);

/**
 * \brief BFD control GET.
 *
 * \param instance [IN] Instance number.
 * \param control [OUT] Control.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 */
mesa_rc vtss_appl_mep_bfd_control_get(const uint32_t                    instance,
                                      vtss_appl_mep_bfd_control_t  *const control);

/**
 * \brief BFD control SET.
 *
 * \param instance [IN] Instance number.
 * \param control [IN]  Control.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 */
mesa_rc vtss_appl_mep_bfd_control_set(const uint32_t                          instance,
                                      const vtss_appl_mep_bfd_control_t  *const control);

/**
 * \brief ITU G.8113.2 BFD CC/CV status GET. (vtss_appl_mep_g8113_2_bfd_state_t)
 *
 * \param instance [IN]  Instance number.
 * \param status [OUT]   BFD CC/CV status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_g8113_2_bfd_status_get(const uint32_t                         instance,
                                             vtss_appl_mep_g8113_2_bfd_state_t *const status);

/**
 * \brief ITU G.8113.2 BFD CC/CV configuration SET. (vtss_appl_mep_g8113_2_bfd_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * This enables or disables a BFD CC/CV session. The instance must be enabled.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mep_g8113_2_bfd_conf_set(const uint32_t                              instance,
                                           const vtss_appl_mep_g8113_2_bfd_conf_t *const conf);

/**
 * \brief ITU G.8113.2 BFD CC/CV configuration GET. (vtss_appl_mep_g8113_2_bfd_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_g8113_2_bfd_conf_get(const uint32_t                        instance,
                                           vtss_appl_mep_g8113_2_bfd_conf_t *const conf);

/**
 * \brief ITU G.8113.2 Clear statistics counters for BFD CC/CV
 *
 * \param instance [IN] Instance number.
 *
 * This clears the statistics counters for a BFD CC/CV session. The instance must be enabled.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mep_g8113_2_bfd_clear_stats(const uint32_t instance);

/**
 * \brief Instance ITU G.8113.2 BFD CC/CV configuration ADD. (vtss_appl_mep_g8113_2_bfd_conf_t)
 *
 * This adds to an instance an enabled BFD CC/CV configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_g8113_2_bfd_conf_add(const uint32_t                               instance,
                                           const vtss_appl_mep_g8113_2_bfd_conf_t  *const conf);

/**
 * \brief instance ITU G.8113.2 BFD CC/CV configuration DELETE. (vtss_appl_mep_g8113_2_bfd_conf_t)
 *
 * This deletes from an instance an enabled BFD CC/CV. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_g8113_2_bfd_conf_delete(const uint32_t  instance);

/**
 * \brief instance ITU G.8113.2 BFD CC/CV configuration iterator.
 *
 * This returns the next enabled instance with enabled BFD CC/CV. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next instance with enabled CC.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_g8113_2_bfd_conf_iter(const uint32_t    *const instance,
                                            uint32_t          *const next_instance);
/**
 * \brief ITU G.8113.2 BFD CC/CV Authentication Key configuration SET. (vtss_appl_mep_g8113_2_bfd_auth_conf_t)
 *
 * \param key_id [IN]   Key identifier.
 * \param conf [IN]     configuration.
 *
 * This configures a BFD CC/CV Authentication Key.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_set(const uint32_t                                   key_id,
                                                const vtss_appl_mep_g8113_2_bfd_auth_conf_t *const conf);

/**
 * \brief ITU G.8113.2 BFD CC/CV Authentication Key configuration GET. (vtss_appl_mep_g8113_2_bfd_auth_conf_t)
 *
 * \param key_id [IN]   Key identifier.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_get(const uint32_t                             key_id,
                                                vtss_appl_mep_g8113_2_bfd_auth_conf_t *const conf);

/**
 * \brief Instance ITU G.8113.2 BFD CC/CV Authentication Key configuration ADD. (vtss_appl_mep_g8113_2_bfd_auth_conf_t)
 *
 * This adds a BFD CC/CV Authentication Key.
 *
 * \param key_id [IN]   Key identifier.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_add(const uint32_t                                    key_id,
                                                const vtss_appl_mep_g8113_2_bfd_auth_conf_t  *const conf);

/**
 * \brief instance ITU G.8113.2 BFD CC/CV Authentication Key configuration DELETE. (vtss_appl_mep_g8113_2_bfd_auth_conf_t)
 *
 * This deletes a BFD CC/CV Authentication Key.
 *
 * \param key_id [IN]   Key identifier.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_delete(const uint32_t key_id);

/**
 * \brief instance ITU G.8113.2 BFD CC/CV Authentication Key configuration iterator.
 *
 * This returns the next configured BFD CC/CV Authentication Key entry. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search will start with the input 'instance' + 1.
 * If the input 'key_id' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param key_id [IN]        Start Key identifier.
 * \param next_key_id [IN]   Next Key identifier.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_iter(const uint32_t    *const key_id,
                                                 uint32_t          *const next_key_id);

/**
 * \brief ITU G.8113.2 Route Trace configuration SET. (vtss_appl_mep_rt_conf_t)
 *
 * This enables or disables a Route Trace session. The instance must be enabled.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_PARAMETER
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 *          VTSS_APPL_MEP_RC_NOT_ENABLED
 *          VTSS_APPL_MEP_RC_MIP
 *          VTSS_MEP_RC_INVALID_PRIO
 */
mesa_rc vtss_appl_mep_rt_conf_set(const uint32_t                       instance,
                                  const vtss_appl_mep_rt_conf_t  *const conf);

/**
 * \brief ITU G.8113.2 Route Trace configuration GET. (vtss_appl_mep_rt_conf_t)
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_INVALID_INSTANCE
 */
mesa_rc vtss_appl_mep_rt_conf_get(const uint32_t                 instance,
                                  vtss_appl_mep_rt_conf_t  *const conf);

/**
 * \brief Instance ITU G.8113.2 Route Trace configuration ADD. (vtss_appl_mep_rt_conf_t)
 *
 * This add to an instance an enabled RT configuration. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_rt_conf_add(const uint32_t                      instance,
                                  const vtss_appl_mep_rt_conf_t  *const conf);

/**
 * \brief instance ITU G.8113.2 Route Trace configuration DELETE. (vtss_appl_mep_rt_conf_t)
 *
 * This delete from an instance an enabled RT. The instance must be enabled.
 *
 * \param instance [IN]  Instance number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_rt_conf_delete(const uint32_t  instance);

/**
 * \brief instance ITU G.8113.2 Route Trace configuration iterator.
 *
 * This returns the next enabled instance with enabled RT. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance [IN]        Start instance number.
 * \param next_instance [OUT]  Next instance with enabled RT.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_rt_conf_iter(const uint32_t    *const instance,
                                   uint32_t          *const next_instance);

/**
 * \brief ITU G.8113.2 Route Trace status GET. (vtss_appl_mep_rt_status_t)
 *
 * This gets the status of RT
 *
 * \param instance [IN]  Instance number.
 * \param status [OUT]   status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_rt_status_get(const uint32_t                  instance,
                                    vtss_appl_mep_rt_status_t  *const status);

/**
 * \brief ITU G.8113.2 Route Trace Reply status GET. (vtss_appl_mep_rt_reply_t)
 *
 * This gets the status of a selected RT reply
 *
 * \param instance [IN]  Instance number.
 * \param seq_no [IN]    RT reply sequence number.
 * \param status [OUT]   status.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_rt_reply_status_get(const uint32_t                 instance,
                                          const uint32_t                 seq_no,
                                          vtss_appl_mep_rt_reply_t  *const status);

/**
 * \brief ITU G.8113.2 Route Trace reply status iterator.
 *
 * This returns the next Route Trace sequence number for any enabled instance. When the end is reached VTSS_APPL_MEP_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance'.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 * The search for the next RT sequence number will start with the input 'seq_no' + 1.
 * If the input 'seq_no' pointer is NULL, the search starts with the lowest possible RT sequence number.
 *
 * \param instance [IN]         Start instance number.
 * \param next_instance [OUT]   instance where next RT Reply sequence number was found.
 * \param seq_no [IN]           Start RT Reply sequence number.
 * \param next_seq_no [OUT]     Next RT Reply sequence number.
 *
 * \return  VTSS_APPL_MEP_RC_OK
 *          VTSS_APPL_MEP_RC_ERROR
 */
mesa_rc vtss_appl_mep_rt_reply_status_iter(const uint32_t    *const instance,
                                           uint32_t          *const next_instance,
                                           const uint32_t    *const seq_no,
                                           uint32_t          *const next_seq_no);

#if defined(MEP_IMPL_G1)
} /* mep_g1 */
#elif defined(MEP_IMPL_G2)
} /* mep_g2 */
#endif
#endif /* _vtss_appl_mep_H_ */
