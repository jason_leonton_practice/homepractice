/*

 Vitesse TT_LOOP software.

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/


/**
* \file
* \brief Public TT_LOOP (Traffic Testing Loop) API
* \details This header file describes the Traffic Testing Loop control functions and types.
* \details This module is able to create different types of loops, that can be used for traffic testing such as RFC2544 and Y.1564
*/

#ifndef _VTSS_APPL_TT_LOOP_H_
#define _VTSS_APPL_TT_LOOP_H_

#include <vtss/appl/interface.h>
#include <mscc/ethernet/switch/api/types.h>
#include <vtss/appl/module_id.h>

#ifdef __cplusplus
extern "C" {
#endif

#define VTSS_APPL_TT_LOOP_INSTANCE_MAX           100      /**< Max number of tt_loop instance */
#define VTSS_APPL_TT_LOOP_LL_ACT_INSTANCE_MAX     16      /**< Max number of concurrently active latching loopback instances */
#define VTSS_APPL_TT_LOOP_NAME_MAX                33      /**< Max length of instance name including a NULL termination */

/**
 * Definition of error return codes.
 * See also tt_loop_error_txt() in traffic_test/tt_loop/tt_loop.c.
 */
enum {
    VTSS_APPL_TT_LOOP_RC_INVALID_PARAMETER =
    MODULE_ERROR_START(VTSS_MODULE_ID_TT_LOOP), /**< Invalid parameter                                   */
    VTSS_APPL_TT_LOOP_RC_INVALID_INSTANCE,      /**< Invalid instance                                    */
    VTSS_APPL_TT_LOOP_RC_CREATED,               /**< Instance is already created                         */
    VTSS_APPL_TT_LOOP_RC_NOT_CREATED,           /**< Instance is not created                             */
    VTSS_APPL_TT_LOOP_RC_NOT_SUPPORTED,         /**< Configuration not supported                         */
    VTSS_APPL_TT_LOOP_RC_DUPLICATE,             /**< Configuration is duplicate of or conflicts with other instance */
    VTSS_APPL_TT_LOOP_RC_INTERNAL,              /**< Internal error                                      */
    VTSS_APPL_TT_LOOP_RC_EVC,                   /**< EVC is not found valid                              */
    VTSS_APPL_TT_LOOP_RC_PORT,                  /**< Residence port is not found valid                   */
    VTSS_APPL_TT_LOOP_RC_VLAN,                  /**< VID is not found valid                              */
    VTSS_APPL_TT_LOOP_LL_RC_MEP,                /**< LL MEP ID is not found valid                        */
    VTSS_APPL_TT_LOOP_LL_MC_SMAC,               /**< LL SMAC cannot be multicast                         */
    VTSS_APPL_TT_LOOP_RC_DIR_SUB_INVALID,       /**< Direction and subscriber mode not supported         */
    VTSS_APPL_TT_LOOP_RC_MEP,                   /**< MEP creation failed                                 */
    VTSS_APPL_TT_LOOP_RC_MEP_VOE,               /**< MEP creation failed. Might be that a VOE based MEP is already existing in this flow on this port */
    VTSS_APPL_TT_LOOP_RC_INVALID_FLOW           /**< Flow interface index is invalid */
};

/**
 * \brief Instance capability structure.
 *
 * This structure is used to contain the instance capability.
 *
 */
typedef struct {
    uint32_t instance_max;           /**< Max number of tt_loop instance */
    uint32_t name_max;               /**< Max length of instance name */
} vtss_appl_tt_loop_capabilities_t;

/**
 *  The instance type
*/
typedef enum {
    VTSS_APPL_TT_LOOP_MAC_LOOP,      /**< MAC loop - Not OAM aware - all frames in the flow is looped with MAC swap          */
    VTSS_APPL_TT_LOOP_OAM_LOOP,      /**< OAM loop - OAM aware - in the EVC domain the configured subscriber frames are looped */
} vtss_appl_tt_loop_type_t;

/**
 *  The instance domain
*/
typedef enum {
    VTSS_APPL_TT_LOOP_PORT,      /**< Flow domain is Port           */
    VTSS_APPL_TT_LOOP_EVC,       /**< Flow domain is EVC Service    */
    VTSS_APPL_TT_LOOP_VLAN,      /**< Flow domain is VLAN           */
} vtss_appl_tt_loop_domain_t;

/**
 *  The instance direction
*/
typedef enum {
    VTSS_APPL_TT_LOOP_FACILITY,     /**< Instance is Facility Remote End - pointing out to the port (Down)  */
    VTSS_APPL_TT_LOOP_TERMINAL      /**< Instance is Terminal Remote End - pointing in to the switch (up)   */
} vtss_appl_tt_loop_direction_t;

/**
 *  The instance administrative state
*/
typedef enum {
    VTSS_APPL_TT_LOOP_ADMIN_DISABLED,   /**< Administrative State is Disabled - not "active" but still created - all resources are deleted    */
    VTSS_APPL_TT_LOOP_ADMIN_ENABLED     /**< Administrative State is Enabled - "active" and operational state can change to 'UP' if possible  */
} vtss_appl_tt_loop_admin_state_t;

/**
 *  The instance subscriber mode
*/
typedef enum {
    VTSS_APPL_TT_LOOP_SUB_NONE,     /**< Instance in EVC domain is looping matching on EVC tag                                                 */
    VTSS_APPL_TT_LOOP_SUB_ALL,      /**< Instance in EVC domain is looping matching on all tagges behind EVC tag                               */
    VTSS_APPL_TT_LOOP_SUB_UNTAGGED, /**< Instance in EVC domain is looping matching on untagged behind EVC tag                                 */
    VTSS_APPL_TT_LOOP_SUB_VID       /**< Instance in EVC domain is looping matching on tag with a specific vid, behind the EVC tag. Vid is configured in 'vid' */
} vtss_appl_tt_loop_subscriber_t;

/**
 *  The instance operational state
*/
typedef enum {
    VTSS_APPL_TT_LOOP_OPER_DOWN,      /**< Instance Operational State is DOWN - not all resources are available or administrative state is disabled   */
    VTSS_APPL_TT_LOOP_OPER_UP,        /**< Instance Operational State is UP - all resources are available and allocated                               */
    VTSS_APPL_TT_LOOP_OPER_INACTIVE   /**< Instance Operational State is INACTIVE - all resources are available and allocated but Deactivated by LL.  */
} vtss_appl_tt_loop_oper_state_t;

/**
 * Latching Loopback deactivation causes
 */
typedef enum {
    VTSS_APPL_TT_LOOP_LL_DEACT_CAUSE_REQ,       /**< Deactivated by LLM request */
    VTSS_APPL_TT_LOOP_LL_DEACT_CAUSE_MAN,       /**< Deactivated by management operation */
    VTSS_APPL_TT_LOOP_LL_DEACT_CAUSE_TIMEOUT,   /**< Deactivated by expiry timer timeout */
    VTSS_APPL_TT_LOOP_LL_DEACT_CAUSE_MEP,       /**< Deactivated as MEP has been deleted */

} vtss_appl_tt_loop_ll_deact_cause;


/**
 * \brief Instance configuration data structure.
 *
 * This structure is used to do instance configuration.
 */
typedef struct {
    char                             name[VTSS_APPL_TT_LOOP_NAME_MAX];  /**< Instance name (string)             */
    vtss_appl_tt_loop_type_t         type;                              /**< Type of TT_LOOP                    */
    vtss_appl_tt_loop_direction_t    direction;                         /**< Terminal or Facility direction     */
    vtss_ifindex_t                   flow;                              /**< Flow (EVC - VLAN) instance         */
    mesa_vid_t                       vid;                               /**< Subscriber VID in the EVC.         */

    /*
     * Note: The 'port' value is *not* saved as an ifindex even though the type
     * says that it is! It is saved internally as a zero-offset iport number and
     * converted to an ifindex value when someone uses the public get API.
     */
    vtss_ifindex_t                   port;                              /**< Residence port                     */
    uint32_t                         level;                             /**< MEG level. Range 0 to 7            */
    vtss_appl_tt_loop_subscriber_t   subscriber;                        /**< The subscriber mode.               */
    vtss_appl_tt_loop_admin_state_t  admin_state;                       /**< Administrative state               */

} vtss_appl_tt_loop_conf_t;

/**
 * \brief Instance Latching Loop configuration data structure.
 *
 * This structure is used to do instance LL configuration.
 */
typedef struct {
    mesa_bool_t                      enable;         /**< Enable/disable the LL state machine to control oper state  */
    uint32_t                         mep;            /**< This is the MEP/MIP instance number that this LL instance relates to   */
    mesa_mac_t                       smac;            /**< The looping source MAC     */
} vtss_appl_tt_loop_ll_conf_t;

/**
 * \brief Instance state data structure.
 *
 * This structure is used to retrieve instance state information.
 */
typedef struct {
    vtss_appl_tt_loop_oper_state_t  oper_state;   /**< Operational state   */
} vtss_appl_tt_loop_status_t;

/**
 * \brief Instance Latching Loop state data structure.
 *
 * This structure is used to retrieve LL instance state information.
 */
typedef struct {

    uint32_t    exp_time_remain;    /**< Remaining time (seconds) in ACTIVE state */

} vtss_appl_tt_loop_ll_status_t;

/**
 * \brief Instance default structure.
 *
 * This structure is used to contain configuration default values.
 *
 */
typedef struct
{
    vtss_appl_tt_loop_conf_t            config;           /**< Instance configuration default */
    vtss_appl_tt_loop_ll_conf_t         ll_conf;          /**< LL configuration default */
} vtss_appl_tt_loop_default_conf_t;



/**
 * \brief Default GET.
 *
 * This returns the instance default configuration values
 *
 * \param conf [OUT]      configuration.
 *
 * \return  VTSS_RC_OK
 */
mesa_rc vtss_appl_tt_loop_default_conf_get(vtss_appl_tt_loop_default_conf_t  *const conf);

/**
 * \brief Capabilities GET.
 *
 * This returns the instance capabilities
 *
 * \param conf [OUT]      configuration.
 *
 * \return  VTSS_RC_OK
 */
mesa_rc vtss_appl_tt_loop_capabilities_get(vtss_appl_tt_loop_capabilities_t *const conf);

/**
 * \brief instance basic configuration SET.
 *
 * This function can change any parameter on an already created instance.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_RC_OK
 *          VTSS_RC_ERROR
 *          VTSS_APPL_TT_LOOP_RC_INVALID_INSTANCE
 *          VTSS_APPL_TT_LOOP_RC_INVALID_PARAMETER
 *          VTSS_APPL_TT_LOOP_RC_NOT_CREATED
 */
mesa_rc vtss_appl_tt_loop_instance_conf_set(uint32_t                          instance,
                                            const vtss_appl_tt_loop_conf_t    *const conf);

/**
 * \brief Instance basic configuration GET.
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_RC_OK
 *          VTSS_RC_ERROR
 *          VTSS_APPL_TT_LOOP_RC_INVALID_INSTANCE
 *          VTSS_APPL_TT_LOOP_RC_NOT_CREATED
 */
mesa_rc vtss_appl_tt_loop_instance_conf_get(uint32_t                    instance,
                                            vtss_appl_tt_loop_conf_t    *const conf);

/**
 * \brief Instance ADD.
 *
 * This add (create) a new instance with the given instance number and configuration.
 * The instance must not already be created.
 * The instance administrative state is set to VTSS_APPL_TT_LOOP_ADMIN_DISABLED.
 *
 * \param instance [IN]  Instance number.
 * \param conf [IN]      configuration.
 *
 * \return  VTSS_RC_OK
 *          VTSS_RC_ERROR
 *          VTSS_APPL_TT_LOOP_RC_INVALID_INSTANCE
 *          VTSS_APPL_TT_LOOP_RC_INVALID_PARAMETER
 */
mesa_rc vtss_appl_tt_loop_instance_conf_add(const uint32_t                   instance,
                                            const vtss_appl_tt_loop_conf_t   *const conf);

/**
 * \brief Instance DELETE.
 *
 * This delete an enabled instance with the given instance number.
 *
 * \param instance [IN]        Instance number.
 *
 * \return  VTSS_RC_OK
 *          VTSS_RC_ERROR
 *          VTSS_APPL_TT_LOOP_RC_INVALID_INSTANCE
 *          VTSS_APPL_TT_LOOP_RC_NOT_CREATED
 */
mesa_rc vtss_appl_tt_loop_instance_conf_delete(const uint32_t  instance);

/**
 * \brief Instance iterator.
 *
 * This returns the next enabled instance number. When the end is reached VTSS_RC_ERROR is returned.
 * The search for enabled instance will start with the input 'instance' + 1.
 * If the input 'instance' pointer is NULL, the search starts with the lowest possible instance number.
 *
 * \param instance      [IN]   Instance number.
 * \param next_instance [OUT]  Next enabled instance.
 *
 * \return  VTSS_RC_OK
 *          VTSS_RC_ERROR
 */
mesa_rc vtss_appl_tt_loop_instance_conf_iter(const uint32_t    *const instance,
                                             uint32_t          *const next_instance);

/**
 * \brief instance status GET.
 *
 * \param instance [IN] Instance number.
 * \param status [OUT]  Status.
 *
 * \return  VTSS_RC_OK
 *          VTSS_RC_ERROR
 *          VTSS_APPL_TT_LOOP_RC_INVALID_INSTANCE
 *          VTSS_APPL_TT_LOOP_RC_NOT_CREATED
 */
mesa_rc vtss_appl_tt_loop_instance_status_get(const uint32_t                   instance,
                                              vtss_appl_tt_loop_status_t  *const status);

/**
 * \brief instance Latching Loop configuration SET.
 *
 * This function configures the Latching Loop functionallity.
 *
 * \param instance [IN] Instance number.
 * \param conf [IN]     configuration.
 *
 * \return  VTSS_RC_OK
 *          VTSS_RC_ERROR
 *          VTSS_APPL_TT_LOOP_RC_INVALID_INSTANCE
 *          VTSS_APPL_TT_LOOP_RC_INVALID_PARAMETER
 *          VTSS_APPL_TT_LOOP_RC_NOT_CREATED
 */
mesa_rc vtss_appl_tt_loop_instance_ll_conf_set(uint32_t                                instance,
                                               const vtss_appl_tt_loop_ll_conf_t  *const conf);

/**
 * \brief Instance Latching Loop configuration GET.
 *
 * \param instance [IN] Instance number.
 * \param conf [OUT]    configuration.
 *
 * \return  VTSS_RC_OK
 *          VTSS_RC_ERROR
 *          VTSS_APPL_TT_LOOP_RC_INVALID_INSTANCE
 *          VTSS_APPL_TT_LOOP_RC_NOT_CREATED
 */
mesa_rc vtss_appl_tt_loop_instance_ll_conf_get(uint32_t                          instance,
                                               vtss_appl_tt_loop_ll_conf_t  *const conf);

/**
 * \brief instance Latching Loop status GET.
 *
 * \param instance [IN] Instance number.
 * \param ll_status [OUT]  Latching Loopback Status.
 *
 * \return  VTSS_RC_OK
 *          VTSS_RC_ERROR
 *          VTSS_APPL_TT_LOOP_RC_INVALID_INSTANCE
 *          VTSS_APPL_TT_LOOP_RC_NOT_CREATED
 */
mesa_rc vtss_appl_tt_loop_instance_ll_status_get(const uint32_t                      instance,
                                                 vtss_appl_tt_loop_ll_status_t  *const ll_status);

#ifdef __cplusplus
}
#endif
#endif /* _vtss_appl_tt_loop_H_ */
