/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

/*!
 * \file
 *
 * \brief Public SR (IEEE 802.1CB - Seamless Redundancy) API.
 *
 * This header file describes SR control functions and types.\n
 *
 */

#ifndef _VTSS_APPL_SR_H_
#define _VTSS_APPL_SR_H_

#include <mscc/ethernet/switch/api/types.h>
#include <vtss/appl/types.h>
#include <vtss/appl/interface.h>

#ifdef __cplusplus
extern "C" {
#endif

/****************************************************************************
 * SR Capabilities
 ****************************************************************************/

/** \brief Seamless Redundancy stream ID */
typedef uint32_t vtss_appl_sr_stream_id_t;

/*! \brief Seamless Redundancy capabilities.
 *
 *  Contains platform specific sr definitions.
 */
typedef struct {
    vtss_appl_sr_stream_id_t   stream_idx_max;  /*!< Maximum allowed value of stream index;
                                                 *   Valid stream index range will be [0..stream_idx_max]
                                                 */
    uint8_t                         seq_width_min;   /*!< Minimum number of bits to be used for sequence number. */
    uint8_t                         seq_width_max;   /*!< Maximum number of bits to be used for sequence number. */
    uint8_t                         seq_history_min; /*!< Minimum length of sequency recovery history window. */
    uint8_t                         seq_history_max; /*!< Maximum length of sequency recovery history window. */
} vtss_appl_sr_capabilities_t;

/*!
 * \brief Get SR capabilities.
 *
 * \param c [OUT] The Seamless Redundancy capabilities.
 *
 * \return Return code.
 */
mesa_rc vtss_appl_sr_capabilities_get(vtss_appl_sr_capabilities_t *c);

/****************************************************************************
 * SR Global Configuration
 ****************************************************************************/

/** \brief Seamless Redundancy configuration */
typedef struct {
    mesa_etype_t etype; /**< Redundancy tag Ethernet Type */
} vtss_appl_sr_conf_t;

#define VTSS_APPL_SR_RED_TAG_ETYPE_DEFAULT_VAL 0x829E   /**< Seamless Redundancy tag's default etype value */

/**
 * \brief Get Seamless Redundancy configuration.
 *
 * \param conf [OUT]  Seamless Redundancy configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_sr_conf_get(vtss_appl_sr_conf_t *const conf);

/**
 * \brief Set Seamless Redundancy configuration.
 *
 * \param conf [IN]  Seamless Redundancy configuration.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_sr_conf_set(const vtss_appl_sr_conf_t *const conf);

/** \brief Stream mode */
typedef enum {
    VTSS_APPL_SR_MODE_DISABLED = 0, /**< Disabled stream */
    VTSS_APPL_SR_MODE_GENERATION,   /**< Generation mode */
    VTSS_APPL_SR_MODE_RECOVERY      /**< Recovery mode */
} vtss_appl_sr_mode_t;

/** \brief Stream configuration */
typedef struct {
    vtss_appl_sr_mode_t mode;       /**< Stream mode */
    uint8_t                  seq_width;  /**< Sequence number width (bits), 1-28 */

    struct {
        mesa_bool_t                seq_enable;    /**< Sequence number generation enable */
        vtss_port_list_stackable_t rx_port_list;  /**< Ingress port list */
        vtss_port_list_stackable_t tx_port_list;  /**< Egress port list for split operation */
        uint8_t                    tx_port_count; /**< Egress port count to keep track */
    } gen; /**< Generation mode configuration */

    struct {
        uint8_t   seq_history_len; /**< Sequence number history length, 1-32 */
        mesa_bool_t pop_tag;         /**< Pop redundancy tag */
    } rec; /**< Recovery mode configuration */

} vtss_appl_sr_stream_conf_t;

/**
 * \brief Get stream configuration.
 *
 * \param id   [IN]   Stream ID.
 * \param conf [OUT]  Stream configuration structure.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_sr_stream_conf_get(
    const vtss_appl_sr_stream_id_t      id,
    vtss_appl_sr_stream_conf_t  *const  conf
);

/**
 * \brief port list parameter serialization for configuration get
 *
 * \param id   [IN]   Stream ID.
 * \param conf [OUT]  Stream configuration structure.
 *
 * \return Return code.
 **/
mesa_rc vtss_sr_stream_config_serialize_get(
    const vtss_appl_sr_stream_id_t      id,
    vtss_appl_sr_stream_conf_t  *const  conf
);

/**
 * \brief Set stream configuration.
 *
 * \param id   [IN]  Stream ID.
 * \param conf [IN]  Stream configuration structure.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_sr_stream_conf_set(
    const vtss_appl_sr_stream_id_t          id,
    const vtss_appl_sr_stream_conf_t *const conf
);

/**
 * \brief port list parameter serialization for configuration set.
 *
 * \param id   [IN]  Stream ID.
 * \param conf [IN]  Stream configuration structure.
 *
 * \return Return code.
 **/
mesa_rc vtss_sr_stream_config_serialize_set(
    const vtss_appl_sr_stream_id_t          id,
    const vtss_appl_sr_stream_conf_t *const conf
);

/**
 * \brief Stream configuration iterator.
 *
 * \param prev_id [IN]   Previous Stream ID.
 * \param next_id [OUT]  Next Stream ID.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_sr_stream_conf_itr(
    const vtss_appl_sr_stream_id_t *const prev_id,
    vtss_appl_sr_stream_id_t       *const next_id
);

/** \brief Latent Error Detection (IEEE802.1CB-d2-1, clause 7.3.4) configuration structure */
typedef struct {
    mesa_bool_t mode;              /**< Mode of Latent Error detection function as per clause 9.4.1.10 */
    uint32_t    threshold;         /**< Maximum allowed difference as per clause 9.4.1.11.1 */
    uint32_t    polling_period;    /**< Number of milliseconds as per clause 9.4.1.11.2 */
    uint32_t    no_of_red_paths;   /**< Number of paths over which SR is operating, clause 9.4.1.11.3 */
    uint32_t    reset_period;      /**< As per clause 9.4.1.11.4 */
} vtss_appl_sr_latent_err_detect_cfg_t;

/** \brief Latent Error Detection status structure */
typedef struct {
    uint32_t   cur_diff_count; /**< CurBaseDifference per clause 7.3.4.2.1 */
    mesa_bool_t error_occurred; /**< SIGNAL_LATENT_ERROR event, point d) in clause 7.3.4.1 */
} vtss_appl_sr_latent_err_detect_status_t;

/**
 * \brief Stream Latent Error detection status.
 *
 * \param id     [IN]   Stream ID.
 * \param status [OUT]  Stream Latent Error detection status structure.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_sr_le_detect_status_get(
    const vtss_appl_sr_stream_id_t                  id,
    vtss_appl_sr_latent_err_detect_status_t *const  status
);

/**
 * \brief Get Latent Error detection configuration.
 *
 * \param id   [IN]   Stream ID.
 * \param conf [OUT]  Stream Latent Error detection configuration structure.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_sr_latent_err_detect_cfg_get(
    const vtss_appl_sr_stream_id_t              id,
    vtss_appl_sr_latent_err_detect_cfg_t *const conf
);

/**
 * \brief Set Latent Error detection configuration.
 *
 * \param id   [IN]  Stream ID.
 * \param conf [IN]  Stream Latent Error detection configuration structure.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_sr_latent_err_detect_cfg_set(
    const vtss_appl_sr_stream_id_t                      id,
    const vtss_appl_sr_latent_err_detect_cfg_t *const   conf
);

/**
 * \brief Stream Latent Error detection iterator.
 *
 * \param prev_id [IN]   Previous Stream ID.
 * \param next_id [OUT]  Next Stream ID.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_sr_latent_err_detect_itr(
    const vtss_appl_sr_stream_id_t *const prev_id,
    vtss_appl_sr_stream_id_t       *const next_id
);

/** \brief Operational port and Stream counters (as defined in IEEE 802.1CB-d2-1, section 9.8 */
typedef struct {
    /* Generated by HW */
    mesa_counter_t rx_total;        /**< Total number of received packets, clause 9.8.1 */
    mesa_counter_t rx_passed;       /**< Number of received packets passed, clause 9.8.6 */
    mesa_counter_t rx_lost;         /**< Number of received packets lost, clause 9.8.8 */
    mesa_counter_t rx_out_of_order; /**< Number of received packets accepted out-of-order, clause 9.8.4 */
    mesa_counter_t rx_rogue;        /**< Number of received packets discarded as outside history window, clause 9.8.5 */
    mesa_counter_t rx_discard;      /**< Number of received packets discarded as duplicate, clause 9.8.7 */
    mesa_counter_t rx_tagless;      /**< Number of received packets without redundancy tag, clause 9.8.9 */

    /* Generated by SW */
    mesa_counter_t tx_total;       /**< Number of tx packets for a given stream, clause 9.8.2 */
    mesa_counter_t seq_gen_resets; /**< Number of sequence generation function resets, clause 9.8.3 */
    mesa_counter_t seq_rcv_resets; /**< Number of sequence recovery function resets, clause 9.8.10 */
    mesa_counter_t la_err_resets;  /**< Counts the total number of latest error resets, clause 9.8.11 */
    mesa_counter_t rx_decode_err;  /**< tsnSeqEncErroredPackets as defined in IEEE 802.1CB, clause 9.8.12. */

} vtss_appl_sr_stream_counters_t;

/**
 * \brief Get stream counters.
 *
 * \param id       [IN]   Stream ID.
 * \param counters [OUT]  Counter structure.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_sr_stream_counters_get(
    const vtss_appl_sr_stream_id_t        id,
    vtss_appl_sr_stream_counters_t *const counters
);

/**
 * \brief Clear stream counters.
 *
 * \param id [IN]    Stream ID.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_sr_stream_counters_clear(
    const vtss_appl_sr_stream_id_t id
);

/**
 * \brief Stream counters iterator.
 *
 * \param prev_id [IN]   Previous Stream ID.
 * \param next_id [OUT]  Next Stream ID.
 *
 * \return Return code.
 **/
mesa_rc vtss_appl_sr_stream_counters_itr(
    const vtss_appl_sr_stream_id_t *const prev_id,
    vtss_appl_sr_stream_id_t       *const next_id
);

#ifdef __cplusplus
}
#endif
#endif /* _VTSS_APPL_SR_H_ */
