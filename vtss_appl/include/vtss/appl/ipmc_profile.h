/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

/**
* \file
* \brief Public IPMC Profile API
* \details This header file describes IPMC Profile control functions and types.
*/

#ifndef _VTSS_APPL_IPMC_PROFILE_H_
#define _VTSS_APPL_IPMC_PROFILE_H_

#include <vtss/appl/types.h>
#include <vtss/appl/ipmc_lib_public.h>

#ifdef __cplusplus
extern "C" {
#endif

/*! \brief IPMC index type by using name string */
typedef struct {
    char    n[VTSS_APPL_IPMC_NAME_MAX_LEN]; /**< Array to store the characters of IPMC name */
} vtss_appl_ipmc_name_index_t;

#if ((defined(VTSS_SW_OPTION_SMB_IPMC) || defined(VTSS_SW_OPTION_MVR)) && defined(VTSS_SW_OPTION_IPMC_LIB))

/**
 * \brief IPMC Profile global configuration
 * The configuration is the system configuration that can enable/disable
 * the IPMC Profile function.
 */
typedef struct {
    /**
     * \brief Global administrative mode, TRUE is to enable IPMC Profile
     * function in the system and FALSE is to disable it.
     */
    mesa_bool_t                            mode;
} vtss_appl_ipmc_profile_global_t;

/**
 * \brief IPMC Profile Management table
 *  The configuration is the profile settings used for managing profile entry.
 *  Key: ProfileName
 */
typedef struct {
    /**
     * \brief profile_desc is used to denote the description of the
     * profile entry.
     */
    char                            profile_description[VTSS_APPL_IPMC_DESC_MAX_LEN];
} vtss_appl_ipmc_profile_mgmt_t;

/**
 * \brief IPMC Profile IPv4 address range table
 *  The configuration is the IPv4 address range settings used for IPMC Profile.
 *  Key: RangeName
 */
typedef struct {
    /**
     * \brief start_address is used to denote starting IPv4 address of the range
     * that IPMC Profile performs checking.
     */
    mesa_ipv4_t                     start_address;

    /**
     * \brief end_address is used to denote ending IPv4 address of the range
     * that IPMC Profile performs checking.
     */
    mesa_ipv4_t                     end_address;
} vtss_appl_ipmc_profile_ipv4_range_t;

#ifdef VTSS_SW_OPTION_IPV6
/**
 * \brief IPMC Profile IPv6 address range table
 *  The configuration is the IPv6 address range settings used for IPMC Profile.
 *  Key: RangeName
 */
typedef struct {
    /**
     * \brief start_address is used to denote starting IPv6 address of the range
     * that IPMC Profile performs checking.
     */
    mesa_ipv6_t                     start_address;

    /**
     * \brief end_address is used to denote ending IPv6 address of the range
     * that IPMC Profile performs checking.
     */
    mesa_ipv6_t                     end_address;
} vtss_appl_ipmc_profile_ipv6_range_t;
#endif /* VTSS_SW_OPTION_IPV6 */

/**
 * \brief IPMC Profile rule table
 *  The configuration is the profiling rule settings used in IPMC Profile.
 *  Key: (ProfileName, RuleRange)
 */
typedef struct {
    /**
     * \brief next_range_name is used to adjust the precedence among the
     * rules in this IPMC profile management entry.
     */
    vtss_appl_ipmc_name_index_t     next_rule_range;

    /**
     * \brief rule_action is used to denote the filtering result when
     * IPMC Profile performs checking.
     */
    vtss_appl_ipmc_action_t         rule_action;

    /**
     * \brief rule_log is to used to decide whether the filtering
     * result should be logged.
     */
    mesa_bool_t                            rule_log;
} vtss_appl_ipmc_profile_rule_t;

/**
 * \brief IPMC Profile precedence table
 *  The configuration is the profiling rule settings used in IPMC Profile.
 *  Key: (ProfileName, RulePrecedence)
 */
typedef struct {
    /**
     * \brief range_name presents the specific range used as a rule that
     * this profile_name IPMC profile management entry should check for
     * filtering based on the associated address.
     */
    vtss_appl_ipmc_name_index_t     rule_range;

    /**
     * \brief next_range_name is used to adjust the precedence among the
     * rules in this IPMC profile management entry.
     */
    vtss_appl_ipmc_name_index_t     next_rule_range;

    /**
     * \brief rule_action is used to denote the filtering result when
     * IPMC Profile performs checking.
     */
    vtss_appl_ipmc_action_t         rule_action;

    /**
     * \brief rule_log is to used to decide whether the filtering
     * result should be logged.
     */
    mesa_bool_t                            rule_log;
} vtss_appl_ipmc_profile_precedence_t;

/**
 * \brief Get IPMC Profile Parameters
 *
 * To read current system parameters in IPMC Profile.
 *
 * \param conf      [OUT]    The IPMC Profile system configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_system_config_get(
    vtss_appl_ipmc_profile_global_t             *const conf
);

/**
 * \brief Set IPMC Profile Parameters
 *
 * To modify current system parameters in IPMC Profile.
 *
 * \param conf      [IN]     The IPMC Profile system configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_system_config_set(
    const vtss_appl_ipmc_profile_global_t       *const conf
);

/**
 * \brief Iterator for retrieving IPMC Profile management table key/index
 *
 * To walk name index of the management table in IPMC Profile.
 *
 * \param prev      [IN]    Name to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_management_table_itr(
    const vtss_appl_ipmc_name_index_t           *const prev,
    vtss_appl_ipmc_name_index_t                 *const next
);

/**
 * \brief Get IPMC Profile specific management configuration
 *
 * To get configuration of the specific entry in IPMC Profile management table.
 *
 * \param name      [IN]    (key) Name of the profile.
 * \param entry     [OUT]   The current configuration of the profile in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_management_table_get(
    const vtss_appl_ipmc_name_index_t           *const name,
    vtss_appl_ipmc_profile_mgmt_t               *const entry
);

/**
 * \brief Set IPMC Profile specific management configuration
 *
 * To modify configuration of the specific entry in IPMC Profile management table.
 *
 * \param name      [IN]    (key) Name of the profile.
 * \param entry     [IN]    The revised configuration of the profile in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_management_table_set(
    const vtss_appl_ipmc_name_index_t           *const name,
    const vtss_appl_ipmc_profile_mgmt_t         *const entry
);

/**
 * \brief Delete IPMC Profile specific management configuration
 *
 * To delete configuration of the specific entry in IPMC Profile management table.
 *
 * \param name      [IN]    (key) Name of the profile.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_management_table_del(
    const vtss_appl_ipmc_name_index_t           *const name
);

/**
 * \brief Add IPMC Profile specific management configuration
 *
 * To add configuration of the specific entry in IPMC Profile management table.
 *
 * \param name      [IN]    (key) Name of the profile.
 * \param entry     [IN]    The new configuration of the profile in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_management_table_add(
    const vtss_appl_ipmc_name_index_t           *const name,
    const vtss_appl_ipmc_profile_mgmt_t         *const entry
);

/**
 * \brief Iterator for retrieving IPMC Profile IPv4 range table key/index
 *
 * To walk name index of the IPv4 range table in IPMC Profile.
 *
 * \param prev      [IN]    Name to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv4_range_table_itr(
    const vtss_appl_ipmc_name_index_t           *const prev,
    vtss_appl_ipmc_name_index_t                 *const next
);

/**
 * \brief Get IPMC Profile specific IPv4 range configuration
 *
 * To read configuration of the specific entry in IPMC Profile IPv4 range table.
 *
 * \param name      [IN]    (key) Name of the range.
 * \param entry     [OUT]   The current configuration of the range in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv4_range_table_get(
    const vtss_appl_ipmc_name_index_t           *const name,
    vtss_appl_ipmc_profile_ipv4_range_t         *const entry
);

/**
 * \brief Set IPMC Profile specific IPv4 range configuration
 *
 * To modify configuration of the specific entry in IPMC Profile IPv4 range table.
 *
 * \param name      [IN]    (key) Name of the range.
 * \param entry     [IN]    The revised configuration of the range in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv4_range_table_set(
    const vtss_appl_ipmc_name_index_t           *const name,
    const vtss_appl_ipmc_profile_ipv4_range_t   *const entry
);

/**
 * \brief Delete IPMC Profile specific IPv4 range configuration
 *
 * To delete configuration of the specific entry in IPMC Profile IPv4 range table.
 *
 * \param name      [IN]    (key) Name of the range.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv4_range_table_del(
    const vtss_appl_ipmc_name_index_t           *const name
);

/**
 * \brief Add IPMC Profile specific IPv4 range configuration
 *
 * To add configuration of the specific entry in IPMC Profile IPv4 range table.
 *
 * \param name      [IN]    (key) Name of the range.
 * \param entry     [IN]    The new configuration of the range in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv4_range_table_add(
    const vtss_appl_ipmc_name_index_t           *const name,
    const vtss_appl_ipmc_profile_ipv4_range_t   *const entry
);

#ifdef VTSS_SW_OPTION_IPV6
/**
 * \brief Iterator for retrieving IPMC Profile IPv4 range table key/index
 *
 * To walk name index of the IPv6 range table in IPMC Profile.
 *
 * \param prev      [IN]    Name to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv6_range_table_itr(
    const vtss_appl_ipmc_name_index_t           *const prev,
    vtss_appl_ipmc_name_index_t                 *const next
);

/**
 * \brief Get IPMC Profile specific IPv6 range configuration
 *
 * To read configuration of the specific entry in IPMC Profile IPv4 range table.
 *
 * \param name      [IN]    (key) Name of the range.
 * \param entry     [OUT]   The current configuration of the range in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv6_range_table_get(
    const vtss_appl_ipmc_name_index_t           *const name,
    vtss_appl_ipmc_profile_ipv6_range_t         *const entry
);

/**
 * \brief Set IPMC Profile specific IPv6 range configuration
 *
 * To modify configuration of the specific entry in IPMC Profile IPv6 range table.
 *
 * \param name      [IN]    (key) Name of the range.
 * \param entry     [IN]    The revised configuration of the range in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv6_range_table_set(
    const vtss_appl_ipmc_name_index_t           *const name,
    const vtss_appl_ipmc_profile_ipv6_range_t   *const entry
);

/**
 * \brief Delete IPMC Profile specific IPv6 range configuration
 *
 * To delete configuration of the specific entry in IPMC Profile IPv6 range table.
 *
 * \param name      [IN]    (key) Name of the range.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv6_range_table_del(
    const vtss_appl_ipmc_name_index_t           *const name
);

/**
 * \brief Add IPMC Profile specific IPv6 range configuration
 *
 * To add configuration of the specific entry in IPMC Profile IPv6 range table.
 *
 * \param name      [IN]    (key) Name of the range.
 * \param entry     [IN]    The new configuration of the range in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv6_range_table_add(
    const vtss_appl_ipmc_name_index_t           *const name,
    const vtss_appl_ipmc_profile_ipv6_range_t   *const entry
);
#endif /* VTSS_SW_OPTION_IPV6 */

/**
 * \brief Iterator for retrieving IPMC Profile rule table key/index
 *
 * To walk indexes of the rule table in IPMC Profile.
 *
 * \param prf_prev  [IN]    Profile name to be used for indexing determination.
 * \param rng_prev  [IN]    Range name to be used for indexing determination.
 *
 * \param prf_next  [OUT]   The key/index of profile should be used for the GET operation.
 * \param rng_next  [OUT]   The key/index of range should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_rule_table_itr(
    const vtss_appl_ipmc_name_index_t           *const prf_prev,
    vtss_appl_ipmc_name_index_t                 *const prf_next,
    const vtss_appl_ipmc_name_index_t           *const rng_prev,
    vtss_appl_ipmc_name_index_t                 *const rng_next
);

/**
 * \brief Get IPMC Profile specific rule configuration
 *
 * To get configuration of the specific entry in IPMC Profile rule table.
 *
 * \param prf_name  [IN]    (key) Name of the profile.
 * \param rng_name  [IN]    (key) Name of the range.
 * \param entry     [OUT]   The current configuration of the rule in a specific profile.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_rule_table_get(
    const vtss_appl_ipmc_name_index_t           *const prf_name,
    const vtss_appl_ipmc_name_index_t           *const rng_name,
    vtss_appl_ipmc_profile_rule_t               *const entry
);

/**
 * \brief Set IPMC Profile specific rule configuration
 *
 * To modify configuration of the specific entry in IPMC Profile rule table.
 *
 * \param prf_name  [IN]    (key) Name of the profile.
 * \param rng_name  [IN]    (key) Name of the range.
 * \param entry     [OUT]   The revised configuration of the rule in a specific profile.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_rule_table_set(
    const vtss_appl_ipmc_name_index_t           *const prf_name,
    const vtss_appl_ipmc_name_index_t           *const rng_name,
    const vtss_appl_ipmc_profile_rule_t         *const entry
);

/**
 * \brief Delete IPMC Profile specific rule configuration
 *
 * To delete configuration of the specific entry in IPMC Profile rule table.
 *
 * \param prf_name  [IN]    (key) Name of the profile.
 * \param rng_name  [IN]    (key) Name of the range.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_rule_table_del(
    const vtss_appl_ipmc_name_index_t           *const prf_name,
    const vtss_appl_ipmc_name_index_t           *const rng_name
);

/**
 * \brief Add IPMC Profile specific rule configuration
 *
 * To add configuration of the specific entry in IPMC Profile rule table.
 *
 * \param prf_name  [IN]    (key) Name of the profile.
 * \param rng_name  [IN]    (key) Name of the range.
 * \param entry     [OUT]   The new configuration of the rule in a specific profile.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_rule_table_add(
    const vtss_appl_ipmc_name_index_t           *const prf_name,
    const vtss_appl_ipmc_name_index_t           *const rng_name,
    const vtss_appl_ipmc_profile_rule_t         *const entry
);

/**
 * \brief Iterator for retrieving IPMC Profile rule precedence table key/index
 *
 * To walk the IPMC Profile rule table in precedence order.
 *
 * \param prf_prev  [IN]    Profile name to be used for indexing determination.
 * \param idx_prev  [IN]    Precedence value to be used for indexing determination.
 *
 * \param prf_next  [OUT]   The key/index of profile should be used for the GET operation.
 * \param idx_next  [OUT]   The key/index of precedence should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_precedence_table_itr(
    const vtss_appl_ipmc_name_index_t           *const prf_prev,
    vtss_appl_ipmc_name_index_t                 *const prf_next,
    const uint32_t                                   *const idx_prev,
    uint32_t                                         *const idx_next
);

/**
 * \brief Get IPMC Profile specific rule configuration w.r.t. precedence
 *
 * To get configuration of the specific entry in IPMC Profile rule table w.r.t. precedence.
 *
 * \param prf_name  [IN]    (key) Name of the profile.
 * \param rule_idx  [IN]    (key) Precedence of the rule.
 * \param entry     [OUT]   The current configuration of the rule in a specific profile.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_precedence_table_get(
    const vtss_appl_ipmc_name_index_t           *const prf_name,
    const uint32_t                                   *const rule_idx,
    vtss_appl_ipmc_profile_precedence_t         *const entry
);

#endif /* ((defined(VTSS_SW_OPTION_SMB_IPMC) || defined(VTSS_SW_OPTION_MVR)) && defined(VTSS_SW_OPTION_IPMC_LIB)) */

#ifdef __cplusplus
}
#endif

#endif  /* _VTSS_APPL_IPMC_PROFILE_H_ */
