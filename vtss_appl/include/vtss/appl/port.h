/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

/**
 * \file
 * \brief Public Port API
 * \details This header file describes Port control functions and types
 */

#ifndef _VTSS_APPL_PORT_H_
#define _VTSS_APPL_PORT_H_

#include <vtss/appl/types.h>
#include <vtss/appl/interface.h>
#include <vtss/appl/module_id.h>
#include <vtss/basics/enum-descriptor.h>
#include <mscc/ethernet/switch/api/port.h>

#include <mscc/ethernet/board/api.h>

#ifdef __cplusplus
extern "C" {
#endif

extern vtss_enum_descriptor_t vtss_port_speed_txt[];           /*!< Enum descriptor text */
extern vtss_enum_descriptor_t vtss_port_media_txt[];           /*!< Enum descriptor text */
extern vtss_enum_descriptor_t vtss_port_duplex_txt[];          /*!< Enum descriptor text */
extern vtss_enum_descriptor_t vtss_port_fc_txt[];              /*!< Enum descriptor text */
extern vtss_enum_descriptor_t vtss_port_sfp_transceiver_txt[]; /*!< Enum descriptor text */
extern vtss_enum_descriptor_t vtss_port_status_speed_txt[];    /*!< Enum descriptor text */
extern vtss_enum_descriptor_t vtss_port_phy_veriphy_status_txt[];    /*!< Enum descriptor text */

/*
 * PORT types and defines
 */
/** Definition of error return codes return by the function in case of malfunctioning behavior*/
enum {
    VTSS_APPL_PORT_ERROR_GEN = MODULE_ERROR_START(VTSS_MODULE_ID_PORT),  /* Generic error code */
    VTSS_APPL_PORT_ERROR_VERIPHY_RUNNING,   /** VeriPHY still running */
    VTSS_APPL_PORT_ERROR_PARM,              /** Illegal parameter */
    VTSS_APPL_PORT_ERROR_REG_TABLE_FULL,    /** Registration table full */
    VTSS_APPL_PORT_ERROR_REQ_TIMEOUT,       /** Timeout on message request */
    VTSS_APPL_PORT_ERROR_STACK_STATE,       /** Illegal MASTER/SLAVE state */
    VTSS_APPL_PORT_ERROR_MUST_BE_MASTER,    /** Not allowed at slave switch */
    VTSS_APPL_PORT_ERROR_ILLEGAL_SPEED,     /** Illegal speed for the current speed */
    VTSS_APPL_PORT_ERROR_ILLEGAL_ADVERTISE, /** Illegal advertise disable */
    VTSS_APPL_PORT_ERROR_INVALID_MTU,       /** MTU is outside the legal range
                                                (between VTSS_MAX_FRAME_LENGTH_STANDARD -  VTSS_MAX_FRAME_LENGTH_MAX) */
    VTSS_APPL_PORT_ERROR_FIBER_NOT_SUPPORT, /** FIBER not supported for the port */
    VTSS_APPL_PORT_ERROR_CU_NOT_SUPPORT,    /** CU not supported for the port */
    VTSS_APPL_PORT_ERROR_IFINDEX_NOT_PORT,  /** The ifindex is not a port interface */
    VTSS_APPL_PORT_ERROR_FLOWCONTROL,       /** Standard flowcontrol cannot be enabled together with PFC or PFC not supported */
    VTSS_APPL_PORT_ERROR_INVALID_PORT,      /** Invalid port number*/
    VTSS_APPL_PORT_ERROR_INVALID_SID,       /** Invalid switch ID*/
};

/** \brief VeriPHY result for one port*/
typedef struct {
    mesa_phy_veriphy_status_t status_pair_a; /**< Status, pair A */
    mesa_phy_veriphy_status_t status_pair_b; /**< Status, pair B */
    mesa_phy_veriphy_status_t status_pair_c; /**< Status, pair C */
    mesa_phy_veriphy_status_t status_pair_d; /**< Status, pair D */
    uint8_t                        length_pair_a; /**< Length (meters) pair A */
    uint8_t                        length_pair_b; /**< Length (meters) pair B */
    uint8_t                        length_pair_c; /**< Length (meters) pair_C */
    uint8_t                        length_pair_d; /**< Length (meters) pair D */
} vtss_appl_port_veriphy_result_t;

/*! \brief Port speed type.*/
typedef enum {
    VTSS_APPL_PORT_SPEED_10M_FDX,    /**< Forced 10M mode, full duplex       */
    VTSS_APPL_PORT_SPEED_10M_HDX,    /**< Forced 10M mode, half duplex       */
    VTSS_APPL_PORT_SPEED_100M_FDX,   /**< Forced 100M mode, full duplex      */
    VTSS_APPL_PORT_SPEED_100M_HDX,   /**< Forced 100M mode, half duplex      */
    VTSS_APPL_PORT_SPEED_1G_FDX,     /**< Forced 1G mode, full duplex        */
    VTSS_APPL_PORT_SPEED_AUTO,       /**< Auto-negotiation mode              */
    VTSS_APPL_PORT_SPEED_2500M_FDX,  /**< Forced 2.5G mode, full duplex      */
    VTSS_APPL_PORT_SPEED_5G_FDX,     /**< Forced 5G mode, full duplex        */
    VTSS_APPL_PORT_SPEED_10G_FDX,    /**< Forced 10G mode, full duplex       */
    VTSS_APPL_PORT_SPEED_12G_FDX,    /**< Forced 12G mode, full duplex       */
} vtss_appl_port_speed_t;

/*! \brief The maximum length of port description - including terminating NULL. */
#define VTSS_APPL_PORT_IF_DESCR_MAX_LEN    201 /**< Maximum length for port description */

/*! \brief Port media type. */
typedef enum {
    VTSS_APPL_PORT_MEDIA_CU,   /**< Port in CU only mode    */
    VTSS_APPL_PORT_MEDIA_SFP,  /**< Port in SFP only mode   */
    VTSS_APPL_PORT_MEDIA_DUAL, /**< Port in dual media mode */
} vtss_appl_port_media_t;

/*! \brief Port flow control type. */
typedef enum {
    VTSS_APPL_PORT_FC_OFF, /**< Flow control off */
    VTSS_APPL_PORT_FC_ON,  /**< Flow control on */
} vtss_appl_port_fc_t;

/*! \brief MIB object configuration parameters which SNMP can get/set. */
typedef struct {
    mesa_bool_t             shutdown;  /**< Shutdown port */
    vtss_appl_port_speed_t  speed;     /**< Port speed and duplex mode (Full or Half) */
    uint32_t                advertise_dis; /**< In auto mode, bitmask that allows features not
                                            to be advertised */
    vtss_appl_port_media_t  media;     /**< Media type */
    vtss_appl_port_fc_t     fc;        /**< Flow control */
    uint8_t                 pfc_mask;  /**< 802.1Qbb Priority Flow Control bitmask.
                                            One bit for each prio. 0x01 = prio 0, 0x80 = prio 7, 0xFF = prio 0-7 */
    uint32_t                mtu;       /**< Maximum Transmission Unit */
    mesa_bool_t             excessive; /**< TRUE to restart half-duplex back-off algorithm after 16 collisions. FALSE to discard frame after 16 collisions */
    mesa_bool_t             frame_length_chk;  /**< TRUE to Enforce 802.3 frame length check (from ethertype field) */
} vtss_appl_port_mib_conf_t;

/*! \brief Port proprietary priority queues counters for better being able to do serialization of these counters */
typedef struct {
    mesa_port_counter_t rx_prio;       /**< Rx frames for this queue */
    mesa_port_counter_t tx_prio;       /**< Tx frames for this queue */
} vtss_appl_port_prio_counter_t;

/*! \brief Detected SFP tranceiver types. */
typedef enum {
    VTSS_APPL_PORT_SFP_NONE,          /**< No SFP */
    VTSS_APPL_PORT_SFP_NOT_SUPPORTED, /**< SFP not supported for this interface */
    VTSS_APPL_PORT_SFP_100FX,         /**< 100M Fiber SFP  */
    VTSS_APPL_PORT_SFP_100BASE_LX,
    VTSS_APPL_PORT_SFP_100BASE_ZX,
    VTSS_APPL_PORT_SFP_100BASE_BX10,  /**< Not supported */
    VTSS_APPL_PORT_SFP_100BASE_T,     /**< Not supported */
    VTSS_APPL_PORT_SFP_1000BASE_BX10, /**< Not supported */
    VTSS_APPL_PORT_SFP_1000BASE_T,    /**< CU SFP  */
    VTSS_APPL_PORT_SFP_1000BASE_CX,   /**< 1G Fiber CX SFP  */
    VTSS_APPL_PORT_SFP_1000BASE_SX,   /**< 1G Fiber SX SFP  */
    VTSS_APPL_PORT_SFP_1000BASE_LX,   /**< 1G Fiber LX SFP  */
    VTSS_APPL_PORT_SFP_1000BASE_X,    /**< 1G Fiber X SFP  */
    VTSS_APPL_PORT_SFP_2G5,           /**< 2.5G Fiber SFP  */
    VTSS_APPL_PORT_SFP_5G,            /**< 5G Fiber SFP  */
    VTSS_APPL_PORT_SFP_10G,           /**< 10G Fiber SFP+  */
    VTSS_APPL_PORT_SFP_10G_SR,        /**< 10G Fiber SFP+ short range (400m) */
    VTSS_APPL_PORT_SFP_10G_LR,        /**< 10G Fiber SFP+ long range (10km) */
    VTSS_APPL_PORT_SFP_10G_LRM,       /**< 10G Fiber SFP+ long range multimode (220m) */
    VTSS_APPL_PORT_SFP_10G_ER,        /**< 10G Fiber SFP+ extended range (40km) */
    VTSS_APPL_PORT_SFP_10G_DAC,       /**< 10G DAC SFP+ Cu  */
} vtss_appl_sfp_tranceiver_t;

/*! \brief SFP status signals. */
typedef struct {
    mesa_bool_t  tx_fault;                  /**< TxFault */
    mesa_bool_t  los;                       /**< Loss Of Signal  */
    mesa_bool_t  present;                   /**< SFP module present  */
} vtss_appl_sfp_status_t;

/*! \brief SFP information which are read from the SFPs via the i2c connection. */
typedef struct {
    char  vendor_name[20];           /**< MSA vendor name (20-35) */
    char  vendor_pn[20];             /**< MSA vendor pn   (40-55) */
    char  vendor_rev[6];             /**< MSA vendor rev  (56-59) */
    char  vendor_sn[20];             /**< Serial number provided by vendor (ASCII) (68-83) */
    vtss_appl_sfp_tranceiver_t type; /**< Transceiver type (e.g. 1000BASEX) */
    vtss_appl_sfp_status_t status;   /**< Status signals from the SFP */
} vtss_appl_sfp_t;

/*! \brief MIB object status parameters which SNMP can get. */
typedef struct {
    mesa_bool_t link;                /**< Signals if port link in is up (if FALSE link is down) */
    mesa_port_speed_t speed;  /**< Current port speed */
    mesa_bool_t fdx;                 /**< Signals if port is in full duplex mode (else in half duplex) */
    mesa_bool_t fiber;               /**< Signals if interface is SFP interface (if FALSE interface is CU) */
    vtss_appl_sfp_t sfp_info; /**< Contains information about the SFP. E.g. Vendor name and version */
} vtss_appl_port_mib_status_t;

/*! \brief Status struct for the green Ethernet parameters. */
/* Port power status */
typedef struct {
    mesa_bool_t                  actiphy_capable;            /**< TRUE when port is able to use actiphy */
    mesa_bool_t                  perfectreach_capable;       /**< TRUE when port is able to use perfectreach */
    mesa_bool_t                  actiphy_power_savings;      /**< TRUE when port is saving power due to actiphy */
    mesa_bool_t                  perfectreach_power_savings; /**< TRUE when port is saving power due to perfectreach */
} vtss_appl_port_power_status_t;

/**
 * \brief Function that can check if at least one port in a stack contains a specific capability.
*/
typedef struct {
    mesa_port_status_t    status           /**< Current status parameter for the port */;
    meba_port_cap_t       cap;             /**< Port capabilities bit mask from the API (The bits are defined in board_if.h) */
    vtss_appl_sfp_t       sfp;             /**< Information about the SFP connected to the port */
    mesa_port_interface_t mac_if;          /**< The interface the port is using */
    mesa_chip_no_t        chip_no;         /**< Chip number used for targets with multiple chips */
    uint                  chip_port;       /**< The chip port number (Port number from a user point of view doesn't have to match the physical port number */
    uint32_t                   port_up_count;   /**< Port statistic counter of port up events */
    uint32_t                   port_down_count; /**< Port statistic counter of port down events */
    vtss_appl_port_power_status_t power;   /**< Current green Ethernet modes */
} vtss_appl_port_status_t;

/** \brief adv_dis flags */
#define VTSS_APPL_PORT_ADV_DIS_HDX      0x00000001   /**< Disable Half duplex */
#define VTSS_APPL_PORT_ADV_DIS_FDX      0x00000002   /**< Disable Full duplex */
#define VTSS_APPL_PORT_ADV_UP_MEP_LOOP  0x00000004   /**< Use port for UP MEP loop port */
#define VTSS_APPL_PORT_ADV_DIS_2500M    0x00000008   /**< Disable 2.5G mode */
#define VTSS_APPL_PORT_ADV_DIS_1G       0x00000010   /**< Disable 1G mode */
#define VTSS_APPL_PORT_ADV_DIS_100M     0x00000040   /**< Disable 100Mbit mode */
#define VTSS_APPL_PORT_ADV_DIS_10M      0x00000080   /**< Disable 10Mbit mode */
#define VTSS_APPL_PORT_ADV_DIS_5G       0x00000100   /**< Disable 5G mode */
#define VTSS_APPL_PORT_ADV_DIS_10G      0x00000200   /**< Disable 10G mode */
#define VTSS_APPL_PORT_ADV_DIS_SPEED    (VTSS_APPL_PORT_ADV_DIS_10M | VTSS_APPL_PORT_ADV_DIS_100M | VTSS_APPL_PORT_ADV_DIS_1G | VTSS_APPL_PORT_ADV_DIS_2500M | VTSS_APPL_PORT_ADV_DIS_5G | VTSS_APPL_PORT_ADV_DIS_10G) /**< All speed bits */
#define VTSS_APPL_PORT_ADV_DIS_DUPLEX   (VTSS_APPL_PORT_ADV_DIS_HDX | VTSS_APPL_PORT_ADV_DIS_FDX) /**< All duplex bits*/
#define VTSS_APPL_PORT_ADV_DIS_ALL      (VTSS_APPL_PORT_ADV_DIS_SPEED | VTSS_APPL_PORT_ADV_DIS_DUPLEX | VTSS_APPL_PORT_ADV_UP_MEP_LOOP) /**< All valid bits */

/** \brief Port configuration structure */
typedef struct {
    //
    meba_port_admin_state_t admin;                     /**< Admin state */
    mesa_bool_t             fdx;                       /**< Forced duplex mode */
    mesa_bool_t             flow_control;              /**< Flow control (Standard 802.3x) */
    mesa_bool_t             pfc[MESA_PRIO_ARRAY_SIZE]; /**< Priority Flow control (802.1Qbb)*/
    mesa_port_speed_t       speed;                     /**< Forced port speed */
    mesa_fiber_port_speed_t dual_media_fiber_speed;    /**< Speed for dual media fiber ports*/
    uint32_t                max_length;                /**< Max frame length */
    mesa_phy_power_mode_t   power_mode;                /**< PHY power mode */
    mesa_bool_t             exc_col_cont;              /**< Excessive collision continuation */
    uint32_t                adv_dis;                   /**< Auto neg advertisement disable */
    mesa_port_max_tags_t    max_tags;                  /**< Maximum number of tags */
    mesa_bool_t             oper_up;                   /**< Force operational state up */
    mesa_bool_t             oper_down;                 /**< Force operational state down */
    mesa_bool_t             frame_length_chk;          /**< True to do 802.3 frame length check for ethertypes below 0x0600 */
} vtss_appl_port_conf_t;

/**
 * \brief Function for getting the current port configuration
 * \param ifindex [IN]  The logical interface index/number.
 * \param conf    [OUT] Pointer to where to put the current configuration
 * \return VTSS_RC_OK if configuration get was perform correct else error code
*/
mesa_rc vtss_appl_port_conf_get(vtss_ifindex_t ifindex, vtss_appl_port_conf_t *conf);

/* Set port configuration */
/**
 * \brief Function for setting the current port configuration
 * \param ifindex [IN]  The logical interface index/number.
 * \param conf    [OUT] Pointer to new configuration
 * \return VTSS_RC_OK if configuration set was perform correct else error code
*/
mesa_rc vtss_appl_port_conf_set(vtss_ifindex_t ifindex, vtss_appl_port_conf_t *conf);

/**
 * \brief Function for getting the current port status
 * \param ifindex [IN]  The logical interface index/number.
 * \param status  [OUT] Pointer to where to put the port status
 * \return VTSS_RC_OK if status get was perform correct else error code
*/
mesa_rc vtss_appl_port_status_get(vtss_ifindex_t ifindex, vtss_appl_port_status_t *status);

/**
 * \brief Function clearing statistic counter for a specific port
 * \param ifindex [IN]  The logical interface index/number.
 * \return VTSS_RC_OK if counter was cleared else error code
*/
mesa_rc vtss_appl_port_counters_clear(vtss_ifindex_t ifindex);

/**
 * \brief Function for getting statistic counter for a specific port
 * \param ifindex [IN]  The logical interface index/number.
 * \param counters [OUT] Pointer to where to put the counters
 * \return VTSS_RC_OK if counters are valid else error code
*/
mesa_rc vtss_appl_port_counters_get(vtss_ifindex_t ifindex, mesa_port_counters_t *counters);

#ifdef __cplusplus
}
#endif

#endif  /* _VTSS_APPL_PORT_H_ */
