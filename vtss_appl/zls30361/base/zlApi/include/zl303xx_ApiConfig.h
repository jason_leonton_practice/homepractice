

/*******************************************************************************
*
*  $Id: zl303xx_ApiConfig.h 14468 2016-11-11 19:10:35Z FL $
*  Copyright 2006-2016 Microsemi Semiconductor Limited.
*  All rights reserved.
*
*  Module Description:
*     Functions for setting and getting the user configurable parameters.
*
*******************************************************************************/

#ifndef ZL303XX_APICONFIG_H_
#define ZL303XX_APICONFIG_H_

#ifdef __cplusplus
extern "C" {
#endif

/*****************   INCLUDE FILES   ******************************************/
#include "zl303xx_Global.h"
#include "zl303xx_Error.h"
#include "zl303xx.h"
#include "zl303xx_Init.h"


#if defined APR_INCLUDED
#include "zl303xx_Apr.h"
#endif


/*****************   DATA TYPES   *********************************************/

/*****************   DATA STRUCTURES   ****************************************/

/*****************   EXPORTED GLOBAL VARIABLE DECLARATIONS   ******************/

/*****************   EXPORTED FUNCTION DEFINITIONS   ***************************/











#if defined ZLS30341_INCLUDED
/* System interrupt config. */
zlStatusE zl303xx_SetLog2SysInterruptPeriod(Uint32T log2Period);
Uint32T zl303xx_GetLog2SysInterruptPeriod(void);
#endif


#ifdef OS_LINUX
zlStatusE zl303xx_SetCliPriority(Uint8T taskPriority);
Uint8T zl303xx_GetCliPriority(void);
#endif  /* OS_LINUX */

#ifdef __cplusplus
}
#endif

#endif /*ZL303XX_APICONFIG_H_*/
