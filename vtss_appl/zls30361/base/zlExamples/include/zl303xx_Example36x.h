

/*******************************************************************************
 *
 *  $Id: zl303xx_Example36x.h 14633 2016-12-07 20:39:29Z FL $
 *
 *  Copyright 2006-2016 Microsemi Semiconductor Limited.
 *  All rights reserved.
 *
 *  Module Description:
 *     Supporting interfaces for the 36x examples
 *
 ******************************************************************************/

#ifndef _ZL303XX_EXAMPLE_36X_H_
#define _ZL303XX_EXAMPLE_36X_H_

#ifdef __cplusplus
extern "C" {
#endif

/*****************   INCLUDE FILES   ******************************************/
#include "zl303xx_Global.h"
#include "zl303xx_Error.h"
#include "zl303xx_DeviceSpec.h"

/*****************   DEFINES   ************************************************/

/*****************   DATA TYPES   *********************************************/

/*****************   DATA STRUCTURES   ****************************************/

typedef struct
{
   zl303xx_DeviceModeE deviceMode;
   Uint32T pllId;
   zl303xx_ParamsS *zl303xx_Params;

} example36xClockCreateS;


/*****************   EXPORTED GLOBAL VARIABLE DECLARATIONS   ******************/

/*****************   EXTERNAL FUNCTION DECLARATIONS   *************************/

zlStatusE example36xEnvInit(void);
zlStatusE example36xEnvClose(void);
zlStatusE example36xSlaveWarmStart(void);

zlStatusE example36xClockCreateStructInit(example36xClockCreateS *pClock);
zlStatusE example36xClockCreate(example36xClockCreateS *pClock);
zlStatusE example36xClockRemove(example36xClockCreateS *pClock);

zlStatusE example36xLoadConfigFile(zl303xx_ParamsS *zl303xx_Params, const char *filename);
zlStatusE example36xLoadConfigDefaults(zl303xx_ParamsS *zl303xx_Params);
zlStatusE example36xReset1588HostRegisters(zl303xx_ParamsS *zl303xx_Params);

zlStatusE example36xStickyLockCallout(void *hwParams, zl303xx_DpllIdE pllId, zl303xx_BooleanE lockFlag);

#if defined _ZL303XX_ZLE30360_BOARD || defined _ZL303XX_ZLE1588_BOARD
zlStatusE example36xCheckPhyTSClockFreq(zl303xx_ParamsS *zl303xx_Params);
#endif

#ifdef __cplusplus
}
#endif

#endif /* MULTIPLE INCLUDE BARRIER */
