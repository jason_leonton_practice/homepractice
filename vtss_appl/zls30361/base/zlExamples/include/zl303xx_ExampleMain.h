



/*******************************************************************************
*
*  $Id: zl303xx_ExampleMain.h 14489 2016-11-15 16:33:04Z SW $
*
*  Copyright 2006-2016 Microsemi Semiconductor Limited.
*  All rights reserved.
*
*  Module Description:
*     This file contains generic functions to configure, start, and stop an
*     example timing application consisting of one or more software modules. The
*     application structure is broken down into 3 major subcomponents:
*
*     - Clock: A hardware device capable generating a timing signal used for time
*       stamping.
*     - Port: A logical point of access to a network carrying timing traffic.
*     - Stream: A uni- or bi-directional connection between two endpoints in the
*       timing network (e.g., server to client).
*
*******************************************************************************/

#ifndef _ZL303XX_EXAMPLE_MAIN_H_
#define _ZL303XX_EXAMPLE_MAIN_H_

#ifdef __cplusplus
extern "C" {
#endif


/*****************   INCLUDE FILES   ******************************************/
#include "zl303xx_Global.h"
#include "zl303xx_Error.h"
#include "zl303xx_DeviceSpec.h"

#if defined APR_INCLUDED
#include "zl303xx_ExampleApr.h"
#endif

#if defined ZLS30341_INCLUDED
#include "zl303xx_Example34x.h"
#endif

#if defined ZLS30361_INCLUDED
#include "zl303xx_Example36x.h"
#endif

#if defined ZLS3072X_INCLUDED || defined ZLS30721_INCLUDED
#include "zl303xx_Example72x.h"
#endif

#if defined ZLS3070X_INCLUDED || defined ZLS30701_INCLUDED
#include "zl303xx_Example70x.h"
#endif

#if defined ZLS3075X_INCLUDED || defined ZLS30751_INCLUDED
#include "zl303xx_Example75x.h"
#endif

#if defined ZLS30390_INCLUDED || defined ZLS30330_INCLUDED
#include "zl303xx_ExamplePtp.h"
#endif

/*****************   DEFINES   ************************************************/

/*****************   DATA TYPES   *********************************************/

/*****************   DATA STRUCTURES   ****************************************/

typedef struct
{
#if defined APR_INCLUDED
   exampleAprStreamCreateS apr;
#endif

#if defined ZLS30390_INCLUDED || defined ZLS30330_INCLUDED
   examplePtpStreamCreateS ptp;
#endif

   zl303xx_BooleanE started;
} exampleAppStreamS;

typedef struct
{
#if defined ZLS30390_INCLUDED || defined ZLS30330_INCLUDED
   examplePtpPortCreateS ptp;
#endif

   zl303xx_BooleanE started;
} exampleAppPortS;

typedef struct
{
#if defined APR_INCLUDED
    exampleAprClockCreateS apr;
#endif

#if defined ZLS30341_INCLUDED
   example34xClockCreateS zl3034x;
#endif

#if defined ZLS30361_INCLUDED
   example36xClockCreateS zl3036x;
#endif

#if defined ZLS3072X_INCLUDED || defined ZLS30721_INCLUDED
   example72xClockCreateS zl3072x;
#endif

#if defined ZLS3070X_INCLUDED || defined ZLS30701_INCLUDED
   example70xClockCreateS zl3070x;
#endif

#if defined ZLS3075X_INCLUDED || defined ZLS30751_INCLUDED
   example75xClockCreateS zl3075x;
#endif

#if defined ZLS30390_INCLUDED || defined ZLS30330_INCLUDED
   examplePtpClockCreateS ptp;
#endif

   zl303xx_BooleanE pktRef;  /* Set FALSE for master, TRUE for slave/BC */

   zl303xx_BooleanE started;
} exampleAppClockS;

typedef struct
{
   exampleAppClockS  *clock;
   Uint32T            clockCount;

   exampleAppPortS   *port;
   Uint32T            portCount;

   exampleAppStreamS *stream;
   Uint32T            streamCount;
   Uint32T            srvId;
} exampleAppS;

/*****************   EXPORTED GLOBAL VARIABLE DECLARATIONS   ******************/

extern exampleAppS zlExampleApp;

extern zl303xx_ParamsS *zl303xx_Params0;
extern zl303xx_ParamsS *zl303xx_Params1;

extern Uint8T TARGET_DPLL;
extern Uint8T SYNCE_DPLL;
extern Uint8T CHASSIS_DPLL;

/*****************   EXTERNAL FUNCTION DECLARATIONS   *************************/

#ifdef ZLS30330_INCLUDED
/* G8275.2 */
zlStatusE examplePtpTelecomPhaseG8275p2Master(void);
zlStatusE examplePtpTelecomPhaseG8275p2Bc(void);
zlStatusE examplePtpTelecomPhaseG8275p2Slave(void);

zlStatusE examplePtpTelecomPhaseMasterIPv4(void);
zlStatusE examplePtpTelecomPhaseBcIPv4(void);
zlStatusE examplePtpTelecomPhaseSlaveIPv4(void);
zlStatusE examplePtpTelecomPhaseBcSlaveIPv4(void);
#endif

#ifdef ZLS30390_INCLUDED
/* Example PTP configurations */
zlStatusE examplePtpMultiMaster(void);
zlStatusE examplePtpMultiSlave(void);

zlStatusE examplePtpUniNegMaster(void);
zlStatusE examplePtpUniNegSlave(void);

zlStatusE examplePtpUniNegSlaveHybrid(void);

zlStatusE examplePtpUniNegMaster2(void);
zlStatusE examplePtpUniNegMaster3(void);
zlStatusE examplePtpUniNegMaster4(void);
zlStatusE examplePtpUniNegSlave_4Streams(void);

zlStatusE examplePtpMultiBC(void);
zlStatusE examplePtpMultiBCSlave(void);

/* G8275.1 (edition 1) */
zlStatusE examplePtpTelecomPhaseG8275p1Master(void);
zlStatusE examplePtpTelecomPhaseG8275p1Bc(void);
zlStatusE examplePtpTelecomPhaseG8275p1BcSlave(void);
zlStatusE examplePtpTelecomPhaseG8275p1Slave(void);
zlStatusE examplePtpTelecomPhaseG8275VirtualPortAdd(zl303xx_PtpClockHandleT clockHandle);

/* G8275.1 (edition 2) */
zlStatusE examplePtpTelecomPhaseG8275p1v2Master(void);
zlStatusE examplePtpTelecomPhaseG8275p1v2Bc(void);
zlStatusE examplePtpTelecomPhaseG8275p1v2BcSlave(void);
zlStatusE examplePtpTelecomPhaseG8275p1v2Slave(void);

/* G8275.2 */
zlStatusE examplePtpTelecomPhaseG8275p2Master(void);
zlStatusE examplePtpTelecomPhaseG8275p2Bc(void);
zlStatusE examplePtpTelecomPhaseG8275p2Slave(void);

zlStatusE examplePtpTelecomPhaseMasterIPv4(void);
zlStatusE examplePtpTelecomPhaseBcIPv4(void);
zlStatusE examplePtpTelecomPhaseSlaveIPv4(void);
zlStatusE examplePtpTelecomPhaseBcSlaveIPv4(void);

zlStatusE examplePtpPowerProfileMaster(void);
zlStatusE examplePtpPowerProfileSlave(void);

zlStatusE examplePtpPeerDelayMaster(void);
zlStatusE examplePtpPeerDelaySlave(void);

void setExampleMulticastIpDest(char const *destIP);
void setExampleUniIpSlaveDest(char const *destIP);
zlStatusE exampleConfig(zl303xx_BooleanE isUnicast, zl303xx_BooleanE slaveCapable, zl303xx_PtpVersionE ptpVersion);
zlStatusE examplePtpPortAdd(Uint16T portNumber, Uint8T const *localAddr);
void setExampleIpSrc(const char *srcIp);

zlStatusE examplePtpTelecomMaster(void);
zlStatusE examplePtpTelecomMaster2(void);
zlStatusE examplePtpTelecomMaster3(void);
zlStatusE examplePtpTelecomMaster4(void);
zlStatusE examplePtpTelecomMaster5(void);
zlStatusE examplePtpTelecomMaster6(void);
zlStatusE examplePtpTelecomMaster7(void);
zlStatusE examplePtpTelecomMaster8(void);
zlStatusE examplePtpTelecomMaster9(void);

zlStatusE examplePtpTelecomSlave(void);

zlStatusE examplePtpTelecomMultiSlave(void);
zlStatusE examplePtpTelecomMultiSlave2(void);
zlStatusE examplePtpTelecomMultiSlave3(void);
zlStatusE examplePtpTelecomMultiMaster(void);

zlStatusE examplePtpTelecomBCMaster(void);
zlStatusE examplePtpTelecomBC(void);
zlStatusE examplePtpTelecomBCSlave(void);

zlStatusE examplePtpTwoClock(void);
zlStatusE examplePtpTwoClockMaster(void);
zlStatusE examplePtpTwoClockSlave(void);
#endif

#ifdef ZLS3038X_INCLUDED
zlStatusE exampleAprMain(void);
zlStatusE exampleAprHybrid(void);
#endif


#if defined ZLS30341_INCLUDED
#if defined ZLS30341_INCLUDED
zlStatusE example341Start(void);
#else
zlStatusE example34xSlave(void);
zlStatusE example34xSlaveHybrid(void);
#endif
#if defined APR_INCLUDED
zlStatusE example34xSlaveHybrid(void);
zlStatusE example34xSlaveWarmStart(void);
zlStatusE example34xMaster(void);
#endif
#endif


#ifdef ZLS30361_INCLUDED
zlStatusE example36x(void);
#endif

#ifdef ZLS30721_INCLUDED
zlStatusE example72x(void);
#endif
#ifdef ZLS3072X_INCLUDED
zlStatusE example72xSlave(void);
zlStatusE example72xSlaveHybrid(void);
zlStatusE example72xSlaveElectrical(void);
zlStatusE example72xMaster(void);
#endif

#ifdef ZLS30701_INCLUDED
zlStatusE example70x(void);
#endif
#ifdef ZLS3070X_INCLUDED
zlStatusE example70xSlave(void);
zlStatusE example70xSlaveHybrid(void);
zlStatusE example70xSlaveElectrical(void);
zlStatusE example70xMaster(void);
#endif

#ifdef ZLS30751_INCLUDED
zlStatusE example75x(void);
#endif
#ifdef ZLS3075X_INCLUDED
zlStatusE example75xSlave(void);
zlStatusE example75xSlaveHybrid(void);
zlStatusE example75xSlaveElectrical(void);
zlStatusE example75xMaster(void);
#endif


/* Shutdown function */
zlStatusE exampleShutdown(void);

/* Application Management */
zlStatusE exampleEnvInit(void);
zlStatusE exampleEnvClose(void);
zlStatusE exampleAppStructInit(Uint32T numClocks,  Uint32T numPorts,
                               Uint32T numStreams, exampleAppS *pApp);
zlStatusE exampleAppStructFree(exampleAppS *pApp);
zlStatusE exampleAppStart(exampleAppS *pApp);
zlStatusE exampleAppStartClock(exampleAppS *pApp, Uint32T indx);
zlStatusE exampleAppStartPort(exampleAppS *pApp, Uint32T indx);
zlStatusE exampleAppStartStream(exampleAppS *pApp, Uint32T indx);
zlStatusE exampleAppStop(exampleAppS *pApp);
zl303xx_BooleanE exampleAppIsRunning(void);

#ifdef __cplusplus
}
#endif

#endif /* MULTIPLE INCLUDE BARRIER */
