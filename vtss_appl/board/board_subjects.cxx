/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include <vtss/basics/notifications.hxx>
#include <vtss/basics/array.hxx>
#include "vtss/basics/notifications/event-handler.hxx"

#include "board_subjects.hxx"
#include "port_api.h"  // For port_phy_wait_until_ready
#include "interrupt.h" /* For VTSS_TRACE_MODULE_ID && TRACE_GRP_IRQ */

static int __irq_fd;

using namespace vtss::notifications;

namespace vtss {
namespace notifications {
notifications::SubjectRunner subject_irq_thread("IRQ Thread", "IRQ Thread Events");
} // namespace notifications


vtss::Array<BoardIrq*, MESA_IRQ_MAX> irq_list;

mesa_rc meba_irq_handler_(mesa_irq_t chip_irq,
                          meba_event_signal_t signal_notifier)
{
    return meba_irq_handler(board_instance, chip_irq, signal_notifier);
}

// Class for handling an event on the irq_fd file descriptor
struct IrqFdEventHandler : public EventHandler {
    IrqFdEventHandler(int irq_fd) : EventHandler(&subject_irq_thread), irq_event_fd(this) {
        T_DG(TRACE_GRP_IRQ, "irq_fd = %d", irq_fd);

        // After this assignment, execute() gets called on every event on irq_fd.
        irq_event_fd.assign(Fd(irq_fd));
        subject_irq_thread.event_fd_add(irq_event_fd, EventFd::READ);
    }

    void initialize() {
        const int enable = 1;

        T_D("Wait for PHY init");
        port_phy_wait_until_ready();

        // Reset point
        meba_reset(board_instance, MEBA_INTERRUPT_INITIALIZE);

        // Detect which IRQ are handled by MEBA
        for (auto irq = (mesa_irq_t) 0; irq < MESA_IRQ_MAX; irq++) {
            if (meba_irq_requested(board_instance, irq) == MESA_RC_OK) {
                T_DG(TRACE_GRP_IRQ, "MEBA handles IRQ %d", irq);
                vtss_interrupt_handler_add(VTSS_MODULE_ID_MEBA, irq, meba_irq_handler_);
            }
        }

        if (write(irq_event_fd.raw(), &enable, sizeof(enable)) != sizeof(enable)) {
            T_EG(TRACE_GRP_IRQ, "Unable to enable IRQs: %s", strerror(errno));
        }
    }

    // Callback when something happens on the IRQ file descriptor
    void execute(EventFd *e) override {
        mesa_irq_status_t status;
        int               irq_seq_num;
        const int         enable = 1;

        if (!(e->events() & EventFd::READ)) {
            T_EG(TRACE_GRP_IRQ, "Unexpected event");
            return;
        }

        if (read(e->raw(), &irq_seq_num, sizeof(irq_seq_num)) != sizeof(irq_seq_num)) {
            T_EG(TRACE_GRP_IRQ, "Read IRQ error: %s", strerror(errno));
            return;
        }

        if (meba_irq_status_get_and_mask(board_instance, &status) != VTSS_RC_OK) {
            if (mesa_irq_status_get_and_mask(NULL, &status) != VTSS_RC_OK) {
                T_EG(TRACE_GRP_IRQ, "mesa_irq_status_get_and_mask() failed");
                return;
            }
        }

        // The status.raw_XXX fields are read directly from the chip, whereas the status.active contains the
        // chip-specific interrupts translated into something platform-independent (VTSS_IRQ_xxx)
        T_DG(TRACE_GRP_IRQ, "IRQ seq. #%d: active = 0x%08x, raw_ident = 0x%08x, raw_status = 0x%08x, raw_mask = 0x%08x", irq_seq_num, status.active, status.raw_ident, status.raw_status, status.raw_mask);
        while(status.active) {
            mesa_irq_t irq = static_cast<mesa_irq_t>(__builtin_ctz(status.active));
            if (irq < irq_list.size()) {
                auto e = irq_list[irq];
                if (e) {
                    e->tally();
                    T_WG(TRACE_GRP_IRQ, "IRQ %d (%s), cnt %d", irq, e->name(), e->cnt());
                    if (vtss_interrupt_handler(irq, interrupt_source_signal) == VTSS_RC_OK) {
                        interrupt_irq_enable(irq, true);
                   } else {
                        T_WG(TRACE_GRP_IRQ, "Unhandled IRQ %d (%s), not re-enabling", irq, e->name());
                    }
                } else {
                    T_EG(TRACE_GRP_IRQ, "Got IRQ %d which is not supported by board?", irq);
                }
                status.active &= ~(VTSS_BIT(irq));    // Clear this
            } else {
                T_EG(TRACE_GRP_IRQ, "Got bougs IRQ %d", irq);
            }
        }

        // Enable IRQ's again
        if (write(e->raw(), &enable, sizeof(enable)) != sizeof(enable)) {
            T_EG(TRACE_GRP_IRQ, "write() failed. Unable to enable IRQs");
        }

        subject_irq_thread.event_fd_add(irq_event_fd, EventFd::READ);
    }

private:
    EventFd irq_event_fd;
};

static void subject_irq_thread_run(vtss_addrword_t data)
{
    static struct IrqFdEventHandler irq_fd_event_handler(__irq_fd);
    // Gotta wait until the PHYs are initialized until
    // we start generating interrupts towards the application.
    T_D("Initialize MEBA interrupts");
    irq_fd_event_handler.initialize();
    T_D("IRQ thread starting");
    subject_irq_thread.run();
}

void board_subjects_start(int irq_fd)
{
    vtss_handle_t thread_handle;
    __irq_fd = irq_fd;
    vtss_thread_create(VTSS_THREAD_PRIO_HIGHER,
                       subject_irq_thread_run,
                       NULL,
                       const_cast<char *>(subject_irq_thread.name),
                       nullptr,
                       0,
                       &thread_handle,
                       NULL);
}

} // namespace vtss
