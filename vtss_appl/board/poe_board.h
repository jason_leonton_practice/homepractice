/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/


#ifndef _VTSS_POE_BOARD_H_
#define _VTSS_POE_BOARD_H_

//#define VTSS_HW_CUSTOM_POE_CHIP_SI3452_SUPPORT  1  // Remove if autodetection of SI3452 should be disabled
//#define VTSS_HW_CUSTOM_POE_CHIP_SLUS787_SUPPORT 1  // Remove if autodetection of SLUS787 should be disabled
#if 0
#define VTSS_HW_CUSTOM_POE_CHIP_PD69200_SUPPORT 1  // Remove if autodetection of PD69200 should be disabled
#else  // Leonton uses the chip
#define VTSS_HW_CUSTOM_POE_CHIP_LTC4271_SUPPORT 1  // Remove if autodetection of LTC4271 should be disabled
#endif

poe_custom_entry_t poe_custom_get_hw_config(mesa_port_no_t port_idx, poe_custom_entry_t *hw_conf);
void poe_board_hw_enable(void);
#endif // _VTSS_POE_BOARD_H_
/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
