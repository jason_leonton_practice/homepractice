/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
**********************************************************************
*/

#include "conf_api.h"  // For conf_mgmt_pwrbgt_get
//**********************************************************************
//  This file contains functions and defines that are board specific
//**********************************************************************
#include "poe_custom_api.h"

/****************************************************************************/
/*  Hardware configuration for Microsemi boards.                            */
/****************************************************************************/
// Gets the hardware board configuration for PoE
//
// In : port_idx - The port index (starting from 0) for which you know the board configuration.
//
// In/Out : hw_conf - The hardware board configuration.
//
// Return : The hardware board configuration. (same as hw_conf)
poe_custom_entry_t poe_custom_get_hw_config(mesa_port_no_t port_idx, poe_custom_entry_t *hw_conf)
{
    lntn_global_conf_t lntn_conf = lntn_conf_get();
    lntn_port_conf_t *port_conf = &lntn_conf.port_conf[port_idx];

    hw_conf->pse_pairs_control_ability = 0; // 0 = hardware has fixed PSE pinout, 1 = Hardware PSE pin is configurable. See pethPsePortPowerPairsControlAbility, rfc 3621
    hw_conf->pse_power_pair    = 1; // 1 = Signal pins pinout, 2 =  Spare pins pinout, ee pethPsePortPowerPairs, rfc 3621

    // Set default values
    hw_conf->available = false;

    strcpy(hw_conf->i2c_device_name[SI3452], "");
    strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
    strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_0");

#if 1
#define LTC4271_BASE_ADDR (0x6C)
    if(port_conf_poe_check(port_conf)) {
        char i2c_dev_name[16];
        hw_conf->available = TRUE;

        // The ltc4271_%d and LTC4271_BASE_ADDR should sync with meba/board-driver.c
        // ltc4271_0: 0x6c, ltc4271_1: 0x6d, ltc4271_2: 0x6e
        snprintf(i2c_dev_name, sizeof(i2c_dev_name), "ltc4271_%d", port_conf->poe.i2c_addr - LTC4271_BASE_ADDR);
        strcpy(hw_conf->i2c_device_name[LTC4271], i2c_dev_name);
    }
#else
    // Define which ports that supports PoE/PoE+.
    switch (port_idx) {
    case 0:
        hw_conf->available = TRUE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_1");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_1");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_1");
        break;
    case 1:
        hw_conf->available = TRUE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_1");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_1");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_1");
        break;

    case 2:
        hw_conf->available = TRUE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_1");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_1");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_1");
        break;

    case 3:
        hw_conf->available = TRUE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_1");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_1");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_1");
        break;

    case 4:
        hw_conf->available = TRUE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_2");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_2");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 5:
        hw_conf->available = TRUE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_2");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_2");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 6:
        hw_conf->available = TRUE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_2");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_2");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 7:
        hw_conf->available = TRUE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_2");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_2");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 8:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_3");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_3");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 9:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_3");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_3");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 10:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_3");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_3");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 11:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_3");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_3");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 12:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_4");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_4");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 13:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_4");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_4");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 14:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_4");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_4");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 15:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_4");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_4");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 16:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_5");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_5");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 17:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_5");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_5");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 18:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_5");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_5");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 19:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_5");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_5");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 20:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_6");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_6");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 21:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_6");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_6");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 22:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_6");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_6");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 23:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_6");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_6");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 24:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_7");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_7");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 25:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_7");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_7");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 26:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_7");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_7");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 27:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_7");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_7");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 28:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_8");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_8");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 29:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_8");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_8");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;


    case 30:
        hw_conf->available = FALSE;
        //hw_conf->i2c_device[SI3452]  = si3452_8_dev;
        //hw_conf->i2c_device[SLUS787] = slus787_8_dev;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_8");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_8");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 31:
        hw_conf->available = FALSE;
        //hw_conf->i2c_device[SI3452]  = si3452_8_dev;
        //hw_conf->i2c_device[SLUS787] = slus787_8_dev;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_8");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_8");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 32:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_9");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_9");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 33:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_9");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_9");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 34:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_9");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_9");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 35:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_9");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_9");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 36:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_10");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_10");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 37:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_10");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_10");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 38:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_10");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_10");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 39:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_10");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_10");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 40:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_11");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_11");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 41:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_11");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_11");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 42:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_11");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_11");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 43:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_11");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_11");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 44:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_12");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_12");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 45:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_12");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_12");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 46:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_12");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_12");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    case 47:
        hw_conf->available = FALSE;
        strcpy(hw_conf->i2c_device_name[SI3452], "si3452_12");
        strcpy(hw_conf->i2c_device_name[SLUS787], "slus787_12");
        strcpy(hw_conf->i2c_device_name[PD69200], "pd69x00-2");
        strcpy(hw_conf->i2c_device_name[LTC4271], "ltc4271_2");
        break;

    default:
        break;
    }
#endif // if 1

    return *hw_conf;
}

// Returning the maximum power the power supply can deliver.
u32 vtss_appl_poe_supply_max_get(void)
{
#if defined(BOARD_RIO_GRANDE)
    switch (vtss_board_type()) {
    case VTSS_BOARD_RIO_GRANDE_4T1T_A:
    case VTSS_BOARD_RIO_GRANDE_4T1T:
    case VTSS_BOARD_RIO_GRANDE_6T2T_A:
    case VTSS_BOARD_RIO_GRANDE_6T2T:
        /* No POE */
        break;
    case VTSS_BOARD_RIO_GRANDE_4P2S_A:
    case VTSS_BOARD_RIO_GRANDE_4P2S:
        return 120;
        break;
    case VTSS_BOARD_RIO_GRANDE_8P2S_A:
    case VTSS_BOARD_RIO_GRANDE_8P2S:
        return 180;
        break;
    default:
        /* Unknown board */
        break;
    }
#endif /* BOARD_RIO_GRANDE */
    return VTSS_APPL_POE_POWER_SUPPLY_MAX;
}

u32 vtss_appl_poe_supply_default_get(void)
{
#if 1  // 20180413 Harvey modify, change to default power budget
    int pwrbgt = 0;

    if (conf_mgmt_pwrbgt_get(&pwrbgt) == 0) {
        return pwrbgt;
    } else {
        return VTSS_APPL_POE_POWER_SUPPLY_MAX;
    }
#else
#if defined(BOARD_RIO_GRANDE)
    switch (vtss_board_type()) {
    case VTSS_BOARD_RIO_GRANDE_4T1T_A:
    case VTSS_BOARD_RIO_GRANDE_4T1T:
    case VTSS_BOARD_RIO_GRANDE_6T2T_A:
    case VTSS_BOARD_RIO_GRANDE_6T2T:
        /* No POE */
        break;
    case VTSS_BOARD_RIO_GRANDE_4P2S_A:
    case VTSS_BOARD_RIO_GRANDE_4P2S:
        return 120;
        break;
    case VTSS_BOARD_RIO_GRANDE_8P2S_A:
    case VTSS_BOARD_RIO_GRANDE_8P2S:
        return 160;
        break;
    default:
        /* Unknown board */
        break;
    }
#endif /* BOARD_RIO_GRANDE */
    return VTSS_APPL_POE_POWER_SUPPLY_MAX;
#endif
}

// If anything hardware related needs to be done to enable PoE
// do it in this function.
void poe_board_hw_enable(void) {
#if defined(BOARD_RIO_GRANDE)
    vtss_appl_api_lock_unlock api_lock(__FILE__, __LINE__);       /* Protect SGIO conf */
    switch (vtss_board_type()) {
    case VTSS_BOARD_RIO_GRANDE_4T1T_A:
    case VTSS_BOARD_RIO_GRANDE_4T1T:
    case VTSS_BOARD_RIO_GRANDE_6T2T_A:
    case VTSS_BOARD_RIO_GRANDE_6T2T:
        /* No POE */

        break;
    case VTSS_BOARD_RIO_GRANDE_4P2S_A:
    case VTSS_BOARD_RIO_GRANDE_4P2S:
    case VTSS_BOARD_RIO_GRANDE_8P2S_A:
    case VTSS_BOARD_RIO_GRANDE_8P2S:
        // Turn PoE on (PD6900_xDIS above)
        mesa_sgpio_conf_t conf;
        if (mesa_sgpio_conf_get(NULL, 0, 0, &conf) != VTSS_RC_OK) {
            T_E("Could not get SGPIO conf");
            return;
        }

        conf.port_conf[29].enabled = TRUE;
        conf.port_conf[29].mode[1] = MESA_SGPIO_MODE_ON; /* PoE ON */

        if (mesa_sgpio_conf_set(NULL, 0, 0, &conf) != VTSS_RC_OK) {
            T_E("Could not set SGPIO conf");
        }

        break;
    default:
        /* Unknown board */
        break;
    }
#endif /* BOARD_RIO_GRANDE */
}


// Returns Minimum power required for the power supply
u32 vtss_appl_poe_supply_min_get(void)
{
    return VTSS_APPL_POE_POWER_SUPPLY_MIN;
}
