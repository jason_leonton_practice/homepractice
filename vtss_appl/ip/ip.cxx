/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

// Thread conventions:
// IP2_*() static functions must not acquire lock
// IP2_*() may call IP2_*()
// IP2_*() may NOT call vtss_ip2_*()

// vtss_ip2_*() public functions may acquire lock
// vtss_ip2_*() may call IP2_*() in LOCKED sections
// vtss_ip2_*() may call vtss_ip2_* in UNLOCKED sections
// vtss_ip2_*() may NOT call vtss_ip2_* in LOCKED sections

#include <stdlib.h>
#include "ip_api.h"
#include "ip_priv.h"
#include "ip_utils.h"
#include "ip_os_api.h"
#include "ip_chip_api.h"
#include "ip_trace.h"
#include "ip_expose.hxx"
#include "ip_filter_api.hxx"
#ifdef VTSS_SW_OPTION_IP_MISC
#include "ip_misc_api.h"
#endif
#include "vtss/basics/types.hxx"
#ifdef VTSS_SW_OPTION_ICLI
#include "ip_icli_priv.h"
#endif
#ifdef VTSS_SW_OPTION_SNMP
#include "ip_snmp.h"
#endif
#ifdef VTSS_SW_OPTION_DNS
#include "ip_dns_api.h"
#endif /* VTSS_SW_OPTION_DNS */
#ifdef VTSS_SW_OPTION_SYSLOG
#include "syslog_api.h"
#endif /* VTSS_SW_OPTION_SYSLOG */
#ifdef VTSS_SW_OPTION_LLDP
#include "lldp_api.h"
#endif /* VTSS_SW_OPTION_LLDP */
#include "packet_api.h"
#include "l2proto_api.h"        /* For L2 port state */
#include "vtss_trace_api.h"
#include "conf_api.h"
#include "critd_api.h"
#include "misc_api.h"           /* misc_ipv6_txt() */
#include "vtss_intrusive_list.h"
#include "dhcp_client_api.h"
#include "vlan_api.h"
#include "vtss/basics/trace.hxx"
#include "vtss/basics/memory.hxx"
#include "vtss/basics/expose/table-status.hxx"

#include "subject.hxx"
#include "vtss/basics/notifications/event.hxx"
#include "vtss/basics/notifications/event-handler.hxx"
#if defined(VTSS_SW_OPTION_FRR)
#include "frr_access.hxx"
#endif /* VTSS_SW_OPTION_FRR */

using namespace vtss;

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_IP
/*
 * Semaphore stuff.
 */
critd_t ip2_crit;

static vtss_trace_reg_t trace_reg = {
    VTSS_MODULE_ID_IP, "ip", "IP"
};

static vtss_trace_grp_t trace_grps[VTSS_TRACE_IP_GRP_CNT] = {
    /* VTSS_TRACE_IP_GRP_DEFAULT */ {
        "default",
        "Default",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_RXPKT_DUMP */ {
        "rxpktdump",
        "Dump of received packets (lvl = noise)",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_TXPKT_DUMP */ {
        "txpktdump",
        "Dump of transmitted packets (lvl = noise)",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_IP_GRP_CRIT */ {
        "crit",
        "Critical regions ",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_IP_GRP_CHIP */ {
        "chip",
        "Chip layer",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_IP_GRP_OS */ {
        "os",
        "OS layer",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_IP_GRP_OS_ROUTER_SOCKET */ {
        "os-rt",
        "OS router socket",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_IP_GRP_OS_DRIVER */ {
        "os-drv",
        "OS driver",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP | VTSS_TRACE_FLAGS_RINGBUF | VTSS_TRACE_FLAGS_IRQ
    },
    /* VTSS_TRACE_IP_GRP_SNMP */ {
        "snmp",
        "SNMP helper",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_IP_GRP_NETLINK */ {
        "netlink",
        "Netlink",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
};

struct Lock {
    Lock(int line) { critd_enter(&ip2_crit, VTSS_TRACE_IP_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, line); }
    ~Lock() { critd_exit(&ip2_crit, VTSS_TRACE_IP_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, 0); }
};
#define IP2_CRIT_ASSERT_LOCKED() critd_assert_locked(&ip2_crit, TRACE_GRP_CRIT, __FILE__, __LINE__)
#define CRIT_SCOPE() Lock __lock_guard__(__LINE__)

#define IP_PUBLIC_API_FUNCTION(X, ...)                                         \
    CRIT_SCOPE();                                                              \
    return (X(__VA_ARGS__))

typedef struct {
    mesa_vid_t vid;
    ip_vif_fwd_t fwd;
} iface_t;

typedef struct {
    CapArray<iface_t, VTSS_APPL_CAP_IP_INTERFACE_CNT> iface;
} fwd_state_t;

typedef struct {
    u32 size;
    vtss_appl_ip_if_status_t status[IP_MAX_STATUS_OBJS];
} if_status_t;

#define MAX_SUBSCRIBER (32)
static vtss_ip_if_callback_t IP2_subscribers[MAX_SUBSCRIBER];
static u32 IP2_vlan_notify_bf[VTSS_BF_SIZE(VTSS_VIDS)];
static u8 IP2_if_MO_flg_update[VTSS_BF_SIZE(VTSS_VIDS)];

vtss::expose::TableStatus<
    expose::ParamKey<vtss_ifindex_t>,
    expose::ParamVal<vtss_appl_ip_if_status_dhcp4c_t *>
> status_if_dhcp;

typedef vtss::expose::TableStatus<
    expose::ParamKey<mesa_routing_entry_t>,
    expose::ParamVal<int>
> IP_conf_route_t;
IP_conf_route_t IP_conf_route_ipv6;
IP_conf_route_t IP_conf_route_ipv4;

StatusAcdIpv4 status_acd_ipv4;

class IpChip *IpChip;

static mesa_rc conv(const mesa_routing_entry_t     *const a,
                    vtss_appl_ip_ipv6_route_conf_t *const b) {
    if (a->type != MESA_ROUTING_ENTRY_TYPE_IPV6_UC) {
        return VTSS_RC_ERROR;
    }

    memcpy(&b->route, &a->route.ipv6_uc, sizeof(b->route));
    if (a->vlan != 0) {
        VTSS_RC(vtss_ifindex_from_vlan(a->vlan, &b->interface));
    } else {
        b->interface = VTSS_IFINDEX_NONE;
    }

    return VTSS_RC_OK;
}

static bool installed_in_os(const mesa_routing_entry_t &dbp)
{
    mesa_rc                        rc;
    vtss_appl_ip_route_status_t    st;
    vtss_appl_ip_ipv6_route_conf_t ipv6_rt;

    if (dbp.type == MESA_ROUTING_ENTRY_TYPE_IPV4_UC) {
        rc = status_rt_ipv4.get(&dbp.route.ipv4_uc, &st);
    } else if (dbp.type == MESA_ROUTING_ENTRY_TYPE_IPV6_UC) {
        if ((rc = conv(&dbp, &ipv6_rt)) == VTSS_RC_OK) {
            rc = status_rt_ipv6.get(&ipv6_rt, &st);
        }
    } else {
        rc = VTSS_RC_ERROR;
    }
    return (rc == VTSS_RC_OK);
}

static struct IP2RouteDbApplyIpv6 : public notifications::EventHandler {
    IP2RouteDbApplyIpv6() :
            EventHandler(&notifications::subject_main_thread),
            e_if_link_status(this),
            e_if_ipv6_status(this),
            e_rt_conf(this) {}

    void init() {
        status_if_link.observer_new(&e_if_link_status);
        status_if_ipv6.observer_new(&e_if_ipv6_status);
        IP_conf_route_ipv6.observer_new(&e_rt_conf);
    }

    void execute(notifications::Event *e) {
        if (e == &e_if_link_status) {
            VTSS_TRACE(INFO) << "Event: if_link_status";
            status_if_link.observer_get(&e_if_link_status, o_if_link_status);
        }
        if (e == &e_if_ipv6_status) {
            VTSS_TRACE(INFO) << "Event: if_ipv6_status";
            status_if_ipv6.observer_get(&e_if_ipv6_status, o_if_ipv6_status);
        }
        if (e == &e_rt_conf) {
            VTSS_TRACE(INFO) << "Event: IP_conf_route_ipv6";
            IP_conf_route_ipv6.observer_get(&e_rt_conf, o_rt_conf);
        }

        this->operator()();
    }

    void operator()();

    // TODO, make this work without the need for an observer... It is a vais of
    // energy as we do not need the "observed" changes, we just need to know
    // that something ahs changed
    notifications::Event e_if_link_status;
    StatusIfLink::Observer o_if_link_status;

    notifications::Event e_if_ipv6_status;
    StatusIfIpv6::Observer o_if_ipv6_status;

    notifications::Event e_rt_conf;
    IP_conf_route_t::Observer o_rt_conf;
} IP2_route_db_apply_ipv6;

static struct IP2RouteDbApplyIpv4 : public notifications::EventHandler {
    IP2RouteDbApplyIpv4() :
            EventHandler(&notifications::subject_main_thread),
            e_if_link_status(this),
            e_if_ipv4_status(this),
            e_rt_conf(this) {}

    void init() {
        status_if_link.observer_new(&e_if_link_status);
        status_if_ipv4.observer_new(&e_if_ipv4_status);
        IP_conf_route_ipv4.observer_new(&e_rt_conf);
    }

    void execute(notifications::Event *e) {
        if (e == &e_if_link_status) {
            VTSS_TRACE(INFO) << "Event: if_link_status";
            status_if_link.observer_get(&e_if_link_status, o_if_link_status);
        }
        if (e == &e_if_ipv4_status) {
            VTSS_TRACE(INFO) << "Event: if_ipv4_status";
            status_if_ipv4.observer_get(&e_if_ipv4_status, o_if_ipv4_status);
        }
        if (e == &e_rt_conf) {
            VTSS_TRACE(INFO) << "Event: IP_conf_route_ipv4";
            IP_conf_route_ipv4.observer_get(&e_rt_conf, o_rt_conf);
        }

/* IP module doesn't need to apply routes into OS if FRR is included.
*/
#if !defined(VTSS_SW_OPTION_FRR)
        this->operator()();
#endif /* !VTSS_SW_OPTION_FRR */
    }

    void operator()();

    notifications::Event e_if_link_status;
    StatusIfLink::Observer o_if_link_status;

    notifications::Event e_if_ipv4_status;
    StatusIfIpv4::Observer o_if_ipv4_status;

    notifications::Event e_rt_conf;
    IP_conf_route_t::Observer o_rt_conf;
} IP2_route_db_apply_ipv4;

vtss::ostream& operator<<(vtss::ostream &o, ip_vif_fwd_t e) {
    switch (e) {
#define CASE(X) case VIF_FWD_ ## X: o  << "(" << (int)e << ")" << #X; return o
        CASE(UNDEFINED);
        CASE(FORWARDING);
        CASE(BLOCKING);
#undef CASE
    }

    o << "Unknown entry: " << (int)e;
    return o;
}

/*
 * Forward declarations
 */
static mesa_rc IP2_route_add_ipv4(const mesa_routing_entry_t  *const rt,
                                  const vtss_routing_params_t *const params,
                                  const vtss_appl_ip_ipv4_route_conf_t *const conf);
static mesa_rc IP2_route_del_ipv4(const mesa_routing_entry_t  *const rt,
                                  const vtss_routing_params_t *const params);
static mesa_rc IP2_route_add_ipv6(const mesa_routing_entry_t  *const rt,
                                  const vtss_routing_params_t *const params);
static mesa_rc IP2_route_del_ipv6(const mesa_routing_entry_t  *const rt,
                                  const vtss_routing_params_t *const params);
static void IP2_if_cache_update();
static BOOL IP2_if_exists(vtss_if_id_vlan_t if_id);
static mesa_rc IP2_if_del(vtss_ifindex_t ifindex);

static mesa_rc IP2_ipv4_conf_set(vtss_if_id_vlan_t                    if_id,
                                 const vtss_appl_ip_ipv4_conf_t      *conf,
                                 BOOL                                 force);
static mesa_rc IP2_ipv6_conf_set(vtss_if_id_vlan_t                    if_id,
                                 const vtss_appl_ip_ipv6_conf_t      *conf);
static mesa_rc IP2_ifs_status_get(vtss_appl_ip_if_status_type_t       type,
                                  vtss::Vector<vtss_appl_ip_if_status_t> &st);
static mesa_rc IP2_if_status_get(vtss_appl_ip_if_status_type_t            type,
                                 vtss_if_id_vlan_t                        id,
                                  vtss::Vector<vtss_appl_ip_if_status_t> &st);
static mesa_rc IP2_if_status_get_first(vtss_appl_ip_if_status_type_t  type,
                                       vtss_if_id_vlan_t              id,
                                       vtss_appl_ip_if_status_t      *status);

static mesa_rc IP2_ip_dhcpc_v4_stop_no_reapply(vtss_if_id_vlan_t if_id);

static void IP2_if_signal_vlan(vtss_if_id_vlan_t vid);

#if defined(VTSS_SW_OPTION_FRR)
static mesa_rc conv(const FrrIpRoute *const a,
                    vtss_appl_ip_ipv4_route_conf_t *const b);
static mesa_rc conv(const mesa_routing_entry_t *const a,
                    const vtss_appl_ip_ipv4_route_conf_t *const c,
                    FrrIpRoute *const b);
#endif /* VTSS_SW_OPTION_FRR */

/* Module config */
static ip_stack_conf_t IP2_stack_conf;

/*
 * State
 */

#define IFNO_INVALID 0xFF

typedef struct IP2_dhcp_fallback_timer_t_ {
    VTSS_LINKED_LIST_DECL_ITEM(IP2_dhcp_fallback_timer_t_);
    vtss_tick_count_t timeout;
    mesa_vid_t vlan;
} IP2_dhcp_fallback_timer_t;

typedef VTSS_LINKED_LIST_TYPE(IP2_dhcp_fallback_timer_t_) IP2_dhcp_fallback_timer_list_t;
IP2_dhcp_fallback_timer_list_t IP2_dhcp_fallback_timer_list;

// TODO, simplify state model
typedef struct ip2_ifstate {
    /* State data */
    uint                        if_no;              /**< Interface number [0; IP_MAX_INTERFACES[ */
    mesa_mac_t                  mac_address;        /**< Interface MAC address */
    mesa_vid_t                  vid;                /**< VLAN Id */
    BOOL                        dhcp_v4_valid;      /**< dhcp_v4_address valid */
    mesa_ipv4_t                 dhcp_v4_gw;         /**< dhcp default gw */

    BOOL                        ipv4_active;
    vtss::Ipv4Network           cur_ipv4;           /**< Current IPv4 address */

    BOOL                        ipv6_active;
    mesa_ipv6_network_t         cur_ipv6;           /**< Current IPv6 address */

    struct {
        BOOL                     active;
        mesa_ipv6_network_t      network;           /**< Current IPv6 address */
        vtss_ip_ipv6_addr_info_t info;
    } dhcpv6;

    IP2_dhcp_fallback_timer_t   dhcpc_start_timer;  /**< Point in time when DHCPC started */

    /* Config data */
    vtss_interface_ip_conf_t   *config;
} ip2_ifstate_t;

// RFC 5227 Address Conflict Detection, all delays in seconds
#define ACD_PROBE_NUM         3 // Number of probes
#define ACD_PROBE_WAIT        1 // Delay before sending first probe
#define ACD_PROBE_MIN         1 // Minimum delay between probes
#define ACD_PROBE_MAX         2 // Maximum delay between probes
#define ACD_ANNOUNCE_NUM      2 // Number of announcements
#define ACD_ANNOUNCE_WAIT     2 // Delay before first announcement
#define ACD_ANNOUNCE_INTERVAL 2 // Delay between announcements

// MAC/ARP header offsets and values
#define O_MAC_DMAC   0  // DMAC
#define O_MAC_SMAC   6  // SMAC
#define O_MAC_ETYPE  12 // Type/length
#define O_ARP_HTYPE  14 // Hardware type
#define O_ARP_PTYPE  16 // Protocol type
#define O_ARP_HLEN   18 // Hardware address length
#define O_ARP_PLEN   19 // Protocol address length
#define O_ARP_OPER   20 // Opcode
#define O_ARP_SHA    22 // Sender hardware address
#define O_ARP_SPA    28 // Sender protocol address
#define O_ARP_THA    32 // Target hardware address
#define O_ARP_TPA    38 // Target protocol address

#define ARP_LEN      42     // ARP Ethernet/IPv4 message length
#define ARP_ETYPE    0x0806 // ARP Ethernet type
#define ARP_HTYPE    1      // Hardware address type Ethernet
#define ARP_PTYPE    0x0800 // Protocol address type IPv4
#define ARP_HLEN     6      // Ethernet address length
#define ARP_PLEN     4      // IPv4 address length
#define ARP_OPER_REQ 1      // ARP request
#define ARP_OPER_REP 2      // ARP response

using namespace vtss::notifications;

struct Acd : public EventHandler {
    // Default constructor
    Acd() : EventHandler(&subject_main_thread), state(State::IDLE), timer(this) {};

    void execute(Timer *e) override {
        CRIT_SCOPE();
        VTSS_TRACE(DEBUG) << "ACD timer event";
        new_state(state);
    }

    enum class State {
        IDLE,    // No IP address setup
        PROBING, // Probing for conflicting IP
        BOUND    // IP address is bound
    } state;

    const char *state_txt(State s) {
        return (s == State::IDLE ? "IDLE" :
                s == State::PROBING ? "PROBING" :
                s == State::BOUND ? "BOUND" : "?");
    }

    void add_timer(u32 scale, u32 offset) {
        u64 msec = 0;

        if (scale) {
            msec = rand();
            msec = ((msec * 1000) / RAND_MAX); // 0-1000 msec
            msec *= scale;
        }
        msec += (offset * 1000);
        VTSS_TRACE(DEBUG) << "ACD add timer: " << msec << " msec";
        nanoseconds nano_sec(msec * 1000000);
        add(&timer, nano_sec);
    }

    void new_state(State s) {
        if (state != s) {
            VTSS_TRACE(INFO) << "ACD state: " << state_txt(state) << "->" << state_txt(s);
            count = 0;
            state = s;
        }
        switch (s) {
        case State::PROBING:
            if (count > ACD_PROBE_NUM ) {
                // Probing done, go to BOUND state
                new_state(State::BOUND);
                if (ifs->config->ipv4.dhcpc) {
                    vtss::dhcp::client_bind(ifs->vid);
                }
            } else {
                if (count) {
                    arp_tx(FALSE);
                }
                if (count == ACD_PROBE_NUM) {
                    add_timer(0, ACD_ANNOUNCE_WAIT);
                } else if (count) {
                    add_timer(ACD_PROBE_MAX - ACD_PROBE_MIN, ACD_PROBE_MIN);
                } else {
                    add_timer(ACD_PROBE_WAIT, 0);
                }
                count++;
            }
            break;
        case State::BOUND:
            if (count < ACD_ANNOUNCE_NUM) {
                arp_tx(TRUE);
                if (count == 0) {
                    if (vtss_ip_os_ipv4_add(ifs->vid, &net) == VTSS_RC_OK) {
                        ifs->ipv4_active = TRUE;
                        IP2_if_signal_vlan(ifs->vid);
                    } else {
                        VTSS_TRACE(INFO) << "failed to set IPv4 address " << net << " on vlan=" << ifs->vid;
                    }
                    add_timer(0, ACD_ANNOUNCE_INTERVAL);
                }
                count++;
            }
            break;
        default:
            break;
        }
    }

    u16 getb16(const u8 *p, int offs) {
        return ((p[offs] << 8) | p[offs + 1]);
    }

    u32 getb32(const u8 *p, int offs) {
        return ((p[offs] << 24) | (p[offs + 1] << 16) | (p[offs + 2] << 8) | p[offs + 3]);
    }

    void putb16(u8 *p, int offs, u16 val) {
        p[offs + 0] = (val >> 8);
        p[offs + 1] = (val >> 0);
    }

    void putb32(u8 *p, int offs, u32 val) {
        p[offs + 0] = (val >> 24);
        p[offs + 1] = (val >> 16);
        p[offs + 2] = (val >> 8);
        p[offs + 3] = (val >> 0);
    }

    void arp_rx(const uchar *const frm, const mesa_packet_rx_info_t *const rx_info) {
        u16                                oper, i;
        vtss_appl_ip_ipv4_acd_status_key_t key;
        vtss_appl_ip_ipv4_acd_status_t     status;

        // Ignore ARP messages in IDLE state
        if (state == State::IDLE) {
            return;
        }

        VTSS_TRACE(NOISE) << "vid: " << rx_info->tag.vid << ", port_no: " << rx_info->port_no << ", len: " << rx_info->length;

        // Only process valid Ethernet/IPv4 ARP request/response messages
        oper = getb16(frm, O_ARP_OPER);
        if (rx_info->length < ARP_LEN ||
            getb16(frm, O_ARP_HTYPE) != ARP_HTYPE ||
            getb16(frm, O_ARP_PTYPE) != ARP_PTYPE ||
            frm[O_ARP_HLEN] != ARP_HLEN ||
            frm[O_ARP_PLEN] != ARP_PLEN ||
            (oper != ARP_OPER_REQ && oper != ARP_OPER_REP)) {
                return;
        }

        // It is a conflict if the sender IP matches and the sender MAC does not match
        for (i = 0; i < 6; i++) {
            key.smac.addr[i] = frm[O_ARP_SHA + i];
        }
        key.sip = getb32(frm, O_ARP_SPA);
        if (key.sip == net.address && key.smac != ifs->mac_address) {
            if (vtss_ifindex_from_vlan(ifs->vid, &status.interface) == VTSS_RC_OK &&
                vtss_ifindex_from_port(VTSS_ISID_START, rx_info->port_no, &status.interface_port) == VTSS_RC_OK) {
                char bufSip4[40];
                VTSS_TRACE(INFO) << "ACD add, key: " << key << ", status: " << status;
                printf("arp: %02x:%02x:%02x:%02x:%02x:%02x is using my IP address %s!\n",
		            key.smac.addr[0], key.smac.addr[1], key.smac.addr[2], key.smac.addr[3], key.smac.addr[4], key.smac.addr[5], misc_ipv4_txt(key.sip, bufSip4));
                if (status_acd_ipv4.set(&key, &status) != VTSS_RC_OK) {
                    VTSS_TRACE(WARNING) << "status_acd_ipv4.set failed";
                }
            }
#if 0 /*Don't avoid using this address*/
            if (state == State::PROBING) {
                // Avoid using address
                new_state(State::IDLE);
                if (ifs->config->ipv4.dhcpc) {
                    vtss::dhcp::client_decline(ifs->vid);
                }
            }
#endif
        }
    }

    void arp_tx(BOOL announce) {
        packet_tx_props_t tx_props;
        u8                *frm, i, *smac = ifs->mac_address.addr;

        if (ifs->vid == VTSS_VID_NULL) {
            return;
        }

        VTSS_TRACE(INFO) << "Tx ARP " << (announce ? "Announce" : "Probe") << ", vid: " << ifs->vid;
        if ((frm = packet_tx_alloc(ARP_LEN)) == NULL) {
            VTSS_TRACE(WARNING) << "packet_tx_alloc() failed";
            return;
        }

        memset(frm, 0, ARP_LEN);
        for (i = 0; i < 6; i++) {
            frm[O_MAC_DMAC + i] = 0xff;
            frm[O_MAC_SMAC + i] = smac[i];
            frm[O_ARP_SHA + i] = smac[i];
        }
        putb16(frm, O_MAC_ETYPE, ARP_ETYPE);
        putb16(frm, O_ARP_HTYPE, ARP_HTYPE);
        putb16(frm, O_ARP_PTYPE, ARP_PTYPE);
        frm[O_ARP_HLEN] = ARP_HLEN;
        frm[O_ARP_PLEN] = ARP_PLEN;
        putb16(frm, O_ARP_OPER, ARP_OPER_REQ);
        putb32(frm, O_ARP_SPA, announce ? net.address : 0);
        putb32(frm, O_ARP_TPA, net.address);

        packet_tx_props_init(&tx_props);
        tx_props.packet_info.modid  = VTSS_MODULE_ID_IP;
        tx_props.tx_info.switch_frm = TRUE;
        tx_props.tx_info.tag.vid    = ifs->vid;
        tx_props.packet_info.frm    = frm;
        tx_props.packet_info.len    = ARP_LEN;
        if (packet_tx(&tx_props) != VTSS_RC_OK) {
            VTSS_TRACE(WARNING) << "packet_tx() failed";
        }
    }

    void start(ip2_ifstate_t *new_ifs, const mesa_ipv4_network_t *new_net) {
        ifs = new_ifs;
        net = *new_net;
        VTSS_TRACE(INFO) << "start, vid: " << ifs->vid;
        new_state(State::PROBING);
    }

    void stop(void) {
        vtss_appl_ip_ipv4_acd_status_key_t key;
        vtss_appl_ip_ipv4_acd_status_t     status;
        vtss_ifindex_t                     if_index;
        VTSS_TRACE(INFO) << "stop";
        new_state(State::IDLE);

        // Delete previous conflict entries on interface
        memset(&key, 0, sizeof(key));
        if (ifs != NULL &&
            vtss_ifindex_from_vlan(ifs->vid, &if_index) == VTSS_RC_OK) {
            while (vtss_appl_ip_ipv4_acd_status_itr(&key, &key) == VTSS_RC_OK &&
                   vtss_appl_ip_ipv4_acd_status_get(&key, &status) == VTSS_RC_OK &&
                   status.interface == if_index) {
                VTSS_TRACE(INFO) << "ACD delete, key: " << key;
                if (status_acd_ipv4.del(&key) != VTSS_RC_OK) {
                    VTSS_TRACE(WARNING) << "status_acd_ipv4.del failed";
                }
            }
        }
    }

    Timer               timer;
    int                 count;
    ip2_ifstate_t       *ifs;
    mesa_ipv4_network_t net;
};

static packet_rx_filter_t ipv4_acd_arp_rx_filter;
static void *ipv4_acd_arp_rx_filter_id = NULL;
static BOOL arp_rx_callback(void *contxt, const uchar *const frm, const mesa_packet_rx_info_t *const rx_info);

static void ipv4_acd_init(void)
{
    // Register for ARP frames
    packet_rx_filter_init(&ipv4_acd_arp_rx_filter);
    ipv4_acd_arp_rx_filter.modid = VTSS_MODULE_ID_IP;
    ipv4_acd_arp_rx_filter.match = PACKET_RX_FILTER_MATCH_ETYPE;
    ipv4_acd_arp_rx_filter.etype = 0x0806;
    ipv4_acd_arp_rx_filter.prio = PACKET_RX_FILTER_PRIO_NORMAL;
    ipv4_acd_arp_rx_filter.cb = arp_rx_callback;
    if (packet_rx_filter_register(&ipv4_acd_arp_rx_filter, &ipv4_acd_arp_rx_filter_id) != VTSS_RC_OK) {
        VTSS_TRACE(WARNING) << "ARP Rx registration failed";
    }
}

static struct {
    mesa_mac_t    master_mac;
    u8            vid2if[VTSS_VIDS];
    CapArray<ip2_ifstate_t, VTSS_APPL_CAP_IP_INTERFACE_CNT> interfaces;
    CapArray<Acd, VTSS_APPL_CAP_IP_INTERFACE_CNT>           acd;
    int           static_routes;
    /* Per unit vid fwd state */
    CapArray<ip_vif_fwd_t, VTSS_APPL_CAP_IP_INTERFACE_CNT> unit_fwd;
    /* Combined state */
    CapArray<ip_vif_fwd_t, VTSS_APPL_CAP_IP_INTERFACE_CNT> master_fwd;
    ip_vlan_state_t vlan_state;
} IP2_state;

static mesa_rc IP2_if_ipv4_del(ip2_ifstate_t *ifs);
static mesa_rc IP2_ipv6_static_conf_del(ip2_ifstate_t             *ifs,
                                        const mesa_ipv6_network_t *network);

static vtss_handle_t ip2_thread_handle;
static vtss_thread_t ip2_thread_block;

#define CTLFLAG_IFCHANGE            VTSS_BIT(0)
#define CTLFLAG_IFNOTIFY            VTSS_BIT(1)
#define CTLFLAG_NEW_TIMER           VTSS_BIT(2)
#define CTLFLAG_SWITCH_ADD          VTSS_BIT(3)
#define CTLFLAG_MASTER_CHANGED      VTSS_BIT(4)
#define CTLFLAG_VLAN_CHANGE         VTSS_BIT(5)
#define CTLFLAG_MO_FLAGS_CHANGE     VTSS_BIT(6)
static vtss_flag_t   ip2_control_flags;

static void IP2_dhcpc_fallback_timer_consider(ip2_ifstate_t *ifs, BOOL reinit);

static BOOL arp_rx_callback(void *contxt, const uchar *const frm, const mesa_packet_rx_info_t *const rx_info)
{
    uint       if_no;
    mesa_vid_t vid = rx_info->tag.vid;

    CRIT_SCOPE();
    for (if_no = 0; if_no < IP2_state.interfaces.size(); if_no++) {
        ip2_ifstate_t *if_state = &IP2_state.interfaces[if_no];
        if (if_state->vid == vid && vid != VTSS_VID_NULL) {
            IP2_state.acd[if_no].arp_rx(frm, rx_info);
            break;
        }
    }
    return FALSE; // Allow other subscribers to receive ARP frames
}

/* Internally exposed  */
ip2_ifstate_t *IP2_if2state(vtss_if_id_vlan_t if_id)
{
    ip2_ifstate_t *ifs = NULL;
    uint if_no;

    IP2_CRIT_ASSERT_LOCKED();
    if (if_id >= VTSS_APPL_VLAN_ID_MIN && if_id <= VTSS_APPL_VLAN_ID_MAX &&
        (if_no = IP2_state.vid2if[if_id]) != IFNO_INVALID) {
        ifs = &IP2_state.interfaces[if_no];
    }

    return ifs;
}

mesa_rc IP_ifindex_to_vlan(vtss_ifindex_t i, mesa_vid_t *v)
{
    vtss_ifindex_elm_t e;
    VTSS_RC(vtss_ifindex_decompose(i, &e));
    if (e.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        VTSS_TRACE(DEBUG) << "Interface " << i << " is not a vlan interface";
        return VTSS_RC_ERROR;
    }
    *v = e.ordinal;

    return VTSS_RC_OK;
}

ip2_ifstate_t *IP2_ifidx2state(vtss_ifindex_t i) {
    mesa_vid_t v;
    uint if_no;

    IP2_CRIT_ASSERT_LOCKED();

    if (IP_ifindex_to_vlan(i, &v) != VTSS_RC_OK) {
        return nullptr;
    }

    if_no = IP2_state.vid2if[v];
    if (if_no == IFNO_INVALID) {
        return nullptr;
    }

    return &IP2_state.interfaces[if_no];
}

mesa_rc ip2_state2if(const ip2_ifstate_t *ifs, vtss_if_id_vlan_t *if_id)
{
    *if_id = ifs->vid;
    return VTSS_RC_OK;
}

static void IP2_ifflags_force_sync(int if_no)
{
    IP2_CRIT_ASSERT_LOCKED();
    /* Force Sync of iff-flags */
    VTSS_TRACE(INFO) << "Poke CTLFLAG_IFCHANGE - ifno " << if_no;
    IP2_state.master_fwd[if_no] = VIF_FWD_UNDEFINED;
    vtss_flag_setbits(&ip2_control_flags, CTLFLAG_IFCHANGE);
}

static void IP2_if_init(ip2_ifstate_t *if_state,
                        uint if_no,
                        struct ip_iface_entry *if_conf,
                        mesa_mac_t *mac)
{
    IP2_CRIT_ASSERT_LOCKED();
    memset(if_state, 0, sizeof(ip2_ifstate_t));
    if_state->if_no = if_no;
    if_state->vid = if_conf->vlan_id;
    if_state->dhcpc_start_timer.vlan = if_conf->vlan_id;
    if_state->mac_address = *mac;
    if_state->config = &if_conf->conf;
    IP2_state.vid2if[if_state->vid] = if_no; /* Index 'backlink' */

    /* Force re-evaluation of if-state */
    IP2_ifflags_force_sync(if_no);
}

static ip2_ifstate_t *IP2_if_new(vtss_if_id_vlan_t if_id)
{
    uint if_no;
    ip2_ifstate_t *ifs = NULL;

    IP2_CRIT_ASSERT_LOCKED();
    for (if_no = 0; if_no < IP2_state.interfaces.size(); if_no++) {
        ip2_ifstate_t *if_state = &IP2_state.interfaces[if_no];
        if (if_state->vid == VTSS_VID_NULL) {
            struct ip_iface_entry *if_conf = &IP2_stack_conf.interfaces[if_no];
            if_conf->vlan_id = if_id; /* Allocate entry */
            VTSS_TRACE(INFO) << "IP2_if_new: Adding new vlanif " << if_id
                             << " is ifno: " << if_no;
            IP2_if_init(if_state, if_no, if_conf, &IP2_state.master_mac);
            ifs = if_state;
            break;
        }
    }
    return ifs;
}

static void IP2_dhcp_gw_clear(ip2_ifstate_t *ifs) {
    IP2_CRIT_ASSERT_LOCKED();

    if (ifs->dhcp_v4_gw) {
        mesa_routing_entry_t rt;
        vtss_routing_params_t param;

        vtss_ipv4_default_route(ifs->dhcp_v4_gw, &rt);
        param.owner = VTSS_APPL_IP_ROUTE_OWNER_DHCP;

        if (IP2_route_del_ipv4(&rt, &param) != VTSS_RC_OK) {
            VTSS_TRACE(INFO) << "Failed to delete route: " << rt.route.ipv4_uc;
        } else {
            VTSS_TRACE(INFO) << "Deleted route: " << rt.route.ipv4_uc;
        }

        ifs->dhcp_v4_gw = 0;
    }
}

static mesa_rc IP2_dhcp_clear(vtss_if_id_vlan_t if_id)
{
    ip2_ifstate_t *ifs;
    mesa_rc rc = VTSS_RC_OK;

    IP2_CRIT_ASSERT_LOCKED();

    VTSS_TRACE(DEBUG) << "IP2_dhcp_clear " << if_id;
    ifs = IP2_if2state(if_id);

    if (!ifs) {
        return VTSS_RC_ERROR;
    }

    IP2_dhcp_gw_clear(ifs);

    if (ifs->dhcp_v4_valid) {
        rc = IP2_if_ipv4_del(ifs);
        if (rc != VTSS_RC_OK)
            VTSS_TRACE(INFO) << "Failed to delete IP address: " <<
                ifs->cur_ipv4;
        ifs->dhcp_v4_valid = 0;
    }

    return VTSS_RC_OK;
}

bool IP2_ipv4_conf_static_address_active(const vtss_appl_ip_ipv4_conf_t &c) {
    return c.active && (!c.dhcpc || (c.dhcpc && c.fallback_timeout));
}

bool IP2_ipv6_conf_static_address_active(const vtss_appl_ip_ipv6_conf_t &c) {
    return c.active;
}

static void IP2_network_to_route(const mesa_ip_network_t *addr,
                                 mesa_routing_entry_t *e) {
    memset(e, 0, sizeof(*e));
    switch (addr->address.type) {
        case MESA_IP_TYPE_IPV4: {
            mesa_ipv4_t mask = (u32)-1;
            (void)vtss_conv_prefix_to_ipv4mask(addr->prefix_size, &mask);

            e->type = MESA_ROUTING_ENTRY_TYPE_IPV4_UC;
            e->route.ipv4_uc.network.prefix_size = addr->prefix_size;
            e->route.ipv4_uc.network.address = (addr->address.addr.ipv4 & mask);
            break;
        }

        case MESA_IP_TYPE_IPV6: {
            mesa_ipv6_t mask;
            (void)vtss_conv_prefix_to_ipv6mask(addr->prefix_size, &mask);
            for (size_t i = 0; i < sizeof(mask.addr); i++)
                e->route.ipv6_uc.network.address.addr[i] = mask.addr[i] & addr->address.addr.ipv6.addr[i];

            e->type = MESA_ROUTING_ENTRY_TYPE_IPV4_UC;
            e->route.ipv6_uc.network.prefix_size = addr->prefix_size;
            break;
        }

        default:
            return;
    }
}

static BOOL IP2_if_ip_check(const mesa_ip_network_t *addr,
                            mesa_vid_t ignore)
{
#define BUF_SIZE 128
    BOOL rc, ipv4;
    char buf1[BUF_SIZE];
    char buf2[BUF_SIZE];

    IP2_CRIT_ASSERT_LOCKED();
    VTSS_TRACE(DEBUG) << "Checking IP address: " << addr
                      << " ignoring vlan: " << ignore;

    if (addr->address.type == MESA_IP_TYPE_IPV4) {
        ipv4 = TRUE;
    } else if (addr->address.type == MESA_IP_TYPE_IPV6) {
        ipv4 = FALSE;
    } else {
        VTSS_TRACE(INFO) << "Un-supported address type";
        return FALSE;
    }

    vtss::Vector<vtss_appl_ip_if_status_t> st(130);
    if (!st.capacity()) {
        T_E("Alloc error");
        return FALSE;
    }

    //////////////////////////////////////////////////////////////////////////
    // Check against active IP address, regardless where they come from

    // get status IPs
    vtss_appl_ip_if_status_type_t status_type =
        ipv4 ? VTSS_APPL_IP_IF_STATUS_TYPE_IPV4 : VTSS_APPL_IP_IF_STATUS_TYPE_IPV6;
    if (IP2_ifs_status_get(status_type, st) != VTSS_RC_OK) {
        T_E("Failed to get if status, ipv4=%d", ipv4);
        rc = FALSE;
        goto DONE;
    }

    rc = TRUE;
    for (const auto &e : st) {
        mesa_ip_network_t tmp;

        if (e.if_id.type == VTSS_ID_IF_TYPE_VLAN && e.if_id.u.vlan == ignore)
            continue;

        if (ipv4) {
            tmp.address.type = MESA_IP_TYPE_IPV4;
            tmp.address.addr.ipv4 = e.u.ipv4.net.address;
            tmp.prefix_size = e.u.ipv4.net.prefix_size;
        } else {
            tmp.address.type = MESA_IP_TYPE_IPV6;
            tmp.address.addr.ipv6 = e.u.ipv6.net.address;
            tmp.prefix_size = e.u.ipv6.net.prefix_size;
        }

        if (vtss_ip_net_overlap(addr, &tmp)) {
            (void) vtss_ip_ip_network_to_txt(buf1, BUF_SIZE, addr);
            (void) vtss_ip_ip_network_to_txt(buf2, BUF_SIZE, &tmp);
            VTSS_TRACE(INFO) << "Address " << addr << " conflicts with " << tmp;
            rc = FALSE;
            goto DONE;
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // Check against static configured addresses
    for (size_t i = 0; i < IP2_state.interfaces.size(); ++i) {
        const ip2_ifstate_t *const ifs = &IP2_state.interfaces[i];
        vtss::IpNetwork if_addr;
        bool active = false;

        if (ifs->vid == VTSS_VID_NULL) {
            continue;
        }

        if (ifs->vid == ignore) {
            continue;
        }

        if (ipv4) {
            active  = IP2_ipv4_conf_static_address_active(ifs->config->ipv4);
            if_addr = ifs->config->ipv4.network;
        } else {
            active  = IP2_ipv6_conf_static_address_active(ifs->config->ipv6);
            if_addr = ifs->config->ipv6.network;
        }

        // Not active -> no conflict
        if (!active) {
            continue;
        }

        // check static ip address
        if (vtss_ip_net_overlap(addr, &if_addr.as_api_type())) {
            VTSS_TRACE(INFO) << "Address " << *addr << " conflicts with " <<
                if_addr << " on vlan " << ifs->vid;
            rc = FALSE;
            goto DONE;
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // Check against routes - the network of a interface must not be equal to
    // the network of a route (for systems with no metrics support).
    {
        int users;
        mesa_routing_entry_t next, r;

        IP2_network_to_route(addr, &r);
        next = r;

        if (r.type == MESA_ROUTING_ENTRY_TYPE_IPV4_UC &&
            IP_conf_route_ipv4.get_next(&next, &users) == VTSS_RC_OK) {

            // Posible match!
            VTSS_TRACE(INFO) << "Possible match: " << next;

            if (next.type == MESA_ROUTING_ENTRY_TYPE_IPV4_UC &&
                vtss_ipv4_net_equal(&next.route.ipv4_uc.network,
                                    &r.route.ipv4_uc.network)) {
                VTSS_TRACE(INFO) << "Address "
                        << *addr
                        << " conflicts with route "
                        << next;
                rc = FALSE;
                goto DONE;
            }
        }

        if (r.type == MESA_ROUTING_ENTRY_TYPE_IPV6_UC &&
            IP_conf_route_ipv6.get_next(&next, &users) == VTSS_RC_OK) {
            // Posible match!
            VTSS_TRACE(INFO) << "Possible match: " << next;

            if (next.type == MESA_ROUTING_ENTRY_TYPE_IPV6_UC &&
                vtss_ipv6_net_equal(&next.route.ipv6_uc.network,
                                    &r.route.ipv6_uc.network)) {
                VTSS_TRACE(INFO) << "Address "
                        << *addr
                        << " conflicts with route "
                        << next;
                rc = FALSE;
                goto DONE;
            }
        }
    }

DONE:
    VTSS_TRACE(INFO) << "Returning: " << rc;
    return rc;
#undef BUF_SIZE
}

static BOOL IP2_if_ipv4_check(const mesa_ipv4_network_t *v4, mesa_vid_t ignore)
{
    // IPv4 address from here
    mesa_ip_network_t n;
    n.address.type = MESA_IP_TYPE_IPV4;
    n.prefix_size = v4->prefix_size;
    n.address.addr.ipv4 = v4->address;
    return (vtss_ip_ipv4_ifaddr_valid(v4) && IP2_if_ip_check(&n, ignore));
}

static BOOL IP2_if_ipv6_check(const mesa_ipv6_network_t *v6, mesa_vid_t ignore)
{
    mesa_ip_network_t n;
    n.address.type = MESA_IP_TYPE_IPV6;
    n.prefix_size = v6->prefix_size;
    n.address.addr.ipv6 = v6->address;
    return IP2_if_ip_check(&n, ignore);
}

static void IP_dhcp_signal(vtss_ifindex_t ifidx) {
    IP2_CRIT_ASSERT_LOCKED();
    vtss_appl_ip_dhcp_client_status_t status;

    auto e = vtss::ifindex_decompose(ifidx);
    if (e.iftype != VTSS_IFINDEX_TYPE_VLAN) return;

    mesa_rc rc = vtss::dhcp::client_status(e.ordinal, &status);
    if (rc != VTSS_RC_OK) {
        status_if_dhcp.del(ifidx);
        return;
    }

    status_if_dhcp.set(ifidx, &status);
}

static void IP_dhcp_signal_vlan(mesa_vid_t vid) {
    vtss_ifindex_t ifidx;
    if (vtss_ifindex_from_vlan(vid, &ifidx) != VTSS_RC_OK) return;
    IP_dhcp_signal(ifidx);
}

static mesa_rc IP2_if_ipv4_set(ip2_ifstate_t *ifs, const Ipv4Network &net)
{
    mesa_rc rc = VTSS_RC_OK;
    Ipv4Network cur(ifs->cur_ipv4);
    Acd *acd = &IP2_state.acd[ifs->if_no];
    const mesa_ipv4_network_t *network = &net.as_api_type();

    if (net == cur && ifs->ipv4_active) {
        VTSS_TRACE(DEBUG) << "Ignore interface update doe to cache. Vlan="
                          << ifs->vid;
        return VTSS_RC_OK;
    }

    acd->stop();
    if (ifs->ipv4_active) {
        VTSS_TRACE(INFO) << "Deleting IPv4 address " << cur << " on vlan=" << ifs->vid;
        rc = vtss_ip_os_ipv4_del(ifs->vid, &ifs->cur_ipv4.as_api_type());
        if (rc == VTSS_RC_OK) {
            ifs->ipv4_active = FALSE;
        } else {
            VTSS_TRACE(ERROR) << "Failed to delete IPv4 address " << cur <<
                " on vlan=" << ifs->vid;
        }
    }

    if (vtss_ip_ipv4_ifaddr_valid(network)) {
        VTSS_TRACE(INFO) << "Set new IPv4 address " << net << " on Vlan=" << ifs->vid;
        acd->start(ifs, network);
    }

    // Update cache on success
    if (rc == VTSS_RC_OK) {
        ifs->cur_ipv4 = *network;
        IP2_if_cache_update();
    }

    return rc;
}

static mesa_rc IP2_if_ipv4_del(ip2_ifstate_t *ifs)
{
    mesa_rc rc = VTSS_RC_OK;

    IP2_state.acd[ifs->if_no].stop();
    if (ifs->ipv4_active) {
        VTSS_TRACE(INFO) << "Deleting IPv4 address " << ifs->cur_ipv4 << " on vlan=" <<
            ifs->vid;
        rc = vtss_ip_os_ipv4_del(ifs->vid, &ifs->cur_ipv4.as_api_type());
        if (rc == VTSS_RC_OK) {
            // Update cache on success
            ifs->ipv4_active = FALSE;
            ifs->cur_ipv4 = vtss::Ipv4Network(0, 0);
            IP2_if_cache_update();
        } else {
            VTSS_TRACE(ERROR) << "Failed to delete IPv4 address " << ifs->cur_ipv4 <<
                " on vlan=" << ifs->vid;
        }
    }

    return rc;
}

static mesa_rc IP2_ip_dhcpc_cb_apply(mesa_vid_t vlan)
{
    mesa_rc rc;
    ip2_ifstate_t *ifs;
    vtss::dhcp::ConfPacket fields;

    IP2_CRIT_ASSERT_LOCKED();
    ifs = IP2_if2state(vlan);
    if (!ifs) {
        T_W("Could not translate vlan %u to interface state", vlan);
        return VTSS_RC_ERROR;
    }

    // get the new IP address (if we got one)
    rc = vtss::dhcp::client_fields_get(vlan, &fields);
    if (rc != VTSS_RC_OK) {
        return rc;
    }

    if (!IP2_if_ipv4_check(&fields.ip.as_api_type(), vlan)) {
        T_W("The ack'ed dhcp ip address is not valid in this system");
        (void) vtss::dhcp::client_release(vlan);
        return VTSS_RC_ERROR;
    }

    // Apply IP settings
    Ipv4Network ip = fields.ip;
    rc = IP2_if_ipv4_set(ifs, ip);
    if (rc == VTSS_RC_OK) {
        ifs->dhcp_v4_valid = TRUE;

    } else {
        VTSS_TRACE(WARNING) << "Failed to set IPv4 interface on vlan: " <<
            vlan << ip;
        return rc;
    }

    // Apply default GW
    if (fields.default_gateway.valid() &&
            ifs->dhcp_v4_gw != fields.default_gateway.get()) {

        // current GW setting is valid, delete the existing GW
        IP2_dhcp_gw_clear(ifs);

        mesa_routing_entry_t rt;
        vtss_routing_params_t params;
        vtss_appl_ip_ipv4_route_conf_t conf = {};

        conf.distance = 254; // Generally DHCP is less trusty than routing protocol

        rt.type = MESA_ROUTING_ENTRY_TYPE_IPV4_UC;
        rt.route.ipv4_uc.network.address = 0;
        rt.route.ipv4_uc.network.prefix_size = 0;
        rt.route.ipv4_uc.destination = fields.default_gateway.get();
        params.owner = VTSS_APPL_IP_ROUTE_OWNER_DHCP;

        VTSS_TRACE(INFO) << "Using default route from dhcp: " << rt;
        rc = IP2_route_add_ipv4(&rt, &params, &conf);
        if (rc == VTSS_RC_OK)
            ifs->dhcp_v4_gw = fields.default_gateway.get();
        else
            VTSS_TRACE(INFO) << "Failed to add default route. vlan: " << vlan <<
                " route: " << rt.route.ipv4_uc;

    } else if (!fields.default_gateway.valid()) {
        VTSS_TRACE(INFO) << "Delete GW settings on " << vlan;
        IP2_dhcp_gw_clear(ifs);
    } else {
        VTSS_TRACE(NOISE) << "DHCP GW settongs on " << vlan <<
            " needs no update";
    }

    return rc;
}

static void IP2_ip_dhcpc_cb_ack(mesa_vid_t vlan) {
    mesa_rc rc;
    size_t valid_offers;
    vtss::dhcp::ConfPacket list[VTSS_DHCP_MAX_OFFERS];

    IP2_CRIT_ASSERT_LOCKED();
    ip2_ifstate_t *ifs = IP2_if2state(vlan);
    if (!ifs) {
        VTSS_TRACE(ERROR) << "No such interface: " << vlan;
        return;
    }

    rc = vtss::dhcp::client_offers_get(vlan, VTSS_DHCP_MAX_OFFERS,
                                       &valid_offers, list);
    if (rc != VTSS_RC_OK) {
        VTSS_TRACE(ERROR) << "Failed to get list of offers!";
        return;
    }

    VTSS_TRACE(INFO) << "Got " << valid_offers <<
        " offers from dhcp client on vlan: " << vlan;

    for (int i = 0; i < valid_offers; ++i) {
        VTSS_TRACE(DEBUG) << "Looking at index: " << i;
        VTSS_TRACE(DEBUG) << " Flags: " << ifs->config->ipv4.dhcpc_params.dhcpc_flags
                          << " vendor-specific-informaiton size: "
                          << list[i].vendor_specific_information.size()
                          << " boot-file-name size: "
                          << list[i].boot_file_name.size();

        // Filer out offers wich does have the required options
        if ((ifs->config->ipv4.dhcpc_params.dhcpc_flags & VTSS_APPL_IP_DHCP_FLAG_OPTION_43)
                && !list[i].vendor_specific_information.size()) {
            VTSS_TRACE(DEBUG) << "No VTSS_APPL_IP_DHCP_FLAG_OPTION_43 found";
            continue;
        }

        if ((ifs->config->ipv4.dhcpc_params.dhcpc_flags & VTSS_APPL_IP_DHCP_FLAG_OPTION_67)
                && !list[i].boot_file_name.size()) {
            VTSS_TRACE(DEBUG) << "No VTSS_APPL_IP_DHCP_FLAG_OPTION_67 found";
            continue;
        }

        if (IP2_if_ipv4_check(&list[i].ip.as_api_type(), vlan)) {
            VTSS_TRACE(INFO) << "Accepting offer: " << i << " " << list[i].ip;
            (void) vtss::dhcp::client_offer_accept(vlan, i);
            return;
        } else {
            VTSS_TRACE(DEBUG) << "IP2_if_ipv4_check failed!";
        }
    }

    VTSS_TRACE(INFO) << "No offer was accepted";
}

static mesa_rc IP2_if_ipv4_signal(ip2_ifstate_t *ifs) {
    // derive the interface address based on configuration and dhcp client state
    mesa_rc rc = VTSS_RC_OK;
    vtss_appl_ip_dhcp_client_status_t status;

    VTSS_TRACE(DEBUG) << "IP2_if_ipv4_signal vlan=" << ifs->vid << " active: "
                      << ifs->config->ipv4.active << " "
                      << (ifs->config->ipv4.dhcpc ? "dhcp" : "no-dhcp");

    // If the interface is not active, then delete the dhcp client
    if (!ifs->config->ipv4.active) {
        VTSS_TRACE(INFO) << "Interface is not active";
        (void) IP2_dhcp_clear(ifs->vid);
        (void) IP2_if_ipv4_del(ifs);
        return VTSS_RC_OK;
    }

    // No dhcp means static configuration
    if (!ifs->config->ipv4.dhcpc) {
        VTSS_TRACE(INFO) << "static config";
        return IP2_if_ipv4_set(ifs, Ipv4Network(ifs->config->ipv4.network));
    }

    // DHCP is active
    rc = vtss::dhcp::client_status(ifs->vid, &status);
    if (rc != VTSS_RC_OK) {
        VTSS_TRACE(INFO) << "Failed to get dhcp status";
        rc = IP2_dhcp_clear(ifs->vid);
        return rc;
    }

    switch (status.state) {
    case VTSS_APPL_IP_DHCP4C_STATE_SELECTING:
        VTSS_TRACE(INFO) << "Selecting";
        rc = IP2_dhcp_clear(ifs->vid);
        IP2_ip_dhcpc_cb_ack(ifs->vid);
        break;

    case VTSS_APPL_IP_DHCP4C_STATE_REBINDING:
    case VTSS_APPL_IP_DHCP4C_STATE_BOUND_ARP_CHECK:
    case VTSS_APPL_IP_DHCP4C_STATE_RENEWING:
        VTSS_TRACE(INFO) << "Apply dhcp client addresses";
        rc = IP2_ip_dhcpc_cb_apply(ifs->vid);
        break;

    case VTSS_APPL_IP_DHCP4C_STATE_FALLBACK:
        VTSS_TRACE(INFO) << "Set fallback address";
        if (ifs->config->ipv4.fallback_timeout) {
            rc = IP2_if_ipv4_set(ifs, Ipv4Network(ifs->config->ipv4.network));
        } else {
            // should not be in fallback state when there is no fallback timer
            if (vtss::dhcp::client_start(ifs->vid, &ifs->config->ipv4.dhcpc_params) != VTSS_RC_OK) {
                T_E("Failed to start DHCP server on %u", ifs->vid);
            }

            // signal the interface
            (void) IP2_if_ipv4_signal(ifs);
        }
        break;

    case VTSS_APPL_IP_DHCP4C_STATE_BOUND:
        VTSS_TRACE(INFO) << "Ignoring BOUND";
        break;

    default:
        VTSS_TRACE(INFO) << "DHCP client have no IP address, clear all addresses";
        rc = IP2_dhcp_clear(ifs->vid);
        break;
    }

#ifdef VTSS_SW_OPTION_DNS
    // DNS settings must be applied after routes has been configured.
    VTSS_TRACE(INFO) << "dns-signal";
    vtss_dns_signal();
#endif /* VTSS_SW_OPTION_DNS */

    VTSS_TRACE(INFO) << "done: " << rc;
    return rc;
}

static void IP2_ip_dhcpc_cb(mesa_vid_t vlan)
{
    ip2_ifstate_t *ifs;
    IP2_CRIT_ASSERT_LOCKED();

    IP_dhcp_signal_vlan(vlan);
    ifs = IP2_if2state(vlan);

    if (!ifs) {
        T_W("Could not translate vlan %u to interface state", vlan);
        return;
    }

    // The fallback timer must react on current state. It must be activated when
    // we go into SELECTING, and otherwise disabled
    IP2_dhcpc_fallback_timer_consider(ifs, FALSE);

    // Update interface settings
    (void) IP2_if_ipv4_signal(ifs);
}

static void vtss_ip2_ip_dhcpc_cb(mesa_vid_t vlan)
{
    // Lock even though we are static... This function is called through
    // callbacks from dhc dhcp client
    CRIT_SCOPE();
    VTSS_TRACE(DEBUG) << "Got dhcp callback on vlan " << vlan;
    IP2_ip_dhcpc_cb(vlan);
}

static vtss_tick_count_t IP2_dhcpc_fallback_timer_evaluate(vtss_tick_count_t t)
{
    IP2_dhcp_fallback_timer_t *i;
    vtss_tick_count_t cur = vtss_current_time();

    while (!VTSS_LINKED_LIST_EMPTY(IP2_dhcp_fallback_timer_list)) {
        i = VTSS_LINKED_LIST_FRONT(IP2_dhcp_fallback_timer_list);

        if (i->timeout <= cur) {
            ip2_ifstate_t *ifs;
            mesa_vid_t vid = i->vlan;
            VTSS_TRACE(DEBUG) << "Time is up " << i->timeout << ", " << cur
                              << ", vlan=" << vid;

            // time is up
            VTSS_LINKED_LIST_POP_FRONT(IP2_dhcp_fallback_timer_list);

            ifs = IP2_if2state(vid);
            if (ifs) {
                // Put the dhcp-client in fallback mode
                if (vtss::dhcp::client_fallback(ifs->vid) != VTSS_RC_OK) {
                    T_E("Failed to stop DHCP server on %u", ifs->vid);
                }

                // signal the interface
                (void) IP2_if_ipv4_signal(ifs);

            } else {
                T_E("Failed to map %u to ifs", vid);
            }

        } else {
            // we got a new timeout
            VTSS_TRACE(DEBUG) << "Next timer: " << t << ", " << i->timeout;
            return (t < i->timeout) ? t : i->timeout;
        }
    }

    // no timers, return input as minimum
    return t;
}

static void IP2_dhcpc_fallback_timer_start(vtss_tick_count_t timeout,
                                           IP2_dhcp_fallback_timer_t *timer)
{
    IP2_dhcp_fallback_timer_t *i;
    VTSS_LINKED_LIST_UNLINK(*timer);
    vtss_tick_count_t cur = vtss_current_time();

    VTSS_TRACE(INFO) << "DHCPC: Start fallback timer " << timeout << ", vlan="
                     << timer->vlan;
    timer->timeout = cur + timeout;

    VTSS_LINKED_LIST_FOREACH(IP2_dhcp_fallback_timer_list, i) {
        if (i->timeout > timer->timeout) {
            break;
        }
    }

    VTSS_LINKED_LIST_INSERT_BEFORE(IP2_dhcp_fallback_timer_list, i, *timer);
    vtss_flag_setbits(&ip2_control_flags, CTLFLAG_NEW_TIMER);
}

static void IP2_dhcpc_fallback_timer_consider(ip2_ifstate_t *ifs, BOOL reinit)
{
    vtss_appl_ip_dhcp_client_status_t  status;

    if (!ifs->config->ipv4.active) {
        goto UNLINK;
    }

    if (ifs->config->ipv4.fallback_timeout == 0) {
        goto UNLINK;
    }

    if (vtss::dhcp::client_status(ifs->vid, &status) != VTSS_RC_OK) {
        goto UNLINK;
    }

    if (status.state != VTSS_APPL_IP_DHCP4C_STATE_SELECTING) {
        goto UNLINK;
    }

    if (!ifs->config->ipv4.dhcpc) {
        goto UNLINK;
    }

    if (!VTSS_LINKED_LIST_IS_LINKED(ifs->dhcpc_start_timer) || reinit) {
        vtss_tick_count_t timeout;

        timeout = VTSS_OS_MSEC2TICK(ifs->config->ipv4.fallback_timeout * 1000);
        IP2_dhcpc_fallback_timer_start(timeout, &ifs->dhcpc_start_timer);
    }
    return;

UNLINK:
    if (VTSS_LINKED_LIST_IS_LINKED(ifs->dhcpc_start_timer)) {
        VTSS_TRACE(INFO) << "Unlink timer, vlan=" << ifs->vid;
        VTSS_LINKED_LIST_UNLINK(ifs->dhcpc_start_timer);
    }
}

static void IP2_dhcpc_fallback_timer_update(ip2_ifstate_t *ifs)
{
    // Delete existing timer
    if (ifs->config->ipv4.fallback_timeout == 0) {
        VTSS_LINKED_LIST_UNLINK(ifs->dhcpc_start_timer);
        return;
    }

    // Set new timeout
    IP2_dhcpc_fallback_timer_consider(ifs, TRUE);
}

static mesa_rc IP2_dhcpc_v4_start(ip2_ifstate_t *ifs,
                                  const vtss_appl_ip_ipv4_conf_t *conf)
{
    mesa_rc rc = VTSS_RC_OK;

    IP2_CRIT_ASSERT_LOCKED();

    VTSS_TRACE(INFO) << "Starting dhcp client on vlan " << ifs->vid;
    if (!ifs) {
        VTSS_TRACE(DEBUG) << "Failed to get infterface status obj";
        return IP_ERROR_PARAMS;
    }

    (void)IP2_if_ipv4_del(ifs);

    VTSS_TRACE(INFO) << "Starting client";
    rc = vtss::dhcp::client_start(ifs->vid, &conf->dhcpc_params);
    if (rc != VTSS_RC_OK) {
        VTSS_TRACE(INFO) << "Failed to start dhcp client";
        return rc;
    }

    VTSS_TRACE(INFO) << "Adding callback";
    rc = vtss::dhcp::client_callback_add(ifs->vid, vtss_ip2_ip_dhcpc_cb);
    if (rc != VTSS_RC_OK) {
        T_E("Failed to start dhcp CB");
        return rc;
    }

    return VTSS_RC_OK;
}

/*
 * Stop interface - OS & chip
 */
static mesa_rc IP2_if_teardown(ip2_ifstate_t *if_state)
{
    int if_no;
    mesa_vid_t vid;
    mesa_rc rc = VTSS_OK;

    IP2_CRIT_ASSERT_LOCKED();

    vid = if_state->vid;
    if_no = if_state->if_no;

    IP2_state.acd[if_no].stop();

    // unlink potential timers
    VTSS_LINKED_LIST_UNLINK(if_state->dhcpc_start_timer);

    VTSS_TRACE(INFO) << "Delete former IP interface: vid=" << vid << ", if_no="
                     << if_no;
    if (if_state->config->ipv4.dhcpc) {
        (void)IP2_ip_dhcpc_v4_stop_no_reapply(vid);
    }

    rc = vtss_ip_os_if_del(if_state->vid);

    /* Now Nuke state */
    memset(if_state, 0, sizeof(*if_state));

    /* And nuke index backlink */
    IP2_state.vid2if[vid] = IFNO_INVALID;

    // Clear forwarding information
    IP2_state.unit_fwd[if_no] = VIF_FWD_UNDEFINED;

    IP2_ifflags_force_sync(if_no);

    return rc;
}

static void IP2_process_fwdstate(const fwd_state_t *rsp);
static void poll_fwdstate() {
    IP2_CRIT_ASSERT_LOCKED();
    VTSS_TRACE(NOISE) << "Publish fwd state";
    fwd_state_t fwd_state;

    mesa_packet_port_info_t info;
    uint32_t if_cnt = ip_interface_cnt_get();

    (void) mesa_packet_port_info_init(&info);
    for (int if_no = 0; if_no < if_cnt; if_no++) {
        CapArray<mesa_packet_port_filter_t, MESA_CAP_PORT_CNT> filter;

        if (IP2_state.interfaces[if_no].vid == VTSS_VID_NULL)
            continue;

        info.vid = IP2_state.interfaces[if_no].vid;
        if (mesa_packet_port_filter_get(NULL, &info, filter.size(), filter.data()) != VTSS_OK)
            continue;

        port_iter_t pit;
        (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL,
                              PORT_ITER_SORT_ORDER_IPORT,
                              PORT_ITER_FLAGS_FRONT | PORT_ITER_FLAGS_UP);

        ip_vif_fwd_t fwd = VIF_FWD_BLOCKING;
        int cnt = 0;
        while (port_iter_getnext(&pit)) {
            cnt ++;
            vtss_appl_ip_if_status_link_t if_status_link;
            vtss_ifindex_t ifidx;
            if (filter[pit.iport].filter != MESA_PACKET_FILTER_DISCARD &&
                vtss_ifindex_from_vlan(info.vid, &ifidx) == VTSS_RC_OK &&
                vtss_appl_ip_if_status_link(ifidx, &if_status_link) == VTSS_RC_OK &&
                    (if_status_link.flags & VTSS_APPL_IP_IF_LINK_FLAG_UP)){
                    VTSS_TRACE(NOISE) << "Port=" << pit.uport
                                  << " on vid=" << info.vid << " is FWD";
                    fwd = VIF_FWD_FORWARDING;
                    break;  // If one port is up and os-layer ip interface is up, then the interface is up
            } else {
                VTSS_TRACE(NOISE) << "Port=" << pit.uport
                                  << " on vid=" << info.vid << " is BLOCKING";
            }
        }
        if (!cnt) {
            T_R("No ports UP for vid=%d", info.vid);
        }

        VTSS_TRACE(NOISE) << "VID=" << info.vid << " " << fwd;
        fwd_state.iface[if_no].vid = info.vid;
        fwd_state.iface[if_no].fwd = fwd;
    }

    IP2_process_fwdstate(&fwd_state);
}

static void IP2_process_fwdstate(const fwd_state_t *fwd_state)
{
    uint if_no;
    int changes = 0;
    uint32_t if_cnt = ip_interface_cnt_get();

    IP2_CRIT_ASSERT_LOCKED();
    VTSS_TRACE(DEBUG) << "Process fwd state";
    for (if_no = 0; if_no < if_cnt; if_no++) {
        if (IP2_state.interfaces[if_no].vid == VTSS_VID_NULL) {
            continue;
        }

        if (IP2_state.interfaces[if_no].vid != fwd_state->iface[if_no].vid) {
            VTSS_TRACE(INFO) << "if" << if_no << ": Vlan mismatch "
                             << IP2_state.interfaces[if_no].vid << " != "
                             << fwd_state->iface[if_no].vid
                             << ", skipping";
            continue;
        }

        VTSS_TRACE(NOISE) << " if=" << if_no
                          << " fwd=" << fwd_state->iface[if_no].fwd
                          << " vid=" << IP2_state.interfaces[if_no].vid;
        if (IP2_state.unit_fwd[if_no] != fwd_state->iface[if_no].fwd) {
            IP2_state.unit_fwd[if_no] = fwd_state->iface[if_no].fwd;
            VTSS_TRACE(INFO) << " if " << if_no
                             << ": Changed to state "
                             << (IP2_state.unit_fwd[if_no]);
            changes++;
        }
    }

    if (changes) {
        /* Have main thread update combined state */
        VTSS_TRACE(DEBUG) << "Signal Changes " << changes;
        vtss_flag_setbits(&ip2_control_flags, CTLFLAG_IFCHANGE);
    }
}

static mesa_rc IP2_os_route_add(const mesa_routing_entry_t *const rt)
{
    IP2_CRIT_ASSERT_LOCKED();
    return vtss_ip_os_route_add(rt);
}

static mesa_rc IP2_os_route_del(const mesa_routing_entry_t *const rt)
{
    IP2_CRIT_ASSERT_LOCKED();
    return vtss_ip_os_route_del(rt);
}

static bool IP2_route_gw_can_be_reached_ipv4(mesa_ipv4_t dst) {
    notifications::LockGlobalSubject lock;
    const auto &st = status_if_ipv4.ref(lock);

    VTSS_TRACE(INFO) << "IP2_route_gw_can_be_reached_ipv4: " << AsIpv4(dst);

    for (const auto &e: st) {
        // Check if the net match
        if (!vtss_ipv4_net_include(&std::get<1>(e.first), &dst))
            continue;

        VTSS_TRACE(INFO) << "Matching network: " << std::get<1>(e.first);
        // Check if the associated interface has exists??
        vtss_appl_ip_if_status_link_t link;
        mesa_rc rc = status_if_link.get(std::get<0>(e.first), &link);
        if (rc != VTSS_RC_OK) {
            VTSS_TRACE(INFO) << "No if_status_link for : "
                    << std::get<0>(e.first);
            continue;
        }

        // Does it have link?
        if (link.flags & VTSS_APPL_IP_IF_LINK_FLAG_UP) {
            VTSS_TRACE(INFO) << "Reach through: "
                    << std::get<0>(e.first);
            return true;
        } else {
            VTSS_TRACE(INFO) << "Interface did not have link: "
                    << std::get<0>(e.first);
        }
    }

    return false;
}

static bool IP2_route_gw_can_be_reached_ipv6(const mesa_ipv6_t *dst) {
    notifications::LockGlobalSubject lock;
    const auto &st = status_if_ipv6.ref(lock);

    VTSS_TRACE(DEBUG) << "IP2_route_gw_can_be_reached_ipv6: " << *dst;

    for (const auto &e: st) {
        // Check if the net match
        if (!vtss_ipv6_net_include(&std::get<1>(e.first), dst) &&
            !vtss_ipv6_addr_is_link_local(dst))
            continue;

        VTSS_TRACE(DEBUG) << "Matching network: " << std::get<1>(e.first);
        // Check if the associated interface has exists??
        vtss_appl_ip_if_status_link_t link;
        mesa_rc rc = status_if_link.get(std::get<0>(e.first), &link);
        if (rc != VTSS_RC_OK) {
            VTSS_TRACE(DEBUG) << "No if_status_link for : "
                    << std::get<0>(e.first);
            continue;
        }

        // Does it have link?
        if (link.flags & VTSS_APPL_IP_IF_LINK_FLAG_UP) {
            return true;
        } else {
            VTSS_TRACE(DEBUG) << "Interface did not have link: "
                    << std::get<0>(e.first);
        }
    }

    return false;
}

static bool IP2_route_gw_can_be_reached(const mesa_routing_entry_t &r) {
    switch (r.type) {
        case MESA_ROUTING_ENTRY_TYPE_IPV4_MC:
        case MESA_ROUTING_ENTRY_TYPE_IPV4_UC:
            return IP2_route_gw_can_be_reached_ipv4(r.route.ipv4_uc.destination);

        case MESA_ROUTING_ENTRY_TYPE_IPV6_UC:
            return IP2_route_gw_can_be_reached_ipv6(&r.route.ipv6_uc.destination);

        default:
            return false;
    }
}

static bool IP2_route_equal_range_active(const vtss::Map<mesa_routing_entry_t, int> &db,
                                         const mesa_routing_entry_t &r) {

    for (const auto &i: db) {
        VTSS_TRACE(DEBUG) << "considering: " << i.first;

        // Types must match
        if (r.type != i.first.type) {
            VTSS_TRACE(DEBUG) << "skipping";
            continue;
        }

        // It must be active
        if (!installed_in_os(i.first)) {
            VTSS_TRACE(DEBUG) << "skipping - not active";
            continue;
        }

        // It must match range
        switch (r.type) {
            case MESA_ROUTING_ENTRY_TYPE_IPV6_UC:
                if (vtss_ipv6_network_equal(&r.route.ipv6_uc.network,
                                            &i.first.route.ipv6_uc.network)) {
                    VTSS_TRACE(DEBUG) << "match";
                    return true;
                }

                break;

            case MESA_ROUTING_ENTRY_TYPE_IPV4_UC:
                if (vtss_ipv4_network_equal(&r.route.ipv4_uc.network,
                                            &i.first.route.ipv4_uc.network)) {
                    VTSS_TRACE(DEBUG) << "match";
                    return true;
                }

                break;

            default:
                ;
        }
    }

    VTSS_TRACE(DEBUG) << "no match";
    return false;
}

void IP2RouteDbApplyIpv6::operator()() {
    CRIT_SCOPE();

    notifications::LockGlobalSubject lock;
    const auto &db = IP_conf_route_ipv6.ref(lock);

    // Step 1: Loop through all routes and delete all routes who's gateway can
    // not be reached throgh an interface route.
    VTSS_TRACE(DEBUG) << "Apply route DB - deleting dead routes";
    for (const auto &i: db) {
        // If the route is not active, then there is nothing more to do
        if (!installed_in_os(i.first)) {
            VTSS_TRACE(DEBUG) << "Route not active " << i.first;
            continue;
        }

        // If the route can still be reached, then leave it as is
        if (IP2_route_gw_can_be_reached(i.first)) {
            VTSS_TRACE(DEBUG) << "GW can still be reached " << i.first;
            continue;
        }

        // Otherwise delete it
        VTSS_TRACE(INFO) << "Del os-route " << i.first;

        // route may be deleted by os already!
        (void)IP2_os_route_del(&i.first);
    }

    // Step 2: Loop through and add all routes that can be reached through an
    // interface route. If multiple "equal" routes are found, only add the first
    VTSS_TRACE(DEBUG) << "Apply route DB - adding new routes";
    for (const auto &i: db) {
        // If the route is already active, record it and continue
        if (installed_in_os(i.first)) {
            VTSS_TRACE(DEBUG) << "Route active already " << i.first;
            continue;
        }

        // If the gateway associated to this route can not be reached then
        // "just" continue.
        if (!IP2_route_gw_can_be_reached(i.first)) {
            VTSS_TRACE(INFO) << "GW can not be reached " << i.first;
            continue;
        }

        // If equal range is already active then continue
        if (IP2_route_equal_range_active(db, i.first)) {
            VTSS_TRACE(DEBUG) << "Ranged already covered " << i.first;
            continue;
        }

        // Otherwise add it, mark it as active and record it
        VTSS_TRACE(INFO) << "Add os-route " << i.first;
        if (IP2_os_route_add(&i.first) == VTSS_RC_OK) {
            VTSS_TRACE(DEBUG) << "SUCCESS: add os-route " << i.first;
        } else {
            VTSS_TRACE(ERROR) << "ERROR: add os-route " << i.first;
        }
    }

    VTSS_TRACE(DEBUG) << "Apply route DB - DONE";
}

/* IP module don't need to determine which routes need to be applied
   to OS if FRR is included.
*/
#if !defined(VTSS_SW_OPTION_FRR)
void IP2RouteDbApplyIpv4::operator()() {
    CRIT_SCOPE();

    notifications::LockGlobalSubject lock;
    const auto &db = IP_conf_route_ipv4.ref(lock);

    // Step 1: Loop through all routes and delete all routes who's gateway can
    // not be reached throgh an interface route.
    VTSS_TRACE(DEBUG) << "Apply route DB - deleting dead routes";
    for (const auto &i: db) {
        // If the route is not active, then there is nothing more to do
        if (!installed_in_os(i.first)) {
            VTSS_TRACE(DEBUG) << "Route not active " << i.first;
            continue;
        }

        // If the route can still be reached, then leave it as is
        if (IP2_route_gw_can_be_reached(i.first)) {
            VTSS_TRACE(DEBUG) << "GW can still be reached " << i.first;
            continue;
        }

        // Otherwise delete it
        VTSS_TRACE(INFO) << "Del os-route " << i.first;

        // route may be deleted by os already!
        (void)IP2_os_route_del(&i.first);
    }

    // Step 2: Loop through and add all routes that can be reached through an
    // interface route. If multiple "equal" routes are found, only add the first
    VTSS_TRACE(DEBUG) << "Apply route DB - adding new routes";
    for (const auto &i: db) {
        // If the route is already active, record it and continue
        if (installed_in_os(i.first)) {
            VTSS_TRACE(DEBUG) << "Route active already " << i.first;
            continue;
        }

        // If the gateway associated to this route can not be reached then
        // "just" continue.
        if (!IP2_route_gw_can_be_reached(i.first)) {
            VTSS_TRACE(INFO) << "GW can not be reached " << i.first;
            continue;
        }

        // If equal range is already active then continue
        if (IP2_route_equal_range_active(db, i.first)) {
            VTSS_TRACE(DEBUG) << "Ranged already covered " << i.first;
            continue;
        }

        // Otherwise add it, mark it as active and record it
        VTSS_TRACE(INFO) << "Add os-route " << i.first;
        if (IP2_os_route_add(&i.first) == VTSS_RC_OK) {
            VTSS_TRACE(DEBUG) << "SUCCESS: add os-route " << i.first;
        } else {
            VTSS_TRACE(ERROR) << "ERROR: add os-route " << i.first;
        }
    }

    VTSS_TRACE(DEBUG) << "Apply route DB - DONE";
}
#endif /* !VTSS_SW_OPTION_FRR */

extern "C" void vtss_appl_ip_debug_print_route_db(vtss_ip_cli_pr *pr) {
    notifications::LockGlobalSubject lock;
    const auto &db4 = IP_conf_route_ipv4.ref(lock);
    StringStream bs;
    std::string s;
    for (const auto &i: db4) {
        const uint32_t &users = i.second;
        bs << i.first << " users: 0x" << FormatHex<const uint32_t>(users, 'a', 0, 0, '0');
        s = bs.buf;
        (void)pr("%s\n", s.c_str());
        bs.clear();
    }
    const auto &db6 = IP_conf_route_ipv6.ref(lock);
    bs.clear();
    for (const auto &i: db6) {
        bs << i.first;
        s += bs.buf;
        (void)pr("%s\n", s.c_str());
        bs.clear();
    }
}

static void IP2_route_db_reset(void)
{
    IP2_CRIT_ASSERT_LOCKED();

    VTSS_TRACE(INFO) << "Empty routes";
    {
        notifications::LockGlobalSubject lock;
        const auto &db4 = IP_conf_route_ipv4.ref(lock);
        for (const auto &i: db4) {
#if defined(VTSS_SW_OPTION_FRR)
            FrrIpRoute frr_re = {};
            VTSS_TRACE(INFO) << "redirect to zebra@FRR ";

            conv(&i.first, NULL, &frr_re);
            VTSS_TRACE(INFO) << "delete " << frr_re.net.route.ipv4_uc
                             << " with distacne " << frr_re.distance
                             << " and tag " << frr_re.tag;
            (void)frr_ip_route_conf_del(frr_re);
#else
            (void)IP2_os_route_del(&i.first);
#endif
        }

        const auto &db6 = IP_conf_route_ipv6.ref(lock);
        for (const auto &i: db6) {
            (void)IP2_os_route_del(&i.first);
        }
    }
    IP_conf_route_ipv4.clear();
    IP_conf_route_ipv6.clear();
}

static mesa_rc IP2_route_db_del_all_owners(const mesa_routing_entry_t *rte) {
    IP2_CRIT_ASSERT_LOCKED();

    if (rte->type == MESA_ROUTING_ENTRY_TYPE_IPV4_UC) {
        if (IP_conf_route_ipv4.del(*rte) != VTSS_RC_OK) {
            T_E("Unable to delete route in DB");
            return VTSS_RC_ERROR;
        }
    } else if (rte->type == MESA_ROUTING_ENTRY_TYPE_IPV6_UC) {
        if (IP_conf_route_ipv6.del(*rte) != VTSS_RC_OK) {
            T_E("Unable to delete route in DB");
            return VTSS_RC_ERROR;
        }
    }

#if defined(VTSS_SW_OPTION_FRR)
    // Ipv4 os route is deleted by frr/zebra,
    // whereas Ipv6 os route is deleted here
    if (rte->type == MESA_ROUTING_ENTRY_TYPE_IPV6_UC) {
        (void)IP2_os_route_del(rte);
    }
#else
    // both Ipv4 and Ipv6 os routes are deleted here
    (void)IP2_os_route_del(rte);
#endif /* VTSS_SW_OPTION_FRR */

    return VTSS_OK;
}

static void IP2_delete_all_interfaces(void)
{
    ip2_ifstate_t *ifs;
    uint i, vid;
    mesa_rc rc;

    IP2_CRIT_ASSERT_LOCKED();
    for (i = 0; i < VTSS_VIDS; i++) {
        if (!IP2_if_exists(i)) {
            continue;
        }

        ifs = IP2_if2state(i);
        if (!ifs) {
            continue;
        }

        vid = ifs->vid;
        rc = IP2_if_teardown(ifs);
        if (rc != VTSS_RC_OK) {
            T_E("IP2_if_teardown failed: if_no=%d, vid=%d msg=%s",
                i, vid, error_txt(rc));
        }
    }
}

/*
 * IP2_conf_default()
 * Initialize configuration to default.
 */
static void IP2_conf_default(ip_stack_conf_t *cfg)
{
    IP2_CRIT_ASSERT_LOCKED();

    VTSS_TRACE(INFO) << "Set default config";
    IP2_delete_all_interfaces();
    IP2_route_db_reset();
    IP2_state.static_routes = 0;

    vtss_clear(*cfg);
    cfg->global.enable_routing = FALSE;

    mesa_rc rc = vtss_ip_os_global_param_set(&IP2_stack_conf.global);
    if (rc != VTSS_OK) {
        T_W("Global config apply error - %s", error_txt(rc));
    }

#ifdef VTSS_SW_OPTION_DNS
    vtss_dns_signal();
#endif /* VTSS_SW_OPTION_DNS */
}

static BOOL IP2_if_exists(vtss_if_id_vlan_t if_id)
{
    IP2_CRIT_ASSERT_LOCKED();
    return IP2_if2state(if_id) != NULL;
}

static void IP2_if_cache_update()
{
#ifdef VTSS_SW_OPTION_LLDP
    lldp_something_has_changed();
#endif /* VTSS_SW_OPTION_LLDP */

    IP2_CRIT_ASSERT_LOCKED();

    // Something changed, all nodes must publish new fwd state
    poll_fwdstate();
}

static void IP2_if_signal(vtss_if_id_vlan_t if_id, vtss_ifindex_t ifidx) {
    IP2_CRIT_ASSERT_LOCKED();
    VTSS_TRACE(INFO) << "Signal vlan=" << if_id << ", ifidx=" << ifidx;

    /* update the cached if status */
    IP2_if_cache_update();

    VTSS_BF_SET(IP2_vlan_notify_bf, if_id, 1);

    /* Synch routes when interfaces change */
    vtss_flag_setbits(&ip2_control_flags, CTLFLAG_IFNOTIFY);
}

static void IP2_if_signal_vlan(vtss_if_id_vlan_t vid) {
    vtss_ifindex_t ifidx;
    if (vtss_ifindex_from_vlan(vid, &ifidx) != VTSS_RC_OK) return;
    IP2_if_signal(vid, ifidx);
}

void vtss_ip_if_signal(vtss_if_id_vlan_t if_id, vtss_ifindex_t ifindex)
{
    {
        CRIT_SCOPE();
        IP2_if_signal(if_id, ifindex);
    }

#ifdef VTSS_SW_OPTION_DNS
    vtss_dns_signal();
#endif /* VTSS_SW_OPTION_DNS */
}

static void vtss_ip2_if_callback(void)
{
    u32 i, vlan;
    u32 bf[VTSS_BF_SIZE(VTSS_VIDS)];

    {
        CRIT_SCOPE();
        memcpy(bf, IP2_vlan_notify_bf, sizeof(bf));
        memset(IP2_vlan_notify_bf, 0, sizeof(bf));
    }

    for (vlan = 0; vlan < VTSS_VIDS; ++vlan) {
        if (!VTSS_BF_GET(bf, vlan)) {
            continue;
        }

        VTSS_TRACE(INFO) << "Callback vlan id: " << vlan;
        for (i = 0; i < MAX_SUBSCRIBER; ++i) {
            vtss_ip_if_callback_t cb;

            // WARNING, Raise condition.... what if we copy a pointer to a
            // callback, release the semaphore, the callee deletes the callback,
            // we invoke the callback after it has been deleted.
            {
                CRIT_SCOPE();
                cb = IP2_subscribers[i];
            }

            if (cb == NULL) {
                continue;
            }

            (*cb)(vlan);
        }
    }
}

static mesa_rc IP2_ip_dhcpc_v4_stop_no_reapply(vtss_if_id_vlan_t if_id)
{
    mesa_rc rc = VTSS_RC_OK;

    IP2_CRIT_ASSERT_LOCKED();
    VTSS_TRACE(DEBUG) << "IP2_ip_dhcpc_v4_stop_no_reapply";

    rc = IP2_dhcp_clear(if_id);
    if (rc != VTSS_RC_OK) {
        T_W("Failed to clear dhcp settings");
    }

    rc = vtss::dhcp::client_kill(if_id);
    IP_dhcp_signal_vlan(if_id);

#ifdef VTSS_SW_OPTION_DNS
    vtss_dns_signal();
#endif /* VTSS_SW_OPTION_DNS */

    return rc;
}

static mesa_rc IP2_if_del(vtss_ifindex_t ifindex)
{
    mesa_rc rc;
    int vid, if_no;
    ip2_ifstate_t *ifs;
    struct ip_iface_entry *if_conf;
    mesa_vid_t vlan;

    IP2_CRIT_ASSERT_LOCKED();
    VTSS_RC(IP_ifindex_to_vlan(ifindex, &vlan));

    VTSS_TRACE(DEBUG) << __FUNCTION__ << " - vid " << vlan;

    ifs = IP2_if2state(vlan);
    if (!ifs) {
        T_W("Could not translate vlan %u to interface state", vlan);
        return IP_ERROR_PARAMS;
    }

    vid = ifs->vid;
    if_no = ifs->if_no;
    if_conf = &IP2_stack_conf.interfaces[if_no];

    IP2_if_ipv4_del(ifs);

    if (vtss_ip_hasipv6() && ifs->ipv6_active) {
        if (vtss_ip_os_ipv6_del(vlan, &ifs->cur_ipv6) == VTSS_OK) {
            ifs->ipv6_active = false;
        }
    }

    rc = IP2_if_teardown(ifs);

    /* Nuke interface */
    memset(if_conf, 0, sizeof(*if_conf));

    /* Notify here, as we can not convert if_index to vlan after this */
    IP2_if_signal(vid, ifindex);

    return rc;
}

static mesa_rc IP2_ipv4_conf_set(vtss_if_id_vlan_t               if_id,
                                 const vtss_appl_ip_ipv4_conf_t *conf,
                                 BOOL                            force)
{
    ip2_ifstate_t *ifs;
    mesa_rc rc = VTSS_RC_OK;
    vtss_appl_ip_ipv4_conf_t *cur;
    BOOL update_timer = FALSE;

    IP2_CRIT_ASSERT_LOCKED();

    VTSS_TRACE(DEBUG) <<  __FUNCTION__;
    ifs = IP2_if2state(if_id);
    if (!ifs) {
        return IP_ERROR_PARAMS;
    }

    cur = &(ifs->config->ipv4);

    VTSS_TRACE(DEBUG) << "conf:{active:" << conf->active
                      << ", dhcpc:" << conf->dhcpc
                      << "}, cur:{active:" << cur->active
                      << ", dhcpc:" << cur->dhcpc << "}";

    // Validate input
    if (conf->active &&
        (!conf->dhcpc || (conf->dhcpc && conf->fallback_timeout))) {

        if (!vtss_ip_ipv4_ifaddr_valid(&conf->network)) {
            VTSS_TRACE(INFO) << "Invalid ip address, vlan=" << if_id;
            return IP_ERROR_PARAMS;

        } else if (!IP2_if_ipv4_check(&conf->network, if_id)) {
            VTSS_TRACE(INFO) << "IP address checks failed, vlan=" << if_id;
            return IP_ERROR_ADDRESS_CONFLICT;
        }
    }

    // enable/disable dhcp client
    if (cur->active != conf->active || cur->dhcpc != conf->dhcpc || force) {
        VTSS_TRACE(INFO) << "Apply dhcp conf, vlan=" << if_id;

        if (conf->dhcpc && conf->active) {
            VTSS_TRACE(INFO) << "Start DHCP client on vlan if: " << if_id;
            rc = IP2_dhcpc_v4_start(ifs, conf);

        } else if (cur->dhcpc && cur->active) {  // DHCP client is active, disable it
            VTSS_TRACE(INFO) << "Stop DHCP client on vlan if: " << if_id;
            rc = IP2_ip_dhcpc_v4_stop_no_reapply(ifs->vid);
        }
    }

    // Fallback timeout
    if (cur->active != conf->active ||
        cur->fallback_timeout != conf->fallback_timeout || force) {
        update_timer = TRUE;
    }
    *cur = *conf;
    if (update_timer) {
        VTSS_TRACE(INFO) << "Apply fallback timer conf, vlan=" << if_id;
        IP2_dhcpc_fallback_timer_update(ifs);
    }

    VTSS_TRACE(DEBUG) << "Conf updated: {active:" << conf->active
                      << ", dhcpc:" << conf->dhcpc << "}";
    (void) IP2_if_ipv4_signal(ifs);

    IP2_ifflags_force_sync(ifs->if_no);
    return rc;
}

static mesa_rc IP2_ipv6_clear_conf_set(ip2_ifstate_t           *ifs)
{
    IP2_CRIT_ASSERT_LOCKED();
    if (ifs->ipv6_active)
        return IP2_ipv6_static_conf_del(ifs, &ifs->cur_ipv6);
    else
        return VTSS_RC_OK;
}

static mesa_rc IP2_ipv6_static_conf_set(ip2_ifstate_t             *ifs,
                                        const mesa_ipv6_network_t *network)
{
    unsigned i;
    uint32_t if_cnt = ip_interface_cnt_get();

    IP2_CRIT_ASSERT_LOCKED();

    if (!vtss_ip_ipv6_ifaddr_valid(network)) {
        T_W("Invalid interface address for vlan %d", ifs->vid);
        return IP_ERROR_PARAMS;
    }

    for (i = 0; i < if_cnt; i++) {
        ip2_ifstate_t *ifo = &IP2_state.interfaces[i];

        if (ifo->vid == VTSS_VID_NULL || /* Invalid entry */
                ifs == ifo) {                /* This is us... */
            continue;
        }

        if (vtss_ipv6_net_equal(network, &ifo->cur_ipv6)) {
            T_W("Trying to add identical IP network on VLAN %d, also on "
                    "VLAN %d", ifs->vid, ifo->vid);
            return IP_ERROR_EXISTS;
        }
    }

    /* Rid old address, if any */
    if (ifs->ipv6_active) {
        (void) vtss_ip_os_ipv6_del(ifs->vid, &ifs->cur_ipv6);
    }

    /* Install address if the VLAN is UP */
    mesa_rc rc = (IP2_state.vlan_state.up[ifs->vid] ? vtss_ip_os_ipv6_add(ifs->vid, network, NULL) : VTSS_RC_OK);
    if (rc == VTSS_RC_OK) {
        ifs->ipv6_active = TRUE;
        ifs->cur_ipv6 = *network;
    }

    return rc;
}

static mesa_rc IP2_ipv6_static_conf_del(ip2_ifstate_t             *ifs,
                                        const mesa_ipv6_network_t *network)
{
    IP2_CRIT_ASSERT_LOCKED();

    if (!vtss_ip_ipv6_ifaddr_valid(network)) {
        T_W("Invalid interface address for vlan %d", ifs->vid);
        return IP_ERROR_PARAMS;
    }

    /* Rid old address, if any */
    if (ifs->ipv6_active) {
        (void) vtss_ip_os_ipv6_del(ifs->vid, &ifs->cur_ipv6);
    }

    ifs->ipv6_active = false;
    memset(&ifs->cur_ipv6, 0x0, sizeof(mesa_ipv6_network_t));
    return VTSS_RC_OK;
}

static mesa_rc IP2_ipv6_conf_set(vtss_if_id_vlan_t               if_id,
                                 const vtss_appl_ip_ipv6_conf_t *conf)
{
    ip2_ifstate_t *ifs;
    mesa_rc rc = VTSS_RC_OK;

    IP2_CRIT_ASSERT_LOCKED();

    VTSS_TRACE(DEBUG) <<  __FUNCTION__;
    ifs = IP2_if2state(if_id);
    if (!ifs) {
        VTSS_TRACE(DEBUG) << "Invalid if_id: " << if_id;
        return IP_ERROR_PARAMS;
    }

    if (conf->active) {
        if (!IP2_if_ipv6_check(&conf->network, if_id)) {
            VTSS_TRACE(DEBUG) << "Check failed for " << conf->network <<
                " if_id: " << if_id;
            return IP_ERROR_ADDRESS_CONFLICT;
        }
    }

    if (conf->active) {
        // set static address
        VTSS_TRACE(INFO) << "Configuring static IPv6 on vlan if: " << if_id <<
            conf->network;
        rc = IP2_ipv6_static_conf_set(ifs, &conf->network);
    } else if (ifs->config->ipv6.active) {
        // clear all ipv6 addresses
        VTSS_TRACE(INFO) << "Clearing IPv6 addresses on vlan if " << if_id;
        rc = IP2_ipv6_clear_conf_set(ifs);
    } else {
        VTSS_TRACE(INFO) << "non active -> non-active";
        rc = VTSS_RC_OK;
    }

    if (rc == VTSS_RC_OK) {
        ifs->config->ipv6 = *conf;
        IP2_ifflags_force_sync(ifs->if_no);
    }
    return rc;
}

static BOOL IP2_route_check_owner(vtss_appl_ip_route_owner_t owner)
{
    IP2_CRIT_ASSERT_LOCKED();
    switch (owner) {
    case VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER:
    case VTSS_APPL_IP_ROUTE_OWNER_DYNAMIC_USER:
    case VTSS_APPL_IP_ROUTE_OWNER_DHCP:
        return TRUE;
    default:
        T_E("Invalid owner: %d", owner);
    }
    return FALSE;
}

static void conv(const mesa_ipv4_t &a, mesa_ip_addr_t &b) {
    b.type = MESA_IP_TYPE_IPV4;
    b.addr.ipv4 = a;
}

static void conv(const mesa_ipv4_network_t &a, mesa_ip_network_t &b) {
    conv(a.address, b.address);
    b.prefix_size = a.prefix_size;
}

static void conv(const mesa_ipv6_t &a, mesa_ip_addr_t &b) {
    b.type = MESA_IP_TYPE_IPV6;
    b.addr.ipv6 = a;
}

static void conv(const mesa_ipv6_network_t &a, mesa_ip_network_t &b) {
    conv(a.address, b.address);
    b.prefix_size = a.prefix_size;
}

static BOOL IP2_route_check_conflict(const mesa_routing_entry_t *const conf_rt) {
    // A route must not be equal to an interface route (for systems without
    // metric support)

#define BUF_SIZE 128
    u32 i;
    BOOL rc, ipv4;
    uint32_t if_cnt = ip_interface_cnt_get();

    IP2_CRIT_ASSERT_LOCKED();

    mesa_ip_network_t net;
    if (conf_rt->type == MESA_ROUTING_ENTRY_TYPE_IPV4_UC) {
        ipv4 = TRUE;
        conv(conf_rt->route.ipv4_uc.network, net);
    } else if (conf_rt->type == MESA_ROUTING_ENTRY_TYPE_IPV6_UC) {
        ipv4 = FALSE;
        conv(conf_rt->route.ipv6_uc.network, net);
    } else {
        VTSS_TRACE(INFO) << "Un-supported address type";
        return FALSE;
    }

    vtss::Vector<vtss_appl_ip_if_status_t> st(130);
    if (!st.capacity()) {
        T_E("Alloc error");
        return FALSE;
    }

    //////////////////////////////////////////////////////////////////////////
    // Check against active IP address, regardless where they come from

    // get status IPs
    vtss_appl_ip_if_status_type_t status_type =
        ipv4 ? VTSS_APPL_IP_IF_STATUS_TYPE_IPV4 : VTSS_APPL_IP_IF_STATUS_TYPE_IPV6;
    if (IP2_ifs_status_get(status_type, st) != VTSS_RC_OK) {
        T_E("Failed to get if status, ipv4=%d", ipv4);
        rc = FALSE;
        goto DONE;
    }

    rc = TRUE;
    for (const auto &e : st) {
        mesa_ip_network_t tmp;

        if (ipv4) conv(e.u.ipv4.net, tmp);
        else conv(e.u.ipv6.net, tmp);

        if (vtss_ip_net_equal(&net, &tmp)) {
            VTSS_TRACE(INFO) << "Route " << net << " conflicts with network "
                             << tmp << " at interface " << e.if_id.u.vlan;
            rc = FALSE;
            goto DONE;
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // Check against static configured addresses
    for (i = 0; i < if_cnt; ++i) {
        const ip2_ifstate_t *const ifs = &IP2_state.interfaces[i];
        vtss::IpNetwork if_addr;
        bool active = false;

        if (ifs->vid == VTSS_VID_NULL) continue;


        if (ipv4) {
            active  = IP2_ipv4_conf_static_address_active(ifs->config->ipv4);
            if_addr = ifs->config->ipv4.network;
        } else {
            active  = IP2_ipv6_conf_static_address_active(ifs->config->ipv6);
            if_addr = ifs->config->ipv6.network;
        }

        // Not active -> no conflict
        if (!active) {
            continue;
        }

        // check static ip address
        if (vtss_ip_net_equal(&net, &if_addr.as_api_type())) {
            VTSS_TRACE(INFO) << "Route " << net << " conflicts with network "
                             << if_addr.as_api_type();
            rc = FALSE;
            goto DONE;
        }
    }

DONE:
    return rc;
#undef BUF_SIZE
}

static mesa_rc IP2_route_add_ipv4(const mesa_routing_entry_t  *const rt,
                                  const vtss_routing_params_t *const params,
                                  const vtss_appl_ip_ipv4_route_conf_t *const conf)
{
    mesa_rc rc;
    int users = 0;

    VTSS_TRACE(INFO) << "route add " << *rt << " " << *params;

    IP2_CRIT_ASSERT_LOCKED();

    if (!vtss_ip_route_valid(rt) ||
        !IP2_route_check_owner(params->owner) ||
        !vtss_ip_route_nh_valid(rt) ||
        !IP2_route_check_conflict(rt)) {
        VTSS_TRACE(INFO) << "Failed to validate route";
        return IP_ERROR_PARAMS;
    }

    /* Do we have this already? */
    if (IP_conf_route_ipv4.get(*rt, &users) == VTSS_RC_OK) {
        if (users & VTSS_BIT(params->owner)) {
            VTSS_TRACE(INFO) << "Exists already - same user";
#if defined(VTSS_SW_OPTION_FRR)
            /* we need to update distance in FRR. */
            goto APPL_FRR;
#endif /* VTSS_SW_OPTION_FRR */
            return IP_ERROR_EXISTS;
        }
        /* Mark extra owner of route */
        users |= VTSS_BIT(params->owner);
    } else {
        users = VTSS_BIT(params->owner);
    }

    if (params->owner == VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER &&
        IP2_state.static_routes == MESA_CAP(VTSS_APPL_CAP_IP_ROUTE_CNT)) {
        VTSS_TRACE(INFO) << "no space";
        return IP_ERROR_NOSPACE;
    }

    /* Add to route DB */
    VTSS_TRACE(INFO) << "Adding route: " << *rt << " to route DB";
    rc = IP_conf_route_ipv4.set(*rt, users);
    if (rc != VTSS_RC_OK) {
        VTSS_TRACE(ERROR) << "Failed to add route: " << *rt;
        return rc;
    }

    if (params->owner == VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER) {
        IP2_state.static_routes++;
    }

    // If FRR is enabled, then install the route by using FRR, otherwise relay
    // on the observer to pickup the change and install the route.
    // Different from IP2_route_del_ipv4(), it's OK to add the same
    // route into FRR.
#if defined(VTSS_SW_OPTION_FRR)
APPL_FRR:
    FrrIpRoute frr_re;
    VTSS_TRACE(INFO) << "redirect to zebra@FRR ";

    VTSS_RC(conv(rt, conf, &frr_re));
    VTSS_TRACE(INFO) << "add " << frr_re.net.route.ipv4_uc
                     << " with distacne " << frr_re.distance
                     << " and tag " << frr_re.tag;

    rc = frr_ip_route_conf_set(frr_re);
    if (rc != VTSS_RC_OK) {
        VTSS_TRACE(ERROR) << "execute FRR command fail, rc = " << rc;
        return rc;
    }
#endif /* VTSS_SW_OPTION_FRR */

    return VTSS_OK;
}

static mesa_rc IP2_route_del_ipv4(const mesa_routing_entry_t  *const rt,
                                  const vtss_routing_params_t *const params)
{
    int users;

    IP2_CRIT_ASSERT_LOCKED();
    if (!IP2_route_check_owner(params->owner)) {
        return IP_ERROR_PARAMS;
    }

    if (IP_conf_route_ipv4.get(*rt, &users) != VTSS_RC_OK) {
        return IP_ERROR_NOTFOUND;
    }

    if (users & VTSS_BIT(params->owner)) {
        /* Remove user from route, possibly purge route */
        users &= ~VTSS_BIT(params->owner);

        if (users) {
            if (IP_conf_route_ipv4.set(*rt, users) != VTSS_RC_OK) {
                T_E("Failed to update route");
            }
        } else {
            IP2_route_db_del_all_owners(rt);
        /* Also delete the route in FRR */
#if defined(VTSS_SW_OPTION_FRR)
            mesa_rc rc = VTSS_RC_ERROR;
            FrrIpRoute frr_re = {};
            VTSS_TRACE(INFO) << "redirect to zebra@FRR ";

            VTSS_RC(conv(rt, NULL, &frr_re));
            VTSS_TRACE(INFO) << "delete " << frr_re.net.route.ipv4_uc
                             << " with distacne " << frr_re.distance
                             << " and tag " << frr_re.tag;
            if ((rc = frr_ip_route_conf_del(frr_re)) != VTSS_RC_OK) {
                VTSS_TRACE(ERROR) << "execute FRR command fail, rc = " << rc;
                return rc;
            }
#endif /* VTSS_SW_OPTION_FRR */
        }

        if (params->owner == VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER) {
            IP2_state.static_routes--;
        }

        return VTSS_OK;
    }

    return IP_ERROR_NOTFOUND;
}

static mesa_rc IP2_route_add_ipv6(const mesa_routing_entry_t  *const rt,
                                  const vtss_routing_params_t *const params)
{
    mesa_rc rc;
    int users = 0;

    VTSS_TRACE(INFO) << "route add " << *rt << " " << *params;

    IP2_CRIT_ASSERT_LOCKED();

    if (!vtss_ip_route_valid(rt) ||
        !IP2_route_check_owner(params->owner) ||
        !vtss_ip_route_nh_valid(rt) ||
        !IP2_route_check_conflict(rt)) {
        VTSS_TRACE(INFO) << "Failed to validate route";
        return IP_ERROR_PARAMS;
    }

    /* Do we have this already? */
    if (IP_conf_route_ipv6.get(*rt, &users) == VTSS_RC_OK) {
        if (users & VTSS_BIT(params->owner)) {
            VTSS_TRACE(INFO) << "Exists already - same user";
            return IP_ERROR_EXISTS;
        }
        /* Mark extra owner of route */
        users |= VTSS_BIT(params->owner);
    } else {
        users = VTSS_BIT(params->owner);
    }

    if (params->owner == VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER &&
        IP2_state.static_routes == MESA_CAP(VTSS_APPL_CAP_IP_ROUTE_CNT)) {
        VTSS_TRACE(INFO) << "no space";
        return IP_ERROR_NOSPACE;
    }

    /* Add to route DB */
    VTSS_TRACE(INFO) << "Adding route: " << *rt << " to route DB";
    rc = IP_conf_route_ipv6.set(*rt, users);
    if (rc != VTSS_RC_OK) {
        VTSS_TRACE(ERROR) << "Failed to add route: " << *rt;
        return rc;
    }

    if (params->owner == VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER) {
        IP2_state.static_routes++;
    }

    return VTSS_OK;
}

static mesa_rc IP2_route_del_ipv6(const mesa_routing_entry_t  *const rt,
                             const vtss_routing_params_t *const params)
{
    int users;

    IP2_CRIT_ASSERT_LOCKED();
    if (!IP2_route_check_owner(params->owner)) {
        return IP_ERROR_PARAMS;
    }

    if (IP_conf_route_ipv6.get(*rt, &users) != VTSS_RC_OK) {
        return IP_ERROR_NOTFOUND;
    }

    if (users & VTSS_BIT(params->owner)) {
        /* Remove user from route, possibly purge route */
        users &= ~VTSS_BIT(params->owner);

        if (users) {
            if (IP_conf_route_ipv6.set(*rt, users) != VTSS_RC_OK) {
                T_E("Failed to update route");
            }
        } else {
            IP2_route_db_del_all_owners(rt);
        }

        if (params->owner == VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER) {
            IP2_state.static_routes--;
        }

        return VTSS_OK;
    }

    return IP_ERROR_NOTFOUND;
}

static void vtss_ip_if_mo_flag_notify(mesa_vid_t vid)
{
    IP2_CRIT_ASSERT_LOCKED();
    if (VTSS_BF_GET(IP2_if_MO_flg_update, vid)) {
#ifdef VTSS_SW_OPTION_DHCP6_CLIENT
        u8  mo_flg=0;
        u32 idx;

        if (vtss_ip_os_vlan_to_if_index(vid, &idx) == VTSS_RC_OK) {
            if ((vtss_ip_os_if_ifp_ra_flags(vid, &mo_flg) != VTSS_RC_OK) ||
                (vtss_ip_os_if_notify_dhcp6(idx, NULL, NULL, NULL, &mo_flg) != VTSS_RC_OK)) {
                VTSS_TRACE(NOISE) << "MO flag not updated for vlan id " << vid;
            }
        }
        VTSS_BF_CLR(IP2_if_MO_flg_update, vid);
#endif /* VTSS_SW_OPTION_DHCP6_CLIENT */
    }
}

inline static void IP2_if_poll_fwd_state(uint if_no)
{
    IP2_CRIT_ASSERT_LOCKED();

    if (IP2_state.interfaces[if_no].vid != VTSS_VID_NULL) {
        ip_vif_fwd_t new_fwd = VIF_FWD_BLOCKING; /* Assume blocking */
        ip_vif_fwd_t old_fwd = IP2_state.master_fwd[if_no];

        VTSS_TRACE(DEBUG) << "iface " << if_no << " cur state " << old_fwd;

        if (IP2_state.unit_fwd[if_no] == VIF_FWD_FORWARDING) {
            new_fwd = VIF_FWD_FORWARDING;
            VTSS_TRACE(DEBUG) << "iface " << if_no << " is forwarding";
        }

        VTSS_TRACE(DEBUG) << "iface " << if_no << " new state " << new_fwd;

        if (old_fwd != new_fwd) {
            IP2_state.master_fwd[if_no] = new_fwd;
            VTSS_TRACE(INFO) << "iface " << if_no << " change state from "
                             << old_fwd << " to " << new_fwd;
            if (old_fwd != new_fwd) {
#ifdef VTSS_SW_OPTION_DHCP6_CLIENT
                i32 prev = (old_fwd == VIF_FWD_FORWARDING ? 1 : -1);
                i32 curr = (new_fwd == VIF_FWD_FORWARDING ? 1 : -1);
                u32 idx;

                if (vtss_ip_os_vlan_to_if_index(IP2_state.interfaces[if_no].vid, &idx) != VTSS_RC_OK ||
                    vtss_ip_os_if_notify_dhcp6(idx, NULL, &prev, &curr, NULL) != VTSS_RC_OK) {
                    VTSS_TRACE(DEBUG) << "Failed to Notify DHCP6C(LINK)";
                }
#endif /* VTSS_SW_OPTION_DHCP6_CLIENT */
#ifdef VTSS_SW_OPTION_SYSLOG
                S_N("LINK-UPDOWN: Interface Vlan %d, changed state to %s.",
                    IP2_state.interfaces[if_no].vid,
                    new_fwd == VIF_FWD_FORWARDING ? "up" : "down");
#endif /* VTSS_SW_OPTION_SYSLOG */
            }
            IP2_if_signal_vlan(IP2_state.interfaces[if_no].vid);
        }
    }
}

static void ip_dhcpv6_timer(u32 timeout)
{
    uint          if_no;
    ip2_ifstate_t *ifs;

    CRIT_SCOPE();
    for (if_no = 0; if_no < IP2_state.interfaces.size(); if_no++) {
        ifs = &IP2_state.interfaces[if_no];
        if (ifs->vid != VTSS_VID_NULL && ifs->dhcpv6.active) {
            if (ifs->dhcpv6.info.lt_valid <= timeout) {
                VTSS_TRACE(INFO) << "DHCPv6 timeout, vid: " << ifs->vid << ", network: " << ifs->dhcpv6.network;
                (void)vtss_ip_os_ipv6_del(ifs->vid, &ifs->dhcpv6.network);
                ifs->dhcpv6.active = false;
            } else {
                ifs->dhcpv6.info.lt_valid -= timeout;
            }
        }
    }
}

static void ip2_thread(vtss_addrword_t data)
{
    vtss_flag_value_t flags;
    vtss_tick_count_t wakeup;
    vtss_tick_count_t sleep_time;
    u32               timeout = 3;
    wakeup = vtss_current_time() + VTSS_OS_MSEC2TICK(timeout * 1000);
    uint32_t if_cnt = ip_interface_cnt_get();

    for (;;) {
        VTSS_TRACE(NOISE) << "TICK";
        wakeup = vtss_current_time() + VTSS_OS_MSEC2TICK(timeout * 1000);
        {
            CRIT_SCOPE();
            poll_fwdstate();
            sleep_time = IP2_dhcpc_fallback_timer_evaluate(wakeup);
        }
        VTSS_TRACE(NOISE) << "sleep_time " << sleep_time;

        do {
            flags = vtss_flag_timed_wait(&ip2_control_flags, 0xffff,
                                         VTSS_FLAG_WAITMODE_OR_CLR,
                                         sleep_time);

            if (flags & CTLFLAG_IFCHANGE) {
                uint if_no;

                CRIT_SCOPE();
                for (if_no = 0; if_no < if_cnt; if_no++) {
                    IP2_if_poll_fwd_state(if_no);
                }
            }

            if (flags & CTLFLAG_IFNOTIFY) {
                vtss_ip2_if_callback();
            }

            if (flags & CTLFLAG_VLAN_CHANGE) {
                (void)vtss::appl::ip::filter::port_conf_update();
            }

            if (flags & CTLFLAG_MO_FLAGS_CHANGE) {
                mesa_vid_t vid;

                CRIT_SCOPE();
                for (vid = VTSS_APPL_VLAN_ID_MIN; vid < VTSS_APPL_VLAN_ID_MAX; vid++) {
                    vtss_ip_if_mo_flag_notify(vid);
                }
            }
        } while (flags);

        {
            // Check for VLAN ingress filtering changes
            mesa_packet_vlan_status_t vlan_status;

            if (mesa_packet_vlan_status_get(NULL, &vlan_status) == VTSS_RC_OK && vlan_status.changed) {
                vtss_flag_setbits(&ip2_control_flags, CTLFLAG_VLAN_CHANGE);
            }
        }
        ip_dhcpv6_timer(timeout);
    }
}


void vtss_ip_vlan_state_update(ip_vlan_state_t *state)
{
    mesa_vid_t    vid;
    ip2_ifstate_t *ifs;
    BOOL          up;

    CRIT_SCOPE();
    for (vid = 1; vid < MESA_VIDS; vid++) {
        up = state->up[vid];
        ifs = IP2_if2state(vid);
        if (ifs != NULL && IP2_state.vlan_state.up[vid] != up) {
            T_I("VLAN %u %s", vid, up ? "UP" : "DOWN");
            if (vtss_ip_os_if_ctl(vid, up) != VTSS_RC_OK) {
                T_W("vtss_ip_os_if_ctrl failed, vid: %u", vid);
            } else if (up) {
                if (ifs->ipv6_active && vtss_ip_os_ipv6_add(vid, &ifs->cur_ipv6, NULL) != VTSS_RC_OK) {
                    /* Install IPv6 address when interface comes up */
                    T_W("vtss_ip_os_ipv6_add failed (static), vid: %u", vid);
                }
                if (ifs->dhcpv6.active && vtss_ip_os_ipv6_add(vid, &ifs->dhcpv6.network, NULL) != VTSS_RC_OK) {
                    /* Install IPv6 address when interface comes up */
                    T_W("vtss_ip_os_ipv6_add failed (dhcp), vid: %u", vid);
                }
            }
        }
    }
    IP2_state.vlan_state = *state;
}

void vtss_ip_if_mo_flag_update(mesa_vid_t vid)
{
    VTSS_BF_SET(IP2_if_MO_flg_update, vid, 1);
    vtss_flag_setbits(&ip2_control_flags, CTLFLAG_MO_FLAGS_CHANGE);
}
/* -------------------------------------------------------------------------
   Implementation of public API starts here
   ------------------------------------------------------------------------- */
const char *ip_error_txt(mesa_rc rc)
{
    switch (rc) {
    case IP_ERROR_EXISTS:
        return "Already exists";
    case IP_ERROR_NOTFOUND:
        return "Not Found";
    case IP_ERROR_NOSPACE:
        return "No space";
    case IP_ERROR_PARAMS:
        return "Invalid parameters";
    case IP_ERROR_FAILED:
        return "Operation failed";
    case IP_ERROR_ADDRESS_CONFLICT:
        return "Address conflict";
    case IP_ERROR_NOT_MASTER:
        return "Not master";
    case IP_ERROR_DHCP_CLIENT_HOSTNAME:
        return "Invalid DHCP client hostname. "
               "A valid name consist of a sequence of domain labels "
               "separated by \".\", each domain label starting and ending "
               "with an alphanumeric character and possibly also "
               "containing \"-\" characters. The length of a domain label "
               "must be 63 characters or less";
    case IP_ERROR_DHCP_CLIENT_ID:
        return "Invalid DHCP client identifiers value. The minimum length"
               "is one. When it is a hexadecimal string, the length must "
               "be even since one byte hex value is presented as two octets string";
    default:
        return "IP: \?\?\?";
    }
}

/* Global config ----------------------------------------------------------- */
mesa_rc vtss_appl_ip_global_param_set(const vtss_appl_ip_global_param_t *const param)
{
    mesa_rc rc;
    vtss_ip_global_param_priv_t conf;
    rc = vtss_ip_global_param_get_(&conf);

    if (rc != VTSS_RC_OK) {
        return rc;
    }

    conf.enable_routing = param->enable_routing;
    rc = vtss_ip_global_param_set_(&conf);
    return rc;
}

mesa_rc vtss_appl_ip_global_param_get(vtss_appl_ip_global_param_t *const param)
{
    vtss_ip_global_param_priv_t conf;
    mesa_rc rc = vtss_ip_global_param_get_(&conf);
    param->enable_routing = conf.enable_routing;
    return rc;
}

mesa_rc vtss_ip_global_param_set_(const vtss_ip_global_param_priv_t *const param)
{
    mesa_rc rc;

    CRIT_SCOPE();
    IP2_stack_conf.global = *param;
#if !defined(VTSS_SW_OPTION_L3RT)
    IP2_stack_conf.global.enable_routing = 0;
#endif
    rc = vtss_ip_os_global_param_set(param);

#ifdef VTSS_SW_OPTION_SNMP
    // we need to update a timer when forwarding is enabled/disabled
    vtss_ip_snmp_signal_global_changes();
#endif

    return rc;
}

mesa_rc vtss_ip_global_param_get_(vtss_ip_global_param_priv_t *param)
{
    CRIT_SCOPE();
    *param = IP2_stack_conf.global;
#if !defined(VTSS_SW_OPTION_L3RT)
    param->enable_routing = 0;
#endif
    return VTSS_OK;
}

/* Interface functions ----------------------------------------------------- */
BOOL vtss_ip_if_exists(vtss_if_id_vlan_t if_id)
{
    CRIT_SCOPE();
    return IP2_if_exists(if_id);
}

/*
    This function is used to validate the input network (n) of an existing IP interface (if_id).
    It mainly checks network overlapping w.r.t the given interface's network in our system.
*/
BOOL vtss_ip_if_address_valid(vtss_if_id_vlan_t if_id, const mesa_ip_network_t *const n)
{
    if (n->address.type == MESA_IP_TYPE_NONE) {
        return TRUE;
    } else {
        if (n->address.type != MESA_IP_TYPE_IPV4 && n->address.type != MESA_IP_TYPE_IPV6) {
            return FALSE;
        }
    }

    CRIT_SCOPE();
    if (!IP2_if_exists(if_id)) {
        return FALSE;
    }

    return IP2_if_ip_check(n, if_id);
}

mesa_rc IP2_if_id_next(vtss_if_id_vlan_t  current,
                       vtss_if_id_vlan_t *const next)
{
    T_R("Get-next iface %d", current);

    // First candidat is the next
    current++;
    for (; current <= VTSS_APPL_VLAN_ID_MAX; ++current ) {
        if (IP2_state.vid2if[current] != IFNO_INVALID) {
            *next = current;
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

mesa_rc vtss_ip_if_id_next(vtss_if_id_vlan_t  current,
                           vtss_if_id_vlan_t *const next)
{
    CRIT_SCOPE();
    return IP2_if_id_next(current, next);
}

mesa_rc vtss_appl_ip_if_conf_itr(const vtss_ifindex_t        *const in,
                                 vtss_ifindex_t              *const out)
{
    mesa_rc rc;
    mesa_vid_t vlan_in, vlan_out;

    if (in) {
        VTSS_TRACE(DEBUG) << "in: " << VTSS_IFINDEX_PRINTF_ARG(*in);
        if (*in < VTSS_IFINDEX_VLAN_OFFSET) {
            vlan_in = 0;
        } else if (*in > VTSS_IFINDEX_VLAN_OFFSET + VTSS_APPL_VLAN_ID_MAX) {
            return VTSS_RC_ERROR;
        } else {
            vlan_in = vtss_ifindex_cast_to_u32(*in) -
                    vtss_ifindex_cast_to_u32(VTSS_IFINDEX_VLAN_OFFSET);
        }
    } else {
        VTSS_TRACE(DEBUG) << "in: nullptr";
        vlan_in = 0;
    }

    VTSS_TRACE(DEBUG) << "vlan_in: " << vlan_in;
    rc = vtss_ip_if_id_next(vlan_in, &vlan_out);
    if (rc != VTSS_RC_OK) {
        return rc;
    }

    VTSS_TRACE(DEBUG) << "vlan_out " << vlan_out;
    rc = vtss_ifindex_from_vlan(vlan_out, out);
    VTSS_TRACE(DEBUG) << "" << vlan_in << " -> " << vlan_out
                      << " (" << *out << ")";
    return rc;
}

// TODO, delete this function
mesa_rc vtss_ip_if_conf_get_(vtss_if_id_vlan_t  if_id,
                             vtss_if_param_t   *const param)
{
    ip2_ifstate_t *ifs;

    VTSS_TRACE(NOISE) << __FUNCTION__ << " - vid " << if_id;

    CRIT_SCOPE();
    if ((ifs = IP2_if2state(if_id))) {
        /* Get params */
        *param = ifs->config->link_layer;
        return VTSS_OK;
    }

    return IP_ERROR_PARAMS;
}

mesa_rc vtss_appl_ip_if_conf_get(vtss_ifindex_t i)
{
    mesa_vid_t v;
    vtss_if_param_t p;

    VTSS_RC(IP_ifindex_to_vlan(i, &v));
    VTSS_RC(vtss_ip_if_conf_get_(v, &p));

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_ip_if_conf_set(vtss_ifindex_t ifidx)
{
    mesa_rc rc;
    mesa_vid_t vlan;
    vtss_if_param_t param;
    ip2_ifstate_t *ifs;

    VTSS_RC(IP_ifindex_to_vlan(ifidx, &vlan));
    vtss_if_default_param(&param);
    VTSS_TRACE(DEBUG) << __FUNCTION__ << " - ifidx=" << ifidx
                      << ", vlan=" << vlan;

    CRIT_SCOPE();

    if (vlan < VTSS_APPL_VLAN_ID_MIN || vlan > VTSS_APPL_VLAN_ID_MAX) {
        return IP_ERROR_PARAMS;
    }

    ifs = IP2_if2state(vlan);
    if (!ifs) {
        // Add new interface
        ifs = IP2_if_new(vlan);
        if (ifs == NULL) {
            T_D("IP2_if_new failed vlan: %u", vlan);
            return IP_ERROR_NOSPACE;
        }

        rc = vtss_ip_os_if_add(ifs->vid);
        if (rc != VTSS_RC_OK) {
            T_W("vtss_ip_os_if_add failed vlan: %u", vlan);
        }

        // The administrative mode is UP if the VLAN is UP
        if (IP2_state.vlan_state.up[ifs->vid]) {
            rc = vtss_ip_os_if_ctl(ifs->vid, true);
            if (rc != VTSS_RC_OK) {
                T_W("vlan%d: IFF flags set: %s", ifs->vid, error_txt(rc));
            }
        }
    }

    // update interface settings
    rc = vtss_ip_os_if_set(ifs->vid, &param);
    if (rc != VTSS_RC_OK) {
        T_E("Failed to apply interface parameters. Vlan: %u", vlan);
    }

    ifs->config->link_layer = param;
    IP2_if_signal(ifs->vid, ifidx);
    return rc;
}

// TODO, delete this
mesa_rc vtss_ip_if_conf_del_(vtss_ifindex_t ifindex)
{
    CRIT_SCOPE();
    return IP2_if_del(ifindex);
}

mesa_rc vtss_appl_ip_if_conf_del(vtss_ifindex_t i) {
    mesa_vid_t v;

    VTSS_RC(IP_ifindex_to_vlan(i, &v));
#ifdef VTSS_SW_OPTION_DHCP6_CLIENT
    u32 idx;

    VTSS_RC(vtss_ip_os_vlan_to_if_index(v, &idx));
    VTSS_RC(vtss_ip_os_if_notify_dhcp6(idx, NULL, NULL, NULL, NULL));
#endif /* VTSS_SW_OPTION_DHCP6_CLIENT */
    VTSS_RC(vtss_ip_if_conf_del_(i));

    return VTSS_RC_OK;
}

mesa_rc IP2_if_non_os_status_get(vtss_appl_ip_if_status_type_t           type,
                                 vtss_if_id_vlan_t                       id,
                                 vtss::Vector<vtss_appl_ip_if_status_t> &st) {
    ip2_ifstate_t *ifs;

    ifs = IP2_if2state(id);
    T_R("Considering vlan %u", id);
    if (ifs == NULL) {
        T_I("Failed to get ifs for vlan %u", id);
        return VTSS_RC_ERROR;
    }

    if (ifs->config == NULL) {
        T_E("How did this happen (VID = %u)", id);
        return VTSS_RC_ERROR;
    }

    // check if this interface uses a dhcp v4 client
    if ((type != VTSS_APPL_IP_IF_STATUS_TYPE_DHCP &&
         type != VTSS_APPL_IP_IF_STATUS_TYPE_ANY) ||
        !ifs->config->ipv4.active || !ifs->config->ipv4.dhcpc)
        return VTSS_RC_OK;

    // get dhcp v4 client status
    vtss_appl_ip_dhcp_client_status_t status_;
    mesa_rc rc_ = vtss::dhcp::client_status(id, &status_);

    if (rc_ != VTSS_RC_OK) {
        T_E("Failed to get DHCP status for vlan %u", id);
        return rc_;
    }

    vtss_appl_ip_if_status_t s = {};
    s.if_id.type = VTSS_ID_IF_TYPE_VLAN;
    s.if_id.u.vlan = id;
    s.type = VTSS_APPL_IP_IF_STATUS_TYPE_DHCP;
    s.u.dhcp4c = status_;
    st.push_back(s);

    // Add other interface status objects here

    return VTSS_RC_OK;
}

mesa_rc IP2_if_status_get(vtss_appl_ip_if_status_type_t           type,
                          vtss_if_id_vlan_t                       id,
                          vtss::Vector<vtss_appl_ip_if_status_t> &status)
{
    IP2_CRIT_ASSERT_LOCKED();
    status.clear();

    // get os status
    VTSS_RC(vtss_ip_os_if_status(type, id, status));

    // get non-os status
    VTSS_RC(IP2_if_non_os_status_get(type, id, status));

    return VTSS_RC_OK;
}

mesa_rc vtss_ip_if_status_get(vtss_appl_ip_if_status_type_t        type,
                              vtss_if_id_vlan_t                    id,
                              const u32                            max,
                              u32                                 *cnt,
                              vtss_appl_ip_if_status_t            *status)
{
    CRIT_SCOPE();

    vtss::Vector<vtss_appl_ip_if_status_t> v(max);
    v.max_size(max);

    VTSS_RC(IP2_if_status_get(type, id, v));

    *cnt = v.size();
    for (const auto &e : v) *status++ = e;

    return VTSS_RC_OK;
}

mesa_rc IP2_if_status_get_first(vtss_appl_ip_if_status_type_t        type,
                                vtss_if_id_vlan_t                    id,
                                vtss_appl_ip_if_status_t            *status)
{
    IP2_CRIT_ASSERT_LOCKED();

    vtss::Vector<vtss_appl_ip_if_status_t> v(1);
    v.max_size(1);
    VTSS_RC(IP2_if_status_get(type, id, v));

    if (v.size() != 1) return VTSS_RC_ERROR;

    *status = v.front();

    return VTSS_RC_OK;
}

static mesa_rc IP2_ifs_status_get(vtss_appl_ip_if_status_type_t           type,
                                  vtss::Vector<vtss_appl_ip_if_status_t> &st) {
    mesa_vid_t cur, next;

    IP2_CRIT_ASSERT_LOCKED();

    // get all os status
    VTSS_RC(vtss_ip_os_if_status_all(type, st));

    // get all non os status
    for (cur = 0; IP2_if_id_next(cur, &next) == VTSS_RC_OK; cur = next) {
        if (IP2_if_non_os_status_get(type, next, st) != VTSS_RC_OK)
            T_E("IP2_if_non_os_status_get failed for vlan %u", next);
    }

    return VTSS_RC_OK;
}

mesa_rc vtss_ip_ifs_status_get(vtss_appl_ip_if_status_type_t   type,
                               const u32                       max,
                               u32                            *cnt,
                               vtss_appl_ip_if_status_t       *status)
{
    CRIT_SCOPE();
    vtss::Vector<vtss_appl_ip_if_status_t> v;
    v.max_size(max);

    VTSS_RC(IP2_ifs_status_get(type, v));

    for (const auto &e : v) *status++ = e;
    *cnt = v.size();

    return VTSS_RC_OK;
}

static void ip_if_callback_test(vtss_if_id_vlan_t if_id)
{
    vtss_ifindex_t           ifidx;
    vtss_appl_ip_if_status_t status;

    if (vtss_ifindex_from_vlan(if_id, &ifidx) == VTSS_RC_OK &&
        vtss_appl_ip_if_status_get_first(VTSS_APPL_IP_IF_STATUS_TYPE_IPV4, ifidx, &status) == VTSS_OK) {
        VTSS_TRACE(INFO) << "vid: " << if_id << ", ipv4: " << status.u.ipv4;
    } else {
        VTSS_TRACE(INFO) << "vid: " << if_id << ", no ipv4";
    }
}

mesa_rc vtss_ip_if_callback_add(const vtss_ip_if_callback_t cb)
{
    int i;

    CRIT_SCOPE();
    for (i = 0; i < MAX_SUBSCRIBER; ++i) {
        if (IP2_subscribers[i] != NULL) {
            continue;
        }

        IP2_subscribers[i] = cb;
        return VTSS_RC_OK;
    }
    return VTSS_RC_ERROR;
}

mesa_rc vtss_ip_if_callback_del(const vtss_ip_if_callback_t cb)
{
    int i;

    CRIT_SCOPE();
    for (i = 0; i < MAX_SUBSCRIBER; ++i) {
        if (IP2_subscribers[i] != cb) {
            continue;
        }

        IP2_subscribers[i] = NULL;
        return VTSS_RC_OK;
    }
    return VTSS_RC_ERROR;
}

void vtss_if_default_param(vtss_if_param_t *param)
{
    param->mtu = 1500;
}

/* IP address functions ---------------------------------------------------- */
mesa_rc vtss_appl_ip_ipv4_conf_get(vtss_ifindex_t i,
                                   vtss_appl_ip_ipv4_conf_t *const conf)
{
    ip2_ifstate_t *ifs;

    CRIT_SCOPE();
    ifs = IP2_ifidx2state(i);

    if (!ifs) {
        return IP_ERROR_PARAMS;
    }

    *conf = ifs->config->ipv4;
    return VTSS_RC_OK;
}

mesa_rc vtss_appl_ip_ipv4_conf_set(vtss_ifindex_t i,
                                   const vtss_appl_ip_ipv4_conf_t *const conf)
{
    mesa_vid_t         v;
    vtss_ifindex_elm_t elm;

    /* Check input parameters */
    // client-id
    switch(conf->dhcpc_params.client_id.type) {
        case VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_AUTO:
            // Do nothing
            break;
        case VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_IFMAC:
            if (vtss_ifindex_decompose(conf->dhcpc_params.client_id.if_mac, &elm) != VTSS_RC_OK ||
                (elm.iftype != VTSS_IFINDEX_TYPE_NONE && elm.iftype != VTSS_IFINDEX_TYPE_PORT)) {
                // Only allow none or port type of ifindex
                return IP_ERROR_PARAMS;
            }
            break;
        case VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_ASCII:
            if (strlen(conf->dhcpc_params.client_id.ascii) < VTSS_APPL_IP_DHCP_CLIENT_ID_MIN_LENGTH) {
                return IP_ERROR_DHCP_CLIENT_ID;
            }
            break;            
        case VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_HEX:
            if (strlen(conf->dhcpc_params.client_id.hex) < (VTSS_APPL_IP_DHCP_CLIENT_ID_MIN_LENGTH * 2) ||
                misc_str_is_hex(conf->dhcpc_params.client_id.hex) != VTSS_OK) {
                return IP_ERROR_DHCP_CLIENT_ID;
            }
            break;
        default: // Unknown types
            return IP_ERROR_PARAMS;
    }

    // hostname
    if (strlen(conf->dhcpc_params.hostname) &&
        misc_str_is_domainname(conf->dhcpc_params.hostname) != VTSS_OK) {
        return IP_ERROR_DHCP_CLIENT_HOSTNAME;
    }

    VTSS_RC(IP_ifindex_to_vlan(i, &v));
    IP_PUBLIC_API_FUNCTION(IP2_ipv4_conf_set, v, conf, FALSE);
}

mesa_rc IP_ipv4_dhcp_restart(vtss_ifindex_t ifidx) {
    mesa_vid_t v;
    ip2_ifstate_t *ifs;
    VTSS_RC(IP_ifindex_to_vlan(ifidx, &v));

    ifs = IP2_ifidx2state(ifidx);

    if (!ifs)
        return IP_ERROR_PARAMS;

    if (!ifs->config->ipv4.dhcpc)
        return VTSS_RC_ERROR;

    if (ifs->ipv4_active)
        (void) IP2_if_ipv4_del(ifs);

    return vtss::dhcp::client_start(v, &ifs->config->ipv4.dhcpc_params);
}

mesa_rc vtss_appl_ip_ipv6_conf_set(vtss_ifindex_t                  ifidx,
                                   const vtss_appl_ip_ipv6_conf_t *const conf)
{
    ip2_ifstate_t   *ifs;

    CRIT_SCOPE();
    if ((ifs = IP2_ifidx2state(ifidx)) == NULL) {
        return IP_ERROR_PARAMS;
    }

    if (!memcmp(conf, &ifs->config->ipv6, sizeof(vtss_appl_ip_ipv6_conf_t))) {
        return VTSS_RC_OK;
    }

    return IP2_ipv6_conf_set(ifs->vid, conf);
}

mesa_rc vtss_appl_ip_ipv6_conf_get(vtss_ifindex_t            ifidx,
                                   vtss_appl_ip_ipv6_conf_t *const conf)
{
    ip2_ifstate_t *ifs;

    CRIT_SCOPE();
    ifs = IP2_ifidx2state(ifidx);

    if (!ifs) {
        return IP_ERROR_PARAMS;
    }

    *conf = ifs->config->ipv6;
    return VTSS_RC_OK;
}

static mesa_rc conv(const vtss_appl_ip_ipv6_route_conf_t *const a,
                    mesa_routing_entry_t                 *const b) {
    b->type = MESA_ROUTING_ENTRY_TYPE_IPV6_UC;
    memcpy(&b->route.ipv6_uc, &a->route, sizeof(b->route.ipv6_uc));

    b->vlan = 0;
    if (a->interface <= VTSS_IFINDEX_VLAN_OFFSET) {
        b->vlan = 0;
    } else if (a->interface >= VTSS_IFINDEX_VLAN_OFFSET + 4096) {
        b->vlan = 4096;
    } else {
        (void) IP_ifindex_to_vlan(a->interface, &b->vlan);
    }

    return VTSS_RC_OK;
}

static mesa_rc conv(const mesa_ipv4_uc_t *const a,
                    mesa_routing_entry_t *const b) {
    b->type = MESA_ROUTING_ENTRY_TYPE_IPV4_UC;
    b->route.ipv4_uc = *a;
    return VTSS_RC_OK;
}

#if defined(VTSS_SW_OPTION_FRR)
static mesa_rc conv(const FrrIpRoute *const a,
                    vtss_appl_ip_ipv4_route_conf_t *const b) {
    b->distance = a->distance;
    return VTSS_RC_OK;
}

static mesa_rc conv(const mesa_routing_entry_t *const a,
                    const vtss_appl_ip_ipv4_route_conf_t *const c,
                    FrrIpRoute *const b) {
    b->net = *a;
    if (c) {
        b->distance = c->distance;
    }
    b->tag = 0;	 //TBD: no idea about the purpose of tag
    return VTSS_RC_OK;
}
#endif /* VTSS_SW_OPTION_FRR */

static mesa_rc conv(const mesa_routing_entry_t *const a,
                    mesa_ipv4_uc_t             *const b) {
    if (a->type != MESA_ROUTING_ENTRY_TYPE_IPV4_UC) {
        return VTSS_RC_ERROR;
    }

    *b = a->route.ipv4_uc;
    return VTSS_RC_OK;
}

static mesa_rc conv(const ip_route_entry_t         *const a,
                    vtss_appl_ip_ipv6_route_conf_t *const b) {
    return conv(&a->route, b);
}

#if defined(VTSS_SW_OPTION_FRR)
FrrRes<Vector<FrrIpRoute>> IP_route_frr_running_config() {
    auto frr_conf = frr_running_config_get();
    if (frr_conf.rc != VTSS_OK) {
        VTSS_TRACE(ERROR) << "Get data from FRR layer failed.";
        return IP_ERROR_FAILED;
    }

    auto frr_route_conf = frr_ip_route_conf_get(frr_conf.val);
    if (!frr_route_conf.size()) {
        return IP_ERROR_NOTFOUND;
    }
    return frr_route_conf;
}
#endif /* VTSS_SW_OPTION_FRR */

mesa_rc vtss_ip_ip_by_vlan(const mesa_vid_t  vlan,
                           mesa_ip_type_t    type,
                           mesa_ip_addr_t   *const src)
{
    vtss_ifindex_t idx;
    VTSS_RC(vtss_ifindex_from_vlan(vlan, &idx));

    switch (type) {
        case MESA_IP_TYPE_IPV4: {
            notifications::LockGlobalSubject lock;
            const auto &st = status_if_ipv4.ref(lock);

            for (const auto &e : st) {
                if (std::get<0>(e.first) == idx) {
                    conv(std::get<1>(e.first).address, *src);
                    return VTSS_RC_OK;
                }
            }

            break;
        }

        case MESA_IP_TYPE_IPV6: {
            notifications::LockGlobalSubject lock;
            const auto &st = status_if_ipv6.ref(lock);

            for (const auto &e : st) {
                if (std::get<0>(e.first) == idx) {
                    conv(std::get<1>(e.first).address, *src);
                    return VTSS_RC_OK;
                }
            }

            break;
        }

        default:
            ;
    }

    T_R("Failed to get ip by vlan, where vlan = %u", vlan);
    return VTSS_RC_ERROR;
}

mesa_rc vtss_ip_route_getnext(const mesa_routing_entry_t  *const key,
                              mesa_routing_entry_t        *const next)
{
    mesa_rc rc = VTSS_RC_ERROR;
    mesa_ipv4_uc_t ipv4_out;
    vtss_appl_ip_ipv6_route_conf_t ipv6_out;

    if (key) {
        mesa_ipv4_uc_t                  ipv4_in;
        vtss_appl_ip_ipv6_route_conf_t  ipv6_in;
        if (key->type == MESA_ROUTING_ENTRY_TYPE_IPV4_UC) {
            conv(key, &ipv4_in);
            if ((rc = vtss_appl_ip_ipv4_route_conf_itr(&ipv4_in, &ipv4_out)) == VTSS_RC_OK) {
                conv(&ipv4_out, next);
            } else if ((rc = vtss_appl_ip_ipv6_route_conf_itr(NULL, &ipv6_out))) {
                conv(&ipv6_out, next);
            }
        } else if (key->type == MESA_ROUTING_ENTRY_TYPE_IPV6_UC) {
            ip_route_entry_t e;
            e.route = *key;
            conv(&e, &ipv6_in);
            if ((rc = vtss_appl_ip_ipv6_route_conf_itr(&ipv6_in, &ipv6_out)) == VTSS_RC_OK) {
                conv(&ipv6_out, next);
            }
        }
    } else {
        if ((rc = vtss_appl_ip_ipv4_route_conf_itr(NULL, &ipv4_out)) == VTSS_RC_OK) {
            conv(&ipv4_out, next);
        } else if ((rc = vtss_appl_ip_ipv6_route_conf_itr(NULL, &ipv6_out)) == VTSS_RC_OK) {
            conv(&ipv6_out, next);
        }

    }
    return rc;
}
mesa_rc vtss_ip_default_route_get(const int              max,
                               mesa_routing_entry_t   *rto,
                               int                    *const cnt) {
    int users, i = 0;
    mesa_routing_entry_t e;
	mesa_ipv4_network_t v4_zero_network;
	memset(&v4_zero_network, 0, sizeof(v4_zero_network));

	CRIT_SCOPE();
#if defined(VTSS_SW_OPTION_FRR)
    /* Get ipv4 first */
    auto frr_route_conf = IP_route_frr_running_config();
    /* we don't return if there's no route configuration in FRR
       because we also need to get IPv6's route configuration.
    */
    if (frr_route_conf.rc != VTSS_RC_OK &&
            frr_route_conf.rc != IP_ERROR_NOTFOUND) {
        VTSS_TRACE(ERROR) << "get FRR route config failed";
        return frr_route_conf.rc;
    }

    if (frr_route_conf.rc == VTSS_RC_OK) {
        mesa_rc rc = VTSS_RC_ERROR;
        for (const auto &itr : frr_route_conf.val) {
            VTSS_TRACE(INFO) << "travers " << itr.net;
            if (i >= max) {
                break;
            }

            e = itr.net;

            if ((rc = IP_conf_route_ipv4.get(e, &users)) != VTSS_RC_OK) {
                VTSS_TRACE(ERROR) << "IP and FRR DB are inconsistent";
                return VTSS_RC_ERROR;
            }

            if (!(users & VTSS_BIT(VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER))) {
                VTSS_TRACE(INFO) << itr.net << " isn't STATIC => " << "continue ";
                continue;
            }
			if(memcmp( &itr.net.route.ipv4_uc.network, &v4_zero_network, sizeof(v4_zero_network))){
				continue;
			}

            rto[i++] = itr.net;
        }
    }
#else
    /* get for IPv4 */
    VTSS_RC(IP_conf_route_ipv4.get_first(&e, &users));
    do {
        if (i >= max)
            break;

        if (users & VTSS_BIT(VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER))
            rto[i++] = e;

    } while(IP_conf_route_ipv4.get_next(&e, &users) == VTSS_RC_OK);
#endif /* VTSS_SW_OPTION_FRR */

    *cnt = i;
    return VTSS_OK;
}

mesa_rc vtss_ip_route_conf_get(const int              max,
                               mesa_routing_entry_t   *rto,
                               int                    *const cnt) {
    int users, i = 0;
    mesa_routing_entry_t e;

    CRIT_SCOPE();
#if defined(VTSS_SW_OPTION_FRR)
    /* Get ipv4 first */
    auto frr_route_conf = IP_route_frr_running_config();
    /* we don't return if there's no route configuration in FRR
       because we also need to get IPv6's route configuration.
    */
    if (frr_route_conf.rc != VTSS_RC_OK &&
            frr_route_conf.rc != IP_ERROR_NOTFOUND) {
        VTSS_TRACE(ERROR) << "get FRR route config failed";
        return frr_route_conf.rc;
    }

    if (frr_route_conf.rc == VTSS_RC_OK) {
        mesa_rc rc = VTSS_RC_ERROR;
        for (const auto &itr : frr_route_conf.val) {
            VTSS_TRACE(INFO) << "travers " << itr.net;
            if (i >= max) {
                break;
            }

            e = itr.net;

            if ((rc = IP_conf_route_ipv4.get(e, &users)) != VTSS_RC_OK) {
                VTSS_TRACE(ERROR) << "IP and FRR DB are inconsistent";
                return VTSS_RC_ERROR;
            }

            if (!(users & VTSS_BIT(VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER))) {
                VTSS_TRACE(INFO) << itr.net << " isn't STATIC => " << "continue ";
                continue;
            }

            rto[i++] = itr.net;
        }
    }
#else
    /* get for IPv4 */
    VTSS_RC(IP_conf_route_ipv4.get_first(&e, &users));
    do {
        if (i >= max)
            break;

        if (users & VTSS_BIT(VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER))
            rto[i++] = e;

    } while(IP_conf_route_ipv4.get_next(&e, &users) == VTSS_RC_OK);
#endif /* VTSS_SW_OPTION_FRR */

    /* get for IPv6 */
    if (IP_conf_route_ipv6.get_first(&e, &users) == VTSS_RC_OK) {
        do {
            if (i >= max)
                break;

            if (!(users & VTSS_BIT(VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER))) {
                continue;
            }
            rto[i++] = e;

        } while(IP_conf_route_ipv6.get_next(&e, &users) == VTSS_RC_OK);
    }

    *cnt = i;
    return VTSS_OK;
}

template<typename T>
struct IpStatistics {
    typedef mesa_rc (*poll_t)(T *);

    IpStatistics(poll_t p, vtss_tick_count_t r = 0) {
        poll = p;
        refresh_rate = r;
    }

    mesa_rc get(T *s) {
        vtss_tick_count_t ts = vtss_current_time();

        if (cache_ts == 0 || refresh_rate == 0 || ts > cache_ts + refresh_rate) {
            VTSS_RC(poll(&cache_value));
            cache_ts = ts;
        }
        *s = cache_value - reset_value;
        return VTSS_RC_OK;
    }

    mesa_rc reset() {
        VTSS_RC(poll(&cache_value));
        reset_value = cache_value;
        return VTSS_RC_OK;
    }

    poll_t poll;
    vtss_tick_count_t cache_ts;
    vtss_tick_count_t refresh_rate;
    T cache_value;
    T reset_value;
};

template<typename T>
struct IpVidStatistics {
    typedef mesa_rc (*poll_t)(mesa_vid_t, T *);

    IpVidStatistics(poll_t p, vtss_tick_count_t r = 0) {
        poll = p;
        refresh_rate = r;
    }

    mesa_rc get(mesa_vid_t vid, T *s) {
        vtss_tick_count_t ts = vtss_current_time();

        if (cache_ts[vid] == 0 || refresh_rate == 0 || ts > cache_ts[vid] + refresh_rate) {
            VTSS_RC(poll(vid, &cache_value[vid]));
            cache_ts[vid] = ts;
        }
        *s = (cache_value[vid] - reset_value[vid]);
        return VTSS_RC_OK;
    }

    mesa_rc reset(mesa_vid_t vid) {
        VTSS_RC(poll(vid, &cache_value[vid]));
        reset_value[vid] = cache_value[vid];
        return VTSS_RC_OK;
    }

    poll_t poll;
    vtss_tick_count_t cache_ts[VTSS_VIDS];
    vtss_tick_count_t refresh_rate;
    T cache_value[VTSS_VIDS];
    T reset_value[VTSS_VIDS];
};

static mesa_rc IP_ipv4_system_statistics_poll(
        vtss_appl_ip_if_status_ip_stat_t *const s) {
    mesa_rc rc = VTSS_RC_OK;;
    mesa_l3_counters_t chip_stat;

    VTSS_RC(vtss_ip_os_ipv4_system_statistics_get(s));
    VTSS_RC(vtss_ip_chip_counters_system_get(&chip_stat));
    s->HCInReceives   += chip_stat.ipv4uc_received_frames;
    s->HCInOctets     += chip_stat.ipv4uc_received_octets;
    s->HCOutTransmits += chip_stat.ipv4uc_transmitted_frames;
    s->HCOutOctets    += chip_stat.ipv4uc_transmitted_octets;

    // TODO, update 32bit counters
    return rc;
}

static mesa_rc IP_ipv6_system_statistics_poll(
        vtss_appl_ip_if_status_ip_stat_t *const s) {
    mesa_rc rc = VTSS_RC_OK;;
    mesa_l3_counters_t chip_stat;

    VTSS_RC(vtss_ip_os_ipv6_system_statistics_get(s));
    VTSS_RC(vtss_ip_chip_counters_system_get(&chip_stat));
    s->HCInReceives   += chip_stat.ipv6uc_received_frames;
    s->HCInOctets     += chip_stat.ipv6uc_received_octets;
    s->HCOutTransmits += chip_stat.ipv6uc_transmitted_frames;
    s->HCOutOctets    += chip_stat.ipv6uc_transmitted_octets;

    // TODO, update 32bit counters
    return rc;
}

static mesa_rc IP_if_statistics_poll(mesa_vid_t vid, vtss_appl_ip_if_status_link_stat_t *const s)
{
    T_D("vid: %u", vid);
    return vtss_ip_os_if_vlan_statistics_get(vid, s);
}

static mesa_rc IP_ipv6_if_statistics_poll(mesa_vid_t vid, vtss_appl_ip_if_status_ip_stat_t *const s)
{
    T_D("vid: %u", vid);
    return vtss_ip_os_ipv6_vlan_statistics_get(vid, s);
}

// TODO, delete this function
mesa_rc vtss_ip_nb_status_get(mesa_ip_type_t               type,
                              const u32                    max,
                              u32                         *cnt,
                              vtss_neighbour_status_t     *status)
{
    CRIT_SCOPE();
    return vtss_ip_os_nb_status_get(type, max, cnt, status);
}

// Exposing public functions ---------------------------------------------------
mesa_rc vtss_appl_ip_global_capabilities_get(vtss_appl_ip_capabilities_t *c) {
    c->has_ipv4_host_capabilities = TRUE;
#if defined(VTSS_SW_OPTION_IPV6)
    c->has_ipv6_host_capabilities = TRUE;
#else
    c->has_ipv6_host_capabilities = FALSE;
#endif

#if defined(VTSS_SW_OPTION_L3RT)
    c->has_ipv4_unicast_routing_capabilities = TRUE;
#else
    c->has_ipv4_unicast_routing_capabilities = FALSE;
#endif

    c->has_ipv6_unicast_routing_capabilities = c->has_ipv6_host_capabilities &&
        c->has_ipv4_unicast_routing_capabilities;

    if (MESA_CAP(MESA_CAP_L3)) {
        c->has_ipv4_unicast_hw_routing_capabilities =
            c->has_ipv4_unicast_routing_capabilities;
        c->has_ipv6_unicast_hw_routing_capabilities =
            c->has_ipv6_unicast_routing_capabilities;
        c->number_of_lpm_hardware_entries = MESA_CAP(MESA_CAP_L3_LPM_CNT);
    } else {
        c->has_ipv4_unicast_hw_routing_capabilities = FALSE;
        c->has_ipv6_unicast_hw_routing_capabilities = FALSE;
        c->number_of_lpm_hardware_entries = 0;
    }

    c->max_number_of_ip_interfaces = ip_interface_cnt_get();
    c->max_number_of_static_routes = MESA_CAP(VTSS_APPL_CAP_IP_ROUTE_CNT);

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_ip_if_status_get_first(vtss_appl_ip_if_status_type_t type,
                                         vtss_ifindex_t                ifidx,
                                         vtss_appl_ip_if_status_t     *status) {
    mesa_vid_t v;
    VTSS_RC(IP_ifindex_to_vlan(ifidx, &v));

    CRIT_SCOPE();
    return IP2_if_status_get_first(type, v, status);
}

mesa_rc vtss_appl_ip_ipv4_route_conf_itr(const mesa_ipv4_uc_t  *const in,
                                         mesa_ipv4_uc_t        *const out) {
    CRIT_SCOPE();

    int users;
    mesa_routing_entry_t i;

    if (in) {
        VTSS_RC(conv(in, &i));
        VTSS_RC(IP_conf_route_ipv4.get_next(&i, &users));
    } else {
        VTSS_RC(IP_conf_route_ipv4.get_first(&i, &users));
    }

    do {
        if (i.type != MESA_ROUTING_ENTRY_TYPE_IPV4_UC)
            continue;

        if (!(users & VTSS_BIT(VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER)))
            continue;

        if (conv(&i, out) != VTSS_RC_OK)
            continue;

        return VTSS_RC_OK;
    } while(IP_conf_route_ipv4.get_next(&i, &users) == VTSS_RC_OK);

    return VTSS_RC_ERROR;
}

mesa_rc vtss_appl_ip_ipv4_route_conf_get(
        const mesa_ipv4_uc_t  *const rt,
        vtss_appl_ip_ipv4_route_conf_t *const conf) {
    CRIT_SCOPE();

    // Check that the route is owned by static user
    int users;
    mesa_routing_entry_t e;
    conv(rt, &e);
    VTSS_RC(IP_conf_route_ipv4.get(e, &users));

    if (!(users & VTSS_BIT(VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER))) {
        return VTSS_RC_ERROR;
    }

#if defined(VTSS_SW_OPTION_FRR)
    VTSS_TRACE(INFO) << "redirect to zebra@FRR "
        << "get \'" << e.route.ipv4_uc.network << "\' ";

    /* Get data from FRR layer */
    auto frr_route_conf = IP_route_frr_running_config();
    if (frr_route_conf.rc != VTSS_OK) {
        return frr_route_conf.rc;
    }

    for (const auto &itr : frr_route_conf.val) {
        /* the network/prefix is sorted in FRR data base,
           we can stop searching if the network/prefix of the key
           is smaller than current entry */
        if(e.route.ipv4_uc.network < itr.net.route.ipv4_uc.network) {
            break;
        }
        if (e.route.ipv4_uc != itr.net.route.ipv4_uc) {
            continue;
        }
        conv(&itr, conf);
        return VTSS_RC_OK;
    }
    return IP_ERROR_NOTFOUND;
#else
    conf->distance = 255; /* Unknown */
    return VTSS_RC_OK;
#endif /* VTSS_SW_OPTION_FRR */
}

#if defined(VTSS_SW_OPTION_FRR)
mesa_rc vtss_ip_ipv4_route_users_get(
        const mesa_ipv4_uc_t  *const rt,
        int *const owner) {
    CRIT_SCOPE();

    mesa_routing_entry_t e;
    conv(rt, &e);
    VTSS_RC(IP_conf_route_ipv4.get(e, owner));
    return VTSS_RC_OK;
}
#endif /* VTSS_SW_OPTION_FRR */

mesa_rc vtss_appl_ip_ipv4_route_conf_set(
        const mesa_ipv4_uc_t  *const rt,
        const vtss_appl_ip_ipv4_route_conf_t *const conf) {
    CRIT_SCOPE();
    mesa_routing_entry_t re = {};
    vtss_routing_params_t p = {};

    VTSS_TRACE(INFO) << "route set " << *rt;
    VTSS_RC(conv(rt, &re));

    p.owner = VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER;
    return IP2_route_add_ipv4(&re, &p, conf);
}

mesa_rc vtss_appl_ip_ipv4_route_conf_del(
        const mesa_ipv4_uc_t  *const rt) {
    CRIT_SCOPE();

    mesa_routing_entry_t re = {};
    vtss_routing_params_t p = {};

    VTSS_TRACE(INFO) << "route del " << *rt;

    VTSS_RC(conv(rt, &re));
    p.owner = VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER;

    return IP2_route_del_ipv4(&re, &p);
}

mesa_rc vtss_appl_ip_ipv6_route_conf_itr(
        const vtss_appl_ip_ipv6_route_conf_t  *const in,
        vtss_appl_ip_ipv6_route_conf_t        *const out) {
    CRIT_SCOPE();

    int users;
    mesa_routing_entry_t i;

    if (in) {
        VTSS_RC(conv(in, &i));
        VTSS_RC(IP_conf_route_ipv6.get_next(&i, &users));
    } else {
        VTSS_RC(IP_conf_route_ipv6.get_first(&i, &users));
    }

    do {
        if (i.type != MESA_ROUTING_ENTRY_TYPE_IPV6_UC)
            continue;

        if (!(users & VTSS_BIT(VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER)))
            continue;

        if (conv(&i, out) != VTSS_RC_OK)
            continue;

        return VTSS_RC_OK;
    } while(IP_conf_route_ipv6.get_next(&i, &users) == VTSS_RC_OK);

    return VTSS_RC_ERROR;
}

mesa_rc vtss_appl_ip_ipv6_route_conf_get(
        const vtss_appl_ip_ipv6_route_conf_t  *const rt) {
    CRIT_SCOPE();

    int users;
    mesa_routing_entry_t e;
    conv(rt, &e);
    return IP_conf_route_ipv6.get(e, &users);
}

mesa_rc vtss_appl_ip_ipv6_route_conf_set(
        const vtss_appl_ip_ipv6_route_conf_t  *const rt) {
    CRIT_SCOPE();

    mesa_routing_entry_t re = {};
    vtss_routing_params_t p = {};

    VTSS_TRACE(INFO) << "route set " << *rt;
    VTSS_RC(conv(rt, &re));
    p.owner = VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER;

    return IP2_route_add_ipv6(&re, &p);
}

mesa_rc vtss_appl_ip_ipv6_route_conf_del(
        const vtss_appl_ip_ipv6_route_conf_t  *const rt) {
    CRIT_SCOPE();

    mesa_routing_entry_t re = {};
    vtss_routing_params_t p = {};

    VTSS_TRACE(INFO) << "route del " << *rt;

    VTSS_RC(conv(rt, &re));
    p.owner = VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER;

    return IP2_route_del_ipv6(&re, &p);
}

mesa_rc vtss_appl_ip_neighbour_clear(mesa_ip_type_t type) {
    IP_PUBLIC_API_FUNCTION(vtss_ip_os_nb_clear, type);
}

static IpVidStatistics<vtss_appl_ip_if_status_link_stat_t>
IP_if_statistics(&IP_if_statistics_poll, VTSS_OS_MSEC2TICK(IP_STAT_REFRESH_RATE));

mesa_rc vtss_appl_ip_if_statistics_link(vtss_ifindex_t ifidx, vtss_appl_ip_if_status_link_stat_t *const s)
{
    CRIT_SCOPE();
    ip2_ifstate_t *ifs = IP2_ifidx2state(ifidx);
    return (ifs == NULL ? VTSS_RC_ERROR : IP_if_statistics.get(ifs->vid, s));
}

mesa_rc vtss_appl_ip_if_statistics_link_clear(vtss_ifindex_t ifidx)
{
    CRIT_SCOPE();
    ip2_ifstate_t *ifs = IP2_ifidx2state(ifidx);
    return (ifs == NULL ? VTSS_RC_ERROR : IP_if_statistics.reset(ifs->vid));
}

mesa_rc vtss_appl_ip_if_statistics_ipv4(vtss_ifindex_t ifidx, vtss_appl_ip_if_status_ip_stat_t *const s)
{
    return VTSS_RC_ERROR;
}

mesa_rc vtss_appl_ip_if_statistics_ipv4_clear(vtss_ifindex_t ifidx)
{
    return VTSS_RC_ERROR;
}

static IpVidStatistics<vtss_appl_ip_if_status_ip_stat_t>
IP_ipv6_if_statistics(&IP_ipv6_if_statistics_poll, VTSS_OS_MSEC2TICK(IP_STAT_REFRESH_RATE));

mesa_rc vtss_appl_ip_if_statistics_ipv6(vtss_ifindex_t ifidx, vtss_appl_ip_if_status_ip_stat_t *const s)
{
    CRIT_SCOPE();
    ip2_ifstate_t *ifs = IP2_ifidx2state(ifidx);
    return (ifs == NULL ? VTSS_RC_ERROR : IP_ipv6_if_statistics.get(ifs->vid, s));
}

mesa_rc vtss_appl_ip_if_statistics_ipv6_clear(vtss_ifindex_t ifidx)
{
    CRIT_SCOPE();
    ip2_ifstate_t *ifs = IP2_ifidx2state(ifidx);
    return (ifs == NULL ? VTSS_RC_ERROR : IP_ipv6_if_statistics.reset(ifs->vid));
}

static IpStatistics<vtss_appl_ip_if_status_ip_stat_t>
IP_ipv4_system_statistics(&IP_ipv4_system_statistics_poll, VTSS_OS_MSEC2TICK(IP_STAT_REFRESH_RATE));

mesa_rc vtss_appl_ip_system_statistics_ipv4_get(vtss_appl_ip_if_status_ip_stat_t *const s)
{
    CRIT_SCOPE();
    return IP_ipv4_system_statistics.get(s);
}

static IpStatistics<vtss_appl_ip_if_status_ip_stat_t>
IP_ipv6_system_statistics(&IP_ipv6_system_statistics_poll, VTSS_OS_MSEC2TICK(IP_STAT_REFRESH_RATE));

mesa_rc vtss_appl_ip_system_statistics_ipv6_get(vtss_appl_ip_if_status_ip_stat_t *const s)
{
    CRIT_SCOPE();
    return IP_ipv6_system_statistics.get(s);
}

mesa_rc vtss_appl_ip_system_statistics_clear(mesa_ip_type_t version)
{
    CRIT_SCOPE();
    if (version == MESA_IP_TYPE_IPV4)
        VTSS_RC(IP_ipv4_system_statistics.reset());
    if (version == MESA_IP_TYPE_IPV6)
        VTSS_RC(IP_ipv6_system_statistics.reset());
    return VTSS_RC_OK;
}

mesa_rc vtss_appl_ip_ipv4_neighbour_status_get_list(
        u32                                        max,
        u32                                       *cnt,
        vtss_appl_ip_ipv4_neighbour_status_pair_t *status) {
    IP_PUBLIC_API_FUNCTION(vtss_ip_os_ipv4_neighbour_status_get_list,
                           max, cnt, status);
}

mesa_rc vtss_appl_ip_ipv6_neighbour_status_get_list(
        u32                                        max,
        u32                                       *cnt,
        vtss_appl_ip_ipv6_neighbour_status_pair_t *status) {
    IP_PUBLIC_API_FUNCTION(vtss_ip_os_ipv6_neighbour_status_get_list,
                           max, cnt, status);
}

mesa_rc vtss_appl_ip_global_controls(
        const vtss_appl_ip_global_actions_t *const a) {

    if (a->ipv4_neighbour_table_clear)
        VTSS_RC(vtss_appl_ip_neighbour_clear(MESA_IP_TYPE_IPV4));

    if (a->ipv6_neighbour_table_clear)
        VTSS_RC(vtss_appl_ip_neighbour_clear(MESA_IP_TYPE_IPV6));

    if (a->ipv4_system_statistics_clear)
        VTSS_RC(vtss_appl_ip_system_statistics_clear(MESA_IP_TYPE_IPV4));

    if (a->ipv6_system_statistics_clear)
        VTSS_RC(vtss_appl_ip_system_statistics_clear(MESA_IP_TYPE_IPV6));

    if (a->ipv4_acd_status_clear) {
        vtss_appl_ip_ipv4_acd_status_key_t key;

        while (vtss_appl_ip_ipv4_acd_status_itr(NULL, &key) == VTSS_RC_OK) {
            VTSS_RC(status_acd_ipv4.del(&key));
        }
    }

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_ip_ipv4_dhcp_client_control_restart(
        vtss_ifindex_t                        ifidx,
        BOOL                                  action) {
    if (!action)
        return VTSS_RC_OK;

    IP_PUBLIC_API_FUNCTION(IP_ipv4_dhcp_restart, ifidx);
}

static mesa_rc ip_dhcpv6_cmd(BOOL add, mesa_vid_t vid,
                             const mesa_ipv6_network_t *const network,
                             const vtss_ip_ipv6_addr_info_t *const info)
{
    ip2_ifstate_t *ifs;
    const char *cmd = (add ? "add" : "del");

    CRIT_SCOPE();

    VTSS_TRACE(INFO) << cmd << ", vid: " << vid << ", network: " << *network;
    ifs = IP2_if2state(vid);
    if (ifs == NULL) {
        T_W("VID %u not created", vid);
        return VTSS_RC_ERROR;
    }
    if (!ifs->dhcpv6.active && !add) {
        T_I("VID %u not active", vid);
        return VTSS_RC_OK;
    }
    ifs->dhcpv6.active = add;
    if (add) {
        ifs->dhcpv6.network = *network;
        ifs->dhcpv6.info = *info;
    }
    return (IP2_state.vlan_state.up[vid] ?
            (add ? vtss_ip_os_ipv6_add(vid, network, NULL) : vtss_ip_os_ipv6_del(vid, network)) : VTSS_RC_OK);
}

mesa_rc vtss_ip_ipv6_dhcp_add(mesa_vid_t vid,
                              const mesa_ipv6_network_t *const network,
                              const vtss_ip_ipv6_addr_info_t *const info)
{
    return ip_dhcpv6_cmd(TRUE, vid, network, info);
}

mesa_rc vtss_ip_ipv6_dhcp_del(mesa_vid_t vid, const mesa_ipv6_network_t *const network)
{
    return ip_dhcpv6_cmd(FALSE, vid, network, NULL);
}

#if defined(VTSS_SW_OPTION_PRIVATE_MIB)
extern "C" void vtss_ip_mib_init();
#endif
#if defined(VTSS_SW_OPTION_JSON_RPC)
void vtss_appl_ip_json_init();
#endif
mesa_rc vtss_appl_ip_kernel_module_if_mux_load();
mesa_rc vtss_appl_ip_kernel_module_if_mux_unload();

static void IP2_vlan_global_conf_change_callback(mesa_etype_t tpid)
{
    vtss_flag_setbits(&ip2_control_flags, CTLFLAG_VLAN_CHANGE);
}

static void IP2_vlan_port_conf_change_callback(vtss_isid_t isid, mesa_port_no_t port_no,
					       const vtss_appl_vlan_port_detailed_conf_t *conf)
{
    vtss_flag_setbits(&ip2_control_flags, CTLFLAG_VLAN_CHANGE);
}

uint32_t ip_interface_cnt_get(void)
{
    static uint32_t val;

    // Cache value of VTSS_APPL_CAP_IP_INTERFACE_CNT, because the call to MESA_CAP()
    // is pretty expensive in terms of time.
    if (!val) {
        val = MESA_CAP(VTSS_APPL_CAP_IP_INTERFACE_CNT);
    }

    return val;
}

extern "C" int ipv6_icli_cmd_register();
extern "C" int ip_icli_cmd_register();

/* Module initialization --------------------------------------------------- */
void vtss_ip_adaptor_init();
mesa_rc vtss_ip_init(vtss_init_data_t *data)
{
    vtss_isid_t isid = data->isid;
    if (data->cmd == INIT_CMD_EARLY_INIT) {
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, VTSS_TRACE_IP_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);
    }

    switch (data->cmd) {
    case INIT_CMD_INIT:
        VTSS_TRACE(INFO) << "INIT";
        if (MESA_CAP(MESA_CAP_L3)) {
            IpChip = getIpLpm();
        } else {
            IpChip = getIpMac();
        }
        IP2_route_db_apply_ipv4.init();
        IP2_route_db_apply_ipv6.init();
        vtss_clear(IP2_stack_conf);
        memset(IP2_state.vid2if, IFNO_INVALID, sizeof(IP2_state.vid2if));

        (void)vtss_appl_ip_kernel_module_if_mux_load();

        critd_init(&ip2_crit, "ip.crit", VTSS_MODULE_ID_IP, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
        critd_exit(&ip2_crit, VTSS_TRACE_IP_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__);

        (void)vtss_ip_adaptor_init();
        (void)vtss_ip_os_init();
        (void)vtss_ip_chip_init();
        vtss_flag_init(&ip2_control_flags);
        vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
                           ip2_thread,
                           0,
                           "IP2.main",
                           nullptr,
                           0,
                           &ip2_thread_handle,
                           &ip2_thread_block);

#if defined(VTSS_SW_OPTION_PRIVATE_MIB)
        vtss_ip_mib_init();
#endif
#if defined(VTSS_SW_OPTION_JSON_RPC)
        vtss_appl_ip_json_init();
#endif
#if defined(VTSS_SW_OPTION_IPV6)
        ipv6_icli_cmd_register();
#endif
        ip_icli_cmd_register();
        break;

    case INIT_CMD_START: {
        VTSS_TRACE(INFO) << "START";

        ipv4_acd_init();

        (void)vtss_ip_chip_start();

#ifdef VTSS_SW_OPTION_SNMP
        (void)vtss_ip_snmp_init();
#endif
#ifdef VTSS_SW_OPTION_ICLI
        (void) vtss_ip_ipv4_icfg_init();
#endif

        vtss_appl_ip_system_statistics_clear(MESA_IP_TYPE_IPV4);
        vtss_appl_ip_system_statistics_clear(MESA_IP_TYPE_IPV6);

        /* VLAN configuration change registration */
        vlan_s_custom_etype_change_register(VTSS_MODULE_ID_VLAN, IP2_vlan_global_conf_change_callback);
        vlan_port_conf_change_register(VTSS_MODULE_ID_VLAN, IP2_vlan_port_conf_change_callback, FALSE);

        (void)vtss_ip_if_callback_add(ip_if_callback_test);

        break;
    }

    case INIT_CMD_CONF_DEF:
        VTSS_TRACE(INFO) << "CONF_DEF, isid: " << isid;
        if (isid == VTSS_ISID_GLOBAL) {
            // Reset global configuration (no local or per switch configuration here)
            CRIT_SCOPE();
            IP2_conf_default(&IP2_stack_conf);
        }
        VTSS_TRACE(INFO) << "CONF_DEF, isid: " << isid << " - DONE";

        // We need to update the list of vlans that the slave devices must
        // monitor for forwarding changes.
        {
            CRIT_SCOPE();
            poll_fwdstate();
        }
        break;

    case INIT_CMD_MASTER_UP: {
        CRIT_SCOPE();
        VTSS_TRACE(INFO) << "MASTER_UP";

        (void)conf_mgmt_mac_addr_get(IP2_state.master_mac.addr, 0);
        (void)vtss_ip_chip_master_up(&IP2_state.master_mac);

        /* Read stack configuration */
        vtss_flag_setbits(&ip2_control_flags, CTLFLAG_MASTER_CHANGED);
        VTSS_TRACE(INFO) << "MASTER_UP - completed";
        break;
    }

    case INIT_CMD_MASTER_DOWN: {
        CRIT_SCOPE();
        VTSS_TRACE(INFO) << "MASTER_DOWN";
        IP2_route_db_reset();
        (void)vtss_ip_os_nb_clear(MESA_IP_TYPE_IPV4);
        (void)vtss_ip_os_nb_clear(MESA_IP_TYPE_IPV6);
        IP2_delete_all_interfaces();
        (void)vtss_ip_chip_master_down();

        vtss_flag_setbits(&ip2_control_flags, CTLFLAG_MASTER_CHANGED);
        VTSS_TRACE(INFO) << "MASTER_DOWN - completed";
        break;
    }

    case INIT_CMD_SWITCH_ADD: {
        CRIT_SCOPE();
        VTSS_TRACE(INFO) << "SWITCH_ADD, isid: " << isid;
        (void)vtss_ip_chip_switch_add(isid);
        poll_fwdstate();
        VTSS_TRACE(INFO) << "SWITCH_ADD, isid: " << isid<< " - completed";
        break;
    }

    default:
        VTSS_TRACE(INFO) << "UNKNOWN CMD: " << data->cmd << ", isid: " << isid;
        break;
    }

    VTSS_TRACE(DEBUG) << "exit";
    return VTSS_OK;
}

