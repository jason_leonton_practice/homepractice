/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "ip_utils.h"
#include "ip_api.h"
#include "msg_api.h" /* msg_switch_is_master() */
#include "misc_api.h"
#include "vtss/appl/vlan.h"
#include "vtss/appl/ospf.h"
#include "vlan_api.h"
#include "vtss/appl/interface.h"
#include "vtss/basics/memory.hxx"
#include "vtss/basics/map.hxx"

using namespace vtss;

#define IP_MAX_STATUS_OBJS 1024

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_IP

#define PRINTF(...)                                         \
    if (size - s > 0) {                                     \
        int res = snprintf(buf + s, size - s, __VA_ARGS__); \
        if (res > 0) {                                      \
            s += res;                                       \
        }                                                   \
    }

#define PRINTFUNC(F, ...)                       \
    if (size - s > 0) {                         \
        s += F(buf + s, size - s, __VA_ARGS__); \
    }

int vtss_ip_ifindex_to_txt(char *buf, int size, vtss_ifindex_t const i) {
    int s = 0;
    vtss_ifindex_elm_t e;

    if (vtss_ifindex_decompose(i, &e) != VTSS_RC_OK) {
        PRINTF("IFINDEX %u", VTSS_IFINDEX_PRINTF_ARG(i));
        return s;
    }

    switch (e.iftype) {
    case VTSS_IFINDEX_TYPE_PORT:
        PRINTF("PORT %u", e.ordinal);
        return s;
    case VTSS_IFINDEX_TYPE_LLAG:
        PRINTF("LLAG %u", e.ordinal);
        return s;
    case VTSS_IFINDEX_TYPE_GLAG:
        PRINTF("GLAG %u", e.ordinal);
        return s;
    case VTSS_IFINDEX_TYPE_VLAN:
        PRINTF("VLAN %u", e.ordinal);
        return s;
    case VTSS_IFINDEX_TYPE_EVC:
        PRINTF("EVC %u", e.ordinal);
        return s;
    default:
        PRINTF("IFINDEX %u", VTSS_IFINDEX_PRINTF_ARG(i));
        return s;
    }
}

bool operator<(const mesa_routing_entry_t &a, const mesa_routing_entry_t &b) {
#define CMP(A, B) \
    if ((A) != (B)) return (A) < (B);

#define CMP_AB(X) CMP(a.X, b.X)

#define CMP_AB_16(X) \
    CMP_AB(X[0]);    \
    CMP_AB(X[1]);    \
    CMP_AB(X[2]);    \
    CMP_AB(X[3]);    \
    CMP_AB(X[4]);    \
    CMP_AB(X[5]);    \
    CMP_AB(X[6]);    \
    CMP_AB(X[7]);    \
    CMP_AB(X[8]);    \
    CMP_AB(X[9]);    \
    CMP_AB(X[10]);   \
    CMP_AB(X[11]);   \
    CMP_AB(X[12]);   \
    CMP_AB(X[13]);   \
    CMP_AB(X[14]);   \
    CMP_AB(X[15]);

    CMP_AB(type);

    switch (a.type) {
    case MESA_ROUTING_ENTRY_TYPE_IPV4_UC:
        CMP_AB(route.ipv4_uc.network.address);
        CMP_AB(route.ipv4_uc.network.prefix_size);
        CMP_AB(route.ipv4_uc.destination);
        break;

    case MESA_ROUTING_ENTRY_TYPE_IPV6_UC:
        CMP_AB_16(route.ipv6_uc.network.address.addr);
        CMP_AB(route.ipv6_uc.network.prefix_size);
        CMP_AB_16(route.ipv6_uc.destination.addr);
        CMP_AB(vlan);
        break;
    case MESA_ROUTING_ENTRY_TYPE_IPV4_MC:
    default:
        return false;
    }
    return false;
#undef CMP_AB_16
#undef CMP_AB
#undef CMP
}

static BOOL vtss_ipv4_addr_is_zero(mesa_ipv4_t addr) { return addr == 0; }

BOOL vtss_ipv6_addr_is_zero(const mesa_ipv6_t *const addr) {
    static const mesa_ipv6_t zero_ipv6 = {};
    return memcmp(addr->addr, zero_ipv6.addr, sizeof(zero_ipv6.addr)) == 0;
}

// Pure function, no side effects
/* Returns TRUE if IPv4/v6 is a zero-address. FALSE otherwise. */
BOOL vtss_ip_addr_is_zero(const mesa_ip_addr_t *ip_addr) {
    switch (ip_addr->type) {
    case MESA_IP_TYPE_IPV4:
        return vtss_ipv4_addr_is_zero(ip_addr->addr.ipv4);
    case MESA_IP_TYPE_IPV6:
        return vtss_ipv6_addr_is_zero(&ip_addr->addr.ipv6);
    case MESA_IP_TYPE_NONE:
        return TRUE; /* Consider MESA_IP_TYPE_NONE as zero */
    }

    return FALSE; /* Undefined */
}

// Pure function, no side effects
BOOL vtss_ipv4_addr_is_multicast(const mesa_ipv4_t *const ip) {
    if (!ip) {
        return FALSE;
    }

    /* IN_CLASSD */
    return (((*ip >> 24) & 0xF0) == 0xE0);
}

// Pure function, no side effects
BOOL vtss_ipv4_addr_is_loopback(const mesa_ipv4_t *const ip) {
    if (!ip) {
        return FALSE;
    }

    /* IN_CLASSD */
    return (((*ip >> 24) & 0xFF) == 0x7F);
}

// Pure function, no side effects
BOOL vtss_ipv6_addr_is_link_local(const mesa_ipv6_t *const ip) {
    if (ip->addr[0] == 0xfe && (ip->addr[1] >> 6) == 0x2) {
        return TRUE;
    }
    return FALSE;
}

// Pure function, no side effects
BOOL vtss_ipv6_addr_is_multicast(const mesa_ipv6_t *const ip) {
    if (ip->addr[0] == 0xff) {
        return TRUE;
    }
    return FALSE;
}

// Pure function, no side effects
BOOL vtss_ipv6_addr_is_loopback(const mesa_ipv6_t *const addr) {
    u8 idx;

    if (!addr) {
        return FALSE;
    }

    for (idx = 0; idx < 16; ++idx) {
        if (idx < 15) {
            if (addr->addr[idx]) {
                return FALSE;
            }
        } else {
            if (addr->addr[idx] != 0x1) {
                return FALSE;
            }
        }
    }

    return TRUE;
}

static BOOL _vtss_ipv6_addr_is_v4_form(const mesa_ipv6_t *const addr) {
    u8 idx;

    if (!addr) {
        return FALSE;
    }

    for (idx = 0; idx < 16; ++idx) {
        if (idx < 10) {
            if (addr->addr[idx]) {
                return FALSE;
            }
        } else {
            if (idx < 12) {
                if (addr->addr[idx] && (addr->addr[idx] != 0xFF)) {
                    return FALSE;
                }
            } else {
                if (vtss_ipv6_addr_is_zero(addr)) {
                    return FALSE;
                }

                break;
            }
        }
    }

    return TRUE;
}

// Pure function, no side effects
BOOL vtss_ipv6_addr_is_mgmt_support(const mesa_ipv6_t *const addr) {
    if (vtss_ipv6_addr_is_loopback(addr) || _vtss_ipv6_addr_is_v4_form(addr)) {
        return FALSE;
    }

    return TRUE;
}

// Pure function, no side effects
int vtss_ipv4_addr_to_prefix(mesa_ipv4_t ip) {
    if ((ip & 0xff000000) == 0x0a000000) {  // 10.0.0.0/8
        return 8;
    } else if ((ip & 0xfff00000) == 0xac100000) {  // 172.16.0.0/12
        return 12;
    } else if ((ip & 0xffff0000) == 0xc0a80000) {  // 192.168.0.0/16
        return 16;
    } else if ((ip & 0x80000000) == 0x00000000) {  // class A
        return 8;
    } else if ((ip & 0xC0000000) == 0x80000000) {  // class B
        return 16;
    } else if ((ip & 0xE0000000) == 0x80000000) {  // class C
        return 24;
    }
    return 0;
}

// Pure function, no side effects
mesa_rc vtss_prefix_cnt(const u8 *data, const u32 length, u32 *prefix) {
    u32 i;
    u32 cnt = 0;
    BOOL prefix_ended = FALSE;

    for (i = 0; i < length; ++i) {
        if (prefix_ended && data[i] != 0) {
            return VTSS_RC_ERROR;
        }

        /*lint --e{616} ... Fallthrough intended */
        switch (data[i]) {
        case 0xff:
            cnt += 8;
            break;
        case 0xfe:
            cnt += 1;
        case 0xfc:
            cnt += 1;
        case 0xf8:
            cnt += 1;
        case 0xf0:
            cnt += 1;
        case 0xe0:
            cnt += 1;
        case 0xc0:
            cnt += 1;
        case 0x80:
            cnt += 1;
        case 0x00:
            prefix_ended = TRUE;
            break;
        default:
            return VTSS_RC_ERROR;
        }
    }

    *prefix = cnt;
    return VTSS_RC_OK;
}


// Pure function, no side effects
mesa_rc vtss_conv_ipv4mask_to_prefix(const mesa_ipv4_t ipv4,
                                     u32 *const prefix) {
    u32 data = htonl(ipv4);
    return vtss_prefix_cnt((u8 *)&data, 4, prefix);
}

// Pure function, no side effects
mesa_rc vtss_conv_prefix_to_ipv4mask(const u32 prefix,
                                     mesa_ipv4_t *const rmask) {
    unsigned i;
    mesa_ipv4_t mask = 0;

    if (prefix > 32u) {
        return VTSS_RC_ERROR;
    }

    for (i = 0; i < 32u; i++) {
        mask <<= 1;

        if (i < prefix) {
            mask |= 1;
        }
    }

    *rmask = mask;
    return VTSS_RC_OK;
}


// Pure function, no side effects
mesa_rc vtss_conv_ipv6mask_to_prefix(const mesa_ipv6_t *const ipv6,
                                     u32 *const prefix) {
    return vtss_prefix_cnt(ipv6->addr, 16, prefix);
}

// Pure function, no side effects
mesa_rc vtss_conv_prefix_to_ipv6mask(const u32 prefix,
                                     mesa_ipv6_t *const mask) {
    u8 v = 0;
    u32 i = 0;
    u32 next_bit = 0;

    if (prefix > 128) {
        return VTSS_RC_ERROR;
    }

    /* byte-wise update or clear */
    for (i = 0; i < 16; ++i) {
        u32 b = (i + 1) * 8;
        if (b <= prefix) {
            mask->addr[i] = 0xff;
            next_bit = b;
        } else {
            mask->addr[i] = 0x0;
        }
    }

    switch (prefix % 8) {
    case 1:
        v = 0x80;
        break;
    case 2:
        v = 0xc0;
        break;
    case 3:
        v = 0xe0;
        break;
    case 4:
        v = 0xf0;
        break;
    case 5:
        v = 0xf8;
        break;
    case 6:
        v = 0xfc;
        break;
    case 7:
        v = 0xfe;
        break;
    }
    mask->addr[(next_bit / 8)] = v;
    return VTSS_RC_OK;
}

// Pure function, no side effects
mesa_rc vtss_build_ipv4_network(mesa_ip_network_t *const network,
                                const mesa_ipv4_t address,
                                const mesa_ipv4_t mask) {
    u32 prefix = 0;
    network->address.type = MESA_IP_TYPE_NONE;
    VTSS_RC(vtss_conv_ipv4mask_to_prefix(mask, &prefix));
    network->address.type = MESA_IP_TYPE_IPV4;
    network->address.addr.ipv4 = address;
    network->prefix_size = prefix;
    return VTSS_RC_OK;
}

// Pure function, no side effects
mesa_rc vtss_build_ipv4_uc(mesa_ipv4_uc_t *route, const mesa_ipv4_t address,
                           const mesa_ipv4_t mask, const mesa_ipv4_t gateway) {
    mesa_ip_network_t net;
    VTSS_RC(vtss_build_ipv4_network(&net, address, mask));
    route->network.address = net.address.addr.ipv4;
    route->network.prefix_size = net.prefix_size;
    route->destination = gateway;
    return VTSS_RC_OK;
}

// Pure function, no side effects
mesa_rc vtss_build_ipv6_network(mesa_ip_network_t *const network,
                                const mesa_ipv6_t *address,
                                const mesa_ipv6_t *mask) {
    u32 prefix;

    if (!network || !address || !mask) {
        return VTSS_RC_ERROR;
    }

    prefix = 0;
    network->address.type = MESA_IP_TYPE_NONE;
    VTSS_RC(vtss_conv_ipv6mask_to_prefix(mask, &prefix));

    network->address.type = MESA_IP_TYPE_IPV6;
    memcpy(&network->address.addr.ipv6, address, sizeof(mesa_ipv6_t));
    network->prefix_size = prefix;

    return VTSS_RC_OK;
}

// Pure function, no side effects
mesa_rc vtss_build_ipv6_network_(mesa_ipv6_network_t *const network,
                                 const mesa_ipv6_t *address,
                                 const mesa_ipv6_t *mask) {
    u32 prefix;

    if (!network || !address || !mask) {
        return VTSS_RC_ERROR;
    }

    prefix = 0;
    VTSS_RC(vtss_conv_ipv6mask_to_prefix(mask, &prefix));
    memcpy(&network->address, address, sizeof(mesa_ipv6_t));
    network->prefix_size = prefix;

    return VTSS_RC_OK;
}

// Pure function, no side effects
mesa_rc vtss_build_ipv6_uc(vtss_appl_ip_ipv6_route_conf_t *const route,
                           const mesa_ipv6_t *address, const mesa_ipv6_t *mask,
                           const mesa_ipv6_t *gateway, const mesa_vid_t vid) {
    mesa_ip_network_t net;

    if (!route || !address || !mask) {
        return VTSS_RC_ERROR;
    }

    memset(route, 0x0, sizeof(vtss_appl_ip_ipv6_route_conf_t));
    VTSS_RC(vtss_build_ipv6_network(&net, address, mask));

    VTSS_RC(vtss_ifindex_from_vlan(vid, &route->interface));
    memcpy(&route->route.network.address, &net.address.addr.ipv6,
           sizeof(mesa_ipv6_t));
    route->route.network.prefix_size = net.prefix_size;
    if (gateway) {
        memcpy(&route->route.destination, gateway, sizeof(mesa_ipv6_t));
    }

    return VTSS_RC_OK;
}

// Pure function, no side effects
int vtss_if_link_flag_to_txt(char *buf, int size,
                             vtss_appl_ip_if_link_flag_t f) {
    int s = 0;
    BOOL first = TRUE;

/*lint --e{438} */
#define F(X)                                 \
    if (f & VTSS_APPL_IP_IF_LINK_FLAG_##X) { \
        if (first) {                         \
            first = FALSE;                   \
            PRINTF(#X);                      \
        } else {                             \
            PRINTF(" " #X);                  \
        }                                    \
    }

    *buf = 0;
    F(UP);
    F(BROADCAST);
    F(LOOPBACK);
    F(RUNNING);
    F(NOARP);
    F(PROMISC);
    F(MULTICAST);
#if 0
    F(IPV6_RA_MANAGED);
    F(IPV6_RA_OTHER);
#endif
#undef F

    buf[MIN(size - 1, s)] = 0;
    return s;
}

// Pure function, no side effects
int vtss_if_ipv6_flag_to_txt(char *buf, int size,
                             vtss_appl_ip_if_ipv6_flag_t f) {
    int s = 0;
    BOOL first = TRUE;

/*lint --e{438} */
#define F(X)                                 \
    if (f & VTSS_APPL_IP_IF_IPV6_FLAG_##X) { \
        if (first) {                         \
            first = FALSE;                   \
            PRINTF(#X);                      \
        } else {                             \
            PRINTF(" " #X);                  \
        }                                    \
    }

    *buf = 0;
    F(UP);
    F(RUNNING);
    F(STATIC);
    F(AUTOCONF);
    F(DHCP);
    F(ANYCAST);
    F(TENTATIVE);
    F(TEMPORARY);
    F(NODAD);
    F(DUPLICATED);
    F(DETACHED);
    F(DEPRECATED);
#undef F

    buf[MIN(size - 1, s)] = 0;
    return s;
}

// Pure function, no side effects
int vtss_routing_flags_to_txt(char *buf, int size,
                              vtss_appl_ip_route_status_flags_t f) {
    int s = 0;
    BOOL first = TRUE;

/*lint --e{438} */
#define F(X)                                      \
    if (f & VTSS_APPL_IP_ROUTE_STATUS_FLAG_##X) { \
        if (first) {                              \
            PRINTF(#X);                           \
        } else {                                  \
            PRINTF(" " #X);                       \
        }                                         \
        first = FALSE;                            \
    }

    *buf = 0;
    F(UP);
    F(HOST);
    F(GATEWAY);
    F(REJECT);
    F(HW_RT);
#undef F

    buf[MIN(size - 1, s)] = 0;
    return s;
}

void vtss_ip_if_id_vlan(mesa_vid_t vid, vtss_if_id_t *if_id) {
    if_id->type = VTSS_ID_IF_TYPE_VLAN;
    if_id->u.vlan = vid;
}

BOOL vtss_ip_equal(const mesa_ip_network_t *const a,
                   const mesa_ip_network_t *const b) {
    if (a->address.type != b->address.type) {
        return FALSE;
    }

    switch (a->address.type) {
    case MESA_IP_TYPE_IPV4:
        return a->address.addr.ipv4 == b->address.addr.ipv4;
    case MESA_IP_TYPE_IPV6:
        return memcmp(a->address.addr.ipv6.addr, b->address.addr.ipv6.addr,
                      sizeof(a->address.addr.ipv6.addr)) == 0;
    default:
        ;
    }

    return TRUE;
}

BOOL vtss_ipv4_network_equal(const mesa_ipv4_network_t *const a,
                             const mesa_ipv4_network_t *const b) {
    if (a->prefix_size != b->prefix_size) {
        return FALSE;
    }

    return a->address == b->address;
}

BOOL vtss_ipv6_network_equal(const mesa_ipv6_network_t *const a,
                             const mesa_ipv6_network_t *const b) {
    if (a->prefix_size != b->prefix_size) {
        return FALSE;
    }

    return memcmp(a->address.addr, b->address.addr, sizeof(a->address.addr)) ==
           0;
}

BOOL vtss_ip_network_equal(const mesa_ip_network_t *const a,
                           const mesa_ip_network_t *const b) {
    // We may compare prefix size regardsless of type
    if (a->prefix_size != b->prefix_size) {
        return FALSE;
    }

    return vtss_ip_equal(a, b);
}

BOOL vtss_ipv4_net_equal(const mesa_ipv4_network_t *const a,
                         const mesa_ipv4_network_t *const b) {
    mesa_ipv4_t mask = (u32)-1;
    if (a->prefix_size != b->prefix_size) {
        return FALSE;
    }

    (void)vtss_conv_prefix_to_ipv4mask(a->prefix_size, &mask);
    return (a->address & mask) == (b->address & mask);
}

BOOL vtss_ipv6_net_equal(const mesa_ipv6_network_t *const a,
                         const mesa_ipv6_network_t *const b) {
    mesa_ipv6_t mask;
    if (a->prefix_size == b->prefix_size &&
        vtss_conv_prefix_to_ipv6mask(a->prefix_size, &mask) == VTSS_OK) {
        size_t i;
        for (i = 0; i < sizeof(mask.addr); i++) {
            u8 maskb = mask.addr[i];
            /* Compare network (masked) only */
            if ((a->address.addr[i] & maskb) != (b->address.addr[i] & maskb)) {
                return FALSE;
            }
        }
        return TRUE;
    }
    return FALSE;
}

mesa_ipv4_network_t vtss_ipv4_net_mask_out(const mesa_ipv4_network_t *n) {
    mesa_ipv4_t mask = (u32)-1;
    mesa_ipv4_network_t res = *n;
    (void)vtss_conv_prefix_to_ipv4mask(n->prefix_size, &mask);
    res.address = n->address & mask;
    return res;
}

mesa_ipv6_network_t vtss_ipv6_net_mask_out(const mesa_ipv6_network_t *n) {
    mesa_ipv6_t mask;
    mesa_ipv6_network_t res = *n;
    (void)vtss_conv_prefix_to_ipv6mask(n->prefix_size, &mask);
    for (size_t i = 0; i < sizeof(mask.addr); i++)
        res.address.addr[i] = n->address.addr[i] & mask.addr[i];
    return res;
}

mesa_ip_network_t vtss_ip_net_mask_out(const mesa_ip_network_t *n) {
    mesa_ip_network_t res = *n;
    switch (n->address.type) {
    case MESA_IP_TYPE_IPV4: {
        mesa_ipv4_t mask = (u32)-1;
        (void)vtss_conv_prefix_to_ipv4mask(n->prefix_size, &mask);
        res.address.addr.ipv4 = n->address.addr.ipv4 & mask;
        return res;
    }

    case MESA_IP_TYPE_IPV6: {
        mesa_ipv6_t mask;
        (void)vtss_conv_prefix_to_ipv6mask(n->prefix_size, &mask);
        for (size_t i = 0; i < sizeof(mask.addr); i++)
            res.address.addr.ipv6.addr[i] =
                    n->address.addr.ipv6.addr[i] & mask.addr[i];
        return res;
    }
    default:
        return res;
    }
}

BOOL vtss_ip_net_equal(const mesa_ip_network_t *const a,
                       const mesa_ip_network_t *const b) {
    if (a->address.type != b->address.type) {
        return FALSE;
    }

    switch (a->address.type) {
    case MESA_IP_TYPE_IPV4: {
        mesa_ipv4_t mask = (u32)-1;
        if (a->prefix_size == b->prefix_size &&
            vtss_conv_prefix_to_ipv4mask(a->prefix_size, &mask) == VTSS_OK) {
            return (a->address.addr.ipv4 & mask) ==
                   (b->address.addr.ipv4 & mask);
        }
        return FALSE;
    }
    case MESA_IP_TYPE_IPV6: {
        mesa_ipv6_t mask;
        if (a->prefix_size == b->prefix_size &&
            vtss_conv_prefix_to_ipv6mask(a->prefix_size, &mask) == VTSS_OK) {
            size_t i;
            for (i = 0; i < sizeof(mask.addr); i++) {
                u8 maskb = mask.addr[i];
                /* Compare network (masked) only */
                if ((a->address.addr.ipv6.addr[i] & maskb) !=
                    (b->address.addr.ipv6.addr[i] & maskb)) {
                    return FALSE;
                }
            }
            return TRUE;
        }
        return FALSE;
    }
    default:
        ;
    }

    return FALSE;
}

static void operator_ipv6_and(const mesa_ipv6_t *const a,
                              const mesa_ipv6_t *const b,
                              mesa_ipv6_t *const res) {
    int i;
    for (i = 0; i < 16; ++i) {
        res->addr[i] = a->addr[i] & b->addr[i];
    }
}

static BOOL operator_ipv6_equal(const mesa_ipv6_t *const a,
                                const mesa_ipv6_t *const b) {
    int i;
    for (i = 0; i < 16; ++i) {
        if (a->addr[i] != b->addr[i]) {
            return FALSE;
        }
    }
    return TRUE;
}

BOOL vtss_ipv4_net_overlap(const mesa_ipv4_network_t *const a,
                           const mesa_ipv4_network_t *const b) {
    mesa_ipv4_t mask_a = 0, mask_b = 0;

    (void)vtss_conv_prefix_to_ipv4mask(a->prefix_size, &mask_a);
    (void)vtss_conv_prefix_to_ipv4mask(b->prefix_size, &mask_b);

    return (a->address & mask_a) == (b->address & mask_a) ||
           (b->address & mask_b) == (a->address & mask_b);
}

BOOL vtss_ipv6_net_overlap(const mesa_ipv6_network_t *const a,
                           const mesa_ipv6_network_t *const b) {
    mesa_ipv6_t mask_a, mask_b;
    mesa_ipv6_t a_mask_a;
    mesa_ipv6_t a_mask_b;
    mesa_ipv6_t b_mask_a;
    mesa_ipv6_t b_mask_b;

    (void)vtss_conv_prefix_to_ipv6mask(a->prefix_size, &mask_a);
    (void)vtss_conv_prefix_to_ipv6mask(b->prefix_size, &mask_b);

    operator_ipv6_and(&a->address, &mask_a, &a_mask_a);
    operator_ipv6_and(&a->address, &mask_b, &a_mask_b);
    operator_ipv6_and(&b->address, &mask_a, &b_mask_a);
    operator_ipv6_and(&b->address, &mask_b, &b_mask_b);

    return operator_ipv6_equal(&a_mask_a, &b_mask_a) ||
           operator_ipv6_equal(&b_mask_b, &a_mask_b);
}

BOOL vtss_ip_net_overlap(const mesa_ip_network_t *const a,
                         const mesa_ip_network_t *const b) {
    if (a->address.type != b->address.type) {
        return false;
    }

    switch (a->address.type) {
    case MESA_IP_TYPE_IPV4: {
        mesa_ipv4_network_t _a, _b;
        _a.prefix_size = a->prefix_size;
        _a.address = a->address.addr.ipv4;
        _b.prefix_size = b->prefix_size;
        _b.address = b->address.addr.ipv4;
        return vtss_ipv4_net_overlap(&_a, &_b);
    }

    case MESA_IP_TYPE_IPV6: {
        mesa_ipv6_network_t _a, _b;
        _a.prefix_size = a->prefix_size;
        _a.address = a->address.addr.ipv6;
        _b.prefix_size = b->prefix_size;
        _b.address = b->address.addr.ipv6;
        return vtss_ipv6_net_overlap(&_a, &_b);
    }

    default:
        return false;
    }
}

BOOL vtss_ipv4_net_include(const mesa_ipv4_network_t   *const net,
                           const mesa_ipv4_t           *const addr) {
    mesa_ipv4_t mask = 0;
    (void) vtss_conv_prefix_to_ipv4mask(net->prefix_size, &mask);
    return (net->address & mask) == (*addr & mask);
}

BOOL vtss_ipv6_net_include(const mesa_ipv6_network_t   *const net,
                           const mesa_ipv6_t           *const addr) {
    mesa_ipv6_t mask;
    mesa_ipv6_t a;
    mesa_ipv6_t b;

    (void) vtss_conv_prefix_to_ipv6mask(net->prefix_size, &mask);

    operator_ipv6_and(&net->address, &mask, &a);
    operator_ipv6_and(addr, &mask, &b);

    return operator_ipv6_equal(&a, &b);
}


mesa_rc vtss_appl_ip_if_status_to_ip(
        const vtss_appl_ip_if_status_t *const status,
        mesa_ip_addr_t *const ip) {
    if (status->type == VTSS_APPL_IP_IF_STATUS_TYPE_IPV4) {
        ip->type = MESA_IP_TYPE_IPV4;
        ip->addr.ipv4 = status->u.ipv4.net.address;
        return VTSS_RC_OK;
    }

    if (status->type == VTSS_APPL_IP_IF_STATUS_TYPE_IPV6) {
        ip->type = MESA_IP_TYPE_IPV6;
        memcpy(ip->addr.ipv6.addr, status->u.ipv6.net.address.addr, 16);
        return VTSS_RC_OK;
    }

    return VTSS_RC_ERROR;
}

BOOL vtss_ip_net_include(const mesa_ip_network_t     *const net,
                         const mesa_ip_addr_t        *const addr)
{
    if (net->address.type != addr->type) {
        return false;
    }

    switch (net->address.type) {
    case MESA_IP_TYPE_IPV4: {
        mesa_ipv4_network_t net_;
        mesa_ipv4_t addr_;
        net_.prefix_size = net->prefix_size;
        net_.address = net->address.addr.ipv4;
        addr_ = addr->addr.ipv4;
        return vtss_ipv4_net_include(&net_, &addr_);
    }

    case MESA_IP_TYPE_IPV6: {
        mesa_ipv6_network_t net_;
        mesa_ipv6_t addr_;
        net_.prefix_size = net->prefix_size;
        net_.address = net->address.addr.ipv6;
        addr_ = addr->addr.ipv6;
        return vtss_ipv6_net_include(&net_, &addr_);
    }

    default:
        return false;
    }
}


BOOL vtss_if_status_match_vlan(mesa_vid_t vlan,
                               vtss_appl_ip_if_status_t *if_status) {
    if (if_status->if_id.type != VTSS_ID_IF_TYPE_VLAN) {
        return FALSE;
    }

    if (if_status->if_id.u.vlan == vlan) {
        return TRUE;
    }

    return FALSE;
}

BOOL vtss_if_status_match_ip_type(mesa_ip_type_t type,
                                  const vtss_appl_ip_if_status_t *if_status) {
    if (type == MESA_IP_TYPE_IPV4 &&
        if_status->type == VTSS_APPL_IP_IF_STATUS_TYPE_IPV4) {
        return TRUE;
    }

    if (type == MESA_IP_TYPE_IPV6 &&
        if_status->type == VTSS_APPL_IP_IF_STATUS_TYPE_IPV6) {
        return TRUE;
    }

    return FALSE;
}

BOOL vtss_if_status_match_ip(const mesa_ip_addr_t *const ip,
                             vtss_appl_ip_if_status_t *if_status) {
    if (ip->type == MESA_IP_TYPE_IPV4 &&
        if_status->type == VTSS_APPL_IP_IF_STATUS_TYPE_IPV4 &&
        if_status->u.ipv4.net.address == ip->addr.ipv4) {
        return TRUE;
    }

    if (ip->type == MESA_IP_TYPE_IPV6 &&
        if_status->type == VTSS_APPL_IP_IF_STATUS_TYPE_IPV6 &&
        memcmp(if_status->u.ipv6.net.address.addr, ip->addr.ipv6.addr, 16) ==
                0) {
        return TRUE;
    }

    return FALSE;
}

void vtss_ipv4_default_route(mesa_ipv4_t gw, mesa_routing_entry_t *rt) {
    rt->type = MESA_ROUTING_ENTRY_TYPE_IPV4_UC;
    rt->route.ipv4_uc.network.address = 0;
    rt->route.ipv4_uc.network.prefix_size = 0;
    rt->route.ipv4_uc.destination = gw;
}

BOOL vtss_ip_route_valid(const mesa_routing_entry_t *const rt) {
    switch (rt->type) {
    case MESA_ROUTING_ENTRY_TYPE_IPV4_UC: {
        mesa_ipv4_t mask = 0;
        if (vtss_ipv4_addr_is_loopback(&rt->route.ipv4_uc.network.address) ||
            vtss_ipv4_addr_is_multicast(&rt->route.ipv4_uc.network.address) ||
            vtss_ipv4_addr_is_loopback(&rt->route.ipv4_uc.destination) ||
            vtss_ipv4_addr_is_multicast(&rt->route.ipv4_uc.destination)) {
            // IPv4 unicast address only
            return FALSE;
        }
        if (vtss_conv_prefix_to_ipv4mask(rt->route.ipv4_uc.network.prefix_size,
                                         &mask) == VTSS_OK) {
            if (rt->route.ipv4_uc.network.address & ~mask) {
                /* has bits set outside prefix */
                return FALSE;
            }
            return TRUE;
        }
        break;
    }
    case MESA_ROUTING_ENTRY_TYPE_IPV6_UC:
        if (rt->route.ipv6_uc.network.prefix_size <= 128) {
            return TRUE;
        }
        break;
    default:
        ; /* Handled below */
    }
    return FALSE;
}

BOOL vtss_ip_route_nh_valid(const mesa_routing_entry_t *const rt) {
    if (!rt) {
        return FALSE;
    }

    switch (rt->type) {
    case MESA_ROUTING_ENTRY_TYPE_IPV4_UC:
    case MESA_ROUTING_ENTRY_TYPE_IPV4_MC:
        if (!vtss_ipv4_addr_is_zero(rt->route.ipv4_uc.destination) &&
            !vtss_ipv4_addr_is_multicast(&rt->route.ipv4_uc.destination)) {
            return TRUE;
        }
        break;
    case MESA_ROUTING_ENTRY_TYPE_IPV6_UC:
        if (!vtss_ipv6_addr_is_zero(&rt->route.ipv6_uc.destination) &&
            !vtss_ipv6_addr_is_multicast(&rt->route.ipv6_uc.destination)) {
            return TRUE;
        }
        break;
    default:
        break;
    }

    return FALSE;
}

BOOL vtss_ip_ipv4_ifaddr_valid(const mesa_ipv4_network_t *const net) {
    mesa_ipv4_t mask = 0;

    /* IPMC/BCast check */
    if (((net->address >> 24) & 0xff) >= 224) {
        return FALSE;
    }

    // Do not allow address in the 0.0.0.0/8 network
    if (((net->address >> 24) & 0xff) == 0) {
        return FALSE;
    }

    /* Prefix check */
    if (net->prefix_size <= 30 &&
        vtss_conv_prefix_to_ipv4mask(net->prefix_size, &mask) == VTSS_OK) {
        /* Host part cheks */
        if ((net->address & ~mask) != (((u32)-1) & ~mask) &&
            (net->address & ~mask) != 0) {
            return TRUE; /* Not using subnet bcast/zero host part */
        }
    }

    return FALSE;
}

BOOL vtss_ip_ipv6_ifaddr_valid(const mesa_ipv6_network_t *const net) {
    if (net->prefix_size <= 128 &&
        vtss_ipv6_addr_is_mgmt_support(&net->address) &&
        !vtss_ipv6_addr_is_zero(&net->address) &&
        !vtss_ipv6_addr_is_multicast(&net->address)) {
        return TRUE; /* Not using subnet bcast/zero host part */
    }

    return FALSE;
}

BOOL vtss_ip_ifaddr_valid(const mesa_ip_network_t *const net) {
    if (net->address.type == MESA_IP_TYPE_IPV4) {
        mesa_ipv4_network_t n;
        n.address = net->address.addr.ipv4;
        n.prefix_size = net->prefix_size;
        return vtss_ip_ipv4_ifaddr_valid(&n);

    } else if (net->address.type == MESA_IP_TYPE_IPV6) {
        mesa_ipv6_network_t n;
        n.address = net->address.addr.ipv6;
        n.prefix_size = net->prefix_size;
        return vtss_ip_ipv6_ifaddr_valid(&n);
    }

    return FALSE; /* Bad address */
}

BOOL vtss_if_id_equal(const vtss_if_id_t *const a,
                      const vtss_if_id_t *const b) {
    if (memcmp(a, b, sizeof(vtss_if_id_t)) == 0) {
        return TRUE;
    } else {
        return FALSE;
    }
}

int vtss_ip_ip_addr_to_txt(char *buf, int size,
                           const mesa_ip_addr_t *const ip) {
    int s = 0;
    char _buf[41];
    mesa_ipv6_t adrs6;

    switch (ip->type) {
    case MESA_IP_TYPE_NONE:
        PRINTF("NONE");
        break;
    case MESA_IP_TYPE_IPV4:
        PRINTF(VTSS_IPV4_FORMAT, VTSS_IPV4_ARGS(ip->addr.ipv4));
        break;
    case MESA_IP_TYPE_IPV6:
        /* Ensure the linklocal IPv6 address to get rid of the scope id for
         * display */
        memcpy(&adrs6, &(ip->addr.ipv6), sizeof(mesa_ipv6_t));
        if (vtss_ipv6_addr_is_link_local(&adrs6)) {
            adrs6.addr[2] = adrs6.addr[3] = 0x0;
        }
        (void)misc_ipv6_txt(&adrs6, _buf);
        _buf[40] = 0;
        PRINTF("%s", _buf);
        break;
    default:
        PRINTF("Unknown-type:%u", ip->type);
    }

    return s;
}

int vtss_ip_ip_network_to_txt(char *buf, int size,
                              const mesa_ip_network_t *const ip) {
    int s = 0;
    PRINTFUNC(vtss_ip_ip_addr_to_txt, &ip->address);
    PRINTF("/%d", ip->prefix_size);
    return s;
}

int vtss_ip_neighbour_to_txt(char *buf, int size,
                             const vtss_neighbour_t *const nb) {
    int s = 0;
    PRINTF("{DMAC: " VTSS_MAC_FORMAT " VLAN: %u IP: ", VTSS_MAC_ARGS(nb->dmac),
           nb->vlan);
    PRINTFUNC(vtss_ip_ip_addr_to_txt, &(nb->dip));
    PRINTF("}");
    return s;
}

int vtss_ip_route_entry_to_txt(char *buf, int size,
                               const mesa_routing_entry_t *const rt) {
    int s = 0;
    char _buf[41];
    mesa_ipv6_t adrs6;

    switch (rt->type) {
    case MESA_ROUTING_ENTRY_TYPE_IPV4_UC:
        PRINTF(VTSS_IPV4N_FORMAT " via ",
               VTSS_IPV4N_ARG(rt->route.ipv4_uc.network));
        PRINTF(VTSS_IPV4_FORMAT, VTSS_IPV4_ARGS(rt->route.ipv4_uc.destination));
        break;

    case MESA_ROUTING_ENTRY_TYPE_IPV6_UC:
        (void)misc_ipv6_txt(&(rt->route.ipv6_uc.network.address), _buf);
        _buf[40] = 0;
        PRINTF("%s/%d via ", _buf, rt->route.ipv6_uc.network.prefix_size);
        /* Ensure the linklocal IPv6 address to get rid of the scope id for
         * display */
        memcpy(&adrs6, &(rt->route.ipv6_uc.destination), sizeof(mesa_ipv6_t));
        if (vtss_ipv6_addr_is_link_local(&adrs6)) {
            adrs6.addr[2] = adrs6.addr[3] = 0x0;
        }
        (void)misc_ipv6_txt(&adrs6, _buf);
        _buf[40] = 0;
        PRINTF("%s", _buf);
        break;

    case MESA_ROUTING_ENTRY_TYPE_IPV4_MC:
        PRINTF("IPv4-MC-not-implemented");
        break;

    case MESA_ROUTING_ENTRY_TYPE_INVALID:
        PRINTF("Invalid");
        break;

    default:
        PRINTF("Unknown-type:%u", rt->type);
        break;
    }

    return s;
}


int vtss_ip_if_id_to_txt(char *buf, int size, const vtss_if_id_t *const if_id) {
    int s = 0;

    switch (if_id->type) {
    case VTSS_ID_IF_TYPE_INVALID:
        PRINTF("invalid");
        break;

    case VTSS_ID_IF_TYPE_VLAN:
        PRINTF("VLAN%u", if_id->u.vlan);
        break;

    case VTSS_ID_IF_TYPE_OS_ONLY:
        PRINTF("OS:%s", if_id->u.os.name);
        break;

    default:
        PRINTF("unknown");
        break;
    }

    buf[MIN(size - 1, s)] = 0;
    return s;
}

int vtss_ip_if_status_to_txt(char *buf, int size, vtss_appl_ip_if_status_t *st,
                             u32 length) {
    /*lint --e{429} */

    u32 i;
    int s = 0;
#if defined(VTSS_SW_OPTION_IPV6)
    char _buf[128];
    mesa_ipv6_t adrs6;
#endif

    if (length == 0) {
        return 0;
    }

    PRINTFUNC(vtss_ip_if_id_to_txt, &(st->if_id));
    PRINTF("\n");

    for (i = 0; i < length; ++i, ++st) {
        switch (st->type) {
        case VTSS_APPL_IP_IF_STATUS_TYPE_LINK:
            PRINTF("  LINK: " VTSS_MAC_FORMAT " Mtu:%u <",
                   VTSS_MAC_ARGS(st->u.link.mac), st->u.link.mtu);
            PRINTFUNC(vtss_if_link_flag_to_txt, st->u.link.flags);
            PRINTF(">\n");
            break;

        case VTSS_APPL_IP_IF_STATUS_TYPE_IPV4:
            PRINTF("  IPv4: " VTSS_IPV4N_FORMAT,
                   VTSS_IPV4N_ARG(st->u.ipv4.net));
            PRINTF(" " VTSS_IPV4_FORMAT "\n",
                   VTSS_IPV4_ARGS(st->u.ipv4.info.broadcast));
            break;

        case VTSS_APPL_IP_IF_STATUS_TYPE_DHCP:
            PRINTF("  DHCP: ");
            PRINTFUNC(vtss::dhcp::to_txt, &st->u.dhcp4c);
            PRINTF("\n");
            break;

        case VTSS_APPL_IP_IF_STATUS_TYPE_IPV6:
#if defined(VTSS_SW_OPTION_IPV6)
            /* Ensure the linklocal IPv6 address to get rid of the scope id for
             * display */
            memcpy(&adrs6, &(st->u.ipv6.net.address), sizeof(mesa_ipv6_t));
            if (vtss_ipv6_addr_is_link_local(&adrs6)) {
                adrs6.addr[2] = adrs6.addr[3] = 0x0;
            }
            (void)misc_ipv6_txt(&adrs6, _buf);
            PRINTF("  IPv6: %s/%d", _buf, st->u.ipv6.net.prefix_size);
            PRINTF(" <");
            PRINTFUNC(vtss_if_ipv6_flag_to_txt, st->u.ipv6.info.flags);
            PRINTF(">\n");
#endif /* VTSS_SW_OPTION_IPV6 */
            break;

        default:
            PRINTF("  UNKNOWN\n");
        }
    }

    buf[MIN(size - 1, s)] = 0;
    return s;
}

int vtss_ip_neighbour_status_to_txt(char *buf, int size,
                                    vtss_neighbour_status_t *st) {
    int s = 0;

    PRINTFUNC(vtss_ip_ip_addr_to_txt, &st->ip_address);

    if (st->flags & VTSS_APPL_IP_NEIGHBOUR_FLAG_VALID) {
        PRINTF(" via ");
        PRINTFUNC(vtss_ip_if_id_to_txt, &(st->interface));
        PRINTF(":" VTSS_MAC_FORMAT, VTSS_MAC_ARGS(st->mac_address));
    } else {
        if (st->interface.type == VTSS_ID_IF_TYPE_OS_ONLY) {
            PRINTF(" via ");
            PRINTFUNC(vtss_ip_if_id_to_txt, &(st->interface));
        } else {
            PRINTF(" (Incomplete)");
        }
    }

    if (st->state[0]) {
        PRINTF(" %s", st->state);
    }
    if (st->flags & VTSS_APPL_IP_NEIGHBOUR_FLAG_PERMANENT) {
        PRINTF(" Permanent");
    }
    if (st->flags & VTSS_APPL_IP_NEIGHBOUR_FLAG_ROUTER) {
        PRINTF(" Router");
    }
    if (st->flags & VTSS_APPL_IP_NEIGHBOUR_FLAG_HARDWARE) {
        PRINTF(" Hardware");
    }

    return s;
}

int vtss_ip_stat_icmp_type_txt(char *buf, int size,
                               const mesa_ip_type_t version,
                               const u32 icmp_type) {
    int s = 0;

    switch (icmp_type) {
    case 0:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Echo Reply");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 1:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Reserved");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Destination Unreachable");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 2:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Reserved");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Packet Too Big");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 3:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Destination Unreachable");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Time Exceeded");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 4:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Source Quench (Deprecated)");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Parameter Problem");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 5:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Redirect");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 6:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Alternate Host Address");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 7:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 8:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Echo");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 9:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Router Advertisement");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 10:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Router Solicitation");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 11:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Time Exceeded");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 12:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Parameter Problem");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 13:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Timestamp");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 14:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Timestamp Reply");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 15:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Information Request");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 16:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Information Reply");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 17:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Address Mask Request");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 18:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Address Mask Reply");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 19:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Reserved (for Security)");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 30:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Traceroute");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 31:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Datagram Conversion Error");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 32:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Mobile Host Redirect");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 33:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("IPv6 Where-Are-You");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 34:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("IPv6 I-Am-Here");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 35:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Mobile Registration Request");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 36:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Mobile Registration Reply");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 37:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Domain Name Request");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 38:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Domain Name Reply");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 39:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("SKIP");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 40:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Photuris");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 41:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("ICMP messages utilized by experimental mobility protocols");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 100:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Private experimentation");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 101:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Private experimentation");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 127:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Reserved for expansion of ICMPv6 error messages");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 128:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Echo Request");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 129:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Echo Reply");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 130:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Multicast Listener Query");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 131:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Multicast Listener Report");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 132:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Multicast Listener Done");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 133:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Router Solicitation (NDP)");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 134:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Router Advertisement (NDP)");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 135:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Neighbor Solicitation (NDP)");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 136:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Neighbor Advertisement (NDP)");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 137:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Redirect Message (NDP)");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 138:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Router Renumbering");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 139:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("ICMP Node Information Query");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 140:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("ICMP Node Information Response");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 141:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Inverse Neighbor Discovery Solicitation Message");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 142:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Inverse Neighbor Discovery Advertisement Message");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 143:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Multicast Listener Discovery (MLDv2) reports");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 144:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Home Agent Address Discovery Request Message");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 145:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Home Agent Address Discovery Reply Message");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 146:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Mobile Prefix Solicitation");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 147:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Mobile Prefix Advertisement");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 148:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Certification Path Solicitation (SEND)");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 149:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Certification Path Advertisement (SEND)");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 151:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Multicast Router Advertisement (MRD)");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 152:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Multicast Router Solicitation (MRD)");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 153:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Multicast Router Termination (MRD)");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 155:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("RPL Control Message");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 200:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Private experimentation");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 201:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Unassigned");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Private experimentation");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 253:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("RFC3692-style Experiment 1");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 254:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("RFC3692-style Experiment 2");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Invalid!");
        }
        break;
    case 255:
        if (version == MESA_IP_TYPE_IPV4) {
            PRINTF("Reserved");
        } else if (version == MESA_IP_TYPE_IPV6) {
            PRINTF("Reserved for expansion of ICMPv6 informational messages");
        } else {
            PRINTF("Invalid!");
        }
        break;
    default:
        if ((version != MESA_IP_TYPE_IPV4) && (version != MESA_IP_TYPE_IPV6)) {
            PRINTF("Invalid!");
            break;
        }

        if ((icmp_type > 19) && (icmp_type < 30)) {
            if (version != MESA_IP_TYPE_IPV4) {
                PRINTF("Unassigned");
            } else {
                PRINTF("Reserved (for Robustness Experiment)");
            }
        } else if ((icmp_type > 41) && (icmp_type < 253)) {
            PRINTF("Unassigned");
        } else {
            PRINTF("Unknown!");
        }
        break;
    }

    return s;
}

static int int_cmp(int a, int b) {
    if (a == b) {
        return 0;
    } else if (a < b) {
        return -1;
    } else {
        return 1;
    }
}

static int if_id_cmp(const void *_a, const void *_b) {
    const vtss_if_id_t *a = (vtss_if_id_t *)_a;
    const vtss_if_id_t *b = (vtss_if_id_t *)_b;

    // first order compare
    int res = int_cmp(a->type, b->type);
    if (res != 0) {
        return res;
    }

    // second order compare
    switch (a->type) {
    case VTSS_ID_IF_TYPE_VLAN:
        return int_cmp(a->u.vlan, b->u.vlan);
    case VTSS_ID_IF_TYPE_OS_ONLY:
        return int_cmp(a->u.os.ifno, b->u.os.ifno);
    default:
        return 0;
    }
}

static int status_cmp(const void *_a, const void *_b) {
    const vtss_appl_ip_if_status_t *a = (vtss_appl_ip_if_status_t *)_a;
    const vtss_appl_ip_if_status_t *b = (vtss_appl_ip_if_status_t *)_b;

    // first order parameter is interface
    int res = if_id_cmp(&(a->if_id), &(b->if_id));
    if (res != 0) {
        return res;
    }

    // second order parameter is status type
    return int_cmp(a->type, b->type);
}

void vtss_ip_if_status_sort(u32 cnt, vtss_appl_ip_if_status_t *status) {
    qsort(status, cnt, sizeof(vtss_appl_ip_if_status_t), status_cmp);
}

#if defined(VTSS_SW_OPTION_IP)
#if defined(VTSS_SW_OPTION_FRR)
static std::string vtss_appl_route_ipv4_entry_print
(const vtss_appl_route_ipv4_key_t& key, const vtss_appl_route_ipv4_status_t& status, bool skip_key1_key2)
{

    StringStream bs;

    if (!skip_key1_key2) {
        switch (key.protocol) {
        case VTSS_APPL_ROUTE_PROTO_DHCP:
            bs << "D";
            break;
        case VTSS_APPL_ROUTE_PROTO_STATIC:
            bs << "S";
            break;
        case VTSS_APPL_ROUTE_PROTO_CONNECTED:
            bs << "C";
            break;
        case VTSS_APPL_ROUTE_PROTO_OSPF:
            bs << "O";
            break;
        case VTSS_APPL_ROUTE_PROTO_COUNT:
            bs << "?";
            /* ignore default case to catch compile warning if any prtocol is
             * missing */
        }
    } else {
            bs << " ";
    }

    if (status.selected) {
        bs <<"* ";
    } else {
        bs <<"  ";
    }

    /* If the network information needs to be skiped. like the second line
       in above example:
         S  25.0.0.0/8 [1/0] via 60.0.0.25, Vlan60
          *            [9/0] via 50.0.0.100, Vlan50
       we need to know the length start of network,
       the method is to use another buffer stream 'ss_net'
    */
    StringStream ss_net;
    ss_net << key.network;

    if (skip_key1_key2) {
        bs << WhiteSpace(ss_net.buf.size());
    } else {
        bs << ss_net.buf;
    }

    // All routes have distance/metric except connected routes.
    if (key.protocol != VTSS_APPL_ROUTE_PROTO_CONNECTED) {
        bs << " [" << status.distance << "/" << status.metric << "]";
    }

    if (key.nexthop == 0) {
        bs << (vtss_ifindex_is_none(status.ifindex) ?
              " is discarded entry" : " is directly connected");
    } else {
        bs << " via " << Ipv4Address(key.nexthop);
    }

    if (!status.active) {
        if (key.nexthop == 0) {
            bs << ", " << status.ifindex;
        }
        bs << " inactive ";
    } else {
        bs << ", " << status.ifindex;
        if (key.protocol == VTSS_APPL_ROUTE_PROTO_OSPF) {
            /* I have considered to use AsTimeUs<uint64_t> to display
               the days, but the display format is too long
               because it count time as usecond. So I go
               back to use misc_time_txt().
            */
            bs << ", " << misc_time_txt(status.uptime);
        }
    }
    return std::move(bs.buf);
}

/*
example:
Codes: C - connected, S - static,
       * - selected route

S* 0.0.0.0/0 via 10.9.52.200, Vlan1
C* 10.9.52.0/24 is directly connected, Vlan1
S  25.0.0.0/8 [1/0] via 60.0.0.25, Vlan60
 *                  via 50.0.0.100, Vlan50
O* 40.0.0.0/8 [110/65735] via 60.0.0.89, Vlan60, 00:00:56
S  41.0.0.0/8 [1/0] via 41.0.0.10 inactive
S* 50.0.0.0/8 [1/0] is directly connected, Vlan50 inactive
O* 55.0.0.0/8 [110/65635] via 60.0.0.89, Vlan60, 00:00:56
O  60.0.0.0/8 [110/100] is directly connected, Vlan60, 00:52:54
C* 60.0.0.0/8 is directly connected, Vlan60
O  61.0.0.0/8 [110/1000] is directly connected, Vlan61, 1d03h13m
C* 61.0.0.0/8 is directly connected, Vlan61
*/
int vtss_appl_route_ipv4_status_print(vtss_ip_cli_pr *pr) {
    vtss_appl_route_ipv4_key_t key, previous_key;
    bool skip_k1_k2 = false;

    vtss::Map<vtss_appl_route_ipv4_key_t,
            vtss_appl_route_ipv4_status_t> ipv4_routes;
    mesa_rc rc;
    std::string s;

    rc = vtss_appl_route_ipv4_status_get_all(ipv4_routes);

    if ( rc == VTSS_RC_OK) {
        s += "Codes: C - connected, S - static, O - OSPF,\n";
        s += "       * - selected route, D - DHCP installed route\n\n";
    }
    (void)pr("%s\n", s.c_str());

    // Initialize previous_key to an impossible matchable value: protocol =
    // VTSS_APPL_ROUTE_PROTO_COUNT;
    memset(&previous_key, 0, sizeof(vtss_appl_route_ipv4_key_t));
    previous_key.protocol = VTSS_APPL_ROUTE_PROTO_COUNT;

    vtss::Map<vtss_appl_route_ipv4_key_t, vtss_appl_route_ipv4_status_t>::iterator it;
    for (it=ipv4_routes.begin(); it!=ipv4_routes.end(); ++it) {
        key = it->first;
        if (previous_key.network == key.network && previous_key.protocol == key.protocol) {
            skip_k1_k2 = true;
        } else {
            skip_k1_k2 = false;
            previous_key = key;
        }
        s = vtss_appl_route_ipv4_entry_print(it->first, it->second, skip_k1_k2);
        (void)pr("%s\n", s.c_str());
    }

    return 0;
}
#else
int vtss_appl_ip_ipv4_route_status_print(vtss_ip_cli_pr *pr) {
#define SIZE 2048
#define BUF_SIZE 128
    u32 i, cnt = 0, res = 0;
    mesa_rc rc = VTSS_RC_OK;
    vtss_appl_ip_ipv4_route_key_status_t *rts;
    char buf[BUF_SIZE];

    VTSS_CALLOC_CAST(rts, SIZE, sizeof(vtss_appl_ip_ipv4_route_key_status_t));

    if (rts == NULL) {
        return VTSS_RC_ERROR;
    }

#define P(...)                          \
    {                                   \
        int __r__ = (*pr)(__VA_ARGS__); \
        if (__r__ >= 0) {               \
            res += __r__;               \
        } else {                        \
            res = __r__;                \
            goto DONE;                  \
        }                               \
    }

    rc = vtss_appl_ip_ipv4_route_status_get_list(SIZE, rts, &cnt);
    if (rc != VTSS_RC_OK) {
        goto DONE;
    }

    for (i = 0; i < cnt; ++i) {
        vtss_appl_ip_ipv4_route_key_status_t *rt = &rts[i];
        // Net
        P(VTSS_IPV4N_FORMAT " via ", VTSS_IPV4N_ARG(rt->key.network));

        // via..
        if (rt->key.destination == 0) {
            // Interface
            if (vtss_ifindex_is_vlan(rt->val.next_hop_interface)) {
                P("VLAN%u", rt->val.next_hop_interface);
            } else {
                P("interface UNKNOWN");
            }
        } else {
            // IP address
            P(VTSS_IPV4_FORMAT, VTSS_IPV4_ARGS(rt->key.destination));
        }

        (void)vtss_routing_flags_to_txt(buf, BUF_SIZE, rt->val.flags);
        P(" <%s>\n", buf);
    }

DONE:
    VTSS_FREE(rts);
    return res;
#undef BUF_SIZE
#undef SIZE
#undef P
}
#endif /*  VTSS_SW_OPTION_FRR */

int vtss_appl_ip_ipv6_route_status_print(vtss_ip_cli_pr *pr) {
#define SIZE 2048
#define BUF_SIZE 128
    u32 i, cnt = 0, res = 0;
    mesa_rc rc = VTSS_RC_OK;
    vtss_appl_ip_ipv6_route_key_status_t *rts;
    char buf[BUF_SIZE];

    VTSS_CALLOC_CAST(rts, SIZE, sizeof(vtss_appl_ip_ipv6_route_key_status_t));

    if (rts == NULL) {
        return VTSS_RC_ERROR;
    }

#define P(...)                          \
    {                                   \
        int __r__ = (*pr)(__VA_ARGS__); \
        if (__r__ >= 0) {               \
            res += __r__;               \
        } else {                        \
            res = __r__;                \
            goto DONE;                  \
        }                               \
    }

    rc = vtss_appl_ip_ipv6_route_status_get_list(SIZE, rts, &cnt);
    if (rc != VTSS_RC_OK) {
        goto DONE;
    }

    for (i = 0; i < cnt; ++i) {
        vtss_appl_ip_ipv6_route_key_status_t *rt = &rts[i];

        // Net
        (void)misc_ipv6_txt(&rt->key.route.network.address, buf);
        P("%s/%d via ", buf, rt->key.route.network.prefix_size);

        // Destination
        if (vtss_ipv6_addr_is_zero(&rt->key.route.destination)) {
            // Interface
            if (vtss_ifindex_is_vlan(rt->val.next_hop_interface)) {
                P("VLAN%u", rt->val.next_hop_interface);
            } else {
                P("interface UNKNOWN");
            }
        } else {
            mesa_ipv6_t adrs6;

            /* Ensure the linklocal IPv6 address to get rid of the scope id for
             * display */
            memcpy(&adrs6, &(rt->key.route.destination), sizeof(mesa_ipv6_t));
            if (vtss_ipv6_addr_is_link_local(&adrs6)) {
                adrs6.addr[2] = adrs6.addr[3] = 0x0;
            }

            // IP address
            (void)misc_ipv6_txt(&adrs6, buf);
            P("%s", buf);
        }

        (void)vtss_routing_flags_to_txt(buf, BUF_SIZE, rt->val.flags);
        P(" <%s>\n", buf);
    }

DONE:
    VTSS_FREE(rts);
    return res;
#undef BUF_SIZE
#undef SIZE
#undef P
}

int vtss_ip_route_print(mesa_routing_entry_type_t type, vtss_ip_cli_pr *pr) {
    if (type == MESA_ROUTING_ENTRY_TYPE_IPV6_UC)
        return vtss_appl_ip_ipv6_route_status_print(pr);

    if (type == MESA_ROUTING_ENTRY_TYPE_IPV4_UC)
#if defined(VTSS_SW_OPTION_FRR)
        return vtss_appl_route_ipv4_status_print(pr);
#else
        return vtss_appl_ip_ipv4_route_status_print(pr);
#endif /*  VTSS_SW_OPTION_FRR */

    return 0;
}

/* CLI/ICLI helpers -------------------------------------------------------- */
int vtss_ip_if_print(vtss_ip_cli_pr *pr, BOOL vlan_only,
                     vtss_appl_ip_if_status_type_t type) {
#define BUF_SIZE 1024
#define MAX_IF 130
    u32 i;
    mesa_rc rc;
    int res = 0;
    BOOL first = TRUE;
    u32 start, if_cnt = 0, if_st_cnt = 0;
    char buf[BUF_SIZE];

    // Ahh, very inefficient... way too small stack size....
    vtss_appl_ip_if_status_t *status = (vtss_appl_ip_if_status_t *)VTSS_CALLOC(
            IP_MAX_STATUS_OBJS, sizeof(vtss_appl_ip_if_status_t));

    if (status == NULL) {
        return -1;
    }

    rc = vtss_ip_ifs_status_get(type, IP_MAX_STATUS_OBJS, &if_st_cnt, status);
    if (rc != VTSS_RC_OK) {
        res = 0;
        goto DONE;
    }
    vtss_ip_if_status_sort(if_st_cnt, status);


    for (i = 0; i < if_st_cnt; ++i) {
        vtss_if_id_t id;

        if (vlan_only && status[i].if_id.type != VTSS_ID_IF_TYPE_VLAN) {
            continue;
        }

        if (!first) {
            (void)(*pr)("\n");
        }

        start = i;
        if_cnt = 1;
        id = status[i].if_id;

        // count matching interfaces in a row
        while (i < if_st_cnt) {
            // Check if next IF is not equal to this IF
            if (!vtss_if_id_equal(&id, &(status[i + 1].if_id))) {
                break;
            }
            ++i, ++if_cnt;
        }

        res += vtss_ip_if_status_to_txt(buf, BUF_SIZE, &status[start], if_cnt);
        (void)(*pr)("%s", buf);
        first = FALSE;
    }
#undef BUF_SIZE

DONE:
    if (status) {
        VTSS_FREE(status);
    }

    return res;
}

int vtss_ip_if_brief_print(mesa_ip_type_t type, vtss_ip_cli_pr *pr)
{
#define BUF_SIZE 64
#define MAX_IF 130
    vtss_ifindex_t           ifidx, *ifidx_in = NULL;
    mesa_ipv4_network_t      ipv4_net, *ipv4_net_in = NULL;
    mesa_ipv6_network_t      ipv6_net, *ipv6_net_in = NULL;
    vtss_appl_ip_if_status_t link;
    vtss_appl_ip_ipv4_conf_t conf;
    char                     buf[BUF_SIZE];
    BOOL                     first = TRUE;

    if (type == MESA_IP_TYPE_IPV4) {
        for (; vtss_appl_ip_if_status_ipv4_itr(ifidx_in, &ifidx, ipv4_net_in, &ipv4_net) == VTSS_RC_OK;
             ifidx_in = &ifidx, ipv4_net_in = &ipv4_net) {

            if (vtss_appl_ip_ipv4_conf_get(ifidx, &conf) != VTSS_RC_OK ||
                vtss_appl_ip_if_status_get_first(VTSS_APPL_IP_IF_STATUS_TYPE_LINK, ifidx, &link) != VTSS_RC_OK) {
                continue;
            }

            if (first) {
                first = FALSE;
                (void)(*pr)("Interface  Address              Method   Status\n");
                (void)(*pr)("---------- -------------------- -------- ------\n");
            }
            (void)vtss_ip_ifindex_to_txt(buf, BUF_SIZE, ifidx);
            (void)(*pr)("%-10s ", buf);
            (void)snprintf(buf, BUF_SIZE, VTSS_IPV4N_FORMAT, VTSS_IPV4N_ARG(ipv4_net));
            (void)(*pr)("%-20s %-8s %-6s\n", buf, conf.dhcpc ? "DHCP" : "Manual", link.u.link.flags & VTSS_APPL_IP_IF_LINK_FLAG_UP ? "UP" : "DOWN");
        }
    } else {
        for (; vtss_appl_ip_if_status_ipv6_itr(ifidx_in, &ifidx, ipv6_net_in, &ipv6_net) == VTSS_RC_OK;
             ifidx_in = &ifidx, ipv6_net_in = &ipv6_net) {

            if (vtss_appl_ip_if_status_get_first(VTSS_APPL_IP_IF_STATUS_TYPE_LINK, ifidx, &link) != VTSS_RC_OK) {
                continue;
            }

            if (first) {
                first = FALSE;
                (void)(*pr)("Interface  Address                                     Status\n");
                (void)(*pr)("---------- ------------------------------------------- ------\n");
            }
            (void)vtss_ip_ifindex_to_txt(buf, BUF_SIZE, ifidx);
            (void)(*pr)("%-10s ", buf);
            (void)snprintf(buf, BUF_SIZE, VTSS_IPV6N_FORMAT, VTSS_IPV6N_ARG(ipv6_net));
            (void)(*pr)("%-43s %-6s\n", buf, link.u.link.flags & VTSS_APPL_IP_IF_LINK_FLAG_UP ? "UP" : "DOWN");
        }
    }
    return 0;
#undef BUF_SIZE
}

int vtss_ip_nb_print(mesa_ip_type_t type, vtss_ip_cli_pr *pr)
{
#define BUF 256
#define ARP_MAX 1024
    mesa_rc rc;
    char buf[BUF];
    u32 cnt, i, s = 0;
    vtss_neighbour_status_t *status;

    VTSS_CALLOC_CAST(status, ARP_MAX, sizeof(vtss_neighbour_status_t));
    if (!status) {
        return 0;
    }

    rc = vtss_ip_nb_status_get(type, ARP_MAX, &cnt, status);
    if (rc != VTSS_RC_OK) {
        s += (*pr)("Failed to get neighbour cache\n");
        VTSS_FREE(status);
        return s;
    }

    for (i = 0; i < cnt; ++i) {
        if (vtss_ip_neighbour_status_to_txt(buf, BUF, &status[i])) {
            s += (*pr)("%s\n", buf);
        }
    }

#undef ARP_MAX
#undef BUF
    VTSS_FREE(status);
    return s;
}

mesa_rc vtss_ip_ip_to_if(const mesa_ip_addr_t *const ip,
                         vtss_appl_ip_if_status_t *if_status) {
    // NOTE: This could be optimized by inspected the kernel space interface
    // structures instead. If this is done, there would be no need for a memory
    // allocation.
    u32 cnt, i;
    mesa_rc rc = VTSS_RC_ERROR;

    if (!msg_switch_is_master()) {
        return IP_ERROR_FAILED;
    }

    auto status = VTSS_CALLOC_UNIQ(vtss_appl_ip_if_status_t,
                                   IP_MAX_STATUS_OBJS);

    if (!status) {
        return VTSS_RC_ERROR;
    }

    rc = vtss_ip_ifs_status_get(VTSS_APPL_IP_IF_STATUS_TYPE_ANY,
                                IP_MAX_STATUS_OBJS, &cnt, status.get());
    if (rc != VTSS_RC_OK) {
        return rc;
    }

    for (i = 0; i < cnt; ++i) {
        if (vtss_if_status_match_ip(ip, &status[i])) {
            *if_status = status[i];
            return VTSS_RC_OK;
        }
    }

    return VTSS_RC_ERROR;
}


// Function for getting ip for a specific port. The IP returned is based upon
// the PVID for the given port.
mesa_rc vtss_ip_ip_by_port(const mesa_port_no_t iport, mesa_ip_type_t type,
                           mesa_ip_addr_t *const src) {
    vtss_appl_vlan_port_conf_t vlan_conf;

    // Getting PVID
    VTSS_RC(vlan_mgmt_port_conf_get(VTSS_ISID_LOCAL, iport, &vlan_conf,
                                    VTSS_APPL_VLAN_USER_ALL, TRUE));

    // Getting the IP for the VLAN with the PVID
    return vtss_ip_ip_by_vlan(vlan_conf.hybrid.pvid, type, src);
}
#endif

vtss::ostream &operator<<(vtss::ostream &o, const mesa_ipv6_uc_t &i) {
    o << i.network << " via " << i.destination;
    return o;
}

vtss::ostream &operator<<(vtss::ostream &o,
                          const vtss_appl_ip_ipv6_route_conf_t &v) {
    o << v.route;
    if (v.interface != VTSS_IFINDEX_NONE) o << "%" << v.interface;
    return o;
}

vtss::ostream &operator<<(vtss::ostream &o, const mesa_ipv4_uc_t &r) {
    o << r.network << " via " << vtss::Ipv4Address(r.destination);
    return o;
}



vtss::ostream &operator<<(vtss::ostream &o, const mesa_routing_entry_t &r) {
    switch (r.type) {
    case MESA_ROUTING_ENTRY_TYPE_IPV6_UC:
        o << r.route.ipv6_uc.network << " via " << r.route.ipv6_uc.destination;

        if (r.vlan != 0) o << "%" << r.vlan;
        break;

    case MESA_ROUTING_ENTRY_TYPE_IPV4_UC:
        o << r.route.ipv4_uc.network << " via "
          << vtss::Ipv4Address(r.route.ipv4_uc.destination);
        break;

    default:
        o << "<Unknown route entry type: " << (uint32_t)r.type << ">";
    }

    return o;
}

vtss::ostream &operator<<(vtss::ostream &o, const vtss_routing_params_t &r) {
    switch (r.owner) {
    case VTSS_APPL_IP_ROUTE_OWNER_DYNAMIC_USER:
        o << "DYNAMIC_USER";
        break;

    case VTSS_APPL_IP_ROUTE_OWNER_STATIC_USER:
        o << "STATIC_USER";
        break;

    case VTSS_APPL_IP_ROUTE_OWNER_DHCP:
        o << "DHCP";
        break;

    default:
        o << "<Unknown owner: " << (uint32_t)r.owner << ">";
    }

    return o;
}

vtss::ostream &operator<<(vtss::ostream &o,
                          const vtss_appl_ip_ipv6_neighbour_status_key_t &k) {
    o << k.ip_address << "%" << k.interface;
    return o;
}

vtss::ostream &operator<<(vtss::ostream &o, const vtss_if_id_t &if_id) {
    switch (if_id.type) {
    case VTSS_ID_IF_TYPE_INVALID:
        o << "invalid";
        return o;

    case VTSS_ID_IF_TYPE_VLAN:
        o << "VLAN:" << if_id.u.vlan;
        return o;

    case VTSS_ID_IF_TYPE_OS_ONLY:
        o << "OS:" << if_id.u.os.name;
        return o;

    default:
        o << "unknown";
        return o;
    }
}

vtss::ostream &operator<<(vtss::ostream &o,
                          const vtss_appl_ip_if_status_link_t &s) {
    o << "{os-ifidx: " << s.os_if_index << " mtu: " << s.mtu
      << " mac: " << s.mac << " flags: " << s.flags << "}";
    return o;
}

vtss::ostream &operator<<(vtss::ostream &o,
                          const vtss_appl_ip_if_status_ipv4_t &s) {
    // TODO, print info??
    o << s.net;
    return o;
}

vtss::ostream &operator<<(vtss::ostream &o,
                          const vtss_appl_ip_if_status_ipv6_t &s) {
    // TODO, print info??
    o << "net: " << s.net;
    return o;
}

vtss::ostream &operator<<(vtss::ostream &o, vtss_appl_ip_if_link_flag_t f) {
    BOOL first = TRUE;

    o << "[";
/*lint --e{438} */
#define F(X)                                 \
    if (f & VTSS_APPL_IP_IF_LINK_FLAG_##X) { \
        if (!first) o << " ";                \
        first = FALSE;                       \
        o << #X;                             \
    }

    F(UP);
    F(BROADCAST);
    F(LOOPBACK);
    F(RUNNING);
    F(NOARP);
    F(PROMISC);
    F(MULTICAST);
    F(IPV6_RA_MANAGED);
    F(IPV6_RA_OTHER);
#undef F
    o << "]";

    return o;
}

vtss::ostream &operator<<(vtss::ostream &o,
                          const vtss_appl_ip_if_status_dhcp4c_t &s) {
    o << "{state: " << s.state << " server: " << s.server_ip << "}";
    return o;
}

vtss::ostream &operator<<(vtss::ostream &o, const vtss_appl_ip_if_status_t &s) {
    o << s.if_id;

    switch (s.type) {
    case VTSS_APPL_IP_IF_STATUS_TYPE_LINK:
        o << " LINK: " << s.u.link;
        break;

    case VTSS_APPL_IP_IF_STATUS_TYPE_IPV4:
        o << " IPV4: " << s.u.ipv4;
        break;

    case VTSS_APPL_IP_IF_STATUS_TYPE_DHCP:
        o << " DHCP: " << s.u.dhcp4c;
        break;

    case VTSS_APPL_IP_IF_STATUS_TYPE_IPV6:
#if defined(VTSS_SW_OPTION_IPV6)
        o << " IPV6: " << s.u.ipv6;
#endif /* VTSS_SW_OPTION_IPV6 */
        break;

    default:
        o << "  UNKNOWN";
        break;
    }

    return o;
}

bool operator<(const mesa_ipv4_network_t &a, const mesa_ipv4_network_t &b) {
    if (a.address != b.address) return a.address < b.address;
    return a.prefix_size < b.prefix_size;
}

bool operator==(const mesa_ipv4_network_t &a, const mesa_ipv4_network_t &b) {
    return vtss_ipv4_network_equal(&a, &b);
}

bool operator!=(const mesa_ipv4_network_t &a, const mesa_ipv4_network_t &b) {
    return !vtss_ipv4_network_equal(&a, &b);
}

bool operator<(const mesa_ipv4_uc_t &a, const mesa_ipv4_uc_t &b) {
    if (a.network != b.network) return a.network < b.network;
    return a.destination < b.destination;
}
bool operator!=(const mesa_ipv4_uc_t &a, const mesa_ipv4_uc_t &b) {
    if (a.network != b.network) return true;
    return a.destination != b.destination;
}
bool operator==(const mesa_ipv4_uc_t &a, const mesa_ipv4_uc_t &b) {
    if (a.network != b.network) return false;
    return a.destination == b.destination;
}

bool operator<(const mesa_mac_t &a, const mesa_mac_t &b) {
    return (a.addr[0] != b.addr[0] ? (a.addr[0] < b.addr[0]) :
            a.addr[1] != b.addr[1] ? (a.addr[1] < b.addr[1]) :
            a.addr[2] != b.addr[2] ? (a.addr[2] < b.addr[2]) :
            a.addr[3] != b.addr[3] ? (a.addr[3] < b.addr[3]) :
            a.addr[4] != b.addr[4] ? (a.addr[4] < b.addr[4]) :
            (a.addr[5] < b.addr[5]));
}

bool operator!=(const mesa_mac_t &a, const mesa_mac_t &b) {
    return (a.addr[0] != b.addr[0] ||
            a.addr[1] != b.addr[1] ||
            a.addr[2] != b.addr[2] ||
            a.addr[3] != b.addr[3] ||
            a.addr[4] != b.addr[4] ||
            a.addr[5] != b.addr[5]);
}

bool operator==(const mesa_mac_t &a, const mesa_mac_t &b) {
    return (a.addr[0] == b.addr[0] &&
            a.addr[1] == b.addr[1] &&
            a.addr[2] == b.addr[2] &&
            a.addr[3] == b.addr[3] &&
            a.addr[4] == b.addr[4] &&
            a.addr[5] == b.addr[5]);
}

bool operator<(const vtss_appl_ip_ipv4_acd_status_key_t &a, const vtss_appl_ip_ipv4_acd_status_key_t &b) {
    return (a.sip != b.sip ? (a.sip < b.sip) : (a.smac < b.smac));
}

bool operator!=(const vtss_appl_ip_ipv4_acd_status_key_t &a, const vtss_appl_ip_ipv4_acd_status_key_t &b) {
    return (a.sip != b.sip || a.smac != b.smac);
}

bool operator==(const vtss_appl_ip_ipv4_acd_status_key_t &a, const vtss_appl_ip_ipv4_acd_status_key_t &b) {
    return (a.sip == b.sip && a.smac == b.smac);
}

vtss::ostream &operator<<(vtss::ostream &o, const vtss_appl_ip_ipv4_acd_status_key_t &k) {
    o << "{sip: " << vtss::Ipv4Address(k.sip) << ", smac: " << k.smac << "}";
    return o;
}

bool operator<(const vtss_appl_ip_ipv4_acd_status_t &a, const vtss_appl_ip_ipv4_acd_status_t &b) {
    return (a.interface != b.interface ? (a.interface < b.interface) : (a.interface_port < b.interface_port));
}

bool operator!=(const vtss_appl_ip_ipv4_acd_status_t &a, const vtss_appl_ip_ipv4_acd_status_t &b) {
    return (a.interface != b.interface || a.interface_port != b.interface_port);
}

bool operator==(const vtss_appl_ip_ipv4_acd_status_t &a, const vtss_appl_ip_ipv4_acd_status_t &b) {
    return (a.interface == b.interface && a.interface_port == b.interface_port);
}

vtss::ostream &operator<<(vtss::ostream &o, const vtss_appl_ip_ipv4_acd_status_t &s) {
    vtss_ifindex_elm_t e;

    o << "{";
    if (vtss_ifindex_decompose(s.interface, &e) == VTSS_RC_OK &&
        e.iftype == VTSS_IFINDEX_TYPE_VLAN) {
        o << "vid: " << e.ordinal << ", ";
    }
    if (vtss_ifindex_decompose(s.interface_port, &e) == VTSS_RC_OK &&
        e.iftype == VTSS_IFINDEX_TYPE_PORT) {
        o << "iport: " << e.ordinal;
    }
    o << "}";
    return o;
}

bool operator<(const mesa_ipv6_network_t &a, const mesa_ipv6_network_t &b) {
    if (a.address != b.address) return a.address < b.address;
    return a.prefix_size < b.prefix_size;
}
bool operator!=(const mesa_ipv6_network_t &a, const mesa_ipv6_network_t &b) {
    return !vtss_ipv6_network_equal(&a, &b);
}
bool operator==(const mesa_ipv6_network_t &a, const mesa_ipv6_network_t &b) {
    return vtss_ipv6_network_equal(&a, &b);
}

bool operator<(const mesa_ipv6_uc_t &a, const mesa_ipv6_uc_t &b) {
    if (a.network != b.network) return a.network < b.network;
    return a.destination < b.destination;
}
bool operator!=(const mesa_ipv6_uc_t &a, const mesa_ipv6_uc_t &b) {
    if (a.network != b.network) return true;
    return a.destination != b.destination;
}
bool operator==(const mesa_ipv6_uc_t &a, const mesa_ipv6_uc_t &b) {
    if (a.network != b.network) return false;
    return a.destination == b.destination;
}

bool operator<(const vtss_appl_ip_ipv6_route_conf_t &a,
               const vtss_appl_ip_ipv6_route_conf_t &b) {
    if (a.route != b.route) return a.route < b.route;
    return a.interface < b.interface;
}

bool operator!=(const vtss_appl_ip_ipv6_route_conf_t &a,
                const vtss_appl_ip_ipv6_route_conf_t &b) {
    if (a.route != b.route) return true;
    return a.interface != b.interface;
}

bool operator==(const vtss_appl_ip_ipv6_route_conf_t &a,
                const vtss_appl_ip_ipv6_route_conf_t &b) {
    if (a.route != b.route) return false;
    return a.interface == b.interface;
}

bool operator!=(const vtss_appl_ip_if_ipv6_info_t &a,
                const vtss_appl_ip_if_ipv6_info_t &b) {
    // Only public part of this struct should be included in the compairson
    unsigned a_flag = a.flags & (VTSS_APPL_IP_IF_IPV6_FLAG_TENTATIVE |
                                 VTSS_APPL_IP_IF_IPV6_FLAG_DUPLICATED |
                                 VTSS_APPL_IP_IF_IPV6_FLAG_DETACHED |
                                 VTSS_APPL_IP_IF_IPV6_FLAG_NODAD |
                                 VTSS_APPL_IP_IF_IPV6_FLAG_AUTOCONF);
    unsigned b_flag = b.flags & (VTSS_APPL_IP_IF_IPV6_FLAG_TENTATIVE |
                                 VTSS_APPL_IP_IF_IPV6_FLAG_DUPLICATED |
                                 VTSS_APPL_IP_IF_IPV6_FLAG_DETACHED |
                                 VTSS_APPL_IP_IF_IPV6_FLAG_NODAD |
                                 VTSS_APPL_IP_IF_IPV6_FLAG_AUTOCONF);

    return a_flag != b_flag;
}

vtss_appl_ip_if_status_ip_stat_t operator-(
        const vtss_appl_ip_if_status_ip_stat_t &a,
        const vtss_appl_ip_if_status_ip_stat_t &b) {
    vtss_appl_ip_if_status_ip_stat_t res = {};

#define UPDATE(name) res.name = a.name - b.name; res.name##Valid = a.name##Valid
    UPDATE(InReceives);
    UPDATE(HCInReceives);
    UPDATE(InOctets);
    UPDATE(HCInOctets);
    UPDATE(InHdrErrors);
    UPDATE(InNoRoutes);
    UPDATE(InAddrErrors);
    UPDATE(InUnknownProtos);
    UPDATE(InTruncatedPkts);
    UPDATE(InForwDatagrams);
    UPDATE(HCInForwDatagrams);
    UPDATE(ReasmReqds);
    UPDATE(ReasmOKs);
    UPDATE(ReasmFails);
    UPDATE(InDiscards);
    UPDATE(InDelivers);
    UPDATE(HCInDelivers);
    UPDATE(OutRequests);
    UPDATE(HCOutRequests);
    UPDATE(OutNoRoutes);
    UPDATE(OutForwDatagrams);
    UPDATE(HCOutForwDatagrams);
    UPDATE(OutDiscards);
    UPDATE(OutFragReqds);
    UPDATE(OutFragOKs);
    UPDATE(OutFragFails);
    UPDATE(OutFragCreates);
    UPDATE(OutTransmits);
    UPDATE(HCOutTransmits);
    UPDATE(OutOctets);
    UPDATE(HCOutOctets);
    UPDATE(InMcastPkts);
    UPDATE(HCInMcastPkts);
    UPDATE(InMcastOctets);
    UPDATE(HCInMcastOctets);
    UPDATE(OutMcastPkts);
    UPDATE(HCOutMcastPkts);
    UPDATE(OutMcastOctets);
    UPDATE(HCOutMcastOctets);
    UPDATE(InBcastPkts);
    UPDATE(HCInBcastPkts);
    UPDATE(OutBcastPkts);
    UPDATE(HCOutBcastPkts);

    res.DiscontinuityTime = a.DiscontinuityTime;
    res.RefreshRate = b.RefreshRate;
#undef UPDATE

    return res;
}

vtss_appl_ip_if_status_link_stat_t operator-(
        const vtss_appl_ip_if_status_link_stat_t &a,
        const vtss_appl_ip_if_status_link_stat_t &b) {
    vtss_appl_ip_if_status_link_stat_t res = {};

#define UPDATE(name) res.name = a.name - b.name
    UPDATE(in_packets);
    UPDATE(out_packets);
    UPDATE(in_bytes);
    UPDATE(out_bytes);
    UPDATE(in_multicasts);
    UPDATE(out_multicasts);
    UPDATE(in_broadcasts);
    UPDATE(out_broadcasts);
#undef UPDATE

    return res;
}
