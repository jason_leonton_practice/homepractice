/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _IP_CHIP_API_H_
#define _IP_CHIP_API_H_

#include "ip_types.h"
#include "main_types.h"

class IpChip {

  public:

    IpChip() {}
    virtual ~IpChip() {}

    virtual mesa_rc init() =0;
    virtual mesa_rc start() =0;
    virtual mesa_rc master_up(const mesa_mac_t *const mac) =0;
    virtual mesa_rc master_down(void) =0;
    virtual mesa_rc switch_add(vtss_isid_t isid) =0;
    virtual mesa_rc switch_del(vtss_isid_t isid) =0;

    virtual mesa_rc routing_enable(BOOL enable) =0;

    virtual mesa_rc rleg_add(const mesa_vid_t vlan) =0;
    virtual mesa_rc rleg_del(const mesa_vid_t vlan) =0;

    virtual mesa_rc mac_subscribe(const mesa_vid_t vlan,
                                  const mesa_mac_t *const mac) =0;
    virtual mesa_rc mac_unsubscribe(const mesa_vid_t vlan,
                                    const mesa_mac_t *const mac) =0;

    virtual mesa_rc route_add(const mesa_routing_entry_t *const rt) =0;
    virtual mesa_rc route_del(const mesa_routing_entry_t *const rt) =0;

    virtual mesa_rc route_bulk_add(uint32_t cnt,
                                   const mesa_routing_entry_t *const rt,
                                   uint32_t *cnt_out) =0;

    virtual mesa_rc route_bulk_del(uint32_t cnt,
                                   const mesa_routing_entry_t *const rt,
                                   uint32_t *cnt_out) =0;

    virtual mesa_rc neighbour_add(const vtss_neighbour_t *const nb) =0;
    virtual mesa_rc neighbour_del(const vtss_neighbour_t *const nb) =0;

    /* Statistics Section: RFC-4293 */
    virtual mesa_rc counters_system_get(mesa_l3_counters_t *c) =0;

    virtual mesa_rc counters_vlan_get(const mesa_vid_t vlan,
                                      mesa_l3_counters_t *counters) =0;
    virtual mesa_rc counters_vlan_clear(const mesa_vid_t vlan) =0;

};

#define IPCHIP_LIST_OF_API_CALLS                  \
    X(init)                                       \
    X(start)                                      \
    X(master_up)                                  \
    X(master_down)                                \
    X(switch_add)                                 \
    X(switch_del)                                 \
    X(routing_enable)                             \
    X(mac_subscribe)                              \
    X(mac_unsubscribe)                            \
    X(rleg_add)                                   \
    X(rleg_del)                                   \
    X(route_add)                                  \
    X(route_del)                                  \
    X(route_bulk_add)                             \
    X(route_bulk_del)                             \
    X(neighbour_add)                              \
    X(neighbour_del)                              \
    X(counters_system_get)                        \
    X(counters_vlan_get)                          \
    X(counters_vlan_clear)

extern class IpChip *IpChip;

class IpChip *getIpLpm(), *getIpMac();

#define IPCHIP_WRAP(name)                                       \
    template <typename ...Args>                                 \
    mesa_rc vtss_ip_chip_ ## name(Args&&... args) {             \
        return IpChip->name(vtss::forward<Args>(args)...);      \
    }

#define X(name) IPCHIP_WRAP(name)
    IPCHIP_LIST_OF_API_CALLS
#undef X

#endif /* _IP_CHIP_API_H_ */
