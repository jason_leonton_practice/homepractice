/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _IP_OS_API_H_
#define _IP_OS_API_H_

#include "ip_api.h"
#include "ip_priv.h"
#include "vtss/basics/vector.hxx"

void if_index_to_if_id(u32 _if_index, vtss_if_id_t  *ifid);
mesa_rc vtss_ip_os_if_index_to_vlan_if(u32 _if_index, vtss_if_id_vlan_t *vlan);
mesa_rc vtss_ip_os_vlan_to_if_index(mesa_vid_t vid, u32 *_if_index);

mesa_rc vtss_ip_os_init(void);

mesa_rc vtss_ip_os_global_param_set(const vtss_ip_global_param_priv_t *const param);

mesa_rc vtss_ip_os_if_add(mesa_vid_t               vid);

mesa_rc vtss_ip_os_if_set(mesa_vid_t               vid,
                          const vtss_if_param_t   *const if_params);

mesa_rc vtss_ip_os_if_ctl(mesa_vid_t               vid,
                          BOOL                     up);

mesa_rc vtss_ip_os_if_del(mesa_vid_t vid);

mesa_rc vtss_ip_os_if_status(vtss_appl_ip_if_status_type_t               type,
                             vtss_if_id_vlan_t                           id,
                             vtss::Vector<vtss_appl_ip_if_status_t>     &st);

mesa_rc vtss_ip_os_if_status_all(vtss_appl_ip_if_status_type_t           type,
                                 vtss::Vector<vtss_appl_ip_if_status_t> &st);

#if defined(VTSS_SW_OPTION_DHCP6_CLIENT)
mesa_rc vtss_ip_os_if_ifp_ra_flags(mesa_vid_t vid, u8 *const ra_flg);
mesa_rc vtss_ip_os_if_notify_dhcp6(i32                          ifidx,
                                   const mesa_ipv6_network_t    *const network,
                                   const i32                    *const prev_link,
                                   const i32                    *const curr_link,
                                   const u8                     *const mo_flag);
#endif /* defined(VTSS_SW_OPTION_DHCP6_CLIENT) */

mesa_rc vtss_ip_os_inject(mesa_vid_t vid,
                          u32                     length,
                          const u8                *const data);

mesa_rc vtss_ip_os_ipv4_add(mesa_vid_t  vid,
                            const mesa_ipv4_network_t *const network);

mesa_rc vtss_ip_os_ipv6_add(mesa_vid_t  vid,
                            const mesa_ipv6_network_t *const network,
                            const vtss_ip_ipv6_addr_info_t *const info);

mesa_rc vtss_ip_os_ipv4_del(mesa_vid_t vid,
                            const mesa_ipv4_network_t *const network);

mesa_rc vtss_ip_os_ipv6_del(mesa_vid_t vid,
                            const mesa_ipv6_network_t *const network);

mesa_rc vtss_ip_os_route_add(const mesa_routing_entry_t *const rt,
                             bool no_poll = false);
mesa_rc vtss_ip_os_route_del(const mesa_routing_entry_t *const rt,
                             bool no_poll = false);

mesa_rc vtss_ip_os_route_get(mesa_routing_entry_type_t type,
                             u32                       max,
                             vtss_routing_status_t     *rt,
                             u32                       *const cnt);

mesa_rc vtss_ip_os_nb_clear(      mesa_ip_type_t        type);

// TODO, delete this
mesa_rc vtss_ip_os_nb_status_get( mesa_ip_type_t        type,
                                  u32                   max,
                                  u32                  *cnt,
                                  vtss_neighbour_status_t *status);

mesa_rc vtss_ip_os_ipv6_neighbour_status_get_list(
    u32                                        max,
    u32                                       *cnt,
    vtss_appl_ip_ipv6_neighbour_status_pair_t *status);

mesa_rc vtss_ip_os_ipv4_neighbour_status_get_list(
    u32                                        max,
    u32                                       *cnt,
    vtss_appl_ip_ipv4_neighbour_status_pair_t *status);

/* Statistics Section: RFC-4293 */
mesa_rc vtss_ip_os_stat_syst_cntr_clear(mesa_ip_type_t version);
mesa_rc vtss_ip_os_stat_intf_cntr_clear(mesa_ip_type_t version, vtss_if_id_t *ifidx);
mesa_rc vtss_ip_os_stat_icmp_cntr_clear(mesa_ip_type_t version);
mesa_rc vtss_ip_os_stat_imsg_cntr_clear(mesa_ip_type_t version, u32 type);

mesa_rc vtss_ip_os_stat_ipoutnoroute_get(      mesa_ip_type_t          version,
                                               u32                     *val);

mesa_rc vtss_ip_os_stat_icmp_cntr_get(         mesa_ip_type_t          version,
                                               vtss_ips_icmp_stat_t    *entry);
mesa_rc vtss_ip_os_stat_icmp_cntr_get_first(   mesa_ip_type_t          version,
                                               vtss_ips_icmp_stat_t    *entry);
mesa_rc vtss_ip_os_stat_icmp_cntr_get_next(    mesa_ip_type_t          version,
                                               vtss_ips_icmp_stat_t    *entry);

mesa_rc vtss_ip_os_stat_imsg_cntr_get(         mesa_ip_type_t          version,
                                               u32                     type,
                                               vtss_ips_icmp_stat_t    *entry);
mesa_rc vtss_ip_os_stat_imsg_cntr_get_first(   mesa_ip_type_t          version,
                                               u32                     type,
                                               vtss_ips_icmp_stat_t    *entry);
mesa_rc vtss_ip_os_stat_imsg_cntr_get_next(    mesa_ip_type_t          version,
                                               u32                     type,
                                               vtss_ips_icmp_stat_t    *entry);

mesa_rc vtss_ip_os_ipv4_system_statistics_get(vtss_appl_ip_if_status_ip_stat_t *s);
mesa_rc vtss_ip_os_ipv6_system_statistics_get(vtss_appl_ip_if_status_ip_stat_t *s);
mesa_rc vtss_ip_os_if_vlan_statistics_get(mesa_vid_t vid, vtss_appl_ip_if_status_link_stat_t *const s);
mesa_rc vtss_ip_os_ipv6_vlan_statistics_get(mesa_vid_t vid, vtss_appl_ip_if_status_ip_stat_t *const s);


#endif /* _IP_OS_API_H_ */
