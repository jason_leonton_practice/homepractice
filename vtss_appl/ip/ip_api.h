/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _IP_API_H_
#define _IP_API_H_

#include "vtss/appl/ip.h"
#include "ip_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Feature config ---------------------------------------------------------- */

static __inline__ BOOL vtss_ip_hasipv6(void) __attribute__ ((const));
static __inline__ BOOL vtss_ip_hasipv6(void)
{
#if defined(VTSS_SW_OPTION_IPV6)
    return true;
#else
    return false;
#endif
}

static __inline__ bool vtss_ip_hasrouting(void) __attribute__ ((const));
static __inline__ bool vtss_ip_hasrouting(void)
{
#if defined(VTSS_SW_OPTION_L3RT)
    return true;
#else
    return false;
#endif
}

static __inline__ bool vtss_ip_hasdns(void) __attribute__ ((const));
static __inline__ bool vtss_ip_hasdns(void)
{
#if defined(VTSS_SW_OPTION_DNS)
    return true;
#else
    return false;
#endif
}

static __inline__ bool vtss_ip_hasdhcpv6(void) __attribute__ ((const));
static __inline__ bool vtss_ip_hasdhcpv6(void)
{
#if defined(VTSS_SW_OPTION_DHCP6_CLIENT)
    return true;
#else
    return false;
#endif
}

/* Global config ----------------------------------------------------------- */
mesa_rc vtss_ip_global_param_set_(  const vtss_ip_global_param_priv_t *const p);

mesa_rc vtss_ip_global_param_get_(  vtss_ip_global_param_priv_t     *p);


/* Interface functions ----------------------------------------------------- */
BOOL    vtss_ip_if_exists(          vtss_if_id_vlan_t            if_id);
/*
    This function is used to validate the input network (n) of an existing IP interface (if_id).
    It mainly checks network overlapping w.r.t the given interface's network in our system.
*/
BOOL    vtss_ip_if_address_valid(   vtss_if_id_vlan_t            if_id,
                                    const mesa_ip_network_t     *const n);

// Returns error when no more interfaces exists. To get first id, usr
// vid=VTSS_VID_NULL as current.
mesa_rc vtss_ip_if_id_next(         vtss_if_id_vlan_t              current,
                                    vtss_if_id_vlan_t             *const next);

/* Gatherer status related to a given interface */
mesa_rc vtss_ip_if_status_get(      vtss_appl_ip_if_status_type_t  type,
                                    vtss_if_id_vlan_t              id,
                                    const u32                      max,
                                    u32                           *cnt,
                                    vtss_appl_ip_if_status_t      *status);

/* Gets the first matched status entry for a given interface */
mesa_rc
vtss_appl_ip_if_status_get_first(vtss_appl_ip_if_status_type_t     type,
                                 vtss_ifindex_t                    ifidx,
                                 vtss_appl_ip_if_status_t         *status);

#if 0
/* Gets the exactly matched IPv4 status entry for a given index */
mesa_rc vtss_appl_ip_if_status_get_ipv4(const vtss_appl_ip_if_ipv4_status_key_t *const index,
                                        vtss_appl_ip_if_status_t                *const status);
#endif

/* Gets the exactly matched IPv6 status entry for a given index */
//mesa_rc vtss_appl_ip_if_status_get_ipv6(const vtss_appl_ip_if_ipv6_status_key_t *const index,
//                                        vtss_appl_ip_if_status_t                *const status);

/* Like vtss_ip_if_status_get, but for all interfaces */
mesa_rc vtss_ip_ifs_status_get(     vtss_appl_ip_if_status_type_t  type,
                                    const u32                      max,
                                    u32                           *cnt,
                                    vtss_appl_ip_if_status_t      *status);

/* Provide post-notifications for other modules when an interface has
 * changed */
typedef void (*vtss_ip_if_callback_t)(vtss_if_id_vlan_t         if_id);
mesa_rc vtss_ip_if_callback_add(    const vtss_ip_if_callback_t cb);
mesa_rc vtss_ip_if_callback_del(    const vtss_ip_if_callback_t cb);

/* Fill in default settings in configurations structs */
void vtss_if_default_param(         vtss_if_param_t             *param);

/* Neighbour functions */
mesa_rc vtss_appl_ip_neighbour_clear(mesa_ip_type_t type);

mesa_rc vtss_ip_nb_status_get(      mesa_ip_type_t               type,
                                    u32                          max,
                                    u32                         *cnt,
                                    vtss_neighbour_status_t     *status);

/* IP address functions ---------------------------------------------------- */

/* Check if an interface exists with the given IP address */
mesa_rc vtss_ip_ip_to_if(           const mesa_ip_addr_t        *const ip,
                                    vtss_appl_ip_if_status_t    *if_status);

/* Returns an appropriate source address to use for clients on a given VLAN */
mesa_rc vtss_ip_ip_by_vlan(         const mesa_vid_t             vlan,
                                    mesa_ip_type_t               type,
                                    mesa_ip_addr_t              *const src);

mesa_rc vtss_ip_ip_by_port(         const mesa_port_no_t         iport,
                                    mesa_ip_type_t               type,
                                    mesa_ip_addr_t              *const src);


/**
 * Return the next (of first) entry of the routing database.
 *
 * \param key (IN) - entry to use as input key. May be NULL, in which
 * case the first entry in the routing database is returned (if any).
 *
 * \param next (OUT) - the returned entry from the DB (see return
 * code).
 *
 * \note The routing entries are compared by: Type (IPv4/IPv6),
 * destination address, prefix length, next hop (gateway).
 *
 * \return VTSS_OK iff entry returned in 'next'.
 */
mesa_rc vtss_ip_route_getnext(      const mesa_routing_entry_t  *const key,
                                    mesa_routing_entry_t        *const next);

mesa_rc vtss_ip_route_conf_get(     const int                    max,
                                    mesa_routing_entry_t        *rt,
                                    int                         *const cnt);

mesa_rc vtss_ip_default_route_get(     const int                    max,
                                    mesa_routing_entry_t        *rt,
									int 						*const cnt);


/* Retrieve IP stack global information */
mesa_rc vtss_ip_ips_status_get(     vtss_ips_status_type_t       type,
                                    const u32                    max,
                                    u32                         *cnt,
                                    vtss_ips_status_t           *status);

/**
 * Clear system's ICMP statistics.
 *
 * \param version (IN) - Specify the IP version (IPv4/IPv6) to clear counters.
 * version MUST be either IPv4 or IPv6.
 * \note Only stacking master is allowed for this operation.
 *
 * \return VTSS_OK iff operation is done successfully.
 *
 */
mesa_rc vtss_ip_stat_icmp_cntr_clear(mesa_ip_type_t version);

/**
 * Return the counters of the matched ICMP message type.
 *
 * \param version (IN) - IP version used as input key.
 * \param icmp_msg (IN) - ICMP message type value used as input key.
 *  Zero is a valid index.
 *
 * \param entry (OUT) - the returned statistics from the IP stack.
 *
 * \note Only stacking master is allowed for this operation.
 *
 * \return VTSS_OK iff entry is found.
 *
 */
mesa_rc vtss_ip_stat_imsg_cntr_get( mesa_ip_type_t              version,
                                    u32                         icmp_msg,
                                    vtss_ips_icmp_stat_t        *entry);

mesa_rc vtss_ip_stat_imsg_cntr_getfirst(mesa_ip_type_t         version,
                                        u32                    icmp_msg,
                                        vtss_ips_icmp_stat_t   *entry);

mesa_rc vtss_ip_stat_imsg_cntr_getnext(mesa_ip_type_t          version,
                                       u32                     icmp_msg,
                                       vtss_ips_icmp_stat_t    *entry);
/**
 * Clear per ICMP type's statistics.
 *
 * \param version (IN) - Specify the IP version (IPv4/IPv6) to clear counters.
 * \param type (IN) - Specify the ICMP message type to clear counters.
 * version MUST be either IPv4 or IPv6; type MUST be valid ICMP message value (0 ~ 255).
 * \note Only stacking master is allowed for this operation.
 *
 * \return VTSS_OK iff operation is done successfully.
 *
 */
mesa_rc vtss_ip_stat_imsg_cntr_clear(mesa_ip_type_t version, u32 type);


typedef struct {
    uint32_t lt_valid;
    uint32_t lt_preferred;
} vtss_ip_ipv6_addr_info_t;

mesa_rc vtss_ip_ipv6_dhcp_add(mesa_vid_t vid,
                              const mesa_ipv6_network_t *const network,
                              const vtss_ip_ipv6_addr_info_t *const info);
mesa_rc vtss_ip_ipv6_dhcp_del(mesa_vid_t vid,
                              const mesa_ipv6_network_t *const network);


/* CLI/ICLI helpers -------------------------------------------------------- */
void vtss_ip_cli_init(              void);

typedef int vtss_ip_cli_pr(         const char *fmt, ...);

int vtss_ip_if_print(               vtss_ip_cli_pr                 *pr,
                                    BOOL                            vlan_only,
                                    vtss_appl_ip_if_status_type_t   type);

int vtss_ip_if_brief_print(mesa_ip_type_t type, vtss_ip_cli_pr *pr);

int vtss_ip_route_print(            mesa_routing_entry_type_t       type,
                                    vtss_ip_cli_pr                 *pr);

int vtss_ip_nb_print(               mesa_ip_type_t                  type,
                                    vtss_ip_cli_pr                 *pr);

// Debug function for adding port interface
mesa_rc vtss_ip_os_if_add_port(uint32_t port);

/* Module initialization --------------------------------------------------- */
mesa_rc vtss_ip_init(               vtss_init_data_t               *data);

#if defined(VTSS_SW_OPTION_FRR)
#define IP_ROUTE_DEF_DISTANCE 1
mesa_rc vtss_ip_ipv4_route_users_get(
        const mesa_ipv4_uc_t  *const rt,
        int *const users);
#endif /* VTSS_SW_OPTION_FRR */

#ifdef __cplusplus
}
#endif

#endif /* _IP_API_H_ */
