/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include <limits.h>
#include <errno.h>
#include <unistd.h>
#include <sys/utsname.h>
#include <sys/syscall.h>
#ifndef __NR_finit_module
#error "__NR_finit_module is not defined!"
#endif

#include "main.h"
#include "ip_trace.h"
#include "vtss/basics/trace.hxx"

#define MODULE_NAME "vtss_if_mux"

#include <fcntl.h>    // Flags

extern "C" int delete_module(const char *name, int flags);

mesa_rc vtss_appl_ip_kernel_module_if_mux_unload() {
    int res = delete_module(MODULE_NAME, O_NONBLOCK);

    if (res == -1) {
        VTSS_TRACE(INFO) << "Failed to unloaded: " MODULE_NAME;
        return VTSS_RC_ERROR;
    }

    VTSS_TRACE(INFO) << "Kernel module unloaded: " MODULE_NAME;
    return VTSS_RC_OK;
}

mesa_rc vtss_appl_ip_kernel_module_if_mux_load() {
    int res = 0, fd;
    mesa_rc rc = VTSS_RC_OK;
    const char *path;
#if !defined(VTSS_IP_KERNEL_MODULE_PATH)
    struct utsname uts;
    char buf[PATH_MAX];
#endif

#if defined(VTSS_IP_KERNEL_MODULE_PATH)
    path = VTSS_IP_KERNEL_MODULE_PATH;
#else
    uname(&uts);
    snprintf(buf, sizeof(buf), "/lib/modules/%s/extra/%s.ko", uts.release, MODULE_NAME);
    path = buf;
#endif

    // Try to unload, we never know
    (void)vtss_appl_ip_kernel_module_if_mux_unload();

    fd = open(path, O_RDONLY|O_CLOEXEC);
    if (fd < 0) {
        VTSS_TRACE(INFO) << "Failed: open " << strerror(errno)
                         << " Path: " << path;
        rc = VTSS_RC_ERROR;
        goto EXIT;
    }

    // finit_module(int fd, const char *uargs, int flags)
    res = syscall(__NR_finit_module, fd, "", 0);
    if (res == -1) {
        VTSS_TRACE(ERROR) << "Failed: finit_module " << strerror(errno)
                          << " module: " << path;
        rc = VTSS_RC_ERROR;
        goto EXIT_CLOSE;
    }

    VTSS_TRACE(INFO) << "Kernel module loaded: " << path;

EXIT_CLOSE:
    close(fd);

EXIT:
    return rc;
}

