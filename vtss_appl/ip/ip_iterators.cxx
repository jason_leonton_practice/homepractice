/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#include "ip_os_api.h"
#include "ip_iterators.h"
#include "vlan_api.h"

#define VTSS_TRACE_MODULE_ID    VTSS_MODULE_ID_IP
#define VTSS_ALLOC_MODULE_ID    VTSS_MODULE_ID_IP

#define IP2_ITER_IPS_GET(a, b, c, d, x)                                 \
(((x) = vtss_ip_ips_status_get((a), (b), (c), (d))) == VTSS_OK)
#define IP2_ITER_IFS_GET(a, b, c, d, x)                                 \
(((x) = vtss_ip_ifs_status_get((a), (b), (c), (d))) == VTSS_OK)
#define IP2_ITER_IF_GET(a, b, c, d, e, x)                               \
(((x) = vtss_ip_if_status_get((a), (b), (c), (d), (e))) == VTSS_OK)
#define IP2_ITER_NBR_GET(a, b, c, d, x)                                 \
(((x) = vtss_ip_nb_status_get((a), (b), (c), (d))) == VTSS_OK)
#define IP2_ITER_IF_STATUS_UP(a)                                        \
  ((a)->type != VTSS_APPL_IP_IF_STATUS_TYPE_LINK) ? FALSE :             \
  ((a)->u.link.flags & VTSS_APPL_IP_IF_LINK_FLAG_UP)
#define IP2_ITER_IF_STATUS_RUN(a)                                       \
  ((a)->type != VTSS_APPL_IP_IF_STATUS_TYPE_LINK) ? FALSE :             \
  ((a)->u.link.flags & VTSS_APPL_IP_IF_LINK_FLAG_RUNNING)


static i32 _vtss_ip2_iter_intf_ifidx_cmp_func(void *elm1, void *elm2)
{
    ip_iter_intf_ifinf_t   *intf1, *intf2;

    if (!elm1 || !elm2) {
        T_W("IP2_ITER_ASSERT(NULL PTR %s%s%s)",
            elm1 ? "" : "ELM1",
            elm1 ? "" : (elm2 ? "" : "&"),
            elm2 ? "" : "ELM2");
        for (;;) {}
    }

    intf1 = (ip_iter_intf_ifinf_t *)elm1;
    intf2 = (ip_iter_intf_ifinf_t *)elm2;

    if (IP_ITER_INTF_IFINFO_VERSION(intf1) > IP_ITER_INTF_IFINFO_VERSION(intf2)) {
        return 1;
    } else if (IP_ITER_INTF_IFINFO_VERSION(intf1) < IP_ITER_INTF_IFINFO_VERSION(intf2)) {
        return -1;
    } else {
        if (IP_ITER_INTF_IFINFO_IFVDX(intf1) > IP_ITER_INTF_IFINFO_IFVDX(intf2)) {
            return 1;
        } else if (IP_ITER_INTF_IFINFO_IFVDX(intf1) < IP_ITER_INTF_IFINFO_IFVDX(intf2)) {
            return -1;
        } else {
            return 0;
        }
    }
}

static i32 _vtss_ip2_iter_intf_ifadr_cmp_func(void *elm1, void *elm2)
{
    ip_iter_intf_ifadr_t   *addr1, *addr2;

    if (!elm1 || !elm2) {
        T_W("IP2_ITER_ASSERT(NULL PTR %s%s%s)",
            elm1 ? "" : "ELM1",
            elm1 ? "" : (elm2 ? "" : "&"),
            elm2 ? "" : "ELM2");
        for (;;) {}
    }

    addr1 = (ip_iter_intf_ifadr_t *)elm1;
    addr2 = (ip_iter_intf_ifadr_t *)elm2;

    if (IP_ITER_INTF_IFADR_IPA_VERSION(addr1) > IP_ITER_INTF_IFADR_IPA_VERSION(addr2)) {
        return 1;
    } else if (IP_ITER_INTF_IFADR_IPA_VERSION(addr1) < IP_ITER_INTF_IFADR_IPA_VERSION(addr2)) {
        return -1;
    } else {
        int cmp = 0;

        switch ( IP_ITER_INTF_IFADR_IPA_VERSION(addr1) ) {
        case MESA_IP_TYPE_IPV4:
            if (addr1->ipa.addr.ipv4 > addr2->ipa.addr.ipv4) {
                cmp++;
            } else {
                if (addr1->ipa.addr.ipv4 < addr2->ipa.addr.ipv4) {
                    cmp--;
                }
            }
            break;
        case MESA_IP_TYPE_IPV6:
            cmp = memcmp(&addr1->ipa.addr.ipv6, &addr2->ipa.addr.ipv6, sizeof(mesa_ipv6_t));
            break;
        default:
            cmp = memcmp(&addr1->ipa, &addr2->ipa, sizeof(mesa_ip_addr_t));
            break;
        }

        if (cmp > 0) {
            return 1;
        } else if (cmp < 0) {
            return -1;
        } else {
            return 0;
        }
    }
}

static i32 _vtss_ip2_iter_intf_neighbor_cmp_func(void *elm1, void *elm2)
{
    ip_iter_intf_nbr_t     *nbr1, *nbr2;

    if (!elm1 || !elm2) {
        T_W("IP2_ITER_ASSERT(NULL PTR %s%s%s)",
            elm1 ? "" : "ELM1",
            elm1 ? "" : (elm2 ? "" : "&"),
            elm2 ? "" : "ELM2");
        for (;;) {}
    }

    nbr1 = (ip_iter_intf_nbr_t *)elm1;
    nbr2 = (ip_iter_intf_nbr_t *)elm2;

    if (IP_ITER_INTF_NBR_VERSION(nbr1) < IP_ITER_INTF_NBR_VERSION(nbr2)) {
        return 1;
    } else if (IP_ITER_INTF_NBR_VERSION(nbr1) < IP_ITER_INTF_NBR_VERSION(nbr2)) {
        return -1;
    } else if (IP_ITER_INTF_NBR_IFIDX(nbr1) > IP_ITER_INTF_NBR_IFIDX(nbr2)) {
        return 1;
    } else if (IP_ITER_INTF_NBR_IFIDX(nbr1) < IP_ITER_INTF_NBR_IFIDX(nbr2)) {
        return -1;
    } else {
        int cmp = 0;

        switch ( IP_ITER_INTF_NBR_VERSION(nbr1) ) {
        case MESA_IP_TYPE_IPV4:
            if (nbr1->nbr.addr.ipv4 > nbr2->nbr.addr.ipv4) {
                cmp++;
            } else {
                if (nbr1->nbr.addr.ipv4 < nbr2->nbr.addr.ipv4) {
                    cmp--;
                }
            }
            break;
        case MESA_IP_TYPE_IPV6:
            cmp = memcmp(&nbr1->nbr.addr.ipv6, &nbr2->nbr.addr.ipv6, sizeof(mesa_ipv6_t));
            break;
        default:
            cmp = memcmp(&nbr1->nbr, &nbr2->nbr, sizeof(mesa_ip_addr_t));
            break;
        }

        if (cmp > 0) {
            return 1;
        } else if (cmp < 0) {
            return -1;
        } else {
            return 0;
        }
    }
}

static BOOL _vtss_ip2_iter_intf_ifidx_prepare(  mesa_ip_type_t version,
                                                vtss_appl_ip_if_status_t *const ifs,
                                                ip_iter_intf_ifinf_t *const ifinf)
{
    mesa_rc                 rc;
    vtss_ip_global_param_priv_t ip_global;
    vtss_appl_ip_if_status_type_t   stype;
    vtss_appl_ip_if_status_t        *fdx;
    u32                     cnt;
    mesa_vid_t              ifid;

    if (!ifs || !ifinf ||
        (ifs->type != VTSS_APPL_IP_IF_STATUS_TYPE_LINK) ||
        (ifs->if_id.type != VTSS_ID_IF_TYPE_VLAN)) {
        return FALSE;
    }

    stype = VTSS_APPL_IP_IF_STATUS_TYPE_INVALID;
    switch ( version ) {
    case MESA_IP_TYPE_IPV4:
        stype = VTSS_APPL_IP_IF_STATUS_TYPE_IPV4;
        break;
    case MESA_IP_TYPE_IPV6:
        stype = VTSS_APPL_IP_IF_STATUS_TYPE_IPV6;
        break;
    default:
        return FALSE;
    }

    if ((VTSS_CALLOC_CAST(fdx, IP_ITER_SINGLE_IFS_OBJS, sizeof(vtss_appl_ip_if_status_t))) == NULL) {
        return FALSE;
    }

    ifid = ifs->if_id.u.vlan;
    IP_ITER_INTF_IFINFO_VERSION_SET(ifinf, version);
    IP_ITER_INTF_IFINFO_IFVDX_SET(ifinf, ifid);
    IP_ITER_INTF_IFINFO_IFINDEX_SET(ifinf, ifs->u.link.os_if_index);
    IP_ITER_INTF_IFINFO_MTU_SET(ifinf, ifs->u.link.mtu);

    cnt = 0;
    IP_ITER_INTF_IFINFO_MGMT_STATE_CLR(ifinf);
    if (IP2_ITER_IF_GET(stype, ifid, IP_ITER_SINGLE_IFS_OBJS, &cnt, fdx, rc)) {
        if (cnt) {
            IP_ITER_INTF_IFINFO_MGMT_STATE_SET(ifinf, IP_ITER_IFINFST_MGMT_ON);
        } else {
            IP_ITER_INTF_IFINFO_MGMT_STATE_SET(ifinf, IP_ITER_IFINFST_MGMT_OFF);
        }
        if (IP2_ITER_IF_STATUS_UP(ifs)) {
            IP_ITER_INTF_IFINFO_MGMT_STATE_SET(ifinf, IP_ITER_IFINFST_LINK_ON);
        } else {
            IP_ITER_INTF_IFINFO_MGMT_STATE_SET(ifinf, IP_ITER_IFINFST_LINK_OFF);
        }
    } else {
        IP_ITER_INTF_IFINFO_MGMT_STATE_SET(ifinf, IP_ITER_IFINFST_MGMT_OFF);
    }
    rc = vtss_ip_global_param_get_(&ip_global);
    if ((rc == VTSS_OK) && ip_global.enable_routing) {
        IP_ITER_INTF_IFINFO_FORWARDING_SET(ifinf, IP_ITER_DO_FORWARDING);
    } else {
        IP_ITER_INTF_IFINFO_FORWARDING_SET(ifinf, IP_ITER_DONOT_FORWARDING);
    }

    /* not yet support; hard coded */
    IP_ITER_INTF_IFINFO_LIFE_TIME_SET(ifinf, 30000);   /* REACHABLE_TIME: msec */
    /* not yet support; hard coded */
    IP_ITER_INTF_IFINFO_RXMT_TIME_SET(ifinf, 1000);    /* RETRANS_TIMER: msec */

    VTSS_FREE(fdx);
    return TRUE;
}

static void _vtss_ip2_iter_intf_ifidx_selection(BOOL *const hit,
                                                ip_iter_intf_ifinf_t *const ifx,
                                                ip_iter_intf_ifinf_t *const fnd,
                                                ip_iter_intf_ifinf_t *const res)
{
    if (hit && ifx && fnd && res) {
        if (_vtss_ip2_iter_intf_ifidx_cmp_func((void *)fnd, (void *)ifx) > 0) {
            if (*hit == FALSE) {
                memcpy(res, fnd, sizeof(ip_iter_intf_ifinf_t));
                *hit = TRUE;
            } else {
                if (_vtss_ip2_iter_intf_ifidx_cmp_func((void *)fnd, (void *)res) < 0) {
                    memcpy(res, fnd, sizeof(ip_iter_intf_ifinf_t));
                }
            }
        }
    }
}

/*
    Return first interface general information found in IP stack.

    \param version (IN) - version to use as input key.
    \param vidx (IN) - vlan index to use as input key.

    \param entry (OUT) - general information of the matched IPv4 or IPv6 interfrace.

    \return VTSS_OK iff entry is found.
 */
mesa_rc vtss_ip_intf_ifidx_iter_first( const mesa_ip_type_t        version,
                                       const vtss_if_id_vlan_t     vidx,
                                       ip_iter_intf_ifinf_t       *const entry)
{
    mesa_rc                 ret;
    BOOL                    hit;
    u32                     idx, cnt;
    vtss_appl_ip_if_status_t        *ifs, *fdx;
    ip_iter_intf_ifinf_t   *ifx, *fnd, *res;

    if (!entry) {
        return VTSS_RC_ERROR;
    }
    if ((VTSS_CALLOC_CAST(ifs, IP_ITER_MAX_IFS_OBJS, sizeof(vtss_appl_ip_if_status_t))) == NULL) {
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(ifx, sizeof(ip_iter_intf_ifinf_t))) == NULL) {
        VTSS_FREE(ifs);
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(fnd, sizeof(ip_iter_intf_ifinf_t))) == NULL) {
        VTSS_FREE(ifx);
        VTSS_FREE(ifs);
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(res, sizeof(ip_iter_intf_ifinf_t))) == NULL) {
        VTSS_FREE(fnd);
        VTSS_FREE(ifx);
        VTSS_FREE(ifs);
        return IP_ERROR_NOSPACE;
    }

    fdx = ifs;
    hit = FALSE;
    memset(res, 0x0, sizeof(ip_iter_intf_ifinf_t));
    if (IP2_ITER_IFS_GET(VTSS_APPL_IP_IF_STATUS_TYPE_LINK, IP_ITER_MAX_IFS_OBJS, &cnt, ifs, ret)) {
        memset(ifx, 0x0, sizeof(ip_iter_intf_ifinf_t));
        for (idx = 0; idx < cnt; ++idx, ++ifs) {
            if (_vtss_ip2_iter_intf_ifidx_prepare(MESA_IP_TYPE_IPV4, ifs, fnd)) {
                _vtss_ip2_iter_intf_ifidx_selection(&hit, ifx, fnd, res);
            }
            if (_vtss_ip2_iter_intf_ifidx_prepare(MESA_IP_TYPE_IPV6, ifs, fnd)) {
                _vtss_ip2_iter_intf_ifidx_selection(&hit, ifx, fnd, res);
            }
        }
    }

    if (hit && (ret == VTSS_OK)) {
        memcpy(entry, res, sizeof(ip_iter_intf_ifinf_t));
    } else {
        if (ret == VTSS_OK) {
            ret = IP_ERROR_NOTFOUND;
        }
    }

    VTSS_FREE(res);
    VTSS_FREE(fnd);
    VTSS_FREE(ifx);
    VTSS_FREE(fdx);

    return ret;
}

/*
    Return specific interface general information found in IP stack.

    \param version (IN) - version to use as input key.
    \param vidx (IN) - vlan index to use as input key.

    \param entry (OUT) - general information of the matched IPv4 or IPv6 interfrace.

    \return VTSS_OK iff entry is found.
 */
mesa_rc vtss_ip_intf_ifidx_iter_get(   const mesa_ip_type_t        version,
                                       const vtss_if_id_vlan_t     vidx,
                                       ip_iter_intf_ifinf_t       *const entry)
{
    mesa_rc                 ret;
    vtss_appl_ip_if_status_t        *fdx;
    u32                     cnt;

    if (!entry) {
        return VTSS_RC_ERROR;
    }

    switch ( version ) {
    case MESA_IP_TYPE_IPV4:
    case MESA_IP_TYPE_IPV6:
        break;
    default:
        return VTSS_RC_ERROR;
    }

    if ((VTSS_CALLOC_CAST(fdx, IP_ITER_SINGLE_IFS_OBJS, sizeof(vtss_appl_ip_if_status_t))) == NULL) {
        return IP_ERROR_NOSPACE;
    }

    cnt = 0;
    if (IP2_ITER_IF_GET(VTSS_APPL_IP_IF_STATUS_TYPE_LINK, vidx, IP_ITER_SINGLE_IFS_OBJS, &cnt, fdx, ret) && cnt) {
        if (!_vtss_ip2_iter_intf_ifidx_prepare(version, fdx, entry)) {
            ret = IP_ERROR_FAILED;
        }
    }

    VTSS_FREE(fdx);
    return ret;
}

/*
    Return next interface general information found in IP stack.

    \param version (IN) - version to use as input key.
    \param vidx (IN) - vlan index to use as input key.

    \param entry (OUT) - general information of the matched IPv4 or IPv6 interfrace.

    \return VTSS_OK iff entry is found.
 */
mesa_rc vtss_ip_intf_ifidx_iter_next(  const mesa_ip_type_t        version,
                                       const vtss_if_id_vlan_t     vidx,
                                       ip_iter_intf_ifinf_t       *const entry)
{
    mesa_rc                 ret;
    BOOL                    hit;
    u32                     idx, cnt;
    vtss_appl_ip_if_status_t        *ifs, *fdx;
    ip_iter_intf_ifinf_t   *ifx, *fnd, *res;

    if (!entry) {
        return VTSS_RC_ERROR;
    }
    if ((VTSS_CALLOC_CAST(ifs, IP_ITER_MAX_IFS_OBJS, sizeof(vtss_appl_ip_if_status_t))) == NULL) {
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(ifx, sizeof(ip_iter_intf_ifinf_t))) == NULL) {
        VTSS_FREE(ifs);
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(fnd, sizeof(ip_iter_intf_ifinf_t))) == NULL) {
        VTSS_FREE(ifx);
        VTSS_FREE(ifs);
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(res, sizeof(ip_iter_intf_ifinf_t))) == NULL) {
        VTSS_FREE(fnd);
        VTSS_FREE(ifx);
        VTSS_FREE(ifs);
        return IP_ERROR_NOSPACE;
    }

    fdx = ifs;
    hit = FALSE;
    memset(res, 0x0, sizeof(ip_iter_intf_ifinf_t));
    if (IP2_ITER_IFS_GET(VTSS_APPL_IP_IF_STATUS_TYPE_LINK, IP_ITER_MAX_IFS_OBJS, &cnt, ifs, ret)) {
        IP_ITER_INTF_IFINFO_VERSION_SET(ifx, version);
        IP_ITER_INTF_IFINFO_IFVDX_SET(ifx, vidx);

        for (idx = 0; idx < cnt; ++idx, ++ifs) {
            if (_vtss_ip2_iter_intf_ifidx_prepare(MESA_IP_TYPE_IPV4, ifs, fnd)) {
                _vtss_ip2_iter_intf_ifidx_selection(&hit, ifx, fnd, res);
            }
            if (_vtss_ip2_iter_intf_ifidx_prepare(MESA_IP_TYPE_IPV6, ifs, fnd)) {
                _vtss_ip2_iter_intf_ifidx_selection(&hit, ifx, fnd, res);
            }
        }
    }

    if (hit && (ret == VTSS_OK)) {
        memcpy(entry, res, sizeof(ip_iter_intf_ifinf_t));
    } else {
        if (ret == VTSS_OK) {
            ret = IP_ERROR_NOTFOUND;
        }
    }

    VTSS_FREE(res);
    VTSS_FREE(fnd);
    VTSS_FREE(ifx);
    VTSS_FREE(fdx);

    return ret;
}

static BOOL _vtss_ip2_iter_intf_ifadr_prepare(  mesa_ip_type_t version,
                                                vtss_appl_ip_if_status_t *const ifs,
                                                ip_iter_intf_ifadr_t *const ifadr)
{
    mesa_rc                 rc;
    u32                     cnt;
    mesa_vid_t              ifid;
    vtss_appl_ip_if_status_t        *fdx;
    vtss_appl_ip_if_status_ipv4_t   *ip4s;
    vtss_appl_ip_if_status_ipv6_t   *ip6s;

    if (!ifs || !ifadr ||
        (ifs->if_id.type != VTSS_ID_IF_TYPE_VLAN)) {
        return FALSE;
    }

    switch ( version ) {
    case MESA_IP_TYPE_IPV4:
        ip4s = &ifs->u.ipv4;
        ip6s = NULL;
        break;
    case MESA_IP_TYPE_IPV6:
        ip4s = NULL;
        ip6s = &ifs->u.ipv6;
        break;
    default:
        return FALSE;
    }

    if ((VTSS_CALLOC_CAST(fdx, IP_ITER_SINGLE_IFS_OBJS, sizeof(vtss_appl_ip_if_status_t))) == NULL) {
        return FALSE;
    }

    cnt = 0;
    ifid = ifs->if_id.u.vlan;
    if (IP2_ITER_IF_GET(VTSS_APPL_IP_IF_STATUS_TYPE_LINK, ifid, IP_ITER_SINGLE_IFS_OBJS, &cnt, fdx, rc) && cnt) {
        mesa_timestamp_t        ips_ts;
        ip_iter_intf_ifinf_t   *ifinf;
        vtss_ip_global_param_priv_t ip_global;

        ifinf = &ifadr->if_info;
        if (ip4s) {
            IP_ITER_INTF_IFADR_IPV4_ADDR_SET(ifadr, ip4s->net.address);
            IP_ITER_INTF_IFADR_TYPE_SET(ifadr, IP_IFADR_TYPE_UNICAST);
            IP_ITER_INTF_IFADR_STATUS_SET(ifadr, IP_IFADR_STATUS_PREFER);

            IP_ITER_INTF_IFINFO_MTU_SET(ifinf, ip4s->info.reasm_max_size);
            IP_ITER_INTF_IFINFO_RXMT_TIME_SET(ifinf, ip4s->info.arp_retransmit_time);
        }
        if (ip6s) {
            IP_ITER_INTF_IFADR_IPV6_ADDR_SET(ifadr, &ip6s->net.address);
            if (ip6s->info.flags & VTSS_APPL_IP_IF_IPV6_FLAG_ANYCAST) {
                IP_ITER_INTF_IFADR_TYPE_SET(ifadr, IP_IFADR_TYPE_ANYCAST);
            } else {
                IP_ITER_INTF_IFADR_TYPE_SET(ifadr, IP_IFADR_TYPE_UNICAST);
            }
            if (ip6s->info.flags & VTSS_APPL_IP_IF_IPV6_FLAG_TENTATIVE) {
                IP_ITER_INTF_IFADR_STATUS_SET(ifadr, IP_IFADR_STATUS_TENTATIVE);
            } else if (ip6s->info.flags & VTSS_APPL_IP_IF_IPV6_FLAG_DETACHED) {
                IP_ITER_INTF_IFADR_STATUS_SET(ifadr, IP_IFADR_STATUS_INACCESS);
            } else if (ip6s->info.flags & VTSS_APPL_IP_IF_IPV6_FLAG_DUPLICATED) {
                IP_ITER_INTF_IFADR_STATUS_SET(ifadr, IP_IFADR_STATUS_DUPLICATE);
            } else if (ip6s->info.flags & VTSS_APPL_IP_IF_IPV6_FLAG_DEPRECATED) {
                IP_ITER_INTF_IFADR_STATUS_SET(ifadr, IP_IFADR_STATUS_DEPRECATE);
            } else {
                if (ip6s->info.flags & VTSS_APPL_IP_IF_IPV6_FLAG_TEMPORARY) {
                    IP_ITER_INTF_IFADR_STATUS_SET(ifadr, IP_IFADR_STATUS_UNKNOWN);
                } else {
                    IP_ITER_INTF_IFADR_STATUS_SET(ifadr, IP_IFADR_STATUS_PREFER);
                }
            }

            IP_ITER_INTF_IFINFO_MTU_SET(ifinf, fdx->u.link.mtu);
            /* not yet support; hard coded */
            IP_ITER_INTF_IFINFO_RXMT_TIME_SET(ifinf, 1000);    /* RETRANS_TIMER: msec */
        }
        memset(&ips_ts, 0x0, sizeof(mesa_timestamp_t));
        IP_ITER_INTF_IFADR_CREATED_SET(ifadr, &ips_ts);
        IP_ITER_INTF_IFADR_LAST_CHANGE_SET(ifadr, &ips_ts);
        IP_ITER_INTF_IFADR_ROW_STATUS_SET(ifadr, IP_ITER_STATE_ENABLED);
        IP_ITER_INTF_IFADR_STORAGE_TYPE_SET(ifadr, IP_IFADR_STORAGE_VOLATILE);

        IP_ITER_INTF_IFINFO_VERSION_SET(ifinf, version);
        IP_ITER_INTF_IFINFO_IFVDX_SET(ifinf, ifid);
        IP_ITER_INTF_IFINFO_IFINDEX_SET(ifinf, fdx->u.link.os_if_index);
        IP_ITER_INTF_IFINFO_MGMT_STATE_CLR(ifinf);
        IP_ITER_INTF_IFINFO_MGMT_STATE_SET(ifinf, IP_ITER_IFINFST_MGMT_ON);
        if (IP2_ITER_IF_STATUS_UP(fdx)) {
            IP_ITER_INTF_IFINFO_MGMT_STATE_SET(ifinf, IP_ITER_IFINFST_LINK_ON);
        } else {
            IP_ITER_INTF_IFINFO_MGMT_STATE_SET(ifinf, IP_ITER_IFINFST_LINK_OFF);
        }
        if ((vtss_ip_global_param_get_(&ip_global) == VTSS_OK) &&
            ip_global.enable_routing) {
            IP_ITER_INTF_IFINFO_FORWARDING_SET(ifinf, IP_ITER_DO_FORWARDING);
        } else {
            IP_ITER_INTF_IFINFO_FORWARDING_SET(ifinf, IP_ITER_DONOT_FORWARDING);
        }
        /* not yet support; hard coded */
        IP_ITER_INTF_IFINFO_LIFE_TIME_SET(ifinf, 30000);       /* REACHABLE_TIME: msec */
    }

    VTSS_FREE(fdx);
    return (cnt && (rc == VTSS_OK));
}

static void _vtss_ip2_iter_intf_ifadr_selection(BOOL *const hit,
                                                ip_iter_intf_ifadr_t *const ifx,
                                                ip_iter_intf_ifadr_t *const fnd,
                                                ip_iter_intf_ifadr_t *const res)
{
    if (hit && ifx && fnd && res) {
        if (_vtss_ip2_iter_intf_ifadr_cmp_func((void *)fnd, (void *)ifx) > 0) {
            if (*hit == FALSE) {
                memcpy(res, fnd, sizeof(ip_iter_intf_ifadr_t));
                *hit = TRUE;
            } else {
                if (_vtss_ip2_iter_intf_ifadr_cmp_func((void *)fnd, (void *)res) < 0) {
                    memcpy(res, fnd, sizeof(ip_iter_intf_ifadr_t));
                }
            }
        }
    }
}

/*
    Return first interface address information found in IP stack.

    \param ifadr (IN) - version and address (defined in mesa_ip_addr_t) to use as input key.

    \param entry (OUT) - address information of the matched IPv4 or IPv6 interfrace.

    \return VTSS_OK iff entry is found.
 */
mesa_rc vtss_ip_intf_ifadr_iter_first( const mesa_ip_addr_t        *ifadr,
                                       ip_iter_intf_ifadr_t       *const entry)
{
    mesa_rc                 ret;
    BOOL                    hit;
    u32                     idx, cnt;
    vtss_appl_ip_if_status_t        *ifs, *fdx;
    ip_iter_intf_ifadr_t   *ifx, *fnd, *res;

    if (!entry) {
        return VTSS_RC_ERROR;
    }
    if ((VTSS_CALLOC_CAST(ifs, IP_ITER_MAX_IFS_OBJS, sizeof(vtss_appl_ip_if_status_t))) == NULL) {
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(ifx, sizeof(ip_iter_intf_ifadr_t))) == NULL) {
        VTSS_FREE(ifs);
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(fnd, sizeof(ip_iter_intf_ifadr_t))) == NULL) {
        VTSS_FREE(ifx);
        VTSS_FREE(ifs);
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(res, sizeof(ip_iter_intf_ifadr_t))) == NULL) {
        VTSS_FREE(fnd);
        VTSS_FREE(ifx);
        VTSS_FREE(ifs);
        return IP_ERROR_NOSPACE;
    }

    fdx = ifs;
    hit = FALSE;
    memset(res, 0x0, sizeof(ip_iter_intf_ifadr_t));
    if (IP2_ITER_IFS_GET(VTSS_APPL_IP_IF_STATUS_TYPE_IPV4, IP_ITER_MAX_IFS_OBJS, &cnt, ifs, ret)) {
        memset(ifx, 0x0, sizeof(ip_iter_intf_ifadr_t));

        for (idx = 0; idx < cnt; ++idx, ++ifs) {
            if (_vtss_ip2_iter_intf_ifadr_prepare(MESA_IP_TYPE_IPV4, ifs, fnd)) {
                _vtss_ip2_iter_intf_ifadr_selection(&hit, ifx, fnd, res);
            }
        }

        ifs = fdx;
        if (!hit && IP2_ITER_IFS_GET(VTSS_APPL_IP_IF_STATUS_TYPE_IPV6, IP_ITER_MAX_IFS_OBJS, &cnt, ifs, ret)) {
            for (idx = 0; idx < cnt; ++idx, ++ifs) {
                if (_vtss_ip2_iter_intf_ifadr_prepare(MESA_IP_TYPE_IPV6, ifs, fnd)) {
                    _vtss_ip2_iter_intf_ifadr_selection(&hit, ifx, fnd, res);
                }
            }
        }
    }

    if (hit && (ret == VTSS_OK)) {
        memcpy(entry, res, sizeof(ip_iter_intf_ifadr_t));
    } else {
        if (ret == VTSS_OK) {
            ret = IP_ERROR_NOTFOUND;
        }
    }

    VTSS_FREE(res);
    VTSS_FREE(fnd);
    VTSS_FREE(ifx);
    VTSS_FREE(fdx);

    return ret;
}

/*
    Return specific interface address information found in IP stack.

    \param ifadr (IN) - version and address (defined in mesa_ip_addr_t) to use as input key.

    \param entry (OUT) - address information of the matched IPv4 or IPv6 interfrace.

    \return VTSS_OK iff entry is found.
 */
mesa_rc vtss_ip_intf_ifadr_iter_get(   const mesa_ip_addr_t        *ifadr,
                                       ip_iter_intf_ifadr_t       *const entry)
{
    mesa_rc                 ret;
    BOOL                    hit;
    u32                     idx, cnt;
    vtss_appl_ip_if_status_type_t   stype;
    vtss_appl_ip_if_status_t        *ifs, *fdx;
    ip_iter_intf_ifadr_t   *ifx;

    if (!ifadr || !entry) {
        return VTSS_RC_ERROR;
    }

    stype = VTSS_APPL_IP_IF_STATUS_TYPE_INVALID;
    switch ( ifadr->type ) {
    case MESA_IP_TYPE_IPV4:
        stype = VTSS_APPL_IP_IF_STATUS_TYPE_IPV4;
        break;
    case MESA_IP_TYPE_IPV6:
        stype = VTSS_APPL_IP_IF_STATUS_TYPE_IPV6;
        break;
    default:
        return VTSS_RC_ERROR;
    }

    if ((VTSS_CALLOC_CAST(ifs, IP_ITER_MAX_IFS_OBJS, sizeof(vtss_appl_ip_if_status_t))) == NULL) {
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(ifx, sizeof(ip_iter_intf_ifadr_t))) == NULL) {
        VTSS_FREE(ifs);
        return IP_ERROR_NOSPACE;
    }

    fdx = ifs;
    hit = FALSE;
    memset(ifx, 0x0, sizeof(ip_iter_intf_ifadr_t));
    if (IP2_ITER_IFS_GET(stype, IP_ITER_MAX_IFS_OBJS, &cnt, ifs, ret)) {
        for (idx = 0; idx < cnt; ++idx, ++ifs) {
            if (stype == VTSS_APPL_IP_IF_STATUS_TYPE_IPV4) {
                if (ifadr->addr.ipv4 == ifs->u.ipv4.net.address) {
                    hit = TRUE;
                }
            }
            if (stype == VTSS_APPL_IP_IF_STATUS_TYPE_IPV6) {
                if (!memcmp(&ifadr->addr.ipv6, &ifs->u.ipv6.net.address, sizeof(mesa_ipv6_t))) {
                    hit = TRUE;
                }
            }

            if (hit) {
                if (!_vtss_ip2_iter_intf_ifadr_prepare(ifadr->type, ifs, ifx)) {
                    hit = FALSE;
                }

                break;
            }
        }
    }

    if (hit && (ret == VTSS_OK)) {
        memcpy(entry, ifx, sizeof(ip_iter_intf_ifadr_t));
    } else {
        if (ret == VTSS_OK) {
            ret = IP_ERROR_NOTFOUND;
        }
    }

    VTSS_FREE(ifx);
    VTSS_FREE(fdx);

    return ret;
}

/*
    Return next interface address information found in IP stack.

    \param ifadr (IN) - version and address (defined in mesa_ip_addr_t) to use as input key.

    \param entry (OUT) - address information of the matched IPv4 or IPv6 interfrace.

    \return VTSS_OK iff entry is found.
 */
mesa_rc vtss_ip_intf_ifadr_iter_next(  const mesa_ip_addr_t        *ifadr,
                                       ip_iter_intf_ifadr_t       *const entry)
{
    mesa_rc                 ret;
    BOOL                    hit;
    u32                     idx, cnt;
    vtss_appl_ip_if_status_t        *ifs, *fdx;
    ip_iter_intf_ifadr_t   *ifx, *fnd, *res;

    if (!ifadr || !entry) {
        return VTSS_RC_ERROR;
    }
    if ((VTSS_CALLOC_CAST(ifs, IP_ITER_MAX_IFS_OBJS, sizeof(vtss_appl_ip_if_status_t))) == NULL) {
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(ifx, sizeof(ip_iter_intf_ifadr_t))) == NULL) {
        VTSS_FREE(ifs);
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(fnd, sizeof(ip_iter_intf_ifadr_t))) == NULL) {
        VTSS_FREE(ifx);
        VTSS_FREE(ifs);
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(res, sizeof(ip_iter_intf_ifadr_t))) == NULL) {
        VTSS_FREE(fnd);
        VTSS_FREE(ifx);
        VTSS_FREE(ifs);
        return IP_ERROR_NOSPACE;
    }

    fdx = ifs;
    hit = FALSE;
    memset(res, 0x0, sizeof(ip_iter_intf_ifadr_t));
    if (IP2_ITER_IFS_GET(VTSS_APPL_IP_IF_STATUS_TYPE_IPV4, IP_ITER_MAX_IFS_OBJS, &cnt, ifs, ret)) {
        memset(ifx, 0x0, sizeof(ip_iter_intf_ifadr_t));
        switch ( ifadr->type ) {
        case MESA_IP_TYPE_IPV4:
            IP_ITER_INTF_IFADR_IPV4_ADDR_SET(ifx, ifadr->addr.ipv4);
            break;
        case MESA_IP_TYPE_IPV6:
            IP_ITER_INTF_IFADR_IPV6_ADDR_SET(ifx, &ifadr->addr.ipv6);
            break;
        default:
            IP_ITER_INTF_IFADR_IPA_VERSION_SET(ifx, MESA_IP_TYPE_NONE);
            break;
        }

        for (idx = 0; idx < cnt; ++idx, ++ifs) {
            if (_vtss_ip2_iter_intf_ifadr_prepare(MESA_IP_TYPE_IPV4, ifs, fnd)) {
                _vtss_ip2_iter_intf_ifadr_selection(&hit, ifx, fnd, res);
            }
        }

        ifs = fdx;
        if (IP2_ITER_IFS_GET(VTSS_APPL_IP_IF_STATUS_TYPE_IPV6, IP_ITER_MAX_IFS_OBJS, &cnt, ifs, ret)) {
            for (idx = 0; idx < cnt; ++idx, ++ifs) {
                if (_vtss_ip2_iter_intf_ifadr_prepare(MESA_IP_TYPE_IPV6, ifs, fnd)) {
                    _vtss_ip2_iter_intf_ifadr_selection(&hit, ifx, fnd, res);
                }
            }
        }
    }

    if (hit && (ret == VTSS_OK)) {
        memcpy(entry, res, sizeof(ip_iter_intf_ifadr_t));
    } else {
        if (ret == VTSS_OK) {
            ret = IP_ERROR_NOTFOUND;
        }
    }

    VTSS_FREE(res);
    VTSS_FREE(fnd);
    VTSS_FREE(ifx);
    VTSS_FREE(fdx);

    return ret;
}

/* Statistics Section: RFC-4293 */
/*
    Return first statistics found in IP stack.

    \param version (IN) - version to use as input key.

    \param entry (OUT) - statistics of either IPv4 or IPv6.

    \return VTSS_RC_OK iff entry is found.
 */
mesa_rc vtss_ip_cntr_syst_stat_iter_first( const mesa_ip_type_t        *const version,
                                           vtss_ips_ip_stat_t          *const entry)
{
    mesa_ip_type_t  ver;

    if (!version || !entry) {
        return VTSS_RC_ERROR;
    }

    ver = MESA_IP_TYPE_NONE;

    return vtss_ip_cntr_syst_stat_iter_next(&ver, entry);
}

/*
    Return specific statistics found in IP stack.

    \param version (IN) - version to use as input key.

    \param entry (OUT) - statistics of either IPv4 or IPv6.

    \return VTSS_RC_OK iff entry is found.
 */
mesa_rc vtss_ip_cntr_syst_stat_iter_get(   const mesa_ip_type_t        *const version,
                                           vtss_ips_ip_stat_t          *const entry)
{
    vtss_appl_ip_if_status_ip_stat_t    get_data;
    mesa_rc                             ret;

    if (!version || !entry) {
        return VTSS_RC_ERROR;
    }

    if (*version == MESA_IP_TYPE_IPV4) {
        ret = vtss_appl_ip_system_statistics_ipv4_get(&get_data);
    } else if (*version == MESA_IP_TYPE_IPV6) {
        ret = vtss_appl_ip_system_statistics_ipv6_get(&get_data);
    } else {
        ret = IP_ERROR_NOTFOUND;
    }

    if (ret == VTSS_RC_OK) {
        entry->IPVersion = *version;
        entry->data = get_data;
    }

    return ret;
}

/*
    Return next statistics found in IP stack.

    \param version (IN) - version to use as input key.

    \param entry (OUT) - statistics of either IPv4 or IPv6.

    \return VTSS_RC_OK iff entry is found.
 */
mesa_rc vtss_ip_cntr_syst_stat_iter_next(  const mesa_ip_type_t        *const version,
                                           vtss_ips_ip_stat_t          *const entry)
{
    mesa_ip_type_t                      get_ver;
    vtss_appl_ip_if_status_ip_stat_t    get_data;
    mesa_rc                             ret;

    if (!version || !entry) {
        return VTSS_RC_ERROR;
    }

    get_ver = MESA_IP_TYPE_NONE;
    if (*version < MESA_IP_TYPE_IPV4) {
        if ((ret = vtss_appl_ip_system_statistics_ipv4_get(&get_data)) != VTSS_RC_OK) {
            ret = vtss_appl_ip_system_statistics_ipv6_get(&get_data);
            get_ver = MESA_IP_TYPE_IPV6;
        } else {
            get_ver = MESA_IP_TYPE_IPV4;
        }
    } else if (*version < MESA_IP_TYPE_IPV6) {
        ret = vtss_appl_ip_system_statistics_ipv6_get(&get_data);
        get_ver = MESA_IP_TYPE_IPV6;
    } else {
        ret = IP_ERROR_NOTFOUND;
    }

    if (ret == VTSS_RC_OK) {
        entry->IPVersion = get_ver;
        entry->data = get_data;
    }

    return ret;
}

/*
    Return first interface statistics found in IP stack.

    \param version (IN) - version to use as input key.
    \param vidx (IN) - vlan index to use as input key.

    \param entry (OUT) - statistics of the matched IPv4 or IPv6 interfrace.

    \return VTSS_RC_OK iff entry is found.
 */
mesa_rc vtss_ip_cntr_intf_stat_iter_first( const mesa_ip_type_t        *const version,
                                           const vtss_if_id_vlan_t     *const vidx,
                                           vtss_if_status_ip_stat_t    *const entry)
{
    mesa_ip_type_t      ver;
    vtss_if_id_vlan_t   vid;

    if (!version || !vidx || !entry) {
        return VTSS_RC_ERROR;
    }

    vid = VTSS_VID_NULL;
    ver = MESA_IP_TYPE_NONE;

    return vtss_ip_cntr_intf_stat_iter_next(&ver, &vid, entry);
}

/*
    Return specific interface statistics found in IP stack.

    \param version (IN) - version to use as input key.
    \param vidx (IN) - vlan index to use as input key.

    \param entry (OUT) - statistics of the matched IPv4 or IPv6 interfrace.

    \return VTSS_RC_OK iff entry is found.
 */
mesa_rc vtss_ip_cntr_intf_stat_iter_get(   const mesa_ip_type_t        *const version,
                                           const vtss_if_id_vlan_t     *const vidx,
                                           vtss_if_status_ip_stat_t    *const entry)
{
    vtss_ifindex_t                      idx;
    vtss_appl_ip_if_status_ip_stat_t    get_data;
    mesa_rc                             ret;

    if (!version || !vidx || !entry) {
        return VTSS_RC_ERROR;
    }

    if (vtss_ifindex_from_vlan(*vidx, &idx) != VTSS_RC_OK) {
        return VTSS_RC_ERROR;
    }

    if (*version == MESA_IP_TYPE_IPV4) {
        ret = vtss_appl_ip_if_statistics_ipv4(idx, &get_data);
    } else if (*version == MESA_IP_TYPE_IPV6) {
        ret = vtss_appl_ip_if_statistics_ipv6(idx, &get_data);
    } else {
        ret = IP_ERROR_NOTFOUND;
    }

    if (ret == VTSS_RC_OK) {
        entry->IPVersion = *version;
        entry->IfIndex.type = VTSS_ID_IF_TYPE_VLAN;
        entry->IfIndex.u.vlan = (mesa_vid_t)(*vidx);
        entry->data = get_data;
    }

    return ret;
}

/*
    Return next interface statistics found in IP stack.

    \param version (IN) - version to use as input key.
    \param vidx (IN) - vlan index to use as input key.

    \param entry (OUT) - statistics of the matched IPv4 or IPv6 interfrace.

    \return VTSS_RC_OK iff entry is found.
 */
mesa_rc vtss_ip_cntr_intf_stat_iter_next(  const mesa_ip_type_t        *const version,
                                           const vtss_if_id_vlan_t     *const vidx,
                                           vtss_if_status_ip_stat_t    *const entry)
{
    vtss_appl_ip_if_status_type_t       get_type;
    vtss_if_id_vlan_t                   get_vidx, nxt;
    vtss_ifindex_t                      idx;
    vtss_appl_ip_if_status_ip_stat_t    get_data;
    mesa_rc                             ret;

    if (!version || !vidx || !entry) {
        return VTSS_RC_ERROR;
    }

    get_type = VTSS_APPL_IP_IF_STATUS_TYPE_ANY;
    get_vidx = VTSS_VID_NULL;
    if (*version < MESA_IP_TYPE_IPV4) {
        get_type = VTSS_APPL_IP_IF_STATUS_TYPE_IPV4;
    } else {
        if (*version < MESA_IP_TYPE_IPV6) {
            get_type = VTSS_APPL_IP_IF_STATUS_TYPE_IPV4;
            if (*vidx <= VTSS_APPL_VLAN_ID_MAX) {
                get_vidx = *vidx;
            } else {
                get_type = VTSS_APPL_IP_IF_STATUS_TYPE_IPV6;
            }
        } else if (*version == MESA_IP_TYPE_IPV6) {
            get_type = VTSS_APPL_IP_IF_STATUS_TYPE_IPV6;
            if (*vidx <= VTSS_APPL_VLAN_ID_MAX) {
                get_vidx = *vidx;
            } else {
                return VTSS_RC_ERROR;
            }
        } else {
            return VTSS_RC_ERROR;
        }
    }

    ret = IP_ERROR_NOTFOUND;
    while (ret != VTSS_RC_OK) {
        if (vtss_ip_if_id_next(get_vidx, &nxt) == VTSS_RC_OK) {
            get_vidx = nxt;

            if (vtss_ifindex_from_vlan(nxt, &idx) != VTSS_RC_OK) {
                continue;
            }

            if (get_type == VTSS_APPL_IP_IF_STATUS_TYPE_IPV4) {
                ret = vtss_appl_ip_if_statistics_ipv4(idx, &get_data);
            } else if (get_type == VTSS_APPL_IP_IF_STATUS_TYPE_IPV6) {
                ret = vtss_appl_ip_if_statistics_ipv6(idx, &get_data);
            } else {
                break;
            }

            if (ret == VTSS_RC_OK) {
                entry->IPVersion = (get_type == VTSS_APPL_IP_IF_STATUS_TYPE_IPV4 ? MESA_IP_TYPE_IPV4 : MESA_IP_TYPE_IPV6);
                entry->IfIndex.type = VTSS_ID_IF_TYPE_VLAN;
                entry->IfIndex.u.vlan = (mesa_vid_t)nxt;
                entry->data = get_data;
            }
        } else {
            if (get_type == VTSS_APPL_IP_IF_STATUS_TYPE_IPV4) {
                get_type = VTSS_APPL_IP_IF_STATUS_TYPE_IPV6;
                get_vidx = VTSS_VID_NULL;
            } else {
                break;
            }
        }
    }

    return ret;
}

/*
    Return first ICMP statistics found in IP stack.

    \param version (IN) - version to use as input key.

    \param entry (OUT) - statistics of either ICMP4 or ICMP6.

    \return VTSS_OK iff entry is found.
 */
mesa_rc vtss_ip_cntr_icmp_ver_iter_first(  const mesa_ip_type_t        *version,
                                           vtss_ips_icmp_stat_t        *const entry)
{
#if 0
    mesa_rc             ret;
    vtss_ips_status_t   *fdx, all_ips[IP_ITER_MAX_IPS_OBJS];
    u32                 cnt_ips;

    if (!version || !entry) {
        return VTSS_RC_ERROR;
    }

    memset(all_ips, 0x0, sizeof(all_ips));
    if (IP2_ITER_IPS_GET(VTSS_IPS_STATUS_TYPE_ANY, IP_ITER_MAX_IPS_OBJS, &cnt_ips, all_ips, ret)) {
        u32 idx;

        ret = IP_ERROR_NOTFOUND;
        for (idx = 0; idx < cnt_ips; idx++) {
            fdx = &all_ips[idx];

            if ((fdx->type == VTSS_IPS_STATUS_TYPE_STAT_ICMP4) ||
                (fdx->type == VTSS_IPS_STATUS_TYPE_STAT_ICMP6)) {
                memcpy(entry, &fdx->u.icmp_stat, sizeof(vtss_ips_icmp_stat_t));
                ret = VTSS_OK;
                break;
            }
        }
    }

    return ret;
#endif
    return VTSS_RC_ERROR;
}

/*
    Return specific ICMP statistics found in IP stack.

    \param version (IN) - version to use as input key.

    \param entry (OUT) - statistics of either ICMP4 or ICMP6.

    \return VTSS_OK iff entry is found.
 */
mesa_rc vtss_ip_cntr_icmp_ver_iter_get(    const mesa_ip_type_t        *version,
                                           vtss_ips_icmp_stat_t        *const entry)
{
#if 0
    mesa_rc                 ret;
    vtss_ips_status_t       *fdx, all_ips[IP_ITER_SINGLE_IPS_OBJS];
    vtss_ips_status_type_t  get_type;
    u32                     cnt_ips;

    if (!version || !entry) {
        return VTSS_RC_ERROR;
    }

    get_type = VTSS_IPS_STATUS_TYPE_ANY;
    if (*version == MESA_IP_TYPE_IPV4) {
        get_type = VTSS_IPS_STATUS_TYPE_STAT_ICMP4;
    } else if (*version == MESA_IP_TYPE_IPV6) {
        get_type = VTSS_IPS_STATUS_TYPE_STAT_ICMP6;
    } else {
        return VTSS_RC_ERROR;
    }

    memset(all_ips, 0x0, sizeof(all_ips));
    if (IP2_ITER_IPS_GET(get_type, IP_ITER_SINGLE_IPS_OBJS, &cnt_ips, all_ips, ret)) {
        u32 idx;

        ret = IP_ERROR_NOTFOUND;
        for (idx = 0; idx < cnt_ips; idx++) {
            fdx = &all_ips[idx];

            if ((fdx->type == VTSS_IPS_STATUS_TYPE_STAT_ICMP4) &&
                (*version == MESA_IP_TYPE_IPV4)) {
                memcpy(entry, &fdx->u.icmp_stat, sizeof(vtss_ips_icmp_stat_t));
                ret = VTSS_OK;
                break;
            }
            if ((fdx->type == VTSS_IPS_STATUS_TYPE_STAT_ICMP6) &&
                (*version == MESA_IP_TYPE_IPV6)) {
                memcpy(entry, &fdx->u.icmp_stat, sizeof(vtss_ips_icmp_stat_t));
                ret = VTSS_OK;
                break;
            }
        }
    }

    return ret;
#endif
    return VTSS_RC_ERROR;
}

/*
    Return next ICMP statistics found in IP stack.

    \param version (IN) - version to use as input key.

    \param entry (OUT) - statistics of either ICMP4 or ICMP6.

    \return VTSS_OK iff entry is found.
 */
mesa_rc vtss_ip_cntr_icmp_ver_iter_next(   const mesa_ip_type_t        *version,
                                           vtss_ips_icmp_stat_t        *const entry)
{
#if 0
    mesa_rc                 ret;
    vtss_ips_status_t       *fdx, all_ips[IP_ITER_SINGLE_IPS_OBJS];
    vtss_ips_status_type_t  get_type;
    u32                     cnt_ips;

    if (!version || !entry) {
        return VTSS_RC_ERROR;
    }

    get_type = VTSS_IPS_STATUS_TYPE_ANY;
    if (*version < MESA_IP_TYPE_IPV4) {
        get_type = VTSS_IPS_STATUS_TYPE_STAT_ICMP4;
    } else {
        if (*version < MESA_IP_TYPE_IPV6) {
            get_type = VTSS_IPS_STATUS_TYPE_STAT_ICMP6;
        } else {
            return VTSS_RC_ERROR;
        }
    }

    memset(all_ips, 0x0, sizeof(all_ips));
    if (IP2_ITER_IPS_GET(get_type, IP_ITER_SINGLE_IPS_OBJS, &cnt_ips, all_ips, ret)) {
        u32 idx;

        ret = IP_ERROR_NOTFOUND;
        for (idx = 0; idx < cnt_ips; idx++) {
            fdx = &all_ips[idx];

            if ((fdx->type == VTSS_IPS_STATUS_TYPE_STAT_ICMP4) ||
                (fdx->type == VTSS_IPS_STATUS_TYPE_STAT_ICMP6)) {
                memcpy(entry, &fdx->u.icmp_stat, sizeof(vtss_ips_icmp_stat_t));
                ret = VTSS_OK;
                break;
            }
        }
    }

    return ret;
#endif
    return VTSS_RC_ERROR;
}

/*
    Return first ICMP MSG statistics found in IP stack.

    \param version (IN) - version to use as input key.
    \param message (IN) - message type to use as input key.

    \param entry (OUT) - statistics of matched ICMP4 or ICMP6 message.

    \return VTSS_OK iff entry is found.
 */
mesa_rc vtss_ip_cntr_icmp_msg_iter_first(  const mesa_ip_type_t        *version,
                                           const u32                   *message,
                                           vtss_ips_icmp_stat_t        *const entry)
{
#if 0
    if (!version || !message || !entry) {
        return VTSS_RC_ERROR;
    }

    return vtss_ip_stat_imsg_cntr_getfirst(MESA_IP_TYPE_NONE, IP_STAT_IMSG_MAX, entry);
#else
    return VTSS_RC_ERROR;
#endif
}

/*
    Return specific ICMP MSG statistics found in IP stack.

    \param version (IN) - version to use as input key.
    \param message (IN) - message type to use as input key.

    \param entry (OUT) - statistics of matched ICMP4 or ICMP6 message.

    \return VTSS_OK iff entry is found.
 */
mesa_rc vtss_ip_cntr_icmp_msg_iter_get(    const mesa_ip_type_t        *version,
                                           const u32                   *message,
                                           vtss_ips_icmp_stat_t        *const entry)
{
#if 0
    if (!version || !message || !entry) {
        return VTSS_RC_ERROR;
    }

    return vtss_ip_stat_imsg_cntr_get(*version, *message, entry);
#else
    return VTSS_RC_ERROR;
#endif
}

/*
    Return next ICMP MSG statistics found in IP stack.

    \param version (IN) - version to use as input key.
    \param message (IN) - message type to use as input key.

    \param entry (OUT) - statistics of matched ICMP4 or ICMP6 message.

    \return VTSS_OK iff entry is found.
 */
mesa_rc vtss_ip_cntr_icmp_msg_iter_next(   const mesa_ip_type_t        *version,
                                           const u32                   *message,
                                           vtss_ips_icmp_stat_t        *const entry)
{
#if 0
    mesa_ip_type_t  get_vers;
    u32             get_imsg;

    if (!version || !message || !entry) {
        return VTSS_RC_ERROR;
    }

    get_vers = MESA_IP_TYPE_NONE;
    get_imsg = IP_STAT_IMSG_MAX;
    if (*version < MESA_IP_TYPE_IPV4) {
        get_vers = MESA_IP_TYPE_IPV4;
        get_imsg = 0;

        return vtss_ip_stat_imsg_cntr_get(get_vers, get_imsg, entry);
    } else {
        get_vers = *version;
        if (*version < MESA_IP_TYPE_IPV6) {
            if (*message < IP_STAT_IMSG_MAX) {
                get_imsg = *message;
            } else {
                get_vers = MESA_IP_TYPE_IPV6;
                get_imsg = 0;

                return vtss_ip_stat_imsg_cntr_get(get_vers, get_imsg, entry);
            }
        } else if (*version == MESA_IP_TYPE_IPV6) {
            if (*message < IP_STAT_IMSG_MAX) {
                get_imsg = *message;
            } else {
                return VTSS_RC_ERROR;
            }
        } else {
            return VTSS_RC_ERROR;
        }
    }

    return vtss_ip_stat_imsg_cntr_getnext(get_vers, get_imsg, entry);
#else
    return VTSS_RC_ERROR;
#endif
}

static BOOL _vtss_ip2_iter_intf_nbr_prepare(    vtss_neighbour_status_t *const nbr,
                                                ip_iter_intf_nbr_t *const ifnbr)
{
    mesa_timestamp_t    nbr_ts;

    if (!nbr || !ifnbr) {
        return FALSE;
    }

    switch ( nbr->ip_address.type ) {
    case MESA_IP_TYPE_IPV4:
        IP_ITER_INTF_NBR_IPV4_ADDR_SET(ifnbr, nbr->ip_address.addr.ipv4);

        IP_ITER_INTF_NBR_STATE_SET(ifnbr, IP_IFNBR_STATE_REACHABLE);
        if (nbr->flags & VTSS_APPL_IP_NEIGHBOUR_FLAG_VALID) {
            if (nbr->flags & VTSS_APPL_IP_NEIGHBOUR_FLAG_PERMANENT) {
                IP_ITER_INTF_NBR_TYPE_SET(ifnbr, IP_IFNBR_TYPE_STATIC);
            } else {
                IP_ITER_INTF_NBR_TYPE_SET(ifnbr, IP_IFNBR_TYPE_OTHER);
            }
        } else {
            IP_ITER_INTF_NBR_TYPE_SET(ifnbr, IP_IFNBR_TYPE_INVALID);
            if (nbr->interface.type != VTSS_ID_IF_TYPE_OS_ONLY) {
                IP_ITER_INTF_NBR_STATE_SET(ifnbr, IP_IFNBR_STATE_INCOMPLETE);
            }
        }

        break;
    case MESA_IP_TYPE_IPV6:
        IP_ITER_INTF_NBR_IPV6_ADDR_SET(ifnbr, &nbr->ip_address.addr.ipv6);

        if (!strncmp(nbr->state, "NONE", 4)) {
            IP_ITER_INTF_NBR_STATE_SET(ifnbr, IP_IFNBR_STATE_UNKNOWN);
        } else if (!strncmp(nbr->state, "INCMP", 5)) {
            IP_ITER_INTF_NBR_STATE_SET(ifnbr, IP_IFNBR_STATE_INCOMPLETE);
        } else if (!strncmp(nbr->state, "REACH", 5)) {
            IP_ITER_INTF_NBR_STATE_SET(ifnbr, IP_IFNBR_STATE_REACHABLE);
        } else if (!strncmp(nbr->state, "STALE", 5)) {
            IP_ITER_INTF_NBR_STATE_SET(ifnbr, IP_IFNBR_STATE_STALE);
        } else if (!strncmp(nbr->state, "DELAY", 5)) {
            IP_ITER_INTF_NBR_STATE_SET(ifnbr, IP_IFNBR_STATE_DELAY);
        } else if (!strncmp(nbr->state, "PROBE", 5)) {
            IP_ITER_INTF_NBR_STATE_SET(ifnbr, IP_IFNBR_STATE_PROBE);
        } else {
            IP_ITER_INTF_NBR_STATE_SET(ifnbr, IP_IFNBR_STATE_INVALID);
        }

        if (nbr->flags & VTSS_APPL_IP_NEIGHBOUR_FLAG_VALID) {
            if (nbr->flags & VTSS_APPL_IP_NEIGHBOUR_FLAG_PERMANENT) {
                IP_ITER_INTF_NBR_TYPE_SET(ifnbr, IP_IFNBR_TYPE_STATIC);
            } else {
                IP_ITER_INTF_NBR_TYPE_SET(ifnbr, IP_IFNBR_TYPE_OTHER);
            }
        } else {
            IP_ITER_INTF_NBR_TYPE_SET(ifnbr, IP_IFNBR_TYPE_INVALID);
        }
        break;
    default:
        return FALSE;
    }

    memset(&nbr_ts, 0x0, sizeof(mesa_timestamp_t));
    IP_ITER_INTF_NBR_LAST_UPDATE_SET(ifnbr, &nbr_ts);
    IP_ITER_INTF_NBR_ROW_STATUS_SET(ifnbr, IP_ITER_STATE_ENABLED);
    IP_ITER_INTF_NBR_PHY_ADDR_SET(ifnbr, &nbr->mac_address);

    memset(ifnbr->if_name, 0x0, sizeof(ifnbr->if_name));
    if (nbr->interface.type != VTSS_ID_IF_TYPE_VLAN) {
        if (nbr->interface.type != VTSS_ID_IF_TYPE_OS_ONLY) {
            strcpy((char *)ifnbr->if_name, "invalid");
        } else {
            strcpy((char *)ifnbr->if_name, nbr->interface.u.os.name);
        }

        IP_ITER_INTF_NBR_IFIDX_SET(ifnbr, VTSS_VID_NULL);
    } else {
        IP_ITER_INTF_NBR_IFIDX_SET(ifnbr, nbr->interface.u.vlan);
    }

    return TRUE;
}

static void _vtss_ip2_iter_intf_nbr_selection(  BOOL exact,
                                                BOOL *const hit,
                                                ip_iter_intf_nbr_t *const nbx,
                                                ip_iter_intf_nbr_t *const fnd,
                                                ip_iter_intf_nbr_t *const res)
{
    if (hit && nbx && fnd && res) {
        if (exact) {
            if (_vtss_ip2_iter_intf_neighbor_cmp_func((void *)fnd, (void *)nbx) == 0) {
                memcpy(res, fnd, sizeof(ip_iter_intf_nbr_t));
                *hit = TRUE;
            }
        } else {
            if (_vtss_ip2_iter_intf_neighbor_cmp_func((void *)fnd, (void *)nbx) > 0) {
                if (*hit == FALSE) {
                    memcpy(res, fnd, sizeof(ip_iter_intf_nbr_t));
                    *hit = TRUE;
                } else {
                    if (_vtss_ip2_iter_intf_neighbor_cmp_func((void *)fnd, (void *)res) < 0) {
                        memcpy(res, fnd, sizeof(ip_iter_intf_nbr_t));
                    }
                }
            }
        }
    }
}

/*
    Return first interface neighbor information found in IP stack.

    \param vidx (IN) - vlan index to use as input key.
    \param nbra (IN) - neighbor address to use as input key.

    \param entry (OUT) - general information of the matched IPv4 or IPv6 neighbor.

    \return VTSS_OK iff entry is found.
 */
mesa_rc vtss_ip_intf_nbr_iter_first(   const vtss_if_id_vlan_t     vidx,
                                       const mesa_ip_addr_t       *nbra,
                                       ip_iter_intf_nbr_t        *const entry)
{
    mesa_rc                 ret;
    BOOL                    hit;
    u32                     idx, cnt;
    vtss_neighbour_status_t *nbr, *ndx;
    ip_iter_intf_nbr_t     *nbx, *fnd, *res;

    if (!entry) {
        return VTSS_RC_ERROR;
    }
    if ((VTSS_CALLOC_CAST(nbr, IP_ITER_MAX_NBR_OBJS, sizeof(vtss_neighbour_status_t))) == NULL) {
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(nbx, sizeof(ip_iter_intf_nbr_t))) == NULL) {
        VTSS_FREE(nbr);
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(fnd, sizeof(ip_iter_intf_nbr_t))) == NULL) {
        VTSS_FREE(nbx);
        VTSS_FREE(nbr);
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(res, sizeof(ip_iter_intf_nbr_t))) == NULL) {
        VTSS_FREE(fnd);
        VTSS_FREE(nbx);
        VTSS_FREE(nbr);
        return IP_ERROR_NOSPACE;
    }

    ndx = nbr;
    memset(nbx, 0x0, sizeof(ip_iter_intf_nbr_t));

    hit = FALSE;
    memset(res, 0x0, sizeof(ip_iter_intf_nbr_t));
    if (IP2_ITER_NBR_GET(MESA_IP_TYPE_IPV4, IP_ITER_MAX_NBR_OBJS, &cnt, nbr, ret)) {
        for (idx = 0; idx < cnt; ++idx, ++nbr) {
            if (_vtss_ip2_iter_intf_nbr_prepare(nbr, fnd)) {
                _vtss_ip2_iter_intf_nbr_selection(FALSE, &hit, nbx, fnd, res);
            }
        }
    }

    if (!hit || (ret != VTSS_OK)) {
        nbr = ndx;
        if (IP2_ITER_NBR_GET(MESA_IP_TYPE_IPV6, IP_ITER_MAX_NBR_OBJS, &cnt, nbr, ret)) {
            for (idx = 0; idx < cnt; ++idx, ++nbr) {
                if (_vtss_ip2_iter_intf_nbr_prepare(nbr, fnd)) {
                    _vtss_ip2_iter_intf_nbr_selection(FALSE, &hit, nbx, fnd, res);
                }
            }
        }
    }

    if (hit && (ret == VTSS_OK)) {
        memcpy(entry, res, sizeof(ip_iter_intf_nbr_t));
    } else {
        if (ret == VTSS_OK) {
            ret = IP_ERROR_NOTFOUND;
        }
    }

    VTSS_FREE(res);
    VTSS_FREE(fnd);
    VTSS_FREE(nbx);
    VTSS_FREE(ndx);

    return ret;
}

/*
    Return specific interface neighbor information found in IP stack.

    \param vidx (IN) - vlan index to use as input key.
    \param nbra (IN) - neighbor address to use as input key.

    \param entry (OUT) - general information of the matched IPv4 or IPv6 neighbor.

    \return VTSS_OK iff entry is found.
 */
mesa_rc vtss_ip_intf_nbr_iter_get(     const vtss_if_id_vlan_t     vidx,
                                       const mesa_ip_addr_t       *nbra,
                                       ip_iter_intf_nbr_t        *const entry)
{
    mesa_rc                 ret;
    BOOL                    hit;
    u32                     idx, cnt;
    mesa_ip_type_t          stype;
    vtss_neighbour_status_t *nbr;
    ip_iter_intf_nbr_t     *nbx, *fnd, *res;

    if (!nbra || !entry ||
        ((nbra->type != MESA_IP_TYPE_IPV4) && (nbra->type != MESA_IP_TYPE_IPV6))) {
        return VTSS_RC_ERROR;
    }
    if ((VTSS_CALLOC_CAST(nbr, IP_ITER_MAX_NBR_OBJS, sizeof(vtss_neighbour_status_t))) == NULL) {
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(nbx, sizeof(ip_iter_intf_nbr_t))) == NULL) {
        VTSS_FREE(nbr);
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(fnd, sizeof(ip_iter_intf_nbr_t))) == NULL) {
        VTSS_FREE(nbx);
        VTSS_FREE(nbr);
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(res, sizeof(ip_iter_intf_nbr_t))) == NULL) {
        VTSS_FREE(fnd);
        VTSS_FREE(nbx);
        VTSS_FREE(nbr);
        return IP_ERROR_NOSPACE;
    }

    stype = nbra->type;
    memset(nbx, 0x0, sizeof(ip_iter_intf_nbr_t));
    IP_ITER_INTF_NBR_IFIDX_SET(nbx, vidx);
    switch ( stype ) {
    case MESA_IP_TYPE_IPV4:
        IP_ITER_INTF_NBR_IPV4_ADDR_SET(nbx, nbra->addr.ipv4);
        break;
    case MESA_IP_TYPE_IPV6:
        IP_ITER_INTF_NBR_IPV6_ADDR_SET(nbx, &nbra->addr.ipv6);
        break;
    default:
        break;
    }

    hit = FALSE;
    ret = VTSS_RC_ERROR;
    memset(res, 0x0, sizeof(ip_iter_intf_nbr_t));
    if ((stype == MESA_IP_TYPE_IPV4) || (stype == MESA_IP_TYPE_IPV6)) {
        if (IP2_ITER_NBR_GET(stype, IP_ITER_MAX_NBR_OBJS, &cnt, nbr, ret)) {
            for (idx = 0; !hit && (idx < cnt); ++idx) {
                if (_vtss_ip2_iter_intf_nbr_prepare(&nbr[idx], fnd)) {
                    _vtss_ip2_iter_intf_nbr_selection(TRUE, &hit, nbx, fnd, res);
                }
            }
        }
    }

    if (hit && (ret == VTSS_OK)) {
        memcpy(entry, res, sizeof(ip_iter_intf_nbr_t));
    } else {
        if (ret == VTSS_OK) {
            ret = IP_ERROR_NOTFOUND;
        }
    }

    VTSS_FREE(res);
    VTSS_FREE(fnd);
    VTSS_FREE(nbx);
    VTSS_FREE(nbr);

    return ret;
}

/*
    Return next interface neighbor information found in IP stack.

    \param vidx (IN) - vlan index to use as input key.
    \param nbra (IN) - neighbor address to use as input key.

    \param entry (OUT) - general information of the matched IPv4 or IPv6 neighbor.

    \return VTSS_OK iff entry is found.
 */
mesa_rc vtss_ip_intf_nbr_iter_next(    const vtss_if_id_vlan_t     vidx,
                                       const mesa_ip_addr_t       *nbra,
                                       ip_iter_intf_nbr_t        *const entry)
{
    mesa_rc                 ret;
    BOOL                    hit;
    u32                     idx, cnt;
    vtss_neighbour_status_t *nbr, *ndx;
    ip_iter_intf_nbr_t     *nbx, *fnd, *res;

    if (!nbra || !entry) {
        return VTSS_RC_ERROR;
    }
    if ((VTSS_CALLOC_CAST(nbr, IP_ITER_MAX_NBR_OBJS, sizeof(vtss_neighbour_status_t))) == NULL) {
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(nbx, sizeof(ip_iter_intf_nbr_t))) == NULL) {
        VTSS_FREE(nbr);
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(fnd, sizeof(ip_iter_intf_nbr_t))) == NULL) {
        VTSS_FREE(nbx);
        VTSS_FREE(nbr);
        return IP_ERROR_NOSPACE;
    }
    if ((VTSS_MALLOC_CAST(res, sizeof(ip_iter_intf_nbr_t))) == NULL) {
        VTSS_FREE(fnd);
        VTSS_FREE(nbx);
        VTSS_FREE(nbr);
        return IP_ERROR_NOSPACE;
    }

    ndx = nbr;
    memset(nbx, 0x0, sizeof(ip_iter_intf_nbr_t));
    IP_ITER_INTF_NBR_IFIDX_SET(nbx, vidx);
    switch ( nbra->type ) {
    case MESA_IP_TYPE_IPV4:
        IP_ITER_INTF_NBR_IPV4_ADDR_SET(nbx, nbra->addr.ipv4);
        break;
    case MESA_IP_TYPE_IPV6:
        IP_ITER_INTF_NBR_IPV6_ADDR_SET(nbx, &nbra->addr.ipv6);
        break;
    default:
        IP_ITER_INTF_NBR_VERSION_SET(nbx, MESA_IP_TYPE_NONE);
        break;
    }

    hit = FALSE;
    ret = VTSS_RC_ERROR;
    memset(res, 0x0, sizeof(ip_iter_intf_nbr_t));
    if ((nbra->type != MESA_IP_TYPE_IPV6) &&
        IP2_ITER_NBR_GET(MESA_IP_TYPE_IPV4, IP_ITER_MAX_NBR_OBJS, &cnt, nbr, ret)) {
        for (idx = 0; idx < cnt; ++idx, ++nbr) {
            if (_vtss_ip2_iter_intf_nbr_prepare(nbr, fnd)) {
                _vtss_ip2_iter_intf_nbr_selection(FALSE, &hit, nbx, fnd, res);
            }
        }
    }

    if (!hit || (ret != VTSS_OK)) {
        nbr = ndx;
        if (IP2_ITER_NBR_GET(MESA_IP_TYPE_IPV6, IP_ITER_MAX_NBR_OBJS, &cnt, nbr, ret)) {
            for (idx = 0; idx < cnt; ++idx, ++nbr) {
                if (_vtss_ip2_iter_intf_nbr_prepare(nbr, fnd)) {
                    _vtss_ip2_iter_intf_nbr_selection(FALSE, &hit, nbx, fnd, res);
                }
            }
        }
    }

    if (hit && (ret == VTSS_OK)) {
        memcpy(entry, res, sizeof(ip_iter_intf_nbr_t));
    } else {
        if (ret == VTSS_OK) {
            ret = IP_ERROR_NOTFOUND;
        }
    }

    VTSS_FREE(res);
    VTSS_FREE(fnd);
    VTSS_FREE(nbx);
    VTSS_FREE(ndx);

    return ret;
}
