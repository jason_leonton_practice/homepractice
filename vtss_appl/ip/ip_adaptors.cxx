/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#include <stdlib.h>
#include "main.h"
#include "ip_api.h"
#include "ip_utils.h"
#include "ip_trace.h"
#include "ip_priv.h"
#include "ip_serializer.hxx"
#include "critd_api.h"
#include "msg_api.h"
#include "vtss/basics/trace.hxx"

#define NB_CACHE_SIZE 1024


int IP_ipv4_neighbour_status_pair_cmp(
        const vtss_appl_ip_ipv4_neighbour_status_pair_t *a,
        const vtss_appl_ip_ipv4_neighbour_status_pair_t *b) {
    if (a->ip_address == b->ip_address) return 0;

    return a->ip_address < b->ip_address ? -1 : 1;
}

int IP_ipv4_neighbour_status_pair_cmp__(const void *a, const void *b) {
    return IP_ipv4_neighbour_status_pair_cmp(
            (vtss_appl_ip_ipv4_neighbour_status_pair_t *)a,
            (vtss_appl_ip_ipv4_neighbour_status_pair_t *)b);
}

static unsigned IP_ipv4_nb_cache_valid = 0;
static vtss_appl_ip_ipv4_neighbour_status_pair_t
        IP_ipv4_nb_cache[NB_CACHE_SIZE];
mesa_rc IP_ipv4_neighbour_status_cache_update() {
    u32 cnt = 0;
    static vtss_tick_count_t ts = 0;

    if (ts != 0 && vtss_current_time() < ts + VTSS_OS_MSEC2TICK(1000)) {
        VTSS_TRACE(DEBUG) << "CACHED RESULT";
        return VTSS_RC_OK;
    }

    IP_ipv4_nb_cache_valid = 0;
    VTSS_RC(vtss_appl_ip_ipv4_neighbour_status_get_list(NB_CACHE_SIZE, &cnt,
                                                        IP_ipv4_nb_cache));

    IP_ipv4_nb_cache_valid = cnt;
    qsort(IP_ipv4_nb_cache, IP_ipv4_nb_cache_valid,
          sizeof(vtss_appl_ip_ipv4_neighbour_status_pair_t),
          IP_ipv4_neighbour_status_pair_cmp__);

    ts = vtss_current_time();
    VTSS_TRACE(INFO) << "Cache updated - valid = " << cnt;
    return VTSS_RC_OK;
}

static mesa_rc IP_ipv4_neighbour_status_get(
        mesa_ipv4_t ip, vtss_appl_ip_ipv4_neighbour_status_t *const status) {
    VTSS_RC(IP_ipv4_neighbour_status_cache_update());

    for (uint32_t i = 0; i < IP_ipv4_nb_cache_valid; ++i) {
        if (ip != IP_ipv4_nb_cache[i].ip_address) continue;

        *status = IP_ipv4_nb_cache[i].status;
        return VTSS_RC_OK;
    }

    return VTSS_RC_ERROR;
}

static mesa_rc IP_ipv4_neighbour_status_itr(const mesa_ipv4_t *const in,
                                            mesa_ipv4_t *const out) {
    VTSS_RC(IP_ipv4_neighbour_status_cache_update());

    if (!in) {
        if (!IP_ipv4_nb_cache_valid) return VTSS_RC_ERROR;

        *out = IP_ipv4_nb_cache[0].ip_address;
        VTSS_TRACE(INFO) << "Get first: " << vtss::Ipv4Address(*out);
        return VTSS_RC_OK;
    }

    for (uint32_t i = 0; i < IP_ipv4_nb_cache_valid; ++i) {
        if (*in >= IP_ipv4_nb_cache[i].ip_address) continue;

        *out = IP_ipv4_nb_cache[i].ip_address;
        VTSS_TRACE(INFO) << "Get next: " << vtss::Ipv4Address(*out);
        return VTSS_RC_OK;
    }

    VTSS_TRACE(INFO) << "No next";
    return VTSS_RC_ERROR;
}

int IP_ipv6_neighbour_status_pair_cmp(
        const vtss_appl_ip_ipv6_neighbour_status_pair_t *a,
        const vtss_appl_ip_ipv6_neighbour_status_pair_t *b) {
    int res = memcmp(a->key.ip_address.addr, b->key.ip_address.addr, 16);

    if (res != 0) return res;

    if (a->key.interface != b->key.interface)
        return a->key.interface < b->key.interface ? -1 : 1;

    return 0;
}

int IP_ipv6_neighbour_status_pair_cmp__(const void *a, const void *b) {
    return IP_ipv6_neighbour_status_pair_cmp(
            (vtss_appl_ip_ipv6_neighbour_status_pair_t *)a,
            (vtss_appl_ip_ipv6_neighbour_status_pair_t *)b);
}

static unsigned IP_ipv6_nb_cache_valid = 0;
static vtss_appl_ip_ipv6_neighbour_status_pair_t
        IP_ipv6_nb_cache[NB_CACHE_SIZE];
mesa_rc IP_ipv6_neighbour_status_cache_update() {
    u32 cnt = 0;
    static vtss_tick_count_t ts = 0;

    if (ts != 0 && vtss_current_time() < ts + VTSS_OS_MSEC2TICK(1000)) {
        VTSS_TRACE(DEBUG) << "CACHED RESULT";
        return VTSS_RC_OK;
    }

    IP_ipv6_nb_cache_valid = 0;
    VTSS_RC(vtss_appl_ip_ipv6_neighbour_status_get_list(NB_CACHE_SIZE, &cnt,
                                                        IP_ipv6_nb_cache));

    IP_ipv6_nb_cache_valid = cnt;
    qsort(IP_ipv6_nb_cache, IP_ipv6_nb_cache_valid,
          sizeof(vtss_appl_ip_ipv6_neighbour_status_pair_t),
          IP_ipv6_neighbour_status_pair_cmp__);

    ts = vtss_current_time();
    VTSS_TRACE(INFO) << "Cache updated - valid = " << cnt;
    return VTSS_RC_OK;
}

static mesa_rc IP_ipv6_neighbour_status_get(
        const vtss_appl_ip_ipv6_neighbour_status_key_t *const ip,
        vtss_appl_ip_ipv6_neighbour_status_value_t *const status) {
    VTSS_RC(IP_ipv6_neighbour_status_cache_update());

    for (uint32_t i = 0; i < IP_ipv6_nb_cache_valid; ++i) {
        if (memcmp(ip, &IP_ipv6_nb_cache[i].key, sizeof(*ip)) != 0) continue;

        *status = IP_ipv6_nb_cache[i].status;
        return VTSS_RC_OK;
    }

    return VTSS_RC_ERROR;
}

static mesa_rc IP_ipv6_neighbour_status_itr(
        const vtss_appl_ip_ipv6_neighbour_status_key_t *const in,
        vtss_appl_ip_ipv6_neighbour_status_key_t *const out) {
    VTSS_RC(IP_ipv6_neighbour_status_cache_update());

    if (!in) {
        if (!IP_ipv6_nb_cache_valid) {
            // No entries in the ARP table
            return VTSS_RC_ERROR;
        }

        *out = IP_ipv6_nb_cache[0].key;
        VTSS_TRACE(INFO) << "Get first: " << *out;
        return VTSS_RC_OK;
    }

    for (uint32_t i = 0; i < IP_ipv6_nb_cache_valid; ++i) {
        if (memcmp(&IP_ipv6_nb_cache[i].key, in, sizeof(*in)) <= 0) continue;

        *out = IP_ipv6_nb_cache[i].key;
        VTSS_TRACE(INFO) << "Get next: " << *out;
        return VTSS_RC_OK;
    }

    VTSS_TRACE(INFO) << "No next";
    return VTSS_RC_ERROR;
}

// Public functions below this point ///////////////////////////////////////////
static critd_t ip_adaptor_crit;
#define EXPOSE_PUBLIC(X, ...)                                  \
    do {                                                       \
        mesa_rc rc;                                            \
        if (!msg_switch_is_master()) {                         \
            return IP_ERROR_FAILED;                            \
        }                                                      \
                                                               \
        critd_enter(&ip_adaptor_crit, VTSS_TRACE_IP_GRP_CRIT,  \
                    VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__); \
                                                               \
        rc = X(__VA_ARGS__);                                   \
                                                               \
        critd_exit(&ip_adaptor_crit, VTSS_TRACE_IP_GRP_CRIT,   \
                   VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__);  \
                                                               \
        return rc;                                             \
    } while (0)

void vtss_ip_adaptor_init() {
    critd_init(&ip_adaptor_crit, "ip.adaptor.crit", VTSS_MODULE_ID_IP,
               VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
    critd_exit(&ip_adaptor_crit, VTSS_TRACE_IP_GRP_CRIT, VTSS_TRACE_LVL_NOISE,
               __FILE__, __LINE__);
}

mesa_rc vtss_appl_ip_ipv4_neighbour_status_get(
        mesa_ipv4_t ip, vtss_appl_ip_ipv4_neighbour_status_t *const status) {
    VTSS_TRACE(INFO) << "vtss_appl_ip_ipv4_neighbour_status_get";
    EXPOSE_PUBLIC(IP_ipv4_neighbour_status_get, ip, status);
}

mesa_rc vtss_appl_ip_ipv4_neighbour_status_itr(const mesa_ipv4_t *const in,
                                               mesa_ipv4_t *const out) {
    VTSS_TRACE(INFO) << "vtss_appl_ip_ipv4_neighbour_status_itr";
    EXPOSE_PUBLIC(IP_ipv4_neighbour_status_itr, in, out);
}

mesa_rc vtss_appl_ip_ipv6_neighbour_status_get(
        const vtss_appl_ip_ipv6_neighbour_status_key_t *const ip,
        vtss_appl_ip_ipv6_neighbour_status_value_t *const status) {
    VTSS_TRACE(INFO) << "vtss_appl_ip_ipv6_neighbour_status_get";
    EXPOSE_PUBLIC(IP_ipv6_neighbour_status_get, ip, status);
}

mesa_rc vtss_appl_ip_ipv6_neighbour_status_itr(
        const vtss_appl_ip_ipv6_neighbour_status_key_t *const in,
        vtss_appl_ip_ipv6_neighbour_status_key_t *const out) {
    VTSS_TRACE(INFO) << "vtss_appl_ip_ipv6_neighbour_status_itr";
    EXPOSE_PUBLIC(IP_ipv6_neighbour_status_itr, in, out);
}

mesa_rc vtss_appl_ip_if_status_link(vtss_ifindex_t ifidx,
                                    vtss_appl_ip_if_status_link_t *const s) {
    return status_if_link.get(ifidx, s);
}

mesa_rc vtss_appl_ip_if_status_ipv4(vtss_ifindex_t ifidx,
                                    const mesa_ipv4_network_t *addr,
                                    vtss_appl_ip_if_ipv4_info_t *const s) {
    return status_if_ipv4.get(ifidx, addr, s);
}

mesa_rc vtss_appl_ip_if_status_ipv4_itr(const vtss_ifindex_t *ifidx_in,
                                        vtss_ifindex_t *ifidx_out,
                                        const mesa_ipv4_network_t *addr_in,
                                        mesa_ipv4_network_t *addr_out)
{
    vtss_appl_ip_if_ipv4_info_t info;

    if (ifidx_in)
        *ifidx_out = *ifidx_in;
    else
        memset(ifidx_out, 0, sizeof(*ifidx_out));

    if (addr_in)
        *addr_out = *addr_in;
    else
        memset(addr_out, 0, sizeof(*addr_out));

    return status_if_ipv4.get_next(ifidx_out, addr_out, &info);
}

mesa_rc vtss_appl_ip_if_status_dhcpc_v4(
        vtss_ifindex_t ifidx, vtss_appl_ip_if_status_dhcp4c_t *const s) {
    return status_if_dhcp.get(ifidx, s);
}

mesa_rc vtss_appl_ip_if_status_ipv6(vtss_ifindex_t ifidx,
                                    const mesa_ipv6_network_t *addr,
                                    vtss_appl_ip_if_ipv6_info_t *const s) {
    return status_if_ipv6.get(ifidx, addr, s);
}

mesa_rc vtss_appl_ip_if_status_ipv6_itr(const vtss_ifindex_t *ifidx_in,
                                        vtss_ifindex_t *ifidx_out,
                                        const mesa_ipv6_network_t *addr_in,
                                        mesa_ipv6_network_t *addr_out)
{
    vtss_appl_ip_if_ipv6_info_t info;

    if (ifidx_in)
        *ifidx_out = *ifidx_in;
    else
        memset(ifidx_out, 0, sizeof(*ifidx_out));

    if (addr_in)
        *addr_out = *addr_in;
    else
        memset(addr_out, 0, sizeof(*addr_out));

    return status_if_ipv6.get_next(ifidx_out, addr_out, &info);
}


mesa_rc vtss_appl_ip_ipv4_route_status_get(
        const mesa_ipv4_uc_t *const rt, vtss_appl_ip_route_status_t *const st) {
    return status_rt_ipv4.get(rt, st);
}

mesa_rc vtss_appl_ip_ipv4_route_status_itr(const mesa_ipv4_uc_t *const in,
                                           mesa_ipv4_uc_t *const out) {
    vtss_appl_ip_route_status_t info;
    if (!in) return status_rt_ipv4.get_first(out, &info);

    *out = *in;
    return status_rt_ipv4.get_next(out, &info);
}

mesa_rc vtss_appl_ip_ipv4_route_status_get_list(
        u32 max, vtss_appl_ip_ipv4_route_key_status_t *list, u32 *const cnt) {
    mesa_rc rc;
    mesa_ipv4_uc_t k;
    vtss_appl_ip_route_status_t v;

    *cnt = 0;
    while (*cnt < max) {
        if (*cnt)
            rc = status_rt_ipv4.get_next(&k, &v);
        else
            rc = status_rt_ipv4.get_first(&k, &v);
        if (rc != VTSS_RC_OK) return VTSS_RC_OK;

        list->key = k;
        list->val = v;
        list++;
        *cnt = (*cnt) + 1;
    }

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_ip_ipv4_acd_status_itr(
    const vtss_appl_ip_ipv4_acd_status_key_t *const in,
    vtss_appl_ip_ipv4_acd_status_key_t       *const out)
{
    vtss_appl_ip_ipv4_acd_status_t status;

    if (in == NULL) {
        return status_acd_ipv4.get_first(out, &status);
    }

    *out = *in;
    return status_acd_ipv4.get_next(out, &status);
}

mesa_rc vtss_appl_ip_ipv4_acd_status_get(
    const vtss_appl_ip_ipv4_acd_status_key_t *const key,
    vtss_appl_ip_ipv4_acd_status_t *const status)
{
    return status_acd_ipv4.get(key, status);
}

mesa_rc vtss_appl_ip_ipv6_route_status_get(
       const vtss_appl_ip_ipv6_route_conf_t *const in,
       vtss_appl_ip_route_status_t *const st) {
    return status_rt_ipv6.get(in, st);
}

mesa_rc vtss_appl_ip_ipv6_route_status_itr(
       const vtss_appl_ip_ipv6_route_conf_t *const in,
       vtss_appl_ip_ipv6_route_conf_t *const out) {
    vtss_appl_ip_route_status_t st;
    if (!in) return status_rt_ipv6.get_first(out, &st);

    *out = *in;
    return status_rt_ipv6.get_next(out, &st);
}

mesa_rc vtss_appl_ip_ipv6_route_status_get_list(
       u32 max, vtss_appl_ip_ipv6_route_key_status_t *list, u32 *const cnt) {
    mesa_rc rc;
    vtss_appl_ip_ipv6_route_conf_t k;
    vtss_appl_ip_route_status_t v;

    *cnt = 0;
    while (*cnt < max) {
        if (*cnt)
            rc = status_rt_ipv6.get_next(&k, &v);
        else
            rc = status_rt_ipv6.get_first(&k, &v);
        if (rc != VTSS_RC_OK) return VTSS_RC_OK;

        list->key = k;
        list->val = v;
        list++;
        *cnt = (*cnt) + 1;
    }

    return VTSS_RC_OK;
}

