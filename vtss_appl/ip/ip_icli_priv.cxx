/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/


#include "mgmt_api.h"
#include "misc_api.h"
#include "msg_api.h"
#include "ip_api.h"
#include "ip_utils.h"
#include "ip_iterators.h"
#include "ip_icli_priv.h"
#include "icli_porting_util.h"

#ifdef VTSS_SW_OPTION_ICFG
#include "icfg_api.h"

#define IP2_ICFG_REG(w, x, y, z)        (((w) = vtss_icfg_query_register((x), (y), (z))) == VTSS_OK)
#define PRINTF(...)                     (void) vtss_icfg_printf(result, __VA_ARGS__);
#endif /* VTSS_SW_OPTION_ICFG */

#define VTSS_TRACE_MODULE_ID            VTSS_MODULE_ID_IP
#define VTSS_ALLOC_MODULE_ID            VTSS_MODULE_ID_IP

/*
******************************************************************************

    Static Function

******************************************************************************
*/
#ifdef VTSS_SW_OPTION_ICFG
static mesa_rc IP2_ipv4_icfg_conf(const vtss_icfg_query_request_t *req,
                                  vtss_icfg_query_result_t *result)
{
    switch (req->cmd_mode) {
    case ICLI_CMD_MODE_GLOBAL_CONFIG: {
        mesa_rc rc;
        int cnt, i;
        mesa_routing_entry_t *routes = (mesa_routing_entry_t *)VTSS_CALLOC(IP_MAX_ROUTES, sizeof(*routes));

        if (!routes) {
            return VTSS_RC_ERROR;
        }

#ifdef VTSS_SW_OPTION_L3RT
        {
            vtss_ip_global_param_priv_t conf;

            rc = vtss_ip_global_param_get_(&conf);
            if (rc != VTSS_OK) {
                goto ROUTE_DONE;
            }
            if (conf.enable_routing) {
                PRINTF("ip routing\n");
            } else if (req->all_defaults) {
                PRINTF("no ip routing\n");
            }
        }
#endif

        /*lint --e{429} */
        rc = vtss_ip_route_conf_get(IP_MAX_ROUTES, routes, &cnt);
        if (rc != VTSS_OK) {
            goto ROUTE_DONE;
        }

        for (i = 0; i < cnt; i++) {
            mesa_ipv4_t mask;
            mesa_routing_entry_t const *rt = &routes[i];
            vtss_appl_ip_ipv4_route_conf_t route_conf;
            if (vtss_appl_ip_ipv4_route_conf_get(&rt->route.ipv4_uc, &route_conf) != VTSS_OK) {
                continue;
            }
            (void)
            vtss_conv_prefix_to_ipv4mask(rt->route.ipv4_uc.network.prefix_size,
                                         &mask);
            if (rt->type == MESA_ROUTING_ENTRY_TYPE_IPV4_UC) {
                PRINTF("ip route " VTSS_IPV4_FORMAT " " VTSS_IPV4_FORMAT
                       " " VTSS_IPV4_FORMAT,
                       VTSS_IPV4_ARGS(rt->route.ipv4_uc.network.address),
                       VTSS_IPV4_ARGS(mask),
                       VTSS_IPV4_ARGS(rt->route.ipv4_uc.destination));
#if defined(VTSS_SW_OPTION_FRR)
                if (route_conf.distance != IP_ROUTE_DEF_DISTANCE) {
                    PRINTF(" %u", route_conf.distance);
                }
#endif /* VTSS_SW_OPTION_FRR */
                PRINTF("\n");
            }
        }

ROUTE_DONE:
        VTSS_FREE(routes);
        if (rc != VTSS_RC_OK) {
            return rc;
        }
        break;
    }

    case ICLI_CMD_MODE_INTERFACE_VLAN: {
        mesa_vid_t vid = (mesa_vid_t)req->instance_id.vlan;
        vtss_appl_ip_ipv4_conf_t ip_conf;

        vtss_ifindex_t ifidx;
        (void) vtss_ifindex_from_vlan(vid, &ifidx);
        if (vtss_appl_ip_ipv4_conf_get(ifidx, &ip_conf) == VTSS_RC_OK) {
            mesa_ipv4_t mask;
            (void) vtss_conv_prefix_to_ipv4mask(ip_conf.network.prefix_size,
                                                &mask);
            if (!ip_conf.active) {
                PRINTF(" no ip address\n");

            } else if (ip_conf.dhcpc) {
                PRINTF(" ip address dhcp");
                if (ip_conf.fallback_timeout) {
                    PRINTF(" fallback " VTSS_IPV4_FORMAT " " VTSS_IPV4_FORMAT,
                           VTSS_IPV4_ARGS(ip_conf.network.address),
                           VTSS_IPV4_ARGS(mask));
                    PRINTF(" timeout %u", ip_conf.fallback_timeout);
                }

                // client_id
                if (ip_conf.dhcpc_params.client_id.type != VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_AUTO) {
                    PRINTF(" client-id ");
                    if (ip_conf.dhcpc_params.client_id.type == VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_IFMAC) {
                        vtss_ifindex_elm_t elm;
                        if (vtss_ifindex_decompose(ip_conf.dhcpc_params.client_id.if_mac, &elm) == VTSS_RC_OK &&
                            elm.iftype == VTSS_IFINDEX_TYPE_PORT) {
                            char str_buf[32];
                            PRINTF("%s", icli_port_info_txt(elm.usid, iport2uport(elm.ordinal), str_buf));
                        }
                    } else if (ip_conf.dhcpc_params.client_id.type == VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_ASCII) {
                        PRINTF("ascii %s", ip_conf.dhcpc_params.client_id.ascii);
                    } else if (ip_conf.dhcpc_params.client_id.type == VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_HEX) {
                        size_t hex_str_len = strlen(ip_conf.dhcpc_params.client_id.hex);
                        PRINTF("hex ");
                        for (int idx = 0; idx < hex_str_len; ++idx) {
                            PRINTF("%c", toupper(ip_conf.dhcpc_params.client_id.hex[idx]));
                        }
                    }
                }

                // hostname
                if (strlen(ip_conf.dhcpc_params.hostname)) {
                    PRINTF(" hostname %s", ip_conf.dhcpc_params.hostname);
                }
                PRINTF("\n");
            } else {
                PRINTF(" ip address " VTSS_IPV4_FORMAT " " VTSS_IPV4_FORMAT "\n",
                        VTSS_IPV4_ARGS(ip_conf.network.address),
                        VTSS_IPV4_ARGS(mask));
            }
        } else {
            PRINTF(" no ip address\n");
        }

        break;
    }

    default:
        //Not needed
        break;
    }

    return VTSS_RC_OK;
}

#ifdef VTSS_SW_OPTION_IPV6
static mesa_rc IP2_ipv6_icfg_interface(const vtss_icfg_query_request_t *req,
                                       vtss_icfg_query_result_t *result)
{
    mesa_rc rc = VTSS_OK;

    if (req && result) {
        vtss_appl_ip_ipv6_conf_t ip6_conf;
        mesa_vid_t      vid6 = (mesa_vid_t)req->instance_id.vlan;

        /*
            COMMAND = ipv6 address <ipv6_subnet>
            T.B.D
            COMMAND = ipv6 address autoconfig [default]
            COMMAND = ipv6 mtu <1280-1500>
            COMMAND = ipv6 nd managed-config-flag
            COMMAND = ipv6 nd other-config-flag
            COMMAND = ipv6 nd reachable-time <milliseconds:0-3600000>
            COMMAND = ipv6 nd prefix <ipv6_subnet> valid-lifetime <seconds:0-4294967295>
            COMMAND = ipv6 nd prefix <ipv6_subnet> preferred-lifetime <seconds:0-4294967295>
            COMMAND = ipv6 nd prefix <ipv6_subnet> off-link
            COMMAND = ipv6 nd prefix <ipv6_subnet> no-autoconfig
            COMMAND = ipv6 nd prefix <ipv6_subnet> no-rtr-address
            COMMAND = ipv6 nd ra interval <maximum_secs:4-1800> [<minimum_secs:3-1350>]
            COMMAND = ipv6 nd ra lifetime <seconds:0-9000>

        */

        vtss_ifindex_t ifidx;
        (void) vtss_ifindex_from_vlan(vid6, &ifidx);
        if (vtss_appl_ip_ipv6_conf_get(ifidx, &ip6_conf) == VTSS_RC_OK) {
            if (!ip6_conf.active && req->all_defaults) {
                PRINTF(" no ipv6 address\n");
            }

            if (ip6_conf.active) {
                PRINTF(" ipv6 address " VTSS_IPV6_FORMAT "/%u\n",
                        VTSS_IPV6_ARGS(ip6_conf.network.address),
                        ip6_conf.network.prefix_size);
            }

        } else {
            if (req->all_defaults) {
                PRINTF(" no ipv6 address\n");
            }
        }
    }

    return rc;
}

static mesa_rc IP2_ipv6_icfg_global(const vtss_icfg_query_request_t *req,
                                    vtss_icfg_query_result_t *result)
{
    mesa_rc rc = VTSS_OK;

    if (req && result) {
        vtss_ip_global_param_priv_t conf;

        /*
            COMMAND = ipv6 route <ipv6_subnet> { <ipv6_ucast> | interface vlan <vlan_id> <ipv6_addr> }
            T.B.D
            COMMAND = ipv6 unicast-routing
        */
        if ((rc = vtss_ip_global_param_get_(&conf)) == VTSS_OK) {
            mesa_routing_entry_t    *routes;

//            if (req->all_defaults ||
//                (conf.enable_routing != IP_ICFG_DEF_ROUTING6)) {
//                PRINTF("%sipv6 unicast-routing\n", conf.enable_routing ? "" : "no ");
//            }

            if ((VTSS_CALLOC_CAST(routes, IP_MAX_ROUTES, sizeof(mesa_routing_entry_t))) != NULL) {
                int i, cnt;

                if ((rc = vtss_ip_route_conf_get(IP_MAX_ROUTES, routes, &cnt)) == VTSS_OK) {
                    mesa_routing_entry_t *rt;

                    for (i = 0; i < cnt; i++) {
                        rt = &routes[i];
                        if (rt->type != MESA_ROUTING_ENTRY_TYPE_IPV6_UC) {
                            continue;
                        }

                        if (rt->vlan) {
                            PRINTF("ipv6 route " VTSS_IPV6N_FORMAT " interface vlan %u " VTSS_IPV6_FORMAT "\n",
                                   VTSS_IPV6N_ARG(rt->route.ipv6_uc.network),
                                   rt->vlan,
                                   VTSS_IPV6_ARGS(rt->route.ipv6_uc.destination));
                        } else {
                            PRINTF("ipv6 route " VTSS_IPV6N_FORMAT " " VTSS_IPV6_FORMAT "\n",
                                   VTSS_IPV6N_ARG(rt->route.ipv6_uc.network),
                                   VTSS_IPV6_ARGS(rt->route.ipv6_uc.destination));
                        }
                    }
                }

                VTSS_FREE(routes);
            } else {
                rc = VTSS_RC_ERROR;
            }
        }
    }

    return rc;
}
#endif /* VTSS_SW_OPTION_IPV6 */
#endif /* VTSS_SW_OPTION_ICFG */

/*
******************************************************************************

    Public functions

******************************************************************************
*/
mesa_rc vtss_ip_ipv4_icfg_init(void)
{
    mesa_rc ip2_icfg_rc = VTSS_RC_OK;

#ifdef VTSS_SW_OPTION_ICFG
    VTSS_RC(vtss_icfg_query_register(VTSS_ICFG_IPV4_GLOBAL, "ipv4",
                                     IP2_ipv4_icfg_conf));
    VTSS_RC(vtss_icfg_query_register(VTSS_ICFG_IPV4_INTERFACE, "ipv4",
                                     IP2_ipv4_icfg_conf));

#ifdef VTSS_SW_OPTION_IPV6
    if (IP2_ICFG_REG(ip2_icfg_rc, VTSS_ICFG_IPV6_GLOBAL, "ipv6", IP2_ipv6_icfg_global)) {
        if (IP2_ICFG_REG(ip2_icfg_rc, VTSS_ICFG_IPV6_INTERFACE, "ipv6", IP2_ipv6_icfg_interface)) {
            T_I("IP2(IPv6) ICFG done");
        }
    }
#endif /* VTSS_SW_OPTION_IPV6 */
#endif

    return ip2_icfg_rc;
}

mesa_rc get_vlan_if(mesa_vid_t vid)
{
    vtss_ifindex_t  ifidx;
    if (vtss_ip_if_exists(vid)) {
        return VTSS_OK;
    }

    (void) vtss_ifindex_from_vlan(vid, &ifidx);
    return vtss_appl_ip_if_conf_set(ifidx);
}

void ip_icli_req_init(ip_icli_req_t *req, u32 session_id)
{
    memset(req, 0, sizeof(*req));
    req->session_id = session_id;
}

static void ip_icli_stats(u32 session_id, vtss_appl_ip_if_status_ip_stat_t *s)
{
    icli_cmd_stati(session_id, "Packets", "", s->HCInReceives, s->HCOutRequests);
    icli_cmd_stati(session_id, "Octets", "", s->HCInOctets, s->HCOutOctets);
    icli_cmd_stati(session_id, "Unicast", "",
                   s->HCInReceives - s->HCInMcastPkts - s->HCInBcastPkts,
                   s->HCOutRequests - s->HCOutMcastPkts - s->HCOutBcastPkts);
    icli_cmd_stati(session_id, "Multicast", "", s->HCInMcastPkts, s->HCOutMcastPkts);
    icli_cmd_stati(session_id, "Broadcast", "", s->HCInBcastPkts, s->HCOutBcastPkts);
    icli_cmd_stati(session_id, "Discards", "", s->InDiscards, s->OutDiscards);
    icli_cmd_stati(session_id, "ReasmOKs",  "FragOKs", s->ReasmOKs, s->OutFragOKs);
    icli_cmd_stati(session_id, "ReasmReqds", "FragCreates", s->ReasmReqds, s->OutFragCreates);
    icli_cmd_stati(session_id, "ReasmFails", "FragFails", s->ReasmFails, s->OutFragFails);
    icli_cmd_stati(session_id, "Delivers", NULL, s->InDelivers, 0);
    icli_cmd_stati(session_id, "HdrErrors", NULL, s->InHdrErrors, 0);
    icli_cmd_stati(session_id, "AddrErrors", NULL, s->InAddrErrors, 0);
    icli_cmd_stati(session_id, "UnknProtos", NULL, s->InUnknownProtos, 0);
    icli_cmd_stati(session_id, "Truncated", NULL, s->InTruncatedPkts, 0);
    ICLI_PRINTF("\n");
}


icli_rc_t ip_icli_stats_show(ip_icli_req_t *req)
{
    mesa_rc                          rc;
    vtss_appl_ip_if_status_ip_stat_t s;
    u32                              i, j, session_id = req->session_id;
    mesa_vid_t                       vid;
    BOOL                             vid_valid[VTSS_VIDS];
    icli_unsigned_range_t            *list = req->vid_list;
    vtss_ifindex_t                   ifidx;
    const char                       *txt = (req->ipv4 ? "IPv4" : "IPv6");

    if (req->system || list == NULL) {
        // System counters
        if (req->ipv4) {
            rc = vtss_appl_ip_system_statistics_ipv4_get(&s);
        } else {
            rc = vtss_appl_ip_system_statistics_ipv6_get(&s);
        }
        if (rc != VTSS_RC_OK) {
            ICLI_PRINTF("Failed to get %s system statistics\n", txt);
            return ICLI_RC_ERROR;
        }
        ICLI_PRINTF("%s system statistics:\n", txt);
        ip_icli_stats(session_id, &s);
    }

    if (!req->system || list != NULL) {
        // Interface counters
        for (vid = 0; vid < VTSS_VIDS; vid++) {
            vid_valid[vid] = (list ? 0 : 1);
        }
        for (i = 0; list != NULL && i < list->cnt; i++) {
            for (j = list->range[i].min; j <= list->range[i].max; j++) {
                if (j < VTSS_VIDS)
                    vid_valid[j] = 1;
            }
        }
        for (vid = 1; vid < VTSS_VIDS; vid++) {
            if (vid_valid[vid] &&
                vtss_ifindex_from_vlan(vid, &ifidx) == VTSS_RC_OK) {
                if (req->ipv4) {
                    rc = vtss_appl_ip_if_statistics_ipv4(ifidx, &s);
                } else {
                    rc = vtss_appl_ip_if_statistics_ipv6(ifidx, &s);
                }
                if (rc == VTSS_RC_OK) {
                    ICLI_PRINTF("%s VLAN %u statistics:\n", txt, vid);
                    ip_icli_stats(session_id, &s);
                }
            }
        }
    }
    return ICLI_RC_OK;
}

icli_rc_t ip_icli_stats_clear(ip_icli_req_t *req)
{
    u32            session_id = req->session_id;
    mesa_vid_t     vid;
    vtss_ifindex_t ifidx;

    // System counters
    if (vtss_appl_ip_system_statistics_clear(req->ipv4 ? MESA_IP_TYPE_IPV4 : MESA_IP_TYPE_IPV6) != VTSS_RC_OK) {
        ICLI_PRINTF("Failed to clear IP system statistics\n");
        return ICLI_RC_ERROR;
    }

    // Interface counters
    for (vid = 1; vid < VTSS_VIDS; vid++) {
        if (vtss_ifindex_from_vlan(vid, &ifidx) == VTSS_RC_OK) {
            if (req->ipv4) {
                (void)vtss_appl_ip_if_statistics_ipv4_clear(ifidx);
            } else {
                (void)vtss_appl_ip_if_statistics_ipv6_clear(ifidx);
            }
        }
    }
    return ICLI_RC_OK;
}

icli_rc_t ip_icli_if_stats_show(ip_icli_req_t *req)
{
    vtss_appl_ip_if_status_link_stat_t s;
    u32                                i, j, session_id = req->session_id;
    mesa_vid_t                         vid;
    BOOL                               vid_valid[VTSS_VIDS];
    icli_unsigned_range_t              *list = req->vid_list;
    vtss_ifindex_t                     ifidx;

    for (vid = 0; vid < VTSS_VIDS; vid++) {
        vid_valid[vid] = (list ? 0 : 1);
    }
    for (i = 0; list != NULL && i < list->cnt; i++) {
        for (j = list->range[i].min; j <= list->range[i].max; j++) {
            if (j < VTSS_VIDS)
                vid_valid[j] = 1;
        }
    }

    for (vid = 1; vid < VTSS_VIDS; vid++) {
        if (vid_valid[vid] &&
            vtss_ifindex_from_vlan(vid, &ifidx) == VTSS_RC_OK &&
            vtss_appl_ip_if_statistics_link(ifidx, &s) == VTSS_RC_OK) {
            ICLI_PRINTF("VLAN %u statistics:\n", vid);
            icli_cmd_stati(session_id, "Packets", "", s.in_packets, s.out_packets);
            icli_cmd_stati(session_id, "Octets", "", s.in_bytes, s.out_bytes);
            ICLI_PRINTF("\n");
        }
    }
    return ICLI_RC_OK;
}

icli_rc_t ip_icli_if_stats_clear(ip_icli_req_t *req)
{
    mesa_vid_t     vid;
    vtss_ifindex_t ifidx;

    for (vid = 1; vid < VTSS_VIDS; vid++) {
        if (vtss_ifindex_from_vlan(vid, &ifidx) == VTSS_RC_OK) {
            (void)vtss_appl_ip_if_statistics_link_clear(ifidx);
        }
    }
    return ICLI_RC_OK;
}

icli_rc_t ip_icli_acd_show(ip_icli_req_t *req)
{
    u32                                session_id = req->session_id;
    BOOL                               header = TRUE;
    vtss_appl_ip_ipv4_acd_status_key_t key, *in = NULL;
    vtss_appl_ip_ipv4_acd_status_t     status;
    vtss_ifindex_elm_t                 e_vid, e_port;
    char                               buf[32];

    while (vtss_appl_ip_ipv4_acd_status_itr(in, &key) == VTSS_RC_OK &&
           vtss_appl_ip_ipv4_acd_status_get(&key, &status) == VTSS_RC_OK &&
           vtss_ifindex_decompose(status.interface, &e_vid) == VTSS_RC_OK &&
           e_vid.iftype == VTSS_IFINDEX_TYPE_VLAN &&
           vtss_ifindex_decompose(status.interface_port, &e_port) == VTSS_RC_OK &&
           e_port.iftype == VTSS_IFINDEX_TYPE_PORT) {
        in = &key;
        if (header) {
            icli_table_header(session_id, "SIP              SMAC               VID   Interface");
            header = FALSE;
        }
        ICLI_PRINTF("%-17s", misc_ipv4_txt(key.sip, buf));
        ICLI_PRINTF("%-19s", misc_mac_txt(key.smac.addr, buf));
        ICLI_PRINTF("%-6u", e_vid.ordinal);
        icli_print_port_info_txt(session_id, VTSS_USID_START, iport2uport(e_port.ordinal));
        ICLI_PRINTF("\n");
    }
    return ICLI_RC_OK;
}

icli_rc_t ip_icli_acd_clear(ip_icli_req_t *req)
{
    u32                           session_id = req->session_id;
    vtss_appl_ip_global_actions_t a;

    memset(&a, 0, sizeof(a));
    a.ipv4_acd_status_clear = TRUE;
    if (vtss_appl_ip_global_controls(&a) != VTSS_RC_OK) {
        ICLI_PRINTF("ACD clear failed\n");
    }
    return ICLI_RC_OK;
}
