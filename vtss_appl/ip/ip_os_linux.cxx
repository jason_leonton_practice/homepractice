/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <linux/if_link.h>
#include <linux/if_addr.h>
#include <linux/if_arp.h>
#include <linux/ip.h>
#include <pthread.h>

#include "conf_api.h"
#include "critd_api.h"

#include "ip_priv.h"
#include "ip_utils.h"
#include "ip_trace.h"
#include "ip_os_api.h"
#include "ip_chip_api.h"
#include "ip_expose.hxx"
#include "vtss_netlink.hxx"
#include "conf_api.h"   // for conf_mgmt_mac_addr_get
#include "packet_api.h" // For definition of PACKET_XTR_QU_IGMP
#ifdef VTSS_SW_OPTION_ACL
#include "acl_api.h"
#endif
#include "vtss/basics/fd.hxx"
#include "vtss/basics/set.hxx"
#include "vtss/basics/map.hxx"
#include "vtss/basics/trace.hxx"
#include "vtss/basics/array.hxx"
#include "vtss/basics/memory.hxx"
#include "vtss/basics/enum_macros.hxx"
#include "vtss/basics/parser_impl.hxx"
#include "vtss/basics/string-utils.hxx"
#include "vtss/basics/synchronized.hxx"
#include "vtss/basics/memcmp-operator.hxx"
#include "vtss/basics/formatting_tags.hxx"
#include "vtss/basics/expose/table-status.hxx"

extern critd_t ip2_crit;

VTSS_ENUM_BITWISE(vtss_appl_ip_if_link_flag_t);

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_IP

typedef struct {
    BOOL ipv4_addr;
    BOOL ipv6_addr;
    BOOL ipv4_route;
    BOOL ipv6_route;
    BOOL ipv4_neighbor;
    BOOL ipv6_neighbor;
    BOOL link;
    BOOL mac_addr_list;
} ip_netlink_poll_t;

using namespace vtss;
using namespace vtss::appl::netlink;

StatusIfLink status_if_link;
StatusIfIpv4 status_if_ipv4;
StatusIfIpv6 status_if_ipv6;
StatusRtIpv4 status_rt_ipv4;
StatusRtIpv6 status_rt_ipv6;

mesa_rc IP_ifindex_to_vlan(vtss_ifindex_t i, mesa_vid_t *v);

namespace vtss {
namespace appl {
namespace ip {
namespace netlink {

struct IfStatus {
    bool ok;
    uint32_t os_flags;
    u32 os_ifindex;
    u32 mtu;
    mesa_mac_t mac;
    mesa_mac_t bcast;
};

VTSS_BASICS_MEMCMP_OPERATOR(IfStatus);

struct VlanMacKey {
    mesa_vid_t vid;
    mesa_mac_t mac;
};

inline bool operator<(const VlanMacKey &a, const VlanMacKey &b) {
    if (a.vid != b.vid) return a.vid < b.vid;
    if (a.mac != b.mac) return a.mac < b.mac;
    return false;
}

ostream &operator<<(ostream &o, const VlanMacKey &i) {
    o << "{vid:" << i.vid << " mac:" << i.mac << "}";
    return o;
}

struct Ipv4Neighbor {
    vtss_appl_ip_ipv4_neighbour_status_pair_t data;
    vtss_if_id_t                              if_id;
    int                                       if_index;
};

inline bool operator<(const Ipv4Neighbor &a, const Ipv4Neighbor &b) {
    return (a.data.ip_address < b.data.ip_address);
}

ostream &operator<<(ostream &o, const Ipv4Neighbor &i) {
    uint32_t a = i.data.ip_address;
    o << "{ip:" << vtss::AsIpv4(a) << " mac:" << i.data.status.mac_address << "}";
    return o;
}

struct Ipv6Neighbor {
    vtss_appl_ip_ipv6_neighbour_status_pair_t data;
    vtss_if_id_t                              if_id;
    int                                       if_index;
};

inline bool operator<(const Ipv6Neighbor &a, const Ipv6Neighbor &b) {
    return (a.data.key.ip_address != b.data.key.ip_address ? (a.data.key.ip_address < b.data.key.ip_address) : (a.data.key.interface < b.data.key.interface));
}

ostream &operator<<(ostream &o, const Ipv6Neighbor &i) {
    o << "{ip:" << i.data.key.ip_address << " mac:" << i.data.status.mac_address << "}";
    return o;
}

struct VlanIpv4NetKey {
    mesa_vid_t vid;
    mesa_ipv4_network_t net;
    mesa_ipv4_t broadcast;
};

inline bool operator<(const VlanIpv4NetKey &a, const VlanIpv4NetKey &b) {
    if (a.vid != b.vid) return a.vid < b.vid;
    if (a.net != b.net) return a.net < b.net;
    if (a.broadcast != b.broadcast) return a.broadcast < b.broadcast;
    return false;
}

ostream &operator<<(ostream &o, const VlanIpv4NetKey &i) {
    uint32_t b = i.broadcast;
    o << "{vid:" << i.vid << " net:" << i.net << " bcast:" << vtss::AsIpv4(b)
      << "}";
    return o;
}

struct VlanIpv6NetKey {
    mesa_vid_t vid;
    mesa_ipv6_network_t net;
};

struct VlanIpv6NetVal {
    vtss_appl_ip_if_ipv6_flag_t flags;
    int os_ifindex;
};

inline bool operator<(const VlanIpv6NetKey &a, const VlanIpv6NetKey &b) {
    if (a.vid != b.vid) return a.vid < b.vid;
    if (a.net != b.net) return a.net < b.net;
    return false;
}

ostream &operator<<(ostream &o, const VlanIpv6NetKey &i) {
    o << "{vid:" << i.vid << " net:" << i.net << "}";
    return o;
}

struct State {
    Set<VlanMacKey> mac_addr_list;
    Map<int, mesa_vid_t> os_ifindex_to_vlan;
    Map<mesa_vid_t, mesa_ipv6_network_t> vid_to_ipv6_network;
    Set<Ipv4Neighbor> ipv4_neighbor_list;
    Set<Ipv6Neighbor> ipv6_neighbor_list;
};

static Synchronized<State, VTSS_MODULE_ID_IP, VTSS_TRACE_IP_GRP_CRIT> state;

#define D VTSS_TRACE(VTSS_MODULE_ID_IP, VTSS_TRACE_IP_GRP_OS, DEBUG)
#define I VTSS_TRACE(VTSS_MODULE_ID_IP, VTSS_TRACE_IP_GRP_OS, INFO)
#define N VTSS_TRACE(VTSS_MODULE_ID_IP, VTSS_TRACE_IP_GRP_OS, NOISE)
#define W VTSS_TRACE(VTSS_MODULE_ID_IP, VTSS_TRACE_IP_GRP_OS, WARNING)
#define E VTSS_TRACE(VTSS_MODULE_ID_IP, VTSS_TRACE_IP_GRP_OS, ERROR)

#define DO(FUNC, ...)                                \
    rc = FUNC(__VA_ARGS__);                          \
    if (rc != VTSS_RC_OK) {                          \
        I << "Failed: " #FUNC " error code: " << rc; \
        return rc;                                   \
    }

#define DO_(FUNC, ...)                                \
    rc = FUNC(__VA_ARGS__);                          \
    if (rc != VTSS_RC_OK) {                          \
        I << "Failed: " #FUNC " error code: " << rc; \
        return;                                   \
    }

#define DO_RECOVER(LABEL, FUNC, ...)                 \
    rc = FUNC(__VA_ARGS__);                          \
    if (rc != VTSS_RC_OK) {                          \
        I << "Failed: " #FUNC " error code: " << rc; \
        goto LABEL;                                  \
    }

template <typename T>
inline mesa_rc nl_req_rtm_dump(const T &t, NetlinkCallbackAbstract *cb,
                               int sndbuf = 32768, int rcvbuf = 1048576) {
    return nl_req(t, cb, RTM_GETLINK, NLM_F_DUMP, sndbuf, rcvbuf);
}

inline mesa_rc nl_req_link_dump(NetlinkCallbackAbstract *cb, int sndbuf = 32768,
                                int rcvbuf = 1048576) {
    struct {
        struct nlmsghdr nlh;
        struct ifinfomsg r;
    } req;

    int seq = netlink_seq();
    memset(&req, 0, sizeof(req));
    req.r.ifi_family = AF_PACKET;
    req.nlh.nlmsg_len = sizeof(req);
    req.nlh.nlmsg_type = RTM_GETLINK;
    req.nlh.nlmsg_flags = NLM_F_DUMP | NLM_F_REQUEST;
    req.nlh.nlmsg_pid = 0;
    req.nlh.nlmsg_seq = seq;

    return nl_req((const void *)&req, sizeof(req), seq, cb, sndbuf, rcvbuf);
}

inline mesa_rc nl_req_link_ipv6_dump(NetlinkCallbackAbstract *cb, int sndbuf = 32768,
                                     int rcvbuf = 1048576) {
    struct {
        struct nlmsghdr nlh;
        struct rtgenmsg r;
    } req;

    int seq = netlink_seq();
    memset(&req, 0, sizeof(req));
    req.r.rtgen_family = AF_INET6;
    req.nlh.nlmsg_len = sizeof(req);
    req.nlh.nlmsg_type = RTM_GETLINK;
    req.nlh.nlmsg_flags = NLM_F_DUMP | NLM_F_REQUEST;
    req.nlh.nlmsg_pid = 0;
    req.nlh.nlmsg_seq = seq;

    return nl_req((const void *)&req, sizeof(req), seq, cb, sndbuf, rcvbuf);
}

inline mesa_rc nl_req_neigh_dump(NetlinkCallbackAbstract *cb,
                                 unsigned char family, int sndbuf = 32768, int rcvbuf = 1048576) {
    struct {
        struct nlmsghdr nlh;
        struct ndmsg ndm;
    } req;

    int seq = netlink_seq();
    memset(&req, 0, sizeof(req));
    req.ndm.ndm_family = family;
    req.nlh.nlmsg_len = sizeof(req);
    req.nlh.nlmsg_type = RTM_GETNEIGH;
    req.nlh.nlmsg_flags = NLM_F_DUMP | NLM_F_REQUEST;
    req.nlh.nlmsg_pid = 0;
    req.nlh.nlmsg_seq = seq;

    return nl_req((const void *)&req, sizeof(req), seq, cb, sndbuf, rcvbuf);
}

inline mesa_rc nl_req_ipaddr_dump(NetlinkCallbackAbstract *cb,
                                  unsigned char family, int sndbuf = 32768, int rcvbuf = 1048576) {
    struct {
        struct nlmsghdr nlh;
        struct ifaddrmsg ifa;
    } req;

    int seq = netlink_seq();
    memset(&req, 0, sizeof(req));
    req.ifa.ifa_family = family;
    req.nlh.nlmsg_len = sizeof(req);
    req.nlh.nlmsg_type = RTM_GETADDR;
    req.nlh.nlmsg_flags = NLM_F_DUMP | NLM_F_REQUEST;
    req.nlh.nlmsg_pid = 0;
    req.nlh.nlmsg_seq = seq;

    return nl_req((const void *)&req, sizeof(req), seq, cb, sndbuf, rcvbuf);
}

inline mesa_rc nl_req_route_dump(NetlinkCallbackAbstract *cb,
                                 unsigned char family,
                                 int sndbuf = 32768, int rcvbuf = 1048576) {
    struct {
        struct nlmsghdr nlh;
        struct rtmsg rtm;
    } req;

    int seq = netlink_seq();
    memset(&req, 0, sizeof(req));
    req.rtm.rtm_family = family;
    req.nlh.nlmsg_len = sizeof(req);
    req.nlh.nlmsg_type = RTM_GETROUTE;
    req.nlh.nlmsg_flags = NLM_F_DUMP | NLM_F_REQUEST;
    req.nlh.nlmsg_pid = 0;
    req.nlh.nlmsg_seq = seq;

    return nl_req((const void *)&req, sizeof(req), seq, cb, sndbuf, rcvbuf);
}


#ifdef VTSS_SW_OPTION_ACL
/* Only 23 bits of IP multicast address are mapped to the MAC-layer multicast
 * address. For class D convention, that means 5 bits in the IP multicast address do
 * not map to the MAC-layer multicast address. So we encounter problem when using
 * chip MAC-table to copy the contorl packets, say '224.0.0.5', to CPU, that
 * also copies '225.0.0.5', '226.0.0.5' such kinds of packets to CPU unexpectedly.
 *
 * To avoid having non-control traffic copied to the CPU, we choose to use ACL to
 * qualify DIP and copy it to CPU instead of using the chip MAC table.
 * Refer to the IPv4 Multicast Address Space Registry, we have chosen to use the
 * 23 bits from the MAC address, and assume that the remaining 5 bits is set to zero.
 * This covers the control packets locating from 224.0.0.0 to 224.127.255.255.
 * - this may be an issue if we want to use the linux kernel for doing IGMPv3 as this
 * is using SSM (source specific multicast)
 */
/* Add multicast IP group into ACL */
static mesa_rc _hw_ip_mac_ace_add(const mesa_vid_t vid,
                                    const mesa_mac_t *const mac) {
    acl_entry_conf_t conf;
    mesa_rc          rc;
    mesa_ip_t        dip = 0xE0000000 | (mac->addr[5]);

    dip |= (mac->addr[4]) << 8;
    dip |= (mac->addr[3] & 0x7f) << 16;

    /* Initialize ACE */
    rc = acl_mgmt_ace_init(MESA_ACE_TYPE_IPV4, &conf);
    if (rc != VTSS_OK) {
        W << "Add IP ACL rc "<< rc <<" failed: vid "<< vid << " IP "
            << vtss::AsIpv4(dip);
        return rc;
    }

    /* Update ACE */
    conf.isdx_disable = TRUE;
    conf.action.force_cpu = TRUE;
    conf.action.cpu_queue = PACKET_XTR_QU_IGMP;
    conf.action.cpu_once = FALSE;
    conf.isid = VTSS_ISID_LOCAL;
    conf.vid.value = vid;
    conf.vid.mask = 0xfff;
    conf.frame.ipv4.dip.value = dip;
    conf.frame.ipv4.dip.mask = 0xffffffff;

    I << "Add IP ACL "<< " vid "<< conf.vid.value << " IP "
            << vtss::AsIpv4(conf.frame.ipv4.dip.value) << "/"
            << vtss::AsIpv4(conf.frame.ipv4.dip.mask);
    /* Apply ACE */
    rc = acl_mgmt_ace_add(ACL_USER_IP, ACL_MGMT_ACE_ID_NONE, &conf);
    if (rc != VTSS_OK) {
        W << "Add IP ACL rc "<< rc <<" failed: vid "<< conf.vid.value << " IP "
            << vtss::AsIpv4(conf.frame.ipv4.dip.value) << "/"
            << vtss::AsIpv4(conf.frame.ipv4.dip.mask);
        return rc;
    }

    return VTSS_OK;
}

/* Delete multicast IP group from ACL */
static mesa_rc _hw_ip_mac_ace_del(const mesa_vid_t vid,
                                    const mesa_mac_t *const mac) {
    acl_entry_conf_t ace_conf;
    mesa_ace_id_t    id = ACL_MGMT_ACE_ID_NONE;
    mesa_rc          rc = VTSS_OK;
    mesa_ip_t        dip = 0xE0000000 | (mac->addr[5]);

    dip |= (mac->addr[4]) << 8;
    dip |= (mac->addr[3] & 0x7f) << 16;

    while (acl_mgmt_ace_get(ACL_USER_IP, VTSS_ISID_LOCAL, id, &ace_conf, NULL, 1) == VTSS_OK) {
        // Assign for next loop
        id = ace_conf.id;

        // Find the matched ACE ID
        if (ace_conf.vid.value != vid ||
            ace_conf.vid.mask != 0xfff ||
            ace_conf.frame.ipv4.dip.value != dip ||
            ace_conf.frame.ipv4.dip.mask != 0xffffffff)
            continue;

        D << "Found and del IP Group ACE ace id " << vtss::AsInt(ace_conf.id);
        rc = acl_mgmt_ace_del(ACL_USER_IP, ace_conf.id);
        if (rc != VTSS_OK && rc != VTSS_APPL_ACL_ERROR_ACE_NOT_FOUND) {
            W << "Del IP ACL failed: ace id " << ace_conf.id;
            return rc;
        }
    }

    return rc;
}

/* Check if the mac is ethernet multicast address */
static bool is_eth_multicast_address(const mesa_mac_t *const mac){

    if(mac->addr[0] == 0x01 && mac->addr[1] == 0x00 && mac->addr[2] == 0x5e
        && ((mac->addr[3] & 0x10) == 0x00)){
        return TRUE;
    }
    return FALSE;
}
#endif

/* We use the ACL rule to trap the multicase control packet
 * to CPU instead of MAC table entry because the solution won't
 * copy more unexpected multicast packet to CPU (both of control and data
 * packets).
 */
static mesa_rc _hw_ip_mac_subscribe(const mesa_vid_t vid,
                                    const mesa_mac_t *const mac) {
    mesa_rc    rc = VTSS_RC_OK;

#ifdef VTSS_SW_OPTION_ACL
    if (is_eth_multicast_address(mac)) {
        // 01005exxxxxx  Add to ACL
        I << "Add IP Group ACE, vid = " << vid << ", mac = " << *mac;
        rc = _hw_ip_mac_ace_add(vid, mac);
        return rc;
    }
#endif

    // Add to MAC table
    I << "Add to chip mac table, vid = " << vid << ", mac = " << *mac;
    rc = vtss_ip_chip_mac_subscribe(vid, mac);

    return rc;
}


static mesa_rc _hw_ip_mac_unsubscribe(const mesa_vid_t vid,
                                    const mesa_mac_t *const mac) {
    mesa_rc rc = VTSS_RC_OK;

#ifdef VTSS_SW_OPTION_ACL
    if (is_eth_multicast_address(mac)) {
        // 01005exxxxxx
        I << "Delete IP Group ACE, vid = " << vid << ", mac = " << *mac;
        rc = _hw_ip_mac_ace_del(vid,mac);
        return rc;
    }
#endif

    I << "Delete to chip mac table, vid = " << vid << ", mac = " << *mac;
    rc = vtss_ip_chip_mac_unsubscribe(vid, mac);

    return rc;
}

static vtss_if_id_t conv_os_ifindex_to_vtss_if_id_t(int i) {
    vtss_if_id_t res = {};
    SYNCHRONIZED(state) {  /////////////////////////////////////////////////////
        auto itr = state.os_ifindex_to_vlan.find(i);
        if (itr != state.os_ifindex_to_vlan.end()) {
            res.type = VTSS_ID_IF_TYPE_VLAN;
            res.u.vlan = itr->second;
        } else {
            res.type = VTSS_ID_IF_TYPE_OS_ONLY;
            res.u.os.ifno = i;
            snprintf(res.u.os.name, IF_NAMESIZE + 1, "os.idx.%d", i);
            res.u.os.name[IF_NAMESIZE] = 0;
        }
    }  /////////////////////////////////////////////////////////////////////////

    return res;
}

static vtss_ifindex_t conv_os_ifindex_to_vtss_ifindex(int i) {
    vtss_ifindex_t res = VTSS_IFINDEX_NONE;
    SYNCHRONIZED(state) {  /////////////////////////////////////////////////////
        auto itr = state.os_ifindex_to_vlan.find(i);
        if (itr != state.os_ifindex_to_vlan.end()) {
            (void)vtss_ifindex_from_vlan(itr->second, &res);
        }
    }  /////////////////////////////////////////////////////////////////////////

    return res;
}

static bool vlan_ipv6_network_match(mesa_vid_t vid, mesa_ipv6_t *addr)
{
    bool match = false;
    SYNCHRONIZED(state) {  /////////////////////////////////////////////////////
        auto itr = state.vid_to_ipv6_network.find(vid);
        if (itr != state.vid_to_ipv6_network.end() &&
            vtss_ipv6_net_include(&itr->second, addr)) {
            match = true;
        }
    }  /////////////////////////////////////////////////////////////////////////
    return match;
}

////////////////////////////////////////////////////////////////////////////////
struct ParseIfName : public vtss::parser::ParserBase {
    typedef uint16_t value_type;

    bool operator()(const char *&b, const char *e) {
        const char *_b = b;

        vtss::parser::Lit sep(VTSS_VLAN_IF_PREFIX);
        if (vtss::parser::Group(b, e, sep, vlan)) return true;

        b = _b;
        return false;
    }

    const value_type &get() const { return vlan.get(); }
    vtss::parser::Int<uint16_t, 10, 1, 4> vlan;
};

struct NetlinkCallbackCopyIfMacAddrList : public NetlinkCallbackAbstract {
    void operator()(struct sockaddr_nl *addr, struct nlmsghdr *n) {
        int len = n->nlmsg_len;

        N << "CB type: " << AsRtmType(n->nlmsg_type);
        N << n;
        if (n->nlmsg_type != RTM_NEWNEIGH && n->nlmsg_type != RTM_GETNEIGH)
            return;

        struct ndmsg *ndm = (struct ndmsg *)NLMSG_DATA(n);
        len -= NLMSG_LENGTH(sizeof(*ndm));
        if (len < 0) {
            E << " <msg too short for this type!>";
            return;
        }

        if ((!ndm->ndm_flags) & NTF_SELF) {
            // not for us
            return;
        }

        struct rtattr *rta = NDM_RTA(ndm);

        while (RTA_OK(rta, len)) {
            switch (rta->rta_type) {
            case NDA_LLADDR: {
                if (RTA_PAYLOAD(rta) != 6) {
                    E << "Unexpected size";
                    break;
                }

                unsigned char *m = (unsigned char *)RTA_DATA(rta);
                std::pair<int, mesa_mac_t> d;
                d.first = ndm->ndm_ifindex;
                for (int i = 0; i < 6; ++i) d.second.addr[i] = m[i];
                data.emplace(d);
                break;
            }

            default:;
            }

            rta = RTA_NEXT(rta, len);
        }
    }

    Set<std::pair<int, mesa_mac_t>> data;
};

struct NetlinkCallbackCopyIpv4NeighborList : public NetlinkCallbackAbstract {
    void operator()(struct sockaddr_nl *addr, struct nlmsghdr *n) {
        struct rtattr *rta;
        bool ip_valid = false;
        bool mac_valid = false;
        Ipv4Neighbor nbr;
        int len = n->nlmsg_len;

        N << "CB type: " << AsRtmType(n->nlmsg_type);
        N << n;
        if (n->nlmsg_type != RTM_NEWNEIGH)
            return;

        struct ndmsg *ndm = (struct ndmsg *)NLMSG_DATA(n);
        len -= NLMSG_LENGTH(sizeof(*ndm));
        if (len < 0) {
            E << " <msg too short for this type!>";
            return;
        }

        if ((!ndm->ndm_flags) & NTF_SELF) {
            // Not for us
            return;
        }

        for (rta = NDM_RTA(ndm); RTA_OK(rta, len); rta = RTA_NEXT(rta, len)) {
            unsigned char *m = (unsigned char *)RTA_DATA(rta);
            switch (rta->rta_type) {
            case NDA_DST:
                if (RTA_PAYLOAD(rta) == 4) {
                    nbr.data.ip_address = ((m[0] << 24) | (m[1] << 16) | (m[2] << 8) | m[3]);
                    ip_valid = true;
                } else {
                    E << "Unexpected size";
                }
                break;

            case NDA_LLADDR:
                if (RTA_PAYLOAD(rta) == 6) {
                    for (int i = 0; i < 6; i++) {
                        nbr.data.status.mac_address.addr[i] = m[i];
                    }
                    mac_valid = true;
                } else {
                    E << "Unexpected size";
                }
                break;

            default:;
            }
        }
        if (ip_valid && mac_valid &&
            (nbr.data.status.interface = conv_os_ifindex_to_vtss_ifindex(ndm->ndm_ifindex)) != VTSS_IFINDEX_NONE) {
            nbr.if_id = conv_os_ifindex_to_vtss_if_id_t(ndm->ndm_ifindex);
            nbr.if_index = ndm->ndm_ifindex;
            data.emplace(nbr);
        }
    }
    Set<Ipv4Neighbor> data;
};

struct NetlinkCallbackCopyIpv6NeighborList : public NetlinkCallbackAbstract {
    void operator()(struct sockaddr_nl *addr, struct nlmsghdr *n) {
        struct rtattr *rta;
        bool ip_valid = false;
        bool mac_valid = false;
        Ipv6Neighbor nbr;
        mesa_ipv6_t *dip = &nbr.data.key.ip_address;
        int len = n->nlmsg_len;

        N << "CB type: " << AsRtmType(n->nlmsg_type);
        N << n;
        if (n->nlmsg_type != RTM_NEWNEIGH)
            return;

        struct ndmsg *ndm = (struct ndmsg *)NLMSG_DATA(n);
        len -= NLMSG_LENGTH(sizeof(*ndm));
        if (len < 0) {
            E << " <msg too short for this type!>";
            return;
        }

        if ((!ndm->ndm_flags) & NTF_SELF) {
            // Not for us
            return;
        }

        if (ndm->ndm_state & NUD_NOARP) {
            // Multicast address or similar
            return;
        }

        for (rta = NDM_RTA(ndm); RTA_OK(rta, len); rta = RTA_NEXT(rta, len)) {
            unsigned char *m = (unsigned char *)RTA_DATA(rta);
            switch (rta->rta_type) {
            case NDA_DST:
                if (RTA_PAYLOAD(rta) == 16) {
                    for (int i = 0; i < 16; i++) {
                        dip->addr[i] = m[i];
                        ip_valid = true;
                    }
                } else {
                    E << "Unexpected size";
                }
                break;

            case NDA_LLADDR:
                if (RTA_PAYLOAD(rta) == 6) {
                    for (int i = 0; i < 6; i++) {
                        nbr.data.status.mac_address.addr[i] = m[i];
                    }
                    mac_valid = true;
                } else {
                    E << "Unexpected size";
                }
                break;

            default:;
            }
        }
        if (ip_valid && mac_valid &&
            (nbr.data.status.interface = conv_os_ifindex_to_vtss_ifindex(ndm->ndm_ifindex)) != VTSS_IFINDEX_NONE) {
            nbr.data.key.interface = nbr.data.status.interface;
            nbr.if_id = conv_os_ifindex_to_vtss_if_id_t(ndm->ndm_ifindex);
            nbr.if_index = ndm->ndm_ifindex;
            if (vtss_ipv6_addr_is_link_local(dip) ||
                (nbr.if_id.type == VTSS_ID_IF_TYPE_VLAN && vlan_ipv6_network_match(nbr.if_id.u.vlan, dip))) {
                // Only add neighbour if link-local address or IPv6 network match
                data.emplace(nbr);
            }
        }
    }
    Set<Ipv6Neighbor> data;
};

static u32 ip_u8_to_u32(u8 *p)
{
    return ((p[0] << 24) | (p[1] << 16) | (p[2] << 8) | p[3]);
}

struct NetlinkCallbackCopyIfIpv4AddrList : public NetlinkCallbackAbstract {
    void operator()(struct sockaddr_nl *addr, struct nlmsghdr *n) {
        int len = n->nlmsg_len;

        N << "CB type: " << AsRtmType(n->nlmsg_type);
        if (n->nlmsg_type != RTM_NEWADDR && n->nlmsg_type != RTM_GETADDR)
            return;

        struct ifaddrmsg *ifa = (struct ifaddrmsg *)NLMSG_DATA(n);
        len -= NLMSG_LENGTH(sizeof(*ifa));
        if (len < 0) {
            E << " <msg too short for this type!>";
            return;
        }

        bool got_addr = false;
        std::tuple<vtss_ifindex_t, mesa_ipv4_network_t> key;
        vtss_appl_ip_if_ipv4_info_t val;

        struct rtattr *rta = IFA_RTA(ifa);
        while (RTA_OK(rta, len)) {
            switch (rta->rta_type) {
            case IFA_ADDRESS: {
                if (RTA_PAYLOAD(rta) != 4) {
                    E << "Unexpected size";
                    break;
                }
                std::get<0>(key) = conv_os_ifindex_to_vtss_ifindex(ifa->ifa_index);
                std::get<1>(key).prefix_size = ifa->ifa_prefixlen;
                std::get<1>(key).address = ip_u8_to_u32((u8 *)RTA_DATA(rta));
                got_addr = true;
                break;
            }

            case IFA_BROADCAST: {
                if (RTA_PAYLOAD(rta) != 4) {
                    E << "Unexpected size";
                    break;
                }
                val.broadcast = ip_u8_to_u32((u8 *)RTA_DATA(rta));
                break;
            }

            default:;
            }

            rta = RTA_NEXT(rta, len);
        }

        if (got_addr) {
            data.emplace(key, val);
        }
    }

    Map<std::tuple<vtss_ifindex_t, mesa_ipv4_network_t>, vtss_appl_ip_if_ipv4_info_t> data;
};

struct NetlinkCallbackCopyIpv4RouteTable : public NetlinkCallbackAbstract {
    void operator()(struct sockaddr_nl *addr, struct nlmsghdr *n) {
        int len = n->nlmsg_len;

        if (n->nlmsg_type != RTM_NEWROUTE && n->nlmsg_type != RTM_GETROUTE)
            return;

        struct rtmsg *rtm = (struct rtmsg *)NLMSG_DATA(n);
        len -= NLMSG_LENGTH(sizeof(*rtm));
        if (len < 0) {
            E << " <msg too short for this type!>";
            return;
        }

        int oif = 0;
        bool got_dst = false;
        bool got_gateway = false;
        mesa_ipv4_uc_t k = {};
        vtss_appl_ip_route_status_t v = {};

        struct rtattr *rta = RTM_RTA(rtm);
        while (RTA_OK(rta, len)) {
            switch (rta->rta_type) {
            case RTA_DST: {
                if (RTA_PAYLOAD(rta) != 4) {
                    E << "Unexpected size";
                    break;
                }

                unsigned char *m = (unsigned char *)RTA_DATA(rta);
                k.network.prefix_size = rtm->rtm_dst_len;
                k.network.address = 0;
                k.network.address |= m[0];
                k.network.address <<= 8;
                k.network.address |= m[1];
                k.network.address <<= 8;
                k.network.address |= m[2];
                k.network.address <<= 8;
                k.network.address |= m[3];
                got_dst = true;
                break;
            }

            case RTA_GATEWAY: {
                if (RTA_PAYLOAD(rta) != 4) {
                    E << "Unexpected size";
                    break;
                }

                unsigned char *m = (unsigned char *)RTA_DATA(rta);
                k.destination = 0;
                k.destination |= m[0];
                k.destination <<= 8;
                k.destination |= m[1];
                k.destination <<= 8;
                k.destination |= m[2];
                k.destination <<= 8;
                k.destination |= m[3];

                got_gateway = true;
                break;
            }

            case RTA_OIF: {
                if (RTA_PAYLOAD(rta) != 4) {
                    E << "Unexpected size";
                    break;
                }

                int *x = (int *)RTA_DATA(rta);
                oif = *x;
                break;
            }

            case RTA_TABLE: {
                if (RTA_PAYLOAD(rta) != 4) {
                    E << "Unexpected size";
                    break;
                }

                int *x = (int *)RTA_DATA(rta);

                // Filtering out loop-back routes
                if (*x != RT_TABLE_MAIN) return;

                break;
            }

            default:;
            }

            rta = RTA_NEXT(rta, len);
        }

        if (got_gateway) {
            v.flags |= VTSS_APPL_IP_ROUTE_STATUS_FLAG_GATEWAY;
        } else if (!got_dst) {
            return;
        }

        // Pretty sure all routes is up - they will be deleted by the kernel
        // otherwise
        v.flags |= VTSS_APPL_IP_ROUTE_STATUS_FLAG_UP;

        if ((v.next_hop_interface = conv_os_ifindex_to_vtss_ifindex(oif)) != VTSS_IFINDEX_NONE) {
            data.emplace(k, v);
       }
    }

    Map<mesa_ipv4_uc_t, vtss_appl_ip_route_status_t> data;
};

struct NetlinkCallbackCopyIpv6RouteTable : public NetlinkCallbackAbstract {
    void operator()(struct sockaddr_nl *addr, struct nlmsghdr *n) {
        int len = n->nlmsg_len;

        if (n->nlmsg_type != RTM_NEWROUTE && n->nlmsg_type != RTM_GETROUTE)
            return;

        struct rtmsg *rtm = (struct rtmsg *)NLMSG_DATA(n);
        len -= NLMSG_LENGTH(sizeof(*rtm));
        if (len < 0) {
            E << " <msg too short for this type!>";
            return;
        }

        int oif = 0;
        bool got_dst = false;
        bool got_gateway = false;
        vtss_appl_ip_ipv6_route_conf_t k = {};
        vtss_appl_ip_route_status_t v = {};

        struct rtattr *rta = RTM_RTA(rtm);
        while (RTA_OK(rta, len)) {
            switch (rta->rta_type) {
            case RTA_DST: {
                if (RTA_PAYLOAD(rta) != 16) {
                    E << "Unexpected size";
                    break;
                }

                unsigned char *m = (unsigned char *)RTA_DATA(rta);
                k.route.network.prefix_size = rtm->rtm_dst_len;
                memcpy(k.route.network.address.addr, m, 16);
                if (!vtss_ipv6_addr_is_link_local(&k.route.network.address) &&
                    !vtss_ipv6_addr_is_multicast(&k.route.network.address)) {
                    got_dst = true;
                }
                break;
            }

            case RTA_GATEWAY: {
                if (RTA_PAYLOAD(rta) != 16) {
                    E << "Unexpected size";
                    break;
                }

                unsigned char *m = (unsigned char *)RTA_DATA(rta);
                memcpy(k.route.destination.addr, m, 16);
                got_gateway = true;
                break;
            }

            case RTA_OIF: {
                if (RTA_PAYLOAD(rta) != 4) {
                    E << "Unexpected size";
                    break;
                }

                int *x = (int *)RTA_DATA(rta);
                oif = *x;
                break;
            }

            default:;
            }

            rta = RTA_NEXT(rta, len);
        }

        if (got_gateway) {
            v.flags |= VTSS_APPL_IP_ROUTE_STATUS_FLAG_GATEWAY;
        } else if (!got_dst) {
            return;
        }

        // Pretty sure all routes is up - they will be deleted by the kernel
        // otherwise
        v.flags |= VTSS_APPL_IP_ROUTE_STATUS_FLAG_UP;

        if ((v.next_hop_interface = conv_os_ifindex_to_vtss_ifindex(oif)) != VTSS_IFINDEX_NONE) {
            data.emplace(k, v);
        }
    }

    Map<vtss_appl_ip_ipv6_route_conf_t, vtss_appl_ip_route_status_t> data;
};

struct NetlinkCallbackCopyIfIpv6AddrList : public NetlinkCallbackAbstract {
    void operator()(struct sockaddr_nl *addr, struct nlmsghdr *n) {
        int len = n->nlmsg_len;

        N << "CB type: " << AsRtmType(n->nlmsg_type);
        if (n->nlmsg_type != RTM_NEWADDR && n->nlmsg_type != RTM_GETADDR)
            return;

        struct ifaddrmsg *ifa = (struct ifaddrmsg *)NLMSG_DATA(n);
        len -= NLMSG_LENGTH(sizeof(*ifa));
        if (len < 0) {
            E << " <msg too short for this type!>";
            return;
        }

        bool got_addr = false;
        std::tuple<vtss_ifindex_t, mesa_ipv6_network_t> key;
        vtss_appl_ip_if_ipv6_info_t val;

        struct rtattr *rta = IFA_RTA(ifa);
        while (RTA_OK(rta, len)) {
            switch (rta->rta_type) {
            case IFA_ADDRESS: {
                if (RTA_PAYLOAD(rta) != 16) {
                    E << "Unexpected size";
                    break;
                }

                std::get<0>(key) = conv_os_ifindex_to_vtss_ifindex(ifa->ifa_index);
                std::get<1>(key).prefix_size = ifa->ifa_prefixlen;
                val.os_if_index = ifa->ifa_index;
                val.flags = VTSS_APPL_IP_IF_IPV6_FLAG_NONE;

#define F(X, Y) \
    if ((X)&ifa->ifa_flags) val.flags |= Y
                F(IFA_F_NODAD, VTSS_APPL_IP_IF_IPV6_FLAG_NODAD);
                F(IFA_F_TENTATIVE, VTSS_APPL_IP_IF_IPV6_FLAG_TENTATIVE);
                F(IFA_F_DEPRECATED, VTSS_APPL_IP_IF_IPV6_FLAG_DEPRECATED);
#undef F
                memcpy(std::get<1>(key).address.addr, RTA_DATA(rta), 16);
                got_addr = true;
                break;
            }

            // case IFA_FLAGS:
            // Future flags are placed in this 32bit field - ifa->ifa_flags
            // is only 8bit

            default:;
            }

            rta = RTA_NEXT(rta, len);
        }

        if (got_addr) {
            data.emplace(key, val);
        }
    }

    Map<std::tuple<vtss_ifindex_t, mesa_ipv6_network_t>, vtss_appl_ip_if_ipv6_info_t> data;
};

struct LinkStateCallbackPoll : public NetlinkCallbackAbstract {
    void operator()(struct sockaddr_nl *addr, struct nlmsghdr *n) {
        int len = n->nlmsg_len;

        N << "CB type: " << AsRtmType(n->nlmsg_type);
        if (n->nlmsg_type != RTM_GETLINK && n->nlmsg_type != RTM_NEWLINK)
            return;

        struct ifinfomsg *ifi = (struct ifinfomsg *)NLMSG_DATA(n);
        len -= NLMSG_LENGTH(sizeof(*ifi));
        if (len < 0) {
            E << "Msg too short for this type!";
            return;
        }

        vtss_appl_ip_if_status_link_t st = {};
        st.os_if_index = ifi->ifi_index;
#define F(X, Y) \
    if ((X)&ifi->ifi_flags) st.flags |= Y;
            F(IFF_LOWER_UP, VTSS_APPL_IP_IF_LINK_FLAG_UP);
            F(IFF_BROADCAST, VTSS_APPL_IP_IF_LINK_FLAG_BROADCAST);
            F(IFF_LOOPBACK, VTSS_APPL_IP_IF_LINK_FLAG_LOOPBACK);
            F(IFF_NOARP, VTSS_APPL_IP_IF_LINK_FLAG_NOARP);
            F(IFF_PROMISC, VTSS_APPL_IP_IF_LINK_FLAG_PROMISC);
            F(IFF_MULTICAST, VTSS_APPL_IP_IF_LINK_FLAG_MULTICAST);
#undef F

        mesa_vid_t vid = 0;
        bool has_mac = false;
        bool has_bcast = false;
        vtss_appl_ip_if_link_flag_t flags = VTSS_APPL_IP_IF_LINK_FLAG_NONE;
        vtss_ifindex_t idx;

        struct rtattr *rta = IFLA_RTA(ifi);
        while (RTA_OK(rta, len)) {
            switch (rta->rta_type) {
            case IFLA_IFNAME: {
                const char *n = (const char *)RTA_DATA(rta);
                const char *e = n + RTA_PAYLOAD(rta);
                ParseIfName p;

                if (p(n, e)) {
                    N << "Vlan: " << p.get();
                    vid = p.get();
                } else {
                    N << "Not a vlan interface: " << vtss::str(n, e);
                }

                break;
            }

            case IFLA_ADDRESS: {
                if (RTA_PAYLOAD(rta) == 6) {
                    memcpy(st.mac.addr, RTA_DATA(rta), 6);
                    has_mac = true;
                } else {
                    D << "Un-expected address length: " << RTA_PAYLOAD(rta);
                }
                break;
            }

            case IFLA_BROADCAST: {
                if (RTA_PAYLOAD(rta) == 6) {
                    memcpy(st.bcast.addr, RTA_DATA(rta), 6);
                    has_bcast = true;
                } else {
                    D << "Un-expected address length: " << RTA_PAYLOAD(rta);
                }
                break;
            }

            case IFLA_MTU: {
                if (RTA_PAYLOAD(rta) == 4) {
                    int *i = (int *)RTA_DATA(rta);
                    st.mtu = *i;
                } else {
                    D << "Unexpected length: " << RTA_PAYLOAD(rta);
                }
                break;
            }

            case IFLA_PROTINFO: {
                struct rtattr *rta1 = (rtattr *)RTA_DATA(rta);
                int len1 = rta->rta_len;
                N << "IFLA_PROTINFO";
                while (RTA_OK(rta1, len1)) {
                    if (rta1->rta_type == IFLA_INET6_FLAGS) {
                        uint32_t ipv6_flags = *(uint32_t *)RTA_DATA(rta1);
                        D << "Index: " << ifi->ifi_index << ", IPv6 flags: " << ipv6_flags;
                        if (ipv6_flags & 0x40 /* IF_RA_MANAGED */) {
                            flags |= VTSS_APPL_IP_IF_LINK_FLAG_IPV6_RA_MANAGED;
                        }
                        if (ipv6_flags & 0x80 /* IF_RA_OTHERCONF */) {
                            flags |= VTSS_APPL_IP_IF_LINK_FLAG_IPV6_RA_OTHER;
                        }
                    }
                    rta1 = RTA_NEXT(rta1, len1);
                }
                break;
            }
            default:;
            }

            rta = RTA_NEXT(rta, len);
        }

        if (vid && vtss_ifindex_from_vlan(vid, &idx) == VTSS_RC_OK) {
            if (has_mac && has_bcast) {
                if (!data.set(idx, st)) {
                    E << "Failed to insert - out-of-memory";
                    ok_ = false;
                } else {
                    D << "Link: " << idx << " "  << st;
                }
            } else if (flags) {
                auto itr = data.find(idx);
                if (itr != data.end()) {
                    st = itr->second;
                    st.flags |= flags;
                    D << "vid: " << vid << ", flags: " << flags;
                    data.set(idx, st);
                }
            }
        } else {
            D << "Skipping invalid entry " << vid << " "
              << has_mac << " " << has_bcast;
        }
    }

    bool ok() const { return ok_; }

    // Cache is needed because we must delete before adding!
    bool ok_ = true;
    Map<vtss_ifindex_t, vtss_appl_ip_if_status_link_t> data;
};

struct LinkStateChipCallback : public StatusIfLink::Callback {
    LinkStateChipCallback(State &_s) : s(_s) {}

    void add(const vtss_ifindex_t &i,
             const vtss_appl_ip_if_status_link_t &st) override {
        mesa_vid_t vid;
        mesa_rc rc = VTSS_RC_OK;

        if (IP_ifindex_to_vlan(i, &vid) != VTSS_RC_OK) {
            N << "Not a vlan";
            return;
        }

        I << "Adding Vlan interface " << vid;
        DO_RECOVER(ERROR0, vtss_ip_chip_rleg_add, vid);
        DO_RECOVER(ERROR1, _hw_ip_mac_subscribe, vid, &st.bcast);
        DO_RECOVER(ERROR2, _hw_ip_mac_subscribe, vid, &st.mac);

        s.os_ifindex_to_vlan.set(st.os_if_index, vid);

        return;

ERROR2:
        (void)_hw_ip_mac_unsubscribe(vid, &st.bcast);

ERROR1:
        (void)vtss_ip_chip_rleg_del(vid);

ERROR0:
        I << "Failed to add Vlan interface " << vid;
    }

    void mod(const vtss_ifindex_t &i,
             const vtss_appl_ip_if_status_link_t &before,
             vtss_appl_ip_if_status_link_t &after) override {
        mesa_vid_t vid;
        mesa_rc rc = VTSS_RC_OK;
        vtss_appl_ip_if_link_flag_t mo_flg = VTSS_APPL_IP_IF_LINK_FLAG_IPV6_RA_MANAGED |
                                             VTSS_APPL_IP_IF_LINK_FLAG_IPV6_RA_OTHER;

        if (IP_ifindex_to_vlan(i, &vid) != VTSS_RC_OK) {
            D << "Not a vlan";
            return;
        }

        if (before.mac != after.mac) {
            I << "Vlan interface " << vid << " update mac address";
            DO_(_hw_ip_mac_unsubscribe, vid, &before.mac);
            DO_(_hw_ip_mac_subscribe, vid, &after.mac);
        }

        if (before.bcast != after.bcast) {
            I << "Vlan interface " << vid << " update broadcast address";
            DO_(_hw_ip_mac_unsubscribe, vid, &before.bcast);
            DO_(_hw_ip_mac_subscribe, vid, &after.bcast);
        }

        if ((before.flags & mo_flg) != (after.flags & mo_flg)) {
            I << "MO flags changed old=" << before.flags << " new =" << after.flags;
            vtss_ip_if_mo_flag_update(vid);
        }
        return;
    }

    void del(const vtss_ifindex_t &i,
             const vtss_appl_ip_if_status_link_t &st) override {
        mesa_vid_t vid;
        mesa_rc rc = VTSS_RC_OK;

        if (IP_ifindex_to_vlan(i, &vid) != VTSS_RC_OK) {
            D << "Not a vlan";
            return;
        }

        I << "Deleting Vlan interface " << vid;
        DO_(_hw_ip_mac_unsubscribe, vid, &st.mac);
        DO_(_hw_ip_mac_unsubscribe, vid, &st.bcast);
        DO_(vtss_ip_chip_rleg_del, vid);

        for (auto i = s.mac_addr_list.begin(); i != s.mac_addr_list.end();) {
            if (i->vid != vid) {
                ++i;
            } else {
                auto m = i->mac;
                s.mac_addr_list.erase(i++);
                DO_(_hw_ip_mac_unsubscribe, vid, &m);
            }
        }

        s.os_ifindex_to_vlan.erase(st.os_if_index);

        return;
    }

    State &s;
};

static void poll_link_state() {
    LinkStateCallbackPoll cb;

    if (!cb.ok()) {
        E << "Out of memory";
        return;
    }

    D << "Polling link state";
    nl_req_link_dump(&cb);
    nl_req_link_ipv6_dump(&cb);

    SYNCHRONIZED(state) {  /////////////////////////////////////////////////////
        LinkStateChipCallback cb2(state);
        status_if_link.set(cb.data, cb2);
    }  // SYNCHRONIZED /////////////////////////////////////////////////////////
    D << "Polling link state - done";
}



static void poll_link_mac_address_list() {
    mesa_rc rc;
    NetlinkCallbackCopyIfMacAddrList p;

    D << "Polling interface mac-address list";
    nl_req_neigh_dump(&p, AF_BRIDGE);

    Set<VlanMacKey> pending;
    Set<VlanMacKey> data_;

    SYNCHRONIZED(state) {  /////////////////////////////////////////////////////
        for (auto &e : p.data) {
            // convert data.first from ifindex to vlan
            auto itr = state.os_ifindex_to_vlan.find(e.first);
            if (itr == state.os_ifindex_to_vlan.end()) continue;
            VlanMacKey d;
            d.vid = itr->second;
            d.mac = e.second;
            data_.emplace(d);
        }
        p.data.clear();

        auto cache = state.mac_addr_list.begin();
        auto cache_end = state.mac_addr_list.end();

        auto data = data_.begin();
        auto data_end = data_.end();

        while (cache != cache_end || data != data_end) {
            if (data != data_end) {
                D << "data: " << *data;
            } else {
                D << "data: undef";
            }

            if (cache != cache_end) {
                D << "cache: " << *cache;
            } else {
                D << "cache: undef";
            }

            if (data == data_end || (cache != cache_end && *cache < *data)) {
                // Entry found in cache but not in data -> DELETE EVENT
                rc = _hw_ip_mac_unsubscribe(cache->vid, &cache->mac);
                if (rc == VTSS_RC_OK) {
                    I << "unsubscribe: " << *cache;
                } else {
                    E << "Failed to unsubscribe: " << *cache;
                }
                state.mac_addr_list.erase(cache++);

            } else if (cache == cache_end ||
                       (data != data_end && *data < *cache)) {
                // Entry found in data but not in cache -> ADD EVENT
                rc = _hw_ip_mac_subscribe(data->vid, &data->mac);
                if (rc == VTSS_RC_OK) {
                    I << "subscribe: " << *data;
                    pending.emplace(*data);
                } else {
                    E << "Failed to subscribe: " << *data;
                }
                data++;

            } else {
                // Entry found in both -> dont care
                D << "Found in both";
                data++;
                cache++;
            }
        }

        for (const auto &e : pending) state.mac_addr_list.insert(e);
    }  // SYNCHRONIZED /////////////////////////////////////////////////////////
}

static bool ipv6_neighbor_reachable(const vtss_appl_ip_ipv6_route_conf_t *conf,
                                    const vtss_appl_ip_route_status_t *st)
{
    SYNCHRONIZED(state) {  /////////////////////////////////////////////////////
        auto i = state.ipv6_neighbor_list.begin();
        auto e = state.ipv6_neighbor_list.end();

        for ( ; i != e; i++) {
            if (i->data.key.ip_address == conf->route.destination &&
                i->data.key.interface == st->next_hop_interface) {
                return true;
            }
        }
    }
    return false;
}

// Router preference flags, giving a preference number in range 0-4
#define IP_ROUTE_PREF_HW        0x01
#define IP_ROUTE_PREF_REACHABLE 0x02
#define IP_ROUTE_PREF_DIRECT    0x04

/* Update IPv6 route entries based on Linux IPv6 route table and neighbor table.
   Add one route per destination network with this precedence:
   4: Direct routes
   3: Routes via reachable router already installed
   2: Routes via reachable router not already installed
   1: Routes via unreachable router already installed
   0: Routes via unreachable router not already installed
*/
static void ipv6_route_update(void)
{
    vtss::notifications::LockGlobalSubject lock;
    auto i = status_rt_ipv6.begin(lock), i_prev = i;
    auto e = status_rt_ipv6.end(lock), i_best = e, i_del = e;
    bool done = false;
    u8   pref, best = 0;

    while (!done) {
        if (i == e) {
            I << "IPv6 route update done";
            done = true;
        } else {
            I << "IPv6 route process: " << i->first;
            if (i->first.route.network == i_prev->first.route.network) {
                // Same network, calculate route preference
                if (i->second.flags & VTSS_APPL_IP_ROUTE_STATUS_FLAG_GATEWAY) {
                    // Indirect route via router
                    pref = 0;
                    if (i->second.flags & VTSS_APPL_IP_ROUTE_STATUS_FLAG_HW_RT) {
                        // Hardware route is installed
                        pref |= IP_ROUTE_PREF_HW;
                    }
                    if (ipv6_neighbor_reachable(&i->first, &i->second)) {
                        // Router is reachable
                        pref |= IP_ROUTE_PREF_REACHABLE;
                    }
                } else {
                    // Direct route
                    pref = IP_ROUTE_PREF_DIRECT;
                }
                I << "IPv6 route preference: " << pref;
                if (pref == IP_ROUTE_PREF_HW) {
                    // Unreachable route in hardware is a delete candidate
                    i_del = i;
                }
                if (pref >= best) {
                    best = pref;
                    i_best = i;
                }
                i_prev = i;
                i++;
                continue;
            }
        }

        // Different networt or no more networks, install route if not already there
        if (i_best != e && !(i_best->second.flags & VTSS_APPL_IP_ROUTE_STATUS_FLAG_HW_RT)) {
            mesa_routing_entry_t        rt;
            vtss_appl_ip_route_status_t st;

            rt.type = MESA_ROUTING_ENTRY_TYPE_IPV6_UC;
            rt.route.ipv6_uc = i_best->first.route;
            if (IP_ifindex_to_vlan(i_best->second.next_hop_interface, &rt.vlan) == MESA_RC_OK) {
                if (i_del != e && (best > IP_ROUTE_PREF_HW)) {
                    I << "IPv6 route del: " << i_del->first;
                    vtss_ip_chip_route_del(&rt);
                    st = i_del->second;
                    st.flags &= ~VTSS_APPL_IP_ROUTE_STATUS_FLAG_HW_RT;
                    status_rt_ipv6.set(&i_del->first, &st);
                }
                I << "IPv6 route add: " << i_best->first;
                if (vtss_ip_chip_route_add(&rt) == MESA_RC_OK) {
                    st = i_best->second;
                    st.flags |= VTSS_APPL_IP_ROUTE_STATUS_FLAG_HW_RT;
                    status_rt_ipv6.set(&i_best->first, &st);
                }
            }
        }

        if (!done) {
            best = 0;
            i_del = e;
            i_best = i;
            i_prev = i;
            i++;
        }
    }
}

/* Add/delete neighbour entry and host route */
static mesa_rc ip_nbr_cmd(vtss_neighbour_t *nbr, BOOL add)
{
    mesa_rc              rc;
    mesa_routing_entry_t rt;
    bool                 add_route = true;

    if (nbr->dip.type == MESA_IP_TYPE_IPV4) {
        rt.type = MESA_ROUTING_ENTRY_TYPE_IPV4_UC;
        rt.route.ipv4_uc.network.address = nbr->dip.addr.ipv4;
        rt.route.ipv4_uc.network.prefix_size = 32;
        rt.route.ipv4_uc.destination = nbr->dip.addr.ipv4;
    } else if (vtss_ipv6_addr_is_link_local(&nbr->dip.addr.ipv6)) {
        /* Avoid adding route entries for link-local addresses */
        add_route = false;
    } else {
        rt.type = MESA_ROUTING_ENTRY_TYPE_IPV6_UC;
        rt.route.ipv6_uc.network.address = nbr->dip.addr.ipv6;
        rt.route.ipv6_uc.network.prefix_size = 128;
        rt.route.ipv6_uc.destination = nbr->dip.addr.ipv6;
    }
    if (add) {
        rc = (add_route ? vtss_ip_chip_route_add(&rt) : VTSS_RC_OK);
        if (rc == VTSS_RC_OK &&
            (rc = vtss_ip_chip_neighbour_add(nbr)) != VTSS_RC_OK &&
            add_route) {
            /* Neighbour add failed, delete route again */
            (void)vtss_ip_chip_route_del(&rt);
        }
    } else {
        if ((rc = vtss_ip_chip_neighbour_del(nbr)) == VTSS_RC_OK && add_route) {
            rc = vtss_ip_chip_route_del(&rt);
        }
    }
    return rc;
}

static void poll_ipv4_neighbor_list()
{
    NetlinkCallbackCopyIpv4NeighborList p;
    vtss_neighbour_t                    nbr;

    D << "Polling IPv4 neighbor list";
    nl_req_neigh_dump(&p, AF_INET);

    Set<Ipv4Neighbor> pending;

    nbr.dip.type = MESA_IP_TYPE_IPV4;
    SYNCHRONIZED(state) {  /////////////////////////////////////////////////////

        auto cache = state.ipv4_neighbor_list.begin();
        auto cache_end = state.ipv4_neighbor_list.end();
        auto data = p.data.begin();
        auto data_end = p.data.end();

        while (cache != cache_end || data != data_end) {
            if (data != data_end) {
                D << "data: " << *data;
            } else {
                D << "data: undef";
            }

            if (cache != cache_end) {
                D << "cache: " << *cache;
            } else {
                D << "cache: undef";
            }

            if (data == data_end || (cache != cache_end && *cache < *data)) {
                // Entry found in cache but not in data -> DELETE EVENT
                nbr.dmac = cache->data.status.mac_address;
                nbr.vlan = cache->if_id.u.vlan;
                nbr.dip.addr.ipv4 = cache->data.ip_address;
                if (ip_nbr_cmd(&nbr, FALSE) == VTSS_RC_OK) {
                    D << "delete: " << *cache;
                } else {
                    I << "Failed to delete: " << *cache;
                }
                state.ipv4_neighbor_list.erase(cache++);
            } else if (cache == cache_end || (data != data_end && *data < *cache)) {
                // Entry found in data but not in cache -> ADD EVENT
                nbr.dmac = data->data.status.mac_address;
                nbr.vlan = data->if_id.u.vlan;
                nbr.dip.addr.ipv4 = data->data.ip_address;
                if (ip_nbr_cmd(&nbr, TRUE) == VTSS_RC_OK) {
                    D << "add: " << *data;
                    pending.emplace(*data);
                } else {
                    I << "Failed to add: " << *data;
                }
                data++;
            } else {
                // Entry found in both -> dont care
                D << "Found in both";
                data++;
                cache++;
            }
        }

        p.data.clear();
        for (const auto &e : pending) state.ipv4_neighbor_list.insert(e);
    }  // SYNCHRONIZED /////////////////////////////////////////////////////////
}

static void poll_ipv6_neighbor_list()
{
    NetlinkCallbackCopyIpv6NeighborList p;
    vtss_neighbour_t                    nbr;

    D << "Polling IPv6 neighbor list";
    nl_req_neigh_dump(&p, AF_INET6);

    Set<Ipv6Neighbor> pending;

    nbr.dip.type = MESA_IP_TYPE_IPV6;
    SYNCHRONIZED(state) {  /////////////////////////////////////////////////////

        auto cache = state.ipv6_neighbor_list.begin();
        auto cache_end = state.ipv6_neighbor_list.end();
        auto data = p.data.begin();
        auto data_end = p.data.end();

        while (cache != cache_end || data != data_end) {
            if (data != data_end) {
                D << "data: " << *data;
            } else {
                D << "data: undef";
            }

            if (cache != cache_end) {
                D << "cache: " << *cache;
            } else {
                D << "cache: undef";
            }

            if (data == data_end || (cache != cache_end && *cache < *data)) {
                // Entry found in cache but not in data -> DELETE EVENT
                nbr.dmac = cache->data.status.mac_address;
                nbr.vlan = cache->if_id.u.vlan;
                nbr.dip.addr.ipv6 = cache->data.key.ip_address;
                if (ip_nbr_cmd(&nbr, FALSE) == VTSS_RC_OK) {
                    I << "delete: " << *cache;
                } else {
                    E << "Failed to delete: " << *cache;
                }
                state.ipv6_neighbor_list.erase(cache++);
            } else if (cache == cache_end || (data != data_end && *data < *cache)) {
                // Entry found in data but not in cache -> ADD EVENT
                nbr.dmac = data->data.status.mac_address;
                nbr.vlan = data->if_id.u.vlan;
                nbr.dip.addr.ipv6 = data->data.key.ip_address;
                if (ip_nbr_cmd(&nbr, TRUE) == VTSS_RC_OK) {
                    I << "add: " << *data;
                    pending.emplace(*data);
                } else {
                    E << "Failed to add: " << *data;
                }
                data++;
            } else {
                // Entry found in both -> dont care
                D << "Found in both";
                data++;
                cache++;
            }
        }

        p.data.clear();
        for (const auto &e : pending) state.ipv6_neighbor_list.insert(e);
    }  // SYNCHRONIZED /////////////////////////////////////////////////////////
    ipv6_route_update();
}

static void poll_ipv4_state()
{
    NetlinkCallbackCopyIfIpv4AddrList p;

    D << "Polling ipv4 addr list";
    nl_req_ipaddr_dump(&p, AF_INET);
    status_if_ipv4.set(p.data);
}

static uint64_t ts() {
    struct timeval t;
    gettimeofday(&t, NULL);
    return t.tv_sec*1000000LL + t.tv_usec;
}

static bool WARN_ONCE = false;
static void poll_ipv4_route_state() {
    NetlinkCallbackCopyIpv4RouteTable p;

    struct Ipv4RouteStateChangePrint : StatusRtIpv4::Callback {
        Vector<mesa_routing_entry_t> rt_add;
        Vector<mesa_routing_entry_t> rt_del;

        Ipv4RouteStateChangePrint() : rt_add(4096), rt_del(4096) {
            rt_add.grow_size(4096);
            rt_del.grow_size(4096);
        }

        void add(const mesa_ipv4_uc_t &k,
                 const vtss_appl_ip_route_status_t &v) override {
            mesa_routing_entry_t rt;

            I << "IPv4 chip route add: " << k;
            if (IP_ifindex_to_vlan(v.next_hop_interface, &rt.vlan) == VTSS_RC_OK) {
                rt.type = MESA_ROUTING_ENTRY_TYPE_IPV4_UC;
                rt.route.ipv4_uc = k;
                rt_add.push_back(rt);
            }
        };

        void del(const mesa_ipv4_uc_t &k,
                 const vtss_appl_ip_route_status_t &v) override {
            mesa_routing_entry_t rt;

            I << "IPv4 chip route del: " << k;
            if (IP_ifindex_to_vlan(v.next_hop_interface, &rt.vlan) == VTSS_RC_OK) {
                rt.type = MESA_ROUTING_ENTRY_TYPE_IPV4_UC;
                rt.route.ipv4_uc = k;
                rt_del.push_back(rt);
            }
        };
    } cb;

    I << "Polling ipv4 route table";
    uint64_t a = ts();
    nl_req_route_dump(&p, AF_INET);
    uint64_t b = ts();
    I << "Got: " << p.data.size() << " entries took " << (b - a) / 1000 << "ms";

    a = ts();
    status_rt_ipv4.set(p.data, cb);
    b = ts();
    I << "Event processing: " << (b - a) / 1000 << "ms";

    a = ts();
    // Delete before adding
    uint32_t rt_missing = cb.rt_del.size();
    mesa_routing_entry_t *rt_ptr = cb.rt_del.data();

    while (rt_missing) {
        uint32_t tmp = 0;
        mesa_rc rc = vtss_ip_chip_route_bulk_del(rt_missing, rt_ptr, &tmp);
        D << "del " << rt_missing << "/" << tmp;
        if (rc == MESA_RC_OK) {
            rt_ptr += tmp;
            rt_missing -= tmp;
        } else {
            break;
        }
    }

    // Add routes
    rt_missing = cb.rt_add.size();
    rt_ptr = cb.rt_add.data();
    while (rt_missing) {
        uint32_t tmp = 0;
        mesa_rc rc = vtss_ip_chip_route_bulk_add(rt_missing, rt_ptr, &tmp);
        D << "add " << rt_missing << "/" << tmp;
        if (rc == MESA_RC_OK) {
            rt_ptr += tmp;
            rt_missing -= tmp;
        } else {
            if (!WARN_ONCE) {
                WARN_ONCE = true;
                W << "Failed to add HW route - properly due to resource starvation.";
            }
            break;
        }
    }

    b = ts();

    I << "done. Took " << (b - a) / 1000 << "ms" << " Add-cnt: "
      << cb.rt_add.size() << " del-cnt: " << cb.rt_del.size();
}

static void poll_ipv6_route_state() {
    NetlinkCallbackCopyIpv6RouteTable p;

    struct Ipv6RouteStateChangePrint : StatusRtIpv6::Callback {
        void mod(const vtss_appl_ip_ipv6_route_conf_t &k,
                 const vtss_appl_ip_route_status_t &v_old,
                 vtss_appl_ip_route_status_t &v_new) override {
            // Copy HW_RT flag from old entry
            if (v_old.flags & VTSS_APPL_IP_ROUTE_STATUS_FLAG_HW_RT) {
                v_new.flags |= VTSS_APPL_IP_ROUTE_STATUS_FLAG_HW_RT;
            }
        };

        void del(const vtss_appl_ip_ipv6_route_conf_t &k,
                 const vtss_appl_ip_route_status_t &v) override {
            mesa_routing_entry_t rt;

            I << "IPv6 route del: " << k;
            if ((v.flags & VTSS_APPL_IP_ROUTE_STATUS_FLAG_HW_RT) &&
                IP_ifindex_to_vlan(v.next_hop_interface, &rt.vlan) == VTSS_RC_OK) {
                rt.type = MESA_ROUTING_ENTRY_TYPE_IPV6_UC;
                rt.route.ipv6_uc = k.route;
                vtss_ip_chip_route_del(&rt);
            }
        };
    } cb;

    D << "Polling ipv6 route table";
    nl_req_route_dump(&p, AF_INET6);
    status_rt_ipv6.set(p.data, cb);
    ipv6_route_update();
}

static void poll_ip_route_state(mesa_routing_entry_type_t t) {
    switch (t) {
    case MESA_ROUTING_ENTRY_TYPE_IPV4_UC:
        poll_ipv4_route_state();
        break;

    case MESA_ROUTING_ENTRY_TYPE_IPV6_UC:
        poll_ipv6_route_state();
        break;

    default:;
    }
}

static void poll_ipv6_state() {
    NetlinkCallbackCopyIfIpv6AddrList p;

#if 0 /* TODO: Overriding callback with multiple keys does not seem to work */
    struct Ipv6AddrStateChangePrint : public StatusIfIpv6::Callback {
        void add(const std::tuple<vtss_ifindex_t, mesa_ipv6_network_t> &k,
                 const vtss_appl_ip_if_ipv6_info_t &v) override {
        };

        void del(const std::tuple<vtss_ifindex_t, mesa_ipv6_network_t> &k,
                 const vtss_appl_ip_if_ipv6_info_t &v) override {
        };
    } cb;
#endif
    D << "Polling ipv6 addr table";
    nl_req_ipaddr_dump(&p, AF_INET6);
    status_if_ipv6.set(p.data);
}

static int netlink_monitor_thread_msg(struct nlmsghdr *nh, int len, ip_netlink_poll_t *poll)
{
    N << "Got message of " << len << " bytes";
    N << nh;

    for (; NLMSG_OK(nh, len); nh = NLMSG_NEXT(nh, len)) {
        switch (nh->nlmsg_type) {
        case NLMSG_DONE:
            D << "NLMSG_DONE";
            break;

        case NLMSG_ERROR:
            E << "Error in netlink mesage";
            return -1;

        case RTM_NEWLINK:
        case RTM_DELLINK:
        case RTM_GETLINK:
            D << "RTM_NEWLINK/RTM_DELLINK/RTM_GETLINK";
            poll->link = TRUE;
            break;

        case RTM_NEWADDR:
        case RTM_DELADDR:
        case RTM_GETADDR: {
            D << "RTM_NEWADDR/RTM_DELADDR/RTM_GETADDR";
            struct ifaddrmsg *ifa = (struct ifaddrmsg *)NLMSG_DATA(nh);
            if (len < NLMSG_LENGTH(sizeof(*ifa))) {
                E << "msg too short for this type!";
                break;
            }

            if (ifa->ifa_family == AF_INET)
                poll->ipv4_addr = TRUE;
            else if (ifa->ifa_family == AF_INET6)
                poll->ipv6_addr = TRUE;
            break;
        }

        case RTM_NEWROUTE:
        case RTM_DELROUTE:
        case RTM_GETROUTE: {
            D << "RTM_NEWROUTE/RTM_DELROUTE/RTM_GETROUTE";
            struct rtmsg *rtm = (struct rtmsg *)NLMSG_DATA(nh);
            if (len < NLMSG_LENGTH(sizeof(*rtm))) {
                E << "msg too short for this type!";
                break;
            }

            if (rtm->rtm_family == AF_INET)
                poll->ipv4_route = TRUE;
            else if (rtm->rtm_family == AF_INET6)
                poll->ipv6_route = TRUE;
            break;
        }

        case RTM_GETNEIGH:
        case RTM_NEWNEIGH:
        case RTM_DELNEIGH: {
            D << "RTM_GETNEIGH/RTM_NEWNEIGH/RTM_DELNEIGH";
            int len = nh->nlmsg_len;
            struct ndmsg *ndm = (struct ndmsg *)NLMSG_DATA(nh);
            len -= NLMSG_LENGTH(sizeof(*ndm));
            if (len < 0) {
                E << "Msg too short";
                break;
            }

            if (ndm->ndm_family == AF_INET)
                poll->ipv4_neighbor = TRUE;
            else if (ndm->ndm_family == AF_INET6)
                poll->ipv6_neighbor = TRUE;
            else
                poll->mac_addr_list = TRUE;
            break;
        }

        default:
            D << "default";
        }
    }

    return 0;
}

static void netlink_monitor_thread(vtss_addrword_t data) {
    char buf[4096];
    int len, fd, res;
    struct iovec iov = {buf, sizeof(buf)};
    struct msghdr msg;
    struct sockaddr_nl sa;
    bool socket_ok = false;

    while (1) {
        I << "Creating netlink socket";

        socket_ok = true;
        fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
        int pid = 0;
        if (fd < 0) {
            E << "Socket failed: " << strerror(errno);
            socket_ok = false;
        }

        memset(&sa, 0, sizeof(sa));
        sa.nl_pid = pid;
        sa.nl_family = AF_NETLINK;
        sa.nl_groups = RTMGRP_LINK | RTMGRP_NOTIFY | RTMGRP_NEIGH |
                       RTMGRP_IPV4_IFADDR | RTMGRP_IPV4_ROUTE |
                       RTMGRP_IPV4_RULE | RTMGRP_IPV6_IFADDR | RTMGRP_IPV6_ROUTE |
                       RTMGRP_IPV6_IFINFO | RTMGRP_IPV6_PREFIX;

        res = bind(fd, (struct sockaddr *)&sa, sizeof(sa));
        if (res != 0) {
            E << "Bind failed: " << strerror(errno);
            socket_ok = false;
        }

        // The netlink connection has (potential) been reset, meaning that we
        // may have lost messages. We therefore need to poll all as we do not
        // know what messages has been lost.
        poll_link_state();
        poll_link_mac_address_list();
        poll_ipv4_state();
        poll_ipv6_state();
        poll_ipv4_route_state();
        poll_ipv6_route_state();
        poll_ipv4_neighbor_list();
        poll_ipv6_neighbor_list();

        while (socket_ok) {
            int res = 0;
            ip_netlink_poll_t poll;
            msg = {&sa, sizeof(sa), &iov, 1, NULL, 0, 0};
            int recvmsg_flag = 0;  // must block at first call
            int cnt = 0;
            memset(&poll, 0, sizeof(poll));

            do {
                len = recvmsg(fd, &msg, recvmsg_flag);
                if (len == -1) {
                    if (errno != EAGAIN && errno != EWOULDBLOCK)
                        socket_ok = false;
                    break;
                }

                res = netlink_monitor_thread_msg((struct nlmsghdr *)buf, len, &poll);

                // we now want to empty the queue but not block
                recvmsg_flag = MSG_DONTWAIT;
                cnt++;

                if (res != 0) socket_ok = false;
            } while (socket_ok && cnt < 100);

            if (poll.link) poll_link_state();
            if (poll.mac_addr_list) poll_link_mac_address_list();
            if (poll.ipv4_addr) poll_ipv4_state();
            if (poll.ipv6_addr) poll_ipv6_state();
            if (poll.ipv4_route) poll_ipv4_route_state();
            if (poll.ipv6_route) poll_ipv6_route_state();
            if (poll.ipv4_neighbor) poll_ipv4_neighbor_list();
            if (poll.ipv6_neighbor) poll_ipv6_neighbor_list();
        }

        close(fd);
    }
}

}  // namespace netlink
}  // namespace ip
}  // namespace appl
}  // namespace vtss

using namespace vtss::appl::ip::netlink;

mesa_rc vtss_ip_os_if_index_to_vlan_if(u32 idx, vtss_if_id_vlan_t *vlan)
{
    vtss_if_id_t if_id = conv_os_ifindex_to_vtss_if_id_t(idx);

    if (if_id.type == VTSS_ID_IF_TYPE_VLAN) {
        *vlan = if_id.u.vlan;
        return VTSS_RC_OK;
    }
    I << "vid for idx " << idx << " not found";
    return VTSS_RC_ERROR;
}

static int vid_to_os_index(mesa_vid_t vid) {
    vtss_ifindex_t idx;
    vtss_appl_ip_if_status_link_t link_st;
    if (vtss_ifindex_from_vlan(vid, &idx) != VTSS_RC_OK) {
        return -1;
    }

    if (status_if_link.get(idx, &link_st) != VTSS_RC_OK) {
        return -1;
    }

    return link_st.os_if_index;
}

mesa_rc vtss_ip_os_vlan_to_if_index(mesa_vid_t vid, u32 *idx) {
    int i = vid_to_os_index(vid);
    if (i < 0) {
        I << "idx for vid " << vid << " not found";
    }

    *idx = i;

    return VTSS_RC_OK;
}

static mesa_rc vtss_sysctl_set_int(const char *path, int i)
{
    char    buf[128], *p = buf;
    int     cnt, res, len = sizeof(buf);

    if (snprintf(buf, len, "/proc/sys/net/%s", path) >= len) {
        return VTSS_RC_ERROR;
    }

    Fd fd(open(buf, O_WRONLY));
    len = 16;
    if (fd.raw() < 0 || (cnt = snprintf(buf, len, "%d\n", i)) >= len) {
        return VTSS_RC_ERROR;
    }

    while (cnt) {
        res = write(fd.raw(), p, cnt);
        if (res <= 0) {
            return VTSS_RC_ERROR;
        }
        p += res;
        cnt -= res;
    }
    return VTSS_RC_OK;
}

mesa_rc vtss_ip_os_global_param_set(const vtss_ip_global_param_priv_t *const param)
{
    int enable = (param->enable_routing ? 1 : 0);

    VTSS_RC(vtss_sysctl_set_int("ipv4/ip_forward", enable));
#ifdef VTSS_SW_OPTION_IPV6
    VTSS_RC(vtss_sysctl_set_int("ipv6/conf/all/forwarding", enable));
#endif
    return vtss_ip_chip_routing_enable(enable);
}

static vtss::str ifname_from_vlan(vtss::Buf *b, mesa_vid_t vid) {
    vtss::BufPtrStream ss(b);
    ss << VTSS_VLAN_IF_PREFIX << vid;
    ss.push(0);
    return vtss::str(ss.begin(), ss.end());
}

static mesa_rc vtss_ip_os_if_cmd(vtss::str ifname, int cmd, int flags,
                                 int ifi_flags = 0, int ifi_change = 0,
                                 int mtu = -1) {
    mesa_rc rc = VTSS_RC_OK;
    NetlinkCallbackPrint cb;
    static const char *ifkind = "vtss_if_mux";
    int seq = netlink_seq();

    struct {
        struct nlmsghdr n;
        struct ifinfomsg i;
        char attr[1024];
    } req;

    memset(&req, 0, sizeof(req));
    req.n.nlmsg_seq = seq;
    req.n.nlmsg_len = NLMSG_LENGTH(sizeof(struct ifinfomsg));
    req.n.nlmsg_flags = flags;
    req.n.nlmsg_type = cmd;
    req.i.ifi_family = AF_UNSPEC;
    req.i.ifi_change = ifi_change;
    req.i.ifi_flags = ifi_flags;

    {  // add ifname
        if (ifname.size() > IFNAMSIZ) {
            I << "Invalid ifname";
            return VTSS_RC_ERROR;
        }
        DO(attr_add_binary, &req.n, sizeof(req), IFLA_IFNAME, ifname.begin(),
           ifname.size());
    }

    {  // prepare nested type - linkinfo/info_kind
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpointer-arith"
        struct rtattr *linkinfo = NLMSG_END(&req.n);
#pragma GCC diagnostic pop
        DO(attr_add_binary, &req.n, sizeof(req), IFLA_LINKINFO, NULL, 0);
        DO(attr_add_str, &req.n, sizeof(req), IFLA_INFO_KIND, ifkind);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpointer-arith"
        linkinfo->rta_len = ((char *)NLMSG_END(&req.n)) - ((char *)linkinfo);
    }

    if (cmd == RTM_NEWLINK) {
        mesa_mac_t m;
        if (conf_mgmt_mac_addr_get(m.addr, 0) == 0) {
            DO(attr_add_mac, &req.n, sizeof(req), IFLA_ADDRESS, m);
        } else {
            E << "Failed to get mac address";
        }
    }

    if (mtu != -1) {
        DO(attr_add_binary, &req.n, sizeof(req), IFLA_MTU, &mtu, 4);
    }

    DO(nl_req, (const void *)&req, sizeof(req), seq, &cb, 2048, 2048);

    return rc;
}

mesa_rc vtss_ip_os_if_add(mesa_vid_t vid) {
    mesa_rc rc;
    StaticBuffer<IFNAMSIZ> buf;

    I << "vtss_ip_os_if_add " << ifname_from_vlan(&buf, vid);
    rc = vtss_ip_os_if_cmd(ifname_from_vlan(&buf, vid), RTM_NEWLINK,
                           NLM_F_REQUEST | NLM_F_CREATE | NLM_F_EXCL | NLM_F_ACK);
    poll_link_state();
    return rc;
}

mesa_rc vtss_ip_os_if_add_port(uint32_t port)
{
    char buf[IFNAMSIZ];

    snprintf(buf, sizeof(buf), "vtss.port.%u", port);
    return vtss_ip_os_if_cmd(vtss::str(buf), RTM_NEWLINK,
                             NLM_F_REQUEST | NLM_F_CREATE | NLM_F_EXCL | NLM_F_ACK, IFF_UP, IFF_UP);
}

mesa_rc vtss_ip_os_if_del(mesa_vid_t vid) {
    mesa_rc rc;
    StaticBuffer<IFNAMSIZ> buf;

    I << "vtss_ip_os_if_del " << vid;
    rc = vtss_ip_os_if_cmd(ifname_from_vlan(&buf, vid), RTM_DELLINK,
                           NLM_F_REQUEST | NLM_F_ACK);
    poll_link_state();
    return rc;
}

mesa_rc vtss_ip_os_if_set(mesa_vid_t vid, const vtss_if_param_t *const if_params) {
    mesa_rc rc;
    StaticBuffer<IFNAMSIZ> buf;

    I << "vtss_ip_os_if_set " << vid;
    rc = vtss_ip_os_if_cmd(ifname_from_vlan(&buf, vid), RTM_NEWLINK,
                           NLM_F_REQUEST | NLM_F_ACK, 0, 0, if_params->mtu);
    poll_link_state();
    return rc;
}

mesa_rc vtss_ip_os_if_ctl(mesa_vid_t vid, BOOL up) {
    mesa_rc rc;
    StaticBuffer<IFNAMSIZ> buf;

    I << "vtss_ip_os_if_ctl " << vid << " " << up;
    rc = vtss_ip_os_if_cmd(ifname_from_vlan(&buf, vid), RTM_NEWLINK,
                           NLM_F_REQUEST | NLM_F_ACK, up ? IFF_UP : 0, IFF_UP);
    poll_link_state();
    poll_ipv4_route_state();
    poll_ipv6_route_state();
    return rc;
}

#if defined(VTSS_SW_OPTION_DHCP6_CLIENT)
#include "dhcp6_client_api.h"

mesa_rc vtss_ip_os_if_ifp_ra_flags(mesa_vid_t vid, u8 *const ra_flg)
{
    vtss_ifindex_t                idx;
    vtss_appl_ip_if_status_link_t st;

    if (vtss_ifindex_from_vlan(vid, &idx) != VTSS_RC_OK ||
        status_if_link.get(idx, &st) != VTSS_RC_OK) {
        return VTSS_RC_ERROR;
    }

    *ra_flg = 0;
    if (st.flags & VTSS_APPL_IP_IF_LINK_FLAG_IPV6_RA_MANAGED) {
        *ra_flg |= 0x80;
    }
    if (st.flags & VTSS_APPL_IP_IF_LINK_FLAG_IPV6_RA_OTHER) {
        *ra_flg |= 0x40;
    }
    return VTSS_RC_OK;
}

static mesa_rc iff_notify_dhcp6(
    u16                     ifidx,
    const struct sockaddr   *addr,
    const i32               *olink,
    const i32               *nlink,
    const u8                *mflag,
    const u8                *oflag
)
{
    Dhcp6cNotify dhcp6c_msg;
    mesa_vid_t   vid;

    memset(&dhcp6c_msg, 0x0, sizeof(Dhcp6cNotify));
    dhcp6c_msg.ifidx = ifidx;
    VTSS_RC(vtss_ip_os_if_index_to_vlan_if(ifidx, &vid));
    dhcp6c_msg.vlanx = vid;

    if (addr) {
        struct sockaddr_in6 *adrs = (struct sockaddr_in6 *)addr;
        dhcp6c_msg.type = DHCP6C_MSG_IF_DAD;
        memcpy(&dhcp6c_msg.msg.dad.address, &adrs->sin6_addr, sizeof(mesa_ipv6_t));
    } else if (mflag && oflag) {
        dhcp6c_msg.type = DHCP6C_MSG_IF_RA_FLAG;
        dhcp6c_msg.msg.ra.m_flag = *mflag;
        dhcp6c_msg.msg.ra.o_flag = *oflag;
    } else if (olink && nlink) {
        dhcp6c_msg.type = DHCP6C_MSG_IF_LINK;
        dhcp6c_msg.msg.link.old_state = *olink;
        dhcp6c_msg.msg.link.new_state = *nlink;
    } else if (!addr && !mflag && !oflag && !olink && !nlink) {
        dhcp6c_msg.type = DHCP6C_MSG_IF_DEL;
    }

    return vtss::dhcp6c::dhcp6_client_interface_notify(&dhcp6c_msg);
}

mesa_rc vtss_ip_os_if_notify_dhcp6(i32 ifidx,
                                   const mesa_ipv6_network_t *const network,
                                   const i32 *const prev_link,
                                   const i32 *const curr_link,
                                   const u8 *const mo_flag)
{
    struct sockaddr     *adr;
    struct sockaddr_in6 addr;
    u8                  managed_flag, other_flag;
    u8                  *mflg, *oflg;

    adr = NULL;
    if (network) {
        memset(&addr, 0x0, sizeof(struct sockaddr_in6));
        addr.sin6_family = AF_INET6;
        //addr.sin6_len = sizeof(struct sockaddr_in6);
        memcpy(&addr.sin6_addr, &network->address, sizeof(mesa_ipv6_t));

        adr = (struct sockaddr *)&addr;
    }

    mflg = oflg = NULL;
    if (mo_flag) {
        mflg = &managed_flag;
        oflg = &other_flag;

        *mflg = (*mo_flag >> 7) & 0x1;
        *oflg = (*mo_flag >> 6) & 0x1;
    }

    return iff_notify_dhcp6((u16)(ifidx & 0xFFFF), adr, prev_link, curr_link, mflg, oflg);
}
#endif /* defined(VTSS_SW_OPTION_DHCP6_CLIENT) */

static mesa_rc if_status_all_(
        const State &state, vtss_appl_ip_if_status_type_t type,
        Vector<vtss_appl_ip_if_status_t> &status,
        int link, int link_end, int ipv4, int ipv4_end, int ipv6, int ipv6_end)
{
    // fast-forward to the end if a geven family is of no interest
    if (type != VTSS_APPL_IP_IF_STATUS_TYPE_ANY &&
        type != VTSS_APPL_IP_IF_STATUS_TYPE_LINK)
        link = link_end;

    if (type != VTSS_APPL_IP_IF_STATUS_TYPE_ANY &&
        type != VTSS_APPL_IP_IF_STATUS_TYPE_IPV4)
        ipv4 = ipv4_end;

    if (type != VTSS_APPL_IP_IF_STATUS_TYPE_ANY &&
        type != VTSS_APPL_IP_IF_STATUS_TYPE_IPV6)
        ipv6 = ipv6_end;

    while (link != link_end || ipv4 != ipv4_end || ipv6 != ipv6_end) {
        mesa_vid_t id = VTSS_VIDS;
        vtss_ifindex_t id_ifindex;
        if (link != link_end && link < id) id = link;
        if (ipv4 != ipv4_end && ipv4 < id) id = ipv4;
        if (ipv6 != ipv6_end && ipv6 < id) id = ipv6;

        if (vtss_ifindex_from_vlan(id, &id_ifindex) != VTSS_RC_OK) {
           return VTSS_RC_ERROR;
        }

        if (link != link_end && link == id) {
            vtss_appl_ip_if_status_t s = {};
            s.type = VTSS_APPL_IP_IF_STATUS_TYPE_LINK;
            s.if_id.type = VTSS_ID_IF_TYPE_VLAN;
            s.if_id.u.vlan = link;
            if (status_if_link.get(id_ifindex, &s.u.link) == VTSS_RC_OK) {
                status.push_back(s);
            }

            link++;
        }

        if (ipv4 != ipv4_end && ipv4 == id) {
            vtss_ifindex_t           ifidx = id_ifindex;
            mesa_ipv4_network_t      addr;
            vtss_appl_ip_if_status_t s = {};

            s.type = VTSS_APPL_IP_IF_STATUS_TYPE_IPV4;
            s.if_id.type = VTSS_ID_IF_TYPE_VLAN;
            s.if_id.u.vlan = id;
            memset(&addr, 0, sizeof(addr));
            while (status_if_ipv4.get_next(&ifidx, &addr, &s.u.ipv4.info) == VTSS_RC_OK &&
                   ifidx == id_ifindex) {
                s.u.ipv4.net = addr;
                status.push_back(s);
            }
            ipv4++;
        }

        if (ipv6 != ipv6_end && ipv6 == id) {
            vtss_ifindex_t           ifidx = id_ifindex;
            mesa_ipv6_network_t      addr;
            vtss_appl_ip_if_status_t s = {};

            s.type = VTSS_APPL_IP_IF_STATUS_TYPE_IPV6;
            s.if_id.type = VTSS_ID_IF_TYPE_VLAN;
            s.if_id.u.vlan = id;
            memset(&addr, 0, sizeof(addr));
            while (status_if_ipv6.get_next(&ifidx, &addr, &s.u.ipv6.info) == VTSS_RC_OK &&
                   ifidx == id_ifindex) {
                s.u.ipv6.net = addr;
                status.push_back(s);
            }
            ipv6++;
        }
    }

    return VTSS_RC_OK;
}

mesa_rc vtss_ip_os_if_status(vtss_appl_ip_if_status_type_t type,
                             vtss_if_id_vlan_t id,
                             Vector<vtss_appl_ip_if_status_t> &status)
{
    int vid = id, vid_end = (id + 1);

    SYNCHRONIZED(state) {  /////////////////////////////////////////////////////
        return if_status_all_(state, type, status, vid, vid_end, vid, vid_end, vid, vid_end);
    }  /////////////////////////////////////////////////////////////////////////

    return VTSS_RC_OK;
}

mesa_rc vtss_ip_os_if_status_all(vtss_appl_ip_if_status_type_t type,
                                 Vector<vtss_appl_ip_if_status_t> &status)
{
    int vid = 1, vid_end;

    status.clear();

    SYNCHRONIZED(state) {  /////////////////////////////////////////////////////
        vid_end = VTSS_VIDS;
        return if_status_all_(state, type, status, vid, vid_end, vid, vid_end, vid, vid_end);
    }  /////////////////////////////////////////////////////////////////////////

    return VTSS_RC_OK;
}

static mesa_rc vtss_ip_os_ipv4_cmd(mesa_vid_t vid, int cmd, int flags,
                                   const mesa_ipv4_network_t *const network,
                                   mesa_ipv4_t bcast = 0) {
    int os_ifindex = vid_to_os_index(vid);
    mesa_rc rc = VTSS_RC_OK;
    NetlinkCallbackPrint cb;
    int seq = netlink_seq();

    struct {
        struct nlmsghdr n;
        struct ifaddrmsg i;
        char attr[1024];
    } req;

    if (os_ifindex < 0) {
        E << "No such interface found";
        return VTSS_RC_ERROR;
    }

    memset(&req, 0, sizeof(req));
    req.n.nlmsg_seq = seq;
    req.n.nlmsg_len = NLMSG_LENGTH(sizeof(struct ifaddrmsg));
    req.n.nlmsg_flags = flags;
    req.n.nlmsg_type = cmd;
    req.i.ifa_family = AF_INET;
    req.i.ifa_index = os_ifindex;
    req.i.ifa_prefixlen = network->prefix_size;

    DO(attr_add_ipv4, &req.n, sizeof(req), IFA_LOCAL, network->address);
    DO(attr_add_ipv4, &req.n, sizeof(req), IFA_ADDRESS, network->address);
    if (bcast) DO(attr_add_ipv4, &req.n, sizeof(req), IFA_BROADCAST, bcast);
    DO(nl_req, (const void *)&req, sizeof(req), seq, &cb, 2048, 2048);

    return rc;
}

mesa_rc vtss_ip_os_ipv4_add(mesa_vid_t vid,
                            const mesa_ipv4_network_t *const network) {
    mesa_rc rc;
    I << "vtss_ip_os_ipv4_add " << vid << " " << *network;

    mesa_ipv4_t mask = 0, bcast = network->address;
    (void)vtss_conv_prefix_to_ipv4mask(network->prefix_size, &mask);
    bcast |= (~mask);

    rc = vtss_ip_os_ipv4_cmd(vid, RTM_NEWADDR, NLM_F_REQUEST | NLM_F_CREATE |
                                                       NLM_F_REPLACE | NLM_F_ACK,
                             network, bcast);
    poll_ipv4_state();
    return rc;
}

mesa_rc vtss_ip_os_ipv4_del(mesa_vid_t vid,
                            const mesa_ipv4_network_t *const network) {
    mesa_rc rc;
    I << "vtss_ip_os_ipv4_del " << vid << " " << *network;
    rc = vtss_ip_os_ipv4_cmd(vid, RTM_DELADDR, NLM_F_REQUEST | NLM_F_ACK,
                             network);
    poll_ipv4_state();
    return rc;
}

static mesa_rc vtss_ip_os_ipv6_cmd(mesa_vid_t vid, int cmd, int flags,
                                   const mesa_ipv6_network_t *const network,
                                   const vtss_ip_ipv6_addr_info_t *const info) {
    int os_ifindex = vid_to_os_index(vid);
    mesa_rc rc = VTSS_RC_OK;
    NetlinkCallbackPrint cb;
    int seq = netlink_seq();

    struct {
        struct nlmsghdr n;
        struct ifaddrmsg i;
        char attr[1024];
    } req;

    if (os_ifindex < 0) {
        E << "No such interface found";
        return VTSS_RC_ERROR;
    }

    memset(&req, 0, sizeof(req));
    req.n.nlmsg_seq = seq;
    req.n.nlmsg_len = NLMSG_LENGTH(sizeof(struct ifaddrmsg));
    req.n.nlmsg_flags = flags;
    req.n.nlmsg_type = cmd;
    req.i.ifa_family = AF_INET6;
    req.i.ifa_index = os_ifindex;
    req.i.ifa_prefixlen = network->prefix_size;

    DO(attr_add_ipv6, &req.n, sizeof(req), IFA_LOCAL, network->address);
    DO(attr_add_ipv6, &req.n, sizeof(req), IFA_ADDRESS, network->address);
    if (info) {
        struct ifa_cacheinfo ci;
        memset(&ci, 0, sizeof(ci));
        ci.ifa_prefered = info->lt_preferred;
        ci.ifa_valid = info->lt_valid;
        DO(attr_add_binary, &req.n, sizeof(req), IFA_CACHEINFO, &ci, sizeof(ci));
    }
    DO(nl_req, (const void *)&req, sizeof(req), seq, &cb, 2048, 2048);

    return rc;
}

mesa_rc vtss_ip_os_ipv6_add(mesa_vid_t vid,
                            const mesa_ipv6_network_t *const network,
                            const vtss_ip_ipv6_addr_info_t *const info) {
    mesa_rc rc;
    I << "vtss_ip_os_ipv6_add " << vid << " " << *network;
    rc = vtss_ip_os_ipv6_cmd(
            vid, RTM_NEWADDR,
            NLM_F_REQUEST | NLM_F_CREATE | NLM_F_REPLACE | NLM_F_ACK, network, info);
    poll_ipv6_state();
    SYNCHRONIZED(state) {
        state.vid_to_ipv6_network.set(vid, *network);
    } // SYNCHRONIZED
    poll_ipv6_neighbor_list();
    return rc;
}

mesa_rc vtss_ip_os_ipv6_del(mesa_vid_t vid,
                            const mesa_ipv6_network_t *const network) {
    mesa_rc rc;
    I << "vtss_ip_os_ipv6_del " << vid << " " << *network;
    rc = vtss_ip_os_ipv6_cmd(vid, RTM_DELADDR, NLM_F_REQUEST | NLM_F_ACK,
                             network, NULL);
    poll_ipv6_state();
    SYNCHRONIZED(state) {
        state.vid_to_ipv6_network.erase(vid);
    } // SYNCHRONIZED
    poll_ipv6_neighbor_list();
    return rc;
}

static mesa_rc route_cmd(int cmd, int flags,
                         const mesa_routing_entry_t *const rt) {
    mesa_rc rc = VTSS_RC_OK;
    NetlinkCallbackPrint cb;
    int seq = netlink_seq();
    int os_ifindex;
    u32 oif;

    struct {
        struct nlmsghdr n;
        struct rtmsg r;
        char attr[1024];
    } req;

    memset(&req, 0, sizeof(req));
    req.n.nlmsg_seq = seq;
    req.n.nlmsg_len = NLMSG_LENGTH(sizeof(struct rtmsg));
    req.n.nlmsg_flags = flags;
    req.n.nlmsg_type = cmd;

    req.r.rtm_table = RT_TABLE_MAIN;
    if (cmd == RTM_DELROUTE)
        req.r.rtm_scope = RT_SCOPE_NOWHERE;
    else
        req.r.rtm_scope = RT_SCOPE_UNIVERSE;

    switch (rt->type) {
    case MESA_ROUTING_ENTRY_TYPE_IPV4_UC:
        if (cmd != RTM_DELROUTE) req.r.rtm_type = RTN_UNICAST;
        req.r.rtm_family = AF_INET;
        req.r.rtm_dst_len = rt->route.ipv4_uc.network.prefix_size;
        DO(attr_add_ipv4, &req.n, sizeof(req), RTA_DST,
           rt->route.ipv4_uc.network.address);
        DO(attr_add_ipv4, &req.n, sizeof(req), RTA_GATEWAY,
           rt->route.ipv4_uc.destination);
        break;

    case MESA_ROUTING_ENTRY_TYPE_IPV6_UC:
        if (cmd != RTM_DELROUTE) req.r.rtm_type = RTN_UNICAST;
        req.r.rtm_family = AF_INET6;
        req.r.rtm_dst_len = rt->route.ipv6_uc.network.prefix_size;
        DO(attr_add_ipv6, &req.n, sizeof(req), RTA_DST,
           rt->route.ipv6_uc.network.address);
        DO(attr_add_ipv6, &req.n, sizeof(req), RTA_GATEWAY,
           rt->route.ipv6_uc.destination);
        if (rt->vlan != VTSS_VID_NULL &&
            (os_ifindex = vid_to_os_index(rt->vlan)) >= 0) {
            oif = os_ifindex;
            DO(attr_add_u32, &req.n, sizeof(req), RTA_OIF, oif);
        }
        break;

    default:
        return VTSS_RC_ERROR;
    }


    DO(nl_req, (const void *)&req, sizeof(req), seq, &cb, 2048, 2048);

    return rc;
}

mesa_rc vtss_ip_os_route_add(const mesa_routing_entry_t *const rt, bool no_poll) {
    I << "vtss_ip_os_route_add " << *rt;
    (void)route_cmd(RTM_NEWROUTE,
                    NLM_F_REQUEST | NLM_F_CREATE | NLM_F_EXCL | NLM_F_ACK, rt);
    if (!no_poll) {
        poll_ip_route_state(rt->type);
    }
    return VTSS_RC_OK;
}

mesa_rc vtss_ip_os_route_del(const mesa_routing_entry_t *const rt, bool no_poll) {
    I << "vtss_ip_os_route_del " << *rt;
    (void)route_cmd(RTM_DELROUTE, NLM_F_REQUEST | NLM_F_ACK, rt);
    if (!no_poll) {
        poll_ip_route_state(rt->type);
    }
    return VTSS_RC_OK;
}

static mesa_rc ipv4_route_get(u32 max, vtss_routing_status_t *rt, u32 *const cnt) {
    *cnt = 0;

    vtss::notifications::LockGlobalSubject lock;
    auto i = status_rt_ipv4.begin(lock);
    auto e = status_rt_ipv4.end(lock);

    for (; *cnt < max && i != e; i++) {
        memset(rt, 0, sizeof(*rt));
        rt->rt.type = MESA_ROUTING_ENTRY_TYPE_IPV4_UC;
        rt->rt.route.ipv4_uc = i->first;
        rt->flags = i->second.flags;

        auto elm = vtss::ifindex_decompose(i->second.next_hop_interface);
        if (elm.iftype == VTSS_IFINDEX_TYPE_VLAN) {
            rt->interface.type = VTSS_ID_IF_TYPE_VLAN;
            rt->interface.u.vlan = elm.ordinal;
        } else {
            rt->interface.type = VTSS_ID_IF_TYPE_OS_ONLY;
            rt->interface.u.os.ifno = 0;
            snprintf(rt->interface.u.os.name, IF_NAMESIZE + 1, "UNKNOWN");
            rt->interface.u.os.name[IF_NAMESIZE] = 0;
        }

        rt++;
        *cnt = *cnt + 1;
    }

    return VTSS_RC_OK;
}

static mesa_rc ipv6_route_get(u32 max, vtss_routing_status_t *rt, u32 *const cnt) {
    *cnt = 0;

    vtss::notifications::LockGlobalSubject lock;
    auto i = status_rt_ipv6.begin(lock);
    auto e = status_rt_ipv6.end(lock);

    for (; *cnt < max && i != e; i++) {
        memset(rt, 0, sizeof(*rt));
        rt->rt.type = MESA_ROUTING_ENTRY_TYPE_IPV6_UC;
        rt->rt.route.ipv6_uc = i->first.route;
        rt->flags = i->second.flags;

        auto elm = vtss::ifindex_decompose(i->second.next_hop_interface);
        if (elm.iftype == VTSS_IFINDEX_TYPE_VLAN) {
            rt->interface.type = VTSS_ID_IF_TYPE_VLAN;
            rt->interface.u.vlan = elm.ordinal;
        } else {
            rt->interface.type = VTSS_ID_IF_TYPE_OS_ONLY;
            rt->interface.u.os.ifno = 0;
            snprintf(rt->interface.u.os.name, IF_NAMESIZE + 1, "UNKNOWN");
            rt->interface.u.os.name[IF_NAMESIZE] = 0;
        }

        rt++;
        *cnt = *cnt + 1;
    }

    return VTSS_RC_OK;
}

mesa_rc vtss_ip_os_route_get(mesa_routing_entry_type_t type, u32 max,
                             vtss_routing_status_t *rt, u32 *const cnt) {
    switch (type) {
    case MESA_ROUTING_ENTRY_TYPE_IPV4_UC:
        return ipv4_route_get(max, rt, cnt);

    case MESA_ROUTING_ENTRY_TYPE_IPV6_UC:
        return ipv6_route_get(max, rt, cnt);

    default:
        return VTSS_RC_ERROR;
    }
}

static mesa_rc neighbor_del(mesa_ip_addr_t *ip, int idx)
{
    mesa_rc rc = VTSS_RC_OK;
    NetlinkCallbackPrint cb;
    int seq = netlink_seq();

    struct {
        struct nlmsghdr n;
        struct ndmsg ndm;
        char attr[256];
    } req;

    memset(&req, 0, sizeof(req));
    req.n.nlmsg_seq = seq;
    req.n.nlmsg_len = NLMSG_LENGTH(sizeof(struct ndmsg));
    req.n.nlmsg_flags = (NLM_F_REQUEST | NLM_F_ACK);
    req.n.nlmsg_type = RTM_DELNEIGH;
    if (ip->type == MESA_IP_TYPE_IPV4) {
        req.ndm.ndm_family = AF_INET;
        DO(attr_add_ipv4, &req.n, sizeof(req), NDA_DST,  ip->addr.ipv4);
    } else {
        req.ndm.ndm_family = AF_INET6;
        DO(attr_add_ipv6, &req.n, sizeof(req), NDA_DST,  ip->addr.ipv6);
    }
    req.ndm.ndm_ifindex = idx;
    DO(nl_req, (const void *)&req, sizeof(req), seq, &cb, 2048, 2048);

    return rc;
}

mesa_rc vtss_ip_os_nb_clear(mesa_ip_type_t type)
{
    mesa_rc        rc = VTSS_RC_OK;
    mesa_ip_addr_t ip;

    ip.type = type;
    if (type == MESA_IP_TYPE_IPV4) {
        Set<Ipv4Neighbor> nbr_list;
        SYNCHRONIZED(state) {
            for (auto cache = state.ipv4_neighbor_list.begin(); cache != state.ipv4_neighbor_list.end(); cache++) {
                D << "delete: " << *cache;
                nbr_list.emplace(*cache);
            }
        } // SYNCHRONIZED
        for (const auto &e : nbr_list) {
            ip.addr.ipv4 = e.data.ip_address;
            if ((rc = neighbor_del(&ip, e.if_index)) != VTSS_RC_OK) {
                break;
            }
        }
    } else {
        Set<Ipv6Neighbor> nbr_list;
        SYNCHRONIZED(state) {
            for (auto cache = state.ipv6_neighbor_list.begin(); cache != state.ipv6_neighbor_list.end(); cache++) {
                D << "delete: " << *cache;
                nbr_list.emplace(*cache);
            }
        } // SYNCHRONIZED
        for (const auto &e : nbr_list) {
            ip.addr.ipv6 = e.data.key.ip_address;
            if ((rc = neighbor_del(&ip, e.if_index)) != VTSS_RC_OK) {
                break;
            }
        }
    }
    return rc;
}

mesa_rc vtss_ip_os_nb_status_get(mesa_ip_type_t type, const u32 max, u32 *cnt, vtss_neighbour_status_t *status)
{
    SYNCHRONIZED(state) {
        *cnt = 0;
        if (type == MESA_IP_TYPE_IPV4) {
            for (auto cache = state.ipv4_neighbor_list.begin(); *cnt < max && cache != state.ipv4_neighbor_list.end(); (*cnt)++, cache++, status++) {
                memset(status, 0, sizeof(*status));
                status->ip_address.type = type;
                status->ip_address.addr.ipv4 = cache->data.ip_address;
                status->mac_address = cache->data.status.mac_address;
                status->interface = cache->if_id;
                status->flags = VTSS_APPL_IP_NEIGHBOUR_FLAG_VALID;
            }
        } else {
            for (auto cache = state.ipv6_neighbor_list.begin(); *cnt < max && cache != state.ipv6_neighbor_list.end(); (*cnt)++, cache++, status++) {
                memset(status, 0, sizeof(*status));
                status->ip_address.type = type;
                status->ip_address.addr.ipv6 = cache->data.key.ip_address;
                status->mac_address = cache->data.status.mac_address;
                status->interface = cache->if_id;
                status->flags = VTSS_APPL_IP_NEIGHBOUR_FLAG_VALID;
            }
        }
    } // SYNCHRONIZED
    return VTSS_RC_OK;
}

mesa_rc vtss_ip_os_ipv4_neighbour_status_get_list(u32 max, u32 *cnt, vtss_appl_ip_ipv4_neighbour_status_pair_t *status)
{
    SYNCHRONIZED(state) {
        *cnt = 0;
        for (auto cache = state.ipv4_neighbor_list.begin(); *cnt < max && cache != state.ipv4_neighbor_list.end(); (*cnt)++, cache++, status++) {
            *status = cache->data;
        }
    } // SYNCHRONIZED
    return VTSS_RC_OK;
}

mesa_rc vtss_ip_os_ipv6_neighbour_status_get_list(u32 max, u32 *cnt, vtss_appl_ip_ipv6_neighbour_status_pair_t *status)
{
    SYNCHRONIZED(state) {
        *cnt = 0;
        for (auto cache = state.ipv6_neighbor_list.begin(); *cnt < max && cache != state.ipv6_neighbor_list.end(); (*cnt)++, cache++, status++) {
            *status = cache->data;
            if (!vtss_ipv6_addr_is_link_local(&status->key.ip_address)) {
                status->key.interface = VTSS_IFINDEX_NONE;
            }
        }
    } // SYNCHRONIZED
    return VTSS_RC_OK;
}

static vtss_handle_t netlink_monitor_thread_handle;
static vtss_thread_t netlink_monitor_thread_block;

mesa_rc vtss_ip_os_init(void) {
    I << __FUNCTION__;
    state.init(__LINE__, "ip_os");
    vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
                       netlink_monitor_thread,
                       0,
                       "IP2.os",
                       nullptr,
                       0,
                       &netlink_monitor_thread_handle,
                       &netlink_monitor_thread_block);

    // Enable gratuitous ARP
    VTSS_RC(vtss_sysctl_set_int("ipv4/conf/default/arp_notify", 1));

    // Avoid IPv4 routes with link down
    VTSS_RC(vtss_sysctl_set_int("ipv4/conf/default/ignore_routes_with_linkdown", 1));

    // Disable IPv6 on vtss.ifh to avoid incrementing global IPv6 statistics
    VTSS_RC(vtss_sysctl_set_int("ipv6/conf/" VTSS_NPI_DEVICE "/disable_ipv6", 1));

    // The NPI port should always be up... Link state is only being managed on
    // the derived vlan interfaces
    (void)vtss_ip_os_if_cmd(vtss::str(VTSS_NPI_DEVICE), RTM_NEWLINK,
                            NLM_F_REQUEST | NLM_F_ACK, IFF_UP, IFF_UP);

    return VTSS_RC_OK;
}

static mesa_rc parse_statistics(Map<str, int64_t> &m, str buf, str type) {
    parser::Int<int64_t, 10, 1> i;
    Vector<str> hdr(64), val(64);
    bool got_hdr = false, got_val = false;
    for (str line : LineIterator(buf)) {
        str h, t;
        if (!split(line, ':', h, t)) continue;

        t = trim(t);
        if (h == type) {
            if (!got_hdr) {
                hdr = split(t, ' ');
                got_hdr = true;
            } else {
                val = split(t, ' ');
                got_val = true;
                break;
            }
        }
    }

    if (!got_hdr || !got_val) {
        E << buf << "\ngot_hdr: " << got_hdr << " got_val: " << got_val;
        return VTSS_RC_ERROR;
    }

    if (hdr.size() != val.size()) {
        E << buf << "\nhdr.size() != val.size() " << hdr.size()
          << " != " << val.size();
        return VTSS_RC_ERROR;
    }

    for (auto h = hdr.begin(), v = val.begin();
         h != hdr.end() && v != val.end(); ++h, ++v) {
        const char *b = v->begin(), *e = v->end();
        if (i(b, e) && b == e) {
            m.set(*h, i.get());
        } else {
            E << "Failed to parse: " << *h << " -> " << *v;
        }
    }

    return VTSS_RC_OK;
}

mesa_rc vtss_ip_os_ipv4_system_statistics_get(vtss_appl_ip_if_status_ip_stat_t *s) {
    auto b1 = read_file_into_buf("/proc/net/snmp");
    if (!b1.size()) {
        E << "Failed to read /proc/net/snmp";
        return VTSS_RC_ERROR;
    }

    auto b2 = read_file_into_buf("/proc/net/netstat");
    if (!b2.size()) {
        E << "Failed to read /proc/net/netstat";
        return VTSS_RC_ERROR;
    }

    Map<str, int64_t> m;
    VTSS_RC(parse_statistics(m, str(b1), str("Ip")));
    VTSS_RC(parse_statistics(m, str(b2), str("IpExt")));

// Not used:
//  - DefaultTTL
//  - Forwarding
//  - InBcastOctets
//  - InCEPkts
//  - InCsumErrors
//  - InECT0Pkts
//  - InECT1Pkts
//  - InNoECTPkts
//  - OutBcastOctets
//  - ReasmTimeout

// Missing fields in vtss_appl_ip_if_status_ip_stat_t:
//  - DiscontinuityTime
//  - HCInForwDatagrams, InForwDatagrams
//  - HCOutTransmits, OutTransmits
//  - OutFragReqds
//  - RefreshRate

#define ASSIGN2(N, V)                    \
    {                                    \
        auto i = m.find(str(N));         \
        if (i == m.end()) {              \
            I << "No such field: " << N; \
        } else {                         \
            s->V = i->second;            \
            s->V##Valid = 1;             \
        }                                \
    }

#define ASSIGN3(N, V1, V2)               \
    {                                    \
        auto i = m.find(str(N));         \
        if (i == m.end()) {              \
            I << "No such field: " << N; \
        } else {                         \
            s->V1 = i->second;           \
            s->V2 = i->second;           \
            s->V1##Valid = 1;            \
            s->V2##Valid = 1;            \
        }                                \
    }

    memset(s, 0, sizeof(*s));
    ASSIGN3("ForwDatagrams", OutForwDatagrams, HCOutForwDatagrams);
    ASSIGN2("FragCreates", OutFragCreates);
    ASSIGN2("FragFails", OutFragFails);
    ASSIGN2("FragOKs", OutFragOKs);
    ASSIGN2("InAddrErrors", InAddrErrors);
    ASSIGN3("InBcastPkts", InBcastPkts, HCInBcastPkts);
    ASSIGN3("InDelivers", InDelivers, HCInDelivers);
    ASSIGN2("InDiscards", InDiscards);
    ASSIGN2("InHdrErrors", InHdrErrors);
    ASSIGN3("InMcastOctets", InMcastOctets, HCInMcastOctets);
    ASSIGN3("InMcastPkts", InMcastPkts, HCInMcastPkts);
    ASSIGN2("InNoRoutes", InNoRoutes);
    ASSIGN3("InOctets", InOctets, HCInOctets);
    ASSIGN3("InReceives", InReceives, HCInReceives);
    ASSIGN2("InTruncatedPkts", InTruncatedPkts);
    ASSIGN2("InUnknownProtos", InUnknownProtos);
    ASSIGN3("OutBcastPkts", OutBcastPkts, HCOutBcastPkts);
    ASSIGN2("OutDiscards", OutDiscards);
    ASSIGN3("OutMcastOctets", OutMcastOctets, HCOutMcastOctets);
    ASSIGN3("OutMcastPkts", OutMcastPkts, HCOutMcastPkts);
    ASSIGN2("OutNoRoutes", OutNoRoutes);
    ASSIGN3("OutOctets", OutOctets, HCOutOctets);
    ASSIGN3("OutRequests", OutRequests, HCOutRequests);
    ASSIGN2("ReasmFails", ReasmFails);
    ASSIGN2("ReasmOKs", ReasmOKs);
    ASSIGN2("ReasmReqds", ReasmReqds);

    return VTSS_RC_OK;
}

static mesa_rc parse_ipv6_statistics(const char *name, vtss_appl_ip_if_status_ip_stat_t *s)
{
    auto buf = read_file_into_buf(name);
    if (!buf.size()) {
        D << "Failed to read " << name;
        return VTSS_RC_ERROR;
    }

    Map<str, int64_t> m;
    for (str line : LineIterator(str(buf))) {
        parser::OneOrMore<parser::group::Word> w;
        parser::OneOrMore<parser::group::Space> sp;
        parser::Int<int64_t, 10, 1> i;

        const char *b = line.begin(), *e = line.end();
        if (parser::Group(b, e, w, sp, i)) {
            m.set(w.get(), i.get());
        } else {
            I << "Could not parse: " << line;
        }
    }

    // Not used:
    //  - Ip6InTooBigErrors
    //  - Ip6ReasmTimeout
    //  - Ip6InBcastOctets
    //  - Ip6OutBcastOctets
    //  - Ip6InNoECTPkts
    //  - Ip6InECT1Pkts
    //  - Ip6InECT0Pkts
    //  - Ip6InCEPkts

    // Missing fields in vtss_appl_ip_if_status_ip_stat_t:
    //  - DiscontinuityTime
    //  - HCInBcastPkts, InBcastPkts
    //  - HCInForwDatagrams, InForwDatagrams
    //  - HCOutBcastPkts, OutBcastPkts
    //  - HCOutTransmits, OutTransmits
    //  - OutFragReqds
    //  - RefreshRate

    memset(s, 0, sizeof(*s));
    ASSIGN2("Ip6FragCreates", OutFragCreates);
    ASSIGN2("Ip6FragFails", OutFragFails);
    ASSIGN2("Ip6FragOKs", OutFragOKs);
    ASSIGN2("Ip6InAddrErrors", InAddrErrors);
    ASSIGN3("Ip6InDelivers", InDelivers, HCInDelivers);
    ASSIGN2("Ip6InDiscards", InDiscards);
    ASSIGN2("Ip6InHdrErrors", InHdrErrors);
    ASSIGN3("Ip6InMcastOctets", InMcastOctets, HCInMcastOctets);
    ASSIGN3("Ip6InMcastPkts", InMcastPkts, HCInMcastPkts);
    ASSIGN2("Ip6InNoRoutes", InNoRoutes);
    ASSIGN3("Ip6InOctets", InOctets, HCInOctets);
    ASSIGN3("Ip6InReceives", InReceives, HCInReceives);
    ASSIGN2("Ip6InTruncatedPkts", InTruncatedPkts);
    ASSIGN2("Ip6InUnknownProtos", InUnknownProtos);
    ASSIGN2("Ip6OutDiscards", OutDiscards);
    ASSIGN3("Ip6OutForwDatagrams", OutForwDatagrams, HCOutForwDatagrams);
    ASSIGN3("Ip6OutMcastOctets", OutMcastOctets, HCOutMcastOctets);
    ASSIGN3("Ip6OutMcastPkts", OutMcastPkts, HCOutMcastPkts);
    ASSIGN2("Ip6OutNoRoutes", OutNoRoutes);
    ASSIGN3("Ip6OutOctets", OutOctets, HCOutOctets);
    ASSIGN3("Ip6OutRequests", OutRequests, HCOutRequests);
    ASSIGN2("Ip6ReasmFails", ReasmFails);
    ASSIGN2("Ip6ReasmOKs", ReasmOKs);
    ASSIGN2("Ip6ReasmReqds", ReasmReqds);

    return VTSS_RC_OK;
}

mesa_rc vtss_ip_os_ipv6_system_statistics_get(vtss_appl_ip_if_status_ip_stat_t *s)
{
    return parse_ipv6_statistics("/proc/net/snmp6", s);
}

mesa_rc vtss_ip_os_ipv6_vlan_statistics_get(mesa_vid_t vid, vtss_appl_ip_if_status_ip_stat_t *const s)
{
    StaticBuffer<IFNAMSIZ> name;
    char buf[64], *p = buf;
    const char *c;
    str ifname = ifname_from_vlan(&name, vid);

    p += sprintf(p, "/proc/net/dev_snmp6/");
    for (c = ifname.begin(); c < ifname.end(); c++, p++) {
        *p = *c;
    }
    *p = '\0';

    return parse_ipv6_statistics(buf, s);
}

mesa_rc vtss_ip_os_if_vlan_statistics_get(mesa_vid_t vid, vtss_appl_ip_if_status_link_stat_t *const s)
{
    const   char *p, *b = NULL, *e;
    int     cnt = 0;
    int64_t val[16];
    parser::Int<int64_t, 10, 1> i;

    auto buf = read_file_into_buf("/proc/net/dev");
    if (!buf.size()) {
        E << "Failed to read /proc/net/dev";
        return VTSS_RC_ERROR;
    }

    memset(s, 0, sizeof(*s));
    StaticBuffer<IFNAMSIZ> name;
    str h, t, ifname = ifname_from_vlan(&name, vid);
    for (str line : LineIterator(str(buf))) {
        if (split(line, ':', h, t) && strncmp(h.begin(), ifname.begin(), ifname.size() - 1) == 0) {
            for (p = t.begin(); p != t.end(); p++) {
                if (isdigit(*p)) {
                    if (b == NULL) {
                        b = p;
                    }
                } else if (b != NULL) {
                    e = p;
                    if (i(b, e) && b == e) {
                        val[cnt++] = i.get();
                    }
                    b = NULL;
                    if (cnt == 10) {
                        s->in_bytes = val[0];
                        s->in_packets = val[1];
                        s->out_bytes = val[8];
                        s->out_packets = val[9];
                        return VTSS_RC_OK;
                    }
                }
            }
        }
    }
    return VTSS_RC_ERROR;
}
