/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

#include "web_api.h"
#include "ip_api.h"
#include "ip_legacy.h"
#include "ip_utils.h"
#include "ping_api.h"
#include "icli_api.h"
#include "traceroute_api.h"
#include "mgmt_api.h"

#ifdef VTSS_SW_OPTION_PRIV_LVL
#include "vtss_privilege_api.h"
#include "vtss_privilege_web_api.h"
#endif

#ifdef VTSS_SW_OPTION_DNS
#include "vtss/appl/dns.h"
#endif /* VTSS_SW_OPTION_DNS */
#if defined(VTSS_SW_OPTION_DHCP6_CLIENT)
#include "vtss/appl/dhcp6_client.h"
#endif /* defined(VTSS_SW_OPTION_DHCP6_CLIENT) */

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_IP

/* =================
 * Trace definitions
 * -------------- */
#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_WEB
#include <vtss_trace_api.h>
/* ============== */


static BOOL rt4_parse(CYG_HTTPD_STATE *p, mesa_ipv4_uc_t *r, vtss_appl_ip_ipv4_route_conf_t *conf, int ix) {
    int prefix = 0, distance = 0;
    memset(r, 0, sizeof(*r));

    if ((web_parse_ipv4_fmt(p, &r->network.address, "rt_net_%d", ix) == VTSS_OK) &&
        (web_parse_ipv4_fmt(p, &r->destination, "rt_dest_%d", ix) == VTSS_OK) &&
        cyg_httpd_form_variable_int_fmt(p, &prefix, "rt_mask_%d", ix) && prefix >= 0 && prefix <= 32 &&
        cyg_httpd_form_variable_int_fmt(p, &distance, "rt_distance_or_nhvid_%d", ix) && (distance > 0)) {
        // Both IPv4/v6 route entry share the same field name 'rt_distance_or_nhvid_'
        conf->distance = distance;

        r->network.prefix_size = prefix;
        return TRUE;
    }
    return FALSE;
}

static BOOL rt6_parse(CYG_HTTPD_STATE *p, vtss_appl_ip_ipv6_route_conf_t *r,
                      int ix) {
    int prefix = 0;
    memset(r, 0, sizeof(*r));
    if (vtss_ip_hasipv6() &&
        (web_parse_ipv6_fmt(p, &r->route.network.address, "rt_net_%d", ix) == VTSS_OK) &&
        (web_parse_ipv6_fmt(p, &r->route.destination, "rt_dest_%d", ix) == VTSS_OK) &&
        cyg_httpd_form_variable_int_fmt(p, &prefix, "rt_mask_%d", ix) && prefix >= 0 && prefix <= 128) {
        int nhvid = 0;

        if (vtss_ipv6_addr_is_link_local(&r->route.destination) &&
            cyg_httpd_form_variable_int_fmt(p, &nhvid, "rt_distance_or_nhvid_%d", ix) && (nhvid > 0)) {
            nhvid = nhvid & 0xFFFF;
            (void) vtss_ifindex_from_vlan(nhvid, &r->interface);
        } else {
            r->interface = VTSS_IFINDEX_NONE;
        }
        r->route.network.prefix_size = prefix;
        return TRUE;
    }
    return FALSE;
}

static bool ip2_ipv4_config_differs(const vtss_appl_ip_ipv4_conf_t *ipc_old,
                                    const vtss_appl_ip_ipv4_conf_t *ipc_new)
{
    return memcmp(ipc_old, ipc_new, sizeof(vtss_appl_ip_ipv4_conf_t)) != 0;
}

static bool ip2_ipv6_config_differs(const vtss_appl_ip_ipv6_conf_t *ipc_old,
                                    const vtss_appl_ip_ipv6_conf_t *ipc_new)
{
    return memcmp(ipc_old, ipc_new, sizeof(vtss_appl_ip_ipv6_conf_t)) != 0;
}

static BOOL net_parse(CYG_HTTPD_STATE *p,
                      mesa_ip_type_t type,
                      mesa_ip_network_t *n,
                      int ix)
{
    int prefix = 0;
    memset(n, 0, sizeof(*n));
    if (type == MESA_IP_TYPE_IPV4 &&
        (web_parse_ipv4_fmt(p, &n->address.addr.ipv4, "if_addr_%d", ix) == VTSS_OK) &&
        cyg_httpd_form_variable_int_fmt(p, &prefix, "if_mask_%d", ix) && prefix >= 0 && prefix <= 32) {
        n->prefix_size = prefix;
        n->address.type = type;
        return TRUE;
    }
    if (type == MESA_IP_TYPE_IPV6 &&
        (web_parse_ipv6_fmt(p, &n->address.addr.ipv6, "if_addr6_%d", ix) == VTSS_OK) &&
        cyg_httpd_form_variable_int_fmt(p, &prefix, "if_mask6_%d", ix) && prefix >= 0 && prefix <= 128) {
        n->prefix_size = prefix;
        n->address.type = type;
        return TRUE;
    }
    return FALSE;
}

typedef struct {
    mesa_vid_t vid;
    BOOL change_ipv4, change_ipv6;
    vtss_appl_ip_ipv4_conf_t ipv4;
    vtss_appl_ip_ipv6_conf_t ipv6;
} ip_web_conf_t;

static i32 handler_ip_config(CYG_HTTPD_STATE *p)
{
    vtss_ip_global_param_priv_t conf;
    CapArray<mesa_routing_entry_t, VTSS_APPL_CAP_IP_ROUTE_CNT> routes;
    mesa_rc rc;
    vtss_ifindex_t ifidx;
    int i, route_ct, ct;
    char buf[128];
#ifdef VTSS_SW_OPTION_DNS
    i8                              *ds, *dv;
    u32                             *ref, nxt, idx;
    vtss_ifindex_elm_t              ife;
    vtss_appl_dns_server_conf_t     dns_serv;
    vtss_appl_dns_name_conf_t       dns_name;
    vtss_appl_dns_proxy_conf_t      dns_prxy;
#endif /* VTSS_SW_OPTION_DNS */
#if defined(VTSS_SW_OPTION_DHCP6_CLIENT)
    vtss_appl_dhcp6c_intf_conf_t    dhcp6c_cfg;
#endif /* defined(VTSS_SW_OPTION_DHCP6_CLIENT) */
    char errmsg[128];

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_IP)) {
        return -1;
    }
#endif

    T_D("handler_ip_config");

    if ((rc = vtss_ip_global_param_get_(&conf)) != VTSS_OK) {
        T_E("get global config: %s", error_txt(rc));
    }
    route_ct = 0;
    ct = routes.size();
#ifdef VTSS_SW_OPTION_DNS
    memset(&dns_prxy, 0x0, sizeof(vtss_appl_dns_proxy_conf_t));
    (void) vtss_appl_dns_proxy_config_get(&dns_prxy);
#endif /* VTSS_SW_OPTION_DNS */

    if (p->method == CYG_HTTPD_METHOD_POST) {
        int val;
        u32 uval;
        if (cyg_httpd_form_varable_int(p, "ip_mode", &val)) {
            conf.enable_routing = val;
        }
        if ((rc = vtss_ip_global_param_set_(&conf)) != VTSS_OK) {
            T_E("set global:  %s", error_txt(rc));
        }
#ifdef VTSS_SW_OPTION_DNS
        if (vtss_appl_dns_domain_name_config_get(&dns_name) == VTSS_RC_OK &&
            cyg_httpd_form_varable_int(p, "ip_dns_domain_type", &val) &&
            val >= VTSS_APPL_DNS_CONFIG_TYPE_NONE &&
            val <= VTSS_APPL_DNS_CONFIG_TYPE_DHCP6_VLAN) {
            memset(&dns_name, 0x0, sizeof(vtss_appl_dns_name_conf_t));
            dns_name.domainname_type = (vtss_appl_dns_config_type_t)val;
            if (dns_name.domainname_type == VTSS_APPL_DNS_CONFIG_TYPE_STATIC) {
                const char  *var_string;
                size_t      var_len;

                var_string = cyg_httpd_form_varable_string(p, "ip_dns_domain_value", &var_len);
                if (var_string) {
                    (void) cgi_unescape(var_string, dns_name.static_domain_name, var_len, sizeof(dns_name.static_domain_name));
                }
            }
            if (dns_name.domainname_type == VTSS_APPL_DNS_CONFIG_TYPE_DHCP4_VLAN ||
                dns_name.domainname_type == VTSS_APPL_DNS_CONFIG_TYPE_DHCP6_VLAN) {
                if (cyg_httpd_form_varable_int(p, "ip_dns_domain_value", &val)) {
                    (void) vtss_ifindex_from_vlan(val, &dns_name.dhcp_ifindex);
                }
            }

            if ((rc = vtss_appl_dns_domain_name_config_set(&dns_name)) != VTSS_RC_OK) {
                T_E("vtss_appl_dns_domain_name_config_set: %s", error_txt(rc));
            }
        }

        ref = NULL;
        while (vtss_appl_dns_server_config_itr(ref, &nxt) == VTSS_RC_OK) {
            idx = nxt;
            ref = &idx;
            if (vtss_appl_dns_server_config_get(&idx, &dns_serv) != VTSS_RC_OK) {
                T_D("Failed to get DNS server %u", idx);
                continue;
            }

            memset(buf, 0x0, sizeof(buf));
            ds = &buf[0];
            dv = &buf[(sizeof(buf) / 2)];
            sprintf(ds, "ip_dns_src_%u", idx);
            sprintf(dv, "ip_dns_value_%u", idx);

            if (cyg_httpd_form_varable_int(p, ds, &val) &&
                val >= VTSS_APPL_DNS_CONFIG_TYPE_NONE &&
                val <= VTSS_APPL_DNS_CONFIG_TYPE_DHCP6_VLAN) {
                BOOL    set_cfg = TRUE;

                memset(&dns_serv, 0x0, sizeof(vtss_appl_dns_server_conf_t));
                dns_serv.server_type = (vtss_appl_dns_config_type_t)val;
                if (dns_serv.server_type == VTSS_APPL_DNS_CONFIG_TYPE_STATIC &&
                    web_parse_ip_fmt(p, &dns_serv.static_ip_address, dv) != VTSS_RC_OK) {
                    T_E("Invalid IP address, not changing DNS config(%u)", idx);
                    set_cfg = FALSE;
                }
                if (dns_serv.server_type == VTSS_APPL_DNS_CONFIG_TYPE_DHCP4_VLAN ||
                    dns_serv.server_type == VTSS_APPL_DNS_CONFIG_TYPE_DHCP6_VLAN) {
                    if (cyg_httpd_form_varable_int(p, dv, &val) &&
                        vtss_ifindex_from_vlan(val, &dns_serv.static_ifindex) == VTSS_RC_OK) {
                    } else {
                        T_E("Invalid VLAN %d, not changing DNS config(%u)", val, idx);
                        set_cfg = FALSE;
                    }
                }

                if (set_cfg && (rc = vtss_appl_dns_server_config_set(&idx, &dns_serv)) != VTSS_RC_OK) {
                    T_E("vtss_appl_dns_server_config_set: %s", error_txt(rc));
                }
            }
        }

        dns_prxy.proxy_admin_state = cyg_httpd_form_variable_check_fmt(p, "ip_dns_proxy");
        if ((rc = vtss_appl_dns_proxy_config_set(&dns_prxy)) != VTSS_RC_OK) {
            T_E("set proxy dns: %s", error_txt(rc));
        }
#endif /* VTSS_SW_OPTION_DNS */
        memset(buf, 0x0, sizeof(buf));
        if (cyg_httpd_form_varable_int(p, "if_ct", &ct)) {
            CapArray<ip_web_conf_t, VTSS_APPL_CAP_IP_INTERFACE_CNT> cfg;
            i8  *ebp = &buf[0];
            int j = 0;
            for (i = 0; i < ct; i++) {
                int vid;
                BOOL vid_changed;
                cfg[j].change_ipv4 = cfg[j].change_ipv6 = vid_changed = FALSE;
                if (cyg_httpd_form_variable_int_fmt(p, &vid, "if_vid_%d", i)) {
                    /* Have a vid - what to do? */
                    (void) vtss_ifindex_from_vlan(vid, &ifidx);
                    if (cyg_httpd_form_variable_check_fmt(p, "if_del_%d", i)) {
                        /* Delete the interface */
                        if ((rc = vtss_appl_ip_if_conf_del(ifidx)) != VTSS_OK) {
                            T_E("Delete IP interface vid %d: %s", vid, error_txt(rc));
                        }
                    } else {
                        vtss_appl_ip_ipv4_conf_t ipconf4, ipconf4_old;
                        vtss_appl_ip_ipv6_conf_t ipconf6, ipconf6_old;

                        if (vtss_appl_ip_if_conf_get(ifidx) != VTSS_OK) {
                            if ((rc = vtss_appl_ip_if_conf_set(ifidx)) != VTSS_OK) {
                                T_E("Add IP interface vid %d: %s", vid, error_txt(rc));
                                continue;
                            }
                        }
                        /* IPv4 */
                        if ((rc = vtss_appl_ip_ipv4_conf_get(ifidx, &ipconf4_old)) == VTSS_OK) {
                            mesa_ip_network_t network;
                            size_t            var_string_len;
                            const char        *var_string;

                            /* Track original config */
                            ipconf4 = ipconf4_old;
                            ipconf4.active = FALSE;

                            /* DHCP enabled? */
                            ipconf4.dhcpc = cyg_httpd_form_variable_check_fmt(p, "if_dhcp_%d", i);
                            if (ipconf4.dhcpc) {
                                ipconf4.active = TRUE;
                            }

                            /* DHCP client_id */
                            int client_id_type = 0;
                            if (ipconf4.active && cyg_httpd_form_variable_int_fmt(p, &client_id_type, "if_client_id_type_%d", i)) {
                                memset(&ipconf4.dhcpc_params.client_id, 0, sizeof(ipconf4.dhcpc_params.client_id));
                                switch (client_id_type) {
                                    case VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_IFMAC:
                                        if (cyg_httpd_form_variable_u32_fmt(p, &uval, "if_client_id_if_mac_%d", i) &&
                                            uval != 0 /* 'none' option, no need to call vtss_ifindex_from_port() */)  {
                                            (void)vtss_ifindex_from_port(VTSS_ISID_LOCAL, uport2iport(uval), &ipconf4.dhcpc_params.client_id.if_mac);
                                        }
                                    break;
                                    case VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_ASCII: {
                                        var_string = cyg_httpd_form_variable_str_fmt(p, &var_string_len, "if_client_id_ascii_%d", i);
                                        if (var_string && var_string_len >= VTSS_APPL_IP_DHCP_CLIENT_ID_MIN_LENGTH) {
                                            (void) cgi_unescape(var_string, ipconf4.dhcpc_params.client_id.ascii, var_string_len, sizeof(ipconf4.dhcpc_params.client_id.ascii));
                                        }
                                        break;
                                    }
                                    case VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_HEX:
                                        var_string = cyg_httpd_form_variable_str_fmt(p, &var_string_len, "if_client_id_hex_%d", i);
                                        if (var_string && (var_string_len >= (VTSS_APPL_IP_DHCP_CLIENT_ID_MIN_LENGTH * 2)) &&
                                            ((var_string_len % 2) == 0) /* Must be even since one byte hex value is presented as two octets string */) {
                                            (void) cgi_unescape(var_string, ipconf4.dhcpc_params.client_id.hex, var_string_len, sizeof(ipconf4.dhcpc_params.client_id.hex));
                                        }
                                        break;
                                    case VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_AUTO:
                                    default:
                                        // Do nothing
                                        break;
                                }
                                ipconf4.dhcpc_params.client_id.type = (vtss_appl_ip_dhcpc_client_id_type_t) client_id_type;
                            } else {
                                 memset(&ipconf4.dhcpc_params.client_id, 0, sizeof(ipconf4.dhcpc_params.client_id));
                            }

                            /* DHCP hostname */
                            var_string = cyg_httpd_form_variable_str_fmt(p, &var_string_len, "if_hostname_%d", i);
                            if (var_string && ipconf4.active) {
                                (void) cgi_unescape(var_string, ipconf4.dhcpc_params.hostname, var_string_len, sizeof(ipconf4.dhcpc_params.hostname));
                            } else {
                                strcpy(ipconf4.dhcpc_params.hostname, "");
                            }

                            if (cyg_httpd_form_variable_u32_fmt(p, &uval, "if_tout_%d", i))  {
                                ipconf4.fallback_timeout = uval;
                            }
                            if (net_parse(p, MESA_IP_TYPE_IPV4, &network, i)) {
                                ipconf4.active = TRUE;
                                ipconf4.network.address = network.address.addr.ipv4;
                                ipconf4.network.prefix_size = network.prefix_size;
                            }
                            T_D("Vid: %u, Dhcp: %u, Active: %u", vid, ipconf4.dhcpc, ipconf4.active);
                            if (ip2_ipv4_config_differs(&ipconf4_old, &ipconf4)) {
                                T_D("Vid: %u, differs", vid);
                                cfg[j].vid = vid;
                                cfg[j].change_ipv4 = vid_changed = TRUE;
                                cfg[j].ipv4 = ipconf4;
                            }
                        } else {
                            T_E("Get IPv4 conf, vid %d: %s", vid, error_txt(rc));
                        }
                        /* IPv6 */
                        if (vtss_ip_hasipv6()) {
                            if ((rc = vtss_appl_ip_ipv6_conf_get(ifidx, &ipconf6_old)) == VTSS_OK) {
                                mesa_ip_network_t network;
                                ipconf6 = ipconf6_old;
                                if (net_parse(p, MESA_IP_TYPE_IPV6, &network, i)) {
                                    ipconf6.active = true;
                                    ipconf6.network.address = network.address.addr.ipv6;
                                    ipconf6.network.prefix_size = network.prefix_size;
                                } else {
                                    ipconf6.active = false;
                                }
                                if (ip2_ipv6_config_differs(&ipconf6_old, &ipconf6)) {
                                    cfg[j].vid = vid;
                                    cfg[j].change_ipv6 = vid_changed = TRUE;
                                    cfg[j].ipv6 = ipconf6;
                                }
                            } else {
                                T_E("Get IPv6 conf, vid %d: %s", vid, error_txt(rc));
                            }
#if defined(VTSS_SW_OPTION_DHCP6_CLIENT)
                            if (rc == VTSS_RC_OK) {
                                BOOL    new_if = FALSE, do_cfg;

                                if (vtss_appl_dhcp6c_interface_config_get(&ifidx, &dhcp6c_cfg) != VTSS_RC_OK) {
                                    if (vtss_appl_dhcp6c_interface_config_default(&dhcp6c_cfg) == VTSS_RC_OK) {
                                        new_if = TRUE;
                                    }
                                }

                                do_cfg = cyg_httpd_form_variable_check_fmt(p, "if_dhcp6_%d", i);
                                dhcp6c_cfg.rapid_commit = cyg_httpd_form_variable_check_fmt(p, "if_rpcmt_%d", i);
                                if (!new_if) {
                                    if (!do_cfg) {
                                        if ((rc = vtss_appl_dhcp6c_interface_config_del(&ifidx)) != VTSS_RC_OK) {
                                            T_D("DHCP6C_DEL(%d): %s", vid, error_txt(rc));
                                        }
                                    } else {
                                        if ((rc = vtss_appl_dhcp6c_interface_config_set(&ifidx, &dhcp6c_cfg)) != VTSS_RC_OK) {
                                            T_D("DHCP6C_SET(%d): %s", vid, error_txt(rc));
                                        }
                                    }
                                } else {
                                    if (do_cfg &&
                                        (rc = vtss_appl_dhcp6c_interface_config_add(&ifidx, &dhcp6c_cfg)) != VTSS_RC_OK) {
                                        T_D("DHCP6C_ADD(%d): %s", vid, error_txt(rc));
                                    }
                                }
                            }
#endif /* defined(VTSS_SW_OPTION_DHCP6_CLIENT) */
                        }
                    }
                }
                if (vid_changed) {
                    j++;        /* One more vid changed */
                }
            }
            if (j) {
                T_D("Need to reconfigure %d interfaces", j);
                /* De-activate changed IP interfaces */
                for (i = 0; i < j; i++) {
                    int vid = cfg[i].vid;
                    if (cfg[i].change_ipv4) {
                        vtss_appl_ip_ipv4_conf_t no_conf = {};
                        (void) vtss_ifindex_from_vlan(vid, &ifidx);
                        if ((rc = vtss_appl_ip_ipv4_conf_set(ifidx, &no_conf)) != VTSS_OK) {
                            T_E("Clear IPv4 conf, vid %d: %s", vid, error_txt(rc));
                        }
                    }
                    if (cfg[i].change_ipv6) {
                        vtss_appl_ip_ipv6_conf_t no_conf = {};
                        (void) vtss_ifindex_from_vlan(vid, &ifidx);
                        if ((rc = vtss_appl_ip_ipv6_conf_set(ifidx, &no_conf)) != VTSS_OK) {
                            T_E("Clear IPv6 conf, vid %d: %s", vid, error_txt(rc));
                        }
                    }
                }
                /* Re-activate changed IP interfaces */
                for (i = 0; i < j; i++) {
                    int vid = cfg[i].vid;
                    if (cfg[i].change_ipv4) {
                        (void) vtss_ifindex_from_vlan(vid, &ifidx);
                        if ((rc = vtss_appl_ip_ipv4_conf_set(ifidx, &cfg[i].ipv4)) != VTSS_OK) {
                            sprintf(errmsg, "Failed to set IPv4 configuration on vid %d: %s", vid, error_txt(rc));
                            redirect_errmsg(p, "ip_config.htm", errmsg);
                            return -1;
                        }
                    }
                    if (cfg[i].change_ipv6) {
                        (void) vtss_ifindex_from_vlan(vid, &ifidx);
                        if ((rc = vtss_appl_ip_ipv6_conf_set(ifidx, &cfg[i].ipv6)) != VTSS_OK) {
                            if (rc == IP_ERROR_ADDRESS_CONFLICT) {
                                int err_strlen = strlen(error_txt(IP_ERROR_ADDRESS_CONFLICT));
                                /*
                                    BZ#15858
                                    SLAAC may have multiple addresses assigned.
                                    So, we alert the setting failures instead.
                                    VID: 1 ~ 4095 -> strlen(VlanXXXX:ERR\n)
                                */
                                T_D("Set IPv6 conf, vid %d: %s", vid, error_txt(rc));
                                if (vid < 10) {
                                    err_strlen += 12;
                                } else {
                                    if (vid < 100) {
                                        err_strlen += 13;
                                    } else {
                                        if (vid < 1000) {
                                            err_strlen += 14;
                                        } else {
                                            err_strlen += 15;
                                        }
                                    }
                                }
                                if (sizeof(buf) - strlen(buf) > err_strlen) {
                                    sprintf(ebp, "Vlan%d:IPv6 %s\n", vid, error_txt(rc));
                                    ebp += err_strlen;
                                }
                            } else {
                                T_E("Set IPv6 conf, vid %d: %s", vid, error_txt(rc));
                            }
                        }
                    }
                }
            }
        }

        if (cyg_httpd_form_varable_int(p, "rt_ct", &ct) && ct > 0) {
            mesa_ipv4_uc_t r4;
            vtss_appl_ip_ipv6_route_conf_t r6;
            vtss_appl_ip_ipv4_route_conf_t route_conf;

            for (i = 0; i < ct; i++) {
                BOOL do_delete = cyg_httpd_form_variable_check_fmt(p, "rt_del_%d", i);
                if (rt4_parse(p, &r4, &route_conf, i)) {
                    if (do_delete) {
                        rc = vtss_appl_ip_ipv4_route_conf_del(&r4);
                    } else {
                        rc = vtss_appl_ip_ipv4_route_conf_set(&r4, &route_conf);
                    }
                } else if (rt6_parse(p, &r6, i)) {
                    if (do_delete) {
                        rc = vtss_appl_ip_ipv6_route_conf_del(&r6);
                    } else {
                        rc = vtss_appl_ip_ipv6_route_conf_set(&r6);
                    }
                }

                if (rc != VTSS_OK && rc != IP_ERROR_EXISTS) {
                    sprintf(errmsg, "Operation failed: %s", error_txt(rc));
                    redirect_errmsg(p, "ip_config.htm", errmsg);
                    return -1;
                }
            }
        }

        if (strlen(buf) > 0) {
            redirect_errmsg(p, "ip_config.htm", buf);
        } else {
            redirect(p, "/ip_config.htm");
        }
    } else {
        vtss_if_id_vlan_t vid;
        cyg_httpd_start_chunked("html");
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                      "%d|%d|%d",
                      MESA_CAP(VTSS_APPL_CAP_IP_INTERFACE_CNT),
                      MESA_CAP(VTSS_APPL_CAP_IP_ROUTE_CNT),
                      conf.enable_routing);
        cyg_httpd_write_chunked(p->outbuffer, ct);
#ifdef VTSS_SW_OPTION_DNS
        if (vtss_appl_dns_domain_name_config_get(&dns_name) == VTSS_RC_OK) {
            char    encoded_buf[(4 * VTSS_APPL_DNS_MAX_STRING_LEN) + 1];

            memset(encoded_buf, 0x0, sizeof(encoded_buf));
            switch ( dns_name.domainname_type ) {
            case VTSS_APPL_DNS_CONFIG_TYPE_STATIC:
                (void) cgi_escape(dns_name.static_domain_name, encoded_buf);
                break;
            case VTSS_APPL_DNS_CONFIG_TYPE_DHCP4_VLAN:
            case VTSS_APPL_DNS_CONFIG_TYPE_DHCP6_VLAN:
                vid = VTSS_VID_NULL;
                if (vtss_ifindex_decompose(dns_name.dhcp_ifindex, &ife) == VTSS_RC_OK &&
                    ife.iftype == VTSS_IFINDEX_TYPE_VLAN) {
                    vid = (vtss_if_id_vlan_t)ife.ordinal;
                }

                (void) snprintf(encoded_buf, sizeof(encoded_buf), "%d", vid);
                break;
            default:
                break;
            }
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                          ";%d|%s", dns_name.domainname_type, encoded_buf);
            cyg_httpd_write_chunked(p->outbuffer, ct);
        }

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), ";%d", dns_prxy.proxy_admin_state);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        ref = NULL;
        while (vtss_appl_dns_server_config_itr(ref, &nxt) == VTSS_RC_OK) {
            idx = nxt;
            ref = &idx;
            if (vtss_appl_dns_server_config_get(&idx, &dns_serv) != VTSS_RC_OK) {
                T_D("Failed to get DNS server %u", idx);
                continue;
            }

            switch ( dns_serv.server_type ) {
            case VTSS_APPL_DNS_CONFIG_TYPE_STATIC:
                (void) vtss_ip_ip_addr_to_txt(buf, sizeof(buf), &dns_serv.static_ip_address);
                break;
            case VTSS_APPL_DNS_CONFIG_TYPE_DHCP4_VLAN:
            case VTSS_APPL_DNS_CONFIG_TYPE_DHCP6_VLAN:
                vid = VTSS_VID_NULL;
                if (vtss_ifindex_decompose(dns_serv.static_ifindex, &ife) == VTSS_RC_OK &&
                    ife.iftype == VTSS_IFINDEX_TYPE_VLAN) {
                    vid = (vtss_if_id_vlan_t)ife.ordinal;
                }

                (void) snprintf(buf, sizeof(buf), "%d", vid);
                break;
            default:
                buf[0] = '\0';
                break;
            }
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                          "%s%d|%s", idx ? "/" : ";", dns_serv.server_type, buf);
            cyg_httpd_write_chunked(p->outbuffer, ct);
        }
#endif /* VTSS_SW_OPTION_DNS */
        cyg_httpd_write_chunked(",", 1);
        vid = VTSS_VID_NULL;
        while (vtss_ip_if_id_next(vid, &vid) == VTSS_OK) {
            vtss_appl_ip_ipv4_conf_t ipconf;
            vtss_appl_ip_ipv6_conf_t ipconf6;
            vtss_appl_ip_if_status_t ifstat;
            u32 ifct;

            (void) vtss_ifindex_from_vlan(vid, &ifidx);
            rc = vtss_appl_ip_ipv4_conf_get(ifidx, &ipconf);
            if (rc != VTSS_OK) {
                T_E("interface_config(%d): %s", vid, error_txt(rc));
                goto NEXT;
            }

            /* Vid */
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d#", vid);
            cyg_httpd_write_chunked(p->outbuffer, ct);

            /* dhcp + hostname + fallback */
            if (ipconf.active) {
                vtss_ifindex_elm_t elm;
                char encoded_string[3 * VTSS_APPL_IP_DHCP_HOSTNAME_MAX_LENGTH];

                // output dhcp enable?
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d", ipconf.dhcpc);
                cyg_httpd_write_chunked(p->outbuffer, ct);

                // output client_id_type/client_id_if_mac/client_id_ascii
                encoded_string[0] = '\0';
                if (ipconf.dhcpc_params.client_id.type == VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_IFMAC) {
                    (void)vtss_ifindex_decompose(ipconf.dhcpc_params.client_id.if_mac, &elm);
                } else if (ipconf.dhcpc_params.client_id.type == VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_ASCII) {
                    (void) cgi_escape(ipconf.dhcpc_params.client_id.ascii, encoded_string);
                }
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                              "#%d#%d#%s#",
                              /* client_id_type   */ ipconf.dhcpc_params.client_id.type,
                              /* client_id_if_mac */ (ipconf.dhcpc_params.client_id.type == VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_IFMAC) ? iport2uport(elm.ordinal) : 0,
                              /* client_id_ascii  */ encoded_string);
                cyg_httpd_write_chunked(p->outbuffer, ct);

                // output client_id_hex
                if (ipconf.dhcpc_params.client_id.type == VTSS_APPL_IP_DHCPC_CLIENT_ID_TYPE_HEX) {
                    size_t hex_str_len = strlen(ipconf.dhcpc_params.client_id.hex);
                    for (i = 0; i < hex_str_len; i++) {
                        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%c", toupper(ipconf.dhcpc_params.client_id.hex[i]));
                        cyg_httpd_write_chunked(p->outbuffer, ct);
                    }
                }

                // output hostname/fallback timeout
                encoded_string[0] = '\0';
                if (strlen(ipconf.dhcpc_params.hostname)) {
                    (void) cgi_escape(ipconf.dhcpc_params.hostname, encoded_string);
                }
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                              "#%s#%u#",
                              /* hostname         */ encoded_string,
                              /* fallback timeout */ ipconf.fallback_timeout);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            } else {
                cyg_httpd_write_chunked("0#0#1####0#", 11);
            }

            /* static configured address */
            if (!ipconf.active || (ipconf.dhcpc && ipconf.fallback_timeout == 0)) {
                cyg_httpd_write_chunked("##", 2);
            } else {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                              VTSS_IPV4_FORMAT"#%d#",
                              VTSS_IPV4_ARGS(ipconf.network.address),
                              ipconf.network.prefix_size);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            }

            /* IPv4 - current */
            if (vtss_ip_if_status_get(VTSS_APPL_IP_IF_STATUS_TYPE_IPV4, vid, 1, &ifct, &ifstat) == VTSS_OK &&
                    ifct == 1 &&
                    ifstat.type == VTSS_APPL_IP_IF_STATUS_TYPE_IPV4) {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                        VTSS_IPV4N_FORMAT,
                        VTSS_IPV4N_ARG(ifstat.u.ipv4.net));
                cyg_httpd_write_chunked(p->outbuffer, ct);
            }
            cyg_httpd_write_chunked("#", 1);

            if (vtss_ip_hasipv6() && (rc = vtss_appl_ip_ipv6_conf_get(ifidx, &ipconf6)) == VTSS_OK) {
                /* IPv6 - configured */
                if (ipconf6.active) {
                    (void) misc_ipv6_txt(&ipconf6.network.address, buf);
                    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s#%d#", buf, ipconf6.network.prefix_size);
                    cyg_httpd_write_chunked(p->outbuffer, ct);
                } else {
                    cyg_httpd_write_chunked("##", 2);
                }
            }
#if defined(VTSS_SW_OPTION_DHCP6_CLIENT)
            if (vtss_appl_dhcp6c_interface_config_get(&ifidx, &dhcp6c_cfg) == VTSS_RC_OK) {
                vtss_appl_dhcp6c_interface_t    dhcp6c_intf;

                if (vtss_appl_dhcp6c_interface_status_get(&ifidx, &dhcp6c_intf) == VTSS_RC_OK) {
                    (void) misc_ipv6_txt(&dhcp6c_intf.address, buf);
                    ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                                  "1#%d#%s#", dhcp6c_cfg.rapid_commit, buf);
                } else {
                    ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                                  "1#%d##", dhcp6c_cfg.rapid_commit);
                }
                cyg_httpd_write_chunked(p->outbuffer, ct);
            } else {
                cyg_httpd_write_chunked("0#0##", 5);
            }
#endif /* defined(VTSS_SW_OPTION_DHCP6_CLIENT) */
NEXT:
            cyg_httpd_write_chunked("|", 1); /* End of interface */
        }

        cyg_httpd_write_chunked(",", 1);
        if (vtss_ip_route_conf_get(IP_MAX_ROUTES, (mesa_routing_entry_t *)&routes[0], &route_ct) == VTSS_OK) {
        for (i = 0; i < route_ct; i++) {
            mesa_routing_entry_t const *rt = &routes[i];
            switch (rt->type) {
            case MESA_ROUTING_ENTRY_TYPE_IPV4_UC: {
                vtss_appl_ip_ipv4_route_conf_t route_conf;
                if (vtss_appl_ip_ipv4_route_conf_get(&rt->route.ipv4_uc, &route_conf) != VTSS_OK) {
                    continue;
                }

                ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                              VTSS_IPV4_FORMAT "#%d#" VTSS_IPV4_FORMAT "#%u|",
                              VTSS_IPV4N_ARG(rt->route.ipv4_uc.network),
                              VTSS_IPV4_ARGS(rt->route.ipv4_uc.destination),
                              route_conf.distance);
                cyg_httpd_write_chunked(p->outbuffer, ct);
                break;
            }
            case MESA_ROUTING_ENTRY_TYPE_IPV6_UC:
                if (vtss_ip_hasipv6()) {
                    (void) misc_ipv6_txt(&rt->route.ipv6_uc.network.address, buf);
                    ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                                  "%s#%d#", buf, rt->route.ipv6_uc.network.prefix_size);

                    memset(buf, 0x0, sizeof(buf));
                    misc_ipv6_txt(&rt->route.ipv6_uc.destination, buf);
                    ct += snprintf(p->outbuffer + ct, sizeof(p->outbuffer) - ct,
                                   "%s#%u|", buf, rt->vlan);
                    cyg_httpd_write_chunked(p->outbuffer, ct);
                }
                break;
            default:
                ;
                }
            }
        }
        cyg_httpd_end_chunked();
    }

    return -1; // Do not further search the file system.
}

CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_config_ip, "/config/ip2_config", handler_ip_config);

static i32 handler_diag_ping(CYG_HTTPD_STATE *p)
{
    char            dest_ipaddr[VTSS_APPL_SYSUTIL_INPUT_DOMAIN_NAME_LEN + 1];
    char            src_ipaddr[VTSS_APPL_SYSUTIL_INPUT_HOSTNAME_LEN + 1];
    const char      *dest_addr_buf;
    const char      *src_addr_buf;
    size_t          dest_len, src_len;
    cli_iolayer_t   *web_io = NULL;
    ulong           ioindex;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_PING)) {
        return -1;
    }
#endif

    if (p->method == CYG_HTTPD_METHOD_POST) {
        u32 src_vid = 0, ping_length = 2, count = 5, interval = 0, ttlvalue = 0, pdata = 0;
        u32 src_portno = 0;
        BOOL quiet = FALSE;
        const char *pdata_str;
        size_t pdata_len = 0;
        char str_buff[64];

        dest_addr_buf = cyg_httpd_form_varable_string(p, "ip_addr", &dest_len);
        src_addr_buf = cyg_httpd_form_varable_string(p, "src_addr", &src_len);

        (void) cyg_httpd_form_varable_long_int(p, "src_vid", &src_vid);
        (void) cyg_httpd_form_varable_long_int(p, "length", &ping_length);
        (void) cyg_httpd_form_varable_long_int(p, "count", &count);
        (void) cyg_httpd_form_varable_long_int(p, "ttlvalue", &ttlvalue);
        (void) cyg_httpd_form_varable_long_int(p, "src_portno", &src_portno);

        // check for hex notation (0x<value>) for payload data field
        pdata_str = cyg_httpd_form_varable_string(p, "pdata", &pdata_len);
        if (pdata_str != nullptr && pdata_len >= 3 && pdata_str[0] == '0' && tolower(pdata_str[1]) == 'x') {
            pdata = strtoul(pdata_str, nullptr, 16);

        } else {
            (void) cyg_httpd_form_varable_long_int(p, "pdata", &pdata);
        }

        quiet = (cyg_httpd_form_varable_find(p, "quiet") != NULL);

        if (dest_addr_buf && ping_length) {
            dest_len = MIN(dest_len, sizeof(dest_ipaddr) - 1);
            strncpy(dest_ipaddr, dest_addr_buf, dest_len);
            dest_ipaddr[dest_len] = '\0';

            src_len = MIN(src_len, sizeof(src_ipaddr) - 1);
            strncpy(src_ipaddr, src_addr_buf, src_len);
            src_ipaddr[src_len] = '\0';

            if (src_portno > 0) {
                mesa_port_no_t iport = uport2iport(src_portno);
                mesa_ip_addr_t check_addr;

                if (vtss_ip_ip_by_port(iport, MESA_IP_TYPE_IPV4, &check_addr) != VTSS_RC_OK) {
                    const char *err = "Invalid source port number or address not configured.";
                    if ( strcmp(web_ver(p), "2.0") == 0 ) {
                        send_custom_textplain_resp(p, 400, err);
                    } else {
                        send_custom_error(p, "Ping Arguments Failure", err, strlen(err));
                    }
                    return -1;
                }

                (void) icli_ipv4_to_str(check_addr.addr.ipv4, src_ipaddr);
            }

            web_io = ping_test_async(dest_ipaddr, src_ipaddr, src_vid, count, interval, ping_length, pdata, ttlvalue, quiet);
            if (web_io) {
                /* Request header of WEB2.0 will have a header ("Lntn-Web-Ver": "2.0")
                 * Therefore, we use this header to seperate different action in different version.
                 */
                if ( strcmp(web_ver(p), "2.0") == 0 ) {
                    char msg[11];
                    sprintf(msg, "0x%08x", web_io);
                    send_custom_textplain_resp(p, 200, msg);
                } else {
                    sprintf(str_buff, "/ping4_result.htm?ioIndex=%p", web_io);
                    redirect(p, str_buff);
                }
            } else {
                const char *err = "No available internal resource, please try again later.";
                if ( strcmp(web_ver(p), "2.0") == 0 ) {
                    send_custom_textplain_resp(p, 500, err);
                } else {
                    send_custom_error(p, "Ping Fail", err, strlen(err));
                }
            }
        }
    } else {                    /* CYG_HTTPD_METHOD_GET (+HEAD) */
        if ((dest_addr_buf = cyg_httpd_form_varable_find(p, "ioIndex"))) {
            (void) cyg_httpd_str_to_hex(dest_addr_buf, &ioindex);
            web_io = (cli_iolayer_t *) ioindex;
            web_send_iolayer_output(WEB_CLI_IO_TYPE_PING, web_io, "html");
        }else{
            cyg_httpd_start_chunked("html");
            cyg_httpd_end_chunked();
        }
    }

    return -1; // Do not further search the file system.
}

CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_diag_ping, "/config/ping4", handler_diag_ping);

#ifdef VTSS_SW_OPTION_IPV6
static i32 handler_diag_ping_ipv6(CYG_HTTPD_STATE *p)
{
    char            dest_ipaddr[VTSS_APPL_SYSUTIL_INPUT_DOMAIN_NAME_LEN + 1];
    char            src_ipaddr[VTSS_APPL_SYSUTIL_INPUT_DOMAIN_NAME_LEN + 1];
    const char      *dest_addr_buf;
    const char      *src_addr_buf;
    size_t          dest_len, src_len;
    cli_iolayer_t   *web_io = NULL;
    ulong           ioindex;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_PING)) {
        return -1;
    }
#endif

    if (p->method == CYG_HTTPD_METHOD_POST) {
        u32 src_vid = 0, ping_length = 2, count = 5, interval = 0, pdata = 0;
        u32 src_portno = 0;
        BOOL quiet = FALSE;
        const char *pdata_str;
        size_t pdata_len = 0;
        char str_buff[64];

        dest_addr_buf = cyg_httpd_form_varable_string(p, "ipv6_addr", &dest_len);
        src_addr_buf = cyg_httpd_form_varable_string(p, "src_addr", &src_len);

        (void) cyg_httpd_form_varable_long_int(p, "src_vid", &src_vid);
        (void)cyg_httpd_form_varable_long_int(p, "length", &ping_length);
        (void)cyg_httpd_form_varable_long_int(p, "count", &count);
        (void) cyg_httpd_form_varable_long_int(p, "src_portno", &src_portno);

        // check for hex notation (0x<value>) for payload data field
        pdata_str = cyg_httpd_form_varable_string(p, "pdata", &pdata_len);
        if (pdata_str != nullptr && pdata_len >= 3 && pdata_str[0] == '0' && tolower(pdata_str[1]) == 'x') {
            pdata = strtoul(pdata_str, nullptr, 16);

        } else {
            (void) cyg_httpd_form_varable_long_int(p, "pdata", &pdata);
        }

        quiet = (cyg_httpd_form_varable_find(p, "quiet") != NULL);

        if (dest_addr_buf) {
            dest_len = MIN(dest_len, sizeof(dest_ipaddr) - 1);
            memset(dest_ipaddr, 0x0, sizeof(dest_ipaddr));
            (void) cgi_unescape(dest_addr_buf, dest_ipaddr, dest_len, sizeof(dest_ipaddr));

            src_len = MIN(src_len, sizeof(src_ipaddr) - 1);
            memset(src_ipaddr, 0x0, sizeof(src_ipaddr));
            (void) cgi_unescape(src_addr_buf, src_ipaddr, src_len, sizeof(src_ipaddr));

            if (src_portno > 0) {
                mesa_port_no_t iport = uport2iport(src_portno);
                mesa_ip_addr_t check_addr;

                if (vtss_ip_ip_by_port(iport, MESA_IP_TYPE_IPV6, &check_addr) != VTSS_RC_OK) {
                    const char *err = "Invalid source port number or address not configured.";
                    if ( strcmp(web_ver(p), "2.0") == 0 ) {
                        send_custom_textplain_resp(p, 400, err);
                    } else {
                        send_custom_error(p, "Ping Arguments Failure", err, strlen(err));
                    }

                    return -1;
                }

                (void) icli_ipv6_to_str(check_addr.addr.ipv6, src_ipaddr);
            }

            web_io = ping6_test_async(dest_ipaddr, src_ipaddr, src_vid, count, interval, ping_length, pdata, quiet);
            if (web_io) {
                if ( strcmp(web_ver(p), "2.0") == 0 ) {
                    char msg[11];
                    sprintf(msg, "0x%08x", web_io);
                    send_custom_textplain_resp(p, 200, msg);
                } else {
                    sprintf(str_buff, "/ping6_result.htm?ioIndex=%p", web_io);
                    redirect(p, str_buff);
                }
            } else {
                const char *err = "No available internal resource, please try again later.";
                if ( strcmp(web_ver(p), "2.0") == 0 ) {
                    send_custom_textplain_resp(p, 500, err);
                } else {
                    send_custom_error(p, "Ping Fail", err, strlen(err));
                }
            }
        }
    } else {                    /* CYG_HTTPD_METHOD_GET (+HEAD) */
        if ((dest_addr_buf = cyg_httpd_form_varable_find(p, "ioIndex"))) {
            (void) cyg_httpd_str_to_hex(dest_addr_buf, &ioindex);
            web_io = (cli_iolayer_t *) ioindex;
            web_send_iolayer_output(WEB_CLI_IO_TYPE_PING, web_io, "html");
        }else{
            cyg_httpd_start_chunked("html");
            cyg_httpd_end_chunked();
        }
    }

    return -1; // Do not further search the file system.
}

CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_diag_ping_ipv6, "/config/ping6", handler_diag_ping_ipv6);
#endif /* VTSS_SW_OPTION_IPV6 */


static i32 handler_diag_traceroute(CYG_HTTPD_STATE *p)
{
    char            dest_ipaddr[VTSS_APPL_SYSUTIL_INPUT_DOMAIN_NAME_LEN + 1];
    char            src_ipaddr[VTSS_APPL_SYSUTIL_INPUT_HOSTNAME_LEN + 1];
    const char      *dest_addr_buf;
    const char      *src_addr_buf;
    size_t          dest_len, src_len;
    cli_iolayer_t   *web_io = NULL;
    ulong           ioindex;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_TRACEROUTE)) {
        return -1;
    }
#endif

    if (p->method == CYG_HTTPD_METHOD_POST) {
        int src_vid = 0, dscp = -1, timeout = -1, probes = -1, firstttl = -1, maxttl = -1;
        BOOL icmp = FALSE, numeric = FALSE;
        char str_buff[64];

        dest_addr_buf = cyg_httpd_form_varable_string(p, "ip_addr", &dest_len);
        src_addr_buf = cyg_httpd_form_varable_string(p, "src_addr", &src_len);

        (void) cyg_httpd_form_varable_long_int(p, "src_vid", &src_vid);
        (void) cyg_httpd_form_varable_long_int(p, "dscp", &dscp);
        (void) cyg_httpd_form_varable_long_int(p, "probes", &probes);
        (void) cyg_httpd_form_varable_long_int(p, "timeout", &timeout);
        (void) cyg_httpd_form_varable_long_int(p, "firstttl", &firstttl);
        (void) cyg_httpd_form_varable_long_int(p, "maxttl", &maxttl);

        icmp = (cyg_httpd_form_varable_find(p, "icmp") != NULL);
        numeric = (cyg_httpd_form_varable_find(p, "numeric") != NULL);

        if (dest_addr_buf) {
            dest_len = MIN(dest_len, sizeof(dest_ipaddr) - 1);
            strncpy(dest_ipaddr, dest_addr_buf, dest_len);
            dest_ipaddr[dest_len] = '\0';

            src_len = MIN(src_len, sizeof(src_ipaddr) - 1);
            strncpy(src_ipaddr, src_addr_buf, src_len);
            src_ipaddr[src_len] = '\0';

            web_io = traceroute_test_async(dest_ipaddr, src_ipaddr, src_vid, dscp, timeout, probes,
                                           firstttl, maxttl, icmp, numeric);
            if (web_io) {
                if ( strcmp(web_ver(p), "2.0") == 0 ) {
                    char msg[11];
                    sprintf(msg, "0x%08x", web_io);
                    send_custom_textplain_resp(p, 200, msg);
                } else {
                    sprintf(str_buff, "/traceroute4_result.htm?ioIndex=%p", web_io);
                    redirect(p, str_buff);
                }
            } else {
                const char *err = "No available internal resource, please try again later.";
                if ( strcmp(web_ver(p), "2.0") == 0 ) {
                    send_custom_textplain_resp(p, 500, err);
                } else {
                    send_custom_error(p, "Traceroute Fail", err, strlen(err));
                }
            }
        }

    } else {                    /* CYG_HTTPD_METHOD_GET (+HEAD) */
        if ((dest_addr_buf = cyg_httpd_form_varable_find(p, "ioIndex"))) {
            (void) cyg_httpd_str_to_hex(dest_addr_buf, &ioindex);
            web_io = (cli_iolayer_t *) ioindex;
            // Traceroute uses same IO type as Ping for convenience
            web_send_iolayer_output(WEB_CLI_IO_TYPE_PING, web_io, "html");
        }else{
            cyg_httpd_start_chunked("html");
            cyg_httpd_end_chunked();
        }
    }

    return -1; // Do not further search the file system.
}

CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_diag_traceroute, "/config/traceroute4", handler_diag_traceroute);

#ifdef VTSS_SW_OPTION_IPV6
static i32 handler_diag_traceroute_ipv6(CYG_HTTPD_STATE *p)
{
    char            dest_ipaddr[VTSS_APPL_SYSUTIL_INPUT_DOMAIN_NAME_LEN + 1];
    char            src_ipaddr[VTSS_APPL_SYSUTIL_INPUT_DOMAIN_NAME_LEN + 1];
    const char      *dest_addr_buf;
    const char      *src_addr_buf;
    size_t          dest_len, src_len;
    cli_iolayer_t   *web_io = NULL;
    ulong           ioindex;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_TRACEROUTE)) {
        return -1;
    }
#endif

    if (p->method == CYG_HTTPD_METHOD_POST) {
        int src_vid = 0, dscp = -1, timeout = -1, probes = -1, maxttl = -1;
        BOOL numeric = FALSE;
        char str_buff[64];

        dest_addr_buf = cyg_httpd_form_varable_string(p, "ipv6_addr", &dest_len);
        src_addr_buf = cyg_httpd_form_varable_string(p, "src_addr", &src_len);

        (void) cyg_httpd_form_varable_long_int(p, "src_vid", &src_vid);
        (void) cyg_httpd_form_varable_long_int(p, "dscp", &dscp);
        (void) cyg_httpd_form_varable_long_int(p, "probes", &probes);
        (void) cyg_httpd_form_varable_long_int(p, "timeout", &timeout);
        (void) cyg_httpd_form_varable_long_int(p, "maxttl", &maxttl);

        numeric = (cyg_httpd_form_varable_find(p, "numeric") != NULL);

        if (dest_addr_buf) {
            dest_len = MIN(dest_len, sizeof(dest_ipaddr) - 1);
            memset(dest_ipaddr, 0x0, sizeof(dest_ipaddr));
            (void) cgi_unescape(dest_addr_buf, dest_ipaddr, dest_len, sizeof(dest_ipaddr));

            src_len = MIN(src_len, sizeof(src_ipaddr) - 1);
            memset(src_ipaddr, 0x0, sizeof(src_ipaddr));
            (void) cgi_unescape(src_addr_buf, src_ipaddr, src_len, sizeof(src_ipaddr));

            web_io = traceroute6_test_async(dest_ipaddr, src_ipaddr, src_vid, dscp, timeout, probes,
                                            maxttl, numeric);
            if (web_io) {
                if ( strcmp(web_ver(p), "2.0") == 0 ) {
                    char msg[11];
                    sprintf(msg, "0x%08x", web_io);
                    send_custom_textplain_resp(p, 200, msg);
                } else {
                    sprintf(str_buff, "/traceroute6_result.htm?ioIndex=%p", web_io);
                    redirect(p, str_buff);
                }
            } else {
                const char *err = "No available internal resource, please try again later.";
                if ( strcmp(web_ver(p), "2.0") == 0 ) {
                    send_custom_textplain_resp(p, 500, err);
                } else {
                    send_custom_error(p, "Traceroute Fail", err, strlen(err));
                }
            }
        }

    } else {                    /* CYG_HTTPD_METHOD_GET (+HEAD) */
        if ((dest_addr_buf = cyg_httpd_form_varable_find(p, "ioIndex"))) {
            (void) cyg_httpd_str_to_hex(dest_addr_buf, &ioindex);
            web_io = (cli_iolayer_t *) ioindex;
            // Traceroute uses same IO type as Ping for convenience
            web_send_iolayer_output(WEB_CLI_IO_TYPE_PING, web_io, "html");
        }else{
            cyg_httpd_start_chunked("html");
            cyg_httpd_end_chunked();
        }
    }

    return -1; // Do not further search the file system.
}

CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_diag_traceroute6, "/config/traceroute6", handler_diag_traceroute_ipv6);
#endif

/*lint -sem(ip2_printf, thread_protected) ... function only called from web server */
static int ip2_printf(const char *fmt, ...)
{
    int ct;
    va_list ap; /*lint -e{530} ... 'ap' is initialized by va_start() */

    va_start(ap, fmt);
    ct = vsnprintf(httpstate.outbuffer, sizeof(httpstate.outbuffer), fmt, ap);
    va_end(ap);
    cyg_httpd_write_chunked(httpstate.outbuffer, ct);

    return ct;
}

static i32 handler_ip_status(CYG_HTTPD_STATE *p)
{
#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_IP)) {
        return -1;
    }
#endif

    cyg_httpd_start_chunked("html");

    (void) vtss_ip_if_print(ip2_printf, FALSE, VTSS_APPL_IP_IF_STATUS_TYPE_ANY);

    cyg_httpd_write_chunked("^@^@^", 5); /* Split interfaces and routes */

    (void) vtss_ip_route_print(MESA_ROUTING_ENTRY_TYPE_IPV4_UC, ip2_printf);
    (void) vtss_ip_route_print(MESA_ROUTING_ENTRY_TYPE_IPV6_UC, ip2_printf);

    cyg_httpd_write_chunked("^@^@^", 5); /* Split routes and neighbours */

    (void) vtss_ip_nb_print(MESA_IP_TYPE_IPV4, ip2_printf);
    (void) vtss_ip_nb_print(MESA_IP_TYPE_IPV6, ip2_printf);

    cyg_httpd_end_chunked();

    return -1; // Do not further search the file system.
}

CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_stat_ip, "/stat/ip2_status", handler_ip_status);

/****************************************************************************/
/*  Module JS lib config routine                                            */
/****************************************************************************/

static size_t ip2_lib_config_js(char **base_ptr, char **cur_ptr, size_t *length)
{
    char buff[2048];

    (void) snprintf(buff, sizeof(buff),
                    "var configIPMaxInterfaces = %d;\n"
                    "var configIPMaxRoutes = %d;\n"
                    "var configIPClientIdLenMin = %d;\n"
                    "var configIPClientIdLenMax = %d;\n"
                    "var configIPHostnameLenMax = %d;\n"
                    "var configIPv6Support = %d;\n"
                    "var configDHCPv6Support = %d;\n"
                    "var configIPRoutingSupport = %d;\n"
                    "var configIPDNSSupport = %d;\n"
                    "var configPingLenMin = %d;\n"
                    "var configPingLenMax = %d;\n"
                    "var configPingCntMin = %d;\n"
                    "var configPingCntMax = %d;\n"
                    "var configPingIntervalMin = %d;\n"
                    "var configPingIntervalMax = %d;\n"
                    "var configPingPdataMin = %d;\n"
                    "var configPingPdataMax = %d;\n"
                    "var configPingTtlMin = %d;\n"
                    "var configPingTtlMax = %d;\n"
                    "var configTracerouteDscpMin = %d;\n"
                    "var configTracerouteDscpMax = %d;\n"
                    "var configTracerouteTimeoutMin = %d;\n"
                    "var configTracerouteTimeoutMax = %d;\n"
                    "var configTracerouteProbesMin = %d;\n"
                    "var configTracerouteProbesMax = %d;\n"
                    "var configTracerouteFttlMin = %d;\n"
                    "var configTracerouteFttlMax = %d;\n"
                    "var configTracerouteMttlMin = %d;\n"
                    "var configTracerouteMttlMax = %d;\n"
                    "var configVidMin = %d;\n"
                    "var configVidMax = %d;\n"
                    ,
                    MESA_CAP(VTSS_APPL_CAP_IP_INTERFACE_CNT),
                    MESA_CAP(VTSS_APPL_CAP_IP_ROUTE_CNT),
                    VTSS_APPL_IP_DHCP_CLIENT_ID_MIN_LENGTH,
                    VTSS_APPL_IP_DHCP_CLIENT_ID_MAX_LENGTH -1,
                    VTSS_APPL_IP_DHCP_HOSTNAME_MAX_LENGTH -1,
                    vtss_ip_hasipv6(),
                    vtss_ip_hasdhcpv6(),
                    vtss_ip_hasrouting(),
                    vtss_ip_hasdns(),
                    PING_MIN_PACKET_LEN, PING_MAX_PACKET_LEN,
                    PING_MIN_PACKET_CNT, PING_MAX_PACKET_CNT,
                    PING_MIN_PACKET_INTERVAL, PING_MAX_PACKET_INTERVAL,
                    PING_MIN_PAYLOAD_DATA, PING_MAX_PAYLOAD_DATA,
                    PING_MIN_TTL, PING_MAX_TTL,
                    TRACEROUTE_DSCP_MIN, TRACEROUTE_DSCP_MAX,
                    TRACEROUTE_TIMEOUT_MIN, TRACEROUTE_TIMEOUT_MAX,
                    TRACEROUTE_PROBES_MIN, TRACEROUTE_PROBES_MAX,
                    TRACEROUTE_FTTL_MIN, TRACEROUTE_FTTL_MAX,
                    TRACEROUTE_MTTL_MIN, TRACEROUTE_MTTL_MAX,
                    1, (MESA_VIDS - 1)
            );

    return webCommonBufferHandler(base_ptr, cur_ptr, length, buff);
}

/****************************************************************************/
/*  JS lib_config table entry                                               */
/****************************************************************************/

web_lib_config_js_tab_entry(ip2_lib_config_js);
