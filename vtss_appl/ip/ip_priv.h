/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _IP_PRIV_H_
#define _IP_PRIV_H_

#include "ip_types.h"

// Prefix for Linux VLAN interfaces
#define VTSS_VLAN_IF_PREFIX             "vtss.vlan."

typedef struct {
    vtss_if_param_t   param;    /**< MTU */
    mesa_ip_network_t network;  /**< Interface address/mask */
} ip_interface_conf_t;

typedef struct ip_route_entry {
    /* NB - leaving room for metrics */
    mesa_routing_entry_t route;
} ip_route_entry_t;

/* Configuration structure (master only) */
struct ip_iface_entry {
    mesa_vid_t               vlan_id;   /**< Key */
    vtss_interface_ip_conf_t conf;      /**< Data */
};

typedef struct {
    /* Global config */
    vtss_ip_global_param_priv_t global;
    /* Per interface */
    CapArray<struct ip_iface_entry, VTSS_APPL_CAP_IP_INTERFACE_CNT> interfaces;
} ip_stack_conf_t;

#define IP_CONF_VERSION  1 /* IP2 flash configuration version */

/* Overall configuration as saved in flash */
typedef struct {
    u32              version;    /* Current version of the configuration in flash */
    ip_stack_conf_t stack_conf; /* Configuration for the whole stack */
} ip_flash_conf_t;

#define VTSS_TRACE_MODULE_ID   VTSS_MODULE_ID_IP
#define VTSS_TRACE_GRP_DEFAULT 0
#define TRACE_GRP_RXPKT_DUMP   1
#define TRACE_GRP_TXPKT_DUMP   2
#define TRACE_GRP_CRIT         3
#define TRACE_GRP_CNT          4

#define IP_MAX_STATUS_OBJS 1024

typedef enum {
    VIF_FWD_UNDEFINED,    /* Initial state */
    VIF_FWD_FORWARDING,   /* At least one port on VIF is forwarding */
    VIF_FWD_BLOCKING,     /* All ports on VIF are blocking  */
} ip_vif_fwd_t;

void vtss_ip_if_signal(vtss_if_id_vlan_t if_id, vtss_ifindex_t ifindex);

void vtss_ip_routing_monitor_enable(void);
void vtss_ip_routing_monitor_disable(void);

typedef struct {
    bool up[MESA_VIDS];
} ip_vlan_state_t;

void vtss_ip_vlan_state_update(ip_vlan_state_t *state);

void vtss_ip_if_mo_flag_update(mesa_vid_t vid);
#endif /* _IP_PRIV_H_ */
