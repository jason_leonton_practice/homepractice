/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _IP_UTILS_H_
#define _IP_UTILS_H_

#include "ip_types.h"

#ifdef __cplusplus
#include "vtss/basics/stream.hxx"
#include "vtss/basics/memcmp-operator.hxx"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define VTSS_MAC_FORMAT "%02x-%02x-%02x-%02x-%02x-%02x"
#define VTSS_MAC_ARGS(X) \
    (X).addr[0], (X).addr[1], (X).addr[2], (X).addr[3], (X).addr[4], (X).addr[5]

#define VTSS_IPV4_FORMAT "%u.%u.%u.%u"
#define VTSS_IPV4_ARGS(X) \
    (((X) >> 24) & 0xff), (((X) >> 16) & 0xff), (((X) >> 8) & 0xff), ((X)&0xff)

#define VTSS_IPV6_FORMAT "%04x:%04x:%04x:%04x:%04x:%04x:%04x:%04x"
#define VTSS_IPV6_ARGS(X)                                         \
    ((((u32)((X).addr[0])) << 8) | ((u32)(X).addr[1])),           \
            ((((u32)((X).addr[2])) << 8) | ((u32)(X).addr[3])),   \
            ((((u32)((X).addr[4])) << 8) | ((u32)(X).addr[5])),   \
            ((((u32)((X).addr[6])) << 8) | ((u32)(X).addr[7])),   \
            ((((u32)((X).addr[8])) << 8) | ((u32)(X).addr[9])),   \
            ((((u32)((X).addr[10])) << 8) | ((u32)(X).addr[11])), \
            ((((u32)((X).addr[12])) << 8) | ((u32)(X).addr[13])), \
            ((((u32)((X).addr[14])) << 8) | ((u32)(X).addr[15]))

#define VTSS_IPV6N_FORMAT VTSS_IPV6_FORMAT "/%d"
#define VTSS_IPV6N_ARG(X) VTSS_IPV6_ARGS((X).address), (X).prefix_size

#define VTSS_IPV6_UC_FORMAT \
    "{network = " VTSS_IPV6N_FORMAT ", destination = " VTSS_IPV6_FORMAT "}"
#define VTSS_IPV6_UC_ARGS(X) \
    VTSS_IPV6N_ARG((X).network), VTSS_IPV6_ARGS((X).destination)

#define VTSS_IPV4N_FORMAT VTSS_IPV4_FORMAT "/%d"
#define VTSS_IPV4N_ARG(X) VTSS_IPV4_ARGS((X).address), (X).prefix_size

#define VTSS_IPV4_UC_FORMAT \
    "{network = " VTSS_IPV4N_FORMAT ", destination = " VTSS_IPV4_FORMAT "}"
#define VTSS_IPV4_UC_ARGS(X) \
    VTSS_IPV4N_ARG((X).network), VTSS_IPV4_ARGS((X).destination)

/* Misc helpers ------------------------------------------------------------ */
#define IP_VALID_VLAN_ID(x) \
    (((x) >= VTSS_APPL_VLAN_ID_MIN) && ((x) <= VTSS_APPL_VLAN_ID_MAX))

/* Returns TRUE if IPv4/v6 is a zero-address. FALSE otherwise. */
BOOL vtss_ip_addr_is_zero(const mesa_ip_addr_t *const ip_addr);
BOOL vtss_ipv4_addr_is_multicast(const mesa_ipv4_t *const ip);
BOOL vtss_ipv4_addr_is_loopback(const mesa_ipv4_t *const ip);
BOOL vtss_ipv6_addr_is_link_local(const mesa_ipv6_t *const ip);
BOOL vtss_ipv6_addr_is_multicast(const mesa_ipv6_t *const ip);
BOOL vtss_ipv6_addr_is_zero(const mesa_ipv6_t *const addr);
BOOL vtss_ipv6_addr_is_loopback(const mesa_ipv6_t *const addr);
BOOL vtss_ipv6_addr_is_mgmt_support(const mesa_ipv6_t *const addr);

// using the class system to derive a prefix
int vtss_ipv4_addr_to_prefix(mesa_ipv4_t ip);

/* Type conversion --------------------------------------------------------- */

mesa_rc vtss_prefix_cnt(const u8 *data, const u32 length, u32 *prefix);
mesa_rc vtss_conv_ipv4mask_to_prefix(const mesa_ipv4_t ipv4, u32 *const prefix);

mesa_rc vtss_conv_prefix_to_ipv4mask(const u32 prefix, mesa_ipv4_t *const ipv4);

mesa_rc vtss_conv_ipv6mask_to_prefix(const mesa_ipv6_t *const ipv6,
                                     u32 *const prefix);

mesa_rc vtss_conv_prefix_to_ipv6mask(const u32 prefix, mesa_ipv6_t *const ipv6);

mesa_rc vtss_build_ipv4_network(mesa_ip_network_t *const net,
                                const mesa_ipv4_t address,
                                const mesa_ipv4_t mask);

mesa_rc vtss_build_ipv4_uc(mesa_ipv4_uc_t *route, const mesa_ipv4_t address,
                           const mesa_ipv4_t mask, const mesa_ipv4_t gateway);

mesa_rc vtss_build_ipv6_network(mesa_ip_network_t *const net,
                                const mesa_ipv6_t *address,
                                const mesa_ipv6_t *mask);

mesa_rc vtss_build_ipv6_network_(mesa_ipv6_network_t *const net,
                                 const mesa_ipv6_t *address,
                                 const mesa_ipv6_t *mask);

mesa_rc vtss_build_ipv6_uc(vtss_appl_ip_ipv6_route_conf_t *const route,
                           const mesa_ipv6_t *address, const mesa_ipv6_t *mask,
                           const mesa_ipv6_t *gateway, const mesa_vid_t vid);

void vtss_ip_if_id_vlan(mesa_vid_t vid, vtss_if_id_t *if_id);

mesa_rc vtss_appl_ip_if_status_to_ip(
    const vtss_appl_ip_if_status_t *const status, mesa_ip_addr_t *const ip);

BOOL vtss_if_status_match_vlan(mesa_vid_t vlan,
                               vtss_appl_ip_if_status_t *if_status);

BOOL vtss_if_status_match_ip_type(mesa_ip_type_t type,
                                  const vtss_appl_ip_if_status_t *if_status);

BOOL vtss_if_status_match_ip(const mesa_ip_addr_t *const ip,
                             vtss_appl_ip_if_status_t *if_status);

void vtss_ipv4_default_route(mesa_ipv4_t gw, mesa_routing_entry_t *rt);

BOOL vtss_ip_ipv4_ifaddr_valid(const mesa_ipv4_network_t *const net);
BOOL vtss_ip_ipv6_ifaddr_valid(const mesa_ipv6_network_t *const net);
BOOL vtss_ip_ifaddr_valid(const mesa_ip_network_t *const net);

/* Various type equal checks ----------------------------------------------- */

BOOL vtss_ip_equal(const mesa_ip_network_t *const a,
                   const mesa_ip_network_t *const b);

BOOL vtss_ipv4_network_equal(const mesa_ipv4_network_t *const a,
                             const mesa_ipv4_network_t *const b);

BOOL vtss_ipv6_network_equal(const mesa_ipv6_network_t *const a,
                             const mesa_ipv6_network_t *const b);

BOOL vtss_ip_network_equal(const mesa_ip_network_t *const a,
                           const mesa_ip_network_t *const b);

BOOL vtss_ipv4_net_equal(const mesa_ipv4_network_t *const a,
                         const mesa_ipv4_network_t *const b);

BOOL vtss_ipv6_net_equal(const mesa_ipv6_network_t *const a,
                         const mesa_ipv6_network_t *const b);

BOOL vtss_ip_net_equal(const mesa_ip_network_t *const a,
                       const mesa_ip_network_t *const b);

mesa_ipv4_network_t vtss_ipv4_net_mask_out(const mesa_ipv4_network_t *n);
mesa_ipv6_network_t vtss_ipv6_net_mask_out(const mesa_ipv6_network_t *n);
mesa_ip_network_t vtss_ip_net_mask_out(const mesa_ip_network_t *n);

BOOL vtss_ipv4_net_overlap(const mesa_ipv4_network_t *const a,
                           const mesa_ipv4_network_t *const b);

BOOL vtss_ipv6_net_overlap(const mesa_ipv6_network_t *const a,
                           const mesa_ipv6_network_t *const b);

BOOL vtss_ip_net_overlap(const mesa_ip_network_t *const a,
                         const mesa_ip_network_t *const b);

BOOL vtss_ipv4_net_include(const mesa_ipv4_network_t *const net,
                           const mesa_ipv4_t *const addr);

BOOL vtss_ipv6_net_include(const mesa_ipv6_network_t *const net,
                           const mesa_ipv6_t *const addr);

BOOL vtss_ip_net_include(const mesa_ip_network_t *const net,
                         const mesa_ip_addr_t *const addr);

bool operator<(const mesa_routing_entry_t &a, const mesa_routing_entry_t &b);

BOOL vtss_if_id_equal(const vtss_if_id_t *const a, const vtss_if_id_t *const b);

BOOL vtss_ip_route_valid(const mesa_routing_entry_t *const rt);
BOOL vtss_ip_route_nh_valid(const mesa_routing_entry_t *const rt);

/* Printers */
int vtss_ip_ip_addr_to_txt(char *buf, int size, const mesa_ip_addr_t *const ip);

int vtss_ip_ip_network_to_txt(char *buf, int size,
                              const mesa_ip_network_t *const ip);

int vtss_ip_neighbour_to_txt(char *buf, int size,
                             const vtss_neighbour_t *const nb);

int vtss_ip_route_entry_to_txt(char *buf, int size,
                               const mesa_routing_entry_t *const rt);

int vtss_ip_if_id_to_txt(char *buf, int size, const vtss_if_id_t *const if_id);

int vtss_if_link_flag_to_txt(char *buf, int size,
                             vtss_appl_ip_if_link_flag_t f);

int vtss_if_ipv6_flag_to_txt(char *buf, int size,
                             vtss_appl_ip_if_ipv6_flag_t f);

int vtss_routing_flags_to_txt(char *buf, int size,
                              vtss_appl_ip_route_status_flags_t f);

int vtss_ip_if_status_to_txt(char *buf, int size, vtss_appl_ip_if_status_t *st,
                             u32 length);

int vtss_ip_neighbour_status_to_txt(char *buf, int size,
                                    vtss_neighbour_status_t *st);

#define IP_MAX_ICMP_TXT_LEN 64
int vtss_ip_stat_icmp_type_txt(char *buf, int size,
                               const mesa_ip_type_t version,
                               const u32 icmp_type);

/* Sorting */
void vtss_ip_if_status_sort(u32 cnt, vtss_appl_ip_if_status_t *status);

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
vtss::ostream &operator<<(vtss::ostream &o,
                          const vtss_appl_ip_ipv6_route_conf_t &v);

vtss::ostream &operator<<(vtss::ostream &o, const mesa_ipv4_uc_t &r);

vtss::ostream &operator<<(vtss::ostream &o, const mesa_ipv6_uc_t &r);

vtss::ostream &operator<<(vtss::ostream &o, const mesa_routing_entry_t &r);

vtss::ostream &operator<<(vtss::ostream &o, const vtss_routing_params_t &r);

vtss::ostream &operator<<(vtss::ostream &o,
                          const vtss_appl_ip_ipv6_neighbour_status_key_t &k);

vtss::ostream &operator<<(vtss::ostream &o, vtss_appl_ip_if_link_flag_t f);

vtss::ostream &operator<<(vtss::ostream &o, const vtss_if_id_t &s);

vtss::ostream &operator<<(vtss::ostream &o,
                          const vtss_appl_ip_if_status_link_t &s);

vtss::ostream &operator<<(vtss::ostream &o,
                          const vtss_appl_ip_if_status_ipv4_t &s);

vtss::ostream &operator<<(vtss::ostream &o,
                          const vtss_appl_ip_if_status_ipv6_t &s);

vtss::ostream &operator<<(vtss::ostream &o,
                          const vtss_appl_ip_if_status_dhcp4c_t &s);

vtss::ostream &operator<<(vtss::ostream &o, const vtss_appl_ip_if_status_t &s);

VTSS_ENUM_BITWISE(vtss_appl_ip_dhcp_flag_t);

VTSS_BASICS_MEMCMP_OPERATOR(mesa_ipv6_t);
VTSS_BASICS_MEMCMP_OPERATOR(vtss_appl_ip_if_status_link_t);
VTSS_BASICS_MEMCMP_OPERATOR(vtss_appl_ip_if_ipv4_info_t);
VTSS_BASICS_MEMCMP_OPERATOR(vtss_appl_ip_if_status_dhcp4c_t);
VTSS_BASICS_MEMCMP_OPERATOR(vtss_appl_ip_route_status_t);

bool operator<(const mesa_mac_t &a, const mesa_mac_t &b);
bool operator!=(const mesa_mac_t &a, const mesa_mac_t &b);
bool operator==(const mesa_mac_t &a, const mesa_mac_t &b);

bool operator!=(const vtss_appl_ip_if_ipv6_info_t &a,
                const vtss_appl_ip_if_ipv6_info_t &b);

bool operator<(const mesa_ipv4_network_t &a, const mesa_ipv4_network_t &b);
bool operator==(const mesa_ipv4_network_t &a, const mesa_ipv4_network_t &b);
bool operator!=(const mesa_ipv4_network_t &a, const mesa_ipv4_network_t &b);

bool operator<(const mesa_ipv4_uc_t &a, const mesa_ipv4_uc_t &b);
bool operator!=(const mesa_ipv4_uc_t &a, const mesa_ipv4_uc_t &b);
bool operator==(const mesa_ipv4_uc_t &a, const mesa_ipv4_uc_t &b);


bool operator<(const mesa_ipv6_network_t &a, const mesa_ipv6_network_t &b);
bool operator!=(const mesa_ipv6_network_t &a, const mesa_ipv6_network_t &b);
bool operator==(const mesa_ipv6_network_t &a, const mesa_ipv6_network_t &b);

bool operator<(const mesa_ipv6_uc_t &a, const mesa_ipv6_uc_t &b);
bool operator!=(const mesa_ipv6_uc_t &a, const mesa_ipv6_uc_t &b);
bool operator==(const mesa_ipv6_uc_t &a, const mesa_ipv6_uc_t &b);

bool operator<(const vtss_appl_ip_ipv6_route_conf_t &a,
               const vtss_appl_ip_ipv6_route_conf_t &b);
bool operator!=(const vtss_appl_ip_ipv6_route_conf_t &a,
                const vtss_appl_ip_ipv6_route_conf_t &b);
bool operator==(const vtss_appl_ip_ipv6_route_conf_t &a,
                const vtss_appl_ip_ipv6_route_conf_t &b);

bool operator<(const vtss_appl_ip_ipv4_acd_status_key_t &a,
               const vtss_appl_ip_ipv4_acd_status_key_t &b);
bool operator!=(const vtss_appl_ip_ipv4_acd_status_key_t &a,
                const vtss_appl_ip_ipv4_acd_status_key_t &b);
bool operator==(const vtss_appl_ip_ipv4_acd_status_key_t &a,
                const vtss_appl_ip_ipv4_acd_status_key_t &b);
vtss::ostream &operator<<(vtss::ostream &o, const vtss_appl_ip_ipv4_acd_status_key_t &k);

bool operator<(const vtss_appl_ip_ipv4_acd_status_t &a,
               const vtss_appl_ip_ipv4_acd_status_t &b);
bool operator!=(const vtss_appl_ip_ipv4_acd_status_t &a,
                const vtss_appl_ip_ipv4_acd_status_t &b);
bool operator==(const vtss_appl_ip_ipv4_acd_status_t &a,
                const vtss_appl_ip_ipv4_acd_status_t &b);
vtss::ostream &operator<<(vtss::ostream &o, const vtss_appl_ip_ipv4_acd_status_t &s);

VTSS_BASICS_MEMCMP_OPERATOR(vtss_appl_ip_if_status_ip_stat_t);
vtss_appl_ip_if_status_ip_stat_t operator-(
        const vtss_appl_ip_if_status_ip_stat_t &a,
        const vtss_appl_ip_if_status_ip_stat_t &b);

vtss_appl_ip_if_status_link_stat_t operator-(
        const vtss_appl_ip_if_status_link_stat_t &a,
        const vtss_appl_ip_if_status_link_stat_t &b);

#endif
#endif /* _IP_UTILS_H_ */

