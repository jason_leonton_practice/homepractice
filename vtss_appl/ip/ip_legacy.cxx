/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifdef VTSS_SW_OPTION_WEB
#include "ip_legacy.h"
#include "mgmt_api.h"
mesa_rc web_parse_ipv4(CYG_HTTPD_STATE *p, char *name, mesa_ipv4_t *ipv4)
{
    const char *buf;
    size_t     len = 16;
    unsigned   int  a, b, c, d, m;

    if ((buf = cyg_httpd_form_varable_string(p, name, &len)) != NULL) {
        m = sscanf(buf, "%u.%u.%u.%u", &a, &b, &c, &d);
        if (m == 4 && a < 256 && b < 256 && c < 256 && d < 256) {
            *ipv4 = ((a << 24) + (b << 16) + (c << 8) + d);
            return VTSS_RC_OK;
        }
    }

    return VTSS_RC_ERROR;
}

mesa_rc web_parse_ipv4_fmt(CYG_HTTPD_STATE *p, mesa_ipv4_t *pip, const char *fmt, ...)
{
    char instance_name[256];
    va_list va;
    va_start(va, fmt);
    (void) vsnprintf(instance_name, sizeof(instance_name), fmt, va);
    va_end(va);
    return web_parse_ipv4(p, instance_name, pip);
}

mesa_rc web_parse_ipv6(CYG_HTTPD_STATE *p, const char *name, mesa_ipv6_t *ipv6)
{
    const char  *buf;
    size_t      len;
    mesa_rc     rc = VTSS_RC_ERROR;
    char        ip_buf[IPV6_ADDR_IBUF_MAX_LEN];

    if ((buf = cyg_httpd_form_varable_string(p, name, &len)) != NULL) {
        memset(&ip_buf[0], 0x0, sizeof(ip_buf));
        if (len > 0 && cgi_unescape(buf, ip_buf, len, sizeof(ip_buf))) {
            rc = mgmt_txt2ipv6(ip_buf, ipv6);
        }

    }
    return rc;
}

mesa_rc web_parse_ipv6_fmt(CYG_HTTPD_STATE *p, mesa_ipv6_t *pip, const char *fmt, ...)
{
    char instance_name[256];
    va_list va;
    va_start(va, fmt);
    (void) vsnprintf(instance_name, sizeof(instance_name), fmt, va);
    va_end(va);
    return web_parse_ipv6(p, instance_name, pip);
}

mesa_rc web_parse_ip(CYG_HTTPD_STATE *p, char *name, mesa_ip_addr_t *pip)
{
    if (vtss_ip_hasipv6() && (web_parse_ipv6(p, name, &pip->addr.ipv6) == VTSS_OK)) {
        pip->type = MESA_IP_TYPE_IPV6;
        return VTSS_OK;
    } else if (web_parse_ipv4(p, name, &pip->addr.ipv4) == VTSS_OK) {
        pip->type = MESA_IP_TYPE_IPV4;
        return VTSS_OK;
    }
    pip->type = MESA_IP_TYPE_NONE;
    return VTSS_INCOMPLETE;
}

mesa_rc web_parse_ip_fmt(CYG_HTTPD_STATE *p, mesa_ip_addr_t *pip, const char *fmt, ...)
{
    char instance_name[256];
    va_list va;
    va_start(va, fmt);
    (void) vsnprintf(instance_name, sizeof(instance_name), fmt, va);
    va_end(va);
    return web_parse_ip(p, instance_name, pip);
}

#endif
