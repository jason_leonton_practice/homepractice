/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _IP_TYPES_H_
#define _IP_TYPES_H_

#include "main.h"
#include "vtss/appl/ip.h"
#include "main_types.h"
#include "vtss/basics/enum_macros.hxx"

/*
 * Maximum router legs.
 */
#define IP_MAX_INTERFACES ip_interface_cnt_get()

#include "../dhcp_client/dhcp_client_api.h"

uint32_t ip_interface_cnt_get(void);

/*
 * Maximum static routes.
 */
#define IP_MAX_ROUTES MESA_CAP(VTSS_APPL_CAP_IP_ROUTE_CNT)

enum {
    IP_ERROR_EXISTS = MODULE_ERROR_START(VTSS_MODULE_ID_IP),
    IP_ERROR_NOTFOUND,
    IP_ERROR_NOSPACE,
    IP_ERROR_PARAMS,
    IP_ERROR_FAILED,
    IP_ERROR_ADDRESS_CONFLICT,
    IP_ERROR_NOT_MASTER,
    IP_ERROR_DHCP_CLIENT_HOSTNAME,
    IP_ERROR_DHCP_CLIENT_ID
};

const char *ip_error_txt(mesa_rc rc);
const char *ip_chip_error_txt(mesa_rc rc);


typedef mesa_vid_t vtss_if_id_vlan_t;

typedef enum {
    VTSS_ID_IF_TYPE_INVALID = 0,
    VTSS_ID_IF_TYPE_OS_ONLY = 1,
    VTSS_ID_IF_TYPE_VLAN = 2,
} vtss_if_id_type_t;

#ifndef IFNAMSIZ
#define IFNAMSIZ    16
#endif
#ifndef IF_NAMESIZE
#define IF_NAMESIZE IFNAMSIZ
#endif
typedef struct {
    char name[IF_NAMESIZE + 1];
    int  ifno;
} vtss_os_if_t;

typedef struct {
    vtss_if_id_type_t type;

    union {
        mesa_vid_t   vlan;
        vtss_os_if_t os;
    } u;
} vtss_if_id_t;

/* Not complete */
typedef struct {
    u32        mtu;
} vtss_if_param_t;

typedef struct {
    BOOL                dhcpc;     /**< enable dhcp v4 client */
    mesa_ip_network_t   network;   /**< Interface address/mask */
    u32                 fallback_timeout;
} vtss_ip_conf_t;

typedef struct {
    vtss_if_param_t          link_layer;
    vtss_appl_ip_ipv4_conf_t ipv4;
    vtss_appl_ip_ipv6_conf_t ipv6;
} vtss_interface_ip_conf_t;





/* Statistics Section: RFC-4293 */
#define IP_STAT_REFRESH_RATE       3000
#define IP_STAT_IMSG_MAX           0x100

typedef struct {
    mesa_ip_type_t      IPVersion;  /* INDEX */

    vtss_appl_ip_if_status_ip_stat_t data;
} vtss_ips_ip_stat_t;

typedef struct {
    u32                     InMsgs;
    u32                     InErrors;
    u32                     OutMsgs;
    u32                     OutErrors;
} vtss_icmp_stat_data_t;

typedef struct {
    mesa_ip_type_t          IPVersion;  /* INDEX */
    u32                     Type;       /* INDEX */

    vtss_icmp_stat_data_t   data;
} vtss_ips_icmp_stat_t;

typedef struct {
    mesa_ip_type_t          IPVersion;  /* INDEX */
    vtss_if_id_t            IfIndex;    /* INDEX */

    vtss_appl_ip_if_status_ip_stat_t data;
} vtss_if_status_ip_stat_t;

typedef enum {
    VTSS_IPS_STATUS_TYPE_INVALID = 0,   /* Invalid IPS status entry */
    VTSS_IPS_STATUS_TYPE_ANY,           /* Used to query all IPS status */

    VTSS_IPS_STATUS_TYPE_STAT_IPV4,     /* IPV4 System Statistics */
    VTSS_IPS_STATUS_TYPE_STAT_IPV6,     /* IPV6 System Statistics */
    VTSS_IPS_STATUS_TYPE_STAT_ICMP4,    /* IPV4 System ICMP Statistics */
    VTSS_IPS_STATUS_TYPE_STAT_ICMP6     /* IPV6 System ICMP Statistics */
} vtss_ips_status_type_t;

/*lint -save -e19 */
VTSS_ENUM_INC(vtss_ips_status_type_t);
/*lint -restore */

typedef struct {
    mesa_ip_type_t          version;
    u32                     imsg;
    vtss_ips_status_type_t  type;

    union {
        vtss_ips_icmp_stat_t    icmp_stat;
        vtss_ips_ip_stat_t      ip_stat;
    } u;
} vtss_ips_status_t;

typedef enum {
    VTSS_APPL_IP_IF_STATUS_TYPE_INVALID = 0,
    VTSS_APPL_IP_IF_STATUS_TYPE_ANY = 1,
    VTSS_APPL_IP_IF_STATUS_TYPE_LINK = 2,
    VTSS_APPL_IP_IF_STATUS_TYPE_IPV4 = 3,
    VTSS_APPL_IP_IF_STATUS_TYPE_DHCP = 4,
    VTSS_APPL_IP_IF_STATUS_TYPE_IPV6 = 5,
} vtss_appl_ip_if_status_type_t;

typedef struct {
    vtss_if_id_t if_id;
    vtss_appl_ip_if_status_type_t type;

    union {
        vtss_appl_ip_if_status_link_t      link;
        vtss_appl_ip_if_status_link_stat_t link_stat;
        vtss_appl_ip_if_status_ipv4_t      ipv4;
        vtss_appl_ip_if_status_ipv6_t      ipv6;
        vtss_appl_ip_if_status_dhcp4c_t    dhcp4c;
    } u;
} vtss_appl_ip_if_status_t;

typedef struct {
    mesa_mac_t     mac_address;
    vtss_if_id_t   interface;
    mesa_ip_addr_t ip_address;
    char           state[8];
    u8             flags;
} vtss_neighbour_status_t;

/* Not complete */
/* This struct is for storing data associated to a route, but data which is not
 * need by the IP stack. */
typedef struct {
    vtss_appl_ip_route_owner_t owner;
} vtss_routing_params_t;

typedef struct {
    u32                        owners; /* Bitmap of route owners */
} vtss_routing_info_t;

typedef struct {
    mesa_routing_entry_t  rt;
    vtss_routing_params_t params;
    vtss_appl_ip_route_status_flags_t flags;
    vtss_if_id_t          interface;
    int                   lifetime;  // ipDefaultRouterLifetime in RFC4293
    int                   preference;  // ipDefaultRouterPreference in RFC4293
} vtss_routing_status_t;

typedef struct {
    mesa_mac_t     dmac; /**< MAC address of destination */
    mesa_vid_t     vlan; /**< VLAN of destination */
    mesa_ip_addr_t dip;  /**< IP address of destination */
} vtss_neighbour_t;

typedef int (*vtss_ipstack_filter_cb_t) (mesa_vid_t, unsigned, const char *);

typedef enum {
    VTSS_IP_SRV_TYPE_DHCP_ANY  = 0x0,
    VTSS_IP_SRV_TYPE_NONE      = 0x1,
    VTSS_IP_SRV_TYPE_STATIC    = 0x2,
    VTSS_IP_SRV_TYPE_DHCP_VLAN = 0x4,
} vtss_ip_srv_conf_type_t;

typedef struct {
    mesa_ip_addr_t ip_srv_address;
} vtss_ip_srv_conf_static_t;

typedef struct {
    mesa_vid_t vlan;
} vtss_ip_srv_conf_dhcp_vlan;

typedef struct {
    vtss_ip_srv_conf_type_t type;

    union {
        vtss_ip_srv_conf_static_t static_conf;
        vtss_ip_srv_conf_dhcp_vlan dhcp_vlan_conf;
    } u;
} vtss_ip_srv_conf_t;

typedef enum {
    VTSS_IP_DBG_FLAG_NONE       = 0,
    VTSS_IP_DBG_FLAG_ND6_LOG    = (1 << 0)
} vtss_ip_debug_type_t;

VTSS_ENUM_BITWISE(vtss_ip_debug_type_t);

typedef struct {
    BOOL enable_routing;        /**< Enable rounting (all interfaces) */
#ifdef VTSS_SW_OPTION_NTP
    vtss_ip_srv_conf_t ntp;
#endif /* VTSS_SW_OPTION_NTP */
    vtss_ip_debug_type_t dbg_flag;
    char reserved[252];
} vtss_ip_global_param_priv_t;

typedef enum {
    VTSS_IPSTACK_MSG_RT_ADD = 0x1,
    VTSS_IPSTACK_MSG_RT_DEL = 0x2,
    VTSS_IPSTACK_MSG_NB_ADD = 0x3,
    VTSS_IPSTACK_MSG_NB_DEL = 0x4,
    VTSS_IPSTACK_MSG_IF_SIG = 0x5,
} vtss_ipstack_msg_type_t;

typedef struct {
    int            ifidx;
    mesa_mac_t     dmac;
    mesa_ip_addr_t dip;
} vtss_ipstack_msg_nb_t;

typedef struct {
    int                  ifidx;
    mesa_routing_entry_t route;
} vtss_ipstack_msg_rt_t;

typedef struct {
    int                  ifidx;
} vtss_ipstack_msg_if_t;

typedef struct {
    vtss_ipstack_msg_type_t type;
    union {
        vtss_ipstack_msg_nb_t nb;
        vtss_ipstack_msg_rt_t rt;
        vtss_ipstack_msg_if_t interface;
    } u;
} vtss_ipstack_msg_t;

#endif /* _IP_TYPES_H_ */
