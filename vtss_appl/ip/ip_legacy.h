/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/


#ifndef _IP_LEGACY_H_
#define _IP_LEGACY_H_
#ifdef VTSS_SW_OPTION_WEB
#include "web_api.h"
#include "main_types.h"

#ifdef __cplusplus
extern "C" {
#endif

mesa_rc web_parse_ip      (CYG_HTTPD_STATE *p, char *name, mesa_ip_addr_t *pip);
mesa_rc web_parse_ipv4    (CYG_HTTPD_STATE *p, char *name, mesa_ipv4_t *ipv4);
mesa_rc web_parse_ipv6    (CYG_HTTPD_STATE *p, const char *name, mesa_ipv6_t *ipv6);
mesa_rc web_parse_ipv4_fmt(CYG_HTTPD_STATE *p, mesa_ipv4_t *pip, const char *fmt, ...);
mesa_rc web_parse_ipv6_fmt(CYG_HTTPD_STATE *p, mesa_ipv6_t *pip, const char *fmt, ...);
mesa_rc web_parse_ip_fmt  (CYG_HTTPD_STATE *p, mesa_ip_addr_t *pip, const char *fmt, ...);

#ifdef __cplusplus
}
#endif
#endif
#endif /* _IP_LEGACY_H_ */

