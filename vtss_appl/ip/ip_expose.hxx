/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef __IP_IP_EXPOSE_HXX__
#define __IP_IP_EXPOSE_HXX__

#include "vtss/appl/ip.h"
#include "vtss/basics/expose.hxx"

typedef vtss::expose::TableStatus<
    vtss::expose::ParamKey<vtss_ifindex_t>,
    vtss::expose::ParamVal<vtss_appl_ip_if_status_link_t *>> StatusIfLink;
extern StatusIfLink status_if_link;

typedef vtss::expose::TableStatus<
    vtss::expose::ParamKey<vtss_ifindex_t>,
    vtss::expose::ParamKey<mesa_ipv4_network_t *>,
    vtss::expose::ParamVal<vtss_appl_ip_if_ipv4_info_t *>> StatusIfIpv4;
extern StatusIfIpv4 status_if_ipv4;

typedef vtss::expose::TableStatus<
    vtss::expose::ParamKey<vtss_ifindex_t>,
    vtss::expose::ParamKey<mesa_ipv6_network_t *>,
    vtss::expose::ParamVal<vtss_appl_ip_if_ipv6_info_t *>> StatusIfIpv6;
extern StatusIfIpv6 status_if_ipv6;

typedef vtss::expose::TableStatus<
    vtss::expose::ParamKey<mesa_ipv4_uc_t *>,
    vtss::expose::ParamVal<vtss_appl_ip_route_status_t *>> StatusRtIpv4;
extern StatusRtIpv4 status_rt_ipv4;

typedef vtss::expose::TableStatus<
    vtss::expose::ParamKey<vtss_appl_ip_ipv6_route_conf_t *>,
    vtss::expose::ParamVal<vtss_appl_ip_route_status_t *>> StatusRtIpv6;
extern StatusRtIpv6 status_rt_ipv6;

extern vtss::expose::TableStatus<
        vtss::expose::ParamKey<vtss_ifindex_t>,
        vtss::expose::ParamVal<vtss_appl_ip_if_status_dhcp4c_t *>> status_if_dhcp;

typedef vtss::expose::TableStatus<
    vtss::expose::ParamKey<vtss_appl_ip_ipv4_acd_status_key_t *>,
    vtss::expose::ParamVal<vtss_appl_ip_ipv4_acd_status_t *>> StatusAcdIpv4;
extern StatusAcdIpv4 status_acd_ipv4;

#endif  // __IP_IP_EXPOSE_HXX__
