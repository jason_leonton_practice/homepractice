/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "mscc/ethernet/switch/api.h"

#include "port_api.h"
#include "critd_api.h"
#include "ip_api.h"
#include "ip_utils.h"
#include "ip_trace.h"
#include "packet_api.h"
#include "ip_chip_api.h"
#include "vtss_trace_api.h"

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_IP
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_IP
#define __GRP VTSS_TRACE_IP_GRP_CHIP
#define E(_fmt, ...) T(__GRP, VTSS_TRACE_LVL_ERROR, _fmt, ##__VA_ARGS__)
#define W(_fmt, ...) T(__GRP, VTSS_TRACE_LVL_WARNING, _fmt, ##__VA_ARGS__)
#define I(_fmt, ...) T(__GRP, VTSS_TRACE_LVL_INFO, _fmt, ##__VA_ARGS__)
#define D(_fmt, ...) T(__GRP, VTSS_TRACE_LVL_DEBUG, _fmt, ##__VA_ARGS__)
#define N(_fmt, ...) T(__GRP, VTSS_TRACE_LVL_NOISE, _fmt, ##__VA_ARGS__)
#define R(_fmt, ...) T(__GRP, VTSS_TRACE_LVL_RACKET, _fmt, ##__VA_ARGS__)

static critd_t crit;

#if VTSS_TRACE_ENABLED
#  define IP2_CRIT_ENTER()          \
      critd_enter(&crit,                   \
                  VTSS_TRACE_IP_GRP_CRIT, \
                  VTSS_TRACE_LVL_NOISE,    \
                  __FILE__, __LINE__)

#  define IP2_CRIT_EXIT()          \
      critd_exit(&crit,                   \
                 VTSS_TRACE_IP_GRP_CRIT, \
                 VTSS_TRACE_LVL_NOISE,    \
                 __FILE__, __LINE__)

#  define IP2_CRIT_ASSERT_LOCKED()          \
      critd_assert_locked(&crit,                   \
                          VTSS_TRACE_IP_GRP_CRIT, \
                          __FILE__, __LINE__)

#else
// Leave out function and line arguments
#  define IP2_CRIT_ENTER() critd_enter(&crit)
#  define IP2_CRIT_EXIT() critd_exit(&crit)
#  define IP2_CRIT_ASSERT_LOCKED() critd_assert_locked(&crit)
#endif

#define IP2_CRIT_RETURN(T, X) \
do {                          \
    T __val = (X);            \
    IP2_CRIT_EXIT();          \
    return __val;             \
} while(0)

#define IP2_CRIT_RETURN_RC(X)   \
    IP2_CRIT_RETURN(mesa_rc, X)


#define R_CHECK(expr, M, ...)                                 \
{                                                             \
    mesa_rc __rc__ = (expr);                                  \
    if (__rc__ != VTSS_RC_OK) {                               \
        E("Check failed: " #expr " ARGS: " M, ##__VA_ARGS__); \
        return __rc__;                                        \
    }                                                         \
}

/*lint --e{123} */
#define R_CHECK_FMT(expr, L, expr_fmt, arg)                   \
{                                                             \
    mesa_rc __rc__ = (expr);                                  \
    if (__rc__ != VTSS_RC_OK) {                               \
        char buf[128];                                        \
        (void)expr_fmt(buf, 128, arg);                        \
        L("Check failed: " #expr " ARGS: " #arg "=%s", buf);  \
        return __rc__;                                        \
    }                                                         \
}

#define DO(expr)                 \
{                                \
    if (rc == VTSS_RC_OK) {      \
        rc = (expr);             \
        if (rc != VTSS_RC_OK) {  \
            E("Failed: " #expr); \
        }                        \
    }                            \
}

#define RC_UPDATE(expr)          \
    {                            \
        mesa_rc lrc = (expr);    \
        if (lrc != VTSS_OK) {    \
            E("Failed: " #expr); \
            rc = lrc;            \
        }                        \
    }

#define PRINTF(...)                                         \
    if (size - s > 0) {                                     \
        int res = snprintf(buf + s, size - s, __VA_ARGS__); \
        if (res >0 ) {                                      \
            s += res;                                       \
        }                                                   \
    }

#define PRINTFUNC(F, ...)                       \
    if (size - s > 0) {                         \
        s += F(buf + s, size - s, __VA_ARGS__); \
    }


enum {
    IP2_ERROR_ISID = MODULE_ERROR_START(VTSS_MODULE_ID_IP_CHIP),
    IP2_ERROR_INVALID_RLEG,
    IP2_ERROR_INVALID_NOADD,
    IP2_ERROR_PORT,
    IP2_ERROR_SLAVE,
    IP2_ERROR_VALUE,
};

static mesa_mac_t base_mac;

// maps between ip2_counters_t.rleg[idx] and vlan
// Used on local node (slave or master)
static CapArray<mesa_vid_t, MESA_CAP_L3_RLEG_CNT> IP2_vlan_index;

/* Combined counters */
typedef struct {
    mesa_vid_t         vlan;
    mesa_l3_counters_t counters;
} ip2_counter_t;

typedef struct {
    CapArray< ip2_counter_t, MESA_CAP_L3_RLEG_CNT> rleg;
} ip2_counters_t;

#if defined(VTSS_SW_OPTION_L3RT)
static ip2_counters_t IP2_counters_stack;
#endif /* VTSS_SW_OPTION_L3RT) */

/******************************************************************************
 * Definition of messages.
 *****************************************************************************/
/* Mac address subscribe/unsubscribe */
typedef struct {
    mesa_mac_t mac;
    mesa_vid_t vlan;
} ip2_msg_mac_t;

typedef struct {
    mesa_rc            rc;     /* Return code from vtss_l3_rleg_counters_get */
    u32                seq_nr; /* Sequence number to match with request */
    CapArray<ip2_counter_t, MESA_CAP_L3_RLEG_CNT> rleg_cnt;
} ip2_msg_ct_rep_t;

static mesa_rc IP2_counter_vlan_cache_add_dry(mesa_vid_t vlan)
{
    int i;
    int _free = -1;

    IP2_CRIT_ASSERT_LOCKED();

    for (i = 0; i < IP2_vlan_index.size(); i++) {
        if (IP2_vlan_index[i] == vlan) {
            return VTSS_RC_ERROR;
        }

        if (IP2_vlan_index[i] == 0) {
            _free = i;
        }
    }

    if (_free >= 0) {
        return VTSS_RC_OK;
    }

    return VTSS_RC_ERROR;
}

static mesa_rc IP2_counter_vlan_cache_add(mesa_vid_t vlan)
{
#if defined(VTSS_SW_OPTION_L3RT)
    int i;
    int _free = -1;

    IP2_CRIT_ASSERT_LOCKED();

    for (i = 0; i < IP2_vlan_index.size(); i++) {
        if (IP2_vlan_index[i] == vlan) {
            return VTSS_RC_ERROR;
        }

        if (IP2_vlan_index[i] == 0) {
            _free = i;
        }
    }

    if (_free >= 0) {
        IP2_vlan_index[_free] = vlan;
        memset(&IP2_counters_stack.rleg[_free], 0, sizeof(ip2_counter_t));
        return VTSS_RC_OK;
    }

    return VTSS_RC_ERROR;
#else
    return VTSS_RC_OK;
#endif /* VTSS_SW_OPTION_L3RT) */
}

static mesa_rc IP2_counter_vlan_cache_del_dry(mesa_vid_t vlan)
{
#if defined(VTSS_SW_OPTION_L3RT)
    int i;

    IP2_CRIT_ASSERT_LOCKED();

    for (i = 0; i < IP2_vlan_index.size(); i++) {
        if (IP2_vlan_index[i] == vlan) {
            return VTSS_RC_OK;
        }
    }

    return VTSS_RC_ERROR;
#else
    return VTSS_RC_OK;
#endif /* VTSS_SW_OPTION_L3RT */
}

static mesa_rc IP2_counter_vlan_cache_del(mesa_vid_t vlan)
{
#if defined(VTSS_SW_OPTION_L3RT)
    int i;

    IP2_CRIT_ASSERT_LOCKED();

    for (i = 0; i < IP2_vlan_index.size(); i++) {
        if (IP2_vlan_index[i] == vlan) {
            IP2_vlan_index[i] = 0;
            memset(&IP2_counters_stack.rleg[i], 0, sizeof(ip2_counter_t));
            return VTSS_RC_OK;
        }
    }

    return VTSS_RC_ERROR;
#else
    return VTSS_RC_OK;
#endif /* VTSS_SW_OPTION_L3RT) */
}

#if defined(VTSS_SW_OPTION_L3RT)
static mesa_rc IP2_counter_vlan_cache_idx(mesa_vid_t vlan, int *idx)
{
    int i;

    IP2_CRIT_ASSERT_LOCKED();
    for (i = 0; i < IP2_vlan_index.size(); i++) {
        if (IP2_vlan_index[i] == vlan) {
            *idx = i;
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}
#endif /* VTSS_SW_OPTION_L3RT */

int vtss_l3_neighbour_to_txt(char                      *buf,
                             int                        size,
                             const mesa_l3_neighbour_t *const nb)
{
    int s = 0;
    PRINTF("{DMAC: " VTSS_MAC_FORMAT " VLAN: %u IP: ",
           VTSS_MAC_ARGS(nb->dmac), nb->vlan);
    PRINTFUNC(vtss_ip_ip_addr_to_txt, &(nb->dip));
    PRINTF("}");
    return s;
}

#define MAX_MAC_SUBSCRIPTIONS 1024
static ip2_msg_mac_t mac_sub[MAX_MAC_SUBSCRIPTIONS];

#if defined(VTSS_SW_OPTION_L3RT)
/* Transmits a messages containing all counters to master */
static mesa_rc IP2_tx_counters_build(ip2_msg_ct_rep_t *ct_rep)
{
    unsigned i;
    mesa_rc rc = VTSS_RC_OK;
    IP2_CRIT_ASSERT_LOCKED();

    /* Get local rleg counters */
    for (i = 0; i < ct_rep->rleg_cnt.size(); i++) {
        ct_rep->rleg_cnt[i].vlan = IP2_vlan_index[i];

        if (IP2_vlan_index[i] == 0) {
            continue;
        }

        RC_UPDATE(mesa_l3_counters_rleg_get(NULL, IP2_vlan_index[i],
                                            &ct_rep->rleg_cnt[i].counters));
    }
    ct_rep->rc = rc;

    return rc;
}
#endif /* VTSS_SW_OPTION_L3RT */


#if defined(VTSS_SW_OPTION_L3RT)
static mesa_rc IP2_rx_counters_process(const ip2_msg_ct_rep_t *ct_rep)
{
    unsigned i;
    IP2_CRIT_ASSERT_LOCKED();

    if (ct_rep->rc != VTSS_OK) {
        W("Error from slave: %u (%s)", ct_rep->rc, error_txt(ct_rep->rc));
        return IP2_ERROR_SLAVE;
    }

#define UPDATE_CNT(__NAME__)                          \
    IP2_counters_stack.rleg[idx].counters.__NAME__ += \
        ct_rep->rleg_cnt[i].counters.__NAME__

    /* Update aggregated rleg counters */
    for (i = 0; i < ct_rep->rleg_cnt.size(); i++) {
        int idx;

        if (ct_rep->rleg_cnt[i].vlan == 0) {
            continue;
        }

        if (IP2_counter_vlan_cache_idx(ct_rep->rleg_cnt[i].vlan,
                                       &idx) != VTSS_RC_OK) {
            E("Failed to find local index for vlan = %u",
              ct_rep->rleg_cnt[i].vlan);
            continue;
        }

        UPDATE_CNT(ipv4uc_received_octets);
        UPDATE_CNT(ipv4uc_received_frames);
        UPDATE_CNT(ipv6uc_received_octets);
        UPDATE_CNT(ipv6uc_received_frames);
        UPDATE_CNT(ipv4uc_transmitted_octets);
        UPDATE_CNT(ipv4uc_transmitted_frames);
        UPDATE_CNT(ipv6uc_transmitted_octets);
        UPDATE_CNT(ipv6uc_transmitted_frames);
    }
#undef UPDATE_CNT

    I("Updated counter");
    return VTSS_OK;
}
#endif /* VTSS_SW_OPTION_L3RT */

#if defined(VTSS_SW_OPTION_L3RT)
static void IP2_counters_poll(void)
{
    I("Updating local counters");
    IP2_counters_stack.rleg.clear();
    ip2_msg_ct_rep_t msg_rep;
    (void) IP2_tx_counters_build(&msg_rep);
    (void) IP2_rx_counters_process(&msg_rep);
}
#endif /* VTSS_SW_OPTION_L3RT */

const char *ip_chip_error_txt(mesa_rc rc)
{
    switch (rc) {
    case IP2_ERROR_ISID:
        return "Invalid Switch ID";
    case IP2_ERROR_INVALID_RLEG:
        return "Invalid router leg parameters";
    case IP2_ERROR_INVALID_NOADD:
        return "Unable to add more router legs";
    case IP2_ERROR_PORT:
        return "Invalid port number";
    case IP2_ERROR_SLAVE:
        return "Could not get data from slave switch";
    case IP2_ERROR_VALUE:
        return "Invalid value";
    default:
        return "";
    }
}

static inline
BOOL mac_equal(const mesa_mac_t *const a,
               const mesa_mac_t *const b)
{
    u32 i;

    for (i = 0; i < 6; ++i) {
        if (a->addr[i] != b->addr[i]) {
            return FALSE;
        }
    }

    return TRUE;
}

static inline
BOOL msg_mac_equal(const ip2_msg_mac_t *const a,
                   const ip2_msg_mac_t *const b)
{
    if (!mac_equal(&a->mac, &b->mac)) {
        return FALSE;
    }

    if (a->vlan != b->vlan) {
        return FALSE;
    }

    return TRUE;
}


static mesa_rc IP2_mac_table_del(const mesa_vid_mac_t *m)
{
    mesa_rc rc;

    rc = mesa_mac_table_del(NULL, m);
    if (rc != VTSS_RC_OK) {
        D("mesa_mac_table_del({%u, " VTSS_MAC_FORMAT "}) failed",
          m->vid, VTSS_MAC_ARGS(m->mac));
    }

    return rc;
}

static mesa_rc IP2_mac_table_add(const mesa_mac_table_entry_t *e)
{
    mesa_rc rc;

    rc = mesa_mac_table_add(NULL, e);
    if (rc != VTSS_RC_OK) {
        D("mesa_mac_table_add({%u, " VTSS_MAC_FORMAT ", %d, %d}) failed",
          e->vid_mac.vid, VTSS_MAC_ARGS(e->vid_mac.mac), e->locked,
          e->copy_to_cpu);
    }

    return rc;
}

static mesa_rc IP2_mac_subscribe_impl(const ip2_msg_mac_t *mac)
{
    mesa_rc rc;
    BOOL found_one = FALSE;
    mesa_mac_table_entry_t entry;
    int i, match_cnt = 0, mac_sub_store_idx = 0;
    int retry_cnt = 0;

    for (i = 0; i < MAX_MAC_SUBSCRIPTIONS; ++i) {
        if (msg_mac_equal(&mac_sub[i], mac)) {
            match_cnt ++;
        }

        if (mac_sub[i].vlan != 0) {
            continue;
        }

        if (!found_one) {
            mac_sub_store_idx = i;
            mac_sub[i] = *mac;
            found_one = TRUE;
        }
    }

    if (!found_one) {
        W("No free mac entries in the cache");
        return VTSS_RC_ERROR;
    }

    if (match_cnt) {
        W("Mac entry allready exists: {%u, " VTSS_MAC_FORMAT "}",
          mac->vlan, VTSS_MAC_ARGS(mac->mac));
        return VTSS_RC_ERROR;
    }

    memset(&entry, 0x0, sizeof(mesa_mac_table_entry_t));
    entry.vid_mac.vid = mac->vlan;
    entry.vid_mac.mac = mac->mac;
    entry.locked = TRUE;
    entry.cpu_queue = (mac->mac.addr[0] == 0xff ? PACKET_XTR_QU_BC : PACKET_XTR_QU_MGMT_MAC);
    entry.copy_to_cpu = TRUE;

    // only multicast address are added this way
    port_iter_t pit;
    (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL,
                          PORT_ITER_SORT_ORDER_IPORT,
                          PORT_ITER_FLAGS_ALL);
    while (port_iter_getnext(&pit)) {
        entry.destination[pit.iport] = TRUE;
    }

    N("%d MAC-ADD: %u " VTSS_MAC_FORMAT, retry_cnt, entry.vid_mac.vid,
      VTSS_MAC_ARGS(entry.vid_mac.mac));
    rc = IP2_mac_table_add(&entry);

    if (rc != VTSS_RC_OK) {
        E("IP2_mac_table_add({%u, " VTSS_MAC_FORMAT ", %d, %d}) failed",
          entry.vid_mac.vid, VTSS_MAC_ARGS(entry.vid_mac.mac),
          entry.locked, entry.copy_to_cpu);
        mac_sub[mac_sub_store_idx].vlan = 0;
    }

    return rc;
}

static mesa_rc IP2_mac_unsubscribe_impl(const ip2_msg_mac_t *mac)
{
    int i;
    mesa_rc rc;
    int cnt = 0;
    mesa_vid_mac_t vid_mac;

    for (i = 0; i < MAX_MAC_SUBSCRIPTIONS; ++i) {
        if (msg_mac_equal(&mac_sub[i], mac)) {
            // mark as invalid
            mac_sub[i].vlan = 0;
            cnt ++;
        }
    }

    if (cnt != 1) {
        W("Found %d entries in the cache: {%u, " VTSS_MAC_FORMAT "}",
          cnt, mac->vlan, VTSS_MAC_ARGS(mac->mac));
    }

    vid_mac.vid = mac->vlan;
    vid_mac.mac = mac->mac;
    N("MAC-DEL: %u " VTSS_MAC_FORMAT, vid_mac.vid,
      VTSS_MAC_ARGS(vid_mac.mac));
    rc = IP2_mac_table_del(&vid_mac);

    if (rc != VTSS_RC_OK) {
        I("IP2_mac_table_del({%u, " VTSS_MAC_FORMAT "}) failed",
          vid_mac.vid, VTSS_MAC_ARGS(vid_mac.mac));
    }
    return rc;
}

static void IP2_mac_unsubscribe_all_impl(void)
{
    int i;
    mesa_rc rc;
    mesa_vid_mac_t vid_mac;

    for (i = 0; i < MAX_MAC_SUBSCRIPTIONS; ++i) {
        if (mac_sub[i].vlan == 0) {
            continue;
        }

        mac_sub[i].vlan = 0;
        vid_mac.vid = mac_sub[i].vlan;
        vid_mac.mac = mac_sub[i].mac;
        D("MAC-DEL: %u " VTSS_MAC_FORMAT, vid_mac.vid,
          VTSS_MAC_ARGS(vid_mac.mac));
        rc = IP2_mac_table_del(&vid_mac);
        if (rc != VTSS_RC_OK) {
            D("IP2_mac_table_del({%u, " VTSS_MAC_FORMAT "}) failed", vid_mac.vid,
              VTSS_MAC_ARGS(vid_mac.mac));
        }
    }
}

static mesa_rc IP2_flush(void)
{
    IP2_mac_unsubscribe_all_impl();
    (void)mesa_l3_flush(NULL);
    return VTSS_RC_OK;
}

static mesa_rc IP2_rleg_add_local(const mesa_l3_rleg_conf_t *const rl)
{
    R_CHECK(IP2_counter_vlan_cache_add_dry(rl->vlan),
            "vlan = %u", rl->vlan);
    R_CHECK(mesa_l3_rleg_add(NULL, rl),
            "rl={ipv4_uc=%d, ipv6_uc=%d, ipv4_icmp=%d, ipv6_icmp=%d, vlan=%u}",
            rl->ipv4_unicast_enable, rl->ipv6_unicast_enable,
            rl->ipv4_icmp_redirect_enable, rl->ipv6_icmp_redirect_enable,
            rl->vlan);
    R_CHECK(IP2_counter_vlan_cache_add(rl->vlan), "vlan = %u", rl->vlan);
    return VTSS_RC_OK;
}

static mesa_rc IP2_rleg_del_local(const mesa_vid_t vlan)
{
    R_CHECK(IP2_counter_vlan_cache_del_dry(vlan), "vlan = %u", vlan);
    R_CHECK(mesa_l3_rleg_del(NULL, vlan), "vlan=%u", vlan);
    R_CHECK(IP2_counter_vlan_cache_del(vlan), "vlan = %u", vlan);
    return VTSS_RC_OK;
}

static mesa_rc IP2_common_set(const mesa_l3_common_conf_t *const conf)
{
    D("%s", __FUNCTION__);
    (void)mesa_l3_common_set(0, conf);
    return VTSS_RC_OK;
}


static mesa_rc IP2_rleg_add(const mesa_l3_rleg_conf_t *const rl)
{
    (void)IP2_rleg_add_local(rl);
    return VTSS_RC_OK;
}

static mesa_rc IP2_rleg_del(const mesa_l3_rleg_conf_t *const rl)
{
    (void)IP2_rleg_del_local(rl->vlan);
    return VTSS_RC_OK;
}

#if defined(VTSS_SW_OPTION_L3RT)
static mesa_rc IP2_route_add(const mesa_routing_entry_t *const rt)
{
#if VTSS_APPL_IP_DEBUG
    R_CHECK_FMT(mesa_l3_route_add(NULL, rt), W, vtss_ip_route_entry_to_txt, rt);
    return VTSS_RC_OK;
#else
    return mesa_l3_route_add(NULL, rt);
#endif
}
#endif /* VTSS_SW_OPTION_L3RT */

#if defined(VTSS_SW_OPTION_L3RT)
static mesa_rc IP2_route_del(const mesa_routing_entry_t *const rt)
{
#if VTSS_APPL_IP_DEBUG
    R_CHECK_FMT(mesa_l3_route_del(NULL, rt), W, vtss_ip_route_entry_to_txt, rt);
    return VTSS_RC_OK;
#else
    return mesa_l3_route_del(NULL, rt);
#endif
}
#endif /* VTSS_SW_OPTION_L3RT */

#if defined(VTSS_SW_OPTION_L3RT)
static mesa_rc IP2_neighbour_add(const mesa_l3_neighbour_t *const nb)
{
#if VTSS_APPL_IP_DEBUG
    R_CHECK_FMT(mesa_l3_neighbour_add(NULL, nb), E, vtss_l3_neighbour_to_txt, nb);
    return VTSS_RC_OK;
#else
    return mesa_l3_neighbour_add(NULL, nb);
#endif
}
#endif /* VTSS_SW_OPTION_L3RT */

#if defined(VTSS_SW_OPTION_L3RT)
static mesa_rc IP2_neighbour_del(const mesa_l3_neighbour_t *const nb)
{
#if VTSS_APPL_IP_DEBUG
    R_CHECK_FMT(mesa_l3_neighbour_del(NULL, nb), I, vtss_l3_neighbour_to_txt, nb);
    return VTSS_RC_OK;
#else
    return mesa_l3_neighbour_del(NULL, nb);
#endif
}
#endif /* VTSS_SW_OPTION_L3RT */

static mesa_rc IP2_mac_subscribe(const mesa_mac_t *mac,
                                 const mesa_vid_t vlan)
{
    mesa_rc rc;
    ip2_msg_mac_t msg;
    msg.mac = *mac;
    msg.vlan = vlan;

    rc = IP2_mac_subscribe_impl(&msg);
    if (rc != VTSS_RC_OK) {
        E("IP2_mac_subscribe_impl failed: vid=%u, mac=" VTSS_MAC_FORMAT,
          vlan, VTSS_MAC_ARGS(*mac));
        return rc;
    }
    return VTSS_RC_OK;
}

static mesa_rc IP2_mac_unsubscribe(const mesa_mac_t *mac,
                                   const mesa_vid_t vlan)
{
    ip2_msg_mac_t msg;
    msg.mac = *mac;
    msg.vlan = vlan;

    (void) IP2_mac_unsubscribe_impl(&msg);
    return VTSS_RC_OK;
}

#if defined(VTSS_SW_OPTION_L3RT)
static mesa_rc IP2_counters_vlan_clear(const mesa_vid_t vlan)
{
    R_CHECK(mesa_l3_counters_rleg_clear(NULL, vlan), " vlan=%u", vlan);
    return VTSS_RC_OK;
}
#endif /* VTSS_SW_OPTION_L3RT */

static BOOL IP2_is_base_mac(const mesa_mac_t *mac)
{
    return (mac->addr[0] == base_mac.addr[0] &&
            mac->addr[1] == base_mac.addr[1] &&
            mac->addr[2] == base_mac.addr[2] &&
            mac->addr[3] == base_mac.addr[3] &&
            mac->addr[4] == base_mac.addr[4] &&
            mac->addr[5] == base_mac.addr[5]);
}

static BOOL IP2_is_broadcast_mac(const mesa_mac_t *mac)
{
    return (mac->addr[0] & 0x1);
}

class IpChipLpm: public IpChip {

mesa_rc init(void)
{
    D("%s", __FUNCTION__);

    critd_init(&crit,
               "ip.chip.crit",
               VTSS_MODULE_ID_IP_CHIP,
               VTSS_TRACE_MODULE_ID,
               CRITD_TYPE_MUTEX);
    IP2_CRIT_RETURN_RC(VTSS_RC_OK);
}

mesa_rc start(void)
{
    return VTSS_RC_OK;
}

mesa_rc master_up(const mesa_mac_t *const mac)
{
    mesa_rc rc = VTSS_RC_OK;

    IP2_CRIT_ENTER();
    D("%s " VTSS_MAC_FORMAT, __FUNCTION__, VTSS_MAC_ARGS(*mac));

    base_mac = *mac;
    mesa_l3_common_conf_t common;

    DO(mesa_l3_common_get(0, &common));
    common.rleg_mode = MESA_ROUTING_RLEG_MAC_MODE_SINGLE;
    common.base_address = *mac;

    IP2_mac_unsubscribe_all_impl();
    DO(mesa_l3_flush(0));
    DO(mesa_l3_common_set(0, &common));
    vtss_clear(IP2_vlan_index);
#if defined(VTSS_SW_OPTION_L3RT)
    vtss_clear(IP2_counters_stack);
#endif /* VTSS_SW_OPTION_L3RT */

    IP2_CRIT_RETURN_RC(VTSS_RC_OK);
}

mesa_rc master_down(void)
{
    mesa_rc rc = VTSS_RC_OK;

    IP2_CRIT_ENTER();
    D("%s", __FUNCTION__);
    (void)IP2_flush();
    IP2_CRIT_RETURN_RC(rc);
}

mesa_rc switch_add(const vtss_isid_t id)
{
    return VTSS_RC_OK;
}

mesa_rc switch_del(const vtss_isid_t id)
{
    return VTSS_RC_OK;
}

mesa_rc routing_enable(BOOL enable)
{
    mesa_rc rc = VTSS_RC_OK;

    IP2_CRIT_ENTER();

    D("%s(%s)", __FUNCTION__, enable ? "TRUE" : "FALSE");

    mesa_l3_common_conf_t common;
    DO(mesa_l3_common_get(0, &common));
    common.routing_enable = enable;

    IP2_CRIT_RETURN_RC(IP2_common_set(&common));
}

mesa_rc rleg_add(const mesa_vid_t vlan)
{
    IP2_CRIT_ENTER();
    mesa_l3_rleg_conf_t conf = {
        .ipv4_unicast_enable = TRUE,
        .ipv6_unicast_enable = TRUE,
        .ipv4_icmp_redirect_enable = TRUE,
        .ipv6_icmp_redirect_enable = TRUE,
        .vlan = vlan
    };

    // this will be added as a router leg on all nodes
    IP2_CRIT_RETURN_RC(IP2_rleg_add(&conf));
}

mesa_rc rleg_del(const mesa_vid_t vlan)
{
    IP2_CRIT_ENTER();

    mesa_l3_rleg_conf_t conf = {
        .ipv4_unicast_enable = FALSE,
        .ipv6_unicast_enable = FALSE,
        .ipv4_icmp_redirect_enable = FALSE,
        .ipv6_icmp_redirect_enable = FALSE,
        .vlan = vlan
    };

    // this will be added as a router leg on all nodes
    IP2_CRIT_RETURN_RC(IP2_rleg_del(&conf));
}

mesa_rc mac_subscribe(const mesa_vid_t vlan,
                                   const mesa_mac_t *const mac)
{
    IP2_CRIT_ENTER();
    D("%s vlan: %u, " VTSS_MAC_FORMAT,
      __FUNCTION__, vlan, VTSS_MAC_ARGS(*mac));

    if (IP2_is_base_mac(mac)) {
        N("Implicit done when calling rleg_add");
        IP2_CRIT_RETURN_RC(VTSS_RC_OK);

    } else if (IP2_is_broadcast_mac(mac)) {
        IP2_CRIT_RETURN_RC(IP2_mac_subscribe(mac, vlan));

    } else {
        // not supported!
        E("Subscription of mac address " VTSS_MAC_FORMAT " "
          " at vlan %d is not supported",
          VTSS_MAC_ARGS(*mac), vlan);
        IP2_CRIT_RETURN_RC(VTSS_RC_ERROR);
    }
}

mesa_rc mac_unsubscribe(const mesa_vid_t vlan,
                                     const mesa_mac_t *const mac)
{
    IP2_CRIT_ENTER();
    D("%s vlan: %u, " VTSS_MAC_FORMAT,
      __FUNCTION__, vlan, VTSS_MAC_ARGS(*mac));

    if (IP2_is_base_mac(mac)) {
        I("Implicit done when calling rleg_del");
        IP2_CRIT_RETURN_RC(VTSS_RC_OK);

    } else if (IP2_is_broadcast_mac(mac)) {
        IP2_CRIT_RETURN_RC(IP2_mac_unsubscribe(mac, vlan));

    } else {
        // not supported!
        IP2_CRIT_RETURN_RC(VTSS_RC_ERROR);
    }
}

mesa_rc route_add(const mesa_routing_entry_t *const rt)
{
#if defined(VTSS_SW_OPTION_L3RT)
#  define BUF_SIZE 128
#if VTSS_APPL_IP_DEBUG
    char buf[BUF_SIZE];
    (void)vtss_ip_route_entry_to_txt(buf, BUF_SIZE, rt);
    D("%s %s", __FUNCTION__, buf);
#endif

    IP2_CRIT_ENTER();
    IP2_CRIT_RETURN_RC(IP2_route_add(rt));
#  undef BUF_SIZE
#else
    return VTSS_RC_OK;
#endif /* VTSS_SW_OPTION_L3RT */
}

mesa_rc route_del(const mesa_routing_entry_t *const rt)
{

#if defined(VTSS_SW_OPTION_L3RT)
#  define BUF_SIZE 128
#if VTSS_APPL_IP_DEBUG
    char buf[BUF_SIZE];
    (void)vtss_ip_route_entry_to_txt(buf, BUF_SIZE, rt);
    D("%s %s", __FUNCTION__, buf);
#endif

    IP2_CRIT_ENTER();
    IP2_CRIT_RETURN_RC(IP2_route_del(rt));
#  undef BUF_SIZE
#else
    return VTSS_RC_OK;
#endif /* VTSS_SW_OPTION_L3RT */
}

mesa_rc route_bulk_add(uint32_t cnt,
                       const mesa_routing_entry_t *const rt,
                       uint32_t *cnt_out) {
    IP2_CRIT_ENTER();
    IP2_CRIT_RETURN_RC(mesa_l3_route_bulk_add(0, cnt, rt, cnt_out));
}

mesa_rc route_bulk_del(uint32_t cnt,
                       const mesa_routing_entry_t *const rt,
                       uint32_t *cnt_out) {
    IP2_CRIT_ENTER();
    IP2_CRIT_RETURN_RC(mesa_l3_route_bulk_del(0, cnt, rt, cnt_out));
}

mesa_rc neighbour_add(const vtss_neighbour_t *const nb)
{
#if defined(VTSS_SW_OPTION_L3RT)
#  define BUF_SIZE 128

    mesa_l3_neighbour_t _nb;
#if VTSS_APPL_IP_DEBUG
    char buf[BUF_SIZE];
    (void)vtss_ip_neighbour_to_txt(buf, BUF_SIZE, nb);
    D("%s %s", __FUNCTION__, buf);
#endif

    _nb.dmac = nb->dmac;
    _nb.vlan = nb->vlan;
    _nb.dip = nb->dip;

    IP2_CRIT_ENTER();
    IP2_CRIT_RETURN_RC(IP2_neighbour_add(&_nb));
#  undef BUF_SIZE
#else
    return VTSS_RC_OK;
#endif /* VTSS_SW_OPTION_L3RT */
}

mesa_rc neighbour_del(const vtss_neighbour_t *const nb)
{

#if defined(VTSS_SW_OPTION_L3RT)
#  define BUF_SIZE 128
    mesa_l3_neighbour_t _nb;
#if VTSS_APPL_IP_DEBUG
    char buf[BUF_SIZE];
    (void)vtss_ip_neighbour_to_txt(buf, BUF_SIZE, nb);
    D("%s %s", __FUNCTION__, buf);
#endif

    _nb.dmac = nb->dmac;
    _nb.vlan = nb->vlan;
    _nb.dip = nb->dip;
    IP2_CRIT_ENTER();
    IP2_CRIT_RETURN_RC(IP2_neighbour_del(&_nb));
#  undef BUF_SIZE
#else
    return VTSS_RC_OK;
#endif /* VTSS_SW_OPTION_L3RT */
}

mesa_rc counters_vlan_get(const mesa_vid_t vlan,
                                       mesa_l3_counters_t *counters)
{
#if defined(VTSS_SW_OPTION_L3RT)
    int idx;

    IP2_CRIT_ENTER();

    IP2_counters_poll();

    if (IP2_counter_vlan_cache_idx(vlan, &idx) != VTSS_RC_OK) {
        IP2_CRIT_RETURN_RC(VTSS_RC_ERROR);
    }

    *counters = IP2_counters_stack.rleg[idx].counters;

    IP2_CRIT_RETURN_RC(VTSS_RC_OK);
#else
    return VTSS_RC_OK;
#endif /* VTSS_SW_OPTION_L3RT */
}

mesa_rc counters_system_get(mesa_l3_counters_t *c)
{
#if defined(VTSS_SW_OPTION_L3RT)
    int i;

    memset(c, 0, sizeof(*c));

    IP2_CRIT_ENTER();
    IP2_counters_poll();

    for (i = 0; i < IP2_vlan_index.size(); ++i) {
        if (!IP2_vlan_index[i]) continue;

        mesa_l3_counters_t *cnt = &IP2_counters_stack.rleg[i].counters;
        c->ipv4uc_received_octets    += cnt->ipv4uc_received_octets;
        c->ipv4uc_received_frames    += cnt->ipv4uc_received_frames;
        c->ipv6uc_received_octets    += cnt->ipv6uc_received_octets;
        c->ipv6uc_received_frames    += cnt->ipv6uc_received_frames;
        c->ipv4uc_transmitted_octets += cnt->ipv4uc_transmitted_octets;
        c->ipv4uc_transmitted_frames += cnt->ipv4uc_transmitted_frames;
        c->ipv6uc_transmitted_octets += cnt->ipv6uc_transmitted_octets;
        c->ipv6uc_transmitted_frames += cnt->ipv6uc_transmitted_frames;
    }

    IP2_CRIT_RETURN_RC(VTSS_RC_OK);
#else
    return VTSS_RC_OK;
#endif /* VTSS_SW_OPTION_L3RT */
}

mesa_rc counters_vlan_clear(const mesa_vid_t vlan)
{
#if defined(VTSS_SW_OPTION_L3RT)
    mesa_rc rc;

    IP2_CRIT_ENTER();
    rc = IP2_counters_vlan_clear(vlan);
    IP2_counters_stack.rleg.clear();

    IP2_CRIT_RETURN_RC(rc);
#else
    return VTSS_RC_OK;
#endif /* VTSS_SW_OPTION_L3RT */
}

};

static class IpChipLpm ipLpm;

class IpChip* getIpLpm()
{
    return &ipLpm;
}
