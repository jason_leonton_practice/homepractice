/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#include "ip_api.h"
#include "ip_snmp.h"
#include "ip_utils.h"
#include "ip_trace.h"
#include "critd_api.h"

#ifdef VTSS_SW_OPTION_SNMP

#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_IP
#define __GRP VTSS_TRACE_IP_GRP_SNMP
#define E(_fmt, ...) T(__GRP, VTSS_TRACE_LVL_ERROR, _fmt, ##__VA_ARGS__)
#define W(_fmt, ...) T(__GRP, VTSS_TRACE_LVL_WARNING, _fmt, ##__VA_ARGS__)
#define I(_fmt, ...) T(__GRP, VTSS_TRACE_LVL_INFO, _fmt, ##__VA_ARGS__)
#define D(_fmt, ...) T(__GRP, VTSS_TRACE_LVL_DEBUG, _fmt, ##__VA_ARGS__)
#define N(_fmt, ...) T(__GRP, VTSS_TRACE_LVL_NOISE, _fmt, ##__VA_ARGS__)
#define R(_fmt, ...) T(__GRP, VTSS_TRACE_LVL_RACKET, _fmt, ##__VA_ARGS__)

static critd_t ip2_crit;

#if (VTSS_TRACE_ENABLED)
#  define IP2_CRIT_ENTER()                                                  \
      critd_enter(&ip2_crit, VTSS_TRACE_IP_GRP_CRIT, VTSS_TRACE_LVL_NOISE, \
                  __FILE__, __LINE__)
#  define IP2_CRIT_EXIT()                                                   \
      critd_exit(&ip2_crit, VTSS_TRACE_IP_GRP_CRIT, VTSS_TRACE_LVL_NOISE,  \
                 __FILE__, __LINE__)

#  define IP2_CRIT_ASSERT_LOCKED()                 \
    critd_assert_locked(&ip2_crit,                 \
                        TRACE_GRP_CRIT,            \
                        __FILE__, __LINE__)
#else
#  define IP2_CRIT_ENTER() critd_enter(&ip2_crit)
#  define IP2_CRIT_EXIT()  critd_exit( &ip2_crit)
#  define IP2_CRIT_ASSERT_LOCKED() critd_assert_locked(&ip2_crit)
#endif /* VTSS_TRACE_ENABLED */

#define IP2_CRIT_RETURN(T, X) \
do {                          \
    T __val = (X);            \
    IP2_CRIT_EXIT();          \
    return __val;             \
} while(0)

#define IP2_CRIT_RETURN_RC(X)    \
    IP2_CRIT_RETURN(mesa_rc, X)

#define IP2_CRIT_RETURN_VOID()   \
    IP2_CRIT_EXIT();             \
    return
typedef struct {
    bool                active;

    bool                has_ipv4;
    mesa_ipv4_network_t ipv4;
    vtss_tick_count_t   ipv4_created;
    vtss_tick_count_t   ipv4_changed;

    bool                has_ipv6;
    mesa_ipv6_network_t ipv6;
    vtss_tick_count_t   ipv6_created;
    vtss_tick_count_t   ipv6_changed;
} if_snmp_status_t;

vtss_tick_count_t IP2_if_table_changed = 0;

// TODO, use a map instead
static if_snmp_status_t IP2_status[4096];

void SNMP_if_change_callback(vtss_if_id_vlan_t if_id)
{
    vtss_ifindex_t ifidx;
    vtss_appl_ip_if_status_t st;
    bool exists;

    (void) vtss_ifindex_from_vlan(if_id, &ifidx);

    IP2_CRIT_ENTER();
    exists = (vtss_appl_ip_if_status_get_first(VTSS_APPL_IP_IF_STATUS_TYPE_ANY,
                                               ifidx, &st) == VTSS_RC_OK);

    if ((!IP2_status[if_id].active) && exists) {
        D("vlan created %u", if_id);
        IP2_status[if_id].active = TRUE;
        IP2_if_table_changed = vtss_current_time();
    }

    if (IP2_status[if_id].active && (!exists)) {
        D("vlan deleted %u", if_id);
        memset(&IP2_status[if_id], 0, sizeof(if_snmp_status_t));
        IP2_if_table_changed = vtss_current_time();
        IP2_CRIT_RETURN_VOID();
    }

    I("SNMP_if_change_callback vlan=%u", if_id);
    if (vtss_appl_ip_if_status_get_first(VTSS_APPL_IP_IF_STATUS_TYPE_IPV4,
                                         ifidx, &st) == VTSS_RC_OK ) {

        if (!IP2_status[if_id].has_ipv4) {
            // IPv4 address created
            D("ipv4 address created %u " VTSS_IPV4N_FORMAT, if_id,
              VTSS_IPV4N_ARG(st.u.ipv4.net));
            IP2_status[if_id].ipv4_created = vtss_current_time();
            IP2_status[if_id].ipv4_changed = vtss_current_time();

        } else if (vtss_ipv4_network_equal(&IP2_status[if_id].ipv4,
                                           &st.u.ipv4.net)) {
            // IPv4 address changed
            D("ipv4 address changed %u " VTSS_IPV4N_FORMAT, if_id,
              VTSS_IPV4N_ARG(st.u.ipv4.net));
            IP2_status[if_id].ipv4_changed = vtss_current_time();
        }

        IP2_status[if_id].ipv4 = st.u.ipv4.net;
        IP2_status[if_id].has_ipv4 = TRUE;

    } else {
        if (IP2_status[if_id].has_ipv4) {
            D("ipv4 address deleted %u ", if_id);
        }
        IP2_status[if_id].has_ipv4 = FALSE;
    }


    if (vtss_appl_ip_if_status_get_first(VTSS_APPL_IP_IF_STATUS_TYPE_IPV6,
                                         ifidx, &st) == VTSS_RC_OK ) {

        if (!IP2_status[if_id].has_ipv6) {
            D("ipv6 address created %u " VTSS_IPV6N_FORMAT, if_id,
              VTSS_IPV6N_ARG(st.u.ipv6.net));
            IP2_status[if_id].ipv6_created = vtss_current_time();
            IP2_status[if_id].ipv6_changed = vtss_current_time();

        } else if (vtss_ipv6_network_equal(&IP2_status[if_id].ipv6,
                                           &st.u.ipv6.net)) {
            D("ipv6 address changed %u " VTSS_IPV6N_FORMAT, if_id,
              VTSS_IPV6N_ARG(st.u.ipv6.net));
            IP2_status[if_id].ipv6_changed = vtss_current_time();
        }

        IP2_status[if_id].ipv6 = st.u.ipv6.net;
        IP2_status[if_id].has_ipv6 = TRUE;

    } else {
        if (IP2_status[if_id].has_ipv6) {
            D("ipv6 address deleted %u ", if_id);
        }
        IP2_status[if_id].has_ipv6 = false;
    }

    IP2_CRIT_RETURN_VOID();
}

mesa_rc vtss_ip_snmp_init()
{
    mesa_rc rc;
    vtss_if_id_vlan_t i;

    critd_init(&ip2_crit, "ip-snmp.crit",
               VTSS_MODULE_ID_IP, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);

    I("vtss_ip_snmp_init");

    memset(IP2_status, 0, sizeof(IP2_status));
    rc = vtss_ip_if_callback_add(SNMP_if_change_callback);

    if (rc != VTSS_RC_OK) {
        E("Failed to registerer callback");
    }

    IP2_CRIT_EXIT();

    // be sure to have an updated state
    if (rc == VTSS_RC_OK) {
        for (i = 0; i < 4096; ++i) {
            SNMP_if_change_callback(i);
        }
    }

    return rc;
}

void vtss_ip_snmp_signal_global_changes()
{
    IP2_CRIT_ENTER();
    IP2_if_table_changed = vtss_current_time();
    IP2_CRIT_EXIT();
}

mesa_rc vtss_ip_interfaces_last_change(u64 *time)
{
    IP2_CRIT_ENTER();
    D("vtss_ip_interfaces_last_change");
    *time = VTSS_OS_TICK2MSEC(IP2_if_table_changed) / 1000LLU;
    IP2_CRIT_RETURN_RC(VTSS_RC_OK);
}

mesa_rc vtss_ip_address_created_ipv4(vtss_if_id_vlan_t if_id, u64 *time)
{
    mesa_rc rc = VTSS_RC_ERROR;

    IP2_CRIT_ENTER();
    D("vtss_ip_address_created_ipv4 vlan=%u", if_id);
    if (IP2_status[if_id].has_ipv4) {
        *time = VTSS_OS_TICK2MSEC(IP2_status[if_id].ipv4_created) / 1000LLU;
        rc = VTSS_RC_OK;
    }
    IP2_CRIT_RETURN_RC(rc);
}

mesa_rc vtss_ip_address_changed_ipv4(vtss_if_id_vlan_t if_id, u64 *time)
{
    mesa_rc rc = VTSS_RC_ERROR;

    IP2_CRIT_ENTER();
    D("vtss_ip_address_changed_ipv4 vlan=%u", if_id);
    if (IP2_status[if_id].has_ipv4) {
        *time = VTSS_OS_TICK2MSEC(IP2_status[if_id].ipv4_changed) / 1000LLU;
        rc = VTSS_RC_OK;
    }
    IP2_CRIT_RETURN_RC(rc);
}

mesa_rc vtss_ip_address_created_ipv6(vtss_if_id_vlan_t if_id, u64 *time)
{
    mesa_rc rc = VTSS_RC_ERROR;

    IP2_CRIT_ENTER();
    D("vtss_ip_address_created_ipv6 vlan=%u", if_id);
    if (IP2_status[if_id].has_ipv6) {
        *time = VTSS_OS_TICK2MSEC(IP2_status[if_id].ipv6_created) / 1000LLU;
        rc = VTSS_RC_OK;
    }
    IP2_CRIT_RETURN_RC(rc);
}

mesa_rc vtss_ip_address_changed_ipv6(vtss_if_id_vlan_t if_id, u64 *time)
{
    mesa_rc rc = VTSS_RC_ERROR;

    IP2_CRIT_ENTER();
    D("vtss_ip_address_changed_ipv6 vlan=%u", if_id);
    if (IP2_status[if_id].has_ipv6) {
        *time = VTSS_OS_TICK2MSEC(IP2_status[if_id].ipv6_changed) / 1000LLU;
        rc = VTSS_RC_OK;
    }
    IP2_CRIT_RETURN_RC(rc);
}

#endif /* VTSS_SW_OPTION_SNMP */

