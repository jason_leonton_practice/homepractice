/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_IP /* TBD - GRP...? */

#include "main.h"
#include "ip_api.h"
//#include "ip_trace.h"
#include "ip_chip_api.h"
#include "packet_api.h"
#include "msg_api.h"
#include "port_api.h"

class IpChipMac: public IpChip {

mesa_rc init(void)
{
    return VTSS_OK;
}

mesa_rc start(void)
{
    return VTSS_OK;
}

mesa_rc master_up(const mesa_mac_t *const mac)
{
    return VTSS_OK;
}

mesa_rc master_down(void)
{
    return VTSS_OK;
}

mesa_rc switch_add(vtss_isid_t isid)
{
    return VTSS_OK;
}

mesa_rc switch_del(vtss_isid_t isid)
{
    return VTSS_OK;
}

mesa_rc mac_subscribe(const mesa_vid_t vlan, const mesa_mac_t *const mac)
{
    mesa_mac_table_entry_t entry;
    BOOL                   is_mc = (mac->addr[0] & 0x1 ? TRUE : FALSE);

    T_D("Add MAC %d", vlan);

    memset(&entry, 0x0, sizeof(mesa_mac_table_entry_t));
    entry.vid_mac.vid = vlan;
    entry.vid_mac.mac = *mac;
    entry.locked = TRUE;
    entry.cpu_queue = (mac->addr[0] == 0xff ? PACKET_XTR_QU_BC : PACKET_XTR_QU_MGMT_MAC);
    entry.copy_to_cpu = TRUE;
    if (is_mc) {
        port_iter_t pit;
        (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        while (port_iter_getnext(&pit)) {
            entry.destination[pit.iport] = TRUE;
        }
    }

    return mesa_mac_table_add(NULL, &entry);
}

mesa_rc rleg_add(const mesa_vid_t vlan)
{
    return VTSS_OK;
}

mesa_rc rleg_del(const mesa_vid_t vlan)
{
    return VTSS_OK;
}

mesa_rc mac_unsubscribe(const mesa_vid_t vlan, const mesa_mac_t *const mac)
{
    mesa_vid_mac_t vid_mac;

    T_D("Remove MAC %d", vlan);

    vid_mac.vid = vlan;
    vid_mac.mac = *mac;
    return mesa_mac_table_del(NULL, &vid_mac);
}

mesa_rc route_add(const mesa_routing_entry_t *const rt)
{
    return VTSS_OK;
}

mesa_rc route_del(const mesa_routing_entry_t *const rt)
{
    return VTSS_OK;
}

mesa_rc route_bulk_add(uint32_t cnt,
                       const mesa_routing_entry_t *const rt,
                       uint32_t *cnt_out) {
    *cnt_out = cnt;
    return VTSS_RC_OK;
}

mesa_rc route_bulk_del(uint32_t cnt,
                       const mesa_routing_entry_t *const rt,
                       uint32_t *cnt_out) {
    *cnt_out = cnt;
    return VTSS_RC_OK;
}


mesa_rc neighbour_add(const vtss_neighbour_t *const nb)
{
    return VTSS_OK;
}

mesa_rc neighbour_del(const vtss_neighbour_t *const nb)
{
    return VTSS_OK;
}

mesa_rc counters_vlan_get(const mesa_vid_t vlan,
                          mesa_l3_counters_t *counters)
{
    memset(counters, 0, sizeof(mesa_l3_counters_t));
    return VTSS_RC_OK;
}

mesa_rc counters_system_get(mesa_l3_counters_t *c)
{
    memset(c, 0, sizeof(mesa_l3_counters_t));
    return VTSS_RC_OK;
}

mesa_rc counters_vlan_clear(const mesa_vid_t vlan)
{
    return VTSS_RC_OK;
}

const char *ip_chip_error_txt(mesa_rc rc)
{
    return "ip_chip: undefined";
}

mesa_rc routing_enable(BOOL enable)
{
    return VTSS_RC_OK;
}

};

static class IpChipMac ipMac;

class IpChip* getIpMac()
{
    return &ipMac;
}
