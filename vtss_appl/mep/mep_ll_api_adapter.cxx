/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#include "mscc/ethernet/switch/api.h"
#include <vtss/appl/mep.h>
#include "mep_ll_api.h"
#include "mep_api.h"

namespace mep_g1 {
    mesa_rc mep_ll_protocol_info_request_tx(const u32 instance, const mep_ll_request_info_t *const req_info);
    mesa_rc mep_ll_protocol_info_reply_tx(const u32 instance, const mep_ll_reply_info_t *const info);
    mesa_rc mep_ll_register(const u32 mep_id);
    mesa_rc mep_ll_deregister(const u32 mep_id);
    u32     mep_tt_loop_mce_id_get();
}

namespace mep_g2 {
    mesa_rc mep_ll_protocol_info_request_tx(const u32 instance, const mep_ll_request_info_t *const req_info);
    mesa_rc mep_ll_protocol_info_reply_tx(const u32 instance, const mep_ll_reply_info_t *const info);
    mesa_rc mep_ll_register(const u32 mep_id);
    mesa_rc mep_ll_deregister(const u32 mep_id);
    u32     mep_tt_loop_mce_id_get();
}

mesa_rc mep_ll_protocol_info_request_tx(const u32 instance, const mep_ll_request_info_t *const req_info)
{








    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_ll_protocol_info_reply_tx(const u32 instance, const mep_ll_reply_info_t *const info)
{








    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_ll_register(const u32 mep_id)
{








    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_ll_deregister(const u32 mep_id)
{








    return MESA_RC_NOT_IMPLEMENTED;
}

u32 mep_tt_loop_mce_id_get()
{








    return 0;
}

