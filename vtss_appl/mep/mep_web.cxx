/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

#include "mscc/ethernet/switch/api.h"
#include "web_api.h"
#include "vtss/appl/mep.h"
#include "mep_api.h"
#include "mep_engine.h" /* Internal interface */
#include "mep_api.h"
#include "mscc/ethernet/switch/api.h"
#include "mep_peer_array.hxx"
#include "packet_api.h"
#ifdef VTSS_SW_OPTION_PRIV_LVL
#include "vtss_privilege_api.h"
#include "vtss_privilege_web_api.h"
#endif


static u8 hexToInt(const char *hex)
{
    return (16 * ((hex[0] >= 'a') ? ((hex[0]-'a')+10) : (hex[0] >= 'A') ? ((hex[0]-'A')+10) : (hex[0]-'0')) + ((hex[1] >= 'a') ? ((hex[1]-'a')+10) : (hex[1] >= 'A') ? ((hex[1]-'A')+10) : (hex[1]-'0')));
}

static const char *mep_web_error_txt(mesa_rc error)
{
    if (error == VTSS_RC_OK)
        return("");
    else
        return(error_txt(error));
}


/****************************************************************************/
/*  Web Handler  functions                                                  */
/****************************************************************************/

static i32 handler_config_mep_create(CYG_HTTPD_STATE *p)
{
    mep_conf_t                   config;
    vtss_mep_mgmt_etree_conf_t   etree_conf;
    vtss_appl_mep_state_t        state;
    vtss_appl_mep_peer_state_t   peer_state;
    mep_default_conf_t           def_conf;
    int                          ct, mep_id, domain, flow, vid, mode, direct, port, level, res_port;
    char                         buf[200];
    u32                          i;
    mesa_rc                      error = VTSS_RC_OK;
    BOOL                         alarm;
    mesa_rc                      rc;
    u32                          vtss_appl_mep_instance_max = VTSS_APPL_MEP_INSTANCE_MAX;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP))
        return -1;
#endif

    if (p->method == CYG_HTTPD_METHOD_POST)
    {
        /* Delete MEP */
        for (mep_id=0; mep_id<vtss_appl_mep_instance_max; ++mep_id) {
            if (((rc = mep_instance_conf_get(mep_id, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_NOT_CREATED) && (rc != VTSS_APPL_MEP_RC_VOLATILE))
                if (error == VTSS_RC_OK)     error = rc;

            if ((rc == VTSS_RC_OK) && config.enable) {   /* Created */
                sprintf(buf, "del_%u", mep_id+1);
                if (cyg_httpd_form_varable_find(p, buf)) {
                    config.enable = FALSE;
                    if ((rc = mep_instance_conf_set(mep_id, &config)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;
                }
            }
        }

        /* Add MEP */
        if (cyg_httpd_form_varable_int(p, "new_mep", &mep_id))  {
            if ((cyg_httpd_form_varable_int(p, "dom", &domain)) &&
                (cyg_httpd_form_varable_int(p, "flow", &flow)) &&
                (cyg_httpd_form_varable_int(p, "direct", &direct)) &&
                (cyg_httpd_form_varable_int(p, "vid", &vid)) &&
                (cyg_httpd_form_varable_int(p, "level", &level)) &&
                (cyg_httpd_form_varable_int(p, "mode", &mode)) &&
                (cyg_httpd_form_varable_int(p, "port", &port))) {
                mep_default_conf_get(&def_conf);
                config = def_conf.config;   /* Initialize confid to default */

                config.domain = (mep_domain_t)domain;
                config.direction = (vtss_appl_mep_direction_t)direct;
                config.level = level;
                config.vid = vid;
                config.mode = (vtss_appl_mep_mode_t)mode;
                config.port = uport2iport(port);

                if (domain != VTSS_APPL_MEP_PORT)
                    config.flow = flow;
                if (domain == VTSS_APPL_MEP_EVC)
                    config.flow--;
                config.enable = TRUE;

                if ((rc = mep_instance_conf_set(mep_id-1, &config)) != VTSS_RC_OK)
                    if (error == VTSS_RC_OK)     error = rc;
            }
        }
        if (error != VTSS_RC_OK) {
            redirect_errmsg(p, "mep.htm", mep_web_error_txt(error));
        } else {
            redirect(p, "/mep.htm");
        }
    } else {
        /* CYG_HTTPD_METHOD_GET (+HEAD) */
        (void)cyg_httpd_start_chunked("html");




        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", 0);

        cyg_httpd_write_chunked(p->outbuffer, ct);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", vtss_appl_mep_instance_max);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        for (mep_id=0; mep_id<vtss_appl_mep_instance_max; ++mep_id) {
            rc = mep_instance_conf_get(mep_id, &config);
            if (rc == VTSS_APPL_MEP_RC_VOLATILE) rc = VTSS_RC_OK;
            res_port = config.port;

            if ((rc != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_NOT_CREATED))
                if (error == VTSS_RC_OK)     error = rc;

            if ((rc == VTSS_RC_OK) && config.enable) {   /* Created */
                memset(&state, 0, sizeof(state));
                if ((rc = vtss_appl_mep_instance_status_get(mep_id, &state)) != VTSS_RC_OK)
                    if (error == VTSS_RC_OK)     error = rc;
                if ((rc = vtss_mep_mgmt_etree_conf_get(mep_id, &etree_conf)) != VTSS_RC_OK)
                    if (error == VTSS_RC_OK)     error = rc;

                alarm = (state.cLevel || state.cMeg || state.cMep || state.cAis || state.cLck || state.cDeg || state.cSsf || (state.oper_state != VTSS_APPL_MEP_OPER_UP));

                for (i=0; i<config.peer_count; ++i) {
                    if ((rc = vtss_appl_mep_instance_peer_status_get(mep_id, config.peer_mep[i], &peer_state)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;
                    alarm = alarm || (peer_state.cLoc || peer_state.cRdi || peer_state.cPeriod || peer_state.cPrio);
                }
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u/%u/%u/%u/%u/%u/%u/%u/%02X-%02X-%02X-%02X-%02X-%02X/%s|",
                              mep_id+1,
                              (uint)config.domain,
                              (uint)config.mode,
                              (uint)config.direction,
                              (uint)etree_conf.e_tree,
                              iport2uport(res_port),
                              config.level,
                              (config.domain == VTSS_APPL_MEP_PORT) ? 0 : (config.domain == VTSS_APPL_MEP_EVC) ? config.flow+1 : config.flow,
                              config.vid,
                              config.mac.addr[0],
                              config.mac.addr[1],
                              config.mac.addr[2],
                              config.mac.addr[3],
                              config.mac.addr[4],
                              config.mac.addr[5],
                              alarm ? "Down" : "Up");
                cyg_httpd_write_chunked(p->outbuffer, ct);
            }
        }
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s", mep_web_error_txt(error));
        cyg_httpd_write_chunked(p->outbuffer, ct);

        cyg_httpd_end_chunked();
    }

    return -1; // Do not further search the file system.
}

static i32 handler_config_mep(CYG_HTTPD_STATE* p)
{
    vtss_isid_t                  sid = web_retrieve_request_sid(p); /* Includes USID = ISID */
    u32                          i, j, cnt, res_port;
    char                         buf[200];
    char                         *bufp;
    int                          os_tlv_uoi[VTSS_APPL_MEP_OUI_SIZE];
    int                          mep_id, peer, level, mep, cc_prio, cc_rate, aps_prio, aps_cast, aps_type, vid=0, aps_octet, format, evcpag=0, evcqos=0, os_tlv_sub, os_tlv_value;
    int                          ct;
    const char                   *megS, *nameS, *macS;
    size_t                       len, n_len, m_len;
    mesa_rc                      error = VTSS_RC_OK;
    mep_conf_t                   config;
    vtss_appl_mep_lst_conf_t     lst_conf;
    vtss_appl_mep_syslog_conf_t  syslog_conf;
    vtss_appl_mep_cc_conf_t      cc_conf;
    vtss_appl_mep_aps_conf_t     aps_conf;
    vtss_appl_mep_tlv_conf_t     tlv_conf;
    vtss_mep_mgmt_etree_conf_t   etree_conf;
    vtss_appl_mep_state_t        state;
    vtss_appl_mep_peer_state_t   peer_state;
    vtss_appl_mep_cc_state_t     cc_state;
    mep_eps_state_t              eps_state;
    u32                          max_peers = mep_caps.mesa_oam_peer_cnt;
    MepPeerArray<u16>            peer_mep;
    MepPeerArray<mesa_mac_t>     peer_mac;
    char                         meg[VTSS_APPL_MEP_MEG_CODE_LENGTH];
    char                         name[VTSS_APPL_MEP_MEG_CODE_LENGTH];
    char                         encoded_name[3 * VTSS_APPL_MEP_MEG_CODE_LENGTH];
    char                         data_string[50 * max_peers];
    char                         tmp_string[50 * max_peers];
    int                          lenb;
    mesa_rc                      rc;

    if(redirectUnmanagedOrInvalid(p, sid)) /* Redirect unmanaged/invalid access to handler */
        return -1;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP))
        return -1;
#endif

    //
    // Setting new configuration
    //
    memset(&config, 0, sizeof(config));
    memset(&lst_conf, 0, sizeof(lst_conf));
    memset(&cc_conf, 0, sizeof(cc_conf));
    memset(&syslog_conf, 0, sizeof(syslog_conf));
    memset(&aps_conf, 0, sizeof(aps_conf));
    memset(&state, 0, sizeof(state));
    memset(name, 0, VTSS_APPL_MEP_MEG_CODE_LENGTH);
    memset(meg, 0, VTSS_APPL_MEP_MEG_CODE_LENGTH);
    peer_mep.clear();
    peer_mac.clear();

    if(p->method == CYG_HTTPD_METHOD_POST) {
        if (cyg_httpd_form_varable_int(p, "inst_id_hidden", &mep_id)) {
            if ((rc = mep_instance_conf_get(mep_id-1, &config)) != VTSS_RC_OK)
                if (error == VTSS_RC_OK)     error = rc;

            if ((rc == VTSS_RC_OK) && config.enable) {   /* Created */
                /* Check for delete peer MEP */
                for (i=0, cnt=0; i<config.peer_count; ++i) {
                    sprintf(buf, "del_%u", config.peer_mep[i]);
                    if (!cyg_httpd_form_varable_find(p, buf)) {   /* NOT delete */
                        sprintf(buf, "peerMAC_%u", config.peer_mep[i]);
                        macS=cyg_httpd_form_varable_string(p, buf, &len);
                        if (len > 0) {   /* Peer MAC found */
                            peer_mep[cnt] = config.peer_mep[i];
                            for (j=0; j<6; ++j)     peer_mac[cnt].addr[j] = hexToInt(&macS[j*3]);
                            cnt++;
                        }
                    }
                }
                memcpy(config.peer_mep, peer_mep.data(), peer_mep.mem_size());
                memcpy(config.peer_mac, peer_mac.data(), peer_mac.mem_size());
                config.peer_count = cnt;

                /* Add peer MEP */
                if (cyg_httpd_form_varable_int(p, "new_peer", &peer)) {
                    macS=cyg_httpd_form_varable_string(p, "new_peerMAC", &len);
                    if (len > 0) {
                        config.peer_mep[config.peer_count] = peer;
                        for (i=0; i<6; ++i)     config.peer_mac[config.peer_count].addr[i] = hexToInt(&macS[i*3]);
                        config.peer_count++;
                    }
                }

                vid = config.vid;
                evcpag = config.evc_pag;
                evcqos = config.evc_qos;
                (void)cyg_httpd_form_varable_int(p, "evcpag", &evcpag);
                (void)cyg_httpd_form_varable_int(p, "evcqos", &evcqos);
                (void)cyg_httpd_form_varable_int(p, "vid", &vid);
                if ((cyg_httpd_form_varable_int(p, "level", &level)) &&
                    (cyg_httpd_form_varable_int(p, "format", &format))) {
                    if ((nameS=cyg_httpd_form_varable_string(p, "name", &n_len))) {    /* Get 'name' */
                        if (n_len && cgi_unescape(nameS, name, n_len, sizeof(name)))  // name needs to be cgi_unescaped (e.g. %20 -> ' ').
                            n_len = strlen(name);   /* Calculate new real length */
                        else
                            n_len = 0;
                    }

                    if ((n_len == 0) || ((n_len > 0) && (n_len <= (VTSS_APPL_MEP_MEG_CODE_LENGTH-1)))) {  /* If Maintenance name is given length must be correct */
                        if ((megS=cyg_httpd_form_varable_string(p, "meg", &m_len))) {  /* get 'meg' */
                            if (m_len && cgi_unescape(megS, meg, m_len, sizeof(meg)))  // meg needs to be cgi_unescaped (e.g. %20 -> ' ').
                                m_len = strlen(meg);   /* Calculate new real length */
                            else
                                m_len = 0;
                        }

                        if ((m_len > 0) && (m_len <= (VTSS_APPL_MEP_MEG_CODE_LENGTH-1))) {
                            if (cyg_httpd_form_varable_int(p, "mep", &mep)) {
                                config.enable = TRUE;
                                config.level = level;
                                config.vid = vid;
                                config.voe = (VTSS_APPL_MEP_SW_OAM_SUPPORT && VTSS_APPL_MEP_VOE_SUPPORTED) ? ((cyg_httpd_form_varable_find(p, "voe") != NULL) ? TRUE : FALSE) : config.voe;   /* Only try to set voe if it is changeable. */
                                config.format = (vtss_appl_mep_format_t)format;
                                memset(config.name, 0, VTSS_APPL_MEP_MEG_CODE_LENGTH);
                                memset(config.meg, 0, VTSS_APPL_MEP_MEG_CODE_LENGTH);
                                memcpy(config.name, name, n_len);
                                memcpy(config.meg, meg, m_len);
                                config.mep = mep;
                                config.evc_pag = evcpag;
                                config.evc_qos = evcqos;
                                if ((rc = mep_instance_conf_set(mep_id-1, &config)) != VTSS_RC_OK)
                                    if (error == VTSS_RC_OK)     error = rc;
                            }
                        }
                    }
                }

                syslog_conf.enable = FALSE;
                if (cyg_httpd_form_varable_find(p, "syslog")) {
                    syslog_conf.enable = TRUE;
                }
                if ((rc = vtss_appl_mep_syslog_conf_set(mep_id-1, &syslog_conf)) != VTSS_RC_OK)
                    if (error == VTSS_RC_OK)     error = rc;

                if ((rc = vtss_appl_mep_cc_conf_get(mep_id-1, &cc_conf)) != VTSS_RC_OK)
                    if (error == VTSS_RC_OK)     error = rc;

                if (rc == VTSS_RC_OK) {
                    if ((cyg_httpd_form_varable_int(p, "cc_prio", &cc_prio)) &&
                        (cyg_httpd_form_varable_int(p, "cc_rate", &cc_rate))) {
                        cc_conf.prio = cc_prio;
                        cc_conf.rate = (vtss_appl_mep_rate_t)cc_rate;
                        if (cyg_httpd_form_varable_find(p, "cc")) {
                            cc_conf.enable = TRUE;
                            cc_conf.tlv_enable = (cyg_httpd_form_varable_find(p, "cc_tlv") != NULL) ? TRUE : FALSE;
                        } else {
                            cc_conf.enable = FALSE;
                        }
                        if ((rc = vtss_appl_mep_cc_conf_set(mep_id-1,   &cc_conf)) != VTSS_RC_OK)
                            if (error == VTSS_RC_OK)     error = rc;
                    }
                }

                if (cyg_httpd_form_varable_find(p, "aps")) {
                    if ((cyg_httpd_form_varable_int(p, "aps_cast", &aps_cast)) &&
                        (cyg_httpd_form_varable_int(p, "aps_type", &aps_type)) &&
                        (cyg_httpd_form_varable_int(p, "aps_prio", &aps_prio)) &&
                        (cyg_httpd_form_varable_int(p, "aps_octet", &aps_octet))) {
                        aps_conf.enable = TRUE;
                        aps_conf.prio = aps_prio;
                        aps_conf.cast = (vtss_appl_mep_cast_t)aps_cast;
                        aps_conf.type = (vtss_appl_mep_aps_type_t)aps_type;
                        aps_conf.raps_octet = aps_octet;
                        if ((rc = vtss_appl_mep_aps_conf_set(mep_id-1,   &aps_conf)) != VTSS_RC_OK)
                            if (error == VTSS_RC_OK)     error = rc;
                    }
                } else {
                    aps_conf.enable = FALSE;
                    if ((rc = vtss_appl_mep_aps_conf_set(mep_id-1,   &aps_conf)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;
                }

                if ((cyg_httpd_form_varable_int(p, "os_tlv_oui1", &os_tlv_uoi[0])) &&
                    (cyg_httpd_form_varable_int(p, "os_tlv_oui2", &os_tlv_uoi[1])) &&
                    (cyg_httpd_form_varable_int(p, "os_tlv_oui3", &os_tlv_uoi[2])) &&
                    (cyg_httpd_form_varable_int(p, "os_tlv_sub", &os_tlv_sub)) &&
                    (cyg_httpd_form_varable_int(p, "os_tlv_value", &os_tlv_value))) {
                    tlv_conf.os_tlv.oui[0] = os_tlv_uoi[0];
                    tlv_conf.os_tlv.oui[1] = os_tlv_uoi[1];
                    tlv_conf.os_tlv.oui[2] = os_tlv_uoi[2];
                    tlv_conf.os_tlv.subtype = os_tlv_sub;
                    tlv_conf.os_tlv.value = os_tlv_value;
                    if ((rc = vtss_appl_mep_tlv_conf_set(&tlv_conf)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;
                }

                if (cyg_httpd_form_varable_find(p, "lst")) {
                    lst_conf.enable = TRUE;
                } else {
                    lst_conf.enable = FALSE;
                }
                if ((rc = vtss_appl_mep_lst_conf_set(mep_id-1,   &lst_conf)) != VTSS_RC_OK)
                    if (error == VTSS_RC_OK)     error = rc;
            }
        }
/*T_D("1 %lu", error);*/
        bufp = buf;
        bufp += sprintf(bufp, "/mep_config.htm?mep=%u", mep_id);
        if (error != VTSS_RC_OK) {
            bufp += sprintf(bufp, "&error=%s", mep_web_error_txt(error));
        }
        redirect(p, buf);
    }
    else {
        (void)cyg_httpd_start_chunked("html");

        mep_id = 1;
        if ((cyg_httpd_form_varable_int(p, "mep", &mep_id)) && (mep_id > 0))
            mep_id = mep_id - 1;

        rc = mep_instance_conf_get(mep_id, &config);
        if (rc == VTSS_APPL_MEP_RC_VOLATILE) rc = VTSS_RC_OK;
        res_port = config.port;

        if (rc != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_instance_status_get(mep_id, &state)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_syslog_conf_get(mep_id, &syslog_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_cc_conf_get(mep_id, &cc_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_aps_conf_get(mep_id, &aps_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_tlv_conf_get(&tlv_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_cc_status_get(mep_id, &cc_state)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = mep_status_eps_get(mep_id, &eps_state)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_lst_conf_get(mep_id, &lst_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_mep_mgmt_etree_conf_get(mep_id, &etree_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
/*T_D("4 %lu", error);*/

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|",  mep_id+1);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", 0);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", 3);
        } else {
            if (mep_caps.mesa_mep_serval) {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", 2);
            } else {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", 0);
            }
        }
        cyg_httpd_write_chunked(p->outbuffer, ct);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u|",
                      (uint)config.evc_pag,
                      (uint)config.evc_qos);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        data_string[0] = '\0';
        bufp = data_string;
        for (i=0; i<eps_state.count; ++i)      /* Compose the EPS instance list */
            bufp += sprintf(bufp,"%u%c",
                    eps_state.inst[i]+1,
                    '-');
        if (eps_state.count == 0) {/* NO EPS instance is related - return value '0' */
            data_string[0] = '0';
            data_string[1] = '\0';
        } else {/* Overwrite the last ',' in string */
            len = strlen(data_string);
            data_string[len-1] = '\0';
        }

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u/%u/%u/%u/%u/%u/%s/%02X-%02X-%02X-%02X-%02X-%02X/%u|",
                      (uint)config.domain,
                      (uint)config.mode,
                      (uint)config.direction,
                      (uint)etree_conf.e_tree,
                      iport2uport(res_port),
                      (config.domain == VTSS_APPL_MEP_PORT) ? 0 : (config.domain == VTSS_APPL_MEP_EVC) ? config.flow+1 : config.flow,
                      config.vid,
                      data_string,
                      config.mac.addr[0],
                      config.mac.addr[1],
                      config.mac.addr[2],
                      config.mac.addr[3],
                      config.mac.addr[4],
                      config.mac.addr[5],
                      state.oper_state);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        data_string[0] = '\0';
        bufp = data_string;
        bufp += sprintf(bufp,"%u/%u/",
                config.level,
                config.format);

        (void) cgi_escape((char*)config.name, encoded_name);
        bufp += sprintf(bufp,"%s/", encoded_name);

        (void) cgi_escape((char*)config.meg, encoded_name);
        bufp += sprintf(bufp, "%s/", encoded_name);

        sprintf(data_string,"%s%u/%u/%u/%u", data_string,
                config.mep,
                config.vid,
                config.voe,
                syslog_conf.enable);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s|", data_string);
        cyg_httpd_write_chunked(p->outbuffer, ct); //2


        data_string[0] = '\0';
        bufp = data_string;
        bufp += sprintf(bufp,"%u", config.peer_count);
        for (i=0; i<config.peer_count; ++i) {
            lenb = sprintf(tmp_string, "/%u/%02X-%02X-%02X-%02X-%02X-%02X",
                          config.peer_mep[i],
                          config.peer_mac[i].addr[0],
                          config.peer_mac[i].addr[1],
                          config.peer_mac[i].addr[2],
                          config.peer_mac[i].addr[3],
                          config.peer_mac[i].addr[4],
                          config.peer_mac[i].addr[5]);

            if ((lenb >= 0) && ((bufp - data_string) + lenb < sizeof(data_string) - 1)) {
                strcpy(bufp, tmp_string);
                bufp += lenb;
            }
        }

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s|", data_string);
        cyg_httpd_write_chunked(p->outbuffer, ct); //3

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u/%u/%u/%u/%u/%u/%u/%u|",
                      cc_conf.enable,
                      cc_conf.prio,
                      (uint)cc_conf.rate,
                      cc_conf.tlv_enable,
                      aps_conf.enable,
                      aps_conf.prio,
                      (uint)aps_conf.cast,
                      (uint)aps_conf.type,
                      aps_conf.raps_octet);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s|",
                      (state.cLevel) ? "Down" : "Up",
                      (state.cMeg) ? "Down" : "Up",
                      (state.cMep) ? "Down" : "Up",
                      (state.cAis) ? "Down" : "Up",
                      (state.cLck) ? "Down" : "Up",
                      (state.cLoop) ? "Down" : "Up",
                      (state.cConfig) ? "Down" : "Up",
                      (state.cDeg) ? "Down" : "Up",
                      (state.cSsf) ? "Down" : "Up",
                      (state.aBlk) ? "Down" : "Up",
                      (state.aTsd) ? "Down" : "Up",
                      (state.aTsf) ? "Down" : "Up");
        cyg_httpd_write_chunked(p->outbuffer, ct); //5

        data_string[0] = '\0';
        bufp = data_string;
        for (i=0; i<config.peer_count; ++i) {
            if ((rc = vtss_appl_mep_instance_peer_status_get(mep_id, config.peer_mep[i], &peer_state)) != VTSS_RC_OK)
                if (error == VTSS_RC_OK)     error = rc;
            lenb = sprintf(tmp_string, "%s/%s/%s/%s/",
                    (peer_state.cLoc) ? "Down" : "Up",
                    (peer_state.cRdi) ? "Down" : "Up",
                    (peer_state.cPeriod) ? "Down" : "Up",
                    (peer_state.cPrio) ? "Down" : "Up");

            if ((lenb >= 0) && ((bufp - data_string) + lenb < sizeof(data_string) - 1)) {
                strcpy(bufp, tmp_string);
                bufp += lenb;
            }
        }

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s|", data_string);
        cyg_httpd_write_chunked(p->outbuffer, ct); //6

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u/%u/%u/%u|",
                      tlv_conf.os_tlv.oui[0],
                      tlv_conf.os_tlv.oui[1],
                      tlv_conf.os_tlv.oui[2],
                      tlv_conf.os_tlv.subtype,
                      tlv_conf.os_tlv.value);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        data_string[0] = '\0';
        bufp = data_string;
        bufp += sprintf(bufp, "%u", config.peer_count);
        for (i=0; i<config.peer_count; ++i) {
            lenb = sprintf(tmp_string,"/%u/%u/%u/%u/%u/%u/%s/%u/%s/%u/%s",
                    config.peer_mep[i],
                    cc_state.os_tlv[i].oui[0],
                    cc_state.os_tlv[i].oui[1],
                    cc_state.os_tlv[i].oui[2],
                    cc_state.os_tlv[i].subtype,
                    cc_state.os_tlv[i].value,
                    (!cc_state.os_received[i]) ? "Down" : "Up",
                    cc_state.ps_tlv[i].value,
                    (!cc_state.ps_received[i]) ? "Down" : "Up",
                    cc_state.is_tlv[i].value,
                    (!cc_state.is_received[i]) ? "Down" : "Up");

            if ((lenb >= 0) && ((bufp - data_string) + lenb < sizeof(data_string) - 1)) {
                strcpy(bufp, tmp_string);
                bufp += lenb;
            }

        }
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s|", data_string);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|",
                      lst_conf.enable);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        if (!config.enable) {    /* If instance is not enabled specific error is given */
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s",  "Instance is not created");
        } else {
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s",  mep_web_error_txt(error));
        }

        cyg_httpd_write_chunked(p->outbuffer, ct);

        cyg_httpd_end_chunked();
    }
    return -1; // Do not further search the file system.
}











static i32 handler_config_fm_mep(CYG_HTTPD_STATE* p)
{
    vtss_isid_t                  sid = web_retrieve_request_sid(p); /* Includes USID = ISID */
    u32                          i, j;
    char                         buf[200];
    int                          mep_id, flow;
    int                          lt_prio, lt_peer, lt_ttl, ais_prio, lck_prio, rate, level, domain;
    int                          lb_prio, lb_dei, lb_cast, lb_peer, lb_tosend, lb_size, lb_interval;



    int                          tst_prio, tst_dei, tst_peer, tst_rate, tst_size, tst_pattern, tst_seq;
    int                          ct;
    const char                   *macS;
    size_t                       len;
    mesa_rc                      error = VTSS_RC_OK;
    mep_default_conf_t           def_conf;
    mep_conf_t                   config;
    vtss_appl_mep_lt_conf_t      lt_conf;
    vtss_appl_mep_lb_conf_t      lb_conf;
    vtss_appl_mep_ais_conf_t     ais_conf;
    vtss_appl_mep_lck_conf_t     lck_conf;
    vtss_appl_mep_tst_conf_t     tst_conf, tst_old_conf;
    mep_client_conf_t            client_conf;
    vtss_mep_mgmt_etree_conf_t   etree_conf;
    vtss_appl_mep_lt_state_t     lt_state;
    vtss_appl_mep_lb_state_t     lb_state;
    vtss_appl_mep_tst_state_t    tst_state;
    char                         data_string[300];
    char                         tmp_string[200];
    int                          lenb;
    char                         *bufp;
    mesa_rc                      rc;

    if(redirectUnmanagedOrInvalid(p, sid)) /* Redirect unmanaged/invalid access to handler */
        return -1;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP))
        return -1;
#endif

    //
    // Setting new configuration
    //
    memset(&def_conf, 0, sizeof(def_conf));
    memset(&config, 0, sizeof(config));
    memset(&lt_conf, 0, sizeof(lt_conf));
    memset(&lb_conf, 0, sizeof(lb_conf));
    memset(&ais_conf,0, sizeof(ais_conf));
    memset(&lck_conf,0, sizeof(lck_conf));
    memset(&tst_conf,0, sizeof(tst_conf));
    memset(&tst_old_conf,0, sizeof(tst_old_conf));
    memset(&lt_state, 0, sizeof(lt_state));
    memset(&lb_state, 0, sizeof(lb_state));
    memset(&client_conf, 0, sizeof(client_conf));
    memset(&etree_conf, 0, sizeof(etree_conf));

    ais_prio = 0;

    if(p->method == CYG_HTTPD_METHOD_POST) {
/*T_D("Post");*/
        mep_default_conf_get(&def_conf);
        if (cyg_httpd_form_varable_int(p, "inst_id_hidden", &mep_id)) {
            if ((rc = mep_instance_conf_get(mep_id-1, &config)) != VTSS_RC_OK)
                if (error == VTSS_RC_OK)     error = rc;

            if ((rc == VTSS_RC_OK) && config.enable) {   /* Created */
                if (cyg_httpd_form_varable_find(p, "lt")) {
                    if ((cyg_httpd_form_varable_int(p, "lt_peer", &lt_peer)) &&
                        (cyg_httpd_form_varable_int(p, "lt_prio", &lt_prio)) &&
                        (cyg_httpd_form_varable_int(p, "lt_ttl", &lt_ttl))) {
                        macS=cyg_httpd_form_varable_string(p, "lt_mac", &len);
                        if (lt_ttl == 0) {
                           lt_conf.enable = FALSE;
                           error =  VTSS_APPL_MEP_RC_TTL_ZERO;
                        }
                        else if (len > 0) {   /* Unicast MAC found */
                            lt_conf.enable = TRUE;
                            lt_conf.prio = lt_prio;
                            lt_conf.mep = lt_peer;
                            for (j=0; j<6; ++j)     lt_conf.mac.addr[j] = hexToInt(&macS[j*3]);
                            lt_conf.ttl = lt_ttl;
                            lt_conf.transactions = VTSS_APPL_MEP_TRANSACTION_MAX;
                            if ((rc = vtss_appl_mep_lt_conf_set(mep_id-1,   &lt_conf)) != VTSS_RC_OK)
                                if (error == VTSS_RC_OK)     error = rc;
                        }
                    }
                } else {
                    lt_conf.enable = FALSE;
                    if ((rc = vtss_appl_mep_lt_conf_set(mep_id-1,   &lt_conf)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;
                }

                if (cyg_httpd_form_varable_find(p, "lb")) {
                    lb_dei = (cyg_httpd_form_varable_find(p, "lb_dei") != NULL);
                    if ((cyg_httpd_form_varable_int(p, "lb_prio", &lb_prio)) &&
                        (cyg_httpd_form_varable_int(p, "lb_cast", &lb_cast)) &&
                        (cyg_httpd_form_varable_int(p, "lb_peer", &lb_peer)) &&



                        (cyg_httpd_form_varable_int(p, "lb_tosend", &lb_tosend)) &&
                        (cyg_httpd_form_varable_int(p, "lb_size", &lb_size)) &&
                        (cyg_httpd_form_varable_int(p, "lb_interval", &lb_interval))) {
                        macS=cyg_httpd_form_varable_string(p, "lb_mac", &len);
                        if (len > 0) {   /* Unicast MAC found */
                            lb_conf.enable = TRUE;
                            lb_conf.prio = lb_prio;
                            lb_conf.dei = lb_dei;
                            lb_conf.cast = (vtss_appl_mep_cast_t)lb_cast;
                            lb_conf.mep = lb_peer;
                            for (j=0; j<6; ++j)     lb_conf.mac.addr[j] = hexToInt(&macS[j*3]);



                            lb_conf.to_send = lb_tosend;
                            lb_conf.size = lb_size;
                            lb_conf.interval = lb_interval;

                            if ((rc = vtss_appl_mep_lb_conf_set(mep_id-1,   &lb_conf)) != VTSS_RC_OK)
                                if (error == VTSS_RC_OK)     error = rc;
                        }
                    }
                } else {
                    if ((rc = vtss_appl_mep_lb_conf_get(mep_id-1,   &lb_conf)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;
                    lb_conf.enable = FALSE;
                    if ((rc = vtss_appl_mep_lb_conf_set(mep_id-1,   &lb_conf)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;
                }

                tst_dei = (cyg_httpd_form_varable_find(p, "tst_dei") != NULL);
                tst_seq = (cyg_httpd_form_varable_find(p, "tst_seq") != NULL);
                if ((cyg_httpd_form_varable_int(p, "tst_prio", &tst_prio)) &&
                    (cyg_httpd_form_varable_int(p, "tst_peer", &tst_peer)) &&
                    (cyg_httpd_form_varable_int(p, "tst_rate", &tst_rate)) &&
                    (cyg_httpd_form_varable_int(p, "tst_size", &tst_size)) &&
                    (cyg_httpd_form_varable_int(p, "tst_pattern", &tst_pattern))) {
                    if ((rc = vtss_appl_mep_tst_conf_get(mep_id-1,   &tst_conf)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;
                    tst_old_conf = tst_conf;

                    tst_conf.dei = tst_dei;
                    tst_conf.prio = tst_prio;
                    tst_conf.mep = tst_peer;
                    tst_conf.rate = tst_rate;
                    tst_conf.size = tst_size;
                    tst_conf.pattern = (vtss_appl_mep_pattern_t)tst_pattern;
                    tst_conf.sequence = tst_seq;
                    tst_conf.enable_rx = FALSE;
                    tst_conf.enable = FALSE;

                    if (cyg_httpd_form_varable_find(p, "tst_rx"))
                        tst_conf.enable_rx = TRUE;

                    if (cyg_httpd_form_varable_find(p, "tst_tx"))
                        tst_conf.enable = TRUE;

                    if (memcmp(&tst_conf, &tst_old_conf, sizeof(tst_old_conf))) {
                        if ((rc = vtss_appl_mep_tst_conf_set(mep_id-1,   &tst_conf)) != VTSS_RC_OK)
                            if (error == VTSS_RC_OK)     error = rc;
                    }
                }

                if (cyg_httpd_form_varable_find(p, "tst_clear"))
                    if ((rc = vtss_appl_mep_tst_status_clear(mep_id-1)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;

                // AIS configuration
                ais_conf = def_conf.ais_conf;   /* Initialize lck_conf to default */
                ais_conf.enable = (cyg_httpd_form_varable_find(p, "ais_en") != NULL);
                ais_conf.protection = (cyg_httpd_form_varable_find(p, "ais_prot") != NULL);
                if (cyg_httpd_form_varable_int(p, "ais_rate", &rate)) {
                    ais_conf.rate = (vtss_appl_mep_rate_t)rate;
                }

                // LOCK configuration
                lck_conf = def_conf.lck_conf;   /* Initialize lck_conf to default */
                lck_conf.enable = (cyg_httpd_form_varable_find(p, "lck_en") != NULL);
                if (cyg_httpd_form_varable_int(p, "lck_rate", &rate)) {
                    lck_conf.rate = (vtss_appl_mep_rate_t)rate;
                }

                // Set new configuration before client configuration when AIS or LOCK is disabled
                if (!ais_conf.enable && (rc = vtss_appl_mep_ais_conf_set(mep_id-1, &ais_conf)) != VTSS_RC_OK)
                    if (error == VTSS_RC_OK)     error = rc;
                if (!lck_conf.enable && (rc = vtss_appl_mep_lck_conf_set(mep_id-1, &lck_conf)) != VTSS_RC_OK)
                    if (error == VTSS_RC_OK)     error = rc;

                // Client configuration
                if ((config.direction == VTSS_APPL_MEP_DOWN) && mep_client_conf_get(mep_id-1, &client_conf) == VTSS_OK) {
                    for (i=0, j=0; i<VTSS_APPL_MEP_CLIENT_FLOWS_MAX; i++) {
                        sprintf(buf, "c_flow_domain%u", i);
                        if (cyg_httpd_form_varable_int(p, buf, &domain)) {
                            sprintf(buf, "c_flow%u", i);
                            if (cyg_httpd_form_varable_int(p, buf, &flow) && (flow > 0)) {
                                sprintf(buf, "c_level%u", i);
                                if (cyg_httpd_form_varable_int(p, buf, &level)) {
                                    sprintf(buf, "c_ais%u", i);
                                    if (cyg_httpd_form_varable_int(p, buf, &ais_prio)) {
                                        sprintf(buf, "c_lck%u", i);
                                        if (cyg_httpd_form_varable_int(p, buf, &lck_prio)) {
                                            client_conf.domain[j] = (mep_domain_t)domain;
                                            client_conf.flows[j] = flow;
                                            if (domain != VTSS_APPL_MEP_VLAN && domain != VTSS_APPL_MEP_MPLS_LSP) {
                                                client_conf.flows[j]--;
                                            }
                                            client_conf.level[j] = level;
                                            client_conf.ais_prio[j] = ais_prio;
                                            client_conf.lck_prio[j] = lck_prio;
                                            j++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    client_conf.flow_count = j;
                    if ((rc = mep_client_conf_set(mep_id-1, &client_conf)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;
                }

                // Set new configuration after client configuration when AIS or LOCK is enabled
                if (ais_conf.enable && (rc = vtss_appl_mep_ais_conf_set(mep_id-1, &ais_conf)) != VTSS_RC_OK)
                    if (error == VTSS_RC_OK)     error = rc;
                if (lck_conf.enable && (rc = vtss_appl_mep_lck_conf_set(mep_id-1, &lck_conf)) != VTSS_RC_OK)
                    if (error == VTSS_RC_OK)     error = rc;
            }
        }

        if (error != VTSS_RC_OK) {
            sprintf(buf, "mep_fm_config.htm?mep=%u", mep_id);
            redirect_errmsg(p, buf, mep_web_error_txt(error));
        } else {
            sprintf(buf, "/mep_fm_config.htm?mep=%u", mep_id);
            redirect(p, buf);
        }
    }
    else
    {
        (void)cyg_httpd_start_chunked("html");

        mep_id = 1;
        if (cyg_httpd_form_varable_int(p, "mep", &mep_id))
        if (mep_id > 0)
            mep_id = mep_id - 1;

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|",  mep_id+1);
        cyg_httpd_write_chunked(p->outbuffer, ct); // 0

        rc = mep_instance_conf_get(mep_id, &config);
        if (rc == VTSS_APPL_MEP_RC_VOLATILE) rc = VTSS_RC_OK;
        if (rc != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_lt_conf_get(mep_id, &lt_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_lb_conf_get(mep_id, &lb_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_ais_conf_get(mep_id, &ais_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_lck_conf_get(mep_id, &lck_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_tst_conf_get(mep_id, &tst_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = mep_client_conf_get(mep_id, &client_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_lt_status_get(mep_id, &lt_state)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_lb_status_get(mep_id, &lb_state)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_tst_status_get(mep_id, &tst_state)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_mep_mgmt_etree_conf_get(mep_id, &etree_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
/*T_D("4 %u", error);*/

        // Loopbak configuration
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u/%u/%02X-%02X-%02X-%02X-%02X-%02X/%u/%u/%u/%u/%u/%u/%02X-%02X-%02X-%02X-%02X-%02X/%u/%u/%u",
                      lt_conf.enable,
                      lt_conf.prio,
                      lt_conf.mep,
                      lt_conf.mac.addr[0],
                      lt_conf.mac.addr[1],
                      lt_conf.mac.addr[2],
                      lt_conf.mac.addr[3],
                      lt_conf.mac.addr[4],
                      lt_conf.mac.addr[5],
                      lt_conf.ttl,
                      lb_conf.enable,
                      lb_conf.dei,
                      lb_conf.prio,
                      (uint)lb_conf.cast,
                      lb_conf.mep,
                      lb_conf.mac.addr[0],
                      lb_conf.mac.addr[1],
                      lb_conf.mac.addr[2],
                      lb_conf.mac.addr[3],
                      lb_conf.mac.addr[4],
                      lb_conf.mac.addr[5],
                      lb_conf.to_send,
                      lb_conf.size,
                      lb_conf.interval);
        cyg_httpd_write_chunked(p->outbuffer, ct);








        (void)cyg_httpd_write_chunked("|", 1);

        // Loopback state
        data_string[0] = '\0';
        bufp = data_string;
        bufp += sprintf(bufp,"%u/%u/%" PRIu64, lb_state.reply_cnt, lb_state.transaction_id, lb_state.lbm_transmitted);

        for (i=0; i<lb_state.reply_cnt; ++i) {
            lenb = sprintf(tmp_string,"/%02X-%02X-%02X-%02X-%02X-%02X/%" PRIu64 "/%" PRIu64 "",
                    lb_state.reply[i].mac.addr[0],
                    lb_state.reply[i].mac.addr[1],
                    lb_state.reply[i].mac.addr[2],
                    lb_state.reply[i].mac.addr[3],
                    lb_state.reply[i].mac.addr[4],
                    lb_state.reply[i].mac.addr[5],
                    lb_state.reply[i].lbr_received,
                    lb_state.reply[i].out_of_order);

            if ((lenb >= 0) && ((bufp - data_string) + lenb < sizeof(data_string) - 1)) {
                strcpy(bufp, tmp_string);
                bufp += lenb;
            }
        }
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s|", data_string);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        data_string[0] = '\0';
        bufp = data_string;
        bufp += sprintf(bufp,"%u/", lt_state.transaction_cnt);
        for (i=0; i<lt_state.transaction_cnt; ++i) {
            lenb = sprintf(tmp_string,"%u/%u/",
                    lt_state.transaction[i].transaction_id,
                    lt_state.transaction[i].reply_cnt);

            if ((lenb >= 0) && ((bufp - data_string) + lenb < sizeof(data_string) - 1)) {
                strcpy(bufp, tmp_string);
                bufp += lenb;
            }

            for (j=0; j<lt_state.transaction[i].reply_cnt; ++j) {
                lenb = sprintf(tmp_string,"%u/%u/%u/%s/%u/%02X-%02X-%02X-%02X-%02X-%02X/%02X-%02X-%02X-%02X-%02X-%02X/",
                        lt_state.transaction[i].reply[j].ttl,
                        (uint)lt_state.transaction[i].reply[j].mode,
                        (uint)lt_state.transaction[i].reply[j].direction,
                        (lt_state.transaction[i].reply[j].forwarded) ? "Yes" : "No",
                        (uint)lt_state.transaction[i].reply[j].relay_action,
                        lt_state.transaction[i].reply[j].last_egress_mac.addr[0],
                        lt_state.transaction[i].reply[j].last_egress_mac.addr[1],
                        lt_state.transaction[i].reply[j].last_egress_mac.addr[2],
                        lt_state.transaction[i].reply[j].last_egress_mac.addr[3],
                        lt_state.transaction[i].reply[j].last_egress_mac.addr[4],
                        lt_state.transaction[i].reply[j].last_egress_mac.addr[5],
                        lt_state.transaction[i].reply[j].next_egress_mac.addr[0],
                        lt_state.transaction[i].reply[j].next_egress_mac.addr[1],
                        lt_state.transaction[i].reply[j].next_egress_mac.addr[2],
                        lt_state.transaction[i].reply[j].next_egress_mac.addr[3],
                        lt_state.transaction[i].reply[j].next_egress_mac.addr[4],
                        lt_state.transaction[i].reply[j].next_egress_mac.addr[5]);

                if ((lenb >= 0) && ((bufp - data_string) + lenb < sizeof(data_string) - 1)) {
                    strcpy(bufp, tmp_string);
                    bufp += lenb;
                }
            }
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s", data_string);
            cyg_httpd_write_chunked(p->outbuffer, ct);
            data_string[0] = '\0';
            bufp = data_string;
        }
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s|", data_string);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        // Client configuration
        data_string[0] = '\0';
        bufp = data_string;
        for (i=0; i<client_conf.flow_count; i++)
            bufp += sprintf(bufp, "%u/%u/%u/%u/%u/", client_conf.domain[i], ((client_conf.domain[i] == VTSS_APPL_MEP_VLAN || client_conf.domain[i] == VTSS_APPL_MEP_MPLS_LSP) ? client_conf.flows[i] : client_conf.flows[i]+1), client_conf.level[i], client_conf.ais_prio[i], client_conf.lck_prio[i]);
        for (i=0; i<(VTSS_APPL_MEP_CLIENT_FLOWS_MAX - client_conf.flow_count); i++)
            bufp += sprintf(bufp, "%u/%u/%u/%u/%u/", 0, 0, 0, 0, 0);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s|", data_string);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        // AIS configuration
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u/%u|",
                      (uint)ais_conf.enable,
                      (uint)ais_conf.rate,
                      (uint)ais_conf.protection);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        // Lock configuration
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u|",
                      (uint)lck_conf.enable,
                      (uint)lck_conf.rate);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u/%u/%u/%u/%u/%u/%u/%u|",
                      tst_conf.enable,
                      tst_conf.enable_rx,
                      tst_conf.dei,
                      tst_conf.prio,
                      tst_conf.mep,
                      tst_conf.rate,
                      tst_conf.size,
                      (uint)tst_conf.pattern,
                      tst_conf.sequence);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%" PRIu64 "/%" PRIu64 "/%u/%u|",
                      tst_state.tx_counter,
                      tst_state.rx_counter,
                      tst_state.rx_rate,
                      tst_state.time);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", 1);
        } else {
            if (mep_caps.mesa_mep_serval) {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", 1);
            } else {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", 0);
            }
        }
        cyg_httpd_write_chunked(p->outbuffer, ct);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", config.mep);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", etree_conf.e_tree);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s",  mep_web_error_txt(error));
        cyg_httpd_write_chunked(p->outbuffer, ct);

        cyg_httpd_end_chunked();
    }
    return -1; // Do not further search the file system.
}


static i32 handler_config_pm_mep(CYG_HTTPD_STATE* p)
{
    vtss_isid_t                     sid = web_retrieve_request_sid(p); /* Includes USID = ISID */
    mesa_rc                         rc;
    char                            buf[2000];
    char                            *bufp;
    int                             mep_id;
    int                             lm_prio, lm_cast, lm_size, lm_mep, lm_rate, lm_ended, lm_flr, lm_meas, lm_oam_cnt, lm_loss_th;
    u32                             slm_test_id;
    int                             dm_prio, dm_mep, dm_gap, dm_count, dm_tunit, dm_act;
    int                             dm_cast, dm_ended, dm_calcway;
    int                             dm_txway;
    int                             dm_fd, dm_ifdv, dm_threshold;
    int                             ct;
    mesa_rc                         error = VTSS_RC_OK;
    mep_conf_t                      config;
    vtss_appl_mep_pm_conf_t         pm_conf;
    vtss_appl_mep_lm_conf_t         lm_conf, old_lm_conf;
    vtss_appl_mep_lm_state_t        lm_state;
    vtss_appl_mep_lm_avail_conf_t   lm_avail_conf;
    vtss_appl_mep_lm_avail_state_t  lm_avail_status;
    vtss_appl_mep_lm_hli_conf_t     lm_hli_conf;
    vtss_appl_mep_lm_hli_state_t    lm_hli_status;
    vtss_appl_mep_lm_sdeg_conf_t    lm_sdeg_conf;
    vtss_appl_mep_dm_conf_t         dm_conf;
    vtss_appl_mep_dm_state_t        dmr_state, dm1_state_far_to_near, dm1_state_near_to_far;
    vtss_mep_mgmt_etree_conf_t      etree_conf;
    int                             i, dyna_val;

    if(redirectUnmanagedOrInvalid(p, sid)) /* Redirect unmanaged/invalid access to handler */
        return -1;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP))
        return -1;
#endif

    //
    // Setting new configuration
    //
    memset(&config, 0, sizeof(config));
    memset(&pm_conf, 0, sizeof(pm_conf));
    memset(&lm_conf, 0, sizeof(lm_conf));
    memset(&old_lm_conf, 0, sizeof(old_lm_conf));
    memset(&lm_state, 0, sizeof(lm_state));
    memset(&lm_avail_conf, 0, sizeof(lm_avail_conf));
    memset(&dm_conf, 0, sizeof(dm_conf));
    memset(&dmr_state, 0, sizeof(dmr_state));
    memset(&dm1_state_far_to_near, 0, sizeof(dm1_state_far_to_near));
    memset(&dm1_state_near_to_far, 0, sizeof(dm1_state_near_to_far));

    if(p->method == CYG_HTTPD_METHOD_POST) {
/*T_D("Post");*/
        if (cyg_httpd_form_varable_int(p, "inst_id_hidden", &mep_id)) {
            if ((rc = mep_instance_conf_get(mep_id-1, &config)) != VTSS_RC_OK)
                if (error == VTSS_RC_OK)     error = rc;

            if ((rc == VTSS_RC_OK) && config.enable) {   /* MEP is created */
                /*
                 * Handle Perf.Mon. data set enable
                 */
                if (cyg_httpd_form_varable_find(p, "pm_data")) {
                    pm_conf.enable = TRUE;
                } else {
                    pm_conf.enable = FALSE;
                }
                if ((rc = vtss_appl_mep_pm_conf_set(mep_id-1,   &pm_conf)) != VTSS_RC_OK)
                    if (error == VTSS_RC_OK)     error = rc;

                /*
                 * Handle new LM configuration values
                 */
                if ((rc = vtss_appl_mep_lm_conf_get(mep_id-1,   &lm_conf)) != VTSS_RC_OK)
                    if (error == VTSS_RC_OK)     error = rc;
                old_lm_conf = lm_conf;

                lm_conf.enable = (cyg_httpd_form_varable_find(p, "lm_tx") != NULL);
                lm_conf.enable_rx = (cyg_httpd_form_varable_find(p, "lm_rx") != NULL);
                lm_conf.synthetic = (cyg_httpd_form_varable_find(p, "lm_synthetic") != NULL);

                if (cyg_httpd_form_varable_int(p, "lm_rate", &lm_rate))
                    lm_conf.rate = (vtss_appl_mep_rate_t)lm_rate;
                if (cyg_httpd_form_varable_int(p, "lm_flr", &lm_flr))
                    lm_conf.flr_interval = lm_flr;
                if (cyg_httpd_form_varable_int(p, "lm_meas", &lm_meas))
                    lm_conf.meas_interval = lm_meas;
                if (cyg_httpd_form_varable_int(p, "lm_ended", &lm_ended))
                    lm_conf.ended = (vtss_appl_mep_ended_t)lm_ended;
                if (cyg_httpd_form_varable_int(p, "lm_cast", &lm_cast))
                    lm_conf.cast = (vtss_appl_mep_cast_t)lm_cast;
                if (cyg_httpd_form_varable_int(p, "lm_prio", &lm_prio))
                    lm_conf.prio = lm_prio;
                if (cyg_httpd_form_varable_int(p, "lm_peer", &lm_mep))
                    lm_conf.mep = lm_mep;
                if (cyg_httpd_form_varable_int(p, "lm_loss_th", &lm_loss_th))
                    lm_conf.loss_th = lm_loss_th;
                if (cyg_httpd_form_varable_int(p, "lm_size", &lm_size))
                    lm_conf.size = lm_size;
                if (cyg_httpd_form_variable_u32(p, "slm_test_id", &slm_test_id))
                    lm_conf.slm_test_id = slm_test_id;
                lm_conf.flow_count = (cyg_httpd_form_varable_find(p, "lm_flow_cnt") != NULL) ? TRUE : FALSE;
                if (cyg_httpd_form_varable_int(p, "lm_oam_cnt", &lm_oam_cnt))
                    lm_conf.oam_count = (vtss_appl_mep_oam_count)lm_oam_cnt;

                if (memcmp(&lm_conf, &old_lm_conf, sizeof(lm_conf))) {
                    if ((rc = vtss_appl_mep_lm_conf_set(mep_id-1,   &lm_conf)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;
                }

                /*
                 * Check for LM statistics clear command
                 */
                if (cyg_httpd_form_varable_find(p, "lm_clear"))
                    if ((rc = vtss_appl_mep_lm_status_clear(mep_id-1)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;

                if ((rc = vtss_appl_mep_dm_conf_get(mep_id-1, &dm_conf)) != VTSS_RC_OK)
                    if (error == VTSS_RC_OK)     error = rc;

                // Loss Measurement Availability
                if (vtss_appl_mep_lm_avail_conf_get(mep_id - 1, &lm_avail_conf) == VTSS_RC_OK) {
                    if (cyg_httpd_form_varable_find(p, "lm_avail_enable")) {
                        lm_avail_conf.enable = TRUE;
                    } else {
                        memset(&lm_avail_conf, 0, sizeof(lm_avail_conf));
                        lm_avail_conf.enable = FALSE;
                    }
                    if (lm_avail_conf.enable) {
                        if (cyg_httpd_form_varable_int(p, "lm_avail_flr_th", &dyna_val)) {
                            lm_avail_conf.flr_th = dyna_val;
                        }
                        if (cyg_httpd_form_varable_int(p, "lm_avail_interval", &dyna_val)) {
                            lm_avail_conf.interval = dyna_val;
                        }
                        if (cyg_httpd_form_varable_find(p, "lm_avail_main")) {
                            lm_avail_conf.main = TRUE;
                        } else {
                            lm_avail_conf.main = FALSE;
                        }
                    }

                    if ((rc = vtss_appl_mep_lm_avail_conf_set(mep_id - 1, &lm_avail_conf)) != VTSS_RC_OK) {
                        error = rc;
                    }
                }

                // Loss Measurement High Loss Interval
                if (vtss_appl_mep_lm_hli_conf_get(mep_id - 1, &lm_hli_conf) == VTSS_RC_OK) {
                    if (cyg_httpd_form_varable_find(p, "lm_hli_enable")) {
                        lm_hli_conf.enable = TRUE;
                    } else {
                        memset(&lm_hli_conf, 0, sizeof(lm_hli_conf));
                        lm_hli_conf.enable = FALSE;
                    }
                    if (lm_hli_conf.enable) {
                        if (cyg_httpd_form_varable_int(p, "lm_hli_flr_th", &dyna_val)) {
                            lm_hli_conf.flr_th = dyna_val;
                        }
                        if (cyg_httpd_form_varable_int(p, "lm_hli_interval", &dyna_val)) {
                            lm_hli_conf.con_int = dyna_val;
                        }
                    }

                    if ((rc = vtss_appl_mep_lm_hli_conf_set(mep_id - 1, &lm_hli_conf)) != VTSS_RC_OK) {
                        error = rc;
                    }
                }

                // Loss Measurement Signal Degrade
                if (vtss_appl_mep_lm_sdeg_conf_get(mep_id - 1, &lm_sdeg_conf) == VTSS_RC_OK) {
                    if (cyg_httpd_form_varable_find(p, "lm_sdeg_enable")) {
                        lm_sdeg_conf.enable = TRUE;
                    } else {
                        memset(&lm_sdeg_conf, 0, sizeof(lm_sdeg_conf));
                        lm_sdeg_conf.enable = FALSE;
                    }
                    if (lm_sdeg_conf.enable) {
                        if (cyg_httpd_form_varable_int(p, "lm_sdeg_tx_min", &dyna_val)) {
                            lm_sdeg_conf.tx_min = dyna_val;
                        }
                        if (cyg_httpd_form_varable_int(p, "lm_sdeg_flr_th", &dyna_val)) {
                            lm_sdeg_conf.flr_th = dyna_val;
                        }
                        if (cyg_httpd_form_varable_int(p, "lm_sdeg_bad_th", &dyna_val)) {
                            lm_sdeg_conf.bad_th = dyna_val;
                        }
                        if (cyg_httpd_form_varable_int(p, "lm_sdeg_good_th", &dyna_val)) {
                            lm_sdeg_conf.good_th = dyna_val;
                        }
                    }

                    if ((rc = vtss_appl_mep_lm_sdeg_conf_set(mep_id - 1, &lm_sdeg_conf)) != VTSS_RC_OK) {
                        error = rc;
                    }
                }

                BOOL dm_variable = TRUE;
                if (mep_caps.mesa_mep_prop_delay_measurement) {
                    dm_variable = cyg_httpd_form_varable_int(p, "dm_txway", &dm_txway);
                }
                
                if (cyg_httpd_form_varable_find(p, "dm")) {
                    if ((cyg_httpd_form_varable_int(p, "dm_prio", &dm_prio)) &&
                        (cyg_httpd_form_varable_int(p, "dm_cast", &dm_cast)) &&
                        (cyg_httpd_form_varable_int(p, "dm_ended", &dm_ended)) &&
                        dm_variable &&
                        (cyg_httpd_form_varable_int(p, "dm_calcway", &dm_calcway)) &&
                        (cyg_httpd_form_varable_int(p, "dm_gap", &dm_gap)) &&
                        (cyg_httpd_form_varable_int(p, "dm_count", &dm_count)) &&
                        (cyg_httpd_form_varable_int(p, "dm_tunit", &dm_tunit)) &&
                        (cyg_httpd_form_varable_int(p, "dm_act", &dm_act)) &&
                        (cyg_httpd_form_varable_int(p, "dm_num_fd", &dm_fd)) &&
                        (cyg_httpd_form_varable_int(p, "dm_num_ifdv", &dm_ifdv)) &&
                        (cyg_httpd_form_varable_int(p, "dm_m_threshold", &dm_threshold))) {
                        dm_conf.enable = TRUE;
                        dm_conf.prio = dm_prio;
                        dm_conf.cast = (vtss_appl_mep_cast_t)dm_cast;
                        if (dm_conf.cast == VTSS_APPL_MEP_UNICAST && cyg_httpd_form_varable_int(p, "dm_mep", &dm_mep)) {
                            dm_conf.mep = dm_mep;
                        }
                        dm_conf.ended = (vtss_appl_mep_ended_t)dm_ended;
                        dm_conf.proprietary = FALSE;
                        if (mep_caps.mesa_mep_prop_delay_measurement) {
                            dm_conf.proprietary = dm_txway;
                        }
                        dm_conf.calcway = (vtss_appl_mep_dm_calcway_t)dm_calcway;
                        dm_conf.interval = dm_gap;
                        dm_conf.lastn = dm_count;
                        dm_conf.tunit = (vtss_appl_mep_dm_tunit_t)dm_tunit;
                        dm_conf.synchronized = (cyg_httpd_form_varable_find(p, "dm_d2ford1") != NULL);
                        dm_conf.overflow_act = (vtss_appl_mep_dm_act_t)dm_act;
                        dm_conf.num_of_bin_fd = dm_fd;
                        dm_conf.num_of_bin_ifdv = dm_ifdv;
                        dm_conf.m_threshold = dm_threshold;


                        if ((rc = vtss_appl_mep_dm_conf_set(mep_id-1,   &dm_conf)) != VTSS_RC_OK)
                            if (error == VTSS_RC_OK)     error = rc;
                    }
                } else {
                    dm_conf.enable = FALSE;
                    if (cyg_httpd_form_varable_int(p, "dm_ended", &dm_ended))
                        dm_conf.ended = (vtss_appl_mep_ended_t)dm_ended;
                    if (cyg_httpd_form_varable_int(p, "dm_tunit", &dm_tunit))   /* This is only in order to change the time unit for 1DM reception - 1DM is not enabled */
                        dm_conf.tunit = (vtss_appl_mep_dm_tunit_t)dm_tunit;
                    if (cyg_httpd_form_varable_int(p, "dm_prio", (int *)&dm_prio))
                        dm_conf.prio = dm_prio;
                    if ((rc = vtss_appl_mep_dm_conf_set(mep_id-1, &dm_conf)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;
                }
                if (cyg_httpd_form_varable_find(p, "dm_clear"))
                    if ((rc = vtss_appl_mep_dm_status_clear(mep_id-1)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;
            }
        }
/*T_D("1 %u", error);*/
        bufp = buf;
        bufp += sprintf(bufp, "/mep_pm_config.htm?mep=%u", mep_id);
        if (error != VTSS_RC_OK)
            bufp += sprintf(bufp, "&error=%s", mep_web_error_txt(error));
        redirect(p, buf);
    } else {
        (void)cyg_httpd_start_chunked("html");

        mep_id = 1;
        if ((cyg_httpd_form_varable_int(p, "mep", &mep_id)) && (mep_id > 0))
            mep_id = mep_id - 1;

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|",  mep_id+1);
        cyg_httpd_write_chunked(p->outbuffer, ct); // 0

        rc = mep_instance_conf_get(mep_id, &config);
        if (rc == VTSS_APPL_MEP_RC_VOLATILE) rc = VTSS_RC_OK;
        if (rc != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_pm_conf_get(mep_id, &pm_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_lm_conf_get(mep_id, &lm_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_dm_conf_get(mep_id, &dm_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_lm_status_get(mep_id, &lm_state)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_appl_mep_dm_status_get(mep_id, &dmr_state, &dm1_state_far_to_near, &dm1_state_near_to_far)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc = vtss_mep_mgmt_etree_conf_get(mep_id, &etree_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;

        // pval 1
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                      "%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/"
                      "%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u|",
                      lm_conf.enable,
                      lm_conf.enable_rx,
                      lm_conf.prio,
                      (uint)lm_conf.cast,
                      lm_conf.mep,
                      (uint)lm_conf.rate,
                      lm_conf.size,
                      lm_conf.synthetic,
                      (uint)lm_conf.ended,
                      lm_conf.flr_interval,
                      lm_conf.meas_interval,
                      lm_conf.flow_count,
                      lm_conf.oam_count,
                      lm_conf.loss_th,
                      lm_conf.slm_test_id,

                      dm_conf.enable,
                      dm_conf.prio,
                      (uint)dm_conf.cast,
                      dm_conf.mep,
                      (uint)dm_conf.ended,
                      (uint)dm_conf.proprietary,
                      (uint)dm_conf.calcway,
                      dm_conf.interval,
                      dm_conf.lastn,
                      (uint)dm_conf.tunit,
                      dm_conf.synchronized,
                      (uint)dm_conf.overflow_act,
                      dm_conf.num_of_bin_fd,
                      dm_conf.num_of_bin_ifdv,
                      dm_conf.m_threshold);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        // pval 2
        buf[0] = '\0';
        sprintf(buf,"%u", config.peer_count);
        for (i = 0; i < config.peer_count; ++i) {
            if (vtss_appl_mep_instance_peer_lm_status_get(mep_id, config.peer_mep[i], &lm_state) != VTSS_RC_OK) continue;
            sprintf(buf,"%s/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u", buf,
                    config.peer_mep[i],
                    lm_state.tx_counter,
                    lm_state.rx_counter,
                    lm_state.near_los_int_cnt,
                    lm_state.near_los_tot_cnt,
                    lm_state.far_los_int_cnt,
                    lm_state.far_los_tot_cnt,
                    lm_state.near_los_th_cnt,
                    lm_state.far_los_th_cnt,
                    lm_state.near_flr_ave_int,
                    lm_state.near_flr_ave_total,
                    lm_state.far_flr_ave_int,
                    lm_state.far_flr_ave_total,
                    lm_state.near_flr_min,
                    lm_state.near_flr_max,
                    lm_state.far_flr_min,
                    lm_state.far_flr_max,
                    lm_state.flr_interval_elapsed);
        }
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s|", buf);
        cyg_httpd_write_chunked(p->outbuffer, ct);


        // Loss Measurement Availability
        memset(&lm_avail_conf, 0, sizeof(lm_avail_conf));
        memset(&lm_avail_status, 0, sizeof(lm_avail_status));
        if ((rc = vtss_appl_mep_lm_avail_conf_get(mep_id, &lm_avail_conf)) != VTSS_RC_OK) {
            if (error == VTSS_RC_OK)     error = rc;
        }

        // pval 3
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d/%u/%u/%d|",
                      lm_avail_conf.enable,
                      lm_avail_conf.enable ? lm_avail_conf.interval : 0,
                      lm_avail_conf.enable ? lm_avail_conf.flr_th : 0,
                      lm_avail_conf.enable ? lm_avail_conf.main : 0);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        // pval 4
        buf[0] = '\0';
        sprintf(buf,"%u", config.peer_count);

        for (i = 0; i < config.peer_count; ++i) {
            if (lm_avail_conf.enable && (rc = vtss_appl_mep_lm_avail_status_get(mep_id, config.peer_mep[i], &lm_avail_status)) != VTSS_RC_OK) {
                if (error == VTSS_RC_OK)     error = rc;
            }
            sprintf(buf, "%s/%u/%u/%u/%u/%u/%u/%u/%s/%s",
                    buf,
                    config.peer_mep[i],
                    lm_avail_conf.enable ? lm_avail_status.near_cnt : 0,
                    lm_avail_conf.enable ? lm_avail_status.far_cnt : 0,
                    lm_avail_conf.enable ? lm_avail_status.near_un_cnt : 0,
                    lm_avail_conf.enable ? lm_avail_status.far_un_cnt : 0,
                    lm_avail_conf.enable ? lm_avail_status.near_avail_cnt : 0,
                    lm_avail_conf.enable ? lm_avail_status.far_avail_cnt : 0,
                    lm_avail_conf.enable ? lm_avail_status.near_state == VTSS_APPL_MEP_OAM_LM_AVAIL ? "Available" : "Unavailable" : "-",
                    lm_avail_conf.enable ? lm_avail_status.far_state == VTSS_APPL_MEP_OAM_LM_AVAIL ? "Available" : "Unavailable" : "-");
        }

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s|", buf);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        // Loss Measurement High Loss Interval
        memset(&lm_hli_conf, 0, sizeof(lm_hli_conf));
        memset(&lm_sdeg_conf, 0, sizeof(lm_sdeg_conf));

        // pval 5
        if ((rc = vtss_appl_mep_lm_hli_conf_get(mep_id, &lm_hli_conf)) != VTSS_RC_OK) {
            if (error == VTSS_RC_OK)      error = rc;
        }
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d/%u/%u|",
                      lm_hli_conf.enable,
                      lm_hli_conf.enable ? lm_hli_conf.flr_th : 0,
                      lm_hli_conf.enable ? lm_hli_conf.con_int : 0);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        // pval 6
        buf[0] = '\0';
        sprintf(buf,"%u", config.peer_count);

        for (i = 0; i < config.peer_count; ++i) {
            memset(&lm_hli_status, 0, sizeof(lm_hli_status));

            if (lm_hli_conf.enable) {
                if ((rc = vtss_appl_mep_lm_hli_status_get(mep_id, config.peer_mep[i], &lm_hli_status)) != VTSS_RC_OK) {
                    if (error == VTSS_RC_OK)     error = rc;
                }
            }

            sprintf(buf, "%s/%u/%u/%u/%u/%u",
                    buf,
                    config.peer_mep[i],
                    lm_hli_conf.enable ? lm_hli_status.near_cnt : 0,
                    lm_hli_conf.enable ? lm_hli_status.far_cnt : 0,
                    lm_hli_conf.enable ? lm_hli_status.near_con_cnt : 0,
                    lm_hli_conf.enable ? lm_hli_status.far_con_cnt : 0);
        }

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s|", buf);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        // Loss Measurement Signal Degrade
        // pval 7
        if ((rc = vtss_appl_mep_lm_sdeg_conf_get(mep_id, &lm_sdeg_conf)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d/%u/%u/%u/%u|",
                      lm_sdeg_conf.enable,
                      lm_sdeg_conf.enable ? lm_sdeg_conf.tx_min : 0,
                      lm_sdeg_conf.enable ? lm_sdeg_conf.flr_th : 0,
                      lm_sdeg_conf.enable ? lm_sdeg_conf.bad_th : 0,
                      lm_sdeg_conf.enable ? lm_sdeg_conf.good_th : 0);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        // pval 8
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u|",
                     dm1_state_far_to_near.tx_cnt,
                     dm1_state_far_to_near.rx_cnt,
                     dm1_state_far_to_near.rx_tout_cnt,
                     dm1_state_far_to_near.rx_err_cnt,
                     dm1_state_far_to_near.avg_delay,
                     dm1_state_far_to_near.avg_n_delay,
                     dm1_state_far_to_near.min_delay,
                     dm1_state_far_to_near.max_delay,
                     dm1_state_far_to_near.avg_delay_var,
                     dm1_state_far_to_near.avg_n_delay_var,
                     dm1_state_far_to_near.min_delay_var,
                     dm1_state_far_to_near.max_delay_var,
                     dm1_state_far_to_near.ovrflw_cnt,
                     dmr_state.tx_cnt,
                     dmr_state.rx_cnt,
                     dmr_state.rx_tout_cnt,
                     dmr_state.rx_err_cnt,
                     dmr_state.avg_delay,
                     dmr_state.avg_n_delay,
                     dmr_state.min_delay,
                     dmr_state.max_delay,
                     dmr_state.avg_delay_var,
                     dmr_state.avg_n_delay_var,
                     dmr_state.min_delay_var,
                     dmr_state.max_delay_var,
                     dmr_state.ovrflw_cnt,
                     dm1_state_near_to_far.tx_cnt,
                     dm1_state_near_to_far.rx_cnt,
                     dm1_state_near_to_far.rx_tout_cnt,
                     dm1_state_near_to_far.rx_err_cnt,
                     dm1_state_near_to_far.avg_delay,
                     dm1_state_near_to_far.avg_n_delay,
                     dm1_state_near_to_far.min_delay,
                     dm1_state_near_to_far.max_delay,
                     dm1_state_near_to_far.avg_delay_var,
                     dm1_state_near_to_far.avg_n_delay_var,
                     dm1_state_near_to_far.min_delay_var,
                     dm1_state_near_to_far.max_delay_var,
                     dm1_state_near_to_far.ovrflw_cnt);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        // pval 9
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|",
                      pm_conf.enable);
        cyg_httpd_write_chunked(p->outbuffer, ct);


        // pval 10
        for (i = 0; i < dm_conf.num_of_bin_fd; ++i) {
            if ( i == (dm_conf.num_of_bin_fd - 1)) {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", dm1_state_far_to_near.fd_bin[i]);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            } else {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/", dm1_state_far_to_near.fd_bin[i]);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            }
        }

        // pval 11
        for (i = 0; i < dm_conf.num_of_bin_fd; ++i) {
            if ( i == (dm_conf.num_of_bin_fd - 1)) {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", dm1_state_near_to_far.fd_bin[i]);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            } else {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/", dm1_state_near_to_far.fd_bin[i]);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            }
        }

        // pval 12
        for (i = 0; i < dm_conf.num_of_bin_fd; ++i) {
            if ( i == (dm_conf.num_of_bin_fd - 1)) {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", dmr_state.fd_bin[i]);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            } else {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/", dmr_state.fd_bin[i]);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            }
        }

        // pval 13
        for (i = 0; i < dm_conf.num_of_bin_ifdv; ++i) {
            if ( i == (dm_conf.num_of_bin_ifdv - 1)) {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", dm1_state_far_to_near.ifdv_bin[i]);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            } else {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/", dm1_state_far_to_near.ifdv_bin[i]);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            }
        }

        // pval 14
        for (i = 0; i < dm_conf.num_of_bin_ifdv; ++i) {
            if ( i == (dm_conf.num_of_bin_ifdv - 1)) {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", dm1_state_near_to_far.ifdv_bin[i]);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            } else {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/", dm1_state_near_to_far.ifdv_bin[i]);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            }
        }

        // pval 15
        for (i = 0; i < dm_conf.num_of_bin_ifdv; ++i) {
            if ( i == (dm_conf.num_of_bin_ifdv - 1)) {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", dmr_state.ifdv_bin[i]);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            } else {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/", dmr_state.ifdv_bin[i]);
                cyg_httpd_write_chunked(p->outbuffer, ct);
            }
        }

        // pval 16
        if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", 2);
        } else {
            if (mep_caps.mesa_mep_serval) {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", 1);
            } else {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", 0);
            }
        }
        cyg_httpd_write_chunked(p->outbuffer, ct);

        // pval 17, mep id
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", config.mep);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        // pval 18, E-TREE info
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", etree_conf.e_tree);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        // pval 19, error msg
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s",  mep_web_error_txt(error));
        cyg_httpd_write_chunked(p->outbuffer, ct);

        cyg_httpd_end_chunked();
    }
    return -1; // Do not further search the file system.
}

// Status
static i32 handler_status_mep(CYG_HTTPD_STATE* p)
{
#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP))
        return -1;
#endif

    if(p->method == CYG_HTTPD_METHOD_GET) {
        (void)cyg_httpd_start_chunked("html");
        cyg_httpd_end_chunked();
    }
    return -1; // Do not further search the file system.
}

/****************************************************************************/
/*  Module JS lib config routine                                            */
/****************************************************************************/

#define MEP_WEB_BUF_LEN 512

static size_t mep_lib_config_js(char **base_ptr, char **cur_ptr, size_t *length)
{
    char buff[MEP_WEB_BUF_LEN];
    (void) snprintf(buff, sizeof(buff),
                    "var configMepIdMin = %d;\n"
                    "var configMepIdMax = %d;\n"
                    "var configMepPacketRxMtu = %d;\n"
                    "var configMepSwitchRxMtu = %d;\n",
                    1,
                    VTSS_APPL_MEP_INSTANCE_MAX,
                    mep_caps.appl_mep_packet_rx_mtu,
                    mep_caps.mesa_port_frame_length_max);
    return webCommonBufferHandler(base_ptr, cur_ptr, length, buff);
}

/****************************************************************************/
/*  Module Filter CSS routine                                               */
/****************************************************************************/
static size_t mep_lib_filter_css(char **base_ptr, char **cur_ptr, size_t *length)
{
    char buff[MEP_WEB_BUF_LEN];

    buff[MEP_WEB_BUF_LEN -1 ] = '\0';
    if (mep_caps.mesa_mep_prop_delay_measurement) {
        //Jaguar 2 and Serval-1 don't have proprietary tx mode
        const char *mep_tx_str = ".mep_dm_tx_mode { display: none; }\r\n";
        (void) strcpy(buff, mep_tx_str);
    }

    return webCommonBufferHandler(base_ptr, cur_ptr, length, buff);
}


/****************************************************************************/
/*  Filter CSS table entry                                                  */
/****************************************************************************/

web_lib_filter_css_tab_entry(mep_lib_filter_css);


/****************************************************************************/
/*  JS lib config table entry                                               */
/****************************************************************************/

web_lib_config_js_tab_entry(mep_lib_config_js);


/****************************************************************************/
/*  HTTPD Table Entries                                                     */
/****************************************************************************/

CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_config_mep_create, "/config/mepCreate", handler_config_mep_create);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_config_mep, "/config/mepConfig", handler_config_mep);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_fm_config_mep, "/config/mepFmConfig", handler_config_fm_mep);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_pm_config_mep, "/config/mepPmConfig", handler_config_pm_mep);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_status_mep, "/stat/mep_status", handler_status_mep);

/****************************************************************************/
/*  End of file.                                                            */
/****************************************************************************/

