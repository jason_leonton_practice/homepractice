/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "mep_serializer.hxx"
#include "vtss/basics/expose/table-status.hxx"

#include "mep_api.h"

vtss_enum_descriptor_t mep_mode_txt[] {
    {VTSS_APPL_MEP_MEP, "mep"},
    {VTSS_APPL_MEP_MIP, "mip"},
    {0, 0},
};

vtss_enum_descriptor_t mep_domain_txt[] {
    {VTSS_APPL_MEP_PORT,        "port"},
    {VTSS_APPL_MEP_EVC,         "evc"},
    {VTSS_APPL_MEP_VLAN,        "vlan"},
    {VTSS_APPL_MEP_MPLS_LINK,   "link"},
    {VTSS_APPL_MEP_MPLS_TUNNEL, "tunnel"},
    {VTSS_APPL_MEP_MPLS_PW,     "pw"},
    {VTSS_APPL_MEP_MPLS_LSP,    "lsp"},
    {0, 0},
};

vtss_enum_descriptor_t mep_format_txt[] {
    {VTSS_APPL_MEP_ITU_ICC,    "ituIcc"},
    {VTSS_APPL_MEP_IEEE_STR,   "ieeeString"},
    {VTSS_APPL_MEP_ITU_CC_ICC, "ituCcIcc"},
    {0, 0},
};

vtss_enum_descriptor_t mep_cast_txt[] {
    {VTSS_APPL_MEP_UNICAST, "uniCast"},
    {VTSS_APPL_MEP_MULTICAST, "multiCast"},
    {0, 0},
};

vtss_enum_descriptor_t mep_ended_txt[] {
    {VTSS_APPL_MEP_SINGEL_ENDED, "singleEnded"},
    {VTSS_APPL_MEP_DUAL_ENDED, "dualEnded"},
    {0, 0},
};

vtss_enum_descriptor_t mep_calcway_txt[] {
    {VTSS_APPL_MEP_RDTRP, "roundTrip"},
    {VTSS_APPL_MEP_FLOW, "flow"},
    {0, 0},
};

vtss_enum_descriptor_t mep_act_txt[] {
    {VTSS_APPL_MEP_DISABLE, "disableDm"},
    {VTSS_APPL_MEP_CONTINUE, "continueDm"},
    {0, 0},
};

vtss_enum_descriptor_t mep_relay_act_txt[] {
    {VTSS_APPL_MEP_RELAY_UNKNOWN, "relayUnknown"},
    {VTSS_APPL_MEP_RELAY_HIT, "relayHit"},
    {VTSS_APPL_MEP_RELAY_FDB, "relayFdb"},
    {VTSS_APPL_MEP_RELAY_MPDB, "relayMpdb"},
    {0, 0},
};

vtss_enum_descriptor_t mep_tst_pattern_txt[] {
    {VTSS_APPL_MEP_PATTERN_ALL_ZERO, "allZero"},
    {VTSS_APPL_MEP_PATTERN_ALL_ONE, "allOne"},
    {VTSS_APPL_MEP_PATTERN_0XAA, "hexAA"},
    {0, 0},
};

vtss_enum_descriptor_t mep_aps_type_txt[] {
    {VTSS_APPL_MEP_INV_APS, "invalidAps"},
    {VTSS_APPL_MEP_L_APS, "linearAps"},
    {VTSS_APPL_MEP_R_APS, "ringAps"},
    {0, 0},
};


vtss_enum_descriptor_t mep_bfd_session_state_type_txt[] {
    {VTSS_APPL_MEP_BFD_STATE_ADMIN_DOWN, "adminDown"},
    {VTSS_APPL_MEP_BFD_STATE_DOWN, "down"},
    {VTSS_APPL_MEP_BFD_STATE_INIT, "init"},
    {VTSS_APPL_MEP_BFD_STATE_UP, "up"},
    {0,0},
};

vtss_enum_descriptor_t mep_bfd_diag_type_txt[] {
    {VTSS_APPL_MEP_BFD_DIAG_NONE, "none"},
    {VTSS_APPL_MEP_BFD_DIAG_DETECT_EXPIRED, "detectExpired"},
    {VTSS_APPL_MEP_BFD_DIAG_ECHO_FAILED, "echoFailed"},
    {VTSS_APPL_MEP_BFD_DIAG_REMOTE_DOWN, "remoteDown"},
    {VTSS_APPL_MEP_BFD_DIAG_FWD_PLANE_RESET, "fwdPlaneReset"},
    {VTSS_APPL_MEP_BFD_DIAG_PATH_DOWN, "pathDown"},
    {VTSS_APPL_MEP_BFD_DIAG_CONCAT_PATH_DOWN, "concatPathDown"},
    {VTSS_APPL_MEP_BFD_DIAG_ADMIN_DOWN, "adminDown"},
    {VTSS_APPL_MEP_BFD_DIAG_REVERSE_CONCAT_PATH_DOWN, "reverseConcatDown"},
    {VTSS_APPL_MEP_BFD_DIAG_MISCONNECTIVITY, "misconnectivity"},
    {0,0},
};

vtss_enum_descriptor_t mep_bfd_auth_type_txt[] {
    {VTSS_APPL_MEP_BFD_AUTH_DISABLED, "disabled"},
    {VTSS_APPL_MEP_BFD_AUTH_SIMPLE_PWD, "simplePwd"},
    {VTSS_APPL_MEP_BFD_AUTH_KEYED_MD5, "keyedMD5"},
    {VTSS_APPL_MEP_BFD_AUTH_METICULOUS_KEYED_MD5, "meticulousKeyedMD5"},
    {VTSS_APPL_MEP_BFD_AUTH_KEYED_SHA1, "keyedSHA1"},
    {VTSS_APPL_MEP_BFD_AUTH_METICULOUS_KEYED_SHA1, "meticulousKeyedSHA1"},
    {0,0},
};

vtss_enum_descriptor_t mep_rt_pad_tlv_type_txt[] {
    {VTSS_APPL_MEP_RT_PAD_TLV_TYPE_NONE, "none"},
    {VTSS_APPL_MEP_RT_PAD_TLV_TYPE_DROP, "drop"},
    {VTSS_APPL_MEP_RT_PAD_TLV_TYPE_COPY, "copy"},
    {0,0},
};

vtss_enum_descriptor_t mep_rt_ret_code_type_txt[] {
    {VTSS_APPL_MEP_RT_RETCODE_NONE                ,"none"},
    {VTSS_APPL_MEP_RT_RETCODE_MALFORMED           ,"malformed"},
    {VTSS_APPL_MEP_RT_RETCODE_TLV_ERR             ,"tlvError"},
    {VTSS_APPL_MEP_RT_RETCODE_IS_EGRESS           ,"isEgress"},
    {VTSS_APPL_MEP_RT_RETCODE_NO_MAPPING          ,"noMapping"},
    {VTSS_APPL_MEP_RT_RETCODE_DOWN_MAP_MISMATCH   ,"downMapMismatch"},
    {VTSS_APPL_MEP_RT_RETCODE_UP_IF_UNKNOWN       ,"upIfUnknown"},
    {VTSS_APPL_MEP_RT_RETCODE_LABEL_SWITCH        ,"labelSwitch"},
    {VTSS_APPL_MEP_RT_RETCODE_LABEL_SWITCH_NO_FWD ,"labelSwitchNoFwd"},
    {VTSS_APPL_MEP_RT_RETCODE_MAPPING_WRONG       ,"mappingWrong"},
    {VTSS_APPL_MEP_RT_RETCODE_NO_LABEL            ,"noLabel"},
    {VTSS_APPL_MEP_RT_RETCODE_PROTOCOL_ERR        ,"protocolError"},
    {VTSS_APPL_MEP_RT_RETCODE_PREMATURE           ,"premature"},
    {0,0},
};

vtss_enum_descriptor_t mep_oam_count_txt[] {
    {VTSS_APPL_MEP_OAM_COUNT_Y1731, "countAsY1731"},
    {VTSS_APPL_MEP_OAM_COUNT_NONE, "countNone"},
    {VTSS_APPL_MEP_OAM_COUNT_ALL, "countAll"},
    {0, 0},
};

vtss_enum_descriptor_t mep_lm_avail_state_txt[] {
    {VTSS_APPL_MEP_OAM_LM_AVAIL,   "available"},
    {VTSS_APPL_MEP_OAM_LM_UNAVAIL, "unavailable"},
    {0,0},
};

vtss_enum_descriptor_t mep_oper_state_txt[] {
    {VTSS_APPL_MEP_OPER_UP,             "operStateUp"},
    {VTSS_APPL_MEP_OPER_DOWN,           "operStateDown"},
    {VTSS_APPL_MEP_OPER_INVALID_CONFIG, "operStateDownInvalidConf"},
    {VTSS_APPL_MEP_OPER_HW_OAM,         "operStateDownfailingOamHw"},
    {VTSS_APPL_MEP_OPER_MCE,            "operStateDownfailingMce"},
    {0,0},
};

vtss::expose::TableStatus<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_state_t *>
> mep_status;

mesa_rc mep_status_get_first(u32 *const instance,  vtss_appl_mep_state_t *status)
{
    return (mep_status.get_first(instance, status));
}

mesa_rc mep_status_get_next(u32 *const instance,  vtss_appl_mep_state_t *status)
{
    return (mep_status.get_next(instance, status));
}

mesa_rc mep_status_get(const u32 instance,  vtss_appl_mep_state_t  *const status)
{
    return (mep_status.get(instance, status));
}

mesa_rc mep_status_set(const u32 instance,  vtss_appl_mep_state_t *status)
{
    return mep_status.set(instance, status);
}

mesa_rc mep_status_del(const u32 instance)
{
    return (mep_status.del(instance));
}


vtss::expose::TableStatus<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lm_notif_state_t *>
> mep_status_lm_notif;

mesa_rc mep_status_lm_notif_get_first(u32 *const instance,  vtss_appl_mep_lm_notif_state_t *status)
{
    return (mep_status_lm_notif.get_first(instance, status));
}

mesa_rc mep_status_lm_notif_get_next(u32 *const instance,  vtss_appl_mep_lm_notif_state_t *status)
{
    return (mep_status_lm_notif.get_next(instance, status));
}

mesa_rc mep_status_lm_notif_get(const u32 instance,  vtss_appl_mep_lm_notif_state_t  *const status)
{
    return (mep_status_lm_notif.get(instance, status));
}

mesa_rc mep_status_lm_notif_set(const u32 instance,  vtss_appl_mep_lm_notif_state_t *status)
{
    return (mep_status_lm_notif.set(instance, status));
}

mesa_rc mep_status_lm_notif_del(const u32 instance)
{
    return (mep_status_lm_notif.del(instance));
}


vtss::expose::TableStatus<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lm_hli_state_t *>
> mep_status_lm_hli;

mesa_rc mep_status_lm_hli_get_first(u32 *const instance, u32 *const peer_id, vtss_appl_mep_lm_hli_state_t *status)
{
    return (mep_status_lm_hli.get_first(instance, peer_id, status));
}

mesa_rc mep_status_lm_hli_get_next(u32 *const instance, u32 *const peer_id, vtss_appl_mep_lm_hli_state_t *status)
{
    return (mep_status_lm_hli.get_next(instance, peer_id, status));
}

mesa_rc mep_status_lm_hli_get(const u32 instance, const u32 peer_id, vtss_appl_mep_lm_hli_state_t  *const status)
{
    return (mep_status_lm_hli.get(instance, peer_id, status));
}

mesa_rc mep_status_lm_hli_set(const u32 instance, const u32 peer_id, vtss_appl_mep_lm_hli_state_t *status)
{
    return (mep_status_lm_hli.set(instance, peer_id, status));
}

mesa_rc mep_status_lm_hli_del(const u32 instance, const u32 peer_id)
{
    return (mep_status_lm_hli.del(instance, peer_id));
}


vtss::expose::TableStatus<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_peer_state_t *>
> mep_status_peer;


mesa_rc mep_status_peer_get_first(u32 *const instance,  u32 *const peer,  vtss_appl_mep_peer_state_t *status)
{
    return (mep_status_peer.get_first(instance, peer, status));
}

mesa_rc mep_status_peer_get_next(u32 *const instance,  u32 *const peer,  vtss_appl_mep_peer_state_t *status)
{
    return (mep_status_peer.get_next(instance, peer, status));
}

mesa_rc mep_status_peer_get(const u32 instance,  const u32 peer,  vtss_appl_mep_peer_state_t  *const status)
{
    return (mep_status_peer.get(instance, peer, status));
}

mesa_rc mep_status_peer_set(const u32 instance,  const u32 peer,  vtss_appl_mep_peer_state_t *status)
{
    return (mep_status_peer.set(instance, peer, status));
}

mesa_rc mep_status_peer_del(const u32 instance,  const u32 peer)
{
    return (mep_status_peer.del(instance, peer));
}


mesa_rc vtss_appl_mep_instance_conf_default(u32 *k, vtss_appl_mep_conf_t *s) {
    *k = 0;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.config;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_instance_peer_conf_default(u32 *k1, u32 *k2, vtss_appl_mep_peer_conf_t *s) {
    *k1 = 0;
    *k2 = 0;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.peer_conf;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_cc_conf_default(u32 *k, vtss_appl_mep_cc_conf_t *s) {
    *k = 0;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.cc_conf;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_lm_conf_default(u32 *k, vtss_appl_mep_lm_conf_t *s) {
    *k = 0;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.lm_conf;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_lm_avail_conf_default(u32 *k, vtss_appl_mep_lm_avail_conf_t *s) {
    *k = 0;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.lm_avail_conf;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_lm_hli_conf_default(u32 *k, vtss_appl_mep_lm_hli_conf_t *s) {
    *k = 0;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.lm_hli_conf;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_lm_sdeg_conf_default(u32 *k, vtss_appl_mep_lm_sdeg_conf_t *s) {
    *k = 0;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.lm_sdeg_conf;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_dm_conf_default(u32 *k, vtss_appl_mep_dm_conf_t *s) {
    *k = 0;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.dm_conf;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_aps_conf_default(u32 *k, vtss_appl_mep_aps_conf_t *s) {
    *k = 0;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.aps_conf;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_lt_conf_default(u32 *k, vtss_appl_mep_lt_conf_t *s) {
    *k = 0;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.lt_conf;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_lb_conf_default(u32 *k, vtss_appl_mep_lb_conf_t *s) {
    *k = 0;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.lb_conf;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_ais_conf_default(u32 *k, vtss_appl_mep_ais_conf_t *s) {
    *k = 0;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.ais_conf;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_lck_conf_default(u32 *k, vtss_appl_mep_lck_conf_t *s) {
    *k = 0;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.lck_conf;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_tst_conf_default(u32 *k, vtss_appl_mep_tst_conf_t *s) {
    *k = 0;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.tst_conf;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_client_flow_conf_default(u32 *k1, vtss_ifindex_t *k2, vtss_appl_mep_client_flow_conf_t *s) {
    *k1 = 0;
    *k2 = VTSS_IFINDEX_NONE;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.client_flow_conf;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_g8113_2_bfd_conf_default(u32 *k, vtss_appl_mep_g8113_2_bfd_conf_t *s) {
    *k = 0;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.bfd_conf;
    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_default(u32 *k, vtss_appl_mep_g8113_2_bfd_auth_conf_t *s) {
    *k = 0;
    memset(s,0,sizeof(vtss_appl_mep_g8113_2_bfd_auth_conf_t));
    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_rt_conf_default(u32 *k, vtss_appl_mep_rt_conf_t *s) {
    *k = 0;
    vtss_appl_mep_default_conf_t  def_conf;

    vtss_appl_mep_default_conf_get(&def_conf);

    *s = def_conf.rt_conf;
    return VTSS_RC_OK;
}
