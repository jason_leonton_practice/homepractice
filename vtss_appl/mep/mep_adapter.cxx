/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#include "mscc/ethernet/switch/api.h"
#include <vtss/appl/mep.h>
#include "mep_api.h"

namespace mep_g1 {
    mesa_rc vtss_appl_mep_tlv_conf_set(const vtss_appl_mep_tlv_conf_t *const conf);
    mesa_rc vtss_appl_mep_tlv_conf_get(vtss_appl_mep_tlv_conf_t *const conf);
    void    vtss_appl_mep_default_conf_get(vtss_appl_mep_default_conf_t *const conf);
    mesa_rc vtss_appl_mep_capabilities_get(vtss_appl_mep_capabilities_t *const conf);
    mesa_rc vtss_appl_mep_instance_conf_set(const uint32_t instance, const vtss_appl_mep_conf_t *const conf);
    mesa_rc vtss_appl_mep_instance_conf_get(const uint32_t instance, vtss_appl_mep_conf_t *const conf);
    mesa_rc vtss_appl_mep_instance_conf_add(const uint32_t instance, const vtss_appl_mep_conf_t *const conf);
    mesa_rc vtss_appl_mep_instance_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_instance_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_instance_peer_conf_set(const uint32_t instance, const uint32_t peer_id, const vtss_appl_mep_peer_conf_t *const conf);
    mesa_rc vtss_appl_mep_instance_peer_conf_get(const uint32_t instance, const uint32_t peer_id, vtss_appl_mep_peer_conf_t *const conf);
    mesa_rc vtss_appl_mep_instance_peer_conf_add(const uint32_t instance, const uint32_t peer_id, const vtss_appl_mep_peer_conf_t *const conf);
    mesa_rc vtss_appl_mep_instance_peer_conf_delete(const uint32_t instance, const uint32_t peer_id);
    mesa_rc vtss_appl_mep_instance_peer_conf_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const peer_id, uint32_t *const next_peer_id);
    mesa_rc vtss_appl_mep_syslog_conf_set(const uint32_t instance, const vtss_appl_mep_syslog_conf_t *const conf);
    mesa_rc vtss_appl_mep_syslog_conf_get(const uint32_t instance, vtss_appl_mep_syslog_conf_t *const conf);
    mesa_rc vtss_appl_mep_syslog_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_instance_status_get(const uint32_t instance, vtss_appl_mep_state_t *const status);
    mesa_rc vtss_appl_mep_instance_status_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_instance_peer_status_get(const uint32_t instance, const uint32_t peer_id, vtss_appl_mep_peer_state_t *const status);
    mesa_rc vtss_appl_mep_instance_peer_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const peer_id, uint32_t *const next_peer_id);
    mesa_rc vtss_appl_mep_cc_status_get(const uint32_t instance, vtss_appl_mep_cc_state_t *const status);
    mesa_rc vtss_appl_mep_cc_peer_status_get(const uint32_t instance, const uint32_t peer_id, vtss_appl_mep_peer_cc_state_t *const status);
    mesa_rc vtss_appl_mep_pm_conf_set(const uint32_t instance, const vtss_appl_mep_pm_conf_t *const conf);
    mesa_rc vtss_appl_mep_pm_conf_get(const uint32_t instance, vtss_appl_mep_pm_conf_t *const conf);
    mesa_rc vtss_appl_mep_pm_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lst_conf_set(const uint32_t instance, const vtss_appl_mep_lst_conf_t *const conf);
    mesa_rc vtss_appl_mep_lst_conf_get(const uint32_t instance, vtss_appl_mep_lst_conf_t *const conf);
    mesa_rc vtss_appl_mep_lst_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_cc_conf_set(const uint32_t instance, const vtss_appl_mep_cc_conf_t *const conf);
    mesa_rc vtss_appl_mep_cc_conf_get(const uint32_t instance, vtss_appl_mep_cc_conf_t *const conf);
    mesa_rc vtss_appl_mep_cc_conf_add(const uint32_t instance, const vtss_appl_mep_cc_conf_t *const conf);
    mesa_rc vtss_appl_mep_cc_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_cc_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lm_conf_set(const uint32_t instance, const vtss_appl_mep_lm_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_conf_get(const uint32_t instance, vtss_appl_mep_lm_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_conf_add(const uint32_t instance, const vtss_appl_mep_lm_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_lm_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lm_status_get(const uint32_t instance, vtss_appl_mep_lm_state_t *const status);
    mesa_rc vtss_appl_mep_instance_peer_lm_status_get(const uint32_t instance, const uint32_t peer_id, vtss_appl_mep_lm_state_t *const status);
    mesa_rc vtss_appl_mep_lm_status_clear(const uint32_t instance);
    mesa_rc vtss_appl_mep_lm_status_clear_dir(const uint32_t instance, vtss_appl_mep_clear_dir direction);
    mesa_rc vtss_appl_mep_lm_control_get(const uint32_t instance, vtss_appl_mep_lm_control_t *const control);
    mesa_rc vtss_appl_mep_lm_control_set(const uint32_t instance, const vtss_appl_mep_lm_control_t *const control);
    mesa_rc vtss_appl_mep_lm_notif_status_get(const uint32_t instance, vtss_appl_mep_lm_notif_state_t *const status);
    mesa_rc vtss_appl_mep_lm_notif_status_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lm_avail_conf_set(const uint32_t instance, const vtss_appl_mep_lm_avail_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_avail_conf_get(const uint32_t instance, vtss_appl_mep_lm_avail_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_avail_conf_add(const uint32_t instance, const vtss_appl_mep_lm_avail_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_avail_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_lm_avail_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lm_avail_status_get(const uint32_t instance, const uint32_t peer_instance, vtss_appl_mep_lm_avail_state_t *const status);
    mesa_rc vtss_appl_mep_lm_hli_conf_set(const uint32_t instance, const vtss_appl_mep_lm_hli_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_hli_conf_get(const uint32_t instance, vtss_appl_mep_lm_hli_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_hli_conf_add(const uint32_t instance, const vtss_appl_mep_lm_hli_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_hli_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_lm_hli_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lm_hli_status_get(const uint32_t instance, const uint32_t peer_id, vtss_appl_mep_lm_hli_state_t *const status);
    mesa_rc vtss_appl_mep_lm_hli_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const peer, uint32_t *const next_peer);
    mesa_rc vtss_appl_mep_lm_sdeg_conf_set(const uint32_t instance, const vtss_appl_mep_lm_sdeg_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_sdeg_conf_get(const uint32_t instance, vtss_appl_mep_lm_sdeg_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_sdeg_conf_add(const uint32_t instance, const vtss_appl_mep_lm_sdeg_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_sdeg_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_lm_sdeg_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_dm_conf_set(const uint32_t instance, const vtss_appl_mep_dm_conf_t *const conf);
    mesa_rc vtss_appl_mep_dm_conf_get(const uint32_t instance, vtss_appl_mep_dm_conf_t *const conf);
    mesa_rc vtss_appl_mep_dm_conf_add(const uint32_t instance, const vtss_appl_mep_dm_conf_t *const conf);
    mesa_rc vtss_appl_mep_dm_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_dm_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_dm_status_get(const uint32_t instance, vtss_appl_mep_dm_state_t *const tw_status, vtss_appl_mep_dm_state_t *const ow_status_far_to_near, vtss_appl_mep_dm_state_t *const ow_status_near_to_far);
    mesa_rc vtss_appl_mep_dm_fd_bin_status_get(const uint32_t instance, const uint32_t index, vtss_appl_mep_dm_fd_bin_state_t *const status);
    mesa_rc vtss_appl_mep_dm_fd_bin_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const index, uint32_t *const next_index);
    mesa_rc vtss_appl_mep_dm_ifdv_bin_status_get(const uint32_t instance, const uint32_t index, vtss_appl_mep_dm_ifdv_bin_state_t *const status);
    mesa_rc vtss_appl_mep_dm_ifdv_bin_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const index, uint32_t *const next_index);
    mesa_rc vtss_appl_mep_dm_fd_bins_get(const uint32_t instance, const uint32_t bin_number, vtss_appl_mep_dm_fd_bin_state_t *const tw_bin, vtss_appl_mep_dm_bin_value_t *const bin_value);
    mesa_rc vtss_appl_mep_dm_fd_bins_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const bin_number, uint32_t *const next_bin_number);
    mesa_rc vtss_appl_mep_dm_ifdv_bins_get(const uint32_t instance, const uint32_t bin_number, vtss_appl_mep_dm_ifdv_bin_state_t *const tw_bin, vtss_appl_mep_dm_bin_value_t *const bin_value);
    mesa_rc vtss_appl_mep_dm_ifdv_bins_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const bin_number, uint32_t *const next_bin_number);
    mesa_rc vtss_appl_mep_dm_tw_status_get(const uint32_t instance, vtss_appl_mep_dm_state_t *const status);
    mesa_rc vtss_appl_mep_dm_ownf_status_get(const uint32_t instance, vtss_appl_mep_dm_state_t *const status);
    mesa_rc vtss_appl_mep_dm_owfn_status_get(const uint32_t instance, vtss_appl_mep_dm_state_t *const status);
    mesa_rc vtss_appl_mep_dm_status_clear(const uint32_t instance);
    mesa_rc vtss_appl_mep_dm_control_get(const uint32_t instance, vtss_appl_mep_dm_control_t *const control);
    mesa_rc vtss_appl_mep_dm_control_set(const uint32_t instance, const vtss_appl_mep_dm_control_t *const control);
    mesa_rc vtss_appl_mep_dm_common_conf_get(const uint32_t instance, vtss_appl_mep_dm_common_conf_t *const conf);
    mesa_rc vtss_appl_mep_dm_common_conf_set(const uint32_t instance, const vtss_appl_mep_dm_common_conf_t *const conf);
    mesa_rc vtss_appl_mep_dm_prio_conf_get(const uint32_t instance, const uint32_t prio, vtss_appl_mep_dm_prio_conf_t *const conf);
    mesa_rc vtss_appl_mep_dm_prio_conf_set(const uint32_t instance, const uint32_t prio, const vtss_appl_mep_dm_prio_conf_t *const conf);
    mesa_rc vtss_appl_mep_dm_prio_status_get(const uint32_t instance, const uint32_t prio, vtss_appl_mep_dm_state_t *const tw_status, vtss_appl_mep_dm_state_t *const ow_status_far_to_near, vtss_appl_mep_dm_state_t *const ow_status_near_to_far);
    mesa_rc vtss_appl_mep_aps_conf_set(const uint32_t instance, const vtss_appl_mep_aps_conf_t *const conf);
    mesa_rc vtss_appl_mep_aps_conf_get(const uint32_t instance, vtss_appl_mep_aps_conf_t *const conf);
    mesa_rc vtss_appl_mep_aps_conf_add(const uint32_t instance, const vtss_appl_mep_aps_conf_t *const conf);
    mesa_rc vtss_appl_mep_aps_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_aps_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lt_conf_set(const uint32_t instance, const vtss_appl_mep_lt_conf_t *const conf);
    mesa_rc vtss_appl_mep_lt_conf_get(const uint32_t instance, vtss_appl_mep_lt_conf_t *const conf);
    mesa_rc vtss_appl_mep_lt_conf_add(const uint32_t instance, const vtss_appl_mep_lt_conf_t *const conf);
    mesa_rc vtss_appl_mep_lt_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_lt_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lt_status_get(const uint32_t instance, vtss_appl_mep_lt_state_t *const status);
    mesa_rc vtss_appl_mep_lt_transaction_status_get(const uint32_t instance, vtss_appl_mep_lt_transaction_t *const status);
    mesa_rc vtss_appl_mep_lt_transaction_status_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lt_reply_status_get(const uint32_t instance, const uint32_t reply_id, vtss_appl_mep_lt_reply_t *const status);
    mesa_rc vtss_appl_mep_lt_reply_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const reply_id, uint32_t *const next_reply_id);
    mesa_rc vtss_appl_mep_lb_conf_set(const uint32_t instance, const vtss_appl_mep_lb_conf_t *const conf);
    mesa_rc vtss_appl_mep_lb_conf_get(const uint32_t instance, vtss_appl_mep_lb_conf_t *const conf);
    mesa_rc vtss_appl_mep_lb_conf_add(const uint32_t instance, const vtss_appl_mep_lb_conf_t *const conf);
    mesa_rc vtss_appl_mep_lb_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_lb_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lb_status_get(const uint32_t instance, vtss_appl_mep_lb_state_t *const status);
    mesa_rc vtss_appl_mep_lb_status_clear(const uint32_t instance);
    mesa_rc vtss_appl_mep_lb_reply_status_get(const uint32_t instance, const uint32_t reply_id, vtss_appl_mep_lb_reply_t *const status);
    mesa_rc vtss_appl_mep_lb_reply_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const reply_id, uint32_t *const next_reply_id);
    mesa_rc vtss_appl_mep_lb_common_conf_get(const uint32_t instance, vtss_appl_mep_lb_common_conf_t *const conf);
    mesa_rc vtss_appl_mep_lb_common_conf_set(const uint32_t instance, const vtss_appl_mep_lb_common_conf_t *const conf);
    mesa_rc vtss_appl_mep_lb_prio_conf_get(const uint32_t instance, const uint32_t prio, const uint32_t test_idx, vtss_appl_mep_test_prio_conf_t *const conf);
    mesa_rc vtss_appl_mep_lb_prio_conf_set(const uint32_t instance, const uint32_t prio, const uint32_t test_idx, const vtss_appl_mep_test_prio_conf_t *const conf);
    mesa_rc vtss_appl_mep_lb_prio_status_get(const uint32_t instance, const uint32_t prio, vtss_appl_mep_lb_state_t *const status);
    mesa_rc vtss_appl_mep_ais_conf_set(const uint32_t instance, const vtss_appl_mep_ais_conf_t *const conf);
    mesa_rc vtss_appl_mep_ais_conf_get(const uint32_t instance, vtss_appl_mep_ais_conf_t *const conf);
    mesa_rc vtss_appl_mep_ais_conf_add(const uint32_t instance, const vtss_appl_mep_ais_conf_t *const conf);
    mesa_rc vtss_appl_mep_ais_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_ais_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lck_conf_set(const uint32_t instance, const vtss_appl_mep_lck_conf_t *const conf);
    mesa_rc vtss_appl_mep_lck_conf_get(const uint32_t instance, vtss_appl_mep_lck_conf_t *const conf);
    mesa_rc vtss_appl_mep_lck_conf_add(const uint32_t instance, const vtss_appl_mep_lck_conf_t *const conf);
    mesa_rc vtss_appl_mep_lck_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_lck_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_tst_conf_set(const uint32_t instance, const vtss_appl_mep_tst_conf_t *const conf);
    mesa_rc vtss_appl_mep_tst_conf_get(const uint32_t instance, vtss_appl_mep_tst_conf_t *const conf);
    mesa_rc vtss_appl_mep_tst_conf_add(const uint32_t instance, const vtss_appl_mep_tst_conf_t *const conf);
    mesa_rc vtss_appl_mep_tst_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_tst_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_tst_status_get(const uint32_t instance, vtss_appl_mep_tst_state_t *const status);
    mesa_rc vtss_appl_mep_tst_status_clear(const uint32_t instance);
    mesa_rc vtss_appl_mep_tst_control_get(const uint32_t instance, vtss_appl_mep_tst_control_t *const control);
    mesa_rc vtss_appl_mep_tst_control_set(const uint32_t instance, const vtss_appl_mep_tst_control_t *const control);
    mesa_rc vtss_appl_mep_tst_common_conf_get(const uint32_t instance, vtss_appl_mep_tst_common_conf_t *const conf);
    mesa_rc vtss_appl_mep_tst_common_conf_set(const uint32_t instance, const vtss_appl_mep_tst_common_conf_t *const conf);
    mesa_rc vtss_appl_mep_tst_prio_conf_get(const uint32_t instance, const uint32_t prio, const uint32_t test_idx, vtss_appl_mep_test_prio_conf_t *const conf);
    mesa_rc vtss_appl_mep_tst_prio_conf_set(const uint32_t instance, const uint32_t prio, const uint32_t test_idx, const vtss_appl_mep_test_prio_conf_t *const conf);
    mesa_rc vtss_appl_mep_tst_prio_status_get(const uint32_t instance, const uint32_t prio, vtss_appl_mep_tst_state_t *const status);
    mesa_rc vtss_appl_mep_client_flow_conf_set(const uint32_t instance, const vtss_ifindex_t flow, const vtss_appl_mep_client_flow_conf_t *const conf);
    mesa_rc vtss_appl_mep_client_flow_conf_get(const uint32_t instance, const vtss_ifindex_t flow, vtss_appl_mep_client_flow_conf_t *const conf);
    mesa_rc vtss_appl_mep_client_flow_conf_add(const uint32_t instance, const vtss_ifindex_t flow, const vtss_appl_mep_client_flow_conf_t *const conf);
    mesa_rc vtss_appl_mep_client_flow_conf_delete(const uint32_t instance, const vtss_ifindex_t flow);
    mesa_rc vtss_appl_mep_client_flow_conf_iter(const uint32_t *const instance, uint32_t *const next_instance, const vtss_ifindex_t *const flow, vtss_ifindex_t *const next_flow);
    mesa_rc vtss_appl_mep_bfd_control_get(const uint32_t instance, vtss_appl_mep_bfd_control_t *const control);
    mesa_rc vtss_appl_mep_bfd_control_set(const uint32_t instance, const vtss_appl_mep_bfd_control_t *const control);
    mesa_rc vtss_appl_mep_g8113_2_bfd_status_get(const uint32_t instance, vtss_appl_mep_g8113_2_bfd_state_t *const status);
    mesa_rc vtss_appl_mep_g8113_2_bfd_conf_set(const uint32_t instance, const vtss_appl_mep_g8113_2_bfd_conf_t *const conf);
    mesa_rc vtss_appl_mep_g8113_2_bfd_conf_get(const uint32_t instance, vtss_appl_mep_g8113_2_bfd_conf_t *const conf);
    mesa_rc vtss_appl_mep_g8113_2_bfd_clear_stats(const uint32_t instance);
    mesa_rc vtss_appl_mep_g8113_2_bfd_conf_add(const uint32_t instance, const vtss_appl_mep_g8113_2_bfd_conf_t *const conf);
    mesa_rc vtss_appl_mep_g8113_2_bfd_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_g8113_2_bfd_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_set(const uint32_t key_id, const vtss_appl_mep_g8113_2_bfd_auth_conf_t *const conf);
    mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_get(const uint32_t key_id, vtss_appl_mep_g8113_2_bfd_auth_conf_t *const conf);
    mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_add(const uint32_t key_id, const vtss_appl_mep_g8113_2_bfd_auth_conf_t *const conf);
    mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_delete(const uint32_t key_id);
    mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_iter(const uint32_t *const key_id, uint32_t *const next_key_id);
    mesa_rc vtss_appl_mep_rt_conf_set(const uint32_t instance, const vtss_appl_mep_rt_conf_t *const conf);
    mesa_rc vtss_appl_mep_rt_conf_get(const uint32_t instance, vtss_appl_mep_rt_conf_t *const conf);
    mesa_rc vtss_appl_mep_rt_conf_add(const uint32_t instance, const vtss_appl_mep_rt_conf_t *const conf);
    mesa_rc vtss_appl_mep_rt_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_rt_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_rt_status_get(const uint32_t instance, vtss_appl_mep_rt_status_t *const status);
    mesa_rc vtss_appl_mep_rt_reply_status_get(const uint32_t instance, const uint32_t seq_no, vtss_appl_mep_rt_reply_t *const status);
    mesa_rc vtss_appl_mep_rt_reply_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const seq_no, uint32_t *const next_seq_no);
    u32     vtss_appl_mep_supp_rt_reply_status_iter(const u32 instance, const u32 *const seq_no, u32 *const next_seq_no);
}

namespace mep_g2 {
    mesa_rc vtss_appl_mep_tlv_conf_set(const vtss_appl_mep_tlv_conf_t *const conf);
    mesa_rc vtss_appl_mep_tlv_conf_get(vtss_appl_mep_tlv_conf_t *const conf);
    void    vtss_appl_mep_default_conf_get(vtss_appl_mep_default_conf_t *const conf);
    mesa_rc vtss_appl_mep_capabilities_get(vtss_appl_mep_capabilities_t *const conf);
    mesa_rc vtss_appl_mep_instance_conf_set(const uint32_t instance, const vtss_appl_mep_conf_t *const conf);
    mesa_rc vtss_appl_mep_instance_conf_get(const uint32_t instance, vtss_appl_mep_conf_t *const conf);
    mesa_rc vtss_appl_mep_instance_conf_add(const uint32_t instance, const vtss_appl_mep_conf_t *const conf);
    mesa_rc vtss_appl_mep_instance_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_instance_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_instance_peer_conf_set(const uint32_t instance, const uint32_t peer_id, const vtss_appl_mep_peer_conf_t *const conf);
    mesa_rc vtss_appl_mep_instance_peer_conf_get(const uint32_t instance, const uint32_t peer_id, vtss_appl_mep_peer_conf_t *const conf);
    mesa_rc vtss_appl_mep_instance_peer_conf_add(const uint32_t instance, const uint32_t peer_id, const vtss_appl_mep_peer_conf_t *const conf);
    mesa_rc vtss_appl_mep_instance_peer_conf_delete(const uint32_t instance, const uint32_t peer_id);
    mesa_rc vtss_appl_mep_instance_peer_conf_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const peer_id, uint32_t *const next_peer_id);
    mesa_rc vtss_appl_mep_syslog_conf_set(const uint32_t instance, const vtss_appl_mep_syslog_conf_t *const conf);
    mesa_rc vtss_appl_mep_syslog_conf_get(const uint32_t instance, vtss_appl_mep_syslog_conf_t *const conf);
    mesa_rc vtss_appl_mep_syslog_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_instance_status_get(const uint32_t instance, vtss_appl_mep_state_t *const status);
    mesa_rc vtss_appl_mep_instance_status_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_instance_peer_status_get(const uint32_t instance, const uint32_t peer_id, vtss_appl_mep_peer_state_t *const status);
    mesa_rc vtss_appl_mep_instance_peer_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const peer_id, uint32_t *const next_peer_id);
    mesa_rc vtss_appl_mep_cc_status_get(const uint32_t instance, vtss_appl_mep_cc_state_t *const status);
    mesa_rc vtss_appl_mep_cc_peer_status_get(const uint32_t instance, const uint32_t peer_id, vtss_appl_mep_peer_cc_state_t *const status);
    mesa_rc vtss_appl_mep_pm_conf_set(const uint32_t instance, const vtss_appl_mep_pm_conf_t *const conf);
    mesa_rc vtss_appl_mep_pm_conf_get(const uint32_t instance, vtss_appl_mep_pm_conf_t *const conf);
    mesa_rc vtss_appl_mep_pm_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lst_conf_set(const uint32_t instance, const vtss_appl_mep_lst_conf_t *const conf);
    mesa_rc vtss_appl_mep_lst_conf_get(const uint32_t instance, vtss_appl_mep_lst_conf_t *const conf);
    mesa_rc vtss_appl_mep_lst_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_cc_conf_set(const uint32_t instance, const vtss_appl_mep_cc_conf_t *const conf);
    mesa_rc vtss_appl_mep_cc_conf_get(const uint32_t instance, vtss_appl_mep_cc_conf_t *const conf);
    mesa_rc vtss_appl_mep_cc_conf_add(const uint32_t instance, const vtss_appl_mep_cc_conf_t *const conf);
    mesa_rc vtss_appl_mep_cc_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_cc_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lm_conf_set(const uint32_t instance, const vtss_appl_mep_lm_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_conf_get(const uint32_t instance, vtss_appl_mep_lm_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_conf_add(const uint32_t instance, const vtss_appl_mep_lm_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_lm_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lm_status_get(const uint32_t instance, vtss_appl_mep_lm_state_t *const status);
    mesa_rc vtss_appl_mep_instance_peer_lm_status_get(const uint32_t instance, const uint32_t peer_id, vtss_appl_mep_lm_state_t *const status);
    mesa_rc vtss_appl_mep_lm_status_clear(const uint32_t instance);
    mesa_rc vtss_appl_mep_lm_status_clear_dir(const uint32_t instance, vtss_appl_mep_clear_dir direction);
    mesa_rc vtss_appl_mep_lm_control_get(const uint32_t instance, vtss_appl_mep_lm_control_t *const control);
    mesa_rc vtss_appl_mep_lm_control_set(const uint32_t instance, const vtss_appl_mep_lm_control_t *const control);
    mesa_rc vtss_appl_mep_lm_notif_status_get(const uint32_t instance, vtss_appl_mep_lm_notif_state_t *const status);
    mesa_rc vtss_appl_mep_lm_notif_status_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lm_avail_conf_set(const uint32_t instance, const vtss_appl_mep_lm_avail_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_avail_conf_get(const uint32_t instance, vtss_appl_mep_lm_avail_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_avail_conf_add(const uint32_t instance, const vtss_appl_mep_lm_avail_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_avail_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_lm_avail_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lm_avail_status_get(const uint32_t instance, const uint32_t peer_instance, vtss_appl_mep_lm_avail_state_t *const status);
    mesa_rc vtss_appl_mep_lm_hli_conf_set(const uint32_t instance, const vtss_appl_mep_lm_hli_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_hli_conf_get(const uint32_t instance, vtss_appl_mep_lm_hli_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_hli_conf_add(const uint32_t instance, const vtss_appl_mep_lm_hli_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_hli_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_lm_hli_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lm_hli_status_get(const uint32_t instance, const uint32_t peer_id, vtss_appl_mep_lm_hli_state_t *const status);
    mesa_rc vtss_appl_mep_lm_hli_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const peer, uint32_t *const next_peer);
    mesa_rc vtss_appl_mep_lm_sdeg_conf_set(const uint32_t instance, const vtss_appl_mep_lm_sdeg_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_sdeg_conf_get(const uint32_t instance, vtss_appl_mep_lm_sdeg_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_sdeg_conf_add(const uint32_t instance, const vtss_appl_mep_lm_sdeg_conf_t *const conf);
    mesa_rc vtss_appl_mep_lm_sdeg_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_lm_sdeg_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_dm_conf_set(const uint32_t instance, const vtss_appl_mep_dm_conf_t *const conf);
    mesa_rc vtss_appl_mep_dm_conf_get(const uint32_t instance, vtss_appl_mep_dm_conf_t *const conf);
    mesa_rc vtss_appl_mep_dm_conf_add(const uint32_t instance, const vtss_appl_mep_dm_conf_t *const conf);
    mesa_rc vtss_appl_mep_dm_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_dm_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_dm_status_get(const uint32_t instance, vtss_appl_mep_dm_state_t *const tw_status, vtss_appl_mep_dm_state_t *const ow_status_far_to_near, vtss_appl_mep_dm_state_t *const ow_status_near_to_far);
    mesa_rc vtss_appl_mep_dm_fd_bin_status_get(const uint32_t instance, const uint32_t index, vtss_appl_mep_dm_fd_bin_state_t *const status);
    mesa_rc vtss_appl_mep_dm_fd_bin_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const index, uint32_t *const next_index);
    mesa_rc vtss_appl_mep_dm_ifdv_bin_status_get(const uint32_t instance, const uint32_t index, vtss_appl_mep_dm_ifdv_bin_state_t *const status);
    mesa_rc vtss_appl_mep_dm_ifdv_bin_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const index, uint32_t *const next_index);
    mesa_rc vtss_appl_mep_dm_fd_bins_get(const uint32_t instance, const uint32_t bin_number, vtss_appl_mep_dm_fd_bin_state_t *const tw_bin, vtss_appl_mep_dm_bin_value_t *const bin_value);
    mesa_rc vtss_appl_mep_dm_fd_bins_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const bin_number, uint32_t *const next_bin_number);
    mesa_rc vtss_appl_mep_dm_ifdv_bins_get(const uint32_t instance, const uint32_t bin_number, vtss_appl_mep_dm_ifdv_bin_state_t *const tw_bin, vtss_appl_mep_dm_bin_value_t *const bin_value);
    mesa_rc vtss_appl_mep_dm_ifdv_bins_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const bin_number, uint32_t *const next_bin_number);
    mesa_rc vtss_appl_mep_dm_tw_status_get(const uint32_t instance, vtss_appl_mep_dm_state_t *const status);
    mesa_rc vtss_appl_mep_dm_ownf_status_get(const uint32_t instance, vtss_appl_mep_dm_state_t *const status);
    mesa_rc vtss_appl_mep_dm_owfn_status_get(const uint32_t instance, vtss_appl_mep_dm_state_t *const status);
    mesa_rc vtss_appl_mep_dm_status_clear(const uint32_t instance);
    mesa_rc vtss_appl_mep_dm_control_get(const uint32_t instance, vtss_appl_mep_dm_control_t *const control);
    mesa_rc vtss_appl_mep_dm_control_set(const uint32_t instance, const vtss_appl_mep_dm_control_t *const control);
    mesa_rc vtss_appl_mep_dm_common_conf_get(const uint32_t instance, vtss_appl_mep_dm_common_conf_t *const conf);
    mesa_rc vtss_appl_mep_dm_common_conf_set(const uint32_t instance, const vtss_appl_mep_dm_common_conf_t *const conf);
    mesa_rc vtss_appl_mep_dm_prio_conf_get(const uint32_t instance, const uint32_t prio, vtss_appl_mep_dm_prio_conf_t *const conf);
    mesa_rc vtss_appl_mep_dm_prio_conf_set(const uint32_t instance, const uint32_t prio, const vtss_appl_mep_dm_prio_conf_t *const conf);
    mesa_rc vtss_appl_mep_dm_prio_status_get(const uint32_t instance, const uint32_t prio, vtss_appl_mep_dm_state_t *const tw_status, vtss_appl_mep_dm_state_t *const ow_status_far_to_near, vtss_appl_mep_dm_state_t *const ow_status_near_to_far);
    mesa_rc vtss_appl_mep_aps_conf_set(const uint32_t instance, const vtss_appl_mep_aps_conf_t *const conf);
    mesa_rc vtss_appl_mep_aps_conf_get(const uint32_t instance, vtss_appl_mep_aps_conf_t *const conf);
    mesa_rc vtss_appl_mep_aps_conf_add(const uint32_t instance, const vtss_appl_mep_aps_conf_t *const conf);
    mesa_rc vtss_appl_mep_aps_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_aps_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lt_conf_set(const uint32_t instance, const vtss_appl_mep_lt_conf_t *const conf);
    mesa_rc vtss_appl_mep_lt_conf_get(const uint32_t instance, vtss_appl_mep_lt_conf_t *const conf);
    mesa_rc vtss_appl_mep_lt_conf_add(const uint32_t instance, const vtss_appl_mep_lt_conf_t *const conf);
    mesa_rc vtss_appl_mep_lt_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_lt_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lt_status_get(const uint32_t instance, vtss_appl_mep_lt_state_t *const status);
    mesa_rc vtss_appl_mep_lt_transaction_status_get(const uint32_t instance, vtss_appl_mep_lt_transaction_t *const status);
    mesa_rc vtss_appl_mep_lt_transaction_status_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lt_reply_status_get(const uint32_t instance, const uint32_t reply_id, vtss_appl_mep_lt_reply_t *const status);
    mesa_rc vtss_appl_mep_lt_reply_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const reply_id, uint32_t *const next_reply_id);
    mesa_rc vtss_appl_mep_lb_conf_set(const uint32_t instance, const vtss_appl_mep_lb_conf_t *const conf);
    mesa_rc vtss_appl_mep_lb_conf_get(const uint32_t instance, vtss_appl_mep_lb_conf_t *const conf);
    mesa_rc vtss_appl_mep_lb_conf_add(const uint32_t instance, const vtss_appl_mep_lb_conf_t *const conf);
    mesa_rc vtss_appl_mep_lb_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_lb_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lb_status_get(const uint32_t instance, vtss_appl_mep_lb_state_t *const status);
    mesa_rc vtss_appl_mep_lb_status_clear(const uint32_t instance);
    mesa_rc vtss_appl_mep_lb_reply_status_get(const uint32_t instance, const uint32_t reply_id, vtss_appl_mep_lb_reply_t *const status);
    mesa_rc vtss_appl_mep_lb_reply_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const reply_id, uint32_t *const next_reply_id);
    mesa_rc vtss_appl_mep_lb_common_conf_get(const uint32_t instance, vtss_appl_mep_lb_common_conf_t *const conf);
    mesa_rc vtss_appl_mep_lb_common_conf_set(const uint32_t instance, const vtss_appl_mep_lb_common_conf_t *const conf);
    mesa_rc vtss_appl_mep_lb_prio_conf_get(const uint32_t instance, const uint32_t prio, const uint32_t test_idx, vtss_appl_mep_test_prio_conf_t *const conf);
    mesa_rc vtss_appl_mep_lb_prio_conf_set(const uint32_t instance, const uint32_t prio, const uint32_t test_idx, const vtss_appl_mep_test_prio_conf_t *const conf);
    mesa_rc vtss_appl_mep_lb_prio_status_get(const uint32_t instance, const uint32_t prio, vtss_appl_mep_lb_state_t *const status);
    mesa_rc vtss_appl_mep_ais_conf_set(const uint32_t instance, const vtss_appl_mep_ais_conf_t *const conf);
    mesa_rc vtss_appl_mep_ais_conf_get(const uint32_t instance, vtss_appl_mep_ais_conf_t *const conf);
    mesa_rc vtss_appl_mep_ais_conf_add(const uint32_t instance, const vtss_appl_mep_ais_conf_t *const conf);
    mesa_rc vtss_appl_mep_ais_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_ais_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_lck_conf_set(const uint32_t instance, const vtss_appl_mep_lck_conf_t *const conf);
    mesa_rc vtss_appl_mep_lck_conf_get(const uint32_t instance, vtss_appl_mep_lck_conf_t *const conf);
    mesa_rc vtss_appl_mep_lck_conf_add(const uint32_t instance, const vtss_appl_mep_lck_conf_t *const conf);
    mesa_rc vtss_appl_mep_lck_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_lck_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_tst_conf_set(const uint32_t instance, const vtss_appl_mep_tst_conf_t *const conf);
    mesa_rc vtss_appl_mep_tst_conf_get(const uint32_t instance, vtss_appl_mep_tst_conf_t *const conf);
    mesa_rc vtss_appl_mep_tst_conf_add(const uint32_t instance, const vtss_appl_mep_tst_conf_t *const conf);
    mesa_rc vtss_appl_mep_tst_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_tst_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_tst_status_get(const uint32_t instance, vtss_appl_mep_tst_state_t *const status);
    mesa_rc vtss_appl_mep_tst_status_clear(const uint32_t instance);
    mesa_rc vtss_appl_mep_tst_control_get(const uint32_t instance, vtss_appl_mep_tst_control_t *const control);
    mesa_rc vtss_appl_mep_tst_control_set(const uint32_t instance, const vtss_appl_mep_tst_control_t *const control);
    mesa_rc vtss_appl_mep_tst_common_conf_get(const uint32_t instance, vtss_appl_mep_tst_common_conf_t *const conf);
    mesa_rc vtss_appl_mep_tst_common_conf_set(const uint32_t instance, const vtss_appl_mep_tst_common_conf_t *const conf);
    mesa_rc vtss_appl_mep_tst_prio_conf_get(const uint32_t instance, const uint32_t prio, const uint32_t test_idx, vtss_appl_mep_test_prio_conf_t *const conf);
    mesa_rc vtss_appl_mep_tst_prio_conf_set(const uint32_t instance, const uint32_t prio, const uint32_t test_idx, const vtss_appl_mep_test_prio_conf_t *const conf);
    mesa_rc vtss_appl_mep_tst_prio_status_get(const uint32_t instance, const uint32_t prio, vtss_appl_mep_tst_state_t *const status);
    mesa_rc vtss_appl_mep_client_flow_conf_set(const uint32_t instance, const vtss_ifindex_t flow, const vtss_appl_mep_client_flow_conf_t *const conf);
    mesa_rc vtss_appl_mep_client_flow_conf_get(const uint32_t instance, const vtss_ifindex_t flow, vtss_appl_mep_client_flow_conf_t *const conf);
    mesa_rc vtss_appl_mep_client_flow_conf_add(const uint32_t instance, const vtss_ifindex_t flow, const vtss_appl_mep_client_flow_conf_t *const conf);
    mesa_rc vtss_appl_mep_client_flow_conf_delete(const uint32_t instance, const vtss_ifindex_t flow);
    mesa_rc vtss_appl_mep_client_flow_conf_iter(const uint32_t *const instance, uint32_t *const next_instance, const vtss_ifindex_t *const flow, vtss_ifindex_t *const next_flow);
    mesa_rc vtss_appl_mep_bfd_control_get(const uint32_t instance, vtss_appl_mep_bfd_control_t *const control);
    mesa_rc vtss_appl_mep_bfd_control_set(const uint32_t instance, const vtss_appl_mep_bfd_control_t *const control);
    mesa_rc vtss_appl_mep_g8113_2_bfd_status_get(const uint32_t instance, vtss_appl_mep_g8113_2_bfd_state_t *const status);
    mesa_rc vtss_appl_mep_g8113_2_bfd_conf_set(const uint32_t instance, const vtss_appl_mep_g8113_2_bfd_conf_t *const conf);
    mesa_rc vtss_appl_mep_g8113_2_bfd_conf_get(const uint32_t instance, vtss_appl_mep_g8113_2_bfd_conf_t *const conf);
    mesa_rc vtss_appl_mep_g8113_2_bfd_clear_stats(const uint32_t instance);
    mesa_rc vtss_appl_mep_g8113_2_bfd_conf_add(const uint32_t instance, const vtss_appl_mep_g8113_2_bfd_conf_t *const conf);
    mesa_rc vtss_appl_mep_g8113_2_bfd_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_g8113_2_bfd_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_set(const uint32_t key_id, const vtss_appl_mep_g8113_2_bfd_auth_conf_t *const conf);
    mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_get(const uint32_t key_id, vtss_appl_mep_g8113_2_bfd_auth_conf_t *const conf);
    mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_add(const uint32_t key_id, const vtss_appl_mep_g8113_2_bfd_auth_conf_t *const conf);
    mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_delete(const uint32_t key_id);
    mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_iter(const uint32_t *const key_id, uint32_t *const next_key_id);
    mesa_rc vtss_appl_mep_rt_conf_set(const uint32_t instance, const vtss_appl_mep_rt_conf_t *const conf);
    mesa_rc vtss_appl_mep_rt_conf_get(const uint32_t instance, vtss_appl_mep_rt_conf_t *const conf);
    mesa_rc vtss_appl_mep_rt_conf_add(const uint32_t instance, const vtss_appl_mep_rt_conf_t *const conf);
    mesa_rc vtss_appl_mep_rt_conf_delete(const uint32_t instance);
    mesa_rc vtss_appl_mep_rt_conf_iter(const uint32_t *const instance, uint32_t *const next_instance);
    mesa_rc vtss_appl_mep_rt_status_get(const uint32_t instance, vtss_appl_mep_rt_status_t *const status);
    mesa_rc vtss_appl_mep_rt_reply_status_get(const uint32_t instance, const uint32_t seq_no, vtss_appl_mep_rt_reply_t *const status);
    mesa_rc vtss_appl_mep_rt_reply_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const seq_no, uint32_t *const next_seq_no);
}

mesa_rc vtss_appl_mep_tlv_conf_set(const vtss_appl_mep_tlv_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_tlv_conf_set(conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_tlv_conf_set(conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_tlv_conf_get(vtss_appl_mep_tlv_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_tlv_conf_get(conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_tlv_conf_get(conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

void vtss_appl_mep_default_conf_get(vtss_appl_mep_default_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_default_conf_get(conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_default_conf_get(conf);
    }
}

mesa_rc vtss_appl_mep_capabilities_get(vtss_appl_mep_capabilities_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_capabilities_get(conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_capabilities_get(conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_instance_conf_set(const uint32_t instance, const vtss_appl_mep_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_instance_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_instance_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_instance_conf_get(const uint32_t instance, vtss_appl_mep_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_instance_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_instance_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_instance_conf_add(const uint32_t instance, const vtss_appl_mep_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_instance_conf_add(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_instance_conf_add(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_instance_conf_delete(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_instance_conf_delete(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_instance_conf_delete(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_instance_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_instance_conf_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_instance_conf_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_instance_peer_conf_set(const uint32_t instance, const uint32_t peer_id, const vtss_appl_mep_peer_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_instance_peer_conf_set(instance, peer_id, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_instance_peer_conf_set(instance, peer_id, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_instance_peer_conf_get(const uint32_t instance, const uint32_t peer_id, vtss_appl_mep_peer_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_instance_peer_conf_get(instance, peer_id, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_instance_peer_conf_get(instance, peer_id, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_instance_peer_conf_add(const uint32_t instance, const uint32_t peer_id, const vtss_appl_mep_peer_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_instance_peer_conf_add(instance, peer_id, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_instance_peer_conf_add(instance, peer_id, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_instance_peer_conf_delete(const uint32_t instance, const uint32_t peer_id)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_instance_peer_conf_delete(instance, peer_id);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_instance_peer_conf_delete(instance, peer_id);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_instance_peer_conf_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const peer_id, uint32_t *const next_peer_id)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_instance_peer_conf_iter(instance, next_instance, peer_id, next_peer_id);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_instance_peer_conf_iter(instance, next_instance, peer_id, next_peer_id);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_syslog_conf_set(const uint32_t instance, const vtss_appl_mep_syslog_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_syslog_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_syslog_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_syslog_conf_get(const uint32_t instance, vtss_appl_mep_syslog_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_syslog_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_syslog_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_syslog_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_syslog_conf_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_syslog_conf_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_instance_status_get(const uint32_t instance, vtss_appl_mep_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_instance_status_get(instance, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_instance_status_get(instance, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_instance_status_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_instance_status_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_instance_status_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_instance_peer_status_get(const uint32_t instance, const uint32_t peer_id, vtss_appl_mep_peer_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_instance_peer_status_get(instance, peer_id, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_instance_peer_status_get(instance, peer_id, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_instance_peer_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const peer_id, uint32_t *const next_peer_id)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_instance_peer_status_iter(instance, next_instance, peer_id, next_peer_id);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_instance_peer_status_iter(instance, next_instance, peer_id, next_peer_id);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_cc_status_get(const uint32_t instance, vtss_appl_mep_cc_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_cc_status_get(instance, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_cc_status_get(instance, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_cc_peer_status_get(const uint32_t instance, const uint32_t peer_id, vtss_appl_mep_peer_cc_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_cc_peer_status_get(instance, peer_id, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_cc_peer_status_get(instance, peer_id, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_pm_conf_set(const uint32_t instance, const vtss_appl_mep_pm_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_pm_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_pm_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_pm_conf_get(const uint32_t instance, vtss_appl_mep_pm_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_pm_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_pm_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_pm_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_pm_conf_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_pm_conf_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lst_conf_set(const uint32_t instance, const vtss_appl_mep_lst_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lst_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lst_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lst_conf_get(const uint32_t instance, vtss_appl_mep_lst_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lst_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lst_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lst_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lst_conf_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lst_conf_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_cc_conf_set(const uint32_t instance, const vtss_appl_mep_cc_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_cc_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_cc_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_cc_conf_get(const uint32_t instance, vtss_appl_mep_cc_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_cc_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_cc_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_cc_conf_add(const uint32_t instance, const vtss_appl_mep_cc_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_cc_conf_add(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_cc_conf_add(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_cc_conf_delete(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_cc_conf_delete(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_cc_conf_delete(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_cc_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_cc_conf_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_cc_conf_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_conf_set(const uint32_t instance, const vtss_appl_mep_lm_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_conf_get(const uint32_t instance, vtss_appl_mep_lm_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_conf_add(const uint32_t instance, const vtss_appl_mep_lm_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_conf_add(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_conf_add(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_conf_delete(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_conf_delete(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_conf_delete(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_conf_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_conf_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_status_get(const uint32_t instance, vtss_appl_mep_lm_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_status_get(instance, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_status_get(instance, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_instance_peer_lm_status_get(const uint32_t instance, const uint32_t peer_id, vtss_appl_mep_lm_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_instance_peer_lm_status_get(instance, peer_id, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_instance_peer_lm_status_get(instance, peer_id, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_status_clear(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_status_clear(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_status_clear(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_status_clear_dir(const uint32_t instance, vtss_appl_mep_clear_dir direction)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_status_clear_dir(instance, direction);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_status_clear_dir(instance, direction);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_control_get(const uint32_t instance, vtss_appl_mep_lm_control_t *const control)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_control_get(instance, control);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_control_get(instance, control);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_control_set(const uint32_t instance, const vtss_appl_mep_lm_control_t *const control)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_control_set(instance, control);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_control_set(instance, control);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_notif_status_get(const uint32_t instance, vtss_appl_mep_lm_notif_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_notif_status_get(instance, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_notif_status_get(instance, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_notif_status_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_notif_status_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_notif_status_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_avail_conf_set(const uint32_t instance, const vtss_appl_mep_lm_avail_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_avail_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_avail_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_avail_conf_get(const uint32_t instance, vtss_appl_mep_lm_avail_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_avail_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_avail_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_avail_conf_add(const uint32_t instance, const vtss_appl_mep_lm_avail_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_avail_conf_add(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_avail_conf_add(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_avail_conf_delete(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_avail_conf_delete(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_avail_conf_delete(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_avail_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_avail_conf_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_avail_conf_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_avail_status_get(const uint32_t instance, const uint32_t peer_instance, vtss_appl_mep_lm_avail_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_avail_status_get(instance, peer_instance, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_avail_status_get(instance, peer_instance, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_hli_conf_set(const uint32_t instance, const vtss_appl_mep_lm_hli_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_hli_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_hli_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_hli_conf_get(const uint32_t instance, vtss_appl_mep_lm_hli_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_hli_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_hli_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_hli_conf_add(const uint32_t instance, const vtss_appl_mep_lm_hli_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_hli_conf_add(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_hli_conf_add(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_hli_conf_delete(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_hli_conf_delete(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_hli_conf_delete(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_hli_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_hli_conf_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_hli_conf_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_hli_status_get(const uint32_t instance, const uint32_t peer_id, vtss_appl_mep_lm_hli_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_hli_status_get(instance, peer_id, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_hli_status_get(instance, peer_id, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_hli_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const peer, uint32_t *const next_peer)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_hli_status_iter(instance, next_instance, peer, next_peer);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_hli_status_iter(instance, next_instance, peer, next_peer);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_sdeg_conf_set(const uint32_t instance, const vtss_appl_mep_lm_sdeg_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_sdeg_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_sdeg_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_sdeg_conf_get(const uint32_t instance, vtss_appl_mep_lm_sdeg_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_sdeg_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_sdeg_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_sdeg_conf_add(const uint32_t instance, const vtss_appl_mep_lm_sdeg_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_sdeg_conf_add(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_sdeg_conf_add(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_sdeg_conf_delete(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_sdeg_conf_delete(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_sdeg_conf_delete(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lm_sdeg_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lm_sdeg_conf_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lm_sdeg_conf_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_conf_set(const uint32_t instance, const vtss_appl_mep_dm_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_conf_get(const uint32_t instance, vtss_appl_mep_dm_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_conf_add(const uint32_t instance, const vtss_appl_mep_dm_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_conf_add(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_conf_add(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_conf_delete(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_conf_delete(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_conf_delete(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_conf_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_conf_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_status_get(const uint32_t instance, vtss_appl_mep_dm_state_t *const tw_status, vtss_appl_mep_dm_state_t *const ow_status_far_to_near, vtss_appl_mep_dm_state_t *const ow_status_near_to_far)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_status_get(instance, tw_status, ow_status_far_to_near, ow_status_near_to_far);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_status_get(instance, tw_status, ow_status_far_to_near, ow_status_near_to_far);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_fd_bin_status_get(const uint32_t instance, const uint32_t index, vtss_appl_mep_dm_fd_bin_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_fd_bin_status_get(instance, index, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_fd_bin_status_get(instance, index, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_fd_bin_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const index, uint32_t *const next_index)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_fd_bin_status_iter(instance, next_instance, index, next_index);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_fd_bin_status_iter(instance, next_instance, index, next_index);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_ifdv_bin_status_get(const uint32_t instance, const uint32_t index, vtss_appl_mep_dm_ifdv_bin_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_ifdv_bin_status_get(instance, index, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_ifdv_bin_status_get(instance, index, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_ifdv_bin_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const index, uint32_t *const next_index)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_ifdv_bin_status_iter(instance, next_instance, index, next_index);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_ifdv_bin_status_iter(instance, next_instance, index, next_index);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_fd_bins_get(const uint32_t instance, const uint32_t bin_number, vtss_appl_mep_dm_fd_bin_state_t *const tw_bin, vtss_appl_mep_dm_bin_value_t *const bin_value)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_fd_bins_get(instance, bin_number, tw_bin, bin_value);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_fd_bins_get(instance, bin_number, tw_bin, bin_value);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_fd_bins_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const bin_number, uint32_t *const next_bin_number)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_fd_bins_iter(instance, next_instance, bin_number, next_bin_number);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_fd_bins_iter(instance, next_instance, bin_number, next_bin_number);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_ifdv_bins_get(const uint32_t instance, const uint32_t bin_number, vtss_appl_mep_dm_ifdv_bin_state_t *const tw_bin, vtss_appl_mep_dm_bin_value_t *const bin_value)
{

    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_ifdv_bins_get(instance, bin_number, tw_bin, bin_value);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_ifdv_bins_get(instance, bin_number, tw_bin, bin_value);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_ifdv_bins_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const bin_number, uint32_t *const next_bin_number)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_ifdv_bins_iter(instance, next_instance, bin_number, next_bin_number);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_ifdv_bins_iter(instance, next_instance, bin_number, next_bin_number);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_tw_status_get(const uint32_t instance, vtss_appl_mep_dm_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_tw_status_get(instance, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_tw_status_get(instance, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_ownf_status_get(const uint32_t instance, vtss_appl_mep_dm_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_ownf_status_get(instance, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_ownf_status_get(instance, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_owfn_status_get(const uint32_t instance, vtss_appl_mep_dm_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_owfn_status_get(instance, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_owfn_status_get(instance, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_status_clear(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_status_clear(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_status_clear(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_control_get(const uint32_t instance, vtss_appl_mep_dm_control_t *const control)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_control_get(instance, control);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_control_get(instance, control);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_control_set(const uint32_t instance, const vtss_appl_mep_dm_control_t *const control)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_control_set(instance, control);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_control_set(instance, control);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_common_conf_get(const uint32_t instance, vtss_appl_mep_dm_common_conf_t *const conf)
{
    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_common_conf_set(const uint32_t instance, const vtss_appl_mep_dm_common_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_common_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_common_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_prio_conf_get(const uint32_t instance, const uint32_t prio, vtss_appl_mep_dm_prio_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_prio_conf_get(instance, prio, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_prio_conf_get(instance, prio, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_prio_conf_set(const uint32_t instance, const uint32_t prio, const vtss_appl_mep_dm_prio_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_prio_conf_set(instance, prio, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_prio_conf_set(instance, prio, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_dm_prio_status_get(const uint32_t instance, const uint32_t prio, vtss_appl_mep_dm_state_t *const tw_status, vtss_appl_mep_dm_state_t *const ow_status_far_to_near, vtss_appl_mep_dm_state_t *const ow_status_near_to_far)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_dm_prio_status_get(instance, prio, tw_status, ow_status_far_to_near, ow_status_near_to_far);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_dm_prio_status_get(instance, prio, tw_status, ow_status_far_to_near, ow_status_near_to_far);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_aps_conf_set(const uint32_t instance, const vtss_appl_mep_aps_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_aps_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_aps_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_aps_conf_get(const uint32_t instance, vtss_appl_mep_aps_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_aps_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_aps_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_aps_conf_add(const uint32_t instance, const vtss_appl_mep_aps_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_aps_conf_add(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_aps_conf_add(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_aps_conf_delete(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_aps_conf_delete(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_aps_conf_delete(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_aps_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_aps_conf_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_aps_conf_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lt_conf_set(const uint32_t instance, const vtss_appl_mep_lt_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lt_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lt_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lt_conf_get(const uint32_t instance, vtss_appl_mep_lt_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lt_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lt_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lt_conf_add(const uint32_t instance, const vtss_appl_mep_lt_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lt_conf_add(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lt_conf_add(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lt_conf_delete(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lt_conf_delete(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lt_conf_delete(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lt_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lt_conf_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lt_conf_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lt_status_get(const uint32_t instance, vtss_appl_mep_lt_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lt_status_get(instance, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lt_status_get(instance, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lt_transaction_status_get(const uint32_t instance, vtss_appl_mep_lt_transaction_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lt_transaction_status_get(instance, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lt_transaction_status_get(instance, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lt_transaction_status_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lt_transaction_status_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lt_transaction_status_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lt_reply_status_get(const uint32_t instance, const uint32_t reply_id, vtss_appl_mep_lt_reply_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lt_reply_status_get(instance, reply_id, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lt_reply_status_get(instance, reply_id, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lt_reply_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const reply_id, uint32_t *const next_reply_id)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lt_reply_status_iter(instance, next_instance, reply_id, next_reply_id);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lt_reply_status_iter(instance, next_instance, reply_id, next_reply_id);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lb_conf_set(const uint32_t instance, const vtss_appl_mep_lb_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lb_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lb_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lb_conf_get(const uint32_t instance, vtss_appl_mep_lb_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lb_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lb_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lb_conf_add(const uint32_t instance, const vtss_appl_mep_lb_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lb_conf_add(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lb_conf_add(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lb_conf_delete(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lb_conf_delete(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lb_conf_delete(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lb_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lb_conf_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lb_conf_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lb_status_get(const uint32_t instance, vtss_appl_mep_lb_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lb_status_get(instance, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lb_status_get(instance, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lb_status_clear(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lb_status_clear(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lb_status_clear(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lb_reply_status_get(const uint32_t instance, const uint32_t reply_id, vtss_appl_mep_lb_reply_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lb_reply_status_get(instance, reply_id, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lb_reply_status_get(instance, reply_id, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lb_reply_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const reply_id, uint32_t *const next_reply_id)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lb_reply_status_iter(instance, next_instance, reply_id, next_reply_id);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lb_reply_status_iter(instance, next_instance, reply_id, next_reply_id);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lb_common_conf_get(const uint32_t instance, vtss_appl_mep_lb_common_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lb_common_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lb_common_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lb_common_conf_set(const uint32_t instance, const vtss_appl_mep_lb_common_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lb_common_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lb_common_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lb_prio_conf_get(const uint32_t instance, const uint32_t prio, const uint32_t test_idx, vtss_appl_mep_test_prio_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lb_prio_conf_get(instance, prio, test_idx, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lb_prio_conf_get(instance, prio, test_idx, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lb_prio_conf_set(const uint32_t instance, const uint32_t prio, const uint32_t test_idx, const vtss_appl_mep_test_prio_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lb_prio_conf_set(instance, prio, test_idx, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lb_prio_conf_set(instance, prio, test_idx, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lb_prio_status_get(const uint32_t instance, const uint32_t prio, vtss_appl_mep_lb_state_t *const status)
{
    return mep_g2::vtss_appl_mep_lb_prio_status_get(instance, prio, status);
}

mesa_rc vtss_appl_mep_ais_conf_set(const uint32_t instance, const vtss_appl_mep_ais_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_ais_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_ais_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_ais_conf_get(const uint32_t instance, vtss_appl_mep_ais_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_ais_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_ais_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_ais_conf_add(const uint32_t instance, const vtss_appl_mep_ais_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_ais_conf_add(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_ais_conf_add(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_ais_conf_delete(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_ais_conf_delete(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_ais_conf_delete(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_ais_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_ais_conf_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_ais_conf_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lck_conf_set(const uint32_t instance, const vtss_appl_mep_lck_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lck_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lck_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lck_conf_get(const uint32_t instance, vtss_appl_mep_lck_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lck_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lck_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lck_conf_add(const uint32_t instance, const vtss_appl_mep_lck_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lck_conf_add(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lck_conf_add(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lck_conf_delete(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lck_conf_delete(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lck_conf_delete(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_lck_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_lck_conf_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_lck_conf_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_tst_conf_set(const uint32_t instance, const vtss_appl_mep_tst_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_tst_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_tst_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_tst_conf_get(const uint32_t instance, vtss_appl_mep_tst_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_tst_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_tst_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_tst_conf_add(const uint32_t instance, const vtss_appl_mep_tst_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_tst_conf_add(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_tst_conf_add(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_tst_conf_delete(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_tst_conf_delete(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_tst_conf_delete(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_tst_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_tst_conf_iter(instance, next_instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_tst_conf_iter(instance, next_instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_tst_status_get(const uint32_t instance, vtss_appl_mep_tst_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_tst_status_get(instance, status);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_tst_status_get(instance, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_tst_status_clear(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_tst_status_clear(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_tst_status_clear(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_tst_control_get(const uint32_t instance, vtss_appl_mep_tst_control_t *const control)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_tst_control_get(instance, control);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_tst_control_get(instance, control);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_tst_control_set(const uint32_t instance, const vtss_appl_mep_tst_control_t *const control)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_tst_control_set(instance, control);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_tst_control_set(instance, control);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_tst_common_conf_get(const uint32_t instance, vtss_appl_mep_tst_common_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_tst_common_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_tst_common_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_tst_common_conf_set(const uint32_t instance, const vtss_appl_mep_tst_common_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_tst_common_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_tst_common_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_tst_prio_conf_get(const uint32_t instance, const uint32_t prio, const uint32_t test_idx, vtss_appl_mep_test_prio_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_tst_prio_conf_get(instance, prio, test_idx, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_tst_prio_conf_get(instance, prio, test_idx, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_tst_prio_conf_set(const uint32_t instance, const uint32_t prio, const uint32_t test_idx, const vtss_appl_mep_test_prio_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_tst_prio_conf_set(instance, prio, test_idx, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_tst_prio_conf_set(instance, prio, test_idx, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_tst_prio_status_get(const uint32_t instance, const uint32_t prio, vtss_appl_mep_tst_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_tst_prio_status_get(instance, prio, status);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_client_flow_conf_set(const uint32_t instance, const vtss_ifindex_t flow, const vtss_appl_mep_client_flow_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_client_flow_conf_set(instance, flow, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_client_flow_conf_set(instance, flow, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_client_flow_conf_get(const uint32_t instance, const vtss_ifindex_t flow, vtss_appl_mep_client_flow_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_client_flow_conf_get(instance, flow, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_client_flow_conf_get(instance, flow, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_client_flow_conf_add(const uint32_t instance, const vtss_ifindex_t flow, const vtss_appl_mep_client_flow_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_client_flow_conf_add(instance, flow, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_client_flow_conf_add(instance, flow, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_client_flow_conf_delete(const uint32_t instance, const vtss_ifindex_t flow)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_client_flow_conf_delete(instance, flow);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_client_flow_conf_delete(instance, flow);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_client_flow_conf_iter(const uint32_t *const instance, uint32_t *const next_instance, const vtss_ifindex_t *const flow, vtss_ifindex_t *const next_flow)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_client_flow_conf_iter(instance, next_instance, flow, next_flow);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_appl_mep_client_flow_conf_iter(instance, next_instance, flow, next_flow);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_bfd_control_get(const uint32_t instance, vtss_appl_mep_bfd_control_t *const control)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_bfd_control_get(instance, control);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_bfd_control_set(const uint32_t instance, const vtss_appl_mep_bfd_control_t *const control)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_bfd_control_set(instance, control);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_g8113_2_bfd_status_get(const uint32_t instance, vtss_appl_mep_g8113_2_bfd_state_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_g8113_2_bfd_status_get(instance, status);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_g8113_2_bfd_conf_set(const uint32_t instance, const vtss_appl_mep_g8113_2_bfd_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_g8113_2_bfd_conf_set(instance, conf);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_g8113_2_bfd_conf_get(const uint32_t instance, vtss_appl_mep_g8113_2_bfd_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_g8113_2_bfd_conf_get(instance, conf);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_g8113_2_bfd_clear_stats(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_g8113_2_bfd_clear_stats(instance);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_g8113_2_bfd_conf_add(const uint32_t instance, const vtss_appl_mep_g8113_2_bfd_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_g8113_2_bfd_conf_add(instance, conf);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_g8113_2_bfd_conf_delete(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_g8113_2_bfd_conf_delete(instance);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_g8113_2_bfd_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_g8113_2_bfd_conf_iter(instance, next_instance);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_set(const uint32_t key_id, const vtss_appl_mep_g8113_2_bfd_auth_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_g8113_2_bfd_auth_conf_set(key_id, conf);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_get(const uint32_t key_id, vtss_appl_mep_g8113_2_bfd_auth_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_g8113_2_bfd_auth_conf_get(key_id, conf);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_add(const uint32_t key_id, const vtss_appl_mep_g8113_2_bfd_auth_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_g8113_2_bfd_auth_conf_add(key_id, conf);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_delete(const uint32_t key_id)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_g8113_2_bfd_auth_conf_delete(key_id);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_iter(const uint32_t *const key_id, uint32_t *const next_key_id)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_g8113_2_bfd_auth_conf_iter(key_id, next_key_id);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_rt_conf_set(const uint32_t instance, const vtss_appl_mep_rt_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_rt_conf_set(instance, conf);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_rt_conf_get(const uint32_t instance, vtss_appl_mep_rt_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_rt_conf_get(instance, conf);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_rt_conf_add(const uint32_t instance, const vtss_appl_mep_rt_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_rt_conf_add(instance, conf);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_rt_conf_delete(const uint32_t instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_rt_conf_delete(instance);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_rt_conf_iter(const uint32_t *const instance, uint32_t *const next_instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_rt_conf_iter(instance, next_instance);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_rt_status_get(const uint32_t instance, vtss_appl_mep_rt_status_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_rt_status_get(instance, status);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_rt_reply_status_get(const uint32_t instance, const uint32_t seq_no, vtss_appl_mep_rt_reply_t *const status)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_rt_reply_status_get(instance, seq_no, status);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc vtss_appl_mep_rt_reply_status_iter(const uint32_t *const instance, uint32_t *const next_instance, const uint32_t *const seq_no, uint32_t *const next_seq_no)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_appl_mep_rt_reply_status_iter(instance, next_instance, seq_no, next_seq_no);
    }






    return MESA_RC_NOT_IMPLEMENTED;
}

u32 vtss_appl_mep_supp_rt_reply_status_iter(const u32           instance,
                                            const u32    *const seq_no,
                                            u32          *const next_seq_no)
{






    return MESA_RC_NOT_IMPLEMENTED;
}

