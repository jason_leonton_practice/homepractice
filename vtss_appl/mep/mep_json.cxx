/*
 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
*/

#include "mep_serializer.hxx"
#include "vtss/basics/expose/json.hxx"

using namespace vtss;
using namespace vtss::json;
using namespace vtss::expose::json;
using namespace vtss::appl::mep::interfaces;

namespace vtss {
void json_node_add(Node *node);
}  // namespace vtss

#define NS(N, P, D) static vtss::expose::json::NamespaceNode N(&P, D);
static NamespaceNode ns_mep("mep");
extern "C" void vtss_appl_mep_json_init() { json_node_add(&ns_mep); }

NS(ns_conf,       ns_mep, "config");
NS(ns_status,     ns_mep, "status");
NS(ns_statistics, ns_mep, "statistics");
NS(ns_control,    ns_mep, "control");

// vtss.mep.capabilities
static StructReadOnly<MepCapabilitiesImpl> mep_capabilities_impl(
        &ns_mep, "capabilities");

// vtss.mep.config.instance
static TableReadWriteAddDelete<MepConfigInstanceTableImpl> ns_conf_instance_table_impl(
        &ns_conf, "instance");

// vtss.mep.config.instancePeer
static TableReadWriteAddDelete<MepConfigInstancePeerTableImpl> ns_conf_instance_peer_table_impl(
        &ns_conf, "instancePeer");

// vtss.mep.config.pm
static TableReadWrite<MepConfigPmTableImpl> ns_conf_pm_table_impl(
        &ns_conf, "pm");

// vtss.mep.config.lst
static TableReadWrite<MepConfigLstTableImpl> ns_conf_lst_table_impl(
        &ns_conf, "lst");

// vtss.mep.config.cc
static TableReadWriteAddDelete<MepConfigCcTableImpl> ns_conf_cc_table_impl(
        &ns_conf, "cc");

// vtss.mep.config.lm
static TableReadWrite<MepConfigLmTableImpl> ns_conf_lm_table_impl(
        &ns_conf, "lm");

// vtss.mep.config.lmAvail
static TableReadWrite<MepConfigLmAvailTableImpl> ns_conf_lm_avail_table_impl(
        &ns_conf, "lmAvail");

// vtss.mep.config.lmHli
static TableReadWrite<MepConfigLmHliTableImpl> ns_conf_lm_hli_table_impl(
        &ns_conf, "lmHli");

// vtss.mep.config.lmSdeg
static TableReadWrite<MepConfigLmSdegTableImpl> ns_conf_lm_sdeg_table_impl(
        &ns_conf, "lmSdeg");

// vtss.mep.config.dm
static TableReadWrite<MepConfigDmTableImpl> ns_conf_dm_table_impl(
        &ns_conf, "dm");

// vtss.mep.config.lb
static TableReadWriteAddDelete<MepConfigLbTableImpl> ns_conf_lb_table_impl(
        &ns_conf, "lb");

// vtss.mep.config.tst
static TableReadWriteAddDelete<MepConfigTstTableImpl> ns_conf_tst_table_impl(
        &ns_conf, "tst");

// vtss.mep.config.lt
static TableReadWriteAddDelete<MepConfigLtTableImpl> ns_conf_lt_table_impl(
        &ns_conf, "lt");

// vtss.mep.config.aps
static TableReadWriteAddDelete<MepConfigApsTableImpl> ns_conf_aps_table_impl(
        &ns_conf, "aps");

// vtss.mep.config.ais
static TableReadWriteAddDelete<MepConfigAisTableImpl> ns_conf_ais_table_impl(
        &ns_conf, "ais");

// vtss.mep.config.lck
static TableReadWriteAddDelete<MepConfigLckTableImpl> ns_conf_lck_table_impl(
        &ns_conf, "lck");

// vtss.mep.config.clientFlow
static TableReadWriteAddDelete<MepConfigClientFlowTableImpl> ns_conf_client_flow_table_impl(
        &ns_conf, "clientFlow");

// vtss.mep.config.syslog
static TableReadWrite<MepConfigSyslogTableImpl> ns_conf_syslog_table_impl(
        &ns_conf, "syslog");

// vtss.mep.config.tlvLeaf
static StructReadWrite<MepConfigTlvLeaf> ns_conf_tlv_leaf(
        &ns_conf, "tlvLeaf");

// vtss.mep.config.bfd
static TableReadWriteAddDelete<MepConfigBfdTableImpl> ns_conf_bfd_table_impl(
        &ns_conf, "bfd");

// vtss.mep.config.bfdAuth
static TableReadWriteAddDelete<MepConfigBfdAuthTableImpl> ns_conf_bfd_auth_table_impl(
        &ns_conf, "bfdAuth");

// vtss.mep.config.rt
static TableReadWriteAddDelete<MepConfigRtTableImpl> ns_conf_rt_table_impl(
        &ns_conf, "rt");

// vtss.mep.status.instance
static TableReadOnlyNotification<MepStatusInstanceTableImpl> ns_status_instance_table_impl(
        &ns_status, "instance", &mep_status);

// vtss.mep.status.instancePeer
static TableReadOnlyNotification<MepStatusInstancePeerTableImpl> ns_status_instance_peer_table_impl(
        &ns_status, "instancePeer", &mep_status_peer);

// vtss.mep.status.lmNotif
static TableReadOnlyNotification<MepStatusLmNotifTableImpl> ns_status_lm_notif_table_impl(
        &ns_status, "lmNotif", &mep_status_lm_notif);

// vtss.mep.status.lm
static TableReadOnly<MepStatusLmTableImpl> ns_status_lm_table_impl(
        &ns_status, "lm");

// vtss.mep.status.lmPeer
static TableReadOnly<MepStatusLmPeerTableImpl> ns_status_lm_peer_table_impl(
        &ns_status, "lmPeer");

// vtss.mep.status.lmAvail
static TableReadOnly<MepStatusLmAvailTableImpl> ns_status_lm_avail_table_impl(
        &ns_status, "lmAvail");

// vtss.mep.status.lmHli
static TableReadOnlyNotification<MepStatusLmHliTableImpl> ns_status_lm_hli_table_impl(
        &ns_status, "lmHli", &mep_status_lm_hli);

// vtss.mep.status.dmTwoWay
static TableReadOnly<MepStatusDmTwTableImpl> ns_status_dm_tw_table_impl(
        &ns_status, "dmTwoWay");

// vtss.mep.status.dmOneWayFarNear
static TableReadOnly<MepStatusDmOwfnTableImpl> ns_status_dm_owfn_table_impl(
        &ns_status, "dmOneWayFarNear");

// vtss.mep.status.dmOneWayNearFar
static TableReadOnly<MepStatusDmOwnfTableImpl> ns_status_dm_ownf_table_impl(
        &ns_status, "dmOneWayNearFar");

// vtss.mep.status.dmBinFd
static TableReadOnly<MepStatusDmFdBinTableImpl> ns_status_dm_bin_fd_table_impl(
        &ns_status, "dmBinFd");

// vtss.mep.status.dmBinIfdv
static TableReadOnly<MepStatusDmIfdvBinTableImpl> ns_status_dm_bin_ifdv_table_impl(
        &ns_status, "dmBinIfdv");

// vtss.mep.status.lb
static TableReadOnly<MepStatusLbTableImpl> ns_status_lb_table_impl(
        &ns_status, "lb");

// vtss.mep.status.lbReply
static TableReadOnly<MepStatusLbReplyTableImpl> ns_status_lb_reply_table_impl(
        &ns_status, "lbReply");

// vtss.mep.status.tst
static TableReadOnly<MepStatusTstTableImpl> ns_status_tst_table_impl(
        &ns_status, "tst");

// vtss.mep.status.lt
static TableReadOnly<MepStatusLtTableImpl> ns_status_lt_table_impl(
        &ns_status, "lt");

// vtss.mep.status.ltReply
static TableReadOnly<MepStatusLtReplyTableImpl> ns_status_lt_reply_table_impl(
        &ns_status, "ltReply");

// vtss.mep.status.ccPeer
static TableReadOnly<MepStatusCcPeerTableImpl> ns_status_cc_peer_table_impl(
        &ns_status, "ccPeer");

// vtss.mep.status.bfd
static TableReadOnly<MepStatusBfdTableImpl> ns_status_bfd_table_impl(
        &ns_status, "bfd");

// vtss.mep.status.rt
static TableReadOnly<MepStatusRtTableImpl> ns_status_rt_table_impl(
        &ns_status, "rt");

// vtss.mep.status.rtReply
static TableReadOnly<MepStatusRtReplyTableImpl> ns_status_rt_reply_table_impl(
        &ns_status, "rtReply");

// vtss.mep.control.lm
static TableReadWrite<MepControlLmTableImpl> mep_control_lm_table_impl(
        &ns_control, "lm");

// vtss.mep.control.dm
static TableReadWrite<MepControlDmTableImpl> mep_control_dm_table_impl(
        &ns_control, "dm");

// vtss.mep.control.tst
static TableReadWrite<MepControlTstTableImpl> mep_control_tst_table_impl(
        &ns_control, "tst");

// vtss.mep.control.bfd
static TableReadWrite<MepControlBfdTableImpl> mep_control_bfd_table_impl(
        &ns_control, "bfd");

