/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#include "mscc/ethernet/switch/api.h"
#include "icli_api.h"
#include "icli_porting_util.h"
#include "mep_api.h"
#include "mep_peer_array.hxx"
#include "misc_api.h"
#include "mep_icli_functions.h"
#include "packet_api.h"
#include "mep_model.h"
#include "mep_engine.h"
#include "mep_trace.h"
#ifdef VTSS_SW_OPTION_AFI
#include "afi_api.h"
#endif

#ifdef VTSS_SW_OPTION_ICFG
#include "icfg_api.h"
#endif








#define MPLS_DOMAIN(domain) (FALSE)



u32 vtss_mep_supp_conf_mac_get(const u32  instance,
                               u8         *mac);

u32 vtss_appl_mep_supp_rt_reply_status_iter(const u32           instance,
                                            const u32    *const seq_no,
                                            u32          *const next_seq_no);

/***************************************************************************/
/*  Internal types                                                         */
/****************************************************************************/

/***************************************************************************/
/*  Internal functions                                                     */
/****************************************************************************/
static void mep_print_error(u32 session_id, mesa_rc rc)
{
    if (rc == VTSS_APPL_MEP_RC_STATE_NOT_UP) {
        ICLI_PRINTF("%s\n", error_txt(rc));
    } else {
        ICLI_PRINTF("Error: %s\n", error_txt(rc));
    }
}

static const char *node_id_to_str(const u32 node_id, char *buf)
{
    (void)sprintf(buf, "%u.%u.%u.%u", node_id >> 24, (node_id >> 16) & 0xff,
                  (node_id >> 8) & 0xff, node_id & 0xff);
    return buf;
}

/***************************************************************************/
/*  Functions called by iCLI                                                */
/****************************************************************************/

BOOL mep_runtime_range_instance(u32                   session_id,
                                icli_runtime_ask_t    ask,
                                icli_runtime_t        *runtime)
{
    switch (ask) {
        case ICLI_ASK_RANGE:
            runtime->range.type = ICLI_RANGE_TYPE_UNSIGNED;
            runtime->range.u.sr.cnt = 1;
            runtime->range.u.sr.range[0].min = 1;
            runtime->range.u.sr.range[0].max = VTSS_APPL_MEP_INSTANCE_MAX;
            return TRUE;

        default:
            break;
    }
    return FALSE;
}

BOOL mep_runtime_mpls_present(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    if (ask == ICLI_ASK_PRESENT) {
        runtime->present = FALSE;



        return TRUE;
    }
    return FALSE;
}

static void print_dm_state_header(i32 session_id)
{
    ICLI_PRINTF("\n");
    ICLI_PRINTF("MEP DM state is:\n");
    ICLI_PRINTF("\n");
    if (mep_caps.mesa_mep_serval) {
        ICLI_PRINTF("RxTime : Rx Timeout\n");
    }
    ICLI_PRINTF("RxErr :  Rx Error\n");
    ICLI_PRINTF("AvTot :  Average delay Total\n");
    ICLI_PRINTF("AvN :    Average delay last N\n");
    ICLI_PRINTF("Min :    Min Delay value\n");
    ICLI_PRINTF("Max :    Max Delay value\n");
    ICLI_PRINTF("AvVarT : Average delay Variation Total\n");
    ICLI_PRINTF("AvVarN : Average delay Variation last N\n");
    ICLI_PRINTF("MinVar : Min Delay Variation value\n");
    ICLI_PRINTF("MaxVar : Max Delay Variation value\n");
    ICLI_PRINTF("OF  :    Overflow. The number of statistics overflow.\n");

    ICLI_PRINTF("\n");
    ICLI_PRINTF("%12s", " ");
    ICLI_PRINTF("%4s", "Inst");
    ICLI_PRINTF("%9s", "Tx");
    ICLI_PRINTF("%9s", "Rx");
    if (mep_caps.mesa_mep_serval) {
        ICLI_PRINTF("%9s", "RxTime");
    }
    ICLI_PRINTF("%9s", "RxErr");
    ICLI_PRINTF("%11s", "AvTot");
    ICLI_PRINTF("%11s", "AvN");
    ICLI_PRINTF("%11s", "Min");
    ICLI_PRINTF("%11s", "Max");
    ICLI_PRINTF("%11s", "AvVarTot");
    ICLI_PRINTF("%11s", "AvVarN");
    ICLI_PRINTF("%11s", "MinVar");
    ICLI_PRINTF("%11s", "MaxVar");
    ICLI_PRINTF("%4s", "OF");
    ICLI_PRINTF("%5s", "Unit");
    ICLI_PRINTF("\n");
}

static void print_dm_state(i32 session_id, u32 instance, vtss_appl_mep_dm_state_t *state)
{
    ICLI_PRINTF("%4u", instance);
    ICLI_PRINTF("%9u", state->tx_cnt);
    ICLI_PRINTF("%9u", state->rx_cnt);
    if (mep_caps.mesa_mep_serval) {
        ICLI_PRINTF("%9u", state->rx_tout_cnt);
    }
    ICLI_PRINTF("%9u", state->rx_err_cnt);
    ICLI_PRINTF("%11u", state->avg_delay);
    ICLI_PRINTF("%11u", state->avg_n_delay);
    ICLI_PRINTF("%11u", state->min_delay);
    ICLI_PRINTF("%11u", state->max_delay);
    ICLI_PRINTF("%11u", state->avg_delay_var);
    ICLI_PRINTF("%11u", state->avg_n_delay_var);
    ICLI_PRINTF("%11u", state->min_delay_var);
    ICLI_PRINTF("%11u", state->max_delay_var);
    ICLI_PRINTF("%4u", state->ovrflw_cnt);
    ICLI_PRINTF("%5s", state->tunit == VTSS_APPL_MEP_US ? "us" : "ns");
    ICLI_PRINTF("\n");
}

static void print_dm_bin_header(i32 session_id, u32 num)
{
    u32 i;
    ICLI_PRINTF("\n");
    ICLI_PRINTF("MEP DM binning state is:\n");
    ICLI_PRINTF("%-12s", "");
    ICLI_PRINTF("%4s", "Inst");
    for (i = 1; i <= num; i++ ) {
        ICLI_PRINTF("%8s%d", "Bin", i);
    }
    ICLI_PRINTF("\n");
}

static void print_bin_state(i32 session_id, u32 instance, u32 *bin, u32 size)
{
    u32 i;

    ICLI_PRINTF("%4u", instance);
    for (i=0; i<size; ++i) {
        ICLI_PRINTF("%9u", bin[i]);
    }
    ICLI_PRINTF("\n");
}

BOOL mep_show_mep(i32 session_id, icli_range_t *inst,
                  BOOL has_peer, BOOL has_cc, BOOL has_lm, BOOL has_dm, BOOL has_lt, BOOL has_lb, BOOL has_tst, BOOL has_aps, BOOL has_client, BOOL has_ais, BOOL has_lck, BOOL has_pm, BOOL has_syslog, BOOL has_tlv, BOOL has_bfd, BOOL has_rt, BOOL has_lst, BOOL has_avail, BOOL has_lm_hli, BOOL has_detail)
{
    u32                              i, j, k, t, l, list_cnt, min, max, res_port;
    BOOL                             vola=FALSE, enable;
    mep_conf_t                       config;
    vtss_appl_mep_state_t            state;
    vtss_appl_mep_cc_conf_t          cc_config;
    vtss_appl_mep_peer_state_t       peer_state;
    vtss_appl_mep_cc_state_t         cc_state;
    vtss_appl_mep_pm_conf_t          pm_config;
    vtss_appl_mep_lst_conf_t         lst_config;
    vtss_appl_mep_syslog_conf_t      syslog_config;
    vtss_appl_mep_tlv_conf_t         tlv_config;
    vtss_appl_mep_lm_conf_t          lm_config;
    vtss_appl_mep_lm_avail_conf_t    lm_avail_config;
    vtss_appl_mep_lm_hli_conf_t      lm_hli_config;
    vtss_appl_mep_lm_state_t         lm_state;
    vtss_appl_mep_lm_avail_state_t   lm_avail_state;
    vtss_appl_mep_lm_hli_state_t     lm_hli_state;
    vtss_appl_mep_dm_conf_t          dm_config;
    vtss_appl_mep_dm_state_t         dmr_state, dm1_state_far_to_near, dm1_state_near_to_far;
    vtss_appl_mep_lt_conf_t          lt_config;
    vtss_appl_mep_lt_state_t         lt_state;
    vtss_appl_mep_lb_conf_t          lb_config;
    vtss_appl_mep_lb_state_t         lb_state;
    vtss_appl_mep_test_prio_conf_t   test_prio_config;
    vtss_appl_mep_tst_common_conf_t  tst_common_config;
    vtss_appl_mep_tst_state_t        tst_state;
    vtss_appl_mep_aps_conf_t         aps_config;
    mep_client_conf_t                client_config;
    vtss_appl_mep_ais_conf_t         ais_config;
    vtss_appl_mep_lck_conf_t         lck_config;
    vtss_mep_mgmt_etree_conf_t       etree_conf;
    mep_eps_state_t                  eps_state;
    char                             string[MEP_EPS_MAX * 6];
    char                             *strp;
    char                             buf[ICLI_PORTING_STR_BUF_SIZE];
    mesa_rc                          rc;
    u32                              vtss_appl_mep_instance_max = VTSS_APPL_MEP_INSTANCE_MAX;








    memset(&peer_state, 0 ,sizeof(peer_state));

    if (!has_peer && !has_cc && !has_lm && !has_dm && !has_lt && !has_lb && !has_tst && !has_aps && !has_client && !has_ais && !has_lck && !has_pm && !has_syslog && !has_tlv && !has_bfd && !has_rt && !has_lst && !has_avail && !has_lm_hli) {
        ICLI_PRINTF("\n");
        if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
            ICLI_PRINTF("Oper = 'Up' ->     The instance is UP meaning it is physically configured and operational\n");
            ICLI_PRINTF("Oper = 'Down' ->   The instance is DOWN meaning it is NOT physically configured and operational\n");
            ICLI_PRINTF("Oper = 'Config' -> The instance is DOWN due to invalid configuration\n");
            ICLI_PRINTF("Oper = 'HW' ->     The instance is DOWN due to failing OAM supporting HW resources\n");
            ICLI_PRINTF("Oper = 'MCE' ->    The instance is DOWN due to failing MCE resources\n");
            ICLI_PRINTF("\n");
        }
        ICLI_PRINTF("MEP state is:\n");
        ICLI_PRINTF("%6s", "Inst");
        if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
            ICLI_PRINTF("%8s", "Oper");
        }
        ICLI_PRINTF("%8s", "cLevel");
        ICLI_PRINTF("%6s", "cMeg");
        ICLI_PRINTF("%6s", "cMep");
        ICLI_PRINTF("%6s", "cAis");
        ICLI_PRINTF("%6s", "cLck");
        ICLI_PRINTF("%6s", "cLoop");
        ICLI_PRINTF("%6s", "cConf");
        ICLI_PRINTF("%6s", "cDeg");
        ICLI_PRINTF("%6s", "cSsf");
        ICLI_PRINTF("%6s", "aBlk");
        ICLI_PRINTF("%6s", "aTsd");
        ICLI_PRINTF("%6s", "aTsf");
        ICLI_PRINTF("%10s", "Peer MEP");
        ICLI_PRINTF("%6s", "cLoc");
        ICLI_PRINTF("%6s", "cRdi");
        ICLI_PRINTF("%8s", "cPeriod");
        ICLI_PRINTF("%6s", "cPrio");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else  {
            list_cnt = inst->u.sr.cnt;
        }

        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {
                if (((rc = mep_instance_conf_get(j, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
                    if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                    continue;
                }
                if (!config.enable) {
                    continue;
                }
                if ((rc = vtss_appl_mep_instance_status_get(j, &state)) != VTSS_RC_OK) {
                    mep_print_error(session_id, rc);
                    continue;
                }
                ICLI_PRINTF("%6u", j+1);
                if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
                    switch (state.oper_state) {
                        case VTSS_APPL_MEP_OPER_UP:             ICLI_PRINTF("%8s", "Up");      break;
                        case VTSS_APPL_MEP_OPER_DOWN:           ICLI_PRINTF("%8s", "Down");    break;
                        case VTSS_APPL_MEP_OPER_INVALID_CONFIG: ICLI_PRINTF("%8s", "Config");  break;
                        case VTSS_APPL_MEP_OPER_HW_OAM:         ICLI_PRINTF("%8s", "HW");      break;
                        case VTSS_APPL_MEP_OPER_MCE:            ICLI_PRINTF("%8s", "MCE");     break;
                    }
                }
                if (config.mode == VTSS_APPL_MEP_MIP) {
                    ICLI_PRINTF("%8s", "(Mip)\n");
                    continue;
                }
                ICLI_PRINTF("%8s",  (state.cLevel) ? "True" : "False");
                ICLI_PRINTF("%6s",  (state.cMeg) ? "True" : "False");
                ICLI_PRINTF("%6s",  (state.cMep) ? "True" : "False");
                ICLI_PRINTF("%6s",  (state.cAis) ? "True" : "False");
                ICLI_PRINTF("%6s",  (state.cLck) ? "True" : "False");
                ICLI_PRINTF("%6s",  (state.cLoop) ? "True" : "False");
                ICLI_PRINTF("%6s",  (state.cConfig) ? "True" : "False");
                ICLI_PRINTF("%6s",  (state.cDeg) ? "True" : "False");
                ICLI_PRINTF("%6s",  (state.cSsf) ? "True" : "False");
                ICLI_PRINTF("%6s",  (state.aBlk) ? "True" : "False");
                ICLI_PRINTF("%6s",  (state.aTsd) ? "True" : "False");
                ICLI_PRINTF("%6s",  (state.aTsf) ? "True" : "False");
                for (k=0; k<config.peer_count; ++k) {
                    if ((rc = vtss_appl_mep_instance_peer_status_get(j, config.peer_mep[k], &peer_state)) != VTSS_RC_OK) {
                        mep_print_error(session_id, rc);
                        continue;
                    }
                    if (k != 0) {
                        ICLI_PRINTF("%80s", " ");
                        if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
                            ICLI_PRINTF("%8s", " ");
                        }
                    }
                    ICLI_PRINTF("%10u", config.peer_mep[k]);
                    ICLI_PRINTF("%6s",  (peer_state.cLoc) ? "True" : "False");
                    ICLI_PRINTF("%6s",  (peer_state.cRdi) ? "True" : "False");
                    ICLI_PRINTF("%8s",  (peer_state.cPeriod) ? "True" : "False");
                    ICLI_PRINTF("%6s",  (peer_state.cPrio) ? "True" : "False");
                    ICLI_PRINTF("\n");
                }
                if (config.peer_count == 0) {
                    ICLI_PRINTF("\n");
                }
            }
        }
        ICLI_PRINTF("\n");

        if (has_detail) {
            ICLI_PRINTF("\n");
            ICLI_PRINTF("MEP Basic Configuration is:\n");
            ICLI_PRINTF("%6s", "Inst");
            ICLI_PRINTF("%6s", "Mode");
            ICLI_PRINTF("%5s", "Voe");
            ICLI_PRINTF("%6s", "Vola");
            ICLI_PRINTF("%8s", "Direct");
            ICLI_PRINTF("%23s", "Port");
            ICLI_PRINTF("%9s", "Dom");
            ICLI_PRINTF("%7s", "Level");
            ICLI_PRINTF("%13s", "Format");
            ICLI_PRINTF("%18s", "Name");
            ICLI_PRINTF("%18s", "Meg id");
            ICLI_PRINTF("%8s", "Mep id");
            ICLI_PRINTF("%6s", "Vid");
            ICLI_PRINTF("%6s", "Flow");
            ICLI_PRINTF("%11s", "Eps");
            ICLI_PRINTF("%19s", "MAC");
            ICLI_PRINTF("\n");

            if (inst == NULL) {
                list_cnt = 1;
            }
            else {
                list_cnt = inst->u.sr.cnt;
            }

            for (i=0; i<list_cnt; ++i) {
                if (inst == NULL) {
                    min = 1;
                    max = vtss_appl_mep_instance_max;
                }
                else {
                    min = inst->u.sr.range[i].min;
                    max = inst->u.sr.range[i].max;
                }

                if ((min == 0) || (max == 0)) {
                    ICLI_PRINTF("Invalid MEP instance number\n");
                    continue;
                }
                for (j=min-1; j<max; ++j) {
                    if (((rc = mep_instance_conf_get(j, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
                        if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                        continue;
                    }
                    if (!config.enable) {
                        continue;
                    }
                    vola = (rc == VTSS_APPL_MEP_RC_VOLATILE);
                    if ((rc = mep_status_eps_get(j, &eps_state)) != VTSS_RC_OK) {
                        mep_print_error(session_id, rc);
                        continue;
                    }
                    if ((rc = vtss_mep_mgmt_etree_conf_get(j, &etree_conf)) != VTSS_RC_OK) {
                        mep_print_error(session_id, rc);
                        continue;
                    }
                    ICLI_PRINTF("%6u", j+1);
                    switch (config.mode) {
                        case VTSS_APPL_MEP_MEP:       ICLI_PRINTF("%6s", "Mep");     break;
                        case VTSS_APPL_MEP_MIP:       ICLI_PRINTF("%6s", "Mip");     break;
                    }
                    if (config.voe) {
                        ICLI_PRINTF("%5s", "Voe");
                    }
                    else {
                        ICLI_PRINTF("%5s", " ");
                    }
                    if (vola) {
                        ICLI_PRINTF("%6s", "Vola");
                    }
                    else {
                        ICLI_PRINTF("%6s", " ");
                    }
                    if (etree_conf.e_tree == VTSS_MEP_ETREE_NONE) {
                        switch (config.direction) {
                            case VTSS_APPL_MEP_DOWN:  ICLI_PRINTF("%8s", "Down");    break;
                            case VTSS_APPL_MEP_UP:    ICLI_PRINTF("%8s", "Up");      break;
                        }
                    } else {
                        switch (etree_conf.e_tree) {
                            case VTSS_MEP_ETREE_NONE:  ICLI_PRINTF("%8s", "None");   break;
                            case VTSS_MEP_ETREE_ROOT:  ICLI_PRINTF("%8s", "Root");   break;
                            case VTSS_MEP_ETREE_LEAF:  ICLI_PRINTF("%8s", "Leaf");   break;
                        }
                    }
                    res_port = config.port;
                    ICLI_PRINTF("%23s", icli_port_info_txt(VTSS_USID_START, iport2uport(res_port), buf));

                    switch (config.domain) {
                        case VTSS_APPL_MEP_PORT:        ICLI_PRINTF("%9s", "Port");                                 break;
//                      case VTSS_APPL_MEP_ESP:         ICLI_PRINTF("%9s", "Eps");                                  break;
                        case VTSS_APPL_MEP_EVC:         ICLI_PRINTF("%9s", (config.vid == 0) ? "Evc" : "Evc-Sub");  break;
                        case VTSS_APPL_MEP_VLAN:        ICLI_PRINTF("%9s", "Vlan");                                 break;
                        case VTSS_APPL_MEP_MPLS_LINK:   ICLI_PRINTF("%9s", "MplsL");                                break;
                        case VTSS_APPL_MEP_MPLS_TUNNEL: ICLI_PRINTF("%9s", "MplsT");                                break;
                        case VTSS_APPL_MEP_MPLS_PW:     ICLI_PRINTF("%9s", "MplsPW");                               break;
                        case VTSS_APPL_MEP_MPLS_LSP:    ICLI_PRINTF("%9s", "MplsL");                                break;
                    }
                    ICLI_PRINTF("%7u", config.level);
                    if (config.mode == VTSS_APPL_MEP_MEP) {
                        switch (config.format) {
                            case VTSS_APPL_MEP_ITU_ICC:     ICLI_PRINTF("%13s", "ITU ICC");       break;
                            case VTSS_APPL_MEP_IEEE_STR:    ICLI_PRINTF("%13s", "IEEE String");   break;
                            case VTSS_APPL_MEP_ITU_CC_ICC:  ICLI_PRINTF("%13s", "ITU ICC+CC");    break;
                        }
                        ICLI_PRINTF("%18s", config.name);
                        ICLI_PRINTF("%18s", config.meg);
                        ICLI_PRINTF("%8u", config.mep);
                    }
                    else {
                        ICLI_PRINTF("%13s"," ");
                        ICLI_PRINTF("%18s"," ");
                        ICLI_PRINTF("%18s"," ");
                        ICLI_PRINTF("%8s"," ");
                    }
                    ICLI_PRINTF("%6u", config.vid);
                    if (config.domain == VTSS_APPL_MEP_PORT) {
                        ICLI_PRINTF("%6s", "-");
                    } else if (MPLS_DOMAIN(config.domain)) {
                        ICLI_PRINTF("%6u", config.flow);
                    } else {
                        ICLI_PRINTF("%6u", (config.domain != VTSS_APPL_MEP_VLAN) ? config.flow+1 : config.flow);
                    }
                    if (config.mode == VTSS_APPL_MEP_MEP) {
                        if (eps_state.count) {
                            string[0] = '\0';
                            strp = &string[0];
                            for (k=0; k<MEP_EPS_MAX && k<3 && k<eps_state.count; ++k) {
                                strp += sprintf(strp, "%u-", eps_state.inst[k]+1);
                            }
                            string[strlen(string)-1] = '\0';
                            ICLI_PRINTF("%11s", string);
                        }
                        else {
                            ICLI_PRINTF("%11s", " ");
                        }
                    }
                    else {
                        ICLI_PRINTF("%11s", " ");
                    }
                    if (MPLS_DOMAIN(config.domain)) {
                        ICLI_PRINTF("      NOT AVAILABLE      " );
                    }
                    else {
                        ICLI_PRINTF("  %02X-%02X-%02X-%02X-%02X-%02X", config.mac.addr[0], config.mac.addr[1], config.mac.addr[2], config.mac.addr[3], config.mac.addr[4], config.mac.addr[5]);
                    }
                    ICLI_PRINTF("\n");
                }
            }
            ICLI_PRINTF("\n");
        }
    }

    if (has_pm && has_detail) {
        ICLI_PRINTF("\n");
        ICLI_PRINTF("MEP PM Configuration is:\n");
        ICLI_PRINTF("%9s", "Inst");
        ICLI_PRINTF("%9s", "enabled");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }
        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {
                if (((rc = mep_instance_conf_get(j, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
                    if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                    continue;
                }
                if (!config.enable) {
                    continue;
                }
                if ((rc = vtss_appl_mep_pm_conf_get(j, &pm_config)) != VTSS_RC_OK) {
                    mep_print_error(session_id, rc);
                    continue;
                }
                ICLI_PRINTF("%9u", j+1);
                ICLI_PRINTF("%9s", (pm_config.enable) ? "True" : "False");
                ICLI_PRINTF("\n");
            }
        }
        ICLI_PRINTF("\n");
    }

    if (has_lst && has_detail) {
        ICLI_PRINTF("\n");
        ICLI_PRINTF("MEP LST Configuration is:\n");
        ICLI_PRINTF("%9s", "Inst");
        ICLI_PRINTF("%9s", "enabled");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }
        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {
                if (((rc = mep_instance_conf_get(j, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
                    if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                    continue;
                }
                if (!config.enable) {
                    continue;
                }
                if ((rc = vtss_appl_mep_lst_conf_get(j, &lst_config)) != VTSS_RC_OK) {
                    mep_print_error(session_id, rc);
                    continue;
                }
                ICLI_PRINTF("%9u", j+1);
                ICLI_PRINTF("%9s", (lst_config.enable) ? "True" : "False");
                ICLI_PRINTF("\n");
            }
        }
        ICLI_PRINTF("\n");
    }

    if (has_syslog && has_detail) {
        ICLI_PRINTF("\n");
        ICLI_PRINTF("MEP Syslog Configuration is:\n");
        ICLI_PRINTF("%9s", "Inst");
        ICLI_PRINTF("%9s", "enabled");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }
        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {
                if (((rc = mep_instance_conf_get(j, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
                    if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                    continue;
                }
                if (!config.enable) {
                    continue;
                }
                if ((rc = vtss_appl_mep_syslog_conf_get(j, &syslog_config)) != VTSS_RC_OK) {
                    mep_print_error(session_id, rc);
                    continue;
                }
                ICLI_PRINTF("%9u", j+1);
                ICLI_PRINTF("%9s", (syslog_config.enable) ? "True" : "False");
                ICLI_PRINTF("\n");
            }
        }
        ICLI_PRINTF("\n");
    }

    if (has_tlv) {
        ICLI_PRINTF("\n");
        ICLI_PRINTF("MEP CCM TLV Status is:\n");
        ICLI_PRINTF("%9s", "Inst");
        ICLI_PRINTF("%10s", "Peer MEP");
        ICLI_PRINTF("%11s", "OS OUI");
        ICLI_PRINTF("%9s", "OS Sub");
        ICLI_PRINTF("%11s", "OS Value");
        ICLI_PRINTF("%11s", "PS Value");
        ICLI_PRINTF("%11s", "IS Value");
        ICLI_PRINTF("%8s", "OS RX");
        ICLI_PRINTF("%8s", "PS RX");
        ICLI_PRINTF("%8s", "IS RX");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }
        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {
                if (((rc = mep_instance_conf_get(j, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
                    if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                    continue;
                }
                if (!config.enable) {
                    continue;
                }
                if ((rc = vtss_appl_mep_cc_status_get(j, &cc_state)) != VTSS_RC_OK) {
                    mep_print_error(session_id, rc);
                    continue;
                }
                ICLI_PRINTF("%9u", j+1);
                for (k=0; k<config.peer_count; ++k) {
                    if (k != 0) {
                        ICLI_PRINTF("%9s", " ");
                    }
                    ICLI_PRINTF("%10u", config.peer_mep[k]);
                    ICLI_PRINTF("%3s%2.2X-%2.2X-%2.2X", " ", cc_state.os_tlv[k].oui[0], cc_state.os_tlv[k].oui[1], cc_state.os_tlv[k].oui[2]);
                    ICLI_PRINTF("%9X", cc_state.os_tlv[k].subtype);
                    ICLI_PRINTF("%11X", cc_state.os_tlv[k].value);
                    ICLI_PRINTF("%11X", cc_state.ps_tlv[k].value);
                    ICLI_PRINTF("%11X", cc_state.is_tlv[k].value);
                    ICLI_PRINTF("%8s", (cc_state.os_received[k]) ? "True" : "False");
                    ICLI_PRINTF("%8s", (cc_state.ps_received[k]) ? "True" : "False");
                    ICLI_PRINTF("%8s", (cc_state.is_received[k]) ? "True" : "False");
                    ICLI_PRINTF("\n");
                }
                if (config.peer_count == 0) {
                    ICLI_PRINTF("\n");
                }
            }
        }
        ICLI_PRINTF("\n");

        if (has_detail) {
            if ((rc = vtss_appl_mep_tlv_conf_get(&tlv_config)) != VTSS_RC_OK) {
                mep_print_error(session_id, rc);
            } else {
                ICLI_PRINTF("\n");
                ICLI_PRINTF("MEP TLV Configuration is:\n");
                ICLI_PRINTF("%s%2.2X-%2.2X-%2.2X\n", "Organization-Specific TLV: OUI ", tlv_config.os_tlv.oui[0], tlv_config.os_tlv.oui[1], tlv_config.os_tlv.oui[2]);
                ICLI_PRINTF("%s%X\n", "Organization-Specific TLV: Sub-Type ", tlv_config.os_tlv.subtype);
                ICLI_PRINTF("%s%X\n", "Organization-Specific TLV: Value ", tlv_config.os_tlv.value);
                ICLI_PRINTF("\n");
            }
        }
    }

    if (has_peer && has_detail) {
        ICLI_PRINTF("\n");
        ICLI_PRINTF("MEP Peer MEP Configuration is:\n");
        ICLI_PRINTF("%9s", "Inst");
        ICLI_PRINTF("%12s", "Peer id");
        ICLI_PRINTF("%22s", "Peer MAC");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }
        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {
                if (((rc = mep_instance_conf_get(j, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
                    if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                    continue;
                }
                if (!config.enable) {
                    continue;
                }
                if (config.peer_count != 0) {
                    ICLI_PRINTF("%9u", j+1);
                    for (k=0; k<config.peer_count; ++k) {
                        if (k != 0) {
                            ICLI_PRINTF("         ");
                        }
                        ICLI_PRINTF("%12u", config.peer_mep[k]);
                        ICLI_PRINTF("     %02X-%02X-%02X-%02X-%02X-%02X", config.peer_mac[k].addr[0], config.peer_mac[k].addr[1], config.peer_mac[k].addr[2],
                                                                          config.peer_mac[k].addr[3], config.peer_mac[k].addr[4], config.peer_mac[k].addr[5]);
                        ICLI_PRINTF("\n");
                    }
                }
            }
        }
        ICLI_PRINTF("\n");
    }

    if (has_cc && has_detail) {
        ICLI_PRINTF("\n");
        ICLI_PRINTF("MEP CC Configuration is:\n");
        ICLI_PRINTF("%9s", "Inst");
        ICLI_PRINTF("%9s", "Prio");
        ICLI_PRINTF("%11s", "Rate");
        ICLI_PRINTF("%5s", "Tlv");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }

        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {
                if ((rc = vtss_appl_mep_cc_conf_get(j, &cc_config)) != VTSS_RC_OK) {
                    if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                    continue;
                }
                if (cc_config.enable) {
                    ICLI_PRINTF("%9u", j+1);
                    ICLI_PRINTF("%9u", cc_config.prio);
                    switch (cc_config.rate) {
                        case VTSS_APPL_MEP_RATE_300S:       ICLI_PRINTF("%11s", "300s");   break;
                        case VTSS_APPL_MEP_RATE_100S:       ICLI_PRINTF("%11s", "100s");   break;
                        case VTSS_APPL_MEP_RATE_10S:        ICLI_PRINTF("%11s", "10s");    break;
                        case VTSS_APPL_MEP_RATE_1S:         ICLI_PRINTF("%11s", "1s");     break;
                        case VTSS_APPL_MEP_RATE_6M:         ICLI_PRINTF("%11s", "6m");     break;
                        case VTSS_APPL_MEP_RATE_1M:         ICLI_PRINTF("%11s", "1m");     break;
                        case VTSS_APPL_MEP_RATE_6H:         ICLI_PRINTF("%11s", "6h");     break;
                        default:                            ICLI_PRINTF("%11s", "Unknown");
                    }
                    if (cc_config.tlv_enable) {
                        ICLI_PRINTF("%5s", "Tlv");
                    }
                    else {
                        ICLI_PRINTF("%5s", " ");
                    }
                    ICLI_PRINTF("\n");
                }
            }
        }
        ICLI_PRINTF("\n");
    }

    if (has_lm) {
        ICLI_PRINTF("\n");
        ICLI_PRINTF("Frame Loss Ratio (Flr) is shown in 100 * percent, i.e. 100*100*Loss/Tx.\n");
        ICLI_PRINTF("MEP LM state is:\n");
        ICLI_PRINTF("%9s", "Inst");
        ICLI_PRINTF("%9s", "Peer");
        ICLI_PRINTF("%12s", "Tx");
        ICLI_PRINTF("%12s", "Rx");
        ICLI_PRINTF("%23s", "Near Loss");
        ICLI_PRINTF("%23s", "Far Loss");
        ICLI_PRINTF("%23s", "Threshold Count");
        ICLI_PRINTF("%17s", "Near Ratio");
        ICLI_PRINTF("%17s", "Far Ratio");
        ICLI_PRINTF("%14s", "Near Ratio");
        ICLI_PRINTF("%13s", "Far Ratio");
        ICLI_PRINTF("%10s", "Intervals");
        ICLI_PRINTF("\n");
        ICLI_PRINTF("%9s", "");
        ICLI_PRINTF("%9s", "");
        ICLI_PRINTF("%12s", "");
        ICLI_PRINTF("%12s", "");
        ICLI_PRINTF("%23s", "interval/total");
        ICLI_PRINTF("%23s", "interval/total");
        ICLI_PRINTF("%23s", "near/far");
        ICLI_PRINTF("%17s", "interval/total");
        ICLI_PRINTF("%17s", "interval/total");
        ICLI_PRINTF("%14s", "min/max");
        ICLI_PRINTF("%13s", "min/max");
        ICLI_PRINTF("%10s", "elapsed");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }
        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {
                if (((rc = mep_instance_conf_get(j, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
                    mep_print_error(session_id, rc);
                    continue;
                }
                if (!config.enable)
                    continue;

                ICLI_PRINTF("%9u", j+1);
                for (k=0; k<config.peer_count; ++k) {
                    if ((rc = vtss_appl_mep_instance_peer_lm_status_get(j, config.peer_mep[k], &lm_state)) != VTSS_RC_OK) {
                        if (rc != VTSS_APPL_MEP_RC_NOT_ENABLED) {
                            mep_print_error(session_id, rc);
                        }
                        continue;
                    }
                    if (k != 0) {
                        ICLI_PRINTF("%9s", " ");
                    }

                    ICLI_PRINTF("%9u", config.peer_mep[k]);

                    char lm_str[32];
                    ICLI_PRINTF("%12u", lm_state.tx_counter);
                    ICLI_PRINTF("%12u", lm_state.rx_counter);
                    sprintf(lm_str, "%u/%u", lm_state.near_los_int_cnt, lm_state.near_los_tot_cnt);
                    ICLI_PRINTF("%23s", lm_str);
                    sprintf(lm_str, "%u/%u", lm_state.far_los_int_cnt, lm_state.far_los_tot_cnt);
                    ICLI_PRINTF("%23s", lm_str);
                    sprintf(lm_str, "%u/%u", lm_state.near_los_th_cnt, lm_state.far_los_th_cnt);
                    ICLI_PRINTF("%23s", lm_str);
                    sprintf(lm_str, "%u/%u", lm_state.near_flr_ave_int, lm_state.near_flr_ave_total);
                    ICLI_PRINTF("%17s", lm_str);
                    sprintf(lm_str, "%u/%u", lm_state.far_flr_ave_int, lm_state.far_flr_ave_total);
                    ICLI_PRINTF("%17s", lm_str);
                    sprintf(lm_str, "%u/%u", lm_state.near_flr_min, lm_state.near_flr_max);
                    ICLI_PRINTF("%14s", lm_str);
                    sprintf(lm_str, "%u/%u", lm_state.far_flr_min, lm_state.far_flr_max);
                    ICLI_PRINTF("%13s", lm_str);
                    ICLI_PRINTF("%10u", lm_state.flr_interval_elapsed);
                    ICLI_PRINTF("\n");
                }
            }
        }
        ICLI_PRINTF("\n");

        if (has_detail) {
            ICLI_PRINTF("\n");
            ICLI_PRINTF("MEP LM Configuration is:\n");
            ICLI_PRINTF("%9s", "Inst");
            ICLI_PRINTF("%6s", "Dir");
            ICLI_PRINTF("%10s", "Synth");
            ICLI_PRINTF("%9s", "Prio");
            ICLI_PRINTF("%9s", "Cast");
            ICLI_PRINTF("%10s", "Ended");
            ICLI_PRINTF("%8s", "Mep");
            ICLI_PRINTF("%9s", "Rate");
            ICLI_PRINTF("%9s", "Size");
            ICLI_PRINTF("%13s", "Flr Int.");
            ICLI_PRINTF("%13s", "Meas Int.");
            ICLI_PRINTF("%12s", "Flow Count");
            ICLI_PRINTF("%12s", "Oam Count");
            ICLI_PRINTF("%16s", "Loss Thres.");
            ICLI_PRINTF("%12s", "Test ID");
            ICLI_PRINTF("\n");

            if (inst == NULL) {
                list_cnt = 1;
            }
            else {
                list_cnt = inst->u.sr.cnt;
            }

            for (i=0; i<list_cnt; ++i) {
                if (inst == NULL) {
                    min = 1;
                    max = vtss_appl_mep_instance_max;
                }
                else {
                    min = inst->u.sr.range[i].min;
                    max = inst->u.sr.range[i].max;
                }

                if ((min == 0) || (max == 0)) {
                    ICLI_PRINTF("Invalid MEP instance number\n");
                    continue;
                }
                for (j=min-1; j<max; ++j) {
                    if (((rc = mep_instance_conf_get(j, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
                        if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                        continue;
                    }
                    if (!config.enable) {
                        continue;
                    }
                    if ((rc = vtss_appl_mep_lm_conf_get(j, &lm_config)) != VTSS_RC_OK) {
                        mep_print_error(session_id, rc);
                        continue;
                    }

                    // Print MEP ID
                    ICLI_PRINTF("%9u", j+1);

                    if (lm_config.enable || lm_config.enable_rx) {
                        ICLI_PRINTF("%6s", lm_config.enable ? "TX" : "RX");
                        ICLI_PRINTF("%10s", lm_config.synthetic ? "True" : "False");
                        ICLI_PRINTF("%9u", lm_config.prio);
                        switch (lm_config.cast) {
                            case VTSS_APPL_MEP_UNICAST:       ICLI_PRINTF("%9s", "Uni");     break;
                            case VTSS_APPL_MEP_MULTICAST:     ICLI_PRINTF("%9s", "Multi");   break;
                        }
                        switch (lm_config.ended) {
                            case VTSS_APPL_MEP_SINGEL_ENDED:      ICLI_PRINTF("%10s", "Single");   break;
                            case VTSS_APPL_MEP_DUAL_ENDED:        ICLI_PRINTF("%10s", "Dual");   break;
                            default:                              ICLI_PRINTF("%10s", "Unknown");
                        }
                        ICLI_PRINTF("%8u", lm_config.mep);
                        switch (lm_config.rate) {
                            case VTSS_APPL_MEP_RATE_300S:       ICLI_PRINTF("%9s", "300s");   break;
                            case VTSS_APPL_MEP_RATE_100S:       ICLI_PRINTF("%9s", "100s");   break;
                            case VTSS_APPL_MEP_RATE_10S:        ICLI_PRINTF("%9s", "10s");    break;
                            case VTSS_APPL_MEP_RATE_1S:         ICLI_PRINTF("%9s", "1s");     break;
                            case VTSS_APPL_MEP_RATE_6M:         ICLI_PRINTF("%9s", "6m");     break;
                            case VTSS_APPL_MEP_RATE_1M:         ICLI_PRINTF("%9s", "1m");     break;
                            case VTSS_APPL_MEP_RATE_6H:         ICLI_PRINTF("%9s", "6h");     break;
                            default:                            ICLI_PRINTF("%9s", "Unknown");
                        }
                        ICLI_PRINTF("%9u", lm_config.size);
                        ICLI_PRINTF("%13u", lm_config.flr_interval);
                        ICLI_PRINTF("%13u", lm_config.meas_interval);

                        ICLI_PRINTF("%12s", lm_config.flow_count ? "Enable" : "Disable");
                        switch (lm_config.oam_count) {
                            case VTSS_APPL_MEP_OAM_COUNT_NONE:      ICLI_PRINTF("%12s", "None");   break;
                            case VTSS_APPL_MEP_OAM_COUNT_ALL:       ICLI_PRINTF("%12s", "All");   break;
                            case VTSS_APPL_MEP_OAM_COUNT_Y1731:     ICLI_PRINTF("%12s", "Y1731");    break;
                            default:                                ICLI_PRINTF("%12s", "Unknown");
                        }
                        ICLI_PRINTF("%16u", lm_config.loss_th);
                        ICLI_PRINTF("%12u", lm_config.slm_test_id);
                    } else {
                        ICLI_PRINTF("%6s", "N/A");
                    }

                    ICLI_PRINTF("\n");
                }
            }
            ICLI_PRINTF("\n");
        }
    }

    /*
     Inst   Near Avail Count   Far Avail Count   Near Unavail Count   Far Unavail Count   Near State   Far State
        1         9876543210        9876543210           9876543210          9876543210      UNAVAIL     UNAVAIL
     */
    if (has_avail) {
        ICLI_PRINTF("\n");
        ICLI_PRINTF("MEP LM Availability state is:\n");
        ICLI_PRINTF("%9s", "Inst");
        ICLI_PRINTF("%6s", "Peer");
        ICLI_PRINTF("%19s", "Near Avail Count");
        ICLI_PRINTF("%18s", "Far Avail Count");
        ICLI_PRINTF("%21s", "Near Unavail Count");
        ICLI_PRINTF("%20s", "Far Unavail Count");
        ICLI_PRINTF("%17s", "NE Window Curr");
        ICLI_PRINTF("%17s", "FE Window Curr");
        ICLI_PRINTF("%11s", "NE State");
        ICLI_PRINTF("%11s", "FE State");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }
        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {

                if (((rc = mep_instance_conf_get(j, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
                    mep_print_error(session_id, rc);
                    continue;
                }
                if (!config.enable) {
                    continue;
                }
                if (config.mode == VTSS_APPL_MEP_MIP) {
                    continue;
                }

                if ((rc = vtss_appl_mep_lm_avail_conf_get(j, &lm_avail_config)) != VTSS_RC_OK) {
                    mep_print_error(session_id, rc);
                    continue;
                }

                for (u32 peer_idx = 0; peer_idx < config.peer_count; peer_idx++) {
                    if ((rc = vtss_appl_mep_lm_avail_status_get(j, config.peer_mep[peer_idx], &lm_avail_state)) != VTSS_RC_OK) {
                        if (rc != VTSS_APPL_MEP_RC_NOT_ENABLED) {
                            mep_print_error(session_id, rc);
                        }
                        continue;
                    }

                    ICLI_PRINTF("%9u", j+1);
                    ICLI_PRINTF("%6u", config.peer_mep[peer_idx]);

                    ICLI_PRINTF("%19u", lm_avail_state.near_cnt);
                    ICLI_PRINTF("%18u", lm_avail_state.far_cnt);
                    ICLI_PRINTF("%21u", lm_avail_state.near_un_cnt);
                    ICLI_PRINTF("%20u", lm_avail_state.far_un_cnt);

                    if (lm_avail_config.enable) {
                        ICLI_PRINTF("%17u", lm_avail_state.near_avail_cnt);
                        ICLI_PRINTF("%17u", lm_avail_state.far_avail_cnt);

                        ICLI_PRINTF("%11s", lm_avail_state.near_state == VTSS_APPL_MEP_OAM_LM_AVAIL ? "AVAIL" : "UNAVAIL");
                        ICLI_PRINTF("%11s", lm_avail_state.far_state == VTSS_APPL_MEP_OAM_LM_AVAIL ? "AVAIL" : "UNAVAIL");
                    }
                    else {
                        ICLI_PRINTF("%11s", "-");
                        ICLI_PRINTF("%11s", "-");

                        ICLI_PRINTF("%17s", "-");
                        ICLI_PRINTF("%17s", "-");
                    }

                    ICLI_PRINTF("\n");
                }
            }
        }
        ICLI_PRINTF("\n");

        /*
          Inst   FLR Threshold     Interval   Maintenance
             1            1000   9876543210      Disabled
        */
        if (has_detail) {
            ICLI_PRINTF("\n");
            ICLI_PRINTF("MEP LM Availability Configuration is:\n");
            ICLI_PRINTF("%9s", "Inst");
            ICLI_PRINTF("%16s", "FLR Threshold");
            ICLI_PRINTF("%13s", "Interval");
            ICLI_PRINTF("%14s", "Maintenance");
            ICLI_PRINTF("\n");

            if (inst == NULL) {
                list_cnt = 1;
            }
            else {
                list_cnt = inst->u.sr.cnt;
            }

            for (i=0; i<list_cnt; ++i) {
                if (inst == NULL) {
                    min = 1;
                    max = vtss_appl_mep_instance_max;
                }
                else {
                    min = inst->u.sr.range[i].min;
                    max = inst->u.sr.range[i].max;
                }

                if ((min == 0) || (max == 0)) {
                    ICLI_PRINTF("Invalid MEP instance number\n");
                    continue;
                }
                for (j=min-1; j<max; ++j) {
                    if (((rc = mep_instance_conf_get(j, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
                        if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                        continue;
                    }
                    if (!config.enable) {
                        continue;
                    }
                    if ((rc = vtss_appl_mep_lm_avail_conf_get(j, &lm_avail_config)) != VTSS_RC_OK) {
                        mep_print_error(session_id, rc);
                        continue;
                    }
                    if (!lm_avail_config.enable) {
                        continue;
                    }
                    ICLI_PRINTF("%9u", j+1);
                    ICLI_PRINTF("%16u", lm_avail_config.flr_th );
                    ICLI_PRINTF("%13u", lm_avail_config.interval);
                    ICLI_PRINTF("%14s", lm_avail_config.main ? "Enabled" : "Disabled");
                    ICLI_PRINTF("\n");
                }
            }
            ICLI_PRINTF("\n");
        }
    }

    /*
     Inst   Near Count   Far Count   Near/Far Consecutive Count
        1   9876543210  9876543210        9876543210/9876543210
     */
    if (has_lm_hli) {
        ICLI_PRINTF("\n");
        ICLI_PRINTF("MEP LM High Loss Interval state is:\n");
        ICLI_PRINTF("%9s", "Inst");
        ICLI_PRINTF("%9s", "Peer");
        ICLI_PRINTF("%13s", "Near Count");
        ICLI_PRINTF("%12s", "Far Count");
        ICLI_PRINTF("%25s", "Near/Far Consec. Count");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }
        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }

            for (j=min-1; j<max; ++j) {
                if (((rc = mep_instance_conf_get(j, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
                    mep_print_error(session_id, rc);
                    continue;
                }
                if (!config.enable)
                    continue;

                char lm_hli_str[32];

                ICLI_PRINTF("%9u", j+1);

                for (k=0; k<config.peer_count; ++k) {
                    if (k != 0) {
                        ICLI_PRINTF("%9s", " ");
                    }

                    ICLI_PRINTF("%9u", config.peer_mep[k]);

                    memset(&lm_hli_state, 0, sizeof(lm_hli_state));
                    if ((rc = vtss_appl_mep_lm_hli_status_get(j, config.peer_mep[k], &lm_hli_state)) == VTSS_RC_OK) {
                        ICLI_PRINTF("%13u", lm_hli_state.near_cnt);
                        ICLI_PRINTF("%12u", lm_hli_state.far_cnt);
                        sprintf(lm_hli_str, "%u/%u", lm_hli_state.near_con_cnt, lm_hli_state.far_con_cnt);
                        ICLI_PRINTF("%25s", lm_hli_str);
                    }
                    ICLI_PRINTF("\n");

                }
            }
        }
        ICLI_PRINTF("\n");

        /*
          Inst     Interval   FLR Threshold
             1   9876543210            1000
        */
        if (has_detail) {
            ICLI_PRINTF("\n");
            ICLI_PRINTF("MEP LM High Loss Interval Configuration is:\n");
            ICLI_PRINTF("%9s", "Inst");
            ICLI_PRINTF("%13s", "Interval");
            ICLI_PRINTF("%16s", "FLR Threshold");
            ICLI_PRINTF("\n");

            if (inst == NULL) {
                list_cnt = 1;
            }
            else {
                list_cnt = inst->u.sr.cnt;
            }

            for (i=0; i<list_cnt; ++i) {
                if (inst == NULL) {
                    min = 1;
                    max = vtss_appl_mep_instance_max;
                }
                else {
                    min = inst->u.sr.range[i].min;
                    max = inst->u.sr.range[i].max;
                }

                if ((min == 0) || (max == 0)) {
                    ICLI_PRINTF("Invalid MEP instance number\n");
                    continue;
                }
                for (j=min-1; j<max; ++j) {
                    if (((rc = mep_instance_conf_get(j, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
                        if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                        continue;
                    }
                    if (!config.enable) {
                        continue;
                    }
                    if ((rc = vtss_appl_mep_lm_hli_conf_get(j, &lm_hli_config)) != VTSS_RC_OK) {
                        mep_print_error(session_id, rc);
                        continue;
                    }
                    if (!lm_hli_config.enable) {
                        continue;
                    }
                    ICLI_PRINTF("%9u", j+1);
                    ICLI_PRINTF("%13u", lm_hli_config.con_int);
                    ICLI_PRINTF("%16u", lm_hli_config.flr_th);
                    ICLI_PRINTF("\n");
                }
            }
            ICLI_PRINTF("\n");
        }
    }

    if (has_dm) {
        print_dm_state_header(session_id);

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }

        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {
                if ((rc = vtss_appl_mep_dm_status_get(j, &dmr_state, &dm1_state_far_to_near, &dm1_state_near_to_far)) != VTSS_RC_OK) {
                    if (rc != VTSS_APPL_MEP_RC_NOT_ENABLED && rc != VTSS_APPL_MEP_RC_NOT_CREATED) {
                        mep_print_error(session_id, rc);
                    }
                    continue;
                }
                ICLI_PRINTF("%-12s", "1-Way FtoN");
                print_dm_state(session_id, j+1, &dm1_state_far_to_near);
                ICLI_PRINTF("%-12s", "1-Way NtoF");
                print_dm_state(session_id, j+1, &dm1_state_near_to_far);
                ICLI_PRINTF("%-12s", "2-Way");
                print_dm_state(session_id, j+1, &dmr_state);
            }
        }
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }

        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }
            for (j=min-1; j<max; ++j) {
                if ((rc = vtss_appl_mep_dm_conf_get(j, &dm_config)) != VTSS_RC_OK) {
                    if (rc != VTSS_APPL_MEP_RC_NOT_ENABLED && rc != VTSS_APPL_MEP_RC_NOT_CREATED) {
                        mep_print_error(session_id, rc);
                    }
                    continue;
                }

                if ((rc = vtss_appl_mep_dm_status_get(j, &dmr_state, &dm1_state_far_to_near, &dm1_state_near_to_far)) != VTSS_RC_OK) {
                    if (rc != VTSS_APPL_MEP_RC_NOT_ENABLED && rc != VTSS_APPL_MEP_RC_NOT_CREATED) {
                        mep_print_error(session_id, rc);
                    }
                    continue;
                }
                print_dm_bin_header(session_id, dm_config.num_of_bin_fd);
                ICLI_PRINTF("Frame Delay\n");
                ICLI_PRINTF("%-12s", "1-Way FtoN");
                print_bin_state(session_id, j+1, dm1_state_far_to_near.fd_bin, dm_config.num_of_bin_fd);
                ICLI_PRINTF("%-12s", "1-Way NtoF");
                print_bin_state(session_id, j+1, dm1_state_near_to_far.fd_bin, dm_config.num_of_bin_fd);
                ICLI_PRINTF("%-12s", "2-Way");
                print_bin_state(session_id, j+1, dmr_state.fd_bin, dm_config.num_of_bin_fd);

                print_dm_bin_header(session_id, dm_config.num_of_bin_ifdv);
                ICLI_PRINTF("Inter-Frame Delay Variation\n");
                ICLI_PRINTF("%-12s", "1-Way FtoN");
                print_bin_state(session_id, j+1, dm1_state_far_to_near.ifdv_bin, dm_config.num_of_bin_ifdv);
                ICLI_PRINTF("%-12s", "1-Way NtoF");
                print_bin_state(session_id, j+1, dm1_state_near_to_far.ifdv_bin, dm_config.num_of_bin_ifdv);
                ICLI_PRINTF("%-12s", "2-Way");
                print_bin_state(session_id, j+1, dmr_state.ifdv_bin, dm_config.num_of_bin_ifdv);
            }
        }
        ICLI_PRINTF("\n");

        if (has_detail) {
            ICLI_PRINTF("\n");
            ICLI_PRINTF("MEP DM Configuration is:\n");
            ICLI_PRINTF("%9s", "Inst");
            ICLI_PRINTF("%9s", "Prio");
            ICLI_PRINTF("%9s", "Cast");
            ICLI_PRINTF("%8s", "Mep");
            ICLI_PRINTF("%10s", "Ended");
            ICLI_PRINTF("%7s", "Proto");
            ICLI_PRINTF("%7s", "Calc");
            ICLI_PRINTF("%10s", "Interval");
            ICLI_PRINTF("%8s", "Last-n");
            ICLI_PRINTF("%7s", "Unit");
            ICLI_PRINTF("%9s", "Sync");
            ICLI_PRINTF("%10s", "Overflow");
            ICLI_PRINTF("%8s", "BinsFD");
            ICLI_PRINTF("%10s", "BinsIFDV");
            ICLI_PRINTF("%12s", "Threshold");
            ICLI_PRINTF("\n");

            if (inst == NULL) {
                list_cnt = 1;
            }
            else {
                list_cnt = inst->u.sr.cnt;
            }

            for (i=0; i<list_cnt; ++i) {
                if (inst == NULL) {
                    min = 1;
                    max = vtss_appl_mep_instance_max;
                }
                else {
                    min = inst->u.sr.range[i].min;
                    max = inst->u.sr.range[i].max;
                }

                if ((min == 0) || (max == 0)) {
                    ICLI_PRINTF("Invalid MEP instance number\n");
                    continue;
                }
                for (j=min-1; j<max; ++j) {
                    if ((rc = vtss_appl_mep_dm_conf_get(j, &dm_config)) != VTSS_RC_OK) {
                        if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                        continue;
                    }
                    if (dm_config.enable) {
                        ICLI_PRINTF("%9u", j+1);
                        ICLI_PRINTF("%9u", dm_config.prio);
                        switch (dm_config.cast) {
                            case VTSS_APPL_MEP_UNICAST:     ICLI_PRINTF("%9s", "Uni");     break;
                            case VTSS_APPL_MEP_MULTICAST:   ICLI_PRINTF("%9s", "Multi");   break;
                            default:                        ICLI_PRINTF("%9s", "ND");
                        }
                        ICLI_PRINTF("%8u", dm_config.mep);
                        switch (dm_config.ended) {
                            case VTSS_APPL_MEP_DUAL_ENDED:     ICLI_PRINTF("%10s", "Dual");   break;
                            case VTSS_APPL_MEP_SINGEL_ENDED:   ICLI_PRINTF("%10s", "Single"); break;
                            default:                           ICLI_PRINTF("%10s", "ND");
                        }
                        if (!dm_config.proprietary) {
                            ICLI_PRINTF("%7s", "Std");
                        } else {
                            ICLI_PRINTF("%7s", "Prop");
                        }
                        switch (dm_config.calcway) {
                            case VTSS_APPL_MEP_RDTRP:       ICLI_PRINTF("%7s", "Rdtrp");   break;
                            case VTSS_APPL_MEP_FLOW:        ICLI_PRINTF("%7s", "Flow");   break;
                            default:                        ICLI_PRINTF("%7s", "ND");
                        }
                        ICLI_PRINTF("%10u", dm_config.interval);
                        ICLI_PRINTF("%8u", dm_config.lastn);
                        switch (dm_config.tunit) {
                            case VTSS_APPL_MEP_US:          ICLI_PRINTF("%7s", "us");   break;
                            case VTSS_APPL_MEP_NS:          ICLI_PRINTF("%7s", "ns");   break;
                            default:                        ICLI_PRINTF("%7s", "ND");
                        }
                        ICLI_PRINTF("%9s", dm_config.synchronized ? "Enable" : "Disable");
                        switch (dm_config.overflow_act) {
                            case VTSS_APPL_MEP_DISABLE:     ICLI_PRINTF("%10s", "keep");    break;
                            case VTSS_APPL_MEP_CONTINUE:    ICLI_PRINTF("%10s", "reset");   break;
                            default:                        ICLI_PRINTF("%10s", "ND");
                        }
                        ICLI_PRINTF("%8u", dm_config.num_of_bin_fd);
                        ICLI_PRINTF("%10u", dm_config.num_of_bin_ifdv);
                        ICLI_PRINTF("%12u", dm_config.m_threshold);
                        ICLI_PRINTF("\n");
                    }
                }
            }
            ICLI_PRINTF("\n");
        }
    }

    if (has_lt) {
        ICLI_PRINTF("MEP LT state is:\n");
        ICLI_PRINTF("%9s", "Inst");
        ICLI_PRINTF("%19s", "Transaction ID");
        ICLI_PRINTF("%8s", "Ttl");
        ICLI_PRINTF("%9s", "Mode");
        ICLI_PRINTF("%14s", "Direction");
        ICLI_PRINTF("%12s", "Forwarded");
        ICLI_PRINTF("%10s", "relay");
        ICLI_PRINTF("%22s", "Last MAC");
        ICLI_PRINTF("%22s", "Next MAC");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }

        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {
                if ((rc = vtss_appl_mep_lt_status_get(j, &lt_state)) != VTSS_RC_OK) {
                    if (rc != VTSS_APPL_MEP_RC_NOT_ENABLED) {
                        mep_print_error(session_id, rc);
                    }
                    continue;
                }
                if (lt_state.transaction_cnt != 0) {
                    ICLI_PRINTF("%9u", j+1);
                    for (t=0; t<lt_state.transaction_cnt; ++t) {
                        if (t != 0) {
                            ICLI_PRINTF("%9s", " ");
                        }
                        ICLI_PRINTF("%19u", lt_state.transaction[t].transaction_id);

                        for (k=0; k<lt_state.transaction[t].reply_cnt; ++k) {
                            if (k != 0) {
                                ICLI_PRINTF("%28s"," ");
                            }
                            ICLI_PRINTF("%8u", lt_state.transaction[t].reply[k].ttl);
                            switch (lt_state.transaction[t].reply[k].mode) {
                                case VTSS_APPL_MEP_MEP:  ICLI_PRINTF("%9s", "Mep");     break;
                                case VTSS_APPL_MEP_MIP:  ICLI_PRINTF("%9s", "Mip");     break;
                            }
                            switch (lt_state.transaction[t].reply[k].direction) {
                                case VTSS_APPL_MEP_DOWN:   ICLI_PRINTF("%14s", "Down");     break;
                                case VTSS_APPL_MEP_UP:     ICLI_PRINTF("%14s", "Up");      break;
                            }
                            ICLI_PRINTF("%12s", lt_state.transaction[t].reply[k].forwarded ? "Yes" : "No");
                            switch (lt_state.transaction[t].reply[k].relay_action) {
                                case VTSS_APPL_MEP_RELAY_UNKNOWN:   ICLI_PRINTF("%10s", "Unknown");     break;
                                case VTSS_APPL_MEP_RELAY_HIT:       ICLI_PRINTF("%10s", "MAC");         break;
                                case VTSS_APPL_MEP_RELAY_FDB:       ICLI_PRINTF("%10s", "FDB");         break;
                                case VTSS_APPL_MEP_RELAY_MPDB:      ICLI_PRINTF("%10s", "CCM DB");      break;
                            }
                            ICLI_PRINTF("%5s%02X-%02X-%02X-%02X-%02X-%02X", " ", lt_state.transaction[t].reply[k].last_egress_mac.addr[0], lt_state.transaction[t].reply[k].last_egress_mac.addr[1], lt_state.transaction[t].reply[k].last_egress_mac.addr[2], lt_state.transaction[t].reply[k].last_egress_mac.addr[3], lt_state.transaction[t].reply[k].last_egress_mac.addr[4], lt_state.transaction[t].reply[k].last_egress_mac.addr[5]);
                            ICLI_PRINTF("%5s%02X-%02X-%02X-%02X-%02X-%02X", " ", lt_state.transaction[t].reply[k].next_egress_mac.addr[0], lt_state.transaction[t].reply[k].next_egress_mac.addr[1], lt_state.transaction[t].reply[k].next_egress_mac.addr[2], lt_state.transaction[t].reply[k].next_egress_mac.addr[3], lt_state.transaction[t].reply[k].next_egress_mac.addr[4], lt_state.transaction[t].reply[k].next_egress_mac.addr[5]);
                            ICLI_PRINTF("\n");
                        }
                        if (lt_state.transaction[t].reply_cnt == 0) {
                            ICLI_PRINTF("\n");
                        }
                    }
                }
            }
        }
        ICLI_PRINTF("\n");

        if (has_detail) {
            ICLI_PRINTF("\n");
            ICLI_PRINTF("MEP LT Configuration is:\n");
            ICLI_PRINTF("%9s", "Inst");
            ICLI_PRINTF("%9s", "Prio");
            ICLI_PRINTF("%8s", "Mep");
            ICLI_PRINTF("%22s", "MAC");
            ICLI_PRINTF("%8s", "Ttl");
            ICLI_PRINTF("\n");

            if (inst == NULL) {
                list_cnt = 1;
            }
            else {
                list_cnt = inst->u.sr.cnt;
            }

            for (i=0; i<list_cnt; ++i) {
                if (inst == NULL) {
                    min = 1;
                    max = vtss_appl_mep_instance_max;
                }
                else {
                    min = inst->u.sr.range[i].min;
                    max = inst->u.sr.range[i].max;
                }

                if ((min == 0) || (max == 0)) {
                    ICLI_PRINTF("Invalid MEP instance number\n");
                    continue;
                }
                for (j=min-1; j<max; ++j) {
                    if ((rc = vtss_appl_mep_lt_conf_get(j, &lt_config)) != VTSS_RC_OK) {
                        if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                        continue;
                    }
                    if (lt_config.enable) {
                        ICLI_PRINTF("%9u", j+1);
                        ICLI_PRINTF("%9u", lt_config.prio);
                        ICLI_PRINTF("%8u", lt_config.mep);
                        ICLI_PRINTF("     %02X-%02X-%02X-%02X-%02X-%02X", lt_config.mac.addr[0], lt_config.mac.addr[1], lt_config.mac.addr[2], lt_config.mac.addr[3], lt_config.mac.addr[4], lt_config.mac.addr[5]);
                        ICLI_PRINTF("%8u", lt_config.ttl);
                        ICLI_PRINTF("\n");
                    }
                }
            }
            ICLI_PRINTF("\n");
        }
    }

    if (has_lb) {
        ICLI_PRINTF("MEP LB state is:\n");
        ICLI_PRINTF("%9s", "Inst");
        ICLI_PRINTF("%19s", "Transaction ID");
        ICLI_PRINTF("%11s", "TX LBM");
        ICLI_PRINTF("%39s", "Peer");
        ICLI_PRINTF("%12s", "Received");
        ICLI_PRINTF("%14s", "Out Of Order");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }

        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {
                if ((rc = vtss_appl_mep_lb_status_get(j, &lb_state)) != VTSS_RC_OK) {
                    if ((rc != VTSS_APPL_MEP_RC_NOT_ENABLED) && (rc != VTSS_APPL_MEP_RC_INVALID_INSTANCE) && (rc != VTSS_APPL_MEP_RC_NOT_CREATED)) {
                        mep_print_error(session_id, rc);
                    }
                    continue;
                }
                if (((rc = mep_instance_conf_get(j, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
                    if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                    continue;
                }
                if (lb_state.reply_cnt != 0) {
                    ICLI_PRINTF("%9u", j+1);
                    ICLI_PRINTF("%19u", lb_state.transaction_id);
                    ICLI_PRINTF("%11" PRIu64, lb_state.lbm_transmitted);
                    for (k=0; k<lb_state.reply_cnt; ++k) {
                        if (k != 0) {
                            ICLI_PRINTF("%39s", " ");
                        }
                        if (MPLS_DOMAIN(config.domain)) {
                            if (lb_state.reply[k].mep_mip_id[0] == 2) { /* 2-byte MEP ID */
                                ICLI_PRINTF("%28smepId:%5u", "", (lb_state.reply[k].mep_mip_id[1] << 8) | lb_state.reply[k].mep_mip_id[2]);
                            } else if (lb_state.reply[k].mep_mip_id[0] == 3) { /* ICC-based MIP ID */
                                char str[7], ipStr[16];
                                u32 nodeId, ifNum;
                                u32 ii;
                                memcpy(str, &lb_state.reply[k].mep_mip_id[1], 6);
                                str[6] = 0;
                                nodeId = ((lb_state.reply[k].mep_mip_id[7] << 24) | (lb_state.reply[k].mep_mip_id[8] << 16) |
                                          (lb_state.reply[k].mep_mip_id[9] <<  8) | lb_state.reply[k].mep_mip_id[10]);
                                (void)node_id_to_str(nodeId, ipStr);
                                ifNum = ((lb_state.reply[k].mep_mip_id[11] << 24) | (lb_state.reply[k].mep_mip_id[12] << 16) |
                                         (lb_state.reply[k].mep_mip_id[13] <<  8) | lb_state.reply[k].mep_mip_id[14]);

                                for (ii = 0; ii < (15 - strlen(ipStr)); ++ii) {
                                    ICLI_PRINTF(" ");
                                }
                                ICLI_PRINTF("  mip:%6s %s %10u", str, ipStr, ifNum);
                            } else {
                                ICLI_PRINTF("%39s", "");
                            }
                        } else {
                            ICLI_PRINTF("%22s%02X-%02X-%02X-%02X-%02X-%02X", " ", lb_state.reply[k].mac.addr[0], lb_state.reply[k].mac.addr[1], lb_state.reply[k].mac.addr[2], lb_state.reply[k].mac.addr[3], lb_state.reply[k].mac.addr[4], lb_state.reply[k].mac.addr[5]);
                        }
                        ICLI_PRINTF("%12" PRIu64, lb_state.reply[k].lbr_received);
                        ICLI_PRINTF("%14" PRIu64 "\n", lb_state.reply[k].out_of_order);
                    }
                }
            }
        }
        ICLI_PRINTF("\n");

        if (has_detail) {
            ICLI_PRINTF("\n");
            ICLI_PRINTF("MEP LB Configuration is:\n");
            ICLI_PRINTF("%9s", "Inst");
            ICLI_PRINTF("%9s", "Dei");
            ICLI_PRINTF("%9s", "Prio");
            ICLI_PRINTF("%9s", "Cast");
            ICLI_PRINTF("%8s", "Mep");
            ICLI_PRINTF("%22s", "MAC");
            ICLI_PRINTF("%11s", "ToSend");
            ICLI_PRINTF("%9s", "Size");
            ICLI_PRINTF("%13s", "Interval");
            ICLI_PRINTF("\n");

            if (inst == NULL) {
                list_cnt = 1;
            }
            else {
                list_cnt = inst->u.sr.cnt;
            }

            for (i=0; i<list_cnt; ++i) {
                if (inst == NULL) {
                    min = 1;
                    max = vtss_appl_mep_instance_max;
                }
                else {
                    min = inst->u.sr.range[i].min;
                    max = inst->u.sr.range[i].max;
                }

                if ((min == 0) || (max == 0)) {
                    ICLI_PRINTF("Invalid MEP instance number\n");
                    continue;
                }
                for (j=min-1; j<max; ++j) {
                    if ((rc = vtss_appl_mep_lb_conf_get(j, &lb_config)) != VTSS_RC_OK) {
                        if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                        continue;
                    }
                    if (lb_config.enable) {
                        ICLI_PRINTF("%9u", j+1);
                        ICLI_PRINTF("%9s", lb_config.dei ? "Enable" : "Disable");
                        ICLI_PRINTF("%9u", lb_config.prio);
                        switch (lb_config.cast) {
                            case VTSS_APPL_MEP_UNICAST:       ICLI_PRINTF("%9s", "Uni");     break;
                            case VTSS_APPL_MEP_MULTICAST:     ICLI_PRINTF("%9s", "Multi");   break;
                        }
                        ICLI_PRINTF("%8u", lb_config.mep);
                        ICLI_PRINTF("     %02X-%02X-%02X-%02X-%02X-%02X", lb_config.mac.addr[0], lb_config.mac.addr[1], lb_config.mac.addr[2], lb_config.mac.addr[3], lb_config.mac.addr[4], lb_config.mac.addr[5]);
                        ICLI_PRINTF("%11u", lb_config.to_send);
                        ICLI_PRINTF("%9u", lb_config.size);
                        ICLI_PRINTF("%13u", lb_config.interval);
                        ICLI_PRINTF("\n");
                    }
                }
            }
            ICLI_PRINTF("\n");
        }
    }

    if (has_tst) {
        ICLI_PRINTF("MEP TST state is:\n");
        ICLI_PRINTF("%9s", "Inst");
        ICLI_PRINTF("%19s", "TX frame count");
        ICLI_PRINTF("%19s", "RX frame count");
        ICLI_PRINTF("%12s", "RX rate");
        ICLI_PRINTF("%14s", "Test time");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }

        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {
                if ((rc = vtss_appl_mep_tst_status_get(j, &tst_state)) != VTSS_RC_OK) {
                    if ((rc != VTSS_APPL_MEP_RC_NOT_ENABLED) && (rc != VTSS_APPL_MEP_RC_INVALID_INSTANCE) && (rc != VTSS_APPL_MEP_RC_NOT_CREATED)) {
                        mep_print_error(session_id, rc);
                    }
                    continue;
                }
                ICLI_PRINTF("%9u", j+1);
                ICLI_PRINTF("%19" PRIu64, tst_state.tx_counter);
                ICLI_PRINTF("%19" PRIu64, tst_state.rx_counter);
                ICLI_PRINTF("%12u", tst_state.rx_rate);
                ICLI_PRINTF("%14u\n", tst_state.time);
            }
        }
        ICLI_PRINTF("\n");

        if (has_detail) {
            ICLI_PRINTF("\n");
            ICLI_PRINTF("MEP TST Configuration is:\n");
            ICLI_PRINTF("%9s", "Inst");
            ICLI_PRINTF("%9s", "Dei");
            ICLI_PRINTF("%9s", "Prio");
            ICLI_PRINTF("%8s", "Mep");
            ICLI_PRINTF("%9s", "rate");
            ICLI_PRINTF("%9s", "Size");
            ICLI_PRINTF("%13s", "Pattern");
            ICLI_PRINTF("%11s", "Sequence");
            ICLI_PRINTF("%9s", "tx");
            ICLI_PRINTF("%9s", "rx");
            ICLI_PRINTF("\n");

            if (inst == NULL) {
                list_cnt = 1;
            }
            else {
                list_cnt = inst->u.sr.cnt;
            }

            for (i=0; i<list_cnt; ++i) {
                if (inst == NULL) {
                    min = 1;
                    max = vtss_appl_mep_instance_max;
                }
                else {
                    min = inst->u.sr.range[i].min;
                    max = inst->u.sr.range[i].max;
                }

                if ((min == 0) || (max == 0)) {
                    ICLI_PRINTF("Invalid MEP instance number\n");
                    continue;
                }
                for (j=min-1; j<max; ++j) {
                    if ((rc = vtss_appl_mep_tst_common_conf_get(j, &tst_common_config)) != VTSS_RC_OK) {
                        if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                        continue;
                    }
                    enable = FALSE;
                    for (k=0; k<VTSS_APPL_MEP_PRIO_MAX; ++k) {  /* Search for enabled TX TST */
                        for (l=0; l<VTSS_APPL_MEP_TEST_MAX; ++l) {
                            if ((rc = vtss_appl_mep_tst_prio_conf_get(j, k, l, &test_prio_config)) != VTSS_RC_OK) {
                                if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                                continue;
                            }
                            if (test_prio_config.enable) {
                                enable = TRUE;
                                ICLI_PRINTF("%9u", j+1);
                                ICLI_PRINTF("%9s", (test_prio_config.dp == VTSS_APPL_MEP_DP_GREEN) ? "Disable" : "Enable");
                                ICLI_PRINTF("%9u", k);
                                ICLI_PRINTF("%8u", tst_common_config.mep);
                                ICLI_PRINTF("%9u", test_prio_config.rate);
                                ICLI_PRINTF("%9u", test_prio_config.size);
                                switch (test_prio_config.pattern) {
                                    case VTSS_APPL_MEP_PATTERN_ALL_ZERO:    ICLI_PRINTF("%13s", "All zero");  break;
                                    case VTSS_APPL_MEP_PATTERN_ALL_ONE:     ICLI_PRINTF("%13s", "All one");   break;
                                    case VTSS_APPL_MEP_PATTERN_0XAA:        ICLI_PRINTF("%13s", "0xAA");      break;
                                }
                                ICLI_PRINTF("%11s", tst_common_config.sequence ? "Enable" : "Disable");
                                ICLI_PRINTF("%9s", test_prio_config.enable ? "Enable" : "Disable");
                                ICLI_PRINTF("%9s", tst_common_config.enable_rx ? "Enable" : "Disable");
                                ICLI_PRINTF("\n");
                            }
                        }
                    }
                    if (!enable && tst_common_config.enable_rx) {
                        ICLI_PRINTF("%9u", j + 1);
                        ICLI_PRINTF("%8s", "-");
                        ICLI_PRINTF("%9s", "-");
                        ICLI_PRINTF("%9u", tst_common_config.mep);
                        ICLI_PRINTF("%8s", "-");
                        ICLI_PRINTF("%9s", "-");
                        ICLI_PRINTF("%13s", "-");
                        ICLI_PRINTF("%12s", tst_common_config.sequence ? "Enable" : "Disable");
                        ICLI_PRINTF("%9s", "Disable");
                        ICLI_PRINTF("%9s", tst_common_config.enable_rx ? "Enable" : "Disable");
                        ICLI_PRINTF("\n");
                    }
                }
            }
            ICLI_PRINTF("\n");
        }
    }

    if (has_aps && has_detail) {
        ICLI_PRINTF("\n");
        ICLI_PRINTF("MEP APS Configuration is:\n");
        ICLI_PRINTF("%9s", "Inst");
        ICLI_PRINTF("%9s", "Prio");
        ICLI_PRINTF("%9s", "Cast");
        ICLI_PRINTF("%9s", "Type");
        ICLI_PRINTF("%9s", "Octet");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }

        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {
                if ((rc = vtss_appl_mep_aps_conf_get(j, &aps_config)) != VTSS_RC_OK) {
                    if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                    continue;
                }
                if (aps_config.enable) {
                    ICLI_PRINTF("%9u", j+1);
                    ICLI_PRINTF("%9u", aps_config.prio);
                    switch (aps_config.cast) {
                        case VTSS_APPL_MEP_UNICAST:       ICLI_PRINTF("%9s", "Uni");     break;
                        case VTSS_APPL_MEP_MULTICAST:     ICLI_PRINTF("%9s", "Multi");   break;
                    }
                    switch (aps_config.type) {
                        case VTSS_APPL_MEP_INV_APS:   ICLI_PRINTF("%9s", "none");   break;
                        case VTSS_APPL_MEP_L_APS:     ICLI_PRINTF("%9s", "laps");   break;
                        case VTSS_APPL_MEP_R_APS:     ICLI_PRINTF("%9s", "raps");   break;
                    }
                    ICLI_PRINTF("%9X", aps_config.raps_octet);
                    ICLI_PRINTF("\n");
                }
            }
        }
        ICLI_PRINTF("\n");
    }

    if (has_client && has_detail) {
        ICLI_PRINTF("\n");
        ICLI_PRINTF("MEP CLIENT Configuration is:\n");
        ICLI_PRINTF("%9s", "Inst");
        ICLI_PRINTF("%11s", "Domain");
        ICLI_PRINTF("%11s", "Client");
        ICLI_PRINTF("%10s", "Flows");
        ICLI_PRINTF("%13s", "AIS Prio");
        ICLI_PRINTF("%13s", "LCK Prio");
        ICLI_PRINTF("%10s", "Level");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }
        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {
                if (((rc = mep_instance_conf_get(j, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
                    if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                    continue;
                }
                if (!config.enable) {
                    continue;
                }
                if (config.mode == VTSS_APPL_MEP_MIP) {
                    continue;
                }
                if ((rc = mep_client_conf_get(j, &client_config)) != VTSS_RC_OK) {
                    mep_print_error(session_id, rc);
                    continue;
                }
                ICLI_PRINTF("%9u", j+1);
                switch (config.domain) {
                    case VTSS_APPL_MEP_PORT:        ICLI_PRINTF("%11s", "Port");    break;
//                  case VTSS_APPL_MEP_ESP:         ICLI_PRINTF("%11s", "ESP");        break;
                    case VTSS_APPL_MEP_EVC:         ICLI_PRINTF("%11s", "Evc");        break;
                    case VTSS_APPL_MEP_VLAN:        ICLI_PRINTF("%11s", "Vlan");       break;
                    case VTSS_APPL_MEP_MPLS_LINK:   ICLI_PRINTF("%11s", "MplsLink");   break;
                    case VTSS_APPL_MEP_MPLS_TUNNEL: ICLI_PRINTF("%11s", "MplsTunnel"); break;
                    case VTSS_APPL_MEP_MPLS_PW:     ICLI_PRINTF("%11s", "MplsPW");     break;
                    case VTSS_APPL_MEP_MPLS_LSP:    ICLI_PRINTF("%11s", "MplsLSP");    break;
                }
                for (k=0; k<client_config.flow_count; ++k) {
                    if (k != 0) {
                        ICLI_PRINTF("%20s"," ");
                    }
                    switch (client_config.domain[k]) {
                        case VTSS_APPL_MEP_PORT:        ICLI_PRINTF("%11s", "Invalid");    break;
//                      case VTSS_APPL_MEP_ESP:         ICLI_PRINTF("%11s", "Invalid");    break;
                        case VTSS_APPL_MEP_EVC:         ICLI_PRINTF("%11s", "Evc");        break;
                        case VTSS_APPL_MEP_VLAN:        ICLI_PRINTF("%11s", "Vlan");    break;
                        case VTSS_APPL_MEP_MPLS_LINK:   ICLI_PRINTF("%11s", "Invalid");    break;
                        case VTSS_APPL_MEP_MPLS_TUNNEL: ICLI_PRINTF("%11s", "Invalid");    break;
                        case VTSS_APPL_MEP_MPLS_PW:     ICLI_PRINTF("%11s", "Invalid");    break;
                        case VTSS_APPL_MEP_MPLS_LSP:    ICLI_PRINTF("%11s", "MplsLSP");    break;
                    }
                    if (client_config.domain[k] == VTSS_APPL_MEP_EVC) {
                        ICLI_PRINTF("%10u", client_config.flows[k]+1);
                    } else {
                        ICLI_PRINTF("%10u", client_config.flows[k]);
                    }
                    if (client_config.ais_prio[k] == VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST) {
                        ICLI_PRINTF("%13s", "Highest");
                    } else {
                        ICLI_PRINTF("%13u", client_config.ais_prio[k]);
                    }
                    if (client_config.lck_prio[k] == VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST) {
                        ICLI_PRINTF("%13s", "Highest");
                    } else {
                        ICLI_PRINTF("%13u", client_config.lck_prio[k]);
                    }
                    ICLI_PRINTF("%10u", client_config.level[k]);
                    ICLI_PRINTF("\n");
                }
                if (client_config.flow_count == 0) {
                    ICLI_PRINTF("\n");
                }
            }
        }
        ICLI_PRINTF("\n");
    }

    if (has_ais && has_detail) {
        ICLI_PRINTF("\n");
        ICLI_PRINTF("MEP AIS Configuration is:\n");
        ICLI_PRINTF("%9s", "Inst");
        ICLI_PRINTF("%11s", "Rate");
        ICLI_PRINTF("%15s","Protection");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }
        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {
                if ((rc = vtss_appl_mep_ais_conf_get(j, &ais_config)) != VTSS_RC_OK) {
                    if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                    continue;
                }
                if (ais_config.enable) {
                    ICLI_PRINTF("%9u", j+1);
                    switch (ais_config.rate) {
                        case VTSS_APPL_MEP_RATE_1S:         ICLI_PRINTF("%11s", "1s");     break;
                        case VTSS_APPL_MEP_RATE_1M:         ICLI_PRINTF("%11s", "1m");     break;
                        default:                            ICLI_PRINTF("%11s", "Unknown");
                    }
                    if (ais_config.protection) {
                        ICLI_PRINTF("%15s", "Enable");
                    }
                    else {
                        ICLI_PRINTF("%15s", "Disable");
                    }
                    ICLI_PRINTF("\n");
                }
            }
        }
        ICLI_PRINTF("\n");
    }

    if (has_lck && has_detail) {
        ICLI_PRINTF("\n");
        ICLI_PRINTF("MEP LCK Configuration is:\n");
        ICLI_PRINTF("%9s", "Inst");
        ICLI_PRINTF("%11s", "Rate");
        ICLI_PRINTF("\n");

        if (inst == NULL) {
            list_cnt = 1;
        }
        else {
            list_cnt = inst->u.sr.cnt;
        }
        for (i=0; i<list_cnt; ++i) {
            if (inst == NULL) {
                min = 1;
                max = vtss_appl_mep_instance_max;
            }
            else {
                min = inst->u.sr.range[i].min;
                max = inst->u.sr.range[i].max;
            }

            if ((min == 0) || (max == 0)) {
                ICLI_PRINTF("Invalid MEP instance number\n");
                continue;
            }
            for (j=min-1; j<max; ++j) {
                if ((rc = vtss_appl_mep_lck_conf_get(j, &lck_config)) != VTSS_RC_OK) {
                    if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) mep_print_error(session_id, rc);
                    continue;
                }
                if (lck_config.enable) {
                    ICLI_PRINTF("%9u", j+1);
                    switch (lck_config.rate) {
                        case VTSS_APPL_MEP_RATE_1S:         ICLI_PRINTF("%11s", "1s");     break;
                        case VTSS_APPL_MEP_RATE_1M:         ICLI_PRINTF("%11s", "1m");     break;
                        default:                            ICLI_PRINTF("%11s", "Unknown");
                    }
                    ICLI_PRINTF("\n");
                }
            }
        }
        ICLI_PRINTF("\n");
    }

    if (has_bfd) {










































































































































































































































        ICLI_PRINTF("BFD not supported\n");

    }
    if (has_rt) {







































































































































































































        ICLI_PRINTF("RT not supported\n");

    }
    return TRUE;
}


BOOL mep_clear_mep(i32 session_id, u32 inst,
                   BOOL has_lm, BOOL has_dm, BOOL has_lb, BOOL has_tst, BOOL has_bfd,
                   BOOL clear_tx, BOOL clear_rx)
{
    mesa_rc rc = VTSS_RC_OK;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }

    // Default is to clear both directions unless user specifies anything else
    vtss_appl_mep_clear_dir direction = VTSS_APPL_MEP_CLEAR_DIR_BOTH;
    if (clear_tx) {
        direction = VTSS_APPL_MEP_CLEAR_DIR_TX;
    } else if (clear_rx) {
        direction = VTSS_APPL_MEP_CLEAR_DIR_RX;
    }

    if (has_lm) {
        rc = vtss_appl_mep_lm_status_clear_dir(inst - 1, direction);
    }

    if (has_dm) {
        rc = vtss_appl_mep_dm_status_clear(inst - 1);
    }

    if (has_lb) {
        rc = vtss_appl_mep_lb_status_clear(inst - 1);
    }

    if (has_tst) {
        rc = vtss_appl_mep_tst_status_clear(inst - 1);
    }

    if (has_bfd) {
        rc = vtss_appl_mep_g8113_2_bfd_clear_stats(inst - 1);
    }

    if (rc != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
    }

    return TRUE;
}

BOOL mep_mep(i32 session_id, u32 inst,
             BOOL has_mip, BOOL has_up, BOOL has_down, BOOL has_port, BOOL has_evc, BOOL has_vlan, BOOL has_tp_link, BOOL has_tunnel_tp, BOOL has_pw, BOOL has_lsp,
             BOOL has_vid, u32 vid, BOOL has_flow, u32 flow, u32 level, BOOL has_interface, icli_switch_port_range_t port)
{
    mep_conf_t                    config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);


    if (has_tp_link || has_tunnel_tp || has_pw || has_lsp) {
        ICLI_PRINTF("MPLS-TP not supported\n");
        return FALSE;
    }


    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        if (rc != VTSS_APPL_MEP_RC_NOT_CREATED) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    if (config.enable) {
        ICLI_PRINTF("MEP instance is already created - must be deleted first\n");
        return FALSE;
    }
    if ((has_evc || has_vlan) && !has_flow) {
        ICLI_PRINTF("EVC and VLAN domain MEP must have 'flow'\n");
        return FALSE;
    }



























    /* mep <inst:uint> [mip] {up|down} domain {port|evc|vlan|tp-link|tunnel-tp|pw|lsp} [vid <vid:vlan_id>] flow <flow:uint> level <level:0-7> [interface <port:port_type_id>] */
    config = def_conf.config; /* Initialize config */
    config.enable = TRUE;
    if (has_mip) {
        config.mode = VTSS_APPL_MEP_MIP;
    }
    if (has_up || has_down) {
        config.direction = has_up ? VTSS_APPL_MEP_UP :
                           has_down ? VTSS_APPL_MEP_DOWN : VTSS_APPL_MEP_UP;
    }
    if (has_port || has_evc || has_vlan) {
        config.domain = has_port ? VTSS_APPL_MEP_PORT :
                        has_evc ? VTSS_APPL_MEP_EVC :
                        has_vlan ? VTSS_APPL_MEP_VLAN : VTSS_APPL_MEP_PORT;
    }










    config.level = level;
    config.port = port.begin_iport;

    if (has_vid) { /* VID can be given to be used for Port Up-MEP or EVC Customer MIP */
        config.vid = vid;
    }
    if (has_evc) { /* In EVC domain the flow is the EVC instance number - start in '0' */
        config.flow = flow-1;
    }
    if (has_vlan) { /* In VLAN domain the flow is the VLAN-ID */
        config.flow = flow;
    }
    if ((rc = mep_instance_conf_set(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
    /* no mep <inst:uint> */
        config.enable = FALSE;

        if ((rc = mep_instance_conf_set(inst-1, &config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }

    return TRUE;
}

BOOL mep_mep_meg_id(i32 session_id, u32 inst,
                    char *megid, BOOL has_itu, BOOL has_itu_cc, BOOL has_ieee, BOOL has_name, char *name)
{
    u32                           meg_size=0,  name_size=0;
    mep_conf_t                    config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable && (megid != NULL)) {
        /* mep <inst:uint> meg-id <megid:word> {itu | itu-cc | {ieee [name <name:word>]}} */
        memcpy(config.name, def_conf.config.name, sizeof(config.name)); /* Initialize Maintenance Domain Name to default */
        memset(config.meg, 0, sizeof(config.meg));  /* Initialize MEGID */
        meg_size = strlen(megid);
        if (name != NULL ) {
            name_size = strlen(name);
        }

        if ((meg_size > (VTSS_APPL_MEP_MEG_CODE_LENGTH-1)) || (name_size > (VTSS_APPL_MEP_MEG_CODE_LENGTH-1))) {
            ICLI_PRINTF("Invalid 'meg-id' or 'name' size\n");
            return FALSE;
        }
        memcpy(config.meg, megid, sizeof(config.meg));    /* Copy the MEGID */
        if (has_name && (name != NULL)) {
            memcpy(config.name, name, sizeof(config.name));
        }
        config.meg[VTSS_APPL_MEP_MEG_CODE_LENGTH-1] = '\0';  /* Assure 'NULL' termination */
        config.name[VTSS_APPL_MEP_MEG_CODE_LENGTH-1] = '\0';
        config.format = has_itu ? VTSS_APPL_MEP_ITU_ICC :
                        has_itu_cc ? VTSS_APPL_MEP_ITU_CC_ICC :
                        has_ieee ? VTSS_APPL_MEP_IEEE_STR : VTSS_APPL_MEP_ITU_ICC;
        if ((rc = mep_instance_conf_set(inst-1, &config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_mep_id(i32 session_id, u32 inst,
                    u32 mepid)
{
    mep_conf_t                config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> mep-id <mepid:uint> */
        config.mep = mepid;

        if ((rc = mep_instance_conf_set(inst-1, &config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_pm(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_pm_conf_t   pm_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> performance-monitoring */
        pm_config.enable = TRUE;

        if ((rc = vtss_appl_mep_pm_conf_set(inst-1, &pm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_pm(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_pm_conf_t   pm_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> performance-monitoring */
        pm_config.enable = FALSE;

        if ((rc = vtss_appl_mep_pm_conf_set(inst-1, &pm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_lst(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_lst_conf_t  lst_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> link-state-tracking */
        lst_config.enable = TRUE;

        if ((rc = vtss_appl_mep_lst_conf_set(inst-1, &lst_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_lst(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_lst_conf_t  lst_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> link-state-tracking */
        lst_config.enable = FALSE;

        if ((rc = vtss_appl_mep_lst_conf_set(inst-1, &lst_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_level(i32 session_id, u32 inst,
                   u32 level)
{
    mep_conf_t                config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> level <level:0-7> */
        config.level = level;

        if ((rc = mep_instance_conf_set(inst-1, &config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_vid(i32 session_id, u32 inst,
                 u32 vid)
{
    mep_conf_t                config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> vid <vid:vlan_id> */
        config.vid = vid;

        if ((rc = mep_instance_conf_set(inst-1, &config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_vid(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> vid */
        config.vid = 0;

        if ((rc = mep_instance_conf_set(inst-1, &config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_voe(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> voe */
        config.voe = TRUE;

        if ((rc = mep_instance_conf_set(inst-1, &config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_voe(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> voe */
        config.voe = FALSE;

        if ((rc = mep_instance_conf_set(inst-1, &config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_peer_mep_id(i32 session_id, u32 inst,
                         u32 mepid, BOOL has_mac, mesa_mac_t peermac)
{
    u32                           i;
    mep_conf_t                    config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> peer-mep-id <mepid:uint> [mac <mac:mac_addr>] */
        if (config.peer_count < mep_caps.mesa_oam_peer_cnt) {
            for (i=0; i<config.peer_count; ++i) {    /* Check for this peer MEP id is known */
                if (config.peer_mep[i] == mepid) {
                    break;
                }
            }
            config.peer_mep[i] = mepid;
            config.peer_mac[i] = def_conf.config.peer_mac[0]; /* Initialize peer MAC to default */
            if (has_mac) {
                config.peer_mac[i] = peermac;
            }
            if (i == config.peer_count) { /* New peer MEP is added */
                config.peer_count++;
            }
            if ((rc = mep_instance_conf_set(inst-1, &config)) != VTSS_RC_OK) {
                mep_print_error(session_id, rc);
                return FALSE;
            }
        }
        else {
            ICLI_PRINTF("Max number of peer MEP is reached\n");
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_peer_mep_id(i32 session_id, u32 inst,
                            u32 mepid, BOOL has_all)
{
    u32                   i, idx;
    mep_conf_t            config;
    MepPeerArray<u16>     peer_mep;
    MepPeerArray<mesa_mac_t> peer_mac;
    mesa_rc               rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> peer-mep-id {<mepid:uint> | all} */
        peer_mep.clear();
        peer_mac.clear();

        if (!has_all) { /* If not all to be deleted then save the peer MEP id that is not going to be deleted */
            for (i=0, idx=0; i<config.peer_count; ++i) {
                if (config.peer_mep[i] != mepid) {
                    peer_mep[idx] = config.peer_mep[i];
                    peer_mac[idx] = config.peer_mac[i];
                    idx++;
                }
            }
        }
        else {
            idx = 0;    /* All is deleted */
        }
        memcpy(config.peer_mep, peer_mep.data(), peer_mep.mem_size());
        memcpy(config.peer_mac, peer_mac.data(), peer_mac.mem_size());
        config.peer_count = idx;
        if ((rc = mep_instance_conf_set(inst-1, &config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_cc(i32 session_id, u32 inst,
                u32 prio, BOOL has_fr300s, BOOL has_fr100s, BOOL has_fr10s, BOOL has_fr1s, BOOL has_fr6m, BOOL has_fr1m, BOOL has_fr6h, BOOL has_rx_only)
{
    mep_conf_t                    config;
    vtss_appl_mep_cc_conf_t       cc_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> cc <prio:0-7> [fr300s|fr100s|fr10s|fr1s|fr6m|fr1m|fr6h] */
        cc_config = def_conf.cc_conf;   /* Initialize cc_conf to default */
        cc_config.enable = cc_config.enable || !has_rx_only;
        cc_config.prio = prio;
        if (has_fr300s || has_fr100s || has_fr10s || has_fr1s || has_fr6m || has_fr1m || has_fr6h) {
            cc_config.rate = has_fr300s ? VTSS_APPL_MEP_RATE_300S :
                            has_fr100s ? VTSS_APPL_MEP_RATE_100S :
                            has_fr10s ? VTSS_APPL_MEP_RATE_10S :
                            has_fr1s ? VTSS_APPL_MEP_RATE_1S :
                            has_fr6m ? VTSS_APPL_MEP_RATE_6M :
                            has_fr1m ? VTSS_APPL_MEP_RATE_1M :
                            has_fr6h ? VTSS_APPL_MEP_RATE_6H : VTSS_APPL_MEP_RATE_1S;
        }
        if ((rc = vtss_appl_mep_cc_conf_set(inst-1, &cc_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_cc(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_cc_conf_t   cc_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> cc */
        if ((rc = vtss_appl_mep_cc_conf_get(inst-1, &cc_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        cc_config.enable = FALSE;
        if ((rc = vtss_appl_mep_cc_conf_set(inst-1, &cc_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_lm(i32 session_id, u32 inst,
                u32 prio, BOOL has_synthetic, BOOL has_uni, BOOL has_multi, BOOL has_mep, BOOL has_single, BOOL has_dual, BOOL has_fr100s, BOOL has_fr10s, BOOL has_fr1s, BOOL has_fr6m, BOOL has_size,
                BOOL has_flr, u32 flr, BOOL has_meas, u32 meas, BOOL has_threshold, u32 loss_th, u32 mepid, u32 size, BOOL has_slm_testid, u32 slm_testid)
{
    mep_conf_t                    config;
    vtss_appl_mep_lm_conf_t       lm_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> lm <prio:0-7> [multi|uni] [single|dual] [fr10s|fr1s|fr6m|fr1m|fr6h] [flr <flr:uint>] [threshold <uint>] */
        if ((rc = vtss_appl_mep_lm_conf_get(inst-1, &lm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }

        // Prevent overwriting possible enabled RX settings
        mesa_bool_t was_rx_enabled = lm_config.enable_rx;

        lm_config = def_conf.lm_conf;   /* Initialize lm_conf to default */
        lm_config.enable = TRUE;

        // restore RX-only settings
        lm_config.enable_rx = was_rx_enabled;

        if (has_synthetic) {
            lm_config.synthetic = TRUE;
        }
        lm_config.dei = def_conf.lm_conf.dei;
        lm_config.prio = prio;
        if (has_fr100s || has_fr10s || has_fr1s || has_fr6m) {
            lm_config.rate = has_fr100s ? VTSS_APPL_MEP_RATE_100S :
                             has_fr10s ? VTSS_APPL_MEP_RATE_10S :
                             has_fr1s ? VTSS_APPL_MEP_RATE_1S :
                             VTSS_APPL_MEP_RATE_6M;
        }
        if (has_uni && has_mep) {
            lm_config.mep = mepid;
        }
        if (has_uni || has_multi) {
            lm_config.cast = has_uni ? VTSS_APPL_MEP_UNICAST :
                             has_multi ? VTSS_APPL_MEP_MULTICAST : VTSS_APPL_MEP_UNICAST;
        }
        if (has_single || has_dual) {
            lm_config.ended = has_single ? VTSS_APPL_MEP_SINGEL_ENDED :
                              has_dual ? VTSS_APPL_MEP_DUAL_ENDED : VTSS_APPL_MEP_SINGEL_ENDED;
        }
        if (has_size) {
            lm_config.size = size;
        }
        if (has_flr) {
            lm_config.flr_interval = flr;
        }
        if (has_meas) {
            lm_config.meas_interval = meas;
        }
        if (has_threshold) {
            lm_config.loss_th = loss_th;
        }
        if (has_synthetic && has_slm_testid) {
            lm_config.slm_test_id = slm_testid;
        }
        if ((rc = vtss_appl_mep_lm_conf_set(inst-1, &lm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_lm(i32 session_id, u32 inst, BOOL disable_tx, BOOL disable_rx)
{
    mep_conf_t                config;
    vtss_appl_mep_lm_conf_t   lm_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> lm */
        if ((rc = vtss_appl_mep_lm_conf_get(inst-1, &lm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        if (disable_tx)
            lm_config.enable = FALSE;
        if (disable_rx)
            lm_config.enable_rx = FALSE;
        if ((rc = vtss_appl_mep_lm_conf_set(inst-1, &lm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_lm_rx(i32 session_id, u32 inst, u32 prio, BOOL has_synthetic, BOOL has_fr100s, BOOL has_fr10s, BOOL has_fr1s, BOOL has_fr6m,
        BOOL has_flr, u32 flr, BOOL has_meas, u32 meas, BOOL has_threshold, u32 loss_th)
{
    vtss_appl_mep_conf_t          config;
    vtss_appl_mep_default_conf_t  def_conf;
    vtss_appl_mep_lm_conf_t       lm_config;
    vtss_rc                       rc;

    vtss_appl_mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = vtss_appl_mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <uint> lm rx [ flr <uint> ] [ loss-ratio-threshold <1-100> ] */
        if ((rc = vtss_appl_mep_lm_conf_get(inst-1, &lm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        lm_config.enable_rx = TRUE;
        if (!mep_caps.mesa_mep_serval) {
            lm_config.prio = prio;
        }
        lm_config.flr_interval = def_conf.lm_conf.flr_interval;
        lm_config.synthetic = has_synthetic;
        if (has_fr100s || has_fr10s || has_fr1s || has_fr6m) {
            lm_config.rate = has_fr100s ? VTSS_APPL_MEP_RATE_100S :
                             has_fr10s ? VTSS_APPL_MEP_RATE_10S :
                             has_fr1s ? VTSS_APPL_MEP_RATE_1S :
                             VTSS_APPL_MEP_RATE_6M;
        }
        if (has_flr) {
            lm_config.flr_interval = flr;
        }
        if (has_meas) {
            lm_config.meas_interval = meas;
        }
        lm_config.loss_th = def_conf.lm_conf.loss_th;
        if (has_threshold) {
            lm_config.loss_th = loss_th;
        }
        if ((rc = vtss_appl_mep_lm_conf_set(inst-1, &lm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_lm_avail(i32 session_id, u32 inst, u32 interval, u32 flr_th)
{
    mep_conf_t                    config;
    vtss_appl_mep_lm_avail_conf_t lm_avail_config;
    mesa_rc                       rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <uint> lm-avail interval <uint> flr-threshold <0-1000> */
        if ((rc = vtss_appl_mep_lm_avail_conf_get(inst-1, &lm_avail_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        lm_avail_config.enable   = TRUE;
        lm_avail_config.flr_th   = flr_th;
        lm_avail_config.interval = interval;
        if ((rc = vtss_appl_mep_lm_avail_conf_set(inst-1, &lm_avail_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    } else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_lm_avail(i32 session_id, u32 inst)
{
    mep_conf_t                    config;
    vtss_appl_mep_lm_avail_conf_t lm_avail_config;
    mesa_rc                       rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <uint> lm-avail */
        if ((rc = vtss_appl_mep_lm_avail_conf_get(inst-1, &lm_avail_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }

        if (!lm_avail_config.enable) {
            ICLI_PRINTF("Availability is not enabled for this MEP\n");
            return FALSE;
        }

        lm_avail_config.enable = FALSE;
        if ((rc = vtss_appl_mep_lm_avail_conf_set(inst-1, &lm_avail_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    } else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_lm_avail_main(i32 session_id, u32 inst, BOOL main)
{
    mep_conf_t                    config;
    vtss_appl_mep_lm_avail_conf_t lm_avail_config;
    mesa_rc                       rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* (no) mep <uint> lm-avail maintenance */
        if ((rc = vtss_appl_mep_lm_avail_conf_get(inst-1, &lm_avail_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        if (!lm_avail_config.enable) {
            ICLI_PRINTF("This MEP does not have availability configured\n");
            return FALSE;
        }
        lm_avail_config.main = main;
        if ((rc = vtss_appl_mep_lm_avail_conf_set(inst-1, &lm_avail_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    } else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_lm_hli(i32 session_id, u32 inst, u32 flr_th, u32 interval)
{
    mep_conf_t                  config;
    vtss_appl_mep_lm_hli_conf_t lm_hli_config;
    mesa_rc                     rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <uint> lm-hli flr-threshold <0-1000> interval <uint> */
        if ((rc = vtss_appl_mep_lm_hli_conf_get(inst-1, &lm_hli_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        lm_hli_config.enable  = TRUE;
        lm_hli_config.flr_th  = flr_th;
        lm_hli_config.con_int = interval;
        if ((rc = vtss_appl_mep_lm_hli_conf_set(inst-1, &lm_hli_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    } else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_lm_hli(i32 session_id, u32 inst)
{
    mep_conf_t                  config;
    vtss_appl_mep_lm_hli_conf_t lm_hli_config;
    mesa_rc                     rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <uint> lm-hli */
        if ((rc = vtss_appl_mep_lm_hli_conf_get(inst-1, &lm_hli_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        lm_hli_config.enable = FALSE;
        if ((rc = vtss_appl_mep_lm_hli_conf_set(inst-1, &lm_hli_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    } else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_lm_sdeg(i32 session_id, u32 inst, u32 tx_min, u32 flr_th, u32 bad_th, u32 good_th)
{
    mep_conf_t                   config;
    vtss_appl_mep_lm_sdeg_conf_t lm_sdeg_config;
    mesa_rc                      rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <uint> lm-sdeg tx-min <uint> flr-threshold <0-1000> bad-threshold <uint> good-threshold <uint> */
        if ((rc = vtss_appl_mep_lm_sdeg_conf_get(inst-1, &lm_sdeg_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        lm_sdeg_config.enable  = TRUE;
        lm_sdeg_config.tx_min  = tx_min;
        lm_sdeg_config.flr_th  = flr_th;
        lm_sdeg_config.bad_th  = bad_th;
        lm_sdeg_config.good_th = good_th;
        if ((rc = vtss_appl_mep_lm_sdeg_conf_set(inst-1, &lm_sdeg_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    } else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_lm_sdeg(i32 session_id, u32 inst)
{
    mep_conf_t                   config;
    vtss_appl_mep_lm_sdeg_conf_t lm_sdeg_config;
    mesa_rc                      rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <uint> lm-sdeg */
        if ((rc = vtss_appl_mep_lm_sdeg_conf_get(inst-1, &lm_sdeg_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        lm_sdeg_config.enable = FALSE;
        if ((rc = vtss_appl_mep_lm_sdeg_conf_set(inst-1, &lm_sdeg_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    } else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_lm_notif(i32 session_id, u32 inst, u32 los_int_cnt_holddown, u32 los_th_cnt_holddown, u32 hli_cnt_holddown)
{
    mep_conf_t                  config;
    vtss_appl_mep_lm_conf_t     lm_config;
    vtss_appl_mep_lm_hli_conf_t lm_hli_config;
    mesa_rc                     rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <uint> lm-notif los-int-cnt-holddown <uint> los-th-cnt-holddown <uint> hli-cnt-holddown <uint> */
        if ((rc = vtss_appl_mep_lm_conf_get(inst-1, &lm_config)) != VTSS_RC_OK ||
            (rc = vtss_appl_mep_lm_hli_conf_get(inst-1, &lm_hli_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        lm_config.los_int_cnt_holddown = los_int_cnt_holddown;
        lm_config.los_th_cnt_holddown  = los_th_cnt_holddown;
        lm_hli_config.cnt_holddown     = hli_cnt_holddown;
        if ((rc = vtss_appl_mep_lm_conf_set(inst-1, &lm_config)) != VTSS_RC_OK ||
            (rc = vtss_appl_mep_lm_hli_conf_set(inst-1, &lm_hli_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    } else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_lm_notif(i32 session_id, u32 inst)
{
    mep_conf_t                  config;
    vtss_appl_mep_lm_conf_t     lm_config;
    vtss_appl_mep_lm_hli_conf_t lm_hli_config;
    mesa_rc                     rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <uint> lm-notif */
        if ((rc = vtss_appl_mep_lm_conf_get(inst-1, &lm_config)) != VTSS_RC_OK ||
            (rc = vtss_appl_mep_lm_hli_conf_get(inst-1, &lm_hli_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        lm_config.los_int_cnt_holddown = 0;
        lm_config.los_th_cnt_holddown  = 0;
        lm_hli_config.cnt_holddown     = 0;
        if ((rc = vtss_appl_mep_lm_conf_set(inst-1, &lm_config)) != VTSS_RC_OK ||
            (rc = vtss_appl_mep_lm_hli_conf_set(inst-1, &lm_hli_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    } else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_lm_flow_count(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_lm_conf_t   lm_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> lm flow-counting */
        if ((rc = vtss_appl_mep_lm_conf_get(inst-1, &lm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        lm_config.flow_count = TRUE;
        if ((rc = vtss_appl_mep_lm_conf_set(inst-1, &lm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_lm_flow_count(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_lm_conf_t   lm_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> lm priority count */
        if ((rc = vtss_appl_mep_lm_conf_get(inst-1, &lm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        lm_config.flow_count = FALSE;
        if ((rc = vtss_appl_mep_lm_conf_set(inst-1, &lm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_lm_oam_count(i32 session_id, u32 inst, BOOL has_y1731, BOOL has_all)
{
    mep_conf_t                config;
    vtss_appl_mep_lm_conf_t   lm_config;
    mep_default_conf_t        def_conf;
    mesa_rc                   rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <uint> lm oam-counting { [ y1731 | all ] } */
        if ((rc = vtss_appl_mep_lm_conf_get(inst-1, &lm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        lm_config.oam_count = def_conf.lm_conf.oam_count;
        if (has_y1731 || has_all) {
            lm_config.oam_count = has_y1731 ? VTSS_APPL_MEP_OAM_COUNT_Y1731 :
                                has_all ? VTSS_APPL_MEP_OAM_COUNT_ALL : VTSS_APPL_MEP_OAM_COUNT_Y1731;
        }
        if ((rc = vtss_appl_mep_lm_conf_set(inst-1, &lm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}


BOOL mep_no_mep_lm_oam_count(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_lm_conf_t   lm_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> lm oam-counting */
        if ((rc = vtss_appl_mep_lm_conf_get(inst-1, &lm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        lm_config.oam_count = VTSS_APPL_MEP_OAM_COUNT_NONE;

        if ((rc = vtss_appl_mep_lm_conf_set(inst-1, &lm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}


BOOL mep_mep_dm(i32 session_id, u32 inst,
                u32 prio, BOOL has_uni, BOOL has_multi, u32 mepid, BOOL has_dual, BOOL has_single, BOOL has_rdtrp, BOOL has_flow, u32 interval, u32 lastn)
{
    mep_conf_t                    config;
    vtss_appl_mep_dm_conf_t       dm_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> dm <prio:0-7> [multi|{uni mep-id <mepid:uint>}] [single|dual] [rdtrp|flow] interval <interval:uint> last-n <lastn:uint> */
        dm_config = def_conf.dm_conf;   /* Initialize dm_conf to default */
        dm_config.enable = TRUE;
        dm_config.prio = prio;
        if (has_uni || has_multi) {
            dm_config.cast = has_uni ? VTSS_APPL_MEP_UNICAST :
                             has_multi ? VTSS_APPL_MEP_MULTICAST : VTSS_APPL_MEP_UNICAST;
        }
        if (has_uni) {
            dm_config.mep = mepid;
        }
        if (has_dual || has_single) {
            dm_config.ended = has_dual ? VTSS_APPL_MEP_DUAL_ENDED :
                              has_single ? VTSS_APPL_MEP_SINGEL_ENDED : VTSS_APPL_MEP_DUAL_ENDED;
        }
        if (has_rdtrp || has_flow) {
            dm_config.calcway = has_rdtrp ? VTSS_APPL_MEP_RDTRP :
                                has_flow ? VTSS_APPL_MEP_FLOW : VTSS_APPL_MEP_RDTRP;
        }
        dm_config.interval = interval;
        dm_config.lastn = lastn;
        if ((rc = vtss_appl_mep_dm_conf_set(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_dm(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_dm_conf_t   dm_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> dm */
        if ((rc = vtss_appl_mep_dm_conf_get(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        dm_config.enable = FALSE;
        if ((rc = vtss_appl_mep_dm_conf_set(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_dm_overflow_reset(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_dm_conf_t   dm_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_dm_conf_get(inst-1, &dm_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> dm overflow-reset */
        dm_config.overflow_act = VTSS_APPL_MEP_CONTINUE;
        if ((rc = vtss_appl_mep_dm_conf_set(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_dm_overflow_reset(i32 session_id, u32 inst)
{
    mep_conf_t                    config;
    vtss_appl_mep_dm_conf_t       dm_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_dm_conf_get(inst-1, &dm_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> dm overflow-reset */
        dm_config.overflow_act = def_conf.dm_conf.overflow_act;   /* Initialize dm_conf.overflow_act to default */
        if ((rc = vtss_appl_mep_dm_conf_set(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_dm_ns(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_dm_conf_t   dm_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_dm_conf_get(inst-1, &dm_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> dm ns */
        dm_config.tunit = VTSS_APPL_MEP_NS;
        if ((rc = vtss_appl_mep_dm_conf_set(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_dm_ns(i32 session_id, u32 inst)
{
    mep_conf_t                    config;
    vtss_appl_mep_dm_conf_t       dm_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_dm_conf_get(inst-1, &dm_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> dm ns */
        dm_config.tunit = def_conf.dm_conf.tunit;   /* Initialize dm_conf.tunit to default */
        if ((rc = vtss_appl_mep_dm_conf_set(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_dm_synchronized(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_dm_conf_t   dm_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_dm_conf_get(inst-1, &dm_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> dm synchronized */
        dm_config.synchronized = TRUE;
        if ((rc = vtss_appl_mep_dm_conf_set(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_dm_synchronized(i32 session_id, u32 inst)
{
    mep_conf_t                    config;
    vtss_appl_mep_dm_conf_t       dm_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_dm_conf_get(inst-1, &dm_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> dm synchronized */
        dm_config.synchronized = def_conf.dm_conf.synchronized;   /* Initialize dm_conf.synchronized to default */
        if ((rc = vtss_appl_mep_dm_conf_set(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_dm_proprietary(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_dm_conf_t   dm_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_dm_conf_get(inst-1, &dm_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> dm proprietary */
        dm_config.proprietary = TRUE;
        if ((rc = vtss_appl_mep_dm_conf_set(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_dm_proprietary(i32 session_id, u32 inst)
{
    mep_conf_t                    config;
    vtss_appl_mep_dm_conf_t       dm_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_dm_conf_get(inst-1, &dm_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> dm proprietary */
        dm_config.proprietary = def_conf.dm_conf.proprietary;   /* Initialize dm_conf.proprietary to default */
        if ((rc = vtss_appl_mep_dm_conf_set(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_dm_bin_threshold(i32 session_id, u32 inst, u32 threshold)
{
    mep_conf_t                config;
    vtss_appl_mep_dm_conf_t   dm_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_dm_conf_get(inst-1, &dm_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> dm bin threshold <1-50000> */
        dm_config.m_threshold = threshold;
        if ((rc = vtss_appl_mep_dm_conf_set(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_dm_bin_threshold(i32 session_id, u32 inst, u32 threshold)
{
    mep_conf_t                    config;
    vtss_appl_mep_dm_conf_t       dm_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_dm_conf_get(inst-1, &dm_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> dm bin threshold */
        dm_config.m_threshold = def_conf.dm_conf.m_threshold;   /* Initialize dm_conf.m_threshold to default */
        if ((rc = vtss_appl_mep_dm_conf_set(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_dm_bin_fd(i32 session_id, u32 inst, u32 num_fd)
{
    mep_conf_t                config;
    vtss_appl_mep_dm_conf_t   dm_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_dm_conf_get(inst-1, &dm_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> dm bin fd <2-10> */
        dm_config.num_of_bin_fd = num_fd;
        if ((rc = vtss_appl_mep_dm_conf_set(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_dm_bin_fd(i32 session_id, u32 inst, u32 num_fd)
{
    mep_conf_t                    config;
    vtss_appl_mep_dm_conf_t       dm_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_dm_conf_get(inst-1, &dm_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> dm bin fd */
        dm_config.num_of_bin_fd = def_conf.dm_conf.num_of_bin_fd;   /* Initialize dm_conf.num_of_bin_fd to default */
        if ((rc = vtss_appl_mep_dm_conf_set(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_dm_bin_ifdv(i32 session_id, u32 inst, u32 num_ifdv)
{
    mep_conf_t                config;
    vtss_appl_mep_dm_conf_t   dm_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_dm_conf_get(inst-1, &dm_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> dm bin ifdv <2-10> */
        dm_config.num_of_bin_ifdv = num_ifdv;
        if ((rc = vtss_appl_mep_dm_conf_set(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_dm_bin_ifdv(i32 session_id, u32 inst, u32 num_ifdv)
{
    mep_conf_t                    config;
    vtss_appl_mep_dm_conf_t       dm_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_dm_conf_get(inst-1, &dm_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> dm bin ifdv */
        dm_config.num_of_bin_ifdv = def_conf.dm_conf.num_of_bin_ifdv;   /* Initialize dm_conf.num_of_bin_ifdv to default */
        if ((rc = vtss_appl_mep_dm_conf_set(inst-1, &dm_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_lt(i32 session_id, u32 inst,
                u32 prio, BOOL has_mep_id, u32 mepid, BOOL has_mac, mesa_mac_t lt_mac, u32 ttl)
{
    mep_conf_t                    config;
    vtss_appl_mep_lt_conf_t       lt_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> lt <prio:0-7> {{mep-id <mepid:uint>} | {mac <mac:mac_addr>}} ttl <ttl:uint> */
        if (ttl == 0) {
          ICLI_PRINTF("TTL=0 not allowed\n");
          return FALSE;
        }

        lt_config = def_conf.lt_conf;   /* Initialize lt_conf to default */
        lt_config.ttl = ttl;
        lt_config.enable = TRUE;
        lt_config.prio = prio;
        if (has_mep_id) {
            lt_config.mep = mepid;
        }
        if (has_mac) {
            lt_config.mac = lt_mac;
        }
        lt_config.transactions = VTSS_APPL_MEP_TRANSACTION_MAX;
        if ((rc = vtss_appl_mep_lt_conf_set(inst-1, &lt_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_lt(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_lt_conf_t   lt_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> lt */
        if ((rc = vtss_appl_mep_lt_conf_get(inst-1, &lt_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        lt_config.enable = FALSE;
        if ((rc = vtss_appl_mep_lt_conf_set(inst-1, &lt_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_lb(i32 session_id, u32 inst,
                u32 prio, BOOL has_dei, BOOL has_multi, BOOL has_uni, BOOL has_mep_id, u32 mepid, BOOL has_mac, mesa_mac_t lb_mac,
                BOOL has_mpls, BOOL has_ttl, u8 mpls_ttl, u32 count, u32 size, u32 interval)
{
    mep_conf_t                    config;
    vtss_appl_mep_lb_conf_t       lb_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> lb <prio:0-7> [dei] [multi|{uni {{mep-id <mepid:uint>} | {mac <mac:mac_addr>}}} | mpls ttl <mpls_ttl:uint>] count <count:uint> size <size:uint> interval <interval:uint> */
        lb_config = def_conf.lb_conf;   /* Initialize lb_conf to default */
        lb_config.enable = TRUE;
        if (has_dei) {
            lb_config.dei = TRUE;
        }
        lb_config.prio = prio;
        if (has_uni || has_multi) {
            lb_config.cast = has_uni ? VTSS_APPL_MEP_UNICAST :
                             has_multi ? VTSS_APPL_MEP_MULTICAST : VTSS_APPL_MEP_UNICAST;
        }
        if (has_mep_id) {
            lb_config.mep = mepid;
        }
        if (has_mac) {
            lb_config.mac = lb_mac;
        }
        if (has_mpls && has_ttl) {
            lb_config.ttl = mpls_ttl;
        } else {
            lb_config.ttl = 255;
        }
        lb_config.to_send = count;
        lb_config.size = size;
        lb_config.interval = interval;
        if ((rc = vtss_appl_mep_lb_conf_set(inst-1, &lb_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_lb(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_lb_conf_t   lb_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> lb */
        if ((rc = vtss_appl_mep_lb_conf_get(inst-1, &lb_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        lb_config.enable = FALSE;
        if ((rc = vtss_appl_mep_lb_conf_set(inst-1, &lb_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
            return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_tst(i32 session_id, u32 inst,
                 u32 prio, BOOL has_dei, u32 mepid, BOOL has_sequence, BOOL has_all_zero, BOOL has_all_one, BOOL has_one_zero, u32 rate, u32 size)
{
    mep_conf_t                    config;
    vtss_appl_mep_tst_conf_t      tst_config;
    mesa_rc                       rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        if ((rc = vtss_appl_mep_tst_conf_get(inst-1, &tst_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        /* mep <inst:uint> tst <prio:0-7> [dei] mep-id <mepid:uint> [sequence] [all-zero|all-one|one-zero] rate <rate:uint> size <size:uint> */
        if (size > mep_caps.appl_mep_packet_rx_mtu - 4) {
            ICLI_PRINTF("Consider that the selected frame size, with added tag, is exceeding the MAX frame size that can be copied to CPU.\nThis is not necessarily a problem - see help\n");
        }

        if (has_dei) {
            tst_config.dei = TRUE;
        }
        tst_config.prio = prio;
        tst_config.mep = mepid;
        tst_config.rate = rate;
        tst_config.size = size;
        if (has_all_zero || has_all_one || has_one_zero) {
            tst_config.pattern = has_all_zero ? VTSS_APPL_MEP_PATTERN_ALL_ZERO :
                                 has_all_one ? VTSS_APPL_MEP_PATTERN_ALL_ONE :
                                 has_one_zero ? VTSS_APPL_MEP_PATTERN_0XAA : VTSS_APPL_MEP_PATTERN_ALL_ZERO;
        }
        if (has_sequence) {
            tst_config.sequence = TRUE;
        }
        if ((rc = vtss_appl_mep_tst_conf_set(inst-1, &tst_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_tst_tx(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_tst_conf_t  tst_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_tst_conf_get(inst-1, &tst_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> tst tx */
        tst_config.enable = TRUE;
        if ((rc = vtss_appl_mep_tst_conf_set(inst-1, &tst_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_tst_tx(i32 session_id, u32 inst)
{
    mep_conf_t                    config;
    vtss_appl_mep_tst_conf_t      tst_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_tst_conf_get(inst-1, &tst_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> tst tx */
        tst_config.enable = def_conf.tst_conf.enable;   /* Initialize tst_conf.enable to default */
        if ((rc = vtss_appl_mep_tst_conf_set(inst-1, &tst_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_tst_rx(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_tst_conf_t  tst_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_tst_conf_get(inst-1, &tst_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> tst rx */
        tst_config.enable_rx = TRUE;
        if ((rc = vtss_appl_mep_tst_conf_set(inst-1, &tst_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_tst_rx(i32 session_id, u32 inst)
{
    mep_conf_t                    config;
    vtss_appl_mep_tst_conf_t      tst_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = vtss_appl_mep_tst_conf_get(inst-1, &tst_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> tst rx */
        tst_config.enable_rx = def_conf.tst_conf.enable_rx;   /* Initialize tst_conf.enable_rx to default */
        if ((rc = vtss_appl_mep_tst_conf_set(inst-1, &tst_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_debug_mep_test(i32 session_id, u32 inst, BOOL has_lb, BOOL has_tst, u32 mepid, BOOL has_sequence)
{
    mep_conf_t                      config;
    vtss_appl_mep_tst_common_conf_t tst_config;
    vtss_appl_mep_lb_common_conf_t  lb_config;
    mep_default_conf_t              def_conf;
    mesa_rc                         rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (!config.enable) {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }
    if (has_tst) {
        if ((rc = vtss_appl_mep_tst_common_conf_get(inst-1, &tst_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }

        /* deb mep <uint> test tst mep-id <uint> [ sequence ] */
        tst_config.sequence = def_conf.tst_common_conf.sequence;   /* Initialize tst_config to default */
        tst_config.mep = mepid;
        if (has_sequence) {
            tst_config.sequence = TRUE;
        }
        if ((rc = vtss_appl_mep_tst_common_conf_set(inst-1, &tst_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    if (has_lb) {
        if ((rc = vtss_appl_mep_lb_common_conf_get(inst-1, &lb_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }

        /* deb mep <uint> test lb mep-id <uint> [ sequence ] */
        lb_config.mep = mepid;
        lb_config.cast = VTSS_APPL_MEP_UNICAST;
        lb_config.to_send = VTSS_APPL_MEP_LB_TO_SEND_INFINITE;

        if ((rc = vtss_appl_mep_lb_common_conf_set(inst-1, &lb_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }

    return TRUE;
}

BOOL mep_debug_mep_test_tx(i32 session_id, u32 inst, BOOL has_lb, BOOL has_tst, u32 prio, BOOL has_all, BOOL has_dei, BOOL has_all_zero, BOOL has_all_one, BOOL has_one_zero, BOOL has_rate, u32 rate, BOOL has_size, u32 size)
{
    u32                             i, j, prio_start, prio_stop, idx_start, idx_stop;
    mep_conf_t                      config;
    vtss_appl_mep_test_prio_conf_t  test_config;
    mep_default_conf_t              def_conf;
    mesa_rc                         rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (!config.enable) {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }
    if (has_tst) {
        if ((rc = vtss_appl_mep_tst_prio_conf_get(inst-1, prio, (has_dei ? 1 : 0), &test_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    if (has_lb) {
        if ((rc = vtss_appl_mep_lb_prio_conf_get(inst-1, prio, (has_dei ? 1 : 0), &test_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }

    /* debug mep <uint> test tx {tst | lb } <0-7> [ dei ] [ all-zero | all-one | one-zero ] [ rate <uint> ] [ size <uint> ] */
    test_config = def_conf.test_prio_conf;   /* Initialize test_config to default */
    test_config.enable = TRUE;
    test_config.dp = (!has_dei ? VTSS_APPL_MEP_DP_GREEN : VTSS_APPL_MEP_DP_YELLOW);
    if (has_rate) {
        test_config.rate = rate;
    }
    if (has_size) {
        test_config.size = size;
    }
    if (has_all_zero || has_all_one || has_one_zero) {
        test_config.pattern = has_all_zero ? VTSS_APPL_MEP_PATTERN_ALL_ZERO :
                            has_all_one ? VTSS_APPL_MEP_PATTERN_ALL_ONE :
                            has_one_zero ? VTSS_APPL_MEP_PATTERN_0XAA : VTSS_APPL_MEP_PATTERN_ALL_ZERO;
    }

    if (has_all) {
        prio_start = 0;
        idx_start = 0;
        prio_stop = VTSS_APPL_MEP_PRIO_MAX;
        idx_stop = VTSS_APPL_MEP_TEST_MAX;
    } else {
        prio_start = prio;
        prio_stop = prio_start+1;
        idx_start = (has_dei ? 1 : 0);
        idx_stop = idx_start+1;
    }

    for (i=prio_start; i<prio_stop; ++i) {
        for (j=idx_start; j<idx_stop; ++j) {
            test_config.dp = ((j==0) ? VTSS_APPL_MEP_DP_GREEN : VTSS_APPL_MEP_DP_YELLOW);
            if (has_tst) {
                if ((rc = vtss_appl_mep_tst_prio_conf_set(inst-1, i, j, &test_config)) != VTSS_RC_OK) {
                    mep_print_error(session_id, rc);
                    return FALSE;
                }
            }
            if (has_lb) {
                if ((rc = vtss_appl_mep_lb_prio_conf_set(inst-1, i, j, &test_config)) != VTSS_RC_OK) {
                    mep_print_error(session_id, rc);
                    return FALSE;
                }
            }
        }
    }

    return TRUE;
}

BOOL mep_no_debug_mep_test_tx(i32 session_id, u32 inst, BOOL has_lb, BOOL has_tst, u32 prio, BOOL has_all, BOOL has_dei, BOOL has_all_zero, BOOL has_all_one, BOOL has_one_zero, BOOL has_rate, u32 rate, BOOL has_size, u32 size)
{
    u32                            i, j, prio_start, prio_stop, idx_start, idx_stop;
    mep_conf_t                     config;
    vtss_appl_mep_test_prio_conf_t test_config;
    mesa_rc                        rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (!config.enable) {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }
    if (has_tst) {
        if ((rc = vtss_appl_mep_tst_prio_conf_get(inst-1, prio, (has_dei ? 1 : 0), &test_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    if (has_lb) {
        if ((rc = vtss_appl_mep_lb_prio_conf_get(inst-1, prio, (has_dei ? 1 : 0), &test_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }

    /* no debug mep <uint> tst tx { <0-7> | all } [ dei ] [ all-zero | all-one | one-zero ] [ rate <uint> ] [ size <uint> ] */
    test_config.enable = FALSE;

    if (has_all) {
        prio_start = 0;
        idx_start = 0;
        prio_stop = VTSS_APPL_MEP_PRIO_MAX;
        idx_stop = VTSS_APPL_MEP_TEST_MAX;
    } else {
        prio_start = prio;
        prio_stop = prio_start+1;
        idx_start = (has_dei ? 1 : 0);
        idx_stop = idx_start+1;
    }

    for (i=prio_start; i<prio_stop; ++i) {
        for (j=idx_start; j<idx_stop; ++j) {
            if (has_tst) {
                if ((rc = vtss_appl_mep_tst_prio_conf_set(inst-1, i, j, &test_config)) != VTSS_RC_OK) {
                    mep_print_error(session_id, rc);
                    return FALSE;
                }
            }
            if (has_lb) {
                if ((rc = vtss_appl_mep_lb_prio_conf_set(inst-1, i, j, &test_config)) != VTSS_RC_OK) {
                    mep_print_error(session_id, rc);
                    return FALSE;
                }
            }
        }
    }

    return TRUE;
}

BOOL mep_debug_mep_dm_tx(i32 session_id, u32 inst, BOOL has_dual, BOOL has_single, u32 prio, BOOL has_interval, u32 interval, BOOL has_synchronized)
{
    mep_conf_t                      config;
    vtss_appl_mep_dm_common_conf_t  dm_common;
    vtss_appl_mep_dm_prio_conf_t    dm_config;
    mep_default_conf_t              def_conf;
    mesa_rc                         rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (!config.enable) {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    dm_common = def_conf.dm_common_conf;
    if (has_dual || has_single) {
        dm_common.ended = (has_dual) ? VTSS_APPL_MEP_DUAL_ENDED : VTSS_APPL_MEP_SINGEL_ENDED;
    }
    if (has_interval) {
        dm_common.interval = interval;
    }
    if (has_synchronized) {
        dm_common.synchronized = TRUE;
    }

    if ((rc = vtss_appl_mep_dm_common_conf_set(inst-1, &dm_common)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }

    dm_config.enable = TRUE;

    if ((rc = vtss_appl_mep_dm_prio_conf_set(inst-1, prio, &dm_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_debug_mep_dm_tx(i32 session_id, u32 inst, BOOL has_dual, BOOL has_single, u32 prio, BOOL has_interval, u32 interval, BOOL has_synchronized)
{
    mep_conf_t                      config;
    vtss_appl_mep_dm_prio_conf_t    dm_config;
    mep_default_conf_t              def_conf;
    mesa_rc                         rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (!config.enable) {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    dm_config.enable = FALSE;

    if ((rc = vtss_appl_mep_dm_prio_conf_set(inst-1, prio, &dm_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }

    return TRUE;
}


BOOL mep_debug_mep_test_status(i32 session_id, u32 inst, BOOL has_lb, BOOL has_tst)
{
    if (mep_caps.mesa_mep_luton26) {
        ICLI_PRINTF("Not possible on this platform\n");
        return FALSE;
    }
    u32                        i;
    mep_conf_t                 config;
    mep_sat_counters_t         sat_counters;
    vtss_appl_mep_tst_state_t  tst_status;
    vtss_appl_mep_lb_state_t   lb_status;
    mesa_rc                    rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (!config.enable) {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    ICLI_PRINTF("MEP TEST state is:\n");
    ICLI_PRINTF("%7s", "Inst");
    ICLI_PRINTF("%11s", "Priority");
    ICLI_PRINTF("%9s", "Colour");
    ICLI_PRINTF("%19s", "TX frame count");
    ICLI_PRINTF("%19s", "RX frame count");
    ICLI_PRINTF("\n");

    if ((rc = mep_volatile_sat_counter_get(inst-1, &sat_counters)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }

    ICLI_PRINTF("%7u", inst);
    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        if (i != 0) {
            ICLI_PRINTF("%7s", " ");
        }
        ICLI_PRINTF("%11u", i);
        if (mep_caps.mesa_mep_serval == MESA_MEP_GENERATION_1) {
            ICLI_PRINTF("%9s", "Green");
            ICLI_PRINTF("%19" PRIu64, sat_counters.counters[i].tx_green.frames);
            ICLI_PRINTF("%19" PRIu64, sat_counters.counters[i].rx_green.frames);
            ICLI_PRINTF("\n");

            ICLI_PRINTF("%7s", " ");
            ICLI_PRINTF("%11s", " ");
            ICLI_PRINTF("%9s", "Yellow");
            ICLI_PRINTF("%19" PRIu64, sat_counters.counters[i].tx_yellow.frames);
            ICLI_PRINTF("%19" PRIu64, sat_counters.counters[i].rx_yellow.frames);
            ICLI_PRINTF("\n");
        }
        if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
            if (has_tst) {
                if ((rc = vtss_appl_mep_tst_prio_status_get(inst-1, i, &tst_status)) != VTSS_RC_OK) {
                    mep_print_error(session_id, rc);
                    return FALSE;
                }

                ICLI_PRINTF("%9s", "TST");
                ICLI_PRINTF("%19" PRIu64, sat_counters.counters[i].tx_green.frames);
                ICLI_PRINTF("%19" PRIu64, tst_status.rx_counter);
                ICLI_PRINTF("\n");
            }
            if (has_lb) {
                if ((rc = vtss_appl_mep_lb_prio_status_get(inst-1, i, &lb_status)) != VTSS_RC_OK) {
                    mep_print_error(session_id, rc);
                    return FALSE;
                }
                ICLI_PRINTF("%9s", "LB");
                ICLI_PRINTF("%19" PRIu64, lb_status.lbm_transmitted);
                ICLI_PRINTF("%19" PRIu64, lb_status.reply[0].lbr_received);
                ICLI_PRINTF("\n");
            }
        }
    }

    return TRUE;
}

BOOL mep_debug_mep_test_bps_actual(i32 session_id, u32 inst)
{
#if !defined(VTSS_SW_OPTION_AFI)
    ICLI_PRINTF("Not possible on this platform\n");
    return FALSE;
#else
    u32           i;
    mep_conf_t    config;
    mep_tx_bps_t  bps;
    mesa_rc       rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (!config.enable) {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    ICLI_PRINTF("MEP TEST actual BPS is:\n");
    ICLI_PRINTF("%7s", "Inst");
    ICLI_PRINTF("%11s", "Priority");
    ICLI_PRINTF("%9s", "Colour");
    ICLI_PRINTF("%16s", "Actual TX BPS");
    ICLI_PRINTF("\n");

    if ((rc = mep_tx_bps_actual_get(inst-1, &bps)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }

    ICLI_PRINTF("%7u", inst);
    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        if (i != 0) {
            ICLI_PRINTF("%7s", " ");
        }
        ICLI_PRINTF("%11u", i);
        ICLI_PRINTF("%9s", "Green");
        ICLI_PRINTF("%16" PRIu64, bps.tx_bps[i][0]);
        ICLI_PRINTF("\n");

        ICLI_PRINTF("%7s", " ");
        ICLI_PRINTF("%11s", " ");
        ICLI_PRINTF("%9s", "Yellow");
        ICLI_PRINTF("%16" PRIu64, bps.tx_bps[i][1]);
        ICLI_PRINTF("\n");
    }

    return TRUE;
#endif /* VTSS_SW_OPTION_AFI */
}


BOOL mep_debug_mep_test_clear(i32 session_id, u32 inst)
{
    if (mep_caps.mesa_mep_luton26) {
        ICLI_PRINTF("Not possible on this platform\n");
        return FALSE;
    }
    mep_conf_t   config;
    mesa_rc      rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (!config.enable) {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    if ((rc = mep_volatile_sat_counter_clear(inst-1)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }

    return TRUE;
}

static u32 afi_id=AFI_ID_NONE;

static mesa_packet_oam_type_t oam_type_calculate(u32 opcode)
{
    switch (opcode) {
        case MESA_OAM_OPCODE_NONE:         return MESA_PACKET_OAM_TYPE_NONE;
        case MESA_OAM_OPCODE_CCM:          return MESA_PACKET_OAM_TYPE_CCM;
        case MESA_OAM_OPCODE_LBM:          return MESA_PACKET_OAM_TYPE_LBM;
        case MESA_OAM_OPCODE_LBR:          return MESA_PACKET_OAM_TYPE_LBR;
        case MESA_OAM_OPCODE_LTM:          return MESA_PACKET_OAM_TYPE_LTM;
        case MESA_OAM_OPCODE_LTR:          return MESA_PACKET_OAM_TYPE_LTR;
        case MESA_OAM_OPCODE_LCK:          return MESA_PACKET_OAM_TYPE_LCK;
        case MESA_OAM_OPCODE_LMM:          return MESA_PACKET_OAM_TYPE_LMM;
        case MESA_OAM_OPCODE_LMR:          return MESA_PACKET_OAM_TYPE_LMR;
        case MESA_OAM_OPCODE_1DM:          return MESA_PACKET_OAM_TYPE_1DM;
        case MESA_OAM_OPCODE_DMM:          return MESA_PACKET_OAM_TYPE_DMM;
        case MESA_OAM_OPCODE_DMR:          return MESA_PACKET_OAM_TYPE_DMR;
        default:                           return MESA_PACKET_OAM_TYPE_CCM;
    }
}

BOOL mep_debug_mep_inject(i32 session_id, BOOL has_single, BOOL has_afi, BOOL has_up, BOOL has_down, u32 opcode, u32 prio, BOOL has_isdx, u32 isdx, BOOL has_pipeline, u32 pipeline, BOOL has_tagged)
{
    if (mep_caps.mesa_mep_luton26) {
        ICLI_PRINTF("Not possible on this platform\n");
        return FALSE;
    }

    u8                  *frame = 0;
    u32                 rc=0, idx=0;
    packet_tx_props_t   tx_props;
    afi_multi_conf_t    multi_conf;

    /* Allocate frame buffer */
    frame = packet_tx_alloc(100);
    if (frame == NULL) {
        ICLI_PRINTF("packet_tx_alloc failed\n");
        return FALSE;
    }
    memset(frame, 0, 100);

    /* Calculate frame header */
    frame[0+0] = 0x01;
    frame[0+1] = 0x80;
    frame[0+2] = 0xC2;
    frame[0+3] = 0x00;
    frame[0+4] = 0x00;
    frame[0+5] = 0x30;
    frame[6+0] = 0x00;
    frame[6+1] = 0x01;
    frame[6+2] = 0xC1;
    frame[6+3] = 0x00;
    frame[6+4] = 0xC3;
    frame[6+5] = 0xB0;

    if (has_tagged) {
        frame[12+0] = 0x81;
        frame[12+1] = 0x00;
        frame[12+2] = (prio&0x7)<<5;
        frame[12+3] = 0x65;  /* 101 */
        idx = 16;
    } else {
        idx = 12;
    }
    frame[idx+0] = 0x89;
    frame[idx+1] = 0x02;

    frame[idx+2] = 0x00;
    frame[idx+3] = opcode;
    frame[idx+4] = 0x00;
    frame[idx+5] = 4;

    /* Initialize transmission */
    packet_tx_props_init(&tx_props);
    tx_props.packet_info.modid         = VTSS_MODULE_ID_MEP;
    tx_props.packet_info.frm           = frame;
    tx_props.packet_info.len           = 100;
    tx_props.tx_info.tag.vid           = 222;
    tx_props.tx_info.cos               = prio;
    tx_props.tx_info.cosid             = prio;
    tx_props.tx_info.dp                = 0;
    tx_props.tx_info.pdu_offset        = has_tagged ? 18 : 14;
    tx_props.tx_info.oam_type          = oam_type_calculate(opcode);
    tx_props.tx_info.isdx              = has_isdx ? isdx : VTSS_ISDX_NONE;
    tx_props.tx_info.pipeline_pt       = has_pipeline ? (mesa_packet_pipeline_pt_t)pipeline : MESA_PACKET_PIPELINE_PT_NONE;
    tx_props.tx_info.dst_port_mask     = has_down ? VTSS_BIT64(0) : 0;
    tx_props.tx_info.switch_frm        = has_up ? TRUE : FALSE;
    tx_props.tx_info.masquerade_port   = has_up ? 2 : VTSS_PORT_NO_NONE;

    T_DG(TRACE_GRP_TX_FRAME, "port_mask 0x%" PRIx64 "  isdx %u  pipeline %u  vid %u  cosid %u  cos %u  pcp %u  dp %u  len %zu  maskarade %u  switched %u  oam_type %u  pdu_off %u  frame %X-%X-%X-%X-%X-%X-%X-%X-%X",
         tx_props.tx_info.dst_port_mask, tx_props.tx_info.isdx, tx_props.tx_info.pipeline_pt, tx_props.tx_info.tag.vid, tx_props.tx_info.cosid, tx_props.tx_info.cos, tx_props.tx_info.tag.pcp,
         tx_props.tx_info.dp, tx_props.packet_info.len, tx_props.tx_info.masquerade_port, tx_props.tx_info.switch_frm, tx_props.tx_info.oam_type, tx_props.tx_info.pdu_offset,
         frame[12+0], frame[12+1], frame[12+2], frame[12+3], frame[12+4], frame[12+5], frame[12+6], frame[12+7], frame[12+8]);

    if (has_afi) {
        if (afi_id != AFI_ID_NONE) {
            if (afi_multi_free(afi_id, NULL) != VTSS_RC_OK) {  /* Cancel multi AFI */
                ICLI_PRINTF("afi_multi_free failed\n");
                afi_id = AFI_ID_NONE;
                packet_tx_free(frame);
                return(FALSE);
            }
            afi_id = AFI_ID_NONE;
        }

        /* Kbps - AFI multi */
        if ((rc = afi_multi_conf_init(&multi_conf)) != VTSS_RC_OK) {
            ICLI_PRINTF("afi_multi_conf_init() failed  %s\n", error_txt(rc));
            packet_tx_free(frame);
            return(FALSE);
        }
        multi_conf.params.bps = 1000;
        multi_conf.params.line_rate = FALSE;
        multi_conf.params.seq_cnt = 0;  /* Run forever (until stopped) */
        multi_conf.tx_props[0] = tx_props;
        if ((rc = afi_multi_alloc(&multi_conf, &afi_id)) != VTSS_RC_OK) {
            ICLI_PRINTF("afi_multi_alloc() failed  %s\n", error_txt(rc));
            packet_tx_free(frame);
            return(FALSE);
        }
        if (afi_multi_pause_resume(afi_id, FALSE) != VTSS_RC_OK) {
            ICLI_PRINTF("afi_multi_pause_resume() failed\n");
            packet_tx_free(frame);
            return(FALSE);
        }
        packet_tx_free(frame);
    }

    if (has_single) {
        if (packet_tx(&tx_props) != VTSS_RC_OK) {    /* Transmit */
            ICLI_PRINTF("packet_tx failed\n");
            packet_tx_free(frame);
            return(FALSE);
        }
    }

    return TRUE;
}

BOOL mep_no_debug_mep_inject(i32 session_id, BOOL has_single, BOOL has_afi, BOOL has_up, BOOL has_down, u32 opcode, u32 prio, BOOL has_isdx, u32 isdx, BOOL has_pipeline, u32 pipeline, BOOL has_tagged)
{
    if (mep_caps.mesa_mep_luton26) {
        ICLI_PRINTF("Not possible on this platform\n");
        return FALSE;
    }
    if (afi_id != AFI_ID_NONE) {
        if (afi_multi_free(afi_id, NULL) != VTSS_RC_OK) {  /* Cancel multi AFI */
            ICLI_PRINTF("afi_multi_free failed\n");
            afi_id = AFI_ID_NONE;
            return(FALSE);
        }
        afi_id = AFI_ID_NONE;
    }
    return TRUE;
}


BOOL mep_debug_mep_dm_status(i32 session_id, u32 inst, u32 prio)
{
    mesa_rc                     rc;
    vtss_appl_mep_dm_state_t    tw_status;
    vtss_appl_mep_dm_state_t    nf_status;
    vtss_appl_mep_dm_state_t    fn_status;
    vtss_appl_mep_dm_conf_t     dm_config;

    if ((rc = vtss_appl_mep_dm_prio_status_get(inst-1, prio, &tw_status, &fn_status, &nf_status)) != VTSS_RC_OK) {
        if (rc != VTSS_APPL_MEP_RC_NOT_ENABLED) {
            mep_print_error(session_id, rc);
        }
        return FALSE;
    }

    if ((rc = vtss_appl_mep_dm_conf_get(inst-1, &dm_config)) != VTSS_RC_OK) {
        if (rc != VTSS_APPL_MEP_RC_NOT_ENABLED && rc != VTSS_APPL_MEP_RC_NOT_CREATED) {
            mep_print_error(session_id, rc);
        }
        return FALSE;
    }

    print_dm_state_header(session_id);

    ICLI_PRINTF("%-12s", "1-Way FtoN");
    print_dm_state(session_id, inst, &fn_status);
    ICLI_PRINTF("%-12s", "1-Way NtoF");
    print_dm_state(session_id, inst, &nf_status);
    ICLI_PRINTF("%-12s", "2-Way");
    print_dm_state(session_id, inst, &tw_status);

    print_dm_bin_header(session_id, dm_config.num_of_bin_fd);

    ICLI_PRINTF("Frame Delay\n");
    ICLI_PRINTF("%-12s", "1-Way FtoN");
    print_bin_state(session_id, inst, fn_status.fd_bin, dm_config.num_of_bin_fd);
    ICLI_PRINTF("%-12s", "1-Way NtoF");
    print_bin_state(session_id, inst, nf_status.fd_bin, dm_config.num_of_bin_fd);
    ICLI_PRINTF("%-12s", "2-Way");
    print_bin_state(session_id, inst, tw_status.fd_bin, dm_config.num_of_bin_fd);

    ICLI_PRINTF("Inter-Frame Delay Variation\n");
    ICLI_PRINTF("%-12s", "1-Way FtoN");
    print_bin_state(session_id, inst, fn_status.ifdv_bin, dm_config.num_of_bin_fd);
    ICLI_PRINTF("%-12s", "1-Way NtoF");
    print_bin_state(session_id, inst, nf_status.ifdv_bin, dm_config.num_of_bin_fd);
    ICLI_PRINTF("%-12s", "2-Way");
    print_bin_state(session_id, inst, tw_status.ifdv_bin, dm_config.num_of_bin_fd);

    return TRUE;
}

BOOL mep_debug_mep_slm_statistics(i32 session_id, u32 inst)
{
    vtss_rc                          rc;
    vtss_appl_mep_conf_t             config;
    vtss_appl_mep_lm_conf_t          lm_config;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }

    u32 instance = inst - 1;

    if (((rc = vtss_appl_mep_instance_conf_get(instance, &config)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (!config.enable) {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    if (((rc = vtss_appl_mep_lm_conf_get(instance, &lm_config)) != VTSS_RC_OK) ||
            !lm_config.synthetic ||
            (!lm_config.enable && !lm_config.enable_rx)) {
        ICLI_PRINTF("SLM TX or RX is not enabled for this MEP\n");
        return FALSE;
    }

    ICLI_PRINTF("\n");
    ICLI_PRINTF("MEP %u (ID %u): SLM Service Counters:\n", inst, config.mep);
    ICLI_PRINTF("%10s", "Peer ID");
    ICLI_PRINTF("%12s", "SLM TX");
    ICLI_PRINTF("%12s", "SLM RX");
    ICLI_PRINTF("%12s", "SLR TX");
    ICLI_PRINTF("%12s", "SLR RX");
    ICLI_PRINTF("\n");

    for (u32 peer_idx = 0; peer_idx < config.peer_count; ++peer_idx) {
        u32 peer_mep = config.peer_mep[peer_idx];

        sl_service_counter_t counters;
        memset(&counters, 0, sizeof(counters));
        if (mep_slm_counters_get(instance, peer_idx, &counters) != VTSS_RC_OK)
            continue;

        ICLI_PRINTF("%10u", peer_mep);
        ICLI_PRINTF(VPRI64Fu("12"), counters.tx_slm_counter);
        ICLI_PRINTF(VPRI64Fu("12"), counters.rx_slm_counter);
        ICLI_PRINTF(VPRI64Fu("12"), counters.tx_slr_counter);
        ICLI_PRINTF(VPRI64Fu("12"), counters.rx_slr_counter);

        ICLI_PRINTF("\n");
    }

    return TRUE;
}

BOOL mep_mep_aps(i32 session_id, u32 inst,
                 u32 prio, BOOL has_multi, BOOL has_uni, BOOL has_laps, BOOL has_raps, BOOL has_octet, u32 octet)
{
    mep_conf_t                    config;
    vtss_appl_mep_aps_conf_t      aps_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> aps <prio:0-7> [multi|uni] {laps|{raps [octet <octet:uint>]}} */
        aps_config = def_conf.aps_conf;   /* Initialize aps_conf to default */
        aps_config.enable = TRUE;
        aps_config.prio = prio;
        if (has_uni || has_multi) {
            aps_config.cast = has_uni ? VTSS_APPL_MEP_UNICAST :
                              has_multi ? VTSS_APPL_MEP_MULTICAST : VTSS_APPL_MEP_UNICAST;
        }
        if (has_laps || has_raps) {
            aps_config.type = has_laps ? VTSS_APPL_MEP_L_APS :
                              has_raps ? VTSS_APPL_MEP_R_APS : VTSS_APPL_MEP_L_APS;
        }
        if (has_octet) {
            aps_config.raps_octet = octet;
        }
        if ((rc = vtss_appl_mep_aps_conf_set(inst-1, &aps_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_aps(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_aps_conf_t  aps_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> aps */
        aps_config.enable = FALSE;
        if ((rc = vtss_appl_mep_aps_conf_set(inst-1, &aps_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_client_flow(i32 session_id, u32 inst,
                         u32 cflow, BOOL has_level, u32 level, BOOL has_ais_prio, u32 aisprio, BOOL has_ais_highest, BOOL has_lck_prio, u32 lckprio, BOOL has_lck_highest, BOOL has_evc, BOOL has_vlan, BOOL has_lsp)
{
    u32                           i;
    mep_conf_t                    config;
    mep_client_conf_t             client_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;
    mep_domain_t                  client_domain;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = mep_client_conf_get(inst-1, &client_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <uint> client domain { evc | vlan | lsp } flow <uint> [ level <0-7> ] [ ais-prio [ <0-7> | ais-highest ] ] [ lck-prio [ <0-7> | lck-highest ] ] ] */
        if (has_evc) {
            client_domain = VTSS_APPL_MEP_EVC;
            cflow = cflow-1;
        } else if (has_vlan) {
            client_domain = VTSS_APPL_MEP_VLAN;
        } else if (has_lsp) {
            client_domain = VTSS_APPL_MEP_MPLS_LSP;
        } else {    /* This is not possible as domain is not optional */
            client_domain = VTSS_APPL_MEP_EVC;
        }

        if (client_config.flow_count < VTSS_APPL_MEP_CLIENT_FLOWS_MAX) {
            for (i=0; i<client_config.flow_count; ++i) {     /* Check for this client id is known */
                if ((client_config.domain[i] == client_domain) && (client_config.flows[i] == cflow)) {
                    break;
                }
            }
            if (i == client_config.flow_count) { /* Adding a new client flow - create new entry */
                client_config.ais_prio[i]      = def_conf.client_flow_conf.ais_prio;    /*set to default */
                client_config.lck_prio[i]      = def_conf.client_flow_conf.lck_prio;
                client_config.level[i]         = def_conf.client_flow_conf.level;

                client_config.domain[i] = client_domain;
                client_config.flows[i]  = cflow;
                client_config.flow_count++;
            }
            if (has_level) {
                client_config.level[i] = level;
            }
            if (has_ais_prio) {
                client_config.ais_prio[i] = has_ais_highest ? VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST : aisprio;
            }
            if (has_lck_prio) {
                client_config.lck_prio[i] = has_lck_highest ? VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST : lckprio;
            }
            if ((rc = mep_client_conf_set(inst-1, &client_config)) != VTSS_RC_OK) {
                mep_print_error(session_id, rc);
                return FALSE;
            }
        }
        else {
            ICLI_PRINTF("Max number of client flow is reached\n");
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_client_flow(i32 session_id, u32 inst,
                            u32 cflow, BOOL has_all, BOOL has_evc, BOOL has_vlan, BOOL has_lsp)
{
    u32                          i, idx;
    mep_conf_t                   config;
    mep_client_conf_t            client_config;
    mep_domain_t                 client_domain;
    mep_domain_t                 domain[VTSS_APPL_MEP_CLIENT_FLOWS_MAX];
    u32                          flows[VTSS_APPL_MEP_CLIENT_FLOWS_MAX];
    u8                           ais_prio[VTSS_APPL_MEP_CLIENT_FLOWS_MAX];
    u8                           lck_prio[VTSS_APPL_MEP_CLIENT_FLOWS_MAX];
    u8                           level[VTSS_APPL_MEP_CLIENT_FLOWS_MAX];
    mesa_rc                      rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if ((rc = mep_client_conf_get(inst-1, &client_config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <uint> client domain { evc | vlan | lsp } flow { <uint> | all } */
        if (has_evc) {
            client_domain = VTSS_APPL_MEP_EVC;
            cflow = cflow-1;
        } else if (has_vlan) {
            client_domain = VTSS_APPL_MEP_VLAN;
        } else if (has_lsp) {
            client_domain = VTSS_APPL_MEP_MPLS_LSP;
        } else {    /* This is not possible as domain is not optional */
            client_domain = VTSS_APPL_MEP_EVC;
        }
        memset(domain, 0, sizeof(domain));
        memset(flows, 0, sizeof(flows));
        memset(ais_prio, 0, sizeof(ais_prio));
        memset(lck_prio, 0, sizeof(lck_prio));
        memset(level, 0, sizeof(level));

        /* Save the client flows that are not going to be deleted */
        for (i=0, idx=0; i<client_config.flow_count; ++i) {
            if ((client_config.domain[i] != client_domain) || (!has_all && client_config.flows[i] != cflow)) {
                domain[idx] = client_config.domain[i];
                flows[idx] = client_config.flows[i];
                ais_prio[idx] = client_config.ais_prio[i];
                lck_prio[idx] = client_config.lck_prio[i];
                level[idx] = client_config.level[i];
                idx++;
            }
        }
        memcpy(client_config.domain, domain, sizeof(client_config.domain));
        memcpy(client_config.flows, flows, sizeof(client_config.flows));
        memcpy(client_config.ais_prio, ais_prio, sizeof(client_config.ais_prio));
        memcpy(client_config.lck_prio, lck_prio, sizeof(client_config.lck_prio));
        memcpy(client_config.level, level, sizeof(client_config.level));
        client_config.flow_count = idx;
        if ((rc = mep_client_conf_set(inst-1, &client_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_ais(i32 session_id, u32 inst,
                 BOOL has_fr1s, BOOL has_fr1m, BOOL has_protect)
{
    mep_conf_t                    config;
    vtss_appl_mep_ais_conf_t      ais_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> ais [fr1s|fr1m] [protect] */
        ais_config = def_conf.ais_conf;   /* Initialize ais_conf to default */
        ais_config.enable = TRUE;
        if (has_fr1s || has_fr1m) {
            ais_config.rate = has_fr1s ? VTSS_APPL_MEP_RATE_1S :
                                has_fr1m ? VTSS_APPL_MEP_RATE_1M : VTSS_APPL_MEP_RATE_1S;
        }
        if (has_protect) {
            ais_config.protection = TRUE;
        }
        if ((rc = vtss_appl_mep_ais_conf_set(inst-1, &ais_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_ais(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_ais_conf_t  ais_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    else {
        if (config.enable) {
            /* no mep <inst:uint> ais */
            ais_config.enable = FALSE;
            if ((rc = vtss_appl_mep_ais_conf_set(inst-1, &ais_config)) != VTSS_RC_OK) {
                mep_print_error(session_id, rc);
                return FALSE;
            }
        }
        else {
            ICLI_PRINTF("This MEP is not enabled\n");
            return FALSE;
        }
    }

    return TRUE;
}

BOOL mep_mep_lck(i32 session_id, u32 inst,
                 BOOL has_fr1s, BOOL has_fr1m)
{
    mep_conf_t                    config;
    vtss_appl_mep_lck_conf_t      lck_config;
    mep_default_conf_t            def_conf;
    mesa_rc                       rc;

    mep_default_conf_get(&def_conf);

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> lck [fr1s|fr1m] */
        lck_config = def_conf.lck_conf;   /* Initialize lck_conf to default */
        lck_config.enable = TRUE;
        if (has_fr1s || has_fr1m) {
            lck_config.rate = has_fr1s ? VTSS_APPL_MEP_RATE_1S :
                                has_fr1m ? VTSS_APPL_MEP_RATE_1M : VTSS_APPL_MEP_RATE_1S;
        }
        if ((rc = vtss_appl_mep_lck_conf_set(inst-1, &lck_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_lck(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    vtss_appl_mep_lck_conf_t  lck_config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* no mep <inst:uint> lck */
        lck_config.enable = FALSE;
        if ((rc = vtss_appl_mep_lck_conf_set(inst-1, &lck_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_syslog(i32 session_id, u32 inst)
{
    mep_conf_t                    config;
    vtss_appl_mep_syslog_conf_t   syslog_config;
    mesa_rc                       rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> syslog */
        syslog_config.enable = TRUE;

        if ((rc = vtss_appl_mep_syslog_conf_set(inst-1, &syslog_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_syslog(i32 session_id, u32 inst)
{
    mep_conf_t                    config;
    vtss_appl_mep_syslog_conf_t   syslog_config;
    mesa_rc                       rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if ((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* mep <inst:uint> syslog */
        syslog_config.enable = FALSE;

        if ((rc = vtss_appl_mep_syslog_conf_set(inst-1, &syslog_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_debug_mep_volatile(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK)) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* debug mep <uint> volatile */
        if ((rc = mep_volatile_conf_set(inst-1, &config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_debug_mep_volatile(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_APPL_MEP_RC_VOLATILE)) {
        ICLI_PRINTF("Not Volatile\n");
        if (rc != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
        }
        return FALSE;
    }
    if (config.enable) {
        /* no debug mep <uint> volatile */
        config.enable = FALSE;

        if ((rc = mep_volatile_conf_set(inst-1, &config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_debug_mep_volatile_sat(i32 session_id, u32 inst)
{
    mep_conf_t      config;
    mep_sat_conf_t  sat_conf;
    mesa_rc         rc;
    u32             i;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK)) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* debug debug mep <uint> sat */
        memcpy(&sat_conf.conf, &config, sizeof(sat_conf.conf));
        for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
            if (mep_caps.mesa_mep_serval) {
                sat_conf.prio_conf[i].policer = 0;
            }
            sat_conf.prio_conf[i].vid = VTSS_MEP_MGMT_SAT_VID_UNUSED;
        }
        if ((rc = mep_volatile_sat_conf_set(inst-1, &sat_conf)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_debug_mep_volatile_sat_prio(i32 session_id, u32 inst, u32 prio, u32 vid, u32 policer, BOOL has_dp, u32 dp)
{
    mep_sat_conf_t  sat_conf;
    mesa_rc         rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (((rc = mep_volatile_sat_conf_get(inst-1, &sat_conf)) != VTSS_RC_OK)) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (sat_conf.conf.enable) {
        /* debug debug mep <uint> sat prio */
        sat_conf.prio_conf[prio].vid = vid;
        if (mep_caps.mesa_mep_serval) {
            sat_conf.prio_conf[prio].policer = policer;
            sat_conf.prio_conf[prio].dp_enable = FALSE;
            if (has_dp) {
                sat_conf.prio_conf[prio].dp_enable = TRUE;
                sat_conf.prio_conf[prio].dp = dp;
            }
        }
        if ((rc = mep_volatile_sat_conf_set(inst-1, &sat_conf)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}


























































BOOL mep_debug_mep_raps_forward(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK)) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* debug mep <uint> raps forward */
        if ((rc = vtss_mep_raps_forwarding(inst-1, TRUE)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}


static void print_ccm_status(i32 session_id,  mep_g2::mep_model_ccm_status_t &status)
{
    ICLI_PRINTF("dInv:           %s\n",status.dInv ? "TRUE" : "FALSE");
    ICLI_PRINTF("dLevel:         %s\n",status.dLevel ? "TRUE" : "FALSE");
    ICLI_PRINTF("dMeg:           %s\n",status.dMeg ? "TRUE" : "FALSE");
    ICLI_PRINTF("dMep:           %s\n",status.dMep ? "TRUE" : "FALSE");
    ICLI_PRINTF("dLoc:           %s\n",status.dLoc ? "TRUE" : "FALSE");
    ICLI_PRINTF("dRdi:           %s\n",status.dRdi ? "TRUE" : "FALSE");
    ICLI_PRINTF("dPeriod:        %s\n",status.dPeriod ? "TRUE" : "FALSE");
    ICLI_PRINTF("dPrio:          %s\n",status.dPrio ? "TRUE" : "FALSE");
    ICLI_PRINTF("dLoop:          %s\n",status.dLoop ? "TRUE" : "FALSE");
    ICLI_PRINTF("dConfig:        %s\n",status.dConfig ? "TRUE" : "FALSE");
    ICLI_PRINTF("if_tlv_value:   %d\n",status.if_tlv_value);
    ICLI_PRINTF("ps_tlv_value:   %d\n",status.ps_tlv_value);
    ICLI_PRINTF("valid_counter:  " VPRI64u "\n",status.valid_counter);
    ICLI_PRINTF("invalid_counter:" VPRI64u "\n",status.invalid_counter);
    ICLI_PRINTF("oo_counter:     " VPRI64u "\n",status.oo_counter);
}


BOOL mep_debug_mep_model_status(i32 session_id, u32 inst)
{
    u32                       i;
    BOOL                      found;
    mep_g2::mep_model_ccm_status_t    status;
    mep_g2::mep_model_mep_debug_t     debug;

    static char event_text[mep_g2::MEP_MODEL_EVENT_COUNT][40] = {{"MEP_MODEL_EVENT_CCM_STATUS_dInv"},
                                                         {"MEP_MODEL_EVENT_CCM_STATUS_dLevel"},
                                                         {"MEP_MODEL_EVENT_CCM_STATUS_dMeg"},
                                                         {"MEP_MODEL_EVENT_CCM_STATUS_dMep"},
                                                         {"MEP_MODEL_EVENT_CCM_STATUS_dLoc"},
                                                         {"MEP_MODEL_EVENT_CCM_STATUS_dRdi"},
                                                         {"MEP_MODEL_EVENT_CCM_STATUS_dPeriod"},
                                                         {"MEP_MODEL_EVENT_CCM_STATUS_dPrio"},
                                                         {"MEP_MODEL_EVENT_CCM_STATUS_dLoop"},
                                                         {"MEP_MODEL_EVENT_CCM_STATUS_dConfig"},
                                                         {"MEP_MODEL_EVENT_MEP_STATUS_dSsf"},
                                                         {"MEP_MODEL_EVENT_MEP_STATUS_ciSsf"},
                                                         {"MEP_MODEL_EVENT_MEP_STATUS_forwarding"},
                                                         {"MEP_MODEL_EVENT_MEP_STATUS_oper"},
                                                         {"MEP_MODEL_EVENT_CCM_STATUS_tlv_port"},
                                                         {"MEP_MODEL_EVENT_CCM_STATUS_tlv_if"}};

    static char oper_state_text[mep_g2::MEP_MODEL_OPER_COUNT][30] = {{"MEP_MODEL_OPER_UP"},
                                                             {"MEP_MODEL_OPER_DOWN"},
                                                             {"MEP_MODEL_OPER_INVALID_CONFIG"},
                                                             {"MEP_MODEL_OPER_HW_OAM"},
                                                             {"MEP_MODEL_OPER_MCE"}};

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (!mep_g2::mep_model_ccm_status_get(inst-1, &status)) {
        return FALSE;
    }
    if (!mep_g2::mep_model_mep_debug_get(inst-1, &debug)) {
        return FALSE;
    }

    ICLI_PRINTF("Event status:\n");
    found = FALSE;
    for (i=0; i<mep_g2::MEP_MODEL_EVENT_COUNT; ++i) {
        if (!debug.event_status.status[i]) {
            continue;
        }
        found = TRUE;
        ICLI_PRINTF("Event active: %s\n", event_text[i]);
    }
    if (!found) {
        ICLI_PRINTF("No Events found active\n");
    }
    ICLI_PRINTF("\n");

    ICLI_PRINTF("Last Get of info:\n");
    ICLI_PRINTF("dSsf:           %s\n",debug.last_info.dSsf ? "TRUE" : "FALSE");
    ICLI_PRINTF("ciSsf:          %s\n",debug.last_info.ciSsf ? "TRUE" : "FALSE");
    ICLI_PRINTF("forwarding:     %s\n",debug.last_info.forwarding ? "TRUE" : "FALSE");
    ICLI_PRINTF("\n");

    ICLI_PRINTF("Last Get of Operational State:\n");
    ICLI_PRINTF("Oper State:     %s\n", oper_state_text[debug.last_oper_state]);
    ICLI_PRINTF("\n");

    ICLI_PRINTF("Last Get of CCM status:\n");
    print_ccm_status(session_id, debug.last_ccm_status);
    ICLI_PRINTF("\n");

    ICLI_PRINTF("New CCM status:\n");
    print_ccm_status(session_id, status);
    ICLI_PRINTF("\n");

    return TRUE;
}

BOOL mep_no_debug_mep_raps_forward(i32 session_id, u32 inst)
{
    mep_conf_t                config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK)) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        /* debug mep <uint> raps forward */
        if ((rc = vtss_mep_raps_forwarding(inst-1, FALSE)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_os_tlv(i32 session_id,
                    u32 oui, u32 subtype, u32 value)
{
    vtss_appl_mep_tlv_conf_t   config;
    mesa_rc                    rc;

    /* mep <inst:uint> os-tlv oui <oui:oui> sub-type <subtype:0-0xFF> value <value:0-0xFF> */
    config.os_tlv.oui[0] = (oui & 0xFF0000) >> 16;
    config.os_tlv.oui[1] = (oui & 0xFF00) >> 8;
    config.os_tlv.oui[2] = (oui & 0xFF);
    config.os_tlv.subtype = subtype;
    config.os_tlv.value = value;

    if ((rc = vtss_appl_mep_tlv_conf_set(&config)) != VTSS_RC_OK) {
        mep_print_error(session_id, rc);
        return FALSE;
    }

    return TRUE;
}

BOOL mep_mep_ccm_tlv(i32 session_id, u32 inst)
{
    vtss_appl_mep_cc_conf_t   cc_config;
    mep_conf_t                config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK)) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        if ((rc = vtss_appl_mep_cc_conf_get(inst-1, &cc_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        if (cc_config.enable) {
        /* mep <inst:uint> ccm-tlv */
            cc_config.tlv_enable = TRUE;
            if ((rc = vtss_appl_mep_cc_conf_set(inst-1, &cc_config)) != VTSS_RC_OK) {
                mep_print_error(session_id, rc);
                return FALSE;
            }
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

BOOL mep_no_mep_ccm_tlv(i32 session_id, u32 inst)
{
    vtss_appl_mep_cc_conf_t   cc_config;
    mep_conf_t                config;
    mesa_rc                   rc;

    if (inst == 0) {
        ICLI_PRINTF("Invalid MEP instance number\n");
        return FALSE;
    }
    if (((rc = mep_instance_conf_get(inst-1, &config)) != VTSS_RC_OK)) {
        mep_print_error(session_id, rc);
        return FALSE;
    }
    if (config.enable) {
        if ((rc = vtss_appl_mep_cc_conf_get(inst-1, &cc_config)) != VTSS_RC_OK) {
            mep_print_error(session_id, rc);
            return FALSE;
        }
        if (cc_config.enable) {
        /* no mep <inst:uint> ccm-tlv */
            cc_config.tlv_enable = FALSE;
            if ((rc = vtss_appl_mep_cc_conf_set(inst-1, &cc_config)) != VTSS_RC_OK) {
                mep_print_error(session_id, rc);
                return FALSE;
            }
        }
    }
    else {
        ICLI_PRINTF("This MEP is not enabled\n");
        return FALSE;
    }

    return TRUE;
}

























































































































































































































































































/****************************************************************************/
/* ICFG callback functions */
/****************************************************************************/
#ifdef VTSS_SW_OPTION_ICFG
static mesa_rc mep_icfg_conf(const vtss_icfg_query_request_t  *req,
                             vtss_icfg_query_result_t         *result)
{
    u32 i, j, res_port;
    mep_conf_t                       config;
    vtss_appl_mep_pm_conf_t          pm_conf;
    vtss_appl_mep_lst_conf_t         lst_conf;
    vtss_appl_mep_syslog_conf_t      syslog_conf;
    vtss_appl_mep_cc_conf_t          cc_conf;
    vtss_appl_mep_lm_conf_t          lm_conf;
    vtss_appl_mep_lm_avail_conf_t    lm_avail_conf;
    vtss_appl_mep_lm_hli_conf_t      lm_hli_conf;
    vtss_appl_mep_lm_sdeg_conf_t     lm_sdeg_conf;
    vtss_appl_mep_dm_conf_t          dm_conf;
    vtss_appl_mep_aps_conf_t         aps_conf;
    vtss_appl_mep_lt_conf_t          lt_conf;
    vtss_appl_mep_lb_conf_t          lb_conf;
    vtss_appl_mep_ais_conf_t         ais_conf;
    vtss_appl_mep_lck_conf_t         lck_conf;
    vtss_appl_mep_tst_conf_t         tst_conf;
    mep_client_conf_t                client_conf;
    mep_default_conf_t               def_conf;
    vtss_appl_mep_tlv_conf_t         tlv_conf;
    mesa_mac_t                       all_zero_mac = {{0,0,0,0,0,0}};
    char                             buf[ICLI_PORTING_STR_BUF_SIZE];






    // Get default configuration
    mep_default_conf_get(&def_conf);
    config = def_conf.config;
    cc_conf = def_conf.cc_conf;
    pm_conf = def_conf.pm_conf;
    lst_conf = def_conf.lst_conf;
    syslog_conf = def_conf.syslog_conf;
    lm_conf = def_conf.lm_conf;
    lm_avail_conf = def_conf.lm_avail_conf;
    lm_hli_conf = def_conf.lm_hli_conf;
    lm_sdeg_conf = def_conf.lm_sdeg_conf;
    dm_conf = def_conf.dm_conf;
    aps_conf = def_conf.aps_conf;
    lt_conf = def_conf.lt_conf;
    lb_conf = def_conf.lb_conf;
    ais_conf = def_conf.ais_conf;
    lck_conf = def_conf.lck_conf;
    tst_conf = def_conf.tst_conf;
    tlv_conf = def_conf.tlv_conf;

    if ((vtss_appl_mep_tlv_conf_get(&tlv_conf)) == VTSS_RC_OK) {
        /* mep os-tlv oui <oui:0-0xFFFFFF> sub-type <subtype:0-0xFF> value <value:0-0xFF> */
        VTSS_RC(vtss_icfg_printf(result, "mep os-tlv oui 0x%X sub-type 0x%X value 0x%X", ((tlv_conf.os_tlv.oui[0]<<16) + (tlv_conf.os_tlv.oui[1]<<8) + (tlv_conf.os_tlv.oui[2])), tlv_conf.os_tlv.subtype, tlv_conf.os_tlv.value));
        VTSS_RC(vtss_icfg_printf(result, "\n"));
    }































    u32 vtss_appl_mep_instance_max = VTSS_APPL_MEP_INSTANCE_MAX;
    for (i = 0; i < vtss_appl_mep_instance_max; ++i) {
        if ((mep_instance_conf_get(i, &config)) != VTSS_RC_OK) {
            continue;
        }
        if (config.enable) {
            /* mep <inst:uint> [mip] {up|down} domain {evc|vlan|port|tp-link|tunnel-tp|pw|lsp} [vid <vid:vlan_id>] [flow <flow:uint>] level <level:0-7> interface <port:port_type_id> */
            VTSS_RC(vtss_icfg_printf(result, "mep %u", i+1));
            if ((config.mode != def_conf.config.mode) || req->all_defaults) {
                VTSS_RC(vtss_icfg_printf(result, " %s", config.mode == VTSS_APPL_MEP_MIP ? "mip" : "mep"));
            }
            res_port = config.port;

            VTSS_RC(vtss_icfg_printf(result, " %s", config.direction == VTSS_APPL_MEP_DOWN ? "down" : "up"));









            VTSS_RC(vtss_icfg_printf(result, " domain %s", config.domain == VTSS_APPL_MEP_PORT ? "port" :
                                     config.domain == VTSS_APPL_MEP_EVC ? "evc" :
                                     config.domain == VTSS_APPL_MEP_VLAN ? "vlan" : "port"));

            if (((config.domain == VTSS_APPL_MEP_PORT) && (config.direction == VTSS_APPL_MEP_UP) && (config.vid != 0)) ||
                ((config.domain == VTSS_APPL_MEP_EVC) && (config.vid != 0))) {
                VTSS_RC(vtss_icfg_printf(result, " vid %u", config.vid));
            }
            if ((config.domain == VTSS_APPL_MEP_VLAN)      || (config.domain == VTSS_APPL_MEP_EVC)         ||




                FALSE) {
                VTSS_RC(vtss_icfg_printf(result, " flow %u", (config.domain == VTSS_APPL_MEP_EVC) ? config.flow+1 : config.flow));
            }
            VTSS_RC(vtss_icfg_printf(result, " level %u", config.level));




            {
                VTSS_RC(vtss_icfg_printf(result, " interface %s", icli_port_info_txt(VTSS_USID_START, iport2uport(res_port), buf)));
            }
            VTSS_RC(vtss_icfg_printf(result, "\n"));
            if ((config.format != def_conf.config.format) || memcmp(config.meg, def_conf.config.meg, VTSS_APPL_MEP_MEG_CODE_LENGTH) ||
                ((config.format == VTSS_APPL_MEP_IEEE_STR) && (config.name[0] != '\0') && memcmp(config.name, def_conf.config.name, VTSS_APPL_MEP_MEG_CODE_LENGTH)) || req->all_defaults) {
                /* mep <inst:uint> meg-id <megid:word> {itu | itu-cc | {ieee [name <name:word>]}} */
                VTSS_RC(vtss_icfg_printf(result, "mep %u", i+1));
                config.meg[VTSS_APPL_MEP_MEG_CODE_LENGTH-1] = '\0'; /* This must be nulterminated string otherwise printf will lead to exception */
                config.name[VTSS_APPL_MEP_MEG_CODE_LENGTH-1] = '\0'; /* This must be nulterminated string otherwise printf will lead to exception */
                VTSS_RC(vtss_icfg_printf(result, " meg-id %s", config.meg));
                VTSS_RC(vtss_icfg_printf(result, " %s", config.format == VTSS_APPL_MEP_ITU_ICC ? "itu" :
                                                        config.format == VTSS_APPL_MEP_IEEE_STR ? "ieee" :
                                                        config.format == VTSS_APPL_MEP_ITU_CC_ICC ? "itu-cc" : "itu"));
                if ((config.format == VTSS_APPL_MEP_IEEE_STR) && (config.name[0] != '\0') && (memcmp(config.name, def_conf.config.name, VTSS_APPL_MEP_MEG_CODE_LENGTH) || req->all_defaults)) {
                    VTSS_RC(vtss_icfg_printf(result, " name %s", config.format == VTSS_APPL_MEP_IEEE_STR ? config.name : ""));
                }
                VTSS_RC(vtss_icfg_printf(result, "\n"));
            }
            if ((config.mep != def_conf.config.mep) || req->all_defaults) {
                /* mep <inst:uint> mep-id <mepid:uint> */
                VTSS_RC(vtss_icfg_printf(result, "mep %u", i+1));
                VTSS_RC(vtss_icfg_printf(result, " mep-id %u", config.mep));
                VTSS_RC(vtss_icfg_printf(result, "\n"));
            }
            if ((config.vid != def_conf.config.vid) || req->all_defaults) {
                /* mep <inst:uint> vid <vid:vlan_id> */
                VTSS_RC(vtss_icfg_printf(result, "mep %u", i+1));
                VTSS_RC(vtss_icfg_printf(result, " vid %u", config.vid));
                VTSS_RC(vtss_icfg_printf(result, "\n"));
            }
            if (VTSS_APPL_MEP_SW_OAM_SUPPORT && VTSS_APPL_MEP_VOE_SUPPORTED && config.voe) {
                /* mep <inst:uint> voe */
                VTSS_RC(vtss_icfg_printf(result, "mep %u", i+1));
                VTSS_RC(vtss_icfg_printf(result, " voe"));
                VTSS_RC(vtss_icfg_printf(result, "\n"));
            }
            /* mep <inst:uint> peer-mep-id <mepid:uint> [mac <mac:mac_addr>] */
            for (j=0; j<config.peer_count; ++j) {
                VTSS_RC(vtss_icfg_printf(result, "mep %u", i+1));
                VTSS_RC(vtss_icfg_printf(result, " peer-mep-id %u", config.peer_mep[j]));
                if (memcmp(&config.peer_mac[j], &def_conf.config.peer_mac[j], sizeof(config.peer_mac[j])) || req->all_defaults) {
                    VTSS_RC(vtss_icfg_printf(result, " mac %02X-%02X-%02X-%02X-%02X-%02X", config.peer_mac[j].addr[0], config.peer_mac[j].addr[1], config.peer_mac[j].addr[2],
                                                                                           config.peer_mac[j].addr[3], config.peer_mac[j].addr[4], config.peer_mac[j].addr[5]));
                }
                VTSS_RC(vtss_icfg_printf(result, "\n"));
            }

            if ((vtss_appl_mep_pm_conf_get(i, &pm_conf)) == VTSS_RC_OK) {
                if (pm_conf.enable) {
                    /* mep <inst:uint> performance-monitoring */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u", i+1));
                    VTSS_RC(vtss_icfg_printf(result, " performance-monitoring"));
                    VTSS_RC(vtss_icfg_printf(result, "\n"));
                }
            }

            if ((vtss_appl_mep_syslog_conf_get(i, &syslog_conf)) == VTSS_RC_OK) {
                if (syslog_conf.enable) {
                    /* mep <inst:uint> syslog */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u", i+1));
                    VTSS_RC(vtss_icfg_printf(result, " syslog"));
                    VTSS_RC(vtss_icfg_printf(result, "\n"));
                }
            }

            if ((vtss_appl_mep_cc_conf_get(i, &cc_conf)) == VTSS_RC_OK) {
                if ((cc_conf.enable) || (cc_conf.prio != def_conf.cc_conf.prio) || (cc_conf.rate != def_conf.cc_conf.rate) || req->all_defaults) {
                    /* mep <inst:uint> cc <prio:0-7> [fr300s|fr100s|fr10s|fr1s|fr6m|fr1m|fr6h] [rx-only] */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u cc", i+1));
                    VTSS_RC(vtss_icfg_printf(result, " %u", cc_conf.prio));
                    if ((cc_conf.rate != def_conf.cc_conf.rate) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " %s", cc_conf.rate == VTSS_APPL_MEP_RATE_300S ? "fr300s" :
                                                                cc_conf.rate == VTSS_APPL_MEP_RATE_100S ? "fr100s" :
                                                                cc_conf.rate == VTSS_APPL_MEP_RATE_10S ? "fr10s" :
                                                                cc_conf.rate == VTSS_APPL_MEP_RATE_1S ? "fr1s" :
                                                                cc_conf.rate == VTSS_APPL_MEP_RATE_6M ? "fr6m" :
                                                                cc_conf.rate == VTSS_APPL_MEP_RATE_1M ? "fr1m" : "fr6h"));
                    }
                    if (!cc_conf.enable) {
                        VTSS_RC(vtss_icfg_printf(result, " rx-only"));
                    }
                    VTSS_RC(vtss_icfg_printf(result, "\n"));
                    if (cc_conf.tlv_enable) {
                        /* mep <inst:uint> ccm-tlv */
                        VTSS_RC(vtss_icfg_printf(result, "mep %u", i+1));
                        VTSS_RC(vtss_icfg_printf(result, " ccm-tlv"));
                        VTSS_RC(vtss_icfg_printf(result, "\n"));
                    }
                }
            }

            if ((vtss_appl_mep_lm_avail_conf_get(i, &lm_avail_conf)) == VTSS_RC_OK) {
                if (lm_avail_conf.enable) {
                    /* mep <uint> lm-avail interval <uint> flr-threshold <0-1000> */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u lm-avail interval %u flr-threshold %u\n", i + 1, lm_avail_conf.interval, lm_avail_conf.flr_th));
                    if (lm_avail_conf.main) {
                        /* mep <uint> lm-avail maintenance */
                        VTSS_RC(vtss_icfg_printf(result, "mep %u lm-avail maintenance\n", i + 1));
                    }
                    else if (req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, "no mep %u lm-avail maintenance\n", i + 1));
                    }
                }
            }

            if ((vtss_appl_mep_lm_conf_get(i, &lm_conf)) == VTSS_RC_OK) {
                if (lm_conf.enable) {
                    /* mep <inst> lm <prio> [ synthetic ] [ multi | { uni [ mep-id <mepid> ] } ] [ single | dual ] [ fr10s | fr1s | fr6m | fr1m | fr6h ] [ size <size> ] [ flr <flr> ] [ loss-ratio-threshold <loss_ratio_threshold> ] */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u lm", i+1));
                    VTSS_RC(vtss_icfg_printf(result, " %u", lm_conf.prio));
                    if (lm_conf.synthetic && ((lm_conf.synthetic != def_conf.lm_conf.synthetic) || req->all_defaults)) {
                        VTSS_RC(vtss_icfg_printf(result, " synthetic"));
                    }
                    if ((lm_conf.cast != def_conf.lm_conf.cast) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " %s", lm_conf.cast == VTSS_APPL_MEP_UNICAST ? "uni" : "multi"));
                    }
                    if (lm_conf.synthetic && (lm_conf.cast == VTSS_APPL_MEP_UNICAST) && ((lm_conf.mep != def_conf.lm_conf.mep) || req->all_defaults)) {
                        VTSS_RC(vtss_icfg_printf(result, " mep-id %u", lm_conf.mep));
                    }
                    if ((lm_conf.ended != def_conf.lm_conf.ended) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " %s", lm_conf.ended == VTSS_APPL_MEP_SINGEL_ENDED ? "single" : "dual"));
                    }
                    if ((lm_conf.rate != def_conf.lm_conf.rate) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " %s", lm_conf.rate == VTSS_APPL_MEP_RATE_100S ? "fr100s" :
                                                                lm_conf.rate == VTSS_APPL_MEP_RATE_10S ? "fr10s" :
                                                                lm_conf.rate == VTSS_APPL_MEP_RATE_1S ? "fr1s" :
                                                                lm_conf.rate == VTSS_APPL_MEP_RATE_6M ? "fr6m" : "fr1s"));
                    }
                    if (lm_conf.synthetic && ((lm_conf.size != def_conf.lm_conf.size) || req->all_defaults)) {
                        VTSS_RC(vtss_icfg_printf(result, " size %u", lm_conf.size));
                    }
                    if ((lm_conf.flr_interval != def_conf.lm_conf.flr_interval) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " flr %u", lm_conf.flr_interval));
                    }
                    if ((lm_conf.meas_interval != def_conf.lm_conf.meas_interval) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " meas %u", lm_conf.meas_interval));
                    }
                    if ((lm_conf.loss_th != def_conf.lm_conf.loss_th) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " threshold %u", lm_conf.loss_th));
                    }
                    if (lm_conf.synthetic && ((lm_conf.slm_test_id != def_conf.lm_conf.slm_test_id) || req->all_defaults)) {
                        VTSS_RC(vtss_icfg_printf(result, " slm-testid %u", lm_conf.slm_test_id));
                    }

                    VTSS_RC(vtss_icfg_printf(result, "\n"));
                }

                if (lm_conf.enable_rx) {
                    /* mep <uint> lm rx [ flr <uint> ] [ loss-ratio-threshold <1-100> ] */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u lm rx", i+1));
                    if (lm_conf.synthetic && ((lm_conf.synthetic != def_conf.lm_conf.synthetic) || req->all_defaults)) {
                        VTSS_RC(vtss_icfg_printf(result, " synthetic"));
                    }
                    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
                        if ((lm_conf.prio != def_conf.lm_conf.prio) || req->all_defaults) {
                            VTSS_RC(vtss_icfg_printf(result, " prio %u", lm_conf.prio));
                        }
                    }
                    if (mep_caps.mesa_mep_serval) {
                        if ((lm_conf.rate != def_conf.lm_conf.rate) || req->all_defaults) {
                            VTSS_RC(vtss_icfg_printf(result, " %s", lm_conf.rate == VTSS_APPL_MEP_RATE_100S ? "fr100s" :
                                                                    lm_conf.rate == VTSS_APPL_MEP_RATE_10S ? "fr10s" :
                                                                    lm_conf.rate == VTSS_APPL_MEP_RATE_1S ? "fr1s" :
                                                                    lm_conf.rate == VTSS_APPL_MEP_RATE_6M ? "fr6m" : "fr1s"));
                        }
                    }
                    if ((lm_conf.meas_interval != def_conf.lm_conf.meas_interval) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " meas %u", lm_conf.meas_interval));
                    }
                    if ((lm_conf.loss_th != def_conf.lm_conf.loss_th) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " threshold %u", lm_conf.loss_th));
                    }
                    VTSS_RC(vtss_icfg_printf(result, "\n"));
                }

                /* mep <uint> lm-notif los-int-cnt-holddown <uint> los-th-cnt-holddown <uint> hli-cnt-holddown <uint> */
                if ((vtss_appl_mep_lm_hli_conf_get(i, &lm_hli_conf)) == VTSS_RC_OK &&
                    (lm_conf.los_int_cnt_holddown != 0 || lm_conf.los_th_cnt_holddown != 0 || lm_hli_conf.cnt_holddown != 0)) {
                    VTSS_RC(vtss_icfg_printf(result, "mep %u lm-notif los-int-cnt-holddown %u los-th-cnt-holddown %u hli-cnt-holddown %u\n", i+1,
                                             lm_conf.los_int_cnt_holddown, lm_conf.los_th_cnt_holddown, lm_hli_conf.cnt_holddown));
                }
            }

            if ((vtss_appl_mep_lm_hli_conf_get(i, &lm_hli_conf)) == VTSS_RC_OK) {
                if (lm_hli_conf.enable) {
                    /* mep <uint> lm-hli flr-threshold <0-1000> interval <uint> */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u lm-hli flr-threshold %u interval %u\n", i+1, lm_hli_conf.flr_th, lm_hli_conf.con_int));
                }
            }

            if ((vtss_appl_mep_lm_sdeg_conf_get(i, &lm_sdeg_conf)) == VTSS_RC_OK) {
                if (lm_sdeg_conf.enable) {
                    /* mep <uint> lm-sdeg tx-min <uint> flr-threshold <0-1000> bad-threshold <uint> good-threshold <uint> */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u lm-sdeg tx-min %u flr-threshold %u bad-threshold %u good-threshold %u\n",
                                             i+1, lm_sdeg_conf.tx_min, lm_sdeg_conf.flr_th, lm_sdeg_conf.bad_th, lm_sdeg_conf.good_th));
                }
            }

            if ((vtss_appl_mep_dm_conf_get(i, &dm_conf)) == VTSS_RC_OK) {
                if (dm_conf.enable) {
                    /* mep <inst:uint> dm <prio:0-7> [multi|{uni mep-id <mepid:uint>}] [single|dual] [rdtrp|flow] interval <interval:uint> last-n <lastn:uint> */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u dm", i+1));
                    VTSS_RC(vtss_icfg_printf(result, " %u", dm_conf.prio));
                    if ((dm_conf.cast != def_conf.dm_conf.cast) || req->all_defaults) {
                        if (dm_conf.cast == VTSS_APPL_MEP_MULTICAST) {
                            VTSS_RC(vtss_icfg_printf(result, " multi"));
                        } else {
                            VTSS_RC(vtss_icfg_printf(result, " uni mep-id %u", dm_conf.mep));
                        }
                    }
                    if ((dm_conf.ended != def_conf.dm_conf.ended) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " %s", dm_conf.ended == VTSS_APPL_MEP_DUAL_ENDED ? "dual" : "single"));
                    }
                    if ((dm_conf.calcway != def_conf.dm_conf.calcway) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " %s", dm_conf.calcway == VTSS_APPL_MEP_RDTRP ? "rdtrp" : "flow"));
                    }
                    VTSS_RC(vtss_icfg_printf(result, " interval %u", dm_conf.interval));
                    VTSS_RC(vtss_icfg_printf(result, " last-n %u", dm_conf.lastn));
                    VTSS_RC(vtss_icfg_printf(result, "\n"));
                    /* mep <inst:uint> dm overflow-reset*/
                    if (dm_conf.overflow_act == VTSS_APPL_MEP_CONTINUE) {
                        VTSS_RC(vtss_icfg_printf(result, "mep %u dm", i+1));
                        VTSS_RC(vtss_icfg_printf(result, " overflow-reset"));
                        VTSS_RC(vtss_icfg_printf(result, "\n"));
                    }
                    /* mep <inst:uint> dm ns */
                    if (dm_conf.tunit == VTSS_APPL_MEP_NS) {
                        VTSS_RC(vtss_icfg_printf(result, "mep %u dm", i+1));
                        VTSS_RC(vtss_icfg_printf(result, " ns"));
                        VTSS_RC(vtss_icfg_printf(result, "\n"));
                    }
                    /* mep <inst:uint> dm synchronized */
                    if (dm_conf.synchronized) {
                        VTSS_RC(vtss_icfg_printf(result, "mep %u dm", i+1));
                        VTSS_RC(vtss_icfg_printf(result, " synchronized"));
                        VTSS_RC(vtss_icfg_printf(result, "\n"));
                    }
                    /* mep <inst:uint> dm proprietary */
                    if (dm_conf.proprietary) {
                        VTSS_RC(vtss_icfg_printf(result, "mep %u dm", i+1));
                        VTSS_RC(vtss_icfg_printf(result, " proprietary"));
                        VTSS_RC(vtss_icfg_printf(result, "\n"));
                    }

                    /* mep <inst:uint> dm bin fd <2-10> */
                    if (dm_conf.num_of_bin_fd != def_conf.dm_conf.num_of_bin_fd) {
                        VTSS_RC(vtss_icfg_printf(result, "mep %u dm", i+1));
                        VTSS_RC(vtss_icfg_printf(result, " bin fd %u", dm_conf.num_of_bin_fd));
                        VTSS_RC(vtss_icfg_printf(result, "\n"));
                    }
                    /* mep <inst:uint> dm bin ifdv <2-10> */
                    if (dm_conf.num_of_bin_ifdv != def_conf.dm_conf.num_of_bin_ifdv) {
                        VTSS_RC(vtss_icfg_printf(result, "mep %u dm", i+1));
                        VTSS_RC(vtss_icfg_printf(result, " bin ifdv %u", dm_conf.num_of_bin_ifdv));
                        VTSS_RC(vtss_icfg_printf(result, "\n"));
                    }
                    /* mep <inst:uint> dm bin threshold <1-50000> */
                    if (dm_conf.m_threshold != def_conf.dm_conf.m_threshold) {
                        VTSS_RC(vtss_icfg_printf(result, "mep %u dm", i+1));
                        VTSS_RC(vtss_icfg_printf(result, " bin threshold %u", dm_conf.m_threshold));
                        VTSS_RC(vtss_icfg_printf(result, "\n"));
                    }
                }
            }

            if ((vtss_appl_mep_aps_conf_get(i, &aps_conf)) == VTSS_RC_OK) {
                if (aps_conf.enable) {
                    /* mep <inst:uint> aps <prio:0-7> [multi|uni] {laps|{raps [octet <octet:uint>]}} */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u aps", i+1));
                    VTSS_RC(vtss_icfg_printf(result, " %u", aps_conf.prio));
                    if ((aps_conf.cast != def_conf.aps_conf.cast) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " %s", aps_conf.cast == VTSS_APPL_MEP_UNICAST ? "uni" : "multi"));
                    }
                    VTSS_RC(vtss_icfg_printf(result, " %s", aps_conf.type == VTSS_APPL_MEP_L_APS ? "laps" : "raps"));
                    if ((aps_conf.raps_octet != def_conf.aps_conf.raps_octet) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " octet %u", aps_conf.raps_octet));
                    }
                    VTSS_RC(vtss_icfg_printf(result, "\n"));
                }
            }

            if ((vtss_appl_mep_lt_conf_get(i, &lt_conf)) == VTSS_RC_OK) {
                if (lt_conf.enable) {
                    /*mep <inst:uint> lt <prio:0-7> {{mep-id <mepid:uint>} | {mac <mac:mac_addr>}} ttl <ttl:uint> */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u lt", i+1));
                    VTSS_RC(vtss_icfg_printf(result, " %u", lt_conf.prio));
                    if (!memcmp(&all_zero_mac, &lt_conf.mac, sizeof(all_zero_mac))) {
                        VTSS_RC(vtss_icfg_printf(result, " mep-id %u", lt_conf.mep));
                    } else {
                        VTSS_RC(vtss_icfg_printf(result, " mac %02X-%02X-%02X-%02X-%02X-%02X", lt_conf.mac.addr[0], lt_conf.mac.addr[1], lt_conf.mac.addr[2], lt_conf.mac.addr[3], lt_conf.mac.addr[4], lt_conf.mac.addr[5]));
                    }
                    VTSS_RC(vtss_icfg_printf(result, "\n"));
                }
            }

            if ((vtss_appl_mep_lb_conf_get(i, &lb_conf)) == VTSS_RC_OK) {
                if (lb_conf.enable) {
                    /* mep <inst:uint> lb <prio:0-7> [dei] [multi|{uni {{mep-id <mepid:uint>} | {mac <mac:mac_addr>}}} | mpls ttl <mpls_ttl:uint>] count <count:uint> size <size:uint> interval <interval:uint> */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u lb", i+1));
                    VTSS_RC(vtss_icfg_printf(result, " %u", lb_conf.prio));
                    if ((lb_conf.dei != def_conf.lb_conf.dei) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " %s", lb_conf.dei ? "dei" : ""));
                    }
                    if ((lb_conf.cast != def_conf.lb_conf.cast) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " %s", (lb_conf.cast == VTSS_APPL_MEP_UNICAST) ? "uni" :
                                                                (lb_conf.cast == VTSS_APPL_MEP_MULTICAST) ? "multi" : "uni"));
                    }
                    if (lb_conf.cast == VTSS_APPL_MEP_UNICAST) {
                        if (!memcmp(&all_zero_mac, &lb_conf.mac, sizeof(all_zero_mac))) {
                            VTSS_RC(vtss_icfg_printf(result, " mep-id %u", lb_conf.mep));
                        } else {
                            VTSS_RC(vtss_icfg_printf(result, " mac %02X-%02X-%02X-%02X-%02X-%02X", lb_conf.mac.addr[0], lb_conf.mac.addr[1], lb_conf.mac.addr[2], lb_conf.mac.addr[3], lb_conf.mac.addr[4], lb_conf.mac.addr[5]));
                        }
                    }






                    VTSS_RC(vtss_icfg_printf(result, " count %u", lb_conf.to_send));
                    VTSS_RC(vtss_icfg_printf(result, " size %u", lb_conf.size));
                    VTSS_RC(vtss_icfg_printf(result, " interval %u", lb_conf.interval));
                    VTSS_RC(vtss_icfg_printf(result, "\n"));
                }
            }

            if ((vtss_appl_mep_tst_conf_get(i, &tst_conf)) == VTSS_RC_OK) {
                if ((tst_conf.prio != def_conf.tst_conf.prio) || (tst_conf.dei != def_conf.tst_conf.dei) || (tst_conf.mep != def_conf.tst_conf.mep) ||
                    (tst_conf.rate != def_conf.tst_conf.rate) || (tst_conf.size != def_conf.tst_conf.size) || (tst_conf.pattern != def_conf.tst_conf.pattern) ||
                    (tst_conf.sequence != def_conf.tst_conf.sequence) || req->all_defaults) {
                    /* mep <inst:uint> tst <prio:0-7> [dei] mep-id <mepid:uint> [sequence] [all-zero|all-one|one-zero] rate <rate:uint> size <size:uint> */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u tst", i+1));
                    VTSS_RC(vtss_icfg_printf(result, " %u", tst_conf.prio));
                    if ((tst_conf.dei != def_conf.tst_conf.dei) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " %s", tst_conf.dei ? "dei" : ""));
                    }
                    VTSS_RC(vtss_icfg_printf(result, " mep-id %u", tst_conf.mep));
                    if ((tst_conf.sequence != def_conf.tst_conf.sequence) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " %s", tst_conf.sequence ? "sequence" : ""));
                    }
                    if ((tst_conf.pattern != def_conf.tst_conf.pattern) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " %s", tst_conf.pattern == VTSS_APPL_MEP_PATTERN_ALL_ZERO ? "all-zero" :
                                                                tst_conf.pattern == VTSS_APPL_MEP_PATTERN_ALL_ONE ? "all-one" : "one-zero"));
                    }
                    VTSS_RC(vtss_icfg_printf(result, " rate %u", tst_conf.rate));
                    VTSS_RC(vtss_icfg_printf(result, " size %u", tst_conf.size));
                    VTSS_RC(vtss_icfg_printf(result, "\n"));
                }
                if (tst_conf.enable) {
                    /* mep <inst:uint> tst tx */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u tst tx", i+1));
                    VTSS_RC(vtss_icfg_printf(result, "\n"));
                }
                if (tst_conf.enable_rx) {
                    /* mep <inst:uint> tst rx */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u tst rx", i+1));
                    VTSS_RC(vtss_icfg_printf(result, "\n"));
                }
            }

            if ((mep_client_conf_get(i, &client_conf)) == VTSS_RC_OK) {
                for (j=0; j<client_conf.flow_count && j<VTSS_APPL_MEP_CLIENT_FLOWS_MAX; ++j) {
                    /* mep <uint> client domain { evc | vlan | lsp } flow <uint> [ level <0-7> ] [ ais-prio [ <0-7> | ais-highest ] ] [ lck-prio [ <0-7> | lck-highest ] ] */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u client domain", i+1));
                    VTSS_RC(vtss_icfg_printf(result, " %s", client_conf.domain[j] == VTSS_APPL_MEP_EVC ? "evc" :
                                             client_conf.domain[j] == VTSS_APPL_MEP_VLAN ? "vlan" :
                                             client_conf.domain[j] == VTSS_APPL_MEP_MPLS_LSP ? "lsp" : "evc"));
                    VTSS_RC(vtss_icfg_printf(result, " flow %u", (client_conf.domain[j] == VTSS_APPL_MEP_EVC) ? client_conf.flows[j]+1 : client_conf.flows[j]));
                    if ((client_conf.level[j] != def_conf.client_flow_conf.level) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " level %u", client_conf.level[j]));
                    }
                    if ((client_conf.ais_prio[j] != def_conf.client_flow_conf.ais_prio) || req->all_defaults) {
                        if (client_conf.ais_prio[j] == VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST) {
                            VTSS_RC(vtss_icfg_printf(result, " ais-prio ais-highest"));
                        } else {
                            VTSS_RC(vtss_icfg_printf(result, " ais-prio %u", client_conf.ais_prio[j]));
                        }
                    }
                    if ((client_conf.lck_prio[j] != def_conf.client_flow_conf.lck_prio) || req->all_defaults) {
                        if (client_conf.lck_prio[j] == VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST) {
                            VTSS_RC(vtss_icfg_printf(result, " lck-prio lck-highest"));
                        } else {
                            VTSS_RC(vtss_icfg_printf(result, " lck-prio %u", client_conf.lck_prio[j]));
                        }
                    }
                    VTSS_RC(vtss_icfg_printf(result, "\n"));
                }
            }

            if ((vtss_appl_mep_ais_conf_get(i, &ais_conf)) == VTSS_RC_OK) {
                if (ais_conf.enable) {
                    /* mep <inst:uint> ais [fr1s|fr1m] [protect] */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u ais", i+1));
                    if ((ais_conf.rate != def_conf.ais_conf.rate) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " %s", ais_conf.rate == VTSS_APPL_MEP_RATE_1S ? "fr1s" : "fr1m"));
                    }
                    if ((ais_conf.protection != def_conf.ais_conf.protection) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " %s", ais_conf.protection ? "protect" : ""));
                    }
                    VTSS_RC(vtss_icfg_printf(result, "\n"));
                }
            }

            if ((vtss_appl_mep_lck_conf_get(i, &lck_conf)) == VTSS_RC_OK) {
                if (lck_conf.enable) {
                    /* mep <inst:uint> lck [fr1s|fr1m] */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u lck", i+1));
                    if ((lck_conf.rate != def_conf.lck_conf.rate) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, " %s", lck_conf.rate == VTSS_APPL_MEP_RATE_1S ? "fr1s" : "fr1m"));
                    }
                    VTSS_RC(vtss_icfg_printf(result, "\n"));
                }
            }








































            if ((vtss_appl_mep_lst_conf_get(i, &lst_conf)) == VTSS_RC_OK) {
                if (lst_conf.enable) {
                    /* mep <inst:uint> link-state-tracking */
                    VTSS_RC(vtss_icfg_printf(result, "mep %u", i+1));
                    VTSS_RC(vtss_icfg_printf(result, " link-state-tracking"));
                    VTSS_RC(vtss_icfg_printf(result, "\n"));
                }
            }
        }
    }

    return VTSS_RC_OK;
}

/* ICFG Initialization function */
mesa_rc mep_icfg_init(void)
{
    VTSS_RC(vtss_icfg_query_register(VTSS_ICFG_MEP_GLOBAL_CONF, "mep", mep_icfg_conf));
    return VTSS_RC_OK;
}
#endif // VTSS_SW_OPTION_ICFG
/****************************************************************************/
/*  End of file.                                                            */
/****************************************************************************/
