/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#ifndef _MEP_SERIALIZER_HXX_
#define _MEP_SERIALIZER_HXX_

#include "vtss_appl_serialize.hxx"
#include "vtss/appl/mep.h"
#include "vtss/basics/memcmp-operator.hxx"
#include "mep_api.h"

mesa_rc vtss_appl_mep_instance_conf_default(u32 *k, vtss_appl_mep_conf_t *s);
mesa_rc vtss_appl_mep_instance_peer_conf_default(u32 *k1, u32 *k2, vtss_appl_mep_peer_conf_t *s);
mesa_rc vtss_appl_mep_cc_conf_default(u32 *k, vtss_appl_mep_cc_conf_t *s);
mesa_rc vtss_appl_mep_lm_conf_default(u32 *k, vtss_appl_mep_lm_conf_t *s);
mesa_rc vtss_appl_mep_lm_avail_conf_default(u32 *k, vtss_appl_mep_lm_avail_conf_t *s);
mesa_rc vtss_appl_mep_lm_hli_conf_default(u32 *k, vtss_appl_mep_lm_hli_conf_t *s);
mesa_rc vtss_appl_mep_lm_sdeg_conf_default(u32 *k, vtss_appl_mep_lm_sdeg_conf_t *s);
mesa_rc vtss_appl_mep_dm_conf_default(u32 *k, vtss_appl_mep_dm_conf_t *s);
mesa_rc vtss_appl_mep_aps_conf_default(u32 *k, vtss_appl_mep_aps_conf_t *s);
mesa_rc vtss_appl_mep_lt_conf_default(u32 *k, vtss_appl_mep_lt_conf_t *s);
mesa_rc vtss_appl_mep_lb_conf_default(u32 *k, vtss_appl_mep_lb_conf_t *s);
mesa_rc vtss_appl_mep_ais_conf_default(u32 *k, vtss_appl_mep_ais_conf_t *s);
mesa_rc vtss_appl_mep_lck_conf_default(u32 *k, vtss_appl_mep_lck_conf_t *s);
mesa_rc vtss_appl_mep_tst_conf_default(u32 *k, vtss_appl_mep_tst_conf_t *s);
mesa_rc vtss_appl_mep_client_flow_conf_default(u32 *k1, vtss_ifindex_t *k2, vtss_appl_mep_client_flow_conf_t *s);
mesa_rc vtss_appl_mep_g8113_2_bfd_conf_default(u32 *k, vtss_appl_mep_g8113_2_bfd_conf_t *s);
mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_default(u32 *k, vtss_appl_mep_g8113_2_bfd_auth_conf_t *s);
mesa_rc vtss_appl_mep_rt_conf_default(u32 *k, vtss_appl_mep_rt_conf_t *s);

#define VTSS_PLING(x) #x
#define VTSS_I_TO_S(x) VTSS_PLING(x)


VTSS_BASICS_MEMCMP_OPERATOR(vtss_appl_mep_state_t);

extern vtss::expose::TableStatus<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_state_t *>
> mep_status;


VTSS_BASICS_MEMCMP_OPERATOR(vtss_appl_mep_peer_state_t);

extern vtss::expose::TableStatus<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_peer_state_t *>
> mep_status_peer;


VTSS_BASICS_MEMCMP_OPERATOR(vtss_appl_mep_lm_notif_state_t);

extern vtss::expose::TableStatus<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lm_notif_state_t *>
> mep_status_lm_notif;


VTSS_BASICS_MEMCMP_OPERATOR(vtss_appl_mep_lm_hli_state_t);

extern vtss::expose::TableStatus<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lm_hli_state_t *>
> mep_status_lm_hli;


extern vtss_enum_descriptor_t mep_mode_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_mode_t,
                         "MepInstanceMode",
                         mep_mode_txt,
                         "The instance mode.");

extern vtss_enum_descriptor_t mep_format_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_format_t,
                         "MepMegIdFormat",
                         mep_format_txt,
                         "The MEG-ID format.");

extern vtss_enum_descriptor_t mep_cast_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_cast_t,
                         "MepCast",
                         mep_cast_txt,
                         "Unicast or Multicast selection.");

extern vtss_enum_descriptor_t mep_ended_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_ended_t,
                         "MepEnded",
                         mep_ended_txt,
                         "Dual/single ended selection.");

extern vtss_enum_descriptor_t mep_calcway_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_dm_calcway_t,
                         "MepDmCalcWay",
                         mep_calcway_txt,
                         "Round trip or flow calculation way selection.");

extern vtss_enum_descriptor_t mep_act_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_dm_act_t,
                         "MepDmOverflowAction",
                         mep_act_txt,
                         "Action when internal total delay counter overflow.");

extern vtss_enum_descriptor_t mep_relay_act_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_relay_act_t,
                         "MepLtRelayAction",
                         mep_relay_act_txt,
                         "The Link Trace relay action.");

extern vtss_enum_descriptor_t mep_tst_pattern_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_pattern_t,
                         "MepTstPattern",
                         mep_tst_pattern_txt,
                         "Pattern in TST frame Data TLV.");

extern vtss_enum_descriptor_t mep_aps_type_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_aps_type_t,
                         "MepApsType",
                         mep_aps_type_txt,
                         "Type of APS.");

extern vtss_enum_descriptor_t mep_bfd_session_state_type_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_bfd_session_state_t,
                         "MepBfdSessionState",
                         mep_bfd_session_state_type_txt,
                         "State of BFD session.");

extern vtss_enum_descriptor_t mep_bfd_diag_type_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_bfd_diag_t,
                         "MepBfdDiagnosticCode",
                         mep_bfd_diag_type_txt,
                         "BFD diagnostic code.");

extern vtss_enum_descriptor_t mep_bfd_auth_type_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_bfd_auth_type_t,
                         "MepBfdAuthenticationType",
                         mep_bfd_auth_type_txt,
                         "BFD authentication type.");

extern vtss_enum_descriptor_t mep_rt_pad_tlv_type_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_rt_pad_tlv_type_t,
                         "MepRtPadTlvType",
                         mep_rt_pad_tlv_type_txt,
                         "RT Pad TLV type.");

extern vtss_enum_descriptor_t mep_rt_ret_code_type_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_rt_ret_code_t,
                         "MepRtReturnCode",
                         mep_rt_ret_code_type_txt,
                         "RT return code type.");

extern vtss_enum_descriptor_t mep_oam_count_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_oam_count,
                         "MepOamCount",
                         mep_oam_count_txt,
                         "Loss Measurement OAM frame counting selection.");

extern vtss_enum_descriptor_t mep_lm_avail_state_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_lm_avail_state,
                         "MepLmAvailState",
                         mep_lm_avail_state_txt,
                         "Loss Measurement Availability State.");

extern vtss_enum_descriptor_t mep_oper_state_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_mep_oper_state_t,
                         "MepOperState",
                         mep_oper_state_txt,
                         "Instance Operation State.");

VTSS_SNMP_TAG_SERIALIZE(MepInstance, uint32_t, a, s) {
    a.add_leaf(vtss::AsInt(s.inner, 1), vtss::tag::Name("Id"),
               vtss::expose::snmp::RangeSpec<uint32_t>(1, 2147483647),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(1),
               vtss::tag::Description("The MEP instance ID"));
}

VTSS_SNMP_TAG_SERIALIZE(PeerId, uint32_t, a, s) {
    a.add_leaf(vtss::AsInt(s.inner), vtss::tag::Name("PeerId"),
               vtss::expose::snmp::RangeSpec<uint32_t>(0, 2147483647),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(2),
               vtss::tag::Description("The Peer MEP ID"));
}

VTSS_SNMP_TAG_SERIALIZE(FlowId, vtss_ifindex_t, a, s) {
    a.add_leaf(vtss::AsInterfaceIndex(s.inner), vtss::tag::Name("FlowId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(2),
               vtss::tag::Description("The Client Flow id - ifindex"));
}

VTSS_SNMP_TAG_SERIALIZE(ReplyId, uint32_t, a, s) {
    a.add_leaf(vtss::AsInt(s.inner), vtss::tag::Name("ReplyId"),
               vtss::expose::snmp::RangeSpec<uint32_t>(0, 2147483647),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(2),
               vtss::tag::Description("The LB or LT Reply id"));
}

VTSS_SNMP_TAG_SERIALIZE(SeqNo, uint32_t, a, s) {
    a.add_leaf(s.inner, vtss::tag::Name("SeqNo"),
               vtss::expose::snmp::RangeSpec<uint32_t>(0, 4294967295),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(2),
               vtss::tag::Description("The RT sequence number"));
}

VTSS_SNMP_TAG_SERIALIZE(BinIndexId, uint32_t, a, s) {
    a.add_leaf(vtss::AsInt(s.inner), vtss::tag::Name("BinIndexId"),
               vtss::expose::snmp::RangeSpec<uint32_t>(0, VTSS_APPL_MEP_DM_BINNING_MAX - 1),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(2),
               vtss::tag::Description("FD or IFDV Bin index ID"));
}

VTSS_SNMP_TAG_SERIALIZE(KeyInstance, uint32_t, a, s) {
    a.add_leaf(vtss::AsInt(s.inner), vtss::tag::Name("Id"),
        vtss::expose::snmp::RangeSpec<uint32_t>(0, 255),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(1),
        vtss::tag::Description("The Bfd Auth Key instance ID"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_capabilities_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_capabilities_t"));
    int ix = 1;

    m.add_leaf(s.instance_max,
               vtss::tag::Name("instanceMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Max number of MEP instance"));
    m.add_leaf(s.peer_max,
               vtss::tag::Name("peerMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Max number of peer MEP"));
    m.add_leaf(s.transaction_max,
               vtss::tag::Name("transactionMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Max number of Link Trace transaction"));
    m.add_leaf(s.reply_max,
               vtss::tag::Name("replyMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Max number of reply in a transaction"));
    m.add_leaf(s.client_flows_max,
               vtss::tag::Name("clientFlowsMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Max number of client flows"));
    m.add_leaf(s.mac_length,
               vtss::tag::Name("macMength"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("MAC length"));
    m.add_leaf(s.meg_code_length,
               vtss::tag::Name("megCodeLength"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("MAX length of Both Maintenance Domain Name and MEG-ID include a NULL termination"));
    m.add_leaf(s.dm_interval_min,
               vtss::tag::Name("dmIntervalMin"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("MIN DM transmission interval "));
    m.add_leaf(s.dm_interval_max,
               vtss::tag::Name("dmIntervalMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("MAX DM transmission interval"));
    m.add_leaf(s.dm_lastn_min,
               vtss::tag::Name("dmLastnMin"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("MIN number of DM LastN for average calculation"));
    m.add_leaf(s.dm_lastn_max,
               vtss::tag::Name("dmLastnMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("MAX number of DM LastN for average calculation"));
    m.add_leaf(s.lbm_size_max,
               vtss::tag::Name("lbmSizeMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The maximum LBM frame size"));
    m.add_leaf(s.lbm_size_min,
               vtss::tag::Name("lbmSizeMin"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The minimum LBM frame size"));
    m.add_leaf(s.tst_size_max,
               vtss::tag::Name("tstSizeMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The maximum TST frame size"));
    m.add_leaf(s.tst_size_min,
               vtss::tag::Name("tstSizeMin"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The minimum TST frame size"));
    m.add_leaf(s.tst_rate_max,
               vtss::tag::Name("tstRateMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The maximum TST bit rate"));
    m.add_leaf(s.client_prio_highest,
               vtss::tag::Name("clientPrioHighest"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The highest possible client priority is requested"));
    m.add_leaf(vtss::AsBool(s.has_itu_g8113_2),
               vtss::tag::Name("ituG8113dot2"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Indicates if ITU G8113.2 is supported on this system"));
    m.add_leaf(vtss::AsBool(s.has_voe_support),
               vtss::tag::Name("voeSupport"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Indicates if HW based OAM engine is supported on this system"));
    m.add_leaf(s.lb_to_send_infinite,
               vtss::tag::Name("lbToSendInfinite"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Send LBM infinite - test behaviour"));
    m.add_leaf(s.bfd_auth_key_max,
               vtss::tag::Name("bfdAuthKeyMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Maximum number of BFD authentication keys"));
    m.add_leaf(vtss::AsBool(s.has_evc_pag),
               vtss::tag::Name("hasEvcPag"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Indicates if EVC generated policy PAG value as IS2 key is supported on this system"));
    m.add_leaf(vtss::AsBool(s.has_evc_qos),
               vtss::tag::Name("hasEvcQos"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Indicates if EVC QoS value used for getting queue frame LM counters is supported on this system"));
    m.add_leaf(vtss::AsBool(s.has_flow_count),
               vtss::tag::Name("hasFlowCount"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Indicates if Traffic (service frames) counted per flow is supported on this system"));
    m.add_leaf(vtss::AsBool(s.has_oam_count),
               vtss::tag::Name("hasOamCount"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Indicates if Loss Measurement count of OAM frames in different ways is supported on this system"));
    m.add_leaf(vtss::AsBool(s.has_oper_state),
               vtss::tag::Name("hasOperState"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Indicates if instance operational state is supported on this system"));
    m.add_leaf(vtss::AsBool(s.has_oam_sw_support),
               vtss::tag::Name("hasOamSwSupport"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Indicates if SW based OAM engine is supported on this system"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_conf_t"));
    int ix = 1;

    m.add_leaf(s.mode,
               vtss::tag::Name("Mode"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The instance mode. Cannot be changed after creation."));
    m.add_leaf(s.direction,
               vtss::tag::Name("Direction"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The instance direction. Cannot be changed after creation."));
    m.add_leaf(vtss::AsInterfaceIndex(s.flow),
               vtss::tag::Name("Flow"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The flow instance (VLAN - EVC). Cannot be changed after creation."));
    m.add_leaf(vtss::AsInterfaceIndex(s.port),
               vtss::tag::Name("Port"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The instance residence port. Cannot be changed after creation."));
    m.add_leaf(s.level,
               vtss::tag::Name("Level"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The instance level"));
    m.add_leaf(s.vid,
               vtss::tag::Name("Vid"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("VID used for tagged Port domain OAM. Create a VLAN MEP instead - if possible"));
    m.add_leaf(vtss::AsBool(s.voe),
               vtss::expose::snmp::PreGetCondition([&](){
                   return (mep_caps.mesa_oam);
               }),
               vtss::tag::Name("Voe"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Make the instance VOE based - if possible"));
    m.add_leaf(s.mac,
               vtss::tag::Name("Mac"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The instance unicast MAC. If 'all zero' the residence port MAC is used"));
    m.add_leaf(s.format,
               vtss::tag::Name("Format"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The MEG-ID format"));
    m.add_leaf(vtss::AsDisplayString(s.name, sizeof(s.name)), vtss::tag::Name("Name"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("IEEE Maintenance Domain Name (string)"));
    m.add_leaf(vtss::AsDisplayString(s.meg, sizeof(s.meg)), vtss::tag::Name("Meg"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Unique MEG ID (string)"));
    m.add_leaf(s.mep,
               vtss::tag::Name("Mep"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("MEP id of this instance"));
    m.add_leaf(s.evc_pag,
               vtss::expose::snmp::PreGetCondition([&](){
                    return(FALSE);
               }),
               vtss::tag::Name("EvcPag"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("EVC generated policy PAG value. This is used as IS2 key"));
    m.add_leaf(s.evc_qos,
               vtss::expose::snmp::PreGetCondition([&](){
                   return (mep_caps.appl_mep_evc_qos);
               }),
               vtss::tag::Name("EvcQos"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("EVC QoS value. Used for getting queue frame LM counters."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_peer_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_peer_conf_t"));
    int ix = 1;

    m.add_leaf(s.mac,
               vtss::tag::Name("Mac"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The peer unicast MAC"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_pm_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_pm_conf_t"));
    int ix = 1;

    m.add_leaf(vtss::AsBool(s.enable),
               vtss::tag::Name("Enable"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The instance can enable contribution of LM and DM results to the Performance Monitoring "));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lst_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lst_conf_t"));
    int ix = 1;

    m.add_leaf(vtss::AsBool(s.enable),
               vtss::tag::Name("Enable"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The instance can enable Link State Tracking. When LST is enabled, Local SF or received 'isDown' in CCM Interface Status TLV, will bring down the residence port. Only valid in Up-MEP."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_syslog_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_syslog_conf_t"));
    int ix = 1;

    m.add_leaf(vtss::AsBool(s.enable),
               vtss::tag::Name("Enable"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The instance can enable generation of syslog entries "));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_cc_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_cc_conf_t"));
    int ix = 1;

    ix++;
    m.add_leaf(s.prio,
               vtss::tag::Name("Prio"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Priority of the transmitted CCM. Range 0 to 7"));
    m.add_leaf(s.rate,
               vtss::tag::Name("Rate"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("CCM transmission and expected reception rate"));
    m.add_leaf(vtss::AsBool(s.tlv_enable),
               vtss::tag::Name("Tlv"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Enable/disable insertion of TLV in CCM PDU. OS, PS and IS TLV are inserted"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lm_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lm_conf_t"));
    int ix = 1;

    m.add_leaf(vtss::AsBool(s.enable),
               vtss::tag::Name("Enable"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Loss Measurement is enabled"));
    m.add_leaf(s.ended,
               vtss::tag::Name("Ended"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Dual/single ended selection. CCM or LMM/LMR based. Cannot be changed after creation."));
    ix++;
    m.add_leaf(s.prio,
               vtss::tag::Name("Prio"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Priority of the transmitted LM PDU. Range 0 to 7"));
    m.add_leaf(s.rate,
               vtss::tag::Name("Rate"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("LM PDU transmission rate"));
    m.add_leaf(s.cast,
               vtss::tag::Name("Cast"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Uni/Multicast selection - on CCM based LM only multicast is possible"));
    m.add_leaf(s.flr_interval,
               vtss::tag::Name("FlrInterval"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Frame loss ratio interval in number of measurements."));
    m.add_leaf(vtss::AsBool(s.flow_count),
               vtss::expose::snmp::PreGetCondition([&](){
                   return(mep_caps.mesa_oam);
               }),
               vtss::tag::Name("FlowCount"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Traffic (service frames) are counted per flow - all priority in one"));
    m.add_leaf(s.oam_count,
               vtss::expose::snmp::PreGetCondition([&](){
                   return(mep_caps.mesa_oam);
               }),
               vtss::tag::Name("OamCount"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Loss Measurement can count OAM frames in different ways."));
    m.add_leaf(s.loss_th,
               vtss::tag::Name("LossTh"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Frame loss threshold. The loss threshold count NearEndLosThCnt / FarEndLosThCnt is incremented if a loss measurement is above this threshold."));
    m.add_leaf(s.los_int_cnt_holddown,
               vtss::tag::Name("LossCounterIntHolddownTimer"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Holddown timer in seconds for JSON notification updates for NearEndLosCounterInt and FarEndLosCounterInt."));
    m.add_leaf(s.los_th_cnt_holddown,
               vtss::tag::Name("LossThCntHolddownTimer"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Holddown timer in seconds for JSON notification updates for NearEndLosThCnt and FarEndLosThCnt."));
    m.add_leaf(vtss::AsBool(s.enable_rx),
               vtss::tag::Name("RxEnable"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("This enables calculation of far-to-near loss when CCM/LMM/SLM/1SL is received. Can only be set TRUE when 'enable' is FALSE"));
    m.add_leaf(vtss::AsBool(s.synthetic),
               vtss::tag::Name("Synthetic"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("TRUE => LM based on synthetic frames (SLM/1SL). FALSE => LM based on service frames (LMM/CCM)."));
    m.add_leaf(s.mep,
               vtss::tag::Name("Mep"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Peer Mep to receive LM - only used if unicast. Value > VTSS_APPL_MEP_ID_MAX can be used when only one peer-mep (default)."));
    m.add_leaf(s.size,
               vtss::tag::Name("FrameSize"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Size of SLM/1SL frame to send."));
    m.add_leaf(s.meas_interval,
               vtss::tag::Name("MeasInterval"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Synthetic LM measurement interval."));
    m.add_leaf(s.slm_test_id,
               vtss::tag::Name("SlmTestId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Test ID value to use in SLM PDUs."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lm_avail_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lm_avail_conf_t"));
    int ix = 1;

    m.add_leaf(vtss::AsBool(s.enable),
               vtss::tag::Name("Enable"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Loss Measurement Availability is enabled"));
    m.add_leaf(s.flr_th,
               vtss::tag::Name("LosRatioThr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Availability frame loss ratio threshold in per mille."));
    m.add_leaf(s.interval,
               vtss::tag::Name("Interval"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Availability interval - number of measurements with same availability in order to change Availability state. The valid range is 1 to 1000."));
    m.add_leaf(vtss::AsBool(s.main),
               vtss::tag::Name("Maintenance"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Loss Measurement Availability Maintenance enable/disable"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lm_hli_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lm_hli_conf_t"));
    int ix = 1;

    m.add_leaf(vtss::AsBool(s.enable),
               vtss::tag::Name("Enable"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Loss Measurement High Loss Interval is enabled"));
    m.add_leaf(s.flr_th,
               vtss::tag::Name("HiLosRatioThr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("High Loss Interval frame loss ratio threshold in per mille."));
    m.add_leaf(s.con_int,
               vtss::tag::Name("ConInterval"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("High Loss Interval consecutive interval (number of measurements)."));
    m.add_leaf(s.cnt_holddown,
               vtss::tag::Name("CntHolddownTimer"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Holddown timer in seconds for JSON notification updates for High Loss Interval NearEndCnt and FarEndCnt."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lm_sdeg_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lm_sdeg_conf_t"));
    int ix = 1;

    m.add_leaf(vtss::AsBool(s.enable),
               vtss::tag::Name("Enable"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Loss Measurement Signal Degrade is enabled"));
    m.add_leaf(s.tx_min,
               vtss::tag::Name("TxMin"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Minimum number of frames that must be transmitted in a measurement before frame loss ratio is tested against LosRatioThr."));
    m.add_leaf(s.flr_th,
               vtss::tag::Name("LosRatioThr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Signal Degraded frame loss ratio threshold in per mille."));
    m.add_leaf(s.bad_th,
               vtss::tag::Name("BadThr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number of consecutive bad interval measurements required to set degrade state."));
    m.add_leaf(s.good_th,
               vtss::tag::Name("GoodThr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number of consecutive good interval measurements required to clear degrade state."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_dm_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_dm_conf_t"));
    int ix = 1;

    m.add_leaf(vtss::AsBool(s.enable),
               vtss::tag::Name("Enable"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Delay Measurement is enabled"));
    m.add_leaf(s.ended,
               vtss::tag::Name("Ended"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Dual/single ended selection. CCM or LMM/LMR based. Cannot be changed after creation."));
    ix++;
    m.add_leaf(s.prio,
               vtss::tag::Name("Prio"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Priority of the transmitted DM PDU. Range 0 to 7"));
    m.add_leaf(s.cast,
               vtss::tag::Name("Cast"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Uni/Multicast selection"));
    m.add_leaf(s.mep,
               vtss::tag::Name("Mep"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Peer MEP to receive DMM/1DM - only used if unicast"));
    m.add_leaf(s.calcway,
               vtss::tag::Name("CalcWay"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Round trip or flow calculation way selection."));
    m.add_leaf(s.interval,
               vtss::tag::Name("Interval"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Interval between 1DM/DMM in 10 ms. Range Capability dm_interval_min to dm_interval_max."));
    m.add_leaf(s.lastn,
               vtss::tag::Name("LastN"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The number of last N measurements used to calculate average delay and variation. Range Capability dm_lastn_min to dm_lastn_max."));
    m.add_leaf(s.tunit,
               vtss::tag::Name("TimeUnit"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Calculation time unit."));
    m.add_leaf(s.overflow_act,
               vtss::tag::Name("OverflowAct"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Action when internal total delay counter overflow."));
    m.add_leaf(vtss::AsBool(s.synchronized),
               vtss::tag::Name("Synchronized"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Near-end and far-end is real-time synchronized. One-way DM calculation on receiving DMR."));
    m.add_leaf(s.num_of_bin_fd,
               vtss::tag::Name("NumOfBinFD"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Configurable number of FD Measurement Bins / per Measurement Interval. default:3, min:2, max:10"));
    m.add_leaf(s.num_of_bin_ifdv,
               vtss::tag::Name("NumOfBinIFDV"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Configurable number of IFDV Measurement Bins / per Measurement Interval. default:2, min:2, max:10"));
    m.add_leaf(s.m_threshold,
               vtss::tag::Name("MThreshold"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Configurable the measurement threshold for each Measurement Bin. default: 5000"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lb_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lb_conf_t"));
    int ix = 1;

    m.add_leaf(vtss::AsBool(s.dei),
               vtss::tag::Name("Dei"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Drop Eligibility Indicator of transmitted LBM PDU"));
    m.add_leaf(s.prio,
               vtss::tag::Name("Prio"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Priority of the transmitted LBM PDU. Range 0 to 7"));
    m.add_leaf(s.cast,
               vtss::tag::Name("Cast"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Uni/Multicast selection"));
    m.add_leaf(s.mep,
               vtss::tag::Name("Mep"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Peer MEP to receive LBM - only used if unicast and 'mac' is 'all zero'."));
    m.add_leaf(s.mac,
               vtss::tag::Name("Mac"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Unicast MAC address to receive LBM - has to be used to send LBM to MIP."));
    m.add_leaf(s.to_send,
               vtss::tag::Name("ToSend"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number of LBM to send. Capability lb_to_send_infinite => test behaviour. Requires VOE."));
    m.add_leaf(s.size,
               vtss::tag::Name("Size"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Size of LBM frame to send - Range Capability lbm_size_min to lbm_size_max"));
    m.add_leaf(s.interval,
               vtss::tag::Name("Interval"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("\
Frame interval.\n\
If 'ToSend' != Capability lb_to_send_infinite then the 'Interval' is in 10 ms. Max 1 s.\n\
If 'ToSend' == Capability lb_to_send_infinite then the 'Interval' is in 1 us. Max 10 ms."));
    m.add_leaf(s.ttl,
               vtss::tag::Name("TimeToLive"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Time To Live."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_tst_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_tst_conf_t"));
    int ix = 1;

    m.add_leaf(vtss::AsBool(s.enable),
               vtss::tag::Name("TxEnable"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Enable/disable transmission of TST PDU."));
    m.add_leaf(vtss::AsBool(s.enable_rx),
               vtss::tag::Name("RxEnable"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Enable/disable reception and analyse of TST PDU."));
    m.add_leaf(vtss::AsBool(s.dei),
               vtss::tag::Name("Dei"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Drop Eligibility Indicator of transmitted TST PDU"));
    m.add_leaf(s.prio,
               vtss::tag::Name("Prio"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Priority of the transmitted TST PDU. Range 0 to 7"));
    m.add_leaf(s.mep,
               vtss::tag::Name("Mep"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Peer MEP to receive TST"));
    m.add_leaf(s.rate,
               vtss::tag::Name("Rate"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The transmission rate in Kbps - Range 1 to Capability tst_rate_max."));
    m.add_leaf(s.size,
               vtss::tag::Name("Size"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Size of TST frame to send - Range Capability tst_size_min to tst_size_max."));
    m.add_leaf(s.pattern,
               vtss::tag::Name("Pattern"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Pattern in TST frame Data TLV."));
    m.add_leaf(vtss::AsBool(s.sequence),
               vtss::tag::Name("Sequence"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Sequence number will be inserted - not checked in receiver on Caracal and Jaguar."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lt_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lt_conf_t"));
    int ix = 1;

    ix++;
    m.add_leaf(s.prio,
               vtss::tag::Name("Prio"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Priority of the transmitted LTM PDU. Range 0 to 7"));
    m.add_leaf(s.mep,
               vtss::tag::Name("Mep"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Target Peer Mep to receive LTM - only used if 'mac' is 'all zero'"));
    m.add_leaf(s.mac,
               vtss::tag::Name("Mac"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Unicast MAC address to receive LTM - has to be used to send LTM to MIP."));
    m.add_leaf(s.ttl,
               vtss::tag::Name("TimeToLive"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Time To Live."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_rt_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_rt_conf_t"));
    int ix = 1;

    m.add_leaf(s.tc,
               vtss::tag::Name("TrafficClass"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Traffic class value used in the transmitted RT request PDU. Range 0 to 7"));
    m.add_leaf(vtss::AsBool(s.src_id_tlv),
               vtss::tag::Name("SrcIdTlv"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Include Source ID TLV in the RT request"));
    m.add_leaf(vtss::AsBool(s.dst_id_tlv),
               vtss::tag::Name("DstIdTlv"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Include Destination ID TLV in the RT request"));
    m.add_leaf(s.pad_tlv_type,
               vtss::tag::Name("PadTlvType"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Pad TLV type"));
    m.add_leaf(s.pad_tlv_len,
               vtss::tag::Name("PadTlvLen"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Pad TLV data length. Range 1 to 1024."));
    m.add_leaf(s.flags,
               vtss::tag::Name("Flags"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Flags for RT request. Bit 0: V flag (Validate FEC stack), bit 1: T flag (Respond Only If TTL Expired), bit 2: R flag (Validate Reverse Path)"));
    m.add_leaf(s.max_ttl,
               vtss::tag::Name("MaxTimeToLive"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Max. Time To Live for RT request."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_aps_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_aps_conf_t"));
    int ix = 1;

    ix++;
    m.add_leaf(s.prio,
               vtss::tag::Name("Prio"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Priority of the transmitted APS PDU. Range 0 to 7"));
    m.add_leaf(s.type,
               vtss::tag::Name("ApsType"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Type of APS"));
    m.add_leaf(s.cast,
               vtss::tag::Name("Cast"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Uni/multicast selection"));
    m.add_leaf(s.raps_octet,
               vtss::tag::Name("RapsOctet"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Last octet in the R-APS multicast DA"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_ais_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_ais_conf_t"));
    int ix = 1;

    m.add_leaf(vtss::AsBool(s.protection),
               vtss::tag::Name("Protection"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The first three AIS frames are transmitted as fast as possible."));
    m.add_leaf(s.rate,
               vtss::tag::Name("rate"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("AIS transmission rate"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lck_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lck_conf_t"));
    int ix = 1;

    m.add_leaf(s.rate,
               vtss::tag::Name("Rate"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("LCK transmission rate"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_client_flow_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_client_flow_conf_t"));
    int ix = 1;

    m.add_leaf(s.ais_prio,
               vtss::tag::Name("AisPrio"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("AIS Priority (EVC COS-ID) 0-7. Capability client_prio_highest indicate highest possible is requested"));
    m.add_leaf(s.lck_prio,
               vtss::tag::Name("LckPrio"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("LCK Priority (EVC COS-ID) 0-7. Capability client_prio_highest indicate highest possible is requested"));
    m.add_leaf(s.level,
               vtss::tag::Name("Level"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Client flow level"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_tlv_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_tlv_conf_t"));
    int ix = 1;

    m.add_leaf(s.os_tlv.oui[0],
               vtss::tag::Name("OsTlvOuiFirst"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Transmitted OS TLV - The OUI content first nibble"));
    m.add_leaf(s.os_tlv.oui[1],
               vtss::tag::Name("OsTlvOuiSecond"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Transmitted OS TLV - The OUI content second nibble"));
    m.add_leaf(s.os_tlv.oui[2],
               vtss::tag::Name("OsTlvOuiThird"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Transmitted OS TLV - The OUI content third nibble"));
    m.add_leaf(s.os_tlv.subtype,
               vtss::tag::Name("OsTlvSubType"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Transmitted OS TLV - The Sub-Type content"));
    m.add_leaf(s.os_tlv.value,
               vtss::tag::Name("OsTlvValue"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Transmitted OS TLV - The Value content"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_g8113_2_bfd_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_g8113_2_bfd_conf_t"));
    int ix = 1;
    m.add_leaf(vtss::AsBool(s.enable),
               vtss::tag::Name("Enable"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("CC/CV transmission and LOC detection is enabled if true."));
    m.add_leaf(vtss::AsBool(s.is_independent),
               vtss::tag::Name("IsIndependent"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Independent mode if true, coordinated if false."));
    m.add_leaf(vtss::AsBool(s.cc_only),
               vtss::tag::Name("CcOnly"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Only transmit CC frames (no CV frames) if true."));
    m.add_leaf(vtss::AsBool(s.tx_auth_enabled),
               vtss::tag::Name("TxAuthEnabled"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Authentication enabled for transmission of CC/CV frames to peer if true."));
    m.add_leaf(vtss::AsBool(s.rx_auth_enabled),
               vtss::tag::Name("RxAuthEnabled"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Authentication enabled for reception of CC/CV frames from peer if true."));
    m.add_leaf(s.tx_auth_key_id,
               vtss::tag::Name("TxAuthKeyId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Authentication used for transmission of CC/CV frames to peer."));
    m.add_leaf(s.cc_period,
               vtss::tag::Name("CcPeriod"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Desired CC transmission period in us, 1 us - 60 sec."));
    m.add_leaf(s.rx_flow,
               vtss::tag::Name("RxFlow"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Rx flow number (MPLS-TP link, MPLS-TP tunnel, MPLS-TP LSP or MPLS-TP Pseudo-Wire) for independent mode."));

}

template<typename T>
void serialize(T &a, vtss_appl_mep_g8113_2_bfd_auth_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_g8113_2_bfd_auth_conf_t"));
    int ix = 1;

    m.add_leaf(s.key_type,
               vtss::tag::Name("KeyType"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Key type."));
    m.add_leaf(s.key_len,
               vtss::tag::Name("KeyLen"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Key length in bytes. 1-16 for Simple Password, 16 for MD5 and 20 for SHA1."));
    m.add_leaf(vtss::AsOctetString(s.key,20),
               vtss::tag::Name("Key"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Key value."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_g8113_2_bfd_state_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_g8113_2_bfd_state_t"));
    int ix = 1;
    m.add_leaf(s.cc_tx_cnt,
               vtss::tag::Name("CcTxCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("BFD CC transmitted."));
    m.add_leaf(s.valid_cc_rx_cnt,
               vtss::tag::Name("ValidCcRxCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Valid BFD CC received."));
    m.add_leaf(s.invalid_cc_rx_cnt,
               vtss::tag::Name("InvalidCcRxCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Invalid BFD CC received."));
    m.add_leaf(s.cv_tx_cnt,
               vtss::tag::Name("CvTxCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("BFD CV transmitted."));
    m.add_leaf(s.valid_cv_rx_cnt,
               vtss::tag::Name("ValidCvRxCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Valid BFD CV received."));
    m.add_leaf(s.invalid_cv_rx_cnt,
               vtss::tag::Name("InvalidCvRxCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Invalid BFD CV received."));
    m.add_leaf(s.my_discr[0],
               vtss::tag::Name("MyDiscr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Selected random 'My Discriminator' value, non-zero."));
    m.add_leaf(s.session_state[0],
               vtss::tag::Name("SessionState0"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Local BFD session state (for coordinated mode only SessionState0 is valid, for independent mode SessionState0 is sink, SessionState1 is source."));
    m.add_leaf(s.session_state[1],
               vtss::tag::Name("SessionState1"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Local BFD session state. Only valid in independent mode where SessionState0 is sink, SessionState1 is source."));
    m.add_leaf(s.local_diag,
               vtss::tag::Name("LocalDiag"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Local diagnostic code."));
    m.add_leaf(s.remote_session_state,
               vtss::tag::Name("RemoteSessionState"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("BFD session state received from remote end."));
    m.add_leaf(s.remote_diag,
               vtss::tag::Name("RemoteDiag"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Diagnostic code received from remote end."));
    m.add_leaf(s.remote_discr[0],
               vtss::tag::Name("RemoteDiscr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Discriminator value received from remote end."));
    m.add_leaf(s.remote_min_rx,
               vtss::tag::Name("RemoteMinRx"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Required Min RX interval value received from remote end."));
    m.add_leaf(s.remote_flags,
               vtss::tag::Name("RemoteFlags"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Flags received from remote end."));
    m.add_leaf(s.unexp_discr,
               vtss::tag::Name("UnexpDiscr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Last received unexpected discriminator value (should be my configured)."));
    m.add_leaf(vtss::AsOctetString(s.exp_src_mep_id_tlv,28),
               vtss::tag::Name("ExpSrcMepIdTlv"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Expected source MEP ID TLV."));
    m.add_leaf(vtss::AsOctetString(s.unexp_src_mep_id_tlv,28),
               vtss::tag::Name("UnexpSrcMepIdTlv"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Last received unexpected source MEP ID TLV."));
    m.add_leaf(s.my_discr[1],
               vtss::tag::Name("MyDiscr1"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Selected random 'My Discriminator' value (sink), non-zero. Only valid in independent mode."));
    m.add_leaf(s.remote_discr[1],
               vtss::tag::Name("RemoteDiscr1"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Discriminator value received from remote end by sink. Only valid in independent mode."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_state_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_state_t"));
    int ix = 1;

    m.add_leaf(vtss::AsBool(s.cLevel),
               vtss::tag::Name("Clevel"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Incorrect CCM level received"));
    m.add_leaf(vtss::AsBool(s.cMeg),
               vtss::tag::Name("Cmeg"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Incorrect CCM MEG id received"));
    m.add_leaf(vtss::AsBool(s.cMep),
               vtss::tag::Name("Cmep"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Incorrect CCM MEP id received"));
    m.add_leaf(vtss::AsBool(s.cSsf),
               vtss::tag::Name("Cssf"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("SSF state"));
    m.add_leaf(vtss::AsBool(s.cAis),
               vtss::tag::Name("Cais"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("AIS state. AIS is received."));
    m.add_leaf(vtss::AsBool(s.cLck),
               vtss::tag::Name("Clck"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Locked State. LCK is received."));
    m.add_leaf(vtss::AsBool(s.aTsf),
               vtss::tag::Name("Atsf"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Trail Signal fail consequent action is calculated"));
    m.add_leaf(vtss::AsBool(s.aTsd),
               vtss::tag::Name("Atsd"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Trail Signal degrade consequent action is calculated"));
    m.add_leaf(vtss::AsBool(s.aBlk),
               vtss::tag::Name("Ablk"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Block consequent action is calculated"));
    m.add_leaf(vtss::AsBool(s.cLoop),
               vtss::tag::Name("Cloop"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Loop is detected. CCM is received with own MEP ID and SMAC."));
    m.add_leaf(vtss::AsBool(s.cConfig),
               vtss::tag::Name("Cconfig"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Configuration error detected. CCM is received with own MEP ID."));
    m.add_leaf(vtss::AsBool(s.cDeg),
               vtss::tag::Name("Cdeg"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Signal Degraded."));
    m.add_leaf(s.oper_state,
               vtss::expose::snmp::PreGetCondition([&](){
                   return (mep_caps.appl_mep_oper_state);
               }),
               vtss::tag::Name("OperState"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("\
Operational State.\n\
'operStateUp' The instance is UP meaning it is physically configured and operational.\n\
'operStateDown' The instance is DOWN meaning it is NOT physically configured and operational.\n\
'operStateDownInvalidConf' The instance is DOWN due to invalid configuration.\n\
'operStateDownfailingOamHw' The instance is DOWN due to failing OAM supporting HW resources.\n\
'operStateDownfailingMce' The instance is DOWN due to failing MCE resources."));
}


template<typename T>
void serialize(T &a, vtss_appl_mep_peer_state_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_peer_state_t"));
    int ix = 1;

    m.add_leaf(vtss::AsBool(s.cLoc),
               vtss::tag::Name("Cloc"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("CCM LOC state from peer MEP"));
    m.add_leaf(vtss::AsBool(s.cRdi),
               vtss::tag::Name("Crdi"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("CCM RDI state from peer MEP"));
    m.add_leaf(vtss::AsBool(s.cPeriod),
               vtss::tag::Name("Cperiod"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("CCM Period state from peer MEP"));
    m.add_leaf(vtss::AsBool(s.cPrio),
               vtss::tag::Name("Cprio"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("CCM Priority state from peer MEP"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lm_state_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lm_state_t"));
    int ix = 1;

    m.add_leaf(s.tx_counter,
               vtss::tag::Name("TxCounter"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Transmitted PDU (LMM - CCM) containing counters."));
    m.add_leaf(s.rx_counter,
               vtss::tag::Name("RxCounter"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Received PDU (LMM - CCM) containing counters."));
    m.add_leaf(s.near_los_tot_cnt,
               vtss::tag::Name("NearEndLosCounterTot"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Near end total frame loss counter."));
    m.add_leaf(s.far_los_tot_cnt,
               vtss::tag::Name("FarEndLosCounterTot"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Far end total frame loss counter."));
    m.add_leaf(s.near_flr_ave_int,
               vtss::tag::Name("NearEndLosRatioInt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Near end frame loss ratio for interval in per mille."));
    m.add_leaf(s.far_flr_ave_int,
               vtss::tag::Name("FarEndLosRatioInt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Far end frame loss ratio for interval in per mille."));
    m.add_leaf(s.near_los_int_cnt,
               vtss::tag::Name("NearEndLosCounterInt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number of measurements that has contributed to near end loss (since last clear)."));
    m.add_leaf(s.far_los_int_cnt,
               vtss::tag::Name("FarEndLosCounterInt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number of measurements that has contributed to far end loss (since last clear)."));
    m.add_leaf(s.near_los_th_cnt,
               vtss::tag::Name("NearEndLosThCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Near end frame loss threshold count."));
    m.add_leaf(s.far_los_th_cnt,
               vtss::tag::Name("FarEndLosThCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Far end frame loss threshold count."));
    m.add_leaf(s.near_flr_ave_total,
               vtss::tag::Name("NearEndLosRatioTot"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Near end total frame loss ratio in per mille (average since last clear)."));
    m.add_leaf(s.far_flr_ave_total,
               vtss::tag::Name("FarEndLosRatioTot"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Far end total frame loss ratio in per mille (average since last clear)."));
    m.add_leaf(s.near_flr_min,
               vtss::tag::Name("NearEndLosRatioMin"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Near end minimum frame loss ratio in per mille (since last clear)."));
    m.add_leaf(s.far_flr_min,
               vtss::tag::Name("FarEndLosRatioMin"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Far end minimum frame loss ratio in per mille (since last clear)."));
    m.add_leaf(s.near_flr_max,
               vtss::tag::Name("NearEndLosRatioMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Near end maximum frame loss ratio in per mille (since last clear)."));
    m.add_leaf(s.far_flr_max,
               vtss::tag::Name("FarEndLosRatioMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Far end maximum frame loss ratio in per mille (since last clear)."));
    m.add_leaf(s.flr_interval_elapsed,
               vtss::tag::Name("ElapsedIntervals"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The number of expired frame loss ratio measurement intervals."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lm_notif_state_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lm_notif_state_t"));
    int ix = 1;

    m.add_leaf(s.near_state,
               vtss::tag::Name("NearEndAvailState"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Near end Availability State."));
    m.add_leaf(s.far_state,
               vtss::tag::Name("FarEndAvailState"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Far end Availability State."));
    m.add_leaf(s.near_los_int_cnt,
               vtss::tag::Name("NearEndLosCounterInt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number of measurements that has contributed to near end loss (since last clear)."));
    m.add_leaf(s.far_los_int_cnt,
               vtss::tag::Name("FarEndLosCounterInt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number of measurements that has contributed to far end loss (since last clear)."));
    m.add_leaf(s.near_los_th_cnt,
               vtss::tag::Name("NearEndLosThCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Near end frame loss threshold count."));
    m.add_leaf(s.far_los_th_cnt,
               vtss::tag::Name("FarEndLosThCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Far end frame loss threshold count."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lm_avail_state_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lm_avail_state_t"));
    int ix = 1;

    m.add_leaf(s.near_state,
               vtss::tag::Name("NearEndAvailState"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Near end Availability State."));
    m.add_leaf(s.far_state,
               vtss::tag::Name("FarEndAvailState"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Far end Availability State."));
    m.add_leaf(s.near_cnt,
               vtss::tag::Name("NearEndAvailCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Near end Availability Count."));
    m.add_leaf(s.far_cnt,
               vtss::tag::Name("FarEndAvailCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Far end Availability Count."));
    m.add_leaf(s.near_un_cnt,
               vtss::tag::Name("NearEndUnAvailCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Near end Unavailability Count."));
    m.add_leaf(s.far_un_cnt,
               vtss::tag::Name("FarEndUnAvailCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Far end Unavailability Count."));
    m.add_leaf(s.near_avail_cnt,
               vtss::tag::Name("NearEndWindowCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Near end Window Count."));
    m.add_leaf(s.far_avail_cnt,
               vtss::tag::Name("FarEndWindowCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Far end Window Count."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lm_hli_state_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lm_hli_state_t"));
    int ix = 1;

    m.add_leaf(s.near_cnt,
               vtss::tag::Name("NearEndCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Near end High Loss Interval count (number of measurements where Availability State is available and FLR is above HLI FLR threshold."));
    m.add_leaf(s.far_cnt,
               vtss::tag::Name("FarEndCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Far end High Loss Interval count (number of measurements where Availability State is available and FLR is above HLI FLR threshold."));
    m.add_leaf(s.near_con_cnt,
               vtss::tag::Name("NearEndConCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Near end High Loss Interval consecutive count."));
    m.add_leaf(s.far_con_cnt,
               vtss::tag::Name("FarEndConCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Far end High Loss Interval consecutive count."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_dm_state_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_dm_state_t"));
    int ix = 1;

    m.add_leaf(s.tx_cnt,
               vtss::tag::Name("TxCounter"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Transmitted DMM/1DM frames."));
    m.add_leaf(s.rx_cnt,
               vtss::tag::Name("RxCounter"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Received DMM/1DM frames."));
    m.add_leaf(s.rx_tout_cnt,
               vtss::tag::Name("RxTimeOutCounter"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Received DMR time-out. After transmission of DMM, the DMR is expected to be received within 1 sec."));
    m.add_leaf(s.rx_err_cnt,
               vtss::tag::Name("RxErrorCounter"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Received error counter. It is considered an error if a delay is negative or above 1 sec."));
    m.add_leaf(s.ovrflw_cnt,
               vtss::tag::Name("InternalOverflowCounter"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The internal total delay counter overflow counter."));
    m.add_leaf(s.avg_delay,
               vtss::tag::Name("AverageDelay"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The total average delay."));
    m.add_leaf(s.avg_n_delay,
               vtss::tag::Name("AverageLastnDelay"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The last N average delay."));
    m.add_leaf(s.avg_delay_var,
               vtss::tag::Name("AverageDelayVariation"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The total average delay variation."));
    m.add_leaf(s.avg_n_delay_var,
               vtss::tag::Name("AverageLastnDelayVariation"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The last N average delay variation."));
    m.add_leaf(s.min_delay,
               vtss::tag::Name("MinimumDelay"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The minimum delay measured."));
    m.add_leaf(s.max_delay,
               vtss::tag::Name("MaximumDelay"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The maximum delay measured."));
    m.add_leaf(s.min_delay_var,
               vtss::tag::Name("MinimumDelayVariation"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The minimum delay variation measured."));
    m.add_leaf(s.max_delay_var,
               vtss::tag::Name("MaximumDelayVariation"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The maximum delay variation measured."));
    m.add_leaf(s.tunit,
               vtss::tag::Name("TimeUnit"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Calculation time unit."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_dm_fd_bin_state_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_dm_fd_bin_state_t"));
    int ix = 2;

    m.add_leaf(vtss::AsInt(s.fd_bin),
               vtss::tag::Name("FdBin"),
               vtss::expose::snmp::Status::Deprecated,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("FD bin value (two-way). "
                                      "This object is deprecated and contains the same value as the TwValue object below."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_dm_ifdv_bin_state_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_dm_ifdv_bin_state_t"));
    int ix = 2;

    m.add_leaf(vtss::AsInt(s.ifdv_bin),
               vtss::tag::Name("IfdvBin"),
               vtss::expose::snmp::Status::Deprecated,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("IFDV bin value (two-way). "
                                      "This object is deprecated and contains the same value as the TwValue object below."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_dm_bin_value_t &s)
{
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_dm_bin_value_t"));
    int ix = 3;

    m.add_leaf(s.two_way,
               vtss::tag::Name("TwValue"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Value for two-way bin."));

    m.add_leaf(s.one_way_fn,
               vtss::tag::Name("OwFtNValue"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Value for one-way far-to-near bin."));

    m.add_leaf(s.one_way_nf,
               vtss::tag::Name("OwNtFValue"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Value for one-way near-to-far bin."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lb_state_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lb_state_t"));
    int ix = 1;

    m.add_leaf(s.transaction_id,
               vtss::tag::Name("TransactionId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("This is the transaction id that is used to send the next LBM."));
    m.add_leaf(s.reply_cnt,
               vtss::tag::Name("ReplyCounter"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The number of replying MAC. In case of 'infinite' LBM there is only one reply."));
    m.add_leaf(vtss::AsCounter(s.lbm_transmitted),
               vtss::tag::Name("LbmTransmitted"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The number of transmitted LBM."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lb_reply_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lb_reply_t"));
    int ix = 1;

    m.add_leaf(s.mac,
               vtss::tag::Name("Mac"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("LBR source MAC - this is only relevant if not 'infinite' LBM and not MPLS loopback"));
    m.add_leaf(vtss::AsCounter(s.lbr_received),
               vtss::tag::Name("LbrReceived"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number LBR received from MAC."));
    m.add_leaf(vtss::AsCounter(s.out_of_order),
               vtss::tag::Name("OutOfOrder"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The OOO counter."));
    m.add_leaf(vtss::AsOctetString(s.mep_mip_id, sizeof(s.mep_mip_id)),
               vtss::tag::Name("MepMipId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Replying MEP/MIP ID - only relevant for loopback on MPLS"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_tst_state_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_tst_state_t"));
    int ix = 1;

    m.add_leaf(vtss::AsCounter(s.tx_counter),
               vtss::tag::Name("TxCounter"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Transmitted TST frames counter."));
    m.add_leaf(vtss::AsCounter(s.rx_counter),
               vtss::tag::Name("RxCounter"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("REceived TST frames counter."));
    m.add_leaf(vtss::AsCounter(s.oo_counter),
               vtss::tag::Name("OutOfOrderCounter"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Out of Order counter."));
    m.add_leaf(s.rx_rate,
               vtss::tag::Name("RxRate"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Receive bit rate in Kbit/s."));
    m.add_leaf(s.time,
               vtss::tag::Name("TestTime"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Test time in seconds. The elapsed time since last clear."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lt_transaction_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lt_transaction_t"));
    int ix = 1;

    m.add_leaf(s.transaction_id,
               vtss::tag::Name("TransactionId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The transaction id of the latest Link Trace."));
    m.add_leaf(s.reply_cnt,
               vtss::tag::Name("ReplyCount"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number of replies for this Link Trace transaction."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lt_reply_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lt_reply_t"));
    int ix = 1;

    m.add_leaf(s.mode,
               vtss::tag::Name("Mode"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The reply is done by a MEP or a MIP."));

    m.add_leaf(s.direction,
               vtss::tag::Name("Direction"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The reply is done by a UP or a Down instance."));
    m.add_leaf(s.ttl,
               vtss::tag::Name("Ttl"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The reply TTL value."));
    m.add_leaf(vtss::AsBool(s.forwarded),
               vtss::tag::Name("Forwarded"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The LTM was forwarded."));
    m.add_leaf(s.relay_action,
               vtss::tag::Name("RelayAction"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The relay action."));
    m.add_leaf(s.last_egress_mac,
               vtss::tag::Name("LastEgressMac"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The Last Egress MAC for this reply."));
    m.add_leaf(s.next_egress_mac,
               vtss::tag::Name("NextEgressMac"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The Next Egress MAC of this reply."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_rt_status_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_rt_status_t"));
    int ix = 1;

    m.add_leaf(s.seq_no,
               vtss::tag::Name("SeqNo"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Sequence number of first RT request."));
    m.add_leaf(s.request_cnt,
               vtss::tag::Name("RequestCount"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number of RT requests sent for current session."));
    m.add_leaf(s.reply_cnt,
               vtss::tag::Name("ReplyCount"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number of replies received for this Route Trace session."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_rt_reply_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_rt_reply_t"));
    int ix = 1;

    m.add_leaf(s.ret_code,
               vtss::tag::Name("ReturnCode"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Return code from reply."));
    m.add_leaf(s.ret_subcode,
               vtss::tag::Name("ReturnSubcode"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Return subcode from reply."));
    m.add_leaf(s.rtt_sec,
               vtss::tag::Name("RttSec"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Round Trip Time (seconds)."));
    m.add_leaf(s.rtt_usec,
               vtss::tag::Name("RttUsec"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Round Trip Time (microseconds)."));
    m.add_leaf(s.ts_sent_sec,
               vtss::tag::Name("TsSentSec"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Timestamp sent (sender, seconds)."));
    m.add_leaf(s.ts_sent_usec,
               vtss::tag::Name("TsSentUsec"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Timestamp sent (sender, microseconds)."));
    m.add_leaf(s.ts_received_sec,
               vtss::tag::Name("TsReceivedSec"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Timestamp received (receiver, seconds)."));
    m.add_leaf(s.ts_received_usec,
               vtss::tag::Name("TsReceivedUsec"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Timestamp received (receiver, microseconds)."));
    m.add_leaf(s.src_global_id,
               vtss::tag::Name("SrcGlobalId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Source global ID from received FEC or source ID TLV in RT reply."));
    m.add_leaf(vtss::AsIpv4(s.src_node_id),
               vtss::tag::Name("SrcNodeId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Source node ID from received FEC or source ID TLV in RT reply."));
    m.add_leaf(s.fec_src_value,
               vtss::tag::Name("FecSrcValue"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("For a reply from a PW MEP this is the source AC-ID from the "
                                      "received FEC in the RT reply. For a Tunnel MEP or LSP MIP bit "
                                      "31:16 is the source tunnel number and bit 15:0 is the LSP number "
                                      "from the received FEC in the RT reply."));
    m.add_leaf(s.rx_if_num,
               vtss::tag::Name("RxIfNum"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Receiver local interface index from reply."));
    m.add_leaf(s.rx_label_cnt,
               vtss::tag::Name("RxLabelCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number of valid label values RxLabel1, RxLabel2 etc."));
    m.add_leaf(s.rx_labels[0],
               vtss::tag::Name("RxLabel1"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("1st MPLS label (top label) of Label Stack of RT request as seen by receiver."));
    m.add_leaf(s.rx_labels[1],
               vtss::tag::Name("RxLabel2"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("2nd MPLS label of Label Stack of RT request as seen by receiver."));
    m.add_leaf(s.rx_labels[2],
               vtss::tag::Name("RxLabel3"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("3rd MPLS label of Label Stack of RT request as seen by receiver."));
    m.add_leaf(s.rx_labels[3],
               vtss::tag::Name("RxLabel4"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("4th MPLS label of Label Stack of RT request as seen by receiver."));
    m.add_leaf(s.rx_labels[4],
               vtss::tag::Name("RxLabel5"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("5th MPLS label of Label Stack of RT request as seen by receiver."));
    m.add_leaf(s.rx_labels[5],
               vtss::tag::Name("RxLabel6"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("6th MPLS label of Label Stack of RT request as seen by receiver."));
    m.add_leaf(s.rx_labels[6],
               vtss::tag::Name("RxLabel7"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("7th MPLS label of Label Stack of RT request as seen by receiver."));
    m.add_leaf(s.rx_labels[7],
               vtss::tag::Name("RxLabel8"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("8th MPLS label of Label Stack of RT request as seen by receiver."));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_peer_cc_state_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_peer_cc_state_t"));
    int ix = 1;

    m.add_leaf(s.os_tlv.oui[0],
               vtss::tag::Name("OsTlvOuiFirst"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Last received OS TLV - The OUI content first nibble"));
    m.add_leaf(s.os_tlv.oui[1],
               vtss::tag::Name("OsTlvOuiSecond"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Last received OS TLV - The OUI content second nibble"));
    m.add_leaf(s.os_tlv.oui[2],
               vtss::tag::Name("OsTlvOuiThird"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Last received OS TLV - The OUI content third nibble"));
    m.add_leaf(s.os_tlv.subtype,
               vtss::tag::Name("OsTlvSubType"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Last received OS TLV - The Sub-Type content"));
    m.add_leaf(s.os_tlv.value,
               vtss::tag::Name("OsTlvValue"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Last received OS TLV - The Value content"));
    m.add_leaf(s.is_tlv.value,
               vtss::tag::Name("IsTlvValue"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Last received IS TLV - The Value content"));
    m.add_leaf(s.ps_tlv.value,
               vtss::tag::Name("PsTlvValue"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Last received PS TLV - The Value content"));
    m.add_leaf(vtss::AsBool(s.os_received),
               vtss::tag::Name("OsTlvReceived"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Last received CCM PDU contained OS TLV"));
    m.add_leaf(vtss::AsBool(s.is_received),
               vtss::tag::Name("IsTlvReceived"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Last received CCM PDU contained IS TLV"));
    m.add_leaf(vtss::AsBool(s.ps_received),
               vtss::tag::Name("PsTlvReceived"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Last received CCM PDU contained PS TLV"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_lm_control_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_lm_control_t"));
    int ix = 1;

    m.add_leaf(vtss::AsBool(s.clear),
               vtss::tag::Name("Clear"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Clear the LM results"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_dm_control_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_dm_control_t"));
    int ix = 1;

    m.add_leaf(vtss::AsBool(s.clear),
               vtss::tag::Name("Clear"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Clear the DM results"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_tst_control_t &s) { int ix = 1;
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_tst_control_t"));

    m.add_leaf(vtss::AsBool(s.clear),
               vtss::tag::Name("Clear"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Clear the TST results"));
}

template<typename T>
void serialize(T &a, vtss_appl_mep_bfd_control_t &s) { int ix = 1;
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_mep_bfd_control_t"));

    m.add_leaf(vtss::AsBool(s.clear),
               vtss::tag::Name("Clear"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Clear the BFD statistics counters"));
}

namespace vtss {
namespace appl {
namespace mep {
namespace interfaces {

struct MepCapabilitiesImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_appl_mep_capabilities_t *>
    > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_mep_capabilities_t &i) {
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_capabilities_get);
};

struct MepConfigInstanceTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_conf_t *>
    > P;

    static constexpr uint32_t snmpRowEditorOid = 100;
    static constexpr const char *table_description =
        "This is a table of created instance configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance configuration parameters. Not all parameters can be changed";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_instance_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_instance_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_mep_instance_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_mep_instance_conf_delete);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_instance_conf_default);
};

struct MepConfigInstancePeerTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_peer_conf_t *>
    > P;

    static constexpr uint32_t snmpRowEditorOid = 100;
    static constexpr const char *table_description =
        "This is a table of peer MEP configuration parameters";

    static constexpr const char *index_description =
        "This is a peer MEP configuration parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(u32 &i) {
        serialize(h, PeerId(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_mep_peer_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_instance_peer_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_peer_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_instance_peer_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_mep_instance_peer_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_mep_instance_peer_conf_delete);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_instance_peer_conf_default);
};

struct MepConfigPmTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_pm_conf_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created instance Performance Monitoring configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Performance Monitoring configuration parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_pm_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_pm_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_pm_conf_set);
};

struct MepConfigLstTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lst_conf_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created instance Link State Tracking configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Link State Tracking configuration parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_lst_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lst_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_lst_conf_set);
};

struct MepConfigCcTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_cc_conf_t *>
    > P;

    static constexpr uint32_t snmpRowEditorOid = 100;
    static constexpr const char *table_description =
        "This is a table of created instance Continuity Check configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Continuity Check configuration parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_cc_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_cc_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_cc_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_cc_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_mep_cc_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_mep_cc_conf_delete);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_cc_conf_default);
};

struct MepConfigLmTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lm_conf_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created instance Loss Measurement configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Loss Measurement parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_lm_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lm_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_lm_conf_set);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_lm_conf_default);
};

struct MepConfigLmAvailTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lm_avail_conf_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created instance Loss Measurement Availability configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Loss Measurement Availability parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_lm_avail_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lm_avail_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_lm_avail_conf_set);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_lm_avail_conf_default);
};

struct MepConfigLmHliTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lm_hli_conf_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created instance Loss Measurement High Loss Interval configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Loss Measurement High Loss Interval parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_lm_hli_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lm_hli_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_lm_hli_conf_set);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_lm_hli_conf_default);
};

struct MepConfigLmSdegTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lm_sdeg_conf_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created instance Loss Measurement Signal Degrade configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Loss Measurement Signal Degrade parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_lm_sdeg_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lm_sdeg_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_lm_sdeg_conf_set);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_lm_sdeg_conf_default);
};

struct MepConfigDmTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_dm_conf_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created instance Delay Measurement configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Delay Measurement parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_dm_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_dm_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_dm_conf_set);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_dm_conf_default);
};

struct MepConfigLbTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lb_conf_t *>
    > P;

    static constexpr uint32_t snmpRowEditorOid = 101;
    static constexpr const char *table_description =
        "This is a table of created instance Loop Back configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Loop Back parameters. This cannot be changed";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_lb_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lb_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_lb_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_lb_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_mep_lb_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_mep_lb_conf_delete);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_lb_conf_default);
};

struct MepConfigTstTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_tst_conf_t *>
    > P;

    static constexpr uint32_t snmpRowEditorOid = 101;
    static constexpr const char *table_description =
        "This is a table of created instance Test Signal configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Test Signal parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_tst_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_tst_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_tst_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_tst_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_mep_tst_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_mep_tst_conf_delete);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_tst_conf_default);
};

struct MepConfigLtTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lt_conf_t *>
    > P;

    static constexpr uint32_t snmpRowEditorOid = 101;
    static constexpr const char *table_description =
        "This is a table of created instance Link Trace configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Link Trace parameters. This cannot be changed";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_lt_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lt_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_lt_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_lt_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_mep_lt_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_mep_lt_conf_delete);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_lt_conf_default);
};

struct MepConfigApsTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_aps_conf_t *>
    > P;

    static constexpr uint32_t snmpRowEditorOid = 101;
    static constexpr const char *table_description =
        "This is a table of created instance Automatic Protection Switching configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Automatic Protection Switching parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_aps_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_aps_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_aps_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_aps_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_mep_aps_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_mep_aps_conf_delete);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_aps_conf_default);
};

struct MepConfigAisTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_ais_conf_t *>
    > P;

    static constexpr uint32_t snmpRowEditorOid = 101;
    static constexpr const char *table_description =
        "This is a table of created instance Alarm Indication Signal configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Alarm Indication Signal parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_ais_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_ais_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_ais_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_ais_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_mep_ais_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_mep_ais_conf_delete);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_ais_conf_default);
};

struct MepConfigLckTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lck_conf_t *>
    > P;

    static constexpr uint32_t snmpRowEditorOid = 101;
    static constexpr const char *table_description =
        "This is a table of created instance Locked Signal configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Locked Signal parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_lck_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lck_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_lck_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_lck_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_mep_lck_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_mep_lck_conf_delete);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_lck_conf_default);
};

struct MepConfigClientFlowTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamKey<vtss_ifindex_t>,
        vtss::expose::ParamVal<vtss_appl_mep_client_flow_conf_t *>
    > P;

    static constexpr uint32_t snmpRowEditorOid = 100;
    static constexpr const char *table_description =
        "This is a table of client flow configuration parameters";

    static constexpr const char *index_description =
        "This is a client flow configuration parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_ifindex_t &i) {
        serialize(h, FlowId(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_mep_client_flow_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_client_flow_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_client_flow_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_client_flow_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_mep_client_flow_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_mep_client_flow_conf_delete);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_client_flow_conf_default);
};

struct MepConfigSyslogTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_syslog_conf_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created instance Syslog configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Syslog configuration parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_syslog_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_syslog_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_syslog_conf_set);
};

struct MepConfigTlvLeaf {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_appl_mep_tlv_conf_t *>
    > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_mep_tlv_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_tlv_conf_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_tlv_conf_set);
};

struct MepConfigBfdTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_g8113_2_bfd_conf_t *>
    > P;

    static constexpr uint32_t snmpRowEditorOid = 100;
    static constexpr const char *table_description =
        "This is a table of created instance Bidirectional Forwarding Detection configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Bidirectional Forwarding Detection configuration parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_g8113_2_bfd_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_g8113_2_bfd_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_g8113_2_bfd_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_g8113_2_bfd_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_mep_g8113_2_bfd_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_mep_g8113_2_bfd_conf_delete);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_g8113_2_bfd_conf_default);
};

struct MepConfigBfdAuthTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_g8113_2_bfd_auth_conf_t *>
    > P;

    static constexpr uint32_t snmpRowEditorOid = 100;
    static constexpr const char *table_description =
        "This is a table of created instance Bidirectional Forwarding Detection Authentication configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Bidirectional Forwarding Detection Authentication configuration parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, KeyInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_g8113_2_bfd_auth_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_g8113_2_bfd_auth_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_g8113_2_bfd_auth_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_g8113_2_bfd_auth_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_mep_g8113_2_bfd_auth_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_mep_g8113_2_bfd_auth_conf_delete);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_g8113_2_bfd_auth_conf_default);
};

struct MepConfigRtTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_rt_conf_t *>
    > P;

    static constexpr uint32_t snmpRowEditorOid = 100;
    static constexpr const char *table_description =
        "This is a table of created instance Route Trace configuration parameters";

    static constexpr const char *index_description =
        "This is a created instance Route Trace configuration parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_rt_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_rt_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_rt_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_rt_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_mep_rt_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_mep_rt_conf_delete);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_mep_rt_conf_default);
};

struct MepStatusInstanceTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_state_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created instance status";

    static constexpr const char *index_description =
        "This is a created instance status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_instance_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_status_iter);
};

struct MepStatusInstancePeerTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_peer_state_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of peer MEP status";

    static constexpr const char *index_description =
        "This is a peer MEP status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(u32 &i) {
        serialize(h, PeerId(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_mep_peer_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_instance_peer_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_peer_status_iter);
};

struct MepStatusLmTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lm_state_t *>
    > P;

    static constexpr const char *table_description =
        "This table show the status of a Loss Measurement session between this MEP"
        " and its first peer MEP."
        "\n"
        "This table is deprecated as it only show the status for the first peer MEP. "
        "It is thus not suitable when using Synthetic Loss Measurement since this option "
        "allow for multiple peers. Use the MepStatusLmPeerTable instead.";

    static constexpr const char *index_description =
        "This is a Loss Measurement status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_lm_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lm_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
};

struct MepStatusLmPeerTableImpl{
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lm_state_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of Loss Measurement peer status";

    static constexpr const char *index_description =
        "This is a Loss Measurement peer status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(u32 &i) {
        serialize(h, PeerId(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_mep_lm_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_instance_peer_lm_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_peer_conf_iter);
};


struct MepStatusLmNotifTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lm_notif_state_t *>
    > P;

    static constexpr const char *table_description =
        "This table show the status of enabled Loss Measurement sessions between "
        "this MEP and all its peer MEPs.";

    static constexpr const char *index_description =
        "This is a Loss Measurement Notification status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_lm_notif_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lm_notif_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_lm_notif_status_iter);
};

struct MepStatusLmAvailTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lm_avail_state_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of Loss Measurement Availability status";

    static constexpr const char *index_description =
        "This is a Loss Measurement Availability status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(u32 &i) {
        serialize(h, PeerId(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_mep_lm_avail_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lm_avail_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_peer_conf_iter);
};

struct MepStatusLmHliTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lm_hli_state_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of Loss Measurement High Loss Interval status";

    static constexpr const char *index_description =
        "This is a Loss Measurement High Loss Interval status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(u32 &i) {
        serialize(h, PeerId(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_mep_lm_hli_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lm_hli_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_lm_hli_status_iter);
};

struct MepStatusDmFdBinTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_dm_fd_bin_state_t*>,
        vtss::expose::ParamVal<vtss_appl_mep_dm_bin_value_t*>
    > P;

    static constexpr const char *table_description =
        "This is a table of Delay Measurement Bin Fd status";

    static constexpr const char *index_description =
        "This is a Delay Measurement Bin Fd status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(u32 &i) {
        serialize(h, BinIndexId(i));
    }

    /*
     * This object collection is added to maintain backward compatibility.
     * The collection values are also present in the vtss_appl_mep_dm_bin_value_t
     * collection.
     */
    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_mep_dm_fd_bin_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    /*
     * This object collection provide both two-way and one-way bin values.
     */
    VTSS_EXPOSE_SERIALIZE_ARG_4(vtss_appl_mep_dm_bin_value_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_dm_fd_bins_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_dm_fd_bins_iter);
};

struct MepStatusDmIfdvBinTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_dm_ifdv_bin_state_t*>,
        vtss::expose::ParamVal<vtss_appl_mep_dm_bin_value_t*>
    > P;

    static constexpr const char *table_description =
        "This is a table of Delay Measurement Bin Ifdv status";

    static constexpr const char *index_description =
        "This is a Delay Measurement Bin Ifdv status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(u32 &i) {
        serialize(h, BinIndexId(i));
    }

    /*
     * This object collection is added to maintain backward compatibility.
     * The collection values are also present in the vtss_appl_mep_dm_bin_value_t
     * collection.
     */
    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_mep_dm_ifdv_bin_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    /*
     * This object collection provide both two-way and one-way bin values.
     */
    VTSS_EXPOSE_SERIALIZE_ARG_4(vtss_appl_mep_dm_bin_value_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_dm_ifdv_bins_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_dm_ifdv_bins_iter);
};

struct MepStatusDmTwTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_dm_state_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of Delay Measurement Two Way status";

    static constexpr const char *index_description =
        "This is a Delay Measurement Two Way status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_dm_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_dm_tw_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
};

struct MepStatusDmOwfnTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_dm_state_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of Delay Measurement One Way Far to Near status";

    static constexpr const char *index_description =
        "This is a Delay Measurement Two Way Far to Near status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_dm_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_dm_owfn_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
};

struct MepStatusDmOwnfTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_dm_state_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of Delay Measurement One Way Near to Far status";

    static constexpr const char *index_description =
        "This is a Delay Measurement Two Way Near to Far status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_dm_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_dm_ownf_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
};

struct MepStatusLbTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lb_state_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of Loop Back status";

    static constexpr const char *index_description =
        "This is a Loop Back status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_lb_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lb_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
};

struct MepStatusLbReplyTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lb_reply_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of Loop Back reply status";

    static constexpr const char *index_description =
        "This is a Loop Back reply status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(u32 &i) {
        serialize(h, ReplyId(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_mep_lb_reply_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lb_reply_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_lb_reply_status_iter);
};

struct MepStatusTstTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_tst_state_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of Test Signal status";

    static constexpr const char *index_description =
        "This is a Test Signal status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_tst_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_tst_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
};

struct MepStatusLtTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lt_transaction_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of Link Trace status";

    static constexpr const char *index_description =
        "This is a Link Trace status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_lt_transaction_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lt_transaction_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
};

struct MepStatusLtReplyTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lt_reply_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of Link Trace reply status";

    static constexpr const char *index_description =
        "This is a Link Trace reply status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(u32 &i) {
        serialize(h, ReplyId(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_mep_lt_reply_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lt_reply_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_lt_reply_status_iter);
};

struct MepStatusRtTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_rt_status_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of Route Trace status";

    static constexpr const char *index_description =
        "This is a Route Trace status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_rt_status_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_rt_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
};

struct MepStatusRtReplyTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_rt_reply_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of Route Trace reply status";

    static constexpr const char *index_description =
        "This is a Route Trace reply status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(u32 &i) {
        serialize(h, SeqNo(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_mep_rt_reply_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_rt_reply_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_rt_reply_status_iter);
};

struct MepStatusCcPeerTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_peer_cc_state_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of peer MEP CC status";

    static constexpr const char *index_description =
        "This is a peer MEP CC status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(u32 &i) {
        serialize(h, PeerId(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_mep_peer_cc_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_cc_peer_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_peer_conf_iter);
};

struct MepStatusBfdTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_g8113_2_bfd_state_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of MEP BFD status";

    static constexpr const char *index_description =
        "This is a MEP BFD status";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_g8113_2_bfd_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_g8113_2_bfd_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_g8113_2_bfd_conf_iter);
};

struct MepControlLmTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_lm_control_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created instance Loss Measurement control parameters";

    static constexpr const char *index_description =
        "This is a created instance Loss Measurement control parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_lm_control_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_lm_control_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_lm_control_set);
};

struct MepControlDmTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_dm_control_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created instance Delay Measurement control parameters";

    static constexpr const char *index_description =
        "This is a created instance Delay Measurement control parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_dm_control_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_dm_control_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_dm_control_set);
};

struct MepControlTstTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_tst_control_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created instance Test Signal control parameters";

    static constexpr const char *index_description =
        "This is a created instance Test Signal control parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_tst_control_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_tst_control_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_instance_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_tst_control_set);
};

struct MepControlBfdTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_mep_bfd_control_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created instance BFD control parameters";

    static constexpr const char *index_description =
        "This is a created instance BFD control parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, MepInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_mep_bfd_control_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_MEP);
    VTSS_EXPOSE_GET_PTR(vtss_appl_mep_bfd_control_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_mep_g8113_2_bfd_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_mep_bfd_control_set);
};

}  // namespace interfaces
}  // namespace mep
}  // namespace appl
}  // namespace vtss
#endif
