/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 
*/

#define MEP_IMPL_G2

#include "mep_g8113_2.h"

namespace mep_g2 {

#define RT_VERSION 1
#define RT_MSG_TYPE_REQUEST 1
#define RT_MSG_TYPE_REPLY   2

static void rt_pdu(u16 version, u16 flags, u8 msg_type, u8 reply_mode, u8 ret_code, u8 ret_subcode,
                   u32 sndHndl, u32 seqNo, u32 ts_sec, u32 ts_usec, u32 tr_sec, u32 tr_usec, u8 *p)
{
    /*lint -e{572} ... avoid Lint Warning "Excessive shift value" */
    p[-2] = OAM_TYPE_G8113_2_RT >> 8;
    p[-1] = OAM_TYPE_G8113_2_RT & 0xff;
    p[ 0] = version >> 8;
    p[ 1] = version;
    p[ 2] = flags >> 8;
    p[ 3] = flags;
    p[ 4] = msg_type;
    p[ 5] = reply_mode;
    p[ 6] = ret_code;
    p[ 7] = ret_subcode;
    p[ 8] = sndHndl >> 24;
    p[ 9] = sndHndl >> 16;
    p[10] = sndHndl >>  8;
    p[11] = sndHndl;
    p[12] = seqNo >> 24;
    p[13] = seqNo >> 16;
    p[14] = seqNo >>  8;
    p[15] = seqNo;
    p[16] = ts_sec >> 24;
    p[17] = ts_sec >> 16;
    p[18] = ts_sec >>  8;
    p[19] = ts_sec;
    p[20] = ts_usec >> 24;
    p[21] = ts_usec >> 16;
    p[22] = ts_usec >>  8;
    p[23] = ts_usec;
    p[24] = tr_sec >> 24;
    p[25] = tr_sec >> 16;
    p[26] = tr_sec >>  8;
    p[27] = tr_sec;
    p[28] = tr_usec >> 24;
    p[29] = tr_usec >> 16;
    p[30] = tr_usec >>  8;
    p[31] = tr_usec;
}

/* get TLVs from pkt and place unsupported TLVs in errTlvList: */
static BOOL get_tlvs(u8 *p, u32 size, u8 msgType,
                     u8 **tlvList, u8 *tlvCnt,
                     u8 **errTlvList, u8 *errTlvCnt,
                     u8 *bestRetCode, u8 *bestRetSubCode)
{
    u32 i;
    u16 tlvType, tlvLen, minTlvLen, subType;
    u8 srcIdTlvCnt = 0, dstIdTlvCnt = 0, fecTlvCnt = 0;

    for (i = 0; i < size; i += (4 + tlvLen)) {
        if (*tlvCnt >= 16 || *errTlvCnt >= 16) {
            T_D("Max. number of TLVs hit");
            break;
        }
        tlvType = (p[i + 0] << 8) | p[i + 1];
        tlvLen  = (p[i + 2] << 8) | p[i + 3];
        minTlvLen = 0;
        switch (tlvType) {
        case  1: /* Target FEC stack TLV              */
            fecTlvCnt++;
            subType = (p[i + 4] << 8) | p[i + 5];
            if (subType == 22)      /* static LSP */
                minTlvLen = 28;
            else if (subType == 23) /* static PW  */
                minTlvLen = 36;
            else
                minTlvLen = tlvLen + 1;
            break;
        case  2: /* Downstream Mapping TLV            */
            minTlvLen = 16;
            break;
        case  3: /* Pad TLV                           */
            minTlvLen = 1;
            break;
        case  7: /* Interface and Label stack TLV     */
            minTlvLen = 12;
            break;
        case 9: /* Errored TLVs */
            if (msgType == RT_MSG_TYPE_REQUEST)
                minTlvLen = tlvLen + 1;
            break;
        case 13: /* Source ID TLV                     */
            minTlvLen = 8;
            srcIdTlvCnt++;
            break;
        case 14: /* Destination ID TLV                */
            minTlvLen = 8;
            dstIdTlvCnt++;
            break;
        case 16: /* Reverse Path Target FEC stack TLV */
            subType = (p[i + 4] << 8) | p[i + 5];
            if (subType == 22)      /* static LSP */
                minTlvLen = 28;
            else if (subType == 23) /* static PW  */
                minTlvLen = 36;
            else
                minTlvLen = tlvLen + 1;
            break;
        case 20: /* Downstream Detailed Mapping TLV   */
            minTlvLen = 16;
            break;
        default:
            if (tlvType < 32768)
                /* One or more of the TLVs was not understood: */
                *bestRetCode = VTSS_APPL_MEP_RT_RETCODE_TLV_ERR;
            break;
        }
        if (msgType == RT_MSG_TYPE_REQUEST) {
            if (*bestRetCode == 2)
                errTlvList[(*errTlvCnt)++] = &p[i];
            else if (tlvLen >= minTlvLen && (i + 4 + tlvLen) <= size)
                tlvList[(*tlvCnt)++] = &p[i];
            else {
                *bestRetCode = VTSS_APPL_MEP_RT_RETCODE_MALFORMED; /* Malformed request received */
                errTlvList[(*errTlvCnt)++] = &p[i];
            }
            if (srcIdTlvCnt > 1 || dstIdTlvCnt > 1)
                *bestRetCode = VTSS_APPL_MEP_RT_RETCODE_MALFORMED; /* Malformed request received */
        } else
            tlvList[(*tlvCnt)++] = &p[i];
        tlvLen  = (tlvLen + 3) & ~3;
    }
    if (msgType == RT_MSG_TYPE_REQUEST && fecTlvCnt != 1)
        return FALSE;
    return TRUE;
}

/* Add FEC stack TLV: */
static u32 add_fec_stack_tlv(vtss_mep_g8113_2_rt_t *inst, u8 *p)
{
    p[0] = 0;
    p[1] = 1; /* FEC TLV */
    p[2] = 0;
    p[4] = 0;
    p[6] = 0;
    if (inst->is_pw) {
        p[3]  = 36; /* FEC TLV length 36 */
        p[5]  = 23; /* subtype static PW */
        p[7]  = 32; /* subtype length 32 */
        memcpy(&p[8], inst->u.pw.src_agi_value, 8);
        p[16] = inst->src_global_id >> 24;
        p[17] = inst->src_global_id >> 16;
        p[18] = inst->src_global_id >>  8;
        p[19] = inst->src_global_id;
        p[20] = inst->src_node_id >> 24;
        p[21] = inst->src_node_id >> 16;
        p[22] = inst->src_node_id >>  8;
        p[23] = inst->src_node_id;
        p[24] = inst->u.pw.src_ac_id >> 24;
        p[25] = inst->u.pw.src_ac_id >> 16;
        p[26] = inst->u.pw.src_ac_id >>  8;
        p[27] = inst->u.pw.src_ac_id;
        p[28] = inst->dst_global_id >> 24;
        p[29] = inst->dst_global_id >> 16;
        p[30] = inst->dst_global_id >>  8;
        p[31] = inst->dst_global_id;
        p[32] = inst->dst_node_id >> 24;
        p[33] = inst->dst_node_id >> 16;
        p[34] = inst->dst_node_id >>  8;
        p[35] = inst->dst_node_id;
        p[36] = inst->u.pw.dst_ac_id >> 24;
        p[37] = inst->u.pw.dst_ac_id >> 16;
        p[38] = inst->u.pw.dst_ac_id >>  8;
        p[39] = inst->u.pw.dst_ac_id;
        return 40;
    }
    p[3]  = 28; /* FEC TLV length 28 */
    p[5]  = 22; /* subtype static LSP */
    p[7]  = 24; /* subtype length 24 */
    p[8]  = inst->src_global_id >> 24;
    p[9]  = inst->src_global_id >> 16;
    p[10] = inst->src_global_id >>  8;
    p[11] = inst->src_global_id;
    p[12] = inst->src_node_id >> 24;
    p[13] = inst->src_node_id >> 16;
    p[14] = inst->src_node_id >>  8;
    p[15] = inst->src_node_id;
    p[16] = inst->u.lsp.src_tunnel_num >> 8;
    p[17] = inst->u.lsp.src_tunnel_num;
    p[18] = inst->u.lsp.src_lsp_num >> 8;
    p[19] = inst->u.lsp.src_lsp_num;
    p[20] = inst->dst_global_id >> 24;
    p[21] = inst->dst_global_id >> 16;
    p[22] = inst->dst_global_id >> 8;
    p[23] = inst->dst_global_id;
    p[24] = inst->dst_node_id >> 24;
    p[25] = inst->dst_node_id >> 16;
    p[26] = inst->dst_node_id >> 8;
    p[27] = inst->dst_node_id;
    p[28] = inst->u.lsp.dst_tunnel_num >> 8;
    p[29] = inst->u.lsp.dst_tunnel_num;
    p[30] = 0;
    p[31] = 0;
    return 32;
}

/* Add source ID TLV: */
static u32 add_srcId_tlv(vtss_mep_g8113_2_rt_t *inst, u8 *p)
{
    p[0]  = 0;
    p[1]  = 13; /* source ID TLV */
    p[2]  = 0;
    p[3]  = 8;
    p[4]  = inst->src_global_id >> 24;
    p[5]  = inst->src_global_id >> 16;
    p[6]  = inst->src_global_id >>  8;
    p[7]  = inst->src_global_id;
    p[8]  = inst->src_node_id >> 24;
    p[9]  = inst->src_node_id >> 16;
    p[10] = inst->src_node_id >>  8;
    p[11] = inst->src_node_id;
    return 12;
}

/* Add destination ID TLV: */
static u32 add_dstId_tlv(vtss_mep_g8113_2_rt_t *inst, u8 *p)
{
    p[0]  = 0;
    p[1]  = 14; /* destination ID TLV */
    p[2]  = 0;
    p[3]  = 8;
    p[4]  = inst->dst_global_id >> 24;
    p[5]  = inst->dst_global_id >> 16;
    p[6]  = inst->dst_global_id >>  8;
    p[7]  = inst->dst_global_id;
    p[8]  = inst->dst_node_id >> 24;
    p[9]  = inst->dst_node_id >> 16;
    p[10] = inst->dst_node_id >>  8;
    p[11] = inst->dst_node_id;
    return 12;
}

/* Add Interface and label stack TLV (addrType 5): */
static u32 add_ifAndLabelStack_tlv(vtss_mep_g8113_2_rt_t *inst, u32 rxIfNum, u32 *labels, u8 labelCnt, u8 *p)
{
    u16 tlvLen = 12 + 4 * labelCnt, i; /* 'K' is 12 for addrType 2 */

    p[0] = 0;
    p[1] = 7; /* Interface and label stack TLV     */
    p[2] = tlvLen >> 8;
    p[3] = tlvLen & 0xff;
    p[4] = 2; /* Address Type is IPv4 Unnumbered (RFC 4379) */
    memset(&p[5], 0, 3 + 4);
    p[12] = rxIfNum >> 24;
    p[13] = rxIfNum >> 16;
    p[14] = rxIfNum >>  8;
    p[15] = rxIfNum;
    for (i = 0; i < labelCnt; ++i) {
        p[4 * i + 16] = labels[i] >> 24;
        p[4 * i + 17] = labels[i] >> 16;
        p[4 * i + 18] = labels[i] >> 8;
        p[4 * i + 19] = labels[i];
    }
    return 4 + tlvLen;
}

static u8 *get_tlv(u8 **tlvList, u8 tlvCnt, u8 tlvType)
{
    u8 i;

    for (i = 0; i < tlvCnt; ++i)
        if (tlvList[i][1] == tlvType && tlvList[i][0] == 0)
            return tlvList[i];
    T_D("TLV tlvType=%u not found", tlvType);
    return NULL;
}

/* Check received FEC: */
static bool check_fec(u8 *rx_fec, u8 *own_fec, BOOL is_pw)
{
    u32 srcGlobalId, srcNodeId, srcAcId;
    u32 dstGlobalId, dstNodeId, dstAcId;

    /* TLV type and subType must match: */
    if (memcmp(rx_fec, own_fec, 8) != 0)
        return FALSE;
    if (is_pw) { /* PW */
        /* check service identifier: */
        if (memcmp(&rx_fec[8], &own_fec[8], 8) != 0)
            return FALSE;
        srcGlobalId = ((own_fec[16] << 24) | (own_fec[17] << 16) |
                       (own_fec[18] <<  8) |  own_fec[19]);
        srcNodeId   = ((own_fec[20] << 24) | (own_fec[21] << 16) |
                       (own_fec[22] <<  8) |  own_fec[23]);
        srcAcId     = ((own_fec[24] << 24) | (own_fec[25] << 16) |
                       (own_fec[26] <<  8) |  own_fec[27]);
        dstGlobalId = ((rx_fec[28] << 24) | (rx_fec[29] << 16) |
                       (rx_fec[30] <<  8) |  rx_fec[31]);
        dstNodeId   = ((rx_fec[32] << 24) | (rx_fec[33] << 16) |
                       (rx_fec[34] <<  8) |  rx_fec[35]);
        dstAcId     = ((rx_fec[36] << 24) | (rx_fec[37] << 16) |
                       (rx_fec[38] <<  8) |  rx_fec[39]);
        if (srcNodeId != dstNodeId)
            return FALSE;
        if (srcGlobalId != 0 && dstGlobalId != 0 && srcGlobalId != dstGlobalId)
            return FALSE;
        if (srcAcId != dstAcId)
            return FALSE;
    } else { /* LSP  */
        srcGlobalId = ((own_fec[ 8] << 24) | (own_fec[ 9] << 16) |
                       (own_fec[10] <<  8) |  own_fec[11]);
        srcNodeId   = ((own_fec[12] << 24) | (own_fec[13] << 16) |
                       (own_fec[14] <<  8) |  own_fec[15]);
        dstGlobalId = ((rx_fec[20] << 24) | (rx_fec[21] << 16) |
                       (rx_fec[22] <<  8) |  rx_fec[23]);
        dstNodeId   = ((rx_fec[24] << 24) | (rx_fec[25] << 16) |
                       (rx_fec[26] <<  8) |  rx_fec[27]);
        if (srcNodeId != dstNodeId)
            return FALSE;
        if (srcGlobalId != 0 && dstGlobalId != 0 && srcGlobalId != dstGlobalId)
            return FALSE;
    }
    return TRUE;
}

/* handle Rx of RT REQUEST message: */
static BOOL rx_request(vtss_mep_g8113_2_rt_t *inst,
                       u8 *pdu, u8 **tlvList, u8 tlvCnt,
                       u8 **errTlvList, u8 errTlvCnt,
                       u8 bestRetCode, u8 bestRetSubCode,
                       mesa_timestamp_t *t, u16 flags,
                       vtss_appl_mep_mode_t mode,
                       u8 *replyPdu, u32 *replyPduLen,
                       u32 *labels, u8 labelCnt, u32 ifNum)
{
    u8 fecStackTlv[40];
    u32 sndHndl, seqNo, ts_sec, ts_usec, tr_sec, tr_usec;
    /*u8 dsMapMismatch*/;
    u8 *p;
    u32 len, bufSize, i, j;
    u16 tx_flags = 0;
    BOOL rxHasMapTlv = get_tlv(tlvList, tlvCnt, 2) != NULL || get_tlv(tlvList, tlvCnt, 20) != NULL;
    u8 myLabelIdx;

    /* FEC stack TLV: */
    (void)add_fec_stack_tlv(inst, fecStackTlv);

    sndHndl = (pdu[ 8] << 24) | (pdu[ 9] << 16) | (pdu[10] << 8) | pdu[11];
    seqNo   = (pdu[12] << 24) | (pdu[13] << 16) | (pdu[14] << 8) | pdu[15];
    ts_sec  = (pdu[16] << 24) | (pdu[17] << 16) | (pdu[18] << 8) | pdu[19];
    ts_usec = (pdu[20] << 24) | (pdu[21] << 16) | (pdu[22] << 8) | pdu[23];

    myLabelIdx = labelCnt - 1;
    if (labelCnt > 1 && (labels[labelCnt - 1] >> 12) == 0xd) { /* GAL */
        myLabelIdx--;
    }

    if (!bestRetCode) {
        /* valid request received */

        /*dsMapMismatch = 0;*/

        if (mode == VTSS_APPL_MEP_MEP) {

            if (flags & 2) { /* only reply if TTL is 1 */
                if ((labels[myLabelIdx] & 0xff) != 1) {
                    T_D("ttl=%u and T flag set -> discard", labels[myLabelIdx] & 0xff);
                    return FALSE;
                }
            }

            /* request received by MEP - endpoint */
            bestRetCode = VTSS_APPL_MEP_RT_RETCODE_IS_EGRESS; /* Replying router is an egress for the FEC at
                                                                 stack-depth */
            bestRetSubCode = myLabelIdx + 1; /* stack depth */

            if (rxHasMapTlv) {
                /* should check Downstream Mapping TLV - if not matching:
                   bestRetCode = VTSS_APPL_MEP_RT_RETCODE_DOWN_MAP_MISMATCH;
                */
            }

            /* if (dsMapMismatch)
                bestRetCode = VTSS_APPL_MEP_RT_RETCODE_DOWN_MAP_MISMATCH;
                else */
            if (flags & 1) { /* Validate FEC stack */
                p = get_tlv(tlvList, tlvCnt, 1);
                if (inst->is_pw) {
                    memcpy(&fecStackTlv[8], inst->u.pw.dst_agi_value, 8);
                }
                if (!p || !check_fec(p, fecStackTlv, inst->is_pw))
                    bestRetCode = VTSS_APPL_MEP_RT_RETCODE_NO_MAPPING; /* Replying router has no mapping for
                                                                          the FEC at stack-depth */
                if (inst->is_pw) {
                    memcpy(&fecStackTlv[8], inst->u.pw.src_agi_value, 8);
                }
            }

            /* Unclear if dstIdTlv should be checked if present ? Which retCode is used if mismatch ? */
        } else {
            /* request received by MIP */
            if (inst->fwd_ok) {
                bestRetCode = VTSS_APPL_MEP_RT_RETCODE_LABEL_SWITCH; /* Label switched at stack-depth */
            } else {
                bestRetCode = VTSS_APPL_MEP_RT_RETCODE_LABEL_SWITCH_NO_FWD; /* Label switched but no MPLS forwarding at stack-depth */
            }
            bestRetSubCode = myLabelIdx + 1; /* stack depth */

            if (rxHasMapTlv) {
                /* should check Downstream Mapping TLV - if not matching:
                   bestRetCode = VTSS_APPL_MEP_RT_RETCODE_DOWN_MAP_MISMATCH;
                */
            }
            /* if (dsMapMismatch) {
              // Downstream Mapping Mismatch
              bestRetCode = VTSS_APPL_MEP_RT_RETCODE_DOWN_MAP_MISMATCH;
            } */
        }
    }

    if (flags & 4) { /* Validate Reverse Path */
        tx_flags = 1; /* Validate FEC stack */
    }

    /* generate RT reply: */
    tr_sec  = t->seconds;
    tr_usec = t->nanoseconds / 1000;
    rt_pdu(RT_VERSION, tx_flags, RT_MSG_TYPE_REPLY, 4 /* Reply via application level control channel */,
           bestRetCode, bestRetSubCode, sndHndl, seqNo, ts_sec, ts_usec, tr_sec, tr_usec, replyPdu);
    *replyPduLen = 32;

    if (flags & 4) { /* Validate Reverse Path */
        /* Add Reverse Path Target FEC stack TLV to reply: */
        fecStackTlv[1] = 16;
        memcpy(&replyPdu[*replyPduLen], fecStackTlv, 4 + fecStackTlv[3]);
        *replyPduLen += 4 + fecStackTlv[3];
    }

    /* add source ID TLV: */
    *replyPduLen += add_srcId_tlv(inst, &replyPdu[*replyPduLen]);

    /* add destination ID TLV: */
    *replyPduLen += add_dstId_tlv(inst, &replyPdu[*replyPduLen]);

    if (rxHasMapTlv) {
        *replyPduLen += add_ifAndLabelStack_tlv(inst, ifNum, labels, labelCnt, &replyPdu[*replyPduLen]);
    } else {
        /* add this useful info anyway: */
        *replyPduLen += add_ifAndLabelStack_tlv(inst, ifNum, labels, labelCnt, &replyPdu[*replyPduLen]);
    }

    /* Pad TLV: */
    p = get_tlv(tlvList, tlvCnt, 3);
    if (p && p[4] == 2) { /* copy Pad TLV to reply */
        len = (p[2] << 8) | p[3];
        memcpy(&replyPdu[*replyPduLen], p, len);
        *replyPduLen += len;
    }

    if (errTlvCnt) {
        /* add Errored TLVs: */
        bufSize = 4;
        for (i = 0; i < errTlvCnt; ++i)
            bufSize += 4 + ((errTlvList[i][2] << 8) | errTlvList[i][3]);
        replyPdu[*replyPduLen + 0] = 0;
        replyPdu[*replyPduLen + 1] = 9; /* Errored TLVs */
        replyPdu[*replyPduLen + 2] = (bufSize - 4) >> 8;
        replyPdu[*replyPduLen + 3] = (bufSize - 4) & 0xff;
        for (i = 0, j = 4; i < errTlvCnt; ++i) {
            len = 4 + ((errTlvList[i][2] << 8) | errTlvList[i][3]);
            memcpy(&replyPdu[*replyPduLen + j], errTlvList[i], len);
            j += len;
        }
        *replyPduLen += bufSize;
    }

    return TRUE;
}

/* handle Rx of RT REPLY message: */
static BOOL rx_reply(vtss_mep_g8113_2_rt_t *inst, u8 *pdu, u8 **tlvList, u8 tlvCnt, u16 flags, mesa_timestamp_t *t)
{
    u32 sndHndl, seqNo, i, j;
    u32 start_sec, start_usec, usec;
    u32 stop_sec, stop_usec;
    u8 fecStackTlv[40];
    u8 *rxFecStackTlv, *srcIdTlv, *ifAndLabelStack;
    u16 tlvLen, labelCnt;

    rxFecStackTlv = get_tlv(tlvList, tlvCnt, 16);
    if (flags & 1) { /* Validate FEC stack */
        /* reverse FEC stack TLV: */
        (void)add_fec_stack_tlv(inst, fecStackTlv);
        if (inst->is_pw) {
            memcpy(&fecStackTlv[8], inst->u.pw.dst_agi_value, 8);
        }
        fecStackTlv[1] = 16;
        /* Check received reverse FEC: */
        if (!rxFecStackTlv || !check_fec(rxFecStackTlv, fecStackTlv, inst->is_pw)) {
            T_D("FEC stack validation failed");
            return FALSE;
        }
    }

    sndHndl = (pdu[ 8] << 24) | (pdu[ 9] << 16) | (pdu[10] << 8) | pdu[11];
    seqNo   = (pdu[12] << 24) | (pdu[13] << 16) | (pdu[14] << 8) | pdu[15];
    if (sndHndl != inst->sndHndl || (seqNo - inst->status.seq_no) > 255) {
        T_D("sndHndl=%u (%u) seqNo=%u (start at %u) mismatch", sndHndl, inst->sndHndl, seqNo, inst->status.seq_no);
        return FALSE;
    }

    if (inst->status.reply_cnt >= VTSS_APPL_MEP_RT_REPLY_MAX) {
        T_W("Number of Route Trace replies exceeds max. (%u), ignoring reply", VTSS_APPL_MEP_RT_REPLY_MAX);
        return FALSE;
    }
    /* request was sent at: */
    start_sec  = (pdu[16] << 24) | (pdu[17] << 16) | (pdu[18] << 8) | pdu[19];
    start_usec = (pdu[20] << 24) | (pdu[21] << 16) | (pdu[22] << 8) | pdu[23];
    usec = start_usec;
    start_usec += 1000000 * start_sec;
    /* reply now received at: */
    stop_sec   = t->seconds;
    stop_usec  = t->nanoseconds / 1000;
    stop_usec += 1000000 * stop_sec;
    i = inst->status.reply_cnt;
    inst->status.reply[i].seq_no           = seqNo;
    inst->status.reply[i].ret_code         = (vtss_appl_mep_rt_ret_code_t) pdu[6];
    inst->status.reply[i].ret_subcode      = pdu[7];
    inst->status.reply[i].rtt_sec          = (stop_usec - start_usec) / 1000000;
    inst->status.reply[i].rtt_usec         = (stop_usec - start_usec) % 1000000;
    inst->status.reply[i].ts_sent_sec      = start_sec;
    inst->status.reply[i].ts_sent_usec     = usec;
    inst->status.reply[i].ts_received_sec  = (pdu[24] << 24) | (pdu[25] << 16) | (pdu[26] << 8) | pdu[27];
    inst->status.reply[i].ts_received_usec = (pdu[28] << 24) | (pdu[29] << 16) | (pdu[30] << 8) | pdu[31];
    if (rxFecStackTlv) {
        if (inst->is_pw) {
            inst->status.reply[i].src_global_id = (rxFecStackTlv[16] << 24) | (rxFecStackTlv[17] << 16) | (rxFecStackTlv[18] << 8) | rxFecStackTlv[19];
            inst->status.reply[i].src_node_id   = (rxFecStackTlv[20] << 24) | (rxFecStackTlv[21] << 16) | (rxFecStackTlv[22] << 8) | rxFecStackTlv[23];
            inst->status.reply[i].fec_src_value = (rxFecStackTlv[24] << 24) | (rxFecStackTlv[25] << 16) | (rxFecStackTlv[26] << 8) | rxFecStackTlv[27];
        } else {
            inst->status.reply[i].src_global_id = (rxFecStackTlv[ 8] << 24) | (rxFecStackTlv[ 9] << 16) | (rxFecStackTlv[10] << 8) | rxFecStackTlv[11];
            inst->status.reply[i].src_node_id   = (rxFecStackTlv[12] << 24) | (rxFecStackTlv[13] << 16) | (rxFecStackTlv[14] << 8) | rxFecStackTlv[15];
            inst->status.reply[i].fec_src_value = (rxFecStackTlv[16] << 24) | (rxFecStackTlv[17] << 16) | (rxFecStackTlv[18] << 8) | rxFecStackTlv[19];
        }
    } else {
        /* No FEC stack TLV in reply */
        srcIdTlv = get_tlv(tlvList, tlvCnt, 13);
        if (srcIdTlv) {
            inst->status.reply[i].src_global_id = (srcIdTlv[4] << 24) | (srcIdTlv[5] << 16) | (srcIdTlv[ 6] << 8) | srcIdTlv[ 7];
            inst->status.reply[i].src_node_id   = (srcIdTlv[8] << 24) | (srcIdTlv[9] << 16) | (srcIdTlv[10] << 8) | srcIdTlv[11];
        } else {
            inst->status.reply[i].src_global_id = 0;
            inst->status.reply[i].src_node_id   = 0;
        }
        inst->status.reply[i].fec_src_value = 0;
    }
    ifAndLabelStack = get_tlv(tlvList, tlvCnt, 7);
    if (ifAndLabelStack) {
        tlvLen = (ifAndLabelStack[2] << 8) | ifAndLabelStack[3];
        labelCnt = (tlvLen - 12) / 4;
        inst->status.reply[i].rx_if_num = (ifAndLabelStack[12] << 24) | (ifAndLabelStack[13] << 16) | (ifAndLabelStack[14] << 8) | ifAndLabelStack[15];
        for (j = 0; j < 8 && j < labelCnt; ++j) {
            inst->status.reply[i].rx_labels[j] = (ifAndLabelStack[4*j+16] << 24) | (ifAndLabelStack[4*j+17] << 16) | (ifAndLabelStack[4*j+18] << 8) | ifAndLabelStack[4*j+19];
        }
        inst->status.reply[i].rx_label_cnt = j;
    } else {
        inst->status.reply[i].rx_label_cnt = 0;
        inst->status.reply[i].rx_if_num = 0;
        memset(inst->status.reply[i].rx_labels, 0, sizeof(inst->status.reply[i].rx_labels));
    }
    inst->status.reply_cnt++;
    return TRUE;
}

u32 vtss_mep_g8113_2_rt_req(vtss_mep_g8113_2_rt_t *inst, u32 hdr_size)
{
    mesa_timestamp_t timestamp;
    u32 dummy, usec, pdu_len;

    vtss_mep_timestamp_get(&timestamp, &dummy);
    usec = timestamp.nanoseconds / 1000;

    /* fill out RT request */
    rt_pdu(RT_VERSION, inst->config.flags, RT_MSG_TYPE_REQUEST, 4 /* Reply via application level control channel */,
           0, 0, inst->sndHndl, inst->seqNo, timestamp.seconds, usec, 0, 0, &inst->tx_rt_buf[hdr_size]);

    /* add FEC stack TLV: */
    pdu_len = 32;
    pdu_len += add_fec_stack_tlv(inst, &inst->tx_rt_buf[hdr_size + pdu_len]);
    if (inst->config.src_id_tlv) {
        /* add source ID TLV: */
        pdu_len += add_srcId_tlv(inst, &inst->tx_rt_buf[hdr_size + pdu_len]);
    }
    if (inst->config.dst_id_tlv) {
        /* add destination ID TLV: */
        pdu_len += add_dstId_tlv(inst, &inst->tx_rt_buf[hdr_size + pdu_len]);
    }
    if (inst->config.pad_tlv_type != VTSS_APPL_MEP_RT_PAD_TLV_TYPE_NONE &&
        inst->config.pad_tlv_len) {
        inst->tx_rt_buf[hdr_size + pdu_len + 0] = 0;
        inst->tx_rt_buf[hdr_size + pdu_len + 1] = 3; /* Pad TLV */
        inst->tx_rt_buf[hdr_size + pdu_len + 2] = inst->config.pad_tlv_len >> 8;
        inst->tx_rt_buf[hdr_size + pdu_len + 3] = inst->config.pad_tlv_len;
        inst->tx_rt_buf[hdr_size + pdu_len + 4] = inst->config.pad_tlv_type;
        pdu_len += 4 + inst->config.pad_tlv_len;
    }

    return pdu_len;
}

/* pdu points just after ACH */
BOOL vtss_mep_g8113_2_rt_rx(vtss_mep_g8113_2_rt_t *inst, u8 *pdu, u32 len, vtss_appl_mep_mode_t mode, u8 *replyPdu, u32 *replyPduLen, u32 *labels, u8 labelCnt, u32 ifNum)
{
    mesa_timestamp_t timestamp;
    u32 dummy;
    u16 version, flags;
    u8  msg_type, reply_mode;
    u8 *tlvList[16], tlvCnt = 0;
    u8 *errTlvList[16], errTlvCnt = 0;
    u8 bestRetCode = 0, bestRetSubCode = 0;

    vtss_mep_timestamp_get(&timestamp, &dummy);

    /* extract fields: */
    version    = (pdu[0] << 8) | pdu[1];
    flags      = (pdu[2] << 8) | pdu[3];
    msg_type   = pdu[4];
    reply_mode = pdu[5];

    /* if MIP and msg_type != REQUEST -> discard */
    if (mode == VTSS_APPL_MEP_MIP && msg_type != 1) {
        T_D("MIP and not REQUEST -> discard");
        return FALSE; /* discard packet */
    }

    /* validate packet, first step (discard): */
    if (len < 32 || version != 1 ||
        (msg_type != 1 && msg_type != 2) || reply_mode != 4) {
        T_D("Invalid PDU fields -> discard");
        return FALSE; /* discard packet */
    }

    /* get TLVs: */
    if (!get_tlvs(&pdu[32], len - 32, msg_type, tlvList, &tlvCnt,
                  errTlvList, &errTlvCnt, &bestRetCode, &bestRetSubCode)) {
        T_D("Invalid packet -> discard");
        return FALSE;
    }

    if (msg_type == RT_MSG_TYPE_REQUEST) {
        return rx_request(inst, pdu, tlvList, tlvCnt, errTlvList, errTlvCnt, bestRetCode, bestRetSubCode, &timestamp, flags, mode, replyPdu, replyPduLen, labels, labelCnt, ifNum);
    } else if (msg_type == RT_MSG_TYPE_REPLY) {
        (void)rx_reply(inst, pdu, tlvList, tlvCnt, flags, &timestamp);
        return FALSE; /* do not reply */
    }
    T_D("Invalid msg_type=%u -> discard", msg_type);
    return FALSE; /* discard packet */
}

}  // mep_g2

