/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

#ifndef _MEP_G8113_2_RT_H_
#define _MEP_G8113_2_RT_H_

#include "main_types.h"
#include "vtss/appl/mep.h"
#include "mep_engine.h"

namespace mep_g2 {

typedef struct {
    /* configuration parameters: */
    vtss_appl_mep_rt_conf_t   config;

    /* RT state parms: */
    u32  sndHndl;                /* Sender's handle                */
    u32  seqNo;                  /* Sequence number                */
    BOOL is_pw;                  /* TRUE if inst is a PW           */
    BOOL fwd_ok;                 /* TRUE if forward ok from here   */
    u8   ttl;                    /* Current TTL                    */
    u8   tx_req_timer;           /* Tx RT request timer (in ticks) */

    u32  src_node_id;
    u32  src_global_id;
    u32  dst_node_id;
    u32  dst_global_id;
    union {
        struct {
            u32  src_ac_id;
            u8   src_agi_value[8];
            u32  dst_ac_id;
            u8   dst_agi_value[8];
        } pw;
        struct {
            u16  src_tunnel_num;
            u16  src_lsp_num;
            u16  dst_tunnel_num;
            u16  dst_lsp_num;
        } lsp;
    } u;

    vtss_appl_mep_rt_status_t status;

    /* these are specific to the MEP module integration: */
    u8    *tx_rt_buf;            /* RT frame tx buffer */
    BOOL  tx_rt_ongoing;
} vtss_mep_g8113_2_rt_t;

/* Fill out RT request: */
u32 vtss_mep_g8113_2_rt_req(vtss_mep_g8113_2_rt_t *inst, u32 hdr_size);

BOOL vtss_mep_g8113_2_rt_rx(vtss_mep_g8113_2_rt_t *inst, u8 *pdu, u32 len, vtss_appl_mep_mode_t mode, u8 *replyPdu, u32 *replyPduLen, u32 *labels, u8 labelCnt, u32 ifNum);

}  // mep_g2

#endif /* _MEP_G8113_2_RT_H_ */
