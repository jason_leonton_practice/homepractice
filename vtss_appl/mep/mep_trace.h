/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _MEP_TRACE_H_
#define _MEP_TRACE_H_


/* ================================================================= *
 *  Trace definitions
 * ================================================================= */

#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>

#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_MEP
#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_MEP

#define VTSS_TRACE_GRP_DEFAULT 0
#define TRACE_GRP_CRIT         1
#define TRACE_GRP_APS_TX       2
#define TRACE_GRP_APS_RX       3
#define TRACE_GRP_TX_FRAME     4
#define TRACE_GRP_RX_FRAME     5
#define TRACE_GRP_DM           6
#define TRACE_GRP_LM           7
#define TRACE_GRP_TST          8
#define TRACE_GRP_LB           9
#define TRACE_GRP_AIS_LCK      10
#define TRACE_GRP_MODEL        11
#define TRACE_GRP_EVENT        12
#define TRACE_GRP_TIMER        13
#define TRACE_GRP_ISDX         14
#define TRACE_GRP_SSF          15
#define TRACE_GRP_CALLBACK     16
#define TRACE_GRP_OUT          17
#define TRACE_GRP_TEMP         18
#define TRACE_GRP_CNT          19

#include <vtss_trace_api.h>

#endif /* _MEP_TRACE_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
