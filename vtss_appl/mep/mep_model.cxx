/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#define MEP_IMPL_G2

#include "main_types.h"
#include "mep_model.h"
#include "mep_trace.h"
#include "packet_api.h"
#include "interrupt_api.h"
#include "mscc/ethernet/switch/api.h"
#include "conf_api.h"
#include "vlan_api.h"
#include "port_api.h"
#include "mstp_api.h"
#include "vtss_common_os.h"
#include "critd_api.h"
#include "vtss/appl/mep.h"  /* To get the VTSS_APPL_MEP_RC_XXX values */

#include "afi_api.h"











#ifdef VTSS_SW_OPTION_QOS
#include "qos_api.h"
#include "vtss/appl/qos.h"
#endif

#include "map.hxx"
#include "vector.hxx"
#include "list.hxx"
#include "vtss_timer_api.h"

namespace mep_g2 {

#define GENERIC_AIS     0
#define GENERIC_LCK     1
#define GENERIC_LAPS    2
#define GENERIC_RAPS    3
#define GENERIC_LLM     4
#define GENERIC_LLR     5
#define P_PORT_NONE     mep_caps.mesa_port_cnt




#define FRAME_HEADER_LENGTH  (6+6+4+4+2) /* DMAC + SMAC + DUMMY_TAG + SUBSCRIBER_TAG + T/L */

#define MIN_FRAME_SIZE  60  // minimum frame size excluding 4 byte for Ethernet FCS









#define MPLS_DOMAIN(domain) (FALSE)


#define VID_OAM_MEG1 0xFFE /* OAM CL-VID identifying MEG 1 */
#define VID_OAM_MEG0 0xFFF /* OAM CL-VID identifying MEG 0 */

#define MCE_ID_ALL_CALC(mep, port, client, tx, inject, leaf) (0x80000000 | ((mep&0xFFF)<<19) | ((client&0x3F)<<13) | ((port&0x3F)<<7) | ((tx&0x0F)<<3) | ((inject&0x03)<<1) | ((leaf&0x01)<<0))
#define MCE_ID_CALC(mep, port, tx, inject, leaf) (MCE_ID_ALL_CALC(mep, port, 0, tx, inject, leaf))
#define MCE_ID_SUB_TX_CALC(mep, port1, port2, inject, leaf) (MCE_ID_ALL_CALC(mep, port1, port2, TX_SUB, inject, leaf))  /* The second port is mapped into the client parameter */
#define MCE_ID_PORT_CLIENT_CALC(port) (MCE_ID_ALL_CALC(0, port, 0, TX_CLIENT_PORT, INJECT, ROOT))
#define MCE_ID_CLIENT_CALC(mep, port, client, inject) (MCE_ID_ALL_CALC(mep, port, client, TX_CLIENT, inject, ROOT))
//#define MCE_ID_SOURCE_CALC(source) ((source&0xF)<<4)

#define RX 0
#define RX_UNTAGGED 1
#define RX_OPCODE 2
#define TX 3
#define TX_SUB 4
#define TX_LOOP 5
#define TX_DM 6
#define TX_CLIENT 7
#define TX_CLIENT_PORT 8

#define EXTRACT 0
#define INJECT  1
#define DISCARD 2
#define LEVEL   3

#define ROOT 0
#define LEAF  1

#define MCE_ID_CALC_FREE 0xC0000000

#define CRIT_ENTER_EXIT() CritdEnterExit __lock_guard__(__LINE__)
#define CRIT_EXIT_ENTER() CritdExitEnter __lock_guard__(__LINE__)

/* TX data to container with AFI information for active "rate" transmission */
typedef struct _afi_data_t {
    BOOL                paused = FALSE;           /* Is the AFI paused */
    u64                 bps_act = 0;              /* Actual BPS for this AFI */
    u32                 afi_id = AFI_ID_NONE;     /* AFI id */
    BOOL                simulated = FALSE;        /* Using simulated AFI? */
    u32                 sim_tgt_inst;             /* simulated AFI target MEP instance */
    vtss::Timer         sim_timer;
} afi_data_t;

#define MEP_TX_AFI_COUNT        2   // Max number of AFIs for a MEP

typedef struct {
    u8                  *frame = NULL;     /* First transmitted frame - this is a place holder for the PDU data to be used during AFI restart */
    mep_model_pdu_tx_t  pdu = {};
    afi_data_t          afi[MEP_TX_AFI_COUNT];     /* AFI data - [1] is for transmission on "second" port */
} tx_data_t;

/*
 * PDU injection control data
 */
typedef struct {
    bool    sim_inject = FALSE;     // Simulate injection of AIS & LCK PDUs?
    u32     target_instance = 0;    // Target MEP instance for simulated injection
} inject_ctrl_data_t;

class inst_rx_isdx {
public:
    u32          rx_port;   /* Rx port  */
    u32          i_vid;     /* Internal VID  */
    mesa_isdx_t  isdx;      /* Rx ISDX  */
    inst_rx_isdx() : rx_port(0), i_vid(0), isdx(VTSS_ISDX_NONE) {}
    inst_rx_isdx(u32 port, u32 vid, mesa_isdx_t isdx) : rx_port(port), i_vid(vid), isdx(isdx) {}
};

class inst_client_config {
public:
    BOOL               oper_state = FALSE;             /* Operational state */
    mep_model_client_t config = {};                    /* Client configuration */
    u32                config_idx = 0;                 /* Client configuration index   */
    mesa_isdx_t        isdx = VTSS_ISDX_NONE;          /* Source Tx ISDX  */
    u32                mce_id = MESA_MCE_ID_LAST;      /* Source MCE ID   */
    mesa_isdx_t        rev_isdx = VTSS_ISDX_NONE;      /* Reverse Tx ISDX */
    u32                rev_mce_id = MESA_MCE_ID_LAST;  /* Reverse MCE ID  */
    inst_client_config() {
        config.domain = MEP_MODEL_EVC;
        config.flow = 0;
    }
    inst_client_config(BOOL oper_state, mep_model_client_t config, u32 config_idx, mesa_isdx_t isdx, u32 mce_id, mesa_isdx_t rev_isdx, u32 rev_mce_id) : oper_state(oper_state), config_idx(config_idx), isdx(isdx), mce_id(mce_id), rev_isdx(rev_isdx), rev_mce_id(rev_mce_id) {
        config = config;
    }
};

typedef struct {
    u32   p_port;       /* the protected port for this protection */
    BOOL  one_plus_one; /* 1p1 or 1:1 arhitechture  */
    BOOL  prot_active;  /* protection active or not */
} port_protection_t;

static void                       timer_cancel(vtss::Timer *timer);     /* Forced to put this declaration here as the function is used in the MepModelInstance destructor */

/* Instance data container and functions */
class MepModelInstance {
    public:
    mep_model_mep_create_t           create = {};                                        /* Configuration */
    mep_model_mep_conf_t             config = {};
    mep_model_ccm_conf_t             ccm_config = {};
    mep_model_lm_conf_t              lm_config = {};
    mep_model_sl_conf_t              sl_config = {};
    mep_model_lb_conf_t              lb_config = {};
    mep_model_tst_conf_t             tst_config = {};
    mep_model_lt_conf_t              lt_config = {};




    vtss::Vector<inst_client_config> client_config;                                   /* Client configuration */
    BOOL                             admin_state = FALSE;
    BOOL                             aSsf = FALSE;
    BOOL                             up_mep_highest_of_two = FALSE;
    BOOL                             mep_highest = FALSE;
    BOOL                             mep_lowest = FALSE;
    mep_model_mep_oper_state_t       oper_state = MEP_MODEL_OPER_DOWN;

    u32                              instance = 0;

    mesa_oam_voe_idx_t               voe_idx = MESA_OAM_VOE_IDX_NONE;                    /* Resources */
    mesa_oam_mip_idx_t               mip_idx = MESA_OAM_MIP_IDX_NONE;
    u16                              sat_cnt_idx = mep_caps.mesa_oam_sat_cosid_ctr_cnt;
    vtss::Vector<mesa_mce_id_t>      mce_ids;

    vtss::Vector<inst_rx_isdx>       rx_isdx;                                            /* ISDX used for reception */
    mesa_isdx_t                      rx_tst_lbr_isdx = VTSS_ISDX_NONE;                   /* ISDX used for TST/LBR reception */
    mesa_isdx_t                      tx_isdx = VTSS_ISDX_NONE;                           /* ISDX used for injection */
    mesa_isdx_t                      tx_sat_dm_isdx = VTSS_ISDX_NONE;                    /* ISDX used for SAT DM injection to avoid hitting the policer*/
    mesa_isdx_t                      tx_rev_isdx = VTSS_ISDX_NONE;                       /* ISDX used for reverse injection */

    BOOL                             dLoop = FALSE;                                      /* Calculated defects not supported by VOE */
    BOOL                             dConfig = FALSE;
    BOOL                             dSsf = FALSE;
    BOOL                             ciSsf = FALSE;                                      /* Client Interface SSF (CI_SSF)  */
    BOOL                             block = FALSE;
    BOOL                             rdi = FALSE;
    u32                              i_vid = 0;                                          /* Internal VID of this instance */
    u32                              leaf_i_vid = 0;                                     /* Internal LEAF VID of this instance */
    u32                              p_port = P_PORT_NONE;                               /* Protecting port for this (Service Down) instance */
    u32                              lowest_level = 0;                                   /* The lowest level this instance on the residence port */

    BOOL                             voe_ccm_enabled = FALSE;                            /* CCM handling is enabled in the VOE */
    BOOL                             forwarding = FALSE;                                 /* Forwarding state in ivid on residence port for this (Down) MEP */
    BOOL                             raps_forwarding = FALSE;                            /* RAPS forwarding state in this MEP */
    mep_model_etree_t                e_tree = MEP_MODEL_ETREE_NONE;                      /* E-TREE type in case this EVC MEP is created in an E-TREE */

    mesa_packet_pipeline_pt_t        inj_pipeline_pt     = MESA_PACKET_PIPELINE_PT_NONE; /* Curent injection pipeline point */
    mesa_packet_pipeline_pt_t        rev_inj_pipeline_pt = MESA_PACKET_PIPELINE_PT_NONE; /* Curent reverse injection pipeline point (AIS, LCK, LTM forwarding) */

    vtss::Map<u32, tx_data_t>        tx_data;                                            /* AFI transmitting data */
    u32                              tx_data_key = 0;

    u64                              tst_mce_counter_last = 0;
    u64                              lbr_mce_counter_last = 0;
    mesa_oam_rx_tx_counter_t         sat_dm_lm_count[VTSS_PRIO_ARRAY_SIZE] = {};         /* "Old" LM counters to be used to clear SAT DM counting per COSID */
    u32                              prio_cosid_map[VTSS_PRIO_ARRAY_SIZE] = {};          /* Used to map between prio and COSid in the subscriber domain. When getting SAT DM counters the prio is mapped to EVC COS through this table. Issue is that the Subcriber VOE is counting on EVC COS */

    mesa_mce_id_t                    sub_inj_mce_id = MESA_MCE_ID_LAST;                  /* Copy of Subscriber domain injection MCE ID to be used when Masqueraded hit in CLM must be updated */
    mesa_mce_id_t                    rx_block_mce_id = MESA_MCE_ID_LAST;                 /* The Port domain ingress data block MCE ID. This MCE is only created when block is enabled */
    mesa_mce_id_t                    tx_block_mce_id = MESA_MCE_ID_LAST;                 /* The Port domain egress data block MCE ID. This MCE is only created when block is enabled */
    mesa_mce_id_t                    vlan_common_mce_id = MESA_MCE_ID_LAST;              /* Copy of first VLAN domain common MCE ID. */
    mesa_mce_id_t                    tst_lbr_rx_mce_id = MESA_MCE_ID_LAST;               /* The MCE ID of the RX MCE rule that the TST/LBR MCE rule must be a copy of and be in front of */
    mesa_mce_id_t                    tst_lbr_mce_id = MESA_MCE_ID_LAST;                  /* The TST matching MCE ID used for TST/LBR frame counting */

    mep_model_mep_oper_state_t       last_oper_state = MEP_MODEL_OPER_DOWN;              /* Debug only. the last fetched information through '_get'functions is saved */
    mep_model_mep_info_t             last_info = {};                                     /* Debug only. */
    mep_model_ccm_status_t           last_ccm_status = {};                               /* Debug only. */
    mep_model_event_status_t         event_status = {};                                  /* Debug only. */
    vtss::Timer                      ccm_rx_timer;
    BOOL                             ccm_rx = TRUE;
    u32                              ccm_longest_period = 0;
};

static mesa_port_list_t los_state;
static u64 pdu_period_to_timer[] = {0,
                                    (3500+1),        /* period 3.33ms*3,5 -> 12 ms. Copy to CPU is 3000ms so 3.5s is used */
                                    (3500+1),        /* period 10ms*3,5 -> 35 ms. Copy to CPU is 3000ms so 3.5s is used */
                                    (3500+1),        /* period 100ms*3,5 -> 350 ms. Copy to CPU is 3000ms so 3.5s is used */
                                    (3500+1),        /* period 1s*3,5 -> 1.000ms*3,5 -> 3500 ms */
                                    (35000+1),       /* period 10s*3,5 -> 10.000ms*3,5 -> 35000 ms */
                                    (210000+1),      /* period 1min*3,5 -> 60.000ms*3,5 -> 210000 ms */
                                    (2100000+1)};    /* period 10 min*3,5 -> 600.000ms*3,5 -> 2100000 ms */
static CapArray<mesa_isdx_t, MESA_CAP_PORT_CNT> client_vlan_source_isdx;

/* Instance date access functions */
static MepModelInstance           *create_instance(u32 instance);
static BOOL                       delete_instance(u32 instance);
static MepModelInstance           *get_instance(u32 instance);
static MepModelInstance           *get_instance_created(u32 *next);
static MepModelInstance           *get_instance_domain(mep_model_domain_t domain,  u32 *next);
static MepModelInstance           *get_instance_up_port(u32 port, u32 *next);
static MepModelInstance           *get_instance_domain_flow(mep_model_domain_t domain,  u32 flow_num,  u32 *next);
static void                       set_instance_rx_isdx(MepModelInstance *data,  mesa_isdx_t isdx,  u32 rx_port,  u32 i_vid);
static void                       del_instance_rx_isdx(MepModelInstance *data);
static void                       del_instance_rx_isdx(MepModelInstance *data, mesa_isdx_t isdx);
static MepModelInstance           *get_instance_tx_isdx(mesa_isdx_t isdx,  u32 residence_port,  u8 *pdu_start);
static MepModelInstance           *get_instance_rx_isdx(mesa_isdx_t isdx,  u32 residence_port,  u32 level,  u32 *rx_port,  u32 *i_vid);
static MepModelInstance           *get_instance_with_tx_isdx(mep_model_domain_t domain,  u32 flow,  u32 vid,  mep_model_direction_t direction,  u32 port);
static MepModelInstance           *get_instance_with_reverse_isdx(mep_model_domain_t domain,  u32 flow,  u32 vid,  mep_model_direction_t direction,  u32 port);
static MepModelInstance           *get_instance_voe(u32 voe);
static MepModelInstance           *get_instance_mep_highest(mep_model_domain_t domain, u32 flow, mep_model_direction_t direction, u32 port);
static MepModelInstance           *get_instance_mep_lowest(mep_model_domain_t domain, u32 flow, mep_model_direction_t direction, u32 port);
static MepModelInstance           *get_instance_mep_highest_port(mep_model_domain_t domain,  u32 flow,  u32 port);
static MepModelInstance           *get_instance_up_mep_lowest_of_two_any(mep_model_domain_t domain,  u32 flow);
static MepModelInstance           *get_instance_up_mep_highest_of_two_any(mep_model_domain_t domain,  u32 flow);
static MepModelInstance           *get_instance_up_mep_highest_of_two(mep_model_domain_t domain,  u32 flow,  u32 port);
static MepModelInstance           *get_instance_down_mep_lowest_of_two(mep_model_domain_t domain,  u32 flow,  u32 port);

/* Protection access functions */
static port_protection_t         *create_protection_entry(u32 w_port);
static BOOL                       delete_protection_entry(u32 w_port);
static port_protection_t         *get_protection_entry(u32 w_port);
static void                       instance_protection_update(u32 w_port, u32 p_port);

/* Callbacks */
static BOOL                       rx_frame_callback(void *contxt,  const u8 *const frame,  const mesa_packet_rx_info_t *const rx_info);
static void                       voe_interrupt_callback(meba_event_t  source_id,  u32 instance_id);
static void                       port_change_callback(mesa_port_no_t  port_no,   port_info_t *info);
static void                       port_shutdown_callback(mesa_port_no_t port_no);
static void                       port_interrupt_callback(meba_event_t source_id,  u32 instance_id);
static void                       vlan_port_change_callback(vtss_isid_t isid,  mesa_port_no_t port_no,  const vtss_appl_vlan_port_detailed_conf_t *new_conf);
static void                       custom_etype_change_callback(mesa_etype_t tpid);
#if defined(VTSS_SW_OPTION_L2PROTO) && defined(VTSS_SW_OPTION_MSTP)
static void                       stp_msti_state_change_callback(vtss_common_port_t l2port, uchar msti, vtss_common_stpstate_t new_state);
#endif



static void                       vlan_membership_change_callback(vtss_isid_t isid, mesa_vid_t vid, vlan_membership_change_t *changes);




/* Internal functions */
static void                       mac_get(u32 port,  u8 mac[VTSS_APPL_MEP_MAC_LENGTH]);
static void                       timer_start(vtss::Timer *timer);
static void                       timer_cancel(vtss::Timer *timer);
static u32                        sim_afi_tx_handler(MepModelInstance *data);
static void                       send_ccm_events(MepModelInstance *data);
static u32                        instance_config(MepModelInstance *data,  BOOL voe_only);
static BOOL                       voe_config(MepModelInstance *data, BOOL lb_start);
static BOOL                       mip_config(MepModelInstance *data);
static BOOL                       port_domain_mce_config(MepModelInstance *data);
static BOOL                       evc_domain_mce_config(MepModelInstance *data);
static BOOL                       subscriber_domain_mce_config(MepModelInstance *data);
static BOOL                       vlan_domain_mce_config(MepModelInstance *data);
static u32                        evc_vlan_domain_config(mep_model_domain_t domain,  u32 evc_id,  u32 instance);



static BOOL                       block_mce_config(MepModelInstance *data);
static BOOL                       client_flow_mce_config(MepModelInstance *data, inst_client_config *client_config);
static BOOL                       tst_lbr_mce_config(MepModelInstance *data);
static BOOL                       tst_lbr_mce_delete(MepModelInstance *data);
static u32                        check_config_instance(u32 instance,  const mep_model_mep_create_t *const create,  const mep_model_mep_conf_t *const config);
static mep_model_mep_conf_t       default_mep_conf();
static void                       voe_rx_tx_block_calculate(MepModelInstance *data,  BOOL *rx_block,  BOOL *tx_block);
static void                       insert_c_s_tag(u8 *header,   u32 tpid,   u32 vid,   u8 prio,   BOOL dei);
static u32                        ether_type(u32 port);
static BOOL                       check_down_transmit_ok(u32 port);
static void                       afi_tx_data_restart(MepModelInstance *data,  mep_model_pdu_tx_id_t tx_id,  tx_data_t *tx_data);
static void                       afi_restart(MepModelInstance *data);
static u32                        afi_tx(MepModelInstance *data,  packet_tx_props_t *tx_props,  u64 rate,  BOOL rate_is_kbps,  BOOL line_rate,  BOOL tx_ok,  afi_data_t *afi, BOOL jitter);
static u32                        sim_afi_tx(MepModelInstance *data,  packet_tx_props_t *tx_props,  u64 rate,  BOOL rate_is_kbps,  BOOL line_rate,  BOOL tx_ok,  afi_data_t *afi,  u32 target_inst);
static void                       free_resources(MepModelInstance *data);
static void                       free_afi_resources(MepModelInstance *data);
static void                       cancel_sim_afi(afi_data_t *afi);
static void                       cancel_afi(tx_data_t  *tx_data, u64 *active_time_ms);
static void                       not_voe_supported_defect_calc(MepModelInstance *data,  u8 *pdu,  u8 *smac);
static void                       operational_port(u32 port);
static void                       los_state_set(const u32 port,  const BOOL state);
static vtss_appl_vlan_port_type_t vlan_port_type_get(u32   port);
static BOOL                       tx_props_create(MepModelInstance *data,  const mep_model_pdu_tx_t *const pdu,  packet_tx_props_t *tx_props, inject_ctrl_data_t *inject_ctrl);
static void                       event_cb(MepModelInstance *data,  mep_model_event_type_t event);
static void                       oper_state_set(MepModelInstance *data,  mep_model_mep_oper_state_t oper_state);
static BOOL                       vlan_flow_info_get(const u32 vid, mesa_port_list_t &nni);
static void                       tx_data_key_free(MepModelInstance *data,  u32 tx_data_key);
static u32                        tx_data_key_alloc(MepModelInstance  *data);
static void                       set_rdi_flag(MepModelInstance *data,  BOOL enable);
static BOOL                       update_dst_port(MepModelInstance *data, port_protection_t *prot, packet_tx_props_t *tx_props, u32 afi);
static BOOL                       get_up_mep_ports(MepModelInstance *data, mesa_port_list_t &ports);
static BOOL                       get_up_mep_ssf(MepModelInstance *data, u32 ignore_port, mesa_port_list_t *ports);
static BOOL                       client_flow_oper_state(vtss::Vector<inst_client_config> &client_config,  const mep_model_client_t &client_flow);
static BOOL                       client_flow_update(u32 flow, mep_model_domain_t domain);
static u32                        vlan_fid_get(u32 vid);
static void                       free_client_flow_mce_resources(MepModelInstance *data);
static void                       port_state_change_event(mesa_port_no_t port_no);
static void                       oper_up_ssf_update(MepModelInstance *data);
static void                       propagate_ssf(MepModelInstance *data);
static void                       propagate_port_ssf(u32 port);
static u32                        static_mac_entry(MepModelInstance *data,  BOOL enable);

/* Model call-back pointers */
static mep_model_event_cb_t mep_model_event_cb = NULL;
static mep_model_rx_cb_t mep_model_rx_cb = NULL;

static CapArray<u32, MESA_CAP_OAM_VOE_CNT> voe_to_instance;
typedef struct
{
    vtss_appl_vlan_port_type_t port_type;
    mesa_vid_t                 pvid;
} mep_vlan_conf_t;
static CapArray<mep_vlan_conf_t, MESA_CAP_PORT_CNT> vlan_port_conf;
static mep_model_init_t init_state = MEP_MODEL_INIT_NONE;
static void *custom_filter_id;
static u8 custom_ethertype[2];
static mesa_oam_voe_event_mask_t voe_event_mask;




struct CritdEnterExit {
    CritdEnterExit(int line) {
        mep_model_critd_enter(__FILE__, line);
        enterLine = line;
    }

    ~CritdEnterExit() {
        mep_model_critd_exit(__FILE__, enterLine);
    }

    u32 enterLine;
};

struct CritdExitEnter {
    CritdExitEnter(int line) {
        mep_model_critd_exit(__FILE__, line);
        exitLine = line;
    }

    ~CritdExitEnter() {
        mep_model_critd_enter(__FILE__, exitLine);
    }

    u32 exitLine;
};


/****************************************************************************/
/*  MEP/MIP create/delete                                                   */
/****************************************************************************/
u32 mep_model_mep_create(const u32                     instance,
                         const mep_model_mep_create_t  *const create)
{
    MepModelInstance   *data;
    port_protection_t *prot;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (create == NULL) {
        T_DG(TRACE_GRP_MODEL, "create pointer invalid");
        return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    }
    data = create_instance(instance);
    if (data == NULL) {
        T_NG(TRACE_GRP_MODEL, "Instance invalid");
        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    }

    data->create = *create;
    data->config = default_mep_conf();

    data->instance = instance;
    data->admin_state = FALSE;
    oper_state_set(data,  MEP_MODEL_OPER_DOWN);

    /* Update instance with protection */
    prot = get_protection_entry(data->config.port);
    if (prot) {     /* Assure that the deafult configuration port is checked for protection */
        data->p_port = prot->p_port;
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(VTSS_RC_OK);
}

BOOL mep_model_mep_delete(const u32 instance)
{
    MepModelInstance *data;

    T_DG(TRACE_GRP_MODEL, "Enter %u", instance);

    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }

    /* cleanup: */
    data->admin_state = FALSE;

    (void)instance_config(data, FALSE);

    (void)delete_instance(instance);

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

u32 mep_model_mep_admin_state_set(const u32   instance,
                                  const BOOL  admin_state)
{
    u32              rc=0;
    MepModelInstance  *data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    }
    if (data->admin_state == admin_state) {
        T_NG(TRACE_GRP_MODEL, "Unchanged admin state");
        return(VTSS_RC_OK);
    }

    data->admin_state = admin_state;

    rc = instance_config(data, FALSE);   /* Configure the instance */
    afi_restart(data);                   /* Start AFI if any */

    T_NG(TRACE_GRP_MODEL, "Exit  rc %s", error_txt(rc));

    return(rc);
}

BOOL mep_model_mep_admin_state_get(const u32    instance,
                                   BOOL *const  admin_state)
{
    MepModelInstance *data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (admin_state == NULL) {
        T_DG(TRACE_GRP_MODEL, "state pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }

    *admin_state = data->admin_state;

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_mep_oper_state_get(const u32                   instance,
                                  mep_model_mep_oper_state_t  *const oper_state)
{
    MepModelInstance *data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (oper_state == NULL) {
        T_DG(TRACE_GRP_MODEL, "oper_state pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }

    *oper_state = data->oper_state;

    data->last_oper_state = *oper_state;     /* Save the last read oper_state for debug purpose */

    /* All the following events are now "cleared" in the sense that status has been fetched in response to event */
    data->event_status.status[MEP_MODEL_EVENT_MEP_STATUS_oper] = FALSE;

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}


/****************************************************************************/
/*  MEP/MIP generic/common configuration                                    */
/****************************************************************************/
u32 mep_model_mep_conf_set(const u32                  instance,
                           const mep_model_mep_conf_t *const config)
{
    u32               rc=0;
    BOOL              voe_only;
    MepModelInstance   *data;
    port_protection_t *prot;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (config == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    }
    if (!config->hw) { /* Only HW based MEP supported */
        T_DG(TRACE_GRP_MODEL, "Only HW supported MEP supported");
        return(VTSS_APPL_MEP_RC_HW_SUPPORT_REQUIRED);
    }
    if (config->level > 7) { /* Invalid level */
        T_DG(TRACE_GRP_MODEL, "Invalid level  %u", config->level);
        return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    }
    if (config->port >= mep_caps.mesa_port_cnt) { /* Invalid port */
        T_DG(TRACE_GRP_MODEL, "Invalid port number  %u",config->port);
        return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    }
    if (config->mepid > 0x1FFF) { /* Invalid MEP id */
        T_DG(TRACE_GRP_MODEL, "Invalid MEP id number  %u", config->mepid);
        return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    }
    if ((data->oper_state == MEP_MODEL_OPER_UP) && ((rc = check_config_instance(instance, &data->create, config)) != VTSS_RC_OK)) {
        T_DG(TRACE_GRP_MODEL, "Instance will be oper-down with this configuration");
        return(rc);
    }
    if (!memcmp(&data->config, config, sizeof(data->config))) {
        T_NG(TRACE_GRP_MODEL, "Unchanged config");
        return(VTSS_RC_OK);
    }

    voe_only = ((data->config.hw != config->hw) || (data->config.level != config->level) || (data->config.port != config->port) || (data->config.vid != config->vid)) ? FALSE : TRUE;

    data->config = *config;

    /* Update instance with protection */
    prot = get_protection_entry(data->config.port);
    if (prot) { /* Assure that potentially new configuration port is checked for protection */
        data->p_port = prot->p_port;
    }

    if (data->admin_state) {    /* Only configure if Admin State is enabled */
        rc = instance_config(data, voe_only);   /* Configure the instance */
        afi_restart(data);            /* Start AFI if any */
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(rc);
}

BOOL mep_model_mep_conf_get(const u32               instance,
                            mep_model_mep_create_t  *const create,
                            mep_model_mep_conf_t    *const config)
{
    MepModelInstance *data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if ((create == NULL) || (config == NULL)) {
        T_DG(TRACE_GRP_MODEL, "create or config pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }

    *create = data->create;
    *config = data->config;

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_mep_info_get(const u32  instance,
                            mep_model_mep_info_t *info)
{
    MepModelInstance *data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (info == NULL) {
        T_DG(TRACE_GRP_MODEL, "info pointer invalid");
        return(FALSE);
    }
    memset(info, 0, sizeof(*info));
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }

    info->e_tree = data->e_tree;
    info->voe_ccm_enabled = data->voe_ccm_enabled;
    info->dSsf = data->dSsf;
    info->ciSsf = data->ciSsf;
    info->forwarding = data->forwarding;

    data->last_info = *info;    /* Save the last read info for debug purpose */

    /* All the following events are now "cleared" in the sense that status has been fetched in response to event */
    data->event_status.status[MEP_MODEL_EVENT_MEP_STATUS_dSsf] = FALSE;
    data->event_status.status[MEP_MODEL_EVENT_MEP_STATUS_ciSsf] = FALSE;
    data->event_status.status[MEP_MODEL_EVENT_MEP_STATUS_forwarding] = FALSE;

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}


BOOL mep_model_mep_debug_get(const u32               instance,
                              mep_model_mep_debug_t  *info)
{
    MepModelInstance *data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (info == NULL) {
        T_DG(TRACE_GRP_MODEL, "info pointer invalid");
        return(FALSE);
    }
    memset(info, 0, sizeof(*info));
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }

    info->event_status = data->event_status;
    info->last_ccm_status = data->last_ccm_status;
    info->last_info = data->last_info;

    return(TRUE);
}

BOOL mep_model_mep_assf_set(const u32   instance,
                            const BOOL  aSsf)
{
    MepModelInstance *data;

    T_NG(TRACE_GRP_MODEL, "Enter  instance %u  aSsf %u", instance, aSsf);

    data = get_instance(instance);

    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->oper_state != MEP_MODEL_OPER_UP) {
        T_DG(TRACE_GRP_MODEL, "Instance not operational");
        return(FALSE);
    }
    data->aSsf = aSsf;

    T_DG(TRACE_GRP_SSF, "instance %u  %s %u %s-MEP  port:%d  level:%d  aSsf:%d", instance,
         data->create.domain == MEP_MODEL_PORT ? "Port" : data->create.domain == MEP_MODEL_VLAN ? "VLAN" : data->create.domain == MEP_MODEL_EVC ? "EVC" : "?", data->create.flow_num,
         data->create.direction == MEP_MODEL_UP ? "Up" : "Down",
         data->config.port, data->config.level, aSsf);

    propagate_ssf(data);

    T_NG(TRACE_GRP_MODEL, "Exit");

    return TRUE;
}

BOOL mep_model_mep_block_set(const u32   instance,
                             const BOOL  block)
{
    u32                  rc;
    MepModelInstance      *data, *hdata;
    mesa_oam_voe_conf_t  cfg;

    T_DG(TRACE_GRP_MODEL, "Enter  instance %u  block %u", instance, block);

    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->oper_state != MEP_MODEL_OPER_UP) {
        T_DG(TRACE_GRP_MODEL, "Instance not operational");
        return(FALSE);
    }
    if (data->block == block) {
        T_DG(TRACE_GRP_MODEL, "Unchanged block");
        return(TRUE);
    }

    data->block = block;

    /* Get the MEP is on highest level on this port. This is doing the data blocking */
    hdata = get_instance_mep_highest_port(data->create.domain, data->create.flow_num, data->config.port);

    if (hdata == NULL) {
        T_DG(TRACE_GRP_MODEL, "MEP instance on highest level was not found!!!");
        return(TRUE);
    }
    T_DG(TRACE_GRP_MODEL, "Highest hdata %p  instance %u  voe %u  vid %u", hdata, hdata->instance, hdata->voe_idx, hdata->i_vid);

    if ((rc = mesa_oam_voe_conf_get(NULL, hdata->voe_idx, &cfg)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_conf_get() failed  voe %u  %s", hdata->voe_idx, error_txt(rc));
    }

    voe_rx_tx_block_calculate(hdata, &cfg.block_data_rx, &cfg.block_data_tx);

    if ((rc = mesa_oam_voe_conf_set(NULL, hdata->voe_idx, &cfg)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_conf_set() failed  voe %u  %s", hdata->voe_idx, error_txt(rc));
    }

    if (hdata->create.direction == MEP_MODEL_DOWN) {  /* If VOE is not used for RX blocking a separate MCE rule is created to block all ingress data in this flow */
        if (block) {  /* Create block MCE when block is enabled */
            (void)block_mce_config(data);
        } else {      /* Delete the block MCE rule when block is disabled */
            if (data->rx_block_mce_id != MESA_MCE_ID_LAST) {
                if ((rc = mesa_mce_del(NULL, data->rx_block_mce_id)) != VTSS_RC_OK) {
                    T_EG(TRACE_GRP_MODEL, "mesa_mce_del failed instance %u mce_id %X %s", data->instance, data->rx_block_mce_id, error_txt(rc));
                }
                data->rx_block_mce_id = MESA_MCE_ID_LAST;
            }
            if (data->tx_block_mce_id != MESA_MCE_ID_LAST) {
                if ((rc = mesa_mce_del(NULL, data->tx_block_mce_id)) != VTSS_RC_OK) {
                    T_EG(TRACE_GRP_MODEL, "mesa_mce_del failed instance %u mce_id %X %s", data->instance, data->tx_block_mce_id, error_txt(rc));
                }
                data->tx_block_mce_id = MESA_MCE_ID_LAST;
            }
        }
    }

    T_DG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

void mep_model_port_protection_change(const u32   w_port,
                                      const u32   p_port,
                                      const BOOL  active)
{
    port_protection_t  *prot;
    MepModelInstance    *data;
    u32                next = 0;

    /* Port protection has changed state */
    T_DG(TRACE_GRP_MODEL, "enter w_port: %d  p:%d  active:%d",w_port,p_port,active);

    prot = get_protection_entry(w_port);
    if (prot == NULL) {
        T_NG(TRACE_GRP_MODEL, "Could not get protection entry");
        return;
    }
    if (prot->prot_active != active) {
        prot->prot_active = active;
        /* Find any Port/EVC/VLAN/MPLS Down-MEP instance and restart AFI */
        while ((data = get_instance_created(&next)) != NULL) {
            if (data->config.port == w_port) {
                afi_restart(data);
            }
        }
    }

    T_NG(TRACE_GRP_MODEL, "Exit");
}

void mep_model_port_protection_create(BOOL       one_plus_one,
                                      const u32  w_port,
                                      const u32  p_port)
{
    port_protection_t  *prot;

    T_NG(TRACE_GRP_MODEL, "Enter %u", w_port);

    prot = create_protection_entry(w_port);
    if (prot == NULL) {
        T_EG(TRACE_GRP_MODEL, "Could not create protection entry");
        return;
    }

    prot->p_port = p_port;
    prot->one_plus_one = one_plus_one;

    /* Update mep instances with the protected port */
    instance_protection_update(w_port, p_port);

    T_NG(TRACE_GRP_MODEL, "Exit");
}

void mep_model_port_protection_delete(const u32  w_port,
                                      const u32  p_port)
{
    T_NG(TRACE_GRP_MODEL, "Enter %u", w_port);

    /* Remove protected port from MEP instances */
    instance_protection_update(w_port, P_PORT_NONE);

    /* Remove from internal structure */
    if (!delete_protection_entry(w_port)) {
        T_EG(TRACE_GRP_MODEL, "Could not delete protection entry");
    }

    T_NG(TRACE_GRP_MODEL, "Exit");
}



void mep_model_ring_protection_block(const u32 port,  BOOL block)
{
    /* Ring protection VLAN blocking state changed on a port */
    /* Domain already locked */

    T_NG(TRACE_GRP_MODEL, "enter");

    port_state_change_event((mesa_port_no_t)port);

    T_NG(TRACE_GRP_MODEL, "Exit");
}


/****************************************************************************/
/*  CCM configuration/status                                                */
/****************************************************************************/
BOOL mep_model_ccm_conf_set(const u32                   instance,
                            const mep_model_ccm_conf_t  *const config)
{
    u32                  rc=0;
    MepModelInstance      *data;
    mesa_oam_voe_conf_t  cfg;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (config == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (config->lm_enable && (data->sl_config.enable_tx || data->sl_config.enable_rx)) {
        T_DG(TRACE_GRP_MODEL, "Synthetic LM is already enabled");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }
    if (!memcmp(&data->ccm_config, config, sizeof(data->ccm_config))) {
        T_DG(TRACE_GRP_MODEL, "Unchanged config");
        return(TRUE);
    }

    data->ccm_config = *config;

    /* Configure VOE */
    if (!voe_config(data, FALSE)) {
        T_NG(TRACE_GRP_MODEL, "VOE configuration failed");
        return(FALSE);
    }

    /* Reset the CCM sequence numbers */
    if ((rc = mesa_oam_voe_conf_get(NULL, data->voe_idx, &cfg)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_conf_get() failed %s", error_txt(rc));
    }

    cfg.ccm.tx_seq_no = 0;
    cfg.ccm.rx_seq_no = 0;

    if ((rc = mesa_oam_voe_conf_set(NULL, data->voe_idx, &cfg)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_conf_set() failed %s", error_txt(rc));
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_ccm_conf_get(const u32            instance,
                            mep_model_ccm_conf_t *const config)
{
    MepModelInstance  *data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (config == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    *config = data->ccm_config;

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_ccm_rdi_set(const u32   instance,
                           const BOOL  enable)
{
    u32                    rc=0;
    MepModelInstance        *data;

    T_NG(TRACE_GRP_MODEL, "Enter  instance %u  enable %u", instance, enable);

    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }
    if (data->rdi == enable) {
        T_DG(TRACE_GRP_MODEL, "Unchanged config");
        return(TRUE);
    }

    data->rdi = enable;

    if (data->voe_ccm_enabled) {   /* Only in case of VOE CCM enabled */
        if ((rc = mesa_oam_voe_ccm_set_rdi_flag(NULL, data->voe_idx, enable)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_ccm_set_rdi_flag failed %s", error_txt(rc));
            return(FALSE);
        }
    } else {
        set_rdi_flag(data, enable);
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_ccm_status_get(const u32               instance,
                              mep_model_ccm_status_t  *const status)
{
    u32                    rc=0;
    MepModelInstance        *data;
    mesa_oam_ccm_status_t  ccm_status;
    mesa_oam_voe_counter_t voe_counters;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (status == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(FALSE);
    }
    memset(status, 0, sizeof(*status));
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    if ((rc = mesa_oam_ccm_status_get(NULL, data->voe_idx, &ccm_status)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_ccm_status_get failed %s", error_txt(rc));
        return(FALSE);
    }
    if ((rc = mesa_oam_voe_counter_get(NULL, data->voe_idx, &voe_counters)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_counter_get failed %s", error_txt(rc));
        return(FALSE);
    }

    status->dLoc = ccm_status.loc;

    if (data->ccm_rx) {
        status->dInv = ccm_status.zero_period_err;
        status->dLevel = ccm_status.mel_low_err;
        status->dMeg = ccm_status.meg_id_err;
        status->dMep = ccm_status.mep_id_err;
        status->dRdi = ccm_status.rx_rdi;
        status->dPeriod = ccm_status.period_err;
        status->dPrio = ccm_status.priority_err;
        status->dLoop = data->dLoop;
        status->dConfig = data->dConfig;
    }

    status->ps_tlv_value = ccm_status.port_status_tlv_value;
    status->if_tlv_value = ccm_status.if_status_tlv_value;

    status->valid_counter = voe_counters.ccm.rx_valid_count;
    status->invalid_counter = voe_counters.ccm.rx_invalid_count;
    status->oo_counter = voe_counters.ccm.rx_invalid_seq_no;

    data->last_ccm_status = *status;    /* Save the last read status for debug purpose */

    /* All the following events are now "cleared" in the sense that status has been fetched in response to event */
    data->event_status.status[MEP_MODEL_EVENT_CCM_STATUS_dInv] = FALSE;
    data->event_status.status[MEP_MODEL_EVENT_CCM_STATUS_dLevel] = FALSE;
    data->event_status.status[MEP_MODEL_EVENT_CCM_STATUS_dMeg] = FALSE;
    data->event_status.status[MEP_MODEL_EVENT_CCM_STATUS_dMep] = FALSE;
    data->event_status.status[MEP_MODEL_EVENT_CCM_STATUS_dLoc] = FALSE;
    data->event_status.status[MEP_MODEL_EVENT_CCM_STATUS_dRdi] = FALSE;
    data->event_status.status[MEP_MODEL_EVENT_CCM_STATUS_dPeriod] = FALSE;
    data->event_status.status[MEP_MODEL_EVENT_CCM_STATUS_dPrio] = FALSE;
    data->event_status.status[MEP_MODEL_EVENT_CCM_STATUS_dLoop] = FALSE;
    data->event_status.status[MEP_MODEL_EVENT_CCM_STATUS_dConfig] = FALSE;
    data->event_status.status[MEP_MODEL_EVENT_CCM_STATUS_tlv_port] = FALSE;
    data->event_status.status[MEP_MODEL_EVENT_CCM_STATUS_tlv_if] = FALSE;

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

/****************************************************************************/
/*  APS/RAPS configuration                                                  */
/****************************************************************************/
u32 mep_model_raps_forwarding(const u32    instance,
                              const BOOL   enable)
{
    u32                  rc=0;
    MepModelInstance      *data;
    mesa_oam_voe_conf_t  cfg;

    T_NG(TRACE_GRP_MODEL, "Enter %u  enable %u", instance, enable);

    data = get_instance(instance);

    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(VTSS_RC_ERROR);
    }

    data->raps_forwarding = enable;

    if (data->voe_idx == MESA_OAM_VOE_IDX_NONE) {
        T_WG(TRACE_GRP_MODEL, "data->voe_idx == MESA_OAM_VOE_IDX_NONE");
        return(VTSS_RC_ERROR);
    }

    if ((rc = mesa_oam_voe_conf_get(NULL, data->voe_idx, &cfg)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_conf_get() failed %s", error_txt(rc));
        return(VTSS_RC_ERROR);
    }

    cfg.generic[GENERIC_RAPS].forward = data->raps_forwarding;

    if ((rc = mesa_oam_voe_conf_set(NULL, data->voe_idx, &cfg)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_conf_set() failed %s", error_txt(rc));
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return VTSS_RC_OK;
}


/****************************************************************************/
/*  SL configuration/status                                                 */
/****************************************************************************/
BOOL mep_model_sl_conf_set(const u32                   instance,
                           const mep_model_sl_conf_t  *const config)
{
    MepModelInstance  *data;
    mep_model_sl_conf_t old_config;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (config == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }
    if ((config->enable_tx || config->enable_rx) && (data->ccm_config.lm_enable || data->lm_config.enable)) {
        T_DG(TRACE_GRP_MODEL, "Service frame based LM is already enabled");
        return(FALSE);
    }
    if (!memcmp(&data->sl_config, config, sizeof(data->sl_config))) {
        T_DG(TRACE_GRP_MODEL, "Unchanged config");
        return(TRUE);
    }

    old_config = data->sl_config;
    data->sl_config = *config;

    if ((old_config.enable_tx && !data->sl_config.enable_tx) || (old_config.enable_rx && !data->sl_config.enable_rx)) {
        /*
         * We are now disabling SLM and we need to handle a potential chip bug.
         * We read VOE counters as they may otherwise not be correct if read afterwards.
         */
        mep_model_sl_status_t status;
        if (!mep_model_sl_status_get(instance, &status)) {
            T_DG(TRACE_GRP_MODEL, "Unable to get SL status");
            return FALSE;
        }
    }

    /* Configure VOE */
    if (!voe_config(data, FALSE)) {
        T_NG(TRACE_GRP_MODEL, "VOE configuration failed");
        return(FALSE);
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_sl_conf_get(const u32             instance,
                           mep_model_sl_conf_t  *const config)
{
    MepModelInstance  *data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (config == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    *config = data->sl_config;

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_sl_status_get(const u32               instance,
                             mep_model_sl_status_t  *const status)
{
    u32                    rc=0;
    MepModelInstance        *data;
    mesa_oam_voe_counter_t voe_counters;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (status == NULL) {
        T_DG(TRACE_GRP_MODEL, "status pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    memset(status, 0, sizeof(*status));
    if ((rc = mesa_oam_voe_counter_get(NULL, data->voe_idx, &voe_counters)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_counter_get failed %s", error_txt(rc));
        return(FALSE);
    }

    status->tx_slm = voe_counters.sl.tx_slm;

    // the chip has no RX SLM counter so we assume that it is equal to the total number of SLR PDUs sent.
    status->rx_slm = 0;
    for (int i = 0; i < MEP_MODEL_MEP_PEER_MAX; i++) {
        status->rx_slm += voe_counters.sl.tx_slr[i];
    }

    memcpy(status->rx_slr, voe_counters.sl.rx_slr, sizeof(status->rx_slr));
    memcpy(status->tx_slr, voe_counters.sl.tx_slr, sizeof(status->tx_slr));

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_sl_meas_expired(const u32 instance)
{
    u32                     rc=0;
    MepModelInstance         *data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    // Enable Hit-Me-Once (HMO) for SLR PDUs
    if ((rc = mesa_oam_voe_arm_hitme(NULL, data->voe_idx, TRUE)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "vtss_oam_voe_arm_hitme failed rc = %s", error_txt(rc));
        return(FALSE);
    }

    return(TRUE);
}

/****************************************************************************/
/*  LM configuration/status                                                 */
/****************************************************************************/
BOOL mep_model_lm_conf_set(const u32                   instance,
                           const mep_model_lm_conf_t  *const config)
{
    MepModelInstance      *data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (config == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }
    if (!memcmp(&data->lm_config, config, sizeof(data->lm_config))) {
        T_DG(TRACE_GRP_MODEL, "Unchanged config");
        return(TRUE);
    }

    data->lm_config = *config;

    /* Configure VOE */
    if (!voe_config(data, FALSE)) {
        T_NG(TRACE_GRP_MODEL, "VOE configuration failed");
        return(FALSE);
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_lm_conf_get(const u32             instance,
                           mep_model_lm_conf_t  *const config)
{
    MepModelInstance  *data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (config == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    *config = data->lm_config;

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_lm_status_get(const u32              instance,
                             mep_model_lm_status_t  *const status)
{
    u32                    rc=0;
    MepModelInstance        *data;
    mesa_oam_voe_counter_t voe_counters;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (status == NULL) {
        T_DG(TRACE_GRP_MODEL, "status pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    memset(status, 0, sizeof(*status));
    if ((rc = mesa_oam_voe_counter_get(NULL, data->voe_idx, &voe_counters)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_counter_get failed %s", error_txt(rc));
        return(FALSE);
    }

    status->tx_lm = voe_counters.lm.tx_lmm;
    status->tx_lmr = voe_counters.lm.tx_lmr;
    status->rx_lmm = voe_counters.lm.rx_lmm;
    status->rx_lm = voe_counters.lm.rx_lmr;

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_lm_status_clear(const u32 instance, bool is_sl, mep_model_clear_dir_t direction)
{
    u32                     rc=0;
    MepModelInstance        *data;
    uint32_t                clear_mask = 0;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    clear_mask = is_sl ? MESA_OAM_CNT_SL : MESA_OAM_CNT_LM;
    if (direction == MEP_MODEL_CLEAR_DIR_BOTH || direction == MEP_MODEL_CLEAR_DIR_TX) {
        clear_mask |= MESA_OAM_CNT_DIR_TX;
    }
    if (direction == MEP_MODEL_CLEAR_DIR_BOTH || direction == MEP_MODEL_CLEAR_DIR_RX) {
        clear_mask |= MESA_OAM_CNT_DIR_RX;
    }

    T_I("instance %u, clearing LM counters, dir %u, mask %X", instance, direction, clear_mask);

    if ((rc = mesa_oam_voe_counter_clear(NULL, data->voe_idx, clear_mask)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_counter_clear failed %s", error_txt(rc));
        return(FALSE);
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}


/****************************************************************************/
/*  DM configuration/status                                                 */
/****************************************************************************/
BOOL mep_model_dm_status_get(const u32              instance,
                             const u8               prio,
                             mep_model_dm_status_t  *const status)
{
    u32                    rc=0, voe_prio;
    MepModelInstance        *data;
    mesa_oam_voe_counter_t voe_counters;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (status == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    memset(status, 0, sizeof(*status));
    if ((rc = mesa_oam_voe_counter_get(NULL, data->voe_idx, &voe_counters)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_counter_get failed %s", error_txt(rc));
        return(FALSE);
    }

    if (data->config.sat) {
        voe_prio = data->prio_cosid_map[prio];
        status->tx_dm = voe_counters.lm.lm_count[voe_prio].tx - data->sat_dm_lm_count[voe_prio].tx;
        status->rx_dm = voe_counters.lm.lm_count[voe_prio].rx - data->sat_dm_lm_count[voe_prio].rx;
    } else {
        status->tx_dm = voe_counters.dm.tx_dmm;
        status->rx_dm = voe_counters.dm.rx_dmr;
    }
    status->tx_dmr = voe_counters.dm.tx_dmr;
    status->rx_dmm = voe_counters.dm.rx_dmm;

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_dm_status_clear(const u32  instance,
                               const u8   prio)
{
    u32                    rc=0;
    MepModelInstance        *data;
    mesa_oam_voe_counter_t voe_counters;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    if ((rc = mesa_oam_voe_counter_clear(NULL, data->voe_idx, MESA_OAM_CNT_DM)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_counter_clear failed %s", error_txt(rc));
        return(FALSE);
    }

    if (data->config.sat) {
        if ((rc = mesa_oam_voe_counter_get(NULL, data->voe_idx, &voe_counters)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_counter_get failed %s", error_txt(rc));
            return(FALSE);
        }
        memcpy(&data->sat_dm_lm_count[prio], &voe_counters.lm.lm_count[prio], sizeof(data->sat_dm_lm_count[prio]));   /* "Clear" SAT DM counters */
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}


/****************************************************************************/
/*  TST configuration/status                                                */
/****************************************************************************/
BOOL mep_model_tst_conf_set(const u32                   instance,
                            const mep_model_tst_conf_t  *const config)
{
    u32                   rc=0;
    MepModelInstance       *data;
    mesa_evc_counters_t   mce_counters;

    T_DG(TRACE_GRP_MODEL, "Enter %u  enable %u", instance, config->enable);

    if (config == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }
    if (!memcmp(&data->tst_config, config, sizeof(data->tst_config))) {
        T_DG(TRACE_GRP_MODEL, "Unchange config");
        return(TRUE);
    }

    /* Check for hit me once copy TST to CPU */
    if (config->copy_to_cpu) {
        if ((rc = mesa_oam_voe_arm_hitme(NULL, data->voe_idx, TRUE)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "vtss_oam_voe_arm_hitme failed rc = %s", error_txt(rc));
            return(FALSE);
        }
    }
    if (config->enable == data->tst_config.enable) {    /* No change in enable */
        return(TRUE);
    }

    data->tst_config = *config;
    data->tst_config.copy_to_cpu = FALSE;

    /* Configure VOE */
    if (!voe_config(data, FALSE)) {
        T_NG(TRACE_GRP_MODEL, "voe_config() failed");
        return(FALSE);
    }
    if (data->create.direction == MEP_MODEL_DOWN) { /* The Down VOE is not able to count received TST frame correctly Bugzilla#20381 */
        if (config->enable) {
            if (!tst_lbr_mce_config(data)) {
                T_NG(TRACE_GRP_MODEL, "tst_lbr_mce_config() failed");
                return(FALSE);
            }
        } else {
            if (data->tst_lbr_mce_id != MESA_MCE_ID_LAST) {
                if ((rc = mesa_mce_counters_get(NULL, data->tst_lbr_mce_id, data->config.port, &mce_counters)) != VTSS_RC_OK) { /* Do the last read of MCE counters before del */
                    T_EG(TRACE_GRP_MODEL, "mesa_mce_counters_get failed %s", error_txt(rc));
                    return(FALSE);
                }
                data->tst_mce_counter_last = mce_counters.rx_green.frames + mce_counters.rx_yellow.frames + mce_counters.rx_red.frames;

                tst_lbr_mce_delete(data);
            }
        }
    }
    T_DG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_tst_conf_get(const u32                   instance,
                            mep_model_tst_conf_t        *const config)
{
    MepModelInstance  *data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (config == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(FALSE);
    }
    memset(config, 0, sizeof(*config));
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    *config = data->tst_config;

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_tst_status_get(const u32               instance,
                              const u8                prio,
                              mep_model_tst_status_t  *const status)
{
    u32                           rc=0;
    MepModelInstance               *data;
    mesa_oam_voe_counter_t        voe_counters;
    mesa_oam_sat_cosid_counter_t  sat_cnt;
    mesa_evc_counters_t           mce_counters;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (status == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }
    if (prio >= MEP_MODEL_PRIORITY_MAX) {
        T_DG(TRACE_GRP_MODEL, "priority value invalid");
        return(FALSE);
    }

    memset(status, 0, sizeof(*status));
    if ((rc = mesa_oam_voe_counter_get(NULL, data->voe_idx, &voe_counters)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_counter_get failed %s", error_txt(rc));
        return(FALSE);
    }

    if (data->config.sat && (data->sat_cnt_idx != mep_caps.mesa_oam_sat_cosid_ctr_cnt)) {
        if ((rc = mesa_oam_sat_cosid_counters_get(NULL, data->voe_idx, data->sat_cnt_idx, &sat_cnt)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_oam_sat_cosid_counters_get failed %s", error_txt(rc));
            return(FALSE);
        }

        status->tx_counter = sat_cnt.tx_ccm_lbm_tst[prio];
        status->rx_counter = sat_cnt.rx_ccm_lbr_tst[prio];
        status->oo_counter  = sat_cnt.rx_lbr_trans_id_err[prio];
        status->crc_err_counter = voe_counters.lb.rx_lbr_crc_err;
    } else {
        status->tx_counter      = voe_counters.tst.tx_tst;
        status->rx_counter      = voe_counters.tst.rx_tst;
        status->oo_counter      = voe_counters.tst.rx_tst_trans_id_err;
        status->crc_err_counter = voe_counters.tst.rx_tst_crc_err;
    }

    if (data->tst_config.enable && (data->tst_lbr_mce_id != MESA_MCE_ID_LAST)) {     /* This is fixing Bugzilla#20381 */
        if ((rc = mesa_mce_counters_get(NULL, data->tst_lbr_mce_id, data->config.port, &mce_counters)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_counters_get failed %s", error_txt(rc));
            return(FALSE);
        }
        status->rx_counter = mce_counters.rx_green.frames + mce_counters.rx_yellow.frames + mce_counters.rx_red.frames;
        status->oo_counter = 0; /* This is counting incorrectly with "large" frames - cannot be fixed */
        data->tst_mce_counter_last = status->rx_counter;
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}


/* instance:    Instance number of MEP   */
/* Clear the TST status counters         */
BOOL mep_model_tst_status_clear(const u32 instance,
                                const u8  prio)
{
    u32                    rc=0;
    MepModelInstance        *data;

    T_DG(TRACE_GRP_MODEL, "Enter %u", instance);

    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    if ((rc = mesa_oam_voe_counter_clear(NULL, data->voe_idx, MESA_OAM_CNT_TST)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_counter_clear failed %s", error_txt(rc));
        return(FALSE);
    }

    if (data->config.sat && (data->sat_cnt_idx != mep_caps.mesa_oam_sat_cosid_ctr_cnt)) {
        if ((rc = mesa_oam_sat_prio_cosid_counters_clear(NULL, data->voe_idx, data->sat_cnt_idx, prio)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_oam_sat_prio_cosid_counters_clear failed %s", error_txt(rc));
            return(FALSE);
        }
    }

    if (data->tst_lbr_mce_id != MESA_MCE_ID_LAST) {     /* This is fixing Bugzilla#20381 */
        if ((rc = mesa_mce_counters_clear(NULL, data->tst_lbr_mce_id, data->config.port)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_counters_clear failed %s", error_txt(rc));
            return(FALSE);
        }
    }

    T_DG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}


/****************************************************************************/
/*  LB configuration/status                                                 */
/****************************************************************************/
BOOL mep_model_lb_conf_set(const u32                  instance,
                           const mep_model_lb_conf_t  *const config)
{
    BOOL                 lb_enable_change;
    MepModelInstance      *data;

    T_DG(TRACE_GRP_MODEL, "Enter %u  enable %u", instance, config->enable);

    if (config == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }
    if (!memcmp(&data->lb_config, config, sizeof(data->lb_config))) {
        T_DG(TRACE_GRP_MODEL, "Unchanged config");
        return(TRUE);
    }

    lb_enable_change = (!data->lb_config.enable && config->enable) ? TRUE : FALSE;
    data->lb_config = *config;

    /* Configure VOE */
    if (!voe_config(data, lb_enable_change)) {
        T_NG(TRACE_GRP_MODEL, "VOE configuration failed");
        return(FALSE);
    }
    if (data->create.direction == MEP_MODEL_DOWN) { /* The Down VOE is not able to count received LBR frame correctly Bugzilla#20381 */
        if (config->enable) {
            if (!tst_lbr_mce_config(data)) {
                T_NG(TRACE_GRP_MODEL, "tst_lbr_mce_config() failed");
                return(FALSE);
            }
        } else {
            /*
             * Bugzilla #22025: Don't delete the MCE entry here - we wait until the client reads the MCE counters
             */
        }
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_lb_conf_get(const u32           instance,
                           mep_model_lb_conf_t *const config)
{
    MepModelInstance  *data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (config == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    *config = data->lb_config;

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_lb_status_get(const u32              instance,
                             const u8               prio,
                             mep_model_lb_status_t  *const status)
{
    u32                           rc=0;
    MepModelInstance               *data;
    mesa_oam_voe_counter_t        voe_counters;
    mesa_oam_proc_status_t        voe_proc_status;
    mesa_oam_sat_cosid_counter_t  sat_cnt;
    mesa_evc_counters_t           mce_counters;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (status == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    memset(status, 0, sizeof(*status));
    if ((rc = mesa_oam_voe_counter_get(NULL, data->voe_idx, &voe_counters)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_counter_get failed %s", error_txt(rc));
        return(FALSE);
    }

    if ((rc = mesa_oam_proc_status_get(NULL, data->voe_idx, &voe_proc_status)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_proc_status_get failed %s", error_txt(rc));
        return(FALSE);
    }

    if (data->config.sat && (data->sat_cnt_idx != mep_caps.mesa_oam_sat_cosid_ctr_cnt)) {
        if ((rc = mesa_oam_sat_cosid_counters_get(NULL, data->voe_idx, data->sat_cnt_idx, &sat_cnt)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_oam_sat_cosid_counters_get failed %s", error_txt(rc));
            return(FALSE);
        }

        status->lbm_counter = sat_cnt.tx_ccm_lbm_tst[prio];
        status->lbr_counter = sat_cnt.rx_ccm_lbr_tst[prio];
        status->oo_counter  = sat_cnt.rx_lbr_trans_id_err[prio];
        status->lbr_crc_err = voe_counters.lb.rx_lbr_crc_err;
        status->trans_id = sat_cnt.tx_ccm_lbm_tst[prio];
    } else {
        status->lbr_counter = voe_counters.lb.rx_lbr;
        status->lbm_counter = voe_counters.lb.tx_lbm;
        status->oo_counter  = voe_counters.lb.rx_lbr_trans_id_err;
        status->lbr_crc_err = voe_counters.lb.rx_lbr_crc_err;
        status->trans_id    = voe_proc_status.tx_next_lbm_transaction_id;
    }

    if (data->tst_lbr_mce_id != MESA_MCE_ID_LAST) {     /* This is fixing Bugzilla#20381 */
        if ((rc = mesa_mce_counters_get(NULL, data->tst_lbr_mce_id, data->config.port, &mce_counters)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_counters_get failed %s", error_txt(rc));
            return(FALSE);
        }
        status->lbr_counter = mce_counters.rx_green.frames + mce_counters.rx_yellow.frames + mce_counters.rx_red.frames;
        status->oo_counter = 0; /* This is counting incorrectly with "large" frames - cannot be fixed */
        data->lbr_mce_counter_last = status->lbr_counter;

        if (!data->lb_config.enable) {
            // Bugzilla #22025: Delete the MCE entry after the client has read the counters.
            tst_lbr_mce_delete(data);
        }
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}


/* instance:    Instance number of MEP   */
/* Clear the LB status counters          */
BOOL mep_model_lb_status_clear(const u32 instance,
                               const u8  prio)
{
    u32                    rc=0;
    MepModelInstance        *data;

    T_DG(TRACE_GRP_MODEL, "Enter %u  prio %u", instance, prio);

    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    if ((rc = mesa_oam_voe_counter_clear(NULL, data->voe_idx, MESA_OAM_CNT_LB)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_counter_clear failed %s", error_txt(rc));
        return(FALSE);
    }

    if (data->config.sat && (data->sat_cnt_idx != mep_caps.mesa_oam_sat_cosid_ctr_cnt)) {
        if ((rc = mesa_oam_sat_prio_cosid_counters_clear(NULL, data->voe_idx, data->sat_cnt_idx, prio)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_oam_sat_prio_cosid_counters_clear failed %s", error_txt(rc));
            return(FALSE);
        }
    }

    if (data->tst_lbr_mce_id != MESA_MCE_ID_LAST) {     /* This is fixing Bugzilla#20381 */
        if ((rc = mesa_mce_counters_clear(NULL, data->tst_lbr_mce_id, data->config.port)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_counters_clear failed %s", error_txt(rc));
            return(FALSE);
        }
    }

    T_DG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

/****************************************************************************/
/*  LT configuration                                                        */
/****************************************************************************/

BOOL mep_model_lt_conf_set(const u32                  instance,
                           const mep_model_lt_conf_t  *const config)
{
    MepModelInstance      *data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (config == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }
    if (!memcmp(&data->lt_config, config, sizeof(data->lt_config))) {
        T_DG(TRACE_GRP_MODEL, "Unchanged config");
        return(TRUE);
    }

    data->lt_config = *config;

    /* Configure VOE */
    if (!voe_config(data, FALSE)) {
        T_NG(TRACE_GRP_MODEL, "VOE configuration failed");
        return(FALSE);
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

BOOL mep_model_lt_conf_get(const u32           instance,
                           mep_model_lt_conf_t *const config)
{
    MepModelInstance  *data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (config == NULL) {
        T_DG(TRACE_GRP_MODEL, "config pointer invalid");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    *config = data->lt_config;

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}


/****************************************************************************/
/*  AIS/LCK client flow configuration                                       */
/****************************************************************************/

BOOL mep_model_client_flow_add(const u32                instance,
                               const mep_model_client_t *client_flow)
{
    MepModelInstance    *data;
    inst_client_config client_config;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);
    T_DG(TRACE_GRP_MODEL, "domain %u  flow %u", client_flow->domain, client_flow->flow);

    if (client_flow == NULL) {
        T_DG(TRACE_GRP_MODEL, "client_flow pointer invalid");
        return FALSE;
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return FALSE;
    }
    if (data->client_config.size() >= MEP_MODEL_CLIENT_FLOW_MAX) {
        T_DG(TRACE_GRP_MODEL, "Number of max client flows reached");
        return FALSE;
    }

    client_config.config_idx = data->client_config.size();
    client_config.config = *client_flow;

    if (!client_flow_mce_config(data, &client_config)) {
        T_DG(TRACE_GRP_MODEL, "client_flow_mce_config failed");
        return FALSE;
    }

    data->client_config.push_back(client_config);
    operational_port(data->config.port);

    return TRUE;
}

BOOL mep_model_client_flow_clear(const u32 instance)
{
    MepModelInstance *data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return FALSE;
    }

    free_client_flow_mce_resources(data);
    data->client_config.clear();
    operational_port(data->config.port);

    return TRUE;
}

/****************************************************************************/
/*  OAM PDU Rx/Tx                                                           */
/****************************************************************************/
/* Register OAM PDU Rx callback function: */
void mep_model_rx_register(mep_model_rx_cb_t cb)
{
    T_NG(TRACE_GRP_MODEL, "Enter");

    if (cb == NULL) {
        T_DG(TRACE_GRP_MODEL, "call back pointer invalid");
        return;
    }
    mep_model_rx_cb = cb;

    T_NG(TRACE_GRP_MODEL, "Exit");
}

/*
 * Return TRUE if the following conditions are all true:
 * - The MEP is a down MEP in the Port domain
 * - The port is part of a L-APS protection group
 * - The port is currently not active
 */
static BOOL is_port_prot_standby(MepModelInstance *data)
{
    // Check if this is a 'down' port MEP
    if (data->create.domain != MEP_MODEL_PORT || data->create.direction != MEP_MODEL_DOWN) {
        return FALSE;
    }

    // check if this port is a working port
    port_protection_t *prot = get_protection_entry(data->config.port);
    if (prot != NULL) {
        // this was the working port in a protection group
        T_DG(TRACE_GRP_MODEL, "Instance %u (working), protection active:%u", data->instance, prot->prot_active);
        return prot->prot_active;
    }

    T_NG(TRACE_GRP_MODEL, "Instance %u, no protection", data->instance);

    return FALSE;
}

u32 mep_model_tx(const u32                 instance,
                 const mep_model_pdu_tx_t  *const pdu,
                 mep_model_pdu_tx_id_t     *const tx_id)
{
    u32                       rc=0, pdu_offset, indx, afi_no;
    BOOL                      tx_ok, tx_down;
    packet_tx_props_t         tx_props;
    MepModelInstance           *data;
    u8                        *frame = 0;
    tx_data_t                 *tx_data;
    mep_model_pdu_tx_id_t     alloc_key;
    port_protection_t         *prot = NULL;
    inject_ctrl_data_t        inject_ctrl;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if ((pdu == NULL) || (pdu->data == NULL) || (tx_id == NULL)) {
        T_DG(TRACE_GRP_MODEL, "Input pointer invalid");
        return(VTSS_APPL_MEP_RC_TX_ERROR);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    }
    if (data->oper_state != MEP_MODEL_OPER_UP) {
        T_DG(TRACE_GRP_MODEL, "Instance not operational");
        return(VTSS_APPL_MEP_RC_STATE_NOT_UP);
    }

    /* If the tx_id is pointing to an active transmission it will be cancelled */
    mep_model_tx_cancel(instance, tx_id, NULL);

    tx_down = ((data->create.domain == MEP_MODEL_MPLS_LSP) || // always treat LSP MIPs as down
               ((data->create.direction == MEP_MODEL_DOWN) && !pdu->reverse_inj) ||
               ((data->create.direction == MEP_MODEL_UP) && pdu->reverse_inj)) ? TRUE : FALSE;

    /* Check if ok to transmit. In case of down injection it might be prohibited */
    if (tx_down) {
        tx_ok = check_down_transmit_ok(data->config.port);
    } else {
        tx_ok = TRUE;

        if (pdu->reverse_inj && is_port_prot_standby(data)) {
            tx_ok = FALSE;
        }
    }
    if (!pdu->rate && !tx_ok) {    /* This is SW generated frame and transmit is not allowed */
        T_DG(TRACE_GRP_MODEL, "Transmit not allowed");
        return(VTSS_APPL_MEP_RC_TX_ERROR);
    }
    T_NG(TRACE_GRP_TX_FRAME, "tx_ok %u  pdu.len %u  pdu.rate %u  opcode %u", tx_ok, pdu->len, pdu->rate, pdu->data[1]);

    /* Calculate the potentially required number of AFI depending on port protection */
    afi_no = 1;  /* Without protection one AFI is required */
    if ((data->p_port != P_PORT_NONE) && tx_down && ((prot = get_protection_entry(data->config.port)) != NULL)) {
        afi_no = 2; /* This is Down injection in a port protection. Potentially two AFI are required */
    }

    if (!tx_props_create(data, pdu, &tx_props, &inject_ctrl)) {   /* Create tx_props */
        T_NG(TRACE_GRP_MODEL, "tx_props_create failed");
        return(VTSS_APPL_MEP_RC_TX_ERROR);
    }
    frame = tx_props.packet_info.frm;    /* For convenience - a frame pointer and PDU offset is in the tx_props */
    pdu_offset = tx_props.tx_info.pdu_offset;

    /* Check for SW CCM-RDI control */
    if (!data->voe_ccm_enabled && (frame[pdu_offset+1] == 1)) {    /* VOE based CCM is not enabled and this is CCM in the frame */
        frame[pdu_offset+2] &= ~0x80;
        frame[pdu_offset+2] |= (data->rdi) ? 0x80 : 0x00;  /* RDI must be controlled in the frame */
    }

    /* Transmit */
    if (!pdu->rate) {    /* This is SW generated frame - NO AFI */
        T_DG(TRACE_GRP_TX_FRAME, "instance %u  port_mask 0x%" PRIx64 "  isdx %u  pipeline %u  vid %u  cosid %u  cos %u  pcp %u  dp %u  len %zu  maskarade %u  switched %u  oam_type %u  pdu_off %u  frame %X-%X-%X-%X-%X-%X-%X-%X-%X",
             instance, tx_props.tx_info.dst_port_mask, tx_props.tx_info.isdx, tx_props.tx_info.pipeline_pt, tx_props.tx_info.tag.vid, tx_props.tx_info.cosid, tx_props.tx_info.cos, tx_props.tx_info.tag.pcp,
             tx_props.tx_info.dp, tx_props.packet_info.len, tx_props.tx_info.masquerade_port, tx_props.tx_info.switch_frm, tx_props.tx_info.oam_type, tx_props.tx_info.pdu_offset,
             frame[12+0], frame[12+1], frame[12+2], frame[12+3], frame[12+4], frame[12+5], frame[12+6], frame[12+7], frame[12+8]);
        T_NG(TRACE_GRP_TX_FRAME, "DMAC-SMAC %X-%X-%X-%X-%X-%X-%X-%X-%X-%X-%X-%X",
             frame[0], frame[1], frame[2], frame[3], frame[4], frame[5], frame[6], frame[7], frame[8], frame[9], frame[10], frame[11]);
        for(indx = 0; indx < afi_no; indx++) {
            if (!update_dst_port(data, prot, &tx_props, indx)) {    /* Check for transmission on 'second' port in port protection */
                continue;
            }
            if ((rc = packet_tx(&tx_props)) != VTSS_RC_OK) {    /* Transmit */
                T_EG(TRACE_GRP_MODEL, "packet_tx() failed %s", error_txt(rc));
                return(VTSS_APPL_MEP_RC_TX_ERROR);
            }
        }
    } else { /* This is AFI based */
        T_DG(TRACE_GRP_TX_FRAME, "AFI injection start  frame %p  prio %u  dei %u  opcode %u", frame, pdu->prio, pdu->dei, pdu->data[1]);

        if ((alloc_key = tx_data_key_alloc(data)) != MEP_MODEL_PDU_TX_ID_NONE) { /* Get unused tx_data key */
            auto res = data->tx_data.emplace(vtss::piecewise_construct, vtss::forward_as_tuple(alloc_key), vtss::forward_as_tuple());
            if (!res.second) { // emplace failed
                packet_tx_free(frame);
                tx_data_key_free(data, alloc_key);

                T_DG(TRACE_GRP_MODEL, "tx_data.emplace failed");
                return(VTSS_APPL_MEP_RC_TX_ERROR);
            }

            *tx_id = alloc_key;
            tx_data = &(res.first->second);

            if (!pdu->client_inj || client_flow_oper_state(data->client_config, pdu->client_flow)) { /* Do not transmit into a client flow that is not operational */
                for (indx = 0; indx < afi_no; indx++) {
                    if (!update_dst_port(data, prot, &tx_props, indx)) {        /* Check for transmission on 'second' port in port protection */
                        continue;
                    }

                    if (!inject_ctrl.sim_inject) {
                        // Transmit using normal AFI
                        rc = afi_tx(data,  &tx_props,  pdu->rate,  pdu->rate_is_kbps,  pdu->line_rate, tx_ok, &tx_data->afi[indx], pdu->jitter);
                    } else {
                        // Transmit using simulated AFI
                        rc = sim_afi_tx(data,  &tx_props,  pdu->rate,  pdu->rate_is_kbps,  pdu->line_rate, tx_ok, &tx_data->afi[indx], inject_ctrl.target_instance);
                    }

                    if (rc != VTSS_RC_OK) {
                        packet_tx_free(frame);
                        tx_data_key_free(data, alloc_key);
                        data->tx_data.erase(res.first);

                        T_WG(TRACE_GRP_MODEL, "afi_tx failed");
                        return(rc);
                    }

                    T_NG(TRACE_GRP_TX_FRAME, "AFI data  indx %u  afi_id %u  bps_act " VPRI64u "  tx_ok %u", indx, tx_data->afi[indx].afi_id, tx_data->afi[indx].bps_act, tx_ok);
                }
            }

            /* Save TX data */
            T_NG(TRACE_GRP_TX_FRAME, "Save TX data  tx_id %u  prio %u  dei %u", *tx_id, pdu->prio, pdu->dei);
            tx_data->pdu = *pdu;                        /* Save the 'pdu' for later AFI restart */
            tx_data->pdu.data = &frame[pdu_offset];     /* In 'pdu' the data element is a pointer to PDU that is now in the frame */
            tx_data->frame = frame;                     /* Save frame pointer for free when TX is disabled */
        }
    } /* AFI based injection */

    T_NG(TRACE_GRP_MODEL, "Exit");

    return VTSS_RC_OK;
}

/* Stop/cancel OAM PDU Tx: */
void mep_model_tx_cancel(const u32              instance,
                         mep_model_pdu_tx_id_t  *const tx_id,
                         u64                    *active_time_ms)
{
    MepModelInstance                     *data;
    tx_data_t                            *tx_data;
    vtss::Map<u32, tx_data_t>::iterator  i;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (tx_id == NULL) {
        T_DG(TRACE_GRP_MODEL, "Invalid pointer");
        return;
    }
    T_DG(TRACE_GRP_MODEL, "tx_id %u", *tx_id);
    if (*tx_id == MEP_MODEL_PDU_TX_ID_NONE) {  /* OK to call with "invalid" ID just do nothing */
        T_NG(TRACE_GRP_MODEL, "Invalid tx_id");
        return;
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return;
    }
    if (data->oper_state != MEP_MODEL_OPER_UP) {
        T_DG(TRACE_GRP_MODEL, "Instance not operational");
        return;
    }

    i = data->tx_data.find(*tx_id);
    if (i == data->tx_data.end()) {
        T_DG(TRACE_GRP_MODEL, "TX ID was not valid");
        return;
    }

    /* Free the afi recourses for this TX ID */
    tx_data = &(*i).second;
    T_DG(TRACE_GRP_TX_FRAME, "Instance %u  tx_id %u  opcode %u", instance, *tx_id, tx_data->pdu.data[1]);
    cancel_afi(tx_data, active_time_ms);
    if (tx_data->frame != NULL) {
        packet_tx_free(tx_data->frame);  /* Frame buffer must be freed */
        tx_data->frame = NULL;
    }
    tx_data_key_free(data, (*i).first);
    data->tx_data.erase(i);

    *tx_id = MEP_MODEL_PDU_TX_ID_NONE;  /* This TX ID is no more valid */

    T_NG(TRACE_GRP_MODEL, "Exit");
}

BOOL mep_model_tx_bps_act_get(const u32                 instance,
                              mep_model_pdu_tx_id_t     tx_id,
                              u64                       *const bps_act)
{
    MepModelInstance                      *data;
    tx_data_t                            *tx_data;
    vtss::Map<u32, tx_data_t>::iterator  i;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (bps_act == NULL) {
        T_DG(TRACE_GRP_MODEL, "Invalid pointer");
        return(FALSE);
    }
    *bps_act = 0;
    T_DG(TRACE_GRP_MODEL, "tx_id %u", tx_id);
    if (tx_id == MEP_MODEL_PDU_TX_ID_NONE) {
        T_DG(TRACE_GRP_MODEL, "Invalid tx_id");
        return(FALSE);
    }
    data = get_instance(instance);
    if (data == NULL) {
        T_DG(TRACE_GRP_MODEL, "Instance invalid");
        return(FALSE);
    }
    if (data->oper_state != MEP_MODEL_OPER_UP) {
        T_DG(TRACE_GRP_MODEL, "Instance not operational");
        return(FALSE);
    }

    i = data->tx_data.find(tx_id);
    if (i == data->tx_data.end()) {
        T_DG(TRACE_GRP_MODEL, "TX ID was not valid");
        return(FALSE);
    }

    tx_data = &(*i).second;
    *bps_act = tx_data->afi[0].bps_act;

    T_NG(TRACE_GRP_MODEL, "Exit");
    return(TRUE);
}

/****************************************************************************/
/*  Events                                                                  */
/****************************************************************************/

void mep_model_event_register(mep_model_event_cb_t cb)
{
    T_NG(TRACE_GRP_MODEL, "Enter");

    if (cb == NULL) {
        T_DG(TRACE_GRP_MODEL, "call back pointer invalid");
    }
    mep_model_event_cb = cb;

    T_NG(TRACE_GRP_MODEL, "Exit");
}

/****************************************************************************/
/*  Initialize                                                                  */
/****************************************************************************/
void mep_model_init(mep_model_init_t  init)
{
    u32                     fast_period, i, rc=0;
    mesa_mac_t              multicast_addr = {{0x01,0x80,0xC2,0x00,0x00,0x30}};
    mesa_packet_rx_queue_t  rx_queue = PACKET_XTR_QU_OAM;
    mesa_oam_vop_conf_t     cfg;
    packet_rx_filter_t      filter;
    void                    *filter_id;
    mesa_vlan_conf_t        vlan_sconf;



    u32                     port_count = port_isid_port_count(VTSS_ISID_LOCAL);

    T_NG(TRACE_GRP_MODEL, "Enter  init %u", init);

    if (init == MEP_MODEL_INIT_INIT) {

        for (i=0; i<mep_caps.mesa_oam_voe_cnt; ++i) {
            voe_to_instance[i] = VTSS_APPL_MEP_INSTANCE_MAX;
        }
        vlan_port_conf.clear();
        for (i=0; i<client_vlan_source_isdx.size(); i++) {
            client_vlan_source_isdx[i] = MESA_ISDX_NONE;
        }
        /* Configure VOP */
        if ((rc = mesa_oam_vop_conf_get(NULL, &cfg)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_oam_vop_conf_get failed %s", error_txt(rc));
        }

        cfg.enable_all_voe = TRUE;
        memcpy(cfg.common_multicast_dmac.addr, multicast_addr.addr, sizeof(cfg.common_multicast_dmac.addr));
        cfg.ccm_lm_enable_rx_fcf_in_reserved_field = TRUE;
        cfg.lmr_proprietary_fcf_use = TRUE;
        cfg.sdlb_cpy_copy_idx = 0;

        cfg.pdu_type.generic[GENERIC_AIS].opcode = MESA_OAM_OPCODE_AIS;
        cfg.pdu_type.generic[GENERIC_AIS].check_dmac = TRUE;
        cfg.pdu_type.generic[GENERIC_AIS].extract.rx_queue = rx_queue;
        cfg.pdu_type.generic[GENERIC_LCK].opcode = MESA_OAM_OPCODE_LCK;
        cfg.pdu_type.generic[GENERIC_LCK].check_dmac = TRUE;
        cfg.pdu_type.generic[GENERIC_LCK].extract.rx_queue = rx_queue;
        cfg.pdu_type.generic[GENERIC_LAPS].opcode = MESA_OAM_OPCODE_LINEAR_APS;
        cfg.pdu_type.generic[GENERIC_LAPS].check_dmac = TRUE;
        cfg.pdu_type.generic[GENERIC_LAPS].extract.rx_queue = rx_queue;
        cfg.pdu_type.generic[GENERIC_RAPS].opcode = MESA_OAM_OPCODE_RING_APS;
        cfg.pdu_type.generic[GENERIC_RAPS].check_dmac = FALSE;
        cfg.pdu_type.generic[GENERIC_RAPS].extract.rx_queue = rx_queue;
        cfg.pdu_type.generic[GENERIC_LLM].opcode = MESA_OAM_OPCODE_LLM;
        cfg.pdu_type.generic[GENERIC_LLM].check_dmac = FALSE;
        cfg.pdu_type.generic[GENERIC_LLM].extract.rx_queue = rx_queue;
        cfg.pdu_type.generic[GENERIC_LLR].opcode = MESA_OAM_OPCODE_LLR;
        cfg.pdu_type.generic[GENERIC_LLR].check_dmac = FALSE;
        cfg.pdu_type.generic[GENERIC_LLR].extract.rx_queue = rx_queue;

        cfg.pdu_type.ccm.rx_queue = rx_queue;
        cfg.pdu_type.ccm_lm.rx_queue = rx_queue;
        cfg.pdu_type.lt.rx_queue = rx_queue;
        cfg.pdu_type.dmm.rx_queue = rx_queue;
        cfg.pdu_type.dmr.rx_queue = rx_queue;
        cfg.pdu_type.lbm.rx_queue = rx_queue;
        cfg.pdu_type.lbr.rx_queue = rx_queue - 1; // TST and LBR frames go into a lower prioritized queue in order not to disturb 1DM/DMR frames.
        cfg.pdu_type.err.rx_queue = rx_queue;
        cfg.pdu_type.other.rx_queue = rx_queue;
        cfg.pdu_type.lmm_slm.rx_queue = rx_queue;
        cfg.pdu_type.lmr_slr.rx_queue = rx_queue;
        cfg.pdu_type.tst.rx_queue = rx_queue;
        cfg.pdu_type.bfd_cc.rx_queue = rx_queue;
        cfg.pdu_type.bfd_cv.rx_queue = rx_queue;

        fast_period = (MEP_MODEL_FAST_HMO_RATE == MEP_MODEL_RATE_1S) ? 1000*1000 :
                    (MEP_MODEL_FAST_HMO_RATE == MEP_MODEL_RATE_10S) ? 10*1000*1000 : 1000*1000;
        cfg.auto_copy_on_ccm_err = 0;           /* Fast poll for CCM error */
        cfg.auto_copy_period[0] = 3*1000*1000;  /* Slow - Normal - 3s */
        cfg.auto_copy_period[1] = fast_period;  /* Fast - 1s */

        if ((rc = mesa_oam_vop_conf_set(NULL, &cfg)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_oam_vop_conf_set failed %s", error_txt(rc));
        }











    }

    if (init == MEP_MODEL_INIT_HOOK) {
        /* Hook up for receiving packets */
        CRIT_EXIT_ENTER();     /* Leave locked domain. Cannot be in locked domain during hook up callout */

        if ((rc = mesa_vlan_conf_get(NULL, &vlan_sconf)) != VTSS_RC_OK) { /* Get the custom S-Port EtherType */
            T_EG(TRACE_GRP_MODEL, "mesa_vlan_conf_get failed %s", error_txt(rc));
        }
        custom_ethertype[0] = (vlan_sconf.s_etype & 0x00FF);
        custom_ethertype[1] = (vlan_sconf.s_etype & 0xFF00) >> 8;

        packet_rx_filter_init(&filter);
        filter.modid = VTSS_MODULE_ID_MEP;
        filter.match = PACKET_RX_FILTER_MATCH_ETYPE;
        filter.cb    = rx_frame_callback;
        filter.prio  = PACKET_RX_FILTER_PRIO_NORMAL;
        filter.etype = 0x8902; // OAM ethertype
        filter.mtu = mep_caps.appl_mep_packet_rx_mtu;
        if ((rc = packet_rx_filter_register(&filter, &filter_id)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "packet_rx_filter_register failed %s", error_txt(rc));
            return;
        }
        filter.etype = 0x8100; // C-TAG
        if ((rc = packet_rx_filter_register(&filter, &filter_id)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "packet_rx_filter_register failed %s", error_txt(rc));
            return;
        }
        filter.etype = 0x88A8; // S-TAG
        if ((rc = packet_rx_filter_register(&filter, &filter_id)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "packet_rx_filter_register failed %s", error_txt(rc));
            return;
        }
        filter.etype = vlan_sconf.s_etype; // Custom S-TAG
        if ((rc = packet_rx_filter_register(&filter, &custom_filter_id)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "packet_rx_filter_register failed %s", error_txt(rc));
            return;
        }
        filter.etype = 0x8847; // MPLS
        if ((rc = packet_rx_filter_register(&filter, &filter_id)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "packet_rx_filter_register failed %s", error_txt(rc));
            return;
        }

        /* Hook up on port state change in order to generate dSSF */
        if ((rc = port_change_register(VTSS_MODULE_ID_MEP, port_change_callback)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "port_change_register failed %s", error_txt(rc));
        }
        if ((rc = port_shutdown_register(VTSS_MODULE_ID_MEP, port_shutdown_callback)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "port_shutdown_register failed %s", error_txt(rc));
        }

        // Hooking IRQ may fail
        if ((rc = vtss_interrupt_source_hook_set(VTSS_MODULE_ID_MEP, port_interrupt_callback, MEBA_EVENT_FLNK, INTERRUPT_PRIORITY_PROTECT)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "vtss_interrupt_source_hook_set failed %s", error_txt(rc));
        }
        if ((rc = vtss_interrupt_source_hook_set(VTSS_MODULE_ID_MEP, port_interrupt_callback, MEBA_EVENT_LOS, INTERRUPT_PRIORITY_PROTECT)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "vtss_interrupt_source_hook_set failed %s", error_txt(rc));
        }

        /* Hook up for VLAN port configuration changes */
        vlan_port_conf_change_register(VTSS_MODULE_ID_MEP, vlan_port_change_callback, FALSE);

        /* Hook up for VLAN port membership configuration changes */
        vlan_membership_change_register(VTSS_MODULE_ID_MEP, vlan_membership_change_callback);

        /* Hook up for S custom etype change */
        vlan_s_custom_etype_change_register(VTSS_MODULE_ID_MEP, custom_etype_change_callback);













#if defined(VTSS_SW_OPTION_L2PROTO) && defined(VTSS_SW_OPTION_MSTP)
        /* Hook up for MSTP configuration changes */
        if ((rc = l2_stp_msti_state_change_register(stp_msti_state_change_callback)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "l2_stp_msti_state_change_register failed %s", error_txt(rc));
        }
#endif
        voe_event_mask = (mep_caps.mesa_oam_event_ccm_period          | mep_caps.mesa_oam_event_ccm_priority |
                          mep_caps.mesa_oam_event_ccm_zero_period     | mep_caps.mesa_oam_event_ccm_rx_rdi   |
                          mep_caps.mesa_oam_event_ccm_loc             | mep_caps.mesa_oam_event_ccm_mep_id   |
                          mep_caps.mesa_oam_event_ccm_meg_id          | mep_caps.mesa_oam_event_meg_level    |
                          mep_caps.mesa_oam_event_ccm_tlv_port_status | mep_caps.mesa_oam_event_ccm_tlv_if_status);









        /* Hook up for VOE events */
        if ((rc = vtss_interrupt_source_hook_set(VTSS_MODULE_ID_MEP, voe_interrupt_callback, MEBA_EVENT_VOE, INTERRUPT_PRIORITY_PROTECT)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "vtss_interrupt_source_hook_set failed %s", error_txt(rc));
        }

        T_NG(TRACE_GRP_MODEL, "Exit");
        return;
    }

    if (init == MEP_MODEL_INIT_DONE) {
        /* Configure operational port now that initialization is done */
        init_state = MEP_MODEL_INIT_DONE;
        for (i=0; i<port_count; ++i) {
            operational_port(i);
        }
    }

    init_state = init;

    T_NG(TRACE_GRP_MODEL, "Exit");
}

u32 mep_model_mce_id_free_get(void)
{
    return(MCE_ID_CALC_FREE);
}


/****************************************************************************/
/*  Call-in functions. Enter locked domain                                  */
/****************************************************************************/
void mep_model_sim_timer_call_back(struct vtss::Timer *timer)
{
    /* Simulated AFI TX time out */
    T_DG(TRACE_GRP_CALLBACK, "Enter");

    if (timer == NULL) {
        T_EG(TRACE_GRP_TIMER, "timer is NULL");
        return;
    }

    CRIT_ENTER_EXIT();

    sim_afi_tx_handler((MepModelInstance *)timer->user_data);
}

void mep_model_ccm_rx_timer_call_back(struct vtss::Timer *timer)
{
    MepModelInstance *data;
    mesa_oam_ccm_status_t ccm_status;

    /* CCM reception time out */
    T_DG(TRACE_GRP_CALLBACK, "Enter");

    if (timer == NULL) {
        T_EG(TRACE_GRP_TIMER, "timer is NULL");
        return;
    }

    CRIT_ENTER_EXIT();

    data = (MepModelInstance *)timer->user_data;

    /* Make sure that there is really a hw LOC */
    if ((mesa_oam_ccm_status_get(NULL, data->voe_idx, &ccm_status)) == VTSS_RC_OK) {
        if (!ccm_status.loc) {
            return; // False alarm, ignore this timeout
        }
    }

    data->ccm_longest_period = 0;
    data->ccm_rx = FALSE;
    send_ccm_events(data);  /* Send events out */
}

static void voe_interrupt_callback(meba_event_t  source_id,
                                   u32           instance_id)
{
    MepModelInstance             *data;
    mesa_oam_voe_event_mask_t   voe_event;
    u32                         voe, rc=0;
    u32                         voe_cnt = mep_caps.mesa_oam_voe_cnt;
    u32                         cnt = ((voe_cnt + 31) / 32);
    u32                         *voe_mask;

    T_DG(TRACE_GRP_EVENT, "Enter");
    T_DG(TRACE_GRP_CALLBACK, "Enter");

    CRIT_ENTER_EXIT();     /* Enter locked domain. This is call-in from outside locked domain */

    if ((rc = vtss_interrupt_source_hook_set(VTSS_MODULE_ID_MEP, voe_interrupt_callback, MEBA_EVENT_VOE, INTERRUPT_PRIORITY_PROTECT)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "vtss_interrupt_source_hook_set failed %s", error_txt(rc));
    }

    if (VTSS_CALLOC_CAST(voe_mask, cnt, sizeof(u32)) == NULL) {
        T_EG(TRACE_GRP_MODEL, "calloc failed");
        return;
    }
    if ((rc = mesa_oam_event_poll(NULL, cnt, voe_mask)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_event_poll failed %s", error_txt(rc));
        goto done;
    }
    //T_DG(TRACE_GRP_EVENT, "voe_mask %X-%X-%X-%X-%X-%X-%X", voe_mask[0], voe_mask[1], voe_mask[2], voe_mask[3], voe_mask[4], voe_mask[5], voe_mask[6]);

    for (voe = 0; voe < voe_cnt; ) {
        if (!(voe_mask[voe / 32])) {
            voe += 32;
            continue;
        }
        if (!(voe_mask[voe / 32] & (1 << (voe % 32)))) {  /* Check for new event on this VOE instance */
            voe++;
            continue;
        }
        if ((rc = mesa_oam_voe_event_poll(NULL, voe, &voe_event)) != VTSS_RC_OK) {    /* Poll the VOE */
            T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_event_poll failed %s", error_txt(rc));
            voe++;
            continue;
        }

        data = get_instance_voe(voe);     /* Get the instance */
        if (data == NULL) {
            T_DG(TRACE_GRP_MODEL, "Instance invalid");
            goto done;         /* Instance was not found */
        }
        if (data->oper_state != MEP_MODEL_OPER_UP) {
            T_DG(TRACE_GRP_MODEL, "Instance not operational");
            goto done;
        }
        T_DG(TRACE_GRP_EVENT, "Active event - call out  voe %u  voe_event %X", voe, voe_event);

        /* Set defects active if new state */









        {

        if (voe_event & mep_caps.mesa_oam_event_ccm_zero_period) {
                event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dInv);
            }
        if (voe_event & mep_caps.mesa_oam_event_meg_level) {
                event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dLevel);
            }
        if (voe_event & mep_caps.mesa_oam_event_ccm_meg_id) {
                event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dMeg);
            }
        if (voe_event & mep_caps.mesa_oam_event_ccm_mep_id) {
                event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dMep);
            }
        if (voe_event & mep_caps.mesa_oam_event_ccm_loc) {
                event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dLoc);
            }
        if (voe_event & mep_caps.mesa_oam_event_ccm_rx_rdi) {
                event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dRdi);
            }
        if (voe_event & mep_caps.mesa_oam_event_ccm_period) {
                event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dPeriod);
            }
        if (voe_event & mep_caps.mesa_oam_event_ccm_priority) {
                event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dPrio);
            }
        if (voe_event & mep_caps.mesa_oam_event_ccm_tlv_port_status) {
                event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_tlv_port);
            }
        if (voe_event & mep_caps.mesa_oam_event_ccm_tlv_if_status) {
                event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_tlv_if);
            }
        }

        voe++;
    }
done:
    T_NG(TRACE_GRP_MODEL, "Exit");
    VTSS_FREE(voe_mask);
}

static BOOL rx_frame_callback(void                         *contxt,
                              const                        u8
                              *const                       frame,
                              const mesa_packet_rx_info_t  *const rx_info)
{
    u32                 i, tl_idx, rx_port=0, i_vid=0, prio=0, fid=0, tag_cnt=0;




    mep_model_pdu_rx_t  pdu_rx;
    u8                  *pdu_start, level;
    MepModelInstance     *data = NULL;

    T_DG(TRACE_GRP_CALLBACK, "Enter  ISDX %u  port %u  ethertype %X%X  custom_ethertype %X%X", rx_info->vstax.isdx, rx_info->port_no, frame[12], frame[13], custom_ethertype[1], custom_ethertype[0]);
    T_DG(TRACE_GRP_RX_FRAME, "Enter  ISDX %u  port %u  ethertype %X%X  custom_ethertype %X%X", rx_info->vstax.isdx, rx_info->port_no, frame[12], frame[13], custom_ethertype[1], custom_ethertype[0]);

    CRIT_ENTER_EXIT();     /* Enter locked domain. This is call-in from outside locked domain */

    prio = rx_info->tag.pcp;   /* Default is that classified PCP is Prio */
    if (rx_info->stripped_tag.tpid != 0) { /* A tag has been removed - stripped tag PCP is Prio */
        prio = rx_info->stripped_tag.pcp;
        tag_cnt++;
    }
    tl_idx = 12;
    /* If there is a not stripped tag in the frame we only accept C and S tag. Also the Ether type must be 0x8902 or 0x8847: */
    if ((frame[tl_idx] == 0x81 && frame[tl_idx+1] == 0x00) || (frame[tl_idx] == 0x88 && frame[tl_idx+1] == 0xA8) || (frame[tl_idx] == custom_ethertype[1] && frame[tl_idx+1] == custom_ethertype[0])) {
        tag_cnt++;
        prio = (frame[tl_idx+2] & 0xE0) >> 5;
        tl_idx += 4;
        if ((frame[tl_idx] == 0x81 && frame[tl_idx+1] == 0x00) || (frame[tl_idx] == 0x88 && frame[tl_idx+1] == 0xA8)) {
            tag_cnt++;
            prio = (frame[tl_idx+2] & 0xE0) >> 5;
            tl_idx += 4;
        }
    }









































    if ((frame[tl_idx] != 0x89) || (frame[tl_idx+1] != 0x02)) {
        T_DG(TRACE_GRP_RX_FRAME, "Not OAM received  tl_idx %u", tl_idx);
        return(FALSE);
    }

    pdu_start = (u8 *)&frame[tl_idx+2];

    // Try to match PDU to instance using the collected RX ISDX.



    level = frame[tl_idx + 2] >> 5;

    data = get_instance_rx_isdx(rx_info->vstax.isdx, rx_info->port_no, level, &rx_port, &i_vid);     /* Get the instance + rx_port. In case of Up-MEP the rx_info->port_no is holding the residence port (Chip fault) */






    if (data == NULL) {
        // No luck - try to match using TX ISDX instead.
        // SLM PDUs are received on the TX ISDX as they are looped to form an SLR PDU
        data = get_instance_tx_isdx(rx_info->vstax.isdx, rx_info->port_no, pdu_start);
    }

    if (data == NULL) {
        T_DG(TRACE_GRP_RX_FRAME, "Instance invalid");
        return(TRUE);         /* Instance was not found */
    }
    if (data->oper_state != MEP_MODEL_OPER_UP) {
        T_DG(TRACE_GRP_RX_FRAME, "Instance not operational");
        return(FALSE);
    }
    fid = vlan_fid_get(i_vid);

    memset(&pdu_rx, 0, sizeof(pdu_rx));

    not_voe_supported_defect_calc(data, (u8 *)&frame[tl_idx+2], (u8 *)&frame[6]);

    if ((data->create.domain == MEP_MODEL_EVC_SUB) && (tag_cnt < 2)) {    /* Subscriber domain and only one tag detected meaning untagged Subscriber OAM */
        for (i=0; i<VTSS_PRIO_ARRAY_SIZE; ++i) {    /* Find the 'prio' in the .prio_cosid_map table. */
            if (data->prio_cosid_map[i] == rx_info->cosid) {
                prio = i;
                break;
            }
        }
    }

    pdu_rx.data = pdu_start;
    pdu_rx.len = rx_info->length - (tl_idx+2);
    pdu_rx.frame_size = rx_info->length + ((rx_info->tag_type != MESA_TAG_TYPE_UNTAGGED) ? 4 : 0) + 4; /* Frame size as received on port inclusive CRC */
    /* COS-ID is prio as this is what VOE is using. In subscriber domain the prio is checked in SW and is the PCP from inner TAG */
    pdu_rx.prio = (data->create.domain == MEP_MODEL_EVC_SUB) || (data->create.domain == MEP_MODEL_PORT) ? prio : rx_info->cosid;
    pdu_rx.dmac  = (u8 *)&frame[0];
    pdu_rx.smac  = (u8 *)&frame[6];
    pdu_rx.port = rx_port;     /* can not trust rx_info->port_no for UP MEP/MIP... */
    pdu_rx.vid  = (fid != 0) ? fid : i_vid;       /* can not trust rx_info->tag.vid for UP MEP/MIP... */
    pdu_rx.alt_vid = (fid != 0) ? 0 : (i_vid == data->i_vid) ? data->leaf_i_vid : data->i_vid;   /* The alternative VID to be used for LT */




    T_DG(TRACE_GRP_RX_FRAME, "Instance: %u  Frame: port %u  vid %u  isdx %u  tagged %u  cos %u  qos %u  pcp %u  dei %u  len %u  smac %X-%X  level %u  opcode %u  period %u,  rdi %u  mep_id %u",
         data->instance,
         rx_info->port_no, rx_info->tag.vid, rx_info->vstax.isdx, (rx_info->tag_type != MESA_TAG_TYPE_UNTAGGED) ? TRUE : FALSE, rx_info->cosid, rx_info->cos, rx_info->tag.pcp,
         rx_info->tag.dei, rx_info->length, frame[10], frame[11], level, frame[tl_idx+3], (frame[tl_idx+4]&0x07), (frame[tl_idx+4]>>7), frame[tl_idx+11]);
    T_NG(TRACE_GRP_RX_FRAME, "DMAC-SMAC %X-%X-%X-%X-%X-%X-%X-%X-%X-%X-%X-%X  frame_size %u  PDU PRIO %u  rx VID %u  PDU VID %u  ALT VID %u  FID %u",
        frame[0], frame[1], frame[2], frame[3], frame[4], frame[5], frame[6], frame[7], frame[8], frame[9], frame[10], frame[11], pdu_rx.frame_size, pdu_rx.prio, i_vid, pdu_rx.vid, pdu_rx.alt_vid, fid);

    mep_model_rx_cb(data->instance, &pdu_rx);

    T_NG(TRACE_GRP_RX_FRAME, "Exit");

    return(TRUE);
}

static void port_change_callback(mesa_port_no_t  port_no,
                                 port_info_t     *info)
{
    T_DG(TRACE_GRP_CALLBACK, "Enter  Port %u  Los %u", port_no, !info->link);

    CRIT_ENTER_EXIT();     /* Enter locked domain. This is call-in from outside locked domain */

    los_state_set(port_no, (info->link) ? FALSE : TRUE);
}

static void port_shutdown_callback(mesa_port_no_t port_no)
{
    T_DG(TRACE_GRP_CALLBACK, "Enter  port %u", port_no);

    CRIT_ENTER_EXIT();     /* Enter locked domain. This is call-in from outside locked domain */

    los_state_set(port_no, TRUE);
}

static void port_interrupt_callback(meba_event_t  source_id,
                                    u32           instance_id)
{
    mesa_rc rc;

    T_DG(TRACE_GRP_CALLBACK, "Enter  source_id %u  instance_id %u", source_id, instance_id);

    CRIT_ENTER_EXIT();     /* Enter locked domain. This is call-in from outside locked domain */

    if ((rc = vtss_interrupt_source_hook_set(VTSS_MODULE_ID_MEP, port_interrupt_callback, source_id, INTERRUPT_PRIORITY_PROTECT)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "vtss_interrupt_source_hook_set failed %s", error_txt(rc));
    }

    los_state_set(instance_id, TRUE);
}


#if defined(VTSS_SW_OPTION_L2PROTO) && defined(VTSS_SW_OPTION_MSTP)
static void stp_msti_state_change_callback(vtss_common_port_t l2port, uchar msti, vtss_common_stpstate_t new_state)
{
    vtss_isid_t     isid;
    mesa_port_no_t  iport;

    T_DG(TRACE_GRP_CALLBACK, "STP MSTI state change callback(l2port = %s, msti = %d, new_state = %d)", l2port2str(l2port), msti, new_state);

    // Convert l2port to isid/iport
    if (!l2port2port(l2port, &isid, &iport)) {
        T_D("Calling l2port2port() failed");
        return;
    }

    CRIT_ENTER_EXIT();     /* Enter locked domain. This is call-in from outside locked domain */
    if (new_state == VTSS_COMMON_STPSTATE_DISCARDING || new_state == VTSS_COMMON_STPSTATE_FORWARDING) {
        /* port_state_change_event checks the VLANs*/
        port_state_change_event(iport);
    }
}
#endif /* VTSS_SW_OPTION_L2PROTO && VTSS_SW_OPTION_MSTP */
















































static void vlan_membership_change_callback(vtss_isid_t                isid,
                                            mesa_vid_t                 vid,
                                            vlan_membership_change_t  *changes)
{
    u32                        rc=0;
    u32                        next = 0;
    mesa_packet_port_info_t    info;
    CapArray<mesa_packet_port_filter_t, MESA_CAP_PORT_CNT> filter;
    MepModelInstance           *idata;
    BOOL                       new_forwarding;

    T_DG(TRACE_GRP_CALLBACK, "Enter  vid %u  static_exists %u", vid, changes->static_vlan_exists);

    CRIT_ENTER_EXIT();     /* Enter locked domain. This is call-in from outside locked domain */

    if ((rc = evc_vlan_domain_config(MEP_MODEL_VLAN, vid, VTSS_APPL_MEP_INSTANCE_MAX)) != VTSS_RC_OK) {
        T_DG(TRACE_GRP_MODEL, "evc_vlan_domain_config failed  rc %s", error_txt(rc));
    }
    if (!client_flow_update(vid, MEP_MODEL_VLAN)) {
        T_DG(TRACE_GRP_MODEL, "client_flow_update failed");
    }

    /* Get filter information for this VID */
    if ((rc = mesa_packet_port_info_init(&info)) != VTSS_RC_OK) {
        T_E("mesa_packet_port_info_init failed  rc %s", error_txt(rc));
        return;
    }
    info.vid = vid;
    if ((rc = mesa_packet_port_filter_get(NULL, &info, filter.size(), filter.data())) != VTSS_RC_OK) {  /* Get the filter info for this VID */
        T_E("mesa_packet_port_filter_get failed  vid %u  rc %s", vid, error_txt(rc));
        return;
    }

    /* Update the forwarding status */
    while ((idata = get_instance_created(&next)) != NULL) {
        if (((idata->create.domain != MEP_MODEL_EVC) && (idata->create.domain != MEP_MODEL_VLAN)) || (idata->create.direction == MEP_MODEL_UP)) {
            continue;
        }
        if (idata->i_vid != vid) {
            continue;
        }
        new_forwarding = (filter[idata->config.port].filter != MESA_PACKET_FILTER_DISCARD) ? TRUE : FALSE;  /* Forwarding is enabled if not discarding */
        T_DG(TRACE_GRP_MODEL, "filter %u  new_forwarding %u  idata->forwarding %u", filter[idata->config.port].filter, new_forwarding, idata->forwarding);
        if (idata->forwarding != new_forwarding) {  /* Forwarding state changed */
            idata->forwarding = new_forwarding;
            if (idata->oper_state == MEP_MODEL_OPER_UP) {
                event_cb(idata,  MEP_MODEL_EVENT_MEP_STATUS_forwarding);
            }
        }
    }
}

static void vlan_port_change_callback(vtss_isid_t                                isid,
                                      mesa_port_no_t                             port_no,
                                      const vtss_appl_vlan_port_detailed_conf_t  *new_conf)
{
    u32              rc=0, next=0;
    MepModelInstance  *data;
    BOOL             check_vlan_up_mep = FALSE;

    T_DG(TRACE_GRP_CALLBACK, "Enter  port_no %u  new port_type %u  old port_type %u", port_no, new_conf->port_type, vlan_port_conf[port_no].port_type);

    CRIT_ENTER_EXIT();     /* Enter locked domain. This is call-in from outside locked domain */

    if (vlan_port_conf[port_no].port_type != new_conf->port_type) { /* Port type has changed */
        /* Check for aware to unaware or unaware to aware */
        check_vlan_up_mep = ((vlan_port_conf[port_no].port_type == VTSS_APPL_VLAN_PORT_TYPE_UNAWARE) != (new_conf->port_type == VTSS_APPL_VLAN_PORT_TYPE_UNAWARE)) ? TRUE : FALSE;
        vlan_port_conf[port_no].port_type = new_conf->port_type;

        data = get_instance_domain_flow(MEP_MODEL_PORT, port_no, &next); /* Get the Port domain MEP on this port */
        if ((data != NULL) && (data->oper_state == MEP_MODEL_OPER_UP) && data->config.vid)  {    /* Port domain MEP with a tag */
            afi_restart(data);
        }
    }

    if (vlan_port_conf[port_no].pvid != new_conf->pvid) { /* Port VID has changed */
        /* check for change in PVID on unaware port */
        check_vlan_up_mep = check_vlan_up_mep || ((vlan_port_conf[port_no].port_type == VTSS_APPL_VLAN_PORT_TYPE_UNAWARE) ? TRUE : FALSE);
        vlan_port_conf[port_no].pvid = new_conf->pvid;
    }

    if (check_vlan_up_mep) { /* If any VLAN Up-MEP on this port VLAN domain must be reconfigured */
        next = 0;
        while ((data = get_instance_domain(MEP_MODEL_VLAN, &next)) != NULL) { /* Get all MEPs in the VLAN domain */
            if ((data->create.direction == MEP_MODEL_UP) && (data->config.port == port_no)) {   /* Up MEP found on this port */
                if ((rc = evc_vlan_domain_config(MEP_MODEL_VLAN, data->create.flow_num, VTSS_APPL_MEP_INSTANCE_MAX)) != VTSS_RC_OK) {
                    T_DG(TRACE_GRP_MODEL, "evc_vlan_domain_config failed");
                }
            }
        }
    }
}

static void custom_etype_change_callback(mesa_etype_t tpid)
{
    u32                next, rc = 0;
    MepModelInstance    *data;
    packet_rx_filter_t filter;

    T_DG(TRACE_GRP_CALLBACK, "Enter  enter  new tpid: 0x%04x", tpid);

    packet_rx_filter_init(&filter);
    filter.modid = VTSS_MODULE_ID_MEP;
    filter.match = PACKET_RX_FILTER_MATCH_ETYPE;
    filter.cb    = rx_frame_callback;
    filter.prio  = PACKET_RX_FILTER_PRIO_NORMAL;
    filter.etype = tpid; // Custom S-TAG
    if ((rc = packet_rx_filter_change(&filter, &custom_filter_id)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "packet_rx_filter_change failed %s", error_txt(rc));
        return;
    }

    CRIT_ENTER_EXIT();     /* Enter locked domain. This is call-in from outside locked domain */

    custom_ethertype[0] = (tpid & 0x00FF);
    custom_ethertype[1] = (tpid & 0xFF00) >> 8;

    next = 0;
    while ((data = get_instance_domain(MEP_MODEL_PORT, &next)) != NULL) { /* Get all Port domain MEPs */
        if ((data->oper_state == MEP_MODEL_OPER_UP) && (vlan_port_type_get(data->create.flow_num) == VTSS_APPL_VLAN_PORT_TYPE_S_CUSTOM) && data->config.vid)  {    /* Port domain MEP with a tag on a custom ethertype port */
            afi_restart(data);
        }
    }
}


/****************************************************************************/
/*  Call-out functions. Leave locked domain during call-out                 */
/****************************************************************************/
static void mac_get(u32 port,  u8 mac[VTSS_APPL_MEP_MAC_LENGTH])
{
    T_DG(TRACE_GRP_OUT, "Enter  port %u", port);

    CRIT_EXIT_ENTER();

    if (conf_mgmt_mac_addr_get(mac, port+1) < 0) {
        T_W("Error getting MAC  port %u\n", port);
    }
}

static void timer_start(vtss::Timer *timer)
{
    T_DG(TRACE_GRP_OUT, "Enter  timer %p", timer);

    if (!timer) { return; }

    CRIT_EXIT_ENTER();

    if (vtss_timer_start(timer) != VTSS_RC_OK) {
        T_E("vtss_timer_start() failed");
    }
}

static void timer_cancel(vtss::Timer *timer)
{
    T_DG(TRACE_GRP_OUT, "Enter  timer %p", timer);

    if (!timer) { return; }

    CRIT_EXIT_ENTER();

    if (vtss_timer_cancel(timer) != VTSS_RC_OK) {
        T_E("vtss_timer_cancel() failed");
    }
}

static u32 vlan_fid_get(u32 vid)
{
    u32         rc=0;
    mesa_vid_t  fid=0;

    T_DG(TRACE_GRP_OUT, "Enter  vid %u", vid);

    CRIT_EXIT_ENTER();

    if ((rc = vtss_appl_vlan_fid_get(vid, &fid)) != VTSS_RC_OK) {
        T_DG(TRACE_GRP_RX_FRAME, "vtss_appl_vlan_fid_get failed  vid %u  rc %s", vid, error_txt(rc));
        return(0);
    }

    T_NG(TRACE_GRP_OUT, "Exit");

    return(fid);
}

static BOOL vlan_flow_info_get(const u32 vid, mesa_port_list_t &nni)
{
    u32                    rc=0;
    vtss_appl_vlan_entry_t conf;
    mesa_port_no_t         port_no;
    BOOL                   port_found=FALSE;

    T_DG(TRACE_GRP_OUT, "Enter  vid %u", vid);

    CRIT_EXIT_ENTER();

    nni.clear_all();

    if ((rc = vtss_appl_vlan_get(VTSS_ISID_START, vid, &conf, FALSE, VTSS_APPL_VLAN_USER_STATIC)) != VTSS_RC_OK) {
        T_DG(TRACE_GRP_MODEL, "vtss_appl_vlan_get failed  VLAN %u  rc %s", vid, error_txt(rc));
        return(FALSE);
    }

    if (conf.vid == 0) { /* Check if this VID exists */
        T_DG(TRACE_GRP_MODEL, "VID does not exist  %u", vid);
        return(FALSE);
    }

    for (port_no = 0; port_no < mep_caps.mesa_port_cnt; port_no++) {
        nni[port_no] = (conf.ports[port_no] == 1) ? TRUE : FALSE;
        if (nni[port_no]) {
            port_found = TRUE;
        }
    }

    T_NG(TRACE_GRP_OUT, "Exit");

    return(port_found);
}

static BOOL evc_flow_info_get(u32     evc_inst,
                              mesa_port_list_t &nni,
                              mesa_port_list_t &uni,
                              u32     *vid,
                              u32     *i_vid,
                              BOOL    *e_tree,
                              mesa_port_list_t &leaf_uni,
                              u32     *leaf_vid,
                              u32     *leaf_i_vid)
{







    T_DG(TRACE_GRP_OUT, "Enter  evc_inst %u", evc_inst);

    CRIT_EXIT_ENTER();

    uni.clear_all();
    leaf_uni.clear_all();
    nni.clear_all();
    *vid = 0;
    *i_vid = 0;
    *leaf_vid = 0;
    *leaf_i_vid = 0;
    *e_tree = FALSE;










































    return(TRUE);
}

static BOOL evc_flow_info_port_get(u32     evc_inst,
                                   mesa_port_list_t &nni,
                                   mesa_port_list_t &uni)
{







    T_DG(TRACE_GRP_OUT, "Enter  evc_inst %u", evc_inst);

    CRIT_EXIT_ENTER();

    nni.clear_all();
    uni.clear_all();






























    T_NG(TRACE_GRP_MODEL, "nni %u-%u-%u-%u-%u-%u", nni.get(0), nni.get(1), nni.get(2), nni.get(3), nni.get(4), nni.get(5));
    T_NG(TRACE_GRP_MODEL, "uni %u-%u-%u-%u-%u-%u", uni.get(0), uni.get(1), uni.get(2), uni.get(3), uni.get(4), uni.get(5));

    return(TRUE);
}

static u32 port_cos_get(u32 port)
{
    mesa_rc                    rc = 0;
    vtss_appl_qos_port_conf_t  qos_port_conf;

    T_DG(TRACE_GRP_OUT, "Enter  port %u", port);

    CRIT_EXIT_ENTER();

    if ((rc = vtss_appl_qos_port_conf_get(VTSS_ISID_LOCAL, port, &qos_port_conf)) != VTSS_RC_OK) {
        T_E("vtss_appl_qos_port_conf_get failed  port %u  rc %s", port, error_txt(rc));
        return (0);
    }
    return (qos_port_conf.port.default_cos);
}

static u32 port_cos_id_get(u32 port)
{
    mesa_rc                    rc = 0;
    vtss_appl_qos_port_conf_t  qos_port_conf;

    T_DG(TRACE_GRP_OUT, "Enter  port %u", port);

    CRIT_EXIT_ENTER();

    if ((rc = vtss_appl_qos_port_conf_get(VTSS_ISID_LOCAL, port, &qos_port_conf)) != VTSS_RC_OK) {
        T_E("vtss_appl_qos_port_conf_get failed  port %u  rc %s", port, error_txt(rc));
        return (0);
    }
    return (qos_port_conf.port.default_cosid);
}

static void evc_ece_i_map_entry_get(u32 evc_inst,  u32 port,  u32 vid,  u32 pcp,  BOOL dei,  vtss_appl_qos_imap_entry_t *entry)
{
    memset(entry, 0, sizeof(*entry));























































    return;
}



























static vtss_appl_vlan_port_type_t vlan_port_type_get(u32  port)
{
    u32                         rc=0;
    vtss_appl_vlan_port_conf_t  vlan_conf;

    T_DG(TRACE_GRP_OUT, "Enter");

    CRIT_EXIT_ENTER();     /* Leave locked domain during call-out */

    if ((rc = vlan_mgmt_port_conf_get(VTSS_ISID_START, port, &vlan_conf, VTSS_APPL_VLAN_USER_ALL, TRUE)) != VTSS_RC_OK) {  /* Get the port type */
        T_EG(TRACE_GRP_MODEL, "mesa_vlan_conf_get failed %s", error_txt(rc));
        return(VTSS_APPL_VLAN_PORT_TYPE_C);
    }

    return(vlan_conf.hybrid.port_type);
}

static BOOL vlan_port_conf_get(u32  port,  vtss_appl_vlan_port_conf_t *conf)
{
    u32  rc=0;

    T_DG(TRACE_GRP_OUT, "Enter");

    CRIT_EXIT_ENTER();     /* Leave locked domain during call-out */

    if ((rc = vlan_mgmt_port_conf_get(VTSS_ISID_START, port, conf, VTSS_APPL_VLAN_USER_ALL, TRUE)) != VTSS_RC_OK) {  /* Get the port type */
        T_EG(TRACE_GRP_MODEL, "mesa_vlan_conf_get failed %s", error_txt(rc));
        return(FALSE);
    }

    return(TRUE);
}

static void operational_port(u32  port)
{
    u32              next;
    BOOL             found=FALSE;
    port_vol_conf_t  port_conf;
    mesa_rc          rc=0;
    MepModelInstance  *data;

    T_DG(TRACE_GRP_OUT, "Enter  port %u  init_state %u", port, init_state);

    if (init_state != MEP_MODEL_INIT_DONE) {    /* The port volatile functions cannot be called until after initialisation */
        return;
    }

    next = 0;
    while ((data = get_instance_created(&next)) != NULL) { /* Get all Up MEPs on this port */
        if (data->admin_state && (data->oper_state == MEP_MODEL_OPER_UP) && (data->config.port == port) &&      /* Instance is operational and recidence on this port */
            ((data->create.direction == MEP_MODEL_UP) || !data->client_config.empty()))  {    /* It is either an Up-MEP/MIP or client flows are added (potentially reverse client injection) */
            found = TRUE;
            break;
        }
    }

    T_DG(TRACE_GRP_MODEL, "port %u  found %u", port, found);

    CRIT_EXIT_ENTER();     /* Leave locked domain during call-out */

    if ((rc = port_vol_conf_get(PORT_USER_MEP, VTSS_ISID_START, port, &port_conf)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "port_vol_conf_get failed  %s port %d", error_txt(rc), port);
        return;
    }

    port_conf.oper_up = found;

    if ((rc = port_vol_conf_set_sync(PORT_USER_MEP, VTSS_ISID_START, port, &port_conf)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "port_vol_conf_set failed  %s port %d", error_txt(rc), port);
    }
}

static u32 ether_type(u32 port)
{
    u32                         rc=0;
    mesa_vlan_conf_t            vlan_sconf;
    vtss_appl_vlan_port_conf_t  vlan_conf;

    T_DG(TRACE_GRP_OUT, "Enter");

    CRIT_EXIT_ENTER();     /* Leave locked domain during call-out */

    vlan_sconf.s_etype = 0x88A8;
    vlan_conf.hybrid.port_type = VTSS_APPL_VLAN_PORT_TYPE_C;

    if ((rc = mesa_vlan_conf_get(NULL, &vlan_sconf)) != VTSS_RC_OK) { /* Get the custom S-Port EtherType */
        T_EG(TRACE_GRP_MODEL, "mesa_vlan_conf_get failed %s", error_txt(rc));
    }
    if ((rc = vlan_mgmt_port_conf_get(VTSS_ISID_START, port, &vlan_conf, VTSS_APPL_VLAN_USER_ALL, TRUE)) != VTSS_RC_OK) {  /* Get the port type */
        T_EG(TRACE_GRP_MODEL, "mesa_vlan_conf_get failed %s", error_txt(rc));
    }

    switch (vlan_conf.hybrid.port_type) {
        case VTSS_APPL_VLAN_PORT_TYPE_C:        return(0x8100);
        case VTSS_APPL_VLAN_PORT_TYPE_S:        return(0x88A8);
        case VTSS_APPL_VLAN_PORT_TYPE_S_CUSTOM: return(vlan_sconf.s_etype);
        case VTSS_APPL_VLAN_PORT_TYPE_UNAWARE:  return(0x8100);
    }
    return(0x8100);
}

static BOOL check_down_transmit_ok(u32 port)
{
    CRIT_EXIT_ENTER();     /* Leave locked domain during call-out */

    return(TRUE);
}





/****************************************************************************/
/*  Assorted internal functions.                                            */
/****************************************************************************/
static void propagate_ssf(MepModelInstance *data)
{
    BOOL            port_mep;
    mesa_port_list_t ports;
    u32             next;
    MepModelInstance *data_tmp = NULL;

    port_mep = (data->create.domain == MEP_MODEL_PORT) ? TRUE : FALSE;

    T_DG(TRACE_GRP_SSF, "Instance %u  %s", data->instance, (data->create.direction == MEP_MODEL_UP) ? "Up-MEP" : "Down-MEP");

    if (data->create.direction == MEP_MODEL_DOWN) { /* This is a Down-MEP - search for Down-MEP to update */
        T_DG(TRACE_GRP_SSF, "Search for Down-MEP to update");
        next = 0;
        while ((data_tmp = (port_mep ? get_instance_created(&next) : get_instance_domain_flow(data->create.domain, data->create.flow_num, &next))) != NULL) {
            if ((data_tmp->oper_state == MEP_MODEL_OPER_DOWN) || (data_tmp == data) || (data_tmp->create.direction == MEP_MODEL_UP)) {
                continue;
            }
            if (!port_mep && (data_tmp->config.level <= data->config.level)) {
                continue;
            }
            if (port_mep && !data_tmp->mep_lowest) {
                continue;
            }
            if ((data_tmp->config.port != data->config.port) && (data_tmp->p_port != data->config.port)) {
                continue;
            }

            data_tmp->dSsf = data->aSsf;
            T_DG(TRACE_GRP_SSF, "Updating  instance %u  ssf %u", data_tmp->instance, data_tmp->dSsf);
            event_cb(data_tmp, MEP_MODEL_EVENT_MEP_STATUS_dSsf);
            if (!port_mep) {
                return;    /* Done if this is not a Port Domain MEP */
            }
        }
    }

    /* This is either a Up-MEP or a Port Down-MEP or no flow Down-MEP was hit */
    /* Search for Up-MEP to update */
    T_DG(TRACE_GRP_SSF, "Search for Up-MEP to update");
    next = 0;
    while ((data_tmp = (port_mep ? get_instance_created(&next) : get_instance_domain_flow(data->create.domain, data->create.flow_num, &next))) != NULL) {
        if ((data_tmp->oper_state == MEP_MODEL_OPER_DOWN) || (data_tmp == data) || (data_tmp->create.direction == MEP_MODEL_DOWN)) {
            continue;
        }
        if ((data->create.direction == MEP_MODEL_UP) && (data->config.port != data_tmp->config.port)) {
            continue;
        }
        if (!port_mep && (data_tmp->config.level <= data->config.level)) {
            continue;
        }
        if ((data->create.direction == MEP_MODEL_DOWN) && !data_tmp->mep_lowest) {
            continue;
        }

        (void)get_up_mep_ports(data_tmp, ports);
        if ((data->create.direction == MEP_MODEL_UP) || ports[data->config.port]) {
            if (data->create.direction == MEP_MODEL_UP) {
                data_tmp->dSsf = data->aSsf;
                T_DG(TRACE_GRP_SSF, "Updating  instance %u  ssf %u", data_tmp->instance, data_tmp->dSsf);
                event_cb(data_tmp, MEP_MODEL_EVENT_MEP_STATUS_dSsf);
                return; /* Up-MEP updating an Up-MEP - Done */
            } else {
                data_tmp->dSsf = get_up_mep_ssf(data_tmp, VTSS_PORT_NO_NONE, &ports);
                T_DG(TRACE_GRP_SSF, "Updating  instance %u  ssf %u", data_tmp->instance, data_tmp->dSsf);
                event_cb(data_tmp, MEP_MODEL_EVENT_MEP_STATUS_dSsf);
            }
        }
    }
}

static void propagate_port_ssf(u32 port)
{
    MepModelInstance tmp_mep;

    T_DG(TRACE_GRP_SSF, "port %u  state %u", port, los_state.get(port));

    tmp_mep.instance = VTSS_APPL_MEP_INSTANCE_MAX;
    tmp_mep.create.domain = MEP_MODEL_PORT;
    tmp_mep.create.direction = MEP_MODEL_DOWN;
    tmp_mep.create.flow_num = port;
    tmp_mep.config.port = port;
    tmp_mep.aSsf = los_state[port];

    propagate_ssf(&tmp_mep);
}

static void send_ccm_events(MepModelInstance *data)
{
    event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dInv);
    event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dLevel);
    event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dMeg);
    event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dMep);
    event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dLoc);
    event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dRdi);
    event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dPeriod);
    event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dPrio);
    event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dLoop);
    event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dConfig);
}

static  mep_model_etree_t calc_e_tree_mode(u32 evc_inst, u32 port)
{
    u32   vid, i_vid, leaf_vid, leaf_i_vid;
    BOOL  e_tree, leaf=FALSE;
    mesa_port_list_t leaf_uni, nni, uni;

    if (!evc_flow_info_get(evc_inst, nni, uni, &vid, &i_vid, &e_tree, leaf_uni, &leaf_vid, &leaf_i_vid)) {
        return MEP_MODEL_ETREE_NONE;
    }
    leaf = e_tree && leaf_uni[port];     /* Calculate if this is a LEAF Up-MEP/MIP */
    return e_tree ? (leaf ? MEP_MODEL_ETREE_LEAF : MEP_MODEL_ETREE_ROOT) : MEP_MODEL_ETREE_NONE;
}

static void voe_rx_tx_block_calculate(MepModelInstance *data,  BOOL *rx_block,  BOOL *tx_block)
{
    MepModelInstance  *idata;
    BOOL             block=FALSE;
    u32              next;

    T_DG(TRACE_GRP_MODEL, "Enter  instance %u  domain %u  flow %u  port %u  block %u", data->instance, data->create.domain, data->create.flow_num, data->config.port, data->block);

    idata = get_instance_mep_highest_port(data->create.domain, data->create.flow_num, data->config.port);
    if (idata == data) {    /* This is the MEP on highest level on this port */
        block = data->block;
        if (!block) {    /* In case of not blocking there might be other on this port blocking */
            next = 0;
            while ((idata = get_instance_domain_flow(data->create.domain, data->create.flow_num, &next)) != NULL) { /* Get all MEPs in this flow */
                if (!idata->block || (idata->oper_state != MEP_MODEL_OPER_UP) || (idata->create.type != MEP_MODEL_MEP) || (idata->config.port != data->config.port)) {   /* Only configure operational instances */
                    continue;
                }
                block = TRUE;   /* Someone wants to block on this port and this instance is on highest level - block */
                break;
            }
        }
    }

    /* Do not activate RX data blocking in the Down VOE as this will also block reverse (masqueraded) injection in case this is hitting the VOE */
    *rx_block = (data->create.direction == MEP_MODEL_DOWN) ? FALSE : block;
    *tx_block = (data->create.domain == MEP_MODEL_PORT) ? FALSE : block;
}

static void instance_protection_update(u32 w_port, u32 p_port)
{
    MepModelInstance    *data;
    u32                flow_num, flow, next, rc=0;
    vtss::Vector<u32>  flows;
    BOOL               found;
    mep_model_domain_t domain;

    T_NG(TRACE_GRP_MODEL, "Enter  w_port %u", w_port);

    next = 0;
    while ((data = get_instance_created(&next)) != NULL) {  /* Run through all created instances */
        if ((data->create.direction == MEP_MODEL_DOWN) && (data->create.domain != MEP_MODEL_PORT) && (data->config.port == w_port)) {   /* If Down-MEP on this w_port the p_port must be updated */
            data->p_port = p_port;

            flow = ((data->create.domain == MEP_MODEL_EVC) ? 0x80000000 : 0) | data->create.flow_num;   /* Remember this flow must be reconfigured */
            found = FALSE;
            for (auto x : flows) {  /* Check if this flow is already in the 'flows' */
                if (x == flow ) {
                    found = TRUE;
                    break;
                }
            }
            if (!found) {   /* Remember the flow if not already in 'flows' */
                flows.push_back(flow);
            }
        }
    }

    for (auto x : flows) {  /* All flows in 'flows' must be reconfigured */
        domain = (x & 0x80000000) ? MEP_MODEL_EVC : MEP_MODEL_VLAN; /* Calculate the domain and flow number */
        flow_num = x & ~0x80000000;
        if (domain == MEP_MODEL_EVC) {  /* This EVC flow must be reconfigured */
            if ((rc = evc_vlan_domain_config(MEP_MODEL_EVC, flow_num, VTSS_APPL_MEP_INSTANCE_MAX)) != VTSS_RC_OK) {
                T_DG(TRACE_GRP_MODEL, "evc_vlan_domain_config failed");
            }
            if ((rc = evc_vlan_domain_config(MEP_MODEL_EVC_SUB, flow_num, VTSS_APPL_MEP_INSTANCE_MAX)) != VTSS_RC_OK) {
                T_DG(TRACE_GRP_MODEL, "evc_vlan_domain_config failed");
            }
        } else if (domain == MEP_MODEL_VLAN) {    /* This VLAN flow must be reconfigured */
            if ((rc = evc_vlan_domain_config(MEP_MODEL_VLAN, flow_num, VTSS_APPL_MEP_INSTANCE_MAX)) != VTSS_RC_OK) {
                T_DG(TRACE_GRP_MODEL, "evc_vlan_domain_config failed  rc %s", error_txt(rc));
            }
        }
    }

    /* Update the SSF with the port protection update */
    next = 0;
    data =  get_instance_domain_flow(MEP_MODEL_PORT, w_port, &next);
    if (data != NULL && data->oper_state == MEP_MODEL_OPER_UP) {
        mep_model_mep_assf_set(data->instance, data->aSsf);
    }
}

static BOOL get_up_mep_ssf(MepModelInstance *data, u32 ignore_port, mesa_port_list_t *nni_in)
{
    MepModelInstance   *data_tmp = NULL;
    mesa_port_list_t  nni_get;
    BOOL              up_mep_lnk = FALSE;
    u32               p;

    /* This function returns the link status that an Up-MEP get from NNI. */
    /* If just one NNI has link, TRUE is returned. In case a Down-MEP is on the NNI the aSSF status is returned */
    if (nni_in == NULL) {   /* Check if NNIs are already calculated */
        (void)get_up_mep_ports(data, nni_get);
        nni_in = &nni_get;
    }

    for (p = 0; p < mep_caps.mesa_port_cnt; ++p) {
        if (nni_in->get(p)) {
            if (p == data->config.port || p == ignore_port) {
                continue;
            }
            if (data->create.domain == MEP_MODEL_EVC) {
                data_tmp = get_instance_mep_highest(MEP_MODEL_EVC, data->create.flow_num, MEP_MODEL_DOWN, p);
            } else {
                data_tmp = get_instance_mep_highest(MEP_MODEL_VLAN, data->i_vid, MEP_MODEL_DOWN, p);
            }
            if (data_tmp == NULL) { /* If no MEP was found on this port in this flow - search for Port domain MEP */
                data_tmp = get_instance_mep_highest(MEP_MODEL_PORT, data->create.flow_num, MEP_MODEL_DOWN, p);
            }
            if (data_tmp) {
                up_mep_lnk = !data_tmp->aSsf || up_mep_lnk;
            } else {
                up_mep_lnk = !los_state[p] || up_mep_lnk;   /* No MEP was found on the port - use the port state itself */
            }
        }
    }
    return !up_mep_lnk;
}


static void  set_rdi_flag(MepModelInstance  *data,
                          BOOL             enable)
{
    tx_data_t                             *tx_data;
    vtss::Map<u32, tx_data_t>::iterator   i;

    T_DG(TRACE_GRP_MODEL, "enter instance %u  enable %u", data->instance, enable);

    for (i = data->tx_data.begin(); i != data->tx_data.end(); ++i) {    /* Run through all AFI */
        tx_data = &(*i).second;
        if (tx_data->pdu.data[1] == 1) {    /* Check if this is CCM in the frame. 'pdu.data' is a pointer to PDU in the frame */
            tx_data->pdu.data[2] &= ~0x80;
            tx_data->pdu.data[2] |= (enable) ? 0x80 : 0x00;

            afi_tx_data_restart(data, (*i).first, tx_data);
        }
    }
}

static void tx_data_key_free(MepModelInstance *data,  u32 tx_data_key)
{
    /* nothing to be done */
}

static u32 tx_data_key_alloc(MepModelInstance  *data)
{
    do {
        data->tx_data_key++;
    } while (data->tx_data_key == MEP_MODEL_PDU_TX_ID_NONE || data->tx_data.find(data->tx_data_key) != data->tx_data.end());
    return data->tx_data_key;
}

static void event_cb(MepModelInstance *data,  mep_model_event_type_t event)
{

    if (mep_model_event_cb == NULL) {
        T_DG(TRACE_GRP_MODEL, "Call back pointer invalid");
        return;
    }
    data->event_status.status[event] = TRUE;
    mep_model_event_cb(data->instance, event);
}

static void oper_up_ssf_update(MepModelInstance *data)
{
    MepModelInstance *data_next = NULL;

    /* Find the MEP on the next lower level */
    if (data->create.domain != MEP_MODEL_PORT) { /* A Port domain MEP does not have any lower level MEP */
        data_next = get_instance_mep_lowest(data->create.domain, data->create.flow_num, data->create.direction, data->config.port);
        if (data_next == data) {    /* Found this */
            data_next = NULL;
        }
        if ((data_next == NULL) && (data->create.direction == MEP_MODEL_DOWN)) { /* No MEP is found - If the MEP is Down the lower level MEP could be a Port MEP */
            data_next = get_instance_mep_highest(MEP_MODEL_PORT, data->config.port, MEP_MODEL_DOWN, data->config.port);
        }
    }

    if (data_next) {
        /* update the MEP with the lower level SSF */
        data->dSsf = data_next->aSsf;
    } else {
        /* no lower level MEP found */
        if (data->create.direction == MEP_MODEL_UP) {   /* Up-MEP must get a calculated status from all ports */
            data->dSsf = get_up_mep_ssf(data, VTSS_PORT_NO_NONE, NULL);
        } else {    /* Down-MEP just get the status of the port */
            data->dSsf = los_state[data->config.port];
        }
    }
    T_DG(TRACE_GRP_SSF, "instance %u  dSsf %u", data->instance, data->dSsf);
}

static void oper_state_set(MepModelInstance *data,  mep_model_mep_oper_state_t oper_state)
{
    BOOL state_change, old_dSsf;
    MepModelInstance *data_next;

    T_DG(TRACE_GRP_MODEL, "Enter - instance %u  new_state %u  old_state %u", data->instance, oper_state, data->oper_state);

    state_change = ((data->oper_state == MEP_MODEL_OPER_UP) != (oper_state == MEP_MODEL_OPER_UP)) ? TRUE : FALSE;
    data->oper_state = oper_state;
    event_cb(data,  MEP_MODEL_EVENT_MEP_STATUS_oper);

    T_DG(TRACE_GRP_MODEL, "Oper State changed  state_change %u", state_change);

    if (state_change) {    /* Oper state is changed from UP to DOWN or from DOWN to UP */
        T_DG(TRACE_GRP_MODEL, "Oper UP changed");
        if (data->create.direction == MEP_MODEL_UP) {   /* Configure forwarding to Up-MEP/MIP residence port even without link */
            operational_port(data->config.port);
        }
        if (oper_state != MEP_MODEL_OPER_UP) {              /* Oper state DOWN - defects must be cleared */
            data->dLoop = FALSE;
            data->dConfig = FALSE;
            data->dSsf = FALSE;
            data->ciSsf = FALSE;

            if (data->create.domain == MEP_MODEL_PORT) {
                T_NG(TRACE_GRP_SSF, "Port MEP deleted. propagate SSF from port");
                propagate_port_ssf(data->config.port);
            }
        } else {                                            /* Oper state is UP */
            if (data->create.direction == MEP_MODEL_UP) {   /* Up-MEP must have ciSsf depending on port state */
                data->ciSsf = los_state[data->config.port];
                event_cb(data,  MEP_MODEL_EVENT_MEP_STATUS_ciSsf);
            }
            if (data->create.direction == MEP_MODEL_DOWN) { /* Down-MEP must have forwarding depending on VLAN forwarding state on this port */
                data->forwarding = data->forwarding;        /* TBD forwarding should be VLAN forwarding state */
                event_cb(data,  MEP_MODEL_EVENT_MEP_STATUS_forwarding);
            }
        }
    }

    T_NG(TRACE_GRP_SSF, "instance %u  state_change %u  ssf %u", data->instance, state_change, data->dSsf);
    if (oper_state == MEP_MODEL_OPER_UP) {
        /* Calculate if this MEP is on lowest level on this port */
        data_next = get_instance_mep_lowest(data->create.domain, data->create.flow_num, data->create.direction, data->config.port);
        data->mep_lowest = ((data_next == NULL) || (data_next == data)) ? TRUE : FALSE;
        data->lowest_level = (data->mep_lowest) ? 0 : data_next->config.level + 1;

        /* Recalculation of dSsf */
        old_dSsf = data->dSsf;
        oper_up_ssf_update(data);
        if ((data->dSsf != old_dSsf) || state_change) {
            event_cb(data, MEP_MODEL_EVENT_MEP_STATUS_dSsf);
        }
    }

    T_DG(TRACE_GRP_MODEL, "Exit");
}

static BOOL update_dst_port(MepModelInstance *data, port_protection_t *prot, packet_tx_props_t *tx_props, u32 afi)
{
    if (!prot || !data || !tx_props) {  /* Check for valid pointers */
        return TRUE;
    }
    if (data->create.domain == MEP_MODEL_PORT) {  /* Port domain MEPs are not injecting behid the port protection */
        return TRUE;
    }
    /* Send on residence (working) port OR protected port OR both */
    if ((afi == 0) && (prot->one_plus_one || !prot->prot_active)) {
        tx_props->tx_info.dst_port_mask = VTSS_BIT64(data->config.port);    /* Injection should be done on working port */
    } else if ((afi == 1) && (prot->one_plus_one || prot->prot_active)) {
        tx_props->tx_info.dst_port_mask = VTSS_BIT64(data->p_port);         /* Injection should be done on protecting port */
    } else {
        return FALSE; // Do not start this AFI
    }
    return TRUE;
}

typedef enum {
    INJECT_SIMULATE,    /* Injection hit MEP of same direction on same port. Simulate injection into MEP */
    INJECT_BLOCK,       /* Injection is blocked by MEP of opposite direction on same port */
    INJECT_UP_MEP,      /* Injection hit Up-MEP on other port. Inject on RX ISDX */
    INJECT_FLOW,        /* Injection do not hit any local MEP. Inject into flow */
} inject_mode_t;

static MepModelInstance *mep_hit_injection_calc(mep_model_domain_t domain, u32 flow, u8 level, u32 instance, mep_model_direction_t direction, u32 port, inject_mode_t *inject_mode)
{
    u32             next=0, found_level=8;
    MepModelInstance *idata, *found_data=NULL;

    T_DG(TRACE_GRP_MODEL, "Enter - instance %u  domain %u  direction %u  port %u", instance, domain, direction, port);

    *inject_mode = INJECT_FLOW; /* Default is injection into flow */

    /* Check if 'domain' Up/Down-MEP is hit. Find the instance on the lowest possible level or one on own port */
    while ((idata = get_instance_domain_flow(domain, flow, &next)) != NULL) { /* Look for MEPs in this domain/flow */
        if ((idata->create.type == MEP_MODEL_MEP) && (idata->oper_state == MEP_MODEL_OPER_UP) && (idata->instance != instance)) { /* An operational instance that is not myself */
            if ((idata->create.direction == direction) && (idata->config.port == port) && (found_level > idata->config.level) && (level <= idata->config.level)) {
                /* Instance with same direction on same port as 'this' and on higher level is found. Search on, as instance on lower level might exist */
                T_DG(TRACE_GRP_MODEL, "INJECT_SIMULATE instance %u", idata->instance);
                found_data = idata;
                found_level = idata->config.level;
                *inject_mode = INJECT_SIMULATE; /* This means "simulated" injection */
            }
            if ((*inject_mode != INJECT_SIMULATE) &&
                (idata->create.direction != direction) && (idata->config.port == port) && (level <= idata->config.level)) {
                /* Instance with opposite direction on same port as 'this' and on higher level is found. */
                T_DG(TRACE_GRP_MODEL, "INJECT_BLOCK instance %u", idata->instance);
                *inject_mode = INJECT_BLOCK;    /* This means injection is "blocked" */
            }
            if ((*inject_mode != INJECT_SIMULATE) && (*inject_mode != INJECT_BLOCK) &&
                (direction == MEP_MODEL_DOWN) && (idata->create.direction == MEP_MODEL_UP) && (idata->config.port != port) && (found_level > idata->config.level) && (level <= idata->config.level)) {
                /* Down-MEP is hitting Up-MEP on other port on higher level. Search on, as instance on lower level might exist */
                T_DG(TRACE_GRP_MODEL, "INJECT_UP_MEP instance %u", idata->instance);
                found_data = idata;
                found_level = idata->config.level;
                *inject_mode = INJECT_UP_MEP;   /* This means injection to Up-MEP */
            }
        }
    }
    return found_data;
}

static BOOL client_flow_isdx_calc(MepModelInstance          *data,
                                  const mep_model_pdu_tx_t  *const pdu,
                                  mesa_isdx_t               *tx_isdx,
                                  inject_ctrl_data_t        *inject_ctrl)
{
    MepModelInstance    *inst;
    inject_mode_t       inject_mode;

    inject_ctrl->sim_inject = FALSE;

    T_DG(TRACE_GRP_MODEL, "Enter  instance %u  reverse %u  domain %u", data->instance, pdu->reverse_inj, pdu->client_flow.domain);

    if (pdu->reverse_inj) {   /* Reverse injection in client flow. Check for hitting any local MEPs */
        if (pdu->client_flow.domain == MEP_MODEL_EVC || pdu->client_flow.domain == MEP_MODEL_VLAN) {  /* This is for both EVC and VLAN domain */
            /* Check if local MEP is hit and get the injection mode */
            inst = mep_hit_injection_calc(pdu->client_flow.domain, pdu->client_flow.flow, (pdu->data[0] >> 5), data->instance, data->create.direction, data->config.port, &inject_mode);
            T_DG(TRACE_GRP_MODEL, "inject_mode %u  instance %u", inject_mode, inst->instance);
            switch (inject_mode) {
                case INJECT_SIMULATE: {     /* Injection is received by other MEP on same port. Simulate RX */
                    inject_ctrl->sim_inject = TRUE;
                    inject_ctrl->target_instance = inst->instance;
                    return TRUE;
                }
                case INJECT_BLOCK: {   /* Injection is blocked by other MEP */
                    return FALSE;   /* Return FALSE here to indicate that frame should not be injected */
                }
                case INJECT_UP_MEP: {   /* Injection is received by Up-MEP on other port */
                    /* Transmit using the ISDX associated with the up MEP(s): */
                    /* Find the rx_isdx for Up MEP 'inst' where rx_port == data->config.port */
                    if (pdu->client_flow.domain == MEP_MODEL_EVC) {    /* In EVC domain an Up-MEP my have two ISDX - one for Root and one for Leaf. Find the one that mappes for Root */













                    } else {    /* In the VLAN domain there is only one ISDX for the VLAN internal VID */
                        for (const auto &e : inst->rx_isdx) {
                            if ((e.isdx != VTSS_ISDX_NONE) && (e.rx_port == data->config.port)) {
                                *tx_isdx = e.isdx;
                                break;
                            }
                        }
                    }
                    break;
                }
                case INJECT_FLOW: {   /* Injection is not hitting other MEP. Injection into flow */
                    if ((pdu->client_flow.domain == MEP_MODEL_EVC) || (pdu->client_flow.domain == MEP_MODEL_VLAN)) {   /* Reverse injection into EVC or VLAN. The client ISDX must be used to map into the EVC or VSI */
                        for (const auto &e : data->client_config) {
                            if ((e.config.domain == pdu->client_flow.domain) && (e.config.flow == pdu->client_flow.flow)) {
                                *tx_isdx = e.rev_isdx;
                                break;
                            }
                        }
                    }
                    break;
                }
            }
        } /* EVC or VLAN domain */
    } else { /* Client reverse injection end */
        /* Client source injection.  e.g. LCK will do this */
        /* Injection in source direction cannot hit other local MEPs as these will be on lower level */
        if (pdu->client_flow.domain == MEP_MODEL_VLAN) {
            *tx_isdx = client_vlan_source_isdx[data->config.port];
        }
        if (pdu->client_flow.domain == MEP_MODEL_EVC) {
            for (const auto &e : data->client_config) {
                if ((e.config.domain == pdu->client_flow.domain) && (e.config.flow == pdu->client_flow.flow)) {
                    *tx_isdx = e.isdx;
                    break;
                }
            }
        }
    }
    return TRUE;
}

static u32 evc_cos_id_get(u32 evc_inst,  u32 port,  u32 vid,  u32 pcp,  BOOL dei)
{
    vtss_appl_qos_imap_entry_t  imap_entry;
    u32                         cosid;

    cosid = port_cos_id_get(port); /* The port default cos id is used if imap does not give any cos id */

    evc_ece_i_map_entry_get(evc_inst, port, vid, pcp, dei, &imap_entry);
    if ((imap_entry.used) && (imap_entry.conf.action.cosid) &&
        ((imap_entry.conf.key == VTSS_APPL_QOS_INGRESS_MAP_KEY_PCP) || (imap_entry.conf.key == VTSS_APPL_QOS_INGRESS_MAP_KEY_PCP_DEI))) { /* Only "used" entry with COSID action make sense */
            cosid = imap_entry.pcp_dei[pcp][dei ? 1 : 0].cosid;
    }

    T_NG(TRACE_GRP_MODEL, "Exit  cosid %u", cosid);

    return(cosid);
}

static u32 evc_cos_get(u32 evc_inst,  u32 port,  u32 vid,  u32 pcp,  BOOL dei)
{
    vtss_appl_qos_imap_entry_t  imap_entry;
    u32                         cos;

    cos = port_cos_get(port); /* The port default cos is used if imap does not give any cos */

    evc_ece_i_map_entry_get(evc_inst, port, vid, pcp, dei, &imap_entry);
    if ((imap_entry.used) && (imap_entry.conf.action.cos) &&
        ((imap_entry.conf.key == VTSS_APPL_QOS_INGRESS_MAP_KEY_PCP) || (imap_entry.conf.key == VTSS_APPL_QOS_INGRESS_MAP_KEY_PCP_DEI))) { /* Only "used" entry with COSID action make sense */
            cos = imap_entry.pcp_dei[pcp][dei ? 1 : 0].cos;
    }

    T_NG(TRACE_GRP_MODEL, "Exit  cos %u", cos);

    return(cos);
}















































static mesa_packet_oam_type_t oam_type_calculate(u32 opcode)
{
    switch (opcode) {
        case MESA_OAM_OPCODE_NONE:         return MESA_PACKET_OAM_TYPE_NONE;
        case MESA_OAM_OPCODE_CCM:          return MESA_PACKET_OAM_TYPE_CCM;
        case MESA_OAM_OPCODE_LBM:          return MESA_PACKET_OAM_TYPE_LBM;
        case MESA_OAM_OPCODE_LBR:          return MESA_PACKET_OAM_TYPE_LBR;
        case MESA_OAM_OPCODE_LTM:          return MESA_PACKET_OAM_TYPE_LTM;
        case MESA_OAM_OPCODE_LTR:          return MESA_PACKET_OAM_TYPE_LTR;
        case MESA_OAM_OPCODE_LCK:          return MESA_PACKET_OAM_TYPE_LCK;
        case MESA_OAM_OPCODE_LMM:          return MESA_PACKET_OAM_TYPE_LMM;
        case MESA_OAM_OPCODE_LMR:          return MESA_PACKET_OAM_TYPE_LMR;
        case MESA_OAM_OPCODE_1DM:          return MESA_PACKET_OAM_TYPE_1DM;
        case MESA_OAM_OPCODE_DMM:          return MESA_PACKET_OAM_TYPE_DMM;
        case MESA_OAM_OPCODE_DMR:          return MESA_PACKET_OAM_TYPE_DMR;
        default:                           return MESA_PACKET_OAM_TYPE_CCM;
    }
}

static BOOL tx_props_create(MepModelInstance          *data,
                            const mep_model_pdu_tx_t  *const pdu,
                            packet_tx_props_t         *tx_props,
                            inject_ctrl_data_t        *inject_ctrl)
{
    u32             frame_length, pdu_off, opcode, cos_vid;
    u8              *frame = 0, ttl = 0;
    mesa_vid_t      vid = 0;
    vtss_isdx_t     rev_tx_isdx = data->tx_rev_isdx;    /* Only Subscriber domain MIB has a reverse injection ISDX */
    BOOL            untagged = FALSE, sat_dm, dm;

    T_DG(TRACE_GRP_MODEL, "Enter  instance %u  reverse_inj %u  client_inj %u  client domain %u  client flow %u  rev_tx_isdx %u", data->instance, pdu->reverse_inj, pdu->client_inj, pdu->client_flow.domain, pdu->client_flow.flow, rev_tx_isdx);

    /* Allocate frame buffer */
    frame_length = FRAME_HEADER_LENGTH + pdu->len;
    if (frame_length < MIN_FRAME_SIZE)
        frame_length = MIN_FRAME_SIZE;
    frame = packet_tx_alloc(frame_length);
    if (frame == NULL) {
        T_EG(TRACE_GRP_MODEL, "packet_tx_alloc returned invalid pointer");
        return FALSE;
    }
    memset(frame, 0, frame_length);
    opcode = pdu->data[1];
    dm = ((opcode == MESA_OAM_OPCODE_DMM) || (opcode == MESA_OAM_OPCODE_1DM)) ? TRUE : FALSE;
    sat_dm = (data->config.sat && dm) ? TRUE : FALSE;

    /* Calculate frame header */
    pdu_off = 0;
    memcpy(&frame[pdu_off], pdu->mac, 6);
    memcpy(&frame[pdu_off+6], data->config.mac, 6);
    pdu_off = 12;
    cos_vid = 0;
    if (!pdu->client_inj) {    /* This is NOT a client flow injection */

        if (MPLS_DOMAIN(data->create.domain)) {



        } else if (data->create.domain == MEP_MODEL_PORT) {
            if (data->config.vid != 0) {
                insert_c_s_tag(&frame[pdu_off], ether_type(data->config.port), data->config.vid, pdu->prio, pdu->dei);
                pdu_off += 4;
            }
        } else {
            if ((data->create.direction == MEP_MODEL_DOWN) && (data->create.type == MEP_MODEL_MEP) && dm) {
                /* Add extra dummy-tag (to get popped) in EVC/VLAN domain when injecting against NNI. This is to fix issue with DM PDU Timestamp update in DEV (Bugzilla#19219) */
                insert_c_s_tag(&frame[pdu_off], 0x8100, 0, 0, 0);
                pdu_off += 4;
            }
            if (data->create.domain == MEP_MODEL_EVC_SUB) {
                /* EVC subscriber MEP/MIP injection - add subscriber c-tag: */
                untagged = (data->config.vid == MEP_MODEL_ALL_VID) && (pdu->vid == MEP_MODEL_VID_UNTAGGED); /* If MEP is "ALL VID" and PDU VID is "UNTAGGED" then no tag is inserted */
                if (!untagged) {
                    insert_c_s_tag(&frame[pdu_off], ether_type(data->config.port), ((data->config.vid == MEP_MODEL_ALL_VID) ? pdu->vid : data->config.vid), pdu->prio, pdu->dei);
                    pdu_off += 4;
                }
                cos_vid = ((frame[(pdu_off-4)+2+0] & 0x0F) << 8) + frame[(pdu_off-4)+2+1];  /* In Subscriber domain COS mapping is based on mapping table pointed to by CLM action. */
                                                                                            /* In case injection is not done into the CLM then COS must be calculated based on VID in tag */
            }
        }
    } else if (!client_flow_isdx_calc(data, pdu, &rev_tx_isdx, inject_ctrl)) { /* Client flow injection. */
        /* No injection can or should be done */
        packet_tx_free(frame);
        return FALSE;
    }
    T_DG(TRACE_GRP_MODEL, "rev_tx_isdx %u", rev_tx_isdx);

    if (!MPLS_DOMAIN(data->create.domain)) {
        frame[pdu_off] = 0x89;
        frame[pdu_off+1] = 0x02;
        pdu_off+=2;
    }

    if (!pdu->client_inj && (data->create.domain == MEP_MODEL_VLAN)) {
        vid = data->i_vid;
    }
    if (pdu->client_inj && (pdu->client_flow.domain == MEP_MODEL_VLAN)) {
        vid = pdu->client_flow.flow;
    }
    if ((data->create.domain == MEP_MODEL_EVC_SUB) && (data->create.direction == MEP_MODEL_UP) && (data->create.type == MEP_MODEL_MEP) && !pdu->reverse_inj) {  /* When injecting into ANA_CLM the classified VID in the IFH might be used if the EVC CLM key is pointing at a range checker */
        vid = !untagged ? ((data->config.vid == MEP_MODEL_ALL_VID) ? pdu->vid : data->config.vid) :  vlan_port_conf[data->config.port].pvid;
    }
    if ((data->create.domain == MEP_MODEL_EVC_SUB) && (data->create.type == MEP_MODEL_MIP)) {
        if (((data->create.direction == MEP_MODEL_UP) && !pdu->reverse_inj) || ((data->create.direction == MEP_MODEL_DOWN) && pdu->reverse_inj)) {  /* Injection into the switch */
            vid = data->config.vid; /* The classified VID must be the Subscriber VID. The classified VSI is given when frame hits the injection rule (through IPT table) */
        } else {      /* Injection directly on the port */
            vid = data->i_vid;  /* The classified VID must be the EVC IVID as this will give correct classified EVC VSI. The VSI is only known in the API per VLAN (EVC IVID) */
        }
    }
    if (data->create.domain == MEP_MODEL_EVC) {     /* In the EVC domain the classified VID must indicate the MEG */
        vid = data->up_mep_highest_of_two ? VID_OAM_MEG1 : VID_OAM_MEG0;
    }
    if ((data->create.domain == MEP_MODEL_PORT) && pdu->reverse_inj && pdu->client_inj && (pdu->client_flow.domain == MEP_MODEL_EVC)) {     /* Port domain reverse injection into the EVC domain the classified VID must indicate the MEG */
        vid = VID_OAM_MEG0;
    }

    /* Insert PDU */
    memcpy(&frame[pdu_off], pdu->data, pdu->len);

    // Ensure that frame is at least min. size to avoid sending random frame data.
    size_t act_frm_len = pdu_off + pdu->len;
    if (act_frm_len < MIN_FRAME_SIZE) {
        act_frm_len = MIN_FRAME_SIZE;
    }

    /* Initialize transmission */
    packet_tx_props_init(tx_props);
    tx_props->packet_info.modid         = VTSS_MODULE_ID_MEP;
    tx_props->packet_info.frm           = frame;
    tx_props->packet_info.len           = act_frm_len;
    tx_props->tx_info.tag.vid           = vid;
    if ((data->create.domain == MEP_MODEL_VLAN) || (pdu->client_inj && (pdu->client_flow.domain == MEP_MODEL_VLAN))) {  /* This is injection into a VLAN - either the flow of this MEP or the client flow */
        tx_props->tx_info.tag.pcp       = pdu->prio;
        tx_props->tx_info.tag.dei       = pdu->dei;
    }
    tx_props->tx_info.cos               = (data->create.domain != MEP_MODEL_EVC_SUB) ? pdu->prio : (untagged) ? port_cos_get(data->config.port) : evc_cos_get(data->create.flow_num, data->config.port, cos_vid, pdu->prio, pdu->dei);  /* In case of injection in Subscriber domain, NOT hitting CLM, the IFH COS must be calculated EVC COS. This is only the case for SAt DM */
    tx_props->tx_info.cosid             = (data->create.domain != MEP_MODEL_EVC_SUB) ? pdu->prio : (untagged) ? port_cos_id_get(data->config.port) : evc_cos_id_get(data->create.flow_num, data->config.port, cos_vid, pdu->prio, pdu->dei);  /* In case of injection in Subscriber domain, NOT hitting CLM, the IFH COSID must be calculated EVC COSID. This is only the case for SAt DM  */
    tx_props->tx_info.dp                = pdu->dei;
    tx_props->tx_info.pdu_offset        = pdu_off;
    if (MPLS_DOMAIN(data->create.domain)) {
        tx_props->tx_info.oam_type      = pdu->mpls2 ? MESA_PACKET_OAM_TYPE_MPLS_TP_2 : MESA_PACKET_OAM_TYPE_MPLS_TP_1;
        tx_props->tx_info.oam_mpls_ttl  = ttl;
        tx_props->tx_info.oam_mpls_sbit = data->create.domain == MEP_MODEL_MPLS_LSP ? 0 : 1;        // LSPs can only do MIPs, so bottom swap label must have S=0.
    } else {
        tx_props->tx_info.oam_type        = oam_type_calculate(opcode);
    }

    if (pdu->reverse_inj) {   /* Reverse injection. This is either MEP reverse in client domain or MIP reverse in own domain */
        tx_props->tx_info.isdx            = rev_tx_isdx;
        tx_props->tx_info.pipeline_pt     = data->rev_inj_pipeline_pt;
        tx_props->tx_info.dst_port_mask   = (data->create.direction == MEP_MODEL_UP)   ? VTSS_BIT64(data->config.port) : 0;
        tx_props->tx_info.switch_frm      = (data->create.direction == MEP_MODEL_DOWN) ? TRUE : FALSE;
        tx_props->tx_info.masquerade_port = (data->create.direction == MEP_MODEL_DOWN) ? data->config.port : VTSS_PORT_NO_NONE;
    } else {  /* Source injection */
        tx_props->tx_info.isdx            = sat_dm ? data->tx_sat_dm_isdx : data->tx_isdx;
        if (!pdu->client_inj) {    /* This is NOT injection into a client flow - use this instance pipeline point */
            tx_props->tx_info.pipeline_pt = ((data->create.domain == MEP_MODEL_EVC_SUB) && (data->create.direction == MEP_MODEL_UP) && (data->create.type == MEP_MODEL_MEP) && !sat_dm) ?  MESA_PACKET_PIPELINE_PT_ANA_CLM : data->inj_pipeline_pt;  /* In case of Subscriber MEP injection(not SAT DM), pipeline point must be NONE so it looks like the frame got in on UNI. Currently to avoid SAT DM hit the policer it is injected on special ISDX not hitting CLM */
        } else {    /* Source injection into client flow */
            tx_props->tx_info.pipeline_pt = data->inj_pipeline_pt;  /* Use the pipeline point of the server MEP */
            if ((pdu->client_flow.domain == MEP_MODEL_VLAN) || (pdu->client_flow.domain == MEP_MODEL_EVC)) {  /* In the EVC or VLAN domain use the client ISDX given by client_flow_prep() */
                tx_props->tx_info.isdx = rev_tx_isdx;   /* This ISDX assure that no VOE (blocking) is hit and frame enters EVC without getting EVC counted */
            }
        }
        if (data->create.domain == MEP_MODEL_MPLS_LSP) { // always treat LSP MIPs as down
            tx_props->tx_info.dst_port_mask   = VTSS_BIT64(data->config.port);
            tx_props->tx_info.switch_frm      = FALSE;
            tx_props->tx_info.masquerade_port = VTSS_PORT_NO_NONE;
        } else {
            tx_props->tx_info.dst_port_mask   = (data->create.direction == MEP_MODEL_DOWN) ? VTSS_BIT64(data->config.port) : 0;
            tx_props->tx_info.switch_frm      = (data->create.direction == MEP_MODEL_UP)   ? TRUE : FALSE;
            tx_props->tx_info.masquerade_port = (data->create.direction == MEP_MODEL_UP)   ? data->config.port : VTSS_PORT_NO_NONE;
        }
    }

    data->prio_cosid_map[pdu->prio] = tx_props->tx_info.cosid;

    T_NG(TRACE_GRP_MODEL, "Exit");

    return TRUE;
}

static void los_state_set(const u32 port,  const BOOL state)
{
    MepModelInstance  *data = NULL;
    u32               next;

    T_NG(TRACE_GRP_MODEL, "Port %u  state %u", port, state);

    if (los_state[port] == state) {
        T_NG(TRACE_GRP_MODEL, "Unchanged state");
        return;
    }

    los_state[port] = state;

    next = 0;
    data = get_instance_domain_flow(MEP_MODEL_PORT, port, &next);
    if (data != NULL && data->oper_state == MEP_MODEL_OPER_UP) {
        /* Port MEP found for this port */
        T_DG(TRACE_GRP_SSF, "(los)Port %u  link:%u.  Port MEP found", port, !state);
        data->dSsf = state;
        event_cb(data, MEP_MODEL_EVENT_MEP_STATUS_dSsf);
    } else {
        /* No port MEP found for this port, propagate SSF from the port */
        T_DG(TRACE_GRP_SSF, "(los)Port %u  link:%u.  No Port MEP found", port, !state);
        propagate_port_ssf(port);
    }

    /*  Find any Up-MEP that have this port as an UNI port  */
    next = 0;
    while ((data = get_instance_up_port(port, &next)) != NULL) {
        if (data->oper_state == MEP_MODEL_OPER_UP) { /* Operational state is UP */
            data->ciSsf = state;
            event_cb(data, MEP_MODEL_EVENT_MEP_STATUS_ciSsf);
        }
    }
}

static void afi_tx_data_restart(MepModelInstance *data,  mep_model_pdu_tx_id_t tx_id,  tx_data_t *tx_data)
{
    u32                                 rc=0;
    u32                                 indx, afi_no;
    BOOL                                tx_ok;
    u8                                  *frame = 0;
    packet_tx_props_t                   tx_props;
    mep_model_pdu_tx_t                  *pdu;
    port_protection_t                   *prot = NULL;
    inject_ctrl_data_t                  inject_ctrl;

    T_DG(TRACE_GRP_TX_FRAME, "Enter");

    cancel_afi(tx_data, NULL);   /* Cancel any active AFI */

    pdu = &tx_data->pdu;

    /* Check if ok to transmit. In case of down injection it might be prohibited */
    afi_no = 1;
    if (((data->create.direction == MEP_MODEL_DOWN) && !pdu->reverse_inj) || ((data->create.direction == MEP_MODEL_UP) && pdu->reverse_inj)) {
        tx_ok = check_down_transmit_ok(data->config.port);
        if ((data->p_port != P_PORT_NONE) && ((prot = get_protection_entry(data->config.port)) != NULL)) {
            afi_no = 2;
        }
    } else {
        tx_ok = TRUE;

        if (pdu->reverse_inj && is_port_prot_standby(data)) {
            tx_ok = FALSE;
        }
    }

    if (!tx_props_create(data, pdu, &tx_props, &inject_ctrl)) {   /* Create tx_props */
        T_NG(TRACE_GRP_MODEL, "tx_props_create failed");
        return;
    }

    frame = tx_props.packet_info.frm;    /* For convenience - a frame pointer is in the tx_props */

    if (!pdu->client_inj || client_flow_oper_state(data->client_config, pdu->client_flow)) { /* Do not transmit into a client flow that is not operational */
        for(indx = 0; indx < afi_no; indx++) {
            if (!update_dst_port(data, prot, &tx_props, indx)) {
                continue;
            }

            if (!inject_ctrl.sim_inject) {
                // Transmit using normal AFI
                rc = afi_tx(data,  &tx_props,  pdu->rate,  pdu->rate_is_kbps,  pdu->line_rate, tx_ok, &tx_data->afi[indx], pdu->jitter);
            } else {
                // Transmit using simulated AFI
                rc = sim_afi_tx(data,  &tx_props,  pdu->rate,  pdu->rate_is_kbps,  pdu->line_rate, tx_ok, &tx_data->afi[indx], inject_ctrl.target_instance);
            }

            if (rc != VTSS_RC_OK) {
                packet_tx_free(frame);
                //tx_data_key_free(data, alloc_key);

                // TODO: remove entry from data->tx_data?

                T_WG(TRACE_GRP_MODEL, "afi_tx failed");
                return;
            }

            T_NG(TRACE_GRP_TX_FRAME, "AFI restarted  tx_id %u  afi_id %u  prio %u  dei %u", tx_id, tx_data->afi[indx].afi_id, pdu->prio, pdu->dei);
        }
    }

    packet_tx_free(frame);  /* Frame buffer can be freed right after AFI start */
}

static void not_voe_supported_defect_calc(MepModelInstance *data,  u8 *pdu,  u8 *smac)
{
    u32 mep, opcode, period;

    opcode = pdu[1];
    period = pdu[2] & 0x07;
    mep = (pdu[8]<<8) + pdu[9];

    if (opcode == 1) {
        /* CCM PDU */
        if (mep == data->config.mepid) {    /* Check if this CCM is from this instance MEP ID */
            if (!memcmp(smac, data->config.mac, MEP_MODEL_MAC_LENGTH)) {    /* Check if SMAC is this instance SMAC */
                if (!data->dLoop) { /* dLoop changed */
                    data->dLoop = TRUE;
                    event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dLoop);
                }
            } else {
                if (!data->dConfig) { /* dConfig changed */
                    data->dConfig = TRUE;
                    event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dConfig);
                }
            }
            if (pdu[2] < 4) {
                pdu[2] = 4; /* The period is the CCM error fast poll sampling period - 1 s. */
            }
        } else {
            if (data->dLoop) { /* dLoop changed */
                data->dLoop = FALSE;
                event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dLoop);
            }
            if (data->dConfig) { /* dConfig changed */
                data->dConfig = FALSE;
                event_cb(data, MEP_MODEL_EVENT_CCM_STATUS_dConfig);
            }
        }

        if (data->voe_ccm_enabled) { /* When CCM is enabled in the VOE the CCM status is used. In case no CCM is received all CCM defects (except LOC) must be cleared */
            /* Timer started to check if CCM is no longer received */
            data->ccm_longest_period = (period > data->ccm_longest_period) ? period : data->ccm_longest_period;  /* Calculate CCM longest period */
            data->ccm_rx_timer.set_period(vtss::milliseconds(pdu_period_to_timer[data->ccm_longest_period]));
            data->ccm_rx_timer.set_repeat(FALSE);
            data->ccm_rx_timer.callback = mep_model_ccm_rx_timer_call_back;
            data->ccm_rx_timer.modid = VTSS_MODULE_ID_MEP;
            data->ccm_rx_timer.user_data = data;
            timer_start(&data->ccm_rx_timer);

            if (!data->ccm_rx) { /* CCM is currently not received. This is the first received CCM after a time out. */
                /* This is required as the received frame might have same active defects as last received frame. */
                /* These defects has been cleared and no new interrupt is generated from VOE */
                data->ccm_rx = TRUE;
                send_ccm_events(data);
            }
        }
        data->ccm_rx = TRUE;
    } /* CCM PDU */
}

static void afi_restart(MepModelInstance *data)
{
    vtss::Map<u32, tx_data_t>::iterator i;

    T_DG(TRACE_GRP_TX_FRAME, "Enter %u  oper_state %u", data->instance, data->oper_state);

    if (data->oper_state != MEP_MODEL_OPER_UP) {
        return;
    }

    for (i = data->tx_data.begin(); i != data->tx_data.end(); ++i) {
        afi_tx_data_restart(data, (*i).first, &(*i).second);
    }
}

static BOOL urgent_tx_calculate(packet_tx_props_t *tx_props)
{
    u32 pdu_offset;
    u8  *frame;

    frame = tx_props->packet_info.frm;
    pdu_offset = tx_props->tx_info.pdu_offset;

    return ((frame[pdu_offset+1] == MESA_OAM_OPCODE_AIS) || (frame[pdu_offset+1] == MESA_OAM_OPCODE_LCK)) ? TRUE : FALSE;
}

static u32 afi_tx(MepModelInstance *data,  packet_tx_props_t *tx_props,  u64 rate,  BOOL rate_is_kbps,  BOOL line_rate,  BOOL tx_ok,  afi_data_t *afi, BOOL jitter)
{
    u32                                  rc = 0, indx;
    u32                                  retval = VTSS_APPL_MEP_RC_TX_ERROR;
    unsigned char                        *frm;
    afi_single_conf_t                    single_conf;
    afi_multi_conf_t                     multi_conf;
    afi_multi_status_t                   multi_status;
    tx_data_t                            *tx_data;
    vtss::Map<u32, tx_data_t>::iterator  i;

    if (afi == NULL)
        return VTSS_APPL_MEP_RC_INVALID_PARAMETER;

    T_DG(TRACE_GRP_MODEL, "Enter  instance %u", data->instance);

    if ((rc = afi_single_conf_init(&single_conf)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "afi_single_conf_init() failed %s", error_txt(rc));
        return(retval);
    }
    if ((rc = afi_multi_conf_init(&multi_conf)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "afi_multi_conf_init() failed %s", error_txt(rc));
        return(retval);
    }

    afi->afi_id = 0;

    if (tx_props->tx_info.pipeline_pt == MESA_PACKET_PIPELINE_PT_ANA_CLM) {   /* AFI injection start in PL=ANA_CLM must be without Masqueraded hit enabled in CLM */
        for (i = data->tx_data.begin(); i != data->tx_data.end(); ++i) {    /* Pause all active multi AFI injections while CLM hit is disabled */
            tx_data = &(*i).second;
            if (tx_data->pdu.rate_is_kbps) {    /* Only for multi AFI */
                for(indx = 0; indx < 2; indx++) {
                    if (tx_data->afi[indx].afi_id != AFI_ID_NONE) {
                        if ((rc = afi_multi_pause_resume(tx_data->afi[indx].afi_id, TRUE)) != VTSS_RC_OK) { /* Pause AFI */
                            T_EG(TRACE_GRP_MODEL, "afi_multi_pause_resume() failed %s", error_txt(rc));
                        }
                    }
                }
            }
        }
        if ((rc = mesa_mce_update_masq_hit_ena(NULL, data->sub_inj_mce_id, FALSE)) != VTSS_RC_OK) {  /* Disable hitting OAM injection CLM entry */
            T_EG(TRACE_GRP_MODEL, "mesa_mce_update_masq_hit_ena() failed %s", error_txt(rc));
        }
    }

    if (rate_is_kbps) { /* Check for kBPS or FPH rate */
        /* kBPS - AFI multi */
        rate *= 1000;   /* convert to bps */
        multi_conf.params.bps = (rate < AFI_MULTI_RATE_BPS_MIN) ? AFI_MULTI_RATE_BPS_MIN : (rate <= AFI_MULTI_RATE_BPS_MAX) ? rate : AFI_MULTI_RATE_BPS_MAX;
        multi_conf.params.line_rate = line_rate;
        multi_conf.params.seq_cnt = 0;  /* Run forever (until stopped) */
        multi_conf.tx_props[0] = *tx_props;
        if ((rc = afi_multi_alloc(&multi_conf, &(afi->afi_id))) != VTSS_RC_OK) {
            T_WG(TRACE_GRP_MODEL, "afi_multi_alloc() failed %s", error_txt(rc));
            if (rc == AFI_RC_OUT_OF_MULTI_ENTRIES)
                retval = VTSS_APPL_MEP_RC_OUT_OF_TX_RESOURCE;
            goto ret_point;
        }
        if (tx_props->tx_info.pipeline_pt == MESA_PACKET_PIPELINE_PT_ANA_CLM) {   /* AFI injection start in PL=ANA_CLM must only be send to hw (AFI) while injection hit disabled in CLM */
            if ((rc = afi_multi_frms_to_hw(afi->afi_id)) != VTSS_RC_OK) {
                T_EG(TRACE_GRP_MODEL, "afi_multi_frms_to_hw() failed %s", error_txt(rc));
                goto ret_point;
            }
        } else {
            if (tx_ok) {    /* Do not transmit if not OK */
                if ((rc = afi_multi_pause_resume(afi->afi_id, FALSE)) != VTSS_RC_OK) {
                    T_EG(TRACE_GRP_MODEL, "afi_multi_pause_resume() failed %s", error_txt(rc));
                    (void)afi_multi_free(afi->afi_id, NULL);
                    afi->afi_id = AFI_ID_NONE;
                    goto ret_point;
                }
                if ((rc = afi_multi_status_get(&afi->afi_id, &multi_status, FALSE)) != VTSS_RC_OK) {
                    T_EG(TRACE_GRP_MODEL, "afi_multi_status_get() failed %s", error_txt(rc));
                    goto ret_point;
                }
                afi->bps_act = multi_status.bps_act;
            }
        }
    } else {
        /* FPH - AFI single */
        u64 fph = rate;
        single_conf.params.fph = (fph <= AFI_SINGLE_RATE_FPH_MAX) ? fph : AFI_SINGLE_RATE_FPH_MAX;
        single_conf.params.jitter = jitter ? AFI_SINGLE_JITTER_75_TO_100_PERCENT : AFI_SINGLE_JITTER_NONE;
        single_conf.tx_props = *tx_props;
        single_conf.params.first_frame_urgent = urgent_tx_calculate(tx_props);
        if ((rc = afi_single_alloc(&single_conf, &afi->afi_id)) != VTSS_RC_OK) {
            T_WG(TRACE_GRP_MODEL, "afi_single_alloc() failed %s", error_txt(rc));
            if (rc == AFI_RC_OUT_OF_SINGLE_ENTRIES)
                retval = VTSS_APPL_MEP_RC_OUT_OF_TX_RESOURCE;
            goto ret_point;
        }
        if (tx_ok && (rc = afi_single_pause_resume(afi->afi_id, FALSE)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "afi_single_pause_resume() failed %s", error_txt(rc));
            (void)afi_single_free(afi->afi_id, NULL);
            afi->afi_id = AFI_ID_NONE;
            goto ret_point;
        }
        afi->bps_act = 0;
    }
    retval = VTSS_RC_OK;

    frm = tx_props->packet_info.frm;
    T_DG(TRACE_GRP_TX_FRAME, "instance %u  port_mask 0x%" PRIx64 "  isdx %u  pipeline %u  vid %u  cosid %u  cos %u  pcp %u  dp %u  len %zu  maskarade %u  switched %u  oam_type %u  pdu_off %u  frame %X-%X-%X-%X-%X-%X-%X-%X-%X-%X",
        data->instance, tx_props->tx_info.dst_port_mask, tx_props->tx_info.isdx, tx_props->tx_info.pipeline_pt, tx_props->tx_info.tag.vid, tx_props->tx_info.cosid, tx_props->tx_info.cos, tx_props->tx_info.tag.pcp,
        tx_props->tx_info.dp, tx_props->packet_info.len, tx_props->tx_info.masquerade_port, tx_props->tx_info.switch_frm, tx_props->tx_info.oam_type, tx_props->tx_info.pdu_offset,
        frm[12+0], frm[12+1], frm[12+2], frm[12+3], frm[12+4], frm[12+5], frm[12+6], frm[12+7], frm[12+8], frm[12+9]);
    T_NG(TRACE_GRP_TX_FRAME, "DMAC-SMAC %X-%X-%X-%X-%X-%X-%X-%X-%X-%X-%X-%X",
        frm[0], frm[1], frm[2], frm[3], frm[4], frm[5], frm[6], frm[7], frm[8], frm[9], frm[10], frm[11]);
    T_NG(TRACE_GRP_TX_FRAME, "rate " VPRI64u "  actual rate " VPRI64u "  line_rate %u  rate_is_kbps %u  tx_ok %u", rate, afi->bps_act, line_rate, rate_is_kbps, tx_ok);

    ret_point:

    if (tx_props->tx_info.pipeline_pt == MESA_PACKET_PIPELINE_PT_ANA_CLM) {
        if ((rc = mesa_mce_update_masq_hit_ena(NULL, data->sub_inj_mce_id, TRUE)) != VTSS_RC_OK) {  /* Restore Masqueraded hit enabled in CLM */
            T_EG(TRACE_GRP_MODEL, "vtss_mce_update_pl_ena() failed %s", error_txt(rc));
        }

        if (tx_ok) {    /* Do not transmit if not OK */
            for (i = data->tx_data.begin(); i != data->tx_data.end(); ++i) {    /* Resume all active multi AFI injections after CLM hit is enabled */
                tx_data = &(*i).second;
                if (tx_data->pdu.rate_is_kbps) {    /* Only for multi AFI */
                    for(indx = 0; indx < 2; indx++) {
                        if (tx_data->afi[indx].afi_id != AFI_ID_NONE) {
                            if ((rc = afi_multi_pause_resume(tx_data->afi[indx].afi_id, FALSE)) != VTSS_RC_OK) { /* Resume AFI */
                                T_EG(TRACE_GRP_MODEL, "afi_multi_pause_resume() failed %s", error_txt(rc));
                            }
                        }
                    }
                }
            }

            if (rate_is_kbps) { /* In case of multi AFI now is the time to start the AFI - frame is previously send to AFI */
                if ((rc = afi_multi_pause_resume(afi->afi_id, FALSE)) != VTSS_RC_OK) {
                    T_EG(TRACE_GRP_MODEL, "afi_multi_pause_resume() failed %s", error_txt(rc));
                    goto ret_point;
                }
                if ((rc = afi_multi_status_get(&afi->afi_id, &multi_status, FALSE)) != VTSS_RC_OK) {
                    T_EG(TRACE_GRP_MODEL, "afi_multi_status_get() failed %s", error_txt(rc));
                    goto ret_point;
                }
                afi->bps_act = multi_status.bps_act;
            }
        }
    }

    afi->paused = !tx_ok;
    afi->simulated = FALSE;

    return(retval);
}

/*
 * Handle a periodic PDU injection into "local" MEP to simulate AFI.
 */
static u32 sim_afi_tx_handler(MepModelInstance *data)
{
    tx_data_t                            *tx_data;
    vtss::Map<u32, tx_data_t>::iterator  i;

    if (data == NULL) {
        T_EG(TRACE_GRP_TIMER, "data is NULL");
        return VTSS_APPL_MEP_RC_INVALID_PARAMETER;
    }

    T_DG(TRACE_GRP_MODEL, "enter instance %u", data->instance);

    // Find any valid TX data structures
    for (i = data->tx_data.begin(); i != data->tx_data.end(); ++i) {
        tx_data = &(*i).second;

        for (int aidx = 0; aidx < MEP_TX_AFI_COUNT; aidx++) {
            afi_data_t *afi = &tx_data->afi[aidx];

            // locate active, simulated AFI which is not paused
            if (afi->afi_id == AFI_ID_NONE || !afi->simulated) {
                continue;
            }

            if (afi->paused) {
                T_DG(TRACE_GRP_TX_FRAME, "instance %u  Sim. AFI inject, target %u (PAUSED)", data->instance, afi->sim_tgt_inst);
                continue;
            }

            T_DG(TRACE_GRP_TX_FRAME, "instance %u  Sim. AFI inject, target %u", data->instance, afi->sim_tgt_inst);

            mep_model_pdu_rx_t pdu_rx;
            memset(&pdu_rx, 0, sizeof(pdu_rx));
            pdu_rx.data       = tx_data->pdu.data;
            pdu_rx.len        = tx_data->pdu.len;
            pdu_rx.frame_size = tx_data->pdu.len + 14;
            pdu_rx.prio       = tx_data->pdu.prio;
            pdu_rx.smac        = data->config.mac;
            pdu_rx.vid        = 0;
            pdu_rx.port       = data->config.port;

            // Inject PDU into target MEP instance
            mep_model_rx_cb(afi->sim_tgt_inst, &pdu_rx);
        }
    }

    return VTSS_RC_OK;
}

/*
 * Start periodic timer to simulate AFI for local PDU injection
 */
static u32 sim_afi_tx(MepModelInstance *data,  packet_tx_props_t *tx_props,  u64 rate,  BOOL rate_is_kbps,  BOOL line_rate,
        BOOL tx_ok,  afi_data_t *afi, u32 target_inst)
{
    if (afi == NULL)
        return VTSS_APPL_MEP_RC_INVALID_PARAMETER;

    afi->afi_id = 1;    // has to be different from AFI_ID_NONE, but the value is otherwise not important
    afi->bps_act = 0;
    afi->paused = !tx_ok;
    afi->simulated = TRUE;
    afi->sim_tgt_inst = target_inst;

    // Calculate TX timer period and start timer ('rate' unit is frames/hour)
    u64 period_ms = 1000 * 60 * 60 / rate;

    afi->sim_timer.set_period(vtss::milliseconds(period_ms));
    afi->sim_timer.set_repeat(TRUE);
    afi->sim_timer.callback = mep_model_sim_timer_call_back;
    afi->sim_timer.modid = VTSS_MODULE_ID_MEP;
    afi->sim_timer.user_data = data;

    T_DG(TRACE_GRP_TIMER, "instance %u, timer period %u msec, target MEP %u, TX-OK:%u",
        data->instance, afi->sim_timer.get_period().raw32(), target_inst, tx_ok);
    timer_start(&afi->sim_timer);

    return(VTSS_RC_OK);
}

static mep_model_mep_conf_t default_mep_conf()
{
    mep_model_mep_conf_t def;

    memset(&def, 0, sizeof(def));
    def.hw = TRUE;
    def.level = 7;
    def.mepid = 1;

    return(def);
}

static void cancel_sim_afi(afi_data_t *afi)
{
    if (afi == NULL)
        return;

    if (afi->sim_timer.get_period() != vtss::microseconds(0)) {
        timer_cancel(&afi->sim_timer);
        afi->sim_timer.set_period(vtss::microseconds(0));
    }

    afi->afi_id = 0;
    afi->simulated = FALSE;
    afi->bps_act = 0;
    afi->paused = FALSE;

    afi->sim_timer.callback = NULL;
    afi->sim_timer.set_repeat(FALSE);
    afi->sim_timer.user_data = NULL;
}

static void cancel_afi(tx_data_t *tx_data, u64 *active_time_ms)
{
    u32 rc=0;

    T_DG(TRACE_GRP_TX_FRAME, "Enter");

    for (int aidx = 0; aidx < MEP_TX_AFI_COUNT; aidx++) {
        if (tx_data->afi[aidx].afi_id != AFI_ID_NONE) { /* Stop any active AFI */

            if (tx_data->afi[aidx].simulated) {
                // Free simulated AFI
                cancel_sim_afi(&tx_data->afi[aidx]);

            } else {
                // Free real AFI
                if (tx_data->pdu.rate_is_kbps && ((rc = afi_multi_free(tx_data->afi[aidx].afi_id, active_time_ms)) != VTSS_RC_OK)) {  /* Cancel multi AFI */
                    T_EG(TRACE_GRP_MODEL, "afi_multi_free failed %s", error_txt(rc));
                }
                if (!tx_data->pdu.rate_is_kbps && ((rc = afi_single_free(tx_data->afi[aidx].afi_id, active_time_ms)) != VTSS_RC_OK)) {  /* Cancel single AFI */
                    T_EG(TRACE_GRP_MODEL, "afi_single_free failed %s", error_txt(rc));
                }
            }

            tx_data->afi[aidx].afi_id = AFI_ID_NONE;
        }
    }
}

static void free_afi_resources(MepModelInstance *data)
{
    tx_data_t                            *tx_data;
    vtss::Map<u32, tx_data_t>::iterator  i;

    T_DG(TRACE_GRP_MODEL, "enter instance %u", data->instance);

    for (i = data->tx_data.begin(); i != data->tx_data.end(); ++i) {
        tx_data = &(*i).second;

        cancel_afi(tx_data, NULL);
        if (!data->admin_state) {   /* Only free tx data key and frame if admin down (instance deleted) */
            if (tx_data->frame != NULL) {
                packet_tx_free(tx_data->frame);  /* Frame buffer must be freed */
                tx_data->frame = NULL;
            }
            tx_data_key_free(data, (*i).first);
        }
    }
    if (!data->admin_state) {   /* Only clear tx_data if admin down (instance deleted) */
        data->tx_data.clear();
    }

    T_DG(TRACE_GRP_MODEL, "Exit");
}


static void free_mce_resources(MepModelInstance *data)
{
    u32 rc=0;

    T_DG(TRACE_GRP_MODEL, "enter instance %u", data->instance);

    for (const auto &e : data->mce_ids) {
        T_NG(TRACE_GRP_MODEL, "MCE id to delete %X", e);
        if ((rc = mesa_mce_del(NULL, e)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_del failed instance %u mce_id %X %s", data->instance, e, error_txt(rc));
        }
    }
    data->mce_ids.clear();      /* 'clear' stored MCE ids for this instance */
    data->vlan_common_mce_id = MESA_MCE_ID_LAST;   /* The vlan_common_mce_id rule is contained in 'data->mce_ids' so at this point this rule is not created */

    if (data->rx_block_mce_id != MESA_MCE_ID_LAST) {
        if ((rc = mesa_mce_del(NULL, data->rx_block_mce_id)) != VTSS_RC_OK) {   /* The rx_block_mce_id rule is NOT pushed in 'data->mce_ids' so it must be deleted seperately */
            T_EG(TRACE_GRP_MODEL, "mesa_mce_del failed instance %u mce_id %X %s", data->instance, data->rx_block_mce_id, error_txt(rc));
        }
        data->rx_block_mce_id = MESA_MCE_ID_LAST;
    }
    if (data->tx_block_mce_id != MESA_MCE_ID_LAST) {
        if ((rc = mesa_mce_del(NULL, data->tx_block_mce_id)) != VTSS_RC_OK) {   /* The tx_block_mce_id rule is NOT pushed in 'data->mce_ids' so it must be deleted seperately */
            T_EG(TRACE_GRP_MODEL, "mesa_mce_del failed instance %u mce_id %X %s", data->instance, data->tx_block_mce_id, error_txt(rc));
        }
        data->tx_block_mce_id = MESA_MCE_ID_LAST;
    }

    if (data->tst_lbr_mce_id != MESA_MCE_ID_LAST) {
        if ((rc = mesa_mce_del(NULL, data->tst_lbr_mce_id)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_del failed instance %u mce_id %X %s", data->instance, data->tst_lbr_mce_id, error_txt(rc));
        }
        del_instance_rx_isdx(data, data->rx_tst_lbr_isdx);
        data->tst_lbr_mce_id = MESA_MCE_ID_LAST;
        data->rx_tst_lbr_isdx = VTSS_ISDX_NONE;
        data->tst_lbr_rx_mce_id = MESA_MCE_ID_LAST;
    }
}

static void free_client_flow_mce_resources(MepModelInstance *data)
{
    u32 rc=0;

    T_DG(TRACE_GRP_MODEL, "enter instance %u", data->instance);

    for (auto &e : data->client_config) {
        if ((e.mce_id != MESA_MCE_ID_LAST) && (rc = mesa_mce_del(NULL, e.mce_id)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_del failed instance %u mce_id %X %s", data->instance, e.mce_id, error_txt(rc));
        }
        e.mce_id = MESA_MCE_ID_LAST;

        if ((e.rev_mce_id != MESA_MCE_ID_LAST) && (rc = mesa_mce_del(NULL, e.rev_mce_id)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_del failed instance %u rev_mce_id %X %s", data->instance, e.rev_mce_id, error_txt(rc));
        }
        e.rev_mce_id = MESA_MCE_ID_LAST;
    }
    if (!data->admin_state) {   /* Only clear client_config if admin down (instance deleted) */
        data->client_config.clear();
    }
}

static void free_resources(MepModelInstance *data)
{
    BOOL change;
    u32  rc=0;

    T_DG(TRACE_GRP_MODEL, "enter instance %u  voe_idx %u", data->instance, data->voe_idx);

    if (mep_caps.mesa_evc_evc_cnt > 0) {
        if ((data->create.domain == MEP_MODEL_EVC) || (data->create.domain == MEP_MODEL_EVC_SUB)) {
            /* clear EVC configuration: */
            mesa_evc_oam_port_conf_t evc_conf;







                if ((rc = mesa_evc_oam_port_conf_get(NULL, data->create.flow_num, data->config.port, &evc_conf)) != VTSS_RC_OK) {
                    T_EG(TRACE_GRP_MODEL, "mesa_evc_oam_port_conf_get failed, rc %s", error_txt(rc));
                } else {
                    change = FALSE;
                    if (data->config.sat) {
                        evc_conf.sat = FALSE;
                        change = TRUE;
                    }
                    if (evc_conf.voe_idx == data->voe_idx) {
                        evc_conf.voe_idx = MESA_OAM_VOE_IDX_NONE;
                        change = TRUE;
                    }
                    if (change) {
                        if ((rc = mesa_evc_oam_port_conf_set(NULL, data->create.flow_num, data->config.port, &evc_conf)) != VTSS_RC_OK) {
                            T_EG(TRACE_GRP_MODEL, "mesa_evc_oam_port_conf_set failed, rc %s", error_txt(rc));
                        }
                    }
                }



        }
    }
    if (data->voe_idx != MESA_OAM_VOE_IDX_NONE) { /* Free the VOE */
        voe_to_instance[data->voe_idx] = VTSS_APPL_MEP_INSTANCE_MAX;
        if ((rc = mesa_oam_voe_free(NULL, data->voe_idx)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_free failed %s", error_txt(rc));
        }
        data->voe_idx = MESA_OAM_VOE_IDX_NONE;
        if (data->sat_cnt_idx != mep_caps.mesa_oam_sat_cosid_ctr_cnt) { /* SAT have COSID counterset allocated */
            if ((rc = mesa_oam_sat_cosid_counters_free(NULL, data->sat_cnt_idx)) != VTSS_RC_OK) {
                T_EG(TRACE_GRP_MODEL, "mesa_oam_sat_cosid_counters_free() failed %s", error_txt(rc));
            }
            data->sat_cnt_idx = mep_caps.mesa_oam_sat_cosid_ctr_cnt;
        }
    }
    if (data->mip_idx != MESA_OAM_MIP_IDX_NONE) { /* Free the MIP */
        if ((rc = mesa_oam_mip_free(NULL, ((data->create.direction == MEP_MODEL_UP) ? MESA_OAM_UPMIP : MESA_OAM_DOWNMIP), data->mip_idx)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_oam_mip_free failed %s", error_txt(rc));
        }
        data->mip_idx = MESA_OAM_MIP_IDX_NONE;
    }
    if (data->create.direction == MEP_MODEL_UP) {   /* Static MAC entry configuration */
        MepModelInstance    *idata;
        u32                 keep_mac = FALSE;
        u32                 next = 0;
        while ((idata = get_instance_domain_flow(data->create.domain, data->create.flow_num, &next)) != NULL) { /* Search for other Up instance in this flow on this port */
            if ((idata != data) && (idata->oper_state == MEP_MODEL_OPER_UP) && (idata->create.direction == MEP_MODEL_UP) && (idata->config.port == data->config.port)) {   /* Other Up MEP found on this port */
                keep_mac = TRUE;  /* Keep MAC entry if other Up instance found on this port */
            }
        }

        if (!keep_mac && ((rc = static_mac_entry(data, FALSE)) != VTSS_RC_OK)) {    /* MAC entry is deleted */
            T_DG(TRACE_GRP_MODEL, "static_mac_entry failed, rc=%s", error_txt(rc));
        }
    }

    free_mce_resources(data);               /* Free all MCE resources             */
    free_client_flow_mce_resources(data);   /* Free all client flow MCE resources */
    del_instance_rx_isdx(data);             /* 'delete' RX ISDX for this instance */
    free_afi_resources(data);               /* Free all AFI resources             */

    T_DG(TRACE_GRP_MODEL, "Exit");
}

static BOOL mip_config(MepModelInstance *data)
{
    mesa_rc rc;
    mesa_oam_mip_conf_t cfg;

    T_NG(TRACE_GRP_MODEL, "Enter %u", data->instance);

    if ((data->create.direction == MEP_MODEL_DOWN && data->mip_idx >= mep_caps.mesa_oam_down_mip_cnt) ||
        (data->create.direction == MEP_MODEL_UP   && data->mip_idx >= (mep_caps.mesa_oam_up_mip_cnt + 1))) { /* The MIP index can be one more than mep_caps.mesa_oam_up_mip_cnt indicates. This is because the first instance is not usable */
        T_DG(TRACE_GRP_MODEL, "No MIP allocated");
        return(FALSE);
    }

    if ((rc = mesa_oam_mip_conf_get(NULL, (data->create.direction == MEP_MODEL_DOWN) ? MESA_OAM_DOWNMIP : MESA_OAM_UPMIP, data->mip_idx, &cfg)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_mip_conf_get() failed %s", error_txt(rc));
        return(FALSE);
    }

    memcpy(cfg.unicast_mac.addr, data->config.mac, sizeof(cfg.unicast_mac.addr));
    cfg.location             = (data->create.domain == MEP_MODEL_EVC_SUB) ? MESA_OAM_MIP_LOCATION_OUTER : MESA_OAM_MIP_LOCATION_INNER;
    cfg.meg_level            = data->config.level;
    cfg.cpu_queue            = PACKET_XTR_QU_OAM;
    cfg.ccm_copy_to_cpu      = FALSE;                          /* TRUE if MIP is used for discovery */
    cfg.ccm_copy_next_to_cpu = cfg.ccm_copy_to_cpu;
    cfg.ccm_copy_next_id     = 0;
    /* mip is only hit via Rx rule that checks encap is ok, so no need to have vid check on mip: */
    cfg.vid                  = 0; /* data->config.vid; */
    cfg.vid_sel              = MESA_OAM_MIP_VID_SEL_DISABLED; /* (data->config.vid == 0) ? MESA_OAM_MIP_VID_SEL_DISABLED : MESA_OAM_MIP_VID_SEL_VID; */
    cfg.lbm_redir_to_cpu     = TRUE;           /**< TRUE => redirect LBM PDUs matching encapsulation, #unicast_mac and MEG level to CPU. */
    cfg.ltm_redir_to_cpu     = TRUE;           /**< TRUE => redirect LTM PDUs matching encapsulation, #unicast_mac and MEG level to CPU. */
    cfg.raps_action          = MESA_OAM_MIP_PDU_ACTION_NONE;                /**< Handling of Y.1731 OAM RAPS PDUs */

    if (data->config.ll_is_used) {
        cfg.generic_opcode       = MESA_OAM_OPCODE_LLM;                         /**< Handling of MEF 46 LLM PDUs */
        cfg.generic_action       = MESA_OAM_MIP_PDU_ACTION_REDIR_TO_CPU;
    } else {
        cfg.generic_opcode       = MESA_OAM_OPCODE_NONE;
        cfg.generic_action       = MESA_OAM_MIP_PDU_ACTION_NONE;
    }

    if ((rc = mesa_oam_mip_conf_set(NULL, (data->create.direction == MEP_MODEL_DOWN) ? MESA_OAM_DOWNMIP : MESA_OAM_UPMIP, data->mip_idx, &cfg)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_mip_conf_set() failed %s", error_txt(rc));
        return(FALSE);
    }

    /* Injection pipeline point: */
    if (cfg.location == MESA_OAM_MIP_LOCATION_OUTER) {
        if (data->create.direction == MEP_MODEL_DOWN) {
            /* Outer down MIP */
            data->inj_pipeline_pt     = MESA_PACKET_PIPELINE_PT_REW_OU_MIP;
            data->rev_inj_pipeline_pt = MESA_PACKET_PIPELINE_PT_ANA_OU_MIP;
        } else {
            /* Outer up MIP */
            data->inj_pipeline_pt     = MESA_PACKET_PIPELINE_PT_ANA_OU_MIP;
            data->rev_inj_pipeline_pt = MESA_PACKET_PIPELINE_PT_REW_OU_MIP;
        }
    } else { /* inner */
        if (data->create.direction == MEP_MODEL_DOWN) {
            /* Inner down MIP */
            data->inj_pipeline_pt     = MESA_PACKET_PIPELINE_PT_REW_IN_MIP;
            data->rev_inj_pipeline_pt = MESA_PACKET_PIPELINE_PT_ANA_IN_MIP;
        } else {
            /* Inner up MIP */
            data->inj_pipeline_pt     = MESA_PACKET_PIPELINE_PT_ANA_IN_MIP;
            data->rev_inj_pipeline_pt = MESA_PACKET_PIPELINE_PT_REW_IN_MIP;
        }
    }

    return(TRUE);
}

static mesa_oam_period_t voe_period_calc(mep_model_rate_t  rate)
{
    switch (rate) {
        case MEP_MODEL_RATE_INV:  return(MESA_OAM_PERIOD_INV);
        case MEP_MODEL_RATE_300S: return(MESA_OAM_PERIOD_3_3_MS);
        case MEP_MODEL_RATE_100S: return(MESA_OAM_PERIOD_10_MS);
        case MEP_MODEL_RATE_10S:  return(MESA_OAM_PERIOD_100_MS);
        case MEP_MODEL_RATE_1S:   return(MESA_OAM_PERIOD_1_SEC);
        case MEP_MODEL_RATE_6M:   return(MESA_OAM_PERIOD_10_SEC);
    }
    return(MESA_OAM_PERIOD_INV);
}























static BOOL voe_config(MepModelInstance *data, BOOL lb_start)
{
    u32                  rc=0, i, oam_count, svc_to_path_idx=0, next;
    BOOL                 enable, svc_to_path=FALSE, highest_up=FALSE, tx_block, rx_block;
    mesa_oam_voe_conf_t  cfg;
    mesa_oam_pdu_type_t  pdu_type = MESA_OAM_PDU_Y1731;
    MepModelInstance     *idata, *lowest_data;

    T_NG(TRACE_GRP_MODEL, "Enter %u", data->instance);

    if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt) {
        T_DG(TRACE_GRP_MODEL, "No VOE allocated");
        return(FALSE);
    }

    if ((rc = mesa_oam_voe_conf_get(NULL, data->voe_idx, &cfg)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_conf_get() failed %s", error_txt(rc));
        return(FALSE);
    }

    /* Calculate the injection pipeline point and path VOE pointers */
    switch (data->create.domain) {
    case MEP_MODEL_PORT:
        data->inj_pipeline_pt     = MESA_PACKET_PIPELINE_PT_REW_PORT_VOE;
        data->rev_inj_pipeline_pt = MESA_PACKET_PIPELINE_PT_ANA_OU_MIP; /* for AIS+LCK */
        break;
    case MEP_MODEL_EVC:
    case MEP_MODEL_VLAN:
        if (data->create.direction == MEP_MODEL_UP) {  /* Up MEP */
            data->inj_pipeline_pt     = MESA_PACKET_PIPELINE_PT_ANA_OU_VOE; /* Default is outer */
            data->rev_inj_pipeline_pt = MESA_PACKET_PIPELINE_PT_REW_OU_VOE; /* Default is outer */
            next = 0;
            while ((idata = get_instance_domain_flow(data->create.domain, data->create.flow_num, &next)) != NULL) { /* Get all MEPs in this flow */
                if ((idata->oper_state == MEP_MODEL_OPER_UP) && (idata->create.direction == MEP_MODEL_UP) && (idata->config.port == data->config.port)) {   /* Other Up MEP found on this port */
                    if (idata->config.level < data->config.level) {
                        /* Up VOE on lower level found - this VOE is outer and point to inner (path) VOE */
                        svc_to_path = TRUE;
                        svc_to_path_idx = idata->voe_idx;
                        data->inj_pipeline_pt     = MESA_PACKET_PIPELINE_PT_ANA_OU_VOE;
                        data->rev_inj_pipeline_pt = MESA_PACKET_PIPELINE_PT_REW_OU_VOE;
                    }
                    if (idata->config.level > data->config.level) {
                        /* Up VOE on higher level found - this VOE is inner */
                        data->inj_pipeline_pt     = MESA_PACKET_PIPELINE_PT_ANA_IN_VOE;
                        data->rev_inj_pipeline_pt = MESA_PACKET_PIPELINE_PT_REW_IN_VOE;
                    }
                }
            }
            if (data->create.domain == MEP_MODEL_EVC) { /* In EVC Domain. In case a Subscribe Up-MEP exist this EVC Up-MEP is inner */
                next = 0;
                while ((idata = get_instance_domain_flow(MEP_MODEL_EVC_SUB, data->create.flow_num, &next)) != NULL) { /* Get all MEPs in this flow */
                    if ((idata->oper_state == MEP_MODEL_OPER_UP) && (idata->create.direction == MEP_MODEL_UP) && (idata->config.port == data->config.port)) {   /* Other Up MEP found on this port */
                        data->inj_pipeline_pt     = MESA_PACKET_PIPELINE_PT_ANA_IN_VOE;
                        data->rev_inj_pipeline_pt = MESA_PACKET_PIPELINE_PT_REW_IN_VOE;
                    }
                }
            }
        } else {    /* Down MEP */
            data->inj_pipeline_pt     = MESA_PACKET_PIPELINE_PT_REW_IN_VOE; /* Default is inner */
            data->rev_inj_pipeline_pt = MESA_PACKET_PIPELINE_PT_ANA_IN_VOE; /* Default is inner */
            next = 0;
            while ((idata = get_instance_domain_flow(data->create.domain, data->create.flow_num, &next)) != NULL) { /* Get all MEPs in this flow */
                if ((idata->oper_state == MEP_MODEL_OPER_UP) && (idata->create.direction == MEP_MODEL_DOWN) && (idata->config.port == data->config.port)) {  /* Other Down MEP found on this port */
                    if (idata->config.level < data->config.level) {
                        /* DOWN VOE on lower level found - this VOE is inner and point to outer (path) VOE */
                        svc_to_path = TRUE;
                        svc_to_path_idx = idata->voe_idx;
                        data->inj_pipeline_pt     = MESA_PACKET_PIPELINE_PT_REW_IN_VOE;
                        data->rev_inj_pipeline_pt = MESA_PACKET_PIPELINE_PT_ANA_IN_VOE;
                    }
                    if (idata->config.level > data->config.level) {
                        /* DOWN VOE on higher level found - this VOE is outer */
                        data->inj_pipeline_pt     = MESA_PACKET_PIPELINE_PT_REW_OU_VOE;
                        data->rev_inj_pipeline_pt = MESA_PACKET_PIPELINE_PT_ANA_OU_VOE;
                    }
                }
            }
        }
        break;
    case MEP_MODEL_EVC_SUB:
        if (data->create.direction == MEP_MODEL_UP) {  /* Up MEP */
            data->inj_pipeline_pt = MESA_PACKET_PIPELINE_PT_ANA_OU_VOE;
            lowest_data = get_instance_mep_lowest(MEP_MODEL_EVC, data->create.flow_num, MEP_MODEL_UP, data->config.port);
            if (!data->config.sat && (lowest_data != NULL)) {   /* NOT SAT as LM counters are used for DM counting. EVC Up MEP found on this port. Pointing to lowest MEP means that service frame base LM will work for this instance. It will break on any higher level EVC Up-MEP */
                svc_to_path = TRUE;
                svc_to_path_idx = lowest_data->voe_idx;
            }
        } else {
            data->inj_pipeline_pt = MESA_PACKET_PIPELINE_PT_REW_IN_VOE;
        }
        break;
    case MEP_MODEL_MPLS_LINK:
    case MEP_MODEL_MPLS_TUNNEL:
    case MEP_MODEL_MPLS_PW:
        data->inj_pipeline_pt     = MESA_PACKET_PIPELINE_PT_REW_IN_VOE; /* Default is inner */
        data->rev_inj_pipeline_pt = MESA_PACKET_PIPELINE_PT_ANA_IN_VOE; /* Default is inner */
        pdu_type = MESA_OAM_PDU_G8113_1;
        break;
    case MEP_MODEL_MPLS_LSP:
        T_DG(TRACE_GRP_MODEL, "MEP_MODEL_MPLS_LSP VOE not supported");
        return FALSE;

    default:
        T_DG(TRACE_GRP_MODEL, "Unknown Domain");
    }

    cfg.voe_type = (data->create.domain == MEP_MODEL_PORT) ? MESA_OAM_VOE_PORT : MESA_OAM_VOE_SERVICE;
    memcpy(cfg.unicast_mac.addr, data->config.mac, sizeof(cfg.unicast_mac.addr));
    cfg.mep_type = (data->create.direction == MEP_MODEL_DOWN) ? MESA_OAM_DOWNMEP : MESA_OAM_UPMEP;

    data->voe_ccm_enabled = ((data->ccm_config.peer_mep != MEP_MODEL_PEER_NONE) && (voe_period_calc(data->ccm_config.cc_rate) != MESA_OAM_PERIOD_INV) && (data->create.domain != MEP_MODEL_EVC_SUB)) ? TRUE : FALSE;   /* Disabled if multiple peers or invalid period */
    cfg.proc.pdu_type = pdu_type;
    cfg.proc.meg_level = data->config.level;
    cfg.proc.dmac_check_type = MESA_OAM_DMAC_CHECK_BOTH;
    cfg.proc.ccm_check_only = TRUE;
    cfg.proc.copy_next_only = data->voe_ccm_enabled;
    cfg.proc.copy_on_ccm_err = TRUE;
    cfg.proc.copy_on_mel_too_low_err = TRUE;
    cfg.proc.copy_on_dmac_err = FALSE;
    cfg.proc.copy_on_mel_too_high_err = FALSE;
    cfg.proc.copy_on_version_err = FALSE;
    cfg.proc.copy_on_ccm_tlv_nonzero = FALSE;
    cfg.proc.copy_next_valid_ccm = TRUE;
    cfg.proc.copy_next_tst_or_lbr = TRUE;
    cfg.proc.copy_on_tx_block_err = FALSE;
    cfg.proc.copy_on_sat_seq_err = FALSE;
    cfg.proc.copy_on_sl_err = FALSE;
    // Enable HitMeOnce for SLR/1SL PDUs
    memset(cfg.proc.copy_next_sl_peer, (data->sl_config.enable_tx || data->sl_config.enable_rx) ? 0xFF : 0, sizeof(cfg.proc.copy_next_sl_peer));
    cfg.proc.auto_copy_next_valid_ccm = data->voe_ccm_enabled;
    cfg.proc.auto_copy_on_ccm_tlv_nonzero = FALSE;
    cfg.proc.auto_copy_on_ccm_err = data->voe_ccm_enabled;
    cfg.proc.auto_copy_next_tst_or_lbr = data->tst_config.enable;
    memset(cfg.proc.auto_copy_next_sl_peer, 0, sizeof(cfg.proc.auto_copy_next_sl_peer));
    cfg.proc.copy_on_g8113_1_lbm_err = FALSE;
    cfg.proc.copy_on_g8113_1_lbr_err = FALSE;

    if ((data->create.domain == MEP_MODEL_EVC) && (data->create.direction == MEP_MODEL_UP)) {
        highest_up = data->mep_highest;
    }
    cfg.svc_to_path = svc_to_path;
    cfg.svc_to_path_idx = svc_to_path_idx;
    cfg.loop_isdx = data->tx_isdx;
    cfg.loop_clear_dp = FALSE;
    cfg.mepid = data->config.mepid;
    cfg.block_mel_high = ((data->create.domain == MEP_MODEL_PORT) || highest_up) ? TRUE : FALSE;    /* Block MEL high if port domain or Up-Mep on highest level */
    /* Calculate the RX and TX block values */
    voe_rx_tx_block_calculate(data, &rx_block, &tx_block);
    cfg.block_data_rx = rx_block;
    cfg.block_data_tx = tx_block;
    cfg.sat_test = (data->create.domain == MEP_MODEL_EVC_SUB) ? TRUE : FALSE;

    memset(cfg.generic, 0, sizeof(cfg.generic));
    cfg.generic[GENERIC_AIS].enable = TRUE;           /* AIS */
    cfg.generic[GENERIC_AIS].copy_to_cpu = TRUE;
    cfg.generic[GENERIC_AIS].forward = FALSE;
    cfg.generic[GENERIC_AIS].count_as_selected = FALSE;
    cfg.generic[GENERIC_AIS].count_as_data = FALSE;
    cfg.generic[GENERIC_LCK].enable = TRUE;           /* LCK */
    cfg.generic[GENERIC_LCK].copy_to_cpu = TRUE;
    cfg.generic[GENERIC_LCK].forward = FALSE;
    cfg.generic[GENERIC_LCK].count_as_selected = FALSE;
    cfg.generic[GENERIC_LCK].count_as_data = FALSE;
    cfg.generic[GENERIC_LAPS].enable = TRUE;           /* LAPS */
    cfg.generic[GENERIC_LAPS].copy_to_cpu = TRUE;
    cfg.generic[GENERIC_LAPS].forward = FALSE;
    cfg.generic[GENERIC_LAPS].count_as_selected = FALSE;
    cfg.generic[GENERIC_LAPS].count_as_data = data->config.sat ? FALSE : TRUE;
    cfg.generic[GENERIC_RAPS].enable = TRUE;           /* RAPS */
    cfg.generic[GENERIC_RAPS].copy_to_cpu = TRUE;
    cfg.generic[GENERIC_RAPS].forward = data->raps_forwarding;
    cfg.generic[GENERIC_RAPS].count_as_selected = FALSE;
    cfg.generic[GENERIC_RAPS].count_as_data = data->config.sat ? FALSE : TRUE;
    cfg.generic[GENERIC_LLM].enable = TRUE;           /* LLM */
    cfg.generic[GENERIC_LLM].copy_to_cpu = TRUE;
    cfg.generic[GENERIC_LLM].forward = FALSE;
    cfg.generic[GENERIC_LLM].count_as_selected = FALSE;
    cfg.generic[GENERIC_LLM].count_as_data = FALSE;
    cfg.generic[GENERIC_LLR].enable = TRUE;           /* LLR */
    cfg.generic[GENERIC_LLR].copy_to_cpu = TRUE;
    cfg.generic[GENERIC_LLR].forward = FALSE;
    cfg.generic[GENERIC_LLR].count_as_selected = FALSE;
    cfg.generic[GENERIC_LLR].count_as_data = FALSE;

    cfg.unknown.enable = FALSE;
    cfg.unknown.copy_to_cpu = FALSE;
    cfg.unknown.count_as_selected = FALSE;
    cfg.unknown.count_as_data = FALSE;

    /* Configure how to LM count per COS */
    cfg.count.cosid_src = MESA_OAM_COSID_SRC_COSID;
    for (i=0; i<8; ++i) {
        cfg.count.port_cosid_tbl[0][i] = (data->lm_config.flow_count) ? 0 : i;
        cfg.count.port_cosid_tbl[1][i] = (data->lm_config.flow_count) ? 0 : i;
        cfg.count.cosid_tbl[i] = i;
    }

    /* CCM */
    cfg.ccm.enable = data->voe_ccm_enabled || data->ccm_config.lm_enable;
    cfg.ccm.copy_to_cpu = TRUE;
    cfg.ccm.count_as_selected = FALSE;
    cfg.ccm.count_as_data = !data->ccm_config.lm_enable && !data->config.sat;  /* Dual-ended LM is not enabled - count CCM as data */
    cfg.ccm.peer_mepid = data->ccm_config.peer_mep;
    memcpy(cfg.ccm.megid.data, data->ccm_config.meg, sizeof(cfg.ccm.megid.data));

    cfg.ccm.tx_seq_no_auto_update = !MPLS_DOMAIN(data->create.domain);  // G-8113.1, sect. 9.1.1: No CCM sequence numbers for MPLS type 1 OAM
    cfg.ccm.rx_seq_no_check       = !MPLS_DOMAIN(data->create.domain);

    cfg.ccm.rx_priority = data->ccm_config.prio;
    cfg.ccm.rx_period = voe_period_calc(data->ccm_config.cc_rate);

    /* CCM LM */
    cfg.ccm_lm.enable = data->ccm_config.lm_enable;
    cfg.ccm_lm.copy_to_cpu = cfg.ccm_lm.enable;
    cfg.count.yellow = TRUE;
    cfg.ccm_lm.count_as_selected = FALSE;
    cfg.ccm_lm.period = voe_period_calc(data->ccm_config.lm_rate);

    /* SLM LM */
    cfg.sl.enable = data->sl_config.enable_tx || data->sl_config.enable_rx;
    cfg.sl.copy_slm_to_cpu = FALSE;
    cfg.sl.copy_slr_to_cpu = FALSE;
    cfg.sl.test_id = data->sl_config.test_id;
    cfg.sl.priority = data->sl_config.prio;
    cfg.sl.count_as_selected = FALSE;
    cfg.sl.count_as_data = FALSE;
    memset(cfg.sl.peer_mepid_valid, 0, sizeof(cfg.sl.peer_mepid_valid));
    for (i=0; i<data->sl_config.peer_count; ++i) {
        cfg.sl.peer_mepid_valid[i] = cfg.sl.enable;
        cfg.sl.peer_mepid[i] = data->sl_config.peer_mep[i];
    }

    /* LMM LM */
    cfg.single_ended_lm.enable = !cfg.ccm_lm.enable && !cfg.sl.enable;    /* If Dual-ended LM and Synthetic LM is not enabled we always respond on single ended */
    cfg.single_ended_lm.copy_lmm_to_cpu = FALSE;
    cfg.single_ended_lm.copy_lmr_to_cpu = TRUE;
    cfg.count.yellow = TRUE;
    cfg.single_ended_lm.count_as_selected = FALSE;
    cfg.single_ended_lm.count_as_data = FALSE;

    /* DM */
    cfg.dm.enable_dmm = TRUE;
    cfg.dm.enable_1dm = TRUE;
    cfg.dm.copy_dmm_to_cpu = FALSE;
    cfg.dm.copy_1dm_to_cpu = TRUE;
    cfg.dm.copy_dmr_to_cpu = TRUE;
    cfg.dm.count_as_selected = FALSE;
    cfg.dm.count_as_data = data->config.sat ? TRUE : FALSE;     /* During SAT the LM counters are used for counting DM per COS */
    cfg.dm.extern_ts = FALSE;

    /* LB */
    if (lb_start) {
        mesa_oam_proc_status_t voe_proc_status;
        (void)mesa_oam_voe_counter_clear(NULL, data->voe_idx, MESA_OAM_CNT_LB);
        (void)mesa_oam_proc_status_get(NULL, data->voe_idx, &voe_proc_status);   /* Set TX and RX transaction id to next transmitted */
        cfg.lb.tx_transaction_id = (voe_proc_status.tx_next_lbm_transaction_id == 0) ? 1 : voe_proc_status.tx_next_lbm_transaction_id;
        cfg.lb.rx_transaction_id = cfg.lb.tx_transaction_id - 1;
    }
    cfg.lb.enable                    = !data->tst_config.enable;
    cfg.lb.copy_lbm_to_cpu           = FALSE; // TBD ((cfg.mep_type == VTSS_OAM_UPMEP) && !data->config.out_of_service) ? TRUE : FALSE;
    cfg.lb.copy_lbr_to_cpu           = data->lb_config.copy_lbr_to_cpu;
    cfg.lb.count_as_selected         = FALSE;
    cfg.lb.count_as_data             = FALSE;
    cfg.lb.tx_update_transaction_id  = TRUE;
    cfg.g8113_1.lb_initiator_mode    = MESA_OAM_G8113_1_LB_MODE_DISCOVERY;
    cfg.g8113_1.lb_check_mepid_megid = TRUE;
    /* not set, as this is only for MIP - and MIP on MPLS can only be handled by sending to CPU
       cfg.g8113_1.mipid.icc_carrier_code[6];
       cfg.g8113_1.mipid.node_id;
       cfg.g8113_1.mipid.if_num;
    */

    /* TST */
    enable = data->tst_config.enable;
    if (enable && !cfg.tst.enable) {
        (void)mesa_oam_voe_counter_clear(NULL, data->voe_idx, MESA_OAM_CNT_TST);
        cfg.tst.tx_seq_no = data->tst_config.transaction_id + 1;
        cfg.tst.rx_seq_no = data->tst_config.transaction_id;
    }
    cfg.tst.enable                = enable;
    cfg.tst.copy_to_cpu           = FALSE;
    cfg.tst.count_as_selected     = FALSE;
    cfg.tst.count_as_data         = FALSE;
    cfg.tst.tx_seq_no_auto_update = TRUE;

    /* LT */
    cfg.lt.enable            = TRUE;
    cfg.lt.copy_ltm_to_cpu   = TRUE;
    cfg.lt.copy_ltr_to_cpu   = data->lt_config.copy_ltr_to_cpu;
    cfg.lt.count_as_selected = FALSE;
    cfg.lt.count_as_data     = FALSE;

    /* SAT */
    cfg.sat.enable                      = data->config.sat;     /* In case of SAT the COSID counterset must be allocated */
    if (data->config.sat && (data->sat_cnt_idx == mep_caps.mesa_oam_sat_cosid_ctr_cnt) && (rc = mesa_oam_sat_cosid_counters_alloc(NULL, &data->sat_cnt_idx)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_sat_cosid_counters_alloc() failed %s", error_txt(rc));
        return(FALSE);
    }
    cfg.sat.cosid_seq_no_mode           = MESA_OAM_SAT_COSID_SEQ_NO_LB_TST;
    cfg.sat.cosid_seq_no_idx            = data->sat_cnt_idx;
    cfg.sat.non_oam_seq_no_offs         = 0;                                       /* Only for NON OAM */
    cfg.sat.non_oam_seq_no_mode         = MESA_OAM_SAT_NON_OAM_SEQ_NO_DISABLED;    /* Only for NON OAM */
    cfg.sat.non_oam_upd_udp_chksum      = FALSE;                                   /* Only for NON OAM */
    cfg.sat.non_oam_seq_no_rx_err_cnt   = FALSE;                                   /* Only for NON OAM */

    if (!data->config.sat && data->lm_config.oam_count != MEP_MODEL_OAM_COUNT_Y1731) {  /* At this point OAM PDU counting is configured as Y.1731 - this can be changed */
        oam_count = (data->lm_config.oam_count == MEP_MODEL_OAM_COUNT_ALL) ? TRUE : FALSE;

        cfg.generic[GENERIC_AIS].count_as_data = oam_count;
        cfg.generic[GENERIC_LCK].count_as_data = oam_count;
        cfg.generic[GENERIC_LAPS].count_as_data = oam_count;
        cfg.generic[GENERIC_RAPS].count_as_data = oam_count;
        cfg.unknown.count_as_data = oam_count;
        cfg.ccm.count_as_data = oam_count;
        cfg.single_ended_lm.count_as_data = oam_count;
        cfg.sl.count_as_data = oam_count;
        cfg.lb.count_as_data = oam_count;
        cfg.tst.count_as_data = oam_count;
        cfg.dm.count_as_data = oam_count;
        cfg.lt.count_as_data = oam_count;
    }









































    if ((rc = mesa_oam_voe_conf_set(NULL, data->voe_idx, &cfg)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_conf_set() failed %s", error_txt(rc));
        return(FALSE);
    }

    u32 event_mask = voe_event_mask;





    if ((rc = mesa_oam_voe_event_enable(NULL, data->voe_idx, event_mask, data->voe_ccm_enabled)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_event_enable() failed %s", error_txt(rc));
        return(FALSE);
    }

    return(TRUE);
}

static BOOL client_flow_mce_config(MepModelInstance     *data,
                                   inst_client_config   *client_config)
{
    mesa_mce_port_info_t  mce_info;
    mesa_mce_t            mce;
    u32                   i, rc=0;
    mesa_port_list_t      uni, nni;

    T_NG(TRACE_GRP_MODEL, "Enter %u", data->instance);

    if (client_config->rev_mce_id != MESA_MCE_ID_LAST) {
        (void)mesa_mce_del(NULL, client_config->rev_mce_id);
        client_config->rev_mce_id = MESA_MCE_ID_LAST;
    }
    if (client_config->mce_id != MESA_MCE_ID_LAST) {
        (void)mesa_mce_del(NULL, client_config->mce_id);
        client_config->mce_id = MESA_MCE_ID_LAST;
    }
    client_config->oper_state = FALSE;

    if (client_config->config.domain == MEP_MODEL_EVC) {   /* In the EVC domain MCE rule is required to do client injection into the EVC */
        if (!evc_flow_info_port_get(client_config->config.flow, nni, uni)) {
            T_DG(TRACE_GRP_MODEL, "evc_flow_info_port_get() failed instance=%u EVC=%u", data->instance, client_config->config.flow);
            return FALSE;
        }
        if (((data->create.direction == MEP_MODEL_DOWN) && !nni[data->config.port]) ||
            ((data->create.direction == MEP_MODEL_UP) && !uni[data->config.port])) {
            T_DG(TRACE_GRP_MODEL, "EVC client flow not operational  instance=%u EVC=%u", data->instance, client_config->config.flow);
            return FALSE;
        }

        /* Create reverse injection rule: */
        if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
        }
        mce.id = MCE_ID_CLIENT_CALC(data->instance, data->config.port, client_config->config_idx, EXTRACT);
        mce.rule = MESA_MCE_RULE_UPINJ;
        mce.key.port_list[data->config.port] = TRUE;
        mce.key.isdx = MESA_MCE_ISDX_NEW;
        for (i = 0; i < mep_caps.mesa_port_cnt; ++i) {
            if (nni[i] || uni[i]) {
                mce.action.port_list[i] = TRUE;
            }
        }
        mce.action.evc_id       = client_config->config.flow;
        mce.action.evc_counting = TRUE;
        mce.action.evc_cos      = TRUE;
        if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
            T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed %s", error_txt(rc));
            return FALSE;
        }
        if ((rc = mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed %s", error_txt(rc));
            return FALSE;
        }
        client_config->rev_isdx   = mce_info.isdx;
        client_config->rev_mce_id = mce.id;

        /* Create source injection rule: */
        if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
        }
        mce.id = MCE_ID_CLIENT_CALC(data->instance, data->config.port, client_config->config_idx, INJECT);
        mce.rule = MESA_MCE_RULE_TX;
        mce.order = MESA_MCE_ORDER_0;   /* Client source injection rule must be in front of any blocking MCE */
        mce.key.isdx = MESA_MCE_ISDX_NEW;
        mce.action.port_list[data->config.port] = TRUE;
        mce.action.evc_id       = client_config->config.flow;
        mce.action.evc_cos      = TRUE;
        mce.action.independent_mel = TRUE;  /* Client source injected OAM (currently) has independent MEL when hitting the port VOE */
        if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
            T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed %s", error_txt(rc));
            return FALSE;
        }
        if ((rc = mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed %s", error_txt(rc));
            return FALSE;
        }
        client_config->isdx   = mce_info.isdx;
        client_config->mce_id = mce.id;
    }

    if ((client_config->config.domain == MEP_MODEL_VLAN) && (data->create.direction == MEP_MODEL_DOWN)) {   /* In the VLAN domain a Down-MEP needs a reverse injection rule in order to get into the VSI. This is to hit any other Down-MEP on other ports for level filtering and blocking. Not required by Up-MEP as it cannot hit any Down-MEP due to level */
        if (!vlan_flow_info_get(client_config->config.flow, nni)) {
            T_DG(TRACE_GRP_MODEL, "vlan_flow_info_get() failed instance=%u VLAN=%u", data->instance, client_config->config.flow);
            return FALSE;
        }
        if (!nni[data->config.port]) {
            T_DG(TRACE_GRP_MODEL, "VLAN client flow not operational  instance=%u EVC=%u", data->instance, client_config->config.flow);
            return FALSE;
        }

        /* Create reverse injection rule: */
        if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
        }
        mce.id = MCE_ID_CLIENT_CALC(data->instance, data->config.port, client_config->config_idx, EXTRACT);
        mce.rule = MESA_MCE_RULE_UPINJ;
        mce.key.port_list[data->config.port] = TRUE;
        mce.key.isdx = MESA_MCE_ISDX_NEW;
        mce.action.vid = client_config->config.flow;
        for (i = 0; i < mep_caps.mesa_port_cnt; ++i) {
            if (nni[i]) {
                mce.action.port_list[i] = TRUE;
            }
        }
        if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
            T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed %s", error_txt(rc));
            return FALSE;
        }
        if ((rc = mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed %s", error_txt(rc));
            return FALSE;
        }
        client_config->rev_isdx   = mce_info.isdx;
        client_config->rev_mce_id = mce.id;

        if (client_vlan_source_isdx[data->config.port] == MESA_ISDX_NONE) {
            /* Create source injection rule: */
            if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
                T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
            }
            mce.id = MCE_ID_PORT_CLIENT_CALC(data->config.port);
            mce.rule = MESA_MCE_RULE_TX;
            mce.order = MESA_MCE_ORDER_0;   /* Client source injection rule must be in front of any blocking MCE */
            mce.key.isdx = MESA_MCE_ISDX_NEW;
            mce.action.port_list[data->config.port] = TRUE;
            mce.action.independent_mel = TRUE;  /* Client source injected OAM (currently) has independent MEL when hitting the port VOE */
            if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
                T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed %s", error_txt(rc));
                return FALSE;
            }
            if ((rc = mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info)) != VTSS_RC_OK) {
                T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed %s", error_txt(rc));
                return FALSE;
            }
            client_vlan_source_isdx[data->config.port] = mce_info.isdx;
        }
    }

    client_config->oper_state = TRUE;

    return TRUE;
}

static BOOL client_flow_oper_state(vtss::Vector<inst_client_config> &client_config,  const mep_model_client_t &client_flow)
{
    for (auto &e : client_config) {
        if ((e.config.domain == client_flow.domain) && (e.config.flow == client_flow.flow)) {
            return e.oper_state;
        }
    }
    return FALSE;
}

static BOOL client_flow_update(u32 flow, mep_model_domain_t domain)
{
    vtss::Map<u32, tx_data_t>::iterator i;
    MepModelInstance                    *data;
    tx_data_t                           *tx_data;
    u32                                 next = 0, rc;
    BOOL                                ret = TRUE;

    while ((data = get_instance_created(&next))) {
        for (auto &e : data->client_config) {
            if (e.config.domain != domain || e.config.flow != flow) {
                continue;
            }
            /* found matching domain client flow - update: */
            if ((e.mce_id != MESA_MCE_ID_LAST) && (rc = mesa_mce_del(NULL, e.mce_id)) != VTSS_RC_OK) {
                T_EG(TRACE_GRP_MODEL, "mesa_mce_del failed instance %u mce_id %X %s", data->instance, e.mce_id, error_txt(rc));
                ret = FALSE;
            }
            e.mce_id = MESA_MCE_ID_LAST;

            if ((e.rev_mce_id != MESA_MCE_ID_LAST) && (rc = mesa_mce_del(NULL, e.rev_mce_id)) != VTSS_RC_OK) {
                T_EG(TRACE_GRP_MODEL, "mesa_mce_del failed instance %u rev_mce_id %X %s", data->instance, e.rev_mce_id, error_txt(rc));
                ret = FALSE;
            }
            e.rev_mce_id = MESA_MCE_ID_LAST;

            if (!client_flow_mce_config(data, &e)) {
                T_DG(TRACE_GRP_MODEL, "client_flow_mce_config failed instance %u mce_id %X rev_mce_id %X", data->instance, e.mce_id, e.rev_mce_id);
                ret = FALSE;
            }
        }

        for (i = data->tx_data.begin(); i != data->tx_data.end(); ++i) {    /* Restart all client injection */
            tx_data = &(*i).second;
            if (tx_data->pdu.client_inj && (tx_data->pdu.client_flow.domain == domain) && (tx_data->pdu.client_flow.flow == flow)) {
                afi_tx_data_restart(data, (*i).first, tx_data);
            }
        }
    }
    return ret;
}

static u8 domain_level_value_mask_calc(MepModelInstance *data,  u32 port,  u32 *mask,  BOOL leaf)
{
    u32              low_level, high_level, retval = 0, next;
    MepModelInstance *idata;

    if (data->create.domain == MEP_MODEL_EVC_SUB) {
        low_level = (data->create.type == MEP_MODEL_MIP) ? data->config.level : 0;
    } else {
        /* Calculate the MCE key low level value and mask - on this port */
        low_level = 0;
        next = 0;
        while ((idata = get_instance_domain_flow(data->create.domain, data->create.flow_num, &next)) != NULL) { /* Get all MEPs in this flow */
            if ((idata->oper_state == MEP_MODEL_OPER_UP) && (idata->create.type == MEP_MODEL_MEP) &&
                !((idata->create.direction != data->create.direction) && (idata->config.port == data->config.port)) &&   /* Instances with opposite direction on same residence port is not used */
                (((idata->create.direction == MEP_MODEL_DOWN) && ((idata->config.port == port) || (idata->p_port == port)) && !leaf) || (idata->create.direction == MEP_MODEL_UP)) && /* Down-MEP on same port (if not leaf) or any Up-MEP is used */
                (idata->config.level < data->config.level) && (idata->config.level >= low_level)) {
                low_level = idata->config.level+1;
            }
        }
    }

    high_level = data->config.level;    /* Default the MEP RX rule match level up to configured level */

    if (data->create.domain == MEP_MODEL_EVC) { /* In the EVC domain the highest match level depends on configuration */
        if (data->mep_highest) { /* This MEP is on highest level on residence port */
            high_level = 7; /* Default the MEP on highest level is covering all levels - up to 7 */
            if ((data->create.direction == MEP_MODEL_DOWN) && ((idata = get_instance_up_mep_lowest_of_two_any(data->create.domain, data->create.flow_num)) != NULL)) {   /* Find an Up-MEP lowest of two on any UNI */
                high_level = idata->config.level;   /* The highest Down-MEP RX rule match level covering the lowest Up-MEP level - if there are two Up-MEPs */
            }
        }
    }

    retval = (0x01 << low_level) - 1;                        /* Level value is calculated                   */
    *mask = (0x01 << (high_level - low_level)) - 1;          /* "don't care" mask value is calculated       */
    *mask <<= low_level;                                     /* mask is rotated to cover levels             */
    *mask = ~*mask;                                          /* Mask is complimented as "don't care" is '0' */

    return(retval & 0xFF);
}

static mesa_oam_voe_idx_t mep_highest_voe_idx(mep_model_domain_t domain,  mep_model_direction_t direction,  u32 port,  u32 flow)
{
    MepModelInstance *data;

    if ((data = get_instance_mep_highest(domain, flow, direction, port)) != NULL) {
        return(data->voe_idx);
    } else {
        return(MESA_OAM_VOE_IDX_NONE);
    }
}

static mesa_isdx_t up_rx_isdx(MepModelInstance *data,  u32 port,  u32 vid)
{
    u32              next;
    MepModelInstance  *idata;

    /* Find other Up-MEP/MIP on my level with allocated rx ISDX  */
    next = 0;
    while ((idata = get_instance_domain_flow(data->create.domain, data->create.flow_num, &next)) != NULL) { /* Look for Up-MEPs in this flow to find RX ISDX to share */
        if ((idata->oper_state == MEP_MODEL_OPER_UP) && (idata != data) && ((data->create.domain != MEP_MODEL_EVC_SUB) || (data->config.vid == idata->config.vid)) &&
            (idata->create.direction == MEP_MODEL_UP) && (idata->config.level == data->config.level)) {
            for (const auto &e : idata->rx_isdx) {
                if ((e.isdx != VTSS_ISDX_NONE) && (e.rx_port == port) && (e.i_vid == vid)) {   /* ISDX on this port found */
                    T_DG(TRACE_GRP_ISDX, "Shared ISDX found  instance %u  'owner' %u  port %u  vid %u", data->instance, idata->instance, port, vid);
                    return(e.isdx);     /* Reuse/share ISDX */
                }
            }
        }
    }

    return MESA_MCE_ISDX_NEW;   /* Return "NEW" if ISDX was not found to indicate new is required */
}

static BOOL get_up_mep_ports(MepModelInstance *data, mesa_port_list_t &nni)
{
    /* This function calculate what ports (NNIs) are in "front of" and Up-MEP */
    mesa_port_list_t uni;

    T_NG(TRACE_GRP_MODEL, "Enter  instance %u", data->instance);

    if (data->create.direction != MEP_MODEL_UP || (data->oper_state == MEP_MODEL_OPER_DOWN)) {
        return FALSE;
    }
    if ((data->create.domain == MEP_MODEL_EVC) || (data->create.domain == MEP_MODEL_EVC_SUB)) {
        return evc_flow_info_port_get(data->create.flow_num, nni, uni);
    } else if (data->create.domain == MEP_MODEL_VLAN) {
        return vlan_flow_info_get(data->create.flow_num, nni);
    }
    return FALSE;
}

static void port_list_in_vlan_domain(u32 vid,  mesa_port_list_t &port_list)
{
    u32              i, next;
    mesa_port_list_t nni;
    MepModelInstance  *idata;

    T_NG(TRACE_GRP_MODEL, "Enter  vid %u", vid);

    next = 0;
    while ((idata = get_instance_domain_flow(MEP_MODEL_VLAN, vid, &next)) != NULL) {	/* Run through all created MEPs in the VLAN domain */
        if (idata->oper_state == MEP_MODEL_OPER_UP) {	/* This instance is operational */
            if (idata->create.direction == MEP_MODEL_DOWN) {
                port_list[idata->config.port] = TRUE;    /* Down-MEP only configure MCE on residence port */
            } else {	/* Up-MEP */
                if (!vlan_flow_info_get(idata->i_vid, nni)) {
                    T_DG(TRACE_GRP_MODEL, "vlan_flow_info_get(%u) failed", vid);
                    return;
                }
                for (i=0; i<mep_caps.mesa_port_cnt; ++i) {
                    if (nni[i]) {  /* All flow ports are port on this VLAN */
                        port_list[i] = TRUE;
                    }
                }
            }
        }
    }
}

static BOOL tst_lbr_mce_config_workaround(MepModelInstance *data)
{
    mesa_rc              rc;
    mesa_mce_t           mce;
    mesa_mce_port_info_t mce_info;

    T_DG(TRACE_GRP_MODEL, "Enter  instance %u  direction %u  domain %u", data->instance, data->create.direction, data->create.domain);

    if (data->tst_lbr_mce_id != MESA_MCE_ID_LAST) {     /* Check if TST MCE rules are already created */
        return TRUE;
    }

    if ((data->create.direction != MEP_MODEL_DOWN) || (data->tst_lbr_rx_mce_id == MESA_MCE_ID_LAST)) { /* Only Down MEP with valid TST RX MCE ID */
        T_EG(TRACE_GRP_MODEL, "Invalid MEP instance  instance %u  direction %u  tst_lbr_rx_mce_id %u", data->instance, data->create.direction, data->tst_lbr_rx_mce_id);
        return FALSE;
    }

    if ((rc = mesa_mce_get(NULL, data->tst_lbr_rx_mce_id, &mce)) != VTSS_RC_OK) {    /* Get the RX MCE that this TST counting MCE must be a copy of and in front of */
        T_EG(TRACE_GRP_MODEL, "mesa_mce_get() failed  mce_id %X  %s", data->tst_lbr_rx_mce_id, error_txt(rc));
        return FALSE;
    }

    mce.id = MCE_ID_CALC(data->instance, data->config.port, RX_OPCODE, EXTRACT, ROOT);
    mce.key.opcode = data->lb_config.enable ? MESA_OAM_OPCODE_LBR : MESA_OAM_OPCODE_TST;
    mce.action.isdx = MESA_MCE_ISDX_NEW;
    mce.action.isdx_counting = TRUE;

    if ((rc = mesa_mce_add(NULL, data->tst_lbr_rx_mce_id, &mce)) != VTSS_RC_OK) {  /* This TST counting MCE must be in front of the RX MCE for this instance */
        T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed %s", error_txt(rc));
        return(FALSE);
    }

    if ((rc = mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed  instance %u  %s", data->instance, error_txt(rc));
        return(FALSE);
    }

    set_instance_rx_isdx(data, mce_info.isdx, data->config.port, data->i_vid);

    data->tst_lbr_mce_id = mce.id;
    data->rx_tst_lbr_isdx = mce_info.isdx;

    return TRUE;
}

static BOOL tst_lbr_mce_config(MepModelInstance *data)
{
    if (mep_caps.mesa_mep_lbr_mce_hw_support) {
        return TRUE;
    }
    // There's no H/W support, so we need to run the work-around
    return tst_lbr_mce_config_workaround(data);
}

static BOOL tst_lbr_mce_delete(MepModelInstance *data)
{
    u32                  rc=0;

    if (data->tst_lbr_mce_id == MESA_MCE_ID_LAST) {
        return TRUE;
    }
    if ((rc = mesa_mce_del(NULL, data->tst_lbr_mce_id)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "vtss_mce_del failed instance %u mce_id %X %s", data->instance, data->tst_lbr_mce_id, error_txt(rc));
    }

    del_instance_rx_isdx(data, data->rx_tst_lbr_isdx);
    data->tst_lbr_mce_id = MESA_MCE_ID_LAST;
    data->rx_tst_lbr_isdx = VTSS_ISDX_NONE;

    return TRUE;
}

static BOOL block_mce_config(MepModelInstance  *data)
{
    u32         rc;
    mesa_mce_t  mce;

    T_DG(TRACE_GRP_MODEL, "Enter  instance %u  direction %u  domain %u", data->instance, data->create.direction, data->create.domain);

    if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed %s", error_txt(rc));
    }

    mce.id = MCE_ID_CALC(data->instance, data->config.port, RX, DISCARD, ROOT);
    mce.rule = MESA_MCE_RULE_RX;
    mce.key.second    = TRUE;

    if (data->create.domain == MEP_MODEL_PORT) {
        mce.order = MESA_MCE_ORDER_1;   /* Port domain MCEs have higher hit order than other domains. This assures that this Port domain RX block MCE is first */
    }
    mce.key.port_list[data->config.port] = TRUE;
    mce.key.service_detect = TRUE;
    mce.key.second_tag.tagged    = MESA_VCAP_BIT_1;
    mce.key.second_tag.s_tagged  = MESA_VCAP_BIT_ANY;
    if ((data->create.domain == MEP_MODEL_VLAN) || (data->create.domain == MEP_MODEL_EVC)) {
        mce.key.first_tag.tagged    = MESA_VCAP_BIT_1;
        mce.key.first_tag.s_tagged  = MESA_VCAP_BIT_ANY;
        mce.key.first_tag.vid.value = data->i_vid;
        mce.key.first_tag.vid.mask  = 0xFFFF;
    }
    if (data->create.domain == MEP_MODEL_PORT) {
        mce.key.first_tag.tagged    = MESA_VCAP_BIT_1;
        mce.key.first_tag.s_tagged  = MESA_VCAP_BIT_ANY;
    }

    mce.action.forward_sel = MESA_MCE_FORWARD_DISABLE;

    if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {  /* This blocking MCE must be in front of the VLAN common MCE. This is achieved through mce.order */
        T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed %s", error_txt(rc));
        return(FALSE);
    }

    data->rx_block_mce_id = mce.id;

    if (data->create.domain == MEP_MODEL_PORT) {    /* In port domain the VOE cannot TX block frames that has hit a service VOE */
        if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed %s", error_txt(rc));
        }

        mce.id = MCE_ID_CALC(data->instance, data->config.port, TX, DISCARD, ROOT);
        mce.rule = MESA_MCE_RULE_TX;
        mce.order = MESA_MCE_ORDER_1;   /* Port domain MCEs have higher hit order than other domains. This assures that this Port domain TX block MCE is first */

        mce.key.vid = MESA_VID_ALL;

        mce.action.port_list[data->config.port] = TRUE;
        mce.action.forward_sel = MESA_MCE_FORWARD_DISABLE;

        if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {  /* This blocking MCE must be in front of the VLAN common MCE. This is achieved through mce.order */
            T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed %s", error_txt(rc));
            return(FALSE);
        }

        data->tx_block_mce_id = mce.id;
    }

    return(TRUE);
}

static u32 evc_rx_rule_isdx_calc(MepModelInstance *data,  u32 port,  u32 vid)
{
    u32                next;
    MepModelInstance   *down_lowest_of_two, *idata;

    /* Down-MEP on lowest level of two are generating own ISDX that cannot be shared */
    down_lowest_of_two = get_instance_down_mep_lowest_of_two(MEP_MODEL_EVC, data->create.flow_num, port);

    if (down_lowest_of_two == data) {   /* This instance is lowest of two */
        return MESA_MCE_ISDX_NEW;
    }

    /* Look for ISDX to share between Up and Down-MEPs on this port */
    next = 0;
    while ((idata = get_instance_domain_flow(MEP_MODEL_EVC, data->create.flow_num, &next)) != NULL) { /* Look for all instances in this flow */
        if ((idata->oper_state != MEP_MODEL_OPER_UP) || (idata == data)) {
            continue;
        }
        if (down_lowest_of_two == idata) { /* Down-MEP on lowest level of two are generating own ISDX that cannot be shared */
            continue;
        }
        for (const auto &e : idata->rx_isdx) {
            if ((e.isdx != VTSS_ISDX_NONE) && (e.rx_port == port) && (e.i_vid == vid)) {   /* ISDX on this port is found */
                T_DG(TRACE_GRP_ISDX, "Shared ISDX found  instance %u  isdx %u  'owner' %u  port %u  vid %u", data->instance, e.isdx, idata->instance, port, vid);
                return(e.isdx);     /* Reuse/share ISDX */
            }
        }
    }
    return (data->create.direction == MEP_MODEL_DOWN) ? MESA_MCE_ISDX_NEW : MESA_MCE_ISDX_EVC;   /* Down-MEP need new ISDX but Up-MEP can use the EVC ISDX */
}

static BOOL evc_domain_mce_config(MepModelInstance *data)
{
    u32                   rc=0;
    mesa_mce_port_info_t  mce_info;
    MepModelInstance      *idata;
    mesa_mce_t            mce;
    mesa_isdx_t           root_rx_isdx, leaf_rx_isdx, up_mep_rx_isdx;
    u32                   mask, vid, i_vid, leaf_vid, leaf_i_vid, port, i, next;
    BOOL                  e_tree, leaf=FALSE;
    MepModelInstance      *instance_mep_highest = NULL;
    mesa_port_list_t      uni;
    mesa_port_list_t      leaf_uni;
    mesa_port_list_t      nni;
    mesa_port_list_t      root_ports, leaf_ports;
    mesa_port_list_t      ports;

    T_DG(TRACE_GRP_MODEL, "Enter  Instance %u", data->instance);

    if (!evc_flow_info_get(data->create.flow_num, nni, uni, &vid, &i_vid, &e_tree, leaf_uni, &leaf_vid, &leaf_i_vid)) {
        return(FALSE);
    }
    memset(root_ports, 0, sizeof(root_ports));
    memset(leaf_ports, 0, sizeof(leaf_ports));
    for (i = 0; i < mep_caps.mesa_port_cnt; ++i) {
        if (uni[i] || nni[i])
            root_ports[i] = TRUE;   /* Ports in the ROOT VLAN */
        if ((uni[i] && !leaf_uni[i]) || nni[i])
            leaf_ports[i] = TRUE;   /* Ports in the LEAF VLAN */
    }
    leaf_rx_isdx = root_rx_isdx = up_mep_rx_isdx = MESA_MCE_ISDX_NONE;

    free_mce_resources(data);   /* Free all MCE resources */

    data->i_vid = i_vid;                    /* Internal ROOT VID for this flow */
    data->leaf_i_vid = leaf_i_vid;          /* Internal LEAD VID for this flow */
    data->e_tree = MEP_MODEL_ETREE_NONE;    /* E-TREE role */
    if (data->create.direction == MEP_MODEL_UP) {
        leaf = e_tree && leaf_uni[data->config.port];     /* Calculate if this is a LEAF Up-MEP/MIP */
        data->e_tree = e_tree ? (leaf ? MEP_MODEL_ETREE_LEAF : MEP_MODEL_ETREE_ROOT) : MEP_MODEL_ETREE_NONE;  /* Calculate E-TREE mode */
    }

    memset(ports, 0, sizeof(ports));    /* Calculate the ports to create RX rules */
    if (data->create.direction == MEP_MODEL_DOWN) {
        ports[data->config.port] = TRUE;    /* Down-MIP only configure RX rules on residence port */
        if (data->p_port != P_PORT_NONE) {  /* Include any protecting port as X2 CLM rules can only match on one port - a Down-MEP needs a rule on both working and protecting port. */
            ports[data->p_port] = TRUE;
        }
    } else {
        memcpy(ports, nni, sizeof(ports));  /* Up-MEP configure RX rules on all NNI port */
    }

    data->up_mep_highest_of_two = (get_instance_up_mep_highest_of_two(MEP_MODEL_EVC, data->create.flow_num, data->config.port) == data) ? TRUE : FALSE;
    data->mep_highest = data->up_mep_highest_of_two || ((get_instance_mep_highest(MEP_MODEL_EVC, data->create.flow_num, data->create.direction, data->config.port) == data) ? TRUE : FALSE);

    /************ Create extraction MCE rules for all ports *************/
    for (port=0; port<mep_caps.mesa_port_cnt; ++port) {
        if (!ports[port]) {
            continue;
        }

        if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
        }

        /* ROOT RX rule */
        mce.id   = MCE_ID_CALC(data->instance, port, RX, EXTRACT, ROOT);
        mce.rule = MESA_MCE_RULE_RX;
        mce.key.second    = TRUE;

        mce.key.port_list[port] = TRUE;
        mce.key.first_tag.tagged    = MESA_VCAP_BIT_1;
        mce.key.first_tag.s_tagged  = MESA_VCAP_BIT_ANY;
        mce.key.first_tag.vid.value = vid;
        mce.key.first_tag.vid.mask  = 0xFFFF;
        mce.key.first_tag.pcp.mask  = 0x00;
        mce.key.first_tag.dei       = MESA_VCAP_BIT_ANY;
        mce.key.second_tag.tagged   = MESA_VCAP_BIT_0;
        mce.key.isdx      = MESA_MCE_ISDX_NONE;

        mce.action.port_list = root_ports;
        mce.action.voe_idx = (data->create.direction == MEP_MODEL_DOWN) ? data->voe_idx : mep_highest_voe_idx(MEP_MODEL_EVC, MEP_MODEL_DOWN, port, data->create.flow_num);
        mce.action.oam_detect   = MESA_MCE_OAM_DETECT_SINGLE_TAGGED;
        mce.action.evc_id       = data->create.flow_num;
        mce.action.evc_counting = (data->create.direction == MEP_MODEL_DOWN) ? FALSE : TRUE;
        mce.action.evc_cos      = TRUE;
        mce.action.pop_cnt      = nni[port] ? 1 : 0;
        mce.action.vid          = data->up_mep_highest_of_two ? VID_OAM_MEG1 : VID_OAM_MEG0;    /* Highest Up-MEP of two classify to MEG-1 */

        /* Only create ROOT RX rule if not already done */
        if ((data->create.direction == MEP_MODEL_DOWN) ||             /* This instance is a Down-MEP OR */
            (((up_mep_rx_isdx = up_rx_isdx(data, port, i_vid)) == MESA_MCE_ISDX_NEW) &&  /* Other Up-MEP on same level has not created RX rules AND */
             (data->up_mep_highest_of_two ||                          /* This instance is highest Up-MEP of two - on residence port OR */
              ((instance_mep_highest = get_instance_mep_highest(MEP_MODEL_EVC, data->create.flow_num, MEP_MODEL_DOWN, port)) == NULL)))) { /* There are no Down-MEPs on this port */
            /* ROOT RX rule must be created */
            T_DG(TRACE_GRP_MODEL, "RX rule must be created  instance %u", data->instance);
            mce.key.mel.value = domain_level_value_mask_calc(data, port, &mask, FALSE);
            mce.key.mel.mask  = mask;

            mce.action.isdx     = evc_rx_rule_isdx_calc(data, port, i_vid);
            mce.action.evc_leaf = FALSE;    /* Already default */

            if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
                T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
                return(FALSE);
            }
            if (!data->mce_ids.push_back(mce.id)) {
                T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
                return(FALSE);
            }
            if ((rc = mesa_mce_port_info_get(NULL, mce.id, port, &mce_info)) != VTSS_RC_OK) {
                T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed  instance %u  %s", data->instance, error_txt(rc));
                return(FALSE);
            }
            if (data->create.direction == MEP_MODEL_DOWN) {  /* This is a Down-MEP - save the MCE ID to be used when TST is enabled. A special TST matching MCE must be created and inserted in front of this MCE in order to count */
                data->tst_lbr_rx_mce_id = mce.id;
            }
            root_rx_isdx = mce_info.isdx;

            if ((data->create.direction == MEP_MODEL_DOWN) && data->mep_highest) {  /* This is highest Down-MEP creating RX rule. In case of an Up-MEP not highest of two, it shares the RX ISDX */
                T_DG(TRACE_GRP_MODEL, "This is highest Down-MEP creating RX rule  instance %u", data->instance);
                next = 0;
                while ((idata = get_instance_domain_flow(MEP_MODEL_EVC, data->create.flow_num, &next)) != NULL) { /* Get all MEPs in this flow */
                    if ((idata->oper_state == MEP_MODEL_OPER_UP) && (idata->create.direction == MEP_MODEL_UP))  { /* Found an operational Up-MEP. It might be that this Up-MEP is highest of two and already got this RX ISDX. */
                        T_DG(TRACE_GRP_ISDX, "Down-MEP RX ISDX given to Up-MEP  instance %u  'receiver' %u", data->instance, idata->instance);
                        set_instance_rx_isdx(idata, root_rx_isdx, port, i_vid);
                    }
                }
            }
        } else {  /* MESA_MCE_RULE_RX */
            /* No RX rule is created for this Up-MEP. The instance still needs a RX ISDX */
            T_DG(TRACE_GRP_MODEL, "No RX rule is created  instance %u", data->instance);
            if (up_mep_rx_isdx != MESA_MCE_ISDX_NEW) {  /* Other Up-MEP on same level has created RX rules */
                T_DG(TRACE_GRP_ISDX, "Other Up-MEP on same level has created RX rules  instance %u  isdx %u", data->instance, up_mep_rx_isdx);
                root_rx_isdx = up_mep_rx_isdx;
            } else if (instance_mep_highest != NULL) {   /* There are Down-MEPs on this port */
                /* Nothing has to be done as the Down-MEPs will give there RX ISDX to thid Up-MEP */
            }
        }
        if (root_rx_isdx != MESA_MCE_ISDX_NONE) {   /* Set Rx ISDX if any calculated */
            T_DG(TRACE_GRP_ISDX, "RX ISDX calculated  instance %u", data->instance);
            set_instance_rx_isdx(data, root_rx_isdx, port, i_vid);
        }

        /* Only create LEAF RX rule if not already done */
        if (e_tree && !leaf && (data->create.direction == MEP_MODEL_UP)) {   /* This is E-TREE ROOT Up-MEP - LEAF VLAN must be handled. There are no Down-MEP in LEAF VLAN AND */
            if ((up_mep_rx_isdx = up_rx_isdx(data, port, leaf_i_vid)) == MESA_MCE_ISDX_NEW) {   /* Other Up-MEP on same level has not created RX rules */
                /* LEAF RX rule */
                mce.id = MCE_ID_CALC(data->instance, port, RX, EXTRACT, LEAF);
                mce.key.first_tag.vid.value = leaf_vid;
                mce.key.mel.value = domain_level_value_mask_calc(data, port, &mask, TRUE);
                mce.key.mel.mask  = mask;

                mce.action.port_list = leaf_ports;
                mce.action.isdx = MESA_MCE_ISDX_NEW;
                mce.action.evc_leaf = TRUE;

                if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
                    T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
                    return(FALSE);
                }
                if (!data->mce_ids.push_back(mce.id)) {
                    T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
                    return(FALSE);
                }
                if ((rc = mesa_mce_port_info_get(NULL, mce.id, port, &mce_info)) != VTSS_RC_OK) {
                    T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed  instance %u  %s", data->instance, error_txt(rc));
                    return(FALSE);
                }
                leaf_rx_isdx = mce_info.isdx;
            } else {
                /* No RX rule is created for this Up-MEP. The instance still needs a RX ISDX */
                leaf_rx_isdx = up_mep_rx_isdx;
            }
            set_instance_rx_isdx(data, leaf_rx_isdx, port, leaf_i_vid);
        } /* ROOT Up-MEP */
    } /* Extraction RX rules port loop */

    if (data->create.direction == MEP_MODEL_UP) {   /* Only Up-MEP has extraction TX rules */
        /* ROOT TX rule */
        if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
        }

        mce.id = MCE_ID_CALC(data->instance, data->config.port, TX, EXTRACT, ROOT);
        mce.rule = MESA_MCE_RULE_TX;

        mce.key.vid = data->up_mep_highest_of_two ? VID_OAM_MEG1 : VID_OAM_MEG0;    /* Highest Up-MEP of two match on MEG-1 */

        mce.action.port_list[data->config.port] = TRUE;
        mce.action.voe_idx = data->voe_idx;
        mce.action.evc_id  = data->create.flow_num;
        mce.action.evc_leaf = FALSE;

        if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
            T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
            return(FALSE);
        }
        if (!data->mce_ids.push_back(mce.id)) {
            T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
            return(FALSE);
        }

        if (e_tree && !leaf) {   /* This is E-TREE ROOT Up-MEP - LEAF VLAN must be handled */
            /* LEAF TX rule */
            mce.id = MCE_ID_CALC(data->instance, data->config.port, TX, EXTRACT, LEAF);

            mce.action.evc_leaf = TRUE;

            if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
                T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
                return(FALSE);
            }
            if (!data->mce_ids.push_back(mce.id)) {
                T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
                return(FALSE);
            }
        }
    } /* Extraction TX rule */

    /************ Create injection MCE rules *************/
    if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
    }

    if (data->create.direction == MEP_MODEL_UP) { /* UPINJ rule */
        mce.id = MCE_ID_CALC(data->instance, data->config.port, RX, INJECT, ROOT);
        mce.rule = MESA_MCE_RULE_UPINJ;

        mce.key.port_list[data->config.port] = TRUE;
        mce.key.isdx = MESA_MCE_ISDX_NEW;

        mce.action.port_list    = (leaf ? leaf_ports : root_ports);
        mce.action.voe_idx      = data->voe_idx;
        mce.action.evc_id       = data->create.flow_num;
        mce.action.evc_leaf     = leaf;
        mce.action.evc_counting = FALSE;
        mce.action.evc_cos      = TRUE;
    } else if (data->create.direction == MEP_MODEL_DOWN) { /* TX rule for residence port */
        mce.id = MCE_ID_CALC(data->instance, data->config.port, TX, INJECT, ROOT);
        mce.rule = MESA_MCE_RULE_TX;

        mce.key.isdx = MESA_MCE_ISDX_NEW;

        mce.action.port_list[data->config.port] = TRUE;
        mce.action.voe_idx      = data->voe_idx;
        mce.action.evc_id       = data->create.flow_num;
        mce.action.evc_counting = FALSE;
        mce.action.evc_cos      = TRUE;
    }
    if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
        T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
        return(FALSE);
    }
    if (!data->mce_ids.push_back(mce.id)) {
        T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
        return(FALSE);
    }
    if ((rc = mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed  instance %u  %s", data->instance, error_txt(rc));
        return(FALSE);
    }
    data->tx_isdx = mce_info.isdx;

    /* TX rule for protected port (use the same ISDX as for working port) */
    if ((data->create.direction == MEP_MODEL_DOWN) && (data->p_port != P_PORT_NONE)) {
        mce.id = MCE_ID_CALC(data->instance, data->p_port, TX, INJECT, ROOT);
        mce.key.isdx = mce_info.isdx;
        mce.action.port_list[data->config.port] = FALSE;
        mce.action.port_list[data->p_port] = TRUE;

        if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
            T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
            return(FALSE);
        }
        if (!data->mce_ids.push_back(mce.id)) {
            T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
            return(FALSE);
        }
    }

    if (data->create.direction == MEP_MODEL_UP) {   /* In case of more Up-MEP on same level on other ports, these MEPs must have injection ISDX as RX ISDX */
        next = 0;
        while ((idata = get_instance_domain_flow(MEP_MODEL_EVC, data->create.flow_num, &next)) != NULL) { /* Get all MEPs in this flow */
            if ((idata != data) && (idata->oper_state == MEP_MODEL_OPER_UP) && (!e_tree || !leaf || (idata->e_tree == MEP_MODEL_ETREE_ROOT)) &&
                (idata->create.direction == MEP_MODEL_UP) && (idata->config.level == data->config.level))  { /* Found an operational Up-MEP on this level that is not "this". In case of ETREE a Leaf can only send to a ROOT */
                set_instance_rx_isdx(idata, data->tx_isdx, mep_caps.mesa_port_cnt, (data->e_tree == MEP_MODEL_ETREE_LEAF) ? leaf_i_vid : i_vid); /* The TX ISDX of "this" is the RX ISDX of the "other" Up-MEP. RX port of this ISDX is not related to a front port */
            }
        }
    }

    if ((data->create.direction == MEP_MODEL_DOWN) && data->mep_highest) {  /* Highest Down-MEP */
        /* Must create level filtering TX rules */
        if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
        }

        mce.id = MCE_ID_CALC(data->instance, data->config.port, TX, LEVEL, ROOT);    /* ROOT indicate MEG-0 matching */
        mce.rule = MESA_MCE_RULE_TX;

        mce.key.vid = VID_OAM_MEG0;

        mce.action.port_list[data->config.port] = TRUE;
        mce.action.voe_idx = data->voe_idx;
        mce.action.evc_id  = data->create.flow_num;
        mce.action.evc_leaf = FALSE;
        mce.action.evc_counting = TRUE;
        mce.action.evc_cos      = TRUE;

        if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
            T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
            return(FALSE);
        }
        if (!data->mce_ids.push_back(mce.id)) {
            T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
            return(FALSE);
        }

        if (get_instance_up_mep_highest_of_two_any(MEP_MODEL_EVC, data->create.flow_num) != NULL) {  /* An Up-MEP highest of two exists */
            /* Must create level filtering TX rules */
            mce.id = MCE_ID_CALC(data->instance, data->config.port, TX, LEVEL, LEAF);    /* LEAF indicate MEG-1 matching */

            mce.key.vid = VID_OAM_MEG1;

            if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
                T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
                return(FALSE);
            }
            if (!data->mce_ids.push_back(mce.id)) {
                T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
                return(FALSE);
            }
        }
    }

    if ((data->create.direction == MEP_MODEL_DOWN) && data->block) {  /* Create the block rule after all "normal" OAM related rules */
        return (block_mce_config(data));
    }
    if ((data->create.direction == MEP_MODEL_DOWN) && (data->tst_config.enable || data->lb_config.enable)) { /* The Down VOE is not able to count received TST or LBR frame correctly Bugzilla#20381 */
        if (!tst_lbr_mce_config(data)) {
            T_NG(TRACE_GRP_MODEL, "tst_lbr_mce_config() failed");
            return(FALSE);
        }
    }

    T_DG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

static BOOL subscriber_domain_mce_config(MepModelInstance *data)
{
    u32                   rc=0;
    mesa_mce_port_info_t  mce_info;
    mesa_isdx_t           root_rx_isdx, leaf_rx_isdx;
    MepModelInstance      *idata;
    mesa_mce_t            mce;
    u32                   mask, vid, i_vid, leaf_vid, leaf_i_vid, port, i, next, mce_tagged;
    BOOL                  e_tree, down_mep, tx_rule_create, rev_rule_create, leaf=FALSE;
    mesa_port_list_t      uni;
    mesa_port_list_t      leaf_uni;
    mesa_port_list_t      nni;
    mesa_port_list_t      root_ports, leaf_ports, ports;

    T_DG(TRACE_GRP_MODEL, "Enter  Instance %u", data->instance);

    if (!evc_flow_info_get(data->create.flow_num, nni, uni, &vid, &i_vid, &e_tree, leaf_uni, &leaf_vid, &leaf_i_vid)) {
        return(FALSE);
    }
    root_ports.clear_all();
    leaf_ports.clear_all();
    for (i = 0; i < mep_caps.mesa_port_cnt; ++i) {
        if (uni[i] || nni[i])
            root_ports[i] = TRUE;   /* Ports in the ROOT VLAN */
        if ((uni[i] && !leaf_uni[i]) || nni[i])
            leaf_ports[i] = TRUE;   /* Ports in the LEAF VLAN */
    }
    leaf_rx_isdx = root_rx_isdx = MESA_MCE_ISDX_NONE;

    free_mce_resources(data);   /* Free all MCE resources */

    data->sub_inj_mce_id = MESA_MCE_ID_LAST;
    data->i_vid = i_vid;                    /* Internal ROOT VID for this flow */
    data->leaf_i_vid = leaf_i_vid;          /* Internal LEAD VID for this flow */
    data->e_tree = MEP_MODEL_ETREE_NONE;    /* E-TREE role */
    if (data->create.direction == MEP_MODEL_UP) {
        leaf = e_tree && leaf_uni[data->config.port];     /* Calculate if this is a LEAF Up-MEP/MIP */
        data->e_tree = e_tree ? (leaf ? MEP_MODEL_ETREE_LEAF : MEP_MODEL_ETREE_ROOT) : MEP_MODEL_ETREE_NONE;  /* Calculate E-TREE mode */
    }
    down_mep = ((data->create.direction == MEP_MODEL_DOWN) && (data->create.type == MEP_MODEL_MEP));

    memset(ports, 0, sizeof(ports));    /* Calculate the ports to create RX rules */
    if (data->create.direction == MEP_MODEL_DOWN) {
        ports[data->config.port] = TRUE;    /* Down-MIP only configure RX rules on residence port */
        if (data->p_port != P_PORT_NONE) {  /* Include any protecting port as X2 CLM rules can only match on one port - a Down-MEP needs a rule on both working and protecting port. */
            ports[data->p_port] = TRUE;
        }
    } else {
        ports = (leaf ? leaf_ports : root_ports);  /* Up-MEP/MIB potentially configure RX rules on both UNI and NNI port */
        ports[data->config.port] = FALSE;       /* Residence UNI port must not be in the 'ports' list. Extraction MCE rules are not created on residence port */
    }

    if (data->create.direction == MEP_MODEL_UP) {    /* This is an Up instance - MEP/MIP */
        /************ Create RX extraction MCE rules for all ports *************/
        for (port=0; port<mep_caps.mesa_port_cnt; ++port) {
            if (!nni[port]) {
                continue;
            }

            if ((data->create.type == MEP_MODEL_MEP) && (data->config.vid == MEP_MODEL_ALL_VID)) {    /* In case of match on all VIDs the OAM frames must be classified to a Subscriber MEG indicating VID for egress matching on UNI */
                T_DG(TRACE_GRP_MODEL, "Create Up-MEP VID ALL extraction MCE rules  port %u  ports[] %u  uni[] %u", port, ports.get(port), uni.get(port));

                /* ROOT RX rule - this is only supported in the ROOT VLAN */
                if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
                    T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
                }

                mce.id   = MCE_ID_CALC(data->instance, port, RX, EXTRACT, ROOT);
                mce.rule = MESA_MCE_RULE_RX;
                mce.key.second    = TRUE;

                mce.key.port_list[port] = TRUE;
                mce.key.first_tag.tagged    = MESA_VCAP_BIT_1;
                mce.key.first_tag.s_tagged  = MESA_VCAP_BIT_ANY;
                mce.key.first_tag.vid.value = vid;
                mce.key.first_tag.vid.mask  = 0xFFFF;
                mce.key.first_tag.pcp.mask  = 0x00;
                mce.key.first_tag.dei       = MESA_VCAP_BIT_ANY;
                mce.key.second_tag.tagged = MESA_VCAP_BIT_1;  /* Matching on double-tagged */
                mce.key.second_tag.s_tagged = MESA_VCAP_BIT_ANY;
                mce.key.second_tag.vid.value = 0;
                mce.key.second_tag.vid.mask = 0;
                mce.key.second_tag.pcp.mask = 0x00;
                mce.key.second_tag.dei = MESA_VCAP_BIT_ANY;
                mce.key.isdx      = MESA_MCE_ISDX_NONE;
                mce.key.mel.value = domain_level_value_mask_calc(data, port, &mask, FALSE);
                mce.key.mel.mask  = mask;

                mce.action.port_list = root_ports;
                mce.action.voe_idx = mep_highest_voe_idx(MEP_MODEL_EVC, MEP_MODEL_DOWN, port, data->create.flow_num);
                mce.action.oam_detect = MESA_MCE_OAM_DETECT_DOUBLE_TAGGED;
                mce.action.evc_id       = data->create.flow_num;
                mce.action.evc_leaf     = FALSE;
                mce.action.isdx         = MESA_MCE_ISDX_EVC;
                mce.action.evc_counting = TRUE;
                mce.action.independent_mel = TRUE;
                mce.action.pop_cnt      = 1;
                mce.action.vid          = VID_OAM_MEG1;    /* Classify to MEG-1 to indicate VID ALL Subscriber OAM */

                if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
                    T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
                    return(FALSE);
                }
                if (!data->mce_ids.push_back(mce.id)) {
                    T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
                    return(FALSE);
                }
                if ((rc = mesa_mce_port_info_get(NULL, mce.id, port, &mce_info)) != VTSS_RC_OK) {
                    T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed  instance %u  %s", data->instance, error_txt(rc));
                    return(FALSE);
                }
                set_instance_rx_isdx(data, mce_info.isdx, port, i_vid);

                mce_tagged = mce.id; /* The untagged MCE must be in front of the tagged MCE */
                mce.id   = MCE_ID_CALC(data->instance, port, RX_UNTAGGED, EXTRACT, ROOT);
                mce.key.second_tag.tagged = MESA_VCAP_BIT_0;  /* Matching on subscriber un-tagged - no second tag */

                mce.action.oam_detect = MESA_MCE_OAM_DETECT_SINGLE_TAGGED;

                if ((rc = mesa_mce_add(NULL, mce_tagged, &mce)) != VTSS_RC_OK) {
                    T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
                    return(FALSE);
                }
                if (!data->mce_ids.push_back(mce.id)) {
                    T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
                    return(FALSE);
                }
                if ((rc = mesa_mce_port_info_get(NULL, mce.id, port, &mce_info)) != VTSS_RC_OK) {
                    T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed  instance %u  %s", data->instance, error_txt(rc));
                    return(FALSE);
                }
                set_instance_rx_isdx(data, mce_info.isdx, port, i_vid);
            } else { /* MEP_MODEL_ALL_VID */
                /* Create a dummy injection rule - just to get the EVC ISDX as RX ISDX */
                if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
                    T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
                }

                mce.id = MCE_ID_CALC(data->instance, port, RX, INJECT, ROOT);
                mce.rule = MESA_MCE_RULE_UPINJ;

                mce.key.port_list[port] = TRUE;
                mce.key.isdx = MESA_MCE_ISDX_EVC;

                mce.action.evc_id       = data->create.flow_num;
                mce.action.evc_leaf     = FALSE;

                if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
                    T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
                    return(FALSE);
                }
                if (!data->mce_ids.push_back(mce.id)) {
                    T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
                    return(FALSE);
                }
                if ((rc = mesa_mce_port_info_get(NULL, mce.id, port, &mce_info)) != VTSS_RC_OK) {
                    T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed  instance %u  %s", data->instance, error_txt(rc));
                    return(FALSE);
                }
                set_instance_rx_isdx(data, mce_info.isdx, port, i_vid);

                if (e_tree && !leaf) {   /* This is E-TREE ROOT instance NNI extraction - LEAF VLAN must be handled */
                    mce.id = MCE_ID_CALC(data->instance, port, RX, INJECT, LEAF);
                    mce.action.evc_leaf     = TRUE;

                    if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
                        T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
                        return(FALSE);
                    }
                    if (!data->mce_ids.push_back(mce.id)) {
                        T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
                        return(FALSE);
                    }
                    if ((rc = mesa_mce_port_info_get(NULL, mce.id, port, &mce_info)) != VTSS_RC_OK) {
                        T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed  instance %u  %s", data->instance, error_txt(rc));
                        return(FALSE);
                    }
                    set_instance_rx_isdx(data, mce_info.isdx, port, leaf_i_vid);
                }
            }
        }   /* NNI Loop */
    } /* Up MEP/MIP */

    if ((data->create.direction == MEP_MODEL_DOWN) && (data->create.type == MEP_MODEL_MIP)) {
        T_DG(TRACE_GRP_MODEL, "Create Down-MIP extraction MCE rules  port %u", data->config.port);

        if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
        }

        mce.id   = MCE_ID_CALC(data->instance, data->config.port, RX, EXTRACT, ROOT);
        mce.rule = MESA_MCE_RULE_RX;
        mce.key.second    = TRUE;

        mce.key.port_list[data->config.port] = TRUE;
        mce.key.first_tag.tagged    = MESA_VCAP_BIT_1;
        mce.key.first_tag.s_tagged  = MESA_VCAP_BIT_ANY;
        mce.key.first_tag.vid.value = data->config.vid;
        mce.key.first_tag.vid.mask  = 0xFFFF;
        mce.key.first_tag.pcp.mask  = 0x00;
        mce.key.first_tag.dei       = MESA_VCAP_BIT_ANY;
        mce.key.second_tag.tagged   = MESA_VCAP_BIT_0;
        mce.key.isdx      = MESA_MCE_ISDX_NONE;
        mce.key.mel.value = domain_level_value_mask_calc(data, data->config.port, &mask, FALSE);
        mce.key.mel.mask  = mask;

        mce.action.port_list = root_ports;
        mce.action.voe_idx = mep_highest_voe_idx(MEP_MODEL_EVC, MEP_MODEL_UP, data->config.port, data->create.flow_num);
        mce.action.mip_idx = data->mip_idx;
        mce.action.isdx = MESA_MCE_ISDX_NEW;
        mce.action.oam_detect = MESA_MCE_OAM_DETECT_SINGLE_TAGGED;
        mce.action.evc_id       = data->create.flow_num;
        mce.action.evc_leaf     = leaf_uni[data->config.port] ? TRUE : FALSE;
        mce.action.evc_counting = TRUE;
        mce.action.evc_policing = TRUE;
        mce.action.independent_mel = TRUE;
        mce.action.pop_cnt      = 0;

        if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
            T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
            return(FALSE);
        }
        if (!data->mce_ids.push_back(mce.id)) {
            T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
            return(FALSE);
        }
        if ((rc = mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed  instance %u  %s", data->instance, error_txt(rc));
            return(FALSE);
        }
        set_instance_rx_isdx(data, mce_info.isdx, data->config.port, (leaf_uni[data->config.port] ? leaf_i_vid : i_vid));
        root_rx_isdx = mce_info.isdx;
    }

    if (data->create.direction == MEP_MODEL_UP) {   /* Up instance has extraction TX rules */
        /* ROOT TX rule */
        if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
        }

        mce.id = MCE_ID_CALC(data->instance, data->config.port, TX, EXTRACT, ROOT);
        mce.rule = MESA_MCE_RULE_TX;

        mce.key.vid = (data->config.vid != MEP_MODEL_ALL_VID) ? data->config.vid : VID_OAM_MEG1;

        mce.action.port_list[data->config.port] = TRUE;
        mce.action.voe_idx = (data->create.type == MEP_MODEL_MEP) ? data->voe_idx : mep_highest_voe_idx(MEP_MODEL_EVC, MEP_MODEL_UP, data->config.port, data->create.flow_num);
        mce.action.mip_idx = data->mip_idx;
        mce.action.evc_id  = data->create.flow_num;
        mce.action.evc_leaf = FALSE;    /* Already default */
        mce.action.evc_counting = TRUE;
        mce.action.evc_cos      = TRUE;
        mce.action.pipeline = MESA_MCE_PIPELINE_EXT_SAT;
        mce.action.independent_mel = (data->create.type == MEP_MODEL_MEP) ? FALSE : TRUE;  /* MIP must hit any EVC Up VOE with independent MEL */

        if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
            T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
            return(FALSE);
        }
        if (!data->mce_ids.push_back(mce.id)) {
            T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
            return(FALSE);
        }

        if (e_tree && !leaf) {   /* This is E-TREE ROOT Up-MEP - LEAF VLAN must be handled */
            /* LEAF TX rule */
            mce.id = MCE_ID_CALC(data->instance, data->config.port, TX, EXTRACT, LEAF);

            mce.action.evc_leaf = TRUE;

            if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
                T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
                return(FALSE);
            }
            if (!data->mce_ids.push_back(mce.id)) {
                T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
                return(FALSE);
            }
        }
    } /* Extraction TX rule */

    /************ Create injection MCE rules *************/
    if ((data->create.direction == MEP_MODEL_UP) && data->config.sat) {   /* SAT Up-MEP */
        /* Create SAT UNI ingress frame discard rule */
        if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
        }

        mce.id = MCE_ID_CALC(data->instance, data->config.port, RX, DISCARD, ROOT);
        mce.rule = MESA_MCE_RULE_RX;

        mce.key.port_list[data->config.port] = TRUE;
        mce.key.service_detect    = TRUE;
        mce.key.first_tag.tagged  = MESA_VCAP_BIT_ANY;
        mce.key.second_tag.tagged = MESA_VCAP_BIT_ANY;

        mce.action.evc_id      = data->create.flow_num;
        mce.action.forward_sel = MESA_MCE_FORWARD_DISABLE;  /* This will make this rule to be only hit by frame ingress on port - not masqueraded */

        if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
            T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
            return(FALSE);
        }
        if (!data->mce_ids.push_back(mce.id)) {
            T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
            return(FALSE);
        }

        /* Create SAT DM injection rule. This is to avoid DM into the policer */
        if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
        }

        mce.id = MCE_ID_CALC(data->instance, data->config.port, TX_DM, INJECT, ROOT);
        mce.rule = MESA_MCE_RULE_UPINJ;

        mce.key.port_list[data->config.port] = TRUE;
        mce.key.isdx = MESA_MCE_ISDX_NEW;

        memcpy(mce.action.port_list, leaf ? leaf_ports : root_ports, sizeof(mce.action.port_list));
        mce.action.voe_idx      = data->voe_idx;
        mce.action.evc_id       = data->create.flow_num;
        mce.action.evc_leaf     = leaf;

        if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
            T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
            return(FALSE);
        }
        if (!data->mce_ids.push_back(mce.id)) {
            T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
            return(FALSE);
        }
        if ((rc = mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed  instance %u  %s", data->instance, error_txt(rc));
            return(FALSE);
        }
        data->tx_sat_dm_isdx = mce_info.isdx;
    } /* SAT Injection rules */

    if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
    }

    tx_rule_create = TRUE;  /* As default an injection rule is created */

    if (data->create.direction == MEP_MODEL_UP) { /* Up-MEP/MIP injection */
        if (data->create.type == MEP_MODEL_MEP) {   /* Up-MEP - injection RX rule. This is a "double" rule that will be hit by frame ingress on port but also the ISDX will be used for injection */
            mce.id = MCE_ID_CALC(data->instance, data->config.port, RX, INJECT, leaf ? LEAF : ROOT);
            mce.rule = MESA_MCE_RULE_RX;
            mce.key.second              = TRUE;

            mce.key.port_list[data->config.port] = TRUE;
            mce.key.first_tag.tagged    = MESA_VCAP_BIT_1;
            mce.key.first_tag.s_tagged  = MESA_VCAP_BIT_ANY;
            mce.key.first_tag.vid.value = data->config.vid;
            mce.key.first_tag.vid.mask  = (data->config.vid == MEP_MODEL_ALL_VID) ? 0 : 0xFFFF;
            mce.key.first_tag.pcp.mask  = 0x00;
            mce.key.first_tag.dei       = MESA_VCAP_BIT_ANY;
            mce.key.second_tag.tagged   = MESA_VCAP_BIT_0;
            mce.key.service_detect      = data->config.sat ? FALSE : TRUE;  /* This set TRUE in order to point also service frames in the Subscriber flow to the VOE for LM counting. Should not be done in case of SAT, as customer simulated frames will hit on its way to AFI and rule changes PT. There are use of LM counters during SAT anyway. */

            memcpy(mce.action.port_list, leaf ? leaf_ports : root_ports, sizeof(mce.action.port_list));
            mce.action.voe_idx      = data->voe_idx;
            mce.action.isdx         = MESA_MCE_ISDX_NEW;
            mce.action.oam_detect   = MESA_MCE_OAM_DETECT_SINGLE_TAGGED;
            mce.action.evc_id       = data->create.flow_num;
            mce.action.evc_leaf     = leaf;
            mce.action.evc_counting = TRUE;
            mce.action.evc_policing = TRUE;
            mce.action.pipeline     = MESA_MCE_PIPELINE_INJ_OU_VOE;
            data->sub_inj_mce_id    = mce.id;
        } else {    /* Up-MIP - injection UPINJ rule. This is a dummy as injection is using the EVC ISDX */
            /* Up-MIP injection is done using the EVC ISDX. */
            if ((idata = get_instance_with_reverse_isdx(MEP_MODEL_EVC_SUB, data->create.flow_num, data->config.vid, MEP_MODEL_DOWN, data->config.port)) != NULL) {
                data->tx_isdx = idata->tx_rev_isdx;
                tx_rule_create = FALSE;  /* When a Subscriber Down instance has a valid reverse TX ISDX we will share that. So no rules need to be created */
            }
            mce.id = MCE_ID_CALC(data->instance, data->config.port, RX, INJECT, leaf ? LEAF : ROOT);
            mce.rule = MESA_MCE_RULE_UPINJ;

            mce.key.port_list[data->config.port] = TRUE;
            mce.key.isdx = MESA_MCE_ISDX_EVC;

            mce.action.evc_id       = data->create.flow_num;
            mce.action.evc_leaf     = leaf;
        }
    } else if (data->create.direction == MEP_MODEL_DOWN) { /* Down MEP/MIP injection TX rule for residence port */
        /* No rule is required as injection is done on subscriber classified VID into the EVC ES0 */
        tx_rule_create = FALSE;
    }
    if (tx_rule_create) {
        if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
            T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
            return(FALSE);
        }
        if (!data->mce_ids.push_back(mce.id)) {
            T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
            return(FALSE);
        }
        if ((rc = mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed  instance %u  %s", data->instance, error_txt(rc));
            return(FALSE);
        }
        data->tx_isdx = mce_info.isdx;

        if ((data->create.direction == MEP_MODEL_UP) && (data->create.type == MEP_MODEL_MEP)) {   /* Up-MEP */
            mce.id = MCE_ID_CALC(data->instance, data->config.port, TX_LOOP, INJECT, ROOT);    /* Loop RX rule on TX ISDX. This is also generated as loop ISDX by VOE */
            mce.rule = MESA_MCE_RULE_RX;
            mce.key.second            = TRUE;

            mce.key.first_tag.tagged  = MESA_VCAP_BIT_ANY;
            mce.key.second_tag.tagged = MESA_VCAP_BIT_ANY;
            mce.key.isdx              = data->tx_isdx;
            mce.key.masqueraded       = FALSE;
            mce.key.looped            = TRUE;   /* This will make this rule only hit by looped frames */

            mce.action.isdx       = MESA_MCE_ISDX_NONE;
            mce.action.oam_detect = MESA_MCE_OAM_DETECT_DOUBLE_TAGGED;
            mce.action.pipeline   = MESA_MCE_PIPELINE_NONE;

            if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
                T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
                return(FALSE);
            }
            if (!data->mce_ids.push_back(mce.id)) {
                T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
                return(FALSE);
            }
        }
    } /* tx_rule_create */

    if (data->create.direction == MEP_MODEL_UP) { /* Subscriber Up-MEP/MIP. In case a terminal TT-LOOP is created in the same subscriber flow on the same port, the OAM frame copied to CPU with the EVC ISDX of that port.  */
        set_instance_rx_isdx(data, data->tx_isdx, data->config.port, (leaf_uni[data->config.port] ? leaf_i_vid : i_vid));
    }

    /********* Create reverse injection rules **********/
    rev_rule_create = TRUE;  /* As default a reverse injection rule is created */

    if (data->create.type == MEP_MODEL_MIP) {   /* Only for MIP */
        if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
        }

        if (data->create.direction == MEP_MODEL_DOWN) { /* Down-MIP reverse UPINJ rule */
            if ((idata = get_instance_with_tx_isdx(MEP_MODEL_EVC_SUB, data->create.flow_num, data->config.vid, MEP_MODEL_UP, data->config.port)) != NULL) {
                data->tx_rev_isdx = idata->tx_isdx;
                rev_rule_create = FALSE;  /* When a Subscriber Up instance has a valid TX ISDX we will share that. So no rules need to be created */
            }
            mce.id = MCE_ID_CALC(data->instance, data->config.port, RX, INJECT, leaf ? LEAF : ROOT);
            mce.rule = MESA_MCE_RULE_UPINJ;

            mce.key.port_list[data->config.port] = TRUE;
            mce.key.isdx = MESA_MCE_ISDX_EVC;

            mce.action.evc_id          = data->create.flow_num;
            mce.action.evc_leaf        = leaf;
        } else if (data->create.direction == MEP_MODEL_UP) { /* reverse TX rule */
            /* No rule is required as injection is done on subscriber classified VID into the EVC ES0 */
            rev_rule_create = FALSE;
        }
        if (rev_rule_create) {
            if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
                T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
                return(FALSE);
            }
            if (!data->mce_ids.push_back(mce.id)) {
                T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
                return(FALSE);
            }
            if ((rc = mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info)) != VTSS_RC_OK) {
                T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed  instance %u  %s", data->instance, error_txt(rc));
                return(FALSE);
            }
            data->tx_rev_isdx = mce_info.isdx;
        }  /* Injection rule is created */
    } /* MIP */

    /*************** Give injection ISDX as RX ISDX to hit "other" Up VOE/MIP *******************/
    if ((data->create.direction == MEP_MODEL_UP) || ((data->create.direction == MEP_MODEL_DOWN) && (data->create.type == MEP_MODEL_MIP))) {   /* This instance has injection or reverse injection ISDX. */
        next = 0;
        while ((idata = get_instance_domain_flow(MEP_MODEL_EVC_SUB, data->create.flow_num, &next)) != NULL) { /* Get all MEP/MIPs in this flow */
            if ((idata != data) && (idata->oper_state == MEP_MODEL_OPER_UP) && (idata->config.vid == data->config.vid) &&
                (!e_tree || !leaf || !leaf_uni[idata->config.port]) &&
                (idata->create.direction == MEP_MODEL_UP) && (idata->config.level == data->config.level) && (idata->config.port != data->config.port)) {     /* Found an operational Up-MEP/MIP on this level on another port that is not "this" */
                set_instance_rx_isdx(idata, (data->create.direction == MEP_MODEL_UP) ? data->tx_isdx : data->tx_rev_isdx, mep_caps.mesa_port_cnt, (data->e_tree == MEP_MODEL_ETREE_LEAF) ? leaf_i_vid : i_vid); /* The injection ISDX of "this" is the RX ISDX of the "other" Up-MEP/MIP. RX port of this ISDX is not related to a front port */

                if ((data->create.direction == MEP_MODEL_DOWN) && (data->create.type == MEP_MODEL_MIP))  {   /* This Down-MIP instance has RX rule. */
                    set_instance_rx_isdx(idata, root_rx_isdx, mep_caps.mesa_port_cnt, (data->e_tree == MEP_MODEL_ETREE_LEAF) ? leaf_i_vid : i_vid); /* The RX ISDX of "this" is the RX ISDX of the "other" Up-MEP/MIP */
                }
            }
        }
    }

    if ((data->create.direction == MEP_MODEL_DOWN) && data->block) {  /* Create the block rule after all "normal" OAM related rules */
        return(block_mce_config(data));
    }
    if ((data->create.direction == MEP_MODEL_DOWN) && (data->tst_config.enable || data->lb_config.enable)) { /* The Down VOE is not able to count received TST or LBR frame correctly Bugzilla#20381 */
        if (!tst_lbr_mce_config(data)) {
            T_NG(TRACE_GRP_MODEL, "tst_lbr_mce_config() failed");
            return(FALSE);
        }
    }

    T_DG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

static BOOL vlan_domain_mce_config(MepModelInstance *data)
{
    u32                   rc=0;
    mesa_mce_port_info_t  mce_info;
    MepModelInstance       *idata;
    mesa_mce_t            mce;
    u32                   mask, port, next;
    mesa_port_list_t      nni;
    mesa_port_list_t      ports;

    T_DG(TRACE_GRP_MODEL, "Enter  instance %u", data->instance);

    free_mce_resources(data);   /* Free all MCE resources */

    ports.clear_all();
    nni.clear_all();
    if (data->create.direction == MEP_MODEL_DOWN) {
        ports[data->config.port] = TRUE;    /* Down-MEP only configure MCE on residence port */
        if (data->p_port != P_PORT_NONE) {  /* Include any protecting port as X2 CLM rules can only match on one port - a Down-MEP needs a rule on both working and protecting port. */
            ports[data->p_port] = TRUE;
        }
    } else {
        if (!vlan_flow_info_get(data->create.flow_num, nni)) {
            return(FALSE);
        }
        memcpy(ports, nni, sizeof(ports));  /* Up-MEP configure MCE on all NNI port except residence port */
        ports[data->config.port] = FALSE;
    }

    data->i_vid = data->create.flow_num;    /* Internal VID for this flow */

    /************ Create extraction MCE rules for all ports *************/
    for (port=0; port<mep_caps.mesa_port_cnt; ++port) {
        if (!ports[port]) {
            continue;
        }
        if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed %s", error_txt(rc));
        }

        /* RX rule */
        mce.id   = MCE_ID_CALC(data->instance, port, RX, EXTRACT, ROOT);
        mce.rule = MESA_MCE_RULE_RX;
        mce.key.second    = TRUE;

        mce.key.port_list[port] = TRUE;
        mce.key.first_tag.tagged    = MESA_VCAP_BIT_1;
        mce.key.first_tag.s_tagged  = MESA_VCAP_BIT_ANY;
        mce.key.first_tag.vid.value = data->i_vid;
        mce.key.first_tag.vid.mask  = 0xFFFF;
        mce.key.first_tag.pcp.mask  = 0x00;
        mce.key.first_tag.dei       = MESA_VCAP_BIT_ANY;
        mce.key.second_tag.tagged   = MESA_VCAP_BIT_0;
        mce.key.isdx      = MESA_MCE_ISDX_NONE;
        mce.key.mel.value = domain_level_value_mask_calc(data, port, &mask, FALSE);
        mce.key.mel.mask  = mask;

        port_list_in_vlan_domain(data->i_vid, mce.action.port_list);   /* This is only to enable forwarding of any RAPS frame to other ring port */

        mce.action.voe_idx = (data->create.direction == MEP_MODEL_DOWN) ? data->voe_idx : mep_highest_voe_idx(MEP_MODEL_VLAN, MEP_MODEL_DOWN, port, data->i_vid);
        mce.action.isdx = (data->create.direction == MEP_MODEL_DOWN) ? MESA_MCE_ISDX_NEW : up_rx_isdx(data, port, data->i_vid);
        mce.action.vid = data->i_vid;
        mce.action.oam_detect = MESA_MCE_OAM_DETECT_SINGLE_TAGGED;
        mce.action.independent_mel = (data->create.direction == MEP_MODEL_DOWN) ? FALSE : (get_instance_mep_highest(MEP_MODEL_VLAN, data->i_vid, MEP_MODEL_DOWN, port) != NULL) ? FALSE : TRUE;  /* RX rule for Up-MEP with no Down-MEP on port must set independent_mel TRUE to avoid hitting Port VOE ingress */
        mce.action.pop_cnt = 1;

        if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
            T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed %s", error_txt(rc));
            return(FALSE);
        }
        if (!data->mce_ids.push_back(mce.id)) {
            T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed");
            return(FALSE);
        }
        if ((rc = mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed %s", error_txt(rc));
            return(FALSE);
        }
        if (data->create.direction == MEP_MODEL_DOWN) {  /* This is a Down-MEP - save the MCE ID to be used when TST is enabled. A special TST matching MCE must be created and inserted in front of this MCE in order to count */
            data->tst_lbr_rx_mce_id = mce.id;
        }
        set_instance_rx_isdx(data, mce_info.isdx, port, data->i_vid);

        /* TX rule */
        if (data->create.direction == MEP_MODEL_UP) {   /* Only Up-MEP has extraction TX rule */
            if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
                T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed %s", error_txt(rc));
            }

            mce.id = MCE_ID_CALC(data->instance, port, TX, EXTRACT, ROOT);
            mce.rule = MESA_MCE_RULE_TX;

            mce.key.isdx = mce_info.isdx;

            mce.action.port_list[data->config.port] = TRUE;
            mce.action.voe_idx = data->voe_idx;

            if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
                T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed %s", error_txt(rc));
                return(FALSE);
            }
            if (!data->mce_ids.push_back(mce.id)) {
                T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed");
                return(FALSE);
            }
        } /* Extraction TX rule */
    } /* for to - Extraction port loop */

    /************ Create injection MCE rules *************/
    if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed %s", error_txt(rc));
    }

    if (data->create.direction == MEP_MODEL_UP) { /* UPINJ rule */
        mce.id = MCE_ID_CALC(data->instance, data->config.port, RX, INJECT, ROOT);
        mce.rule = MESA_MCE_RULE_UPINJ;

        mce.key.port_list[data->config.port] = TRUE;
        mce.key.isdx = MESA_MCE_ISDX_NEW;

        for (port = 0; port < mep_caps.mesa_port_cnt; port++) {
            mce.action.port_list[port] = nni[port];
        }
        mce.action.voe_idx      = data->voe_idx;
    } else if (data->create.direction == MEP_MODEL_DOWN) { /* TX rule for residence port */
        mce.id = MCE_ID_CALC(data->instance, data->config.port, TX, INJECT, ROOT);
        mce.rule = MESA_MCE_RULE_TX;

        mce.key.isdx = MESA_MCE_ISDX_NEW;

        mce.action.port_list[data->config.port] = TRUE;
        mce.action.voe_idx      = data->voe_idx;
    }
    if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
        T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed %s", error_txt(rc));
        return(FALSE);
    }
    if (!data->mce_ids.push_back(mce.id)) {
        T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed");
        return(FALSE);
    }
    if ((rc = mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed %s", error_txt(rc));
        return(FALSE);
    }
    data->tx_isdx = mce_info.isdx;

    /* TX rule for protected port (use the same ISDX as for working port) */
    if ((data->create.direction == MEP_MODEL_DOWN) && (data->p_port != P_PORT_NONE)) {
        mce.id = MCE_ID_CALC(data->instance, data->p_port, TX, INJECT, ROOT);
        mce.key.isdx = mce_info.isdx;
        mce.action.port_list[data->config.port] = FALSE;
        mce.action.port_list[data->p_port] = TRUE;

        if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
            T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed %s", error_txt(rc));
            return(FALSE);
        }
        if (!data->mce_ids.push_back(mce.id)) {
            T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed");
            return(FALSE);
        }
    }

    if ((data->create.type == MEP_MODEL_MEP) && (data->create.direction == MEP_MODEL_UP)) {   /* In case of more Up-MEP on same level on other ports, TX rules must created to hit related VOE */
        next = 0;
        while ((idata = get_instance_domain_flow(MEP_MODEL_VLAN, data->i_vid, &next)) != NULL) { /* Get all MEPs in this flow */
            if ((idata != data) && (idata->oper_state == MEP_MODEL_OPER_UP) && (idata->create.direction == MEP_MODEL_UP) && (idata->config.level == data->config.level))  { /* Found an operational Up-MEP on this level that is not "this" */
                /************ Create TX MCE rules *************/
                if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
                    T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
                }

                mce.id = MCE_ID_CALC(data->instance, idata->config.port, TX, INJECT, ROOT);
                mce.rule = MESA_MCE_RULE_TX;

                mce.key.isdx = data->tx_isdx;

                mce.action.port_list[idata->config.port] = TRUE;
                mce.action.voe_idx = idata->voe_idx;

                if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
                    T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
                    return(FALSE);
                }
                if (!data->mce_ids.push_back(mce.id)) {
                    T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
                    return(FALSE);
                }
                set_instance_rx_isdx(idata, data->tx_isdx, mep_caps.mesa_port_cnt, data->i_vid); /* The TX ISDX of "this" is the RX ISDX of the "other" Up-MEP. RX port of this ISDX is not related to a front port */
            }
        }
    }

    if ((data->create.direction == MEP_MODEL_DOWN) && data->block) {  /* Create the block rule after all "normal" OAM related rules */
        return(block_mce_config(data));
    }
    if ((data->create.direction == MEP_MODEL_DOWN) && (data->tst_config.enable || data->lb_config.enable)) { /* The Down VOE is not able to count received TST or LBR frame correctly Bugzilla#20381 */
        if (!tst_lbr_mce_config(data)) {
            T_NG(TRACE_GRP_MODEL, "tst_lbr_mce_config() failed");
            return(FALSE);
        }
    }

    return(TRUE);
}

//static BOOL evc_domain_meg_mce_config(u32 flow_id)    /* This function is not completed as a configuration without Up-MEP on UNI is considered an invalid configuration */
//{
//    u32                   next, vid=0;
//    MepModelInstance      *idata;
//    BOOL                  down_mep_created=FALSE;
//    mesa_port_list_t      down_mep_ports;
//
//    /* If no Up-MEP is created in this flow and there is Down-MEP on only some NNI, EVC OAM MEG identification RX rule must be created on NNI without Down-MEP */
//
//    /* Find an Up-MEP on any port - in EVC domain */
//    memset(down_mep_ports, 0, sizeof(down_mep_ports));
//    next = 0;
//    while ((idata = get_instance_domain_flow(MEP_MODEL_EVC, flow_id, &next)) != NULL) { /* Get all MEPs in this flow */
//        if ((idata->create.type == MEP_MODEL_MEP) && (idata->create.direction == MEP_MODEL_UP) && (idata->oper_state == MEP_MODEL_OPER_UP)) {
//            return TRUE;    /* Any Up-MEP has created this rule */
//        }
//        if ((idata->create.type == MEP_MODEL_MEP) && (idata->create.direction == MEP_MODEL_DOWN) && (idata->oper_state == MEP_MODEL_OPER_UP)) {
//            down_mep_created = TRUE;
//            down_mep_ports[idata->config.port] = TRUE;
//        }
//    }
//
//    for (u32 i=0; i<MESA_CAP(MESA_CAP_PORT_CNT); ++i) {    /* On all with no Down-MEP a EVC OAM MEG identification RX rule must be created */
//        if (!down_mep_ports[i]) {
//            if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
//                T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed  instance %u  %s", data->instance, error_txt(rc));
//            }
//
//            /* ROOT RX rule */
//            mce.id   = MCE_ID_CALC(data->instance, port, RX, EXTRACT, ROOT);
//            mce.rule = MESA_MCE_RULE_RX;
//
//            mce.key.port_list[port] = TRUE;
//            mce.key.first_tag.tagged    = MESA_VCAP_BIT_1;
//            mce.key.first_tag.s_tagged  = MESA_VCAP_BIT_ANY;
//            mce.key.first_tag.vid.value = vid;
//            mce.key.first_tag.vid.mask  = 0xFFFF;
//            mce.key.first_tag.pcp.mask  = 0x00;
//            mce.key.first_tag.dei       = MESA_VCAP_BIT_ANY;
//            mce.key.second_tag.tagged   = MESA_VCAP_BIT_0;
//            mce.key.isdx      = MESA_MCE_ISDX_NONE;
//            mce.key.second    = TRUE;
//            mce.key.mel.value = (0x1 << 7) - 1;
//            mce.key.mel.mask  = 0xFF;
//
//            mce.action.port_list = root_ports;
//            mce.action.voe_idx = (data->create.direction == MEP_MODEL_DOWN) ? data->voe_idx : mep_highest_voe_idx(MEP_MODEL_EVC, MEP_MODEL_DOWN, port, data->create.flow_num);
//            mce.action.oam_detect   = MESA_MCE_OAM_DETECT_SINGLE_TAGGED;
//            mce.action.evc_id       = data->create.flow_num;
//            mce.action.evc_counting = (data->create.direction == MEP_MODEL_DOWN) ? FALSE : TRUE;
//            mce.action.evc_cos      = TRUE;
//            mce.action.pop_cnt      = nni[port] ? 1 : 0;
//            mce.action.vid          = VID_OAM_MEG0;
//            mce.action.isdx         = MESA_MCE_ISDX_NEW;
//
//        if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
//            T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed  instance %u  %s", data->instance, error_txt(rc));
//            return(FALSE);
//        }
//        if (!data->mce_ids.push_back(mce.id)) {
//            T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed  instance %u", data->instance);
//            return(FALSE);
//        }
//        if ((rc = mesa_mce_port_info_get(NULL, mce.id, port, &mce_info)) != VTSS_RC_OK) {
//            T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed  instance %u  %s", data->instance, error_txt(rc));
//            return(FALSE);
//        }
//        if (data->create.direction == MEP_MODEL_DOWN) {  /* This is a Down-MEP - save the MCE ID to be used when TST is enabled. A special TST matching MCE must be created and inserted in front of this MCE in order to count */
//            data->tst_lbr_rx_mce_id = mce.id;
//        }
//        set_instance_rx_isdx(data, mce_info.isdx, port, i_vid);
//        root_rx_isdx = mce_info.isdx;
//    }
//
//    return TRUE;
//}

static BOOL vlan_domain_leak_mce_config(u32 flow)
{
    u32                   rc=0, next, port;
    mesa_mce_t            mce;
    mesa_port_list_t      ports;
    mesa_port_list_t      down_mep_ports;
    MepModelInstance       *idata, *odata;
    mesa_mce_id_t         vlan_common_mce_id = MESA_MCE_ID_LAST;

    T_DG(TRACE_GRP_MODEL, "Enter  flow %u", flow);

    /* Leaking/blocking rules must be created in this seperate function, as they must be in the TCAM after any OAM to VOE mapping rules */

    memset(down_mep_ports, 0, sizeof(down_mep_ports));
    if (!vlan_flow_info_get(flow, ports)) {
        T_NG(TRACE_GRP_MODEL, "vlan_flow_info_get failed  flow %u", flow);
        return(TRUE);   /* If VLAN does not exist we just do not create any OAM marking rule */
    }

    odata = NULL;

    next = 0;
    while ((idata = get_instance_domain_flow(MEP_MODEL_VLAN, flow, &next)) != NULL) { /* Get all MEPs in this flow */
        if ((idata->oper_state != MEP_MODEL_OPER_UP) || (idata->create.type != MEP_MODEL_MEP)) {   /* Only configure operational instances */
            continue;
        }

        if (idata->create.direction == MEP_MODEL_DOWN) { /* There is a Down-MEP on "this" port */
            T_DG(TRACE_GRP_MODEL, "Down-MEP  instance %u  port %u", idata->instance, idata->config.port);
            down_mep_ports[idata->config.port] = TRUE;
        }

        /************ Create leaking/blocking MCE rules per MEP *************/
        odata = idata;  /* This instance is going to "own" the common MCE rules */

        if (idata == get_instance_mep_highest_port(MEP_MODEL_VLAN, idata->i_vid, idata->config.port)) {   /* MEP on highest level must have leaking/blocking RX rule */
            if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
                T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed %s", error_txt(rc));
            }

            /* Create leaking/blocking RX rule on residence port. */
            mce.id = MCE_ID_CALC(idata->instance, idata->config.port, RX, EXTRACT, LEAF);   /* LEAF is used to distinguish from "normal" VLAN rules using ROOT */
            mce.rule = MESA_MCE_RULE_RX;
            mce.key.second    = TRUE;
            mce.order = MESA_MCE_ORDER_3;   /* This is a service frame entry that must be after any OAM or TT_LOOP entries */

            mce.key.port_list[idata->config.port] = TRUE;
            mce.key.first_tag.tagged    = MESA_VCAP_BIT_1;
            mce.key.first_tag.s_tagged  = MESA_VCAP_BIT_ANY;
            mce.key.first_tag.vid.value = idata->i_vid;
            mce.key.first_tag.vid.mask  = 0xFFFF;
            mce.key.first_tag.pcp.mask  = 0x00;
            mce.key.first_tag.dei       = MESA_VCAP_BIT_ANY;
            mce.key.second_tag.tagged   = MESA_VCAP_BIT_1;
            mce.key.second_tag.s_tagged = MESA_VCAP_BIT_ANY;
            mce.key.isdx                = MESA_MCE_ISDX_NONE;
            mce.key.service_detect      = TRUE;

            for (port = 0; port < mep_caps.mesa_port_cnt; port++) {
                mce.action.port_list[port] = ports[port];
            }
            mce.action.voe_idx    = idata->voe_idx;
            mce.action.isdx       = MESA_MCE_ISDX_NEW;
            mce.action.oam_detect = MESA_MCE_OAM_DETECT_SINGLE_TAGGED;
            mce.action.pop_cnt    = 1;
            mce.action.vid        = flow;

            if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
                T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed %s", error_txt(rc));
                return(FALSE);
            }
            if (!idata->mce_ids.push_back(mce.id)) {
                T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed");
                return(FALSE);
            }
            if (vlan_common_mce_id == MESA_MCE_ID_LAST) {
                vlan_common_mce_id = mce.id;
            }

            /* Create leaking/blocking TX rule on residence port. */

            if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
                T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed %s", error_txt(rc));
            }

            mce.id = MCE_ID_CALC(idata->instance, idata->config.port, TX, EXTRACT, LEAF);   /* LEAF is used to distinguish from "normal" VLAN rules using ROOT */
            mce.rule = MESA_MCE_RULE_TX;
            mce.order = MESA_MCE_ORDER_3;   /* This is a service frame entry that must be after any OAM or TT_LOOP entries */

            mce.key.vid = idata->i_vid;

            mce.action.port_list[idata->config.port] = TRUE;
            mce.action.voe_idx = idata->voe_idx;

            if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
                T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed %s", error_txt(rc));
                return(FALSE);
            }
            if (!idata->mce_ids.push_back(mce.id)) {
                T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed");
                return(FALSE);
            }
        } /* MEP on highest level */
    } /* While - Get all MEPs in this flow */

    if (odata != NULL) {    /* At least one instance was found */
        /************ Create common leaking RX MCE rules *************/

        if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
            T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed %s", error_txt(rc));
        }

        /* Common OAM marking RX rule to enable hitting any MEPs on egress preventing leaking and enabling blocking */
        mce.id   = MCE_ID_CALC(odata->instance, odata->config.port, RX, DISCARD, LEAF);     /* DISCARD is used to distinguish from RX rule on "highest" */
        mce.rule = MESA_MCE_RULE_RX;
        mce.key.second              = TRUE;
        mce.order = MESA_MCE_ORDER_3;   /* This is a service frame entry that must be after any OAM or TT_LOOP or blocking entries */

        for (port = 0; port < mep_caps.mesa_port_cnt; port++) {
            mce.key.port_list[port] = ports[port];    /* All ports in this VLAN */
        }
        mce.key.first_tag.tagged    = MESA_VCAP_BIT_1;
        mce.key.first_tag.s_tagged  = MESA_VCAP_BIT_ANY;
        mce.key.first_tag.vid.value = flow;
        mce.key.first_tag.vid.mask  = 0xFFFF;
        mce.key.first_tag.pcp.mask  = 0x00;
        mce.key.first_tag.dei       = MESA_VCAP_BIT_ANY;
        mce.key.second_tag.tagged   = MESA_VCAP_BIT_1;
        mce.key.second_tag.s_tagged = MESA_VCAP_BIT_ANY;
        mce.key.isdx                = MESA_MCE_ISDX_NONE;
        mce.key.service_detect      = TRUE;

        mce.action.port_list = mce.key.port_list;  /* All ports in this VLAN */
        mce.action.oam_detect      = MESA_MCE_OAM_DETECT_SINGLE_TAGGED;  /* Mark as  OAM */
        mce.action.independent_mel = TRUE;  /* Must set independent_mel TRUE to avoid hitting Port VOE ingress */
        mce.action.isdx            = MESA_MCE_ISDX_NEW;
        mce.action.vid             = flow;

        if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
            T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed %s", error_txt(rc));
            return(FALSE);
        }
        if (!odata->mce_ids.push_back(mce.id)) {
            T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed");
            return(FALSE);
        }
        if (vlan_common_mce_id == MESA_MCE_ID_LAST) {
            vlan_common_mce_id = mce.id;
        }

        /************ Create common independent MEL TX MCE rules *************/
        for (u32 i=0; i<mep_caps.mesa_port_cnt; ++i) {    /* On all ports in this VLAN without a Down-MEP a ES0 entry is required to mark as independent MEL in order to avoid Port VOE doing level filtering */
            if (ports[i] && !down_mep_ports[i]) {
                T_DG(TRACE_GRP_MODEL, "Independent MEL ES0  port %u  vid %u", i, flow);

                if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
                    T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed %s", error_txt(rc));
                }

                mce.id = MCE_ID_CALC(odata->instance, i, TX_SUB, EXTRACT, LEAF);   /* LEAF is used to distinguish from "normal" VLAN rules using ROOT */
                                                                                   /* This is ugly - had to use 'TX_SUB' to make this unique. */
                mce.rule = MESA_MCE_RULE_TX;

                mce.key.vid = flow;

                mce.action.port_list[i] = TRUE;
                mce.action.independent_mel = TRUE;

                if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
                    T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed %s", error_txt(rc));
                    return(FALSE);
                }
                if (!odata->mce_ids.push_back(mce.id)) {
                    T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed");
                    return(FALSE);
                }
            }
        }
    }  /* At least one instance was found */

    next = 0;
    while ((idata = get_instance_domain_flow(MEP_MODEL_VLAN, flow, &next)) != NULL) { /* Get all MEPs in this flow */
        if ((idata->oper_state != MEP_MODEL_OPER_UP) || (idata->create.type != MEP_MODEL_MEP)) {   /* Only configure operational instances */
            continue;
        }
        idata->vlan_common_mce_id = vlan_common_mce_id;     /* All MEPs in this VLAN flow must know the first common MCE ID in the flow */
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

static BOOL port_domain_mce_config(MepModelInstance *data)
{
    u32                   rc=0;
    mesa_mce_port_info_t  mce_info;
    mesa_mce_t            mce;

    T_NG(TRACE_GRP_MODEL, "Enter");

    free_mce_resources(data);   /* Free all MCE resources */
    del_instance_rx_isdx(data); /* 'delete' previously allocated RX ISDX for this instance */

    /******************************* Create extraction MCE rule ***************************/
    if ((rc = mesa_mce_init(NULL, &mce)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_mce_init() failed %s", error_txt(rc));
    }

    mce.id = MCE_ID_CALC(data->instance, data->config.port, RX, EXTRACT, ROOT);
    mce.rule = MESA_MCE_RULE_RX;
    mce.key.second = TRUE;
    mce.order = MESA_MCE_ORDER_1;   /* Port domain MCEs have higher hit order than other domains */

    mce.key.port_list[data->config.port] = TRUE;
    mce.key.first_tag.tagged = (data->config.vid != 0) ? MESA_VCAP_BIT_1 : MESA_VCAP_BIT_0;
    mce.key.first_tag.s_tagged = MESA_VCAP_BIT_ANY;
    mce.key.first_tag.vid.value = data->config.vid;
    mce.key.first_tag.vid.mask = (mce.key.first_tag.tagged == MESA_VCAP_BIT_1) ? 0xFFFF : 0;
    mce.key.first_tag.pcp.mask = 0x00;
    mce.key.first_tag.dei = MESA_VCAP_BIT_ANY;
    mce.key.isdx = MESA_MCE_ISDX_NONE;
    mce.key.mel.mask = 0x00;

    for (u32 i=0; i<mep_caps.mesa_port_cnt; ++i) {
        mce.action.port_list[i] = TRUE;/* Enable forwarding of any RAPS frame to other ring ports (in the same VLAN) */
    }

    mce.action.voe_idx = data->voe_idx;
    mce.action.isdx = MESA_MCE_ISDX_NEW;
    mce.action.oam_detect = (mce.key.first_tag.tagged == MESA_VCAP_BIT_1) ? MESA_MCE_OAM_DETECT_SINGLE_TAGGED : MESA_MCE_OAM_DETECT_UNTAGGED;

    if ((rc = mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce)) != VTSS_RC_OK) {
        T_IG(TRACE_GRP_MODEL, "mesa_mce_add failed %s", error_txt(rc));
        return(FALSE);
    }
    if ((rc = mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info)) != VTSS_RC_OK) {
        T_EG(TRACE_GRP_MODEL, "mesa_mce_port_info_get failed %s", error_txt(rc));
        return(FALSE);
    }
    data->tst_lbr_rx_mce_id = mce.id;
    set_instance_rx_isdx(data, mce_info.isdx, data->config.port, VTSS_VID_NULL);

    if (!data->mce_ids.push_back(mce.id)) {
        T_EG(TRACE_GRP_MODEL, "mce_ids.push_back failed");
        return(FALSE);
    }

    /******************************* Create injection MCE rule ***************************/
    /* No injection rule is required as Port domain injection is bypassing the rewriter. Instructed by IFH */

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(TRUE);
}

static u32 voe_mip_config(MepModelInstance *data)
{
    T_DG(TRACE_GRP_MODEL, "Enter  instance %u  voe_idx %u  mip_idx %u", data->instance, data->voe_idx, data->mip_idx);

    /* Configure VOE */
    if ((data->voe_idx != MESA_OAM_VOE_IDX_NONE) && !voe_config(data, FALSE)) {
        T_NG(TRACE_GRP_MODEL, "VOE configuration failed");
        return(VTSS_APPL_MEP_RC_RESOURCE_CONF_FAILED);
    }

    /* Config MIP */
    if ((data->mip_idx != MESA_OAM_MIP_IDX_NONE) && !mip_config(data)) {
        T_DG(TRACE_GRP_MODEL, "MIP configuration failed");
        return(VTSS_APPL_MEP_RC_RESOURCE_CONF_FAILED);
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(VTSS_RC_OK);
}

static u32 voe_mip_alloc(MepModelInstance *data)
{
    u32                       rc=0;
    mesa_oam_voe_alloc_cfg_t  alloc_cfg;

    T_NG(TRACE_GRP_MODEL, "Enter  instance %u  hw %u  voe_idx %u  mip_idx %u", data->instance, data->config.hw, data->voe_idx, data->mip_idx);

    /* Allocate VOE/MIP - if required */
    if (data->config.hw) {
        /* Allocate HW support */
        if ((data->create.type == MEP_MODEL_MEP) && (data->voe_idx == MESA_OAM_VOE_IDX_NONE)) {   /* VOE not already allocated - allocate VOE */
            alloc_cfg.phys_port = data->create.flow_num;
            if ((rc = mesa_oam_voe_alloc(NULL, (data->create.domain == MEP_MODEL_PORT) ? MESA_OAM_VOE_PORT : MESA_OAM_VOE_SERVICE, &alloc_cfg, &data->voe_idx)) != VTSS_RC_OK) {   /* Allocate VOE - if necessary */
                T_EG(TRACE_GRP_MODEL, "mesa_oam_voe_alloc failed %s", error_txt(rc));
                return(VTSS_APPL_MEP_RC_NO_VOE);
            }
            voe_to_instance[data->voe_idx] = data->instance;
        }

        if ((data->create.type == MEP_MODEL_MIP) && !MPLS_DOMAIN(data->create.domain) && (data->mip_idx == MESA_OAM_MIP_IDX_NONE)) {   /* MIP not already allocated - allocate MIP */
            if ((rc = mesa_oam_mip_alloc(NULL, (data->create.direction == MEP_MODEL_DOWN) ? MESA_OAM_DOWNMIP : MESA_OAM_UPMIP, &data->mip_idx)) != VTSS_RC_OK) { /* Allocate MIP - if necessary */
                T_EG(TRACE_GRP_MODEL, "mesa_oam_mip_alloc failed %s", error_txt(rc));
                return(VTSS_APPL_MEP_RC_NO_MIP);
            }
        }
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(VTSS_RC_OK);
}

static u32 mce_config(MepModelInstance *data)
{
    T_DG(TRACE_GRP_MODEL, "Enter  instance %u", data->instance);

    if ((data->create.domain == MEP_MODEL_EVC) && !evc_domain_mce_config(data)) {
        T_DG(TRACE_GRP_MODEL, "evc_domain_mce_config failed");
        return(VTSS_APPL_MEP_RC_RESOURCE_CONF_FAILED);
    }
    if ((data->create.domain == MEP_MODEL_EVC_SUB) && !subscriber_domain_mce_config(data)) {
        T_DG(TRACE_GRP_MODEL, "subscriber_domain_mce_config failed");
        return(VTSS_APPL_MEP_RC_RESOURCE_CONF_FAILED);
    }
    if ((data->create.domain == MEP_MODEL_VLAN) && !vlan_domain_mce_config(data)) {
        T_DG(TRACE_GRP_MODEL, "vlan_domain_mce_config failed");
        return(VTSS_APPL_MEP_RC_RESOURCE_CONF_FAILED);
    }

    T_NG(TRACE_GRP_MODEL, "Exit");

    return(VTSS_RC_OK);
}

static u32 static_mac_entry(MepModelInstance *data,  BOOL enable)
{
    u32                     rc;
    mesa_mac_table_entry_t  entry;

    memset(&entry, 0, sizeof(entry));
    mac_get(data->config.port, entry.vid_mac.mac.addr);  /* Get MAC of residence port */
    entry.vid_mac.vid = data->i_vid;
    entry.locked = TRUE;
    entry.destination[data->config.port] = TRUE;

    if (!enable) {
        rc = mesa_mac_table_del(NULL, &entry.vid_mac);
    } else {
        rc = mesa_mac_table_add(NULL, &entry);
    }
    return rc;
}

static u32 evc_vlan_domain_config(mep_model_domain_t domain,  u32 flow_id,  u32 instance)
{
    u32              next, flow_found, rc=VTSS_RC_OK, rc1=VTSS_RC_OK;
    BOOL             config_fail;
    MepModelInstance  *idata;
    mesa_port_list_t nni, uni;

    T_DG(TRACE_GRP_MODEL, "Enter  instance %u  doamin %u  flow_id %u", instance, domain, flow_id);

    /* Check configuration for all instances and give operational state accordingly */
    next = 0;
    while ((idata = get_instance_domain_flow(domain, flow_id, &next)) != NULL) { /* Get all MEPs in this flow */
        if ((rc1 = check_config_instance(idata->instance, &idata->create, &idata->config)) != VTSS_RC_OK) { /* Check for correct configuration */
            free_resources(idata);
            oper_state_set(idata,  MEP_MODEL_OPER_INVALID_CONFIG);
            if ((rc == VTSS_RC_OK) && (instance == idata->instance)) {     /* Can only return one error - the first */
                rc = rc1;
            }
            continue;
        }
        if ((rc1 = voe_mip_alloc(idata)) != VTSS_RC_OK) {                  /* Allocate VOE/MIP if required */
            free_resources(idata);
            oper_state_set(idata,  MEP_MODEL_OPER_HW_OAM);
            if ((rc == VTSS_RC_OK) && (instance == idata->instance)) {     /* Can only return one error - the first */
                rc = rc1;
            }
            continue;
        }

        oper_state_set(idata,  MEP_MODEL_OPER_UP);
    }

    /* Update the e_etree mode for all up-mep instances */
    next = 0;
    while (((idata = get_instance_domain_flow(domain, flow_id, &next)) != NULL) && (idata->create.direction == MEP_MODEL_UP)) {
        idata->e_tree = calc_e_tree_mode(idata->create.flow_num, idata->config.port);
    }

    /* Now do MCE and VOE/MIP configuration for all (operational) instances */
    do {
        next = 0;
        while ((idata = get_instance_domain_flow(domain, flow_id, &next)) != NULL) { /* Get all MEPs in this flow */
            /* Some registered ISDXs are "cleared" here as all instances in the flow will be re-created and some instances depends on other instances RX ISDX */
            del_instance_rx_isdx(idata);           /* 'delete' previously allocated RX ISDX for this instance. */
            idata->tx_isdx = VTSS_ISDX_NONE;       /* This TX ISDX can no longer be considered valid. */
            idata->tx_rev_isdx = VTSS_ISDX_NONE;   /* This reverse TX ISDX can no longer be considered valid. */
        }

        config_fail = FALSE;
        next = 0;
        while ((idata = get_instance_domain_flow(domain, flow_id, &next)) != NULL) { /* Get all MEPs in this flow */
            if (idata->oper_state != MEP_MODEL_OPER_UP) {   /* Only configure operational instances */
                continue;
            }
            /* MCE configuration */
            if ((rc1 = mce_config(idata)) != VTSS_RC_OK) {
                free_resources(idata);  /* In case failing - resources are freed and operational state is Down */
                oper_state_set(idata,  MEP_MODEL_OPER_MCE);
                if ((rc == VTSS_RC_OK) && (instance == idata->instance)) {     /* Can only return one error - the first */
                    rc = rc1;
                }
                config_fail = TRUE;
                break;
            }
            /* VOE/MIP configuration */
            if ((rc1 = voe_mip_config(idata)) != VTSS_RC_OK) {
                free_resources(idata);
                oper_state_set(idata,  MEP_MODEL_OPER_HW_OAM);
                if ((rc == VTSS_RC_OK) && (instance == idata->instance)) {     /* Can only return one error - the first */
                    rc = rc1;
                }
                config_fail = TRUE;
                break;
            }
            /* EVC configuration */
            if (MESA_CAP(MESA_CAP_EVC_EVC_CNT) > 0) {
                if ((domain == MEP_MODEL_EVC) || (domain == MEP_MODEL_EVC_SUB)) {
                    BOOL change;
                    mesa_evc_oam_port_conf_t evc_conf;
                    evc_conf.sat = FALSE;
                    evc_conf.mip_idx = MESA_OAM_MIP_IDX_NONE;
                    evc_conf.voe_idx = MESA_OAM_VOE_IDX_NONE;
                    if ((rc1 = mesa_evc_oam_port_conf_get(NULL, flow_id, idata->config.port, &evc_conf)) != VTSS_RC_OK) {
                        T_EG(TRACE_GRP_MODEL, "mesa_evc_oam_port_conf_get failed, rc=%s", error_txt(rc1));
                        if ((rc == VTSS_RC_OK) && (instance == idata->instance)) {     /* Can only return one error - the first */
                            rc = rc1;
                        }
                    }
                    change = FALSE;
                    if (idata->config.sat) {
                        evc_conf.sat = TRUE;
                        change = TRUE;
                    }
                    if ((domain == MEP_MODEL_EVC) && idata->mep_highest) {  /* EVC and instance on highest level */
                        evc_conf.voe_idx = idata->voe_idx;
                        change = TRUE;
                    }
                    if (change) {
                        if ((rc1 = mesa_evc_oam_port_conf_set(NULL, flow_id, idata->config.port, &evc_conf)) != VTSS_RC_OK) {  /* Hook EVC to VOE and MIPs */
                            T_EG(TRACE_GRP_MODEL, "mesa_evc_oam_port_conf_set failed, rc=%s", error_txt(rc1));
                            free_resources(idata);  /* In case failing - resources are freed and oper state is Down */
                            oper_state_set(idata,  MEP_MODEL_OPER_DOWN);
                            if ((rc == VTSS_RC_OK) && (instance == idata->instance)) {     /* Can only return one error - the first */
                                rc = rc1;
                            }
                            config_fail = TRUE;
                            break;
                        }
                    }
                }
            }
            /* Static MAC entry configuration */
            if (idata->create.direction == MEP_MODEL_UP) {
                if ((rc1 = static_mac_entry(idata, TRUE)) != VTSS_RC_OK) {
                    T_EG(TRACE_GRP_MODEL, "static_mac_entry failed, rc=%s", error_txt(rc1));
                    free_resources(idata);  /* In case failing - resources are freed and oper state is Down */
                    oper_state_set(idata,  MEP_MODEL_OPER_DOWN);
                    if ((rc == VTSS_RC_OK) && (instance == idata->instance)) {     /* Can only return one error - the first */
                        rc = rc1;
                    }
                    config_fail = TRUE;
                    break;
                }
            }

            /* No configuration failing for this instance - operational up */
            oper_state_set(idata,  MEP_MODEL_OPER_UP);
        } /* while() Get all MEPs in this flow */
    } while(config_fail); /* In case any failing configuration resources are freed and all instances must be re-configured */

//    if ((domain == MEP_MODEL_EVC) && !evc_domain_meg_mce_config(flow_id)) {  /* In EVC domain some OAM MEG rules must possibly be created on NNIs. This should be done here after the OAM MCE rules */
//        T_DG(TRACE_GRP_MODEL, "evc_domain_meg_mce_config failed  flow_id %u", flow_id);
//        return(VTSS_APPL_MEP_RC_RESOURCE_CONF_FAILED);
//    }

    if ((domain == MEP_MODEL_VLAN) && !vlan_domain_leak_mce_config(flow_id)) {  /* In VLAN domain some leaking rules must possibly be created. This should be done here after the OAM MCE rules */
        T_DG(TRACE_GRP_MODEL, "vlan_domain_leak_mce_config failed  flow_id %u", flow_id);
        return(VTSS_APPL_MEP_RC_RESOURCE_CONF_FAILED);
    }

    /* Restart of AFI must be done after all MEPs are created as some injection depends on allocated ISDX */
    next = 0;
    while ((idata = get_instance_domain_flow(domain, flow_id, &next)) != NULL) { /* Get all MEPs in this domain and flow */
        if (idata->oper_state != MEP_MODEL_OPER_UP) {   /* Only configure operational instances */
            continue;
        }
        /* Restart AFI if any */
        afi_restart(idata);
    }

    /* Restart of AFI must be done in any Flow Terminating (server) MEP that has this flow as client flow, after all MEPs are created as some injection depends on allocated ISDX */
    /* First get the relevante ports for this flow */
    if ((domain == MEP_MODEL_VLAN) && !vlan_flow_info_get(flow_id, nni)) {
        T_DG(TRACE_GRP_MODEL, "vlan_flow_info_get() failed instance=%u VLAN=%u", instance, flow_id);
        return VTSS_RC_OK; // no ports in flow (which is ok)
    }
    if ((domain == MEP_MODEL_EVC) && !evc_flow_info_port_get(flow_id, nni, uni)) {
        T_DG(TRACE_GRP_MODEL, "evc_flow_info_port_get() failed instance=%u EVC=%u", instance, flow_id);
        return VTSS_RC_OK; // no ports in flow (which is ok)
    }

    /* Get all MEPs in the port domain - this is currently the only possible server MEP */
    next = 0;
    while ((idata = get_instance_domain(MEP_MODEL_PORT, &next)) != NULL) {
        if ((idata->oper_state != MEP_MODEL_OPER_UP) || !nni[idata->config.port]) {   /* Only configure operational instances with residence port on NNI of this flow */
            continue;
        }
        /* Check if this flow is client flow for this Port domain instance */
        flow_found = FALSE;
        for (const auto &e : idata->client_config) {
            if ((e.config.domain == domain) && (e.config.flow == flow_id)) {
                flow_found = TRUE;
                break;
            }
        }
        if (flow_found) {
            /* Restart AFI if any. Ideally only the AFIs that are injecting into this specific flow should be restarted */
            afi_restart(idata);
        }
    }

    T_DG(TRACE_GRP_MODEL, "Exit  rc %u", rc);

    return (rc);
}















































































































































































static u32 instance_config(MepModelInstance *data,  BOOL voe_only)
{
    u32            rc = VTSS_RC_OK;

    T_DG(TRACE_GRP_MODEL, "Enter %u", data->instance);

    if (!data->admin_state) {   /* Free resources in case of admin down */
        free_resources(data);
        oper_state_set(data,  MEP_MODEL_OPER_DOWN);
    }

    switch (data->create.domain) {
    case MEP_MODEL_PORT:
        /* In port domain only 'this' instance is configured. */
        if (data->admin_state) {
            /* Check for valid configuration */
            if ((rc = check_config_instance(data->instance, &data->create, &data->config)) != VTSS_RC_OK) {
                T_DG(TRACE_GRP_MODEL, "check_config_instance failed");
                free_resources(data);
                oper_state_set(data,  MEP_MODEL_OPER_INVALID_CONFIG);
                return (rc);
            }
            if ((rc = voe_mip_alloc(data)) != VTSS_RC_OK) {
                free_resources(data);
                oper_state_set(data,  MEP_MODEL_OPER_HW_OAM);
                return (rc);
            }
            if (!voe_only && !port_domain_mce_config(data)) {    /* MCE configuration */
                T_DG(TRACE_GRP_MODEL, "port_domain_mce_config failed");
                free_resources(data);
                oper_state_set(data,  MEP_MODEL_OPER_MCE);
                return(VTSS_APPL_MEP_RC_RESOURCE_CONF_FAILED);
            }
            if ((rc = voe_mip_config(data)) != VTSS_RC_OK) {    /* VOE/MIP configuration */
                free_resources(data);
                oper_state_set(data,  MEP_MODEL_OPER_HW_OAM);
                return (rc);
            }

            oper_state_set(data,  MEP_MODEL_OPER_UP);
        }
        break;
    case MEP_MODEL_EVC:
        T_DG(TRACE_GRP_MODEL, "MEP_MODEL_EVC  voe_only %u  admin_state %u", voe_only, data->admin_state);













        return(VTSS_APPL_MEP_RC_RESOURCE_CONF_FAILED);

        break;
    case MEP_MODEL_EVC_SUB:
        T_DG(TRACE_GRP_MODEL, "MEP_MODEL_EVC_SUB  voe_only %u  admin_state %u", voe_only, data->admin_state);










        return(VTSS_APPL_MEP_RC_RESOURCE_CONF_FAILED);

        break;
    case MEP_MODEL_VLAN:
        T_DG(TRACE_GRP_MODEL, "MEP_MODEL_VLAN  voe_only %u  admin_state %u", voe_only, data->admin_state);
        if (!voe_only && ((rc = evc_vlan_domain_config(MEP_MODEL_VLAN, data->create.flow_num, data->instance)) != VTSS_RC_OK)) {    /* Full VLAN domain configuration - MCE and VOE/MIP configuration */
            return(rc);
        }
        if (voe_only && (rc = voe_mip_config(data)) != VTSS_RC_OK) {    /* VOE/MIP configuration - if VOE/MIP only */
            free_resources(data);
            oper_state_set(data,  MEP_MODEL_OPER_HW_OAM);
            return (rc);
        }
        break;





















    default:
        T_EG(TRACE_GRP_MODEL, "Unknown Domain");
        free_resources(data);
        oper_state_set(data,  MEP_MODEL_OPER_DOWN);
        return(VTSS_APPL_MEP_RC_INVALID_DOMAIN);
    }

    return(VTSS_RC_OK);
}

static u32 check_config_instance(u32 instance,  const mep_model_mep_create_t *const create,  const mep_model_mep_conf_t *const config)
{
    u32                         next, i;
    MepModelInstance             *data;
    mesa_port_list_t            nni;
    vtss_appl_vlan_port_conf_t  vlan_conf;
    CapArray<u32, MESA_CAP_PORT_CNT> up_cnt;
    CapArray<u32, MESA_CAP_PORT_CNT> up_enabled_cnt;
    CapArray<u32, MESA_CAP_PORT_CNT> up_level;
    CapArray<u32, MESA_CAP_PORT_CNT> down_enabled_cnt;




    up_cnt.clear();
    up_enabled_cnt.clear();
    up_level.clear();
    down_enabled_cnt.clear();

    T_DG(TRACE_GRP_MODEL, "Enter  instance %u", instance);

    if (create->domain == MEP_MODEL_PORT) { /* check config in Port Domain */
        if (create->type == MEP_MODEL_MIP) { /* No MIP in Port Domain */
            T_NG(TRACE_GRP_MODEL, "Port MIP not supported");
            return(VTSS_APPL_MEP_RC_MIP_SUPPORT);
        }
        if (create->direction == MEP_MODEL_UP) { /* No Up-MEP in Port Domain */
            T_NG(TRACE_GRP_MODEL, "Port Up-MEP not supported");
            return(VTSS_APPL_MEP_RC_UP_SUPPORT);
        }
        if (create->flow_num != config->port) { /* Residence port is the flow number */
            T_NG(TRACE_GRP_MODEL, "Port flow not correct");
            return(VTSS_APPL_MEP_RC_INVALID_PORT);
        }
        next = 0;
        while ((data = get_instance_domain_flow(create->domain, create->flow_num, &next)) != NULL) { /* Get all MEPs in this flow */
            if (data->instance != instance) { /* Only one port domain MEP on each port */
                T_NG(TRACE_GRP_MODEL, "Only one Port MEP per port supported");
                return(VTSS_APPL_MEP_RC_DOWN_MAX);
            }
        }
    }
    if (create->domain == MEP_MODEL_EVC) { /* check config in EVC Domain */






































































        return(VTSS_APPL_MEP_RC_INVALID_EVC);

    }
    if (create->domain == MEP_MODEL_EVC_SUB) { /* check config in EVC Subscriber Domain */

































































        return(VTSS_APPL_MEP_RC_INVALID_EVC);

    }
    if (create->domain == MEP_MODEL_VLAN) { /* check config in VLAN Domain */
        if (create->type == MEP_MODEL_MIP) { /* No MIP in VLAN Domain */
            T_NG(TRACE_GRP_MODEL, "VLAN MIP not supported");
            return(VTSS_APPL_MEP_RC_MIP_SUPPORT);
        }
        if (config->vid != 0) { /* VID is not supported for MEP in VLAN Domain */
            T_NG(TRACE_GRP_MODEL, "VID not supported");
            return(VTSS_APPL_MEP_RC_INVALID_VID);
        }
        if (create->direction == MEP_MODEL_UP) {    /* Check ports in the VLAN if UP MEP */
            if (!vlan_flow_info_get(create->flow_num, nni)) {
                T_NG(TRACE_GRP_MODEL, "vlan_flow_info_get failed");
                return(VTSS_APPL_MEP_RC_INVALID_VID);
            }
            T_NG(TRACE_GRP_MODEL, "nni %u-%u-%u-%u-%u-%u", nni.get(0), nni.get(1), nni.get(2), nni.get(3), nni.get(4), nni.get(5));
            if (!nni[config->port]) {
                T_NG(TRACE_GRP_MODEL, "Invalid VLAN Port");
                return(VTSS_APPL_MEP_RC_INVALID_PORT);
            }
        }
        if (!vlan_port_conf_get(config->port,  &vlan_conf)) {
            return(FALSE);
        }
        if ((vlan_conf.mode == VTSS_APPL_VLAN_PORT_MODE_HYBRID) && (vlan_conf.hybrid.port_type == VTSS_APPL_VLAN_PORT_TYPE_UNAWARE) && (vlan_conf.hybrid.pvid != create->flow_num)) {    /* An unaware port must have VID as PVID */
            T_NG(TRACE_GRP_MODEL, "Invalid VLAN Port");
            return(VTSS_APPL_MEP_RC_INVALID_PORT);
        }
        next = 0;
        while ((data = get_instance_domain_flow(MEP_MODEL_VLAN, create->flow_num, &next)) != NULL) { /* Get all MEPs in this flow */
            if (data->instance == instance){    /* Skip 'this' */
                continue;
            }
            if (data->create.direction == MEP_MODEL_UP) {  /* Count Up MEP per port and register their level */
                up_cnt[data->config.port]++;
                up_level[data->config.port] |= 1<<data->config.level;
            }
            if (data->oper_state != MEP_MODEL_OPER_UP) {
                continue;
            }
            if (data->create.direction == MEP_MODEL_DOWN) {  /* Count enabled Down MEP per port */
                down_enabled_cnt[data->config.port]++;
            }
            if (data->create.direction == MEP_MODEL_UP) {  /* Count enabled Up MEP per port */
                up_enabled_cnt[data->config.port]++;
            }
            if ((config->port == data->config.port) && (create->direction == data->create.direction) && (config->level == data->config.level)) {
                T_DG(TRACE_GRP_MODEL, "Illegal Level");
                return(VTSS_APPL_MEP_RC_INVALID_LEVEL);
            }
            if ((create->direction == MEP_MODEL_DOWN) && (data->create.direction == MEP_MODEL_UP) && (data->config.level <= config->level) && (data->config.port != config->port)) {
                T_DG(TRACE_GRP_MODEL, "Illegal Level");
                return(VTSS_APPL_MEP_RC_INVALID_LEVEL);
            }
            if ((create->direction == MEP_MODEL_UP) && (data->create.direction == MEP_MODEL_DOWN) && (data->config.level >= config->level) && (data->config.port != config->port)) {
                T_DG(TRACE_GRP_MODEL, "Illegal Level");
                return(VTSS_APPL_MEP_RC_INVALID_LEVEL);
            }
        }
        if ((create->direction == MEP_MODEL_DOWN) && (create->type == MEP_MODEL_MEP) && (down_enabled_cnt[config->port] == 2)) { /* Check for MAX number of enabled Down MEP/MIP */
            T_DG(TRACE_GRP_MODEL, "Maximum number of Down MEP");
            return(VTSS_APPL_MEP_RC_MEP_MAX);
        }
        if ((create->direction == MEP_MODEL_UP) && (create->type == MEP_MODEL_MEP) && (up_enabled_cnt[config->port] == 2)) { /* Check for MAX number of enabled Up MEP/MIP */
            T_DG(TRACE_GRP_MODEL, "Maximum number of Up MEP");
            return(VTSS_APPL_MEP_RC_MEP_MAX);
        }
        if (create->direction == MEP_MODEL_UP) {
            for (i=0; i<mep_caps.mesa_port_cnt; ++i) {
                if ((i != config->port) && (up_cnt[i] != 0) && !(up_level[i] & 1<<config->level)){  /* If Up-MEP on other ports there must be one on my level */
                    T_DG(TRACE_GRP_MODEL, "Illegal Level");
                    return(VTSS_APPL_MEP_RC_INVALID_LEVEL);
                }
            }
        }
    }
    // TBD BLA FIXME add checks for MPLS domains here

    T_DG(TRACE_GRP_MODEL, "Exit config OK");

    return(VTSS_RC_OK);
}

static void insert_c_s_tag(u8 *header,   u32 tpid,   u32 vid,   u8 prio,   BOOL dei)
{
    header[0] = (tpid & 0xFF00) >> 8;
    header[1] = (tpid & 0xFF);
    header[2] = ((vid & 0x0F00) >> 8) | (prio << 5) | ((dei) ? 0x10 : 0);
    header[3] = (vid & 0xFF);
}


static void port_state_change_event(mesa_port_no_t port_no)
{
    MepModelInstance            *idata;
    u32                        next = 0;
    mesa_rc                    rc;
    mesa_packet_port_info_t    info;
    CapArray<mesa_packet_port_filter_t, MESA_CAP_PORT_CNT> filter;
    BOOL                       new_forwarding;

    /* The port state has changed, now update the MEP/VLAN instance state to the state the API holds and notify subscribers */
    while ((idata = get_instance_created(&next)) != NULL) {
        if (((idata->create.domain != MEP_MODEL_EVC) && (idata->create.domain != MEP_MODEL_VLAN)) || (idata->create.direction == MEP_MODEL_UP)) {
            continue;
        }
        if (idata->config.port == port_no) {
            if ((rc = mesa_packet_port_info_init(&info)) != VTSS_RC_OK) {
                T_E("mesa_packet_port_info_init failed  rc %s", error_txt(rc));
                return;
            }
            info.vid = idata->i_vid;
            if ((rc = mesa_packet_port_filter_get(NULL, &info, filter.size(), filter.data())) != VTSS_RC_OK) {
                T_E("mesa_packet_port_filter_get failed  vid %u  rc %s", info.vid, error_txt(rc));
                return;
            }
            new_forwarding = (filter[idata->config.port].filter != MESA_PACKET_FILTER_DISCARD) ? TRUE : FALSE;  /* Forwarding is enabled if not discarding */
            T_DG(TRACE_GRP_MODEL, "filter %u  new_forwarding %u  idata->forwarding %u", filter[idata->config.port].filter, new_forwarding, idata->forwarding);
            if (idata->forwarding != new_forwarding) {  /* Forwarding state changed */
                idata->forwarding = new_forwarding;
                if (idata->oper_state == MEP_MODEL_OPER_UP) {
                    event_cb(idata,  MEP_MODEL_EVENT_MEP_STATUS_forwarding);
                }
            }
        }
    }
}

/****************************************************************************/
/*  Instance data + operations                                              */
/****************************************************************************/
class rx_isdx_inst {
public:
    u32 rx_port;   /* Rx port  */
    u32 i_vid;     /* Internal VID  */
    vtss::List<u32> instance;  /* Instance list. more up-MEP on same level can share RX ISDX */
    rx_isdx_inst() : rx_port(0), i_vid(0) {}
    rx_isdx_inst(u32 port,  u32 vid) : rx_port(port), i_vid(vid) {}
};
static vtss::Map<u32, MepModelInstance>  meps;
static vtss::Map<u32, rx_isdx_inst>     rx_isdx_to_instance;

static MepModelInstance *create_instance(u32 instance)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX) {
        T_DG(TRACE_GRP_MODEL, "Instance number invalid");
        return NULL;
    }

    auto res = meps.emplace(vtss::piecewise_construct, vtss::forward_as_tuple(instance), vtss::forward_as_tuple());
    if (!res.second) {
        T_EG(TRACE_GRP_MODEL, "Instance emplace failed");
        return NULL;
    }

    return &(res.first->second);
}

static BOOL delete_instance(u32 instance)
{
    vtss::Map<u32, MepModelInstance>::iterator i;

    T_NG(TRACE_GRP_MODEL, "Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX) {
        T_DG(TRACE_GRP_MODEL, "Instance number invalid");
        return FALSE;
    }
    i = meps.find(instance);
    if (i == meps.end()) {
        return FALSE;
    }
    del_instance_rx_isdx(&(*i).second);
    meps.erase(i);

    T_NG(TRACE_GRP_MODEL, "Exit");

    return TRUE;
}

static MepModelInstance *get_instance(u32 instance)
{
    vtss::Map<u32, MepModelInstance>::iterator i;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX) {
        T_DG(TRACE_GRP_MODEL, "Instance number invalid");
        return NULL;
    }
    i = meps.find(instance);
    if (i == meps.end()) {
        return NULL;
    }
    return &(*i).second;
}

static MepModelInstance *get_instance_created(u32 *next)
{
    vtss::Map<u32, MepModelInstance>::iterator i;

    i = meps.greater_than_or_equal(*next);
    if (i == meps.end()) {
        return NULL;
    }
    *next = (*i).first + 1;
    return &(*i).second;
}

/* TBD could create specific search map for below... (note only changed at create/delete) */
static MepModelInstance *get_instance_domain(mep_model_domain_t domain,  u32 *next)
{
    vtss::Map<u32, MepModelInstance>::iterator i;

    for (i = meps.greater_than_or_equal(*next); i != meps.end(); ++i) {
        if ((*i).second.admin_state && ((*i).second.create.domain == domain)) {
            *next = (*i).first + 1;
            return &(*i).second;
        }
    }
    return NULL;
}

/* TBD could create specific search map for below... (note only changed at create/delete) */
static MepModelInstance *get_instance_domain_flow(mep_model_domain_t domain,  u32 flow_num,  u32 *next)
{
    vtss::Map<u32, MepModelInstance>::iterator i;

    for (i = meps.greater_than_or_equal(*next); i != meps.end(); ++i) {
        if ((*i).second.admin_state && ((*i).second.create.domain == domain) && ((*i).second.create.flow_num == flow_num)) {
            *next = (*i).first + 1;
            return &(*i).second;
        }
    }
    return NULL;
}

static MepModelInstance *get_instance_with_tx_isdx(mep_model_domain_t domain,  u32 flow,  u32 vid,  mep_model_direction_t direction,  u32 port)
{
    u32              next;
    MepModelInstance *idata;

    /* Find the MEP/MIP with a valid TX ISDX */
    next = 0;
    while ((idata = get_instance_domain_flow(domain, flow, &next)) != NULL) { /* Get all MEPs in this flow */
        if ((idata->oper_state == MEP_MODEL_OPER_UP) && ((idata->create.domain != MEP_MODEL_EVC_SUB) || (idata->config.vid == vid)) &&
            (idata->create.direction == direction) && (idata->config.port == port) && (idata->tx_isdx != VTSS_ISDX_NONE)) {
            return(idata);
        }
    }
    return(NULL);
}

static MepModelInstance *get_instance_with_reverse_isdx(mep_model_domain_t domain,  u32 flow,  u32 vid,  mep_model_direction_t direction,  u32 port)
{
    u32              next;
    MepModelInstance *idata;

    /* Find the MEP/MIP with a valid TX ISDX */
    next = 0;
    while ((idata = get_instance_domain_flow(domain, flow, &next)) != NULL) { /* Get all MEPs in this flow */
        if ((idata->oper_state == MEP_MODEL_OPER_UP) && ((idata->create.domain != MEP_MODEL_EVC_SUB) || (idata->config.vid == vid)) &&
            (idata->create.direction == direction) && (idata->config.port == port) && (idata->tx_rev_isdx != VTSS_ISDX_NONE)) {
            return(idata);
        }
    }
    return(NULL);
}

static MepModelInstance *get_instance_mep_highest(mep_model_domain_t domain,  u32 flow,  mep_model_direction_t direction,  u32 port)
{
    u32             highest_level, next;
    MepModelInstance *idata, *rdata;

    /* Find the Up/Down MEP on the highest level on this port in this domain in this direction */
    highest_level = 0;
    next = 0;
    rdata = NULL;
    while ((idata = get_instance_domain_flow(domain, flow, &next)) != NULL) { /* Get all MEPs in this flow */
        if ((idata->create.type == MEP_MODEL_MEP) && (idata->oper_state == MEP_MODEL_OPER_UP) &&
            (idata->create.direction == direction) && (idata->config.port == port) && (idata->config.level >= highest_level)) {
            highest_level = idata->config.level;
            rdata = idata;
        }
    }
    return(rdata);
}

static MepModelInstance *get_instance_mep_lowest(mep_model_domain_t domain,  u32 flow,  mep_model_direction_t direction,  u32 port)
{
    u32             lowest_level, next;
    MepModelInstance *idata, *rdata;

    /* Find the Up/Down MEP on the lowest level on this port in this domain in this direction */
    lowest_level = 7;
    next = 0;
    rdata = NULL;
    while ((idata = get_instance_domain_flow(domain, flow, &next)) != NULL) { /* Get all MEPs in this flow */
        if ((idata->create.type == MEP_MODEL_MEP) && (idata->oper_state == MEP_MODEL_OPER_UP) &&
            (idata->create.direction == direction) && (idata->config.port == port) && (idata->config.level <= lowest_level)) {
            lowest_level = idata->config.level;
            rdata = idata;
        }
    }
    return(rdata);
}

static MepModelInstance *get_instance_mep_highest_port(mep_model_domain_t domain,  u32 flow,  u32 port)
{
    u32             highest_level, next;
    MepModelInstance *idata, *rdata;

    /* Find the Up/Down MEP on the highest level on this port in this domain */
    highest_level = 0;
    next = 0;
    rdata = NULL;
    while ((idata = get_instance_domain_flow(domain, flow, &next)) != NULL) { /* Get all MEPs in this flow */
        if ((idata->create.type == MEP_MODEL_MEP) && (idata->oper_state == MEP_MODEL_OPER_UP) &&
            (idata->config.port == port) && (idata->config.level >= highest_level)) {
            highest_level = idata->config.level;
            rdata = idata;
        }
    }
    return(rdata);
}

static MepModelInstance *get_instance_up_mep_lowest_of_two_any(mep_model_domain_t domain,  u32 flow)
{
    u32                                             next;
    MepModelInstance                                *idata;
    CapArray<MepModelInstance*, MESA_CAP_PORT_CNT>  port_inst;

    /* Find an Up-MEP on the lowest level on any port with two Up-MEPs - in this domain */
    next = 0;
    while ((idata = get_instance_domain_flow(domain, flow, &next)) != NULL) { /* Get all MEPs in this flow */
        if ((idata->create.type == MEP_MODEL_MEP) && (idata->create.direction == MEP_MODEL_UP) && (idata->oper_state == MEP_MODEL_OPER_UP)) {
            if (port_inst[idata->config.port] != NULL) {    /* Found port with two Up-MEP */
                return (idata->config.level < port_inst[idata->config.port]->config.level) ? idata : port_inst[idata->config.port]; /* Return the instance on lowest level */
            }
            port_inst[idata->config.port] = idata;
        }
    }
    return(NULL);
}

static MepModelInstance *get_instance_up_mep_highest_of_two_any(mep_model_domain_t domain,  u32 flow)
{
    u32                                             next;
    MepModelInstance                                *idata;
    CapArray<MepModelInstance*, MESA_CAP_PORT_CNT>  port_inst;

    /* Find an Up-MEP on the highest level on any port with two Up-MEPs - in this domain */
    next = 0;
    while ((idata = get_instance_domain_flow(domain, flow, &next)) != NULL) { /* Get all MEPs in this flow */
        if ((idata->create.type == MEP_MODEL_MEP) && (idata->create.direction == MEP_MODEL_UP) && (idata->oper_state == MEP_MODEL_OPER_UP)) {
            if (port_inst[idata->config.port] != NULL) {    /* Found port with two Up-MEP */
                return (idata->config.level > port_inst[idata->config.port]->config.level) ? idata : port_inst[idata->config.port]; /* Return the instance on highest level */
            }
            port_inst[idata->config.port] = idata;
        }
    }
    return(NULL);
}

static MepModelInstance *get_instance_up_mep_highest_of_two(mep_model_domain_t domain,  u32 flow,  u32 port)
{
    u32             highest_level, next, mep_cnt;
    MepModelInstance *idata, *rdata;

    /* Find the Up-MEP on the highest level on this port with two Up-MEPs - in this domain */
    highest_level = 0;
    next = 0;
    mep_cnt = 0;
    rdata = NULL;
    while ((idata = get_instance_domain_flow(domain, flow, &next)) != NULL) { /* Get all MEPs in this flow */
        if ((idata->create.type == MEP_MODEL_MEP) && (idata->create.direction == MEP_MODEL_UP) && (idata->oper_state == MEP_MODEL_OPER_UP) && (idata->config.port == port)) {
            mep_cnt++;
            if (idata->config.level >= highest_level) {
                highest_level = idata->config.level;
                rdata = idata;
            }
        }
    }
    return((mep_cnt == 2) ? rdata : NULL);
}

static MepModelInstance *get_instance_down_mep_lowest_of_two(mep_model_domain_t domain,  u32 flow,  u32 port)
{
    u32             lowest_level, next, mep_cnt;
    MepModelInstance *idata, *rdata;

    /* Find the Down-MEP on the lowest level on this port with two Down-MEPs - in this domain */
    lowest_level = 7;
    next = 0;
    mep_cnt = 0;
    rdata = NULL;
    while ((idata = get_instance_domain_flow(domain, flow, &next)) != NULL) { /* Get all MEPs in this flow */
        if ((idata->create.type == MEP_MODEL_MEP) && (idata->create.direction == MEP_MODEL_DOWN) && (idata->oper_state == MEP_MODEL_OPER_UP) && (idata->config.port == port)) {
            mep_cnt++;
            if (idata->config.level <= lowest_level) {
                lowest_level = idata->config.level;
                rdata = idata;
            }
        }
    }
    return((mep_cnt == 2) ? rdata : NULL);
}

static MepModelInstance *get_instance_up_port(u32 port, u32 *next)
{
    vtss::Map<u32, MepModelInstance>::iterator i;

    for (i = meps.greater_than_or_equal(*next); i != meps.end(); ++i) {
        if ((*i).second.admin_state && ((*i).second.create.direction == MEP_MODEL_UP) && ((*i).second.config.port == port)) {
            *next = (*i).first + 1;
            return &(*i).second;
        }
    }
    return NULL;
}

static void del_rx_isdx_to_instance(u32 instance, mesa_isdx_t isdx)
{
    vtss::Map<u32, rx_isdx_inst>::iterator map_i;

    T_DG(TRACE_GRP_ISDX, "Enter  instance %u  isdx %u", instance, isdx);

    map_i = rx_isdx_to_instance.find(isdx); /* Find the ISDX in the ISDX to instance MAP */
    if (map_i != rx_isdx_to_instance.end()) { /* ISDX found */
        T_NG(TRACE_GRP_ISDX, "ISDX found %u", isdx);
        for (auto inst_i = map_i->second.instance.begin(); inst_i != map_i->second.instance.end();) {  /* Find instance in instance List */
            if (*inst_i == instance) {  /* Instance found */
                T_NG(TRACE_GRP_ISDX, "instance.erase %u\n",*inst_i);
                map_i->second.instance.erase(inst_i++); /* Erase instance from instance List */
                if (map_i->second.instance.empty()) {    /* If instance List is empty the ISDX can be erased from the MAP */
                    T_NG(TRACE_GRP_ISDX, "instance.empty  rx_isdx_to_instance.erase\n");
                    rx_isdx_to_instance.erase(map_i);
                    break;
                }
            } else {
                ++inst_i;
            }
        }
    }

    T_NG(TRACE_GRP_ISDX, "Exit");
}

static void set_instance_rx_isdx(MepModelInstance *data,  mesa_isdx_t isdx,  u32 rx_port,  u32 i_vid)
{
    T_DG(TRACE_GRP_ISDX, "Enter  instance %u  isdx %u  rx_port %u  i_vid %u", data->instance, isdx, rx_port, i_vid);

    vtss::Map<u32, rx_isdx_inst>::iterator i;

    auto res = rx_isdx_to_instance.emplace(vtss::piecewise_construct, vtss::forward_as_tuple(isdx), vtss::forward_as_tuple(rx_port, i_vid));    /* Add ISDX to MAP or existing IDSX is returned */
    i = res.first;
    if (!res.second) {
        T_DG(TRACE_GRP_ISDX, "ISDX already exist");
    }

    if (!i->second.instance.push_back(data->instance)) {    /* Push instance into instance List for this ISDX */
        T_EG(TRACE_GRP_ISDX, "instance.push_back failed");
    }

    if (!data->rx_isdx.push_back(inst_rx_isdx(rx_port, i_vid, isdx))) {    /* Push ISDX into ISDX Vector for this instance */
        T_EG(TRACE_GRP_ISDX, "rx_isdx.push_back failed");
    }
}

static void del_instance_rx_isdx(MepModelInstance *data)
{
    T_DG(TRACE_GRP_ISDX, "Enter  instance %u", data->instance);

    for (const auto &isdx_e : data->rx_isdx) {   /* Find all IDSXs for this instance */
        del_rx_isdx_to_instance(data->instance, isdx_e.isdx);    /* Delete this ISDX from rx_isdx_to_instance MAP */
    }

    data->rx_isdx.clear();      /* 'clear' stored RX ISDXs for this instance */

    T_NG(TRACE_GRP_ISDX, "Exit");
}

static void del_instance_rx_isdx(MepModelInstance *data, mesa_isdx_t isdx)
{
    u32 i=0;

    T_DG(TRACE_GRP_ISDX, "Enter  instance %u", data->instance);

    for (const auto &isdx_e : data->rx_isdx) {   /* Find all IDSXs for this instance */
        if (isdx_e.isdx == isdx) {
            del_rx_isdx_to_instance(data->instance, isdx_e.isdx);    /* Delete them from rx_isdx_to_instance MAP */
            data->rx_isdx.erase(data->rx_isdx.begin() + i);      /* 'clear' stored RX ISDXs for this instance */
            break;
        }
        i++;
    }

    T_NG(TRACE_GRP_ISDX, "Exit");
}

static MepModelInstance *get_instance_rx_isdx(mesa_isdx_t isdx,  u32 residence_port,  u32 level,  u32 *rx_port,  u32 *i_vid)
{
    vtss::Map<u32, rx_isdx_inst>::iterator    i = rx_isdx_to_instance.find(isdx);  /* Find ISDX in RX ISDX Map */
    vtss::Map<u32, MepModelInstance>::iterator ii;

    T_DG(TRACE_GRP_ISDX, "ISDX %u  residence_port %u  level %u", isdx, residence_port, level);

    if (i == rx_isdx_to_instance.end()) {
        T_DG(TRACE_GRP_ISDX, "ISDX %u not found", isdx);
        return NULL;
    }

    for (const auto &instance_e : i->second.instance) {   /* Find instance in instance Vector that is on this residence port */
        T_NG(TRACE_GRP_ISDX, "instance_e %u\n", instance_e);
        ii = meps.find(instance_e);
        if ((ii != meps.end()) && ((*ii).second.config.port == residence_port) && ((*ii).second.config.level >= level) && ((*ii).second.lowest_level <= level)) {
            T_NG(TRACE_GRP_ISDX, "Isdx %u -> instance %u found", isdx, (*ii).second.instance);
            *rx_port = (*i).second.rx_port;
            *i_vid = (*i).second.i_vid;
            return &(*ii).second;
        }
    }
    T_DG(TRACE_GRP_ISDX, "ISDX %u -> instance not found", isdx);
    return NULL;
}

static MepModelInstance *get_instance_tx_isdx(mesa_isdx_t isdx,  u32 residence_port,  u8 *pdu_start)
{
    vtss::Map<u32, MepModelInstance>::iterator iter;

    if (pdu_start[1] != MESA_OAM_OPCODE_SLM && pdu_start[1] != MESA_OAM_OPCODE_SLR) {
        // This is not an SLM/SLR PDU
        return NULL;
    }

    T_DG(TRACE_GRP_ISDX, "ISDX %u  residence_port %u", isdx, residence_port);

    for (iter = meps.begin(); iter != meps.end(); iter++) {
        MepModelInstance *data = &iter->second;

        if (data->tx_isdx == isdx) {
            // Found a MEP instance - check for PDU type
            if (data->create.direction == MEP_MODEL_UP && pdu_start[1] == MESA_OAM_OPCODE_SLR) {
                // This is an SLM in disguise. For an Up-MEP it is looped and converted before it is copied to CPU.
                pdu_start[1] = MESA_OAM_OPCODE_SLM;
            }

            if (data->create.domain == MEP_MODEL_PORT && data->create.flow_num != residence_port) {
                // All port MEPs share the same TX ISDX (0)) - check for residence port
                continue;
            }

            return data;
        }
    }
    T_DG(TRACE_GRP_ISDX, "ISDX %u -> instance not found", isdx);
    return NULL;
}

static MepModelInstance *get_instance_voe(u32 voe)
{
    u32 instance = voe_to_instance[voe];

    if (instance < VTSS_APPL_MEP_INSTANCE_MAX) {
        return get_instance(instance);
    } else {
        return NULL;
    }
}

/****************************************************************************/
/*  Protection data + operations                                              */
/****************************************************************************/
static vtss::Map<u32, port_protection_t> protection;

static port_protection_t *create_protection_entry(u32 w_port)
{
    if (w_port >= mep_caps.mesa_port_cnt) {
        T_DG(TRACE_GRP_MODEL, "Invalid working port");
        return NULL;
    }

    auto res = protection.emplace(vtss::piecewise_construct, vtss::forward_as_tuple(w_port), vtss::forward_as_tuple());

    if (!res.second) {
        T_DG(TRACE_GRP_MODEL, "w_port emplace failed");
        return NULL;
    }

    return &(res.first->second);
}

static BOOL delete_protection_entry(u32 w_port)
{
    vtss::Map<u32, port_protection_t>::iterator i;

    T_NG(TRACE_GRP_MODEL, "Enter %u", w_port);

    if (w_port >= mep_caps.mesa_port_cnt) {
        T_DG(TRACE_GRP_MODEL, "Invalid working port");
        return FALSE;
    }

    i = protection.find(w_port);
    if (i == protection.end()) {
        return FALSE;
    }

    protection.erase(i);

    T_NG(TRACE_GRP_MODEL, "Exit");

    return TRUE;
}

/*
 * Returns the protection entry for this working port or NULL if this port
 * is not a working port in a protection group.
 */
static port_protection_t *get_protection_entry(u32 w_port)
{
    vtss::Map<u32, port_protection_t>::iterator i;

    if (w_port >= mep_caps.mesa_port_cnt) {
        T_DG(TRACE_GRP_MODEL, "Invalid working port");
        return NULL;
    }

    i = protection.find(w_port);
    if (i == protection.end()) {
        return NULL;
    }
    return &(*i).second;
}














































































































































}  // mep_g2

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
