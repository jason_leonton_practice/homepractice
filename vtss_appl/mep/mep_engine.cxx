/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#define MEP_IMPL_G2

#include "mscc/ethernet/switch/api.h"
#include "vtss/appl/mep.h"
#include "vtss/appl/vlan.h"
#include "icli_porting_util.h"
#include "mep_engine.h" /* Internal interface */
#include "mep_trace.h"
#include "mep_model.h"
#include "mep_peer_array.hxx"
#include "main.h"
#include <string.h>







#include <map.hxx>
#include "vector.hxx"
#include "vlan_api.h"
#include "packet_api.h"
#include "mep_ll_pdu_defs.h"

namespace mep_g2 {

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_MEP

/****************************************************************************/
/*  MEP global variables                                                    */
/****************************************************************************/

/****************************************************************************/
/*  MEP local variables                                                     */
/****************************************************************************/

#define CCM_PDU_LENGTH       (75+16) /* Port Status + Interface Status + Organization-Specific TLV is added */
#define LMR_PDU_LENGTH       18
#define LMM_PDU_LENGTH       18
#define SLM_PDU_MIN_LENGTH   (21+3)   /* SLM PDU with empty TLV */
#define SLR_PDU_LENGTH       21
#define LAPS_PDU_LENGTH      9
#define RAPS_PDU_LENGTH      37
#define LTM_PDU_LENGTH       33
#define LTR_PDU_LENGTH       40
#define LBM_PDU_LENGTH       9   /* This is only the size of the 'emty' LBM PDU -  - without contained TLV */
#define DM1_PDU_LENGTH       21
#define DMM_PDU_LENGTH       37
#define DMR_PDU_LENGTH       37
#define AIS_PDU_LENGTH       5
#define LCK_PDU_LENGTH       5
#define TST_PDU_LENGTH       9   /* This is only the size of the 'emty' TST PDU - without contained TLV */
#define CCM_PDU_TLV_OFFSET   74
#define SLM_TLV_OFFSET       16
#define LAPS_DATA_LENGTH      4
#define RAPS_DATA_LENGTH      8
#define APS_DATA_LENGTH ((data->aps_config.type == VTSS_APPL_MEP_L_APS) ? LAPS_DATA_LENGTH : RAPS_DATA_LENGTH)
#define APS_PDU_LENGTH ((data->aps_config.type == VTSS_APPL_MEP_L_APS) ? LAPS_PDU_LENGTH : RAPS_PDU_LENGTH)
#define BFD_CC_PDU_LENGTH    (4 + 24 + 28)      /* BFD authentication is max. 28 bytes */
#define BFD_CV_PDU_LENGTH    (4 + 24 + 28 + 28) /* MEP ID TLV is max. 28 bytes and BFD authentication is max. 28 bytes*/

#define MEP_FLR_SCALE_FACTOR        10000    // Frame-loss-ratio scale factor


// Additional non-standard OAM opcodes
#define OAM_TYPE_1DM_FUP  0xFE /* Use the reserved code 0xfe as the opcode of follow-up packets */
#define OAM_TYPE_DMR_FUP  0xFF /* Use the reserved code 0xff as the opcode of follow-up packets */

#define MEP_DM_MAX                  VTSS_APPL_MEP_DM_LASTN_MAX    /* Max number of DM counters saved in RAM */
#define MEP_DM_TS_MCAST             0

#define MEP_DEFAULT_SYSLOG_TIMEOUT  (30 * 1000)
#define MEP_APS_TIMEOUT  5000




#define MPLS_DOMAIN(domain) (FALSE)


#define MEG_ID_LENGTH (2+(VTSS_APPL_MEP_MEG_CODE_LENGTH-1)+2+(VTSS_APPL_MEP_MEG_CODE_LENGTH-1))
#define var_to_string(string, var) {(string)[0] = (var&0xFF000000)>>24;   (string)[1] = (var&0x00FF0000)>>16;   (string)[2] = (var&0x0000FF00)>>8;   (string)[3] = (var&0x000000FF);}
#define string_to_var(string)  (((string)[0]<<24) | ((string)[1]<<16) | ((string)[2]<<8) | (string)[3])

static void delete_timer(vtss::Timer **timer);

typedef struct
{
    u32 farTxC;             /* Far end transmitted frames               */
    u32 nearTxC;            /* Near end transmitted frames              */
    u32 nearRxC;            /* Near end received frames                 */
    u32 farRxC;             /* Far end received frames                  */
} lm_counters_t;


typedef struct {
    u32      next_prt;
    u32      delay[MEP_DM_MAX];
    u32      delay_var[MEP_DM_MAX];

    u64      sum;
    u64      var_sum;
    u64      n_sum;
    u64      n_var_sum;
    u32      delay_cnt;

    u32      last_delay;
} delay_state_t;

typedef struct
{
    BOOL    dInv;                       /* Invalid period (0) received                                    */
    BOOL    dLevel;                     /* Incorrect CCM level received                                   */
    BOOL    dMeg;                       /* Incorrect CCM MEG id received                                  */
    BOOL    dMep;                       /* Incorrect CCM MEP id received                                  */
    MepPeerArray<BOOL> dLoc;            /* CCM LOC state from all peer MEP                                */
    MepPeerArray<BOOL> dRdi;            /* CCM RDI state from all peer MEP                                */
    MepPeerArray<BOOL> dPeriod;         /* CCM Period state from all peer MEP                             */
    MepPeerArray<BOOL> dPrio;           /* CCM Priority state from all peer MEP                           */
    BOOL    dAis;                       /* AIS received                                                   */
    BOOL    dLck;                       /* LCK received                                                   */
    BOOL    dLoop;                      /* Loop is detected. CCM is received with own MEP ID and SMAC     */
    BOOL    dConfig;                    /* Configuration error detected. CCM is received with own MEP ID  */
    BOOL    dSsf;                       /* SSF detected                                                   */
    BOOL    ciSsf;                      /* Client Interface SSF detected                                  */
} defect_state_t;

typedef struct
{
    u32                     valid_counter;
    u32                     invalid_counter;
    u32                     unexp_level;                                  /* Last received invalid level value                                          */
    u32                     unexp_period;                                 /* Last received invalid period value                                         */
    u32                     unexp_prio;                                   /* Last received invalid priority value                                       */
    u32                     unexp_mep;                                    /* Last received unexpected MEP id                                            */
    char                    unexp_name[VTSS_APPL_MEP_MEG_CODE_LENGTH];    /* Last received unexpected Domain Name or ITU Carrier Code (ICC). (string)   */
    char                    unexp_meg[VTSS_APPL_MEP_MEG_CODE_LENGTH];     /* Last received unexpected Unique MEG ID Code.                    (string)   */
    MepPeerArray<vtss_appl_mep_ps_tlv_t> ps_tlv;      /* Last received Port Status TLV content                                      */
    MepPeerArray<vtss_appl_mep_is_tlv_t> is_tlv;      /* Last received Interface Status TLV content                                 */
    MepPeerArray<vtss_appl_mep_os_tlv_t> os_tlv;      /* Last received Organizational Specific TLV content                          */
    MepPeerArray<BOOL>      ps_received;
    MepPeerArray<BOOL>      is_received;
    MepPeerArray<BOOL>      os_received;
    u8                      unexp_meg_id[MEG_ID_LENGTH];
} ccm_state_t;

typedef struct
{
    defect_state_t           old_defect_state;
    defect_state_t           defect_state;
    ccm_state_t              ccm_state;
    delay_state_t            tw_state[VTSS_APPL_MEP_PRIO_MAX];
    delay_state_t            fn_state[VTSS_APPL_MEP_PRIO_MAX];
    delay_state_t            nf_state[VTSS_APPL_MEP_PRIO_MAX];
} supp_state_t;

/*
 * Contains the counters contained in a single received LMM/LMR/CCM-LM/SLM/SLR/1SL PDU.
 * Not all counter members are valid for every PDU type.
 */
typedef struct
{
    u32                      far_tx;        // Transmitted by far end.
    u32                      far_rx;        // Received by far end
    u32                      near_tx;       // Transmitted by near end
    u32                      near_rx;       // Received by near end

    BOOL                     is_valid;      // Set to TRUE on reception of PDU.
                                            // Reset to FALSE every measurement period.

    void clear()
    {
        far_tx = 0;
        far_rx = 0;
        near_tx = 0;
        near_rx = 0;

        is_valid = FALSE;
    }

} lm_counter_sample_t;

/*
 * Loss Measurement instance state
 */
typedef struct
{
    vtss::Timer                 *timer;                 // LM measurement interval timer
    BOOL                        clear_counters;         // Clear counters on config change
    vtss_appl_mep_clear_dir     clear_dir;              // Counter direction to clear.
    u32                         unicast_peer_index;     // Index of unicast LM peer ID

} lm_state_t;

/*
 * Loss Measurement state for each peer MEP
 */
typedef struct
{
    u32                      flr_interval_count;
    u32                      near_tx_counter;
    u32                      far_tx_counter;
    u64                      near_tot_tx_counter;
    u64                      far_tot_tx_counter;

    // See Appendix III in Y.1731
    u32                      near_CT1;
    u32                      far_CT1;
    u32                      near_CR1;
    u32                      far_CR1;

    i32                      lm_nearLos;
    i32                      lm_farLos;

    BOOL                     lm_start;

    lm_counter_sample_t      last_pdu_sample;           // Counters from last LM PDU received

} lm_peer_state_t;

/*
 * Loss Measurement High Loss Interval state for each peer MEP.
 */
typedef struct {
    u32                     near_cnt;
    u32                     far_cnt;
} lm_hli_state_t;

typedef struct
{
    u32         transaction_id;
    vtss::Timer *timer;
} lt_state_t;

typedef struct
{
    vtss::Timer *timer;
    u32         lbm_tx_count;
    MepPeerArray<u32> rx_transaction_id[VTSS_APPL_MEP_PRIO_MAX];
} lb_state_t;

typedef struct
{
    u32          rx_frame_size;
    BOOL         enable[VTSS_APPL_MEP_PRIO_MAX] = {};
    vtss::Timer  *timer;
} tst_state_t;

typedef struct
{
    BOOL        fe_disable;
    vtss::Timer *ign_timer;
    vtss::Timer *sf_timer;
} lst_state_t;

typedef struct
{
    vtss::Timer *timer;
    MepPeerArray<u32> peer_mep_timer;
    MepPeerArray<u32> transaction_id;
} syslog_state_t;

typedef struct
{
    u32         old_period = 0;
    vtss::Timer *timer = NULL;
} defect_timer_t;

typedef struct
{
    BOOL                            is_valid;
    mep_model_pdu_rx_t              rx_pdu;

} ll_state_t;

typedef struct
{
    vtss_mep_mgmt_state_t           state;
    MepPeerArray<vtss_appl_mep_lm_state_t> lm_state;
    MepPeerArray<vtss_appl_mep_lm_avail_state_t> lm_avail_state;
    MepPeerArray<vtss_appl_mep_lm_hli_state_t> lm_hli_state;
    vtss_appl_mep_lt_state_t        lt_state;
    vtss_appl_mep_lb_state_t        lb_state[VTSS_APPL_MEP_PRIO_MAX];
    vtss_appl_mep_tst_state_t       tst_state[VTSS_APPL_MEP_PRIO_MAX];
    vtss_appl_mep_dm_state_t        tw_state[VTSS_APPL_MEP_PRIO_MAX];
    vtss_appl_mep_dm_state_t        fn_state[VTSS_APPL_MEP_PRIO_MAX];
    vtss_appl_mep_dm_state_t        nf_state[VTSS_APPL_MEP_PRIO_MAX];
    u8                              rx_aps[MEP_APS_DATA_LENGTH];
} out_state_t;

typedef struct MepInstance {
    u32                              instance;
    mep_conf_t                       config;                         /* Input from management */
    vtss_appl_mep_cc_conf_t          cc_config;
    vtss_appl_mep_pm_conf_t          pm_config;
    vtss_appl_mep_lst_conf_t         lst_config;
    vtss_appl_mep_syslog_conf_t      syslog_config;
    vtss_appl_mep_lm_conf_t          lm_config;
    vtss_mep_mgmt_dm_conf_t          dm_config;
    vtss_appl_mep_aps_conf_t         aps_config;
    vtss_appl_mep_lt_conf_t          lt_config;
    vtss_mep_mgmt_lb_conf_t          lb_config;
    vtss_appl_mep_ais_conf_t         ais_config;                     /* AIS configuration input from management    */
    vtss_appl_mep_lck_conf_t         lck_config;                     /* LCK configuration input from management    */
    vtss_mep_mgmt_tst_conf_t         tst_config;                     /* TST configuration input from management    */
    mep_client_conf_t                client_config;                  /* Client configuration input from management */











    BOOL                             vola;                                  /* This instance is created volatile  */
    BOOL                             sat;                                   /* This instance is in SAT mode.      */
    mep_sat_prio_conf_t              sat_prio_conf[VTSS_APPL_MEP_PRIO_MAX]; /* The SAT EVC priority configuration */

    supp_state_t                     supp_state;                            /* Input from lower support           */

    u8                               ais_cnt;                               /* Counter for the 3 fast AIS         */
    u8                               tx_aps[RAPS_PDU_LENGTH];               /* Input from module interface        */
    BOOL                             raps_forward;                          /* Enable RAPS fwd between ring ports */
    BOOL                             raps_transmit;                         /* Enable RAPS transmission           */

    out_state_t                      out_state;                             /* Output state                       */

    lm_state_t                       lm_state;
    MepPeerArray<lm_peer_state_t>    lm_peer_state;
    lt_state_t                       lt_state;                              /* Internal state                     */
    lb_state_t                       lb_state;
    tst_state_t                      tst_state;
    lst_state_t                      lst_state;
    syslog_state_t                   syslog_state;
    ll_state_t                       ll_state;
    BOOL                             forwarding;

    u32                              aps_count;
    BOOL                             aps_event;

    mep_model_pdu_tx_id_t            aps_tx_id;
    mep_model_pdu_tx_id_t            lmm_tx_id;
    mep_model_pdu_tx_id_t            slm_tx_id;
    mep_model_pdu_tx_id_t            ccm_tx_id;
    mep_model_pdu_tx_id_t            dm_tx_id[VTSS_APPL_MEP_PRIO_MAX];
    mep_model_pdu_tx_id_t            tst_tx_id[VTSS_APPL_MEP_PRIO_MAX][VTSS_APPL_MEP_TEST_MAX];
    mep_model_pdu_tx_id_t            lbm_tx_id[VTSS_APPL_MEP_PRIO_MAX][VTSS_APPL_MEP_TEST_MAX];
    vtss::Vector<mep_model_pdu_tx_id_t> *ais_tx_id = NULL;
    vtss::Vector<mep_model_pdu_tx_id_t> *lck_tx_id = NULL;

    BOOL                             dm_enabled[VTSS_APPL_MEP_PRIO_MAX];
    BOOL                             lb_enabled[VTSS_APPL_MEP_PRIO_MAX][VTSS_APPL_MEP_TEST_MAX];
    BOOL                             tst_tx_enabled[VTSS_APPL_MEP_PRIO_MAX][VTSS_APPL_MEP_TEST_MAX];
    BOOL                             tst_rx_enabled;

    u8                               exp_meg[MEG_ID_LENGTH];                /* expected MA-ID/MEG-ID  */
    u32                              exp_meg_len;                           /* length of expected MEG */

    u32                              ccm_keep_alive;
    u32                              loc_timer_val;
    BOOL                             voe_ccm_enabled;
    defect_timer_t                   dLevel;                                /* Defect timers          */
    defect_timer_t                   dMeg;
    defect_timer_t                   dMep;
    MepPeerArray<defect_timer_t>     dPeriod;
    MepPeerArray<defect_timer_t>     dPrio;
    defect_timer_t                   dAis;
    defect_timer_t                   dLck;
    defect_timer_t                   dLoop;
    defect_timer_t                   dConfig;
    MepPeerArray<vtss::Timer*>       rx_ccm_timer;
    vtss::Timer                      *tx_aps_timer = NULL;
    vtss::Timer                      *rx_aps_timer = NULL;

    lm_counters_t                    *lm_avail_fifo = NULL;                        /* containing lm_counters */
    u32                              lm_avail_fifo_ix;
    vtss_appl_mep_lm_avail_conf_t    lm_avail_config;
    vtss_appl_mep_lm_hli_conf_t      lm_hli_config;
    MepPeerArray<lm_hli_state_t>     lm_hli_state;
    vtss_appl_mep_lm_sdeg_conf_t     lm_sdeg_config;
    BOOL                             lm_sdeg_bad;
    BOOL                             dDeg;
    u32                              lm_sdeg_bad_cnt;
    u32                              lm_sdeg_good_cnt;
    vtss::Timer                      *los_int_cnt_timer = NULL;
    vtss::Timer                      *los_th_cnt_timer = NULL;
    vtss::Timer                      *hli_cnt_timer = NULL;
    vtss::Timer                      *ais_cnt_timer = NULL;

    mesa_timestamp_t               txTimef;                                 /* Latest timestamps to support PTP */
    mesa_timestamp_t               rxTimef;
    mesa_timestamp_t               txTimeb;
    mesa_timestamp_t               rxTimeb;
    BOOL                           dmr_ts_ok;

    MepInstance() : instance(0), vola(FALSE), sat(FALSE), ais_tx_id(NULL), lck_tx_id(NULL)
    {
        config.enable = FALSE;
        for (u32 i = 0; i < mep_caps.mesa_oam_peer_cnt; ++i) {
            rx_ccm_timer[i] = NULL;
        }
    }
    ~MepInstance()
    {
    }
} mep_instance_data_t;

static u32                                 pdu_period_to_timer[8];
static u32                                 voe_period_to_timer[8];
static vtss::Map<u32, mep_instance_data_t> instance_data;
static vtss_appl_mep_os_tlv_t              os_tlv_config;
static u32                                 vlan_raps_acl_id[VTSS_VIDS];
static u32                                 vlan_raps_mac_octet[VTSS_VIDS];
static u8                                  lm_null_counter[12] = {0,0,0,0,0,0,0,0,0,0,0,0};
static u8 all_zero_mac[VTSS_APPL_MEP_MAC_LENGTH] = {0,0,0,0,0,0};
static u8 raps_mac[VTSS_APPL_MEP_MAC_LENGTH] = {0x01,0x19,0xA7,0x00,0x00,0x01};
static u8 class1_multicast[8][VTSS_APPL_MEP_MAC_LENGTH] = {{0x01,0x80,0xC2,0x00,0x00,0x30},
                                                           {0x01,0x80,0xC2,0x00,0x00,0x31},
                                                           {0x01,0x80,0xC2,0x00,0x00,0x32},
                                                           {0x01,0x80,0xC2,0x00,0x00,0x33},
                                                           {0x01,0x80,0xC2,0x00,0x00,0x34},
                                                           {0x01,0x80,0xC2,0x00,0x00,0x35},
                                                           {0x01,0x80,0xC2,0x00,0x00,0x36},
                                                           {0x01,0x80,0xC2,0x00,0x00,0x37}};
static vtss_appl_mep_rate_t  pdu_period_to_rate[8] = {VTSS_APPL_MEP_RATE_INV,
                                                      VTSS_APPL_MEP_RATE_300S,
                                                      VTSS_APPL_MEP_RATE_100S,
                                                      VTSS_APPL_MEP_RATE_10S,
                                                      VTSS_APPL_MEP_RATE_1S,
                                                      VTSS_APPL_MEP_RATE_6M,
                                                      VTSS_APPL_MEP_RATE_1M,
                                                      VTSS_APPL_MEP_RATE_6H};

typedef struct
{
    mep_instance_data_t *data;
    //    vtss::Timer **timer_pp;
    void (*func_ptr)(u32, struct vtss::Timer *timer);
    void *t_obj;
    BOOL active;
} timer_info_t;

typedef struct {
    vtss::Timer timer_p;
    timer_info_t t_info;
} timer_obj_t;

/****************************************************************************/
/*  MEP local functions                                                     */
/****************************************************************************/
static void run_ssf_ssd_state(u32 instance);
static u32 run_cc_config(u32 instance);
static u32 run_lm_config(u32 instance);
static void run_aps_config(u32 instance);
static void run_ais_config(u32 instance);
static void run_lck_config(u32 instance);
static void run_lst_sf_timer(u32 instance, struct vtss::Timer *timer);
static void run_tst_timer(u32 instance, struct vtss::Timer *timer);
static void run_lb_timer(u32 instance, struct vtss::Timer *timer);
static void run_rx_ccm_timer(u32 instance, struct vtss::Timer *timer);
static void run_lt_timer(u32 instance, struct vtss::Timer *timer);
static void run_syslog_timer(u32 instance, struct vtss::Timer *timer);
static void run_slm_timer(u32 instance, struct vtss::Timer *timer);




static u32 run_tst_config(u32 instance, BOOL force, u64 *active_time_ms);
static u32 run_lb_config(u32 instance, BOOL force, u64 *active_time_ms);
void  vtss_mep_mgmt_default_conf_get(mep_default_conf_t  *const def_conf);
u32 is_tlv_change(mep_instance_data_t  *data);
static void new_defect_state(mep_instance_data_t  *data);
static BOOL mep_timer_active(struct vtss::Timer *timer);
static void mep_timer_start(u32 period_ms, vtss::Timer **timer, mep_instance_data_t *data_p, void (*func_ptr)(u32, struct vtss::Timer *timer));
static void mep_timer_cancel(vtss::Timer *timer);
static void tx_ais(mep_instance_data_t *data, u32 rate);
static BOOL mep_vlan_member_set(u32 vid,  mesa_port_list_t &ports,  BOOL enable);
static BOOL mep_vlan_member_get(u32 vid,  mesa_port_list_t &ports);
static u32 apply_aps_config(u32 instance);
static void run_tst_clear(u32 instance);
static void run_slm_meas_interval_expired(mep_instance_data_t *data);
static void run_defect_timer(u32 instance, struct vtss::Timer *timer);

static mep_instance_data_t *mep_instance_create(u32 instance)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX) {
        T_D("Instance number invalid");
        return NULL;
    }

    auto res = instance_data.emplace(instance, mep_instance_data_t());
    if (!res.second) {
        T_E("Instance emplace failed");
        return NULL;
    }

    return &(res.first->second);
}

static mep_instance_data_t *get_instance(u32 instance)
{
    vtss::Map<u32, mep_instance_data_t>::iterator i;
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX) {
        return NULL;
    }
    i = instance_data.find(instance);
    if (i == instance_data.end()) {
        return NULL;
    }
    return &(*i).second;
}

static mep_instance_data_t *get_instance_created(u32 *next)
{
    vtss::Map<u32, mep_instance_data_t>::iterator i;

    i = instance_data.greater_than_or_equal(*next);
    if (i == instance_data.end()) {
        return NULL;
    }
    *next = (*i).first + 1;
    return &(*i).second;
}

static void instance_data_clear(u32 instance,  mep_instance_data_t *data)
{
    u32                 i, j;                           /* The VID of rules for this flow - used for create/delete of PDU capture/forward rules */
    mep_default_conf_t  def_conf;

    T_N("Enter %u", instance);

    vtss_mep_mgmt_default_conf_get(&def_conf);

    vtss_clear(*data);

    data->ais_tx_id     = new vtss::Vector<mep_model_pdu_tx_id_t>;
    data->lck_tx_id     = new vtss::Vector<mep_model_pdu_tx_id_t>;

    data->config = def_conf.config;
    data->cc_config = def_conf.cc_conf;
    data->lm_config = def_conf.lm_conf;
    data->lm_avail_config = def_conf.lm_avail_conf;
    data->lm_hli_config = def_conf.lm_hli_conf;
    data->lm_sdeg_config = def_conf.lm_sdeg_conf;
    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        data->dm_config.dm[i] = def_conf.dm_prio_conf;
    }
    data->dm_config.common = def_conf.dm_common_conf;
    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            data->lb_config.tests[i][j] = def_conf.test_prio_conf;
        }
    }
    data->lb_config.common = def_conf.lb_common_conf;
    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            data->tst_config.tests[i][j] = def_conf.test_prio_conf;
        }
    }
    data->tst_config.common = def_conf.tst_common_conf;
    data->lt_config = def_conf.lt_conf;
    data->aps_config = def_conf.aps_conf;
    data->ais_config = def_conf.ais_conf;
    data->lck_config = def_conf.lck_conf;




    data->instance = instance;
}

/*
 * Return the index of the peer with the given peer MEP ID
 */
static BOOL get_peer_index(mep_instance_data_t *data, u32 peer_id, u32 &peer_index)
{
    u32 i;

    if (peer_id > VTSS_APPL_MEP_ID_MAX) {
        // peer ID is out of range
        return FALSE;
    }

    for (i = 0; i < data->config.peer_count; ++i) {/* Search for peer MEP-ID */
        if (data->config.peer_mep[i] == peer_id)
            break;
    }
    if (i == data->config.peer_count)
        return FALSE;

    peer_index = i;
    return TRUE;
}

static vtss_appl_mep_oper_state_t mep_oper_state_calc(mep_model_mep_oper_state_t oper_state)
{
    switch (oper_state)
    {
        case MEP_MODEL_OPER_UP:             return(VTSS_APPL_MEP_OPER_UP);
        case MEP_MODEL_OPER_DOWN:           return(VTSS_APPL_MEP_OPER_DOWN);
        case MEP_MODEL_OPER_INVALID_CONFIG: return(VTSS_APPL_MEP_OPER_INVALID_CONFIG);
        case MEP_MODEL_OPER_HW_OAM:         return(VTSS_APPL_MEP_OPER_HW_OAM);
        case MEP_MODEL_OPER_MCE:            return(VTSS_APPL_MEP_OPER_MCE);
        case MEP_MODEL_OPER_COUNT:          return(VTSS_APPL_MEP_OPER_DOWN);    /* Invallid value */
    }
    return(VTSS_APPL_MEP_OPER_DOWN);
}

static vtss_mep_mgmt_etree_t mep_etree_calc(mep_model_etree_t e_tree)
{
    switch (e_tree)
    {
        case MEP_MODEL_ETREE_NONE: return(VTSS_MEP_ETREE_NONE);
        case MEP_MODEL_ETREE_ROOT: return(VTSS_MEP_ETREE_ROOT);
        case MEP_MODEL_ETREE_LEAF: return(VTSS_MEP_ETREE_LEAF);
    }
    return(VTSS_MEP_ETREE_NONE);
}

static mep_model_oam_count_t model_count_calc(vtss_appl_mep_oam_count count)
{
    switch (count)
    {
        case VTSS_APPL_MEP_OAM_COUNT_Y1731:  return(MEP_MODEL_OAM_COUNT_Y1731);
        case VTSS_APPL_MEP_OAM_COUNT_NONE:   return(MEP_MODEL_OAM_COUNT_NONE);
        case VTSS_APPL_MEP_OAM_COUNT_ALL:    return(MEP_MODEL_OAM_COUNT_ALL);
    }
    return(MEP_MODEL_OAM_COUNT_Y1731);
}

/* returns rate in frames/hour */
static u32 frame_rate_calc(vtss_appl_mep_rate_t   rate)
{
    switch (rate)
    {
        case VTSS_APPL_MEP_RATE_INV:   return(0);
        case VTSS_APPL_MEP_RATE_300S:  return(60*60*300);
        case VTSS_APPL_MEP_RATE_100S:  return(60*60*100);
        case VTSS_APPL_MEP_RATE_10S:   return(60*60*10);
        case VTSS_APPL_MEP_RATE_1S:    return(60*60*1);
        case VTSS_APPL_MEP_RATE_6M:    return(60*6);
        case VTSS_APPL_MEP_RATE_1M:    return(60);
        case VTSS_APPL_MEP_RATE_6H:    return(6);
    }
    return(0);
}

static mep_model_type_t model_type_calc(vtss_appl_mep_mode_t  mode)
{
    switch (mode)
    {
        case VTSS_APPL_MEP_MEP: return(MEP_MODEL_MEP);
        case VTSS_APPL_MEP_MIP: return(MEP_MODEL_MIP);
    }
    return(MEP_MODEL_MEP);
}

static mep_model_direction_t model_direction_calc(vtss_appl_mep_direction_t  direction)
{
    switch (direction)
    {
        case VTSS_APPL_MEP_DOWN: return(MEP_MODEL_DOWN);
        case VTSS_APPL_MEP_UP:   return(MEP_MODEL_UP);
    }
    return(MEP_MODEL_DOWN);
}

static mep_model_domain_t model_domain_calc(mep_domain_t domain, mesa_vid_t vid)
{
    switch (domain) {
    case VTSS_APPL_MEP_PORT:          return(MEP_MODEL_PORT);
    case VTSS_APPL_MEP_EVC:           return (vid == 0 ? MEP_MODEL_EVC : MEP_MODEL_EVC_SUB);
    case VTSS_APPL_MEP_VLAN:          return(MEP_MODEL_VLAN);
    case VTSS_APPL_MEP_MPLS_LINK:     return(MEP_MODEL_MPLS_LINK);
    case VTSS_APPL_MEP_MPLS_TUNNEL:   return(MEP_MODEL_MPLS_TUNNEL);
    case VTSS_APPL_MEP_MPLS_PW:       return(MEP_MODEL_MPLS_PW);
    case VTSS_APPL_MEP_MPLS_LSP:      return(MEP_MODEL_MPLS_LSP);
    }
    return(MEP_MODEL_PORT);
}

static mep_model_rate_t model_rate_calc(vtss_appl_mep_rate_t rate)
{
    switch (rate)
    {
        case VTSS_APPL_MEP_RATE_300S:    return(MEP_MODEL_RATE_300S);
        case VTSS_APPL_MEP_RATE_100S:    return(MEP_MODEL_RATE_100S);
        case VTSS_APPL_MEP_RATE_10S:     return(MEP_MODEL_RATE_10S);
        case VTSS_APPL_MEP_RATE_1S:      return(MEP_MODEL_RATE_1S);
        case VTSS_APPL_MEP_RATE_6M:      return(MEP_MODEL_RATE_6M);
        case VTSS_APPL_MEP_RATE_1M:
        case VTSS_APPL_MEP_RATE_6H:
        case VTSS_APPL_MEP_RATE_INV:
        default:                         return(MEP_MODEL_RATE_INV);
    }
}

static u8 rate_to_pdu_calc(vtss_appl_mep_rate_t  rate)
{
    switch (rate)
    {
        case VTSS_APPL_MEP_RATE_INV:   return(0);
        case VTSS_APPL_MEP_RATE_300S:  return(1);
        case VTSS_APPL_MEP_RATE_100S:  return(2);
        case VTSS_APPL_MEP_RATE_10S:   return(3);
        case VTSS_APPL_MEP_RATE_1S:    return(4);
        case VTSS_APPL_MEP_RATE_6M:    return(5);
        case VTSS_APPL_MEP_RATE_1M:    return(6);
        case VTSS_APPL_MEP_RATE_6H:    return(7);
    }
    return(4);
}

/**
 * Convert rate to layer-1 rate given the frame size and MEP domain.
 *
 * A layer-1 rate relates to the physical line speed (aka. line rate) and
 * includes the standard inter-frame gap and preamble (20 bytes). A layer-2 rate
 * is calculated based on the Ethernet frame size alone.
 *
 * For MEPs in the port domain the rate will be provided as a layer-1 rate. All
 * other domains will have the rate specified as a layer-2 rate.
 *
 * @param rate          The rate value provided by the user
 * @param frame_size    The frame size used
 * @param domain        The MEP domain.
 * @return              The resulting layer-1 rate
 */
static u32 get_l1_bitrate(u32 rate, u32 frame_size, mep_domain_t domain)
{
    if (domain == VTSS_APPL_MEP_PORT) {
        // Rate is already specified as layer-1 for port domain
        return rate;
    }

    // Rate is specified as layer-2 for all other domains - calculate layer-1 rate
    const u64 ifg_and_preamble_sz = 20LLU;
    u64 l1rate = ((frame_size + ifg_and_preamble_sz) * rate) / frame_size;
    return (u32)l1rate;
}

static void lst_action(mep_instance_data_t *data)
{
    BOOL ne_disable = FALSE;
    BOOL fe_disable = FALSE;

    if (data->lst_config.enable) {  /* Link State Tracking is enabled */
        /* Near-end disable if local TSF is detected and stable. */
        ne_disable = (data->out_state.state.mep.aTsf && !mep_timer_active(data->lst_state.sf_timer));
        /* far-end disable if IS down received - without ignore timer running to avoid "deadlock" */
        fe_disable = (data->supp_state.ccm_state.is_received[0] && (data->supp_state.ccm_state.is_tlv[0].value == 2) && !mep_timer_active(data->lst_state.ign_timer));

        /* In case of LOS on residence port and the port is not disabled - far-end disable is not allowed to avoid "deadlock" */
        if (data->supp_state.defect_state.ciSsf && !data->lst_state.fe_disable)
            fe_disable = FALSE;
    }
    T_D("Instance %u  is_received %u  is_tlv %u  fe_disable %u  ne_disable %u", data->instance, data->supp_state.ccm_state.is_received[0], data->supp_state.ccm_state.is_tlv[0].value, fe_disable, ne_disable);

    /* Enable or disable the port */
    data->lst_state.fe_disable = fe_disable;
    vtss_mep_port_conf_disable(data->instance, data->config.port, ne_disable || fe_disable);
}


static void consequent_action_calc_and_deliver(mep_instance_data_t *data)
{
    u32                    i;
    BOOL                   old_aTsf, old_aTsd, old_aBlk, dLoc=FALSE;
    defect_state_t         *supp_state;
    vtss_appl_mep_state_t  *out_state;

    T_N("Enter  instance %u", data->instance);

    supp_state = &data->supp_state.defect_state;
    out_state = &data->out_state.state.mep;

    old_aTsf = out_state->aTsf;
    old_aTsd = out_state->aTsd;
    old_aBlk = out_state->aBlk;

    for (i=0; i<data->config.peer_count; ++i)
        dLoc = dLoc || supp_state->dLoc[i];





    out_state->aTsf = ((supp_state->dLck && !data->cc_config.enable) || (dLoc && data->cc_config.enable) || (supp_state->dAis && !data->cc_config.enable) ||
                                      supp_state->dLevel || supp_state->dMeg || supp_state->dMep || supp_state->dSsf);

    out_state->aTsd = data->dDeg && !out_state->aTsf;

    out_state->aBlk = supp_state->dLevel || supp_state->dMeg || supp_state->dMep;

    if (old_aTsf != out_state->aTsf) {  /* aTsf has changed */
        if (!mep_model_ccm_rdi_set(data->instance, out_state->aTsf)) {     /* Control RDI */
            T_D("mep_model_ccm_rdi_set failed");
        }
        if (!mep_model_mep_assf_set(data->instance,  out_state->aTsf)) {    /* Control SSF */
            T_D("mep_model_mep_assf_set failed");
        }

        if (data->ais_config.enable) {  /* Control AIS */
            run_ais_config(data->instance);
        }

        if (data->lst_config.enable) {
            if (out_state->aTsf) {   /* Active aTsf */
                mep_timer_start(200, &data->lst_state.sf_timer, data, run_lst_sf_timer);
            } else {
                mep_timer_cancel(data->lst_state.sf_timer);
            }
            lst_action(data);
        }
    }

    if (old_aBlk != out_state->aBlk) { /* aBlk has changed */
        if (!mep_model_mep_block_set(data->instance, (out_state->aBlk || data->lck_config.enable))) {   /* Control service frame block */
            T_D("mep_model_mep_block_set failed");
        }
    }

    // Update status before calling out.
    vtss_mep_status_change(data->instance, data->config.enable);

    if ((old_aTsd != out_state->aTsd) || (old_aTsf != out_state->aTsf)) { /* New SF/SD state */
        vtss_mep_sf_sd_state_set(data->instance, data->config.domain, data->out_state.state.mep.aTsf, data->out_state.state.mep.aTsd);
    }

    T_N("Exit");
}


static void fault_cause_calc(mep_instance_data_t *data)
{
    u32                          i;
    defect_state_t               *supp_state;
    vtss_appl_mep_state_t        *out_state;
    vtss_appl_mep_peer_state_t   *p_out_state;

    T_N("Enter  instance %u", data->instance);

    supp_state = &data->supp_state.defect_state;
    out_state = &data->out_state.state.mep;

    out_state->cLevel = supp_state->dLevel;
    out_state->cMeg = supp_state->dMeg;
    out_state->cMep = supp_state->dMep;

    out_state->cSsf    = supp_state->dSsf || supp_state->dAis;
    out_state->cAis    = supp_state->dAis;
    out_state->cLck    = supp_state->dLck && !supp_state->dAis;
    out_state->cLoop   = supp_state->dLoop;
    out_state->cConfig = supp_state->dConfig;
    out_state->cDeg    = data->dDeg && !supp_state->dAis && !supp_state->dLck && !supp_state->dSsf;

    for (i=0; i<mep_caps.mesa_oam_peer_cnt; ++i)
    {
        p_out_state = &data->out_state.state.peer_mep[i];
        p_out_state->cPeriod = supp_state->dPeriod[i];
        p_out_state->cPrio = supp_state->dPrio[i];




        p_out_state->cRdi = supp_state->dRdi[i] && !supp_state->dLoc[i] && data->cc_config.enable;
        p_out_state->cLoc = (supp_state->dLoc[i] && !supp_state->dAis && !supp_state->dLck && !supp_state->dSsf && data->cc_config.enable);

    }

    vtss_mep_status_change(data->instance, data->config.enable);

    T_N("Exit");
}

static void syslog_timer_start(mep_instance_data_t *data, u32 peer_mep)
{
    u32     idx;

    for (idx = 0; idx < mep_caps.mesa_oam_peer_cnt; ++idx) {
        if (data->syslog_state.peer_mep_timer[idx] == 0) {
            data->syslog_state.peer_mep_timer[idx] = MEP_DEFAULT_SYSLOG_TIMEOUT;
            data->syslog_state.transaction_id[idx] = peer_mep;
            break;
        }
    }

    if (!mep_timer_active(data->syslog_state.timer)) {
        mep_timer_start(MEP_DEFAULT_SYSLOG_TIMEOUT, &data->syslog_state.timer, data, run_syslog_timer);
    }
}

static void syslog_timer_stop(mep_instance_data_t *data, u32 peer_mep)
{
    u32     idx;

    for (idx = 0; idx < mep_caps.mesa_oam_peer_cnt; ++idx) {
        if (data->syslog_state.transaction_id[idx] == peer_mep) {
            data->syslog_state.peer_mep_timer[idx] = 0;
            data->syslog_state.transaction_id[idx] = 0;
            break;
        }
    }
}


static void peer_mep_change_handle(mep_instance_data_t *data,  mep_conf_t *old_conf)
{
    u32             i, j, peer_mep;
    defect_state_t  *defect_state;

    defect_state = &data->supp_state.defect_state;

    /* Find any deleted peer MEP */
    for (i=0; i<old_conf->peer_count; ++i) {
        peer_mep = old_conf->peer_mep[i];
        for (j=0; j<data->config.peer_count; ++j) {
            if (peer_mep == data->config.peer_mep[i])
                break;
        }
        if (j == data->config.peer_count) {
            T_D("Deleted peer MEP found  instance %u  peer %u", data->instance, peer_mep);
            /* Clear defects */
            defect_state->dPeriod[i] = FALSE;
            defect_state->dPrio[i] = FALSE;
            defect_state->dRdi[i] = FALSE;
            defect_state->dLoc[i] = FALSE;
            new_defect_state(data);
        }
    }

    /* Restart defect clear timers for all peers. The timer value is selected to 3.5 sec. In case a peer is transmitting with a lower rate a defect might be temporary cleared */
    for (i=0; i<data->config.peer_count; ++i) {
        mep_timer_start(3500, &data->dPeriod[i].timer, data, run_defect_timer);
        mep_timer_start(3500, &data->dPrio[i].timer, data, run_defect_timer);
    }
    /* Stop any unused defect timers */
    for (i=i; i<mep_caps.mesa_oam_peer_cnt; ++i) {
        delete_timer(&data->dPeriod[i].timer);
        delete_timer(&data->dPrio[i].timer);
    }
}

static void syslog_new_peer_mep(mep_instance_data_t *data,  mep_conf_t *old_conf)
{
    u32  i, j, peer_mep;

    /* check the control flag of syslog for CFM is enabled or not */
    if (data->syslog_config.enable == FALSE) {
        T_N("syslog_new_peer_mep syslog not enabled  instance %u", data->instance);
        return;
    }

    /* Find any added peer MEP */
    for (i=0; i<data->config.peer_count; ++i) {
        peer_mep = data->config.peer_mep[i];
        for (j=0; j<old_conf->peer_count; ++j) {
            if (peer_mep == old_conf->peer_mep[i])
                break;
        }
        if (j == old_conf->peer_count) {
            T_D("Added peer MEP found  instance %u  peer %u", data->instance, peer_mep);
            syslog_timer_start( data, peer_mep);
        }
    }

    /* Find any deleted peer MEP */
    for (i=0; i<old_conf->peer_count; ++i) {
        peer_mep = old_conf->peer_mep[i];
        for (j=0; j<data->config.peer_count; ++j) {
            if (peer_mep == data->config.peer_mep[i])
                break;
        }
        if (j == data->config.peer_count) {
            T_D("Deleted peer MEP found  instance %u  peer %u", data->instance, peer_mep);
            syslog_timer_stop( data, peer_mep);
        }
    }
}


static void syslog_new_defect_state(mep_instance_data_t *data)
{
    /* Calculate entries in sys_log based on changes in defect state */

    u32             i, self_mep, peer_mep;
    BOOL            all_cleared;
    BOOL            loc_changed;
    defect_state_t  *state, *old_state;

    /* check the control flag of syslog for CFM is enabled or not */
    if (data->syslog_config.enable == FALSE) {
        T_N("syslog not enabled for instance %u", data->instance);
        return;
    }

    self_mep = data->instance+1;
    state = &data->supp_state.defect_state;
    old_state = &data->supp_state.old_defect_state;

    loc_changed = FALSE;
    for (i=0; i<data->config.peer_count; ++i) {
        if (state->dLoc[i] != old_state->dLoc[i]) {
            loc_changed = TRUE;
            peer_mep = data->config.peer_mep[i];
            T_D("LOC state changed  instance %u  peer %u  state %u", data->instance, peer_mep, state->dLoc[i]);
            /* syslog for CFM, REMOTE_MEP_DOWN, REMOTE_MEP_UP */
            if (state->dLoc[i]) {
                vtss_mep_syslog(VTSS_MEP_REMOTE_MEP_DOWN, self_mep, peer_mep);
                syslog_timer_stop( data, peer_mep);
            } else {
                vtss_mep_syslog(VTSS_MEP_REMOTE_MEP_UP, self_mep, peer_mep);
            }
        }
    }

    if (loc_changed) {
        all_cleared = TRUE;
        for (i=0; i<data->config.peer_count; ++i) {
            if (state->dLoc[i]) {
                all_cleared = FALSE;
                break;
            }
        }
        if (all_cleared) {
            /* syslog for CFM, VTSS_APPL_MEP_CROSSCHECK_SERVICE_UP */
            vtss_mep_syslog(VTSS_MEP_CROSSCHECK_SERVICE_UP, self_mep, 0);
        }
    }

    if (state->dAis != old_state->dAis) {
        /* syslog for CFM, ENTER_AIS, EXIT_AIS */
        if (state->dAis) {
            vtss_mep_syslog(VTSS_MEP_ENTER_AIS, self_mep, 0);
        } else {
            vtss_mep_syslog(VTSS_MEP_EXIT_AIS, self_mep, 0);
        }
    }

    if (state->dMeg != old_state->dMeg) {
        /* syslog for CFM, CROSS_CONNECTED_SERVICE */
        if (state->dMeg) {
            vtss_mep_syslog(VTSS_MEP_CROSS_CONNECTED_SERVICE, self_mep, 0);
        }
    }

    if (state->dMep != old_state->dMep) {
        /* syslog for CFM, CROSSCHECK_MEP_UNKNOWN */
        if (state->dMep) {
            vtss_mep_syslog(VTSS_MEP_CROSSCHECK_MEP_UNKNOWN, self_mep, 0);
        }
    }

    if (state->dLoop != old_state->dLoop) {
        /* syslog for CFM, FORWARDING_LOOP */
        if (state->dLoop) {
            vtss_mep_syslog(VTSS_MEP_FORWARDING_LOOP, self_mep, 0);
        }
    }

    if (state->dConfig != old_state->dConfig) {
        /* syslog for CFM, CONFIG_ERROR */
        if (state->dConfig) {
            vtss_mep_syslog(VTSS_MEP_CONFIG_ERROR, self_mep, 0);
        }
    }
}

static u32 highest_client_prio_calc(mep_model_client_t *client,  u32 port)
{
    u32  prio=7;














    return prio;
}

static BOOL test_enable_calc(const vtss_appl_mep_test_prio_conf_t   tests[VTSS_APPL_MEP_PRIO_MAX][VTSS_APPL_MEP_TEST_MAX])
{
    u32 i, j;

    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {  /* Calculate if any TST transmission is active */
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            if (tests[i][j].enable) {
                return TRUE;
            }
        }
    }
    return FALSE;
}

static BOOL dm_enable_calc(const vtss_appl_mep_dm_prio_conf_t   dm[VTSS_APPL_MEP_PRIO_MAX])
{
    u32 i;

    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {  /* Calculate if any DM transmission is active */
        if (dm[i].enable) {
            return TRUE;
        }
    }
    return FALSE;
}

/*
 * Calculate the maximum frame size usable for LB and TST
 */
static u32 max_size_lb_tst_calc(const mep_instance_data_t *data, u32 max_size, u32 prio)
{
    switch (data->config.domain) {
        case VTSS_APPL_MEP_PORT:
            return data->config.vid != 0 ? max_size - 4 : max_size;
        case VTSS_APPL_MEP_VLAN:
            return max_size - 4;
        case VTSS_APPL_MEP_EVC:
            if (data->sat) {
                // Use SAT VID for check
                return data->sat_prio_conf[prio].vid == VTSS_MEP_MGMT_SAT_VID_UNTAGGED ? max_size : max_size - 4;
            } else {
                // Use normal configured VID for check
                return data->config.vid == 0 ? max_size - 4 : max_size - 8;
            }
        default:
            return max_size;
    }

    // Default return same size
    return max_size;
}

static void push_delay_data(delay_state_t *delay_state, u32 lastn, u32 in_de, u32 in_de_var, u32 *out_de, u32 *out_del_var)
{
    *out_de      = delay_state->delay[delay_state->next_prt];
    *out_del_var = delay_state->delay_var[delay_state->next_prt];

    delay_state->delay[delay_state->next_prt] = in_de;
    delay_state->delay_var[delay_state->next_prt] = in_de_var;

    delay_state->next_prt = (delay_state->next_prt + 1) % lastn;
}


static void run_tlv_change(u32 instance)
{
    mep_instance_data_t  *data = get_instance(instance);

    if (data && data->config.enable && data->cc_config.enable && data->cc_config.tlv_enable) {
        (void)run_cc_config(instance);
    }
}


static u32 megid_calc(mep_instance_data_t *data,  u8 *buf)
{
    u32  n_len, m_len, inx=0;

    switch (data->config.format) {
        case VTSS_APPL_MEP_ITU_ICC:
            buf[0] = 1;
            buf[1] = 32;
            buf[2] = 13;
            memcpy(&buf[3], data->config.meg, 13);
            inx = 16;
            break;
        case VTSS_APPL_MEP_ITU_CC_ICC:
            buf[0] = 1;
            buf[1] = 33;
            buf[2] = 15;
            memcpy(&buf[3], data->config.meg, 15);
            inx = 18;
            break;
        case VTSS_APPL_MEP_IEEE_STR:
            n_len = strlen(data->config.name);
            m_len = strlen(data->config.meg);
            if (n_len) { /* Maintenance Domain Name present */
                buf[inx++] = 4;
                buf[inx++] = n_len;
                memcpy(&buf[inx], data->config.name, n_len);
                inx += n_len;
            }
            else
                buf[inx++] = 1; /* No Maintenance Domain Name */

            buf[inx++] = 2;    /* Add Short MA name */
            buf[inx++] = m_len;
            memcpy(&buf[inx], data->config.meg, m_len);
            inx += m_len;
            break;
    }

    return(inx);    /* Return length of MEG-ID */
}

static u32 ccm_pdu_init(mep_instance_data_t *data,  u8 *pdu)
{
    u32                        off = CCM_PDU_TLV_OFFSET;
    u8                         ps_tlv_value, is_tlv_value;

    T_D("Enter  tlv_enable %u", data->cc_config.tlv_enable);

    pdu[0] = data->config.level << 5;
    pdu[1] = MESA_OAM_OPCODE_CCM;
    pdu[2] = (!data->cc_config.enable) ? rate_to_pdu_calc(data->lm_config.rate) : rate_to_pdu_calc(data->cc_config.rate);
    pdu[3] = 70;
    pdu[8] = (data->config.mep & 0xFF00) >> 8;
    pdu[9] = data->config.mep & 0xFF;

    (void)megid_calc(data, &pdu[10]);         /* Calculate transmitted MEG-ID */

    if (data->cc_config.tlv_enable) {
        ps_tlv_value = 2; /* Default Port Status is 'psUp' */
        if ((data->config.direction == VTSS_APPL_MEP_DOWN) && data->config.domain != VTSS_APPL_MEP_PORT) {    /* This is a Down-MEP (not Port) - Port status TLV must be added */
            ps_tlv_value = data->forwarding ? 2 : 1;  /* Port Status is 'psUp' if 'forwarding' */
        }
        is_tlv_value = 1; /* Default Interface Status is 'isUp' */
        if ((data->config.direction == VTSS_APPL_MEP_UP) && data->supp_state.defect_state.ciSsf) {   /* This is a Up-MEP - the CI-SSF (residence port state) is used */
            is_tlv_value = 2; /* LOS on Up-MEP residence port - Interface Status is 'isDown'*/
        }

        T_D("ps_tlv_value %u  is_tlv_value %u", ps_tlv_value, is_tlv_value);
        pdu[off++] = 2;    /* Insert Port Status TLV */
        pdu[off++] = 0;
        pdu[off++] = 1;
        pdu[off++] = ps_tlv_value;
        pdu[off++] = 4;    /* Insert Interface Status TLV */
        pdu[off++] = 0;
        pdu[off++] = 1;
        pdu[off++] = is_tlv_value;
        pdu[off++] = 31;   /* Insert Organization-Specific TLV */
        pdu[off++] = 0;
        pdu[off++] = 5;
        pdu[off++] = os_tlv_config.oui[0];
        pdu[off++] = os_tlv_config.oui[1];
        pdu[off++] = os_tlv_config.oui[2];
        pdu[off++] = os_tlv_config.subtype;
        pdu[off++] = os_tlv_config.value;
    }
    pdu[off++] = 0;    /* Insert End TLV */

    return(off);
}

static u32 run_config(mep_instance_data_t *data,  BOOL *config_ok)
{
    u32                     i;
    mep_model_mep_conf_t    mep_conf;
    mep_model_mep_create_t  mep_create;
    BOOL                    admin_state, create=FALSE;
    u32                     rc=0, instance = data->instance;

    T_N("Enter %u", data->instance);

    *config_ok = FALSE;

    if (data->config.enable) {
        if (!data->cc_config.enable) {
            data->cc_config.rate = VTSS_APPL_MEP_RATE_1S;
        }

        if (data->sat) {
            data->config.vid = VTSS_MEP_MGMT_VID_ALL;
        }
        if (!mep_model_mep_admin_state_get(data->instance, &admin_state)) {   /* Get admin state to check if already created */
            mep_create.type = model_type_calc(data->config.mode);
            mep_create.domain = model_domain_calc(data->config.domain, data->config.vid);
            mep_create.direction = model_direction_calc(data->config.direction);
            mep_create.flow_num = (data->config.domain != VTSS_APPL_MEP_PORT) ? data->config.flow : data->config.port;
            if ((rc = mep_model_mep_create(instance, &mep_create)) != VTSS_RC_OK) {  /* Create MEP */
                T_D("mep_model_mep_create failed  %s", error_txt(rc));
                return(rc);
            }
            create = TRUE;
        }

        if (!mep_model_mep_conf_get(instance, &mep_create, &mep_conf)) {   /* Get MEP configuration */
            T_D("mep_model_mep_conf_get failed");
            return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
        }
        mep_conf.hw = data->config.voe;
        if (!MPLS_DOMAIN(data->config.domain)) {
            if (!memcmp(all_zero_mac, data->config.mac.addr, sizeof(all_zero_mac)))   /* Check if the configured MAC is all zero */
                vtss_mep_mac_get(data->config.port, data->config.mac.addr);  /* Get MAC of residence port if all zero mac */
        }
        memcpy(mep_conf.mac, data->config.mac.addr, sizeof(mep_conf.mac));
        mep_conf.level = data->config.level;
        mep_conf.port = data->config.port;
        mep_conf.mepid = data->config.mep;
        mep_conf.vid = (data->config.vid == VTSS_MEP_MGMT_VID_ALL) ? MEP_MODEL_ALL_VID : data->config.vid;
        mep_conf.sat = data->sat;
        mep_conf.ll_is_used = data->config.ll_is_used;
        if ((rc = mep_model_mep_conf_set(instance, &mep_conf)) != VTSS_RC_OK) {   /* Set MEP configuration */
            T_D("mep_model_mep_conf_set failed  %s", error_txt(rc));
            if (create) { /* Configuration set during "create" then the instance must be deleted */
                if (!mep_model_mep_delete(instance)) {
                    T_D("mep_model_mep_delete failed");
                }
            }
            return(rc);
        }
        *config_ok = TRUE;
        if (create && (rc = mep_model_mep_admin_state_set(instance, TRUE)) != VTSS_RC_OK) {   /* Set admin state to 'enabled' in case of create instance */
            T_D("mep_model_mep_admin_state_set failed  %s", error_txt(rc));
            return(rc);
        }

        vtss_mep_signal_out(instance, data->config.domain);
        vtss_mep_sf_sd_state_set(instance, data->config.domain, data->out_state.state.mep.aTsf, data->out_state.state.mep.aTsd);
    } else {    /* Disable */
        for (i = 0; i < data->config.peer_count; ++i) {
            vtss_mep_lm_hli_status_set(instance, data->config.peer_mep[i], NULL);
        }

        data->lst_config.enable = FALSE;    /* Link state tracking must be disabled when MEP is deleted */
        lst_action(data);

        if (!mep_model_mep_delete(instance)) {
            T_D("mep_model_mep_delete failed");
        }

        if (data->lm_avail_fifo) {
            VTSS_FREE(data->lm_avail_fifo);
            data->lm_avail_fifo = NULL;
            data->lm_avail_fifo_ix = 0;
        }
        vtss_mep_lm_notif_status_set(instance, NULL);

        if (data->ais_tx_id) {
            delete data->ais_tx_id;
        }
        if (data->lck_tx_id) {
            delete data->lck_tx_id;
        }

        delete_timer(&data->lm_state.timer);
        delete_timer(&data->lt_state.timer);
        delete_timer(&data->lb_state.timer);
        delete_timer(&data->tst_state.timer);
        delete_timer(&data->lst_state.ign_timer);
        delete_timer(&data->lst_state.sf_timer);
        delete_timer(&data->syslog_state.timer);
        delete_timer(&data->dLevel.timer);
        delete_timer(&data->dMeg.timer);
        delete_timer(&data->dMep.timer);
        for (i = 0; i < mep_caps.mesa_oam_peer_cnt; ++i) {
            delete_timer(&data->dPeriod[i].timer);
            delete_timer(&data->dPrio[i].timer);
            delete_timer(&data->rx_ccm_timer[i]);
        }
        delete_timer(&data->dAis.timer);
        delete_timer(&data->dLck.timer);
        delete_timer(&data->dLoop.timer);
        delete_timer(&data->dConfig.timer);
        delete_timer(&data->los_int_cnt_timer);
        delete_timer(&data->los_th_cnt_timer);
        delete_timer(&data->hli_cnt_timer);
        delete_timer(&data->ais_cnt_timer);
        delete_timer(&data->tx_aps_timer);
        delete_timer(&data->rx_aps_timer);






        *config_ok = TRUE;
        rc = VTSS_RC_OK;
    }

    vtss_mep_status_change(instance, data->config.enable);

    T_N("Exit");

    return(rc);
}


static u32 run_cc_config(u32 instance)
{
    u32                   rc=0, i, tx_rate;
    mep_instance_data_t   *data;
    mep_model_ccm_conf_t  ccm_conf;
    mep_model_mep_info_t  info;
    mep_model_pdu_tx_t    pdu_tx = {};
    u8                    pdu[CCM_PDU_LENGTH];
    BOOL                  dual_ended_lm, enable_changed;

    T_D("Enter %u", instance);

    data = get_instance(instance);
    if (!data) {
        T_D("Instance not found");
        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    }

    data->exp_meg_len = megid_calc(data, data->exp_meg);         /* Calculate and save the expected MEG-ID */

    dual_ended_lm = (data->lm_config.enable && (data->lm_config.ended == VTSS_APPL_MEP_DUAL_ENDED)) ? TRUE : FALSE;
    if (!data->cc_config.enable && dual_ended_lm) {  /* If CC not enabled but dual ended LM enable - we set CC rate+prio to LM rate+prio */
        data->cc_config.rate = data->lm_config.rate;
        data->cc_config.prio = data->lm_config.prio;
    }
    if (data->cc_config.enable && dual_ended_lm) {   /* If CC and dual ended LM are enabled - we set LM rate+prio to CC rate+prio */
        data->lm_config.prio = data->cc_config.prio;
    }

    if (!mep_model_ccm_conf_get(instance, &ccm_conf)) {
        T_D("mep_model_ccm_conf_get failed");
        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    }

    enable_changed = (ccm_conf.enable != data->cc_config.enable) ? TRUE : FALSE;
    ccm_conf.enable    = data->cc_config.enable;
    ccm_conf.lm_enable = dual_ended_lm;
    ccm_conf.lm_rate   = model_rate_calc(data->lm_config.rate);
    ccm_conf.cc_rate   = model_rate_calc(data->cc_config.rate);
    ccm_conf.prio      = data->cc_config.prio;
    ccm_conf.peer_mep  = (data->config.peer_count > 1) ? MEP_MODEL_PEER_NONE : data->config.peer_mep[0];
    memset(ccm_conf.meg, 0, sizeof(ccm_conf.meg));
    memcpy(ccm_conf.meg, data->exp_meg, data->exp_meg_len);

    mep_model_tx_cancel(instance, &data->ccm_tx_id, NULL);

    if (!mep_model_ccm_conf_set(instance, &ccm_conf)) {
        T_D("mep_model_ccm_conf_set failed");
        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    }

    if (enable_changed) {   /* If enable changes, fault causes and consequent actions must be recalculated */
        new_defect_state(data);
    }

    memset(&info, 0, sizeof(info));     /* Get VOE CCM enabled information */
    if (!mep_model_mep_info_get(instance, &info)) {
        T_D("mep_model_mep_info_get failed");
        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    }

    if (data->voe_ccm_enabled != info.voe_ccm_enabled) {   /* Change from VOE to SW based CCM. In case event (from VOE) has set defect, no clear timer is running */
        data->supp_state.defect_state.dInv = FALSE;
        data->supp_state.defect_state.dLevel = FALSE;
        data->supp_state.defect_state.dMeg = FALSE;
        data->supp_state.defect_state.dMep = FALSE;
        data->supp_state.defect_state.dRdi.clear();
        data->supp_state.defect_state.dPeriod.clear();
        data->supp_state.defect_state.dPrio.clear();
        new_defect_state(data);
    }

    data->voe_ccm_enabled = info.voe_ccm_enabled;

    tx_rate = (!data->cc_config.enable)
            ? (dual_ended_lm ? frame_rate_calc(data->lm_config.rate) : 0)
            : frame_rate_calc(data->cc_config.rate);
    if (!data->voe_ccm_enabled && (tx_rate > frame_rate_calc(VTSS_APPL_MEP_RATE_1S))) {   /* In case VOE is not handling CCM the rate must be limited as all frames are copied to CPU */
        T_D("Invalid CCM rate");
        tx_rate = frame_rate_calc(VTSS_APPL_MEP_RATE_1S);
        data->cc_config.rate = data->lm_config.rate = VTSS_APPL_MEP_RATE_1S;
        rc = VTSS_APPL_MEP_RC_RATE_INTERVAL;
    }

    if (data->cc_config.enable || dual_ended_lm) { /* CCM is enabled or dual ended LM */
        memset(pdu, 0, sizeof(pdu));
        pdu_tx.len  = ccm_pdu_init(data, pdu);
        pdu_tx.data = pdu;
        pdu_tx.vid  = 0;
        pdu_tx.prio = data->cc_config.prio;
        pdu_tx.dei  = FALSE;
        pdu_tx.mac  = class1_multicast[data->config.level];
        pdu_tx.rate = tx_rate;
        pdu_tx.rate_is_kbps = FALSE;

        if (mep_model_tx(instance, &pdu_tx, &data->ccm_tx_id) != VTSS_RC_OK) {
            T_D("mep_model_tx failed");
        }
    }

    data->loc_timer_val = pdu_period_to_timer[rate_to_pdu_calc(data->cc_config.rate)];   /* Default LOC timer is equal to 1 sec period */

    /* Start or restart any used CCM RX timers */
    for (i=0; i<data->config.peer_count; ++i)
        mep_timer_start(data->loc_timer_val, &data->rx_ccm_timer[i], data, run_rx_ccm_timer);
    /* Stop any unused CCM RX timers */
    for (i=i; i<mep_caps.mesa_oam_peer_cnt; ++i)
        delete_timer(&data->rx_ccm_timer[i]);

    T_N("Exit");

    return(rc);
}

static u32 set_lm_model_conf(const mep_instance_data_t *data)
{
    mep_model_lm_conf_t   lm_conf;

    if (!mep_model_lm_conf_get(data->instance, &lm_conf)) {
        T_D("mep_model_lm_conf_get failed");
        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    }

    lm_conf.enable = data->lm_config.enable;
    lm_conf.flow_count = data->lm_config.flow_count;
    lm_conf.oam_count = model_count_calc(data->lm_config.oam_count);

    if (!mep_model_lm_conf_set(data->instance, &lm_conf)) {
        T_D("mep_model_lm_conf_set failed");
        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    }

    return VTSS_RC_OK;
}

static u32 set_sl_model_conf(const mep_instance_data_t *data)
{
    mep_model_sl_conf_t   sl_conf;

    if (!mep_model_sl_conf_get(data->instance, &sl_conf)) {
        T_D("mep_model_sl_conf_get failed");
        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    }

    sl_conf.enable_tx = data->lm_config.enable;

    /*
     * We do not enable SLM RX if TX single-ended is enabled, since it is not necessary.
     * A TX single-ended instance is capable of performing full near-to-far and
     * far-to-near measurement.
     */
    sl_conf.enable_rx =  data->lm_config.enable_rx && !(data->lm_config.enable && data->lm_config.ended == VTSS_APPL_MEP_SINGEL_ENDED);
    sl_conf.peer_count = data->config.peer_count;
    sl_conf.prio = data->lm_config.prio;
    sl_conf.test_id = data->lm_config.slm_test_id;

    memset(sl_conf.peer_mep, 0, sizeof(sl_conf.peer_mep));
    for (int i = 0; i < data->config.peer_count; i++) {
        sl_conf.peer_mep[i] = data->config.peer_mep[i];
    }

    if (!mep_model_sl_conf_set(data->instance, &sl_conf)) {
        T_D("mep_model_sl_conf_set failed");
        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    }

    return VTSS_RC_OK;
}

static u32 enable_lm_model_tx(mep_instance_data_t *data)
{
    u32                   rc = 0;
    u8                    pdu[LMM_PDU_LENGTH];
    mep_model_pdu_tx_t    pdu_tx = {};
    mep_model_ccm_conf_t  ccm_conf;

    if (data->lm_config.ended == VTSS_APPL_MEP_DUAL_ENDED) { /* LM based on SW generated CCM session */
        if ((rc = run_cc_config(data->instance)) != VTSS_RC_OK) {
            return(rc);
        }
    } else { /* LM based on LMM */
        if (!mep_model_ccm_conf_get(data->instance, &ccm_conf)) {
            T_D("mep_model_ccm_conf_get failed");
            return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
        }

        ccm_conf.lm_enable = FALSE;

        if (!mep_model_ccm_conf_set(data->instance, &ccm_conf)) {
            T_D("mep_model_ccm_conf_set failed");
            return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
        }

        /* Init of LMM transmitter frame */
        memset(pdu, 0, sizeof(pdu));
        pdu[0] = data->config.level << 5;
        pdu[1] = MESA_OAM_OPCODE_LMM;
        pdu[3] = 12;

        /* Start transmitting */
        pdu_tx.len  = LMM_PDU_LENGTH;
        pdu_tx.data = pdu;
        pdu_tx.vid  = 0;
        pdu_tx.prio = data->lm_config.prio;
        pdu_tx.dei  = FALSE;
        if (data->lm_config.cast == VTSS_APPL_MEP_UNICAST) {
            pdu_tx.mac = data->config.peer_mac[0].addr;
        } else {
            pdu_tx.mac = class1_multicast[data->config.level];
        }
        pdu_tx.rate = frame_rate_calc(data->lm_config.rate);
        pdu_tx.rate_is_kbps = FALSE;
        if (mep_model_tx(data->instance, &pdu_tx, &data->lmm_tx_id) != VTSS_RC_OK) {
            T_D("mep_model_tx failed");
        }
    }

    return VTSS_RC_OK;
}

static u32 enable_sl_model_tx(mep_instance_data_t *data)
{
    u32                     pdu_size = data->lm_config.size - (12+2+4);
    u8                      pdu[pdu_size];
    mep_model_pdu_tx_t      pdu_tx = {};

    /* Init of LMM transmitter frame */
    memset(pdu, 0, sizeof(pdu));
    pdu[0] = data->config.level << 5;
    pdu[1] = data->lm_config.ended == VTSS_APPL_MEP_DUAL_ENDED ? MESA_OAM_OPCODE_1SL : MESA_OAM_OPCODE_SLM;
    pdu[3] = SLM_TLV_OFFSET;

    // Setup Source MEP ID
    pdu[4] = (u8)((data->config.mep&0xFF00) >> 8);
    pdu[5] = (u8)((data->config.mep&0x00FF));

    pdu[8]  = (data->lm_config.slm_test_id & 0xFF000000) >> 24;
    pdu[9]  = (data->lm_config.slm_test_id & 0x00FF0000) >> 16;
    pdu[10] = (data->lm_config.slm_test_id & 0x0000FF00) >> 8;
    pdu[11] = (data->lm_config.slm_test_id & 0x000000FF);

    if (pdu_size > SLM_PDU_MIN_LENGTH) {
        u32 pattern_size = pdu_size - SLM_PDU_MIN_LENGTH;
        pdu[20] = 3;  /* Data TLV */
        pdu[21] = (pattern_size & 0xFF00)>>8;
        pdu[22] = (pattern_size & 0xFF);
        for (int i = 0; i < pattern_size; ++i) {
            pdu[23+i] = 0xAA;
        }
    }

    // No need to set the End TLV specifically as we have zero'ed the whole PDU for starters

    /* Start transmitting */
    pdu_tx.len  = pdu_size;
    pdu_tx.data = pdu;
    pdu_tx.vid  = 0;
    pdu_tx.prio = data->lm_config.prio;
    pdu_tx.dei  = data->lm_config.dei;
    if (data->lm_config.cast == VTSS_APPL_MEP_UNICAST) {
        pdu_tx.mac = data->config.peer_mac[data->lm_state.unicast_peer_index].addr;
    } else {
        pdu_tx.mac = class1_multicast[data->config.level];
    }
    pdu_tx.rate = frame_rate_calc(data->lm_config.rate);
    pdu_tx.rate_is_kbps = FALSE;
    if (mep_model_tx(data->instance, &pdu_tx, &data->slm_tx_id) != VTSS_RC_OK) {
        T_D("mep_model_tx failed");
    }

    return VTSS_RC_OK;
}

static mep_model_clear_dir_t mep_model_lm_clear_dir_get(vtss_appl_mep_clear_dir direction)
{
    return direction == VTSS_APPL_MEP_CLEAR_DIR_BOTH ? MEP_MODEL_CLEAR_DIR_BOTH :
           direction == VTSS_APPL_MEP_CLEAR_DIR_TX ? MEP_MODEL_CLEAR_DIR_TX : MEP_MODEL_CLEAR_DIR_RX;
}

static u32 run_lm_config(u32 instance)
{
    u32                   rc=0;
    mep_instance_data_t   *data;
    BOOL                  do_start_lm_timer = FALSE;

    T_N("Enter %u", instance);

    data = get_instance(instance);
    if (!data) {
        T_D("Instance not found");
        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    }

    // Set specific model configuration
    if (data->lm_config.synthetic) {
        if ((rc = set_sl_model_conf(data)) != VTSS_RC_OK) {
            return rc;
        }
    } else {
        if ((rc = set_lm_model_conf(data)) != VTSS_RC_OK) {
            return rc;
        }
    }

    // Cancel any ongoing transmissions
    mep_model_tx_cancel(instance, &data->lmm_tx_id, NULL);
    mep_model_tx_cancel(instance, &data->slm_tx_id, NULL);
    if (!data->cc_config.enable) {
        mep_model_tx_cancel(instance, &data->ccm_tx_id, NULL);
    }

    if (data->lm_state.clear_counters) {
        // Clear counters if instructed to do so (typically only when we enable TX or RX))
        data->out_state.lm_state.clear();
        data->lm_peer_state.clear();

        for (int peer_idx = 0; peer_idx < mep_caps.mesa_oam_peer_cnt; peer_idx++) {
            /* This is to know when the first LM counters are received. The first counters are only used to get 'previous' frame counters initialized */
            data->lm_peer_state[peer_idx].lm_start = TRUE;
        }

        if (!mep_model_lm_status_clear(instance, data->lm_config.synthetic, mep_model_lm_clear_dir_get(data->lm_state.clear_dir))) {
            T_D("mep_model_lm_status_clear failed");
        }
    }

    /*
     * The LM interval timer is only used in the following circumstances:
     * - This is synthetic LM. Service based LM uses a different approach.
     * - This is either a single-ended initiator or a receiver. In both cases
     *   we expect to receive PDUs and calculate loss based on those.
     *   We thus don't use the timer for dual-ended initiators.
     */
    do_start_lm_timer = data->lm_config.synthetic &&
            ((data->lm_config.enable && data->lm_config.ended == VTSS_APPL_MEP_SINGEL_ENDED) ||
              data->lm_config.enable_rx);

    if (do_start_lm_timer) {
        mep_timer_start(data->lm_config.meas_interval, &data->lm_state.timer, data, run_slm_timer);
        mep_model_sl_meas_expired(data->instance);
    } else {
        mep_timer_cancel(data->lm_state.timer);

        // Clear interval counters
        for (int peer_idx = 0; peer_idx < mep_caps.mesa_oam_peer_cnt; peer_idx++) {
            data->lm_peer_state[peer_idx].flr_interval_count = 0;
            data->out_state.lm_state[peer_idx].flr_interval_elapsed = 0;
        }
    }

    if (data->lm_config.enable) {
        if (data->lm_config.synthetic) {
            /* Enable SLM TX */
            if ((rc = enable_sl_model_tx(data)) != VTSS_RC_OK)
                return rc;
        } else {
            /* Enable LM TX */
            if ((rc = enable_lm_model_tx(data)) != VTSS_RC_OK)
                return rc;
        }

    } else {
        /* LM TX is disabled - we do nothing */
    }

    T_N("Exit");

    return(VTSS_RC_OK);
}

static void run_dm_config(u32 instance,  BOOL force)
{
    u32                  i;
    BOOL                 single;
    mep_instance_data_t  *data;
    mep_model_pdu_tx_t   pdu_tx = {};
    u8                   pdu[DMM_PDU_LENGTH];

    T_D("Enter  Instance %u  force %u", instance, force);

    data = get_instance(instance);
    if (!data) { return; }

    single = (data->dm_config.common.ended == VTSS_APPL_MEP_SINGEL_ENDED) ? TRUE : FALSE;

    /* Init of DM transmitted PDU */
    memset(pdu, 0, sizeof(pdu));
    pdu[0] = data->config.level << 5;
    pdu[1] = single ? MESA_OAM_OPCODE_DMM : MESA_OAM_OPCODE_1DM;
    pdu[2] = 0;
    pdu[3] = single ? 32 : 16;

    pdu_tx.len  = single ? DMM_PDU_LENGTH : DM1_PDU_LENGTH;
    pdu_tx.data = pdu;
    pdu_tx.vid  = 0;
    pdu_tx.prio = 0;
    pdu_tx.dei  = FALSE;
    if (data->dm_config.common.cast == VTSS_APPL_MEP_UNICAST) {
        for (i=0; i<data->config.peer_count; ++i)
            if (data->dm_config.common.mep == data->config.peer_mep[i]) break;
        if (i == data->config.peer_count)                       return;
        pdu_tx.mac = data->config.peer_mac[i].addr;
    } else {
        pdu_tx.mac = class1_multicast[data->config.level];
    }
//    pdu_tx.rate = data->dmm_config.interval * 10 * 1000;    /* In microseconds */
    pdu_tx.rate = 60*60*100/data->dm_config.common.interval;
    pdu_tx.rate_is_kbps = FALSE;

    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        if (data->dm_config.dm[i].enable) {     /* Transmission of DM is enabled */
            if (!force && data->dm_enabled[i]) {
                continue;   /* DM on this prio is already enabled */
            }
            data->dm_enabled[i] = TRUE;

            /* Initialize state on newly enabled prio */
            memset(&data->supp_state.tw_state[i], 0, sizeof(data->supp_state.tw_state[i]));
            memset(&data->supp_state.fn_state[i], 0, sizeof(data->supp_state.fn_state[i]));
            memset(&data->supp_state.nf_state[i], 0, sizeof(data->supp_state.nf_state[i]));

            memset(&data->out_state.tw_state[i], 0, sizeof(data->out_state.tw_state[i]));
            memset(&data->out_state.fn_state[i], 0, sizeof(data->out_state.fn_state[i]));
            memset(&data->out_state.nf_state[i], 0, sizeof(data->out_state.nf_state[i]));

            data->out_state.tw_state[i].min_delay = 0xffffffff;
            data->out_state.tw_state[i].min_delay_var = 0xffffffff;
            data->out_state.fn_state[i].min_delay = 0xffffffff;
            data->out_state.fn_state[i].min_delay_var = 0xffffffff;
            data->out_state.nf_state[i].min_delay = 0xffffffff;
            data->out_state.nf_state[i].min_delay_var = 0xffffffff;

            if (!mep_model_dm_status_clear(data->instance, i)) {
                T_D("mep_model_dm_status_clear failed");
            }

            pdu_tx.vid  = (data->sat) ? ((data->sat_prio_conf[i].vid == VTSS_MEP_MGMT_SAT_VID_UNTAGGED) ? MEP_MODEL_VID_UNTAGGED : data->sat_prio_conf[i].vid) : 0;
            pdu_tx.prio = i;
            T_DG(TRACE_GRP_DM, "TX start  prio %u", i);
            if (mep_model_tx(data->instance, &pdu_tx, &data->dm_tx_id[i]) != VTSS_RC_OK ) {
                T_D("mep_model_tx failed");
            }
        } else {
            data->dm_enabled[i] = FALSE;
            T_DG(TRACE_GRP_DM, "TX stop  prio %u", i);
            mep_model_tx_cancel(data->instance, &data->dm_tx_id[i], NULL);
        }

        /* set DM state time unit */
        data->out_state.tw_state[i].tunit = data->dm_config.common.tunit;
        data->out_state.fn_state[i].tunit = data->dm_config.common.tunit;
        data->out_state.nf_state[i].tunit = data->dm_config.common.tunit;
    } /* for to */
}

static u32 apply_aps_config(u32 instance)
{
    mep_instance_data_t *inst, *this_inst = get_instance(instance);
    u32 mep_cnt = 0, forward = 0, octet = 0, mep[2] = {0}, next = 0, vid, i;
    mesa_port_list_t control_vlan;

    if (!this_inst) {
        T_E("Instance not found");
        return(VTSS_RC_ERROR);
    }

    vid = (this_inst->config.domain == VTSS_APPL_MEP_PORT) ? this_inst->config.vid : this_inst->config.flow;

    if (!mep_vlan_member_get(vid, control_vlan)) {  /* In case of interconnected rings, the ports in this (control) VLAN contains at least the ports in the master ring as it is a protected VLAN */
        T_E("Could not get ERPS VLAN %d", vid);
        return(VTSS_RC_ERROR);
    }

    while ((inst = get_instance_created(&next)) != NULL) {
        if (!mep_cnt) {
            octet = inst->aps_config.raps_octet;
        }
        if (inst->config.enable && inst->aps_config.enable && ((inst->config.vid == vid) || (inst->config.flow == vid)) &&
            (inst->aps_config.type == VTSS_APPL_MEP_R_APS) && (octet == inst->aps_config.raps_octet)) {     /* Find MEPs with RAPS enabled */
            T_D("RAPS found  Instance %d  forward %u", inst->instance, inst->raps_forward);

            control_vlan[inst->config.port] = TRUE;    /* The port with the MEP should in the control VLAN */

            if (inst->raps_forward) {   /* Count the number of MEPs that has RAPS forwarding enabled */
                forward++;
            }

            mep[mep_cnt] = inst->instance;
            mep_cnt++;
            if (mep_cnt == 2) {     /* There can max be two MEPs with RAPS enabled on a VID */
                break;
            }
        }
    }

    T_D("mep_cnt %d  forward %u", mep_cnt, forward);

    for (i = 0; i < mep_cnt; ++i) {
        /* enable MEP forwarding if there are 2 MEPs forward enabled */
        if (mep_model_raps_forwarding(mep[i], (forward == 2)) != VTSS_RC_OK) {
            T_E("mep_model_raps_forwarding (MEP forwarding) failed");
            return(VTSS_RC_ERROR);
        }
    }

    /* Add control VLAN for the RAPS PDUs - should it be checked if other user has configured this VLAN???? */
    if (!mep_vlan_member_set(vid, control_vlan, (forward == 2))) {
        T_E("Could not %s ERPS VLAN %d",(mep_cnt == 2) ? "add" : "delete", vid);
        return(VTSS_RC_ERROR);
    }

    return(VTSS_RC_OK);
}

static void run_aps_config(u32 instance)
{
    if (apply_aps_config(instance) != VTSS_RC_OK) {
        T_E("Could not set aps config for instance %d", instance);
    }
}


static void tx_ltm(mep_instance_data_t *data)
{
    mep_model_pdu_tx_t    pdu_tx = {};
    u8                    pdu[LTM_PDU_LENGTH], tmac[6];
    u32                   i;
    mep_model_pdu_tx_id_t tx_id = MEP_MODEL_PDU_TX_ID_NONE;
    if (data->lt_config.ttl == 0) return; // TTL is 0, so silently ignore

    if (!memcmp(all_zero_mac, data->lt_config.mac.addr, sizeof(all_zero_mac))) {
        /* All zero mac - mep must be a known peer mep */
        for (i=0; i<data->config.peer_count; ++i)
            if (data->lt_config.mep == data->config.peer_mep[i])
                break;
        if (i == data->config.peer_count)
            return;
        memcpy(tmac, data->config.peer_mac[i].addr, sizeof(tmac));
    } else
        memcpy(tmac, data->lt_config.mac.addr, sizeof(tmac));

    /* Init of LTM transmitter frame: */
    pdu[0] = data->config.level << 5;
    pdu[1] = MESA_OAM_OPCODE_LTM;
    pdu[2] = 0x80;
    pdu[3] = 17;
    var_to_string(&pdu[4], data->lt_state.transaction_id);
    pdu[8] = data->lt_config.ttl;
    memcpy(&pdu[9], data->config.mac.addr, 6);
    memcpy(&pdu[15], tmac, 6);
    pdu[21] = 7;
    pdu[22] = 0;
    pdu[23] = 8;
    pdu[24] = 0;
    pdu[25] = 0;
    memcpy(&pdu[26], data->config.mac.addr, 6);
    pdu[32] = 0;

    pdu_tx.mac          = class1_multicast[data->config.level];
    pdu_tx.mac[5]       |= 0x08;  /* This makes it a class2 multicast address */
    pdu_tx.len          = sizeof(pdu);
    pdu_tx.data         = pdu;
    pdu_tx.vid          = 0;
    pdu_tx.prio         = data->lt_config.prio;
    pdu_tx.dei          = FALSE;
    pdu_tx.line_rate    = FALSE;
    pdu_tx.rate         = 0;
    pdu_tx.rate_is_kbps = FALSE;

    if (mep_model_tx(data->instance, &pdu_tx, &tx_id) != VTSS_RC_OK) {
        T_DG(TRACE_GRP_TX_FRAME, "mep_model_tx failed");
    }
}

static void run_lt_config(mep_instance_data_t *data)
{
    mep_model_lt_conf_t  conf;

    T_D("Enter  Instance %u", data->instance);

    conf.copy_ltr_to_cpu = data->lt_config.enable;
    if (!mep_model_lt_conf_set(data->instance, &conf)) {
        T_D("mep_model_lt_conf_set(%u) failed", data->instance);
    }
    if (data->lt_config.enable) {
        data->lt_state.transaction_id++;
        mep_timer_start(5000, &data->lt_state.timer, data, run_lt_timer);
        memset(&data->out_state.lt_state, 0, sizeof(data->out_state.lt_state));
        data->out_state.lt_state.transaction[0].transaction_id = data->lt_state.transaction_id;
        tx_ltm(data);
    } else {
        delete_timer(&data->lt_state.timer);
    }
}

static void ltr_to_peer(const mep_instance_data_t *data, const mep_model_pdu_rx_t *rx_pdu, BOOL forward, BOOL mep, u8 action)
{
    mep_model_pdu_tx_t    pdu_tx = {};
    u8                    pdu[LTR_PDU_LENGTH];
    mep_model_pdu_tx_id_t tx_id = MEP_MODEL_PDU_TX_ID_NONE;

    if (rx_pdu->data[8]==0) return; // TTL is 0, silently ignore

    /* Generate LTR PDU: */
    pdu[0] = rx_pdu->data[0];
    pdu[1] = MESA_OAM_OPCODE_LTR;
    pdu[2] = (rx_pdu->data[2] & 0x80) | ((mep) ? 0x20 : 0x00) | ((forward) ? 0x40 : 0x00);
    pdu[3] = 6;
    memcpy(&pdu[4], &rx_pdu->data[4], 4);                             /* Transaction ID */
    pdu[8] = rx_pdu->data[8]-1;                                       /* Decrement TTL */
    pdu[9] = action;                                                  /* Relay action */
    pdu[10] = 8;
    pdu[11] = 0;
    pdu[12] = 16;
    memcpy(&pdu[13], &rx_pdu->data[24], 8);                           /* Last Egress Identifier */
    pdu[21] = 0;
    pdu[22] = 0;
    memcpy(&pdu[23], data->config.mac.addr, 6);                       /* Next Egress Identifier */
    pdu[29] = (data->config.direction == VTSS_APPL_MEP_DOWN) ? 5 : 6; /* down/up reply */
    pdu[30] = 0;
    pdu[31] = 7;
    pdu[32] = 1;     /* down/up action is OK */
    memcpy(&pdu[33], data->config.mac.addr, 6);                       /* down/up Identifier */
    pdu[39] = 0;

    pdu_tx.mac          = &rx_pdu->data[9];
    pdu_tx.len          = sizeof(pdu);
    pdu_tx.data         = pdu;
    pdu_tx.vid          = 0;
    pdu_tx.prio         = rx_pdu->prio;
    pdu_tx.dei          = FALSE; // TBD no rx_pdu->dei
    pdu_tx.line_rate    = FALSE;
    pdu_tx.rate         = 0;
    pdu_tx.rate_is_kbps = FALSE;

    if (mep_model_tx(data->instance, &pdu_tx, &tx_id) != VTSS_RC_OK) {
        T_DG(TRACE_GRP_TX_FRAME, "mep_model_tx failed");
    }
}

static void ltm_to_forward(const mep_instance_data_t *data, const mep_model_pdu_rx_t *rx_pdu, u8 dmac[])
{
    mep_model_pdu_tx_t    pdu_tx = {};
    u8                    pdu[LTM_PDU_LENGTH];
    mep_model_pdu_tx_id_t tx_id = MEP_MODEL_PDU_TX_ID_NONE;

    if (rx_pdu->data[8] <= 1)
        return;  /* Don't forward with a 'zero' TTL */

    /* Generate LTM PDU: */
    memcpy(pdu, rx_pdu->data, LTM_PDU_LENGTH);
    pdu[8] -= 1;             /* Decrement TTL */
    pdu[21] = 7;             /* Egress identifier */
    pdu[22] = 0;
    pdu[23] = 8;
    pdu[24] = 0;
    pdu[25] = 0;
    memcpy(&pdu[26], data->config.mac.addr, 6);
    pdu[32] = 0;

    pdu_tx.mac          = dmac;
    pdu_tx.len          = sizeof(pdu);
    pdu_tx.data         = pdu;
    pdu_tx.vid          = 0;
    pdu_tx.prio         = rx_pdu->prio;
    pdu_tx.dei          = FALSE; // TBD no rx_pdu->dei
    pdu_tx.line_rate    = FALSE;
    pdu_tx.rate         = 0;
    pdu_tx.rate_is_kbps = FALSE;
    pdu_tx.reverse_inj  = TRUE;

    if (mep_model_tx(data->instance, &pdu_tx, &tx_id) != VTSS_RC_OK) {
        T_DG(TRACE_GRP_TX_FRAME, "mep_model_tx failed");
    }
}

static void lbr_to_peer(const mep_instance_data_t *data, const mep_model_pdu_rx_t *rx_pdu, BOOL is_mpls)
{
    mep_model_pdu_tx_t    pdu_tx = {};
    u8                    *pdu;
    mep_model_pdu_tx_id_t tx_id = MEP_MODEL_PDU_TX_ID_NONE;


    if (is_mpls) {
        return;
    }

    pdu = (u8 *)VTSS_MALLOC(rx_pdu->len);
    if (!pdu) {
        return;
    }
    /* Generate LBR PDU: */
    memcpy(pdu, rx_pdu->data, rx_pdu->len);
    pdu[1] = MESA_OAM_OPCODE_LBR;


































    pdu_tx.mac          = rx_pdu->smac;
    pdu_tx.len          = rx_pdu->len;
    pdu_tx.data         = pdu;
    pdu_tx.vid          = 0;
    pdu_tx.prio         = rx_pdu->prio;
    pdu_tx.dei          = FALSE; // TBD no rx_pdu->dei
    pdu_tx.line_rate    = FALSE;
    pdu_tx.rate         = 0;
    pdu_tx.rate_is_kbps = FALSE;

    if (mep_model_tx(data->instance, &pdu_tx, &tx_id) != VTSS_RC_OK) {
        T_DG(TRACE_GRP_TX_FRAME, "mep_model_tx failed");
    }
    VTSS_FREE(pdu);
}

static u32 tx_lbm(mep_instance_data_t *data,  BOOL infinite,  BOOL force)
{
    mep_model_pdu_tx_t             pdu_tx = {};
    u8                             pdu[VTSS_APPL_MEP_LBM_SIZE_MAX], pattern;
    u32                            i, j, k, pattern_idx, pattern_size, payload_size, pdu_size, offs, rc = VTSS_RC_OK, rc_not_ok;
    vtss_appl_mep_test_prio_conf_t *test;

    T_DG(TRACE_GRP_LB, "Enter  instance %u  force %u", data->instance, force);

    if (data->lb_config.common.cast == VTSS_APPL_MEP_UNICAST && !MPLS_DOMAIN(data->config.domain)) {
        if (!memcmp(all_zero_mac, data->lb_config.common.mac.addr, sizeof(all_zero_mac))) {
            /* All zero mac - mep must be a known peer mep */
            for (i=0; i<data->config.peer_count; ++i)
                if (data->lb_config.common.mep == data->config.peer_mep[i]) break;
            if (i == data->config.peer_count) {
                T_D("peer_mep %u not found instance=%u", data->lb_config.common.mep, data->instance);
                return VTSS_APPL_MEP_RC_PEER_ID;
            }
            pdu_tx.mac = data->config.peer_mac[i].addr;
        } else
            pdu_tx.mac = data->lb_config.common.mac.addr;
    } else
        pdu_tx.mac = class1_multicast[data->config.level];

    offs = MPLS_DOMAIN(data->config.domain) ? 28 : 0;
    for (i = 0; i < VTSS_APPL_MEP_PRIO_MAX; ++i) {
        for (j = 0; j < VTSS_APPL_MEP_TEST_MAX; ++j) {
            if (!data->lb_config.tests[i][j].enable || (!force && data->lb_enabled[i][j]))  continue;
            /* Init of LBM transmitted PDU: */
            test = &data->lb_config.tests[i][j];
            payload_size = test->size - (12+2+4); /* Payload size is configured frame size subtracted untagged overhead and CRC */
            pdu_size = LBM_PDU_LENGTH + 3 + offs;
            if (payload_size > pdu_size)   /* Check if pattern must be added to TLV - requested payload is bigger than LBM PDU with empty TLV */
                pattern_size = payload_size - pdu_size;
            else
                pattern_size = 0;
            pdu[0] = data->config.level << 5;
            pdu[1] = MESA_OAM_OPCODE_LBM;
            pdu[2] = 0;
            pdu[3] = 4;
            pdu[4] = 0;
            pdu[5] = 0;
            pdu[6] = 0;
            pdu[7] = 0;



























            pdu[offs+8] = 3;  /* Data TLV */
            pdu[offs+9] = (pattern_size & 0xFF00)>>8;
            pdu[offs+10] = (pattern_size & 0xFF);
            pattern_idx = 11;
            switch (test->pattern) {
            case VTSS_APPL_MEP_PATTERN_ALL_ZERO: pattern = 0; break;
            case VTSS_APPL_MEP_PATTERN_ALL_ONE:  pattern = 0xFF; break;
            case VTSS_APPL_MEP_PATTERN_0XAA:     pattern = 0xAA; break;
            default:                             pattern = 0; break;
            }
            for (k=0; k<pattern_size; ++k)     pdu[offs+pattern_idx+k] = pattern;
            pdu[offs+pattern_idx+k] = 0;

            pdu_tx.len          = pdu_size + pattern_size;
            pdu_tx.data         = pdu;
            pdu_tx.vid          = (data->sat) ? data->sat_prio_conf[i].vid : 0;
            pdu_tx.prio         = i;
            pdu_tx.dei          = (test->dp != VTSS_APPL_MEP_DP_GREEN);
            pdu_tx.line_rate    = (data->config.domain == VTSS_APPL_MEP_PORT);
            if (infinite) { /* This is HW based generation of LBM - AFI frame rate */
                pdu_tx.rate         = test->rate;
                pdu_tx.rate_is_kbps = TRUE;
            } else {
                /* SW based generation of LBM */
                pdu_tx.rate         = 0;
                pdu_tx.rate_is_kbps = FALSE;
            }

            T_DG(TRACE_GRP_LB, "TX start prio %u  test %u  offs %u  test->size %u  test->rate %u", i, j, offs, test->size, test->rate);
            if ((rc_not_ok = mep_model_tx(data->instance, &pdu_tx, &data->lbm_tx_id[i][j])) != VTSS_RC_OK) {
                T_DG(TRACE_GRP_LB, "mep_model_tx failed");
                rc = rc_not_ok;
                data->lb_enabled[i][j] = FALSE;
            } else {
                data->lb_enabled[i][j] = TRUE;
            }
        }
    }
    T_DG(TRACE_GRP_LB, "Exit");
    return rc;
}

static u32 run_lb_config(u32 instance,  BOOL force, u64 *active_time_ms)
{
    u32                            i, j, rc = VTSS_RC_OK, rc_not_ok;
    mep_instance_data_t            *data;
    mep_model_lb_conf_t            lb_conf;
    mep_model_lb_status_t          status;
    BOOL                           infinite, enable = FALSE;

    T_DG(TRACE_GRP_LB, "Enter  Instance %u  Force %u", instance, force);

    data = get_instance(instance);
    if (!data) { return VTSS_APPL_MEP_RC_INVALID_INSTANCE; }

    infinite = (data->lb_config.common.to_send == VTSS_APPL_MEP_LB_TO_SEND_INFINITE) ? TRUE : FALSE;

    /* Cancel any Tx disabled LBM: */
    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            if (data->lb_config.tests[i][j].enable)  { enable = TRUE; continue; }
            /* TX is no longer enabled */
            if (data->lb_enabled[i][j]) {
                data->lb_enabled[i][j] = FALSE;
                mep_model_tx_cancel(instance, &data->lbm_tx_id[i][j], active_time_ms);
                T_DG(TRACE_GRP_LB, "TX stop prio %u  test %u", i, j);
            }
        }
    }
    /* Configure LBM: */
    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        if ((data->lb_config.tests[i][0].enable || data->lb_config.tests[i][1].enable) != (data->lb_enabled[i][0] || data->lb_enabled[i][1])) {  /* Change in enable */
            if (data->lb_config.tests[i][0].enable || data->lb_config.tests[i][1].enable) { /* Just enabled */
                /* clear LB stats: */
                T_DG(TRACE_GRP_LB, "Clear status prio %u", i);
                if (!mep_model_lb_status_clear(instance, i)) {
                    T_DG(TRACE_GRP_LB, "mep_model_lb_status_clear(%u) failed", instance);
                }
                status.trans_id = 0;
                (void)mep_model_lb_status_get(instance, i, &status);
                for (j = 0; j < mep_caps.mesa_oam_peer_cnt; ++j)
                    data->lb_state.rx_transaction_id[i][j] = status.trans_id;
                memset(&data->out_state.lb_state[i], 0, sizeof(data->out_state.lb_state[i]));
            }
        }
    }
    if (!enable) {
        delete_timer(&data->lb_state.timer);
    }
    lb_conf.copy_lbr_to_cpu = enable && !infinite;
    lb_conf.enable = enable;
    if (!mep_model_lb_conf_set(instance, &lb_conf)) {
        T_DG(TRACE_GRP_LB, "mep_model_lb_conf_set(%u) failed", instance);
    }

    /* Start any Tx enabled LBM: */
    if ((rc_not_ok = tx_lbm(data, infinite, force)) != VTSS_RC_OK) {
        /* Failed. Clean up before leaving */
        lb_conf.copy_lbr_to_cpu = FALSE;
        lb_conf.enable = FALSE;
        rc = rc_not_ok;
        if (!mep_model_lb_conf_set(instance, &lb_conf)) {
            T_DG(TRACE_GRP_LB, "mep_model_lb_conf_set(%u) failed", instance);
        }
    }

    if (enable && !infinite) {
        data->lb_state.lbm_tx_count = 1;
        if (data->lb_state.lbm_tx_count < data->lb_config.common.to_send) {
            mep_timer_start(10 * data->lb_config.common.interval, &data->lb_state.timer, data, run_lb_timer);
        } else {
            /* last LBM sent, now wait 5 sec for LBR's: */
            mep_timer_start(5000, &data->lb_state.timer, data, run_lb_timer);
        }
    }

    T_DG(TRACE_GRP_LB, "Exit");
    return rc;
}

static void tx_ais(mep_instance_data_t *data, u32 rate)
{
    mep_model_pdu_tx_id_t tx_id = MEP_MODEL_PDU_TX_ID_NONE;
    mep_model_pdu_tx_t    pdu_tx = {};
    u8                    pdu[AIS_PDU_LENGTH];

    T_DG(TRACE_GRP_AIS_LCK, "Enter  Instance %u  rate %u, ais_config.rate %u", data->instance, rate, data->ais_config.rate);

    /* Init of AIS transmitter frame: */
    pdu[1] = MESA_OAM_OPCODE_AIS;
    pdu[2] = rate_to_pdu_calc(data->ais_config.rate);
    pdu[3] = 0;
    pdu[4] = 0;

    pdu_tx.len          = sizeof(pdu);
    pdu_tx.data         = pdu;
    pdu_tx.dei          = FALSE;
    pdu_tx.line_rate    = FALSE;
    pdu_tx.rate         = rate;
    pdu_tx.rate_is_kbps = FALSE;
    pdu_tx.reverse_inj  = TRUE;
    pdu_tx.client_inj   = TRUE;

    for (u32 i = 0; i < data->client_config.flow_count; i++) {
        pdu[0]             = data->client_config.level[i] << 5;
        pdu_tx.mac         = class1_multicast[data->client_config.level[i]];
        pdu_tx.vid         = 0;
        pdu_tx.client_flow.domain  = model_domain_calc(data->client_config.domain[i], 0);
        pdu_tx.client_flow.flow    = data->client_config.flows[i];
        pdu_tx.prio                = data->client_config.ais_prio[i] == VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST ? highest_client_prio_calc(&pdu_tx.client_flow, data->config.port) : data->client_config.ais_prio[i];

        if (mep_model_tx(data->instance, &pdu_tx, &tx_id) != VTSS_RC_OK) {
            T_DG(TRACE_GRP_TX_FRAME, "mep_model_tx failed");
        } else if (rate) {
            (*data->ais_tx_id).push_back(tx_id);
        }
        tx_id = MEP_MODEL_PDU_TX_ID_NONE;
    }

    T_DG(TRACE_GRP_AIS_LCK, "Exit");
}

static void run_ais_cnt_timer(u32 instance, struct vtss::Timer *timer)
{
    mep_instance_data_t         *data;

    T_DG(TRACE_GRP_AIS_LCK, "Enter  instance %u", instance);

    data = get_instance(instance);
    if (!data) { return; }

    tx_ais(data, 0);
    data->ais_cnt--;
    if (data->ais_cnt > 0) {
        mep_timer_start(3, &data->ais_cnt_timer, data, run_ais_cnt_timer);
    }
}

static void run_ais_config(u32 instance)
{
    mep_instance_data_t *data;

    T_DG(TRACE_GRP_AIS_LCK, "Enter  Instance %u", instance);

    data = get_instance(instance);
    if (!data) { return; }

    data = get_instance(instance);
    if (!data) { return; }

    if (!(*data->ais_tx_id).empty()) {
        /* Stop all AIS Tx: */
        for (auto &id : *data->ais_tx_id) {
            mep_model_tx_cancel(instance, &id, NULL);
        }
        (*data->ais_tx_id).clear();
    }
    if (data->ais_config.enable && data->out_state.state.mep.aTsf) {
        /* Start / restart AIS: */
        tx_ais(data, frame_rate_calc(data->ais_config.rate));
        if (data->ais_config.protection) {
            /* transmit 3 fast AIS frames (spaced 3.3 ms) */
            data->ais_cnt = 2;
            mep_timer_start(3, &data->ais_cnt_timer, data, run_ais_cnt_timer);
        }
    }

    T_DG(TRACE_GRP_AIS_LCK, "Exit");
}

static void tx_lck(mep_instance_data_t *data, u32 rate)
{
    mep_model_pdu_tx_id_t tx_id = MEP_MODEL_PDU_TX_ID_NONE;
    mep_model_pdu_tx_t    pdu_tx = {};
    u8                    pdu[LCK_PDU_LENGTH];

    T_DG(TRACE_GRP_AIS_LCK, "Enter  Instance %u  rate %u", data->instance, rate);

    /* Init of LCK transmitter frame: */
    pdu[1] = MESA_OAM_OPCODE_LCK;
    pdu[2] = rate_to_pdu_calc(data->lck_config.rate);
    pdu[3] = 0;
    pdu[4] = 0;

    pdu_tx.len          = sizeof(pdu);
    pdu_tx.data         = pdu;
    pdu_tx.dei          = FALSE;
    pdu_tx.line_rate    = FALSE;
    pdu_tx.rate         = rate;
    pdu_tx.rate_is_kbps = FALSE;
    pdu_tx.client_inj   = TRUE;

    for (u32 i = 0; i < data->client_config.flow_count; i++) {
        pdu[0]              = data->client_config.level[i] << 5;
        pdu_tx.mac          = class1_multicast[data->client_config.level[i]];
        pdu_tx.vid          = 0;
        pdu_tx.client_flow.domain  = model_domain_calc(data->client_config.domain[i], 0);
        pdu_tx.client_flow.flow    = data->client_config.flows[i];
        pdu_tx.prio         = data->client_config.lck_prio[i] == VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST ? highest_client_prio_calc(&pdu_tx.client_flow, data->config.port) : data->client_config.lck_prio[i];

        pdu_tx.reverse_inj  = TRUE;
        if (mep_model_tx(data->instance, &pdu_tx, &tx_id) != VTSS_RC_OK) {
            T_DG(TRACE_GRP_TX_FRAME, "mep_model_tx failed");
        } else if (rate) {
            (*data->lck_tx_id).push_back(tx_id);
        }
        tx_id = MEP_MODEL_PDU_TX_ID_NONE;

        pdu_tx.reverse_inj  = FALSE;
        if (mep_model_tx(data->instance, &pdu_tx, &tx_id) != VTSS_RC_OK) {
            T_DG(TRACE_GRP_TX_FRAME, "mep_model_tx failed");
        } else if (rate) {
            (*data->lck_tx_id).push_back(tx_id);
        }
        tx_id = MEP_MODEL_PDU_TX_ID_NONE;
    }

    T_DG(TRACE_GRP_AIS_LCK, "Exit");
}

static void run_lck_config(u32 instance)
{
    mep_instance_data_t *data;

    T_DG(TRACE_GRP_AIS_LCK, "Enter  Instance %u", instance);

    data = get_instance(instance);
    if (!data) { return; }

    data = get_instance(instance);
    if (!data) { return; }

    if (!(*data->lck_tx_id).empty()) {
        /* Stop all LCK Tx: */
        for (auto &id : *data->lck_tx_id) {
            mep_model_tx_cancel(instance, &id, NULL);
        }
        (*data->lck_tx_id).clear();
    }
    if (data->lck_config.enable) {
        /* Start / restart LCK: */
        tx_lck(data, frame_rate_calc(data->lck_config.rate));
    }

    if (!mep_model_mep_block_set(data->instance, (data->out_state.state.mep.aBlk || data->lck_config.enable))) {   /* Control service frame block */
        T_D("mep_model_mep_block_set failed");
    }

    T_DG(TRACE_GRP_AIS_LCK, "Exit");
}

static BOOL tst_tx_enable_calc(mep_instance_data_t *data)
{
    u32 i, j;

    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {      /* Calculate tx_enable */
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            if (data->tst_config.tests[i][j].enable) {
                return (TRUE);
            }
        }
    }
    return (FALSE);
}

static u32 run_tst_config(u32 instance, BOOL force, u64 *active_time_ms)
{
    u32                            i, j, k, pattern_idx, pattern_size, payload_size, pdu_size, rc_not_ok, rc = VTSS_RC_OK, counters_cleared = 0;
    mep_instance_data_t            *data;
    mep_model_tst_conf_t           tst_conf, old_tst_conf;
    mep_model_pdu_tx_t             pdu_tx = {};
    u8                             *pdu, pattern, prio;
    BOOL                           enable=FALSE;
    vtss_appl_mep_test_prio_conf_t *test;

    data = get_instance(instance);
    if (!data) { return VTSS_APPL_MEP_RC_INVALID_INSTANCE; }

    T_DG(TRACE_GRP_TST, "Enter  Instance %u  enable_rx %u  Force %u", instance, data->tst_config.common.enable_rx, force);

    /* Cancel any Tx disabled TST: */
    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            if (data->tst_config.tests[i][j].enable) { continue; }
            /* TX is no longer enabled */
            if (data->tst_tx_enabled[i][j]) {
                data->tst_tx_enabled[i][j] = FALSE;
                mep_model_tx_cancel(instance, &data->tst_tx_id[i][j], active_time_ms);
                T_DG(TRACE_GRP_TST, "TX stop prio %u  test %u", i, j);
            }
        }
    }

    /* 'pdu' must be allocted dynamically as the size can be over 10KB */
    pdu = new u8[VTSS_APPL_MEP_TST_SIZE_MAX];
    if (pdu == NULL) {
        T_EG(TRACE_GRP_TST, "Could not allocate memory");
        return VTSS_APPL_MEP_RC_OUT_OF_TX_RESOURCE;
    }

    enable = (data->tst_config.common.enable_rx || tst_tx_enable_calc(data));  /* enable is common for tx or rx*/

    if (!mep_model_tst_conf_get(instance, &old_tst_conf)) {
        T_DG(TRACE_GRP_TST, "mep_model_tst_conf_get(%u, %u) failed", instance, 0);
    }
    tst_conf.enable = enable;   /* TST must be enabled in model layer before any transmission is started */
    tst_conf.copy_to_cpu = FALSE;
    tst_conf.transaction_id = 0;
    if (!mep_model_tst_conf_set(instance, &tst_conf)) {
        T_DG(TRACE_GRP_TST, "mep_model_tst_conf_set(%u, %u) failed", instance, 0);
    }

    /* Start any Tx enabled TST: */
    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            if (!data->tst_config.tests[i][j].enable || (!force && data->tst_tx_enabled[i][j])) continue;
            /* Init of TST transmitted PDU: */
            test = &data->tst_config.tests[i][j];

            // BZ #20989: Do not clear counters if RX is enabled for this priority
            bool should_not_clear_stats = data->tst_rx_enabled && i == data->tst_config.prio;
            if (!should_not_clear_stats) {
                if (!mep_model_tst_status_clear(instance, i)) {
                    T_DG(TRACE_GRP_TST, "mep_model_tst_status_clear(%u, %u) failed", instance, i);
                }
                else
                    counters_cleared |= 1 << i; /* Indicate that for this prio, counters have been cleared */
            }

            payload_size = test->size - (12+2+4); /* Payload size is configured frame size subtracted untagged overhead and CRC */
            pdu_size = TST_PDU_LENGTH + 3;
            if (payload_size > pdu_size)   /* Check if pattern must be added to TLV - requested payload is bigger than TST PDU with empty TLV */
                pattern_size = payload_size - pdu_size;
            else
                pattern_size = 0;
            pdu[0] = data->config.level << 5;
            pdu[1] = MESA_OAM_OPCODE_TST;
            pdu[2] = 0;
            pdu[3] = 4;
            pdu[4] = 0;
            pdu[5] = 0;
            pdu[6] = 0;
            pdu[7] = 0;
            pdu[8] = 3;  /* Data TLV */
            pdu[9] = (pattern_size & 0xFF00)>>8;
            pdu[10] = (pattern_size & 0xFF);
            pattern_idx = 11;
            switch (test->pattern) {
            case VTSS_APPL_MEP_PATTERN_ALL_ZERO: pattern = 0; break;
            case VTSS_APPL_MEP_PATTERN_ALL_ONE:  pattern = 0xFF; break;
            case VTSS_APPL_MEP_PATTERN_0XAA:     pattern = 0xAA; break;
            default:                             pattern = 0; break;
            }
            for (k=0; k<pattern_size; ++k)     pdu[pattern_idx+k] = pattern;
            pdu[pattern_idx+k] = 0;
            pdu_tx.len  = pdu_size + pattern_size;
            pdu_tx.data = pdu;
            pdu_tx.vid  = (data->sat) ? data->sat_prio_conf[i].vid : 0;
            pdu_tx.prio = i;
            pdu_tx.dei  = (test->dp != VTSS_APPL_MEP_DP_GREEN);
            for (k=0; k<data->config.peer_count; ++k)
                if (data->tst_config.common.mep == data->config.peer_mep[k]) break;
            if (k == data->config.peer_count) {
                T_D("peer_mep %u not found instance=%u prio=%u", data->tst_config.common.mep, instance, i);
                continue;
            }
            pdu_tx.mac          = data->config.peer_mac[k].addr;
            pdu_tx.rate         = test->rate;
            pdu_tx.rate_is_kbps = TRUE;
            pdu_tx.line_rate    = (data->config.domain == VTSS_APPL_MEP_PORT);

            /* This call to mep_model_tx may fail, typically by "afi_multi_alloc() failed No more free multi-frame flow entries" */
            /* so it is important that we can back out with appropriate error code again.*/
            T_DG(TRACE_GRP_TST, "TX start prio %u  test %u", i, j);
            if ((rc_not_ok = mep_model_tx(data->instance, &pdu_tx, &data->tst_tx_id[i][j])) != VTSS_RC_OK) {
                T_DG(TRACE_GRP_TST, "mep_model_tx failed");
                data->tst_tx_enabled[i][j] = FALSE;
                data->tst_state.enable[i] = FALSE;
                rc = rc_not_ok;
            } else {
                data->tst_tx_enabled[i][j] = TRUE;
            }
        }
    }

    /* Free the memory */
    delete[] pdu;

    prio = data->tst_config.prio;

    if (prio < VTSS_APPL_MEP_PRIO_MAX) { /* The test time and RX rate is only supported when the user interface one priority 'prio' is valid */
        if (enable && !old_tst_conf.enable) { /*If the combnined enable goes from false to true then reset time value*/
            data->out_state.tst_state[prio].time = 0;
            mep_timer_start(1000, &data->tst_state.timer, data, run_tst_timer);
        }

        /* Rx is going from disabled to enabled, we reset rx counters*/
        if (data->tst_config.common.enable_rx && !data->tst_rx_enabled) {
            data->tst_rx_enabled = TRUE;
            data->tst_state.rx_frame_size = 0;
            data->out_state.tst_state[prio].rx_rate = 0;
            if (!(counters_cleared & (1 << prio))) { /* Only clear tst counters if not already cleared */
                if (!mep_model_tst_status_clear(instance, prio)) {
                    T_DG(TRACE_GRP_TST, "mep_model_tst_status_clear(%u, %u) failed", instance, prio);
                }
            }
        }
    }

    if (!enable) {
        mep_timer_cancel(data->tst_state.timer);
        run_tst_timer(instance, NULL); /* At this point the timer is stopped. Force timeout to get the latest TST stats */
    }

    T_DG(TRACE_GRP_TST, "Exit");

    return rc;
}



























































































































































































































































































































































































































































































































static void run_ssf_ssd_state(u32 instance)
{
    mep_instance_data_t *data;

    T_D("Enter");

    data = get_instance(instance);
    if (!data) { return; }

    fault_cause_calc(data);

    consequent_action_calc_and_deliver(data);       /* Calculate aTsf (SF) and aTsd (SD) and deliver to EPS */
}

static void delay_data_calc(u32 instance,  delay_state_t *delay_state,  vtss_appl_mep_dm_state_t *out_state,  u32 delay)
{
    mep_instance_data_t            *data;
    vtss_appl_mep_dm_common_conf_t *dm_config;
    u32                            out_delay, bucket;
    u32                            delay_var, out_delay_var, newcount;
    u32                            n_count, ov_cnt;

    data = get_instance(instance);
    if (!data) { return; }
    dm_config = &data->dm_config.common;

    if ((dm_config->overflow_act == VTSS_APPL_MEP_DISABLE) && (out_state->ovrflw_cnt != 0)) {   /* The overflow action is not to clear everything, keep the counter */
        return;
    }

    if (delay_state->sum > (delay_state->sum + delay)) {   /* This is indicating overflow of the total delay sum of this measuring period */
        if (dm_config->overflow_act == VTSS_APPL_MEP_DISABLE) {   /* The overflow action is not to clear everything, keep the counter */
            out_state->ovrflw_cnt = 1;
            return;
        }

        /* overflow, reset anything */
        ov_cnt = out_state->ovrflw_cnt + 1;

        memset(delay_state, 0, sizeof(*delay_state));
        memset(out_state, 0, sizeof(*out_state));
        out_state->min_delay = 0xffffffff;
        out_state->min_delay_var = 0xffffffff;
        out_state->ovrflw_cnt = ov_cnt;
    }

    if (delay_state->delay_cnt == 0) {
        delay_var = 0; /* the first one */
    } else {
        delay_var = ((delay >= delay_state->last_delay)) ? (delay - delay_state->last_delay) : (delay_state->last_delay - delay);
    }

    push_delay_data(delay_state, dm_config->lastn, delay, delay_var, &out_delay, &out_delay_var);

    delay_state->delay_cnt += 1;
    newcount = delay_state->delay_cnt;

    delay_state->sum = delay_state->sum + delay;
    delay_state->var_sum = delay_state->var_sum + delay_var;
    out_state->avg_delay = delay_state->sum / newcount;        /* Calculate the total average delay */
    if (newcount > 1)
        out_state->avg_delay_var = delay_state->var_sum / (newcount-1);     /* Calculate the total average delay veriation - the first received delay does not add to dm1_var_sum_near_to_far */

    delay_state->n_sum = delay_state->n_sum + delay - out_delay;
    delay_state->n_var_sum = delay_state->n_var_sum + delay_var - out_delay_var;
    n_count = (delay_state->delay_cnt >= dm_config->lastn) ? dm_config->lastn : newcount;
    if (n_count)  out_state->avg_n_delay = delay_state->n_sum / n_count;            /* Calculate the Last N average delay */
    if (n_count)  out_state->avg_n_delay_var = delay_state->n_var_sum / n_count;    /* Calculate the Last N average delay variation */

    if (delay < out_state->min_delay)
        out_state->min_delay = delay;

    if (delay > out_state->max_delay)
        out_state->max_delay = delay;

    /* Processing delay measurement bins for FD */
    if (dm_config->m_threshold > 0) {
        bucket = (delay / dm_config->m_threshold);
        if (bucket > dm_config->num_of_bin_fd) {
            bucket = dm_config->num_of_bin_fd - 1;
            out_state->fd_bin[bucket]++;
        } else {
            out_state->fd_bin[bucket]++;
        }
    } else {
        T_DG(TRACE_GRP_DM, "m threshold isn't correct  instance %u  m_threshold %u", instance, dm_config->m_threshold);
    }

    if (newcount >= 2) { /* Start min and max register after three measurements - at least two frames must be received in order to measure min/max delay variation */
        if (delay_var < out_state->min_delay_var)
            out_state->min_delay_var = delay_var;

        if (delay_var > out_state->max_delay_var)
            out_state->max_delay_var = delay_var;
    }

    /* Processing delay measurement bins for IFDV */
    if (dm_config->m_threshold > 0) {
        bucket = (delay_var / dm_config->m_threshold);
        if (bucket > dm_config->num_of_bin_ifdv) {
            bucket = dm_config->num_of_bin_ifdv - 1;
            out_state->ifdv_bin[bucket]++;
        } else {
            out_state->ifdv_bin[bucket]++;
        }
    } else {
        T_DG(TRACE_GRP_DM, "m threshold isn't correct  instance %u  m_threshold %u", instance, dm_config->m_threshold);
    }

    delay_state->last_delay = delay;
}

/* put new LM counter measurement into FIFO and take out the oldest entry: */
static void put_lm_avail_fifo(mep_instance_data_t  *data,
                              const lm_counters_t *in_counters,  /* new measurement */
                              lm_counters_t *out_counters)       /* oldest measurement in fifo */
{
    if (!data->lm_avail_fifo) {
        /* no fifo, treat as fifo with 1 element: */
        *out_counters = *in_counters;
    } else {
        data->lm_avail_fifo[data->lm_avail_fifo_ix] = *in_counters;
        if ((data->lm_avail_fifo_ix + 1) < data->lm_avail_config.interval) {
            data->lm_avail_fifo_ix++;
        } else {
            data->lm_avail_fifo_ix = 0;
        }
        *out_counters = data->lm_avail_fifo[data->lm_avail_fifo_ix];
    }
}

static void vtss_mep_mgmt_lm_notif_set(const u32 instance, const u32 peer_idx)
{
    vtss_appl_mep_lm_notif_state_t data;
    out_state_t                    *out_state;
    mep_instance_data_t            *p;

    p = get_instance(instance);
    if (!p) { return; }
    out_state = &p->out_state;

    vtss_mep_lm_notif_status_get(instance, &data);

    data.peer_index       = peer_idx;
    data.near_state       = out_state->lm_avail_state[peer_idx].near_state;
    data.far_state        = out_state->lm_avail_state[peer_idx].far_state;
    if (!mep_timer_active(p->los_int_cnt_timer)) {
        data.near_los_int_cnt = out_state->lm_state[peer_idx].near_los_int_cnt;
        data.far_los_int_cnt  = out_state->lm_state[peer_idx].far_los_int_cnt;
    }
    if (!mep_timer_active(p->los_th_cnt_timer)) {
        data.near_los_th_cnt  = out_state->lm_state[peer_idx].near_los_th_cnt;
        data.far_los_th_cnt   = out_state->lm_state[peer_idx].far_los_th_cnt;
    }

    vtss_mep_lm_notif_status_set(instance, &data);
}

/*
 * Update the state structure with current Loss Measurement counters if LM TX
 * and/or RX is enabled.
 */
static void get_lm_pdu_counters(mep_instance_data_t          *data,
                                vtss_appl_mep_lm_state_t     *const state)
{
    mep_model_lm_status_t  lm_status;
    memset(&lm_status, 0, sizeof(lm_status));

    if (!mep_model_lm_status_get(data->instance, &lm_status)) {
        T_D("mep_model_lm_status_get failed");
        return;
    }

    if (data->lm_config.enable) {
        state->tx_counter = lm_status.tx_lm;
        state->rx_counter = lm_status.rx_lm;
    } else if (data->lm_config.enable_rx) {
        state->tx_counter = 0;
        state->rx_counter = data->lm_config.ended == VTSS_APPL_MEP_SINGEL_ENDED ? lm_status.rx_lmm : lm_status.rx_lm;
    } else {
        // nothing is enabled - do not update counters
    }
}

/*
 * Update the state structure with current Synthetic Loss counters if SL TX
 * and/or RX is enabled.
 */
static void get_sl_pdu_counters(mep_instance_data_t          *data,
                                const u32                    peer_index,
                                vtss_appl_mep_lm_state_t     *const state)
{
    mep_model_sl_status_t  sl_status;
    memset(&sl_status, 0, sizeof(sl_status));

    if (!mep_model_sl_status_get(data->instance, &sl_status)) {
        T_D("mep_model_sl_status_get failed");
        return;
    }

    if (data->lm_config.enable) {
        /*
         * This is an initiator LM instance. Use the single SLM/1SL TX counter.
         * If instance is set to unicast check that this peer is the active one.
         */
        if ((data->lm_config.cast == VTSS_APPL_MEP_MULTICAST) ||
            (data->config.peer_mep[peer_index] == data->lm_config.mep)) {
            state->tx_counter = sl_status.tx_slm;
        }

        /*
         * Now select which counter to use as the RX counter:
         * - If this is a single-ended initiator we expect to receive SLR responses.
         * - If this is a dual-ended initiator then we do not expect to receive anything
         *   except if RX is also enabled.
         */
        state->rx_counter = data->lm_config.ended == VTSS_APPL_MEP_SINGEL_ENDED || data->lm_config.enable_rx ?
            sl_status.rx_slr[peer_index] : 0;

    } else if (data->lm_config.enable_rx) {
        /*
         * Responder-only: TX is always 0, in order not to confuse the user.
         * The UI paradigm is that "TX" is only relevant for initiators. A responder
         * will by definition only "receive" and it is understood that it will
         * of course reply to every valid PDU received.
         */
        state->tx_counter = 0;
        /*
         * Responder-only: We do not know if we are in a single-ended or a dual-ended
         * session (only the initiator knows that). So we report the sum of TX SLR and RX SLR:
         * - TX SLR is for single-ended (SLM), as there must have been one received PDU for every PDU transmitted.
         * - RX SLR is for dual-ended (1SL)
         */
        state->rx_counter = sl_status.tx_slr[peer_index] + sl_status.rx_slr[peer_index];

    } else {
        // nothing is enabled - do not update counters
    }
}

static void lm_measurement(mep_instance_data_t *data,  u32 peer_idx,  lm_counters_t *lm_counters)
{
    i32                         nearLos, farLos;
    u16                         nearFLR = 0, farFLR = 0;
    lm_counters_t               out_counters;
    BOOL                        ssf_ssd = FALSE;
    lm_peer_state_t             *lm_state;
    out_state_t                 *out_state;
    vtss_appl_mep_lm_state_t    *out_lm_state;
    vtss_appl_mep_lm_avail_state_t *out_lm_avail_state;

    T_DG(TRACE_GRP_LM, "instance %u.%u  near_TX %u  far_RX %u  far_TX %u  near_rx %u", data->instance, peer_idx,
         lm_counters->nearTxC, lm_counters->farRxC, lm_counters->farTxC, lm_counters->nearRxC);

    lm_state = &data->lm_peer_state[peer_idx];
    out_state = &data->out_state;
    out_lm_state = &out_state->lm_state[peer_idx];
    out_lm_avail_state = &out_state->lm_avail_state[peer_idx];

    if (lm_state->flr_interval_count == 0) {
        /* Frame Loss Ratio interval restart: */
        lm_state->near_tx_counter                = 0;
        lm_state->far_tx_counter                 = 0;
        lm_state->lm_nearLos                     = 0;
        lm_state->lm_farLos                      = 0;
    }

    /* put new counter measurement in fifo and take out the oldest entry: */
    put_lm_avail_fifo(data, lm_counters, &out_counters);

    /* Availability (using new lm_counters measurement): */
    if (data->lm_avail_config.enable && !data->lm_avail_config.main) {
        /* calculate loss for this measurement: */
        nearLos = (lm_counters->farTxC  - lm_counters->nearRxC);
        farLos  = (lm_counters->nearTxC - lm_counters->farRxC);

        /* calculate FLR for this measurement: */
        if (nearLos > 0 && lm_counters->farTxC > 0)
            nearFLR = (((u32)nearLos) * 1000) / lm_counters->farTxC;
        if (farLos > 0 && lm_counters->nearTxC > 0)
            farFLR = (((u32)farLos) * 1000)/ lm_counters->nearTxC;

      //note: the variable lm_near_avail_cnt is used to count both up from LM_AVAIL and LM_UNAVAIL, but with the meaning unavail_cnt contra avail_cnt.
      //note: and similar fo lm_far_avail_cnt is used to count both up from LM_AVAIL and LM_UNAVAIL, but with the meaning unavail_cnt contra avail_cnt.
      if (out_lm_avail_state->near_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
        if (nearFLR >= data->lm_avail_config.flr_th && nearFLR != 0) {
            out_lm_avail_state->near_avail_cnt++;
            if (out_lm_avail_state->near_avail_cnt >= data->lm_avail_config.interval) {   // n consequtive unavail, so change state
                out_lm_avail_state->near_state = VTSS_APPL_MEP_OAM_LM_UNAVAIL;
                out_lm_avail_state->near_avail_cnt= 0;
            }
         } else { // just one ok keeps state and reset cnt
            out_lm_avail_state->near_avail_cnt= 0;
         }
      } else {
        if (nearFLR < data->lm_avail_config.flr_th || nearFLR == 0) {
            out_lm_avail_state->near_avail_cnt++;
            if (out_lm_avail_state->near_avail_cnt >= data->lm_avail_config.interval) { // n consequtive avail, so change state
                out_lm_avail_state->near_state = VTSS_APPL_MEP_OAM_LM_AVAIL;
                out_lm_avail_state->near_avail_cnt= 0;
            }
         } else { // just one ok keeps state and reset cnt
            out_lm_avail_state->near_avail_cnt= 0;
         }
      }
      if (out_lm_avail_state->far_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
        if (farFLR >= data->lm_avail_config.flr_th && farFLR != 0) {
            out_lm_avail_state->far_avail_cnt++;
            if (out_lm_avail_state->far_avail_cnt >= data->lm_avail_config.interval) {   // n consequtive unavail, so change state
                out_lm_avail_state->far_state = VTSS_APPL_MEP_OAM_LM_UNAVAIL;
                out_lm_avail_state->far_avail_cnt= 0;
            }
         } else { // just one ok keeps state and reset cnt
            out_lm_avail_state->far_avail_cnt= 0;
         }
      } else {
        if (farFLR < data->lm_avail_config.flr_th || farFLR == 0) {
            out_lm_avail_state->far_avail_cnt++;
            if (out_lm_avail_state->far_avail_cnt >= data->lm_avail_config.interval) { // n consequtive avail, so change state
                out_lm_avail_state->far_state = VTSS_APPL_MEP_OAM_LM_AVAIL;
                out_lm_avail_state->far_avail_cnt= 0;
            }
         } else { // just one ok keeps state and reset cnt
            out_lm_avail_state->far_avail_cnt= 0;
         }
      }

      // calculate more info to the management interfaces
      if (!data->lm_avail_config.main) {
            if (out_lm_avail_state->near_state == VTSS_APPL_MEP_OAM_LM_AVAIL)
                out_lm_avail_state->near_cnt++;
            else
                out_lm_avail_state->near_un_cnt++;

            if (out_lm_avail_state->far_state == VTSS_APPL_MEP_OAM_LM_AVAIL)
                out_lm_avail_state->far_cnt++;
            else
                out_lm_avail_state->far_un_cnt++;
      }
    }
    /* the following are using out_counters from the fifo: */
    lm_counters = &out_counters;

    /* Loss: */
    /* calculate loss for this measurement: */
    nearLos = lm_counters->farTxC  - lm_counters->nearRxC;
    farLos  = lm_counters->nearTxC - lm_counters->farRxC;

    if (out_lm_avail_state->near_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
        /* near end loss: */
        if (nearLos > 0) {
            out_lm_state->near_los_int_cnt++;
        }
        out_lm_state->near_los_tot_cnt += nearLos;
        lm_state->lm_nearLos += nearLos;

        if (nearLos > 0 && nearLos > data->lm_config.loss_th) {
            out_lm_state->near_los_th_cnt++;
        }
    }
    if (out_lm_avail_state->far_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
        /* far end loss: */
        if (farLos > 0) {
            out_lm_state->far_los_int_cnt++;
        }
        out_lm_state->far_los_tot_cnt += farLos;
        lm_state->lm_farLos += farLos;

        if (farLos > 0 && farLos > data->lm_config.loss_th) {
            out_lm_state->far_los_th_cnt++;
        }
    }

    /* FLR: */
    lm_state->flr_interval_count++;

    if (out_lm_avail_state->near_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
        /* Accumulate for near end ratio calculation */
        lm_state->far_tx_counter       += lm_counters->farTxC;
        lm_state->far_tot_tx_counter   += lm_counters->farTxC;

        if (lm_state->flr_interval_count >= data->lm_config.flr_interval) {
            /* Frame Loss Ratio interval has expired - calculate ratio */
            if ((lm_state->lm_nearLos > 0) && (lm_state->far_tx_counter > 0)) {
                out_lm_state->near_flr_ave_int = (((u32)(lm_state->lm_nearLos)*MEP_FLR_SCALE_FACTOR)/lm_state->far_tx_counter);

                if (out_lm_state->near_flr_ave_int > 0 && out_lm_state->near_flr_min == 0 && out_lm_state->near_flr_max == 0) {
                    /* first non-zero FLR */
                    out_lm_state->near_flr_min = out_lm_state->near_flr_ave_int;
                    out_lm_state->near_flr_max = out_lm_state->near_flr_ave_int;
                } else {
                    if (out_lm_state->near_flr_ave_int < out_lm_state->near_flr_min)
                        out_lm_state->near_flr_min = out_lm_state->near_flr_ave_int;
                    if (out_lm_state->near_flr_ave_int > out_lm_state->near_flr_max)
                        out_lm_state->near_flr_max = out_lm_state->near_flr_ave_int;
                }

            } else {
                out_lm_state->near_flr_ave_int = 0;
            }
        }
        if ((out_lm_state->near_los_tot_cnt > 0) && (lm_state->far_tot_tx_counter > 0)) {
            out_lm_state->near_flr_ave_total = (((u32)out_lm_state->near_los_tot_cnt) * MEP_FLR_SCALE_FACTOR) / lm_state->far_tot_tx_counter;
        }
        else {
            out_lm_state->near_flr_ave_total = 0;
        }
    }

    if (out_lm_avail_state->far_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
        /* Accumulate for far end ratio calculation */
        lm_state->near_tx_counter      += lm_counters->nearTxC;
        lm_state->near_tot_tx_counter  += lm_counters->nearTxC;

        if (lm_state->flr_interval_count >= data->lm_config.flr_interval) {
            /* Frame Loss Ratio interval has expired - calculate ratio */
            if ((lm_state->lm_farLos > 0) && (lm_state->near_tx_counter > 0)) {
                out_lm_state->far_flr_ave_int = (((u32)(lm_state->lm_farLos)*MEP_FLR_SCALE_FACTOR)/lm_state->near_tx_counter);

                if (out_lm_state->far_flr_ave_int > 0 && out_lm_state->far_flr_min == 0 && out_lm_state->far_flr_max == 0) {
                    /* first non-zero FLR */
                    out_lm_state->far_flr_min = out_lm_state->far_flr_ave_int;
                    out_lm_state->far_flr_max = out_lm_state->far_flr_ave_int;
                } else {
                    if (out_lm_state->far_flr_ave_int < out_lm_state->far_flr_min)
                        out_lm_state->far_flr_min = out_lm_state->far_flr_ave_int;
                    if (out_lm_state->far_flr_ave_int > out_lm_state->far_flr_max)
                        out_lm_state->far_flr_max = out_lm_state->far_flr_ave_int;
                }

            } else {
                out_lm_state->far_flr_ave_int = 0;
            }
        }
        if ((out_lm_state->far_los_tot_cnt > 0) && (lm_state->near_tot_tx_counter > 0)) {
            out_lm_state->far_flr_ave_total = (((u32)out_lm_state->far_los_tot_cnt) * MEP_FLR_SCALE_FACTOR) / lm_state->near_tot_tx_counter;
        } else
            out_lm_state->far_flr_ave_total = 0;
    }

    /* HLI: */
    if (data->lm_hli_config.enable) {
        if (out_lm_avail_state->near_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
            if (nearFLR > data->lm_hli_config.flr_th) {
                out_state->lm_hli_state[peer_idx].near_cnt++;
                data->lm_hli_state[peer_idx].near_cnt++;
                if (data->lm_hli_state[peer_idx].near_cnt == data->lm_hli_config.con_int) {
                    out_state->lm_hli_state[peer_idx].near_con_cnt++;
                }
            } else
                data->lm_hli_state[peer_idx].near_cnt = 0;
        }
        if (out_lm_avail_state->far_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
            if (farFLR > data->lm_hli_config.flr_th) {
                out_state->lm_hli_state[peer_idx].far_cnt++;
                data->lm_hli_state[peer_idx].far_cnt++;
                if (data->lm_hli_state[peer_idx].far_cnt == data->lm_hli_config.con_int) {
                    out_state->lm_hli_state[peer_idx].far_con_cnt++;
                }
            } else
                data->lm_hli_state[peer_idx].far_cnt = 0;
        }
    }

    /* DEG: */
    if (data->lm_sdeg_config.enable) {
        if (out_lm_avail_state->near_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
            if (lm_counters->farTxC > data->lm_sdeg_config.tx_min && nearLos >= 0) {
                if (nearFLR > data->lm_sdeg_config.flr_th) {
                    data->lm_sdeg_bad = TRUE;
                } else {
                    data->lm_sdeg_bad = FALSE;
                }
            }
            if (data->lm_sdeg_bad) {
                data->lm_sdeg_good_cnt = 0;
                if (data->lm_sdeg_bad_cnt < data->lm_sdeg_config.bad_th) { /* wrap guard */
                    data->lm_sdeg_bad_cnt++;
                }
            } else {
                data->lm_sdeg_bad_cnt = 0;
                if (data->lm_sdeg_good_cnt < data->lm_sdeg_config.good_th) { /* wrap guard */
                    data->lm_sdeg_good_cnt++;
                }
            }
            if (!data->dDeg && data->lm_sdeg_bad_cnt >= data->lm_sdeg_config.bad_th) {
                data->dDeg = TRUE;
                ssf_ssd = TRUE;
            } else if (data->dDeg && data->lm_sdeg_good_cnt >= data->lm_sdeg_config.good_th) {
                data->dDeg = FALSE;
                ssf_ssd = TRUE;
            }
        }
    }

    vtss_mep_mgmt_lm_notif_set(data->instance, peer_idx);
    if (data->lm_hli_config.enable && !mep_timer_active(data->hli_cnt_timer)) {
        vtss_mep_lm_hli_status_set(data->instance, data->config.peer_mep[peer_idx], &out_state->lm_hli_state[peer_idx]);
    }

    if (lm_state->flr_interval_count >= data->lm_config.flr_interval) {
        /* restart Frame Loss Ratio interval */
        lm_state->flr_interval_count = 0;
        out_lm_state->flr_interval_elapsed++;
    }

    if (data->lm_config.synthetic) {
        get_sl_pdu_counters(data, peer_idx, out_lm_state);
    } else {
        get_lm_pdu_counters(data, out_lm_state);
    }

    if (ssf_ssd) {
        run_ssf_ssd_state(data->instance);
    }
}

static void run_slm_timer(u32 instance, struct vtss::Timer *timer)
{
    mep_instance_data_t *data;

    T_D("Enter  Instance %u", instance);

    data = get_instance(instance);
    if (!data) { return; }

    run_slm_meas_interval_expired(data);

    if (data->lm_config.synthetic && (data->lm_config.enable || data->lm_config.enable_rx)) {
        mep_timer_start(data->lm_config.meas_interval, &data->lm_state.timer, data, run_slm_timer);
    }
}

static void run_lt_timer(u32 instance, struct vtss::Timer *timer)
{
    mep_instance_data_t *data;

    T_D("Enter  Instance %u", instance);

    data = get_instance(instance);
    if (!data) { return; }

    if (data->lt_config.enable) {
        data->out_state.lt_state.transaction_cnt++;
        if (data->out_state.lt_state.transaction_cnt < data->lt_config.transactions) {
            data->lt_state.transaction_id++;
            data->out_state.lt_state.transaction[data->out_state.lt_state.transaction_cnt].transaction_id = data->lt_state.transaction_id;
            tx_ltm(data);
            mep_timer_start(5000, &data->lt_state.timer, data, run_lt_timer);
        } else {
            /* No more transactions - disable LTM: */
            data->lt_config.enable = FALSE;
            run_lt_config(data);
        }
    }
}


static void run_lb_timer(u32 instance, struct vtss::Timer *timer)
{
    mep_instance_data_t *data;
    u32 i, j;

    T_NG(TRACE_GRP_LB, "Enter  Instance %u", instance);

    data = get_instance(instance);
    if (!data) { return; }

    if (test_enable_calc(data->lb_config.tests)) {
        if (data->lb_state.lbm_tx_count < data->lb_config.common.to_send) {
            tx_lbm(data, FALSE, TRUE);
            data->lb_state.lbm_tx_count++;
            if (data->lb_state.lbm_tx_count == data->lb_config.common.to_send) {
                /* last LBM sent, now wait 5 sec for LBR's: */
                mep_timer_start(5000, &data->lb_state.timer, data, run_lb_timer);
            } else {
                mep_timer_start(10 * data->lb_config.common.interval, &data->lb_state.timer, data, run_lb_timer);
            }
        } else { /* All LBM is now transmitted, disable LB tests: */
            for (i = 0; i < VTSS_APPL_MEP_PRIO_MAX; ++i) {
                for (j = 0; j < VTSS_APPL_MEP_TEST_MAX; ++j) {
                    data->lb_config.tests[i][j].enable = FALSE;
                    data->lb_enabled[i][j] = FALSE;
                }
            }
            delete_timer(&data->lb_state.timer);
        }
    }

    T_NG(TRACE_GRP_LB, "Exit");
}

static void run_lst_ign_timer(u32 instance, struct vtss::Timer *timer)
{
    mep_instance_data_t         *data;

    data = get_instance(instance);
    if (!data) { return; }

    if (data->lst_config.enable) {
        lst_action(data);
    }
}


static void run_lst_sf_timer(u32 instance, struct vtss::Timer *timer)
{
    mep_instance_data_t         *data;

    data = get_instance(instance);
    if (!data) { return; }

    if (data->lst_config.enable) {
        lst_action(data);
    }
}

static void run_tx_aps_timer(u32 instance, struct vtss::Timer *timer)
{
    mep_instance_data_t         *data;
    mep_model_pdu_tx_t          pdu_tx = {};
    u32                         timeout;
    u8                          tmp_store[2] = {0};

    data = get_instance(instance);
    if (!data) { return; }
    if (!data->aps_config.enable) { return; }

    pdu_tx.len  = APS_PDU_LENGTH;
    pdu_tx.data = data->tx_aps;;
    pdu_tx.vid  = 0;
    pdu_tx.prio = data->aps_config.prio;
    pdu_tx.dei  = FALSE;
    if (data->aps_config.type == VTSS_APPL_MEP_R_APS) {
        raps_mac[5] = data->aps_config.raps_octet;
        pdu_tx.mac  = raps_mac;
    } else {
        pdu_tx.mac  = class1_multicast[data->config.level];
    }
    pdu_tx.rate = 0;
    pdu_tx.rate_is_kbps = FALSE;

    if (data->aps_event) {
        tmp_store[0]   = data->tx_aps[4];/* Store current state */
        tmp_store[1]   = data->tx_aps[5];
        data->tx_aps[4] = 0xE0;           /* Replace Request/State with an ERPS event */
        data->tx_aps[5] = 0;
    }

    do  {
        T_DG(TRACE_GRP_APS_TX, "Transmit aps (%x %x %x %x %x %x) inst:%d Aps count:%d ",
             data->tx_aps[0],data->tx_aps[1],data->tx_aps[2],data->tx_aps[3],data->tx_aps[4],data->tx_aps[5],data->instance, data->aps_count);

        if (mep_model_tx(instance, &pdu_tx, &data->aps_tx_id) != VTSS_RC_OK) {
            T_DG(TRACE_GRP_TX_FRAME, "mep_model_tx failed");
        }

        if (data->aps_count > 0) {
            data->aps_count--;
        }
    } while (data->aps_count > 0);

    if (!data->aps_event) {
        timeout = MEP_APS_TIMEOUT; // 5000 ms
        mep_timer_start(timeout, &data->tx_aps_timer, data, &run_tx_aps_timer);
    } else {
        data->tx_aps[4] = tmp_store[0];
        data->tx_aps[5] = tmp_store[1];
        data->aps_event = 0;
    }
}

static void run_rx_aps_timer(u32 instance, struct vtss::Timer *timer)
{
    mep_instance_data_t   *data;
    u8                    null_aps[RAPS_DATA_LENGTH] = {0,0,0,0,0,0,0,0};

    data = get_instance(instance);
    if (!data) {
        return;
    }
    vtss_mep_rx_aps_info_set(instance, data->config.domain, null_aps);
}


static void run_tst_timer(u32 instance, struct vtss::Timer *timer)
{
    u32                    sec_count, rx_bits;
    u8                     prio;
    mep_instance_data_t    *data;
    mep_model_tst_status_t tst_status;

    data = get_instance(instance);
    if (!data) { return; }

    T_NG(TRACE_GRP_TST, "Enter  Instance %u", instance);

    for (prio = 0; prio < VTSS_APPL_MEP_PRIO_MAX; ++prio) {
        if (prio != data->tst_config.prio) { continue; }
        if (!mep_model_tst_status_get(instance, prio, &tst_status)) {
            T_DG(TRACE_GRP_TST, "mep_model_tst_status_get(%u, %u) failed", instance, prio);
        } else {
            if (timer) {
                sec_count = tst_status.rx_counter - data->out_state.tst_state[prio].rx_counter;   /* This is received in the last second */
                data->out_state.tst_state[prio].rx_counter = tst_status.rx_counter;               /* Save the RX counter to be used at next timeout */
                rx_bits = sec_count * data->tst_state.rx_frame_size * 8;                          /* Calculate the information rate for the last second */
                data->out_state.tst_state[prio].rx_rate = rx_bits/1000;                           /* RX rate in Kbps */
            } else {
                /* stopping - calculate over full test: */
                if (data->out_state.tst_state[prio].time == 0) {
                    data->out_state.tst_state[prio].time = 1;
                }
                data->out_state.tst_state[prio].rx_rate = (tst_status.rx_counter * data->tst_state.rx_frame_size * 8) / data->out_state.tst_state[prio].time / 1000;
            }

            /* TBD future data->out_state.tst_state[prio].crc_err_counter = tst_status.crc_err_counter; */
        }
        if (timer) {
            data->out_state.tst_state[prio].time++;    /* Increment the 'Test time' */
        }
    }
    if (timer) {
        mep_timer_start(1000, &data->tst_state.timer, data, run_tst_timer);
    }
    T_NG(TRACE_GRP_TST, "Exit");
}


static void run_los_int_cnt_timer(u32 instance, struct vtss::Timer *timer)
{
    mep_instance_data_t         *data;

    data = get_instance(instance);
    if (!data) { return; }

    for (int peer_index = 0; peer_index < data->config.peer_count; peer_index++) {
        vtss_mep_mgmt_lm_notif_set(instance, peer_index);
    }

    if ((1000 * data->lm_config.los_int_cnt_holddown) > 0) {
        mep_timer_start(1000 * data->lm_config.los_int_cnt_holddown, &data->los_int_cnt_timer, data, run_los_int_cnt_timer);
    }
}

static void run_los_th_cnt_timer(u32 instance, struct vtss::Timer *timer)
{
    mep_instance_data_t         *data;

    data = get_instance(instance);
    if (!data) { return; }

    for (int peer_index = 0; peer_index < data->config.peer_count; peer_index++) {
        vtss_mep_mgmt_lm_notif_set(instance, peer_index);
    }

    if ((1000 * data->lm_config.los_th_cnt_holddown) > 0) {
        mep_timer_start(1000 * data->lm_config.los_th_cnt_holddown, &data->los_th_cnt_timer, data, run_los_th_cnt_timer);
    }
}

static void run_hli_cnt_timer(u32 instance, struct vtss::Timer *timer)
{
    mep_instance_data_t         *data;

    data = get_instance(instance);
    if (!data) { return; }

    for (int peer_index = 0; peer_index < data->config.peer_count; peer_index++) {
        vtss_mep_lm_hli_status_set(instance, data->config.peer_mep[peer_index], &data->out_state.lm_hli_state[peer_index]);
    }

    if ((1000 * data->lm_hli_config.cnt_holddown) > 0) {
        mep_timer_start(1000 * data->lm_hli_config.cnt_holddown, &data->hli_cnt_timer, data, run_hli_cnt_timer);
    }
}


static void run_syslog_timer(u32 instance, struct vtss::Timer *timer)
{
    mep_instance_data_t         *data;
    u32                         idx, self_mep, timer_period = 0;
    BOOL                        find_first_entry = FALSE;

    data = get_instance(instance);
    if (!data) { return; }

    self_mep = data->instance + 1;

    /* syslog for CFM, VTSS_MEP_CROSSCHECK_MEP_MISSING */
    for (idx = 0; idx < mep_caps.mesa_oam_peer_cnt; ++idx) {
        if (data->syslog_state.transaction_id[idx] > 0 && data->syslog_state.peer_mep_timer[idx] == 0) {
            vtss_mep_syslog(VTSS_MEP_CROSSCHECK_MEP_MISSING, self_mep, data->syslog_state.transaction_id[idx]);
        }
        /* reset transaction id */
        data->syslog_state.transaction_id[idx] = 0;
    }

    /* find the next timeout timer */
    for (idx = 0; idx < mep_caps.mesa_oam_peer_cnt; ++idx) {
        if (data->syslog_state.transaction_id[idx] > 0 && data->syslog_state.peer_mep_timer[idx] > 0) {
            if (!find_first_entry) {
                timer_period = data->syslog_state.peer_mep_timer[idx];
                find_first_entry = TRUE;
                continue;
            }
            if (timer_period > data->syslog_state.peer_mep_timer[idx]) {
                timer_period = data->syslog_state.peer_mep_timer[idx];
            }
        }
    }
    mep_timer_start(timer_period, &data->syslog_state.timer, data, run_syslog_timer);
}

static void run_lm_clear(u32 instance, vtss_appl_mep_clear_dir direction)
{
    mep_instance_data_t     *data;

    data = get_instance(instance);
    if (!data) { return; }

    if (!mep_model_lm_status_clear(instance, data->lm_config.synthetic, mep_model_lm_clear_dir_get(direction))) {
        T_D("mep_model_lm_status_clear failed");
    }

    for (int peer_index = 0; peer_index < mep_caps.mesa_oam_peer_cnt; peer_index++) {
        data->lm_peer_state[peer_index].flr_interval_count  = 0;
        data->lm_peer_state[peer_index].near_tx_counter     = 0;
        data->lm_peer_state[peer_index].far_tx_counter      = 0;
        data->lm_peer_state[peer_index].near_tot_tx_counter = 0;
        data->lm_peer_state[peer_index].far_tot_tx_counter  = 0;
        data->lm_peer_state[peer_index].lm_nearLos          = 0;
        data->lm_peer_state[peer_index].lm_farLos           = 0;

        data->out_state.lm_state[peer_index].tx_counter         = 0;
        data->out_state.lm_state[peer_index].rx_counter         = 0;
        data->out_state.lm_state[peer_index].near_los_tot_cnt   = 0;
        data->out_state.lm_state[peer_index].far_los_tot_cnt    = 0;
        data->out_state.lm_state[peer_index].near_flr_ave_int   = 0;
        data->out_state.lm_state[peer_index].far_flr_ave_int    = 0;
        data->out_state.lm_state[peer_index].near_los_int_cnt   = 0;
        data->out_state.lm_state[peer_index].far_los_int_cnt    = 0;
        data->out_state.lm_state[peer_index].near_los_th_cnt    = 0;
        data->out_state.lm_state[peer_index].far_los_th_cnt     = 0;
        data->out_state.lm_state[peer_index].near_flr_ave_total = 0;
        data->out_state.lm_state[peer_index].far_flr_ave_total  = 0;
        data->out_state.lm_state[peer_index].near_flr_min       = 0;
        data->out_state.lm_state[peer_index].far_flr_min        = 0;
        data->out_state.lm_state[peer_index].near_flr_max       = 0;
        data->out_state.lm_state[peer_index].far_flr_max        = 0;
        data->out_state.lm_state[peer_index].flr_interval_elapsed = 0;

        data->out_state.lm_hli_state[peer_index].near_cnt       = 0;
        data->out_state.lm_hli_state[peer_index].far_cnt        = 0;
        data->out_state.lm_hli_state[peer_index].near_con_cnt   = 0;
        data->out_state.lm_hli_state[peer_index].far_con_cnt    = 0;
        data->lm_hli_state[peer_index].near_cnt                 = 0;
        data->lm_hli_state[peer_index].far_cnt                  = 0;
    }
}


static void run_dm_clear(u32 instance)
{
    mep_instance_data_t         *data;
    vtss_appl_mep_dm_tunit_t    tunit_dmfn, tunit_dmnf, tunit_dmtw;
    u32                         i;

    data = get_instance(instance);
    if (!data) { return; }

    memset(data->supp_state.tw_state, 0, sizeof(data->supp_state.tw_state));
    memset(data->supp_state.fn_state, 0, sizeof(data->supp_state.fn_state));
    memset(data->supp_state.nf_state, 0, sizeof(data->supp_state.nf_state));

    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        /* Time unit must be reserved */
        tunit_dmtw = data->out_state.tw_state[i].tunit;
        tunit_dmfn = data->out_state.fn_state[i].tunit;
        tunit_dmnf = data->out_state.nf_state[i].tunit;

        memset(&data->out_state.tw_state[i], 0, sizeof(data->out_state.tw_state[i]));
        data->out_state.tw_state[i].min_delay = 0xffffffff;
        data->out_state.tw_state[i].min_delay_var = 0xffffffff;

        memset(&data->out_state.fn_state[i], 0, sizeof(data->out_state.fn_state[i]));
        data->out_state.fn_state[i].min_delay = 0xffffffff;
        data->out_state.fn_state[i].min_delay_var = 0xffffffff;

        memset(&data->out_state.nf_state[i], 0, sizeof(data->out_state.nf_state[i]));
        data->out_state.nf_state[i].min_delay = 0xffffffff;
        data->out_state.nf_state[i].min_delay_var = 0xffffffff;

        /* Revert time unit */
        data->out_state.tw_state[i].tunit = tunit_dmtw;
        data->out_state.fn_state[i].tunit = tunit_dmfn;
        data->out_state.nf_state[i].tunit = tunit_dmnf;

        if (!mep_model_dm_status_clear(instance, i)) {
            T_D("mep_model_dm_status_clear(%u) failed", instance);
        }
    }
}


static void run_tst_clear(u32 instance)
{
    mep_instance_data_t  *data;
    u32                  i;

    data = get_instance(instance);
    if (!data) { return; }

    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        if (!mep_model_tst_status_clear(instance, i)) {
            T_D("mep_model_tst_status_clear(%u, %u) failed", instance, i);
        }
    }
    memset(&data->out_state.tst_state, 0, sizeof(data->out_state.tst_state));
    if (data->tst_state.timer) {
        mep_timer_start(1000, &data->tst_state.timer, data, run_tst_timer);
    }
}

static void run_lb_clear(u32 instance)
{
    mep_instance_data_t  *data;
    u32                  i;

    data = get_instance(instance);
    if (!data) { return; }

    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        if (!mep_model_lb_status_clear(instance, i)) {
            T_DG(TRACE_GRP_LB, "mep_model_lb_status_clear(%u) failed", instance);
        }
    }
}

static void run_lst_config(u32 instance)
{
    mep_instance_data_t  *data;

    data = get_instance(instance);
    if (!data) { return; }

    data->lst_state.fe_disable = 0;
    lst_action(data);
}

/****************************************************************************/
/*  MEP management interface                                                */
/****************************************************************************/

u32 vtss_mep_mgmt_tlv_conf_set(const vtss_appl_mep_tlv_conf_t    *const conf)
{
    vtss::Map<u32, mep_instance_data_t>::iterator i;

    T_N("Enter");

    if (!memcmp(&os_tlv_config, &conf->os_tlv, sizeof(os_tlv_config)))    return(VTSS_RC_OK);

    os_tlv_config = conf->os_tlv;

    for (i = instance_data.begin(); i != instance_data.end(); ++i) {
        if ((*i).second.config.enable && (*i).second.cc_config.enable)
            run_tlv_change((*i).first);
    }

    return(VTSS_RC_OK);
}

u32 vtss_mep_mgmt_tlv_conf_get(vtss_appl_mep_tlv_conf_t    *const conf)
{
    T_N("Enter");

    conf->os_tlv = os_tlv_config;

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_conf_set(const u32           instance,
                           const mep_conf_t   *const conf)
{
    vtss::Map<u32, mep_instance_data_t>::iterator  ii;
    u32                                            i, j, vid, port_cnt, res_port, con_rc, cc_rc, lm_rc, rc;
    mep_instance_data_t                            *data;    /* Instance data reference */
    BOOL                                           restart=FALSE, cc_restart=FALSE, config_ok=FALSE;
    mep_conf_t                                     config_save;

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    con_rc = cc_rc = lm_rc = VTSS_RC_OK;

    data = get_instance(instance);
    if (!data) {    /* Instance is not created */
        if (!conf->enable)                               return(VTSS_RC_OK);    /* This is disable of a non existing MEP - just return */
        data = mep_instance_create(instance);
        if (!data) {    /* Instance date is not created */
            return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        }
        instance_data_clear(instance, data);    /* Initialize instance */
    } else {                            /* Instance found */
        if (!conf->enable && !data->config.enable) {    /* This is disable of already disabled instance */
            instance_data.erase(instance);  /* Erase it */
            return(VTSS_RC_OK);
        }
    }

    if (data->vola)                                      return(VTSS_APPL_MEP_RC_VOLATILE);     /* This is a volatile instance */
    vid = 0;
    port_cnt = vtss_mep_port_count();
    res_port = conf->port;
    if (conf->enable) {
        if (data->config.enable) {  /* The MEP Instance is already enabled - some parameters must not change */
            if ((data->config.mode != conf->mode) || (data->config.direction != conf->direction) || (data->config.domain != conf->domain) ||
                (data->config.flow != conf->flow) || (data->config.port != res_port) ||
                (memcmp(&data->config.mac, &conf->mac, sizeof(data->config.mac))))              return(VTSS_APPL_MEP_RC_CHANGE_PARAMETER);
        }
        if (conf->level > 7)                                                                    return(VTSS_APPL_MEP_RC_INVALID_LEVEL);
        if (!MPLS_DOMAIN(conf->domain) && (conf->domain != VTSS_APPL_MEP_PORT)) {
            for (i=0; i<data->client_config.flow_count; i++) {
                if (data->client_config.level[i] <= conf->level)                                return(VTSS_APPL_MEP_RC_INVALID_CLIENT_LEVEL);
            }
        }
        if (conf->mep > 0x1FFF)                                                                 return(VTSS_APPL_MEP_RC_INVALID_MEP);
        if (conf->vid > 4095 && conf->vid != VTSS_MEP_MGMT_VID_ALL)                             return(VTSS_APPL_MEP_RC_INVALID_VID);
        if (res_port >= port_cnt)                                                               return(VTSS_APPL_MEP_RC_INVALID_PORT);
        if (conf->peer_count > mep_caps.mesa_oam_peer_cnt)                                      return(VTSS_APPL_MEP_RC_PEER_CNT);
        for (i=0; i<conf->peer_count; ++i) {
            if (conf->peer_mep[i] > 0x1FFF)                                                     return(VTSS_APPL_MEP_RC_PEER_ID);
            if (conf->peer_mep[i] == conf->mep)                                                 return(VTSS_APPL_MEP_RC_PEER_ID);
        }
        if ((conf->peer_count == 0) &&
            (data->cc_config.enable || data->lm_config.enable || data->lt_config.enable ||
             dm_enable_calc(data->dm_config.dm) || data->lst_config.enable ||
             data->aps_config.enable))                                                          return(VTSS_APPL_MEP_RC_PEER_CNT);
        if ((conf->peer_count != 1) && data->lm_sdeg_config.enable)                             return(VTSS_APPL_MEP_RC_PEER_CNT);



        if (conf->peer_count > 1)
        {
            if (data->lst_config.enable || data->lm_config.enable || data->aps_config.enable)   return(VTSS_APPL_MEP_RC_PEER_CNT);
            for (i=0; i<conf->peer_count; ++i)
                for (j=0; j<conf->peer_count; ++j)
                    if ((i != j) && (conf->peer_mep[i] == conf->peer_mep[j]))                   return(VTSS_APPL_MEP_RC_PEER_ID);
        }

        for (i=0; i<VTSS_APPL_MEP_MEG_CODE_LENGTH; ++i)      if (conf->name[i] == '\0') break;
        if (i == VTSS_APPL_MEP_MEG_CODE_LENGTH)                                                 return(VTSS_APPL_MEP_RC_INVALID_MEG);
        if (((conf->format == VTSS_APPL_MEP_ITU_ICC) ||
             (conf->format == VTSS_APPL_MEP_ITU_CC_ICC)) && (i != 0))                           return(VTSS_APPL_MEP_RC_INVALID_MEG);

        for (i=0; i<VTSS_APPL_MEP_MEG_CODE_LENGTH; ++i)      if (conf->meg[i] == '\0') break;
        if ((i == VTSS_APPL_MEP_MEG_CODE_LENGTH) || (i == 0))                                   return(VTSS_APPL_MEP_RC_INVALID_MEG);
        if ((conf->format == VTSS_APPL_MEP_ITU_ICC) && (i > 13))                                return(VTSS_APPL_MEP_RC_INVALID_MEG);
        if ((conf->format == VTSS_APPL_MEP_ITU_CC_ICC) && (i > 15))                             return(VTSS_APPL_MEP_RC_INVALID_MEG);

        if (data->config.enable) { /* enable -> enable - possibly restart of PDU transmission */
            restart = ((conf->level != data->config.level) || (conf->vid != data->config.vid) || memcmp(conf->mac.addr, data->config.mac.addr, sizeof(conf->mac.addr))) ? TRUE : FALSE;
            cc_restart = ((conf->format != data->config.format) || (conf->mep != data->config.mep) || (conf->peer_count != data->config.peer_count) ||
                          (conf->peer_mep != data->config.peer_mep) ||
                          memcmp(conf->name, data->config.name,sizeof(conf->name)) || memcmp(conf->meg, data->config.meg, sizeof(conf->meg))) ? TRUE : FALSE;
            T_D("restart %u  cc_restart %u", restart, cc_restart);
        }

        config_save = data->config;     /* Save the configuration in order to restore in case anything goes wrong */
        data->config = *conf;
        /*If new meg strlen is shorter than old meg strlen, the next statement will ensure that leftovers from old meg will be null'ed */
        strncpy(data->config.meg, conf->meg, VTSS_APPL_MEP_MEG_CODE_LENGTH-1);
    } else {            /* Disable */
        data->config.enable = FALSE;
        config_save = data->config;     /* Save the configuration in order to restore in case anything goes wrong */
    }

    con_rc = run_config(data, &config_ok);
    if (config_ok) {    /* Configuraton went well - check for any PDU transmission restarts */
        if (data->config.mode == VTSS_APPL_MEP_MEP) {
            if (restart || cc_restart) {    /* CC needs to be restarted */
                cc_rc = run_cc_config(instance);
            }
            if (restart) {                  /* Any PDU transmission needs to be restarted */
                run_dm_config(instance, TRUE);
                lm_rc = run_lm_config(instance);
                run_lb_config(instance, TRUE, NULL);
                run_tst_config(instance, TRUE, NULL);



            }
            if ((conf->peer_count != config_save.peer_count) || (conf->peer_mep != config_save.peer_mep)) {  /* Change in peer MEP configuration */
                syslog_new_peer_mep(data, &config_save);
                peer_mep_change_handle(data, &config_save);
            }
        }
    } else {    /* Something went wrong - restore the old configuration */
        data->config = config_save;
        /*If new meg strlen is shorter than old meg strlen, the next statement will ensure that leftovers from old meg will be null'ed */
        strncpy(data->config.meg, config_save.meg, VTSS_APPL_MEP_MEG_CODE_LENGTH-1);
    }

    rc = (con_rc != VTSS_RC_OK) ? con_rc : (cc_rc != VTSS_RC_OK) ? cc_rc : lm_rc;

    if ((rc == VTSS_RC_OK || rc == VTSS_APPL_MEP_RC_INVALID_INSTANCE) && (data->config.enable)
        && (data->out_state.state.mep.oper_state != VTSS_APPL_MEP_OPER_UP)) {
        rc = VTSS_APPL_MEP_RC_STATE_NOT_UP;
    }

    if (!data->config.enable) {
        /* delete instance: */
        instance_data.erase(instance);
    }

    T_N("Exit rc:%d\n",rc);

    return rc;
}

u32 vtss_mep_mgmt_volatile_conf_set(const u32          instance,
                                    const mep_conf_t  *const conf)
{
    mep_instance_data_t  *data;    /* Instance data reference */
    mep_default_conf_t   def_conf;
    u32                  rc = VTSS_RC_OK;

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        if (!conf->enable && !data->vola) {
            return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);    /* Only allowed to disable a volatile instance */
        }
        data->vola = FALSE;   /* Disable 'Volatile' while 'normal' instance configuring */
    }

    rc = vtss_mep_mgmt_conf_set(instance, conf);

    if (rc == VTSS_RC_OK) {
        data = get_instance(instance);
        if (data) {
            data->vola = conf->enable;    /* change instance volatile state */
        }
    } else { /* If the configure failed then disable instance as we do not want NOT operational volatile instances */
        vtss_mep_mgmt_default_conf_get(&def_conf);
        (void)vtss_mep_mgmt_conf_set(instance, &def_conf.config);
    }

    T_N("Exit");

    return(rc);
}


u32 vtss_mep_mgmt_volatile_sat_conf_set(const u32             instance,
                                        const mep_sat_conf_t  *const conf)
{
    mep_instance_data_t  *data;    /* Instance data reference */
    mep_default_conf_t   def_conf;
    u32                  rc = VTSS_RC_OK;

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                                                                        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (!conf->conf.voe || (conf->conf.domain != VTSS_APPL_MEP_EVC) || (conf->conf.direction != VTSS_APPL_MEP_UP))     return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    data = get_instance(instance);
    if (!data) {    /* Instance is not created */
        if (!conf->conf.enable)                               return(VTSS_RC_OK);    /* This is disable of a non existing MEP - just return */
        data = mep_instance_create(instance);   /* Have to create the instance here as .sat must TRUE when calling vtss_mep_mgmt_conf_set - not nice */
        if (!data) {    /* Instance date is not created */
            return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        }
        instance_data_clear(instance, data);    /* Initialize instance */
    } else {                            /* Instance found */
        if (!conf->conf.enable && !data->config.enable) {    /* This is disable of already disabled instance */
            instance_data.erase(instance);  /* Erase it */
            return(VTSS_RC_OK);
        }
    }

    memcpy(data->sat_prio_conf, conf->prio_conf, sizeof(data->sat_prio_conf));
    data->vola = FALSE;   /* Disable 'Volatile' while 'normal' instance configuring */
    data->sat = TRUE;     /* Enable SAT mode */

    rc = vtss_mep_mgmt_conf_set(instance, &conf->conf); /* Now try to configure with SAT  */

    data = get_instance(instance);
    if (data) {
        if (rc != VTSS_RC_OK) { /* If the configure failed then disable instance as we do not want NOT operational volatile instances */
            T_D("vtss_mep_mgmt_conf_set() failed  %s", error_txt(rc));
            data->sat = FALSE;
            vtss_mep_mgmt_default_conf_get(&def_conf);
            (void)vtss_mep_mgmt_conf_set(instance, &def_conf.config);
        } else {    /* SAT configure went well */
            data->vola = conf->conf.enable;    /* change instance volatile state */
            data->sat = conf->conf.enable;     /* change instance SAT state */
        }
    }

    T_N("Exit");

    return(rc);
}


u32 vtss_mep_mgmt_volatile_sat_conf_get(const u32       instance,
                                        mep_sat_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                                                                        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        if (!data->sat)     return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);    /* Only allowed to get a SAT instance */

        memcpy(conf->prio_conf, data->sat_prio_conf, sizeof(conf->prio_conf));
        conf->conf = data->config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}

u32 vtss_mep_mgmt_volatile_sat_counter_get(const u32           instance,
                                           mep_sat_counters_t  *const counters)
{
    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    memset(counters, 0, sizeof(*counters));

    return(VTSS_RC_OK);
}

u32 vtss_mep_mgmt_volatile_sat_counter_clear_set(const u32  instance)
{
    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    return(VTSS_RC_OK);
}

u32 vtss_mep_mgmt_volatile_sat_bps_act_get(const u32     instance,
                                           mep_tx_bps_t  *const bps)
{
    u32                  i, j;
    mep_instance_data_t  *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data == NULL) {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    memset(bps, 0, sizeof(*bps));
    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            if (data->tst_config.tests[i][j].enable) {
                if (!mep_model_tx_bps_act_get(instance, data->tst_tx_id[i][j], &bps->tx_bps[i][j])) {
                    T_D("TST mep_model_tx_bps_act_get failed  instance %u  prio %u  dei %u", instance, i, j);
                }
            } else if (data->lb_config.tests[i][j].enable) {
                if (!mep_model_tx_bps_act_get(instance, data->lbm_tx_id[i][j], &bps->tx_bps[i][j])) {
                    T_D("LB mep_model_tx_bps_act_get failed  instance %u  prio %u  dei %u", instance, i, j);
                }
            }
        }
    }

    return(VTSS_RC_OK);
}

BOOL vtss_mep_mgmt_conf_iter(u32 *instance)
{
    vtss::Map<u32, mep_instance_data_t>::iterator i;

    i = instance_data.greater_than_or_equal(*instance);
    if (i == instance_data.end()) {
        return FALSE;
    }
    *instance = (*i).first;
    return TRUE;
}

u32 vtss_mep_mgmt_conf_get(const u32         instance,
                           mep_conf_t       *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */
    BOOL vola;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->config;
        vola = data->vola;
    } else {
        mep_default_conf_t def_conf;
        vtss_mep_mgmt_default_conf_get(&def_conf);
        *conf = def_conf.config;
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

//    return(vola ? VTSS_APPL_MEP_RC_VOLATILE : VTSS_RC_OK);
    if (vola) {
        return(VTSS_APPL_MEP_RC_VOLATILE);
    } else {
        return(VTSS_RC_OK);
    }
}

BOOL vtss_mep_mgmt_is_instance_enabled(u32 instance)
{
    mep_instance_data_t *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)
        return FALSE;

    data = get_instance(instance);
    return (data && !data->vola && data->config.enable);
}

u32 vtss_mep_mgmt_get_next_enabled_instance(const u32    *const instance,
                                            u32          *const next)
{
    u32                 idx;

    for (idx = instance ? *instance + 1 : 0; idx < VTSS_APPL_MEP_INSTANCE_MAX; idx++) {
        if (vtss_mep_mgmt_is_instance_enabled(idx)) {
            *next = idx;
            return(VTSS_RC_OK);
        }
    }

    // Lead to maximum range <u32> in order to speed up the process time while SNMP 'GETNEXT' operation
    *next = 0xFFFFFFFF;
    return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
}

u32 vtss_mep_mgmt_get_next_enabled_instance_peer(const u32    *const instance,
                                                 u32          *const next_instance,
                                                 const u32    *const peer,
                                                 u32          *const next_peer)
{
    mesa_rc rc;
    mep_conf_t  config;
    u32 curr_instance;
    int curr_peer;

    T_D("Enter - instance %p  peer %p  *instance %u  *peer %u", instance, peer, (instance) ? *instance : 0, (peer) ? *peer : 0);

    if (instance == NULL) {  /* Get the first enabled instance as requested */
        if ((rc = vtss_mep_mgmt_get_next_enabled_instance(NULL, next_instance)) != VTSS_RC_OK) {
            T_D("Could not find next enabled instance, rc = %u", rc);
            return rc;
        }

    } else {
        // Check if the given instance is actually enabled; find the next enabled instance if not
        for (curr_instance = *instance; curr_instance < VTSS_APPL_MEP_INSTANCE_MAX; ++curr_instance) {
            if (vtss_mep_mgmt_is_instance_enabled(curr_instance)) {
                break;
            }
        }
        if (curr_instance == VTSS_APPL_MEP_INSTANCE_MAX) {
            T_D("Could not find any enabled instances");
            return VTSS_RC_ERROR;
        }

        *next_instance = curr_instance;
    }

    /* Now we have an enabled next instance to work with */

    curr_peer = peer != NULL ? *peer : -1;

    while (TRUE) {
        curr_instance = *next_instance;

        if ((rc = vtss_mep_mgmt_conf_get(curr_instance, &config)) != VTSS_RC_OK) {
            T_D("vtss_mep_mgmt_conf_get failed  instance %u  rc %s", curr_instance, error_txt(rc));
            return(rc);
        }

        // check if this instance has any peers at all
        if (config.peer_count > 0) {
            // We need to collect and sort the peer MEP IDs as they are most likely not in order,
            // i.e. we cannot assume that config.peer_mep[a] < config.peer_mep[a+1] for any given 'a'
            vtss::Vector<int> peer_ids;
            for (u32 peer_idx = 0; peer_idx < config.peer_count; ++peer_idx) {
                peer_ids.push_back(config.peer_mep[peer_idx]);
            }
            vtss::sort(peer_ids.begin(), peer_ids.end());
            // find the first peer ID that is greater than the given peer ID
            if (curr_peer >= 0) {
                auto up = vtss::upper_bound (peer_ids.begin(), peer_ids.end(), curr_peer);
                if (up != peer_ids.end()) {
                    *next_peer = (u32)*up;
                    break;
                }

            } else {
                // return first peer
                *next_peer = peer_ids[0];
                break;

            }
        }

        // we ran out of peers for this instance - try the next instance
        if ((rc = vtss_mep_mgmt_get_next_enabled_instance(&curr_instance, next_instance)) != VTSS_RC_OK) {
            T_D("Could not find next enabled instance #2, rc %u", rc);
            return rc;
        }

        // use first peer on next instance
        curr_peer = -1;
    }

    return (VTSS_RC_OK);
}

u32 vtss_mep_mgmt_etree_conf_get(const u32                    instance,
                                 vtss_mep_mgmt_etree_conf_t   *const conf)
{
    mep_model_mep_info_t  info;

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    memset(&info, 0, sizeof(info));     /* Get VOE CCM enabled informain */
    if (!mep_model_mep_info_get(instance, &info)) {
        T_D("mep_model_info_get failed");
    }
    conf->e_tree = mep_etree_calc(info.e_tree);

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_pm_conf_set(const u32                      instance,
                              const vtss_appl_mep_pm_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)           return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)                    return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (!conf->enable && !data->pm_config.enable)         return(VTSS_RC_OK);
    if (data->config.mode == VTSS_APPL_MEP_MIP)           return(VTSS_APPL_MEP_RC_MIP);


    if (conf->enable)   data->pm_config = *conf;
    else                data->pm_config.enable = FALSE;

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_pm_conf_get(const u32                 instance,
                              vtss_appl_mep_pm_conf_t   *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->pm_config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_lst_conf_set(const u32                       instance,
                               const vtss_appl_mep_lst_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)                      return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (!conf->enable && !data->lst_config.enable)          return(VTSS_RC_OK);
    if (data->config.mode == VTSS_APPL_MEP_MIP)             return(VTSS_APPL_MEP_RC_MIP);
    if (data->config.direction != VTSS_APPL_MEP_UP)         return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    if (data->config.peer_count != 1)                       return(VTSS_APPL_MEP_RC_PEER_CNT);
    if (!data->cc_config.enable)                            return(VTSS_APPL_MEP_RC_LST);
    if (!((data->cc_config.rate == VTSS_APPL_MEP_RATE_300S) ||
          (data->cc_config.rate == VTSS_APPL_MEP_RATE_100S) ||
          (data->cc_config.rate == VTSS_APPL_MEP_RATE_10S) ||
          (data->cc_config.rate == VTSS_APPL_MEP_RATE_1S))) return(VTSS_APPL_MEP_RC_RATE_INTERVAL);
    if (!data->cc_config.tlv_enable)                        return(VTSS_APPL_MEP_RC_LST);

    if (conf->enable)   data->lst_config = *conf;
    else                data->lst_config.enable = FALSE;

    run_lst_config(instance);

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_lst_conf_get(const u32                  instance,
                               vtss_appl_mep_lst_conf_t   *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->lst_config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_syslog_conf_set(const u32                          instance,
                                  const vtss_appl_mep_syslog_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)           return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)                    return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (!conf->enable && !data->syslog_config.enable)     return(VTSS_RC_OK);
    if (data->config.mode == VTSS_APPL_MEP_MIP)           return(VTSS_APPL_MEP_RC_MIP);


    if (conf->enable)   data->syslog_config = *conf;
    else                data->syslog_config.enable = FALSE;

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_syslog_conf_get(const u32                     instance,
                                  vtss_appl_mep_syslog_conf_t   *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->syslog_config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_cc_conf_set(const u32                        instance,
                              const vtss_appl_mep_cc_conf_t    *const conf)
{
    u32                  rc=0;
    mep_instance_data_t  *data;    /* Instance data reference */

    T_N("Enter %u  prio %u", instance, conf->prio);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                                               return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)                                                        return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (!memcmp(&data->cc_config, conf, sizeof(data->cc_config)))                             return(VTSS_RC_OK);
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                               return(VTSS_APPL_MEP_RC_MIP);
    if (data->out_state.state.mep.oper_state != VTSS_APPL_MEP_OPER_UP)                        return(VTSS_APPL_MEP_RC_INVALID_MEP_CONFIG);

    if (conf->enable)
    {

        if (data->lm_config.enable && (data->lm_config.ended == VTSS_APPL_MEP_DUAL_ENDED)) {
           if (conf->rate >  data->lm_config.rate)                                           return(VTSS_APPL_MEP_RC_LM_DUAL_ENDED_RATE_EXCEED_CC_RATE);
           // note that the enum is made such highest rate first, then lower and lower rates
        }
        if (conf->rate == VTSS_APPL_MEP_RATE_INV)                                             return(VTSS_APPL_MEP_RC_RATE_INTERVAL);
        if (conf->prio > 7)                                                                   return(VTSS_APPL_MEP_RC_INVALID_PRIO);
        if (data->config.peer_count == 0)                                                     return(VTSS_APPL_MEP_RC_PEER_CNT);
        if (data->lst_config.enable && !conf->tlv_enable)                                     return(VTSS_APPL_MEP_RC_LST);
        if (data->lst_config.enable && !((conf->rate == VTSS_APPL_MEP_RATE_300S) ||
                                         (conf->rate == VTSS_APPL_MEP_RATE_100S) ||
            (conf->rate == VTSS_APPL_MEP_RATE_10S) || (conf->rate == VTSS_APPL_MEP_RATE_1S))) return(VTSS_APPL_MEP_RC_RATE_INTERVAL);








    } else {
        if (data->lst_config.enable)                                                          return(VTSS_APPL_MEP_RC_LST);
    }

    data->cc_config = *conf;

    rc = run_cc_config(instance);

    T_N("Exit");

    return(rc);
}


u32 vtss_mep_mgmt_cc_conf_get(const u32                 instance,
                              vtss_appl_mep_cc_conf_t   *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->cc_config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}








































































































































































































































u32 vtss_mep_mgmt_lm_conf_set(const u32                      instance,
                              const vtss_appl_mep_lm_conf_t  *const conf)
{
    u32                  rc=0;
    u32                  unicast_peer_index = 0;
    mep_instance_data_t  *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                                                    return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)                                                             return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                                    return(VTSS_APPL_MEP_RC_MIP);

    if (conf->enable) {
        if (!conf->synthetic && data->cc_config.enable && (conf->ended == VTSS_APPL_MEP_DUAL_ENDED)) {
           if (data->cc_config.rate >  conf->rate)                                                 return(VTSS_APPL_MEP_RC_LM_DUAL_ENDED_RATE_EXCEED_CC_RATE);
           // note that the enum is made such highest rate first, then lower and lower rates
        }
        if (!memcmp(&data->lm_config, conf, sizeof(data->lm_config)))                              return(VTSS_RC_OK);
        if (conf->rate == VTSS_APPL_MEP_RATE_INV)                                                  return(VTSS_APPL_MEP_RC_RATE_INTERVAL);
        if (conf->prio > 7)                                                                        return(VTSS_APPL_MEP_RC_INVALID_PRIO);
        if (data->lm_config.enable && ((conf->ended != data->lm_config.ended) ||
                (conf->size != data->lm_config.size) ||
                (conf->slm_test_id != data->lm_config.slm_test_id) ||
                (conf->synthetic != data->lm_config.synthetic)))                                   return(VTSS_APPL_MEP_RC_CHANGE_PARAMETER);
        if (conf->synthetic && (frame_rate_calc(conf->rate) == 0))                                 return(VTSS_APPL_MEP_RC_MEAS_INTERVAL);
        if (conf->synthetic && (conf->rate == VTSS_APPL_MEP_RATE_300S))                            return(VTSS_APPL_MEP_RC_RATE_INTERVAL);
        if (!conf->synthetic) {
            if ((conf->ended == VTSS_APPL_MEP_DUAL_ENDED) &&
                (conf->rate != VTSS_APPL_MEP_RATE_1S) && (conf->rate != VTSS_APPL_MEP_RATE_10S))   return(VTSS_APPL_MEP_RC_RATE_INTERVAL);
            if ((conf->ended == VTSS_APPL_MEP_SINGEL_ENDED) &&
                (conf->rate != VTSS_APPL_MEP_RATE_6M) && (conf->rate != VTSS_APPL_MEP_RATE_1S) &&
                (conf->rate != VTSS_APPL_MEP_RATE_10S))                                            return(VTSS_APPL_MEP_RC_RATE_INTERVAL);
        }
        if (data->config.peer_count == 0)                                                          return(VTSS_APPL_MEP_RC_PEER_CNT);
        if (!conf->synthetic && (data->config.peer_count != 1))                                    return(VTSS_APPL_MEP_RC_PEER_CNT);
        if (conf->synthetic && (data->config.peer_count > MEP_MODEL_MEP_PEER_MAX))                 return(VTSS_APPL_MEP_RC_PEER_CNT);

        if (!MPLS_DOMAIN(data->config.domain) && conf->cast == VTSS_APPL_MEP_UNICAST) {
            // Handle unicast LM
            if (!conf->synthetic) {
                // For LM there is only one peer
                unicast_peer_index = 0;
            } else {
                // Locate the SLM peer with the given MEP ID
                if (!get_peer_index(data, conf->mep, unicast_peer_index))                          return(VTSS_APPL_MEP_RC_PEER_ID);
            }
            // Check if we have the MAC address of the selected peer
            if (!memcmp(all_zero_mac, data->config.peer_mac[unicast_peer_index].addr,
                       VTSS_APPL_MEP_MAC_LENGTH))                                                  return(VTSS_APPL_MEP_RC_PEER_MAC);
        }

        if (!conf->synthetic && (data->config.domain == VTSS_APPL_MEP_VLAN))                       return(VTSS_APPL_MEP_RC_VLAN_SUPPORT);
        if (data->out_state.state.mep.oper_state != VTSS_APPL_MEP_OPER_UP)                         return(VTSS_APPL_MEP_RC_INVALID_MEP_CONFIG);










        for (int peer_index = 0; peer_index < data->config.peer_count; peer_index++) {
            vtss_mep_mgmt_lm_notif_set(instance, peer_index);
        }

        mep_timer_start(1000 * conf->los_int_cnt_holddown, &data->los_int_cnt_timer, data, run_los_int_cnt_timer);
        mep_timer_start(1000 * conf->los_th_cnt_holddown, &data->los_th_cnt_timer, data, run_los_th_cnt_timer);
    } else {
        if (conf->enable_rx) {
            if (conf->synthetic &&
                    (data->config.peer_count == 0 ||
                     data->config.peer_count > MEP_MODEL_MEP_PEER_MAX))                            return(VTSS_APPL_MEP_RC_PEER_CNT);
        }

        if (data->lm_avail_config.enable) {
            for (int peer_index = 0; peer_index < data->config.peer_count; peer_index++) {
                vtss_mep_mgmt_lm_notif_set(instance, peer_index);
            }
        } else {
            vtss_mep_lm_notif_status_set(instance, NULL);
        }
    }

    if ((data->lm_config.enable && !conf->enable) || (data->lm_config.enable_rx && !conf->enable_rx)) {
        // we are disabling either LM TX or RX - grab the current LM counters before disabling for good
        vtss_appl_mep_lm_state_t *curr_state;

        if (data->lm_config.synthetic) {
            for (u32 peer_idx = 0; peer_idx < data->config.peer_count; peer_idx++) {
                curr_state = &data->out_state.lm_state[peer_idx];
                get_sl_pdu_counters(data, peer_idx, curr_state);
            }

        } else {
            curr_state = &data->out_state.lm_state[0];
            get_lm_pdu_counters(data, curr_state);
        }
    }

    // control counter clear action in run_lm_config
    data->lm_state.clear_counters = FALSE;

    if (conf->enable && !data->lm_config.enable) {
        // we are enabling TX - always clear TX counters
        data->lm_state.clear_counters = TRUE;

        if (conf->ended == VTSS_APPL_MEP_DUAL_ENDED && data->lm_config.enable_rx) {
            // AA-9962: Don't clear RX when enabling TX on dual-ended when RX is already enabled
            data->lm_state.clear_dir = VTSS_APPL_MEP_CLEAR_DIR_TX;
        } else {
            data->lm_state.clear_dir = VTSS_APPL_MEP_CLEAR_DIR_BOTH;
        }
    }
    if (conf->enable_rx && !data->lm_config.enable_rx && !conf->enable) {
        // We are enabling RX without TX
        data->lm_state.clear_counters = TRUE;
        data->lm_state.clear_dir = VTSS_APPL_MEP_CLEAR_DIR_BOTH;
    }
    if (!conf->enable && data->lm_config.enable && conf->ended == VTSS_APPL_MEP_SINGEL_ENDED && conf->enable_rx) {
        // We are disabling a single-ended initiator and RX is still enabled
        // (as the RX counter suddenly counts SLM or 1SL PDUs instead of SLR PDUs)
        data->lm_state.clear_counters = TRUE;
        data->lm_state.clear_dir = VTSS_APPL_MEP_CLEAR_DIR_BOTH;
    }

    // Set new configuration
    data->lm_config = *conf;

    // Set unicast peer index
    data->lm_state.unicast_peer_index = unicast_peer_index;

    rc = run_lm_config(instance);

    // don't clear counters again unless specifically enabled above
    data->lm_state.clear_counters = FALSE;

    return(rc);
}


u32 vtss_mep_mgmt_lm_conf_get(const u32                instance,
                             vtss_appl_mep_lm_conf_t   *const conf)
{
    mep_instance_data_t            *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->lm_config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}

static void clear_lm_avail_fifo(mep_instance_data_t *data)
{
    if (data->lm_avail_fifo) {
        VTSS_FREE(data->lm_avail_fifo);
        data->lm_avail_fifo = NULL;
    }

    data->lm_avail_fifo_ix = 0;
}


static void vtss_mep_lm_avail_clear_peer_states(out_state_t *state)
{
    for (int peer_idx = 0; peer_idx < mep_caps.mesa_oam_peer_cnt; peer_idx++) {
        state->lm_avail_state[peer_idx].near_state  = VTSS_APPL_MEP_OAM_LM_AVAIL;
        state->lm_avail_state[peer_idx].far_state   = VTSS_APPL_MEP_OAM_LM_AVAIL;

        state->lm_avail_state[peer_idx].near_cnt    = 0;
        state->lm_avail_state[peer_idx].far_cnt     = 0;
        state->lm_avail_state[peer_idx].near_un_cnt = 0;
        state->lm_avail_state[peer_idx].far_un_cnt  = 0;

        state->lm_avail_state[peer_idx].far_avail_cnt = 0;
        state->lm_avail_state[peer_idx].near_avail_cnt = 0;
    }
}


u32 vtss_mep_mgmt_lm_avail_conf_set(const u32                            instance,
                                    const vtss_appl_mep_lm_avail_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                                    return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)                                             return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                    return(VTSS_APPL_MEP_RC_MIP);

    if (conf->enable) {
        if (!memcmp(&data->lm_avail_config, conf, sizeof(data->lm_avail_config)))  return(VTSS_RC_OK);
        if (conf->flr_th > 1000)                                                   return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        if (conf->interval != data->lm_avail_config.interval &&
            data->lm_config.enable)                                                return(VTSS_APPL_MEP_RC_CHANGE_PARAMETER);
        if (conf->interval != data->lm_avail_config.interval || !data->lm_avail_fifo) {
            if (data->lm_avail_fifo) {
                VTSS_FREE(data->lm_avail_fifo);
                data->lm_avail_fifo = NULL;
            }
            data->lm_avail_fifo_ix = 0;
            if (conf->interval) {
                data->lm_avail_fifo = (lm_counters_t *)VTSS_CALLOC(conf->interval, sizeof(data->lm_avail_fifo[0]));
                if (!data->lm_avail_fifo)                                          return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
            }
        }

        for (int peer_index = 0; peer_index < data->config.peer_count; peer_index++) {
            vtss_mep_mgmt_lm_notif_set(instance, peer_index);
        }

    } else {

        vtss_mep_lm_avail_clear_peer_states(&data->out_state);

        clear_lm_avail_fifo(data);

        if (data->lm_config.enable) {
            for (int peer_index = 0; peer_index < data->config.peer_count; peer_index++) {
                vtss_mep_mgmt_lm_notif_set(instance, peer_index);
            }
        } else {
            vtss_mep_lm_notif_status_set(instance, NULL);
        }
    }

    data->lm_avail_config = *conf;

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_lm_avail_conf_get(const u32                      instance,
                                    vtss_appl_mep_lm_avail_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->lm_avail_config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_lm_hli_conf_set(const u32                          instance,
                                  const vtss_appl_mep_lm_hli_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                                 return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)                                          return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                 return(VTSS_APPL_MEP_RC_MIP);

    if (conf->enable) {
        if (!memcmp(&data->lm_hli_config, conf, sizeof(data->lm_hli_config)))   return(VTSS_RC_OK);
        if (conf->flr_th > 1000)                                                return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

        for (u32 peer_index = 0; peer_index < data->config.peer_count; peer_index++) {
            vtss_mep_lm_hli_status_set(instance, peer_index, &data->out_state.lm_hli_state[peer_index]);
        }

        mep_timer_start(1000 * conf->cnt_holddown, &data->hli_cnt_timer, data, run_hli_cnt_timer);
    } else {
        for (u32 peer_index = 0; peer_index < data->config.peer_count; peer_index++) {
            data->out_state.lm_hli_state[peer_index].near_cnt     = 0;
            data->out_state.lm_hli_state[peer_index].far_cnt      = 0;
            data->out_state.lm_hli_state[peer_index].near_con_cnt = 0;
            data->out_state.lm_hli_state[peer_index].far_con_cnt  = 0;
            data->lm_hli_state[peer_index].near_cnt               = 0;
            data->lm_hli_state[peer_index].far_cnt                = 0;
            vtss_mep_lm_hli_status_set(instance, data->config.peer_mep[peer_index], NULL);
        }
    }

    data->lm_hli_config = *conf;

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_lm_hli_conf_get(const u32                    instance,
                                  vtss_appl_mep_lm_hli_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->lm_hli_config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_lm_sdeg_conf_set(const u32                           instance,
                                   const vtss_appl_mep_lm_sdeg_conf_t  *const conf)
{
    mep_instance_data_t  *data;    /* Instance data reference */
    BOOL                 ssf_ssd = FALSE;

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                                  return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)                                           return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                  return(VTSS_APPL_MEP_RC_MIP);

    if (conf->enable) {
        if (!memcmp(&data->lm_sdeg_config, conf, sizeof(data->lm_sdeg_config)))  return(VTSS_RC_OK);
        if (conf->flr_th > 1000)                                                 return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        if (data->config.peer_count != 1)                                        return(VTSS_APPL_MEP_RC_PEER_CNT);
    } else {
        data->lm_sdeg_bad      = FALSE;
        data->lm_sdeg_bad_cnt  = 0;
        data->lm_sdeg_good_cnt = 0;
        if (data->dDeg) {
            data->dDeg = FALSE;
            ssf_ssd = TRUE;
        }
    }

    data->lm_sdeg_config = *conf;

    if (ssf_ssd) {
        run_ssf_ssd_state(instance);
    }

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_lm_sdeg_conf_get(const u32                     instance,
                                   vtss_appl_mep_lm_sdeg_conf_t  *const conf)
{
    mep_instance_data_t  *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->lm_sdeg_config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_dm_conf_set(const u32                      instance,
                              const vtss_mep_mgmt_dm_conf_t  *const conf)
{
    u8                              i;
    mep_instance_data_t             *data;    /* Instance data reference */
    BOOL                            enable = FALSE, restart=FALSE;
    BOOL                            cos_id[VTSS_MEP_COS_ID_SIZE];
    const vtss_appl_mep_dm_common_conf_t  *common;

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                    return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    memset(cos_id, 0, sizeof(cos_id));

    if (!data || !data->config.enable)                             return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (data->config.mode == VTSS_APPL_MEP_MIP)                    return(VTSS_APPL_MEP_RC_MIP);
    if (!memcmp(&data->dm_config, conf, sizeof(data->dm_config)))  return(VTSS_RC_OK);
    if (data->out_state.state.mep.oper_state != VTSS_APPL_MEP_OPER_UP) return(VTSS_APPL_MEP_RC_INVALID_MEP_CONFIG);






    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        if (conf->dm[i].enable) {
            enable = TRUE;



            if (data->sat && (data->sat_prio_conf[i].vid == VTSS_MEP_MGMT_SAT_VID_UNUSED))          return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        }
    }

    T_D("enable %u", enable);

    if (enable)
    {
        common = &conf->common;
        if (data->config.peer_count == 0)                                                           return(VTSS_APPL_MEP_RC_PEER_CNT);
        if (common->cast == VTSS_APPL_MEP_UNICAST && !MPLS_DOMAIN(data->config.domain))
        {/* All zero mac - mep must be a known peer mep */
            for (i=0; i<data->config.peer_count; ++i)
                if (common->mep == data->config.peer_mep[i]) break;
            if (i == data->config.peer_count)                                                       return(VTSS_APPL_MEP_RC_PEER_ID);
            if (!MPLS_DOMAIN(data->config.domain)) {
                if (!memcmp(all_zero_mac, data->config.peer_mac[i].addr, VTSS_APPL_MEP_MAC_LENGTH)) return(VTSS_APPL_MEP_RC_PEER_MAC);
            }
        }
        if (dm_enable_calc(data->dm_config.dm) && (common->ended != data->dm_config.common.ended))                     return(VTSS_APPL_MEP_RC_CHANGE_PARAMETER);
        if ((common->interval < VTSS_APPL_MEP_DM_INTERVAL_MIN) || (common->interval > VTSS_APPL_MEP_DM_INTERVAL_MAX))  return(VTSS_APPL_MEP_RC_RATE_INTERVAL);
        if ((common->lastn < VTSS_APPL_MEP_DM_LASTN_MIN) || (common->lastn > VTSS_APPL_MEP_DM_LASTN_MAX))              return(VTSS_APPL_MEP_RC_LAST_N);
        if ((common->tunit != VTSS_APPL_MEP_US) && (common->tunit != VTSS_APPL_MEP_NS))                                return(VTSS_APPL_MEP_RC_T_UNIT);
        if ((common->overflow_act != VTSS_APPL_MEP_DISABLE) && (common->overflow_act != VTSS_APPL_MEP_CONTINUE))       return(VTSS_APPL_MEP_RC_OVERFLOW);
        if ((common->num_of_bin_fd < VTSS_APPL_MEP_DM_BINS_FD_MIN) || (common->num_of_bin_fd > VTSS_APPL_MEP_DM_BINS_FD_MAX))          return(VTSS_APPL_MEP_RC_FD_BIN);
        if ((common->num_of_bin_ifdv < VTSS_APPL_MEP_DM_BINS_IFDV_MIN) || (common->num_of_bin_ifdv > VTSS_APPL_MEP_DM_BINS_IFDV_MAX))  return(VTSS_APPL_MEP_RC_IFDV_BIN);
        if (common->m_threshold == 0)                                                                                                  return(VTSS_APPL_MEP_RC_M_THRESHOLD);

        restart = ((conf->common.cast != common->cast) || (conf->common.mep != common->mep) ||
                   (data->dm_config.common.tunit != common->tunit) ||
                   (conf->common.ended != common->ended) || (conf->common.interval != common->interval)) ? TRUE : FALSE;

    }

    data->dm_config = *conf;

    run_dm_config(instance, restart);

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_dm_conf_get(const u32                instance,
                              vtss_mep_mgmt_dm_conf_t  *const conf)
{
    mep_instance_data_t             *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->dm_config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_aps_conf_set(const u32                       instance,
                               const vtss_appl_mep_aps_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                                             return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)                                                      return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (!conf->enable && !data->aps_config.enable)                                          return(VTSS_RC_OK);
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                             return(VTSS_APPL_MEP_RC_MIP);
    if ((conf->type == VTSS_APPL_MEP_R_APS) && (data->config.domain == VTSS_APPL_MEP_EVC))  return(VTSS_APPL_MEP_RC_APS_DOMAIN);
    if ((conf->type == VTSS_APPL_MEP_R_APS) && (data->config.domain == VTSS_APPL_MEP_PORT) &&
        (data->config.vid == 0))                                                            return(VTSS_APPL_MEP_RC_INVALID_VID);
    if ((conf->type == VTSS_APPL_MEP_L_APS) && (data->config.domain == VTSS_APPL_MEP_EVC) &&
        (data->config.direction == VTSS_APPL_MEP_UP))                                       return(VTSS_APPL_MEP_RC_APS_UP);
    if (data->config.peer_count != 1)                                                       return(VTSS_APPL_MEP_RC_PEER_CNT);

    if (conf->enable)
    {
        if (!memcmp(&data->aps_config, conf, sizeof(data->aps_config)))                     return(VTSS_RC_OK);
        if (conf->prio > 7)                                                                 return(VTSS_APPL_MEP_RC_INVALID_PRIO);
    }
    if (data->out_state.state.mep.oper_state != VTSS_APPL_MEP_OPER_UP)                      return(VTSS_APPL_MEP_RC_INVALID_MEP_CONFIG);
    if (conf->enable)   data->aps_config = *conf;
    else                data->aps_config.enable = FALSE;

    if (data->aps_config.type == VTSS_APPL_MEP_R_APS) { /* Configuration only needed in case of RAPS */
        run_aps_config(instance);
    }

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_aps_conf_get(const u32                 instance,
                               vtss_appl_mep_aps_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->aps_config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_lt_conf_set(const u32                      instance,
                              const vtss_appl_mep_lt_conf_t  *const conf)
{
    vtss::Map<u32, mep_instance_data_t>::iterator ii;
    u32 i;
    mep_instance_data_t   *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    T_D("instance %u  enable %u  transactions %u", instance, conf->enable, conf->transactions);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)            return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)                     return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (!conf->enable && !data->lt_config.enable)          return(VTSS_RC_OK);
    if (data->config.mode == VTSS_APPL_MEP_MIP)            return(VTSS_APPL_MEP_RC_MIP);








    if (conf->enable)
    {
        if (data->lt_config.enable)                        return(VTSS_APPL_MEP_RC_CHANGE_PARAMETER);
        if (conf->ttl > 0xFF)                              return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        if (conf->ttl == 0)                                return(VTSS_APPL_MEP_RC_TTL_ZERO);
        if (conf->prio > 7)                                return(VTSS_APPL_MEP_RC_INVALID_PRIO);
        if (data->config.peer_count == 0)                  return(VTSS_APPL_MEP_RC_PEER_CNT);
        if ((data->lt_config.transactions == 0) ||
            (data->lt_config.transactions > VTSS_APPL_MEP_TRANSACTION_MAX))                      return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        if (data->out_state.state.mep.oper_state != VTSS_APPL_MEP_OPER_UP)                       return(VTSS_APPL_MEP_RC_INVALID_MEP_CONFIG);
        if (!memcmp(all_zero_mac, conf->mac.addr, VTSS_APPL_MEP_MAC_LENGTH))
        {/* All zero mac - mep must be a known peer mep */
            for (i=0; i<data->config.peer_count; ++i)
                if (conf->mep == data->config.peer_mep[i]) break;
            if (i == data->config.peer_count)                                                    return(VTSS_APPL_MEP_RC_PEER_ID);
            if (!memcmp(all_zero_mac, data->config.peer_mac[i].addr, VTSS_APPL_MEP_MAC_LENGTH))  return(VTSS_APPL_MEP_RC_PEER_MAC);
            for (ii = instance_data.begin(); ii != instance_data.end(); ++ii) {  /* Check if the peer MEP is in this node */
                if (((*ii).first != instance) && (data->config.domain == (*ii).second.config.domain) &&
                    (data->config.flow == (*ii).second.config.flow) && (conf->mep == (*ii).second.config.mep)) return(VTSS_APPL_MEP_RC_PEER_ID);
            }
        }








    }

    if (conf->enable)   data->lt_config = *conf;
    else                data->lt_config.enable = FALSE;

    run_lt_config(data);

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_lt_conf_get(const u32                 instance,
                               vtss_appl_mep_lt_conf_t  *const conf)
{
    mep_instance_data_t   *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->lt_config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_lb_conf_set(const u32                      instance,
                              const vtss_mep_mgmt_lb_conf_t  *const conf,
                                    u64                      *active_time_ms)
{
    u32                   i, j, rc;
    mep_instance_data_t   *data;    /* Instance data reference */
    BOOL                  enable = FALSE;
    BOOL                  cos_id[VTSS_MEP_COS_ID_SIZE];
    vtss_mep_mgmt_lb_conf_t old_lb_config;

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                                       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    memset(cos_id, 0, sizeof(cos_id));

    if (!data || !data->config.enable)                                                return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                       return(VTSS_APPL_MEP_RC_MIP);
    if (test_enable_calc(conf->tests) && (test_enable_calc(data->tst_config.tests) || data->tst_config.common.enable_rx))   return(VTSS_APPL_MEP_RC_TEST);







    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        /* The actual MAX frame size may be less if one or more tag are added */
        u32 max_frame_size = max_size_lb_tst_calc(data, VTSS_APPL_MEP_LBM_SIZE_MAX, i);
        u32 max_cpu_frame_size = max_size_lb_tst_calc(data, mep_caps.appl_mep_packet_rx_mtu, i);

        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            if (conf->tests[i][j].enable) {
                enable = TRUE;
                if (MPLS_DOMAIN(data->config.domain)) {
                    if (conf->tests[i][j].size > VTSS_APPL_MEP_LBM_MPLS_SIZE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_LB_FRM_SIZE);
                } else {
                    if (conf->common.to_send > 0) {
                        // Packets need to be copied to CPU
                        if (conf->tests[i][j].size > max_cpu_frame_size)                return(VTSS_APPL_MEP_RC_INVALID_LB_FRM_SIZE);
                    } else {
                        // Packets will just be counted by HW
                       if (conf->tests[i][j].size > max_frame_size)                     return(VTSS_APPL_MEP_RC_INVALID_LB_FRM_SIZE);
                    }
                }
                if (conf->tests[i][j].size < VTSS_APPL_MEP_LBM_SIZE_MIN)                return(VTSS_APPL_MEP_RC_INVALID_LB_FRM_SIZE);
                // Calculate layer-1 bitrate and validate rate range
                u32 l1rate = get_l1_bitrate(conf->tests[i][j].rate, conf->tests[i][j].size, data->config.domain);
                if (l1rate > VTSS_APPL_MEP_TST_RATE_MAX)                                return(VTSS_APPL_MEP_RC_RATE_INTERVAL);
                if (l1rate < 1)                                                         return(VTSS_APPL_MEP_RC_RATE_INTERVAL);



                if (data->sat && (data->sat_prio_conf[i].vid == VTSS_MEP_MGMT_SAT_VID_UNUSED))  return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
            }
        }
    }

    T_D("instance %u  enable %u", instance, enable);

    if (enable)
    {
        if (test_enable_calc(data->lb_config.tests) && (data->lb_config.common.to_send != 0))                    return(VTSS_APPL_MEP_RC_CHANGE_PARAMETER);
        if (test_enable_calc(data->lb_config.tests) && (data->lb_config.common.to_send != conf->common.to_send)) return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        if ((conf->common.to_send != VTSS_APPL_MEP_LB_TO_SEND_INFINITE) && (conf->common.to_send == 0))          return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        if ((conf->common.to_send == VTSS_APPL_MEP_LB_TO_SEND_INFINITE) && !data->config.voe)                    return(VTSS_APPL_MEP_RC_NO_VOE);
        if ((conf->common.to_send != VTSS_APPL_MEP_LB_TO_SEND_INFINITE) && (conf->common.interval > 100))        return(VTSS_APPL_MEP_RC_RATE_INTERVAL);
        if (conf->common.cast == VTSS_APPL_MEP_UNICAST)
        if (conf->common.cast == VTSS_APPL_MEP_UNICAST && !MPLS_DOMAIN(data->config.domain))
        {
            if (!memcmp(all_zero_mac, conf->common.mac.addr, VTSS_APPL_MEP_MAC_LENGTH))
            {/* All zero mac - mep must be a known peer mep */
                for (i=0; i<data->config.peer_count; ++i)
                    if (conf->common.mep == data->config.peer_mep[i]) break;
                if (i == data->config.peer_count)                                                       return(VTSS_APPL_MEP_RC_PEER_ID);
                if (!memcmp(all_zero_mac, data->config.peer_mac[i].addr, VTSS_APPL_MEP_MAC_LENGTH))     return(VTSS_APPL_MEP_RC_PEER_MAC);
            }
        }
    }

    old_lb_config = data->lb_config;
    data->lb_config = *conf;

    rc = run_lb_config(instance, FALSE, active_time_ms);
    if (rc != VTSS_RC_OK) {
        data->lb_config = old_lb_config;
    }
    return(rc);
}


u32 vtss_mep_mgmt_lb_conf_get(const u32                 instance,
                              vtss_mep_mgmt_lb_conf_t   *const conf)
{
    mep_instance_data_t   *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->lb_config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_ais_conf_set(const u32                       instance,
                               const vtss_appl_mep_ais_conf_t  *const conf)
{
    mep_instance_data_t  *data;    /* Instance data reference */

    T_DG(TRACE_GRP_AIS_LCK, "Enter  Instance %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);

    if (!data || !data->config.enable)                                                                   return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (!conf->enable && !data->ais_config.enable)                                                       return(VTSS_RC_OK);
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                                          return(VTSS_APPL_MEP_RC_MIP);
    if (conf->enable) {
        if (!data->client_config.flow_count)                                                             return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        if ((data->config.domain == VTSS_APPL_MEP_EVC) && (data->config.direction == VTSS_APPL_MEP_UP))  return(VTSS_APPL_MEP_RC_UP_CLIENT);
        if ((conf->rate != VTSS_APPL_MEP_RATE_1S) && (conf->rate != VTSS_APPL_MEP_RATE_1M))              return(VTSS_APPL_MEP_RC_RATE_INTERVAL);
        for (u32 i = 0; i < data->client_config.flow_count; i++) {
            if ((data->client_config.domain[i] != VTSS_APPL_MEP_MPLS_LSP) &&
                (data->client_config.domain[i] != VTSS_APPL_MEP_EVC) &&
                (data->client_config.domain[i] != VTSS_APPL_MEP_VLAN))                                   return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        }
        if (data->out_state.state.mep.oper_state != VTSS_APPL_MEP_OPER_UP)                               return(VTSS_APPL_MEP_RC_INVALID_MEP_CONFIG);
    }

    data->ais_config = *conf;

    run_ais_config(instance);

    T_DG(TRACE_GRP_AIS_LCK, "Exit");

    return(VTSS_RC_OK);
}

u32 vtss_mep_mgmt_ais_conf_get(const u32                 instance,
                               vtss_appl_mep_ais_conf_t  *const conf)
{
    mep_instance_data_t  *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->ais_config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}

u32 vtss_mep_mgmt_lck_conf_set(const u32                       instance,
                               const vtss_appl_mep_lck_conf_t  *const conf)
{
    mep_instance_data_t  *data;    /* Instance data reference */

    T_DG(TRACE_GRP_AIS_LCK, "Enter  Instance %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);

    if (!data || !data->config.enable)                                                                   return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (!conf->enable && !data->lck_config.enable)                                                       return(VTSS_RC_OK);
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                                          return(VTSS_APPL_MEP_RC_MIP);
    if (conf->enable) {
        if (!data->client_config.flow_count)                                                             return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        if ((data->config.domain == VTSS_APPL_MEP_EVC) && (data->config.direction == VTSS_APPL_MEP_UP))  return(VTSS_APPL_MEP_RC_UP_CLIENT);
        if ((conf->rate != VTSS_APPL_MEP_RATE_1S) && (conf->rate != VTSS_APPL_MEP_RATE_1M))              return(VTSS_APPL_MEP_RC_RATE_INTERVAL);
        for (u32 i = 0; i < data->client_config.flow_count; i++) {
            if ((data->client_config.domain[i] != VTSS_APPL_MEP_MPLS_LSP) &&
                (data->client_config.domain[i] != VTSS_APPL_MEP_EVC) &&
                (data->client_config.domain[i] != VTSS_APPL_MEP_VLAN))                                   return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        }
        if (data->out_state.state.mep.oper_state != VTSS_APPL_MEP_OPER_UP)                               return(VTSS_APPL_MEP_RC_INVALID_MEP_CONFIG);
    }

    data->lck_config = *conf;

    run_lck_config(instance);

    T_DG(TRACE_GRP_AIS_LCK, "Exit");

    return(VTSS_RC_OK);
}

u32 vtss_mep_mgmt_lck_conf_get(const u32                 instance,
                               vtss_appl_mep_lck_conf_t  *const conf)
{
    mep_instance_data_t  *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->lck_config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}

u32 vtss_mep_mgmt_tst_conf_set(const u32                       instance,
                               const vtss_mep_mgmt_tst_conf_t  *const conf,
                                     u64                       *active_time_ms)
{
    mep_instance_data_t *data;    /* Instance data reference */
    BOOL                cos_id[VTSS_MEP_COS_ID_SIZE];
    u32 rc;
    vtss_mep_mgmt_tst_conf_t old_tst_config;

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)                                            return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    memset(cos_id, 0, sizeof(cos_id));

    if (data->config.mode == VTSS_APPL_MEP_MIP)                                   return(VTSS_APPL_MEP_RC_MIP);
    if (test_enable_calc(data->lb_config.tests) && (test_enable_calc(conf->tests) || conf->common.enable_rx) ) return(VTSS_APPL_MEP_RC_TEST);

    if (mep_caps.appl_afi) {
        u32 i, j;
        BOOL enable = FALSE;
        if (data->out_state.state.mep.oper_state != VTSS_APPL_MEP_OPER_UP)            return(VTSS_APPL_MEP_RC_INVALID_MEP_CONFIG);






        for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
            /* The actual MAX frame size may be less if one or more tag are added */
            u32 max_size = max_size_lb_tst_calc(data, VTSS_APPL_MEP_TST_SIZE_MAX, i);

            for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
                if (conf->tests[i][j].enable) {
                    enable = TRUE;
                }
                if (MPLS_DOMAIN(data->config.domain)) {
                    if (conf->tests[i][j].size > VTSS_APPL_MEP_TST_MPLS_SIZE_MAX) return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
                } else {
                    if (conf->tests[i][j].size > max_size)                        return(VTSS_APPL_MEP_RC_INVALID_TST_FRM_SIZE);
                }
                if (conf->tests[i][j].size < VTSS_APPL_MEP_TST_SIZE_MIN)          return(VTSS_APPL_MEP_RC_INVALID_TST_FRM_SIZE);
                // Calculate layer-1 bitrate and validate rate range
                u32 l1rate = get_l1_bitrate(conf->tests[i][j].rate, conf->tests[i][j].size, data->config.domain);
                if (l1rate > VTSS_APPL_MEP_TST_RATE_MAX)                          return(VTSS_APPL_MEP_RC_RATE_INTERVAL);
                if (l1rate < 1)                                                   return(VTSS_APPL_MEP_RC_RATE_INTERVAL);




                if (conf->tests[i][j].enable && data->sat &&
                    (data->sat_prio_conf[i].vid == VTSS_MEP_MGMT_SAT_VID_UNUSED)) return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

            }
        }

        T_D("instance %u  enable %u", instance, enable);

        if (enable)
        {
            if (!data->config.voe)                                                               return(VTSS_APPL_MEP_RC_NO_VOE);
            if (!memcmp(&data->tst_config, conf, sizeof(data->tst_config)))                      return(VTSS_RC_OK);
            for (i=0; i<data->config.peer_count; ++i)
                if (conf->common.mep == data->config.peer_mep[i]) break;
            if (i == data->config.peer_count)                                                    return(VTSS_APPL_MEP_RC_PEER_ID);
            if (!MPLS_DOMAIN(data->config.domain)) {
                if (!memcmp(all_zero_mac, data->config.peer_mac[i].addr, sizeof(all_zero_mac)))  return(VTSS_APPL_MEP_RC_PEER_MAC);
            }
        }

        old_tst_config = data->tst_config;
        data->tst_config = *conf;
        rc = run_tst_config(instance, FALSE, active_time_ms);
        if (rc != VTSS_RC_OK) {
            /* config failed, so restore configuration */
            data->tst_config = old_tst_config;
            (void)run_tst_config(instance, FALSE, NULL);
        }
        return(rc);
    }
    /* No AFI support */
    return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
}

u32 vtss_mep_mgmt_tst_conf_get(const u32                 instance,
                               vtss_mep_mgmt_tst_conf_t  *const conf)
{
    mep_instance_data_t *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->tst_config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}

u32 vtss_mep_mgmt_client_conf_set(const u32                 instance,
                                  const mep_client_conf_t  *const conf)
{
    mep_instance_data_t *data;    /* Instance data reference */
    u32                 i;
    BOOL                has_evc = FALSE, has_vlan = FALSE;
    mep_model_client_t  client_conf;
    vtss_rc             rc = VTSS_RC_OK;

    T_DG(TRACE_GRP_AIS_LCK, "Enter  Instance %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);

    if (!data || !data->config.enable)                                                               return(VTSS_APPL_MEP_RC_NOT_ENABLED);

    if (data->config.mode == VTSS_APPL_MEP_MIP)                                                      return(VTSS_APPL_MEP_RC_MIP);
    if ((data->config.domain == VTSS_APPL_MEP_EVC) && (data->config.direction == VTSS_APPL_MEP_UP))  return(VTSS_APPL_MEP_RC_UP_CLIENT);
    if (conf->flow_count > VTSS_APPL_MEP_CLIENT_FLOWS_MAX)                                           return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    if ((conf->flow_count == 0) && (data->ais_config.enable || data->lck_config.enable))             return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    if (!MPLS_DOMAIN(data->config.domain) && conf->flow_count) {
        if ((data->config.domain != VTSS_APPL_MEP_PORT) && (conf->flow_count != 1))                  return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        if ((data->config.domain != VTSS_APPL_MEP_PORT) && (data->config.flow != conf->flows[0]))    return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        if ((data->config.domain != VTSS_APPL_MEP_PORT) && (data->config.level == 7))                return(VTSS_APPL_MEP_RC_CLIENT_MAX_LEVEL);
    }
    for (i=0; i<conf->flow_count; i++) {
        if (conf->level[i] > 7)                                                                      return(VTSS_APPL_MEP_RC_INVALID_CLIENT_LEVEL);
        if (!MPLS_DOMAIN(data->config.domain) && (data->config.domain != VTSS_APPL_MEP_PORT) &&
            conf->level[i] <= data->config.level)                                                    return(VTSS_APPL_MEP_RC_INVALID_CLIENT_LEVEL);
        if ((conf->ais_prio[i] != VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST) && (conf->ais_prio[i] > 7))     return(VTSS_APPL_MEP_RC_INVALID_PRIO);
        if ((conf->lck_prio[i] != VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST) && (conf->lck_prio[i] > 7))     return(VTSS_APPL_MEP_RC_INVALID_PRIO);


        if (conf->domain[i] == VTSS_APPL_MEP_MPLS_LSP)                                               return(VTSS_APPL_MEP_RC_INVALID_DOMAIN);

















        {
            if ((conf->ais_prio[i] != VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST) && (conf->ais_prio[i] > 7))      return(VTSS_APPL_MEP_RC_INVALID_COS_ID);
            if ((conf->lck_prio[i] != VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST) && (conf->lck_prio[i] > 7))      return(VTSS_APPL_MEP_RC_INVALID_COS_ID);
        }
        if (conf->domain[i] != VTSS_APPL_MEP_EVC &&
            conf->domain[i] != VTSS_APPL_MEP_VLAN &&
            conf->domain[i] != VTSS_APPL_MEP_MPLS_LSP)                                                    return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        if (conf->domain[i] == VTSS_APPL_MEP_EVC) {
            has_evc = TRUE;
        } else if (conf->domain[i] == VTSS_APPL_MEP_VLAN) {
            has_vlan = TRUE;
        }
        if (!MPLS_DOMAIN(data->config.domain) && has_evc && has_vlan)                                     return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        if (MPLS_DOMAIN(data->config.domain) &&
            conf->domain[i] == VTSS_APPL_MEP_VLAN)                                                        return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
        if (conf->domain[i] == VTSS_APPL_MEP_MPLS_LSP &&
            data->config.domain != VTSS_APPL_MEP_MPLS_TUNNEL &&
            data->config.domain != VTSS_APPL_MEP_MPLS_LINK)                                               return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    }

    (void)mep_model_client_flow_clear(instance);

    for (i = 0; i < conf->flow_count; i++) {
        client_conf.domain   = model_domain_calc(conf->domain[i], 0);
        client_conf.flow     = conf->flows[i];
        if (!mep_model_client_flow_add(instance, &client_conf)) {
            T_D("mep_model_client_flow_add inst=%u domain=%u flow=%u failed", instance, client_conf.domain, client_conf.flow);
            break;
        }
    }

    if (i < conf->flow_count) {
        // New client flow add failed, re-apply the existing config
        for (i = 0; i < conf->flow_count; i++) {
            client_conf.domain   = model_domain_calc(data->client_config.domain[i], 0);
            client_conf.flow     = data->client_config.flows[i];
            if (!mep_model_client_flow_add(instance, &client_conf)) {
                T_D("mep_model_client_flow_add inst=%u domain=%u flow=%u failed", instance, client_conf.domain, client_conf.flow);
            }
        }
        T_DG(TRACE_GRP_AIS_LCK, "Failed to apply client configuration");
        rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;
    } else {
        // Store the new config
        data->client_config = *conf;
    }

    run_ais_config(instance);
    run_lck_config(instance);

    T_DG(TRACE_GRP_AIS_LCK, "Exit");

    return rc;
}

u32 vtss_mep_mgmt_client_conf_get(const u32          instance,
                                  mep_client_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)   return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (data) {
        *conf = data->client_config;
    } else {
        return VTSS_APPL_MEP_RC_NOT_CREATED;
    }

    return(VTSS_RC_OK);
}

u32 vtss_mep_mgmt_state_get(const u32                instance,
                            vtss_mep_mgmt_state_t    *const state)
{
    mep_instance_data_t *data;

    T_N("Enter  instance %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)   return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)            return(VTSS_APPL_MEP_RC_NOT_ENABLED);

    *state = data->out_state.state;

    return(VTSS_RC_OK);
}

mesa_rc vtss_mep_mgmt_cc_state_get(const u32                 instance,
                                   vtss_appl_mep_cc_state_t  *const state)
{
    mep_instance_data_t *data;
    ccm_state_t         *ccm_state;

    T_N("Enter  instance %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)    return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)             return(VTSS_APPL_MEP_RC_NOT_ENABLED);

    memset(state, 0, sizeof(*state));

    ccm_state = &data->supp_state.ccm_state;
    memcpy(state->unexp_name, ccm_state->unexp_name, sizeof(state->unexp_name));
    memcpy(state->unexp_meg, ccm_state->unexp_meg, sizeof(state->unexp_meg));
    memcpy(state->ps_tlv, ccm_state->ps_tlv.data(), ccm_state->ps_tlv.mem_size());
    memcpy(state->is_tlv, ccm_state->is_tlv.data(), ccm_state->is_tlv.mem_size());
    memcpy(state->os_tlv, ccm_state->os_tlv.data(), ccm_state->os_tlv.mem_size());
    memcpy(state->ps_received, ccm_state->ps_received.data(), ccm_state->ps_received.mem_size());
    memcpy(state->is_received, ccm_state->is_received.data(), ccm_state->is_received.mem_size());
    memcpy(state->os_received, ccm_state->os_received.data(), ccm_state->os_received.mem_size());

    return(VTSS_RC_OK);
}

u32 vtss_mep_mgmt_lm_state_get(const u32                   instance,
                               const u32                   peer_id,
                               vtss_appl_mep_lm_state_t    *const state)
{
    u32 peer_index;
    mep_instance_data_t    *data;    /* Instance data reference */
    vtss_appl_mep_lm_state_t *curr_state;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)     return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)              return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (!get_peer_index(data, peer_id, peer_index)) return(VTSS_APPL_MEP_RC_PEER_ID);

    curr_state = &data->out_state.lm_state[peer_index];

    if (data->lm_config.synthetic) {
        get_sl_pdu_counters(data, peer_index, curr_state);
    } else {
        get_lm_pdu_counters(data, curr_state);
    }

    *state = *curr_state;
    if (state->near_los_tot_cnt < 0) state->near_los_tot_cnt = 0;
    if (state->far_los_tot_cnt  < 0) state->far_los_tot_cnt  = 0;

    return(VTSS_RC_OK);
}

u32 vtss_mep_mgmt_slm_counters_get(const u32            instance,
                                   const u32            peer_idx,
                                   sl_service_counter_t *const counters)
{
    mep_instance_data_t    *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)     return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (peer_idx >= MEP_MODEL_MEP_PEER_MAX)         return(VTSS_APPL_MEP_RC_PEER_CNT);
    if (counters == NULL)                           return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    data = &instance_data[instance];    /* Instance data reference */

    mep_model_sl_status_t sl_status;
    memset(&sl_status, 0, sizeof(sl_status));

    if (!mep_model_sl_status_get(instance, &sl_status)) {
        T_D("mep_model_sl_status_get failed");
    } else {
        counters->tx_slm_counter = sl_status.tx_slm;
        counters->rx_slm_counter = sl_status.rx_slm;
        counters->tx_slr_counter = sl_status.tx_slr[peer_idx];
        counters->rx_slr_counter = sl_status.rx_slr[peer_idx];
    }

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_lm_avail_state_get(const u32                        instance,
                                     const u32                        peer_id,
                                     vtss_appl_mep_lm_avail_state_t   *const state)
{
    u32 peer_index;
    mep_instance_data_t        *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)   return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)            return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (!get_peer_index(data, peer_id, peer_index))   return(VTSS_APPL_MEP_RC_PEER_ID);

    *state = data->out_state.lm_avail_state[peer_index];

    return(VTSS_RC_OK);
}

u32 vtss_mep_mgmt_lm_hli_state_get(const u32                      instance,
                                   const u32                      peer_id,
                                   vtss_appl_mep_lm_hli_state_t   *const state)
{
    u32 peer_index;
    mep_instance_data_t        *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)   return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)            return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (!get_peer_index(data, peer_id, peer_index))   return(VTSS_APPL_MEP_RC_PEER_ID);

    *state = data->out_state.lm_hli_state[peer_index];

    return(VTSS_RC_OK);
}

u32 vtss_mep_mgmt_dm_timestamp_get(const u32           instance,
                                   mep_dm_timestamp_t  *const dm1_timestamp_far_to_near,
                                   mep_dm_timestamp_t  *const dm1_timestamp_near_to_far)
{
    mep_instance_data_t    *data;    /* Instance data reference */

    memset(dm1_timestamp_far_to_near, 0, sizeof(*dm1_timestamp_far_to_near));
    memset(dm1_timestamp_near_to_far, 0, sizeof(*dm1_timestamp_near_to_far));

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                                      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)                                               return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if ((data->dm_config.common.synchronized != TRUE) || (data->dmr_ts_ok != TRUE))  return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    dm1_timestamp_near_to_far->tx_time = data->txTimef;
    dm1_timestamp_near_to_far->rx_time = data->rxTimef;
    dm1_timestamp_far_to_near->tx_time = data->txTimeb;
    dm1_timestamp_far_to_near->rx_time = data->rxTimeb;
    data->dmr_ts_ok = FALSE;

    return(VTSS_RC_OK);
}

u32 vtss_mep_mgmt_dm_prio_state_get(const u32                   instance,
                                    const u32                   prio,
                                    vtss_appl_mep_dm_state_t    *const tw_state,
                                    vtss_appl_mep_dm_state_t    *const fn_state,
                                    vtss_appl_mep_dm_state_t    *const nf_state)
{
    mep_model_dm_status_t  dm_status;
    mep_instance_data_t    *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)   return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)            return(VTSS_APPL_MEP_RC_NOT_ENABLED);

    T_D("Enter  instance %u", instance);

    memcpy(tw_state, &data->out_state.tw_state[prio], sizeof(*tw_state));
    if (tw_state->min_delay == 0xffffffff)
        tw_state->min_delay = 0;
    if (tw_state->min_delay_var == 0xffffffff)
        tw_state->min_delay_var = 0;

    memcpy(fn_state, &data->out_state.fn_state[prio], sizeof(*fn_state));
    fn_state->tx_cnt = 0;
    if (fn_state->min_delay == 0xffffffff)
        fn_state->min_delay = 0;
    if (fn_state->min_delay_var == 0xffffffff)
        fn_state->min_delay_var = 0;

    memcpy(nf_state, &data->out_state.nf_state[prio], sizeof(*nf_state));
    nf_state->rx_cnt = 0;
    nf_state->rx_tout_cnt = 0;
    if (nf_state->min_delay == 0xffffffff)
        nf_state->min_delay = 0;
    if (nf_state->min_delay_var == 0xffffffff)
        nf_state->min_delay_var = 0;

    memset(&dm_status, 0, sizeof(dm_status));
    if (!mep_model_dm_status_get(instance, prio, &dm_status)) {
        T_D("mep_model_dm_status_get failed");
    }

    if (data->dm_config.common.ended == VTSS_APPL_MEP_SINGEL_ENDED) {
        tw_state->tx_cnt = dm_status.tx_dm;
        tw_state->rx_cnt = dm_status.rx_dm;
    } else {
        nf_state->tx_cnt = dm_status.tx_dm;
        fn_state->rx_cnt = dm_status.rx_dm;
    }

    return(VTSS_RC_OK);
}

u32 vtss_mep_mgmt_dm_state_clear_set(const u32    instance)
{
    mep_instance_data_t        *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)    return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)             return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (data->config.mode == VTSS_APPL_MEP_MIP)    return(VTSS_APPL_MEP_RC_MIP);

    run_dm_clear(instance);

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_lt_state_get(const u32                  instance,
                               vtss_appl_mep_lt_state_t   *const state)
{
    mep_instance_data_t        *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)    return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)             return(VTSS_APPL_MEP_RC_NOT_ENABLED);

    *state = data->out_state.lt_state;

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_lb_prio_state_get(const u32                  instance,
                                    const u32                  prio,
                                    vtss_appl_mep_lb_state_t   *const state)
{
    mep_instance_data_t   *data;    /* Instance data reference */
    mep_model_lb_status_t status;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)     return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)              return(VTSS_APPL_MEP_RC_NOT_ENABLED);

    if (!mep_model_lb_status_get(instance, prio, &status)) return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data->out_state.lb_state[prio].transaction_id        = status.trans_id;
    data->out_state.lb_state[prio].lbm_transmitted       = status.lbm_counter;

    if (data->lb_config.common.to_send == VTSS_APPL_MEP_LB_TO_SEND_INFINITE) {
        /* No replies are received by SW - simulate one in order to print reply info: */
        data->out_state.lb_state[prio].reply_cnt = 1;
        memset(data->out_state.lb_state[prio].reply[0].mac.addr, 0, sizeof(data->out_state.lb_state[prio].reply[0].mac.addr));
        data->out_state.lb_state[prio].reply[0].lbr_received = status.lbr_counter;
        data->out_state.lb_state[prio].reply[0].out_of_order = status.oo_counter;
    }

    *state = data->out_state.lb_state[prio];

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_lb_state_clear_set(const u32    instance)
{
    mep_instance_data_t        *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)                   return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (data->config.mode == VTSS_APPL_MEP_MIP)          return(VTSS_APPL_MEP_RC_MIP);

    run_lb_clear(instance);

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_lm_state_clear_set(const u32    instance,
                                     vtss_appl_mep_clear_dir direction)
{
    mep_instance_data_t        *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)                   return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (data->config.mode == VTSS_APPL_MEP_MIP)          return(VTSS_APPL_MEP_RC_MIP);

    run_lm_clear(instance, direction);

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_tst_prio_state_get(const u32                   instance,
                                     const u32                   prio,
                                     vtss_appl_mep_tst_state_t   *const state)
{
    mep_instance_data_t    *data;    /* Instance data reference */
    mep_model_tst_status_t tst_status;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return VTSS_APPL_MEP_RC_INVALID_INSTANCE;
    data = get_instance(instance);

    if (!data || !data->config.enable)                   return VTSS_APPL_MEP_RC_NOT_ENABLED;

    T_DG(TRACE_GRP_TST, "Enter  Instance %u  prio %u  enable_rx %u", instance, prio, data->tst_config.common.enable_rx);

    memset(state, 0, sizeof(*state));
    if (!mep_model_tst_status_get(instance, prio, &tst_status)) {
        T_DG(TRACE_GRP_TST, "mep_model_tst_status_get(%u, %u) failed", instance, prio);
    }
    T_DG(TRACE_GRP_TST, "tx_counter " VPRI64u "  rx_counter " VPRI64u "  oo_counter " VPRI64u "", tst_status.tx_counter, tst_status.rx_counter, tst_status.oo_counter);

    state->tx_counter = tst_status.tx_counter;
    state->rx_counter = tst_status.rx_counter;
    state->oo_counter = tst_status.oo_counter;

    /* TBD - for now only one rate for a MEP: */
    if (data->out_state.tst_state[prio].rx_rate) {
        state->rx_rate = data->out_state.tst_state[prio].rx_rate;
    }
    if (data->out_state.tst_state[prio].time) {
        state->time    = data->out_state.tst_state[prio].time;
    }

    T_DG(TRACE_GRP_TST, "Exit");

    return(VTSS_RC_OK);
}


u32 vtss_mep_mgmt_tst_state_clear_set(const u32    instance)
{
    mep_instance_data_t        *data;    /* Instance data reference */

    T_N("Enter %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)                   return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (data->config.mode == VTSS_APPL_MEP_MIP)          return(VTSS_APPL_MEP_RC_MIP);

    run_tst_clear(instance);

    return(VTSS_RC_OK);
}

void  vtss_mep_mgmt_default_conf_get(mep_default_conf_t  *const def_conf)
{
    char meg[] = "ICC000MEG0000";

    memset(def_conf, 0, sizeof(*def_conf));

    def_conf->config.enable = FALSE;
    def_conf->config.mode = VTSS_APPL_MEP_MEP;
    def_conf->config.direction = VTSS_APPL_MEP_DOWN;
    def_conf->config.domain = VTSS_APPL_MEP_PORT;
    def_conf->config.flow = 0;
    def_conf->config.port = 0;
    def_conf->config.level = 0;
    def_conf->config.vid = 0;
    def_conf->config.voe = TRUE;
    def_conf->config.format = VTSS_APPL_MEP_ITU_ICC;
    memset(def_conf->config.name, 0, sizeof(def_conf->config.name));
    memset(def_conf->config.meg, 0, sizeof(def_conf->config.meg));
    memcpy(def_conf->config.meg, meg, sizeof(meg));
    def_conf->config.mep = 1;
    def_conf->config.peer_count =0 ;
    memset(def_conf->config.peer_mep, 0, sizeof(def_conf->config.peer_mep));
    memset(def_conf->config.peer_mac, 0, sizeof(def_conf->config.peer_mac));
    def_conf->config.out_of_service = FALSE;
    def_conf->config.evc_pag = 0;
    def_conf->config.evc_qos = 0;
    def_conf->config.ll_is_used = FALSE;

    memset(&def_conf->peer_conf.mac, 0, sizeof(def_conf->peer_conf.mac));

    def_conf->syslog_conf.enable = FALSE;

    def_conf->pm_conf.enable = FALSE;

    def_conf->lst_conf.enable = FALSE;

    def_conf->tlv_conf.os_tlv.oui[0] = 0x00;
    def_conf->tlv_conf.os_tlv.oui[1] = 0x00;
    def_conf->tlv_conf.os_tlv.oui[2] = 0x0C;
    def_conf->tlv_conf.os_tlv.subtype = 0x01;
    def_conf->tlv_conf.os_tlv.value = 0x02;

    def_conf->cc_conf.enable = FALSE;
    def_conf->cc_conf.prio = 0;
    def_conf->cc_conf.rate = VTSS_APPL_MEP_RATE_1S;
    def_conf->cc_conf.tlv_enable = FALSE;

    def_conf->lm_conf.enable = FALSE;
    def_conf->lm_conf.enable_rx = FALSE;
    def_conf->lm_conf.synthetic = FALSE;
    def_conf->lm_conf.prio = 0;
    def_conf->lm_conf.dei = FALSE;
    def_conf->lm_conf.rate = VTSS_APPL_MEP_RATE_1S;
    def_conf->lm_conf.cast = VTSS_APPL_MEP_MULTICAST;
    def_conf->lm_conf.ended = VTSS_APPL_MEP_SINGEL_ENDED;
    def_conf->lm_conf.size = 64;
    def_conf->lm_conf.mep = 1;
    def_conf->lm_conf.flr_interval = 5;
    def_conf->lm_conf.meas_interval = 1000; /* One second */
    def_conf->lm_conf.flow_count = FALSE;
    def_conf->lm_conf.oam_count = VTSS_APPL_MEP_OAM_COUNT_Y1731;
    def_conf->lm_conf.loss_th = 1;

    def_conf->lm_avail_conf.enable   = FALSE;
    def_conf->lm_avail_conf.flr_th   = 10; /* <1% for AVAIL */
    def_conf->lm_avail_conf.interval = 10; /* measurements */
    def_conf->lm_avail_conf.main     = FALSE;

    def_conf->lm_hli_conf.enable   = FALSE;
    def_conf->lm_hli_conf.flr_th   = 100; /* >10% for High Loss */
    def_conf->lm_hli_conf.con_int  = 100; /* measurements */

    def_conf->lm_sdeg_conf.enable   = FALSE;
    def_conf->lm_sdeg_conf.tx_min   = 0;
    def_conf->lm_sdeg_conf.flr_th   = 10; /* >1% to get SDEG */
    def_conf->lm_sdeg_conf.bad_th   = 10; /* number of lm_conf.flr_interval */
    def_conf->lm_sdeg_conf.good_th  = 10; /* number of lm_conf.flr_interval */

    def_conf->dm_conf.enable = FALSE;;
    def_conf->dm_conf.prio = 0;
    def_conf->dm_conf.cast = VTSS_APPL_MEP_MULTICAST;
    def_conf->dm_conf.mep = 1;
    def_conf->dm_conf.ended = VTSS_APPL_MEP_SINGEL_ENDED;
    def_conf->dm_conf.proprietary = FALSE;
    def_conf->dm_conf.calcway = VTSS_APPL_MEP_FLOW;
    def_conf->dm_conf.interval = 10;
    def_conf->dm_conf.lastn = 10;
    def_conf->dm_conf.tunit = VTSS_APPL_MEP_US;
    def_conf->dm_conf.overflow_act = VTSS_APPL_MEP_DISABLE;
    def_conf->dm_conf.synchronized = FALSE;;
    def_conf->dm_conf.num_of_bin_fd = 3;
    def_conf->dm_conf.num_of_bin_ifdv = 3;
    def_conf->dm_conf.m_threshold = 5000;


    def_conf->aps_conf.enable = FALSE;
    def_conf->aps_conf.prio = 0;
    def_conf->aps_conf.type = VTSS_APPL_MEP_L_APS;
    def_conf->aps_conf.cast = VTSS_APPL_MEP_MULTICAST;
    def_conf->aps_conf.raps_octet = 1;

    def_conf->lt_conf.enable = FALSE;
    def_conf->lt_conf.prio = 0;
    def_conf->lt_conf.mep = 1;
    memset(def_conf->lt_conf.mac.addr, 0, sizeof(def_conf->lt_conf.mac.addr));
    def_conf->lt_conf.ttl = 1;
    def_conf->lt_conf.transactions = 1;

    def_conf->lb_conf.enable = FALSE;
    def_conf->lb_conf.dei = FALSE;
    def_conf->lb_conf.prio = 0;
    def_conf->lb_conf.cast = VTSS_APPL_MEP_MULTICAST;
    def_conf->lb_conf.mep = 1;
    memset(def_conf->lb_conf.mac.addr, 0, sizeof(def_conf->lb_conf.mac.addr));
    def_conf->lb_conf.to_send = 10;
    def_conf->lb_conf.size = 64;
    def_conf->lb_conf.interval = 100;
    def_conf->lb_conf.ttl = 0;

    def_conf->ais_conf.enable = FALSE;
    def_conf->ais_conf.protection = FALSE;
    def_conf->ais_conf.rate = VTSS_APPL_MEP_RATE_1S;

    def_conf->lck_conf.enable = FALSE;
    def_conf->lck_conf.rate = VTSS_APPL_MEP_RATE_1S;

    def_conf->tst_conf.enable = FALSE;
    def_conf->tst_conf.enable_rx = FALSE;
    def_conf->tst_conf.prio = 0;
    def_conf->tst_conf.dei = FALSE;
    def_conf->tst_conf.mep = 1;
    def_conf->tst_conf.rate = 1000;
    def_conf->tst_conf.size = 64;
    def_conf->tst_conf.pattern = VTSS_APPL_MEP_PATTERN_ALL_ZERO;
    def_conf->tst_conf.sequence = FALSE;

    def_conf->test_prio_conf.enable = FALSE;
    def_conf->test_prio_conf.dp = VTSS_APPL_MEP_DP_GREEN;
    def_conf->test_prio_conf.rate = 1000;
    def_conf->test_prio_conf.size = 64;
    def_conf->test_prio_conf.pattern = VTSS_APPL_MEP_PATTERN_ALL_ZERO;

    def_conf->lb_common_conf.mep = 1;
    memset(def_conf->lb_common_conf.mac.addr, 0, sizeof(def_conf->lb_common_conf.mac.addr));
    def_conf->lb_common_conf.cast = VTSS_APPL_MEP_MULTICAST;
    def_conf->lb_common_conf.sequence = FALSE;
    def_conf->lb_common_conf.to_send = 10;
    def_conf->lb_common_conf.interval = 100;

    def_conf->tst_common_conf.mep = 1;
    def_conf->tst_common_conf.sequence = FALSE;
    def_conf->tst_common_conf.enable_rx = FALSE;

    def_conf->dm_prio_conf.enable = FALSE;

    def_conf->dm_common_conf.cast = VTSS_APPL_MEP_MULTICAST;
    def_conf->dm_common_conf.mep = 1;
    def_conf->dm_common_conf.ended = VTSS_APPL_MEP_SINGEL_ENDED;
    def_conf->dm_common_conf.proprietary = FALSE;
    def_conf->dm_common_conf.calcway = VTSS_APPL_MEP_FLOW;
    def_conf->dm_common_conf.interval = 10;
    def_conf->dm_common_conf.lastn = 10;
    def_conf->dm_common_conf.tunit = VTSS_APPL_MEP_US;
    def_conf->dm_common_conf.overflow_act = VTSS_APPL_MEP_DISABLE;
    def_conf->dm_common_conf.synchronized = FALSE;;
    def_conf->dm_common_conf.num_of_bin_fd = 3;
    def_conf->dm_common_conf.num_of_bin_ifdv = 3;
    def_conf->dm_common_conf.m_threshold = 5000;

    def_conf->client_flow_conf.ais_prio = VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST;
    def_conf->client_flow_conf.lck_prio = VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST;
    def_conf->client_flow_conf.level = 0;

    def_conf->bfd_conf.enable          = FALSE;
    def_conf->bfd_conf.is_independent  = FALSE;
    def_conf->bfd_conf.rx_flow         = 0;
    def_conf->bfd_conf.cc_only         = FALSE;
    def_conf->bfd_conf.cc_period       = 1000000; /* 1 sec */
    def_conf->bfd_conf.tx_auth_enabled = FALSE;
    def_conf->bfd_conf.rx_auth_enabled = FALSE;
    def_conf->bfd_conf.tx_auth_key_id  = VTSS_APPL_MEP_BFD_AUTH_DISABLED;

    def_conf->rt_conf.enable = FALSE;
    def_conf->rt_conf.tc     = 0;
}

/****************************************************************************/
/*  MEP module interface                                                    */
/****************************************************************************/

u32 vtss_mep_tx_aps_info_set(const u32    instance,
                             const u8     *aps,
                             const BOOL   event)
{
    mep_instance_data_t   *data;    /* Instance data reference */
    u8                    *pdu;
    u32                   opcode;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                         return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    data = get_instance(instance);
    if (!data || !data->config.enable)                                  return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (data->config.mode == VTSS_APPL_MEP_MIP)                         return(VTSS_APPL_MEP_RC_MIP);
    if (event && (data->aps_config.type != VTSS_APPL_MEP_R_APS))        return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    if (!event) {
        mep_model_tx_cancel(instance, &data->aps_tx_id, NULL);
    }

    if (mep_timer_active(data->tx_aps_timer)) {
        if (!memcmp(&data->tx_aps[4], aps, APS_DATA_LENGTH)) {
            T_NG(TRACE_GRP_APS_RX, "Unchanged APS");
            return(VTSS_RC_OK);
        }
    }

    pdu = data->tx_aps;
    if (data->aps_config.enable)  {
        opcode = (data->aps_config.type == VTSS_APPL_MEP_L_APS) ? MESA_OAM_OPCODE_LINEAR_APS : MESA_OAM_OPCODE_RING_APS;
        pdu[0] = data->config.level << 5;
        pdu[0] |= (data->aps_config.type == VTSS_APPL_MEP_L_APS) ? 0x00 : 0x01;     /* This is new version number for Version 2 of ERPS */
        pdu[1] = opcode;
        pdu[3] = (data->aps_config.type == VTSS_APPL_MEP_L_APS) ? 4 : 32;
        memcpy(&pdu[4], aps, APS_DATA_LENGTH);
        data->aps_event = event;
        data->aps_count = 3;
        if ((data->aps_config.type != VTSS_APPL_MEP_R_APS) || data->raps_transmit) {
            /* If not currently enabled then  vtss_mep_raps_transmission will start the transmission later */
            run_tx_aps_timer(instance, data->tx_aps_timer);
        }
    }
    return(VTSS_RC_OK);
}


u32 vtss_mep_signal_in(const u32   instance)
{
    mep_instance_data_t        *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    data = get_instance(instance);
    if (!data || !data->config.enable)               return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (data->config.mode == VTSS_APPL_MEP_MIP)      return(VTSS_APPL_MEP_RC_MIP);

    if (data->aps_config.enable) {
        vtss_mep_rx_aps_info_set(instance, data->config.domain, data->out_state.rx_aps);
    }

    /* Update the caller with SF state */
    vtss_mep_sf_sd_state_set(instance, data->config.domain, data->out_state.state.mep.aTsf, data->out_state.state.mep.aTsd);

    return(VTSS_RC_OK);
}





u32 vtss_mep_raps_forwarding(const u32    instance,
                             const BOOL   enable)
{
    mep_instance_data_t        *data;  /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                         return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    data = get_instance(instance);
    if (!data || !data->config.enable)                                  return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    if (data->out_state.state.mep.oper_state != VTSS_APPL_MEP_OPER_UP)  return(VTSS_APPL_MEP_RC_STATE_NOT_UP);
    if (data->config.mode == VTSS_APPL_MEP_MIP)                         return(VTSS_APPL_MEP_RC_MIP);
    if (data->config.domain == VTSS_APPL_MEP_EVC)                       return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    if ((data->config.domain == VTSS_APPL_MEP_PORT) &&
        (data->config.vid == 0))                                        return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    T_D("Enter  Instance %d  enable %u", instance, enable);

    data->raps_forward = enable;
    return apply_aps_config(instance);
}


u32 vtss_mep_raps_transmission(const u32    instance,
                               const BOOL   enable)
{
    mep_instance_data_t *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX) {
        return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    }

    data = get_instance(instance);
    if (!data || !data->config.enable) {
        return(VTSS_APPL_MEP_RC_NOT_ENABLED);
    }
    if (data->config.mode == VTSS_APPL_MEP_MIP) {
        return(VTSS_APPL_MEP_RC_MIP);
    }

    data->raps_transmit = enable;

    if (mep_timer_active(data->tx_aps_timer) && enable) {
        return(VTSS_RC_OK);
    }

    if (!enable) {
        /* cancel running transmission */
        mep_model_tx_cancel(instance, &data->aps_tx_id, NULL);
        mep_timer_cancel(data->tx_aps_timer);
    } else {
        /* start/restart timer transmission */
        run_tx_aps_timer(instance, data->tx_aps_timer);
    }

    return(VTSS_RC_OK);
}


const char* vtss_mep_rate_to_string(vtss_appl_mep_rate_t rate)
{
    switch (rate)
    {
        case VTSS_APPL_MEP_RATE_300S: return("300s");
        case VTSS_APPL_MEP_RATE_100S: return("100s");
        case VTSS_APPL_MEP_RATE_10S: return("10s");
        case VTSS_APPL_MEP_RATE_1S: return("1s");
        case VTSS_APPL_MEP_RATE_6M: return("6m");
        case VTSS_APPL_MEP_RATE_1M: return("1m");
        case VTSS_APPL_MEP_RATE_6H: return("6h");
        default: return("invalid");
    }
}

const char* vtss_mep_flow_to_name(mep_domain_t domain, u32 flow, vtss_uport_no_t port_no, char *str)
{
    switch (domain)
    {
















        case VTSS_APPL_MEP_PORT:
            icli_port_info_txt(VTSS_USID_START, port_no, str);
            break;

        case VTSS_APPL_MEP_VLAN:
            if (vtss_appl_vlan_name_get(flow, str, NULL) != VTSS_RC_OK)
            {
                sprintf(str, "VLAN%u", flow);
            }
            break;

        default:
            sprintf(str, "invalid");
            break;
    }
    return str;
}

const char* vtss_mep_domain_to_string(mep_domain_t domain)
{
    switch (domain)
    {
        case VTSS_APPL_MEP_PORT: return("Port");
        case VTSS_APPL_MEP_EVC: return("EVC");
        case VTSS_APPL_MEP_VLAN: return("VLAN");
        default: return("invalid");
    }
}

const char* vtss_mep_direction_to_string(vtss_appl_mep_direction_t direction)
{
    switch (direction)
    {
        case VTSS_APPL_MEP_DOWN: return("Down");
        case VTSS_APPL_MEP_UP: return("Up");
        default: return("invalid");
    }
}

const char* vtss_mep_unit_to_string(vtss_appl_mep_dm_tunit_t unit)
{
    switch (unit)
    {
        case VTSS_APPL_MEP_US: return("us");
        case VTSS_APPL_MEP_NS: return("ns");
        default: return("invalid");
    }
}


static void handle_1dm(u32 instance,  u32 prio,  mesa_timestamp_t *TXf,  mesa_timestamp_t *RXf)
{
    mep_instance_data_t  *data;
    i32                  dm_delay;
    BOOL                 is_ns;

    T_DG(TRACE_GRP_DM, "Far TX s %u Far TX ns %u Near RX s %u Near RX ns %u", TXf->seconds, TXf->nanoseconds, RXf->seconds, RXf->nanoseconds);

    data = get_instance(instance);
    if (!data) { return; }

    is_ns = (data->dm_config.common.tunit == VTSS_APPL_MEP_NS) ? TRUE : FALSE;
    dm_delay = vtss_mep_delay_calc(RXf, TXf, is_ns);

    if ((dm_delay >= 0) && (dm_delay < (is_ns ? 1e9 : 1e6))) {   /* Check for valid delay */
        delay_data_calc(instance, &data->supp_state.fn_state[prio], &data->out_state.fn_state[prio], dm_delay);
        data->out_state.fn_state[prio].tx_cnt = 0;
        data->out_state.nf_state[prio].rx_cnt = 0;
    } else {
        T_DG(TRACE_GRP_DM, "1DM Invalid delay calculated %d", dm_delay);
        data->out_state.fn_state[prio].rx_err_cnt++;
    }
}

void handle_dmr(u32 instance,  u32 prio,  mesa_timestamp_t *TXf,  mesa_timestamp_t *RXf,  mesa_timestamp_t *TXb,  mesa_timestamp_t *RXb)
{
    mep_instance_data_t  *data;
    i32                  total_delay, residence_time, dm_delay;
    BOOL                 is_ns;

    T_DG(TRACE_GRP_DM, "Near TX s %u Near TX ns %u Far RX s %u Far RX ns %u Far TX s %u Far TX ns %u Near RX s %u Near RX ns %u", TXf->seconds, TXf->nanoseconds, RXf->seconds, RXf->nanoseconds, TXb->seconds, TXb->nanoseconds, RXb->seconds, RXb->nanoseconds);

    data = get_instance(instance);
    if (!data) { return; }

    is_ns = (data->dm_config.common.tunit == VTSS_APPL_MEP_NS) ? TRUE : FALSE;
    total_delay = vtss_mep_delay_calc(RXb,  TXf,  is_ns);
    residence_time = 0;
    if (data->dm_config.common.calcway == VTSS_APPL_MEP_FLOW) {
        residence_time = vtss_mep_delay_calc(TXb, RXf, is_ns);
    }
    T_DG(TRACE_GRP_DM, "is_ns %u  total_delay %d  residence_time %d", is_ns, total_delay, residence_time);

    if ((total_delay >= 0) && (residence_time >= 0)) {
        dm_delay = total_delay - residence_time;

        if ((dm_delay >= 0) && (dm_delay < (is_ns ? 1e9 : 1e6))) {   /* If dm_delay <= 0, it means something wrong. Just skip it. */
            delay_data_calc(instance, &data->supp_state.tw_state[prio], &data->out_state.tw_state[prio], dm_delay);
        } else {
            T_DG(TRACE_GRP_DM, "DMR Invalid delay calculated  total_delay %d residence_time %d", total_delay, residence_time);
            data->out_state.tw_state[prio].rx_err_cnt++;
        }
    } else {
        if (total_delay < 0) {
            T_DG(TRACE_GRP_DM, "DMR Invalid delay calculated  total_delay %d", total_delay);
        }
        if (residence_time < 0) {
            T_DG(TRACE_GRP_DM, "DMR Invalid delay calculated  residence_time %d", residence_time);
        }
        data->out_state.tw_state[prio].rx_err_cnt++;
    }

    if (data->dm_config.common.synchronized == TRUE) {
        /* One-way Standard (far-to-near)*/
        dm_delay = vtss_mep_delay_calc(RXb, TXb, is_ns);

        if ((dm_delay >= 0) && (dm_delay < (is_ns ? 1e9 : 1e6))) {   /* Check for valid delay */
            delay_data_calc(instance, &data->supp_state.fn_state[prio], &data->out_state.fn_state[prio], dm_delay);
        } else {  /* If dm_delay <= 0 or above 1 sec, it means something wrong. Just skip it. */
            T_DG(TRACE_GRP_DM, "DMR Invalid FN delay calculated  dm_delay %d", dm_delay);
            data->out_state.fn_state[prio].rx_err_cnt++;
        }

        /* One-way Standard (near-to-far)*/
        dm_delay = vtss_mep_delay_calc(RXf, TXf, is_ns);

        if ((dm_delay >= 0) && (dm_delay < (is_ns ? 1e9 : 1e6))) {   /* Check for valid delay */
            delay_data_calc(instance, &data->supp_state.nf_state[prio], &data->out_state.nf_state[prio], dm_delay);
        } else {
            T_DG(TRACE_GRP_DM, "DMR Invalid NF delay calculated  dm_delay %d", dm_delay);
            data->out_state.nf_state[prio].rx_err_cnt++;
        }
    }
}

static void lm_counter_calc(u32 instance,  u32 peer_idx,  u32 near_CT2,  u32 far_CT2,  u32 near_CR2,  u32 far_CR2,  BOOL is_valid = TRUE)
{
    mep_instance_data_t  *data;
    lm_counters_t        lm;
    out_state_t          *out_state;

    data = get_instance(instance);
    if (!data) { return; }

    T_DG(TRACE_GRP_LM, "instance %u  near_TX %u  far_RX %u  far_TX %u  near_rx %u", instance, near_CT2, far_CR2, far_CT2, near_CR2);

    if (!data->lm_peer_state[peer_idx].lm_start) { /* The first measurement is only used to initialize the 'last' frame counters */
        /* See Appendix III in Y.1731  */
        lm.farTxC  = (far_CT2  - data->lm_peer_state[peer_idx].far_CT1);
        lm.nearTxC = (near_CT2 - data->lm_peer_state[peer_idx].near_CT1);
        lm.nearRxC = (near_CR2 - data->lm_peer_state[peer_idx].near_CR1);
        lm.farRxC  = (far_CR2  - data->lm_peer_state[peer_idx].far_CR1);

        /* pass new measurement counters: */
        lm_measurement(data, peer_idx, &lm);

    } else {  // first time
        out_state = &data->out_state;
        out_state->lm_avail_state[peer_idx].near_state = VTSS_APPL_MEP_OAM_LM_AVAIL;
        out_state->lm_avail_state[peer_idx].far_state  = VTSS_APPL_MEP_OAM_LM_AVAIL;
        out_state->lm_avail_state[peer_idx].near_avail_cnt = 0;
        out_state->lm_avail_state[peer_idx].far_avail_cnt  = 0;

        if (is_valid) {
            // We need one valid counter sample before we exit the "first time" state
            data->lm_peer_state[peer_idx].lm_start = FALSE;
        }
    }

    // save last counters
    data->lm_peer_state[peer_idx].near_CT1 = near_CT2;
    data->lm_peer_state[peer_idx].near_CR1 = near_CR2;
    data->lm_peer_state[peer_idx].far_CT1 = far_CT2;
    data->lm_peer_state[peer_idx].far_CR1 = far_CR2;

    return;
}


/****************************************************************************/
/*  MEP platform interface                                                  */
/****************************************************************************/
static void new_defect_state(mep_instance_data_t  *data)
{
    T_D("Enter  Instance %u", data->instance);

    syslog_new_defect_state(data);   /* Calculate entries in sys_log based on changes in defect state */

    if (data->supp_state.old_defect_state.ciSsf != data->supp_state.defect_state.ciSsf) {    /* Check for new CI-SSF */
        is_tlv_change(data);
    }

    data->supp_state.old_defect_state = data->supp_state.defect_state;

    fault_cause_calc(data);     /* Calculate new fault cause state */

    consequent_action_calc_and_deliver(data);       /* Calculate aTsf (SF) and aTsd (SD) and deliver to EPS */

    T_D("Exit %u", data->instance);
}

static void new_ccm_state(mep_instance_data_t  *data)
{
    T_D("Enter %u", data->instance);
    /* Nothing to do here yet */
}

static void run_defect_timer(u32 instance, struct vtss::Timer *timer)
{
    mep_instance_data_t  *data;
    BOOL                 new_;
    u32                  i;
    defect_state_t       *state;

    T_NG(TRACE_GRP_TIMER, "Enter  Instance %u", instance);

    data = get_instance(instance);
    if (!data) { return; }

    new_  = FALSE;
    state = &data->supp_state.defect_state;
    if (timer == data->dLevel.timer && !mep_timer_active(data->dLevel.timer)) {
        if (state->dLevel) {
            T_DG(TRACE_GRP_TIMER, "Instance %u  dLevel = FALSE", instance);
            data->dLevel.old_period = 0;
            state->dLevel = FALSE;
            new_ = TRUE;
        }
    }
    if (timer == data->dMeg.timer && !mep_timer_active(data->dMeg.timer)) {
        if (state->dMeg) {
            T_DG(TRACE_GRP_TIMER, "Instance %u  dMeg = FALSE", instance);
            data->dMeg.old_period = 0;
            state->dMeg = FALSE;
            new_ = TRUE;
        }
    }
    if (timer == data->dMep.timer && !mep_timer_active(data->dMep.timer)) {
        if (state->dMep) {
            T_DG(TRACE_GRP_TIMER, "Instance %u  dMep = FALSE", instance);
            data->dMep.old_period = 0;
            state->dMep = FALSE;
            new_ = TRUE;
        }
    }

    for (i=0; i<data->config.peer_count; ++i) {
        if (timer == data->dPeriod[i].timer && !mep_timer_active(data->dPeriod[i].timer)) {
            if (state->dPeriod[i]) {
                T_DG(TRACE_GRP_TIMER, "Instance %u  dPeriod[%u]  = FALSE", instance, i);
                data->dPeriod[i].old_period = 0;
                state->dPeriod[i] = FALSE;
                new_ = TRUE;
            }
        }

        if (timer == data->dPrio[i].timer && !mep_timer_active(data->dPrio[i].timer)) {
            if (state->dPrio[i]) {
                T_DG(TRACE_GRP_TIMER, "Instance %u  dPrio[%u]  = FALSE", instance, i);
                data->dPrio[i].old_period = 0;
                state->dPrio[i] = FALSE;
                new_ = TRUE;
            }
        }
    }
    if (timer == data->dLoop.timer && !mep_timer_active(data->dLoop.timer)) {
        if (state->dLoop) {
            T_DG(TRACE_GRP_TIMER, "Instance %u  dLoop = FALSE", instance);
            data->dLoop.old_period = 0;
            state->dLoop = FALSE;
            new_ = TRUE;
        }
    }
    if (timer == data->dConfig.timer && !mep_timer_active(data->dConfig.timer)) {
        if (state->dConfig) {
            T_DG(TRACE_GRP_TIMER, "Instance %u  dConfig = FALSE", instance);
            data->dConfig.old_period = 0;
            state->dConfig = FALSE;
            new_ = TRUE;
        }
    }
    if (timer == data->dAis.timer && !mep_timer_active(data->dAis.timer)) {
        if (state->dAis) {
            T_DG(TRACE_GRP_TIMER, "Instance %u  dAis = FALSE", instance);
            data->dAis.old_period = 0;
            state->dAis = FALSE;
            new_ = TRUE;
        }
    }
    if (timer == data->dLck.timer && !mep_timer_active(data->dLck.timer)) {
        if (state->dLck) {
            T_DG(TRACE_GRP_TIMER, "Instance %u  dLck = FALSE", instance);
            data->dLck.old_period = 0;
            state->dLck = FALSE;
            new_ = TRUE;
        }
    }

    if (new_) {
        T_DG(TRACE_GRP_TIMER, "instance %u  New Defect", instance);
        new_defect_state(data);
    }
}

static void run_rx_ccm_timer(u32 instance, struct vtss::Timer *timer)
{
    mep_instance_data_t  *data;
    defect_state_t       *state;
    BOOL                 new_=FALSE;
    u32                  i;

    data = get_instance(instance);
    if (!data) { return; }

    for (i=0; i<data->config.peer_count; ++i) {
        if (data->rx_ccm_timer[i] == timer) {
            break;
        }
    }
    if (i>=data->config.peer_count) { return; }

    state = &data->supp_state.defect_state;

    if (!data->voe_ccm_enabled && !mep_timer_active(data->rx_ccm_timer[i])) {    /* Only do RX CCM timeout action if not VOE based CCM */
        T_DG(TRACE_GRP_TIMER, "Instance %u  i %u  peer %u  dLoc = TRUE", instance, i, data->config.peer_mep[i]);
        state->dLoc[i] = TRUE;
        new_ = TRUE;
    }

    if (new_) {
        T_DG(TRACE_GRP_TIMER, "new LOC defect");
        new_defect_state(data);
    }
}

static void delete_timer(vtss::Timer **timer)
{
    if (!*timer) { return; }
    mep_timer_cancel(*timer);
    timer_info_t *t_info = (timer_info_t*)(*timer)->user_data;
    timer_obj_t  *t_obj = (timer_obj_t*)t_info->t_obj;

    delete t_obj;
    *timer = NULL;
}

static const char *timer_to_string(const mep_instance_data_t *data, const vtss::Timer *timer)
{
    if (data->lm_state.timer       == timer) return "lm_state";
    if (data->lt_state.timer       == timer) return "lt_state";
    if (data->lb_state.timer       == timer) return "lb_timer";
    if (data->tst_state.timer      == timer) return "tst_state";
    if (data->lst_state.ign_timer  == timer) return "lst_state.ign_timer";
    if (data->lst_state.sf_timer   == timer) return "lst_state.sf_timer";
    if (data->syslog_state.timer   == timer) return "syslog";
    if (data->dLevel.timer         == timer) return "dLevel";
    if (data->dMeg.timer           == timer) return "dMeg";
    if (data->dMep.timer           == timer) return "dMep";
    if (data->dAis.timer           == timer) return "dAis";
    if (data->dLck.timer           == timer) return "dLck";
    if (data->dLoop.timer          == timer) return "dLoop";
    if (data->dConfig.timer        == timer) return "dConfig";
    if (data->los_int_cnt_timer    == timer) return "los_int_cnt_timer";
    if (data->los_th_cnt_timer     == timer) return "los_th_cnt_timer";
    if (data->hli_cnt_timer        == timer) return "hli_cnt_timer";
    if (data->tx_aps_timer         == timer) return "tx_aps";
    if (data->rx_aps_timer         == timer) return "rx_aps";






    if (data->ais_cnt_timer        == timer) return "ais_cnt_timer";
    for (u32 i = 0; i < mep_caps.mesa_oam_peer_cnt; ++i) {
        if (data->dPeriod[i].timer == timer) return "dPeriod";
        if (data->dPrio[i].timer   == timer) return "dPrio";
        if (data->rx_ccm_timer[i]  == timer) return "rx_ccm";
    }
    return "?";
}

static BOOL mep_timer_active(struct vtss::Timer *timer)
{
    if (!timer)  { return FALSE; }
    timer_info_t *t_info = (timer_info_t*)timer->user_data;
    return t_info->active;
}

void vtss_mep_timer_cb(struct vtss::Timer *timer)
{
    if (!timer) {
        T_E("Invalid pointer");
        return;
    }

    timer_info_t *t_info = (timer_info_t*)timer->user_data;
    timer_obj_t  *t_obj = (timer_obj_t*)t_info->t_obj;
    mep_instance_data_t *data = t_info->data;
    u32 instance;

    if (!t_obj || !data) {
        T_E("Invalid pointer");
        return;
    }
    instance = data->instance;
    if (!t_info->active) {
        T_DG(TRACE_GRP_TIMER, "Timer '%s' expired but is not active (instance %d).",timer_to_string(data, timer), instance);
        return;
    }

    T_DG(TRACE_GRP_TIMER, "Timer '%s' expired (instance %d). Now run the associated function.",timer_to_string(data, timer), instance);

    t_info->active = FALSE;

    // Run the associated timer function
    if (*t_info->func_ptr) {
        (*t_info->func_ptr)(instance, timer);
    }
}

static void mep_timer_start(u32 period_ms, vtss::Timer **timer, mep_instance_data_t *data, void (*func_ptr)(u32, struct vtss::Timer *timer))
{
    timer_info_t *t_info;
    vtss::Timer  *timer_p;
    timer_obj_t  *timer_obj;

    T_NG(TRACE_GRP_TIMER, "Enter  period_ms %u", period_ms);

    if (period_ms == 0) {
        period_ms = 1;      /* A period of '0' ms means timeout at once (1 ms) */
    }
    if (!timer || !data) {
        T_E("Invalid pointer");
        return;
    }

    if (*timer == NULL) {
        timer_obj = new timer_obj_t; // deleted through data instance destructor
        if (timer_obj == NULL) {
            T_E("Could not create Timer object");
            return;
        }
        *timer             = &timer_obj->timer_p;
        t_info             = &timer_obj->t_info;
        t_info->func_ptr   = func_ptr;
        t_info->data       = data;
        t_info->t_obj      = timer_obj;

        timer_p            = &timer_obj->timer_p;
        timer_p->user_data = &timer_obj->t_info;
        timer_p->callback  = mep_timer_call_back;   /* Give a pointer to a call back function in mep.c file. This is because all critd lock/unlock is done in mep.c */
        timer_p->modid     = VTSS_MODULE_ID_MEP;
        timer_p->set_repeat(FALSE);
        T_DG(TRACE_GRP_TIMER, "Timer '%s' created (instance %d)",timer_to_string(data, timer_p), data->instance);
    } else {
        timer_p = *timer;
        t_info = (timer_info_t*)timer_p->user_data;
        T_DG(TRACE_GRP_TIMER, "Timer '%s' restarted (instance %d)" ,timer_to_string(data, timer_p), data->instance);
    }

    t_info->active     = TRUE;
    timer_p->set_period(vtss::milliseconds(period_ms));

    /* start/restart timer */
    vtss_mep_timer_start(timer_p);
}

static void mep_timer_cancel(vtss::Timer *timer)
{
    if (!timer) { return; }

    timer_info_t        *t_info = (timer_info_t*)timer->user_data;
    mep_instance_data_t *data = t_info->data;

    T_DG(TRACE_GRP_TIMER, "Timer '%s' cancelled (instance %d)" ,timer_to_string(data, timer), data->instance);

    t_info->active = FALSE;
    vtss_mep_timer_cancel(timer);
}

static void defect_timer_calc(mep_instance_data_t  *data,  defect_timer_t *defect,   u32 period,  BOOL period_only)
{
    if (period > defect->old_period) {
        defect->old_period = period; /* Save the longest seen period */
    }
    if (period_only) {
        return;                      /* This is update of longest received period only. Defect clear timers should not run. */
    }
    mep_timer_start(pdu_period_to_timer[defect->old_period], &defect->timer, data, run_defect_timer);
}

u32 is_tlv_change(mep_instance_data_t  *data)
{
    u32 period;

    T_D("Enter %u", data->instance);
    if (data->lst_config.enable) {  /* Link State Tracking is enabled */
        if (!data->supp_state.defect_state.ciSsf) { /* Check CI-SSF */
            /* LOS is not active. Start Remote Ignore Timer  - to avoid "deadlock" */
            switch (data->cc_config.rate) { /* This timer should cover the time for IS to get other end - remote link to get up (1 sec) - IS to get back here */
                case VTSS_APPL_MEP_RATE_300S:
                case VTSS_APPL_MEP_RATE_100S:
                case VTSS_APPL_MEP_RATE_10S: period = 6000; break;
                case VTSS_APPL_MEP_RATE_1S: period = 7000; break;
                default: period = 7000; break;     /* This is not possible */
            }
            mep_timer_start(period, &data->lst_state.ign_timer, data, run_lst_ign_timer);
        } else {
            mep_timer_cancel(data->lst_state.ign_timer);
        }
    }

    run_tlv_change(data->instance);

    T_D("Exit %u", data->instance);
    return(VTSS_RC_OK);
}

static BOOL ccm_defect_calc(u32 instance,   mep_instance_data_t *data,   u8 smac[],   u8 pdu[],   u32 prio,   BOOL *defect,   BOOL *state)
{
    u32             i;
    u32             mep, level, period;
    BOOL            rdi, unexp, period_only;
    defect_state_t  *defect_state;
    ccm_state_t     *ccm_state;

    defect_state = &data->supp_state.defect_state;
    ccm_state = &data->supp_state.ccm_state;

    /* Extract PDU information */
    level = pdu[0]>>5;
    mep = (pdu[8]<<8) + pdu[9];
    period = pdu[2] & 0x07;
    rdi = (pdu[2] & 0x80);

    unexp = FALSE;
    period_only = data->voe_ccm_enabled;    /* In case of VOE CCM handling this function is called for longst received period update only. */

    if (period == 0)
    {
        unexp = TRUE;
        if (!defect_state->dInv)                 *defect = TRUE;
        if (ccm_state->unexp_period != period)   *state = TRUE;
        defect_state->dInv = TRUE;
        ccm_state->unexp_period = period;
        goto return_;
    }
    else
    {
        if (defect_state->dInv)    *defect = TRUE;
        defect_state->dInv = FALSE;
    }

    /* Calculate all defects related to this CCM PDU */
    if (level != data->config.level)
    {
        unexp = TRUE;
        defect_timer_calc(data, &data->dLevel, period, period_only);
        if (!period_only) {
            if (!defect_state->dLevel)             *defect = TRUE;
            if (ccm_state->unexp_level != level)   *state = TRUE;
            defect_state->dLevel = TRUE;
            ccm_state->unexp_level = level;
        }
        goto return_;
    }
    if (memcmp(&pdu[10], data->exp_meg, data->exp_meg_len))
    {
        unexp = TRUE;
        defect_timer_calc(data, &data->dMeg, period, period_only);
        if (!period_only) {
            if (!defect_state->dMeg)                                       *defect = TRUE;
            if (memcmp(&pdu[10], ccm_state->unexp_meg_id, MEG_ID_LENGTH))  *state = TRUE;
            defect_state->dMeg = TRUE;
            memcpy(ccm_state->unexp_meg_id, &pdu[10], MEG_ID_LENGTH);
        }
        goto return_;
    }
    if (mep == data->config.mep)    /* Check if this CCM is from this instance MEP ID */
    {
        unexp = TRUE;

        if (!memcmp(smac, data->config.mac.addr, sizeof(data->config.mac.addr))) {    /* Check if SMAC is this instance SMAC */
            defect_timer_calc(data, &data->dLoop, period, period_only);
            if (!period_only) {
                if (!defect_state->dLoop)    *defect = TRUE;   /* SMAC is same as this instance SMAC - Loop is detected */
                defect_state->dLoop = TRUE;
            }
        } else {
            defect_timer_calc(data, &data->dConfig, period, period_only);
            if (!period_only) {
                if (!defect_state->dConfig)  *defect = TRUE;   /* SMAC is different from this instance SMAC - Configuration error is detected */
                defect_state->dConfig = TRUE;
            }
        }
    }
    for (i=0; i<data->config.peer_count; ++i)   if (data->config.peer_mep[i] == mep)   break;
    if (i == data->config.peer_count)   /* Valid Peer-MEP is not found */
    {
        unexp = TRUE;
        defect_timer_calc(data, &data->dMep, period, period_only);
        if (!period_only) {
            if (!defect_state->dMep)         *defect = TRUE;
            if (ccm_state->unexp_mep != mep) *state = TRUE;
            defect_state->dMep = TRUE;
            ccm_state->unexp_mep = mep;
        }
        goto return_;
    } else {    /* Valid Peer-MEP found */
        memcpy(data->config.peer_mac[i].addr, smac, sizeof(data->config.peer_mac[i].addr));
    }
    if (pdu_period_to_rate[period] != data->cc_config.rate)
    {
        defect_timer_calc(data, &data->dPeriod[i], period, period_only);
        if (!period_only) {
            if (!defect_state->dPeriod[i])          *defect = TRUE;
            if (ccm_state->unexp_period != period)  *state = TRUE;
            defect_state->dPeriod[i] = TRUE;
            ccm_state->unexp_period = period;
        }
    }
    if ((prio != 0xFFFFFFFF) && (prio != data->cc_config.prio))
    {
        defect_timer_calc(data, &data->dPrio[i], period, period_only);
        if (!period_only) {
            if (!defect_state->dPrio[i])         *defect = TRUE;
            if (ccm_state->unexp_prio != prio)   *state = TRUE;
            defect_state->dPrio[i] = TRUE;
            ccm_state->unexp_prio = prio;
        }
    }
    if (defect_state->dRdi[i] != rdi)
    {
        *defect = TRUE;
        defect_state->dRdi[i] = rdi;
    }

    if (defect_state->dLoc[i])
        *defect = TRUE;

    defect_state->dLoc[i] = FALSE;

    if (!period_only) {
        mep_timer_start(data->loc_timer_val, &data->rx_ccm_timer[i], data, run_rx_ccm_timer);
    }

    return_:

    return(unexp);
}

static void extract_tlv_info(u8 pdu[], mep_instance_data_t *data,  BOOL *new_is)
{
    u32          i, mep, off = CCM_PDU_TLV_OFFSET, max_loop=3;
    BOOL         is_received;
    u8           is_tlv_value;
    ccm_state_t  *ccm_state;

    ccm_state = &data->supp_state.ccm_state;

    mep = (pdu[8]<<8) + pdu[9];     /* Find the peer MEP */

    for (i=0; i<data->config.peer_count; ++i) {
        if (data->config.peer_mep[i] == mep) {
            break;
        }
    }

    if (i == data->config.peer_count) {  /* Check for known peer MEP */
        return;
    }

    is_received = ccm_state->is_received[i];
    is_tlv_value = ccm_state->is_tlv[i].value;

    ccm_state->ps_received[i] = FALSE;
    ccm_state->is_received[i] = FALSE;
    ccm_state->os_received[i] = FALSE;

    if (pdu[off] != 0) {  /* Check TLV present */
        while (max_loop && (pdu[off] != 0)) {
            switch (pdu[off]) {
                case 2:         /* Port TLV */
                    ccm_state->ps_received[i] = TRUE;
                    ccm_state->ps_tlv[i].value = pdu[off+3];
                    off += 4;
                    break;
                case 4:         /* Interface TLV */
                    ccm_state->is_received[i] = TRUE;
                    ccm_state->is_tlv[i].value = pdu[off+3];
                    off += 4;
                    break;
                case 31:        /* Organization Specific TLV */
                    ccm_state->os_received[i] = TRUE;
                    memcpy(ccm_state->os_tlv[i].oui, &pdu[off+3], VTSS_APPL_MEP_OUI_SIZE);
                    ccm_state->os_tlv[i].subtype = pdu[off+3+VTSS_APPL_MEP_OUI_SIZE];
                    ccm_state->os_tlv[i].value = pdu[off+3+VTSS_APPL_MEP_OUI_SIZE+1];
                    off += 3 + VTSS_APPL_MEP_OUI_SIZE + 2;
                    break;
            }
            max_loop--;
        }
    }

    if ((ccm_state->is_received[i] != is_received) || (ccm_state->is_tlv[i].value != is_tlv_value))
        *new_is = TRUE;
}





















































































































































































































































































































































































































































































/*
 * Process a received Synthetic Loss Measurement (SLM) PDU.
 */
static void process_sl_pdu(mep_instance_data_t *data, u8 *pdu_data)
{
    u32         src_mep_id = 0, resp_mep_id = 0;
    u32         peer_index = 0;
    lm_counter_sample_t *last_sample;

    // Get the opcode from the PDU
    mesa_oam_opcode_value_t opcode = (mesa_oam_opcode_value_t)pdu_data[1];

    // Get the Source MEP ID from the PDU
    src_mep_id = pdu_data[4] << 8 | pdu_data[5];

    if (opcode == MESA_OAM_OPCODE_1SL) {
        // Incoming request - try to match to one of our configured peer MEPs
        if (!get_peer_index(data, src_mep_id, peer_index)) {
            // not one of our peer MEP IDs
            T_DG(TRACE_GRP_RX_FRAME, "Instance %u, Invalid peer MEP ID %u", data->instance, src_mep_id);
            return;
        }

        // Extract counters from PDU:
        last_sample = &data->lm_peer_state[peer_index].last_pdu_sample;
        last_sample->far_tx = string_to_var(&pdu_data[12]);     // Get the PDU TxFCf counter
        last_sample->near_rx = string_to_var(&pdu_data[16]);    // There's no 'RxFCb' in the 1SL but the chip has added it (see 3.24.10.2 in the JR2 manual)

        // These are always zero for 1SL
        last_sample->near_tx = 0;
        last_sample->far_rx = 0;

        last_sample->is_valid = TRUE;                           // Last PDU counter sample is now valid

        T_NG(TRACE_GRP_RX_FRAME, "Instance %u, got 1SL PDU counters %u, %u, %u, %u", data->instance,
             last_sample->far_tx, last_sample->far_rx, last_sample->near_tx, last_sample->near_rx);

    } else if (opcode == MESA_OAM_OPCODE_SLR) {
        // Incoming reply - check both our own MEP ID and our peer MEP IDs
        src_mep_id = pdu_data[4] << 8 | pdu_data[5];
        resp_mep_id = pdu_data[6] << 8 | pdu_data[7];

        if (src_mep_id != data->config.mep) {
            // not our MEP ID
            T_DG(TRACE_GRP_RX_FRAME, "Instance %u, Invalid source MEP ID, got %u, expected %u", data->instance, src_mep_id, data->config.mep);
            return;
        }
        if (!get_peer_index(data, resp_mep_id, peer_index)) {
            // not one of our peer MEP IDs
            T_DG(TRACE_GRP_RX_FRAME, "Instance %u, Invalid peer MEP ID %u", data->instance, resp_mep_id);
            return;
        }

        // Extract counters from PDU
        last_sample = &data->lm_peer_state[peer_index].last_pdu_sample;
        last_sample->near_tx = string_to_var(&pdu_data[12]);
        last_sample->far_rx = string_to_var(&pdu_data[16]);     // There's no 'RxFCf' in the SLR so we assume that it is equal to TxFCb
        last_sample->far_tx = string_to_var(&pdu_data[16]);
        last_sample->near_rx = string_to_var(&pdu_data[20]);    // There's no 'RxFCb' in the SLR but the chip has added it (see 3.24.9.4 in the JR2 manual)

        last_sample->is_valid = TRUE;                           // Last PDU counter sample is now valid

        T_NG(TRACE_GRP_RX_FRAME, "Instance %u, got SLR PDU counters %u, %u, %u, %u", data->instance,
             last_sample->near_tx, last_sample->far_rx, last_sample->far_tx, last_sample->near_rx);
    }
}

static void mep_model_rx_cb(const u32 instance, const mep_model_pdu_rx_t *const pdu)
{
    const u32                                     single_peer_idx = 0;

    BOOL                                          unexpected, new_defect, new_state, new_is, line_rate, forward;
    mep_instance_data_t                           *data;
    u32                                           near_CT2, near_CR2, far_CT2, far_CR2, i, f_port;
    mesa_timestamp_t                              txTimef, rxTimef, txTimeb, rxTimeb;
    u8                                            mel;
    vtss_appl_mep_lt_transaction_t                *transaction;
    mesa_oam_opcode_value_t                       opcode = (mesa_oam_opcode_value_t)pdu->data[1];




    T_NG(TRACE_GRP_RX_FRAME, "Enter  instance %u", instance);

    data = get_instance(instance);
    if (!data) { return; }

    unexpected = new_defect = new_state = new_is = FALSE;
    memset(&txTimef, 0, sizeof(txTimef));
    memset(&rxTimef, 0, sizeof(rxTimef));
    memset(&txTimeb, 0, sizeof(txTimeb));
    memset(&rxTimeb, 0, sizeof(rxTimeb));









































































































    switch (opcode) {
        case MESA_OAM_OPCODE_CCM:
            unexpected = ccm_defect_calc(instance, data, pdu->smac, pdu->data, pdu->prio, &new_defect, &new_state);
            if (!unexpected) {
                data->supp_state.ccm_state.valid_counter++;
                extract_tlv_info(pdu->data, data, &new_is);
            } else {
                data->supp_state.ccm_state.invalid_counter++;
            }
            if (unexpected)      break;
            if (!(data->lm_config.enable && (data->lm_config.ended == VTSS_APPL_MEP_DUAL_ENDED)))       break;  /* CCM based LM is not enabled */
            if (!memcmp(lm_null_counter, &pdu->data[58], 12))            break;  /* No counters in this CCM */

            if (!data->config.voe) { /* This is not a VOE based instance */
                break;  /* Only LM on VOE based MEP */
            }

            near_CT2 = string_to_var(&pdu->data[66]);          /* Calculate LM information */
            far_CT2 = string_to_var(&pdu->data[58]);
            near_CR2 = string_to_var(&pdu->data[70]);
            far_CR2 = string_to_var(&pdu->data[62]);

            lm_counter_calc(instance,  single_peer_idx,  near_CT2,  far_CT2,  near_CR2,  far_CR2);   /* Calculate LM counters */
            break;
        case MESA_OAM_OPCODE_LBM:
            T_DG(TRACE_GRP_RX_FRAME, "Rx LBM inst=%u pdu->vid=%u pdu->port=%u pdu->prio=%u pdu->len=%u\n", instance, pdu->vid, pdu->port, pdu->prio, pdu->len);
            lbr_to_peer(data, pdu, MPLS_DOMAIN(data->config.domain));
            break;
        case MESA_OAM_OPCODE_LBR:
            T_DG(TRACE_GRP_RX_FRAME, "Rx LBR pdu->prio=%u pdu->len=%u\n", pdu->prio, pdu->len);
            mel = pdu->data[0] >> 5;
            if (mel != data->config.level)
                break;  /* Wrong MEG level */
            if (!test_enable_calc(data->lb_config.tests))
                break;  /* LBM is not enabled */
            /* Check if this MAC is already received: */
            for (i = 0; i < data->out_state.lb_state[pdu->prio].reply_cnt; ++i) {
                if (MPLS_DOMAIN(data->config.domain)) {
                    if (!memcmp(&pdu->data[11], data->out_state.lb_state[pdu->prio].reply[i].mep_mip_id, 16))
                        break;
                } else if (!memcmp(pdu->smac, data->out_state.lb_state[pdu->prio].reply[i].mac.addr, VTSS_APPL_MEP_MAC_LENGTH))
                    break;
            }
            if (i < mep_caps.mesa_oam_peer_cnt) { /* Old reply found or there is still room for more new replies */
                u32 transaction_id = string_to_var(&pdu->data[4]);
                if (i == data->out_state.lb_state[pdu->prio].reply_cnt) {/* New reply received */
                    data->out_state.lb_state[pdu->prio].reply_cnt++;
                    if (MPLS_DOMAIN(data->config.domain)) {
                        memcpy(data->out_state.lb_state[pdu->prio].reply[i].mep_mip_id, &pdu->data[11], 16);
                    } else {
                        memcpy(data->out_state.lb_state[pdu->prio].reply[i].mac.addr, pdu->smac, VTSS_APPL_MEP_MAC_LENGTH);
                    }
                }
                data->out_state.lb_state[pdu->prio].reply[i].lbr_received++;
                if (transaction_id != data->lb_state.rx_transaction_id[pdu->prio][i])
                    data->out_state.lb_state[pdu->prio].reply[i].out_of_order++;
                data->lb_state.rx_transaction_id[pdu->prio][i] = transaction_id + 1;  /* Next expected Transaction ID is this received - plus one */
            }
            break;
        case MESA_OAM_OPCODE_LTR:
            T_DG(TRACE_GRP_RX_FRAME, "Rx LTR inst=%u pdu->vid=%u pdu->port=%u pdu->prio=%u pdu->len=%u\n", instance, pdu->vid, pdu->port, pdu->prio, pdu->len);
            if (MPLS_DOMAIN(data->config.domain))     break;  /* not supported for MPLS */
            mel = pdu->data[0] >> 5;
            if (mel != data->config.level)            break;  /* Wrong MEG level */
            if (!data->lt_config.enable)              break;  /* LT is not enabled */
            transaction = &data->out_state.lt_state.transaction[data->out_state.lt_state.transaction_cnt];
            if (transaction->reply_cnt < VTSS_APPL_MEP_REPLY_MAX) {
                u32 transaction_id = string_to_var(&pdu->data[4]);
                if (transaction_id == data->lt_state.transaction_id) {
                    vtss_appl_mep_lt_reply_t *ltr = &transaction->reply[transaction->reply_cnt];
                    ltr->mode           = (pdu->data[2] & 0x20) ? VTSS_APPL_MEP_MEP : VTSS_APPL_MEP_MIP;
                    ltr->direction      = (pdu->data[29] == 5) ? VTSS_APPL_MEP_DOWN : VTSS_APPL_MEP_UP;
                    ltr->ttl            = pdu->data[8];
                    ltr->forwarded      = (pdu->data[2] & 0x40);
                    ltr->relay_action   = ((pdu->data[9] == 1) ? VTSS_APPL_MEP_RELAY_HIT :
                                           (pdu->data[9] == 2) ? VTSS_APPL_MEP_RELAY_FDB :
                                           (pdu->data[9] == 3) ? VTSS_APPL_MEP_RELAY_MPDB : VTSS_APPL_MEP_RELAY_UNKNOWN);
                    memcpy(ltr->last_egress_mac.addr, &pdu->data[15], 6);
                    memcpy(ltr->next_egress_mac.addr, &pdu->data[23], 6);

                    transaction->reply_cnt++;
                }
            }
            break;
        case MESA_OAM_OPCODE_LTM:
            T_DG(TRACE_GRP_RX_FRAME, "Rx LTM inst=%u  pdu->vid=%u  pdu->alt_vid=%u  pdu->port=%u  pdu->prio=%u  pdu->len=%u\n", instance, pdu->vid, pdu->alt_vid, pdu->port, pdu->prio, pdu->len);
            if (MPLS_DOMAIN(data->config.domain))     break;  /* not supported for MPLS */
            if (pdu->data[8] == 0)                    break;  /* TTL is "dead" */
            if (pdu->data[21] != 7)                   break;  /* No Egress identifier present */
            mel = pdu->data[0] >> 5;
            forward = vtss_mep_check_forwarding(pdu->port, &f_port, &pdu->data[15], pdu->vid); /* Is forwarding of LTM possible in received classified VID? */
            T_DG(TRACE_GRP_RX_FRAME, "forward %u\n", forward);
            if (!forward && pdu->alt_vid != 0) {
                forward = vtss_mep_check_forwarding(pdu->port, &f_port, &pdu->data[15], pdu->alt_vid); /* Is forwarding of LTM possible in E-TREE alternative classified VID? */
                T_DG(TRACE_GRP_RX_FRAME, "alternative forward %u\n", forward);
            }
            /* Look for ingress/egress MEP match: */
            if (data->config.mode == VTSS_APPL_MEP_MEP) {
                if (data->config.direction == VTSS_APPL_MEP_DOWN && data->config.level == mel) {
                    /* ingress MEP match found: */
                    if (!memcmp(&pdu->data[15], data->config.mac.addr, 6)) {/* Reply if this is the target MAC */
                        ltr_to_peer(data, pdu, FALSE, TRUE, 1);
                        break;
                    }
                    if (forward) {/* Reply if forwarding is possible */
                        ltr_to_peer(data, pdu, FALSE, TRUE, 2);
                        break;
                    }
                }
                if (data->config.direction == VTSS_APPL_MEP_UP && data->config.level == mel) {
                    /* egress MEP match found: */
                    if (!memcmp(&pdu->data[15], data->config.mac.addr, 6)) {/* Reply if this is the target MAC */
                        ltr_to_peer(data, pdu, FALSE, TRUE, 1);
                        break;
                    }
                    if (forward) { /* Reply if forwarding is possible on residence port */
                        if (data->config.port == f_port) {
                            ltr_to_peer(data, pdu, FALSE, TRUE, 2);
                            break;
                        }
                    }
                }
            } else if (data->config.mode == VTSS_APPL_MEP_MIP) {
                /* LTM has not hit a terminating ingress/egress MEP - check MIPs: */
                if (data->config.level == mel && !memcmp(&pdu->data[15], data->config.mac.addr, 6)) {
                    /* MIP with target MAC hit, send reply: */
                    ltr_to_peer(data, pdu, FALSE, FALSE, 1);
                    break;
                }
                /* No MIP with target MAC was found */
                if (data->config.level == mel && forward) {
                    /* Forwarding is possible - means that we will reply but not necessarily forward (depending on TTL) */
                    ltr_to_peer(data, pdu, (pdu->data[8] > 1), FALSE, 2);
                    ltm_to_forward(data, pdu, pdu->dmac);
                }
            }
            break;
        case MESA_OAM_OPCODE_AIS:
            T_DG(TRACE_GRP_AIS_LCK, "Rx AIS inst %u  dAis %u, period %u old_period = %u", instance, \
                   data->supp_state.defect_state.dAis, (pdu->data[2] & 0x07), data->dAis.old_period);
            if (!data->supp_state.defect_state.dAis) {
                new_defect = TRUE;
            }
            data->supp_state.defect_state.dAis = TRUE;
            defect_timer_calc(data, &data->dAis, (pdu->data[2] & 0x07), FALSE);
            break;
        case MESA_OAM_OPCODE_LCK:
            T_DG(TRACE_GRP_AIS_LCK, "RX LCK inst %u  dLck %u", instance, data->supp_state.defect_state.dLck);
            if (!data->supp_state.defect_state.dLck) {
                new_defect = TRUE;
            }
            data->supp_state.defect_state.dLck = TRUE;
            defect_timer_calc(data, &data->dLck, (pdu->data[2] & 0x07), FALSE);
            break;
        case MESA_OAM_OPCODE_LINEAR_APS:
        case MESA_OAM_OPCODE_RING_APS:
            mel = pdu->data[0] >> 5;
            if (mel != data->config.level)          break;  /* Wrong level */
            if (!data->aps_config.enable)           break;  /* APS is not enabled */
            vtss_mep_rx_aps_info_set(instance, data->config.domain, &pdu->data[4]);
            mep_timer_start(MEP_APS_TIMEOUT * 3, &data->rx_aps_timer, data, run_rx_aps_timer);
            break;
        case MESA_OAM_OPCODE_LMR:
            near_CT2 = string_to_var(&pdu->data[4]);          /* Calculate LM information */
            far_CT2 = string_to_var(&pdu->data[12]);
            near_CR2 = string_to_var(&pdu->data[16]);
            far_CR2 = string_to_var(&pdu->data[8]);

            lm_counter_calc(instance,  single_peer_idx,  near_CT2,  far_CT2,  near_CR2,  far_CR2);   /* Calculate LM counters */
            break;
        case MESA_OAM_OPCODE_LMM:
            T_DG(TRACE_GRP_RX_FRAME, "Unsupported MESA_OAM_OPCODE_LMM");
            break;

        case MESA_OAM_OPCODE_SLR:
        case MESA_OAM_OPCODE_1SL:
            process_sl_pdu(data, pdu->data);
            break;

        case MESA_OAM_OPCODE_1DM:
            txTimef.seconds = string_to_var(&pdu->data[4]);
            txTimef.nanoseconds = string_to_var(&pdu->data[8]);
            rxTimef.seconds = string_to_var(&pdu->data[12]);
            rxTimef.nanoseconds = string_to_var(&pdu->data[16]);

            handle_1dm(instance, pdu->prio, &txTimef, &rxTimef);
            break;
        case MESA_OAM_OPCODE_DMR:
            /* Save the timestamps as the latest measurement is used by PTP */
            data->txTimef.seconds = string_to_var(&pdu->data[4]);
            data->txTimef.nanoseconds = string_to_var(&pdu->data[8]);
            data->rxTimef.seconds = string_to_var(&pdu->data[12]);
            data->rxTimef.nanoseconds = string_to_var(&pdu->data[16]);
            data->txTimeb.seconds = string_to_var(&pdu->data[20]);
            data->txTimeb.nanoseconds = string_to_var(&pdu->data[24]);
            data->rxTimeb.seconds = string_to_var(&pdu->data[28]);
            data->rxTimeb.nanoseconds = string_to_var(&pdu->data[32]);
            data->dmr_ts_ok = TRUE;

            handle_dmr(instance, pdu->prio, &data->txTimef, &data->rxTimef, &data->txTimeb, &data->rxTimeb);
            break;
        case MESA_OAM_OPCODE_DMM:
            T_DG(TRACE_GRP_RX_FRAME, "Unsupported MESA_OAM_OPCODE_DMM");
            break;
        case MESA_OAM_OPCODE_TST:
            T_DG(TRACE_GRP_RX_FRAME, "Rx TST pdu->prio=%u pdu->len=%u\n", pdu->prio, pdu->len);
            line_rate = (data->config.domain == VTSS_APPL_MEP_PORT);
            data->tst_state.rx_frame_size = pdu->frame_size + (line_rate ? 20 : 0);  /* Frame size with added Preamble and Inter frame gap incase of line rate */
            break;



























        default:
            T_DG(TRACE_GRP_RX_FRAME, "Unknown OAM Opcode received");
            break;
    }

    if (new_defect)     new_defect_state(data);
    if (new_state)      new_ccm_state(data);
    if (new_is)         lst_action(data);
}

static void voe_defect_timer_calc(mep_instance_data_t  *data,   defect_timer_t *defect,   BOOL state)
{
    if (state) {
        mep_timer_cancel(defect->timer);
    } else {
        mep_timer_start(voe_period_to_timer[defect->old_period], &defect->timer, data, run_defect_timer);
    }
}

static void run_slm_meas_interval_expired(mep_instance_data_t *data)
{
    /* SLM Measurement interval expired.
     *
     * We feed the counters from the last received PDU into the loss calculation.
     * If no counters has been received since the last interval expired then we
     * just feed the last counters into the calculation anyway. This will make
     * the intervals counter increase but the loss ratio will not be updated
     * until we actually receive the next valid PDU.
     */

    for (int i = 0; i < data->config.peer_count; i++) {
        lm_counter_sample_t *sample = &data->lm_peer_state[i].last_pdu_sample;
        lm_counter_calc(data->instance, i, sample->near_tx, sample->far_tx, sample->near_rx, sample->far_rx, sample->is_valid);

        // Set counter sample to invalid
        sample->is_valid = FALSE;
    }

    mep_model_sl_meas_expired(data->instance);
}

static void  mep_model_event_cb(const u32                     instance,
                                const mep_model_event_type_t  event_type)
{
    u32                         i;
    mep_instance_data_t         *data;
    mep_model_ccm_status_t      ccm_status;



    mep_model_mep_info_t        info;
    defect_state_t              *defect_state;
    mep_model_mep_oper_state_t  oper_state;
    BOOL                        new_defect=TRUE;
    ccm_state_t                 *ccm_state;

    T_DG(TRACE_GRP_EVENT, "Enter  instance %u  type %u", instance,  event_type);

    data = get_instance(instance);
    if (!data) {
        return;
    }
    if ((data->config.mode == VTSS_APPL_MEP_MIP) && (event_type != MEP_MODEL_EVENT_MEP_STATUS_oper)) {
        return;
    }
    if ((data->out_state.state.mep.oper_state != VTSS_APPL_MEP_OPER_UP) && (event_type != MEP_MODEL_EVENT_MEP_STATUS_oper)) { /* If not operational ignore events other that "oper" */
        return;
    }
    T_NG(TRACE_GRP_EVENT, "oper_state %u", data->out_state.state.mep.oper_state);

    defect_state = &data->supp_state.defect_state;
    ccm_state    = &data->supp_state.ccm_state;

    if (!mep_model_ccm_status_get(instance, &ccm_status)) {
        T_D("mep_model_ccm_status_get failed");
    }

    if (!mep_model_mep_info_get(instance, &info)) {
        T_D("mep_model_info_get failed");
    }







    switch (event_type) {
        case MEP_MODEL_EVENT_CCM_STATUS_dInv:
            T_DG(TRACE_GRP_EVENT, "MEP_MODEL_EVENT_CCM_STATUS_dInv  dInv %u", ccm_status.dInv);
            defect_state->dInv = ccm_status.dInv;
            break;
        case MEP_MODEL_EVENT_CCM_STATUS_dLevel:
            T_DG(TRACE_GRP_EVENT, "MEP_MODEL_EVENT_CCM_STATUS_dLevel  dLevel %u", ccm_status.dLevel);
            defect_state->dLevel = TRUE;
            voe_defect_timer_calc(data, &data->dLevel, ccm_status.dLevel);
            break;
        case MEP_MODEL_EVENT_CCM_STATUS_dMeg:
            T_DG(TRACE_GRP_EVENT, "MEP_MODEL_EVENT_CCM_STATUS_dMeg  dMeg %u", ccm_status.dMeg);
            defect_state->dMeg = TRUE;
            voe_defect_timer_calc(data, &data->dMeg, ccm_status.dMeg);
            break;
        case MEP_MODEL_EVENT_CCM_STATUS_dMep:
            T_DG(TRACE_GRP_EVENT, "MEP_MODEL_EVENT_CCM_STATUS_dMep  dMep %u", ccm_status.dMep);
            defect_state->dMep = TRUE;
            voe_defect_timer_calc(data, &data->dMep, ccm_status.dMep);
            break;
        case MEP_MODEL_EVENT_CCM_STATUS_dLoop:
            T_DG(TRACE_GRP_EVENT, "MEP_MODEL_EVENT_CCM_STATUS_dLoop  dLoop %u", ccm_status.dLoop);
            defect_state->dLoop = TRUE;
            voe_defect_timer_calc(data, &data->dLoop, ccm_status.dLoop);
            break;
        case MEP_MODEL_EVENT_CCM_STATUS_dConfig:
            T_DG(TRACE_GRP_EVENT, "MEP_MODEL_EVENT_CCM_STATUS_dConfig  dConfig %u", ccm_status.dConfig);
            defect_state->dConfig = TRUE;
            voe_defect_timer_calc(data, &data->dConfig, ccm_status.dConfig);
            break;
        case MEP_MODEL_EVENT_CCM_STATUS_dPeriod:
            T_DG(TRACE_GRP_EVENT, "Instance %u  MEP_MODEL_EVENT_CCM_STATUS_dPeriod  dPeriod %u", instance, ccm_status.dPeriod);
            defect_state->dPeriod[0] = TRUE;
            voe_defect_timer_calc(data, &data->dPeriod[0], ccm_status.dPeriod);
            break;
        case MEP_MODEL_EVENT_CCM_STATUS_dPrio:
            T_DG(TRACE_GRP_EVENT, "Instance %u  MEP_MODEL_EVENT_CCM_STATUS_dPrio  dPrio %u", instance, ccm_status.dPrio);
            defect_state->dPrio[0] = TRUE;
            voe_defect_timer_calc(data, &data->dPrio[0], ccm_status.dPrio);
            break;
        case MEP_MODEL_EVENT_CCM_STATUS_dRdi:
            T_DG(TRACE_GRP_EVENT, "MEP_MODEL_EVENT_CCM_STATUS_dRdi  dRdi %u", ccm_status.dRdi);
            defect_state->dRdi[0] = ccm_status.dRdi;
            break;
        case MEP_MODEL_EVENT_CCM_STATUS_dLoc:
            T_DG(TRACE_GRP_EVENT, "MEP_MODEL_EVENT_CCM_STATUS_dLoc  dLoc %u", ccm_status.dLoc);
            defect_state->dLoc[0] = ccm_status.dLoc;
            break;
        case MEP_MODEL_EVENT_MEP_STATUS_dSsf:
            T_DG(TRACE_GRP_EVENT, "MEP_MODEL_EVENT_MEP_STATUS_dSsf  dSsf %u", info.dSsf);
            defect_state->dSsf = info.dSsf;
            break;
        case MEP_MODEL_EVENT_MEP_STATUS_ciSsf:
            T_DG(TRACE_GRP_EVENT, "MEP_MODEL_EVENT_MEP_STATUS_ciSsf  ciSsf %u", info.ciSsf);
            defect_state->ciSsf = info.ciSsf;
            break;
        case MEP_MODEL_EVENT_MEP_STATUS_forwarding:
            T_DG(TRACE_GRP_EVENT, "MEP_MODEL_EVENT_MEP_STATUS_forwarding  forwarding %u", info.forwarding);
            new_defect = FALSE;
            data->forwarding = info.forwarding;
            run_tlv_change(instance);
            break;
        case MEP_MODEL_EVENT_MEP_STATUS_oper:
            if (!mep_model_mep_oper_state_get(instance, &oper_state)) {
                T_D("mep_model_mep_oper_state_get failed");
                break;
            }
            new_defect = FALSE;

            if (data->out_state.state.mep.oper_state != mep_oper_state_calc(oper_state)) {  /* Oper state has changed */
                T_DG(TRACE_GRP_EVENT, "Oper state changed  state %u", oper_state);
                data->out_state.state.mep.oper_state = mep_oper_state_calc(oper_state); /* Save new oper state */

                if (data->config.mode == VTSS_APPL_MEP_MEP) {   /* MEP defect state depend on oper state */
                    if (oper_state != MEP_MODEL_OPER_UP) {  /* Not operational */
                        for (i = 0; i < mep_caps.mesa_oam_peer_cnt; ++i) {  /* Stop RX CCM timers */
                            delete_timer(&data->rx_ccm_timer[i]);
                        }
                        vtss_clear(*defect_state); /* Clear all defects */
                        new_defect = TRUE;
                    } else {                                /* Operational */
                        for (i=0; i<data->config.peer_count; ++i) { /* Start RX CCM timers */
                            mep_timer_start(data->loc_timer_val, &data->rx_ccm_timer[i], data, run_rx_ccm_timer);
                        }
                    }
                }
                if (!new_defect) {  /* If no new defect then status object must be updated with new oper state */
                    vtss_mep_status_change(data->instance, data->config.enable);
                }
            }
            break;
         case MEP_MODEL_EVENT_CCM_STATUS_tlv_port:
            T_DG(TRACE_GRP_EVENT, "MEP_MODEL_EVENT_CCM_STATUS_tlv_port  value %u", ccm_status.ps_tlv_value);
            ccm_state->ps_received[0] = TRUE;
            ccm_state->ps_tlv[0].value = ccm_status.ps_tlv_value;
            lst_action(data);
            break;
         case MEP_MODEL_EVENT_CCM_STATUS_tlv_if:
            T_DG(TRACE_GRP_EVENT, "MEP_MODEL_EVENT_CCM_STATUS_tlv_if  value %u", ccm_status.if_tlv_value);
            ccm_state->is_received[0] = TRUE;
            ccm_state->is_tlv[0].value = ccm_status.if_tlv_value;
            lst_action(data);
            break;
         case MEP_MODEL_EVENT_BFD_STATUS_dLoc:





            break;
         case MEP_MODEL_EVENT_BFD_STATUS_change:
            // not used
            break;
         case MEP_MODEL_EVENT_MPLS_update:



            break;
         case MEP_MODEL_EVENT_COUNT:    /* Invalid value */
            new_defect = FALSE;
            break;
    }

    if (new_defect) {
        T_DG(TRACE_GRP_EVENT, "new MEP model defect");
        new_defect_state(data);
    }

    T_DG(TRACE_GRP_EVENT, "Exit");
}

static BOOL mep_vlan_member_set(u32 vid,  mesa_port_list_t &ports,  BOOL enable)
{
    mesa_rc                rc;
    vtss_appl_vlan_entry_t vlan_entry;
    mesa_port_no_t         port_no;

    rc = vtss_appl_vlan_get(VTSS_ISID_START, vid, &vlan_entry, FALSE, VTSS_APPL_VLAN_USER_MEP);
    if (rc != VTSS_RC_OK && rc != VLAN_ERROR_ENTRY_NOT_FOUND) {
        return FALSE;
    }

    vlan_entry.vid = vid;
    for (port_no = 0; port_no < mep_caps.mesa_port_cnt; port_no++) {
        vlan_entry.ports[port_no] = (ports[port_no] ? 1 : 0);
    }

    if (enable)     {
        T_D("Add ERPS vlan %d",vid);
        if (vlan_mgmt_vlan_add(VTSS_ISID_START,  &vlan_entry,  VTSS_APPL_VLAN_USER_MEP) != VTSS_RC_OK) {
            return(FALSE);
        }
    }  else {
        T_D("Delete ERPS vlan %d",vid);
        if (vlan_mgmt_vlan_del(VTSS_ISID_GLOBAL, vid, VTSS_APPL_VLAN_USER_MEP) != VTSS_RC_OK) {
            return(FALSE);
        }
    }

    return(TRUE);
}

static BOOL mep_vlan_member_get(u32 vid,  mesa_port_list_t &ports)
{
    mesa_rc                rc;
    vtss_appl_vlan_entry_t vlan_entry;
    mesa_port_no_t         port_no;

    rc = vtss_appl_vlan_get(VTSS_ISID_START, vid, &vlan_entry, FALSE, VTSS_APPL_VLAN_USER_MEP);
    if (rc != VTSS_RC_OK && rc != VLAN_ERROR_ENTRY_NOT_FOUND) {
        return(FALSE);
    }

    if (vlan_entry.vid == 0) { /* Check if this VID exists */
        T_D("VID does not exist  %u", vid);
        return(TRUE);
    }

    for (port_no = 0; port_no < mep_caps.mesa_port_cnt; port_no++) {
        ports[port_no] = (vlan_entry.ports[port_no] == 1) ? TRUE : FALSE;
    }

    return(TRUE);
}

u32 vtss_mep_init(void)
{
    mep_default_conf_t  def_conf;

    pdu_period_to_timer[0] = 0;
    pdu_period_to_timer[1] = (12+1);          /* period 3.33ms*3,5 -> 12 ms */
    pdu_period_to_timer[2] = (35+1);          /* period 10ms*3,5 -> 35 ms */
    pdu_period_to_timer[3] = (350+1);         /* period 100ms*3,5 -> 350 ms */
    pdu_period_to_timer[4] = (3500+1);        /* period 1s*3,5 -> 1.000ms*3,5 -> 3500 ms */
    pdu_period_to_timer[5] = (35000+1);       /* period 10s*3,5 -> 10.000ms*3,5 -> 35000 ms */
    pdu_period_to_timer[6] = (210000+1);      /* period 1min*3,5 -> 60.000ms*3,5 -> 210000 ms */
    pdu_period_to_timer[7] = (2100000+1);     /* period 10 min*3,5 -> 600.000ms*3,5 -> 2100000 ms */

    voe_period_to_timer[0] = 0;
    voe_period_to_timer[1] = (10+1);          /* period 3.33ms*3 -> 10 ms */
    voe_period_to_timer[2] = (30+1);          /* period 10ms*3 -> 30 ms */
    voe_period_to_timer[3] = (300+1);         /* period 100ms*3 -> 300 ms */
    voe_period_to_timer[4] = (3000+1);        /* period 1s*3 -> 1.000ms*3 -> 3000 ms */
    voe_period_to_timer[5] = (30000+1);       /* period 10s*3 -> 10.000ms*3 -> 30000 ms */
    voe_period_to_timer[6] = (180000+1);      /* period 1min*3 -> 60.000ms*3 -> 180000 ms */
    voe_period_to_timer[7] = (1800000+1);     /* period 10 min*3 -> 600.000ms*3 -> 1800000 ms */

    vtss_mep_mgmt_default_conf_get(&def_conf);
    os_tlv_config = def_conf.tlv_conf.os_tlv;

    memset(vlan_raps_acl_id, 0xFF, sizeof(vlan_raps_acl_id));
    memset(vlan_raps_mac_octet, 0xFF, sizeof(vlan_raps_mac_octet));

    /* Register OAM PDU Rx callback function: */
    mep_model_rx_register(mep_model_rx_cb);

    /* Register event callback function: */
    mep_model_event_register(mep_model_event_cb);





    return(VTSS_RC_OK);
}

}  // mep_g2

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
