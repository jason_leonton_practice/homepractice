/* Switch API software.

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _MEP_ICLI_FUNCTIONS_H_
#define _MEP_ICLI_FUNCTIONS_H_

#include "icli_api.h"
#ifdef VTSS_SW_OPTION_ICFG
#include "icfg_api.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif


BOOL mep_runtime_range_instance(u32                   session_id,
                                icli_runtime_ask_t    ask,
                                icli_runtime_t        *runtime);

BOOL mep_runtime_mpls_present(u32                   session_id,
                                icli_runtime_ask_t    ask,
                                icli_runtime_t        *runtime);


BOOL mep_show_mep(i32 session_id, icli_range_t *inst,
                  BOOL has_peer, BOOL has_cc, BOOL has_lm, BOOL has_dm, BOOL has_lt, BOOL has_lb, BOOL has_tst, BOOL has_aps, BOOL has_client, BOOL has_ais, BOOL has_lck, BOOL has_pm, BOOL has_syslog, BOOL has_tlv, BOOL has_bfd, BOOL has_rt, BOOL has_lst, BOOL has_avail, BOOL has_lm_hli, BOOL has_detail);


BOOL mep_clear_mep(i32 session_id, u32 inst,
                   BOOL has_lm, BOOL has_dm, BOOL has_lb, BOOL has_tst, BOOL has_bfd,
                   BOOL clear_tx, BOOL clear_rx);


BOOL mep_mep(i32 session_id, u32 inst,
             BOOL has_mip, BOOL has_up, BOOL has_down, BOOL has_port, BOOL has_evc, BOOL has_vlan, BOOL has_tp_link, BOOL has_tunnel_tp, BOOL has_pw, BOOL has_lsp,
             BOOL has_vid, u32 vid, BOOL has_flow, u32 flow, u32 level, BOOL has_interface, icli_switch_port_range_t port);


BOOL mep_no_mep(i32 session_id, u32 inst);


BOOL mep_mep_meg_id(i32 session_id, u32 inst,
                    char *megid, BOOL has_itu, BOOL has_itu_cc, BOOL has_ieee, BOOL has_name, char *name);


BOOL mep_mep_mep_id(i32 session_id, u32 inst,
                    u32 mepid);

BOOL mep_mep_pm(i32 session_id, u32 inst);

BOOL mep_no_mep_pm(i32 session_id, u32 inst);


BOOL mep_mep_lst(i32 session_id, u32 inst);

BOOL mep_no_mep_lst(i32 session_id, u32 inst);


BOOL mep_mep_level(i32 session_id, u32 inst,
                   u32 level);


BOOL mep_mep_vid(i32 session_id, u32 inst,
                 u32 vid);

BOOL mep_no_mep_vid(i32 session_id, u32 inst);

BOOL mep_mep_voe(i32 session_id, u32 inst);


BOOL mep_no_mep_voe(i32 session_id, u32 inst);


BOOL mep_mep_peer_mep_id(i32 session_id, u32 inst,
                         u32 mepid, BOOL has_mac, mesa_mac_t mac);

BOOL mep_no_mep_peer_mep_id(i32 session_id, u32 inst,
                            u32 mepid, BOOL has_all);

BOOL mep_mep_cc(i32 session_id, u32 inst,
                u32 prio, BOOL has_fr300s, BOOL has_fr100s, BOOL has_fr10s, BOOL has_fr1s, BOOL has_fr6m, BOOL has_fr1m, BOOL has_fr6h, BOOL has_rx_only);


BOOL mep_no_mep_cc(i32 session_id, u32 inst);


BOOL mep_mep_lm(i32 session_id, u32 inst,
                u32 prio, BOOL has_synthetic, BOOL has_uni, BOOL has_multi, BOOL has_mep, BOOL has_single, BOOL has_dual, BOOL has_fr100s, BOOL has_fr10s, BOOL has_fr1s, BOOL has_fr6m, BOOL has_size,
                BOOL has_flr, u32 flr, BOOL has_meas, u32 meas, BOOL has_threshold, u32 loss_th, u32 mepid, u32 size, BOOL has_slm_testid, u32 slm_testid);

BOOL mep_no_mep_lm(i32 session_id, u32 inst, BOOL disable_tx, BOOL disable_rx);

BOOL mep_mep_lm_rx(i32 session_id, u32 inst, u32 prio, BOOL has_synthetic, BOOL has_fr100s, BOOL has_fr10s, BOOL has_fr1s, BOOL has_fr6m,
                   BOOL has_flr, u32 flr, BOOL has_meas, u32 meas, BOOL has_loss_ratio_threshold, u32 loss_ratio_threshold);

BOOL mep_mep_lm_flow_count(i32 session_id, u32 inst);

BOOL mep_no_mep_lm_flow_count(i32 session_id, u32 inst);


BOOL mep_mep_lm_oam_count(i32 session_id, u32 inst, BOOL has_y1731, BOOL has_all);

BOOL mep_no_mep_lm_oam_count(i32 session_id, u32 inst);

BOOL mep_mep_lm_avail(i32 session_id, u32 inst, u32 interval, u32 flr_th);

BOOL mep_no_mep_lm_avail(i32 session_id, u32 inst);

BOOL mep_mep_lm_avail_main(i32 session_id, u32 inst, BOOL main);

BOOL mep_mep_lm_hli(i32 session_id, u32 inst, u32 flr_th, u32 interval);

BOOL mep_no_mep_lm_hli(i32 session_id, u32 inst);

BOOL mep_mep_lm_sdeg(i32 session_id, u32 inst, u32 tx_min, u32 flr_th, u32 bad_th, u32 good_th);

BOOL mep_no_mep_lm_sdeg(i32 session_id, u32 inst);

BOOL mep_mep_lm_notif(i32 session_id, u32 inst, u32 los_int_cnt_holddown, u32 los_th_cnt_holddown, u32 hli_cnt_holddown);

BOOL mep_no_mep_lm_notif(i32 session_id, u32 inst);

BOOL mep_mep_dm(i32 session_id, u32 inst,
                u32 prio, BOOL has_uni, BOOL has_multi, u32 mepid, BOOL has_dual, BOOL has_single, BOOL has_rdtrp, BOOL has_flow, u32 interval, u32 lastn);

BOOL mep_no_mep_dm(i32 session_id, u32 inst);

BOOL mep_mep_dm_overflow_reset(i32 session_id, u32 inst);

BOOL mep_no_mep_dm_overflow_reset(i32 session_id, u32 inst);

BOOL mep_mep_dm_ns(i32 session_id, u32 inst);

BOOL mep_no_mep_dm_ns(i32 session_id, u32 inst);

BOOL mep_mep_dm_synchronized(i32 session_id, u32 inst);

BOOL mep_no_mep_dm_synchronized(i32 session_id, u32 inst);

BOOL mep_mep_dm_proprietary(i32 session_id, u32 inst);

BOOL mep_no_mep_dm_proprietary(i32 session_id, u32 inst);

BOOL mep_mep_dm_bin_threshold(i32 session_id, u32 inst, u32 threshold);

BOOL mep_no_mep_dm_bin_threshold(i32 session_id, u32 inst, u32 threshold);

BOOL mep_mep_dm_bin_fd(i32 session_id, u32 inst, u32 num_fd);

BOOL mep_no_mep_dm_bin_fd(i32 session_id, u32 inst, u32 num_fd);

BOOL mep_mep_dm_bin_ifdv(i32 session_id, u32 inst, u32 num_ifdv);

BOOL mep_no_mep_dm_bin_ifdv(i32 session_id, u32 inst, u32 num_ifdv);


BOOL mep_mep_lt(i32 session_id, u32 inst,
                u32 prio, BOOL has_mep_id, u32 mepid, BOOL has_mac, mesa_mac_t mac, u32 ttl);

BOOL mep_no_mep_lt(i32 session_id, u32 inst);


BOOL mep_mep_lb(i32 session_id, u32 inst,
                u32 prio, BOOL has_dei, BOOL has_multi, BOOL has_uni, BOOL has_mep_id, u32 mepid, BOOL has_mac, mesa_mac_t mac,
                BOOL has_mpls, BOOL has_ttl, u8 mpls_ttl, u32 count, u32 size, u32 interval);

BOOL mep_no_mep_lb(i32 session_id, u32 inst);


BOOL mep_mep_tst(i32 session_id, u32 inst,
                 u32 prio, BOOL has_dei, u32 mepid, BOOL has_sequence, BOOL has_all_zero, BOOL has_all_one, BOOL has_one_zero, u32 rate, u32 size);

BOOL mep_mep_tst_tx(i32 session_id, u32 inst);

BOOL mep_no_mep_tst_tx(i32 session_id, u32 inst);

BOOL mep_mep_tst_rx(i32 session_id, u32 inst);

BOOL mep_no_mep_tst_rx(i32 session_id, u32 inst);

BOOL mep_mep_aps(i32 session_id, u32 inst,
                 u32 prio, BOOL has_multi, BOOL has_uni, BOOL has_laps, BOOL has_raps, BOOL has_octet, u32 octet);


BOOL mep_no_mep_aps(i32 session_id, u32 inst);

BOOL mep_mep_client_flow(i32 session_id, u32 inst,
                         u32 cflow, BOOL has_level, u32 level, BOOL has_ais_prio, u32 aisprio, BOOL has_ais_highest, BOOL has_lck_prio, u32 lckprio, BOOL has_lck_highest, BOOL has_evc, BOOL has_vlan, BOOL has_lsp);

BOOL mep_no_mep_client_flow(i32 session_id, u32 inst,
                            u32 cflow, BOOL has_all, BOOL has_evc, BOOL has_vlan, BOOL has_lsp);


BOOL mep_mep_ais(i32 session_id, u32 inst,
                 BOOL has_fr1s, BOOL has_fr1m, BOOL has_protect);

BOOL mep_no_mep_ais(i32 session_id, u32 inst);


BOOL mep_mep_lck(i32 session_id, u32 inst,
                 BOOL has_fr1s, BOOL has_fr1m);

BOOL mep_no_mep_lck(i32 session_id, u32 inst);

BOOL mep_mep_syslog(i32 session_id, u32 inst);

BOOL mep_no_mep_syslog(i32 session_id, u32 inst);

BOOL mep_mep_os_tlv(i32 session_id,
                    u32 oui, u32 subtype, u32 value);

BOOL mep_mep_ccm_tlv(i32 session_id, u32 inst);


BOOL mep_no_mep_ccm_tlv(i32 session_id, u32 inst);





















BOOL mep_debug_mep_volatile(i32 session_id, u32 inst);

BOOL mep_no_debug_mep_volatile(i32 session_id, u32 inst);

BOOL mep_debug_mep_volatile_sat(i32 session_id, u32 inst);

BOOL mep_debug_mep_volatile_sat_prio(i32 session_id, u32 inst, u32 prio, u32 vid, u32 policer, BOOL has_dp, u32 dp);

BOOL mep_debug_mep_test(i32 session_id, u32 inst, BOOL has_lb, BOOL has_tst, u32 mepid, BOOL has_sequence);

BOOL mep_debug_mep_test_tx(i32 session_id, u32 inst, BOOL has_lb, BOOL has_tst, u32 prio, BOOL has_all, BOOL has_dei, BOOL has_all_zero, BOOL has_all_one, BOOL has_one_zero, BOOL has_rate, u32 rate, BOOL has_size, u32 size);

BOOL mep_no_debug_mep_test_tx(i32 session_id, u32 inst, BOOL has_lb, BOOL has_tst, u32 prio, BOOL has_all, BOOL has_dei, BOOL has_all_zero, BOOL has_all_one, BOOL has_one_zero, BOOL has_rate, u32 rate, BOOL has_size, u32 size);

BOOL mep_debug_mep_dm_tx(i32 session_id, u32 inst, BOOL has_dual, BOOL has_single, u32 prio, BOOL has_interval, u32 interval, BOOL has_synchronized);

BOOL mep_no_debug_mep_dm_tx(i32 session_id, u32 inst, BOOL has_dual, BOOL has_single, u32 prio, BOOL has_interval, u32 interval, BOOL has_synchronized);

BOOL mep_debug_mep_test_status(i32 session_id, u32 inst, BOOL has_lb, BOOL has_tst);

BOOL mep_debug_mep_test_clear(i32 session_id, u32 inst);

BOOL mep_debug_mep_test_bps_actual(i32 session_id, u32 inst);

BOOL mep_debug_mep_dm_status(i32 session_id, u32 inst, u32 prio);

BOOL mep_debug_mep_llm_tx(i32 session_id, u32 inst, BOOL has_activate, BOOL has_deactivate,
        BOOL has_status, mesa_mac_t dmac_addr, BOOL has_exptimer, u32 exptimerval,
        BOOL has_prio, u8 prio);
BOOL mep_debug_mep_slm_statistics(i32 session_id, u32 inst);

BOOL mep_debug_mep_inject(i32 session_id, BOOL has_single, BOOL has_afi, BOOL has_up, BOOL has_down, u32 opcode, u32 prio, BOOL has_isdx, u32 isdx, BOOL has_pipeline, u32 pipeline, BOOL has_tagged);

BOOL mep_no_debug_mep_inject(i32 session_id, BOOL has_single, BOOL has_afi, BOOL has_up, BOOL has_down, u32 opcode, u32 prio, BOOL has_isdx, u32 isdx, BOOL has_pipeline, u32 pipeline, BOOL has_tagged);

BOOL mep_debug_mep_raps_forward(i32 session_id, u32 inst);

BOOL mep_no_debug_mep_raps_forward(i32 session_id, u32 inst);

BOOL mep_debug_mep_model_status(i32 session_id, u32 inst);

mesa_rc mep_icfg_init(void);

#ifdef __cplusplus
}
#endif
#endif /* _MEP_ICLI_FUNCTIONS_H_ */



/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
