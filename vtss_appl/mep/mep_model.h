/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _MEP_MODEL_H_
#define _MEP_MODEL_H_

#include <mscc/ethernet/switch/api.h>   /* To get MESA types */
#include "mep_api.h"

namespace mep_g2 {

# define MEP_MODEL_PRIORITY_MAX 8

/****************************************************************************/
/*  Model initialization and data protection                                */
/****************************************************************************/
typedef enum {
    MEP_MODEL_INIT_NONE,
    MEP_MODEL_INIT_INIT,
    MEP_MODEL_INIT_HOOK,
    MEP_MODEL_INIT_DONE
} mep_model_init_t;

/* Model is initialized. All internal date are initialized and call-out to hookup for */
/* received frames and events and other system changes is done.                       */
void mep_model_init(mep_model_init_t  init);

/* Return free MCE id starting point */
u32 mep_model_mce_id_free_get(void);

/* Model is considered part of a locked domain meaning it does not contain own internal locking                            */
/* The model lock is NOT done when called at the functional model API (defined in this file)                               */
/* The model lock is done when called from outside the locked domain through callback like interrupts and receiving frames */
/* The model unlock is done whenever call-out is done to any function not defined as model API (outside the locked domain) */
void mep_model_critd_enter(const char* const file,  const int line);
void mep_model_critd_exit(const char* const file,  const int line);

/****************************************************************************/
/*  MEP/MIP create/delete                                                   */
/****************************************************************************/

/* MEP or MIP type: */
typedef enum {
    MEP_MODEL_MEP,
    MEP_MODEL_MIP
} mep_model_type_t;

typedef enum {
    MEP_MODEL_PORT,
    MEP_MODEL_EVC,
    MEP_MODEL_VLAN,
    MEP_MODEL_EVC_SUB,
    MEP_MODEL_MPLS_LINK,
    MEP_MODEL_MPLS_TUNNEL,
    MEP_MODEL_MPLS_PW,
    MEP_MODEL_MPLS_LSP,
} mep_model_domain_t;

typedef enum {
    MEP_MODEL_DOWN,
    MEP_MODEL_UP
} mep_model_direction_t;

/* MEP/MIP create parameters: */
typedef struct {
    mep_model_type_t      type;        /* MEP or MIP                  */
    mep_model_domain_t    domain;      /* Domain for this MEP         */
    mep_model_direction_t direction;   /* Up or Down MEP direction    */
    u32                   flow_num;    /* Flow number in this domain  */
} mep_model_mep_create_t;

/* MEP/MIP operational state: */
typedef enum {
    MEP_MODEL_OPER_UP,
    MEP_MODEL_OPER_DOWN,
    MEP_MODEL_OPER_INVALID_CONFIG,
    MEP_MODEL_OPER_HW_OAM,
    MEP_MODEL_OPER_MCE,
    /* add enums here for different down reasons */
    MEP_MODEL_OPER_COUNT        /* Number of States. This must be last always      */
} mep_model_mep_oper_state_t;

/* Create/delete MEP or MIP: */
u32 mep_model_mep_create(const u32 instance, const mep_model_mep_create_t *const create);
BOOL mep_model_mep_delete(const u32 instance);

/* Set/get administrative state for MEP or MIP: */
/* Return value FALSE indicate the MEP is not created */
u32 mep_model_mep_admin_state_set(const u32 instance, const BOOL admin_state);
BOOL mep_model_mep_admin_state_get(const u32 instance, BOOL *const admin_state);

/* Get operational state for MEP or MIP: */
BOOL mep_model_mep_oper_state_get(const u32 instance, mep_model_mep_oper_state_t *const oper_state);

/****************************************************************************/
/*  MEP/MIP generic/common configuration                                    */
/****************************************************************************/

#define MEP_MODEL_MAC_LENGTH  6
#define MEP_MODEL_ALL_VID  0xFFFF   /* In Subscriber domain the OAM is handled behind tag with all VID values - or without tag */
typedef struct {
    BOOL          hw;                           /* Select the use of a VOE/MIP HW instance for this MEP  */
    BOOL          sat;                          /* SAT mode enable                                       */
    u8            mac[MEP_MODEL_MAC_LENGTH];    /* MAC of this MEP                                       */
    u32           level;                        /* Level  0-7                                            */
    u32           port;                         /* Residence port                                        */
    u32           mepid;                        /* MepId for this MEP                                    */
    mesa_vid_t    vid;                          /* In Subscriber and Port domain the OAM is tagged with this VID. In Subscriber domain this can be MEP_MODEL_ALL_VID */
    BOOL          ll_is_used;                   /* TRUE if this MEP/MIP is used by a TT-LOOP LL instance */
} mep_model_mep_conf_t;

typedef enum {
    MEP_MODEL_ETREE_NONE,
    MEP_MODEL_ETREE_ROOT,
    MEP_MODEL_ETREE_LEAF
} mep_model_etree_t;

typedef struct {
    mep_model_etree_t   e_tree;              /* E-TREE type in case this EVC MEP is created in an E-TREE       */
    mesa_mac_t          mac;                 /* MAC address   ???????                                          */
    BOOL                voe_ccm_enabled;     /* CCM handling is enabled in the VOE and "slow" HMO is
                                                enabled. Otherwise all CCM PDUs are redirected to CPU          */
    BOOL                dSsf;                /* SSF is detected                                                */
    BOOL                ciSsf;               /* SSF is detected on Client Interface                            */
    BOOL                forwarding;          /* Residence port is forwarding the internal VID of this Down-MEP */
} mep_model_mep_info_t;

/* instance:    Instance number of MEP/MIP     */
/* conf:        Configuration                  */
/* Set generic/common configuration of MEP/MIP */
u32 mep_model_mep_conf_set(const u32                  instance,
                           const mep_model_mep_conf_t *const config);

/* instance:    Instance number of MEP/MIP     */
/* create:      MEP/MIP create info            */
/* config:      Configuration                  */
/* Get generic/common configuration of MEP/MIP */
BOOL mep_model_mep_conf_get(const u32               instance,
                            mep_model_mep_create_t  *const create,
                            mep_model_mep_conf_t    *const config);

/* Get info for MEP: */
BOOL mep_model_mep_info_get(const u32             instance,
                            mep_model_mep_info_t  *info);

/* instance:    Instance number of MEP             */
/* enable:      Enable/disable of aSSF             */
/* This controls aSSF generation in sink direction */
BOOL mep_model_mep_assf_set(const u32   instance,
                            const BOOL  aSsf);

/* instance:    Instance number of MEP                                    */
/* enable:      Enable/disable of blocking                                */
/* This controls service frame blocking in both sink and source direction */
BOOL mep_model_mep_block_set(const u32   instance,
                             const BOOL  block);

/* Port protection has changed state */
void mep_model_port_protection_change(const u32   w_port,
                                      const u32   p_port,
                                      const BOOL  active);

/* Port protection has been created */
void mep_model_port_protection_create(BOOL       one_plus_one,
                                      const u32  w_port,
                                      const u32  p_port);

/* Port protection has been deleted */
void mep_model_port_protection_delete(const u32  w_port,
                                      const u32  p_port);

/* Ring protection VLAN blocking state hac changed on a port */
void mep_model_ring_protection_block(const u32  port,
                                     BOOL       block);

/****************************************************************************/
/*  CCM configuration                                                       */
/****************************************************************************/

typedef enum {
    MEP_MODEL_RATE_INV,
    MEP_MODEL_RATE_300S,
    MEP_MODEL_RATE_100S,
    MEP_MODEL_RATE_10S,
    MEP_MODEL_RATE_1S,
    MEP_MODEL_RATE_6M
} mep_model_rate_t;

#define MEP_MODEL_MEG_CODE_LENGTH 48
#define MEP_MODEL_PEER_NONE       0xFFFFFFFF             /* No Peer Mep ID is given. This is in case of multiple peers
                                                            where the CCM RX check must be disabled in the VOE and all
                                                            CCM redirected to CPU                                           */
typedef struct {
    BOOL                enable;                          /* Enable/disable CCM PDU handling                                 */
    BOOL                lm_enable;                       /* Enable/disable CCM-LM PDU handling - loss counters will be
                                                            inserted with lm_rate                                           */
    mep_model_rate_t    lm_rate;                         /* This is the CCM-LM loss counter insertion rate                  */
    mep_model_rate_t    cc_rate;                         /* This is the CCM rate expected in received PDUs - see
                                                            MEP_MODEL_RATE_NONE                                             */
    u8                  prio;                            /* Priority                                                        */
    u8                  meg[MEP_MODEL_MEG_CODE_LENGTH];  /* Expected MEG ID Code.                                           */
    u32                 peer_mep;                        /* MEP id of peer MEP - see MEP_MODEL_PEER_NONE                    */
} mep_model_ccm_conf_t;

/* instance:    Instance number of MEP */
/* conf:        Configuration          */
/* Set CCM configuration of MEP        */
BOOL mep_model_ccm_conf_set(const u32                  instance,
                            const mep_model_ccm_conf_t *const conf);

/* instance:    Instance number of MEP */
/* conf:        Configuration          */
/* Get CCM configuration of MEP        */
BOOL mep_model_ccm_conf_get(const u32            instance,
                            mep_model_ccm_conf_t *const conf);

/* instance:    Instance number of MEP      */
/* enable:      Enable/disable of CCM RDI   */
/* This controls the RDI bit sent with CCM  */
BOOL mep_model_ccm_rdi_set(const u32  instance,
                           const BOOL enable);

/* Only MEP_MODEL_RATE_1S or MEP_MODEL_RATE_10S is supported */
#define MEP_MODEL_FAST_HMO_RATE  MEP_MODEL_RATE_1S

/****************************************************************************/
/*  CCM status                                                              */
/****************************************************************************/
typedef struct {
    u64                              valid_counter;   /* Valid CCM received                                 */
    u64                              invalid_counter; /* Invalid CCM received                               */
    u64                              oo_counter;      /* Counted received Out of Order sequence numbers
                                                         (VOE capability) */
    BOOL                             dInv;            /* Invalid period (0) received                        */
    BOOL                             dLevel;          /* Incorrect CCM level received                       */
    BOOL                             dMeg;            /* Incorrect CCM MEG id received                      */
    BOOL                             dMep;            /* Incorrect CCM MEP id received                      */
    BOOL                             dLoc;            /* CCM LOC state from peer MEP                        */
    BOOL                             dRdi;            /* CCM RDI state from peer MEP                        */
    BOOL                             dPeriod;         /* CCM Period state from peer MEP                     */
    BOOL                             dPrio;           /* CCM Priority state from peer MEP                   */
    BOOL                             dLoop;           /* CCM Loop state from peer MEP                       */
    BOOL                             dConfig;         /* CCM Config state from peer MEP                     */
    mesa_oam_if_status_tlv_value_t   if_tlv_value;    /* TLV port status for last CCM RX frame              */
    mesa_oam_port_status_tlv_value_t ps_tlv_value;    /* TLV interface status for last CCM RX frame         */
} mep_model_ccm_status_t;


/* instance:    Instance number of MEP  */
/* status:      CCM status              */
/* Get CCM status.                      */
BOOL mep_model_ccm_status_get(const u32              instance,
                              mep_model_ccm_status_t *const status);

/* instance:    Instance number of MEP    */
/* Clear the CCM status                   */
BOOL mep_model_ccm_status_clear(const u32 instance);

/****************************************************************************/
/*  LM configuration                                                        */
/****************************************************************************/

typedef enum {
    MEP_MODEL_OAM_COUNT_Y1731,           /* OAM PDUs are not counted as service frames as specified in Y1731 */
    MEP_MODEL_OAM_COUNT_NONE,            /* OAM PDUs are not counted as service frames                       */
    MEP_MODEL_OAM_COUNT_ALL              /* OAM PDUs are all counted as service frames                       */
} mep_model_oam_count_t;

typedef struct {
    BOOL                   enable;       /* Enable Single ended LM                                           */
    BOOL                   flow_count;   /* TRUE means LM is counting traffic (service frames) per flow -    */
                                         /* all priorities in one counter                                  */
    mep_model_oam_count_t  oam_count;    /* Configuration of how to count OAM PDU                            */
} mep_model_lm_conf_t;

/* instance:    Instance number of MEP         */
/* conf:        Configuration                  */
/* Set LMM configuration.                      */
BOOL mep_model_lm_conf_set(const u32                   instance,
                           const mep_model_lm_conf_t  *const conf);

/* instance:    Instance number of MEP         */
/* conf:        Configuration                  */
/* Get LMM configuration.                      */
BOOL mep_model_lm_conf_get(const u32             instance,
                           mep_model_lm_conf_t  *const conf);

/****************************************************************************/
/*  LM status                                                               */
/****************************************************************************/

typedef struct {
    u64     tx_lm;        /* Count of TX LMM/CCM-LM PDUs */
    u64     tx_lmr;       /* Count of TX LMR PDUs        */
    u64     rx_lmm;       /* Count of RX LMM PDUs        */
    u64     rx_lm;        /* Count of RX LMR/CCM-LM PDUs */
} mep_model_lm_status_t;

typedef enum {
    MEP_MODEL_CLEAR_DIR_BOTH,       /* Clear counters for both directions (TX and RX).                                  */
    MEP_MODEL_CLEAR_DIR_TX,         /* Clear counters for TX direction only.                                            */
    MEP_MODEL_CLEAR_DIR_RX          /* Clear counters for RX direction only.                                            */
} mep_model_clear_dir_t;

/* instance:   Instance number of MEP     */
/* Accumulated LMM counters are returned  */
BOOL mep_model_lm_status_get(const u32              instance,
                             mep_model_lm_status_t  *const status);

/* instance:    Instance number of MEP   */
/* Clear the LM status                   */
BOOL mep_model_lm_status_clear(const u32 instance, bool is_sl, mep_model_clear_dir_t direction);

/****************************************************************************/
/*  SL configuration                                                        */
/****************************************************************************/

#define MEP_MODEL_MEP_PEER_MAX 8
typedef struct {
    BOOL   enable_tx;                            // Enable SL transmission
    BOOL   enable_rx;                            // Enable SL reception
    u32    test_id;                              // SL initiator Test ID, sent in SLM/1SL and validated in received SLR/1SL
    u32    prio;                                 // SL priority
    u32    peer_count;                           // Number of peer MEP's. 0 to MEP_MODEL_MEP_PEER_MAX-1
    u16    peer_mep[MEP_MODEL_MEP_PEER_MAX];     // MEP id of peer MEP's
} mep_model_sl_conf_t;

/* instance:    Instance number of MEP         */
/* conf:        Configuration                  */
/* Set SLM configuration.                      */
BOOL mep_model_sl_conf_set(const u32                   instance,
                           const mep_model_sl_conf_t  *const conf);

/* instance:    Instance number of MEP         */
/* conf:        Configuration                  */
/* Get SL configuration.                       */
BOOL mep_model_sl_conf_get(const u32             instance,
                           mep_model_sl_conf_t  *const conf);

/****************************************************************************/
/*  SL status                                                               */
/****************************************************************************/

typedef struct {
    u64   tx_slm;                          /* Count of TX SLM/1SL PDUs */
    u64   rx_slm;                          /* Count of RX SLM/1SL PDUs */
    u64   rx_slr[MEP_MODEL_MEP_PEER_MAX];  /* Count of RX SLR/1SL PDUs per SL peer */
    u64   tx_slr[MEP_MODEL_MEP_PEER_MAX];  /* Count of TX SLR PDUs per SL peer */
} mep_model_sl_status_t;

/* instance:   Instance number of MEP     */
/* Accumulated SL counters are returned   */
BOOL mep_model_sl_status_get(const u32               instance,
                             mep_model_sl_status_t  *const status);

/* instance:   Instance number of MEP     */
/* Inform the model that a measurement interval has expired   */
BOOL mep_model_sl_meas_expired(const u32             instance);


/****************************************************************************/
/*  DM configuration                                                        */
/****************************************************************************/


/****************************************************************************/
/*  DM status                                                               */
/****************************************************************************/

typedef struct {
    u64     tx_dm;     /* Count of TX DMM/1DM PDUs. */
    u64     tx_dmr;    /* Count of TX DMR PDUs      */
    u64     rx_dmm;    /* Count of RX DMM PDUs      */
    u64     rx_dm;     /* Count of RX DMR/1DM PDUs. */
} mep_model_dm_status_t;

/* instance:   Instance number of MEP    */
/* Accumulated DM counters are returned  */
BOOL mep_model_dm_status_get(const u32              instance,
                             const u8               prio,
                             mep_model_dm_status_t  *const status);

/* instance:    Instance number of MEP   */
/* Clear the DM status                   */
BOOL mep_model_dm_status_clear(const u32 instance,
                               const u8  prio);

/****************************************************************************/
/*  APS/RAPS configuration                                                  */
/****************************************************************************/

typedef struct {
} mep_model_aps_conf_t;

/* instance:    Instance number of MEP         */
/* conf:        Configuration                  */
/* Set APS/RAPS configuration                  */
u32 mep_model_aps_conf_set(const u32                  instance,
                           const mep_model_aps_conf_t *const conf);

/* instance:    Instance number of MEP         */
/* conf:        Configuration                  */
/* Get APS/RAPS configuration                  */
u32 mep_model_aps_conf_get(const u32                  instance,
                           mep_model_aps_conf_t       *const conf);

/* instance:    Instance number of MEP.                 */
/* enable:      TRUE means that R-APS is forwarded      */
/* R-APS forwarding from the MEP related port, can be enabled/disabled (VOE capability) */
u32 mep_model_raps_forwarding(const u32    instance,
                              const BOOL   enable);

/****************************************************************************/
/*  LT configuration                                                        */
/****************************************************************************/

typedef struct {
    BOOL copy_ltr_to_cpu;
} mep_model_lt_conf_t;

/* instance:    Instance number of MEP         */
/* conf:        Configuration                  */
/* Set LT configuration.                       */
BOOL mep_model_lt_conf_set(const u32                  instance,
                           const mep_model_lt_conf_t  *const conf);

/* instance:    Instance number of MEP         */
/* conf:        Configuration                  */
/* Get LT configuration.                       */
BOOL mep_model_lt_conf_get(const u32                  instance,
                           mep_model_lt_conf_t        *const conf);

/****************************************************************************/
/*  LB/TST configuration                                                    */
/****************************************************************************/

typedef struct {
    BOOL enable;         /* Enable/disable LBM */
    BOOL copy_lbr_to_cpu;   /* TRUE: Copy LBR to CPU  */
} mep_model_lb_conf_t;

/* instance:    Instance number of MEP    */
/* conf:        Configuration             */
/* Set LB configuration.                  */
BOOL mep_model_lb_conf_set(const u32                  instance,
                           const mep_model_lb_conf_t *const conf);

/* instance:    Instance number of MEP    */
/* conf:        Configuration             */
/* Get LB configuration. A to_send number of LBM is transmitted. Received LBR is delivered to upper logic */
BOOL mep_model_lb_conf_get(const u32                  instance,
                           mep_model_lb_conf_t       *const conf);

typedef struct {
    BOOL enable;            /* Enable/disable TST                                  */
    BOOL copy_to_cpu;       /* Copy next received TST to CPU. This is a "one shot" */
    u32  transaction_id;    /* Transaction ID                                      */
} mep_model_tst_conf_t;

/* instance:    Instance number of MEP    */
/* conf:        Configuration             */
/* Set TST configuration.                 */
BOOL mep_model_tst_conf_set(const u32                   instance,
                            const mep_model_tst_conf_t  *const conf);

/* instance:    Instance number of MEP    */
/* conf:        Configuration             */
/* Get TST configuration.                 */
BOOL mep_model_tst_conf_get(const u32                   instance,
                            mep_model_tst_conf_t        *const conf);

/****************************************************************************/
/*  LB status                                                               */
/****************************************************************************/

typedef struct {
    u64     lbr_counter;      /* Received LBR PDU count                                          */
    u64     lbm_counter;      /* Transmitted LBM PDU count                                       */
    u64     oo_counter;       /* Counted received Out of Order sequence numbers (VOE capability) */
    u64     lbr_crc_err;      /* Received LBR PDUs with a Test TLV that has CRC error(s)         */
    u32     trans_id;         /* The next transmitted transaction id                             */
} mep_model_lb_status_t;

/* instance:    Instance number of MEP   */
/* status:      LB status                */
/* The LB status counters                */
BOOL mep_model_lb_status_get(const u32               instance,
                             const u8                prio,
                             mep_model_lb_status_t  *const status);

/* instance:    Instance number of MEP   */
/* Clear the LB status counters          */
BOOL mep_model_lb_status_clear(const u32 instance,
                               const u8  prio);

/****************************************************************************/
/*  TST status                                                              */
/****************************************************************************/

typedef struct {
    u64     tx_counter;       /* Transmitted TST PDU count                                       */
    u64     rx_counter;       /* Received TST PDU count (VOE capability)                         */
    u64     oo_counter;       /* Counted received Out of Order sequence numbers (VOE capability) */
    u64     crc_err_counter;  /* Received TST PDUs with a Test TLV that has CRC error(s)         */
} mep_model_tst_status_t;

/* instance:    Instance number of MEP   */
/* prio:        Priority                 */
/* status:      TST status               */
/* The TST status counters               */
BOOL mep_model_tst_status_get(const u32               instance,
                              const u8                prio,
                              mep_model_tst_status_t  *const status);

/* instance:    Instance number of MEP   */
/* prio:        Priority                 */
/* Clear the TST status counters         */
BOOL mep_model_tst_status_clear(const u32 instance,
                                const u8  prio);

/****************************************************************************/
/*  AIS/LCK client flow configuration                                       */
/****************************************************************************/

#define MEP_MODEL_CLIENT_FLOW_MAX  10   /* The maximum number of client flows to be added */

typedef struct {
    mep_model_domain_t domain;
    u32                flow;
} mep_model_client_t;

/* instance:    Instance number of MEP   */
/* client_flow: Client flow to add       */
/* Add client flow for instance          */
BOOL mep_model_client_flow_add(const u32 instance,
                               const mep_model_client_t *client_flow);

/* instance:    Instance number of MEP   */
/* Clear all client flows for instance   */
BOOL mep_model_client_flow_clear(const u32 instance);

/****************************************************************************/
/*  MPLS G.8113.2 BFD                                                       */
/****************************************************************************/























































/****************************************************************************/
/*  OAM PDU Rx/Tx                                                           */
/****************************************************************************/

/* OAM PDU Rx information: */
typedef struct {
    u8          *data;        /* Pointer to OAM PDU data                                                */
    u32         len;          /* OAM PDU length in bytes                                                */
    u32         frame_size;   /* Size of frame (as received on port inclusive CRC) containing this PDU  */
    u8          prio;         /* Priority                                                               */
    u8          *dmac;        /* Pointer to DMAC                                                        */
    u8          *smac;        /* Pointer to SMAC                                                        */
    mesa_vid_t  vid;          /* Classified VID                                                         */
    mesa_vid_t  alt_vid;      /* Alternative Classified VID in case of E-TREE to be used for LTM forwarding    */
    u32         port;         /* Rx port                                                                */



} mep_model_pdu_rx_t;

/* OAM PDU Tx information: */
#define MEP_MODEL_VID_UNTAGGED   0xFFFFFFFE   /* Inject in Subscriber domain un-tagged */
typedef struct {
    u8                 *data;        /* Pointer to OAM PDU data  */
    u32                len;          /* OAM PDU length in bytes  */
    u32                vid;          /* In Subscriber domain and mep_model_mep_conf_t.vid == MEP_MODEL_ALL_VID, the subscriber TAG will contain this VID. Can be MEP_MODEL_VID_UNTAGGED meaning no subscriber TAG */
    u8                 prio;         /* Priority                 */
    BOOL               dei;          /* DEI                      */
    u8                 *mac;         /* Pointer to DMAC          */
    u32                rate;         /* Frame rate in kbit/s or f/h depending on 'rate_is_kbps'
                                        0 means transmit only 1 frame
                                        !=0 means AFI transmission if possible, otherwise (low rate) SW injected  */
    BOOL               rate_is_kbps; /* TRUE: Frame rate is in kbit/s. FALSE: Frame rate is f/s                   */
    BOOL               line_rate;    /* Should preamble and inter frame gap be included in frame size when
                                        calculating the frame rate                                                */
    BOOL               jitter;       /* FALSE: No jitter. TRUE: Transmit with jitter. The next frame is
                                        transmitted randomly from 75% to 100% of the period after the previous    */
    BOOL               reverse_inj;  /* TRUE: Reverse injection, e.g. for AIS, LCK, LTM forwarding                */
    BOOL               client_inj;   /* TRUE: Client injection as configured in 'client_flow'                     */
    mep_model_client_t client_flow;  /* Client flow configuration                                                 */
    BOOL               mpls2;        /* TRUE: ITU G.8113.2 MPLS PDU. FALSE: ITU G.8113.1 MPLS / Y.1731 Eth        */
} mep_model_pdu_tx_t;

/* OAM PDU Tx handle: */
typedef u32 mep_model_pdu_tx_id_t;
#define MEP_MODEL_PDU_TX_ID_NONE    0

/* OAM PDU Rx callback function. This is done locked - considered inside the locked domain */
typedef void (*mep_model_rx_cb_t)(const u32 instance, const mep_model_pdu_rx_t *const pdu);

/* Register OAM PDU Rx callback function: */
void mep_model_rx_register(mep_model_rx_cb_t cb);

/* Transmit OAM PDU: */
/* 'tx_id' is loaded with unique id to be used when canceling */
u32  mep_model_tx(const u32                 instance,
                  const mep_model_pdu_tx_t  *const pdu,
                  mep_model_pdu_tx_id_t     *const tx_id);

/* Stop/cancel OAM PDU Tx: */
/* 'tx_id' is loaded with MEP_MODEL_PDU_TX_ID_NONE. Call with MEP_MODEL_PDU_TX_ID_NONE has no effect */
void mep_model_tx_cancel(const u32              instance,
                         mep_model_pdu_tx_id_t  *const tx_id,
                         u64                    *active_time_ms);

/* Get the actual TX BSP for a 'tx_id' */
BOOL mep_model_tx_bps_act_get(const u32                 instance,
                              mep_model_pdu_tx_id_t     tx_id,
                              u64                       *const bps_act);


/****************************************************************************/
/*  Events                                                                  */
/****************************************************************************/

/* Event type: */
typedef enum {
    MEP_MODEL_EVENT_CCM_STATUS_dInv = 0,         /* CCM status dInv changed                         */
    MEP_MODEL_EVENT_CCM_STATUS_dLevel,           /* CCM status dLevel changed                       */
    MEP_MODEL_EVENT_CCM_STATUS_dMeg,             /* CCM status dMeg changed                         */
    MEP_MODEL_EVENT_CCM_STATUS_dMep,             /* CCM status dMep changed                         */
    MEP_MODEL_EVENT_CCM_STATUS_dLoc,             /* CCM status dLoc changed                         */
    MEP_MODEL_EVENT_CCM_STATUS_dRdi,             /* CCM status dRdi changed                         */
    MEP_MODEL_EVENT_CCM_STATUS_dPeriod,          /* CCM status dPeriod changed                      */
    MEP_MODEL_EVENT_CCM_STATUS_dPrio,            /* CCM status dPrio changed                        */
    MEP_MODEL_EVENT_CCM_STATUS_dLoop,            /* CCM status dLoop changed                        */
    MEP_MODEL_EVENT_CCM_STATUS_dConfig,          /* CCM status dConfig changed                      */
    MEP_MODEL_EVENT_MEP_STATUS_dSsf,             /* MEP status dSsf changed                         */
    MEP_MODEL_EVENT_MEP_STATUS_ciSsf,            /* MEP status ciSsf changed  (Client Interface)    */
    MEP_MODEL_EVENT_MEP_STATUS_forwarding,       /* MEP status forwarding state changed             */
    MEP_MODEL_EVENT_MEP_STATUS_oper,             /* MEP status oper state changed                   */
    MEP_MODEL_EVENT_CCM_STATUS_tlv_port,         /* CCM status tlv port state changed               */
    MEP_MODEL_EVENT_CCM_STATUS_tlv_if,           /* CCM status tlv interface state changed          */
    MEP_MODEL_EVENT_BFD_STATUS_dLoc,             /* BFD status dLoc changed                         */
    MEP_MODEL_EVENT_BFD_STATUS_change,           /* BFD status changed                              */
    MEP_MODEL_EVENT_MPLS_update,                 /* MPLS interface update                           */
    /* add here */
    MEP_MODEL_EVENT_COUNT,                       /* Number of Events. This must be last always      */
} mep_model_event_type_t;

/* Event callback function. This is done locked - considered inside the locked domain */
typedef void (*mep_model_event_cb_t)(const u32                     instance,
                                     const mep_model_event_type_t  event_type);

/* Register event callback function: */
void mep_model_event_register(mep_model_event_cb_t cb);


/****************************************************************************/
/*  Debug                                                                   */
/****************************************************************************/

typedef struct {
    BOOL  status[MEP_MODEL_EVENT_COUNT];
} mep_model_event_status_t;

typedef struct {
    mep_model_event_status_t    event_status;        /* Event status          */
    mep_model_ccm_status_t      last_ccm_status;     /* Last CCM status get   */
    mep_model_mep_info_t        last_info;           /* Last info get         */
    mep_model_mep_oper_state_t  last_oper_state;     /* Last oper state       */
} mep_model_mep_debug_t;

/* Get debug info for MEP: */
BOOL mep_model_mep_debug_get(const u32              instance,
                             mep_model_mep_debug_t  *info);

}  // mep_g2

#endif /* _MEP_MODEL_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
