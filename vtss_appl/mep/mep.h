/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

#ifndef _MEP_H_
#define _MEP_H_

#include "main_types.h"
#include "vtss_timer_api.h"
#include "vtss/appl/mep.h"
#include "mep_ll_api.h"
#include "packet_api.h"
#include "mep_g2_api.h"

namespace mep_g2 {

const char *mep_error_txt(mesa_rc rc);

void mep_default_conf_get(mep_default_conf_t  *const conf);

mesa_rc mep_instance_conf_get(u32 instance,  mep_conf_t *const conf);

mesa_rc mep_volatile_sat_conf_set(const u32              instance,
                                  const mep_sat_conf_t   *const conf);

mesa_rc mep_volatile_sat_conf_get(const u32        instance,
                                  mep_sat_conf_t   *const conf);

mesa_rc mep_volatile_sat_counter_get(const u32           instance,
                                     mep_sat_counters_t  *const counters);

mesa_rc mep_volatile_sat_counter_clear(const u32  instance);

mesa_rc mep_tx_bps_actual_get(const u32     instance,
                              mep_tx_bps_t  *const bps);

vtss_rc mep_volatile_sat_lb_prio_conf_set(const                                 u32 instance,
                                          const                                 u32 prio,
                                          const                                 u32 test_idx,
                                          const vtss_appl_mep_test_prio_conf_t  *const conf,
                                                                                u64 *active_time_ms);

vtss_rc mep_volatile_sat_tst_prio_conf_set(const                                 u32 instance,
                                           const                                 u32 prio,
                                           const                                 u32 test_idx,
                                           const vtss_appl_mep_test_prio_conf_t  *const conf,
                                                                                 u64 *active_time_ms);

mesa_rc mep_volatile_conf_set(const u32         instance,
                              const mep_conf_t *const conf);

mesa_rc mep_slm_counters_get(const u32            instance,
                             const u32            peer_idx,
                             sl_service_counter_t *const counters);

mesa_rc mep_dm_timestamp_get(const u32           instance,
                             mep_dm_timestamp_t  *const dm1_timestamp_far_to_near,
                             mep_dm_timestamp_t  *const dm1_timestamp_near_to_far);

#define MEP_EPS_MAX                65      /* Max number of EPS relations */

/* instance:    Instance number of MEP.               */
/* eps_inst:    Instance number of calling EPS/REPS   */
/* eps_type:    It is a ELPS or ERPS register         */
/* enable:      TRUE means register is enabled        */
/* Receiving and transmitting of APS from/to this MEP, can be enabled/disabled */
mesa_rc mep_eps_aps_register(const u32              instance,
                             const u32              eps_inst,
                             const mep_eps_type_t   eps_type,
                             const BOOL             enable);

/* instance:    Instance number of MEP.               */
/* eps_inst:    Instance number of calling EPS/REPS   */
/* eps_type:    It is a ELPS or ERPS register         */
/* enable:      TRUE means register is enabled        */
/* Receiving of SF state from this MEP, can be enabled/disabled */
mesa_rc mep_eps_sf_register(const u32              instance,
                            const u32              eps_inst,
                            const mep_eps_type_t   eps_type,
                            const BOOL             enable);

/* instance:    Instance number of MEP.             */
/* eps_inst:    Instance number of calling EPS/REPS */
/* aps:         transmitted APS specific info.      */
/* event:       transmit APS as an ERPS event.      */
/* This called by EPS to transmit APS info. The first three APS is transmitted with 3.3 ms. interval and thereafter with 5 s. interval.  */
/* The length of the 'aps' array must be MEP_APS_DATA_LENGTH minimum to satisfy LINT                                                */
mesa_rc mep_tx_aps_info_set(const u32   instance,
                            const u32   eps_inst,
                            const u8    *aps,
                            const BOOL  event);

/* instance:    Instance number of MEP.             */
/* eps_inst:    Instance number of calling EPS/REPS */
/* This is called by EPS to activate MEP calling out with SF state and received APS */
mesa_rc mep_signal_in(const u32   instance,
                      const u32   eps_inst);


/* instance:    Instance number of MEP.               */
/* eps_inst:    Instance number of calling EPS/REPS   */
/* enable:      TRUE means that R-APS is forwarded    */
/* R-APS forwarding to the MEP related port, can be enabled/disabled */
mesa_rc mep_raps_forwarding(const u32     instance,
                            const u32     eps_inst,
                            const BOOL    enable);


/* instance:    Instance number of MEP.                                    */
/* eps_inst:    Instance number of calling EPS/REPS                        */
/* enable:      TRUE means that R-APS is transmitted at 5 sec. interval    */
/* R-APS transmission from the MEP related port, can be enabled/disabled   */
mesa_rc mep_raps_transmission(const u32     instance,
                              const u32     eps_inst,
                              const BOOL    enable);

void mep_port_protection_create(mep_eps_architecture_t architecture,  const u32 w_port,  const u32 p_port);

void mep_port_protection_change(const u32         w_port,
                                const u32         p_port,
                                const BOOL        active);

void mep_port_protection_delete(const u32 w_port,  const u32 p_port);

void mep_ring_protection_block(const u32 port,  BOOL block);

/* Initialize module */
mesa_rc mep_init(vtss_init_data_t *data);

const char* vtss_mep_rate_to_string(vtss_appl_mep_rate_t rate);
const char* vtss_mep_flow_to_name(mep_domain_t domain, u32 flow, vtss_uport_no_t port_no, char *str);
const char* vtss_mep_domain_to_string(mep_domain_t domain);
const char* vtss_mep_direction_to_string(vtss_appl_mep_direction_t direction);
const char* vtss_mep_unit_to_string(vtss_appl_mep_dm_tunit_t unit);

/*
 * Start a MEP timer
 */
void vtss_mep_timer_start(vtss::Timer *timer);

/*
 * Cancel a MEP timer
 */
void vtss_mep_timer_cancel(vtss::Timer *timer);

/*
 * MEP engine callback function called by time module - placed in mep.c for critd lock/unlock
 */
void mep_timer_call_back(vtss::Timer *timer);

/*
 * MEP model callback function called by time module - placed in mep.c for critd lock/unlock
 */
void mep_model_timer_call_back(struct vtss::Timer *timer);

}  // mep_g2

#endif /* _MEP_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
