/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

#ifndef _MEP_G2_API_H_
#define _MEP_G2_API_H_

#include "mep_api.h"

namespace mep_g2 {
const char *mep_error_txt(mesa_rc rc);

/****************************************************************************/
/*  MEP module interface                                                    */
/****************************************************************************/

/*
 * Start a MEP timer
 */
void vtss_mep_timer_start(vtss::Timer *timer);

/*
 * Cancel a MEP timer
 */
void vtss_mep_timer_cancel(vtss::Timer *timer);

/*
 * MEP engine callback function called by time module - placed in mep.c for critd lock/unlock
 */
void mep_timer_call_back(vtss::Timer *timer);

/*
 * MEP model callback function called by time module - placed in mep.c for critd lock/unlock
 */
void mep_model_timer_call_back(struct vtss::Timer *timer);

}  // namespace mep_g2
#endif /* _MEP_G2_API_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
