/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable format
 (e.g. HEX file) and only in or with products utilizing the Microsemi switch and
 PHY products.  The source code of the software may not be disclosed, transmitted
 or distributed without the prior written permission of Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all ownership,
 copyright, trade secret and proprietary rights in the software and its source code,
 including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL WARRANTIES
 OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES ARE EXPRESS,
 IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION, WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND NON-INFRINGEMENT.

*/

#ifndef _MEP_LL_PDU_DEFS_H_
#define _MEP_LL_PDU_DEFS_H_

/*
 * MEF 46 Latching Loopback PDU definitions
 */

#define MEP_LL_PDU_MIN_SIZE                     12    // min PDU size excl. TLVs
#define MEP_LL_PDU_TIMER_TLV_SIZE               (1 + 2 + 1 + 4) // Timer TLV size
#define MEP_LL_MESSAGE_TYPE_ACTIVATE            1
#define MEP_LL_MESSAGE_TYPE_DEACTIVATE          2
#define MEP_LL_MESSAGE_TYPE_STATE               3

#define MEP_TLV_TYPE_END                        0       // End TLV
#define MEP_TLV_TYPE_OSPEC                      31      // Organization-Specific TLV
#define MEP_TLV_TYPE_LL                         37      // LL TLV type

#define MEP_TLV_SUBTYPE_LL_EXP_TIMER            1       // LL expiration timer subtype


/*
 * Information used to build LLM or LLR PDUs
 */
typedef struct {
    u8                  message_type;               /* Message type */
    u8                  flags;                      /* Flags */
    u8                  response;                   /* Response code */
    u32                 expiration_timer;           /* Expiration timer value */
    u8                  *tlv_buffer;                /* TLV buffer for reply. Only contains unknown TLVs. */
    u32                 tlv_max_size;               /* Max size of TLV buffer */
    u32                 tlv_size;                   /* Actual size of TLV data in buffer */
} mep_ll_pdu_info_t;


#endif /* _MEP_LL_PDU_DEFS_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
