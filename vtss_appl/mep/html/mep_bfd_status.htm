<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

-->
<html>

<head>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <title>MEP BFDs Status</title>
 <link href="lib/normal.css" rel="stylesheet" type="text/css">
 <script type="text/javascript" src="lib/mootools-core.js"></script>
 <script type="text/javascript" src="lib/dynforms.js"></script>
 <script type="text/javascript" src="lib/validate.js"></script>
 <script type="text/javascript" src="lib/json.js"></script>
 <script type="text/javascript">

// Help page magic
var help_page = "/help/help_mep_bfd_status.htm";

// Global variables
var myDynamicTable, timerID;

var oTBfdType = Array("Independent", "Coordinated");
var oTSourceState = Array("Admin Down", "Down", "Init", "Up");
var oVSourceState = Array("adminDown", "down", "init", "up");
var oTDiagCode = Array("None", "Detect Expired", "Echo Failed", "Remote Down", "Fwd Plane Reset", "Path Down", "Concat Path Down", "Admin Down", "Reverse Concat Down", "Misconnectivity");
var oVDiagCode = Array("none", "detectExpired", "echoFailed", "remoteDown", "fwdPlaneReset", "pathDown", "concatPathDown", "adminDown", "reverseConcatDown", "misconnectivity");

function addHeader(table_rows)
{
    var row;

    // 1st row
    row = {fields:[
             {type:"disp_hdr", params:["Clear", 1, 2]},
             {type:"disp_hdr", params:["MEP #", 1, 2]},
             {type:"disp_hdr", params:["Type", 1, 2]},
             {type:"disp_hdr", params:["CC", 3]},
             {type:"disp_hdr", params:["CV", 3]},
             {type:"disp_hdr", params:["Local", 4]},
             {type:"disp_hdr", params:["Remote", 5]},
             {type:"disp_hdr", params:["Latest Received Unexp. Discr.", 1, 2]},
             {type:"disp_hdr", params:["Exp. Source MEP ID TLV", 1, 2]},
             {type:"disp_hdr", params:["Latest Received Unexp. Source MEP ID TLV", 1, 2]}
            ]
          };
    table_rows.push(row);

    // 2nd row
    row = {fields:[
             {type:"disp_hdr", params:["TX"]},
             {type:"disp_hdr", params:["Valid RX"]},
             {type:"disp_hdr", params:["Invalid RX"]},
             {type:"disp_hdr", params:["TX"]},
             {type:"disp_hdr", params:["Valid RX"]},
             {type:"disp_hdr", params:["Invalid RX"]},
             {type:"disp_hdr", params:["Source State"]},
             {type:"disp_hdr", params:["Sink State"]},
             {type:"disp_hdr", params:["Discr."]},
             {type:"disp_hdr", params:["Diag. Code"]},
             {type:"disp_hdr", params:["State"]},
             {type:"disp_hdr", params:["Discr."]},
             {type:"disp_hdr", params:["Diag. Code"]},
             {type:"disp_hdr", params:["Min. RX Interval"]},
             {type:"disp_hdr", params:["Flags"]}
            ]
          };
    table_rows.push(row);
}

function addRow(key, val, idx)
{
    var empty_src_mep_id = "00000000000000000000000000000000000000000000000000000000";
    var recv_conf = myDynamicTable.getRecvJson("config");
    var conf = recv_conf[idx].val;
    row = {fields:[
            {type:"chkbox", params:[0, "c", "clear_" + key]},
            {type:"link",   params:["cr", "mep_config.htm?mep=" + key, key]},
            {type:"text",   params:[conf.IsIndependent ? oTBfdType[0] : oTBfdType[1], "c"]},
            {type:"digit",  params:[val.CcTxCnt]},
            {type:"digit",  params:[val.ValidCcRxCnt]},
            {type:"digit",  params:[val.InvalidCcRxCnt]},
            {type:"digit",  params:[val.CvTxCnt]},
            {type:"digit",  params:[val.ValidCvRxCnt]},
            {type:"digit",  params:[val.InvalidCvRxCnt]},
            {type:"text",   params:[conf.IsIndependent ? oTSourceState[oVSourceState.indexOf(val.SessionState1)] : "-", "c"]},
            {type:"text",   params:[oTSourceState[oVSourceState.indexOf(val.SessionState0)], "c"]},
            {type:"digit",  params:["0x" + parseInt(val.MyDiscr, 10).toString(16)]},
            {type:"text",   params:[oTDiagCode[oVDiagCode.indexOf(val.LocalDiag)], "c"]},
            {type:"text",   params:[oTSourceState[oVSourceState.indexOf(val.RemoteSessionState)], "c"]},
            {type:"digit",  params:["0x" + parseInt(val.RemoteDiscr, 10).toString(16)]},
            {type:"text",   params:[oTDiagCode[oVDiagCode.indexOf(val.RemoteDiag)], "c"]},
            {type:"digit",  params:[val.RemoteMinRx]},
            {type:"digit",  params:[val.RemoteFlags]},
            {type:"digit",  params:[val.UnexpDiscr]},
            {type:"text",   params:[val.ExpSrcMepIdTlv != empty_src_mep_id ? val.ExpSrcMepIdTlv : "None", "c"]},
            {type:"text",   params:[val.UnexpSrcMepIdTlv != empty_src_mep_id ? val.UnexpSrcMepIdTlv : "None", "c"]}
          ]
         };

    return row;
}

function addRows(recv_json)
{
    var table_rows = new Array();
    var row, empty_colspan = 21;

    addHeader(table_rows);

    // Add data rows
    Object.each(recv_json, function(record, idx) {
        table_rows.push(addRow(record.key, record.val, idx));
    });

    // Empty row
    if (!recv_json.length) {
        table_rows.push({fields:[{type:"empty_row", params:[empty_colspan]}]});
    }

    return table_rows;
}

function processUpdate(recv_json)
{
    // Ignore the process if no data is received
    if (!recv_json) {
        alert("Get dynamic data failed.");
        return;
    }

    // The config and status entry count should be the same
    if (recv_json.length != myDynamicTable.getRecvJson("config").length) {
        alert("Get dynamic data failed.");
        recv_json = [];
    }

    // Save the received JSON data
    myDynamicTable.saveRecvJson("status", recv_json);

    // Add table rows
    myDynamicTable.addRows(addRows(recv_json));

    // Update this dynamic table
    myDynamicTable.update();

    // Refresh timer
    var autorefresh = document.getElementById("autorefresh");
    if (autorefresh && autorefresh.checked) {
        if (timerID) {
            clearTimeout(timerID);
        }
        timerID = setTimeout('requestUpdate()', settingsRefreshInterval());
    }
}

function prepareUpdate(recv_json, name)
{
    // Save the received JSON data
    myDynamicTable.saveRecvJson(name, recv_json);

    // This table requires two JSON data.
    if (myDynamicTable.getRecvJsonCnt() == 2) {
        processUpdate(myDynamicTable.getRecvJson("status"));
    }
}

function requestUpdate()
{
    // Restore table content
    myDynamicTable.restore();

    // This table requires two JSON data(config/status).
    requestJsonDoc("mep.config.bfd.get", null, prepareUpdate, "config");
    requestJsonDoc("mep.status.bfd.get", null, prepareUpdate, "status");
}

function checkRefresh(fld)
{
    if (fld.checked) {
        requestUpdate();
    } else if (timerID) {
        clearTimeout(timerID);
        timerID = null;
    }
}

function processClear(recv_json, req_params)
{
    var clear_keys = [];

    Object.each(recv_json, function(record) {
        var key = record.key;
        if (req_params == "all") {
            clear_keys.push(key);
        } else {
            var fld = document.getElementById("clear_" + key);
            if (fld && fld.checked) {
                clear_keys.push(key);
            }

        }
    });

    // Clear statistics by each entry
    Object.each(clear_keys, function(key) {
        var submit_json = [key, {"Clear":true}];
        requestJsonDoc("mep.control.bfd.set", submit_json, clear_keys.indexOf(key) == (clear_keys.length  - 1) ? requestUpdate : null);
    });

    // Reload page again
    if (!clear_keys.length) {
        requestUpdate();
    }
}

function requestClear(req_params)
{
    requestJsonDoc("mep.control.bfd.get", null, processClear, req_params);
}

window.addEvent('domready', function() {
    // Create a from with table body for receive/transmit JSON data
    myDynamicTable = new DynamicTable("myTableContent", "display");

    requestUpdate();
});
 </script>
</head>

<body class="content">
<h1>MEP BFDs Status</h1>

<div class="refreshbar">
  <label for="autorefresh">Auto-refresh</label>
  <input type="checkbox" name="autorefresh" id="autorefresh" onclick="checkRefresh(this);">
  <input type="button" value="Refresh"  onClick="requestUpdate();      this.blur();">
  <input type="button" value="Clear"    onClick="requestClear();       this.blur();">
  <input type="button" value="ClearAll" onClick="requestClear('all');  this.blur();">
  <img id="update" alt="" src="images/updating.gif">
</div>

<div id="myTableContent"></div>

</body>
</html>
