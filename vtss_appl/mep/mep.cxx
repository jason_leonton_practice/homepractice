/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#define MEP_IMPL_G2

#include "mep.h"
#include <vtss/basics/vector.hxx>
#include "mep_trace.h"
#include "conf_api.h"
#include "critd_api.h"
#include "vtss/appl/mep.h"
#include "vtss_tod_api.h"
#include "vlan_api.h"
#include "port_api.h"
#include "interrupt_api.h"
#include "mep_model.h"
#include "mep_engine.h"
#include "mep_expose.h"
#include "mep_peer_array.hxx"
#include <vtss_module_id.h>









#if defined(VTSS_SW_OPTION_EPS)
#include "eps_api.h"
#endif

#if defined(VTSS_SW_OPTION_ERPS)
#include "erps_api.h"
#endif

#ifdef VTSS_SW_OPTION_ICFG
#include "mep_icli_functions.h"
#endif

#ifdef VTSS_SW_OPTION_SYSLOG
#include "syslog_api.h"
#endif

#ifdef VTSS_SW_OPTION_QOS
#include "qos_api.h"
#endif


/****************************************************************************/
/*  Global variables                                                        */
/****************************************************************************/

namespace mep_g2 {

static critd_t       crit;        /* MEP critd */

#if (VTSS_TRACE_ENABLED)
static vtss_trace_reg_t trace_reg =
{
    VTSS_TRACE_MODULE_ID, "MEP", "MEP module."
};

static vtss_trace_grp_t trace_grps[TRACE_GRP_CNT] =
{
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CRIT */ {
        "crit",
        "Critical regions ",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_APS_TX */ {
        "txAPS",
        "Tx APS print out ",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_APS_RX */ {
        "rxAPS",
        "Rx APS print out ",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_TX_FRAME */ {
        "txFRAME",
        "Tx frame print out ",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_RX_FRAME */ {
        "rxFRAME",
        "Rx frame print out ",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_DM */ {
        "dm",
        "DM print out",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_LM */ {
        "lm",
        "LM print out",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_TST */ {
        "tst",
        "TST print out",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_LB */ {
        "lb",
        "LB print out",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_AIS_LCK */ {
        "ais-lck",
        "AIS and LCK print out",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_MODEL */ {
        "model",
        "Model print out",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_EVENT */ {
        "event",
        "Event print out",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_TIMER */ {
        "timer",
        "Timer print out",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_ISDX */ {
        "isdx",
        "ISDX print out",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_SSF */ {
        "ssf",
        "Server signal failure",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CALLBACK */ {
        "callb",
        "Call back",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CALLOUT */ {
        "callo",
        "Call out",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_TEMP */ {
        "temp",
        "Temporary",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
};
#define CRIT_ENTER critd_enter(&crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define CRIT_EXIT  critd_exit( &crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#else
#define CRIT_ENTER critd_enter(&crit)
#define CRIT_EXIT  critd_exit( &crit)
#endif /* VTSS_TRACE_ENABLED */

struct CritdEnterExit {
    CritdEnterExit(int line) {
        critd_enter(&crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, line);
        enterLine = line;
    }

    ~CritdEnterExit() {
        critd_exit(&crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, enterLine);
    }

    u32 enterLine;
};
#define CRIT_ENTER_EXIT() CritdEnterExit __lock_guard__(__LINE__)

struct CritdExitEnter {
    CritdExitEnter(int line) {
        critd_exit(&crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, line);
        exitLine = line;
    }

    ~CritdExitEnter() {
        critd_enter(&crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, exitLine);
    }

    u32 exitLine;
};
#define CRIT_EXIT_ENTER() CritdExitEnter __lock_guard__(__LINE__)

#define APS_INST_INV        0xFFFF
#define SF_EPS_MAX          (MEP_EPS_MAX-1)

typedef struct
{
    mesa_port_list_t           port;                             /* Port to be used by MEP */
    u32                        ssf_count;                        /* Number of related EPS (SF_EPS_MAX)            */
    u16                        ssf_inst[SF_EPS_MAX];             /* SF/SD state to all    */
    u16                        aps_inst;
    vtss_appl_mep_aps_type_t   aps_type;
    mep_eps_type_t             eps_type;
    mep_domain_t               domain;
    u32                        flow;
    u32                        res_port;
    BOOL                       enable;
    u16                        vid;
    BOOL                       syslog_control_flag;

    u8                         aps_txdata[MEP_APS_DATA_LENGTH];
    BOOL                       raps_tx;
    BOOL                       raps_forward;
} instance_data_t;

static CapArray<instance_data_t, VTSS_APPL_CAP_MEP_INSTANCE_MAX> instance_data;
static mesa_port_list_t                  los_state;
static mesa_port_list_t                  conv_los_state;
static CapArray<u32 , MESA_CAP_PORT_CNT> in_port_conv;
static CapArray<u32 , MESA_CAP_PORT_CNT> out_port_conv;
static mesa_port_list_t                  out_port_1p1;
static mesa_port_list_t                  out_port_tx;
static BOOL             trace_init = FALSE;

static mesa_rc conv_conf_ext_int(const vtss_appl_mep_conf_t *a, mep_conf_t *b);
static void conv_conf_int_ext(const mep_conf_t *a, vtss_appl_mep_conf_t *b);
static void conv_def_int_ext(const mep_default_conf_t *a, vtss_appl_mep_default_conf_t *b);
static mesa_rc conv_mep_client_flow_conf_ext_to_int(vtss_ifindex_t ifindex, u32 *flow, mep_domain_t *domain);
static mesa_rc conv_mep_client_flow_conf_int_to_ext(u32 flow, mep_domain_t domain, vtss_ifindex_t *ifindex);
static void restore_to_default(void);
static BOOL known_peer_mep_id(u32 peer,  mep_conf_t *config);


const char *mep_error_txt(mesa_rc error)
{
    switch (error) {
        case VTSS_APPL_MEP_RC_APS_PROT_CONNECTED:     return "Mismatch between assosiated protection type and APS type";
        case VTSS_APPL_MEP_RC_INVALID_PARAMETER:      return "Invalid parameter error returned from MEP";
        case VTSS_APPL_MEP_RC_NOT_ENABLED:            return "MEP instance is not enabled";
        case VTSS_APPL_MEP_RC_NOT_CREATED:            return "MEP instance has not been created";
        case VTSS_APPL_MEP_RC_RATE_INTERVAL:          return "The selected frame rate or interval is invalid";
        case VTSS_APPL_MEP_RC_PEER_CNT:               return "Invalid number of peer's for this configuration";
        case VTSS_APPL_MEP_RC_PEER_ID:                return "Invalid peer MEP ID";
        case VTSS_APPL_MEP_RC_MIP:                    return "Not allowed on a MIP";
        case VTSS_APPL_MEP_RC_INVALID_EVC:            return "EVC flow was found invalid";
        case VTSS_APPL_MEP_RC_INVALID_PRIO:           return "Priority was found invalid";
        case VTSS_APPL_MEP_RC_APS_UP:                 return "APS not allowed on EVC UP MEP";
        case VTSS_APPL_MEP_RC_APS_DOMAIN:             return "R-APS not allowed in this domain";
        case VTSS_APPL_MEP_RC_INVALID_VID:            return "The VID is invalid or VLAN is not created for this VID or attribut 'VID' is illegal in this domain";
        case VTSS_APPL_MEP_RC_INVALID_COS_ID:         return "Invalid COS ID (priority) for this EVC";
        case VTSS_APPL_MEP_RC_NO_VOE:                 return "No VOE available";
        case VTSS_APPL_MEP_RC_NO_TIMESTAMP_DATA:      return "There is no DMR time-stamp data available";
        case VTSS_APPL_MEP_RC_PEER_MAC:               return "Peer Unicast MAC must be known to do this";
        case VTSS_APPL_MEP_RC_INVALID_INSTANCE:       return "Invalid MEP instance ID";
        case VTSS_APPL_MEP_RC_INVALID_MEG:            return "Invalid MEG-ID or IEEE Name";
        case VTSS_APPL_MEP_RC_PROP_SUPPORT:           return "Proprietary DM is not supported";
        case VTSS_APPL_MEP_RC_VOLATILE:               return "Cannot change volatile entry";
        case VTSS_APPL_MEP_RC_VLAN_SUPPORT:           return "VLAN domain is not supported";
        case VTSS_APPL_MEP_RC_CLIENT_MAX_LEVEL:       return "The MEP is on MAX level (7) - it is not possible to have a client on higher level";
        case VTSS_APPL_MEP_RC_INVALID_CLIENT_LEVEL:   return "The client level is invalid";
        case VTSS_APPL_MEP_RC_MIP_SUPPORT:            return "This MIP is not supported";
        case VTSS_APPL_MEP_RC_INVALID_MAC:            return "On this platform the selected MAC is invaled";
        case VTSS_APPL_MEP_RC_CHANGE_PARAMETER:       return "Some parameters are not allowed to change on an enabled instance";
        case VTSS_APPL_MEP_RC_NO_LOOP_PORT:           return "There is no loop port - this is required to create Up-MEP";
        case VTSS_APPL_MEP_RC_INVALID_PORT:           return "The residence port is not valid in this domain or for this MEP type";
        case VTSS_APPL_MEP_RC_INVALID_HW_CCM:         return "All MEP in same classified VID and same MEG level must have same HW CCM state (above or below 100 f/s)";
        case VTSS_APPL_MEP_RC_LST:                    return "Link State Tracking cannot be enabled as either CC or CC-TLV is not enabled";
        case VTSS_APPL_MEP_RC_NNI_PORT:               return "The MEP must be configured on a EVC NNI port";
        case VTSS_APPL_MEP_RC_TEST:                   return "TST and LBM cannot be enabled at the same time";
        case VTSS_APPL_MEP_RC_E_TREE:                 return "Invalid MEP configuration for E-TREE EVC";
        case VTSS_APPL_MEP_RC_INVALID_MEP_CONFIG:     return "Invalid MEP configuration. Like Port Domain Up-MEP";
        case VTSS_APPL_MEP_RC_INVALID_LEVEL:          return "Invalid MEG Level";
        case VTSS_APPL_MEP_RC_INVALID_MEP:            return "Invalid MEP-ID";
        case VTSS_APPL_MEP_RC_NO_MIP:                 return "No MIP available";
        case VTSS_APPL_MEP_RC_INVALID_DOMAIN:         return "The domain is not supported";
        case VTSS_APPL_MEP_RC_RESOURCE_CONF_FAILED:   return "Resource configuration failed";
        case VTSS_APPL_MEP_RC_HW_SUPPORT_REQUIRED:    return "HW (VOP/MIP) support is required";
        case VTSS_APPL_MEP_RC_UP_SUPPORT:             return "UP MEP/MIP is not supported in this domain";
        case VTSS_APPL_MEP_RC_DOWN_MAX:               return "MAX number of Down-MEPs is exceeded in this flow";
        case VTSS_APPL_MEP_RC_LAST_N:                 return "The 'last N' numer of delay measurement for avarage calculation is invalid";
        case VTSS_APPL_MEP_RC_T_UNIT:                 return "The 'Time unit' of delay measurement is invalid";
        case VTSS_APPL_MEP_RC_OVERFLOW:               return "The 'Overflow Action' of delay measurement is invalid";
        case VTSS_APPL_MEP_RC_FD_BIN:                 return "The number of 'FD Bins' is invalid";
        case VTSS_APPL_MEP_RC_IFDV_BIN:               return "The number of 'IFDV Bins' is invalid";
        case VTSS_APPL_MEP_RC_M_THRESHOLD:            return "The binning measurement threshold is invalid";
        case VTSS_APPL_MEP_RC_MEP_MAX:                return "Max number of MEPs has been exceeded. Like too many Down or Up-MEPs on same port";
        case VTSS_APPL_MEP_RC_INVALID_WORK_PORT:      return "Invalid working port ID";
        case VTSS_APPL_MEP_RC_MIP_MAX:                return "Max number of MIPs has been exceeded. Like too many Down or Up-MIPs on same port";
        case VTSS_APPL_MEP_RC_STATE_NOT_UP:           return "The MEPs operational state is not up";
        case VTSS_APPL_MEP_RC_TTL_ZERO:               return "TTL=0 is not allowed";
        case VTSS_APPL_MEP_RC_LM_DUAL_ENDED_RATE_EXCEED_CC_RATE: return "LM DUAL_ENDED rate cannot be higher than CC rate";
        case VTSS_APPL_MEP_RC_OUT_OF_TX_RESOURCE:     return "There are no free transmission resources";
        case VTSS_APPL_MEP_RC_TX_ERROR:               return "Transmitting OAM frame failed due to system error";
        case VTSS_APPL_MEP_RC_UP_CLIENT:              return "Up-MEP is not allowed to do client injection";
        case VTSS_APPL_MEP_RC_INVALID_LB_FRM_SIZE:    return "The framesize for LB function is invalid";
        case VTSS_APPL_MEP_RC_INVALID_TST_FRM_SIZE:   return "The framesize for TST function is invalid";
    }

    return "Unknown error code";
}




/****************************************************************************/
/*  MEP management interface                                                */
/****************************************************************************/
void mep_default_conf_get(mep_default_conf_t *const def_conf)
{
    if (trace_init) {
        CRIT_ENTER_EXIT();
    }
    vtss_mep_mgmt_default_conf_get(def_conf);
}

void vtss_appl_mep_default_conf_get(vtss_appl_mep_default_conf_t  *const def_conf) {
    mep_default_conf_t c;

    mep_default_conf_get(&c);
    conv_def_int_ext(&c, def_conf);
}

mesa_rc vtss_appl_mep_tlv_conf_set(const vtss_appl_mep_tlv_conf_t  *const conf)
{
    mesa_rc rc;

    T_N("Enter");

    if (conf == NULL)        return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_tlv_conf_set(conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_tlv_conf_set failed  rc %s", error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tlv_conf_get(vtss_appl_mep_tlv_conf_t  *const conf)
{
    mesa_rc rc;

    T_N("Enter");

    if (conf == NULL)        return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_tlv_conf_get(conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_tlv_conf_get failed  rc %s", error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc mep_status_eps_get(const u32       instance,
                           mep_eps_state_t *const state)
{
    u32 i, inx, aps_count = 0;

    T_N("Enter  instance %u", instance);

    if (state == NULL)                               return VTSS_RC_OK;
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER_EXIT();
    if (instance_data[instance].aps_inst != APS_INST_INV) {
        state->inst[0] = instance_data[instance].aps_inst;   /* APS EPS instance number is first */
        aps_count = 1;
    }
    for (i = 0, inx = aps_count; i < instance_data[instance].ssf_count; i++) /* SSF EPS instance numbers - but not if also APS instance */ {
        if ((aps_count == 0) || (instance_data[instance].ssf_inst[i] != instance_data[instance].aps_inst)) {
            state->inst[inx] = instance_data[instance].ssf_inst[i];
            inx++;
        }
    }
    state->count = inx;

    return (VTSS_RC_OK);
}

mesa_rc mep_dm_timestamp_get(const u32           instance,
                             mep_dm_timestamp_t  *const dm1_timestamp_far_to_near,
                             mep_dm_timestamp_t  *const dm1_timestamp_near_to_far)
{
    mesa_rc rc;

    if ((dm1_timestamp_far_to_near == NULL) || (dm1_timestamp_near_to_far == NULL))       return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_dm_timestamp_get(instance, dm1_timestamp_far_to_near, dm1_timestamp_near_to_far)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_timestamp_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc mep_volatile_conf_set(const u32         instance,
                              const mep_conf_t *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return VTSS_APPL_MEP_RC_INVALID_INSTANCE;
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_volatile_conf_set(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_volatile_conf_set failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    instance_data[instance].enable = conf->enable;
    if (conf->enable)
    {
        instance_data[instance].domain = conf->domain;
        instance_data[instance].flow = conf->flow;
        instance_data[instance].res_port = conf->port;
        instance_data[instance].vid = conf->vid;
    } else
        instance_data[instance].aps_type = VTSS_APPL_MEP_INV_APS;

    return VTSS_RC_OK;
}


mesa_rc mep_volatile_sat_conf_set(const u32              instance,
                                  const mep_sat_conf_t   *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return VTSS_APPL_MEP_RC_INVALID_INSTANCE;
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_volatile_sat_conf_set(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_volatile_sat_conf_set failed Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    instance_data[instance].enable = conf->conf.enable;
    if (conf->conf.enable)
    {
        instance_data[instance].domain = conf->conf.domain;
        instance_data[instance].flow = conf->conf.flow;
        instance_data[instance].res_port = conf->conf.port;
        instance_data[instance].vid = conf->conf.vid;
    }
    else
        instance_data[instance].aps_type = VTSS_APPL_MEP_INV_APS;

    return VTSS_RC_OK;
}

mesa_rc mep_volatile_sat_conf_get(const u32        instance,
                                  mep_sat_conf_t   *const conf)
{
    T_N("Enter  instance %u  conf %p", instance, conf);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return VTSS_APPL_MEP_RC_INVALID_INSTANCE;
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    return(vtss_mep_mgmt_volatile_sat_conf_get(instance, conf));
}


mesa_rc mep_volatile_sat_counter_get(const u32           instance,
                                     mep_sat_counters_t  *const counters)
{
    mesa_rc rc;

    T_N("Enter  instance %u  counters %p", instance, counters);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return VTSS_APPL_MEP_RC_INVALID_INSTANCE;
    if (counters == NULL)                            return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_volatile_sat_counter_get(instance, counters)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_volatile_sat_counter_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc mep_volatile_sat_counter_clear(const u32  instance)
{
    mesa_rc rc;

    T_N("instance %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return VTSS_APPL_MEP_RC_INVALID_INSTANCE;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_volatile_sat_counter_clear_set(instance)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_volatile_sat_counter_clear_set failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc mep_tx_bps_actual_get(const u32     instance,
                              mep_tx_bps_t  *const bps)
{
    mesa_rc rc;

    T_N("instance %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return VTSS_APPL_MEP_RC_INVALID_INSTANCE;
    if (bps == NULL)                                 return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    memset(bps, 0, sizeof(*bps));
    if ((rc = vtss_mep_mgmt_volatile_sat_bps_act_get(instance,  bps)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_volatile_sat_bps_act_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc mep_slm_counters_get(const u32            instance,
                             const u32            peer_idx,
                             sl_service_counter_t *const counters)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return VTSS_APPL_MEP_RC_INVALID_INSTANCE;

    CRIT_ENTER_EXIT();
    if (vtss_mep_mgmt_slm_counters_get(instance, peer_idx, counters) != VTSS_RC_OK)
        return VTSS_RC_ERROR;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_capabilities_get(vtss_appl_mep_capabilities_t *const conf)
{
    T_N("conf %p", conf);

    conf->instance_max = VTSS_APPL_MEP_INSTANCE_MAX;
    conf->peer_max = mep_caps.mesa_oam_peer_cnt;
    conf->transaction_max = VTSS_APPL_MEP_TRANSACTION_MAX;
    conf->reply_max = VTSS_APPL_MEP_REPLY_MAX;
    conf->client_flows_max = VTSS_APPL_MEP_CLIENT_FLOWS_MAX;
    conf->mep_id_max = VTSS_APPL_MEP_ID_MAX;
    conf->mac_length = VTSS_APPL_MEP_MAC_LENGTH;
    conf->meg_code_length = VTSS_APPL_MEP_MEG_CODE_LENGTH;
    conf->dm_interval_min = VTSS_APPL_MEP_DM_INTERVAL_MIN;
    conf->dm_interval_max = VTSS_APPL_MEP_DM_INTERVAL_MAX;
    conf->dm_lastn_min = VTSS_APPL_MEP_DM_LASTN_MIN;
    conf->dm_lastn_max = VTSS_APPL_MEP_DM_LASTN_MAX;
    conf->dm_bin_max = VTSS_APPL_MEP_DM_BINNING_MAX;
    conf->dm_bin_fd_max = VTSS_APPL_MEP_DM_BINS_FD_MAX;
    conf->dm_bin_fd_min = VTSS_APPL_MEP_DM_BINS_FD_MIN;
    conf->dm_bin_ifdv_max = VTSS_APPL_MEP_DM_BINS_IFDV_MAX;
    conf->dm_bin_ifdv_min = VTSS_APPL_MEP_DM_BINS_IFDV_MIN;
    conf->lbm_size_max = VTSS_APPL_MEP_LBM_SIZE_MAX;
    conf->lbm_size_min = VTSS_APPL_MEP_LBM_SIZE_MIN;
    conf->lbm_mpls_size_max = VTSS_APPL_MEP_LBM_MPLS_SIZE_MAX;
    conf->tst_size_max = VTSS_APPL_MEP_TST_SIZE_MAX;
    conf->tst_size_min = VTSS_APPL_MEP_TST_SIZE_MIN;
    conf->tst_rate_max = VTSS_APPL_MEP_TST_RATE_MAX;
    conf->tst_mpls_size_max = VTSS_APPL_MEP_TST_MPLS_SIZE_MAX;
    conf->test_flow_max = VTSS_APPL_MEP_TEST_MAX;
    conf->slm_size_max = VTSS_APPL_MEP_SLM_SIZE_MAX;
    conf->slm_size_min = VTSS_APPL_MEP_SLM_SIZE_MIN;
    conf->client_prio_highest = VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST;
    conf->has_itu_g8113_2 = VTSS_APPL_MEP_ITU_8113_2_CAPABILITY;
    conf->has_voe_support = VTSS_APPL_MEP_VOE_SUPPORTED;
    conf->lb_to_send_infinite = VTSS_APPL_MEP_LB_TO_SEND_INFINITE;
    conf->bfd_auth_key_max = VTSS_APPL_MEP_BFD_AUTH_KEYS_MAX;
    conf->has_evc_pag = VTSS_APPL_MEP_EVC_PAG_CAPABILITY;
    conf->has_evc_qos = VTSS_APPL_MEP_EVC_QOS_CAPABILITY;
    conf->has_flow_count = VTSS_APPL_MEP_FLOW_COUNT_CAPABILITY;
    conf->has_oam_count = VTSS_APPL_MEP_OAM_COUNT_CAPABILITY;
    conf->has_oper_state = mep_caps.appl_mep_oper_state;
    conf->has_oam_sw_support = VTSS_APPL_MEP_SW_OAM_SUPPORT;

    return (VTSS_RC_OK);
}

mesa_rc mep_instance_conf_set(const u32 instance, const mep_conf_t *const conf)
{



    mesa_rc rc;

    T_N("instance %u  conf %p  enable %u", instance, conf, (conf != NULL) ? conf->enable : 0xFFFFFFFF);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return VTSS_APPL_MEP_RC_INVALID_INSTANCE;
    if (conf == NULL)                                return VTSS_RC_OK;

    // We need a non-const version (lconf) to update the port parameter
    mep_conf_t lconf = *conf;









    if (lconf.domain == VTSS_APPL_MEP_MPLS_LINK   ||
        lconf.domain == VTSS_APPL_MEP_MPLS_TUNNEL ||
        lconf.domain == VTSS_APPL_MEP_MPLS_PW     ||
        lconf.domain == VTSS_APPL_MEP_MPLS_LSP) {
        T_D("MPLS domain not supported");
        return (VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    }


    CRIT_ENTER;
    if ((rc = vtss_mep_mgmt_conf_set(instance, &lconf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_conf_set failed  Instance %u  rc %s", instance, error_txt(rc));
        CRIT_EXIT;
        return rc;
    }

    instance_data[instance].enable = lconf.enable;
    if (lconf.enable) {
        instance_data[instance].domain = lconf.domain;
        instance_data[instance].flow = lconf.flow;
        instance_data[instance].res_port = lconf.port;
        instance_data[instance].vid = lconf.vid;
    } else
        instance_data[instance].aps_type = VTSS_APPL_MEP_INV_APS;

    CRIT_EXIT;








    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_instance_conf_set(u32 instance, const vtss_appl_mep_conf_t *const conf)
{
    mep_conf_t c = {0};

    T_N("instance %u", instance);

    VTSS_RC(conv_conf_ext_int(conf, &c));
    return mep_instance_conf_set(instance, &c);
}

mesa_rc mep_instance_conf_get(const u32 instance, mep_conf_t *const conf)
{
    mesa_rc rc;

//    T_N("instance %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)     return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                               return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if (((rc = vtss_mep_mgmt_conf_get(instance, conf)) != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_NOT_CREATED)) {
        T_D("vtss_mep_mgmt_conf_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_instance_conf_get(const u32 instance, vtss_appl_mep_conf_t *const conf) {
    mep_conf_t c;
    mesa_rc    rc;

    T_N("instance %u  conf %p", instance, conf);
    rc = mep_instance_conf_get(instance, &c);
    (void)conv_conf_int_ext(&c, conf);

    return rc;
}

mesa_rc mep_instance_conf_add(const u32 instance, const mep_conf_t *const conf)
{
    mep_conf_t  config;

    T_N("instance %u", instance);

    if (conf == NULL)                                return VTSS_RC_OK;
    config = *conf;
    config.enable = TRUE;
    if (mep_instance_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    T_N("OK");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_instance_conf_add(u32 instance, const vtss_appl_mep_conf_t *const conf)
{
    mep_conf_t c = {0};

    VTSS_RC(conv_conf_ext_int(conf, &c));
    return mep_instance_conf_add(instance, &c);
}

mesa_rc vtss_appl_mep_instance_conf_delete(const u32  instance)
{
    mep_conf_t  config;

    T_N("instance %u", instance);

    if ((mep_instance_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (mep_instance_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    T_N("OK\n");
    return (VTSS_RC_OK);
}


mesa_rc vtss_appl_mep_instance_conf_iter(const u32    *const instance,
                                         u32          *const next)
{
    CRIT_ENTER_EXIT();
    return vtss_mep_mgmt_get_next_enabled_instance(instance, next);
}


mesa_rc vtss_appl_mep_instance_status_iter(const u32    *const instance,
                                           u32          *const next)
{
    vtss_appl_mep_state_t status;

    if (!instance) { // get-first
        return mep_status_get_first(next, &status);
    } else {
        *next = *instance;
        return mep_status_get_next(next, &status);
    }
}


mesa_rc vtss_appl_mep_lm_notif_status_iter(const u32    *const instance,
                                           u32          *const next)
{
    vtss_appl_mep_lm_notif_state_t status;

    if (!instance) { // get-first
        return mep_status_lm_notif_get_first(next, &status);
    } else {
        *next = *instance;
        return mep_status_lm_notif_get_next(next, &status);
    }
}


mesa_rc vtss_appl_mep_lm_hli_status_iter(const u32    *const instance,
                                         u32          *const next_instance,
                                         const u32    *const peer,
                                         u32          *const next_peer)
{
    vtss_appl_mep_lm_hli_state_t status;

    if (!instance) { // get-first
        return mep_status_lm_hli_get_first(next_instance, next_peer, &status);
    } else {
        *next_instance = *instance;
        *next_peer = *peer;
        return mep_status_lm_hli_get_next(next_instance, next_peer, &status);
    }
}


mesa_rc vtss_appl_mep_instance_peer_status_iter(const u32  *const instance,
                                                u32        *const next_instance,
                                                const u32  *const peer,
                                                u32        *const next_peer)
{
    vtss_appl_mep_peer_state_t  status;

    if (!instance)  // get-first
        return mep_status_peer_get_first(next_instance, next_peer, &status);

    *next_instance = *instance;
    if (!peer) {  // get-first level-2
        *next_peer = 0;
        if (mep_status_peer_get(*next_instance, *next_peer, &status) == VTSS_RC_OK) {
            return VTSS_RC_OK;
        }
        return(mep_status_peer_get_next(next_instance, next_peer, &status));
    }

    *next_peer = *peer;
    return mep_status_peer_get_next(next_instance, next_peer, &status);
}


mesa_rc vtss_appl_mep_instance_peer_conf_add(const u32                        instance,
                                             const u32                        peer,
                                             const vtss_appl_mep_peer_conf_t  *const conf)
{
    mep_conf_t  config;
    u32 i;

    T_N("instance %u  peer %u", instance, peer);

    if ((mep_instance_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    if (!config.enable)  /* MEP must be created */
        return (VTSS_RC_ERROR);

    if (config.peer_count >= mep_caps.mesa_oam_peer_cnt)  /* Not room for any more peer MEP */
        return (VTSS_RC_ERROR);

    for (i=0; i<config.peer_count; ++i) /* Search for peer MEP-ID */
        if (config.peer_mep[i] == peer)
            break;

    if (i < config.peer_count)  /* Peer MEP-ID was found */
        return (VTSS_RC_ERROR);

    config.peer_mep[i] = peer;
    config.peer_mac[i] = conf->mac;
    config.peer_count += 1;

    if (mep_instance_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}


mesa_rc vtss_appl_mep_instance_peer_conf_delete(const u32  instance,
                                                const u32  peer)
{
    mep_conf_t  config;
    u32                   i, idx;
    MepPeerArray<u16> peer_mep;
    MepPeerArray<mesa_mac_t> peer_mac;

    T_N("instance %u  peer %u", instance, peer);

    if ((mep_instance_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    if (!config.enable)  /* MEP must be created */
        return (VTSS_RC_ERROR);

    peer_mep.clear();
    peer_mac.clear();

    for (i=0, idx=0; i<config.peer_count; ++i) {
        if (config.peer_mep[i] != peer) {
            peer_mep[idx] = config.peer_mep[i];
            peer_mac[idx] = config.peer_mac[i];
            idx++;
        }
    }

    memcpy(config.peer_mep, peer_mep.data(), peer_mep.mem_size());
    memcpy(config.peer_mac, peer_mac.data(), peer_mac.mem_size());
    config.peer_count = idx;

    if (mep_instance_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}


mesa_rc vtss_appl_mep_instance_peer_conf_set(const u32                        instance,
                                             const u32                        peer,
                                             const vtss_appl_mep_peer_conf_t  *const conf)
{
    mep_conf_t  config;
    u32 i;

    T_N("instance %u  peer %u", instance, peer);

    if ((mep_instance_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    if (!config.enable)  /* MEP must be created */
        return (VTSS_RC_ERROR);

    for (i=0; i<config.peer_count; ++i) /* Search for peer MEP-ID */
        if (config.peer_mep[i] == peer)
            break;

    if (i < config.peer_count) { /* Peer MEP-ID was  found */
        config.peer_mac[i] = conf->mac; /* Configure MAC */

        if (mep_instance_conf_set(instance, &config) != VTSS_RC_OK)
            return (VTSS_RC_ERROR); /* Not able to set MEP */
    } else /* Did not find peer MEP-ID */
        return (VTSS_RC_ERROR);

    return (VTSS_RC_OK);
}


mesa_rc vtss_appl_mep_instance_peer_conf_get(const u32                  instance,
                                             const u32                  peer,
                                             vtss_appl_mep_peer_conf_t  *const conf)
{
    mep_conf_t  config;
    u32 i;

    T_N("instance %u  peer %u", instance, peer);

    if ((mep_instance_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    for (i=0; i<config.peer_count; ++i) {/* Search for peer MEP-ID */
        if (config.peer_mep[i] == peer)
            break;
    }

    if (i < config.peer_count) { /* Peer MEP-ID was  found */
        conf->mac = config.peer_mac[i];
    } else /* Did not find peer MEP-ID */
        return (VTSS_RC_ERROR);

    return (VTSS_RC_OK);
}


mesa_rc vtss_appl_mep_instance_peer_conf_iter(const u32    *const instance,
                                              u32          *const next_instance,
                                              const u32    *const peer,
                                              u32          *const next_peer)
{
    CRIT_ENTER_EXIT();
    return vtss_mep_mgmt_get_next_enabled_instance_peer(instance, next_instance, peer, next_peer);
}

mesa_rc vtss_appl_mep_pm_conf_set(const u32                      instance,
                                  const vtss_appl_mep_pm_conf_t  *const conf)
{
    mesa_rc rc;

    T_N("instance %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_pm_conf_set(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_pm_conf_set failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_pm_conf_get(const u32                 instance,
                                  vtss_appl_mep_pm_conf_t   *const conf)
{
    mesa_rc rc;

    T_N("instance %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_pm_conf_get(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_pm_conf_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_pm_conf_iter(const u32    *const instance,
                                   u32          *const next)
{
    mep_conf_t               config;
    vtss_appl_mep_pm_conf_t  pm_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for PM active */
            continue;
        if ((vtss_appl_mep_pm_conf_get(*next, &pm_config)) != VTSS_RC_OK)
            continue;
        if (pm_config.enable)  /* If PM is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}


mesa_rc vtss_appl_mep_lst_conf_set(const u32                      instance,
                                   const vtss_appl_mep_lst_conf_t *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lst_conf_set(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lst_conf_set failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lst_conf_get(const u32                  instance,
                                   vtss_appl_mep_lst_conf_t   *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lst_conf_get(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lst_conf_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lst_conf_iter(const u32    *const instance,
                                    u32          *const next)
{
    vtss_appl_mep_conf_t      config;
    vtss_appl_mep_lst_conf_t  lst_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((vtss_appl_mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for LST active */
            continue;
        if ((vtss_appl_mep_lst_conf_get(*next, &lst_config)) != VTSS_RC_OK)
            continue;
        if (lst_config.enable)  /* If LST is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}


mesa_rc vtss_appl_mep_syslog_conf_set(const u32                          instance,
                                      const vtss_appl_mep_syslog_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_syslog_conf_set(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_syslog_conf_set failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_syslog_conf_get(const u32                 instance,
                                      vtss_appl_mep_syslog_conf_t   *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_syslog_conf_get(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_syslog_conf_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_syslog_conf_iter(const u32    *const instance,
                                       u32          *const next)
{
    mep_conf_t                   config;
    vtss_appl_mep_syslog_conf_t  syslog_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for PM active */
            continue;
        if ((vtss_appl_mep_syslog_conf_get(*next, &syslog_config)) != VTSS_RC_OK)
            continue;
        if (syslog_config.enable)  /* If Syslog is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}


mesa_rc vtss_appl_mep_cc_conf_set(const u32                        instance,
                                  const vtss_appl_mep_cc_conf_t    *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_cc_conf_set(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_cc_conf_set failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_cc_conf_get(const u32                 instance,
                                  vtss_appl_mep_cc_conf_t   *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_cc_conf_get(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_cc_conf_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_cc_conf_add(const u32                       instance,
                                  const vtss_appl_mep_cc_conf_t  *const conf)
{
    vtss_appl_mep_cc_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_cc_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_cc_conf_delete(const u32  instance)
{
    vtss_appl_mep_cc_conf_t  config;

    if ((vtss_appl_mep_cc_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_cc_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_cc_conf_iter(const u32    *const instance,
                                   u32          *const next)
{
    mep_conf_t               config;
    vtss_appl_mep_cc_conf_t  cc_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for CC active */
            continue;
        if ((vtss_appl_mep_cc_conf_get(*next, &cc_config)) != VTSS_RC_OK)
            continue;
        if (cc_config.enable)  /* If CC is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lm_conf_set(const u32                      instance,
                                  const vtss_appl_mep_lm_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lm_conf_set(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lm_conf_set failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_conf_get(const u32                instance,
                                  vtss_appl_mep_lm_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lm_conf_get(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lm_conf_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_conf_add(const u32                       instance,
                                  const vtss_appl_mep_lm_conf_t  *const conf)
{
    vtss_appl_mep_lm_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_lm_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_conf_delete(const u32  instance)
{
    vtss_appl_mep_lm_conf_t  config;

    if ((vtss_appl_mep_lm_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_lm_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_conf_iter(const u32    *const instance,
                                   u32          *const next)
{
    mep_conf_t               config;
    vtss_appl_mep_lm_conf_t  lm_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for LM active */
            continue;
        if ((vtss_appl_mep_lm_conf_get(*next, &lm_config)) != VTSS_RC_OK)
            continue;
        if (lm_config.enable)  /* If LM is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lm_avail_conf_set(const u32                           instance,
                                        const vtss_appl_mep_lm_avail_conf_t *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lm_avail_conf_set(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lm_avail_conf_set failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_avail_conf_get(const u32                      instance,
                                        vtss_appl_mep_lm_avail_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lm_avail_conf_get(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lm_avail_conf_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_avail_conf_add(const u32                            instance,
                                        const vtss_appl_mep_lm_avail_conf_t  *const conf)
{
    vtss_appl_mep_lm_avail_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_lm_avail_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_avail_conf_delete(const u32  instance)
{
    vtss_appl_mep_lm_avail_conf_t  config;

    if ((vtss_appl_mep_lm_avail_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_lm_avail_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_avail_conf_iter(const u32    *const instance,
                                         u32          *const next)
{
    mep_conf_t                     config;
    vtss_appl_mep_lm_avail_conf_t  lm_avail_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for LM active */
            continue;
        if ((vtss_appl_mep_lm_avail_conf_get(*next, &lm_avail_config)) != VTSS_RC_OK)
            continue;
        if (lm_avail_config.enable)  /* If LM Availability is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lm_hli_conf_set(const u32                           instance,
                                      const vtss_appl_mep_lm_hli_conf_t *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lm_hli_conf_set(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lm_hli_conf_set failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_hli_conf_get(const u32                      instance,
                                      vtss_appl_mep_lm_hli_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lm_hli_conf_get(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lm_hli_conf_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_hli_conf_add(const u32                            instance,
                                      const vtss_appl_mep_lm_hli_conf_t  *const conf)
{
    vtss_appl_mep_lm_hli_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_lm_hli_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_hli_conf_delete(const u32  instance)
{
    vtss_appl_mep_lm_hli_conf_t  config;

    if ((vtss_appl_mep_lm_hli_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_lm_hli_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_hli_conf_iter(const u32    *const instance,
                                       u32          *const next)
{
    mep_conf_t                   config;
    vtss_appl_mep_lm_hli_conf_t  lm_hli_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for LM active */
            continue;
        if ((vtss_appl_mep_lm_hli_conf_get(*next, &lm_hli_config)) != VTSS_RC_OK)
            continue;
        if (lm_hli_config.enable)  /* If LM HLI is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lm_sdeg_conf_set(const u32                          instance,
                                       const vtss_appl_mep_lm_sdeg_conf_t *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lm_sdeg_conf_set(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lm_sdeg_conf_set failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_sdeg_conf_get(const u32                     instance,
                                       vtss_appl_mep_lm_sdeg_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lm_sdeg_conf_get(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lm_sdeg_conf_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_sdeg_conf_add(const u32                           instance,
                                       const vtss_appl_mep_lm_sdeg_conf_t  *const conf)
{
    vtss_appl_mep_lm_sdeg_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_lm_sdeg_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_sdeg_conf_delete(const u32  instance)
{
    vtss_appl_mep_lm_sdeg_conf_t  config;

    if ((vtss_appl_mep_lm_sdeg_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_lm_sdeg_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_sdeg_conf_iter(const u32    *const instance,
                                        u32          *const next)
{
    mep_conf_t                   config;
    vtss_appl_mep_lm_sdeg_conf_t lm_sdeg_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for LM active */
            continue;
        if ((vtss_appl_mep_lm_sdeg_conf_get(*next, &lm_sdeg_config)) != VTSS_RC_OK)
            continue;
        if (lm_sdeg_config.enable)  /* If LM SDEG is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

 mesa_rc vtss_appl_mep_dm_conf_set(const u32                      instance,
                                  const vtss_appl_mep_dm_conf_t  *const conf)
{
    mesa_rc                  rc;
    u32                      i;
    vtss_mep_mgmt_dm_conf_t  base_conf;

    T_DG(TRACE_GRP_DM, "Instance %u  enable %u  prio %u", instance, conf->enable, conf->prio);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return (VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return (VTSS_RC_OK);
    if (conf->prio >= VTSS_APPL_MEP_PRIO_MAX)        return (VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_dm_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {  /* Disable DM for all priorities */
        base_conf.dm[i].enable = FALSE;
    }

    base_conf.dm[conf->prio].enable = conf->enable;      /* Configure DM for the selected priority */
    base_conf.prio = conf->prio;  /* This is only to support old DM interface */
    base_conf.common.cast = conf->cast;
    base_conf.common.mep = conf->mep;
    base_conf.common.ended = conf->ended;
    base_conf.common.calcway = conf->calcway;
    base_conf.common.interval = conf->interval;
    base_conf.common.lastn = conf->lastn;
    base_conf.common.tunit = conf->tunit;
    base_conf.common.overflow_act = conf->overflow_act;
    base_conf.common.synchronized = conf->synchronized;
    base_conf.common.proprietary = conf->proprietary;
    base_conf.common.num_of_bin_fd = conf->num_of_bin_fd;
    base_conf.common.num_of_bin_ifdv = conf->num_of_bin_ifdv;
    base_conf.common.m_threshold = conf->m_threshold;

    if ((rc = vtss_mep_mgmt_dm_conf_set(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_conf_set  failed Instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_dm_conf_get(const u32                instance,
                                  vtss_appl_mep_dm_conf_t  *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_dm_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_dm_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    conf->enable = base_conf.dm[base_conf.prio].enable;
    conf->prio = base_conf.prio;
    conf->cast = base_conf.common.cast;
    conf->mep = base_conf.common.mep;
    conf->ended = base_conf.common.ended;
    conf->calcway = base_conf.common.calcway;
    conf->interval = base_conf.common.interval;
    conf->lastn = base_conf.common.lastn;
    conf->tunit = base_conf.common.tunit;
    conf->overflow_act = base_conf.common.overflow_act;
    conf->synchronized = base_conf.common.synchronized;
    conf->proprietary = base_conf.common.proprietary;
    conf->num_of_bin_fd = base_conf.common.num_of_bin_fd;
    conf->num_of_bin_ifdv = base_conf.common.num_of_bin_ifdv;
    conf->m_threshold = base_conf.common.m_threshold;

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_conf_add(const u32                       instance,
                                  const vtss_appl_mep_dm_conf_t  *const conf)
{
    vtss_appl_mep_dm_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_dm_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_conf_delete(const u32  instance)
{
    vtss_appl_mep_dm_conf_t  config;

    if ((vtss_appl_mep_dm_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_dm_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_conf_iter(const u32    *const instance,
                                   u32          *const next)
{
    mep_conf_t               config;
    vtss_appl_mep_dm_conf_t  dm_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for DM active */
            continue;
        if ((vtss_appl_mep_dm_conf_get(*next, &dm_config)) != VTSS_RC_OK)
            continue;
        if (dm_config.enable)  /* If DM is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_dm_control_get(const u32                   instance,
                                     vtss_appl_mep_dm_control_t  *const control)
{
    control->clear = FALSE;

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_control_set(const u32                         instance,
                                     const vtss_appl_mep_dm_control_t  *const control)
{
    if (!control->clear)
        return (VTSS_RC_OK);

    if ((vtss_appl_mep_dm_status_clear(instance)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR);

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_common_conf_set(const u32                             instance,
                                         const vtss_appl_mep_dm_common_conf_t  *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_dm_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_dm_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    base_conf.common = *conf;

    if ((rc = vtss_mep_mgmt_dm_conf_set(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_conf_set failed  Instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_dm_prio_conf_get(const                         u32 instance,
                                       const                         u32 prio,
                                       vtss_appl_mep_dm_prio_conf_t  *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_dm_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;
    if (prio >= VTSS_APPL_MEP_PRIO_MAX)              return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_dm_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    memcpy(conf, &base_conf.dm[prio], sizeof(*conf));

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_prio_conf_set(const                               u32 instance,
                                       const                               u32 prio,
                                       const vtss_appl_mep_dm_prio_conf_t  *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_dm_conf_t base_conf;

    T_DG(TRACE_GRP_DM, "Instance %u  enable %u  prio %u", instance, conf->enable, prio);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;
    if (prio >= VTSS_APPL_MEP_PRIO_MAX)              return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_dm_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    memcpy(&base_conf.dm[prio], conf, sizeof(base_conf.dm[prio]));
    base_conf.prio = prio;  /* This is only to support old TST interface */

    if ((rc = vtss_mep_mgmt_dm_conf_set(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_conf_set failed  Instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_aps_conf_set(const u32                         instance,
                                   const vtss_appl_mep_aps_conf_t    *const conf)
{
    mesa_rc rc;
    BOOL    invalid = FALSE;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if (conf->enable && (((conf->type == VTSS_APPL_MEP_L_APS) && (instance_data[instance].eps_type == MEP_EPS_TYPE_ERPS)) ||
                         ((conf->type == VTSS_APPL_MEP_R_APS) && (instance_data[instance].eps_type == MEP_EPS_TYPE_ELPS)))) {
        invalid = TRUE;
    }

    if (invalid)        return VTSS_APPL_MEP_RC_APS_PROT_CONNECTED;

    if ((rc = vtss_mep_mgmt_aps_conf_set(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_aps_conf_set failed  Instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    instance_data[instance].aps_type = (conf->enable) ? conf->type : VTSS_APPL_MEP_INV_APS;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_aps_conf_get(const u32                  instance,
                                   vtss_appl_mep_aps_conf_t   *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_aps_conf_get(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_aps_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    if (!conf->enable) {
        conf->raps_octet = 1;   /* This is just to give the value '1' as a default value to the "manager" */
    }

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_aps_conf_add(const u32                       instance,
                                   const vtss_appl_mep_aps_conf_t  *const conf)
{
    vtss_appl_mep_aps_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_aps_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_aps_conf_delete(const u32  instance)
{
    vtss_appl_mep_aps_conf_t  config;

    if ((vtss_appl_mep_aps_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_aps_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_aps_conf_iter(const u32    *const instance,
                                    u32          *const next)
{
    mep_conf_t                config;
    vtss_appl_mep_aps_conf_t  aps_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for APS active */
            continue;
        if ((vtss_appl_mep_aps_conf_get(*next, &aps_config)) != VTSS_RC_OK)
            continue;
        if (aps_config.enable)  /* If APS is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lt_conf_set(const u32                      instance,
                                  const vtss_appl_mep_lt_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lt_conf_set(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lt_conf_set failed  Instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_lt_conf_get(const u32                 instance,
                                  vtss_appl_mep_lt_conf_t   *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lt_conf_get(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lt_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lt_conf_add(const u32                      instance,
                                  const vtss_appl_mep_lt_conf_t  *const conf)
{
    vtss_appl_mep_lt_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_lt_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lt_conf_delete(const u32  instance)
{
    vtss_appl_mep_lt_conf_t  config;

    if ((vtss_appl_mep_lt_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_lt_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lt_conf_iter(const u32    *const instance,
                                   u32          *const next)
{
    mep_conf_t               config;
    vtss_appl_mep_lt_conf_t  lt_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for LT active */
            continue;
        if ((vtss_appl_mep_lt_conf_get(*next, &lt_config)) != VTSS_RC_OK)
            continue;
        if (lt_config.enable)  /* If LT is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lb_conf_set(const u32                      instance,
                                  const vtss_appl_mep_lb_conf_t  *const conf)
{
    mesa_rc rc;
    u32     i, j, test_idx;
    u64     bit_per_sec, frame_per_sec;
    vtss_mep_mgmt_lb_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)   return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                             return VTSS_RC_OK;
    if (conf->prio >= VTSS_APPL_MEP_PRIO_MAX)     return (VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lb_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lb_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {  /* Disable LB for all priorities */
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            base_conf.tests[i][j].enable = FALSE;
        }
    }

    test_idx = (conf->dei) ? 1 : 0;
    base_conf.tests[conf->prio][test_idx].enable = conf->enable;      /* Configure TST for the selected priority */
    base_conf.tests[conf->prio][test_idx].dp = !conf->dei ? VTSS_APPL_MEP_DP_GREEN : VTSS_APPL_MEP_DP_YELLOW;
    base_conf.tests[conf->prio][test_idx].size = conf->size;



    base_conf.tests[conf->prio][test_idx].rate = 1;
    if (conf->to_send == VTSS_APPL_MEP_LB_TO_SEND_INFINITE) {   /* Bit rate must be used for "infinite" transmission. This should be given on public interface */
        if (conf->interval) {
            frame_per_sec = 1000000/conf->interval;
            bit_per_sec = frame_per_sec * conf->size * 8;
            base_conf.tests[conf->prio][test_idx].rate = bit_per_sec/1000;  /* This is the bit rate in Kbps that give the approx. interval as configured */
            if (base_conf.tests[conf->prio][test_idx].rate > VTSS_APPL_MEP_TST_RATE_MAX)
                base_conf.tests[conf->prio][test_idx].rate = VTSS_APPL_MEP_TST_RATE_MAX;
        }
    }
    base_conf.tests[conf->prio][test_idx].pattern = VTSS_APPL_MEP_PATTERN_0XAA;
    base_conf.prio = conf->prio;  /* This is only to support old TST interface */
    base_conf.test_idx = test_idx;  /* This is only to support old TST interface */
    base_conf.common.cast = conf->cast;
    memcpy(&base_conf.common.mac, &conf->mac, sizeof(base_conf.common.mac));
    base_conf.common.mep = conf->mep;
    base_conf.common.to_send = conf->to_send;
    base_conf.common.interval = conf->interval;

    if ((rc = vtss_mep_mgmt_lb_conf_set(instance, &base_conf, NULL)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lb_conf_set failed  Instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_lb_conf_get(const u32                 instance,
                                  vtss_appl_mep_lb_conf_t   *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_lb_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return (VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return (VTSS_RC_OK);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lb_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lb_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    conf->enable = base_conf.tests[base_conf.prio][base_conf.test_idx].enable;
    conf->prio = base_conf.prio;
    conf->dei = (base_conf.tests[base_conf.prio][base_conf.test_idx].dp == VTSS_APPL_MEP_DP_GREEN) ? FALSE : TRUE;
    conf->size = base_conf.tests[base_conf.prio][base_conf.test_idx].size;
    conf->ttl = base_conf.tests[base_conf.prio][base_conf.test_idx].ttl;
    conf->mep = base_conf.common.mep;
    conf->cast = base_conf.common.cast;
    conf->to_send = base_conf.common.to_send;
    memcpy(&conf->mac, &base_conf.common.mac, sizeof(conf->mac));
    conf->interval = base_conf.common.interval;

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lb_conf_add(const u32                       instance,
                                  const vtss_appl_mep_lb_conf_t  *const conf)
{
    vtss_appl_mep_lb_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_lb_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lb_conf_delete(const u32  instance)
{
    vtss_appl_mep_lb_conf_t  config;

    if ((vtss_appl_mep_lb_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_lb_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lb_conf_iter(const u32    *const instance,
                                   u32          *const next)
{
    mep_conf_t               config;
    vtss_appl_mep_lb_conf_t  lb_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for LB active */
            continue;
        if ((vtss_appl_mep_lb_conf_get(*next, &lb_config)) != VTSS_RC_OK)
            continue;
        if (lb_config.enable)  /* If LB is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lb_common_conf_get(const u32                      instance,
                                         vtss_appl_mep_lb_common_conf_t *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_lb_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lb_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lb_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    *conf = base_conf.common;

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lb_common_conf_set(const u32                            instance,
                                         const vtss_appl_mep_lb_common_conf_t *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_lb_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lb_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lb_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    base_conf.common = *conf;

    if ((rc = vtss_mep_mgmt_lb_conf_set(instance, &base_conf, NULL)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lb_conf_set failed  Instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lb_prio_conf_get(const                           u32 instance,
                                       const                           u32 prio,
                                       const                           u32 test_idx,
                                       vtss_appl_mep_test_prio_conf_t  *const conf)
{
    mesa_rc                  rc;
    vtss_mep_mgmt_lb_conf_t  base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;
    if (prio >= VTSS_APPL_MEP_PRIO_MAX)              return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    if (test_idx >= VTSS_APPL_MEP_TEST_MAX)          return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lb_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lb_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    memcpy(conf, &base_conf.tests[prio][test_idx], sizeof(*conf));

    return (VTSS_RC_OK);
}

mesa_rc mep_volatile_sat_lb_prio_conf_set(const                                 u32 instance,
                                          const                                 u32 prio,
                                          const                                 u32 test_idx,
                                          const vtss_appl_mep_test_prio_conf_t  *const conf,
                                                                                u64 *active_time_ms)
{
    mesa_rc rc;
    vtss_mep_mgmt_lb_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;
    if (prio >= VTSS_APPL_MEP_PRIO_MAX)              return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    if (test_idx >= VTSS_APPL_MEP_TEST_MAX)          return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lb_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lb_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    memcpy(&base_conf.tests[prio][test_idx], conf, sizeof(base_conf.tests[prio][test_idx]));
    base_conf.prio = prio;  /* This is only to support old LB interface */
    base_conf.test_idx = test_idx;  /* This is only to support old LB interface */

    if ((rc = vtss_mep_mgmt_lb_conf_set(instance, &base_conf, active_time_ms)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lb_conf_set failed  Instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lb_prio_conf_set(const                                 u32 instance,
                                       const                                 u32 prio,
                                       const                                 u32 test_idx,
                                       const vtss_appl_mep_test_prio_conf_t  *const conf)
{
    // Re-use SAT function, but with last parameter NULL.
    return mep_volatile_sat_lb_prio_conf_set(instance, prio, test_idx, conf, NULL);
}

mesa_rc vtss_appl_mep_ais_conf_set(const u32                      instance,
                                   const vtss_appl_mep_ais_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_ais_conf_set(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_ais_conf_set failed  Instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_ais_conf_get(const u32                 instance,
                                   vtss_appl_mep_ais_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_ais_conf_get(instance,conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_ais_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_ais_conf_add(const u32                       instance,
                                   const vtss_appl_mep_ais_conf_t  *const conf)
{
    vtss_appl_mep_ais_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_ais_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_ais_conf_delete(const u32  instance)
{
    vtss_appl_mep_ais_conf_t  config;

    if ((vtss_appl_mep_ais_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_ais_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_ais_conf_iter(const u32    *const instance,
                                    u32          *const next)
{
    mep_conf_t                config;
    vtss_appl_mep_ais_conf_t  ais_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for AIS active */
            continue;
        if ((vtss_appl_mep_ais_conf_get(*next, &ais_config)) != VTSS_RC_OK)
            continue;
        if (ais_config.enable)  /* If AIS is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lck_conf_set(const u32                       instance,
                                   const vtss_appl_mep_lck_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lck_conf_set(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lck_conf_set failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_lck_conf_get(const u32                 instance,
                                   vtss_appl_mep_lck_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lck_conf_get(instance,conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lck_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    return VTSS_RC_OK;
}


mesa_rc vtss_appl_mep_lck_conf_add(const u32                       instance,
                                   const vtss_appl_mep_lck_conf_t  *const conf)
{
    vtss_appl_mep_lck_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_lck_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lck_conf_delete(const u32  instance)
{
    vtss_appl_mep_lck_conf_t  config;

    if ((vtss_appl_mep_lck_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_lck_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lck_conf_iter(const u32    *const instance,
                                    u32          *const next)
{
    mep_conf_t                config;
    vtss_appl_mep_lck_conf_t  lck_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for LCK active */
            continue;
        if ((vtss_appl_mep_lck_conf_get(*next, &lck_config)) != VTSS_RC_OK)
            continue;
        if (lck_config.enable)  /* If LCK is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_tst_conf_set(const u32                       instance,
                                   const vtss_appl_mep_tst_conf_t  *const conf)
{
    mesa_rc rc;
    u32     i, j, test_idx;
    vtss_mep_mgmt_tst_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return (VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return (VTSS_RC_OK);
    if (conf->prio >= VTSS_APPL_MEP_PRIO_MAX)        return (VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_tst_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_tst_conf_get failed  Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {  /* Disable TST for all priorities */
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            base_conf.tests[i][j].enable = FALSE;
        }
    }

    test_idx = (conf->dei) ? 1 : 0;
    base_conf.tests[conf->prio][test_idx].enable = conf->enable;      /* Configure TST for the selected priority */
    base_conf.tests[conf->prio][test_idx].dp = !conf->dei ? VTSS_APPL_MEP_DP_GREEN : VTSS_APPL_MEP_DP_YELLOW;
    base_conf.tests[conf->prio][test_idx].size = conf->size;
    base_conf.tests[conf->prio][test_idx].rate = conf->rate;
    base_conf.tests[conf->prio][test_idx].pattern = conf->pattern;
    base_conf.prio = conf->prio;  /* This is only to support old TST interface */
    base_conf.test_idx = test_idx;  /* This is only to support old TST interface */
    base_conf.common.enable_rx = conf->enable_rx;
    base_conf.common.mep = conf->mep;
    base_conf.common.sequence = conf->sequence;

    if ((rc = vtss_mep_mgmt_tst_conf_set(instance, &base_conf, NULL)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_tst_conf_set  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_tst_conf_get(const u32                  instance,
                                   vtss_appl_mep_tst_conf_t   *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_tst_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_tst_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_tst_conf_get  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    conf->enable = base_conf.tests[base_conf.prio][base_conf.test_idx].enable;
    conf->prio = base_conf.prio;
    conf->dei = (base_conf.tests[base_conf.prio][base_conf.test_idx].dp == VTSS_APPL_MEP_DP_GREEN) ? FALSE : TRUE;
    conf->size = base_conf.tests[base_conf.prio][base_conf.test_idx].size;
    conf->rate = base_conf.tests[base_conf.prio][base_conf.test_idx].rate;
    conf->pattern = base_conf.tests[base_conf.prio][base_conf.test_idx].pattern;
    conf->enable_rx = base_conf.common.enable_rx;
    conf->mep = base_conf.common.mep;
    conf->sequence = base_conf.common.sequence;

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_conf_add(const u32                       instance,
                                   const vtss_appl_mep_tst_conf_t  *const conf)
{
    vtss_appl_mep_tst_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_tst_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_conf_delete(const u32  instance)
{
    vtss_appl_mep_tst_conf_t  config;

    if ((vtss_appl_mep_tst_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_tst_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_conf_iter(const u32    *const instance,
                                    u32          *const next)
{
    mep_conf_t                config;
    vtss_appl_mep_tst_conf_t  tst_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for TST active */
            continue;
        if ((vtss_appl_mep_tst_conf_get(*next, &tst_config)) != VTSS_RC_OK)
            continue;
        if (tst_config.enable)  /* If TST is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_tst_control_get(const u32                    instance,
                                      vtss_appl_mep_tst_control_t  *const control)
{
    if (control == NULL)       return VTSS_RC_OK;

    control->clear = FALSE;

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_control_set(const u32                          instance,
                                      const vtss_appl_mep_tst_control_t  *const control)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (control == NULL)                             return VTSS_RC_OK;

    if (!control->clear)
        return (VTSS_RC_OK);

    if ((vtss_appl_mep_tst_status_clear(instance)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR);

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_common_conf_get(const u32                       instance,
                                          vtss_appl_mep_tst_common_conf_t *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_tst_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_tst_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_tst_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    *conf = base_conf.common;

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_common_conf_set(const u32                             instance,
                                          const vtss_appl_mep_tst_common_conf_t *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_tst_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_tst_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_tst_conf_get  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    base_conf.prio = VTSS_APPL_MEP_PRIO_MAX;  /* This is only to support old TST interface */
    base_conf.common = *conf;

    if ((rc = vtss_mep_mgmt_tst_conf_set(instance, &base_conf, NULL)) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_prio_conf_get(const                           u32 instance,
                                        const                           u32 prio,
                                        const                           u32 test_idx,
                                        vtss_appl_mep_test_prio_conf_t  *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_tst_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;
    if (prio >= VTSS_APPL_MEP_PRIO_MAX)              return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    if (test_idx >= VTSS_APPL_MEP_TEST_MAX)          return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_tst_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_tst_conf_get  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    memcpy(conf, &base_conf.tests[prio][test_idx], sizeof(*conf));

    return (VTSS_RC_OK);
}

mesa_rc mep_volatile_sat_tst_prio_conf_set(const                                 u32 instance,
                                           const                                 u32 prio,
                                           const                                 u32 test_idx,
                                           const vtss_appl_mep_test_prio_conf_t  *const conf,
                                                                                 u64 *active_time_ms)
{
    mesa_rc rc;
    vtss_mep_mgmt_tst_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;
    if (prio >= VTSS_APPL_MEP_PRIO_MAX)              return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    if (test_idx >= VTSS_APPL_MEP_TEST_MAX)          return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_tst_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_tst_conf_get  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    memcpy(&base_conf.tests[prio][test_idx], conf, sizeof(base_conf.tests[prio][test_idx]));
    base_conf.prio = prio;  /* This is only to support old TST interface */
    base_conf.test_idx = test_idx;  /* This is only to support old TST interface */

    if ((rc = vtss_mep_mgmt_tst_conf_set(instance, &base_conf, active_time_ms)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_tst_conf_set  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_prio_conf_set(const                                 u32 instance,
                                        const                                 u32 prio,
                                        const                                 u32 test_idx,
                                        const vtss_appl_mep_test_prio_conf_t  *const conf)
{
    // Re-use SAT function, but with last parameter NULL.
    return mep_volatile_sat_tst_prio_conf_set(instance, prio, test_idx, conf, NULL);
}

mesa_rc mep_client_conf_set(const u32                 instance,
                            const mep_client_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_client_conf_set(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_client_conf_set failed  Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    return VTSS_RC_OK;
}

mesa_rc mep_client_conf_get(const u32           instance,
                            mep_client_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_client_conf_get(instance, conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_client_conf_get failed  Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_mep_client_flow_conf_add(const u32                                instance,
                                           const vtss_ifindex_t                     flow_in,
                                           const vtss_appl_mep_client_flow_conf_t  *const conf)
{
    mep_client_conf_t  config;
    u32                i, flow;
    mep_domain_t       domain;

    T_N("instance %u  flow_in %u", instance, VTSS_IFINDEX_PRINTF_ARG(flow_in));

    if ((mep_client_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    if (config.flow_count >= VTSS_APPL_MEP_CLIENT_FLOWS_MAX)  /* Not room for any more Client Flows */
        return (VTSS_RC_ERROR);

    VTSS_RC(conv_mep_client_flow_conf_ext_to_int(flow_in, &flow, &domain));

    for (i=0; i<config.flow_count; ++i) {
        if ((config.domain[i] == domain) && (config.flows[i] == flow))  /* Client Flow was found */
            return (VTSS_RC_ERROR);
    }

    config.ais_prio[i]      = conf->ais_prio;
    config.lck_prio[i]      = conf->lck_prio;
    config.level[i]         = conf->level;
    config.flows[i]         = flow;
    config.domain[i] = domain;
    config.flow_count += 1;

    if (mep_client_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    T_N("OK");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_client_flow_conf_delete(const u32             instance,
                                              const vtss_ifindex_t  flow_in)
{
    mep_client_conf_t  config;
    mep_client_conf_t  new_config;
    u32                i, idx, flow;
    mep_domain_t       domain;

    T_N("instance %u  flow_in %u", instance, VTSS_IFINDEX_PRINTF_ARG(flow_in));

    if ((mep_client_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    VTSS_RC(conv_mep_client_flow_conf_ext_to_int(flow_in, &flow, &domain));

    memset(&new_config, 0, sizeof(new_config));
    for (i=0, idx=0; i<config.flow_count; ++i) {
        if ((config.domain[i] != domain) || (config.flows[i] != flow)) {
            new_config.flows[idx]       = config.flows[i];
            new_config.domain[idx]      = config.domain[i];
            new_config.ais_prio[idx]    = config.ais_prio[i];
            new_config.lck_prio[idx]    = config.lck_prio[i];
            new_config.level[idx]       = config.level[i];
            idx++;
        }
    }
    new_config.flow_count = idx;

    if (mep_client_conf_set(instance, &new_config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    T_N("OK");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_client_flow_conf_set(const u32                                instance,
                                           const vtss_ifindex_t                     flow_in,
                                           const vtss_appl_mep_client_flow_conf_t  *const conf)
{
    mep_client_conf_t  config;
    u32                i, flow;
    mep_domain_t       domain;

    T_N("instance %u  flow_in %u", instance, VTSS_IFINDEX_PRINTF_ARG(flow_in));

    if ((mep_client_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    VTSS_RC(conv_mep_client_flow_conf_ext_to_int(flow_in, &flow, &domain));

    for (i=0; i<config.flow_count; ++i) /* Search for Client Flow */
        if ((config.domain[i] == domain) && (config.flows[i] == flow))  /* Client Flow was found */
            break;

    if (i < config.flow_count) { /* Client Flow was found */
        config.ais_prio[i]      = conf->ais_prio;
        config.lck_prio[i]      = conf->lck_prio;
        config.level[i]         = conf->level;
        config.flows[i]         = flow;
        config.domain[i] = domain;

        if (mep_client_conf_set(instance, &config) != VTSS_RC_OK)
            return (VTSS_RC_ERROR); /* Not able to set MEP */
    } else /* Did not find Client Flow */
        return (VTSS_RC_ERROR);

    T_N("OK");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_client_flow_conf_get(const u32                            instance,
                                           const vtss_ifindex_t                 flow_in,
                                           vtss_appl_mep_client_flow_conf_t    *const conf)
{
    mep_client_conf_t  config;
    u32                i, flow;
    mep_domain_t       domain;

    T_N("instance %u  flow_in %u", instance, VTSS_IFINDEX_PRINTF_ARG(flow_in));

    if ((mep_client_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    VTSS_RC(conv_mep_client_flow_conf_ext_to_int(flow_in, &flow, &domain));

    for (i=0; i<config.flow_count; ++i) {/* Search for Client Flow */
        if ((config.domain[i] == domain) && (config.flows[i] == flow))  /* Client Flow was found */
            break;
    }

    if (i < config.flow_count) { /* Client Flow was found */
        conf->ais_prio      = config.ais_prio[i];
        conf->lck_prio      = config.lck_prio[i];
        conf->level         = config.level[i];
    } else /* Did not find Client Flow */
        return (VTSS_RC_ERROR);

    T_N("OK\n");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_client_flow_conf_iter(const u32               *const instance,
                                            u32                     *const next_instance,
                                            const vtss_ifindex_t    *const flow_in,
                                            vtss_ifindex_t          *const next_flow)
{
    mep_client_conf_t  config;
    u32                i, next_flow_i, flow;
    mep_domain_t       domain;

    T_N("instance %p  flow_in %p  *instance %u  *flow_in %u", instance, flow_in, (instance) ? *instance : 0, (flow_in) ? VTSS_IFINDEX_PRINTF_ARG(*flow_in) : 0);

    if (instance == NULL) {  /* Get the first enabled instance as requested */
        if (vtss_appl_mep_instance_conf_iter(instance, next_instance) == VTSS_RC_ERROR)
            return(VTSS_RC_ERROR);
    } else
        *next_instance = *instance;

    /* Now we have an enabled instance to start with */

    if (flow_in) { /* Calculate the starting point for the search for next flow */
        if ((mep_client_conf_get(*next_instance, &config)) != VTSS_RC_OK)  /* get client configuration */
            return (VTSS_RC_ERROR);

        VTSS_RC(conv_mep_client_flow_conf_ext_to_int(*flow_in, &flow, &domain));
        for (i=0; i<config.flow_count; ++i) {/* Search for Client Flow */
            if ((config.domain[i] == domain) && (config.flows[i] == flow))  /* Client Flow was found */
                break;
        }
        if (i == config.flow_count) /* Check if flow was found */
            return (VTSS_RC_ERROR);

        next_flow_i = i + 1;
    } else {
        next_flow_i = 0;
    }

    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i) { /* This loop should be terminated through a return of error or ok */
        if ((mep_client_conf_get(*next_instance, &config)) != VTSS_RC_OK)  /* get configuration */
            return (VTSS_RC_ERROR);

        if (next_flow_i >= config.flow_count) { /* The end of flow for this instance */
            next_flow_i = 0;  /* The flow index always start in '0' for the next instance */
            if (vtss_appl_mep_instance_conf_iter(next_instance,  next_instance) == VTSS_RC_ERROR)  /* Get next enabled instance */
                return(VTSS_RC_ERROR);
        } else {
            T_N("OK");
            VTSS_RC(conv_mep_client_flow_conf_int_to_ext(config.flows[next_flow_i], config.domain[next_flow_i], next_flow));
            return(VTSS_RC_OK); /* The next instance is found with the next Client flow index */
        }
    }

    return (VTSS_RC_ERROR);
}


































































































































































































































































































































































































mesa_rc vtss_appl_mep_g8113_2_bfd_status_get(const u32                         instance,
                                             vtss_appl_mep_g8113_2_bfd_state_t *const status)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_clear_stats(const u32 instance)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_bfd_control_get(const u32                    instance,
                                      vtss_appl_mep_bfd_control_t  *const control)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_bfd_control_set(const u32                          instance,
                                      const vtss_appl_mep_bfd_control_t  *const control)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_conf_set(const u32                              instance,
                                           const vtss_appl_mep_g8113_2_bfd_conf_t *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_conf_get(const u32                        instance,
                                           vtss_appl_mep_g8113_2_bfd_conf_t *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_conf_add(const u32                               instance,
                                           const vtss_appl_mep_g8113_2_bfd_conf_t  *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_conf_delete(const u32  instance)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_conf_iter(const u32    *const instance,
                                            u32          *const next)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_set(const u32                                   key_id,
                                                const vtss_appl_mep_g8113_2_bfd_auth_conf_t *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_get(const u32                             key_id,
                                                vtss_appl_mep_g8113_2_bfd_auth_conf_t *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_add(const u32                                    key_id,
                                                const vtss_appl_mep_g8113_2_bfd_auth_conf_t  *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_delete(const u32 key_id)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_iter(const u32    *const key_id,
                                                 u32          *const next_key_id)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_rt_conf_set(const u32                      instance,
                                  const vtss_appl_mep_rt_conf_t  *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_rt_conf_get(const u32                 instance,
                                  vtss_appl_mep_rt_conf_t   *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_rt_conf_add(const u32                      instance,
                                  const vtss_appl_mep_rt_conf_t  *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_rt_conf_delete(const u32  instance)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_rt_conf_iter(const u32    *const instance,
                                   u32          *const next)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_rt_status_get(const u32                  instance,
                                    vtss_appl_mep_rt_status_t  *const status)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_rt_reply_status_get(const u32                 instance,
                                          const u32                 seq_no,
                                          vtss_appl_mep_rt_reply_t  *const status)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_rt_reply_status_iter(const u32    *const instance,
                                           u32          *const next_instance,
                                           const u32    *const seq_no,
                                           u32          *const next_seq_no)
{
    return (VTSS_RC_ERROR);
}



mesa_rc vtss_appl_mep_instance_status_get(const u32              instance,
                                          vtss_appl_mep_state_t  *const state)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    return mep_status_get(instance, state);
}

mesa_rc vtss_appl_mep_lm_notif_status_get(const u32              instance,
                                          vtss_appl_mep_lm_notif_state_t  *const state)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    return mep_status_lm_notif_get(instance, state);
}

mesa_rc vtss_appl_mep_instance_peer_status_get(const u32                   instance,
                                               const u32                   peer,
                                               vtss_appl_mep_peer_state_t  *const state)
{
    T_N("instance %u  peer %u", instance, peer);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    return mep_status_peer_get(instance, peer, state);
}

mesa_rc vtss_appl_mep_cc_status_get(const u32                    instance,
                                    vtss_appl_mep_cc_state_t     *const state)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_cc_state_get(instance, state)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_cc_state_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_cc_peer_status_get(const u32                      instance,
                                         const u32                      peer,
                                         vtss_appl_mep_peer_cc_state_t  *const state)
{
    u32 i;
    vtss_appl_mep_cc_state_t cc_state;
    mep_conf_t               config;

    T_N("instance %u  peer %u", instance, peer);

    if (state == NULL)                                return VTSS_RC_OK;

    if ((mep_instance_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    for (i=0; i<config.peer_count; ++i) {/* Search for peer MEP-ID */
        if (config.peer_mep[i] == peer)
            break;
    }

    if (i < config.peer_count) { /* Peer MEP-ID was  found */
        if ((vtss_appl_mep_cc_status_get(instance, &cc_state)) != VTSS_RC_OK)
            return (VTSS_RC_ERROR); /* Not able to get MEP */

        state->ps_tlv = cc_state.ps_tlv[i];
        state->is_tlv = cc_state.is_tlv[i];
        state->os_tlv = cc_state.os_tlv[i];
        state->ps_received = cc_state.ps_received[i];
        state->is_received = cc_state.is_received[i];
        state->os_received = cc_state.os_received[i];
    } else /* Did not find peer MEP-ID */
        return (VTSS_RC_ERROR);

    T_N("OK\n");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lt_status_get(const u32                  instance,
                                    vtss_appl_mep_lt_state_t   *const state)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lt_state_get(instance, state)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lt_state_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lt_transaction_status_get(const u32                        instance,
                                                vtss_appl_mep_lt_transaction_t   *const status)
{
    vtss_appl_mep_lt_state_t state;

    T_N("instance %u", instance);

    if (status == NULL)                                return VTSS_RC_OK;

    if ((vtss_appl_mep_lt_status_get(instance, &state)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    if (state.transaction_cnt == 0) {
        memset(status, 0, sizeof(*status));
    } else {
        *status = state.transaction[state.transaction_cnt-1];
    }

    T_N("OK\n");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lt_transaction_status_iter(const u32    *const instance,
                                                 u32          *const next)
{
    mep_conf_t                config;
    vtss_appl_mep_lt_state_t  state;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for any LT transactions */
            continue;
        if ((vtss_appl_mep_lt_status_get(*next, &state)) != VTSS_RC_OK)
            continue;
        if (state.transaction_cnt != 0)  /* If LT transactions the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lt_reply_status_get(const u32                 instance,
                                          const u32                 reply,
                                          vtss_appl_mep_lt_reply_t  *const status)
{
    vtss_appl_mep_lt_state_t state;

    T_N("instance %u  reply %u", instance, reply);

    if (status == NULL)                                return VTSS_RC_OK;

    if ((vtss_appl_mep_lt_status_get(instance, &state)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    if (state.transaction_cnt == 0)
        return (VTSS_RC_ERROR); /* No transactions */

    if (reply >= state.transaction[state.transaction_cnt-1].reply_cnt)
        return (VTSS_RC_ERROR); /* Invalid reply id */

    *status = state.transaction[state.transaction_cnt-1].reply[reply];

    T_N("OK\n");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lt_reply_status_iter(const u32    *const instance,
                                           u32          *const next_instance,
                                           const u32    *const reply,
                                           u32          *const next_reply)
{
    vtss_appl_mep_lt_state_t state;
    BOOL need_find_next_instance = FALSE;

    T_N("instance %p  reply %p  *instance %u  *reply %u", instance, reply, (instance) ? *instance : 0, (reply) ? *reply : 0);

    if (instance == NULL) {  /* Get the first enabled instance as requested */
        need_find_next_instance = TRUE;
        *next_reply = 0;
    } else {
        *next_instance = *instance;

        /* Now we have an enabled instance to start with */

        if (reply) { /* Calculate the starting point for the search for next reply */
            *next_reply = *reply + 1;
        } else {
            *next_reply = 0;
        }
    }

    for (; *next_instance < VTSS_APPL_MEP_INSTANCE_MAX; /* The update statement is processed by vtss_appl_mep_instance_conf_iter() */) { /* This loop should be terminated through a return of error or ok */
        if ((need_find_next_instance && vtss_appl_mep_instance_conf_iter(instance, next_instance) != VTSS_RC_OK) ||
            vtss_appl_mep_lt_status_get(*next_instance, &state) != VTSS_RC_OK /* Not able to get MEP */ ||
            state.transaction_cnt == 0 ||
            (state.transaction_cnt > 0 && *next_reply >= state.transaction[state.transaction_cnt-1].reply_cnt)) {
            need_find_next_instance = TRUE;
            *next_reply = 0;
            continue;
        }

        T_N("OK");
        return(VTSS_RC_OK); /* The next instance is found with the next LB Reply id */
    }

    if (*next_instance >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */ {
        // Lead to maximum range <u32> in order to speed up the process time while SNMP 'GETNEXT' operation
        *next_instance = 0xFFFFFFFF;
        *next_reply = 0xFFFFFFFF;
    }
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_lb_status_get(const u32                  instance,
                                    vtss_appl_mep_lb_state_t   *const state)
{
    mesa_rc                  rc;
    vtss_mep_mgmt_lb_conf_t  base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)    return (VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                             return (VTSS_RC_OK);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lb_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lb_conf_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    if ((rc = vtss_mep_mgmt_lb_prio_state_get(instance, base_conf.prio, state)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lb_prio_state_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lb_prio_status_get(const u32                  instance,
                                         const u32                  prio,
                                         vtss_appl_mep_lb_state_t   *const state)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                 return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lb_prio_state_get(instance, prio, state)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lb_prio_state_get failed  Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lb_reply_status_get(const u32                 instance,
                                          const u32                 reply,
                                          vtss_appl_mep_lb_reply_t  *const status)
{
    vtss_appl_mep_lb_state_t state;

    T_N("instance %u  reply %u", instance, reply);

    if (status == NULL)                                return VTSS_RC_OK;

    if ((vtss_appl_mep_lb_status_get(instance, &state)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    if (reply >= state.reply_cnt)
        return (VTSS_RC_ERROR); /* Invalid reply id */

    *status = state.reply[reply];

    T_N("OK\n");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lb_reply_status_iter(const u32    *const instance,
                                           u32          *const next_instance,
                                           const u32    *const reply,
                                           u32          *const next_reply)
{
    vtss_appl_mep_lb_state_t state;
    BOOL need_find_next_instance = FALSE;

    T_N("instance %p  reply %p  *instance %u  *reply %u", instance, reply, (instance) ? *instance : 0, (reply) ? *reply : 0);

    if (instance == NULL) {  /* Get the first enabled instance as requested */
        need_find_next_instance = TRUE;
        *next_reply = 0;
    } else {
        *next_instance = *instance;

        /* Now we have an enabled instance to start with */

        if (reply) { /* Calculate the starting point for the search for next reply */
            *next_reply = *reply + 1;
        } else {
            *next_reply = 0;
        }
    }

    for (; *next_instance < VTSS_APPL_MEP_INSTANCE_MAX; /* The update statement is processed by vtss_appl_mep_instance_conf_iter() */) { /* This loop should be terminated through a return of error or ok */
        if ((need_find_next_instance && vtss_appl_mep_instance_conf_iter(instance, next_instance) != VTSS_RC_OK) ||
            vtss_appl_mep_lb_status_get(*next_instance, &state) != VTSS_RC_OK ||
            *next_reply >= state.reply_cnt) {
            need_find_next_instance = TRUE;
            *next_reply = 0;
            continue;
        }

        T_N("OK");
        return(VTSS_RC_OK); /* The next instance is found with the next LB Reply id */
    }

    if (*next_instance >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */ {
        // Lead to maximum range <u32> in order to speed up the process time while SNMP 'GETNEXT' operation
        *next_instance = 0xFFFFFFFF;
        *next_reply = 0xFFFFFFFF;
    }
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_lb_status_clear(const u32 instance)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return (VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lb_state_clear_set(instance)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lb_state_clear_set failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}


mesa_rc vtss_appl_mep_lm_status_get(const u32                   instance,
                                    vtss_appl_mep_lm_state_t    *const state)
{
    mesa_rc rc;
    vtss_appl_mep_conf_t  config;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    if ((vtss_appl_mep_instance_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    if (config.peer_count == 0)
        return (VTSS_RC_OK); /* No peer MEP - just return all zero status */

    CRIT_ENTER_EXIT();

    if ((rc = vtss_mep_mgmt_lm_state_get(instance, config.peer_mep[0], state)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lm_state_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_instance_peer_lm_status_get(const u32                 instance,
                                                  const u32                 peer,
                                                  vtss_appl_mep_lm_state_t  *const state)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lm_state_get(instance, peer, state)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lm_state_get failed  instance %u peer %u rc %s", instance, peer, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_status_clear(const u32    instance)
{
    return vtss_appl_mep_lm_status_clear_dir(instance, VTSS_APPL_MEP_CLEAR_DIR_BOTH);
}

mesa_rc vtss_appl_mep_lm_status_clear_dir(const uint32_t          instance,
                                          vtss_appl_mep_clear_dir direction)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lm_state_clear_set(instance, direction)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lm_state_clear_set failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_control_get(const u32                   instance,
                                     vtss_appl_mep_lm_control_t  *const control)
{
    if (control == NULL)                                return VTSS_RC_OK;

    control->clear = FALSE;

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_control_set(const u32                         instance,
                                     const vtss_appl_mep_lm_control_t  *const control)
{
    if (!control->clear)
        return (VTSS_RC_OK);

    if ((vtss_appl_mep_lm_status_clear(instance)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR);

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_avail_status_get(const u32                         instance,
                                          const u32                         peer_id,
                                          vtss_appl_mep_lm_avail_state_t    *const state)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_lm_avail_state_get(instance, peer_id, state)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_lm_avail_state_get failed  instance %u  peer_id %u  rc %s", instance, peer_id, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_hli_status_get(const u32                       instance,
                                        const u32                       peer_id,
                                        vtss_appl_mep_lm_hli_state_t    *const state)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    return mep_status_lm_hli_get(instance, peer_id, state);
}

mesa_rc vtss_appl_mep_dm_status_get(const u32                   instance,
                                    vtss_appl_mep_dm_state_t    *const dmr_state,
                                    vtss_appl_mep_dm_state_t    *const dm1_state_far_to_near,
                                    vtss_appl_mep_dm_state_t    *const dm1_state_near_to_far)
{
    mesa_rc                  rc;
    vtss_mep_mgmt_dm_conf_t  base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (dmr_state == NULL)                             return VTSS_RC_OK;
    if (dm1_state_far_to_near == NULL)                 return VTSS_RC_OK;
    if (dm1_state_near_to_far == NULL)                 return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_dm_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_conf_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    if ((rc = vtss_mep_mgmt_dm_prio_state_get(instance, base_conf.prio, dmr_state, dm1_state_far_to_near, dm1_state_near_to_far)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_prio_state_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_fd_bin_status_get(const u32                          instance,
                                           const u32                          index,
                                           vtss_appl_mep_dm_fd_bin_state_t    *const status)
{
    mesa_rc                   rc;
    vtss_appl_mep_dm_state_t  dm_state;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                                              return VTSS_APPL_MEP_RC_INVALID_INSTANCE;
    if (status == NULL)                                                                      return VTSS_RC_OK;
    if (index >= VTSS_APPL_MEP_DM_BINS_FD_MAX)                                                return VTSS_APPL_MEP_RC_FD_BIN;
    if ((rc = vtss_appl_mep_dm_tw_status_get(instance, &dm_state)) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    status->fd_bin = dm_state.fd_bin[index];
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_fd_bin_status_iter(const u32  *const instance,
                                            u32        *const next_instance,
                                            const u32  *const index,
                                            u32        *const next_index)
{
    mep_conf_t  config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        if (index) {
            if (*index >=(VTSS_APPL_MEP_DM_BINS_FD_MAX-1)) {
                *next_instance = *instance + 1;
                *next_index    = 0;
            } else {
                *next_instance = *instance;
                *next_index    = *index + 1;
            }
        } else {
            *next_instance = *instance;
            *next_index    = 0;
        }
    } else {
        *next_instance = 0;
        *next_index    = 0;
    }

    for (*next_instance=*next_instance; *next_instance<VTSS_APPL_MEP_INSTANCE_MAX; ++*next_instance) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next_instance, &config)) == VTSS_RC_OK) {
            if (config.enable) {  /* If the MEP is enabled the search is completed */
                return (VTSS_RC_OK);
            }
        }
        *next_index = 0;
    }
    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_dm_ifdv_bin_status_get(const u32                          instance,
                                             const u32                          index,
                                             vtss_appl_mep_dm_ifdv_bin_state_t    *const status)
{
    mesa_rc                   rc;
    vtss_appl_mep_dm_state_t  dm_state;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                                                  return VTSS_APPL_MEP_RC_INVALID_INSTANCE;
    if (status == NULL)                                                                          return VTSS_RC_OK;
    if (index >= VTSS_APPL_MEP_DM_BINS_IFDV_MAX)                                                  return VTSS_APPL_MEP_RC_IFDV_BIN;
    if ((rc = vtss_appl_mep_dm_tw_status_get(instance, &dm_state)) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    status->ifdv_bin = dm_state.ifdv_bin[index];
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_ifdv_bin_status_iter(const u32  *const instance,
                                              u32        *const next_instance,
                                              const u32  *const index,
                                              u32        *const next_index)
{
    mep_conf_t  config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        if (index) {
            if (*index >=(VTSS_APPL_MEP_DM_BINS_IFDV_MAX-1)) {
                *next_instance = *instance + 1;
                *next_index    = 0;
            } else {
                *next_instance = *instance;
                *next_index    = *index + 1;
            }
        } else {
            *next_instance = *instance;
            *next_index    = 0;
        }
    } else {
        *next_instance = 0;
        *next_index    = 0;
    }

    for (*next_instance=*next_instance; *next_instance<VTSS_APPL_MEP_INSTANCE_MAX; ++*next_instance) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next_instance, &config)) == VTSS_RC_OK) {
            if (config.enable) {  /* If the MEP is enabled the search is completed */
                return (VTSS_RC_OK);
            }
        }
        *next_index = 0;
    }
    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_dm_fd_bins_get(const u32                          instance,
                                     const u32                          bin_number,
                                     vtss_appl_mep_dm_fd_bin_state_t    *const tw_bin,
                                     vtss_appl_mep_dm_bin_value_t       *const bin_value)
{
    mesa_rc                   rc;
    vtss_mep_mgmt_dm_conf_t   base_conf;
    vtss_appl_mep_dm_state_t  dmr_state;
    vtss_appl_mep_dm_state_t  dm1_state_far_to_near;
    vtss_appl_mep_dm_state_t  dm1_state_near_to_far;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)     return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (bin_value == NULL)                          return VTSS_RC_OK;

    if ((rc = vtss_mep_mgmt_dm_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    if (bin_number == 0 || bin_number > base_conf.common.num_of_bin_fd)     return(VTSS_APPL_MEP_RC_INVALID_BIN_NUMBER);

    // TODO: This call could be replaced with an optimized function so that we avoid making a data copy of three structs.
    if (vtss_appl_mep_dm_prio_status_get(instance, base_conf.prio, &dmr_state, &dm1_state_far_to_near, &dm1_state_near_to_far) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not enable to get DM state */

    u32 bin_index = bin_number - 1;
    bin_value->two_way = dmr_state.fd_bin[bin_index];
    bin_value->one_way_fn = dm1_state_far_to_near.fd_bin[bin_index];
    bin_value->one_way_nf = dm1_state_near_to_far.fd_bin[bin_index];

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_ifdv_bins_get(const u32                            instance,
                                       const u32                            bin_number,
                                       vtss_appl_mep_dm_ifdv_bin_state_t    *const tw_bin,
                                       vtss_appl_mep_dm_bin_value_t         *const bin_value)
{
    mesa_rc                   rc;
    vtss_mep_mgmt_dm_conf_t   base_conf;
    vtss_appl_mep_dm_state_t  dmr_state;
    vtss_appl_mep_dm_state_t  dm1_state_far_to_near;
    vtss_appl_mep_dm_state_t  dm1_state_near_to_far;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)     return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (bin_value == NULL)                          return VTSS_RC_OK;

    if ((rc = vtss_mep_mgmt_dm_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    if (bin_number == 0 || bin_number > base_conf.common.num_of_bin_fd)     return(VTSS_APPL_MEP_RC_INVALID_BIN_NUMBER);

    // TODO: This call could be replaced with an optimized function so that we avoid making a data copy of three structs.
    if (vtss_appl_mep_dm_prio_status_get(instance, base_conf.prio, &dmr_state, &dm1_state_far_to_near, &dm1_state_near_to_far) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not enable to get DM state */

    u32 bin_index = bin_number - 1;
    bin_value->two_way = dmr_state.ifdv_bin[bin_index];
    bin_value->one_way_fn = dm1_state_far_to_near.ifdv_bin[bin_index];
    bin_value->one_way_nf = dm1_state_near_to_far.ifdv_bin[bin_index];

    return (VTSS_RC_OK);
}


mesa_rc vtss_appl_mep_dm_fd_bins_iter(const u32    *const instance,
                                      u32          *const next_instance,
                                      const u32    *const bin_number,
                                      u32          *const next_bin_number)
{
    T_N("instance %p  bin %p  *instance %u  *bin %u", instance, bin_number, (instance) ? *instance : 0, (bin_number) ? *bin_number : 0);

    if (instance == NULL) {  /* Get the first enabled instance as requested */
        if (vtss_appl_mep_instance_conf_iter(instance, next_instance) == VTSS_RC_ERROR)
            return(VTSS_RC_ERROR);
    } else
        *next_instance = *instance;

    /* Now we have an enabled instance to start with */

    /* Calculate the starting point for the search for next bin */
    *next_bin_number = bin_number ? *bin_number + 1 : 1;

    while (TRUE) {
        vtss_rc rc;
        vtss_mep_mgmt_dm_conf_t   dm_conf;
        if ((rc = vtss_mep_mgmt_dm_conf_get(*next_instance, &dm_conf)) != VTSS_RC_OK) {
            return rc;
        }
        if (*next_bin_number <= dm_conf.common.num_of_bin_fd) {
            // next bin number is valid - complete search
            break;
        }

        // find next MEP instance
        if (vtss_appl_mep_instance_conf_iter(next_instance, next_instance) == VTSS_RC_ERROR)  /* Get next enabled instance */
            return(VTSS_RC_ERROR);

        // Bin numbers start at 1
        *next_bin_number = 1;
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_ifdv_bins_iter(const u32    *const instance,
                                        u32          *const next_instance,
                                        const u32    *const bin_number,
                                        u32          *const next_bin_number)
{
    T_N("instance %p  bin %p  *instance %u  *bin %u", instance, bin_number, (instance) ? *instance : 0, (bin_number) ? *bin_number : 0);

    if (instance == NULL) {  /* Get the first enabled instance as requested */
        if (vtss_appl_mep_instance_conf_iter(instance, next_instance) == VTSS_RC_ERROR)
            return(VTSS_RC_ERROR);
    } else
        *next_instance = *instance;

    /* Now we have an enabled instance to start with */

    /* Calculate the starting point for the search for next bin */
    *next_bin_number = bin_number ? *bin_number + 1 : 1;

    while (TRUE) {
        vtss_rc rc;
        vtss_mep_mgmt_dm_conf_t   dm_conf;
        if ((rc = vtss_mep_mgmt_dm_conf_get(*next_instance, &dm_conf)) != VTSS_RC_OK) {
            return rc;
        }
        if (*next_bin_number <= dm_conf.common.num_of_bin_ifdv) {
            // next bin number is valid - complete search
            break;
        }

        // find next MEP instance
        if (vtss_appl_mep_instance_conf_iter(next_instance, next_instance) == VTSS_RC_ERROR)  /* Get next enabled instance */
            return(VTSS_RC_ERROR);

        // Bin numbers start at 1
        *next_bin_number = 1;
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_tw_status_get(const u32                   instance,
                                       vtss_appl_mep_dm_state_t    *const status)
{
    mesa_rc                   rc;
    vtss_mep_mgmt_dm_conf_t   base_conf;
    vtss_appl_mep_dm_state_t  dm1_state_far_to_near;
    vtss_appl_mep_dm_state_t  dm1_state_near_to_far;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)     return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (status == NULL)                             return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_dm_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    if ((rc = vtss_mep_mgmt_dm_prio_state_get(instance, base_conf.prio, status, &dm1_state_far_to_near, &dm1_state_near_to_far)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_prio_state_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_ownf_status_get(const u32                   instance,
                                         vtss_appl_mep_dm_state_t    *const status)
{
    mesa_rc                   rc;
    vtss_mep_mgmt_dm_conf_t   base_conf;
    vtss_appl_mep_dm_state_t  dmr_state;
    vtss_appl_mep_dm_state_t  dm1_state_far_to_near;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)     return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (status == NULL)                             return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_dm_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_conf_get failed  Instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    if ((rc = vtss_mep_mgmt_dm_prio_state_get(instance, base_conf.prio, &dmr_state, &dm1_state_far_to_near, status)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_prio_state_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_owfn_status_get(const u32                   instance,
                                         vtss_appl_mep_dm_state_t    *const status)
{
    mesa_rc                   rc;
    vtss_mep_mgmt_dm_conf_t   base_conf;
    vtss_appl_mep_dm_state_t  dmr_state;
    vtss_appl_mep_dm_state_t  dm1_state_near_to_far;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)     return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (status == NULL)                             return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_dm_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_conf_get failed  Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    if ((rc = vtss_mep_mgmt_dm_prio_state_get(instance, base_conf.prio, &dmr_state, status, &dm1_state_near_to_far)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_prio_state_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_prio_status_get(const u32                  instance,
                                         const u32                  prio,
                                         vtss_appl_mep_dm_state_t   *const dmr_state,
                                         vtss_appl_mep_dm_state_t   *const dm1_state_far_to_near,
                                         vtss_appl_mep_dm_state_t   *const dm1_state_near_to_far)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (dmr_state == NULL)                             return VTSS_RC_OK;
    if (dm1_state_far_to_near == NULL)                 return VTSS_RC_OK;
    if (dm1_state_near_to_far == NULL)                 return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_dm_prio_state_get(instance, prio, dmr_state, dm1_state_far_to_near, dm1_state_near_to_far)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_prio_state_get failed  Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_status_clear(const u32 instance)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_dm_state_clear_set(instance)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_dm_state_clear_set failed  Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_status_get(const u32                    instance,
                                     vtss_appl_mep_tst_state_t    *const state)
{
    mesa_rc                   rc;
    vtss_mep_mgmt_tst_conf_t  base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)    return (VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                             return (VTSS_RC_OK);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_tst_conf_get(instance, &base_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_tst_conf_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    if ((rc = vtss_mep_mgmt_tst_prio_state_get(instance, base_conf.prio, state)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_tst_prio_state_get failed  instance %u  rc %s", instance, error_txt(rc));
        return(rc);
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_status_clear(const u32 instance)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return (VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_tst_state_clear_set(instance)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_tst_state_clear_set failed  Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_prio_status_get(const u32                   instance,
                                          const u32                   prio,
                                          vtss_appl_mep_tst_state_t   *const state)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                 return VTSS_RC_OK;

    CRIT_ENTER_EXIT();
    if ((rc = vtss_mep_mgmt_tst_prio_state_get(instance, prio, state)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_tst_prio_state_get failed  Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    return (VTSS_RC_OK);
}

/****************************************************************************/
/*  END OF MEP management interface                                         */
/****************************************************************************/






/****************************************************************************/
/*  MEP module interface - call out - lock active                           */
/****************************************************************************/
void vtss_mep_rx_aps_info_set(const u32                      instance,
                              const mep_domain_t             domain,
                              const u8                       *aps)
{
#if defined(VTSS_SW_OPTION_EPS) || defined(VTSS_SW_OPTION_ERPS)
    u32            eps = 0;
    mep_eps_type_t eps_type = MEP_EPS_TYPE_INV;
    mesa_rc        rc = VTSS_RC_OK;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return;

    if (instance_data[instance].eps_type != MEP_EPS_TYPE_INV) {
        eps_type = instance_data[instance].eps_type;
    } else {
        rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;
    }
    if (instance_data[instance].aps_inst != APS_INST_INV) {
        eps = instance_data[instance].aps_inst;
    } else  {
        rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;
    }

    T_DG(TRACE_GRP_APS_RX, "rx aps  instance %u  %X %X\n",instance, aps[0], aps[1]);

    CRIT_EXIT_ENTER();
    if (rc == VTSS_RC_OK) {
#if defined(VTSS_SW_OPTION_EPS)
        if (eps_type == MEP_EPS_TYPE_ELPS) {
            if (eps_rx_aps_info_set(eps, instance, aps) != VTSS_RC_OK) {
                T_D("Error during APS rx set %u", instance);
            }
        }
#endif
#if defined(VTSS_SW_OPTION_ERPS)
        if (eps_type == MEP_EPS_TYPE_ERPS) {
            erps_handover_raps_pdu(aps, instance, VTSS_MEP_RAPS_DATA_LENGTH, eps);
        }
#endif
    }
#endif
}

void vtss_mep_signal_out(const u32           instance,
                         const mep_domain_t  domain)
{
#if defined(VTSS_SW_OPTION_EPS)
    u32              eps=0, forward, raps_tx;
    u8               aps_txdata[MEP_APS_DATA_LENGTH];
    mep_eps_type_t   eps_type=MEP_EPS_TYPE_INV;
    mesa_rc          rc = VTSS_RC_OK;

    T_N("instance %u  domain %u", instance, domain);

    memcpy(aps_txdata, instance_data[instance].aps_txdata, sizeof(aps_txdata));
    if (instance_data[instance].eps_type == MEP_EPS_TYPE_ERPS) {
        forward = instance_data[instance].raps_forward ? 2 : 1;
        raps_tx = instance_data[instance].raps_tx ? 2 : 1;
    } else {
        forward = 0;
        raps_tx = 0;
    }

    if (instance_data[instance].eps_type != MEP_EPS_TYPE_INV)    eps_type = instance_data[instance].eps_type;
    else    rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;
    if (instance_data[instance].aps_inst != APS_INST_INV)        eps = instance_data[instance].aps_inst;
    else    rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;

    if (raps_tx) {
        if ((rc = vtss_mep_tx_aps_info_set(instance, aps_txdata, FALSE)) != VTSS_RC_OK) {
            T_D("vtss_mep_tx_aps_info_set failed  instance %u  rc %s", instance, error_txt(rc));
        }
        if ((rc = vtss_mep_raps_transmission(instance, (raps_tx == 2))) != VTSS_RC_OK) {
            T_D("vtss_mep_raps_transmission failed  instance %u  rc %s", instance, error_txt(rc));
        }
    }

    if ((forward) && ((rc = vtss_mep_raps_forwarding(instance, (forward == 2))) != VTSS_RC_OK)) {
        T_D("vtss_mep_raps_forwarding failed  instance %u  rc %s", instance, error_txt(rc));
    }

    CRIT_EXIT_ENTER();
    if (rc == VTSS_RC_OK) {
        if (eps_type == MEP_EPS_TYPE_ELPS) {
            if (eps_signal_in(eps, instance) != VTSS_RC_OK) {
                T_D("eps_signal_in failed  instance %u", instance);
            }
        }

        if (eps_type == MEP_EPS_TYPE_ERPS) {
        }
    }
#endif
}

void vtss_mep_sf_sd_state_set(const u32           instance,
                              const mep_domain_t  domain,
                              const BOOL          sf_state,
                              const BOOL          sd_state)
{
#if defined(VTSS_SW_OPTION_EPS) || defined(VTSS_SW_OPTION_ERPS)
    u32              i;
    u32              eps_rc = VTSS_RC_OK;
    u32              eps_count;
    u16              eps_inst[SF_EPS_MAX];
    mep_eps_type_t   eps_type=MEP_EPS_TYPE_INV;
    mesa_rc          erps_rc = VTSS_RC_OK;

    T_N("instance %u  sf_state %u  sd_state %u",instance,sf_state,sd_state);

    eps_type = instance_data[instance].eps_type;
    eps_count = instance_data[instance].ssf_count;
    memcpy(eps_inst, instance_data[instance].ssf_inst, sizeof(eps_inst));

    CRIT_EXIT_ENTER();

#if defined(VTSS_SW_OPTION_EPS)
    if (eps_type == MEP_EPS_TYPE_ELPS)
    {
        for (i=0; i<eps_count; ++i)
            eps_rc += eps_sf_sd_state_set(eps_inst[i], instance, sf_state, sd_state);
    }
#endif
#if defined(VTSS_SW_OPTION_ERPS)
    if (eps_type == MEP_EPS_TYPE_ERPS) {
        for (i=0; i<eps_count; ++i) {
            erps_rc += erps_sf_sd_state_set(eps_inst[i], instance, sf_state, sd_state);
        }
    }
#endif
    if (eps_rc != VTSS_RC_OK || erps_rc != VTSS_RC_OK) {
        T_D("Error during SF/SD set %u", instance);
    }
#endif
}

void vtss_mep_status_change(const u32  instance,  const BOOL enable)
{
    mep_conf_t                  config;
    vtss_mep_mgmt_state_t       state;
    vtss_appl_mep_peer_state_t  peer_state;
    u32                         next_instance, next_peer, i;
    mesa_rc                     rc = VTSS_RC_OK;

    T_N("Enter  instance %u", instance);

    rc = vtss_mep_mgmt_conf_get(instance, &config);
    if ((rc != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE)) {
        T_D("vtss_mep_mgmt_conf_get failed  rc %s", error_txt(rc));
        return; /* Not able to get MEP */
    }

    if (enable) {    /* This instance is enabled - update the status objects */
        if ((rc = vtss_mep_mgmt_state_get(instance, &state)) != VTSS_RC_OK) {  /* Get internal MEP status */
            T_D("vtss_mep_mgmt_state_get failed  rc %s", error_txt(rc));
            return;
        }

        if ((rc = mep_status_set(instance, &state.mep)) != VTSS_RC_OK) {    /* Update MEP status object */
            T_D("mep_status_set failed  instance %u  rc %s", instance, error_txt(rc));
        }
        for (i=0; i<config.peer_count; ++i) {     /* Update peer MEP status object with all known peer MEP id */
            if ((rc = mep_status_peer_set(instance, config.peer_mep[i], &state.peer_mep[i])) != VTSS_RC_OK) {
                T_D("mep_status_peer_set failed  instance %u  peer %u  rc %s", instance, config.peer_mep[i], error_txt(rc));
            }
        }

        next_instance = instance;   /* Now we will check if any peer MEP id in the status object is no more a known id */
        next_peer = 0;
        if (mep_status_peer_get(next_instance, next_peer, &peer_state) == VTSS_RC_OK) {     /* Try and get the possible first peer MEP id */
            if (!known_peer_mep_id(next_peer, &config))
                (void)mep_status_peer_del(next_instance, next_peer);
        }
        while ((mep_status_peer_get_next(&next_instance, &next_peer, &peer_state) == VTSS_RC_OK) && (next_instance == instance)) {    /* Get next for this instance */
            if (!known_peer_mep_id(next_peer, &config))
                (void)mep_status_peer_del(next_instance, next_peer);
        }
    } else {        /* This instance is disabled - delete the status objects */
        (void)mep_status_del(instance);        /* Delete the MEP status object */

        next_instance = instance;   /* Delete the peer MEP status object for this MEP instance */
        next_peer = 0;
        if (mep_status_peer_get(next_instance, next_peer, &peer_state) == VTSS_RC_OK) {     /* Try and get the possible first peer MEP id */
            (void)mep_status_peer_del(next_instance, next_peer);
        }
        while ((mep_status_peer_get_next(&next_instance, &next_peer, &peer_state) == VTSS_RC_OK) && (next_instance == instance)) {    /* Get next for this instance */
            (void)mep_status_peer_del(next_instance, next_peer);
        }
    }
    T_N("Exit");
}

void vtss_mep_lm_notif_status_get(const u32  instance,
                                  vtss_appl_mep_lm_notif_state_t *state)
{
    (void)mep_status_lm_notif_get(instance, state);
}

void vtss_mep_lm_notif_status_set(const u32  instance,
                                  vtss_appl_mep_lm_notif_state_t *state)
{
    T_NG(TRACE_GRP_LM, "Enter");

    if (state) {
        (void)mep_status_lm_notif_set(instance, state);
    } else {
        (void)mep_status_lm_notif_del(instance);
    }
}

void vtss_mep_lm_hli_status_get(const u32  instance,
                                const u32 peer_id,
                                vtss_appl_mep_lm_hli_state_t *state)
{
    T_N("Enter");

    (void)mep_status_lm_hli_get(instance, peer_id, state);
}

void vtss_mep_lm_hli_status_set(const u32 instance,
                                const u32 peer_id,
                                vtss_appl_mep_lm_hli_state_t *state)
{
    T_N("Enter");

    if (state) {
        (void)mep_status_lm_hli_set(instance, peer_id, state);
    } else {
        (void)mep_status_lm_hli_del(instance, peer_id);
    }
}

void vtss_mep_syslog(vtss_mep_syslog_t type, const u32 param1, const u32 param2)
{
    T_N("Enter");

    switch(type) {
        case VTSS_MEP_ENTER_AIS:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_I("MEP-ENTER_AIS: MEP %d, has entered an AIS defect condition", param1);
#endif /* VTSS_SW_OPTION_SYSLOG */
            break;
        case VTSS_MEP_EXIT_AIS:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_I("MEP-EXIT_AIS: MEP %d, has exited an AIS defect condition", param1);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        case VTSS_MEP_CROSSCHECK_SERVICE_UP:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_I("MEP-CROSSCHECK_SERVICE_UP: MEP %d, CSI or MA is up, as it receives CC messages from all remote, statically configured MEPs", param1);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        case VTSS_MEP_CROSSCHECK_MEP_MISSING:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_W("MEP-CROSSCHECK_MEP_MISSING: MEP %d, peer MEP %d does not come up during the crosscheck start timeout interval", param1, param2);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        case VTSS_MEP_CROSS_CONNECTED_SERVICE:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_I("MEP-CROSS_CONNECTED_SERVICE: MEP %d, the CC message contains a CSI ID or MA ID different from what is configured locally", param1);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        case VTSS_MEP_CONFIG_ERROR:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_W("MEP-CONF_ERR: MEP %d, has received a CC message with its own MPID but a different source MAC address", param1);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        case VTSS_MEP_FORWARDING_LOOP:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_W("MEP-FORWARDING_LOOP: MEP %d, has received a CC messages with its own MPID and source MAC address", param1);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        case VTSS_MEP_CROSSCHECK_MEP_UNKNOWN:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_W("MEP-CROSSCHECK_MEP_UNKNOWN: MEP %d, the remote MEP that is received is not in the configured static list", param1);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        case VTSS_MEP_REMOTE_MEP_DOWN:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_I("MEP-REMOTE_MEP_DOWN: MEP %d, from peer MEP %d a CC message is received with a zero hold time or CC times out", param1, param2);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        case VTSS_MEP_REMOTE_MEP_UP:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_I("MEP-REMOTE_MEP_UP: MEP %d, from peer MEP %d CC message is received", param1, param2);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        default:
            T_D("unknown type!!");
            break;
    }
}

void vtss_mep_mac_get(u32 port,  u8 mac[VTSS_APPL_MEP_MAC_LENGTH])
{
    T_N("Enter");

    CRIT_EXIT_ENTER();

    if (conf_mgmt_mac_addr_get(mac, port+1) < 0)
        T_W("Error getting own MAC:\n");
}

BOOL vtss_mep_check_forwarding(u32 i_port,  u32 *f_port,  u8 *mac,  u32 vid)
{
    u32                     i, dest;
    mesa_vid_mac_t          vid_mac;
    mesa_mac_table_entry_t  entry;

    T_N("Enter");

    CRIT_EXIT_ENTER();

    memset(&entry, 0, sizeof(entry));
    vid_mac.vid = vid;
    memcpy(vid_mac.mac.addr, mac, sizeof(vid_mac.mac.addr));

    if (mesa_mac_table_get(NULL,  &vid_mac,  &entry) != VTSS_RC_OK)
        return FALSE;

    /* 'mac' found in MAC table */
    if (entry.locked)     /* It must be a dynamic entry */
        if (mesa_mac_table_get_next(NULL,  &vid_mac,  &entry) != VTSS_RC_OK)
            return FALSE;

    if (entry.locked)
        return FALSE;

    /* 'mac' found in MAC table and it is dynamic */
    /* Find and count destination ports:          */
    for (i = 0, dest = 0; i < mep_caps.mesa_port_cnt; ++i) {
        if ((entry.destination[i]) && (in_port_conv[i] != 0xFFFF)) {
            dest++;
            *f_port = i;
        }
    }

    if (dest != 1)            return FALSE;    /* Only forward if one destination port is found */
    if (*f_port == i_port)    return FALSE;    /* Only forward if destination port is different from ingress port */

    return TRUE;
}

u32 vtss_mep_port_count(void)
{
    CRIT_EXIT_ENTER();

    return(port_isid_port_count(VTSS_ISID_LOCAL));
}








































































void vtss_mep_port_conf_disable(const u32       instance,
                                const u32       port,
                                const BOOL      disable)
{
    port_vol_conf_t            port_conf;

    CRIT_EXIT_ENTER();

    if (port_vol_conf_get(PORT_USER_MEP, VTSS_ISID_START, port, &port_conf) != VTSS_RC_OK) {
        T_I("port_vol_conf_set failed  port %d", port);
        return;
    }

    if (port_conf.disable != disable) {
        port_conf.disable = disable;

        if (port_vol_conf_set(PORT_USER_MEP, VTSS_ISID_START, port, &port_conf) != VTSS_RC_OK) {
            T_I("port_vol_conf_set failed  port %d", port);
        }
    }
}

void vtss_mep_timer_start(vtss::Timer *timer)
{
    if (!timer) {
        T_E("vtss_timer_start() failed");
        return;
    }

    CRIT_EXIT_ENTER();

    if (vtss_timer_start(timer) != VTSS_RC_OK) {
        T_E("vtss_timer_start() failed");
    }
}

void vtss_mep_timer_cancel(vtss::Timer *timer)
{
    if (!timer) { return; }

    CRIT_EXIT_ENTER();

    if (vtss_timer_cancel(timer) != VTSS_RC_OK) {
        T_E("vtss_timer_cancel() failed");
    }
}

void vtss_mep_timestamp_get(mesa_timestamp_t *timestamp, u32 *tc)
{
    vtss_tod_gettimeofday(0, timestamp, tc);
}

/* The unit of the return value is nanosecond or microsecond */
i32 vtss_mep_delay_calc(const mesa_timestamp_t *x,
                        const mesa_timestamp_t *y,
                        const BOOL             is_ns)
{
    i64          diff=0;

    vtss_tod_sub_TimeStamps(&diff, x, y);

    if (diff < 0){
        if(diff > -100) {
            diff = -diff;
        }
        T_DG(TRACE_GRP_DM, "(x - y) < 0 - Operator x.s %u-%u x.ns %u y.s %u-%u y.ns %u", x->sec_msb, x->seconds, x->nanoseconds, y->sec_msb, y->seconds, y->nanoseconds);
        T_DG(TRACE_GRP_DM, "diff %" PRIi64 "", diff);
    }

    if (is_ns)
    {
        return diff;
    }
    else
    {
        diff = diff / 1000;
        if ((diff % 1000) > 500)
            diff++; /*round up*/
        return diff;
    }
}

void mep_model_critd_enter(const char* const file,  const int line)
{
    critd_enter(&crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, file, line);
}

void mep_model_critd_exit(const char* const file,  const int line)
{
    critd_exit(&crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, file, line);
}
/****************************************************************************/
/*  END OF MEP module interface - call out                                  */
/****************************************************************************/




/****************************************************************************/
/*  MEP module interface - call in - take lock                              */
/****************************************************************************/
mesa_rc mep_eps_aps_register(const u32            instance,
                             const u32            eps_inst,
                             const mep_eps_type_t eps_type,
                             const BOOL           enable)
{
    BOOL invalid = FALSE;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER_EXIT();
    if (enable && instance_data[instance].aps_inst != APS_INST_INV)
        invalid = TRUE;
    if (!enable && instance_data[instance].aps_inst != eps_inst)
        invalid = TRUE;
    if (enable && (((instance_data[instance].aps_type == VTSS_APPL_MEP_L_APS) && (eps_type == MEP_EPS_TYPE_ERPS)) ||
                   ((instance_data[instance].aps_type == VTSS_APPL_MEP_R_APS) && (eps_type == MEP_EPS_TYPE_ELPS))))
        invalid = TRUE;

    if (!invalid) {
        if (enable) {/* Add EPS as APS instance */
            instance_data[instance].aps_inst = eps_inst;
            instance_data[instance].eps_type = eps_type;
        } else {/* Remove EPS as APS instance */
            instance_data[instance].aps_inst = APS_INST_INV;
            if (instance_data[instance].ssf_count == 0) {
                instance_data[instance].eps_type = MEP_EPS_TYPE_INV;    /* No EPS Related */
            }
        }
    }

    return (invalid ? (mesa_rc)VTSS_APPL_MEP_RC_INVALID_PARAMETER : VTSS_RC_OK);
}

mesa_rc mep_eps_sf_register(const u32            instance,
                            const u32            eps_inst,
                            const mep_eps_type_t eps_type,
                            const BOOL           enable)
{
    u32 i;
    BOOL invalid = FALSE;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER_EXIT();
    if (enable && (((instance_data[instance].aps_type == VTSS_APPL_MEP_L_APS) && (eps_type == MEP_EPS_TYPE_ERPS)) ||
                   ((instance_data[instance].aps_type == VTSS_APPL_MEP_R_APS) && (eps_type == MEP_EPS_TYPE_ELPS)))) {
        invalid = TRUE;
    }
    if (enable && instance_data[instance].ssf_count >= SF_EPS_MAX) {
        invalid = TRUE;
    }
    for (i=0; i<instance_data[instance].ssf_count; ++i) {
        if (instance_data[instance].ssf_inst[i] == eps_inst) {
            break;
        }
    }
    if (enable && (i < instance_data[instance].ssf_count))      invalid = TRUE;
    if (!enable && (i == instance_data[instance].ssf_count))    invalid = TRUE;

    if (!invalid)
    {
        if (enable)
        {/* Add EPS as SSF instance */
            instance_data[instance].ssf_inst[i] = eps_inst;
            instance_data[instance].ssf_count++;
            instance_data[instance].eps_type = eps_type;
        }
        else
        {/* Remove EPS as SSF instance */
            for (i=i; i<(instance_data[instance].ssf_count-1); ++i)
                instance_data[instance].ssf_inst[i] = instance_data[instance].ssf_inst[i+1];
            if (instance_data[instance].ssf_count)  instance_data[instance].ssf_count--;

            if ((instance_data[instance].ssf_count == 0) && (instance_data[instance].aps_inst == APS_INST_INV))
                instance_data[instance].eps_type = MEP_EPS_TYPE_INV;    /* No EPS Related */
        }
    }

    return (invalid ? (mesa_rc)VTSS_APPL_MEP_RC_INVALID_PARAMETER : VTSS_RC_OK);
}

mesa_rc mep_tx_aps_info_set(const u32  instance,
                            const u32  eps_inst,
                            const u8   *aps,
                            const BOOL event)
{
    mesa_rc rc = VTSS_RC_OK;
    BOOL    created = FALSE;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                  return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER_EXIT();
    if (instance_data[instance].eps_type == MEP_EPS_TYPE_INV)    return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    if ((instance_data[instance].aps_inst == APS_INST_INV) ||
        (instance_data[instance].aps_inst != eps_inst))          return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    memcpy(instance_data[instance].aps_txdata, aps, (instance_data[instance].eps_type == MEP_EPS_TYPE_ERPS) ? VTSS_MEP_RAPS_DATA_LENGTH : VTSS_MEP_LAPS_DATA_LENGTH);
    created = instance_data[instance].enable;

    T_DG(TRACE_GRP_APS_TX, "tx aps  instance %u  %X %X\n", instance, aps[0], aps[1]);

    if ((created) && ((rc = vtss_mep_tx_aps_info_set(instance, aps, event)) != VTSS_RC_OK)) {
        T_D("vtss_mep_tx_aps_info_set failed  instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    return VTSS_RC_OK;
}

mesa_rc mep_signal_in(const u32 instance,
                      const u32 eps_inst)
{
    u32     i;
    mesa_rc rc = VTSS_RC_OK;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                  return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER_EXIT();
    if (instance_data[instance].eps_type == MEP_EPS_TYPE_INV)    return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    for (i=0; i<instance_data[instance].ssf_count; ++i) {
        if (instance_data[instance].ssf_inst[i] == eps_inst) {
            break;
        }
    }
    if ((i==instance_data[instance].ssf_count) &&
        ((instance_data[instance].aps_inst == APS_INST_INV) ||
         (instance_data[instance].aps_inst != eps_inst)))        return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    if ((rc = vtss_mep_signal_in(instance)) != VTSS_RC_OK) {
        T_D("vtss_mep_signal_in failed  instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    return VTSS_RC_OK;
}

mesa_rc mep_raps_forwarding(const u32  instance,
                            const u32  eps_inst,
                            const BOOL enable)
{
    mesa_rc rc = VTSS_RC_OK;
    BOOL    created = FALSE;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                   return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER_EXIT();
    if (instance_data[instance].eps_type != MEP_EPS_TYPE_ERPS)    return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    if ((instance_data[instance].aps_inst == APS_INST_INV) ||
        (instance_data[instance].aps_inst != eps_inst))           return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    instance_data[instance].raps_forward = enable;
    created = instance_data[instance].enable;

//T_D("forward enable  instance %lu  enable %u\n", instance, enable);
    if ((created) && ((rc = vtss_mep_raps_forwarding(instance, enable)) != VTSS_RC_OK)) {
        T_D("vtss_mep_raps_forwarding failed  instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    return VTSS_RC_OK;
}

mesa_rc mep_raps_transmission(const u32  instance,
                              const u32  eps_inst,
                              const BOOL enable)
{
    mesa_rc rc = VTSS_RC_OK;
    BOOL    created = FALSE;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                   return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER_EXIT();
    if (instance_data[instance].eps_type != MEP_EPS_TYPE_ERPS)    return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    if ((instance_data[instance].aps_inst == APS_INST_INV) ||
        (instance_data[instance].aps_inst != eps_inst))           return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    instance_data[instance].raps_tx = enable;
    created = instance_data[instance].enable;

//T_D("tx enable  instance %lu  enable %u\n", instance, enable);
    if ((created) && ((rc = vtss_mep_raps_transmission(instance, enable)) != VTSS_RC_OK)) {
        T_D("vtss_mep_raps_transmission failed  instance %u  rc %s", instance, error_txt(rc));
        return rc;
    }

    return VTSS_RC_OK;

}

void mep_port_protection_change(const u32   w_port,
                                const u32   p_port,
                                const BOOL  active)
{
    CRIT_ENTER_EXIT();
    mep_model_port_protection_change(w_port, p_port, active);
}

void mep_port_protection_create(mep_eps_architecture_t  architecture,
                                const u32               w_port,
                                const u32               p_port)
{
    CRIT_ENTER_EXIT();
    mep_model_port_protection_create((architecture == MEP_EPS_ARCHITECTURE_1P1) ? TRUE : FALSE, w_port, p_port);
}

void mep_port_protection_delete(const u32  w_port,
                                const u32  p_port)
{
    CRIT_ENTER_EXIT();
    mep_model_port_protection_delete(w_port, p_port);
}

void mep_ring_protection_block(const u32 port,  BOOL block)
{
    CRIT_ENTER_EXIT();
    mep_model_ring_protection_block(port, block);
}

void mep_timer_call_back(struct vtss::Timer *timer)
{
    CRIT_ENTER_EXIT();
    vtss_mep_timer_cb(timer);
}

/****************************************************************************/
/*  END OF MEP module interface - call in                                   */
/****************************************************************************/


/****************************************************************************/
/*  MEP Initialize module                                                   */
/****************************************************************************/
#ifdef VTSS_SW_OPTION_PRIVATE_MIB
VTSS_PRE_DECLS void mep_mib_init(void);
#endif
#ifdef VTSS_SW_OPTION_JSON_RPC
VTSS_PRE_DECLS void vtss_appl_mep_json_init(void);
#endif
#include "mep_model.h"
extern "C" int mep_icli_cmd_register();
mesa_rc mep_init(vtss_init_data_t *data)
{
    u32               base_rc, i;
    vtss_isid_t       isid = data->isid;

    if (data->cmd == INIT_CMD_EARLY_INIT) {
        /* Initialize and register trace ressources */
        trace_init = TRUE;
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);
    }

    T_D("enter, cmd: %d, isid: %u, flags: 0x%x", data->cmd, data->isid, data->flags);

    switch (data->cmd)
    {
        case INIT_CMD_INIT:
            T_D("INIT");

            /* initialize critd */
            critd_init(&crit, "MEP Crit", VTSS_MODULE_ID_MEP, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);

            los_state.set_all();
            for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i) {
                vtss_clear(instance_data[i]);
                instance_data[i].aps_inst = APS_INST_INV;
            }
            base_rc = vtss_mep_init();
            if (base_rc)        T_D("Error during init");

            for (i=0; i<mep_caps.mesa_port_cnt; ++i) {
                in_port_conv[i] = out_port_conv[i] = i;
                conv_los_state[i] = TRUE;
                out_port_1p1[i] = FALSE;
                out_port_tx[i] = TRUE;
            }

            CRIT_EXIT;

#ifdef VTSS_SW_OPTION_ICFG
            // Initialize ICLI "show running" configuration
            VTSS_RC(mep_icfg_init());
#endif

#ifdef VTSS_SW_OPTION_PRIVATE_MIB
            mep_mib_init(); /* Currently the MEP MIB is not allowed on Cisco target */
#endif
#ifdef VTSS_SW_OPTION_JSON_RPC
            vtss_appl_mep_json_init();
#endif
            mep_icli_cmd_register();

            mep_model_init(MEP_MODEL_INIT_INIT);
            break;
        case INIT_CMD_START:
            T_D("START");
            break;
        case INIT_CMD_CONF_DEF:
            T_D("CONF_DEF");
            if (isid == VTSS_ISID_GLOBAL) {
                restore_to_default();
            }
            break;
        case INIT_CMD_MASTER_UP:
            T_D("MASTER_UP");
            CRIT_ENTER;
            mep_model_init(MEP_MODEL_INIT_HOOK);
            CRIT_EXIT;
            restore_to_default();
            break;
        case INIT_CMD_MASTER_DOWN:
            T_D("MASTER_DOWN");
            break;
        case INIT_CMD_SWITCH_ADD:
            T_D("SWITCH_ADD");
            CRIT_ENTER;
            mep_model_init(MEP_MODEL_INIT_DONE);
            CRIT_EXIT;
            break;
        case INIT_CMD_SWITCH_DEL:
            T_D("SWITCH_DEL");
            break;
        case INIT_CMD_SUSPEND_RESUME:
            T_D("SUSPEND_RESUME");
            break;
        default:
            break;
    }

    T_D("exit");
    return VTSS_RC_OK;
}



/****************************************************************************/
/*  Assorted internal functions.                                            */
/****************************************************************************/

static mesa_rc conv_conf_ext_int(const vtss_appl_mep_conf_t *a, mep_conf_t *b) {
    vtss_ifindex_elm_t flow, port;
    b->enable = a->enable;
    b->mode = a->mode;
    b->direction = a->direction;

    VTSS_RC(vtss_ifindex_decompose(a->flow, &flow));
    b->flow = flow.ordinal;
    switch (flow.iftype) {
        case VTSS_IFINDEX_TYPE_PORT:
            b->domain = VTSS_APPL_MEP_PORT;
            break;

        case VTSS_IFINDEX_TYPE_VLAN:
            b->domain = VTSS_APPL_MEP_VLAN;
            break;

        case VTSS_IFINDEX_TYPE_EVC:
            b->domain = VTSS_APPL_MEP_EVC;
            break;

        case VTSS_IFINDEX_TYPE_MPLS_LINK:
            b->domain = VTSS_APPL_MEP_MPLS_LINK;
            break;

        case VTSS_IFINDEX_TYPE_MPLS_TUNNEL:
            b->domain = VTSS_APPL_MEP_MPLS_TUNNEL;
            break;

        case VTSS_IFINDEX_TYPE_MPLS_PW:
            b->domain = VTSS_APPL_MEP_MPLS_PW;
            break;

        case VTSS_IFINDEX_TYPE_MPLS_LSP:
            b->domain = VTSS_APPL_MEP_MPLS_LSP;
            break;

        default:
           return VTSS_RC_ERROR;
    }

    VTSS_RC(vtss_ifindex_decompose(a->port, &port));
    switch (port.iftype) {
        case VTSS_IFINDEX_TYPE_PORT:
            b->port = port.ordinal;
            break;

        default:
           return VTSS_RC_ERROR;
    }

    b->level = a->level;
    b->vid = a->vid;
    b->voe = a->voe;
    b->mac = a->mac;
    b->format = a->format;
    memcpy((void *)b->name, a->name, sizeof(b->name));
    memcpy((void *)b->meg, a->meg, sizeof(b->meg));
    b->mep = a->mep;
    b->peer_count = a->peer_count;
    memcpy((void *)b->peer_mep, a->peer_mep, sizeof(b->peer_mep));
    memcpy((void *)b->peer_mac, a->peer_mac, sizeof(b->peer_mac));
    b->out_of_service = a->out_of_service;
    b->evc_pag = a->evc_pag;
    b->evc_qos = a->evc_qos;
    return VTSS_RC_OK;
}

static void conv_conf_int_ext(const mep_conf_t *a, vtss_appl_mep_conf_t *b) {
    b->enable = a->enable;
    b->mode = a->mode;
    b->direction = a->direction;
    b->port = VTSS_IFINDEX_NONE;    /* Indication that the following ifindex could not be calculated */
    b->flow = VTSS_IFINDEX_NONE;    /* Indication that the following ifindex could not be calculated */
    (void)vtss_ifindex_from_port(VTSS_ISID_LOCAL, a->port, &b->port);

    switch (a->domain) {
        case VTSS_APPL_MEP_PORT:
            (void)vtss_ifindex_from_port(VTSS_ISID_LOCAL, a->flow, &b->flow);
            break;

        case VTSS_APPL_MEP_VLAN:
            (void)vtss_ifindex_from_vlan(a->flow, &b->flow);
            break;

        case VTSS_APPL_MEP_EVC:
            (void)vtss_ifindex_from_evc(a->flow, &b->flow);
            break;

        case VTSS_APPL_MEP_MPLS_LINK:
            (void)vtss_ifindex_from_mpls_link(a->flow, &b->flow);
            break;

        case VTSS_APPL_MEP_MPLS_TUNNEL:
            (void)vtss_ifindex_from_mpls_tunnel(a->flow, &b->flow);
            break;

        case VTSS_APPL_MEP_MPLS_PW:
            (void)vtss_ifindex_from_mpls_pw(a->flow, &b->flow);
            break;

        case VTSS_APPL_MEP_MPLS_LSP:
            (void)vtss_ifindex_from_mpls_lsp(a->flow, &b->flow);
            break;
    }

    b->level = a->level;
    b->vid = a->vid;
    b->voe = a->voe;
    b->mac = a->mac;
    b->format = a->format;
    memcpy((void *)b->name, a->name, sizeof(b->name));
    memcpy((void *)b->meg, a->meg, sizeof(b->meg));
    b->mep = a->mep;
    b->peer_count = a->peer_count;
    memcpy((void *)b->peer_mep, a->peer_mep, sizeof(b->peer_mep));
    memcpy((void *)b->peer_mac, a->peer_mac, sizeof(b->peer_mac));
    b->out_of_service = a->out_of_service;
    b->evc_pag = a->evc_pag;
    b->evc_qos = a->evc_qos;
}

static void conv_def_int_ext(const mep_default_conf_t *a, vtss_appl_mep_default_conf_t *b) {
    conv_conf_int_ext(&a->config, &b->config);
    b->peer_conf = a->peer_conf;
    b->pm_conf = a->pm_conf;
    b->syslog_conf = a->syslog_conf;
    b->tlv_conf = a->tlv_conf;
    b->cc_conf = a->cc_conf;
    b->lm_conf = a->lm_conf;
    b->lm_avail_conf = a->lm_avail_conf;
    b->lm_hli_conf = a->lm_hli_conf;
    b->lm_sdeg_conf = a->lm_sdeg_conf;
    b->dm_conf = a->dm_conf;
    b->aps_conf = a->aps_conf;
    b->lt_conf = a->lt_conf;
    b->lb_conf = a->lb_conf;
    b->ais_conf = a->ais_conf;
    b->lck_conf = a->lck_conf;
    b->tst_conf = a->tst_conf;
    b->tst_common_conf = a->tst_common_conf;
    b->lb_common_conf = a->lb_common_conf;
    b->test_prio_conf = a->test_prio_conf;
    b->dm_common_conf = a->dm_common_conf;
    b->dm_prio_conf = a->dm_prio_conf;
    //b->client_conf = a->client_conf; // TODO
    b->client_flow_conf = a->client_flow_conf;
    b->bfd_conf = a->bfd_conf;
    b->rt_conf = a->rt_conf;
}

static mesa_rc conv_mep_client_flow_conf_ext_to_int(vtss_ifindex_t ifindex, u32 *flow, mep_domain_t *domain)
{
    vtss_ifindex_elm_t ifindex_elm;

    VTSS_RC(vtss_ifindex_decompose(ifindex, &ifindex_elm));
    *flow = ifindex_elm.ordinal;
    switch (ifindex_elm.iftype) {
        case VTSS_IFINDEX_TYPE_PORT:         *domain = VTSS_APPL_MEP_PORT;        break;
        case VTSS_IFINDEX_TYPE_VLAN:         *domain = VTSS_APPL_MEP_VLAN;        break;
        case VTSS_IFINDEX_TYPE_EVC:          *domain = VTSS_APPL_MEP_EVC;         break;
        case VTSS_IFINDEX_TYPE_MPLS_LINK:    *domain = VTSS_APPL_MEP_MPLS_LINK;   break;
        case VTSS_IFINDEX_TYPE_MPLS_TUNNEL:  *domain = VTSS_APPL_MEP_MPLS_TUNNEL; break;
        case VTSS_IFINDEX_TYPE_MPLS_PW:      *domain = VTSS_APPL_MEP_MPLS_PW;     break;
        case VTSS_IFINDEX_TYPE_MPLS_LSP:     *domain = VTSS_APPL_MEP_MPLS_LSP;    break;
        default: return VTSS_RC_ERROR;
    }
    return VTSS_RC_OK;
}

static mesa_rc conv_mep_client_flow_conf_int_to_ext(u32 flow, mep_domain_t domain, vtss_ifindex_t *ifindex)
{
    switch (domain) {
        case VTSS_APPL_MEP_PORT:        VTSS_RC(vtss_ifindex_from_port(VTSS_ISID_LOCAL, flow, ifindex)); break;
        case VTSS_APPL_MEP_VLAN:        VTSS_RC(vtss_ifindex_from_vlan(flow, ifindex)); break;
        case VTSS_APPL_MEP_EVC:         VTSS_RC(vtss_ifindex_from_evc(flow, ifindex)); break;
        case VTSS_APPL_MEP_MPLS_LINK:   VTSS_RC(vtss_ifindex_from_mpls_link(flow, ifindex)); break;
        case VTSS_APPL_MEP_MPLS_TUNNEL: VTSS_RC(vtss_ifindex_from_mpls_tunnel(flow, ifindex)); break;
        case VTSS_APPL_MEP_MPLS_PW:     VTSS_RC(vtss_ifindex_from_mpls_pw(flow, ifindex)); break;
        case VTSS_APPL_MEP_MPLS_LSP:    VTSS_RC(vtss_ifindex_from_mpls_lsp(flow, ifindex)); break;
        default:                        return VTSS_RC_ERROR;
    }
    return VTSS_RC_OK;
}

static void restore_to_default(void)
{
    uint                i, rc;
    mep_default_conf_t  def_conf;
//    vtss_appl_mep_g8113_2_bfd_auth_conf_t bfd_auth;

    CRIT_ENTER_EXIT();
    vtss_mep_mgmt_default_conf_get(&def_conf);

    for (i = 0; vtss_mep_mgmt_conf_iter(&i); ++i) {
        if ((rc = vtss_mep_mgmt_conf_set(i, &def_conf.config)) == VTSS_APPL_MEP_RC_VOLATILE) {
            if ((rc = vtss_mep_mgmt_volatile_conf_set(i, &def_conf.config)) != VTSS_RC_OK) {
                T_D("vtss_mep_mgmt_volatile_conf_set failed  instance %u  rc %s", i, error_txt(rc));
            }
        }
    }

    if ((rc = vtss_mep_mgmt_tlv_conf_set(&def_conf.tlv_conf)) != VTSS_RC_OK) {
        T_D("vtss_mep_mgmt_tlv_conf_set failed rc %s", error_txt(rc));
    }

//    memset(&bfd_auth, 0, sizeof(bfd_auth));
//    for (i = 0; i < VTSS_APPL_MEP_BFD_AUTH_KEYS_MAX; ++i) {
//        rc = vtss_appl_mep_g8113_2_bfd_auth_conf_set(i, &bfd_auth);
//        if (rc != VTSS_RC_OK)
//            T_D("Error returned from BFD auth %u\n", i);
//    }

    VTSS_OS_MSLEEP(100);    /* Wait for async MEP disable */

    vtss_clear(instance_data);

    for (i=0; i<mep_caps.mesa_port_cnt; ++i) {
        in_port_conv[i] = out_port_conv[i] = i;
        conv_los_state[i] = FALSE;
        out_port_1p1[i] = FALSE;
        out_port_tx[i] = TRUE;
    }

    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
    {
        instance_data[i].aps_inst = APS_INST_INV;
        instance_data[i].domain = VTSS_APPL_MEP_PORT;
    }
}

static BOOL known_peer_mep_id(u32 peer,  mep_conf_t *config)
{
    u32 i;

    for (i=0; i<config->peer_count; ++i) {
        if (config->peer_mep[i] == peer)
            break;
    }
    return ((i == config->peer_count) ? FALSE : TRUE);
}

/****************************************************************************/
/*  MEP module interface - Latching Loopback                                */
/****************************************************************************/


















































































}  // mep_g2

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
