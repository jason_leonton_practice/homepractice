/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#ifndef _MEP_EXPOSE_H_
#define _MEP_EXPOSE_H_

mesa_rc mep_status_get_first(u32 *const instance,  vtss_appl_mep_state_t *status);
mesa_rc mep_status_get_next(u32 *const instance,  vtss_appl_mep_state_t *status);
mesa_rc mep_status_get(const u32 instance,  vtss_appl_mep_state_t  *const status);
mesa_rc mep_status_set(const u32 instance,  vtss_appl_mep_state_t *state);
mesa_rc mep_status_del(const u32 instance);

mesa_rc mep_status_peer_get_first(u32 *const instance,  u32 *const peer,  vtss_appl_mep_peer_state_t *status);
mesa_rc mep_status_peer_get_next(u32 *const instance,  u32 *const peer,  vtss_appl_mep_peer_state_t *status);
mesa_rc mep_status_peer_get(const u32 instance,  const u32 peer,  vtss_appl_mep_peer_state_t  *const status);
mesa_rc mep_status_peer_set(const u32 instance,  const u32 peer,  vtss_appl_mep_peer_state_t *status);
mesa_rc mep_status_peer_del(const u32 instance,  const u32 peer);

mesa_rc mep_status_lm_notif_get_first(u32 *const instance,  vtss_appl_mep_lm_notif_state_t *status);
mesa_rc mep_status_lm_notif_get_next(u32 *const instance,  vtss_appl_mep_lm_notif_state_t *status);
mesa_rc mep_status_lm_notif_get(const u32 instance,  vtss_appl_mep_lm_notif_state_t  *const status);
mesa_rc mep_status_lm_notif_set(const u32 instance,  vtss_appl_mep_lm_notif_state_t *status);
mesa_rc mep_status_lm_notif_del(const u32 instance);

mesa_rc mep_status_lm_hli_get_first(u32 *const instance, u32 *const peer_id, vtss_appl_mep_lm_hli_state_t *status);
mesa_rc mep_status_lm_hli_get_next(u32 *const instance, u32 *const peer_id, vtss_appl_mep_lm_hli_state_t *status);
mesa_rc mep_status_lm_hli_get(const u32 instance,  const u32 peer_id,  vtss_appl_mep_lm_hli_state_t  *const status);
mesa_rc mep_status_lm_hli_set(const u32 instance,  const u32 peer_id,  vtss_appl_mep_lm_hli_state_t *status);
mesa_rc mep_status_lm_hli_del(const u32 instance,  const u32 peer_id);


#endif // _MEP_EXPOSE_H_
