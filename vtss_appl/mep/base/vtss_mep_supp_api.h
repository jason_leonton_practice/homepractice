/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _VTSS_MEP_SUPP_API_H_
#define _VTSS_MEP_SUPP_API_H_

#include "main_types.h"
#include "mscc/ethernet/switch/api.h"
#include "vtss/appl/mep.h"
#include "vtss_mep_api.h"
#include "../mep_ll_pdu_defs.h"

namespace mep_g1 {

/*
 * Convert array of octets in network order to 32-bit unsigned integer
 */
#define string_to_var(string)  (((string)[0]<<24) | ((string)[1]<<16) | ((string)[2]<<8) | (string)[3])

/*
 * Convert 32-bit unsigned integer to array of octets in network order
 */
#define var_to_string(string, var) {(string)[0] = (var&0xFF000000)>>24;   (string)[1] = (var&0x00FF0000)>>16;   (string)[2] = (var&0x0000FF00)>>8;   (string)[3] = (var&0x000000FF);}

#define VTSS_MEP_SUPP_CREATED_MAX      mep_caps.appl_mep_instance_max   /* Max number of created MEP */
#define VTSS_MEP_SUPP_PEER_MAX         VTSS_APPL_MEP_PEER_MAX          /* Max number of peer MEP */
#define VTSS_MEP_SUPP_SERVICE_VOE_MAX  VTSS_OAM_PATH_SERVICE_VOE_CNT    /* Max number of service VOE */
#define VTSS_MEP_SUPP_PORT_VOE_MAX     VTSS_OAM_PORT_VOE_CNT            /* Max number of port VOE */

#define VTSS_MEP_SUPP_RC_OK                 0   /* Management operation is ok */
#define VTSS_MEP_SUPP_RC_INVALID_PARAMETER  1   /* Invalid parameter */
#define VTSS_MEP_SUPP_RC_NO_RESOURCES       2   /* No resources -  */
#define VTSS_MEP_SUPP_RC_NO_VOE             3   /* VOE not supported -  */
#define VTSS_MEP_SUPP_RC_VOE_ERROR          4   /* VOE returned error  */

#define VTSS_MEP_SUPP_LAPS_DATA_LENGTH    4
#define VTSS_MEP_SUPP_RAPS_DATA_LENGTH    VTSS_MEP_APS_DATA_LENGTH
#define VTSS_MEP_SUPP_MAC_LENGTH          VTSS_APPL_MEP_MAC_LENGTH
#define VTSS_MEP_SUPP_MEG_CODE_LENGTH     VTSS_APPL_MEP_MEG_CODE_LENGTH
#define VTSS_MEP_SUPP_LBR_MAX             10
#define VTSS_MEP_SUPP_LTR_MAX             10
#define VTSS_MEP_SUPP_DM_MAX              10    /* A risk to cause stack overflow if the value is too large */
#define VTSS_MEP_SUPP_CLIENT_FLOWS_MAX    10    /* Max number of flows to insert AIS/LCK */
#define VTSS_MEP_SUPP_OUI_SIZE            VTSS_APPL_MEP_OUI_SIZE
#define VTSS_MEP_SUPP_PRIO_MAX            VTSS_APPL_MEP_PRIO_MAX
#define VTSS_MEP_SUPP_TEST_MAX            VTSS_APPL_MEP_TEST_MAX
#define VTSS_MEP_SUPP_TEST_SIZE_MAX       mep_caps.mesa_port_frame_length_max    /* The maximum Test frame size - includung DMAC + SMAC + T/L + FCS = 18 bytes - excluding any tags that might be added */
#define VTSS_MEP_SUPP_TEST_SIZE_MIN       64      /* The minimum TST frame size - includung DMAC + SMAC + T/L + FCS = 18 bytes - excluding any tags that might be added  */
#define VTSS_MEP_SUPP_SAT_VID_UNUSED      VTSS_MEP_MGMT_SAT_VID_UNUSED
#define VTSS_MEP_SUPP_SAT_VID_UNTAGGED    VTSS_MEP_MGMT_SAT_VID_UNTAGGED
#define VTSS_MEP_SUPP_SAT_VID_PRIO        VTSS_MEP_MGMT_SAT_VID_PRIO
#define VTSS_MEP_SUPP_TEST_RATE_MAX       mep_caps.appl_mep_test_rate_max     /* 250000 Kbps = 2,5 Gbps */
#define VTSS_MEP_SUPP_LBM_TO_SEND_INFINITE  VTSS_APPL_MEP_LB_TO_SEND_INFINITE

/****************************************************************************/
/*  MEP upper logic call in to lower support                                */
/****************************************************************************/

#define  VTSS_MEP_SUPP_FLOW_MASK_PORT         0x0001
#define  VTSS_MEP_SUPP_FLOW_MASK_PORT_P       0x0002
#define  VTSS_MEP_SUPP_FLOW_MASK_CINVID       0x0004
#define  VTSS_MEP_SUPP_FLOW_MASK_SINVID       0x0008
#define  VTSS_MEP_SUPP_FLOW_MASK_COUTVID      0x0010
#define  VTSS_MEP_SUPP_FLOW_MASK_SOUTVID      0x0020
#define  VTSS_MEP_SUPP_FLOW_MASK_CUOUTVID     (0x0040 | VTSS_MEP_SUPP_FLOW_MASK_SOUTVID)   /* Custom outer VID - this is two bit's as a Customer VID is also an S-VID */
#define  VTSS_MEP_SUPP_FLOW_MASK_VID          0x0080
#define  VTSS_MEP_SUPP_FLOW_MASK_EVC_ID       0x0100
#define  VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID      0x0200
#define  VTSS_MEP_SUPP_FLOW_MASK_PATH         0x0400
#define  VTSS_MEP_SUPP_FLOW_MASK_PATH_P       0x0800
#define  VTSS_MEP_SUPP_FLOW_MASK_PORT_UNI     0x1000
#define  VTSS_MEP_SUPP_FLOW_MASK_MPLS_LSP     0x2000
#define  VTSS_MEP_SUPP_FLOW_MASK_MPLS_LSP_FWD 0x4000


typedef enum
{
    VTSS_MEP_SUPP_MEP,
    VTSS_MEP_SUPP_MIP
} vtss_mep_supp_mode_t;

typedef enum
{
    VTSS_MEP_SUPP_NONE,
    VTSS_MEP_SUPP_LINE,
    VTSS_MEP_SUPP_LAN,
    VTSS_MEP_SUPP_ROOT,
    VTSS_MEP_SUPP_LEAF
} vtss_mep_supp_evc_type_t;

#define VTSS_MEP_SUPP_ETREE_ELAN(type) ((type != VTSS_MEP_SUPP_NONE) && (type != VTSS_MEP_SUPP_LINE))
#define VTSS_MEP_SUPP_ETREE(type) ((type == VTSS_MEP_SUPP_ROOT) || (type == VTSS_MEP_SUPP_LEAF))


typedef enum
{
    VTSS_MEP_SUPP_DOWN,
    VTSS_MEP_SUPP_UP
} vtss_mep_supp_direction_t;

typedef struct
{
    u32   mask;                                /* Mask to indicate use of following member variables                                                              */
    mesa_port_list_t port;                     /* Port to be used by MEP - that are in front of MEP                                                               */
    u32   in_vid;                              /* Inner VID                                                                                                       */
    u32   out_vid;                             /* Outer VID                                                                                                       */
    u32   vid;                                 /* Classified VID (Root) - checked during frame reception                                                          */
    u32   flow_id;                             /* This is the flow ID (e.g. EVC ID, LSP number) related to this flow.                                             */
    u32   leaf_vid;                            /* Classified VID (Leaf) - checked during frame reception                                                          */
    u32   leaf_out_vid;                        /* Outer VID (Leaf)                                                                                                */

    u32   port_p;                              /* Protection port - this is used for a path protected down mep                                  (VOE capability)  */
    u32   path_mep;                            /* Path MEP relation. Used for configuring UP/DOWN MEP relation to a PATH MEP                    (VOE capability)  */
    u32   path_mep_p;                          /* Protection path MEP relation. Used for configuring DOWN-MEP relation to a protection PATH MEP (VOE capability)  */
} vtss_mep_supp_flow_t;

#define VTSS_MEP_SUPP_EVC_ID_NONE 0xFFFFFFFF
typedef mep_sat_prio_conf_t   vtss_mep_supp_sat_conf_t;
typedef struct
{
    BOOL                        enable;                             /* Enable/disable of this MEP                                             */
    BOOL                        voe;                                /* Select the use of an VOE instance for this MEP                         */
    vtss_mep_supp_mode_t        mode;                               /* MEP or MIP mode                                                        */
    vtss_mep_supp_direction_t   direction;                          /* Up or Down MEP direction                                               */
    u8                          mac[VTSS_MEP_SUPP_MAC_LENGTH];      /* MAC of this MEP                                                        */
    u32                         level;                              /* Level  0-7                                                             */
    vtss_mep_supp_flow_t        flow;                               /* MEP is related to this flow                                            */
    u32                         port;                               /* Residence port                                                         */
    u32                         mep;                                /* MepId for this MEP                                                     */
    mep_domain_t                domain;                             /* Domain for this MEP                                                    */
    u32                         flow_num;                           /* Flow number for this MEP                                               */
    vtss_mep_supp_evc_type_t    evc_type;                           /* E-TREE type                                                            */
    BOOL                        out_of_service;                     /* Set this UP-MEP out of service.      (VOE capability)                  */
    BOOL                        sat;                                /* Set this UP-MEP SAT. Test injection into policer                       */
    vtss_mep_supp_sat_conf_t    sat_conf[VTSS_MEP_SUPP_PRIO_MAX];   /* SAT configuration per priority                                         */
} vtss_mep_supp_conf_t;

/* instance:    Instance number of MEP     */
/* conf:        Configuration              */
/* A MEP is enabled or disabled. The flow relation is set and all OAM PDU sessions on this MEP vill be related to this flow */
u32 vtss_mep_supp_conf_set(const u32                     instance,
                           const vtss_mep_supp_conf_t    *const conf,
                           vtss_flag_t                   *sync_flag);


u32 vtss_mep_supp_conf_mac_get(const u32  instance,
                               u8         *mac);



u32 vtss_mep_supp_lm_flow_count_set(const u32        instance,
                                    const BOOL       flow_count);

typedef enum {
    VTSS_MEP_SUPP_OAM_COUNT_Y1731,      /* OAM PDUs are not counted as service frames as specified in Y1731 */
    VTSS_MEP_SUPP_OAM_COUNT_NONE,       /* OAM PDUs are not counted as service frames                       */
    VTSS_MEP_SUPP_OAM_COUNT_ALL         /* OAM PDUs are all counted as service frames                       */
} vtss_mep_supp_oam_count;

u32 vtss_mep_supp_lm_oam_count_set(const u32                        instance,
                                   const vtss_mep_supp_oam_count    oam_count);

typedef mep_sat_counters_t    vtss_mep_supp_sat_counters_t;
/* instance:   Instance number of MEP                                  */
/* Get the SAT counters. This is the number of transmitted and received OAM frames on the UNI */
u32 vtss_mep_supp_sat_counters_get(const u32                     instance,
                                   vtss_mep_supp_sat_counters_t  *const counters);

/* instance:   Instance number of MEP                                  */
/* Clear the SAT counters. */
u32 vtss_mep_supp_sat_counters_clear(const u32  instance);



typedef enum
{
    VTSS_MEP_SUPP_RATE_INV,
    VTSS_MEP_SUPP_RATE_300S,      /* This will result in HW supported CCM transmission on all platforms */
    VTSS_MEP_SUPP_RATE_100S,      /* This will result in HW supported CCM transmission on all platforms */
    VTSS_MEP_SUPP_RATE_10S,
    VTSS_MEP_SUPP_RATE_1S,
    VTSS_MEP_SUPP_RATE_6M,
    VTSS_MEP_SUPP_RATE_1M,
    VTSS_MEP_SUPP_RATE_6H,
} vtss_mep_supp_rate_t;

typedef enum
{
    VTSS_MEP_SUPP_ITU_ICC,    /* MEG-ID is ITU ICC format as described in Y.1731 ANNEX A         */
    VTSS_MEP_SUPP_IEEE_STR,   /* MEG-ID is IEEE Domain Name format as described in 802.1ag 21.6.5 - Domain format '1' or '4' and Short format '2' (Character string) */
    VTSS_MEP_SUPP_ITU_CC_ICC  /* MEG-ID is ITU CC and ICC format as described in Y.1731 ANNEX A.2 */
} vtss_mep_supp_format_t;

typedef struct {
    u8  value;
} vtss_mep_supp_ps_tlv_t;

typedef struct {
    u8  value;
} vtss_mep_supp_is_tlv_t;

typedef struct {
    u8  oui[VTSS_MEP_SUPP_OUI_SIZE];
    u8  subtype;
    u8  value;
} vtss_mep_supp_os_tlv_t;

typedef struct
{
    vtss_mep_supp_rate_t     rate;                                   /* Rate expected in received and inserted in transmitted CCM PDU              */
    BOOL                     dei;                                    /* DEI                                                                        */
    u32                      prio;                                   /* Priority                                                                   */
    vtss_mep_supp_format_t   format;                                 /* MEG-ID format.                                                             */
    char                     name[VTSS_MEP_SUPP_MEG_CODE_LENGTH];    /* IEEE Maintenance Domain Name. (string \0 terminated)                       */
    char                     meg[VTSS_MEP_SUPP_MEG_CODE_LENGTH];     /* Unique MEG ID Code.           (string \0 terminated)                       */
    u32                      mep;                                    /* Id for this MEP                                                            */
    u32                      peer_count;                             /* Number of peer MEPs  (VTSS_APPL_MEP_PEER_MAX2)                              */
    u16                      peer_mep[VTSS_APPL_MEP_PEER_MAX];       /* MEP id of peer MEPs                                                        */
    u8                       dmac[VTSS_MEP_SUPP_MAC_LENGTH];         /* CCM destination MAC                                                        */
    BOOL                     tlv_enable;                             /* Enable/disable insertion of TLV in CCM PDU. OS, PS and IS TLV are inserted */
    vtss_mep_supp_ps_tlv_t   ps_tlv;                                 /* The Port Status TLV content                                                */
    vtss_mep_supp_is_tlv_t   is_tlv;                                 /* The Interface Status TLV content                                           */
    vtss_mep_supp_os_tlv_t   os_tlv;                                 /* The Organizational Specific TLV content                                    */
} vtss_mep_supp_ccm_conf_t;

/* instance:    Instance number of MEP           */
/* conf:        Configuration                    */
/* The CCM parameters are configured. All defect calculation is based on this. Transmitted CCM is based on this if generation is enabled */
u32 vtss_mep_supp_ccm_conf_set(const u32                          instance,
                               const vtss_mep_supp_ccm_conf_t     *const conf);




typedef struct
{
    BOOL                    enable;      /* Enable/disable of CCM transmission - CCM will be generated with cc_rate                    */
    BOOL                    lm_enable;   /* Enable/disable of LM based on CCM - CCM with loss counters will be generated with lm_rate  */
    vtss_mep_supp_rate_t    lm_rate;     /* This is the LM transmission rate                                                           */
    vtss_mep_supp_rate_t    cc_rate;     /* This is the CC transmission rate                                                           */
} vtss_mep_supp_gen_conf_t;

/* instance:    Instance number of MEP                    */
/* This enable/disable the HW or SW CCM generator.        */
u32 vtss_mep_supp_ccm_generator_set(const u32                         instance,
                                    const vtss_mep_supp_gen_conf_t    *const conf);

/* instance:    Instance number of MEP                */
/* This returns if CCM generator is HW based .        */
BOOL vtss_mep_supp_hw_ccm_generator_get(vtss_mep_supp_rate_t  rate);




/* instance:    Instance number of MEP              */
/* enable:      Enable/disable of CCM RDI           */
/* This controll the RDI bit on a HW/SW based CCM   */
u32 vtss_mep_supp_ccm_rdi_set(const u32        instance,
                              const BOOL       enable);



typedef struct
{
    u64                     valid_counter;                                /* Valid CCM received                                                         */
    u64                     invalid_counter;                              /* Invalid CCM received                                                       */
    u64                     oo_counter;                                   /* Counted received Out of Order sequence numbers (VOE capability)            */

    u32                     unexp_level;                                  /* Last received invalid level value                                          */
    u32                     unexp_period;                                 /* Last received invalid period value                                         */
    u32                     unexp_prio;                                   /* Last received invalid priority value                                       */
    u32                     unexp_mep;                                    /* Last received unexpected MEP id                                            */
    char                    unexp_name[VTSS_MEP_SUPP_MEG_CODE_LENGTH];    /* Last received unexpected Domain Name or ITU Carrier Code (ICC). (string)   */
    char                    unexp_meg[VTSS_MEP_SUPP_MEG_CODE_LENGTH];     /* Last received unexpected Unique MEG ID Code.                    (string)   */
    vtss_mep_supp_ps_tlv_t  ps_tlv[VTSS_APPL_MEP_PEER_MAX];               /* Last received Port Status TLV content                                      */
    vtss_mep_supp_is_tlv_t  is_tlv[VTSS_APPL_MEP_PEER_MAX];               /* Last received Interface Status TLV content                                 */
    vtss_mep_supp_os_tlv_t  os_tlv[VTSS_APPL_MEP_PEER_MAX];               /* Last received Organizational Specific TLV content                          */
    BOOL                    ps_received[VTSS_APPL_MEP_PEER_MAX];
    BOOL                    is_received[VTSS_APPL_MEP_PEER_MAX];
    BOOL                    os_received[VTSS_APPL_MEP_PEER_MAX];
} vtss_mep_supp_ccm_state_t;

/* instance:    Instance number of MEP        */
/* state:       CCM state                     */
/* The CCM state is returned.                 */
u32 vtss_mep_supp_ccm_state_get(const u32                     instance,
                                vtss_mep_supp_ccm_state_t     *const state);

typedef struct
{
    u32 tx_counter;         /* Number of LMM/CCM-LM PDU transmitted  */
    u32 rx_counter;         /* Number of LMR/CCM-LM PDU received     */

    i32 near_los_counter;   /* Near end loss measurement counter     */
    i32 far_los_counter;    /* Far end loss measurement counter      */

    u32 near_tx_delta;      /* Near end TX counter                   */
    u32 near_rx_delta;      /* Near end RX counter                   */
    u32 far_tx_delta;       /* Far end TX counter                    */
    u32 far_rx_delta;       /* Far end RX counter                    */

    bool has_near_rx;       /* Is near-end RX counter valid?         */

} vtss_mep_supp_lm_peer_counters_t;

typedef struct
{
    vtss_mep_supp_lm_peer_counters_t  peer_counters[VTSS_APPL_MEP_PEER_MAX];
} vtss_mep_supp_lm_counters_t;

/**
 * MEP counter clear direction
 */
typedef enum {
    VTSS_MEP_SUPP_CLEAR_DIR_BOTH,       /* Clear counters for both directions (TX and RX).                       */
    VTSS_MEP_SUPP_CLEAR_DIR_TX,         /* Clear counters for TX direction only.                                 */
    VTSS_MEP_SUPP_CLEAR_DIR_RX          /* Clear counters for RX direction only.                                 */
} vtss_mep_supp_clear_dir;

/* instance:   Instance number of MEP                        */
/* Clear LM counters */
u32 vtss_mep_supp_lm_counters_clear(const u32 instance, const vtss_mep_supp_clear_dir direction);


typedef struct
{
    BOOL    dInv;                              /* Invalid period (0) received                                    */
    BOOL    dLevel;                            /* Incorrect CCM level received                                   */
    BOOL    dMeg;                              /* Incorrect CCM MEG id received                                  */
    BOOL    dMep;                              /* Incorrect CCM MEP id received                                  */
    BOOL    dLoc[VTSS_APPL_MEP_PEER_MAX];      /* CCM LOC state from all peer MEP                                */
    BOOL    dRdi[VTSS_APPL_MEP_PEER_MAX];      /* CCM RDI state from all peer MEP                                */
    BOOL    dPeriod[VTSS_APPL_MEP_PEER_MAX];   /* CCM Period state from all peer MEP                             */
    BOOL    dPrio[VTSS_APPL_MEP_PEER_MAX];     /* CCM Priority state from all peer MEP                           */
    BOOL    dAis;                              /* AIS received                                                   */
    BOOL    dLck;                              /* LCK received                                                   */
    BOOL    dLoop;                             /* Loop is detected. CCM is received with own MEP ID and SMAC     */
    BOOL    dConfig;                           /* Configuration error detected. CCM is received with own MEP ID  */
} vtss_mep_supp_defect_state_t;

/* instance:    Instance number of MEP                        */
/* The compleate defect state related to CCM is returned      */
u32 vtss_mep_supp_defect_state_get(const u32                      instance,
                                   vtss_mep_supp_defect_state_t   *const state);




/* instance:    Instance number of MEP                */
/* Any learned MAC from expected MEP is returned      */
u32 vtss_mep_supp_learned_mac_get(const u32   instance,
                                  u8          (*mac)[VTSS_MEP_SUPP_MAC_LENGTH]);

typedef enum {
    VTSS_MEP_SUPP_SINGLE_ENDED,     /* Single ended LM based on LMM/LMR or DM based on DMM/DMR   */
    VTSS_MEP_SUPP_DUAL_ENDED        /* Dual ended LM based on CCM or DM based on 1DM             */
} vtss_mep_supp_ended_t;

typedef struct
{
    BOOL                     enable;                           /* Enable/disable transmission of LMM PDU                                        */
    BOOL                     enable_rx;                        /* Enable/disable reception of of LMM PDU                                        */
    BOOL                     dei;                              /* DEI                                                                           */
    u32                      prio;                             /* Priority                                                                      */
    vtss_mep_supp_rate_t     rate;                             /* Transmission rate -  Must be VTSS_MEP_SUPP_RATE_1S or VTSS_MEP_SUPP_RATE_10S  */
    u8                       dmac[VTSS_MEP_SUPP_MAC_LENGTH];   /* LMM destination MAC                                                           */
    vtss_mep_supp_ended_t    ended;                            /* Dual/single ended selection. */
} vtss_mep_supp_lmm_conf_t;

/* instance:    Instance number of MEP         */
/* enable:      Enable/disable of LMM session  */
/* conf:        Configuration                  */
/* A LMM session is created.                   */
u32 vtss_mep_supp_lmm_conf_set(const u32                        instance,
                               const vtss_mep_supp_lmm_conf_t   *const conf);

typedef struct
{
    BOOL                     enable;                           /* Enable/disable transmission of SLM PDU                                         */
    BOOL                     enable_rx;                        /* Enable/disable reception of of SLM/1SL PDU                                     */
    BOOL                     dei;                              /* DEI                                                                            */
    u32                      prio;                             /* Priority                                                                       */
    vtss_mep_supp_rate_t     rate;                             /* Transmission rate -  Must be VTSS_MEP_SUPP_RATE_1S -> VTSS_MEP_SUPP_RATE_100S  */
    u32                      meas_interval;                    /* Measurement interval in number of transmitted SLM/1SL PDUs                     */
    u32                      size;                             /* Size of SLM/1SL frame to send                                                  */
    u8                       dmac[VTSS_MEP_SUPP_MAC_LENGTH];   /* SLM destination MAC                                                            */
    u32                      test_id;                          /* SLM PDU Test ID value */
    vtss_mep_supp_ended_t    ended;                            /* Dual/single ended selection. */
} vtss_mep_supp_slm_conf_t;

/* instance:    Instance number of MEP         */
/* enable:      Enable/disable of SLM/1SL session  */
/* conf:        Configuration                  */
/* A SLM/1SL session is created.                   */
u32 vtss_mep_supp_slm_conf_set(const u32                        instance,
                               const vtss_mep_supp_slm_conf_t   *const conf);


/*
 * Get the current SLM PDU counters
 */
u32 vtss_mep_supp_slm_counters_get(const u32            instance,
                                   const u32            peer_idx,
                                   sl_service_counter_t *const counters);


typedef enum
{
    VTSS_MEP_SUPP_RDTRIP,   /* Use two timestamps to calculate DM */
    VTSS_MEP_SUPP_FLOW      /* Use four timestamps to calculate DM - far end residence time substracted */
} vtss_mep_supp_calcway_t;

typedef enum
{
    VTSS_MEP_SUPP_US,  /* MicroSecond */
    VTSS_MEP_SUPP_NS   /* NanoSecond */
} vtss_mep_supp_tunit_t;

typedef enum
{
    VTSS_MEP_SUPP_ACT_DISABLE,     /* When overflow, DM is disabled automatically */
    VTSS_MEP_SUPP_ACT_CONTINUE     /* When overflow, counter is reset and DM continues running */
} vtss_mep_supp_act_t;

typedef struct
{
/* Modify/add this for DMM */
    BOOL                     enable[VTSS_MEP_SUPP_PRIO_MAX];   /* There can be a DM per prio                                                               */
    BOOL                     dei;                              /* DEI                                                                                      */
    u8                       dmac[VTSS_MEP_SUPP_MAC_LENGTH];   /* DMM destination MAC                                                                      */
    u32                      interval;                         /* Interval between DMM in 10 ms                                                            */
    vtss_mep_supp_tunit_t    tunit;                            /* Time resolution                                                                          */
    BOOL                     proprietary;                      /* DM based on Vitesse proprietary follow-up PDU                                            */
    vtss_mep_supp_calcway_t  calcway;                          /* Calculation way selection                                                                */
    BOOL                     synchronized;                     /* Near-end and far-end is real time synchronized. One-way DM calculation on receiving DMR   */
} vtss_mep_supp_dmm_conf_t;

/* instance:    Instance number of MEP         */
/* enable:      Enable/disable of DMM session  */
/* conf:        Configuration                  */
/* A DMM session is created.                   */
u32 vtss_mep_supp_dmm_conf_set(const u32                        instance,
                               const vtss_mep_supp_dmm_conf_t   *const conf);

u32 vtss_mep_supp_dmm_conf_get(const u32                  instance,
                               vtss_mep_supp_dmm_conf_t   *const conf);



typedef struct
{
/* Modify/add this for 1DM */
    BOOL                     enable[VTSS_MEP_SUPP_PRIO_MAX];  /* Enable/disable transmission of 1DM PDU         */
    BOOL                     dei;                             /* DEI                                            */
    u8                       dmac[VTSS_MEP_SUPP_MAC_LENGTH];  /* 1DM destination MAC                            */
    u32                      interval;                        /* Interval between 1DM in 10 ms                  */
    vtss_mep_supp_tunit_t    tunit;                           /* Time resolution                                */
    BOOL                     proprietary;                     /* DM based on Vitesse proprietary follow-up PDU  */
} vtss_mep_supp_dm1_conf_t;

/* instance:    Instance number of MEP            */
/* enable:      Enable/disable TX of 1DM session  */
/* conf:        Configuration                     */
/* A 1DM session is created.                      */
u32 vtss_mep_supp_dm1_conf_set(const u32                        instance,
                               const vtss_mep_supp_dm1_conf_t   *const conf);

u32 vtss_mep_supp_dm1_conf_get(const u32                  instance,
                               vtss_mep_supp_dm1_conf_t   *const conf);

#define VTSS_MEP_SUPP_DELAY_INVALID 0xFFFFFFFF
typedef struct
{
    u32  tx_counter;                      /* Number of DMM PDU transmitted                                                                     */
    u32  rx_counter;                      /* Number of DMR PDU received - delays in 'xx_delays'                                                */
    u32  rx_tout_counter;                 /* Number of timeout on recieved DMR PDU                                                             */

    u32  tw_rx_err_counter;               /* Number of invalid Two-way delays calculated when DMR received - 0 < valid delay < 1s              */
    u32  fn_rx_err_counter;               /* Number of invalid one-way far-to-near delays caculated when DMR received - 0 < valid delay < 1s   */
    u32  nf_rx_err_counter;               /* Number of invalid one-way near-to-fardelays caculated when DMR received - 0 < valid delay < 1s    */

    u32  tw_delays[VTSS_MEP_SUPP_DM_MAX];  /* Calculated two-way delays based on 'rx_counter' number of DMR received                            */
    u32  fn_delays[VTSS_MEP_SUPP_DM_MAX];  /* Calculated one-way far-to-near delays based on 'rx_counter' number of DMR received                */
    u32  nf_delays[VTSS_MEP_SUPP_DM_MAX];  /* Calculated one-way near-to-far delays based on 'rx_counter' number of DMR received                */
} vtss_mep_supp_dmr_prio_status;

typedef struct
{
    vtss_mep_supp_dmr_prio_status   prio_status[VTSS_MEP_SUPP_PRIO_MAX];   /* Per priority status */
} vtss_mep_supp_dmr_status;

/* instance:   Instance number of MEP                         */
/* Accumulated DMM counters are returned - cleared after get  */
u32 vtss_mep_supp_dmr_get(const u32                        instance,
                          vtss_mep_supp_dmr_status *const  status);


typedef struct
{
    u32  tx_counter;                    /* Number of 1DM PDU transmitted */
    u32  rx_counter;                    /* Number of 1DM PDU received    */

    u32  rx_err_counter;                /* Number of invalid one-way delays calculated when 1DM received - 0 < valid delay < 1s  */

    u32  delays[VTSS_MEP_SUPP_DM_MAX];  /* Calculated two-way delays based on 'rx_counter' number of DMR received                            */
} vtss_mep_supp_dm1_prio_status;

typedef struct
{
    vtss_mep_supp_dm1_prio_status   prio_status[VTSS_MEP_SUPP_PRIO_MAX];
} vtss_mep_supp_dm1_status;

/* instance:   Instance number of MEP                         */
/* Accumulated DM1 counters are returned - cleared after get  */
u32 vtss_mep_supp_dm1_get(const u32                        instance,
                          vtss_mep_supp_dm1_status *const  status);


typedef enum
{
    VTSS_MEP_SUPP_L_APS,    /* Liniar protection APS    */
    VTSS_MEP_SUPP_R_APS     /* Ring protection APS    */
} vtss_mep_supp_aps_type_t;

typedef struct
{
    BOOL                      enable;                           /* Enable/disable of APS       */
    vtss_mep_supp_aps_type_t  type;                             /* Type of APS                 */
    BOOL                      dei;                              /* DEI                         */
    u32                       prio;                             /* Priority                    */
    u8                        dmac[VTSS_MEP_SUPP_MAC_LENGTH];   /* APS destination MAC         */
} vtss_mep_supp_aps_conf_t;

/* instance:    Instance number of MEP         */
/* enable:      Enable/disable of APS session  */
/* conf:        Configuration                  */
/* A APS session is created. An APS is transmitted every 5 sec. Received APS info is delivered to upper logic   */
u32 vtss_mep_supp_aps_conf_set(const u32                        instance,
                               const vtss_mep_supp_aps_conf_t   *const conf);



/* instance:    Instance number of MEP           */
/* txdata:      Transmitted APS specific info    */
/* event:       transmit APS as an ERPS event.   */
/* First three APS PDU's are transmitted with 3.3 ms (approx) interval */
u32 vtss_mep_supp_aps_txdata_set(const u32     instance,
                                 const u8      *txdata,
                                 const BOOL    event);


/* instance:    Instance number of MEP.                 */
/* enable:      TRUE means that R-APS is forwarded      */
/* R-APS forwarding from the MEP related port, can be enabled/disabled (VOE capability) */
u32 vtss_mep_supp_raps_forwarding(const u32    instance,
                                  const BOOL   enable);


/* instance:    Instance number of MEP.                                    */
/* enable:      TRUE means that R-APS is transmitted at 5 sec. interval    */
/* R-APS transmission from the MEP related port, can be enabled/disabled   */
u32 vtss_mep_supp_raps_transmission(const u32    instance,
                                    const BOOL   enable);





typedef struct
{
    BOOL         enable;                           /* Enable/disable of LT        */
    u32          transaction_id;                   /* Transaction id              */
    BOOL         dei;                              /* DEI                         */
    u32          prio;                             /* Priority                    */
    u8           ttl;                              /* Time To Live                */
    u8           dmac[VTSS_MEP_SUPP_MAC_LENGTH];   /* LTM destination MAC         */
    u8           tmac[VTSS_MEP_SUPP_MAC_LENGTH];   /* LTM target MAC              */
} vtss_mep_supp_ltm_conf_t;

/* instance:    Instance number of MEP         */
/* enable:      Enable/disable of LT session   */
/* conf:        Configuration                  */
/* A LT session is created. One LTM is transmitted. Received LTR is delivered to upper logic   */
u32 vtss_mep_supp_ltm_conf_set(const u32                         instance,
                               const vtss_mep_supp_ltm_conf_t    *const conf);

typedef enum
{
    VTSS_MEP_SUPP_RELAY_UNKNOWN,    /* Unknown relay action */
    VTSS_MEP_SUPP_RELAY_HIT,        /* Relay based on match of target MAC */
    VTSS_MEP_SUPP_RELAY_FDB,        /* Relay based on hit in FDB */
    VTSS_MEP_SUPP_RELAY_MPDB,       /* Relay based on hit in MIP CCM database */
} vtss_mep_supp_relay_act_t;

typedef struct
{
    u32                         transaction_id;
    vtss_mep_supp_mode_t        mode;               /* The reply is done by a MEP or a MIP */
    vtss_mep_supp_direction_t   direction;          /* The reply is done by a UP or a Down instance */
    u8                          ttl;                /* The reply TTL value */
    BOOL                        forwarded;          /* The LTM was forwarded */
    vtss_mep_supp_relay_act_t   relay_action;       /* The relay action */
    u8                          last_egress_mac[VTSS_MEP_SUPP_MAC_LENGTH];
    u8                          next_egress_mac[VTSS_MEP_SUPP_MAC_LENGTH];
} vtss_mep_supp_ltr_t;

u32 vtss_mep_supp_ltr_get(const u32            instance,
                          u32                  *const count,
                          vtss_mep_supp_ltr_t  ltr[VTSS_MEP_SUPP_LTR_MAX]);

typedef enum
{
    VTSS_MEP_SUPP_PATTERN_NONE,      /* No data pattern          */
    VTSS_MEP_SUPP_PATTERN_ALL_ZERO,  /* All zero data pattern    */
    VTSS_MEP_SUPP_PATTERN_ALL_ONE,   /* All zero data pattern    */
    VTSS_MEP_SUPP_PATTERN_0XAA,      /* 10101010 data pattern    */
} vtss_mep_supp_pattern_t;

typedef struct {
    BOOL                     enable;   /* Enable/disable Test frame transmission                                                                        */
    u32                      dp;       /* Drop Precedence of the Test frame.                                                                            */
    u32                      size;     /* Size of Test frame to send - max. VTSS_APPL_MEP_TST_SIZE_MAX bytes - min. VTSS_MEP_SUPP_TST_SIZE_MIN          */
                                       /* This is the Layer 2 frame size including SMAC+DMAC+ETHERTYPE+CRC                                              */
    u32                      rate;     /* The transmission data bit rate in Kbps - max mep_caps.appl_mep_test_rate_max) - min 1                */
                                       /* In VLAN and EVC domain the Layer 2 frame size is used when calculating the frame rate                         */
                                       /* In Port domain preamble and interframe gap is added to Layer 2 frame size when calculating the frame rate     */
    vtss_mep_supp_pattern_t  pattern;  /* Pattern used to build Data TLV in Test frame. If VTSS_APPL_MEP_PATTERN_NONE then 'Null' Test TLV is inserted  */
    u8                       ttl;      /* MPLS-TP TTL value                                                                                             */
} vtss_mep_supp_test_conf_t;

typedef struct
{
    vtss_mep_supp_test_conf_t  tests[VTSS_MEP_SUPP_PRIO_MAX][VTSS_MEP_SUPP_TEST_MAX];   /* There can be a VTSS_MEP_SUPP_TEST_MAX number of active Tests per prio          */
    u32                        to_send;                          /* Number of LBM to send. VTSS_APPL_MEP_LB_TO_SEND_INFINITE => test behaviour. HW based and requires VOE */
                                                                 /*                        Not VTSS_MEP_SUPP_LBM_TO_SEND_INFINITE => series behaviour. SW based           */
    u32                        interval;                         /* Frame interval. Used for series behaviour.   (SW based)                                               */
    u8                         dmac[VTSS_MEP_SUPP_MAC_LENGTH];   /* LBM destination MAC                                                                                   */
    BOOL                       line_rate;                        /* Should preamble and inter frame be included in frame size when calculating the frame rate             */
} vtss_mep_supp_lbm_conf_t;

/* instance:    Instance number of MEP         */
/* enable:      Enable/disable of LB session   */
/* conf:        Configuration                  */
/* A LB session is created. A to_send number of LBM is transmitted. Received LBR is delivered to upper logic   */
u32 vtss_mep_supp_lbm_conf_set(const u32                         instance,
                               const vtss_mep_supp_lbm_conf_t    *const conf,
                                     u64                         *active_time_ms);

typedef struct
{
    u32     transaction_id;
    union {
        u8 mac[VTSS_MEP_SUPP_MAC_LENGTH];
        u8 mep_mip_id[16];
    } u;
} vtss_mep_supp_lbr_t;

/* instance:    Instance number of MEP   */
/* count:       Number of LBR received   */
/* lbr:         LBR array                */
/* Received MAC and transaction ID for each LBR - this is only supported for SW based Discovery-like LB */
u32 vtss_mep_supp_lbr_get(const u32            instance,
                          u32                  *const count,
                          vtss_mep_supp_lbr_t  lbr[VTSS_MEP_SUPP_LBR_MAX]);


typedef struct
{
    u64     lbr_counter;      /* Received LBR PDU count                                          */
    u64     lbm_counter;      /* Transmitted LBM PDU count                                       */
    u64     oo_counter;       /* Counted received Out of Order sequence numbers (VOE capability) */
    u32     trans_id;         /* The next transmitted transaction id                             */
} vtss_mep_supp_lb_status_t;

/* instance:    Instance number of MEP   */
/* status:      LB status   */
/* The LB status counters - this is only supported for HW based Series-like LB */
u32 vtss_mep_supp_lb_status_get(const u32                  instance,
                                vtss_mep_supp_lb_status_t  *const status);

/* instance:    Instance number of MEP   */
/* Clear the LBM status counters */
u32 vtss_mep_supp_lb_clear(const u32    instance);


typedef struct
{
    vtss_mep_supp_test_conf_t  tests[VTSS_MEP_SUPP_PRIO_MAX][VTSS_MEP_SUPP_TEST_MAX];   /* There can be a VTSS_MEP_SUPP_TEST_MAX number of active Tests per prio */
    BOOL                       enable_rx;                        /* Enable/disable reception and analyze of TST PDU                                              */
    u8                         dmac[VTSS_MEP_SUPP_MAC_LENGTH];   /* TST destination MAC                                                                          */
    u32                        transaction_id;                   /* Transaction id start value                                                                   */
    BOOL                       sequence;                         /* Sequence number will be inserted                                                             */
    BOOL                       line_rate;                        /* Should preamble and inter frame be included in frame size when calculating the frame rate    */
} vtss_mep_supp_tst_conf_t;

/* instance:    Instance number of MEP               */
/* enable:      Enable/disable of TST transmission   */
/* conf:        Configuration                        */
/* A TST transmission is enabled or disabled against a unicast MAC   */
u32 vtss_mep_supp_tst_conf_set(const u32                         instance,
                               const vtss_mep_supp_tst_conf_t    *const conf,
                                     u64                         *active_time_ms);


typedef struct
{
    u64     tx_counter;   /* Transmitted TST PDU count                                       */
    u64     rx_counter;   /* Received TST PDU count (VOE capability)                         */
    u64     oo_counter;   /* Counted received Out of Order sequence numbers (VOE capability) */
} vtss_mep_supp_tst_status_t;

/* instance:    Instance number of MEP   */
/* status:      TST status   */
/* The TST status counters */
u32 vtss_mep_supp_tst_status_get(const u32                   instance,
                                 vtss_mep_supp_tst_status_t  *const status);


/* instance:    Instance number of MEP   */
/* Clear the TST status counters */
u32 vtss_mep_supp_tst_clear(const u32    instance);


typedef struct
{
    vtss_mep_supp_rate_t       rate;                                     /* Transmission rate                                             */
    BOOL                       dei;                                      /* DEI                                                           */
    BOOL                       protection;                               /* TRUE will transmit the first three frames as fast as possible */
    u8                         dmac[VTSS_MEP_SUPP_MAC_LENGTH];           /* AIS destination MAC                                           */
    u32                        flow_count;                               /* Number of client flows (configured EVC/VLAN/LSP)              */
    u8                         level[VTSS_MEP_SUPP_CLIENT_FLOWS_MAX];    /* Client MEG level                                              */
    u32                        prio[VTSS_MEP_SUPP_CLIENT_FLOWS_MAX];     /* Client Priority                                               */
    vtss_mep_supp_flow_t       flows[VTSS_MEP_SUPP_CLIENT_FLOWS_MAX];    /* Client flows                                                  */
} vtss_mep_supp_ais_conf_t;

u32 vtss_mep_supp_ais_conf_set(const u32  instance,
                               const vtss_mep_supp_ais_conf_t   *const conf);

u32 vtss_mep_supp_ais_set(const u32  instance,
                          const BOOL enable);


typedef struct
{
    BOOL                        enable;
    vtss_mep_supp_rate_t        rate;                                     /* Transmission rate                                */
    BOOL                        dei;                                      /* DEI                                              */
    u8                          dmac[VTSS_MEP_SUPP_MAC_LENGTH];           /* LCK destination MAC                              */
    u32                         flow_count;                               /* Number of client flows (configured EVC/VLAN/LSP) */
    u8                          level[VTSS_MEP_SUPP_CLIENT_FLOWS_MAX];    /* Client MEG level                                 */
    u32                         prio[VTSS_MEP_SUPP_CLIENT_FLOWS_MAX];     /* Client Priority                                  */
    vtss_mep_supp_flow_t        flows[VTSS_MEP_SUPP_CLIENT_FLOWS_MAX];    /* Client flows                                     */
} vtss_mep_supp_lck_conf_t;

u32 vtss_mep_supp_lck_conf_set(const u32  instance,
                               const vtss_mep_supp_lck_conf_t   *const conf);





u32 vtss_mep_supp_loc_state(const u32   instance,
                            const BOOL  state);



u32 vtss_mep_supp_rx_isdx_get(const u32 instance,  u32 port);

u32 vtss_mep_supp_tx_isdx_get(const u32  instance);















































typedef struct {
    mesa_timestamp_t tx_time;                           /* DMM transmit time  */
    mesa_timestamp_t rx_time;                           /* DMM receive time returned in DMR */
} mep_supp_dm_timestamp_t;

u32 vtss_mep_supp_dmr_timestamps_get(const u32               instance,
                                     mep_supp_dm_timestamp_t *const dm1_timestamp_far_to_near,
                                     mep_supp_dm_timestamp_t *const dm1_timestamp_near_to_far);

/* This function is called to inform that protection has changed for this instance */
void vtss_mep_supp_protection_change(u32 instance);



/****************************************************************************/
/*  MEP lower support call out to upper logic                               */
/****************************************************************************/

/* instance:    Instance number of MEP                               */
/* This is called by lower support when new defect state is detected */
void vtss_mep_supp_new_defect_state(const u32  instance);

/* instance:    Instance number of MEP                                                   */
/* This is called by lower support when new ccm state (unexpected values) is detected    */
void vtss_mep_supp_new_ccm_state(const u32  instance);

/* instance:    Instance number of MEP                           */
/* This is called by lower support when new APS PDU is received  */
void vtss_mep_supp_new_aps(const u32  instance,
                           const u8   *aps);

/* instance:    Instance number of MEP                          */
/* This is called by lower support when new LTR PDU is received */
void vtss_mep_supp_new_ltr(const u32  instance);

/* instance:    Instance number of MEP                          */
/* This is called by lower support when new LBR PDU is received */
void vtss_mep_supp_new_lbr(const u32  instance);

/* instance:    Instance number of MEP                          */
/* This is called by lower support when new DMR PDU is received */
void vtss_mep_supp_new_dmr(const u32  instance);

/* instance:    Instance number of MEP                          */
/* This is called by lower support when new DMR PDU is received */
void vtss_mep_supp_new_dm1(const u32  instance);

/* instance:    Instance number of MEP                          */
/* This is called by lower support when new SMAC from peer MEP is received */
void vtss_mep_supp_new_mac(const u32  instance);

/* instance:    Instance number of MEP                          */
/* This is called by lower support when new TST PDU is received. The size is al inclusive - Preamble + TAG + CRC + IFG */
void vtss_mep_supp_new_tst(const u32   instance,
                           const u32   frame_size);

/* instance:    Instance number of MEP                          */
/* This is called by lower support when new CCM TLV Interface Status is received. */
void vtss_mep_supp_new_tlv_is(const u32   instance);

/* instance:    Instance number of MEP                          */
/* This is called by lower support when new E-TREE extraction rules are created. */
u32 vtss_mep_supp_new_etree_elan_extraction(const u32   instance);

/* instance:    Instance number of MEP                          */
/* This is called by lower support when new AFI TST PDU transmission is done (cancelled) */
void vtss_mep_supp_tst_done(const u32   instance);

/* instance:    Instance number of MEP                          */
/* This is called by lower support when new AFI LBM PDU transmission is done (cancelled) */
void vtss_mep_supp_lbm_done(const u32   instance);

/* instance:    Instance number of MEP                                          */
/* This is called by lower support to retrieve frame counters for this instance */
BOOL vtss_mep_supp_counters_get(const u32       instance,
                                u32             *const rx_frames,
                                u32             *const tx_frames);

/* instance:    Instance number of MEP                                             */
/* peer_idx:    Index of peer MEP                                                  */
/* peer_counters: LM counters for the peer                                         */
/* This is called by module (supp) when a new LM counter measurement has been done */
void vtss_mep_supp_lm_measurement(const u32    instance,
                                  const u32    peer_idx,
                                  const vtss_mep_supp_lm_peer_counters_t *peer_counters);

/* instance:    Instance number of MEP                                                   */
/* peer_idx:    Index of peer MEP                                                        */
/* peer_counters: LM counters for the peer                                               */
/* This is called by module (supp) when the TX counters for an instance has been updated */
void vtss_mep_supp_lm_tx_update(const u32    instance,
                                const u32    peer_idx,
                                const vtss_mep_supp_lm_peer_counters_t *peer_counters);

/* instance:    Instance number of MEP      */
/* This is called by module (supp) when a new LM counter measurement has to be init */
void vtss_mep_supp_lm_measurement_init(const u32    instance,
                                       const u32    peer_idx);


/****************************************************************************/
/*  MEP platform call in to lower support                                   */
/****************************************************************************/

/* stop:    Return value indicating if calling this thread has to stop.                                                 */
/* This is the thread driving timing . Has to be called every 'timer_resolution' ms. by upper logic until 'stop'        */
/* Initially this thread is not called until callout on vtss_mep_supp_timer_start()                                     */
void vtss_mep_supp_timer_thread(BOOL  *const stop);

/* This is the thread driving the state machine. Has to be call by upper logic when calling out on vtss_mep_supp_run() */
void vtss_mep_supp_run_thread(void);

/* timer_resolution:    This is the interval of calling vtss_mep_supp_run_thread() in ms.    */
/* This is the initializing of lower support. Has to be called by upper logic                */
u32 vtss_mep_supp_init(const u32 timer_resolution, u32 up_mep_loop_port, u32 mep_pag, u32 mip_pag);


typedef struct
{
    u32 vid;
    u32 qos;
    u32 pcp;
    u32 oam_info;
    u32 isdx;
    BOOL dei;
    BOOL tagged;
    BOOL discard;

    u32 otag_vid;
    u32 otag_pcp;
    BOOL otag_dei;
    u32 itag_vid;
    u32 itag_pcp;
    BOOL itag_dei;
} vtss_mep_supp_frame_info_t;

/* This function is called with received OAM PDU frames */
void vtss_mep_supp_rx_frame(u32 port, vtss_mep_supp_frame_info_t *frame_info, u8 frame[], u32 len, mesa_timestamp_t *rx_time, u32 rx_count);

#define VTSS_MEP_SUPP_VOE_CCM_PERIOD_EV       (1<<7)
#define VTSS_MEP_SUPP_VOE_CCM_PRIORITY_EV     (1<<6)
#define VTSS_MEP_SUPP_VOE_CCM_ZERO_PERIOD_EV  (1<<5)
#define VTSS_MEP_SUPP_VOE_CCM_RX_RDI_EV       (1<<4)
#define VTSS_MEP_SUPP_VOE_CCM_LOC_EV          (1<<3)
#define VTSS_MEP_SUPP_VOE_CCM_MEP_ID_EV       (1<<2)
#define VTSS_MEP_SUPP_VOE_CCM_MEG_ID_EV       (1<<1)
#define VTSS_MEP_SUPP_VOE_MEG_LEVEL_EV        (1<<0)
/* This function is called on detected VOE interrupt - ev_mask is a return value containing indication of detected events */
void vtss_mep_supp_voe_interrupt(void);

/* This function is called to determine if this ISDX relates to a VOE based UP-MEP */
BOOL vtss_mep_supp_voe_up(u32 isdx, u32 vid, u32 level, u32 *voe);

u32 vtss_mep_supp_tt_loop_mce_id_get(void);



/****************************************************************************/
/*  MEP lower support call out to platform                                  */
/****************************************************************************/

/* This is call by MEP lower support to lock/unlock critical code protection */
void vtss_mep_supp_loc_crit_lock(const char *location,
                                 uint       line_no);

void vtss_mep_supp_loc_crit_unlock(const char *location,
                                   uint       line_no);

/* This is called by MEP lower support when vtss_mep_supp_run_thread(void) has to be called */
void vtss_mep_supp_run(void);

/* This is called by MEP lower support when vtss_mep_supp_timer_thread(BOOL  *const stop) has to be called until 'stop' is indicated */
void vtss_mep_supp_timer_start(void);

/* This is called by MEP lower support in order to do debug tracing */
void vtss_mep_supp_trace(const char   *string,
                         const u32    param1,
                         const u32    param2,
                         const u32    param3,
                         const u32    param4);

/* This is called by MEP lower support in order to do error tracing */
void vtss_mep_supp_trace_err(const char   *string,
                             const u32    param1,
                             const u32    param2,
                             const u32    param3,
                             const u32    param4);

void vtss_mep_supp_lm_trace(const char  *string,
                            const u32   param1,
                            const u32   param2,
                            const u32   param3,
                            const u32   param4);

void vtss_mep_supp_dm_trace(const char  *string,
                            const u32   param1,
                            const u32   param2,
                            const u32   param3,
                            const u32   param4);


u8 *vtss_mep_supp_packet_tx_alloc(u32 size);

void vtss_mep_supp_packet_tx_free(u8 *buffer);

void vtss_mep_supp_packet_tx_cancel(u32 instance, u8 *buffer, u64 *active_time_ms);

struct vtss_mep_supp_ts_done_s;
typedef void (*vtss_mep_supp_cb_t)(struct vtss_mep_supp_ts_done_s *done, mesa_timestamp_t *tx_time);
typedef struct vtss_mep_supp_ts_done_s {
    u32                 instance;
    u32                 prio;
    vtss_mep_supp_cb_t  cb;
} vtss_mep_supp_ts_done_t;

typedef struct
{
    u32                       tc;             /* hw counter corresponding to the TX timestamp */
    mesa_timestamp_t          ts;
    u32                       tx_time;        /* hw counter corresponding to the pre_done correction field update time */
    mesa_timeinterval_t       sw_egress_delay;/* set to the calculated min sw egress delay in the tx_event_onestep function */
    vtss_mep_supp_ts_done_t   *ts_done;
    u8                        tag_cnt;
} vtss_mep_supp_onestep_extra_t;

/* latency observed in onestep tx timestamping */
typedef struct
{
    mesa_timeinterval_t max;
    mesa_timeinterval_t min;
    mesa_timeinterval_t mean;
    u32 cnt;
} vtss_mep_supp_observed_egr_lat_t;

#define VTSS_MEP_SUPP_INDX_INVALID  0xFFFFFFFF
typedef struct
{
    BOOL  bypass;         /* Frame is bypassing the analyzer */
    BOOL  vid_inj;        /* Injection into classified VID */
    BOOL  line_rate;      /* Should preamble and inter frame be included in frame size when calculating the frame rate */
    u32   maskerade_port; /* Maskerading is disabled if VTSS_PORT_NO_NONE */
    u32   isdx;           /* ISDX */
    u32   vid;            /* Classified VID */
    u32   qos;            /* Classified QOS */
    u32   pcp;            /* Classified PCP - EVC COS ID */
    u32   dp;             /* Claasified Drop Precedence */
} vtss_mep_supp_tx_frame_info_t;

typedef enum {
  VTSS_MEP_SUPP_OAM_TYPE_NONE = 0,
  VTSS_MEP_SUPP_OAM_TYPE_CCM,
  VTSS_MEP_SUPP_OAM_TYPE_CCM_LM,
  VTSS_MEP_SUPP_OAM_TYPE_LBM,
  VTSS_MEP_SUPP_OAM_TYPE_LBR,
  VTSS_MEP_SUPP_OAM_TYPE_LMM,
  VTSS_MEP_SUPP_OAM_TYPE_LMR,
  VTSS_MEP_SUPP_OAM_TYPE_DMM,
  VTSS_MEP_SUPP_OAM_TYPE_DMR,
  VTSS_MEP_SUPP_OAM_TYPE_1DM,
  VTSS_MEP_SUPP_OAM_TYPE_LTM,
  VTSS_MEP_SUPP_OAM_TYPE_LTR,
  VTSS_MEP_SUPP_OAM_TYPE_GENERIC,
} vtss_mep_supp_oam_type_t;

#define VTSS_MEP_SUPP_RATE_IS_BPS   0x80000000
u32 vtss_mep_supp_packet_tx(u32                           instance,
                            mesa_timestamp_t              *tx_time,
                            mesa_port_list_t              &port,
                            u32                           len,
                            unsigned char                 *frm,
                            vtss_mep_supp_tx_frame_info_t *frame_info,
                            u32                           rate,
                            BOOL                          count_enable,
                            u32                           seq_offset,
                            vtss_mep_supp_oam_type_t      oam_type,
                            BOOL                          sat);

u32 vtss_mep_supp_packet_tx_1(u32                           instance,
                              mesa_timestamp_t              *tx_time,
                              u32                           port,
                              u32                           len,
                              unsigned char                 *frm,
                              vtss_mep_supp_tx_frame_info_t *frame_info,
                              vtss_mep_supp_oam_type_t      oam_type);

u32 vtss_mep_supp_packet_tx_two_step_ts(u32                     instance,
                                        vtss_mep_supp_ts_done_t *ts_done,
                                        mesa_port_list_t        &port,
                                        u32                     len,
                                        unsigned char           *frm);

u32 vtss_mep_supp_packet_tx_1_two_step_ts(u32                     instance,
                                          vtss_mep_supp_ts_done_t *ts_done,
                                          u32                     port,
                                          u32                     len,
                                          unsigned char           *frm);

u32 vtss_mep_supp_packet_tx_1_one_step_ts(u32                           instance,
                                          u32                           port,
                                          u32                           len,
                                          unsigned char                 *frm,
                                          vtss_mep_supp_tx_frame_info_t *frame_info,
                                          u32                           hw_time,
                                          vtss_mep_supp_onestep_extra_t *onestep_extra,
                                          vtss_mep_supp_oam_type_t      oam_type);

u32 vtss_mep_supp_packet_tx_one_step_ts(u32                           instance,
                                        mesa_port_list_t              &port,
                                        u32                           len,
                                        unsigned char                 *frm,
                                        vtss_mep_supp_tx_frame_info_t *frame_info,
                                        u32                           hw_time,
                                        vtss_mep_supp_onestep_extra_t *buf_handle,
                                        vtss_mep_supp_oam_type_t      oam_type);

BOOL vtss_mep_supp_check_hw_timestamp(void);

i32 vtss_mep_supp_delay_calc(const mesa_timestamp_t *x,
                             const mesa_timestamp_t *y,
                             const BOOL             is_ns);

BOOL vtss_mep_supp_check_phy_timestamp(u32 port, u8 *mac);

void vtss_mep_supp_timestamp_get(mesa_timestamp_t *timestamp, u32 *tc);

BOOL vtss_mep_supp_check_forwarding(u32   i_port,
                                    u32   *f_port,
                                    u8    mac[VTSS_MEP_SUPP_MAC_LENGTH],
                                    u32   vid);

/* This function should return number of transmitted frames related to the pointer 'frm' */
void vtss_mep_supp_packet_tx_frm_cnt(u8 *frm, u64 *frm_cnt);

/* Get the port ethertype */
u32 vtss_mep_supp_port_ether_type(u32 port);

/* Control the UNI port configuration*/
void vtss_mep_supp_port_conf(const u32       instance,
                             const u32       port,
                             const BOOL      up_mep,
                             const BOOL      out_of_service);

/*
 * Information about received LLM frame. This will be used when sending a
 * LLR reply later on.
 */
typedef struct {
    BOOL                is_valid;                   /* Is the content valid? */
    u32                 rxport;                     /* port where request was received */
    mesa_mac_t          dmac;                       /* DMAC for the request */
    vtss_mep_supp_frame_info_t frame_info;          /* Frame info for the request */
} vtss_mep_supp_ll_frame_info_t;

/*
 * Information used to build LLM request
 */
typedef struct {
    mesa_mac_t          dmac;                       /* DMAC for the request */
    u8                  prio;                       /* Priority for tagged OAM */
    u8                  message_type;               /* Message type */
    u8                  flags;                      /* Flags */
    u32                 expiration_timer;           /* Expiration timer value */
} vtss_mep_supp_ll_request_info_t;

/*
 * Information used to build LLR reply
 */
typedef struct {
    vtss_mep_supp_ll_frame_info_t ll_frame_info;    /* Frame info for the request */
    mesa_mac_t          lpmac;                      /* Loopback port MAC */
    u8                  message_type;               /* Message type */
    u8                  flags;                      /* Flags */
    u8                  response;                   /* Response code */
    u32                 expiration_timer;           /* Expiration timer value */
    u8                  *tlv_buffer;                /* TLV buffer for reply. Only contains unknown TLVs. */
    u32                 tlv_max_size;               /* Max size of TLV buffer */
    u32                 tlv_size;                   /* Actual size of TLV data in buffer */
} vtss_mep_supp_ll_reply_info_t;

/*
 * Call-out from MEP lower layer (supp) when a MEF 46 Latching Loopback Message
 * (LLM) in a CFM PDU has been received.
 */
BOOL vtss_mep_supp_llm_received(u32 mepid, u8 *frame,
        u8 *pdu, u32 pdu_length, vtss_mep_supp_ll_reply_info_t *supp_reply);

/*
 * Call-out from MEP lower layer (supp) when a MEF 46 Latching Loopback reply
 * (LLR) in a CFM PDU has been received.
 */
void vtss_mep_supp_llr_received(u32 mepid, u8 *frame, u8 *pdu, u32 pdu_length);

/*
 * Call-in to MEP lower layer (supp) when a MEF 46 Latching Loopback Message
 * (LLM) needs to be transmitted.
 */
vtss_rc vtss_mep_supp_llm_transmit(const u32 instance,
        vtss_mep_supp_ll_request_info_t *ll_request);

/*
 * Call-in to MEP lower layer (supp) when a MEF 46 Latching Loopback Reply
 * (LLR) needs to be transmitted.
 */
vtss_rc vtss_mep_supp_llr_transmit(const u32 instance, u8* llm_pdu,
        vtss_mep_supp_ll_reply_info_t *ll_reply);

typedef struct {
    BOOL                enable;
    mesa_mce_pcp_mode_t pcp_mode;
    mesa_tagprio_t      pcp;
    mesa_mce_dei_mode_t dei_mode;
    mesa_dei_t          dei;
} vtss_mep_supp_evc_mce_outer_tag_t;

typedef struct {
    BOOL    enable;
    u32     pcp;
} vtss_mep_supp_evc_mce_key_t;

typedef enum {
    VTSS_MEP_SUPP_EVC_NNI_TYPE_E_NNI,
    VTSS_MEP_SUPP_EVC_NNI_TYPE_I_NNI
} vtss_mep_supp_evc_nni_type_t;

#define VTSS_MEP_SUPP_COS_ID_SIZE 8
void vtss_mep_supp_evc_mce_info_get(const u32                          evc_inst,
                                    vtss_mep_supp_evc_nni_type_t       *nni_type,
                                    vtss_mep_supp_evc_mce_outer_tag_t  nni_outer_tag[VTSS_MEP_SUPP_COS_ID_SIZE],
                                    vtss_mep_supp_evc_mce_key_t        nni_key[VTSS_MEP_SUPP_COS_ID_SIZE]);

BOOL vtss_mep_supp_evc_ece_action_get(u32 evc_inst,  u32 port,  u32 vid,  u32 pcp,  mesa_ece_action_t  *action);

}  // mep_g1

#endif /* _VTSS_MEP_SUPP_API_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
