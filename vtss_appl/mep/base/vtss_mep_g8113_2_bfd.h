/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

#ifndef _VTSS_MEP_G8113_2_BFD_H_
#define _VTSS_MEP_G8113_2_BFD_H_

#include "main_types.h"
#include "vtss/appl/mep.h"

namespace mep_g1 {

typedef struct {
    /* configuration parameters: */
    vtss_appl_mep_g8113_2_bfd_conf_t config;
    u32                              my_discr[2]; /* "My Discriminator" value for CC and CV frames, must be non-zero, randomly selected */
    u8                               tx_src_mep_id_tlv[28];
    u8                               rx_src_mep_id_tlv[28];

    /* these are specific to the MEP module integration: */
    u8    *tx_cc_buf;            /* CC frame tx buffer */
    u8    *tx_cv_buf;            /* CV frame tx buffer */
    u16   tx_cc_timer[2];        /* BFD CC Tx timer(s) (source/sink), in ms */
    u16   tx_cv_timer;           /* BFD CV Tx timer, in ms */
    BOOL  tx_cc_ongoing;         /* TRUE while BFD CC Tx not yet complete */
    BOOL  tx_cv_ongoing;         /* TRUE while BFD CV Tx not yet complete */
    u16   miss_cnt;              /* BFD miss counter for detecting dLOC   */
    u8    dMMG_cnt;              /* Counter for clearing of dMMG          */

    /* BFD status: */
    u8    unexp_rx_src_mep_id_tlv[28];
    u64   cc_tx_cnt;
    u64   valid_cc_rx_cnt;
    u64   invalid_cc_rx_cnt;
    u64   cv_tx_cnt;
    u64   valid_cv_rx_cnt;
    u64   invalid_cv_rx_cnt;
    u8    admin_down_cnt;
    u8    session_state[2];
    u8    local_diag;
    u8    poll;
    u8    final;

    /* authentication: */
    u8    rx_auth_seq_known;
    u32   rx_auth_seq;
    u32   tx_auth_seq;
    u8    tx_auth[20]; /* to speed up non-meticulous mode */

    u8    remote_session_state;
    u8    remote_diag;
    u8    remote_flags;
    u32   remote_discr[2];
    u32   remote_min_rx;
    u32   unexp_discr;

} vtss_mep_g8113_2_bfd_t;

/* update BFD CC/CV config, return FALSE for illegal update (e.g. change period while running): */
BOOL vtss_mep_g8113_2_bfd_config(vtss_mep_g8113_2_bfd_t *inst,
                                 const vtss_appl_mep_g8113_2_bfd_conf_t *const config);

/* configure BFD authentication key: */
BOOL vtss_mep_g8113_2_bfd_auth_config(const u8 key_id,
                                      const vtss_appl_mep_g8113_2_bfd_auth_conf_t *const config);

/* get BFD authentication key config: */
BOOL vtss_mep_g8113_2_bfd_auth_config_get(const u8 key_id,
                                          vtss_appl_mep_g8113_2_bfd_auth_conf_t *const config);

/* receive BFD CC/CV pdu - pdu must point just after ACH: */
void vtss_mep_g8113_2_bfd_rx(vtss_mep_g8113_2_bfd_t *inst,
                             u16 opCode, u8 *pdu, u32 len, u16 ach_val,
                             BOOL *dLOC, BOOL *dMMG, BOOL *dRDI, BOOL *dUNP);

/* BFD CC Tx timeout: */
void vtss_mep_g8113_2_bfd_cc_tx_timeout(vtss_mep_g8113_2_bfd_t *inst, u8 sink,
                                        u8 *pdu, u32 *len, BOOL *dLOC);

/* BFD CV Tx timeout: */
void vtss_mep_g8113_2_bfd_cv_tx_timeout(vtss_mep_g8113_2_bfd_t *inst,
                                        u8 *pdu, u32 *len, BOOL *dMMG);

/* init BFD CC/CV: */
void vtss_mep_g8113_2_bfd_init(const u32 timer_resolution); /* in ms */

}  // mep_g1

#endif /* _VTSS_MEP_G8113_2_BFD_H_ */
