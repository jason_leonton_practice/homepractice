/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _VTSS_MEP_H_
#define _VTSS_MEP_H_

#include "main_types.h"
#include "vtss/appl/mep.h"
#include "vtss_mep_api.h"
#include "vtss_mep_supp_api.h"

namespace mep_g1 {

#define VTSS_MEP_RC_OK                      0   /* Management operation is ok */
#define VTSS_MEP_RC_INVALID_PARAMETER       1   /* invalid parameter */
#define VTSS_MEP_RC_NOT_ENABLED             2   /* MEP instance is not enabled */
#define VTSS_MEP_RC_CAST                    3   /* LM and CC sharing SW generated CCM must have same casting (multi/uni) */
#define VTSS_MEP_RC_RATE_INTERVAL           4   /* Invalid frame rate or interval */
#define VTSS_MEP_RC_PEER_CNT                5   /* Invalid peer count for this configuration */
#define VTSS_MEP_RC_PEER_ID                 6   /* Dublicate peer ID when more than one peer. Unknown peer MEP-ID. */
#define VTSS_MEP_RC_MIP                     7   /* Not allowed on a MIP */
#define VTSS_MEP_RC_INVALID_EVC             8   /* EVC flow was found invalid */
#define VTSS_MEP_RC_APS_UP                  9   /* APS not allowed on an EVC UP MEP */
#define VTSS_MEP_RC_APS_DOMAIN             10   /* R-APS is only allowed in port domain */
#define VTSS_MEP_RC_INVALID_VID            11   /* VLAN is not created on this VID */
#define VTSS_MEP_RC_INVALID_COS_ID         12   /* The COS ID is not defined for this EVC */
#define VTSS_MEP_RC_NO_VOE                 13   /* There is no VOE available to be created or MEP must be VOE */
#define VTSS_MEP_RC_NO_TIMESTAMP_DATA      14   /* There is no dmr timestamp data available f*/
#define VTSS_MEP_RC_PEER_MAC               15   /* Peer Unicast MAC must be known to do HW based CCM on SW MEP */
#define VTSS_MEP_RC_INVALID_INSTANCE       16   /* Invalid instance number */
#define VTSS_MEP_RC_INVALID_MEG            17   /* Invalid MEG */
#define VTSS_MEP_RC_PROP_SUPPORT           18   /* Propritary DM is not supported */
#define VTSS_MEP_RC_VOLATILE               19   /* This MEP instance is volatile */
#define VTSS_MEP_RC_VLAN_SUPPORT           20   /* VLAN domain is not supported */
#define VTSS_MEP_RC_CLIENT_MAX_LEVEL       21   /* The MEP is on MAX level (7) - it is not possible to have a client on higher level */
#define VTSS_MEP_RC_MIP_SUPPORT            22   /* This MIP is not supported */
#define VTSS_MEP_RC_INVALID_MAC            23   /* On this platform the selected MAC is invalid */
#define VTSS_MEP_RC_CHANGE_PARAMETER       24   /* Some parameters are not allowed to change on an enabled instance */
#define VTSS_MEP_RC_NO_LOOP_PORT           25   /* There is no loop port - this is required to create Up-MEP */
#define VTSS_MEP_RC_INVALID_PRIO           26   /* Priority was found invalid */
#define VTSS_MEP_RC_INVALID_CLIENT_LEVEL   27   /* The client level is invalid */
#define VTSS_MEP_RC_INVALID_PORT           28   /* The residence port is not valid in this domain or for this MEP type */
#define VTSS_MEP_RC_INVALID_HW_CCM         29   /* On Jaguar all MEP in same classified VID must have same HW CCM state */
#define VTSS_MEP_RC_LST                    30   /* Link State Tracking cannot be enabled as either CC or CC-TLV is not enabled */
#define VTSS_MEP_RC_NNI_PORT               31   /* The MEP must be configured on a NNI port */
#define VTSS_MEP_RC_TEST                   32   /* TST and LBM cannot be enabled at the same time */
#define VTSS_MEP_RC_E_TREE                 33   /* Invalid configuration for E-TREE EVC */
#define VTSS_MEP_RC_CC_BFD_CONFLICT        34   /* CC and BFD cannot be enabled at the same time */
#define VTSS_MEP_RC_INVALID_VOE_CHANGE     35   /* VOE support cannot be added/deleted on an enabled MEP instance */
#define VTSS_MEP_RC_TTL_ZERO               36   /* TTL=0 not allowed */
#define VTSS_MEP_RC_LM_DUAL_ENDED_RATE_EXCEED_CC_RATE 37 /* LM_DUAL_ENDED rate cannot be higher than CC RATE */
#define VTSS_MEP_RC_OUT_OF_TX_RESOURCE     38   /* There are no free transmission resources */
#define VTSS_MEP_RC_TX_ERROR               39   /* Transmitting OAM frame failed due to system error */
#define VTSS_MEP_RC_TCAM_CONFIGURATION     40   /* The TCAM configuration failed */
#define VTSS_MEP_RC_INTERNAL               41   /* Internal error */
#define VTSS_MEP_RC_VOE_CONFIG_ERROR       42   /* Too configured VOE based MEPs on a port in a flow or VOE based MEP on wrong level. */
#define VTSS_MEP_RC_VOE_MPLS_ERROR         43   /* VOE is not supported on MEP MPLS domains. */
#define VTSS_MEP_RC_MEAS_INTERVAL          44   /* Invalid LM measurement interval */
#define VTSS_MEP_RC_INVALID_VOE            45   /* LAG port for MEP is in STANDBY state */
#define VTSS_MEP_RC_INVALID_LEVEL          46   /* Invalid MEG level */
#define VTSS_MEP_RC_EVC_VID_SUPPORT        47   /* EVC subscriber MEP not supported */

#define VTSS_MEP_APS_DATA_LENGTH     8

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* vola:        This function is called from the volatile interface. Default is FALSE */
/* Instance is configured or disabled. If instance is marked as 'volatile an error is returned */
u32 vtss_mep_mgmt_conf_set(const u32         instance,
                           const mep_conf_t  *const conf,
                           BOOL              vola=FALSE);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* When this instance is enabled, it is marked as a 'volatile' Instance' and can only be disabled using this function. */
/* If this instance is used as parameter to 'vtss_mep_mgmt_conf_set' an error is returned */
u32 vtss_mep_mgmt_volatile_conf_set(const u32          instance,
                                    const mep_conf_t  *const conf);

/* instance:    Instance number of MEP.                     */
/* conf:        Configuration data for this SAT instance    */
/* When this instance is enabled, it is marked as a 'volatile' SAT Instance' and can only be disabled using this function. */
/* If this instance is used as parameter to 'vtss_mep_mgmt_conf_set' an error is returned */
u32 vtss_mep_mgmt_volatile_sat_conf_set(const u32             instance,
                                        const mep_sat_conf_t  *const conf);

u32 vtss_mep_mgmt_volatile_sat_conf_get(const u32       instance,
                                        mep_sat_conf_t  *const conf);

/* instance:   Instance number of MEP                                  */
/* Get the SAT counters. This is the number of transmitted and received OAM frames on the UNI per priority */
u32 vtss_mep_mgmt_volatile_sat_counter_get(const u32           instance,
                                           mep_sat_counters_t  *const counters);

/* instance:   Instance number of MEP                                  */
/* Clear the SAT counters. */
u32 vtss_mep_mgmt_volatile_sat_counter_clear_set(const u32  instance);

/* instance:    Instance number of MEP.                 */
/* mac:         MAC of this MEP instance                */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_conf_get(const u32         instance,
                           mep_conf_t       *const conf);

/* instance:  Instance number of MEP.                 */
/* conf:      Performance Monitoring Configuration data for this instance    */
u32 vtss_mep_mgmt_pm_conf_set(const u32                      instance,
                              const vtss_appl_mep_pm_conf_t  *const conf);

u32 vtss_mep_mgmt_pm_conf_get(const u32                 instance,
                              vtss_appl_mep_pm_conf_t   *const conf);

/* instance:  Instance number of MEP.                 */
/* conf:      Link State Tracking Configuration data for this instance    */
u32 vtss_mep_mgmt_lst_conf_set(const u32                       instance,
                               const vtss_appl_mep_lst_conf_t  *const conf);

u32 vtss_mep_mgmt_lst_conf_get(const u32                  instance,
                               vtss_appl_mep_lst_conf_t   *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:      Configuration data for this instance    */
u32 vtss_mep_mgmt_syslog_conf_set(const u32                          instance,
                                  const vtss_appl_mep_syslog_conf_t  *const conf);

u32 vtss_mep_mgmt_syslog_conf_get(const u32                     instance,
                                  vtss_appl_mep_syslog_conf_t   *const conf);

/* instance:    Instance number of MEP.                                         */
/* CC based on CCM is enabled/disabled. When period is VTSS_MEP_MGMT_PERIOD_300S, CCM is HW driven - otherwise SW driven. */
/* The parameters 'period' and 'cast' must be the same as for vtss_mep_mgmt_lm_conf_set() if both CC and LM is configured */
/* to be based on SW driven CCM session.                                                                                  */
u32 vtss_mep_mgmt_cc_conf_set(const u32                        instance,
                              const vtss_appl_mep_cc_conf_t    *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_cc_conf_get(const u32                 instance,
                              vtss_appl_mep_cc_conf_t   *const conf);

/* instance:    Instance number of MEP.                                     */
/* LM based on either CCM/LMM (depending on 'ended') is enabled/disabled.   */
/* The parameters 'period' and 'cast' must be the same as for vtss_mep_mgmt_cc_conf_set() */
/* if both CC and LM is configured to be based on SW driven CCM session.                  */
u32 vtss_mep_mgmt_lm_conf_set(const u32                      instance,
                              const vtss_appl_mep_lm_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_lm_conf_get(const u32                instance,
                              vtss_appl_mep_lm_conf_t  *const conf);

/* instance:    Instance number of MEP.                                                  */
/* LM Availability based on either CCM/LMM (depending on 'ended') is enabled/disabled.   */
u32 vtss_mep_mgmt_lm_avail_conf_set(const u32                            instance,
                                    const vtss_appl_mep_lm_avail_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_lm_avail_conf_get(const u32                      instance,
                                    vtss_appl_mep_lm_avail_conf_t  *const conf);

/* instance:    Instance number of MEP.                                                  */
/* LM HLI based on either CCM/LMM (depending on 'ended') is enabled/disabled.   */
u32 vtss_mep_mgmt_lm_hli_conf_set(const u32                          instance,
                                  const vtss_appl_mep_lm_hli_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_lm_hli_conf_get(const u32                    instance,
                                  vtss_appl_mep_lm_hli_conf_t  *const conf);

/* instance:    Instance number of MEP.                                                  */
/* LM SDEG based on either CCM/LMM (depending on 'ended') is enabled/disabled.   */
u32 vtss_mep_mgmt_lm_sdeg_conf_set(const u32                           instance,
                                   const vtss_appl_mep_lm_sdeg_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_lm_sdeg_conf_get(const u32                     instance,
                                   vtss_appl_mep_lm_sdeg_conf_t  *const conf);

/* instance:    Instance number of MEP.                                   */
/* DM based on either 1DM/DMM (depending on 'way') is enabled/disabled.   */
u32 vtss_mep_mgmt_dm_conf_set(const u32                       instance,
                              const vtss_mep_mgmt_dm_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_dm_conf_get(const u32                instance,
                              vtss_mep_mgmt_dm_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* Enable/disable of APS. The APS data contained in the APS PDU is controlled by EPS */
u32 vtss_mep_mgmt_aps_conf_set(const u32                       instance,
                               const vtss_appl_mep_aps_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_aps_conf_get(const u32                 instance,
                               vtss_appl_mep_aps_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* Enable/disable of Link Trace. */
u32 vtss_mep_mgmt_lt_conf_set(const u32                      instance,
                              const vtss_appl_mep_lt_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_lt_conf_get(const u32                 instance,
                              vtss_appl_mep_lt_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* Enable/disable of Loop Back. */
u32 vtss_mep_mgmt_lb_conf_set(const u32                      instance,
                              const vtss_mep_mgmt_lb_conf_t  *const conf,
                                                             u64 *active_time_ms);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_lb_conf_get(const u32                 instance,
                              vtss_mep_mgmt_lb_conf_t  *const conf);

/* instance:    Instance number of MEP. */
/* Clear LB counters for this MEP      */
u32 vtss_mep_mgmt_lb_state_clear_set(const u32    instance);

/* instance:    Instance number of MEP.                 */
/* Get the Loop Back Result                             */
u32 vtss_mep_mgmt_lb_state_get(const u32                  instance,
                               vtss_appl_mep_lb_state_t   *const state);

/* instance:    Instance number of MEP.                 */
/* Get the Link Trace Result                            */
u32 vtss_mep_mgmt_lt_state_get(const u32                  instance,
                               vtss_appl_mep_lt_state_t   *const state);

/* instance:    Instance number of MEP. */
/* Get the ETH state of this MEP        */
u32 vtss_mep_mgmt_state_get(const u32                instance,
                            vtss_mep_mgmt_state_t    *const state);

/* instance:    Instance number of MEP. */
/* Get the CCM state of this MEP        */
mesa_rc vtss_mep_mgmt_cc_state_get(const u32                    instance,
                                   vtss_appl_mep_cc_state_t     *const state);

/* instance:    Instance number of MEP. */
/* Get the LM counters of this MEP related and this peer MEP-id     */
u32 vtss_mep_mgmt_lm_state_get(const u32                   instance,
                               const u32                   peer,
                               vtss_appl_mep_lm_state_t    *const state);

/* instance:    Instance number of MEP. */
/* Clear LM counters for this MEP       */
u32 vtss_mep_mgmt_lm_state_clear_set(const u32                      instance,
                                     const vtss_appl_mep_clear_dir  direction);

/* instance:    Instance number of MEP. */
/* Get the LM Availability status of this MEP      */
u32 vtss_mep_mgmt_lm_avail_state_get(const u32                       instance,
                                     const u32                       peer,
                                     vtss_appl_mep_lm_avail_state_t  *const state);

/* instance:    Instance number of MEP. */
/* Get the LM HLI status of this MEP    */
u32 vtss_mep_mgmt_lm_hli_state_get(const u32                     instance,
                                   const u32                     peer_id,
                                   vtss_appl_mep_lm_hli_state_t  *const state);

/* instance:    Instance number of MEP. */
/* Get the Latest timestamps of this MEP         */
u32 vtss_mep_mgmt_dm_timestamp_get(const u32           instance,
                                   mep_dm_timestamp_t  *const dm1_timestamp_far_to_near,
                                   mep_dm_timestamp_t  *const dm1_timestamp_near_to_far);

/* instance:    Instance number of MEP. */
/* Get the DM state per prio of this MEP         */
u32 vtss_mep_mgmt_dm_prio_state_get(const u32                   instance,
                                    const u32                   prio,
                                    vtss_appl_mep_dm_state_t    *const tw_state,
                                    vtss_appl_mep_dm_state_t    *const fn_state,
                                    vtss_appl_mep_dm_state_t    *const nf_state);

/* instance:    Instance number of MEP. */
/* Get the debug DM state of this MEP         */
u32 vtss_mep_mgmt_dm_db_state_get(const u32  instance,
                                  u32        *delay,
                                  u32        *delay_var);

/* instance:    Instance number of MEP. */
/* Clear DM counters for this MEP       */
u32 vtss_mep_mgmt_dm_state_clear_set(const u32    instance);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* Enable/disable of ETH-AIS. */
u32 vtss_mep_mgmt_ais_conf_set(const u32                      instance,
                               const vtss_appl_mep_ais_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_ais_conf_get(const u32                 instance,
                               vtss_appl_mep_ais_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* Enable/disable of ETH-LCK */
u32 vtss_mep_mgmt_lck_conf_set(const u32                       instance,
                               const vtss_appl_mep_lck_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_lck_conf_get(const u32                 instance,
                               vtss_appl_mep_lck_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* Enable/disable of Test frame generation. */
u32 vtss_mep_mgmt_tst_conf_set(const u32                       instance,
                               const vtss_mep_mgmt_tst_conf_t  *const conf,
                                     u64                       *active_time_ms);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_tst_conf_get(const u32                 instance,
                               vtss_mep_mgmt_tst_conf_t  *const conf);

/* instance:    Instance number of MEP. */
/* Clear TST counters for this MEP      */
u32 vtss_mep_mgmt_tst_state_clear_set(const u32    instance);

/* instance:    Instance number of MEP.            */
/* Get the test Result                             */
u32 vtss_mep_mgmt_tst_state_get(const u32                   instance,
                                vtss_appl_mep_tst_state_t   *const state);

void  vtss_mep_mgmt_up_mep_enable(void);

/* instance:    Instance number of MEP.                 */
/* Change happend that require re-configuration of the MEP instance    */
u32 vtss_mep_mgmt_change_set(const u32 instance,  BOOL evc_change,  BOOL e_tree);

/* instance:    Instance number of MEP.                 */
/* Port protection change happend that require re-configuration of the MEP instance    */
u32 vtss_mep_mgmt_protection_change_set(const u32   instance);

/* instance:    Instance number of MEP.                 */
/* Interface status changed for this instance    */
u32 vtss_mep_mgmt_is_tlv_change_set(const u32   instance);

/* Some port status changed the MEP instance    */
u32 vtss_mep_mgmt_ps_tlv_change_set(void);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* Client configuration set */
u32 vtss_mep_mgmt_client_conf_set(const u32                 instance,
                                  const mep_client_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_client_conf_get(const u32           instance,
                                  mep_client_conf_t  *const conf);

void  vtss_mep_mgmt_default_conf_get(mep_default_conf_t  *const conf);











































/****************************************************************************/
/*  MEP module call in interface                                            */
/****************************************************************************/

/* instance:    Instance number of MEP.             */
/* aps:         transmitted APS specific info.      */
/* event:       transmit APS as an ERPS event.      */
/* This called by EPS/ERPS to transmit APS specific info. The first three APS is transmitted with 3.3 ms. interval and thereafter with 5 s. interval.  */
/* The length of the 'aps' array depends on the aps type configuration but is VTSS_MEP_APS_DATA_LENGTH maximum                                         */
/* If 'aps' contains the already transmitting APS, nothing happens.                                                                                    */
u32 vtss_mep_tx_aps_info_set(const u32   instance,
                             const u8    *aps,
                             const BOOL  event);


/* instance:    Instance number of MEP      */
/* state:       SSF state                   */
/* This is called by module when new SSF state is detected */
void vtss_mep_new_ssf_state(const u32    instance,
                            const BOOL   state);




/* instance:    Instance number of MEP.                                    */
/* enable:      TRUE means that R-APS is transmitted at 5 sec. interval    */
/* R-APS transmission from the MEP related port, can be enabled/disabled   */
u32 vtss_mep_raps_transmission(const u32    instance,
                               const BOOL   enable);


/* instance:    Instance number of MEP.       */
/* This is called by external to activate MEP calling out with SF state and received APS */
u32 vtss_mep_signal_in(const u32   instance);


/* evc_inst:    Instance number of EVC.     */
/* This is called when there is any change to a EVC configuration  */
void vtss_mep_evc_change_callback(u16  evc_inst);

/* instance:    Instance number of MEP      */
/* lm_counters: LM counters                 */
/* This is called by module (supp) when a new LM counter measurement has been done */
void vtss_mep_lm_measurement(const u32 instance,
                             const u32 peer_index,
                             vtss_mep_supp_lm_peer_counters_t *peer_counters);

const char* vtss_mep_rate_to_string(vtss_appl_mep_rate_t rate);
const char* vtss_mep_flow_to_name(mep_domain_t domain, u32 flow, vtss_uport_no_t port_no, char *str);
const char* vtss_mep_domain_to_string(mep_domain_t domain);
const char* vtss_mep_direction_to_string(vtss_appl_mep_direction_t direction);
const char* vtss_mep_unit_to_string(vtss_appl_mep_dm_tunit_t unit);


/****************************************************************************/
/*  MEP module call out interface                                           */
/****************************************************************************/
/* instance:    Instance number of MEP.              */
/* domain:      Domain.                              */
/* aps_info:    Received APS specific info.          */
/* This is called by MEP to deliver received APS specific info to EPS/ERPS     */
void vtss_mep_rx_aps_info_set(const u32                      instance,
                              const mep_domain_t             domain,
                              const u8                       *aps);


/* instance:    Instance number of MEP.                  */
/* domain:      Domain.                                  */
/* sf_state:    SF state delivered to EPS.               */
/* sd_state:    SD state delivered to EPS.               */
/* This is called by MEP to deliver SF/SD info to EPS    */
void vtss_mep_sf_sd_state_set(const u32                      instance,
                              const mep_domain_t             domain,
                              const BOOL                     sf_state,
                              const BOOL                     sd_state);


/* instance:    Instance number of MEP.                    */
/* domain:      Domain.                                    */
/* This is called by MEP to activate external to call in on all in functions     */
void vtss_mep_signal_out(const u32                       instance,
                         const mep_domain_t              domain);


/* instance:    Instance number of MEP.                    */
/* Notify external that MEP state has changed */
void vtss_mep_status_change(const u32  instance,
                            const BOOL enable);


/* instance:    Instance number of MEP.                    */
/* Get LM notification state                               */
void vtss_mep_lm_notif_status_get(const u32  instance,
                                  vtss_appl_mep_lm_notif_state_t *state);

/* instance:    Instance number of MEP.                    */
/* Set LM notification state                               */
/* state == NULL to delete                                 */
void vtss_mep_lm_notif_status_set(const u32  instance,
                                  vtss_appl_mep_lm_notif_state_t *state);

/* instance:    Instance number of MEP.                    */
/* Get LM HLI state                                        */
void vtss_mep_lm_hli_status_get(const u32  instance,
                                const u32  peer_id,
                                vtss_appl_mep_lm_hli_state_t *state);

/* instance:    Instance number of MEP.                    */
/* Set LM HLI state                                        */
/* state == NULL to delete                                 */
void vtss_mep_lm_hli_status_set(const u32  instance,
                                const u32  peer_id,
                                vtss_appl_mep_lm_hli_state_t *state);

/* instance:    Instance number of MEP.     */
/* port:        Port number.                */
/* This is called by MEP to register on the relevant ports     */
void vtss_mep_port_register(const u32              instance,
                            const mesa_port_list_t &port);


BOOL vtss_mep_port_los_get(u32 port);

/* Expected to return the CPU queue to get OAM frames into. */
mesa_packet_rx_queue_t vtss_mep_oam_cpu_queue_get(void);

/****************************************************************************/
/*  MEP platform call in interface                                          */
/****************************************************************************/

/* stop:    Returnvalue indicating if calling this thread has to stop.                                                   */
/* This is the thread driving timing in the MEP. Has to be called every 'timer_resolution' ms. by platform until 'stop'  */
/* Initially this thread is not called until MEP callout on vtss_mep_timer_start()                                       */
void vtss_mep_timer_thread(BOOL  *const stop);


/* This is the thread driving the state machine. Has to be call when MEP is calling out on vtss_mep_run() */
void vtss_mep_run_thread(void);


/* This is the thread driving poll of counters and 'hit me once' of HW CCM ACL rules */
void vtss_mep_ccm_thread(void);


/* timer_resolution:    This is the interval of calling vtss_mep_run_thread() in ms.    */
/* This is the initializing of MEP. Has to be called by platform                        */
u32 vtss_mep_init(const u32  timer_resolution, u32 mep_pag, u32 mip_pag);

/****************************************************************************/
/*  MEP platform call out interface                                         */
/****************************************************************************/

/* This is call by MEP upper logic to lock/unlock critical code protection */
void vtss_mep_crit_lock(void);

void vtss_mep_crit_unlock(void);

/* This is called by MEP upper logic when vtss_mep_run_thread(void) has to be called */
void vtss_mep_run(void);

/* This is called by MEP upper logic when vtss_mep_timer_thread(BOOL  *const stop) has to be called until 'stop' is indicated */
void vtss_mep_timer_start(void);

/* This is called by MEP upper logic in order to do debug tracing */
void vtss_mep_trace(const char   *string,
                    const u32    param1,
                    const u32    param2,
                    const u32    param3,
                    const u32    param4);

void vtss_mep_lm_trace(const char  *string,
                       const u32   param1,
                       const u32   param2,
                       const u32   param3,
                       const u32   param4);

void vtss_mep_dm_trace(const char  *string,
                       const u32   param1,
                       const u32   param2,
                       const u32   param3,
                       const u32   param4);

void vtss_mep_discard_trace(const char  *string,
                            const u32   param1,
                            const u32   param2,
                            const u32   param3,
                            const u32   param4);

/* This is called by MEP upper logic in order to do syslog */
/**
 * Definition of syslog type.
 */
typedef enum {
    VTSS_MEP_REMOTE_MEP_DOWN,
    VTSS_MEP_REMOTE_MEP_UP,
    VTSS_MEP_ENTER_AIS,
    VTSS_MEP_EXIT_AIS,
    VTSS_MEP_CROSS_CONNECTED_SERVICE,
    VTSS_MEP_CROSSCHECK_MEP_MISSING,
    VTSS_MEP_CROSSCHECK_MEP_UNKNOWN,
    VTSS_MEP_CROSSCHECK_SERVICE_UP,
    VTSS_MEP_FORWARDING_LOOP,
    VTSS_MEP_CONFIG_ERROR,
} vtss_mep_syslog_t;

void vtss_mep_syslog(vtss_mep_syslog_t type, const u32 param1, const u32 param2);

/* This is called by MEP upper logic in order to decide if the loss measurement notificaiton is needed */
void vtss_mep_lm_notificaiton(u32 instance, const vtss_appl_mep_lm_conf_t *const conf, const vtss_appl_mep_lm_state_t *const state);

/* This is called by MEP upper logic in order to get own MAC */
void vtss_mep_mac_get(u32 port,  u8 mac[VTSS_APPL_MEP_MAC_LENGTH]);

typedef enum
{
    VTSS_MEP_TAG_NONE,
    VTSS_MEP_TAG_C,
    VTSS_MEP_TAG_S,
    VTSS_MEP_TAG_S_CUSTOM,
} vtss_mep_tag_type_t;

/* This is called by MEP to get relevant data for this EVC    */
BOOL vtss_mep_evc_flow_info_get(const u32                  evc_inst,
                                mesa_port_list_t           &nni,
                                mesa_port_list_t           &uni,
                                u32                        *const vid,
                                u32                        *const evid,
                                vtss_mep_tag_type_t        *const ttype,
                                u32                        *const tvid,
                                BOOL                       *const tunnel,
                                u32                        *const tag_cnt,
                                BOOL                       *e_tree,
                                mesa_port_list_t           &leaf_uni,
                                u32                        *const leaf_vid,
                                u32                        *const leaf_evid);

#define VTSS_MEP_COS_ID_SIZE 8
void vtss_mep_evc_cos_id_get(const u32  evc_inst,
                             BOOL       cos_id[VTSS_MEP_COS_ID_SIZE]);

/* This is called by MEP upper logic in order to get PVID and Port type on this port */
BOOL vtss_mep_pvid_get(u32 port, u32 *pvid, vtss_mep_tag_type_t *ptype);

BOOL vtss_mep_vlan_get(const u32 vid,   mesa_port_list_t &nni);

BOOL vtss_mep_port_counters_get(u32 instance,  u32 port,  mesa_port_counters_t *counters);

BOOL vtss_mep_protection_port_get(const u32 port,  u32 *p_port);

u32 vtss_mep_port_count(void);

/* This is called by MEP upper logic in order to control VLAN aware on a port */
BOOL vtss_mep_vlan_aware_port(u32 port,  BOOL enable);

/* This is called by MEP upper logic in order to control VLAN port member ship */
BOOL vtss_mep_vlan_member(u32 vid,  mesa_port_list_t &ports,  BOOL enable);

/* This is called by MEP upper logic in order to add MAC address for PHY h/w timestamp */
BOOL vtss_mep_phy_config_mac(const u8  port,
                             const u8  is_add,
                             const u8  is_ing,
                             const u8  mac[VTSS_APPL_MEP_MAC_LENGTH]);

BOOL vtss_mep_port_prio_set(u32 port,  u32 prio,  BOOL enable);

void vtss_mep_port_conf_disable(const u32       instance,
                                const u32       port,
                                const BOOL      disable);

void vtss_mep_rule_update_failed(u32 vid);

#define VTSS_OAM_ANY_TYPE        0xFFFFFFFF
typedef struct
{
    u32   vid;
    u32   oam;      /* VTSS_OAM_ANY_TYPE is used to indicate any OAM type - not a specific OAM type */
    u32   level;
    u32   mask;
//    u8    mac[VTSS_MEP_MAC_LENGTH];
    mesa_port_list_t ing_port;
    mesa_port_list_t eg_port;
    BOOL  cpu;
    BOOL  ts;
    BOOL  hw_ccm;
} vtss_mep_acl_entry;

void vtss_mep_acl_del_vid(u32  vid);

void vtss_mep_acl_del_instance(u32 instance);

BOOL vtss_mep_acl_add(vtss_mep_acl_entry    *acl);

BOOL vtss_up_mep_acl_add(u32 pag_oam, u32 pag_port, u32 customer, u32 first_egress_loop);

BOOL vtss_mep_acl_raps_add(u32 vid,  mesa_port_list_t &ports,  u8 *mac,  u32 *id);

void vtss_mep_acl_dm_add(mep_domain_t domain,  u32 vid,  u32 pag,  u8 level);

void vtss_mep_acl_raps_del(u32 id);

BOOL vtss_mep_acl_ccm_add(u32 instance,  u32 vid,  mesa_port_list_t &port,  u32 level,  u8 *smac,  u32 pag,  BOOL hw_ccm);

void vtss_mep_acl_ccm_del(u32 instance);

void vtss_mep_acl_ccm_hit(u32 instance);

void vtss_mep_acl_ccm_count(u32 instance,  u32 *count);

void vtss_mep_acl_ccm_mac_change(u32 instance,  u8 *mac);

BOOL vtss_mep_acl_tst_add(u32 instance,  u32 vid,  mesa_port_list_t &port,  u32 level,  u32 pag);

void vtss_mep_acl_tst_count(u32 instance,  u32 *count);

void vtss_mep_acl_tst_clear(u32 instance);

typedef struct {
    u32 tx_count[VTSS_APPL_MEP_PRIO_MAX][2];
    u32 rx_count[VTSS_APPL_MEP_PRIO_MAX][2];
} vtss_mep_test_sat_count;

void vtss_mep_acl_test_sat_count(u32 instance,  vtss_mep_test_sat_count *count);

void vtss_mep_acl_test_sat_clear(u32 instance);

BOOL vtss_mep_acl_evc_add(BOOL mep,  mesa_port_list_t &mip_nni,  mesa_port_list_t &mip_uni,  mesa_port_list_t &mip_egress,  u32 mep_pag,  u32 mip_pag);

BOOL vtss_mep_acl_mip_lbm_add(u32 instance,  u32 dmac_port,  mesa_port_list_t &ports,  u32 mip_pag,  BOOL isdx_to_zero);

BOOL vtss_mep_acl_mip_llm_add(u32 instance, mesa_mac_t dmac, u8 level, mesa_port_list_t &ports, u32 mip_pag);

BOOL vtss_mep_acl_etree_elan_add(u32 instance, u32 isdx, u32 port, mesa_port_list_t *ports, u32 pag, BOOL second);
void vtss_mep_acl_etree_elan_del(u32 instance);

/*
 * Setup LLM PDU forwarding to CPU for VOE-based MEP
 */
BOOL vtss_mep_acl_llm_voe_add(u32 instance, u32 isdx, u32 level, mesa_port_list_t &ports);

/*
 * Remove existing ACL entry for LLM PDU forwarding to CPU
 */
BOOL vtss_mep_acl_llm_del(u32 instance);

}  // mep_g1

#endif /* _VTSS_MEP_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
