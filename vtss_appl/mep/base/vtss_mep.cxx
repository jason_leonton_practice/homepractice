/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#define MEP_IMPL_G1

#include "misc_api.h"
#include "mscc/ethernet/switch/api.h"
#include "vtss/appl/evc.h"
#include "vtss/appl/mep.h"
#include "vtss/appl/vlan.h"
#include "vtss_mep_supp_api.h"
#include "packet_api.h"

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_MEP

#include "vtss_mep.h" /* Interface between base and platform */
#include "main.h"
#include <string.h>
#include "icli_porting_util.h"






namespace mep_g1 {

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_MEP

/*lint -sem( vtss_mep_crit_lock, thread_lock ) */
/*lint -sem( vtss_mep_crit_unlock, thread_unlock ) */

/****************************************************************************/
/*  MEP global variables                                                    */
/****************************************************************************/

/****************************************************************************/
/*  MEP local variables                                                     */
/****************************************************************************/

#define     EVENT_IN_CONFIG        0x0000000000000002LL
#define     EVENT_IN_CC_CONFIG     0x0000000000000004LL
#define     EVENT_IN_LM_CONFIG     0x0000000000000008LL
#define     EVENT_IN_DM_CONFIG     0x0000000000000010LL
#define     EVENT_IN_APS_CONFIG    0x0000000000000020LL
#define     EVENT_IN_LT_CONFIG     0x0000000000000040LL
#define     EVENT_IN_AIS_CONFIG    0x0000000000000100LL
#define     EVENT_IN_LCK_CONFIG    0x0000000000000200LL
#define     EVENT_IN_CLIENT_CONFIG 0x0000000000000800LL
#define     EVENT_IN_TST_CLEAR     0x0000000000001000LL
#define     EVENT_IN_LM_CLEAR      0x0000000000002000LL
#define     EVENT_IN_DM_CLEAR      0x0000000000004000LL
#define   EVENT_IN_LB_CLEAR        0x0000000000008000LL
#define     EVENT_IN_APS_INFO      0x0000000000010000LL
#define     EVENT_IN_DEFECT_STATE  0x0000000000020000LL
#define     EVENT_IN_SSF_SSD_STATE 0x0000000000040000LL
#define     EVENT_IN_LTR_NEW       0x0000000000080000LL
#define     EVENT_IN_LBR_NEW       0x0000000000100000LL
#define     EVENT_IN_DMR_NEW       0x0000000000200000LL
#define     EVENT_IN_1DM_NEW       0x0000000000400000LL
#define     EVENT_IN_CCM_STATE     0x0000000000800000LL
#define     EVENT_IN_TST_NEW       0x0000000001000000LL
#define     EVENT_IN_LT_TIMER      0x0000000004000000LL
#define     EVENT_IN_LB_TIMER      0x0000000008000000LL
#define     EVENT_IN_TST_TIMER     0x0000000010000000LL
#define   EVENT_IN_TST_DONE_TIMER  0x0000000020000000LL
#define   EVENT_IN_LB_DONE_TIMER   0x0000000040000000LL
#define     EVENT_IN_SYSLOG_TIMER  0x0000000080000000LL
#define     EVENT_IN_TLV_CHANGE    0x0000000100000000LL
#define     EVENT_IN_MAC_NEW       0x0000000200000000LL
#define     EVENT_IN_LST_CONFIG    0x0000000400000000LL
#define     EVENT_IN_TLV_IS_NEW    0x0000000800000000LL
#define     EVENT_IN_LST_IGN_TIMER 0x0000001000000000LL
#define     EVENT_IN_LST_SF_TIMER  0x0000002000000000LL
#define     EVENT_IN_ETREE_NEW     0x0000004000000000LL





#define     EVENT_IN_MASK          0x0000007FFFFFFFFFLL


#define     EVENT_OUT_SIGNAL       0x8000000000000000LL
#define     EVENT_OUT_APS          0x4000000000000000LL
#define     EVENT_OUT_SF_SD        0x2000000000000000LL
#define     EVENT_OUT_STATE        0x1000000000000000LL
#define     EVENT_OUT_MASK         0xF000000000000000LL

#define MEP_LTR_MAX                 5
#define MEP_DM_MAX                  VTSS_APPL_MEP_DM_LASTN_MAX    /* Max number of DM counters saved in RAM */
#define MEP_DM_TS_MCAST             0

#define MEP_DEFAULT_SYSLOG_TIMEOUT  (30 * 1000)




#define MPLS_DOMAIN(domain) (FALSE)


#define MEP_AVAIL_FLR_TH_MAX        1000    /* Maximum value for availability FLR threshold parameter (== 100%) */

#define MEP_FLR_SCALE_FACTOR        10000   // Frame-loss-ratio scale factor

#define MEP_AVAIL_INTERVAL_DEF      10      /* Default value for availability interval parameter */
#define MEP_AVAIL_INTERVAL_MIN      1       /* Minimum value for availability interval parameter */
#define MEP_AVAIL_INTERVAL_MAX      1000    /* Maximum value for availability interval parameter */

#undef VTSS_TRACE_MODULE_ID
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_MEP

typedef struct {
    u32      next_prt;
    u32      delay[MEP_DM_MAX];
    u32      delay_var[MEP_DM_MAX];

    u64      sum;
    u64      var_sum;
    u64      n_sum;
    u64      n_var_sum;
    u32      delay_cnt;

    u32      last_delay;
} delay_state_t;

typedef struct
{
    vtss_mep_supp_defect_state_t    defect_state;
    delay_state_t                   tw_state[VTSS_APPL_MEP_PRIO_MAX];
    delay_state_t                   fn_state[VTSS_APPL_MEP_PRIO_MAX];
    delay_state_t                   nf_state[VTSS_APPL_MEP_PRIO_MAX];
    vtss_mep_supp_is_tlv_t          is_tlv;
    BOOL                            is_received;
} supp_state_t;

typedef struct
{
    u32     transaction_id;
    u32     timer;
} lt_state_t;

typedef struct
{
    BOOL    enable[VTSS_APPL_MEP_PRIO_MAX][2];
    u32     timer;
    u32     done_timer;
    u32     rx_transaction_id[VTSS_APPL_MEP_PEER_MAX];
} lb_state_t;

/*
 * Loss Measurement instance state
 */
typedef struct
{
    BOOL                        clear_counters;         // Clear counters on config change
    vtss_appl_mep_clear_dir     clear_dir;              // Counter direction to clear.
} lm_state_t;

typedef struct
{
    u32             flr_interval_count;

    i32             near_los_counter;
    i32             far_los_counter;
    u32             near_tx_counter;
    u32             far_tx_counter;
    u32             tot_near_tx_counter;
    u32             tot_far_tx_counter;
} lm_peer_state_t;

typedef struct {
    u32                     near_cnt;
    u32                     far_cnt;
} lm_hli_state_t;

typedef struct
{
    BOOL                      active;
    BOOL                      rx_count_active;
    u32                       rx_frame_size;
    u32                       timer;
    u32                       done_timer;
    u32                       tst_count;
} tst_state_t;

typedef struct
{
    BOOL      fe_disable;
    u32       ign_timer;
    u32       sf_timer;
} lst_state_t;

typedef struct
{
    u32     timer;
    u32     peer_mep_timer[VTSS_APPL_MEP_PEER_MAX];
    u32     transaction_id[VTSS_APPL_MEP_PEER_MAX];
} syslog_state_t;

typedef struct
{
    vtss_mep_mgmt_state_t           state;
    vtss_appl_mep_lm_state_t        lm_state[VTSS_APPL_MEP_PEER_MAX];
    vtss_appl_mep_lm_avail_state_t  lm_avail_state[VTSS_APPL_MEP_PEER_MAX];
    vtss_appl_mep_lm_hli_state_t    lm_hli_state[VTSS_APPL_MEP_PEER_MAX];
    vtss_appl_mep_lt_state_t        lt_state;
    vtss_appl_mep_lb_state_t        lb_state;
    vtss_appl_mep_tst_state_t       tst_state;
    vtss_appl_mep_dm_state_t        tw_state[VTSS_APPL_MEP_PRIO_MAX];
    vtss_appl_mep_dm_state_t        fn_state[VTSS_APPL_MEP_PRIO_MAX];
    vtss_appl_mep_dm_state_t        nf_state[VTSS_APPL_MEP_PRIO_MAX];
    u8                              rx_aps[VTSS_MEP_APS_DATA_LENGTH];
} out_state_t;

typedef struct
{
    u32                           instance;
    mep_conf_t                    config;                            /* Input from management */
    vtss_appl_mep_cc_conf_t       cc_config;
    vtss_appl_mep_pm_conf_t       pm_config;
    vtss_appl_mep_lst_conf_t      lst_config;
    vtss_appl_mep_syslog_conf_t   syslog_config;
    vtss_appl_mep_lm_conf_t       lm_config;
    vtss_mep_supp_lm_peer_counters_t  *lm_avail_fifo;                    /* containing lm_counters */
    u32                           lm_avail_fifo_ix;
    vtss_appl_mep_lm_avail_conf_t lm_avail_config;
    vtss_appl_mep_lm_hli_conf_t   lm_hli_config;
    lm_hli_state_t                lm_hli_state[VTSS_APPL_MEP_PEER_MAX];
    vtss_appl_mep_lm_sdeg_conf_t  lm_sdeg_config;
    BOOL                          lm_sdeg_bad;
    BOOL                          dDeg;
    u32                           lm_sdeg_bad_cnt;
    u32                           lm_sdeg_good_cnt;
    u32                           los_int_cnt_timer;
    u32                           los_th_cnt_timer;
    u32                           hli_cnt_timer;
    vtss_mep_mgmt_dm_conf_t       dm_config;
    vtss_appl_mep_aps_conf_t      aps_config;
    vtss_appl_mep_lt_conf_t       lt_config;
    vtss_mep_mgmt_lb_conf_t       lb_config;
    vtss_appl_mep_ais_conf_t      ais_config;                        /* AIS configuration input from management */
    vtss_appl_mep_lck_conf_t      lck_config;                        /* LCK configuration input from management */
    vtss_mep_mgmt_tst_conf_t      tst_config;                        /* TST configuration input from management */
    mep_client_conf_t             client_config;                     /* Client configuration input from management */
    BOOL                          vola;                              /* This instance is created volatile */
    BOOL                          sat;                               /* This instance is in SAT mode. */
    vtss_mep_supp_evc_type_t      evc_type;                          /* This instance is in a EVC E-TREE. */
    mep_sat_prio_conf_t           sat_prio_conf[VTSS_APPL_MEP_PRIO_MAX]; /* The SAT EVC priority configuration */

    supp_state_t                  supp_state;                         /* Input from lower support */

    u8                            tx_aps[VTSS_MEP_APS_DATA_LENGTH];   /* input from module interface */
    BOOL                          ssf_state;
    BOOL                          raps_forward;

    out_state_t                   out_state;                          /* output state */

    u64                           event_flags;                        /* Flags used to indicate what input/output events has activated the 'run' thread */

    lt_state_t                    lt_state;                           /* Internal state */
    lb_state_t                    lb_state;
    tst_state_t                   tst_state;
    lst_state_t                   lst_state;
    syslog_state_t                syslog_state;
    lm_state_t                    lm_state;
    lm_peer_state_t               lm_peer_state[VTSS_APPL_MEP_PEER_MAX];
    u32                           rule_vid;                           /* The VID of rules for this flow - used for create/delete of PDU capture/forward rules */
    u32                           leaf_vid;                           /* The E-TREE Leaf VID */
    mesa_port_list_t              rule_ports;                         /* This ports are used to create capture/forward rules */
    BOOL                          tunnel;                             /* This MEP is related to a tunnel EVC */
    u32                           ace_count;                          /* Last counter value from HW CCM ACE rule */
    u32                           ps_tlv_value;




} mep_instance_data_t;


static CapArray<mep_instance_data_t, VTSS_APPL_CAP_MEP_INSTANCE_MAX> instance_data;
static vtss_appl_mep_os_tlv_t os_tlv_config;
static u32                    vlan_raps_acl_id[VTSS_VIDS];
static u32                    vlan_raps_mac_octet[VTSS_VIDS];
static u32                    timer_res;
static u8 all_zero_mac[VTSS_APPL_MEP_MAC_LENGTH] = {0,0,0,0,0,0};
static u8 raps_mac[VTSS_APPL_MEP_MAC_LENGTH] = {0x01,0x19,0xA7,0x00,0x00,0x01};
static u8 class1_multicast[8][VTSS_APPL_MEP_MAC_LENGTH] = {{0x01,0x80,0xC2,0x00,0x00,0x30},
                                                      {0x01,0x80,0xC2,0x00,0x00,0x31},
                                                      {0x01,0x80,0xC2,0x00,0x00,0x32},
                                                      {0x01,0x80,0xC2,0x00,0x00,0x33},
                                                      {0x01,0x80,0xC2,0x00,0x00,0x34},
                                                      {0x01,0x80,0xC2,0x00,0x00,0x35},
                                                      {0x01,0x80,0xC2,0x00,0x00,0x36},
                                                      {0x01,0x80,0xC2,0x00,0x00,0x37}};

typedef struct
{
    u32 cnt;        /* Number of ACL rules for this level range */
    u8 level[4];    /* There can be a max of four ACL rules implementing a level range */
    u8 mask[4];     /* Mask is pointing on significant bits in level - '0' indicate 'don't care' */
} level_table_t;
                                           /* level 0-0  */
static level_table_t level_table[8][8] = {{{1,{0,0,0,0},{7,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}},
                                           /* level 1-0                level 1-1  */
                                          {{1,{0,0,0,0},{6,0,0,0}}, {1,{1,0,0,0},{7,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}},
                                           /* level 2-0                level 2-1                level 2-2  */
                                          {{2,{2,0,0,0},{7,6,0,0}}, {2,{2,1,0,0},{7,7,0,0}}, {1,{2,0,0,0},{7,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}},
                                           /* level 3-0                level 3-1                level 3-2                level 3-3  */
                                          {{1,{0,0,0,0},{4,0,0,0}}, {2,{2,1,0,0},{6,7,0,0}}, {1,{2,0,0,0},{6,0,0,0}}, {1,{3,0,0,0},{7,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}},
                                           /* level 4-0                level 4-1                level 4-2                level 4-3                level 4-4  */
                                          {{2,{4,0,0,0},{7,4,0,0}}, {3,{4,2,1,0},{7,6,7,0}}, {2,{4,2,0,0},{7,6,0,0}}, {2,{4,3,0,0},{7,7,0,0}}, {1,{4,0,0,0},{7,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}},
                                           /* level 5-0                level 5-1                level 5-2                level 5-3                level 5-4                level 5-5  */
                                          {{2,{4,0,0,0},{6,4,0,0}}, {3,{4,2,1,0},{6,6,7,0}}, {2,{4,2,0,0},{6,6,0,0}}, {2,{4,3,0,0},{6,7,0,0}}, {1,{4,0,0,0},{6,0,0,0}}, {1,{5,0,0,0},{7,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}},
                                           /* level 6-0                level 6-1                level 6-2                level 6-3                level 6-4                level 6-5                level 6-6  */
                                          {{3,{6,4,0,0},{7,6,4,0}}, {4,{6,4,2,1},{7,6,6,7}}, {3,{6,4,2,0},{7,6,6,0}}, {3,{6,4,3,0},{7,6,7,0}}, {2,{6,4,0,0},{7,6,0,0}}, {2,{6,5,0,0},{7,7,0,0}}, {1,{6,0,0,0},{7,0,0,0}}, {0,{0,0,0,0},{0,0,0,0}}},
                                           /* level 7-0                level 7-1                level 7-2                level 7-3                level 7-4                level 7-5                level 7-6                level 7-7 */
                                          {{1,{0,0,0,0},{0,0,0,0}}, {3,{4,2,1,0},{4,6,7,0}}, {2,{4,2,0,0},{4,6,0,0}}, {2,{4,3,0,0},{4,7,0,0}}, {1,{4,0,0,0},{4,0,0,0}}, {2,{6,5,0,0},{6,7,0,0}}, {1,{6,0,0,0},{6,0,0,0}}, {1,{7,0,0,0},{7,0,0,0}}}};

static u32      base_mep_pag = 0;
static u32      base_mip_pag = 0;

/****************************************************************************/
/*  MEP local functions                                                     */
/****************************************************************************/
static void run_ssf_ssd_state(u32 instance);
static void run_cc_config(u32 instance);
static void run_lm_config(u32 instance);
static u32 run_aps_config(u32 instance);
static void run_ais_config(u32 instance);
static void run_lck_config(u32 instance);



static vtss_mep_supp_direction_t supp_direction_calc(vtss_appl_mep_direction_t  direction);
static vtss_mep_supp_rate_t supp_rate_calc(vtss_appl_mep_rate_t rate);
void  vtss_mep_mgmt_default_conf_get(mep_default_conf_t  *const def_conf);

static void instance_data_clear(u32 instance,  mep_instance_data_t *data)
{
    u32     rule_vid, i, j;                           /* The VID of rules for this flow - used for create/delete of PDU capture/forward rules */
    mesa_port_list_t rule_ports;
    mep_default_conf_t  def_conf;

    vtss_mep_mgmt_default_conf_get(&def_conf);

    rule_vid = data->rule_vid;
    memcpy(rule_ports, data->rule_ports, sizeof(rule_ports));

    memset(data, 0, sizeof(mep_instance_data_t));

    data->config = def_conf.config;
    data->cc_config = def_conf.cc_conf;
    data->lm_config = def_conf.lm_conf;
    data->lm_avail_config = def_conf.lm_avail_conf;
    data->lm_hli_config = def_conf.lm_hli_conf;
    data->lm_sdeg_config = def_conf.lm_sdeg_conf;
    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        data->dm_config.dm[i] = def_conf.dm_prio_conf;
    }
    data->dm_config.common = def_conf.dm_common_conf;
    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            data->lb_config.tests[i][j] = def_conf.test_prio_conf;
        }
    }
    data->lb_config.common = def_conf.lb_common_conf;
    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            data->tst_config.tests[i][j] = def_conf.test_prio_conf;
        }
    }
    data->tst_config.common = def_conf.tst_common_conf;
    data->lt_config = def_conf.lt_conf;
    data->aps_config = def_conf.aps_conf;
    data->ais_config = def_conf.ais_conf;
    data->lck_config = def_conf.lck_conf;
    data->client_config.flow_count = 0;





    data->instance = instance;
    data->rule_vid = rule_vid;
    memcpy(data->rule_ports, rule_ports, sizeof(data->rule_ports));
}


#if defined(VTSS_SW_OPTION_ACL)
#define ACL_LIST_MAX        64
#define OAM_LTM_TYPE        05
#define OAM_CCM_TYPE        01

typedef struct
{
    u32   oam;
    u32   level;
    mesa_port_list_t ing_port;
    mesa_port_list_t eg_port;
    BOOL  cpu;
    BOOL  hw_ccm;
    BOOL  range;
    u32   seq;
} acl_logic_rule_t;
static acl_logic_rule_t acl_logic_list[ACL_LIST_MAX];
static u32 acl_logic_cnt;

static BOOL acl_logical_rule_list(u32 vid,  mesa_port_list_t &ports,  mep_instance_data_t **inst,  u32 inst_cnt)
{
    u32     i;
    i32     lvl_cnt;
    BOOL    mip_ing, mip_eg, mep_ing, mep_eg;

    mesa_port_list_t mep_blk;
    mesa_port_list_t mep_fwr;
    mesa_port_list_t mip_blk;
    mesa_port_list_t mip_fwr_ing;
    mesa_port_list_t mip_fwr_eg;
    mesa_port_list_t hw_ccm;
    mesa_port_list_t all_zero_port;

    if (vid == 0)   return(FALSE);

    acl_logic_cnt = 0;
    memset(acl_logic_list, 0, sizeof(acl_logic_list));
    mep_blk.clear_all(); /* Default MEP is not blocking any ports */
    mep_fwr = ports;     /* Default MEP is forwarding all ports   */
    all_zero_port.clear_all();
    mep_ing = mep_eg = FALSE;
    for (lvl_cnt=7; lvl_cnt>=0; --lvl_cnt)
    {/* Run through all levels from the top */
        mip_ing = mip_eg = FALSE;

        hw_ccm.clear_all(); /* On this level no HW CCM on any ports */
        mip_blk = mep_blk;
        mip_fwr_ing = mep_fwr;
        mip_fwr_eg = mep_fwr;

        for (i=0; i<inst_cnt; ++i)
        {/* Run through all instances in this flow to find MIP on this level */
            if ((inst[i]->config.level == lvl_cnt) && (inst[i]->config.mode == VTSS_APPL_MEP_MIP))
            {/* A MIP on this level */
                if (inst[i]->config.direction == VTSS_APPL_MEP_DOWN)
                {/* Ingress */
                    mip_ing = TRUE;
                    mip_blk[inst[i]->config.port] = TRUE;       /* Ingress MIP is blocking LTM on this port */
                    mip_fwr_ing[inst[i]->config.port] = FALSE;  /* Ingress MIP is not forwarding LTM from this port */
                }
                if (inst[i]->config.direction == VTSS_APPL_MEP_UP)
                {/* Egress */
                    mip_eg = TRUE;
                    mip_fwr_eg[inst[i]->config.port] = FALSE;  /* Egress MIP is not forwarding LTM to this port */
                }
            }
        }
        for (i=0; i<inst_cnt; ++i)
        {/* Run through all instances in this flow to find MEP on this level */
            if ((inst[i]->config.level == lvl_cnt) && (inst[i]->config.mode == VTSS_APPL_MEP_MEP))
            {
                if (inst[i]->config.direction == VTSS_APPL_MEP_DOWN)
                    mep_ing = TRUE;
                if (inst[i]->config.direction == VTSS_APPL_MEP_UP)
                    mep_eg = TRUE;

                if ((inst[i]->config.domain != VTSS_APPL_MEP_EVC) || ports[inst[i]->config.port])    /* For now - block rules are not made on EVC UNI ports (UNI is not in 'ports' array )*/
                    mep_blk[inst[i]->config.port] = TRUE;       /* Ingress/Egress MEP is blocking any PDU on this port */
                mep_fwr[inst[i]->config.port] = FALSE;          /* Ingress/Egress MEP is not forwarding any PDU to/from this port */
                mip_blk[inst[i]->config.port] = TRUE;           /* Ingress/Egress MEP is blocking LTM on this port */
                mip_fwr_ing[inst[i]->config.port] = FALSE;      /* Ingress/Egress MEP is not forwarding LTM from this port */
                mip_fwr_eg[inst[i]->config.port] = FALSE;       /* Ingress/Egress MEP is not forwarding LTM to this port */
            }
        }

        if (mip_ing)
        {   /* Create ingress MIP block rule */
            acl_logic_list[acl_logic_cnt].oam = OAM_LTM_TYPE;
            acl_logic_list[acl_logic_cnt].level = lvl_cnt;
            memcpy(acl_logic_list[acl_logic_cnt].ing_port, mip_blk, sizeof(acl_logic_list[acl_logic_cnt].ing_port));
            acl_logic_list[acl_logic_cnt].cpu = TRUE;
            acl_logic_list[acl_logic_cnt].range = FALSE;
            acl_logic_list[acl_logic_cnt].seq = 0;
            if ((++acl_logic_cnt) >= ACL_LIST_MAX)    return(FALSE);
        }
        if (mip_eg)
        {   /* Create egress MIP forward rule */
            acl_logic_list[acl_logic_cnt].oam = OAM_LTM_TYPE;
            acl_logic_list[acl_logic_cnt].level = lvl_cnt;
            memcpy(acl_logic_list[acl_logic_cnt].ing_port, mip_fwr_ing, sizeof(acl_logic_list[acl_logic_cnt].ing_port));
            memcpy(acl_logic_list[acl_logic_cnt].eg_port, mip_fwr_eg, sizeof(acl_logic_list[acl_logic_cnt].eg_port));
            acl_logic_list[acl_logic_cnt].cpu = TRUE;
            acl_logic_list[acl_logic_cnt].range = FALSE;
            acl_logic_list[acl_logic_cnt].seq = 1;
            if ((++acl_logic_cnt) >= ACL_LIST_MAX)    return(FALSE);
        }
        if (mep_ing)
        {/* Create ingress MEP HW CCM rules */
            for (i=0; i<mep_caps.mesa_port_cnt; ++i)
            {/* HW CCM on all ports */
                if (hw_ccm[i])
                {/* HW CCM must be created on this port */
                    acl_logic_list[acl_logic_cnt].oam = OAM_CCM_TYPE;
                    acl_logic_list[acl_logic_cnt].level = lvl_cnt;
                    acl_logic_list[acl_logic_cnt].ing_port[i] = TRUE;
                    acl_logic_list[acl_logic_cnt].cpu = FALSE;      /* Hit Me Once must be used */
                    acl_logic_list[acl_logic_cnt].range = FALSE;
                    acl_logic_list[acl_logic_cnt].hw_ccm = TRUE;
                    acl_logic_list[acl_logic_cnt].seq = 2;
                    if ((++acl_logic_cnt) >= ACL_LIST_MAX)    return(FALSE);
                }
            }
        }
        if (mep_ing || mep_eg)
        {   /* Create MEP block rule */
            acl_logic_list[acl_logic_cnt].oam = VTSS_OAM_ANY_TYPE;
            acl_logic_list[acl_logic_cnt].level = lvl_cnt;
            memcpy(acl_logic_list[acl_logic_cnt].ing_port, mep_blk, sizeof(acl_logic_list[acl_logic_cnt].ing_port));
            memset(acl_logic_list[acl_logic_cnt].eg_port, FALSE, sizeof(acl_logic_list[acl_logic_cnt].eg_port));
            acl_logic_list[acl_logic_cnt].cpu = (mep_ing);
            acl_logic_list[acl_logic_cnt].range = TRUE;
            acl_logic_list[acl_logic_cnt].seq = 3;
            if (memcmp(mep_blk, all_zero_port, sizeof(mep_blk))) /* Don't create a block rule where ingress ports are all zero */
                if ((++acl_logic_cnt) >= ACL_LIST_MAX)    return(FALSE);

            /* Create MEP forward rule */
            acl_logic_list[acl_logic_cnt].oam = VTSS_OAM_ANY_TYPE;
            acl_logic_list[acl_logic_cnt].level = lvl_cnt;
            memcpy(acl_logic_list[acl_logic_cnt].ing_port, mep_fwr, sizeof(acl_logic_list[acl_logic_cnt].ing_port));
            memcpy(acl_logic_list[acl_logic_cnt].eg_port, mep_fwr, sizeof(acl_logic_list[acl_logic_cnt].eg_port));
            acl_logic_list[acl_logic_cnt].cpu = (mep_eg);
            acl_logic_list[acl_logic_cnt].range = TRUE;
            acl_logic_list[acl_logic_cnt].seq = 4;
            if (memcmp(mep_fwr, all_zero_port, sizeof(mep_fwr))) /* Don't create a forward rule where forward ports are all zero */
                if ((++acl_logic_cnt) >= ACL_LIST_MAX)    return(FALSE);
        }
    }
    return(TRUE);
}


static BOOL acl_luton26_rule_list(u32  vid)
{
    u32                  i, seq, inx, first_level, last_level, oam;
    mesa_port_list_t     ing_port, eg_port;
    BOOL                 cpu, hw_ccm;
    vtss_mep_acl_entry   acl;

    acl.ing_port = ing_port;

    if (!acl_logic_cnt)     return(TRUE);

    /* On LUTON 26 - logical rules covering a range of levels can (in most cases) be implemented by fewer ACL rules */
    /* The logical range can be implemented as a set of ACL rules using don't cares in level - mask is pointing on significant bits */

    for (seq=0; seq<5; ++seq)
    {/* In a sequence there is max one logical rule on each level - the sequence number is also giving the order ACL rules are created */
        inx = 0;
        do
        {/* Keep on until all logical rules in this sequence are transformed to ACL rules */
            for (inx=inx; inx<acl_logic_cnt; ++inx)     if (acl_logic_list[inx].seq == seq)     break;  /* Search for first rule in the level range (if it is a range) */

            if (inx<acl_logic_cnt)
            {/* A rule was found */
                first_level = acl_logic_list[inx].level;    /* First level in range is this level */
                last_level = first_level;                   /* So far last level in range is also this level */
                cpu = acl_logic_list[inx].cpu;              /* Save this information for creation of ACL rules */
                oam = acl_logic_list[inx].oam;
                hw_ccm = acl_logic_list[inx].hw_ccm;
                memcpy(ing_port, acl_logic_list[inx].ing_port, sizeof(ing_port));
                memcpy(eg_port, acl_logic_list[inx].eg_port, sizeof(eg_port));

                if (acl_logic_list[inx].range)
                {/* This rule is part of a level range */
                    for (inx=inx+1; inx<acl_logic_cnt; ++inx)   /* Search for end of level range - or end of logical list */
                    {
                        if (acl_logic_list[inx].seq != seq)     continue;
                        if ((oam == acl_logic_list[inx].oam) && !memcmp(acl_logic_list[inx].ing_port, ing_port, sizeof(ing_port)) && !memcmp(acl_logic_list[inx].eg_port, eg_port, sizeof(eg_port)))
                        {/* This rule is in the same range */
                            cpu = cpu || acl_logic_list[inx].cpu;    /* Copy to CPU is an OR of all rules in the range */
                            last_level = acl_logic_list[inx].level;  /* New last level in range is the level of this rule */
                        }
                        else    break;  /* A rule is found that is not part of this level range (start of next range) - index is now pointing on next logical rule */
                    }
                }
                else    inx++;  /* This rule is a single rule - index must point on next logical rule */

                /* Create new ACL rules for this level range */
                acl.vid = vid;
                acl.oam = oam;
                acl.cpu = cpu;
                acl.ts = TRUE;
                acl.hw_ccm = hw_ccm;
                memcpy(acl.ing_port, ing_port, sizeof(ing_port));
                memcpy(acl.eg_port, eg_port, sizeof(eg_port));

                for (i=0; i<level_table[first_level][last_level].cnt; ++i)
                {/* Create the set of ACL rules */
                    acl.level = level_table[first_level][last_level].level[i];
                    acl.mask = level_table[first_level][last_level].mask[i];

                    if (!vtss_mep_acl_add(&acl))    return(FALSE);
                }
            }
        } while(inx<acl_logic_cnt);
    }

    return(TRUE);
}

BOOL acl_list_update(u32 vid,  mesa_port_list_t &ports,  mep_instance_data_t **inst,  u32 inst_cnt)
{
    vlan_raps_acl_id[vid] = 0xFFFFFFFF;   /* This VID no longer got an ACL rule */

    if (!acl_logical_rule_list(vid, ports, inst, inst_cnt))   return(FALSE);
    if (!acl_luton26_rule_list(vid))   return(FALSE);
    return(TRUE);
}
#endif /* VTSS_SW_OPTION_ACL */

/* Initialize MAC address entry */
void mep_mac_init(mesa_mac_table_entry_t *entry)
{
    memset(entry, 0, sizeof(*entry));
    entry->locked = 1;
    entry->copy_to_cpu = 1;
    entry->cpu_queue = vtss_mep_oam_cpu_queue_get();
}

#if !defined(VTSS_SW_OPTION_ACL)
static u8  ccm_mac[6] = {0x01,0x80,0xC2,0x00,0x00,0x30};

static BOOL mac_table_update(u32 vid,  mesa_port_list_t &ports,  mep_instance_data_t **inst,  u32 inst_cnt)
{
    mesa_rc                 ret_val=0, ret=0;
    u32                     i, j, port_cnt;
    i32                     lvl_cnt;
    mesa_port_list_t        class_port;
    mesa_port_list_t        tmp_port;
    BOOL                    mep_found, mip_found, ingress_mep;
    mesa_mac_table_entry_t  entry;

    /* Initialize entry parameters */
    mep_mac_init(&entry);
    entry.vid_mac.vid = vid;
    memcpy(entry.vid_mac.mac.addr, ccm_mac, 6);
    memset(entry.destination, 0, sizeof(entry.destination));

    mep_found = FALSE;
    mip_found = FALSE;
    ingress_mep = FALSE;
    class_port = ports;
    tmp_port.clear_all();

    for (lvl_cnt=7; lvl_cnt>=0; --lvl_cnt)
    {/* Run through all levels from the top */
        entry.vid_mac.mac.addr[5] &= 0xF0;            /* Multicast Class 1 and level */
        entry.vid_mac.mac.addr[5] |= lvl_cnt & 0x07;

        if (!mep_found)
        {/* MEP not found in this VID yet */
            for (i=0; i<inst_cnt; ++i)    if ((inst[i]->config.level == lvl_cnt) && (inst[i]->config.mode == VTSS_APPL_MEP_MEP))  break;
            mep_found = (i<inst_cnt);
        }
        if (!ingress_mep)
        {/* Ingress MEP not found in this VID yet */
            for (i=0; i<inst_cnt; ++i)    if ((inst[i]->config.level == lvl_cnt) && (inst[i]->config.mode == VTSS_APPL_MEP_MEP) && (inst[i]->config.direction == VTSS_APPL_MEP_DOWN))  break;
            if (i<inst_cnt)
            {   /* Ingress MEP found - means no forwarding on this level and down - on any ports */
                ingress_mep = TRUE;
                class_port.clear_all();
                tmp_port.clear_all();
            }
        }

        if (!mep_found) /* Check for delete of MAC entry */
        {/* MEP not found in this VID yet */
            ret += mesa_mac_table_del(NULL, &entry.vid_mac);
            for (i=0; i<inst_cnt; ++i)    if ((inst[i]->config.level == lvl_cnt) && (inst[i]->config.mode == VTSS_APPL_MEP_MIP))  break;
            mip_found = (i<inst_cnt);
            if (!mip_found)
            {/* No MIP either */
                entry.vid_mac.mac.addr[5] |= 0x08;            /* Multicast Class 2 */
                ret += mesa_mac_table_del(NULL, &entry.vid_mac);
            }
        }

        if (mep_found || mip_found)
        { /* MEP is found on this VID or MIP is found on this level */
            if (!ingress_mep)
            {/* ingress MEP not found yet - calculate the port mask for MAC entry */
                tmp_port = class_port;
                for (i=0; i<inst_cnt; ++i)
                {
                    if ((inst[i]->config.level == lvl_cnt) && (inst[i]->config.mode == VTSS_APPL_MEP_MEP) && (inst[i]->config.direction == VTSS_APPL_MEP_UP))
                    {/* Egress MEP found on this level - don't forward class 1 and class 2 to this port on this level and down */
                        class_port[inst[i]->config.port] = tmp_port[inst[i]->config.port] = FALSE;
                        for (j=0, port_cnt=0; j<mep_caps.mesa_port_cnt; ++j)    if (class_port[j])  port_cnt++;
                        if (port_cnt<2)
                        { /* Only forward if more that one port 'in front' when egress MEP */
                            class_port.clear_all;
                            tmp_port.clear_all;
                        }
                    }
                    if ((inst[i]->config.level == lvl_cnt) && (inst[i]->config.mode == VTSS_APPL_MEP_MIP) && (inst[i]->config.direction == VTSS_APPL_MEP_DOWN))
                    /* Inress MIP found on this level - don't forward class 2 to ports behind on this level */
                        for (port_cnt=0; port_cnt<mep_caps.mesa_port_cnt; ++port_cnt)    if (inst[i]->config.port != port_cnt)  tmp_port[port_cnt] = FALSE;
                    if ((inst[i]->config.level == lvl_cnt) && (inst[i]->config.mode == VTSS_APPL_MEP_MIP) && (inst[i]->config.direction == VTSS_APPL_MEP_UP))
                    /* Egress MIP found on this level - don't forward class 2 to this port on this level */
                        tmp_port[inst[i]->config.port] = FALSE;
                }
            }

            if (mep_found)
            { /* Multicast Class 1 is only for MEP */
                for (i=0; i<inst_cnt; ++i)
                    if ((inst[i]->config.level == lvl_cnt) && inst[i]->cc_config.enable && vtss_mep_supp_hw_ccm_generator_get(supp_rate_calc(inst[i]->cc_config.rate)))  break;    /* If any MEP has HW CCM on this level - no copy to CPU */
                if (i<inst_cnt)     entry.copy_to_cpu = 0;
                entry.destination = class_port;
                ret_val += mesa_mac_table_add(NULL, &entry);
                entry.copy_to_cpu = 1;
            }

            entry.vid_mac.mac.addr[5] |= 0x08;            /* Multicast Class 2 */
            entry.destination = tmp_port;
            ret_val += mesa_mac_table_add(NULL, &entry);
        }
    } /* for to */

    if (ret != VTSS_RC_OK)      vtss_mep_trace("mac_table_update: Return value is not OK", vid, 0, 0, 0);
    return((ret_val == VTSS_RC_OK));
}
#endif

/*
 * Return the index of the peer with the given peer MEP ID
 */
static BOOL get_peer_index(mep_instance_data_t *data, u32 peer_id, u32 &peer_index)
{
    u32 i;
    for (i = 0; i < data->config.peer_count; ++i) {/* Search for peer MEP-ID */
        if (data->config.peer_mep[i] == peer_id)
            break;
    }
    if (i == data->config.peer_count)
        return FALSE;

    peer_index = i;
    return TRUE;
}

static u32 raps_forwarding_control(u32 vid)
{
    mesa_port_list_t tmp_port;
    u32  i, j, port_cnt, forward, voe_cnt, voe[2], octet=01;

    if (vid == 0)       return VTSS_MEP_RC_OK;

    tmp_port.clear_all();
    memset(voe, 0, sizeof(voe));

    /* Count how many MEP instances that has RAPS enabled and check if forwardig is enabled */
    for (i=0, port_cnt=0, voe_cnt=0, forward=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
    {
        if (!port_cnt)  octet = instance_data[i].aps_config.raps_octet;
        if (instance_data[i].config.enable && instance_data[i].aps_config.enable && (instance_data[i].rule_vid == vid) &&
            (instance_data[i].aps_config.type == VTSS_APPL_MEP_R_APS) && (octet == instance_data[i].aps_config.raps_octet))
        {/* RAPS is enabled in this VID for this instance */
            port_cnt++;
            if (instance_data[i].raps_forward)      forward++;
            if (instance_data[i].config.voe) {
                if (voe_cnt < 2)   voe[voe_cnt] = i;
                voe_cnt++;
            }
            for (j=0; j<mep_caps.mesa_port_cnt; ++j)  if (instance_data[i].rule_ports[j])     tmp_port[j] = TRUE;
        }
    }
#if defined(VTSS_SW_OPTION_ACL)
//printf("raps_forwarding_control  voe_cnt %u  forward %u  port_cnt %u\n",voe_cnt, forward, port_cnt);
    if (voe_cnt == 1)   /* Only one VOE with RAPS - disable forwarding */
        (void)vtss_mep_supp_raps_forwarding(voe[0], (forward == 2) && (port_cnt == 2));
    else if (voe_cnt == 2) { /* Two VOE with RAPS - enable forwarding if both has forwarding active */
        (void)vtss_mep_supp_raps_forwarding(voe[0], (forward == 2) && (port_cnt == 2));
        (void)vtss_mep_supp_raps_forwarding(voe[1], (forward == 2) && (port_cnt == 2));
    }
    if (voe_cnt < 2) { /* SW MEP - RAPS must be controlled using ACL rules */
        if (vlan_raps_acl_id[vid] != 0xFFFFFFFF)
        {/* This VID got an RAPS ACL rule - delete it */
            vtss_mep_acl_raps_del(vlan_raps_acl_id[vid]);
            vlan_raps_acl_id[vid] = 0xFFFFFFFF;
        }

        if ((port_cnt == 2) && (forward == 2))  /* Two MEP want to forward */
        {
            raps_mac[5] = octet;
            if (!vtss_mep_acl_raps_add(vid,  tmp_port,  raps_mac,  &vlan_raps_acl_id[vid])) {
                return VTSS_MEP_RC_TCAM_CONFIGURATION;  /* No RAPS ACL rule on this VID yet - create one */
            }
        }
    }

    if (mep_caps.mesa_mep_serval) {
        /* On Serval the port MEP VLAN is only created when two MEP has enabled ERPS */
        (void)vtss_mep_vlan_member(vid, tmp_port, (port_cnt==2));
    }
#else  // VTSS_SW_OPTION_ACL
    mesa_mac_table_entry_t    entry;
    mep_mac_init(&entry);
    memcpy(entry.vid_mac.mac.addr, raps_mac, sizeof(entry.vid_mac.mac.addr));
    entry.vid_mac.vid = vid;

    if (vlan_raps_mac_octet[vid] != 0xFFFFFFFF)
    {/* This VID has a MAC entry - delete entry */
        entry.vid_mac.mac.addr[5] = vlan_raps_mac_octet[vid];
        vlan_raps_mac_octet[vid] = 0xFFFFFFFF;
        if (mesa_mac_table_del(NULL, &entry.vid_mac) != VTSS_RC_OK) {
            return VTSS_MEP_RC_INTERNAL;
        }
    }

    if (port_cnt)
    {/* At least one MEP has RAPS enabled */
        entry.vid_mac.mac.addr[5] = octet;
        if ((port_cnt == 2) && (forward == 2))  memcpy(entry.destination, tmp_port, sizeof(entry.destination));  /* Two MEP with RAPS enabled and forwarding enabled - add entry with active destination ports */
        else                                    memset(entry.destination, 0, sizeof(entry.destination));         /* Otherwise no active destination ports */
        if (mesa_mac_table_add(NULL, &entry) != VTSS_RC_OK) {
            return VTSS_MEP_RC_INTERNAL;
        }
        vlan_raps_mac_octet[vid] = octet;
    }

#endif  // VTSS_SW_OPTION_ACL
    return VTSS_MEP_RC_OK;
}


static BOOL pdu_fwr_cap_update_luton26(u32 vid,  mesa_port_list_t &ports)
{
    mep_instance_data_t     *inst[VTSS_APPL_MEP_INSTANCE_MAX];
    u32                     i, port_cnt, inst_cnt;
    mesa_mac_table_entry_t  entry;
    mesa_rc                 ret_val=0, ret=0;
    mesa_port_list_t        tmp_port;
//printf("pdu_fwr_cap_update  vid %lu  ports %u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u\n", vid, ports[0], ports[1], ports[2], ports[3], ports[4], ports[5], ports[6], ports[7], ports[8], ports[9], ports[10], ports[11]);
    if (vid == 0)   return(FALSE);

    memset(inst, 0, sizeof(inst));

    for (inst_cnt=0, i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
    {/* Search for relevant instances on this VID */
        if (instance_data[i].config.enable && (instance_data[i].rule_vid == vid)) {
            inst[inst_cnt++] = &instance_data[i];
        }
    }

#if !defined(VTSS_SW_OPTION_ACL)
    if (!mac_table_update(vid,  ports,  inst,  inst_cnt))     return(FALSE);
#endif

#if defined(VTSS_SW_OPTION_ACL)
    vtss_mep_acl_del_vid(vid);                /* All ACL rules (IS2) in this VID is deleted */

    if (inst_cnt)
        if (!acl_list_update(vid,  ports,  inst,  inst_cnt))     return(FALSE);
#endif
    for (i=0; i<inst_cnt; ++i) { /* Add ACL rules for CCM and TST PDU reception */
        if (inst[i]->config.mode == VTSS_APPL_MEP_MIP) continue;
        tmp_port.clear_all();
        if (inst[i]->config.direction == VTSS_APPL_MEP_DOWN)
            tmp_port[inst[i]->config.port] = TRUE;
        else
        if (inst[i]->config.direction == VTSS_APPL_MEP_UP)
        {
            tmp_port = inst[i]->rule_ports;
            tmp_port[inst[i]->config.port] = FALSE;
        }
        if (inst[i]->cc_config.enable && (inst[i]->config.peer_count == 1))  /* ACL rule need to be added to handle CCM */
            if (!vtss_mep_acl_ccm_add(inst[i]->instance,  inst[i]->rule_vid,  tmp_port,  inst[i]->config.level,  inst[i]->config.peer_mac[0].addr, inst[i]->config.evc_pag, vtss_mep_supp_hw_ccm_generator_get(supp_rate_calc(inst[i]->cc_config.rate)))) return(FALSE);
        if (!vtss_mep_acl_tst_add(inst[i]->instance,  inst[i]->rule_vid,  tmp_port,  inst[i]->config.level, inst[i]->config.evc_pag)) return(FALSE);
    }

    mep_mac_init(&entry);
    entry.vid_mac.vid = vid;
    entry.destination.clear_all();

    for (port_cnt=0; port_cnt<mep_caps.mesa_port_cnt; ++port_cnt)
    {   /* If any MEP/MIP is created resident on this port it needs a unicast MAC entry with this port MAC adresse */
        vtss_mep_mac_get(port_cnt, entry.vid_mac.mac.addr);
        for (i=0; i<inst_cnt; ++i)
#if defined(VTSS_SW_OPTION_ACL)
            if ((inst[i]->config.port == port_cnt) && (inst[i]->config.mode == VTSS_APPL_MEP_MIP))  break;       /* On Luton26 and Jaguar a MEP does not need unicast MAC entry as this is handled by the ACL rule - A MIP does need as the ACL rule is for LTM only */
#else
            if (inst[i]->config.port == port_cnt)  break;
#endif
        if (i<inst_cnt)            ret_val += mesa_mac_table_add(NULL, &entry);
        else                       ret += mesa_mac_table_del(NULL, &entry.vid_mac);
    }

    tmp_port.clear_all();
    for (i=0; i<inst_cnt; ++i)
    {
        /* A tagged port MEP need a port that is member of that VLAN in order to assure reception of frames if filtering is enabled */
        if (inst[i]->config.domain == VTSS_APPL_MEP_PORT)
            tmp_port[inst[i]->config.port] = TRUE;
    }
    if (!vtss_mep_vlan_member(vid, tmp_port, (port_cnt!=0)))     return(FALSE);     /* Ports need to be member of VLAN */

    if (raps_forwarding_control(vid) != VTSS_MEP_RC_OK)  return FALSE;   /* Add rules for RAPS capture and forwarding */

    vtss_mep_trace("pdu_fwr_cap_update  exit", 0, 0, 0, 0);

    return((ret_val == VTSS_RC_OK));
}

static BOOL pdu_fwr_cap_update_serval(u32 vid,  mesa_port_list_t &ports)
{
    mep_instance_data_t  *data;
    u32   i, j;
    mesa_port_list_t tmp_port;
    mesa_port_list_t mip_uni, mip_nni, mip_egress;
    BOOL  mep;
    if (vid == 0)   return(FALSE);
    tmp_port.clear_all();
    mip_uni.clear_all();
    mip_nni.clear_all();
    mip_egress.clear_all();

    mep = FALSE;

    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
    {/* Search for all EVC SW (not VOE based) MEP/MIP */
        data = &instance_data[i];

        if (MPLS_DOMAIN(data->config.domain))   /* Do not do this for MPLS domain MEP */
            continue;

        if (data->config.enable && !data->config.voe && (!VTSS_MEP_SUPP_ETREE_ELAN(data->evc_type) || (data->config.mode == VTSS_APPL_MEP_MIP))) {   /* An enabled SW (IS2) based MEP/MIP */
            if (data->rule_vid == vid)  vtss_mep_acl_del_instance(i);   /* All ACL rules (IS2) for this instance is deleted */

            tmp_port.clear_all();
            if (data->config.direction == VTSS_APPL_MEP_DOWN) {
                tmp_port[data->config.port] = TRUE;
            } else if (data->config.direction == VTSS_APPL_MEP_UP) {
                tmp_port = data->rule_ports;
                tmp_port[data->config.port] = FALSE;
            }
            if ((data->config.domain == VTSS_APPL_MEP_PORT) || (data->config.domain == VTSS_APPL_MEP_VLAN)) {   /* Port MEP or VLAN MEP */
                if ((data->config.mode == VTSS_APPL_MEP_MEP) && (data->rule_vid == vid)) {
                    /* Add ACL rules for HW CCM and TST PDU reception */
                    if (data->cc_config.enable && (data->config.peer_count == 1))  /* ACL rule need to be added to hanle CCM */
                        if (!vtss_mep_acl_ccm_add(data->instance,  vid,  tmp_port,  data->config.level,  data->config.peer_mac[0].addr, base_mep_pag, vtss_mep_supp_hw_ccm_generator_get(supp_rate_calc(data->cc_config.rate)))) return FALSE;
                    if (!vtss_mep_acl_tst_add(data->instance,  vid,  tmp_port,  data->config.level, base_mep_pag))   return FALSE;
                }
            }

            if (data->config.domain == VTSS_APPL_MEP_EVC) {   /* EVC */
                if ((data->config.mode == VTSS_APPL_MEP_MEP) && (data->rule_vid == vid)) {
                    /* Add ACL rules for HW CCM and TST PDU reception */
                    if (data->cc_config.enable && (data->config.peer_count == 1))  /* ACL rule need to be added to hanle CCM */
                        if (!vtss_mep_acl_ccm_add(data->instance,  vtss_mep_supp_rx_isdx_get(i, 0),  tmp_port,  data->config.level,  data->config.peer_mac[0].addr, base_mep_pag, vtss_mep_supp_hw_ccm_generator_get(supp_rate_calc(data->cc_config.rate)))) return FALSE;
                    if (!vtss_mep_acl_tst_add(data->instance,  vtss_mep_supp_rx_isdx_get(i, 0),  tmp_port,  data->config.level, base_mep_pag)) return FALSE;
                }
            }

            if (data->config.domain == VTSS_APPL_MEP_EVC) {   /* EVC MIP */
                if ((data->config.mode == VTSS_APPL_MEP_MIP) && (data->config.direction == VTSS_APPL_MEP_DOWN)) {
                    mip_uni[data->config.port] = TRUE;  /* An ingress MIP is enabled on this UNI */
                    for (j=0; j<mep_caps.mesa_port_cnt; ++j)
                        if (data->rule_ports[j])
                            mip_egress[j] = TRUE;  /* The egress ports for a down MIP is all NNI */
                    if (data->rule_vid == vid) {
                        /* Add ACL rules for UNI LBM PDU reception */
                        if (!vtss_mep_acl_mip_lbm_add(data->instance, data->config.port, tmp_port, base_mip_pag, TRUE))    return FALSE;
                        /* Add ACL rules for UNI LLM PDU reception */
                        if (!vtss_mep_acl_mip_llm_add(data->instance, data->config.mac, data->config.level, tmp_port, base_mip_pag))    return FALSE;
                    }
                }
                if ((data->config.mode == VTSS_APPL_MEP_MIP) && (data->config.direction == VTSS_APPL_MEP_UP)) {
                    for (j=0; j<mep_caps.mesa_port_cnt; ++j)
                        if (data->rule_ports[j])
                            mip_nni[j] = TRUE;  /* An egress MIP is enabled on this NNI */
                    mip_egress[data->config.port] = TRUE;  /* The egress ports for a up MIP is the UNI */
                    if (data->rule_vid == vid) {
                        /* Add ACL rules for NNI LBM PDU reception */
                        if (!vtss_mep_acl_mip_lbm_add(data->instance, data->config.port, tmp_port, base_mip_pag, FALSE))   return FALSE;
                        /* Add ACL rules for NNI LLM PDU reception */
                        if (!vtss_mep_acl_mip_llm_add(data->instance, data->config.mac, data->config.level, tmp_port, base_mip_pag))    return FALSE;
                    }
                }
            }
        }

        if (data->config.enable && (data->config.mode == VTSS_APPL_MEP_MEP) && !data->config.voe && !VTSS_MEP_SUPP_ETREE_ELAN(data->evc_type))
            mep = TRUE;

        if (!data->config.enable && ((data->config.domain == VTSS_APPL_MEP_EVC) || (data->config.domain == VTSS_APPL_MEP_VLAN)) && !data->config.voe)   /* All not enabled SW (IS2) based EVC MEP/MIP must be deleted */
            if (data->rule_vid == vid){
                vtss_mep_acl_del_instance(i);   /* All ACL rules (IS2) for this instance is deleted */
            if (data->rule_vid == vid)  vtss_mep_acl_del_instance(i);   /* All ACL rules (IS2) for this instance is deleted */
            } 

        if (data->config.enable &&
            (data->config.domain == VTSS_APPL_MEP_EVC || data->config.domain == VTSS_APPL_MEP_VLAN) &&
            data->config.voe &&
            data->config.mode == VTSS_APPL_MEP_MEP) {
            // setup LLM PDU forwarding to CPU to VOE-based EVC or VLAN MEP (both uses the saved RX ISDX value)
            if (data->rule_vid == vid) {
                vtss_mep_acl_del_instance(i);   /* All ACL rules (IS2) for this instance is deleted */

                tmp_port.clear_all();
                if (data->config.direction == VTSS_APPL_MEP_DOWN) {
                    tmp_port[data->config.port] = TRUE;
                } else if (data->config.direction == VTSS_APPL_MEP_UP) {
                    tmp_port = data->rule_ports;
                    tmp_port[data->config.port] = FALSE;
                }

                if (!vtss_mep_acl_llm_voe_add(data->instance, vtss_mep_supp_rx_isdx_get(i, 0), data->config.level, tmp_port)) {
                    return(FALSE);
                }
            }
        }
    }

    /* ADD ACL rules common for all MEP/MIP instances */
    if (!vtss_mep_acl_evc_add(mep, mip_nni, mip_uni, mip_egress, base_mep_pag, base_mip_pag)) {
        T_W("vtss_mep_acl_evc_add failed");
        return FALSE;
    }

    if (raps_forwarding_control(vid) != VTSS_MEP_RC_OK) {
        return FALSE;   /* Add rules for RAPS capture and forwarding */
    }

    vtss_mep_trace("pdu_fwr_cap_update  exit", 0, 0, 0, 0);

    return(TRUE);
}

static BOOL pdu_fwr_cap_update(u32 vid,  mesa_port_list_t &ports)
{
    if (mep_caps.mesa_mep_serval) {
        return pdu_fwr_cap_update_serval(vid, ports);
    }
    if (mep_caps.mesa_mep_luton26) {
        return pdu_fwr_cap_update_luton26(vid, ports);
    }
    return FALSE;
}


static u32 supp_meas_interval_calc(vtss_appl_mep_rate_t rate,  u32 meas_interval)
{
    u32 frame_interval_in_ms;

    switch (rate)
    {
        case VTSS_APPL_MEP_RATE_100S:    frame_interval_in_ms = 10;     break;
        case VTSS_APPL_MEP_RATE_10S:     frame_interval_in_ms = 100;    break;
        case VTSS_APPL_MEP_RATE_1S:      frame_interval_in_ms = 1000;   break;
        case VTSS_APPL_MEP_RATE_6M:      frame_interval_in_ms = 10000;  break;
        case VTSS_APPL_MEP_RATE_1M:      frame_interval_in_ms = 60000;  break;
        case VTSS_APPL_MEP_RATE_6H:      frame_interval_in_ms = 600000; break;
        default:                         return 0;
    }

    return (meas_interval/frame_interval_in_ms);
}

static vtss_mep_supp_rate_t supp_rate_calc(vtss_appl_mep_rate_t rate)
{
    switch (rate)
    {
        case VTSS_APPL_MEP_RATE_300S:    return(VTSS_MEP_SUPP_RATE_300S);
        case VTSS_APPL_MEP_RATE_100S:    return(VTSS_MEP_SUPP_RATE_100S);
        case VTSS_APPL_MEP_RATE_10S:     return(VTSS_MEP_SUPP_RATE_10S);
        case VTSS_APPL_MEP_RATE_1S:      return(VTSS_MEP_SUPP_RATE_1S);
        case VTSS_APPL_MEP_RATE_6M:      return(VTSS_MEP_SUPP_RATE_6M);
        case VTSS_APPL_MEP_RATE_1M:      return(VTSS_MEP_SUPP_RATE_1M);
        case VTSS_APPL_MEP_RATE_6H:      return(VTSS_MEP_SUPP_RATE_6H);
        default:                         return(VTSS_MEP_SUPP_RATE_10S);
    }
}


static vtss_mep_supp_pattern_t supp_pattern_calc(vtss_appl_mep_pattern_t pattern)
{
    switch (pattern)
    {
        case VTSS_APPL_MEP_PATTERN_ALL_ZERO:    return(VTSS_MEP_SUPP_PATTERN_ALL_ZERO);
        case VTSS_APPL_MEP_PATTERN_ALL_ONE:     return(VTSS_MEP_SUPP_PATTERN_ALL_ONE);
        case VTSS_APPL_MEP_PATTERN_0XAA:        return(VTSS_MEP_SUPP_PATTERN_0XAA);
        default:                                return(VTSS_MEP_SUPP_PATTERN_ALL_ZERO);
    }
}


static vtss_mep_supp_calcway_t supp_calcway_calc(vtss_appl_mep_dm_calcway_t calcway)
{
    switch (calcway)
    {
        case VTSS_APPL_MEP_RDTRP:   return(VTSS_MEP_SUPP_RDTRIP);
        case VTSS_APPL_MEP_FLOW:    return(VTSS_MEP_SUPP_FLOW);
        default:                    return(VTSS_MEP_SUPP_FLOW);
    }
}


static vtss_mep_supp_tunit_t supp_tunit_calc(vtss_appl_mep_dm_tunit_t tunit)
{
    switch (tunit)
    {
        case VTSS_APPL_MEP_US:   return(VTSS_MEP_SUPP_US);
        case VTSS_APPL_MEP_NS:   return(VTSS_MEP_SUPP_NS);
        default:                 return(VTSS_MEP_SUPP_US);
    }
}


static vtss_mep_supp_aps_type_t supp_aps_calc(vtss_appl_mep_aps_type_t aps_type)
{
    switch (aps_type)
    {
        case VTSS_APPL_MEP_L_APS:    return(VTSS_MEP_SUPP_L_APS);
        case VTSS_APPL_MEP_R_APS:    return(VTSS_MEP_SUPP_R_APS);
        default:                     return(VTSS_MEP_SUPP_L_APS);
    }
}


static vtss_appl_mep_relay_act_t relay_action_calc(vtss_mep_supp_relay_act_t relay_action)
{
    switch (relay_action)
    {
        case VTSS_MEP_SUPP_RELAY_UNKNOWN:   return(VTSS_APPL_MEP_RELAY_UNKNOWN);
        case VTSS_MEP_SUPP_RELAY_HIT:       return(VTSS_APPL_MEP_RELAY_HIT);
        case VTSS_MEP_SUPP_RELAY_FDB:       return(VTSS_APPL_MEP_RELAY_FDB);
        case VTSS_MEP_SUPP_RELAY_MPDB:      return(VTSS_APPL_MEP_RELAY_MPDB);
    }
    return(VTSS_APPL_MEP_RELAY_UNKNOWN);
}


static vtss_mep_supp_oam_count supp_oam_count_calc(vtss_appl_mep_oam_count rate)
{
    switch (rate)
    {
        case VTSS_APPL_MEP_OAM_COUNT_NONE:   return(VTSS_MEP_SUPP_OAM_COUNT_NONE);
        case VTSS_APPL_MEP_OAM_COUNT_ALL:    return(VTSS_MEP_SUPP_OAM_COUNT_ALL);
        case VTSS_APPL_MEP_OAM_COUNT_Y1731:  return(VTSS_MEP_SUPP_OAM_COUNT_Y1731);
        default:                             return(VTSS_MEP_SUPP_OAM_COUNT_NONE);
    }
}


static void lst_action(mep_instance_data_t *data)
{
    BOOL ne_disable = FALSE;
    BOOL fe_disable = FALSE;

    if (data->lst_config.enable) {  /* Link State Tracking is enabled */
        /* Near-end disable if local TSF is detected and stable. */
        ne_disable = (data->out_state.state.mep.aTsf && !data->lst_state.sf_timer);
        /* far-end disable if IS down received - without ignore timer running to avoid "deadlock" */
        fe_disable = (data->supp_state.is_received && (data->supp_state.is_tlv.value == 2) && !data->lst_state.ign_timer);

        /* In case of LOS on residence port and the port is not disabled - far-end disable is not allowed to avoid "deadlock" */
        if (vtss_mep_port_los_get(data->config.port) && !data->lst_state.fe_disable)
            fe_disable = FALSE;
//printf("lst_action  ne_disable %u  fe_disable %u  state_disable %u  los %u  aTsf %u  is_received %u  is_value %u  ignore_timer %u  sf_timer %u\n", ne_disable, fe_disable, data->lst_state.fe_disable, vtss_mep_port_los_get(data->config.port), data->out_state.state.mep.aTsf, data->supp_state.is_received, data->supp_state.is_tlv.value, data->lst_state.ign_timer, data->lst_state.sf_timer);
    }

    /* Enable or disable the port */
    data->lst_state.fe_disable = fe_disable;

    // The call to vtss_mep_port_conf_disable() may result in the port module
    // calling us back, where we attempt to take our lock again, which we can't
    // unless we exit it now.
    vtss_mep_crit_unlock();
    vtss_mep_port_conf_disable(data->instance, data->config.port, ne_disable || fe_disable);
    vtss_mep_crit_lock();
}


static void consequent_action_calc_and_deliver(mep_instance_data_t *data)
{
    u32  rc = VTSS_MEP_SUPP_RC_OK, i;
    BOOL old_aTsf, old_aTsd, old_aBlk, dLoc=FALSE;

    old_aTsf = data->out_state.state.mep.aTsf;
    old_aTsd = data->out_state.state.mep.aTsd;
    old_aBlk = data->out_state.state.mep.aBlk;

    for (i=0; i<data->config.peer_count; ++i)
        dLoc = dLoc || data->supp_state.defect_state.dLoc[i];






    data->out_state.state.mep.aTsf = ((dLoc && data->cc_config.enable) || (data->supp_state.defect_state.dLck && !data->cc_config.enable) || (data->supp_state.defect_state.dAis && !data->cc_config.enable) ||
                                     data->supp_state.defect_state.dLevel || data->supp_state.defect_state.dMeg || data->supp_state.defect_state.dMep || data->ssf_state);

    data->out_state.state.mep.aTsd = data->dDeg && !data->out_state.state.mep.aTsf;

    data->out_state.state.mep.aBlk = data->supp_state.defect_state.dLevel || data->supp_state.defect_state.dMeg || data->supp_state.defect_state.dMep;

    if (old_aTsd != data->out_state.state.mep.aTsd) { /* aTsd has changed */
        data->event_flags |= EVENT_OUT_SF_SD;   /* Deliver SF and SD out */
    }

    if (old_aTsf != data->out_state.state.mep.aTsf)
    {/* aTsf has changed */
        data->event_flags |= EVENT_OUT_SF_SD;   /* Deliver SF and SD out */

        rc = vtss_mep_supp_ccm_rdi_set(data->instance,   data->out_state.state.mep.aTsf);   /* Control RDI */

        if (data->ais_config.enable)    rc += vtss_mep_supp_ais_set(data->instance, data->out_state.state.mep.aTsf);  /* Control AIS */

        if (data->lst_config.enable) {
            if (data->out_state.state.mep.aTsf) {   /* Active aTsf */
                data->lst_state.sf_timer = 200/timer_res;     /* Wait to see if aTsf is stable active */
                vtss_mep_timer_start();
            } else {
                data->lst_state.sf_timer = 0;
            }
            lst_action(data);
        }
    }

    if (old_aBlk != data->out_state.state.mep.aBlk)
    {/* aBlk has changed */
    }

    if (rc != VTSS_MEP_SUPP_RC_OK)       vtss_mep_trace("consequent_action_calc_and_deliver: CCM RDI set failed", data->instance, 0, 0, 0);

    data->event_flags |= EVENT_OUT_STATE;
}


static void fault_cause_calc(mep_instance_data_t *data)
{
    u32 i;

    data->out_state.state.mep.cLevel = data->supp_state.defect_state.dLevel;
    data->out_state.state.mep.cMeg = data->supp_state.defect_state.dMeg;
    data->out_state.state.mep.cMep = data->supp_state.defect_state.dMep;

    for (i=0; i<mep_caps.mesa_oam_peer_cnt; ++i)
    {
        data->out_state.state.peer_mep[i].cPeriod = data->supp_state.defect_state.dPeriod[i];
        data->out_state.state.peer_mep[i].cPrio = data->supp_state.defect_state.dPrio[i];





        data->out_state.state.peer_mep[i].cRdi = data->supp_state.defect_state.dRdi[i] && data->cc_config.enable;
        data->out_state.state.peer_mep[i].cLoc = (data->supp_state.defect_state.dLoc[i] && !data->supp_state.defect_state.dAis && !data->supp_state.defect_state.dLck && !data->ssf_state &&
                                         data->cc_config.enable);

    }

    data->out_state.state.mep.cSsf    = data->ssf_state || data->supp_state.defect_state.dAis;
    data->out_state.state.mep.cAis    = data->supp_state.defect_state.dAis;
    data->out_state.state.mep.cLck    = data->supp_state.defect_state.dLck && !data->supp_state.defect_state.dAis;
    data->out_state.state.mep.cLoop   = data->supp_state.defect_state.dLoop;
    data->out_state.state.mep.cConfig = data->supp_state.defect_state.dConfig;
    data->out_state.state.mep.cDeg    = data->dDeg && !data->supp_state.defect_state.dAis && !data->supp_state.defect_state.dLck && !data->ssf_state;

    data->event_flags |= EVENT_OUT_STATE;
}

static void syslog_timer_start(mep_instance_data_t *data, u32 peer_mep)
{
    u32     idx;

    for (idx = 0; idx < mep_caps.mesa_oam_peer_cnt; ++idx) {
        if (data->syslog_state.peer_mep_timer[idx] == 0) {
            data->syslog_state.peer_mep_timer[idx] = MEP_DEFAULT_SYSLOG_TIMEOUT;
            data->syslog_state.transaction_id[idx] = peer_mep;
            break;
        }
    }

    if (data->syslog_state.timer == 0) {
        data->syslog_state.timer = MEP_DEFAULT_SYSLOG_TIMEOUT;
    }
}

static void syslog_timer_stop(mep_instance_data_t *data, u32 peer_mep)
{
    u32     idx;

    for (idx = 0; idx < mep_caps.mesa_oam_peer_cnt; ++idx) {
        if (data->syslog_state.transaction_id[idx] == peer_mep) {
            data->syslog_state.peer_mep_timer[idx] = 0;
            data->syslog_state.transaction_id[idx] = 0;
            break;
        }
    }
}


static void syslog_new_peer_mep(mep_instance_data_t *data,  const mep_conf_t *conf)
{
    u32  i, j, peer_mep;

    /* check the control flag of syslog for CFM is enabled or not */
    if (data->syslog_config.enable == FALSE) {
        vtss_mep_trace("syslog_new_peer_mep syslog not enabled for instance", data->instance, 0, 0, 0);
        return;
    }

    /* Find any added peer MEP */
    for (i=0; i<conf->peer_count; ++i) {
        peer_mep = conf->peer_mep[i];
        for (j=0; j<data->config.peer_count; ++j) {
            if (peer_mep == data->config.peer_mep[i])
                break;
        }
        if (j == data->config.peer_count) {
            vtss_mep_trace("Added peer MEP found", data->instance, peer_mep, 0, 0);
            syslog_timer_start( data, peer_mep);
        }
    }

    /* Find any deleted peer MEP */
    for (i=0; i<data->config.peer_count; ++i) {
        peer_mep = data->config.peer_mep[i];
        for (j=0; j<conf->peer_count; ++j) {
            if (peer_mep == conf->peer_mep[i])
                break;
        }
        if (j == conf->peer_count) {
            vtss_mep_trace("Deleted peer MEP found", data->instance, peer_mep, 0, 0);
            syslog_timer_stop( data, peer_mep);
        }
    }
}


static void syslog_new_defect_state(mep_instance_data_t *data,  vtss_mep_supp_defect_state_t *defect_state)
{
    /* Calculate entries in sys_log based on changes in defect state */

    u32                     i, self_mep, peer_mep;
    BOOL                    all_cleared;
    BOOL                    loc_changed;

    /* check the control flag of syslog for CFM is enabled or not */
    if (data->syslog_config.enable == FALSE) {
        vtss_mep_trace("syslog_new_defect_state syslog not enabled for instance", data->instance, 0, 0, 0);
        return;
    }

    self_mep = data->instance+1;

    loc_changed = FALSE;
    for (i=0; i<data->config.peer_count; ++i) {
        if (data->supp_state.defect_state.dLoc[i] != defect_state->dLoc[i]) {
            loc_changed = TRUE;
            peer_mep = data->config.peer_mep[i];
            vtss_mep_trace("LOC state changed", data->instance, peer_mep, defect_state->dLoc[i], 0);
            /* syslog for CFM, REMOTE_MEP_DOWN, REMOTE_MEP_UP */
            if (defect_state->dLoc[i]) {
                vtss_mep_syslog(VTSS_MEP_REMOTE_MEP_DOWN, self_mep, peer_mep);
                syslog_timer_stop( data, peer_mep);
            } else {
                vtss_mep_syslog(VTSS_MEP_REMOTE_MEP_UP, self_mep, peer_mep);
            }
        }
    }

    if (loc_changed) {
        all_cleared = TRUE;
        for (i=0; i<data->config.peer_count; ++i) {
            if (defect_state->dLoc[i]) {
                all_cleared = FALSE;
                break;
            }
        }
        if (all_cleared) {
            /* syslog for CFM, VTSS_APPL_MEP_CROSSCHECK_SERVICE_UP */
            vtss_mep_syslog(VTSS_MEP_CROSSCHECK_SERVICE_UP, self_mep, 0);
        }
    }

    if (data->supp_state.defect_state.dAis != defect_state->dAis) {
        /* syslog for CFM, ENTER_AIS, EXIT_AIS */
        if (defect_state->dAis) {
            vtss_mep_syslog(VTSS_MEP_ENTER_AIS, self_mep, 0);
        } else {
            vtss_mep_syslog(VTSS_MEP_EXIT_AIS, self_mep, 0);
        }
    }

    if (data->supp_state.defect_state.dMeg != defect_state->dMeg) {
        /* syslog for CFM, CROSS_CONNECTED_SERVICE */
        if (defect_state->dMeg) {
            vtss_mep_syslog(VTSS_MEP_CROSS_CONNECTED_SERVICE, self_mep, 0);
        }
    }

    if (data->supp_state.defect_state.dMep != defect_state->dMep) {
        /* syslog for CFM, CROSSCHECK_MEP_UNKNOWN */
        if (defect_state->dMep) {
            vtss_mep_syslog(VTSS_MEP_CROSSCHECK_MEP_UNKNOWN, self_mep, 0);
        }
    }

    if (data->supp_state.defect_state.dLoop != defect_state->dLoop) {
        /* syslog for CFM, FORWARDING_LOOP */
        if (defect_state->dLoop) {
            vtss_mep_syslog(VTSS_MEP_FORWARDING_LOOP, self_mep, 0);
        }
    }

    if (data->supp_state.defect_state.dConfig != defect_state->dConfig) {
        /* syslog for CFM, CONFIG_ERROR */
        if (defect_state->dConfig) {
            vtss_mep_syslog(VTSS_MEP_CONFIG_ERROR, self_mep, 0);
        }
    }
}


static vtss_mep_supp_mode_t supp_mode_calc(vtss_appl_mep_mode_t  mode)
{
    switch (mode)
    {
        case VTSS_APPL_MEP_MEP: return(VTSS_MEP_SUPP_MEP);
        case VTSS_APPL_MEP_MIP: return(VTSS_MEP_SUPP_MIP);
        default:                return(VTSS_MEP_SUPP_MEP);
    }
}

static vtss_mep_supp_direction_t supp_direction_calc(vtss_appl_mep_direction_t  direction)
{
    switch (direction)
    {
        case VTSS_APPL_MEP_DOWN: return(VTSS_MEP_SUPP_DOWN);
        case VTSS_APPL_MEP_UP:   return(VTSS_MEP_SUPP_UP);
        default:                 return(VTSS_MEP_SUPP_DOWN);
    }
}

static vtss_mep_supp_format_t supp_format_calc(vtss_appl_mep_format_t  format)
{
    switch (format)
    {
        case VTSS_APPL_MEP_ITU_ICC:    return(VTSS_MEP_SUPP_ITU_ICC);
        case VTSS_APPL_MEP_IEEE_STR:   return(VTSS_MEP_SUPP_IEEE_STR);
        case VTSS_APPL_MEP_ITU_CC_ICC: return(VTSS_MEP_SUPP_ITU_CC_ICC);
        default:                       return(VTSS_MEP_SUPP_ITU_ICC);
    }
}

static vtss_appl_mep_mode_t mode_calc(vtss_mep_supp_mode_t  mode)
{
    switch (mode)
    {
        case VTSS_MEP_SUPP_MEP: return(VTSS_APPL_MEP_MEP);
        case VTSS_MEP_SUPP_MIP: return(VTSS_APPL_MEP_MIP);
        default:                return(VTSS_APPL_MEP_MEP);
    }
}

static vtss_appl_mep_direction_t direction_calc(vtss_mep_supp_direction_t  direction)
{
    switch (direction)
    {
        case VTSS_MEP_SUPP_DOWN: return(VTSS_APPL_MEP_DOWN);
        case VTSS_MEP_SUPP_UP:   return(VTSS_APPL_MEP_UP);
        default:                 return(VTSS_APPL_MEP_DOWN);
    }
}

#define outer_tag_type(ptype)  ((ptype==VTSS_MEP_TAG_S) ? VTSS_MEP_SUPP_FLOW_MASK_SOUTVID : (ptype==VTSS_MEP_TAG_S_CUSTOM) ? VTSS_MEP_SUPP_FLOW_MASK_CUOUTVID : VTSS_MEP_SUPP_FLOW_MASK_COUTVID)

static BOOL flow_info_calc(mep_instance_data_t      *data,
                           vtss_mep_supp_flow_t     *const info,
                           mesa_port_list_t         &mep_port)
{
    u32   pvid, pport, vport=0;
    vtss_mep_tag_type_t ptype;
    mesa_port_list_t nni;

    memset(info, 0, sizeof(vtss_mep_supp_flow_t));
    data->rule_ports.clear_all();
    mep_port.clear_all();
    data->evc_type = VTSS_MEP_SUPP_NONE;
    data->rule_vid = 0;

    if (MPLS_DOMAIN(data->config.domain)) {
        info->port[data->config.port] = TRUE;
        mep_port[data->config.port] = TRUE;
        return TRUE;
    } else if (data->config.domain == VTSS_APPL_MEP_PORT) {
        if (data->config.direction == VTSS_APPL_MEP_DOWN)
        {
            info->port[data->config.port] = TRUE;
            vport = data->config.port;
            if (vtss_mep_vlan_get(data->config.vid, nni))    memcpy(data->rule_ports, nni, data->rule_ports.array_size()); /* If there is a VLAN on this VID all NNI must be a rule_port */
            else                                             data->rule_ports[data->config.port] = TRUE;
            mep_port[data->config.port] = TRUE;
        }
        if (data->config.direction == VTSS_APPL_MEP_UP)
        {
            if (!vtss_mep_vlan_get(data->config.vid, nni))    return (FALSE);
            memcpy(info->port, nni, info->port.array_size());
            info->port[data->config.port] = FALSE;      /* This is UP MEP - All but residence port is a flow port */
            memcpy(data->rule_ports, nni, data->rule_ports.array_size());
            for (vport=0; vport<mep_caps.mesa_port_cnt; ++vport)  if (info->port[vport])   break;    /* Assume that all NNI ports has same VLAN configuration */
            if (vport == mep_caps.mesa_port_cnt)
            {
                vtss_mep_trace("flow_info_calc: No nni ports found", 0, 0, 0, 0);
                return (FALSE); /* Did not find the port*/
            }
            mep_port = info->port;
        }

        if (!vtss_mep_pvid_get(vport, &pvid, &ptype))    return (FALSE);

        if (data->config.vid != 0)        info->mask = VTSS_MEP_SUPP_FLOW_MASK_PORT | outer_tag_type(ptype);
        else                              info->mask = VTSS_MEP_SUPP_FLOW_MASK_PORT;
        info->out_vid = data->config.vid;

        if (data->config.vid != 0)        data->rule_vid = data->config.vid;    /* This is a tagged port MEP - VID is the classified VID */
        else                              data->rule_vid = pvid;                /* This is a un tagged port MEP - VID is the PVID */

        info->vid = data->rule_vid;    /* Classified VID */
    } else if (data->config.domain == VTSS_APPL_MEP_VLAN)
    {
        if (data->config.direction == VTSS_APPL_MEP_DOWN)
        {
            if (vtss_mep_vlan_get(data->config.flow, nni))    memcpy(data->rule_ports, nni, data->rule_ports.array_size()); /* If there is a VLAN on this VID all NNI must be a rule_port */
            else                                             data->rule_ports[data->config.port] = TRUE;

            info->port[data->config.port] = TRUE;                   /* This is Down MEP - the residence port is a flow port */

            if (vtss_mep_protection_port_get(data->config.port, &pport))
                info->port[pport] = TRUE;                          /* The flow port array now contains the 'other' port if the residence port is part of a port protection */

            vport = data->config.port;
            mep_port[data->config.port] = TRUE;
        }
        if (data->config.direction == VTSS_APPL_MEP_UP)
        {
            if (!vtss_mep_vlan_get(data->config.flow, nni))    return (FALSE);
            memcpy(data->rule_ports, nni, data->rule_ports.array_size()); /* VLAN NNI must be a rule_port */

            memcpy(info->port, nni, info->port.array_size());
            info->port[data->config.port] = FALSE;                  /* This is Up MEP - All but residence port is a flow port */
            for (vport=0; vport<mep_caps.mesa_port_cnt; ++vport)  if (info->port[vport])   break;    /* Assume that all NNI ports has same VLAN configuration */
            if (vport == mep_caps.mesa_port_cnt)
            {
                vtss_mep_trace("flow_info_calc: No nni ports found", 0, 0, 0, 0);
                return (FALSE); /* Did not find the port*/
            }
            mep_port = info->port;
        }

        if (!vtss_mep_pvid_get(vport, &pvid, &ptype))    return (FALSE);

        data->rule_vid = data->config.flow;

        info->mask = VTSS_MEP_SUPP_FLOW_MASK_PORT | VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID | outer_tag_type(ptype);
        info->out_vid = data->config.flow;
        info->vid = data->config.flow;    /* Classified VID */
    }












































































































    return (data->rule_vid != 0);
}


static BOOL client_flow_info_calc(mep_instance_data_t      *data,
                                  u32                      client_flow_count,
                                  u32                      client_flows[VTSS_APPL_MEP_CLIENT_FLOWS_MAX],
                                  vtss_mep_supp_flow_t     *const info,
                                  u32                      *count)
{
    u32   vid;
    vtss_mep_tag_type_t ptype;






    mesa_port_list_t nni;
    *count = 0;

    if (!vtss_mep_pvid_get(data->config.port, &vid, &ptype))    return (FALSE);

    if (data->config.domain == VTSS_APPL_MEP_PORT ||
        MPLS_DOMAIN(data->config.domain)) {






























    }
















    if (data->config.domain == VTSS_APPL_MEP_VLAN)
    {   /* Get flow info from VLAN */
        if (vtss_mep_vlan_get(data->config.flow, nni))
        {
            memcpy(info[0].port, nni, info[0].port.array_size());
            info[0].vid = data->config.flow;
            info[0].mask = VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID | VTSS_MEP_SUPP_FLOW_MASK_PORT | outer_tag_type(ptype);
            info[0].out_vid = data->config.flow;
            *count = 1;
        }
    }

    return (TRUE);
}

u32 highest_client_prio_calc(mep_instance_data_t *data, u32 client)
{
    u32                   prio=7;




































    return (prio);
}

static void peer_mac_update(mep_instance_data_t *data)
{
    u32 i;
    u8  peer_mac[mep_caps.mesa_oam_peer_cnt][VTSS_MEP_SUPP_MAC_LENGTH];

    if (vtss_mep_supp_learned_mac_get(data->instance, peer_mac) != VTSS_MEP_SUPP_RC_OK)     {vtss_mep_trace("peer_mac_update: Call to supp failed", data->instance, 0, 0, 0); return;};

    for (i=0; i<data->config.peer_count; ++i)
        if (memcmp(all_zero_mac, peer_mac[i], VTSS_APPL_MEP_MAC_LENGTH)) {
        /* Not all zero mac - use this learned MAC for configuration */
            memcpy(data->config.peer_mac[i].addr, peer_mac[i], VTSS_APPL_MEP_MAC_LENGTH);
        }
}


static void ccm_conf_calc(mep_instance_data_t *data,   vtss_mep_supp_ccm_conf_t *ccm_conf)
{
    mesa_packet_port_info_t   info;
    CapArray<mesa_packet_port_filter_t, MESA_CAP_PORT_CNT> filter;

    memset(ccm_conf, 0, sizeof(*ccm_conf));

    ccm_conf->rate = supp_rate_calc((data->cc_config.enable) ? data->cc_config.rate : data->lm_config.rate);
    ccm_conf->prio = (data->cc_config.enable) ? data->cc_config.prio : data->lm_config.prio;
    ccm_conf->dei = FALSE;
    ccm_conf->format = supp_format_calc(data->config.format);
    memcpy(ccm_conf->name, data->config.name, VTSS_MEP_SUPP_MEG_CODE_LENGTH);
    memcpy(ccm_conf->meg, data->config.meg, VTSS_MEP_SUPP_MEG_CODE_LENGTH);
    ccm_conf->mep = data->config.mep;
    ccm_conf->peer_count = data->config.peer_count;
    memcpy(ccm_conf->peer_mep, data->config.peer_mep, sizeof(*ccm_conf->peer_mep) * mep_caps.mesa_oam_peer_cnt);
    if (!data->lm_config.synthetic && data->lm_config.enable && (data->lm_config.ended == VTSS_APPL_MEP_DUAL_ENDED) && (data->lm_config.cast == VTSS_APPL_MEP_UNICAST))
        memcpy(ccm_conf->dmac, data->config.peer_mac[0].addr, VTSS_MEP_SUPP_MAC_LENGTH);   /* UNICAST dual ended LM is enabled */
    else
        memcpy(ccm_conf->dmac, class1_multicast[data->config.level], VTSS_MEP_SUPP_MAC_LENGTH);
    ccm_conf->tlv_enable = data->cc_config.tlv_enable;
    if (ccm_conf->tlv_enable) { /* TLV is enabled - calculate TLV content */
        ccm_conf->ps_tlv.value = 2; /* Default Port Status is 'psUp' */
        if (data->config.direction == VTSS_APPL_MEP_DOWN) {    /* This is a Down-MEP - Port status TLV must be added */
            if (mesa_packet_port_info_init(&info) != VTSS_RC_OK)
                return;
            info.vid = data->rule_vid;
            if (mesa_packet_port_filter_get(NULL, &info, filter.size(), filter.data()) != VTSS_RC_OK) /* Get the filter info for this VID */
                return;
            ccm_conf->ps_tlv.value = (filter[data->config.port].filter != MESA_PACKET_FILTER_DISCARD) ? 2 : 1;  /* Port Status is 'psUp' if not discarding */
            data->ps_tlv_value = ccm_conf->ps_tlv.value;  /* Save the currently transmitted PS TLV value */
        }
        ccm_conf->is_tlv.value = 1; /* Default Interface Status is 'isUp' */
        if ((data->config.direction == VTSS_APPL_MEP_UP) && (vtss_mep_port_los_get(data->config.port))) {   /* This is a Up-MEP - the residence port state is used */
            ccm_conf->is_tlv.value = 2; /* LOS on Up-MEP residence port - Interface Status is 'isDown'*/
        }
        memcpy(ccm_conf->os_tlv.oui, os_tlv_config.oui, sizeof(ccm_conf->os_tlv.oui));
        ccm_conf->os_tlv.subtype = os_tlv_config.subtype;
        ccm_conf->os_tlv.value = os_tlv_config.value;
    }
}


static void lt_conf_calc(vtss_mep_supp_ltm_conf_t *conf,  mep_instance_data_t *data)
{
    u32  i;

    conf->enable = data->lt_config.enable;
    conf->transaction_id = data->lt_state.transaction_id;
    conf->dei = FALSE;
    conf->prio = data->lt_config.prio;
    conf->ttl = data->lt_config.ttl;
    memcpy(conf->dmac, class1_multicast[data->config.level], sizeof(conf->dmac));
    conf->dmac[5] |= 0x08;  /* This makes it a class2 multicast address */

    if (!memcmp(all_zero_mac, data->lt_config.mac.addr, sizeof(all_zero_mac)))
    {/* All zero mac - mep must be a known peer mep */
        peer_mac_update(data);
        for (i=0; i<data->config.peer_count; ++i)
            if (data->lt_config.mep == data->config.peer_mep[i]) break;
        if (i == data->config.peer_count)                       return;
        memcpy(conf->tmac, data->config.peer_mac[i].addr, sizeof(conf->tmac));
    }
    else
        memcpy(conf->tmac, data->lt_config.mac.addr, sizeof(conf->tmac));
}


static BOOL test_enable_calc(const vtss_appl_mep_test_prio_conf_t   tests[VTSS_APPL_MEP_PRIO_MAX][VTSS_APPL_MEP_TEST_MAX])
{
    u32 i, j;

    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {  /* Calculate if any TST transmission is active */
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            if (tests[i][j].enable) {
                return TRUE;
            }
        }
    }
    return FALSE;
}

static BOOL dm_enable_calc(const vtss_appl_mep_dm_prio_conf_t   dm[VTSS_APPL_MEP_PRIO_MAX])
{
    u32 i;

    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {  /* Calculate if any DM transmission is active */
        if (dm[i].enable) {
            return TRUE;
        }
    }
    return FALSE;
}

static u32 max_size_calc(const mep_instance_data_t   *data, u32 max_size)
{
    return ( ((data->config.domain == VTSS_APPL_MEP_PORT) && (data->config.vid != 0)) ? max_size - 4 :
             ((data->config.domain == VTSS_APPL_MEP_EVC) && (data->config.vid != 0)) ? max_size - 8 :
             ((data->config.domain == VTSS_APPL_MEP_EVC) && (data->config.vid == 0)) ? max_size - 4 :
             (data->config.domain == VTSS_APPL_MEP_VLAN) ? max_size - 4 : max_size
           );
}

static void push_delay_data(delay_state_t *delay_state, u32 lastn, u32 in_de, u32 in_de_var, u32 *out_de, u32 *out_del_var)
{
    *out_de      = delay_state->delay[delay_state->next_prt];
    *out_del_var = delay_state->delay_var[delay_state->next_prt];

    delay_state->delay[delay_state->next_prt] = in_de;
    delay_state->delay_var[delay_state->next_prt] = in_de_var;

    delay_state->next_prt = (delay_state->next_prt + 1) % lastn;
}


static BOOL peer_mep_is_local(mep_instance_data_t *data,  u32 mep_id)
{
    u32                 i;
    mep_instance_data_t *idata;

    if (data->config.direction == VTSS_APPL_MEP_DOWN) {
        return FALSE;
    }
    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i) {
        idata = &instance_data[i];
        if ((idata != data) && (idata->config.domain == data->config.domain) && (idata->config.flow == data->config.flow) &&
            (idata->config.direction == data->config.direction) && (idata->config.level == data->config.level) && (idata->config.mep == mep_id)) {
            return TRUE;
        }
    }
    return FALSE;
}

static void run_tlv_change(u32 instance)
{
    mep_instance_data_t         *data;
    vtss_mep_supp_ccm_conf_t    ccm_conf;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_TLV_CHANGE;

    if (data->config.enable && data->cc_config.enable) {
        ccm_conf_calc(data, &ccm_conf);
        if (vtss_mep_supp_ccm_conf_set(instance, &ccm_conf) != VTSS_MEP_SUPP_RC_OK)
            vtss_mep_trace("run_tlv_change: Call to supp failed", instance, 0, 0, 0);
    }
}

static void clear_lm_avail_fifo(u32 instance)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)
        return;

    mep_instance_data_t *data = &instance_data[instance];

    if (data->lm_avail_fifo) {
        VTSS_FREE(data->lm_avail_fifo);
        data->lm_avail_fifo = NULL;
    }

    data->lm_avail_fifo_ix = 0;
}

static u32 run_config(u32 instance)
{
    mep_instance_data_t         *data;
    vtss_mep_supp_conf_t        conf;
    vtss_mep_supp_ccm_conf_t    ccm_conf;
    mesa_port_list_t            mep_port;
    u32                         rc=VTSS_MEP_SUPP_RC_OK;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_CONFIG;
    memset(&conf, 0, sizeof(conf));

    if (data->config.enable)
    {
        if (!flow_info_calc(data, &conf.flow, mep_port)) {     /* Flow information is calculated */
            vtss_mep_trace("run_config flow_info_calc() failed", instance, 0, 0, 0);
            return(VTSS_MEP_RC_INTERNAL);
        }
        vtss_mep_port_register(instance, mep_port);

        if (!data->cc_config.enable)      data->cc_config.rate = VTSS_APPL_MEP_RATE_1S;

        conf.enable = data->config.enable;
        conf.voe = data->config.voe;
        if (!MPLS_DOMAIN(data->config.domain)) {
            if (!memcmp(all_zero_mac, data->config.mac.addr, sizeof(all_zero_mac)))   /* Check if the configured MAC is all zero */
                vtss_mep_mac_get(data->config.port, data->config.mac.addr);  /* Get MAC of residence port if all zero mac */
        }
        memcpy(conf.mac, data->config.mac.addr, sizeof(conf.mac)); /* Get the configured MAC */
        conf.level = data->config.level;
        conf.out_of_service = data->config.out_of_service;
        conf.sat = data->sat;
        conf.evc_type = data->evc_type;
        memcpy(conf.sat_conf, data->sat_prio_conf, sizeof(conf.sat_conf));
        conf.port = data->config.port;
        conf.mode = supp_mode_calc(data->config.mode);
        conf.direction = supp_direction_calc(data->config.direction);
        conf.mep = data->config.mep;
        conf.domain = data->config.domain;
        conf.flow_num = data->config.flow;
        vtss_mep_crit_unlock(); /* The below function is changed to execute synchroniously and is doing a callout that again call in and requests this lock */
        if ((rc = vtss_mep_supp_conf_set(instance, &conf, NULL)) != VTSS_MEP_SUPP_RC_OK)     vtss_mep_trace("run_config: Call to supp failed", instance, 0, 0, 0);
        vtss_mep_crit_lock();
        if (rc != VTSS_MEP_SUPP_RC_OK)  return(VTSS_MEP_RC_INTERNAL);

        ccm_conf_calc(data,   &ccm_conf);
        if (vtss_mep_supp_ccm_conf_set(instance, &ccm_conf) != VTSS_MEP_SUPP_RC_OK)     {vtss_mep_trace("run_config: Call to supp failed", instance, 0, 0, 0); return VTSS_MEP_RC_INTERNAL;}

        if (data->cc_config.enable)      run_cc_config(instance);
        if (data->lm_config.enable)      run_lm_config(instance);
        if (data->aps_config.enable)     (void)run_aps_config(instance);
        if (data->ais_config.enable)     run_ais_config(instance);
        if (data->lck_config.enable)     run_lck_config(instance);



        data->event_flags |= EVENT_OUT_SIGNAL;
        data->event_flags |= EVENT_OUT_SF_SD;

        if (!MPLS_DOMAIN(data->config.domain)) {
            if (!pdu_fwr_cap_update(data->rule_vid, data->rule_ports)) {
                vtss_mep_trace("run_config pdu_fwr_cap_update() failed", instance, 0, 0, 0);
                vtss_mep_rule_update_failed(data->rule_vid);
                return VTSS_MEP_RC_TCAM_CONFIGURATION;
            }
        }

        if (mep_caps.mesa_phy_ts) {
#if MEP_DM_TS_MCAST
            vtss_mep_phy_config_mac(data->config.port, 1, 1, data->config.mac.addr);
            vtss_mep_phy_config_mac(data->config.port, 1, 0, data->config.mac.addr);
            vtss_mep_phy_config_mac(data->config.port, 1, 1, class1_multicast[data->config.level]);
            phy_ts_dump(data->config.port);
#endif
        }
        vtss_mep_timer_start();
    }
    else
    {
        conf.enable = FALSE;

        data->lst_config.enable = FALSE;    /* Link state tracking must be disabled when MEP is deleted */
        lst_action(data);

        if (mep_caps.mesa_phy_ts) {
#if MEP_DM_TS_MCAST
            vtss_mep_phy_config_mac(data->config.port, 0, 1, data->config.mac.addr);
            vtss_mep_phy_config_mac(data->config.port, 0, 0, data->config.mac.addr);
            vtss_mep_phy_config_mac(data->config.port, 0, 1, class1_multicast[data->config.level]);
            phy_ts_dump(data->config.port);
#endif
        }
        vtss_mep_crit_unlock(); /* The below function is changed to execute synchroniously and is doing a callout that again call in and requests this lock */
        if (vtss_mep_supp_conf_set(instance, &conf, NULL) != VTSS_MEP_SUPP_RC_OK) {
            vtss_mep_trace("run_config: Call to supp failed", instance, 0, 0, 0);
            return(VTSS_MEP_RC_INTERNAL);
        }
        vtss_mep_crit_lock();
        if (data->lm_avail_fifo) {
            VTSS_FREE(data->lm_avail_fifo);
            data->lm_avail_fifo = NULL;
            data->lm_avail_fifo_ix = 0;
        }

        for (int i = 0; i < data->config.peer_count; ++i) {
            vtss_mep_lm_hli_status_set(instance, data->config.peer_mep[i], NULL);
        }

        instance_data_clear(instance, data);
        vtss_mep_lm_notif_status_set(instance, NULL);
    }
    data->event_flags |= EVENT_OUT_STATE;

    return VTSS_MEP_RC_OK;
}


static void run_cc_config(u32 instance)
{
    u32                         rc = VTSS_MEP_SUPP_RC_OK;
    mep_instance_data_t         *data;
    vtss_mep_supp_ccm_conf_t    ccm_conf;
    vtss_mep_supp_gen_conf_t    gen_conf;

    data = &instance_data[instance];    /* Instance data reference */
    memset(&gen_conf, 0, sizeof(gen_conf));

    data->event_flags &= ~EVENT_IN_CC_CONFIG;

    if (data->cc_config.enable)
    {
        /* CCM is configured */
        if (data->lm_config.ended == VTSS_APPL_MEP_DUAL_ENDED)
            data->lm_config.prio = data->cc_config.prio;        /* CC and dual ended LM priority is set to the same */
        peer_mac_update(data);
        ccm_conf_calc(data, &ccm_conf);
        rc += vtss_mep_supp_ccm_conf_set(instance, &ccm_conf);
    }

    /* CCM Generator is configured */
    gen_conf.enable = (data->cc_config.enable || (!data->lm_config.synthetic && data->lm_config.enable && (data->lm_config.ended == VTSS_APPL_MEP_DUAL_ENDED)));
    gen_conf.lm_enable = (!data->lm_config.synthetic && data->lm_config.enable && (data->lm_config.ended == VTSS_APPL_MEP_DUAL_ENDED));
    gen_conf.lm_rate = supp_rate_calc((data->lm_config.rate));
    gen_conf.cc_rate = supp_rate_calc((data->cc_config.enable) ? data->cc_config.rate : data->lm_config.rate);
    rc += vtss_mep_supp_ccm_generator_set(instance, &gen_conf);

    if (rc != VTSS_MEP_SUPP_RC_OK)       vtss_mep_trace("run_cc_config: Call to supp failed", instance, 0, 0, 0);
}

static vtss_mep_supp_clear_dir mep_supp_clear_dir_get(vtss_appl_mep_clear_dir direction)
{
    return direction == VTSS_APPL_MEP_CLEAR_DIR_BOTH ? VTSS_MEP_SUPP_CLEAR_DIR_BOTH :
        direction == VTSS_APPL_MEP_CLEAR_DIR_TX ? VTSS_MEP_SUPP_CLEAR_DIR_TX : VTSS_MEP_SUPP_CLEAR_DIR_RX;
}

static void run_lm_config(u32 instance)
{
    u32                         rc = VTSS_MEP_SUPP_RC_OK;
    mep_instance_data_t         *data;
    vtss_mep_supp_ccm_conf_t    ccm_conf;
    vtss_mep_supp_slm_conf_t    slm_conf;
    vtss_mep_supp_lmm_conf_t    lmm_conf;
    vtss_mep_supp_gen_conf_t    gen_conf;
    u32                         peer_idx;

    data = &instance_data[instance];    /* Instance data reference */
    memset(&lmm_conf, 0, sizeof(lmm_conf));
    memset(&slm_conf, 0, sizeof(slm_conf));
    memset(&gen_conf, 0, sizeof(gen_conf));

    data->event_flags &= ~EVENT_IN_LM_CONFIG;

    vtss_mep_lm_trace("run_lm_config  instance  enable  synthetic", instance, data->lm_config.enable, data->lm_config.synthetic, 0);

    if (data->lm_state.clear_counters) {
        // Clear counters if instructed to do so (typically only when we enable TX or RX))
        memset(data->out_state.lm_state, 0, sizeof(data->out_state.lm_state));
        memset(data->lm_peer_state, 0, sizeof(data->lm_peer_state));

        T_I("instance %u, direction %u", instance, data->lm_state.clear_dir);

        (void)vtss_mep_supp_lm_counters_clear(instance, mep_supp_clear_dir_get(data->lm_state.clear_dir));
    }

    if (data->lm_config.enable) {
        /* Enable LM TX */
        if (!data->lm_config.synthetic && (data->lm_config.ended == VTSS_APPL_MEP_DUAL_ENDED)) {
            /* LM based on SW generated CCM session */
            /* CCM is configured */
            data->cc_config.prio = data->lm_config.prio;    /* CC and dual ended LM priority is set to the same */
            ccm_conf_calc(data, &ccm_conf);
            rc += vtss_mep_supp_ccm_conf_set(instance, &ccm_conf);
        }
    }
    else {
        /* LM TX is disabled - we do nothing */
    }

    rc += vtss_mep_supp_lm_flow_count_set(instance,  data->lm_config.flow_count);
    rc += vtss_mep_supp_lm_oam_count_set(instance,  supp_oam_count_calc(data->lm_config.oam_count));

    /* LM based on LMM */
    lmm_conf.enable = (!data->lm_config.synthetic && data->lm_config.enable && (data->lm_config.ended == VTSS_APPL_MEP_SINGEL_ENDED));
    lmm_conf.enable_rx = (!data->lm_config.synthetic && data->lm_config.enable_rx);
    lmm_conf.prio = data->lm_config.prio;
    lmm_conf.dei = FALSE;
    lmm_conf.rate = supp_rate_calc(data->lm_config.rate);
    lmm_conf.ended = data->lm_config.ended == VTSS_APPL_MEP_SINGEL_ENDED ? VTSS_MEP_SUPP_SINGLE_ENDED : VTSS_MEP_SUPP_DUAL_ENDED;
    peer_mac_update(data);
    if (data->lm_config.cast == VTSS_APPL_MEP_UNICAST)    memcpy(lmm_conf.dmac, data->config.peer_mac[0].addr, VTSS_APPL_MEP_MAC_LENGTH);
    else                                                  memcpy(lmm_conf.dmac, class1_multicast[data->config.level], VTSS_APPL_MEP_MAC_LENGTH);
    rc += vtss_mep_supp_lmm_conf_set(instance, &lmm_conf);

    /* LM based on SLM */
    for (peer_idx=0; peer_idx<data->config.peer_count; ++peer_idx)
        if (data->lm_config.mep == data->config.peer_mep[peer_idx]) break;
    if (peer_idx == data->config.peer_count)
        peer_idx = 0;
    slm_conf.enable = (data->lm_config.synthetic && data->lm_config.enable);
    slm_conf.enable_rx = (data->lm_config.synthetic && data->lm_config.enable_rx);
    slm_conf.prio = data->lm_config.prio;
    slm_conf.dei = FALSE;
    slm_conf.rate = supp_rate_calc(data->lm_config.rate);
    slm_conf.meas_interval = supp_meas_interval_calc(data->lm_config.rate, data->lm_config.meas_interval);
    slm_conf.size = data->lm_config.size;
    slm_conf.test_id = data->lm_config.slm_test_id;
    slm_conf.ended = data->lm_config.ended == VTSS_APPL_MEP_SINGEL_ENDED ? VTSS_MEP_SUPP_SINGLE_ENDED : VTSS_MEP_SUPP_DUAL_ENDED;
    peer_mac_update(data);
    if (data->lm_config.cast == VTSS_APPL_MEP_UNICAST)    memcpy(slm_conf.dmac, data->config.peer_mac[peer_idx].addr, VTSS_APPL_MEP_MAC_LENGTH);
    else                                                  memcpy(slm_conf.dmac, class1_multicast[data->config.level], VTSS_APPL_MEP_MAC_LENGTH);
    rc += vtss_mep_supp_slm_conf_set(instance, &slm_conf);

    /* CCM Generator is configurated */
    gen_conf.enable = (data->cc_config.enable || (!data->lm_config.synthetic && data->lm_config.enable && (data->lm_config.ended == VTSS_APPL_MEP_DUAL_ENDED)));
    gen_conf.lm_enable = (!data->lm_config.synthetic && data->lm_config.enable && (data->lm_config.ended == VTSS_APPL_MEP_DUAL_ENDED));
    gen_conf.lm_rate = supp_rate_calc(data->lm_config.rate);
    gen_conf.cc_rate = supp_rate_calc((data->cc_config.enable) ? data->cc_config.rate : data->lm_config.rate);
    rc += vtss_mep_supp_ccm_generator_set(instance, &gen_conf);

    if (rc != VTSS_MEP_SUPP_RC_OK)       vtss_mep_trace("run_lm_config: Call to supp failed", instance, 0, 0, 0);
}


static void run_dm_config(u32 instance)
{
    u32                         rc = VTSS_MEP_SUPP_RC_OK, i;
    BOOL                        enable;
    mep_instance_data_t         *data;
    vtss_mep_supp_dmm_conf_t    conf, old_conf;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_DM_CONFIG;

    memset(&old_conf, 0, sizeof(old_conf));

    if (data->dm_config.common.ended == VTSS_APPL_MEP_SINGEL_ENDED) {
        if (vtss_mep_supp_dmm_conf_get(instance, &old_conf) != VTSS_MEP_SUPP_RC_OK) {
            vtss_mep_trace("run_dm_config: Call to supp failed", instance, 0, 0, 0);
            return;
        }
    }
    if (data->dm_config.common.ended == VTSS_APPL_MEP_DUAL_ENDED) {
        if (vtss_mep_supp_dm1_conf_get(instance, (vtss_mep_supp_dm1_conf_t *)&old_conf) != VTSS_MEP_SUPP_RC_OK) {
            vtss_mep_trace("run_dm_config: Call to supp failed", instance, 0, 0, 0);
            return;
        }
    }

    memset(&conf, 0, sizeof(conf));     /* This is initialized to DISABLE */
    conf.dei = FALSE;
    conf.interval = data->dm_config.common.interval;
    conf.proprietary = data->dm_config.common.proprietary;
    conf.calcway = supp_calcway_calc(data->dm_config.common.calcway);
    conf.tunit = supp_tunit_calc(data->dm_config.common.tunit);
    conf.synchronized = data->dm_config.common.synchronized;

    enable = dm_enable_calc(data->dm_config.dm);

    if (!enable || (data->dm_config.common.ended == VTSS_APPL_MEP_SINGEL_ENDED)) { /* Disable one-way DM also if two-way is enabled */
        rc = vtss_mep_supp_dm1_conf_set(instance, (vtss_mep_supp_dm1_conf_t *)&conf);
    }
    if (!enable || (data->dm_config.common.ended == VTSS_APPL_MEP_DUAL_ENDED)) { /* Disable two-way DM also if one-way is enabled */
        rc = vtss_mep_supp_dmm_conf_set(instance, &conf);
    }

    vtss_mep_trace("run_dm_config  instance  enable  tunit", instance, enable, data->dm_config.common.tunit, 0);

    if (enable) {
        for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
            if (data->dm_config.dm[i].enable) {     /* Transmission of DM is enabled */
                conf.enable[i] = TRUE;
                if (old_conf.enable[i])      continue;   /* DM on this prio is already enabled */

                /* Initialize state on newly enabled prio */
                memset(&data->supp_state.tw_state[i], 0, sizeof(data->supp_state.tw_state[i]));
                memset(&data->supp_state.fn_state[i], 0, sizeof(data->supp_state.fn_state[i]));
                memset(&data->supp_state.nf_state[i], 0, sizeof(data->supp_state.nf_state[i]));

                memset(&data->out_state.tw_state[i], 0, sizeof(data->out_state.tw_state[i]));
                memset(&data->out_state.fn_state[i], 0, sizeof(data->out_state.fn_state[i]));
                memset(&data->out_state.nf_state[i], 0, sizeof(data->out_state.nf_state[i]));

                data->out_state.tw_state[i].min_delay = 0xffffffff;
                data->out_state.tw_state[i].min_delay_var = 0xffffffff;
                data->out_state.fn_state[i].min_delay = 0xffffffff;
                data->out_state.fn_state[i].min_delay_var = 0xffffffff;
                data->out_state.nf_state[i].min_delay = 0xffffffff;
                data->out_state.nf_state[i].min_delay_var = 0xffffffff;
            }

            /* set DM state time unit */
            data->out_state.tw_state[i].tunit = data->dm_config.common.tunit;
            data->out_state.fn_state[i].tunit = data->dm_config.common.tunit;
            data->out_state.nf_state[i].tunit = data->dm_config.common.tunit;
        }

        if (data->dm_config.common.cast == VTSS_APPL_MEP_UNICAST) {
            peer_mac_update(data);
            for (i=0; i<data->config.peer_count; ++i)
                if (data->dm_config.common.mep == data->config.peer_mep[i]) break;
            if (i == data->config.peer_count)                       return;
            memcpy(conf.dmac, data->config.peer_mac[i].addr, VTSS_MEP_SUPP_MAC_LENGTH);
        } else {
            memcpy(conf.dmac, class1_multicast[data->config.level], VTSS_APPL_MEP_MAC_LENGTH);
        }

        if (enable && (data->dm_config.common.ended == VTSS_APPL_MEP_DUAL_ENDED)) { /* Enable one-way DM */
            rc = vtss_mep_supp_dm1_conf_set(instance, (vtss_mep_supp_dm1_conf_t *)&conf);
        }
        if (enable && (data->dm_config.common.ended == VTSS_APPL_MEP_SINGEL_ENDED)) { /* Enable two-way DM */
            rc = vtss_mep_supp_dmm_conf_set(instance, &conf);
        }
    }

    if (rc != VTSS_MEP_SUPP_RC_OK)       vtss_mep_trace("run_dm_config: Call to supp failed", instance, 0, 0, 0);
}


static u32 run_aps_config(u32 instance)
{
    u32                       rc = VTSS_MEP_SUPP_RC_OK;
    mep_instance_data_t       *data;
    vtss_mep_supp_aps_conf_t  conf;

    data = &instance_data[instance];    /* Instance data reference */
    memset(&conf, 0, sizeof(conf));

    data->event_flags &= ~EVENT_IN_APS_CONFIG;

    conf.enable = data->aps_config.enable;
    conf.prio = data->aps_config.prio;
    conf.dei = FALSE;
    conf.type = supp_aps_calc(data->aps_config.type);
    if (data->aps_config.type == VTSS_APPL_MEP_L_APS)
    {
        peer_mac_update(data);
        if (data->aps_config.cast == VTSS_APPL_MEP_UNICAST)    memcpy(conf.dmac, data->config.peer_mac[0].addr, VTSS_APPL_MEP_MAC_LENGTH);
        else                                                   memcpy(conf.dmac, class1_multicast[data->config.level], VTSS_APPL_MEP_MAC_LENGTH);
    }
    else
    {
        raps_mac[5] = data->aps_config.raps_octet;
        memcpy(conf.dmac, raps_mac, VTSS_APPL_MEP_MAC_LENGTH);
    }
    if ((rc = raps_forwarding_control((data->config.domain == VTSS_APPL_MEP_PORT) ? data->config.vid : data->config.flow)) != VTSS_MEP_RC_OK)
        return rc;

    if (vtss_mep_supp_aps_conf_set(instance, &conf) != VTSS_MEP_SUPP_RC_OK) {
        vtss_mep_trace("run_aps_config: Call to supp failed", instance, 0, 0, 0);
        return VTSS_MEP_RC_INTERNAL;
    }

    /* No PDU has been received yet */
    memset(data->out_state.rx_aps, 0, VTSS_MEP_APS_DATA_LENGTH);
    instance_data[instance].event_flags |= EVENT_OUT_APS;
    return VTSS_MEP_RC_OK;
}


static void run_lt_config(u32 instance)
{
    u32                       rc = VTSS_MEP_SUPP_RC_OK;
    mep_instance_data_t       *data;
    vtss_mep_supp_ltm_conf_t  conf;

    data = &instance_data[instance];    /* Instance data reference */
    memset(&conf, 0, sizeof(conf));

    data->event_flags &= ~EVENT_IN_LT_CONFIG;

    if (data->lt_config.enable)
    {
        data->lt_state.transaction_id++;
        data->lt_state.timer = 5000/timer_res;
        vtss_mep_timer_start();

        memset(&data->out_state.lt_state, 0, sizeof(vtss_appl_mep_lt_state_t));
        data->out_state.lt_state.transaction[0].transaction_id = data->lt_state.transaction_id;

        lt_conf_calc(&conf,  data);
    }
    else
    {
        conf.enable = data->lt_config.enable;
        data->lt_state.timer = 0;
    }

    rc = vtss_mep_supp_ltm_conf_set(instance, &conf);

    if (rc != VTSS_MEP_SUPP_RC_OK)       vtss_mep_trace("run_lt_config: Call to supp failed", instance, 0, 0, 0);
}


static u32 run_lb_config(u32 instance, u64 *active_time_ms)
{
    u32                        i, j, rc = VTSS_MEP_SUPP_RC_OK;
    mep_instance_data_t        *data;
    vtss_mep_supp_lbm_conf_t   conf;
    vtss_mep_supp_lb_status_t  status;
    BOOL                       enable;

    data = &instance_data[instance];    /* Instance data reference */
    memset(&conf, 0, sizeof(conf));

    enable = FALSE;
    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            if (data->lb_config.tests[i][j].enable) {     /* Transmission of LBM is enabled */
                enable = TRUE;
                conf.tests[i][j].enable = data->lb_config.tests[i][j].enable;
                conf.tests[i][j].dp = (data->lb_config.tests[i][j].dp == VTSS_APPL_MEP_DP_GREEN) ? 0 : 1;
                conf.tests[i][j].pattern = supp_pattern_calc(data->lb_config.tests[i][j].pattern);
                conf.tests[i][j].size = data->lb_config.tests[i][j].size;
                conf.tests[i][j].ttl = data->lb_config.tests[i][j].ttl;
                conf.tests[i][j].rate = data->lb_config.tests[i][j].rate;
            }
        }
    }

    if (enable)
    {
        if (data->lb_config.common.to_send != VTSS_APPL_MEP_LB_TO_SEND_INFINITE) { /* Only start the time if this is not infinite LBM transmission */
            data->lb_state.timer = ((data->lb_config.common.to_send*((data->lb_config.common.interval*10)+5))+(1000*5))/timer_res; /* Adding 5ms. to the gap to compensate for the sw handling time. Adding 5s. that is the wait time to collect all LBR */
            vtss_mep_timer_start();
        }

        memset(&data->out_state.lb_state, 0, sizeof(vtss_appl_mep_lb_state_t));
        if (vtss_mep_supp_lb_status_get(instance, &status) == VTSS_MEP_SUPP_RC_OK) {
            for (i=0; i<mep_caps.mesa_oam_peer_cnt; ++i)
                data->lb_state.rx_transaction_id[i] = status.trans_id;
        }

        conf.to_send = data->lb_config.common.to_send;
        conf.interval = data->lb_config.common.interval;

        if (data->lb_config.common.cast == VTSS_APPL_MEP_UNICAST && !MPLS_DOMAIN(data->config.domain))
        {
            if (!memcmp(all_zero_mac, data->lb_config.common.mac.addr, VTSS_APPL_MEP_MAC_LENGTH))
            {/* All zero mac - mep must be a known peer mep */
                peer_mac_update(data);
                for (i=0; i<data->config.peer_count; ++i)
                    if (data->lb_config.common.mep == data->config.peer_mep[i]) break;
                if (i == data->config.peer_count)                       return (VTSS_MEP_RC_PEER_ID);
                memcpy(conf.dmac, data->config.peer_mac[i].addr, VTSS_MEP_SUPP_MAC_LENGTH);
            }
            else
                memcpy(conf.dmac, data->lb_config.common.mac.addr, VTSS_MEP_SUPP_MAC_LENGTH);
        }
        else
            memcpy(conf.dmac, class1_multicast[data->config.level], VTSS_MEP_SUPP_MAC_LENGTH);
    }
    else
    {
        data->lb_state.timer = 0;
    }

    rc = vtss_mep_supp_lbm_conf_set(instance, &conf, active_time_ms);

    if (rc != VTSS_MEP_SUPP_RC_OK) {
        vtss_mep_trace("run_lb_config: Call to supp failed", instance, 0, 0, 0);
        return ((rc != VTSS_MEP_RC_OUT_OF_TX_RESOURCE) ? VTSS_MEP_RC_INTERNAL : rc);
    } else {
        return VTSS_MEP_RC_OK;
    }
}

static void run_ais_config(u32 instance)
{
    u32                       i, rc = VTSS_MEP_SUPP_RC_OK;
    mep_instance_data_t       *data;
    vtss_mep_supp_ais_conf_t  conf;

    data = &instance_data[instance];    /* Instance data reference */
    memset(&conf, 0, sizeof(conf));

    data->event_flags &= ~EVENT_IN_AIS_CONFIG;

    /* copy data from data->ais_config into conf and let supp know about the same */
    if (data->ais_config.enable)
    {
        conf.rate = supp_rate_calc(data->ais_config.rate);
        conf.dei = FALSE;
        conf.protection = data->ais_config.protection;
        memcpy(conf.dmac, class1_multicast[data->config.level], VTSS_MEP_SUPP_MAC_LENGTH);
        for (i=0; i<data->client_config.flow_count; ++i) {
            conf.prio[i] = data->client_config.ais_prio[i] == VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST ? highest_client_prio_calc(data, i) : data->client_config.ais_prio[i];
            conf.level[i] = data->client_config.level[i];
        }
        if (!client_flow_info_calc(data, data->client_config.flow_count, data->client_config.flows, conf.flows, &conf.flow_count))      return;
        if (conf.flow_count) {
            for (i=0; i<conf.flow_count; ++i) {
                if (data->client_config.domain[i] == VTSS_APPL_MEP_MPLS_LSP)
                    continue;
                if (data->config.direction == VTSS_APPL_MEP_DOWN)   /* Down MEP AIS is not transmitted on residence port */
                    conf.flows[i].port[data->config.port] = FALSE;
                if (data->config.direction == VTSS_APPL_MEP_UP) {   /* Up MEP AIS is only transmitted on residence port */
                    memset(conf.flows[i].port, FALSE, sizeof(conf.flows[i].port));
                    conf.flows[i].port[data->config.port] = TRUE;
                }
            }
            rc = vtss_mep_supp_ais_conf_set(instance, &conf);
            rc += vtss_mep_supp_ais_set(instance, data->out_state.state.mep.aTsf);  /* Control AIS */
        }
    }
    else
        rc += vtss_mep_supp_ais_set(instance, FALSE);

    if (rc != VTSS_MEP_SUPP_RC_OK)       vtss_mep_trace("run_ais_config: Call to supp failed", instance, 0, 0, 0);
}


static void run_lck_config(u32 instance)
{
    u32                       rc = VTSS_MEP_SUPP_RC_OK, i;
    mep_instance_data_t       *data;
    vtss_mep_supp_lck_conf_t  conf;

    data = &instance_data[instance];    /* Instance data reference */
    memset(&conf, 0, sizeof(conf));

    data->event_flags &= ~EVENT_IN_LCK_CONFIG;

    /* copy data from data->lck_config onto conf and let supp know about the same */
    if (data->lck_config.enable)
    {
        conf.enable = data->lck_config.enable;
        conf.rate = supp_rate_calc(data->lck_config.rate);
        conf.dei = FALSE;
        memcpy(conf.dmac, class1_multicast[data->config.level], VTSS_MEP_SUPP_MAC_LENGTH);
        for (i=0; i<data->client_config.flow_count; ++i) {
            conf.prio[i] = data->client_config.lck_prio[i];
            conf.prio[i] = data->client_config.lck_prio[i] == VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST ? highest_client_prio_calc(data, i) : data->client_config.lck_prio[i];
            conf.level[i] = data->client_config.level[i];
        }
        if (!client_flow_info_calc(data, data->client_config.flow_count, data->client_config.flows, conf.flows, &conf.flow_count))      return;
    }
    else
        conf.enable = data->lck_config.enable;

    if (!conf.enable || conf.flow_count) {  /* In case of 'enable' there must be a flow_count */
        rc = vtss_mep_supp_lck_conf_set(instance, &conf);
    }

    if (rc != VTSS_MEP_SUPP_RC_OK)       vtss_mep_trace("run_lck_config: Call to supp failed", instance, 0, 0, 0);
}

static u32 run_tst_config(u32 instance, u64 *active_time_ms)
{
    u32                       i, j, rc=VTSS_MEP_SUPP_RC_OK;
    mep_instance_data_t       *data;
    vtss_mep_supp_tst_conf_t  conf;
    BOOL                      enable;

    data = &instance_data[instance];    /* Instance data reference */
    memset(&conf, 0, sizeof(conf));

    enable = FALSE;
    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            if (data->tst_config.tests[i][j].enable) {     /* Transmission of TST is enabled */
                enable = TRUE;
                conf.tests[i][j].enable = data->tst_config.tests[i][j].enable;
                conf.tests[i][j].dp = (data->tst_config.tests[i][j].dp == VTSS_APPL_MEP_DP_GREEN) ? 0 : 1;
                conf.tests[i][j].pattern = supp_pattern_calc(data->tst_config.tests[i][j].pattern);
                conf.tests[i][j].size = data->tst_config.tests[i][j].size;
                conf.tests[i][j].rate = data->tst_config.tests[i][j].rate;
            }
        }
    }

    if (enable) {   /* TST is enabled - configure the common part */
        conf.sequence = data->tst_config.common.sequence;
        conf.transaction_id = 0;

        peer_mac_update(data);
        for (i=0; i<data->config.peer_count; ++i)
            if (data->tst_config.common.mep == data->config.peer_mep[i]) break;
        if (i == data->config.peer_count)                     return (VTSS_MEP_RC_PEER_ID);
        memcpy(conf.dmac, data->config.peer_mac[i].addr, VTSS_MEP_SUPP_MAC_LENGTH);
    }

    if (data->config.domain == VTSS_APPL_MEP_PORT) { /* Preamble and IFG is included in port domain */
        conf.line_rate = TRUE;
    }
    conf.enable_rx = data->tst_config.common.enable_rx;

    if (!data->tst_state.active && (enable || conf.enable_rx)) {    /* Start of new measurement - clear state and counters */
        memset(&data->tst_state, 0, sizeof(data->tst_state));
        memset(&data->out_state.tst_state, 0, sizeof(data->out_state.tst_state));

        vtss_mep_acl_tst_clear(instance);
        (void)vtss_mep_supp_tst_clear(instance);

        data->tst_state.timer = 1000/timer_res;     /* Start the TST timer only to make 'Test time' counting */
        vtss_mep_timer_start();

        data->tst_state.active = TRUE;
    }
    if (!(enable || conf.enable_rx)) {     /* Both TX and RX is disabled - stop timer */
        data->tst_state.timer = 0;
        data->tst_state.active = FALSE;
    }

    rc = vtss_mep_supp_tst_conf_set(instance, &conf, active_time_ms);

    if (rc != VTSS_MEP_SUPP_RC_OK) {
        vtss_mep_trace("run_tst_config: Call to supp failed", instance, 0, 0, 0);
        return ((rc != VTSS_MEP_RC_OUT_OF_TX_RESOURCE) ? VTSS_MEP_RC_INTERNAL : rc);
    } else {
        return VTSS_MEP_RC_OK;
    }
}

static void run_tst_done_timer(u32 instance)
{
    mep_instance_data_t         *data;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_TST_DONE_TIMER;
}

static void run_lb_done_timer(u32 instance)
{
    mep_instance_data_t         *data;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_LB_DONE_TIMER;
}





































static void run_aps_info(u32 instance)
{
    instance_data[instance].event_flags &= ~EVENT_IN_APS_INFO;

    /* Deliver APS to EPS */
    instance_data[instance].event_flags |= EVENT_OUT_APS;
}


static void run_defect_state(u32 instance)
{
    mep_instance_data_t           *data;
    vtss_mep_supp_defect_state_t  defect_state;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_DEFECT_STATE;

    if (vtss_mep_supp_defect_state_get(instance,  &defect_state) != VTSS_MEP_SUPP_RC_OK)     vtss_mep_trace("run_ccm_state: Call to supp failed", instance, 0, 0, 0);

    syslog_new_defect_state(data, &defect_state);   /* Calculate entries in sys_log based on changes in defect state */

    data->supp_state.defect_state = defect_state;

    fault_cause_calc(data);     /* Calculate new fault cause state */

    consequent_action_calc_and_deliver(data);       /* Calculate aTsf (SF) and aTsd (SD) and deliver to EPS */
}


static void run_ssf_ssd_state(u32 instance)
{
    mep_instance_data_t *data;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_SSF_SSD_STATE;

    fault_cause_calc(data);

    consequent_action_calc_and_deliver(data);       /* Calculate aTsf (SF) and aTsd (SD) and deliver to EPS */
}

static void run_ltr_new(u32 instance)
{
    mep_instance_data_t             *data;
    vtss_mep_supp_ltr_t             ltr[VTSS_MEP_SUPP_LTR_MAX];
    vtss_appl_mep_lt_transaction_t  *transaction;
    vtss_appl_mep_lt_reply_t        *reply;
    u32 i, ltr_cnt;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_LTR_NEW;

    if (vtss_mep_supp_ltr_get(instance, &ltr_cnt, ltr) != VTSS_MEP_SUPP_RC_OK)      return;
    if (!data->lt_config.enable)                                                    return;

    transaction = &data->out_state.lt_state.transaction[data->out_state.lt_state.transaction_cnt];

    for (i=0; i<ltr_cnt; ++i)
    {/* Walk through the new received replies */
        if ((data->lt_state.transaction_id == ltr[i].transaction_id) && (transaction->reply_cnt < VTSS_APPL_MEP_REPLY_MAX))
        {   /* LT is enabled and LTR has the active transaction ID and there is still room for a reply */
            reply = &transaction->reply[transaction->reply_cnt];

            reply->ttl = ltr[i].ttl;
            reply->mode = mode_calc(ltr[i].mode);
            reply->direction = direction_calc(ltr[i].direction);
            reply->forwarded = ltr[i].forwarded;
            reply->relay_action = relay_action_calc(ltr[i].relay_action);
            memcpy(reply->last_egress_mac.addr, ltr[i].last_egress_mac, VTSS_APPL_MEP_MAC_LENGTH);
            memcpy(reply->next_egress_mac.addr, ltr[i].next_egress_mac, VTSS_APPL_MEP_MAC_LENGTH);

            transaction->reply_cnt++;
        }
    }
}


static void run_lbr_new(u32 instance)
{
    mep_instance_data_t  *data;
    vtss_mep_supp_lbr_t  lbr[VTSS_MEP_SUPP_LBR_MAX];
    u32                  i, j, lbr_cnt;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_LBR_NEW;

    if (vtss_mep_supp_lbr_get(instance, &lbr_cnt, lbr) != VTSS_MEP_SUPP_RC_OK)      return;
    if (!test_enable_calc(data->lb_config.tests))                                   return;

    for (j=0; j<lbr_cnt; ++j)
    {/* Walk through the new received replies */
        for (i=0; i<data->out_state.lb_state.reply_cnt; ++i) /* Check if this MAC is already received */
            if (MPLS_DOMAIN(data->config.domain)) {
                if (!memcmp(lbr[j].u.mep_mip_id, data->out_state.lb_state.reply[i].mep_mip_id, 16))                 break;
            } else if (!memcmp(lbr[j].u.mac, data->out_state.lb_state.reply[i].mac.addr, VTSS_APPL_MEP_MAC_LENGTH)) break;
        if (i < mep_caps.mesa_oam_peer_cnt)
        {/* Old reply found or there is still room for more new replies */
            if (i == data->out_state.lb_state.reply_cnt)
            {/* New reply received */
                data->out_state.lb_state.reply_cnt++;
                if (MPLS_DOMAIN(data->config.domain)) {
                    memcpy(data->out_state.lb_state.reply[i].mep_mip_id, lbr[j].u.mep_mip_id, 16);
                } else {
                    memcpy(data->out_state.lb_state.reply[i].mac.addr, lbr[j].u.mac, VTSS_APPL_MEP_MAC_LENGTH);
                }
            }
            data->out_state.lb_state.reply[i].lbr_received++;
            if (lbr[j].transaction_id != data->lb_state.rx_transaction_id[i])       data->out_state.lb_state.reply[i].out_of_order++;
            data->lb_state.rx_transaction_id[i] = lbr[j].transaction_id + 1;  /* Next expected Transaction ID is this received - plus one */
        }
    }
}


static void run_tst_new(u32 instance)
{
    mep_instance_data_t  *data;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_TST_NEW;

    if (MPLS_DOMAIN(data->config.domain)) {
        data->tst_state.rx_count_active = TRUE;
        return;
    }

#if !defined(VTSS_SW_OPTION_ACL)
    mesa_vid_mac_t         vid_mac;
    mesa_mac_table_entry_t entry;

    vid_mac.vid = data->rule_vid;
    vid_mac.mac = data->config.mac;
    if (mesa_mac_table_get(NULL, &vid_mac, &entry) == VTSS_RC_OK) {
        entry.copy_to_cpu = 0;
        if (mesa_mac_table_add(NULL, &entry) != VTSS_RC_OK) vtss_mep_trace("run_tst_new: Call to mesa_mac_table_add", instance, 0, 0, 0);
    }
#endif

    if (test_enable_calc(data->tst_config.tests) || data->tst_config.common.enable_rx) {    /* Either tx or rx is enabled */
        data->tst_state.rx_count_active = TRUE;
        data->tst_state.timer = 1000/timer_res;
        vtss_mep_timer_start();
    }
}

static u32 run_etree_elan_new(u32 instance)
{
    mep_instance_data_t *data;
    mep_instance_data_t *idata;
    BOOL                second = FALSE;
    u32                 i, j;

    data = &instance_data[instance];
    data->event_flags &= ~EVENT_IN_ETREE_NEW;

    if (!data->config.enable || !VTSS_MEP_SUPP_ETREE_ELAN(data->evc_type))
        return VTSS_MEP_RC_INTERNAL;

    if (mep_caps.mesa_mep_serval) {
        for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)  {/* Search for all MEPs in this E-TREE */
            idata = &instance_data[i];

            if (idata->config.enable && VTSS_MEP_SUPP_ETREE_ELAN(idata->evc_type) && (idata->rule_vid == data->rule_vid) && (idata->leaf_vid == data->leaf_vid)) {   /* A MEP in this E-TREE */
                vtss_mep_acl_etree_elan_del(i);   /* E-TREEE and E-LAN ACL rules (IS2) for this instance is deleted */

                for (j=0; j<mep_caps.mesa_port_cnt; ++j) {
                    if (data->rule_ports[j]) {
                        if (!vtss_mep_acl_etree_elan_add(i, vtss_mep_supp_rx_isdx_get(instance, j), j, &data->rule_ports, base_mep_pag, second))   return VTSS_MEP_RC_TCAM_CONFIGURATION;
                    }
                }
                second = TRUE;
            }
        }
    }
    return VTSS_MEP_RC_OK;
}

static void delay_data_calc(u32 instance,  delay_state_t *delay_state,  vtss_appl_mep_dm_state_t *out_state,  u32 *delay,  u32 count)
{
    vtss_appl_mep_dm_common_conf_t *dm_config;
    u32                            i, out_delay, bucket;
    u32                            delay_var, out_delay_var, newcount;
    u32                            n_count, ov_cnt;

    dm_config = &instance_data[instance].dm_config.common;

    if ((dm_config->overflow_act == VTSS_APPL_MEP_DISABLE) && (out_state->ovrflw_cnt != 0)) {   /* The overflow action is not to clear everything, keep the counter */
        return;
    }

    for (i=0; (i<count && i<VTSS_MEP_SUPP_DM_MAX); ++i) { /* For any received delay do the delay data calculation */
        if (delay_state->sum > (delay_state->sum + delay[i])) {   /* This is indicating overflow of the total delay sum of this measuring period */
            if (dm_config->overflow_act == VTSS_APPL_MEP_DISABLE) {   /* The overflow action is not to clear everything, keep the counter */
                out_state->ovrflw_cnt = 1;
                return;
            }

            /* overflow, reset anything */
            ov_cnt = out_state->ovrflw_cnt + 1;

            memset(delay_state, 0, sizeof(*delay_state));
            memset(out_state, 0, sizeof(*out_state));
            out_state->min_delay = 0xffffffff;
            out_state->min_delay_var = 0xffffffff;
            out_state->ovrflw_cnt = ov_cnt;
        }

        if (delay_state->delay_cnt == 0) {
            delay_var = 0; /* the first one */
        } else {
            delay_var = ((delay[i] >= delay_state->last_delay)) ? (delay[i] - delay_state->last_delay) : (delay_state->last_delay - delay[i]);
        }

        push_delay_data(delay_state, dm_config->lastn, delay[i], delay_var, &out_delay, &out_delay_var);

        delay_state->delay_cnt += 1;
        newcount = delay_state->delay_cnt;

        delay_state->sum = delay_state->sum + delay[i];
        delay_state->var_sum = delay_state->var_sum + delay_var;
        out_state->avg_delay = delay_state->sum / newcount;        /* Calculate the total average delay */
        if (newcount > 1)
            out_state->avg_delay_var = delay_state->var_sum / (newcount-1);     /* Calculate the total average delay veriation - the first received delay does not add to dm1_var_sum_near_to_far */

        delay_state->n_sum = delay_state->n_sum + delay[i] - out_delay;
        delay_state->n_var_sum = delay_state->n_var_sum + delay_var - out_delay_var;
        n_count = (delay_state->delay_cnt >= dm_config->lastn) ? dm_config->lastn : newcount;
        if (n_count)  out_state->avg_n_delay = delay_state->n_sum / n_count;            /* Calculate the Last N average delay */
        if (n_count)  out_state->avg_n_delay_var = delay_state->n_var_sum / n_count;    /* Calculate the Last N average delay variation */

        if (delay[i] < out_state->min_delay)
            out_state->min_delay = delay[i];

        if (delay[i] > out_state->max_delay)
            out_state->max_delay = delay[i];

        /* Processing delay measurement bins for FD */
        if (dm_config->m_threshold > 0) {
            bucket = (delay[i] / dm_config->m_threshold);
            if (bucket >= dm_config->num_of_bin_fd) {
                bucket = dm_config->num_of_bin_fd - 1;
                out_state->fd_bin[bucket]++;
            } else {
                out_state->fd_bin[bucket]++;
            }
        } else {
            vtss_mep_trace("delay_data_calc: m threshold isn't correct", instance, dm_config->m_threshold, 0, 0);
        }

        if (newcount >= 2) { /* Start min and max register after three measurements - at least two frames must be received in order to measure min/max delay variation */
            if (delay_var < out_state->min_delay_var)
                out_state->min_delay_var = delay_var;

            if (delay_var > out_state->max_delay_var)
                out_state->max_delay_var = delay_var;
        }

        /* Processing delay measurement bins for IFDV */
        if (dm_config->m_threshold > 0) {
            bucket = (delay_var / dm_config->m_threshold);
            if (bucket >= dm_config->num_of_bin_ifdv) {
                bucket = dm_config->num_of_bin_ifdv - 1;
                out_state->ifdv_bin[bucket]++;
            } else {
                out_state->ifdv_bin[bucket]++;
            }
        } else {
            vtss_mep_trace("delay_data_calc: m threshold isn't correct", instance, dm_config->m_threshold, 0, 0);
        }

        delay_state->last_delay = delay[i];
    }
}

static void run_dmr_new(u32 instance)
{
    mep_instance_data_t      *data;
    vtss_mep_supp_dmr_status status;
    u32                      i;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_DMR_NEW;

    if (!data->config.enable)  return;

    if (vtss_mep_supp_dmr_get(instance, &status) != VTSS_MEP_SUPP_RC_OK)      return;

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) { /* For any priority do the delay and counter calculation */
        if (status.prio_status[i].rx_counter) {
            delay_data_calc(instance, &data->supp_state.tw_state[i], &data->out_state.tw_state[i], status.prio_status[i].tw_delays, status.prio_status[i].rx_counter);

            if (data->dm_config.common.synchronized) { /* Near end and far end are synchronized */
                delay_data_calc(instance, &data->supp_state.fn_state[i], &data->out_state.fn_state[i], status.prio_status[i].fn_delays, status.prio_status[i].rx_counter);
                delay_data_calc(instance, &data->supp_state.nf_state[i], &data->out_state.nf_state[i], status.prio_status[i].nf_delays, status.prio_status[i].rx_counter);
                data->out_state.fn_state[i].rx_err_cnt += status.prio_status[i].fn_rx_err_counter;
                data->out_state.nf_state[i].rx_err_cnt += status.prio_status[i].nf_rx_err_counter;
            }
        }

        data->out_state.tw_state[i].tx_cnt += status.prio_status[i].tx_counter;
        data->out_state.tw_state[i].rx_cnt += status.prio_status[i].rx_counter;
        data->out_state.tw_state[i].rx_tout_cnt += status.prio_status[i].rx_tout_counter;
        data->out_state.tw_state[i].rx_err_cnt += status.prio_status[i].tw_rx_err_counter;
    }
}

static void run_dm1_new(u32 instance)
{
    vtss_mep_supp_dm1_status status;
    u32                      i;
    mep_instance_data_t      *data;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_1DM_NEW;

    if (vtss_mep_supp_dm1_get(instance, &status) != VTSS_MEP_SUPP_RC_OK)      return;

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) { /* For any priority do the delay and counter calculation */
        if (status.prio_status[i].rx_counter) {
            delay_data_calc(instance, &data->supp_state.fn_state[i], &data->out_state.fn_state[i], status.prio_status[i].delays, status.prio_status[i].rx_counter);
        }

        data->out_state.fn_state[i].tx_cnt = 0;
        data->out_state.fn_state[i].rx_cnt += status.prio_status[i].rx_counter;
        data->out_state.fn_state[i].rx_err_cnt += status.prio_status[i].rx_err_counter;

        data->out_state.nf_state[i].tx_cnt += status.prio_status[i].tx_counter;
        data->out_state.nf_state[i].rx_cnt = 0;
    }
}

static void run_mac_new(u32 instance)
{
    mep_instance_data_t  *data;
    u8                   mac[mep_caps.mesa_oam_peer_cnt][VTSS_MEP_SUPP_MAC_LENGTH];

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_MAC_NEW;

    if (!data->config.enable || data->config.voe)   /* This is only for instances that are enabled and not VOE based */
        return;

    /* Change SMAC the ACL rules for HW CCM reception */
    if (data->cc_config.enable && (data->config.peer_count == 1)) { /* ACL rule added to handle CCM */
        if (vtss_mep_supp_learned_mac_get(instance, mac) != VTSS_MEP_SUPP_RC_OK) {
            vtss_mep_trace("run_mac_new: Call to supp failed", instance, 0, 0, 0);
            return;
        }

        vtss_mep_acl_ccm_mac_change(data->instance,  mac[0]);   /* Change the SMAC in the CCM IS2 rule */
    }
}

/* put new LM counter measurement into FIFO and take out the oldest entry: */
static void put_lm_avail_fifo(mep_instance_data_t  *data,
                              const vtss_mep_supp_lm_peer_counters_t *in_counters,  /* new measurement */
                              vtss_mep_supp_lm_peer_counters_t *out_counters)       /* oldest measurement in fifo */
{
    if (!data->lm_avail_fifo) {
        /* no fifo, treat as fifo with 1 element: */
        *out_counters = *in_counters;
    } else {
        data->lm_avail_fifo[data->lm_avail_fifo_ix] = *in_counters;
        if ((data->lm_avail_fifo_ix + 1) < data->lm_avail_config.interval) {
            data->lm_avail_fifo_ix++;
        } else {
            data->lm_avail_fifo_ix = 0;
        }
        *out_counters = data->lm_avail_fifo[data->lm_avail_fifo_ix];
    }
}

static void vtss_mep_mgmt_lm_notif_set(const u32 instance, const u32 peer_idx)
{
    vtss_appl_mep_lm_notif_state_t data;
    out_state_t                    *out_state;

    out_state = &instance_data[instance].out_state;

    vtss_mep_lm_notif_status_get(instance, &data);

    data.peer_index       = peer_idx;
    data.near_state       = out_state->lm_avail_state[peer_idx].near_state;
    data.far_state        = out_state->lm_avail_state[peer_idx].far_state;
    if (instance_data[instance].los_int_cnt_timer == 0) {
        data.near_los_int_cnt = out_state->lm_state[peer_idx].near_los_int_cnt;
        data.far_los_int_cnt  = out_state->lm_state[peer_idx].far_los_int_cnt;
    }
    if (instance_data[instance].los_th_cnt_timer == 0) {
        data.near_los_th_cnt  = out_state->lm_state[peer_idx].near_los_th_cnt;
        data.far_los_th_cnt   = out_state->lm_state[peer_idx].far_los_th_cnt;
    }

    vtss_mep_lm_notif_status_set(instance, &data);
}

void vtss_mep_supp_lm_tx_update(const u32    instance,
                                const u32    peer_idx,
                                const vtss_mep_supp_lm_peer_counters_t *peer_counters)
{
    mep_instance_data_t         *data;
    out_state_t                 *out_state;
    vtss_appl_mep_lm_state_t    *out_lm_state;

    data = &instance_data[instance];    /* Instance data reference */
    out_state = &data->out_state;
    out_lm_state = &out_state->lm_state[peer_idx];
    out_lm_state->tx_counter = peer_counters->tx_counter;
}

/* called by vtss_mep_supp.c - beware, vtss_mep_supp_crit_lock() is taken */
void vtss_mep_supp_lm_measurement_init(const u32 instance,
                                       const u32 peer_idx)
{
    out_state_t                 *out_state;
    mep_instance_data_t         *data;
    data = &instance_data[instance];    /* Instance data reference */
    out_state = &data->out_state;
    out_state->lm_avail_state[peer_idx].near_state = VTSS_APPL_MEP_OAM_LM_AVAIL;
    out_state->lm_avail_state[peer_idx].far_state  = VTSS_APPL_MEP_OAM_LM_AVAIL;
    out_state->lm_avail_state[peer_idx].near_avail_cnt = 0;
    out_state->lm_avail_state[peer_idx].far_avail_cnt  = 0;
}

void vtss_mep_supp_lm_measurement(const u32     instance,
                                  const u32     peer_idx,
                                  const vtss_mep_supp_lm_peer_counters_t *peer_counters)
{
    out_state_t                 *out_state;
    mep_instance_data_t         *data;
    i32                         nearLos, farLos;
    u16                         nearFLR = 0, farFLR = 0;
    vtss_mep_supp_lm_peer_counters_t out_counters;
    lm_peer_state_t             *lm_state;
    vtss_appl_mep_lm_state_t    *out_lm_state;

    vtss_mep_crit_lock();

    data = &instance_data[instance];    /* Instance data reference */
    lm_state = &data->lm_peer_state[peer_idx];
    out_state = &data->out_state;
    out_lm_state = &out_state->lm_state[peer_idx];
    vtss_appl_mep_lm_avail_state_t *out_lm_avail_state = &out_state->lm_avail_state[peer_idx];

    if (data->lm_config.enable || data->lm_config.enable_rx) {
        T_N("lm_measurement - %u/%u, TX:%u, RX:%u, NTD:%u, NRD:%u, FTD:%u, FRD:%u", instance, peer_idx,
            peer_counters->tx_counter, peer_counters->rx_counter,
            peer_counters->near_tx_delta, peer_counters->near_rx_delta,
            peer_counters->far_tx_delta, peer_counters->far_rx_delta);
    }

    if (lm_state->flr_interval_count == 0) {
        /* Frame Loss Ratio interval restart: */
        lm_state->near_tx_counter       = 0;
        lm_state->far_tx_counter        = 0;
        lm_state->near_los_counter      = 0;
        lm_state->far_los_counter       = 0;
    }

    /* put new counter measurement in fifo and take out the oldest entry: */
    put_lm_avail_fifo(data, peer_counters, &out_counters);

    /* Availability (using new lm_counters measurement): */
    if (data->lm_avail_config.enable && !data->lm_avail_config.main && peer_counters->has_near_rx) {
        /* calculate loss for this measurement: */
        nearLos = (peer_counters->far_tx_delta  - peer_counters->near_rx_delta);
        farLos  = (peer_counters->near_tx_delta - peer_counters->far_rx_delta);

        /* calculate FLR for this measurement: */
        if (nearLos > 0 && peer_counters->far_tx_delta > 0)
            nearFLR = (((u32)nearLos) * 1000) / peer_counters->far_tx_delta;
        if (farLos > 0 && peer_counters->near_tx_delta > 0)
            farFLR = (((u32)farLos) * 1000)/ peer_counters->near_tx_delta;

        //note: the variable lm_near_avail_cnt is used to count both up from LM_AVAIL and LM_UNAVAIL, but with the meaning unavail_cnt contra avail_cnt.
        //note: and similar fo lm_far_avail_cnt is used to count both up from LM_AVAIL and LM_UNAVAIL, but with the meaning unavail_cnt contra avail_cnt.
        if (out_lm_avail_state->near_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
            if (nearFLR >= data->lm_avail_config.flr_th && nearFLR != 0) {
                out_lm_avail_state->near_avail_cnt++;
                if (out_lm_avail_state->near_avail_cnt >= data->lm_avail_config.interval) {   // n consequtive unavail, so change state
                    out_lm_avail_state->near_state = VTSS_APPL_MEP_OAM_LM_UNAVAIL;
                    out_lm_avail_state->near_avail_cnt= 0;
                }
            } else { // just one ok keeps state and reset cnt
               out_lm_avail_state->near_avail_cnt= 0;
            }
        } else {
            if (nearFLR < data->lm_avail_config.flr_th || nearFLR == 0) {
                out_lm_avail_state->near_avail_cnt++;
                if (out_lm_avail_state->near_avail_cnt >= data->lm_avail_config.interval) { // n consequtive avail, so change state
                    out_state->lm_avail_state[peer_idx].near_state = VTSS_APPL_MEP_OAM_LM_AVAIL;
                    out_lm_avail_state->near_avail_cnt= 0;
                }
            } else { // just one ok keeps state and reset cnt
               out_lm_avail_state->near_avail_cnt= 0;
            }
        }
        if (out_state->lm_avail_state[peer_idx].far_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
            if (farFLR >= data->lm_avail_config.flr_th && farFLR != 0) {
                out_lm_avail_state->far_avail_cnt++;
                if (out_lm_avail_state->far_avail_cnt >= data->lm_avail_config.interval) {   // n consequtive unavail, so change state
                    out_state->lm_avail_state[peer_idx].far_state = VTSS_APPL_MEP_OAM_LM_UNAVAIL;
                    out_lm_avail_state->far_avail_cnt= 0;
                }
            } else { // just one ok keeps state and reset cnt
               out_lm_avail_state->far_avail_cnt= 0;
            }
        } else {
            if (farFLR < data->lm_avail_config.flr_th || farFLR == 0) {
                out_lm_avail_state->far_avail_cnt++;
                if (out_lm_avail_state->far_avail_cnt >= data->lm_avail_config.interval) { // n consequtive avail, so change state
                    out_state->lm_avail_state[peer_idx].far_state = VTSS_APPL_MEP_OAM_LM_AVAIL;
                    out_lm_avail_state->far_avail_cnt= 0;
                }
            } else { // just one ok keeps state and reset cnt
               out_lm_avail_state->far_avail_cnt= 0;
            }
        }

        // calculate more info to the management interfaces
        if (!data->lm_avail_config.main) {
            if (out_state->lm_avail_state[peer_idx].near_state == VTSS_APPL_MEP_OAM_LM_AVAIL)
                out_state->lm_avail_state[peer_idx].near_cnt++;
            else
                out_state->lm_avail_state[peer_idx].near_un_cnt++;

            if (out_state->lm_avail_state[peer_idx].far_state == VTSS_APPL_MEP_OAM_LM_AVAIL)
                out_state->lm_avail_state[peer_idx].far_cnt++;
            else
                out_state->lm_avail_state[peer_idx].far_un_cnt++;
        }
    }

    /* the following are using out_counters from the fifo: */
    peer_counters = &out_counters;

    /* Loss: */
    out_lm_state->tx_counter = peer_counters->tx_counter;
    out_lm_state->rx_counter = peer_counters->rx_counter;

    /* calculate loss for this measurement: */
    nearLos = peer_counters->has_near_rx ? (peer_counters->far_tx_delta  - peer_counters->near_rx_delta) : 0;
    farLos  = (peer_counters->near_tx_delta - peer_counters->far_rx_delta);

    if (out_state->lm_avail_state[peer_idx].near_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
        /* near end loss: */
        if (nearLos > 0) {
            out_lm_state->near_los_int_cnt++;
        }
        out_lm_state->near_los_tot_cnt += nearLos;
        lm_state->near_los_counter += nearLos;

        if (nearLos >= 0 && nearLos >= data->lm_config.loss_th)
            out_lm_state->near_los_th_cnt++;
    }
    if (out_state->lm_avail_state[peer_idx].far_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
        /* far end loss: */
        if (farLos > 0) {
            out_lm_state->far_los_int_cnt++;
        }
        out_lm_state->far_los_tot_cnt += farLos;
        lm_state->far_los_counter += farLos;

        if (farLos >= 0 && farLos >= data->lm_config.loss_th) {
            out_lm_state->far_los_th_cnt++;
        }
    }

    /* FLR: */
    lm_state->flr_interval_count++;

    if (out_state->lm_avail_state[peer_idx].near_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
        /* Accumulate for near end ratio calculation */
        lm_state->far_tx_counter        += peer_counters->far_tx_delta;
        lm_state->tot_far_tx_counter    += peer_counters->far_tx_delta;

        if (lm_state->flr_interval_count >= data->lm_config.flr_interval) {
            /* Frame Loss Ratio interval has expired - calculate interval ratio */
            if ((lm_state->near_los_counter > 0) && (lm_state->far_tx_counter > 0)) {
                out_lm_state->near_flr_ave_int = (((u32)(lm_state->near_los_counter)*MEP_FLR_SCALE_FACTOR)/lm_state->far_tx_counter);

                if (out_lm_state->near_flr_ave_int > 0 && out_lm_state->near_flr_min == 0 && out_lm_state->near_flr_max == 0) {
                    /* first non-zero FLR */
                    out_lm_state->near_flr_min = out_lm_state->near_flr_ave_int;
                    out_lm_state->near_flr_max = out_lm_state->near_flr_ave_int;
                } else {
                    if (out_lm_state->near_flr_ave_int < out_lm_state->near_flr_min)
                        out_lm_state->near_flr_min = out_lm_state->near_flr_ave_int;
                    if (out_lm_state->near_flr_ave_int > out_lm_state->near_flr_max)
                        out_lm_state->near_flr_max = out_lm_state->near_flr_ave_int;
                }

            } else {
                out_lm_state->near_flr_ave_int = 0;
            }

            //if ((out_lm_state->near_flr_ave_int/100) > data->lm_config.loss_ratio_threshold) {
            //    vtss_mep_lm_notificaiton(instance, &data->lm_config, out_lm_state); /* Check if MEP LM notification is needed */
            //}
        }

        if ((out_lm_state->near_los_tot_cnt > 0) && (lm_state->tot_far_tx_counter > 0))
            out_lm_state->near_flr_ave_total = (((u32)(out_lm_state->near_los_tot_cnt)*MEP_FLR_SCALE_FACTOR)/lm_state->tot_far_tx_counter);
        else
            out_lm_state->near_flr_ave_total = 0;
    }

    if (out_state->lm_avail_state[peer_idx].far_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
        /* Accumulate for far end ratio calculation */
        lm_state->near_tx_counter      += peer_counters->near_tx_delta;
        lm_state->tot_near_tx_counter  += peer_counters->near_tx_delta;

        if (lm_state->flr_interval_count >= data->lm_config.flr_interval) {
            /* Frame Loss Ratio interval has expired - calculate internal ratio */
            if ((lm_state->far_los_counter > 0) && (lm_state->near_tx_counter > 0)) {
                out_lm_state->far_flr_ave_int = (((u32)(lm_state->far_los_counter)*MEP_FLR_SCALE_FACTOR)/lm_state->near_tx_counter);

                if (out_lm_state->far_flr_ave_int > 0 && out_lm_state->far_flr_min == 0 && out_lm_state->far_flr_max == 0) {
                    /* first non-zero FLR */
                    out_lm_state->far_flr_min = out_lm_state->far_flr_ave_int;
                    out_lm_state->far_flr_max = out_lm_state->far_flr_ave_int;
                } else {
                    if (out_lm_state->far_flr_ave_int < out_lm_state->far_flr_min)
                        out_lm_state->far_flr_min = out_lm_state->far_flr_ave_int;
                    if (out_lm_state->far_flr_ave_int > out_lm_state->far_flr_max)
                        out_lm_state->far_flr_max = out_lm_state->far_flr_ave_int;
                }

            } else {
                out_lm_state->far_flr_ave_int = 0;
            }

            //if ((out_lm_state->far_flr_ave_int/100) > data->lm_config.loss_ratio_threshold) {
            //    vtss_mep_lm_notificaiton(instance, &data->lm_config, out_lm_state); /* Check if MEP LM notification is needed */
            //}
        }

        if ((out_lm_state->far_los_tot_cnt > 0) && (lm_state->tot_near_tx_counter > 0))
            out_lm_state->far_flr_ave_total = (((u32)(out_lm_state->far_los_tot_cnt)*MEP_FLR_SCALE_FACTOR)/lm_state->tot_near_tx_counter);
        else
            out_lm_state->far_flr_ave_total = 0;
    }

    /* HLI: */
    if (data->lm_hli_config.enable) {
        if (out_state->lm_avail_state[peer_idx].near_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
            if (nearFLR > data->lm_hli_config.flr_th) {
                data->out_state.lm_hli_state[peer_idx].near_cnt++;
                data->lm_hli_state[peer_idx].near_cnt++;
                if (data->lm_hli_state[peer_idx].near_cnt == data->lm_hli_config.con_int) {
                    data->out_state.lm_hli_state[peer_idx].near_con_cnt++;
                }
            } else
                data->lm_hli_state[peer_idx].near_cnt = 0;
        }
        if (out_state->lm_avail_state[peer_idx].far_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
            if (farFLR > data->lm_hli_config.flr_th) {
                data->out_state.lm_hli_state[peer_idx].far_cnt++;
                data->lm_hli_state[peer_idx].far_cnt++;
                if (data->lm_hli_state[peer_idx].far_cnt == data->lm_hli_config.con_int) {
                    data->out_state.lm_hli_state[peer_idx].far_con_cnt++;
                }
            } else
                data->lm_hli_state[peer_idx].far_cnt = 0;
        }
    }

    /* DEG: */
    if (data->lm_sdeg_config.enable) {
        if (out_state->lm_avail_state[peer_idx].near_state == VTSS_APPL_MEP_OAM_LM_AVAIL) {
            if (peer_counters->far_tx_delta > data->lm_sdeg_config.tx_min && nearLos >= 0) {
                if (nearFLR > data->lm_sdeg_config.flr_th) {
                    data->lm_sdeg_bad = TRUE;
                } else {
                    data->lm_sdeg_bad = FALSE;
                }
            }
            if (data->lm_sdeg_bad) {
                data->lm_sdeg_good_cnt = 0;
                if (data->lm_sdeg_bad_cnt < data->lm_sdeg_config.bad_th) { /* wrap guard */
                    data->lm_sdeg_bad_cnt++;
                }
            } else {
                data->lm_sdeg_bad_cnt = 0;
                if (data->lm_sdeg_good_cnt < data->lm_sdeg_config.good_th) { /* wrap guard */
                    data->lm_sdeg_good_cnt++;
                }
            }
            if (!data->dDeg && data->lm_sdeg_bad_cnt >= data->lm_sdeg_config.bad_th) {
                data->dDeg = TRUE;
                data->event_flags |= EVENT_IN_SSF_SSD_STATE;
                vtss_mep_run();
            } else if (data->dDeg && data->lm_sdeg_good_cnt >= data->lm_sdeg_config.good_th) {
                data->dDeg = FALSE;
                data->event_flags |= EVENT_IN_SSF_SSD_STATE;
                vtss_mep_run();
            }
        }
    }

    vtss_mep_mgmt_lm_notif_set(instance, peer_idx);
    if (data->lm_hli_config.enable &&
        data->hli_cnt_timer == 0) {
        vtss_mep_lm_hli_status_set(instance, data->config.peer_mep[peer_idx],
                                   &data->out_state.lm_hli_state[peer_idx]);
    }

    if (lm_state->flr_interval_count >= data->lm_config.flr_interval) {
        /* restart Frame Loss Ratio interval */
        lm_state->flr_interval_count    = 0;
        out_lm_state->flr_interval_elapsed++;
    }

    vtss_mep_crit_unlock();
}


static void run_lt_timer(u32 instance)
{
    u32                         rc = VTSS_MEP_SUPP_RC_OK;
    mep_instance_data_t         *data;
    vtss_mep_supp_ltm_conf_t    conf;

    data = &instance_data[instance];    /* Instance data reference */
    memset(&conf, 0, sizeof(conf));

    data->event_flags &= ~EVENT_IN_LT_TIMER;

    if (data->lt_config.enable)
    {
        data->out_state.lt_state.transaction_cnt++;
        if (data->out_state.lt_state.transaction_cnt < data->lt_config.transactions)
        {
            data->lt_state.transaction_id++;
            data->lt_state.timer = 5000/timer_res;
            vtss_mep_timer_start();

            data->out_state.lt_state.transaction[data->out_state.lt_state.transaction_cnt].transaction_id = data->lt_state.transaction_id;

            lt_conf_calc(&conf,  data);

            rc = vtss_mep_supp_ltm_conf_set(instance, &conf);
        }
        else
        {   /* No more transactions - disable LTM */
            data->lt_config.enable = FALSE;
            conf.enable = FALSE;
            rc += vtss_mep_supp_ltm_conf_set(instance, &conf);
        }
        if (rc != VTSS_MEP_SUPP_RC_OK)       vtss_mep_trace("run_lt_timer: Call to supp failed", instance, 0, 0, 0);
    }
}


static void run_lb_timer(u32 instance)
{
    u32                         i, j;
    mep_instance_data_t         *data;
    vtss_mep_supp_lbm_conf_t    conf;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_LB_TIMER;

    if (test_enable_calc(data->lb_config.tests)) {
        if (data->lb_config.common.to_send != VTSS_APPL_MEP_LB_TO_SEND_INFINITE) { /* series behaviour - SW driven - stop transmission */
            memset(&conf, 0, sizeof(conf));

            for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
                for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
                    data->lb_config.tests[i][j].enable = FALSE;
                }
            }

            if (vtss_mep_supp_lbm_conf_set(instance, &conf, NULL) != VTSS_MEP_SUPP_RC_OK)       vtss_mep_trace("run_lb_timer: Call to supp failed", instance, 0, 0, 0);
        }
    }
}


static void run_lst_ign_timer(u32 instance)
{
    mep_instance_data_t         *data;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_LST_IGN_TIMER;

    if (data->lst_config.enable) {
        lst_action(data);
    }
}


static void run_lst_sf_timer(u32 instance)
{
    mep_instance_data_t         *data;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_LST_SF_TIMER;

    if (data->lst_config.enable) {
        lst_action(data);
    }
}


static void run_tst_timer(u32 instance)
{
    u32                         count, sec_count, rx_bits;
    vtss_mep_supp_tst_status_t  tst_state;
    mep_instance_data_t         *data;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_TST_TIMER;

    if (data->tst_state.rx_count_active) {
#if !defined(VTSS_SW_OPTION_ACL)
        if (!MPLS_DOMAIN(data->config.domain)) {
            mesa_vid_mac_t         vid_mac;
            mesa_mac_table_entry_t entry;

            vid_mac.vid = data->rule_vid;   /* Every second the copy to CPU is enabled in MAC table. When TST PDU is received it is disabled as all TST PDU will go to CPU and overload it */
            vid_mac.mac = data->config.mac;
            if (mesa_mac_table_get(NULL, &vid_mac, &entry) == VTSS_RC_OK) {
                entry.copy_to_cpu = 1;
                if (mesa_mac_table_add(NULL, &entry) != VTSS_RC_OK) vtss_mep_trace("run_tst_timer: Call to mesa_mac_table_add", instance, 0, 0, 0);
            }
        }
#endif

        if (!MPLS_DOMAIN(data->config.domain) && !data->config.voe) {   /* This is not a VOE based MEP */
            vtss_mep_acl_tst_count(instance,  &count);      /* Get ACL counter */
            sec_count = count - data->tst_state.tst_count;   /* This is received in this second */
            data->tst_state.tst_count = count;
            data->out_state.tst_state.rx_counter += sec_count;
        }
        else {  /* This is VOE based or MPLS - get the TST frame counter from SUPP */
            if (vtss_mep_supp_tst_status_get(instance, &tst_state) != VTSS_MEP_SUPP_RC_OK)       vtss_mep_trace("vtss_appl_mep_tst_state_get: Call to supp failed", instance, 0, 0, 0);
            sec_count = tst_state.rx_counter - data->tst_state.tst_count;   /* This is received in this second */
            data->tst_state.tst_count = tst_state.rx_counter;
        }

        rx_bits = sec_count * data->tst_state.rx_frame_size * 8;    /* Calculate the information rate for this second */
        data->out_state.tst_state.rx_rate = rx_bits/1000;           /* RX rate in Kbps */
    }

    data->out_state.tst_state.time += 1;    /* Increment the 'Test time' */
    data->tst_state.timer = 1000/timer_res;
    vtss_mep_timer_start();
}


static void run_syslog_timer(u32 instance)
{
    mep_instance_data_t         *data;
    u32                         idx, self_mep;
    BOOL                        find_first_entry = FALSE;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_SYSLOG_TIMER;

    self_mep = data->instance + 1;

    /* syslog for CFM, VTSS_MEP_CROSSCHECK_MEP_MISSING */
    for (idx = 0; idx < mep_caps.mesa_oam_peer_cnt; ++idx) {
        if (data->syslog_state.transaction_id[idx] > 0 && data->syslog_state.peer_mep_timer[idx] == 0) {
            vtss_mep_syslog(VTSS_MEP_CROSSCHECK_MEP_MISSING, self_mep, data->syslog_state.transaction_id[idx]);
        }
        /* reset transaction id */
        data->syslog_state.transaction_id[idx] = 0;
    }

    /* find the next timeout timer */
    for (idx = 0; idx < mep_caps.mesa_oam_peer_cnt; ++idx) {
        if (data->syslog_state.transaction_id[idx] > 0 && data->syslog_state.peer_mep_timer[idx] > 0) {
            if (!find_first_entry) {
                data->syslog_state.timer = data->syslog_state.peer_mep_timer[idx];
                find_first_entry = TRUE;
                continue;
            }
            if (data->syslog_state.timer > data->syslog_state.peer_mep_timer[idx]) {
                data->syslog_state.timer = data->syslog_state.peer_mep_timer[idx];
            }
        }
    }
}

static void run_lm_clear(u32 instance, vtss_appl_mep_clear_dir direction)
{
    mep_instance_data_t     *data;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_LM_CLEAR;

    memset(data->lm_peer_state, 0, sizeof(data->lm_peer_state));
    memset(data->out_state.lm_state, 0, sizeof(data->out_state.lm_state));

    for (int peer_index = 0; peer_index < mep_caps.mesa_oam_peer_cnt; peer_index++) {
        data->out_state.lm_hli_state[peer_index].near_cnt       = 0;
        data->out_state.lm_hli_state[peer_index].far_cnt        = 0;
        data->out_state.lm_hli_state[peer_index].near_con_cnt   = 0;
        data->out_state.lm_hli_state[peer_index].far_con_cnt    = 0;

        data->lm_hli_state[peer_index].near_cnt                 = 0;
        data->lm_hli_state[peer_index].far_cnt                  = 0;
    }

    if (vtss_mep_supp_lm_counters_clear(instance, mep_supp_clear_dir_get(direction)) != VTSS_MEP_SUPP_RC_OK)
        vtss_mep_trace("run_lm_clear: Call to supp failed", instance, 0, 0, 0);
}


static void run_dm_clear(u32 instance)
{
    mep_instance_data_t         *data;
    vtss_appl_mep_dm_tunit_t    tunit_dm1, tunit_dmr, tunit_dm2;
    u32                         i;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_DM_CLEAR;

    memset(data->supp_state.tw_state, 0, sizeof(data->supp_state.tw_state));
    memset(data->supp_state.fn_state, 0, sizeof(data->supp_state.fn_state));
    memset(data->supp_state.nf_state, 0, sizeof(data->supp_state.nf_state));

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {
        /* Time unit must be reserved */
        tunit_dmr = data->out_state.tw_state[i].tunit;
        tunit_dm1 = data->out_state.fn_state[i].tunit;
        tunit_dm2 = data->out_state.nf_state[i].tunit;

        memset(&data->out_state.tw_state[i], 0, sizeof(data->out_state.tw_state[i]));
        data->out_state.tw_state[i].min_delay = 0xffffffff;
        data->out_state.tw_state[i].min_delay_var = 0xffffffff;

        memset(&data->out_state.fn_state[i], 0, sizeof(data->out_state.fn_state[i]));
        data->out_state.fn_state[i].min_delay = 0xffffffff;
        data->out_state.fn_state[i].min_delay_var = 0xffffffff;

        memset(&data->out_state.nf_state[i], 0, sizeof(data->out_state.nf_state[i]));
        data->out_state.nf_state[i].min_delay = 0xffffffff;
        data->out_state.nf_state[i].min_delay_var = 0xffffffff;

        /* Revert time unit */
        data->out_state.tw_state[i].tunit = tunit_dmr;
        data->out_state.fn_state[i].tunit = tunit_dm1;
        data->out_state.nf_state[i].tunit = tunit_dm2;
    }
}


static void run_tst_clear(u32 instance)
{
    mep_instance_data_t     *data;
    tst_state_t             old_tst_state;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_TST_CLEAR;

    vtss_mep_acl_tst_clear(instance);           /* Clear HW counters */
    (void)vtss_mep_supp_tst_clear(instance);

    old_tst_state = data->tst_state;                        /* Clear TST state */
    memset(&data->tst_state, 0, sizeof(data->tst_state));
    data->tst_state.active = old_tst_state.active;

    memset(&data->out_state.tst_state, 0, sizeof(data->out_state.tst_state));   /* Clear OUT state */

    if (test_enable_calc(data->tst_config.tests) || data->tst_config.common.enable_rx) {    /* Either tx or rx is enabled */
        data->tst_state.timer = 1000/timer_res;     /* Start the TST timer only to make 'Test time' counting */
        vtss_mep_timer_start();
    }

#if !defined(VTSS_SW_OPTION_ACL)
    mesa_vid_mac_t         vid_mac;
    mesa_mac_table_entry_t entry;

    vid_mac.vid = data->rule_vid;
    vid_mac.mac = data->config.mac;
    if (mesa_mac_table_get(NULL, &vid_mac, &entry) == VTSS_RC_OK) {
        entry.copy_to_cpu = 1;
        if (mesa_mac_table_add(NULL, &entry) != VTSS_RC_OK) vtss_mep_trace("run_tst_clear: Call to mesa_mac_table_add", instance, 0, 0, 0);
    }
#endif
}

static void run_lb_clear(u32 instance)
{
    mep_instance_data_t  *data;
    u32                  transaction_id;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_LB_CLEAR;

    (void)vtss_mep_supp_lb_clear(instance);

    transaction_id = data->out_state.lb_state.transaction_id;                 /* Clear OUT state */
    memset(&data->out_state.lb_state, 0, sizeof(data->out_state.lb_state));
    data->out_state.lb_state.transaction_id = transaction_id;
}

static void run_lst_config(u32 instance)
{
    mep_instance_data_t  *data;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_LST_CONFIG;
    data->lst_state.fe_disable = 0;
    data->lst_state.ign_timer = 0;
    data->lst_state.sf_timer = 0;

    lst_action(data);
}

static void run_tlv_is_new(u32 instance)
{
    mep_instance_data_t         *data;
    vtss_mep_supp_ccm_state_t   ccm_state;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_TLV_IS_NEW;

    if (vtss_mep_supp_ccm_state_get(instance,  &ccm_state) == VTSS_MEP_SUPP_RC_OK) {
        data->supp_state.is_received = ccm_state.is_received[0];
        data->supp_state.is_tlv = ccm_state.is_tlv[0];

        lst_action(data);
    }
}


/****************************************************************************/
/*  MEP management interface                                                */
/****************************************************************************/

u32 vtss_mep_mgmt_tlv_conf_set(const vtss_appl_mep_tlv_conf_t    *const conf)
{
    u32 i;
    vtss_mep_crit_lock();

    os_tlv_config = conf->os_tlv;

    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i) {
        if (instance_data[i].config.enable && instance_data[i].cc_config.enable)
            instance_data[i].event_flags |= EVENT_IN_TLV_CHANGE;
    }

    vtss_mep_run();

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_tlv_conf_get(vtss_appl_mep_tlv_conf_t    *const conf)
{
    vtss_mep_crit_lock();

    conf->os_tlv = os_tlv_config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_conf_set(const u32        instance,
                           const mep_conf_t *const conf,
                           BOOL             vola)
{
    u32                   rc, i, j, vid, port_cnt, res_port;
    mep_instance_data_t   *data;    /* Instance data reference */
    mesa_port_list_t      nni;
    mep_conf_t           *iconf;






    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];
    vid = data->rule_vid;
    port_cnt = vtss_mep_port_count();




    if (!vola && data->vola)                                                                    {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_VOLATILE);}
    if (!conf->enable && !data->config.enable)                                                  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}

    res_port = conf->port;

    if (conf->enable)
    {
        res_port = conf->port;

        if (data->config.enable) {  /* The MEP Instance is already enabled - some parameters must not change */
            if ((data->config.mode != conf->mode) || (data->config.direction != conf->direction) || (data->config.domain != conf->domain) ||
                (data->config.flow != conf->flow) || (data->config.port != res_port) ||
                (memcmp(&data->config.mac, &conf->mac, sizeof(data->config.mac))))              {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_CHANGE_PARAMETER);}
        }
        if (conf->level > 7)                                                                    {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_LEVEL);}
        if (conf->mep > 0x1FFF)                                                                 {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if (conf->vid > 4095)                                                                   {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if (res_port >= port_cnt)                                                               {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if (conf->peer_count > mep_caps.mesa_oam_peer_cnt)                                      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if ((conf->domain == VTSS_APPL_MEP_PORT) && (conf->mode != VTSS_APPL_MEP_MEP))          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP_SUPPORT);}
        if ((conf->domain == VTSS_APPL_MEP_VLAN) && (conf->mode != VTSS_APPL_MEP_MEP))          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP_SUPPORT);}
        if ((conf->domain == VTSS_APPL_MEP_PORT) &&
            (conf->direction == VTSS_APPL_MEP_UP) && (conf->vid == 0))                          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_VID);}
        for (i=0; i<conf->peer_count; ++i) {
            if (conf->peer_mep[i] > 0x1FFF)                                                     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_ID);}
            if (conf->peer_mep[i] == conf->mep)                                                 {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_ID);}
        }
        if ((conf->peer_count == 0) &&
            (data->cc_config.enable || data->lm_config.enable || data->aps_config.enable))      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_CNT);}
        if ((conf->peer_count != 1) && data->lm_sdeg_config.enable)                             {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_CNT);}



        if (conf->peer_count > 1)
        {
            if (vtss_mep_supp_hw_ccm_generator_get(supp_rate_calc(data->cc_config.rate)))       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_CNT);}
            if (data->lst_config.enable)                                                        {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_CNT);}
            for (i=0; i<conf->peer_count; ++i)
                for (j=0; j<conf->peer_count; ++j)
                    if ((i != j) && (conf->peer_mep[i] == conf->peer_mep[j]))                   {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_ID);}
        }

        for (i=0; i<VTSS_APPL_MEP_MEG_CODE_LENGTH; ++i)      if (conf->name[i] == '\0') break;
        if (i == VTSS_APPL_MEP_MEG_CODE_LENGTH)                                                 {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_MEG);}
        if (((conf->format == VTSS_APPL_MEP_ITU_ICC) ||
             (conf->format == VTSS_APPL_MEP_ITU_CC_ICC)) && (i != 0))                           {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_MEG);}

        for (i=0; i<VTSS_APPL_MEP_MEG_CODE_LENGTH; ++i)      if (conf->meg[i] == '\0') break;
        if ((i == VTSS_APPL_MEP_MEG_CODE_LENGTH) || (i == 0))                                   {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_MEG);}
        if ((conf->format == VTSS_APPL_MEP_ITU_ICC) && (i > 13))                                {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_MEG);}
        if ((conf->format == VTSS_APPL_MEP_ITU_CC_ICC) && (i > 15))                             {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_MEG);}
        if (!mep_caps.mesa_mep_serval) {
            /* This check is for NON SERVAL only */
            u8 mac[VTSS_APPL_MEP_MAC_LENGTH];

            if (!MPLS_DOMAIN(data->config.domain)) {
                if (memcmp(all_zero_mac, data->config.mac.addr, sizeof(all_zero_mac))) {  /* Check if the configured MAC is NOT all zero */
                    vtss_mep_mac_get(data->config.port, mac);  /* Get MAC of residence port if NOT all zero mac - it must be port MAC!! */
                    if (memcmp(mac, data->config.mac.addr, sizeof(mac)))                                {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_MAC);}
                }
            }
        }

        if (conf->domain == VTSS_APPL_MEP_EVC)                                                  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_EVC);}

        if (mep_caps.mesa_mep_serval) {
            if ((conf->domain == VTSS_APPL_MEP_PORT) && (conf->direction == VTSS_APPL_MEP_UP))      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
            if (conf->out_of_service && (!conf->voe || (conf->direction != VTSS_APPL_MEP_UP)))      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}

            BOOL enabled;
            /* Attempt to change the use of VOE. Reject if this mep DO have an enabled management function which relies on VOE*/
            if (data->config.voe != conf->voe) {
                enabled = FALSE;
                for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
                    if (data->dm_config.dm[i].enable) { enabled = TRUE; break; }
                    for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j)
                        if (data->lb_config.tests[i][j].enable) { enabled = TRUE; break; break; }
                    for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j)
                        if (data->tst_config.tests[i][j].enable) { enabled = TRUE; break; break; }
                }
                if (!enabled && (data->cc_config.enable || data->lm_config.enable || data->lm_config.enable || data->aps_config.enable || data->ais_config.enable || data->lck_config.enable))
                    enabled = TRUE;
                if (enabled) {
                    vtss_mep_crit_unlock();
                    return(VTSS_MEP_RC_INVALID_VOE_CHANGE);
                }
            }
        }
        if (!mep_caps.mesa_mep_serval) {
            if (conf->domain == VTSS_APPL_MEP_VLAN)                                                 {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_VLAN_SUPPORT);}
            if ((conf->domain == VTSS_APPL_MEP_PORT) && (conf->direction == VTSS_APPL_MEP_UP))
            { /* An Egress port MEP is created - ATM this is only for support of ERPS version 2 */
                if (!vtss_mep_vlan_get(conf->vid, nni))                                             {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_VID);}
                if (!nni[res_port])                                                                 {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_VID);}
                for (i=0; i<mep_caps.mesa_port_cnt; ++i)  /* There must be a active NNI that is not my residence port */
                    if ((i != res_port) && nni[i])    break;
                if (i == mep_caps.mesa_port_cnt)                                                      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_VID);}
                for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
                {
                    if ((i != instance) && (instance_data[i].config.enable) && (instance_data[i].config.domain == VTSS_APPL_MEP_PORT) && (instance_data[i].config.vid == conf->vid))
                    {/* Another port MEP is created in this VLAN - not all combinations are allowed */
                        if (instance_data[i].config.level == conf->level)
                        {/* MEP on same level already created */
                            if (instance_data[i].config.direction == VTSS_APPL_MEP_UP)                                                        {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                            if ((instance_data[i].config.direction == VTSS_APPL_MEP_DOWN) && (res_port != instance_data[i].config.port))      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                        }
                    }
                }
            }
        }























































































        if (conf->domain == VTSS_APPL_MEP_VLAN)
        {   /* Get flow info from VLAN */
            if (conf->direction == VTSS_APPL_MEP_UP) {
                if (!vtss_mep_vlan_get(conf->flow, nni))                                           {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_VID);}
                if (!nni[res_port])                                                                 {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PORT);}
                for (i=0; i<mep_caps.mesa_port_cnt; ++i)  /* There must be an active NNI that is not my residence port */
                    if ((i != res_port) && nni[i])    break;
                if (i == mep_caps.mesa_port_cnt)                                                      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_VID);}
            }
            for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
            {
                if ((i != instance) && (instance_data[i].config.enable) && (instance_data[i].config.domain == VTSS_APPL_MEP_VLAN) && (instance_data[i].config.flow == conf->flow))
                {/* Another VLAN MEP is created in this VLAN - not all combinations are allowed */
                    iconf = &instance_data[i].config;
                    if (conf->voe && (conf->direction == VTSS_APPL_MEP_DOWN) &&
                        iconf->voe && (iconf->direction == VTSS_APPL_MEP_DOWN) && (res_port == iconf->port))    {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                    if (conf->voe && (conf->direction == VTSS_APPL_MEP_UP) &&
                        iconf->voe && (iconf->direction == VTSS_APPL_MEP_UP))                                   {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                    if ((conf->direction == VTSS_APPL_MEP_UP) && (iconf->direction == VTSS_APPL_MEP_UP) &&
                        (res_port != iconf->port))                                                              {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                    if (conf->voe && (conf->direction == VTSS_APPL_MEP_DOWN) &&
                        (iconf->direction == VTSS_APPL_MEP_DOWN) && (iconf->port == res_port) &&
                        (iconf->level >= conf->level))                                                          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                    if (conf->voe && (conf->direction == VTSS_APPL_MEP_UP) &&
                        (iconf->direction == VTSS_APPL_MEP_UP) && (iconf->level <= conf->level))                {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                    if (!conf->voe && (conf->direction == VTSS_APPL_MEP_DOWN) &&
                        iconf->voe && (iconf->direction == VTSS_APPL_MEP_DOWN) && (iconf->port == res_port) &&
                        (iconf->level <= conf->level))                                                          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                    if (!conf->voe && (conf->direction == VTSS_APPL_MEP_UP) &&
                        iconf->voe && (iconf->direction == VTSS_APPL_MEP_UP) && (iconf->level >= conf->level))  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                    if ((iconf->direction == VTSS_APPL_MEP_UP) && (iconf->port != res_port) &&
                        (iconf->level <= conf->level) && (conf->direction == VTSS_APPL_MEP_DOWN))               {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                    if ((iconf->direction == VTSS_APPL_MEP_DOWN) && (iconf->port != res_port) &&
                        (iconf->level >= conf->level) && (conf->direction == VTSS_APPL_MEP_UP))                 {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                }
            }
        }
        if (mep_caps.mesa_mep_serval) {
            u32 port_voe = 0;
            u32 service_voe = 0;
            if (conf->voe) {
                for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i) {
                    if (i == instance)  continue;
                    if (instance_data[i].config.enable && instance_data[i].config.voe && ((instance_data[i].config.domain == VTSS_APPL_MEP_EVC) || (instance_data[i].config.domain == VTSS_APPL_MEP_VLAN))) ++service_voe;
                    if (instance_data[i].config.enable && instance_data[i].config.voe && (instance_data[i].config.domain == VTSS_APPL_MEP_PORT)) ++port_voe;
                }
                if ((service_voe >= mep_caps.mesa_oam_path_service_voe_cnt) || (port_voe >= mep_caps.mesa_oam_port_voe_cnt))     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NO_VOE);}

                /* It should only be possible to create one VOE based MEP on a port */
                for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i) {
                    if (i == instance)  continue;
                    if (instance_data[i].config.enable && instance_data[i].config.voe && (instance_data[i].config.domain == VTSS_APPL_MEP_PORT) &&
                        (conf->domain == VTSS_APPL_MEP_PORT) && (res_port == instance_data[i].config.port))         {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                }
            }
            else {    /* conf->voe */
                /* It should be possible to create multiple SW based MEPs on a port but on different levels */
                for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i) {
                    if (i == instance)  continue;
                    if (instance_data[i].config.enable && (instance_data[i].config.domain == VTSS_APPL_MEP_PORT) &&
                        (conf->domain == VTSS_APPL_MEP_PORT) && (res_port == instance_data[i].config.port) &&
                        (conf->level == instance_data[i].config.level))                                             {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_LEVEL);}
                 }
            }

        } else {
            if (conf->voe)                                                                                          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NO_VOE);}
        }

    } /* conf->enable == TRUE */


    if (((conf->enable) && (data->config.enable) && (data->rule_vid) &&
         ((conf->voe && !data->config.voe) || ((conf->domain == VTSS_APPL_MEP_PORT) && (conf->vid != data->config.vid)) || ((conf->domain == VTSS_APPL_MEP_EVC) && (vid != data->rule_vid)))) ||
        (!conf->enable))
    {
    /* NOT VOE based is change to VOE based - OR - Tagged VID is changed on a PORT MEP - OR - VID has changed on a EVC MEP - OR - this MEP is disabled */
        /* Rules are updated with this MEP disabled - existing rules on "old" VID are maybe removed */
        data->config.enable = FALSE;
        if (!MPLS_DOMAIN(data->config.domain)) {
            if (data->rule_vid && !pdu_fwr_cap_update(data->rule_vid, data->rule_ports)) {
                vtss_mep_rule_update_failed(data->rule_vid);
            }
        }
    }

    if ((conf->enable) && ((conf->peer_count != data->config.peer_count) || (memcmp(conf->peer_mep, data->config.peer_mep, data->config.peer_count*sizeof(u16))))) {  /* Change in peer MEP configuration */
        syslog_new_peer_mep(data,  conf);
    }

    if (conf->enable) {
        data->config = *conf;
        /*If new meg strlen is shorter than old meg strlen, the next statement will ensure that leftovers from old meg will be null'ed */
        strncpy(data->config.meg, conf->meg, VTSS_APPL_MEP_MEG_CODE_LENGTH-1);
        data->config.port = res_port;
    }
    else                data->config.enable = FALSE;


    if ((rc = run_config(instance)) != VTSS_MEP_RC_OK) {   /* This is now called synchronously */
        data->config.enable = FALSE;
        (void)run_config(instance);
    }

    vtss_mep_crit_unlock();

    return(rc);
}


u32 vtss_mep_mgmt_volatile_conf_set(const u32          instance,
                                    const mep_conf_t  *const conf)
{
    u32 rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!conf->enable && !instance_data[instance].vola)     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}    /* Only allowed to disable a volatile instance */

    vtss_mep_crit_unlock(); /* Unlock while configuring */
    rc = vtss_mep_mgmt_conf_set(instance, conf, TRUE);
    vtss_mep_crit_lock();

    if (rc == VTSS_MEP_RC_OK) {
        instance_data[instance].vola = conf->enable;    /* change instance volatile state */
    }

    vtss_mep_crit_unlock();

    return(rc);
}


u32 vtss_mep_mgmt_volatile_sat_conf_set(const u32             instance,
                                        const mep_sat_conf_t  *const conf)
{
    u32 rc = VTSS_MEP_RC_OK;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                                                                        return(VTSS_MEP_RC_INVALID_INSTANCE);
    if (!conf->conf.voe || (conf->conf.domain != VTSS_APPL_MEP_EVC) || (conf->conf.direction != VTSS_APPL_MEP_UP))     return(VTSS_MEP_RC_INVALID_PARAMETER);

    vtss_mep_crit_lock();

    if (!conf->conf.enable && !instance_data[instance].vola)     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}    /* Only allowed to disable a volatile instance */

    memcpy(instance_data[instance].sat_prio_conf, conf->prio_conf, sizeof(instance_data[instance].sat_prio_conf));
    instance_data[instance].sat = TRUE;     /* Enable SAT mode */

    vtss_mep_crit_unlock(); /* Unlock while configuring */
    rc = vtss_mep_mgmt_conf_set(instance, &conf->conf, TRUE); /* Try and configure this */
    vtss_mep_crit_lock();

    if (rc != VTSS_MEP_RC_OK) { /* If the configure failed then SAT mode is disabled before return */
        instance_data[instance].vola = FALSE;
        instance_data[instance].sat = FALSE;
    } else {
        instance_data[instance].vola = conf->conf.enable;    /* change instance volatile state */
        instance_data[instance].sat = conf->conf.enable;     /* change instance SAT state */
    }

    vtss_mep_crit_unlock();

    return(rc);
}


u32 vtss_mep_mgmt_volatile_sat_conf_get(const u32       instance,
                                        mep_sat_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                                                                        return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!instance_data[instance].sat)     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}    /* Only allowed to get a SAT instance */

    memcpy(conf->prio_conf, instance_data[instance].sat_prio_conf, sizeof(conf->prio_conf));
    conf->conf = instance_data[instance].config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_volatile_sat_counter_get(const u32           instance,
                                           mep_sat_counters_t  *const counters)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_MEP_RC_INVALID_INSTANCE);

    memset(counters, 0, sizeof(*counters));

    return(vtss_mep_supp_sat_counters_get(instance, counters));
}

u32 vtss_mep_mgmt_volatile_sat_counter_clear_set(const u32  instance)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_MEP_RC_INVALID_INSTANCE);

    return(vtss_mep_supp_sat_counters_clear(instance));
}

u32 vtss_mep_mgmt_conf_get(const u32         instance,
                           mep_conf_t       *const conf)
{
    BOOL vola;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (instance_data[instance].config.enable) {
        peer_mac_update(&instance_data[instance]);
    }
    *conf = instance_data[instance].config;
    vola = instance_data[instance].vola;

    vtss_mep_crit_unlock();

    return(vola ? VTSS_MEP_RC_VOLATILE : VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_etree_conf_get(const u32                    instance,
                                 vtss_mep_mgmt_etree_conf_t   *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    conf->e_tree = VTSS_MEP_ETREE_NONE;
    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MEP) { /* Only MEP (not MIP) Should be identified as ETREE instance */
        switch (instance_data[instance].evc_type) {
            case VTSS_MEP_SUPP_ROOT:  conf->e_tree = VTSS_MEP_ETREE_ROOT; break;
            case VTSS_MEP_SUPP_LEAF:  conf->e_tree = VTSS_MEP_ETREE_LEAF; break;
            default:                  conf->e_tree = VTSS_MEP_ETREE_NONE; break;
        }
    }

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_pm_conf_set(const u32                      instance,
                              const vtss_appl_mep_pm_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)           return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];
    if (!conf->enable && !data->pm_config.enable)           {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}
    if (!data->config.enable)                               {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (data->config.mode == VTSS_APPL_MEP_MIP)             {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}


    if (conf->enable)   data->pm_config = *conf;
    else                data->pm_config.enable = FALSE;

    vtss_mep_run();

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_pm_conf_get(const u32                 instance,
                              vtss_appl_mep_pm_conf_t   *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    *conf = instance_data[instance].pm_config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_lst_conf_set(const u32                       instance,
                               const vtss_appl_mep_lst_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)           return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];
    if (!conf->enable && !data->lst_config.enable)          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}
    if (!data->config.enable)                               {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (data->config.mode == VTSS_APPL_MEP_MIP)             {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}
    if (data->config.direction != VTSS_APPL_MEP_UP)         {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
    if (data->config.peer_count != 1)                       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_CNT);}
    if (!data->cc_config.enable)                            {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_LST);}
    if (!((data->cc_config.rate == VTSS_APPL_MEP_RATE_300S) ||
          (data->cc_config.rate == VTSS_APPL_MEP_RATE_100S) ||
          (data->cc_config.rate == VTSS_APPL_MEP_RATE_10S) ||
          (data->cc_config.rate == VTSS_APPL_MEP_RATE_1S))) {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_RATE_INTERVAL);}
    if (!data->cc_config.tlv_enable)                        {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_LST);}

    if (conf->enable)   data->lst_config = *conf;
    else                data->lst_config.enable = FALSE;
    data->event_flags |= EVENT_IN_LST_CONFIG;

    vtss_mep_run();

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_lst_conf_get(const u32                  instance,
                               vtss_appl_mep_lst_conf_t   *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    *conf = instance_data[instance].lst_config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_syslog_conf_set(const u32                          instance,
                                  const vtss_appl_mep_syslog_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)           return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];
    if (!conf->enable && !data->syslog_config.enable)       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}
    if (!data->config.enable)                               {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (data->config.mode == VTSS_APPL_MEP_MIP)             {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}


    if (conf->enable)   data->syslog_config = *conf;
    else                data->syslog_config.enable = FALSE;

    vtss_mep_run();

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_syslog_conf_get(const u32                     instance,
                                  vtss_appl_mep_syslog_conf_t   *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    *conf = instance_data[instance].syslog_config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_cc_conf_set(const u32                        instance,
                              const vtss_appl_mep_cc_conf_t    *const conf)
{
    u32                     rc = VTSS_MEP_RC_OK;
    mep_instance_data_t     *data;    /* Instance data reference */
    BOOL                    hw_ccm;

    vtss_mep_trace("vtss_mep_mgmt_cc_conf_set  Enter  instance  enable", instance, conf->enable, 0, 0);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)           return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();
    data = &instance_data[instance];
    if (!conf->enable && !data->cc_config.enable)                                                            {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}
    if (!data->config.enable)                                                                                {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}



    if (data->config.mode == VTSS_APPL_MEP_MIP)                                                              {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}

    if (conf->enable)
    {
        if (!data->lm_config.synthetic && data->lm_config.enable && (data->lm_config.ended == VTSS_APPL_MEP_DUAL_ENDED)) {
           if (conf->rate >  data->lm_config.rate)                                                           {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_LM_DUAL_ENDED_RATE_EXCEED_CC_RATE);}
           // note that the enum is made such highest rate first, then lower and lower rates
        }
        hw_ccm = vtss_mep_supp_hw_ccm_generator_get(supp_rate_calc(conf->rate));
        if (!memcmp(&data->cc_config, conf, sizeof(data->cc_config)))                                        {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}
        if (conf->rate == VTSS_APPL_MEP_RATE_INV)                                                            {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_RATE_INTERVAL);}
        if (conf->prio > 7)                                                                                  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PRIO);}
        if (data->config.peer_count == 0)                                                                    {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_CNT);}
        if ((data->config.peer_count > 1) && hw_ccm)                                                         {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_CNT);}
        if (VTSS_MEP_SUPP_ETREE_ELAN(data->evc_type) && hw_ccm)                                              {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_E_TREE);}
        if (data->lst_config.enable && !conf->tlv_enable)                                                    {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_LST);}
        if (data->lst_config.enable && !((conf->rate == VTSS_APPL_MEP_RATE_300S) || (conf->rate == VTSS_APPL_MEP_RATE_100S) ||
            (conf->rate == VTSS_APPL_MEP_RATE_10S) || (conf->rate == VTSS_APPL_MEP_RATE_1S)))                {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_RATE_INTERVAL);}
        if (vtss_mep_supp_hw_ccm_generator_get(supp_rate_calc(conf->rate)) &&
            (data->config.peer_count > 1))                                                                   {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if (!data->config.voe) {
            if (!mep_caps.appl_afi) {
                if (hw_ccm)                                                                                  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
            }
#ifndef VTSS_SW_OPTION_AFI
            if (hw_ccm)                                                                                      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
#endif
            if (!mep_caps.mesa_mep_serval) {
                if (hw_ccm && (data->config.domain == VTSS_APPL_MEP_EVC) &&
                    (data->config.direction == VTSS_APPL_MEP_UP))                                            {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
            }
        }










    } else {
        if (data->lst_config.enable)                                                                         {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_LST);}
    }
    if (conf->enable)   data->cc_config = *conf;
    else                data->cc_config.enable = FALSE;

    if (!MPLS_DOMAIN(data->config.domain)) {
        if (!pdu_fwr_cap_update(data->rule_vid, data->rule_ports)) {
            vtss_mep_rule_update_failed(data->rule_vid);

            data->cc_config.enable = FALSE;     /* Disable CC */
            (void)pdu_fwr_cap_update(data->rule_vid, data->rule_ports); /* Try 'forward capture update' again */

            rc = VTSS_MEP_RC_TCAM_CONFIGURATION;  /* CC could not be enabled due to TCAM resources */
        }
    }

    run_cc_config(instance);    /* This is now called synchronously */

    vtss_mep_crit_unlock();
    vtss_mep_trace("vtss_mep_mgmt_cc_conf_set  Exit", 0, 0, 0, 0);

    return(rc);
}


u32 vtss_mep_mgmt_cc_conf_get(const u32                 instance,
                              vtss_appl_mep_cc_conf_t   *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    *conf = instance_data[instance].cc_config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}








































































































































































































u32 vtss_mep_mgmt_lm_conf_set(const u32                      instance,
                              const vtss_appl_mep_lm_conf_t  *const conf)
{
    mep_instance_data_t  *data;    /* Instance data reference */
    u32                  peer_idx = 0;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    vtss_mep_lm_trace("vtss_mep_mgmt_lm_conf_set  instance  enable  synthetic", instance, conf->enable, conf->synthetic, 0);

    data = &instance_data[instance];
    if (!data->config.enable)                                                                         {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                                       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}

    if (conf->enable) {
        if (!conf->synthetic && data->cc_config.enable && (conf->ended == VTSS_APPL_MEP_DUAL_ENDED)) {
           if (data->cc_config.rate >  conf->rate)                                                    {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_LM_DUAL_ENDED_RATE_EXCEED_CC_RATE);}
           // note that the enum is made such highest rate first, then lower and lower rates
        }
        if (!memcmp(&data->lm_config, conf, sizeof(data->lm_config)))                                 {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}
        if (conf->rate == VTSS_APPL_MEP_RATE_INV)                                                     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if (conf->prio > 7)                                                                           {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PRIO);}
        if (mep_caps.mesa_mep_luton26) {
            if ((conf->ended == VTSS_APPL_MEP_DUAL_ENDED) && data->cc_config.enable &&
                    vtss_mep_supp_hw_ccm_generator_get(supp_rate_calc(data->cc_config.rate)))         {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        }
        if (data->lm_config.enable && ((conf->ended != data->lm_config.ended) ||
                (conf->size != data->lm_config.size) ||
                (conf->slm_test_id != data->lm_config.slm_test_id) ||
                (conf->synthetic != data->lm_config.synthetic)))  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_CHANGE_PARAMETER);}
        if (conf->synthetic && (supp_meas_interval_calc(conf->rate, conf->meas_interval) == 0))       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MEAS_INTERVAL);}
        if (conf->synthetic && 
                (conf->rate == VTSS_APPL_MEP_RATE_300S || conf->rate == VTSS_APPL_MEP_RATE_100S))     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_RATE_INTERVAL);}
        if (!conf->synthetic) {
            if ((conf->ended == VTSS_APPL_MEP_DUAL_ENDED) &&
                (conf->rate != VTSS_APPL_MEP_RATE_1S) && (conf->rate != VTSS_APPL_MEP_RATE_10S))      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_RATE_INTERVAL);}
            if ((conf->ended == VTSS_APPL_MEP_SINGEL_ENDED) &&
                (conf->rate != VTSS_APPL_MEP_RATE_6M) && (conf->rate != VTSS_APPL_MEP_RATE_1S) &&
                (conf->rate != VTSS_APPL_MEP_RATE_10S))                                               {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_RATE_INTERVAL);}
        }
        if (!conf->synthetic && (data->config.peer_count != 1))                                       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_CNT);}
        if ((conf->cast == VTSS_APPL_MEP_UNICAST) && (conf->mep > VTSS_APPL_MEP_ID_MAX) &&
            (data->config.peer_count != 1))                                                           {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_CNT);}
        if (!conf->synthetic && (data->config.domain == VTSS_APPL_MEP_VLAN))                          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_VLAN_SUPPORT);}
        peer_mac_update(data);

        if (!MPLS_DOMAIN(data->config.domain)) {
            if (conf->cast == VTSS_APPL_MEP_UNICAST) {
                for (peer_idx=0; peer_idx<data->config.peer_count; ++peer_idx)
                    if (conf->mep == data->config.peer_mep[peer_idx]) break;
                if (peer_idx == data->config.peer_count)                                              {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_ID);}
                if (peer_mep_is_local(data, conf->mep))                                               {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_ID);}
                if (!memcmp(all_zero_mac, data->config.peer_mac[peer_idx].addr, VTSS_APPL_MEP_MAC_LENGTH)){vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_MAC);}
            }
            if (conf->synthetic) {  /* Validate frame size */
                /* The MAX frame size is four/eight byte less in case of adding TAG */
                u32 max_size = max_size_calc(data, VTSS_APPL_MEP_SLM_SIZE_MAX);
                if (conf->size > max_size)                                                                {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                if (conf->size < VTSS_APPL_MEP_TST_SIZE_MIN)                                              {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
            }
        }











        data->los_int_cnt_timer = 0;
        data->los_th_cnt_timer  = 0;

        vtss_mep_mgmt_lm_notif_set(instance, peer_idx);

        data->los_int_cnt_timer = 1000 * conf->los_int_cnt_holddown / timer_res;
        data->los_th_cnt_timer  = 1000 * conf->los_th_cnt_holddown  / timer_res;
        if (data->los_int_cnt_timer > 0 || data->los_th_cnt_timer > 0) {
            vtss_mep_timer_start();
        }
    } else {
        vtss_mep_mgmt_lm_notif_set(instance, peer_idx);
    }

    if (!conf->enable && !data->lm_config.enable &&         /* TX is not enabled and no change in TX enable */
        conf->enable_rx && data->lm_config.enable_rx &&     /* No change in 'enable_rx' - check for no reconfigure */
        (conf->flr_interval == data->lm_config.flr_interval) &&
        (conf->meas_interval == data->lm_config.meas_interval) &&
        (conf->loss_th == data->lm_config.loss_th)) {

        if (conf->synthetic &&     /* Synthetic -> synthetic: one of the following must change to reconfigure */
            (conf->rate == data->lm_config.rate) &&
            (conf->meas_interval == data->lm_config.meas_interval))         {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}

        if (!conf->synthetic)     /* Not synthetic -> not synthetic: one of the following must change to reconfigure */
                                                                            {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}
    }

    // control counter clear action in run_lm_config
    data->lm_state.clear_counters = FALSE;
    if (conf->enable && !data->lm_config.enable) {
        // we are enabling TX - always clear TX counters
        data->lm_state.clear_counters = TRUE;

        if (conf->ended == VTSS_APPL_MEP_DUAL_ENDED && data->lm_config.enable_rx) {
            // AA-9962: Don't clear RX when enabling TX on dual-ended when RX is already enabled
            T_I("Dual with RX enabled - Only TX clear");
            data->lm_state.clear_dir = VTSS_APPL_MEP_CLEAR_DIR_TX;
        } else {
            T_I("enable TX without RX - Clear all");
            data->lm_state.clear_dir = VTSS_APPL_MEP_CLEAR_DIR_BOTH;
        }
    }
    if (conf->enable_rx && !data->lm_config.enable_rx && !conf->enable) {
        // We are enabling RX without TX
        T_I("enabling RX without TX - Clear all");
        data->lm_state.clear_counters = TRUE;
        data->lm_state.clear_dir = VTSS_APPL_MEP_CLEAR_DIR_BOTH;
    }
    if (!conf->enable && data->lm_config.enable && conf->ended == VTSS_APPL_MEP_SINGEL_ENDED && conf->enable_rx) {
        // We are disabling a single-ended initiator and RX is still enabled
        // (as the RX counter suddenly counts SLM or 1SL PDUs instead of SLR PDUs)
        T_I("disable TX with RX enabled - Clear all");
        data->lm_state.clear_counters = TRUE;
        data->lm_state.clear_dir = VTSS_APPL_MEP_CLEAR_DIR_BOTH;
    }

    data->lm_config = *conf;
    data->event_flags |= EVENT_IN_LM_CONFIG;

    vtss_mep_run();

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_lm_conf_get(const u32                instance,
                             vtss_appl_mep_lm_conf_t   *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    *conf = instance_data[instance].lm_config;

    if (!conf->synthetic) {
        switch (conf->rate)
        {
            case VTSS_APPL_MEP_RATE_100S:    conf->meas_interval = 10;     break;
            case VTSS_APPL_MEP_RATE_10S:     conf->meas_interval = 100;    break;
            case VTSS_APPL_MEP_RATE_1S:      conf->meas_interval = 1000;   break;
            case VTSS_APPL_MEP_RATE_6M:      conf->meas_interval = 10000;  break;
            case VTSS_APPL_MEP_RATE_1M:      conf->meas_interval = 60000;  break;
            case VTSS_APPL_MEP_RATE_6H:      conf->meas_interval = 600000; break;
            default:                         conf->meas_interval = 0;
        }
    }

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

static void vtss_mep_lm_avail_clear_peer_states(out_state_t *state)
{
    for (int peer_idx = 0; peer_idx < mep_caps.mesa_oam_peer_cnt; peer_idx++) {
        state->lm_avail_state[peer_idx].near_state  = VTSS_APPL_MEP_OAM_LM_AVAIL;
        state->lm_avail_state[peer_idx].far_state   = VTSS_APPL_MEP_OAM_LM_AVAIL;

        state->lm_avail_state[peer_idx].near_cnt    = 0;
        state->lm_avail_state[peer_idx].far_cnt     = 0;
        state->lm_avail_state[peer_idx].near_un_cnt = 0;
        state->lm_avail_state[peer_idx].far_un_cnt  = 0;

        state->lm_avail_state[peer_idx].far_avail_cnt = 0;
        state->lm_avail_state[peer_idx].near_avail_cnt = 0;
    }
}

u32 vtss_mep_mgmt_lm_avail_conf_set(const u32                            instance,
                                    const vtss_appl_mep_lm_avail_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];
    if (!data->config.enable)                                                                         {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                                       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}

    if (conf->enable) {

        BOOL was_enabled = data->lm_avail_config.enable;

        if (!memcmp(&data->lm_avail_config, conf, sizeof(data->lm_avail_config)))                     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}
        if (conf->flr_th > MEP_AVAIL_FLR_TH_MAX)                                                      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if (conf->interval < MEP_AVAIL_INTERVAL_MIN || conf->interval > MEP_AVAIL_INTERVAL_MAX)       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if (conf->interval != data->lm_avail_config.interval &&
            data->lm_config.enable)                                                                   {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_CHANGE_PARAMETER);}

        if (!was_enabled) {
            // clear initial avail states
            vtss_mep_lm_avail_clear_peer_states(&data->out_state);
        }

        if (conf->interval != data->lm_avail_config.interval || !data->lm_avail_fifo) {

            clear_lm_avail_fifo(instance);

            if (conf->interval) {
                data->lm_avail_fifo = (vtss_mep_supp_lm_peer_counters_t *)VTSS_CALLOC(conf->interval, sizeof(data->lm_avail_fifo[0]));
                if (!data->lm_avail_fifo)                                                             {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
            }
        }
        //vtss_mep_mgmt_lm_notif_set(instance);
    } else {

        vtss_mep_lm_avail_clear_peer_states(&data->out_state);

        clear_lm_avail_fifo(instance);
    }

    data->lm_avail_config = *conf;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_lm_avail_conf_get(const u32                      instance,
                                    vtss_appl_mep_lm_avail_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    *conf = instance_data[instance].lm_avail_config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_lm_hli_conf_set(const u32                          instance,
                                  const vtss_appl_mep_lm_hli_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];
    if (!data->config.enable)                                                                         {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                                       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}

    if (conf->enable) {
        if (!memcmp(&data->lm_hli_config, conf, sizeof(data->lm_hli_config)))                         {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}
        if (conf->flr_th > 1000)                                                                      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        data->hli_cnt_timer = 1000 * conf->cnt_holddown  / timer_res;
        for (u32 peer_index = 0; peer_index < data->config.peer_count; peer_index++) {
            vtss_mep_lm_hli_status_set(instance, data->config.peer_mep[peer_index], &data->out_state.lm_hli_state[peer_index]);
        }
        if (data->hli_cnt_timer > 0) {
            vtss_mep_timer_start();
        }
    } else {
        for (u32 peer_index = 0; peer_index < data->config.peer_count; peer_index++) {
            data->out_state.lm_hli_state[peer_index].near_cnt     = 0;
            data->out_state.lm_hli_state[peer_index].far_cnt      = 0;
            data->out_state.lm_hli_state[peer_index].near_con_cnt = 0;
            data->out_state.lm_hli_state[peer_index].far_con_cnt  = 0;
            data->lm_hli_state[peer_index].near_cnt               = 0;
            data->lm_hli_state[peer_index].far_cnt                = 0;
            vtss_mep_lm_hli_status_set(instance, data->config.peer_mep[peer_index], NULL);
        }
    }

    data->lm_hli_config = *conf;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_lm_hli_conf_get(const u32                    instance,
                                  vtss_appl_mep_lm_hli_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    *conf = instance_data[instance].lm_hli_config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_lm_sdeg_conf_set(const u32                           instance,
                                   const vtss_appl_mep_lm_sdeg_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];
    if (!data->config.enable)                                                                         {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                                       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}

    if (conf->enable) {
        if (!memcmp(&data->lm_sdeg_config, conf, sizeof(data->lm_sdeg_config)))                       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}
        if (conf->flr_th > 1000)                                                                      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if (data->config.peer_count != 1)                                                             {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_CNT);}
    } else {
        data->lm_sdeg_bad      = FALSE;
        data->lm_sdeg_bad_cnt  = 0;
        data->lm_sdeg_good_cnt = 0;
        if (data->dDeg) {
            data->dDeg = FALSE;
            data->event_flags |= EVENT_IN_SSF_SSD_STATE;
            vtss_mep_run();
        }
    }

    data->lm_sdeg_config = *conf;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_lm_sdeg_conf_get(const u32                     instance,
                                   vtss_appl_mep_lm_sdeg_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    *conf = instance_data[instance].lm_sdeg_config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_dm_conf_set(const u32                      instance,
                              const vtss_mep_mgmt_dm_conf_t  *const conf)
{
    u8                              i;
    mep_instance_data_t             *data;    /* Instance data reference */
    BOOL                            enable = FALSE;
    BOOL                            cos_id[VTSS_MEP_COS_ID_SIZE];
    const vtss_appl_mep_dm_common_conf_t  *common;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)              return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];
    memset(cos_id, 0, sizeof(cos_id));

    if (!data->config.enable)                                                                   {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                                 {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}
    if (!memcmp(&data->dm_config, conf, sizeof(data->dm_config)))                               {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}









    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        if (conf->dm[i].enable) {
            enable = TRUE;





            if (data->sat && (data->sat_prio_conf[i].vid == VTSS_MEP_MGMT_SAT_VID_UNUSED))      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        }
    }

    vtss_mep_trace("vtss_mep_mgmt_dm_conf_set  instance  enable  tunit", instance, enable, conf->common.tunit, 0);

    if (enable)
    {
        common = &conf->common;
        if (data->config.peer_count == 0)                                                       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_CNT);}
        if (common->cast == VTSS_APPL_MEP_UNICAST && !MPLS_DOMAIN(data->config.domain))
        {/* All zero mac - mep must be a known peer mep */
            peer_mac_update(data);
            for (i=0; i<data->config.peer_count; ++i)
                if (common->mep == data->config.peer_mep[i]) break;
            if (i == data->config.peer_count)                                                   {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_ID);}
            if (peer_mep_is_local(data, common->mep))                                           {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_ID);}
            if (!MPLS_DOMAIN(data->config.domain)) {
                if (!memcmp(all_zero_mac, data->config.peer_mac[i].addr, VTSS_APPL_MEP_MAC_LENGTH)) {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_MAC);}
            }
        }
        if ((common->ended == VTSS_APPL_MEP_SINGEL_ENDED) &&
            (common->proprietary) &&
            (common->calcway == VTSS_APPL_MEP_RDTRP))                                                                  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if (dm_enable_calc(data->dm_config.dm) && (common->ended != data->dm_config.common.ended))                     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_CHANGE_PARAMETER);}
        if ((common->interval < VTSS_APPL_MEP_DM_INTERVAL_MIN) || (common->interval > VTSS_APPL_MEP_DM_INTERVAL_MAX))  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_RATE_INTERVAL);}
        if ((common->lastn < VTSS_APPL_MEP_DM_LASTN_MIN) || (common->lastn > VTSS_APPL_MEP_DM_LASTN_MAX))              {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if ((common->tunit != VTSS_APPL_MEP_US) && (common->tunit != VTSS_APPL_MEP_NS))                                {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if ((common->overflow_act != VTSS_APPL_MEP_DISABLE) && (common->overflow_act != VTSS_APPL_MEP_CONTINUE))       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if (mep_caps.mesa_mep_serval) {
            if (common->proprietary)                                                                                   {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PROP_SUPPORT);}
        }
        if ((common->num_of_bin_fd < VTSS_APPL_MEP_DM_BINS_FD_MIN) || (common->num_of_bin_fd > VTSS_APPL_MEP_DM_BINS_FD_MAX))          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if ((common->num_of_bin_ifdv < VTSS_APPL_MEP_DM_BINS_IFDV_MIN) || (common->num_of_bin_ifdv > VTSS_APPL_MEP_DM_BINS_IFDV_MAX))  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if (common->m_threshold == 0)                                                                                                  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
    }

    data->dm_config = *conf;
    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {	/* The configured 'tunit' must go into the 'out_state' on all priorities. Do actually not know why !!! */
        data->out_state.tw_state[i].tunit = conf->common.tunit;
        data->out_state.fn_state[i].tunit = conf->common.tunit;
        data->out_state.nf_state[i].tunit = conf->common.tunit;
    }

    data->event_flags |= EVENT_IN_DM_CONFIG;

    vtss_mep_run();

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_dm_conf_get(const u32                instance,
                              vtss_mep_mgmt_dm_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)               return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    vtss_mep_trace("vtss_mep_mgmt_dm_conf_get  instance  tunit", instance, instance_data[instance].dm_config.common.tunit, 0, 0);

    *conf = instance_data[instance].dm_config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_aps_conf_set(const u32                       instance,
                               const vtss_appl_mep_aps_conf_t  *const conf)
{
    u32                     rc = VTSS_MEP_RC_OK;
    mep_instance_data_t     *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];
    if (!conf->enable && !data->aps_config.enable)                                          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}
    if (!data->config.enable)                                                               {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                             {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}
    if ((conf->type == VTSS_APPL_MEP_R_APS) && (data->config.domain == VTSS_APPL_MEP_EVC))  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_APS_DOMAIN);}
    if ((conf->type == VTSS_APPL_MEP_R_APS) && (data->config.domain == VTSS_APPL_MEP_PORT) &&
        (data->config.vid == 0))                                                            {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_VID);}
    if ((conf->type == VTSS_APPL_MEP_L_APS) && (data->config.domain == VTSS_APPL_MEP_EVC) &&
        (data->config.direction == VTSS_APPL_MEP_UP))                                       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_APS_UP);}

    if (conf->enable)
    {
        if (!memcmp(&data->aps_config, conf, sizeof(data->aps_config)))                     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}
        if (conf->prio > 7)                                                                 {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PRIO);}
    }

    if (conf->enable)   data->aps_config = *conf;
    else                data->aps_config.enable = FALSE;

    if ((rc = run_aps_config(instance)) != VTSS_MEP_RC_OK) {   /* This is now called synchronously */
        data->aps_config.enable = FALSE;
        (void)run_aps_config(instance);
    }
    vtss_mep_crit_unlock();

    return(rc);
}


u32 vtss_mep_mgmt_aps_conf_get(const u32                 instance,
                               vtss_appl_mep_aps_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    *conf = instance_data[instance].aps_config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_lt_conf_set(const u32                      instance,
                              const vtss_appl_mep_lt_conf_t  *const conf)
{
    u32                   i;
    mep_instance_data_t   *data;    /* Instance data reference */

    vtss_mep_trace("vtss_mep_mgmt_lt_conf_set  instance  enable  transactions", instance, conf->enable, conf->transactions, 0);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)         return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];
    if (!conf->enable && !data->lt_config.enable)                       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}
    if (!data->config.enable)                                           {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (data->config.mode == VTSS_APPL_MEP_MIP)                         {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}

    /* link trace not supported on MPLS: */
    if (MPLS_DOMAIN(data->config.domain))                               {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}

    if (conf->enable)
    {
        if (data->lt_config.enable)                                     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_CHANGE_PARAMETER);}
        if (conf->ttl > 0xFF)                                           {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if (conf->prio > 7)                                             {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PRIO);}
        if (data->config.peer_count == 0)                               {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_CNT);}
        if ((data->lt_config.transactions == 0) ||
            (data->lt_config.transactions > VTSS_APPL_MEP_TRANSACTION_MAX))  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if (!memcmp(all_zero_mac, conf->mac.addr, VTSS_APPL_MEP_MAC_LENGTH))
        {/* All zero mac - mep must be a known peer mep */
            peer_mac_update(data);
            for (i=0; i<data->config.peer_count; ++i)
                if (conf->mep == data->config.peer_mep[i]) break;
            if (i == data->config.peer_count)                                                      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_ID);}
            if (peer_mep_is_local(data, conf->mep))                                                {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_ID);}
            if (!memcmp(all_zero_mac, data->config.peer_mac[i].addr, VTSS_APPL_MEP_MAC_LENGTH))    {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_MAC);}
            for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i) {  /* Check if the peer MEP is in this node */
                if ((i != instance) && (data->config.domain == instance_data[i].config.domain) && (data->config.domain != VTSS_APPL_MEP_PORT) &&
                    (data->config.flow == instance_data[i].config.flow) && (conf->mep == instance_data[i].config.mep)) break;
            }
            if (i < VTSS_APPL_MEP_INSTANCE_MAX)                                                    {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_ID);}
        }










    }

    if (conf->enable)   data->lt_config = *conf;
    else                data->lt_config.enable = FALSE;
    data->event_flags |= EVENT_IN_LT_CONFIG;

    vtss_mep_run();

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_lt_conf_get(const u32                 instance,
                               vtss_appl_mep_lt_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    *conf = instance_data[instance].lt_config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

/*
 * Calculate the maximum frame size usable for LB and TST
 */
static u32 max_size_lb_tst_calc(const mep_instance_data_t *data, u32 max_size, u32 prio)
{
    switch (data->config.domain) {
        case VTSS_APPL_MEP_PORT:
            return data->config.vid != 0 ? max_size - 4 : max_size;
        case VTSS_APPL_MEP_VLAN:
            return max_size - 4;
        case VTSS_APPL_MEP_EVC:
            if (data->sat) {
                // Use SAT VID for check
                return data->sat_prio_conf[prio].vid == VTSS_MEP_MGMT_SAT_VID_UNTAGGED ? max_size : max_size - 4;
            } else {
                // Use normal configured VID for check
                return data->config.vid == 0 ? max_size - 4 : max_size - 8;
            }
        default:
            return max_size;
    }

    // Default return same size
    return max_size;
}


u32 vtss_mep_mgmt_lb_conf_set(const u32                      instance,
                              const vtss_mep_mgmt_lb_conf_t  *const conf,
                                    u64                      *active_time_ms)
{
    u32                   i, j, rc;
    mep_instance_data_t   *data;    /* Instance data reference */
    BOOL                  enable = FALSE;
    BOOL                  cos_id[VTSS_MEP_COS_ID_SIZE];
    vtss_mep_mgmt_lb_conf_t old_lb_config;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)         return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];
    memset(cos_id, 0, sizeof(cos_id));

    if (!data->config.enable)                                                                 {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                               {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}
    if (test_enable_calc(conf->tests) && (test_enable_calc(data->tst_config.tests) || data->tst_config.common.enable_rx)) {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_TEST);}









    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
        /* The actual MAX frame size may be less if one or more tag are added */
        u32 max_frame_size = max_size_lb_tst_calc(data, VTSS_APPL_MEP_LBM_SIZE_MAX, i);
        u32 max_cpu_frame_size = max_size_lb_tst_calc(data, mep_caps.appl_mep_packet_rx_mtu, i);

        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            if (conf->tests[i][j].enable) {
                enable = TRUE;
                if (MPLS_DOMAIN(data->config.domain)) {
                    if (conf->tests[i][j].size > VTSS_APPL_MEP_LBM_MPLS_SIZE_MAX) {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                } else {
                    if (conf->common.to_send > 0) {
                        // Packets need to be copied to CPU
                        if (conf->tests[i][j].size > max_cpu_frame_size)          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                    } else {
                        // Packets will just be counted by HW
                       if (conf->tests[i][j].size > max_frame_size)               {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                    }
                }
                if (conf->tests[i][j].size < VTSS_APPL_MEP_LBM_SIZE_MIN)          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                if (conf->tests[i][j].rate > VTSS_APPL_MEP_TST_RATE_MAX)          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                if (conf->tests[i][j].rate < 1)                                   {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}





                if (data->sat && (data->sat_prio_conf[i].vid == VTSS_MEP_MGMT_SAT_VID_UNUSED))     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
            }
        }
    }

    vtss_mep_trace("vtss_mep_mgmt_lb_conf_set  instance  enable", instance, enable, 0, 0);

    if (enable)
    {
        if (test_enable_calc(data->lb_config.tests) && (data->lb_config.common.to_send != 0))                    {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_CHANGE_PARAMETER);}
        if (test_enable_calc(data->lb_config.tests) && (data->lb_config.common.to_send != conf->common.to_send)) {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if ((conf->common.to_send != VTSS_APPL_MEP_LB_TO_SEND_INFINITE) && (conf->common.to_send == 0))          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if ((conf->common.to_send == VTSS_APPL_MEP_LB_TO_SEND_INFINITE) && !data->config.voe)                    {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NO_VOE);}
        if ((conf->common.to_send != VTSS_APPL_MEP_LB_TO_SEND_INFINITE) && (conf->common.interval > 100))        {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_RATE_INTERVAL);}
        if (conf->common.cast == VTSS_APPL_MEP_UNICAST)
        if (conf->common.cast == VTSS_APPL_MEP_UNICAST && !MPLS_DOMAIN(data->config.domain))
        {
            if (!memcmp(all_zero_mac, conf->common.mac.addr, VTSS_APPL_MEP_MAC_LENGTH))
            {/* All zero mac - mep must be a known peer mep */
                peer_mac_update(data);
                for (i=0; i<data->config.peer_count; ++i)
                    if (conf->common.mep == data->config.peer_mep[i]) break;
                if (i == data->config.peer_count)                                                       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_ID);}
                if (!memcmp(all_zero_mac, data->config.peer_mac[i].addr, VTSS_APPL_MEP_MAC_LENGTH))     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_MAC);}
            }
        }
    }

    old_lb_config = data->lb_config;
    data->lb_config = *conf;

    rc = run_lb_config(instance, active_time_ms);

    if (rc != VTSS_MEP_RC_OK) {
        data->lb_config = old_lb_config;
        (void)run_lb_config(instance, NULL);
    }

    vtss_mep_crit_unlock();

    return(rc);
}


u32 vtss_mep_mgmt_lb_conf_get(const u32                 instance,
                              vtss_mep_mgmt_lb_conf_t   *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    *conf = instance_data[instance].lb_config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_ais_conf_set(const u32                       instance,
                               const vtss_appl_mep_ais_conf_t  *const conf)
{
    u32                  i;
    mep_instance_data_t  *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];
    if (!conf->enable && !data->ais_config.enable)                                                       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}
    if (!data->config.enable)                                                                            {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                                          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}
    if (conf->enable) {
        if (VTSS_MEP_SUPP_ETREE(data->evc_type))                                                         {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_E_TREE);}
        if (!data->client_config.flow_count)                                                             {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if ((data->config.domain == VTSS_APPL_MEP_EVC) && (data->config.direction == VTSS_APPL_MEP_UP))  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if ((conf->rate != VTSS_APPL_MEP_RATE_1S) && (conf->rate != VTSS_APPL_MEP_RATE_1M))              {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_RATE_INTERVAL);}
        for (i=0; i<data->client_config.flow_count; ++i) {
            if ((data->client_config.domain[i] != VTSS_APPL_MEP_MPLS_LSP) &&
                (data->client_config.domain[i] != VTSS_APPL_MEP_EVC) &&
                (data->client_config.domain[i] != VTSS_APPL_MEP_VLAN))                            {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        }
    }

    data->ais_config = *conf;

    data->event_flags |= EVENT_IN_AIS_CONFIG;

    vtss_mep_run();

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_ais_conf_get(const u32                 instance,
                               vtss_appl_mep_ais_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    *conf = instance_data[instance].ais_config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_lck_conf_set(const u32                       instance,
                               const vtss_appl_mep_lck_conf_t  *const conf)
{
    u32                  i;
    mep_instance_data_t  *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];
    if (!conf->enable && !data->lck_config.enable)                                                       {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}
    if (!data->config.enable)                                                                            {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                                          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}
    if (conf->enable) {
        if (VTSS_MEP_SUPP_ETREE(data->evc_type))                                                         {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_E_TREE);}
        if (!data->client_config.flow_count)                                                             {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if ((data->config.domain == VTSS_APPL_MEP_EVC) && (data->config.direction == VTSS_APPL_MEP_UP))  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if ((conf->rate != VTSS_APPL_MEP_RATE_1S) && (conf->rate != VTSS_APPL_MEP_RATE_1M))              {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_RATE_INTERVAL);}
        for (i=0; i<data->client_config.flow_count; ++i) {
            if ((data->client_config.domain[i] != VTSS_APPL_MEP_MPLS_LSP) &&
                (data->client_config.domain[i] != VTSS_APPL_MEP_EVC) &&
                (data->client_config.domain[i] != VTSS_APPL_MEP_VLAN))                            {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        }
    }

    data->lck_config = *conf;

    data->event_flags |= EVENT_IN_LCK_CONFIG;

    vtss_mep_run();

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_lck_conf_get(const u32                 instance,
                               vtss_appl_mep_lck_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    *conf = instance_data[instance].lck_config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_tst_conf_set(const u32                       instance,
                               const vtss_mep_mgmt_tst_conf_t  *const conf,
                                     u64                       *active_time_ms)
{
    mep_instance_data_t         *data;    /* Instance data reference */
    BOOL                        cos_id[VTSS_MEP_COS_ID_SIZE];
    vtss_mep_mgmt_tst_conf_t    old_tst_config;
    u32                         rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];
    memset(cos_id, 0, sizeof(cos_id));

    if (!data->config.enable)                                                     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                   {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}
    if (test_enable_calc(data->lb_config.tests) && (test_enable_calc(conf->tests) || conf->common.enable_rx)) {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_TEST);}

    if (mep_caps.appl_afi) {
        u32 i, j;
        BOOL enable = FALSE;









        for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
            /* The actual MAX frame size may be less if one or more tag are added */
            u32 max_size = max_size_lb_tst_calc(data, VTSS_APPL_MEP_TST_SIZE_MAX, i);

            for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
                if (conf->tests[i][j].enable) {
                    enable = TRUE;
                }
                if (MPLS_DOMAIN(data->config.domain)) {
                    if (conf->tests[i][j].size > VTSS_APPL_MEP_TST_MPLS_SIZE_MAX) {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                } else {
                    if (conf->tests[i][j].size > max_size)                        {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                }
                if (conf->tests[i][j].size < VTSS_APPL_MEP_TST_SIZE_MIN)          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                if (conf->tests[i][j].rate > VTSS_APPL_MEP_TST_RATE_MAX)          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
                if (conf->tests[i][j].rate < 1)                                   {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}





                if (conf->tests[i][j].enable && data->sat &&
                    (data->sat_prio_conf[i].vid == VTSS_MEP_MGMT_SAT_VID_UNUSED)) {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
            }
        }

        vtss_mep_trace("vtss_mep_mgmt_tst_conf_set  instance  enable", instance, enable, 0, 0);

        if (enable)
        {
            if (!memcmp(&data->tst_config, conf, sizeof(data->tst_config)))                  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}
            for (i=0; i<data->config.peer_count; ++i)
                if (conf->common.mep == data->config.peer_mep[i]) break;
            if (i == data->config.peer_count)                                                {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_ID);}
        if (peer_mep_is_local(data, conf->common.mep))                                   {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_ID);}
            peer_mac_update(data);
            if (!MPLS_DOMAIN(data->config.domain)) {
                if (!memcmp(all_zero_mac, data->config.peer_mac[i].addr, sizeof(all_zero_mac)))  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_MAC);}
            }
        }

        old_tst_config = data->tst_config;
        data->tst_config = *conf;
        rc = run_tst_config(instance, active_time_ms);
        if (rc != VTSS_RC_OK) {
            /* config failed, so restore configuration */
            data->tst_config = old_tst_config;
            (void)run_tst_config(instance, NULL);
        }

        vtss_mep_crit_unlock();

        return(rc);
    } else { /* No AFI support */
        vtss_mep_crit_unlock();
        return(VTSS_MEP_RC_INVALID_PARAMETER);
    }
}

u32 vtss_mep_mgmt_tst_conf_get(const u32                 instance,
                               vtss_mep_mgmt_tst_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    *conf = instance_data[instance].tst_config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_client_conf_set(const u32                 instance,
                                  const mep_client_conf_t  *const conf)
{
    mep_instance_data_t     *data;    /* Instance data reference */
    u32 i;
    BOOL has_evc = FALSE, has_vlan = FALSE;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];

    if (!mep_caps.mesa_mpls && MPLS_DOMAIN(data->config.domain))                                     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}

    if (!data->config.enable)                                                                        {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (data->config.mode == VTSS_APPL_MEP_MIP)                                                      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}
    if (conf->flow_count > VTSS_APPL_MEP_CLIENT_FLOWS_MAX)                                           {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
    if ((conf->flow_count == 0) && (data->ais_config.enable || data->lck_config.enable))             {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
    if (!MPLS_DOMAIN(data->config.domain) && conf->flow_count) {
        if ((data->config.domain != VTSS_APPL_MEP_PORT) && (conf->flow_count != 1))                  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if ((data->config.domain != VTSS_APPL_MEP_PORT) && (data->config.flow != conf->flows[0]))    {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if ((data->config.domain != VTSS_APPL_MEP_PORT) && (data->config.level == 7))                {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_CLIENT_MAX_LEVEL);}
    }
    for (i=0; i<conf->flow_count; i++) {
        if (conf->level[i] > 7)                                                                      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_CLIENT_LEVEL);}
        if (!MPLS_DOMAIN(data->config.domain) && (data->config.domain != VTSS_APPL_MEP_PORT) &&
            conf->level[i] <= data->config.level)                                                    {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_CLIENT_LEVEL);}
        if ((conf->ais_prio[i] != VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST) && (conf->ais_prio[i] > 7))     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PRIO);}
        if ((conf->lck_prio[i] != VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST) && (conf->lck_prio[i] > 7))     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PRIO);}


































        if ((conf->ais_prio[i] != VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST) && (conf->ais_prio[i] > 7))      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_COS_ID);}
        if ((conf->lck_prio[i] != VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST) && (conf->lck_prio[i] > 7))      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_COS_ID);}

        if (conf->domain[i] != VTSS_APPL_MEP_EVC &&
            conf->domain[i] != VTSS_APPL_MEP_VLAN &&
            conf->domain[i] != VTSS_APPL_MEP_MPLS_LSP)                                                    {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if (conf->domain[i] == VTSS_APPL_MEP_EVC) {
            has_evc = TRUE;
        } else if (conf->domain[i] == VTSS_APPL_MEP_VLAN) {
            has_vlan = TRUE;
        }
        if (!MPLS_DOMAIN(data->config.domain) && has_evc && has_vlan)                                     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if (MPLS_DOMAIN(data->config.domain) &&
            conf->domain[i] == VTSS_APPL_MEP_VLAN)                                                        {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
        if (conf->domain[i] == VTSS_APPL_MEP_MPLS_LSP &&
            data->config.domain != VTSS_APPL_MEP_MPLS_TUNNEL &&
            data->config.domain != VTSS_APPL_MEP_MPLS_LINK)                                               {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
    }

    data->client_config = *conf;

    data->event_flags |= EVENT_IN_AIS_CONFIG;
    data->event_flags |= EVENT_IN_LCK_CONFIG;

    vtss_mep_run();

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_client_conf_get(const u32           instance,
                                  mep_client_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    *conf = instance_data[instance].client_config;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_state_get(const u32                instance,
                            vtss_mep_mgmt_state_t    *const state)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)        {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}

    *state = instance_data[instance].out_state.state;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

mesa_rc vtss_mep_mgmt_cc_state_get(const u32                    instance,
                                   vtss_appl_mep_cc_state_t     *const state)
{
    vtss_mep_supp_ccm_state_t     supp_state;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)        {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}

    vtss_mep_crit_unlock();

    memset(state, 0, sizeof(*state));

    if (vtss_mep_supp_ccm_state_get(instance,  &supp_state) == VTSS_MEP_SUPP_RC_OK) {
        memcpy(state->unexp_name, supp_state.unexp_name, sizeof(state->unexp_name));
        memcpy(state->unexp_meg, supp_state.unexp_meg, sizeof(state->unexp_meg));
        memcpy(state->ps_tlv, supp_state.ps_tlv, sizeof(state->ps_tlv));
        memcpy(state->is_tlv, supp_state.is_tlv, sizeof(state->is_tlv));
        memcpy(state->os_tlv, supp_state.os_tlv, sizeof(state->os_tlv));
        memcpy(state->ps_received, supp_state.ps_received, sizeof(state->ps_received));
        memcpy(state->is_received, supp_state.is_received, sizeof(state->is_received));
        memcpy(state->os_received, supp_state.os_received, sizeof(state->os_received));
    } else
       vtss_mep_trace("vtss_mep_mgmt_ccm_state_get: Call to supp failed", instance, 0, 0, 0);

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_lm_state_get(const u32                   instance,
                               const u32                   peer,
                               vtss_appl_mep_lm_state_t    *const state)
{
    u32 peer_idx;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)                     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (!get_peer_index(&instance_data[instance], peer, peer_idx))  {vtss_mep_crit_unlock(); return VTSS_MEP_RC_PEER_ID;}

    *state = instance_data[instance].out_state.lm_state[peer_idx];
    if (state->near_los_tot_cnt < 0) state->near_los_tot_cnt = 0;
    if (state->far_los_tot_cnt  < 0) state->far_los_tot_cnt  = 0;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_lm_avail_state_get(const u32                        instance,
                                     const u32                        peer,
                                     vtss_appl_mep_lm_avail_state_t   *const state)
{
    u32 i;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable || !instance_data[instance].lm_avail_config.enable)     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}

    for (i = 0; i < instance_data[instance].config.peer_count; ++i) {/* Search for peer MEP-ID */
        if (instance_data[instance].config.peer_mep[i] == peer)
            break;
    }
    if (i == instance_data[instance].config.peer_count)   {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_PEER_ID);}

    *state = instance_data[instance].out_state.lm_avail_state[i];

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_lm_hli_state_get(const u32                      instance,
                                   const u32                      peer_id,
                                   vtss_appl_mep_lm_hli_state_t   *const state)
{
    u32 peer_index;
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (!get_peer_index(&instance_data[instance], peer_id, peer_index)) {vtss_mep_crit_unlock(); return(VTSS_APPL_MEP_RC_PEER_ID);}

    *state = instance_data[instance].out_state.lm_hli_state[peer_index];

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_dm_timestamp_get(const u32           instance,
                                   mep_dm_timestamp_t  *const dm1_timestamp_far_to_near,
                                   mep_dm_timestamp_t  *const dm1_timestamp_near_to_far)
{
    u32 ret_val = VTSS_MEP_RC_OK;
    mep_supp_dm_timestamp_t  far_to_near;
    mep_supp_dm_timestamp_t  near_to_far;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}

    if (VTSS_MEP_SUPP_RC_OK != vtss_mep_supp_dmr_timestamps_get(instance, &far_to_near, &near_to_far)) {
        ret_val = VTSS_MEP_RC_NO_TIMESTAMP_DATA;
    } else {
        dm1_timestamp_far_to_near->tx_time = far_to_near.tx_time;
        dm1_timestamp_far_to_near->rx_time = far_to_near.rx_time;
        dm1_timestamp_near_to_far->tx_time = near_to_far.tx_time;
        dm1_timestamp_near_to_far->rx_time = near_to_far.rx_time;
    }

    vtss_mep_crit_unlock();

    return(ret_val);
}

u32 vtss_mep_mgmt_dm_prio_state_get(const u32                   instance,
                                    const u32                   prio,
                                    vtss_appl_mep_dm_state_t    *const tw_state,
                                    vtss_appl_mep_dm_state_t    *const fn_state,
                                    vtss_appl_mep_dm_state_t    *const nf_state)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)     {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}

    vtss_mep_trace("vtss_mep_mgmt_dm_prio_state_get  instance  prio  tw-tunit  fn-tunit", instance, prio, instance_data[instance].out_state.tw_state[prio].tunit, instance_data[instance].out_state.fn_state[prio].tunit);

    memcpy(tw_state, &instance_data[instance].out_state.tw_state[prio], sizeof(*tw_state));
    if (tw_state->min_delay == 0xffffffff)
        tw_state->min_delay = 0;
    if (tw_state->min_delay_var == 0xffffffff)
        tw_state->min_delay_var = 0;

    memcpy(fn_state, &instance_data[instance].out_state.fn_state[prio], sizeof(*fn_state));
    fn_state->tx_cnt = 0;
    if (fn_state->min_delay == 0xffffffff)
        fn_state->min_delay = 0;
    if (fn_state->min_delay_var == 0xffffffff)
        fn_state->min_delay_var = 0;

    memcpy(nf_state, &instance_data[instance].out_state.nf_state[prio], sizeof(*nf_state));
    nf_state->rx_cnt = 0;
    nf_state->rx_tout_cnt = 0;
    if (nf_state->min_delay == 0xffffffff)
        nf_state->min_delay = 0;
    if (nf_state->min_delay_var == 0xffffffff)
        nf_state->min_delay_var = 0;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_dm_state_clear_set(const u32    instance)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)           return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)                            {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}

    instance_data[instance].event_flags |= EVENT_IN_DM_CLEAR;

    run_dm_clear(instance);

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_dm_db_state_get(const u32  instance,
                                  u32        *delay,
                                  u32        *delay_var)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    *delay = 0;
    *delay_var = 0;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_lt_state_get(const u32                  instance,
                               vtss_appl_mep_lt_state_t   *const state)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)        {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}

    *state = instance_data[instance].out_state.lt_state;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_lb_state_get(const u32                  instance,
                               vtss_appl_mep_lb_state_t   *const state)
{
    vtss_mep_supp_lb_status_t  status;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)        {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}

    if (instance_data[instance].lb_config.common.to_send != VTSS_APPL_MEP_LB_TO_SEND_INFINITE) {   /* This is not HW generated LBM with VOE support - LB status is generated here in upper layer */
        if (vtss_mep_supp_lb_status_get(instance, &status) == VTSS_MEP_SUPP_RC_OK) {
            instance_data[instance].out_state.lb_state.transaction_id = status.trans_id;
            instance_data[instance].out_state.lb_state.lbm_transmitted = status.lbm_counter;

            *state = instance_data[instance].out_state.lb_state;
            if (state->reply_cnt == 0) {    /* No replies are received - simulate one in order to print transmitted LBM */
                state->reply_cnt = 1;
                memset(state->reply[0].mac.addr, 0, sizeof(state->reply[0].mac.addr));
            }
        } else
            *state = instance_data[instance].out_state.lb_state;
    }
    else
    {
        if (vtss_mep_supp_lb_status_get(instance, &status) == VTSS_MEP_SUPP_RC_OK) {    /* This is HW generated LBM with VOE support - LB status is taken from VOE */
            instance_data[instance].out_state.lb_state.transaction_id = status.trans_id;
            instance_data[instance].out_state.lb_state.lbm_transmitted = status.lbm_counter;
            instance_data[instance].out_state.lb_state.reply_cnt = 1;   /* No replies are received by 'upper logic' - simulate one in order to print transmitted LBM */
            memset(instance_data[instance].out_state.lb_state.reply[0].mac.addr, 0, sizeof(instance_data[instance].out_state.lb_state.reply[0].mac.addr));
            instance_data[instance].out_state.lb_state.reply[0].lbr_received = status.lbr_counter;
            instance_data[instance].out_state.lb_state.reply[0].out_of_order = status.oo_counter;
            *state = instance_data[instance].out_state.lb_state;
        }
    }

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_lb_state_clear_set(const u32    instance)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)           return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)                            {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}

    instance_data[instance].event_flags |= EVENT_IN_LB_CLEAR;

    run_lb_clear(instance);

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_lm_state_clear_set(const u32                      instance,
                                     const vtss_appl_mep_clear_dir  direction)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)           return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)                            {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}

    instance_data[instance].event_flags |= EVENT_IN_LM_CLEAR;

    run_lm_clear(instance, direction);

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_tst_state_get(const u32                   instance,
                                vtss_appl_mep_tst_state_t   *const state)
{
    vtss_mep_supp_tst_status_t  supp_state;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)             return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)        {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}

    if (vtss_mep_supp_tst_status_get(instance, &supp_state) != VTSS_MEP_SUPP_RC_OK)       vtss_mep_trace("vtss_mep_mgmt_tst_state_get: Call to supp failed", instance, 0, 0, 0);
    instance_data[instance].out_state.tst_state.tx_counter = supp_state.tx_counter;
    if (MPLS_DOMAIN(instance_data[instance].config.domain) || instance_data[instance].config.voe)    /* This is a VOE based MEP or MPLS */
        instance_data[instance].out_state.tst_state.rx_counter = supp_state.rx_counter;
    instance_data[instance].out_state.tst_state.oo_counter = supp_state.oo_counter;
    *state = instance_data[instance].out_state.tst_state;

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_tst_state_clear_set(const u32    instance)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)           return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)                            {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)          {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}

    instance_data[instance].event_flags |= EVENT_IN_TST_CLEAR;

    run_tst_clear(instance);

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_change_set(const u32 instance,  BOOL evc_change,  BOOL e_tree)
{
    mep_instance_data_t *data;    /* Instance data reference */
    u32                 i, rc = VTSS_MEP_RC_OK;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)           return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];

    if (evc_change) {
        if (e_tree && ((data->config.direction == VTSS_APPL_MEP_DOWN) || data->config.voe)) {   /* Down MEP and VOE based MEP is not allowed in an E-TREE */
            data->config.enable = FALSE;
            rc = VTSS_MEP_RC_NOT_ENABLED;
        }

        if (!e_tree && VTSS_MEP_SUPP_ETREE(data->evc_type)) { /* This EVC is not E-TREE and instance is a E-TREE MEP. */
            for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i) {  /* Check to see if more Up-MEPs are on different ports in this E-TREE */
                if ((i != instance) && (data->config.enable) && VTSS_MEP_SUPP_ETREE(instance_data[i].evc_type) && (instance_data[i].config.flow == data->config.flow)) {
                    break;
                }
            }
            if (i < VTSS_APPL_MEP_INSTANCE_MAX) {   /* Only one Up-MEP is allowed in a non E-TREE */
                data->config.enable = FALSE;
                rc = VTSS_MEP_RC_NOT_ENABLED;
            }
        }
    }

    instance_data[instance].event_flags |= EVENT_IN_CONFIG;

    vtss_mep_run();

    vtss_mep_crit_unlock();

    return(rc);
}


u32 vtss_mep_mgmt_protection_change_set(const u32   instance)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)           return(VTSS_MEP_RC_INVALID_INSTANCE);

    if (!mep_caps.mesa_mep_serval) {
        vtss_mep_crit_lock();

        instance_data[instance].event_flags |= EVENT_IN_CONFIG;

        vtss_mep_run();

        vtss_mep_crit_unlock();
    } else {
        vtss_mep_supp_protection_change(instance);
    }

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_mgmt_ps_tlv_change_set()
{
    mep_instance_data_t       *data;    /* Instance data reference */
    mesa_packet_port_info_t   info;
    CapArray<mesa_packet_port_filter_t, MESA_CAP_PORT_CNT> filter;
    u32 i;

    vtss_mep_crit_lock();

    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i) {
        data = &instance_data[i];

        if (data->config.enable && (data->config.direction == VTSS_APPL_MEP_DOWN) && data->cc_config.enable && data->cc_config.tlv_enable) {  /* Any Down-MEP with enabled CCM with TLV */
            if (mesa_packet_port_info_init(&info) != VTSS_RC_OK)
                continue;
            info.vid = data->rule_vid;
            if (mesa_packet_port_filter_get(NULL, &info, filter.size(), filter.data()) != VTSS_RC_OK) /* Get the filter info for the VID of this instance */
                continue;
            if (data->ps_tlv_value != ((filter[data->config.port].filter != MESA_PACKET_FILTER_DISCARD) ? 2 : 1))  /* Port Status has changed for this instance - activate new TLV */
                data->event_flags |= EVENT_IN_TLV_CHANGE;
        }
    }

    vtss_mep_run();

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}

u32 vtss_mep_mgmt_is_tlv_change_set(const u32   instance)
{
    mep_instance_data_t       *data;    /* Instance data reference */

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)           return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    data = &instance_data[instance];
    data->event_flags |= EVENT_IN_TLV_CHANGE; /* The interface state has changed - activate new TLV */

    if (data->lst_config.enable) {  /* Link State Tracking is enabled */
        if (!vtss_mep_port_los_get(data->config.port)) {
            /* LOS is not active. Start Remote Ignore Timer  - to avoid "deadlock" */
            switch (data->cc_config.rate) { /* This timer should cover the time for IS to get other end - remote link to get up (1 sec) - IS to get back here */
                case VTSS_APPL_MEP_RATE_300S:
                case VTSS_APPL_MEP_RATE_100S:
                case VTSS_APPL_MEP_RATE_10S: data->lst_state.ign_timer = 6000/timer_res; break;
                case VTSS_APPL_MEP_RATE_1S: data->lst_state.ign_timer = 7000/timer_res; break;
                default: data->lst_state.ign_timer = 7000/timer_res; break;     /* This is not possible */
            }
            vtss_mep_timer_start();
        } else {
            data->lst_state.ign_timer = 0;
        }
    }

    vtss_mep_run();

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


void  vtss_mep_mgmt_default_conf_get(mep_default_conf_t  *const def_conf)
{
    char meg[] = "ICC000MEG0000";

    memset(def_conf, 0, sizeof(*def_conf));

    def_conf->config.enable = FALSE;
    def_conf->config.mode = VTSS_APPL_MEP_MEP;
    def_conf->config.direction = VTSS_APPL_MEP_DOWN;
    def_conf->config.domain = VTSS_APPL_MEP_PORT;
    def_conf->config.flow = 0;
    def_conf->config.port = 0;
    def_conf->config.level = 0;
    def_conf->config.vid = 0;
    def_conf->config.voe = FALSE;
    def_conf->config.format = VTSS_APPL_MEP_ITU_ICC;
    memset(def_conf->config.name, 0, sizeof(def_conf->config.name));
    memset(def_conf->config.meg, 0, sizeof(def_conf->config.meg));
    memcpy(def_conf->config.meg, meg, sizeof(meg));
    def_conf->config.mep = 1;
    def_conf->config.peer_count =0 ;
    memset(def_conf->config.peer_mep, 0, sizeof(def_conf->config.peer_mep));
    memset(def_conf->config.peer_mac, 0, sizeof(def_conf->config.peer_mac));
    def_conf->config.out_of_service = FALSE;
    def_conf->config.evc_pag = 0;
    def_conf->config.evc_qos = 0;

    memset(&def_conf->peer_conf.mac, 0, sizeof(def_conf->peer_conf.mac));

    def_conf->syslog_conf.enable = FALSE;

    def_conf->pm_conf.enable = FALSE;

    def_conf->lst_conf.enable = FALSE;

    def_conf->tlv_conf.os_tlv.oui[0] = 0x00;
    def_conf->tlv_conf.os_tlv.oui[1] = 0x00;
    def_conf->tlv_conf.os_tlv.oui[2] = 0x0C;
    def_conf->tlv_conf.os_tlv.subtype = 0x01;
    def_conf->tlv_conf.os_tlv.value = 0x02;

    def_conf->cc_conf.enable = FALSE;
    def_conf->cc_conf.prio = 0;
    def_conf->cc_conf.rate = VTSS_APPL_MEP_RATE_1S;
    def_conf->cc_conf.tlv_enable = FALSE;

    def_conf->lm_conf.enable = FALSE;
    def_conf->lm_conf.enable_rx = FALSE;
    def_conf->lm_conf.synthetic = FALSE;
    def_conf->lm_conf.dei = FALSE;
    def_conf->lm_conf.prio = 0;
    def_conf->lm_conf.cast = VTSS_APPL_MEP_MULTICAST;
    def_conf->lm_conf.mep = 1;
    def_conf->lm_conf.rate = VTSS_APPL_MEP_RATE_1S;
    def_conf->lm_conf.size = 64;
    def_conf->lm_conf.ended = VTSS_APPL_MEP_SINGEL_ENDED;
    def_conf->lm_conf.flr_interval = 5;
    def_conf->lm_conf.meas_interval = 1000; /* One second */
    def_conf->lm_conf.flow_count = FALSE;
    def_conf->lm_conf.oam_count = VTSS_APPL_MEP_OAM_COUNT_Y1731;
    def_conf->lm_conf.loss_th = 1;

    def_conf->lm_avail_conf.enable   = FALSE;
    def_conf->lm_avail_conf.flr_th   = 10; /* <1% for AVAIL */
    def_conf->lm_avail_conf.interval = MEP_AVAIL_INTERVAL_DEF; /* measurements */
    def_conf->lm_avail_conf.main     = FALSE;

    def_conf->lm_hli_conf.enable   = FALSE;
    def_conf->lm_hli_conf.flr_th   = 100; /* >10% for High Loss */
    def_conf->lm_hli_conf.con_int  = 100; /* measurements */

    def_conf->lm_sdeg_conf.enable   = FALSE;
    def_conf->lm_sdeg_conf.tx_min   = 0;
    def_conf->lm_sdeg_conf.flr_th   = 10; /* >1% to get SDEG */
    def_conf->lm_sdeg_conf.bad_th   = 10; /* number of lm_conf.flr_interval */
    def_conf->lm_sdeg_conf.good_th  = 10; /* number of lm_conf.flr_interval */

    def_conf->dm_conf.enable = FALSE;;
    def_conf->dm_conf.prio = 0;
    def_conf->dm_conf.cast = VTSS_APPL_MEP_MULTICAST;
    def_conf->dm_conf.mep = 1;
    def_conf->dm_conf.ended = VTSS_APPL_MEP_SINGEL_ENDED;
    def_conf->dm_conf.proprietary = FALSE;
    def_conf->dm_conf.calcway = VTSS_APPL_MEP_FLOW;
    def_conf->dm_conf.interval = 10;
    def_conf->dm_conf.lastn = 10;
    def_conf->dm_conf.tunit = VTSS_APPL_MEP_US;
    def_conf->dm_conf.overflow_act = VTSS_APPL_MEP_DISABLE;
    def_conf->dm_conf.synchronized = FALSE;;
    def_conf->dm_conf.num_of_bin_fd = 3;
    def_conf->dm_conf.num_of_bin_ifdv = 3;
    def_conf->dm_conf.m_threshold = 5000;


    def_conf->aps_conf.enable = FALSE;
    def_conf->aps_conf.prio = 0;
    def_conf->aps_conf.type = VTSS_APPL_MEP_L_APS;
    def_conf->aps_conf.cast = VTSS_APPL_MEP_MULTICAST;
    def_conf->aps_conf.raps_octet = 1;

    def_conf->lt_conf.enable = FALSE;
    def_conf->lt_conf.prio = 0;
    def_conf->lt_conf.mep = 1;
    memset(def_conf->lt_conf.mac.addr, 0, sizeof(def_conf->lt_conf.mac.addr));
    def_conf->lt_conf.ttl = 1;
    def_conf->lt_conf.transactions = 1;

    def_conf->lb_conf.enable = FALSE;
    def_conf->lb_conf.dei = FALSE;
    def_conf->lb_conf.prio = 0;
    def_conf->lb_conf.cast = VTSS_APPL_MEP_MULTICAST;
    def_conf->lb_conf.mep = 1;
    memset(def_conf->lb_conf.mac.addr, 0, sizeof(def_conf->lb_conf.mac.addr));
    def_conf->lb_conf.to_send = 10;
    def_conf->lb_conf.size = 64;
    def_conf->lb_conf.interval = 100;
    def_conf->lb_conf.ttl = 0;

    def_conf->ais_conf.enable = FALSE;
    def_conf->ais_conf.protection = FALSE;
    def_conf->ais_conf.rate = VTSS_APPL_MEP_RATE_1S;

    def_conf->lck_conf.enable = FALSE;
    def_conf->lck_conf.rate = VTSS_APPL_MEP_RATE_1S;

    def_conf->tst_conf.enable = FALSE;
    def_conf->tst_conf.enable_rx = FALSE;
    def_conf->tst_conf.prio = 0;
    def_conf->tst_conf.dei = FALSE;
    def_conf->tst_conf.mep = 1;
    def_conf->tst_conf.rate = 1000;
    def_conf->tst_conf.size = 64;
    def_conf->tst_conf.pattern = VTSS_APPL_MEP_PATTERN_ALL_ZERO;
    def_conf->tst_conf.sequence = FALSE;

    def_conf->test_prio_conf.enable = FALSE;
    def_conf->test_prio_conf.dp = VTSS_APPL_MEP_DP_GREEN;
    def_conf->test_prio_conf.rate = 1000;
    def_conf->test_prio_conf.size = 64;
    def_conf->test_prio_conf.pattern = VTSS_APPL_MEP_PATTERN_ALL_ZERO;

    def_conf->lb_common_conf.mep = 1;
    memset(def_conf->lb_common_conf.mac.addr, 0, sizeof(def_conf->lb_common_conf.mac.addr));
    def_conf->lb_common_conf.cast = VTSS_APPL_MEP_MULTICAST;
    def_conf->lb_common_conf.sequence = FALSE;
    def_conf->lb_common_conf.to_send = 10;
    def_conf->lb_common_conf.interval = 100;

    def_conf->tst_common_conf.mep = 1;
    def_conf->tst_common_conf.sequence = FALSE;
    def_conf->tst_common_conf.enable_rx = FALSE;

    def_conf->dm_prio_conf.enable = FALSE;

    def_conf->dm_common_conf.cast = VTSS_APPL_MEP_MULTICAST;
    def_conf->dm_common_conf.mep = 1;
    def_conf->dm_common_conf.ended = VTSS_APPL_MEP_SINGEL_ENDED;
    def_conf->dm_common_conf.proprietary = FALSE;
    def_conf->dm_common_conf.calcway = VTSS_APPL_MEP_FLOW;
    def_conf->dm_common_conf.interval = 10;
    def_conf->dm_common_conf.lastn = 10;
    def_conf->dm_common_conf.tunit = VTSS_APPL_MEP_US;
    def_conf->dm_common_conf.overflow_act = VTSS_APPL_MEP_DISABLE;
    def_conf->dm_common_conf.synchronized = FALSE;;
    def_conf->dm_common_conf.num_of_bin_fd = 3;
    def_conf->dm_common_conf.num_of_bin_ifdv = 3;
    def_conf->dm_common_conf.m_threshold = 5000;

    def_conf->client_flow_conf.ais_prio = VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST;
    def_conf->client_flow_conf.lck_prio = VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST;
    def_conf->client_flow_conf.level = 0;

    def_conf->bfd_conf.enable          = FALSE;
    def_conf->bfd_conf.is_independent  = FALSE;
    def_conf->bfd_conf.rx_flow         = 0;
    def_conf->bfd_conf.cc_only         = FALSE;
    def_conf->bfd_conf.cc_period       = 1000000; /* 1 sec */
    def_conf->bfd_conf.tx_auth_enabled = FALSE;
    def_conf->bfd_conf.rx_auth_enabled = FALSE;
    def_conf->bfd_conf.tx_auth_key_id  = VTSS_APPL_MEP_BFD_AUTH_DISABLED;

    def_conf->rt_conf.enable = FALSE;
    def_conf->rt_conf.tc     = 0;
}

/****************************************************************************/
/*  MEP module interface                                                    */
/****************************************************************************/

u32 vtss_mep_tx_aps_info_set(const u32    instance,
                             const u8     *aps,
                             const BOOL   event)
{
    u32   rc = VTSS_MEP_SUPP_RC_OK;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)           return(VTSS_MEP_RC_INVALID_INSTANCE);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)                                           {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)                         {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}
    if (event && (instance_data[instance].aps_config.type != VTSS_APPL_MEP_R_APS))        {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
    if ((instance_data[instance].aps_config.type == VTSS_APPL_MEP_R_APS) &&
        !event && !memcmp(instance_data[instance].tx_aps, aps, VTSS_MEP_APS_DATA_LENGTH)) {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_OK);}

    memcpy(instance_data[instance].tx_aps, aps, VTSS_MEP_APS_DATA_LENGTH);

    vtss_mep_crit_unlock();

    rc = vtss_mep_supp_aps_txdata_set(instance, aps, event);

    if (rc != VTSS_MEP_SUPP_RC_OK)       vtss_mep_trace("vtss_mep_tx_aps_info_set: Call to supp failed", instance, 0, 0, 0);

    return(VTSS_MEP_RC_OK);
}


void vtss_mep_new_ssf_state(const u32    instance,
                            const BOOL   state)
{
    BOOL                    aTsf_c, aTsd_c;
    mep_domain_t            domain_c;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)           return;

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)                                    {vtss_mep_crit_unlock(); return;}
    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)                  {vtss_mep_crit_unlock(); return;}

    instance_data[instance].ssf_state = state;

    instance_data[instance].event_flags |= EVENT_IN_SSF_SSD_STATE;

    run_ssf_ssd_state(instance);

    if (instance_data[instance].event_flags & EVENT_OUT_SF_SD)
    {   /* Transmit SF/SD info to EPS */
        instance_data[instance].event_flags &= ~EVENT_OUT_SF_SD;
        domain_c = instance_data[instance].config.domain;
        aTsf_c = instance_data[instance].out_state.state.mep.aTsf;
        aTsd_c = instance_data[instance].out_state.state.mep.aTsd;

        vtss_mep_crit_unlock();
        vtss_mep_sf_sd_state_set(instance,  domain_c,   aTsf_c,   aTsd_c);
        return;
    }

    vtss_mep_crit_unlock();
}


u32 vtss_mep_signal_in(const u32   instance)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)            return(VTSS_MEP_RC_INVALID_PARAMETER);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)                        {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)      {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}

    if (instance_data[instance].aps_config.enable)
        instance_data[instance].event_flags |= EVENT_OUT_APS;

    instance_data[instance].event_flags |= EVENT_OUT_SF_SD;

    vtss_mep_run();

    vtss_mep_crit_unlock();

    return(VTSS_MEP_RC_OK);
}


u32 vtss_mep_raps_forwarding(const u32    instance,
                             const BOOL   enable)
{
    u32 rc = VTSS_MEP_RC_OK;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)            return(VTSS_MEP_RC_INVALID_PARAMETER);
//printf("vtss_mep_raps_forwarding  instance %u enable %u\n", instance, enable);
    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)                                    {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)                  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}
    if (instance_data[instance].config.domain == VTSS_APPL_MEP_EVC)                {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}
    if ((instance_data[instance].config.domain == VTSS_APPL_MEP_PORT) &&
        (instance_data[instance].config.vid == 0))                                 {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_INVALID_PARAMETER);}

    instance_data[instance].raps_forward = enable;

    rc = raps_forwarding_control((instance_data[instance].config.domain == VTSS_APPL_MEP_PORT) ? instance_data[instance].config.vid : instance_data[instance].config.flow);

    vtss_mep_crit_unlock();

    return(rc);
}


u32 vtss_mep_raps_transmission(const u32    instance,
                               const BOOL   enable)
{
    u32   rc = VTSS_MEP_SUPP_RC_OK;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)          return(VTSS_MEP_RC_INVALID_PARAMETER);

    vtss_mep_crit_lock();

    if (!instance_data[instance].config.enable)                                    {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_NOT_ENABLED);}
    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)                  {vtss_mep_crit_unlock(); return(VTSS_MEP_RC_MIP);}

    vtss_mep_crit_unlock();

    rc = vtss_mep_supp_raps_transmission(instance, enable);

    if (rc != VTSS_MEP_SUPP_RC_OK)       vtss_mep_trace("vtss_mep_raps_transmission: Call to supp failed", instance, 0, 0, 0);

    return(VTSS_MEP_RC_OK);
}


const char* vtss_mep_rate_to_string(vtss_appl_mep_rate_t rate)
{
    switch (rate)
    {
        case VTSS_APPL_MEP_RATE_300S: return("300s");
        case VTSS_APPL_MEP_RATE_100S: return("100s");
        case VTSS_APPL_MEP_RATE_10S: return("10s");
        case VTSS_APPL_MEP_RATE_1S: return("1s");
        case VTSS_APPL_MEP_RATE_6M: return("6m");
        case VTSS_APPL_MEP_RATE_1M: return("1m");
        case VTSS_APPL_MEP_RATE_6H: return("6h");
        default: return("invalid");
    }
}

const char* vtss_mep_flow_to_name(mep_domain_t domain, u32 flow, vtss_uport_no_t port_no, char *str)
{
    switch (domain)
    {
















        case VTSS_APPL_MEP_PORT:
            icli_port_info_txt(VTSS_USID_START, port_no, str);
            break;

        case VTSS_APPL_MEP_VLAN:
            if (vtss_appl_vlan_name_get(flow, str, NULL) != VTSS_RC_OK)
            {
                sprintf(str, "VLAN%u", flow);
            }
            break;

        default:
            sprintf(str, "invalid");
            break;
    }

    return str;
}

const char* vtss_mep_domain_to_string(mep_domain_t domain)
{
    switch (domain)
    {
        case VTSS_APPL_MEP_PORT: return("Port");
        case VTSS_APPL_MEP_EVC: return("EVC");
        case VTSS_APPL_MEP_VLAN: return("VLAN");






        default: return("invalid");
    }
}

const char* vtss_mep_direction_to_string(vtss_appl_mep_direction_t direction)
{
    switch (direction)
    {
        case VTSS_APPL_MEP_DOWN: return("Down");
        case VTSS_APPL_MEP_UP: return("Up");
        default: return("invalid");
    }
}

const char* vtss_mep_unit_to_string(vtss_appl_mep_dm_tunit_t unit)
{
    switch (unit)
    {
        case VTSS_APPL_MEP_US: return("us");
        case VTSS_APPL_MEP_NS: return("ns");
        default: return("invalid");
    }
}




/****************************************************************************/
/*  MEP lower support interface                                             */
/****************************************************************************/

void vtss_mep_supp_new_aps(const u32    instance,
                           const u8     *aps)
{
    mep_domain_t  domain_c;

    vtss_mep_crit_lock();

    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)          {vtss_mep_crit_unlock();    return;}

    memcpy(instance_data[instance].out_state.rx_aps, aps, VTSS_MEP_APS_DATA_LENGTH);
    domain_c = instance_data[instance].config.domain;

    vtss_mep_crit_unlock();

    vtss_mep_rx_aps_info_set(instance,   domain_c,   aps);
}


void vtss_mep_supp_new_defect_state(const u32  instance)
{
    vtss_mep_crit_lock();

    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)          {vtss_mep_crit_unlock();    return;}

    instance_data[instance].event_flags |= EVENT_IN_DEFECT_STATE;
    vtss_mep_run();

    vtss_mep_crit_unlock();
}


void vtss_mep_supp_new_ccm_state(const u32  instance)
{
    vtss_mep_crit_lock();

    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)          {vtss_mep_crit_unlock();    return;}

    instance_data[instance].event_flags |= EVENT_IN_CCM_STATE;
    vtss_mep_run();

    vtss_mep_crit_unlock();
}


void vtss_mep_supp_new_ltr(const u32     instance)
{
    vtss_mep_crit_lock();

    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)          {vtss_mep_crit_unlock(); return;}

    instance_data[instance].event_flags |= EVENT_IN_LTR_NEW;
    vtss_mep_run();

    vtss_mep_crit_unlock();
}



void vtss_mep_supp_new_lbr(const u32     instance)
{
    vtss_mep_crit_lock();

    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)          {vtss_mep_crit_unlock(); return;}

    instance_data[instance].event_flags |= EVENT_IN_LBR_NEW;
    vtss_mep_run();

    vtss_mep_crit_unlock();
}


void vtss_mep_supp_new_dmr(const u32     instance)
{
    vtss_mep_crit_lock();

    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)         {vtss_mep_crit_unlock(); return;}

    instance_data[instance].event_flags |= EVENT_IN_DMR_NEW;
    vtss_mep_run();

    vtss_mep_crit_unlock();
}


void vtss_mep_supp_new_dm1(const u32  instance)
{
    vtss_mep_crit_lock();

    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)         {vtss_mep_crit_unlock(); return;}

    instance_data[instance].event_flags |= EVENT_IN_1DM_NEW;
    vtss_mep_run();

    vtss_mep_crit_unlock();
}


void vtss_mep_supp_new_tst(const u32   instance,
                           const u32   frame_size)
{
    vtss_mep_crit_lock();

    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)         {vtss_mep_crit_unlock(); return;}

    instance_data[instance].event_flags |= EVENT_IN_TST_NEW;
    instance_data[instance].tst_state.rx_frame_size = frame_size;

    vtss_mep_run();

    vtss_mep_crit_unlock();
}


void vtss_mep_supp_new_mac(const u32  instance)
{
    vtss_mep_crit_lock();

    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)         {vtss_mep_crit_unlock(); return;}

    instance_data[instance].event_flags |= EVENT_IN_MAC_NEW;

    vtss_mep_run();

    vtss_mep_crit_unlock();
}


void vtss_mep_supp_new_tlv_is(const u32   instance)
{
    vtss_mep_crit_lock();

    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)         {vtss_mep_crit_unlock(); return;}

    instance_data[instance].event_flags |= EVENT_IN_TLV_IS_NEW;

    vtss_mep_run();

    vtss_mep_crit_unlock();
}


u32 vtss_mep_supp_new_etree_elan_extraction(u32 instance)
{
    u32 rc;

    vtss_mep_crit_lock();

    if (instance_data[instance].config.mode == VTSS_APPL_MEP_MIP)         {vtss_mep_crit_unlock(); return VTSS_MEP_RC_INTERNAL;}

    rc = run_etree_elan_new(instance);

    vtss_mep_crit_unlock();

    return(rc);
}

BOOL vtss_mep_supp_counters_get(const u32       instance,
                                u32             *const rx_frames,
                                u32             *const tx_frames)
{
    BOOL is_valid = FALSE;
    vtss_mep_crit_lock();

    if (instance_data[instance].config.domain == VTSS_APPL_MEP_PORT)
    {
        mesa_basic_counters_t   counters;

        if (mesa_port_basic_counters_get(0, instance_data[instance].config.port, &counters) != VTSS_RC_OK)    {vtss_mep_crit_unlock(); return FALSE;}

        *rx_frames = counters.rx_frames;
        *tx_frames = counters.tx_frames;
        is_valid = TRUE;
    }
    else if (instance_data[instance].config.domain == VTSS_APPL_MEP_VLAN) {
        *rx_frames = 0; /* SW based VLAN LM is not supported */
        *tx_frames = 0;
    }
















































    else    {vtss_mep_crit_unlock(); return FALSE;}

    vtss_mep_crit_unlock();
    return is_valid;
}


void vtss_mep_supp_tst_done(const u32   instance)
{
    vtss_mep_crit_lock();

    instance_data[instance].tst_state.done_timer = 300/timer_res;     /* Start the TST done timer to update the TST rules */
    vtss_mep_timer_start();

    vtss_mep_crit_unlock();
}


void vtss_mep_supp_lbm_done(const u32   instance)
{
    vtss_mep_crit_lock();

    instance_data[instance].lb_state.done_timer = 300/timer_res;     /* Start the LBM done timer to update the LB rules */
    vtss_mep_timer_start();

    vtss_mep_crit_unlock();
}




/****************************************************************************/
/*  MEP platform interface                                                  */
/****************************************************************************/

void vtss_mep_timer_thread(BOOL  *const stop)
{
    u32  i, j;
    BOOL run;

    run = FALSE;
    *stop = TRUE;

    vtss_mep_crit_lock();

    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
    {
        if (!instance_data[i].config.enable) continue;
        if (instance_data[i].lt_state.timer)
        {
            instance_data[i].lt_state.timer--;
            if (!instance_data[i].lt_state.timer)
            {
                instance_data[i].event_flags |= EVENT_IN_LT_TIMER;
                run = TRUE;
            }
            else
                *stop = FALSE;
        }
        if (instance_data[i].lb_state.timer)
        {
            instance_data[i].lb_state.timer--;
            if (!instance_data[i].lb_state.timer)
            {
                instance_data[i].event_flags |= EVENT_IN_LB_TIMER;
                run = TRUE;
            }
            else
                *stop = FALSE;
        }
        if (instance_data[i].tst_state.timer)
        {
            instance_data[i].tst_state.timer--;
            if (!instance_data[i].tst_state.timer)
            {
                instance_data[i].event_flags |= EVENT_IN_TST_TIMER;
                run = TRUE;
            }
            else
                *stop = FALSE;
        }
        if (instance_data[i].tst_state.done_timer)
        {
            instance_data[i].tst_state.done_timer--;
            if (!instance_data[i].tst_state.done_timer)
            {
                instance_data[i].event_flags |= EVENT_IN_TST_DONE_TIMER;
                run = TRUE;
            }
            else
                *stop = FALSE;
        }
        if (instance_data[i].lb_state.done_timer)
        {
            instance_data[i].lb_state.done_timer--;
            if (!instance_data[i].lb_state.done_timer)
            {
                instance_data[i].event_flags |= EVENT_IN_LB_DONE_TIMER;
                run = TRUE;
            }
            else
                *stop = FALSE;
        }
        if (instance_data[i].lst_state.ign_timer)
        {
            instance_data[i].lst_state.ign_timer--;
            if (!instance_data[i].lst_state.ign_timer)
            {
                instance_data[i].event_flags |= EVENT_IN_LST_IGN_TIMER;
                run = TRUE;
            }
            else
                *stop = FALSE;
        }
        if (instance_data[i].lst_state.sf_timer)
        {
            instance_data[i].lst_state.sf_timer--;
            if (!instance_data[i].lst_state.sf_timer)
            {
                instance_data[i].event_flags |= EVENT_IN_LST_SF_TIMER;
                run = TRUE;
            }
            else
                *stop = FALSE;
        }
        if (instance_data[i].syslog_state.timer)
        {
            for (j=0; j < mep_caps.mesa_oam_peer_cnt; ++j) {
                if (instance_data[i].syslog_state.peer_mep_timer[j] > 0) {
                    instance_data[i].syslog_state.peer_mep_timer[j]--;
                }
            }

            instance_data[i].syslog_state.timer--;
            if (!instance_data[i].syslog_state.timer)
            {
                instance_data[i].event_flags |= EVENT_IN_SYSLOG_TIMER;
                run = TRUE;
            }
            else
                *stop = FALSE;
        }
        if (instance_data[i].los_int_cnt_timer)
        {
            instance_data[i].los_int_cnt_timer--;
            if (!instance_data[i].los_int_cnt_timer)
            {
                // TODO: vtss_mep_mgmt_lm_notif_set(i);
                instance_data[i].los_int_cnt_timer = 1000 * instance_data[i].lm_config.los_int_cnt_holddown / timer_res;
                run = TRUE;
            }
            else
                *stop = FALSE;
        }
        if (instance_data[i].los_th_cnt_timer)
        {
            instance_data[i].los_th_cnt_timer--;
            if (!instance_data[i].los_th_cnt_timer)
            {
                // TODO: vtss_mep_mgmt_lm_notif_set(i);
                instance_data[i].los_th_cnt_timer  = 1000 * instance_data[i].lm_config.los_th_cnt_holddown  / timer_res;
                run = TRUE;
            }
            else
                *stop = FALSE;
        }
        if (instance_data[i].hli_cnt_timer)
        {
            instance_data[i].hli_cnt_timer--;
            if (!instance_data[i].hli_cnt_timer)
            {
                for (int peer_index = 0; peer_index < instance_data[i].config.peer_count; peer_index++) {
                    vtss_mep_lm_hli_status_set(i, instance_data[i].config.peer_mep[peer_index],
                                               &instance_data[i].out_state.lm_hli_state[peer_index]);
                }

                instance_data[i].hli_cnt_timer = 1000 * instance_data[i].lm_hli_config.cnt_holddown  / timer_res;
                run = TRUE;
            }
            else
                *stop = FALSE;
        }
    }

    if (run)    vtss_mep_run();

    vtss_mep_crit_unlock();
}


void vtss_mep_run_thread(void)
{
    u32                     i;
    u8                      aps_c[VTSS_MEP_APS_DATA_LENGTH];
    BOOL                    aTsf_c, aTsd_c, enable;
    mep_domain_t  domain_c;

    vtss_mep_crit_lock();

    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
    {
        if (instance_data[i].event_flags & EVENT_IN_MASK)
        {/* New input */
            if (instance_data[i].event_flags & EVENT_IN_CONFIG)         (void)run_config(i);
            if (instance_data[i].event_flags & EVENT_IN_LM_CONFIG)      run_lm_config(i);
            if (instance_data[i].event_flags & EVENT_IN_DM_CONFIG)      run_dm_config(i);
            if (instance_data[i].event_flags & EVENT_IN_LT_CONFIG)      run_lt_config(i);
            if (instance_data[i].event_flags & EVENT_IN_AIS_CONFIG)     run_ais_config(i);
            if (instance_data[i].event_flags & EVENT_IN_LCK_CONFIG)     run_lck_config(i);
            if (instance_data[i].event_flags & EVENT_IN_APS_INFO)       run_aps_info(i);
            if (instance_data[i].event_flags & EVENT_IN_DEFECT_STATE)   run_defect_state(i);
            if (instance_data[i].event_flags & EVENT_IN_SSF_SSD_STATE)  run_ssf_ssd_state(i);
            if (instance_data[i].event_flags & EVENT_IN_LM_CLEAR)       run_lm_clear(i, VTSS_APPL_MEP_CLEAR_DIR_BOTH);
            if (instance_data[i].event_flags & EVENT_IN_DM_CLEAR)       run_dm_clear(i);
            if (instance_data[i].event_flags & EVENT_IN_TST_CLEAR)      run_tst_clear(i);
            if (instance_data[i].event_flags & EVENT_IN_LTR_NEW)        run_ltr_new(i);
            if (instance_data[i].event_flags & EVENT_IN_LBR_NEW)        run_lbr_new(i);
            if (instance_data[i].event_flags & EVENT_IN_DMR_NEW)        run_dmr_new(i);
            if (instance_data[i].event_flags & EVENT_IN_1DM_NEW)        run_dm1_new(i);
            if (instance_data[i].event_flags & EVENT_IN_TST_NEW)        run_tst_new(i);
            if (instance_data[i].event_flags & EVENT_IN_LT_TIMER)       run_lt_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_LB_TIMER)       run_lb_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_TST_TIMER)      run_tst_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_LST_IGN_TIMER)  run_lst_ign_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_LST_SF_TIMER)   run_lst_sf_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_TST_DONE_TIMER) run_tst_done_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_LB_DONE_TIMER)  run_lb_done_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_SYSLOG_TIMER)   run_syslog_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_TLV_CHANGE)     run_tlv_change(i);
            if (instance_data[i].event_flags & EVENT_IN_MAC_NEW)        run_mac_new(i);




            if (instance_data[i].event_flags & EVENT_IN_LST_CONFIG)     run_lst_config(i);
            if (instance_data[i].event_flags & EVENT_IN_TLV_IS_NEW)     run_tlv_is_new(i);
        }

        if (instance_data[i].event_flags & EVENT_OUT_MASK)
        {/* New output */
            /* Now do unlocked call-out to avoid any deadlock */
            if (instance_data[i].event_flags & EVENT_OUT_STATE)
            {   /* Notify external that MEP state has changed */
                instance_data[i].event_flags &= ~EVENT_OUT_STATE;

                enable = instance_data[i].config.enable;

                vtss_mep_crit_unlock();
                vtss_mep_status_change(i, enable);
                vtss_mep_crit_lock();
            }
            if (instance_data[i].event_flags & EVENT_OUT_APS)
            {   /* Transmit APS info to EPS */
                instance_data[i].event_flags &= ~EVENT_OUT_APS;

                /* Copy data for call-out */
                domain_c = instance_data[i].config.domain;
                memcpy(aps_c, instance_data[i].out_state.rx_aps, VTSS_MEP_APS_DATA_LENGTH);

                vtss_mep_crit_unlock();
                vtss_mep_rx_aps_info_set(i,   domain_c,   aps_c);
                vtss_mep_crit_lock();
            }
            if (instance_data[i].event_flags & EVENT_OUT_SF_SD)
            {   /* Transmit SF/SD info to EPS */
                instance_data[i].event_flags &= ~EVENT_OUT_SF_SD;

                /* Copy data for call-out */
                domain_c = instance_data[i].config.domain;
                aTsf_c = instance_data[i].out_state.state.mep.aTsf;
                aTsd_c = instance_data[i].out_state.state.mep.aTsd;

                vtss_mep_crit_unlock();
                vtss_mep_sf_sd_state_set(i,  domain_c,   aTsf_c,   aTsd_c);
                vtss_mep_crit_lock();
            }
            if (instance_data[i].event_flags & EVENT_OUT_SIGNAL)
            {   /* Send signal */
                instance_data[i].event_flags &= ~EVENT_OUT_SIGNAL;

                domain_c = instance_data[i].config.domain;

                vtss_mep_crit_unlock();
                vtss_mep_signal_out(i,   domain_c);
                vtss_mep_crit_lock();
            }
        }
    }

    vtss_mep_crit_unlock();
}


static vtss_mtimer_t hit_timer;
static vtss_mtimer_t start_timer;

// CCM loop static variables
static u32 mep_ccm_hit_inx = 0;
static u32 mep_ccm_poll_cnt = 0;
static u32 mep_ccm_start_inx = 0;
static bool mep_ccm_thread_is_init = FALSE;

void vtss_mep_ccm_thread(void)
{
    u32        rc=VTSS_MEP_SUPP_RC_OK, i, hit_cnt=0;
    u32        counter;
    u32        instances, timer_next_run, instances_per_run, number_of_runs;
    u32        mep_max_instances = VTSS_APPL_MEP_INSTANCE_MAX;

    if (!mep_ccm_thread_is_init) {
        // Delay usage of this capability value until the system is up
        mep_ccm_start_inx = mep_max_instances;
        mep_ccm_thread_is_init = TRUE;
    }

    vtss_mep_crit_lock();

    mep_ccm_poll_cnt++;
    for (i = 0; i < mep_max_instances; ++i)
    {
        if (MPLS_DOMAIN(instance_data[i].config.domain))   /* Do not do this for MPLS domain MEP */
            continue;

        if (instance_data[i].cc_config.enable && !instance_data[i].config.voe && (instance_data[i].config.peer_count == 1))
        {/* Active HW based CCM - poll ACL counter. */
            hit_cnt++;

            if ((instance_data[i].cc_config.rate == VTSS_APPL_MEP_RATE_300S) ||
                ((instance_data[i].cc_config.rate == VTSS_APPL_MEP_RATE_100S) && (mep_ccm_poll_cnt%3 == 0)) ||
                ((instance_data[i].cc_config.rate == VTSS_APPL_MEP_RATE_10S) && (mep_ccm_poll_cnt%30 == 0)))
            { /* The 3.3 ms interval is polled every 10 ms - the 10 ms interval is polled every 30 ms - the 100 ms interval is polled every 300 ms */
                vtss_mep_acl_ccm_count(i, &counter);
                if ((counter - instance_data[i].ace_count) == 0)    rc += vtss_mep_supp_loc_state(i, TRUE);  /* Ignore zero counter during rule update */
                else                                                rc += vtss_mep_supp_loc_state(i, FALSE);
                instance_data[i].ace_count = counter;
            }
        }
    }

    if (hit_cnt)
    {   /* There are some MEP with active HW CCM */
        if (VTSS_MTIMER_TIMEOUT(&hit_timer))
        {

            i = 0;
            instances = 0;
            instances_per_run = (hit_cnt/10) + 1;              /* All instances must be "hit me once" during max 10 runs */
            number_of_runs = (hit_cnt/instances_per_run) + 1;  /* This can not be more than max. 10 runs */
            timer_next_run = 800/number_of_runs;               /* Time until next run */
            VTSS_MTIMER_START(&hit_timer, timer_next_run);    /* Start timer for next run */
//vtss_mep_trace("Timeout -- hit_inx   instances_per_run   number_of_runs   timer_next_run", hit_inx, instances_per_run, number_of_runs, timer_next_run);

            do {
                if (instance_data[mep_ccm_hit_inx].cc_config.enable && !instance_data[mep_ccm_hit_inx].config.voe && (instance_data[mep_ccm_hit_inx].config.peer_count == 1)) {
//vtss_mep_trace("Loop -- hit_inx   i   hit_cnt", hit_inx, i, hit_cnt, 0);
                    instances++;
                    if (mep_ccm_hit_inx <= mep_ccm_start_inx) {     /* This is start of a new run through all instances */
                        mep_ccm_start_inx = mep_ccm_hit_inx;
                        if (VTSS_MTIMER_TIMEOUT(&start_timer)) {  /* Check if all instances was "hit me once" in one second */
                            vtss_mep_trace("Warning: did not CCM 'Hit Me Once' all instances in one second", 0,0,0,0);
                        }
                        VTSS_MTIMER_START(&start_timer, 1000);
                    }
                    vtss_mep_acl_ccm_hit(mep_ccm_hit_inx);
                    instance_data[mep_ccm_hit_inx].ace_count = 0;   /* On Luton26 and Serval the ACE counter is cleared in order to trick Hit Me Once */
                }
                mep_ccm_hit_inx++;
                if (mep_ccm_hit_inx>=VTSS_APPL_MEP_INSTANCE_MAX)     mep_ccm_hit_inx = 0;
            } while ((++i < VTSS_APPL_MEP_INSTANCE_MAX) && (instances < instances_per_run));

//vtss_mep_trace("return -- hit_inx   start_inx   i   hit_timer", hit_inx, start_inx, i, hit_timer);
        }
    }
    else
        mep_ccm_hit_inx = 0;

    if (rc)        vtss_mep_trace("vtss_mep_ccm_thread: error during poll", 0, 0, 0, 0);

    vtss_mep_crit_unlock();
}


u32 vtss_mep_init(const u32  timer_resolution, u32 mep_pag, u32 mip_pag)
{
    u32         i;
    mep_default_conf_t  def_conf;

    if ((timer_resolution == 0) || (timer_resolution > 100))     return(VTSS_MEP_RC_INVALID_PARAMETER);

    if (mep_caps.mesa_mep_serval) {
        base_mep_pag = mep_pag;
        base_mip_pag = mip_pag;
    }
    timer_res = timer_resolution;

    /* Initialize all instance data */
    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
        instance_data_clear(i, &instance_data[i]);

    vtss_mep_mgmt_default_conf_get(&def_conf);
    os_tlv_config = def_conf.tlv_conf.os_tlv;

    memset(vlan_raps_acl_id, 0xFF, sizeof(vlan_raps_acl_id));
    memset(vlan_raps_mac_octet, 0xFF, sizeof(vlan_raps_mac_octet));

    VTSS_MTIMER_START(&hit_timer, 1000);
    VTSS_MTIMER_START(&start_timer, 1000);
    return(VTSS_MEP_RC_OK);
}

}  // mep_g1

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
