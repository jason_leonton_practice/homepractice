/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _VTSS_MEP_API_H_
#define _VTSS_MEP_API_H_

#include "main_types.h"
#include "mep_api.h"

typedef struct {
    vtss_appl_mep_test_prio_conf_t   tests[VTSS_APPL_MEP_PRIO_MAX][VTSS_APPL_MEP_TEST_MAX];   /* Test flow configuration per prio */
    vtss_appl_mep_tst_common_conf_t  common;    /* The common Test Signal configuration */
    u32                              prio;      /* This is not used internally. This can be used to contain the latest priority used to support the old TST interface */
    u32                              test_idx;  /* This is not used internally. This can be used to contain the latest test index to support the old TST interface */
} vtss_mep_mgmt_tst_conf_t;

typedef struct {
    vtss_appl_mep_test_prio_conf_t   tests[VTSS_APPL_MEP_PRIO_MAX][VTSS_APPL_MEP_TEST_MAX];   /* LB flow configuration per prio */
    vtss_appl_mep_lb_common_conf_t   common;    /* The common Loop Back configuration */
    u32                              prio;      /* This is not used internally. This can be used to contain the latest priority used to support the old LB interface */
    u32                              test_idx;  /* This is not used internally. This can be used to contain the latest test index to support the old LB interface */
} vtss_mep_mgmt_lb_conf_t;

typedef struct {
    vtss_appl_mep_dm_prio_conf_t     dm[VTSS_APPL_MEP_PRIO_MAX];   /* Delay Measurement configuration per prio */
    vtss_appl_mep_dm_common_conf_t   common;    /* The common Delay Measurement configuration */
    u32                              prio;      /* This is not used internally. This can be used to contain the latest priority used to support the old DM interface */
} vtss_mep_mgmt_dm_conf_t;

typedef struct {
    vtss_appl_mep_state_t       mep;
    vtss_appl_mep_peer_state_t  peer_mep[VTSS_APPL_MEP_PEER_MAX];
} vtss_mep_mgmt_state_t;

#endif /* _VTSS_MEP_API_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
