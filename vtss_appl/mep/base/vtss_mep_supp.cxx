/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#define MEP_IMPL_G1

#include "vtss_mep_supp_api.h"
#include "mscc/ethernet/switch/api.h"
#include "vtss_mep.h" /* Internal interface */
#include <string.h>
#include <stdlib.h>





namespace mep_g1 {

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_MEP

#undef VTSS_TRACE_MODULE_ID
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_MEP

/*lint -sem( vtss_mep_supp_crit_lock, thread_lock ) */
/*lint -sem( vtss_mep_supp_crit_unlock, thread_unlock ) */

/****************************************************************************/
/*  MEP global variables                                                    */
/****************************************************************************/


/****************************************************************************/
/*  MEP local variables                                                     */
/****************************************************************************/
#define DEBUG_PHY_TS      0  //James
#define F_2DM_FOR_1DM       1  /* Use DMM/DMR packets to measure one-way DM if set */

#define OAM_TYPE_CCM      1
#define OAM_TYPE_LBR      2
#define OAM_TYPE_LBM      3
#define OAM_TYPE_LTR      4
#define OAM_TYPE_LTM      5
#define OAM_TYPE_AIS      33
#define OAM_TYPE_LCK      35
#define OAM_TYPE_TST      37
#define OAM_TYPE_LAPS     39
#define OAM_TYPE_RAPS     40
#define OAM_TYPE_LMR      42
#define OAM_TYPE_LMM      43
#define OAM_TYPE_1DM      45
#define OAM_TYPE_DMR      46
#define OAM_TYPE_DMM      47
#define OAM_TYPE_1SL      53
#define OAM_TYPE_SLR      54
#define OAM_TYPE_SLM      55
#define OAM_TYPE_LLR      56
#define OAM_TYPE_LLM      57
#define OAM_TYPE_1DM_FUP  0xFE /* Use the reserved code 0xfe as the opcode of follow-up packets */
#define OAM_TYPE_DMR_FUP  0xFF /* Use the reserved code 0xff as the opcode of follow-up packets */
#define OAM_TYPE_RT       0xFFFF





#define HEADER_MAX_SIZE      30


#define CCM_PDU_LENGTH       (75+(data->ccm_config.tlv_enable ? 16 : 0)) /* Port Status + Interface Status + Organization-Specific TLV is added */
#define LMR_PDU_LENGTH       17
#define LMM_PDU_LENGTH       17
#define SLM_PDU_LENGTH       (21+3)   /* SLM PDU with empty TLV */
#define SLR_PDU_LENGTH       21
#define SL1_PDU_LENGTH       21
#define LAPS_PDU_LENGTH      9
#define RAPS_PDU_LENGTH      37
#define LTM_PDU_LENGTH       33
#define LTR_PDU_LENGTH       40
#define LBM_PDU_LENGTH       9   /* This is only the size of the 'emty' LBM PDU -  - without contained TLV */
#define DM1_PDU_STD_LENGTH   21
#define DM1_PDU_VTSS_LENGTH  DM1_PDU_STD_LENGTH + 4 /* Add "VTSS" after data for remote site to know a follow-up packet is expected */
#define DM1_PDU_LENGTH       DM1_PDU_VTSS_LENGTH
#define DMM_PDU_STD_LENGTH   37
#define DMM_PDU_VTSS_LENGTH  DMM_PDU_STD_LENGTH + 4 /* Add "VTSS" after data for remote site to know a follow-up packet is expected */
#define DMM_PDU_LENGTH       DMM_PDU_VTSS_LENGTH
#define DMR_PDU_LENGTH       37
#define AIS_PDU_LENGTH       5
#define LCK_PDU_LENGTH       5
#define TST_PDU_LENGTH       9   /* This is only the size of the 'emty' TST PDU - without contained TLV */
#define CCM_PDU_TLV_OFFSET   74






#define EVENT_IN_CONFIG           0x0000000000000001LL
#define EVENT_IN_CCM_CONFIG       0x0000000000000002LL
#define EVENT_IN_CCM_GEN          0x0000000000000004LL
#define EVENT_IN_CCM_RDI          0x0000000000000008LL
#define EVENT_IN_LMM_CONFIG       0x0000000000000010LL
#define EVENT_IN_SLM_CONFIG       0x0000000000000020LL
#define EVENT_IN_APS_CONFIG       0x0000000000000040LL
#define EVENT_IN_APS_FORWARD      0x0000000000000080LL
#define EVENT_IN_APS_TXDATA       0x0000000000000100LL
#define EVENT_IN_LTM_CONFIG       0x0000000000000200LL
#define EVENT_IN_LBM_CONFIG       0x0000000000000400LL
#define EVENT_IN_DMM_CONFIG       0x0000000000000800LL
#define EVENT_IN_1DM_CONFIG       0x0000000000001000LL
#define EVENT_IN_LCK_CONFIG       0x0000000000002000LL
#define EVENT_IN_TST_CONFIG       0x0000000000004000LL
#define EVENT_IN_AIS_SET          0x0000000000008000LL
#define EVENT_IN_DEFECT_TIMER     0x0000000000010000LL
#define EVENT_IN_RX_CCM_TIMER     0x0000000000020000LL
#define EVENT_IN_TX_CCM_TIMER     0x0000000000040000LL
#define EVENT_IN_TX_APS_TIMER     0x0000000000080000LL
#define EVENT_IN_TX_LMM_TIMER     0x0000000000100000LL
#define EVENT_IN_TX_LBM_TIMER     0x0000000000200000LL
#define EVENT_IN_TX_DMM_TIMER     0x0000000000400000LL
#define EVENT_IN_TX_1DM_TIMER     0x0000000000800000LL
#define EVENT_IN_RX_APS_TIMER     0x0000000001000000LL
#define EVENT_IN_RX_LBM_TIMER     0x0000000002000000LL
#define EVENT_IN_RX_DMR_TIMER     0x0000000004000000LL
#define EVENT_IN_TX_AIS_TIMER     0x0000000008000000LL
#define EVENT_IN_TX_LCK_TIMER     0x0000000010000000LL
#define EVENT_IN_RDI_TIMER        0x0000000020000000LL
#define EVENT_IN_TST_TIMER        0x0000000040000000LL










#define EVENT_IN_MASK             0x000000007FFFFFFFLL



#define EVENT_OUT_NEW_DEFECT      0x8000000000000000LL
#define EVENT_OUT_NEW_APS         0x4000000000000000LL
#define EVENT_OUT_NEW_DMR         0x2000000000000000LL
#define EVENT_OUT_NEW_DM1         0x1000000000000000LL
#define EVENT_OUT_TST_DONE        0x0800000000000000LL
#define EVENT_OUT_LBM_DONE        0x0400000000000000LL
#define EVENT_OUT_MASK            0xFF00000000000000LL

#define APS_DATA_LENGTH ((data->aps_config.type == VTSS_MEP_SUPP_L_APS) ? VTSS_MEP_SUPP_LAPS_DATA_LENGTH : VTSS_MEP_SUPP_RAPS_DATA_LENGTH)
#define APS_PDU_LENGTH ((data->aps_config.type == VTSS_MEP_SUPP_L_APS) ? LAPS_PDU_LENGTH : RAPS_PDU_LENGTH)

#define TX_QOS_NORMAL   7
#define REPLY_COS_NONE  0xFFFFFFFF

#define MEG_ID_LENGTH (2+(VTSS_MEP_SUPP_MEG_CODE_LENGTH-1)+2+(VTSS_MEP_SUPP_MEG_CODE_LENGTH-1))

#define GENERIC_AIS     0
#define GENERIC_LCK     1
#define GENERIC_LAPS    2
#define GENERIC_RAPS    3
#define GENERIC_SLM     4
#define GENERIC_SLR     5
#define GENERIC_1SL     6

typedef struct
{
    i32     near_los_counter;   /* Near end los counter */
    i32     far_los_counter;    /* Far end los counter */
    u32     near_tx_counter;    /* Accumulated Near end transmitted service frames counter */
    u32     far_tx_counter;     /* Accumulated Far end transmitted service frames counter */

    u32     near_CT1, far_CT1;  /* Saved Near/Far Rx/Tx service frame counters */
    u32     near_CR1, far_CR1;

    u32     tx_msg_counter;     // Number of LMM/SLM/1SL PDUs transmitted
    u32     tx_rsp_counter;     // Number of LMR/SLR PDUs transmitted

    u32     rx_msg_counter;     // Number of LMM/SLM/1SL PDUs received
    u32     rx_rsp_counter;     // Number of LMR/SLR PDUs received

} lm_counter_t;

typedef struct
{
    lm_counter_t          lm_counter;
    sl_service_counter_t  sl_service_counter;
    u32                   meas_interval_tx_counter;
    u32                   rx_lm_timer;
} supp_lm_state_t;

typedef struct
{
    u32     timer;
    u32     old_period;
} defect_timer_t;

typedef struct
{
    vtss_mep_supp_ccm_state_t     state;
    u8                            unexp_meg_id[MEG_ID_LENGTH];
} ccm_state_t;

typedef struct
{
    BOOL                active;
    BOOL                enabled;       /* This is the "old" config enable */
    u8                  *tx_dm;        /* DM frame tx buffer */

    BOOL                dmm_tx_active;                          /* Don't send new DMM until previous DMM is done */
    u32                 dm_tx_cnt;

    u32                 tw_delays[VTSS_MEP_SUPP_DM_MAX];
    u32                 tw_cnt;
    u32                 fn_delays[VTSS_MEP_SUPP_DM_MAX];
    u32                 fn_cnt;
    u32                 nf_delays[VTSS_MEP_SUPP_DM_MAX];
    u32                 nf_cnt;

    u32                 dmr_rx_tout_cnt;
    u32                 tw_rx_err_cnt;
    u32                 fn_rx_err_cnt;
    u32                 nf_rx_err_cnt;

    mesa_timestamp_t    tx_dmm_fup_timestamp;                   /* DMM transmission time (follow-up) */
    mesa_timestamp_t    rx_dmr_timestamp;                       /* DMR receive time      */
    mesa_timestamp_t    rx_dm1_timestamp;                       /* DM1 receive time      */
} dm_state_t;

typedef struct
{
    vtss_mep_supp_conf_t           config;                  /* Input from upper logic */
    vtss_mep_supp_ccm_conf_t       ccm_config;
    vtss_mep_supp_gen_conf_t       ccm_gen;
    BOOL                           ccm_rdi;
    vtss_mep_supp_lmm_conf_t       lmm_config;
    vtss_mep_supp_slm_conf_t       slm_config;
    vtss_mep_supp_dmm_conf_t       dmm_config;
    vtss_mep_supp_dm1_conf_t       dm1_config;
    vtss_mep_supp_aps_conf_t       aps_config;
    u8                             aps_txdata[VTSS_MEP_SUPP_RAPS_DATA_LENGTH];
    BOOL                           aps_tx;
    BOOL                           aps_event;
    BOOL                           aps_forward;
    BOOL                           ais_enable;
    vtss_mep_supp_ltm_conf_t       ltm_config;
    vtss_mep_supp_lbm_conf_t       lbm_config;
    vtss_mep_supp_ais_conf_t       ais_config;
    vtss_mep_supp_lck_conf_t       lck_config;
    vtss_mep_supp_tst_conf_t       tst_config;

    //vtss_mep_supp_lm_counters_t    ccm_lm_counter;          /* Output to upper logic */
    u8                             peer_mac[VTSS_MEP_SUPP_PEER_MAX][VTSS_MEP_SUPP_MAC_LENGTH];
    //vtss_mep_supp_lm_counters_t    lmm_lm_counter;
    vtss_mep_supp_defect_state_t   defect_state;
    ccm_state_t                    ccm_state;
    dm_state_t                     dm_state[VTSS_MEP_SUPP_PRIO_MAX];
    supp_lm_state_t                lm_state[VTSS_MEP_SUPP_PEER_MAX];
    u32                            lm_cosid;
    vtss_mep_supp_lb_status_t      lb_state;
    u32                            lbr_cnt;
    vtss_mep_supp_lbr_t            lbr[VTSS_MEP_SUPP_LBR_MAX];
    u32                            ltr_cnt;
    vtss_mep_supp_ltr_t            ltr[VTSS_MEP_SUPP_LTR_MAX];

    u32                            tx_lmm_buffer_size;
    u64                            tst_tx_cnt;
    u64                            tst_rx_cnt; /* used by MPLS */

    u64                            event_flags;                            /* Flags used to indicate what input events has activated the 'run' thread */

    u32                            tx_header_size;                         /* Size of complete transmitted header (index for start of OAM PDU) */
    u32                            tx_header_size_aps;                     /* Size of complete APS transmitted header (index for start of OAM PDU) */
    u32                            tx_dmr_buffer_size;
    u32                            tx_lmr_buffer_size;
    u32                            tx_isdx;                                /* Transmit ISDX for VOE based MEP */
    CapArray<u32, VTSS_APPL_CAP_MEP_RX_ISDX_SIZE> rx_isdx;                 /* Receive ISDX for VOE based MEP */
    u32                            tx_isdx_tst;
    u8                             exp_meg[MEG_ID_LENGTH];                 /* expected MA-ID/MEG-ID */
    u32                            exp_meg_len;                            /* length of expected MEG */

    BOOL                           defect_timer_active;                    /* Indicating that a defect timer is active */
    BOOL                           ccm_defect_active;                      /* Indicating that a VOE CCM defect is active driving a "fast" poll of CCM PDU */
    u32                            loc_timer_val;
    u32                            ccm_timer_val;
    u32                            aps_timer_val;
    u32                            lmm_timer_val;
    u32                            lbm_count;
    u32                            aps_count;
    u32                            ais_count, ais_inx;
    u32                            lck_count, lck_inx;
    u32                            dmr_fup_port;
    i32                            dmr_dev_delay;

    u32                            is2_rx_cnt;                             /* This is for Serval to compensate MIP PDU copied by IS2 to CPU and therefor is not counted by Up-MEP VOE */

    defect_timer_t                 dLevel;                                 /* Defect timers */
    defect_timer_t                 dMeg;
    defect_timer_t                 dMep;
    defect_timer_t                 dPeriod[VTSS_MEP_SUPP_PEER_MAX];
    defect_timer_t                 dPrio[VTSS_MEP_SUPP_PEER_MAX];
    defect_timer_t                 dAis;
    defect_timer_t                 dLck;
    defect_timer_t                 dLoop;
    defect_timer_t                 dConfig;

    u32                            tx_ccm_timer;                           /* Assorted timers */
    u32                            tx_aps_timer;
    u32                            tx_lmm_timer;
    u32                            tx_lbm_timer;
    u32                            tx_dmm_timer;
    u32                            tx_dm1_timer;
    u32                            rx_dmr_timeout_timer;
    u32                            tx_ais_timer;
    u32                            tx_lck_timer;
    u32                            rx_aps_timer;
    u32                            rx_lbm_timer;
    u32                            rx_ais_timer;
    u32                            rx_lck_timer;
    u32                            rx_ccm_timer[VTSS_MEP_SUPP_PEER_MAX];
    u32                            rdi_timer;
    u32                            tst_timer;

    BOOL                           hw_ccm_transmitting;                    /* This is added temporary - I hope - in order to only transmit HW CCM once */
    u32                            voe_idx;                                /* VOE instance index - only relevant for VOE capable HW */
    BOOL                           voe_config;                             /* Indicating that the VOE is configured - to avoid repeating VOE config with same values */
    BOOL                           lm_flow_count;                          /* LM is counting traffic (service frames) per flow - all priority */
    vtss_mep_supp_oam_count        lm_oam_count;                           /* LM is counting OAM PDUs as traffic or not */
    BOOL                           ccm_start;
    BOOL                           lbm_start;
    BOOL                           tst_start;
    BOOL                           lm_start[VTSS_MEP_SUPP_PEER_MAX];

    u8                             *tx_ccm_sw;                             /* SW CCM frame tx buffer */
    u8                             *tx_ccm_hw;                             /* HW CCM frame tx buffer */
    u8                             *tx_aps;                                /* APS frame tx buffer */
    u8                             *tx_lmm;                                /* LMM frame tx buffer */
    u8                             *tx_lmr;                                /* LMR frame tx buffer */
    u8                             *tx_lbr;                                /* LBR frame tx buffer */
    u8                             *tx_dmr;                                /* DMR frame tx buffer */
    u8                             *tx_ais;                                /* AIS frame tx buffer */
    u8                             *tx_lck;                                /* LCK frame tx buffer */
    u8                             *tx_tst[VTSS_MEP_SUPP_PRIO_MAX][VTSS_MEP_SUPP_TEST_MAX]; /* TST frame tx buffer */
    u8                             *tx_lbm[VTSS_MEP_SUPP_PRIO_MAX][VTSS_MEP_SUPP_TEST_MAX]; /* LBM frame tx buffer */

    vtss_mep_supp_ts_done_t        ts_done_dm1;                            /* 1DM timestamp done handler */
    vtss_mep_supp_ts_done_t        ts_done_dmr;                            /* DMR timestamp done handler */

    vtss_mep_supp_onestep_extra_t  onestep_extra_m;                        /* One step extra buffer for dmm/dm1 */
    vtss_mep_supp_onestep_extra_t  onestep_extra_r;                        /* One step extra buffer for dmr */

    mesa_timestamp_t               rxTimef;                                /* DMM receive time returned in DMR */
    mesa_timestamp_t               txTimeb;                                /* DMR transmit time extracted from DMR */
    BOOL                           dmr_ts_ok;                              /* set when timestamps received, cleared when read */






    CapArray<u32,VTSS_APPL_CAP_MEP_MCE_IDX_USED_MAX> mce_idx_used;
} supp_instance_data_t;

static CapArray<supp_instance_data_t, VTSS_APPL_CAP_MEP_INSTANCE_MAX> instance_data;  /* MEP instance data */
static u32                   timer_res;                                    /* Timer resolution */
static u8                    lm_null_counter[12];
static u32                   pdu_period_to_timer[8];
static vtss_mep_supp_rate_t  pdu_period_to_rate[8] = {VTSS_MEP_SUPP_RATE_INV,
                                                      VTSS_MEP_SUPP_RATE_300S,
                                                      VTSS_MEP_SUPP_RATE_100S,
                                                      VTSS_MEP_SUPP_RATE_10S,
                                                      VTSS_MEP_SUPP_RATE_1S,
                                                      VTSS_MEP_SUPP_RATE_6M,
                                                      VTSS_MEP_SUPP_RATE_1M,
                                                      VTSS_MEP_SUPP_RATE_6H};
static CapArray<u32, MESA_CAP_OAM_VOE_CNT> voe_to_mep;
static BOOL     voe_down_mep_present(mesa_port_list_t &port,  u32 vid,  u32 *voe_idx);
static u32      sat_dm_vid = 0;

/* VOE Up-MEP loop port - this is initialized from platform */
static u32      supp_mep_pag = 0;
static u32      supp_mip_pag = 0;
static mesa_oam_voe_event_mask_t voe_event_mask;

/****************************************************************************/
/*  MEP local functions                                                     */
/****************************************************************************/

#define var_to_string(string, var) {(string)[0] = (var&0xFF000000)>>24;   (string)[1] = (var&0x00FF0000)>>16;   (string)[2] = (var&0x0000FF00)>>8;   (string)[3] = (var&0x000000FF);}

#define string_to_var(string)  (((string)[0]<<24) | ((string)[1]<<16) | ((string)[2]<<8) | (string)[3])

#define c_s_tag(frame)  (((*(frame) == 0x81) && (*((frame)+1) == 0x00)) || ((*(frame) == 0x88) && (*((frame)+1) == 0xA8)))

#define init_frame_info(frame_info) \
            frame_info.bypass = TRUE;\
            frame_info.line_rate = FALSE;\
            frame_info.maskerade_port = VTSS_PORT_NO_NONE;\
            frame_info.isdx = VTSS_MEP_SUPP_INDX_INVALID;\
            frame_info.vid = 0;\
            frame_info.qos = TX_QOS_NORMAL;\
            frame_info.pcp = 0;\
            frame_info.dp = 0



static void run_async_ccm_gen(u32 instance);
static void run_async_lmm_config(u32 instance);
static void run_async_slm_config(u32 instance);
static void run_async_aps_config(u32 instance);
static void run_dmm_config(u32 instance);
static void run_1dm_config(u32 instance);
static void run_ltm_config(u32 instance);
static u32 run_lbm_config(u32 instance, u64 *active_time_ms);
static void run_lck_config(u32 instance);
static void run_ais_set(u32 instance);
static u32 run_tst_config(u32 instance, u64 *active_time_ms);
static void run_tx_aps_timer(u32 instance);
static void voe_config(supp_instance_data_t *data);
static BOOL ccm_defect_calc(u32 instance,   supp_instance_data_t *data,   u8 smac[],   u8 pdu[],   u32 prio,   BOOL *defect,   BOOL *state,   BOOL *mac);
static void extract_tlv_info(u8 pdu[], supp_instance_data_t *data,  BOOL *new_is);


static BOOL evc_vlan_up(supp_instance_data_t *data) {
    if (mep_caps.mesa_mep_serval) {
        return (data->config.flow.mask & (VTSS_MEP_SUPP_FLOW_MASK_EVC_ID | VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID)) && (data->config.direction == VTSS_MEP_SUPP_UP);
    }
    return data->config.voe != 0; /* This is always FALSE on not Serval - this is only done like this as Lint complains if this is just FALSE */
}

static BOOL vlan_down(supp_instance_data_t *data) {
    if (mep_caps.mesa_mep_serval) {
        return (data->config.flow.mask & VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID) && (data->config.direction == VTSS_MEP_SUPP_DOWN);
    }
    return data->config.voe != 0; /* This is always FALSE on not Serval - this is only done like this as Lint complains if this is just FALSE */
}

static BOOL evc_vlan_down(supp_instance_data_t *data) {
    if (mep_caps.mesa_mep_serval) {
        return (data->config.flow.mask & (VTSS_MEP_SUPP_FLOW_MASK_EVC_ID | VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID)) && (data->config.direction == VTSS_MEP_SUPP_DOWN);
    }
    return data->config.voe != 0; /* This is always FALSE on not Serval - this is only done like this as Lint complains if this is just FALSE */
}

static void insert_c_s_tag(u8 *header,   u32 tpid,   u32 vid,   u32 prio,   BOOL dei)
{
    header[0] = (tpid & 0xFF00) >> 8;
    header[1] = (tpid & 0xFF);
    header[2] = ((vid & 0x0F00) >> 8) | (prio << 5) | ((dei) ? 0x10 : 0);
    header[3] = (vid & 0xFF);
}

static u32 prio_tx_header_size(u32 instance, u32 prio, u32 opcode)
{   /* This is calculating the software injected TX header size depending on SAT and priority */
    u32 size;
    if (instance_data[instance].config.sat) {   /* This is SAT instance the header size depends on tagging in this priority */
        size = 2* VTSS_MEP_SUPP_MAC_LENGTH;    /* SMAC and DMAC */
        if ((opcode == OAM_TYPE_1DM) || (opcode == OAM_TYPE_DMM))    /* SAT DM injection requires extra DM identifying tag */
            size+=4;
        if (instance_data[instance].config.sat_conf[prio].vid != VTSS_MEP_SUPP_SAT_VID_UNTAGGED)  /* This is not untagged */
            size+=4;
        size+=2;    /* 0x8902 */
    } else {    /* This is NOT SAT - all frame with same header */
        size = instance_data[instance].tx_header_size;
    }

    return(size);
}

static void insert_reply_prio(supp_instance_data_t *data, u8 *frame, u32 pcp, BOOL dei, u32 cos)
{
    if ((data->config.flow.mask & VTSS_MEP_SUPP_FLOW_MASK_EVC_ID) && (data->config.direction == VTSS_MEP_SUPP_UP) && (data->config.mode == VTSS_MEP_SUPP_MEP)) {      /* EVC Up-MEP */
        if ((frame[12+0] != 0x89) || (frame[12+1] != 0x02)) {   /* Dummy tag must be updated with COS */
            frame[12+2] = (frame[12+2] & ~0xF0) | (cos << 5) | ((dei ? 1 : 0) << 4);
        }
        if ((frame[12+4] != 0x89) || (frame[12+5] != 0x02)) {   /* Subscriber tag must be upfdated with pcp and dei */
            frame[12+6] = (frame[12+6] & ~0xF0) | (pcp << 5) | ((dei ? 1 : 0) << 4);
        }
    } else {
        if ((frame[12+0] != 0x89) || (frame[12+1] != 0x02)) {   /* Tag must be upfdated with pcp and dei */
            frame[12+2] = (frame[12+2] & ~0xF0) | (pcp << 5) | ((dei ? 1 : 0) << 4);
        }
    }
}












#define inst_is_mpls(x) (FALSE)





























































































































































































































































































































static u32 init_tx_frame(u32 instance, u8 dmac[], u8 smac[], u32 prio, BOOL dei, u8 frame[], u32 opcode, u32 reply_cos = REPLY_COS_NONE)
{
    u32 size=0;
    vtss_mep_supp_conf_t     *config = &instance_data[instance].config;
    mesa_vlan_conf_t          vlan_sconf;
    mesa_etype_t              s_etype;
    mesa_vlan_port_type_t     port_type;





































    memcpy(&frame[size], dmac, VTSS_MEP_SUPP_MAC_LENGTH);
    memcpy(&frame[size+VTSS_MEP_SUPP_MAC_LENGTH], smac, VTSS_MEP_SUPP_MAC_LENGTH);

    size += 2* VTSS_MEP_SUPP_MAC_LENGTH;

    (void)mesa_vlan_conf_get(NULL, &vlan_sconf); /* Get the custom S-Port EtherType */

    s_etype = vlan_sconf.s_etype;
    port_type = ((config->flow.mask & VTSS_MEP_SUPP_FLOW_MASK_CUOUTVID) == VTSS_MEP_SUPP_FLOW_MASK_CUOUTVID) ? MESA_VLAN_PORT_TYPE_S_CUSTOM :
                (config->flow.mask & VTSS_MEP_SUPP_FLOW_MASK_SOUTVID) ? MESA_VLAN_PORT_TYPE_S :
                MESA_VLAN_PORT_TYPE_C;

#define ether_type() ((port_type == MESA_VLAN_PORT_TYPE_S_CUSTOM) ? s_etype : (port_type == MESA_VLAN_PORT_TYPE_S) ? 0x88A8 : 0x8100)

    if (mep_caps.mesa_mep_serval) {
        u32                   prio_in, dei_in, cosid;
        mesa_qos_port_conf_t  qos_conf;
        mesa_vlan_port_conf_t vlan_conf;
        mesa_qos_port_tag_conf_t     *tag = &qos_conf.tag;
        mesa_qos_port_pcp_dei_conf_t *map;

        (void)mesa_qos_port_conf_get(NULL, config->port, &qos_conf);

        if (config->flow.mask & VTSS_MEP_SUPP_FLOW_MASK_EVC_ID) {  /* This is a EVC MEP/MIP */
            if ((config->mode == VTSS_MEP_SUPP_MEP) && (config->direction == VTSS_MEP_SUPP_UP)) {  /* This is a SW/VOE EVC Up-MEP - 'Dummy tag' must be added */
                vlan_conf.port_type = MESA_VLAN_PORT_TYPE_UNAWARE;      /* Type of dummy tag is depending on the VLAN port configuration */
                (void)mesa_vlan_port_conf_get(NULL, config->port, &vlan_conf);
                port_type = vlan_conf.port_type;

                if (!config->sat && (tag->class_enable)) {    /* Do not do this mapping in case of SAT. QOS classification on tag is enabled - it should be */
                    dei_in = dei;
                    prio_in = prio;
                    for (prio=0; prio<VTSS_PRIO_ARRAY_SIZE; ++prio) {   /* Search for the QOS mapping that gives correct classified qos and dp - This is only relevant if the PMF_4 fix is active. If not active the QOS port mapping must be 1:1 */
                        dei = 0;
                        map = &tag->pcp_dei_map[prio][dei];
                        if (map->prio == prio_in && map->dpl == dei_in)     break;
                        dei = 1;
                        map = &tag->pcp_dei_map[prio][dei];
                        if (map->prio == prio_in && map->dpl == dei_in)     break;
                    }
                    if (prio >= VTSS_PRIO_ARRAY_SIZE) {  /* Relevant mapping was not found - This should not happen */
                        prio = prio_in;
                        dei = dei_in;
                    }
                }

                if (!config->sat) {  /* This is NOT SAT injection */
                    if (reply_cos != REPLY_COS_NONE) {
                        cosid = reply_cos;
                    } else {
                        cosid = prio;
                    }
                insert_c_s_tag(&frame[size], ether_type(), (config->evc_type == VTSS_MEP_SUPP_LEAF) ? config->flow.leaf_vid : config->flow.vid, cosid, dei);   /* If not SAT - insert outer dummy tag with EVC internal VID */
                    size+=4;
                    if (config->flow.mask & VTSS_MEP_SUPP_FLOW_MASK_CINVID) {
                        insert_c_s_tag(&frame[size], 0x8100, config->flow.in_vid, prio, dei);   /* If C inner VID is present then OAM is behind subscriber tag */
                        size+=4;
                    }
                    if (!config->voe && (opcode == OAM_TYPE_TST)) {    /* SW TST UP injection requires extra TST identifying tag */
                        insert_c_s_tag(&frame[size], 0x8100, 0, 0, 0);
                        size+=4;
                    }
                } else {    /* This is SAT injection the VID is per priority */
                    if ((opcode == OAM_TYPE_1DM) || (opcode == OAM_TYPE_DMM)) {    /* SAT DM injection requires extra DM identifying tag */
                        insert_c_s_tag(&frame[size], 0x8100, sat_dm_vid, prio, dei);
                        size+=4;
                    }
                    if (config->sat_conf[prio].vid != VTSS_MEP_SUPP_SAT_VID_UNTAGGED) { /* This is not untagged */
                        insert_c_s_tag(&frame[size], 0x8100, config->sat_conf[prio].vid, prio, dei);
                        size+=4;
                    }
                }
            } else if ((config->mode == VTSS_MEP_SUPP_MIP) && (config->direction == VTSS_MEP_SUPP_UP)) {  /* This is a SW EVC Up-MIP (subscriber MIP) */
                insert_c_s_tag(&frame[size], vtss_mep_supp_port_ether_type(config->port), config->flow.in_vid, prio, dei);  /* Subscriber MIB always inject with residence port ether type */
                size+=4;
















            } else if (config->voe && (config->mode == VTSS_MEP_SUPP_MEP) && (config->direction == VTSS_MEP_SUPP_DOWN)) {
                /* This is a VOE EVC Down-MEP */
                if (config->flow.mask & VTSS_MEP_SUPP_FLOW_MASK_CINVID) {
                    insert_c_s_tag(&frame[size], 0x8100, config->flow.in_vid, prio, dei);   /* If C inner VID is present then OAM is behind subscriber tag */
                    size+=4;
                }
            } else if ((config->mode == VTSS_MEP_SUPP_MIP) && (config->direction == VTSS_MEP_SUPP_DOWN)) {  /* This is a SW EVC Down-MIP (subscriber MIP) */
                insert_c_s_tag(&frame[size], vtss_mep_supp_port_ether_type(config->port), config->flow.out_vid, prio, dei);  /* Subscriber MIB always inject with residence port ether type */
                size+=4;
            }
        } else if (config->flow.mask & VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID) {  /* This is a VLAN MEP */
            if ((config->direction == VTSS_MEP_SUPP_UP) && (opcode != OAM_TYPE_RAPS)) {  /* This is a SW/VOE VLAN Up-MEP that is not transmitting RAPS - 'Dummy tag' must be added */
                vlan_conf.port_type = MESA_VLAN_PORT_TYPE_UNAWARE;      /* Type of dummy tag is depending on the VLAN port configuration */
                (void)mesa_vlan_port_conf_get(NULL, config->port, &vlan_conf);
                port_type = vlan_conf.port_type;

                insert_c_s_tag(&frame[size], ether_type(), config->flow.vid, prio, dei);
                size+=4;
                if (!config->voe && (opcode == OAM_TYPE_TST)) {    /* SW TST UP injection requires extra TST identifying tag */
                    insert_c_s_tag(&frame[size], 0x8100, 0, 0, 0);
                    size+=4;
                }
            }
        }
        else {  /* This is a Port MEP */
            if (config->flow.mask & (VTSS_MEP_SUPP_FLOW_MASK_COUTVID | VTSS_MEP_SUPP_FLOW_MASK_SOUTVID)) {
                insert_c_s_tag(&frame[size], ether_type(), config->flow.out_vid, prio, dei);
                size+=4;
            }
        }
    } else {
        if (config->flow.mask & (VTSS_MEP_SUPP_FLOW_MASK_COUTVID | VTSS_MEP_SUPP_FLOW_MASK_SOUTVID)) {
            insert_c_s_tag(&frame[size], ether_type(), config->flow.out_vid, prio, dei);
            size+=4;
        }

        if (config->flow.mask & (VTSS_MEP_SUPP_FLOW_MASK_CINVID | VTSS_MEP_SUPP_FLOW_MASK_SINVID)) {
            insert_c_s_tag(&frame[size], (config->flow.mask & VTSS_MEP_SUPP_FLOW_MASK_CINVID) ? 0x8100 : 0x88A8, config->flow.in_vid, prio, dei);
            size+=4;
        }
    }

    frame[size] = 0x89;
    frame[size+1] = 0x02;
    size+=2;

    memset(&frame[size], 0, 4); /* Clear Common OAM PDU Format */

    return(size);
}

static u32 init_client_tx_frame(u32 instance,   u8 dmac[],   u8 smac[],   u32 prio,   BOOL dei,   u8 frame[])
{
    u32 size=0;
    vtss_mep_supp_conf_t *config = &instance_data[instance].config;
    vtss_mep_supp_flow_t *flow = &config->flow;





    if (frame == instance_data[instance].tx_ais)    /* Check for transmitting AIS */
        flow = &(instance_data[instance].ais_config.flows[instance_data[instance].ais_inx]);
    if (frame == instance_data[instance].tx_lck) {  /* Check for transmitting LCK */
        flow = &(instance_data[instance].lck_config.flows[instance_data[instance].lck_inx]);










    }















    memcpy(&frame[size], dmac, VTSS_MEP_SUPP_MAC_LENGTH);
    memcpy(&frame[size+VTSS_MEP_SUPP_MAC_LENGTH], smac, VTSS_MEP_SUPP_MAC_LENGTH);

    size += 2* VTSS_MEP_SUPP_MAC_LENGTH;

    if (mep_caps.mesa_mep_serval) {
        if (flow->mask & VTSS_MEP_SUPP_FLOW_MASK_EVC_ID) { /* The client flow is EVC */
        }
        if (flow->mask & VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID) { /* The client flow is VLAN */
        }
    } else {
        if (flow->mask & VTSS_MEP_SUPP_FLOW_MASK_COUTVID) {
            insert_c_s_tag(&frame[size], 0x8100, flow->out_vid, prio, dei);
            size+=4;
        }
        else
            if (flow->mask & VTSS_MEP_SUPP_FLOW_MASK_SOUTVID) {
                insert_c_s_tag(&frame[size], 0x88A8, flow->out_vid, prio, dei);
                size+=4;
            }
        if (flow->mask & VTSS_MEP_SUPP_FLOW_MASK_CINVID) {
            insert_c_s_tag(&frame[size], 0x8100, flow->in_vid, prio, dei);
            size+=4;
        }
        else
            if (flow->mask & VTSS_MEP_SUPP_FLOW_MASK_SINVID) {
                insert_c_s_tag(&frame[size], 0x88A8, flow->in_vid, prio, dei);
                size+=4;
            }
    }
    frame[size] = 0x89;
    frame[size+1] = 0x02;
    size+=2;

    memset(&frame[size], 0, 4); /* Clear Common OAM PDU Format */

    return(size);
}

static u32 rx_tag_cnt_calc(supp_instance_data_t  *data,  u32 prio)
{
    u32 retval = 0;

    if (!data->config.sat || (prio >= VTSS_MEP_SUPP_PRIO_MAX)) {    /* This is NOT SAT */
        retval += (data->config.flow.mask & (VTSS_MEP_SUPP_FLOW_MASK_COUTVID | VTSS_MEP_SUPP_FLOW_MASK_SOUTVID)) ? 1 : 0;
        retval += (data->config.flow.mask & (VTSS_MEP_SUPP_FLOW_MASK_CINVID | VTSS_MEP_SUPP_FLOW_MASK_SINVID)) ? 1 : 0;
    } else {    /* This is SAT - number of tags are priority dependent - untagged has one tag - tagged has two tags */
        retval = 1;
        retval += (data->config.sat_conf[prio].vid != VTSS_MEP_SUPP_SAT_VID_UNTAGGED) ? 1 : 0;
    }

    return(retval);
}

static void init_pdu_period_to_timer(void)
{
    pdu_period_to_timer[0] = 0;
    pdu_period_to_timer[1] = ((12/timer_res)+1);          /* period 3.33ms -> 12 ms */
    pdu_period_to_timer[2] = ((35/timer_res)+1);          /* period 10ms -> 35 ms */
    pdu_period_to_timer[3] = ((350/timer_res)+1);         /* period 100ms -> 350 ms */
    pdu_period_to_timer[4] = ((3500/timer_res)+1);        /* period 1s. -> 1.000ms -> 35000 ms */
    pdu_period_to_timer[5] = ((35000/timer_res)+1);       /* period 10s. -> 10.000ms -> 35 ms */
    pdu_period_to_timer[6] = ((210000/timer_res)+1);      /* period 1min. -> 60.000ms -> 35 ms */
    pdu_period_to_timer[7] = ((2100000/timer_res)+1);     /* period 10 min. -> 600.000ms -> 35 ms */
}

static u32 tx_timer_calc(vtss_mep_supp_rate_t   rate)
{
    switch (rate)
    {
        case VTSS_MEP_SUPP_RATE_INV:   return(0);
        case VTSS_MEP_SUPP_RATE_300S:  return(10/timer_res);
        case VTSS_MEP_SUPP_RATE_100S:  return(10/timer_res);
        case VTSS_MEP_SUPP_RATE_10S:   return(100/timer_res);
        case VTSS_MEP_SUPP_RATE_1S:    return(1000/timer_res);
        case VTSS_MEP_SUPP_RATE_6M:    return(10*1000/timer_res);
        case VTSS_MEP_SUPP_RATE_1M:    return(60*1000/timer_res);
        case VTSS_MEP_SUPP_RATE_6H:    return(600*1000/timer_res);
        default:                       return(0);
    }
}

static u8 rate_to_pdu_calc(vtss_mep_supp_rate_t   rate)
{
    switch (rate)
    {
        case VTSS_MEP_SUPP_RATE_INV:   return(0);
        case VTSS_MEP_SUPP_RATE_300S:  return(1);
        case VTSS_MEP_SUPP_RATE_100S:  return(2);
        case VTSS_MEP_SUPP_RATE_10S:   return(3);
        case VTSS_MEP_SUPP_RATE_1S:    return(4);
        case VTSS_MEP_SUPP_RATE_6M:    return(5);
        case VTSS_MEP_SUPP_RATE_1M:    return(6);
        case VTSS_MEP_SUPP_RATE_6H:    return(7);
        default:                       return(4);
    }
}

static u32 frame_rate_calc(vtss_mep_supp_rate_t   rate)
{
    switch (rate)
    {
        case VTSS_MEP_SUPP_RATE_INV:   return(0);
        case VTSS_MEP_SUPP_RATE_300S:  return(300);
        case VTSS_MEP_SUPP_RATE_100S:  return(100);
        case VTSS_MEP_SUPP_RATE_10S:   return(10);
        case VTSS_MEP_SUPP_RATE_1S:    return(1);
        default:                       return(0);
    }
}

static BOOL hw_ccm_calc(vtss_mep_supp_rate_t  rate)
{
    return((rate == VTSS_MEP_SUPP_RATE_300S) || (rate == VTSS_MEP_SUPP_RATE_100S) || (rate == VTSS_MEP_SUPP_RATE_10S));
}

static u32 megid_calc(supp_instance_data_t  *data, u8 *buf)
{
    u32  n_len, m_len, inx=0;

    switch (data->ccm_config.format) {
        case VTSS_MEP_SUPP_ITU_ICC:
            buf[0] = 1;
            buf[1] = 32;
            buf[2] = 13;
            memcpy(&buf[3], data->ccm_config.meg, 13);
            inx = 16;
            break;
        case VTSS_MEP_SUPP_ITU_CC_ICC:
            buf[0] = 1;
            buf[1] = 33;
            buf[2] = 15;
            memcpy(&buf[3], data->ccm_config.meg, 15);
            inx = 18;
            break;
        case VTSS_MEP_SUPP_IEEE_STR:
            n_len = strlen(data->ccm_config.name);
            m_len = strlen(data->ccm_config.meg);
            if (n_len) { /* Maintenance Domain Name present */
                buf[inx++] = 4;
                buf[inx++] = n_len;
                memcpy(&buf[inx], data->ccm_config.name, n_len);
                inx += n_len;
            }
            else
                buf[inx++] = 1; /* No Maintenance Domain Name */

            buf[inx++] = 2;    /* Add Short MA name */
            buf[inx++] = m_len;
            memcpy(&buf[inx], data->ccm_config.meg, m_len);
            inx += m_len;
            break;
    }

    return(inx);    /* Return length of MEG-ID */
}

static BOOL test_enable_calc(const vtss_mep_supp_test_conf_t  tests[VTSS_MEP_SUPP_PRIO_MAX][VTSS_MEP_SUPP_TEST_MAX])
{
    u32 i, j;

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Calculate if any TST transmission is active */
        for (j=0; j<VTSS_MEP_SUPP_TEST_MAX; ++j) {
            if (tests[i][j].enable) {
                return TRUE;
            }
        }
    }
    return FALSE;
}

static BOOL test_enable_more_calc(const vtss_mep_supp_test_conf_t  tests[VTSS_MEP_SUPP_PRIO_MAX][VTSS_MEP_SUPP_TEST_MAX])
{
    u32 i, j, count=0;

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Calculate if any TST transmission is active */
        for (j=0; j<VTSS_MEP_SUPP_TEST_MAX; ++j) {
            if (tests[i][j].enable) {
                if (++count > 1) {
                    return TRUE;
                }
            }
        }
    }
    return FALSE;
}

static void test_enable_clear(vtss_mep_supp_test_conf_t  tests[VTSS_MEP_SUPP_PRIO_MAX][VTSS_MEP_SUPP_TEST_MAX])
{
    u32 i, j;

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {
        for (j=0; j<VTSS_MEP_SUPP_TEST_MAX; ++j) {
            tests[i][j].enable = FALSE;
        }
    }
}

static BOOL test_check(const vtss_mep_supp_test_conf_t  tests[VTSS_MEP_SUPP_PRIO_MAX][VTSS_MEP_SUPP_TEST_MAX])
{
    u32 i, j;

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Check each TST flow configuration */
        for (j=0; j<VTSS_MEP_SUPP_TEST_MAX; ++j) {
            if (tests[i][j].enable) {
                if (tests[i][j].size > VTSS_MEP_SUPP_TEST_SIZE_MAX)          return(TRUE);
                if (tests[i][j].size < VTSS_MEP_SUPP_TEST_SIZE_MIN)          return(TRUE);
                if (tests[i][j].rate > VTSS_MEP_SUPP_TEST_RATE_MAX)          return(TRUE);
                if (tests[i][j].rate < 1)                                    return(TRUE);
            }
        }
    }
    return (FALSE);
}

static BOOL dm_enable_calc(const BOOL  enable[VTSS_MEP_SUPP_PRIO_MAX])
{
    u32 i;

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Calculate if any TST transmission is active */
        if (enable[i]) {
            return TRUE;
        }
    }
    return FALSE;
}

static void dm_enable_clear(BOOL  enable[VTSS_MEP_SUPP_PRIO_MAX])
{
    u32 i;

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {
        enable[i] = FALSE;
    }
}

static BOOL get_dm_tunit(supp_instance_data_t *data)
{
    if (dm_enable_calc(data->dmm_config.enable))
        return (data->dmm_config.tunit);
    else
        return (data->dm1_config.tunit);
}

static BOOL handle_dmr(supp_instance_data_t *data,  dm_state_t *state)
{
    i32   total_delay, dm_delay;
    u32   i;
    BOOL  is_ns;

    if (!data->dmm_config.proprietary)
    {

        is_ns =  get_dm_tunit(data);
        total_delay = vtss_mep_supp_delay_calc(&state->rx_dmr_timestamp,
                                               &state->tx_dmm_fup_timestamp,
                                               is_ns);

        if ((total_delay >= 0) && (data->dmr_dev_delay >= 0))
        {
            /* If delay < 0, it means something wrong. Just skip it */
            dm_delay = total_delay - data->dmr_dev_delay;

            if (dm_delay >= 0 && dm_delay < 1e9 /* timeout is 1 second */)
            {   /* If dm_delay <= 0, it means something wrong. Just skip it. */
                state->tw_delays[state->tw_cnt % VTSS_MEP_SUPP_DM_MAX] = dm_delay;
                state->tw_cnt++;
            }
            else
            {
                vtss_mep_supp_trace("DMR Invalid dm_delay", total_delay, data->dmr_dev_delay, 0, 0);
                state->tw_rx_err_cnt++;
            }
        }
        else
        {
            if (total_delay < 0)
                vtss_mep_supp_trace("DMR Invalid total_delay", total_delay, 0, 0, 0);
            if (data->dmr_dev_delay < 0)
                vtss_mep_supp_trace("DMR Invalid dmr_dev_delay", data->dmr_dev_delay, 0, 0, 0);
            state->tw_rx_err_cnt++;
        }
    }
    else if (data->dmm_config.proprietary && data->dmm_config.calcway == VTSS_MEP_SUPP_FLOW)
    {
        /* two-way with followup - flow measurement */
        /* Save the rx timestamp for calculation when flowup packet arrives */

        return FALSE;
    }

    state->dmm_tx_active = FALSE;

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Check if any transmission is still ongoing */
        if (data->dmm_config.enable[i] && data->dm_state[i].dmm_tx_active) {
            break;
        }
    }

    if (i == VTSS_MEP_SUPP_PRIO_MAX) {
        data->rx_dmr_timeout_timer = 0;
    }

    return TRUE;
}

static void dm1_fup_tx(u32 instance, u32 prio, mesa_timestamp_t *tx_time)
{
    u32                           tx_header_size = prio_tx_header_size(instance, prio, OAM_TYPE_1DM_FUP);
    supp_instance_data_t          *data          = &instance_data[instance];
    dm_state_t                    *state         = &data->dm_state[prio];
    vtss_mep_supp_tx_frame_info_t tx_frame_info;
    u32                           rc;

    state->tx_dm[tx_header_size + 1] = OAM_TYPE_1DM_FUP;
    var_to_string(&state->tx_dm[tx_header_size + 4], tx_time->seconds);
    var_to_string(&state->tx_dm[tx_header_size + 8], tx_time->nanoseconds);

    init_frame_info(tx_frame_info);

    rc = vtss_mep_supp_packet_tx(instance, NULL, data->config.flow.port, tx_header_size + DM1_PDU_LENGTH, state->tx_dm, &tx_frame_info, VTSS_MEP_SUPP_RATE_INV, FALSE, 0, VTSS_MEP_SUPP_OAM_TYPE_NONE, data->config.sat);
    if (rc != VTSS_RC_OK) {
        vtss_mep_supp_trace("dm1_fup_tx(): packet tx failed", 0, 0, 0, 0);
    }

    // PDU type back to normal
    state->tx_dm[tx_header_size + 1] = OAM_TYPE_1DM;
}

static void dm1_fup_done(vtss_mep_supp_ts_done_t *ts_done, mesa_timestamp_t *tx_time)
{
    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
    dm1_fup_tx(ts_done->instance, ts_done->prio, tx_time);
    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
}

static void dmr_fup_tx(u32 instance, mesa_timestamp_t *tx_time)
{
    supp_instance_data_t          *data = &instance_data[instance];
    vtss_mep_supp_tx_frame_info_t tx_frame_info;
    u32                           rc;

    /* DMR is out. Send the follow-up packet now */
    data->tx_dmr[data->tx_header_size + 1] = OAM_TYPE_DMR_FUP;
    var_to_string(&data->tx_dmr[data->tx_header_size + 20], tx_time->seconds);
    var_to_string(&data->tx_dmr[data->tx_header_size + 24], tx_time->nanoseconds);

    init_frame_info(tx_frame_info);

    rc = vtss_mep_supp_packet_tx_1(instance, NULL, data->dmr_fup_port, data->tx_header_size + DMR_PDU_LENGTH, data->tx_dmr, &tx_frame_info, VTSS_MEP_SUPP_OAM_TYPE_NONE);
    if (!rc)       vtss_mep_supp_trace("fup_done: packet tx failed", 0, 0, 0, 0);

    // PDU type back to normal
    data->tx_dmr[data->tx_header_size + 1] = OAM_TYPE_DMR;
}

static void dmr_fup_done(vtss_mep_supp_ts_done_t *ts_done, mesa_timestamp_t *tx_time)
{
    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
    dmr_fup_tx(ts_done->instance, tx_time);
    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
}

static void instance_data_clear(u32 instance,  supp_instance_data_t *data)
{
    u32 i;

    *data = {};

    if (mep_caps.mesa_mep_serval) {
        data->voe_idx = mep_caps.mesa_oam_voe_cnt;
    }

    data->loc_timer_val = pdu_period_to_timer[rate_to_pdu_calc(VTSS_MEP_SUPP_RATE_1S)];   /* Default LOC timer is equal to 1 sec period */

    for (i=0; i<mep_caps.appl_mep_rx_isdx_size; ++i)
        data->rx_isdx[i] = VTSS_MEP_SUPP_INDX_INVALID;
    data->tx_isdx = VTSS_MEP_SUPP_INDX_INVALID;
    data->tx_isdx_tst = VTSS_MEP_SUPP_INDX_INVALID;

    for (i = 0; i< mep_caps.mesa_oam_peer_cnt; ++i)
        data->lm_start[i] = TRUE;
}

static mesa_oam_period_t voe_period_calc(vtss_mep_supp_rate_t  rate)
{
    switch (rate) {
        case VTSS_MEP_SUPP_RATE_300S: return(MESA_OAM_PERIOD_3_3_MS);
        case VTSS_MEP_SUPP_RATE_100S: return(MESA_OAM_PERIOD_10_MS);
        case VTSS_MEP_SUPP_RATE_10S:  return(MESA_OAM_PERIOD_100_MS);
        case VTSS_MEP_SUPP_RATE_1S:   return(MESA_OAM_PERIOD_1_SEC);
        default:                      return(MESA_OAM_PERIOD_INV);
    }
}

static BOOL voe_ccm_enable(supp_instance_data_t *data)
{
    if (mep_caps.mesa_mep_serval) {
        return ((data->config.voe && (data->voe_idx < mep_caps.mesa_oam_voe_cnt) && (data->ccm_config.peer_count == 1) && (voe_period_calc(data->ccm_config.rate) != MESA_OAM_PERIOD_INV)) ? TRUE : FALSE);
    }
    return FALSE;
}

static void voe_config(supp_instance_data_t *data)
{
    if (mep_caps.mesa_mep_serval) {
        u32                    voe_idx;
        BOOL                   oam_count;
        mesa_oam_voe_conf_t    cfg;
        mesa_oam_proc_status_t voe_status;

        if (data->voe_config)   return;
        if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt)   return;

        (void)mesa_oam_voe_conf_get(NULL, data->voe_idx, &cfg);

        cfg.voe_type = (data->config.flow.mask & (VTSS_MEP_SUPP_FLOW_MASK_EVC_ID | VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID)) ? MESA_OAM_VOE_SERVICE : MESA_OAM_VOE_PORT;    /* Calculate if VOE is a Port or a EVC or VLAN VOE */
        memcpy(cfg.unicast_mac.addr, data->config.mac, sizeof(cfg.unicast_mac.addr));
        if (data->config.mode == VTSS_MEP_SUPP_MIP)
            cfg.mep_type = MESA_OAM_MIP;
        else
        if (data->config.direction == VTSS_MEP_SUPP_DOWN)
            cfg.mep_type = MESA_OAM_DOWNMEP;
        else
            cfg.mep_type = MESA_OAM_UPMEP;
        cfg.svc_to_path = FALSE;

        if (data->config.enable && (data->config.direction == VTSS_MEP_SUPP_UP) && voe_down_mep_present(data->config.flow.port, data->config.flow.vid, &voe_idx)) {  /* This EVC Up-MEP enabled - must point to any present VOE Down-MEP */
            cfg.svc_to_path = TRUE;
            cfg.svc_to_path_idx_w = voe_idx;
        }
        if ((data->config.flow.mask & VTSS_MEP_SUPP_FLOW_MASK_PATH_P) && (instance_data[data->config.flow.path_mep_p].voe_idx < mep_caps.mesa_oam_voe_cnt))
            cfg.svc_to_path_idx_p = instance_data[data->config.flow.path_mep_p].voe_idx;

        cfg.loop_isdx_w = (data->tx_isdx == VTSS_MEP_SUPP_INDX_INVALID) ? 0 : data->tx_isdx;
    //    cfg.loop_isdx_p =
        cfg.loop_portidx_p = data->config.flow.port_p;

        cfg.proc.meg_level = data->config.level;
        cfg.proc.dmac_check_type = MESA_OAM_DMAC_CHECK_BOTH;
        cfg.proc.ccm_check_only = FALSE;
        cfg.proc.copy_next_only = voe_ccm_enable(data);
        cfg.proc.copy_on_ccm_err = TRUE;            /* Hit me once on fail as we want to track failing edge defect values */
        cfg.proc.copy_on_mel_too_low_err = TRUE;    /* Hit me once on low level for same reason */
        cfg.proc.copy_on_ccm_more_than_one_tlv = FALSE;
        cfg.proc.copy_on_dmac_err = FALSE;
        memset(cfg.generic, 0, sizeof(cfg.generic));
        cfg.generic[GENERIC_AIS].enable = TRUE;           /* AIS */
        cfg.generic[GENERIC_AIS].copy_to_cpu = TRUE;
        cfg.generic[GENERIC_AIS].forward = FALSE;
        cfg.generic[GENERIC_AIS].count_as_selected = FALSE;
        cfg.generic[GENERIC_AIS].count_as_data = FALSE;
        cfg.generic[GENERIC_LCK].enable = TRUE;           /* LCK */
        cfg.generic[GENERIC_LCK].copy_to_cpu = TRUE;
        cfg.generic[GENERIC_LCK].forward = FALSE;
        cfg.generic[GENERIC_LCK].count_as_selected = FALSE;
        cfg.generic[GENERIC_LCK].count_as_data = FALSE;
        cfg.generic[GENERIC_LAPS].enable = TRUE;           /* LAPS */
        cfg.generic[GENERIC_LAPS].copy_to_cpu = TRUE;
        cfg.generic[GENERIC_LAPS].forward = data->aps_forward;
        cfg.generic[GENERIC_LAPS].count_as_selected = FALSE;
        cfg.generic[GENERIC_LAPS].count_as_data = TRUE;
        cfg.generic[GENERIC_RAPS].enable = TRUE;           /* RAPS */
        cfg.generic[GENERIC_RAPS].copy_to_cpu = TRUE;
        cfg.generic[GENERIC_RAPS].forward = data->aps_forward;
        cfg.generic[GENERIC_RAPS].count_as_selected = FALSE;
        cfg.generic[GENERIC_RAPS].count_as_data = TRUE;
        cfg.generic[GENERIC_SLM].enable = TRUE;            /* SLM */
        cfg.generic[GENERIC_SLM].copy_to_cpu = TRUE;
        cfg.generic[GENERIC_SLM].forward = FALSE;
        cfg.generic[GENERIC_SLM].count_as_selected = FALSE;
        cfg.generic[GENERIC_SLM].count_as_data = FALSE;
        cfg.generic[GENERIC_SLR].enable = TRUE;            /* SLR */
        cfg.generic[GENERIC_SLR].copy_to_cpu = TRUE;
        cfg.generic[GENERIC_SLR].forward = FALSE;
        cfg.generic[GENERIC_SLR].count_as_selected = FALSE;
        cfg.generic[GENERIC_SLR].count_as_data = FALSE;
        cfg.generic[GENERIC_1SL].enable = TRUE;            /* 1SL */
        cfg.generic[GENERIC_1SL].copy_to_cpu = TRUE;
        cfg.generic[GENERIC_1SL].forward = FALSE;
        cfg.generic[GENERIC_1SL].count_as_selected = FALSE;
        cfg.generic[GENERIC_1SL].count_as_data = FALSE;

        cfg.unknown.enable = FALSE;
        cfg.unknown.copy_to_cpu = FALSE;
        cfg.unknown.count_as_selected = FALSE;
        cfg.unknown.count_as_data = FALSE;

        /* CCM */
        cfg.ccm.enable = voe_ccm_enable(data);
        cfg.ccm.copy_to_cpu =  TRUE;
        cfg.ccm.forward = FALSE;
        cfg.ccm.count_as_selected = FALSE;
        cfg.ccm.count_as_data = !(data->ccm_gen.enable && data->ccm_gen.lm_enable);  /* Dual-ended LM is not enabled - count CCM as data */
        cfg.ccm.peer_mepid = data->ccm_config.peer_mep[0];
        memset(cfg.ccm.megid.data, 0, sizeof(cfg.ccm.megid.data));
        (void)megid_calc(data, cfg.ccm.megid.data);
        cfg.ccm.tx_seq_no_auto_upd_op = MESA_OAM_AUTOSEQ_INCREMENT_AND_UPDATE;
        if (data->ccm_start) {
            data->ccm_start = FALSE;
            cfg.ccm.tx_seq_no = 0;
            cfg.ccm.rx_seq_no = 0;
        }
        cfg.ccm.rx_seq_no_check = TRUE;
        cfg.ccm.rx_priority = data->ccm_config.prio;
        cfg.ccm.rx_period = voe_period_calc(data->ccm_config.rate);

        /* CCM LM */
        cfg.ccm_lm.enable = data->ccm_gen.enable && data->ccm_gen.lm_enable;
        cfg.ccm_lm.copy_to_cpu = data->ccm_gen.enable && data->ccm_gen.lm_enable;
        cfg.ccm_lm.forward = FALSE;
        cfg.count.priority_mask = ((cfg.voe_type == MESA_OAM_VOE_SERVICE) && !data->lm_flow_count) ? 0x1FF : 0x000;
        cfg.count.yellow = TRUE;
        cfg.ccm_lm.count_as_selected = TRUE; /* The count_as_selected counter is used to count the tx packets for LM*/
        cfg.ccm_lm.period = voe_period_calc(data->ccm_gen.lm_rate);

        /* LMM LM */
        cfg.single_ended_lm.enable = TRUE;
        cfg.single_ended_lm.copy_lmm_to_cpu = ((cfg.mep_type == MESA_OAM_UPMEP) && !data->config.out_of_service) ? TRUE : FALSE;
        cfg.single_ended_lm.copy_lmr_to_cpu = TRUE;
        cfg.single_ended_lm.forward_lmm = FALSE;
        cfg.single_ended_lm.forward_lmr = FALSE;
        cfg.count.priority_mask = ((cfg.voe_type == MESA_OAM_VOE_SERVICE) && !data->lm_flow_count) ? 0x1FF : 0x000;
        cfg.count.yellow = TRUE;
        cfg.single_ended_lm.count_as_selected = FALSE;
        cfg.single_ended_lm.count_as_data = FALSE;

        /* LB */
        cfg.lb.enable = !(data->tst_config.enable_rx || test_enable_calc(data->tst_config.tests));
        cfg.lb.copy_lbr_to_cpu = (test_enable_calc(data->lbm_config.tests) && (data->lbm_config.to_send != VTSS_MEP_SUPP_LBM_TO_SEND_INFINITE)) ? TRUE : FALSE;
        cfg.lb.copy_lbm_to_cpu = ((cfg.mep_type == MESA_OAM_UPMEP) && !data->config.out_of_service) ? TRUE : FALSE;
        cfg.lb.forward_lbm = FALSE;
        cfg.lb.forward_lbr = FALSE;
        cfg.lb.count_as_selected = FALSE;
        cfg.lb.count_as_data = FALSE;
        cfg.lb.tx_update_transaction_id = TRUE;
        (void)mesa_oam_proc_status_get(NULL, data->voe_idx, &voe_status);   /* Set TX and RX transaction id to next transmitted */
        cfg.lb.tx_transaction_id = voe_status.tx_next_lbm_transaction_id;
        cfg.lb.rx_transaction_id = voe_status.tx_next_lbm_transaction_id;
        if (data->lbm_start) {
            (void)mesa_oam_voe_counter_clear(NULL, data->voe_idx, MESA_OAM_CNT_LB);
            data->lbm_start = FALSE;
        }

        /* TST */
        cfg.tst.enable = data->tst_config.enable_rx || test_enable_calc(data->tst_config.tests);
        cfg.tst.forward = FALSE;
        cfg.tst.count_as_selected = FALSE;
        cfg.tst.count_as_data = (data->config.flow.mask & VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID) ? TRUE : FALSE;
        cfg.tst.tx_seq_no_auto_update = TRUE;
        if (data->tst_start) {
            if (data->tst_config.enable_rx)
                cfg.tst.copy_to_cpu = TRUE;
            data->tst_start = FALSE;
            cfg.tst.tx_seq_no = data->tst_config.transaction_id;
            cfg.tst.rx_seq_no = data->tst_config.transaction_id;
        }

        /* DM */
        cfg.dm.enable_dmm = TRUE;
        cfg.dm.enable_1dm = TRUE;
        cfg.dm.copy_dmm_to_cpu = ((cfg.mep_type == MESA_OAM_UPMEP) && !data->config.out_of_service) ? TRUE : FALSE;
        cfg.dm.copy_1dm_to_cpu = TRUE;
        cfg.dm.copy_dmr_to_cpu = TRUE;
        cfg.dm.forward_1dm = FALSE;
        cfg.dm.forward_dmm = FALSE;
        cfg.dm.forward_dmr = FALSE;
        cfg.dm.count_as_selected = FALSE;
        cfg.dm.count_as_data = FALSE;

        /* LT */
        cfg.lt.enable = TRUE;
        cfg.lt.copy_ltm_to_cpu = TRUE;
        cfg.lt.copy_ltr_to_cpu = TRUE;
        cfg.lt.forward_ltm = FALSE;
        cfg.lt.forward_ltr = FALSE;
        cfg.lt.count_as_selected = FALSE;
        cfg.lt.count_as_data = FALSE;

        if (!data->config.sat) {
            cfg.upmep.discard_rx = ((cfg.mep_type == MESA_OAM_UPMEP) && data->config.out_of_service) ? TRUE : FALSE;
            cfg.upmep.loopback = cfg.upmep.discard_rx;
        } else {
            cfg.upmep.discard_rx = TRUE;
            cfg.upmep.loopback = FALSE;
        }
        cfg.upmep.port = data->config.port;

        if (data->lm_oam_count != VTSS_MEP_SUPP_OAM_COUNT_Y1731) {  /* At this point OAM PDU counting is configured as Y.1731 - this can be changed */
            oam_count = (data->lm_oam_count == VTSS_MEP_SUPP_OAM_COUNT_ALL) ? TRUE : FALSE;

            cfg.generic[GENERIC_AIS].count_as_data = oam_count;
            cfg.generic[GENERIC_LCK].count_as_data = oam_count;
            cfg.generic[GENERIC_LAPS].count_as_data = oam_count;
            cfg.generic[GENERIC_RAPS].count_as_data = oam_count;
            cfg.unknown.count_as_data = oam_count;
            cfg.ccm.count_as_data = oam_count;
            cfg.single_ended_lm.count_as_data = oam_count;
            cfg.lb.count_as_data = oam_count;
            cfg.tst.count_as_data = oam_count;
            cfg.dm.count_as_data = oam_count;
            cfg.lt.count_as_data = oam_count;
        }

        (void)mesa_oam_voe_conf_set(NULL, data->voe_idx, &cfg);

        if (voe_ccm_enable(data))
            (void)mesa_oam_voe_event_enable(NULL, data->voe_idx, voe_event_mask, TRUE);

        data->voe_config = TRUE;
    }
    return;
}

static u32 run_sync_config(u32 instance,   const vtss_mep_supp_conf_t  *const conf)
{
    supp_instance_data_t *data;
    mesa_oam_voe_alloc_cfg_t alloc_cfg;
    data = &instance_data[instance];    /* Instance data reference */

    if (mep_caps.mesa_mep_serval) {
        if (conf->voe) {   /* VOE based MEP */
            if (data->voe_idx < mep_caps.mesa_oam_voe_cnt)    return(VTSS_MEP_SUPP_RC_OK);    /* VOE is already allocated */

            if (conf->flow.mask & (VTSS_MEP_SUPP_FLOW_MASK_EVC_ID | VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID)) { /* This VOE relates to a EVC or VLAN - Service VOE is required */
                if (mesa_oam_voe_alloc(NULL, MESA_OAM_VOE_SERVICE, NULL, &data->voe_idx) != VTSS_RC_OK)
                    return(VTSS_MEP_SUPP_RC_NO_RESOURCES);
            }
            else {
                /* This requires a VOE port instance */
                alloc_cfg.phys_port = conf->port;
                if (mesa_oam_voe_alloc(NULL, MESA_OAM_VOE_PORT, &alloc_cfg, &data->voe_idx) != VTSS_RC_OK)
                    return(VTSS_MEP_SUPP_RC_NO_RESOURCES);
            }
            voe_to_mep[data->voe_idx] = instance;
        }
    }

    return(VTSS_MEP_SUPP_RC_OK);
}

static u8 tx_tag_cnt_calc(u32  tx_header_size)
{
    u32 tag_cnt;

    tag_cnt = (tx_header_size - ((2* VTSS_MEP_SUPP_MAC_LENGTH) + 2)) / 4;

    return (tag_cnt);
}

/* One is added in this calculation in order to avoid MCE id 0 - equal to MESA_MCE_ID_LAST */
#define MCE_ID_CALC(mep, port, cos, inject) (0x80000000 | ((mep&0xFF)<<16) | ((port&0xFF)<<8) | ((cos&0x0F)<<4) | (inject&0x03))
#define INJECT_EXTRACT 0
#define INJECT_INJECT 1
#define INJECT_EXTRACT_DOUBLE 2
#define MCE_ID_CALC_SAT_DM(cos) (0x81000000 | cos)
#define MCE_ID_CALC_TT_LOOP 0x82000000
#define MCE_ID_CALC_TST(mep, port) (0x83000000 | ((mep&0xFF)<<8) | (port&0xFF))

u32 vtss_mep_supp_tt_loop_mce_id_get()
{
    return(MCE_ID_CALC_TT_LOOP);
}

BOOL shared_mel_calculate(vtss_mep_supp_flow_t *flow,  vtss_mep_supp_flow_t *iflow)
{
    BOOL port_mep = (flow->mask & (VTSS_MEP_SUPP_FLOW_MASK_EVC_ID  | VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID)) ? FALSE : TRUE;
    BOOL iport_mep = (iflow->mask & (VTSS_MEP_SUPP_FLOW_MASK_EVC_ID  | VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID)) ? FALSE : TRUE;

    if (port_mep)   return FALSE;
    if (iport_mep)  return ((iflow->out_vid == flow->out_vid) && (iflow->in_vid == flow->in_vid)) ? TRUE : FALSE;

    return (iflow->mask & flow->mask & (VTSS_MEP_SUPP_FLOW_MASK_EVC_ID | VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID)) &&
           (iflow->vid == flow->vid) && (iflow->out_vid == flow->out_vid) && (iflow->in_vid == flow->in_vid) ? TRUE : FALSE;
}

static u8 level_value_mask_calc(supp_instance_data_t *data,  u32 level,  u32 *mask)
{
    u32                  i, low_level, retval = 0;
    supp_instance_data_t *idata;
    vtss_mep_supp_flow_t *iflow, *flow;

    flow = &data->config.flow;
    low_level = 0;

    for (i=0; i<VTSS_MEP_SUPP_CREATED_MAX; i++) { /* Find lower level on this vid */
        idata = &instance_data[i];    /* Instance data reference */
        iflow = &idata->config.flow;

        if (idata->config.enable && (flow->port[idata->config.port] || (idata->config.port == data->config.port)) &&
            shared_mel_calculate(flow, iflow) &&
            (idata->config.level < level) && (idata->config.level >= low_level))
            low_level = idata->config.level+1;
    }

    retval = (0x01 << low_level) - 1;  /* Level value is calculated */
    *mask = (0x01 << (level - low_level)) - 1;  /* "don't care" mask value is calculated */
    *mask <<= low_level;    /* mask is rotated to cover levels */
    *mask = ~*mask;         /* Mask is complimented as "don't care" is '0' */

    return(retval & 0xFF);
}


static BOOL voe_up_mep_present(u32 vid,  u32 *voe_idx)
{
    u32 i;

    for (i=0; i < mep_caps.mesa_oam_path_service_voe_cnt; ++i)
        if ((voe_to_mep[i] < VTSS_MEP_SUPP_CREATED_MAX) && instance_data[voe_to_mep[i]].config.enable &&
            (instance_data[voe_to_mep[i]].config.flow.vid == vid) && (instance_data[voe_to_mep[i]].config.direction == VTSS_MEP_SUPP_UP)) {
            *voe_idx = i;
            return(TRUE);
        }

    *voe_idx = MESA_OAM_VOE_IDX_NONE;
    return(FALSE);
}

static BOOL voe_down_mep_present(mesa_port_list_t &port,  u32 vid,  u32 *voe_idx)
{
    u32 i, mep;
    supp_instance_data_t     *data;

    for (i=0; i < mep_caps.mesa_oam_path_service_voe_cnt; ++i) {
        mep = voe_to_mep[i];
        data = &instance_data[mep];    /* Instance data reference */

        if ((mep < VTSS_MEP_SUPP_CREATED_MAX) && data->config.enable && port[data->config.port] &&
            (data->config.flow.vid == vid) && (data->config.direction == VTSS_MEP_SUPP_DOWN)) {
            *voe_idx = i;
            return(TRUE);
        }
    }

    *voe_idx = MESA_OAM_VOE_IDX_NONE;
    return(FALSE);
}

static BOOL up_mip_present(u32 vid,  u32 *instance)
{
    u32 i;

    for (i=0; i<VTSS_MEP_SUPP_CREATED_MAX; ++i)
        if (instance_data[i].config.enable && (instance_data[i].config.mode == VTSS_MEP_SUPP_MIP) &&
            (instance_data[i].config.flow.vid == vid) && (instance_data[i].config.direction == VTSS_MEP_SUPP_UP)) {
            *instance = i;
            return(TRUE);
        }

    *instance = 0;
    return(FALSE);
}

static BOOL etree_elan_mep_present(u32 evc_id, u32 *instance)
{
    u32 i, j;

    if (instance != NULL)
        *instance = VTSS_MEP_SUPP_CREATED_MAX;
    for (i=0; i<VTSS_MEP_SUPP_CREATED_MAX; ++i) { /* Search for E-TREE or E-LAN MEP in this EVC that has extraction rules created */
        if (instance_data[i].config.enable && VTSS_MEP_SUPP_ETREE_ELAN(instance_data[i].config.evc_type) && (instance_data[i].config.flow.flow_id == evc_id)) {
            if ((instance != NULL) && (*instance == VTSS_MEP_SUPP_CREATED_MAX))
                *instance = i;  /* E-TREE or E-LAN instance found */

            for (j=0; j<mep_caps.mesa_port_cnt; ++j)
                if (instance_data[i].rx_isdx[j] != VTSS_MEP_SUPP_INDX_INVALID)
                    break;
            if (j < mep_caps.mesa_port_cnt)
                return(TRUE);    /* This instance has extraction rules created */
        }
    }

    return(FALSE);
}

static u32 mep_on_highest_level(u32 vid)
{
    u32 i, instance, level;

    level = 0;

    instance = VTSS_MEP_SUPP_CREATED_MAX;
    for (i=0; i<VTSS_MEP_SUPP_CREATED_MAX; i++) /* Find instance on highest level on this vid */
        if (instance_data[i].config.enable && (instance_data[i].config.mode == VTSS_MEP_SUPP_MEP) && (instance_data[i].config.flow.mask & VTSS_MEP_SUPP_FLOW_MASK_EVC_ID) &&
            (instance_data[i].config.flow.vid == vid) && (instance_data[i].config.level >= level)) {
            instance = i;
            level = instance_data[i].config.level;
        }

    return(instance);
}

static void port_on_vlan_id(u32 vid, mesa_port_list_t &port)
{
    u32 i, j;

    for (i=0; i<VTSS_MEP_SUPP_CREATED_MAX; i++) {/* Find VLAN MEP instance on this vid */
        if (instance_data[i].config.enable && (instance_data[i].config.mode == VTSS_MEP_SUPP_MEP) && (instance_data[i].config.flow.mask & VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID) &&
            (instance_data[i].config.flow.vid == vid)) {    /* This is a VLAN MEP on this VID */
            for (j=0; j<mep_caps.mesa_port_cnt; ++j)
                if (instance_data[i].config.flow.port[j])   /* All flow ports are port on this VLAN */
                    port[j] = TRUE;
            port[instance_data[i].config.port] = TRUE;      /* Residence port is port on this VLAN */
        }
    }
}

#define MCE_IDX_USED_SAVE(id) {if (mce_idx_used_idx < mep_caps.appl_mep_mce_idx_used_max) {data->mce_idx_used[mce_idx_used_idx++] = id;}}

/*lint -e{454, 455, 456} ... The mutex is locked so it is ok to unlock */
static u32 mce_rule_config(u32 instance)
{
    supp_instance_data_t     *data;
    u32                      rc, i, nni, voe_idx, mask, vid, mce_idx_used_idx;
    mesa_evc_oam_port_conf_t evc_conf;
    mesa_vlan_port_conf_t    vlan_conf;
    mesa_mce_port_info_t     mce_info;
    mesa_mce_t               mce;

    vtss_mep_supp_trace("mce_rule_config  enter  instance", instance, 0, 0, 0);

    data = &instance_data[instance];    /* Instance data reference */
    data->tx_isdx = VTSS_MEP_SUPP_INDX_INVALID;
    for (i=0; i<mep_caps.appl_mep_rx_isdx_size; ++i) {
        data->rx_isdx[i] = VTSS_MEP_SUPP_INDX_INVALID;
    }

    /* Delete all MCE for this instance */
    for (i=0; i<mep_caps.appl_mep_mce_idx_used_max; ++i) {
        if (data->mce_idx_used[i] != 0)   (void)mesa_mce_del(NULL, data->mce_idx_used[i]);
    }
    data->mce_idx_used.clear();
    mce_idx_used_idx = 0;

    for (nni=0; nni<mep_caps.mesa_port_cnt; ++nni)  if (data->config.flow.port[nni])    break;    /* Find first port that is a NNI */
    if (nni >= mep_caps.mesa_port_cnt)    return VTSS_MEP_SUPP_RC_NO_RESOURCES;

    if (data->config.enable && !data->config.voe && (data->config.flow.mask & VTSS_MEP_SUPP_FLOW_MASK_EVC_ID)) { /* EVC SW MEP/MIP NOT based on VOE but MCE only */
        if (data->config.mode == VTSS_MEP_SUPP_MEP) {   /* This is a EVC SW MEP */
            if (data->config.direction == VTSS_MEP_SUPP_UP) {   /* This is an EVC SW Up-MEP */
                (void)mesa_mce_init(NULL, &mce);            /************************* Create injection MCE rule *******************************/

                //vlan_conf.port_type = MESA_VLAN_PORT_TYPE_UNAWARE;      /* Type of dummy tag is depending on the VLAN port configuration */
                //(void)mesa_vlan_port_conf_get(NULL, data->config.port, &vlan_conf);

                mce.id = MCE_ID_CALC(instance, data->config.port, 0, 1);

                vid = (data->config.evc_type == VTSS_MEP_SUPP_LEAF) ? data->config.flow.leaf_vid : data->config.flow.vid;
                mce.key.port_list[data->config.port] = TRUE;
                mce.key.tag.tagged = MESA_VCAP_BIT_1;       /* Outer "Dummy" TAG */
                mce.key.tag.s_tagged = MESA_VCAP_BIT_ANY;
                mce.key.tag.vid.value = vid;
                mce.key.tag.vid.mask = 0xFFFF;
                mce.key.tag.dei = MESA_VCAP_BIT_ANY;
                mce.key.inner_tag.tagged = MESA_VCAP_BIT_0; /* NO Inner TAG */
                mce.key.inner_tag.s_tagged = MESA_VCAP_BIT_0;
                mce.key.inner_tag.dei = MESA_VCAP_BIT_0;
                mce.key.mel.value = (0x1 << data->config.level) - 1;
                mce.key.mel.mask = 0xFF;
                mce.key.injected = MESA_VCAP_BIT_1;

                if (VTSS_MEP_SUPP_ETREE_ELAN(data->config.evc_type)) { /* An E-TREE or E-LAN Up-MEP must have rules per NNI port */
                    memcpy (mce.action.port_list, data->config.flow.port, sizeof(mce.action.port_list));
                    mce.action.evc_etree = TRUE;
                }
                mce.action.evc_id = data->config.flow.flow_id;
                mce.action.voe_idx = MESA_OAM_VOE_IDX_NONE;
                if (voe_up_mep_present(vid, &voe_idx))    /* This MCE should only point to a VOE if an VOE based Up-MEP exist */
                    mce.action.voe_idx = voe_idx;
                else
                if (voe_down_mep_present(data->config.flow.port, vid, &voe_idx))  /* This MCE should only point to a VOE if an VOE based Down-MEP exist */
                    mce.action.voe_idx = voe_idx;
                mce.action.vid = vid;
                mce.action.pop_cnt = 1;
                mce.action.tx_lookup = MESA_MCE_TX_LOOKUP_VID;
                mce.action.oam_detect = MESA_MCE_OAM_DETECT_NONE;
                mce.action.isdx = MESA_MCE_ISDX_NEW;

                if (mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* ADD the MCE injection rule */
                MCE_IDX_USED_SAVE(mce.id)
                if (mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* Get the transmit ISDX */
                data->tx_isdx = mce_info.isdx;
            }

            if (!VTSS_MEP_SUPP_ETREE_ELAN(data->config.evc_type) || !etree_elan_mep_present(data->config.flow.flow_id, NULL)) { /* Only one set of E-TREE or E-LAN extraction rules in this EVC */
                do {
                    (void)mesa_mce_init(NULL, &mce);    /********************************* Create extraction MCE rule *******************************/

                    mce.id = MCE_ID_CALC(instance, nni, 0, 0);

                    if (VTSS_MEP_SUPP_ETREE_ELAN(data->config.evc_type)) { /* An E-TREE or E-LAN Up-MEP must have rules per NNI port */
                        mce.key.port_list[nni] = TRUE;
                    } else {
                        memcpy (mce.key.port_list, data->config.flow.port, sizeof(mce.key.port_list));
                    }
                    if (data->config.direction == VTSS_MEP_SUPP_UP) {  /* This is an EVC SW Up-MEP */
                        mce.action.evc_counting = TRUE;
                        mce.action.evc_id = data->config.flow.flow_id;
                    }
                    mce.key.lookup = 1;                         /* Hit in second lookup. The first lookup is hitting the Service entry */
                    mce.key.tag.tagged = MESA_VCAP_BIT_1;       /* Outer EVC TAG */
                    mce.key.tag.s_tagged = MESA_VCAP_BIT_ANY;
                    mce.key.tag.vid.value = data->config.flow.out_vid;
                    mce.key.tag.vid.mask = 0xFFFF;
                    mce.key.tag.dei = MESA_VCAP_BIT_ANY;
                    mce.key.inner_tag.tagged = MESA_VCAP_BIT_0; /* NO Inner TAG */
                    mce.key.inner_tag.s_tagged = MESA_VCAP_BIT_0;
                    mce.key.inner_tag.dei = MESA_VCAP_BIT_0;
                    mce.key.mel.value = level_value_mask_calc(data, data->config.level, &mask);
                    mce.key.mel.mask = mask;
                    mce.key.injected = MESA_VCAP_BIT_0;

                    /* mce.action.port_list is empty */
                    mce.action.voe_idx = MESA_OAM_VOE_IDX_NONE;
                    mce.action.pop_cnt = MESA_MCE_POP_NONE;
                    mce.action.tx_lookup = MESA_MCE_TX_LOOKUP_VID;
                    mce.action.oam_detect = MESA_MCE_OAM_DETECT_NONE;
                    mce.action.policy_no = supp_mep_pag;
                    mce.action.isdx = MESA_MCE_ISDX_NEW;

                    if (mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* ADD the MCE extraction rule */
                    MCE_IDX_USED_SAVE(mce.id)
                    if (mesa_mce_port_info_get(NULL, mce.id, nni, &mce_info) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* Get the receive ISDX */
                    data->rx_isdx[VTSS_MEP_SUPP_ETREE_ELAN(data->config.evc_type) ? nni : 0] = mce_info.isdx;

                    if (VTSS_MEP_SUPP_ETREE(data->config.evc_type)) { /* An E-TREE MEP must extract/forward in the leaf VLAN also */
                        mce.id = MCE_ID_CALC(instance, nni, 1, 0);
                        mce.key.tag.vid.value = data->config.flow.leaf_out_vid;
                        mce.action.isdx = mce_info.isdx;

                        if (mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* ADD the MCE extraction rule */
                        MCE_IDX_USED_SAVE(mce.id)
                    }

                    for (nni+=1; nni<mep_caps.mesa_port_cnt; ++nni)  if (data->config.flow.port[nni])    break;    /* Find next port that is a NNI */
                } while(VTSS_MEP_SUPP_ETREE_ELAN(data->config.evc_type) && (nni < mep_caps.mesa_port_cnt)); /* In case of E-TREE or E-LAN run for all NNIs */

                if (VTSS_MEP_SUPP_ETREE_ELAN(data->config.evc_type)) { /* E-TREE or E-LAN extraction rules are created - notify upper logic */
                    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
                    rc = vtss_mep_supp_new_etree_elan_extraction(instance);
                    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
                    if (rc != VTSS_MEP_RC_OK)   return VTSS_MEP_SUPP_RC_NO_RESOURCES;
                }
            }
        } else {   /* This is a SW MIP */
            (void)mesa_mce_init(NULL, &mce);            /********************************* Create extraction MCE rule *******************************/

            mce.id = MCE_ID_CALC(instance, nni, 0, 0);

            memcpy (mce.key.port_list, data->config.flow.port, sizeof(mce.key.port_list));
            mce.key.lookup = 1;                         /* Hit in second lookup. The first lookup is hitting the Service entry */
            mce.key.tag.tagged = MESA_VCAP_BIT_1;       /* Outer EVC TAG */
            mce.key.tag.s_tagged = MESA_VCAP_BIT_ANY;
            mce.key.tag.vid.value = data->config.flow.out_vid;
            mce.key.tag.vid.mask = 0xFFFF;
            mce.key.tag.dei = MESA_VCAP_BIT_ANY;
            mce.key.inner_tag.tagged = (data->config.direction == VTSS_MEP_SUPP_UP) ? MESA_VCAP_BIT_1 : MESA_VCAP_BIT_0;
            mce.key.inner_tag.s_tagged = MESA_VCAP_BIT_ANY;
            mce.key.inner_tag.vid.value = data->config.flow.in_vid;
            mce.key.inner_tag.vid.mask = (mce.key.inner_tag.tagged == MESA_VCAP_BIT_1) ? 0xFFFF : 0;
            mce.key.inner_tag.dei = MESA_VCAP_BIT_ANY;
            mce.key.mel.value = (0x1 << data->config.level) - 1;
            mce.key.mel.mask = 0xFF;
            mce.key.injected = MESA_VCAP_BIT_0;

            /* mce.action.port_list is empty */
            mce.action.voe_idx = MESA_OAM_VOE_IDX_NONE;
            mce.action.pop_cnt = (data->config.direction == VTSS_MEP_SUPP_UP) ? 1 : 0;
            mce.action.tx_lookup = MESA_MCE_TX_LOOKUP_VID;
            mce.action.oam_detect = MESA_MCE_OAM_DETECT_NONE;
            mce.action.policy_no = supp_mip_pag;
            mce.action.isdx = MESA_MCE_ISDX_NONE;

            if (mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* ADD the MCE extraction rule */
            MCE_IDX_USED_SAVE(mce.id)
        }
    }

    if (data->config.enable && !(data->config.flow.mask & (VTSS_MEP_SUPP_FLOW_MASK_EVC_ID  | VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID))) { /* Port MEP is enabled  */
        (void)mesa_mce_init(NULL, &mce);            /******************************* Create extraction MCE rule ***************************/

        mce.id = MCE_ID_CALC(instance, data->config.port, 0, 0);

        mce.key.port_list[data->config.port] = TRUE;
        mce.key.tag.tagged = (data->config.flow.mask & (VTSS_MEP_SUPP_FLOW_MASK_COUTVID | VTSS_MEP_SUPP_FLOW_MASK_SOUTVID)) ? MESA_VCAP_BIT_1 : MESA_VCAP_BIT_0;
        mce.key.tag.s_tagged = MESA_VCAP_BIT_ANY;
        mce.key.tag.vid.value = data->config.flow.out_vid;
        mce.key.tag.vid.mask = (mce.key.tag.tagged == MESA_VCAP_BIT_1) ? 0xFFFF : 0;
        mce.key.tag.pcp.mask = 0x00;
        mce.key.tag.dei = MESA_VCAP_BIT_ANY;
        mce.key.inner_tag.tagged = MESA_VCAP_BIT_0; /* NO Inner TAG */
        mce.key.inner_tag.s_tagged = MESA_VCAP_BIT_0;
        mce.key.inner_tag.dei = MESA_VCAP_BIT_0;
        mce.key.mel.mask = 0x00;
        mce.key.injected = MESA_VCAP_BIT_0;

        /* mce.action.port_list is empty */
        mce.action.voe_idx = MESA_OAM_VOE_IDX_NONE;
        mce.action.vid = data->config.flow.vid;
        mce.action.pop_cnt = 1;
        mce.action.tx_lookup = MESA_MCE_TX_LOOKUP_VID;
        mce.action.oam_detect = (data->config.voe) ? ((mce.key.tag.tagged == MESA_VCAP_BIT_1) ? MESA_MCE_OAM_DETECT_SINGLE_TAGGED : MESA_MCE_OAM_DETECT_UNTAGGED) : MESA_MCE_OAM_DETECT_NONE;
        if (!data->config.voe)
            mce.action.policy_no = supp_mep_pag;
//        mce.action.prio_enable = TRUE;
//        mce.action.prio = 7;
        mce.action.isdx = (data->config.voe) ? MESA_MCE_ISDX_NEW : MESA_MCE_ISDX_NONE;

        if (mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* ADD the MCE extraction rule */
        MCE_IDX_USED_SAVE(mce.id)
        if (data->config.voe) {
            if (mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* Get the receive ISDX */
            data->rx_isdx[0] = mce_info.isdx;
        }
    }





























































































































































































































































































































































































    if (data->config.enable && data->config.voe && (data->config.flow.mask & VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID)) { /* VLAN MEP is enabled and is VOE based */
        evc_conf.voe_idx = data->voe_idx;

        /* Create the MCE entries required to handle Service OAM */
        if (data->config.direction == VTSS_MEP_SUPP_UP) {   /* This VLAN MEP is an VOE Up-MEP */
            (void)mesa_mce_init(NULL, &mce);            /************************* Create injection MCE rule *******************************/

            vlan_conf.port_type = MESA_VLAN_PORT_TYPE_UNAWARE;      /* Type of dummy tag is depending on the VLAN port configuration */
            (void)mesa_vlan_port_conf_get(NULL, data->config.port, &vlan_conf);

            mce.id = MCE_ID_CALC(instance, data->config.port, 0, 1);

            mce.key.port_list[data->config.port] = TRUE;
            mce.key.tag.tagged = MESA_VCAP_BIT_1;       /* Outer "Dummy" TAG */
            mce.key.tag.s_tagged = (vlan_conf.port_type == MESA_VLAN_PORT_TYPE_C) ? MESA_VCAP_BIT_0 : MESA_VCAP_BIT_1;
            mce.key.tag.vid.value = data->config.flow.vid;
            mce.key.tag.vid.mask = 0xFFFF;
            mce.key.tag.dei = MESA_VCAP_BIT_ANY;
            mce.key.inner_tag.tagged = MESA_VCAP_BIT_0; /* NO Inner TAG */
            mce.key.inner_tag.s_tagged = MESA_VCAP_BIT_0;
            mce.key.inner_tag.dei = MESA_VCAP_BIT_0;
            mce.key.mel.value = (0x1 << data->config.level) - 1;
            mce.key.mel.mask = 0xFF;
            mce.key.injected = MESA_VCAP_BIT_1;

            memcpy (mce.action.port_list, data->config.flow.port, sizeof(mce.key.port_list));
            mce.action.voe_idx = data->voe_idx;
            mce.action.vid = data->config.flow.vid;
            mce.action.pop_cnt = 1;
            mce.action.tx_lookup = MESA_MCE_TX_LOOKUP_VID;
            mce.action.oam_detect = MESA_MCE_OAM_DETECT_SINGLE_TAGGED;
            mce.action.isdx = MESA_MCE_ISDX_NEW;

            if (mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* ADD the MCE injection rule */
            MCE_IDX_USED_SAVE(mce.id)
            if (mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* Get the transmit ISDX */
            data->tx_isdx = mce_info.isdx;

            (void)mesa_mce_init(NULL, &mce);            /********************************* Create extraction MCE rule *******************************/

            mce.id = MCE_ID_CALC(instance, nni, 0, 0);

            memcpy (mce.key.port_list, data->config.flow.port, sizeof(mce.key.port_list));
            mce.key.lookup = 0;                         /* Hit in first lookup. */
            mce.key.tag.tagged = MESA_VCAP_BIT_1;       /* VLAN TAG */
            mce.key.tag.s_tagged = MESA_VCAP_BIT_ANY;
            mce.key.tag.vid.value = data->config.flow.vid;
            mce.key.tag.vid.mask = 0xFFFF;
            mce.key.tag.dei = MESA_VCAP_BIT_ANY;
            mce.key.inner_tag.tagged = MESA_VCAP_BIT_0; /* NO Inner TAG */
            mce.key.inner_tag.s_tagged = MESA_VCAP_BIT_0;
            mce.key.inner_tag.dei = MESA_VCAP_BIT_0;
            mce.key.mel.value = level_value_mask_calc(data, data->config.level, &mask);
            mce.key.mel.mask = mask;
            mce.key.injected = MESA_VCAP_BIT_0;

            mce.action.port_list[data->config.port] = TRUE;
            mce.action.voe_idx = data->voe_idx;
            mce.action.pop_cnt = 1;
            mce.action.tx_lookup = MESA_MCE_TX_LOOKUP_ISDX;
            mce.action.oam_detect = MESA_MCE_OAM_DETECT_SINGLE_TAGGED;
            mce.action.isdx = MESA_MCE_ISDX_NEW;

            if (mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* ADD the MCE extraction rule */
            MCE_IDX_USED_SAVE(mce.id)
            if (mesa_mce_port_info_get(NULL, mce.id, nni, &mce_info) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* Get the receive ISDX */
            data->rx_isdx[0] = mce_info.isdx;
        } else { /* VLAN MEP is VOE Down MEP */
            (void)mesa_mce_init(NULL, &mce);            /******************************** Create injection MCE rule ***************************/

            mce.id = MCE_ID_CALC(instance, nni, 0, 1);

            mce.key.port_cpu = TRUE;
            mce.key.tag.tagged = MESA_VCAP_BIT_0;       /* NO Outer TAG */
            mce.key.tag.s_tagged = MESA_VCAP_BIT_0;
            mce.key.tag.dei = MESA_VCAP_BIT_0;
            mce.key.inner_tag.tagged = MESA_VCAP_BIT_0; /* NO Inner TAG */
            mce.key.inner_tag.s_tagged = MESA_VCAP_BIT_0;
            mce.key.inner_tag.dei = MESA_VCAP_BIT_0;
            mce.key.mel.mask = 0xFF;
            mce.key.injected = MESA_VCAP_BIT_0;

            memcpy (mce.action.port_list, data->config.flow.port, sizeof(mce.key.port_list));
            mce.action.voe_idx = data->voe_idx;
            mce.action.pop_cnt = 0;
            mce.action.oam_detect = MESA_MCE_OAM_DETECT_UNTAGGED;
            mce.action.isdx = MESA_MCE_ISDX_NEW;
            mce.action.tx_lookup = MESA_MCE_TX_LOOKUP_ISDX;
            if (mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* ADD the MCE injection rule */
            MCE_IDX_USED_SAVE(mce.id)
            if (mesa_mce_port_info_get(NULL, mce.id, VTSS_PORT_NO_CPU, &mce_info) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* Get the transmit ISDX */
            data->tx_isdx = mce_info.isdx;

            (void)mesa_mce_init(NULL, &mce);            /******************************* Create extraction MCE rule ***************************/

            mce.id = MCE_ID_CALC(instance, nni, 0, 0);

            memcpy (mce.key.port_list, data->config.flow.port, sizeof(mce.key.port_list));
            mce.key.tag.tagged = MESA_VCAP_BIT_1;       /* Outer EVC TAG */
            mce.key.tag.s_tagged = MESA_VCAP_BIT_ANY;
            mce.key.tag.vid.value = data->config.flow.out_vid;
            mce.key.tag.vid.mask = 0xFFFF;
            mce.key.tag.dei = MESA_VCAP_BIT_ANY;
            mce.key.tag.pcp.mask = 0x00;
            mce.key.inner_tag.tagged = MESA_VCAP_BIT_0; /* NO Inner TAG */
            mce.key.inner_tag.s_tagged = MESA_VCAP_BIT_0;
            mce.key.inner_tag.dei = MESA_VCAP_BIT_0;
            mce.key.mel.value = level_value_mask_calc(data, data->config.level, &mask);
            mce.key.mel.mask = mask;
            mce.key.injected = MESA_VCAP_BIT_0;

            /* mce.action.port_list is empty */
            port_on_vlan_id(data->config.flow.vid, mce.action.port_list);   /* THis is only to enable forwarding of any RAPS frame to other ring port */
            mce.action.voe_idx = data->voe_idx;
            mce.action.vid = data->config.flow.vid;
            mce.action.pop_cnt = 1;
            mce.action.tx_lookup = MESA_MCE_TX_LOOKUP_ISDX;     /* This is to enable VOE loop frames (LMR + DMR + ..) to hit ES0 */
            mce.action.rule = MESA_MCE_RULE_RX;     /* Only RX rules - no ES0 */
            mce.action.oam_detect = MESA_MCE_OAM_DETECT_SINGLE_TAGGED;
            mce.action.isdx = MESA_MCE_ISDX_NEW;
            mce.action.prio_enable = FALSE;
            if (mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* ADD the MCE extraction rule */
            MCE_IDX_USED_SAVE(mce.id)
            if (mesa_mce_port_info_get(NULL, mce.id, nni, &mce_info) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* Get the receive ISDX */
            data->rx_isdx[0] = mce_info.isdx;
        }
    }

    if (data->config.enable && !data->config.voe && (data->config.flow.mask & VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID)) { /* VLAN SW MEP NOT based on VOE but MCE only */
        (void)mesa_mce_init(NULL, &mce);            /********************************* Create extraction MCE rule *******************************/

        mce.id = MCE_ID_CALC(instance, nni, 0, 0);

        memcpy (mce.key.port_list, data->config.flow.port, sizeof(mce.key.port_list));
        mce.key.lookup = 0;                         /* Hit in first lookup. */
        mce.key.tag.tagged = MESA_VCAP_BIT_1;       /* Outer VLAN TAG */
        mce.key.tag.s_tagged = MESA_VCAP_BIT_ANY;
        mce.key.tag.vid.value = data->config.flow.out_vid;
        mce.key.tag.vid.mask = 0xFFFF;
        mce.key.tag.dei = MESA_VCAP_BIT_ANY;
        mce.key.inner_tag.tagged = MESA_VCAP_BIT_0; /* NO Inner TAG */
        mce.key.inner_tag.s_tagged = MESA_VCAP_BIT_0;
        mce.key.inner_tag.dei = MESA_VCAP_BIT_0;
        mce.key.mel.value = level_value_mask_calc(data, data->config.level, &mask);
        mce.key.mel.mask = mask;
        mce.key.injected = MESA_VCAP_BIT_0;

        /* mce.action.port_list is empty */
        mce.action.voe_idx = MESA_OAM_VOE_IDX_NONE;
        mce.action.pop_cnt = 1;
        mce.action.tx_lookup = MESA_MCE_TX_LOOKUP_VID;
        mce.action.oam_detect = MESA_MCE_OAM_DETECT_NONE;
        mce.action.policy_no = supp_mep_pag;
        mce.action.isdx = MESA_MCE_ISDX_NONE;

        if (mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* ADD the MCE extraction rule */
        MCE_IDX_USED_SAVE(mce.id)
    }

    if (data->config.enable && (data->config.mode == VTSS_MEP_SUPP_MEP) && (data->config.level != 7) && (data->config.flow.mask & VTSS_MEP_SUPP_FLOW_MASK_EVC_ID) && (instance == mep_on_highest_level(data->config.flow.vid))) { /* EVC MEP is enabled and this is highest level */
        (void)mesa_mce_init(NULL, &mce);            /********************************* Create termination MCE rule *******************************/

        mce.id = MCE_ID_CALC(instance, nni, 0, 1);

        memcpy (mce.key.port_list, data->config.flow.port, sizeof(mce.key.port_list));
        mce.key.lookup = 1;                         /* Hit in second lookup. The first lookup is hitting the Service entry */
        mce.key.tag.tagged = MESA_VCAP_BIT_1;       /* Outer EVC TAG */
        mce.key.tag.s_tagged = MESA_VCAP_BIT_ANY;
        mce.key.tag.vid.value = data->config.flow.out_vid;
        mce.key.tag.vid.mask = 0xFFFF;
        mce.key.tag.dei = MESA_VCAP_BIT_ANY;
        mce.key.inner_tag.tagged = MESA_VCAP_BIT_0; /* NO Inner TAG */
        mce.key.inner_tag.s_tagged = MESA_VCAP_BIT_0;
        mce.key.inner_tag.dei = MESA_VCAP_BIT_0;
        mce.key.mel.value = level_value_mask_calc(data, 7, &mask);
        mce.key.mel.mask = mask;
        mce.key.injected = MESA_VCAP_BIT_0;

        /* mce.action.port_list is empty */
        mce.action.voe_idx = MESA_OAM_VOE_IDX_NONE;
        mce.action.pop_cnt = 0;
        mce.action.tx_lookup = MESA_MCE_TX_LOOKUP_VID;
        mce.action.oam_detect = MESA_MCE_OAM_DETECT_NONE;
        mce.action.policy_no = supp_mep_pag;
        mce.action.isdx = MESA_MCE_ISDX_NONE;

// If this is going to work a macro must be defined to calculate MCE id for this - this should be done in IS2 saving IS1 entry
//        if (mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce) != VTSS_RC_OK)  return VTSS_MEP_SUPP_RC_NO_RESOURCES; /* ADD the MCE extraction rule */
//        mce_idx_used[mce.id] = TRUE;
    }

    if ((!data->config.enable || !data->config.voe) && (data->config.flow.mask & VTSS_MEP_SUPP_FLOW_MASK_EVC_ID) && (data->voe_idx < mep_caps.mesa_oam_voe_cnt)) { /* EVC MEP is disabled or no longer VOE based, with a VOE reference */
        (void)mesa_evc_oam_port_conf_get(NULL, data->config.flow.flow_id, data->config.port, &evc_conf);
        if (evc_conf.voe_idx == data->voe_idx) {    /* The EVC is hooked to this VOE */
            evc_conf.voe_idx = MESA_OAM_VOE_IDX_NONE;
            (void)mesa_evc_oam_port_conf_set(NULL, data->config.flow.flow_id, data->config.port, &evc_conf);  /* Un-Hook EVC residence port to VOE */

            for (i=0; i<mep_caps.mesa_port_cnt; ++i) {    /* All possible NNI ports must be Un-Hooked to VOE */
                if ((i != data->config.port) && data->config.flow.port[i])
                    (void)mesa_evc_oam_port_conf_set(NULL, data->config.flow.flow_id, i, &evc_conf);
            }
        }
    }

    vtss_mep_supp_trace("mce_rule_config  exit  instance", instance, 0, 0, 0);
    return VTSS_MEP_SUPP_RC_OK;
}

static u32 run_async_config(u32 instance)
{
    supp_instance_data_t *data;
    u8                   dummy_mac[VTSS_MEP_SUPP_MAC_LENGTH] = {0,0,0,0,0,0};
    u32                  i;
    BOOL                 out_of_service, up_mep_found;
    u32                  rc, etree_instance;
    u8                   dummy_frame[50];

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_CONFIG;

    vtss_mep_supp_trace("run_async_config  enter  instance  enable", instance, data->config.enable, 0, 0);

    if (mep_caps.mesa_mep_serval) {
        if ((!data->config.enable || !data->config.voe) && (data->voe_idx < mep_caps.mesa_oam_voe_cnt))    /* This MEP is either disabled or is no longer based on VOE */
            voe_to_mep[data->voe_idx] = VTSS_MEP_SUPP_CREATED_MAX;

        out_of_service = FALSE;
        up_mep_found = FALSE;

        for (i=0; i<VTSS_MEP_SUPP_CREATED_MAX; i++) {  /* Search for any UP-MEP/MIP */
            if (instance_data[i].config.enable && (instance_data[i].config.direction == VTSS_MEP_SUPP_UP) && (instance_data[i].config.port == data->config.port)) {    /* Enabled Up-MEP found on this port */
                up_mep_found = TRUE;
                if (instance_data[i].config.out_of_service)
                    out_of_service = TRUE;              /* Register an VOE UP-MEP is out of service on this UNI */
            }
        }

        vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
        vtss_mep_supp_port_conf(instance, data->config.port, up_mep_found, out_of_service);  /* UNI port must be configured depending on Up-MEP and 'out of service' MEP was found */
        vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);




        if ((rc = mce_rule_config(instance)) != VTSS_MEP_SUPP_RC_OK)  return rc;    /* Now configure the related OAM IS1 tcam entries */
            voe_config(data);             /* Now configure the VOE */




    if (!data->config.enable && VTSS_MEP_SUPP_ETREE_ELAN(data->config.evc_type) && !etree_elan_mep_present(data->config.flow.flow_id, &etree_instance)) { /* Potentially we just disabled the E-TREE instance that implemented the extraction rules for this EVC */
            if (etree_instance != VTSS_MEP_SUPP_CREATED_MAX) { /* There are still MEPs in this etree - reconfigure the first found to create new extraction rules */
                mce_rule_config(etree_instance);    /* Now configure the related OAM IS1 tcam entries */
            }
        }

        if ((!data->config.enable || !data->config.voe) && (data->voe_idx < mep_caps.mesa_oam_voe_cnt)) {    /* This MEP is either disabled or is no longer based on VOE */
            (void)mesa_oam_voe_free(NULL, data->voe_idx);   /* Free the VOE */
            data->voe_idx = mep_caps.mesa_oam_voe_cnt;
        }
    }

    if (data->config.enable)
    {
        /* Init of 'tx_header_size' */
        data->tx_header_size = init_tx_frame(instance, dummy_mac, data->config.mac, 0, 0, dummy_frame, OAM_TYPE_DMR, REPLY_COS_NONE);

        /* Init of DMR transmitter frame */
        if (data->tx_dmr != NULL) {
            (void)init_tx_frame(instance, dummy_mac, data->config.mac, 0, 0, data->tx_dmr, OAM_TYPE_DMR, REPLY_COS_NONE);
        }
        /* Init of LMR(SLR transmitter frame */
        if (data->tx_lmr != NULL) {
            (void)init_tx_frame(instance, dummy_mac, data->config.mac, 0, 0, data->tx_lmr, OAM_TYPE_SLR, REPLY_COS_NONE);
        }

        /* Init of DMR tag count for replying DMM correctly even 2-way DM is not enabled */
        data->onestep_extra_r.tag_cnt = tx_tag_cnt_calc(data->tx_header_size);

        run_async_ccm_gen(instance);
        run_async_lmm_config(instance);
        run_async_slm_config(instance);
        run_async_aps_config(instance);
        (void)run_tst_config(instance, NULL);
        (void)run_lbm_config(instance, NULL);

        data->event_flags |= EVENT_OUT_NEW_DEFECT;     /* Deliver CCM state to upper logic */
    }
    else
    {   /* Disable of this MEP  */
        if (data->tx_lmr != NULL) {
            vtss_mep_supp_packet_tx_free(data->tx_lmr);
            data->tx_lmr = NULL;
        }
        data->tx_lmr_buffer_size = 0;
        data->tx_dmr_buffer_size = 0;

        if (data->tx_dmr != NULL) {
            vtss_mep_supp_packet_tx_free(data->tx_dmr);
            data->tx_dmr = NULL;
        }
















        data->ccm_gen.enable = FALSE;
        data->lmm_config.enable = FALSE;
        data->slm_config.enable = FALSE;
        dm_enable_clear(data->dmm_config.enable);
        dm_enable_clear(data->dm1_config.enable);
        data->aps_config.enable = FALSE;
        data->ltm_config.enable = FALSE;
        test_enable_clear(data->lbm_config.tests);
        test_enable_clear(data->tst_config.tests);
        data->lck_config.enable = FALSE;
        data->ais_enable = FALSE;

        run_async_ccm_gen(instance);
        run_async_lmm_config(instance);
        run_async_slm_config(instance);
        run_async_aps_config(instance);
        run_dmm_config(instance);
        run_1dm_config(instance);
        run_ltm_config(instance);
        (void)run_lbm_config(instance, NULL);
        run_lck_config(instance);
        (void)run_tst_config(instance, NULL);
        run_ais_set(instance);

        instance_data_clear(instance,  data);
    }
    vtss_mep_supp_trace("run_async_config  exit  instance", instance, 0, 0, 0);
    return VTSS_RC_OK;
}

static void run_ccm_config(u32 instance)
{
    supp_instance_data_t  *data;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_CCM_CONFIG;

    voe_config(data);

    data->exp_meg_len = megid_calc(data, data->exp_meg);         /* Calculate and save the expected MEG-ID */

    /* LOC timer is based on expected rate */
    if (data->ccm_config.rate > VTSS_MEP_SUPP_RATE_1S)
        data->loc_timer_val = pdu_period_to_timer[rate_to_pdu_calc(data->ccm_config.rate)];
    else
        data->loc_timer_val = pdu_period_to_timer[rate_to_pdu_calc(VTSS_MEP_SUPP_RATE_1S)];

    run_async_ccm_gen(instance);
}

static u32 run_sync_ccm_sw_gen(u32 instance)
{
    supp_instance_data_t  *data;

    data = &instance_data[instance];    /* Instance data reference */

    if (data->tx_ccm_sw == NULL)
        if (!(data->tx_ccm_sw = vtss_mep_supp_packet_tx_alloc(HEADER_MAX_SIZE + CCM_PDU_LENGTH)))
            return(VTSS_MEP_SUPP_RC_NO_RESOURCES);

    return(VTSS_MEP_SUPP_RC_OK);
}

static u32 run_sync_ccm_hw_gen(u32 instance)
{
    supp_instance_data_t  *data;

    data = &instance_data[instance];    /* Instance data reference */

    if (data->tx_ccm_hw == NULL)
    {
        if (!(data->tx_ccm_hw = vtss_mep_supp_packet_tx_alloc(HEADER_MAX_SIZE + CCM_PDU_LENGTH)))
            return(VTSS_MEP_SUPP_RC_NO_RESOURCES);
        data->hw_ccm_transmitting = FALSE;
    }

    return(VTSS_MEP_SUPP_RC_OK);
}

static void ccm_pdu_init(supp_instance_data_t  *data, u8 *pdu)
{
    u32 off = CCM_PDU_TLV_OFFSET;

    memset(pdu, 0, CCM_PDU_LENGTH);

    pdu[0] = data->config.level << 5;
    pdu[1] = OAM_TYPE_CCM;
    pdu[2] = rate_to_pdu_calc(data->ccm_config.rate);
    pdu[2] |= ((data->ccm_rdi) ? 0x80 : 0x00);
    pdu[3] = 70;
    pdu[8] = (data->ccm_config.mep & 0xFF00) >> 8;
    pdu[9] = data->ccm_config.mep & 0xFF;

    (void)megid_calc(data, &pdu[10]);         /* Calculate transmitted MEG-ID */

    if (data->ccm_config.tlv_enable) {
        pdu[off++] = 2;    /* Insert Port Status TLV */
        pdu[off++] = 0;
        pdu[off++] = 1;
        pdu[off++] = data->ccm_config.ps_tlv.value;
        pdu[off++] = 4;    /* Insert Interface Status TLV */
        pdu[off++] = 0;
        pdu[off++] = 1;
        pdu[off++] = data->ccm_config.is_tlv.value;
        pdu[off++] = 31;   /* Insert Organization-Specific TLV */
        pdu[off++] = 0;
        pdu[off++] = 5;
        pdu[off++] = data->ccm_config.os_tlv.oui[0];
        pdu[off++] = data->ccm_config.os_tlv.oui[1];
        pdu[off++] = data->ccm_config.os_tlv.oui[2];
        pdu[off++] = data->ccm_config.os_tlv.subtype;
        pdu[off++] = data->ccm_config.os_tlv.value;
    }
}

static void run_async_ccm_gen(u32 instance)
{
    static u32 first_timer = 2;
    supp_instance_data_t            *data;
    u32                             i, header_size;
    vtss_mep_supp_tx_frame_info_t   tx_frame_info;
    vtss_mep_supp_rate_t            sw_ccm_rate;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_CCM_GEN;

    voe_config(data);

    vtss_mep_supp_trace("run_async_ccm_gen: Enter  instance", instance, 0, 0, 0);

    if (data->ccm_gen.enable) {  /* CC or LM is enabled */
        if (hw_ccm_calc(data->ccm_gen.cc_rate)) { /* HW CCM generation */
            if (mep_caps.mesa_mep_serval) {
                if (!data->ccm_gen.lm_enable && (data->tx_ccm_sw != NULL))    {vtss_mep_supp_packet_tx_free(data->tx_ccm_sw); data->tx_ccm_sw = NULL;}  /* On Jaguar and Serval without LM - No SW based CCM-LM */
                if (data->config.voe && (data->config.direction == VTSS_MEP_SUPP_DOWN) && (data->tx_ccm_sw != NULL))    {vtss_mep_supp_packet_tx_free(data->tx_ccm_sw); data->tx_ccm_sw = NULL;}  /* On Serval VOE down mep - No SW based CCM-LM */
            }
            if (mep_caps.mesa_mep_luton26) {
                if (data->tx_ccm_sw != NULL)    {vtss_mep_supp_packet_tx_free(data->tx_ccm_sw); data->tx_ccm_sw = NULL;}  /* On Luton26 SW CCM-LM is not possible */
            }
            if ((data->hw_ccm_transmitting) && (data->tx_ccm_hw != NULL))
            {   /* HW CCM transmitting is on-going. This must be stopped before starting the new one */
                vtss_mep_supp_packet_tx_cancel(instance, data->tx_ccm_hw, NULL);
            }

            if (data->tx_ccm_hw == NULL)    {vtss_mep_supp_trace("run_async_ccm_gen: tx_cmm_hw is NULL", 0, 0, 0, 0);   return;}

            header_size = init_tx_frame(instance, data->ccm_config.dmac, data->config.mac, data->ccm_config.prio, data->ccm_config.dei, data->tx_ccm_hw, OAM_TYPE_CCM, REPLY_COS_NONE);

            ccm_pdu_init(data, &data->tx_ccm_hw[header_size]);

            tx_frame_info.bypass = evc_vlan_up(data) ? FALSE : TRUE;
            tx_frame_info.maskerade_port = evc_vlan_up(data) ? data->config.port : VTSS_PORT_NO_NONE;
            tx_frame_info.isdx = data->tx_isdx;
            tx_frame_info.vid = data->config.flow.vid;
            tx_frame_info.vid_inj = vlan_down(data);
            tx_frame_info.qos = data->ccm_config.prio;
            tx_frame_info.pcp = data->ccm_config.prio;
            tx_frame_info.dp = data->ccm_config.dei;

            if (VTSS_RC_OK != vtss_mep_supp_packet_tx(instance, NULL, data->config.flow.port, (header_size + CCM_PDU_LENGTH), data->tx_ccm_hw, &tx_frame_info, frame_rate_calc(data->ccm_gen.cc_rate), FALSE, 0, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_CCM : VTSS_MEP_SUPP_OAM_TYPE_NONE), data->config.sat)) {
                vtss_mep_supp_trace("run_async_ccm_gen: packet tx failed", 0, 0, 0, 0); /* If packet tx was not done the buffer must be freed */
                data->hw_ccm_transmitting = FALSE;
            } else {
                data->hw_ccm_transmitting = TRUE;   /* HW based CCM is now on-going */
            }

            data->tx_ccm_timer = 0;

            sw_ccm_rate = data->ccm_gen.lm_rate;    /* In case of any CCM-LM the period is LM period */
        }
        else {  /* SW CCM generation */
            if (data->tx_ccm_hw != NULL) {
                if (data->hw_ccm_transmitting) { /* Only allowed to cancel if transmission ongoing */
                    vtss_mep_supp_packet_tx_cancel(instance, data->tx_ccm_hw, NULL);
                }
                vtss_mep_supp_packet_tx_free(data->tx_ccm_hw);
                data->tx_ccm_hw = NULL;
            }
            data->hw_ccm_transmitting = FALSE;

            for (i=0; i<data->ccm_config.peer_count; ++i)       /* All peer LOC timers are initialized with this LOC timer value */
                data->rx_ccm_timer[i] = data->loc_timer_val;

            data->defect_state.dLoc[0] &= ~0x2;     /* Assure that any contribution from ACL SW polling based LOC is cleared - see vtss_mep_supp_loc_state() */

            sw_ccm_rate = data->ccm_gen.cc_rate;    /* Both CC and LM is based on SW CCM - the period is CC period */
        }

        if (data->tx_ccm_sw != NULL) {  /* SW based CCM must be transmitted with the calculated period */
            header_size = init_tx_frame(instance, data->ccm_config.dmac, data->config.mac, data->ccm_config.prio, data->ccm_config.dei, data->tx_ccm_sw, OAM_TYPE_CCM, REPLY_COS_NONE);
            ccm_pdu_init(data, &data->tx_ccm_sw[header_size]);

            data->ccm_timer_val = tx_timer_calc(sw_ccm_rate);
//            data->tx_ccm_timer = data->ccm_timer_val;   /* Start timer transmitting CCM */
            // Now CCM is send the first time with 20 ms interval within one sec - this is only temporary
            data->tx_ccm_timer = first_timer;
            first_timer += 2;
            if (first_timer >= 100) first_timer = 2;

            vtss_mep_supp_timer_start();
        }
    }
    else {  /* Transmission of CCM PDU is disabled */
        if (data->tx_ccm_sw != NULL)   vtss_mep_supp_packet_tx_free(data->tx_ccm_sw);
        if (data->tx_ccm_hw != NULL) {
            if (data->hw_ccm_transmitting) { /* Only allowed to cancel if transmission ongoing */
                vtss_mep_supp_packet_tx_cancel(instance, data->tx_ccm_hw, NULL);
            }
            vtss_mep_supp_packet_tx_free(data->tx_ccm_hw);
            data->tx_ccm_hw = NULL;
        }

        data->tx_ccm_hw = NULL;
        data->tx_ccm_sw = NULL;
        data->hw_ccm_transmitting = FALSE;
        data->tx_ccm_timer = 0;



        data->defect_state.dLoc[0] = 0;



    }

    vtss_mep_supp_trace("run_async_ccm_gen: Exit  instance", instance, 0, 0, 0);

    data->event_flags |= EVENT_OUT_NEW_DEFECT;     /* Deliver CCM state to upper logic */
}

static void run_ccm_rdi(u32 instance)
{
    supp_instance_data_t  *data;
    u8                    flags;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_CCM_RDI;

    vtss_mep_supp_trace("run_ccm_rdi: Enter  instance", instance, 0, 0, 0);

    if (mep_caps.mesa_mep_serval) {
        if (data->config.voe)   /* VOE is assigned */
            (void)mesa_oam_voe_ccm_set_rdi_flag(NULL, data->voe_idx, data->ccm_rdi);
    }
    if (data->tx_ccm_sw)
    {   /* SW CCM */
        flags = data->tx_ccm_sw[data->tx_header_size + 2];
        data->tx_ccm_sw[data->tx_header_size + 2] = ((flags & ~0x80) | ((data->ccm_rdi) ? 0x80 : 0x00));
    }

    if (!data->config.voe && hw_ccm_calc(data->ccm_gen.cc_rate))
    {   /* This is a non VOE hw based CCM transmission */
        data->rdi_timer = 1000/timer_res;   /* Start timer for detecting stable RDI */
        vtss_mep_supp_timer_start();
    }
}

static u32 run_sync_lmm_config(u32 instance)
{
    supp_instance_data_t  *data;

    data = &instance_data[instance];    /* Instance data reference */

    if (instance_data[instance].tx_lmm == NULL)
        if (!(data->tx_lmm = vtss_mep_supp_packet_tx_alloc(HEADER_MAX_SIZE + LMM_PDU_LENGTH)))
            return(VTSS_MEP_SUPP_RC_NO_RESOURCES);

    return(VTSS_MEP_SUPP_RC_OK);
}

static void run_async_lmm_config(u32 instance)
{
    static u32 first_timer = 1;
    supp_instance_data_t   *data;
    vtss_mep_supp_conf_t   *config = &instance_data[instance].config;
    u8                     *pdu;
    u32                    header_size;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_LMM_CONFIG;

    voe_config(data);

    vtss_mep_supp_lm_trace("run_async_lmm_config  instance  enable  synthetic", instance, data->lmm_config.enable, 0, 0);

    if (data->lmm_config.enable)
    {
        header_size = init_tx_frame(instance, data->lmm_config.dmac, config->mac, data->lmm_config.prio, data->lmm_config.dei, data->tx_lmm, OAM_TYPE_LMM, REPLY_COS_NONE);
        data->lm_cosid = data->lmm_config.prio;

        pdu = &data->tx_lmm[header_size];
        pdu[0] = config->level << 5;
        pdu[1] = OAM_TYPE_LMM;
        pdu[3] = 12;
        memset(&pdu[4], 0, LMM_PDU_LENGTH-4);
        data->tx_lmm_buffer_size = header_size + LMM_PDU_LENGTH;
        data->lmm_timer_val = tx_timer_calc(data->lmm_config.rate);
//        data->tx_lmm_timer = data->lmm_timer_val;   /* Start timer transmitting LMM */
        // Now LMM is send the first time with 20 ms interval within one sec - this is only temporary
        data->tx_lmm_timer = first_timer;
        first_timer += 2;
        if (first_timer >= 100) first_timer = 1;

        vtss_mep_supp_timer_start();
    }
    else
    {
        if (!data->slm_config.enable) {
            if (data->tx_lmm != NULL) {
                vtss_mep_supp_packet_tx_free(data->tx_lmm);
                data->tx_lmm = NULL;
            }

            data->tx_lmm_timer = 0;
        }
    }
}


static u32 slm_pattern_size_calc(u32 size)
{
    u32 payload_size;

    if (size < (12+2+4)) {
        vtss_mep_supp_lm_trace("slm_pattern_size_calc  Odd size!!!  return", size, 0, 0, 0);
        return(0);
    }

    payload_size = size - (12+2+4); /* Payload size is configured frame size subtracted untagged overhead and CRC */

    if (payload_size > SLM_PDU_LENGTH) {  /* Check if pattern must be added to TLV - requested payload is bigger than TST PDU with empty TLV */
        vtss_mep_supp_lm_trace("slm_pattern_size_calc  size  return", size, (payload_size - SLM_PDU_LENGTH), 0, 0);
        return(payload_size - SLM_PDU_LENGTH);
    } else {
        vtss_mep_supp_lm_trace("slm_pattern_size_calc  size  return", size, 0, 0, 0);
        return(0);
    }
}

static u32 run_sync_slm_config(u32 instance,  u32 size)
{
    supp_instance_data_t  *data;

    data = &instance_data[instance];    /* Instance data reference */

    vtss_mep_supp_lm_trace("run_sync_slm_config  instance", instance, 0, 0, 0);
    if (data->tx_lmm == NULL) {
        vtss_mep_supp_lm_trace("run_sync_slm_config  instance  Tx SLM buffer alloc", instance, 0, 0, 0);
        if (!(data->tx_lmm = vtss_mep_supp_packet_tx_alloc(HEADER_MAX_SIZE + SLM_PDU_LENGTH + slm_pattern_size_calc(size))))
            return(VTSS_MEP_SUPP_RC_NO_RESOURCES);
    }
    return(VTSS_MEP_SUPP_RC_OK);
}

static void run_async_slm_config(u32 instance)
{
    static u32             first_timer = 1;
    supp_instance_data_t   *data;
    vtss_mep_supp_conf_t   *config = &instance_data[instance].config;
    u8                     *pdu, oam_type;
    u32                    header_size, pattern_size, k;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_SLM_CONFIG;

    vtss_mep_supp_lm_trace("run_async_slm_config  instance  enable  service", instance, data->slm_config.enable, data->lmm_config.enable, 0);

    if (data->slm_config.enable)
    {
        oam_type = (data->slm_config.ended == VTSS_MEP_SUPP_SINGLE_ENDED) ? OAM_TYPE_SLM : OAM_TYPE_1SL;
        header_size = init_tx_frame(instance, data->slm_config.dmac, config->mac, data->slm_config.prio, data->slm_config.dei, data->tx_lmm, oam_type, REPLY_COS_NONE);
        data->lm_cosid = data->slm_config.prio;

        pattern_size = slm_pattern_size_calc(data->slm_config.size);

        pdu = &data->tx_lmm[header_size];
        pdu[0] = config->level << 5;
        pdu[1] = oam_type;
        pdu[3] = 16;
        pdu[4] = (data->ccm_config.mep & 0xFF00) >> 8;
        pdu[5] = data->ccm_config.mep & 0xFF;
        pdu[8]  = (data->slm_config.test_id & 0xFF000000) >> 24;
        pdu[9]  = (data->slm_config.test_id & 0x00FF0000) >> 16;
        pdu[10] = (data->slm_config.test_id & 0x0000FF00) >> 8;
        pdu[11] = (data->slm_config.test_id & 0x000000FF);
        pdu[20] = 3;  /* Data TLV */
        pdu[21] = (pattern_size & 0xFF00)>>8;
        pdu[22] = (pattern_size & 0xFF);
        for (k=0; k<pattern_size; ++k)     pdu[23+k] = 0xAA;
        pdu[23+k] = 0;
        /* Remember header_size includes everything in front of the PDU and CRC must not be part of buffer size */
        data->tx_lmm_buffer_size = header_size + SLM_PDU_LENGTH + pattern_size;
        data->lmm_timer_val = tx_timer_calc(data->slm_config.rate);

        // Now SLM is send the first time with 20 ms interval within one sec - this is only temporary
        data->tx_lmm_timer = first_timer;
        first_timer += 2;
        if (first_timer >= 100) first_timer = 1;

        vtss_mep_supp_timer_start();
    }
    else
    {
        if (!data->lmm_config.enable) {
            if (data->tx_lmm != NULL)    vtss_mep_supp_packet_tx_free(data->tx_lmm);

            data->tx_lmm = NULL;
            data->tx_lmm_timer = 0;
        }
    }
}

static void run_dmm_config(u32 instance)
{
    static u32             first_timer = 2;
    supp_instance_data_t   *data;
    vtss_mep_supp_conf_t   *config = &instance_data[instance].config;
    u8                     *pdu;
    u32                    header_size, i;
    dm_state_t             *state;
    BOOL                   enable=FALSE;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_DMM_CONFIG;

    vtss_mep_supp_trace("run_dmm_config  instance", instance, 0, 0, 0);

    if (dm_enable_calc(data->dm1_config.enable))    return;     /* Do not do any DMM configuration if 1DM is enabled */
    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Configure any enabled DMM */
        state = &data->dm_state[i];
        if (data->dmm_config.enable[i]) {
            enable = TRUE; // RBNTBD: Shouldn't this be beneath if (state->enabled) continue?

            if (state->enabled)     continue;   /* DM on this prio is already enabled */
            state->enabled = TRUE;

            if (state->tx_dm == NULL)
                state->tx_dm = vtss_mep_supp_packet_tx_alloc(HEADER_MAX_SIZE + DMM_PDU_LENGTH);

            if (state->tx_dm != NULL) {/* DMM is ready and a tx buffer has been allocated */
                /* Init of DMM transmitter frame */
                header_size = init_tx_frame(instance, data->dmm_config.dmac, config->mac, i, data->dmm_config.dei, state->tx_dm, OAM_TYPE_DMM, REPLY_COS_NONE);
                pdu = &state->tx_dm[header_size];
                pdu[0] = config->level << 5;
                pdu[1] = OAM_TYPE_DMM;
                pdu[2] = 0;
                pdu[3] = 32;
            } else
                return;

            state->dmm_tx_active = FALSE;
            state->dm_tx_cnt = 0;
            state->tw_cnt = 0;
            state->nf_cnt = 0;
            state->fn_cnt = 0;
            state->tx_dmm_fup_timestamp.seconds = 0;
            state->tx_dmm_fup_timestamp.nanoseconds = 0;

            if (data->dmm_config.proprietary) {
                /* Expect to get a follow-up packet for DMR and add "VTSS" after "End TLV" to inform the remote site to send a follow-up packet. */
                u32 pdu_len = DMM_PDU_LENGTH;

                if (pdu_len == DMM_PDU_VTSS_LENGTH) {
                    memcpy(&pdu[DMM_PDU_STD_LENGTH], "VTSS", 4);
                }
            } else
                memset(&pdu[DMM_PDU_STD_LENGTH], 0, 4);
        } else {
            state->enabled = FALSE;

            if (state->tx_dm != NULL) {
                vtss_mep_supp_packet_tx_free(state->tx_dm);
                state->tx_dm = NULL;
            }
        }
    }

    data->rx_dmr_timeout_timer = 0;
    data->tx_dmm_timer = 0;

    if (enable) {
        for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i)
            data->dm_state[i].active = FALSE;

        data->tx_dmm_timer = first_timer;
        first_timer += 2;
        if (first_timer >= 100) first_timer = 2;

        vtss_mep_supp_timer_start();
    }
}


static void run_1dm_config(u32 instance)
{
    static u32             first_timer = 2;
    supp_instance_data_t   *data;
    u8                     *pdu;
    u32                    header_size, i;
    dm_state_t             *state;
    BOOL                   enable=FALSE;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_1DM_CONFIG;

    if (dm_enable_calc(data->dmm_config.enable))    return;     /* Do not do any 1DM configuration if DMM is enabled */

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Configure any enabled 1DM */
        state = &data->dm_state[i];
        if (data->dm1_config.enable[i]) {
            enable = TRUE;

            if (state->enabled)     continue;   /* DM on this prio is already enabled */
            state->enabled = TRUE;

            if (state->tx_dm == NULL)
                state->tx_dm = vtss_mep_supp_packet_tx_alloc(HEADER_MAX_SIZE + DM1_PDU_LENGTH);

            if (state->tx_dm != NULL)
            {/* 1DM is ready and a tx buffer has been allocated */
                /* Init of 1DM transmitter frame */
                header_size = init_tx_frame(instance, data->dm1_config.dmac, data->config.mac, i, data->dm1_config.dei, state->tx_dm, OAM_TYPE_1DM, REPLY_COS_NONE);
                pdu = &state->tx_dm[header_size];
                pdu[0] = data->config.level << 5;
                pdu[1] = OAM_TYPE_1DM;
                pdu[2] = 0;
                pdu[3] = 16;
            } else
                return;

            state->dm_tx_cnt = 0;
            state->tw_cnt = 0;
            state->nf_cnt = 0;
            state->fn_cnt = 0;

            if (data->dm1_config.proprietary) {
                /* Inform the remote site that a follow-up packet will be sent out */
                u32 pdu_len = DM1_PDU_LENGTH;

                if (pdu_len == DM1_PDU_VTSS_LENGTH) {
                    memcpy(&pdu[DM1_PDU_STD_LENGTH], "VTSS", 4);
                }
            } else {
                memset(&pdu[DM1_PDU_STD_LENGTH], 0, 4);
            }
        } else {
            state->enabled = FALSE;

            if (state->tx_dm != NULL) {
                vtss_mep_supp_packet_tx_free(state->tx_dm);
                state->tx_dm = NULL;
            }
        }
    }

    data->tx_dm1_timer = 0;
    if (enable) {
        for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i)
            data->dm_state[i].active = FALSE;

        data->tx_dm1_timer = first_timer;
        first_timer += 2;
        if (first_timer >= 100) first_timer = 2;

        vtss_mep_supp_timer_start();
    }
}

static u32 run_sync_aps_config(u32 instance)
{
    supp_instance_data_t  *data;

    data = &instance_data[instance];    /* Instance data reference */

    if (instance_data[instance].tx_aps == NULL)
        if (!(data->tx_aps = vtss_mep_supp_packet_tx_alloc(HEADER_MAX_SIZE + RAPS_PDU_LENGTH)))
            return(VTSS_MEP_SUPP_RC_NO_RESOURCES);

    return(VTSS_MEP_SUPP_RC_OK);
}

static void run_async_aps_config(u32 instance)
{
    supp_instance_data_t  *data;
    u8                    *pdu;
    u32                   opcode;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_APS_CONFIG;

    if (data->aps_config.enable)
    {
        opcode = (data->aps_config.type == VTSS_MEP_SUPP_L_APS) ? OAM_TYPE_LAPS : OAM_TYPE_RAPS;
        data->tx_header_size_aps = init_tx_frame(instance, data->aps_config.dmac, data->config.mac, data->aps_config.prio, data->aps_config.dei, data->tx_aps, opcode, REPLY_COS_NONE);

        pdu = &data->tx_aps[data->tx_header_size_aps];
        pdu[0] = data->config.level << 5;
        pdu[0] |= (data->aps_config.type == VTSS_MEP_SUPP_L_APS) ? 0x00 : 0x01;     /* This is new version number for Version 2 of ERPS */
        pdu[1] = opcode;
        pdu[3] = (data->aps_config.type == VTSS_MEP_SUPP_L_APS) ? 4 : 32;
        memcpy(&pdu[4], data->aps_txdata, APS_DATA_LENGTH);

        data->aps_timer_val = ((5000/timer_res)+1);

        data->aps_count = 0;
        data->event_flags |= EVENT_IN_TX_APS_TIMER;       /* Simulate run out of TX APS timer */
        vtss_mep_supp_run();
    }
    else
    {
        if (data->tx_aps != NULL) {
            vtss_mep_supp_packet_tx_free(data->tx_aps);
            data->tx_aps = NULL;
        }

        data->tx_aps_timer = 0;
    }
}

static void run_aps_txdata(u32 instance)
{
    supp_instance_data_t  *data;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_APS_TXDATA;

    if (data->aps_config.enable)
    {
        if (!data->aps_event)   memcpy(&data->tx_aps[data->tx_header_size_aps + 4], data->aps_txdata, APS_DATA_LENGTH);
        else                    data->tx_aps[data->tx_header_size_aps + 4] = 0xE0;     /* Transmission of an ERPS event */
        data->aps_count = 2;
        instance_data[instance].event_flags |= EVENT_IN_TX_APS_TIMER;       /* Simulate run out of TX APS timer */
        run_tx_aps_timer(instance);
    }
}

static void run_aps_forward(u32 instance)
{
    mesa_oam_voe_conf_t   cfg;
    supp_instance_data_t  *data;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_APS_FORWARD;

    if (data->aps_config.enable)
    {
        if (data->voe_idx >= mep_caps.mesa_oam_voe_cnt)   return;

        (void)mesa_oam_voe_conf_get(NULL, data->voe_idx, &cfg);
        cfg.generic[GENERIC_RAPS].forward = data->aps_forward;
        (void)mesa_oam_voe_conf_set(NULL, data->voe_idx, &cfg);
    }
}

static void run_ltm_config(u32 instance)
{
    u32                             rc, header_size;
    supp_instance_data_t            *data;
    u8                              *pdu, *tx_ltm;
    vtss_mep_supp_tx_frame_info_t   tx_frame_info;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_LTM_CONFIG;

    if (!data->ltm_config.enable) {
         return;
    }

    if ((tx_ltm = vtss_mep_supp_packet_tx_alloc(HEADER_MAX_SIZE + LTM_PDU_LENGTH)) == NULL) {
        return;
    }

    /* Init of LTM transmitter frame */
    header_size = init_tx_frame(instance, data->ltm_config.dmac, data->config.mac, data->ltm_config.prio, data->ltm_config.dei, tx_ltm, OAM_TYPE_LTM, REPLY_COS_NONE);
    pdu = &tx_ltm[header_size];
    pdu[0] = data->config.level << 5;
    pdu[1] = OAM_TYPE_LTM;
    pdu[2] = 0x80;
    pdu[3] = 17;
    var_to_string(&pdu[4], data->ltm_config.transaction_id);
    pdu[8] = data->ltm_config.ttl;
    memcpy(&pdu[9], data->config.mac, VTSS_MEP_SUPP_MAC_LENGTH);
    memcpy(&pdu[15], data->ltm_config.tmac, VTSS_MEP_SUPP_MAC_LENGTH);
    pdu[21] = 7;
    pdu[22] = 0;
    pdu[23] = 8;
    pdu[24] = 0;
    pdu[25] = 0;
    memcpy(&pdu[26], data->config.mac, VTSS_MEP_SUPP_MAC_LENGTH);
    pdu[32] = 0;

    data->ltr_cnt = 0;

    tx_frame_info.bypass = !evc_vlan_up(data);
    tx_frame_info.maskerade_port = (evc_vlan_up(data)) ? data->config.port : VTSS_PORT_NO_NONE;
    tx_frame_info.isdx = data->tx_isdx;
    tx_frame_info.vid = data->config.flow.vid;
    tx_frame_info.vid_inj = vlan_down(data);
    tx_frame_info.qos = data->ltm_config.prio;
    tx_frame_info.pcp = data->ltm_config.prio;
    tx_frame_info.dp = data->ltm_config.dei;

    rc = vtss_mep_supp_packet_tx(instance, NULL, data->config.flow.port, (header_size + LTM_PDU_LENGTH), tx_ltm, &tx_frame_info, VTSS_MEP_SUPP_RATE_INV, FALSE, 0, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_LTM : VTSS_MEP_SUPP_OAM_TYPE_NONE), data->config.sat);
    vtss_mep_supp_packet_tx_free(tx_ltm);

    if (rc != VTSS_RC_OK) vtss_mep_supp_trace("run_ltm_config: packet tx failed", instance, 0, 0, 0);
}

/*lint -e{454, 455, 456} ... The mutex is locked so it is ok to unlock */
static void local_etree_elan_lbr(u32 instance,  u8 *tx_lbm)
{
    supp_instance_data_t  *data, *idata;
    BOOL                  new_lbr = FALSE;
    u32                   i;

    data = &instance_data[instance];    /* Instance data reference */

    for (i=0; i<VTSS_MEP_SUPP_CREATED_MAX; ++i) {
        idata = &instance_data[i];    /* Instance data reference */
        /* Find a E-TREE MEP in this VID on this level */
        if ((i != instance) && idata->config.enable && VTSS_MEP_SUPP_ETREE_ELAN(idata->config.evc_type) && (idata->config.flow.vid == data->config.flow.vid) &&
            (idata->config.flow.leaf_vid == data->config.flow.leaf_vid) && (idata->config.level == data->config.level) &&
            ((*tx_lbm & 0x01) || !memcmp(tx_lbm, idata->config.mac, VTSS_MEP_SUPP_MAC_LENGTH)) &&
            (!VTSS_MEP_SUPP_ETREE(idata->config.evc_type) || (data->config.evc_type == VTSS_MEP_SUPP_ROOT) || (idata->config.evc_type == VTSS_MEP_SUPP_ROOT))) {
            /* This instance is expected to reply */
            data->lb_state.lbr_counter++;
            if (data->lbr_cnt < VTSS_MEP_SUPP_LBR_MAX) {
                data->lbr[data->lbr_cnt].transaction_id = data->lb_state.trans_id;
                memcpy(data->lbr[data->lbr_cnt].u.mac, idata->config.mac, sizeof(data->lbr[data->lbr_cnt].u.mac));
                data->lbr_cnt++;
                new_lbr = TRUE;
            }
        }
    }

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
    if (new_lbr)        vtss_mep_supp_new_lbr(instance);
    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
}

static u32 run_lbm_config(u32 instance, u64 *active_time_ms)
{
    u32                             rc = VTSS_RC_OK, i, j, k, frame_rate, header_size, pattern_size, payload_size, pdu_size, tx_buffer_size, pattern_idx, pattern, offs = 0;
    supp_instance_data_t            *data;
    u8                              *pdu, **tx_lbm, *etree_tx_lbm;
    BOOL                            enable, infinite;
    vtss_mep_supp_tx_frame_info_t   tx_frame_info;
    vtss_mep_supp_test_conf_t       *test;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_LBM_CONFIG;

    voe_config(data);

    infinite = (data->lbm_config.to_send == VTSS_MEP_SUPP_LBM_TO_SEND_INFINITE) ? TRUE : FALSE;
    enable = FALSE;
    etree_tx_lbm = NULL;






    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Configure any enabled LBM */
        for (j=0; j<VTSS_MEP_SUPP_TEST_MAX; ++j) {
            if (data->lbm_config.tests[i][j].enable) { /* LBM enable */
                /* TX is enabled */
                enable = TRUE;

                test = &data->lbm_config.tests[i][j];
                tx_lbm = &data->tx_lbm[i][j];

                if (infinite && (*tx_lbm != NULL))  continue;      /* Transmission is already active */

                payload_size = test->size - (12+2+4); /* Payload size is configured frame size subtracted untagged overhead and CRC */
                pdu_size = LBM_PDU_LENGTH + ((test->pattern == VTSS_MEP_SUPP_PATTERN_NONE) ? 4 : 3) + offs; /* PDU size with empty TLV is depending on the TLV type */
                pattern_size = 0;

                if (payload_size > pdu_size)   /* Check if pattern must be added to TLV - requested payload is bigger than TST PDU with empty TLV */
                    pattern_size = payload_size - pdu_size;

                if ((*tx_lbm == NULL) && (*tx_lbm = vtss_mep_supp_packet_tx_alloc((HEADER_MAX_SIZE + pdu_size + pattern_size))))
                {/* LBM is ready and a tx buffer has been allocated */
                    /* Init of LBM transmitter frame - remember that the CRC is inserted by packet transmitter */
                    header_size = init_tx_frame(instance, data->lbm_config.dmac, data->config.mac, i, ((test->dp == 0) ? FALSE : TRUE), *tx_lbm, OAM_TYPE_LBM, REPLY_COS_NONE);
                    pdu = &(*tx_lbm)[header_size];
                    pdu[0] = data->config.level << 5;
                    pdu[1] = OAM_TYPE_LBM;
                    pdu[2] = 0;
                    pdu[3] = 4;
                    data->lb_state.trans_id++;
                    var_to_string(&pdu[4], data->lb_state.trans_id);



























                    if (test->pattern == VTSS_MEP_SUPP_PATTERN_NONE) { /* Check for Data TLV pattern 'None' */
                        pdu[offs+8] = 32;  /* Test TLV */
                        pdu[offs+9] = ((pattern_size+1) & 0xFF00)>>8;
                        pdu[offs+10] = ((pattern_size+1) & 0xFF);
                        pdu[offs+11] = 0;    /* Test TLV Pattern type */
                        pattern_idx = 12;
                    } else {
                        pdu[offs+8] = 3;  /* Data TLV */
                        pdu[offs+9] = (pattern_size & 0xFF00)>>8;
                        pdu[offs+10] = (pattern_size & 0xFF);
                        pattern_idx = 11;
                    }
                    switch (test->pattern)
                    {
                        case VTSS_MEP_SUPP_PATTERN_NONE:     pattern = 0; break;
                        case VTSS_MEP_SUPP_PATTERN_ALL_ZERO: pattern = 0; break;
                        case VTSS_MEP_SUPP_PATTERN_ALL_ONE:  pattern = 0xFF; break;
                        case VTSS_MEP_SUPP_PATTERN_0XAA:     pattern = 0xAA; break;
                        default:                             pattern = 0; break;
                    }
                    for (k=0; k<pattern_size; ++k)     pdu[offs+pattern_idx+k] = pattern;
                    pdu[offs+pattern_idx+k] = 0;

                    /* Remember header_size includes everything in front of the PDU and CRC must not be part of buffer size */
                    tx_buffer_size = header_size + pdu_size + pattern_size;
                    frame_rate = 0;
                    if (infinite) { /* This is HW based generation of LBM - Calculation of AFI frame rate */
                        frame_rate = test->rate | VTSS_MEP_SUPP_RATE_IS_BPS;
                        vtss_mep_supp_trace("supp run_lbm_config  test_size  test_rate  frame_rate ", test->size, test->rate, frame_rate, 0);
                    }

                    tx_frame_info.bypass = evc_vlan_up(data) ? FALSE : TRUE;
                    tx_frame_info.maskerade_port = evc_vlan_up(data) ? data->config.port : VTSS_PORT_NO_NONE;
                    tx_frame_info.line_rate = data->lbm_config.line_rate;
                    tx_frame_info.isdx = data->tx_isdx;
                    tx_frame_info.vid = data->config.flow.vid;
                    tx_frame_info.vid_inj = vlan_down(data);
                    tx_frame_info.qos = i;
                    tx_frame_info.pcp = i;
                    tx_frame_info.dp = ((test->dp == 0) ? FALSE : TRUE);

                    rc = vtss_mep_supp_packet_tx(instance, NULL, data->config.flow.port, tx_buffer_size, *tx_lbm, &tx_frame_info, frame_rate, FALSE, 0, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_LBM : VTSS_MEP_SUPP_OAM_TYPE_NONE), data->config.sat);
                    if (rc != VTSS_RC_OK) {  /* If packet tx did not happen the buffer must be freed */
                        vtss_mep_supp_trace("run_lbm_config: packet tx failed", instance, 0, 0, 0);
                        vtss_mep_supp_packet_tx_free(*tx_lbm);
                        *tx_lbm = NULL;
                        data->lbm_config.tests[i][j].enable = FALSE;
                    }
                    if (VTSS_MEP_SUPP_ETREE_ELAN(data->config.evc_type)) {    /* This is an E-TREE or E-LAN instance. Any other instances in this E-TREE is potentially replying */
                        etree_tx_lbm = *tx_lbm;
                    }
                }
            } else {  /* Disable LBM */
                if (data->tx_lbm[i][j] != NULL) {
                    if (!infinite) {  /* This is SW based generation of LBM - buffer must be freed */
                        vtss_mep_supp_packet_tx_free(data->tx_lbm[i][j]);
                    } else {
                        vtss_mep_supp_packet_tx_cancel(instance, data->tx_lbm[i][j], active_time_ms);
                        vtss_mep_supp_packet_tx_free(data->tx_lbm[i][j]);
                        data->event_flags |= EVENT_OUT_LBM_DONE;       /* Signal LBM done to upper layer */
                        vtss_mep_supp_run();
                    }
                    data->tx_lbm[i][j] = NULL;
                }
            }
        }
    }

    data->tx_lbm_timer = 0;

    if (!infinite) {    /* SW generation */
        if (enable) {   /* Active transmission */
            data->lbm_count = data->lbm_config.to_send - 1;
            data->lbr_cnt = 0;
            data->lb_state.oo_counter = 0;
            data->lb_state.lbr_counter = 0;
            data->lb_state.lbm_counter = 1;

            if (etree_tx_lbm != NULL) {    /* This is an E-TREE or E-LAN instance. Any other instances in this E-TREE is potentially replying */
                local_etree_elan_lbr(instance,  etree_tx_lbm);
            }

            if (data->lbm_config.interval)  /* This is SW based generation of LBM - tx time is required */
            {/* Start LBM tx timer if any - otherwise send next on tx done */
                data->tx_lbm_timer = (data->lbm_config.interval*10)/timer_res;
                vtss_mep_supp_timer_start();
            }
        }
    }
    return (rc);
}

static void run_lck_config (u32 instance)
{
    supp_instance_data_t  *data;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_LCK_CONFIG;

    if (data->lck_config.enable)
    {
        /* ETH-LCK packet to be constructed here */
        if ((data->tx_lck == NULL) && (data->tx_lck = vtss_mep_supp_packet_tx_alloc(HEADER_MAX_SIZE + LCK_PDU_LENGTH)))
        {/* LCK is ready and a tx buffer has been allocated */
            data->lck_count = 0;
            data->lck_inx = 0;
            data->event_flags |= EVENT_IN_TX_LCK_TIMER;       /* Simulate run out of TX LCK timer */
            vtss_mep_supp_run();
        }
    }
    else
    {
        data->tx_lck_timer = 0;
        if (data->tx_lck != NULL) {
            vtss_mep_supp_packet_tx_free(data->tx_lck);
            data->tx_lck = NULL;
        }
    }
}

static void run_ais_set (u32 instance)
{
    supp_instance_data_t  *data;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_AIS_SET;

    if (data->ais_enable)
    {
        if ((data->tx_ais == NULL) && (data->tx_ais = vtss_mep_supp_packet_tx_alloc(HEADER_MAX_SIZE + AIS_PDU_LENGTH)))
        {
            data->ais_count = (data->ais_config.protection) ? 0 : 2;
            data->ais_inx = 0;
            data->event_flags |= EVENT_IN_TX_AIS_TIMER;       /* Simulate run out of TX AIS timer */
            vtss_mep_supp_run();
        }
    }
    else
    {
        data->tx_ais_timer = 0;

        if (data->tx_ais != NULL) {
            vtss_mep_supp_packet_tx_free(data->tx_ais);
            data->tx_ais = NULL;
        }
    }
}

static void tst_mce_rule_config(u32 instance, u32 prio, BOOL enabled)
{
    supp_instance_data_t   *data;
    mesa_evc_counters_t    evc_counters;
    mesa_mce_port_info_t   mce_info;
    mesa_mce_t             mce;
    u32                    vid;

    data = &instance_data[instance];    /* Instance data reference */

    if (data->config.voe) {
        return;
    }

    if (enabled && (data->tx_isdx_tst == VTSS_MEP_SUPP_INDX_INVALID)) { /* Create the TST MCE rules */
        if (data->config.direction == VTSS_MEP_SUPP_UP) {   /* This is an SW Up-MEP */
            vid = (data->config.evc_type == VTSS_MEP_SUPP_LEAF) ? data->config.flow.leaf_vid : data->config.flow.vid;

            (void)mesa_mce_init(NULL, &mce);            /************************* Create injection MCE rule *******************************/

            mce.id = MCE_ID_CALC_TST(instance, data->config.port);

            mce.key.port_list[data->config.port] = TRUE;
            mce.key.tag.tagged = MESA_VCAP_BIT_1;       /* Outer "Dummy" TAG */
            mce.key.tag.s_tagged = MESA_VCAP_BIT_ANY;
            mce.key.tag.vid.value = vid;
            mce.key.tag.vid.mask = 0xFFFF;
            mce.key.tag.dei = MESA_VCAP_BIT_ANY;
            mce.key.inner_tag.tagged = MESA_VCAP_BIT_1; /* TST Inner TAG */
            mce.key.inner_tag.s_tagged = MESA_VCAP_BIT_ANY;
            mce.key.inner_tag.vid.value = 0;
            mce.key.inner_tag.vid.mask = 0xFFFF;
            mce.key.inner_tag.dei = MESA_VCAP_BIT_ANY;
            mce.key.mel.value = (0x1 << data->config.level) - 1;
            mce.key.mel.mask = 0xFF;
            mce.key.injected = MESA_VCAP_BIT_1;

            mce.action.isdx = MESA_MCE_ISDX_NEW;
            mce.action.evc_id = (data->config.flow.mask & VTSS_MEP_SUPP_FLOW_MASK_EVC_ID) ? data->config.flow.flow_id : 0;
            mce.action.vid = vid;
            mce.action.pop_cnt = 2;
            mce.action.tx_lookup = MESA_MCE_TX_LOOKUP_VID;
            if (VTSS_MEP_SUPP_ETREE_ELAN(data->config.evc_type)) { /* An E-TREE or E-LAN Up-MEP must have rules per NNI port */
                memcpy (mce.action.port_list, data->config.flow.port, sizeof(mce.action.port_list));
                mce.action.evc_etree = TRUE;
            }

            if (mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce) != MESA_RC_OK)  return; /* ADD the MCE injection rule */
            if (mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info) != MESA_RC_OK)  return; /* Get the transmit ISDX */
            data->tx_isdx_tst = mce_info.isdx;
        } else {    /* This is a SW Down-MEP */
            (void)mesa_mce_init(NULL, &mce);            /******************************** Create injection MCE rule ***************************/

            mce.id = MCE_ID_CALC_TST(instance, data->config.port);

            mce.key.port_cpu = TRUE;
            mce.key.injected = MESA_VCAP_BIT_0;

            memcpy (mce.action.port_list, data->config.flow.port, sizeof(mce.action.port_list));
            mce.action.outer_tag.enable = (data->config.flow.mask & VTSS_MEP_SUPP_FLOW_MASK_EVC_ID) ? TRUE : FALSE;
            mce.action.outer_tag.vid = data->config.flow.out_vid;
            mce.action.outer_tag.pcp_mode = MESA_MCE_PCP_MODE_MAPPED;
            mce.action.outer_tag.dei_mode = MESA_MCE_DEI_MODE_DP;
            mce.action.tx_lookup = MESA_MCE_TX_LOOKUP_ISDX;
            mce.action.isdx = MESA_MCE_ISDX_NEW;




















            if (mesa_mce_add(NULL, MESA_MCE_ID_LAST, &mce) != MESA_RC_OK)  return; /* ADD the MCE injection rule */
            if (mesa_mce_port_info_get(NULL, mce.id, data->config.port, &mce_info) != MESA_RC_OK)  return; /* Get the transmit ISDX */
            data->tx_isdx_tst = mce_info.isdx;
        } /* SW MEP */
    } else { /* Possible TST disable */
        if (!enabled && (data->tx_isdx_tst != VTSS_MEP_SUPP_INDX_INVALID)) {
            if (mesa_mce_counters_get(NULL, MCE_ID_CALC_TST(instance, data->config.port),  data->config.port,  &evc_counters) == MESA_RC_OK) { /* Get the TX counter befole deleting the rules */
                data->tst_tx_cnt = (data->config.direction == VTSS_MEP_SUPP_UP) ? (evc_counters.rx_green.frames + evc_counters.rx_yellow.frames + evc_counters.rx_discard.frames) :
                                                                                  (evc_counters.tx_green.frames + evc_counters.tx_yellow.frames + evc_counters.tx_discard.frames);
            }

            (void)mesa_mce_del(NULL, MCE_ID_CALC_TST(instance, data->config.port));

            data->tx_isdx_tst = VTSS_MEP_SUPP_INDX_INVALID; /* The TST TX ISDX is no more valid */
        }
    }
}

static u32 run_tst_config(u32 instance, u64 *active_time_ms)
{
    supp_instance_data_t            *data;
    u8                              *pdu, **tx_tst;
    u32                             i, j, k, rc = VTSS_MEP_SUPP_RC_OK, pdu_size, pattern_size, pattern, pattern_idx, payload_size, frame_rate, header_size, tx_buffer_size, prio=0;
    vtss_mep_supp_tx_frame_info_t   tx_frame_info;
    vtss_mep_supp_test_conf_t       *test;
    BOOL                            enabled=FALSE;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_TST_CONFIG;

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Cancel any disabled Tests */
        for (j=0; j<VTSS_MEP_SUPP_TEST_MAX; ++j) {
            if (data->tst_config.tests[i][j].enable) {
                enabled = TRUE;
                prio = i;   /* Save the first prio. This only make sense if TST is enabled in one prio */
                continue;
            }

            /* TX is not enabled */

            if (data->tx_tst[i][j] != NULL)  {    /* TX is ongoing */
                vtss_mep_supp_packet_tx_cancel(instance, data->tx_tst[i][j], active_time_ms); /* Cancel TST transmission */
                vtss_mep_supp_packet_tx_free(data->tx_tst[i][j]);
                data->tx_tst[i][j] = NULL;
                data->event_flags |= EVENT_OUT_TST_DONE;       /* Signal TST done to upper layer */
                vtss_mep_supp_run();

            }
        }
    }

    voe_config(data);   /* VOE configuration must be done here before any AFI is activated - in order to count tx TST PDU correctly */

    if (mep_caps.mesa_mep_serval) {
        if (enabled)  {
            tst_mce_rule_config(instance, prio, enabled);
        }
    }
    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Configure any enabled Tests */
        for (j=0; j<VTSS_MEP_SUPP_TEST_MAX; ++j) {
            if (!data->tst_config.tests[i][j].enable)  continue;
            /* TX is enabled */
            test = &data->tst_config.tests[i][j];
            tx_tst = &data->tx_tst[i][j];

            if (*tx_tst != NULL)    continue;   /* Transmission is already active */

            payload_size = test->size - (12+2+4); /* Payload size is configured frame size subtracted untagged overhead and CRC */
            pdu_size = TST_PDU_LENGTH + ((test->pattern == VTSS_MEP_SUPP_PATTERN_NONE) ? 4 : 3); /* PDU size with empty TLV is depending on the TLV type */
            pattern_size = 0;

            if (payload_size > pdu_size)   /* Check if pattern must be added to TLV - requested payload is bigger than TST PDU with empty TLV */
                pattern_size = payload_size - pdu_size;

            if ((*tx_tst == NULL) && (*tx_tst = vtss_mep_supp_packet_tx_alloc((HEADER_MAX_SIZE + pdu_size + pattern_size))))
            {/* TST is ready and a tx buffer has been allocated */
                /* Init of TST transmitter frame - remember that the CRC is inserted by packet transmitter */
                header_size = init_tx_frame(instance, data->tst_config.dmac, data->config.mac, i, ((test->dp == 0) ? FALSE : TRUE), *tx_tst, OAM_TYPE_TST, REPLY_COS_NONE);











                pdu = &(*tx_tst)[header_size];
                pdu[0] = data->config.level << 5;
                pdu[1] = OAM_TYPE_TST;
                pdu[2] = 0;
                pdu[3] = 4;
                pdu[4] = 0;
                pdu[5] = 0;
                pdu[6] = 0;
                pdu[7] = 0;
                if (test->pattern == VTSS_MEP_SUPP_PATTERN_NONE) { /* Check for Data TLV pattern 'None' */
                    pdu[8] = 32;  /* Test TLV */
                    pdu[9] = ((pattern_size+1) & 0xFF00)>>8;
                    pdu[10] = ((pattern_size+1) & 0xFF);
                    pdu[11] = 0;    /* Test TLV Pattern type */
                    pattern_idx = 12;
                } else {
                    pdu[8] = 3;  /* Data TLV */
                    pdu[9] = (pattern_size & 0xFF00)>>8;
                    pdu[10] = (pattern_size & 0xFF);
                    pattern_idx = 11;
                }
                switch (test->pattern)
                {
                    case VTSS_MEP_SUPP_PATTERN_NONE:     pattern = 0; break;
                    case VTSS_MEP_SUPP_PATTERN_ALL_ZERO: pattern = 0; break;
                    case VTSS_MEP_SUPP_PATTERN_ALL_ONE:  pattern = 0xFF; break;
                    case VTSS_MEP_SUPP_PATTERN_0XAA:     pattern = 0xAA; break;
                    default:                             pattern = 0; break;
                }
                for (k=0; k<pattern_size; ++k)     pdu[pattern_idx+k] = pattern;
                pdu[pattern_idx+k] = 0;

                /* Remember header_size includes everything in front of the PDU and CRC must not be part of buffer size */
                tx_buffer_size = header_size + pdu_size + pattern_size;
                frame_rate = test->rate | VTSS_MEP_SUPP_RATE_IS_BPS;
                vtss_mep_supp_trace("supp run_tst_config  test_size  test_rate  frame_rate", test->size, test->rate, frame_rate, 0);

                data->tst_tx_cnt = 0;

                tx_frame_info.bypass = evc_vlan_up(data) ? FALSE : TRUE;
                tx_frame_info.maskerade_port = evc_vlan_up(data) ? data->config.port : VTSS_PORT_NO_NONE;
                tx_frame_info.line_rate = data->tst_config.line_rate;
                if (mep_caps.mesa_mep_serval) {
                    tx_frame_info.isdx = (data->tx_isdx_tst != MESA_MCE_ISDX_NONE) ? data->tx_isdx_tst : data->tx_isdx;
                } else {
                    tx_frame_info.isdx = data->tx_isdx;
                }
                tx_frame_info.vid = data->config.flow.vid;
                tx_frame_info.vid_inj = vlan_down(data);
                tx_frame_info.qos = i;
                tx_frame_info.pcp = i;
                tx_frame_info.dp = ((test->dp == 0) ? FALSE : TRUE);

                rc = vtss_mep_supp_packet_tx(instance, NULL, data->config.flow.port, tx_buffer_size, *tx_tst, &tx_frame_info, frame_rate, TRUE, (data->tst_config.sequence) ? (header_size + 4) : 0, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_LBM : VTSS_MEP_SUPP_OAM_TYPE_NONE), data->config.sat);
                if (rc != VTSS_RC_OK) {  /* If packet tx did not happen the buffer must be freed */
                    vtss_mep_supp_trace("run_tst_config: packet tx failed", instance, 0, 0, 0);
                    vtss_mep_supp_packet_tx_free(*tx_tst);
                    *tx_tst = NULL;
                }
            }
        }
    }
    if (mep_caps.mesa_mep_serval) {
        if (!enabled)  {
            tst_mce_rule_config(instance, prio, enabled);
        }
    }
    return (rc);
}


static void run_defect_timer(u32 instance)
{
    supp_instance_data_t  *data;
    BOOL                  new_, ccm_active, ais_lck_active;
    u32                   i;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_DEFECT_TIMER;

    new_ = ccm_active = ais_lck_active = FALSE;

    if (!data->dLevel.timer)
    {
        if (data->defect_state.dLevel)
        {
            data->dLevel.old_period = 0;
            data->defect_state.dLevel = FALSE;
            new_ = TRUE;
        }
    } else ccm_active = TRUE;
    if (!data->dMeg.timer)
    {
        if (data->defect_state.dMeg)
        {
            data->dMeg.old_period = 0;
            data->defect_state.dMeg = FALSE;
            new_ = TRUE;
        }
    } else ccm_active = TRUE;
    if (!data->dMep.timer)
    {
        if (data->defect_state.dMep)
        {
            data->dMep.old_period = 0;
            data->defect_state.dMep = FALSE;
            new_ = TRUE;
        }
    } else ccm_active = TRUE;
    for (i=0; i<data->ccm_config.peer_count; ++i)
    {
        if (!data->dPeriod[i].timer)
        {
            if (data->defect_state.dPeriod[i])
            {
                data->dPeriod[i].old_period = 0;
                data->defect_state.dPeriod[i] = FALSE;
                new_ = TRUE;
            }
        } else ccm_active = TRUE;
        if (!data->dPrio[i].timer)
        {
            if (data->defect_state.dPrio[i])
            {
                data->dPrio[i].old_period = 0;
                data->defect_state.dPrio[i] = FALSE;
                new_ = TRUE;
            }
        } else ccm_active = TRUE;
    }
    if (!data->dLoop.timer)
    {
        if (data->defect_state.dLoop)
        {
            data->dLoop.old_period = 0;
            data->defect_state.dLoop = FALSE;
            new_ = TRUE;
        }
    } else ccm_active = TRUE;
    if (!data->dConfig.timer)
    {
        if (data->defect_state.dConfig)
        {
            data->dConfig.old_period = 0;
            data->defect_state.dConfig = FALSE;
            new_ = TRUE;
        }
    } else ccm_active = TRUE;
    if (!data->dAis.timer)
    {
        if (data->defect_state.dAis)
        {
            data->dAis.old_period = 0;
            data->defect_state.dAis = FALSE;
            new_ = TRUE;
        }
    } else ais_lck_active = TRUE;
    if (!data->dLck.timer)
    {
        if (data->defect_state.dLck)
        {
            data->dLck.old_period = 0;
            data->defect_state.dLck = FALSE;
            new_ = TRUE;
        }
    } else ais_lck_active = TRUE;

    data->defect_timer_active = ccm_active || ais_lck_active;
    data->ccm_defect_active = ccm_active;

    if (new_) {
        data->event_flags |= EVENT_OUT_NEW_DEFECT;
    }
}

static void run_rx_ccm_timer(u32 instance)
{
    supp_instance_data_t  *data;
    BOOL                  new_;
    u32                   i;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_RX_CCM_TIMER;

    new_ = FALSE;

    for (i=0; i<data->ccm_config.peer_count; ++i)
    {
        if (!data->rx_ccm_timer[i])
        {
            if (!(data->defect_state.dLoc[i] & 0x01))
            { /* sw detected LOC is active */
                data->defect_state.dLoc[i] |= 0x01;
                data->defect_state.dRdi[i] = FALSE;
                new_ = TRUE;
            }
        }
    }

    if (new_) {
        data->event_flags |= EVENT_OUT_NEW_DEFECT;
    }
}

/*lint -e{454, 455, 456} ... The mutex is locked so it is ok to unlock */
static void local_etree_elan_ccm(u32 instance)
{
    supp_instance_data_t  *data, *idata;
    BOOL                  defect, state, is, mac;
    u32                   i;

    data = &instance_data[instance];    /* Instance data reference */

    for (i=0; i<VTSS_MEP_SUPP_CREATED_MAX; ++i) {
        idata = &instance_data[i];    /* Instance data reference */
        /* Find a E-TREE MEP in this VID on this level */
        if ((i != instance) && idata->config.enable && VTSS_MEP_SUPP_ETREE_ELAN(idata->config.evc_type) && (idata->config.flow.vid == data->config.flow.vid) &&
            (idata->config.flow.leaf_vid == data->config.flow.leaf_vid) && (idata->config.level == data->config.level) &&
            (!VTSS_MEP_SUPP_ETREE(idata->config.evc_type) || (data->config.evc_type == VTSS_MEP_SUPP_ROOT) || (idata->config.evc_type == VTSS_MEP_SUPP_ROOT))) {
            defect = state = is = FALSE;
            if (!ccm_defect_calc(i, idata, &data->tx_ccm_sw[6], &data->tx_ccm_sw[data->tx_header_size], data->ccm_config.prio, &defect, &state, &mac)) {
                extract_tlv_info(&data->tx_ccm_sw[data->tx_header_size], idata, &is);
                idata->ccm_state.state.valid_counter++;
            } else {
                idata->ccm_state.state.invalid_counter++;
            }
            vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
            if (defect)     vtss_mep_supp_new_defect_state(i);
            if (state)      vtss_mep_supp_new_ccm_state(i);
            if (is)         vtss_mep_supp_new_tlv_is(i);
            vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
        }
    }
}

/*lint -e{454, 455, 456} ... The mutex is locked so it is ok to unlock */
static void run_tx_ccm_timer(u32 instance)
{
    u32                             rc, tx_frames, rx_frames;
    supp_instance_data_t            *data;
    vtss_mep_supp_tx_frame_info_t   tx_frame_info;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_TX_CCM_TIMER;

    if (!data->config.enable)      return;
    if (!data->ccm_gen.enable)     return;
    if (data->tx_ccm_sw == NULL)   return;

    if (data->ccm_gen.lm_enable && !data->config.voe) {
        vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
        vtss_mep_supp_counters_get(instance, &rx_frames, &tx_frames);
        vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
        // The supp_loc_crit has been unlocked and locked again, and this mep might have been disabled/deleted or
        // changed state during the unlock, so we have to check status again.
        if (!data->config.enable)      return;
        if (!data->ccm_gen.enable)     return;
        if (data->tx_ccm_sw == NULL)   return;
    }

    data->tx_ccm_timer = data->ccm_timer_val;
    vtss_mep_supp_timer_start();

    if (data->ccm_gen.lm_enable)
    {   /* LM carried in CCM PDU is enabled */
        data->lm_state[0].lm_counter.tx_msg_counter++;

        if (!data->config.voe) { /* This is not a VOE based instance */
            var_to_string(&data->tx_ccm_sw[data->tx_header_size+58], tx_frames);
        }
        else { /* This is VOE based - in case of UP-MEP the counters to get updated must be non zero */
            memset(&data->tx_ccm_sw[data->tx_header_size+58], 1, 4);
        }
    }

    tx_frame_info.bypass = !evc_vlan_up(data);
    tx_frame_info.maskerade_port = (evc_vlan_up(data)) ? data->config.port : VTSS_PORT_NO_NONE;
    tx_frame_info.isdx = data->tx_isdx;
    tx_frame_info.vid = data->config.flow.vid;
    tx_frame_info.vid_inj = vlan_down(data);
    tx_frame_info.qos = data->ccm_config.prio;
    tx_frame_info.pcp = data->ccm_config.prio;
    tx_frame_info.dp = data->ccm_config.dei;

    rc = vtss_mep_supp_packet_tx(instance, NULL, data->config.flow.port, (data->tx_header_size + CCM_PDU_LENGTH), data->tx_ccm_sw, &tx_frame_info, VTSS_MEP_SUPP_RATE_INV, FALSE, 0, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_CCM : VTSS_MEP_SUPP_OAM_TYPE_NONE), data->config.sat);
    if (rc != VTSS_RC_OK)   vtss_mep_supp_trace("run_tx_ccm_timer: packet tx failed", instance, 0, 0, 0);

    if (VTSS_MEP_SUPP_ETREE_ELAN(data->config.evc_type)) {    /* This is an E-TREE or E-LAN instance. Any other instances in this E-TREE must "receive" this frame */
        local_etree_elan_ccm(instance);
    }
}

























































































































































































static void run_tx_aps_timer(u32 instance)
{
    u32                             rc;
    supp_instance_data_t            *data;
    vtss_mep_supp_tx_frame_info_t   tx_frame_info;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_TX_APS_TIMER;
    data->tx_aps_timer = data->aps_timer_val;
    vtss_mep_supp_timer_start();

    if ((data->aps_config.type != VTSS_MEP_SUPP_R_APS) || data->aps_tx)
    {/* R-APS PDU must only be transmitted when 'active' is enabled from ERPS */
        tx_frame_info.bypass = TRUE;
        tx_frame_info.maskerade_port = VTSS_PORT_NO_NONE;
        tx_frame_info.isdx = data->tx_isdx;
        tx_frame_info.vid = data->config.flow.vid;
        tx_frame_info.vid_inj = (data->config.flow.mask & VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID) ? TRUE : FALSE;
        tx_frame_info.qos = data->aps_config.prio;
        tx_frame_info.pcp = data->aps_config.prio;
        tx_frame_info.dp = data->aps_config.dei;

        rc = vtss_mep_supp_packet_tx(instance, NULL, data->config.flow.port, (data->tx_header_size_aps + APS_PDU_LENGTH), data->tx_aps, &tx_frame_info, VTSS_MEP_SUPP_RATE_INV, FALSE, 0, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_GENERIC : VTSS_MEP_SUPP_OAM_TYPE_NONE), data->config.sat);
        if (rc != VTSS_RC_OK)   vtss_mep_supp_trace("run_tx_aps_timer: packet tx failed", instance, 0, 0, 0);

        if (data->aps_config.enable) {
            if (data->aps_count) {
                /* Still APS frames to be send */
                data->aps_count--;
                instance_data[instance].event_flags |= EVENT_IN_TX_APS_TIMER;       /* Simulate run out of TX APS timer */
                vtss_mep_supp_run();
            } else if (data->aps_event) {
                /* End of ERPS event transmission */
                data->aps_event = FALSE;
                data->tx_aps[instance_data[instance].tx_header_size_aps + 4] = instance_data[instance].aps_txdata[0];     /* Transmission of an 'Normal' ERPS request */
            }
        }
    }
}

// Return TRUE if either LMM or SLM TX is enabled.
static BOOL is_lm_tx_enabled(const u32 instance)
{
    if (!instance_data[instance].config.enable)
        return FALSE;
    if (instance_data[instance].lmm_config.enable)
        return TRUE;
    if (instance_data[instance].ccm_gen.lm_enable)
        return TRUE;
    if (instance_data[instance].slm_config.enable)
        return TRUE;
    return FALSE;
}

// Return TRUE if either LMM or SLM RX is enabled.
static BOOL is_lm_rx_enabled(const u32 instance)
{
    if (!instance_data[instance].config.enable)
        return FALSE;
    if (instance_data[instance].lmm_config.enable_rx)
        return TRUE;
    if (instance_data[instance].slm_config.enable_rx)
        return TRUE;
    return FALSE;
}

// Return TRUE if either LMM or SLM initiator is enabled and set to single-ended.
static BOOL is_lm_single_ended_initiator(const u32 instance)
{
    if (!instance_data[instance].config.enable)
        return FALSE;
    if (instance_data[instance].lmm_config.enable && instance_data[instance].lmm_config.ended == VTSS_MEP_SUPP_SINGLE_ENDED)
        return TRUE;
    if (instance_data[instance].slm_config.enable && instance_data[instance].slm_config.ended == VTSS_MEP_SUPP_SINGLE_ENDED)
        return TRUE;
    return FALSE;
}

static void get_lm_peer_counters(const u32 instance, lm_counter_t *lm,
                                 vtss_mep_supp_lm_peer_counters_t *peer_counters)
{
    memset(peer_counters, 0, sizeof(*peer_counters));

    if (is_lm_tx_enabled(instance)) {
        // Always us the CCM/LLM/SLM/1SL TX counter
        peer_counters->tx_counter = lm->tx_msg_counter;

        /*
         * Now select which counter to use as the RX counter:
         * - If this is a single-ended initiator we expect to receive LMR/SLR responses
         *   and we thus use the response RX counter.
         * - If this is a dual-ended initiator then we do not expect to receive anything
         *   except if RX is also enabled. Then we expect to receive messages
         *   (CCM/1SL RX counter) from the other end.
         */
        peer_counters->rx_counter = is_lm_single_ended_initiator(instance) ? lm->rx_rsp_counter :
            is_lm_rx_enabled(instance) ? lm->rx_msg_counter : 0;

    } else if (is_lm_rx_enabled(instance)) {
        // responder-only: TX is always 0, in order not to confuse the user and
        // because it will always be equal to the RX counter.
        peer_counters->tx_counter = 0;
        // responder-only: Always us the CCM/LLM/SLM/1SL RX counter
        peer_counters->rx_counter = lm->rx_msg_counter;

    } else {
        // nothing is enabled - don't collect anything
    }
}

/*lint -e{454, 455, 456} ... The mutex is locked so it is ok to unlock */
static void run_tx_lmm_timer(u32 instance)
{
    u32                             rc, rx_frames, tx_frames, peer_idx;
    supp_instance_data_t            *data;
    vtss_mep_supp_tx_frame_info_t   tx_frame_info;
    vtss_mep_supp_oam_type_t        oam_type=VTSS_MEP_SUPP_OAM_TYPE_NONE;

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
    vtss_mep_supp_counters_get(instance, &rx_frames, &tx_frames);
    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_TX_LMM_TIMER;

    vtss_mep_supp_lm_trace("run_tx_lmm_timer  instance  Service  Synthetic", instance, data->lmm_config.enable, data->slm_config.enable, 0);

    if (!data->config.enable)                                   return;
    if (!data->lmm_config.enable && !data->slm_config.enable)   return;
    if (data->tx_lmm == NULL)                                   return;

    data->tx_lmm_timer = data->lmm_timer_val;
    vtss_mep_supp_timer_start();

    if (data->slm_config.enable) {
        var_to_string(&data->tx_lmm[data->tx_header_size+12], data->lm_state[0].sl_service_counter.tx_slm_counter);
        oam_type = VTSS_MEP_SUPP_OAM_TYPE_GENERIC;
        vtss_mep_supp_lm_trace("run_tx_lmm_timer  Tx SLM  instance  TxFCf", instance, string_to_var(&data->tx_lmm[data->tx_header_size+12]), 0, 0);
    }
    if (data->lmm_config.enable) {
        if (instance_data[instance].config.voe) { /* I think this must be != 0 in or to be overwritten by VOE */
            tx_frames = 1;
        }
        var_to_string(&data->tx_lmm[data->tx_header_size+4], tx_frames);
        oam_type = VTSS_MEP_SUPP_OAM_TYPE_LMM;
        vtss_mep_supp_lm_trace("run_tx_lmm_timer  Tx LMM  instance  TxFCf", instance, string_to_var(&data->tx_lmm[data->tx_header_size+4]), 0, 0);
    }

    tx_frame_info.bypass = !evc_vlan_up(data);
    tx_frame_info.maskerade_port = (evc_vlan_up(data)) ? data->config.port : VTSS_PORT_NO_NONE;
    tx_frame_info.isdx = data->tx_isdx;
    tx_frame_info.vid = data->config.flow.vid;
    tx_frame_info.vid_inj = vlan_down(data);
    tx_frame_info.qos = data->lm_cosid;
    tx_frame_info.pcp = data->lm_cosid;
    tx_frame_info.dp = (data->lmm_config.enable) ? data->lmm_config.dei : data->slm_config.dei;

    rc = vtss_mep_supp_packet_tx(instance, NULL, data->config.flow.port, data->tx_lmm_buffer_size, data->tx_lmm, &tx_frame_info, VTSS_MEP_SUPP_RATE_INV, FALSE, 0, ((data->config.voe) ? oam_type : VTSS_MEP_SUPP_OAM_TYPE_NONE), data->config.sat);
    if (rc != VTSS_RC_OK)   vtss_mep_supp_trace("run_tx_lmm_timer: packet tx failed", instance, 0, 0, 0);
    if (rc == VTSS_RC_OK) {
        // Only update TX counters if TX was successful
        vtss_mep_supp_lm_peer_counters_t peer_counters;

        if (data->slm_config.enable) {
            data->lm_state[0].sl_service_counter.tx_slm_counter++;

            // update TX message counter for all peers
            for (peer_idx = 0; peer_idx < mep_caps.mesa_oam_peer_cnt; peer_idx++) {
                data->lm_state[peer_idx].lm_counter.tx_msg_counter++;
                get_lm_peer_counters(instance, &data->lm_state[peer_idx].lm_counter, &peer_counters);
                vtss_mep_supp_lm_tx_update(instance, peer_idx, &peer_counters);
            }
        } else if (data->lmm_config.enable) {
            // update TX message counter for single peer
            data->lm_state[0].lm_counter.tx_msg_counter++;
            get_lm_peer_counters(instance, &data->lm_state[0].lm_counter, &peer_counters);
            vtss_mep_supp_lm_tx_update(instance, 0, &peer_counters);
        }
    }
}

static void run_rx_aps_timer(u32 instance)
{
    instance_data[instance].event_flags &= ~EVENT_IN_RX_APS_TIMER;

    instance_data[instance].event_flags |= EVENT_OUT_NEW_APS;
}

static void run_tx_lbm_timer(u32 instance)
{
    u32                             rc, i, j, pattern_size, payload_size, pdu_size, tx_buffer_size, tx_header_size, offs = 0;
    u8                              **tx_lbm;
    supp_instance_data_t            *data;
    vtss_mep_supp_tx_frame_info_t   tx_frame_info;
    vtss_mep_supp_test_conf_t       *test;

    // S/W-based LBM transmission.

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_TX_LBM_TIMER;

    if (!test_enable_calc(data->lbm_config.tests))   return;
    if (data->lbm_config.to_send == VTSS_MEP_SUPP_LBM_TO_SEND_INFINITE) return;   /* This HW transmission - there should be no LBM tx timer !!!! */

    if (!data->lbm_count) {
        return;
    }

    if (data->lbm_config.interval)
    {/* Start LBM tx timer if any - otherwise send next on tx done */
        data->tx_lbm_timer = (data->lbm_config.interval*10)/timer_res;
        vtss_mep_supp_timer_start();
    }

    data->lbm_count--;
    data->lb_state.trans_id++;   /* Increment Transaction ID */

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Transmit any enabled LBM */
        tx_header_size = prio_tx_header_size(instance, i, OAM_TYPE_LBM);
        for (j=0; j<VTSS_MEP_SUPP_TEST_MAX; ++j) {
            if (data->lbm_config.tests[i][j].enable) { /* LBM enable */
                test = &data->lbm_config.tests[i][j];
                tx_lbm = &data->tx_lbm[i][j];

                var_to_string(&(*tx_lbm)[tx_header_size+4], data->lb_state.trans_id);

                tx_frame_info.bypass = !evc_vlan_up(data);
                tx_frame_info.maskerade_port = (evc_vlan_up(data)) ? data->config.port : VTSS_PORT_NO_NONE;
                tx_frame_info.isdx = data->tx_isdx;
                tx_frame_info.vid = data->config.flow.vid;
                tx_frame_info.vid_inj = vlan_down(data);
                tx_frame_info.qos = i;
                tx_frame_info.pcp = i;
                tx_frame_info.dp = ((test->dp == 0) ? FALSE : TRUE);

                payload_size = test->size - (12+2+4); /* Payload size is configured frame size subtracted untagged overhead and CRC */
                pdu_size = LBM_PDU_LENGTH + ((test->pattern == VTSS_MEP_SUPP_PATTERN_NONE) ? 4 : 3); /* PDU size with empty TLV is depending on the TLV type */
                pattern_size = 0;

                if (payload_size > pdu_size)   /* Check if pattern must be added to TLV - requested payload is bigger than TST PDU with empty TLV */
                    pattern_size = payload_size - pdu_size;













                tx_buffer_size = tx_header_size + pdu_size + pattern_size + offs;
                rc = vtss_mep_supp_packet_tx(instance, NULL, data->config.flow.port, tx_buffer_size, *tx_lbm, &tx_frame_info, VTSS_MEP_SUPP_RATE_INV, FALSE, 0, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_LBM : VTSS_MEP_SUPP_OAM_TYPE_NONE), data->config.sat);
                if (rc != VTSS_RC_OK)   vtss_mep_supp_trace("run_tx_lbm_timer: packet tx failed", instance, 0, 0, 0);

                if (VTSS_MEP_SUPP_ETREE_ELAN(data->config.evc_type)) {    /* This is an E-TREE instance. Any other instances in this E-TREE is potentially replying */
                    local_etree_elan_lbr(instance, *tx_lbm);
                }
            }
        }
    }

    data->lb_state.lbm_counter++;
}

static void run_rx_dmr_timer(u32 instance)
{
    supp_instance_data_t    *data;
    u32                     i;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_RX_DMR_TIMER;

    /* The expected DMR not coming. Give it up */
    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Check if any transmission is still ongoing */
        if (data->dmm_config.enable[i] && data->dm_state[i].dmm_tx_active) {
            data->dm_state[i].dmm_tx_active = FALSE;
            data->dm_state[i].dmr_rx_tout_cnt++;
        }
    }

    data->event_flags |= EVENT_OUT_NEW_DMR;
    if (!data->tx_dmm_timer)    /* Assure timeout of DMM tx timer */
    {
        data->event_flags |= EVENT_IN_TX_DMM_TIMER;
    }
}

static void run_tx_dmm_timer(u32 instance)
{
    u32                           rc, tx_header_size;
    supp_instance_data_t          *data;
    mesa_timestamp_t              timestamp;
    u32                           i, tc=0;
    vtss_mep_supp_tx_frame_info_t tx_frame_info;
    dm_state_t                    *state;
    BOOL                          tx_started=FALSE;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_TX_DMM_TIMER;

    if (!dm_enable_calc(data->dmm_config.enable))   return;

    data->tx_dmm_timer = (data->dmm_config.interval * 10) / timer_res;  /* Restart the common transmission timer */
    vtss_mep_supp_timer_start();

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Check if any transmission is still ongoing */
        if (data->dmm_config.enable[i] && data->dm_state[i].dmm_tx_active)
            break;
    }

    if (i < VTSS_MEP_SUPP_PRIO_MAX) {
        vtss_mep_supp_trace("run_tx_dmm_timer. (Still) waiting for DMR. prio", i, 0, 0, 0);
        return;     /* Still waiting for DMR */
    }

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Transmit any enabled DMM */
        if (!data->dmm_config.enable[i])    continue;

        state = &data->dm_state[i];

        tx_header_size = prio_tx_header_size(instance, i, OAM_TYPE_DMM);
        vtss_mep_supp_timestamp_get(&timestamp, &tc);
        var_to_string(&state->tx_dm[tx_header_size + 4], timestamp.seconds);
        var_to_string(&state->tx_dm[tx_header_size + 8], timestamp.nanoseconds);

        data->onestep_extra_m.tag_cnt = tx_tag_cnt_calc(tx_header_size);

        state->tx_dmm_fup_timestamp.seconds = 0;
        state->tx_dmm_fup_timestamp.nanoseconds = 0;

        tx_frame_info.bypass = !evc_vlan_up(data);
        tx_frame_info.maskerade_port = (evc_vlan_up(data)) ? data->config.port : VTSS_PORT_NO_NONE;
        tx_frame_info.isdx = data->tx_isdx;
        tx_frame_info.vid = data->config.flow.vid;
        tx_frame_info.vid_inj = vlan_down(data);
        tx_frame_info.qos = i;
        tx_frame_info.pcp = i;
        tx_frame_info.dp = data->dmm_config.dei;

        if (!vtss_mep_supp_check_hw_timestamp()) {
            /* no h/w timestamp */
            rc = vtss_mep_supp_packet_tx(instance, &state->tx_dmm_fup_timestamp, data->config.flow.port, (tx_header_size + DMM_PDU_LENGTH), state->tx_dm, &tx_frame_info, VTSS_MEP_SUPP_RATE_INV, FALSE, 0, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_DMM : VTSS_MEP_SUPP_OAM_TYPE_NONE), data->config.sat);
        } else {
            /* one step */
#if DEBUG_PHY_TS
            u32 zero = 0;
            var_to_string(&state->tx_dm[tx_header_size + 4], zero);
            var_to_string(&state->tx_dm[tx_header_size + 8], zero);
            rc = vtss_mep_supp_packet_tx(instance, &state->tx_dmm_fup_timestamp, data->config.flow.port, (tx_header_size + DMM_PDU_LENGTH), state->tx_dm, &tx_frame_info, VTSS_MEP_SUPP_RATE_INV, FALSE, 0, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_DMM : VTSS_MEP_SUPP_OAM_TYPE_NONE), data->config.sat);
#else
            rc = vtss_mep_supp_packet_tx_one_step_ts(instance, data->config.flow.port, (tx_header_size + DMM_PDU_LENGTH), state->tx_dm, &tx_frame_info, tc, &data->onestep_extra_m, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_DMM : VTSS_MEP_SUPP_OAM_TYPE_NONE));
            rc = (rc == 1) ? VTSS_RC_OK : 1;
#endif
        }

        if (rc != VTSS_RC_OK)   vtss_mep_supp_trace("run_tx_dmm_timer: packet tx failed", instance, 0, 0, 0);

        tx_started = TRUE;
        state->dmm_tx_active = TRUE;
        state->active = TRUE;
        state->dm_tx_cnt++;
    }

    if (tx_started) {
        /* If DMR not received in one second, treat it as timeout */
        data->rx_dmr_timeout_timer = 1000 / timer_res;
    }
}

static void run_tx_dm1_timer(u32 instance)
{
    u32                           rc, tx_header_size;
    mesa_timestamp_t              timestamp;
    supp_instance_data_t          *data;
    u32                           i, tc=0;
    vtss_mep_supp_tx_frame_info_t tx_frame_info;
    dm_state_t                    *state;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_TX_1DM_TIMER;

    if (!dm_enable_calc(data->dm1_config.enable))   return;

    data->tx_dm1_timer = (data->dm1_config.interval * 10) / timer_res;
    vtss_mep_supp_timer_start();

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Transmit any enabled 1DM */
        if (!data->dm1_config.enable[i])    continue;

        state = &data->dm_state[i];

        tx_header_size = prio_tx_header_size(instance, i, OAM_TYPE_1DM);
        vtss_mep_supp_timestamp_get(&timestamp, &tc);
        var_to_string(&state->tx_dm[tx_header_size + 4], timestamp.seconds);
        var_to_string(&state->tx_dm[tx_header_size + 8], timestamp.nanoseconds);

        data->onestep_extra_m.tag_cnt = tx_tag_cnt_calc(tx_header_size);

        tx_frame_info.bypass = !evc_vlan_up(data);
        tx_frame_info.maskerade_port = (evc_vlan_up(data)) ? data->config.port : VTSS_PORT_NO_NONE;
        tx_frame_info.isdx = data->tx_isdx;
        tx_frame_info.vid = data->config.flow.vid;
        tx_frame_info.vid_inj = vlan_down(data);
        tx_frame_info.qos = i;
        tx_frame_info.pcp = i;
        tx_frame_info.dp = data->dm1_config.dei;

        if (data->dm1_config.proprietary) {
            // Two-step.

            if (!vtss_mep_supp_check_hw_timestamp()) {
                // No H/W timestamp. Tx DM immediately followed by DM-follow-up
                // time-stamped with the time we got from the first Tx.
                mesa_timestamp_t tx_timestamp;
                if ((rc = vtss_mep_supp_packet_tx(instance, &tx_timestamp, data->config.flow.port, (tx_header_size + DM1_PDU_LENGTH), state->tx_dm, &tx_frame_info, VTSS_MEP_SUPP_RATE_INV, FALSE, 0, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_1DM : VTSS_MEP_SUPP_OAM_TYPE_NONE), data->config.sat)) == VTSS_RC_OK) {
                    // Tx follow-up right away
                    dm1_fup_tx(instance, i, &tx_timestamp);
                }
            } else {
                // H/W timestamp. Tx DM and wait for timestamp of that to come
                // back before sending DM-follow-up.
                data->ts_done_dm1.instance = instance;
                data->ts_done_dm1.prio     = i;
                data->ts_done_dm1.cb       = dm1_fup_done;
                rc = vtss_mep_supp_packet_tx_two_step_ts(instance, &data->ts_done_dm1, data->config.flow.port, (tx_header_size + DM1_PDU_LENGTH), state->tx_dm);
                rc = (rc == 1) ? VTSS_RC_OK : 1;
            }
        } else {
            if (!vtss_mep_supp_check_hw_timestamp()) {
                /* no h/w timestamp*/
                rc = vtss_mep_supp_packet_tx(instance, NULL, data->config.flow.port, (tx_header_size + DM1_PDU_LENGTH), state->tx_dm, &tx_frame_info, VTSS_MEP_SUPP_RATE_INV, FALSE, 0, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_1DM : VTSS_MEP_SUPP_OAM_TYPE_NONE), data->config.sat);
            } else {
                /* one step */
#if DEBUG_PHY_TS
                u32 zero = 0;
                var_to_string(&state->tx_dm[tx_header_size + 4], zero);
                var_to_string(&state->tx_dm[tx_header_size + 8], zero);
                rc = vtss_mep_supp_packet_tx(instance, NULL, data->config.flow.port, (tx_header_size + DM1_PDU_LENGTH), state->tx_dm, &tx_frame_info, VTSS_MEP_SUPP_RATE_INV, FALSE, 0, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_1DM : VTSS_MEP_SUPP_OAM_TYPE_NONE), data->config.sat);
#else
                rc = vtss_mep_supp_packet_tx_one_step_ts(instance, data->config.flow.port, (tx_header_size + DM1_PDU_LENGTH), state->tx_dm, &tx_frame_info, tc, &data->onestep_extra_m, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_1DM : VTSS_MEP_SUPP_OAM_TYPE_NONE));
                rc = (rc == 1) ? VTSS_RC_OK : 1;
#endif
            }
        }

        if (rc != VTSS_RC_OK)   vtss_mep_supp_trace("run_tx_dm1_timer: packet tx failed", instance, 0, 0, 0);

        state->active = TRUE;
        state->dm_tx_cnt++;
        data->event_flags |= EVENT_OUT_NEW_DM1;
    }
}

static void run_rx_lbm_timer(u32 instance)
{
    supp_instance_data_t  *data;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_RX_LBM_TIMER;

}

static BOOL find_evc_vlan_upmep(u16 vid, u8 level, supp_instance_data_t **mep_data)
{
    supp_instance_data_t *data;
    u32 i;

    for (i = 0; i < VTSS_MEP_SUPP_CREATED_MAX; ++i) {
        data = &instance_data[i];
        if (!data->config.enable)                                  continue;
        if (!evc_vlan_up(data))                                    continue;
        if (data->config.level < level)                            continue;         /* Check level */
        if (data->config.flow.vid != vid)                          continue;         /* Check classified VID */
        *mep_data = data;
        return TRUE;
    }
    return FALSE;
}

static BOOL find_evc_vlan_downmep(u16 vid, u8 level, u32 port, supp_instance_data_t **mep_data)
{
    supp_instance_data_t *data;
    u32 i;

    for (i = 0; i < VTSS_MEP_SUPP_CREATED_MAX; ++i) {
        data = &instance_data[i];
        if (!data->config.enable)                                  continue;
        if (!evc_vlan_down(data))                                  continue;
        if (data->config.level < level)                            continue;         /* Check level */
        if (data->config.port != port)                             continue;         /* Check port */
        if (data->config.flow.vid != vid)                          continue;         /* Check classified VID */
        *mep_data = data;
        return TRUE;
    }
    return FALSE;
}

static void defect_timer_calc(supp_instance_data_t  *data,   defect_timer_t *defect,   u32 period)
{
    if (period > defect->old_period)       defect->old_period = period;
    defect->timer = pdu_period_to_timer[defect->old_period];

    data->defect_timer_active = TRUE;
    vtss_mep_supp_timer_start();
}

static void run_tx_ais_timer(u32 instance)
{
    u32                           rc, header_size, i;
    mesa_port_list_t              inj_port;
    supp_instance_data_t          *data, *mep_data;
    vtss_mep_supp_tx_frame_info_t tx_frame_info;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_TX_AIS_TIMER;

    if (!data->ais_config.flow_count) return;

    data->tx_ais_timer = 1;

    vtss_mep_supp_timer_start();

    /* Init of AIS transmitter frame */
    if (data->ais_config.dmac[0] & 1) {
        data->ais_config.dmac[5] = 0x30 | data->ais_config.level[data->ais_inx];
    }
    header_size = init_client_tx_frame(instance, data->ais_config.dmac, data->config.mac, data->ais_config.prio[data->ais_inx], data->ais_config.dei, data->tx_ais);

    data->tx_ais[header_size]     = data->ais_config.level[data->ais_inx] << 5;
    data->tx_ais[header_size + 1] = OAM_TYPE_AIS;
    data->tx_ais[header_size + 2] = rate_to_pdu_calc(data->ais_config.rate);
    data->tx_ais[header_size + 3] = 0;
    data->tx_ais[header_size + 4] = 0;

    tx_frame_info.bypass         = TRUE;
    tx_frame_info.maskerade_port = VTSS_PORT_NO_NONE;
    tx_frame_info.isdx           = 0;    /* The ISDX for AIS in EVC can be fetched from EVC API */
    tx_frame_info.vid            = data->ais_config.flows[data->ais_inx].vid;
    tx_frame_info.vid_inj        = (data->ais_config.flows[data->ais_inx].mask & VTSS_MEP_SUPP_FLOW_MASK_MPLS_LSP) ? FALSE : TRUE;
    tx_frame_info.qos            = data->ais_config.prio[data->ais_inx];
    tx_frame_info.pcp            = data->ais_config.prio[data->ais_inx];
    tx_frame_info.dp             = data->ais_config.dei;

    if (data->ais_config.flows[data->ais_inx].mask & (VTSS_MEP_SUPP_FLOW_MASK_EVC_ID | VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID)) {
        /* Simulate hit of AIS on local EVC/VLAN Down MEP on the server residence port if any: */
        if (find_evc_vlan_downmep(tx_frame_info.vid, data->ais_config.level[data->ais_inx], data->config.port, &mep_data)) {
            inj_port.clear_all();   /* Do not inject on any port */
            if (data->ais_config.level[data->ais_inx] == mep_data->config.level) {  /* Simulate RX of AIS */
                if (!mep_data->defect_state.dAis)    mep_data->event_flags |= EVENT_OUT_NEW_DEFECT;
                mep_data->defect_state.dAis = TRUE;
                defect_timer_calc(mep_data, &mep_data->dAis, data->tx_ais[header_size + 2] & 0x07);
            }
        } else {    /* If no Down-MEP on server residence port was hit - continue */
            inj_port = data->ais_config.flows[data->ais_inx].port;

            /* Simulate hit of AIS on local EVC/VLAN UP MEP if any: */
            if (find_evc_vlan_upmep(tx_frame_info.vid, data->ais_config.level[data->ais_inx], &mep_data)) {
                inj_port[mep_data->config.port] = FALSE; /* Do not inject on port with hit Up_MEP */
                if (data->ais_config.level[data->ais_inx] == mep_data->config.level) {    /* Simulate RX of AIS */
                    if (!mep_data->defect_state.dAis)    mep_data->event_flags |= EVENT_OUT_NEW_DEFECT;
                    mep_data->defect_state.dAis = TRUE;
                    defect_timer_calc(mep_data, &mep_data->dAis, data->tx_ais[header_size + 2] & 0x07);
                }
            }

            /* Simulate hit of AIS on local EVC/VLAN Down MEP on the client ports if any: */
            for (i=0; i<mep_caps.mesa_port_cnt; ++i) {
                if (inj_port[i] && find_evc_vlan_downmep(tx_frame_info.vid, data->ais_config.level[data->ais_inx], i, &mep_data)) {  /* Do not inject on ports with Down-MEP blocking */
                    inj_port[i] = FALSE;
                }
            }
        }
    } else if (data->ais_config.flows[data->ais_inx].mask & (VTSS_MEP_SUPP_FLOW_MASK_MPLS_LSP | VTSS_MEP_SUPP_FLOW_MASK_MPLS_LSP_FWD)) {
        inj_port = data->ais_config.flows[data->ais_inx].port;
    }

    rc = vtss_mep_supp_packet_tx(instance, NULL, inj_port, (header_size + AIS_PDU_LENGTH), data->tx_ais, &tx_frame_info, VTSS_MEP_SUPP_RATE_INV, FALSE, 0, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_GENERIC : VTSS_MEP_SUPP_OAM_TYPE_NONE), data->config.sat);
    if (rc != VTSS_RC_OK)   vtss_mep_supp_trace("run_tx_ais_timer: packet tx failed", instance, 0, 0, 0);

    data->ais_inx++;
    if (data->ais_inx >= data->ais_config.flow_count) {  /* Transmit AIS in all configured flows (EVC/VLAN) */
        data->ais_inx = 0;
        if (data->ais_count < 3)    data->ais_count++;  /* This counter is used for transmitting 3 frames as fast as possible in case 'Protection' is selected */
    }

    data->tx_ais_timer = tx_timer_calc(data->ais_config.rate) / data->ais_config.flow_count;
    if (data->tx_ais_timer == 0) {
        data->tx_ais_timer = 1;
    }

    if (data->ais_count < 3)
    {
        /* Still AIS frames to be send fast as possible */
        data->event_flags |= EVENT_IN_TX_AIS_TIMER;       /* Simulate run out of TX AIS timer */
        vtss_mep_supp_run();
    }
}

static void run_tx_lck_timer(u32 instance)
{
    u32                             rc, header_size, i;
    mesa_port_list_t                inj_port;
    supp_instance_data_t            *data, *mep_data;
    vtss_mep_supp_tx_frame_info_t   tx_frame_info;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_TX_LCK_TIMER;

    if (!data->lck_config.enable)   return;

    if (!data->lck_config.flow_count) return;

    data->tx_lck_timer = 1;

    vtss_mep_supp_timer_start();

    /* Init of LCK transmitter frame */
    if (data->lck_config.dmac[0] & 1) {
        data->lck_config.dmac[5] = 0x30 | data->lck_config.level[data->lck_inx];
    }
    header_size = init_client_tx_frame(instance, data->lck_config.dmac, data->config.mac, data->lck_config.prio[data->lck_inx], data->lck_config.dei, data->tx_lck);
    data->tx_lck[header_size] = data->lck_config.level[data->lck_inx] << 5;
    data->tx_lck[header_size + 1] = OAM_TYPE_LCK;
    data->tx_lck[header_size + 2] = rate_to_pdu_calc(data->lck_config.rate);
    data->tx_lck[header_size + 3] = 0;
    data->tx_lck[header_size + 4] = 0;

    tx_frame_info.bypass         = TRUE;
    tx_frame_info.maskerade_port = VTSS_PORT_NO_NONE;
    tx_frame_info.isdx           = data->tx_isdx;
    tx_frame_info.vid            = data->lck_config.flows[data->lck_inx].vid;
    tx_frame_info.vid_inj        = (data->lck_config.flows[data->lck_inx].mask & VTSS_MEP_SUPP_FLOW_MASK_MPLS_LSP) ? FALSE : TRUE;
    tx_frame_info.qos            = data->lck_config.prio[data->lck_inx];
    tx_frame_info.pcp            = data->lck_config.prio[data->lck_inx];
    tx_frame_info.dp             = data->lck_config.dei;

    if (data->lck_config.flows[data->lck_inx].mask & (VTSS_MEP_SUPP_FLOW_MASK_EVC_ID | VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID)) {
        /* Simulate hit of LCK on local EVC/VLAN Down MEP on the server residence port if any: */
        if (find_evc_vlan_downmep(tx_frame_info.vid, data->lck_config.level[data->lck_inx], data->config.port, &mep_data)) {
            inj_port.clear_all();   /* Do not inject on any port */
            if (data->lck_config.level[data->lck_inx] == mep_data->config.level) {  /* Simulate RX of LCK */
                if (!mep_data->defect_state.dLck)    mep_data->event_flags |= EVENT_OUT_NEW_DEFECT;
                mep_data->defect_state.dLck = TRUE;
                defect_timer_calc(mep_data, &mep_data->dLck, data->tx_lck[header_size + 2] & 0x07);
            }
        } else {    /* If no Down-MEP on server residence port was hit - continue */
            inj_port = data->lck_config.flows[data->lck_inx].port;

            /* Simulate hit of LCK on local EVC/VLAN UP MEP if any: */
            if (find_evc_vlan_upmep(tx_frame_info.vid, data->lck_config.level[data->lck_inx], &mep_data)) {
                inj_port[mep_data->config.port] = FALSE; /* Do not inject on port with hit Up_MEP */
                if (data->lck_config.level[data->lck_inx] == mep_data->config.level) {    /* Simulate RX of LCK */
                    if (!mep_data->defect_state.dLck)    mep_data->event_flags |= EVENT_OUT_NEW_DEFECT;
                    mep_data->defect_state.dLck = TRUE;
                    defect_timer_calc(mep_data, &mep_data->dLck, data->tx_lck[header_size + 2] & 0x07);
                }
            }

            /* Simulate hit of LCK on local EVC/VLAN Down MEP on the client ports if any: */
            for (i=0; i<mep_caps.mesa_port_cnt; ++i) {
                if (inj_port[i] && find_evc_vlan_downmep(tx_frame_info.vid, data->lck_config.level[data->lck_inx], i, &mep_data)) {  /* Do not inject on ports with Down-MEP blocking */
                    inj_port[i] = FALSE;
                }
            }
        }
    } else if (data->lck_config.flows[data->lck_inx].mask & (VTSS_MEP_SUPP_FLOW_MASK_MPLS_LSP | VTSS_MEP_SUPP_FLOW_MASK_MPLS_LSP_FWD)) {
        inj_port = data->lck_config.flows[data->lck_inx].port;
    }

    rc = vtss_mep_supp_packet_tx(instance, NULL, inj_port, (header_size + LCK_PDU_LENGTH), data->tx_lck, &tx_frame_info, VTSS_MEP_SUPP_RATE_INV, FALSE, 0, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_GENERIC : VTSS_MEP_SUPP_OAM_TYPE_NONE), data->config.sat);
    if (rc != VTSS_RC_OK)   vtss_mep_supp_trace("run_tx_lck_timer: packet tx failed", instance, 0, 0, 0);

    if (!(data->lck_config.flows[data->lck_inx].mask & VTSS_MEP_SUPP_FLOW_MASK_MPLS_LSP_FWD)) {
        data->tx_lck_timer = tx_timer_calc(data->lck_config.rate) / data->lck_config.flow_count;
        if (data->tx_lck_timer == 0) {
            data->tx_lck_timer = 1;
        }
        data->lck_inx++;
        if (data->lck_inx >= data->lck_config.flow_count)   /* Transmit LCK in all configured flows */
            {
                data->lck_inx = 0;
                if (data->lck_count < 1)    data->lck_count++;   /* This counter could be used for transmitting 3 frames as fast as possible - currently unused */
            }
    }

    if (data->lck_count < 1) {
        /* Still LCK frames to be send fast as possible */
        instance_data[instance].event_flags |= EVENT_IN_TX_LCK_TIMER;       /* Simulate run out of TX LCK timer */
        vtss_mep_supp_run();
    }
}

static void run_rdi_timer (u32 instance)
{
    supp_instance_data_t   *data;
    BOOL                   rdi;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_RDI_TIMER;

    vtss_mep_supp_trace("run_ccm_rdi: Enter  instance", instance, 0, 0, 0);

    if ((data->ccm_gen.enable) && hw_ccm_calc(data->ccm_gen.cc_rate))
    {   /* HW generated CCM */
        rdi = ((data->tx_ccm_hw[data->tx_header_size + 2] & 0x80) != 0) ? TRUE : FALSE;
        if (rdi != data->ccm_rdi)  run_async_ccm_gen(instance);
    }
}

static void run_tst_timer (u32 instance)
{
    supp_instance_data_t  *data;
    mesa_oam_voe_conf_t   cfg;

    data = &instance_data[instance];    /* Instance data reference */

    data->event_flags &= ~EVENT_IN_TST_TIMER;

    if (data->voe_idx < mep_caps.mesa_oam_voe_cnt) { /* VOE based TST */
        (void)mesa_oam_voe_conf_get(NULL, data->voe_idx, &cfg);
        cfg.tst.copy_to_cpu = TRUE;
        (void)mesa_oam_voe_conf_set(NULL, data->voe_idx, &cfg);
    }
}

static void lm_counter_calc(const u32 instance,
                            const u32 peer_idx,
                            lm_counter_t *lm,
                            BOOL                        *lm_start,
                            u32                         near_CT2,
                            u32                         far_CT2,
                            u32                         near_CR2,
                            u32                         far_CR2,
                            bool        has_near_rx)
{
    vtss_mep_supp_lm_peer_counters_t peer_counters;

    if (!*lm_start) { /* The first measurement is only used to initialize the 'previously frame counters */

        get_lm_peer_counters(instance, lm, &peer_counters);

        /* See Appendix III in Y.1731  */
        peer_counters.far_tx_delta  = (far_CT2  - lm->far_CT1);
        peer_counters.near_tx_delta = (near_CT2 - lm->near_CT1);
        peer_counters.near_rx_delta = (near_CR2 - lm->near_CR1);
        peer_counters.far_rx_delta  = (far_CR2  - lm->far_CR1);

        peer_counters.has_near_rx = has_near_rx;

        /* pass new measurement counters: */
        vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
        vtss_mep_supp_lm_measurement(instance, peer_idx, &peer_counters);
        vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    } else {  // first time
        vtss_mep_supp_lm_measurement_init(instance, peer_idx);
    }
    *lm_start = FALSE;

    lm->near_CT1 = near_CT2;
    lm->near_CR1 = near_CR2;
    lm->far_CT1 = far_CT2;
    lm->far_CR1 = far_CR2;

    return;
}

static BOOL ccm_defect_calc(u32 instance,   supp_instance_data_t *data,   u8 smac[],   u8 pdu[],   u32 prio,   BOOL *defect,   BOOL *state,   BOOL *mac)
{
    u32   i;
    u32   mep, level, period, rx_period;
    BOOL  rdi, unexp;

    /* Extract PDU information */
    level = pdu[0]>>5;
    mep = (pdu[8]<<8) + pdu[9];
    period = rx_period = pdu[2] & 0x07;
    rdi = (pdu[2] & 0x80);

    unexp = FALSE;

    if (period == 0)
    {
        unexp = TRUE;
        if (!data->defect_state.dInv)                       *defect = TRUE;
        if (data->ccm_state.state.unexp_period != period)   *state = TRUE;
        data->defect_state.dInv = TRUE;
        data->ccm_state.state.unexp_period = period;
        goto return_;
    }
    else
    {
        if (data->defect_state.dInv)    *defect = TRUE;
        data->defect_state.dInv = FALSE;
    }

    if (voe_ccm_enable(data)) { /* This is a VOE based CCM - hit me once poll of PDU */
        if (period < rate_to_pdu_calc(VTSS_MEP_SUPP_RATE_10S))  period = rate_to_pdu_calc(VTSS_MEP_SUPP_RATE_10S);
    }
    else {  /* Either this is not VOE based MEP or there are more peers. In this case either all CCM come to CPU or they are polled with 1s interval */
        if (!data->config.voe && (data->ccm_config.peer_count == 1))  period = rate_to_pdu_calc(VTSS_MEP_SUPP_RATE_1S);      /* Not VOE and only one peer - 1s. ACL HMO */
    }

    /* Calculate all defects related to this CCM PDU */
    if (level != data->config.level)
    {
        unexp = TRUE;
        if (!data->defect_state.dLevel)                  *defect = TRUE;
        if (data->ccm_state.state.unexp_level != level)  *state = TRUE;
        data->defect_state.dLevel = TRUE;
        data->ccm_state.state.unexp_level = level;
        defect_timer_calc(data, &data->dLevel, period);
        data->ccm_defect_active = TRUE;
        goto return_;
    }
    if (memcmp(&pdu[10], data->exp_meg, data->exp_meg_len))
    {
        unexp = TRUE;
        if (!data->defect_state.dMeg)                                       *defect = TRUE;
        if (memcmp(&pdu[10], data->ccm_state.unexp_meg_id, MEG_ID_LENGTH))  *state = TRUE;
        data->defect_state.dMeg = TRUE;
        memcpy(data->ccm_state.unexp_meg_id, &pdu[10], MEG_ID_LENGTH);
        defect_timer_calc(data, &data->dMeg, period);
        data->ccm_defect_active = TRUE;
        goto return_;
    }
    if (mep == data->ccm_config.mep)    /* Check if this CCM is from this instance MEP ID */
    {
        unexp = TRUE;

        if (!memcmp(smac, data->config.mac, VTSS_MEP_SUPP_MAC_LENGTH)) {    /* Check if SMAC is this instance SMAC */
            if (!data->defect_state.dLoop)  *defect = TRUE;   /* SMAC is same as this instance SMAC - Loop is detected */
            data->defect_state.dLoop = TRUE;
            defect_timer_calc(data, &data->dLevel, period);
        } else {
            if (!data->defect_state.dConfig)  *defect = TRUE;   /* SMAC is different from this instance SMAC - Configuration error is detected */
            data->defect_state.dConfig = TRUE;
            defect_timer_calc(data, &data->dConfig, period);
        }
        data->ccm_defect_active = TRUE;
    }
    for (i=0; i<data->ccm_config.peer_count; ++i)   if (data->ccm_config.peer_mep[i] == mep)   break;
    if (i == data->ccm_config.peer_count)   /* Valid Peer-MEP is not found */
    {
        unexp = TRUE;
        if (!data->defect_state.dMep)               *defect = TRUE;
        if (data->ccm_state.state.unexp_mep != mep) *state = TRUE;
        data->defect_state.dMep = TRUE;
        data->ccm_state.state.unexp_mep = mep;
        defect_timer_calc(data, &data->dMep, period);
        data->ccm_defect_active = TRUE;
        goto return_;
    } else {    /* Valid Peer-MEP found */
        *mac =   (memcmp(data->peer_mac[i], smac, VTSS_MEP_SUPP_MAC_LENGTH) != 0) ? TRUE : FALSE;
        if (*mac) memcpy(data->peer_mac[i], smac, VTSS_MEP_SUPP_MAC_LENGTH);
    }
    if (pdu_period_to_rate[rx_period] != data->ccm_config.rate)
    {
        if (!data->defect_state.dPeriod[i])                   *defect = TRUE;
        if (data->ccm_state.state.unexp_period != rx_period)  *state = TRUE;
        data->defect_state.dPeriod[i] = TRUE;
        data->ccm_state.state.unexp_period = rx_period;
        defect_timer_calc(data, &data->dPeriod[i], period);
        data->ccm_defect_active = TRUE;
    }
    if ((prio != 0xFFFFFFFF) && (prio != data->ccm_config.prio))
    {
        if (!data->defect_state.dPrio[i])               *defect = TRUE;
        if (data->ccm_state.state.unexp_prio != prio)   *state = TRUE;
        data->defect_state.dPrio[i] = TRUE;
        data->ccm_state.state.unexp_prio = prio;
        defect_timer_calc(data, &data->dPrio[i], period);
        data->ccm_defect_active = TRUE;
    }
    if (data->defect_state.dRdi[i] != rdi)
    {
        *defect = TRUE;
        data->defect_state.dRdi[i] = rdi;
    }

    if (data->defect_state.dLoc[i] & 0x01)
        *defect = TRUE;

    data->defect_state.dLoc[i] &= ~0x01;   /* sw detected LOC is passive */
    data->rx_ccm_timer[i] = data->loc_timer_val;
    vtss_mep_supp_timer_start();

    return_:

    return(unexp);
}

static BOOL tx_reply_buff_alloc(supp_instance_data_t *data,  u32 pdu_size,  u8 **tx_reply,  u32 *tx_reply_buffer_size,  BOOL *init)
{
    *init = FALSE;
    if ((*tx_reply != NULL) && (*tx_reply_buffer_size < (HEADER_MAX_SIZE + pdu_size))) {  /* In case a buffer is already allocate but is too small - free */
        vtss_mep_supp_lm_trace("tx_reply_buff_alloc  Free old Reply", 0, 0, 0, 0);
        vtss_mep_supp_packet_tx_free(*tx_reply);
        *tx_reply = NULL;
    }
    if (*tx_reply == NULL) {   /* Allocate a buffer if needed */
        vtss_mep_supp_lm_trace("tx_reply_buff_alloc  Alloc Reply", 0, 0, 0, 0);
        if (!(*tx_reply = vtss_mep_supp_packet_tx_alloc(HEADER_MAX_SIZE + pdu_size)))
            return(FALSE);
        *init = TRUE;
    }
    *tx_reply_buffer_size = HEADER_MAX_SIZE + pdu_size;
    return(TRUE);
}

static void reply_pcp_dei_calc(supp_instance_data_t *data, vtss_mep_supp_frame_info_t *frame_info, u32 *pcp, BOOL *dei)
{
    if ((data->config.mode == VTSS_MEP_SUPP_MIP)) { /* Subscriber domain */
        *pcp = (frame_info->itag_vid != 0) ? frame_info->itag_pcp : (frame_info->otag_vid != 0) ? frame_info->otag_pcp : frame_info->pcp;
        *dei = (frame_info->itag_vid != 0) ? frame_info->itag_dei : (frame_info->otag_vid != 0) ? frame_info->otag_dei : frame_info->dei;
    } else {
        if (data->config.flow.mask & (VTSS_MEP_SUPP_FLOW_MASK_EVC_ID | VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID)) {
            *pcp = frame_info->pcp;
            *dei = frame_info->dei;
        } else {
            *pcp = frame_info->otag_pcp;
            *dei = frame_info->otag_dei;
        }
    }
}

/*lint -e{454, 455, 456} ... The mutex is locked so it is ok to unlock */
static void lmr_to_peer(u32 instance, supp_instance_data_t *data, u8 smac[], u8 lmm[], u32 lmm_len, vtss_mep_supp_frame_info_t *frame_info, u32 port, BOOL is_mpls, u8 prio, u32 *rx_frames)
{
    u32                             rc=TRUE;
    u32                             tx_frames=0, pcp;
    vtss_mep_supp_tx_frame_info_t   tx_frame_info;
    BOOL                            init, dei;

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
    vtss_mep_supp_counters_get(instance, rx_frames, &tx_frames);
    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    if (!data->config.enable)   return;

    /* Allocate LMR TX buffer if not already allocated */
    if (!tx_reply_buff_alloc(data, lmm_len, &data->tx_lmr, &data->tx_lmr_buffer_size, &init))
        return;

    if (init) { /* Init of LMR transmitted frame */
        (void)init_tx_frame(instance, smac, data->config.mac, 0, 0, data->tx_lmr, OAM_TYPE_LMR, 0);
    }

    /* Generate LMR frame */
    if (is_mpls) {
        data->tx_header_size = init_tx_frame(instance, smac, data->config.mac, prio, 0, data->tx_lmr, OAM_TYPE_LMR);
    } else {
        /* DMAC */
        memcpy(data->tx_lmr, smac, VTSS_MEP_SUPP_MAC_LENGTH);
        /* PCP and DEI */
        reply_pcp_dei_calc(data, frame_info, &pcp, &dei);
        insert_reply_prio(data, data->tx_lmr, pcp, dei, frame_info->pcp);
    }
    /* PDU */
    memcpy(&data->tx_lmr[data->tx_header_size], lmm, lmm_len);
    data->tx_lmr[data->tx_header_size+1] = OAM_TYPE_LMR;
    if (is_mpls && c_s_tag(&data->tx_lmr[12])) {
        data->tx_lmr[12+2] = (data->tx_lmr[12+2] & 0x0F) | (frame_info->pcp << 5) | (frame_info->dei << 4);
    }
    /* Counters */
    if (data->config.voe) { /* This is a VOE based instance - Up-MEP only */
        if (mep_caps.mesa_mep_serval) {
            u32 mip;
            mesa_oam_voe_counter_t voe_counter;
            (void)mesa_oam_voe_counter_get(NULL, data->voe_idx, &voe_counter);
            if ((frame_info->oam_info>>27) != voe_counter.lm.up_mep.rx_lmm_sample_seq_no) {
                vtss_mep_supp_trace("lmr_to_peer: Unexpected LMM LM sequence number", instance, frame_info->oam_info>>27, voe_counter.lm.up_mep.rx_lmm_sample_seq_no, 0);
                return;
            }
            *rx_frames = voe_counter.lm.up_mep.lmm;
            if (up_mip_present(data->config.flow.vid, &mip)) { /* This is a VOE based Up-MEP with a Up-MIP behind. Any LBM or LTM received by this MIP has to be added to 'rx_frames' */
                data->is2_rx_cnt += instance_data[mip].is2_rx_cnt;
                *rx_frames = *rx_frames + data->is2_rx_cnt;
                instance_data[mip].is2_rx_cnt = 0;
            }
            tx_frames = 1;   /* I think this must be != 0 in order to be overwritten by VOE */
        }
    }

    var_to_string(&data->tx_lmr[data->tx_header_size+8], *rx_frames);                   /* Rx counter in LMR */
    var_to_string(&data->tx_lmr[data->tx_header_size+12], tx_frames);                   /* Tx counter in LMR */

    tx_frame_info.bypass = !evc_vlan_up(data);
    tx_frame_info.maskerade_port = (evc_vlan_up(data)) ? data->config.port : VTSS_PORT_NO_NONE;
    tx_frame_info.isdx = data->tx_isdx;
    tx_frame_info.vid = data->config.flow.vid;
    tx_frame_info.vid_inj = vlan_down(data);
    tx_frame_info.qos = frame_info->qos;
    tx_frame_info.pcp = frame_info->pcp;
    tx_frame_info.dp = frame_info->dei;

    vtss_mep_supp_lm_trace("lmr_to_peer  instance  TxFCf  RxFCf  TxFCb", instance, string_to_var(&data->tx_lmr[data->tx_header_size+4]), string_to_var(&data->tx_lmr[data->tx_header_size+8]), string_to_var(&data->tx_lmr[data->tx_header_size+12]));
    rc = vtss_mep_supp_packet_tx_1(instance, NULL, port, (data->tx_header_size + lmm_len), data->tx_lmr, &tx_frame_info, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_LMR : VTSS_MEP_SUPP_OAM_TYPE_NONE));
    if (!rc)    vtss_mep_supp_trace("lmr_to_peer: packet tx failed", 0, 0, 0, 0);
    if (rc) {
        data->lm_state[0].lm_counter.tx_rsp_counter++;
    }
}

static void slr_to_peer(u32 instance,  supp_instance_data_t *data, u32 peer_idx, u8 smac[], u8 slm[], u32 slm_len, vtss_mep_supp_frame_info_t *frame_info, u32 port, u32 *rx_frames)
{
    u32                             rc=TRUE;
    u32                             pcp;
    vtss_mep_supp_tx_frame_info_t   tx_frame_info;
    BOOL                            init, dei;

    *rx_frames = 0;

    /* Allocate SLR TX buffer if not already allocated */
    if (!tx_reply_buff_alloc(data, slm_len, &data->tx_lmr, &data->tx_lmr_buffer_size, &init))
        return;

    if (init) { /* Init of SLR transmitted frame */
        (void)init_tx_frame(instance, smac, data->config.mac, 0, 0, data->tx_lmr, OAM_TYPE_SLR, 0);
    }

    /* Generate SLR frame */
    /* DMAC */
    memcpy(data->tx_lmr, smac, VTSS_MEP_SUPP_MAC_LENGTH);
    /* PCP and DEI */
    reply_pcp_dei_calc(data, frame_info, &pcp, &dei);
    insert_reply_prio(data, data->tx_lmr, pcp, dei, frame_info->pcp);
    /* PDU */
    memcpy(&data->tx_lmr[data->tx_header_size], slm, slm_len);
    data->tx_lmr[data->tx_header_size+1] = OAM_TYPE_SLR;
    data->tx_lmr[data->tx_header_size+6] = (data->ccm_config.mep & 0xFF00) >> 8;
    data->tx_lmr[data->tx_header_size+7] = data->ccm_config.mep & 0xFF;
    /* Counters */
    *rx_frames = data->lm_state[peer_idx].sl_service_counter.rx_slm_counter;
    var_to_string(&data->tx_lmr[data->tx_header_size+16], *rx_frames);

    tx_frame_info.bypass = !evc_vlan_up(data);
    tx_frame_info.maskerade_port = (evc_vlan_up(data)) ? data->config.port : VTSS_PORT_NO_NONE;
    tx_frame_info.isdx = data->tx_isdx;
    tx_frame_info.vid = data->config.flow.vid;
    tx_frame_info.vid_inj = vlan_down(data);
    tx_frame_info.qos = frame_info->qos;
    tx_frame_info.pcp = frame_info->pcp;
    tx_frame_info.dp = frame_info->dei;

    vtss_mep_supp_lm_trace("slr_to_peer  instance  TxFCf  TxFCb  tx_header_size", instance, string_to_var(&data->tx_lmr[data->tx_header_size+12]), string_to_var(&data->tx_lmr[data->tx_header_size+16]), data->tx_header_size);
    rc = vtss_mep_supp_packet_tx_1(instance, NULL, port, (data->tx_header_size + slm_len), data->tx_lmr, &tx_frame_info, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_GENERIC : VTSS_MEP_SUPP_OAM_TYPE_NONE));
    if (!rc)    vtss_mep_supp_trace("slr_to_peer: packet tx failed", 0, 0, 0, 0);
    if (rc) {
        data->lm_state[peer_idx].sl_service_counter.tx_slr_counter++;
        data->lm_state[peer_idx].lm_counter.tx_rsp_counter++;
    }
}

static void ltr_to_peer(u32 instance,  supp_instance_data_t *data,  u8 ltm[],  u8 inner_tag[],  vtss_mep_supp_frame_info_t *frame_info,  u32 port,  BOOL forward,  BOOL mep,  u32 action)
{
    u32                             rc;
    u8                              *tx_pdu, *tx_ltr;
    u32                             header_size, pcp;
    BOOL                            dei;
    vtss_mep_supp_tx_frame_info_t   tx_frame_info;

    if (ltm[8]==0) return; // TTL is 0, silently ignore

    /* Generate LTR frame */
    if ((tx_ltr = vtss_mep_supp_packet_tx_alloc(HEADER_MAX_SIZE + LTR_PDU_LENGTH)) == NULL) {
        return;
    }

    /* Init of LTR transmitter frame */
    reply_pcp_dei_calc(data, frame_info, &pcp, &dei);
    header_size = init_tx_frame(instance, &ltm[9], data->config.mac, pcp, dei, tx_ltr, OAM_TYPE_LTR, frame_info->pcp);

    /* PDU */
    tx_pdu = &tx_ltr[header_size];
    tx_pdu[0] = ltm[0];
    tx_pdu[1] = OAM_TYPE_LTR;
    tx_pdu[2] = (ltm[2] & 0x80) | ((mep) ? 0x20 : 0x00) | ((forward) ? 0x40 : 0x00);
    tx_pdu[3] = 6;
    memcpy(&tx_pdu[4], &ltm[4], 4);    /* Transaction ID */
    tx_pdu[8] = ltm[8]-1;              /* Decrement TTL */
    tx_pdu[9] = action;                /* Relay action */
    tx_pdu[10] = 8;
    tx_pdu[11] = 0;
    tx_pdu[12] = 16;
    memcpy(&tx_pdu[13], &ltm[24], 8);  /* Last Egress Identifier */
    tx_pdu[21] = 0;
    tx_pdu[22] = 0;
    memcpy(&tx_pdu[23], data->config.mac, VTSS_MEP_SUPP_MAC_LENGTH);  /* Next Egress Identifier */
    tx_pdu[29] = (data->config.direction == VTSS_MEP_SUPP_DOWN) ? 5 : 6; /* down/up reply */
    tx_pdu[30] = 0;
    tx_pdu[31] = 7;
    tx_pdu[32] = 1;     /* down/up action is OK */
    memcpy(&tx_pdu[33], data->config.mac, VTSS_MEP_SUPP_MAC_LENGTH);  /* down/up Identifier */
    tx_pdu[39] = 0;

    tx_frame_info.bypass = !evc_vlan_up(data);
    tx_frame_info.maskerade_port = (evc_vlan_up(data)) ? data->config.port : VTSS_PORT_NO_NONE;
    tx_frame_info.isdx = data->tx_isdx;
    tx_frame_info.vid = data->config.flow.vid;
    tx_frame_info.vid_inj = vlan_down(data);
    tx_frame_info.qos = frame_info->qos;
    tx_frame_info.pcp = frame_info->pcp;
    tx_frame_info.dp = frame_info->dei;

    rc = vtss_mep_supp_packet_tx_1(instance, NULL, port, (header_size + LTR_PDU_LENGTH), tx_ltr, &tx_frame_info, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_LTR : VTSS_MEP_SUPP_OAM_TYPE_NONE));
    if (!rc)       vtss_mep_supp_trace("ltr_to_peer: packet tx failed", 0, 0, 0, 0);
    vtss_mep_supp_packet_tx_free(tx_ltr);
}

static void lbr_to_peer(u32 instance,   supp_instance_data_t *data,   u8 smac[],   u8 lbm[],   u32 lbm_len,   vtss_mep_supp_frame_info_t *frame_info,   u32 port,   BOOL is_mpls, u8 prio)
{
    u32                             rc;
    u32                             header_size, pcp;
    BOOL                            dei;
    vtss_mep_supp_tx_frame_info_t   tx_frame_info;

    /* Generate LBR frame */
    if ((data->tx_lbr = vtss_mep_supp_packet_tx_alloc(HEADER_MAX_SIZE + lbm_len)))
    {/* LBR tx buffer is/has been allocated */
        /* Init of LRR transmitter frame */
        reply_pcp_dei_calc(data, frame_info, &pcp, &dei);
        header_size = init_tx_frame(instance, smac, data->config.mac, is_mpls ? prio : pcp, dei, data->tx_lbr, OAM_TYPE_LBR, frame_info->pcp);

        /* PDU */
        memcpy(&data->tx_lbr[header_size], lbm, lbm_len);
        data->tx_lbr[header_size+1] = OAM_TYPE_LBR;
        if (is_mpls) {










































        } else {
            tx_frame_info.bypass = !evc_vlan_up(data);
            tx_frame_info.maskerade_port = (evc_vlan_up(data)) ? data->config.port : VTSS_PORT_NO_NONE;
            tx_frame_info.isdx = data->tx_isdx;
            tx_frame_info.vid = data->config.flow.vid;
            tx_frame_info.vid_inj = vlan_down(data);
            tx_frame_info.qos = frame_info->qos;
            tx_frame_info.pcp = frame_info->pcp;
            tx_frame_info.dp = frame_info->dei;
        }

        rc = vtss_mep_supp_packet_tx_1(instance, NULL, port, (header_size + lbm_len), data->tx_lbr, &tx_frame_info, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_LBR : VTSS_MEP_SUPP_OAM_TYPE_NONE));
        if (!rc)    vtss_mep_supp_trace("lbr_to_peer: packet tx failed", 0, 0, 0, 0);

        data->rx_lbm_timer = 2000/timer_res;   /* If no LBM is received in two sec. then free the TX buffer */
        vtss_mep_supp_timer_start();
    }
}

static void ltm_to_forward(u32 instance,   supp_instance_data_t *data,   u8 dmac[],   u8 ltm[],   vtss_mep_supp_frame_info_t *frame_info,   u32 port)
{
    u32                             rc;
    u8                              *tx_pdu, *tx_ltm;
    u32                             header_size, pcp;
    BOOL                            dei;
    vtss_mep_supp_tx_frame_info_t   tx_frame_info;

    if (ltm[8] <= 1) {
        return;  /* Don't forward with a 'zero' TTL */
    }

    if ((tx_ltm = vtss_mep_supp_packet_tx_alloc(HEADER_MAX_SIZE + LTM_PDU_LENGTH)) == NULL) {
        return;
    }

    /* Init of LTM transmitter frame */
    reply_pcp_dei_calc(data, frame_info, &pcp, &dei);
    header_size = init_tx_frame(instance, dmac, data->config.mac, pcp, dei, tx_ltm, OAM_TYPE_LTM, frame_info->pcp);

    /* PDU */
    memcpy(&tx_ltm[header_size], ltm, LTM_PDU_LENGTH);
    tx_pdu = &tx_ltm[data->tx_header_size];
    tx_pdu[8] -= 1;             /* Decrement TTL */
    tx_pdu[21] = 7;             /* Egress identifier */
    tx_pdu[22] = 0;
    tx_pdu[23] = 8;
    tx_pdu[24] = 0;
    tx_pdu[25] = 0;
    memcpy(&tx_pdu[26], data->config.mac, VTSS_MEP_SUPP_MAC_LENGTH);
    tx_pdu[32] = 0;

    if (mep_caps.mesa_mep_serval) {
        tx_frame_info.bypass = evc_vlan_up(data);
        tx_frame_info.maskerade_port = (!evc_vlan_up(data)) ? data->config.port : VTSS_PORT_NO_NONE;
    } else {
        tx_frame_info.bypass = TRUE;
        tx_frame_info.maskerade_port = VTSS_PORT_NO_NONE;
    }
    tx_frame_info.vid = data->config.flow.vid;
    tx_frame_info.vid_inj = FALSE;
    tx_frame_info.isdx = data->tx_isdx;
    tx_frame_info.qos = frame_info->qos;
    tx_frame_info.pcp = frame_info->pcp;
    tx_frame_info.dp = frame_info->dei;

    rc = vtss_mep_supp_packet_tx_1(instance, NULL, port, (header_size + LTM_PDU_LENGTH), tx_ltm, &tx_frame_info, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_LTM : VTSS_MEP_SUPP_OAM_TYPE_NONE));
    vtss_mep_supp_packet_tx_free(tx_ltm);

    if (!rc) vtss_mep_supp_trace("ltm_to_forward: packet tx failed", 0, 0, 0, 0);
}

static void dmr_to_peer(u32 instance,  supp_instance_data_t *data,  dm_state_t *state,  mesa_timestamp_t *rx_time,  u8 smac[],  u8 dmm[], u32 dmm_len,  vtss_mep_supp_frame_info_t *frame_info,  u32 port,  BOOL is_mpls)
{
    u32                           rc = 0;
    mesa_timestamp_t              timestamp;
    u32                           tc=0, pcp;
    vtss_mep_supp_tx_frame_info_t tx_frame_info;
    BOOL                          init, dei;

    /* Allocate DMR TX buffer if not already allocated */
    if (!tx_reply_buff_alloc(data, dmm_len, &data->tx_dmr, &data->tx_dmr_buffer_size, &init))
        return;
    if (init)
        (void)init_tx_frame(instance, smac, data->config.mac, 0, 0, data->tx_dmr, OAM_TYPE_DMR);

    /* Generate DMR frame */
    if (is_mpls) {
        (void)init_tx_frame(instance, smac, data->config.mac, frame_info->pcp, 0, data->tx_dmr, OAM_TYPE_DMR);
    } else {
        /* DMAC */
        memcpy(data->tx_dmr, smac, VTSS_MEP_SUPP_MAC_LENGTH);
        /* PCP and DEI */
        reply_pcp_dei_calc(data, frame_info, &pcp, &dei);
        insert_reply_prio(data, data->tx_dmr, pcp, dei, frame_info->pcp);
    }
    /* PDU */
    memcpy(&data->tx_dmr[data->tx_header_size], dmm, dmm_len);
    data->tx_dmr[data->tx_header_size+1] = OAM_TYPE_DMR;
    if (is_mpls && c_s_tag(&data->tx_dmr[12])) {
        data->tx_dmr[12+2] = (data->tx_dmr[12+2] & 0x0F) | (frame_info->pcp << 5) | (frame_info->dei << 4);
    }

    /* RxTimeStampf */
    var_to_string(&data->tx_dmr[data->tx_header_size+12], rx_time->seconds);
    var_to_string(&data->tx_dmr[data->tx_header_size+16], rx_time->nanoseconds);

    /* TxTimeStampb */
    vtss_mep_supp_timestamp_get(&timestamp, &tc);
    var_to_string(&data->tx_dmr[data->tx_header_size+20], timestamp.seconds);
    var_to_string(&data->tx_dmr[data->tx_header_size+24], timestamp.nanoseconds);

    tx_frame_info.bypass = !evc_vlan_up(data);
    tx_frame_info.maskerade_port = (evc_vlan_up(data)) ? data->config.port : VTSS_PORT_NO_NONE;
    tx_frame_info.isdx = data->tx_isdx;
    tx_frame_info.vid = data->config.flow.vid;
    tx_frame_info.vid_inj = vlan_down(data);
    tx_frame_info.qos = frame_info->qos;
    tx_frame_info.pcp = frame_info->pcp;
    tx_frame_info.dp = frame_info->dei;

    BOOL is_vtss = memcmp(&dmm[DMM_PDU_STD_LENGTH], "VTSS", 4) == 0;
    if (!mep_caps.mesa_mep_serval && is_vtss) {
        // Two-step, i.e. we need to send a follow-up packet.

        data->dmr_fup_port = port;

        if (!vtss_mep_supp_check_hw_timestamp()) {
            // No H/W timestamp. Tx DMR immediately followed by DMR-follow-up
            // time-stamped with the time we got from the first Tx.
            mesa_timestamp_t tx_timestamp;
            rc = vtss_mep_supp_packet_tx_1(instance, &tx_timestamp, port, (data->tx_header_size + dmm_len), data->tx_dmr, &tx_frame_info, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_DMR : VTSS_MEP_SUPP_OAM_TYPE_NONE));
            if (rc) {
                dmr_fup_tx(instance, &tx_timestamp);
            }
        } else {
            // H/W timestamp. Tx DM and wait for timestamp of that to come
            // back before sending DMR-follow-up.
            data->ts_done_dmr.instance = instance;
            data->ts_done_dmr.cb       = dmr_fup_done;
            rc = vtss_mep_supp_packet_tx_1_two_step_ts(instance, &data->ts_done_dmr, port, (data->tx_header_size + dmm_len), data->tx_dmr);
        }
    } else {
        if (!vtss_mep_supp_check_hw_timestamp()) {
            /* no h/w timestamp */
            rc = vtss_mep_supp_packet_tx_1(instance, NULL, port, (data->tx_header_size + dmm_len), data->tx_dmr, &tx_frame_info, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_DMR : VTSS_MEP_SUPP_OAM_TYPE_NONE));
        }
        else {
            /* one step */
#if DEBUG_PHY_TS
            u32 zero = 0;
            var_to_string(&data->tx_dmr[data->tx_header_size+20], zero);
            var_to_string(&data->tx_dmr[data->tx_header_size+24], zero);
            rc = vtss_mep_supp_packet_tx_1(instance, NULL, port, (data->tx_header_size + dmm_len), data->tx_dmr, &tx_frame_info, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_DMR : VTSS_MEP_SUPP_OAM_TYPE_NONE));
#else
            rc = vtss_mep_supp_packet_tx_1_one_step_ts(instance, port, (data->tx_header_size + dmm_len), data->tx_dmr, &tx_frame_info, tc, &data->onestep_extra_r, ((data->config.voe) ? VTSS_MEP_SUPP_OAM_TYPE_DMR : VTSS_MEP_SUPP_OAM_TYPE_NONE));
#endif
        }
    }
    if (!rc)       vtss_mep_supp_trace("dmr_to_peer: packet tx failed", 0, 0, 0, 0);
}

static BOOL find_lowest_mep(u32 port, vtss_mep_supp_direction_t direct, u16 *meps, u32 meps_idx, u32 *mep)
{
    u32  mep_level, i;
    BOOL found = FALSE;

    mep_level = 8;
    for (i=0; i<meps_idx; ++i)      /* Find the MEP with the lowest level on this port */
        if ((instance_data[meps[i]].config.port == port) && (instance_data[meps[i]].config.direction == direct) && (instance_data[meps[i]].config.level < mep_level))
        {
            mep_level = instance_data[meps[i]].config.level;
            *mep = meps[i];
            found = TRUE;
        }
    return(found);
}

static u32 reply_port_calc(supp_instance_data_t *data,  u32 port)
{
    u32 i;

    if (!data->config.flow.port[port])      /* A PDU is received on a port that is not a flow port - this is possible in case of Up-MEP according to DS1076 */
    {
        for (i=0; i<mep_caps.mesa_port_cnt; ++i)  if (data->config.flow.port[i])  break;     /* This is assuming that this Up-Mep has one flow port only */
        return(i);
    }
    else
        return (port);
}

static void extract_tlv_info(u8 pdu[], supp_instance_data_t *data,  BOOL *new_is)
{
    u32   i, mep, off = CCM_PDU_TLV_OFFSET, max_loop=3;
    BOOL  is_received;
    u8    is_tlv_value;

    mep = (pdu[8]<<8) + pdu[9];     /* Find the peer MEP */

    for (i=0; i<data->ccm_config.peer_count; ++i)
        if (data->ccm_config.peer_mep[i] == mep)
            break;

    if (i == data->ccm_config.peer_count)   /* Check for known peer MEP */
        return;

    is_received = data->ccm_state.state.is_received[i];
    is_tlv_value = data->ccm_state.state.is_tlv[i].value;

    data->ccm_state.state.ps_received[i] = FALSE;
    data->ccm_state.state.is_received[i] = FALSE;
    data->ccm_state.state.os_received[i] = FALSE;

    if (pdu[off] != 0) {  /* Check TLV present */
        while (max_loop && (pdu[off] != 0)) {
            switch (pdu[off]) {
                case 2:         /* Port TLV */
                    data->ccm_state.state.ps_received[i] = TRUE;
                    data->ccm_state.state.ps_tlv[i].value = pdu[off+3];
                    off += 4;
                    break;
                case 4:         /* Interface TLV */
                    data->ccm_state.state.is_received[i] = TRUE;
                    data->ccm_state.state.is_tlv[i].value = pdu[off+3];
                    off += 4;
                    break;
                case 31:        /* Organization Specific TLV */
                    data->ccm_state.state.os_received[i] = TRUE;
                    memcpy(data->ccm_state.state.os_tlv[i].oui, &pdu[off+3], VTSS_MEP_SUPP_OUI_SIZE);
                    data->ccm_state.state.os_tlv[i].subtype = pdu[off+3+VTSS_MEP_SUPP_OUI_SIZE];
                    data->ccm_state.state.os_tlv[i].value = pdu[off+3+VTSS_MEP_SUPP_OUI_SIZE+1];
                    off += 3 + VTSS_MEP_SUPP_OUI_SIZE + 2;
                    break;
            }
            max_loop--;
        }
    }

    if ((data->ccm_state.state.is_received[i] != is_received) || (data->ccm_state.state.is_tlv[i].value != is_tlv_value))
        *new_is = TRUE;
}

static BOOL lm_reception_expected(supp_instance_data_t *data)
{
    return (((data->slm_config.ended == VTSS_MEP_SUPP_SINGLE_ENDED) && data->slm_config.enable) ||
            (data->lmm_config.enable) ||
            (data->ccm_gen.enable && data->ccm_gen.lm_enable)) ? TRUE : FALSE;
}























































































































































































static BOOL rx_isdx_check(supp_instance_data_t *data, u32 isdx)
{
    if (data->rx_isdx[0] == isdx)      return TRUE;
    if (!data->config.sat)             return FALSE;
    for (u32 i=1; i<mep_caps.appl_mep_rx_isdx_size; ++i) {
        if (data->rx_isdx[i] == isdx)  return TRUE;
    }
    return FALSE;
}

void vtss_mep_supp_rx_frame(u32 port,  vtss_mep_supp_frame_info_t *frame_info,  u8 frame[],  u32 len, mesa_timestamp_t *rx_time, u32 rx_count)
{
/* This is called by API when a OAM frame is received */
    supp_instance_data_t *data = NULL;
    vtss_mep_supp_ltr_t  *ltr;
    dm_state_t           *state;
    u8                   *inner_tag = NULL;
    BOOL                 check_lm, new_lbr, new_dmr, new_ltr, new_aps, new_dm1, new_defect, new_state, new_tst, new_mac, new_is;
    u8                   *pdu;
    u32                  pdu_length;
    u32                  mep_lm, near_CT2, near_CR2, far_CT2, far_CR2, priority;
    bool                 has_near_rx_count = TRUE;
    u32                  tag_idx, tl_idx, tag_cnt, f_port;
    u32                  level, meps_idx, mips_idx, mep_idx, i, j, tx_count, tst_frame_size, rx_tag_cnt, peer_idx, peer_mep;
    u32                  rx_count_read = 0, tx_count_read = 0;
    i32                  dm_delay, total_delay, device_delay;
    mesa_timestamp_t     rxTimef, txTimeb;
    mesa_port_list_t     egress_ports;
    BOOL                 is_ns;
    BOOL                 is_mpls = FALSE;






    u32                  mepid = 0xFFFFFFFF;
    u32                  mep_mpls = 0xFFFFFFFF;




    #define MEPS_MIPS_MAX      30
    u16                  meps[MEPS_MIPS_MAX], mipss[MEPS_MIPS_MAX];
    u16                  mep[MEPS_MIPS_MAX];

    peer_idx = 0;

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    new_lbr = new_ltr = new_aps = new_dmr = new_dm1 = new_defect = new_state = new_tst = new_mac = new_is = FALSE;
    pdu = frame;
    tst_frame_size = 0;
    priority = (frame_info->tagged) ? frame_info->pcp : 0xFFFFFFFF;
    memset(&rxTimef, 0, sizeof(rxTimef));
    memset(&txTimeb, 0, sizeof(txTimeb));
    meps_idx = mips_idx = mep_idx = 0;
    memset(meps, 0, sizeof(meps));
    memset(mipss, 0, sizeof(mipss));
    memset(mep, 0, sizeof(mipss));
    egress_ports.set_all();


    /* Calculate number of tags */
    tag_idx = 12;
    tag_cnt = 0;








    if ((frame[tag_idx] != 0x89) || (frame[tag_idx+1] != 0x02)) {
        tag_cnt += 1;
    }


    /* Check for type of frame - has to be OAM */
    tl_idx = tag_idx+(tag_cnt*4);



    if (((frame[tl_idx] != 0x89) || (frame[tl_idx+1] != 0x02)) && !is_mpls)   goto unlock;






























    if ((tl_idx + 2) >= len)
        goto unlock;
    pdu = &frame[tl_idx + 2];
    pdu_length = len - (tl_idx + 2);
    level = pdu[0]>>5;
    rx_tag_cnt = tag_cnt + ((frame_info->tagged) ? 1 : 0);

//printf("vtss_mep_supp_rx_frame  port %u  vid %u  isdx %u  tl_idx %u  level %u  rx_tag_cnt %u  pdu[1+8+9] %X-%X-%X  sec %u sec %u nano %u\n", port, frame_info->vid, frame_info->isdx, tl_idx, level, rx_tag_cnt, pdu[1], pdu[8], pdu[9], rx_time->sec_msb, rx_time->seconds, rx_time->nanoseconds);















    if (mep_mpls != 0xFFFFFFFF) { /* MPLS special case */
        mep_mpls--;
        meps[meps_idx++] = mep_mpls;
        data = &instance_data[mep_mpls];
        mep[mep_idx++] = mep_mpls;
    } else {
        /* Find all MEP/MIP in this flow */
        for (i=0; i<VTSS_MEP_SUPP_CREATED_MAX; ++i) {
            /* Find the MEP with the received frame header as expected */
            data = &instance_data[i];    /* Instance data reference */
            /* Check if this MEP/MIP is configured with this expected header */
            if (!data->config.enable)                                                                  continue;         /* Check if enabled */













            if (mep_caps.mesa_mep_serval) {
                if (data->config.voe && rx_isdx_check(data, frame_info->isdx))                             mep[mep_idx++] = i;   /* VOE based MEP is found */
            }
            if (rx_tag_cnt != rx_tag_cnt_calc(data, priority))                                         continue;         /* Check number of tags */
            if (data->config.level < level)                                                            continue;         /* Check level */
            if ((data->config.flow.vid != frame_info->vid) &&
                ((data->config.evc_type != VTSS_MEP_SUPP_ROOT) || (data->config.flow.leaf_vid != frame_info->vid)))  continue;  /* Check classified VID */
            if (mep_caps.mesa_mep_serval) {
                if ((data->config.mode == VTSS_MEP_SUPP_MIP) && (data->config.flow.port[port]))            mep[mep_idx++] = i;   /* Subscriber MIP is found */
            }
            if ((data->config.mode == VTSS_MEP_SUPP_MEP) && (meps_idx < MEPS_MIPS_MAX))                                    meps[meps_idx++] = i;  /* All MEPs are saved for later analysis */
            if ((data->config.mode == VTSS_MEP_SUPP_MIP) && (data->config.flow.port[port]) && (mips_idx < MEPS_MIPS_MAX))  mipss[mips_idx++] = i; /* All MIPs are saved for later analysis */
        }
    }

    /* Check for discard */
    if ((mips_idx == 0) && (meps_idx == 0) && (mep_idx == 0))   goto unlock;     /* No MEP or MIP was found */
    if (mep_idx == 0) { /* this frame is not related to a VOE based MEP or a Serval Subscriber MIP */
        if (pdu[1] != OAM_TYPE_CCM) {/* This is not a CCM - check for discard */
            for (i=0; i<meps_idx; ++i)  /* search for Ingress MEP on this level on this port */
                if ((instance_data[meps[i]].config.direction == VTSS_MEP_SUPP_DOWN) && (instance_data[meps[i]].config.port == port) && (instance_data[meps[i]].config.level == level))
                    {mep[mep_idx++] = meps[i]; break;}
            if (mep_idx == 0) {/* Ingress MEP on this level was not found - search for ingress/egress MEP on this port,level or higher to discard */
                for (i=0; i<meps_idx; ++i)    if ((instance_data[meps[i]].config.port == port) && (instance_data[meps[i]].config.level >= level))    goto unlock;
                if (pdu[1] != OAM_TYPE_LTM) {/* This is not a LTM - find target MEP/MIP */
                    if ((pdu[1] == OAM_TYPE_LBM) && !(frame[0] & 0x01)) {/* This is a unicast LBM PDU - search for MIP with this MAC */
                        for (i=0; i<mips_idx; ++i)
                            if ((instance_data[mipss[i]].config.level == level) && !memcmp(&frame[0], instance_data[mipss[i]].config.mac, VTSS_MEP_SUPP_MAC_LENGTH))   {mep[mep_idx++] = mipss[i]; break;}
                    }
                    if (mep_idx == 0) {/* No MEP/MIP was found - search for egress MEP on this level */
                        for (i=0; i<meps_idx; ++i)
                            if ((instance_data[meps[i]].config.direction == VTSS_MEP_SUPP_UP) && (instance_data[meps[i]].config.level == level) &&
                                 ((frame[0] & 0x01) || !memcmp(&frame[0], instance_data[meps[i]].config.mac, VTSS_MEP_SUPP_MAC_LENGTH)))
                                {mep[mep_idx++] = meps[i];}
                    }
                    if (mep_idx == 0)  goto unlock;
                }
            }
        }
    } else { /* This frame was related to a VOE based MEP (or is MPLS) */
        if (mep_caps.mesa_mep_serval) {
            if (mips_idx)   /* If Subscriber MIP is found then any MEP found is not relevant */
                meps_idx = 0;
        }
    }

    /* No reason was found to discard this PDU - analyze */







































































    /* Ethernet OAM or G.8113.1 MPLS OAM: */
    switch (pdu[1])
    {
        case OAM_TYPE_CCM:
            if (meps_idx == 0)     goto unlock;
            check_lm = FALSE;
            mep_lm = 0;

            if (mep_idx != 0)   /* Check if MEP has already been found */
                mepid = mep[--mep_idx];

            if ((mepid != 0xFFFFFFFF) || find_lowest_mep(port, VTSS_MEP_SUPP_DOWN, meps, meps_idx, &mepid)) { /* Either a VOE based MEP was found or the Down-MEP with the lowest level on this port was found */
                mep[mep_idx++] = mepid;
                check_lm = !ccm_defect_calc(mepid, &instance_data[mepid], &frame[VTSS_MEP_SUPP_MAC_LENGTH], pdu, priority, &new_defect, &new_state, &new_mac);
                if (check_lm)   extract_tlv_info(pdu, &instance_data[mepid], &new_is);
                mep_lm = mepid;
                if (check_lm)   instance_data[mepid].ccm_state.state.valid_counter++;
                else            instance_data[mepid].ccm_state.state.invalid_counter++;
            } else { /* No Down-MEP was found on this port */
                if (frame_info->discard) {
                    vtss_mep_discard_trace("Discard CCM to not Down-MEP  port  vid", port, frame_info->vid, 0, 0);
                    goto unlock;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
                }
                for (i=0; i<meps_idx; ++i)    if ((instance_data[meps[i]].config.port == port) && (instance_data[meps[i]].config.level >= level))    goto unlock; /* Discard if egress MEP on this level or higher on this port */
                egress_ports[port] = FALSE;     /* Don't check for egress on this port */
                for (i=0; i<meps_idx; ++i) { /* Find all Egress MEP on this level or higher */
                    if ((instance_data[meps[i]].config.direction == VTSS_MEP_SUPP_UP) && egress_ports[instance_data[meps[i]].config.port]) { /* Egress on a port not already checked */
                        egress_ports[instance_data[meps[i]].config.port] = FALSE;   /* Now this port is checked */
                        if (find_lowest_mep(instance_data[meps[i]].config.port, VTSS_MEP_SUPP_UP, meps, meps_idx, &mepid)) { /* The egress MEP with the lowest level on this port was found */
                            mep[mep_idx++] = mepid;
                            if (!ccm_defect_calc(mepid, &instance_data[mepid], &frame[VTSS_MEP_SUPP_MAC_LENGTH], pdu, priority, &new_defect, &new_state, &new_mac)) {
                                extract_tlv_info(pdu, &instance_data[mepid], &new_is);
                                check_lm = TRUE;  /* No defects and on right level - check for LM */
                                mep_lm = mepid;
                                instance_data[mepid].ccm_state.state.valid_counter++;
                            } else
                                instance_data[mepid].ccm_state.state.invalid_counter++;
                        }
                    }
                }
            }
            if (!check_lm)      break;
            if (!instance_data[mep_lm].config.enable)                    break;
            if (!instance_data[mep_lm].ccm_gen.enable || !instance_data[mep_lm].ccm_gen.lm_enable)        break;  /* CCM based LM is not enabled */
            if (instance_data[mep_lm].tx_ccm_sw == NULL)                 break;
            if (!memcmp(lm_null_counter, &pdu[58], 12))                  break;  /* No counters in this CCM */

            instance_data[mep_lm].lm_state[0].lm_counter.rx_msg_counter++;

            if (instance_data[mep_lm].config.voe) { /* This is a VOE based instance */
                if (mep_caps.mesa_mep_serval) {
                    mesa_oam_voe_counter_t     voe_counter;
                    (void)mesa_oam_voe_counter_get(NULL, instance_data[mep_lm].voe_idx, &voe_counter);
                    if (instance_data[mep_lm].config.direction == VTSS_MEP_SUPP_UP) { /* In case of UP-MEP the RX frame counter is fetched in the VOE */
                        if (frame_info->oam_info>>27 != voe_counter.lm.up_mep.rx_ccm_lm_sample_seq_no) {
                            vtss_mep_supp_trace("vtss_mep_supp_rx_frame CCM: Unexpected CCM LM sequence number", mep_lm, frame_info->oam_info>>27, voe_counter.lm.up_mep.rx_ccm_lm_sample_seq_no, 0);
                            break;
                        }
                        rx_count = voe_counter.lm.up_mep.ccm_lm;

                    } else  {
                        // if VOE-based down-MEP we have gotten the value for rx_count in the function call
                    }

                    instance_data[mep_lm].lm_state[0].lm_counter.tx_msg_counter = voe_counter.sel.selected_frames.tx;
                }
            } else {
                // Software-based (non-VOE) MEP
                vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);    /* Get RX frame counter from platform */
                has_near_rx_count = vtss_mep_supp_counters_get(mep_lm, &rx_count, &tx_count);
                vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
                // The supp_loc_crit has been unlocked and locked again, and this mep might have been disabled/deleted or
                // changed state during the unlock, so we have to check status again.
                if (!instance_data[mep_lm].config.enable)                    break;
                if (!instance_data[mep_lm].ccm_gen.enable || !instance_data[mep_lm].ccm_gen.lm_enable)        break;  /* CCM based LM is not enabled */
                if (instance_data[mep_lm].tx_ccm_sw == NULL)                 break;
            }

            if (instance_data[mep_lm].tx_ccm_sw) {
                var_to_string(&instance_data[mep_lm].tx_ccm_sw[instance_data[mep_lm].tx_header_size + 62], rx_count);   /* Rx counter back to far end */
                memcpy(&instance_data[mep_lm].tx_ccm_sw[instance_data[mep_lm].tx_header_size + 66], &pdu[58], 4);        /* far end TX counter back to far end */
            }

            near_CT2 = string_to_var(&pdu[66]);          /* Calculate LM information */
            far_CT2 = string_to_var(&pdu[58]);
            near_CR2 = rx_count;
            far_CR2 = string_to_var(&pdu[62]);

            lm_counter_calc(mep_lm, 0, &(instance_data[mep_lm].lm_state[0].lm_counter),
                            &(instance_data[mep_lm].lm_start[0]),
                            near_CT2, far_CT2, near_CR2, far_CR2, has_near_rx_count);   /* Calculate LM counters */
            break;

        case OAM_TYPE_LMM:
            for (i=0; i<mep_idx; ++i) {
                mepid = mep[i];
                data = &instance_data[mepid];    /* Instance data reference */
                if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                    vtss_mep_discard_trace("Discard LMM to not Down-MEP  instance  port  vid", mepid, port, frame_info->vid, 0);
                    continue;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
                }
                if (level != data->config.level)          continue;  /* Wrong level */
                lmr_to_peer(mepid, data, &frame[VTSS_MEP_SUPP_MAC_LENGTH], pdu, len-(tl_idx + 2), frame_info, reply_port_calc(data, port), is_mpls, priority, &rx_count);

                if (!data->lmm_config.enable_rx)          continue;  /* LM reception is is not enabled */
                if (lm_reception_expected(data))          continue;  /* LM reception is expected - do not calculate far-to-near */

                data->lm_state[0].lm_counter.rx_msg_counter++;   /* Calculate far-to-near based on received LMM */

                near_CT2 = 0;          /* Calculate LM information */
                far_CT2 = string_to_var(&pdu[4]);
                near_CR2 = rx_count;
                far_CR2 = 0;
                lm_counter_calc(mepid, 0, &data->lm_state[0].lm_counter,  &data->lm_start[0],
                        near_CT2,  far_CT2,  near_CR2,  far_CR2, has_near_rx_count);   /* Calculate LM counters */
            }
            break;

        case OAM_TYPE_LMR:
            mepid = mep[mep_idx-1];
            vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
            vtss_mep_supp_counters_get(mepid, &rx_count_read, &tx_count_read);
            vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
            data = &instance_data[mepid];    /* Instance data reference. LMR is UC so only one instance can be relevant */
            if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                vtss_mep_discard_trace("Discard LMR to not Down-MEP  instance  port  vid", mepid, port, frame_info->vid, 0);
                break;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
            }
            if (!data->config.enable)                 break;
            if (level != data->config.level)          break;  /* Wrong level */
            if (!data->lmm_config.enable)             break;  /* LMM is not enabled */

            data->lm_state[0].lm_counter.rx_rsp_counter++;
            if (!data->config.voe) { /* This is not a VOE based instance */
                // Use the SW-based counters read
                rx_count = rx_count_read;
                tx_count = tx_count_read;
            }
            else { /* This is a VOE based instance */
              // For VOE-based MEPs use the rx_count originally given in the call unless for Up-MEPs on Serval ...
                mesa_oam_voe_counter_t     voe_counter;
                if (data->config.direction == VTSS_MEP_SUPP_UP) { /* In case of UP-MEP the RX frame counter is fetched in the VOE */
                    (void)mesa_oam_voe_counter_get(NULL, data->voe_idx, &voe_counter);
                    if (frame_info->oam_info>>27 != voe_counter.lm.up_mep.rx_lmr_sample_seq_no) {
                        vtss_mep_supp_trace("vtss_mep_supp_rx_frame LMR: Unexpected LMR LM sequence number", mep[mep_idx-1], frame_info->oam_info>>27, voe_counter.lm.up_mep.rx_lmr_sample_seq_no, 0);
                        break;
                    }
                    rx_count = voe_counter.lm.up_mep.lmr;
                }
            }

            near_CT2 = string_to_var(&pdu[4]);          /* Calculate LM information */
            far_CT2 = string_to_var(&pdu[12]);
            near_CR2 = rx_count;
            far_CR2 = string_to_var(&pdu[8]);
            lm_counter_calc(mepid, 0, &data->lm_state[0].lm_counter, &data->lm_start[0],
                            near_CT2,  far_CT2,  near_CR2,  far_CR2, has_near_rx_count);   /* Calculate LM counters */
            break;

        case OAM_TYPE_SLM:
            peer_mep = (pdu[4]<<8) + pdu[5];

            for (i=0; i<mep_idx; ++i) {
                mepid = mep[i];
                vtss_mep_supp_trace("RX SLM PDU", mepid, peer_mep, level, 0);

                data = &instance_data[mepid];    /* Instance data reference */
                if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                    vtss_mep_discard_trace("Discard SLM to not Down-MEP  instance  port  vid", mepid, port, frame_info->vid, 0);
                    continue;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
                }
                if (level != data->config.level)          continue;  /* Wrong level */

                for (peer_idx=0; peer_idx<data->ccm_config.peer_count; ++peer_idx) {
                    if (data->ccm_config.peer_mep[peer_idx] == peer_mep)    break;
                }
                if (peer_idx == data->ccm_config.peer_count)   continue;

                data->lm_state[peer_idx].sl_service_counter.rx_slm_counter++;
                slr_to_peer(mepid, data, peer_idx, &frame[VTSS_MEP_SUPP_MAC_LENGTH], pdu, len-(tl_idx + 2), frame_info, reply_port_calc(data, port), &rx_count);

                if (!data->slm_config.enable_rx)            continue;  /* LM reception is not enabled */
                if (lm_reception_expected(data))            continue;  /* LM reception is expected - do not calculate far-to-near */

                data->lm_state[peer_idx].lm_counter.rx_msg_counter++;   /* Calculate far-to-near based on received SLM */

                far_CT2 = string_to_var(&pdu[12]);
                if ((far_CT2 - data->lm_state[peer_idx].meas_interval_tx_counter) >= data->slm_config.meas_interval) {  /* Measurement interval elapsed */
                    data->lm_state[peer_idx].meas_interval_tx_counter = far_CT2;
                    near_CT2 = 0;          /* Calculate LM information */
                    near_CR2 = data->lm_state[peer_idx].sl_service_counter.rx_slm_counter;
                    far_CR2 = 0;
                    lm_counter_calc(mepid, peer_idx,
                            &data->lm_state[peer_idx].lm_counter,  &data->lm_start[peer_idx],
                            near_CT2, far_CT2, near_CR2, far_CR2, TRUE);   /* Calculate LM counters */
                }
            }
            break;

        case OAM_TYPE_SLR:
            mepid = mep[mep_idx-1];
            data = &instance_data[mepid];    /* Instance data reference. SLR is UC so only one instance can be relevant */
            if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                vtss_mep_discard_trace("Discard SLR to not Down-MEP  instance  port  vid", mepid, port, frame_info->vid, 0);
                break;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
            }
            if (level != data->config.level)          break;  /* Wrong level */

            peer_mep = (pdu[6]<<8) + pdu[7];
            for (peer_idx=0; peer_idx<data->ccm_config.peer_count; ++peer_idx) {
                if (data->ccm_config.peer_mep[peer_idx] == peer_mep)    break;
            }
            if (peer_idx == data->ccm_config.peer_count)   goto unlock;;

            data->lm_state[peer_idx].lm_counter.rx_rsp_counter++;
            data->lm_state[peer_idx].sl_service_counter.rx_slr_counter++;

            far_CT2 = string_to_var(&pdu[16]);
            if ((far_CT2 - data->lm_state[peer_idx].meas_interval_tx_counter) >= data->slm_config.meas_interval) {  /* Measurement interval elapsed */
                data->lm_state[peer_idx].meas_interval_tx_counter = far_CT2;
                near_CT2 = string_to_var(&pdu[12]);          /* Calculate LM information */
                near_CR2 = data->lm_state[peer_idx].sl_service_counter.rx_slr_counter;
                far_CR2 = string_to_var(&pdu[16]);
                lm_counter_calc(mepid, peer_idx,
                        &data->lm_state[peer_idx].lm_counter, &data->lm_start[peer_idx],
                        near_CT2, far_CT2, near_CR2, far_CR2, TRUE);   /* Calculate LM counters */
            }
            break;

        case OAM_TYPE_1SL:
            peer_mep = (pdu[4]<<8) + pdu[5];

            for (i=0; i<mep_idx; ++i) {
                mepid = mep[i];
                data = &instance_data[mepid];    /* Instance data reference */
                if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                    vtss_mep_discard_trace("Discard 1SL to not Down-MEP  instance  port  vid", mepid, port, frame_info->vid, 0);
                    continue;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
                }
                if (level != data->config.level)          continue;  /* Wrong level */
                if (!data->slm_config.enable_rx)          continue;  /* LM reception is is not enabled */
                if (lm_reception_expected(data))          continue;  /* LM reception is expected - do not calculate far-to-near */

                for (peer_idx=0; peer_idx<data->ccm_config.peer_count; ++peer_idx) {
                    if (data->ccm_config.peer_mep[peer_idx] == peer_mep)    break;
                }

                if (peer_idx == data->ccm_config.peer_count)   continue;    // no peer found

                data->lm_state[peer_idx].lm_counter.rx_msg_counter++;
                data->lm_state[peer_idx].sl_service_counter.rx_slr_counter++;

                far_CT2 = string_to_var(&pdu[12]);
                if ((far_CT2 - data->lm_state[peer_idx].meas_interval_tx_counter) >= data->slm_config.meas_interval) {  /* Measurement interval elapsed */
                    data->lm_state[peer_idx].meas_interval_tx_counter = far_CT2;
                    near_CT2 = 0;          /* Calculate LM information */
                    near_CR2 = data->lm_state[peer_idx].sl_service_counter.rx_slr_counter;
                    far_CR2 = 0;
                    lm_counter_calc(mepid, peer_idx,
                            &data->lm_state[peer_idx].lm_counter, &data->lm_start[peer_idx],
                            near_CT2, far_CT2, near_CR2, far_CR2, TRUE);   /* Calculate LM counters */
                }
            }
            break;

        case OAM_TYPE_LAPS:
        case OAM_TYPE_RAPS:
            data = &instance_data[mep[mep_idx-1]];    /* Instance data reference */
            if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                vtss_mep_discard_trace("Discard LAPS or RAPS to not Down-MEP  instance  port  vid", mep[mep_idx-1], port, frame_info->vid, 0);
                break;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
            }
            if (level != data->config.level)          break;  /* Wrong level */
            if (!data->aps_config.enable)             break;  /* APS is not enabled */

            data->rx_aps_timer = data->aps_timer_val*3;

            new_aps = TRUE;
            break;

        case OAM_TYPE_LTM:
            if (is_mpls)                              break;  /* not supported for MPLS */
            if (pdu[8] == 0)                          break;  /* TTL is "dead" */
            if (pdu[21] != 7)                         break;  /* No Egress identifier present */

            if (rx_tag_cnt == 2) { /* This is double tagged LTM so replys must be done with preserved inner tag */
                inner_tag = &frame[tl_idx - 4];
            }

            if (meps_idx != 0)
            {   /* One or more MEP's got this expected frame header */
                for (i=0; i<meps_idx; ++i)
                    if ((instance_data[meps[i]].config.level == level) && (instance_data[meps[i]].config.port == port) && (instance_data[meps[i]].config.direction == VTSS_MEP_SUPP_DOWN))
                        break;
                if (i < meps_idx)
                {/* Ingress MEP on this level,port was found */
                    instance_data[meps[i]].is2_rx_cnt++;
                    if (!memcmp(&pdu[15], instance_data[meps[i]].config.mac, VTSS_MEP_SUPP_MAC_LENGTH)) {/* Reply if this is the target MAC */
                        ltr_to_peer(meps[i], &instance_data[meps[i]], pdu, inner_tag, frame_info, reply_port_calc(&instance_data[meps[i]], port), FALSE, TRUE, 1);
                        break;
                    }
                    if (vtss_mep_supp_check_forwarding(port, &f_port, &pdu[15], frame_info->vid)) {/* Reply if forwarding is possible */
                        ltr_to_peer(meps[i], &instance_data[meps[i]], pdu, inner_tag, frame_info, reply_port_calc(&instance_data[meps[i]], port), FALSE, TRUE, 2);
                        break;
                    }
                    break;
                }
                if (frame_info->discard) {
                    vtss_mep_discard_trace("Discard LTM to not Down-MEP  port  vid", port, frame_info->vid, 0, 0);
                    break;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
                }
                for (i=0; i<meps_idx; ++i) {
                    if ((instance_data[meps[i]].config.level == level) && (instance_data[meps[i]].config.direction == VTSS_MEP_SUPP_UP)) {
                        /* Egress MEP on this level was found */
                        instance_data[meps[i]].is2_rx_cnt++;
                        if (!memcmp(&pdu[15], instance_data[meps[i]].config.mac, VTSS_MEP_SUPP_MAC_LENGTH)) {/* Reply if this is the target MAC */
                            ltr_to_peer(meps[i], &instance_data[meps[i]], pdu, inner_tag, frame_info, reply_port_calc(&instance_data[meps[i]], port), FALSE, TRUE, 1);
                            goto unlock;
                        }
                        if (vtss_mep_supp_check_forwarding(port, &f_port, &pdu[15], frame_info->vid)) { /* Reply if forwarding is possible on residence port */
                            if (instance_data[meps[i]].config.port == f_port) {
                                ltr_to_peer(meps[i], &instance_data[meps[i]], pdu, inner_tag, frame_info, reply_port_calc(&instance_data[meps[i]], port), FALSE, TRUE, 2);
                                goto unlock;
                            }
                        }
                    }
                }
                if (vtss_mep_supp_check_forwarding(port, &f_port, &pdu[15], frame_info->vid))
                    for (i=0; i<meps_idx; ++i)  /* Search for ingress/egress MEP on this level or higher, on forwarding port - must not leak */
                        if ((instance_data[meps[i]].config.port == f_port) && (instance_data[meps[i]].config.level >= level))
                            goto unlock;
            }

            if (frame_info->discard) {
                vtss_mep_discard_trace("Discard LTM to not Down-MEP  port  vid", port, frame_info->vid, 0, 0);
                break;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
            }

            /* No terminating ingress/egress MEP was found */
            if (mips_idx != 0)
            {   /* One or more MIP's got this expected frame header */
                for (i=0; i<mips_idx; ++i) /* Check for MIP with target MAC */
                    if ((instance_data[mipss[i]].config.level == level) && !memcmp(&pdu[15], instance_data[mipss[i]].config.mac, VTSS_MEP_SUPP_MAC_LENGTH)) {/* Reply if this is the target MAC */
                        instance_data[mipss[i]].is2_rx_cnt++;
                        ltr_to_peer(mipss[i], &instance_data[mipss[i]], pdu, inner_tag, frame_info, port, FALSE, FALSE, 1);
                        break;
                    }
                if (i < mips_idx) break;  /* MIP with target MAC was found - done */

                /* No MIP with target MAC was found */
                if (vtss_mep_supp_check_forwarding(port, &f_port, &pdu[15], frame_info->vid))
                {/* Forwarding is possible - means that we will reply but not necessarily forward (depending on TTL) */
                    for (i=0; i<mips_idx; ++i)    /* Check if an egress MIP on forwarding port exists */
                        if ((instance_data[mipss[i]].config.level == level) && (instance_data[mipss[i]].config.direction == VTSS_MEP_SUPP_UP) && (instance_data[mipss[i]].config.port == f_port))
                            break;
                    if (i < mips_idx)
                    {/* Egress MIP was found on forwarding port */
                        instance_data[mipss[i]].is2_rx_cnt++;
                        ltr_to_peer(mipss[i], &instance_data[mipss[i]], pdu, inner_tag, frame_info, port, (pdu[8] > 1), FALSE, 2);
                        if (pdu[8] > 1)    ltm_to_forward(mipss[i], &instance_data[mipss[i]], frame, pdu, frame_info, f_port);  /* Don't forwared with a 'zero' TTL */
                        break;
                    }
                    /* No egress MIP was found */
                    for (i=0; i<mips_idx; ++i)    /* Check if an ingress MIP exists */
                        if ((instance_data[mipss[i]].config.level == level) && instance_data[mipss[i]].config.direction == VTSS_MEP_SUPP_DOWN)
                            break;
                    if (i < mips_idx)
                    {/* Ingress MIP was found */
                        instance_data[mipss[i]].is2_rx_cnt++;
                        ltr_to_peer(mipss[i], &instance_data[mipss[i]], pdu, inner_tag, frame_info, port, (pdu[8] > 1), FALSE, 2);
                        if (pdu[8] > 1)
                            ltm_to_forward(mipss[i], &instance_data[mipss[i]], frame, pdu, frame_info, f_port);  /* Don't forwared with a 'zero' TTL */
                    }
                }
            }

            break;

        case OAM_TYPE_LTR:
            if (is_mpls)                          break;  /* not supported for MPLS */
            data = &instance_data[mep[mep_idx-1]];    /* Instance data reference */
            if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                vtss_mep_discard_trace("Discard LTR to not Down-MEP  instance  port  vid", mep[mep_idx-1], port, frame_info->vid, 0);
                break;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
            }
            if (level != data->config.level)      break;  /* Wrong level */
            if (!data->ltm_config.enable)         break;  /* LTM is not enabled */

            if (data->ltr_cnt < VTSS_MEP_SUPP_LTR_MAX)
            {
                ltr = &data->ltr[data->ltr_cnt];

                ltr->mode = (pdu[2] & 0x20) ? VTSS_MEP_SUPP_MEP : VTSS_MEP_SUPP_MIP;
                ltr->forwarded = (pdu[2] & 0x40);
                ltr->transaction_id = string_to_var(&pdu[4]);
                ltr->ttl = pdu[8];
                ltr->relay_action = (pdu[9] == 1) ? VTSS_MEP_SUPP_RELAY_HIT : (pdu[9] == 2) ? VTSS_MEP_SUPP_RELAY_FDB : (pdu[9] == 3) ? VTSS_MEP_SUPP_RELAY_MPDB : VTSS_MEP_SUPP_RELAY_UNKNOWN;
                memcpy(ltr->last_egress_mac, &pdu[15], VTSS_MEP_SUPP_MAC_LENGTH);
                memcpy(ltr->next_egress_mac, &pdu[23], VTSS_MEP_SUPP_MAC_LENGTH);
                ltr->direction = (pdu[29] == 5) ? VTSS_MEP_SUPP_DOWN : VTSS_MEP_SUPP_UP;

                data->ltr_cnt++;

                new_ltr = TRUE;
            }
            break;

        case OAM_TYPE_LBM:
            for (i=0; i<mep_idx; ++i) {
                data = &instance_data[mep[i]];    /* Instance data reference */
                if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                    vtss_mep_discard_trace("Discard LBM to not Down-MEP  instance  port  vid", mep[i], port, frame_info->vid, 0);
                    continue;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
                }
                if (level != data->config.level)      continue;  /* Wrong level */
                if (!is_mpls && !(frame[0] & 0x01) && memcmp(&frame[0], data->config.mac, VTSS_MEP_SUPP_MAC_LENGTH))      continue;  /* Wrong unicast MAC */

                data->is2_rx_cnt++;

                lbr_to_peer(mep[i], data, &frame[VTSS_MEP_SUPP_MAC_LENGTH], pdu, len-(tl_idx + 2), frame_info, reply_port_calc(data, port), is_mpls, priority);
            }
            break;

        case OAM_TYPE_LBR:
            data = &instance_data[mep[mep_idx-1]];    /* Instance data reference */
            if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                vtss_mep_discard_trace("Discard LBR to not Down-MEP  instance  port  vid", mep[mep_idx-1], port, frame_info->vid, 0);
                break;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
            }
            if (level != data->config.level)                       break;  /* Wrong level */
            if (!test_enable_calc(data->lbm_config.tests))         break;  /* LBM is not enabled */

            data->lb_state.lbr_counter++;
            if (data->lbr_cnt < VTSS_MEP_SUPP_LBR_MAX)
            {
                data->lbr[data->lbr_cnt].transaction_id = string_to_var(&pdu[4]);
                if (is_mpls) {
                    memcpy(data->lbr[data->lbr_cnt].u.mep_mip_id, &pdu[11], 16);
                } else {
                    memcpy(data->lbr[data->lbr_cnt].u.mac, &frame[VTSS_MEP_SUPP_MAC_LENGTH], VTSS_MEP_SUPP_MAC_LENGTH);
                }
                data->lbr_cnt++;
                new_lbr = TRUE;
            }
            break;

        case OAM_TYPE_DMR:
            data = &instance_data[mep[mep_idx-1]];    /* Instance data reference */
            if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                vtss_mep_discard_trace("Discard DMR to not Down-MEP  instance  port  vid", mep[mep_idx-1], port, frame_info->vid, 0);
                break;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
            }
            if (level != data->config.level)                      break;  /* Wrong level */
            if (data->config.sat) {    /* The SAT DMR frame get the COS related to the RX ISDX */
                for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {
                    if (data->rx_isdx[i] == frame_info->isdx)
                        break;
                }
                if (i < VTSS_MEP_SUPP_PRIO_MAX) {
                    priority = i;
                }
            }
            if (priority != 0xFFFFFFFF) { /* The priority was calculated - use state of that priority */
                state = &data->dm_state[priority];
            } else {    /* The priority was not calculated. In this case only one active DM flow is possible */
                for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i)
                    if (data->dm_state[i].active) break;
                i = (i == VTSS_MEP_SUPP_PRIO_MAX) ? frame_info->pcp : i;
                state = &data->dm_state[i];
            }

            /* Make sure the DMR is expected */
            if (!data->dmm_config.proprietary && data->dmm_config.calcway == VTSS_MEP_SUPP_FLOW) {
                data->rxTimef.seconds = string_to_var(&pdu[12]);
                data->rxTimef.nanoseconds = string_to_var(&pdu[16]);
                data->txTimeb.seconds = string_to_var(&pdu[20]);
                data->txTimeb.nanoseconds = string_to_var(&pdu[24]);
                vtss_mep_supp_trace("Far RX s  Far RX ns  Far TX s  Far TX ns", data->rxTimef.seconds, data->rxTimef.nanoseconds, data->txTimeb.seconds, data->txTimeb.nanoseconds);
                data->dmr_dev_delay = vtss_mep_supp_delay_calc(&data->txTimeb, &data->rxTimef, get_dm_tunit(data));
            }
            else {
                data->dmr_dev_delay = 0;
            }

            state->rx_dmr_timestamp = *rx_time;          /* Save RX time stamp */

            if (!vtss_mep_supp_check_hw_timestamp())
            {
                /* Check if the TX time is calculated */
                if (state->tx_dmm_fup_timestamp.seconds == 0 && state->tx_dmm_fup_timestamp.nanoseconds == 0) {
                    vtss_mep_supp_trace_err("Real tx time is not set yet", 0, 0, 0, 0);
                    break; /* Real tx time is not returned yet */
                }
            }
            else
            {
                /* Get tx time from packets. To simplify code to handle normal/fup dmr, always use fup timestamp to calculate DM. Therefore copy tx_time on packets to fup_timestamp */
                state->tx_dmm_fup_timestamp.seconds = string_to_var(&pdu[4]);
                state->tx_dmm_fup_timestamp.nanoseconds = string_to_var(&pdu[8]);
                data->dmr_ts_ok = TRUE;
            }

            new_dmr = handle_dmr(data, state);
#if F_2DM_FOR_1DM
            if (data->dmm_config.synchronized == TRUE) {
                if (data->dmm_config.proprietary && data->dmm_config.calcway == VTSS_MEP_SUPP_FLOW) {
                    /* One-way follow-up. Save the rx timestamp for calculation when follow-up packet arrives */
                    state->rx_dm1_timestamp = *rx_time;
                    break;
                }
                else {
                    /* One-way Standard (far-to-near)*/
                    txTimeb.seconds = string_to_var(&pdu[20]);
                    txTimeb.nanoseconds = string_to_var(&pdu[24]);
                    dm_delay = vtss_mep_supp_delay_calc(rx_time, &txTimeb, get_dm_tunit(data));

                    if (dm_delay >= 0 && dm_delay < 1e9) {   /* Check for valid delay */
                        state->fn_delays[state->fn_cnt % VTSS_MEP_SUPP_DM_MAX] = dm_delay;
                        state->fn_cnt++;
                    }
                    else {  /* If dm_delay <= 0 or above 1 sec, it means something wrong. Just skip it. */
                        vtss_mep_supp_trace("vtss_mep_supp_rx_frame: dm_delay invalid", 0, 0, 0, 0);
                        state->fn_rx_err_cnt++;
                    }

                    /* One-way Standard (near-to-far)*/
                    rxTimef.seconds = string_to_var(&pdu[12]);
                    rxTimef.nanoseconds = string_to_var(&pdu[16]);
                    dm_delay = vtss_mep_supp_delay_calc(&rxTimef, &state->tx_dmm_fup_timestamp, get_dm_tunit(data));

                    if (dm_delay >= 0 && dm_delay < 1e9) {   /* Check for valid delay */
                        state->nf_delays[state->nf_cnt % VTSS_MEP_SUPP_DM_MAX] = dm_delay;
                        state->nf_cnt++;
                    }
                    else {
                        vtss_mep_supp_trace("vtss_mep_supp_rx_frame: dm_delay invalid", 0, 0, 0, 0);
                        state->nf_rx_err_cnt++;
                    }
                }
            }
#endif
            break;

        case OAM_TYPE_DMR_FUP:
            data = &instance_data[mep[mep_idx-1]];    /* Instance data reference */
            if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                vtss_mep_discard_trace("Discard DMR_FUP to not Down-MEP  instance  port  vid", mep[mep_idx-1], port, frame_info->vid, 0);
                break;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
            }
            if (level != data->config.level)                      break;  /* Wrong level */
            if (!dm_enable_calc(data->dmm_config.enable))         break;  /* DMM is not enabled */

            state = &data->dm_state[frame_info->pcp];

            /* Make sure the real tx time is gotten */
            if (state->tx_dmm_fup_timestamp.seconds == 0 && state->tx_dmm_fup_timestamp.nanoseconds == 0) {
                vtss_mep_supp_trace("Real tx time is not returned yet", 0, 0, 0, 0);
                break; /* Real tx time is not returned yet */
            }

            if (data->dmm_config.proprietary && data->dmm_config.calcway == VTSS_MEP_SUPP_FLOW) {
                /* two-way with followup - flow measurement */
                rxTimef.seconds = string_to_var(&pdu[12]);
                rxTimef.nanoseconds = string_to_var(&pdu[16]);
                txTimeb.seconds = string_to_var(&pdu[20]);
                txTimeb.nanoseconds = string_to_var(&pdu[24]);
                /* the timestamp of followup packets seems not to be correct */
                is_ns =  get_dm_tunit(data);
                total_delay = vtss_mep_supp_delay_calc(&state->rx_dmr_timestamp, &state->tx_dmm_fup_timestamp, is_ns);
                if (total_delay < 0)
                    vtss_mep_supp_trace("total_delay < 0", 0, 0, 0, 0);

                device_delay = vtss_mep_supp_delay_calc(&txTimeb, &rxTimef, is_ns);
                if (device_delay < 0)
                    vtss_mep_supp_trace("device_delay < 0", 0, 0, 0, 0);
                if ((total_delay >= 0) && (device_delay >= 0)) {
                    /* If delay < 0, it means something wrong. Just skip it */
                    dm_delay = total_delay - device_delay;
                    if (dm_delay >= 0 && dm_delay < 1e9 /* timeout is 1 second */) {   /* If dm_delay <= 0, it means something wrong. Just skip it. */
                        state->tw_delays[state->tw_cnt % VTSS_MEP_SUPP_DM_MAX] = dm_delay;
                        state->tw_cnt++;
                    }
                    else {
                        if (dm_delay < 0)
                            vtss_mep_supp_trace("dm_delay < 0", 0, 0, 0, 0);
                        state->tw_rx_err_cnt++;
                    }
                }
                else {
                    state->tw_rx_err_cnt++;
                }
            }

            new_dmr = TRUE;
            state->dmm_tx_active = FALSE;

            for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Check if any transmission is still ongoing */
                if (data->dmm_config.enable[i] && data->dm_state[i].dmm_tx_active)
                    break;
            }

            if (i == VTSS_MEP_SUPP_PRIO_MAX)
                data->rx_dmr_timeout_timer = 0;

#if F_2DM_FOR_1DM
            if (data->dmm_config.synchronized == TRUE) {
                /* one-way with followup (far-to-near)*/
                txTimeb.seconds = string_to_var(&pdu[20]);
                txTimeb.nanoseconds = string_to_var(&pdu[24]);

                if (state->rx_dm1_timestamp.seconds == 0) {
                    /* Unexpected 1DM FUP packet */
                    break;
                }

                dm_delay = vtss_mep_supp_delay_calc(&state->rx_dm1_timestamp, &txTimeb, get_dm_tunit(data));

                if (dm_delay >= 0 && dm_delay < 1e9 /* delay over 1 second  */) {   /* If dm_delay <= 0, it means something wrong. Just skip it. */
                    state->fn_delays[state->fn_cnt % VTSS_MEP_SUPP_DM_MAX] = dm_delay;
                    state->fn_cnt++;
                }
                else {
                    state->fn_rx_err_cnt++;
                }

                /* One-way Standard (near-to-far)*/
                rxTimef.seconds = string_to_var(&pdu[12]);
                rxTimef.nanoseconds = string_to_var(&pdu[16]);
                dm_delay = vtss_mep_supp_delay_calc(&rxTimef, &state->tx_dmm_fup_timestamp, get_dm_tunit(data));

                if (dm_delay >= 0 && dm_delay < 1e9) {   /* Check for valid delay */
                    state->nf_delays[state->nf_cnt % VTSS_MEP_SUPP_DM_MAX] = dm_delay;
                    state->nf_cnt++;
                }
                else {
                    vtss_mep_supp_trace("vtss_mep_supp_rx_frame: dm_delay invalid", 0, 0, 0, 0);
                    state->nf_rx_err_cnt++;
                }

                state->rx_dm1_timestamp.seconds = 0;
                state->rx_dm1_timestamp.nanoseconds = 0;
            }
#endif
            break;

        case OAM_TYPE_DMM:
            for (i=0; i<mep_idx; ++i) {
                data = &instance_data[mep[i]];    /* Instance data reference */
                if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                    vtss_mep_discard_trace("Discard DMM to not Down-MEP  instance  port  vid", mep[i], port, frame_info->vid, 0);
                    continue;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
                }
                if (level != data->config.level)      continue;  /* Wrong level */
                if (data->config.voe && (data->config.direction == VTSS_MEP_SUPP_DOWN))   continue;  /* Not expected to receive this */

                state = &data->dm_state[frame_info->pcp];

                dmr_to_peer(mep[i], data, state, rx_time, &frame[VTSS_MEP_SUPP_MAC_LENGTH], pdu, len-(tl_idx + 2), frame_info, reply_port_calc(data, port), is_mpls);
            }
            break;

        case OAM_TYPE_1DM:
            for (i=0; i<mep_idx; ++i) {
                data = &instance_data[mep[i]];    /* Instance data reference */
                if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                    vtss_mep_discard_trace("Discard 1DM to not Down-MEP  instance  port  vid", mep[i], port, frame_info->vid, 0);
                    continue;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
                }
                if (level != data->config.level)                continue;  /* Wrong level */
                if (dm_enable_calc(data->dmm_config.enable))    continue;  /* DMM is enabled - cannot receive 1DM will mess up the DMM based one way statistics */

                if (data->config.sat) {    /* The SAT DMR frame get the COS related to the RX ISDX */
                    for (j=0; j<VTSS_MEP_SUPP_PRIO_MAX; ++j) {
                        if (data->rx_isdx[j] == frame_info->isdx)
                            break;
                    }
                    if (j < VTSS_MEP_SUPP_PRIO_MAX) {
                        priority = j;
                    }
                }
                if (priority != 0xFFFFFFFF) { /* The priority was calculated - use state of that priority */
                    state = &data->dm_state[priority];
                } else {    /* The priority was not calculated. In this case only one active DM flow is possible */
                    for (j=0; j<VTSS_MEP_SUPP_PRIO_MAX; ++j)
                        if (data->dm_state[j].active) break;
                    j = (j == VTSS_MEP_SUPP_PRIO_MAX) ? frame_info->pcp : j;
                    state = &data->dm_state[j];
                }

                if (memcmp(&pdu[DM1_PDU_STD_LENGTH], "VTSS", 4)) {
                    /* One-way Standard */
                    txTimeb.seconds = string_to_var(&pdu[4]);
                    txTimeb.nanoseconds = string_to_var(&pdu[8]);

                    dm_delay = vtss_mep_supp_delay_calc(rx_time, &txTimeb, get_dm_tunit(data));

                    if (dm_delay >= 0 && dm_delay < 1e9 /* delay over 1 second  */) {   /* If dm_delay <= 0, it means something wrong. Just skip it. */
                        state->fn_delays[state->fn_cnt % VTSS_MEP_SUPP_DM_MAX] = dm_delay;
                        state->fn_cnt++;
                    }
                    else {
                        vtss_mep_supp_trace("xxx vtss_mep_supp_rx_frame: dm_delay < 0", 0, 0, 0, 0);
                        state->fn_rx_err_cnt++;
                    }
                }
                else {
                    /* One-way follow-up */
                    /* Save the rx timestamp for calculation when follow-up packet arrives */
                    state->rx_dm1_timestamp.seconds = rx_time->seconds;
                    state->rx_dm1_timestamp.nanoseconds = rx_time->nanoseconds;
                    continue;
                }
                new_dm1 = TRUE;
            }
            break;

        case OAM_TYPE_1DM_FUP:
            for (i=0; i<mep_idx; ++i) {
                data = &instance_data[mep[i]];    /* Instance data reference */
                if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                    vtss_mep_discard_trace("Discard 1DM_FUP to not Down-MEP  instance  port  vid", mep[i], port, frame_info->vid, 0);
                    continue;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
                }
                if (level != data->config.level)      continue;  /* Wrong level */

                state = &data->dm_state[frame_info->pcp];

                /* one-way with followup */
                txTimeb.seconds = string_to_var(&pdu[4]);
                txTimeb.nanoseconds = string_to_var(&pdu[8]);

                if (state->rx_dm1_timestamp.seconds == 0) {
                    /* Unexpected 1DM FUP packet */
                    continue;
                }

                dm_delay = vtss_mep_supp_delay_calc(&state->rx_dm1_timestamp, &txTimeb, get_dm_tunit(data));

                if (dm_delay >= 0 && dm_delay < 1e9 /* delay over 1 second  */) {   /* If dm_delay <= 0, it means something wrong. Just skip it. */
                    state->fn_delays[state->fn_cnt % VTSS_MEP_SUPP_DM_MAX] = dm_delay;
                    state->fn_cnt++;
                }
                else {
                    state->fn_rx_err_cnt++;
                }

                state->rx_dm1_timestamp.seconds = 0;
                state->rx_dm1_timestamp.nanoseconds = 0;

                new_dm1 = TRUE;
            }
            break;

        case OAM_TYPE_AIS:
            for (i=0; i<mep_idx; ++i) {
                data = &instance_data[mep[i]];    /* Instance data reference */
                if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                    vtss_mep_discard_trace("Discard AIS to not Down-MEP  instance  port  vid", mep[i], port, frame_info->vid, 0);
                    continue;    /* Frame that is marked to discard, must be discarded */
                }
                if (level != data->config.level)      continue;  /* Wrong level */

                if (!data->defect_state.dAis)    new_defect = TRUE;
                data->defect_state.dAis = TRUE;
                defect_timer_calc(data, &data->dAis, pdu[2] & 0x07);
            }
            break;

        case OAM_TYPE_LCK:
            for (i=0; i<mep_idx; ++i) {
                data = &instance_data[mep[i]];    /* Instance data reference */
                if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                    vtss_mep_discard_trace("Discard LCK to not Down-MEP  instance  port  vid", mep[i], port, frame_info->vid, 0);
                    continue;    /* Frame that is marked to discard, must be discarded */
                }
                if (level != data->config.level)      continue;  /* Wrong level */

                if (!data->defect_state.dLck)    new_defect = TRUE;
                data->defect_state.dLck = TRUE;
                defect_timer_calc(data, &data->dLck, pdu[2] & 0x07);
            }
            break;

        case OAM_TYPE_TST:
            data = &instance_data[mep[mep_idx-1]];    /* Instance data reference */
            if (frame_info->discard && (data->config.direction != VTSS_MEP_SUPP_DOWN)) {
                vtss_mep_discard_trace("Discard TST to not Down-MEP  instance  port  vid", mep[mep_idx-1], port, frame_info->vid, 0);
                break;    /* Frame that is marked to discard, and do not hit a Down-MEP, must be discarded */
            }
            if (level != data->config.level)      break;  /* Wrong level */

            tst_frame_size = len + 4 + (data->tst_config.line_rate ? 20 : 0);  /* Added CRC and Preamble and Inter frame gap */
            new_tst = TRUE;

            data->tst_rx_cnt++;
            if (mep_caps.mesa_mep_serval) {
                mesa_oam_voe_conf_t cfg;

                if (data->voe_idx < mep_caps.mesa_oam_voe_cnt) { /* VOE based TST */
                    (void)mesa_oam_voe_conf_get(NULL, data->voe_idx, &cfg);
                    cfg.tst.copy_to_cpu = FALSE;
                    (void)mesa_oam_voe_conf_set(NULL, data->voe_idx, &cfg);

#if 0
                    if (data->tst_config.enable_rx) {   /* Get another TST PDU sample in 1 sec - if TST analyse is still enabled */
                        data->tst_timer = 1000/timer_res;
                        vtss_mep_supp_timer_start();
                    }
#endif
                }
            }
            break;




































































        default:            break;
    }

    unlock:
    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    /* The following call-out is done here where crit is unlocked, in order to avoid deadlock */
    for (i=0; i<mep_idx; ++i) {
        if (new_ltr)        vtss_mep_supp_new_ltr(mep[i]);
        if (new_lbr)        vtss_mep_supp_new_lbr(mep[i]);
        if (new_dm1)        vtss_mep_supp_new_dm1(mep[i]);
        if (new_dmr)        vtss_mep_supp_new_dmr(mep[i]);
        if (new_aps)        vtss_mep_supp_new_aps(mep[i], &pdu[4]);
        if (new_tst)        vtss_mep_supp_new_tst(mep[i], tst_frame_size);
        if (new_defect)     vtss_mep_supp_new_defect_state(mep[i]);
        if (new_state)      vtss_mep_supp_new_ccm_state(mep[i]);
        if (new_mac)        vtss_mep_supp_new_mac(mep[i]);
        if (new_is)         vtss_mep_supp_new_tlv_is(mep[i]);
    }
}


void vtss_mep_supp_voe_interrupt(void)
{
    supp_instance_data_t        *data;
    CapArray<u32, MESA_CAP_OAM_EVENT_ARRAY_SIZE> voe_mask;
    mesa_oam_voe_event_mask_t   voe_event;
    mesa_oam_ccm_status_t       ccm_status;
    u32                         size, voe, indexx, mask, mep;
    BOOL                        new_defect;

    (void)mesa_oam_event_poll(NULL, mep_caps.mesa_oam_event_array_size, voe_mask.data());
    size = sizeof(voe_mask[0]) * 8;

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
    for (voe=0, indexx=0, mask =0x01; voe<mep_caps.mesa_oam_voe_cnt; ++voe, indexx+=(!(voe%size) ? 1 : 0), mask = (!(voe%size) ? 0x01 : (mask<<1))) {
        if (!(voe_mask[indexx] & mask))     continue;                                         /* Check for new event on this VOE instance */
        if (mesa_oam_voe_event_poll(NULL, voe, &voe_event) != VTSS_RC_OK)   continue;                 /* Poll the VOE */
        (void)mesa_oam_voe_event_enable(NULL, voe, voe_event, FALSE);

        mep = voe_to_mep[voe];
        data = &instance_data[mep];    /* Instance data reference */

        if (!voe_ccm_enable(data))    continue;   /* Check if this MEP instance has valid VOE and only one peer */

        new_defect = FALSE;

        (void)mesa_oam_ccm_status_get(NULL, data->voe_idx, &ccm_status);

        /* Set defects active if new state */
        if (voe_event & mep_caps.mesa_oam_event_ccm_zero_period)     new_defect = TRUE;      /* Status is fetched directly from VOE */
        if (voe_event & mep_caps.mesa_oam_event_ccm_rx_rdi)          new_defect = TRUE;
        if (voe_event & mep_caps.mesa_oam_event_ccm_loc)             new_defect = TRUE;
        if (voe_event & mep_caps.mesa_oam_event_ccm_meg_id) {
            if (!data->defect_state.dMeg && ccm_status.meg_id_err) { /* Only Raising edge */
                new_defect = TRUE;
                data->defect_state.dMeg = TRUE;
                defect_timer_calc(data, &data->dMeg, rate_to_pdu_calc(VTSS_MEP_SUPP_RATE_1S));     /* This is to activate defect timer in case this detect is not caught by CCM poll */
                data->ccm_defect_active = TRUE;
            }
        }
        if (voe_event & mep_caps.mesa_oam_event_ccm_mep_id) {
            if (!data->defect_state.dMep && ccm_status.mep_id_err) { /* Only Raising edge */
                new_defect = TRUE;
                data->defect_state.dMep = TRUE;
                defect_timer_calc(data, &data->dMep, rate_to_pdu_calc(VTSS_MEP_SUPP_RATE_1S));     /* This is to activate defect timer in case this detect is not caught by CCM poll */
                data->ccm_defect_active = TRUE;
            }
        }
        if (voe_event & mep_caps.mesa_oam_event_ccm_period) {
            if (!data->defect_state.dPeriod[0] && ccm_status.period_err) { /* Only Raising edge */
                new_defect = TRUE;
                data->defect_state.dPeriod[0] = TRUE;
                defect_timer_calc(data, &data->dPeriod[0], rate_to_pdu_calc(VTSS_MEP_SUPP_RATE_1S));     /* This is to activate defect timer in case this detect is not caught by CCM poll */
                data->ccm_defect_active = TRUE;
            }
        }
        if (voe_event & mep_caps.mesa_oam_event_ccm_priority) {
            if (!data->defect_state.dPrio[0] && ccm_status.priority_err) { /* Only Raising edge */
                new_defect = TRUE;
                data->defect_state.dPrio[0] = TRUE;
                defect_timer_calc(data, &data->dPrio[0], rate_to_pdu_calc(VTSS_MEP_SUPP_RATE_1S));     /* This is to activate defect timer in case this detect is not caught by CCM poll */
                data->ccm_defect_active = TRUE;
            }
        }

        if (new_defect) {
            vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
            vtss_mep_supp_new_defect_state(mep);
            vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
        }
    }
    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
}

BOOL vtss_mep_supp_voe_up(u32 isdx, u32 vid, u32 level, u32 *voe)
{   /* Check if this ISDX relates to a VOE UP-MEP */
    if (mep_caps.mesa_mep_serval) {
        u32 i;
        supp_instance_data_t  *data;

        vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
        for (i=0; i<mep_caps.mesa_oam_path_service_voe_cnt; ++i) {
            if (voe_to_mep[i] < VTSS_MEP_SUPP_CREATED_MAX) { /* This VOE index is in use */
                data = &instance_data[voe_to_mep[i]];        /* Instance data reference */
                if (data->rx_isdx[0] == isdx) {         /* Check if this ISDX belong to this MEP */
                    *voe = i;
                    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
                    return (data->config.direction == VTSS_MEP_SUPP_UP);
                }
            }
        }

        /* VOE based Up-MEP was not found using ISDX - it might be that a SAT Up-MEP should receive this as we do not have all possible eight ISDX for a SAT MEP */
        for (i=0; i<VTSS_MEP_SUPP_CREATED_MAX; ++i) {
            /* Find a SAT MEP in this VID on this level */
            if (instance_data[i].config.enable && instance_data[i].config.sat && (instance_data[i].config.flow.vid == vid) && (instance_data[i].config.level == level)) {
                *voe = instance_data[i].voe_idx;
                vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
                return (TRUE);
            }
        }

        vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
    }
    return(FALSE);
}



u32 vtss_mep_supp_rx_isdx_get(const u32 instance,  u32 port)
{
    return(instance_data[instance].rx_isdx[port]);
}


u32 vtss_mep_supp_tx_isdx_get(const u32  instance)
{
    return(instance_data[instance].tx_isdx);
}

/****************************************************************************/
/*  MEP configuration interface                                             */
/****************************************************************************/

u32 vtss_mep_supp_conf_set(const u32                   instance,
                           const vtss_mep_supp_conf_t  *const conf,
                           vtss_flag_t                 *sync_flag)
{
    u32 retval;

    vtss_mep_supp_trace("vtss_mep_supp_conf_set enter", instance, conf->enable, 0, 0);

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX) {
        retval = VTSS_MEP_SUPP_RC_INVALID_PARAMETER;
        goto do_exit;
    }

    if (!instance_data[instance].config.enable && !conf->enable) {
        retval = VTSS_MEP_SUPP_RC_OK;
        goto do_exit;
    }

#if 0
    if (!memcmp(&instance_data[instance].config, conf, sizeof(vtss_mep_supp_conf_t))) {
        retval = VTSS_MEP_SUPP_RC_OK;
        goto do_exit;
    }
#endif

    if (instance_data[instance].config.enable && conf->enable &&
        ((instance_data[instance].config.flow.mask & (VTSS_MEP_SUPP_FLOW_MASK_EVC_ID | VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID)) !=
         (conf->flow.mask & (VTSS_MEP_SUPP_FLOW_MASK_EVC_ID | VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID)))) {
        retval = VTSS_MEP_SUPP_RC_INVALID_PARAMETER;
        goto do_exit;
    }

    if (instance_data[instance].config.enable && conf->enable && (instance_data[instance].config.direction != conf->direction)) {
        retval = VTSS_MEP_SUPP_RC_INVALID_PARAMETER;
        goto do_exit;
    }

    if (!mep_caps.mesa_mep_serval) {
        if (conf->voe) {
            retval = VTSS_MEP_SUPP_RC_NO_VOE;
            goto do_exit;
        }
    }

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    retval = VTSS_MEP_SUPP_RC_OK;
    if (conf->enable)      retval = run_sync_config(instance, conf);
    if (retval == VTSS_MEP_SUPP_RC_OK) {
        if (conf->enable)   instance_data[instance].config = *conf;
        else                instance_data[instance].config.enable = FALSE;

        if (mep_caps.mesa_mep_serval) {
            u32 i;
            if ((instance_data[instance].config.flow.mask & (VTSS_MEP_SUPP_FLOW_MASK_EVC_ID | VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID)) && (instance_data[instance].config.mode == VTSS_MEP_SUPP_MEP)) { /* This is a EVC or VLAN MEP - all MEP in this VID must be re-configured to assure correct TCAM rules and VOE configuration */
                for (i=0; i<VTSS_MEP_SUPP_CREATED_MAX; i++) /* Find instance on this vid */
                    if (instance_data[i].config.enable && (instance_data[i].config.mode == VTSS_MEP_SUPP_MEP) && (instance_data[i].config.flow.mask & (VTSS_MEP_SUPP_FLOW_MASK_EVC_ID | VTSS_MEP_SUPP_FLOW_MASK_VLAN_ID)) &&
                        (instance_data[i].config.flow.vid == instance_data[instance].config.flow.vid)) {
                        instance_data[i].event_flags |= EVENT_IN_CONFIG;
                    }
            }
        }

        retval = run_async_config(instance); /* Call synchronously */
        instance_data[instance].voe_config = FALSE;

        vtss_mep_supp_run();    /* This still needs to be done to assure call of run_async_config() for all MEPs in the EVC/VLAN */
    }

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
do_exit:
    return retval;
}

u32 vtss_mep_supp_sat_counters_get(const u32                     instance,
                                   vtss_mep_supp_sat_counters_t  *const counters)
{
    u32                   i, nni, retval=VTSS_MEP_SUPP_RC_OK;
    mesa_mce_id_t         mce_id;
    supp_instance_data_t  *data;
    mesa_evc_counters_t   evc_counters;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                          return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    if (!instance_data[instance].config.enable)                         {vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__); return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);}
    if (!instance_data[instance].config.sat)                            {vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__); return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);}
    if (instance_data[instance].config.direction != VTSS_MEP_SUPP_UP)   {vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__); return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);}

    data = &instance_data[instance];    /* Instance data reference */

    for (nni=0; nni<mep_caps.mesa_port_cnt; ++nni)  if (data->config.flow.port[nni])    break;    /* Find first port that is a NNI */
    if (nni >= mep_caps.mesa_port_cnt)    {vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__); return (VTSS_MEP_SUPP_RC_NO_RESOURCES);}

    memset(counters, 0, sizeof(*counters));

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {
        if (data->config.sat_conf[i].vid == VTSS_MEP_SUPP_SAT_VID_UNUSED)
            continue;

        /* Get injection (TX) counters */
        mce_id = MCE_ID_CALC(instance, data->config.port, i, 1);

        if (mesa_mce_counters_get(NULL, mce_id,  data->config.port,  &evc_counters) == VTSS_RC_OK) {
            counters->counters[i].tx_discard = evc_counters.rx_discard;
            counters->counters[i].tx_green = evc_counters.rx_green;
            counters->counters[i].tx_yellow = evc_counters.rx_yellow;
        } else {
            retval = VTSS_MEP_SUPP_RC_NO_RESOURCES;
        }

        /* Get extraction (RX) counters */
        mce_id = MCE_ID_CALC(instance, nni, i, 0);

        if (mesa_mce_counters_get(NULL, mce_id,  data->config.port,  &evc_counters) == VTSS_RC_OK) {
            counters->counters[i].rx_discard = evc_counters.tx_discard;
            counters->counters[i].rx_green = evc_counters.tx_green;
            counters->counters[i].rx_yellow = evc_counters.tx_yellow;
        } else {
            retval = VTSS_MEP_SUPP_RC_NO_RESOURCES;
        }
    }

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(retval);
}

u32 vtss_mep_supp_sat_counters_clear(const u32  instance)
{
    u32                   i, nni, retval=VTSS_MEP_SUPP_RC_OK;
    mesa_mce_id_t         mce_id;
    supp_instance_data_t  *data;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                          return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    if (!instance_data[instance].config.enable)                         {vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__); return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);}
    if (!instance_data[instance].config.sat)                            {vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__); return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);}
    if (instance_data[instance].config.direction != VTSS_MEP_SUPP_UP)   {vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__); return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);}

    data = &instance_data[instance];    /* Instance data reference */

    for (nni=0; nni<mep_caps.mesa_port_cnt; ++nni)  if (data->config.flow.port[nni])    break;    /* Find first port that is a NNI */
    if (nni >= mep_caps.mesa_port_cnt)    {vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__); return (VTSS_MEP_SUPP_RC_NO_RESOURCES);}

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {
        if (data->config.sat_conf[i].vid == VTSS_MEP_SUPP_SAT_VID_UNUSED)
            continue;

        /* Clear injection (TX) counters */
        mce_id = MCE_ID_CALC(instance, data->config.port, i, 1);
        if (mesa_mce_counters_clear(NULL, mce_id,  data->config.port) != VTSS_RC_OK) {
            retval = VTSS_MEP_SUPP_RC_NO_RESOURCES;
        }

        /* Clear extraction (RX) counters */
        mce_id = MCE_ID_CALC(instance, nni, i, 0);
        if (mesa_mce_counters_clear(NULL, mce_id,  data->config.port) != VTSS_RC_OK) {
            retval = VTSS_MEP_SUPP_RC_NO_RESOURCES;
        }
    }

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(retval);
}

u32 vtss_mep_supp_conf_mac_get(const u32  instance,
                               u8         *mac)
{
    if (instance >= VTSS_MEP_SUPP_CREATED_MAX) {
        return VTSS_MEP_SUPP_RC_INVALID_PARAMETER;
    }

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
    memcpy(mac, instance_data[instance].config.mac, 6);
    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return VTSS_MEP_SUPP_RC_OK;
}

u32 vtss_mep_supp_lm_flow_count_set(const u32        instance,
                                    const BOOL       flow_count)
{
    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                                                   return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    if (mep_caps.mesa_mep_serval) {
        vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

        instance_data[instance].lm_flow_count = flow_count;
        voe_config(&instance_data[instance]);
        instance_data[instance].voe_config = FALSE;

        vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
    }

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_lm_oam_count_set(const u32                        instance,
                                   const vtss_mep_supp_oam_count    oam_count)
{
    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                                                   return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    if (mep_caps.mesa_mep_serval) {
        vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

        instance_data[instance].lm_oam_count = oam_count;
        voe_config(&instance_data[instance]);
        instance_data[instance].voe_config = FALSE;

        vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
    }

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_ccm_conf_set(const u32                          instance,
                               const vtss_mep_supp_ccm_conf_t     *const conf)
{
    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                                                   return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (!memcmp(&instance_data[instance].ccm_config, conf, sizeof(vtss_mep_supp_ccm_conf_t)))    return(VTSS_MEP_SUPP_RC_OK);
    if ((conf->peer_count > 1) && hw_ccm_calc(conf->rate))                                       return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    instance_data[instance].event_flags |= EVENT_IN_CCM_CONFIG;

    /* Check if the learned peer MAC must be deleted */
    if (conf->peer_count >= instance_data[instance].ccm_config.peer_count) { /* Number of peer MEP is increased */
        if (memcmp(conf->peer_mep, instance_data[instance].ccm_config.peer_mep, instance_data[instance].ccm_config.peer_count*sizeof(u16))) /* Clear if the existing peer MEP is changed */
            memset(instance_data[instance].peer_mac, 0, (mep_caps.mesa_oam_peer_cnt * VTSS_MEP_SUPP_MAC_LENGTH));
    }
    else {   /* Number of peer MEP is the same or less */
        if (memcmp(conf->peer_mep, instance_data[instance].ccm_config.peer_mep, conf->peer_count*sizeof(u16)))  /* Clear if the remaining peer MEP is changed */
            memset(instance_data[instance].peer_mac, 0, (mep_caps.mesa_oam_peer_cnt * VTSS_MEP_SUPP_MAC_LENGTH));
        memset(instance_data[instance].peer_mac[conf->peer_count], 0, ((mep_caps.mesa_oam_peer_cnt - conf->peer_count) * VTSS_MEP_SUPP_MAC_LENGTH));
    }

    instance_data[instance].ccm_config = *conf;

    vtss_mep_supp_run();

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_ccm_generator_set(const u32                         instance,
                                    const vtss_mep_supp_gen_conf_t    *const conf)
{
    u32 retval;

    retval = VTSS_MEP_SUPP_RC_OK;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                                                return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (!memcmp(&instance_data[instance].ccm_gen, conf, sizeof(vtss_mep_supp_gen_conf_t)))    return(VTSS_MEP_SUPP_RC_OK);
    if ((instance_data[instance].ccm_config.peer_count > 1) && hw_ccm_calc(conf->cc_rate))    return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    if (hw_ccm_calc(conf->cc_rate))
    {   /* HW generation requested */
        if (mep_caps.mesa_mep_luton26) {
            if (conf->lm_enable)    {vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);  return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);}   /* On Luton26 SW CCM-LM is not possible besides HW CCM */
        }
        if (conf->enable)
            retval = run_sync_ccm_hw_gen(instance);
        if ((conf->lm_enable) && (retval == VTSS_MEP_SUPP_RC_OK))
            retval = run_sync_ccm_sw_gen(instance);  /* On Jaguar CCM-LM must be transmitted by SW */
        if (mep_caps.mesa_mep_serval) {
            if ((conf->lm_enable) && (retval == VTSS_MEP_SUPP_RC_OK) && instance_data[instance].config.voe && (instance_data[instance].config.direction == VTSS_MEP_SUPP_UP))
                retval = run_sync_ccm_sw_gen(instance); /* On Serval CCM-LM must be transmitted by SW in case of UP-MEP */
        }
    }
    else
    {   /* SW generator requested */
        if (conf->enable)     retval = run_sync_ccm_sw_gen(instance);
    }

    if (retval == VTSS_MEP_SUPP_RC_OK)
    {
        if (conf->lm_enable && !instance_data[instance].ccm_gen.lm_enable)
            instance_data[instance].lm_start[0] = TRUE;  /* This is to know when the first LM counters are received. The first counters only used to get 'previous' frame counters initialized */

        instance_data[instance].event_flags |= EVENT_IN_CCM_GEN;
        instance_data[instance].ccm_start = ((conf->enable) && (!instance_data[instance].ccm_gen.enable)) ? TRUE : FALSE;
        if (conf->enable)   instance_data[instance].ccm_gen = *conf;
        else                instance_data[instance].ccm_gen.enable = FALSE;

        vtss_mep_supp_run();
    }

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(retval);
}

u32 vtss_mep_supp_ccm_rdi_set(const u32       instance,
                              const BOOL      state)
{
    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)              return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (instance_data[instance].ccm_rdi == state)           return(VTSS_MEP_SUPP_RC_OK);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    instance_data[instance].event_flags |= EVENT_IN_CCM_RDI;
    instance_data[instance].ccm_rdi = state;

    vtss_mep_supp_run();

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_ccm_state_get(const u32                     instance,
                                vtss_mep_supp_ccm_state_t     *const state)
{
    supp_instance_data_t        *data;
    u32 offset, length;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)              return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    data = &instance_data[instance];    /* Instance data reference */
    *state = data->ccm_state.state;

    if (data->ccm_state.unexp_meg_id[0] == 1) {
        if (data->ccm_state.unexp_meg_id[1] == 32) {   /* ITU ICC based MEG-ID - short MA name */
            memcpy(state->unexp_name, &data->ccm_state.unexp_meg_id[3], 6);
            state->unexp_name[6] = '\0';
            memcpy(state->unexp_meg, &data->ccm_state.unexp_meg_id[9], 7);
            state->unexp_meg[7] = '\0';
        }
    }
    else {
        if (data->ccm_state.unexp_meg_id[0] == 4) {   /* IEEE string */
            offset = 2; /* Name offset and length */
            length = data->ccm_state.unexp_meg_id[1];
            length = (length >= VTSS_MEP_SUPP_MEG_CODE_LENGTH-1) ? VTSS_MEP_SUPP_MEG_CODE_LENGTH-1 : length;
            memcpy(state->unexp_name, &data->ccm_state.unexp_meg_id[offset], length);
            state->unexp_name[length] = '\0';

            offset = 4 + data->ccm_state.unexp_meg_id[1];   /* Short name offset and length */
            if (offset < MEG_ID_LENGTH) { /* The short name is in the buffer */
                if (data->ccm_state.unexp_meg_id[2+data->ccm_state.unexp_meg_id[1]] == 2) {   /* IEEE string */
                    length = data->ccm_state.unexp_meg_id[3+data->ccm_state.unexp_meg_id[1]];
                    length = (length >= VTSS_MEP_SUPP_MEG_CODE_LENGTH-1) ? VTSS_MEP_SUPP_MEG_CODE_LENGTH-1 : length;
                    length = ((offset+length) > MEG_ID_LENGTH) ? MEG_ID_LENGTH-offset : length;
                    memcpy(state->unexp_meg, &data->ccm_state.unexp_meg_id[offset], length);
                    state->unexp_meg[length] = '\0';
                }
            }
        }
    }

    if (mep_caps.mesa_mep_serval) {
        mesa_oam_voe_counter_t voe_counter;
        if (voe_ccm_enable(data)) {  /* This VOE based CCM - Check for VOE based status */
            if (mesa_oam_voe_counter_get(NULL, data->voe_idx, &voe_counter) != VTSS_RC_OK) {
                vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
                return(VTSS_MEP_SUPP_RC_VOE_ERROR);
            }

            state->valid_counter = voe_counter.ccm.rx_valid_count;
            state->invalid_counter = voe_counter.ccm.rx_invalid_count;
            state->oo_counter = voe_counter.ccm.rx_invalid_seq_no;
        }
    }
    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}
































































































































































































































































































u32 vtss_mep_supp_defect_state_get(const u32                      instance,
                                   vtss_mep_supp_defect_state_t   *const state)
{
    u32 i;
    mesa_rc rc = VTSS_RC_OK;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)        return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    *state = instance_data[instance].defect_state;

    /* sw/hw detected LOC is converted to a TRUE/FALSE */
    state->dLoc[0] = (state->dLoc[0] != 0) ? TRUE : FALSE;
    for (i=1; i<instance_data[instance].ccm_config.peer_count; ++i)
        state->dLoc[i] = (state->dLoc[i] != 0) ? TRUE : FALSE;

    if (mep_caps.mesa_mep_serval) {
        mesa_oam_ccm_status_t       ccm_status;
        if (voe_ccm_enable(&instance_data[instance])) {  /* This is VOE based CCM - Check for VOE based status */
            rc = mesa_oam_ccm_status_get(NULL, instance_data[instance].voe_idx, &ccm_status);
            state->dInv = ccm_status.zero_period_err;
            state->dLoc[0] = ccm_status.loc;
            state->dRdi[0] = ccm_status.rx_rdi;
            if (state->dLoc[0])    state->dRdi[0] = FALSE;
        }
    }
    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return((rc == VTSS_RC_OK) ? VTSS_MEP_SUPP_RC_OK : VTSS_MEP_SUPP_RC_VOE_ERROR);
}

u32 vtss_mep_supp_learned_mac_get(const u32   instance,
                                  u8          (*mac)[VTSS_MEP_SUPP_MAC_LENGTH])
{
    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)        return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    memcpy(mac, instance_data[instance].peer_mac, (mep_caps.mesa_oam_peer_cnt * VTSS_MEP_SUPP_MAC_LENGTH));

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_lmm_conf_set(const u32                        instance,
                               const vtss_mep_supp_lmm_conf_t   *const conf)
{
    u32 retval;

    vtss_mep_supp_lm_trace("vtss_mep_supp_lmm_conf_set  instance  enable  synthetic", instance, conf->enable, instance_data[instance].slm_config.enable, 0);

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                                                   return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (instance_data[instance].slm_config.enable && conf->enable)                               return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (!instance_data[instance].lmm_config.enable && !conf->enable &&
        !instance_data[instance].lmm_config.enable_rx && !conf->enable_rx)                       return(VTSS_MEP_SUPP_RC_OK);
    if (!memcmp(&instance_data[instance].lmm_config, conf, sizeof(vtss_mep_supp_lmm_conf_t)))    return(VTSS_MEP_SUPP_RC_OK);
    vtss_mep_supp_lm_trace("vtss_mep_supp_lmm_conf_set  config changed", 0, 0, 0, 0);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    retval = VTSS_MEP_SUPP_RC_OK;
    if (conf->enable)      retval = run_sync_lmm_config(instance);
    if (retval == VTSS_MEP_SUPP_RC_OK)
    {
        if ((conf->enable && !instance_data[instance].lmm_config.enable) ||
            (conf->enable_rx && !instance_data[instance].lmm_config.enable_rx))
            instance_data[instance].lm_start[0] = TRUE;  /* This is to know when the first LM counters are received. The first counters only used to get 'previous' frame counters initialized */
        instance_data[instance].event_flags |= EVENT_IN_LMM_CONFIG;
        if (conf->enable || conf->enable_rx)  instance_data[instance].lmm_config = *conf;
        else                                  {instance_data[instance].lmm_config.enable = FALSE; instance_data[instance].lmm_config.enable_rx = FALSE;}
    }

    vtss_mep_supp_run();

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_slm_conf_set(const u32                        instance,
                               const vtss_mep_supp_slm_conf_t   *const conf)
{
    u32 retval, i;

    vtss_mep_supp_lm_trace("vtss_mep_supp_slm_conf_set  instance  enable  service  size", instance, conf->enable, instance_data[instance].lmm_config.enable, conf->size);

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                                                   return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (instance_data[instance].lmm_config.enable && conf->enable)                               return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (!instance_data[instance].slm_config.enable && !conf->enable &&
        !instance_data[instance].slm_config.enable_rx && !conf->enable_rx)                       return(VTSS_MEP_SUPP_RC_OK);
    if (!memcmp(&instance_data[instance].slm_config, conf, sizeof(vtss_mep_supp_slm_conf_t)))    return(VTSS_MEP_SUPP_RC_OK);
    vtss_mep_supp_lm_trace("vtss_mep_supp_slm_conf_set  config changed", 0, 0, 0, 0);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    retval = VTSS_MEP_SUPP_RC_OK;
    if (conf->enable)      retval = run_sync_slm_config(instance, conf->size);
    if (retval == VTSS_MEP_SUPP_RC_OK)
    {
        if ((conf->enable && !instance_data[instance].slm_config.enable) ||
            (conf->enable_rx && !instance_data[instance].slm_config.enable_rx)) {
            for (i = 0; i < mep_caps.mesa_oam_peer_cnt; ++i)
                instance_data[instance].lm_start[i] = TRUE;  /* This is to know when the first LM counters are received. The first counters only used to get 'previous' frame counters initialized */
        }
        if (instance_data[instance].slm_config.enable || conf->enable)  /* Only run config when initiator enable */
            instance_data[instance].event_flags |= EVENT_IN_SLM_CONFIG;
        if (conf->enable || conf->enable_rx)   instance_data[instance].slm_config = *conf;
        else                                   {instance_data[instance].slm_config.enable = FALSE; instance_data[instance].slm_config.enable_rx = FALSE;}
    }

//    vtss_mep_supp_run();         No longer asynchronious
    run_async_slm_config(instance);

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_slm_counters_get(const u32            instance,
                                   const u32            peer_idx,
                                   sl_service_counter_t *const counters)
{
    supp_instance_data_t  *data;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)         return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    data = &instance_data[instance];    /* Instance data reference */
    *counters = data->lm_state[peer_idx].sl_service_counter;

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_lm_counters_clear(const u32 instance, const vtss_mep_supp_clear_dir direction)
{
    supp_instance_data_t  *data;
    lm_counter_t          *lm_counter;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)         return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    data = &instance_data[instance];    /* Instance data reference */

    for (int peer_idx = 0; peer_idx < mep_caps.mesa_oam_peer_cnt; ++peer_idx) {

        lm_counter = &data->lm_state[peer_idx].lm_counter;

        if (direction == VTSS_MEP_SUPP_CLEAR_DIR_BOTH || direction == VTSS_MEP_SUPP_CLEAR_DIR_TX) {
            lm_counter->tx_msg_counter = 0;
            lm_counter->tx_rsp_counter = 0;
        }
        if (direction == VTSS_MEP_SUPP_CLEAR_DIR_BOTH || direction == VTSS_MEP_SUPP_CLEAR_DIR_RX) {
            lm_counter->rx_msg_counter = 0;
            lm_counter->rx_rsp_counter = 0;
        }

        lm_counter->near_los_counter = 0;
        lm_counter->far_los_counter = 0;
        lm_counter->near_tx_counter = 0;
        lm_counter->far_tx_counter = 0;

        data->lm_state[peer_idx].meas_interval_tx_counter = 0;
    }

    uint32_t clear_mask = MESA_OAM_CNT_SEL | MESA_OAM_CNT_LM;
    if (direction == VTSS_MEP_SUPP_CLEAR_DIR_BOTH || direction == VTSS_MEP_SUPP_CLEAR_DIR_TX) {
        clear_mask |= MESA_OAM_CNT_DIR_TX;
    }
    if (direction == VTSS_MEP_SUPP_CLEAR_DIR_BOTH || direction == VTSS_MEP_SUPP_CLEAR_DIR_RX) {
        clear_mask |= MESA_OAM_CNT_DIR_RX;
    }
    (void)mesa_oam_voe_counter_clear(NULL, data->voe_idx, clear_mask);    /* Clear selected OAM counter in case of LM tx counting */

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_aps_conf_set(const u32                         instance,
                               const vtss_mep_supp_aps_conf_t    *const conf)
{
    u32 retval;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                                                 return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (!instance_data[instance].aps_config.enable && !conf->enable)                           return(VTSS_MEP_SUPP_RC_OK);
    if (!memcmp(&instance_data[instance].aps_config, conf, sizeof(vtss_mep_supp_aps_conf_t)))  return(VTSS_MEP_SUPP_RC_OK);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    retval = VTSS_MEP_SUPP_RC_OK;
    if (conf->enable)      retval = run_sync_aps_config(instance);
    if (retval == VTSS_MEP_SUPP_RC_OK)
    {
        instance_data[instance].event_flags |= EVENT_IN_APS_CONFIG;
        if (conf->enable)   instance_data[instance].aps_config = *conf;
        else                instance_data[instance].aps_config.enable = FALSE;
    }

    run_async_aps_config(instance);

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(retval);
}

u32 vtss_mep_supp_aps_txdata_set(const u32     instance,
                                 const u8      *txdata,
                                 const BOOL    event)
{
    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                                                 {vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__); return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);}

    instance_data[instance].event_flags |= EVENT_IN_APS_TXDATA;
    instance_data[instance].aps_event = event;
    if (!event)     memcpy(instance_data[instance].aps_txdata, txdata, VTSS_MEP_SUPP_RAPS_DATA_LENGTH);

//    vtss_mep_supp_run();         No longer asynchronious
    run_aps_txdata(instance);
    instance_data[instance].voe_config = FALSE;

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_raps_transmission(const u32    instance,
                                    const BOOL   enable)
{
    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)        {vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__); return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);}
    if (instance_data[instance].aps_tx == enable)     {vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__); return(VTSS_MEP_SUPP_RC_OK);}

    instance_data[instance].aps_tx = enable;

    if (enable)     run_aps_txdata(instance);
    instance_data[instance].voe_config = FALSE;

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_raps_forwarding(const u32    instance,
                                  const BOOL   enable)
{
    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)             {vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__); return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);}
    if (!instance_data[instance].config.voe)               {vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__); return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);}
    if (instance_data[instance].aps_forward == enable)     {vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__); return(VTSS_MEP_SUPP_RC_OK);}

    instance_data[instance].event_flags |= EVENT_IN_APS_FORWARD;
    instance_data[instance].aps_forward = enable;

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    vtss_mep_supp_run();

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_ltm_conf_set(const u32                         instance,
                               const vtss_mep_supp_ltm_conf_t    *const conf)
{
    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                                return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (!instance_data[instance].ltm_config.enable && !conf->enable)          return(VTSS_MEP_SUPP_RC_OK);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    instance_data[instance].event_flags |= EVENT_IN_LTM_CONFIG;
    if (conf->enable)   instance_data[instance].ltm_config = *conf;
    else                instance_data[instance].ltm_config.enable = FALSE;

    vtss_mep_supp_run();

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_ltr_get(const u32            instance,
                          u32                  *const count,
                          vtss_mep_supp_ltr_t  ltr[VTSS_MEP_SUPP_LTR_MAX])
{
    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)               return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (!instance_data[instance].ltm_config.enable)          return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    *count = instance_data[instance].ltr_cnt;
    memcpy(ltr, instance_data[instance].ltr, sizeof(vtss_mep_supp_ltr_t) * instance_data[instance].ltr_cnt);
    instance_data[instance].ltr_cnt = 0;

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_lbm_conf_set(const u32                         instance,
                               const vtss_mep_supp_lbm_conf_t    *const conf,
                                     u64                         *active_time_ms)
{
    supp_instance_data_t  *data;
    BOOL                  old_enable, new_enable;
    u32                   rc=VTSS_MEP_SUPP_RC_OK;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)          return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (test_check(conf->tests))                        return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    data = &instance_data[instance];    /* Instance data reference */

    old_enable = test_enable_calc(data->lbm_config.tests);
    new_enable = test_enable_calc(conf->tests);

    if (old_enable && (data->lbm_config.to_send != 0) && new_enable)                                   return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (!old_enable && !new_enable)                                                                    return(VTSS_MEP_SUPP_RC_OK);
    if (!memcmp(&data->lbm_config, conf, sizeof(data->lbm_config)))                                    return(VTSS_MEP_SUPP_RC_OK);
    if (test_enable_more_calc(conf->tests) && (conf->to_send != VTSS_MEP_SUPP_LBM_TO_SEND_INFINITE))   return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    data->lbm_start = (new_enable && !old_enable) ? TRUE : FALSE;

    if (new_enable)   data->lbm_config = *conf;
    else              test_enable_clear(data->lbm_config.tests);

    if (conf->to_send == VTSS_MEP_SUPP_LBM_TO_SEND_INFINITE) {
        rc = run_lbm_config(instance, active_time_ms);  /* In case of INFINITE, call is done synchronously to get error code from AFI allocation. Bugzilla#20885 */
    } else {
        data->event_flags |= EVENT_IN_LBM_CONFIG;   /* SW injected is done asynchronously as callback to upper logic my happen in case of multiple Up-MEP in same flow */
        vtss_mep_supp_run();
    }

    data->voe_config = FALSE;

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(rc);
}

u32 vtss_mep_supp_lbr_get(const u32            instance,
                          u32                  *const count,
                          vtss_mep_supp_lbr_t  lbr[VTSS_MEP_SUPP_LBR_MAX])
{
    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                         return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (!test_enable_calc(instance_data[instance].lbm_config.tests))   return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    *count = instance_data[instance].lbr_cnt;
    memcpy(lbr, instance_data[instance].lbr, sizeof(vtss_mep_supp_lbr_t) * instance_data[instance].lbr_cnt);
    instance_data[instance].lbr_cnt = 0;

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_lb_status_get(const u32                  instance,
                                vtss_mep_supp_lb_status_t  *const status)
{
    supp_instance_data_t  *data;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)               return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    data = &instance_data[instance];    /* Instance data reference */

    *status = data->lb_state;
    status->trans_id += 1;

    if (mep_caps.mesa_mep_serval) {
        mesa_oam_voe_counter_t voe_counter;
        mesa_oam_proc_status_t voe_status;

        if (data->voe_idx < mep_caps.mesa_oam_voe_cnt) {
            if (mesa_oam_voe_counter_get(NULL, data->voe_idx, &voe_counter) != VTSS_RC_OK) {
                vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
                return(VTSS_MEP_SUPP_RC_VOE_ERROR);
            }
            status->lbr_counter = voe_counter.lb.rx_lbr;
            status->lbm_counter = voe_counter.lb.tx_lbm;
            status->oo_counter = voe_counter.lb.rx_lbr_trans_id_err;

            (void)mesa_oam_proc_status_get(NULL, data->voe_idx, &voe_status);
            status->trans_id = voe_status.tx_next_lbm_transaction_id + 1;
        }
    }
    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_lb_clear(const u32    instance)
{
    supp_instance_data_t  *data;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                 return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    data = &instance_data[instance];    /* Instance data reference */

    data->lb_state.lbr_counter = 0;
    data->lb_state.lbm_counter = 0;
    data->lb_state.oo_counter = 0;

    if (mep_caps.mesa_mep_serval) {
        if (data->voe_idx < mep_caps.mesa_oam_voe_cnt) { /* On a VOE the counters need to be cleared */
            (void)mesa_oam_voe_counter_clear(NULL, data->voe_idx, MESA_OAM_CNT_LB);
        }
    }

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_tst_conf_set(const u32                         instance,
                               const vtss_mep_supp_tst_conf_t    *const conf,
                                     u64                         *active_time_ms)
{
    supp_instance_data_t     *data;
    BOOL                     old_tst_enable, new_tst_enable;
    u32                      rc;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)          return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (test_check(conf->tests))                        return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    data = &instance_data[instance];    /* Instance data reference */

    old_tst_enable = test_enable_calc(data->tst_config.tests);
    new_tst_enable = test_enable_calc(conf->tests);

    if (!old_tst_enable && !new_tst_enable && !data->tst_config.enable_rx && !conf->enable_rx)   return(VTSS_MEP_SUPP_RC_OK);
    if (!memcmp(&data->tst_config, conf, sizeof(data->tst_config)))                              return(VTSS_MEP_SUPP_RC_OK);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    data->tst_start = ((new_tst_enable && !old_tst_enable) || (conf->enable_rx && !data->tst_config.enable_rx)) ? TRUE : FALSE;
    if (new_tst_enable || conf->enable_rx)   data->tst_config = *conf;
    else {
        test_enable_clear(data->tst_config.tests);
        data->tst_config.enable_rx = FALSE;
    }

    rc = run_tst_config(instance, active_time_ms);
    data->voe_config = FALSE;

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(rc);
}

u32 vtss_mep_supp_tst_status_get(const u32                   instance,
                                 vtss_mep_supp_tst_status_t  *const status)
{
    supp_instance_data_t   *data;
    u64                    tx_counter=0;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                 return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    data = &instance_data[instance];    /* Instance data reference */

    memset(status, 0, sizeof(*status));

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    if (test_enable_calc(data->tst_config.tests) && data->tx_tst[0][0]) {  /* If test is ongoing we return currently transmitted frame count - this is not accurate. The accurate value is obtained when test is stopped */
        vtss_mep_supp_packet_tx_frm_cnt(data->tx_tst[0][0], &tx_counter);
    }

    status->tx_counter = tx_counter;

    if (mep_caps.mesa_mep_serval) {
        mesa_oam_voe_counter_t voe_counter;
        mesa_evc_counters_t    evc_counters;

        if (data->voe_idx < mep_caps.mesa_oam_voe_cnt) {
            if (mesa_oam_voe_counter_get(NULL, data->voe_idx, &voe_counter) == VTSS_RC_OK) {
                status->tx_counter = voe_counter.tst.tx_tst;
                status->rx_counter = voe_counter.tst.rx_tst;
                status->oo_counter = voe_counter.tst.rx_tst_trans_id_err;
            }
        } else { /* SW MEP on Serval. Get TST TX count through MCE ISDX counters. */
            status->rx_counter = 0;
            status->oo_counter = 0;
            if (data->tx_isdx_tst != VTSS_MEP_SUPP_INDX_INVALID) {
                if (mesa_mce_counters_get(NULL, MCE_ID_CALC_TST(instance, data->config.port),  data->config.port,  &evc_counters) == VTSS_RC_OK) {
                    status->tx_counter = (data->config.direction == VTSS_MEP_SUPP_UP) ? (evc_counters.rx_green.frames + evc_counters.rx_yellow.frames + evc_counters.rx_discard.frames) :
                                                                                        (evc_counters.tx_green.frames + evc_counters.tx_yellow.frames + evc_counters.tx_discard.frames);
                }
            } else {
                status->tx_counter = data->tst_tx_cnt;
            }
        }






    }

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}


u32 vtss_mep_supp_tst_clear(const u32    instance)
{
    supp_instance_data_t   *data;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                 return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    data = &instance_data[instance];    /* Instance data reference */

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    data->tst_tx_cnt = 0;
    data->tst_rx_cnt = 0;

    if (mep_caps.mesa_mep_serval) {
        mesa_oam_voe_conf_t   cfg;

        if (data->voe_idx < mep_caps.mesa_oam_voe_cnt) { /* On a VOE the counters need to be cleared and arm to receive the next TST PDU */
            (void)mesa_oam_voe_counter_clear(NULL, data->voe_idx, MESA_OAM_CNT_TST);
            if (data->tst_config.enable_rx && mesa_oam_voe_conf_get(NULL, data->voe_idx, &cfg) == VTSS_RC_OK) {
                cfg.tst.copy_to_cpu = TRUE;
                (void)mesa_oam_voe_conf_set(NULL, data->voe_idx, &cfg);
            }
        } else {
            (void)mesa_mce_counters_clear(NULL, MCE_ID_CALC_TST(instance, data->config.port),  data->config.port);
        }
    }

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}


u32 vtss_mep_supp_dmm_conf_set(const u32                        instance,
                               const vtss_mep_supp_dmm_conf_t   *const conf)
{
    supp_instance_data_t  *data;
    BOOL                  old_enable, new_enable, dm1_enable;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                                                   return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    data = &instance_data[instance];    /* Instance data reference */

    old_enable = dm_enable_calc(data->dmm_config.enable);
    new_enable = dm_enable_calc(conf->enable);
    dm1_enable = dm_enable_calc(data->dm1_config.enable);

    vtss_mep_supp_trace("vtss_mep_supp_dmm_conf_set  instance  old_enable  new_enable", instance, old_enable, new_enable, 0);

    if (dm1_enable)                                                   return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (!old_enable && !new_enable)                                   return(VTSS_MEP_SUPP_RC_OK);
    if (!memcmp(&data->dmm_config, conf, sizeof(data->dmm_config)))   return(VTSS_MEP_SUPP_RC_OK);
    if (mep_caps.mesa_mep_serval) {
        if (new_enable && conf->proprietary)                          return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    }

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    data->event_flags |= EVENT_IN_DMM_CONFIG;
    if (new_enable)   data->dmm_config = *conf;
    else              dm_enable_clear(data->dmm_config.enable);

    vtss_mep_supp_run();

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_dmm_conf_get(const u32                  instance,
                               vtss_mep_supp_dmm_conf_t   *const conf)
{
    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
    *conf = instance_data[instance].dmm_config;
    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_dm1_conf_set(const u32                        instance,
                               const vtss_mep_supp_dm1_conf_t   *const conf)
{
    supp_instance_data_t  *data;
    BOOL                  dm1_enable, dmm_enable;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                                                   return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    data = &instance_data[instance];    /* Instance data reference */

    dm1_enable = dm_enable_calc(conf->enable);
    dmm_enable = dm_enable_calc(data->dmm_config.enable);

    if (dmm_enable)                                                   return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (!memcmp(&data->dm1_config, conf, sizeof(data->dm1_config)))   return(VTSS_MEP_SUPP_RC_OK);
    if (mep_caps.mesa_mep_serval) {
        if (dm1_enable && conf->proprietary)                          return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    }

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    data->event_flags |= EVENT_IN_1DM_CONFIG;
    if (dm1_enable)   data->dm1_config = *conf;
    else {
        dm_enable_clear(data->dm1_config.enable);
        data->dm1_config.tunit = conf->tunit;    /* This is only in order to change the time unit for 1DM reception - 1DM is not enabled  */
    }

    vtss_mep_supp_run();

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_dm1_conf_get(const u32                  instance,
                               vtss_mep_supp_dm1_conf_t   *const conf)
{
    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
    *conf = instance_data[instance].dm1_config;
    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

static void dm_delay_copy(u32 *rx_count,  u32 out_dm[VTSS_MEP_SUPP_DM_MAX],  u32 in_dm[VTSS_MEP_SUPP_DM_MAX])
{
    u32 postition, num;

    if (*rx_count > VTSS_MEP_SUPP_DM_MAX) {  /* The delay buffer has wrapped - copy the delays in out_dm in correct order */
        postition = *rx_count % VTSS_MEP_SUPP_DM_MAX;
        num = VTSS_MEP_SUPP_DM_MAX - postition;

        memcpy(out_dm, &in_dm[postition], sizeof(u32) * num);
        memcpy(&out_dm[num], in_dm, sizeof(u32) * postition);

        *rx_count = VTSS_MEP_SUPP_DM_MAX;
    } else {
        memcpy(out_dm, in_dm, sizeof(u32) * (*rx_count));
    }
}


u32 vtss_mep_supp_dmr_get(const u32                 instance,
                          vtss_mep_supp_dmr_status  *const status)
{
    u32                            i;
    supp_instance_data_t           *data;
    dm_state_t                     *state;
    vtss_mep_supp_dmr_prio_status  *prio_status;

    memset(status, 0, sizeof(*status));

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)             return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    data = &instance_data[instance];    /* Instance data reference */

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {
        state = &data->dm_state[i];
        prio_status = &status->prio_status[i];

        dm_delay_copy(&state->tw_cnt,  prio_status->tw_delays,  state->tw_delays);   /* Copy TW delay to status */

        prio_status->tx_counter = state->dm_tx_cnt;     /* Copy TW counters to status */
        prio_status->rx_counter = state->tw_cnt;
        prio_status->rx_tout_counter = state->dmr_rx_tout_cnt;
        prio_status->tw_rx_err_counter = state->tw_rx_err_cnt;

        if (data->dmm_config.synchronized == TRUE) {    /* If synchronized copy NF and FN delays to status */
            dm_delay_copy(&state->nf_cnt,  prio_status->nf_delays,  state->nf_delays);   /* Copy TW delay to status */
            dm_delay_copy(&state->fn_cnt,  prio_status->fn_delays,  state->fn_delays);   /* Copy TW delay to status */

            prio_status->nf_rx_err_counter = state->nf_rx_err_cnt;  /* Copy NF and FN counters to status */
            prio_status->fn_rx_err_counter = state->fn_rx_err_cnt;
        }

        state->dm_tx_cnt = 0;       /* Clear counters */
        state->tw_cnt = 0;
        state->nf_cnt = 0;
        state->fn_cnt = 0;
        state->dmr_rx_tout_cnt = 0;
        state->tw_rx_err_cnt = 0;
        state->nf_rx_err_cnt = 0;
        state->fn_rx_err_cnt = 0;
    }

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}


u32 vtss_mep_supp_dm1_get(const u32                        instance,
                          vtss_mep_supp_dm1_status *const  status)
{
    u32                            i;
    supp_instance_data_t           *data;
    dm_state_t                     *state;
    vtss_mep_supp_dm1_prio_status  *prio_status;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)      return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    data = &instance_data[instance];    /* Instance data reference */

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    memset(status, 0, sizeof(*status));

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {
        state = &data->dm_state[i];
        prio_status = &status->prio_status[i];

        dm_delay_copy(&state->fn_cnt,  prio_status->delays,  state->fn_delays);   /* Copy FN delay to status */

        prio_status->tx_counter = state->dm_tx_cnt;     /* Copy counters to status */
        prio_status->rx_counter = state->fn_cnt;
        prio_status->rx_err_counter = state->fn_rx_err_cnt;

        state->dm_tx_cnt = 0;       /* Clear counters */
        state->fn_cnt = 0;
        state->fn_rx_err_cnt = 0;
    }

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}


u32 vtss_mep_supp_dmr_timestamps_get(const u32                instance,
                                     mep_supp_dm_timestamp_t  *const dm1_timestamp_far_to_near,
                                     mep_supp_dm_timestamp_t  *const dm1_timestamp_near_to_far)
{
    u32                     i;
    supp_instance_data_t    *data;

    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                                  return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    data = &instance_data[instance];    /* Instance data reference */

    for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Find the first enabled priority - this will only work with one priority enabled !!! */
        if (data->dmm_config.enable[i])    break;
    }

    if (i == VTSS_MEP_SUPP_PRIO_MAX)                                            return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if ((data->dmm_config.synchronized != TRUE) || (data->dmr_ts_ok != TRUE) ||
        (data->dm_state[i].tx_dmm_fup_timestamp.seconds == 0))                  return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    dm1_timestamp_near_to_far->tx_time = data->dm_state[i].tx_dmm_fup_timestamp;
    dm1_timestamp_near_to_far->rx_time = data->rxTimef;
    dm1_timestamp_far_to_near->tx_time = data->txTimeb;
    dm1_timestamp_far_to_near->rx_time = data->dm_state[i].rx_dmr_timestamp;

    instance_data[instance].dmr_ts_ok = FALSE;

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}


u32 vtss_mep_supp_ais_conf_set(const u32                        instance,
                               const vtss_mep_supp_ais_conf_t   *const conf)
{
    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)      return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    instance_data[instance].ais_config = *conf;

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_ais_set(const u32  instance,
                          const BOOL enable)
{
    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                           return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (enable && instance_data[instance].ais_enable)                    return(VTSS_MEP_SUPP_RC_OK);
    if (enable && !instance_data[instance].ais_config.flow_count)        return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    instance_data[instance].event_flags |= EVENT_IN_AIS_SET;
    instance_data[instance].ais_enable = enable;

    vtss_mep_supp_run();

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_lck_conf_set(const u32                        instance,
                               const vtss_mep_supp_lck_conf_t   *const conf)
{
    if (instance >= VTSS_MEP_SUPP_CREATED_MAX)                                                      return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);
    if (!instance_data[instance].lck_config.enable && !conf->enable)                                return(VTSS_MEP_SUPP_RC_OK);
    if (!memcmp(&instance_data[instance].lck_config, conf, sizeof(vtss_mep_supp_lck_conf_t)))       return(VTSS_MEP_SUPP_RC_OK);
    if (conf->enable && (conf->flow_count > VTSS_MEP_SUPP_CLIENT_FLOWS_MAX))                        return(VTSS_MEP_SUPP_RC_OK);
    if (conf->enable && (conf->flow_count == 0))                                                    return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    instance_data[instance].event_flags |= EVENT_IN_LCK_CONFIG;
    if (conf->enable)   instance_data[instance].lck_config = *conf;
    else                instance_data[instance].lck_config.enable = FALSE;

    vtss_mep_supp_run();

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}

u32 vtss_mep_supp_loc_state(u32 instance,   BOOL state)
{
/* This is called when LOC state has changed by SW poll of ACL */
    BOOL new_state;

    supp_instance_data_t  *data;

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    data = &instance_data[instance];    /* Instance data reference */

    if ((data->ccm_gen.enable) && hw_ccm_calc(data->ccm_config.rate))
    {
        new_state = (state) ? 0x02 : 0x00;
        if (new_state != (data->defect_state.dLoc[0] & 0x02))
        { /* Change in hw detected LOC */
            if (new_state)  data->defect_state.dLoc[0] |= 0x2;
            else            data->defect_state.dLoc[0] &= ~0x2;

            if (data->defect_state.dLoc[0])    data->defect_state.dRdi[0] = FALSE;
            data->event_flags |= EVENT_OUT_NEW_DEFECT;

            vtss_mep_supp_run();
        }
    }

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return(VTSS_MEP_SUPP_RC_OK);
}


BOOL vtss_mep_supp_hw_ccm_generator_get(vtss_mep_supp_rate_t  rate)
{
    BOOL hw_gen;

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    hw_gen = hw_ccm_calc(rate);

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);

    return (hw_gen);
}

void vtss_mep_supp_protection_change(u32 instance)
{
    u32 i, j;
    supp_instance_data_t  *data;

    /* Port protection has changed meaning AFI generation might be moved to other port - This is only Serval down MEP */

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    data = &instance_data[instance];    /* Instance data reference */

    if (!data->config.direction == VTSS_MEP_SUPP_DOWN) {
        vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
        return;
    }

    if ((data->ccm_gen.enable) && hw_ccm_calc(data->ccm_gen.cc_rate)) {  /* AFI based CCM is enabled - restart */
        data->event_flags |= EVENT_IN_CCM_GEN;
        vtss_mep_supp_run();
    }
    if (test_enable_calc(data->lbm_config.tests) && (data->lbm_config.to_send == VTSS_MEP_SUPP_LBM_TO_SEND_INFINITE))  /* AFI based LBM is enabled - restart */
    {
        for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Configure any enabled LBM */
            for (j=0; j<VTSS_MEP_SUPP_TEST_MAX; ++j) {
                if (data->lbm_config.tests[i][j].enable && (data->tx_lbm[i][j] != NULL)) { /* LBM enable */
                    /* TX is enabled */
                    vtss_mep_supp_packet_tx_cancel(instance, data->tx_lbm[i][j], NULL);
                    vtss_mep_supp_packet_tx_free(data->tx_lbm[i][j]);
                    data->tx_lbm[i][j] = NULL;
                }
            }
        }
        data->event_flags |= EVENT_IN_LBM_CONFIG;
        vtss_mep_supp_run();
    }
    if (test_enable_calc(data->tst_config.tests))  {    /* AFI based TST is enabled - restart */
        for (i=0; i<VTSS_MEP_SUPP_PRIO_MAX; ++i) {  /* Configure any enabled LBM */
            for (j=0; j<VTSS_MEP_SUPP_TEST_MAX; ++j) {
                if (data->tst_config.tests[i][j].enable && (data->tx_tst[i][j] != NULL)) { /* TST enable */
                    /* TX is enabled */
                    vtss_mep_supp_packet_tx_cancel(instance, data->tx_tst[i][j], NULL);
                    vtss_mep_supp_packet_tx_free(data->tx_tst[i][j]);
                    data->tx_tst[i][j] = NULL;
                }
            }
        }
        data->event_flags |= EVENT_IN_TST_CONFIG;
        vtss_mep_supp_run();
    }

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
}

/****************************************************************************/
/*  MEP platform interface                                                  */
/****************************************************************************/

void vtss_mep_supp_timer_thread(BOOL  *const stop)
{
    u32  i, j;
    BOOL run;
    supp_instance_data_t  *data;

    run = FALSE;
    *stop = TRUE;

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    for (i=0; i<VTSS_MEP_SUPP_CREATED_MAX; ++i)
    {
        if (!instance_data[i].config.enable)    continue;

        data = &instance_data[i];    /* Instance data reference */

        if (data->defect_timer_active)
        {
            if (data->dLevel.timer)
            {
                data->dLevel.timer--;
                if (!data->dLevel.timer)
                {
                    data->event_flags |= EVENT_IN_DEFECT_TIMER;
                    run = TRUE;
                }
                else  *stop = FALSE;
            }
            if (data->dMeg.timer)
            {
                data->dMeg.timer--;
                if (!data->dMeg.timer)
                {
                    data->event_flags |= EVENT_IN_DEFECT_TIMER;
                    run = TRUE;
                }
                else  *stop = FALSE;
            }
            if (data->dMep.timer)
            {
                data->dMep.timer--;
                if (!data->dMep.timer)
                {
                    data->event_flags |= EVENT_IN_DEFECT_TIMER;
                    run = TRUE;
                }
                else  *stop = FALSE;
            }
            for (j=0; j<data->ccm_config.peer_count; ++j)
            {
                if (data->dPeriod[j].timer)
                {
                    data->dPeriod[j].timer--;
                    if (!data->dPeriod[j].timer)
                    {
                        data->event_flags |= EVENT_IN_DEFECT_TIMER;
                        run = TRUE;
                    }
                    else  *stop = FALSE;
                }
                if (data->dPrio[j].timer)
                {
                    data->dPrio[j].timer--;
                    if (!data->dPrio[j].timer)
                    {
                        data->event_flags |= EVENT_IN_DEFECT_TIMER;
                        run = TRUE;
                    }
                    else  *stop = FALSE;
                }
            }
            if (data->dLoop.timer)
            {
                data->dLoop.timer--;
                if (!data->dLoop.timer)
                {
                    data->event_flags |= EVENT_IN_DEFECT_TIMER;
                    run = TRUE;
                }
                else  *stop = FALSE;
            }
            if (data->dConfig.timer)
            {
                data->dConfig.timer--;
                if (!data->dConfig.timer)
                {
                    data->event_flags |= EVENT_IN_DEFECT_TIMER;
                    run = TRUE;
                }
                else  *stop = FALSE;
            }
            if (data->dAis.timer)
            {
                data->dAis.timer--;
                if (!data->dAis.timer)
                {
                    data->event_flags |= EVENT_IN_DEFECT_TIMER;
                    run = TRUE;
                }
                else  *stop = FALSE;
            }
            if (data->dLck.timer)
            {
                data->dLck.timer--;
                if (!data->dLck.timer)
                {
                    data->event_flags |= EVENT_IN_DEFECT_TIMER;
                    run = TRUE;
                }
                else  *stop = FALSE;
            }
        }/* Defect active */
        for (j=0; j<data->ccm_config.peer_count; ++j) {
            if (data->rx_ccm_timer[j]) {
                data->rx_ccm_timer[j]--;
                if (!data->rx_ccm_timer[j]) {
                    data->event_flags |= EVENT_IN_RX_CCM_TIMER;
                    run = TRUE;
                }
                else  *stop = FALSE;
            }
        }
        if (data->tx_ccm_timer)
        {
            data->tx_ccm_timer--;
            if (!data->tx_ccm_timer)
            {
                data->event_flags |= EVENT_IN_TX_CCM_TIMER;
                run = TRUE;
            }
            else  *stop = FALSE;
        }
        if (data->tx_aps_timer)
        {
            data->tx_aps_timer--;
            if (!data->tx_aps_timer)
            {
                data->event_flags |= EVENT_IN_TX_APS_TIMER;
                run = TRUE;
            }
            else  *stop = FALSE;
        }
        if (data->tx_lmm_timer)
        {
            data->tx_lmm_timer--;
            if (!data->tx_lmm_timer)
            {
                data->event_flags |= EVENT_IN_TX_LMM_TIMER;
                run_tx_lmm_timer(i);      // LM Timeout is now synchronous
                //run = TRUE;
            }
            else  *stop = FALSE;
        }
        if (data->tx_lbm_timer)
        {
            data->tx_lbm_timer--;
            if (!data->tx_lbm_timer)
            {
                data->event_flags |= EVENT_IN_TX_LBM_TIMER;
                run = TRUE;
            }
            else  *stop = FALSE;
        }
        if (data->tx_dmm_timer)
        {
            data->tx_dmm_timer--;
            if (!data->tx_dmm_timer)
            {
                data->event_flags |= EVENT_IN_TX_DMM_TIMER;
                run = TRUE;
            }
            else  *stop = FALSE;
        }
        if (data->rx_dmr_timeout_timer)
        {
            data->rx_dmr_timeout_timer--;
            if (!data->rx_dmr_timeout_timer)
            {
                data->event_flags |= EVENT_IN_RX_DMR_TIMER;
                run = TRUE;
            }
            else  *stop = FALSE;
        }
        if (data->tx_dm1_timer)
        {
            data->tx_dm1_timer--;
            if (!data->tx_dm1_timer)
            {
                data->event_flags |= EVENT_IN_TX_1DM_TIMER;
                run = TRUE;
            }
            else  *stop = FALSE;
        }
        if (data->tx_ais_timer)
        {
            data->tx_ais_timer--;
            if (!data->tx_ais_timer)
            {
                data->event_flags |= EVENT_IN_TX_AIS_TIMER;
                run = TRUE;
            }
            else *stop = FALSE;
        }
        if (data->tx_lck_timer)
        {
            data->tx_lck_timer--;
            if (!data->tx_lck_timer)
            {
                data->event_flags |= EVENT_IN_TX_LCK_TIMER;
                run = TRUE;
            }
            else *stop = FALSE;
        }
        if (data->rx_aps_timer)
        {
            data->rx_aps_timer--;
            if (!data->rx_aps_timer)
            {
                data->event_flags |= EVENT_IN_RX_APS_TIMER;
                run = TRUE;
            }
            else  *stop = FALSE;
        }
        if (data->rx_lbm_timer)
        {
            data->rx_lbm_timer--;
            if (!data->rx_lbm_timer)
            {
                data->event_flags |= EVENT_IN_RX_LBM_TIMER;
                run = TRUE;
            }
            else  *stop = FALSE;
        }
        if (data->rdi_timer)
        {
            data->rdi_timer--;
            if (!data->rdi_timer)
            {
                data->event_flags |= EVENT_IN_RDI_TIMER;
                run = TRUE;
            }
            else  *stop = FALSE;
        }
        if (data->tst_timer)
        {
            data->tst_timer--;
            if (!data->tst_timer)
            {
                data->event_flags |= EVENT_IN_TST_TIMER;
                run = TRUE;
            }
            else  *stop = FALSE;
        }








































    }

    if (run)    vtss_mep_supp_run();

    if (mep_caps.mesa_mep_serval) {
        static u32 voe_fast_poll_timer = 0;
        static u32 voe_slow_poll_timer = 0;
        if (++voe_fast_poll_timer >= (100/timer_res)) {  /* VOE "fast" polling */
            voe_fast_poll_timer = 0;
            for (i=0; i<mep_caps.mesa_oam_voe_cnt; ++i) {
                if ((voe_to_mep[i] < VTSS_MEP_SUPP_CREATED_MAX) && voe_ccm_enable(&instance_data[voe_to_mep[i]])) { /* This is a VOE based CCM - VOE event is utilized */
                    if (instance_data[voe_to_mep[i]].ccm_defect_active)
                        (void)mesa_oam_voe_arm_hitme(NULL, i, TRUE);    /* CCM defect is active - CCM PDU is polled "fast" in order to check for defect going passive */
                }
                if (voe_to_mep[i] < VTSS_MEP_SUPP_CREATED_MAX) /* This is a active VOE - do the one-bit counter accumulation on Serval-1 */
                    (void)mesa_oam_voe_counter_update_serval(NULL, i);
            }
        }

        if (++voe_slow_poll_timer >= (1000/timer_res)) {  /* VOE "slow" polling */
            voe_slow_poll_timer = 0;
            for (i=0; i<mep_caps.mesa_oam_voe_cnt; ++i) {
                if ((voe_to_mep[i] < VTSS_MEP_SUPP_CREATED_MAX) && voe_ccm_enable(&instance_data[voe_to_mep[i]])) { /* This is a VOE based CCM - VOE event is utilized */
                    if (!instance_data[voe_to_mep[i]].ccm_defect_active)
                        (void)mesa_oam_voe_arm_hitme(NULL, i, TRUE);   /* No CCM defect is active - CCM PDU is polled "slow" in order to learn Peer MAC */
                    (void)mesa_oam_voe_event_enable(NULL, i, voe_event_mask, TRUE); /* Enable all VOE CCM events */
                }
            }
        }

        *stop = FALSE;
    }
    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
}

static u8  null_aps[VTSS_MEP_SUPP_RAPS_DATA_LENGTH] = {0,0,0,0,0,0,0,0};
void vtss_mep_supp_run_thread(void)
{
    u32 i;

    vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);

    for (i=0; i<VTSS_MEP_SUPP_CREATED_MAX; ++i) {

        if (instance_data[i].event_flags & EVENT_IN_MASK)
        {/* New input */
            if (instance_data[i].event_flags & EVENT_IN_CONFIG)         (void)run_async_config(i);
            if (instance_data[i].event_flags & EVENT_IN_CCM_CONFIG)     run_ccm_config(i);
            if (instance_data[i].event_flags & EVENT_IN_CCM_GEN)        run_async_ccm_gen(i);
            if (instance_data[i].event_flags & EVENT_IN_CCM_RDI)        run_ccm_rdi(i);
            if (instance_data[i].event_flags & EVENT_IN_LMM_CONFIG)     run_async_lmm_config(i);
            if (instance_data[i].event_flags & EVENT_IN_SLM_CONFIG)     run_async_slm_config(i);
            if (instance_data[i].event_flags & EVENT_IN_DMM_CONFIG)     run_dmm_config(i);
            if (instance_data[i].event_flags & EVENT_IN_1DM_CONFIG)     run_1dm_config(i);
            if (instance_data[i].event_flags & EVENT_IN_APS_CONFIG)     run_async_aps_config(i);
            if (instance_data[i].event_flags & EVENT_IN_APS_TXDATA)     run_aps_txdata(i);
            if (mep_caps.mesa_mep_serval) {
                if (instance_data[i].event_flags & EVENT_IN_APS_FORWARD) run_aps_forward(i);
            }
            if (instance_data[i].event_flags & EVENT_IN_LTM_CONFIG)     run_ltm_config(i);
            if (instance_data[i].event_flags & EVENT_IN_LBM_CONFIG)     (void)run_lbm_config(i, NULL);
            if (instance_data[i].event_flags & EVENT_IN_LCK_CONFIG)     run_lck_config(i);
            if (instance_data[i].event_flags & EVENT_IN_TST_CONFIG)     (void)run_tst_config(i, NULL);
            if (instance_data[i].event_flags & EVENT_IN_AIS_SET)        run_ais_set(i);
            if (instance_data[i].event_flags & EVENT_IN_DEFECT_TIMER)   run_defect_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_RX_CCM_TIMER)   run_rx_ccm_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_TX_CCM_TIMER)   run_tx_ccm_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_TX_APS_TIMER)   run_tx_aps_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_TX_LMM_TIMER)   run_tx_lmm_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_TX_LBM_TIMER)   run_tx_lbm_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_RX_DMR_TIMER)   run_rx_dmr_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_TX_DMM_TIMER)   run_tx_dmm_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_TX_1DM_TIMER)   run_tx_dm1_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_RX_APS_TIMER)   run_rx_aps_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_RX_LBM_TIMER)   run_rx_lbm_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_TX_AIS_TIMER)   run_tx_ais_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_TX_LCK_TIMER)   run_tx_lck_timer(i);
            if (instance_data[i].event_flags & EVENT_IN_RDI_TIMER)      run_rdi_timer(i);
            if (mep_caps.mesa_mep_serval) {
                if (instance_data[i].event_flags & EVENT_IN_TST_TIMER)  run_tst_timer(i);
            }






        }

        instance_data[i].voe_config = FALSE;

        if (instance_data[i].event_flags & EVENT_OUT_MASK)
        {/* New output */
            /* Now do unlocked call-out to avoid any deadlock */
            if (instance_data[i].event_flags & EVENT_OUT_NEW_DEFECT)
            {   /* Deliver new CCM state */
                instance_data[i].event_flags &= ~EVENT_OUT_NEW_DEFECT;

                vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
                vtss_mep_supp_new_defect_state(i);
                vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
            }
            if (instance_data[i].event_flags & EVENT_OUT_NEW_APS)
            {   /* Deliver new APS */
                instance_data[i].event_flags &= ~EVENT_OUT_NEW_APS;

                vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
                vtss_mep_supp_new_aps(i, null_aps);
                vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
            }
            if (instance_data[i].event_flags & EVENT_OUT_NEW_DMR)
            {   /* Deliver new DMR */
                instance_data[i].event_flags &= ~EVENT_OUT_NEW_DMR;

                vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
                vtss_mep_supp_new_dmr(i);
                vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
            }
            if (instance_data[i].event_flags & EVENT_OUT_NEW_DM1)
            {   /* Deliver new DM1 */
                instance_data[i].event_flags &= ~EVENT_OUT_NEW_DM1;

                vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
                vtss_mep_supp_new_dm1(i);
                vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
            }
            if (instance_data[i].event_flags & EVENT_OUT_TST_DONE)
            {   /* Deliver TST done */
                instance_data[i].event_flags &= ~EVENT_OUT_TST_DONE;

                vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
                vtss_mep_supp_tst_done(i);
                vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
            }
            if (instance_data[i].event_flags & EVENT_OUT_LBM_DONE)
            {   /* Deliver LBM done */
                instance_data[i].event_flags &= ~EVENT_OUT_LBM_DONE;

                vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
                vtss_mep_supp_lbm_done(i);
                vtss_mep_supp_loc_crit_lock(__VTSS_LOCATION__, __LINE__);
            }
        }
    }

    vtss_mep_supp_loc_crit_unlock(__VTSS_LOCATION__, __LINE__);
}

u32 vtss_mep_supp_init(const u32 timer_resolution, u32 up_mep_loop_port, u32 mep_pag, u32 mip_pag)
{
    u32  i;

    mesa_mac_t   multicast_addr = {{0x01,0x80,0xC2,0x00,0x00,0x30}};
    mesa_packet_rx_queue_t rx_queue = vtss_mep_oam_cpu_queue_get();
    mesa_oam_vop_conf_t cfg;

    supp_mep_pag = mep_pag;
    supp_mip_pag = mip_pag;
    if ((timer_resolution == 0) || (timer_resolution > 100))     return(VTSS_MEP_SUPP_RC_INVALID_PARAMETER);

    timer_res = timer_resolution;
    memset(lm_null_counter, 0, 12);
    init_pdu_period_to_timer();

    /* Initialize all instance data */
    for (i=0; i<VTSS_MEP_SUPP_CREATED_MAX; ++i)
        instance_data_clear(i,  &instance_data[i]);

    if (mep_caps.mesa_mep_serval) {
        for (i=0; i<mep_caps.mesa_oam_voe_cnt; ++i)
            voe_to_mep[i] = VTSS_MEP_SUPP_CREATED_MAX;

        (void)mesa_oam_vop_conf_get(NULL, &cfg);

        cfg.enable_all_voe = TRUE;
        cfg.external_cpu_portmask = 1 << up_mep_loop_port;
        memcpy(cfg.common_multicast_dmac.addr, multicast_addr.addr, sizeof(cfg.common_multicast_dmac.addr));
        cfg.ccm_lm_enable_rx_fcf_in_reserved_field = FALSE;
        cfg.down_mep_lmr_proprietary_fcf_use = TRUE;
        cfg.sdlb_cpy_copy_idx = 0;

        cfg.pdu_type.generic[GENERIC_AIS].opcode = MESA_OAM_OPCODE_AIS;
        cfg.pdu_type.generic[GENERIC_AIS].check_dmac = TRUE;
        cfg.pdu_type.generic[GENERIC_AIS].extract.to_front = FALSE;
        cfg.pdu_type.generic[GENERIC_AIS].extract.rx_queue = rx_queue;
        cfg.pdu_type.generic[GENERIC_LCK].opcode = MESA_OAM_OPCODE_LCK;
        cfg.pdu_type.generic[GENERIC_LCK].check_dmac = TRUE;
        cfg.pdu_type.generic[GENERIC_LCK].extract.to_front = FALSE;
        cfg.pdu_type.generic[GENERIC_LCK].extract.rx_queue = rx_queue;
        cfg.pdu_type.generic[GENERIC_LAPS].opcode = MESA_OAM_OPCODE_LINEAR_APS;
        cfg.pdu_type.generic[GENERIC_LAPS].check_dmac = TRUE;
        cfg.pdu_type.generic[GENERIC_LAPS].extract.to_front = FALSE;
        cfg.pdu_type.generic[GENERIC_LAPS].extract.rx_queue = rx_queue;
        cfg.pdu_type.generic[GENERIC_RAPS].opcode = MESA_OAM_OPCODE_RING_APS;
        cfg.pdu_type.generic[GENERIC_RAPS].check_dmac = FALSE;
        cfg.pdu_type.generic[GENERIC_RAPS].extract.to_front = FALSE;
        cfg.pdu_type.generic[GENERIC_RAPS].extract.rx_queue = rx_queue;
        cfg.pdu_type.generic[GENERIC_SLM].opcode = MESA_OAM_OPCODE_SLM;
        cfg.pdu_type.generic[GENERIC_SLM].check_dmac = FALSE;
        cfg.pdu_type.generic[GENERIC_SLM].extract.to_front = FALSE;
        cfg.pdu_type.generic[GENERIC_SLM].extract.rx_queue = rx_queue;
        cfg.pdu_type.generic[GENERIC_SLR].opcode = MESA_OAM_OPCODE_SLR;
        cfg.pdu_type.generic[GENERIC_SLR].check_dmac = FALSE;
        cfg.pdu_type.generic[GENERIC_SLR].extract.to_front = FALSE;
        cfg.pdu_type.generic[GENERIC_SLR].extract.rx_queue = rx_queue;
        cfg.pdu_type.generic[GENERIC_1SL].opcode = MESA_OAM_OPCODE_1SL;
        cfg.pdu_type.generic[GENERIC_1SL].check_dmac = FALSE;
        cfg.pdu_type.generic[GENERIC_1SL].extract.to_front = FALSE;
        cfg.pdu_type.generic[GENERIC_1SL].extract.rx_queue = rx_queue;
        cfg.pdu_type.ccm.to_front = FALSE;
        cfg.pdu_type.ccm.rx_queue = rx_queue;
        cfg.pdu_type.ccm_lm.to_front = FALSE;
        cfg.pdu_type.ccm_lm.rx_queue = rx_queue;
        cfg.pdu_type.lt.to_front = FALSE;
        cfg.pdu_type.lt.rx_queue = rx_queue;
        cfg.pdu_type.dmm.to_front = FALSE;
        cfg.pdu_type.dmm.rx_queue = rx_queue;
        cfg.pdu_type.dmr.to_front = FALSE;
        cfg.pdu_type.dmr.rx_queue = rx_queue;
        cfg.pdu_type.lmm.to_front = FALSE;
        cfg.pdu_type.lmm.rx_queue = rx_queue;
        cfg.pdu_type.lmr.to_front = FALSE;
        cfg.pdu_type.lmr.rx_queue = rx_queue;
        cfg.pdu_type.lbm.to_front = FALSE;
        cfg.pdu_type.lbm.rx_queue = rx_queue;
        cfg.pdu_type.lbr.to_front = FALSE;
        cfg.pdu_type.lbr.rx_queue = rx_queue - 1; // TST and LBR frames go into a lower prioritized queue in order not to disturb 1DM/DMR frames.
        cfg.pdu_type.err.to_front = FALSE;
        cfg.pdu_type.err.rx_queue = rx_queue;
        cfg.pdu_type.other.to_front = FALSE;
        cfg.pdu_type.other.rx_queue = rx_queue;

        (void)mesa_oam_vop_conf_set(NULL,  &cfg);
    }

    voe_event_mask = (mep_caps.mesa_oam_event_ccm_period      | mep_caps.mesa_oam_event_ccm_priority |
                      mep_caps.mesa_oam_event_ccm_zero_period | mep_caps.mesa_oam_event_ccm_rx_rdi   |
                      mep_caps.mesa_oam_event_ccm_loc         | mep_caps.mesa_oam_event_ccm_mep_id   |
                      mep_caps.mesa_oam_event_ccm_meg_id);





    return(VTSS_MEP_SUPP_RC_OK);
}

}  // mep_g1

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/

