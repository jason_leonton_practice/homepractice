/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _MEP_ENGINE_H_
#define _MEP_ENGINE_H_

#include "main_types.h"
#include "vtss/appl/mep.h"
#include "mep.h"

namespace mep_g2 {

#define VTSS_MEP_LAPS_DATA_LENGTH    4
#define VTSS_MEP_RAPS_DATA_LENGTH    8

typedef struct {
    vtss_appl_mep_test_prio_conf_t   tests[VTSS_APPL_MEP_PRIO_MAX][VTSS_APPL_MEP_TEST_MAX];   /* Test flow configuration per prio */
    vtss_appl_mep_tst_common_conf_t  common;    /* The common Test Signal configuration */
    u32                              prio;      /* This is not used internally. This can be used to contain the latest priority used to support the old TST interface */
    u32                              test_idx;  /* This is not used internally. This can be used to contain the latest test index to support the old TST interface */
} vtss_mep_mgmt_tst_conf_t;

typedef struct {
    vtss_appl_mep_test_prio_conf_t   tests[VTSS_APPL_MEP_PRIO_MAX][VTSS_APPL_MEP_TEST_MAX];   /* LB flow configuration per prio */
    vtss_appl_mep_lb_common_conf_t   common;    /* The common Loop Back configuration */
    u32                              prio;      /* This is not used internally. This can be used to contain the latest priority used to support the old LB interface */
    u32                              test_idx;  /* This is not used internally. This can be used to contain the latest test index to support the old LB interface */
} vtss_mep_mgmt_lb_conf_t;

typedef struct {
    vtss_appl_mep_dm_prio_conf_t     dm[VTSS_APPL_MEP_PRIO_MAX];   /* Delay Measurement configuration per prio */
    vtss_appl_mep_dm_common_conf_t   common;    /* The common Delay Measurement configuration */
    u32                              prio;      /* This is not used internally. This can be used to contain the latest priority used to support the old DM interface */
} vtss_mep_mgmt_dm_conf_t;

typedef struct {
    vtss_appl_mep_state_t       mep;
    vtss_appl_mep_peer_state_t  peer_mep[VTSS_APPL_MEP_PEER_MAX];
} vtss_mep_mgmt_state_t;



/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* Instance is configured or disabled. If instance is marked as 'volatile an error is returned */
u32 vtss_mep_mgmt_conf_set(const u32          instance,
                           const mep_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* When this instance is enabled, it is marked as a 'volatile' Instance' and can only be disabled using this function. */
/* If this instance is used as parameter to 'vtss_mep_mgmt_conf_set' an error is returned */
u32 vtss_mep_mgmt_volatile_conf_set(const u32          instance,
                                    const mep_conf_t  *const conf);

/* instance:    Instance number of MEP.                     */
/* conf:        Configuration data for this SAT instance    */
/* When this instance is enabled, it is marked as a 'volatile' SAT Instance' and can only be disabled using this function. */
/* If this instance is used as parameter to 'vtss_mep_mgmt_conf_set' an error is returned */
u32 vtss_mep_mgmt_volatile_sat_conf_set(const u32             instance,
                                        const mep_sat_conf_t  *const conf);

u32 vtss_mep_mgmt_volatile_sat_conf_get(const u32       instance,
                                        mep_sat_conf_t  *const conf);

/* instance:   Instance number of MEP                                  */
/* Get the SAT counters. This is the number of transmitted and received OAM frames on the UNI per priority */
u32 vtss_mep_mgmt_volatile_sat_counter_get(const u32           instance,
                                           mep_sat_counters_t  *const counters);

/* instance:   Instance number of MEP                                  */
/* Clear the SAT counters. */
u32 vtss_mep_mgmt_volatile_sat_counter_clear_set(const u32  instance);

/* instance:   Instance number of MEP                                  */
/* Get the SAT actual BPS. */
u32 vtss_mep_mgmt_volatile_sat_bps_act_get(const u32  instance,  mep_tx_bps_t *const bps);

/* instance:    Instance number of MEP.                 */
/* mac:         MAC of this MEP instance                */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_conf_get(const u32    instance,
                           mep_conf_t   *const conf);

/* Check if given instance is enabled                                       */
/* instance:    Instance number of MEP.                                     */
BOOL vtss_mep_mgmt_is_instance_enabled(u32 instance);

/* instance:    Instance number of MEP.                                     */
/* next:        Next instance number of MEP which configuration is enabled. */
u32 vtss_mep_mgmt_get_next_enabled_instance(const u32    *const instance,
                                            u32          *const next);

/* instance:        Instance number of MEP.                                     */
/* next_instance:   Next instance number of MEP which configuration is enabled. */
/* peer:            Peer MEP ID                                                 */
/* next_peer:       Next Peer MEP ID.                                           */
u32 vtss_mep_mgmt_get_next_enabled_instance_peer(const u32    *const instance,
                                                 u32          *const next_instance,
                                                 const u32    *const peer,
                                                 u32          *const next_peer);

/* instance:    Instance number.                        */
/* return:      TRUE if next found                      */
BOOL vtss_mep_mgmt_conf_iter(u32 *instance);

/* instance:  Instance number of MEP.                 */
/* conf:      Performance Monitoring Configuration data for this instance    */
u32 vtss_mep_mgmt_pm_conf_set(const u32                      instance,
                              const vtss_appl_mep_pm_conf_t  *const conf);

u32 vtss_mep_mgmt_pm_conf_get(const u32                 instance,
                              vtss_appl_mep_pm_conf_t   *const conf);

/* instance:  Instance number of MEP.                 */
/* conf:      Link State Tracking Configuration data for this instance    */
u32 vtss_mep_mgmt_lst_conf_set(const u32                       instance,
                               const vtss_appl_mep_lst_conf_t  *const conf);

u32 vtss_mep_mgmt_lst_conf_get(const u32                  instance,
                               vtss_appl_mep_lst_conf_t   *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:      Configuration data for this instance    */
u32 vtss_mep_mgmt_syslog_conf_set(const u32                          instance,
                                  const vtss_appl_mep_syslog_conf_t  *const conf);

u32 vtss_mep_mgmt_syslog_conf_get(const u32                     instance,
                                  vtss_appl_mep_syslog_conf_t   *const conf);

u32 vtss_mep_mgmt_etree_conf_get(const u32                    instance,
                                 vtss_mep_mgmt_etree_conf_t   *const conf);

/* instance:    Instance number of MEP.                                         */
/* CC based on CCM is enabled/disabled. When period is VTSS_MEP_MGMT_PERIOD_300S, CCM is HW driven - otherwise SW driven. */
/* The parameters 'period' and 'cast' must be the same as for vtss_mep_mgmt_lm_conf_set() if both CC and LM is configured */
/* to be based on SW driven CCM session.                                                                                  */
u32 vtss_mep_mgmt_cc_conf_set(const u32                        instance,
                              const vtss_appl_mep_cc_conf_t    *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_cc_conf_get(const u32                 instance,
                              vtss_appl_mep_cc_conf_t   *const conf);

/* instance:    Instance number of MEP.                                     */
/* LM based on either CCM/LMM (depending on 'ended') is enabled/disabled.   */
/* The parameters 'period' and 'cast' must be the same as for vtss_mep_mgmt_cc_conf_set() */
/* if both CC and LM is configured to be based on SW driven CCM session.                  */
u32 vtss_mep_mgmt_lm_conf_set(const u32                      instance,
                              const vtss_appl_mep_lm_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_lm_conf_get(const u32                instance,
                              vtss_appl_mep_lm_conf_t  *const conf);

/* instance:    Instance number of MEP.                                                  */
/* LM Availability based on either CCM/LMM (depending on 'ended') is enabled/disabled.   */
u32 vtss_mep_mgmt_lm_avail_conf_set(const u32                            instance,
                                    const vtss_appl_mep_lm_avail_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_lm_avail_conf_get(const u32                      instance,
                                    vtss_appl_mep_lm_avail_conf_t  *const conf);

/* instance:    Instance number of MEP.                                                  */
/* LM HLI based on either CCM/LMM (depending on 'ended') is enabled/disabled.   */
u32 vtss_mep_mgmt_lm_hli_conf_set(const u32                          instance,
                                  const vtss_appl_mep_lm_hli_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_lm_hli_conf_get(const u32                    instance,
                                  vtss_appl_mep_lm_hli_conf_t  *const conf);

/* instance:    Instance number of MEP.                                                  */
/* LM SDEG based on either CCM/LMM (depending on 'ended') is enabled/disabled.   */
u32 vtss_mep_mgmt_lm_sdeg_conf_set(const u32                           instance,
                                   const vtss_appl_mep_lm_sdeg_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_lm_sdeg_conf_get(const u32                     instance,
                                   vtss_appl_mep_lm_sdeg_conf_t  *const conf);

/* instance:    Instance number of MEP.                                   */
/* DM based on either 1DM/DMM (depending on 'way') is enabled/disabled.   */
u32 vtss_mep_mgmt_dm_conf_set(const u32                       instance,
                              const vtss_mep_mgmt_dm_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_dm_conf_get(const u32                instance,
                              vtss_mep_mgmt_dm_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* Enable/disable of APS. The APS data contained in the APS PDU is controlled by EPS */
u32 vtss_mep_mgmt_aps_conf_set(const u32                       instance,
                               const vtss_appl_mep_aps_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_aps_conf_get(const u32                 instance,
                               vtss_appl_mep_aps_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* Enable/disable of Link Trace. */
u32 vtss_mep_mgmt_lt_conf_set(const u32                      instance,
                              const vtss_appl_mep_lt_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_lt_conf_get(const u32                 instance,
                              vtss_appl_mep_lt_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* Enable/disable of Loop Back. */
u32 vtss_mep_mgmt_lb_conf_set(const u32                      instance,
                              const vtss_mep_mgmt_lb_conf_t  *const conf,
                                    u64                      *active_time_ms);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_lb_conf_get(const u32                 instance,
                              vtss_mep_mgmt_lb_conf_t  *const conf);

/* instance:    Instance number of MEP. */
/* Clear LB counters for this MEP      */
u32 vtss_mep_mgmt_lb_state_clear_set(const u32    instance);

/* instance:    Instance number of MEP.                 */
/* Get the Loop Back Result                             */
u32 vtss_mep_mgmt_lb_prio_state_get(const u32                  instance,
                                    const u32                  prio,
                                    vtss_appl_mep_lb_state_t   *const state);

/* instance:    Instance number of MEP.                 */
/* Get the Link Trace Result                            */
u32 vtss_mep_mgmt_lt_state_get(const u32                  instance,
                               vtss_appl_mep_lt_state_t   *const state);

/* instance:    Instance number of MEP. */
/* Get the ETH state of this MEP        */
u32 vtss_mep_mgmt_state_get(const u32                instance,
                            vtss_mep_mgmt_state_t    *const state);

/* instance:    Instance number of MEP. */
/* Get the CCM state of this MEP        */
mesa_rc vtss_mep_mgmt_cc_state_get(const u32                    instance,
                                   vtss_appl_mep_cc_state_t     *const state);

/* instance:    Instance number of MEP. */
/* Get the LM counters of this MEP      */
u32 vtss_mep_mgmt_lm_state_get(const u32                   instance,
                               const u32                   peer_id,
                               vtss_appl_mep_lm_state_t    *const state);

/* instance:    Instance number of MEP. */
/* Clear LM counters for this MEP       */
u32 vtss_mep_mgmt_lm_state_clear_set(const u32    instance,
                                     vtss_appl_mep_clear_dir direction);

/* instance:    Instance number of MEP. */
/* Get the LM Availability status of this MEP      */
u32 vtss_mep_mgmt_lm_avail_state_get(const u32                       instance,
                                     const u32                       peer_id,
                                     vtss_appl_mep_lm_avail_state_t  *const state);

/* instance:    Instance number of MEP. */
/* Get the LM HLI status of this MEP    */
u32 vtss_mep_mgmt_lm_hli_state_get(const u32                     instance,
                                   const u32                     peer_id,
                                   vtss_appl_mep_lm_hli_state_t  *const state);

/* instance:    Instance number of MEP. */
/* peer_idx:    Index of peer MEP.      */
/* Get the SLM counters for this MEP and its peer    */
u32 vtss_mep_mgmt_slm_counters_get(const u32            instance,
                                   const u32            peer_idx,
                                   sl_service_counter_t *const counters);

/* instance:    Instance number of MEP. */
/* Get the Latest timestamps of this MEP         */
u32 vtss_mep_mgmt_dm_timestamp_get(const u32           instance,
                                   mep_dm_timestamp_t  *const dm1_timestamp_far_to_near,
                                   mep_dm_timestamp_t  *const dm1_timestamp_near_to_far);

/* instance:    Instance number of MEP. */
/* Get the DM state per prio of this MEP         */
u32 vtss_mep_mgmt_dm_prio_state_get(const u32                   instance,
                                    const u32                   prio,
                                    vtss_appl_mep_dm_state_t    *const tw_state,
                                    vtss_appl_mep_dm_state_t    *const fn_state,
                                    vtss_appl_mep_dm_state_t    *const nf_state);

/* instance:    Instance number of MEP. */
/* Clear DM counters for this MEP       */
u32 vtss_mep_mgmt_dm_state_clear_set(const u32    instance);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* Enable/disable of ETH-AIS. */
u32 vtss_mep_mgmt_ais_conf_set(const u32                      instance,
                               const vtss_appl_mep_ais_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_ais_conf_get(const u32                 instance,
                               vtss_appl_mep_ais_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* Enable/disable of ETH-LCK */
u32 vtss_mep_mgmt_lck_conf_set(const u32                       instance,
                               const vtss_appl_mep_lck_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_lck_conf_get(const u32                 instance,
                               vtss_appl_mep_lck_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* Enable/disable of Test frame generation. */
u32 vtss_mep_mgmt_tst_conf_set(const u32                       instance,
                               const vtss_mep_mgmt_tst_conf_t  *const conf,
                                     u64                       *active_time_ms);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_tst_conf_get(const u32                 instance,
                               vtss_mep_mgmt_tst_conf_t  *const conf);

/* instance:    Instance number of MEP. */
/* Clear TST counters for this MEP      */
u32 vtss_mep_mgmt_tst_state_clear_set(const u32    instance);

/* instance:    Instance number of MEP.            */
/* Get the test Result                             */
u32 vtss_mep_mgmt_tst_prio_state_get(const u32                   instance,
                                     const u32                   prio,
                                     vtss_appl_mep_tst_state_t   *const state);

/* instance:    Instance number of MEP.                 */
/* Change happend that require re-configuration of the MEP instance    */
u32 vtss_mep_mgmt_change_set(const u32 instance,  BOOL evc_change,  BOOL e_tree);

/* instance:    Instance number of MEP.                 */
/* Port protection change happend that require re-configuration of the MEP instance    */
u32 vtss_mep_mgmt_protection_change_set(const u32   instance);

/* instance:    Instance number of MEP.                 */
/* Interface status changed for this instance    */
u32 vtss_mep_mgmt_is_tlv_change_set(const u32   instance);

/* Some port status changed the MEP instance    */
u32 vtss_mep_mgmt_ps_tlv_change_set(void);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
/* Client configuration set */
u32 vtss_mep_mgmt_client_conf_set(const u32                 instance,
                                  const mep_client_conf_t  *const conf);

/* instance:    Instance number of MEP.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_mep_mgmt_client_conf_get(const u32           instance,
                                  mep_client_conf_t  *const conf);

void  vtss_mep_mgmt_default_conf_get(mep_default_conf_t  *const conf);











































/****************************************************************************/
/*  MEP module call in interface                                            */
/****************************************************************************/

/* instance:    Instance number of MEP.             */
/* aps:         transmitted APS specific info.      */
/* event:       transmit APS as an ERPS event.      */
/* This called by EPS/ERPS to transmit APS specific info. The first three APS is transmitted with 3.3 ms. interval and thereafter with 5 s. interval.  */
/* The length of the 'aps' array depends on the aps type configuration but is MEP_APS_DATA_LENGTH maximum                                              */
/* If 'aps' contains the already transmitting APS, nothing happens.                                                                                    */
u32 vtss_mep_tx_aps_info_set(const u32   instance,
                             const u8    *aps,
                             const BOOL  event);


/* instance:    Instance number of MEP      */
/* state:       SSF state                   */
/* This is called by module when new SSF state is detected */
void vtss_mep_new_ssf_state(const u32    instance,
                            const BOOL   state);


/* instance:    Instance number of MEP.                                    */
/* enable:      TRUE means that R-APS is transmitted at 5 sec. interval    */
/* R-APS transmission from the MEP related port, can be enabled/disabled   */
u32 vtss_mep_raps_transmission(const u32    instance,
                               const BOOL   enable);


/* instance:    Instance number of MEP.       */
/* This is called by external to activate MEP calling out with SF state and received APS */
u32 vtss_mep_signal_in(const u32   instance);


mesa_rc mep_instance_conf_set(u32 instance, const mep_conf_t   *const conf);
mesa_rc mep_instance_conf_add(u32 instance, const mep_conf_t   *const conf);











mesa_rc mep_status_eps_get(const u32        instance,
                           mep_eps_state_t  *const state);

/****************************************************************************/
/*  MEP module call out interface                                           */
/****************************************************************************/
/* instance:    Instance number of MEP.              */
/* domain:      Domain.                              */
/* aps_info:    Received APS specific info.          */
/* This is called by MEP to deliver received APS specific info to EPS/ERPS     */
void vtss_mep_rx_aps_info_set(const u32                      instance,
                              const mep_domain_t             domain,
                              const u8                       *aps);


/* instance:    Instance number of MEP.                  */
/* domain:      Domain.                                  */
/* sf_state:    SF state delivered to EPS.               */
/* sd_state:    SD state delivered to EPS.               */
/* This is called by MEP to deliver SF/SD info to EPS    */
void vtss_mep_sf_sd_state_set(const u32                      instance,
                              const mep_domain_t             domain,
                              const BOOL                     sf_state,
                              const BOOL                     sd_state);


/* instance:    Instance number of MEP.                    */
/* domain:      Domain.                                    */
/* This is called by MEP to activate external to call in on all in functions     */
void vtss_mep_signal_out(const u32                       instance,
                         const mep_domain_t              domain);


/* instance:    Instance number of MEP.                    */
/* Notify external that MEP state has changed */
void vtss_mep_status_change(const u32  instance,
                            const BOOL enable);


/* instance:    Instance number of MEP.                    */
/* Get LM notification state                               */
void vtss_mep_lm_notif_status_get(const u32  instance,
                                  vtss_appl_mep_lm_notif_state_t *state);

/* instance:    Instance number of MEP.                    */
/* Set LM notification state                               */
/* state == NULL to delete                                 */
void vtss_mep_lm_notif_status_set(const u32  instance,
                                  vtss_appl_mep_lm_notif_state_t *state);

/* instance:    Instance number of MEP.                    */
/* Get LM HLI state                                        */
void vtss_mep_lm_hli_status_get(const u32  instance,
                                const u32  peer_index,
                                vtss_appl_mep_lm_hli_state_t *state);

/* instance:    Instance number of MEP.                    */
/* Set LM HLI state                                        */
/* state == NULL to delete                                 */
void vtss_mep_lm_hli_status_set(const u32  instance,
                                const u32  peer_index,
                                vtss_appl_mep_lm_hli_state_t *state);


/* This is the initializing of MEP. Has to be called by platform       */
u32 vtss_mep_init(void);

/****************************************************************************/
/*  MEP platform call out interface                                         */
/****************************************************************************/

/* This is called by MEP upper logic in order to do syslog */
/**
 * Definition of syslog type.
 */
typedef enum {
    VTSS_MEP_REMOTE_MEP_DOWN,
    VTSS_MEP_REMOTE_MEP_UP,
    VTSS_MEP_ENTER_AIS,
    VTSS_MEP_EXIT_AIS,
    VTSS_MEP_CROSS_CONNECTED_SERVICE,
    VTSS_MEP_CROSSCHECK_MEP_MISSING,
    VTSS_MEP_CROSSCHECK_MEP_UNKNOWN,
    VTSS_MEP_CROSSCHECK_SERVICE_UP,
    VTSS_MEP_FORWARDING_LOOP,
    VTSS_MEP_CONFIG_ERROR,
} vtss_mep_syslog_t;

void vtss_mep_syslog(vtss_mep_syslog_t type, const u32 param1, const u32 param2);

/* This is called by MEP upper logic in order to get own MAC */
void vtss_mep_mac_get(u32 port,  u8 mac[VTSS_APPL_MEP_MAC_LENGTH]);

/* This is called by MEP LT OAM engine in order to check forwarding of LTM PDU: */
BOOL vtss_mep_check_forwarding(u32 i_port, u32 *f_port, u8 *mac,  u32 vid);

typedef enum
{
    VTSS_MEP_TAG_NONE,
    VTSS_MEP_TAG_C,
    VTSS_MEP_TAG_S,
    VTSS_MEP_TAG_S_CUSTOM,
} vtss_mep_tag_type_t;

/* This is called by MEP to get relevant data for this EVC    */
#define VTSS_MEP_COS_ID_SIZE 8
void vtss_mep_evc_cos_id_get(const u32  evc_inst,
                             u32        port,
                             BOOL       cos_id[VTSS_MEP_COS_ID_SIZE]);

i32 vtss_mep_delay_calc(const mesa_timestamp_t *x,
                        const mesa_timestamp_t *y,
                        const BOOL             is_ns);

void vtss_mep_timestamp_get(mesa_timestamp_t *timestamp, u32 *tc);

u32 vtss_mep_port_count(void);

void vtss_mep_port_conf_disable(const u32       instance,
                                const u32       port,
                                const BOOL      disable);

void vtss_mep_timer_cb(vtss::Timer *timer);     /* This is the call back function called by mep.c into engine.cxx where no critd lock/unlock happens */

}  // mep_g2

#endif /* _MEP_ENGINE_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
