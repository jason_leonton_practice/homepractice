/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#define MEP_IMPL_G1
#include "main.h"
#include "conf_api.h"
#include "critd_api.h"
#include "port_api.h"
#include "vtss/appl/mep.h"
#include "../base/vtss_mep.h"
#include "../base/vtss_mep_supp_api.h"
#include "packet_api.h"
#include "vtss_tod_api.h"
#include "vtss_timer_api.h"
#include "vtss_tod_phy_engine.h"
#include "vlan_api.h"
#include "misc_api.h"
#include "mstp_api.h"
#include "qos_api.h"
#include "mscc/ethernet/switch/api.h"
#include "mep_api.h"
#include "../mep_ll_api.h"
#include "../mep_ll_pdu_defs.h"
#include "mep_expose.h"

#include "qos_api.h"
#if defined(VTSS_SW_OPTION_PTP)
#include "vtss_ptp_os.h"
#endif
#include "interrupt_api.h"
#if defined(VTSS_SW_OPTION_ACL)
#include "acl_api.h"
#endif





#if defined(VTSS_SW_OPTION_EPS)
#include "eps_api.h"
#endif

#if defined(VTSS_SW_OPTION_ERPS)
#include "erps_api.h"
#endif

#ifdef VTSS_SW_OPTION_ICFG
#include "mep_icli_functions.h"
#endif





#ifdef VTSS_SW_OPTION_AFI
#include "afi_api.h"
#endif


/* ================================================================= *
 *  Trace definitions
 * ================================================================= */

#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>
#include <sys/time.h>
#include "vtss_tod_mod_man.h"
#include "tod_api.h"
#if defined(VTSS_SW_OPTION_PHY)
#include "phy_api.h"
#endif
#define API_INST_DEFAULT        PHY_INST

#undef VTSS_TRACE_MODULE_ID
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_MEP

#ifdef VTSS_SW_OPTION_SYSLOG
#include "syslog_api.h"
#endif

#define VTSS_TRACE_GRP_DEFAULT 0
#define TRACE_GRP_CRIT         1
#define TRACE_GRP_APS_TX       2
#define TRACE_GRP_APS_RX       3
#define TRACE_GRP_TX_FRAME     4
#define TRACE_GRP_RX_FRAME     5
#define TRACE_GRP_DM           6
#define TRACE_GRP_LM           7
#define TRACE_GRP_EVENT        8
#define TRACE_GRP_DISCARD      9
#define TRACE_GRP_CNT          10

#include <vtss_trace_api.h>

/****************************************************************************/
/*  Global variables                                                        */
/****************************************************************************/

namespace mep_g1 {

#if (VTSS_TRACE_ENABLED)
static vtss_trace_reg_t trace_reg =
{
    VTSS_TRACE_MODULE_ID, "MEP", "MEP module."
};

static vtss_trace_grp_t trace_grps[TRACE_GRP_CNT] =
{
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CRIT */ {
        "crit",
        "Critical regions ",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_APS_TX */ {
        "txAPS",
        "Tx APS print out ",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_APS_RX */ {
        "rxAPS",
        "Rx APS print out ",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_TX_FRAME */ {
        "txFRAME",
        "Tx frame print out ",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_RX_FRAME */ {
        "rxFRAME",
        "Rx frame print out ",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_DM */ {
        "dm",
        "DM print out",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_LM */ {
        "lm",
        "LM print out",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_EVENT */ {
        "Event",
        "Event print out",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_DISCARD */ {
        "Discard",
        "Discard print out",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    }
};
#define CRIT_ENTER(crit) critd_enter(&crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define CRIT_EXIT(crit)  critd_exit( &crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#else
#define CRIT_ENTER(crit) critd_enter(&crit)
#define CRIT_EXIT(crit)  critd_exit( &crit)
#endif /* VTSS_TRACE_ENABLED */




#define MPLS_DOMAIN(domain) (FALSE)


/****************************************************************************/
/*  Various local functions                                                 */
/****************************************************************************/
#define FLAG_TIMER          0x0001
#define FLAG_TIMER_SUPP     0x0002

#define FLAG_RUN            0x0002
#define FLAG_MSTP           0x0004
#define FLAG_CCM            0x0008

#define APS_INST_INV        0xFFFF
#define SF_EPS_MAX          (MEP_EPS_MAX-1)
#define TX_BUF_MAX          3

#undef  MEP_DATA_DUMP
#define DEBUG_WO_PHY            0
#define VTSS_FLOW_NUM_PER_PORT  4
#define DEBUG_MORE_INFO         0

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_MEP

static vtss_handle_t timer_thread_handle;
static vtss_thread_t timer_thread_block;
static vtss_handle_t run_thread_handle;
static vtss_thread_t run_thread_block;

static critd_t       crit_p;      /* Platform critd */
static critd_t       crit;        /* Base MEP critd */
static critd_t       crit_supp;   /* Base MEP Support critd */

static vtss_flag_t   run_wait_flag;
static vtss_flag_t   timer_wait_flag;

typedef struct {
    BOOL  used;
    BOOL  multi;
    struct {
        struct {
            u32   afi_id;
            u8    *frame;
        }data[VTSS_APPL_MEP_PRIO_MAX][VTSS_APPL_MEP_TEST_MAX];
    }port[2];
} tx_buffer_t;

typedef struct
{
    mesa_port_list_t           port;       /* Port to be used by MEP */
    u32                        ssf_count;                        /* Number of related EPS (SF_EPS_MAX)            */
    u16                        ssf_inst[SF_EPS_MAX];             /* SF/SD state to all    */
    u16                        aps_inst;
    vtss_appl_mep_aps_type_t   aps_type;
    mep_eps_type_t             eps_type;
    mep_domain_t               domain;
    u32                        flow;
    u32                        res_port;
    BOOL                       enable;
    u16                        vid;
    BOOL                       syslog_control_flag;

    u8                         aps_txdata[VTSS_MEP_APS_DATA_LENGTH];
    BOOL                       raps_tx;
    BOOL                       raps_forward;

    tx_buffer_t                tx_buffer[TX_BUF_MAX];           /* Extra transmit buffers for auto CCM/TST transmission on protection port or into major ring */
    mesa_ace_id_t              ccm_loc_ace_id[2];                   /* The ACE id of the HW LOC CCM ACE rule */
    mesa_ace_id_t              ccm_any_ace_id[2];               /* The ACE id of the Any SMAC CCM ACE rule */
    CapArray<mesa_ace_id_t, MESA_CAP_PORT_CNT> tst_ace_id;      /* The ACE id of the TST ACE rule */
    mesa_ace_id_t              mip_lbm_ace_id;
    CapArray<mesa_ace_id_t, MESA_CAP_PORT_CNT> etree_elan_mc_ace_id;
    CapArray<mesa_ace_id_t, MESA_CAP_PORT_CNT> etree_elan_uc_ace_id;
    CapArray<mesa_ace_id_t, MESA_CAP_PORT_CNT> etree_elan_other_uc_ace_id;
    BOOL                       ll_is_used;                      /* TRUE if MEP is used by at least one TT-LOOP LL instance */
    mesa_ace_id_t              llm_ace_id;                      /* MEP ACL ID for LLM */
    vtss_mep_supp_ll_frame_info_t llm_frame_info;

    void init() {
        port.clear_all();
        ssf_count = 0;
        memset(ssf_inst, 0, sizeof(ssf_inst));
        aps_inst = 0;
        aps_type = VTSS_APPL_MEP_INV_APS;
        eps_type = MEP_EPS_TYPE_INV;
        domain = VTSS_APPL_MEP_PORT;
        flow = 0;
        res_port = 0;
        enable = 0;
        vid = 0;
        syslog_control_flag = 0;
        memset(aps_txdata, 0, sizeof(aps_txdata));
        raps_tx = FALSE;
        raps_forward = FALSE;
        memset(tx_buffer, 0, sizeof(tx_buffer));
        memset(ccm_loc_ace_id, 0, sizeof(ccm_loc_ace_id));
        memset(ccm_any_ace_id, 0, sizeof(ccm_any_ace_id));
        tst_ace_id.clear();
        mip_lbm_ace_id = 0;
        etree_elan_mc_ace_id.clear();
        etree_elan_uc_ace_id.clear();
        etree_elan_other_uc_ace_id.clear();
        ll_is_used = FALSE;
        llm_ace_id = 0;
        llm_frame_info = {};
    }

} instance_data_t;

static void vlan_custom_ethertype_callback(void);

typedef struct
{
    mesa_timeinterval_t max;
    mesa_timeinterval_t min;
    mesa_timeinterval_t mean;
    u32 cnt;
} mep_observed_egr_lat_t;

/* latency observed in onestep tx timestamping */
static mep_observed_egr_lat_t observed_egr_lat = {};

static CapArray<instance_data_t, VTSS_APPL_CAP_MEP_INSTANCE_MAX> instance_data;
static mesa_port_list_t                 los_state;
static mesa_port_list_t                 conv_los_state;
static CapArray<u32, MESA_CAP_PORT_CNT> in_port_conv;
static CapArray<u32, MESA_CAP_PORT_CNT> out_port_conv;
static mesa_port_list_t                 out_port_1p1;
static mesa_port_list_t                 out_port_tx;
static u32 voe_up_mep_loop_port = 0;

#define EVC_VLAN_VID_SIZE 4096
static u32              evc_vlan_vid[EVC_VLAN_VID_SIZE];

typedef struct {
    u16 channel_id;                             /* identifies the channel id in the PHY
                                                   (needed to access the timestamping feature) */
    BOOL port_shared;                           /* port shares engine with another port */
    mesa_port_no_t shared_port_no;              /* the port this engine is shared with. */
    BOOL port_phy_ts;                           /* PHY timestamping type PHY            */
} port_data_t;
using port_data_array_t = CapArray<port_data_t, MESA_CAP_PORT_CNT>;
static port_data_array_t port_data;

typedef struct {
    BOOL                        phy_ts_port;  /* TRUE if this port is a PTP port and has the PHY timestamp feature */

    mesa_phy_ts_engine_t        engine_id;   /* MESA_PHY_TS_ENGINE_ID_INVALID indicates that no engine is allocated */
    u8                          ing_mac[VTSS_FLOW_NUM_PER_PORT][VTSS_APPL_MEP_MAC_LENGTH]; /* MAC address of the flow to set to PHY */
    u8                          ing_shr_no[VTSS_FLOW_NUM_PER_PORT]; /* the number of mep to share this flow (MAC and tag_no) */
    u8                          ing_mac_cnt; /* count of the used flow*/
    //mesa_phy_ts_engine_t        egress_id;    /* MESA_PHY_TS_ENGINE_ID_INVALID indicates that no engine is allocated */
    u8                          eg_mac[VTSS_FLOW_NUM_PER_PORT][VTSS_APPL_MEP_MAC_LENGTH];
    u8                          eg_shr_no[VTSS_FLOW_NUM_PER_PORT]; /* the number of mep to share this flow (MAC and tag_no) */
    u8                          eg_mac_cnt; /* count of the used flow*/
    u8                          flow_id_low;  /* identifies the flows allocated for this port */
    u8                          flow_id_high; /* identifies the flows allocated for this port */
} mep_phy_ts_engine_alloc_t;

using mep_phy_ts_engine_alloc_array_t = CapArray<mep_phy_ts_engine_alloc_t, MESA_CAP_PORT_CNT>;
mep_phy_ts_engine_alloc_array_t mep_phy_ts_port;


#ifdef MEP_DATA_DUMP
void phy_ts_dump(u8 port)
{
    u16 i,p;
    u16 y, z;

    if (port == 0xFF){
        /* dump all ports */
        y = 0;
        z = mep_caps.mesa_port_cnt;
    } else {
        y = port;
        z = port + 1;
    }


    for (i = y ; i < z ; i++) {
        printf("port %d: is_ts_port: %d, igrid %d, egrid %d f_lo: %d f_hi: %d\n", i,
                       mep_phy_ts_port[i].phy_ts_port,
                       mep_phy_ts_port[i].engine_id,
                       mep_phy_ts_port[i].engine_id,
                       mep_phy_ts_port[i].flow_id_low,
                       mep_phy_ts_port[i].flow_id_high);
        printf("ing_mac_cnt: %d\n", mep_phy_ts_port[i].ing_mac_cnt);
        for (p = 0 ; p < mep_phy_ts_port[i].ing_mac_cnt ; p++) {
            printf(" mac address %s, share_no: %d\n",
            misc_mac2str(&mep_phy_ts_port[i].ing_mac[p][0]),
            mep_phy_ts_port[i].ing_shr_no[p]);
        }

        printf("eg_mac_cnt: %d\n", mep_phy_ts_port[i].eg_mac_cnt);
        for (p = 0 ; p < mep_phy_ts_port[i].eg_mac_cnt ; p++) {
            printf(" mac address %s, share_no: %d\n",
            misc_mac2str(&mep_phy_ts_port[i].eg_mac[p][0]),
            mep_phy_ts_port[i].eg_shr_no[p]);
        }

    }
}

static void dump_conf(mesa_phy_ts_engine_flow_conf_t *flow_conf)
{
    u16 i;
    printf("flow conf: eng_mode %d ", flow_conf->eng_mode);
    printf("mep comm: ppb_en %d, etype %x, tpid %x\n", flow_conf->flow_conf.oam.eth1_opt.comm_opt.pbb_en,
           flow_conf->flow_conf.oam.eth1_opt.comm_opt.etype,
           flow_conf->flow_conf.oam.eth1_opt.comm_opt.tpid);
    for (i = 0; i < 8; i++) {
        printf(" channel_map[%d] %d ", i, flow_conf->channel_map[i]);
        printf(" mep flow: flow_en %d, match_mode %d, match_select %d\n", flow_conf->flow_conf.oam.eth1_opt.flow_opt[i].flow_en,
               flow_conf->flow_conf.oam.eth1_opt.flow_opt[i].addr_match_mode,
               flow_conf->flow_conf.oam.eth1_opt.flow_opt[i].addr_match_select);
        printf(" mac address %s\n",misc_mac2str(flow_conf->flow_conf.oam.eth1_opt.flow_opt[i].mac_addr));
        printf(" vlan_check %d, num_tag %d, outer_tag_type %d, inner_tag_type %d, tag_range_mode %d\n",
               flow_conf->flow_conf.oam.eth1_opt.flow_opt[i].vlan_check,
               flow_conf->flow_conf.oam.eth1_opt.flow_opt[i].num_tag,
               flow_conf->flow_conf.oam.eth1_opt.flow_opt[i].outer_tag_type,
               flow_conf->flow_conf.oam.eth1_opt.flow_opt[i].inner_tag_type,
               flow_conf->flow_conf.oam.eth1_opt.flow_opt[i].tag_range_mode);
    }
}

static void dump_mep_action(mesa_phy_ts_engine_action_t *oam_action)
{
    u16 i;
    printf("action_ptp %d\n", oam_action->action_ptp);
    for (i = 0; i < 2; i++) {
        printf(" action[%d]: enable %d, channel_map %d\n", i, oam_action->action.ptp_conf[i].enable, oam_action->action.oam_conf[i].channel_map);
#if 0
        printf("  ptpconf: range_en %d, val/upper %d, mask/lower %d\n", oam_action->action.oam_conf[i].oam_conf.range_en,
               oam_action->action.ptp_conf[i].oam_conf.domain.value.val,
               oam_action->action.ptp_conf[i].oam_conf.domain.value.mask);
        printf("   clk__mode %d, delaym_type %d\n",oam_action->action.oam_conf[i].clk_mode,
              oam_action->action.ptp_conf[i].delaym_type);
#endif
    }
}

#endif //#ifdef MEP_DATA_DUMP

static void mep_phy_config_mac(const u8  is_del,
                               const u8  port,
                               const u8  is_ing,
                               const u8  mac[VTSS_MEP_SUPP_MAC_LENGTH])
{
    /*
     *  If is_del is 0, it means to add an entry. Otherwise the
     *  value of (is_del - 1) is the index to delete. Add 1 to avoid
     *  0 used for addition.
     */
    if (is_ing) {
        /* ingress */
        if (is_del == 0) {
#if DEBUG_MORE_INFO
            printf(" add : is_del: %d  mep_phy_ts_port[port].ing_mac_cnt:%d\n", is_del, mep_phy_ts_port[port].ing_mac_cnt);
#endif
            memcpy(mep_phy_ts_port[port].ing_mac[mep_phy_ts_port[port].ing_mac_cnt], mac, sizeof(mep_phy_ts_port[port].ing_mac[mep_phy_ts_port[port].ing_mac_cnt]));
            mep_phy_ts_port[port].ing_shr_no[mep_phy_ts_port[port].ing_mac_cnt] = 0;
            mep_phy_ts_port[port].ing_mac_cnt++;
        } else {
            /* delete */
#if DEBUG_MORE_INFO
            printf("delete : is_del: %d  mep_phy_ts_port[port].ing_mac_cnt:%d\n", is_del, mep_phy_ts_port[port].ing_mac_cnt);
#endif
            if (mep_phy_ts_port[port].ing_mac_cnt) {
                if (is_del != mep_phy_ts_port[port].ing_mac_cnt) {
                    /* means not to delete the last one
                       move the last one to the position deleted */
                    memcpy(mep_phy_ts_port[port].ing_mac[is_del - 1], mep_phy_ts_port[port].ing_mac[mep_phy_ts_port[port].ing_mac_cnt - 1], sizeof(mep_phy_ts_port[port].ing_mac[is_del - 1]));
                }
                mep_phy_ts_port[port].ing_mac_cnt--;
            }
        }

    } else {
        /* egress */
        if (is_del == 0) {
#if DEBUG_MORE_INFO
            printf(" add : is_del: %d  mep_phy_ts_port[port].eg_mac_cnt:%d\n", is_del, mep_phy_ts_port[port].eg_mac_cnt);
#endif
            memcpy(mep_phy_ts_port[port].eg_mac[mep_phy_ts_port[port].eg_mac_cnt], mac, sizeof(mep_phy_ts_port[port].eg_mac[mep_phy_ts_port[port].eg_mac_cnt]));
            mep_phy_ts_port[port].eg_shr_no[mep_phy_ts_port[port].eg_mac_cnt] = 0;
            mep_phy_ts_port[port].eg_mac_cnt++;
        } else {
            /* delete */
#if DEBUG_MORE_INFO
            printf("delete : is_del: %d  mep_phy_ts_port[port].eg_mac_cnt:%d\n", is_del, mep_phy_ts_port[port].eg_mac_cnt);
#endif
            if (mep_phy_ts_port[port].eg_mac_cnt) {
                if (is_del != mep_phy_ts_port[port].eg_mac_cnt) {
                    /* means not to delete the last one
                       move the last one to the position deleted */
                    memcpy(mep_phy_ts_port[port].eg_mac[is_del - 1], mep_phy_ts_port[port].eg_mac[mep_phy_ts_port[port].eg_mac_cnt - 1], sizeof(mep_phy_ts_port[port].eg_mac[is_del - 1]));
                }
                mep_phy_ts_port[port].eg_mac_cnt--;
            }
        }
    }

}

static mesa_rc mep_phy_ts_update(u8 port)
{
    mesa_rc rc = VTSS_RC_OK;
    u8  mac_count;
    mesa_phy_ts_encap_t encap_type;
    mesa_port_no_t shared_port;
    u8  flow_id;
    BOOL engine_free = FALSE;

    encap_type = MESA_PHY_TS_ENCAP_ETH_OAM;
    T_D("setting ts rules for port %d", port);
#if DEBUG_MORE_INFO
    printf("mep_phy_ts_update 1 \n");
#endif

    /* We only support unicast solution.
     * We don't have the issue of h/w resource not enough so we just use the
     * easy way to allocate and configure it in the initial time and not to free
     * even some are really not used. The software architeture still reserves the
     * flexibility to free h/w resource for use of implementing multicast soulution
     * later.
     */
    if (mep_phy_ts_port[port].phy_ts_port) {
        T_D("allocate engines for port %d", port);
        /* Allocate engines for this instance */
        if (mep_phy_ts_port[port].engine_id == MESA_PHY_TS_ENGINE_ID_INVALID) { /* if not already allocated */
            /* allocate the engine only if PHY time stamping support is available*/
            mep_phy_ts_port[port].engine_id = tod_phy_eng_alloc(port, encap_type);
            T_I("allocated engine %d for port %d", mep_phy_ts_port[port].engine_id, port);

            rc = mesa_phy_ts_ingress_engine_init(API_INST_DEFAULT,
                    port,
                    mep_phy_ts_port[port].engine_id,
                    encap_type,
                    0, 3, /* 4 flows are always available (engine 2 can be shared between 2 API engines), we can allocate maximum up to 8 flows  */
                    MESA_PHY_TS_ENG_FLOW_MATCH_STRICT);

             if (rc != VTSS_RC_OK) {
                T_E("mep_phy_ts_update - fail to allocate ingress engine");
                return rc;
             }
#if DEBUG_MORE_INFO
            T_E("ing eng alloc id: %d for port %d, encap_type %d", mep_phy_ts_port[port].engine_id, port, encap_type);
#endif
            if (port_data[port].port_shared) {
                /* In dual PHY, the first port uses the first two of the 4 allocated flows
                   and the second one uses the last two */
                shared_port = port_data[port].shared_port_no;
                mep_phy_ts_port[shared_port].engine_id = mep_phy_ts_port[port].engine_id;
#if DEBUG_MORE_INFO
                T_E("shared engine: port %d, sharedPort %ld, ing_id %d",
                     port, shared_port, mep_phy_ts_port[port].engine_id);
#endif
                if (port_data[port].channel_id == 0) {
                    mep_phy_ts_port[port].flow_id_low = 0;
                    mep_phy_ts_port[port].flow_id_high = 1;
                    mep_phy_ts_port[shared_port].flow_id_low = 2;
                    mep_phy_ts_port[shared_port].flow_id_high = 3;
                } else {
                    mep_phy_ts_port[port].flow_id_low = 2;
                    mep_phy_ts_port[port].flow_id_high = 3;
                    mep_phy_ts_port[shared_port].flow_id_low = 0;
                    mep_phy_ts_port[shared_port].flow_id_high = 1;
                }
            } else {
                /* In single PHY, the port uses 4 flows */
#if DEBUG_MORE_INFO
                T_E("non shared engine: port %d, ing_id %d",
                     port, mep_phy_ts_port[port].engine_id);
#endif
                mep_phy_ts_port[port].flow_id_low = 0;
                mep_phy_ts_port[port].flow_id_high = 3;
            }

            rc = mesa_phy_ts_egress_engine_init(API_INST_DEFAULT,
              port,
               mep_phy_ts_port[port].engine_id,
              encap_type,
              0, 3, /* 4 flows are always available (engine 2 can be shared between 2 API engines)  */
              MESA_PHY_TS_ENG_FLOW_MATCH_STRICT);

            if (rc != VTSS_RC_OK) {
                 T_E("mep_phy_ts_update - fail to allocate egress engine");
                 return rc;
            }
#if DEBUG_MORE_INFO
        T_E("eg eng alloc id: %d for port %d, encap_type %d", mep_phy_ts_port[port].engine_id, port, encap_type);
#endif
        }

        /* Set up flow comparators */
        mesa_phy_ts_engine_flow_conf_t flow_conf;
        rc = mesa_phy_ts_ingress_engine_conf_get(API_INST_DEFAULT,
                port,
                mep_phy_ts_port[port].engine_id,
                &flow_conf);

        if (rc != VTSS_RC_OK) {
            T_E("mesa_phy_ts_ingress_engine_conf_get fail ");
            return rc;
        }

#ifdef MEP_DATA_DUMP
        T_E("conf dump before:");
        dump_conf(&flow_conf);
#endif
#if DEBUG_MORE_INFO
        T_E("get ing engine conf: %d", mep_phy_ts_port[port].engine_id);
#endif
        /* modify flow configuration */
        flow_conf.eng_mode = TRUE;
        flow_conf.flow_conf.oam.eth1_opt.comm_opt.etype = 0x8902;

        mac_count = mep_phy_ts_port[port].ing_mac_cnt;
        for (flow_id = mep_phy_ts_port[port].flow_id_low; flow_id <= mep_phy_ts_port[port].flow_id_high; flow_id++) {
            if (mac_count == 0) {
                /* all MACs are in already */
                break;
            } else {
               mac_count--;
            }

            flow_conf.channel_map[flow_id] = port_data[port].channel_id == 0 ?
                                          MESA_PHY_TS_ENG_FLOW_VALID_FOR_CH0 : MESA_PHY_TS_ENG_FLOW_VALID_FOR_CH1;
            flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].flow_en = TRUE;
            flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].addr_match_mode = MESA_PHY_TS_ETH_ADDR_MATCH_48BIT;
            flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].addr_match_select = MESA_PHY_TS_ETH_MATCH_DEST_ADDR;
            memcpy(flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].mac_addr,  /* match DA unicast address */
                   mep_phy_ts_port[port].ing_mac[flow_id-mep_phy_ts_port[port].flow_id_low], sizeof(flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].mac_addr));
            flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].vlan_check = FALSE;
#if DEBUG_MORE_INFO
            T_E("eth: flow_opt[%d]: channel_map %d, flow_en %d, match_mode %d, match:_sel %d",
                 flow_id,
                 flow_conf.channel_map[flow_id],
                 flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].flow_en,
                 flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].addr_match_mode,
                 flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].addr_match_select);
#endif
        }
        /* the same configuration is applied to both ingress and egress */
#ifdef MEP_DATA_DUMP
        T_E("conf dump after:");
        dump_conf(&flow_conf);
#endif
        rc = mesa_phy_ts_ingress_engine_conf_set(API_INST_DEFAULT,
                port,
                mep_phy_ts_port[port].engine_id,
                &flow_conf);

        if (rc != VTSS_RC_OK) {
            T_E("mesa_phy_ts_ingress_engine_conf_set fail");
            return rc;
        }

        rc = mesa_phy_ts_egress_engine_conf_get(API_INST_DEFAULT,
                port,
                mep_phy_ts_port[port].engine_id,
                &flow_conf);

        if (rc != VTSS_RC_OK) {
            T_E("mesa_phy_ts_egress_engine_conf_get fail ");
            return rc;
        }

#if DEBUG_MORE_INFO
        T_E("set ing engine conf: port %d, id %d", port, mep_phy_ts_port[port].engine_id);
#endif
        /* modify flow configuration */
        flow_conf.eng_mode = TRUE;
        flow_conf.flow_conf.oam.eth1_opt.comm_opt.etype = 0x8902;

        mac_count = mep_phy_ts_port[port].eg_mac_cnt;
        for (flow_id = mep_phy_ts_port[port].flow_id_low; flow_id <= mep_phy_ts_port[port].flow_id_high; flow_id++) {
            if (mac_count == 0) {
                /* all MACs are in already */
                break;
            } else {
               mac_count--;
            }
            flow_conf.channel_map[flow_id] = port_data[port].channel_id == 0 ?
                                          MESA_PHY_TS_ENG_FLOW_VALID_FOR_CH0 : MESA_PHY_TS_ENG_FLOW_VALID_FOR_CH1;
            flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].flow_en = TRUE;
            flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].addr_match_mode = MESA_PHY_TS_ETH_ADDR_MATCH_48BIT;
            flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].addr_match_select = MESA_PHY_TS_ETH_MATCH_SRC_ADDR;
            memcpy(flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].mac_addr,  /* match SA unicast address */
                   mep_phy_ts_port[port].eg_mac[flow_id-mep_phy_ts_port[port].flow_id_low], sizeof(flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].mac_addr));
            flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].vlan_check = FALSE;
#if DEBUG_MORE_INFO
            T_E("eth: flow_opt[%d]: channel_map %d, flow_en %d, match_mode %d, match:_sel %d",
                 flow_id,
                 flow_conf.channel_map[flow_id],
                 flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].flow_en,
                 flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].addr_match_mode,
                 flow_conf.flow_conf.oam.eth1_opt.flow_opt[flow_id].addr_match_select);
#endif
        }

        rc = mesa_phy_ts_egress_engine_conf_set(API_INST_DEFAULT,
                port,
                mep_phy_ts_port[port].engine_id,
                &flow_conf);

        if (rc != VTSS_RC_OK) {
            T_E("mesa_phy_ts_egress_engine_conf_set failure");
            return rc;
        }
#if DEBUG_MORE_INFO
        T_E("set eg engine conf: %d", mep_phy_ts_port[port].engine_id);
#endif
        /* Configure the Actions */
        /* set up actions */
        mesa_phy_ts_engine_action_t oam_action;
        /* Get the default actions which the API gives */
        rc = mesa_phy_ts_ingress_engine_action_get(API_INST_DEFAULT,
                port,
                mep_phy_ts_port[port].engine_id,
                &oam_action);
        if (rc != VTSS_RC_OK) {
            T_E("get engine action fail");
            return rc;
        }
#if DEBUG_MORE_INFO
        T_E("get ing action: %d", mep_phy_ts_port[port].engine_id);
#endif
        oam_action.action_ptp = FALSE;
        /* Maximum of 6 actions are possible for each engine  */
        oam_action.action.oam_conf[0].enable   = TRUE;
        oam_action.action.oam_conf[0].y1731_en = TRUE;
        oam_action.action.oam_conf[0].channel_map |= port_data[port].channel_id == 0 ?
                                         MESA_PHY_TS_ENG_FLOW_VALID_FOR_CH0 : MESA_PHY_TS_ENG_FLOW_VALID_FOR_CH1;

        oam_action.action.oam_conf[0].oam_conf.y1731_oam_conf.range_en = FALSE;
        oam_action.action.oam_conf[0].oam_conf.y1731_oam_conf.meg_level.value.val = 0;
        oam_action.action.oam_conf[0].oam_conf.y1731_oam_conf.meg_level.value.mask = 0; /* Don't care */
         /* The action can be set to 1DM or DMM or DMR */
        oam_action.action.oam_conf[0].oam_conf.y1731_oam_conf.delaym_type = MESA_PHY_TS_Y1731_OAM_DELAYM_1DM;

        oam_action.action.oam_conf[1].enable   = TRUE;
        oam_action.action.oam_conf[1].y1731_en = TRUE;
        oam_action.action.oam_conf[1].channel_map |= port_data[port].channel_id == 0 ?
                                         MESA_PHY_TS_ENG_FLOW_VALID_FOR_CH0 : MESA_PHY_TS_ENG_FLOW_VALID_FOR_CH1;

        oam_action.action.oam_conf[1].oam_conf.y1731_oam_conf.range_en = FALSE;
        oam_action.action.oam_conf[1].oam_conf.y1731_oam_conf.meg_level.value.val = 0;
        oam_action.action.oam_conf[1].oam_conf.y1731_oam_conf.meg_level.value.mask = 0; /* Don't care */
         /* The action can be set to 1DM or DMM or DMR */
        oam_action.action.oam_conf[1].oam_conf.y1731_oam_conf.delaym_type = MESA_PHY_TS_Y1731_OAM_DELAYM_DMM;

        oam_action.action.oam_conf[2].enable = FALSE;

#ifdef MEP_DATA_DUMP
        dump_mep_action(&oam_action);
#endif
        rc = mesa_phy_ts_ingress_engine_action_set(API_INST_DEFAULT,
                port,
                mep_phy_ts_port[port].engine_id,
                &oam_action);
        if (rc != VTSS_RC_OK) {
            T_E("mesa_phy_ts_ingress_engine_action_set: failure");
            return rc;
        }
#if DEBUG_MORE_INFO
        T_E("set ing action: %d", mep_phy_ts_port[port].engine_id);
#endif
        rc = mesa_phy_ts_egress_engine_action_set(API_INST_DEFAULT,
                port,
                mep_phy_ts_port[port].engine_id,
                &oam_action);
        if (rc != VTSS_RC_OK) {
            T_E("mesa_phy_ts_egress_engine_action_set: failure");
            return rc;
        }
#if DEBUG_MORE_INFO
        T_E("set eg action: %d", mep_phy_ts_port[port].engine_id);
#endif
        /* Enable phy_ts mode */
        rc = mesa_phy_ts_mode_set(API_INST_DEFAULT, port ,TRUE);
        if (rc != VTSS_RC_OK) {
            T_E("mesa_phy_ts_mode_set: failure");
            return rc;
        }

    } else {
        /* not used by this port: is it used by the shared port ? */
        engine_free = TRUE;
        if (port_data[port].port_shared) {
            shared_port = port_data[port].shared_port_no;
            if (mep_phy_ts_port[shared_port].phy_ts_port) {
                engine_free = FALSE;
            }
        } else {
            shared_port = 0;
        }
        if (engine_free && mep_phy_ts_port[port].engine_id != MESA_PHY_TS_ENGINE_ID_INVALID) { /* if not used any more */
            rc = mesa_phy_ts_ingress_engine_clear(API_INST_DEFAULT, port, mep_phy_ts_port[port].engine_id);
            if (rc != VTSS_RC_OK) {
                T_E("mesa_phy_ts_ingress_engine_clear fail");
                return rc;
            }
#if DEBUG_MORE_INFO
            T_E("ing eng free id: %d for port %d", mep_phy_ts_port[port].engine_id, port);
#endif
            tod_phy_eng_free(port, mep_phy_ts_port[port].engine_id);
            mep_phy_ts_port[port].engine_id = MESA_PHY_TS_ENGINE_ID_INVALID;
        }
        if (engine_free && mep_phy_ts_port[port].engine_id != MESA_PHY_TS_ENGINE_ID_INVALID) {
            rc = mesa_phy_ts_egress_engine_clear(API_INST_DEFAULT, port, mep_phy_ts_port[port].engine_id);
            if (rc != VTSS_RC_OK) {
                T_E("vtss_phy_ts_egress_engine_free fail");
                return rc;
            }
#if DEBUG_MORE_INFO
            T_E("eg eng free id: %d for port %d", mep_phy_ts_port[port].engine_id, port);
#endif
            tod_phy_eng_free(port, mep_phy_ts_port[port].engine_id);
            mep_phy_ts_port[port].engine_id = MESA_PHY_TS_ENGINE_ID_INVALID;
        }
        if (engine_free && port_data[port].port_shared) {
            mep_phy_ts_port[shared_port].engine_id = MESA_PHY_TS_ENGINE_ID_INVALID;
        }
    }
#ifdef MEP_DATA_DUMP
    phy_ts_dump(port);
#endif
    return rc;
}

static void port_data_initialize(void)
{
    mesa_phy_10g_id_t          phy_id;
    mesa_rc                    rc = VTSS_RC_OK;
    mesa_phy_10g_mode_t  phy_10g_mode;
    u16                        i;

    CRIT_ENTER(crit_p);
    for (i = 0; i < mep_caps.mesa_port_cnt; i++) {
        /* is this port a PHY TS port ? */
        if (mep_caps.mesa_port_10g) {
#if !DEBUG_WO_PHY
            phy_10g_mode.oper_mode = MESA_PHY_1G_MODE;
            rc = mesa_phy_10g_id_get(API_INST_DEFAULT, i, &phy_id);
            if (rc == VTSS_RC_OK && (phy_id.part_number == 0x8488 || phy_id.part_number == 0x8487) &&
                                    phy_id.revision >= 4) {
                rc = mesa_phy_10g_mode_get (API_INST_DEFAULT, i, &phy_10g_mode);
            }
            if (rc == VTSS_RC_OK && (phy_id.part_number == 0x8488 || phy_id.part_number == 0x8487) &&
                    phy_id.revision >= 4 && phy_10g_mode.oper_mode != MESA_PHY_1G_MODE) {

#else
            BOOL  test = TRUE;
            if (test){
#endif
                mep_phy_ts_port[i].phy_ts_port = TRUE;
                mep_phy_ts_port[i].engine_id = MESA_PHY_TS_ENGINE_ID_INVALID;
                mep_phy_ts_port[i].ing_mac_cnt = 0;
                mep_phy_ts_port[i].eg_mac_cnt = 0;

                /* This information needs to be maintained in the application to know which
                 * ports are shared as the PHY has only one analyzer that needs to be shared
                 * between the two channels
                 */
                port_data[i].channel_id  = phy_id.channel_id;
                port_data[i].port_phy_ts = TRUE;

                if (phy_id.phy_api_base_no != i) {
                    port_data[i].port_shared = TRUE;
                    port_data[i].shared_port_no = phy_id.phy_api_base_no;
                    port_data[phy_id.phy_api_base_no].port_shared = TRUE;
                    port_data[phy_id.phy_api_base_no].shared_port_no = i;
                }
                /* Initialize the TS Functionality: is moved to the tod module */
    #if DEBUG_MORE_INFO
                T_E("xxx Port_no = %d, uses 1588 PHY, channel %d, phy_api_base_no %ld, rc = %x ", i, phy_id.channel_id, phy_id.phy_api_base_no, rc);
    #endif

            } else {
                port_data[i].port_phy_ts = FALSE;
            }
        } else {
            port_data[i].port_phy_ts = FALSE;
        }
    }
    CRIT_EXIT(crit_p);
}


BOOL vtss_mep_phy_config_mac(const u8  port,
                             const u8  is_add,
                             const u8  is_ing,
                             const u8  mac[VTSS_APPL_MEP_MAC_LENGTH])
{
    u8      i, changed = 0;
    BOOL    ret;

    CRIT_ENTER(crit_p);
#if !DEBUG_WO_PHY
    /* for debug only*/
    if (!mep_phy_ts_port[port].phy_ts_port) {
        CRIT_EXIT(crit_p);
        return TRUE; /* Do nothing for port not support PHY timestamp */
    }
#endif
#if DEBUG_MORE_INFO
    printf("port: %d, is_add: %d, is_ing: %d xxx\n", port,is_add, is_ing);
#endif
    if (is_ing) {
        if (mep_phy_ts_port[port].ing_mac_cnt == 0) {
            if (is_add) {
                mep_phy_config_mac(0, port, is_ing, mac);
#if DEBUG_MORE_INFO
                T_E("1\n");
#endif
                changed = 1;
                ret = TRUE;
            } else {
                /* delete */
                /* databse is empty, nothing to do */
#if DEBUG_MORE_INFO
                T_E("2\n");
#endif
                ret =  TRUE;
            }
        } else {
            for (i = 0; i < VTSS_FLOW_NUM_PER_PORT; i++) {
                if (memcmp(&mep_phy_ts_port[port].ing_mac[i][0],
                    mac, VTSS_MEP_SUPP_MAC_LENGTH) == 0) {
                    /* the MAC address is alreay existed,
                     * just update share number
                     */
#if DEBUG_MORE_INFO
                    T_E("2-1");
#endif
                    break;
                }
            }
            if (i == VTSS_FLOW_NUM_PER_PORT) {
                /* entry not existed */
                if (is_add) {
                    if (mep_phy_ts_port[port].ing_mac_cnt < VTSS_FLOW_NUM_PER_PORT) {
                        /* with room to add */
                        mep_phy_config_mac(0, port, is_ing, mac);
                        changed = 1;
#if DEBUG_MORE_INFO
                        T_E("3\n");
#endif
                        ret =  TRUE;
                    }
                    else {
                        /* without room to add */
                        T_D("vtss_mep_phy_add_mac:ingrss no room to addd\n");
#if DEBUG_MORE_INFO
                        T_E("4\n");
#endif
                        ret =  FALSE;
                    }
                } else {
                    /* delete */
                    /* entry not existed, nothing to do */
#if DEBUG_MORE_INFO
                    T_E("5\n");
#endif
                    ret =  TRUE;
                }
            } else {
                if (is_add) {
                    /* share with the existing entry */
                    /* add 1 to share number */
                    mep_phy_ts_port[port].ing_shr_no[i]++;
                    changed = 1;
#if DEBUG_MORE_INFO
                    T_E("6\n");
#endif
                    ret =  TRUE;
                } else {
                    /* delete */
                    if (mep_phy_ts_port[port].ing_shr_no[i]) {
                        /* the entry is still used by others */
                        /* subtract 1 from share number */
                        mep_phy_ts_port[port].ing_shr_no[i]--;
#if DEBUG_MORE_INFO
                        T_E("7\n");
#endif
                        ret =  TRUE;
                    } else {
                        /* remove entry */
                        mep_phy_config_mac((i+1), port, is_ing, mac);
                        changed = 1;
#if DEBUG_MORE_INFO
                        T_E("8\n");
#endif
                        ret =  TRUE;
                    }
                }
            }

        }
    } else {
        /* egress */
        if (mep_phy_ts_port[port].eg_mac_cnt == 0) {
            if (is_add) {
                mep_phy_config_mac(0, port, is_ing, mac);
#if DEBUG_MORE_INFO
                T_E("1\n");
#endif
                changed = 1;
                ret =  TRUE;
            } else {
                /* delete */
                /* databse is empty, nothing to do */
#if DEBUG_MORE_INFO
                T_E("2\n");
#endif
                ret =  TRUE;
            }
        } else {
            for (i = 0; i < VTSS_FLOW_NUM_PER_PORT; i++) {
                if (memcmp(&mep_phy_ts_port[port].eg_mac[i][0],
                    mac, VTSS_MEP_SUPP_MAC_LENGTH) == 0) {
                    /* the MAC address is alreay existed,
                     * just update share number
                     */
#if DEBUG_MORE_INFO
                     T_E("2-1");
#endif
                     break;
                }
            }
            if (i == VTSS_FLOW_NUM_PER_PORT) {
                /* entry not existed */
                if (is_add) {
                    if (mep_phy_ts_port[port].eg_mac_cnt < VTSS_FLOW_NUM_PER_PORT) {
                        /* with room to add */
                        mep_phy_config_mac(0, port, is_ing, mac);
                        changed = 1;
#if DEBUG_MORE_INFO
                        T_E("3\n");
#endif
                        ret =  TRUE;
                    }
                    else {
                        /* without room to add */
                        T_D("vtss_mep_phy_add_mac:ingrss no room to addd\n");
#if DEBUG_MORE_INFO
                        T_E("4\n");
#endif
                        ret =  FALSE;
                    }
                } else {
                    /* delete */
                    /* entry not existed, nothing to do */
#if DEBUG_MORE_INFO
                    T_E("5\n");
#endif
                    ret =  TRUE;
                }
            } else {
                if (is_add) {
                    /* share with the existing entry */
                    /* add 1 to share number */
                    mep_phy_ts_port[port].eg_shr_no[i]++;
#if DEBUG_MORE_INFO
                    T_E("6\n");
#endif
                    ret =  TRUE;
                } else {
                    /* delete */
                    if (mep_phy_ts_port[port].eg_shr_no[i]) {
                        /* the entry is still used by others */
                        /* subtract 1 from share number */
                        mep_phy_ts_port[port].eg_shr_no[i]--;
#if DEBUG_MORE_INFO
                        T_E("7\n");
#endif
                        ret =  TRUE;
                    } else {
                        /* remove entry */
                        mep_phy_config_mac((i+1), port, is_ing, mac);
                        changed = 1;
#if DEBUG_MORE_INFO
                        T_E("8\n");
#endif
                        ret =  TRUE;
                    }
                }
            }

        }
    }
#if DEBUG_MORE_INFO
    T_E("changed: %d\n", changed);
#endif
    if (changed) {
        /* do nothing */
        /* mep_phy_ts_update(port); */
    }

    CRIT_EXIT(crit_p);
    return ret;
}

const char *mep_error_txt(mesa_rc error)
{
    switch (error) {
        case VTSS_APPL_MEP_RC_APS_PROT_CONNECTED:     return "Mismatch between assosiated protection type and APS type";
        case VTSS_APPL_MEP_RC_INVALID_PARAMETER:      return "Invalid parameter error returned from MEP";
        case VTSS_APPL_MEP_RC_NOT_ENABLED:            return "MEP instance is not enabled";
        case VTSS_APPL_MEP_RC_CAST:                   return "LM and CC sharing SW generated CCM must have same casting (multi/uni)";
        case VTSS_APPL_MEP_RC_RATE_INTERVAL:          return "The selected frame rate or interval is invalid";
        case VTSS_APPL_MEP_RC_PEER_CNT:               return "Invalid number of peer's for this configuration";
        case VTSS_APPL_MEP_RC_PEER_ID:                return "Invalid peer MEP ID";
        case VTSS_APPL_MEP_RC_MIP:                    return "Not allowed on a MIP";
        case VTSS_APPL_MEP_RC_INVALID_EVC:            return "EVC flow was found invalid";
        case VTSS_APPL_MEP_RC_INVALID_PRIO:           return "Priority was found invalid";
        case VTSS_APPL_MEP_RC_APS_UP:                 return "APS not allowed on EVC UP MEP";
        case VTSS_APPL_MEP_RC_APS_DOMAIN:             return "R-APS not allowed in this domain";
        case VTSS_APPL_MEP_RC_INVALID_VID:            return "VLAN is not created for this VID";
        case VTSS_APPL_MEP_RC_INVALID_COS_ID:         return "Invalid COS ID (priority) for this EVC";
        case VTSS_APPL_MEP_RC_NO_VOE:                 return "No VOE available";
        case VTSS_APPL_MEP_RC_NO_TIMESTAMP_DATA:      return "There is no DMR time-stamp data available";
        case VTSS_APPL_MEP_RC_PEER_MAC:               return "Peer Unicast MAC must be known to do this";
        case VTSS_APPL_MEP_RC_INVALID_INSTANCE:       return "Invalid MEP instance ID";
        case VTSS_APPL_MEP_RC_INVALID_MEG:            return "Invalid MEG-ID or IEEE Name";
        case VTSS_APPL_MEP_RC_PROP_SUPPORT:           return "Proprietary DM is not supported";
        case VTSS_APPL_MEP_RC_VOLATILE:               return "Cannot change volatile entry";
        case VTSS_APPL_MEP_RC_VLAN_SUPPORT:           return "VLAN domain is not supported";
        case VTSS_APPL_MEP_RC_CLIENT_MAX_LEVEL:       return "The MEP is on MAX level (7) - it is not possible to have a client on higher level";
        case VTSS_APPL_MEP_RC_INVALID_CLIENT_LEVEL:   return "The client level is invalid";
        case VTSS_APPL_MEP_RC_MIP_SUPPORT:            return "This MIP is not supported";
        case VTSS_APPL_MEP_RC_INVALID_MAC:            return "On this platform the selected MAC is invaled";
        case VTSS_APPL_MEP_RC_CHANGE_PARAMETER:       return "Some parameters are not allowed to change on an enabled instance";
        case VTSS_APPL_MEP_RC_NO_LOOP_PORT:           return "There is no loop port - this is required to create Up-MEP";
        case VTSS_APPL_MEP_RC_INVALID_PORT:           return "The residence port is not valid in this domain or for this MEP type";
        case VTSS_APPL_MEP_RC_INVALID_HW_CCM:         return "All MEP in same classified VID and same MEG level must have same HW CCM state (above or below 100 f/s)";
        case VTSS_APPL_MEP_RC_LST:                    return "Link State Tracking cannot be enabled as either CC or CC-TLV is not enabled";
        case VTSS_APPL_MEP_RC_NNI_PORT:               return "The MEP must be configured on a EVC NNI port";
        case VTSS_APPL_MEP_RC_TEST:                   return "TST and LBM cannot be enabled at the same time";
        case VTSS_APPL_MEP_RC_E_TREE:                 return "Invalid MEP configuration for E-TREE or E-LAN EVC";
        case VTSS_APPL_MEP_RC_CC_BFD_CONFLICT:        return "CC and BFD cannot be enabled at the same time";
        case VTSS_APPL_MEP_RC_INVALID_VOE_CHANGE:     return "VOE support cannot be added/deleted on an enabled MEP instance";
        case VTSS_APPL_MEP_RC_TTL_ZERO:               return "TTL=0 is not allowed";
        case VTSS_APPL_MEP_RC_LM_DUAL_ENDED_RATE_EXCEED_CC_RATE:  return "LM_DUAL_ENDED rate cannot be higher than CC rate";
        case VTSS_APPL_MEP_RC_OUT_OF_TX_RESOURCE:     return "There are no free transmission resources";
        case VTSS_APPL_MEP_RC_TX_ERROR:               return "Transmitting OAM frame failed due to system error";
        case VTSS_APPL_MEP_RC_TCAM_CONFIGURATION:     return "The TCAM configuration failed. This could be due to lack of ACL resources";
        case VTSS_APPL_MEP_RC_INTERNAL:               return "Internal error. Some internal configuration fail";
        case VTSS_APPL_MEP_RC_VOE_CONFIG_ERROR:       return "Too configured VOE based MEPs on a port in a flow or VOE based MEP on wrong level.";
        case VTSS_APPL_MEP_RC_VOE_MPLS_ERROR:         return "VOE is not supported on MEP MPLS domains";
        case VTSS_APPL_MEP_RC_MEAS_INTERVAL:          return "Invalid LM measurement interval";
        case VTSS_APPL_MEP_RC_INVALID_BIN_NUMBER:     return "Invalid MEP DM bin number";
        case VTSS_APPL_MEP_RC_INVALID_LEVEL:          return "Invalid level for this instance";
        case VTSS_APPL_MEP_RC_INVALID_VOE:            return "This instance cannot be VOE based. See help on VOE selection";
        case VTSS_APPL_MEP_RC_EVC_VID_SUPPORT:        return "EVC subscriber MEP not supported, vid can't be configured in EVC domain MEP";
        // By not specifying a "default:" we get lint or compiler warnings when not all are specified.
    }

    return "Unknown error code";
}

mesa_rc mep_acl_ace_add(acl_user_t user_id, mesa_ace_id_t next_id, acl_entry_conf_t *conf)
{
    mesa_rc          rc, rc_del;
    acl_entry_conf_t get_conf;

    T_D("Enter");

    memset(&get_conf, 0, sizeof(get_conf));

    get_conf.conflict = FALSE;
    if (next_id != ACL_MGMT_ACE_ID_NONE) { /* This ACL is not added last. To check for conflicts we must try and add it last first */
        if ((rc = acl_mgmt_ace_add(user_id,  ACL_MGMT_ACE_ID_NONE,  conf)) == VTSS_RC_OK) {    /* Add ACL last */
            get_conf.conflict = FALSE;
            if ((rc = acl_mgmt_ace_get(user_id,  VTSS_ISID_LOCAL,  conf->id,  &get_conf,  NULL,  FALSE)) != VTSS_RC_OK) { /* Get the ACL again to check for confict */
                T_W("acl_mgmt_ace_get() failed for this  id %u  rc %s", conf->id, error_txt(rc));
            }
        } else {
            T_W("acl_mgmt_ace_add() failed for this  id %u  rc %s", conf->id, error_txt(rc));
        }
        if ((rc_del = acl_mgmt_ace_del(user_id, conf->id)) != VTSS_RC_OK) {  /* Always try and delete this entry as it is only for testing conflict */
            T_W("acl_mgmt_ace_del() failed for this  id %u  rc %s", conf->id, error_txt(rc_del));
            return VTSS_RC_ERROR;
        }
        if ((rc != VTSS_RC_OK) || get_conf.conflict) {  /* The ACL add or get did fail or there is a conflict. Return an error. */
            T_D("Exit  add failed or conflict");
            return VTSS_RC_ERROR;
        }
    }

    if ((rc = acl_mgmt_ace_add(user_id,  next_id,  conf)) == VTSS_RC_OK) {
        get_conf.conflict = FALSE;
        if ((rc = acl_mgmt_ace_get(user_id,  VTSS_ISID_LOCAL,  conf->id,  &get_conf,  NULL,  FALSE)) != VTSS_RC_OK) {
            T_W("acl_mgmt_ace_get() failed for this  id %u  rc %s", conf->id, error_txt(rc));
        }
    } else {
        T_W("acl_mgmt_ace_add() failed for this  id %u  rc %s", conf->id, error_txt(rc));
    }

    if ((rc != VTSS_RC_OK) || get_conf.conflict) { /* The ACL add or get did fail or there is a conflict. Return an error. */
        if ((rc = acl_mgmt_ace_del(user_id, conf->id)) != VTSS_RC_OK)
            T_W("acl_mgmt_ace_del() failed for this  id %u  rc %s", conf->id, error_txt(rc));
        T_D("Exit  add failed or conflict");
        return VTSS_RC_ERROR;
    }

    T_D("Exit");

    return rc;
}

static void los_state_set(const u32 port,     const BOOL state)
{
    u32 i, j, los=FALSE;

//printf("los_state_set  port %u  state %u\n", port, state);
    CRIT_ENTER(crit_p);
    if (los_state[port] != state)
    {/* Only do this if state changed */
        los_state[port] = state;

        if (in_port_conv[port] != 0xFFFF)       /* If we don't discard on this port due to port protection */
            conv_los_state[in_port_conv[port]] = state;
        conv_los_state[port] = state;
//printf("conv_los_state  %u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u\n", conv_los_state[0], conv_los_state[1], conv_los_state[2], conv_los_state[3], conv_los_state[4], conv_los_state[5], conv_los_state[6], conv_los_state[7], conv_los_state[8], conv_los_state[9], conv_los_state[10]);
        for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
        {
            if (!instance_data[i].enable)   continue;
            if ((instance_data[i].domain == VTSS_APPL_MEP_PORT || MPLS_DOMAIN(instance_data[i].domain)) && instance_data[i].port[port]) { /* Port (or MPLS) Domain instance with this port as a related port */
                CRIT_EXIT(crit_p);
                vtss_mep_new_ssf_state(i, state);
                CRIT_ENTER(crit_p);
            }
            if (((instance_data[i].domain == VTSS_APPL_MEP_EVC) || (instance_data[i].domain == VTSS_APPL_MEP_VLAN)) && (in_port_conv[port] != 0xFFFF) && (instance_data[i].port[in_port_conv[port]]))
            {/* EVC domain and NOT discarding due to port protection and port is related to this MEP */
                for (j=0; j<mep_caps.mesa_port_cnt; ++j)  /* Calculate SSF for this mep instance based on los state on all related ports */
                    if ((instance_data[i].port[j]) && (!conv_los_state[j]))      break;
                los = (j == mep_caps.mesa_port_cnt);      /* LOS is detected if no related port without LOS active was found */
                CRIT_EXIT(crit_p);
                vtss_mep_new_ssf_state(i, los);
                CRIT_ENTER(crit_p);
            }
            if ((instance_data[i].res_port == port) && !instance_data[i].port[port]) { /* Check if this is the residence port of an Up-MEP */
                CRIT_EXIT(crit_p);
                (void)vtss_mep_mgmt_is_tlv_change_set(i);
                CRIT_ENTER(crit_p);
            }
        }
    }
    CRIT_EXIT(crit_p);
}

/*lint -e{454, 455, 456} ... The mutex is locked so it is ok to unlock */
static void prot_state_change(const u32 port)   /* This port is now active in the port protection */
{
    u32 i, j, los;

/*T_D("port %lu", port);*/
    conv_los_state[in_port_conv[port]] = los_state[port];   /* Calculate the port state on the active port */
//printf("conv_los_state  %u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u\n", conv_los_state[0], conv_los_state[1], conv_los_state[2], conv_los_state[3], conv_los_state[4], conv_los_state[5], conv_los_state[6], conv_los_state[7], conv_los_state[8], conv_los_state[9], conv_los_state[10], conv_los_state[11]);

    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)    /* For all EVC MEP 'behind' this port protection */
        if ((instance_data[i].enable) && ((instance_data[i].domain == VTSS_APPL_MEP_EVC) || (instance_data[i].domain == VTSS_APPL_MEP_VLAN)) && (instance_data[i].port[in_port_conv[port]]))
        {/* MEP is enabled in EVC domaine and port is related to this MEP */
            for (j=0; j < mep_caps.mesa_port_cnt; ++j)  /* Calculate SSF for this mep instance based on los state on all related ports */
                if ((instance_data[i].port[j]) && (!conv_los_state[j]))      break;
            los = (j == mep_caps.mesa_port_cnt);      /* LOS is detected if no related port without LOS active was found */
            CRIT_EXIT(crit_p);
            vtss_mep_new_ssf_state(i,  los);
            CRIT_ENTER(crit_p);
        }
}

static void restore_to_default(void)
{
    uint i, rc;
    mep_default_conf_t  def_conf;
    vtss_appl_mep_g8113_2_bfd_auth_conf_t bfd_auth;

    vtss_mep_mgmt_default_conf_get(&def_conf);

    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
    {
        if ((rc = vtss_mep_mgmt_conf_set(i, &def_conf.config)) == VTSS_MEP_RC_VOLATILE)
            rc = vtss_mep_mgmt_volatile_conf_set(i, &def_conf.config);
        if (rc != VTSS_MEP_RC_OK)
            T_D("Error returned from MEP %u\n", i);
    }

    if (vtss_mep_mgmt_tlv_conf_set(&def_conf.tlv_conf) != VTSS_RC_OK) {
            T_D("Error returned from TLV configuration\n");
    }

    memset(&bfd_auth, 0, sizeof(bfd_auth));
    for (i = 0; i < VTSS_APPL_MEP_BFD_AUTH_KEYS_MAX; ++i) {
        rc = vtss_appl_mep_g8113_2_bfd_auth_conf_set(i, &bfd_auth);
        if (rc != VTSS_MEP_RC_OK)
            T_D("Error returned from BFD auth %u\n", i);
    }

    CRIT_ENTER(crit_p);
    vtss_clear(instance_data);

    for (i=0; i<mep_caps.mesa_port_cnt; ++i) {
        in_port_conv[i] = out_port_conv[i] = i;
        conv_los_state[i] = FALSE;
        out_port_1p1[i] = FALSE;
        out_port_tx[i] = TRUE;
    }

    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
    {
        instance_data[i].aps_inst = APS_INST_INV;
        instance_data[i].domain = VTSS_APPL_MEP_PORT;
    }

    CRIT_EXIT(crit_p);
}


static void mep_timer_thread(vtss_addrword_t data)
{
    BOOL              stop, supp_stop;
    vtss_flag_value_t flag_value;
    vtss_mtimer_t     onesec_timer;
    mesa_vlan_conf_t  vlan_conf;
    mesa_etype_t      s_etype;
    struct timeval    now;
    u32               now_ms, next_ms, t = 10;

    stop = supp_stop = FALSE;
    s_etype = 0;

    VTSS_MTIMER_START(&onesec_timer, 1000);

    (void)gettimeofday(&now, NULL);
    now_ms = 1000 * now.tv_sec + now.tv_usec / 1000;
    next_ms = now_ms + 10;
    while (1) {
        if (t > 0) VTSS_OS_MSLEEP(t);
        (void)gettimeofday(&now, NULL);
        now_ms = 1000 * now.tv_sec + now.tv_usec / 1000;
        if ((now_ms - next_ms) > 1000) {
            next_ms = now_ms + 10;
            t = 10;
        } else {
            t = 10 - (now_ms - next_ms);
            if (t > 10) {
                t = 0;
            }
            next_ms += 10;
        }

        flag_value = vtss_flag_poll(&timer_wait_flag, FLAG_TIMER | FLAG_TIMER_SUPP, VTSS_FLAG_WAITMODE_OR_CLR);

        if (!stop || (flag_value & FLAG_TIMER)) {
            vtss_mep_timer_thread(&stop);
        }

        if (!supp_stop || (flag_value & FLAG_TIMER_SUPP)){
            vtss_mep_supp_timer_thread(&supp_stop);
        }

        if (VTSS_MTIMER_TIMEOUT(&onesec_timer)) {
            /* Polling of the Custom S-Tag Ether Type is done once a second - as there are currently no call back on this */
            VTSS_MTIMER_START(&onesec_timer, 1000);

            if (mesa_vlan_conf_get(NULL, &vlan_conf) == VTSS_RC_OK) { /* Get the custom S-Port EtherType */
                if (s_etype != vlan_conf.s_etype) { /* The custom S-Port EtherType has changed */
                    s_etype = vlan_conf.s_etype;
                    vlan_custom_ethertype_callback();
                }
            }
        }
    }
}

void ccm_timer_expired(struct vtss::Timer *timer)
{
    vtss_flag_setbits(&run_wait_flag, FLAG_CCM);
}

static void mep_run_thread(vtss_addrword_t data)
{
    vtss_flag_value_t       flags;
    vtss::Timer             ccm_timer;

    if (mep_caps.mesa_phy_ts) {
        u32                i;
        vtss_tick_count_t  wakeup = vtss_current_time() + VTSS_OS_MSEC2TICK(100);
        vtss_flag_t        control_flags;

        vtss_flag_init(&control_flags);
        while (!tod_ready()) {
            T_I("wait until TOD module is ready");
            flags = vtss_flag_timed_wait(&control_flags, 0xffff, (vtss_flag_mode_t)0, wakeup);
            T_I("wait until TOD module is ready (flags %x)", flags);
            wakeup += VTSS_OS_MSEC2TICK(100);
        }

        port_data_initialize();
        /* Initial related rule for PHY TS */
#if 1
        u8      mac[VTSS_APPL_MEP_MAC_LENGTH];
        mesa_rc rc;
        for (i = 0; i < mep_caps.mesa_port_cnt; i++) {
            if (mep_phy_ts_port[i].phy_ts_port == TRUE) {
                vtss_mep_mac_get(i, mac);  /* Get MAC of recidence port */
                (void)vtss_mep_phy_config_mac(i, 1, 1, mac); /* Add port mac to ingress side */
                (void)vtss_mep_phy_config_mac(i, 1, 0, mac); /* Add port mac to egress side */
#ifdef MEP_DATA_DUMP
                phy_ts_dump(i);
#endif
                rc = mep_phy_ts_update(i);
                if (rc != VTSS_RC_OK) {
                    T_E("Error during config PHY timeStamp");
                }
            }
        }
#endif
    }

    ccm_timer.set_repeat(FALSE);
    ccm_timer.set_period(vtss::milliseconds(10));
    ccm_timer.callback    = ccm_timer_expired;
    ccm_timer.modid       = VTSS_MODULE_ID_MEP;
    if (vtss_timer_start(&ccm_timer) != VTSS_RC_OK) {
        T_D("vtss_timer_start failed");
    }

    vtss_thread_create(VTSS_THREAD_PRIO_HIGHER,
                       mep_timer_thread,
                       0,
                       "MEP Timer",
                       nullptr,
                       0,
                       &timer_thread_handle,
                       &timer_thread_block);

    while (1) {
        flags = vtss_flag_wait(&run_wait_flag, FLAG_RUN | FLAG_MSTP | FLAG_CCM, VTSS_FLAG_WAITMODE_OR_CLR);
        if (!flags)        T_D("Thread was released");
        if (flags & FLAG_RUN) {
            vtss_mep_supp_run_thread();
            vtss_mep_run_thread();
        }
        if (flags & FLAG_MSTP) {
            (void)vtss_mep_mgmt_ps_tlv_change_set();
        }
        if (flags & FLAG_CCM) {
            vtss_mep_ccm_thread();
            if (vtss_timer_start(&ccm_timer) != VTSS_RC_OK)
                T_D("vtss_timer_start failed");
        }
    }
}


static mesa_rc rc_conv(u32 base_rc)
{
    switch (base_rc) {
        case VTSS_MEP_RC_OK:                    return VTSS_RC_OK;
        case VTSS_MEP_RC_INVALID_PARAMETER:     return VTSS_APPL_MEP_RC_INVALID_PARAMETER;
        case VTSS_MEP_RC_NOT_ENABLED:           return VTSS_APPL_MEP_RC_NOT_ENABLED;
        case VTSS_MEP_RC_CAST:                  return VTSS_APPL_MEP_RC_CAST;
        case VTSS_MEP_RC_RATE_INTERVAL:         return VTSS_APPL_MEP_RC_RATE_INTERVAL;
        case VTSS_MEP_RC_PEER_CNT:              return VTSS_APPL_MEP_RC_PEER_CNT;
        case VTSS_MEP_RC_PEER_ID:               return VTSS_APPL_MEP_RC_PEER_ID;
        case VTSS_MEP_RC_MIP:                   return VTSS_APPL_MEP_RC_MIP;
        case VTSS_MEP_RC_INVALID_EVC:           return VTSS_APPL_MEP_RC_INVALID_EVC;
        case VTSS_MEP_RC_INVALID_PRIO:          return VTSS_APPL_MEP_RC_INVALID_PRIO;
        case VTSS_MEP_RC_APS_UP:                return VTSS_APPL_MEP_RC_APS_UP;
        case VTSS_MEP_RC_APS_DOMAIN:            return VTSS_APPL_MEP_RC_APS_DOMAIN;
        case VTSS_MEP_RC_INVALID_VID:           return VTSS_APPL_MEP_RC_INVALID_VID;
        case VTSS_MEP_RC_INVALID_COS_ID:        return VTSS_APPL_MEP_RC_INVALID_COS_ID;
        case VTSS_MEP_RC_NO_VOE:                return VTSS_APPL_MEP_RC_NO_VOE;
        case VTSS_MEP_RC_NO_TIMESTAMP_DATA:     return VTSS_APPL_MEP_RC_NO_TIMESTAMP_DATA;
        case VTSS_MEP_RC_PEER_MAC:              return VTSS_APPL_MEP_RC_PEER_MAC;
        case VTSS_MEP_RC_INVALID_INSTANCE:      return VTSS_APPL_MEP_RC_INVALID_INSTANCE;
        case VTSS_MEP_RC_INVALID_MEG:           return VTSS_APPL_MEP_RC_INVALID_MEG;
        case VTSS_MEP_RC_PROP_SUPPORT:          return VTSS_APPL_MEP_RC_PROP_SUPPORT;
        case VTSS_MEP_RC_VOLATILE:              return VTSS_APPL_MEP_RC_VOLATILE;
        case VTSS_MEP_RC_VLAN_SUPPORT:          return VTSS_APPL_MEP_RC_VLAN_SUPPORT;
        case VTSS_MEP_RC_CLIENT_MAX_LEVEL:      return VTSS_APPL_MEP_RC_CLIENT_MAX_LEVEL;
        case VTSS_MEP_RC_INVALID_CLIENT_LEVEL:  return VTSS_APPL_MEP_RC_INVALID_CLIENT_LEVEL;
        case VTSS_MEP_RC_MIP_SUPPORT:           return VTSS_APPL_MEP_RC_MIP_SUPPORT;
        case VTSS_MEP_RC_INVALID_MAC:           return VTSS_APPL_MEP_RC_INVALID_MAC;
        case VTSS_MEP_RC_CHANGE_PARAMETER:      return VTSS_APPL_MEP_RC_CHANGE_PARAMETER;
        case VTSS_MEP_RC_NO_LOOP_PORT:          return VTSS_APPL_MEP_RC_NO_LOOP_PORT;
        case VTSS_MEP_RC_INVALID_PORT:          return VTSS_APPL_MEP_RC_INVALID_PORT;
        case VTSS_MEP_RC_INVALID_HW_CCM:        return VTSS_APPL_MEP_RC_INVALID_HW_CCM;
        case VTSS_MEP_RC_LST:                   return VTSS_APPL_MEP_RC_LST;
        case VTSS_MEP_RC_NNI_PORT:              return VTSS_APPL_MEP_RC_NNI_PORT;
        case VTSS_MEP_RC_TEST:                  return VTSS_APPL_MEP_RC_TEST;
        case VTSS_MEP_RC_E_TREE:                return VTSS_APPL_MEP_RC_E_TREE;
        case VTSS_MEP_RC_CC_BFD_CONFLICT:       return VTSS_APPL_MEP_RC_CC_BFD_CONFLICT;
        case VTSS_MEP_RC_INVALID_VOE_CHANGE:    return VTSS_APPL_MEP_RC_INVALID_VOE_CHANGE;
        case VTSS_MEP_RC_TTL_ZERO:              return VTSS_APPL_MEP_RC_TTL_ZERO;
        case VTSS_MEP_RC_LM_DUAL_ENDED_RATE_EXCEED_CC_RATE: return VTSS_APPL_MEP_RC_LM_DUAL_ENDED_RATE_EXCEED_CC_RATE;
        case VTSS_MEP_RC_OUT_OF_TX_RESOURCE:    return VTSS_APPL_MEP_RC_OUT_OF_TX_RESOURCE;
        case VTSS_MEP_RC_TX_ERROR:              return VTSS_APPL_MEP_RC_TX_ERROR;
        case VTSS_MEP_RC_TCAM_CONFIGURATION:    return VTSS_APPL_MEP_RC_TCAM_CONFIGURATION;
        case VTSS_MEP_RC_INTERNAL:              return VTSS_APPL_MEP_RC_INTERNAL;
        case VTSS_MEP_RC_VOE_CONFIG_ERROR:      return VTSS_APPL_MEP_RC_VOE_CONFIG_ERROR;
        case VTSS_MEP_RC_VOE_MPLS_ERROR:        return VTSS_APPL_MEP_RC_VOE_MPLS_ERROR;
        case VTSS_MEP_RC_MEAS_INTERVAL:         return VTSS_APPL_MEP_RC_MEAS_INTERVAL;
        case VTSS_MEP_RC_INVALID_LEVEL:         return VTSS_APPL_MEP_RC_INVALID_LEVEL;
        case VTSS_MEP_RC_INVALID_VOE:           return VTSS_APPL_MEP_RC_INVALID_VOE;
        case VTSS_MEP_RC_EVC_VID_SUPPORT:       return VTSS_APPL_MEP_RC_EVC_VID_SUPPORT;
        // By not specifying a "default:" we get lint or compiler warnings when not all are specified.
    }

    return (VTSS_RC_OK);
}

/****************************************************************************/
/*  MEP management interface                                                */
/****************************************************************************/
static mesa_rc conv_conf_ext_int(const vtss_appl_mep_conf_t *a, mep_conf_t *b) {
    vtss_ifindex_elm_t flow, port;
    b->enable = a->enable;
    b->mode = a->mode;
    b->direction = a->direction;

    VTSS_RC(vtss_ifindex_decompose(a->flow, &flow));
    b->flow = flow.ordinal;
    switch (flow.iftype) {
        case VTSS_IFINDEX_TYPE_PORT:
            b->domain = VTSS_APPL_MEP_PORT;
            break;

        case VTSS_IFINDEX_TYPE_VLAN:
            b->domain = VTSS_APPL_MEP_VLAN;
            break;

        case VTSS_IFINDEX_TYPE_EVC:
            b->domain = VTSS_APPL_MEP_EVC;
            break;

        case VTSS_IFINDEX_TYPE_MPLS_LINK:
            b->domain = VTSS_APPL_MEP_MPLS_LINK;
            break;

        case VTSS_IFINDEX_TYPE_MPLS_TUNNEL:
            b->domain = VTSS_APPL_MEP_MPLS_TUNNEL;
            break;

        case VTSS_IFINDEX_TYPE_MPLS_PW:
            b->domain = VTSS_APPL_MEP_MPLS_PW;
            break;

        case VTSS_IFINDEX_TYPE_MPLS_LSP:
            b->domain = VTSS_APPL_MEP_MPLS_LSP;
            break;

        default:
           return VTSS_RC_ERROR;
    }

    VTSS_RC(vtss_ifindex_decompose(a->port, &port));
    switch (port.iftype) {
        case VTSS_IFINDEX_TYPE_PORT:
            b->port = port.ordinal;
            break;

        default:
           return VTSS_RC_ERROR;
    }

    b->level = a->level;
    b->vid = a->vid;
    b->voe = a->voe;
    b->mac = a->mac;
    b->format = a->format;
    memcpy((void *)b->name, a->name, sizeof(b->name));
    memcpy((void *)b->meg, a->meg, sizeof(b->meg));
    b->mep = a->mep;
    b->peer_count = a->peer_count;
    memcpy((void *)b->peer_mep, a->peer_mep, sizeof(b->mep));
    memcpy((void *)b->peer_mac, a->peer_mac, sizeof(b->mac));
    b->out_of_service = a->out_of_service;
    b->evc_pag = a->evc_pag;
    b->evc_qos = a->evc_qos;
    return VTSS_RC_OK;
}

static mesa_rc conv_conf_int_ext(const mep_conf_t *a, vtss_appl_mep_conf_t *b) {
    b->enable = a->enable;
    b->mode = a->mode;
    b->direction = a->direction;
    b->port = VTSS_IFINDEX_NONE;    /* Indication that the following ifindex could not be calculated */
    b->flow = VTSS_IFINDEX_NONE;    /* Indication that the following ifindex could not be calculated */
    (void)vtss_ifindex_from_port(VTSS_ISID_LOCAL, a->port, &b->port);

    switch (a->domain) {
        case VTSS_APPL_MEP_PORT:
            (void)vtss_ifindex_from_port(VTSS_ISID_LOCAL, a->flow, &b->flow);
            break;

        case VTSS_APPL_MEP_VLAN:
            (void)vtss_ifindex_from_vlan(a->flow, &b->flow);
            break;

        case VTSS_APPL_MEP_EVC:
            (void)vtss_ifindex_from_evc(a->flow, &b->flow);
            break;

        case VTSS_APPL_MEP_MPLS_LINK:
            (void)vtss_ifindex_from_mpls_link(a->flow, &b->flow);
            break;

        case VTSS_APPL_MEP_MPLS_TUNNEL:
            (void)vtss_ifindex_from_mpls_tunnel(a->flow, &b->flow);
            break;

        case VTSS_APPL_MEP_MPLS_PW:
            (void)vtss_ifindex_from_mpls_pw(a->flow, &b->flow);
            break;

        case VTSS_APPL_MEP_MPLS_LSP:
            (void)vtss_ifindex_from_mpls_lsp(a->flow, &b->flow);
            break;
    }

    b->level = a->level;
    b->vid = a->vid;
    b->voe = a->voe;
    b->mac = a->mac;
    b->format = a->format;
    memcpy((void *)b->name, a->name, sizeof(b->name));
    memcpy((void *)b->meg, a->meg, sizeof(b->meg));
    b->mep = a->mep;
    b->peer_count = a->peer_count;
    memcpy((void *)b->peer_mep, a->peer_mep, sizeof(b->peer_mep));
    memcpy((void *)b->peer_mac, a->peer_mac, sizeof(b->peer_mac));
    b->out_of_service = a->out_of_service;
    b->evc_pag = a->evc_pag;
    b->evc_qos = a->evc_qos;
    return VTSS_RC_OK;
}

static mesa_rc conv_def_int_ext(const mep_default_conf_t *a, vtss_appl_mep_default_conf_t *b) {
    (void)conv_conf_int_ext(&a->config, &b->config);
    b->peer_conf = a->peer_conf;
    b->pm_conf = a->pm_conf;
    b->syslog_conf = a->syslog_conf;
    b->tlv_conf = a->tlv_conf;
    b->cc_conf = a->cc_conf;
    b->lm_conf = a->lm_conf;
    b->lm_avail_conf = a->lm_avail_conf;
    b->lm_hli_conf = a->lm_hli_conf;
    b->lm_sdeg_conf = a->lm_sdeg_conf;
    b->dm_conf = a->dm_conf;
    b->aps_conf = a->aps_conf;
    b->lt_conf = a->lt_conf;
    b->lb_conf = a->lb_conf;
    b->ais_conf = a->ais_conf;
    b->lck_conf = a->lck_conf;
    b->tst_conf = a->tst_conf;
    b->tst_common_conf = a->tst_common_conf;
    b->lb_common_conf = a->lb_common_conf;
    b->test_prio_conf = a->test_prio_conf;
    b->dm_common_conf = a->dm_common_conf;
    b->dm_prio_conf = a->dm_prio_conf;
    //b->client_conf = a->client_conf; // TODO
    b->client_flow_conf = a->client_flow_conf;
    b->bfd_conf = a->bfd_conf;
    b->rt_conf = a->rt_conf;
    return VTSS_RC_OK;
}

mesa_rc conv_mep_client_flow_conf_ext_to_int(vtss_ifindex_t ifindex, u32 *flow, mep_domain_t *domain)
{
    vtss_ifindex_elm_t ifindex_elm;

    VTSS_RC(vtss_ifindex_decompose(ifindex, &ifindex_elm));
    *flow = ifindex_elm.ordinal;
    switch (ifindex_elm.iftype) {
        case VTSS_IFINDEX_TYPE_PORT:         *domain = VTSS_APPL_MEP_PORT;        break;
        case VTSS_IFINDEX_TYPE_VLAN:         *domain = VTSS_APPL_MEP_VLAN;        break;
        case VTSS_IFINDEX_TYPE_EVC:          *domain = VTSS_APPL_MEP_EVC;         break;
        case VTSS_IFINDEX_TYPE_MPLS_LINK:    *domain = VTSS_APPL_MEP_MPLS_LINK;   break;
        case VTSS_IFINDEX_TYPE_MPLS_TUNNEL:  *domain = VTSS_APPL_MEP_MPLS_TUNNEL; break;
        case VTSS_IFINDEX_TYPE_MPLS_PW:      *domain = VTSS_APPL_MEP_MPLS_PW;     break;
        case VTSS_IFINDEX_TYPE_MPLS_LSP:     *domain = VTSS_APPL_MEP_MPLS_LSP;    break;
        default: return VTSS_RC_ERROR;
    }
    return VTSS_RC_OK;
}

mesa_rc conv_mep_client_flow_conf_int_to_ext(u32 flow, mep_domain_t domain, vtss_ifindex_t *ifindex)
{
    switch (domain) {
        case VTSS_APPL_MEP_PORT:        VTSS_RC(vtss_ifindex_from_port(VTSS_ISID_LOCAL, flow, ifindex)); break;
        case VTSS_APPL_MEP_VLAN:        VTSS_RC(vtss_ifindex_from_vlan(flow, ifindex)); break;
        case VTSS_APPL_MEP_EVC:         VTSS_RC(vtss_ifindex_from_evc(flow, ifindex)); break;
        case VTSS_APPL_MEP_MPLS_LINK:   VTSS_RC(vtss_ifindex_from_mpls_link(flow, ifindex)); break;
        case VTSS_APPL_MEP_MPLS_TUNNEL: VTSS_RC(vtss_ifindex_from_mpls_tunnel(flow, ifindex)); break;
        case VTSS_APPL_MEP_MPLS_PW:     VTSS_RC(vtss_ifindex_from_mpls_pw(flow, ifindex)); break;
        case VTSS_APPL_MEP_MPLS_LSP:    VTSS_RC(vtss_ifindex_from_mpls_lsp(flow, ifindex)); break;
        default:                        return VTSS_RC_ERROR;
    }
    return VTSS_RC_OK;
}

void mep_default_conf_get(mep_default_conf_t *const def_conf)
{
    vtss_mep_mgmt_default_conf_get(def_conf);
}

void vtss_appl_mep_default_conf_get(vtss_appl_mep_default_conf_t *const def_conf) {
    mep_default_conf_t c;
    mep_default_conf_get(&c);
    (void)conv_def_int_ext(&c, def_conf);
}

mesa_rc vtss_appl_mep_tlv_conf_set(const vtss_appl_mep_tlv_conf_t    *const conf)
{
    if (conf == NULL)        return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_tlv_conf_set(conf));
}

mesa_rc vtss_appl_mep_tlv_conf_get(vtss_appl_mep_tlv_conf_t    *const conf)
{
    if (conf == NULL)        return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_tlv_conf_get(conf));
}

mesa_rc mep_status_eps_get(const u32       instance,
                           mep_eps_state_t *const state)
{
    u32 i, inx, aps_count = 0;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER(crit_p);
    if (instance_data[instance].aps_inst != APS_INST_INV) {
        state->inst[0] = instance_data[instance].aps_inst;   /* APS EPS instance number is first */
        aps_count = 1;
    }
    for (i = 0, inx = aps_count; i < instance_data[instance].ssf_count; i++) /* SSF EPS instance numbers - but not if also APS instance */ {
        if ((aps_count == 0) || (instance_data[instance].ssf_inst[i] != instance_data[instance].aps_inst)) {
            state->inst[inx] = instance_data[instance].ssf_inst[i];
            inx++;
        }
    }
    state->count = inx;
    CRIT_EXIT(crit_p);

    return (VTSS_RC_OK);
}

mesa_rc mep_dm_db_state_get(const u32                   instance,
                            u32                         *delay,
                            u32                         *delay_var)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    return rc_conv(vtss_mep_mgmt_dm_db_state_get(instance, delay, delay_var));
}

mesa_rc mep_dm_timestamp_get(const u32           instance,
                             mep_dm_timestamp_t  *const dm1_timestamp_far_to_near,
                             mep_dm_timestamp_t  *const dm1_timestamp_near_to_far)
{
    return rc_conv(vtss_mep_mgmt_dm_timestamp_get(instance, dm1_timestamp_far_to_near, dm1_timestamp_near_to_far));
}

mesa_rc mep_volatile_conf_set(const u32         instance,
                              const mep_conf_t *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return VTSS_APPL_MEP_RC_INVALID_INSTANCE;
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER(crit_p);
    if (conf->enable)
    {
        instance_data[instance].domain = conf->domain;
        instance_data[instance].flow = conf->flow;
        instance_data[instance].res_port = conf->port;
        instance_data[instance].vid = conf->vid;
    }
    else
        instance_data[instance].aps_type = VTSS_APPL_MEP_INV_APS;
    CRIT_EXIT(crit_p);

    if ((rc = rc_conv(vtss_mep_mgmt_volatile_conf_set(instance, conf))) == VTSS_RC_OK)
    {
        CRIT_ENTER(crit_p);
        instance_data[instance].enable = conf->enable;
        CRIT_EXIT(crit_p);
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}


mesa_rc mep_volatile_sat_conf_set(const u32              instance,
                                  const mep_sat_conf_t   *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return VTSS_APPL_MEP_RC_INVALID_INSTANCE;
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER(crit_p);
    if (conf->conf.enable)
    {
        instance_data[instance].domain = conf->conf.domain;
        instance_data[instance].flow = conf->conf.flow;
        instance_data[instance].res_port = conf->conf.port;
        instance_data[instance].vid = conf->conf.vid;
    }
    else
        instance_data[instance].aps_type = VTSS_APPL_MEP_INV_APS;
    CRIT_EXIT(crit_p);

    if ((rc = rc_conv(vtss_mep_mgmt_volatile_sat_conf_set(instance, conf))) == VTSS_RC_OK)
    {
        CRIT_ENTER(crit_p);
        instance_data[instance].enable = conf->conf.enable;
        CRIT_EXIT(crit_p);
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc mep_volatile_sat_conf_get(const u32        instance,
                                  mep_sat_conf_t   *const conf)
{
    T_N("instance %u  conf %p", instance, conf);

    if (conf != NULL) {
        return rc_conv(vtss_mep_mgmt_volatile_sat_conf_get(instance, conf));
    }

    return(VTSS_RC_OK);
}


mesa_rc mep_volatile_sat_counter_get(const u32           instance,
                                     mep_sat_counters_t  *const counters)
{
    T_N("instance %u  counters %p", instance, counters);

    if (counters != NULL) {
        return rc_conv(vtss_mep_mgmt_volatile_sat_counter_get(instance, counters));
    }

    return(VTSS_RC_OK);
}

mesa_rc mep_volatile_sat_counter_clear(const u32  instance)
{
    T_N("instance %u", instance);

    return rc_conv(vtss_mep_mgmt_volatile_sat_counter_clear_set(instance));
}

mesa_rc mep_slm_counters_get(const u32            instance,
                             const u32            peer_idx,
                             sl_service_counter_t *const counters)
{
    if (vtss_mep_supp_slm_counters_get(instance, peer_idx, counters) != VTSS_RC_OK)
        return VTSS_RC_ERROR;

    return VTSS_RC_OK;
}

#ifdef VTSS_SW_OPTION_AFI
mesa_rc mep_tx_bps_actual_get(const u32     instance,
                              mep_tx_bps_t  *const bps)
{
    u32                 i, j, tx_idx, rc;
    instance_data_t     *data;
    tx_buffer_t         *tx_buffer;
    afi_multi_status_t  multi_status;

    T_N("instance %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return VTSS_APPL_MEP_RC_INVALID_INSTANCE;
    if (bps == NULL)                                 return VTSS_RC_OK;

    memset(bps, 0, sizeof(*bps));

    CRIT_ENTER(crit_p);
    data = &instance_data[instance];    /* Instance data reference */

    for (tx_idx=0; tx_idx<TX_BUF_MAX; ++tx_idx) { /* Find a active multi AFI tx_buffer element */
        if (data->tx_buffer[tx_idx].used && data->tx_buffer[tx_idx].multi) {
            break;
        }
    }

    T_NG(TRACE_GRP_TX_FRAME, "tx_idx %u", tx_idx);
    if (tx_idx < TX_BUF_MAX) { /* tx_buffer element was found */
        tx_buffer = &instance_data[instance].tx_buffer[tx_idx];
        for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {  /* Get actual BPS from all valid AFI */
            for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
                if (tx_buffer->port[0].data[i][j].afi_id != AFI_ID_NONE) {  /* Valid AFI id */

                    T_NG(TRACE_GRP_TX_FRAME, "afi_id %u  i %u  j %u", tx_buffer->port[0].data[i][j].afi_id, i, j);
                    if ((rc = afi_multi_status_get(&tx_buffer->port[0].data[i][j].afi_id, &multi_status, FALSE)) != VTSS_RC_OK) {
                        T_DG(TRACE_GRP_TX_FRAME, "afi_multi_status_get() failed %s", error_txt(rc));
                        CRIT_EXIT(crit_p);
                        return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
                    }

                    if (multi_status.bps_act == 0) {
                        T_EG(TRACE_GRP_TX_FRAME, "bps_act is 0");
                        CRIT_EXIT(crit_p);
                        return VTSS_RC_ERROR;
                    }

                    T_NG(TRACE_GRP_TX_FRAME, "bps (L%c) = " VPRI64u, multi_status.params.line_rate ? '1' : '2', multi_status.bps_act);
                    bps->tx_bps[i][j] = multi_status.bps_act;
                }
            }
        }
    }

    CRIT_EXIT(crit_p);

    return VTSS_RC_OK;
}
#endif /* VTSS_SW_OPTION_AFI */

mesa_rc vtss_appl_mep_capabilities_get(vtss_appl_mep_capabilities_t *const conf)
{
    T_N("conf %p", conf);

    conf->instance_max = VTSS_APPL_MEP_INSTANCE_MAX;
    conf->peer_max = mep_caps.mesa_oam_peer_cnt;
    conf->transaction_max = VTSS_APPL_MEP_TRANSACTION_MAX;
    conf->reply_max = VTSS_APPL_MEP_REPLY_MAX;
    conf->client_flows_max = VTSS_APPL_MEP_CLIENT_FLOWS_MAX;
    conf->mep_id_max = VTSS_APPL_MEP_ID_MAX;
    conf->mac_length = VTSS_APPL_MEP_MAC_LENGTH;
    conf->meg_code_length = VTSS_APPL_MEP_MEG_CODE_LENGTH;
    conf->dm_interval_min = VTSS_APPL_MEP_DM_INTERVAL_MIN;
    conf->dm_interval_max = VTSS_APPL_MEP_DM_INTERVAL_MAX;
    conf->dm_lastn_min = VTSS_APPL_MEP_DM_LASTN_MIN;
    conf->dm_lastn_max = VTSS_APPL_MEP_DM_LASTN_MAX;
    conf->dm_bin_max = VTSS_APPL_MEP_DM_BINNING_MAX;
    conf->dm_bin_fd_max = VTSS_APPL_MEP_DM_BINS_FD_MAX;
    conf->dm_bin_fd_min = VTSS_APPL_MEP_DM_BINS_FD_MIN;
    conf->dm_bin_ifdv_max = VTSS_APPL_MEP_DM_BINS_IFDV_MAX;
    conf->dm_bin_ifdv_min = VTSS_APPL_MEP_DM_BINS_IFDV_MIN;
    conf->lbm_size_max = VTSS_APPL_MEP_LBM_SIZE_MAX;
    conf->lbm_size_min = VTSS_APPL_MEP_LBM_SIZE_MIN;
    conf->lbm_mpls_size_max = VTSS_APPL_MEP_LBM_MPLS_SIZE_MAX;
    conf->tst_size_max = VTSS_APPL_MEP_TST_SIZE_MAX;
    conf->tst_size_min = VTSS_APPL_MEP_TST_SIZE_MIN;
    conf->tst_rate_max = VTSS_APPL_MEP_TST_RATE_MAX;
    conf->tst_mpls_size_max = VTSS_APPL_MEP_TST_MPLS_SIZE_MAX;
    conf->test_flow_max = VTSS_APPL_MEP_TEST_MAX;
    conf->slm_size_max = VTSS_APPL_MEP_SLM_SIZE_MAX;
    conf->slm_size_min = VTSS_APPL_MEP_SLM_SIZE_MIN;
    conf->client_prio_highest = VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST;
    conf->has_itu_g8113_2 = VTSS_APPL_MEP_ITU_8113_2_CAPABILITY;
    conf->has_voe_support = VTSS_APPL_MEP_VOE_SUPPORTED;
    conf->lb_to_send_infinite = VTSS_APPL_MEP_LB_TO_SEND_INFINITE;
    conf->bfd_auth_key_max = VTSS_APPL_MEP_BFD_AUTH_KEYS_MAX;
    conf->has_evc_pag = VTSS_APPL_MEP_EVC_PAG_CAPABILITY;
    conf->has_evc_qos = VTSS_APPL_MEP_EVC_QOS_CAPABILITY;
    conf->has_flow_count = VTSS_APPL_MEP_FLOW_COUNT_CAPABILITY;
    conf->has_oam_count = VTSS_APPL_MEP_OAM_COUNT_CAPABILITY;
    conf->has_oper_state = mep_caps.appl_mep_oper_state;
    conf->has_oam_sw_support = VTSS_APPL_MEP_SW_OAM_SUPPORT;

    return (VTSS_RC_OK);
}

mesa_rc mep_instance_conf_set(const u32 instance, const mep_conf_t *const conf)
{
    mesa_rc rc;

    T_N("instance %u  conf %p  enable %u", instance, conf, (conf != NULL) ? conf->enable : 0xFFFFFFFF);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return VTSS_APPL_MEP_RC_INVALID_INSTANCE;
    if (conf == NULL)                                return VTSS_RC_OK;

    // We need a non-const version (lconf) to update the port parameter
    mep_conf_t lconf = *conf;






















































    if (lconf.domain == VTSS_APPL_MEP_MPLS_LINK) {
        T_D("link domain not supported");
        return (VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    } else if (lconf.domain == VTSS_APPL_MEP_MPLS_TUNNEL) {
        T_D("tunnel domain not supported");
        return (VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    } else if (lconf.domain == VTSS_APPL_MEP_MPLS_PW) {
        T_D("PW domain not supported");
        return (VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    } else if (lconf.domain == VTSS_APPL_MEP_MPLS_LSP) {
        T_D("LSP domain not supported");
        return (VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    }


    CRIT_ENTER(crit_p);
    if (lconf.enable)
    {
        instance_data[instance].domain = lconf.domain;
        instance_data[instance].flow = lconf.flow;
        instance_data[instance].res_port = lconf.port;
        instance_data[instance].vid = lconf.vid;
    }
    else {
        instance_data[instance].aps_type = VTSS_APPL_MEP_INV_APS;
    }
    CRIT_EXIT(crit_p);

    if ((rc = rc_conv(vtss_mep_mgmt_conf_set(instance, &lconf))) == VTSS_RC_OK) {

        CRIT_ENTER(crit_p);
        instance_data[instance].enable = lconf.enable;
        CRIT_EXIT(crit_p);







    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc vtss_appl_mep_instance_conf_set(u32 instance, const vtss_appl_mep_conf_t *const conf)
{
    mep_conf_t c = {0};
    T_N("instance %u", instance);
    VTSS_RC(conv_conf_ext_int(conf, &c));
    return mep_instance_conf_set(instance, &c);
}

mesa_rc mep_instance_conf_get(const u32 instance, mep_conf_t *const conf)
{
    T_N("instance %u  conf %p", instance, conf);

    if (conf != NULL) {
        return rc_conv(vtss_mep_mgmt_conf_get(instance, conf));
    }

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_instance_conf_get(const u32 instance, vtss_appl_mep_conf_t *const conf) {
    mep_conf_t c;
    mesa_rc    rc;

    T_N("instance %u  conf %p", instance, conf);
    rc = mep_instance_conf_get(instance, &c);
    (void)conv_conf_int_ext(&c, conf);

    return rc;
}

mesa_rc mep_instance_conf_add(const u32 instance, const mep_conf_t *const conf)
{
    mep_conf_t  config;

    T_N("instance %u", instance);

    if (conf == NULL)                                return VTSS_RC_OK;
    config = *conf;
    config.enable = TRUE;
    if (mep_instance_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    T_N("OK");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_instance_conf_add(u32 instance, const vtss_appl_mep_conf_t *const conf) {
    mep_conf_t c = {0};
    VTSS_RC(conv_conf_ext_int(conf, &c));
    return mep_instance_conf_add(instance, &c);
}

mesa_rc vtss_appl_mep_instance_conf_delete(const u32  instance)
{
    mep_conf_t  config;

    T_N("instance %u", instance);

    if ((mep_instance_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (mep_instance_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    T_N("OK\n");
    return (VTSS_RC_OK);
}


mesa_rc vtss_appl_mep_instance_conf_iter(const u32    *const instance,
                                         u32          *const next)
{
    mep_conf_t  config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (config.enable)  /* If the MEP is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}


mesa_rc vtss_appl_mep_instance_status_iter(const u32    *const instance,
                                           u32          *const next)
{
    vtss_appl_mep_state_t status;

    if (!instance) { // get-first
        return mep_status_get_first(next, &status);
    } else {
        *next = *instance;
        return mep_status_get_next(next, &status);
    }
}


mesa_rc vtss_appl_mep_lm_notif_status_iter(const u32    *const instance,
                                           u32          *const next)
{
    vtss_appl_mep_lm_notif_state_t status;

    if (!instance) { // get-first
        return mep_status_lm_notif_get_first(next, &status);
    } else {
        *next = *instance;
        return mep_status_lm_notif_get_next(next, &status);
    }
}


mesa_rc vtss_appl_mep_lm_hli_status_iter(const u32  *const instance,
                                         u32        *const next_instance,
                                         const u32  *const peer,
                                         u32        *const next_peer)
{
    vtss_appl_mep_lm_hli_state_t status;

    if (!instance) { // get-first
        return mep_status_lm_hli_get_first(next_instance, next_peer, &status);
    } else {
        *next_instance = *instance;
        return mep_status_lm_hli_get_next(next_instance, next_peer, &status);
    }
}


mesa_rc vtss_appl_mep_instance_peer_status_iter(const u32  *const instance,
                                                u32        *const next_instance,
                                                const u32  *const peer,
                                                u32        *const next_peer)
{
    vtss_appl_mep_peer_state_t  status;

    if (!instance)  // get-first
        return mep_status_peer_get_first(next_instance, next_peer, &status);

    *next_instance = *instance;
    if (!peer) {  // get-first level-2
        *next_peer = 0;
        if (mep_status_peer_get(*next_instance, *next_peer, &status) == VTSS_RC_OK) {
            return VTSS_RC_OK;
        }
        return(mep_status_peer_get_next(next_instance, next_peer, &status));
    }

    *next_peer = *peer;
    return mep_status_peer_get_next(next_instance, next_peer, &status);
}


mesa_rc vtss_appl_mep_instance_peer_conf_add(const u32                        instance,
                                             const u32                        peer,
                                             const vtss_appl_mep_peer_conf_t  *const conf)
{
    mep_conf_t  config;
    u32 i;

    T_N("instance %u  peer %u", instance, peer);

    if ((mep_instance_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    if (!config.enable)  /* MEP must be created */
        return (VTSS_RC_ERROR);

    if (config.peer_count >= mep_caps.mesa_oam_peer_cnt)  /* Not room for any more peer MEP */
        return (VTSS_RC_ERROR);

    for (i=0; i<config.peer_count; ++i) /* Search for peer MEP-ID */
        if (config.peer_mep[i] == peer)
            break;

    if (i < config.peer_count)  /* Peer MEP-ID was found */
        return (VTSS_RC_ERROR);

    config.peer_mep[i] = peer;
    config.peer_mac[i] = conf->mac;
    config.peer_count += 1;

    if (mep_instance_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    T_N("OK");
    return (VTSS_RC_OK);
}


mesa_rc vtss_appl_mep_instance_peer_conf_delete(const u32  instance,
                                                const u32  peer)
{
    mep_conf_t  config;
    u32                   i, idx;
    u16                   peer_mep[mep_caps.mesa_oam_peer_cnt];
    mesa_mac_t            peer_mac[mep_caps.mesa_oam_peer_cnt];

    T_N("instance %u  peer %u", instance, peer);

    if ((mep_instance_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    if (!config.enable)  /* MEP must be created */
        return (VTSS_RC_ERROR);

    memset(peer_mep, 0, sizeof(peer_mep));
    memset(peer_mac, 0, sizeof(peer_mac));

    for (i=0, idx=0; i<config.peer_count; ++i) {
        if (config.peer_mep[i] != peer) {
            peer_mep[idx] = config.peer_mep[i];
            peer_mac[idx] = config.peer_mac[i];
            idx++;
        }
    }

    memcpy(config.peer_mep, peer_mep, sizeof(config.peer_mep));
    memcpy(config.peer_mac, peer_mac, sizeof(config.peer_mac));
    config.peer_count = idx;

    if (mep_instance_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    T_N("OK");
    return (VTSS_RC_OK);
}


mesa_rc vtss_appl_mep_instance_peer_conf_set(const u32                        instance,
                                             const u32                        peer,
                                             const vtss_appl_mep_peer_conf_t  *const conf)
{
    mep_conf_t  config;
    u32 i;

    T_N("instance %u  peer %u", instance, peer);

    if ((mep_instance_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    if (!config.enable)  /* MEP must be created */
        return (VTSS_RC_ERROR);

    for (i=0; i<config.peer_count; ++i) /* Search for peer MEP-ID */
        if (config.peer_mep[i] == peer)
            break;

    if (i < config.peer_count) { /* Peer MEP-ID was  found */
        config.peer_mac[i] = conf->mac; /* Configure MAC */

        if (mep_instance_conf_set(instance, &config) != VTSS_RC_OK)
            return (VTSS_RC_ERROR); /* Not able to set MEP */
    } else /* Did not find peer MEP-ID */
        return (VTSS_RC_ERROR);

    T_N("OK");
    return (VTSS_RC_OK);
}


mesa_rc vtss_appl_mep_instance_peer_conf_get(const u32                  instance,
                                             const u32                  peer,
                                             vtss_appl_mep_peer_conf_t  *const conf)
{
    mep_conf_t  config;
    u32 i;

    T_N("instance %u  peer %u", instance, peer);

    if ((mep_instance_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    for (i=0; i<config.peer_count; ++i) {/* Search for peer MEP-ID */
        if (config.peer_mep[i] == peer)
            break;
    }

    if (i < config.peer_count) { /* Peer MEP-ID was  found */
        conf->mac = config.peer_mac[i];
    } else /* Did not find peer MEP-ID */
        return (VTSS_RC_ERROR);

    T_N("OK\n");
    return (VTSS_RC_OK);
}


mesa_rc vtss_appl_mep_instance_peer_conf_iter(const u32    *const instance,
                                              u32          *const next_instance,
                                              const u32    *const peer,
                                              u32          *const next_peer)
{
    mep_conf_t  config;
    u32 i, j, min;

    T_N("instance %p  peer %p  *instance %u  *peer %u", instance, peer, (instance) ? *instance : 0, (peer) ? *peer : 0);

    if (instance == NULL) {  /* Get the first enabled instance as requested */
        if (vtss_appl_mep_instance_conf_iter(instance, next_instance) == VTSS_RC_ERROR)
            return(VTSS_RC_ERROR);
    } else
        *next_instance = *instance;

    /* Now we have an enabled instance to start with */

    if (peer) { /* Calculate the starting point for the search for next peer */
        *next_peer = *peer + 1;
    } else {
        *next_peer = 0;
    }

    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i) { /* This loop should be terminated through a return of error or ok */
        if ((mep_instance_conf_get(*next_instance, &config)) != VTSS_RC_OK)  /* get configuration */
            return (VTSS_RC_ERROR);

        if (!config.enable)  /* This should not be possible */
            return (VTSS_RC_ERROR);

        for (j=0, min=0xFFFFFFFF; j<config.peer_count; ++j) {/* Search for then minimum peer MEP-ID */
            if ((config.peer_mep[j] >= *next_peer) && (config.peer_mep[j] < min))
                min = config.peer_mep[j];
        }

        if (min == 0xFFFFFFFF) { /* The end of peer for this instance */
            *next_peer = 0;  /* The peer MEP index always start in '0' for the next instance */
            if (vtss_appl_mep_instance_conf_iter(next_instance,  next_instance) == VTSS_RC_ERROR)  /* Get next enabled instance */
                return(VTSS_RC_ERROR);
        } else {
            T_N("OK");
            return(VTSS_RC_OK); /* The next instance is found with the next peer MEP */
        }
    }

    return (VTSS_RC_ERROR);
}


mesa_rc vtss_appl_mep_pm_conf_set(const u32                     instance,
                                  const vtss_appl_mep_pm_conf_t *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_pm_conf_set(instance, conf))) == VTSS_RC_OK) {
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc vtss_appl_mep_pm_conf_get(const u32                 instance,
                                  vtss_appl_mep_pm_conf_t   *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_pm_conf_get(instance, conf));
}

mesa_rc vtss_appl_mep_pm_conf_iter(const u32    *const instance,
                                   u32          *const next)
{
    mep_conf_t               config;
    vtss_appl_mep_pm_conf_t  pm_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for PM active */
            continue;
        if ((vtss_appl_mep_pm_conf_get(*next, &pm_config)) != VTSS_RC_OK)
            continue;
        if (pm_config.enable)  /* If PM is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}


mesa_rc vtss_appl_mep_lst_conf_set(const u32                      instance,
                                   const vtss_appl_mep_lst_conf_t *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_lst_conf_set(instance, conf))) == VTSS_RC_OK) {
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc vtss_appl_mep_lst_conf_get(const u32                  instance,
                                   vtss_appl_mep_lst_conf_t   *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_lst_conf_get(instance, conf));
}

mesa_rc vtss_appl_mep_lst_conf_iter(const u32    *const instance,
                                    u32          *const next)
{
    vtss_appl_mep_conf_t      config;
    vtss_appl_mep_lst_conf_t  lst_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((vtss_appl_mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for LST active */
            continue;
        if ((vtss_appl_mep_lst_conf_get(*next, &lst_config)) != VTSS_RC_OK)
            continue;
        if (lst_config.enable)  /* If LST is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}


mesa_rc vtss_appl_mep_syslog_conf_set(const u32                     instance,
                                      const vtss_appl_mep_syslog_conf_t *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_syslog_conf_set(instance, conf))) == VTSS_RC_OK) {
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc vtss_appl_mep_syslog_conf_get(const u32                 instance,
                                      vtss_appl_mep_syslog_conf_t   *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_syslog_conf_get(instance, conf));
}

mesa_rc vtss_appl_mep_syslog_conf_iter(const u32    *const instance,
                                       u32          *const next)
{
    mep_conf_t                   config;
    vtss_appl_mep_syslog_conf_t  syslog_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for PM active */
            continue;
        if ((vtss_appl_mep_syslog_conf_get(*next, &syslog_config)) != VTSS_RC_OK)
            continue;
        if (syslog_config.enable)  /* If Syslog is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}


mesa_rc vtss_appl_mep_cc_conf_set(const u32                        instance,
                                  const vtss_appl_mep_cc_conf_t    *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_cc_conf_set(instance, conf))) == VTSS_RC_OK) {
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc vtss_appl_mep_cc_conf_get(const u32                 instance,
                                  vtss_appl_mep_cc_conf_t   *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_cc_conf_get(instance, conf));
}

mesa_rc vtss_appl_mep_cc_conf_add(const u32                       instance,
                                  const vtss_appl_mep_cc_conf_t  *const conf)
{
    vtss_appl_mep_cc_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_cc_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_cc_conf_delete(const u32  instance)
{
    vtss_appl_mep_cc_conf_t  config;

    if ((vtss_appl_mep_cc_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_cc_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_cc_conf_iter(const u32    *const instance,
                                   u32          *const next)
{
    mep_conf_t               config;
    vtss_appl_mep_cc_conf_t  cc_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for CC active */
            continue;
        if ((vtss_appl_mep_cc_conf_get(*next, &cc_config)) != VTSS_RC_OK)
            continue;
        if (cc_config.enable)  /* If CC is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lm_conf_set(const u32                      instance,
                                  const vtss_appl_mep_lm_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_lm_conf_set(instance, conf))) == VTSS_RC_OK) {
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc vtss_appl_mep_lm_conf_get(const u32                instance,
                                  vtss_appl_mep_lm_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_lm_conf_get(instance, conf));
}

mesa_rc vtss_appl_mep_lm_conf_add(const u32                       instance,
                                  const vtss_appl_mep_lm_conf_t  *const conf)
{
    vtss_appl_mep_lm_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_lm_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_conf_delete(const u32  instance)
{
    vtss_appl_mep_lm_conf_t  config;

    if ((vtss_appl_mep_lm_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_lm_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_conf_iter(const u32    *const instance,
                                   u32          *const next)
{
    mep_conf_t               config;
    vtss_appl_mep_lm_conf_t  lm_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for LM active */
            continue;
        if ((vtss_appl_mep_lm_conf_get(*next, &lm_config)) != VTSS_RC_OK)
            continue;
        if (lm_config.enable)  /* If LM is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lm_avail_conf_set(const u32                           instance,
                                        const vtss_appl_mep_lm_avail_conf_t *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_lm_avail_conf_set(instance, conf))) == VTSS_RC_OK) {
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc vtss_appl_mep_lm_avail_conf_get(const u32                      instance,
                                        vtss_appl_mep_lm_avail_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_lm_avail_conf_get(instance, conf));
}

mesa_rc vtss_appl_mep_lm_avail_conf_add(const u32                            instance,
                                        const vtss_appl_mep_lm_avail_conf_t  *const conf)
{
    vtss_appl_mep_lm_avail_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_lm_avail_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_avail_conf_delete(const u32  instance)
{
    vtss_appl_mep_lm_avail_conf_t  config;

    if ((vtss_appl_mep_lm_avail_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_lm_avail_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_avail_conf_iter(const u32    *const instance,
                                         u32          *const next)
{
    mep_conf_t                     config;
    vtss_appl_mep_lm_avail_conf_t  lm_avail_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for LM active */
            continue;
        if ((vtss_appl_mep_lm_avail_conf_get(*next, &lm_avail_config)) != VTSS_RC_OK)
            continue;
        if (lm_avail_config.enable)  /* If LM Availability is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lm_hli_conf_set(const u32                           instance,
                                      const vtss_appl_mep_lm_hli_conf_t *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_lm_hli_conf_set(instance, conf))) == VTSS_RC_OK) {
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc vtss_appl_mep_lm_hli_conf_get(const u32                      instance,
                                      vtss_appl_mep_lm_hli_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_lm_hli_conf_get(instance, conf));
}

mesa_rc vtss_appl_mep_lm_hli_conf_add(const u32                            instance,
                                      const vtss_appl_mep_lm_hli_conf_t  *const conf)
{
    vtss_appl_mep_lm_hli_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_lm_hli_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_hli_conf_delete(const u32  instance)
{
    vtss_appl_mep_lm_hli_conf_t  config;

    if ((vtss_appl_mep_lm_hli_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_lm_hli_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_hli_conf_iter(const u32    *const instance,
                                       u32          *const next)
{
    mep_conf_t                   config;
    vtss_appl_mep_lm_hli_conf_t  lm_hli_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for LM active */
            continue;
        if ((vtss_appl_mep_lm_hli_conf_get(*next, &lm_hli_config)) != VTSS_RC_OK)
            continue;
        if (lm_hli_config.enable)  /* If LM HLI is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lm_sdeg_conf_set(const u32                          instance,
                                       const vtss_appl_mep_lm_sdeg_conf_t *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_lm_sdeg_conf_set(instance, conf))) == VTSS_RC_OK) {
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc vtss_appl_mep_lm_sdeg_conf_get(const u32                     instance,
                                       vtss_appl_mep_lm_sdeg_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_lm_sdeg_conf_get(instance, conf));
}

mesa_rc vtss_appl_mep_lm_sdeg_conf_add(const u32                           instance,
                                       const vtss_appl_mep_lm_sdeg_conf_t  *const conf)
{
    vtss_appl_mep_lm_sdeg_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_lm_sdeg_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_sdeg_conf_delete(const u32  instance)
{
    vtss_appl_mep_lm_sdeg_conf_t  config;

    if ((vtss_appl_mep_lm_sdeg_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_lm_sdeg_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_sdeg_conf_iter(const u32    *const instance,
                                        u32          *const next)
{
    mep_conf_t                   config;
    vtss_appl_mep_lm_sdeg_conf_t lm_sdeg_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for LM active */
            continue;
        if ((vtss_appl_mep_lm_sdeg_conf_get(*next, &lm_sdeg_config)) != VTSS_RC_OK)
            continue;
        if (lm_sdeg_config.enable)  /* If LM SDEG is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

 mesa_rc vtss_appl_mep_dm_conf_set(const u32                      instance,
                                  const vtss_appl_mep_dm_conf_t  *const conf)
{
    mesa_rc rc;
    u32     i;
    vtss_mep_mgmt_dm_conf_t base_conf;

    T_DG(TRACE_GRP_DM, "Instance %u  enable %u  prio %u", instance, conf->enable, conf->prio);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return (VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return (VTSS_RC_OK);
    if (conf->prio >= VTSS_APPL_MEP_PRIO_MAX)        return (VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    if ((rc = rc_conv(vtss_mep_mgmt_dm_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {  /* Disable DM for all priorities */
        base_conf.dm[i].enable = FALSE;
    }

    base_conf.dm[conf->prio].enable = conf->enable;      /* Configure DM for the selected priority */
    base_conf.prio = conf->prio;  /* This is only to support old DM interface */
    base_conf.common.cast = conf->cast;
    base_conf.common.mep = conf->mep;
    base_conf.common.ended = conf->ended;
    base_conf.common.calcway = conf->calcway;
    base_conf.common.interval = conf->interval;
    base_conf.common.lastn = conf->lastn;
    base_conf.common.tunit = conf->tunit;
    base_conf.common.overflow_act = conf->overflow_act;
    base_conf.common.synchronized = conf->synchronized;
    base_conf.common.proprietary = conf->proprietary;
    base_conf.common.num_of_bin_fd = conf->num_of_bin_fd;
    base_conf.common.num_of_bin_ifdv = conf->num_of_bin_ifdv;
    base_conf.common.m_threshold = conf->m_threshold;

    if ((rc = rc_conv(vtss_mep_mgmt_dm_conf_set(instance, &base_conf))) != VTSS_RC_OK)
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc vtss_appl_mep_dm_conf_get(const u32                instance,
                                  vtss_appl_mep_dm_conf_t  *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_dm_conf_t base_conf;

    T_DG(TRACE_GRP_DM, "Instance %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_dm_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    conf->enable = base_conf.dm[base_conf.prio].enable;
    conf->prio = base_conf.prio;
    conf->cast = base_conf.common.cast;
    conf->mep = base_conf.common.mep;
    conf->ended = base_conf.common.ended;
    conf->calcway = base_conf.common.calcway;
    conf->interval = base_conf.common.interval;
    conf->lastn = base_conf.common.lastn;
    conf->tunit = base_conf.common.tunit;
    conf->overflow_act = base_conf.common.overflow_act;
    conf->synchronized = base_conf.common.synchronized;
    conf->proprietary = base_conf.common.proprietary;
    conf->num_of_bin_fd = base_conf.common.num_of_bin_fd;
    conf->num_of_bin_ifdv = base_conf.common.num_of_bin_ifdv;
    conf->m_threshold = base_conf.common.m_threshold;

    T_DG(TRACE_GRP_DM, "Instance %u  int-tunit %u  ret-tunit %u", instance, base_conf.common.tunit, conf->tunit);

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_conf_add(const u32                       instance,
                                  const vtss_appl_mep_dm_conf_t  *const conf)
{
    vtss_appl_mep_dm_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_dm_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_conf_delete(const u32  instance)
{
    vtss_appl_mep_dm_conf_t  config;

    if ((vtss_appl_mep_dm_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_dm_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_conf_iter(const u32    *const instance,
                                   u32          *const next)
{
    mep_conf_t               config;
    vtss_appl_mep_dm_conf_t  dm_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for DM active */
            continue;
        if ((vtss_appl_mep_dm_conf_get(*next, &dm_config)) != VTSS_RC_OK)
            continue;
        if (dm_config.enable)  /* If DM is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_dm_control_get(const u32                   instance,
                                     vtss_appl_mep_dm_control_t  *const control)
{
    control->clear = FALSE;

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_control_set(const u32                         instance,
                                     const vtss_appl_mep_dm_control_t  *const control)
{
    if (!control->clear)
        return (VTSS_RC_OK);

    if ((vtss_appl_mep_dm_status_clear(instance)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR);

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_common_conf_set(const u32                             instance,
                                         const vtss_appl_mep_dm_common_conf_t  *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_dm_conf_t base_conf;

    T_DG(TRACE_GRP_DM, "Instance %u  tunit %u", instance, conf->tunit);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_dm_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    base_conf.common = *conf;

    if ((rc = rc_conv(vtss_mep_mgmt_dm_conf_set(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_prio_conf_get(const                         u32 instance,
                                       const                         u32 prio,
                                       vtss_appl_mep_dm_prio_conf_t  *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_dm_conf_t base_conf;

    T_DG(TRACE_GRP_DM, "Instance %u  prio %u", instance, prio);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;
    if (prio >= VTSS_APPL_MEP_PRIO_MAX)              return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    if ((rc = rc_conv(vtss_mep_mgmt_dm_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    memcpy(conf, &base_conf.dm[prio], sizeof(*conf));

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_prio_conf_set(const                               u32 instance,
                                       const                               u32 prio,
                                       const vtss_appl_mep_dm_prio_conf_t  *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_dm_conf_t base_conf;

    T_DG(TRACE_GRP_DM, "Instance %u  enable %u  prio %u", instance, conf->enable, prio);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;
    if (prio >= VTSS_APPL_MEP_PRIO_MAX)              return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    if ((rc = rc_conv(vtss_mep_mgmt_dm_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    memcpy(&base_conf.dm[prio], conf, sizeof(base_conf.dm[prio]));
    base_conf.prio = prio;  /* This is only to support old TST interface */

    if ((rc = rc_conv(vtss_mep_mgmt_dm_conf_set(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_aps_conf_set(const u32                         instance,
                                   const vtss_appl_mep_aps_conf_t    *const conf)
{
    mesa_rc rc;
    BOOL    invalid = FALSE;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    CRIT_ENTER(crit_p);
    if (conf->enable && (((conf->type == VTSS_APPL_MEP_L_APS) && (instance_data[instance].eps_type == MEP_EPS_TYPE_ERPS)) ||
                         ((conf->type == VTSS_APPL_MEP_R_APS) && (instance_data[instance].eps_type == MEP_EPS_TYPE_ELPS))))
        invalid = TRUE;
    CRIT_EXIT(crit_p);

    if (invalid)        return VTSS_APPL_MEP_RC_APS_PROT_CONNECTED;

    if ((rc = rc_conv(vtss_mep_mgmt_aps_conf_set(instance, conf))) == VTSS_RC_OK) {
        CRIT_ENTER(crit_p);
        instance_data[instance].aps_type = (conf->enable) ? conf->type : VTSS_APPL_MEP_INV_APS;
        CRIT_EXIT(crit_p);
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc vtss_appl_mep_aps_conf_get(const u32                  instance,
                                   vtss_appl_mep_aps_conf_t   *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    rc = rc_conv(vtss_mep_mgmt_aps_conf_get(instance, conf));

    if (!conf->enable)  conf->raps_octet = 1;   /* This is just to give the value '1' as a default value to the "manager" */

    return rc;
}

mesa_rc vtss_appl_mep_aps_conf_add(const u32                       instance,
                                   const vtss_appl_mep_aps_conf_t  *const conf)
{
    vtss_appl_mep_aps_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_aps_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_aps_conf_delete(const u32  instance)
{
    vtss_appl_mep_aps_conf_t  config;

    if ((vtss_appl_mep_aps_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_aps_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_aps_conf_iter(const u32    *const instance,
                                    u32          *const next)
{
    mep_conf_t                config;
    vtss_appl_mep_aps_conf_t  aps_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for APS active */
            continue;
        if ((vtss_appl_mep_aps_conf_get(*next, &aps_config)) != VTSS_RC_OK)
            continue;
        if (aps_config.enable)  /* If APS is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lt_conf_set(const u32                      instance,
                                  const vtss_appl_mep_lt_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_lt_conf_set(instance, conf))) != VTSS_RC_OK)
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc vtss_appl_mep_lt_conf_get(const u32                 instance,
                                  vtss_appl_mep_lt_conf_t   *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_lt_conf_get(instance, conf));
}

mesa_rc vtss_appl_mep_lt_conf_add(const u32                      instance,
                                  const vtss_appl_mep_lt_conf_t  *const conf)
{
    vtss_appl_mep_lt_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_lt_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lt_conf_delete(const u32  instance)
{
    vtss_appl_mep_lt_conf_t  config;

    if ((vtss_appl_mep_lt_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_lt_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lt_conf_iter(const u32    *const instance,
                                   u32          *const next)
{
    mep_conf_t               config;
    vtss_appl_mep_lt_conf_t  lt_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for LT active */
            continue;
        if ((vtss_appl_mep_lt_conf_get(*next, &lt_config)) != VTSS_RC_OK)
            continue;
        if (lt_config.enable)  /* If LT is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lb_conf_set(const u32                      instance,
                                  const vtss_appl_mep_lb_conf_t  *const conf)
{
    mesa_rc rc;
    u32     i, j, test_idx;
    u64     bit_per_sec, frame_per_sec;
    vtss_mep_mgmt_lb_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;
    if (conf->prio >= VTSS_APPL_MEP_PRIO_MAX)   return (VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    if ((rc = rc_conv(vtss_mep_mgmt_lb_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {  /* Disable LB for all priorities */
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            base_conf.tests[i][j].enable = FALSE;
        }
    }

    test_idx = (conf->dei) ? 1 : 0;
    base_conf.tests[conf->prio][test_idx].enable = conf->enable;      /* Configure TST for the selected priority */
    base_conf.tests[conf->prio][test_idx].dp = !conf->dei ? VTSS_APPL_MEP_DP_GREEN : VTSS_APPL_MEP_DP_YELLOW;
    base_conf.tests[conf->prio][test_idx].size = conf->size;



    base_conf.tests[conf->prio][test_idx].rate = 1;
    if (conf->to_send == VTSS_APPL_MEP_LB_TO_SEND_INFINITE) {   /* Bit rate must be used for "infinite" transmission. This should be given on public interface */
        if (conf->interval) {
            frame_per_sec = 1000000/conf->interval;
            bit_per_sec = frame_per_sec * conf->size * 8;
            base_conf.tests[conf->prio][test_idx].rate = bit_per_sec/1000;  /* This is the bit rate in Kbps that give the approx. interval as configured */
            if (base_conf.tests[conf->prio][test_idx].rate > VTSS_APPL_MEP_TST_RATE_MAX)
                base_conf.tests[conf->prio][test_idx].rate = VTSS_APPL_MEP_TST_RATE_MAX;
        }
    }
    base_conf.tests[conf->prio][test_idx].pattern = VTSS_APPL_MEP_PATTERN_0XAA;
    base_conf.prio = conf->prio;  /* This is only to support old TST interface */
    base_conf.test_idx = test_idx;  /* This is only to support old TST interface */
    base_conf.common.cast = conf->cast;
    memcpy(&base_conf.common.mac, &conf->mac, sizeof(base_conf.common.mac));
    base_conf.common.mep = conf->mep;
    base_conf.common.to_send = conf->to_send;
    base_conf.common.interval = conf->interval;

    if ((rc = rc_conv(vtss_mep_mgmt_lb_conf_set(instance, &base_conf, NULL))) != VTSS_RC_OK)
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc vtss_appl_mep_lb_conf_get(const u32                 instance,
                                  vtss_appl_mep_lb_conf_t   *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_lb_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return (VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return (VTSS_RC_OK);

    if ((rc = rc_conv(vtss_mep_mgmt_lb_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    conf->enable = base_conf.tests[base_conf.prio][base_conf.test_idx].enable;
    conf->prio = base_conf.prio;
    conf->dei = (base_conf.tests[base_conf.prio][base_conf.test_idx].dp == VTSS_APPL_MEP_DP_GREEN) ? FALSE : TRUE;
    conf->size = base_conf.tests[base_conf.prio][base_conf.test_idx].size;
    conf->ttl = base_conf.tests[base_conf.prio][base_conf.test_idx].ttl;
    conf->mep = base_conf.common.mep;
    conf->cast = base_conf.common.cast;
    conf->to_send = base_conf.common.to_send;
    memcpy(&conf->mac, &base_conf.common.mac, sizeof(conf->mac));
    conf->interval = base_conf.common.interval;

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lb_conf_add(const u32                       instance,
                                  const vtss_appl_mep_lb_conf_t  *const conf)
{
    vtss_appl_mep_lb_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_lb_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lb_conf_delete(const u32  instance)
{
    vtss_appl_mep_lb_conf_t  config;

    if ((vtss_appl_mep_lb_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_lb_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lb_conf_iter(const u32    *const instance,
                                   u32          *const next)
{
    mep_conf_t               config;
    vtss_appl_mep_lb_conf_t  lb_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for LB active */
            continue;
        if ((vtss_appl_mep_lb_conf_get(*next, &lb_config)) != VTSS_RC_OK)
            continue;
        if (lb_config.enable)  /* If LB is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lb_common_conf_get(const u32                      instance,
                                         vtss_appl_mep_lb_common_conf_t *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_lb_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_lb_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    *conf = base_conf.common;

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lb_common_conf_set(const u32                            instance,
                                         const vtss_appl_mep_lb_common_conf_t *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_lb_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_lb_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    base_conf.common = *conf;

    if ((rc = rc_conv(vtss_mep_mgmt_lb_conf_set(instance, &base_conf, NULL))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lb_prio_conf_get(const                           u32 instance,
                                       const                           u32 prio,
                                       const                           u32 test_idx,
                                       vtss_appl_mep_test_prio_conf_t  *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_lb_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;
    if (prio >= VTSS_APPL_MEP_PRIO_MAX)         return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    if (test_idx >= VTSS_APPL_MEP_TEST_MAX)          return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    if ((rc = rc_conv(vtss_mep_mgmt_lb_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    memcpy(conf, &base_conf.tests[prio][test_idx], sizeof(*conf));

    return (VTSS_RC_OK);
}

mesa_rc mep_volatile_sat_lb_prio_conf_set(const                                 u32 instance,
                                          const                                 u32 prio,
                                          const                                 u32 test_idx,
                                          const vtss_appl_mep_test_prio_conf_t  *const conf,
                                                                                u64 *active_time_ms)
{
    mesa_rc rc;
    vtss_mep_mgmt_lb_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;
    if (prio >= VTSS_APPL_MEP_PRIO_MAX)              return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    if (test_idx >= VTSS_APPL_MEP_TEST_MAX)          return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    if ((rc = rc_conv(vtss_mep_mgmt_lb_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    memcpy(&base_conf.tests[prio][test_idx], conf, sizeof(base_conf.tests[prio][test_idx]));
    base_conf.prio = prio;  /* This is only to support old LB interface */
    base_conf.test_idx = test_idx;  /* This is only to support old LB interface */

    if ((rc = rc_conv(vtss_mep_mgmt_lb_conf_set(instance, &base_conf, active_time_ms))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lb_prio_conf_set(const                                 u32 instance,
                                       const                                 u32 prio,
                                       const                                 u32 test_idx,
                                       const vtss_appl_mep_test_prio_conf_t  *const conf)
{
    // Re-use SAT function, but with last parameter NULL.
    return mep_volatile_sat_lb_prio_conf_set(instance, prio, test_idx, conf, NULL);
}

mesa_rc vtss_appl_mep_ais_conf_set(const u32                      instance,
                                   const vtss_appl_mep_ais_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_ais_conf_set(instance, conf))) == VTSS_RC_OK) {
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc vtss_appl_mep_ais_conf_get(const u32                 instance,
                                   vtss_appl_mep_ais_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_ais_conf_get(instance,conf));
}

mesa_rc vtss_appl_mep_ais_conf_add(const u32                       instance,
                                   const vtss_appl_mep_ais_conf_t  *const conf)
{
    vtss_appl_mep_ais_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_ais_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_ais_conf_delete(const u32  instance)
{
    vtss_appl_mep_ais_conf_t  config;

    if ((vtss_appl_mep_ais_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_ais_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_ais_conf_iter(const u32    *const instance,
                                    u32          *const next)
{
    mep_conf_t                config;
    vtss_appl_mep_ais_conf_t  ais_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for AIS active */
            continue;
        if ((vtss_appl_mep_ais_conf_get(*next, &ais_config)) != VTSS_RC_OK)
            continue;
        if (ais_config.enable)  /* If AIS is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lck_conf_set(const u32                       instance,
                                   const vtss_appl_mep_lck_conf_t  *const conf)
{
    mesa_rc rc;


    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_lck_conf_set(instance, conf))) == VTSS_RC_OK) {
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc vtss_appl_mep_lck_conf_get(const u32                 instance,
                                   vtss_appl_mep_lck_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_lck_conf_get(instance,conf));
}


mesa_rc vtss_appl_mep_lck_conf_add(const u32                       instance,
                                   const vtss_appl_mep_lck_conf_t  *const conf)
{
    vtss_appl_mep_lck_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_lck_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lck_conf_delete(const u32  instance)
{
    vtss_appl_mep_lck_conf_t  config;

    if ((vtss_appl_mep_lck_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_lck_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lck_conf_iter(const u32    *const instance,
                                    u32          *const next)
{
    mep_conf_t                config;
    vtss_appl_mep_lck_conf_t  lck_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for LCK active */
            continue;
        if ((vtss_appl_mep_lck_conf_get(*next, &lck_config)) != VTSS_RC_OK)
            continue;
        if (lck_config.enable)  /* If LCK is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_tst_conf_set(const u32                       instance,
                                   const vtss_appl_mep_tst_conf_t  *const conf)
{
    mesa_rc rc;
    u32     i, j, test_idx;
    vtss_mep_mgmt_tst_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return (VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return (VTSS_RC_OK);
    if (conf->prio >= VTSS_APPL_MEP_PRIO_MAX)   return (VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    if ((rc = rc_conv(vtss_mep_mgmt_tst_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {  /* Disable TST for all priorities */
        for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
            base_conf.tests[i][j].enable = FALSE;
        }
    }

    test_idx = (conf->dei) ? 1 : 0;
    base_conf.tests[conf->prio][test_idx].enable = conf->enable;      /* Configure TST for the selected priority */
    base_conf.tests[conf->prio][test_idx].dp = !conf->dei ? VTSS_APPL_MEP_DP_GREEN : VTSS_APPL_MEP_DP_YELLOW;
    base_conf.tests[conf->prio][test_idx].size = conf->size;
    base_conf.tests[conf->prio][test_idx].rate = conf->rate;
    base_conf.tests[conf->prio][test_idx].pattern = conf->pattern;
    base_conf.prio = conf->prio;  /* This is only to support old TST interface */
    base_conf.test_idx = test_idx;  /* This is only to support old TST interface */
    base_conf.common.enable_rx = conf->enable_rx;
    base_conf.common.mep = conf->mep;
    base_conf.common.sequence = conf->sequence;

    if ((rc = rc_conv(vtss_mep_mgmt_tst_conf_set(instance, &base_conf, NULL))) != VTSS_RC_OK)
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc vtss_appl_mep_tst_conf_get(const u32                  instance,
                                   vtss_appl_mep_tst_conf_t   *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_tst_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_tst_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    conf->enable = base_conf.tests[base_conf.prio][base_conf.test_idx].enable;
    conf->prio = base_conf.prio;
    conf->dei = (base_conf.tests[base_conf.prio][base_conf.test_idx].dp == VTSS_APPL_MEP_DP_GREEN) ? FALSE : TRUE;
    conf->size = base_conf.tests[base_conf.prio][base_conf.test_idx].size;
    conf->rate = base_conf.tests[base_conf.prio][base_conf.test_idx].rate;
    conf->pattern = base_conf.tests[base_conf.prio][base_conf.test_idx].pattern;
    conf->enable_rx = base_conf.common.enable_rx;
    conf->mep = base_conf.common.mep;
    conf->sequence = base_conf.common.sequence;

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_conf_add(const u32                       instance,
                                   const vtss_appl_mep_tst_conf_t  *const conf)
{
    vtss_appl_mep_tst_conf_t  config;

    config = *conf;
    config.enable = TRUE;
    if (vtss_appl_mep_tst_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_conf_delete(const u32  instance)
{
    vtss_appl_mep_tst_conf_t  config;

    if ((vtss_appl_mep_tst_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    config.enable = FALSE;

    if (vtss_appl_mep_tst_conf_set(instance, &config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_conf_iter(const u32    *const instance,
                                    u32          *const next)
{
    mep_conf_t                config;
    vtss_appl_mep_tst_conf_t  tst_config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for TST active */
            continue;
        if ((vtss_appl_mep_tst_conf_get(*next, &tst_config)) != VTSS_RC_OK)
            continue;
        if (tst_config.enable)  /* If TST is enabled the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_tst_control_get(const u32                    instance,
                                      vtss_appl_mep_tst_control_t  *const control)
{
    if (control == NULL)       return VTSS_RC_OK;

    control->clear = FALSE;

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_control_set(const u32                          instance,
                                      const vtss_appl_mep_tst_control_t  *const control)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (control == NULL)                             return VTSS_RC_OK;

    if (!control->clear)
        return (VTSS_RC_OK);

    if ((vtss_appl_mep_tst_status_clear(instance)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR);

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_common_conf_get(const u32                       instance,
                                          vtss_appl_mep_tst_common_conf_t *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_tst_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_tst_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    *conf = base_conf.common;

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_common_conf_set(const u32                             instance,
                                          const vtss_appl_mep_tst_common_conf_t *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_tst_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_tst_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    base_conf.prio = VTSS_APPL_MEP_PRIO_MAX;  /* This is only to support old TST interface */
    base_conf.common = *conf;

    if ((rc = rc_conv(vtss_mep_mgmt_tst_conf_set(instance, &base_conf, NULL))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_tst_prio_conf_get(const                           u32 instance,
                                        const                           u32 prio,
                                        const                           u32 test_idx,
                                        vtss_appl_mep_test_prio_conf_t  *const conf)
{
    mesa_rc rc;
    vtss_mep_mgmt_tst_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;
    if (prio >= VTSS_APPL_MEP_PRIO_MAX)              return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    if (test_idx >= VTSS_APPL_MEP_TEST_MAX)          return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    if ((rc = rc_conv(vtss_mep_mgmt_tst_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    memcpy(conf, &base_conf.tests[prio][test_idx], sizeof(*conf));

    return (VTSS_RC_OK);
}

mesa_rc mep_volatile_sat_tst_prio_conf_set(const                                 u32 instance,
                                           const                                 u32 prio,
                                           const                                 u32 test_idx,
                                           const vtss_appl_mep_test_prio_conf_t  *const conf,
                                                                                 u64 *active_time_ms)
{
    mesa_rc rc;
    vtss_mep_mgmt_tst_conf_t base_conf;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;
    if (prio >= VTSS_APPL_MEP_PRIO_MAX)              return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);
    if (test_idx >= VTSS_APPL_MEP_TEST_MAX)          return(VTSS_APPL_MEP_RC_INVALID_PARAMETER);

    if ((rc = rc_conv(vtss_mep_mgmt_tst_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    memcpy(&base_conf.tests[prio][test_idx], conf, sizeof(base_conf.tests[prio][test_idx]));
    base_conf.prio = prio;  /* This is only to support old TST interface */
    base_conf.test_idx = test_idx;  /* This is only to support old TST interface */

    if ((rc = rc_conv(vtss_mep_mgmt_tst_conf_set(instance, &base_conf, active_time_ms))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    return (VTSS_RC_OK);
}

vtss_rc vtss_appl_mep_tst_prio_conf_set(const                                 u32 instance,
                                        const                                 u32 prio,
                                        const                                 u32 test_idx,
                                        const vtss_appl_mep_test_prio_conf_t  *const conf)
{
    // Re-use SAT function, but with last parameter NULL.
    return mep_volatile_sat_tst_prio_conf_set(instance, prio, test_idx, conf, NULL);
}

mesa_rc mep_client_conf_set(const u32                 instance,
                            const mep_client_conf_t  *const conf)
{
    mesa_rc rc;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_client_conf_set(instance, conf))) == VTSS_RC_OK) {
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return rc;
}

mesa_rc mep_client_conf_get(const u32           instance,
                            mep_client_conf_t  *const conf)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)      return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (conf == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_client_conf_get(instance, conf));
}

mesa_rc vtss_appl_mep_client_flow_conf_add(const u32                                instance,
                                           const vtss_ifindex_t                     flow_in,
                                           const vtss_appl_mep_client_flow_conf_t  *const conf)
{
    mep_client_conf_t  config;
    u32                i, flow;
    mep_domain_t       domain;

    T_N("instance %u  flow_in %u", instance, VTSS_IFINDEX_PRINTF_ARG(flow_in));

    if ((mep_client_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    if (config.flow_count >= VTSS_APPL_MEP_CLIENT_FLOWS_MAX)  /* Not room for any more Client Flows */
        return (VTSS_RC_ERROR);

    VTSS_RC(conv_mep_client_flow_conf_ext_to_int(flow_in, &flow, &domain));

    for (i=0; i<config.flow_count; ++i) {
        if ((config.domain[i] == domain) && (config.flows[i] == flow))  /* Client Flow was found */
            return (VTSS_RC_ERROR);
    }

    config.ais_prio[i]      = conf->ais_prio;
    config.lck_prio[i]      = conf->lck_prio;
    config.level[i]         = conf->level;
    config.flows[i]         = flow;
    config.domain[i] = domain;
    config.flow_count += 1;

    if (mep_client_conf_set(instance, &config) != VTSS_RC_OK) {
        return (VTSS_RC_ERROR); /* Not able to set MEP */
    }

    T_N("OK");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_client_flow_conf_delete(const u32             instance,
                                              const vtss_ifindex_t  flow_in)
{
    mep_client_conf_t  config;
    mep_client_conf_t  new_config;
    u32                i, idx, flow;
    mep_domain_t       domain;

    T_N("instance %u  flow_in %u", instance, VTSS_IFINDEX_PRINTF_ARG(flow_in));

    if ((mep_client_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    VTSS_RC(conv_mep_client_flow_conf_ext_to_int(flow_in, &flow, &domain));

    memset(&new_config, 0, sizeof(new_config));
    for (i=0, idx=0; i<config.flow_count; ++i) {
        if ((config.domain[i] != domain) || (config.flows[i] != flow)) {
            new_config.flows[idx]       = config.flows[i];
            new_config.domain[idx]      = config.domain[i];
            new_config.ais_prio[idx]    = config.ais_prio[i];
            new_config.lck_prio[idx]    = config.lck_prio[i];
            new_config.level[idx]       = config.level[i];
            idx++;
        }
    }
    new_config.flow_count = idx;

    if (mep_client_conf_set(instance, &new_config) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to set MEP */

    T_N("OK");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_client_flow_conf_set(const u32                                instance,
                                           const vtss_ifindex_t                     flow_in,
                                           const vtss_appl_mep_client_flow_conf_t  *const conf)
{
    mep_client_conf_t  config;
    u32                i, flow;
    mep_domain_t       domain;

    T_N("instance %u  flow_in %u", instance, VTSS_IFINDEX_PRINTF_ARG(flow_in));

    if ((mep_client_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    VTSS_RC(conv_mep_client_flow_conf_ext_to_int(flow_in, &flow, &domain));

    for (i=0; i<config.flow_count; ++i) /* Search for Client Flow */
        if ((config.domain[i] == domain) && (config.flows[i] == flow))  /* Client Flow was found */
            break;

    if (i < config.flow_count) { /* Client Flow was found */
        config.ais_prio[i]      = conf->ais_prio;
        config.lck_prio[i]      = conf->lck_prio;
        config.level[i]         = conf->level;
        config.flows[i]         = flow;
        config.domain[i] = domain;

        if (mep_client_conf_set(instance, &config) != VTSS_RC_OK)
            return (VTSS_RC_ERROR); /* Not able to set MEP */
    } else /* Did not find Client Flow */
        return (VTSS_RC_ERROR);

    T_N("OK");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_client_flow_conf_get(const u32                            instance,
                                           const vtss_ifindex_t                 flow_in,
                                           vtss_appl_mep_client_flow_conf_t    *const conf)
{
    mep_client_conf_t  config;
    u32                i, flow;
    mep_domain_t       domain;

    T_N("instance %u  flow_in %u", instance, VTSS_IFINDEX_PRINTF_ARG(flow_in));

    if ((mep_client_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    VTSS_RC(conv_mep_client_flow_conf_ext_to_int(flow_in, &flow, &domain));

    for (i=0; i<config.flow_count; ++i) {/* Search for Client Flow */
        if ((config.domain[i] == domain) && (config.flows[i] == flow))  /* Client Flow was found */
            break;
    }

    if (i < config.flow_count) { /* Client Flow was found */
        conf->ais_prio      = config.ais_prio[i];
        conf->lck_prio      = config.lck_prio[i];
        conf->level         = config.level[i];
    } else /* Did not find Client Flow */
        return (VTSS_RC_ERROR);

    T_N("OK\n");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_client_flow_conf_iter(const u32               *const instance,
                                            u32                     *const next_instance,
                                            const vtss_ifindex_t    *const flow_in,
                                            vtss_ifindex_t          *const next_flow)
{
    mep_client_conf_t  config;
    u32                i, next_flow_i, flow;
    mep_domain_t       domain;

    T_N("instance %p  flow_in %p  *instance %u  *flow_in %u", instance, flow_in, (instance) ? *instance : 0, (flow_in) ? VTSS_IFINDEX_PRINTF_ARG(*flow_in) : 0);

    if (instance == NULL) {  /* Get the first enabled instance as requested */
        if (vtss_appl_mep_instance_conf_iter(instance, next_instance) == VTSS_RC_ERROR)
            return(VTSS_RC_ERROR);
    } else
        *next_instance = *instance;

    /* Now we have an enabled instance to start with */

    if (flow_in) { /* Calculate the starting point for the search for next flow */
        if ((mep_client_conf_get(*next_instance, &config)) != VTSS_RC_OK)  /* get client configuration */
            return (VTSS_RC_ERROR);

        VTSS_RC(conv_mep_client_flow_conf_ext_to_int(*flow_in, &flow, &domain));
        for (i=0; i<config.flow_count; ++i) {/* Search for Client Flow */
            if ((config.domain[i] == domain) && (config.flows[i] == flow))  /* Client Flow was found */
                break;
        }
        if (i == config.flow_count) /* Check if flow was found */
            return (VTSS_RC_ERROR);

        next_flow_i = i + 1;
    } else {
        next_flow_i = 0;
    }

    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i) { /* This loop should be terminated through a return of error or ok */
        if ((mep_client_conf_get(*next_instance, &config)) != VTSS_RC_OK)  /* get configuration */
            return (VTSS_RC_ERROR);

        if (next_flow_i >= config.flow_count) { /* The end of flow for this instance */
            next_flow_i = 0;  /* The flow index always start in '0' for the next instance */
            if (vtss_appl_mep_instance_conf_iter(next_instance,  next_instance) == VTSS_RC_ERROR)  /* Get next enabled instance */
                return(VTSS_RC_ERROR);
        } else {
            T_N("OK");
            VTSS_RC(conv_mep_client_flow_conf_int_to_ext(config.flows[next_flow_i], config.domain[next_flow_i], next_flow));
            return(VTSS_RC_OK); /* The next instance is found with the next Client flow index */
        }
    }

    return (VTSS_RC_ERROR);
}
























































































































































































































































































































































































mesa_rc vtss_appl_mep_g8113_2_bfd_status_get(const u32                         instance,
                                             vtss_appl_mep_g8113_2_bfd_state_t *const status)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_clear_stats(const u32 instance)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_bfd_control_get(const u32                    instance,
                                      vtss_appl_mep_bfd_control_t  *const control)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_bfd_control_set(const u32                          instance,
                                      const vtss_appl_mep_bfd_control_t  *const control)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_conf_set(const u32                              instance,
                                           const vtss_appl_mep_g8113_2_bfd_conf_t *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_conf_get(const u32                        instance,
                                           vtss_appl_mep_g8113_2_bfd_conf_t *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_conf_add(const u32                               instance,
                                           const vtss_appl_mep_g8113_2_bfd_conf_t  *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_conf_delete(const u32  instance)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_conf_iter(const u32    *const instance,
                                            u32          *const next)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_set(const u32                                   key_id,
                                                const vtss_appl_mep_g8113_2_bfd_auth_conf_t *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_get(const u32                             key_id,
                                                vtss_appl_mep_g8113_2_bfd_auth_conf_t *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_add(const u32                                    key_id,
                                                const vtss_appl_mep_g8113_2_bfd_auth_conf_t  *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_delete(const u32 key_id)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_g8113_2_bfd_auth_conf_iter(const u32    *const key_id,
                                                 u32          *const next_key_id)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_rt_conf_set(const u32                      instance,
                                  const vtss_appl_mep_rt_conf_t  *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_rt_conf_get(const u32                 instance,
                                  vtss_appl_mep_rt_conf_t   *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_rt_conf_add(const u32                      instance,
                                  const vtss_appl_mep_rt_conf_t  *const conf)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_rt_conf_delete(const u32  instance)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_rt_conf_iter(const u32    *const instance,
                                   u32          *const next)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_rt_status_get(const u32                  instance,
                                    vtss_appl_mep_rt_status_t  *const status)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_rt_reply_status_get(const u32                 instance,
                                          const u32                 seq_no,
                                          vtss_appl_mep_rt_reply_t  *const status)
{
    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_rt_reply_status_iter(const u32    *const instance,
                                           u32          *const next_instance,
                                           const u32    *const seq_no,
                                           u32          *const next_seq_no)
{
    return (VTSS_RC_ERROR);
}



mesa_rc vtss_appl_mep_instance_status_get(const u32              instance,
                                          vtss_appl_mep_state_t  *const state)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    return mep_status_get(instance, state);
}

mesa_rc vtss_appl_mep_lm_notif_status_get(const u32              instance,
                                          vtss_appl_mep_lm_notif_state_t  *const state)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    return mep_status_lm_notif_get(instance, state);
}

mesa_rc vtss_appl_mep_instance_peer_status_get(const u32                   instance,
                                               const u32                   peer,
                                               vtss_appl_mep_peer_state_t  *const state)
{
    T_N("instance %u  peer %u", instance, peer);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    return mep_status_peer_get(instance, peer, state);
}

mesa_rc vtss_appl_mep_cc_status_get(const u32                    instance,
                                    vtss_appl_mep_cc_state_t     *const state)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_cc_state_get(instance, state));
}

mesa_rc vtss_appl_mep_cc_peer_status_get(const u32                      instance,
                                         const u32                      peer,
                                         vtss_appl_mep_peer_cc_state_t  *const state)
{
    u32 i;
    vtss_appl_mep_cc_state_t cc_state;
    mep_conf_t               config;

    T_N("instance %u  peer %u", instance, peer);

    if (state == NULL)                                return VTSS_RC_OK;

    if ((mep_instance_conf_get(instance, &config)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    for (i=0; i<config.peer_count; ++i) {/* Search for peer MEP-ID */
        if (config.peer_mep[i] == peer)
            break;
    }

    if (i < config.peer_count) { /* Peer MEP-ID was  found */
        if ((vtss_appl_mep_cc_status_get(instance, &cc_state)) != VTSS_RC_OK)
            return (VTSS_RC_ERROR); /* Not able to get MEP */

        state->ps_tlv = cc_state.ps_tlv[i];
        state->is_tlv = cc_state.is_tlv[i];
        state->os_tlv = cc_state.os_tlv[i];
        state->ps_received = cc_state.ps_received[i];
        state->is_received = cc_state.is_received[i];
        state->os_received = cc_state.os_received[i];
    } else /* Did not find peer MEP-ID */
        return (VTSS_RC_ERROR);

    T_N("OK\n");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lt_status_get(const u32                  instance,
                                    vtss_appl_mep_lt_state_t   *const state)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_lt_state_get(instance, state));
}

mesa_rc vtss_appl_mep_lt_transaction_status_get(const u32                        instance,
                                                vtss_appl_mep_lt_transaction_t   *const status)
{
    vtss_appl_mep_lt_state_t state;

    T_N("instance %u", instance);

    if (status == NULL)                                return VTSS_RC_OK;

    if ((vtss_appl_mep_lt_status_get(instance, &state)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    if (state.transaction_cnt == 0) {
        memset(status, 0, sizeof(*status));
    } else {
        *status = state.transaction[state.transaction_cnt-1];
    }

    T_N("OK\n");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lt_transaction_status_iter(const u32    *const instance,
                                                 u32          *const next)
{
    mep_conf_t                config;
    vtss_appl_mep_lt_state_t  state;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_MEP_INSTANCE_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (; *next<VTSS_APPL_MEP_INSTANCE_MAX; ++*next) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next, &config)) != VTSS_RC_OK)
            continue;
        if (!config.enable)  /* If the MEP is enabled check for any LT transactions */
            continue;
        if ((vtss_appl_mep_lt_status_get(*next, &state)) != VTSS_RC_OK)
            continue;
        if (state.transaction_cnt != 0)  /* If LT transactions the search is completed */
            return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_lt_reply_status_get(const u32                 instance,
                                          const u32                 reply,
                                          vtss_appl_mep_lt_reply_t  *const status)
{
    vtss_appl_mep_lt_state_t state;

    T_N("instance %u  reply %u", instance, reply);

    if (status == NULL)                                return VTSS_RC_OK;

    if ((vtss_appl_mep_lt_status_get(instance, &state)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    if (state.transaction_cnt == 0)
        return (VTSS_RC_ERROR); /* No transactions */

    if (reply >= state.transaction[state.transaction_cnt-1].reply_cnt)
        return (VTSS_RC_ERROR); /* Invalid reply id */

    *status = state.transaction[state.transaction_cnt-1].reply[reply];

    T_N("OK\n");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lt_reply_status_iter(const u32    *const instance,
                                           u32          *const next_instance,
                                           const u32    *const reply,
                                           u32          *const next_reply)
{
    vtss_appl_mep_lt_state_t state;
    u32 i;

    T_N("instance %p  reply %p  *instance %u  *reply %u", instance, reply, (instance) ? *instance : 0, (reply) ? *reply : 0);

    if (instance == NULL) {  /* Get the first enabled instance as requested */
        if (vtss_appl_mep_instance_conf_iter(instance, next_instance) == VTSS_RC_ERROR)
            return(VTSS_RC_ERROR);
    } else
        *next_instance = *instance;

    /* Now we have an enabled instance to start with */

    if (reply) { /* Calculate the starting point for the search for next reply */
        *next_reply = *reply + 1;
    } else {
        *next_reply = 0;
    }

    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i) { /* This loop should be terminated through a return of error or ok */
        if ((vtss_appl_mep_lt_status_get(*next_instance, &state)) != VTSS_RC_OK)
            return (VTSS_RC_ERROR); /* Not able to get MEP */

        if ((state.transaction_cnt == 0) ||
            (*next_reply >= state.transaction[state.transaction_cnt-1].reply_cnt)) { /* The end of reply for this instance */
            *next_reply = 0;  /* The reply index always start in '0' for the next instance */
            if (vtss_appl_mep_instance_conf_iter(next_instance,  next_instance) == VTSS_RC_ERROR)  /* Get next enabled instance */
                return(VTSS_RC_ERROR);
        } else {
            T_N("OK");
            return(VTSS_RC_OK); /* The next instance is found with the next LB Reply id */
        }
    }

    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_lb_status_get(const u32                  instance,
                                    vtss_appl_mep_lb_state_t   *const state)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_lb_state_get(instance, state));
}

mesa_rc vtss_appl_mep_lb_reply_status_get(const u32                 instance,
                                          const u32                 reply,
                                          vtss_appl_mep_lb_reply_t  *const status)
{
    vtss_appl_mep_lb_state_t state;

    T_N("instance %u  reply %u", instance, reply);

    if (status == NULL)                                return VTSS_RC_OK;

    if ((vtss_appl_mep_lb_status_get(instance, &state)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not able to get MEP */

    if (reply >= state.reply_cnt)
        return (VTSS_RC_ERROR); /* Invalid reply id */

    *status = state.reply[reply];

    T_N("OK\n");
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lb_reply_status_iter(const u32    *const instance,
                                           u32          *const next_instance,
                                           const u32    *const reply,
                                           u32          *const next_reply)
{
    vtss_appl_mep_lb_state_t state;
    u32 i;

    T_N("instance %p  reply %p  *instance %u  *reply %u", instance, reply, (instance) ? *instance : 0, (reply) ? *reply : 0);

    if (instance == NULL) {  /* Get the first enabled instance as requested */
        if (vtss_appl_mep_instance_conf_iter(instance, next_instance) == VTSS_RC_ERROR)
            return(VTSS_RC_ERROR);
    } else
        *next_instance = *instance;

    /* Now we have an enabled instance to start with */

    if (reply) { /* Calculate the starting point for the search for next reply */
        *next_reply = *reply + 1;
    } else {
        *next_reply = 0;
    }

    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i) { /* This loop should be terminated through a return of error or ok */
        if ((vtss_appl_mep_lb_status_get(*next_instance, &state)) != VTSS_RC_OK)
            return (VTSS_RC_ERROR); /* Not able to get MEP */

        if (*next_reply >= state.reply_cnt) {  /* The end of reply for this instance */
            *next_reply = 0;  /* The reply index always start in '0' for the next instance */
            if (vtss_appl_mep_instance_conf_iter(next_instance,  next_instance) == VTSS_RC_ERROR)  /* Get next enabled instance */
                return(VTSS_RC_ERROR);
        } else {
            T_N("OK");
            return(VTSS_RC_OK); /* The next instance is found with the next LB Reply id */
        }
    }

    return (VTSS_RC_ERROR);
}

mesa_rc vtss_appl_mep_lb_status_clear(const u32 instance)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return (VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    return rc_conv(vtss_mep_mgmt_lb_state_clear_set(instance));
}


mesa_rc vtss_appl_mep_lm_status_get(const u32                   instance,
                                    vtss_appl_mep_lm_state_t    *const state)
{
    mesa_rc       rc = VTSS_RC_OK;
    mep_conf_t    config;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    memset(state, 0, sizeof(*state));

    rc = mep_instance_conf_get(instance, &config);
    if ((rc != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE))
        return (VTSS_RC_ERROR); /* Not enable to get MEP */

    if (config.peer_count == 0)
        return (VTSS_RC_OK); /* No peer MEP - just return all zero status */

    return rc_conv(vtss_mep_mgmt_lm_state_get(instance, config.peer_mep[0], state));  /* Get the LM state from the first (only) peer MEP */
}

mesa_rc vtss_appl_mep_instance_peer_lm_status_get(const u32                 instance,
                                                  const u32                 peer,
                                                  vtss_appl_mep_lm_state_t  *const state)
{
    T_N("instance %u  peer %u", instance, peer);

    if (state == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_lm_state_get(instance, peer, state));  /* Get the LM state from this specific peer MEP */
}

mesa_rc vtss_appl_mep_lm_status_clear(const u32    instance)
{
    return vtss_appl_mep_lm_status_clear_dir(instance, VTSS_APPL_MEP_CLEAR_DIR_BOTH);
}

mesa_rc vtss_appl_mep_lm_status_clear_dir(const uint32_t          instance,
                                          vtss_appl_mep_clear_dir direction)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    return rc_conv(vtss_mep_mgmt_lm_state_clear_set(instance, direction));
}

mesa_rc vtss_appl_mep_lm_control_get(const u32                   instance,
                                     vtss_appl_mep_lm_control_t  *const control)
{
    if (control == NULL)                                return VTSS_RC_OK;

    control->clear = FALSE;

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_control_set(const u32                         instance,
                                     const vtss_appl_mep_lm_control_t  *const control)
{
    if (!control->clear)
        return (VTSS_RC_OK);

    if ((vtss_appl_mep_lm_status_clear(instance)) != VTSS_RC_OK)
        return (VTSS_RC_ERROR);

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_lm_avail_status_get(const u32                         instance,
                                          const u32                         peer_instance,
                                          vtss_appl_mep_lm_avail_state_t    *const state)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_lm_avail_state_get(instance, peer_instance, state));
}

mesa_rc vtss_appl_mep_lm_hli_status_get(const u32                       instance,
                                        const uint32_t                  peer_id,
                                        vtss_appl_mep_lm_hli_state_t    *const state)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)       return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                                return VTSS_RC_OK;

    return mep_status_lm_hli_get(instance, peer_id, state);
}

mesa_rc vtss_appl_mep_dm_status_get(const u32                   instance,
                                    vtss_appl_mep_dm_state_t    *const dmr_state,
                                    vtss_appl_mep_dm_state_t    *const dm1_state_far_to_near,
                                    vtss_appl_mep_dm_state_t    *const dm1_state_near_to_far)
{
    mesa_rc rc;
    vtss_mep_mgmt_dm_conf_t base_conf;

    T_DG(TRACE_GRP_DM, "Instance %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (dmr_state == NULL)                             return VTSS_RC_OK;
    if (dm1_state_far_to_near == NULL)                 return VTSS_RC_OK;
    if (dm1_state_near_to_far == NULL)                 return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_dm_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    return rc_conv(vtss_mep_mgmt_dm_prio_state_get(instance, base_conf.prio, dmr_state, dm1_state_far_to_near, dm1_state_near_to_far));
}

mesa_rc vtss_appl_mep_dm_fd_bin_status_get(const u32                          instance,
                                           const u32                          index,
                                           vtss_appl_mep_dm_fd_bin_state_t    *const status)
{
    mesa_rc                   rc;
    vtss_appl_mep_dm_state_t  dm_state;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                                              return VTSS_APPL_MEP_RC_INVALID_INSTANCE;
    if (status == NULL)                                                                      return VTSS_RC_OK;
    if (index >= VTSS_APPL_MEP_DM_BINS_FD_MAX)                                               return VTSS_APPL_MEP_RC_FD_BIN;
    if ((rc = rc_conv(vtss_appl_mep_dm_tw_status_get(instance, &dm_state))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    status->fd_bin = dm_state.fd_bin[index];
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_fd_bin_status_iter(const u32  *const instance,
                                            u32        *const next_instance,
                                            const u32  *const index,
                                            u32        *const next_index)
{
    mep_conf_t  config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        if (index) {
            if (*index >=(VTSS_APPL_MEP_DM_BINS_FD_MAX-1)) {
                *next_instance = *instance + 1;
                *next_index    = 0;
            } else {
                *next_instance = *instance;
                *next_index    = *index + 1;
            }
        } else {
            *next_instance = *instance;
            *next_index    = 0;
        }
    } else {
        *next_instance = 0;
        *next_index    = 0;
    }

    for (*next_instance=*next_instance; *next_instance<VTSS_APPL_MEP_INSTANCE_MAX; ++*next_instance) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next_instance, &config)) == VTSS_RC_OK) {
            if (config.enable) {  /* If the MEP is enabled the search is completed */
                return (VTSS_RC_OK);
            }
        }
        *next_index = 0;
    }
    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_dm_ifdv_bin_status_get(const u32                          instance,
                                             const u32                          index,
                                             vtss_appl_mep_dm_ifdv_bin_state_t    *const status)
{
    mesa_rc                   rc;
    vtss_appl_mep_dm_state_t  dm_state;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)                                                  return VTSS_APPL_MEP_RC_INVALID_INSTANCE;
    if (status == NULL)                                                                          return VTSS_RC_OK;
    if (index >= VTSS_APPL_MEP_DM_BINS_IFDV_MAX)                                                 return VTSS_APPL_MEP_RC_IFDV_BIN;
    if ((rc = rc_conv(vtss_appl_mep_dm_tw_status_get(instance, &dm_state))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    status->ifdv_bin = dm_state.ifdv_bin[index];
    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_ifdv_bin_status_iter(const u32  *const instance,
                                              u32        *const next_instance,
                                              const u32  *const index,
                                              u32        *const next_index)
{
    mep_conf_t  config;

    if (instance) { /* Calculate the starting point for the search for next enabled MEP */
        if (index) {
            if (*index >=(VTSS_APPL_MEP_DM_BINS_IFDV_MAX-1)) {
                *next_instance = *instance + 1;
                *next_index    = 0;
            } else {
                *next_instance = *instance;
                *next_index    = *index + 1;
            }
        } else {
            *next_instance = *instance;
            *next_index    = 0;
        }
    } else {
        *next_instance = 0;
        *next_index    = 0;
    }

    for (*next_instance=*next_instance; *next_instance<VTSS_APPL_MEP_INSTANCE_MAX; ++*next_instance) {    /* search for the next enabled MEP instance */
        if ((mep_instance_conf_get(*next_instance, &config)) == VTSS_RC_OK) {
            if (config.enable) {  /* If the MEP is enabled the search is completed */
                return (VTSS_RC_OK);
            }
        }
        *next_index = 0;
    }
    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_mep_dm_fd_bins_get(const u32                          instance,
                                     const u32                          bin_number,
                                     vtss_appl_mep_dm_fd_bin_state_t    *const tw_bin,
                                     vtss_appl_mep_dm_bin_value_t       *const bin_value)
{
    mesa_rc                   rc;
    vtss_mep_mgmt_dm_conf_t   base_conf;
    vtss_appl_mep_dm_state_t  dmr_state;
    vtss_appl_mep_dm_state_t  dm1_state_far_to_near;
    vtss_appl_mep_dm_state_t  dm1_state_near_to_far;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)     return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (bin_value == NULL)                          return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_dm_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    if (bin_number == 0 || bin_number > base_conf.common.num_of_bin_fd)     return(VTSS_APPL_MEP_RC_INVALID_BIN_NUMBER);

    // TODO: This call could be replaced with an optimized function so that we avoid making a data copy of three structs.
    if (vtss_appl_mep_dm_prio_status_get(instance, base_conf.prio, &dmr_state, &dm1_state_far_to_near, &dm1_state_near_to_far) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not enable to get DM state */

    u32 bin_index = bin_number - 1;
    bin_value->two_way = dmr_state.fd_bin[bin_index];
    bin_value->one_way_fn = dm1_state_far_to_near.fd_bin[bin_index];
    bin_value->one_way_nf = dm1_state_near_to_far.fd_bin[bin_index];

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_ifdv_bins_get(const u32                            instance,
                                       const u32                            bin_number,
                                       vtss_appl_mep_dm_ifdv_bin_state_t    *const tw_bin,
                                       vtss_appl_mep_dm_bin_value_t         *const bin_value)
{
    mesa_rc                   rc;
    vtss_mep_mgmt_dm_conf_t   base_conf;
    vtss_appl_mep_dm_state_t  dmr_state;
    vtss_appl_mep_dm_state_t  dm1_state_far_to_near;
    vtss_appl_mep_dm_state_t  dm1_state_near_to_far;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)     return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (bin_value == NULL)                          return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_dm_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    if (bin_number == 0 || bin_number > base_conf.common.num_of_bin_fd)     return(VTSS_APPL_MEP_RC_INVALID_BIN_NUMBER);

    // TODO: This call could be replaced with an optimized function so that we avoid making a data copy of three structs.
    if (vtss_appl_mep_dm_prio_status_get(instance, base_conf.prio, &dmr_state, &dm1_state_far_to_near, &dm1_state_near_to_far) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not enable to get DM state */

    u32 bin_index = bin_number - 1;
    bin_value->two_way = dmr_state.ifdv_bin[bin_index];
    bin_value->one_way_fn = dm1_state_far_to_near.ifdv_bin[bin_index];
    bin_value->one_way_nf = dm1_state_near_to_far.ifdv_bin[bin_index];

    return (VTSS_RC_OK);
}


mesa_rc vtss_appl_mep_dm_fd_bins_iter(const u32    *const instance,
                                      u32          *const next_instance,
                                      const u32    *const bin_number,
                                      u32          *const next_bin_number)
{
    T_N("instance %p  bin %p  *instance %u  *bin %u", instance, bin_number, (instance) ? *instance : 0, (bin_number) ? *bin_number : 0);

    if (instance == NULL) {  /* Get the first enabled instance as requested */
        if (vtss_appl_mep_instance_conf_iter(instance, next_instance) == VTSS_RC_ERROR)
            return(VTSS_RC_ERROR);
    } else
        *next_instance = *instance;

    /* Now we have an enabled instance to start with */

    /* Calculate the starting point for the search for next bin */
    *next_bin_number = bin_number ? *bin_number + 1 : 1;

    while (TRUE) {
        mesa_rc rc;
        vtss_mep_mgmt_dm_conf_t   dm_conf;
        if ((rc = rc_conv(vtss_mep_mgmt_dm_conf_get(*next_instance, &dm_conf))) != VTSS_RC_OK) {
            return rc;
        }
        if (*next_bin_number <= dm_conf.common.num_of_bin_fd) {
            // next bin number is valid - complete search
            break;
        }

        // find next MEP instance
        if (vtss_appl_mep_instance_conf_iter(next_instance, next_instance) == VTSS_RC_ERROR)  /* Get next enabled instance */
            return(VTSS_RC_ERROR);

        // Bin numbers start at 1
        *next_bin_number = 1;
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_ifdv_bins_iter(const u32    *const instance,
                                        u32          *const next_instance,
                                        const u32    *const bin_number,
                                        u32          *const next_bin_number)
{
    T_N("instance %p  bin %p  *instance %u  *bin %u", instance, bin_number, (instance) ? *instance : 0, (bin_number) ? *bin_number : 0);

    if (instance == NULL) {  /* Get the first enabled instance as requested */
        if (vtss_appl_mep_instance_conf_iter(instance, next_instance) == VTSS_RC_ERROR)
            return(VTSS_RC_ERROR);
    } else
        *next_instance = *instance;

    /* Now we have an enabled instance to start with */

    /* Calculate the starting point for the search for next bin */
    *next_bin_number = bin_number ? *bin_number + 1 : 1;

    while (TRUE) {
        mesa_rc rc;
        vtss_mep_mgmt_dm_conf_t   dm_conf;
        if ((rc = rc_conv(vtss_mep_mgmt_dm_conf_get(*next_instance, &dm_conf))) != VTSS_RC_OK) {
            return rc;
        }
        if (*next_bin_number <= dm_conf.common.num_of_bin_ifdv) {
            // next bin number is valid - complete search
            break;
        }

        // find next MEP instance
        if (vtss_appl_mep_instance_conf_iter(next_instance, next_instance) == VTSS_RC_ERROR)  /* Get next enabled instance */
            return(VTSS_RC_ERROR);

        // Bin numbers start at 1
        *next_bin_number = 1;
    }

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_tw_status_get(const u32                   instance,
                                       vtss_appl_mep_dm_state_t    *const status)
{
    mesa_rc                   rc;
    vtss_mep_mgmt_dm_conf_t   base_conf;
    vtss_appl_mep_dm_state_t  dm1_state_far_to_near;
    vtss_appl_mep_dm_state_t  dm1_state_near_to_far;

    T_DG(TRACE_GRP_DM, "Instance %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)     return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (status == NULL)                             return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_dm_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    if (vtss_appl_mep_dm_prio_status_get(instance, base_conf.prio, status, &dm1_state_far_to_near, &dm1_state_near_to_far) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not enable to get DM state */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_ownf_status_get(const u32                   instance,
                                         vtss_appl_mep_dm_state_t    *const status)
{
    mesa_rc                   rc;
    vtss_mep_mgmt_dm_conf_t   base_conf;
    vtss_appl_mep_dm_state_t  dmr_state;
    vtss_appl_mep_dm_state_t  dm1_state_far_to_near;

    T_DG(TRACE_GRP_DM, "Instance %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)     return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (status == NULL)                             return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_dm_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    if (vtss_appl_mep_dm_prio_status_get(instance, base_conf.prio, &dmr_state, &dm1_state_far_to_near, status) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not enable to get DM state */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_owfn_status_get(const u32                   instance,
                                         vtss_appl_mep_dm_state_t    *const status)
{
    mesa_rc                   rc;
    vtss_mep_mgmt_dm_conf_t   base_conf;
    vtss_appl_mep_dm_state_t  dmr_state;
    vtss_appl_mep_dm_state_t  dm1_state_near_to_far;

    T_DG(TRACE_GRP_DM, "Instance %u", instance);

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)     return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (status == NULL)                             return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_mep_mgmt_dm_conf_get(instance, &base_conf))) != VTSS_RC_OK) {
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));
        return rc;
    }

    if (vtss_appl_mep_dm_prio_status_get(instance, base_conf.prio, &dmr_state, status, &dm1_state_near_to_far) != VTSS_RC_OK)
        return (VTSS_RC_ERROR); /* Not enable to get DM state */

    return (VTSS_RC_OK);
}

mesa_rc vtss_appl_mep_dm_prio_status_get(const u32                  instance,
                                         const u32                  prio,
                                         vtss_appl_mep_dm_state_t   *const dmr_state,
                                         vtss_appl_mep_dm_state_t   *const dm1_state_far_to_near,
                                         vtss_appl_mep_dm_state_t   *const dm1_state_near_to_far)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (dmr_state == NULL)                             return VTSS_RC_OK;
    if (dm1_state_far_to_near == NULL)                 return VTSS_RC_OK;
    if (dm1_state_near_to_far == NULL)                 return VTSS_RC_OK;

    return rc_conv(vtss_mep_mgmt_dm_prio_state_get(instance, prio, dmr_state, dm1_state_far_to_near, dm1_state_near_to_far));
}

mesa_rc vtss_appl_mep_dm_status_clear(const u32 instance)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    return rc_conv(vtss_mep_mgmt_dm_state_clear_set(instance));
}

mesa_rc vtss_appl_mep_tst_status_get(const u32                    instance,
                                     vtss_appl_mep_tst_state_t    *const state)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)    return (VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    if (state == NULL)                             return (VTSS_RC_OK);

    return rc_conv(vtss_mep_mgmt_tst_state_get(instance, state));
}

mesa_rc vtss_appl_mep_tst_status_clear(const u32 instance)
{
    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return (VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    return rc_conv(vtss_mep_mgmt_tst_state_clear_set(instance));
}

/****************************************************************************/
/*  MEP module interface - call out                                         */
/****************************************************************************/
BOOL vtss_mep_port_los_get(u32 port)
{
    return(conv_los_state[port]);
}

void vtss_mep_rx_aps_info_set(const u32                      instance,
                              const mep_domain_t             domain,
                              const u8                       *aps)
{
#if defined(VTSS_SW_OPTION_EPS) || defined(VTSS_SW_OPTION_ERPS)
    u32            eps = 0;
    mep_eps_type_t eps_type = MEP_EPS_TYPE_INV;
    mesa_rc        rc = VTSS_RC_OK;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return;

    CRIT_ENTER(crit_p);
    if (instance_data[instance].eps_type != MEP_EPS_TYPE_INV) {
        eps_type = instance_data[instance].eps_type;
    } else {
        rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;
    }
    if (instance_data[instance].aps_inst != APS_INST_INV) {
        eps = instance_data[instance].aps_inst;
    } else  {
        rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;
    }
    CRIT_EXIT(crit_p);

    T_DG(TRACE_GRP_APS_RX, "rx aps  instance %u  %X %X\n",instance, aps[0], aps[1]);

    if (rc == VTSS_RC_OK) {
#if defined(VTSS_SW_OPTION_EPS)
        if (eps_type == MEP_EPS_TYPE_ELPS) {
            if (eps_rx_aps_info_set(eps, instance, aps) != VTSS_RC_OK) {
                T_D("Error during APS rx set %u", instance);
            }
        }
#endif
#if defined(VTSS_SW_OPTION_ERPS)
        if (eps_type == MEP_EPS_TYPE_ERPS) {
            erps_handover_raps_pdu(aps, instance, VTSS_MEP_APS_DATA_LENGTH, eps);
        }
#endif
    }
#endif
}

void vtss_mep_signal_out(const u32             instance,
                         const mep_domain_t    domain)
{
    u32  i;
    BOOL los=FALSE;

    CRIT_ENTER(crit_p);
    for (i=0; i<mep_caps.mesa_port_cnt; ++i)  /* Calculate SSF for this mep instance based on los state */
        if (instance_data[instance].port[i])    /* Check if this port(i) is related to this MEP */
        {
            los = ((domain == VTSS_APPL_MEP_EVC) || (domain == VTSS_APPL_MEP_VLAN)) ? conv_los_state[i] : los_state[i];
            if (!los)   break;      /* Only report los active (SSF) if all related ports are failing */
        }

#if defined(VTSS_SW_OPTION_EPS)
    u32              eps=0, forward, raps_tx;
    u8               aps_txdata[VTSS_MEP_APS_DATA_LENGTH];
    mep_eps_type_t   eps_type=MEP_EPS_TYPE_INV;
    mesa_rc          rc = VTSS_RC_OK;

    memcpy(aps_txdata, instance_data[instance].aps_txdata, sizeof(aps_txdata));
    if (instance_data[instance].eps_type == MEP_EPS_TYPE_ERPS)
    {
        forward = instance_data[instance].raps_forward ? 2 : 1;
        raps_tx = instance_data[instance].raps_tx ? 2 : 1;
    }
    else
    {
        forward = 0;
        raps_tx = 0;
    }

    if (instance_data[instance].eps_type != MEP_EPS_TYPE_INV)    eps_type = instance_data[instance].eps_type;
    else    rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;
    if (instance_data[instance].aps_inst != APS_INST_INV)        eps = instance_data[instance].aps_inst;
    else    rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;
#endif
    CRIT_EXIT(crit_p);

    vtss_mep_new_ssf_state(instance,  los); /* This in order to recalculate SSF status */
    if (mep_caps.mesa_mep_serval) {
        if (vtss_mep_mgmt_protection_change_set(i) != VTSS_RC_OK) { /* This in order to restart any possible AFI */
            T_D("Error during vtss_mep_mgmt_protection_change_set() %u", instance);
        }
    }

#if defined(VTSS_SW_OPTION_EPS)
    if (raps_tx)
    {
        if (vtss_mep_tx_aps_info_set(instance, aps_txdata, FALSE))    T_D("Error during configuration");
        if (vtss_mep_raps_transmission(instance, (raps_tx == 2)))     T_D("Error during configuration");
    }

    if (forward)        if (vtss_mep_raps_forwarding(instance, (forward == 2)))     T_D("Error during configuration");

    if (rc == VTSS_RC_OK) {
        if (eps_type == MEP_EPS_TYPE_ELPS) {
            if (eps_signal_in(eps, instance) != VTSS_RC_OK) {
                T_D("Error during EPS signal %u", instance);
            }
        }

        if (eps_type == MEP_EPS_TYPE_ERPS) {
        }
    }
#endif
}

void vtss_mep_sf_sd_state_set(const u32                      instance,
                              const mep_domain_t             domain,
                              const BOOL                     sf_state,
                              const BOOL                     sd_state)
{
#if defined(VTSS_SW_OPTION_EPS) || defined(VTSS_SW_OPTION_ERPS)
    u32              i;
    u32              eps_rc = VTSS_RC_OK;
    u32              eps_count;
    u16              eps_inst[SF_EPS_MAX];
    mep_eps_type_t   eps_type=MEP_EPS_TYPE_INV;
    mesa_rc          erps_rc = VTSS_RC_OK;

    T_N("instance %u  sf_state %u  sd_state %u",instance,sf_state,sd_state);

    CRIT_ENTER(crit_p);
    eps_type = instance_data[instance].eps_type;
    eps_count = instance_data[instance].ssf_count;
    memcpy(eps_inst, instance_data[instance].ssf_inst, sizeof(eps_inst));
    CRIT_EXIT(crit_p);

#if defined(VTSS_SW_OPTION_EPS)
    if (eps_type == MEP_EPS_TYPE_ELPS)
    {
        for (i=0; i<eps_count; ++i)
            eps_rc += eps_sf_sd_state_set(eps_inst[i], instance, sf_state, sd_state);
    }
#endif
#if defined(VTSS_SW_OPTION_ERPS)
    if (eps_type == MEP_EPS_TYPE_ERPS) {
        for (i=0; i<eps_count; ++i) {
            erps_rc += erps_sf_sd_state_set(eps_inst[i], instance, sf_state, sd_state);
        }
    }
#endif
    if (eps_rc != VTSS_RC_OK || erps_rc != VTSS_RC_OK) {
        T_D("Error during SF/SD set %u", instance);
    }
#endif
}

BOOL known_peer_mep_id(u32 peer,  mep_conf_t *config)
{
    u32 i;

    for (i=0; i<config->peer_count; ++i) {
        if (config->peer_mep[i] == peer)
            break;
    }
    return ((i == config->peer_count) ? FALSE : TRUE);
}


void vtss_mep_status_change(const u32  instance,  const BOOL enable)
{
    mep_conf_t        config;
    vtss_mep_mgmt_state_t       state;
    vtss_appl_mep_peer_state_t  peer_state;
    u32                         next_instance, next_peer, i;
    mesa_rc                     rc = VTSS_RC_OK;

    rc = mep_instance_conf_get(instance, &config);
    if ((rc != VTSS_RC_OK) && (rc != VTSS_APPL_MEP_RC_VOLATILE))
        return; /* Not able to get MEP */

    if (enable) {    /* This instance is enabled - update the status objects */
        if (vtss_mep_mgmt_state_get(instance, &state) != VTSS_RC_OK)   /* Get internal MEP status */
            return;

        if (mep_status_set(instance, &state.mep) != VTSS_RC_OK)     /* Update MEP status object */
            T_D("mep_status_set failed  instance %u",instance);
        for (i=0; i<config.peer_count; ++i) {     /* Update peer MEP status object with all known peer MEP id */
            if (mep_status_peer_set(instance, config.peer_mep[i], &state.peer_mep[i]) != VTSS_RC_OK)
                T_D("mep_status_peer_set failed  instance %u  peer %u",instance,config.peer_mep[i]);
        }

        next_instance = instance;   /* Now we will check if any peer MEP id in the status object is no more a known id */
        next_peer = 0;
        if (mep_status_peer_get(next_instance, next_peer, &peer_state) == VTSS_RC_OK) {     /* Try and get the possible first peer MEP id */
            if (!known_peer_mep_id(next_peer, &config))
                (void)mep_status_peer_del(next_instance, next_peer);
        }
        while ((mep_status_peer_get_next(&next_instance, &next_peer, &peer_state) == VTSS_RC_OK) && (next_instance == instance)) {    /* Get next for this instance */
            if (!known_peer_mep_id(next_peer, &config))
                (void)mep_status_peer_del(next_instance, next_peer);
        }
    } else {        /* This instance is disabled - delete the status objects */
        (void)mep_status_del(instance);        /* Delete the MEP status object */

        next_instance = instance;   /* Delete the peer MEP status object for this MEP instance */
        next_peer = 0;
        if (mep_status_peer_get(next_instance, next_peer, &peer_state) == VTSS_RC_OK) {     /* Try and get the possible first peer MEP id */
            (void)mep_status_peer_del(next_instance, next_peer);
        }
        while ((mep_status_peer_get_next(&next_instance, &next_peer, &peer_state) == VTSS_RC_OK) && (next_instance == instance)) {    /* Get next for this instance */
            (void)mep_status_peer_del(next_instance, next_peer);
        }
    }
}

void vtss_mep_lm_notif_status_get(const u32  instance,
                                  vtss_appl_mep_lm_notif_state_t *state)
{
    (void)mep_status_lm_notif_get(instance, state);
}

void vtss_mep_lm_notif_status_set(const u32  instance,
                                  vtss_appl_mep_lm_notif_state_t *state)
{
    if (state) {
        (void)mep_status_lm_notif_set(instance, state);
    } else {
        (void)mep_status_lm_notif_del(instance);
    }
}

BOOL known_peer_mep_id(u32 peer,  vtss_appl_mep_conf_t *config)
{
    u32 i;

    for (i = 0; i < config->peer_count; ++i) {
        if (config->peer_mep[i] == peer)
            break;
    }
    return ((i == config->peer_count) ? FALSE : TRUE);
}

void vtss_mep_lm_hli_status_get(const u32  instance,
                                const u32  peer_id,
                                vtss_appl_mep_lm_hli_state_t *state)
{
    (void)mep_status_lm_hli_get(instance, peer_id, state);
}

void vtss_mep_lm_hli_status_set(const u32  instance,
                                const u32  peer_id,
                                vtss_appl_mep_lm_hli_state_t *state)
{
    if (state) {
        (void)mep_status_lm_hli_set(instance, peer_id, state);
    } else {
        (void)mep_status_lm_hli_del(instance, peer_id);
    }
}

/****************************************************************************/
/*  MEP module interface - call in                                          */
/****************************************************************************/





















































































































































































































































































































































































































mesa_rc mep_eps_aps_register(const u32            instance,
                             const u32            eps_inst,
                             const mep_eps_type_t eps_type,
                             const BOOL           enable)
{
    BOOL invalid = FALSE;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER(crit_p);
    if (enable && instance_data[instance].aps_inst != APS_INST_INV)
        invalid = TRUE;
    if (!enable && instance_data[instance].aps_inst != eps_inst)
        invalid = TRUE;
    if (enable && (((instance_data[instance].aps_type == VTSS_APPL_MEP_L_APS) && (eps_type == MEP_EPS_TYPE_ERPS)) ||
                   ((instance_data[instance].aps_type == VTSS_APPL_MEP_R_APS) && (eps_type == MEP_EPS_TYPE_ELPS))))
        invalid = TRUE;

    if (!invalid)
    {
        if (enable)
        {/* Add EPS as APS instance */
            instance_data[instance].aps_inst = eps_inst;
            instance_data[instance].eps_type = eps_type;
        }
        else
        {/* Remove EPS as APS instance */
            instance_data[instance].aps_inst = APS_INST_INV;
            if (instance_data[instance].ssf_count == 0)
                instance_data[instance].eps_type = MEP_EPS_TYPE_INV;    /* No EPS Related */
        }
    }
    CRIT_EXIT(crit_p);

    return (invalid ? (mesa_rc)VTSS_APPL_MEP_RC_INVALID_PARAMETER : VTSS_RC_OK);
}


mesa_rc mep_eps_sf_register(const u32            instance,
                            const u32            eps_inst,
                            const mep_eps_type_t eps_type,
                            const BOOL           enable)
{
    u32 i;
    BOOL invalid = FALSE;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER(crit_p);
    if (enable && (((instance_data[instance].aps_type == VTSS_APPL_MEP_L_APS) && (eps_type == MEP_EPS_TYPE_ERPS)) ||
                   ((instance_data[instance].aps_type == VTSS_APPL_MEP_R_APS) && (eps_type == MEP_EPS_TYPE_ELPS))))
        invalid = TRUE;
    if (enable && instance_data[instance].ssf_count >= SF_EPS_MAX)
        invalid = TRUE;
    for (i=0; i<instance_data[instance].ssf_count; ++i)     if (instance_data[instance].ssf_inst[i] == eps_inst)     break;
    if (enable && (i < instance_data[instance].ssf_count))      invalid = TRUE;
    if (!enable && (i == instance_data[instance].ssf_count))    invalid = TRUE;

    if (!invalid)
    {
        if (enable)
        {/* Add EPS as SSF instance */
            instance_data[instance].ssf_inst[i] = eps_inst;
            instance_data[instance].ssf_count++;
            instance_data[instance].eps_type = eps_type;
        }
        else
        {/* Remove EPS as SSF instance */
            for (i=i; i<(instance_data[instance].ssf_count-1); ++i)
                instance_data[instance].ssf_inst[i] = instance_data[instance].ssf_inst[i+1];
            if (instance_data[instance].ssf_count)  instance_data[instance].ssf_count--;

            if ((instance_data[instance].ssf_count == 0) && (instance_data[instance].aps_inst == APS_INST_INV))
                instance_data[instance].eps_type = MEP_EPS_TYPE_INV;    /* No EPS Related */
        }
    }
    CRIT_EXIT(crit_p);

    return (invalid ? (mesa_rc)VTSS_APPL_MEP_RC_INVALID_PARAMETER : VTSS_RC_OK);
}

mesa_rc mep_tx_aps_info_set(const u32  instance,
                            const u32  eps_inst,
                            const u8   *aps,
                            const BOOL event)
{
    mesa_rc rc = VTSS_RC_OK;
    BOOL    created = FALSE;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);
    CRIT_ENTER(crit_p);
    if (instance_data[instance].eps_type == MEP_EPS_TYPE_INV)    rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;
    if ((instance_data[instance].aps_inst == APS_INST_INV) || (instance_data[instance].aps_inst != eps_inst))    rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;
    if (rc == VTSS_RC_OK)   memcpy(instance_data[instance].aps_txdata, aps, (instance_data[instance].eps_type == MEP_EPS_TYPE_ERPS) ? VTSS_MEP_SUPP_RAPS_DATA_LENGTH : VTSS_MEP_SUPP_LAPS_DATA_LENGTH);
    created = instance_data[instance].enable;
    CRIT_EXIT(crit_p);

    T_DG(TRACE_GRP_APS_TX, "tx aps  instance %u  %X %X\n", instance, aps[0], aps[1]);

    if (rc != VTSS_RC_OK) {
        return rc;
    }

    if (created) {
        rc = rc_conv(vtss_mep_tx_aps_info_set(instance, aps, event));
    }
    return rc;
}


mesa_rc mep_signal_in(const u32 instance,
                      const u32 eps_inst)
{
    u32     i;
    mesa_rc rc = VTSS_RC_OK;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER(crit_p);
    if (instance_data[instance].eps_type == MEP_EPS_TYPE_INV)    rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;
    for (i=0; i<instance_data[instance].ssf_count; ++i)
        if (instance_data[instance].ssf_inst[i] == eps_inst)     break;
    if ((i==instance_data[instance].ssf_count) &&
        ((instance_data[instance].aps_inst == APS_INST_INV) || (instance_data[instance].aps_inst != eps_inst)))    rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;
    CRIT_EXIT(crit_p);

    if (rc != VTSS_RC_OK) {
        return rc;
    }

    return rc_conv(vtss_mep_signal_in(instance));
}


mesa_rc mep_raps_forwarding(const u32  instance,
                            const u32  eps_inst,
                            const BOOL enable)
{
    mesa_rc rc = VTSS_RC_OK;
    BOOL    created = FALSE;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER(crit_p);
    if (instance_data[instance].eps_type != MEP_EPS_TYPE_ERPS)    rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;
    if ((instance_data[instance].aps_inst == APS_INST_INV) || (instance_data[instance].aps_inst != eps_inst))    rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;
    if (rc == VTSS_RC_OK)   instance_data[instance].raps_forward = enable;
    created = instance_data[instance].enable;
    CRIT_EXIT(crit_p);

//T_D("forward enable  instance %lu  enable %u\n", instance, enable);
    if (rc != VTSS_RC_OK) {
        return rc;
    }

    if (created) {
        rc = rc_conv(vtss_mep_raps_forwarding(instance, enable));
    }
    return rc;
}


mesa_rc mep_raps_transmission(const u32  instance,
                              const u32  eps_inst,
                              const BOOL enable)
{
    mesa_rc rc = VTSS_RC_OK;
    BOOL    created = FALSE;

    if (instance >= VTSS_APPL_MEP_INSTANCE_MAX)        return(VTSS_APPL_MEP_RC_INVALID_INSTANCE);

    CRIT_ENTER(crit_p);
    if (instance_data[instance].eps_type != MEP_EPS_TYPE_ERPS)    rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;
    if ((instance_data[instance].aps_inst == APS_INST_INV) || (instance_data[instance].aps_inst != eps_inst))    rc = VTSS_APPL_MEP_RC_INVALID_PARAMETER;
    if (rc == VTSS_RC_OK)   instance_data[instance].raps_tx = enable;
    created = instance_data[instance].enable;
    CRIT_EXIT(crit_p);

//T_D("tx enable  instance %lu  enable %u\n", instance, enable);
    if (rc != VTSS_RC_OK) {
        return rc;
    }

    if (created) {
        rc = rc_conv(vtss_mep_raps_transmission(instance, enable));
    }
    return rc;
}

/*lint -e{454, 455, 456} ... The mutex is locked so it is ok to unlock */
void mep_port_protection_changed_evc_mep(u32 port, BOOL sw)
{
    u32 i, rc, tlv_change=FALSE;

    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i) {
        if (!instance_data[i].enable)   continue;
        if (((instance_data[i].domain == VTSS_APPL_MEP_EVC) || (instance_data[i].domain == VTSS_APPL_MEP_VLAN)) && (instance_data[i].port[port])) { /* Check if an EVC or VLAN on this port  */
            CRIT_EXIT(crit_p);
            if (sw)
                rc = vtss_mep_mgmt_protection_change_set(i);    /* This MEP needs to reconfigure depending on port protection */
            else
                rc = vtss_mep_mgmt_change_set(i, FALSE, FALSE);   /* This MEP needs to reconfigure depending on port protection */
            CRIT_ENTER(crit_p);
            if (rc != VTSS_MEP_RC_OK)   continue;
        }

        /* Find if any MEP instance need to change PS TLV based on RAPS change */
        if ((instance_data[i].res_port == port) && (instance_data[i].port[port]))  /* Check if this is Down-MEP that is injecting into this ELPS port */
            tlv_change = TRUE;
    }

    if (tlv_change) {
        CRIT_EXIT(crit_p);
        (void)vtss_mep_mgmt_ps_tlv_change_set();
        CRIT_ENTER(crit_p);
    }
}

static u32 working_port_get(const u32 port)    /* Port can be either working or protecting - or a port not part of port protection */
{
    u32 i;

    if (in_port_conv[port] == port)   /* This port is either a working port or a port not part of port protection */
        return(port);
    if (out_port_conv[port] != port)  /* This port is a protected port (working) */
        return(port);
    if (in_port_conv[port] != 0xFFFF) /* This port is a active protecting port */
        return(in_port_conv[port]);
    for (i=0; i<mep_caps.mesa_port_cnt; ++i)  /* This is a NOT active protecting port - find working */
        if ((i != port) && (out_port_conv[i] == port))      break;
    if (i < mep_caps.mesa_port_cnt)
        return(i);                    /* The protected port was found */
    return(port);   /* This should not happen */
}

static u32 port_protection_mask_calc(u32 port)
{
    u32 mask, w_port, p_port;

    w_port = working_port_get(port);
    p_port = out_port_conv[w_port];
    mask = 0;

    if (p_port != w_port) {  /* This is a protected port */
        if (out_port_1p1[w_port])  /* This is 1+1 - mask is both working and protecting port */
            mask = (1<<w_port) | (1<<p_port);
        else { /* This is 1:1 - mask is only active port */
            if (in_port_conv[w_port] == w_port) /* Working is active */
                mask = (1<<w_port);
            else if (in_port_conv[p_port] == w_port) /* protecting is active */
                mask = (1<<p_port);
        }
    }
    else    /* This is NOT a protected port */
        mask = 1<<w_port;

    return(mask);
}

void mep_port_protection_change(const u32         w_port,
                                const u32         p_port,
                                const BOOL        active)
{
    BOOL change = FALSE;

    CRIT_ENTER(crit_p);
    /* Change in port protection selector state */
    if ((active && (in_port_conv[w_port] == w_port)) || (!active && (in_port_conv[w_port] == 0xFFFF)))  /* Protection state changed */
        change = TRUE;

    if (!active)
    {/* Port protection is no more active */
        out_port_tx[w_port] = TRUE;
        out_port_tx[p_port] = (out_port_1p1[w_port]) ? TRUE : FALSE;

        in_port_conv[w_port] = w_port;
        if (in_port_conv[p_port] == w_port)     in_port_conv[p_port] = 0xFFFF;        /* p_port is protecting this w_port - Discard on p_port port */
        prot_state_change(w_port);
    }
    else
    {/* Port protection is active */
        out_port_tx[w_port] = (out_port_1p1[w_port]) ? TRUE : FALSE;
        out_port_tx[p_port] = TRUE;

        if (in_port_conv[p_port] == 0xFFFF)
        {/* p_port is not already protecting other w_port */
            in_port_conv[w_port] = 0xFFFF;        /* Discard on w_port port */
            in_port_conv[p_port] = w_port;
            prot_state_change(p_port);
        }
    }

    if (change)     /* Signal any change to the MEP base part - it might be necessary to do configuration change */
        mep_port_protection_changed_evc_mep(w_port, TRUE);
//printf("mep_port_protection_change  in_port_conv %lX-%lX-%lX-%lX-%lX-%lX-%lX-%lX-%lX-%lX-%lX-%lX\n", in_port_conv[0], in_port_conv[1], in_port_conv[2], in_port_conv[3], in_port_conv[4], in_port_conv[5], in_port_conv[6], in_port_conv[7], in_port_conv[8], in_port_conv[9], in_port_conv[10], in_port_conv[11]);
    CRIT_EXIT(crit_p);
}

void mep_port_protection_create(mep_eps_architecture_t architecture,  const u32 w_port,  const u32 p_port)
{
    CRIT_ENTER(crit_p);
    in_port_conv[w_port] = w_port;
    if (in_port_conv[p_port] == p_port)            in_port_conv[p_port] = 0xFFFF;       /* Not already protecting some w_port - discard on protecting port */

    out_port_1p1[w_port] = (architecture == MEP_EPS_ARCHITECTURE_1P1) ? TRUE : FALSE;   /* This is a 1+1 port protection */

    out_port_tx[w_port] = TRUE;
    out_port_tx[p_port] = (out_port_1p1[w_port]) ? TRUE : FALSE;

    if (out_port_conv[w_port] != p_port) { /* Protection changed */
        out_port_conv[w_port] = p_port;
        mep_port_protection_changed_evc_mep(w_port, FALSE);
    }
//printf("mep_port_protection_create  in_port_conv %lX-%lX-%lX-%lX-%lX-%lX-%lX-%lX-%lX-%lX-%lX-%lX\n", in_port_conv[0], in_port_conv[1], in_port_conv[2], in_port_conv[3], in_port_conv[4], in_port_conv[5], in_port_conv[6], in_port_conv[7], in_port_conv[8], in_port_conv[9], in_port_conv[10], in_port_conv[11]);
    CRIT_EXIT(crit_p);
}

void mep_port_protection_delete(const u32 w_port,  const u32 p_port)
{
    u32 i;

    CRIT_ENTER(crit_p);
    /* Check for other that got the same p_port */
    for (i=0; i<mep_caps.mesa_port_cnt; ++i)
        if ((i != w_port) && (out_port_conv[i] == p_port))      break;
    if (i == mep_caps.mesa_port_cnt)
    {/* No Other ESP got this p_port as protection port */
        in_port_conv[p_port] = p_port;
        conv_los_state[p_port] = los_state[p_port];
    }

    prot_state_change(w_port);      /* Working is now active - New SF state to MEP */

    in_port_conv[w_port] = w_port;
    out_port_conv[w_port] = w_port;
    conv_los_state[w_port] = los_state[w_port];

    mep_port_protection_changed_evc_mep(w_port, FALSE);
    CRIT_EXIT(crit_p);
}

void mep_ring_protection_block(const u32 port,  BOOL block)
{
    T_D("mep_ring_protection_block port %u  block %u\n", port, block);

    u32 i, rc, tlv_change;

    CRIT_ENTER(crit_p);

    tlv_change = FALSE;
    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i) {
        if (!instance_data[i].enable)   continue;
        /* Find if any MEP instance need to be re-configured or change AFI injection based on RAPS change */
        if ((instance_data[i].res_port != port) && (instance_data[i].port[port])) { /* Check if this is an Up-MEP that is injecting into this ring port */
            if ((mep_caps.mesa_mep_serval && instance_data[i].domain == VTSS_APPL_MEP_PORT) || !mep_caps.mesa_mep_serval) {
                CRIT_EXIT(crit_p);
                rc = vtss_mep_mgmt_protection_change_set(i);    /* This MEP needs to reconfigure depending on ring protection */
                CRIT_ENTER(crit_p);
                if (rc != VTSS_MEP_RC_OK)   continue;
            }
        }
        /* Find if any MEP instance need to change PS TLV based on RAPS change */
        if ((instance_data[i].res_port == port) && (instance_data[i].port[port]))  /* Check if this is Down-MEP that is injecting into this ring port */
            tlv_change = TRUE;
    }

    if (tlv_change) {
        CRIT_EXIT(crit_p);
        (void)vtss_mep_mgmt_ps_tlv_change_set();
        CRIT_ENTER(crit_p);
    }

    CRIT_EXIT(crit_p);
}










/****************************************************************************/
/*  MEP platform interface                                                  */
/****************************************************************************/
void vtss_mep_crit_lock(void)
{
    CRIT_ENTER(crit);
}

void vtss_mep_crit_unlock(void)
{
    CRIT_EXIT(crit);
}

void vtss_mep_run(void)
{
    vtss_flag_setbits(&run_wait_flag, FLAG_RUN);
}

void vtss_mep_timer_start(void)
{
    vtss_flag_setbits(&timer_wait_flag, FLAG_TIMER);
}

void vtss_mep_trace(const char  *string,
                    const u32   param1,
                    const u32   param2,
                    const u32   param3,
                    const u32   param4)
{
    T_D("%s - %d, %d, %d, %d", string, param1, param2, param3, param4);
}

void vtss_mep_lm_trace(const char  *string,
                       const u32   param1,
                       const u32   param2,
                       const u32   param3,
                       const u32   param4)
{
    T_DG(TRACE_GRP_LM, "%s - %u, %u, %u, %u", string, param1, param2, param3, param4);
}

void vtss_mep_dm_trace(const char  *string,
                       const u32   param1,
                       const u32   param2,
                       const u32   param3,
                       const u32   param4)
{
    T_DG(TRACE_GRP_DM, "%s - %u, %u, %u, %u", string, param1, param2, param3, param4);
}

void vtss_mep_discard_trace(const char  *string,
                            const u32   param1,
                            const u32   param2,
                            const u32   param3,
                            const u32   param4)
{
    T_DG(TRACE_GRP_DISCARD, "%s - %u, %u, %u, %u", string, param1, param2, param3, param4);
}

void vtss_mep_syslog(vtss_mep_syslog_t type, const u32 param1, const u32 param2)
{
    switch(type) {
        case VTSS_MEP_ENTER_AIS:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_I("MEP-ENTER_AIS: MEP %d, has entered an AIS defect condition", param1);
#endif /* VTSS_SW_OPTION_SYSLOG */
            break;
        case VTSS_MEP_EXIT_AIS:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_I("MEP-EXIT_AIS: MEP %d, has exited an AIS defect condition", param1);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        case VTSS_MEP_CROSSCHECK_SERVICE_UP:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_I("MEP-CROSSCHECK_SERVICE_UP: MEP %d, CSI or MA is up, as it receives CC messages from all remote, statically configured MEPs", param1);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        case VTSS_MEP_CROSSCHECK_MEP_MISSING:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_W("MEP-CROSSCHECK_MEP_MISSING: MEP %d, peer MEP %d does not come up during the crosscheck start timeout interval", param1, param2);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        case VTSS_MEP_CROSS_CONNECTED_SERVICE:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_I("MEP-CROSS_CONNECTED_SERVICE: MEP %d, the CC message contains a CSI ID or MA ID different from what is configured locally", param1);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        case VTSS_MEP_CONFIG_ERROR:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_W("MEP-CONF_ERR: MEP %d, has received a CC message with its own MPID but a different source MAC address", param1);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        case VTSS_MEP_FORWARDING_LOOP:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_W("MEP-FORWARDING_LOOP: MEP %d, has received a CC messages with its own MPID and source MAC address", param1);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        case VTSS_MEP_CROSSCHECK_MEP_UNKNOWN:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_W("MEP-CROSSCHECK_MEP_UNKNOWN: MEP %d, the remote MEP that is received is not in the configured static list", param1);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        case VTSS_MEP_REMOTE_MEP_DOWN:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_I("MEP-REMOTE_MEP_DOWN: MEP %d, from peer MEP %d a CC message is received with a zero hold time or CC times out", param1, param2);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        case VTSS_MEP_REMOTE_MEP_UP:
#if defined(VTSS_SW_OPTION_SYSLOG)
            S_I("MEP-REMOTE_MEP_UP: MEP %d, from peer MEP %d CC message is received", param1, param2);
            break;
#endif /* VTSS_SW_OPTION_SYSLOG */
        default:
            T_D("unknown type!!");
            break;
    }
}

void vtss_mep_mac_get(u32 port,  u8 mac[VTSS_APPL_MEP_MAC_LENGTH])
{
    if (conf_mgmt_mac_addr_get(mac, port+1) < 0)
        T_W("Error getting own MAC:\n");
}

BOOL vtss_mep_pvid_get(u32 port, u32 *pvid, vtss_mep_tag_type_t *ptype)
{
    vtss_appl_vlan_port_conf_t vlan_conf;

    if (port >= mep_caps.mesa_port_cnt) {
        // You cannot call vlan_mgmt_port_conf_get() with port >= mep_caps.mesa_port_cnt.
        T_E("Invalid port number (%u)", port);
        return FALSE;
    }

   if (vlan_mgmt_port_conf_get(VTSS_ISID_START, port, &vlan_conf, VTSS_APPL_VLAN_USER_ALL, TRUE) != VTSS_RC_OK) {
       return FALSE;
   }

   switch (vlan_conf.hybrid.port_type) {
        case VTSS_APPL_VLAN_PORT_TYPE_C:        *ptype = VTSS_MEP_TAG_C; break;
        case VTSS_APPL_VLAN_PORT_TYPE_S:        *ptype = VTSS_MEP_TAG_S; break;
        case VTSS_APPL_VLAN_PORT_TYPE_S_CUSTOM: *ptype = VTSS_MEP_TAG_S_CUSTOM; break;
        default:                                *ptype = VTSS_MEP_TAG_NONE;
    }
    *pvid = vlan_conf.hybrid.pvid;

    return(TRUE);
}


BOOL vtss_mep_vlan_get(const u32 vid, mesa_port_list_t &nni)
{
    vtss_appl_vlan_entry_t conf;
    mesa_port_no_t         port_no;

    if (vtss_appl_vlan_get(VTSS_ISID_START, vid, &conf, FALSE, VTSS_APPL_VLAN_USER_STATIC) != VTSS_RC_OK) {
        return FALSE;
    }

    for (port_no = VTSS_PORT_NO_START; port_no < mep_caps.mesa_port_cnt; port_no++) {
        nni[port_no] = (conf.ports[port_no] == 1) ? TRUE : FALSE;
    }

    return TRUE;
}

BOOL vtss_mep_port_counters_get(u32 instance,  u32 port,  mesa_port_counters_t *counters)
{
    if (((instance_data[instance].domain == VTSS_APPL_MEP_EVC) || (instance_data[instance].domain == VTSS_APPL_MEP_VLAN)) && (in_port_conv[port] == 0xFFFF))
        return (mesa_port_counters_get(NULL, out_port_conv[port], counters) != VTSS_RC_OK);
    return (mesa_port_counters_get(NULL, port, counters) != VTSS_RC_OK);
}

u32 vtss_mep_port_count(void)
{
    return(port_isid_port_count(VTSS_ISID_LOCAL));
}




























































































































































































void vtss_mep_port_register(const u32              instance,
                            const mesa_port_list_t &port)
{
    CRIT_ENTER(crit_p);
    instance_data[instance].port = port;
    CRIT_EXIT(crit_p);
}

mesa_packet_rx_queue_t vtss_mep_oam_cpu_queue_get(void)
{
    return PACKET_XTR_QU_OAM;
}

BOOL vtss_mep_vlan_member(u32 vid,  mesa_port_list_t &ports,  BOOL enable)
{
    mesa_rc                rc;
    vtss_appl_vlan_entry_t vlan_entry;
    mesa_port_no_t         port_no;

    rc = vtss_appl_vlan_get(VTSS_ISID_START, vid, &vlan_entry, FALSE, VTSS_APPL_VLAN_USER_MEP);
    if (rc != VTSS_RC_OK && rc != VLAN_ERROR_ENTRY_NOT_FOUND) {
        return FALSE;
    }

    vlan_entry.vid = vid;
    for (port_no = VTSS_PORT_NO_START; port_no < mep_caps.mesa_port_cnt; port_no++) {
        vlan_entry.ports[port_no] = (ports[port_no] ? 1 : 0);
    }
    if (enable)     {if (vlan_mgmt_vlan_add(VTSS_ISID_START,  &vlan_entry,  VTSS_APPL_VLAN_USER_MEP) != VTSS_RC_OK)     return(FALSE);}
    else            {if (vlan_mgmt_vlan_del(VTSS_ISID_GLOBAL, vid, VTSS_APPL_VLAN_USER_MEP) != VTSS_RC_OK)    return(FALSE);}

    return(TRUE);
}

BOOL vtss_mep_port_prio_set(u32 port,  u32 prio,  BOOL enable)
{
    return (qos_port_volatile_default_prio_set(VTSS_ISID_START, port, (enable) ? prio : VTSS_PRIO_NO_NONE) == VTSS_RC_OK);
}

#if defined(VTSS_SW_OPTION_ACL)
void vtss_mep_acl_etree_elan_del(u32    instance)
{
    u32 i;

    T_D("Enter  Instance %u", instance);

    for (i=0; i<mep_caps.mesa_port_cnt; ++i) {
        if (instance_data[instance].etree_elan_uc_ace_id[i] != ACL_MGMT_ACE_ID_NONE)
        {
            (void)acl_mgmt_ace_del(ACL_USER_MEP, instance_data[instance].etree_elan_uc_ace_id[i]);
            instance_data[instance].etree_elan_uc_ace_id[i] = ACL_MGMT_ACE_ID_NONE;
        }
        if (instance_data[instance].etree_elan_mc_ace_id[i] != ACL_MGMT_ACE_ID_NONE)
        {
            (void)acl_mgmt_ace_del(ACL_USER_MEP, instance_data[instance].etree_elan_mc_ace_id[i]);
            instance_data[instance].etree_elan_mc_ace_id[i] = ACL_MGMT_ACE_ID_NONE;
        }
        if (instance_data[instance].etree_elan_other_uc_ace_id[i] != ACL_MGMT_ACE_ID_NONE)
        {
            (void)acl_mgmt_ace_del(ACL_USER_MEP, instance_data[instance].etree_elan_other_uc_ace_id[i]);
            instance_data[instance].etree_elan_other_uc_ace_id[i] = ACL_MGMT_ACE_ID_NONE;
        }
        if (instance_data[instance].tst_ace_id[i] != ACL_MGMT_ACE_ID_NONE)
        {
            (void)acl_mgmt_ace_del(ACL_USER_MEP, instance_data[instance].tst_ace_id[i]);
            instance_data[instance].tst_ace_id[i] = ACL_MGMT_ACE_ID_NONE;
        }
    }
}

void vtss_mep_acl_del_instance(u32    instance)
{
    T_D("Enter  Instance %u", instance);

    if (instance_data[instance].mip_lbm_ace_id != ACL_MGMT_ACE_ID_NONE)
    {
        (void)acl_mgmt_ace_del(ACL_USER_MEP, instance_data[instance].mip_lbm_ace_id);
        instance_data[instance].mip_lbm_ace_id = ACL_MGMT_ACE_ID_NONE;
    }
    if (instance_data[instance].ccm_loc_ace_id[0] != ACL_MGMT_ACE_ID_NONE)
    {
        (void)acl_mgmt_ace_del(ACL_USER_MEP, instance_data[instance].ccm_loc_ace_id[0]);
        instance_data[instance].ccm_loc_ace_id[0] = ACL_MGMT_ACE_ID_NONE;
    }
    if (instance_data[instance].ccm_any_ace_id[0] != ACL_MGMT_ACE_ID_NONE)
    {
        (void)acl_mgmt_ace_del(ACL_USER_MEP, instance_data[instance].ccm_any_ace_id[0]);
        instance_data[instance].ccm_any_ace_id[0] = ACL_MGMT_ACE_ID_NONE;
    }
    if (instance_data[instance].llm_ace_id != ACL_MGMT_ACE_ID_NONE) {
        (void)acl_mgmt_ace_del(ACL_USER_MEP, instance_data[instance].llm_ace_id);
        instance_data[instance].llm_ace_id = ACL_MGMT_ACE_ID_NONE;
    }
    for (u32 i=0; i<mep_caps.mesa_port_cnt; ++i) {
        if (instance_data[instance].tst_ace_id[i] != ACL_MGMT_ACE_ID_NONE)
        {
            (void)acl_mgmt_ace_del(ACL_USER_MEP, instance_data[instance].tst_ace_id[i]);
            instance_data[instance].tst_ace_id[i] = ACL_MGMT_ACE_ID_NONE;
        }
    }
}

void vtss_mep_acl_del_vid(u32    vid)
{
    acl_entry_conf_t     conf;
    mesa_ace_id_t        id;
    mesa_ace_counter_t   counter;
    mesa_rc              rc;
    u32                  i;

//T_D("vtss_mep_acl_del  vid %lu\n", vid);

    id = ACL_MGMT_ACE_ID_NONE;
    do
    {
        if ((rc = acl_mgmt_ace_get(ACL_USER_MEP,  VTSS_ISID_LOCAL,  id,  &conf,  &counter,  TRUE)) == VTSS_RC_OK)
        {
            if (conf.vid.value == vid)
            {
                rc = acl_mgmt_ace_del(ACL_USER_MEP, conf.id);   /* Entry on this VID so delete - next get is on same 'id' as last time */
                for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
                { /* Check if this entry is a HW based CCM/TST frame rule */
                    if (conf.id == instance_data[i].ccm_loc_ace_id[0])  instance_data[i].ccm_loc_ace_id[0] = ACL_MGMT_ACE_ID_NONE;
                    if (conf.id == instance_data[i].ccm_loc_ace_id[1])  instance_data[i].ccm_loc_ace_id[1] = ACL_MGMT_ACE_ID_NONE;
                    if (conf.id == instance_data[i].ccm_any_ace_id[0])  instance_data[i].ccm_any_ace_id[0] = ACL_MGMT_ACE_ID_NONE;
                    if (conf.id == instance_data[i].ccm_any_ace_id[1])  instance_data[i].ccm_any_ace_id[1] = ACL_MGMT_ACE_ID_NONE;
                    if (conf.id == instance_data[i].tst_ace_id[0])  instance_data[i].tst_ace_id[0] = ACL_MGMT_ACE_ID_NONE;
                    if (conf.id == instance_data[i].tst_ace_id[1])  instance_data[i].tst_ace_id[1] = ACL_MGMT_ACE_ID_NONE;
                }
            }
            else   id = conf.id;    /* Entry NOT on this VID - next get on this new 'id' */
        }
    }
    while (rc == VTSS_RC_OK);
}

BOOL vtss_mep_acl_add(vtss_mep_acl_entry    *acl)
{
    acl_entry_conf_t     conf;




//    T_D("vtss_mep_acl_add\n");
//    T_D("vid = %lu   oam = %lX\n", acl->vid, acl->oam);
//    T_D("level = %lX   mask = %lX\n", acl->level, acl->mask);
//    T_D("ing_port = %u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u\n", acl->ing_port[0],acl->ing_port[1],acl->ing_port[2],acl->ing_port[3],acl->ing_port[4],acl->ing_port[5],acl->ing_port[6],acl->ing_port[7],acl->ing_port[8],acl->ing_port[9],acl->ing_port[10],acl->ing_port[11],acl->ing_port[12],acl->ing_port[13],acl->ing_port[14],acl->ing_port[15],acl->ing_port[16],acl->ing_port[17],acl->ing_port[18],acl->ing_port[19],acl->ing_port[20],acl->ing_port[21],acl->ing_port[22],acl->ing_port[23]);
//    T_D("eg_port = %u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u-%u\n", acl->eg_port[0],acl->eg_port[1],acl->eg_port[2],acl->eg_port[3],acl->eg_port[4],acl->eg_port[5],acl->eg_port[6],acl->eg_port[7],acl->eg_port[8],acl->eg_port[9],acl->eg_port[10],acl->eg_port[11],acl->eg_port[12],acl->eg_port[13],acl->eg_port[14],acl->eg_port[15],acl->eg_port[16],acl->eg_port[17],acl->eg_port[18],acl->eg_port[19],acl->eg_port[20],acl->eg_port[21],acl->eg_port[22],acl->eg_port[23]);
//    T_D("ing_port = %u-%u-%u-%u-%u-%u-%u-%u-%u-%u\n", acl->ing_port[0],acl->ing_port[1],acl->ing_port[2],acl->ing_port[3],acl->ing_port[4],acl->ing_port[5],acl->ing_port[6],acl->ing_port[7],acl->ing_port[8],acl->ing_port[9]);
//    T_D("eg_port = %u-%u-%u-%u-%u-%u-%u-%u-%u-%u\n", acl->eg_port[0],acl->eg_port[1],acl->eg_port[2],acl->eg_port[3],acl->eg_port[4],acl->eg_port[5],acl->eg_port[6],acl->eg_port[7],acl->eg_port[8],acl->eg_port[9]);
//    T_D("cpu = %u  hw_ccm %u\n", acl->cpu, acl->hw_ccm);
//    T_D("\n");

    if (acl_mgmt_ace_init(MESA_ACE_TYPE_ETYPE, &conf) != VTSS_RC_OK) {
        return FALSE;
    }

    conf.action.cpu_queue = PACKET_XTR_QU_OAM;

    conf.isid = VTSS_ISID_LOCAL;
    conf.vid.value = acl->vid;                          /* Look for this VID */
    conf.vid.mask = 0xFFFF;
    conf.frame.etype.etype.mask[0] = 0xFF;              /* Look for OAM PDU */
    conf.frame.etype.etype.mask[1] = 0xFF;
    conf.frame.etype.etype.value[0] = 0x89;
    conf.frame.etype.etype.value[1] = 0x02;
    conf.frame.etype.data.mask[0] |= acl->mask<<5;      /* Look for this level(s) */
    conf.frame.etype.data.value[0] |= acl->level<<5;
/*
    if (acl->hw_ccm)
    {
        memcpy(conf.frame.etype.smac.value, acl->mac, sizeof(conf.frame.etype.smac.value));
        memset(conf.frame.etype.smac.mask, 0xFF, sizeof(conf.frame.etype.smac.mask));
    }
*/
    if (acl->oam != VTSS_OAM_ANY_TYPE)                  /* Look for a specific OAM PDU type */
    {
        conf.frame.etype.data.mask[1] = 0xFF;
        conf.frame.etype.data.value[1] = acl->oam;
    }















    memcpy(conf.port_list, acl->ing_port, sizeof(conf.port_list));
    conf.action.force_cpu = acl->cpu;
    conf.action.port_action = MESA_ACL_PORT_ACTION_FILTER;
    memcpy(conf.action.port_list, acl->eg_port, sizeof(conf.action.port_list));

    if (mep_caps.mesa_mep_luton26) {
        conf.action.ptp_action = (acl->ts) ? MESA_ACL_PTP_ACTION_TWO_STEP : MESA_ACL_PTP_ACTION_NONE;
    }
    return(mep_acl_ace_add(ACL_USER_MEP,  ACL_MGMT_ACE_ID_NONE,  &conf) == VTSS_RC_OK);
}


BOOL vtss_mep_acl_raps_add(u32 vid,  mesa_port_list_t &ports,  u8 *mac,  u32 *id)
{
    mesa_rc             rc;
    mesa_ace_id_t       next_id;
    acl_entry_conf_t    conf;
    mesa_ace_counter_t  counter;

    T_D("Enter  vid %u", vid);

    if (acl_mgmt_ace_get(ACL_USER_MEP,  VTSS_ISID_LOCAL,  ACL_MGMT_ACE_ID_NONE,  &conf,  &counter,  TRUE) != VTSS_RC_OK)    return FALSE;

    next_id = conf.id;
    if (acl_mgmt_ace_init(MESA_ACE_TYPE_ETYPE, &conf) != VTSS_RC_OK) {
        return FALSE;
    }

    conf.action.cpu_queue = PACKET_XTR_QU_OAM;

    conf.action.port_action = MESA_ACL_PORT_ACTION_FILTER;
    memset(conf.action.port_list, 0, sizeof(conf.action.port_list));
    conf.isid = VTSS_ISID_LOCAL;
    conf.vid.value = vid;                          /* Look for this VID */
    conf.vid.mask = 0xFFFF;
    memcpy(conf.frame.etype.dmac.value, mac, sizeof(conf.frame.etype.dmac.value));
    memset(conf.frame.etype.dmac.mask, 0xFF, sizeof(conf.frame.etype.dmac.mask));
    conf.action.force_cpu = TRUE;
    conf.port_list = ports;
    conf.action.port_action = MESA_ACL_PORT_ACTION_FILTER;
    conf.action.port_list = ports;

    if ((rc = mep_acl_ace_add(ACL_USER_MEP,  next_id,  &conf)) != VTSS_RC_OK) {
        T_W("mep_acl_ace_add() failed for this VID %u  id %u  rc %s", vid, conf.id, error_txt(rc));
        return FALSE;
    }

    *id = conf.id;
    T_D("Exit  vid %u", vid);
    return TRUE;
}

void vtss_mep_acl_raps_del(u32 id)
{
    if (acl_mgmt_ace_del(ACL_USER_MEP, id) != VTSS_RC_OK)      T_W("acl_mgmt_ace_del() failed for this ID  %u", id);
}

void vtss_mep_acl_ccm_mac_change(u32 instance,  u8 *mac)
{
    mesa_rc              rc;
    u32                  i;
    acl_entry_conf_t     conf, nxt_conf;
    mesa_ace_counter_t   counter;

    for (i=0; i<2; ++i) {
        if (instance_data[instance].ccm_loc_ace_id[i] != ACL_MGMT_ACE_ID_NONE) {
            /* Get conf for id */
            if (acl_mgmt_ace_get(ACL_USER_MEP,  VTSS_ISID_LOCAL,  instance_data[instance].ccm_loc_ace_id[i],  &conf,  &counter,  FALSE) != VTSS_RC_OK) {
                T_W("acl_mgmt_ace_get() failed for this id  %u", instance_data[instance].ccm_loc_ace_id[i]);
                return;
            }

            memcpy(conf.frame.etype.smac.value, mac, sizeof(conf.frame.etype.smac.value));     /* Change the SMAC */

            /* Get next config for this id */
            rc = acl_mgmt_ace_get(ACL_USER_MEP,  VTSS_ISID_LOCAL,  instance_data[instance].ccm_loc_ace_id[i],  &nxt_conf,  &counter,  TRUE);
            if (rc == VTSS_APPL_ACL_ERROR_ACE_NOT_FOUND) {
                nxt_conf.id = ACL_MGMT_ACE_ID_NONE;
            } else {
                if (rc != VTSS_RC_OK) {
                    T_W("acl_mgmt_ace_get() get next failed for this id  %u", instance_data[instance].ccm_loc_ace_id[i]);
                    return;
                }
            }

            /* Add config for this id in front of next id */
            if (mep_acl_ace_add(ACL_USER_MEP,  nxt_conf.id,  &conf) != VTSS_RC_OK) {
                T_W("mep_acl_ace_add() failed for this id  %u", nxt_conf.id);
            }
        }
    }
}

BOOL vtss_mep_acl_ccm_add(u32 instance,  u32 vid,  mesa_port_list_t &port,  u32 level,  u8 *smac,  u32 pag,  BOOL hw_ccm)
{
    u32 i;
    mesa_ace_id_t       next_id;
    acl_entry_conf_t    conf = {};
    mesa_ace_counter_t  counter;
    mesa_rc             rc;

    T_D("Enter instance %u  vid %u  hw_ccm %u", instance, vid, hw_ccm);

    rc = acl_mgmt_ace_get(ACL_USER_MEP,  VTSS_ISID_LOCAL,  ACL_MGMT_ACE_ID_NONE,  &conf,  &counter,  TRUE);  /* Get first entry for this user - MEP */

    if (rc == VTSS_RC_OK)
        next_id = conf.id;
    else {
        if (rc == VTSS_APPL_ACL_ERROR_ACE_NOT_FOUND)
            next_id = ACL_MGMT_ACE_ID_NONE;
        else {
            T_W("acl_mgmt_ace_get() failed for this instance %u  First entry  rc %s", instance, error_txt(rc));
            return FALSE;
        }
    }
    if ((rc = acl_mgmt_ace_init(MESA_ACE_TYPE_ETYPE, &conf)) != VTSS_RC_OK) {
        T_W("acl_mgmt_ace_init() failed for this instance %u  rc %s", instance, error_txt(rc));
        return FALSE;
    }

    conf.action.cpu_queue = PACKET_XTR_QU_OAM;
    conf.isid = VTSS_ISID_LOCAL;
    conf.port_list = port;

    // TODO: Memset acl_entry_conf_t conf and the set the Serval values?
    if (mep_caps.mesa_mep_serval) {
        conf.vid.value = vid;                          /* Look for this VID/ISDX */
        conf.vid.mask = 0xFFFF;
        conf.policy.value = pag;                       /* Look for this PAG */
        conf.policy.mask = 0x3F;
        if ((instance_data[instance].domain == VTSS_APPL_MEP_EVC) && (pag != 0)) {
            conf.isdx_enable = TRUE;
            conf.port_list.clear_all();
        }
    } else {
        conf.vid.value = vid;                       /* Look for this VID */
        conf.vid.mask = 0xFFFF;
    }

    for (i=0; i<mep_caps.mesa_port_cnt; ++i)     if (port[i])    break;
    if ((i < mep_caps.mesa_port_cnt) && (instance_data[instance].domain == VTSS_APPL_MEP_EVC)) { /* Check if this VID is EVC */
        if ((out_port_conv[i] != i) && (in_port_conv[i] == 0xFFFF)){ /* This port is protected and not active */
            port[i] = FALSE;
            port[out_port_conv[i]] = TRUE;     /* The protecting port must hit the rule */
        }
    }

    conf.frame.etype.etype.mask[0] = 0xFF;     /* Look for OAM Ether Type */
    conf.frame.etype.etype.mask[1] = 0xFF;
    conf.frame.etype.etype.value[0] = 0x89;
    conf.frame.etype.etype.value[1] = 0x02;
    conf.frame.etype.data.mask[0] = 7<<5;      /* Look for this level(s) */
    conf.frame.etype.data.value[0] = level<<5;
    conf.frame.etype.data.mask[1] = 0xFF;      /* Look for CCM */
    conf.frame.etype.data.value[1] = 0x01;
    memcpy(conf.frame.etype.smac.value, smac, sizeof(conf.frame.etype.smac.value));
    memset(conf.frame.etype.smac.mask, 0xFF, sizeof(conf.frame.etype.smac.mask));
    conf.action.cpu_once = TRUE;
    conf.action.force_cpu = FALSE;
    conf.action.port_action = MESA_ACL_PORT_ACTION_FILTER;
    conf.action.port_list = port;

    /* Allways create an ACL rule to be hit by any SMAC */
    memset(conf.frame.etype.smac.mask, 0x00, sizeof(conf.frame.etype.smac.mask));
    conf.id = ACL_MGMT_ACE_ID_NONE;
    if ((rc = mep_acl_ace_add(ACL_USER_MEP,  next_id,  &conf)) != VTSS_RC_OK) {
        T_W("mep_acl_ace_add() failed for this VID %u  instance %u  id %u  rc %s", vid, instance, conf.id, error_txt(rc));
        return FALSE;
    }
    instance_data[instance].ccm_any_ace_id[0] = conf.id;
    next_id = conf.id;

    if (hw_ccm) {   /* In case of HW CCM an ACL rule is created to count CCM from a specific SMAC */
        memset(conf.frame.etype.smac.mask, 0xFF, sizeof(conf.frame.etype.smac.mask));
        conf.id = ACL_MGMT_ACE_ID_NONE;
        if ((rc = mep_acl_ace_add(ACL_USER_MEP,  next_id,  &conf)) != VTSS_RC_OK) {
            T_W("mep_acl_ace_add() failed for this VID %u  instance %u  id %u  rc %s", vid, instance, conf.id, error_txt(rc));
            return FALSE;
        }

        instance_data[instance].ccm_loc_ace_id[0] = conf.id;
        next_id = conf.id;
    } /* HW CCM */

    T_D("Exit");
    return TRUE;
}

BOOL vtss_mep_acl_llm_voe_add(u32 instance, u32 isdx, u32 level, mesa_port_list_t &ports)
{






























































    return TRUE;
}

BOOL vtss_mep_acl_mip_llm_add(u32 instance, mesa_mac_t dmac, u8 level, mesa_port_list_t &ports, u32 mip_pag)
{

































































    return TRUE;
}

BOOL vtss_mep_acl_mip_lbm_add(u32 instance,  u32 dmac_port,  mesa_port_list_t &ports,  u32 mip_pag,  BOOL isdx_to_zero)
{



    acl_entry_conf_t  conf;
    u8                mac[VTSS_APPL_MEP_MAC_LENGTH];
    mesa_rc           rc;

    vtss_mep_mac_get(dmac_port,  mac);

    if (acl_mgmt_ace_init(MESA_ACE_TYPE_ETYPE, &conf) != VTSS_RC_OK)     return FALSE;

    conf.action.cpu_queue = PACKET_XTR_QU_OAM;
    conf.isid = VTSS_ISID_LOCAL;
    conf.policy.value = mip_pag;
    conf.policy.mask = 0X3F;
    conf.frame.etype.etype.mask[0] = 0xFF;
    conf.frame.etype.etype.mask[1] = 0xFF;
    conf.frame.etype.etype.value[0] = 0x89;
    conf.frame.etype.etype.value[1] = 0x02;
    conf.frame.etype.data.mask[1] = 0xFF;
    conf.frame.etype.data.value[1] = 3;
    memcpy(conf.frame.etype.dmac.value, mac, sizeof(conf.frame.etype.smac.value));
    memset(conf.frame.etype.dmac.mask, 0xFF, sizeof(conf.frame.etype.smac.mask));











    conf.port_list = ports;

    conf.action.force_cpu = TRUE;
    conf.action.port_action = MESA_ACL_PORT_ACTION_REDIR;
    conf.action.lm_cnt_disable = (isdx_to_zero) ? TRUE : FALSE;
    memset(conf.action.port_list, 0, sizeof(conf.action.port_list));
/*  This is not implemented yet
    if (isdx_to_zero) {
        conf.action.isdx_enable = TRUE;
        conf.action.isdx = 0;
    }
*/

    if ((rc = mep_acl_ace_add(ACL_USER_MEP, ACL_MGMT_ACE_ID_NONE, &conf)) != VTSS_RC_OK) {
        T_W("mep_acl_ace_add() failed for this  instance %u  id %u  rc %s", instance, conf.id, error_txt(rc));
        return FALSE;
    }

    instance_data[instance].mip_lbm_ace_id = conf.id;

    return TRUE;
}


static mesa_ace_id_t              service_mep_ace_id;
static mesa_ace_id_t              service_mip_ccm_ace_id;
static mesa_ace_id_t              service_mip_uni_ltm_ace_id;
static mesa_ace_id_t              service_mip_nni_ltm_ace_id;
static mesa_ace_id_t              service_mip_uni_llm_ace_id;
static mesa_ace_id_t              service_mip_nni_llm_ace_id;

BOOL vtss_mep_acl_etree_elan_add(u32 instance, u32 isdx, u32 port, mesa_port_list_t *ports, u32 pag, BOOL second)
{
    mesa_ace_counter_t  counter;
    acl_entry_conf_t    conf;
    u8                  mac[VTSS_APPL_MEP_MAC_LENGTH];
    mesa_ace_id_t       next_id;
    mesa_rc             rc;

    rc = acl_mgmt_ace_get(ACL_USER_MEP,  VTSS_ISID_LOCAL,  ACL_MGMT_ACE_ID_NONE,  &conf,  &counter,  TRUE);  /* Get first entry for this user - MEP */

    if (rc == VTSS_RC_OK)
        next_id = conf.id;
    else if (rc == VTSS_APPL_ACL_ERROR_ACE_NOT_FOUND)
        next_id = service_mep_ace_id;
    else
        return FALSE;

    vtss_mep_mac_get(instance_data[instance].res_port,  mac);

    /* Create the TST RX counting rule */
    if (acl_mgmt_ace_init(MESA_ACE_TYPE_ETYPE, &conf) != VTSS_RC_OK)     return FALSE;

    conf.action.cpu_queue = PACKET_XTR_QU_OAM;
    conf.isid = VTSS_ISID_LOCAL;
    conf.policy.value = pag;
    conf.policy.mask = 0X3F;
    conf.vid.value = isdx;
    conf.vid.mask = 0xFFFF;
    conf.isdx_enable = TRUE;
    conf.frame.etype.data.mask[1] = 0xFF;      /* Look for TST */
    conf.frame.etype.data.value[1] = 37;
    memcpy(conf.frame.etype.dmac.value, mac, sizeof(conf.frame.etype.smac.value));
    memset(conf.frame.etype.dmac.mask, 0xFF, sizeof(conf.frame.etype.smac.mask));
    conf.action.cpu_once = TRUE;
    conf.action.force_cpu = FALSE;
    conf.action.port_action = MESA_ACL_PORT_ACTION_REDIR;
    memset(conf.action.port_list, 0, sizeof(conf.action.port_list));
    if ((rc = mep_acl_ace_add(ACL_USER_MEP, next_id, &conf)) != VTSS_RC_OK) {
        T_W("mep_acl_ace_add() failed for this id  %u  rc %s", conf.id, error_txt(rc));
        return FALSE;
    }
    instance_data[instance].tst_ace_id[port] = conf.id;

    /* Create the Unicast CPU copy rule */
    if (acl_mgmt_ace_init(MESA_ACE_TYPE_ETYPE, &conf) != VTSS_RC_OK)     return FALSE;

    conf.action.cpu_queue = PACKET_XTR_QU_OAM;
    conf.isid = VTSS_ISID_LOCAL;
    conf.policy.value = pag;
    conf.policy.mask = 0X3F;
    conf.vid.value = isdx;
    conf.vid.mask = 0xFFFF;
    conf.isdx_enable = TRUE;
    memcpy(conf.frame.etype.dmac.value, mac, sizeof(conf.frame.etype.smac.value));
    memset(conf.frame.etype.dmac.mask, 0xFF, sizeof(conf.frame.etype.smac.mask));
    conf.action.force_cpu = TRUE;
    conf.action.port_action = MESA_ACL_PORT_ACTION_REDIR;
    memset(conf.action.port_list, 0, sizeof(conf.action.port_list));
    if ((rc = mep_acl_ace_add(ACL_USER_MEP, next_id, &conf)) != VTSS_RC_OK) {
        T_W("mep_acl_ace_add() failed for this id  %u  rc %s", conf.id, error_txt(rc));
        return FALSE;
    }

    instance_data[instance].etree_elan_uc_ace_id[port] = conf.id;

    if (!second) {  /* Only create the following two rules for the first E-TREE MEP instance */
        /* Create the Multicast forwarding and CPU copy rule */
        if (acl_mgmt_ace_init(MESA_ACE_TYPE_ETYPE, &conf) != VTSS_RC_OK)     return FALSE;

        conf.action.cpu_queue = PACKET_XTR_QU_OAM;
        conf.isid = VTSS_ISID_LOCAL;
        conf.policy.value = pag;
        conf.policy.mask = 0X3F;
        conf.vid.value = isdx;
        conf.vid.mask = 0xFFFF;
        conf.isdx_enable = TRUE;
        VTSS_BF_SET(conf.flags.value, ACE_FLAG_DMAC_MC, TRUE);
        VTSS_BF_SET(conf.flags.mask, ACE_FLAG_DMAC_MC, TRUE);
        conf.action.force_cpu = TRUE;
        conf.action.port_action = MESA_ACL_PORT_ACTION_FILTER;
        conf.action.port_list = *ports;
        if (mep_acl_ace_add(ACL_USER_MEP, service_mep_ace_id, &conf) != VTSS_RC_OK)      T_W("mep_acl_ace_add() failed for this id  %u", conf.id);
        instance_data[instance].etree_elan_mc_ace_id[port] = conf.id;

        if (acl_mgmt_ace_init(MESA_ACE_TYPE_ETYPE, &conf) != VTSS_RC_OK)     return FALSE;

        /* Create the other Unicast forwarding rule */
        conf.isid = VTSS_ISID_LOCAL;
        conf.policy.value = pag;
        conf.policy.mask = 0X3F;
        conf.vid.value = isdx;
        conf.vid.mask = 0xFFFF;
        conf.isdx_enable = TRUE;
        conf.action.port_action = MESA_ACL_PORT_ACTION_FILTER;
        conf.action.port_list = *ports;
        if ((rc = mep_acl_ace_add(ACL_USER_MEP, service_mep_ace_id, &conf)) != VTSS_RC_OK) {
            T_W("mep_acl_ace_add() failed for this id  %u  rc %s", conf.id, error_txt(rc));
            return FALSE;
        }
        instance_data[instance].etree_elan_other_uc_ace_id[port] = conf.id;
    }
    return TRUE;
}

BOOL vtss_mep_acl_evc_add(BOOL mep,  mesa_port_list_t &mip_nni,  mesa_port_list_t &mip_uni,  mesa_port_list_t &mip_egress,  u32 mep_pag,  u32 mip_pag)
{
    acl_entry_conf_t  conf, nxt_conf;
    mesa_rc           rc;

    if (!mep) {
        (void)acl_mgmt_ace_del(ACL_USER_MEP, service_mep_ace_id);
        service_mep_ace_id = ACL_MGMT_ACE_ID_NONE;
    }

    if (mep && (service_mep_ace_id == ACL_MGMT_ACE_ID_NONE)) { /* Add the SW MEP ACL */
        if (acl_mgmt_ace_init(MESA_ACE_TYPE_ETYPE, &conf) != VTSS_RC_OK)       return FALSE;

        conf.action.cpu_queue = PACKET_XTR_QU_OAM;
        conf.isid = VTSS_ISID_LOCAL;
        conf.policy.value = mep_pag;
        conf.policy.mask = 0X3F;
        conf.action.force_cpu = TRUE;
        conf.action.port_action = MESA_ACL_PORT_ACTION_REDIR;
        memset(conf.action.port_list, 0, sizeof(conf.action.port_list));
        if ((rc = mep_acl_ace_add(ACL_USER_MEP, ACL_MGMT_ACE_ID_NONE, &conf)) != VTSS_RC_OK) {
            T_W("mep_acl_ace_add() failed for this id  %u  rc %s", conf.id, error_txt(rc));
            return FALSE;
        }
        service_mep_ace_id = conf.id;
    }


















































































































































































































































































    T_D("Exit");
    return TRUE;
}

BOOL hit_acl_once(mesa_ace_id_t ace_id)
{
    mesa_rc              rc;
    acl_entry_conf_t     conf, nxt_conf;
    mesa_ace_counter_t   counter;

    /* Get conf for id */
    if (acl_mgmt_ace_get(ACL_USER_MEP,  VTSS_ISID_LOCAL,  ace_id,  &conf,  &counter,  FALSE) != VTSS_RC_OK)    return(FALSE);
    conf.action.cpu_once = TRUE;

    /* Get next config for this id */
    rc = acl_mgmt_ace_get(ACL_USER_MEP,  VTSS_ISID_LOCAL,  ace_id,  &nxt_conf,  &counter,  TRUE);
    if (rc == VTSS_APPL_ACL_ERROR_ACE_NOT_FOUND)
        nxt_conf.id = ACL_MGMT_ACE_ID_NONE;
    else
    if (rc != VTSS_RC_OK)
        return(FALSE);

    /* Add config for this id in front of next id */
    if (mep_acl_ace_add(ACL_USER_MEP,  nxt_conf.id,  &conf) != VTSS_RC_OK)      T_W("mep_acl_ace_add() failed for this id  %u", conf.id);
    return(TRUE);
}

void vtss_mep_acl_ccm_hit(u32 instance)
{
    mesa_rc  rc;

    if (instance_data[instance].ccm_loc_ace_id[0] != ACL_MGMT_ACE_ID_NONE)
    {
        rc = mesa_ace_counter_clear(NULL, instance_data[instance].ccm_loc_ace_id[0] | (ACL_USER_MEP<<16));
        if (rc != VTSS_RC_OK)      return;
    }
    if (instance_data[instance].ccm_any_ace_id[0] != ACL_MGMT_ACE_ID_NONE)
    {
        rc = mesa_ace_counter_clear(NULL, instance_data[instance].ccm_any_ace_id[0] | (ACL_USER_MEP<<16));
        if (rc != VTSS_RC_OK)      return;
    }
}

void vtss_mep_acl_ccm_count(u32 instance,  u32 *count)
{
    u32 i;
    mesa_ace_counter_t   counter=0;

    *count = 0;
    for (i=0; i<2; ++i) {
        if (instance_data[instance].ccm_loc_ace_id[i] != ACL_MGMT_ACE_ID_NONE)
        {
            if (mesa_ace_counter_get(NULL, instance_data[instance].ccm_loc_ace_id[i] | (ACL_USER_MEP<<16), &counter) != VTSS_RC_OK)      T_W("mesa_ace_counter_get() failed for this id  %u  i %u  instance %u", instance_data[instance].ccm_loc_ace_id[i], i, instance);
            *count += (u32)counter;
        }
    }
}

BOOL vtss_mep_acl_tst_add(u32 instance,  u32 vid,  mesa_port_list_t &port,  u32 level,  u32 pag)
{
    u32 i;
    mesa_ace_id_t       next_id;
    acl_entry_conf_t    conf = {};
    mesa_ace_counter_t  counter;
    mesa_rc             rc;

    rc = acl_mgmt_ace_get(ACL_USER_MEP,  VTSS_ISID_LOCAL,  ACL_MGMT_ACE_ID_NONE,  &conf,  &counter,  TRUE);

    if (rc == VTSS_RC_OK)
        next_id = conf.id;
    else
    if (rc == VTSS_APPL_ACL_ERROR_ACE_NOT_FOUND)
        next_id = ACL_MGMT_ACE_ID_NONE;
    else
        return FALSE;

    if (acl_mgmt_ace_init(MESA_ACE_TYPE_ETYPE, &conf) != VTSS_RC_OK) {
        return FALSE;
    }

    conf.action.cpu_queue = PACKET_XTR_QU_OAM;
    conf.isid = VTSS_ISID_LOCAL;
    conf.port_list = port;

    if (mep_caps.mesa_mep_serval) {
        conf.vid.value = vid;                          /* Look for this VID/ISDX */
        conf.vid.mask = 0xFFFF;
        conf.policy.value = pag;                       /* Look for this PAG */
        conf.policy.mask = 0x3F;
        if ((instance_data[instance].domain == VTSS_APPL_MEP_EVC) && (pag != 0)) {
            conf.isdx_enable = TRUE;
            conf.port_list.clear_all();
        }
    } else {
        conf.vid.value = vid;                       /* Look for this VID */
        conf.vid.mask = 0xFFFF;
    }

    for (i=0; i<mep_caps.mesa_port_cnt; ++i)     if (port[i])    break;
    if ((i < mep_caps.mesa_port_cnt) && (instance_data[instance].domain == VTSS_APPL_MEP_EVC)) { /* Check if this VID is EVC */
        if ((out_port_conv[i] != i) && (in_port_conv[i] == 0xFFFF)){ /* This port is protected and not active */
            port[i] = FALSE;
            port[out_port_conv[i]] = TRUE;     /* The protecting port must hit the rule */
        }
    }

    conf.frame.etype.etype.mask[0] = 0xFF;     /* Look for OAM Ether Type */
    conf.frame.etype.etype.mask[1] = 0xFF;
    conf.frame.etype.etype.value[0] = 0x89;
    conf.frame.etype.etype.value[1] = 0x02;
    conf.frame.etype.data.mask[0] = 7<<5;      /* Look for this level(s) */
    conf.frame.etype.data.value[0] = level<<5;
    conf.frame.etype.data.mask[1] = 0xFF;      /* Look for TST */
    conf.frame.etype.data.value[1] = 37;
    conf.action.cpu_once = TRUE;
    conf.action.force_cpu = FALSE;
    conf.action.port_action = MESA_ACL_PORT_ACTION_FILTER;
    conf.action.port_list = port;

    if ((rc = mep_acl_ace_add(ACL_USER_MEP,  next_id,  &conf)) != VTSS_RC_OK) {
        T_W("mep_acl_ace_add() failed for this VID %u  instance %u  id %u  rc %s", vid, instance, conf.id, error_txt(rc));
        return FALSE;
    }

    instance_data[instance].tst_ace_id[0] = conf.id;
    return TRUE;
}

void vtss_mep_acl_tst_count(u32 instance,  u32 *count)
{
    u32 i;
    mesa_ace_counter_t   counter=0;

    *count = 0;
    for (i=0; i<mep_caps.mesa_port_cnt; ++i) {
        if (instance_data[instance].tst_ace_id[i] != ACL_MGMT_ACE_ID_NONE)
        {
            if (mesa_ace_counter_get(NULL, instance_data[instance].tst_ace_id[i] | (ACL_USER_MEP<<16), &counter) != VTSS_RC_OK)      T_W("mesa_ace_counter_get() failed for this id  %u", instance_data[instance].tst_ace_id[i]);
            *count += (u32)counter;
        }
    }
}

void vtss_mep_acl_tst_clear(u32 instance)
{
    u32     i;

    for (i=0; i<mep_caps.mesa_port_cnt; ++i) {
        if (instance_data[instance].tst_ace_id[i] != ACL_MGMT_ACE_ID_NONE) {
            if (mesa_ace_counter_clear(NULL, instance_data[instance].tst_ace_id[i] | (ACL_USER_MEP<<16)) != VTSS_RC_OK)      T_W("mesa_ace_counter_clear() failed for this id  %u", instance_data[instance].tst_ace_id[i]);
        }
    }
}

#endif /* VTSS_SW_OPTION_ACL */

void vtss_mep_rule_update_failed(u32 vid)
{
    T_W("Updating rules (MAC/ACL) failed for this VID  %u", vid);
//T_D("vtss_mep_update_failed  vid %u\n", vid);
}

BOOL vtss_mep_protection_port_get(const u32 port,  u32 *p_port)
{
    u32 i;

    if (out_port_conv[port] != port) { /* This port is a protected port (working) */
        *p_port = out_port_conv[port];
        return(TRUE);
    }

    for (i=0; i<mep_caps.mesa_port_cnt; ++i)  /* If this is a protecting port - find working */
        if ((i != port) && (out_port_conv[i] == port))      break;

    if (i != mep_caps.mesa_port_cnt) {    /* A working port was found for this protecting port */
        *p_port = out_port_conv[i];
        return(TRUE);
    }

    *p_port = 0;
    return(FALSE);
}


/****************************************************************************/
/*  MEP lower support platform interface                                    */
/****************************************************************************/

void vtss_mep_supp_loc_crit_lock(const char *location,
                                 uint       line_no)
{
    critd_enter(&crit_supp, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, location, line_no);
}

void vtss_mep_supp_loc_crit_unlock(const char *location,
                                   uint       line_no)
{
    critd_exit(&crit_supp, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, location, line_no);
}

void vtss_mep_supp_run(void)
{
    vtss_flag_setbits(&run_wait_flag, FLAG_RUN);
}

void vtss_mep_supp_timer_start(void)
{
    vtss_flag_setbits(&timer_wait_flag, FLAG_TIMER_SUPP);
}

void vtss_mep_supp_trace(const char  *const string,
                         const u32   param1,
                         const u32   param2,
                         const u32   param3,
                         const u32   param4)
{
    T_D("%s - %u, %u, %u, %u", string, param1, param2, param3, param4);
}

void vtss_mep_supp_trace_err(const char  *const string,
                             const u32   param1,
                             const u32   param2,
                             const u32   param3,
                             const u32   param4)
{
    T_E("%s - %u, %u, %u, %u", string, param1, param2, param3, param4);
}


void vtss_mep_supp_lm_trace(const char  *const string,
                            const u32   param1,
                            const u32   param2,
                            const u32   param3,
                            const u32   param4)
{
    T_DG(TRACE_GRP_LM, "%s - %u, %u, %u, %u", string, param1, param2, param3, param4);
}

void vtss_mep_supp_dm_trace(const char  *const string,
                            const u32   param1,
                            const u32   param2,
                            const u32   param3,
                            const u32   param4)
{
    T_DG(TRACE_GRP_DM, "%s - %u, %u, %u, %u", string, param1, param2, param3, param4);
}

u32 vtss_mep_supp_port_ether_type(u32 port)
{
    u32                         rc=0;
    mesa_vlan_conf_t            vlan_sconf;
    vtss_appl_vlan_port_conf_t  vlan_conf;

    vlan_sconf.s_etype = 0x88A8;
    vlan_conf.hybrid.port_type = VTSS_APPL_VLAN_PORT_TYPE_C;

    if ((rc = mesa_vlan_conf_get(NULL, &vlan_sconf)) != VTSS_RC_OK) { /* Get the custom S-Port EtherType */
        T_E("mesa_vlan_conf_get failed %s", error_txt(rc));
    }
    if ((rc = vlan_mgmt_port_conf_get(VTSS_ISID_START, port, &vlan_conf, VTSS_APPL_VLAN_USER_ALL, TRUE)) != VTSS_RC_OK) {  /* Get the port type */
        T_E("mesa_vlan_conf_get failed %s", error_txt(rc));
    }

    switch (vlan_conf.hybrid.port_type) {
        case VTSS_APPL_VLAN_PORT_TYPE_C:        return(0x8100);
        case VTSS_APPL_VLAN_PORT_TYPE_S:        return(0x88A8);
        case VTSS_APPL_VLAN_PORT_TYPE_S_CUSTOM: return(vlan_sconf.s_etype);
        case VTSS_APPL_VLAN_PORT_TYPE_UNAWARE:  return(0x8100);
    }
    return(0x8100);
}

void vtss_mep_supp_port_conf(const u32       instance,
                             const u32       port,
                             const BOOL      up_mep,
                             const BOOL      out_of_service)
{
    port_vol_conf_t            port_conf;
    vtss_appl_vlan_port_conf_t vlan_conf;
    mesa_rc                    rc;

    if (port_vol_conf_get(PORT_USER_MEP, VTSS_ISID_START, port, &port_conf) != VTSS_RC_OK) {
        T_I("port_vol_conf_get() failed port %d", port);
        return;
    }

    port_conf.loop = out_of_service ? MESA_PORT_LOOP_PCS_HOST : MESA_PORT_LOOP_DISABLE;
    port_conf.oper_up = out_of_service || up_mep;
    port_conf.oper_down = FALSE;

    if (port_vol_conf_set_sync(PORT_USER_MEP, VTSS_ISID_START, port, &port_conf) != VTSS_RC_OK) {
        T_I("port_vol_conf_set_sync() failed port %d", port);
    }

    memset(&vlan_conf, 0, sizeof(vlan_conf));
    vlan_conf.hybrid.flags = port_conf.loop ? VTSS_APPL_VLAN_PORT_FLAGS_TX_TAG_TYPE : 0;
    vlan_conf.hybrid.tx_tag_type = VTSS_APPL_VLAN_TX_TAG_TYPE_TAG_ALL;

    if ((rc = vlan_mgmt_port_conf_set(VTSS_ISID_START, port, &vlan_conf, VTSS_APPL_VLAN_USER_MEP)) != VTSS_RC_OK) {
        T_E("vlan_mgmt_port_conf_set failed  port %d. Error = %s", port, error_txt(rc));
    }
}

void vtss_mep_port_conf_disable(const u32       instance,
                                const u32       port,
                                const BOOL      disable)
{
    port_vol_conf_t            port_conf;

    if (port_vol_conf_get(PORT_USER_MEP, VTSS_ISID_START, port, &port_conf) != VTSS_RC_OK) {
        T_I("port_vol_conf_set failed  port %d", port);
        return;
    }

    if (port_conf.disable != disable) {
        port_conf.disable = disable;

        if (port_vol_conf_set(PORT_USER_MEP, VTSS_ISID_START, port, &port_conf) != VTSS_RC_OK) {
            T_I("port_vol_conf_set failed  port %d", port);
        }
    }
}

void vtss_mep_supp_packet_tx_frm_cnt(u8 *frm, u64 *frm_cnt)
{
    *frm_cnt = 0;
}

u8 *vtss_mep_supp_packet_tx_alloc(u32 size)
{
    u8 *buffer = 0;
    buffer = packet_tx_alloc(size);
    if (buffer == NULL) {
        T_EG(TRACE_GRP_TX_FRAME, "packet_tx_alloc() returned NULL pointer");
        return NULL;
    }
    memset(buffer, 0, size);
    T_NG(TRACE_GRP_TX_FRAME, "Enter  frame %p", buffer);
    return buffer;
}

void vtss_mep_supp_packet_tx_free(u8 *buffer)
{
    T_NG(TRACE_GRP_TX_FRAME, "Enter  frame %p", buffer);

    packet_tx_free(buffer);
}

void vtss_mep_supp_packet_tx_cancel(u32 instance, u8 *buffer, u64 *active_time_ms)
{
    T_DG(TRACE_GRP_TX_FRAME, "Enter  frame %p", buffer);
#if defined(VTSS_SW_OPTION_AFI)
    if (mep_caps.appl_afi) {
        u32           i, j, tx_idx, afi_id;
        tx_buffer_t   *tx_buffer;
        BOOL          multi;

        for (tx_idx=0; tx_idx<TX_BUF_MAX; ++tx_idx) { /* Find buffer in a tx_buffer element */
            if (!instance_data[instance].tx_buffer[tx_idx].used)    continue;
            for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {
                for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
                    if (instance_data[instance].tx_buffer[tx_idx].port[0].data[i][j].frame == buffer)   goto done;
                }
            }
        }
        done:

        if ((tx_idx < TX_BUF_MAX) && (i < VTSS_APPL_MEP_PRIO_MAX) && (j < VTSS_APPL_MEP_TEST_MAX)) { /* buffer for port '0' was found */
            tx_buffer = &instance_data[instance].tx_buffer[tx_idx];
            multi = tx_buffer->multi;
            afi_id = tx_buffer->port[0].data[i][j].afi_id;
            T_NG(TRACE_GRP_TX_FRAME, "tx_buffer element was found  multi %u  afi_id %u  i %u  j %u", multi, afi_id, i, j);

            if (multi && (afi_multi_free(afi_id, active_time_ms) != VTSS_RC_OK))   /* Cancel multi AFI */
                T_D("Error during afi_multi_free()");
            if (!multi && (afi_single_free(afi_id, active_time_ms) != VTSS_RC_OK))   /* Cancel single AFI */
                T_D("Error during afi_single_free()");
            tx_buffer->port[0].data[i][j].afi_id = AFI_ID_NONE;

            if (tx_buffer->port[1].data[i][j].afi_id != AFI_ID_NONE) {  /* AFI id for port '1' is used */
                afi_id = tx_buffer->port[1].data[i][j].afi_id;
                T_NG(TRACE_GRP_TX_FRAME, "Second port AFI was found  afi_id %u", afi_id);
                if (multi && (afi_multi_free(afi_id, active_time_ms) != VTSS_RC_OK))   /* Cancel multi AFI */
                    T_D("Error during afi_multi_free()");
                if (!multi && (afi_single_free(afi_id, active_time_ms) != VTSS_RC_OK))   /* Cancel single AFI */
                    T_D("Error during afi_single_free()");
                tx_buffer->port[1].data[i][j].afi_id = AFI_ID_NONE;
            }

            for (i=0; i<VTSS_APPL_MEP_PRIO_MAX; ++i) {   /* Check for tx_buffer not used */
                for (j=0; j<VTSS_APPL_MEP_TEST_MAX; ++j) {
                    if (tx_buffer->port[0].data[i][j].afi_id != AFI_ID_NONE)   goto done1;
                }
            }
            done1:

            if ((i == VTSS_APPL_MEP_PRIO_MAX) && (j == VTSS_APPL_MEP_TEST_MAX)) { /* This tx_buffer is not used any more */
                memset(tx_buffer, 0, sizeof(*tx_buffer));
            }
        } else {
            T_NG(TRACE_GRP_TX_FRAME, "tx_buffer element was NOT!! found");
        }

    }
#endif /* VTSS_SW_OPTION_AFI */
}

mesa_packet_oam_type_t oam_type_calc(vtss_mep_supp_oam_type_t   oam_type)
{
    switch(oam_type) {
        case VTSS_MEP_SUPP_OAM_TYPE_NONE:     return(MESA_PACKET_OAM_TYPE_NONE);
        case VTSS_MEP_SUPP_OAM_TYPE_CCM:      return(MESA_PACKET_OAM_TYPE_CCM);
        case VTSS_MEP_SUPP_OAM_TYPE_CCM_LM:   return(MESA_PACKET_OAM_TYPE_CCM_LM);
        case VTSS_MEP_SUPP_OAM_TYPE_LBM:      return(MESA_PACKET_OAM_TYPE_LBM);
        case VTSS_MEP_SUPP_OAM_TYPE_LBR:      return(MESA_PACKET_OAM_TYPE_LBR);
        case VTSS_MEP_SUPP_OAM_TYPE_LMM:      return(MESA_PACKET_OAM_TYPE_LMM);
        case VTSS_MEP_SUPP_OAM_TYPE_LMR:      return(MESA_PACKET_OAM_TYPE_LMR);
        case VTSS_MEP_SUPP_OAM_TYPE_DMM:      return(MESA_PACKET_OAM_TYPE_DMM);
        case VTSS_MEP_SUPP_OAM_TYPE_DMR:      return(MESA_PACKET_OAM_TYPE_DMR);
        case VTSS_MEP_SUPP_OAM_TYPE_1DM:      return(MESA_PACKET_OAM_TYPE_1DM);
        case VTSS_MEP_SUPP_OAM_TYPE_LTM:      return(MESA_PACKET_OAM_TYPE_LTM);
        case VTSS_MEP_SUPP_OAM_TYPE_LTR:      return(MESA_PACKET_OAM_TYPE_LTR);
        case VTSS_MEP_SUPP_OAM_TYPE_GENERIC:  return(MESA_PACKET_OAM_TYPE_GENERIC);
        default:                              return(MESA_PACKET_OAM_TYPE_NONE);
    }
}

#if defined(VTSS_SW_OPTION_AFI)
static u32 afi_tx(u32 instance, packet_tx_props_t *tx_props, u32 rate, BOOL line_rate, u32 *afi_id, BOOL sat)
{
    u32                 rc = 0;
    u32                 retval = VTSS_MEP_RC_TX_ERROR;
    unsigned char       *frm;
    afi_single_conf_t   single_conf;
    afi_multi_conf_t    multi_conf;

    T_DG(TRACE_GRP_TX_FRAME, "Enter  rate %X  line_rate %u", rate, line_rate);

    if ((rc = afi_single_conf_init(&single_conf)) != VTSS_RC_OK) {
        T_DG(TRACE_GRP_TX_FRAME, "afi_single_conf_init() failed %s", error_txt(rc));
        return(retval);
    }
    if ((rc = afi_multi_conf_init(&multi_conf)) != VTSS_RC_OK) {
        T_DG(TRACE_GRP_TX_FRAME, "afi_multi_conf_init() failed %s", error_txt(rc));
        return(retval);
    }

    *afi_id = 0;

    if (rate & VTSS_MEP_SUPP_RATE_IS_BPS) { /* Check for BPS or FPH rate */
        /* BPS - AFI multi */
        rate &= ~VTSS_MEP_SUPP_RATE_IS_BPS;
        rate *= 1000;   /* Here rate is given in kbps */
        multi_conf.params.bps = (rate < AFI_MULTI_RATE_BPS_MIN) ? AFI_MULTI_RATE_BPS_MIN : (rate <= AFI_MULTI_RATE_BPS_MAX) ? rate : AFI_MULTI_RATE_BPS_MAX;
        multi_conf.params.line_rate = line_rate;
        multi_conf.params.seq_cnt = 0;  /* Run forever (until stopped) */
        multi_conf.params.alternate_resource_pool = sat;
        multi_conf.tx_props[0] = *tx_props;
        if ((rc = afi_multi_alloc(&multi_conf, afi_id)) != VTSS_RC_OK) {
            T_WG(TRACE_GRP_TX_FRAME, "afi_multi_alloc() failed %s", error_txt(rc));
            retval = VTSS_MEP_RC_OUT_OF_TX_RESOURCE;
            return(retval);
        }

        rc = afi_multi_pause_resume(*afi_id, FALSE);

        if (rc != VTSS_RC_OK) {
            (void)afi_multi_free(*afi_id, NULL);
            *afi_id = AFI_ID_NONE;
            T_DG(TRACE_GRP_TX_FRAME, "afi_multi_pause_resume() failed %s", error_txt(rc));
            return(rc);
        }
    } else {
        /* FPH - AFI single */
        u64 fph = rate * 3600LLU;
        single_conf.params.fph = (fph <= AFI_SINGLE_RATE_FPH_MAX) ? fph : AFI_SINGLE_RATE_FPH_MAX;
        single_conf.tx_props = *tx_props;
        if ((rc = afi_single_alloc(&single_conf, afi_id)) != VTSS_RC_OK) {
            T_DG(TRACE_GRP_TX_FRAME, "afi_single_alloc() failed %s", error_txt(rc));
            retval = VTSS_MEP_RC_OUT_OF_TX_RESOURCE;
            return(retval);
        }

        rc = afi_single_pause_resume(*afi_id, FALSE);

        if (rc != VTSS_RC_OK) {
            T_DG(TRACE_GRP_TX_FRAME, "afi_single_pause_resume() failed %s", error_txt(rc));
            (void)afi_single_free(*afi_id, NULL);
            *afi_id = AFI_ID_NONE;
            return(rc);
        }
    }

    frm = tx_props->packet_info.frm;
    T_DG(TRACE_GRP_TX_FRAME, "frame DMAC-SMAC %X-%X-%X-%X-%X-%X-%X-%X-%X-%X-%X-%X",
        frm[0], frm[1], frm[2], frm[3], frm[4], frm[5], frm[6], frm[7], frm[8], frm[9], frm[10], frm[11]);
    T_DG(TRACE_GRP_TX_FRAME, "instance %u  port_mask 0x%" PRIx64 "  isdx %u  vid %u  qos %u  pcp %u  dp %u  len %zu  maskarade %u  switched %u  oam_type %u  frame %X-%X-%X-%X-%X-%X-%X-%X-%X-%X",
        instance, tx_props->tx_info.dst_port_mask, tx_props->tx_info.isdx, tx_props->tx_info.tag.vid, tx_props->tx_info.cos, tx_props->tx_info.tag.pcp,
        tx_props->tx_info.dp, tx_props->packet_info.len, tx_props->tx_info.masquerade_port, tx_props->tx_info.switch_frm, tx_props->tx_info.oam_type,
        frm[12+0], frm[12+1], frm[12+2], frm[12+3], frm[12+4], frm[12+5], frm[12+6], frm[12+7], frm[12+8], frm[12+9]);

    return(VTSS_RC_OK);
}
#endif /* VTSS_SW_OPTION_AFI*/


u32 vtss_mep_supp_packet_tx(u32                            instance,
                            mesa_timestamp_t               *tx_timestamp,
                            mesa_port_list_t               &port,
                            u32                            len,
                            unsigned char                  *frm,
                            vtss_mep_supp_tx_frame_info_t  *frame_info,
                            u32                            rate,
                            BOOL                           count_enable,
                            u32                            seq_offset,
                            vtss_mep_supp_oam_type_t       oam_type,
                            BOOL                           sat)
{
    u32 rc = 0;
    u32  i, port_mask=0, mask=0;
    BOOL port_down_mep, bypass_on_not_res;
    packet_tx_props_t         tx_props;
    mesa_packet_port_info_t   info;
    CapArray<mesa_packet_port_filter_t, MESA_CAP_PORT_CNT> filter;
    instance_data_t           *data;

    T_DG(TRACE_GRP_TX_FRAME, "Enter  instance %u", instance);

    if (frm == NULL)     return(VTSS_MEP_RC_TX_ERROR);

    /* This is in order to inter-operate with vendors that require tagged frames to be transmitted with minimum length of 68 bytes - four bytes will be added by packet_tx() */
    /* In case of rate <> 0 the frame is either CCM (>64) or a TST/LBM PDU. TST generator need to transmit frames smaller that 64 byte */
    if ((!rate) && (len < 64) && ((frm[12] == 0x81) || (frm[12] == 0x88)))   len = 64;

    if (mesa_packet_port_info_init(&info) != VTSS_RC_OK)
        return(VTSS_MEP_RC_TX_ERROR);

    info.vid = frame_info->vid;
    if (mesa_packet_port_filter_get(NULL, &info, filter.size(), filter.data()) != VTSS_RC_OK) /* Get the filter info for this VID */
        return(VTSS_MEP_RC_TX_ERROR);

    CRIT_ENTER(crit_p);
    data = &instance_data[instance];    /* Instance data reference */
    port_down_mep = ((data->domain == VTSS_APPL_MEP_PORT || MPLS_DOMAIN(data->domain)) && port[data->res_port]);
    bypass_on_not_res = (frame_info->bypass && !port[data->res_port]);

    if (frame_info->bypass) { /* This is injection directly on a port */
        for (i=0, mask=1; i<mep_caps.mesa_port_cnt; ++i, mask<<=1) {   /* All ports are checked to be valid */
            if (!port[i])    continue;
            if ((((filter[i].filter != MESA_PACKET_FILTER_DISCARD) || !bypass_on_not_res) &&   /* Port must not be filtering if injection directly on a port that is not the residence port */
                (out_port_tx[i] || port_down_mep))) {                                         /* The port must not be blocked by port protection unless it is a port Down MEP */
                port_mask |= mask;
            } else {    /* Transmit will not happen on this port */
                T_DG(TRACE_GRP_DISCARD, "invalid port  los %u  filter %u  i %u  out_port_tx %u",los_state.get(i),filter[i].filter,i,out_port_tx.get(i));
            }
        }
        if (!port_mask) { /* There must be a port mask when transmitting directly on a port */
            T_DG(TRACE_GRP_DISCARD, "Empty port mask - not transmitting  rate %u  port_down_mep %u  bypass_on_not_res %u  bypass %u  res_port %u",rate,port_down_mep,bypass_on_not_res,frame_info->bypass,instance_data[instance].res_port);
            CRIT_EXIT(crit_p);
            return(VTSS_MEP_RC_TX_ERROR);
        }
    }

    packet_tx_props_init(&tx_props);
    tx_props.packet_info.modid       = VTSS_MODULE_ID_MEP;
    tx_props.packet_info.frm         = frm;
    tx_props.packet_info.len         = len;
    tx_props.tx_info.dst_port_mask   = port_mask;
    tx_props.packet_info.no_free     = TRUE;
    tx_props.tx_info.switch_frm      = !frame_info->bypass;
    tx_props.tx_info.cos             = frame_info->qos;
    tx_props.tx_info.dp              = frame_info->dp;
    tx_props.tx_info.tag.dei         = frame_info->dp;
    tx_props.tx_info.tag.pcp         = frame_info->pcp;
    tx_props.tx_info.isdx            = (frame_info->isdx != VTSS_MEP_SUPP_INDX_INVALID) ? frame_info->isdx : VTSS_ISDX_NONE;
    tx_props.tx_info.masquerade_port = frame_info->maskerade_port;
    tx_props.tx_info.tag.vid         = frame_info->vid_inj ? frame_info->vid : 0;
    if (mep_caps.mesa_mep_serval) {
        tx_props.tx_info.oam_type           = oam_type_calc(oam_type);
    }

    if (!rate) {    /* This is SW generated frame - NO AFI */
        if ((rc = packet_tx(&tx_props)) != VTSS_RC_OK) {    /* Transmit */
            T_EG(TRACE_GRP_TX_FRAME, "packet_tx() failed %s", error_txt(rc));
            CRIT_EXIT(crit_p);
            return(VTSS_MEP_RC_TX_ERROR);
        } else {
            T_NG(TRACE_GRP_TX_FRAME, "instance %u  port_mask 0x%" PRIx64 "  isdx %u  vid %u  qos %u  pcp %u  dp %u  len %zu  maskarade %u  switched %u  oam_type %u  frame %X-%X-%X-%X-%X-%X-%X-%X-%X",
                instance, tx_props.tx_info.dst_port_mask, tx_props.tx_info.isdx, tx_props.tx_info.tag.vid, tx_props.tx_info.cos, tx_props.tx_info.tag.pcp,
                tx_props.tx_info.dp, tx_props.packet_info.len, tx_props.tx_info.masquerade_port, tx_props.tx_info.switch_frm, tx_props.tx_info.oam_type,
                frm[12+0], frm[12+1], frm[12+2], frm[12+3], frm[12+4], frm[12+5], frm[12+6], frm[12+7], frm[12+8]);
        }
    } else { /* This is AFI based CCM or TST/LBM - transmission on more ports demands more buffers */
#if defined(VTSS_SW_OPTION_AFI)
        if (mep_caps.appl_afi) {
            u32  afi_id;

            T_DG(TRACE_GRP_TX_FRAME, "AFI injection start  frame %p  qos%u  dp %u", frm, frame_info->qos, frame_info->dp);

            if ((frame_info->qos >= VTSS_APPL_MEP_PRIO_MAX) || (frame_info->dp >= VTSS_APPL_MEP_TEST_MAX)) {
                T_DG(TRACE_GRP_TX_FRAME, "Invalid qos or dp  qos = %u  dp = %u", frame_info->qos, frame_info->dp);
                CRIT_EXIT(crit_p);
                return(VTSS_MEP_RC_TX_ERROR);
            }

            if (mep_caps.mesa_packet_vstax) {
                tx_props.tx_info.tx_vstax_hdr = MESA_PACKET_TX_VSTAX_NONE;
            }
 #if VTSS_OPT_FDMA
            tx_props.fdma_info.afi_fps = 0;
            tx_props.fdma_info.afi_enable_counting = count_enable;
            if (seq_offset != 0) { /* Offset for sequence numbers is indicating active */
                tx_props.fdma_info.afi_enable_sequence_numbering = TRUE;
                tx_props.fdma_info.afi_sequence_number_offset = seq_offset;
            }
#endif
            i = 0;
            if (frame_info->bypass) { /* This is AFI injection directly on a port - port mask must only contain one port */
                for (i=0, mask=1; i<mep_caps.mesa_port_cnt; ++i, mask<<=1)
                    if (port_mask & mask)    break;         /* Transmit frame on the first port in mask */
                if (i == mep_caps.mesa_port_cnt) {
                    CRIT_EXIT(crit_p);
                    return(VTSS_MEP_RC_TX_ERROR);
                }
                tx_props.tx_info.dst_port_mask = VTSS_BIT64(i);
#if VTSS_OPT_FDMA
                tx_props.fdma_info.afi_enable_counting = (in_port_conv[i] == 0xFFFF) ? FALSE : count_enable;    /* No counting and call back on not active port */
#endif
            }
            u32 tx_idx;
            for (tx_idx=0; tx_idx<TX_BUF_MAX; ++tx_idx)  /* Find a free tx_buffer element. In case of multi, many flows can be started on different COS/DP */
                if (!data->tx_buffer[tx_idx].used || ((rate & VTSS_MEP_SUPP_RATE_IS_BPS) && data->tx_buffer[tx_idx].multi))   break;
            if (tx_idx >= TX_BUF_MAX) {
                T_DG(TRACE_GRP_TX_FRAME, "No TX buffer resource was found");
                CRIT_EXIT(crit_p);
                return(VTSS_MEP_RC_TX_ERROR);
            }

            if ((rc = afi_tx(instance, &tx_props, rate, frame_info->line_rate, &afi_id, sat)) != VTSS_RC_OK) {   /* Start AFI transmission */
                CRIT_EXIT(crit_p);
                return(rc);
            }
            /* Save frame pointer and AFI id */
            T_DG(TRACE_GRP_TX_FRAME, "Save frame and AFI  tx_idx %u  frame %p  afi_id %u  qos %u  dp %u", tx_idx, frm, afi_id, frame_info->qos, frame_info->dp);
            if (!data->tx_buffer[tx_idx].used) {
                memset(&data->tx_buffer[tx_idx], 0, sizeof(data->tx_buffer[tx_idx]));   /* This is first use of this tx_buffer */
                data->tx_buffer[tx_idx].used = TRUE;
                data->tx_buffer[tx_idx].multi = (rate & VTSS_MEP_SUPP_RATE_IS_BPS) ? TRUE : FALSE;
            } else {
                if (data->tx_buffer[tx_idx].port[0].data[frame_info->qos][frame_info->dp].afi_id != AFI_ID_NONE) {   /* Check if transmission is ongoing on this COS/DP */
                    T_DG(TRACE_GRP_TX_FRAME, "No TX buffer resource was found");
                    CRIT_EXIT(crit_p);
                    return(VTSS_MEP_RC_TX_ERROR);
                }
            }
            data->tx_buffer[tx_idx].port[0].data[frame_info->qos][frame_info->dp].frame = frm;
            data->tx_buffer[tx_idx].port[0].data[frame_info->qos][frame_info->dp].afi_id = afi_id;

            if (frame_info->bypass) { /* This is AFI injection directly on a port - there might be more port in the port mask */
                for (i=i+1, mask<<=1; i<mep_caps.mesa_port_cnt; ++i, mask<<=1)    /* Search for a second port in mask */
                    if (port_mask & mask)    break;
                if (i == mep_caps.mesa_port_cnt) { /* Check if any port was found - if not we are done */
                    CRIT_EXIT(crit_p);
                    return(VTSS_RC_OK);
                }

                /* Transmit copy of frame on the second port */
                tx_props.tx_info.dst_port_mask = VTSS_BIT64(i);
#if VTSS_OPT_FDMA
                tx_props.fdma_info.afi_enable_counting = (in_port_conv[i] == 0xFFFF) ? FALSE : count_enable;    /* No counting and call back on not active port */
#endif

                if ((rc = afi_tx(instance,  &tx_props,  rate,  frame_info->line_rate, &afi_id, sat)) != VTSS_RC_OK) {   /* Start AFI transmission */
                    CRIT_EXIT(crit_p);
                    return(VTSS_RC_OK);
                }
                /* Save AFI id */
                T_DG(TRACE_GRP_TX_FRAME, "Save (second port) frame and AFI  tx_idx %u  frame %p  afi_id %u", tx_idx, frm, afi_id);
                data->tx_buffer[tx_idx].port[1].data[frame_info->qos][frame_info->dp].afi_id = afi_id;
            }
        }
#endif /* VTSS_SW_OPTION_AFI*/
    }

    CRIT_EXIT(crit_p);

    T_DG(TRACE_GRP_TX_FRAME, "Exit  instance %u", instance);

    if (tx_timestamp) {
        mesa_os_timestamp_t sw_tstamp;

        MESA_OS_TIMESTAMP(&sw_tstamp);
        vtss_tod_ts_to_time(0, sw_tstamp.hw_cnt, tx_timestamp);
    }

    return (VTSS_RC_OK);
}

u32 vtss_mep_supp_packet_tx_1(u32                            instance,
                              mesa_timestamp_t               *tx_timestamp,
                              u32                            port,
                              u32                            len,
                              unsigned char                  *frm,
                              vtss_mep_supp_tx_frame_info_t  *frame_info,
                              vtss_mep_supp_oam_type_t       oam_type)
{
    u32 rc = 0;
    u32 i, mask;
    packet_tx_props_t tx_props;

    if (frm == NULL)     return(rc);

    /* This is in order to inter-orperate with vendors that require tagged frames to be transmitted with minimum length of 68 bytes - four bytes will be added by packet_tx() */
    if ((len < 64) && ((frm[12] == 0x81) || (frm[12] == 0x88)))   len = 64;

    CRIT_ENTER(crit_p);
    packet_tx_props_init(&tx_props);
    tx_props.packet_info.modid              = VTSS_MODULE_ID_MEP;
    tx_props.packet_info.frm                = frm;
    tx_props.packet_info.len                = len;
    tx_props.packet_info.no_free            = TRUE;
    tx_props.tx_info.switch_frm             = !frame_info->bypass;
    tx_props.tx_info.cos                    = frame_info->qos;
    tx_props.tx_info.dp                     = frame_info->dp;
    tx_props.tx_info.tag.pcp                = frame_info->pcp;
    tx_props.tx_info.isdx                   = (frame_info->isdx != VTSS_MEP_SUPP_INDX_INVALID) ? frame_info->isdx : VTSS_ISDX_NONE;
    tx_props.tx_info.masquerade_port        = frame_info->maskerade_port;
    tx_props.tx_info.tag.vid                = frame_info->vid_inj ? frame_info->vid : 0;
    if (mep_caps.mesa_mep_serval) {
        tx_props.tx_info.oam_type           = oam_type_calc(oam_type);
    }

    if ((instance_data[instance].domain == VTSS_APPL_MEP_EVC) || (instance_data[instance].domain == VTSS_APPL_MEP_VLAN)) {
        tx_props.tx_info.dst_port_mask = port_protection_mask_calc(port);
        for (i=0, mask=1; i<mep_caps.mesa_port_cnt; ++i, mask<<=1)    /* Clear any port with active LOS */
            if (los_state[i])   tx_props.tx_info.dst_port_mask &= ~(u64)mask;
        if (tx_props.tx_info.dst_port_mask) { /* Do not transmit if mask is all zero */
            rc = packet_tx(&tx_props) == VTSS_RC_OK;
            if (!rc)
                T_DG(TRACE_GRP_TX_FRAME, "packet_tx() returned error");
            else
                T_DG(TRACE_GRP_TX_FRAME, "instance %u  port_mask 0x%" PRIx64 "  isdx %u  vid %u  qos %u  pcp %u  dp %u  len %zu  maskarade %u  switched %u  oam_type %u  frame %X-%X-%X-%X-%X-%X-%X-%X-%X",
                    instance, tx_props.tx_info.dst_port_mask, tx_props.tx_info.isdx, tx_props.tx_info.tag.vid, tx_props.tx_info.cos, tx_props.tx_info.tag.pcp,
                    tx_props.tx_info.dp, tx_props.packet_info.len, tx_props.tx_info.masquerade_port, tx_props.tx_info.switch_frm, tx_props.tx_info.oam_type,
                    frm[12+0], frm[12+1], frm[12+2], frm[12+3], frm[12+4], frm[12+5], frm[12+6], frm[12+7], frm[12+8]);
        }
    }
    else if (instance_data[instance].domain == VTSS_APPL_MEP_PORT || MPLS_DOMAIN(instance_data[instance].domain)) {
        if (!los_state[port]) {
            tx_props.tx_info.dst_port_mask = VTSS_BIT64(port);
            rc = packet_tx(&tx_props) == VTSS_RC_OK;
            if (!rc)
                T_DG(TRACE_GRP_TX_FRAME, "packet_tx() returned error");
            else
                T_DG(TRACE_GRP_TX_FRAME, "instance %u  port_mask 0x%" PRIx64 "  isdx %u  vid %u  qos %u  pcp %u  dp %u  len %zu  maskarade %u  switched %u  oam_type %u  frame %X-%X-%X-%X-%X-%X-%X-%X-%X",
                    instance, tx_props.tx_info.dst_port_mask, tx_props.tx_info.isdx, tx_props.tx_info.tag.vid, tx_props.tx_info.cos, tx_props.tx_info.tag.pcp,
                    tx_props.tx_info.dp, tx_props.packet_info.len, tx_props.tx_info.masquerade_port, tx_props.tx_info.switch_frm, tx_props.tx_info.oam_type,
                    frm[12+0], frm[12+1], frm[12+2], frm[12+3], frm[12+4], frm[12+5], frm[12+6], frm[12+7], frm[12+8]);
        }
    }

    CRIT_EXIT(crit_p);

    if (tx_timestamp) {
        mesa_os_timestamp_t sw_tstamp;

        MESA_OS_TIMESTAMP(&sw_tstamp);
        vtss_tod_ts_to_time(0, sw_tstamp.hw_cnt, tx_timestamp);
    }

    return (rc != 0);
}

static void timestamp_two_step_cb(void *context, u32 port_no, mesa_ts_timestamp_t *ts)
{
    vtss_mep_supp_ts_done_t *ts_done = (vtss_mep_supp_ts_done_t *)context;
    mesa_timestamp_t        tx_time;

    T_D("timestamp_two_step_cb: port_no: %d", port_no);

    if (!ts_done || !ts_done->cb)    return;

    vtss_tod_ts_to_time(0, ts->ts, &tx_time);
    T_D("timestamp_two_step_cb: Sec:%u  NanoSec:%u", tx_time.seconds, tx_time.nanoseconds);

    ts_done->cb(ts_done, &tx_time);
}

static mesa_rc mep_ts_two_step_cb_reg(packet_tx_props_t *tx_props,
                                u64  port_mask,
                                void *context)
{
    mesa_rc rc;
    mesa_ts_timestamp_alloc_t alloc_parm;

    alloc_parm.port_mask = port_mask;
    alloc_parm.context = context;
    alloc_parm.cb = timestamp_two_step_cb;
    mesa_ts_id_t ts_id;
    rc = mesa_tx_timestamp_idx_alloc(0, &alloc_parm, &ts_id); /* allocate id for transmission*/
    T_D("Timestamp Id (%u)allocated  rc - %d", ts_id.ts_id, rc);
    if (mep_caps.mesa_mep_luton26) {
        tx_props->tx_info.ptp_action    = MESA_PACKET_PTP_ACTION_TWO_STEP; /* twostep action */
        tx_props->tx_info.ptp_id        = ts_id.ts_id;
        tx_props->tx_info.ptp_timestamp = 0; /* subtracted from current time in the tx-fifo */
    }
    return (rc);
}

u32 vtss_mep_supp_packet_tx_two_step_ts(u32                         instance,
                                        vtss_mep_supp_ts_done_t     *ts_done,
                                        mesa_port_list_t            &port,
                                        u32                         len,
                                        unsigned char               *frm)
{
    u32 rc = 1;
    u32  i, port_mask=0, mask;
    u64  port_mask_ts;
    packet_tx_props_t tx_props;
    mesa_rc rc_reg = VTSS_RC_OK;

    if (frm == NULL)     return(1);

    /* This is in order to inter-orperate with vendors that require tagged frames to be transmitted with minimum length of 68 bytes - four bytes will be added by packet_tx() */
    if ((len < 64) && ((frm[12] == 0x81) || (frm[12] == 0x88)))   len = 64;

    CRIT_ENTER(crit_p);
    if (instance_data[instance].domain == VTSS_APPL_MEP_EVC)
    {
        for (i=0, mask = 1 << (0 + VTSS_PORT_NO_START); i<mep_caps.mesa_port_cnt; ++i, mask <<= 1)
            if (port[i])
            {
                port_mask |= mask;
                if ((out_port_conv[i] != i) && (!los_state[out_port_conv[i]]))   port_mask |= 1 << (out_port_conv[i] + VTSS_PORT_NO_START);     /* Transmit packet on protecting port if any */
            }
        if (port_mask)
        {
            packet_tx_props_init(&tx_props);
            tx_props.packet_info.modid     = VTSS_MODULE_ID_MEP;
            tx_props.packet_info.frm       = frm;
            tx_props.packet_info.len       = len;
            tx_props.tx_info.dst_port_mask = port_mask;
            tx_props.packet_info.no_free   = TRUE;
            if (ts_done)
            {
                if (mep_caps.mesa_mep_luton26) {
                    // Set-up TS to call us back when timestamp is available.
                    port_mask_ts = port_mask;
                    rc_reg = mep_ts_two_step_cb_reg(&tx_props, port_mask_ts, ts_done);
                }
            }

            if ((rc_reg == VTSS_RC_OK)) {
                rc = packet_tx(&tx_props) == VTSS_RC_OK;
            }
        }
    }
    if (instance_data[instance].domain == VTSS_APPL_MEP_PORT || MPLS_DOMAIN(instance_data[instance].domain)) {
        for (i=0, mask = 1 << (0 + VTSS_PORT_NO_START); i<mep_caps.mesa_port_cnt; ++i, mask <<= 1)
            if (port[i])     {port_mask |= mask; break;}
        if (port_mask)
        {
            packet_tx_props_init(&tx_props);
            tx_props.packet_info.modid     = VTSS_MODULE_ID_MEP;
            tx_props.packet_info.frm       = frm;
            tx_props.packet_info.len       = len;
            tx_props.tx_info.dst_port_mask = VTSS_BIT64(i);
            tx_props.packet_info.no_free   = TRUE;
            if (ts_done)
            {
                if (mep_caps.mesa_mep_luton26) {
                    // Set-up TS to call us back when timestamp is available.
                    port_mask_ts = 1;
                    port_mask_ts = port_mask_ts << (i + VTSS_PORT_NO_START);
                    T_D("port_mask_ts: " VPRI64u"",port_mask_ts);
                    rc_reg = mep_ts_two_step_cb_reg(&tx_props, port_mask_ts, ts_done);
                }
            }

            if ((rc_reg == VTSS_RC_OK)) {
                rc = packet_tx(&tx_props) == VTSS_RC_OK;
            }
        }
    }
    CRIT_EXIT(crit_p);

    return (rc != 0);
}

u32 vtss_mep_supp_packet_tx_1_two_step_ts(u32                       instance,
                                          vtss_mep_supp_ts_done_t   *ts_done,
                                          u32                       port,
                                          u32                       len,
                                          unsigned char             *frm)
{
    u32 rc = 1;
    u64  port_mask_ts;
    u32  port_mask=0;
    packet_tx_props_t tx_props;
    mesa_rc rc_reg = VTSS_RC_OK;

    if (frm == NULL)     return(1);

    /* This is in order to inter-orperate with vendors that require tagged frames to be transmitted with minimum length of 68 bytes - four bytes will be added by packet_tx() */
    if ((len < 64) && ((frm[12] == 0x81) || (frm[12] == 0x88)))   len = 64;

    CRIT_ENTER(crit_p);
    if (instance_data[instance].domain == VTSS_APPL_MEP_EVC)
    {
        port_mask = (1 << (port + VTSS_PORT_NO_START));
        if ((out_port_conv[port] != port) && (!los_state[out_port_conv[port]]))    port_mask |= (1 << (out_port_conv[port] + VTSS_PORT_NO_START));     /* Transmit packet on protecting port if any */
        packet_tx_props_init(&tx_props);
        tx_props.packet_info.modid     = VTSS_MODULE_ID_MEP;
        tx_props.packet_info.frm       = frm;
        tx_props.packet_info.len       = len;
        tx_props.tx_info.dst_port_mask = port_mask;
        tx_props.packet_info.no_free   = TRUE;
        if (ts_done)
        {
            if (mep_caps.mesa_mep_luton26) {
                // Set-up TS to call us back when timestamp is available.
                port_mask_ts = port_mask;
                rc_reg = mep_ts_two_step_cb_reg(&tx_props, port_mask_ts, ts_done);
            }
        }
        if ((rc_reg == VTSS_RC_OK)) {
            rc = packet_tx(&tx_props) == VTSS_RC_OK;
        }
    }
    if (instance_data[instance].domain == VTSS_APPL_MEP_PORT || MPLS_DOMAIN(instance_data[instance].domain)) {
        packet_tx_props_init(&tx_props);
        tx_props.packet_info.modid     = VTSS_MODULE_ID_MEP;
        tx_props.packet_info.frm       = frm;
        tx_props.packet_info.len       = len;
        tx_props.tx_info.dst_port_mask = VTSS_BIT64(port);
        tx_props.packet_info.no_free   = TRUE;
        if (ts_done)
        {
            if (mep_caps.mesa_mep_luton26) {
                // Set-up TS to call us back when timestamp is available.
                port_mask_ts = 1;
                port_mask_ts = port_mask_ts << (port + VTSS_PORT_NO_START);
                T_D("port_mask_ts: " VPRI64u"",port_mask_ts);
                rc_reg = mep_ts_two_step_cb_reg(&tx_props, port_mask_ts, ts_done);
            }
        }

        if ((rc_reg == VTSS_RC_OK)) {
            rc = packet_tx(&tx_props) == VTSS_RC_OK;
        }
    }
    CRIT_EXIT(crit_p);
    return (rc != 0);
}

static void vtss_mep_ts_one_step_pre_cb_reg(vtss_mep_supp_onestep_extra_t *onestep_extra, u32 hw_time)
{
    vtss_mep_supp_timestamp_get(&onestep_extra->ts, &onestep_extra->tc);
    if (mep_caps.mesa_mep_luton26) {
        /* in L26 the pre callback uses ns value instead of tc */
        onestep_extra->tc = onestep_extra->ts.nanoseconds;
    }
    /* egress latency compensation in SW */
    onestep_extra->sw_egress_delay = observed_egr_lat.mean;
    if (observed_egr_lat.cnt)   /* Latency has been calculated - .mean does no longer contain initial value */
        onestep_extra->sw_egress_delay -= 0x9d00000; /* Subtract this to assure that the delays do not become too small. Due to SW inaccuracy this can lead to negative delays */
}

    /************************  TX TS-FIFO callback *************************/
static void one_step_timestamped(void *context, u32 port_no, mesa_ts_timestamp_t *ts)
{
    vtss_mep_supp_onestep_extra_t *onestep_extra = (vtss_mep_supp_onestep_extra_t *)context;
    u32 now = 0;
    u32 lat;
    mesa_timeinterval_t egr_lat;
    char buf1 [25];

    if (ts->ts_valid) {
        now = ts->ts;
        if (mep_caps.mesa_mep_luton26) {
            mesa_timestamp_t tx_time;
            vtss_tod_ts_to_time(0, ts->ts, &tx_time);
            if (tx_time.nanoseconds >= onestep_extra->tx_time) {
                lat = tx_time.nanoseconds - onestep_extra->tx_time;
            } else {
                lat = tx_time.nanoseconds + 1000000000 - onestep_extra->tx_time;
            }
            egr_lat = ((i64)lat)<<16;
        } else {
            vtss_tod_ts_cnt_sub(&lat, now, onestep_extra->tx_time);
            vtss_tod_ts_cnt_to_timeinterval(&egr_lat, lat);
        }
        T_D("MEP: Onestep data: hw_time %d, tx_time %d, now %d, xxx_lat: %d, lat %d, egr_lat %s ",
             onestep_extra->tc, onestep_extra->tx_time, now,(onestep_extra->tx_time-onestep_extra->tc), lat, vtss_tod_TimeInterval_To_String(&egr_lat,buf1,'.'));

        CRIT_ENTER(crit_p);
        if (observed_egr_lat.cnt == 0) {
            observed_egr_lat.max = egr_lat;
            observed_egr_lat.min = egr_lat;
            observed_egr_lat.mean = egr_lat;
        } else {
            if (observed_egr_lat.max < egr_lat) observed_egr_lat.max = egr_lat;
            if (observed_egr_lat.min > egr_lat) observed_egr_lat.min = egr_lat;
            observed_egr_lat.mean = ((observed_egr_lat.mean<<2) + egr_lat)/5;
        }
        ++observed_egr_lat.cnt;

        //T_D("observed_egr_lat.min: " VPRI64u"", observed_egr_lat.min);
        T_DG(TRACE_GRP_DM, "observed_egr_lat.mean: " VPRI64u"", observed_egr_lat.mean);

        CRIT_EXIT(crit_p);
    } else {
        T_E("TX time stamp invalid  port %u", port_no);
    }

}

static mesa_rc mep_ts_one_step_timed_cb_reg(packet_tx_props_t  *tx_props,
                                            u64                port_mask,
                                            void               *context)
{
    mesa_ts_id_t              ts_id;
    mesa_rc                   rc_reg = VTSS_RC_OK;
    mesa_ts_timestamp_alloc_t alloc_parm;

    if (tx_props->tx_info.switch_frm)    return(VTSS_RC_OK);     /* This is only working for frames not switched - bypassed */
//    if (observed_egr_lat.cnt >= 100)     return(VTSS_RC_OK);     /* The egress latency mean value is not going to change significantly */

    alloc_parm.port_mask = port_mask;
    alloc_parm.context = context;
    alloc_parm.cb = one_step_timestamped;
    rc_reg = mesa_tx_timestamp_idx_alloc(0, &alloc_parm, &ts_id); /* allocate id for transmission*/

    if (mep_caps.mesa_mep_luton26) {
        /*
         * It is triky here. We don't have a real 1-step timestamp for DM in Lu26
         * so we use 2-step mechanism to simulate 1-step here.
         *
         */
        tx_props->tx_info.ptp_id             = ts_id.ts_id;
        tx_props->tx_info.ptp_action         = MESA_PACKET_PTP_ACTION_TWO_STEP; /* twostep action */
        tx_props->tx_info.ptp_timestamp      = 0; /* used for correction field update */
    }

    return rc_reg;
}

u32 vtss_mep_supp_packet_tx_1_one_step_ts(u32                            instance,
                                          u32                            port,
                                          u32                            len,
                                          unsigned char                  *frm,
                                          vtss_mep_supp_tx_frame_info_t  *frame_info,
                                          u32                            hw_time,
                                          vtss_mep_supp_onestep_extra_t  *buf_handle,
                                          vtss_mep_supp_oam_type_t       oam_type)
{
    /*
     *  Both JR and Lu26 don't support real 1-step timestamp. We use
     *  2-step timestamp mechanism to simulate 1-step ts. Two call back functions
     *  are used to reach the purpose. The first one call back function is called
     *  to update the correct time when packet module will leave the s/w part.
     *  The second one is callede with the real h/w sending time, which is a 2-step
     *  mechanism.  In the second one, we can calculate the time offset between
     *  packet to leave s/w part(get in the first callback function) to packet
     *  to really sendout. We input this offset to the first call back function to
     *  make the timestamp more acurate.
     */

    u32 rc = 0;
    u64  port_mask_ts;
    u32  port_mask=0, i, mask;
    packet_tx_props_t tx_props;
    mesa_rc rc_reg = VTSS_RC_OK;

    if (frm == NULL)     return(rc);

    /* This is in order to inter-orperate with vendors that require tagged frames to be transmitted with minimum length of 68 bytes - four bytes will be added by packet_tx() */
    if ((len < 64) && ((frm[12] == 0x81) || (frm[12] == 0x88)))   len = 64;

    CRIT_ENTER(crit_p);
    if ((instance_data[instance].domain == VTSS_APPL_MEP_EVC) || (instance_data[instance].domain == VTSS_APPL_MEP_VLAN))
    {
        port_mask = port_protection_mask_calc(port);
        for (i=0, mask=1; i<mep_caps.mesa_port_cnt; ++i, mask<<=1)    /* Clear any port with active LOS */
            if (los_state[i])   port_mask &= ~mask;

        if (port_mask) { /* Do not transmit if mask is all zero */
            if (oam_type == VTSS_MEP_SUPP_OAM_TYPE_NONE) {  /* Only do CB insert timestamp if NOT VOE based MEP */
                vtss_mep_ts_one_step_pre_cb_reg((vtss_mep_supp_onestep_extra_t *)buf_handle, hw_time);
            }

            packet_tx_props_init(&tx_props);
            tx_props.packet_info.modid       = VTSS_MODULE_ID_MEP;
            tx_props.packet_info.frm         = frm;
            tx_props.packet_info.len         = len;
            tx_props.packet_info.no_free     = TRUE;
            tx_props.tx_info.dst_port_mask   = port_mask;
            tx_props.tx_info.switch_frm      = !frame_info->bypass;
            tx_props.tx_info.cos             = frame_info->qos;
            tx_props.tx_info.dp              = frame_info->dp;
            tx_props.tx_info.tag.pcp         = frame_info->pcp;
            tx_props.tx_info.isdx            = (frame_info->isdx != VTSS_MEP_SUPP_INDX_INVALID) ? frame_info->isdx : VTSS_ISDX_NONE;
            tx_props.tx_info.masquerade_port = frame_info->maskerade_port;
            tx_props.tx_info.tag.vid         = frame_info->vid_inj ? frame_info->vid : 0;
            if (mep_caps.mesa_mep_serval) {
                tx_props.tx_info.oam_type           = oam_type_calc(oam_type);
            }

            if (mep_caps.mesa_mep_luton26) {
                port_mask_ts = port_mask;
                rc_reg = mep_ts_one_step_timed_cb_reg(&tx_props, port_mask_ts, (void *)buf_handle);
            }
            if ((rc_reg == VTSS_RC_OK)) {
                rc = packet_tx(&tx_props) == VTSS_RC_OK;
                if (!rc)
                    T_DG(TRACE_GRP_TX_FRAME, "packet_tx() returned error");
                else
                    T_DG(TRACE_GRP_TX_FRAME, "instance %u  port_mask 0x%" PRIx64 "  isdx %u  vid %u  qos %u  pcp %u  dp %u  len %zu  maskarade %u  switched %u  oam_type %u  frame %X-%X-%X-%X-%X-%X-%X-%X-%X",
                        instance, tx_props.tx_info.dst_port_mask, tx_props.tx_info.isdx, tx_props.tx_info.tag.vid, tx_props.tx_info.cos, tx_props.tx_info.tag.pcp,
                        tx_props.tx_info.dp, tx_props.packet_info.len, tx_props.tx_info.masquerade_port, tx_props.tx_info.switch_frm, tx_props.tx_info.oam_type,
                        frm[12+0], frm[12+1], frm[12+2], frm[12+3], frm[12+4], frm[12+5], frm[12+6], frm[12+7], frm[12+8]);
            }
        }
    }
    if (instance_data[instance].domain == VTSS_APPL_MEP_PORT || MPLS_DOMAIN(instance_data[instance].domain)) {
        if (!los_state[port]) { /* Do not transmit if LOS active on port */
            if (oam_type == VTSS_MEP_SUPP_OAM_TYPE_NONE) {
                /* Only do CB insert timestamp if NOT VOE based MEP */
                vtss_mep_ts_one_step_pre_cb_reg((vtss_mep_supp_onestep_extra_t *)buf_handle, hw_time);
            }

            packet_tx_props_init(&tx_props);
            tx_props.packet_info.modid       = VTSS_MODULE_ID_MEP;
            tx_props.packet_info.frm         = frm;
            tx_props.packet_info.len         = len;
            tx_props.packet_info.no_free     = TRUE;
            tx_props.tx_info.dst_port_mask   = VTSS_BIT64(port);
            tx_props.tx_info.switch_frm      = !frame_info->bypass;
            tx_props.tx_info.cos             = frame_info->qos;
            tx_props.tx_info.dp              = frame_info->dp;
            tx_props.tx_info.tag.pcp         = frame_info->pcp;
            tx_props.tx_info.isdx            = (frame_info->isdx != VTSS_MEP_SUPP_INDX_INVALID) ? frame_info->isdx : VTSS_ISDX_NONE;
            tx_props.tx_info.masquerade_port = frame_info->maskerade_port;
            if (mep_caps.mesa_mep_serval) {
                tx_props.tx_info.oam_type           = oam_type_calc(oam_type);
            }

            if (mep_caps.mesa_mep_luton26) {
                port_mask_ts = 1;
                port_mask_ts = port_mask_ts << (port + VTSS_PORT_NO_START);
                rc_reg = mep_ts_one_step_timed_cb_reg(&tx_props, port_mask_ts, (void *)buf_handle);
            }
            if ((rc_reg == VTSS_RC_OK)) {
                rc = packet_tx(&tx_props) == VTSS_RC_OK;
                if (!rc)
                    T_DG(TRACE_GRP_TX_FRAME, "packet_tx() returned error");
                else
                    T_DG(TRACE_GRP_TX_FRAME, "instance %u  port_mask 0x%" PRIx64 "  isdx %u  vid %u  qos %u  pcp %u  dp %u  len %zu  maskarade %u  switched %u  oam_type %u  frame %X-%X-%X-%X-%X-%X-%X-%X-%X",
                        instance, tx_props.tx_info.dst_port_mask, tx_props.tx_info.isdx, tx_props.tx_info.tag.vid, tx_props.tx_info.cos, tx_props.tx_info.tag.pcp,
                        tx_props.tx_info.dp, tx_props.packet_info.len, tx_props.tx_info.masquerade_port, tx_props.tx_info.switch_frm, tx_props.tx_info.oam_type,
                        frm[12+0], frm[12+1], frm[12+2], frm[12+3], frm[12+4], frm[12+5], frm[12+6], frm[12+7], frm[12+8]);
            }
        }
    }
    CRIT_EXIT(crit_p);
    return (rc != 0);
}

u32 vtss_mep_supp_packet_tx_one_step_ts(u32                           instance,
                                        mesa_port_list_t              &port,
                                        u32                           len,
                                        unsigned char                 *frm,
                                        vtss_mep_supp_tx_frame_info_t *frame_info,
                                        u32                           hw_time,
                                        vtss_mep_supp_onestep_extra_t *buf_handle,
                                        vtss_mep_supp_oam_type_t      oam_type)
{
    /*
     *  Both JR and Lu26 don't support real 1-step timestamp. We use
     *  2-step timestamp mechanism to simulate 1-step ts. Two call back functions
     *  are used to reach the purpose. The first one call back function is called
     *  to update the correct time when packet module will leave the s/w part.
     *  The second one is callede with the real h/w sending time, which is a 2-step
     *  mechanism.  In the second one, we can calculate the time offset between
     *  packet to leave s/w part(get in the first callback function) to packet
     *  to really sendout. We input this offset to the first call back function to
     *  make the timestamp more acurate.
     */

    u32 rc = 0;
    u64  port_mask_ts;
    u32  i, port_mask=0, mask;
    packet_tx_props_t tx_props;
    mesa_rc rc_reg = VTSS_RC_OK;
    mesa_packet_port_info_t   info;
    CapArray<mesa_packet_port_filter_t, MESA_CAP_PORT_CNT> filter;
    BOOL port_down_mep, bypass_on_not_res;

    if (frm == NULL)     return(rc);

    /* This is in order to inter-orperate with vendors that require tagged frames to be transmitted with minimum length of 68 bytes - four bytes will be added by packet_tx() */
    if ((len < 64) && ((frm[12] == 0x81) || (frm[12] == 0x88)))   len = 64;

    if (mesa_packet_port_info_init(&info) != VTSS_RC_OK)
        return(rc);

    info.vid = frame_info->vid;
    if (mesa_packet_port_filter_get(NULL, &info, filter.size(), filter.data()) != VTSS_RC_OK)
        return(rc);

    CRIT_ENTER(crit_p);
    port_down_mep = ((instance_data[instance].domain == VTSS_APPL_MEP_PORT || MPLS_DOMAIN(instance_data[instance].domain)) && port[instance_data[instance].res_port]);
    bypass_on_not_res = (frame_info->bypass && !port[instance_data[instance].res_port]);

    for (i=0, mask=1; i<mep_caps.mesa_port_cnt; ++i, mask<<=1)    /* All ports are checked to be valid */
        if (port[i] && ((i == voe_up_mep_loop_port) || !frame_info->bypass ||              /* Always legal to transmit on loop port or through the analyser */
            (!los_state[i] &&                                                              /* Port with active los is not in the mask */
             ((filter[i].filter != MESA_PACKET_FILTER_DISCARD) || !bypass_on_not_res) &&   /* Port must not be filtering if injection directly on a port that is not the residence port */
             (out_port_tx[i] || port_down_mep))))                                          /* The port must not be blocked by port protection unless it is a port Down MEP */
            port_mask |= mask;

    if (port_mask) {    /* Do not transmit if mask is all zero */
        if ((instance_data[instance].domain == VTSS_APPL_MEP_EVC) || (instance_data[instance].domain == VTSS_APPL_MEP_VLAN))
        {
            if (oam_type == VTSS_MEP_SUPP_OAM_TYPE_NONE) {  /* Only do CB insert timestamp if NOT VOE based MEP */
                vtss_mep_ts_one_step_pre_cb_reg((vtss_mep_supp_onestep_extra_t *)buf_handle, hw_time);
            }

            packet_tx_props_init(&tx_props);
            tx_props.packet_info.modid       = VTSS_MODULE_ID_MEP;
            tx_props.packet_info.frm         = frm;
            tx_props.packet_info.len         = len;
            tx_props.packet_info.no_free     = TRUE;
            tx_props.tx_info.dst_port_mask   = port_mask;
            tx_props.tx_info.switch_frm      = !frame_info->bypass;
            tx_props.tx_info.cos             = frame_info->qos;
            tx_props.tx_info.dp              = frame_info->dp;
            tx_props.tx_info.tag.pcp         = frame_info->pcp;
            tx_props.tx_info.isdx            = (frame_info->isdx != VTSS_MEP_SUPP_INDX_INVALID) ? frame_info->isdx : VTSS_ISDX_NONE;
            tx_props.tx_info.masquerade_port = frame_info->maskerade_port;
            tx_props.tx_info.tag.vid         = frame_info->vid_inj ? frame_info->vid : 0;
            if (mep_caps.mesa_mep_serval) {
                tx_props.tx_info.oam_type           = oam_type_calc(oam_type);
            }

            if (mep_caps.mesa_mep_luton26) {
                port_mask_ts = port_mask;
                rc_reg = mep_ts_one_step_timed_cb_reg(&tx_props, port_mask_ts, (void *)buf_handle);
            }
            if ((rc_reg == VTSS_RC_OK)) {
                rc = packet_tx(&tx_props) == VTSS_RC_OK;
                if (!rc)
                    T_DG(TRACE_GRP_TX_FRAME, "packet_tx() returned error");
                else
                    T_DG(TRACE_GRP_TX_FRAME, "instance %u  port_mask 0x%" PRIx64 "  isdx %u  vid %u  qos %u  pcp %u  dp %u  len %zu  maskarade %u  switched %u  oam_type %u  frame %X-%X-%X-%X-%X-%X-%X-%X-%X",
                        instance, tx_props.tx_info.dst_port_mask, tx_props.tx_info.isdx, tx_props.tx_info.tag.vid, tx_props.tx_info.cos, tx_props.tx_info.tag.pcp,
                        tx_props.tx_info.dp, tx_props.packet_info.len, tx_props.tx_info.masquerade_port, tx_props.tx_info.switch_frm, tx_props.tx_info.oam_type,
                        frm[12+0], frm[12+1], frm[12+2], frm[12+3], frm[12+4], frm[12+5], frm[12+6], frm[12+7], frm[12+8]);
            }
        }
        if (instance_data[instance].domain == VTSS_APPL_MEP_PORT || MPLS_DOMAIN(instance_data[instance].domain)) {
            if (oam_type == VTSS_MEP_SUPP_OAM_TYPE_NONE) {
                /* Only do CB insert timestamp if NOT VOE based MEP */
                vtss_mep_ts_one_step_pre_cb_reg(buf_handle, hw_time);
            }

            packet_tx_props_init(&tx_props);
            tx_props.packet_info.modid       = VTSS_MODULE_ID_MEP;
            tx_props.packet_info.frm         = frm;
            tx_props.packet_info.len         = len;
            tx_props.packet_info.no_free     = TRUE;
            tx_props.tx_info.dst_port_mask   = port_mask;
            tx_props.tx_info.switch_frm      = !frame_info->bypass;
            tx_props.tx_info.cos             = frame_info->qos;
            tx_props.tx_info.dp              = frame_info->dp;
            tx_props.tx_info.tag.pcp         = frame_info->pcp;
            tx_props.tx_info.isdx            = (frame_info->isdx != VTSS_MEP_SUPP_INDX_INVALID) ? frame_info->isdx : VTSS_ISDX_NONE;
            tx_props.tx_info.masquerade_port = frame_info->maskerade_port;
            if (mep_caps.mesa_mep_serval) {
                tx_props.tx_info.oam_type           = oam_type_calc(oam_type);
            }

            if (mep_caps.mesa_mep_luton26) {
                port_mask_ts = 1;
                port_mask_ts = port_mask_ts << (i + VTSS_PORT_NO_START);
                //T_D("port_mask_ts: " VPRI64u"",port_mask_ts);
                rc_reg = mep_ts_one_step_timed_cb_reg(&tx_props, port_mask_ts, (void *)buf_handle);
            }
            if ((rc_reg == VTSS_RC_OK)) {
                rc = packet_tx(&tx_props) == VTSS_RC_OK;
                if (!rc)
                    T_DG(TRACE_GRP_TX_FRAME, "packet_tx() returned error");
                else
                    T_DG(TRACE_GRP_TX_FRAME, "instance %u  port_mask 0x%" PRIx64 "  isdx %u  vid %u  qos %u  pcp %u  dp %u  len %zu  maskarade %u  switched %u  oam_type %u  frame %X-%X-%X-%X-%X-%X-%X-%X-%X",
                        instance, tx_props.tx_info.dst_port_mask, tx_props.tx_info.isdx, tx_props.tx_info.tag.vid, tx_props.tx_info.cos, tx_props.tx_info.tag.pcp,
                        tx_props.tx_info.dp, tx_props.packet_info.len, tx_props.tx_info.masquerade_port, tx_props.tx_info.switch_frm, tx_props.tx_info.oam_type,
                        frm[12+0], frm[12+1], frm[12+2], frm[12+3], frm[12+4], frm[12+5], frm[12+6], frm[12+7], frm[12+8]);
            }
        }
    }

    CRIT_EXIT(crit_p);

    return (rc != 0);
}

void vtss_mep_supp_timestamp_get(mesa_timestamp_t *timestamp, u32 *tc)
{
#if 1
    vtss_tod_gettimeofday(0, timestamp, tc);
#else
    vtss_tod_ts_to_time(0, vtss_tod_get_ts_cnt(), (mesa_timestamp_t *)timestamp);
#endif
}

BOOL vtss_mep_supp_check_hw_timestamp(void)
{
    return TRUE;
}

BOOL vtss_mep_supp_check_phy_timestamp(u32 port, u8 *mac)
{
    if (mep_caps.mesa_phy_ts) {
        /* Only unicast is supported */
        if ((mac[0] & 0x1) == 0) {
            return (mep_phy_ts_port[port].phy_ts_port);
        } else {
            return FALSE;
        }
    }
    return FALSE;
}


/* The unit of the return value is nanosecond or microsecond */
i32 vtss_mep_supp_delay_calc(const mesa_timestamp_t *x,
                             const mesa_timestamp_t *y,
                             const BOOL             is_ns)
{
    i64          diff=0;

    vtss_tod_sub_TimeStamps(&diff, x, y);

    if (diff < 0) {
        if(diff > -100) {
            diff = -diff;
        }

        T_DG(TRACE_GRP_DM, "(x - y) < 0 - Operator x.s %u-%u x.ns %u y.s %u-%u y.ns %u", x->sec_msb, x->seconds, x->nanoseconds, y->sec_msb, y->seconds, y->nanoseconds);
        T_DG(TRACE_GRP_DM, "diff %" PRIi64 , diff);
    }

    if (is_ns)
    {
        return diff;
    }
    else
    {
        diff = diff / 1000;
        if ((diff % 1000) > 500)
            diff++; /*round up*/
        return diff;
    }
}

BOOL vtss_mep_supp_check_forwarding(u32 i_port,  u32 *f_port,  u8 *mac,  u32 vid)
{
    u32                     i, dest;
    mesa_vid_mac_t          vid_mac;
    mesa_mac_table_entry_t  entry;

    memset(&entry, 0, sizeof(entry));
    vid_mac.vid = vid;
    memcpy(vid_mac.mac.addr, mac, sizeof(vid_mac.mac.addr));

    if (mesa_mac_table_get(NULL,  &vid_mac,  &entry) != VTSS_RC_OK)   return(FALSE);

    /* 'mac' found in MAC table */
    if (entry.locked)     /* It must be an dynamic entry */
        if (mesa_mac_table_get_next(NULL,  &vid_mac,  &entry) != VTSS_RC_OK)   return(FALSE);

    if (entry.locked)   return(FALSE);
    /* 'mac' found in MAC table and it is dynamic */

    /* Find and count destination ports */
    for (i=0, dest=0; i<mep_caps.mesa_port_cnt; ++i)
    {
        if ((entry.destination[i]) && (in_port_conv[i] != 0xFFFF))
        {
            dest++;
            *f_port = i;
        }
    }

    if (dest != 1)            return(FALSE);    /* Only forward if one destination port is found */
    if (*f_port == i_port)    return(FALSE);    /* Only forward if destination port is different from ingress port */

    return(TRUE);
}


/****************************************************************************/
/*  MEP Initialize module                                                   */
/****************************************************************************/



















































































































static void port_change_callback(mesa_port_no_t port_no, port_info_t *info)
{
    T_D("Port %u   Los %u\n", port_no, !info->link);

    los_state_set(port_no, (info->link) ? FALSE : TRUE);
}

static void port_shutdown_callback(mesa_port_no_t port_no)
{
    T_D("Port %u\n", port_no);

    los_state_set(port_no, TRUE);
}

typedef struct
{
    vtss_appl_vlan_port_type_t port_type;
    mesa_vid_t                 pvid;
} mep_vlan_conf_t;

static CapArray<mep_vlan_conf_t, MESA_CAP_PORT_CNT> vlan_port_conf;

static void vlan_port_change_callback(vtss_isid_t isid, mesa_port_no_t port_no, const vtss_appl_vlan_port_detailed_conf_t *new_conf)
{
    u32 i;
    u32         base_rc;
    mep_conf_t  mep_conf;
    CRIT_ENTER(crit_p);

    T_D("port_no %u  new port_type %u  old port_type %u", port_no, new_conf->port_type, vlan_port_conf[port_no].port_type);

    if (vlan_port_conf[port_no].port_type != new_conf->port_type) {
        vlan_port_conf[port_no].port_type = new_conf->port_type;

        for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
        {
            if (instance_data[i].enable)
            {
                if (instance_data[i].port[port_no]) {
                    CRIT_EXIT(crit_p);
                    /* This is some change in vlan port configuration - re-create MEP/MIP bypassing database */
                    (void)vtss_mep_mgmt_change_set(i, FALSE, FALSE);
                    CRIT_ENTER(crit_p);
                }
                else if (mep_caps.mesa_mep_serval) {
                    if ((instance_data[i].res_port == port_no) && ((instance_data[i].domain == VTSS_APPL_MEP_EVC) || (instance_data[i].domain == VTSS_APPL_MEP_VLAN))) {
                        CRIT_EXIT(crit_p);
                        base_rc = vtss_mep_mgmt_conf_get(i, &mep_conf);
                        if ((mep_conf.mode == VTSS_APPL_MEP_MEP) && (mep_conf.direction == VTSS_APPL_MEP_UP))  /* This is a Up-MEP on this port */
                            if (base_rc == VTSS_MEP_RC_OK)    (void)vtss_mep_mgmt_change_set(i, FALSE, FALSE);
                        CRIT_ENTER(crit_p);
                    }
                }
            }
        }
    }

    if (vlan_port_conf[port_no].pvid != new_conf->pvid) {
        vlan_port_conf[port_no].pvid = new_conf->pvid;

        for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
        {
            if (instance_data[i].enable && (instance_data[i].domain == VTSS_APPL_MEP_PORT || MPLS_DOMAIN(instance_data[i].domain)))
            {
                if (instance_data[i].port[port_no]) {
                    CRIT_EXIT(crit_p);
                    /* This is some change in vlan port configuration - re-create MEP/MIP bypassing database */
                    (void)vtss_mep_mgmt_change_set(i, FALSE, FALSE);
                    CRIT_ENTER(crit_p);
                }
            }
        }
    }
    CRIT_EXIT(crit_p);
}

static void vlan_custom_ethertype_callback(void)
{
    u32                   i, j;
    mesa_vlan_port_conf_t vlan_conf;
    mep_conf_t mep_conf;

    T_D("Enter");

    CRIT_ENTER(crit_p);
    for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
    {
        if (instance_data[i].enable)
        {
            for (j=0; j<mep_caps.mesa_port_cnt; ++j) { /* Check ports to see if they are 'Custom S Port' */
                if (instance_data[i].port[j]) {
                    if (mesa_vlan_port_conf_get(NULL, j, &vlan_conf) != VTSS_RC_OK) /* Get the VLAN port type */
                        continue;
                    if (vlan_conf.port_type != MESA_VLAN_PORT_TYPE_S_CUSTOM)   /* If the port is Custom S the the MEP must be reconfigured */
                        continue;
                    CRIT_EXIT(crit_p);  /* Reconfigure */
                    (void)vtss_mep_mgmt_change_set(i, FALSE, FALSE);
                    CRIT_ENTER(crit_p);
                }
                else if (mep_caps.mesa_mep_serval) {
                    if ((instance_data[i].res_port == j) && ((instance_data[i].domain == VTSS_APPL_MEP_EVC) || (instance_data[i].domain == VTSS_APPL_MEP_VLAN))) {
                        CRIT_EXIT(crit_p);
                        if (vtss_mep_mgmt_conf_get(i, &mep_conf) == VTSS_MEP_RC_OK) {  /* Get MEP instance configuration */
                            if ((mep_conf.mode == VTSS_APPL_MEP_MEP) && (mep_conf.direction == VTSS_APPL_MEP_UP)) {  /* This is a Up-MEP on this port */
                                if (mesa_vlan_port_conf_get(NULL, j, &vlan_conf) == VTSS_RC_OK) {   /* Get the VLAN port type */
                                    if (vlan_conf.port_type == MESA_VLAN_PORT_TYPE_S_CUSTOM)   /* If the port is Custom S the the MEP must be reconfigured */
                                        (void)vtss_mep_mgmt_change_set(i, FALSE, FALSE);
                                }
                            }
                        }
                        CRIT_ENTER(crit_p);
                    }
                }
            }
        }
    }
    CRIT_EXIT(crit_p);
}

static void vlan_membership_change_callback(vtss_isid_t isid, mesa_vid_t vid, vlan_membership_change_t *changes)
{
    u32                       i, j, res_port;
    BOOL                      nni_found=TRUE, tlv_change=FALSE;
    mep_conf_t                conf;
    vtss_appl_mep_aps_conf_t  aps_conf;
    vlan_ports_t              resulting;

    T_D("vid %u static_exists %u", vid, changes->static_vlan_exists);

    // Resulting configuration for static user must be computed based on both the static and the forbidden VLAN users' configuration.
    for (i = 0; i < mep_caps.mesa_port_cnt; i++) {
        // Include static ports that are not forbidden.
        if (changes->static_ports.ports[i] & !changes->forbidden_ports.ports[i]) {
            resulting.ports[i] = TRUE;
        }
    }

    CRIT_ENTER(crit_p);

    T_D("evc_vlan_vid[vid] %X", evc_vlan_vid[vid]);

    if (evc_vlan_vid[vid] >= mep_caps.mesa_evc_evc_cnt) {
        // The VID is registered as a "VLAN" VID. This has implications when frames are received through a port protection
        // The static user may or may not have members set, independent of whether the VLAN has been created
        // by him. Therefore, it's not good enough to look at changes->static_ports to check whether it has bits set or not.
        evc_vlan_vid[vid] = changes->static_vlan_exists ? mep_caps.mesa_evc_evc_cnt : 0xFFFFFFFF;
    }

    for (i = 0; i < VTSS_APPL_MEP_INSTANCE_MAX; i++) {
        if ((instance_data[i].enable) && (instance_data[i].domain == VTSS_APPL_MEP_VLAN) && (instance_data[i].flow == vid)) {
            /* This is a VLAN MEP in this VID */
            nni_found = TRUE;
            res_port = instance_data[i].res_port;

            CRIT_EXIT(crit_p);
            if (vtss_mep_mgmt_conf_get(i, &conf) == VTSS_RC_OK) {
                if (conf.direction == VTSS_APPL_MEP_UP) {   /* An Up-MEP requires a "NNI" port*/
                    nni_found = FALSE;
                    for (j=0; j<mep_caps.mesa_port_cnt; ++j) {
                        if ((j != res_port) && resulting.ports[j]) {
                            nni_found = TRUE;     /* 'NNI port was found */
                            break;
                        }
                    }

                    if (nni_found && resulting.ports[res_port]) {
                        /* This MEP is in this VLAN */
                        if (vtss_mep_mgmt_change_set(i, FALSE, FALSE) != VTSS_RC_OK) {
                            T_D("Error during call to MEP base");
                        }
                    } else {
                        /* This MEP is no longer on a VLAN port - delete MEP in database */
                        conf.enable = FALSE;
                        if (mep_instance_conf_set(i, &conf) != VTSS_RC_OK) {
                            T_D("Error during call to MEP base");
                        }
                    }
                }
            }
            CRIT_ENTER(crit_p);
        }

        if ((instance_data[i].enable) && (instance_data[i].domain == VTSS_APPL_MEP_PORT) && (instance_data[i].vid == vid)) {
            /* This is a Port MEP in this VID - check for any active RAPS */
            CRIT_EXIT(crit_p);
            if (vtss_mep_mgmt_aps_conf_get(i, &aps_conf) == VTSS_RC_OK) {
                if (aps_conf.enable && (aps_conf.type == VTSS_APPL_MEP_R_APS)) {   /* RAPS is enabled */
                    /* The change in VLAN configuration might have implications for the RAPS configuration */
                    if (vtss_mep_mgmt_change_set(i, FALSE, FALSE) != VTSS_RC_OK) {
                        T_D("Error during call to MEP base");
                    }
                }
            }
            CRIT_ENTER(crit_p);
        }

        /* Find if any MEP instance need to change PS TLV based on VLAN change */
        if ((instance_data[i].enable) && (instance_data[i].flow == vid) && (changes->changed_ports.ports[instance_data[i].res_port]) &&
            (instance_data[i].port[instance_data[i].res_port]))  /* Check if this is Down-MEP resident on a changed port in this VID */
            tlv_change = TRUE;
    }

    if (tlv_change) {
        CRIT_EXIT(crit_p);
        (void)vtss_mep_mgmt_ps_tlv_change_set();
        CRIT_ENTER(crit_p);
    }

    CRIT_EXIT(crit_p);
}

typedef struct
{
    vtss_appl_qos_tag_cos_entry_t cos_tag_map[VTSS_PCP_ARRAY_SIZE][VTSS_DEI_ARRAY_SIZE]; /* Ingress mapping for tagged frames from PCP,DEI to COS,DPL */
    vtss_appl_qos_cos_tag_entry_t tag_cos_map[VTSS_APPL_QOS_PORT_PRIO_CNT][2];           /* Egress mapping from CoS,DPL to PCP,DEI */
} mep_qos_conf_t;
static CapArray<mep_qos_conf_t, MESA_CAP_PORT_CNT> qos_port_conf;

static void qos_port_change_callback(const vtss_isid_t isid, const mesa_port_no_t iport, const vtss_appl_qos_port_conf_t *const conf)
{
    u32                  i, base_rc;
    mep_conf_t           mep_conf;

    T_D("iport %u", iport);

    CRIT_ENTER(crit_p);
    if (memcmp(conf->cos_tag_map, qos_port_conf[iport].cos_tag_map, sizeof(conf->cos_tag_map))) {

        memcpy(qos_port_conf[iport].cos_tag_map, conf->cos_tag_map, sizeof(qos_port_conf[iport].cos_tag_map));

        for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
        {
            if (instance_data[i].enable && (instance_data[i].res_port == iport) && (instance_data[i].domain == VTSS_APPL_MEP_EVC))
            {
                CRIT_EXIT(crit_p);
                base_rc = vtss_mep_mgmt_conf_get(i, &mep_conf);
                if ((mep_conf.mode == VTSS_APPL_MEP_MEP) && (mep_conf.direction == VTSS_APPL_MEP_UP))  /* This is a Up-MEP on this port */
                    if (base_rc == VTSS_MEP_RC_OK)    (void)vtss_mep_mgmt_change_set(i, FALSE, FALSE);
                CRIT_ENTER(crit_p);
            }
        }
    }

    if (memcmp(conf->tag_cos_map, qos_port_conf[iport].tag_cos_map, sizeof(conf->tag_cos_map))) {

        memcpy(qos_port_conf[iport].tag_cos_map, conf->tag_cos_map, sizeof(qos_port_conf[iport].tag_cos_map));

        for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
        {
            if (instance_data[i].enable && (instance_data[i].port[iport]) && (instance_data[i].domain == VTSS_APPL_MEP_EVC))
            {
                CRIT_EXIT(crit_p);
                base_rc = vtss_mep_mgmt_conf_get(i, &mep_conf);
                if (!mep_conf.voe && (mep_conf.mode == VTSS_APPL_MEP_MEP) && (mep_conf.direction == VTSS_APPL_MEP_DOWN))   /* This is a SW EVC Down-MEP on this port */
                    if (base_rc == VTSS_MEP_RC_OK)    (void)vtss_mep_mgmt_change_set(i, FALSE, FALSE);
                CRIT_ENTER(crit_p);
            }
        }
    }

    CRIT_EXIT(crit_p);
}

static void mstp_change_callback(vtss_common_port_t l2port, vtss_common_stpstate_t new_state)
{
    vtss_flag_setbits(&run_wait_flag, FLAG_MSTP);
}

static void link_interrupt_function(meba_event_t        source_id,
                                    u32                            instance_id)
{
    mesa_rc rc;

//printf("link_interrupt_function  source_id %u  instance_id %u\n", source_id, instance_id);
    rc = vtss_interrupt_source_hook_set(VTSS_MODULE_ID_MEP,
                                        link_interrupt_function,
                                        source_id,
                                        INTERRUPT_PRIORITY_PROTECT);
    if (rc != VTSS_RC_OK)       T_D("Error during interrupt hook");

    los_state_set(instance_id, TRUE);
}

#define string_to_var(string)  (((string)[0]<<24) | ((string)[1]<<16) | ((string)[2]<<8) | (string)[3])

static BOOL rx_frame(void *contxt, const u8 *const frame, const mesa_packet_rx_info_t *const rx_info)
{
/* This is only for L28 - frame is received with stripped TAG */
    u32                        port_conv;
    u32                        tl_idx;
    mesa_timestamp_t           rx_time;
    vtss_mep_supp_frame_info_t frame_info;
    mesa_timestamp_t           timee;
    mesa_ts_id_t ts_id;
    mesa_ts_timestamp_t ts;
    u32 tc;
    u32                 rx_count=0;
    BOOL timestamp_ok;
    mesa_packet_filter_t       filter;
    mesa_packet_frame_info_t   info;
    u32 voe_idx;
    mesa_oam_ts_id_t          oam_ts_id;
    mesa_oam_ts_timestamp_t   oam_ts;
    BOOL is_mpls = FALSE, is_mpls_oam2 = FALSE;
    u16 eth_offs = 0;



    u32 rc;
    BOOL ts_valid = FALSE;

    port_conv = rx_info->port_no;
    if ((rx_info->tag_type == MESA_TAG_TYPE_S_TAGGED) || (rx_info->tag_type == MESA_TAG_TYPE_C_TAGGED) || (rx_info->tag_type == MESA_TAG_TYPE_S_CUSTOM_TAGGED))
    {   /* Received with a C or S TAG. Customer tagged will be treated as S-tagged */















    }

    tl_idx = 12;


    /* If there is a not stripped tag in the frame we skip it. Also the Ether type must be 0x8902 or 0x8847: */
    if ((frame[tl_idx] != 0x89) || (frame[tl_idx+1] != 0x02))
        tl_idx += 4;
    if ((frame[tl_idx] != 0x89) || (frame[tl_idx+1] != 0x02))  return(FALSE);





















































    T_DG(TRACE_GRP_RX_FRAME, "Frame:  port %u  vid %u  isdx %u  tagged %u  qos %u  pcp %u  dei %u  len %u  count %u  smac %X-%X  level %u  pdu %u  flags %X  mep_id %u",
    rx_info->port_no, rx_info->tag.vid, rx_info->isdx, (rx_info->tag_type != MESA_TAG_TYPE_UNTAGGED) ? TRUE : FALSE, rx_info->cos, rx_info->tag.pcp, rx_info->tag.dei, rx_info->length, rx_count, frame[10], frame[11], frame[tl_idx+2]>>5, frame[tl_idx+3], frame[tl_idx+4], frame[tl_idx+11]);

    memset(&timee, 0, sizeof(timee));
    memset(&rx_time, 0, sizeof(rx_time));
    memset(&frame_info, 0, sizeof(frame_info));

    if (mep_caps.mesa_mep_luton26) {
        memset(&ts, 0, sizeof(ts));  /* To satisfy lint */
        ts_id.ts_id = rx_info->tstamp_id;
        timestamp_ok = rx_info->tstamp_id_decoded;
        if (timestamp_ok) {
            T_DG(TRACE_GRP_DM, "PDU %u  Timestamp_id %d, Timestamp_decoded %d", frame[tl_idx+3], ts_id.ts_id, timestamp_ok);
            if (((rc = mesa_rx_timestamp_get(0,&ts_id,&ts)) == VTSS_RC_OK) && ts.ts_valid) { /* On Caracal all OAM PDU are timestamped as they are hitting the same IS2 rule - no special rule for DM */
                T_DG(TRACE_GRP_DM, "valid timestamp detected for id: %d, ts_valid:%d",ts_id.ts_id, ts.ts_valid);
                ts_valid = TRUE;
            } else {
                /* This is not necessarily a problem as tstamp_id_decoded on Caracal is set if the frame is redirected through hit in IS2. */
                /* In IFH it is not possible to see if tstamp_id is valid or not */
                T_NG(TRACE_GRP_DM, "No valid timestamp detected for id: %d, ts_valid:%d  rc %u  PDU %u",ts_id.ts_id, ts.ts_valid, rc, frame[tl_idx+3]);
            }
        }
    }

    //T_D("rx_frame  tag_type %u\n", rx_info->tag_type);
    if (!is_mpls_oam2 && ((frame[tl_idx+3] == 45) || (frame[tl_idx+3] == 46) || (frame[tl_idx+3] == 47)))
    { /* Only get timestamp if it is a delay measurement frame */
        if (mep_caps.mesa_mep_serval) {
            if (rx_info->hw_tstamp_decoded) { /* This is timestamp captured by hit in S2 - PTP two-step */
                T_DG(TRACE_GRP_DM, "hw_tstamp_decoded  rx_info->hw_tstamp %u", rx_info->hw_tstamp);
                vtss_tod_ts_to_time(0, rx_info->hw_tstamp, &timee);
            }
            else {
                if (rx_info->oam_info_decoded) { /* VOE - TS is in FIFO or in .oam_info */
                    if (!vtss_mep_supp_voe_up(rx_info->isdx, rx_info->tag.vid, frame[tl_idx+2]>>5, &voe_idx)) { /* This is not an UP-MEP. The .oam_info member contains a timestamp */
                        vtss_tod_ts_to_time(0, rx_info->oam_info, &timee);
                    }
                    else { /* This is an UP-MEP. The .oam_info member contains a timestamp FIFO sequence number */
                        u32 rc;
                        if ((rc = mesa_tx_timestamp_update(0)) == VTSS_RC_OK) { /* Getting the timestamp failed - it might be that the timestamp FIFO needs to be updated */
                            T_DG(TRACE_GRP_DM, "mesa_tx_timestamp_update() failed  rc %s",error_txt(rc));
                        }
                        oam_ts_id.voe_id = voe_idx;
                        oam_ts_id.voe_sq = rx_info->oam_info>>27;
                        T_DG(TRACE_GRP_DM, "UP-MEP voe_id %u, voe_sq %u", oam_ts_id.voe_id, oam_ts_id.voe_sq);
                        oam_ts.ts_valid = FALSE;
                        (void)mesa_oam_timestamp_get(NULL, &oam_ts_id, &oam_ts);

                        if (!oam_ts.ts_valid) {
                            T_WG(TRACE_GRP_DM, "UP-MEP Time stamp invalid  voe_id %u  voe_sq %u", oam_ts_id.voe_id, oam_ts_id.voe_sq);
                            return(TRUE);   /* Discard frame if no valid timestamp was found */
                        }
                        vtss_tod_ts_to_time(0, oam_ts.ts, &timee);
                    }
                }
            }
        }
        if (mep_caps.mesa_mep_luton26) {
            if (ts_valid) { /* The time stamp was previously found valid */
                vtss_tod_ts_to_time(0, ts.ts, &timee);
            } else {
                //Use SW timestamp which is ns insted of the internal time counter
                vtss_tod_gettimeofday(0, &timee, &tc);
            }
        }
        rx_time = timee;
        T_DG(TRACE_GRP_DM, "Frame rx PDU %u  time  sec %u  nsec %u", frame[tl_idx+3], rx_time.seconds, rx_time.nanoseconds);

        if (mep_caps.mesa_phy_ts) {
            /* According to the standard, rx timestamp for 1dm,dmm and dmr must be 0.
             * If it is not zero, use it as the rx timestamp. Only support ucast
             * solution at this moment so it is the only way to check if PHY
             * timestamp is used. For multicast DM packets, MAC timestamp is
             * still used
             */
            CRIT_ENTER(crit_p);
            if (mep_phy_ts_port[rx_info->port_no].phy_ts_port == TRUE) {
                u8  ts_offset;
                u32 sec, nanosec;

                if ((frame[tl_idx+3] == 45) || (frame[tl_idx+3] == 47)) {
                    /* 1DM and DMM(opcode), the rx time offset is 11 bytes after opcode */
                    ts_offset = tl_idx + 3 + 11;
                } else {
                    /* DMR(opcode), the rx time offset is 27 bytes after opcode */
                    ts_offset = tl_idx + 3 + 27;
                }
                sec = vtss_tod_unpack32(&frame[ts_offset]); //tshw
                nanosec = vtss_tod_unpack32(&frame[ts_offset + 4]); //tshw

                if (sec != 0 || nanosec != 0) {
                    rx_time.seconds = sec;
                    rx_time.nanoseconds = nanosec;
                    timestamp_ok = TRUE;
                    T_DG(TRACE_GRP_DM, "rx_frame: PHY timestamp get, timestamp_ok:%d, ts_offset%d", timestamp_ok, ts_offset);
                } else {
#if DEBUG_MORE_INFO
                    T_DG(TRACE_GRP_DM, "*** rx_frame: PHY timestamp not get  *** ");
#endif
                }
            } else {
#if DEBUG_MORE_INFO
                T_DG(TRACE_GRP_DM, " *** rx_frame: port %ld not support PHY timestamp   *** ", rx_info->port_no);
#endif
            }
            CRIT_EXIT(crit_p);
        }
    }

    if (!is_mpls_oam2 && ((frame[tl_idx+3] == 1) || (frame[tl_idx+3] == 42) || (frame[tl_idx+3] == 43)))
    { /* Only get rx frame count if it is a LM frame */
        if (mep_caps.mesa_mep_serval) {
            if (rx_info->oam_info_decoded) {
                rx_count = rx_info->oam_info;
                frame_info.oam_info = rx_info->oam_info;
                T_DG(TRACE_GRP_LM, "RX count decoded %u", rx_count);
            }
        }
        u8 lm_null_counter[12];
        memset(lm_null_counter, 0, sizeof(lm_null_counter));
        if ((frame[tl_idx+3] == 1) && memcmp(lm_null_counter, &frame[tl_idx+2+58], sizeof(lm_null_counter)))
            T_DG(TRACE_GRP_LM, "RX CCM  TxFCf %u RxFCb %u TxFCb %u", string_to_var(&frame[tl_idx+2+58]), string_to_var(&frame[tl_idx+2+62]), string_to_var(&frame[tl_idx+2+66]));
        if (frame[tl_idx+3] == 42)
            T_DG(TRACE_GRP_LM, "RX LMR  TxFCf %u RxFCb %u TxFCb %u", string_to_var(&frame[tl_idx+2+4]), string_to_var(&frame[tl_idx+2+8]), string_to_var(&frame[tl_idx+2+12]));
        if (frame[tl_idx+3] == 43)
            T_DG(TRACE_GRP_LM, "RX LMM  TxFCf %u", string_to_var(&frame[tl_idx+2+4]));
    }

    mesa_packet_frame_info_init(&info);
    info.vid = rx_info->tag.vid;
    info.port_no = rx_info->port_no;
    if (mesa_packet_frame_filter(NULL, &info, &filter) != VTSS_RC_OK) /* Get the filter info for this VID */
        return(TRUE);
    T_NG(TRACE_GRP_DISCARD, "vid %u  port %u  opcode %u  filter %u", rx_info->tag.vid, rx_info->port_no, frame[tl_idx+3], filter);

    frame_info.discard = (filter == MESA_PACKET_FILTER_DISCARD);
    frame_info.vid = rx_info->tag.vid;
    frame_info.qos = rx_info->cos;
    frame_info.pcp = rx_info->tag.pcp;
    frame_info.dei = rx_info->tag.dei;
    frame_info.isdx = rx_info->isdx;
    frame_info.tagged = (!is_mpls && (rx_info->tag_type != MESA_TAG_TYPE_UNTAGGED)) ? TRUE : FALSE;
    frame_info.otag_vid = rx_info->stripped_tag.vid;
    frame_info.otag_pcp = rx_info->stripped_tag.pcp;
    frame_info.otag_dei = rx_info->stripped_tag.dei;
    frame_info.itag_vid = (tl_idx > 12) ? (((frame[tl_idx-2] & 0x0F) << 8) + frame[tl_idx-1]) : 0;
    frame_info.itag_pcp = (tl_idx > 12) ? ((frame[tl_idx-2] & 0xE0) >> 5) : 0;
    frame_info.itag_dei = (tl_idx > 12) ? ((frame[tl_idx-2] & 0x10) >> 4) : 0;

    vtss_mep_supp_rx_frame(port_conv, &frame_info, (u8 *)&frame[eth_offs], rx_info->length - eth_offs, &rx_time, rx_count);

    return(TRUE);
}

static void mep_voe_interrupt(meba_event_t source_id, u32 instance_id)
{
    mesa_rc rc;

    T_DG(TRACE_GRP_EVENT, "source_id %u  instance_id %u", source_id, instance_id);

    // mep_voe_interrupt() is just a stepping-stone function
    vtss_mep_supp_voe_interrupt();

    // Re-hook
    if ((rc = vtss_interrupt_source_hook_set(VTSS_MODULE_ID_MEP, mep_voe_interrupt, MEBA_EVENT_VOE, INTERRUPT_PRIORITY_PROTECT)) != VTSS_RC_OK &&
         rc != MESA_RC_NOT_IMPLEMENTED) {
        T_E("vtss_interrupt_source_hook_set failed %s", error_txt(rc));
    }
}

#ifdef VTSS_SW_OPTION_PRIVATE_MIB
VTSS_PRE_DECLS void mep_mib_init(void);
#endif
#ifdef VTSS_SW_OPTION_JSON_RPC
VTSS_PRE_DECLS void vtss_appl_mep_json_init(void);
#endif

extern "C" int mep_icli_cmd_register();

mesa_rc mep_init(vtss_init_data_t *data)
{
    u32                  base_rc, i, j;
    vtss_isid_t          isid = data->isid;
    packet_rx_filter_t   filter;
    void                 *filter_id;
    mesa_rc              rc = VTSS_RC_OK;
    mesa_port_list_t     oper_up;
    mesa_port_list_t     out_of_service;
    mep_conf_t           conf;

    if (data->cmd == INIT_CMD_EARLY_INIT) {
        /* Initialize and register trace ressources */
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);
    }

    T_D("enter, cmd: %d, isid: %u, flags: 0x%x", data->cmd, data->isid, data->flags);

    switch (data->cmd)
    {
        case INIT_CMD_INIT:
            T_D("INIT");

            {
                uint32_t mep_loop_port = mep_caps.appl_mep_loop_port;
                if (mep_loop_port != MESA_PORT_NO_NONE) {
                    voe_up_mep_loop_port = mep_loop_port;
                } else {
                    voe_up_mep_loop_port = mep_caps.mesa_port_cnt;
                }
            }

            /* initialize critd */
            critd_init(&crit_p, "MEP Platform Crit", VTSS_MODULE_ID_MEP, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
            critd_init(&crit, "MEP Crit", VTSS_MODULE_ID_MEP, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
            critd_init(&crit_supp, "MEP supp Crit", VTSS_MODULE_ID_MEP, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);

            vtss_flag_init(&run_wait_flag);
            vtss_flag_init(&timer_wait_flag);

            los_state.clear_all();
            for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; ++i)
            {
                instance_data[i].init();
                instance_data[i].aps_inst = APS_INST_INV;
                instance_data[i].mip_lbm_ace_id = ACL_MGMT_ACE_ID_NONE;
                for (j=0; j<mep_caps.mesa_port_cnt; ++j) {
                    instance_data[i].etree_elan_uc_ace_id[j] = ACL_MGMT_ACE_ID_NONE;
                    instance_data[i].etree_elan_mc_ace_id[j] = ACL_MGMT_ACE_ID_NONE;
                    instance_data[i].etree_elan_other_uc_ace_id[j] = ACL_MGMT_ACE_ID_NONE;
                    instance_data[i].tst_ace_id[j] = ACL_MGMT_ACE_ID_NONE;
                }
                instance_data[i].ccm_loc_ace_id[0] = ACL_MGMT_ACE_ID_NONE;
                instance_data[i].ccm_loc_ace_id[1] = ACL_MGMT_ACE_ID_NONE;
                instance_data[i].ccm_any_ace_id[0] = ACL_MGMT_ACE_ID_NONE;
                instance_data[i].ccm_any_ace_id[1] = ACL_MGMT_ACE_ID_NONE;
                instance_data[i].llm_ace_id = ACL_MGMT_ACE_ID_NONE;
            }
            base_rc = vtss_mep_init(10, ACL_POLICY_MEP, ACL_POLICY_MIP);
            base_rc += vtss_mep_supp_init(10, voe_up_mep_loop_port, ACL_POLICY_MEP, ACL_POLICY_MIP);
            if (base_rc)        T_D("Error during init");

            for (i=0; i<mep_caps.mesa_port_cnt; ++i) {
                in_port_conv[i] = out_port_conv[i] = i;
                conv_los_state[i] = TRUE;
                out_port_1p1[i] = FALSE;
                out_port_tx[i] = TRUE;
            }

            if (mep_caps.mesa_mep_serval) {
                qos_port_conf.clear();
                vlan_port_conf.clear();

                service_mep_ace_id = ACL_MGMT_ACE_ID_NONE;
                service_mip_ccm_ace_id = ACL_MGMT_ACE_ID_NONE;
                service_mip_uni_ltm_ace_id = ACL_MGMT_ACE_ID_NONE;
                service_mip_nni_ltm_ace_id = ACL_MGMT_ACE_ID_NONE;
                service_mip_uni_llm_ace_id = ACL_MGMT_ACE_ID_NONE;
                service_mip_nni_llm_ace_id = ACL_MGMT_ACE_ID_NONE;
            }
            CRIT_EXIT(crit_p);
            CRIT_EXIT(crit);
            CRIT_EXIT(crit_supp);

#ifdef VTSS_SW_OPTION_ICFG
            // Initialize ICLI "show running" configuration
            VTSS_RC(mep_icfg_init());
#endif

#ifdef VTSS_SW_OPTION_PRIVATE_MIB
            mep_mib_init(); /* Currently the MEP MIB is not allowed on Cisco target */
#endif
#ifdef VTSS_SW_OPTION_JSON_RPC
            vtss_appl_mep_json_init();
#endif
            mep_icli_cmd_register();


            break;
        case INIT_CMD_START:
            T_D("START");
            // Hooking IRQ may fail - hence the test for MESA_RC_NOT_IMPLEMENTED
            /* In case of protection we need to hook on fast link fail and LOS detection */
            if ((rc = vtss_interrupt_source_hook_set(VTSS_MODULE_ID_MEP,
                                                     link_interrupt_function,
                                                     MEBA_EVENT_FLNK,
                                                     INTERRUPT_PRIORITY_PROTECT)) != VTSS_RC_OK &&
                rc != MESA_RC_NOT_IMPLEMENTED) {
                T_E("vtss_interrupt_source_hook_set failed %s", error_txt(rc));
            }
            if ((rc = vtss_interrupt_source_hook_set(VTSS_MODULE_ID_MEP,
                                                     link_interrupt_function,
                                                     MEBA_EVENT_LOS,
                                                     INTERRUPT_PRIORITY_PROTECT)) != VTSS_RC_OK &&
                rc != MESA_RC_NOT_IMPLEMENTED) {
                T_E("vtss_interrupt_source_hook_set failed %s", error_txt(rc));
            }


            /* latency observed in onestep tx timestamping */
            if (mep_caps.mesa_mep_serval) {
                observed_egr_lat.mean = 0x509B3BD5; /* The mean value is initialized to a experimental value */
                /* Hook up for VOE events */
                if ((rc = vtss_interrupt_source_hook_set(VTSS_MODULE_ID_MEP, mep_voe_interrupt, MEBA_EVENT_VOE, INTERRUPT_PRIORITY_PROTECT)) != VTSS_RC_OK &&
                    rc != MESA_RC_NOT_IMPLEMENTED) {
                    T_E("vtss_interrupt_source_hook_set failed %s", error_txt(rc));
                }
            } else if (mep_caps.mesa_mep_luton26) {
                observed_egr_lat.mean = 0x38CB3BD5; /* The mean value is initialized to a experimental value */
            }
            break;
        case INIT_CMD_CONF_DEF:
            T_D("CONF_DEF");
            if (isid == VTSS_ISID_GLOBAL)
            {
                restore_to_default();
            }
            break;
        case INIT_CMD_MASTER_UP:
            T_D("MASTER_UP");
            restore_to_default();
            break;
        case INIT_CMD_MASTER_DOWN:
            T_D("MASTER_DOWN");
            break;
        case INIT_CMD_SWITCH_ADD:
            T_D("SWITCH_ADD");
            // We create the thread here because the async config code indirectly expects a valid ISID.
            vtss_thread_create(VTSS_THREAD_PRIO_HIGHER,
                               mep_run_thread,
                               0,
                               "MEP State Machine",
                               nullptr,
                               0,
                               &run_thread_handle,
                               &run_thread_block);

            /* MEP need to hook on port state change */
            rc = port_change_register(VTSS_MODULE_ID_MEP, port_change_callback);
            if (rc != VTSS_RC_OK)        T_D("Error during port change register");

            rc = port_shutdown_register(VTSS_MODULE_ID_MEP, port_shutdown_callback);
            if (rc != VTSS_RC_OK)        T_D("Error during port shutdown register");

            memset(evc_vlan_vid, 0xFF, sizeof(evc_vlan_vid));


























            /* Register for VLAN port configuration changes */
            vlan_port_conf_change_register(VTSS_MODULE_ID_MEP, vlan_port_change_callback, FALSE /* Get called back once the change has occurred */);

            if (mep_caps.mesa_mep_serval) {
                vtss_appl_vlan_entry_t vlan_conf;
                mesa_vid_t             vid;

                vid = VTSS_VID_NULL;
                uint32_t evcs_cnt = mep_caps.mesa_evc_evc_cnt;
                while (vtss_appl_vlan_get(VTSS_ISID_START, vid, &vlan_conf, TRUE, VTSS_APPL_VLAN_USER_STATIC) == VTSS_RC_OK) { /* Register all static configured VID */
                    vid = vlan_conf.vid; /* Select next entry */
                    if (evc_vlan_vid[vid] >= evcs_cnt) {    /* This VID is not registered as a EVC VID */
                        evc_vlan_vid[vid] = evcs_cnt;
                    }
                }

                vlan_membership_change_register(VTSS_MODULE_ID_MEP, vlan_membership_change_callback);
                rc = qos_port_conf_change_register(FALSE, VTSS_MODULE_ID_MEP, qos_port_change_callback);
                if (rc != VTSS_RC_OK)        vtss_mep_trace("vtss_mep_init - QOS register failed", 0, 0, 0, 0);
            }

            if (l2_stp_state_change_register(mstp_change_callback) != VTSS_RC_OK)     vtss_mep_trace("vtss_mep_init - MSTP register failed", 0, 0, 0, 0);

            packet_rx_filter_init(&filter);
            filter.modid = VTSS_MODULE_ID_MEP;
            filter.match = PACKET_RX_FILTER_MATCH_ETYPE;
            filter.cb    = rx_frame;
            filter.prio  = PACKET_RX_FILTER_PRIO_NORMAL;
            filter.mtu = mep_caps.appl_mep_packet_rx_mtu;
            filter.etype = 0x8902; // OAM ethertype
            if (packet_rx_filter_register(&filter, &filter_id) != VTSS_RC_OK)   return(FALSE);
            filter.etype = 0x8100; // C-TAG
            if (packet_rx_filter_register(&filter, &filter_id) != VTSS_RC_OK)   return(FALSE);
            filter.etype = 0x88A8; // S-TAG
            if (packet_rx_filter_register(&filter, &filter_id) != VTSS_RC_OK)   return(FALSE);





            /* The following must be done now - in 'switch add' - as port module must be ready.  */
            /* Problem is that ICLI is calling MEP during 'master up' and configuration is executed at that time. I guess ideally configuration should only be stored during 'master up' and then executed during 'switch add' */
            memset(oper_up, 0, sizeof(oper_up));
            memset(out_of_service, 0, sizeof(out_of_service));
            for (i=0; i<VTSS_APPL_MEP_INSTANCE_MAX; i++) {
                if ((instance_data[i].enable) && (vtss_mep_mgmt_conf_get(i, &conf) == VTSS_RC_OK)) {
                    if (conf.direction == VTSS_APPL_MEP_UP) {   /* An Up-MEP requires a "NNI" port*/
                        oper_up[conf.port] = TRUE;
                        if (conf.out_of_service) {
                            out_of_service[conf.port] = TRUE;
                        }
                    }
                }
            }
            for (i=0; i<mep_caps.mesa_port_cnt; i++) {
                vtss_mep_supp_port_conf(0, i, oper_up[i], out_of_service[i]);
            }
            break;
        case INIT_CMD_SWITCH_DEL:
            T_D("SWITCH_DEL");
            break;
        case INIT_CMD_SUSPEND_RESUME:
            T_D("SUSPEND_RESUME");
            break;
        default:
            break;
    }

    T_D("exit");
    return VTSS_RC_OK;
}

}  // mep_g1

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
