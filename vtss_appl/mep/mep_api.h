/*
 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
*/

#ifndef _MEP_API_H_
#define _MEP_API_H_

#include "main_types.h"
#include "mep_ll_api.h"
#include "packet_api.h"
#include "vtss/appl/mep.h"
#include "vtss_timer_api.h"

// Structure with cached MESA/Application capabilities.
typedef struct {
    uint32_t mesa_evc_etree;
    uint32_t mesa_evc_evc_cnt;
    uint32_t mesa_mep_generation;
    uint32_t mesa_mep_lbr_mce_hw_support;
    uint32_t mesa_mep_luton26;
    uint32_t mesa_mep_prop_delay_measurement;
    uint32_t mesa_mep_serval;
    uint32_t mesa_mep_tst_bitrate_mbps;
    uint32_t mesa_misc_chip_family;
    uint32_t mesa_mpls;
    uint32_t mesa_oam;
    uint32_t mesa_oam_down_mip_cnt;
    uint32_t mesa_oam_event_array_size;
    uint32_t mesa_oam_event_ccm_loc;
    uint32_t mesa_oam_event_ccm_meg_id;
    uint32_t mesa_oam_event_ccm_mep_id;
    uint32_t mesa_oam_event_ccm_period;
    uint32_t mesa_oam_event_ccm_priority;
    uint32_t mesa_oam_event_ccm_rx_rdi;
    uint32_t mesa_oam_event_ccm_tlv_if_status;
    uint32_t mesa_oam_event_ccm_tlv_port_status;
    uint32_t mesa_oam_event_ccm_zero_period;
    uint32_t mesa_oam_event_meg_level;
    uint32_t mesa_oam_path_service_voe_cnt;
    uint32_t mesa_oam_peer_cnt;
    uint32_t mesa_oam_port_voe_cnt;
    uint32_t mesa_oam_sat_cosid_ctr_cnt;
    uint32_t mesa_oam_up_mip_cnt;
    uint32_t mesa_oam_v1;
    uint32_t mesa_oam_v2;
    uint32_t mesa_oam_voe_cnt;
    uint32_t mesa_oam_voe_event_bfd_loc;
    uint32_t mesa_oam_voe_event_bfd_rx_diag_change_sink;
    uint32_t mesa_oam_voe_event_bfd_rx_diag_change_src;
    uint32_t mesa_oam_voe_event_bfd_rx_dm_change_sink;
    uint32_t mesa_oam_voe_event_bfd_rx_dm_change_src;
    uint32_t mesa_oam_voe_event_bfd_rx_f_set_sink;
    uint32_t mesa_oam_voe_event_bfd_rx_f_set_src;
    uint32_t mesa_oam_voe_event_bfd_rx_p_set_sink;
    uint32_t mesa_oam_voe_event_bfd_rx_p_set_src;
    uint32_t mesa_oam_voe_event_bfd_rx_state_change_sink;
    uint32_t mesa_oam_voe_event_bfd_rx_state_change_src;
    uint32_t mesa_packet_vstax;
    uint32_t mesa_phy_ts;
    uint32_t mesa_port_10g;
    uint32_t mesa_port_cnt;
    uint32_t mesa_port_frame_length_max;
    uint32_t mesa_qos_ingress_map_cnt;
    uint32_t appl_afi;
    uint32_t appl_mep_evc_qos;
    uint32_t appl_mep_instance_max;
    uint32_t appl_mep_loop_port;
    uint32_t appl_mep_mce_idx_used_max;
    uint32_t appl_mep_oper_state;
    uint32_t appl_mep_packet_rx_mtu;
    uint32_t appl_mep_rx_isdx_size;
    uint32_t appl_mep_test_rate_max;
} mep_caps_t;

extern mep_caps_t mep_caps;

/* In Subscriber domain the MEP instance will handle ALL VID values in inner tag
 */
#define VTSS_MEP_MGMT_VID_ALL 0xFFFF

/* Max number of MEP instance */
#define VTSS_APPL_MEP_INSTANCE_MAX mep_caps.appl_mep_instance_max

#define MEP_APS_DATA_LENGTH 8

/* Max number of EPS relations */
#define MEP_EPS_MAX 65

/** The number of priority */
#define VTSS_APPL_MEP_PRIO_MAX 8

#define VTSS_APPL_MEP_TST_RATE_MAX           mep_caps.appl_mep_test_rate_max /**< The maximum TST bit rate on Serval - 250000 Kbps = 2,5 Gbps */

 /**< HW OAM support capability */
#define VTSS_APPL_MEP_VOE_SUPPORTED          mep_caps.mesa_oam

/**< HW flow LM count capability */
#define VTSS_APPL_MEP_FLOW_COUNT_CAPABILITY  mep_caps.mesa_oam

/**< HW OAM LM count capability */
#define VTSS_APPL_MEP_OAM_COUNT_CAPABILITY   mep_caps.mesa_oam

#define VTSS_APPL_MEP_SW_OAM_SUPPORT (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1)

/** MIN DM transmission interval  */
#define VTSS_APPL_MEP_DM_INTERVAL_MIN 10

/** MAX DM transmission interval */
#define VTSS_APPL_MEP_DM_INTERVAL_MAX 65535

/** MIN number of DM LastN for average calculation */
#define VTSS_APPL_MEP_DM_LASTN_MIN 10

/** MAX number of DM LastN for average calculation */
#define VTSS_APPL_MEP_DM_LASTN_MAX 100

/** The minimum LBM frame size - including DMAC + SMAC + T/L + FCS = 18 bytes -
  * including any tags that might be added */
#define VTSS_APPL_MEP_LBM_SIZE_MIN 64

/** The maximum LBM frame size - including DMAC + SMAC + T/L + FCS = 18 bytes -
  * including any tags that might be added */
#define VTSS_APPL_MEP_LBM_SIZE_MAX mep_caps.mesa_port_frame_length_max

/** The minimum TST frame size - including DMAC + SMAC + T/L + FCS = 18 bytes -
  * including any tags that might be added */
#define VTSS_APPL_MEP_TST_SIZE_MIN 64

/** The maximum TST frame size - including DMAC + SMAC + T/L + FCS = 18 bytes -
 * including any tags that might be added */
#define VTSS_APPL_MEP_TST_SIZE_MAX mep_caps.mesa_port_frame_length_max

/** The minimum TST frame size - including DMAC + SMAC + T/L + FCS = 18 bytes -
 * including any tags that might be added */
#define VTSS_APPL_MEP_SLM_SIZE_MIN 64

/** The maximum SLM frame size - including DMAC + SMAC + T/L + FCS = 18 bytes -
 * including any tags that might be added */
#define VTSS_APPL_MEP_SLM_SIZE_MAX mep_caps.mesa_port_frame_length_max

/** The maximum LBM frame size for MPLS - limited by packet module */
#define VTSS_APPL_MEP_LBM_MPLS_SIZE_MAX 1526

/** The maximum TST frame size - limited by packet module */
#define VTSS_APPL_MEP_TST_MPLS_SIZE_MAX 1526

/** The number of TEST flow per priority */
#define VTSS_APPL_MEP_TEST_MAX 2

/** Min number of Measurement Bins for FD */
#define VTSS_APPL_MEP_DM_BINS_FD_MIN 2

/** Min number of Measurement Bins for IFDV */
#define VTSS_APPL_MEP_DM_BINS_IFDV_MIN 2

/** Max number of client flows */
#define VTSS_APPL_MEP_CLIENT_FLOWS_MAX 10

/** MAC length */
#define VTSS_APPL_MEP_MAC_LENGTH 6

/** Max MEP ID value  */
#define VTSS_APPL_MEP_ID_MAX 0x1FFF

/** The highest possible client priority is requested */
#define VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST 0xFF

/** Send LBM infinite - test behaviour */
#define VTSS_APPL_MEP_LB_TO_SEND_INFINITE 0

/** Max number of BFD Authentication Keys */
#define VTSS_APPL_MEP_BFD_AUTH_KEYS_MAX 256

/** EVC PAG capability */
#define VTSS_APPL_MEP_EVC_PAG_CAPABILITY FALSE

/**< EVC qos capability */
#define VTSS_APPL_MEP_EVC_QOS_CAPABILITY mep_caps.appl_mep_evc_qos





/**< ITU G.8113.2 OAM capabilties */
#define VTSS_APPL_MEP_ITU_8113_2_CAPABILITY            FALSE


typedef enum {
    MEP_EPS_TYPE_INV,
    MEP_EPS_TYPE_ELPS,
    MEP_EPS_TYPE_ERPS,
} mep_eps_type_t;

typedef enum {
    MEP_EPS_ARCHITECTURE_1F1,
    MEP_EPS_ARCHITECTURE_1P1
} mep_eps_architecture_t;

// The instance flow domain
typedef enum {
    VTSS_APPL_MEP_PORT,
    VTSS_APPL_MEP_EVC,
    VTSS_APPL_MEP_VLAN,
    VTSS_APPL_MEP_MPLS_LINK,
    VTSS_APPL_MEP_MPLS_TUNNEL,
    VTSS_APPL_MEP_MPLS_PW,
    VTSS_APPL_MEP_MPLS_LSP,
} mep_domain_t;

typedef struct {
    /** Instance is enabled/disabled */
    BOOL enable;

    /** MEP or MIP mode */
    vtss_appl_mep_mode_t mode;

    /** Up-MEP or Down-MEP direction */
    vtss_appl_mep_direction_t direction;

    /** Domain */
    mep_domain_t domain;

    /** Flow (EVC - VLAN) instance */
    u32 flow;

    /** Residence port */
    u32 port;

    /** MEG level. Range 0 to 7 */
    u32 level;

    /** VID used for tagged Port domain OAM. VTSS_MEP_MGMT_VID_ALL can be used
     * for subscriber domain */
    u16 vid;

    /** This should be VOE based if possible */
    BOOL voe;

    /** MEP Unicast MAC. If 'all zero' the residence port MAC is used */
    mesa_mac_t mac;

    /** MEG-ID format. */
    vtss_appl_mep_format_t format;

    /** IEEE Maintenance Domain Name.  (string)               */
    char name[VTSS_APPL_MEP_MEG_CODE_LENGTH];

    /** Unique MEG ID.  (string)               */
    char meg[VTSS_APPL_MEP_MEG_CODE_LENGTH];

    /** MEP id of this instance */
    u32 mep;

    /** Number of peer MEP's. 0 to VTSS_APPL_MEP_PEER_MAX */
    u32 peer_count;

    /** MEP id of peer MEP's */
    u16 peer_mep[VTSS_APPL_MEP_PEER_MAX];

    /** Peer unicast MAC */
    mesa_mac_t peer_mac[VTSS_APPL_MEP_PEER_MAX];

    /** Up-MEP is out of service. On Serval-1 a VOE based Up-MEP is set OOS and
     * the UNI is looping - it now has functionality as a Down-MEP. */
    BOOL out_of_service;

    /** EVC generated policy PAG value. On Jaguar this is used as IS2 key. For
     * Up-MEP (DS1076) this is used when creating MCE (IS1) entries */
    u32 evc_pag;

    /** EVC QoS value. Only used on Caracal for getting queue frame counters.
     * For Up-MEP (DS1076) this is used when creating MCE (IS1) entries */
    u32 evc_qos;

    // Only used by G2
    /** TRUE if this MEP/MIP is used by a TT-LOOP LL instance */
    BOOL ll_is_used;
} mep_conf_t;

typedef struct {
    /** Instance configuration default */
    mep_conf_t config;

    /** Peer configuration default */
    vtss_appl_mep_peer_conf_t peer_conf;

    /** Performance Monitoring default */
    vtss_appl_mep_pm_conf_t pm_conf;

    /** Link State Tracking default */
    vtss_appl_mep_lst_conf_t lst_conf;

    /** Syslog generation configuration default */
    vtss_appl_mep_syslog_conf_t syslog_conf;

    /** Transmitted TLV configuration default */
    vtss_appl_mep_tlv_conf_t tlv_conf;

    /** Continuity Check default */
    vtss_appl_mep_cc_conf_t cc_conf;

    /** Loss Measurement default */
    vtss_appl_mep_lm_conf_t lm_conf;

    /** Loss Measurement Availability default */
    vtss_appl_mep_lm_avail_conf_t lm_avail_conf;

    /** Loss Measurement High Loss Interval default */
    vtss_appl_mep_lm_hli_conf_t lm_hli_conf;

    /** Loss Measurement Signal Degrade default */
    vtss_appl_mep_lm_sdeg_conf_t lm_sdeg_conf;

    /** Delay Measurement default */
    vtss_appl_mep_dm_conf_t dm_conf;

    /** Automatic Protection Switching default */
    vtss_appl_mep_aps_conf_t aps_conf;

    /** Link Trace default */
    vtss_appl_mep_lt_conf_t lt_conf;

    /** Loop Back default */
    vtss_appl_mep_lb_conf_t lb_conf;

    /** Alarm Indication Signal default */
    vtss_appl_mep_ais_conf_t ais_conf;

    /** Locked Signal default */
    vtss_appl_mep_lck_conf_t lck_conf;

    /** Test Signal default */
    vtss_appl_mep_tst_conf_t tst_conf;

    /** Test Signal common default */
    vtss_appl_mep_tst_common_conf_t tst_common_conf;

    /** Loop Back common default */
    vtss_appl_mep_lb_common_conf_t lb_common_conf;

    /** Test priority default */
    vtss_appl_mep_test_prio_conf_t test_prio_conf;

    /** Delay Measurement common default */
    vtss_appl_mep_dm_common_conf_t dm_common_conf;

    /** Delay Measurement priority default */
    vtss_appl_mep_dm_prio_conf_t dm_prio_conf;

    /** Client flow configuration default */
    vtss_appl_mep_client_flow_conf_t client_flow_conf;

    /** BFD CC/CV default */
    vtss_appl_mep_g8113_2_bfd_conf_t bfd_conf;

    /** Route Trace default */
    vtss_appl_mep_rt_conf_t rt_conf;
} mep_default_conf_t;

typedef struct {
    /* RX and TX counters per priority */
    mesa_evc_counters_t counters[VTSS_APPL_MEP_PRIO_MAX];
} mep_sat_counters_t;

typedef struct {
    /* SLM transmitted counter(Service frame counter).  */
    u64 tx_slm_counter;

    /* SLM received counter(Service frame counter). */
    u64 rx_slm_counter;

    /* SLR transmitted counter(Service frame counter). */
    u64 tx_slr_counter;

    /* SLR received counter(Service frame counter). */
    u64 rx_slr_counter;
} sl_service_counter_t;


/**
 * \brief Client configuration data structure.
 *
 * This structure is used to do Client configuration of an instance.
 * This is configuring client layer where AIS and LCK is transmitted. All flows
 * at the client layer must be in the same domain.
 */
typedef struct {
    /** Number of client flows. 0 to VTSS_APPL_MEP_CLIENT_FLOWS_MAX */
    u32 flow_count;

    /** Client flow domain. Can only be VLAN, EVC or MPLS LSP. */
    mep_domain_t domain[VTSS_APPL_MEP_CLIENT_FLOWS_MAX];

    /** Client flow instance numbers */
    u32 flows[VTSS_APPL_MEP_CLIENT_FLOWS_MAX];

    /** AIS Priority (EVC COS-ID) 0-7.  VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST
     * indicate highest possible is requested */
    u8 ais_prio[VTSS_APPL_MEP_CLIENT_FLOWS_MAX];

    /** LCK Priority (EVC COS-ID) 0-7.  VTSS_APPL_MEP_CLIENT_PRIO_HIGHEST
     * indicate highest possible is requested */
    u8 lck_prio[VTSS_APPL_MEP_CLIENT_FLOWS_MAX];

    /** Client flow level */
    u8 level[VTSS_APPL_MEP_CLIENT_FLOWS_MAX];
} mep_client_conf_t;

typedef struct {
    /* Actual transmitted bits per second in line rate */
    u64 tx_bps[VTSS_APPL_MEP_PRIO_MAX][VTSS_APPL_MEP_TEST_MAX];
} mep_tx_bps_t;

typedef struct {
    u32 count;
    u16 inst[MEP_EPS_MAX];
} mep_eps_state_t;

#define VTSS_MEP_MGMT_SAT_VID_UNUSED 0xFFFFFFFF
#define VTSS_MEP_MGMT_SAT_VID_UNTAGGED 0xFFFFFFFE
#define VTSS_MEP_MGMT_SAT_VID_PRIO 0
typedef struct {
    /* G1: The SAT EVC VLAN ID per COS. VTSS_MEP_MGMT_SAT_VID_UNTAGGED and
     * VTSS_MEP_MGMT_SAT_VID_PRIO can used */
    /* G2: The SAT EVC VLAN ID per PRIO. VTSS_MEP_MGMT_SAT_VID_UNTAGGED and
     * VTSS_MEP_MGMT_SAT_VID_PRIO can used */
    u32 vid;

    /* The following is to have same interface on Jaguar2 and Serval1. Only used
     * on Serval1. */
    /* The SAT EVC policer ID per COS */
    mesa_evc_policer_id_t policer;
    /* Enable DP classification */
    BOOL dp_enable;
    /* Drop precedence */
    mesa_dp_level_t dp;
} mep_sat_prio_conf_t;

typedef struct {
    mep_conf_t conf;
    mep_sat_prio_conf_t prio_conf[VTSS_APPL_MEP_PRIO_MAX];
} mep_sat_conf_t;

typedef struct {
    /* DMM transmit time  */
    mesa_timestamp_t tx_time;
    /* DMM receive time returned in DMR */
    mesa_timestamp_t rx_time;
} mep_dm_timestamp_t;

typedef enum
{
    VTSS_MEP_ETREE_NONE,
    VTSS_MEP_ETREE_ROOT,
    VTSS_MEP_ETREE_LEAF
} vtss_mep_mgmt_etree_t;

typedef struct {
    vtss_mep_mgmt_etree_t  e_tree;
} vtss_mep_mgmt_etree_conf_t;


#if defined(MEP_IMPL_G1)
namespace mep_g1 {
#elif defined(MEP_IMPL_G2)
namespace mep_g2 {
#endif

const char *mep_error_txt(mesa_rc rc);

void mep_default_conf_get(mep_default_conf_t *const conf);

mesa_rc mep_instance_conf_set(u32 instance, const mep_conf_t *const conf);
mesa_rc mep_instance_conf_add(u32 instance, const mep_conf_t *const conf);
mesa_rc mep_instance_conf_get(u32 instance, mep_conf_t *const conf);

mesa_rc mep_volatile_sat_conf_set(const u32 instance,
                                  const mep_sat_conf_t *const conf);

mesa_rc mep_volatile_sat_conf_get(const u32 instance, mep_sat_conf_t *const conf);

mesa_rc mep_volatile_sat_counter_get(const u32 instance,
                                     mep_sat_counters_t *const counters);

mesa_rc mep_volatile_sat_counter_clear(const u32 instance);

vtss_rc mep_volatile_sat_lb_prio_conf_set(
        const u32 instance,
        const u32 prio,
        const u32 test_idx,
        const vtss_appl_mep_test_prio_conf_t *const conf,
        u64 *active_time_ms);

vtss_rc mep_volatile_sat_tst_prio_conf_set(
        const u32 instance,
        const u32 prio,
        const u32 test_idx,
        const vtss_appl_mep_test_prio_conf_t *const conf,
        u64 *active_time_ms);

mesa_rc mep_tx_bps_actual_get(const u32 instance, mep_tx_bps_t *const bps);

mesa_rc mep_volatile_conf_set(const u32 instance, const mep_conf_t *const conf);

mesa_rc mep_slm_counters_get(const u32 instance,
                             const u32 peer_idx,
                             sl_service_counter_t *const counters);

mesa_rc mep_dm_timestamp_get(const u32 instance,
                             mep_dm_timestamp_t *const dm1_timestamp_far_to_near,
                             mep_dm_timestamp_t *const dm1_timestamp_near_to_far);

mesa_rc mep_status_eps_get(const u32 instance, mep_eps_state_t *const state);


/* instance:    Instance number of MEP.               */
/* eps_inst:    Instance number of calling EPS/REPS   */
/* eps_type:    It is a ELPS or ERPS register         */
/* enable:      TRUE means register is enabled        */
/* Receiving and transmitting of APS from/to this MEP, can be enabled/disabled
 */
mesa_rc mep_eps_aps_register(const u32 instance,
                             const u32 eps_inst,
                             const mep_eps_type_t eps_type,
                             const BOOL enable);


/* instance:    Instance number of MEP.               */
/* eps_inst:    Instance number of calling EPS/REPS   */
/* eps_type:    It is a ELPS or ERPS register         */
/* enable:      TRUE means register is enabled        */
/* Receiving of SF state from this MEP, can be enabled/disabled */
mesa_rc mep_eps_sf_register(const u32 instance,
                            const u32 eps_inst,
                            const mep_eps_type_t eps_type,
                            const BOOL enable);


/* instance:    Instance number of MEP.             */
/* eps_inst:    Instance number of calling EPS/REPS */
/* aps:         transmitted APS specific info.      */
/* event:       transmit APS as an ERPS event.      */
/* This called by EPS to transmit APS info. The first three APS is transmitted
 * with 3.3 ms. interval and thereafter with 5 s. interval.  */
/* The length of the 'aps' array must be MEP_APS_DATA_LENGTH minimum to satisfy
 * LINT                                                */
mesa_rc mep_tx_aps_info_set(const u32 instance,
                            const u32 eps_inst,
                            const u8 *aps,
                            const BOOL event);


/* instance:    Instance number of MEP.             */
/* eps_inst:    Instance number of calling EPS/REPS */
/* This is called by EPS to activate MEP calling out with SF state and received
 * APS */
mesa_rc mep_signal_in(const u32 instance, const u32 eps_inst);

/* instance:    Instance number of MEP.               */
/* eps_inst:    Instance number of calling EPS/REPS   */
/* enable:      TRUE means that R-APS is forwarded    */
/* R-APS forwarding to the MEP related port, can be enabled/disabled */
mesa_rc mep_raps_forwarding(const u32 instance,
                            const u32 eps_inst,
                            const BOOL enable);

/* instance:    Instance number of MEP.                                    */
/* eps_inst:    Instance number of calling EPS/REPS                        */
/* enable:      TRUE means that R-APS is transmitted at 5 sec. interval    */
/* R-APS transmission from the MEP related port, can be enabled/disabled   */
mesa_rc mep_raps_transmission(const u32 instance,
                              const u32 eps_inst,
                              const BOOL enable);



void mep_port_protection_create(mep_eps_architecture_t architecture,
                                const u32 w_port,
                                const u32 p_port);

void mep_port_protection_change(const u32 w_port,
                                const u32 p_port,
                                const BOOL active);

void mep_port_protection_delete(const u32 w_port, const u32 p_port);

void mep_ring_protection_block(const u32 port, BOOL block);

const char *vtss_mep_rate_to_string(vtss_appl_mep_rate_t rate);
const char *vtss_mep_flow_to_name(mep_domain_t domain,
                                  u32 flow,
                                  vtss_uport_no_t port_no,
                                  char *str);
const char *vtss_mep_domain_to_string(mep_domain_t domain);
const char *vtss_mep_direction_to_string(vtss_appl_mep_direction_t direction);
const char *vtss_mep_unit_to_string(vtss_appl_mep_dm_tunit_t unit);

u32 vtss_mep_mgmt_etree_conf_get(const u32                    instance,
                                 vtss_mep_mgmt_etree_conf_t   *const conf);

mesa_rc mep_client_conf_set(const u32 instance, const mep_client_conf_t *const conf);
mesa_rc mep_client_conf_get(const u32 instance, mep_client_conf_t *const conf);

/* instance:    Instance number of MEP.                 */
/* enable:      TRUE means that R-APS is forwarded      */
/* R-APS forwarding from the MEP related port, can be enabled/disabled */
u32 vtss_mep_raps_forwarding(const u32    instance,
                             const BOOL   enable);


u32 vtss_mep_mgmt_tlv_conf_set(const vtss_appl_mep_tlv_conf_t    *const conf);
u32 vtss_mep_mgmt_tlv_conf_get(vtss_appl_mep_tlv_conf_t    *const conf);

mesa_rc mep_init(vtss_init_data_t *data);

////////////////////////////////////////////////////////////////////////////////

#if defined(MEP_IMPL_G1)
} /* mep_g1 */
#elif defined(MEP_IMPL_G2)
} /* mep_g2 */
#endif

#endif  // _MEP_API_H_
