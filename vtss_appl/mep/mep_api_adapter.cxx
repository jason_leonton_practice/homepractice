/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#include "mscc/ethernet/switch/api.h"
#include <vtss/appl/mep.h>
#include "mep_api.h"

// Cached MESA/Application capabilities
mep_caps_t mep_caps;

namespace mep_g1 {
    const char *mep_error_txt(mesa_rc rc);
    void        mep_default_conf_get(mep_default_conf_t *const conf);
    mesa_rc     mep_instance_conf_set(u32 instance, const mep_conf_t *const conf);
    mesa_rc     mep_instance_conf_add(u32 instance, const mep_conf_t *const conf);
    mesa_rc     mep_instance_conf_get(u32 instance, mep_conf_t *const conf);
    mesa_rc     mep_volatile_sat_conf_set(const u32 instance, const mep_sat_conf_t *const conf);
    mesa_rc     mep_volatile_sat_conf_get(const u32 instance, mep_sat_conf_t *const conf);
    mesa_rc     mep_volatile_sat_counter_get(const u32 instance, mep_sat_counters_t *const counters);
    mesa_rc     mep_volatile_sat_counter_clear(const u32 instance);
    vtss_rc     mep_volatile_sat_lb_prio_conf_set(const u32 instance, const u32 prio, const u32 test_idx, const vtss_appl_mep_test_prio_conf_t *const conf, u64 * active_time_ms);
    vtss_rc     mep_volatile_sat_tst_prio_conf_set(const u32 instance, const u32 prio, const u32 test_idx, const vtss_appl_mep_test_prio_conf_t *const conf, u64 * active_time_ms);
    mesa_rc     mep_tx_bps_actual_get(const u32 instance, mep_tx_bps_t *const bps);
    mesa_rc     mep_volatile_conf_set(const u32 instance, const mep_conf_t *const conf);
    mesa_rc     mep_slm_counters_get(const u32 instance, const u32 peer_idx, sl_service_counter_t *const counters);
    mesa_rc     mep_dm_timestamp_get(const u32 instance, mep_dm_timestamp_t *const dm1_timestamp_far_to_near, mep_dm_timestamp_t *const dm1_timestamp_near_to_far);
    mesa_rc     mep_status_eps_get(const u32 instance, mep_eps_state_t *const state);
    mesa_rc     mep_eps_aps_register(const u32 instance, const u32 eps_inst, const mep_eps_type_t eps_type, const BOOL enable);
    mesa_rc     mep_eps_sf_register(const u32 instance, const u32 eps_inst, const mep_eps_type_t eps_type, const BOOL enable);
    mesa_rc     mep_tx_aps_info_set(const u32 instance, const u32 eps_inst, const u8 * aps, const BOOL event);
    mesa_rc     mep_signal_in(const u32 instance, const u32 eps_inst);
    mesa_rc     mep_raps_forwarding(const u32 instance, const u32 eps_inst, const BOOL enable);
    mesa_rc     mep_raps_transmission(const u32 instance, const u32 eps_inst, const BOOL enable);
    void        mep_port_protection_create(mep_eps_architecture_t architecture, const u32 w_port, const u32 p_port);
    void        mep_port_protection_change(const u32 w_port, const u32 p_port, const BOOL active);
    void        mep_port_protection_delete(const u32 w_port, const u32 p_port);
    void        mep_ring_protection_block(const u32 port, BOOL block);
    const char *vtss_mep_rate_to_string(vtss_appl_mep_rate_t rate);
    const char *vtss_mep_flow_to_name(mep_domain_t domain, u32 flow, vtss_uport_no_t port_no, char * str);
    const char *vtss_mep_domain_to_string(mep_domain_t domain);
    const char *vtss_mep_direction_to_string(vtss_appl_mep_direction_t direction);
    const char *vtss_mep_unit_to_string(vtss_appl_mep_dm_tunit_t unit);
    u32         vtss_mep_mgmt_etree_conf_get(const u32 instance, vtss_mep_mgmt_etree_conf_t *const conf);
    mesa_rc     mep_client_conf_set(const u32 instance, const mep_client_conf_t *const conf);
    mesa_rc     mep_client_conf_get(const u32 instance, mep_client_conf_t *const conf);
    u32         vtss_mep_raps_forwarding(const u32 instance, const BOOL enable);
    u32         vtss_mep_mgmt_tlv_conf_set(const vtss_appl_mep_tlv_conf_t *const conf);
    u32         vtss_mep_mgmt_tlv_conf_get(vtss_appl_mep_tlv_conf_t *const conf);
    mesa_rc     mep_init(vtss_init_data_t * data);
}

namespace mep_g2 {
    const char *mep_error_txt(mesa_rc rc);
    void        mep_default_conf_get(mep_default_conf_t *const conf);
    mesa_rc     mep_instance_conf_set(u32 instance, const mep_conf_t *const conf);
    mesa_rc     mep_instance_conf_add(u32 instance, const mep_conf_t *const conf);
    mesa_rc     mep_instance_conf_get(u32 instance, mep_conf_t *const conf);
    mesa_rc     mep_volatile_sat_conf_set(const u32 instance, const mep_sat_conf_t *const conf);
    mesa_rc     mep_volatile_sat_conf_get(const u32 instance, mep_sat_conf_t *const conf);
    mesa_rc     mep_volatile_sat_counter_get(const u32 instance, mep_sat_counters_t *const counters);
    mesa_rc     mep_volatile_sat_counter_clear(const u32 instance);
    vtss_rc     mep_volatile_sat_lb_prio_conf_set(const u32 instance, const u32 prio, const u32 test_idx, const vtss_appl_mep_test_prio_conf_t *const conf, u64 * active_time_ms);
    vtss_rc     mep_volatile_sat_tst_prio_conf_set(const u32 instance, const u32 prio, const u32 test_idx, const vtss_appl_mep_test_prio_conf_t *const conf, u64 * active_time_ms);
    mesa_rc     mep_tx_bps_actual_get(const u32 instance, mep_tx_bps_t *const bps);
    mesa_rc     mep_volatile_conf_set(const u32 instance, const mep_conf_t *const conf);
    mesa_rc     mep_slm_counters_get(const u32 instance, const u32 peer_idx, sl_service_counter_t *const counters);
    mesa_rc     mep_dm_timestamp_get(const u32 instance, mep_dm_timestamp_t *const dm1_timestamp_far_to_near, mep_dm_timestamp_t *const dm1_timestamp_near_to_far);
    mesa_rc     mep_status_eps_get(const u32 instance, mep_eps_state_t *const state);
    mesa_rc     mep_eps_aps_register(const u32 instance, const u32 eps_inst, const mep_eps_type_t eps_type, const BOOL enable);
    mesa_rc     mep_eps_sf_register(const u32 instance, const u32 eps_inst, const mep_eps_type_t eps_type, const BOOL enable);
    mesa_rc     mep_tx_aps_info_set(const u32 instance, const u32 eps_inst, const u8 * aps, const BOOL event);
    mesa_rc     mep_signal_in(const u32 instance, const u32 eps_inst);
    mesa_rc     mep_raps_forwarding(const u32 instance, const u32 eps_inst, const BOOL enable);
    mesa_rc     mep_raps_transmission(const u32 instance, const u32 eps_inst, const BOOL enable);
    void        mep_port_protection_create(mep_eps_architecture_t architecture, const u32 w_port, const u32 p_port);
    void        mep_port_protection_change(const u32 w_port, const u32 p_port, const BOOL active);
    void        mep_port_protection_delete(const u32 w_port, const u32 p_port);
    void        mep_ring_protection_block(const u32 port, BOOL block);
    const char *vtss_mep_rate_to_string(vtss_appl_mep_rate_t rate);
    const char *vtss_mep_flow_to_name(mep_domain_t domain, u32 flow, vtss_uport_no_t port_no, char * str);
    const char *vtss_mep_domain_to_string(mep_domain_t domain);
    const char *vtss_mep_direction_to_string(vtss_appl_mep_direction_t direction);
    const char *vtss_mep_unit_to_string(vtss_appl_mep_dm_tunit_t unit);
    u32         vtss_mep_mgmt_etree_conf_get(const u32 instance, vtss_mep_mgmt_etree_conf_t *const conf);
    mesa_rc     mep_client_conf_set(const u32 instance, const mep_client_conf_t *const conf);
    mesa_rc     mep_client_conf_get(const u32 instance, mep_client_conf_t *const conf);
    u32         vtss_mep_raps_forwarding(const u32 instance, const BOOL enable);
    u32         vtss_mep_mgmt_tlv_conf_set(const vtss_appl_mep_tlv_conf_t *const conf);
    u32         vtss_mep_mgmt_tlv_conf_get(vtss_appl_mep_tlv_conf_t *const conf);
    mesa_rc     mep_init(vtss_init_data_t * data);
}

const char *mep_error_txt(mesa_rc rc)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_error_txt(rc);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_error_txt(rc);
    }

    return 0;
}

void mep_default_conf_get(mep_default_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_default_conf_get(conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_default_conf_get(conf);
    }
}

mesa_rc mep_instance_conf_set(u32 instance, const mep_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_instance_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_instance_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_instance_conf_add(u32 instance, const mep_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_instance_conf_add(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_instance_conf_add(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_instance_conf_get(u32 instance, mep_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_instance_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_instance_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_volatile_sat_conf_set(const u32 instance, const mep_sat_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_volatile_sat_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_volatile_sat_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_volatile_sat_conf_get(const u32 instance, mep_sat_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_volatile_sat_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_volatile_sat_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_volatile_sat_counter_get(const u32 instance, mep_sat_counters_t *const counters)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_volatile_sat_counter_get(instance, counters);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_volatile_sat_counter_get(instance, counters);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_volatile_sat_counter_clear(const u32 instance)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_volatile_sat_counter_clear(instance);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_volatile_sat_counter_clear(instance);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

vtss_rc  mep_volatile_sat_lb_prio_conf_set(const u32 instance, const u32 prio, const u32 test_idx, const vtss_appl_mep_test_prio_conf_t *const conf, u64 * active_time_ms)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_volatile_sat_lb_prio_conf_set(instance, prio, test_idx, conf, active_time_ms);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_volatile_sat_lb_prio_conf_set(instance, prio, test_idx, conf, active_time_ms);
    }

    return 0;
}

vtss_rc  mep_volatile_sat_tst_prio_conf_set(const u32 instance, const u32 prio, const u32 test_idx, const vtss_appl_mep_test_prio_conf_t *const conf, u64 * active_time_ms)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_volatile_sat_tst_prio_conf_set(instance, prio, test_idx, conf, active_time_ms);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_volatile_sat_tst_prio_conf_set(instance, prio, test_idx, conf, active_time_ms);
    }

    return 0;
}

mesa_rc mep_tx_bps_actual_get(const u32 instance, mep_tx_bps_t *const bps)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_tx_bps_actual_get(instance, bps);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_tx_bps_actual_get(instance, bps);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_volatile_conf_set(const u32 instance, const mep_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_volatile_conf_set(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_volatile_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_slm_counters_get(const u32 instance, const u32 peer_idx, sl_service_counter_t *const counters)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_slm_counters_get(instance, peer_idx, counters);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_slm_counters_get(instance, peer_idx, counters);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_dm_timestamp_get(const u32 instance, mep_dm_timestamp_t *const dm1_timestamp_far_to_near, mep_dm_timestamp_t *const dm1_timestamp_near_to_far)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_dm_timestamp_get(instance, dm1_timestamp_far_to_near, dm1_timestamp_near_to_far);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_dm_timestamp_get(instance, dm1_timestamp_far_to_near, dm1_timestamp_near_to_far);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_status_eps_get(const u32 instance, mep_eps_state_t *const state)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_status_eps_get(instance, state);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_status_eps_get(instance, state);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_eps_aps_register(const u32 instance, const u32 eps_inst, const mep_eps_type_t eps_type, const BOOL enable)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_eps_aps_register(instance, eps_inst, eps_type, enable);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_eps_aps_register(instance, eps_inst, eps_type, enable);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_eps_sf_register(const u32 instance, const u32 eps_inst, const mep_eps_type_t eps_type, const BOOL enable)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_eps_sf_register(instance, eps_inst, eps_type, enable);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_eps_sf_register(instance, eps_inst, eps_type, enable);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_tx_aps_info_set(const u32 instance, const u32 eps_inst, const u8 * aps, const BOOL event)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_tx_aps_info_set(instance, eps_inst, aps, event);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_tx_aps_info_set(instance, eps_inst, aps, event);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_signal_in(const u32 instance, const u32 eps_inst)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_signal_in(instance, eps_inst);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_signal_in(instance, eps_inst);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_raps_forwarding(const u32 instance, const u32 eps_inst, const BOOL enable)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_raps_forwarding(instance, eps_inst, enable);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_raps_forwarding(instance, eps_inst, enable);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_raps_transmission(const u32 instance, const u32 eps_inst, const BOOL enable)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_raps_transmission(instance, eps_inst, enable);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_raps_transmission(instance, eps_inst, enable);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

void mep_port_protection_create(mep_eps_architecture_t architecture, const u32 w_port, const u32 p_port)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_port_protection_create(architecture, w_port, p_port);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_port_protection_create(architecture, w_port, p_port);
    }
}

void mep_port_protection_change(const u32 w_port, const u32 p_port, const BOOL active)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_port_protection_change(w_port, p_port, active);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_port_protection_change(w_port, p_port, active);
    }
}

void mep_port_protection_delete(const u32 w_port, const u32 p_port)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_port_protection_delete(w_port, p_port);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_port_protection_delete(w_port, p_port);
    }
}

void mep_ring_protection_block(const u32 port, BOOL block)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_ring_protection_block(port, block);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_ring_protection_block(port, block);
    }
}

const char *vtss_mep_rate_to_string(vtss_appl_mep_rate_t rate)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_mep_rate_to_string(rate);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_mep_rate_to_string(rate);
    }

    return 0;
}

const char *vtss_mep_flow_to_name(mep_domain_t domain, u32 flow, vtss_uport_no_t port_no, char * str)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_mep_flow_to_name(domain, flow, port_no, str);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_mep_flow_to_name(domain, flow, port_no, str);
    }

    return 0;
}

const char *vtss_mep_domain_to_string(mep_domain_t domain)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_mep_domain_to_string(domain);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_mep_domain_to_string(domain);
    }

    return 0;
}

const char *vtss_mep_direction_to_string(vtss_appl_mep_direction_t direction)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_mep_direction_to_string(direction);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_mep_direction_to_string(direction);
    }

    return 0;
}

const char *vtss_mep_unit_to_string(vtss_appl_mep_dm_tunit_t unit)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_mep_unit_to_string(unit);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_mep_unit_to_string(unit);
    }

    return 0;
}

u32 vtss_mep_mgmt_etree_conf_get(const u32 instance, vtss_mep_mgmt_etree_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_mep_mgmt_etree_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_mep_mgmt_etree_conf_get(instance, conf);
    }

    return 0;
}

mesa_rc mep_client_conf_set(const u32 instance, const mep_client_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_client_conf_set(instance, conf);
    }else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_client_conf_set(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

mesa_rc mep_client_conf_get(const u32 instance, mep_client_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_client_conf_get(instance, conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_client_conf_get(instance, conf);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

u32 vtss_mep_raps_forwarding(const u32 instance, const BOOL enable)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_mep_raps_forwarding(instance, enable);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_mep_raps_forwarding(instance, enable);
    }

    return 0;
}

u32 vtss_mep_mgmt_tlv_conf_set(const vtss_appl_mep_tlv_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_mep_mgmt_tlv_conf_set(conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_mep_mgmt_tlv_conf_set(conf);
    }

    return 0;
}

u32 vtss_mep_mgmt_tlv_conf_get(vtss_appl_mep_tlv_conf_t *const conf)
{
    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::vtss_mep_mgmt_tlv_conf_get(conf);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::vtss_mep_mgmt_tlv_conf_get(conf);
    }

    return 0;
}

mesa_rc mep_init(vtss_init_data_t *data)
{
    if (data->cmd == INIT_CMD_EARLY_INIT) {
        // This must be cached very early, since it's used below.
        mep_caps.mesa_mep_generation = MESA_CAP(MESA_CAP_MEP_GENERATION);
    }

    if (data->cmd == INIT_CMD_INIT) {
        // Cache capabilities
        mep_caps.mesa_evc_etree                              = MESA_CAP(MESA_CAP_EVC_ETREE);
        mep_caps.mesa_evc_evc_cnt                            = MESA_CAP(MESA_CAP_EVC_EVC_CNT);
        mep_caps.mesa_mep_lbr_mce_hw_support                 = MESA_CAP(MESA_CAP_MEP_LBR_MCE_HW_SUPPORT);
        mep_caps.mesa_mep_luton26                            = MESA_CAP(MESA_CAP_MEP_LUTON26);
        mep_caps.mesa_mep_prop_delay_measurement             = MESA_CAP(MESA_CAP_MEP_PROP_DELAY_MEASUREMENT);
        mep_caps.mesa_mep_serval                             = MESA_CAP(MESA_CAP_MEP_SERVAL);
        mep_caps.mesa_mep_tst_bitrate_mbps                   = MESA_CAP(MESA_CAP_MEP_TST_BITRATE_MBPS);
        mep_caps.mesa_misc_chip_family                       = MESA_CAP(MESA_CAP_MISC_CHIP_FAMILY);
        mep_caps.mesa_mpls                                   = MESA_CAP(MESA_CAP_MPLS);
        mep_caps.mesa_oam                                    = MESA_CAP(MESA_CAP_OAM);
        mep_caps.mesa_oam_down_mip_cnt                       = MESA_CAP(MESA_CAP_OAM_DOWN_MIP_CNT);
        mep_caps.mesa_oam_event_array_size                   = MESA_CAP(MESA_CAP_OAM_EVENT_ARRAY_SIZE);
        mep_caps.mesa_oam_event_ccm_loc                      = MESA_CAP(MESA_CAP_OAM_EVENT_CCM_LOC);
        mep_caps.mesa_oam_event_ccm_meg_id                   = MESA_CAP(MESA_CAP_OAM_EVENT_CCM_MEG_ID);
        mep_caps.mesa_oam_event_ccm_mep_id                   = MESA_CAP(MESA_CAP_OAM_EVENT_CCM_MEP_ID);
        mep_caps.mesa_oam_event_ccm_period                   = MESA_CAP(MESA_CAP_OAM_EVENT_CCM_PERIOD);
        mep_caps.mesa_oam_event_ccm_priority                 = MESA_CAP(MESA_CAP_OAM_EVENT_CCM_PRIORITY);
        mep_caps.mesa_oam_event_ccm_rx_rdi                   = MESA_CAP(MESA_CAP_OAM_EVENT_CCM_RX_RDI);
        mep_caps.mesa_oam_event_ccm_tlv_if_status            = MESA_CAP(MESA_CAP_OAM_EVENT_CCM_TLV_IF_STATUS);
        mep_caps.mesa_oam_event_ccm_tlv_port_status          = MESA_CAP(MESA_CAP_OAM_EVENT_CCM_TLV_PORT_STATUS);
        mep_caps.mesa_oam_event_ccm_zero_period              = MESA_CAP(MESA_CAP_OAM_EVENT_CCM_ZERO_PERIOD);
        mep_caps.mesa_oam_event_meg_level                    = MESA_CAP(MESA_CAP_OAM_EVENT_MEG_LEVEL);
        mep_caps.mesa_oam_path_service_voe_cnt               = MESA_CAP(MESA_CAP_OAM_PATH_SERVICE_VOE_CNT);
        mep_caps.mesa_oam_peer_cnt                           = MESA_CAP(MESA_CAP_OAM_PEER_CNT);
        mep_caps.mesa_oam_port_voe_cnt                       = MESA_CAP(MESA_CAP_OAM_PORT_VOE_CNT);
        mep_caps.mesa_oam_sat_cosid_ctr_cnt                  = MESA_CAP(MESA_CAP_OAM_SAT_COSID_CTR_CNT);
        mep_caps.mesa_oam_up_mip_cnt                         = MESA_CAP(MESA_CAP_OAM_UP_MIP_CNT);
        mep_caps.mesa_oam_v1                                 = MESA_CAP(MESA_CAP_OAM_V1);
        mep_caps.mesa_oam_v2                                 = MESA_CAP(MESA_CAP_OAM_V2);
        mep_caps.mesa_oam_voe_cnt                            = MESA_CAP(MESA_CAP_OAM_VOE_CNT);
        mep_caps.mesa_oam_voe_event_bfd_loc                  = MESA_CAP(MESA_CAP_OAM_VOE_EVENT_BFD_LOC);
        mep_caps.mesa_oam_voe_event_bfd_rx_diag_change_sink  = MESA_CAP(MESA_CAP_OAM_VOE_EVENT_BFD_RX_DIAG_CHANGE_SINK);
        mep_caps.mesa_oam_voe_event_bfd_rx_diag_change_src   = MESA_CAP(MESA_CAP_OAM_VOE_EVENT_BFD_RX_DIAG_CHANGE_SRC);
        mep_caps.mesa_oam_voe_event_bfd_rx_dm_change_sink    = MESA_CAP(MESA_CAP_OAM_VOE_EVENT_BFD_RX_DM_CHANGE_SINK);
        mep_caps.mesa_oam_voe_event_bfd_rx_dm_change_src     = MESA_CAP(MESA_CAP_OAM_VOE_EVENT_BFD_RX_DM_CHANGE_SRC);
        mep_caps.mesa_oam_voe_event_bfd_rx_f_set_sink        = MESA_CAP(MESA_CAP_OAM_VOE_EVENT_BFD_RX_F_SET_SINK);
        mep_caps.mesa_oam_voe_event_bfd_rx_f_set_src         = MESA_CAP(MESA_CAP_OAM_VOE_EVENT_BFD_RX_F_SET_SRC);
        mep_caps.mesa_oam_voe_event_bfd_rx_p_set_sink        = MESA_CAP(MESA_CAP_OAM_VOE_EVENT_BFD_RX_P_SET_SINK);
        mep_caps.mesa_oam_voe_event_bfd_rx_p_set_src         = MESA_CAP(MESA_CAP_OAM_VOE_EVENT_BFD_RX_P_SET_SRC);
        mep_caps.mesa_oam_voe_event_bfd_rx_state_change_sink = MESA_CAP(MESA_CAP_OAM_VOE_EVENT_BFD_RX_STATE_CHANGE_SINK);
        mep_caps.mesa_oam_voe_event_bfd_rx_state_change_src  = MESA_CAP(MESA_CAP_OAM_VOE_EVENT_BFD_RX_STATE_CHANGE_SRC);
        mep_caps.mesa_packet_vstax                           = MESA_CAP(MESA_CAP_PACKET_VSTAX);
        mep_caps.mesa_phy_ts                                 = MESA_CAP(MESA_CAP_PHY_TS);
        mep_caps.mesa_port_10g                               = MESA_CAP(MESA_CAP_PORT_10G);
        mep_caps.mesa_port_cnt                               = MESA_CAP(MESA_CAP_PORT_CNT);
        mep_caps.mesa_port_frame_length_max                  = MESA_CAP(MESA_CAP_PORT_FRAME_LENGTH_MAX);
        mep_caps.mesa_qos_ingress_map_cnt                    = MESA_CAP(MESA_CAP_QOS_INGRESS_MAP_CNT);
        mep_caps.appl_afi                                    = MESA_CAP(VTSS_APPL_CAP_AFI);
        mep_caps.appl_mep_evc_qos                            = MESA_CAP(VTSS_APPL_CAP_MEP_EVC_QOS);
        mep_caps.appl_mep_instance_max                       = MESA_CAP(VTSS_APPL_CAP_MEP_INSTANCE_MAX);
        mep_caps.appl_mep_loop_port                          = MESA_CAP(VTSS_APPL_CAP_MEP_LOOP_PORT);
        mep_caps.appl_mep_mce_idx_used_max                   = MESA_CAP(VTSS_APPL_CAP_MEP_MCE_IDX_USED_MAX);
        mep_caps.appl_mep_oper_state                         = MESA_CAP(VTSS_APPL_CAP_MEP_OPER_STATE);
        mep_caps.appl_mep_packet_rx_mtu                      = MESA_CAP(VTSS_APPL_CAP_MEP_PACKET_RX_MTU);
        mep_caps.appl_mep_rx_isdx_size                       = MESA_CAP(VTSS_APPL_CAP_MEP_RX_ISDX_SIZE);
        mep_caps.appl_mep_test_rate_max                      = MESA_CAP(VTSS_APPL_CAP_MEP_TEST_RATE_MAX);
    }

    if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_1) {
        return mep_g1::mep_init(data);
    } else if (mep_caps.mesa_mep_generation == MESA_MEP_GENERATION_2) {
        return mep_g2::mep_init(data);
    }

    return MESA_RC_NOT_IMPLEMENTED;
}

