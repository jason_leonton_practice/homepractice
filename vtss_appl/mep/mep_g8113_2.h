/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

#ifndef _MEP_G8113_2_H_
#define _MEP_G8113_2_H_

#include <vtss_trace_api.h>
#include "main_types.h"
#include "mep_g8113_2_bfd.h"
#include "mep_g8113_2_rt.h"

#ifndef VTSS_TRACE_MODULE_ID
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_MEP
#endif

#define OAM_TYPE_G8113_2_CC_ONLY  0x0007
#define OAM_TYPE_G8113_2_CC       0x0022
#define OAM_TYPE_G8113_2_CV       0x0023
#define OAM_TYPE_G8113_2_FM       0x0058
#define OAM_TYPE_G8113_2_RT       0x0025
#define OAM_TYPE_G8113_2_LI       0x0026
#define OAM_TYPE_G8113_2_DLM      0x000A
#define OAM_TYPE_G8113_2_ILM      0x000B
#define OAM_TYPE_G8113_2_DM       0x000C
#define OAM_TYPE_G8113_2_DLM_DM   0x000D
#define OAM_TYPE_G8113_2_ILM_DM   0x000E

#endif /* _MEP_G8113_2_H_ */
