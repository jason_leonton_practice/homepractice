/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#define MEP_IMPL_G2

#include "mep_g8113_2.h"

namespace mep_g2 {

#define min(a, b) ((a) < (b) ? (a) : (b))

static u32 timer_res = 1000; /* in ms */

static vtss_appl_mep_g8113_2_bfd_auth_conf_t bfd_auth_keys[VTSS_APPL_MEP_BFD_AUTH_KEYS_MAX];

/********************************************************************
 * MD5 calc follows (for authentication)
 ********************************************************************/

// Constants are the integer part of the sines of integers (in radians) * 2^32.
static const uint32_t k[64] = {
    0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee ,
    0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501 ,
    0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be ,
    0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821 ,
    0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa ,
    0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8 ,
    0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed ,
    0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a ,
    0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c ,
    0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70 ,
    0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05 ,
    0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665 ,
    0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039 ,
    0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1 ,
    0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1 ,
    0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391 };

// r specifies the per-round shift amounts
static const uint32_t r[] = {7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
                             5,  9, 14, 20, 5,  9, 14, 20, 5,  9, 14, 20, 5,  9, 14, 20,
                             4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
                             6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21};

// leftrotate function definition
#define LEFTROTATE(x, c) (((x) << (c)) | ((x) >> (32 - (c))))

static void to_bytes(uint32_t val, uint8_t *bytes)
{
    bytes[0] = (uint8_t) val;
    bytes[1] = (uint8_t) (val >> 8);
    bytes[2] = (uint8_t) (val >> 16);
    bytes[3] = (uint8_t) (val >> 24);
}

static uint32_t to_uint32(const uint8_t *bytes)
{
    return (uint32_t) bytes[0]
        | ((uint32_t) bytes[1] << 8)
        | ((uint32_t) bytes[2] << 16)
        | ((uint32_t) bytes[3] << 24);
}

/* assumes initial_len <= 64 */
static void md5_calc(u8 *initial_msg, u32 initial_len, u8 *digest)
{
    uint32_t h0, h1, h2, h3;
    uint8_t msg[64];
    size_t new_len, offset;
    uint32_t w[16];
    uint32_t a, b, c, d, i, f, g, temp;

    // Initialize variables - simple count in nibbles:
    h0 = 0x67452301;
    h1 = 0xefcdab89;
    h2 = 0x98badcfe;
    h3 = 0x10325476;

    // Pre-processing:
    // append "1" bit to message
    // append "0" bits until message length in bits is 448 (mod 512)
    // append length mod (2^64) to message
    new_len = 448/8;

    memcpy(msg, initial_msg, initial_len);
    msg[initial_len] = 0x80; // append the "1" bit; most significant bit is "first"
    for (offset = initial_len + 1; offset < new_len; offset++)
        msg[offset] = 0; // append "0" bits

    // append the len in bits at the end of the buffer.
    to_bytes(initial_len*8, msg + new_len);
    // initial_len>>29 == initial_len*8>>32, but avoids overflow.
    to_bytes(initial_len>>29, msg + new_len + 4);

    // message is just one 512-bit chunk:
    // break chunk into sixteen 32-bit words w[j], 0 <= j <= 15
    for (i = 0; i < 16; i++)
        w[i] = to_uint32(msg + i*4);
    // Initialize hash value for this chunk:
    a = h0;
    b = h1;
    c = h2;
    d = h3;
    // Main loop:
    for(i = 0; i<64; i++) {
        if (i < 16) {
            f = (b & c) | ((~b) & d);
            g = i;
        } else if (i < 32) {
            f = (d & b) | ((~d) & c);
            g = (5*i + 1) % 16;
        } else if (i < 48) {
            f = b ^ c ^ d;
            g = (3*i + 5) % 16;
        } else {
            f = c ^ (b | (~d));
            g = (7*i) % 16;
        }
        temp = d;
        d = c;
        c = b;
        b = b + LEFTROTATE((a + f + k[i] + w[g]), r[i]);
        a = temp;
    }
    // Add this chunk's hash to result so far:
    h0 += a;
    h1 += b;
    h2 += c;
    h3 += d;
    //var char digest[16] := h0 append h1 append h2 append h3 //(Output is in little-endian)
    to_bytes(h0, digest);
    to_bytes(h1, digest + 4);
    to_bytes(h2, digest + 8);
    to_bytes(h3, digest + 12);
}

/********************************************************************
 * SHA1 calc follows (for authentication)
 ********************************************************************/
/* assumes initial_len <= 64 */
static void sha1_calc(u8 *initial_msg, u32 initial_len, u8 *digest)
{
    uint32_t h0, h1, h2, h3, h4;
    uint8_t msg[64] = {0};
    size_t new_len;
    uint32_t w[80];
    uint32_t a, b, c, d, e, f, i, kk, temp;

    h0 = 0x67452301;
    h1 = 0xEFCDAB89;
    h2 = 0x98BADCFE;
    h3 = 0x10325476;
    h4 = 0xC3D2E1F0;

    /* Pre-processing:
       append "1" bit to message
       append "0" bits until message length in bits is 448 (mod 512)
       append length mod (2^64) to message */
    new_len = 448/8;
    memcpy(msg, initial_msg, initial_len);
    msg[initial_len] = 0x80; /* append the "1" bit; most significant bit is
                                "first" */
    /* append length of message (before pre-processing), in bits, as 64-bit
       big-endian integer: */
    msg[new_len + 4] = (8 * initial_len) >> 24;
    msg[new_len + 5] = (8 * initial_len) >> 16;
    msg[new_len + 6] = (8 * initial_len) >>  8;
    msg[new_len + 7] = 8 * initial_len;
    /* break chunk into sixteen 32-bit big-endian words w[j], 0 <= j <= 15 */
    for (i = 0; i < 16; i++)
        w[i] = ((msg[i * 4 + 0] << 24) | (msg[i * 4 + 1] << 16) |
                (msg[i * 4 + 2] <<  8) | msg[i * 4 + 3]);
    /* extend the sixteen 32-bit words into eighty 32-bit words: */
    for (i = 16; i < 80; i++) {
        temp = w[i-3] ^ w[i-8] ^ w[i-14] ^ w[i-16];
        w[i] = LEFTROTATE(temp, 1);
    }
    /* Initialize hash value for this chunk: */
    a = h0;
    b = h1;
    c = h2;
    d = h3;
    e = h4;
    /* Main loop: */
    for (i = 0; i < 80; i++) {
        if (i <= 19) {
            f = (b & c) | ((~b) & d);
            kk = 0x5A827999;
        } else if (i <= 39) {
            f = b ^ c ^ d;
            kk = 0x6ED9EBA1;
        } else if (i <= 59) {
            f = (b & c) | (b & d) | (c & d);
            kk = 0x8F1BBCDC;
        } else {
            f = b ^ c ^ d;
            kk = 0xCA62C1D6;
        }
        temp = LEFTROTATE(a, 5) + f + e + kk + w[i];
        e = d;
        d = c;
        c = LEFTROTATE(b, 30);
        b = a;
        a = temp;
    }
    h0 += a;
    h1 += b;
    h2 += c;
    h3 += d;
    h4 += e;
    digest[0]  = h0 >> 24;
    digest[1]  = h0 >> 16;
    digest[2]  = h0 >>  8;
    digest[3]  = h0;
    digest[4]  = h1 >> 24;
    digest[5]  = h1 >> 16;
    digest[6]  = h1 >>  8;
    digest[7]  = h1;
    digest[8]  = h2 >> 24;
    digest[9]  = h2 >> 16;
    digest[10] = h2 >>  8;
    digest[11] = h2;
    digest[12] = h3 >> 24;
    digest[13] = h3 >> 16;
    digest[14] = h3 >>  8;
    digest[15] = h3;
    digest[16] = h4 >> 24;
    digest[17] = h4 >> 16;
    digest[18] = h4 >>  8;
    digest[19] = h4;
}

/* Sequence number less than: */
static BOOL seq_lt(u32 seq1, u32 seq2)
{
    return (i32)(seq1 - seq2) < 0;
}

/* Sequence number less than or equal */
static BOOL seq_lte(u32 seq1, u32 seq2)
{
    return (i32)(seq1 - seq2) <= 0;
}

/* Add BFD CC/CV TLV to PDU, return number of bytes put: */
static u32 add_tlv(u8 *p, u32 offs, u8 *tlv)
{
    u16 len, i;

    len = 4 + ((tlv[2] << 8) | tlv[3]);
    for (i = 0; i < len; ++i)
        p[offs + i] = tlv[i];
    return len;
}

static void calc_auth(u8 *p, u32 len, u8 auth_type, u8 *result)
{
    switch (auth_type) {
    case VTSS_APPL_MEP_BFD_AUTH_KEYED_MD5:
    case VTSS_APPL_MEP_BFD_AUTH_METICULOUS_KEYED_MD5:
        md5_calc(p, len, result);
        break;
    case VTSS_APPL_MEP_BFD_AUTH_KEYED_SHA1:
    case VTSS_APPL_MEP_BFD_AUTH_METICULOUS_KEYED_SHA1:
        sha1_calc(p, len, result);
        break;
    case VTSS_APPL_MEP_BFD_AUTH_SIMPLE_PWD:
    default:
        break;
    }
}

static u32 build_cc_cv_pdu(u8 *p, u8 version, u8 diag, u8 sta,
                           u8 flags, u8 detect_mult, u8 len,
                           u32 my_discriminator, u32 your_discriminator,
                           u32 desired_min_tx_interval,
                           u32 required_min_rx_interval,
                           u32 required_min_echo_interval,
                           u8 *authentication,
                           u8 **tlv_list, u8 tlv_cnt)
{
    u32 i, offs;

    p[0] = (version << 5) | diag;
    p[1] = (sta << 6) | flags;
    p[2] = detect_mult;
    p[3] = len;
    p[4] = my_discriminator >> 24;
    p[5] = my_discriminator >> 16;
    p[6] = my_discriminator >> 8;
    p[7] = my_discriminator;
    p[8] = your_discriminator >> 24;
    p[9] = your_discriminator >> 16;
    p[10] = your_discriminator >> 8;
    p[11] = your_discriminator;
    p[12] = desired_min_tx_interval >> 24;
    p[13] = desired_min_tx_interval >> 16;
    p[14] = desired_min_tx_interval >> 8;
    p[15] = desired_min_tx_interval;
    p[16] = required_min_rx_interval >> 24;
    p[17] = required_min_rx_interval >> 16;
    p[18] = required_min_rx_interval >> 8;
    p[19] = required_min_rx_interval;
    p[20] = required_min_echo_interval >> 24;
    p[21] = required_min_echo_interval >> 16;
    p[22] = required_min_echo_interval >> 8;
    p[23] = required_min_echo_interval;
    offs = 24;
    if (authentication) {
        memcpy(&p[offs], authentication, authentication[1]);
        offs += authentication[1];
        calc_auth(p, offs, authentication[0], &p[32]);
    }
    for (i = 0; i < tlv_cnt; ++i)
        offs += add_tlv(p, offs, tlv_list[i]);
    return offs;
}

/* build authentication if any: */
static BOOL build_auth(vtss_mep_g8113_2_bfd_t *inst, u8 *auth)
{
    vtss_appl_mep_g8113_2_bfd_auth_conf_t *auth_key;

    if (!inst->config.tx_auth_enabled)
        return FALSE;
    auth_key = &bfd_auth_keys[inst->config.tx_auth_key_id];
    if (auth_key->key_type == VTSS_APPL_MEP_BFD_AUTH_DISABLED)
        return FALSE;

    /* build authentication to insert in packet before doing calculation: */
    memset(auth, 0, 28);
    auth[0] = auth_key->key_type;
    auth[2] = inst->config.tx_auth_key_id;
    if (auth_key->key_type == VTSS_APPL_MEP_BFD_AUTH_SIMPLE_PWD) {
        auth[1] = 3 + auth_key->key_len;
        memcpy(&auth[3], auth_key->key, auth_key->key_len);
    } else {
        auth[1] = 8 + auth_key->key_len;
        auth[3] = 0;
        auth[4] = inst->tx_auth_seq >> 24;
        auth[5] = inst->tx_auth_seq >> 16;
        auth[6] = inst->tx_auth_seq >>  8;
        auth[7] = inst->tx_auth_seq;
        memcpy(&auth[8], auth_key->key, auth_key->key_len);
    }
    return TRUE;
}

/* check auth in received packet */
static BOOL check_auth(u8 *pdu, vtss_mep_g8113_2_bfd_t *inst, u8 detect_mult)
{
    vtss_appl_mep_g8113_2_bfd_auth_conf_t *auth_key;
    u8 auth_type, auth_len, key_id, i;
    u8 digest[20], rx_digest[20];
    u32 seq_no, min_seq_no;

    if (!inst->config.rx_auth_enabled) {
        T_D("rx_auth_enabled=0 but authentication in packet -> mismatch");
        return FALSE;
    }

    auth_type = pdu[24];
    auth_len  = pdu[25];
    if (auth_type == 0 || auth_type > 5) {
        T_D("auth_type=%u -> illegal", auth_type);
        return FALSE;
    }

    key_id = pdu[26];
#if VTSS_APPL_MEP_BFD_AUTH_KEYS_MAX < 256
    if (key_id >= VTSS_APPL_MEP_BFD_AUTH_KEYS_MAX)
        return FALSE;
#endif
    auth_key = &bfd_auth_keys[key_id];
    T_D("key_id=%u key_type=%u", key_id, auth_key->key_type);
    if (auth_key->key_type != auth_type) {
        if ((auth_type == VTSS_APPL_MEP_BFD_AUTH_KEYED_MD5 ||
             auth_type == VTSS_APPL_MEP_BFD_AUTH_METICULOUS_KEYED_MD5) &&
            auth_key->key_type != VTSS_APPL_MEP_BFD_AUTH_KEYED_MD5 &&
            auth_key->key_type != VTSS_APPL_MEP_BFD_AUTH_METICULOUS_KEYED_MD5)
            return FALSE;
        if ((auth_type == VTSS_APPL_MEP_BFD_AUTH_KEYED_SHA1 ||
             auth_type == VTSS_APPL_MEP_BFD_AUTH_METICULOUS_KEYED_SHA1) &&
            auth_key->key_type != VTSS_APPL_MEP_BFD_AUTH_KEYED_SHA1 &&
            auth_key->key_type != VTSS_APPL_MEP_BFD_AUTH_METICULOUS_KEYED_SHA1)
            return FALSE;
    }

    if (auth_type == VTSS_APPL_MEP_BFD_AUTH_SIMPLE_PWD) {
        if (auth_len != (auth_key->key_len + 3)) {
            T_D("auth_len=%u != %u -> mismatch", auth_len, auth_key->key_len + 3);
            return FALSE;
        }
        if (memcmp(&pdu[27], auth_key->key, auth_key->key_len) != 0) {
            for (i = 0; i < auth_key->key_len; ++i)
                T_D("SIMPLE_PWD compare failure i=%u %02x %02x %s",
                    i, pdu[27 + i], auth_key->key[i], pdu[27 + i] != auth_key->key[i] ? "!!" : "");
            return FALSE;
        }
    } else {
        if (auth_type == VTSS_APPL_MEP_BFD_AUTH_KEYED_MD5 ||
            auth_type == VTSS_APPL_MEP_BFD_AUTH_METICULOUS_KEYED_MD5) {
            if (auth_len != 24 || auth_len != (auth_key->key_len + 8)) {
                T_D("auth_len=%u != 24 or %u -> mismatch", auth_len, auth_key->key_len + 8);
                return FALSE;
            }
        } else if (auth_type == VTSS_APPL_MEP_BFD_AUTH_KEYED_SHA1 ||
                   auth_type == VTSS_APPL_MEP_BFD_AUTH_METICULOUS_KEYED_SHA1) {
            if (auth_len != 28 || auth_len != (auth_key->key_len + 8)) {
                T_D("auth_len=%u != 28 or %u -> mismatch", auth_len, auth_key->key_len + 8);
                return FALSE;
            }
        }
        seq_no = ((pdu[28] << 24) | (pdu[29] << 16) | (pdu[30] <<  8) | pdu[31]);
        if (inst->rx_auth_seq_known) {
            if (auth_type == VTSS_APPL_MEP_BFD_AUTH_METICULOUS_KEYED_MD5 ||
                auth_type == VTSS_APPL_MEP_BFD_AUTH_METICULOUS_KEYED_SHA1)
                min_seq_no = inst->rx_auth_seq + 1;
            else
                min_seq_no = inst->rx_auth_seq;
            /* min_seq_no <= seq_no <= (min_seq_no + 3 * detect_mult) */
            if (seq_lt(seq_no, min_seq_no) || !seq_lte(seq_no, min_seq_no + 3 * detect_mult)) {
                T_D("%p seq_no=%u min_seq_no=%u -> mismatch", inst, seq_no, min_seq_no);
                return FALSE;
            }
        } else
            inst->rx_auth_seq_known = 1;
        inst->rx_auth_seq = seq_no;
        for (i = 0; i < auth_key->key_len; ++i) {
            rx_digest[i] = pdu[32 + i];
            pdu[32 + i] = auth_key->key[i];
        }
        calc_auth(pdu, 24 + auth_len, auth_type, digest);
        if (memcmp(digest, rx_digest, auth_key->key_len) != 0) {
            for (i = 0; i < auth_key->key_len; ++i)
                T_D("auth_type=%u digest mismatch i=%u %02x %02x %s",
                    auth_type, i, digest[i], rx_digest[i], digest[i] != rx_digest[i] ? "!!" : "");
            return FALSE;
        }
    }
    return TRUE;
}

BOOL vtss_mep_g8113_2_bfd_auth_config(const u8 key_id,
                                      const vtss_appl_mep_g8113_2_bfd_auth_conf_t *const config)
{
#if VTSS_APPL_MEP_BFD_AUTH_KEYS_MAX < 256
    if (key_id >= VTSS_APPL_MEP_BFD_AUTH_KEYS_MAX)
        return FALSE;
#endif
    bfd_auth_keys[key_id] = *config;
    return TRUE;
}

BOOL vtss_mep_g8113_2_bfd_auth_config_get(const u8 key_id,
                                          vtss_appl_mep_g8113_2_bfd_auth_conf_t *const config)
{
#if VTSS_APPL_MEP_BFD_AUTH_KEYS_MAX < 256
    if (key_id >= VTSS_APPL_MEP_BFD_AUTH_KEYS_MAX)
        return FALSE;
#endif
    *config = bfd_auth_keys[key_id];
    return TRUE;
}

BOOL vtss_mep_g8113_2_bfd_config(vtss_mep_g8113_2_bfd_t *inst,
                                 const vtss_appl_mep_g8113_2_bfd_conf_t *const config)
{
    if (!inst->config.enable && config->enable) {
        /* enable BFD CC/CV */
        if (inst->session_state[0] != VTSS_APPL_MEP_BFD_STATE_ADMIN_DOWN)
            return FALSE;
        T_D("enable BFD CC/CV, cc_period=%u, cc_only=%u", config->cc_only, config->cc_period);
        inst->miss_cnt             = 4;
        inst->dMMG_cnt             = 4;
        inst->cc_tx_cnt            = 0;
        inst->valid_cc_rx_cnt      = 0;
        inst->invalid_cc_rx_cnt    = 0;
        inst->cv_tx_cnt            = 0;
        inst->valid_cv_rx_cnt      = 0;
        inst->invalid_cv_rx_cnt    = 0;
        inst->admin_down_cnt       = 3; // no Tx on 0
        inst->session_state[0]     = VTSS_APPL_MEP_BFD_STATE_DOWN;
        inst->session_state[1]     = VTSS_APPL_MEP_BFD_STATE_DOWN;
        T_D("%p enable -> VTSS_APPL_MEP_BFD_STATE_DOWN", inst);
        inst->local_diag           = VTSS_APPL_MEP_BFD_DIAG_NONE;
        inst->poll                 = 0;
        inst->final                = 0;
        inst->remote_discr[0]      = 0;
        inst->remote_discr[1]      = 0;
        inst->remote_min_rx        = 1;
        inst->remote_session_state = VTSS_APPL_MEP_BFD_STATE_DOWN;
        inst->remote_diag          = VTSS_APPL_MEP_BFD_DIAG_NONE;
        inst->remote_flags         = 0;
        inst->rx_auth_seq_known    = 0;
        inst->tx_auth_seq          = rand();
        inst->tx_cc_timer[0]       = 1000 / timer_res; /* initially 1 sec */
        if (config->is_independent)
            inst->tx_cc_timer[1]   = 1000 / timer_res; /* initially 1 sec */
        else
            inst->tx_cc_timer[1]   = 0;
        T_D("is_independent:%u tx_cc_timer:%u %u", config->is_independent, inst->tx_cc_timer[0], inst->tx_cc_timer[1]);
        do {
            inst->my_discr[0] = rand();
        } while (inst->my_discr[0] == 0); /* must be non-zero */
        if (config->is_independent) {
            do {
                inst->my_discr[1] = rand();
            } while (inst->my_discr[1] == 0 ||
                     inst->my_discr[0] == inst->my_discr[1]); /* must be different and non-zero */
        } else {
            inst->my_discr[1] = inst->my_discr[0];
        }
        if (config->cc_only)
            inst->tx_cv_timer = 0;
        else
            inst->tx_cv_timer = 1000 / timer_res; /* always 1 sec */
        inst->tx_cc_ongoing = FALSE;
        inst->tx_cv_ongoing = FALSE;
    } else if (inst->config.enable && !config->enable) {
        T_D("disable BFD CC/CV, cc_period=%u, cc_only=%u", inst->config.cc_only, inst->config.cc_period);
        T_D("%p -> VTSS_APPL_MEP_BFD_STATE_ADMIN_DOWN", inst);
        inst->session_state[0]  = VTSS_APPL_MEP_BFD_STATE_ADMIN_DOWN;
        inst->session_state[1]  = VTSS_APPL_MEP_BFD_STATE_ADMIN_DOWN;
        inst->local_diag        = VTSS_APPL_MEP_BFD_DIAG_ADMIN_DOWN;
        inst->miss_cnt          = 0;
        inst->dMMG_cnt          = 0;
        inst->rx_auth_seq_known = 0;
    } else
        return FALSE;
    inst->config = *config;
    return TRUE;
}

/* check if tx_auth_seq needs increment: */
static void check_incr_tx_auth(vtss_mep_g8113_2_bfd_t *inst, u8 *auth)
{
    if (!auth)
        return;
    if (auth[0] == VTSS_APPL_MEP_BFD_AUTH_METICULOUS_KEYED_MD5  ||
        auth[0] == VTSS_APPL_MEP_BFD_AUTH_METICULOUS_KEYED_SHA1 ||
        auth[0] == VTSS_APPL_MEP_BFD_AUTH_KEYED_MD5             ||
        auth[0] == VTSS_APPL_MEP_BFD_AUTH_KEYED_SHA1)
        /* always increment for each CC/CV sent */
        inst->tx_auth_seq++;
}

static void vtss_mep_g8113_2_bfd_rx_timeout(vtss_mep_g8113_2_bfd_t *inst, BOOL *dLOC)
{
    T_D("session_state=%u", inst->session_state[0]);

    if (*dLOC && inst->session_state[0] == VTSS_APPL_MEP_BFD_STATE_DOWN) {
        inst->rx_auth_seq_known = 0;
        inst->poll              = 0;
    } else if (inst->local_diag != VTSS_APPL_MEP_BFD_DIAG_PATH_DOWN) {
        T_D("%p dLOC=1 -> VTSS_APPL_MEP_BFD_STATE_DOWN", inst);
        *dLOC = TRUE;
        inst->session_state[0]  = VTSS_APPL_MEP_BFD_STATE_DOWN;
        inst->local_diag        = VTSS_APPL_MEP_BFD_DIAG_DETECT_EXPIRED;
        inst->rx_auth_seq_known = 0;
        inst->poll              = 0;
    }
}

static void vtss_mep_g8113_2_bfd_dMMG_timeout(vtss_mep_g8113_2_bfd_t *inst, BOOL *dMMG)
{
    T_D("session_state=%u", inst->session_state[0]);

    *dMMG = FALSE;
}

/* BFD CC Tx timeout: */
void vtss_mep_g8113_2_bfd_cc_tx_timeout(vtss_mep_g8113_2_bfd_t *inst, u8 sink,
                                        u8 *pdu, u32 *len, BOOL *dLOC)
{
    const u8 version = 1;
    const u8 detect_mult = 3;
    u8 flags = 0;
    u8 pdu_len = 24;
    u8 *auth = NULL, buf[28];
    u32 min_tx, min_rx, val;

    T_D("session_state=%u admin_down_cnt=%u poll=%u final=%u sink=%u",
        inst->session_state[0], inst->admin_down_cnt, inst->poll, inst->final, sink);

    if (inst->session_state[0] == VTSS_APPL_MEP_BFD_STATE_ADMIN_DOWN) {
        if (inst->admin_down_cnt > 0)
            inst->admin_down_cnt--;
    } else
        inst->admin_down_cnt = 3;

    if (build_auth(inst, buf)) {
        /* authentication: */
        flags |= (1 << 2);
        auth = buf;
        pdu_len += auth[1];
    }

    if (sink) {
        min_tx = 0;
        min_rx = 1000000;
    } else {
        flags |= (inst->final << 4);
        if (!inst->final && inst->session_state[0] == VTSS_APPL_MEP_BFD_STATE_UP)
            flags |= (inst->poll << 5);
        if (inst->session_state[0] == VTSS_APPL_MEP_BFD_STATE_UP)
            min_tx = inst->config.cc_period;
        else
            min_tx = 1000000;
        min_rx = min_tx;
    }
    *len = build_cc_cv_pdu(pdu, version, inst->local_diag, inst->session_state[0],
                           flags, detect_mult, pdu_len,
                           inst->my_discr[sink], inst->remote_discr[sink],
                           min_tx, min_rx, 0, auth, NULL, 0);
    if (inst->admin_down_cnt < 3 && inst->config.is_independent && sink)
        inst->tx_cc_timer[sink] = 0;
    else if (inst->admin_down_cnt > 0) {
        if (inst->session_state[0] == VTSS_APPL_MEP_BFD_STATE_UP) {
            // HW Tx will take over incl. jitter:
            if (inst->poll || (inst->config.is_independent && sink))
                /* AFI restart Tx timer - 1 sec (jitter by HW): */
                val = 1000000;
            else
                /* AFI restart Tx timer - cc_period (jitter by HW): */
                val = inst->config.cc_period;
        } else {
            /* restart Tx timer - jitter 75% - 100% of 1 sec: */
            val = 1000000 - (rand() % 250000);
        }
        inst->tx_cc_timer[sink] = val / (1000 * timer_res);
        if (inst->tx_cc_timer[sink] == 0)
            inst->tx_cc_timer[sink] = 1;
        T_D("restart cc_timer val=%u", val);
    } else {
        inst->tx_cc_timer[0] = 0;
        inst->tx_cc_timer[1] = 0;
    }
    if (inst->final) inst->final = 0;
    inst->cc_tx_cnt++;
    T_D("cc_tx_cnt:" VPRI64u " cc_rx_cnt:" VPRI64u " state:%u remote_state:%u poll:%u(%u) final:%u min_tx:%u admin_down_cnt:%u",
        inst->cc_tx_cnt, inst->valid_cc_rx_cnt, inst->session_state[0], inst->remote_session_state, (flags >> 5) & 1, inst->poll, (flags >> 4) & 1, min_tx, inst->admin_down_cnt);
    check_incr_tx_auth(inst, auth);
    if (sink == 0 && inst->miss_cnt) {
        inst->miss_cnt--;
        if (!inst->miss_cnt) {
            vtss_mep_g8113_2_bfd_rx_timeout(inst, dLOC);
        }
    }
}

/* BFD CV Tx timeout: */
void vtss_mep_g8113_2_bfd_cv_tx_timeout(vtss_mep_g8113_2_bfd_t *inst,
                                        u8 *pdu, u32 *len, BOOL *dMMG)
{
    const u8 version = 1;
    const u8 detect_mult = 3;
    const u32 min_tx = 1000000;
    u8 *tlv_list[1];
    u8 tlv_cnt = 0;
    u8 flags = 0;
    u8 pdu_len = 24;
    u8 *auth = NULL, buf[28];
    u32 val;

    T_D("session_state=%u admin_down_cnt=%u poll=%u",
        inst->session_state[0], inst->admin_down_cnt, inst->poll);

    /* source MEP TLV:: */
    tlv_list[tlv_cnt++] = inst->tx_src_mep_id_tlv;

    if (build_auth(inst, buf)) {
        /* authentication: */
        flags |= (1 << 2);
        auth = buf;
        pdu_len += auth[1];
    }

    *len = build_cc_cv_pdu(pdu, version, inst->local_diag, inst->session_state[0],
                           flags, detect_mult, pdu_len,
                           inst->my_discr[0], inst->remote_discr[0],
                           min_tx, min_tx, 0, auth, tlv_list, tlv_cnt);
    if (inst->admin_down_cnt > 0) {
        if (inst->session_state[0] == VTSS_APPL_MEP_BFD_STATE_UP) {
            // HW Tx will take over incl. jitter:
            /* restart Tx timer - 1 sec (jitter by HW): */
            val = 1000;
        } else {
            /* restart Tx timer - jitter 75% - 100% of 1 sec: */
            val = 1000 - (rand() % 250);
        }
        inst->tx_cv_timer = val / timer_res;
        T_D("restart cv_timer val=%u", val);
    } else
        inst->tx_cv_timer = 0;
    inst->cv_tx_cnt++;
    T_D("cv_tx_cnt:" VPRI64u " cv_rx_cnt:" VPRI64u " state:%u remote_state:%u poll:%u(%u) final:%u admin_down_cnt:%u",
        inst->cv_tx_cnt, inst->valid_cv_rx_cnt, inst->session_state[0], inst->remote_session_state, (flags >> 5) & 1, inst->poll, (flags >> 4) & 1, inst->admin_down_cnt);
    check_incr_tx_auth(inst, auth);
    if (inst->dMMG_cnt) {
        inst->dMMG_cnt--;
        if (!inst->dMMG_cnt)
            vtss_mep_g8113_2_bfd_dMMG_timeout(inst, dMMG);
    }
}

/* pdu points just after ACH */
void vtss_mep_g8113_2_bfd_rx(vtss_mep_g8113_2_bfd_t *inst,
                             u16 ach_opcode, u8 *pdu, u32 len,
                             BOOL *dLOC, BOOL *dMMG, BOOL *dRDI, BOOL *dUNP)
{
    u8 ver, diag, sta, flags, detect_mult, pdu_len, exp_len, sel, sink;
    u16 tlv_type, tlv_len, exp_tlv_type, exp_tlv_len;
    u32 my_discr, your_discr, min_tx, min_rx, i;
    u16 miss_cnt_val = 4;

    T_D("enter");

    if (!inst->config.enable) {
        T_D("BFD not enabled -> discard");
        return;
    }

    if (inst->config.cc_only && ach_opcode == OAM_TYPE_G8113_2_CV) {
        T_D("cc_only, but ach_opcode=0x%04x -> discard", ach_opcode);
        return;
    }
    if (!inst->config.cc_only && ach_opcode == OAM_TYPE_G8113_2_CC_ONLY) {
        T_D("!cc_only, but ach_opcode=0x%04x -> discard", ach_opcode);
        return;
    }
    if (ach_opcode != OAM_TYPE_G8113_2_CC_ONLY &&
        ach_opcode != OAM_TYPE_G8113_2_CC &&
        ach_opcode != OAM_TYPE_G8113_2_CV) {
        T_D("ach_opcode=0x%04x -> discard", ach_opcode);
        return;
    }

    /* read pdu fields: */
    ver         = pdu[0] >> 5;
    diag        = pdu[0] & 0x1f;
    sta         = pdu[1] >> 6;
    flags       = pdu[1] & 0x3f;
    detect_mult = pdu[2];
    pdu_len     = pdu[3];
    my_discr    = ((pdu[4] << 24) | (pdu[5] << 16) |
                   (pdu[6] <<  8) |  pdu[7]);
    your_discr  = ((pdu[8] << 24) | (pdu[9] << 16) |
                   (pdu[10] << 8) |  pdu[11]);
    min_tx      = ((pdu[12] << 24) | (pdu[13] << 16) |
                   (pdu[14] <<  8) |  pdu[15]);
    min_rx      = ((pdu[16] << 24) | (pdu[17] << 16) |
                   (pdu[18] <<  8) |  pdu[19]);
    sel = inst->config.is_independent && min_tx == 0 ? 1 : 0;
    // sel == 1 means "received from remote sink (we are source then)"
    // sel == 0 means "received from remote source (we are sink then - or source if not independent mode)"
    sink = inst->config.is_independent ? sel ^ 1 : 0;
    if (flags & (1 << 2)) {
        if (len < 26) { /* auth type and len */
            T_D("pdu len %u < 28 -> discard", len);
            goto rx_invalid;
        }
        exp_len = 24 + pdu[25];
    } else
        exp_len = 24;
    /* pdu validation: */
    if (ver != 1) {
        T_D("ver=%u != 1 -> discard", ver);
        goto rx_invalid;
    }
    if (pdu_len < exp_len || pdu_len > len) {
        T_D("pdu_len=%u, exp_len=%u, max_len=%u -> discard", pdu_len, exp_len, len);
        goto rx_invalid;
    }
    if (detect_mult == 0) {
        T_D("detect_mult=0 -> discard");
        goto rx_invalid;
    }
    if ((flags & 1) || ((flags >> 4) == 3)) {
        T_D("flags=0x%02x -> discard", flags);
        goto rx_invalid;
    }
    if (my_discr == 0) {
        T_D("my_discr=%u -> discard", my_discr);
        goto rx_invalid;
    }
    if (your_discr == 0 && sta != VTSS_APPL_MEP_BFD_STATE_DOWN && sta != VTSS_APPL_MEP_BFD_STATE_ADMIN_DOWN) {
        T_D("your_discr=%u, sta=%u -> discard", your_discr, sta);
        goto rx_invalid;
    }
    if (!inst->config.is_independent && min_tx == 0) {
        T_D("min_tx=0 is reserved -> discard");
        goto rx_invalid;
    }
    if (ach_opcode != OAM_TYPE_G8113_2_CV) {
        if (sta == VTSS_APPL_MEP_BFD_STATE_UP && inst->session_state[sel] == VTSS_APPL_MEP_BFD_STATE_UP &&
            my_discr != inst->remote_discr[sink]) {
            T_D("both ends UP but my_discr changed -> discard");
            goto rx_invalid;
        }
        if (sta != VTSS_APPL_MEP_BFD_STATE_UP && inst->poll && min_rx != 1000000) {
            T_D("sta=%u min_rx=%u -> discard", sta, min_rx);
            goto rx_invalid;
        }
    }

    /* authentication check: */
    if (flags & (1 << 2)) { /* authentication */
        if (!check_auth(pdu, inst, detect_mult)) {
            T_D("auth validation failed -> discard");
            if (sel == 0) *dMMG = TRUE;
            goto rx_invalid_dMMG;
        }
    } else if (inst->config.rx_auth_enabled) {
        /* expected authentication, but none in pdu -> discard */
        T_D("rx_auth_enabled but no authentication in packet -> discard");
        if (sel == 0) *dMMG = TRUE;
        goto rx_invalid_dMMG;
    }

    /* further check for dMMG: */
    if (your_discr != 0 && your_discr != inst->my_discr[sink]) {
        T_D("dMMG=1 (1)");
        if (sel == 0) *dMMG = TRUE;
        inst->unexp_discr = your_discr;
        goto rx_invalid_dMMG;
    }
    if (ach_opcode == OAM_TYPE_G8113_2_CV) {
        /* check BFD CV source MEP-ID TLV: */
        if ((len - pdu_len) < 4) {
            T_D("dMMG=1 (len=%u pdu_len=%u) -> discard", len, pdu_len);
            if (sel == 0) *dMMG = TRUE;
            goto rx_invalid_dMMG;
        } else {
            tlv_type = (pdu[pdu_len] << 8) | pdu[pdu_len + 1];
            tlv_len  = (pdu[pdu_len + 2] << 8) | pdu[pdu_len + 3];
            exp_tlv_type = (inst->rx_src_mep_id_tlv[0] << 8) | inst->rx_src_mep_id_tlv[1];
            exp_tlv_len  = (inst->rx_src_mep_id_tlv[2] << 8) | inst->rx_src_mep_id_tlv[3];
            if (tlv_type != exp_tlv_type || tlv_len != exp_tlv_len) {
                T_D("dMMG=1 (tlv_type=%x/%x, tlv_len=%x/%x) -> discard", tlv_type, exp_tlv_type, tlv_len, exp_tlv_len);
                if (sel == 0) *dMMG = TRUE;
                memcpy(inst->unexp_rx_src_mep_id_tlv, &pdu[pdu_len], min(tlv_len, 24));
                goto rx_invalid_dMMG;
            } else {
                for (i = 0; i < tlv_len; ++i) {
                    if (pdu[pdu_len + 4 + i] != inst->rx_src_mep_id_tlv[4 + i]) {
                        T_D("dMMG=1 (tlv_data i=%u %02x != %02x) -> discard", i, pdu[pdu_len + 4 + i], inst->rx_src_mep_id_tlv[4 + i]);
                        if (sel == 0) *dMMG = TRUE;
                        memcpy(inst->unexp_rx_src_mep_id_tlv, &pdu[pdu_len], 4 + tlv_len);
                        goto rx_invalid_dMMG;
                    }
                }
            }
        }
    }

    inst->remote_session_state = sta;
    inst->remote_diag          = diag;
    inst->remote_flags         = flags;
    if (ach_opcode != OAM_TYPE_G8113_2_CV)
        if (!inst->config.is_independent || sink)
            /* coordinated mode or independent mode sink Rx: */
            inst->remote_min_rx = min_rx;

    if (inst->session_state[sel] == VTSS_APPL_MEP_BFD_STATE_ADMIN_DOWN) {
        T_D("session_state is ADMIN_DOWN -> discard");
        goto rx_invalid;
    }
    if (ach_opcode != OAM_TYPE_G8113_2_CV && sta == VTSS_APPL_MEP_BFD_STATE_UP &&
        inst->poll) {
        if (flags & (1 << 4)) {
            T_D("sta UP, poll==1 and received final flag, clear poll");
            if (inst->config.cc_period < 1000000) {
                miss_cnt_val = 1 + 3 * (1000000 / inst->config.cc_period);
            }
            inst->poll = 0;
        }
        if ((flags & (3 << 4)) == 0) {
            T_D("sta UP, poll==1 and received poll=final=0 flag, clear poll");
            if (inst->config.cc_period < 1000000) {
                miss_cnt_val = 1 + 3 * (1000000 / inst->config.cc_period);
            }
            inst->poll = 0;
        }
    }
    if (sta == VTSS_APPL_MEP_BFD_STATE_ADMIN_DOWN &&
        ach_opcode != OAM_TYPE_G8113_2_CV) {
        inst->local_diag         = VTSS_APPL_MEP_BFD_DIAG_REMOTE_DOWN;
        inst->session_state[sel] = VTSS_APPL_MEP_BFD_STATE_DOWN;
        inst->remote_discr[sink] = 0;
        T_D("%p remote down -> VTSS_APPL_MEP_BFD_STATE_DOWN", inst);
    } else if (ach_opcode != OAM_TYPE_G8113_2_CV) {
        inst->remote_discr[sink] = my_discr;
        if (inst->session_state[sel] == VTSS_APPL_MEP_BFD_STATE_DOWN && !*dMMG) {
            if (sta == VTSS_APPL_MEP_BFD_STATE_DOWN) {
                inst->session_state[sel] = VTSS_APPL_MEP_BFD_STATE_INIT;
                if (inst->remote_diag == VTSS_APPL_MEP_BFD_DIAG_NONE)
                    inst->local_diag = VTSS_APPL_MEP_BFD_DIAG_NONE;
                T_D("%p -> VTSS_APPL_MEP_BFD_STATE_INIT", inst);
            } else if (sta == VTSS_APPL_MEP_BFD_STATE_INIT) {
                inst->session_state[sel] = VTSS_APPL_MEP_BFD_STATE_UP;
                inst->local_diag = VTSS_APPL_MEP_BFD_DIAG_NONE;
                T_D("%p -> VTSS_APPL_MEP_BFD_STATE_UP", inst);
                inst->poll = 1;
            }
            if (sink && sta == VTSS_APPL_MEP_BFD_STATE_UP) {
                /* independent mode sink Rx of UP leads state to UP: */
                inst->session_state[sel] = VTSS_APPL_MEP_BFD_STATE_UP;
                inst->local_diag = VTSS_APPL_MEP_BFD_DIAG_NONE;
                T_D("%p -> VTSS_APPL_MEP_BFD_STATE_UP", inst);
                inst->poll = 1;
            }
        } else if (inst->session_state[sel] == VTSS_APPL_MEP_BFD_STATE_INIT) {
            if (sta == VTSS_APPL_MEP_BFD_STATE_INIT || sta == VTSS_APPL_MEP_BFD_STATE_UP) {
                inst->session_state[sel] = VTSS_APPL_MEP_BFD_STATE_UP;
                inst->local_diag = VTSS_APPL_MEP_BFD_DIAG_NONE;
                T_D("%p -> VTSS_APPL_MEP_BFD_STATE_UP", inst);
                inst->poll = 1;
            }
        }
        if (inst->session_state[sel] == VTSS_APPL_MEP_BFD_STATE_UP) {
            if (inst->config.is_independent && sink == 0) {
                /* independent mode source stays in up, only admin down can bring it down */
            } else if (sta == VTSS_APPL_MEP_BFD_STATE_DOWN) {
                inst->session_state[sel] = VTSS_APPL_MEP_BFD_STATE_DOWN;
                inst->local_diag    = VTSS_APPL_MEP_BFD_DIAG_REMOTE_DOWN;
                T_D("%p -> VTSS_APPL_MEP_BFD_STATE_DOWN", inst);
            }
        }
    }

    if (inst->poll && sink == 0 && inst->session_state[sel] != VTSS_APPL_MEP_BFD_STATE_UP) {
        inst->poll = 0;
    }

    if (inst->session_state[sel] == VTSS_APPL_MEP_BFD_STATE_UP)
        *dLOC = FALSE;

    if (ach_opcode == OAM_TYPE_G8113_2_CV)
        inst->valid_cv_rx_cnt++;
    else {
        inst->valid_cc_rx_cnt++;
        if (sta == VTSS_APPL_MEP_BFD_STATE_UP ||
            (diag != VTSS_APPL_MEP_BFD_DIAG_DETECT_EXPIRED && diag != VTSS_APPL_MEP_BFD_DIAG_PATH_DOWN && diag != VTSS_APPL_MEP_BFD_DIAG_MISCONNECTIVITY))
            *dRDI = FALSE;
        else if (sta != VTSS_APPL_MEP_BFD_STATE_UP &&
                 (diag == VTSS_APPL_MEP_BFD_DIAG_DETECT_EXPIRED || diag == VTSS_APPL_MEP_BFD_DIAG_PATH_DOWN || diag == VTSS_APPL_MEP_BFD_DIAG_MISCONNECTIVITY))
            *dRDI = TRUE;
        if (inst->session_state[sel] == VTSS_APPL_MEP_BFD_STATE_UP && (!inst->config.is_independent || sink)) {
            /* UP + coordinated mode or independent mode sink Rx: */
            if (min_tx == inst->config.cc_period && min_rx <= inst->config.cc_period)
                *dUNP = FALSE;
            else if (((flags & 0x30) || /* poll or final bit set */
                      sta == VTSS_APPL_MEP_BFD_STATE_UP) &&
                     (min_tx != inst->config.cc_period || min_rx > inst->config.cc_period)) {
                *dUNP = TRUE;
                inst->session_state[sel] = VTSS_APPL_MEP_BFD_STATE_DOWN;
                inst->local_diag    = VTSS_APPL_MEP_BFD_DIAG_PATH_DOWN;
                T_D("%p -> VTSS_APPL_MEP_BFD_STATE_DOWN (dUNP)", inst);
            }
        }
        if ((flags & (1 << 5)) && // Rx poll=1
            sta == VTSS_APPL_MEP_BFD_STATE_UP &&
            inst->session_state[sel] == VTSS_APPL_MEP_BFD_STATE_UP) {
            inst->final = 1; /* request Tx BFD CC packet with final=1 */
            inst->poll  = 0;
        }
        if (sta == VTSS_APPL_MEP_BFD_STATE_ADMIN_DOWN)
            /* set dLOC - we know why the other end is not transmitting */
            *dLOC = TRUE;
        else if (sink == 0 && inst->miss_cnt < 4) {
            inst->miss_cnt = miss_cnt_val;
        }
    }
    return;

 rx_invalid_dMMG:
    if (*dMMG) {
        inst->dMMG_cnt = 4;
        inst->rx_auth_seq_known = 0;
        inst->local_diag = VTSS_APPL_MEP_BFD_DIAG_MISCONNECTIVITY;
        if (inst->session_state[sel] == VTSS_APPL_MEP_BFD_STATE_UP &&
            inst->config.is_independent && sink == 0) {
            /* independent mode source stays in up, only admin down can bring it down */
        } else {
            inst->session_state[sel] = VTSS_APPL_MEP_BFD_STATE_DOWN;
            inst->local_diag    = VTSS_APPL_MEP_BFD_DIAG_MISCONNECTIVITY;
            T_D("%p -> VTSS_APPL_MEP_BFD_STATE_DOWN (dMMG)", inst);
        }
    }
 rx_invalid:
    if (ach_opcode == OAM_TYPE_G8113_2_CV)
        inst->invalid_cv_rx_cnt++;
    else
        inst->invalid_cc_rx_cnt++;
}

void vtss_mep_g8113_2_bfd_init(const u32 timer_resolution) /* in ms */
{
    timer_res = timer_resolution;
}

}  // mep_g2

