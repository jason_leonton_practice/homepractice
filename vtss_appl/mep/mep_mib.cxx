
/*
 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
*/

#include "mep_serializer.hxx"
#include "vtss/appl/mep.h"
#include "mep_api.h"

VTSS_MIB_MODULE("MepMib", "MEP", mep_mib_init, VTSS_MODULE_ID_MEP, root, h) {
    h.add_history_element("201407010000Z", "Initial version");
    h.add_history_element("201409030000Z", "Added vtssMepConfigClientFlowDomain and vtssMepConfigClientFlowFlowId to vtssMepConfigClientFlowTable");
    h.add_history_element("201409300000Z", "Added vtssMepConfigRt and vtssMepStatusRt for Route Trace configuration and status");
    h.add_history_element("201411140000Z", "Refactoring regarding Ifindex. Domain element is removed and flow element is changed to be Ifindex containing domain");
    h.add_history_element("201411260000Z", "Adding Loss Measurement OAM frame counting object");
    h.add_history_element("201412030000Z", "Adding Link State Tracking");
    h.add_history_element("201501020000Z", "Removed DEI");
    h.add_history_element("201501190000Z", "Added Loss Measurement Availability, High Loss Interval and Signal Degrade");
    h.add_history_element("201508060000Z", "Added FD and IFDV bins for DM");
    h.add_history_element("201508130000Z", "Added Operational State and more capabilities");
    h.add_history_element("201508190000Z", "Bugzila 19123 fixed, by updating FD and IFDV get status function");
    h.add_history_element("201509230000Z", "Changed type and description for vtssMepStatusLmNearEndLosCounterInt and vtssMepStatusLmFarEndLosCounterInt");
    h.add_history_element("201606200000Z", "Added capability element for OAM SW support");
    h.add_history_element("201705190000Z", "Added LM Availability sliding window counters (NearEndWindowCnt and FarEndWindowCnt) to mepStatusLmAvailTable.");
    h.add_history_element("201705220000Z", "Changed Mep instance ID numbering convention to start from 1 instead of from 0.");
    h.add_history_element("201706270000Z", "Added option to view DM FD and IFDV bin values in StatusDmFdBinsTable and StatusDmIfdvBinsTable.");
    h.add_history_element("201707070000Z", "Added support for management of Synthetic Loss Measurement (SLM) function. "
                                           "Added MepStatusLmPeerTable to provide Loss Measurement status for multiple peer MEPs. "
                                           "Added Peer Id index to MepStatusLmAvailTable and MepStatusLmHliTable to support multiple peers for SLM. "
                                           "Added ElapsedIntervals object to MepStatusLmTable. ");
    h.add_history_element("201711150000Z", "Added MyDiscr1 and RemoteDiscr1 objects to MepStatusBfdTable.");

    h.description("This is a private service OAM (Y.1731/G802.1ag/G8113.1/G8113.2) MIB");
}
#define NS(VAR, P, ID, NAME) static NamespaceNode VAR(&P, OidElement(ID, NAME))

using namespace vtss;
using namespace expose::snmp;

namespace vtss {
namespace appl {
namespace mep {
namespace interfaces {

NS(mep, root, 1, "mepMibObjects");

NS(mep_config, mep, 2, "mepConfig");
NS(mep_status, mep, 3, "mepStatus");
NS(mep_control, mep, 4, "mepControl");

NS(mep_config_instance, mep_config, 1, "mepConfigInstance");
NS(mep_config_pm, mep_config, 2, "mepConfigPm");
NS(mep_config_cc, mep_config, 3, "mepConfigCc");
NS(mep_config_lm, mep_config, 4, "mepConfigLm");
NS(mep_config_dm, mep_config, 5, "mepConfigDm");
NS(mep_config_lb, mep_config, 6, "mepConfigLb");
NS(mep_config_tst, mep_config, 7, "mepConfigTst");
NS(mep_config_lt, mep_config, 8, "mepConfigLt");
NS(mep_config_aps, mep_config, 9, "mepConfigAps");
NS(mep_config_ais, mep_config, 10, "mepConfigAis");
NS(mep_config_lck, mep_config, 11, "mepConfigLck");
NS(mep_config_client, mep_config, 12, "mepConfigClient");
NS(mep_config_syslog, mep_config, 13, "mepConfigSyslog");
NS(mep_config_tlv, mep_config, 14, "mepConfigTlv");
NS(mep_config_bfd, mep_config, 15, "mepConfigBfd");
NS(mep_config_bfd_auth, mep_config, 16, "mepConfigBfdAuth");
NS(mep_config_rt, mep_config, 17, "mepConfigRt");
NS(mep_config_lst, mep_config, 18, "mepConfigLst");
NS(mep_config_lm_avail, mep_config, 19, "mepConfigLmAvail");
NS(mep_config_lm_hli, mep_config, 20, "mepConfigLmHli");
NS(mep_config_lm_sdeg, mep_config, 21, "mepConfigLmSdeg");

NS(mep_status_instance, mep_status, 1, "mepStatusInstance");
NS(mep_status_lm, mep_status, 2, "mepStatusLm");
NS(mep_status_dm, mep_status, 3, "mepStatusDm");
NS(mep_status_lb, mep_status, 4, "mepStatusLb");
NS(mep_status_tst, mep_status, 5, "mepStatusTst");
NS(mep_status_lt, mep_status, 6, "mepStatusLt");
NS(mep_status_cc, mep_status, 7, "mepStatusCc");
NS(mep_status_bfd, mep_status, 8, "mepStatusBfd");
NS(mep_status_rt, mep_status, 9, "mepStatusRt");
NS(mep_status_lm_avail, mep_status, 10, "mepStatusLmAvail");
NS(mep_status_lm_hli, mep_status, 11, "mepStatusLmHli");
NS(mep_status_dm_fd_bin, mep_status, 12, "mepStatusDmBinFd");
NS(mep_status_dm_ifdv_bin, mep_status, 13, "mepStatusDmBinIfdv");

NS(mep_control_lm, mep_control, 1, "mepControlLm");
NS(mep_control_dm, mep_control, 2, "mepControlDm");
NS(mep_control_tst, mep_control, 3, "mepControlTst");
NS(mep_control_bfd, mep_control, 4, "mepControlBfd");

static StructRO2<MepCapabilitiesImpl> mep_capabilities_impl(
        &mep, vtss::expose::snmp::OidElement(1, "mepCapabilities"));

static TableReadWriteAddDelete2<MepConfigInstanceTableImpl> mep_config_instance_table_impl(
        &mep_config_instance, vtss::expose::snmp::OidElement(1, "mepConfigInstanceTable"), vtss::expose::snmp::OidElement(2, "mepConfigInstanceRowEditor"));

static TableReadWriteAddDelete2<MepConfigInstancePeerTableImpl> mep_config_instance_peer_table_impl(
        &mep_config_instance, vtss::expose::snmp::OidElement(3, "mepConfigInstancePeerTable"), vtss::expose::snmp::OidElement(4, "mepConfigInstancePeerRowEditor"));

static TableReadWrite2<MepConfigPmTableImpl> mep_config_pm_table_impl(
        &mep_config_pm, vtss::expose::snmp::OidElement(1, "mepConfigPmTable"));

static TableReadWriteAddDelete2<MepConfigCcTableImpl> mep_config_cc_table_impl(
        &mep_config_cc, vtss::expose::snmp::OidElement(1, "mepConfigCcTable"), vtss::expose::snmp::OidElement(2, "mepConfigCcRowEditor"));

static TableReadWrite2<MepConfigLmTableImpl> mep_config_lm_table_impl(
        &mep_config_lm, vtss::expose::snmp::OidElement(1, "mepConfigLmTable"));

static TableReadWrite2<MepConfigDmTableImpl> mep_config_dm_table_impl(
        &mep_config_dm, vtss::expose::snmp::OidElement(1, "mepConfigDmTable"));

static TableReadWriteAddDelete2<MepConfigLbTableImpl> mep_config_lb_table_impl(
        &mep_config_lb, vtss::expose::snmp::OidElement(1, "mepConfigLbTable"), vtss::expose::snmp::OidElement(2, "mepConfigLbRowEditor"));

static TableReadWriteAddDelete2<MepConfigTstTableImpl> mep_config_tst_table_impl(
        &mep_config_tst, vtss::expose::snmp::OidElement(1, "mepConfigTstTable"), vtss::expose::snmp::OidElement(2, "mepConfigTstRowEditor"));

static TableReadWriteAddDelete2<MepConfigLtTableImpl> mep_config_lt_table_impl(
        &mep_config_lt, vtss::expose::snmp::OidElement(1, "mepConfigLtTable"), vtss::expose::snmp::OidElement(2, "mepConfigLtRowEditor"));

static TableReadWriteAddDelete2<MepConfigApsTableImpl> mep_config_aps_table_impl(
        &mep_config_aps, vtss::expose::snmp::OidElement(1, "mepConfigApsTable"), vtss::expose::snmp::OidElement(2, "mepConfigApsRowEditor"));

static TableReadWriteAddDelete2<MepConfigAisTableImpl> mep_config_ais_table_impl(
        &mep_config_ais, vtss::expose::snmp::OidElement(1, "mepConfigAisTable"), vtss::expose::snmp::OidElement(2, "mepConfigAisRowEditor"));

static TableReadWriteAddDelete2<MepConfigLckTableImpl> mep_config_lck_table_impl(
        &mep_config_lck, vtss::expose::snmp::OidElement(1, "mepConfigLckTable"), vtss::expose::snmp::OidElement(2, "mepConfigLckRowEditor"));

static TableReadWriteAddDelete2<MepConfigClientFlowTableImpl> mep_config_client_flow_table_impl(
        &mep_config_client, vtss::expose::snmp::OidElement(2, "mepConfigClientFlowTable"), vtss::expose::snmp::OidElement(3, "mepConfigClientFlowRowEditor"));

static TableReadWrite2<MepConfigSyslogTableImpl> mep_config_syslog_table_impl(
        &mep_config_syslog, vtss::expose::snmp::OidElement(1, "mepConfigSyslogTable"));

static StructRW2<MepConfigTlvLeaf> mep_config_tlv_leaf(
        &mep_config_tlv, vtss::expose::snmp::OidElement(1, "mepConfigTlvLeaf"));

static TableReadWriteAddDelete2<MepConfigBfdTableImpl> mep_config_bfd_table_impl(
        &mep_config_bfd, vtss::expose::snmp::OidElement(1, "mepConfigBfdTable"), vtss::expose::snmp::OidElement(2, "mepConfigBfdRowEditor"));

static TableReadWriteAddDelete2<MepConfigBfdAuthTableImpl> mep_config_bfd_auth_table_impl(
        &mep_config_bfd_auth, vtss::expose::snmp::OidElement(1, "mepConfigBfdAuthTable"), vtss::expose::snmp::OidElement(2, "mepConfigBfdAuthRowEditor"));

static TableReadWriteAddDelete2<MepConfigRtTableImpl> mep_config_rt_table_impl(
        &mep_config_rt, vtss::expose::snmp::OidElement(1, "mepConfigRtTable"), vtss::expose::snmp::OidElement(2, "mepConfigRtRowEditor"));

static TableReadWrite2<MepConfigLstTableImpl> mep_config_lst_table_impl(
        &mep_config_lst, vtss::expose::snmp::OidElement(1, "mepConfigLstTable"));

static TableReadWrite2<MepConfigLmAvailTableImpl> mep_config_lm_avail_table_impl(
        &mep_config_lm_avail, vtss::expose::snmp::OidElement(1, "mepConfigLmAvailTable"));

static TableReadWrite2<MepConfigLmHliTableImpl> mep_config_lm_hli_table_impl(
        &mep_config_lm_hli, vtss::expose::snmp::OidElement(1, "mepConfigLmHliTable"));

static TableReadWrite2<MepConfigLmSdegTableImpl> mep_config_lm_sdeg_table_impl(
        &mep_config_lm_sdeg, vtss::expose::snmp::OidElement(1, "mepConfigLmSdegTable"));

static TableReadOnly2<MepStatusInstanceTableImpl> mep_status_instance_table_impl(
        &mep_status_instance, vtss::expose::snmp::OidElement(1, "mepStatusInstanceTable"));

static TableReadOnly2<MepStatusInstancePeerTableImpl> mep_status_instance_peer_table_impl(
        &mep_status_instance, vtss::expose::snmp::OidElement(2, "mepStatusInstancePeerTable"));

static TableReadOnly2<MepStatusLmTableImpl> mep_status_lm_table_impl(
        &mep_status_lm, vtss::expose::snmp::OidElement(1, "mepStatusLmTable"));

static TableReadOnly2<MepStatusLmPeerTableImpl> mep_status_lm_peer_table_impl(
        &mep_status_lm, vtss::expose::snmp::OidElement(2, "mepStatusLmPeerTable"));

static TableReadOnly2<MepStatusDmTwTableImpl> mep_status_dm_tw_table_impl(
        &mep_status_dm, vtss::expose::snmp::OidElement(1, "mepStatusDmTwoWayTable"));

static TableReadOnly2<MepStatusDmFdBinTableImpl> mep_status_dm_bin_fd_table_impl(
        &mep_status_dm_fd_bin, vtss::expose::snmp::OidElement(1, "mepStatusDmBinFdTable"));

static TableReadOnly2<MepStatusDmIfdvBinTableImpl> mep_status_dm_bin_ifdv_table_impl(
        &mep_status_dm_ifdv_bin, vtss::expose::snmp::OidElement(1, "mepStatusDmBinIfdvTable"));

static TableReadOnly2<MepStatusDmOwfnTableImpl> mep_status_dm_owfn_table_impl(
        &mep_status_dm, vtss::expose::snmp::OidElement(2, "mepStatusDmOneWayFarNearTable"));

static TableReadOnly2<MepStatusDmOwnfTableImpl> mep_status_dm_ownf_table_impl(
        &mep_status_dm, vtss::expose::snmp::OidElement(3, "mepStatusDmOneWayNearFarTable"));

static TableReadOnly2<MepStatusLbTableImpl> mep_status_lb_table_impl(
        &mep_status_lb, vtss::expose::snmp::OidElement(1, "mepStatusLbTable"));

static TableReadOnly2<MepStatusLbReplyTableImpl> mep_status_lb_reply_table_impl(
        &mep_status_lb, vtss::expose::snmp::OidElement(2, "mepStatusLbReplyTable"));

static TableReadOnly2<MepStatusTstTableImpl> mep_status_tst_table_impl(
        &mep_status_tst, vtss::expose::snmp::OidElement(1, "mepStatusTstTable"));

static TableReadOnly2<MepStatusLtTableImpl> mep_status_lt_table_impl(
        &mep_status_lt, vtss::expose::snmp::OidElement(1, "mepStatusLtTable"));

static TableReadOnly2<MepStatusLtReplyTableImpl> mep_status_lt_reply_table_impl(
        &mep_status_lt, vtss::expose::snmp::OidElement(2, "mepStatusLtReplyTable"));

static TableReadOnly2<MepStatusCcPeerTableImpl> mep_status_cc_peer_table_impl(
        &mep_status_cc, vtss::expose::snmp::OidElement(2, "mepStatusCcPeerTable"));

static TableReadOnly2<MepStatusBfdTableImpl> mep_status_bfd_table_impl(
        &mep_status_bfd, vtss::expose::snmp::OidElement(2, "mepStatusBfdTable"));

static TableReadOnly2<MepStatusRtTableImpl> mep_status_rt_table_impl(
        &mep_status_rt, vtss::expose::snmp::OidElement(1, "mepStatusRtTable"));

static TableReadOnly2<MepStatusRtReplyTableImpl> mep_status_rt_reply_table_impl(
        &mep_status_rt, vtss::expose::snmp::OidElement(2, "mepStatusRtReplyTable"));

static TableReadOnly2<MepStatusLmAvailTableImpl> mep_status_lm_avail_table_impl(
        &mep_status_lm_avail, vtss::expose::snmp::OidElement(2, "mepStatusLmAvailTable"));

static TableReadOnly2<MepStatusLmHliTableImpl> mep_status_lm_hli_table_impl(
        &mep_status_lm_hli, vtss::expose::snmp::OidElement(1, "mepStatusLmHliTable"));

static TableReadWrite2<MepControlLmTableImpl> mep_control_lm_table_impl(
        &mep_control_lm, vtss::expose::snmp::OidElement(1, "mepControlLmTable"));

static TableReadWrite2<MepControlDmTableImpl> mep_control_dm_table_impl(
        &mep_control_dm, vtss::expose::snmp::OidElement(1, "mepControlDmTable"));

static TableReadWrite2<MepControlTstTableImpl> mep_control_tst_table_impl(
        &mep_control_tst, vtss::expose::snmp::OidElement(1, "mepControlTstTable"));

static TableReadWrite2<MepControlBfdTableImpl> mep_control_bfd_table_impl(
        &mep_control_bfd, vtss::expose::snmp::OidElement(1, "mepControlBfdTable"));

}  // namespace interfaces
}  // namespace mep
}  // namespace appl
}  // namespace vtss
