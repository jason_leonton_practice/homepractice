/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable format
 (e.g. HEX file) and only in or with products utilizing the Microsemi switch and
 PHY products.  The source code of the software may not be disclosed, transmitted
 or distributed without the prior written permission of Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all ownership,
 copyright, trade secret and proprietary rights in the software and its source code,
 including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL WARRANTIES
 OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES ARE EXPRESS,
 IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION, WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND NON-INFRINGEMENT.

*/

/*
 * Internal API definition for MEF 45 (Latching Loopback) handling.
 *
 * This information is shared between the MEP moduel and the TT-LOOP module.
 */

#ifndef _MEP_LL_API_H_
#define _MEP_LL_API_H_

typedef enum {
    MEP_LL_ACTIVATE,
    MEP_LL_DEACTIVATE,
    MEP_LL_STATE,
} mep_ll_message_type_t;

typedef enum {
    MEP_LL_STATUS_INACTIVE,
    MEP_LL_STATUS_ACTIVE
} mep_ll_status_t;

typedef enum {
    MEP_LL_DIRECTION_INTERNAL,
    MEP_LL_DIRECTION_EXTERNAL,
} mep_ll_direction_t;

typedef struct {
    mep_ll_status_t     loopback_status;
    mep_ll_direction_t  loopback_direction;
    BOOL                unrecognized_tlv;
} mep_ll_flags_t;

typedef enum {
    MEP_LL_RESPONSE_NO_ERROR = 0,
    MEP_LL_RESPONSE_MALFORMED = 1,
    MEP_LL_RESPONSE_SESSION = 2,
    MEP_LL_RESPONSE_RESOURCES = 3,
    MEP_LL_RESPONSE_ACTIVE = 4,
    MEP_LL_RESPONSE_INACTIVE = 5,
    MEP_LL_RESPONSE_UNSUPPORTED = 6,
    MEP_LL_RESPONSE_MP = 7,
    MEP_LL_RESPONSE_TIMEOUT = 8,
    MEP_LL_RESPONSE_PROHIBITED = 9,
    MEP_LL_RESPONSE_UNKNOWN_MESSAGE = 10,
    MEP_LL_RESPONSE_UNKNOWN_ERROR = 255
} mep_ll_response_t;

typedef struct {
    mesa_mac_t             dmac;             /* DMAC of received LLM PDU */
    mesa_mac_t             smac;             /* SMAC of received LLM PDU */
    mesa_mac_t             lbport_mac;       /* Loopback Port MAC Address from LLM PDU */
    u8                     prio;
    mep_ll_message_type_t  message_type;
    u32                    expiration_timer;
} mep_ll_request_info_t;

typedef struct {
    mep_ll_flags_t         flags;
    mep_ll_response_t      response;
    mep_ll_message_type_t  message_type;
    u32                    expiration_timer;
} mep_ll_reply_info_t;

namespace mep_g2 {

/*
 *
 */
mesa_rc mep_ll_protocol_info_request_forward(const u32                    instance,
                                             const mep_ll_request_info_t  *const request_info,
                                             mep_ll_reply_info_t          *const reply_info);

} /* mep_g2 */

/*
 * Callout from MEP module to TT-LOOP module:
 * A Latching Loopback request (LLM) has been received.
 */
mesa_rc mep_ll_protocol_info_request_rx(const u32                    instance,
                                        const mep_ll_request_info_t  *const req_info,
                                        mep_ll_reply_info_t          *const rpl_info);

/*
 * Call-in from MEP CLI (debug):
 * A Latching Loopback request (LLM) should be sent.
 */
mesa_rc mep_ll_protocol_info_request_tx(const u32                    instance,
                                        const mep_ll_request_info_t  *const req_info);

/*
 * Callout from MEP module to TT-LOOP module:
 * The MEP instance was deleted.
 */
mesa_rc mep_ll_mep_deleted(const u32 mep_id);

/*
 * This function is called from the TT-LOOP module when an asynchronous notification
 * (i.e. an LLR not directly initiated by an LLM) needs to be sent to
 * the original LLM activation initiator. The reply is sent to the last
 * initiator which successfully requested a loop activation.
 */
mesa_rc mep_ll_protocol_info_reply_tx(const u32                  instance,
                                      const mep_ll_reply_info_t  *const info);

/*
 * This function is called from the TT-LOOP module to inform the MEP module that
 * the MEP is referenced by at least one Latching Loopback instance.
 */
mesa_rc mep_ll_register(const u32 mep_id);

/*
 * This function is called from the TT-LOOP module to inform the MEP module that
 * the MEP is no longer referenced by any Latching Loopback instance.
 */
mesa_rc mep_ll_deregister(const u32 mep_id);

u32 mep_tt_loop_mce_id_get(void);

#endif /* _MEP_LL_API_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
