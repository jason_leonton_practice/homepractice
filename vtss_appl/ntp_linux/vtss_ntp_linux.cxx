/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "main.h"
#include "subject.hxx"
#include "vtss_ntp_linux.h"
#include "vtss_ntp_linux_api.h"
#include <vtss/basics/notifications/process-daemon.hxx>

static vtss_trace_reg_t trace_reg = {VTSS_TRACE_MODULE_ID, "ntp_linux",
                                     "ntp Linux Module"};

static vtss_trace_grp_t trace_grps[TRACE_GRP_CNT] = {
                [VTSS_TRACE_GRP_DEFAULT] = {"default", "Default",
                                            VTSS_TRACE_LVL_WARNING,
                                            VTSS_TRACE_FLAGS_TIMESTAMP},
};

static vtss::notifications::ProcessDaemon vtss_process_ntpd(
        &vtss::notifications::subject_main_thread, "ntpd");

mesa_rc ntp_linux_init(vtss_init_data_t *data) {
    if (data->cmd == INIT_CMD_EARLY_INIT) {
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);
    }

    T_D("enter, cmd: %d, isid: %u, flags: 0x%x", data->cmd, data->isid,
        data->flags);

    switch (data->cmd) {
    case INIT_CMD_INIT:
        T_D("INIT");
        vtss_process_ntpd.executable = "/usr/sbin/ntpd";

        // Use SIGKILL instead of SIGTERM to kill process
        vtss_process_ntpd.kill_policy(true);

        // Allow the first adjustment to be Big
        vtss_process_ntpd.arguments.push_back("-g");

        // Do not fork - we want to keep contact such that we can monitor it
        vtss_process_ntpd.arguments.push_back("-n");

        // Redirect stdout/stderr to trace system
        vtss_process_ntpd.trace_stdout_conf(VTSS_TRACE_MODULE_ID,
                                            VTSS_TRACE_GRP_DEFAULT,
                                            VTSS_TRACE_LVL_INFO);

        vtss_process_ntpd.trace_stderr_conf(VTSS_TRACE_MODULE_ID,
                                            VTSS_TRACE_GRP_DEFAULT,
                                            VTSS_TRACE_LVL_ERROR);

        break;

    case INIT_CMD_START:
        T_D("START");
        vtss_process_ntpd.adminMode(vtss::notifications::ProcessDaemon::ENABLE);
        break;

    default:
        break;
    }

    T_D("exit");

    return VTSS_OK;
}
