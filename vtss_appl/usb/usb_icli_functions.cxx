/*************************************************************************************************
 * Copyright (C) 2018 Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: USB icli functions
 *
 *************************************************************************************************/

#include "icli_api.h"
#include "icli_porting_util.h"
#include "usb_api.h"
#include "usb_icli_functions.h"

usb_rc_t usb_icli_is_detect(u32 session_id, bool *detect)
{
    usb_rc_t rc;

    if ((rc = usb_is_detect(detect)) != USB_RC_OK) {
        ICLI_PRINTF("%s\n", usb_error_txt(rc));
    }

    return rc;
}

usb_rc_t usb_icli_vnc2_version_get(u32 session_id, unsigned char *ver)
{
    usb_rc_t rc;

    if ((rc = usb_vnc_ver_get(ver)) != USB_RC_OK) {
        ICLI_PRINTF("%s\n", usb_error_txt(rc));
    }

    return rc;
}

usb_rc_t usb_icli_vnc2_reset()
{
    return usb_vnc_sw_reset();
}
