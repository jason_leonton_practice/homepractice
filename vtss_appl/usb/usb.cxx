/*************************************************************************************************
 * Copyright (C) 2018 Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: USB
 *
 *************************************************************************************************/
#include <string.h>
#include "conf_api.h"
#include "critd_api.h"
#include "icli_api.h"
#include "main.h"
#include "misc_api.h"
#include "usb.h"            // For trace
#include "usb_api.h"        // For usb_rc_t
#include "msg_api.h"

#include <sys/ioctl.h>      // For ioctl
#include "vnc2.h"

/**************************************************************************************************
 * Private definition
 *************************************************************************************************/
#define USB_SPI_DEVNAME         "/dev/vnc2-dev.2"
#define USB_SPI_RETRY           (10)

#define USB_VNC_VERSION_MAJOR   (1)
#define USB_VNC_VERSION_MANOR   (3)

#define USB_VNC_DETECT_BIT      (1 << 5)

/**************************************************************************************************
 * Global variables
 *************************************************************************************************/
/* Critical region protection */
static critd_t crit;  // Critical region for global variables

#if (VTSS_TRACE_ENABLED)

static vtss_trace_reg_t trace_reg =
{
    VTSS_TRACE_MODULE_ID, "usb", "Universal Serial Bus"
};

static vtss_trace_grp_t trace_grps[VTSS_TRACE_GRP_CNT] = {
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CRIT */ {
        "crit",
        "Critical regions",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
};

#define USB_CRIT_ENTER() critd_enter(&crit, VTSS_TRACE_GRP_CRIT, \
                                        VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define USB_CRIT_EXIT()  critd_exit( &crit, VTSS_TRACE_GRP_CRIT, \
                                        VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)

#else   /* VTSS_TRACE_ENABLED */
#define USB_CRIT_ENTER() critd_enter(&crit)
#define USB_CRIT_EXIT()  critd_exit( &crit)

#endif  /* VTSS_TRACE_ENABLED */

static bool vnc_is_busy = false;

/**************************************************************************************************
 * Private function
 *************************************************************************************************/
static void usb_vnc_busy_set(bool is_busy)
{
    USB_CRIT_ENTER();
    vnc_is_busy = is_busy;
    USB_CRIT_EXIT();
}

static bool usb_vnc_is_busy()
{
    bool is_busy;

    USB_CRIT_ENTER();
    is_busy = vnc_is_busy;
    USB_CRIT_EXIT();

    return is_busy;
}

static mesa_rc usb_spi_ss_ctrl(int fd, bool var)
{
    return (ioctl(fd, VNC2_IOC_S_SS_CTRL, &var) == -1) ? MESA_RC_ERROR : MESA_RC_OK;
}

static usb_rc_t usb_spi_dev_open(int *fd)
{
    usb_vnc_busy_set(true);

    *fd = open(USB_SPI_DEVNAME, O_RDWR | O_SYNC);
    if (*fd < 0) {
        T_W("open error[%d]", *fd);
        usb_vnc_busy_set(false);
        return USB_RC_ERR_SPI_DEV_OPEN;
    }

    if (usb_spi_ss_ctrl(*fd, 1) != MESA_RC_OK) {
        T_W("can't set CS0 control - acquire");
        usb_vnc_busy_set(false);
        return USB_RC_ERR_SPI_SS_CTRL;
    }

    return USB_RC_OK;
}

static void usb_spi_dev_close(int fd)
{
    if (usb_spi_ss_ctrl(fd, 0) == MESA_RC_ERROR) {
        T_W("can't set CS0 control - acquire");
    }

    close(fd);
    usb_vnc_busy_set(false);
}

static usb_rc_t usb_vnc_stat_check()
{
    usb_rc_t        usb_rc;
    bool            present;
    unsigned char   version[2];

    // Initialize
    present = false;
    memset(version, 0, sizeof(version));

    // Check VNC version
    if ((usb_rc = usb_vnc_ver_get(version)) != USB_RC_OK) {
        return usb_rc;
    }
    if ((version[0] != USB_VNC_VERSION_MAJOR) || (version[1] != USB_VNC_VERSION_MANOR)) {
        return USB_RC_ERR_VNC_VERSION_CHECK;
    }

    return USB_RC_OK;
}

/**************************************************************************************************
 * Public function
 *************************************************************************************************/
const char *usb_error_txt(usb_rc_t rc)
{
    switch(rc) {
        case USB_RC_ERR_MEM_ALLOCATE:               return "Memory allocation error";
        case USB_RC_ERR_SPI_DEV_OPEN:               return "Open SPI device error";
        case USB_RC_ERR_SPI_DEV_CLOSE:              return "Close SPI device error";
        case USB_RC_ERR_SPI_SS_CTRL:                return "Control SPI SS error";
        case USB_RC_ERR_VNC_IS_BUSY:                return "USB is busy";
        case USB_RC_ERR_VNC_FILENAME_SET:           return "Set USB file name error";
        case USB_RC_ERR_VNC_WRITEFILE_END:          return "USB write file end error";
        case USB_RC_ERR_VNC_WRITEFILE_TIMEOUT:      return "USB transfer timeout";
        case USB_RC_ERR_VNC_WRITEFILE_START:
        case USB_RC_ERR_VNC_READFILE_START:         return "Invalid file, Please check the file name";
        case USB_RC_ERR_VNC_PRESET_GET:             return "Get VNC preset error";
        case USB_RC_ERR_VNC_VERSION_GET:            return "Get VNC version error";
        case USB_RC_ERR_VNC_VERSION_CHECK:          return "USB Chip not detected";
        case USB_RC_ERR_VNC_CHIP_RESET:             return "Can't reset USB Chip";
        case USB_RC_ERR_USB_PRESENT_CHECK:          return "USB not detected";
        default:                                    return "Unknown usb error code";
    }
}

usb_rc_t usb_vnc_sw_reset()
{
    int         fd;
    int         retry = 6;
    usb_rc_t    usb_rc;

    if (usb_vnc_is_busy()) {
        T_W("%s", usb_error_txt(USB_RC_ERR_VNC_IS_BUSY));
        return USB_RC_ERR_VNC_IS_BUSY;
    }

    if ((usb_rc = usb_spi_dev_open(&fd)) != USB_RC_OK) {
        return usb_rc;
    }

    /* VNC2_IOC_S_RESET_VNC2 */
    if (ioctl(fd, VNC2_IOC_S_RESET_VNC2, NULL) == -1) {
        T_W("Can't reset VNC2 chip");
    } else {
        T_I("Reset vnc2 chip");
    }

    sleep(1);

    /* VNC2_IOC_S_RESET_VNC2SPI */
    if (ioctl(fd, VNC2_IOC_S_RESET_VNC2SPI, NULL) == -1) {
        T_W("Can't reset VNC2 SPI");
        usb_rc = USB_RC_ERR_VNC_CHIP_RESET;
    } else {
        T_I("Reset vnc2 SPI");
    }

    usb_spi_dev_close(fd);

    // Check reset completed
    if (usb_rc == USB_RC_OK) {
        usb_rc = USB_RC_ERR_VNC_CHIP_RESET;
        for (; retry > 0; retry--) {
            T_I("Wait vnc2 SW reset");
            VTSS_OS_MSLEEP(500);

            // Check VNC buffer is normal
            if ((usb_rc = usb_vnc_stat_check()) == USB_RC_OK) {
                T_D("vnc2 SW reset success");
                usb_rc = USB_RC_OK;
                break;
            }
        }
    }

    return usb_rc;
}

usb_rc_t usb_file_write(unsigned char *filename, unsigned int filename_size,
                       unsigned char *filebuf, unsigned int filesize)
{
    int                         fd;
    int                         txlen = 0;
    int                         rxcnt;
    int                         ioctl_rc;
    usb_rc_t                    usb_rc;
    int                         retry = USB_SPI_RETRY;
    struct vnc2_file_name       vnc2_filename;
    bool                        present;

    // Check VNC buffer is normal
    if ((usb_rc = usb_vnc_stat_check()) != USB_RC_OK) {
        if (usb_rc == USB_RC_ERR_VNC_IS_BUSY) {
            return usb_rc;
        }

        if (usb_vnc_sw_reset() != USB_RC_OK) {
            return usb_rc;
        } else {
            // Waiting for vnc to detect USB
            VTSS_OS_MSLEEP(2000);
        }
    }

    if ((usb_rc = usb_is_detect(&present)) != USB_RC_OK) {
        return usb_rc;
    } else {
        if (!present) {
            return USB_RC_ERR_USB_PRESENT_CHECK;
        }
    }

    if ((usb_rc = usb_spi_dev_open(&fd)) != USB_RC_OK) {
        return usb_rc;
    }

    T_I("filename[%s], filename_size[%u]", filename, filename_size);

    // 1.write filename
    vnc2_filename.size = filename_size;
    vnc2_filename.name = (char*)filename;
    if ((ioctl_rc = ioctl(fd, VNC2_IOC_S_WFILENAME, &vnc2_filename)) != 0) {
        T_I("VNC2_IOC_S_WFILENAME error[%d]", ioctl_rc);
        usb_rc = USB_RC_ERR_VNC_FILENAME_SET;
        goto WFILE_EXIT;
    }

    // 2.write file
    // write file start
    if ((ioctl_rc = ioctl(fd, VNC2_IOC_S_WRITEFILE)) != 0) {
        T_I("VNC2_IOC_S_WRITEFILE error[%d]", ioctl_rc);
        usb_rc = USB_RC_ERR_VNC_WRITEFILE_START;
        goto WFILE_EXIT;
    }

    // Waiting for vnc to be ready
    VTSS_OS_MSLEEP(1000);

    T_I("Write start");
    while (txlen < filesize){
        rxcnt = write(fd, filebuf + txlen, filesize - txlen);

        if (rxcnt <= 0) {
            T_W("write file failed");
            retry--;
        } else {
            txlen += rxcnt;
            retry = 0;
            T_D("write file %d(%u%%)", rxcnt, 100 * txlen / filesize);
        }

        if (retry < 0) {
            T_D("*** VNC communitation error");
            usb_rc = USB_RC_ERR_VNC_WRITEFILE_TIMEOUT;
            goto WFILE_EXIT;
        }
    }
    T_I("Write end");

    /* close file on USB */
    if ((rxcnt = write(fd, filebuf, 0)) < 0) {
        T_I("write file close failed[%d]", rxcnt);
        usb_rc = USB_RC_ERR_VNC_WRITEFILE_END;
    } else {
        // Waiting for vnc to close the file
        VTSS_OS_MSLEEP(2000);
    }

WFILE_EXIT:
    usb_spi_dev_close(fd);

    return usb_rc;
}

usb_rc_t usb_file_read(unsigned char *filename, unsigned int filename_size,
                      unsigned char **filebuf, unsigned int *filesize)
{
    unsigned char               *rxbuf = NULL;
    int                         fd;
    int                         rxlen = 0;
    int                         rxcnt = 0;
    int                         ioctl_rc;
    usb_rc_t                    usb_rc;
    int                         retry = USB_SPI_RETRY;
    struct vnc2_file_name       vnc2_filename;
    bool                        present;

    // Check VNC buffer is normal
    if ((usb_rc = usb_vnc_stat_check()) != USB_RC_OK) {
        if (usb_rc == USB_RC_ERR_VNC_IS_BUSY) {
            return usb_rc;
        }

        if (usb_vnc_sw_reset() != USB_RC_OK) {
            return usb_rc;
        } else {
            // Waiting for vnc to detect USB
            VTSS_OS_MSLEEP(2000);
        }
    }

    if ((usb_rc = usb_is_detect(&present)) != USB_RC_OK) {
        return usb_rc;
    } else {
        if (!present) {
            return USB_RC_ERR_USB_PRESENT_CHECK;
        }
    }

    if ((usb_rc = usb_spi_dev_open(&fd)) != USB_RC_OK) {
        return usb_rc;
    }

    T_I("filename[%s], filename_size[%u]", filename, filename_size);

    // 1.write filename
    vnc2_filename.size = filename_size;
    vnc2_filename.name = (char*)filename;
    if ((ioctl_rc = ioctl(fd, VNC2_IOC_S_RFILENAME, &vnc2_filename)) != 0) {
        T_I("VNC2_IOC_S_WFILENAME error[%d]", ioctl_rc);
        usb_rc = USB_RC_ERR_VNC_FILENAME_SET;
        goto RFILE_EXIT;
    }

    // 2.read file
    if ((ioctl_rc = ioctl(fd, VNC2_IOC_S_READFILE, filesize)) != 0) {
        T_I("can't set readfile[%d]", ioctl_rc);
        usb_rc = USB_RC_ERR_VNC_READFILE_START;
        goto RFILE_EXIT;
    }

    T_I("file size %d\n", *filesize);

    // Waiting for vnc to be ready
    VTSS_OS_MSLEEP(1000);

    rxbuf = (unsigned char *) malloc(*filesize);
    if (!rxbuf) {
        T_I("fail to allocate memory for rxbuf");
        usb_rc = USB_RC_ERR_MEM_ALLOCATE;
        goto RFILE_EXIT;
    }

    *filebuf = rxbuf;

    T_I("Download start");
    while (rxlen < *filesize){
        rxcnt = read(fd, rxbuf + rxlen, *filesize - rxlen);

        if (rxcnt <= 0) {
            T_W("write file failed");
            retry--;
        } else {
            rxlen += rxcnt;
            retry = 0;
            T_D("read file %d(%u%%)", rxcnt, 100 * rxlen / *filesize);
        }

        if (retry < 0) {
            T_D("*** VNC communitation error");
            usb_rc = USB_RC_ERR_VNC_WRITEFILE_TIMEOUT;
            goto RFILE_EXIT;
        }
    }
    T_I("Download end");

    // Waiting for vnc to close the file
    VTSS_OS_MSLEEP(1000);

RFILE_EXIT:
    usb_spi_dev_close(fd);

    return usb_rc;
}

usb_rc_t usb_is_detect(bool *present)
{
    int             fd;
    int             ioctl_rc;
    usb_rc_t        usb_rc;
    unsigned char   preset;

    if (usb_vnc_is_busy()) {
        return USB_RC_ERR_VNC_IS_BUSY;
    }

    if ((usb_rc = usb_spi_dev_open(&fd)) != USB_RC_OK) {
        return usb_rc;
    }

    if ((ioctl_rc = ioctl(fd, VNC2_IOC_G_PRESET, &preset)) == 0) {
        *present = (preset & USB_VNC_DETECT_BIT) ? true : false;
        T_I("preset[%x]", preset);
        T_I("USB present[%d]", *present);
    } else {
        T_W("ioctl error[%d]", ioctl_rc);
        usb_rc = USB_RC_ERR_VNC_PRESET_GET;
    }

    usb_spi_dev_close(fd);

    return usb_rc;
}

usb_rc_t usb_vnc_ver_get(unsigned char *ver)
{
    int         fd;
    int         ioctl_rc;
    usb_rc_t    usb_rc;

    if (usb_vnc_is_busy()) {
        return USB_RC_ERR_VNC_IS_BUSY;
    }

    if ((usb_rc = usb_spi_dev_open(&fd)) != USB_RC_OK) {
        return usb_rc;
    }

    if ((ioctl_rc = ioctl(fd, VNC2_IOC_G_VER, ver)) == 0) {
        T_I("VNC2 Version[%x.%x]", ver[0], ver[1]);
    } else {
        T_W("ioctl error[%d]", ioctl_rc);
        usb_rc = USB_RC_ERR_VNC_VERSION_GET;
    }

    usb_spi_dev_close(fd);

    return usb_rc;
}

/**************************************************************************************************
 * Main
 *************************************************************************************************/
extern "C" int usb_icli_cmd_register();

mesa_rc usb_init(vtss_init_data_t *data)
{
    switch (data->cmd) {
        case INIT_CMD_EARLY_INIT:
            /* Initialize and register trace resources */
            VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, VTSS_TRACE_GRP_CNT);
            VTSS_TRACE_REGISTER(&trace_reg);
            break;

        case INIT_CMD_INIT:
            T_N("INIT_CMD_INIT");

            critd_init(&crit, "USB crit", VTSS_MODULE_ID_USB, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
            USB_CRIT_EXIT();
            usb_icli_cmd_register();

            T_N("INIT_CMD_INIT LEAVE");
            break;

        case INIT_CMD_START:
            T_N("INIT_CMD_START");
            break;
        case INIT_CMD_CONF_DEF:
            T_N("INIT_CMD_CONF_DEF");
            break;
        case INIT_CMD_MASTER_UP:
            T_N("INIT_CMD_MASTER_UP");
            break;
        case INIT_CMD_MASTER_DOWN:
            T_N("INIT_CMD_MASTER_DOWN");
            break;
        case INIT_CMD_SWITCH_ADD:
            T_N("INIT_CMD_SWITCH_ADD");
            break;
        case INIT_CMD_SWITCH_DEL:
            T_N("INIT_CMD_SWITCH_DEL");
            break;
        case INIT_CMD_SUSPEND_RESUME:
            T_N("INIT_CMD_SUSPEND_RESUME");
            break;

        default:
            /*  Do nothing...   */
            break;
    }

    return MESA_RC_OK;
}
