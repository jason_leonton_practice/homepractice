/*************************************************************************************************
 * Copyright (C) 2018 Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: USB API header
 *
 *************************************************************************************************/
#ifndef _USB_API_H_
#define _USB_API_H_

typedef enum {
    USB_RC_OK = 0,
    USB_RC_ERR_MEM_ALLOCATE,
    USB_RC_ERR_SPI_DEV_OPEN,
    USB_RC_ERR_SPI_DEV_CLOSE,
    USB_RC_ERR_SPI_SS_CTRL,
    USB_RC_ERR_VNC_IS_BUSY,
    USB_RC_ERR_VNC_FILENAME_SET,
    USB_RC_ERR_VNC_WRITEFILE_START,
    USB_RC_ERR_VNC_WRITEFILE_END,
    USB_RC_ERR_VNC_WRITEFILE_TIMEOUT,
    USB_RC_ERR_VNC_READFILE_START,
    USB_RC_ERR_VNC_PRESET_GET,
    USB_RC_ERR_VNC_VERSION_GET,
    USB_RC_ERR_VNC_VERSION_CHECK,
    USB_RC_ERR_VNC_CHIP_RESET,
    USB_RC_ERR_USB_PRESENT_CHECK,
    USB_RC_ERR_MAX
} usb_rc_t;

mesa_rc usb_init(vtss_init_data_t *data);

const char *usb_error_txt(usb_rc_t rc);
usb_rc_t usb_vnc_sw_reset(void);
usb_rc_t usb_file_read(unsigned char *filename, unsigned int filename_size, unsigned char **filebuf, unsigned int *filesize);
usb_rc_t usb_file_write(unsigned char *filename, unsigned int filename_size, unsigned char *filebuf, unsigned int filesize);
usb_rc_t usb_is_detect(bool *detect);
usb_rc_t usb_vnc_ver_get(unsigned char *ver);

#endif  /* _USB_API_H_ */
