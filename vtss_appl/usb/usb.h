/*************************************************************************************************
 * Copyright (C) 2018 Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: USB header
 *
 *************************************************************************************************/
#ifndef _USB_H_
#define _USB_H_

#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>
#include "vtss_os_wrapper.h"
#include "critd_api.h"

#define VTSS_TRACE_MODULE_ID                VTSS_MODULE_ID_USB
#define VTSS_TRACE_GRP_DEFAULT              0
#define VTSS_TRACE_GRP_CRIT                 1
#define VTSS_TRACE_GRP_CNT                  2

#include <vtss_trace_api.h>


#endif  /* _USB_H_ */
