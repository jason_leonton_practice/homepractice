/*************************************************************************************************
 * Copyright (C) 2018 Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: USB icli functions header
 *
 *************************************************************************************************/

#ifndef _MPTEST_ICLI_FUNCTIONS_H_
#define _MPTEST_ICLI_FUNCTIONS_H_

#include "icli_api.h"
#include "usb_api.h"

#ifdef __cplusplus
extern "C" {
#endif

usb_rc_t usb_icli_is_detect(u32 session_id, bool *detect);
usb_rc_t usb_icli_vnc2_version_get(u32 session_id, unsigned char *ver);
usb_rc_t usb_icli_vnc2_reset(void);

#ifdef __cplusplus
}
#endif

#endif /* _MPTEST_ICLI_FUNCTIONS_H_ */
