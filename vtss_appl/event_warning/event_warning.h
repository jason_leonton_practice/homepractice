/***********************************************************
 *
 * Copyright (c) 2018 LEONTON Technologies, Co.Ltd All Rights Reserved.
 *
 * Event Warning Module
 *
 ***********************************************************/

/* ================================================================= *
 * Trace definitions
 * ================================================================= */
#include "vtss_module_id.h"
#include "vtss_trace_lvl_api.h"
#include "vtss_trace_api.h"

#define VTSS_TRACE_MODULE_ID	VTSS_MODULE_ID_EVENT_WARNING
#define VTSS_TRACE_GRP_DEFAULT	0
#define VTSS_TRACE_GRP_CRIT		1
#define VTSS_TRACE_GRP_ICLI		2
#define VTSS_TRACE_GRP_CNT		3

