/***********************************************************
 *
 * Copyright (c) 2018 LEONTON Technologies, Co.Ltd All Rights Reserved.
 *
 * Event Warning Module
 *
 ***********************************************************/

#include "icli_api.h"
#include "icli_porting_util.h"
#include "event_warning.h" // For trace
#include "event_warning_api.h"
#include "event_warning_icli_functions.h"

#include "icfg_api.h" // For vtss_icfg_query_request_t
#include "misc_api.h" // for uport2iport
#include "msg_api.h" // For msg_switch_exists

void event_warning_icli_linkdown_register(i32 session_id,
											icli_stack_port_range_t *plist,
											bool use_relay,
											bool use_email,
											bool has_on,
											bool has_off)
{
	port_iter_t pit;
	event_executer_e etype;
	u32 iport_list;
	bool is_warning;

	if (icli_port_iter_init(&pit, plist->switch_range[0].isid, PORT_ITER_FLAGS_FRONT | PORT_ITER_SORT_ORDER_IPORT) == VTSS_RC_OK) {
		iport_list = 0x0;
		while (icli_port_iter_getnext(&pit, plist)) {
			T_D("got pit.iport %d, pit.uport %d", pit.iport, pit.uport);
			iport_list |= VTSS_BIT(pit.iport);
		}
	} else {
		T_E("Failed at get port list");
		return;
	}

	is_warning = has_on? true: false;
	etype = use_email? EVENT_EXECUTER_EMAIL: EVENT_EXECUTER_RELAY;

	T_D("iport_list 0x%x, Event:%s is_warning %s",
		iport_list,
		use_email? "Email": "Relay",
		has_on? "on": "off");

	event_warning_port_linkdown_register(etype, iport_list, is_warning);
}

void event_warning_icli_power_loss_register(i32 session_id,
											u32 idx,
											bool use_relay,
											bool use_email,
											bool has_on,
											bool has_off)
{
	u8 pwr_list;
	event_executer_e etype;
	bool is_warning;

	if(idx == 1)
		pwr_list = EVENT_POWER_P1;
	else if(idx == 2)
		pwr_list = EVENT_POWER_P2;
	else
		pwr_list = EVENT_POWER_BOTH;

	etype = use_email? EVENT_EXECUTER_EMAIL: EVENT_EXECUTER_RELAY;

	is_warning = has_on? true: false;

	T_D("pwr_list 0x%x, Event:%s, is_warning %s",
		pwr_list,
		use_email? "Email": "Relay",
		is_warning? "on": "off");

	event_warning_power_loss_register(etype, pwr_list, is_warning);
}

#if defined(VTSS_SW_OPTION_DDMI)
void event_warning_icli_ddmi_state_register(i32 session_id,
											bool use_relay,
											bool use_email,
											bool has_on,
											bool has_off)
{
	u32 port_list;
	event_executer_e etype;
	bool is_warning;

	port_list = ~0; // All ports, we just support ddmi on/off

	etype = use_email? EVENT_EXECUTER_EMAIL: EVENT_EXECUTER_RELAY;

	is_warning = has_on? true: false;

	T_D("ddmi_state 0x%x, Event:%s, is_warning %s",
		port_list,
		use_email? "Email": "Relay",
		is_warning? "on": "off");

	event_warning_ddmi_state_register(etype, port_list, is_warning);
}
#endif

/***************************************************************************/
/*  Functions called by iCLI                                                */
/****************************************************************************/
// Check if ddmi is supported 
BOOL event_warning_icli_ddmi_runtime_supported(u32 session_id,
                                icli_runtime_ask_t ask,
                                icli_runtime_t     *runtime)
{
    bool ddmi_cli_supported=event_warning_ddmi_supported();

	switch (ask) {
	case ICLI_ASK_PRESENT:
        if (ddmi_cli_supported) {
          runtime->present = TRUE;
        } else {
          runtime->present = FALSE;
        }
        return TRUE;
    default:
        return FALSE;
  }
}


static void	event_warning_icli_show_event(i32 session_id, int show_type)
{
	int 						etype;
	char 						buf[250];
	port_iter_t 				pit;
	evt_obj_t 					evt;
	switch_iter_t 				sit;
	icli_stack_port_range_t 	*list = NULL;
	bool ddmi_cli_supported=event_warning_ddmi_supported();
    
	(void)event_warning_get_event_obj(&evt);

	for(etype = 0; etype < EVENT_EXECUTER_MAX; etype++) {
		if(show_type == etype || show_type == EVENT_EXECUTER_MAX) {
			sprintf(buf, "%-7s %-9s", "Power", "Status");
			icli_table_header(session_id, buf); // Print header

			ICLI_PRINTF("%-7s %-9s\n", "1", evt.power.monitorred_mask[etype] & EVENT_POWER_P1? "on": "off");
			ICLI_PRINTF("%-7s %-9s\n\n", "2", evt.power.monitorred_mask[etype] & EVENT_POWER_P2? "on": "off");

#if defined(VTSS_SW_OPTION_DDMI)
            //DDMI
            if(ddmi_cli_supported){
                sprintf(buf, "%-7s %-9s", "DDMI", "Status");
			    icli_table_header(session_id, buf); // Print header
			    ICLI_PRINTF("%-7s %-9s\n\n", "State", evt.ddmi_port.monitorred_mask[etype] ? "on": "off");
            }
#endif

			// Header
			sprintf(buf, "%-23s %-9s", "Interface", "Status");
			icli_table_header(session_id, buf); // Print header

			if (icli_switch_iter_init(&sit) == VTSS_RC_OK) {
				while (icli_switch_iter_getnext(&sit, list)) {
					if(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT | PORT_ITER_SORT_ORDER_IPORT) == VTSS_RC_OK) {
						while (icli_port_iter_getnext(&pit, list)) {
							(void) icli_port_info_txt(sit.usid, pit.uport, buf);  // Get interface as printable string
							ICLI_PRINTF("%-23s %-9s \n",
							&buf[0], // interface
							evt.port.monitorred_mask[etype] & VTSS_BIT(pit.iport)? "on": "off");
						}
					}
				}
			}
		} /* end of if(show_type) */
	} /* end of for */
}

void event_warning_icli_show(i32 session_id, bool show_relay, bool show_email)
{
	int show_type = 0;

	if(show_relay)
		show_type = EVENT_EXECUTER_RELAY;
	else if(show_email)
		show_type = EVENT_EXECUTER_EMAIL;
	else
		show_type = EVENT_EXECUTER_MAX; /* show all */

	event_warning_icli_show_event(session_id, show_type);
}
