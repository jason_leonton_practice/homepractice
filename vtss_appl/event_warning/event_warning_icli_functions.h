/***********************************************************
 *
 * Copyright (c) 2018 LEONTON Technologies, Co.Ltd All Rights Reserved.
 *
 * Event Warning Module
 *
 ***********************************************************/
#ifndef _VTSS_ICLI_EVENT_WARNING_H_
#define _VTSS_ICLI_EVENT_WARNING_H_

#ifdef __cplusplus
extern "C" {
#endif

void event_warning_icli_linkdown_register(i32 session_id,
											icli_stack_port_range_t *plist,
											bool use_relay,
											bool use_email,
											bool has_on,
											bool has_off);

void event_warning_icli_power_loss_register(i32 session_id,
											u32 idx,
											bool use_relay,
											bool use_email,
											bool has_on,
											bool has_off);

void event_warning_icli_ddmi_state_register(i32 session_id,
											bool use_relay,
											bool use_email,
											bool has_on,
											bool has_off);

BOOL event_warning_icli_ddmi_runtime_supported(u32 session_id,
                                icli_runtime_ask_t ask,
                                icli_runtime_t     *runtime);

void event_warning_icli_show(i32 session_id, bool show_relay, bool show_email);

#ifdef __cplusplus
}
#endif

#endif /* _VTSS_ICLI_EVENT_WARNING_H_ */
