/***********************************************************
 *
 * Copyright (c) 2018 LEONTON Technologies, Co.Ltd All Rights Reserved.
 *
 * Event Warning Module WEB
 *
 ***********************************************************/

#include "web_api.h"
#include "port_api.h"
#include "event_warning.h"
#include "event_warning_api.h"
#ifdef VTSS_SW_OPTION_PRIV_LVL
#include "vtss_privilege_api.h"
#include "vtss_privilege_web_api.h"
#endif

/* =================
 * Trace definitions
 * -------------- */
#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>
#include <vtss_trace_api.h>
/* ============== */

/****************************************************************************/
/*  Web Handler functions - Relay                                           */
/****************************************************************************/
static i32 handler_config_event_warning_relay(CYG_HTTPD_STATE *p)
{
	vtss_isid_t sid = web_retrieve_request_sid(p); /* Includes USID = ISID */
	int ct, var;
	port_iter_t pit;
	evt_obj_t conf;
    int ddmi_enable=0;
    bool ddmi_web_supported=event_warning_ddmi_supported();
    

	if (redirectUnmanagedOrInvalid(p, sid)) { /* Redirect unmanaged/invalid access to handler */
		return -1;
	}
#ifdef VTSS_SW_OPTION_PRIV_LVL
	if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_EVENT_WARNING)) {
		return -1;
	}
#endif

	if (p->method == CYG_HTTPD_METHOD_POST) {
		int errors = 0;
		u8 pwr_pin = 0;
		u32 iport_list = 0;

		if (event_warning_get_event_obj(&conf) != VTSS_OK) {
			errors++;   /* Probably stack error */
		} else {

			if (cyg_httpd_form_varable_int(p, "power_1", &var)) {
				if (var == 1) {
					pwr_pin |= VTSS_BIT(0);
				}
			}
			if (cyg_httpd_form_varable_int(p, "power_2", &var)) {
				if (var == 1) {
					pwr_pin |= VTSS_BIT(1);
				}
			}

			event_warning_power_loss_register(EVENT_EXECUTER_RELAY, pwr_pin, 1);
			event_warning_power_loss_register(EVENT_EXECUTER_RELAY, ~pwr_pin, 0);

#if defined(VTSS_SW_OPTION_DDMI)
			if (cyg_httpd_form_varable_int(p, "ddmi_state", &var)) {
			    if (var == 1) {
			        event_warning_ddmi_state_register(EVENT_EXECUTER_RELAY, ~0, true);
			    }else{
			        event_warning_ddmi_state_register(EVENT_EXECUTER_RELAY, ~0, false);
			    }
			}
#endif
			/* Ports */
			(void) port_iter_init(&pit, NULL, sid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
			while (port_iter_getnext(&pit)) {
				int i = pit.iport, uport = pit.uport;

				if (cyg_httpd_form_variable_check_fmt(p, "enable_%d", uport)) {
					iport_list |= VTSS_BIT(i);
				}
			}

			event_warning_port_linkdown_register(EVENT_EXECUTER_RELAY, iport_list, 1);
			event_warning_port_linkdown_register(EVENT_EXECUTER_RELAY, ~iport_list, 0);
		}
		redirect(p, errors ? STACK_ERR_URL : "/event_warning_relay.htm");
	} else { /* CYG_HTTPD_METHOD_GET (+HEAD) */
		cyg_httpd_start_chunked("html");

		if(event_warning_get_event_obj(&conf) == VTSS_OK) {

			ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d,%d,",
					(conf.power.monitorred_mask[EVENT_EXECUTER_RELAY] & VTSS_BIT(0)) ? 1 : 0,
					(conf.power.monitorred_mask[EVENT_EXECUTER_RELAY] & VTSS_BIT(1)) ? 1 : 0);
			cyg_httpd_write_chunked(p->outbuffer, ct);

            /* DDMI */
#if defined(VTSS_SW_OPTION_DDMI)
            ddmi_enable=(conf.ddmi_port.monitorred_mask[EVENT_EXECUTER_RELAY] ? 1 : 0);
#endif
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d/%d,",(ddmi_web_supported ? 1:0),ddmi_enable);
            cyg_httpd_write_chunked(p->outbuffer, ct);

			/* Ports */
			(void) port_iter_init(&pit, NULL, sid,  PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
			while (port_iter_getnext(&pit)) {
				int i = pit.iport, uport = pit.uport;

				ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d/%d|", uport,
							  (conf.port.monitorred_mask[EVENT_EXECUTER_RELAY] & VTSS_BIT(i)) ? 1 : 0);
				cyg_httpd_write_chunked(p->outbuffer, ct);
			}
		}
		cyg_httpd_end_chunked();
	}
	return -1; // Do not further search the file system.
}



/****************************************************************************/
/*  Web Handler functions - Email                                           */
/****************************************************************************/
// static i32 handler_config_event_warning_email(CYG_HTTPD_STATE *p)
// {
//	 vtss_isid_t sid = web_retrieve_request_sid(p); /* Includes USID = ISID */
//	 int ct, var;
//	 port_iter_t pit;
//	 evt_obj_t conf;

//	 if (redirectUnmanagedOrInvalid(p, sid)) { /* Redirect unmanaged/invalid access to handler */
//		 return -1;
//	 }
// #ifdef VTSS_SW_OPTION_PRIV_LVL
//	 if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_EVENT_WARNING)) {
//		 return -1;
//	 }
// #endif

//	 if (p->method == CYG_HTTPD_METHOD_POST) {
//		 int errors = 0;
//		 u8 pwr_pin = 0;
//		 u32 iport_list = 0;

//		 if (event_warning_get_event_obj(&conf) != VTSS_OK) {
//			 errors++;   /* Probably stack error */
//		 } else {

//			 if (cyg_httpd_form_varable_int(p, "power_1", &var)) {
//			 	if (var == 1) {
//			 		pwr_pin |= VTSS_BIT(0);
//			 	}
//			 }
//			 if (cyg_httpd_form_varable_int(p, "power_2", &var)) {
//			 	if (var == 1) {
//			 		pwr_pin |= VTSS_BIT(1);
//			 	}
//			 }

//			 event_warning_power_loss_register(EVENT_EXECUTER_RELAY, pwr_pin, 1);
//			 event_warning_power_loss_register(EVENT_EXECUTER_RELAY, ~pwr_pin, 0);

// 			/* Ports */
// 			(void) port_iter_init(&pit, NULL, sid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
// 			while (port_iter_getnext(&pit)) {
// 				int i = pit.iport, uport = pit.uport;

//				 if (cyg_httpd_form_variable_check_fmt(p, "enable_%d", uport)) {
//				 	iport_list |= VTSS_BIT(i);
//				 }
// 			}

// 			event_warning_port_linkdown_register(EVENT_EXECUTER_RELAY, iport_list, 1);
// 			event_warning_port_linkdown_register(EVENT_EXECUTER_RELAY, ~iport_list, 0);
// 		}
//		 redirect(p, errors ? STACK_ERR_URL : "/event_warning_relay.htm");
//	 } else { /* CYG_HTTPD_METHOD_GET (+HEAD) */
//		 cyg_httpd_start_chunked("html");

//		 if(event_warning_get_event_obj(&conf) == VTSS_OK) {

// 			ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d,%d,",
// 					(conf.power.monitorred_mask[EVENT_EXECUTER_RELAY] & VTSS_BIT(0)) ? 1 : 0,
// 					(conf.power.monitorred_mask[EVENT_EXECUTER_RELAY] & VTSS_BIT(1)) ? 1 : 0);
// 			cyg_httpd_write_chunked(p->outbuffer, ct);

// 			/* Ports */
// 			(void) port_iter_init(&pit, NULL, sid,  PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
// 			while (port_iter_getnext(&pit)) {
// 				int i = pit.iport, uport = pit.uport;

// 				ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d/%d|", uport,
// 							  (conf.port.monitorred_mask[EVENT_EXECUTER_RELAY] & VTSS_BIT(i)) ? 1 : 0);
// 				cyg_httpd_write_chunked(p->outbuffer, ct);
// 			}
// 		}
//		 cyg_httpd_end_chunked();
//	 }
//	 return -1; // Do not further search the file system.
// }



/****************************************************************************/
/*  HTTPD Table Entries                                                     */
/****************************************************************************/
CYG_HTTPD_HANDLER_TABLE_ENTRY(cb_handler_config_event_warning_relay, "/config/event_warning_relay", handler_config_event_warning_relay);
// CYG_HTTPD_HANDLER_TABLE_ENTRY(cb_handler_config_event_warning_email, "/config/event_warning_email", handler_config_event_warning_email);