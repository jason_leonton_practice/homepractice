/***********************************************************
 *
 * Copyright (c) 2018 LEONTON Technologies, Co.Ltd All Rights Reserved.
 *
 * Event Warning Module
 *
 ***********************************************************/
#include "main.h"
#include "conf_api.h"
#include "critd_api.h"
#include "port_api.h"
#include "lock.hxx"
#include "event_warning.h"
#include "event_warning_api.h"
#include "icli_api.h"
#include "icli_porting_util.h"
#include "icfg_api.h" // For vtss_icfg_query_request_t
#include "led_api.h"
//#include "vncusb_api.h"
#ifdef VTSS_SW_OPTION_SYSLOG
#include "syslog_api.h"
#endif
#ifdef VTSS_SW_OPTION_SNMP
#include "vtss_snmp_api.h"
#include "snmp_custom_api.h"
#endif

extern "C" int event_warning_icli_cmd_register();

/****************************************************************************
 *  TRACE system
 ****************************************************************************/
#if (VTSS_TRACE_ENABLED)
static vtss_trace_reg_t trace_reg = {
	VTSS_TRACE_MODULE_ID, "Event_Warning", "Event Warning Module"
};

static vtss_trace_grp_t trace_grps[VTSS_TRACE_GRP_CNT] = {
	/* VTSS_TRACE_GRP_DEFAULT */ {
		"default",
		"Default",
		VTSS_TRACE_LVL_WARNING,
		VTSS_TRACE_FLAGS_TIMESTAMP
	},
	/* VTSS_TRACE_GRP_CRIT */ {
		"crit",
		"Critical regions ",
		VTSS_TRACE_LVL_ERROR,
		VTSS_TRACE_FLAGS_TIMESTAMP
		},
	/* VTSS_TRACE_GRP_ICLI */ {
		"iCLI",
		"iCLI",
		VTSS_TRACE_LVL_ERROR,
		VTSS_TRACE_FLAGS_TIMESTAMP
	},
};
#define EW_CRIT_ENTER() critd_enter(&crit, VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define EW_CRIT_EXIT() critd_exit(&crit, VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)

#else /* !VTSS_TRACE_ENABLED */

#define EW_CRIT_ENTER() critd_enter(&crit)
#define EW_CRIT_EXIT() critd_exit(&crit)

#endif /* VTSS_TRACE_ENABLED */

/****************************************************************************
 *  Pre-defined functions
 ****************************************************************************/
#define POWER_GET_BOTH_STATUS (event_warning_power_status_get(EVENT_POWER_BOTH))
#define POWER_GET_P1_STATUS (POWER_GET_BOTH_STATUS & EVENT_POWER_P1)
#define POWER_GET_P2_STATUS (POWER_GET_BOTH_STATUS & EVENT_POWER_P2)

static void EW_do_relay(bool open);

/****************************************************************************
 *  Global variables
 ****************************************************************************/
static evt_obj_t g_event;
static critd_t crit;
static int g_pwr_status = EVENT_POWER_P1 | EVENT_POWER_P2;

#if defined(VTSS_SW_OPTION_DDMI)
char ddmi_syslog_str[VTSS_APPL_DDMI_EVENT_TYPE_TOTAL][VTSS_APPL_DDMI_STR_MAX_LEN]={
	"Temperature", "Voltage", "Bias", "Tx Power", "Rx Power"
};
#endif

/*************************************************************************
 *  Private functions
 *************************************************************************/

/***************************************************************************/
/* ICFG (Show running)                                                     */
/***************************************************************************/
#ifdef VTSS_SW_OPTION_ICFG

static void EW_icfg_reset_to_default(void)
{
	T_D("Do reset conf to default");

	event_warning_port_linkdown_register(EVENT_EXECUTER_RELAY, ~0, false);
	event_warning_power_loss_register(EVENT_EXECUTER_RELAY, ~0, false);
#if defined(VTSS_SW_OPTION_DDMI)
	event_warning_ddmi_state_register(EVENT_EXECUTER_RELAY, ~0, false);
#endif
}

// Function called by ICFG.
static mesa_rc EW_icfg_global_conf(const vtss_icfg_query_request_t *req,
										vtss_icfg_query_result_t *result)
{
	int etype;

	T_D("req->cmd_mode %d, req->order %d, req->all_defaults %d", req->cmd_mode, req->order, req->all_defaults);

	/* TODO: all_defaults */
	switch (req->cmd_mode) {
		/* configure terminal */
		case ICLI_CMD_MODE_GLOBAL_CONFIG:
			for(etype = EVENT_EXECUTER_RELAY; etype < EVENT_EXECUTER_MAX; etype++) {
				if(g_event.power.monitorred_mask[etype] & EVENT_POWER_P1) {
					VTSS_RC(vtss_icfg_printf(result, "eventwarning power 1 %s\n", (etype == EVENT_EXECUTER_RELAY)? "relay": "email"));
				}
				if(g_event.power.monitorred_mask[etype] & EVENT_POWER_P2) {
					VTSS_RC(vtss_icfg_printf(result, "eventwarning power 2 %s\n", (etype == EVENT_EXECUTER_RELAY)? "relay": "email"));
				}
#if defined(VTSS_SW_OPTION_DDMI)
                if(g_event.ddmi_port.monitorred_mask[etype] & ~0) {
                    VTSS_RC(vtss_icfg_printf(result, "eventwarning ddmi %s\n", (etype == EVENT_EXECUTER_RELAY)? "relay": "email"));
                }
#endif
			} /* end of for(etype) */
		break;

		/* Interface configurations */
		case ICLI_CMD_MODE_INTERFACE_PORT_LIST:
			for(etype = EVENT_EXECUTER_RELAY; etype < EVENT_EXECUTER_MAX; etype++) {
				u16 iport = req->instance_id.port.begin_iport;

				if(g_event.port.monitorred_mask[etype] & VTSS_BIT(iport)) {
					VTSS_RC(vtss_icfg_printf(result, "eventwarning linkdown %s\n", (etype == EVENT_EXECUTER_RELAY)? "relay": "email"));
				}
			} /* end of for(etype) */
			break;
		default:
			break;
	} /* end of switch(req->cmd_mode) */

	return VTSS_RC_OK;
}

/* ICFG Initialization function */
static mesa_rc event_warning_icfg_init(void)
{
	VTSS_RC(vtss_icfg_query_register(VTSS_ICFG_EVENT_WARNING_GLOBAL_CONF, "event_warning", EW_icfg_global_conf));
	VTSS_RC(vtss_icfg_query_register(VTSS_ICFG_EVENT_WARNING_PORT_CONF, "event_warning", EW_icfg_global_conf));
	return VTSS_RC_OK;
}
#endif /* VTSS_SW_OPTION_ICFG */

static void EW_port_link_status_update_callback(mesa_port_no_t port_no, port_info_t *info)
{
	u32 iport = port_no;
	T_D("iport %d, info->link %d", iport, info->link);

	EW_CRIT_ENTER();
	/* info->link == 1: link-up
	 * info->link == 0: link-down */
	if(info->link == 1)
		g_event.port.link_status |= VTSS_BIT(iport);
	else
		g_event.port.link_status &= ~VTSS_BIT(iport);
	EW_CRIT_EXIT();

	/* to reslove the port link status will not be updated by PORT module*/
	if(!g_event.port.first_status_got)
		g_event.port.first_status_got = true;
}

#if defined(VTSS_SW_OPTION_DDMI)
static void EW_ddmi_state_status_update_callback(mesa_port_no_t port_no, vtss_appl_ddmi_event_state_t state[])
{
	u32 iport = port_no;
	int ddmi_event_type;
	EW_CRIT_ENTER();
	for(ddmi_event_type=VTSS_APPL_DDMI_EVENT_TYPE_TEMPERATURE;ddmi_event_type<VTSS_APPL_DDMI_EVENT_TYPE_TOTAL;ddmi_event_type++){
	    if(state[ddmi_event_type]==VTSS_APPL_DDMI_EVENT_STATE_HI_ALARM || state[ddmi_event_type]==VTSS_APPL_DDMI_EVENT_STATE_LO_ALARM){
	        T_D("iport %d, ddmi_event_type %d ALARM", iport, ddmi_event_type);
	        g_event.ddmi_port.state_status[ddmi_event_type] |= VTSS_BIT(iport);
	    }else{
	        g_event.ddmi_port.state_status[ddmi_event_type] &= ~VTSS_BIT(iport);
	    }
	}
	EW_CRIT_EXIT();

}
#endif

static void EW_do_relay(bool open)
{
	T_D("Do relay '%s'", open? "open": "close");

	if(open == EVENT_RELAY_OPEN) {
		mesa_gpio_write(NULL, 0, MEBA_GPIO_O_RELAY, true);
	} else {
		mesa_gpio_write(NULL, 0, MEBA_GPIO_O_RELAY, false);
	}
}

/*************************************************************************
 *  Public functions
 *************************************************************************/
#define EW_PORT_APPEND(_etype, _iport_list, _iswarning) \
do { \
    if(_iswarning){ \
        g_event.port.monitorred_mask[_etype] |= _iport_list;\
        g_event.port.old_status |= _iport_list;\
    } else { \
        g_event.port.monitorred_mask[_etype] &= ~_iport_list;\
        g_event.port.old_status &= ~_iport_list;\
    } \
    g_event.port.changed = true;\
} while(0)

void event_warning_port_linkdown_register(event_executer_e etype, u32 iport_list, bool is_warning)
{
    /* send SNMP trap by port module itself, we just focus on control relay and email */
    EW_CRIT_ENTER();
    EW_PORT_APPEND(etype, iport_list, is_warning);
    EW_CRIT_EXIT();
}

#define EW_POWER_APPEND(_etype, _pwr_list, _iswarning) \
do { \
	if(_iswarning) {\
		g_event.power.monitorred_mask[_etype] |= _pwr_list;\
	} else {\
		g_event.power.monitorred_mask[_etype] &= ~_pwr_list;\
	}\
	g_event.power.changed = true;\
} while(0)

void event_warning_power_loss_register(event_executer_e etype, u8 pwr_list, bool is_warning)
{
	/* send SNMP trap by port module itself, we just focus on control relay and email */
	EW_CRIT_ENTER();
	EW_POWER_APPEND(etype, pwr_list, is_warning);
	EW_CRIT_EXIT();
}

#if defined(VTSS_SW_OPTION_DDMI)
#define EW_DDMMI_PORT_APPEND(_etype, _iport_list, _iswarning) \
do { \
    if(_iswarning){ \
        g_event.ddmi_port.monitorred_mask[_etype] |= _iport_list;\
    } else { \
        g_event.ddmi_port.monitorred_mask[_etype] &= ~_iport_list;\
    } \
    g_event.ddmi_port.changed = true;\
} while(0)

void event_warning_ddmi_state_register(event_executer_e etype, u32 iport_list, bool is_warning)
{
    EW_CRIT_ENTER();
    EW_DDMMI_PORT_APPEND(etype, iport_list, is_warning);
    EW_CRIT_EXIT();
}
#endif

mesa_rc event_warning_get_event_obj(evt_obj_t *evt_obj)
{
	if(!evt_obj)
		return VTSS_RC_ERROR;

	memcpy(evt_obj, &g_event, sizeof(evt_obj_t));
	return VTSS_RC_OK;
}

u8 event_warning_power_status_get(u8 pwr_list)
{
	int power[2] = {0};
	meba_led_color_t color;

	if(MESA_RC_OK == lntn_sysled_get_color(MEBA_LED_TYPE_POWER1, &color))
		power[0] = (color == MEBA_LED_COLOR_GREEN)? 1: 0;

	if(MESA_RC_OK == lntn_sysled_get_color(MEBA_LED_TYPE_POWER2, &color))
		power[1] = (color == MEBA_LED_COLOR_GREEN)? 1: 0;

	T_D("power[0] 0x%x, power[1] 0x%x\n", power[0], power[1]);

	if (power[0]) {
		g_pwr_status |= EVENT_POWER_P1;
	} else {
		g_pwr_status &= ~EVENT_POWER_P1;
	}

	if (power[1]) {
		g_pwr_status |= EVENT_POWER_P2;
	} else {
		g_pwr_status &= ~EVENT_POWER_P2;
	}

	return g_pwr_status & pwr_list;
}


// Check if ddmi is supported
BOOL event_warning_ddmi_supported()
{
    bool ddmi_supported=0;
    int sfp_supported=0;
#if defined(VTSS_SW_OPTION_DDMI)
    ddmi_supported=1;
#endif

    sfp_supported=lntn_conf_mgmt_sfp_count_get();

    if (ddmi_supported && sfp_supported) {
        return TRUE;
    } else {
        return FALSE;
    }
}

mesa_bool_t event_warning_relay_status_get()
{
    mesa_bool_t val=0;
    mesa_rc rc=0;
    rc=mesa_gpio_read(NULL, 0, MEBA_GPIO_O_RELAY, &val);
    if(rc==MESA_RC_OK){
        return val;
    }else{
        return EVENT_RELAY_CLOSE;
    }
}

/**********************************************************************
 *  Thread
 **********************************************************************/
static vtss_handle_t EW_thread_handle;
static vtss_thread_t EW_thread_block;

static void EW_thread_init_relay(void)
{
	/* Turn Relay close to and set Relay(gpio) as output */
	EW_do_relay(EVENT_RELAY_CLOSE);
	mesa_gpio_direction_set(NULL, 0, MEBA_GPIO_O_RELAY, true);
}

static void EW_thread_register_port_link_status(void)
{
	/* Update port link status via callback function which register to PORT module */
	port_change_register(VTSS_MODULE_ID_EVENT_WARNING, EW_port_link_status_update_callback);
}
#if defined(VTSS_SW_OPTION_DDMI)
static void EW_thread_register_ddmi_state_status(void)
{
    /* Update ddmistate status via callback function which register to PORT module */
    ddmi_state_change_register(VTSS_MODULE_ID_EVENT_WARNING, EW_ddmi_state_status_update_callback);
}
#endif
static void EW_thread_update_pwr_status(void)
{
	int etype;

	EW_CRIT_ENTER();

	g_event.power.curr_status = event_warning_power_status_get(EVENT_POWER_BOTH);

	for(etype = EVENT_EXECUTER_RELAY; etype < EVENT_EXECUTER_MAX; etype++) {
		if((g_event.power.old_status & g_event.power.monitorred_mask[etype]) !=
			(g_event.power.curr_status & g_event.power.monitorred_mask[etype])) {
			/* all of events(relay, email) uses the same variable 'g_event.power.changed' */
			g_event.power.changed |= true;
		} /* end of if(old_status != curr_status) */

		if(etype == EVENT_EXECUTER_RELAY) {
			if(g_event.power.monitorred_mask[etype] &
				(~(g_event.power.curr_status & g_event.power.monitorred_mask[etype]))) {
				/* some monitorred power loss */
				g_event.power.do_relay = true;
			} else {
				/* all monitorred power plugin */
				g_event.power.do_relay = false;
			}
		} /* end of if(etype == EVENT_EXECUTER_RELAY) */
	} /* end of for */

	EW_CRIT_EXIT();
}

static void EW_thread_update_port_status(void)
{
	int etype;

	EW_CRIT_ENTER();

	for(etype = EVENT_EXECUTER_RELAY; etype < EVENT_EXECUTER_MAX; etype++) {
		/* link_status is updated by callback */
		g_event.port.curr_status = g_event.port.link_status;

		if((g_event.port.old_status & g_event.port.monitorred_mask[etype]) !=
			(g_event.port.curr_status & g_event.port.monitorred_mask[etype])) {
			/* all of events(relay, email) uses the same variable 'g_event.port.changed' */
			g_event.port.changed |= true;
		}/* end of if(old_status != curr_status) */

		if(etype == EVENT_EXECUTER_RELAY) {
			if(g_event.port.monitorred_mask[etype] &
				(~(g_event.port.curr_status & g_event.port.monitorred_mask[etype]))) {
				/* some monitorred port linkdown */
				g_event.port.do_relay = true;
			} else {
				/* all monitorred port back */
				g_event.port.do_relay = false;
			}
		} /* end of if(etype == EVENT_EXECUTER_RELAY) */
	} /* end of for */

	EW_CRIT_EXIT();
}

#if defined(VTSS_SW_OPTION_DDMI)
static void EW_thread_update_ddmi_status(void)
{
	int etype;
	int ddmi_event_type;
	u8 relay_flag=0;

	EW_CRIT_ENTER();

	for(etype = EVENT_EXECUTER_RELAY; etype < EVENT_EXECUTER_MAX; etype++) {
		for(ddmi_event_type=VTSS_APPL_DDMI_EVENT_TYPE_TEMPERATURE;ddmi_event_type<VTSS_APPL_DDMI_EVENT_TYPE_TOTAL;ddmi_event_type++){
			/* ddmi state_status is updated by callback */
			g_event.ddmi_port.curr_status[ddmi_event_type] = g_event.ddmi_port.state_status[ddmi_event_type];

			if((g_event.ddmi_port.old_status[ddmi_event_type] & g_event.ddmi_port.monitorred_mask[etype]) !=
			    (g_event.ddmi_port.curr_status[ddmi_event_type] & g_event.ddmi_port.monitorred_mask[etype])) {
			    /* all of events(relay, email) uses the same variable 'g_event.ddmi_port.changed' */
			    g_event.ddmi_port.changed |= true;
			}/* end of if(old_status != curr_status) */

			if(etype == EVENT_EXECUTER_RELAY) {
			    if(g_event.ddmi_port.curr_status[ddmi_event_type] & g_event.ddmi_port.monitorred_mask[etype]) {
			        relay_flag |= VTSS_BIT(ddmi_event_type);
			    } else {
			        relay_flag &= ~VTSS_BIT(ddmi_event_type);
			    }
			} /* end of if(etype == EVENT_EXECUTER_RELAY) */
		}
	} /* end of for */

	if(relay_flag){
		g_event.ddmi_port.do_relay = true;
	}else{
		g_event.ddmi_port.do_relay = false;
	}

	EW_CRIT_EXIT();
}
#endif

static void EW_thread_update_status(void)
{
    EW_thread_update_pwr_status();
    EW_thread_update_port_status();
#if defined(VTSS_SW_OPTION_DDMI)
    EW_thread_update_ddmi_status();
#endif
}


static bool EW_thread_continue(void)
{
	return !(g_event.port.changed | g_event.power.changed 
 #if defined(VTSS_SW_OPTION_DDMI)
        | g_event.ddmi_port.changed
 #endif
        ) | !g_event.port.first_status_got;
}

#define EW_EMAIL_PORT_FALLING(_iport_idx) \
			(VTSS_BIT(_iport_idx) & \
			g_event.port.monitorred_mask[EVENT_EXECUTER_EMAIL] & \
			g_event.port.old_status & \
			~g_event.port.curr_status)

#define EW_EMAIL_POWER_FALLING(_pwr_idx) \
			(VTSS_BIT(_pwr_idx) & \
			g_event.power.monitorred_mask[EVENT_EXECUTER_EMAIL] & \
			g_event.power.old_status & \
			~g_event.power.curr_status)

#define EW_SYSLOG_POWER_FALLING(_pwr_idx) \
			(VTSS_BIT(_pwr_idx) & \
			g_event.power.old_status & \
			~g_event.power.curr_status)

#define EW_SYSLOG_POWER_RISING(_pwr_idx) \
			(VTSS_BIT(_pwr_idx) & \
			~g_event.power.old_status & \
			g_event.power.curr_status)

static void EW_thread_update_email(void)
{
	u32 iport_idx;
	u8 pwr_idx;
	/* TODO: implement Email(SMTP) */
	/* TODO: VTSS_PORTS need to be replaced by co-version conf of port mapping */
	for(iport_idx = 0; iport_idx < MESA_CAP(MESA_CAP_PORT_CNT); iport_idx++) {
		if(EW_EMAIL_PORT_FALLING(iport_idx))
			T_W("SendEmail: iPort %d Linkdown", iport_idx);
	} /* end of for */

	/* only 2 power input */
	for(pwr_idx = 0; pwr_idx < 2; pwr_idx++) {
		if(EW_EMAIL_POWER_FALLING(pwr_idx))
			T_W("SendEmail: Power %s loss", VTSS_BIT(pwr_idx) == EVENT_POWER_P1? "P1": "P2");
	} /* end of for */
}

static void EW_thread_update_relay(void)
{
	EW_CRIT_ENTER();

	if(g_event.port.do_relay | g_event.power.do_relay 
#if defined(VTSS_SW_OPTION_DDMI)
        | g_event.ddmi_port.do_relay
#endif
        ) {
		/* 5.1 turn relay OPEN if [one of mointorred ports link-down] or [mointorred power pins loss] */
		EW_do_relay(EVENT_RELAY_OPEN);
		lntn_sysled_system_solid_red();
	} else {
		/* 5.2 turn relay CLOSE if [all mointorred ports link-up] and [mointorred power pins plugin] */
		EW_do_relay(EVENT_RELAY_CLOSE);
		lntn_sysled_system_solid_green();
	}

	EW_CRIT_EXIT();
}

static void EW_thread_send_power_syslog_and_trap(void)
{
	u8 pwr_idx;
	/* Send syslog "Port link down" by PORT module itself */
	/* only 2 power input */
	for(pwr_idx = 0; pwr_idx < 2; pwr_idx++) {
		if(EW_SYSLOG_POWER_FALLING(pwr_idx)) {
			T_D("send syslog 'Power %s loss'", VTSS_BIT(pwr_idx) == EVENT_POWER_P1? "P1": "P2");
#ifdef VTSS_SW_OPTION_SYSLOG
			S_W("Power %s loss", VTSS_BIT(pwr_idx) == EVENT_POWER_P1? "P1": "P2");
#endif
#ifdef VTSS_SW_OPTION_SNMP
			snmp_private_mib_powerloss_trap_send(VTSS_BIT(pwr_idx));
#endif
		} else if(EW_SYSLOG_POWER_RISING(pwr_idx)) {
			T_D("send syslog 'Power %s plugin'", VTSS_BIT(pwr_idx) == EVENT_POWER_P1? "P1": "P2");
#ifdef VTSS_SW_OPTION_SYSLOG
			S_W("Power %s plugin", VTSS_BIT(pwr_idx) == EVENT_POWER_P1? "P1": "P2");
#endif
		}
	} /* end of for */
}

#if defined(VTSS_SW_OPTION_DDMI)
static void EW_thread_send_ddmi_syslog_and_trap(void)
{
    int port_idx,port_count= MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT);
    int ddmi_event_type;

    for(port_idx = 0; port_idx < port_count; port_idx++) {
        for(ddmi_event_type=VTSS_APPL_DDMI_EVENT_TYPE_TEMPERATURE;ddmi_event_type<VTSS_APPL_DDMI_EVENT_TYPE_TOTAL;ddmi_event_type++){
            if(VTSS_BIT(port_idx) & ~g_event.ddmi_port.old_status[ddmi_event_type] & g_event.ddmi_port.curr_status[ddmi_event_type]){
                S_W("DDMI Alarm : Port %d  %s(type %d) exceeded the alarm value",port_idx+1,ddmi_syslog_str[ddmi_event_type],ddmi_event_type);
#ifdef VTSS_SW_OPTION_SNMP
                T_D("send trap ddmi port:%d  type:%d %s", port_idx,ddmi_event_type,ddmi_syslog_str[ddmi_event_type]);
                snmp_private_mib_ddmi_trap_send(port_idx,ddmi_event_type);
#endif
            }
        }
    }
}
#endif

static void EW_thread_send_syslog_and_trap(void)
{
    EW_thread_send_power_syslog_and_trap();
#if defined(VTSS_SW_OPTION_DDMI)
    EW_thread_send_ddmi_syslog_and_trap();
#endif

}


static void EW_thread_update_changed_status(void)
{
    int ddmi_event_type;

    g_event.port.old_status = g_event.port.curr_status;
    g_event.power.old_status = g_event.power.curr_status;
#if defined(VTSS_SW_OPTION_DDMI)
    for(ddmi_event_type=VTSS_APPL_DDMI_EVENT_TYPE_TEMPERATURE;ddmi_event_type<VTSS_APPL_DDMI_EVENT_TYPE_TOTAL;ddmi_event_type++)
        g_event.ddmi_port.old_status[ddmi_event_type] = g_event.ddmi_port.curr_status[ddmi_event_type];
#endif
}

static void EW_thread_clear_changed_status(void)
{
	g_event.port.changed = false;
	g_event.power.changed = false;
#if defined(VTSS_SW_OPTION_DDMI)
	g_event.ddmi_port.changed =false;
#endif
}


static void EW_thread(vtss_addrword_t data)
{
	EW_thread_init_relay();
	EW_thread_register_port_link_status();
#if defined(VTSS_SW_OPTION_DDMI)
	EW_thread_register_ddmi_state_status();
#endif
	/* to reslove the port link status will not be updated by PORT module*/
	VTSS_OS_MSLEEP(5000);

	for (;;) {
		T_N("event thread alive!");

		EW_thread_update_changed_status();

		/* STEP.0 sleep 1s */
		VTSS_OS_MSLEEP(1000);

		/* STEP.1 update monitorred status */
		EW_thread_update_status();

		/* STEP.2 send syslog and trap  */
		EW_thread_send_syslog_and_trap();

		/* STEP.3 goto STEP.0 if no change */
		if(EW_thread_continue())
			continue;

		/* STEP.4 send E-Mail(power status) if any change */
		//TODO on next stage
		EW_thread_update_email();

		/* STEP.5 update relay */
		EW_thread_update_relay();

		/* Clear changed status */
        EW_thread_clear_changed_status();
	} /* end of for */
}

/****************************************************************************
 *  Initialization functions
 ****************************************************************************/

/* Initialize module */
mesa_rc event_warning_init(vtss_init_data_t *data)
{
	switch (data->cmd) {
	case INIT_CMD_EARLY_INIT:
		/* Initialize and register trace ressources */
		VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, VTSS_TRACE_GRP_CNT);
		VTSS_TRACE_REGISTER(&trace_reg);
		break;

	case INIT_CMD_INIT:
		T_D("INIT");
		event_warning_icli_cmd_register();
		memset(&g_event, 0, sizeof(evt_obj_t));

		critd_init(&crit, "EventWarning crit", VTSS_MODULE_ID_EVENT_WARNING, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);

#ifdef VTSS_SW_OPTION_ICFG
		event_warning_icfg_init();
#endif
		EW_CRIT_EXIT();

		vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
						EW_thread,
						0,
						"EVENT_WARNING",
						nullptr,
						0,
						&EW_thread_handle,
						&EW_thread_block);

		break;
	case INIT_CMD_START:
		T_D("START");
		break;
	case INIT_CMD_CONF_DEF:
		T_D("CONF");
		EW_icfg_reset_to_default();
		break;
	case INIT_CMD_MASTER_UP:
		T_D("MASTER UP");
		break;
	case INIT_CMD_MASTER_DOWN:
		T_D("MASTER DOWN");
		break;
	case INIT_CMD_SWITCH_ADD:
		T_D("SWITCH ADD");
		break;
	case INIT_CMD_SWITCH_DEL:
		T_D("SWITCH DEL");
		break;
	case INIT_CMD_SUSPEND_RESUME:
		T_D("SUSPEND RESUME");
		break;
	default:
		break;
	}

	T_D("EVENT EXIT");
	return 0;
}
