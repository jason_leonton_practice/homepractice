/***********************************************************
 *
 * Copyright (c) 2018 LEONTON Technologies, Co.Ltd All Rights Reserved.
 *
 * Event Warning Module
 *
 ***********************************************************/

#ifndef _EVENT_WARNING_API_H_
#define _EVENT_WARNING_API_H_

#if defined(VTSS_SW_OPTION_DDMI)
#include "ddmi_api.h"
#endif

typedef enum {
	EVENT_EXECUTER_RELAY = 0,
	EVENT_EXECUTER_EMAIL,

	EVENT_EXECUTER_MAX,
} event_executer_e;

typedef enum {
	EVENT_RELAY_CLOSE = 0,
	EVENT_RELAY_OPEN,

	EVENT_RELAY_MAX,
} event_relay_e;

typedef struct {
	/* Port linkdown */
	u32 link_status; /* link_status: updated by EA_port_link_status_update_callback */

	u32 monitorred_mask[EVENT_EXECUTER_MAX]; /* PortN(VTSS_BIT(N)) */
	u32 old_status;
	u32 curr_status;
	bool changed;
	bool do_relay;

	/* to reslove the port link status will not be updated by PORT module*/
	bool first_status_got;
} evt_obj_port_t;

typedef struct {
	/* Power plugout */
	u8 monitorred_mask[EVENT_EXECUTER_MAX]; /* P1: VTSS_BIT(0), P2: VTSS_BIT(1) */
	u8 old_status;
	u8 curr_status;
	bool changed;
	bool do_relay;
} evt_obj_pwr_t;

#if defined(VTSS_SW_OPTION_DDMI)
typedef struct {
	/* DDMI State */
	u32 state_status[VTSS_APPL_DDMI_EVENT_TYPE_TOTAL]; /* state_status: updated by EW_port_ddmi_state_update_callback */

	u32 monitorred_mask[EVENT_EXECUTER_MAX]; /* PortN(VTSS_BIT(N)) */
	u32 old_status[VTSS_APPL_DDMI_EVENT_TYPE_TOTAL];
	u32 curr_status[VTSS_APPL_DDMI_EVENT_TYPE_TOTAL];
	bool changed;
	bool do_relay;

	/* to reslove the port link status will not be updated by PORT module*/
	bool first_status_got;
} evt_obj_ddmi_port_t;
#endif

typedef struct {
	evt_obj_port_t port;
	evt_obj_pwr_t power;
#if defined(VTSS_SW_OPTION_DDMI)
	evt_obj_ddmi_port_t ddmi_port;
#endif
} evt_obj_t;

#define EVENT_POWER_P1 VTSS_BIT(0)
#define EVENT_POWER_P2 VTSS_BIT(1)
#define EVENT_POWER_BOTH (EVENT_POWER_P1 | EVENT_POWER_P2)

vtss_rc event_warning_init(vtss_init_data_t *data);
void event_warning_port_linkdown_register(event_executer_e etype, u32 iport_list, bool is_warning);
void event_warning_power_loss_register(event_executer_e etype, u8 pwr_list, bool is_warning);
#if defined(VTSS_SW_OPTION_DDMI)
void event_warning_ddmi_state_register(event_executer_e etype, u32 iport_list, bool is_warning);
#endif
vtss_rc event_warning_get_event_obj(evt_obj_t *evt_obj);
mesa_bool_t event_warning_relay_status_get();
u8 event_warning_power_status_get(u8 pwr_list);
BOOL event_warning_ddmi_supported(void);

#endif  /* _EVENT_WARNING_API_H_ */

