#
# Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.
#
# Unpublished rights reserved under the copyright laws of the United States of
# America, other countries and international treaties. Permission to use, copy,
# store and modify, the software and its source code is granted but only in
# connection with products utilizing the Microsemi switch and PHY products.
# Permission is also granted for you to integrate into other products, disclose,
# transmit and distribute the software only in an absolute machine readable
# format (e.g. HEX file) and only in or with products utilizing the Microsemi
# switch and PHY products.  The source code of the software may not be
# disclosed, transmitted or distributed without the prior written permission of
# Microsemi.
#
# This copyright notice must appear in any copy, modification, disclosure,
# transmission or distribution of the software.  Microsemi retains all
# ownership, copyright, trade secret and proprietary rights in the software and
# its source code, including all modifications thereto.
#
# THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
# WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
# ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
# NON-INFRINGEMENT.
#
class PortJson
def port_config_base_get(*params) 
 @client.call("port.config.base.get",*params) 
end
def port_config_base_set(*params) 
 @client.call("port.config.base.set",*params) 
end
def port_status_veriphy_result_get(*params) 
 @client.call("port.status.veriphy.result.get",*params) 
end
def port_status_information_get(*params) 
 @client.call("port.status.information.get",*params) 
end
def port_control_statistics_clear_get(*params) 
 @client.call("port.control.statistics.clear.get",*params) 
end
def port_control_statistics_clear_set(*params) 
 @client.call("port.control.statistics.clear.set",*params) 
end
def port_control_veriphy_start_get(*params) 
 @client.call("port.control.veriphy.start.get",*params) 
end
def port_control_veriphy_start_set(*params) 
 @client.call("port.control.veriphy.start.set",*params) 
end
def port_statistics_rmon_get(*params) 
 @client.call("port.statistics.rmon.get",*params) 
end
def port.statistics.ifGroup.get(*params) 
 @client.call("port.statistics.ifGroup.get",*params) 
end
def port.statistics.ethernetLike.get(*params) 
 @client.call("port.statistics.ethernetLike.get",*params) 
end
def port_statistics_bridge_get(*params) 
 @client.call("port.statistics.bridge.get",*params) 
end
def port_statistics_queues_get(*params) 
 @client.call("port.statistics.queues.get",*params) 
end
end
class PortJson
def port_config_base_get(*params) 
 @client.call("port.config.base.get",*params) 
end
def port_config_base_set(*params) 
 @client.call("port.config.base.set",*params) 
end
def port_status_veriphy_result_get(*params) 
 @client.call("port.status.veriphy.result.get",*params) 
end
def port_status_information_get(*params) 
 @client.call("port.status.information.get",*params) 
end
def port_control_statistics_clear_get(*params) 
 @client.call("port.control.statistics.clear.get",*params) 
end
def port_control_statistics_clear_set(*params) 
 @client.call("port.control.statistics.clear.set",*params) 
end
def port_control_veriphy_start_get(*params) 
 @client.call("port.control.veriphy.start.get",*params) 
end
def port_control_veriphy_start_set(*params) 
 @client.call("port.control.veriphy.start.set",*params) 
end
def port_statistics_rmon_get(*params) 
 @client.call("port.statistics.rmon.get",*params) 
end
def port.statistics.ifGroup.get(*params) 
 @client.call("port.statistics.ifGroup.get",*params) 
end
def port.statistics.ethernetLike.get(*params) 
 @client.call("port.statistics.ethernetLike.get",*params) 
end
def port_statistics_bridge_get(*params) 
 @client.call("port.statistics.bridge.get",*params) 
end
def port_statistics_queues_get(*params) 
 @client.call("port.statistics.queues.get",*params) 
end
end
