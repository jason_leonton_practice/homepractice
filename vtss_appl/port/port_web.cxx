/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

#include "web_api.h"
#include "port_api.h"
#include "msg_api.h"
#include "qos_api.h"
#ifdef VTSS_SW_OPTION_PRIV_LVL
#include "vtss_privilege_web_api.h"
#endif


#define PORT_WEB_BUF_LEN 512

/* =================
 * Trace definitions
 * -------------- */
#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_WEB
#include <vtss_trace_api.h>
/* ============== */

/****************************************************************************/
/*  Module JS lib config routine                                            */
/****************************************************************************/

#define PORT_WEB_BUF_LEN 512

static size_t port_lib_config_js(char **base_ptr, char **cur_ptr, size_t *length)
{
    char buf[PORT_WEB_BUF_LEN];
    (void) snprintf(buf, PORT_WEB_BUF_LEN,
                    "var configPortFrameSizeMin = %d;\n"
                    "var configPortFrameSizeMax = %d;\n",
                    MESA_MAX_FRAME_LENGTH_STANDARD,
                    MESA_CAP(MESA_CAP_PORT_FRAME_LENGTH_MAX));
    return webCommonBufferHandler(base_ptr, cur_ptr, length, buf);
}

/****************************************************************************/
/*  JS lib_config table entry                                               */
/****************************************************************************/

web_lib_config_js_tab_entry(port_lib_config_js);

/****************************************************************************/
/*  Web Handler  functions                                                  */
/****************************************************************************/

#if VTSS_UI_OPT_VERIPHY == 1
static i32 handler_config_veriphy(CYG_HTTPD_STATE* p)
{
    vtss_isid_t               sid = web_retrieve_request_sid(p); /* Includes USID = ISID */
    int                       val;
    mesa_port_no_t            iport;
    vtss_uport_no_t           selected_uport = 0;
    port_isid_port_info_t     info;
    BOOL                      is_post_request_in_disguise = FALSE;
    int                       cnt;
    mesa_phy_veriphy_result_t veriphy_result;
    BOOL                      first = TRUE;
    u32                       port_count = port_isid_port_count(sid);

    if(redirectUnmanagedOrInvalid(p, sid)) /* Redirect unmanaged/invalid access to handler */
        return -1;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_PING))
        return -1;
#endif

    // This function does not support the HTTP POST method because
    // running VeriPHY on a 10/100 connection will cause the PHY to
    // link down, which in turn may cause the browser to not get any
    // reply to a HTTP POST request, causing it to timeout. This is
    // not the case with GET requests. If it's a request, the URL
    // has a "?port=<valid_port_number>". If it's a status get, the URL
    // has a "?port=-1" or no port at all.
    if(p->method == CYG_HTTPD_METHOD_POST) {
        redirect(p, "/veriphy.htm");
        return -1;
    }

    if(cyg_httpd_form_varable_int(p, "port", &val)) {
        selected_uport = val;
        if(selected_uport == 0) {
            is_post_request_in_disguise = TRUE;
        } else {
            if(uport2iport(selected_uport) < port_count)
                is_post_request_in_disguise = TRUE;
        }
    }

    if(is_post_request_in_disguise) {
        CapArray<port_veriphy_mode_t, MESA_CAP_PORT_CNT> veriphy_mode;

        // Fill-in array telling which ports to run VeriPHY on.
        // Even when VeriPHY is already running on a given port, it's safe to
        // call port_mgmt_veriphy_start() with PORT_VERIPHY_MODE_FULL again.
        for (iport = 0; iport < port_count; iport++) {
            if ((selected_uport == 0 || uport2iport(selected_uport) == iport) &&
                port_isid_port_info_get(sid, iport, &info) == VTSS_OK &&
                (info.cap & MEBA_PORT_CAP_1G_PHY)) {
                veriphy_mode[iport] = PORT_VERIPHY_MODE_FULL;
            } else {
                veriphy_mode[iport] = PORT_VERIPHY_MODE_NONE;
            }
        }

        // Don't care about return code.
        (void) port_mgmt_veriphy_start(sid, veriphy_mode.data());
    }

    // Always reply.
    cyg_httpd_start_chunked("html");

    // Only data for ports with a PHY will be transferred to the browser.
    // Format of data:
    // port_1/veriphy_status_1/status_1_1/length_1_1/status_1_2/length_1_2/status_1_3/length_1_3/status_1_4/length_1_4;
    // port_2/veriphy_status_2/status_2_1/length_2_1/status_2_2/length_2_2/status_2_3/length_2_3/status_2_4/length_2_4;
    // ...
    // port_n/veriphy_status_n/status_n_1/length_n_1/status_n_2/length_n_2/status_n_3/length_n_3/status_n_4/length_n_4;
    // If veriphy_status_n is 0, VeriPHY has not been run in this port, and the remaining data is invalid.
    // If veriphy_status_n is 1, VeriPHY is in progress, and the remaining data is invalid.
    // If veriphy_status_n is 2, VeriPHY has been run on this port, and the remaining data is ok.
    for (iport = 0; iport < port_count; iport++) {
        if (port_isid_port_info_get(sid, iport, &info) == VTSS_OK &&
            (info.cap & MEBA_PORT_CAP_1G_PHY)) {
            mesa_rc rc = port_mgmt_veriphy_get(sid, iport, &veriphy_result, 0);
            if(rc == VTSS_OK || rc == VTSS_APPL_PORT_ERROR_VERIPHY_RUNNING || rc == VTSS_APPL_PORT_ERROR_GEN) {
                // Other port errors we don't bother to handle
                cnt = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s%d/%u/%u/%u/%u/%u/%u/%u/%u/%u",
                    first ? "" : ";",
                    iport2uport(iport),
                    rc == VTSS_APPL_PORT_ERROR_GEN ? 0 : (rc == VTSS_APPL_PORT_ERROR_VERIPHY_RUNNING ? 1 : 2),
                    veriphy_result.status[0],
                    veriphy_result.length[0],
                    veriphy_result.status[1],
                    veriphy_result.length[1],
                    veriphy_result.status[2],
                    veriphy_result.length[2],
                    veriphy_result.status[3],
                    veriphy_result.length[3]);
                cyg_httpd_write_chunked(p->outbuffer, cnt);
                first = FALSE;
            }
        }
    }

    cyg_httpd_end_chunked();
    return -1; // Do not further search the file system.
}
#endif /* VTSS_UI_OPT_VERIPHY == 1 */
static i32 handler_stat_ports(CYG_HTTPD_STATE* p)
{
    vtss_isid_t          sid = web_retrieve_request_sid(p); /* Includes USID = ISID */
    mesa_port_no_t       iport;
    mesa_port_counters_t counters;
    int                  ct;
    BOOL                 clear = (cyg_httpd_form_varable_find(p, "clear") != NULL);
    u32                  port_count;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_PORT))
        return -1;
#endif

    cyg_httpd_start_chunked("html");

    if(VTSS_ISID_LEGAL(sid) &&
       msg_switch_exists(sid)) {
        port_count = port_isid_port_count(sid);
        for (iport = 0; iport < port_count; iport++) {
            vtss_ifindex_t ifindex;
            if (vtss_ifindex_from_port(sid, iport, &ifindex) != VTSS_RC_OK) {
                T_E("Could not get ifindex");
            }

            if(clear) {      /* Clear? */
                if(vtss_appl_port_counters_clear(ifindex) != VTSS_OK)
                    break;              /* Most likely stack error - bail out */
                memset(&counters, 0, sizeof(counters)); /* Cheating a little... */
            } else {
                /* Normal read */
                if (vtss_appl_port_counters_get(ifindex, &counters) != VTSS_OK)
                    break;              /* Most likely stack error - bail out */
            }
            /* Output the counters */
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"%s",
                          iport2uport(iport),
                          counters.rmon.rx_etherStatsPkts,
                          counters.rmon.tx_etherStatsPkts,
                          counters.rmon.rx_etherStatsOctets,
                          counters.rmon.tx_etherStatsOctets,
                          counters.if_group.ifInErrors,
                          counters.if_group.ifOutErrors,
                          counters.rmon.rx_etherStatsDropEvents,
                          counters.rmon.tx_etherStatsDropEvents,
                          counters.bridge.dot1dTpPortInDiscards,
                          iport == (port_count - 1) ? "" : "|");
            cyg_httpd_write_chunked(p->outbuffer, ct);
        }
    }

    cyg_httpd_end_chunked();
    return -1; // Do not further search the file system.
}

/* Support for a given feature is encoded like this:
   0: No supported
   1: Supported on other ports
   2: Supported on this port */
static u8 port_feature_support(meba_port_cap_t cap, meba_port_cap_t port_cap, meba_port_cap_t mask)
{
    return ((cap & mask) ? ((port_cap & mask) ? 2 : 1) : 0);
}

/*  BOOL pfc[8] = {1,1,1,0,0,0,0,1} to "0-2,7" */
static char *prio_arr_to_str(BOOL *pfc, char *buf)
{
    u8 i, a = 0, first = 1, b, tot = 0, once = 1;
    for (i = 0; i < 8; i++) {
        if (!pfc[i]) {
            continue;
        }
        if (first) {
            tot = a = + sprintf(buf + a, "%d", i);
            first = 0;
        } else {
            if (pfc[i]) {
                if (pfc[i - 1]) {
                    b = sprintf(buf + a, "-%d", i);
                    if (once) {
                        tot = tot + b;
                        once = 0;
                    }
                } else {
                    a = tot = tot + sprintf(buf + tot, ",%d", i);
                    once = 1;
                }
            }
        }
    }
    if (first)
        *buf = '\0';
    return buf;
}

/* "0-2,7" to BOOL pfc[8] = {1,1,1,0,0,0,0,1} */
static void prio_str_to_arr(char *buf, u32 len, BOOL *pfc)
{
    u8 c, a, indx, dash = 0;
    for (indx = 0; indx < 8; indx++) {
        pfc[indx] = 0;
    }

    for (c = 0; c<len; c++) {
        if (buf[c] == '-') {
            dash = 1;
        } else if (buf[c] == ',') {
            continue;
        } else {
            if (dash) {
                for (a = indx; a < (int)buf[c]-'0'; a++) {
                    pfc[a] = 1;
                }
                dash = 0;
            }
            indx = (int)buf[c]-'0';
            pfc[indx] = 1;
        }
    }
}

static i32 handler_config_ports(CYG_HTTPD_STATE* p)
{
    vtss_isid_t      sid = web_retrieve_request_sid(p); /* Includes USID = ISID */
    vtss_uport_no_t  uport;
    mesa_port_no_t   iport;
    vtss_appl_port_conf_t conf;
    int              ct;
    u32              port_count = port_isid_port_count(sid), i;
    port_isid_info_t info;
    meba_port_cap_t       cap, sfp_cap;
    char             buf[80];

    if(redirectUnmanagedOrInvalid(p, sid)) /* Redirect unmanaged/invalid access to handler */
        return -1;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_PORT))
        return -1;
#endif

    if(p->method == CYG_HTTPD_METHOD_POST) {
        int errors = 0;

        for (iport = 0; iport < port_count; iport++) {
            vtss_appl_port_conf_t newconf;
            uport = iport2uport(iport);
            int enable, autoneg, fiber_speed, speed, fdx, max, val;
            const char *str;
            bool fdx_value, hdx_value, auto_10, auto_100, auto_1000, auto_2500, auto_5g, auto_10g_cop;
            char             buf2[80] = "";
            vtss_ifindex_t ifindex;
            if (vtss_ifindex_from_port(sid, iport, &ifindex) != VTSS_RC_OK) {
                T_E("Could not get ifindex");
            }
            if(vtss_appl_port_conf_get(ifindex, &conf) < 0) {
                T_E("Could not get appl_port_conf_get, sid = %d, iport =%u", sid, iport);
                errors++;   /* Probably stack error */
                continue;
            }
            newconf = conf;
            if((str = cyg_httpd_form_variable_str_fmt(p, NULL, "speed_%d", uport)) &&
                    sscanf(str, "%uA%uA%uA%uA%u", &enable, &autoneg, &speed, &fdx, &fiber_speed) == 5
              ) {
                T_I("Port %2d: enb:%d auto:%d speed:%d fdx:%d fiber:%d", iport, enable, autoneg, speed, fdx, fiber_speed);

                if ((newconf.admin.enable = enable)) {
                    if (!autoneg) {
                        newconf.speed = (mesa_port_speed_t)speed;
                        newconf.fdx = fdx;
                    } else {
                        newconf.speed = MESA_SPEED_AUTO;
                        sfp_cap = port_isid_port_cap(sid, iport);
                        if (sfp_cap & MEBA_PORT_CAP_SFP_DETECT) {
                            newconf.adv_dis = 0;
                        } else {
                            /*Advertise duplex configuration*/
                            newconf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_FDX;
                            newconf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_HDX;
                            fdx_value = cyg_httpd_form_variable_check_fmt(p, "fdx_%d", uport);
                            hdx_value = cyg_httpd_form_variable_check_fmt(p, "hdx_%d", uport);

                            if (fdx_value) {
                                newconf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_FDX;
                            }
                            if (hdx_value) {
                                newconf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_HDX;
                            }
                            /*If none of the duplex is Selected changing it to Default behaviour */
                            if (!fdx_value && !hdx_value) {
                                newconf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_FDX; // Clear the bit
                                newconf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_HDX; // Clear the bit
                            }

                            /*Advertise Speed configuration*/
                            newconf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_10G;
                            newconf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_5G;
                            newconf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_2500M;
                            newconf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_1G;
                            newconf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_100M;
                            newconf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_10M;
                            auto_10 = cyg_httpd_form_variable_check_fmt(p, "speed_10_%d", uport);
                            auto_100 = cyg_httpd_form_variable_check_fmt(p, "speed_100_%d", uport);
                            auto_1000 = cyg_httpd_form_variable_check_fmt(p, "speed_1000_%d", uport);
                            auto_2500 = cyg_httpd_form_variable_check_fmt(p, "speed_2500_%d", uport);
                            auto_5g = cyg_httpd_form_variable_check_fmt(p, "speed_5g_%d", uport);
                            auto_10g_cop = cyg_httpd_form_variable_check_fmt(p, "speed_10g_cop_%d", uport);

                            if (auto_10g_cop) {
                                newconf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_10G; // Clear the bit
                            }
                            if (auto_5g) {
                                newconf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_5G; // Clear the bit
                            }
                            if (auto_2500) {
                                newconf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_2500M; // Clear the bit
                            }
                            if (auto_1000) {
                                newconf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_1G; // Clear the bit
                            }
                            if (auto_100) {
                                newconf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_100M; // Clear the bit
                            }
                            if (auto_10) {
                                newconf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_10M; // Clear the bit
                            }

                            /*If none of the speed is Selected changing it to Default behaviour */
                            if (!(auto_10 || auto_100 || auto_1000 || auto_2500 || auto_5g || auto_10g_cop)) {
                                newconf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_10G;
                                newconf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_5G;
                                newconf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_2500M;
                                newconf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_1G;
                                newconf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_100M;
                                newconf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_10M;
                            }
                        }
                    }
                    newconf.dual_media_fiber_speed = (mesa_fiber_port_speed_t)fiber_speed; // We must make sure that number in htm corresponds to the number defined in mesa_fiber_port_speed_t
                }
                T_I("Port:%d, conf speed:%d", iport, newconf.speed);
            }

            /* flow_%d: CHECKBOX */
            newconf.flow_control = cyg_httpd_form_variable_check_fmt(p, "flow_%d", uport);

            /* max_%d: TEXT */
            if (cyg_httpd_form_variable_int_fmt(p, &max, "max_%d", uport) &&
               max >= MESA_MAX_FRAME_LENGTH_STANDARD &&
                max <= MESA_CAP(MESA_CAP_PORT_FRAME_LENGTH_MAX)) {
                newconf.max_length = max;
            }

            /* exc_%d: INT */
            if(cyg_httpd_form_variable_int_fmt(p, &val, "exc_%d", uport))
                newconf.exc_col_cont = val;


            /* frame length check CHECKBOX */
            newconf.frame_length_chk = cyg_httpd_form_variable_check_fmt(p, "frm_len_chk_%d", uport);

            /* pfc checkbox and prio_range */
            if (cyg_httpd_form_variable_check_fmt(p, "pfc_ena_%d", uport)) {
                char pfc_range[15] = "";
                size_t len;
                const char *var_string;
                sprintf(pfc_range,"pfc_range_%d",uport);
                var_string = cyg_httpd_form_varable_string(p, pfc_range, &len);
                if (len > 0) {
                    (void) cgi_unescape(var_string, buf2, len, len);
                    prio_str_to_arr(buf2, len, newconf.pfc);
                }
            } else {
                for (i = 0; i < VTSS_PRIOS; i++) {
                    newconf.pfc[i] = 0;
                }
            }

            if(memcmp(&newconf, &conf, sizeof(newconf)) != 0) {
                T_D("appl_port_conf_set(%u,%d)", sid, iport);
                if (vtss_appl_port_conf_set(ifindex, &newconf) < 0) {
                    T_E("Could not set appl_port_conf_get, sid = %d, iport =%u", sid, iport);
                    errors++; /* Probably stack error */
                }
            }
        }
        T_D("errors = %d", errors);

        redirect(p, errors ? STACK_ERR_URL : "/ports.htm");
    } else {                    /* CYG_HTTPD_METHOD_GET (+HEAD) */
        cyg_httpd_start_chunked("html");

        cap = (port_isid_info_get(sid, &info) == VTSS_RC_OK ? info.cap : 0);
        for (iport = 0; iport < port_count; iport++) {
            vtss_appl_port_status_t port_status;
            mesa_port_status_t *status = &port_status.status;
            port_vol_status_t  vol_status;
            BOOL               rx_pause, tx_pause, fdx, hdx, auto_10, auto_100, auto_1000, auto_2500, auto_5g, auto_10g_cop;
            BOOL               phy_10g, pfc_cap = FALSE;
            char               buf2[80] = "";

            vtss_ifindex_t ifindex;
            if (vtss_ifindex_from_port(sid, iport, &ifindex) != VTSS_RC_OK) {
                T_E("Could not get ifindex");
            };

            if(vtss_appl_port_conf_get(ifindex, &conf) < 0 ||
               vtss_appl_port_status_get(ifindex, &port_status) < 0)
                break;          /* Probably stack error - bail out */
            if(!port_isid_port_no_is_stack(sid, iport)) {
                if (status->link)
                    strcpy(buf, port_mgmt_mode_txt(iport, status->speed, status->fdx, port_status.status.fiber));
                else if (port_vol_status_get(PORT_USER_CNT, sid, iport, &vol_status) == VTSS_OK &&
                         (vol_status.conf.disable || vol_status.conf.disable_adm_recover) && vol_status.user != PORT_USER_STATIC)
                    sprintf(buf, "Down (%s)", vol_status.name);
                else
                    strcpy(buf, "Down");

                rx_pause = (conf.speed == MESA_SPEED_AUTO ? (status->link ? status->aneg.obey_pause : 0) :
                            conf.flow_control);
                tx_pause = (conf.speed == MESA_SPEED_AUTO ? (status->link ? status->aneg.generate_pause : 0) :
                            conf.flow_control);
                fdx = (conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_FDX) ? FALSE : TRUE;
                hdx = (conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_HDX) ? FALSE : TRUE;
                auto_10 = (conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_10M) ? FALSE : TRUE;
                auto_100 = (conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_100M) ? FALSE : TRUE;
                auto_1000 = (conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_1G) ? FALSE : TRUE;
                auto_2500 = (conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_2500M) ? FALSE : TRUE;
                auto_5g = (conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_5G) ? FALSE : TRUE;
                auto_10g_cop = (conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_10G) ? FALSE : TRUE;
                pfc_cap = MESA_CAP(MESA_CAP_PORT_PFC);
                phy_10g = mesa_phy_10G_is_valid(NULL, iport);
                sfp_cap = port_isid_port_cap(sid, iport);
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                              "%u/" VPRI64u "/%u/%u/%u/%u/%u/%u/%u/%s/%s/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%u/%s/%u/%u/%u/%u|",
                              iport2uport(iport),                                               // 0
                              /* BZ#22390: We dont't consider stacking now, so we use vtss_board_port_cap() to get local cap on SFP port */
                              (sfp_cap & MEBA_PORT_CAP_SFP_ONLY) ? vtss_board_port_cap(iport) : port_status.cap,  // 1
                              conf.admin.enable,                                                // 2
                              conf.speed == MESA_SPEED_AUTO,                                    // 3
                              conf.speed,                                                       // 4
                              conf.fdx,                                                         // 5
                              conf.max_length,                                                  // 6
                              port_feature_support(cap, port_status.cap, MEBA_PORT_CAP_FLOW_CTRL),   // 7
                              conf.flow_control,                                                // 8
                              status->link ? "Up" : "Down",                                     // 9
                              buf,                                                              // 10
                              rx_pause,                                                         // 11
                              tx_pause,                                                         // 12
                              port_feature_support(cap, port_status.cap, MEBA_PORT_CAP_HDX),         // 13
                              conf.exc_col_cont,                                                // 14
                              0, // Power savings Legacy - Not used anymore                     // 15
                              0, // Power savings Legacy - Not used anymore                     // 16
                              port_feature_support(cap, port_status.cap, MEBA_PORT_CAP_TRI_SPEED_DUAL_ANY_FIBER), // 17
                              conf.dual_media_fiber_speed,                                      // 18
                              fdx,                                                              // 19
                              hdx,                                                              // 20
                              auto_10,                                                          // 21
                              auto_100,                                                         // 22
                              auto_1000,                                                        // 23
                              phy_10g,                                                          // 24
                              (port_isid_port_cap(sid, iport) & MEBA_PORT_CAP_SFP_DETECT) ? 1 : 0,   // 25
                              pfc_cap,                                                          // 26
                              prio_arr_to_str(conf.pfc, buf2),                                  // 27
                              conf.frame_length_chk,                                            // 28
                              auto_2500,                                                        // 29
                              auto_5g,                                                          // 30
                              auto_10g_cop);                                                    // 31

                cyg_httpd_write_chunked(p->outbuffer, ct);
            }
        }
        cyg_httpd_end_chunked();
    }

    return -1; // Do not further search the file system.
}

// handler_stat_port()
static i32 handler_stat_port(CYG_HTTPD_STATE* p)
{
    vtss_isid_t          sid = web_retrieve_request_sid(p); /* Includes USID = ISID */
    mesa_port_no_t       iport = 0;
    mesa_port_counters_t counters;
    int ct, val;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_PORT))
        return -1;
#endif

    cyg_httpd_start_chunked("html");

    if(!VTSS_ISID_LEGAL(sid) ||
       !msg_switch_exists(sid))
        goto out;             /* Most likely stack error - bail out */

    if(cyg_httpd_form_varable_int(p, "port", &val))
        iport = uport2iport(val);
    if(iport > port_isid_port_count(sid))
        iport = 0;

    vtss_ifindex_t ifindex;
    if (vtss_ifindex_from_port(sid, iport, &ifindex) != VTSS_RC_OK) {
        T_E("Could not get ifindex");
    };

    if((cyg_httpd_form_varable_find(p, "clear") != NULL)) {          /* Clear? */
        if(vtss_appl_port_counters_clear(ifindex) != VTSS_OK)
            goto out;         /* Most likely stack error - bail out */
        memset(&counters, 0, sizeof(counters)); /* Cheating a little... */
    } else
        /* Normal read */
        if (vtss_appl_port_counters_get(ifindex, &counters) != VTSS_OK)
            goto out;         /* Most likely stack error - bail out */

    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u,%u,%d/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u,
                  iport2uport(iport),
                  port_isid_port_count(sid),
                  1,            /* STD counters */
                  /* 1 */ counters.rmon.rx_etherStatsPkts,
                  /* 2 */ counters.rmon.tx_etherStatsPkts,
                  /* 3 */ counters.rmon.rx_etherStatsOctets,
                  /* 4 */ counters.rmon.tx_etherStatsOctets,
                  /* 5 */ counters.rmon.rx_etherStatsDropEvents,
                  /* 6 */ counters.rmon.tx_etherStatsDropEvents,
                  /* 7 */ counters.if_group.ifInErrors,
                  /* 8 */ counters.if_group.ifOutErrors);
    cyg_httpd_write_chunked(p->outbuffer, ct);
    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%d/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u,
                  2,            /* CAST counters */
                  /* 1 */ counters.if_group.ifInUcastPkts,
                  /* 2 */ counters.if_group.ifOutUcastPkts,
                  /* 3 */ counters.rmon.rx_etherStatsMulticastPkts,
                  /* 4 */ counters.rmon.tx_etherStatsMulticastPkts,
                  /* 5 */ counters.rmon.rx_etherStatsBroadcastPkts,
                  /* 6 */ counters.rmon.tx_etherStatsBroadcastPkts);
    cyg_httpd_write_chunked(p->outbuffer, ct);
    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%d/" VPRI64u"/" VPRI64u,
                  3,            /* PAUSE counters */
                  /* 1 */ counters.ethernet_like.dot3InPauseFrames,
                  /* 2 */ counters.ethernet_like.dot3OutPauseFrames);
    cyg_httpd_write_chunked(p->outbuffer, ct);
    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%d/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u,
                  4,            /* RMON ADV counters */
                  /* 1 */ counters.rmon.rx_etherStatsPkts64Octets,
                  /* 2 */ counters.rmon.tx_etherStatsPkts64Octets,
                  /* 3 */ counters.rmon.rx_etherStatsPkts65to127Octets,
                  /* 4 */ counters.rmon.tx_etherStatsPkts65to127Octets,
                  /* 5 */ counters.rmon.rx_etherStatsPkts128to255Octets,
                  /* 6 */ counters.rmon.tx_etherStatsPkts128to255Octets,
                  /* 7 */ counters.rmon.rx_etherStatsPkts256to511Octets,
                  /* 8 */ counters.rmon.tx_etherStatsPkts256to511Octets,
                  /* 9 */ counters.rmon.rx_etherStatsPkts512to1023Octets,
                  /* 10 */ counters.rmon.tx_etherStatsPkts512to1023Octets,
                  /* 11 */ counters.rmon.rx_etherStatsPkts1024to1518Octets,
                  /* 12 */ counters.rmon.tx_etherStatsPkts1024to1518Octets,
                  /* 13 */ counters.rmon.rx_etherStatsCRCAlignErrors,
                  /* 14 */ counters.if_group.ifOutErrors,
                  /* 15 */ counters.rmon.rx_etherStatsUndersizePkts,
                  /* 16 */ counters.rmon.rx_etherStatsOversizePkts,
                  /* 17 */ counters.rmon.rx_etherStatsFragments,
                  /* 18 */ counters.rmon.rx_etherStatsJabbers);
    cyg_httpd_write_chunked(p->outbuffer, ct);
    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%d/" VPRI64u"/" VPRI64u,
                  5,            /* JUMBO counters */
                  /* 1 */ counters.rmon.rx_etherStatsPkts1519toMaxOctets,
                  /* 2 */ counters.rmon.tx_etherStatsPkts1519toMaxOctets);
    cyg_httpd_write_chunked(p->outbuffer, ct);

    if (MESA_CAP(MESA_CAP_PORT_CNT_ETHER_LIKE)) {
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%d/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u,
                      6,            /* ETHER_LIKE counters */
                      /* 1 */ counters.rmon.rx_etherStatsCRCAlignErrors,
                      /* 2 */ counters.ethernet_like.dot3StatsLateCollisions,
                      /* 3 */ counters.ethernet_like.dot3StatsSymbolErrors,
                      /* 4 */ counters.ethernet_like.dot3StatsExcessiveCollisions,
                      /* 5 */ counters.rmon.rx_etherStatsUndersizePkts,
                      /* 6 */ counters.ethernet_like.dot3StatsCarrierSenseErrors);
        cyg_httpd_write_chunked(p->outbuffer, ct);
    }

    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%d/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u,
                  7,
                  counters.prio[0].rx,
                  counters.prio[0].tx,
                  counters.prio[1].rx,
                  counters.prio[1].tx,
                  counters.prio[2].rx,
                  counters.prio[2].tx,
                  counters.prio[3].rx,
                  counters.prio[3].tx,
                  counters.prio[4].rx,
                  counters.prio[4].tx,
                  counters.prio[5].rx,
                  counters.prio[5].tx,
                  counters.prio[6].rx,
                  counters.prio[6].tx,
                  counters.prio[7].rx,
                  counters.prio[7].tx);
    cyg_httpd_write_chunked(p->outbuffer, ct);

    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%d/" VPRI64u,
                  8,
                  counters.bridge.dot1dTpPortInDiscards);
    cyg_httpd_write_chunked(p->outbuffer, ct);

#if defined(VTSS_SW_OPTION_QOS_ADV)
    if (MESA_CAP(MESA_CAP_QOS_FRAME_PREEMPTION)) {
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%d/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/" VPRI64u"/%s/" VPRI64u"/%s",
                9,
                counters.dot3br.aMACMergeFragCountRx,
                counters.dot3br.aMACMergeFragCountTx,
                counters.dot3br.aMACMergeFrameAssOkCount,
                counters.dot3br.aMACMergeHoldCount,
                counters.dot3br.aMACMergeFrameAssErrorCount,
                "",
                counters.dot3br.aMACMergeFrameSmdErrorCount,
                "");
        cyg_httpd_write_chunked(p->outbuffer, ct);
    }
#endif /* VTSS_SW_OPTION_QOS_ADV */

 out:
    cyg_httpd_end_chunked();
    return -1; // Do not further search the file system.
}

static notice_callback_t port_web_notice_callback = NULL;

void port_web_set_notice_callback(notice_callback_t new_callback_function)
{
    if (port_web_notice_callback && new_callback_function)
        T_E("Notice callback being overridden (%p -> %p)",
            port_web_notice_callback, new_callback_function);

    port_web_notice_callback = new_callback_function;
}

notice_callback_t port_web_get_notice_callback(void)
{
    return port_web_notice_callback;
}

/*
 * Toplevel portstate handler
 */
static i32 handler_stat_portstate(CYG_HTTPD_STATE* p)
{
    int cnt;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_PORT))
        return -1;
#endif

    cyg_httpd_start_chunked("html");

    switch (MESA_CAP(MESA_CAP_MISC_CHIP_FAMILY)) {
        case MESA_CHIP_FAMILY_CARACAL:
            stat_portstate_switch_lu26(p, VTSS_USID_START, VTSS_ISID_START);
            break;
        case MESA_CHIP_FAMILY_SERVAL:
        case MESA_CHIP_FAMILY_OCELOT:
            stat_portstate_switch_serval(p, VTSS_USID_START, VTSS_ISID_START);
            break;
        case MESA_CHIP_FAMILY_SERVALT:
        case MESA_CHIP_FAMILY_JAGUAR2:
            stat_portstate_switch_jr2(p, VTSS_USID_START, VTSS_ISID_START);
            break;
        default:
            T_E("Unsupported chip family: %d", MESA_CAP(MESA_CAP_MISC_CHIP_FAMILY));
            return -1;
    }

    if (port_web_notice_callback)
    {
        p->outbuffer[0] = ';'; // field separator
        cnt = port_web_notice_callback(p->outbuffer + 1, sizeof(p->outbuffer) - 1);
        if (cnt > 0)
            cyg_httpd_write_chunked(p->outbuffer, cnt);
    }

    cyg_httpd_end_chunked();

    return -1; // Do not further search the file system.
}

/*
 * Get port types of the stack switches.
 */
void port_type_supported(BOOL *p_has_rj45_port,
                         BOOL *p_has_sfp_port,
                         BOOL *p_has_x2_port) {
    switch_iter_t       sit;
    port_isid_info_t    info;
    meba_port_cap_t          cap;

    *p_has_rj45_port = *p_has_sfp_port = *p_has_x2_port = FALSE;

    (void)switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_USID_CFG);

    while (switch_iter_getnext(&sit)) {
        cap = (port_isid_info_get(sit.isid, &info) == VTSS_RC_OK ? info.cap : 0);

        if ((!*p_has_rj45_port) &&
            (cap & (MEBA_PORT_CAP_COPPER |
                  MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER | MEBA_PORT_CAP_DUAL_SFP_DETECT))) {
            *p_has_rj45_port = TRUE;
        }
        if ((!*p_has_sfp_port) &&
            (cap & (MEBA_PORT_CAP_FIBER | MEBA_PORT_CAP_SFP_DETECT | MEBA_PORT_CAP_SFP_ONLY |
                   MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER | MEBA_PORT_CAP_DUAL_SFP_DETECT))) {
            *p_has_sfp_port = TRUE;
        }
        if ((!*p_has_x2_port) &&
            (cap & (MEBA_PORT_CAP_10G_FDX | MEBA_PORT_CAP_VTSS_10G_PHY))) {
            *p_has_x2_port = TRUE;
        }

        if (!(!*p_has_rj45_port || !*p_has_sfp_port || !*p_has_x2_port)) {
            break;
        }
    }

    return;
}

/****************************************************************************/
/*  Module Filter CSS routine                                               */
/****************************************************************************/
static size_t port_lib_filter_css(char **base_ptr, char **cur_ptr, size_t *length)
{
    port_isid_info_t info;
    meba_port_cap_t       cap;
    BOOL             power = 0;
    BOOL             switch_has_rj45_port,
                     switch_has_sfp_port,
                     switch_has_x2_port;

    cap = (port_isid_info_get(VTSS_ISID_LOCAL, &info) == VTSS_RC_OK ? info.cap : 0);
#ifdef VTSS_SW_OPTION_PHY_POWER_CONTROL
    if (cap & MEBA_PORT_CAP_1G_PHY)
        power = 1;
#endif /* VTSS_SW_OPTION_PHY_POWER_CONTROL */

    port_type_supported(&switch_has_rj45_port, &switch_has_sfp_port, &switch_has_x2_port);

    char buff[PORT_WEB_BUF_LEN];
    (void) snprintf(buff, PORT_WEB_BUF_LEN,
                    "%s%s%s%s%s%s%s%s%s",
                    power ? "" : ".PHY_PWR_CTRL { display: none; }\r\n",
                    cap & MEBA_PORT_CAP_FLOW_CTRL ? "" : ".PORT_FLOW_CTRL { display: none; }\r\n",
                    cap & MEBA_PORT_CAP_HDX ? "" : ".PORT_EXC_CONT { display: none; }\r\n",
                    cap &  MEBA_PORT_CAP_ANY_FIBER ? "" : ".PORT_FIBER_CTRL { display: none; }\r\n",
                    cap &  MEBA_PORT_CAP_DUAL_COPPER ? ".PORT_1000X_AMS_FIBER_PREFERRED { display: none; }\r\n.PORT_100FX_AMS_FIBER_PREFERRED { display: none; }\r\n" : ".PORT_1000X_AMS_COPPRT_PREFERRED { display: none; }\r\n.PORT_100FX_AMS_COPPRT_PREFERRED { display: none; }\r\n",
                    switch_has_rj45_port == TRUE ? "" : ".switch_has_rj45_port { display: none; }\r\n",
                    switch_has_sfp_port == TRUE ? "" : ".switch_has_sfp_port { display: none; }\r\n",
                    switch_has_x2_port == TRUE ? "" : ".switch_has_x2_port { display: none; }\r\n",
                    cap & MEBA_PORT_CAP_2_5G_FDX ? "" : ".switch_has_2_5g_port { display: none; }\r\n");
    return webCommonBufferHandler(base_ptr, cur_ptr, length, buff);
}
/****************************************************************************/
/*  Filter CSS table entry                                                  */
/****************************************************************************/
web_lib_filter_css_tab_entry(port_lib_filter_css);

/****************************************************************************/
/*  HTTPD Table Entries                                                     */
/****************************************************************************/
/****************************************************************************/
/*  HTTPD Table Entries                                                     */
/****************************************************************************/

#if VTSS_UI_OPT_VERIPHY == 1
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_config_veriphy, "/config/veriphy", handler_config_veriphy);
#endif /* VTSS_UI_OPT_VERIPHY == 1 */
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_stat_ports, "/stat/ports", handler_stat_ports);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_config_ports, "/config/ports", handler_config_ports);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_stat_port, "/stat/port", handler_stat_port);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_stat_portstate, "/stat/portstate", handler_stat_portstate);

/****************************************************************************/
/*  End of file.                                                            */
/****************************************************************************/
