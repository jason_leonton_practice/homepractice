/*
 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
*/

#include "snmp.hxx"  // For CYG_HTTPD_STATE
#include "vtss/appl/port.h"
#include "port_serializer.hxx"
#include "port_trace.h"
#include "port.h"
#include "vtss_os_wrapper.h"
#ifdef VTSS_SW_OPTION_PRIVATE_MIB_GEN
#include "web_api.h"
#endif
#ifdef VTSS_SW_OPTION_QOS
#include "qos_api.h"  // For VTSS_SW_OPTION_QOS_ADV
#endif


// Dummy function because the "Write Only" OID is implemented with a
// TableReadWrite
mesa_rc port_stats_dummy_get(vtss_ifindex_t ifindex, BOOL *const clear) {
    if (clear && *clear) {
        *clear = FALSE;
    }
    return VTSS_RC_OK;
}

#ifdef VTSS_UI_OPT_VERIPHY
// Wrapper for starting VeriPHy, converting from ifindex to isid, port
// In : ifindex - OID ifindex
//      start   - Set to TRUE to start VeriPhy for the select ifindex interface
mesa_rc port_veriphy_start_set(vtss_ifindex_t ifindex,
                               const BOOL *const start) {
    vtss_ifindex_elm_t ife;

    if (start && *start) {  // Make sure the Start is not a NULL pointer
        CapArray<port_veriphy_mode_t, MESA_CAP_PORT_CNT> mode;

        if (!vtss_ifindex_is_port(ifindex)) {
            return VTSS_RC_ERROR;
        }

        VTSS_RC(vtss_ifindex_decompose(ifindex, &ife));

        // Start by setting all ports to NOT run veriphy, because the mgmt
        // veriphy function takes a port boolean array.
        port_iter_t pit;
        VTSS_RC(port_iter_init(&pit, NULL, ife.isid, PORT_ITER_SORT_ORDER_UPORT,
                               PORT_ITER_FLAGS_FRONT | PORT_ITER_FLAGS_NPI));


        while (port_iter_getnext(&pit)) {
            mode[pit.iport] = PORT_VERIPHY_MODE_NONE;
        }

        port_isid_port_info_t info;
        // Start veriphy at the selected port.
        mode[ife.ordinal] = (port_isid_port_info_get(ife.isid, ife.ordinal,
                                                     &info) != VTSS_OK ||
                                             (info.cap & MEBA_PORT_CAP_1G_PHY) == 0
                                     ? PORT_VERIPHY_MODE_NONE
                                     : PORT_VERIPHY_MODE_FULL);

        VTSS_RC(port_mgmt_veriphy_start(ife.isid, mode.data()));
    }
    return VTSS_RC_OK;
}

// Wrapper for starting VeriPHy, converting from ifindex to isid, port
// In : ifindex - OID ifindex
//      start   - Set to TRUE to start VeriPhy for the select ifindex interface
mesa_rc port_veriphy_result_get(vtss_ifindex_t ifindex,
                                vtss_appl_port_veriphy_result_t *port_result) {
    if (!vtss_ifindex_is_port(ifindex)) {
        return VTSS_RC_ERROR;
    }
    mesa_phy_veriphy_result_t phy_result;
    vtss_ifindex_elm_t ife;
    VTSS_RC(vtss_ifindex_decompose(ifindex, &ife));

    (void)vtss_port_mgmt_veriphy_get(ife.isid, ife.ordinal, &phy_result,
                                     1);  // Run for maximum 1 sec.
    T_NG_PORT(TRACE_GRP_MIB, ife.ordinal,
              "Getting VeriPhy result, phy_result.status[0]:%d",
              phy_result.status[0]);
    T_NG_PORT(TRACE_GRP_MIB, ife.ordinal,
              "Getting VeriPhy result, phy_result.status[1]:%d",
              phy_result.status[1]);
    T_NG_PORT(TRACE_GRP_MIB, ife.ordinal,
              "Getting VeriPhy result, phy_result.status[2]:%d",
              phy_result.status[2]);
    T_NG_PORT(TRACE_GRP_MIB, ife.ordinal,
              "Getting VeriPhy result, phy_result.status[3]:%d",
              phy_result.status[3]);

    port_result->status_pair_a = phy_result.status[0];
    port_result->status_pair_b = phy_result.status[1];
    port_result->status_pair_c = phy_result.status[2];
    port_result->status_pair_d = phy_result.status[3];
    port_result->length_pair_a = phy_result.length[0];
    port_result->length_pair_b = phy_result.length[1];
    port_result->length_pair_c = phy_result.length[2];
    port_result->length_pair_d = phy_result.length[3];
    return VTSS_RC_OK;
}

#endif  // VTSS_UI_OPT_VERIPHY

// Wrapper for clear counters, converting from ifindex to isid, port
// In : ifindex - OID ifindex
//      clear   - Set to TRUE to clear counters for the select ifindex interface
mesa_rc port_stats_clr_set(vtss_ifindex_t ifindex, const BOOL *const clear) {
    if (clear && *clear) {  // Make sure the Clear is not a NULL pointer
        if (!vtss_ifindex_is_port(ifindex)) {
            return VTSS_RC_ERROR;
        }

        VTSS_RC(vtss_appl_port_counters_clear(ifindex));
    }
    return VTSS_RC_OK;
}

// Wrapper for converting from ifindex to isid,iport for configuration get
mesa_rc vtss_port_mib_mgmt_conf_get(vtss_ifindex_t ifindex,
                                    vtss_appl_port_mib_conf_t *port_mib_conf) {
    if (!vtss_ifindex_is_port(ifindex)) {
        return VTSS_RC_ERROR;
    }

    vtss_ifindex_elm_t ife;
    VTSS_RC(vtss_ifindex_decompose(ifindex, &ife));

    vtss_appl_port_conf_t port_conf;
    VTSS_RC(vtss_appl_port_conf_get(ifindex, &port_conf));

    port_mib_conf->advertise_dis = 0;
    // Wrapper between MIB and "old" port configuration
    if (port_conf.speed == MESA_SPEED_AUTO) {
        port_mib_conf->speed = VTSS_APPL_PORT_SPEED_AUTO;
        port_mib_conf->advertise_dis = port_conf.adv_dis;
    } else if (port_conf.speed ==  MESA_SPEED_1G) {
        port_mib_conf->speed = VTSS_APPL_PORT_SPEED_1G_FDX;
    } else if (port_conf.speed ==  MESA_SPEED_10M) {
        port_mib_conf->speed = port_conf.fdx ? VTSS_APPL_PORT_SPEED_10M_FDX
                                             : VTSS_APPL_PORT_SPEED_10M_HDX;
    } else if (port_conf.speed ==  MESA_SPEED_100M) {
        port_mib_conf->speed = port_conf.fdx ? VTSS_APPL_PORT_SPEED_100M_FDX
                                             : VTSS_APPL_PORT_SPEED_100M_HDX;
    } else if (port_conf.speed ==  MESA_SPEED_2500M) {
        port_mib_conf->speed = VTSS_APPL_PORT_SPEED_2500M_FDX;
    } else if (port_conf.speed ==  MESA_SPEED_5G) {
        port_mib_conf->speed = VTSS_APPL_PORT_SPEED_5G_FDX;
    } else if (port_conf.speed ==  MESA_SPEED_10G) {
        port_mib_conf->speed = VTSS_APPL_PORT_SPEED_10G_FDX;
    } else if (port_conf.speed ==  MESA_SPEED_12G) {
        port_mib_conf->speed = VTSS_APPL_PORT_SPEED_12G_FDX;
    } else {
        T_E("Unexpected port configuration: speed=%d, duplex=%d, adv_dis=%d",
            port_conf.speed, port_conf.fdx, port_conf.adv_dis);
    }
    T_DG_PORT(TRACE_GRP_MIB, ife.ordinal,
              "Port configuration: speed=%d, duplex=%d, adv_dis=%d maps to: speed=%d",
              port_conf.speed, port_conf.fdx, port_conf.adv_dis, port_mib_conf->speed);

    // Wrapper between MIB and "old" port configuration for media type
    BOOL rj45;
    BOOL sfp;

    VTSS_RC(vtss_port_conf2media(&rj45, &sfp, &port_conf, ife.isid,
                                 ife.ordinal));

    if (rj45 & !sfp) {
        port_mib_conf->media = VTSS_APPL_PORT_MEDIA_CU;
    } else if (rj45 & sfp) {
        port_mib_conf->media = VTSS_APPL_PORT_MEDIA_DUAL;
    } else {
        port_mib_conf->media = VTSS_APPL_PORT_MEDIA_SFP;
    }

    // Wrapper between MIB and "old" port configuration for flow control
    port_mib_conf->fc = port_conf.flow_control ? VTSS_APPL_PORT_FC_ON
                                               : VTSS_APPL_PORT_FC_OFF;

    // Wrapper for Priority Flow control
    u8 q;
    for (port_mib_conf->pfc_mask = 0, q = 0; q < VTSS_PRIOS; q++) {
      port_mib_conf->pfc_mask |= port_conf.pfc[q] ? (1 << q) : 0;
    }
    // Wrapper for enable
    port_mib_conf->shutdown = !port_conf.admin.enable;

    // Wrapper for MTU
    port_mib_conf->mtu = port_conf.max_length;

    // Wrapper for frame length check
    port_mib_conf->frame_length_chk = port_conf.frame_length_chk;

    // Wrapper for Excessive
    port_mib_conf->excessive = port_conf.exc_col_cont;

    return VTSS_RC_OK;
}

// Wrappere For converting from ifindex to isid,iport for configuration set
mesa_rc vtss_mib_port_conf_set(vtss_ifindex_t ifindex,
                               const vtss_appl_port_mib_conf_t *port_mib_conf) {
    vtss_appl_port_speed_t mib_speed = port_mib_conf->speed;
    if (!vtss_ifindex_is_port(ifindex)) {
        return VTSS_RC_ERROR;
    }

    vtss_ifindex_elm_t ife;

    VTSS_RC(vtss_ifindex_decompose(ifindex, &ife));
    vtss_appl_port_mib_conf_t current_port_mib_conf;

    VTSS_RC(vtss_port_mib_mgmt_conf_get(ifindex, &current_port_mib_conf));

    // Wrapper between MIB and "old" port configuration
    vtss_appl_port_conf_t port_conf;
    VTSS_RC(vtss_appl_port_conf_get(ifindex, &port_conf));

    // Wrapper between MIB and "old" port configuration for media type.
    if (current_port_mib_conf.media !=
        port_mib_conf->media) {  // Because media type and speed depend upon
                                 // each other we only do this if this parameter
                                 // has changed
        switch (port_mib_conf->media) {
        case VTSS_APPL_PORT_MEDIA_CU:
            VTSS_RC(vtss_port_media2conf(TRUE, FALSE, &port_conf, ife.isid,
                                         ife.ordinal));
            break;
        case VTSS_APPL_PORT_MEDIA_SFP:
            VTSS_RC(vtss_port_media2conf(FALSE, TRUE, &port_conf, ife.isid,
                                         ife.ordinal));
            break;
        case VTSS_APPL_PORT_MEDIA_DUAL:
            VTSS_RC(vtss_port_media2conf(TRUE, TRUE, &port_conf, ife.isid,
                                         ife.ordinal));
            mib_speed = VTSS_APPL_PORT_SPEED_AUTO;
            break;
        }
    }

    if (( port_mib_conf->advertise_dis & (VTSS_APPL_PORT_ADV_DIS_ALL)) !=
          port_mib_conf->advertise_dis ) {
        T_IG_PORT(TRACE_GRP_MIB, ife.ordinal, "mib_advertise:%d", port_mib_conf->advertise_dis);
        return VTSS_APPL_PORT_ERROR_ILLEGAL_ADVERTISE;
    }

    if ( ( 0 != port_mib_conf->advertise_dis ) &&
          VTSS_APPL_PORT_SPEED_AUTO != mib_speed ) {
        T_IG_PORT(TRACE_GRP_MIB, ife.ordinal, "mib_advertise:%d, speed:%d", port_mib_conf->advertise_dis, mib_speed);
        return VTSS_APPL_PORT_ERROR_ILLEGAL_ADVERTISE;
    } else {
        port_conf.adv_dis = port_mib_conf->advertise_dis;
    }

    // Wrapper for speed
    if (current_port_mib_conf.speed !=
        port_mib_conf->speed) {  // Because media type and speed depend upon
                                 // each other we only do this if this parameter
                                 // has changed
        T_IG_PORT(TRACE_GRP_MIB, ife.ordinal, "mib_speed:%d", mib_speed);

        if (port_conf.dual_media_fiber_speed == MESA_SPEED_FIBER_AUTO &&
            mib_speed != VTSS_APPL_PORT_SPEED_AUTO) {
            VTSS_RC(vtss_port_media2conf(FALSE, TRUE, &port_conf, ife.isid,
                                         ife.ordinal));
        }

        switch (mib_speed) {
        case VTSS_APPL_PORT_SPEED_AUTO:
            port_conf.speed = MESA_SPEED_AUTO;
            port_conf.fdx = TRUE;
            break;
        case VTSS_APPL_PORT_SPEED_1G_FDX:
            port_conf.speed = MESA_SPEED_1G;
            port_conf.fdx = TRUE;
            break;

        case VTSS_APPL_PORT_SPEED_10M_FDX:
            port_conf.speed = MESA_SPEED_10M;
            port_conf.fdx = TRUE;
            break;

        case VTSS_APPL_PORT_SPEED_10M_HDX:
            port_conf.speed = MESA_SPEED_10M;
            port_conf.fdx = FALSE;
            break;

        case VTSS_APPL_PORT_SPEED_100M_FDX:
            port_conf.speed = MESA_SPEED_100M;
            port_conf.fdx = TRUE;
            break;

        case VTSS_APPL_PORT_SPEED_100M_HDX:
            port_conf.speed = MESA_SPEED_100M;
            port_conf.fdx = FALSE;
            break;

        case VTSS_APPL_PORT_SPEED_2500M_FDX:
            if (!is_port_capability_stack_supported(MEBA_PORT_CAP_2_5G_FDX)) {
                T_W("VTSS_APPL_PORT_ERROR_ILLEGAL_SPEED");
                return VTSS_APPL_PORT_ERROR_ILLEGAL_SPEED;
            }
            port_conf.speed = MESA_SPEED_2500M;
            port_conf.fdx = TRUE;
            break;

        case VTSS_APPL_PORT_SPEED_5G_FDX:
            if (!is_port_capability_stack_supported(MEBA_PORT_CAP_5G_FDX)) {
                T_W("VTSS_APPL_PORT_ERROR_ILLEGAL_SPEED");
                return VTSS_APPL_PORT_ERROR_ILLEGAL_SPEED;
            }
            port_conf.speed = MESA_SPEED_10G;
            port_conf.fdx = TRUE;
            break;

        case VTSS_APPL_PORT_SPEED_10G_FDX:
            if (!is_port_capability_stack_supported(MEBA_PORT_CAP_10G_FDX)) {
                T_W("VTSS_APPL_PORT_ERROR_ILLEGAL_SPEED");
                return VTSS_APPL_PORT_ERROR_ILLEGAL_SPEED;
            }
            port_conf.speed = MESA_SPEED_10G;
            port_conf.fdx = TRUE;
            break;

        case VTSS_APPL_PORT_SPEED_12G_FDX:
            if (!is_port_capability_stack_supported(MEBA_PORT_CAP_10G_FDX)) { // Note: 12G is running at the 10G interface.
                T_W("VTSS_APPL_PORT_ERROR_ILLEGAL_SPEED");
                return VTSS_APPL_PORT_ERROR_ILLEGAL_SPEED;
            }
            port_conf.speed = MESA_SPEED_1G;
            port_conf.fdx = TRUE;
            break;
        }
    }

    // Wrapper for Flow control
    port_conf.flow_control =
            port_mib_conf->fc == VTSS_APPL_PORT_FC_ON ? TRUE : FALSE;

    // Wrapper for Priority Flow control
    u8 q;
    for (q = 0; q < VTSS_PRIOS; q++) {
      port_conf.pfc[q] = (port_mib_conf->pfc_mask & (1 << q)) > 0 ? TRUE : FALSE;
    }
    // Wrapper for Enable
    port_conf.admin.enable = !port_mib_conf->shutdown;

    // Wrapper for MTU
    port_conf.max_length = port_mib_conf->mtu;

    // Wrapper for frame length check
    port_conf.frame_length_chk = port_mib_conf->frame_length_chk;

    // Wrapper for Excessive
    port_conf.exc_col_cont = port_mib_conf->excessive;

    T_IG_PORT(TRACE_GRP_MIB, ife.ordinal,
              "speed:%d, port_mib_conf->speed:%d", port_conf.speed, port_mib_conf->speed);

    // Set the new configuration;
    return vtss_appl_port_conf_set(ifindex, &port_conf);
}

// Port status table
mesa_rc vtss_port_mib_status_get(vtss_ifindex_t ifindex,
                                 vtss_appl_port_mib_status_t *port_mib_status) {
    T_NG(TRACE_GRP_MIB, "Getting port mgmt status");
    return port_status_update.get(ifindex, port_mib_status);
}



// Getting the RFC2861 counter from the port counters
mesa_rc port_if_group_statistics_get(
        vtss_ifindex_t ifindex,
        mesa_port_if_group_counters_t *port_if_group_counters) {
    mesa_port_counters_t counters;

    VTSS_RC(vtss_appl_port_counters_get(ifindex, &counters));

    *port_if_group_counters = counters.if_group;
    return VTSS_RC_OK;
}

/**
 * \brief Getting bridge Interface counters */
mesa_rc port_bridge_statistics_get(
        vtss_ifindex_t ifindex, mesa_port_bridge_counters_t *bridge_counters) {
    mesa_port_counters_t counters;

    VTSS_RC(vtss_appl_port_counters_get(ifindex, &counters));

    *bridge_counters = counters.bridge;
    return VTSS_RC_OK;
}

/**
 * \brief Getting Ethernet-like Interface counters */
mesa_rc port_ethernet_like_statistics_get(
        vtss_ifindex_t ifindex,
        mesa_port_ethernet_like_counters_t *eth_counters) {
    mesa_port_counters_t counters;

    VTSS_RC(vtss_appl_port_counters_get(ifindex, &counters));

    *eth_counters = counters.ethernet_like;
    return VTSS_RC_OK;
}

/**
 * \brief Getting queues counters */
mesa_rc port_queues_statistics_get(
        vtss_ifindex_t ifindex, mesa_prio_t prio,
        vtss_appl_port_prio_counter_t *prop_counters) {
    mesa_port_counters_t counters;

    VTSS_RC(vtss_appl_port_counters_get(ifindex, &counters));

    prop_counters->rx_prio = counters.prio[prio].rx;
    prop_counters->tx_prio = counters.prio[prio].tx;
    return VTSS_RC_OK;
}

// Getting the RMON part of the counter from the port counters
mesa_rc port_rmon_statistics_get(vtss_ifindex_t ifindex,
                                 mesa_port_rmon_counters_t *rmon_statistics) {
    mesa_port_counters_t counters;

    VTSS_RC(vtss_appl_port_counters_get(ifindex, &counters));

    *rmon_statistics = counters.rmon;
    return VTSS_RC_OK;
}

// Getting the 802.3br part of the counter from the port counters
mesa_rc port_dot3br_statistics_get(vtss_ifindex_t ifindex,
                                   mesa_port_dot3br_counters_t *dot3br_statistics) {
#if defined(VTSS_SW_OPTION_QOS_ADV)
    mesa_port_counters_t counters;

    if (!MESA_CAP(MESA_CAP_QOS_FRAME_PREEMPTION)) {
        return VTSS_APPL_PORT_ERROR_GEN; // Feature not suported
    }
    VTSS_RC(vtss_appl_port_counters_get(ifindex, &counters));

    *dot3br_statistics = counters.dot3br;
    return VTSS_RC_OK;
#else
    return VTSS_APPL_PORT_ERROR_GEN; // Feature not suported
#endif
}
