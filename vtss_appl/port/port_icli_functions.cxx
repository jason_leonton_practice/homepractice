/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#include "icli_api.h"
#include "icli_porting_util.h"
#include "misc_api.h" // For e.g. vtss_uport_no_t
#include "msg_api.h"
#include "mgmt_api.h" // For mgmt_prio2txt
#ifdef VTSS_SW_OPTION_ICFG
#include "icfg_api.h"
#endif
#ifdef VTSS_SW_OPTION_QOS
#include "qos_api.h"  // For VTSS_SW_OPTION_QOS_ADV
#endif
#include "port_api.h" // For vtss_port_conf_default
#include "port.h"
#include "port_trace.h"
#include "sysutil_api.h"
#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************************/
/*  Internal types                                                         */
/****************************************************************************/
// Enum for selecting which port configuration to perform.
typedef enum {
  MTU,               // Configuration of max frame length
  SHUT_DOWN,         // shut down of port
  FORCED_SPEED,      // Configuration of forced speed
  AUTONEG,           // Configuration of auto negotiation
  DUPLEX,            // Configuration of duplex mode
  MEDIA,             // Configuration of media type (cu/sfp)
  FLOW_CONTROL,      // Configuration of flow control
  EXCESSIVE_RESTART, // Configuration of excessive collisions.
  FRM_LEN_CHK,       // Configuration of frame length check.
  PFC                // Configuration of priority flowcontrol
} port_conf_type_t;


// Enum for selecting which port status to display.
typedef enum {
  STATISTICS,        // Displaying statistics
  CAPABILITIES,      // Displaying capabilities
  CHANGE_EVENT,      // Displaying port count change
  STATUS,            // Displaying status.
} port_status_type_t;

// Used to passed the port configuration to be changed.
typedef union {
  // Statistics options
  struct {
    BOOL has_clear; // Clear statistics
    BOOL has_packets;
    BOOL has_bytes;
    BOOL has_errors;
    BOOL has_discards;
    BOOL has_filtered;
    BOOL has_dot3br;
    BOOL has_priority;
    BOOL has_up;
    BOOL has_down;
    icli_unsigned_range_t *priority_list;
  } statistics;

  // Used to passed the port capabilities to be shown.
  struct {
    BOOL sfp_detect; // Show SFP information read from detected SFP (via i2c)
  } port_capa_show;

  // Used to passed the port capabilities to be shown.
  struct {
    BOOL has_down; // Show only for ports that are down
    BOOL has_up;   // Show only for ports that are up
    BOOL has_clear;// TRUE to clear change event counters
  } change_event;

  // Plain status
  struct {
    BOOL has_errdisable; // Show only errdisabled ports
  } plain;
} port_status_value_t;

// Used to passed the port status to be displayed.
typedef union {
  u16 max_length;
  mesa_port_speed_t speed;

  // Media type
  struct {
    BOOL rj45;
    BOOL sfp;
  } media_type;

  // Auto negotiation
  struct {
    BOOL autoneg;
    BOOL has_neg_10;
    BOOL has_neg_100;
    BOOL has_neg_1000;
    BOOL has_neg_2500;
    BOOL has_neg_5g;
    BOOL has_neg_10g;
  } autoneg_conf;

  // Duplex
  struct {
    BOOL has_full;
    BOOL has_half;
    BOOL has_auto;
    BOOL has_advertise_fdx;
    BOOL has_advertise_hdx;
  } duplex;

  struct {
    BOOL fc_enable;
  } flow_control;

  struct {
    BOOL pfc[VTSS_PRIOS];
  } priority_fc;

  struct {
    BOOL frm_len_chk;
  } frm;
} port_conf_value_t;

/***************************************************************************/
/*  Internal functions                                                     */
/****************************************************************************/
#if defined(VTSS_SW_OPTION_QOS_ADV)
static void port_icli_stat_dot3br_print(i32 session_id, port_status_value_t *status_value, const switch_iter_t *sit, const port_iter_t *pit, const mesa_port_counters_t *counters, BOOL header)
{
  ICLI_PRINTF("\n");
  if (header) {
    icli_print_port_info_txt(session_id, sit->usid, pit->uport);
    ICLI_PRINTF("802.3br Statistics:\n");
  }
  icli_cmd_stati(session_id, "MM Fragments", "", counters->dot3br.aMACMergeFragCountRx, counters->dot3br.aMACMergeFragCountTx);
  icli_cmd_stati(session_id, "MM Assembly Ok", "MM Hold", counters->dot3br.aMACMergeFrameAssOkCount, counters->dot3br.aMACMergeHoldCount);
  icli_cmd_stati(session_id, "MM Assembly Errors", NULL, counters->dot3br.aMACMergeFrameAssErrorCount, 0);
  icli_cmd_stati(session_id, "MM SMD Errors", NULL, counters->dot3br.aMACMergeFrameSmdErrorCount, 0);
}
#endif /* VTSS_SW_OPTION_QOS_ADV */

#ifdef VTSS_SW_OPTION_QOS
/* Print counters for priority queue */
static void port_icli_stat_priority(i32 session_id, vtss_usid_t usid, vtss_uport_no_t uport, BOOL first, const char *name,
                                    u64 c1, u64 c2)
{
  char buf[200];

  if (first) {
    sprintf(&buf[0], "%-43s Rx %-17s Tx %-17s", icli_port_info_txt(usid, uport, buf), name, name);
    icli_table_header(session_id, buf);
  } else {
    ICLI_PRINTF("%-43s " VPRI64Fu("-20") " " VPRI64Fu("-20") "\n", name, c1, c2);
  }
}

// Loop though priority queue and print statistics
static void port_icli_stat_priority_print(i32 session_id, port_status_value_t *status_value, const switch_iter_t *sit, const port_iter_t *pit, const mesa_port_counters_t *counters, BOOL print_header)
{
  /* Handle '<class>' specified */
  u8 i, idx;
  char prio_str_buf[40];

  if (print_header) {
    port_icli_stat_priority(session_id, sit->usid, pit->uport, TRUE, "Priority queue", 0, 0);
  }

  if (status_value->statistics.priority_list != NULL) {
    // User want some specific priorities
    for (i = 0; i < status_value->statistics.priority_list->cnt; i++) {
      for (idx = status_value->statistics.priority_list->range[i].min; idx <= status_value->statistics.priority_list->range[i].max; idx++) {
        strcpy(prio_str_buf, "Priority ");
        strncat(prio_str_buf, mgmt_prio2txt(idx, FALSE), 20);
        port_icli_stat_priority(session_id, sit->usid, pit->uport, FALSE, prio_str_buf,
                                counters->prio[idx].rx, counters->prio[idx].tx);
      }
    }
  } else {
    // User want all priorities
    for (idx = VTSS_PRIO_START; idx < VTSS_PRIO_END; idx++) {
      strcpy(prio_str_buf, "Priority ");
      strcat(prio_str_buf, mgmt_prio2txt(idx, FALSE));
      port_icli_stat_priority(session_id, sit->usid, pit->uport, FALSE, prio_str_buf,
                              counters->prio[idx].rx, counters->prio[idx].tx);
    }
  }
}
#endif

static char *prio_range_to_str(BOOL *pfc, char *buf)
{
  u8 i, a = 0, first = 1, b, tot = 0, once = 1;
  buf[0] = 0;
  for (i = 0; i < 8; i++) {
    if (!pfc[i]) {
      continue;
    }
    if (first) {
      tot = a = + sprintf(buf + a, "%d", i);
      first = 0;
    } else {
      if (pfc[i]) {
        if (pfc[i - 1]) {
          b = sprintf(buf + a, "-%d", i);
          if (once) {
            tot = tot + b;
            once = 0;
          }
        } else {
          a = tot = tot + sprintf(buf + tot, ",%d", i);
          once = 1;
        }
      }
    }
  }
  return buf;
}

/* Print counters in two columns with header */
// IN : session_id - For ICLI printing
//      usid       - User switch id
//      uport      - User port id
//      first      - Set to TRUE to print header
//      name       - String with the counter name
//      tx         - Set to TRUE is it is a Tx counter, set to FALSE for both rx & tx counters
//      link       - Set to TRUE if the port has link up
//      has_down   - Set to TRUE to only print statistics for ports with link down
//      has_up     - Set to TRUE to only print statistics for ports with link up
//      c1         - RX counter value
//      c2         - TX counter value
static void cli_cmd_stat_port(i32 session_id, vtss_usid_t usid, vtss_uport_no_t uport, BOOL first, const char *name, BOOL tx, BOOL link, BOOL has_down, BOOL has_up,
                              u64 c1, u64 c2)
{
  char buf[ICLI_PORTING_STR_BUF_SIZE], *p;

  if (first) {
    p = &buf[0];
    p += sprintf(p, "%-23s Rx %-17s", "Interface", name);
    if (tx) {
      sprintf(p, "Tx %-17s", name);
    }

    icli_table_header(session_id, buf);
  }

  if ((has_down && link) ||
      (has_up && !link)) {
    T_IG(TRACE_GRP_ICLI, "has_up:%d, has_down:%d, link:%d", has_up, has_down, link);
    return;
  }

  ICLI_PRINTF("%-23s " VPRI64Fu("-20"), icli_port_info_txt(usid, uport, buf), c1); // 20 is 17 + the size of "Rx "
  if (tx) {
    ICLI_PRINTF(VPRI64Fu("-20"), c2);
  }
  ICLI_PRINTF("\n");
}

// Help function for printing statistis
// IN - session_id - For iCLI printing
//      sit        - Switch information
//      pit        - Port information.
//      status_value - Information about what to print.
static mesa_rc icli_print_statistics(i32 session_id, const switch_iter_t *sit, const port_iter_t *pit, port_status_value_t *status_value)
{
  vtss_appl_port_status_t        port_status;
  mesa_port_status_t   *status = &port_status.status;
  mesa_port_counters_t counters;

  vtss_ifindex_t ifindex;
  VTSS_RC(vtss_ifindex_from_port(sit->isid, pit->iport, &ifindex));

  VTSS_RC(vtss_appl_port_status_get(ifindex, &port_status)); // Get status

  T_IG_PORT(TRACE_GRP_ICLI, pit->iport, "has_clear:%d", status_value->statistics.has_clear);
  /* Handle 'clear' command */
  if (status_value->statistics.has_clear) {
    VTSS_RC(vtss_appl_port_counters_clear(ifindex));
    return VTSS_RC_OK;
  }

  /* Get counters for remaining commands */
  if (vtss_appl_port_counters_get(ifindex, &counters) != VTSS_OK) {
    return VTSS_RC_OK;
  }

  /* Handle 'packet' command */
  if (status_value->statistics.has_packets) {
    cli_cmd_stat_port(session_id, sit->usid, pit->uport, pit->first, "Packets", 1, status->link, status_value->statistics.has_down, status_value->statistics.has_up, counters.rmon.rx_etherStatsPkts, counters.rmon.tx_etherStatsPkts);
    return VTSS_RC_OK;
  }

  /* Handle 'bytes' command */
  if (status_value->statistics.has_bytes) {
    cli_cmd_stat_port(session_id, sit->usid, pit->uport, pit->first, "Octets", 1, status->link, status_value->statistics.has_down, status_value->statistics.has_up, counters.rmon.rx_etherStatsOctets, counters.rmon.tx_etherStatsOctets);
    return VTSS_RC_OK;
  }

  /* Handle 'errors' command */
  if (status_value->statistics.has_errors) {
    cli_cmd_stat_port(session_id, sit->usid, pit->uport, pit->first, "Errors", 1, status->link, status_value->statistics.has_down, status_value->statistics.has_up, counters.if_group.ifInErrors, counters.if_group.ifOutErrors);
    return VTSS_RC_OK;
  }

  /* Handle 'discards' command */
  if (status_value->statistics.has_discards) {
    cli_cmd_stat_port(session_id, sit->usid, pit->uport, pit->first, "Discards", 1, status->link, status_value->statistics.has_down, status_value->statistics.has_up, counters.if_group.ifInDiscards, counters.if_group.ifOutDiscards);
    return VTSS_RC_OK;
  }

  /* Handle 'filtered' command */
  if (status_value->statistics.has_filtered) {
    cli_cmd_stat_port(session_id, sit->usid, pit->uport, pit->first, "Filtered", 0, status->link, status_value->statistics.has_down, status_value->statistics.has_up, counters.bridge.dot1dTpPortInDiscards, 0);
    return VTSS_RC_OK;
  }

  // Skipping if has_ up/down
  if ((status_value->statistics.has_down && status->link) ||
      (status_value->statistics.has_up && !status->link)) {
    return VTSS_RC_OK;
  }

#if defined(VTSS_SW_OPTION_QOS_ADV)
  if (status_value->statistics.has_dot3br && MESA_CAP(MESA_CAP_QOS_FRAME_PREEMPTION)) {
    port_icli_stat_dot3br_print(session_id, status_value, sit, pit, &counters, TRUE);
    return VTSS_RC_OK;
  }
#endif /* VTSS_SW_OPTION_QOS_ADV */

#ifdef VTSS_SW_OPTION_QOS
  if (status_value->statistics.has_priority) {
    port_icli_stat_priority_print(session_id, status_value, sit, pit, &counters, TRUE);
    ICLI_PRINTF("\n");
    return VTSS_RC_OK;
  }
#endif /* VTSS_SW_OPTION_QOS */

  // Print out interface
  ICLI_PRINTF("\n");
  icli_print_port_info_txt(session_id, sit->usid, pit->uport);
  ICLI_PRINTF("Statistics:\n");

  /* Handle default command */
  icli_cmd_stati(session_id, "Packets", "",
                 counters.rmon.rx_etherStatsPkts,
                 counters.rmon.tx_etherStatsPkts);
  icli_cmd_stati(session_id, "Octets", "",
                 counters.rmon.rx_etherStatsOctets,
                 counters.rmon.tx_etherStatsOctets);
  icli_cmd_stati(session_id, "Unicast", "",
                 counters.if_group.ifInUcastPkts,
                 counters.if_group.ifOutUcastPkts);
  icli_cmd_stati(session_id, "Multicast", "",
                 counters.rmon.rx_etherStatsMulticastPkts,
                 counters.rmon.tx_etherStatsMulticastPkts);
  icli_cmd_stati(session_id, "Broadcast", "",
                 counters.rmon.rx_etherStatsBroadcastPkts,
                 counters.rmon.tx_etherStatsBroadcastPkts);
  icli_cmd_stati(session_id, "Pause", "",
                 counters.ethernet_like.dot3InPauseFrames,
                 counters.ethernet_like.dot3OutPauseFrames);
  ICLI_PRINTF("\n");
  icli_cmd_stati(session_id, "64", "",
                 counters.rmon.rx_etherStatsPkts64Octets,
                 counters.rmon.tx_etherStatsPkts64Octets);
  icli_cmd_stati(session_id, "65-127", "",
                 counters.rmon.rx_etherStatsPkts65to127Octets,
                 counters.rmon.tx_etherStatsPkts65to127Octets);
  icli_cmd_stati(session_id, "128-255", "",
                 counters.rmon.rx_etherStatsPkts128to255Octets,
                 counters.rmon.tx_etherStatsPkts128to255Octets);
  icli_cmd_stati(session_id, "256-511", "",
                 counters.rmon.rx_etherStatsPkts256to511Octets,
                 counters.rmon.tx_etherStatsPkts256to511Octets);
  icli_cmd_stati(session_id, "512-1023", "",
                 counters.rmon.rx_etherStatsPkts512to1023Octets,
                 counters.rmon.tx_etherStatsPkts512to1023Octets);
  icli_cmd_stati(session_id, "1024-1526", "",
                 counters.rmon.rx_etherStatsPkts1024to1518Octets,
                 counters.rmon.tx_etherStatsPkts1024to1518Octets);
  icli_cmd_stati(session_id, "1527-    ", "",
                 counters.rmon.rx_etherStatsPkts1519toMaxOctets,
                 counters.rmon.tx_etherStatsPkts1519toMaxOctets);
  ICLI_PRINTF("\n");

#ifdef VTSS_SW_OPTION_QOS
  char prio_str_buf[50];
  u8 idx;
  for (idx = VTSS_PRIO_START; idx < VTSS_PRIO_END; idx++) {
    strcpy(prio_str_buf, "Priority ");
    strncat(prio_str_buf, mgmt_prio2txt(idx, FALSE), 30);
    icli_cmd_stati(session_id, prio_str_buf, "",
                   counters.prio[idx].rx, counters.prio[idx].tx);
  }
  ICLI_PRINTF("\n");
#endif /* VTSS_SW_OPTION_QOS */


  icli_cmd_stati(session_id, "Drops", "",
                 counters.rmon.rx_etherStatsDropEvents,
                 counters.rmon.tx_etherStatsDropEvents);
  icli_cmd_stati(session_id, "CRC/Alignment", "Late/Exc. Coll.",
                 counters.rmon.rx_etherStatsCRCAlignErrors,
                 counters.if_group.ifOutErrors);
  icli_cmd_stati(session_id, "Undersize", NULL, counters.rmon.rx_etherStatsUndersizePkts, 0);
  icli_cmd_stati(session_id, "Oversize", NULL, counters.rmon.rx_etherStatsOversizePkts, 0);
  icli_cmd_stati(session_id, "Fragments", NULL, counters.rmon.rx_etherStatsFragments, 0);
  icli_cmd_stati(session_id, "Jabbers", NULL, counters.rmon.rx_etherStatsJabbers, 0);
  /* Bridge counters */
  icli_cmd_stati(session_id, "Filtered", NULL, counters.bridge.dot1dTpPortInDiscards, 0);

#if defined(VTSS_SW_OPTION_QOS_ADV)
  if (MESA_CAP(MESA_CAP_QOS_FRAME_PREEMPTION)) {
    port_icli_stat_dot3br_print(session_id, status_value, sit, pit, &counters, FALSE);
  }
#endif /* VTSS_SW_OPTION_QOS_ADV */

  return VTSS_RC_OK;
}

// Help function for printing port status.
// IN - session_id - For iCLI printing
//      sit        - Switch information
//      pit        - Port information-
static mesa_rc print_port_status(i32 session_id, const switch_iter_t *sit, const port_iter_t *pit, BOOL has_errdisable)
{
  char buf[150];
  char *txt = buf;
  vtss_appl_port_status_t port_status;
  vtss_appl_port_conf_t   port_conf;
  u8 i, pfc = 0;

  vtss_ifindex_t ifindex;
  VTSS_RC(vtss_ifindex_from_port(sit->isid, pit->iport, &ifindex));
  VTSS_RC(vtss_appl_port_conf_get(ifindex, &port_conf));
  VTSS_RC(vtss_appl_port_status_get(ifindex, &port_status)); // Get status
  port_isid_info_t       info;
  VTSS_RC(port_isid_info_get(sit->isid, &info));

  port_vol_status_t  vol_status;
  BOOL vol_link_down = FALSE;

  if (sit->isid != VTSS_ISID_LOCAL &&
      port_vol_status_get(PORT_USER_CNT, sit->isid, pit->iport, &vol_status) == VTSS_OK &&
      (vol_status.conf.disable || vol_status.conf.disable_adm_recover) && vol_status.user != PORT_USER_STATIC) {
    vol_link_down = TRUE;
  }

  T_NG(TRACE_GRP_ICLI, "info:" VPRI64x, info.cap);

  // Header
  if (pit->first) {
    txt +=  sprintf(txt, "%-23s %-8s %-15s ", "Interface", "Mode", "Speed & Duplex");
    if (info.cap & MEBA_PORT_CAP_FLOW_CTRL) {
      txt += sprintf(txt, "%-13s", "Flow Control");
    }
    txt += sprintf(txt, " %-10s %-10s %-10s", "Max Frame", "Excessive", "Link");
    icli_table_header(session_id, buf);
  }








  // Print Interface and mode
  ICLI_PRINTF("%-23s %-8s ", icli_port_info_txt(sit->usid, pit->uport, buf), icli_bool_txt(port_conf.admin.enable));

  // Print speed and duplex
  BOOL no_fiber = (port_conf.dual_media_fiber_speed == MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED);
  T_NG(TRACE_GRP_ICLI, "no_fiber:%d, spd:%d", no_fiber, port_conf.speed);
  if (no_fiber) {
    ICLI_PRINTF("%-15s ",
                port_conf.speed == MESA_SPEED_AUTO ? "Auto" : vtss_port_mgmt_mode_txt(pit->iport, port_conf.speed, port_conf.fdx, FALSE));
  } else {
    ICLI_PRINTF("%-15s ",
                port_fiber_mgmt_mode_txt(pit->iport, port_conf.dual_media_fiber_speed, port_conf.speed == MESA_SPEED_AUTO));

  }

  // Print flow control
  if (info.cap & MEBA_PORT_CAP_FLOW_CTRL) {
    for (i = 0; i < VTSS_PRIOS; i++) {
      if (port_conf.pfc[i]) {
        pfc = 1;
        (void)prio_range_to_str(port_conf.pfc, buf);
        sprintf(buf, "%s (PFC)", buf);
      }
    }
    ICLI_PRINTF("%-13s", pfc ? buf : icli_bool_txt(port_conf.flow_control));
  }

  // Print Max frame size
  ICLI_PRINTF(" %-10u ", port_conf.max_length);

  // Print Excessive
  ICLI_PRINTF("%-10s ", port_conf.exc_col_cont ? "Restart" : "Discard");


  // Print Link
  if (port_status.status.link) {
    sprintf(buf, "%s",
            vtss_port_mgmt_mode_txt(pit->iport, port_status.status.speed, port_status.status.fdx, port_status.status.fiber));
  } else if (vol_link_down) {
    sprintf(buf, "Down (%s)", vol_status.name);
  } else {
    strcpy(buf, "Down");
  }
  ICLI_PRINTF("%-10s ", buf);

  ICLI_PRINTF("\n");
  return VTSS_RC_OK;
}

#define CAPABILITY_YES_STR "yes"
#define CAPABILITY_NO_STR "no"

#define HEAD_PRINT(head) do {char tmp[64]; sprintf(tmp, "%s:", head);ICLI_PRINTF("  %-22s ", tmp);}while(0)
#define HEAD_VAL_PRINT(head, val, ...) do {HEAD_PRINT(head); ICLI_PRINTF(val, __VA_ARGS__);ICLI_PRINTF("\n");}while(0)

static i8 *icli_cmd_port_speed_txt(meba_port_cap_t cap, i8 *str)
{
  i8 *ptr = str;

  strcpy(ptr, ""); // Making sure that the string is empty

  //if force mode is unsupported, don't show the force speed
  if ((cap &  (MEBA_PORT_CAP_10M_HDX | MEBA_PORT_CAP_10M_FDX)) &&
      (!(cap & MEBA_PORT_CAP_NO_FORCE))) {
    sprintf(ptr, "10");
    ptr += strlen(ptr);
  }
  if ((cap & (MEBA_PORT_CAP_100M_HDX | MEBA_PORT_CAP_100M_FDX)) &&
      (!(cap & MEBA_PORT_CAP_NO_FORCE))) {
    if (ptr != &str[0]) {
      *ptr++ = ',';
    }
    sprintf(ptr, "100");
    ptr += strlen(ptr);
  }
  if ((cap & (MEBA_PORT_CAP_1G_FDX)) &&
      (!(cap & MEBA_PORT_CAP_NO_FORCE))) {
    if (ptr != &str[0]) {
      *ptr++ = ',';
    }
    sprintf(ptr, "1000");
    ptr += strlen(ptr);
  }

  if ((cap & (MEBA_PORT_CAP_2_5G_FDX)) &&
      (!(cap & MEBA_PORT_CAP_NO_FORCE))) {
    if (ptr != &str[0]) {
      *ptr++ = ',';
    }
    sprintf(ptr, "2.5G");
    ptr += strlen(ptr);
  }

  if ((cap & (MEBA_PORT_CAP_5G_FDX)) &&
      (!(cap & MEBA_PORT_CAP_NO_FORCE))) {
    if (ptr != &str[0]) {
      *ptr++ = ',';
    }
    sprintf(ptr, "5G");
    ptr += strlen(ptr);
  }

  if ((cap & (MEBA_PORT_CAP_10G_FDX)) &&
      (!(cap & MEBA_PORT_CAP_NO_FORCE))) {
    if (ptr != &str[0]) {
      *ptr++ = ',';
    }
    sprintf(ptr, "10G");
    ptr += strlen(ptr);
  }

  if (cap & MEBA_PORT_CAP_AUTONEG) {
    if (ptr != &str[0]) {
      *ptr++ = ',';
    }
    sprintf(ptr, "auto");
    ptr += strlen(ptr);
  }

  return str;
}

static i8 *icli_cmd_port_duplex_txt(meba_port_cap_t cap, i8 *str)
{
  i8 *ptr = str;

  if (cap &  MEBA_PORT_CAP_HDX) {
    sprintf(ptr, "half");
    ptr += strlen(ptr);
  }
  if (cap & MEBA_PORT_CAP_TRI_SPEED_FDX) {
    if (ptr != &str[0]) {
      *ptr++ = ',';
    }
    sprintf(ptr, "full");
    ptr += strlen(ptr);
  }
  if (cap & MEBA_PORT_CAP_AUTONEG) {
    if (ptr != &str[0]) {
      *ptr++ = ',';
    }
    sprintf(ptr, "auto");
    ptr += strlen(ptr);
  }

  return str;
}

static void icli_print_sfp_info(const i32 session_id, const vtss_appl_port_status_t &port_status)
{
  HEAD_VAL_PRINT("SFP Vendor P/N", "%s", port_status.sfp.vendor_pn[0] == 0 ? "None" : port_status.sfp.vendor_pn);
  HEAD_VAL_PRINT("SFP Vendor S/N", "%s", port_status.sfp.vendor_sn[0] == 0 ? "None" : port_status.sfp.vendor_sn);
  HEAD_VAL_PRINT("SFP Vendor Name", "%s", port_status.sfp.vendor_name[0] == 0 ? "None" : port_status.sfp.vendor_name);
  HEAD_VAL_PRINT("SFP Vendor Revision", "%s", port_status.sfp.vendor_rev[0] == 0 ? "None" : port_status.sfp.vendor_rev);
  HEAD_VAL_PRINT("SFP Type", "%s", sfp_if2txt(port_status.sfp.type));
}

/* Port status */
static mesa_rc icli_cmd_port_status(i32 session_id, icli_stack_port_range_t *plist, port_status_type_t status_type, port_status_value_t *status_value)
{
  i8 str_txt[65];
  switch_iter_t           sit;
  vtss_appl_port_status_t port_status;

  // Loop through all switches in stack
  VTSS_RC(switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID));
  while (icli_switch_iter_getnext(&sit, plist)) {
    // Loop through all ports
    port_iter_t pit;
    VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_ALL));
    T_IG(TRACE_GRP_ICLI, "status_type:%d", status_type);
    while (icli_port_iter_getnext(&pit, plist)) {
      vtss_ifindex_t ifindex;
      VTSS_RC(vtss_ifindex_from_port(sit.isid, pit.iport, &ifindex));

      switch (status_type) {
      case STATISTICS:
        VTSS_RC(icli_print_statistics(session_id, &sit, &pit, status_value));
        break;

      case CHANGE_EVENT:
        VTSS_RC(vtss_appl_port_status_get(ifindex, &port_status)); // Get status

        if ((status_value->change_event.has_clear)) {
          VTSS_RC(vtss_appl_port_counters_clear(ifindex));
          continue;
        }
        T_IG_PORT(TRACE_GRP_ICLI, pit.iport, "has_up:%d, has_down:%d, link:%d", status_value->change_event.has_up, status_value->change_event.has_down, port_status.status.link);
        if ((status_value->change_event.has_down && port_status.status.link) ||
            (status_value->change_event.has_up && !port_status.status.link)) {
          continue;
        }

        char str_buf[100];
        if (pit.first) {
          sprintf(str_buf, "%-23s %-8s %-8s ", "Interface", "Link up", "Link down");
          icli_table_header(session_id, str_buf);
        }

        ICLI_PRINTF("%-23s %-8u %-8u\n", icli_port_info_txt(sit.usid, pit.uport, str_buf),
                    port_status.port_up_count, port_status.port_down_count);
        break;

      case CAPABILITIES:
        VTSS_RC(vtss_appl_port_status_get(ifindex, &port_status)); // Get status

        ICLI_PRINTF("\n");
        icli_print_port_info_txt(session_id, sit.usid, pit.uport);
        ICLI_PRINTF("Capabilities:\n");

        {
          if (port_status.cap & (MEBA_PORT_CAP_SFP_DETECT | MEBA_PORT_CAP_DUAL_SFP_DETECT)) {
            icli_print_sfp_info(session_id, port_status);
          }
          if (port_status.cap & (MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER)) {
            HEAD_VAL_PRINT("Dual Port", "%s", "yes");
          }
          switch (port_status.mac_if) {
          case MESA_PORT_INTERFACE_SGMII_CISCO:
            HEAD_VAL_PRINT("Speed cap", "%s", "auto");
            HEAD_VAL_PRINT("Duplex cap", "%s", "auto");
            break;
          case MESA_PORT_INTERFACE_100FX:
            HEAD_VAL_PRINT("Speed cap", "%s", "100");
            HEAD_VAL_PRINT("Duplex cap", "%s", "full");
            break;
          default:
            HEAD_VAL_PRINT("Speed cap", "%s", icli_cmd_port_speed_txt(port_status.cap, &str_txt[0]));
            HEAD_VAL_PRINT("Duplex cap", "%s", icli_cmd_port_duplex_txt(port_status.cap, &str_txt[0]));
          }
          HEAD_PRINT("Trunk encap. type");
#ifdef VTSS_SW_OPTION_VLAN
          ICLI_PRINTF("802.1Q\n");
#else
          ICLI_PRINTF("%s\n", CAPABILITY_NO_STR );
#endif

#ifdef VTSS_SW_OPTION_VLAN
          HEAD_VAL_PRINT("Trunk mode", "%s", "access,hybrid,trunk");
#else
          HEAD_VAL_PRINT("Trunk mode", "%s", "CAPABILITY_NO_STR");
#endif
          HEAD_PRINT("Channel");
#if defined(VTSS_SW_OPTION_IPMC)
          ICLI_PRINTF("%s\n", CAPABILITY_YES_STR );
#else
          ICLI_PRINTF("%s\n", CAPABILITY_NO_STR );
#endif
          HEAD_PRINT("Broadcast suppression");
#if defined(VTSS_QOS_PORT_STORM_POLICER)
          ICLI_PRINTF("100-13200000(kfps or fps)\n", );
#elif defined(VTSS_QOS_GLOBAL_STORM_POLICER)
          ICLI_PRINTF("1,2,4,8,16,32,64,128,256,512 (kfps or fps) or 1024 kfps\n" );
#else
          ICLI_PRINTF("%s\n", CAPABILITY_NO_STR );
#endif /* defined(VTSS_QOS_PORT_STORM_POLICER) */
          HEAD_VAL_PRINT("Flowcontrol",  "%s", (port_status.cap & MEBA_PORT_CAP_FLOW_CTRL) ? CAPABILITY_YES_STR : CAPABILITY_NO_STR);
          HEAD_VAL_PRINT("Fast Start", "%s", CAPABILITY_NO_STR);

#ifdef VTSS_SW_OPTION_QOS
          HEAD_VAL_PRINT("QoS scheduling", "tx-(%dq)", VTSS_PRIO_END - VTSS_PRIO_START);
          HEAD_VAL_PRINT("CoS rewrite", "%s", CAPABILITY_YES_STR);
          HEAD_VAL_PRINT("ToS rewrite", "%s", CAPABILITY_YES_STR);
#else
          HEAD_VAL_PRINT("QoS scheduling", "%s", CAPABILITY_NO_STR);
          HEAD_VAL_PRINT("CoS rewrite", "%s", CAPABILITY_NO_STR);
          HEAD_VAL_PRINT("ToS rewrite", "%s", CAPABILITY_NO_STR);
#endif
          HEAD_VAL_PRINT("UDLD", "%s", CAPABILITY_NO_STR);
          HEAD_PRINT("Inline power");
#ifdef VTSS_SW_OPTION_POE
          ICLI_PRINTF("%s\n", CAPABILITY_YES_STR );
#else
          ICLI_PRINTF("%s\n", CAPABILITY_NO_STR );
#endif
          HEAD_PRINT("RMirror");
#ifdef VTSS_SW_OPTION_RMIRROR
          ICLI_PRINTF("%s\n", CAPABILITY_YES_STR );
#else
          ICLI_PRINTF("%s\n", CAPABILITY_NO_STR );
#endif
          HEAD_PRINT("PortSecure");
#ifdef VTSS_SW_OPTION_MAC
          ICLI_PRINTF("%s\n", CAPABILITY_YES_STR );
#else
          ICLI_PRINTF("%s\n", CAPABILITY_NO_STR );
#endif
          HEAD_PRINT("Dot1x");
#ifdef VTSS_SW_OPTION_DOT1X
          ICLI_PRINTF("%s\n", CAPABILITY_YES_STR );
#else
          ICLI_PRINTF("%s\n", CAPABILITY_NO_STR );
#endif
        }

        break;
      case STATUS:
        VTSS_RC(print_port_status(session_id, &sit, &pit, status_value->plain.has_errdisable));
        break;
      }
    }
  } /* USID loop */
  return VTSS_RC_OK;
}

//  see port_icli_functions.h
mesa_rc port_icli_statistics(i32 session_id, icli_stack_port_range_t *plist, BOOL has_packets, BOOL has_bytes, BOOL has_errors,
                             BOOL has_discards, BOOL has_filtered, BOOL has_dot3br, BOOL has_priority, icli_unsigned_range_t *priority_list,
                             BOOL has_up, BOOL has_down)
{
  port_status_value_t port_status_value;

#if 1 /* CP, 08/27/2013 10:50, Bugzilla#12550 - Unable to view interface statistics in serval */
  memset(&port_status_value, 0, sizeof(port_status_value));
#endif

  port_status_value.statistics.has_packets   = has_packets;
  port_status_value.statistics.has_bytes     = has_bytes;
  port_status_value.statistics.has_errors    = has_errors;
  port_status_value.statistics.has_discards  = has_discards;
  port_status_value.statistics.has_filtered  = has_filtered;
  port_status_value.statistics.has_dot3br    = has_dot3br;
  port_status_value.statistics.has_priority  = has_priority;
  port_status_value.statistics.has_up        = has_up;
  port_status_value.statistics.has_down      = has_down;
  port_status_value.statistics.priority_list = priority_list;
  T_IG(TRACE_GRP_ICLI, "has_priority:%d, has_packets:%d", has_priority, has_packets);
  ICLI_RC_CHECK_PRINT_RC(icli_cmd_port_status(session_id, plist,  STATISTICS, &port_status_value));
  return VTSS_RC_OK;
}



static void port_icli_duplex_get(vtss_isid_t isid, mesa_port_no_t port_no, const vtss_appl_port_conf_t *port_conf, port_conf_value_t *duplex)
{
  duplex->duplex.has_auto          = FALSE;
  duplex->duplex.has_advertise_fdx = FALSE;
  duplex->duplex.has_advertise_hdx = FALSE;
  duplex->duplex.has_half          = FALSE;
  duplex->duplex.has_full          = FALSE;

  // SFP only ports only support full duplex.
  meba_port_cap_t cap;
  cap = port_isid_port_cap(isid, port_no);
  if (cap & MEBA_PORT_CAP_SFP_DETECT) {
    duplex->duplex.has_full          = TRUE;
    return;
  }

  if ((port_conf->adv_dis & VTSS_APPL_PORT_ADV_DIS_FDX) == 0) {
    duplex->duplex.has_auto = TRUE;
    duplex->duplex.has_advertise_fdx = TRUE;
  }

  if ((port_conf->adv_dis & VTSS_APPL_PORT_ADV_DIS_HDX) == 0) {
    duplex->duplex.has_auto = TRUE;
    duplex->duplex.has_advertise_hdx = TRUE;
  }

  if (port_conf->fdx) {
    duplex->duplex.has_full = TRUE;
  } else {
    duplex->duplex.has_half = TRUE;
  }

  T_DG(TRACE_GRP_ICLI, "has_auto:%d, has_advertise_fdx:%d, has_advertise_hdx:%d, has_half:%d, has_full:%d", duplex->duplex.has_auto, duplex->duplex.has_advertise_fdx, duplex->duplex.has_advertise_hdx, duplex->duplex.has_half, duplex->duplex.has_full);
  T_DG(TRACE_GRP_ICLI, "adv_dis:0x%X, fdx:%d", port_conf->adv_dis, port_conf->fdx);
}


static void port_icli_duplex_set(vtss_appl_port_conf_t *port_conf, const port_conf_value_t duplex)
{

  port_conf->adv_dis |= VTSS_APPL_PORT_ADV_DIS_FDX | VTSS_APPL_PORT_ADV_DIS_HDX;

  T_DG(TRACE_GRP_ICLI, "adv_dis:0x%X, fdx:%d", port_conf->adv_dis, port_conf->fdx);
  if (duplex.duplex.has_auto) {
    // Is it is not specified that either advertise half or full. Then we advertise both
    if (!duplex.duplex.has_advertise_fdx && !duplex.duplex.has_advertise_hdx) {
      port_conf->adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_FDX; // Clear the bit
      port_conf->adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_HDX; // Clear the bit
    }

    if (duplex.duplex.has_advertise_fdx) {
      port_conf->adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_FDX; // Clear the bit
    }

    if (duplex.duplex.has_advertise_hdx) {
      port_conf->adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_HDX; // Clear the bit
    }
    port_conf->fdx = TRUE;
  } else {
    port_conf->fdx = FALSE;
    if (duplex.duplex.has_full) {
      port_conf->fdx = TRUE;
    }
  }
  T_DG(TRACE_GRP_ICLI, "has_auto:%d, has_advertise_fdx:%d, has_advertise_hdx:%dx, has_half:%d, has_full:%d", duplex.duplex.has_auto, duplex.duplex.has_advertise_fdx, duplex.duplex.has_advertise_hdx, duplex.duplex.has_half, duplex.duplex.has_full);
  T_DG(TRACE_GRP_ICLI, "adv_dis:0x%X, fdx:%d", port_conf->adv_dis, port_conf->fdx);
}


// help function for checking that the new configuration (and adjusting) is valid.
// In - session_id         - For message printouts
//      port_conf          - Pointer to the new port configuration
//      update_duplex      - check and in case of mis-configuration overwrite duplex mode
//      update_fiber_speed - check and in case of mis-configuration overwrite fiber speed configuration
//
// Out - auto_cfg_txt - Pointer to where to put information text in case auto adjustment is done.
//
// return : TRUE if new port speed is valid, else FALSE
static BOOL port_icli_is_port_speed_valid(u32 session_id, vtss_appl_port_conf_t *port_conf, BOOL update_duplex, BOOL update_fiber_speed, switch_iter_t sit, port_iter_t pit, i8 *auto_cfg_txt)
{
  // Check for half duplex - Speed / media type takes precedence
  T_DG(TRACE_GRP_ICLI, "fdx:%d, update_fiber_speed:%d, update_duplex:%d, speed:%d, fiber_speed:%d, aneg:%d",
       port_conf->fdx, update_fiber_speed, update_duplex, port_conf->speed, port_conf->dual_media_fiber_speed, port_conf->speed == MESA_SPEED_AUTO);

  port_conf_value_t duplex;
  port_icli_duplex_get(sit.isid, pit.iport, port_conf, &duplex);

  //  if (port_conf->speed == MESA_SPEED_AUTO) {
  //    port_conf->speed = MESA_SPEED_1G;
  //}

  // Checking for dual media and forced speed.
  meba_port_cap_t cap = port_isid_port_cap(sit.isid, pit.iport); // get port capabilities
  if (cap & (MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER)) {
    if (port_conf->dual_media_fiber_speed == MESA_SPEED_FIBER_AUTO && port_conf->speed != MESA_SPEED_AUTO) {
      sprintf(auto_cfg_txt, "doesn't support forced speed in dual media mode (defaulting to auto negotiation speed)\n");
      port_conf->speed = MESA_SPEED_AUTO;
    }
  }

  if (update_duplex) {
    if (port_conf->speed == MESA_SPEED_AUTO) {
      if (!duplex.duplex.has_auto) {
        duplex.duplex.has_auto          = TRUE;
        duplex.duplex.has_advertise_hdx = TRUE;
        duplex.duplex.has_advertise_fdx = TRUE;
        duplex.duplex.has_full          = TRUE;
        duplex.duplex.has_half          = FALSE;
        port_icli_duplex_set(port_conf, duplex);
      }
    } else {
      if (duplex.duplex.has_auto) {
        duplex.duplex.has_auto          = FALSE;
        duplex.duplex.has_advertise_hdx = FALSE;
        duplex.duplex.has_advertise_fdx = FALSE;
        duplex.duplex.has_full          = TRUE;
        duplex.duplex.has_half          = FALSE;
        port_icli_duplex_set(port_conf, duplex);

      }

      if (!port_conf->fdx) {
        if (port_conf->dual_media_fiber_speed != MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED) {
          sprintf(auto_cfg_txt, "does only support half duplex in pure rj45 media mode, duplex changed to full duplex\n");
        } else if ((port_conf->speed != MESA_SPEED_10M && port_conf->speed != MESA_SPEED_100M)) {
          sprintf(auto_cfg_txt, "does only support half duplex in 10 and 100 Mbit mode, duplex changed to full duplex\n");
          port_conf->fdx = TRUE;
        }
      }
    }
  }

  // Checking SFP configuration
  if (update_fiber_speed) {
    if (port_conf->dual_media_fiber_speed == MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED) {
      // Fiber currently disabled - Don't change
    } else {
      // Half duplex not supported for fiber interface
      if (!port_conf->fdx && port_conf->speed != MESA_SPEED_AUTO) {
        sprintf(auto_cfg_txt, "does not support half duplex for SFP interface, duplex changed to full duplex\n");
        port_conf->fdx = TRUE;
      }


      if (cap & (MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER)) {
        if (port_conf->speed == MESA_SPEED_AUTO) {
          T_IG(TRACE_GRP_CONF, "port_conf->dual_media_fiber_speed:%d", port_conf->dual_media_fiber_speed);
          if (port_conf->dual_media_fiber_speed != MESA_SPEED_FIBER_AUTO) {
            port_conf->dual_media_fiber_speed = MESA_SPEED_FIBER_ONLY_AUTO;
          }
        } else if (port_conf->speed == MESA_SPEED_100M) {
          port_conf->dual_media_fiber_speed = MESA_SPEED_FIBER_100FX;
          T_IG(TRACE_GRP_CONF, "port_conf->dual_media_fiber_speed:%d", port_conf->dual_media_fiber_speed);
        } else  if (port_conf->speed == MESA_SPEED_1G) {
          port_conf->dual_media_fiber_speed = MESA_SPEED_FIBER_1000X;
          T_IG(TRACE_GRP_CONF, "port_conf->dual_media_fiber_speed:%d", port_conf->dual_media_fiber_speed);
        } else {
          sprintf(auto_cfg_txt, "does not supported the requested speed\n");
          return FALSE;
        }
      }
    }
  }
  return TRUE;
}

// Help function for setting port configurations
// In : max_length - Max frame length (MTU)
//      plist - List of ports to configure
//      no - TRUE when the configuration parameter shall be set to default.
static mesa_rc port_icli_port_conf_set(i32 session_id, const port_conf_type_t port_conf_type, const port_conf_value_t *port_conf_new, icli_stack_port_range_t *plist, BOOL no)
{
  switch_iter_t sit;
  port_iter_t pit;
  mesa_rc rc               = VTSS_RC_OK;
  meba_port_cap_t cap;
  mesa_rc port_setup_valid = TRUE;
  i8 auto_cfg_txt[100];
  u8 i;
  strcpy(&auto_cfg_txt[0], "");

  // Loop through all switches in a stack
  VTSS_RC(icli_switch_iter_init(&sit));
  while (icli_switch_iter_getnext(&sit, plist)) {
    T_RG(TRACE_GRP_ICLI, "isid:%d, port_conf_type:%d", sit.isid, port_conf_type);
    // Loop though the ports
    VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT));
    while (icli_port_iter_getnext(&pit, plist)) {
      strcpy(&auto_cfg_txt[0], "");
      vtss_appl_port_conf_t port_conf;
      vtss_appl_port_conf_t port_conf_def; // Used to get the default configuration settings
      port_isid_port_info_t info;

      T_IG(TRACE_GRP_ICLI, "isid:%d, iport:%d, conf_type:%d", sit.isid, pit.iport, port_conf_type);
      VTSS_RC(port_isid_port_info_get(sit.isid, pit.iport, &info));

      // Get current configuration
      vtss_ifindex_t ifindex;
      VTSS_RC(vtss_ifindex_from_port(sit.isid, pit.iport, &ifindex));
      VTSS_RC(vtss_appl_port_conf_get(ifindex, &port_conf));

      // Get default values for this interface
      VTSS_RC(vtss_port_conf_default(sit.isid, pit.iport, &port_conf_def));

      switch (port_conf_type) {
      case MTU:
        // Setup max length (MTU)
        if (no) {
          port_conf.max_length = port_conf_def.max_length; // update MTU to default value
        } else {
          port_conf.max_length = port_conf_new->max_length; // update new MTU
        }
        break;

      case SHUT_DOWN:
        // Setup port enable/disable
        port_conf.admin.enable = no; // No means "no shut down" = enable
        break;

      case MEDIA:
        cap = port_isid_port_cap(sit.isid, pit.iport); // get port capabilities

        T_RG_PORT(TRACE_GRP_ICLI, pit.iport, "cap:0x%X", cap);

        // It only make sense to change media type for dual media port
        if (!no) {
          // Check if fiber is supported
          if ((cap & (MEBA_PORT_CAP_DUAL_SFP_DETECT | MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER)) == 0) {
            icli_print_port_info_txt(session_id, sit.usid, pit.uport); // Print interface
            ICLI_PRINTF("is not dual media port.\n");
            break;
          }

          if (port_conf_new->media_type.sfp) {
            T_IG_PORT(TRACE_GRP_ICLI, pit.iport, "cap:0x%X", cap );
            if ((cap & (MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER | MEBA_PORT_CAP_SFP_ONLY)) == 0) {
              icli_print_port_info_txt(session_id, sit.usid, pit.uport); // Print interface
              ICLI_PRINTF("only supports rj45 media type. New media type configuration ignored.\n");
              break;
            }
          }

          if (port_conf_new->media_type.rj45) {
            T_IG_PORT(TRACE_GRP_ICLI, pit.iport, "cap:0x%X, 0x%X", cap, cap & (MEBA_PORT_CAP_COPPER | MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER));
            if ((cap & (MEBA_PORT_CAP_COPPER | MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER)) == 0) {
              icli_print_port_info_txt(session_id, sit.usid, pit.uport); // Print interface
              ICLI_PRINTF("only supports SFP media type. New media type configuration ignored.\n");
              break;
            }
          }
        }

        // Setup port media type
        if (no) {
          // Default is dual media
          port_conf.dual_media_fiber_speed = port_conf_def.dual_media_fiber_speed;
          port_conf.speed = MESA_SPEED_AUTO;
          port_conf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_ALL;
        } else {
          VTSS_RC(vtss_port_media2conf(port_conf_new->media_type.rj45, port_conf_new->media_type.sfp, &port_conf, sit.isid, pit.iport));

          T_IG_PORT(TRACE_GRP_ICLI, pit.iport, "media:%d, cap:0x%X, sfp_only:0x%X, dual_media_fiber_speed:%d, port_conf.speed:%d, port_conf_new->media_type.sfp:%d",
                    port_conf_new->media_type.rj45, cap, cap & MEBA_PORT_CAP_SFP_ONLY, port_conf.dual_media_fiber_speed, port_conf.speed, port_conf_new->media_type.sfp);
        }

        port_setup_valid = port_icli_is_port_speed_valid(session_id, &port_conf, TRUE, FALSE, sit, pit, &auto_cfg_txt[0]);

        T_NG_PORT(TRACE_GRP_ICLI, pit.iport, "port_conf_type:%d, no:%d, port_conf_new->media_type.sfp:%d, port_conf_new->media_type.rj45:%d",
                  port_conf_type, no, port_conf_new->media_type.sfp, port_conf_new->media_type.rj45);
        break;

      case FORCED_SPEED:
        if (no) {
          port_conf.speed = port_conf_def.speed;
        } else {
          port_conf.speed = port_conf_new->speed;
        }

        // Forced_speed
        //        port_conf.autoneg = FALSE;

        port_setup_valid = port_icli_is_port_speed_valid(session_id, &port_conf, TRUE, TRUE, sit, pit, &auto_cfg_txt[0]);

        T_DG_PORT(TRACE_GRP_ICLI, pit.iport, "speed:%d, no:%d", port_conf.speed, no);
        break;

      case AUTONEG:
        T_IG_PORT(TRACE_GRP_ICLI, pit.iport, "speed:%d, no:%d", port_conf.speed, no);
        // Setup auto negotiation
        if (no) {
          port_conf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_ALL;
          port_conf.speed = port_conf_def.speed;
        } else {
          port_conf.speed = MESA_SPEED_AUTO;
          // Change the advertisement if user has set any of the advertisement settings
          if (port_conf_new->autoneg_conf.has_neg_10g | port_conf_new->autoneg_conf.has_neg_5g | port_conf_new->autoneg_conf.has_neg_2500 |
              port_conf_new->autoneg_conf.has_neg_1000 | port_conf_new->autoneg_conf.has_neg_100 | port_conf_new->autoneg_conf.has_neg_10) {
            port_conf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_10G;
            port_conf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_5G;
            port_conf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_2500M;
            port_conf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_1G;
            port_conf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_100M; //
            port_conf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_10M; //

            if (port_conf_new->autoneg_conf.has_neg_10g) {
              port_conf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_10G; // Clear the bit
            }

            if (port_conf_new->autoneg_conf.has_neg_5g) {
              port_conf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_5G; // Clear the bit
            }

            if (port_conf_new->autoneg_conf.has_neg_2500) {
              port_conf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_2500M; // Clear the bit
            }

            if (port_conf_new->autoneg_conf.has_neg_1000) {
              port_conf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_1G; // Clear the bit
            }

            if (port_conf_new->autoneg_conf.has_neg_100) {
              port_conf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_100M; // Clear the bit
            }

            if (port_conf_new->autoneg_conf.has_neg_10) {
              port_conf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_10M; // Clear the bit
            }
          } else {
            port_conf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_ALL;
          }
        }

        port_setup_valid = port_icli_is_port_speed_valid(session_id, &port_conf, TRUE, TRUE, sit, pit, &auto_cfg_txt[0]);
        break;

      case DUPLEX:
        if (no) {
          //Setup auto negotiation advertisement
          if (port_conf_new->duplex.has_advertise_fdx) {
            port_conf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_FDX; //
          } else if (port_conf_new->duplex.has_advertise_hdx) {
            port_conf.adv_dis |= VTSS_APPL_PORT_ADV_DIS_HDX; // Set the bit
          } else {
            icli_print_port_info_txt(session_id, sit.usid, pit.uport); // Print interface
            ICLI_PRINTF("can't advertise no duplex. Advertisement changed to advertise both full and half duplex.\n");
            port_conf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_FDX; // Clear the bit
            port_conf.adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_HDX; // Clear the bit
          }
        } else {
          port_icli_duplex_set(&port_conf, *port_conf_new);
        }

        port_setup_valid = port_icli_is_port_speed_valid(session_id, &port_conf, TRUE, TRUE, sit, pit, &auto_cfg_txt[0]);

        break;
      case FLOW_CONTROL:
        if (no) {
          port_conf.flow_control = port_conf_def.flow_control;
        } else {
          port_conf.flow_control = port_conf_new->flow_control.fc_enable ;
        }
        break;
      case PFC:
        for (i = 0; i < VTSS_PRIOS; i++) {
          if (port_conf_new->priority_fc.pfc[i]) {
            port_conf.pfc[i] = no ? 0 : 1;
          }
        }
        break;
      case EXCESSIVE_RESTART:
        if (no) {
          port_conf.exc_col_cont = port_conf_def.exc_col_cont;
        } else {
          port_conf.exc_col_cont = TRUE;
        }
        break;
      case FRM_LEN_CHK:
        if (no) {
          port_conf.frame_length_chk = port_conf_def.frame_length_chk;
        } else {
          port_conf.frame_length_chk = TRUE;
        }
        break;
      }


      T_DG_PORT(TRACE_GRP_ICLI, pit.iport, "speed:%d, no:%d, port_conf.adv_dis:0x%X", port_conf.speed, no, port_conf.adv_dis);
      //
      // Set the new configuration
      //

      // Give an error message if the parameters is not supported for the given interface
      if ((rc = vtss_appl_port_conf_set(ifindex, &port_conf)) != VTSS_RC_OK && port_setup_valid) {
        T_IG_PORT(TRACE_GRP_ICLI, pit.iport, "%s, port_setup_valid:0x%X, rc:%s", port_error_txt(vtss_appl_port_conf_set(ifindex, &port_conf)), port_setup_valid, error_txt(rc));
        icli_print_port_info_txt(session_id, sit.usid, pit.uport);
        ICLI_PRINTF("does not support this mode/speed\n");
        rc = VTSS_RC_OK; // No need to return error code, since we have already given an error message.
      } else {
        T_IG_PORT(TRACE_GRP_ICLI, pit.iport, "strlen:%d", strlen(auto_cfg_txt));
        // Print if there has been any auto adjustments
        if (strlen(auto_cfg_txt) > 0) {
          icli_print_port_info_txt(session_id, sit.usid, pit.uport);
          ICLI_PRINTF("%s", &auto_cfg_txt[0]);
          rc = VTSS_RC_OK; // No need to return error code, since we have already given an error message.
        }
      }
    }
  }
  return rc;
}

/***************************************************************************/
/*  Functions called by iCLI                                                */
/****************************************************************************/
//  see port_icli_functions.h
BOOL port_icli_runtime_qos_dot3br(u32                session_id,
                                  icli_runtime_ask_t ask,
                                  icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
    runtime->present = FALSE;
#if defined(VTSS_SW_OPTION_QOS_ADV)
    runtime->present = MESA_CAP(MESA_CAP_QOS_FRAME_PREEMPTION);
#endif
    return TRUE;
  default:
    return FALSE;
  }
}

//  see port_icli_functions.h
BOOL port_icli_runtime_qos_queues(u32                session_id,
                                  icli_runtime_ask_t ask,
                                  icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
#if defined(VTSS_SW_OPTION_QOS)
    runtime->present = TRUE;
#else
    runtime->present = FALSE;
#endif
    return TRUE;
  case ICLI_ASK_BYWORD:
    icli_sprintf(runtime->byword, "<queue : %u-%u>", VTSS_PRIO_START, VTSS_PRIO_END - 1);
    return TRUE;
  case ICLI_ASK_RANGE:
    runtime->range.type = ICLI_RANGE_TYPE_UNSIGNED;
    runtime->range.u.sr.cnt = 1;
    runtime->range.u.sr.range[0].min = VTSS_PRIO_START;
    runtime->range.u.sr.range[0].max = VTSS_PRIO_END - 1;
    return TRUE;
  default:
    break;
  }
  return FALSE;
}

// Returns VTSS_RC_OK if any port in a port list (plist) is a cu port, else return VTSS_RC_ERROR
static mesa_rc is_any_port_cu(icli_stack_port_range_t *plist)
{
  switch_iter_t sit;
  mesa_rc at_least_one_port_is_cu = VTSS_RC_ERROR;

  VTSS_RC(switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID));
  while (icli_switch_iter_getnext(&sit, plist)) {
    port_iter_t pit;
    VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT | PORT_ITER_FLAGS_NPI));
    while (icli_port_iter_getnext(&pit, plist)) {
      meba_port_cap_t cap = port_isid_port_cap(sit.isid, pit.iport);
      if (cap & MEBA_PORT_CAP_SFP_ONLY) {
        T_NG(TRACE_GRP_ICLI, "SFP only port - Continue");
        continue;
      }

      // Ok - This port is a cu port, we can stop checking the rest of the ports.
      at_least_one_port_is_cu = VTSS_RC_OK;
      goto done;
    }
  }

done:
  T_DG(TRACE_GRP_ICLI, "at_least_one_port_is_cu:%d", at_least_one_port_is_cu);
  return at_least_one_port_is_cu;
}


// Function for checking if a specific interface type is part of the currently (by user) selected interfaces.
// IN : session_id - Current session id
//      type       - Interface type to check for,
// Return TRUE if interface type is found within the currently selected interfaces, else FALSE
static BOOL port_icli_if_exist(u32 session_id, icli_port_type_t type, BOOL must_be_cu_port = FALSE)
{
  icli_variable_value_t       value;
  icli_stack_port_range_t     *plist;

  if (ICLI_MODE_PARA_GET(NULL, &value) != ICLI_RC_OK) {
    ICLI_PRINTF("%% Fail to get mode para\n");
    return FALSE;
  }

  // port list
  plist = &(value.u.u_port_type_list);

  if (must_be_cu_port) {
    if (is_any_port_cu(plist) != VTSS_RC_OK) {
      return FALSE;
    }
  }

  for (auto i = 0; i < plist->cnt; ++i ) {
    T_DG(TRACE_GRP_ICLI, "i:%d, type:%d, cnt:%d, must_be_cu_port:%d, ask_type:%d",
         i, plist->switch_range[i].port_type, plist->switch_range[i].port_cnt, must_be_cu_port, type);

    if (plist->switch_range[i].port_type == type) {
      T_RG(TRACE_GRP_ICLI, "Type valid");
      return TRUE;
      break;
    }
  }
  T_RG(TRACE_GRP_ICLI, "Type in-valid");
  return FALSE;
}

static BOOL port_icli_if_auto_speed_support(u32 session_id, meba_port_cap_t cap, BOOL exact)
{
  switch_iter_t             sit;
  port_iter_t               pit;
  icli_variable_value_t     value;
  icli_stack_port_range_t   *plist;
  vtss_appl_port_status_t   port_status;

  if (ICLI_MODE_PARA_GET(NULL, &value) != ICLI_RC_OK) {
    ICLI_PRINTF("%% Fail to get mode para\n");
    return FALSE;
  }

  // port list
  plist = &(value.u.u_port_type_list);

  VTSS_RC(switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG));
  while (icli_switch_iter_getnext(&sit, plist)) {
    VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT | PORT_ITER_FLAGS_NPI));
    while (icli_port_iter_getnext(&pit, plist)) {
      vtss_ifindex_t ifindex;
      VTSS_RC(vtss_ifindex_from_port(sit.isid, pit.iport, &ifindex));
      VTSS_RC(vtss_appl_port_status_get(ifindex, &port_status));
      if ((port_status.cap & (MEBA_PORT_CAP_COPPER | MEBA_PORT_CAP_AUTONEG)) != (MEBA_PORT_CAP_COPPER | MEBA_PORT_CAP_AUTONEG)) {
        continue;
      }

      if ((exact && (port_status.cap & cap) == cap) ||
          (!exact && (port_status.cap & cap) != 0)) {
        return TRUE;
      }
    }
  }

  return FALSE;
}

//Currently is used for forced speed/duplex mode
static BOOL port_icli_if_force_speed_support(u32 session_id)
{
  switch_iter_t             sit;
  port_iter_t               pit;
  icli_variable_value_t     value;
  icli_stack_port_range_t   *plist;
  vtss_appl_port_status_t   port_status;

  if (ICLI_MODE_PARA_GET(NULL, &value) != ICLI_RC_OK) {
    ICLI_PRINTF("%% Fail to get mode para\n");
    return FALSE;
  }

  // port list
  plist = &(value.u.u_port_type_list);

  VTSS_RC(switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG));
  while (icli_switch_iter_getnext(&sit, plist)) {
    VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT | PORT_ITER_FLAGS_NPI));
    while (icli_port_iter_getnext(&pit, plist)) {
      vtss_ifindex_t ifindex;
      VTSS_RC(vtss_ifindex_from_port(sit.isid, pit.iport, &ifindex));
      VTSS_RC(vtss_appl_port_status_get(ifindex, &port_status));
      if (((port_status.cap & (MEBA_PORT_CAP_COPPER | MEBA_PORT_CAP_NO_FORCE)) == (MEBA_PORT_CAP_COPPER | MEBA_PORT_CAP_NO_FORCE)) &&
          ((port_status.cap & MEBA_PORT_CAP_ANY_FIBER) == 0x0) ) {
        // copper only and no force capability
        continue;
      } else {
        return TRUE;
      }
    }
  }
  return FALSE;
}


//  see port_icli_functions.h
BOOL port_icli_runtime_10g(u32                session_id,
                           icli_runtime_ask_t ask,
                           icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
    runtime->present =  port_icli_if_exist(session_id, ICLI_PORT_TYPE_TEN_GIGABIT_ETHERNET);
    return TRUE;
  default:
    return FALSE;
  }
}

//  see port_icli_functions.h
BOOL port_icli_runtime_10M(u32                session_id,
                           icli_runtime_ask_t ask,
                           icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
    runtime->present =  (port_icli_if_exist(session_id, ICLI_PORT_TYPE_FAST_ETHERNET) ||
                         port_icli_if_exist(session_id, ICLI_PORT_TYPE_GIGABIT_ETHERNET)) &&
                        (port_icli_if_force_speed_support(session_id));
    return TRUE;
  default:
    return FALSE;
  }
}

//  see port_icli_functions.h
BOOL port_icli_runtime_100M(u32                session_id,
                            icli_runtime_ask_t ask,
                            icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
    runtime->present =  (port_icli_if_exist(session_id, ICLI_PORT_TYPE_FAST_ETHERNET) ||
                         port_icli_if_exist(session_id, ICLI_PORT_TYPE_GIGABIT_ETHERNET) ||
                         port_icli_if_exist(session_id, ICLI_PORT_TYPE_2_5_GIGABIT_ETHERNET) ||
                         port_icli_if_exist(session_id, ICLI_PORT_TYPE_TEN_GIGABIT_ETHERNET)) &&
                        (port_icli_if_force_speed_support(session_id));

    return TRUE;
  default:
    return FALSE;
  }
}

//  see port_icli_functions.h
BOOL port_icli_runtime_2g5(u32                session_id,
                           icli_runtime_ask_t ask,
                           icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
    runtime->present =  (port_icli_if_exist(session_id, ICLI_PORT_TYPE_2_5_GIGABIT_ETHERNET) ||
                         port_icli_if_exist(session_id, ICLI_PORT_TYPE_TEN_GIGABIT_ETHERNET)) &&
                        (port_icli_if_force_speed_support(session_id));
    return TRUE;
  default:
    return FALSE;
  }
}

BOOL port_icli_runtime_1g(u32                session_id,
                          icli_runtime_ask_t ask,
                          icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
    runtime->present =  (port_icli_if_exist(session_id, ICLI_PORT_TYPE_GIGABIT_ETHERNET) ||
                         port_icli_if_exist(session_id, ICLI_PORT_TYPE_2_5_GIGABIT_ETHERNET) ||
                         port_icli_if_exist(session_id, ICLI_PORT_TYPE_TEN_GIGABIT_ETHERNET)) &&
                        (port_icli_if_force_speed_support(session_id));
    return TRUE;
  default:
    return FALSE;
  }
}

BOOL port_icli_runtime_force_duplex_half(u32                session_id,
                                         icli_runtime_ask_t ask,
                                         icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
    runtime->present =  port_icli_if_force_speed_support(session_id);
    return TRUE;
  default:
    return FALSE;
  }
}

BOOL port_icli_runtime_force_duplex_full(u32                session_id,
                                         icli_runtime_ask_t ask,
                                         icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
    runtime->present =  port_icli_if_force_speed_support(session_id);
    return TRUE;
  default:
    return FALSE;
  }
}


//  see port_icli_functions.h
BOOL port_icli_runtime_advertise_auto_speed_1G(u32                session_id,
                                               icli_runtime_ask_t ask,
                                               icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
    runtime->present =  port_icli_if_auto_speed_support(session_id, MEBA_PORT_CAP_1G_FDX, TRUE);
    return TRUE;
  default:
    return FALSE;
  }
}

//  see port_icli_functions.h
BOOL port_icli_runtime_advertise_auto_speed_100M(u32                session_id,
                                                 icli_runtime_ask_t ask,
                                                 icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
    runtime->present = port_icli_if_auto_speed_support(session_id, MEBA_PORT_CAP_100M_FDX | MEBA_PORT_CAP_100M_FDX, FALSE);
    return TRUE;
  default:
    return FALSE;
  }
}

//  see port_icli_functions.h
BOOL port_icli_runtime_advertise_auto_speed_10M(u32                session_id,
                                                icli_runtime_ask_t ask,
                                                icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
    runtime->present = port_icli_if_auto_speed_support(session_id, MEBA_PORT_CAP_10M_HDX | MEBA_PORT_CAP_10M_FDX, FALSE);
    return TRUE;
  default:
    return FALSE;
  }
}

BOOL port_icli_runtime_advertise_auto_speed_2G5(u32                session_id,
                                                icli_runtime_ask_t ask,
                                                icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
    runtime->present = port_icli_if_auto_speed_support(session_id, MEBA_PORT_CAP_2_5G_FDX, TRUE);
    return TRUE;
  default:
    return FALSE;
  }
}

BOOL port_icli_runtime_advertise_auto_speed_5G(u32                session_id,
                                               icli_runtime_ask_t ask,
                                               icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
    runtime->present = port_icli_if_auto_speed_support(session_id, MEBA_PORT_CAP_5G_FDX, TRUE);
    return TRUE;
  default:
    return FALSE;
  }
}

BOOL port_icli_runtime_advertise_auto_speed_10G(u32                session_id,
                                                icli_runtime_ask_t ask,
                                                icli_runtime_t     *runtime)
{
  switch (ask) {
  case ICLI_ASK_PRESENT:
    runtime->present = port_icli_if_auto_speed_support(session_id, MEBA_PORT_CAP_10G_FDX, TRUE);
    return TRUE;
  default:
    return FALSE;
  }
}

//  see port_icli_functions.h
mesa_rc port_icli_statistics_clear(i32 session_id, icli_stack_port_range_t *plist)
{
  port_status_value_t port_status_value;
  port_status_value.statistics.has_clear = TRUE;
  T_RG(TRACE_GRP_ICLI, "Enter");
  ICLI_RC_CHECK_PRINT_RC(icli_cmd_port_status(session_id, plist,  STATISTICS, &port_status_value));
  return VTSS_RC_OK;
}

//  see port_icli_functions.h
mesa_rc port_icli_capabilities(i32 session_id, icli_stack_port_range_t *plist)
{
  port_status_value_t port_status_value;
  port_status_value.port_capa_show.sfp_detect = TRUE;

  ICLI_RC_CHECK_PRINT_RC(icli_cmd_port_status(session_id, plist, CAPABILITIES, &port_status_value));
  return VTSS_RC_OK;
}

//  see port_icli_functions.h
mesa_rc icli_cmd_port_veriphy(i32 session_id, icli_stack_port_range_t *plist, BOOL show)
{
#ifdef VTSS_UI_OPT_VERIPHY
  port_isid_port_info_t info;
  CapArray<port_veriphy_mode_t, MESA_CAP_PORT_CNT> mode;
  switch_iter_t         sit;
  if (!msg_switch_is_master()) {
    return VTSS_APPL_PORT_ERROR_MUST_BE_MASTER;
  }


  VTSS_RC(switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID));
  while (icli_switch_iter_getnext(&sit, plist)) {
    // Loop through all ports
    port_iter_t pit;
    VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT | PORT_ITER_FLAGS_NPI));

    // Start by setting all port to NOT run veriphy
    while (icli_port_iter_getnext(&pit, NULL)) {
      mode[pit.iport] = PORT_VERIPHY_MODE_NONE;
    }

    if (!show) {
      ICLI_PRINTF("Starting VeriPHY - Please wait\n");

      VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT | PORT_ITER_FLAGS_NPI));
      // Start veriphy at the selected ports.
      while (icli_port_iter_getnext(&pit, plist)) {
        mode[pit.iport] = (port_isid_port_info_get(sit.isid, pit.iport, &info) != VTSS_OK ||
                           (info.cap & MEBA_PORT_CAP_1G_PHY) == 0 ? PORT_VERIPHY_MODE_NONE :
                           PORT_VERIPHY_MODE_FULL);
      }
      VTSS_RC(port_mgmt_veriphy_start(sit.isid, mode.data()));
    }
  }

  // Get veriPHY result
  VTSS_RC(switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID));
  while (icli_switch_iter_getnext(&sit, plist)) {
    port_iter_t               pit;
    char                      buf[250], *p;
    int                       i;
    mesa_phy_veriphy_result_t veriphy;
    mesa_rc                   port_info_rc = VTSS_RC_OK, veriphy_get_rc = VTSS_RC_OK;
    VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT | PORT_ITER_FLAGS_NPI));

    memset(&veriphy, 0, sizeof(veriphy)); // Make sure that veriphy is initialized (even though it is not used if not initialized).

    while (icli_port_iter_getnext(&pit, plist)) {


      BOOL result_invalid = ((port_info_rc = port_isid_port_info_get(sit.isid, pit.iport, &info) != VTSS_OK) ||
                             (info.cap & MEBA_PORT_CAP_1G_PHY) == 0 ||
                             (veriphy_get_rc = vtss_port_mgmt_veriphy_get(sit.isid, pit.iport, &veriphy, 60) != VTSS_OK));

      T_I("port_info_rc:%d, cap:" VPRI64x ", veriphy_get_rc:%d", port_info_rc, info.cap & MEBA_PORT_CAP_1G_PHY, veriphy_get_rc);

      if (pit.first) {
        sprintf(buf, "%-23s %-7s %-7s %-7s %-7s %-7s %-7s %-7s %-7s", "Interface", "Pair A", "Length", "Pair B,", "Length", "Pair C", "Length", "Pair D", "Length");
        icli_table_header(session_id, buf);
      }

      // Print interface
      ICLI_PRINTF("%-23s ", icli_port_info_txt(sit.usid, pit.uport, &buf[0]));

      if (result_invalid) {
        ICLI_PRINTF("No test results\n");
      } else {
        p = &buf[0];
        for (i = 0; i < 4; i++) {
          p += sprintf(p, "%-7s %-7d ",
                       port_mgmt_veriphy_txt(veriphy.status[i]), veriphy.length[i]);
        }
        ICLI_PRINTF("%s\n", buf);
      }
    }
  }
#endif
  return VTSS_RC_OK;
}

//  see port_icli_functions.h
mesa_rc port_icli_veriphy(i32 session_id, icli_stack_port_range_t *plist, BOOL show)
{
  ICLI_RC_CHECK_PRINT_RC(icli_cmd_port_veriphy(session_id, plist, show));
  return VTSS_RC_OK;
}

//  see port_icli_functions.h
mesa_rc port_icli_status(i32 session_id, icli_stack_port_range_t *plist, BOOL has_errdisable)
{
  port_status_value_t port_status_value;
  port_status_value.plain.has_errdisable = has_errdisable;

  ICLI_RC_CHECK_PRINT_RC(icli_cmd_port_status(session_id, plist, STATUS, &port_status_value));
  return VTSS_RC_OK;
}

//  see port_icli_functions.h
mesa_rc port_icli_media_type(i32 session_id, icli_stack_port_range_t *plist, BOOL has_rj45, BOOL has_sfp, BOOL no)
{
  port_conf_value_t port_conf_value;

  port_conf_value.media_type.rj45 = has_rj45;
  port_conf_value.media_type.sfp = has_sfp;
  ICLI_RC_CHECK_PRINT_RC(port_icli_port_conf_set(session_id, MEDIA, &port_conf_value, plist, no));
  return VTSS_RC_OK;
}

//  see port_icli_functions.h
mesa_rc port_icli_mtu(i32 session_id, u16 max_length, icli_stack_port_range_t *plist, BOOL no)
{
  port_conf_value_t port_conf_value;

  port_conf_value.max_length = max_length;
  ICLI_RC_CHECK_PRINT_RC(port_icli_port_conf_set(session_id, MTU, &port_conf_value, plist, no));
  return VTSS_RC_OK;
}

// See port_icli_functions.h
mesa_rc port_icli_shutdown(i32 session_id, icli_stack_port_range_t *plist, BOOL no)
{
  ICLI_RC_CHECK_PRINT_RC(port_icli_port_conf_set(session_id, SHUT_DOWN, NULL, plist, no));
  return VTSS_RC_OK;
}

// See port_icli_functions.h
mesa_rc port_icli_flow_control(i32 session_id, icli_stack_port_range_t *plist, BOOL has_on, BOOL has_off, BOOL no)
{
  port_conf_value_t port_conf_value;
  port_conf_value.flow_control.fc_enable = has_on; // If has_on is false then has_off is TRUE.

  ICLI_RC_CHECK_PRINT_RC(port_icli_port_conf_set(session_id, FLOW_CONTROL, &port_conf_value, plist, no));
  return VTSS_RC_OK;
}

mesa_rc port_icli_pfc(i32 session_id, icli_stack_port_range_t *plist, BOOL *pfc, BOOL no)
{
  port_conf_value_t port_conf_value;
  u32 i;

  for (i = 0; i < VTSS_PRIOS; i++) {
    port_conf_value.priority_fc.pfc[i] = pfc[i];
  }

  ICLI_RC_CHECK_PRINT_RC(port_icli_port_conf_set(session_id, PFC, &port_conf_value, plist, no));
  return VTSS_RC_OK;

}

// See port_icli_functions.h
mesa_rc port_icli_speed(i32 session_id, icli_stack_port_range_t *plist,  BOOL has_speed_10g,
                        BOOL has_speed_2g5, BOOL has_speed_1g, BOOL has_speed_100m, BOOL has_speed_10m,
                        BOOL has_speed_auto, BOOL has_neg_10, BOOL has_neg_100, BOOL has_neg_1000,
                        BOOL has_neg_2500, BOOL has_neg_5g, BOOL has_neg_10g, BOOL no)
{
  port_conf_value_t port_conf_value;

  if (has_speed_10g) {
    port_conf_value.speed = MESA_SPEED_10G;
  } else   if (has_speed_2g5) {
    port_conf_value.speed = MESA_SPEED_2500M;
  } else if (has_speed_1g) {
    port_conf_value.speed = MESA_SPEED_1G;
  } else if (has_speed_100m) {
    port_conf_value.speed = MESA_SPEED_100M;
  } else if (has_speed_10m) {
    port_conf_value.speed = MESA_SPEED_10M;
  }

  if (has_speed_auto) {
    port_conf_value.speed = MESA_SPEED_AUTO;
    port_conf_value.autoneg_conf.autoneg = TRUE;
  }
  port_conf_value.autoneg_conf.has_neg_10   = has_neg_10;
  port_conf_value.autoneg_conf.has_neg_100  = has_neg_100;
  port_conf_value.autoneg_conf.has_neg_1000 = has_neg_1000;
  port_conf_value.autoneg_conf.has_neg_2500 = has_neg_2500;
  port_conf_value.autoneg_conf.has_neg_5g   = has_neg_5g;
  port_conf_value.autoneg_conf.has_neg_10g  = has_neg_10g;

  ICLI_RC_CHECK_PRINT_RC(port_icli_port_conf_set(session_id,  has_speed_auto ? AUTONEG : FORCED_SPEED, &port_conf_value, plist, no));
  return VTSS_RC_OK;
}

// See port_icli_functions.h
mesa_rc port_icli_duplex(i32 session_id, icli_stack_port_range_t *plist, BOOL has_half, BOOL has_full, BOOL has_auto, BOOL has_advertise_hdx, BOOL has_advertise_fdx, BOOL no)
{
  port_conf_value_t port_conf_value;

  port_conf_value.duplex.has_advertise_hdx = has_advertise_hdx;
  port_conf_value.duplex.has_advertise_fdx = has_advertise_fdx;
  port_conf_value.duplex.has_full = has_full;
  port_conf_value.duplex.has_half = has_half;
  port_conf_value.duplex.has_auto = has_auto;
  ICLI_RC_CHECK_PRINT_RC(port_icli_port_conf_set(session_id, DUPLEX, &port_conf_value, plist, no));
  return VTSS_RC_OK;
}

// See port_icli_functions.h
mesa_rc port_icli_excessive_restart(i32 session_id, icli_stack_port_range_t *plist, BOOL no)
{
  port_conf_value_t port_conf_value; // Dummy. In fact no data is needed for this command
  port_conf_value.duplex.has_auto = TRUE;
  ICLI_RC_CHECK_PRINT_RC(port_icli_port_conf_set(session_id, EXCESSIVE_RESTART, &port_conf_value, plist, no));
  return VTSS_RC_OK;
}

mesa_rc port_icli_description(i32 session_id, icli_stack_port_range_t *plist, const char *descr, BOOL enable, BOOL show)
{
  switch_iter_t           sit;
  port_iter_t             pit;
  vtss_appl_port_descr_t  ifc_desc;

  VTSS_RC(icli_switch_iter_init(&sit));
  while (icli_switch_iter_getnext(&sit, plist)) {
    VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT | PORT_ITER_FLAGS_NPI));
    while (icli_port_iter_getnext(&pit, plist)) {
      vtss_ifindex_t ifindex;
      VTSS_RC(vtss_ifindex_from_port(sit.isid, pit.iport, &ifindex));

      if (!show) {
        if (enable) {
          strcpy(ifc_desc.description, descr);
          VTSS_RC(vtss_appl_ifc_desc_set(ifindex, &ifc_desc));
        } else {
          strcpy(ifc_desc.description, "");
          VTSS_RC(vtss_appl_ifc_desc_set(ifindex, &ifc_desc));
        }
      } else {
        VTSS_RC(vtss_appl_ifc_desc_get(ifindex, &ifc_desc));
        char str_buf[VTSS_APPL_PORT_IF_DESCR_MAX_LEN];
        if (pit.first) {
          sprintf(str_buf, "%-23s %-23s", "Interface", "Description");
          icli_table_header(session_id, str_buf);
        }
        ICLI_PRINTF("%-23s %s\n", icli_port_info_txt(sit.usid, pit.uport, str_buf), ifc_desc.description);
      }
    }
  }
  return VTSS_RC_OK;
}

mesa_rc port_icli_frm_len_chk(i32 session_id, icli_stack_port_range_t *plist, BOOL no)
{
  port_conf_value_t port_conf_value; // Dummy. In fact no data is needed for this command
  ICLI_RC_CHECK_PRINT_RC(port_icli_port_conf_set(session_id, FRM_LEN_CHK, &port_conf_value, plist, no));
  return VTSS_RC_OK;
}

//  see port_icli_functions.h
mesa_rc port_icli_debug_change_event(i32 session_id, icli_stack_port_range_t *plist, BOOL has_up, BOOL has_down, BOOL has_clear)
{
  port_status_value_t port_status_value;
  port_status_value.change_event.has_down = has_down;
  port_status_value.change_event.has_up   = has_up;
  port_status_value.change_event.has_clear = has_clear;

  ICLI_RC_CHECK_PRINT_RC(icli_cmd_port_status(session_id, plist, CHANGE_EVENT, &port_status_value));
  return VTSS_RC_OK;
}

// Help function for printing port registrations
static void port_reg(i32 session_id, BOOL *first, vtss_module_id_t module_id, vtss_tick_count_t ticks)
{
  ulong msec;

  if (*first) {
    *first = 0;
    icli_table_header(session_id, "Module           Max Callback [msec]");
  }
  msec = VTSS_OS_TICK2MSEC(ticks);
  ICLI_PRINTF("%-15.15s  %lu\n", vtss_module_names[module_id], msec);
}

//  see port_icli_functions.h
mesa_rc port_icli_debug_show_registrations(i32 session_id, BOOL clear)
{
  port_change_reg_t        local_reg;
  port_global_change_reg_t global_reg;
  port_shutdown_reg_t      shutdown_reg;
  BOOL                     first;

  /* Local port change registrations */
  local_reg.module_id = VTSS_MODULE_ID_NONE;
  first = 1;
  while (port_change_reg_get(&local_reg, clear) == VTSS_RC_OK) {
    if (first) {
      icli_header(session_id, "Local Port Change Registrations", TRUE);
    }
    port_reg(session_id, &first, local_reg.module_id, local_reg.max_ticks);
  }

  /* Global port change registrations */
  global_reg.module_id = VTSS_MODULE_ID_NONE;
  first = 1;
  while (port_global_change_reg_get(&global_reg, clear) == VTSS_RC_OK) {
    if (first) {
      icli_header(session_id, "Global Port Change Registrations", TRUE);
    }
    port_reg(session_id, &first, global_reg.module_id, global_reg.max_ticks);
  }

  /* Local port shutdown registrations */
  shutdown_reg.module_id = VTSS_MODULE_ID_NONE;
  first = 1;
  while (port_shutdown_reg_get(&shutdown_reg, clear) == VTSS_RC_OK) {
    if (first) {
      icli_header(session_id, "Local Port Shutdown Registrations", TRUE);
    }
    port_reg(session_id, &first, shutdown_reg.module_id, shutdown_reg.max_ticks);
  }

  return VTSS_RC_OK;
}
//  see port_icli_functions.h
mesa_rc port_icli_debug_show_board_info(i32 session_id)
{
  port_isid_info_t info;
  int              i, type;
  mesa_port_no_t   iport;
  switch_iter_t    sit;

  VTSS_RC(switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID));
  while (switch_iter_getnext(&sit)) {
    if (port_isid_info_get(sit.isid, &info) != VTSS_OK) {
      continue;
    }

    if (sit.first) {
      icli_table_header(session_id, "USID  ISID  Board Type  Port Count  Port_0  Port_1");
    }

    type = vtss_board_type();
    ICLI_PRINTF("%-6u%-6u%-12u%-12u",
                sit.usid,
                sit.isid,
                type,
                info.port_count);

    for (i = 0; i < 2; i++) {
      iport = (i == 0 ? info.stack_port_0 : info.stack_port_1);

      if (iport == VTSS_PORT_NO_NONE) {
        ICLI_PRINTF("%-8s", "None");
      } else {
        ICLI_PRINTF("%-8u", iport2uport(iport));
      }
    }

    ICLI_PRINTF("\n");
  }

  return VTSS_RC_OK;
}

mesa_rc port_icli_debug_kr_poll(i32 session_id, BOOL ena, u32 poll)
{
  (void)misc_kr_thread_set(ena, poll);
  return VTSS_RC_OK;
}

static u32 tap_result(u32 value, u32 mask)
{
  if ((value & ~mask) > 0) {
    return ((~value) + 1) & mask;
  } else {
    return value;
  }
}

mesa_rc port_icli_debug_kr_status(i32 session_id, icli_stack_port_range_t *plist)
{
  port_iter_t   pit;
  mesa_port_10g_kr_status_t status;
  mesa_port_10g_kr_conf_t conf;

  VTSS_RC(icli_port_iter_init(&pit, 1, PORT_ITER_FLAGS_FRONT | PORT_ITER_FLAGS_NPI));
  while (icli_port_iter_getnext(&pit, plist)) {
    ICLI_PRINTF("Port %u\n", pit.uport);
    if (mesa_port_10g_kr_status_get(NULL, pit.iport, &status) != VTSS_RC_OK) {
      return VTSS_RC_ERROR;
    }
    if (mesa_port_10g_kr_conf_get(NULL, pit.iport, &conf) != VTSS_RC_OK) {
      return VTSS_RC_ERROR;
    }
    if (conf.aneg.enable) {
      ICLI_PRINTF("LP aneg ability                 :%s\n", status.aneg.lp_aneg_able ? "Yes" : "No");
      ICLI_PRINTF("Aneg active (running)           :%s\n", status.aneg.active ? "Yes" : "No");
      ICLI_PRINTF("PCS block lock                  :%s\n", status.aneg.block_lock ? "Yes" : "No");
      ICLI_PRINTF("Aneg complete                   :%s\n", status.aneg.complete ? "Yes" : "No");
      ICLI_PRINTF("  Enable 10G request            :%s\n", status.aneg.request_10g ? "Yes" : "No");
      ICLI_PRINTF("  Enable 1G request             :%s\n", status.aneg.request_1g ? "Yes" : "No");
      ICLI_PRINTF("  FEC change request            :%s\n", status.aneg.request_fec_change ? "Yes" : "No");
    } else {
      ICLI_PRINTF("Aneg                            :Disabled\n");
    }
    BOOL cm = (status.train.cm_ob_tap_result >> 6) > 0 ? 1 : 0;
    BOOL cp = (status.train.cp_ob_tap_result >> 6) > 0 ? 1 : 0;
    BOOL c0 = (status.train.c0_ob_tap_result >> 6) > 0 ? 1 : 0;
    if (conf.train.enable) {
      ICLI_PRINTF("Training complete (BER method)  :%s\n", status.train.complete ? "Yes" : "No");
      ICLI_PRINTF("  CM OB tap (7-bit signed)      :%s%d (%d)\n", cm ? "-" : "+", tap_result(status.train.cm_ob_tap_result, 0x3f), status.train.cm_ob_tap_result);
      ICLI_PRINTF("  CP OB tap (7-bit signed)      :%s%d (%d)\n", cp ? "-" : "+", tap_result(status.train.cp_ob_tap_result, 0x3f), status.train.cp_ob_tap_result);
      ICLI_PRINTF("  C0 OB tap (7-bit signed)      :%s%d (%d)\n", c0 ? "-" : "+", tap_result(status.train.c0_ob_tap_result, 0x3f), status.train.c0_ob_tap_result);
    } else {
      ICLI_PRINTF("Training                        :Disabled\n");
    }

    ICLI_PRINTF("FEC                             :%s\n", status.fec.enable ? "Enabled" : "Disabled");
    if (status.fec.enable) {
      ICLI_PRINTF("  Corrected block count         :%d\n", status.fec.corrected_block_cnt);
      ICLI_PRINTF("  Un-corrected block count      :%d\n", status.fec.uncorrected_block_cnt);
    }

  }
  return VTSS_RC_OK;
}

mesa_rc port_icli_debug_kr_conf(i32 session_id, icli_stack_port_range_t *plist, BOOL all, BOOL adv_1g, BOOL adv_10g, BOOL fec_abil, BOOL fec_req, BOOL train_only, BOOL aneg_only, BOOL aneg_off)
{
  port_iter_t   pit;
  mesa_port_10g_kr_conf_t conf;
  BOOL get_only = FALSE;

  if (!aneg_off && !all && !adv_1g && !adv_10g && !train_only) {
    get_only = TRUE;
  } else {
    conf.aneg.enable = aneg_off ? 0 : train_only ? 0 : 1;
    conf.train.enable = aneg_off ? 0 : aneg_only ? 0 : 1;
    conf.aneg.adv_10g = (adv_10g || all);
    conf.aneg.fec_abil = (fec_abil || all);
    conf.aneg.fec_req = (fec_req || all);
  }

  VTSS_RC(icli_port_iter_init(&pit, 1, PORT_ITER_FLAGS_FRONT | PORT_ITER_FLAGS_NPI));
  while (icli_port_iter_getnext(&pit, plist)) {
    if (get_only) {
      ICLI_PRINTF("Port %u\n", pit.uport);
      if (mesa_port_10g_kr_conf_get(NULL, pit.iport, &conf) != VTSS_RC_OK) {
        return VTSS_RC_ERROR;
      }
      ICLI_PRINTF("Aneg enabled              :%s\n", conf.aneg.enable   ? "Yes" : "No");
      ICLI_PRINTF("  Advertise 10G           :%s\n", conf.aneg.adv_10g  ? "Yes" : "No");
      ICLI_PRINTF("  Advertise FEC ability   :%s\n", conf.aneg.fec_abil ? "Yes" : "No");
      ICLI_PRINTF("  Advertise FEC request   :%s\n", conf.aneg.fec_req  ? "Yes" : "No");
      ICLI_PRINTF("Training enabled          :%s\n", conf.train.enable  ? "Yes" : "No");
    } else {
      (void)mesa_port_10g_kr_conf_set(NULL, pit.iport, &conf);
    }
  }

  // Start the poll thread
  (void)misc_kr_thread_set(conf.aneg.enable || conf.train.enable, 100);

  return VTSS_RC_OK;
}


mesa_rc port_icli_debug_kr_fec(i32 session_id, icli_stack_port_range_t *plist, BOOL ena)
{

  return VTSS_RC_OK;
}

mesa_rc port_icli_debug_loopback(i32 session_id, icli_stack_port_range_t *plist, mesa_port_lb_t lb, BOOL set)
{
  port_iter_t           pit;
  mesa_port_test_conf_t conf;
  BOOL                  first = TRUE;

  VTSS_RC(icli_port_iter_init(&pit, VTSS_ISID_START, PORT_ITER_FLAGS_ALL));
  while (icli_port_iter_getnext(&pit, plist)) {
    if (mesa_port_test_conf_get(NULL, pit.iport, &conf) != MESA_RC_OK) {
      /* Skip port */
    } else if (set) {
      conf.loopback = lb;
      (void)mesa_port_test_conf_set(NULL, pit.iport, &conf);
    } else {
      if (first) {
        first = FALSE;
        ICLI_PRINTF("Port  Loopback\n");
      }
      lb = conf.loopback;
      ICLI_PRINTF("%-6u%s\n",
                  pit.uport,
                  lb == MESA_PORT_LB_NEAR_END ? "Near-End" :
                  lb == MESA_PORT_LB_FAR_END ? "Far-End" :
                  lb == MESA_PORT_LB_FACILITY ? "Facility" :
                  lb == MESA_PORT_LB_EQUIPMENT ? "Equipment" : "Disabled");
    }
  }
  return MESA_RC_OK;
}

//  see port_icli_functions.h
mesa_rc port_icli_debug_show_capabilities(i32 session_id, icli_stack_port_range_t *plist)
{
  switch_iter_t sit;
  port_iter_t   pit;
  u32           i, m;

  vtss_appl_port_status_t port_status;
  VTSS_RC(switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID));
  while (icli_switch_iter_getnext(&sit, plist)) {
    VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT | PORT_ITER_FLAGS_NPI));

    while (icli_port_iter_getnext(&pit, plist)) {
      if (pit.first) {
        icli_table_header(session_id, "Port  CAP         Capabilities");
      }

      vtss_ifindex_t ifindex;
      VTSS_RC(vtss_ifindex_from_port(sit.isid, pit.iport, &ifindex));

      VTSS_RC(vtss_appl_port_status_get(ifindex, &port_status));
      ICLI_PRINTF("%-6u" VPRI64x "  ", pit.uport, port_status.cap);
      BOOL inset_dash = FALSE;
      for (i = 0; i < 32; i++) {
        m = (1 << i);
        if (port_status.cap & m) {
          ICLI_PRINTF("%s%s",
                      inset_dash ?  "-" : "",
                      m == MEBA_PORT_CAP_AUTONEG ? "ANEG" :
                      m == MEBA_PORT_CAP_10M_HDX ? "10MH" :
                      m == MEBA_PORT_CAP_10M_FDX ? "10MF" :
                      m == MEBA_PORT_CAP_100M_HDX ? "100MH" :
                      m == MEBA_PORT_CAP_100M_FDX ? "100MF" :
                      m == MEBA_PORT_CAP_1G_FDX ? "1GF" :
                      m == MEBA_PORT_CAP_2_5G_FDX ? "2.5GF" :
                      m == MEBA_PORT_CAP_5G_FDX ? "5GF" :
                      m == MEBA_PORT_CAP_10G_FDX ? "10GF" :
                      m == MEBA_PORT_CAP_FLOW_CTRL ? "FC" :
                      m == MEBA_PORT_CAP_COPPER ? "CU" :
                      m == MEBA_PORT_CAP_FIBER ? "FI" :
                      m == MEBA_PORT_CAP_DUAL_COPPER ? "DUAL_CU" :
                      m == MEBA_PORT_CAP_DUAL_FIBER ? "DUAL_FI" :
                      m == MEBA_PORT_CAP_SD_ENABLE ? "SD_ENA" :
                      m == MEBA_PORT_CAP_SD_HIGH ? "SD_HIGH" :
                      m == MEBA_PORT_CAP_SD_INTERNAL ? "SD_INT" :
                      m == MEBA_PORT_CAP_XAUI_LANE_FLIP ? "XAUI_FLIP" :
                      m == MEBA_PORT_CAP_VTSS_10G_PHY ? "10G_PHY" :
                      m == MEBA_PORT_CAP_SFP_DETECT ? "SFP_DET" :
                      m == MEBA_PORT_CAP_STACKING ? "STACK" :
                      m == MEBA_PORT_CAP_DUAL_SFP_DETECT ? "MEBA_PORT_CAP_DUAL_SFP_DETECT" :
                      m == MEBA_PORT_CAP_NO_FORCE ? "NO_FORCE" : "?");
          inset_dash = TRUE;
        }
      }
      ICLI_PRINTF("\n");
    }
  }

  return VTSS_RC_OK;
}
/***************************************************************************/
/* ICFG callback functions */
/****************************************************************************/
#ifdef VTSS_SW_OPTION_ICFG
static mesa_rc port_icfg_conf(const vtss_icfg_query_request_t *req, vtss_icfg_query_result_t *result)
{
  vtss_isid_t isid;
  mesa_port_no_t iport;
  BOOL is_default;
  vtss_icfg_conf_print_t conf_print;
  vtss_appl_port_media_t media;
  char buf[100];
  vtss_appl_port_descr_t ifc_desc;

  vtss_icfg_conf_print_init(&conf_print, req, result);

  T_NG(TRACE_GRP_ICLI, "mode:%d", req->cmd_mode);
  switch (req->cmd_mode) {
  case ICLI_CMD_MODE_GLOBAL_CONFIG:
    // Get current configuration for this switch
    break;
  case ICLI_CMD_MODE_INTERFACE_PORT_LIST:
    isid =  req->instance_id.port.isid;
    iport = req->instance_id.port.begin_iport;
    T_DG_PORT(TRACE_GRP_ICLI, iport, "Isid:%d, req->instance_id.port.usid:%d configurable:%d", isid, req->instance_id.port.usid, msg_switch_configurable(isid));

    if (msg_switch_configurable(isid)) {
      // Get current configuration for this switch
      vtss_appl_port_conf_t port_conf;
      vtss_ifindex_t ifindex;
      VTSS_RC(vtss_ifindex_from_port(isid, iport, &ifindex));
      VTSS_RC(vtss_appl_port_conf_get(ifindex, &port_conf));

      // Get default configuration
      vtss_appl_port_conf_t port_conf_def;
      VTSS_RC(vtss_port_conf_default(isid, iport, &port_conf_def));

      //
      // Media
      //
      meba_port_cap_t cap = port_isid_port_cap(isid, iport);
      BOOL media_type_is_default = FALSE;

      if (cap & (MEBA_PORT_CAP_DUAL_SFP_DETECT | MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER)) {
        if (port_conf.dual_media_fiber_speed == MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED) {
          // cu only
          media = VTSS_APPL_PORT_MEDIA_CU;
        } else if (port_conf.dual_media_fiber_speed == MESA_SPEED_FIBER_AUTO) {
          // Both SFP and cu (default)
          media = VTSS_APPL_PORT_MEDIA_DUAL;
          media_type_is_default = TRUE;
        } else {
          // sfp only
          media = VTSS_APPL_PORT_MEDIA_SFP;
        }

        // Print default if required
        if (req->all_defaults || !media_type_is_default) {
          switch (media) {
          case VTSS_APPL_PORT_MEDIA_CU:
            VTSS_RC(vtss_icfg_printf(result, " media-type rj45\n"));
            break;
          case VTSS_APPL_PORT_MEDIA_DUAL:
            VTSS_RC(vtss_icfg_printf(result, " media-type dual\n"));
            break;
          case VTSS_APPL_PORT_MEDIA_SFP:
            VTSS_RC(vtss_icfg_printf(result, " media-type sfp\n"));
            break;
          }
        }
      } else {
        // SFP or CU only ports - meaning always return default as it make no sense to not having a media interface
        if (cap & (MEBA_PORT_CAP_SFP_ONLY)) {
          media = VTSS_APPL_PORT_MEDIA_SFP;
        } else if (port_conf.speed == MESA_SPEED_AUTO && (port_conf.dual_media_fiber_speed == MESA_SPEED_FIBER_AUTO || port_conf.dual_media_fiber_speed == MESA_SPEED_FIBER_ONLY_AUTO)) {
          media = VTSS_APPL_PORT_MEDIA_DUAL;
        } else if (port_conf.dual_media_fiber_speed == MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED) {
          media = VTSS_APPL_PORT_MEDIA_CU;
        } else {
          if (port_conf.speed == MESA_SPEED_AUTO) { // Autonneg = TRUE, means that we are running AMS mode
            media = VTSS_APPL_PORT_MEDIA_DUAL;
          } else {
            media = VTSS_APPL_PORT_MEDIA_SFP;
          }
        }
      }

      T_DG_PORT(TRACE_GRP_ICLI, iport, "all:%d, speed:%d, default_speed:%d", req->all_defaults, port_conf.speed, port_conf_def.speed);

      //
      // Speed
      //
      if (port_conf_def.speed == MESA_SPEED_AUTO) {
        is_default = (port_conf.speed == port_conf_def.speed) && ((port_conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_SPEED) == (port_conf_def.adv_dis & VTSS_APPL_PORT_ADV_DIS_SPEED));
      } else {
        is_default = (port_conf.speed == port_conf_def.speed);
      }


      T_IG(TRACE_GRP_ICLI, "is_default:%d  speed:%d, def.speed:%d, adv_dis:0x%X, def.adv_dis:0x%X", is_default, port_conf.speed, port_conf_def.speed, port_conf.adv_dis, port_conf_def.adv_dis);
      // Auto negotiation
      if (req->all_defaults || !is_default) {
        VTSS_RC(vtss_icfg_printf(result, " speed "));

        T_IG_PORT(TRACE_GRP_ICLI, iport, "port_conf.adv_dis:0x%X", port_conf.adv_dis);

        switch (port_conf.speed) {
        case MESA_SPEED_UNDEFINED:
          T_E_PORT(iport, "%s", "Speed shall never be undefined");
          break;
        case MESA_SPEED_10M:
          VTSS_RC(vtss_icfg_printf(result, "10"));
          break;
        case MESA_SPEED_100M:
          VTSS_RC(vtss_icfg_printf(result, "100"));
          break;
        case MESA_SPEED_1G:
          VTSS_RC(vtss_icfg_printf(result, "1000"));
          break;
        case MESA_SPEED_2500M:
          VTSS_RC(vtss_icfg_printf(result, "2500"));
          break;
        case MESA_SPEED_5G:
          VTSS_RC(vtss_icfg_printf(result, "5g"));
          break;
        case MESA_SPEED_10G:
          VTSS_RC(vtss_icfg_printf(result, "10g"));
          break;
        case MESA_SPEED_12G:
          VTSS_RC(vtss_icfg_printf(result, "12g"));
          break;
        case MESA_SPEED_AUTO:
          VTSS_RC(vtss_icfg_printf(result, "auto"));

          // We do not show the auto-negotiation advertise speeds if they are default values.
          BOOL is_aneg_advertise_default = (port_conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_SPEED) == (port_conf_def.adv_dis & VTSS_APPL_PORT_ADV_DIS_SPEED);

          meba_port_cap_t cap = port_isid_port_cap(isid, iport);
          if (cap & MEBA_PORT_CAP_SFP_ONLY) {
            break;
          }

          if (req->all_defaults || !is_aneg_advertise_default) {
            if (req->instance_id.port.port_type == ICLI_PORT_TYPE_GIGABIT_ETHERNET ||
                req->instance_id.port.port_type == ICLI_PORT_TYPE_FAST_ETHERNET ||
                req->instance_id.port.port_type == ICLI_PORT_TYPE_2_5_GIGABIT_ETHERNET ||
                req->instance_id.port.port_type == ICLI_PORT_TYPE_TEN_GIGABIT_ETHERNET) {
              if (req->instance_id.port.port_type != ICLI_PORT_TYPE_TEN_GIGABIT_ETHERNET &&
                  req->instance_id.port.port_type != ICLI_PORT_TYPE_2_5_GIGABIT_ETHERNET &&
                  (port_conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_10M) == 0) {
                if (!is_aneg_advertise_default) {
                  VTSS_RC(vtss_icfg_printf(result, " 10"));
                }
              }

              if ((port_conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_100M) == 0) {
                if (!is_aneg_advertise_default) {
                  VTSS_RC(vtss_icfg_printf(result, " 100"));
                }
              }
            }

            if (req->instance_id.port.port_type == ICLI_PORT_TYPE_GIGABIT_ETHERNET ||
                req->instance_id.port.port_type == ICLI_PORT_TYPE_2_5_GIGABIT_ETHERNET ||
                req->instance_id.port.port_type == ICLI_PORT_TYPE_TEN_GIGABIT_ETHERNET) {
              if ((port_conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_1G) == 0) {
                if (!is_aneg_advertise_default) {
                  VTSS_RC(vtss_icfg_printf(result, " 1000"));
                }
              }
            }

            if ((port_conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_2500M) == 0) {
              if (!is_aneg_advertise_default) {
                VTSS_RC(vtss_icfg_printf(result, " 2500"));
              }
            }
            if ((port_conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_5G) == 0) {
              if (!is_aneg_advertise_default) {
                VTSS_RC(vtss_icfg_printf(result, " 5g"));
              }
            }
            if ((port_conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_10G) == 0) {
              if (!is_aneg_advertise_default) {
                VTSS_RC(vtss_icfg_printf(result, " 10g"));
              }
            }
          }
          break;
        }
        VTSS_RC(vtss_icfg_printf(result, "\n"));
      }

      //
      // Flow control
      //
      conf_print.is_default = port_conf.flow_control == port_conf_def.flow_control;
      VTSS_RC(vtss_icfg_conf_print(&conf_print, "flowcontrol", "%s", port_conf.flow_control ? "on" : "off"));

      //
      // PFC
      //
      if (MESA_CAP(MESA_CAP_PORT_PFC)) {
        conf_print.is_default = memcmp(port_conf.pfc, port_conf_def.pfc, sizeof(port_conf.pfc)) == 0;
        VTSS_RC(vtss_icfg_conf_print(&conf_print, "priority-flowcontrol prio", "%s", prio_range_to_str(port_conf.pfc, buf)));
      }

      //
      // MTU
      //
      conf_print.is_default = port_conf.max_length == port_conf_def.max_length;
      VTSS_RC(vtss_icfg_conf_print(&conf_print, "mtu", "%d", port_conf.max_length));

      //
      // DUPLEX
      //
      port_conf_value_t duplex;
      port_icli_duplex_get(isid, iport, &port_conf, &duplex);

      if (port_conf_def.speed == MESA_SPEED_AUTO) {
        conf_print.is_default = port_conf.speed == port_conf_def.speed && ((port_conf.adv_dis & VTSS_APPL_PORT_ADV_DIS_DUPLEX) == (port_conf_def.adv_dis & VTSS_APPL_PORT_ADV_DIS_DUPLEX));
      } else {
        conf_print.is_default = port_conf.fdx == port_conf_def.fdx;
      }

      T_DG_PORT(TRACE_GRP_ICLI, iport, "conf_print.is_default:%d, port_conf.fdx:%d, port_conf_def.fdx:%d, port_conf.adv_dis:0x%X, port_conf_def.adv_dis:0x%X", conf_print.is_default, port_conf.fdx, port_conf_def.fdx, port_conf.adv_dis, port_conf_def.adv_dis);
      VTSS_RC(vtss_icfg_conf_print(&conf_print, "duplex", "%s",
                                   duplex.duplex.has_auto ? duplex.duplex.has_advertise_fdx ? "auto full" : "auto half" :
                                   duplex.duplex.has_full ? "full" : duplex.duplex.has_half ? "half" :
                                   duplex.duplex.has_advertise_fdx ? "auto full" : "auto half"));

      //
      // Interface Description
      //
      VTSS_RC(vtss_ifindex_from_port(isid, iport, &ifindex));
      VTSS_RC(vtss_appl_ifc_desc_get(ifindex, &ifc_desc));
      conf_print.is_default = strcmp(ifc_desc.description, "") == 0 ? 1 : 0;
      VTSS_RC(vtss_icfg_conf_print(&conf_print, "description", "%s", ifc_desc.description));

      //
      // Excessive Restart
      //
      conf_print.is_default = port_conf.exc_col_cont == port_conf_def.exc_col_cont;
      VTSS_RC(vtss_icfg_conf_print(&conf_print, "excessive-restart", "%s", ""));

      //
      // Frame length check
      //
      conf_print.is_default = port_conf.frame_length_chk == port_conf_def.frame_length_chk;
      VTSS_RC(vtss_icfg_conf_print(&conf_print, "frame-length-check", "%s", ""));

      //
      // Shut down
      //
      conf_print.is_default = port_conf.admin.enable == port_conf_def.admin.enable;
      VTSS_RC(vtss_icfg_conf_print(&conf_print, "shutdown", "%s", ""));

    } // End msg_switch_configurable()
    break;

  default:
    //Not needed
    break;
  }

  return VTSS_RC_OK;
}

/* ICFG Initialization function */
mesa_rc port_icfg_init(void)
{
  VTSS_RC(vtss_icfg_query_register(VTSS_ICFG_PORT_GLOBAL_CONF, "port", port_icfg_conf));
  VTSS_RC(vtss_icfg_query_register(VTSS_ICFG_PORT_INTERFACE_CONF, "port", port_icfg_conf));
  return VTSS_RC_OK;
}
#endif // VTSS_SW_OPTION_ICFG

#ifdef __cplusplus
}
#endif

/****************************************************************************/
/*  End of file.                                                            */
/****************************************************************************/
