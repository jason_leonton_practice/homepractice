/* Switch API software.

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

/**
 * \file
 * \brief Port icli functions
 * \details This header file describes port control functions
 */

#ifndef VTSS_ICLI_PORT_H
#define VTSS_ICLI_PORT_H

#include "icli_api.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief Function for setting LED power reduction on event configuration
 *
 * \param session_id      [IN] Needed for being able to print error messages
 * \param port_list       [IN] Port list with ports to show.
 * \param has_packets     [IN] TRUE to only show statistics for packets.
 * \param has_bytes       [IN] TRUE to only show statistics for bytes.
 * \param has_errors      [IN] TRUE to only show statistics for errors.
 * \param has_discards    [IN] TRUE to only show statistics for discards.
 * \param has_filtered    [IN] TRUE to only show statistics for filtered.
 * \param has_dot3br      [IN] TRUE to only show statistics for dot3br.
 * \param has_priority    [IN] TRUE to only show statistics for priority.
 * \param priority_list   [IN] Priority list with priorities to show.
 * \param has_up          [IN] TRUE to only show statistics for ports with link up
 * \param has_down        [IN] TRUE to only show statistics for ports with link down
 * \return Error code.
 **/
mesa_rc port_icli_statistics(i32 session_id, icli_stack_port_range_t *port_list, BOOL has_packets, BOOL has_bytes, BOOL has_errors,
                             BOOL has_discards, BOOL has_filtered, BOOL has_dot3br, BOOL has_priority, icli_unsigned_range_t *priority_list,
                             BOOL has_up, BOOL has_down);

/**
 * \brief Function for setting maximum frames size
 *
 * \param session_id [IN]  For printing error messages.
 * \param max_length [IN]  Maximum frame size
 * \param plist [IN]  Port list with ports to configure.
 * \param no [IN]  TRUE to set the maximum frame size to default
 * \return Error code.
 **/
mesa_rc port_icli_mtu(i32 session_id, u16 max_length, icli_stack_port_range_t *plist, BOOL no);

/**
 * \brief Function for enabling/disabling a port.
 *
 * \param session_id [IN]  For printing error messages.
 * \param plist [IN]  Port list with ports to configure.
 * \param no [IN]  TRUE to enable port. FALSE to disable port.
 * \return Error code.
 **/
mesa_rc port_icli_shutdown(i32 session_id, icli_stack_port_range_t *plist, BOOL no);

/**
 * \brief Function for configuring port speed
 *
 * \param session_id [IN]  For printing error messages.
 * \param plist [IN]  Port list with ports to configure.
 * \param has_speed_10g [IN]  TRUE to set port in 10G mode.
 * \param has_speed_2g5 [IN]  TRUE to set port in 2.5G mode.
 * \param has_speed_1g [IN]  TRUE to set port in 1G mode.
 * \param has_speed_100 [IN]  TRUE to set port in 100M mode.
 * \param has_speed_10 [IN]  TRUE to set port in 10M mode.
 * \param has_speed_auto [IN]  TRUE to set port in auto-neg mode.
 * \param has_neg_10 [IN]  TRUE to enable 10M auto-neg advertise
 * \param has_neg_100 [IN]  TRUE to enable 100M auto-neg advertise
 * \param has_neg_1000 [IN]  TRUE to enable 1G auto-neg advertise
 * \param no [IN]  TRUE to set speed to default.
 * \return Error code.
 **/
mesa_rc port_icli_speed(i32 session_id, icli_stack_port_range_t *plist,  BOOL has_speed_10g,
                        BOOL has_speed_2g5, BOOL has_speed_1g, BOOL has_speed_100m, BOOL has_speed_10m,
                        BOOL has_speed_auto, BOOL has_neg_10, BOOL has_neg_100, BOOL has_neg_1000,
                        BOOL has_neg_2500, BOOL has_neg_5g, BOOL has_neg_10g, BOOL no);

/**
 * \brief Function for clearing port statistics
 *
 * \param session_id [IN]  For printing .
 * \param plist [IN]  Port list with ports to clear.
 * \return Error code.
 **/
mesa_rc port_icli_statistics_clear(i32 session_id, icli_stack_port_range_t *port_list);

/**
 * \brief Function for showing port capabilities
 *
 * \param session_id [IN]  For printing .
 * \param plist [IN]  Port list with ports to show.
 * \return Error code.
 **/
mesa_rc port_icli_capabilities(i32 session_id, icli_stack_port_range_t *port_list);

/**
 * \brief Function for showing port status
 *
 * \param session_id [IN]  For printing .
 * \param plist [IN]  Port list with ports to show.
 * \param has_errdisable [IN]  Print only port status for port that are in errdisabled mode.
  * \return Error code.
 **/
mesa_rc port_icli_status(i32 session_id, icli_stack_port_range_t *port_list, BOOL has_errdisable);

/**
 * \brief Function for configuring media type.
 *
 * \param session_id [IN]  For printing .
 * \param plist [IN]  Port list with ports to configure.
 * \param has_rj45 [IN]  TRUE if port supports rj45 (cu port).
 * \param has_sfp [IN]  TRUE if port supports sfp (fiber port).
 * \param no [IN]  TRUE to set media type to default.
 * \return Error code.
 **/
mesa_rc port_icli_media_type(i32 session_id, icli_stack_port_range_t *plist, BOOL has_rj45, BOOL has_sfp, BOOL no);

/**
 * \brief Function for configuring duplex
 *
 * \param session_id [IN]  For printing .
 * \param plist [IN]  Port list with ports to configure.
 * \param has_half [IN]  TRUE if port duplex mode to forced half duplex.
 * \param has_full [IN]  TRUE if port duplex mode to forced full duplex.
 * \param has_auto [IN]  TRUE if port duplex mode to auto negotiation.
 * \param has_advertise_hdx [IN]  TRUE to advertise half duplex in auto negotiation mode
 * \param has_advertise_fdx [IN]  TRUE to advertise full duplex in auto negotiation mode
 * \param no [IN]  TRUE to set duplex to default.
 * \return Error code.
 **/
mesa_rc port_icli_duplex(i32 session_id, icli_stack_port_range_t *plist, BOOL has_half, BOOL has_full, BOOL has_auto, BOOL has_advertise_hdx, BOOL has_advertise_fdx, BOOL no);

/**
 * \brief Function for configuring flow control
 *
 * \param session_id [IN]  For printing .
 * \param plist [IN]  Port list with ports to configure.
 * \param has_on [IN]  TRUE to enable the rx/tx flow control.
 * \param has_off [IN]  TRUE to disable the rx/tx flow control.
 * \param no [IN]  TRUE to set media type to default.
 * \return Error code.
 **/
mesa_rc port_icli_flow_control(i32 session_id, icli_stack_port_range_t *plist, BOOL has_on, BOOL has_off, BOOL no);

/**
 * \brief Function for configuring flow control
 *
 * \param session_id [IN]  For printing .
 * \param plist [IN]  Port list with ports to configure.
 * \param pfc [IN]  TRUE for each prio to enable or disable.
 * \param no [IN]  TRUE to set prio flowcontrol to default.
 * \return Error code.
 **/
mesa_rc port_icli_pfc(i32 session_id, icli_stack_port_range_t *plist, BOOL *pfc, BOOL no);

/**
 * \brief Function for configuring excessive collisions
 *
 * \param session_id [IN]  For printing .
 * \param plist [IN]  Port list with ports to configure.
 * \param no [IN]  TRUE to set media type to default.
 * \return Error code.
 **/

mesa_rc port_icli_excessive_restart(i32 session_id, icli_stack_port_range_t *plist, BOOL no);

/**
 * \brief Function for running and showing VeriPHY
 *
 * \param session_id [IN]  For printing .
 * \param plist      [IN]  Port list with ports to configure.
 * \param show       [IN]  Set to TRUE in order to show veriphy result. Set to FALSE to start new diagnostics
 * \return Error code.
 **/
mesa_rc port_icli_veriphy(i32 session_id, icli_stack_port_range_t *port_list, BOOL show);

/**
 * \brief Init function ICLI CFG
 * \return Error code.
 **/
mesa_rc port_icfg_init(void);

/**
 * \brief Function for at runtime getting information about 802.3br Frame Preemption
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param ask [IN]  Asking
 * \param runtime [IN]  Pointer to where to put the "answer"
 * \return TRUE if runtime contains valid information.
 **/
BOOL port_icli_runtime_qos_dot3br(u32                session_id,
                                  icli_runtime_ask_t ask,
                                  icli_runtime_t     *runtime);

/**
 * \brief Function for at runtime getting information about QOS queues
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param ask [IN]  Asking
 * \param runtime [IN]  Pointer to where to put the "answer"
 * \return TRUE if runtime contains valid information.
 **/
BOOL port_icli_runtime_qos_queues(u32                session_id,
                                  icli_runtime_ask_t ask,
                                  icli_runtime_t     *runtime);

/**
 * \brief Function for at runtime getting information if the interface(s) supports 10G
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param ask        [IN] Asking
 * \param runtime    [IN] Pointer to where to put the "answer"
 * \return TRUE if runtime contains valid information.
 **/
BOOL port_icli_runtime_10g(u32                session_id,
                           icli_runtime_ask_t ask,
                           icli_runtime_t     *runtime);

/**
 * \brief Function for at runtime getting information if the interface(s) supports 10M
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param ask        [IN] Asking
 * \param runtime    [IN] Pointer to where to put the "answer"
 * \return TRUE if runtime contains valid information.
 **/
BOOL port_icli_runtime_10M(u32                session_id,
                           icli_runtime_ask_t ask,
                           icli_runtime_t     *runtime);

/**
 * \brief Function for at runtime getting information if the interface(s) supports 100M
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param ask        [IN] Asking
 * \param runtime    [IN] Pointer to where to put the "answer"
 * \return TRUE if runtime contains valid information.
 **/
BOOL port_icli_runtime_100M(u32                session_id,
                            icli_runtime_ask_t ask,
                            icli_runtime_t     *runtime);

/**
 * \brief Function for at runtime getting information if the interface(s) supports auto negotiation of speed 10M
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param ask        [IN] Asking
 * \param runtime    [IN] Pointer to where to put the "answer"
 * \return TRUE if runtime contains valid information.
 **/
BOOL port_icli_runtime_advertise_auto_speed_10M(u32                session_id,
                                                icli_runtime_ask_t ask,
                                                icli_runtime_t     *runtime);

/**
 * \brief Function for at runtime getting information if the interface(s) supports auto negotiation of speed 100M
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param ask        [IN] Asking
 * \param runtime    [IN] Pointer to where to put the "answer"
 * \return TRUE if runtime contains valid information.
 **/
BOOL port_icli_runtime_advertise_auto_speed_100M(u32                session_id,
                                                 icli_runtime_ask_t ask,
                                                 icli_runtime_t     *runtime);

/**
 * \brief Function for at runtime getting information if the interface(s) supports auto negotiation of speed 1G
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param ask        [IN] Asking
 * \param runtime    [IN] Pointer to where to put the "answer"
 * \return TRUE if runtime contains valid information.
 **/
BOOL port_icli_runtime_advertise_auto_speed_1G(u32                session_id,
                                               icli_runtime_ask_t ask,
                                               icli_runtime_t     *runtime);

/**
 * \brief Function for at runtime getting information if the interface(s) supports auto negotiation of speed 2.5G
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param ask        [IN] Asking
 * \param runtime    [IN] Pointer to where to put the "answer"
 * \return TRUE if runtime contains valid information.
 **/
BOOL port_icli_runtime_advertise_auto_speed_2G5(u32                session_id,
                                                icli_runtime_ask_t ask,
                                                icli_runtime_t     *runtime);
BOOL port_icli_runtime_advertise_auto_speed_5G(u32                session_id,
                                               icli_runtime_ask_t ask,
                                               icli_runtime_t     *runtime);
BOOL port_icli_runtime_advertise_auto_speed_10G(u32                session_id,
                                                icli_runtime_ask_t ask,
                                                icli_runtime_t     *runtime);

/**
 * \brief Function for at runtime getting information if the stack supports 2.5G
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param ask        [IN] Asking
 * \param runtime    [IN] Pointer to where to put the "answer"
 * \return TRUE if runtime contains valid information.
 **/
BOOL port_icli_runtime_2g5(u32                session_id,
                           icli_runtime_ask_t ask,
                           icli_runtime_t     *runtime);

/**
 * \Function for setting Interface description
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param plist      [IN] Port list with ports to show capabilities for.
 * \param descr      [IN] Description of the interface.
 * \param enable     [IN] TRUE to enable, FALSE to disable.
 * \param show       [IN] TRUE to Display description of interface.
 *
 * \return error code
 **/
mesa_rc port_icli_description(i32 session_id, icli_stack_port_range_t *plist, const char *descr, BOOL enable, BOOL show);

/**
 * \brief Function for at runtime getting information if the stack supports 1G
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param ask        [IN] Asking
 * \param runtime    [IN] Pointer to where to put the "answer"
 * \return TRUE if runtime contains valid information.
 **/
BOOL port_icli_runtime_1g(u32                session_id,
                          icli_runtime_ask_t ask,
                          icli_runtime_t     *runtime);

/**
 * \brief Function for at runtime getting information if the port supports force half duplex mode
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param ask        [IN] Asking
 * \param runtime    [IN] Pointer to where to put the "answer"
 * \return TRUE if runtime contains valid information.
 **/
BOOL port_icli_runtime_force_duplex_half(u32                session_id,
                                         icli_runtime_ask_t ask,
                                         icli_runtime_t     *runtime);
/**
 * \brief Function for at runtime getting information if the port supports force full duplex mode
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param ask        [IN] Asking
 * \param runtime    [IN] Pointer to where to put the "answer"
 * \return TRUE if runtime contains valid information.
 **/
BOOL port_icli_runtime_force_duplex_full(u32                session_id,
                                         icli_runtime_ask_t ask,
                                         icli_runtime_t     *runtime);


/**
 * \brief Function for show the port capabilities
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param plist [IN]  Port list with ports to show capabilities for.
 * \return TRUE if capabilities was shown else error code
 **/
mesa_rc port_icli_debug_show_capabilities(i32 session_id, icli_stack_port_range_t *plist);

/**
 * \brief Debug function for show/clear the port change events
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param plist      [IN] Port list with ports to show capabilities for.
 * \param has_up     [IN] TRUE to only show/clear change events for ports with link up
 * \param has_down   [IN] TRUE to only show/clear change events for ports with link up
 *
 * \return error code
 **/
mesa_rc port_icli_debug_change_event(i32 session_id, icli_stack_port_range_t *plist, BOOL has_up, BOOL has_down, BOOL clear);

/**
 * \brief Debug function for show board information
 *
 * \return error code
 **/
mesa_rc port_icli_debug_show_board_info(i32 session_id);


/**
 * \brief Debug function for show/clear port mode registrations.
 *
 * \return error code
 **/
mesa_rc port_icli_debug_show_registrations(i32 session_id, BOOL clear);

/**
 * \brief Function for setting frame length check directly through the API
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param plist      [IN] Port list with ports to show capabilities for.
 * \param chk        [IN] TRUE to enable, FALSE to disable
 *
 * \return error code
 **/
mesa_rc port_icli_frm_len_chk(i32 session_id, icli_stack_port_range_t *plist, BOOL chk);

/**
 * \brief Debug function for get 10G-KR status from the API
 **/
mesa_rc port_icli_debug_kr_status(i32 session_id, icli_stack_port_range_t *plist);

/**
 * \brief Debug function for set 10G-KR configuration
 **/
mesa_rc port_icli_debug_kr_conf(i32 session_id, icli_stack_port_range_t *plist, BOOL all, BOOL adv_1g,
                                BOOL adv_10g, BOOL fec_abil, BOOL fec_req, BOOL train_only, BOOL aneg_only, BOOL aneg_off);
/**
 * \brief Debug function for set 10G-KR FEC
 **/
mesa_rc port_icli_debug_kr_fec(i32 session_id, icli_stack_port_range_t *plist, BOOL ena);

/**
 * \brief Debug function for configure the 10G-KR Thread
 **/
mesa_rc port_icli_debug_kr_poll(i32 session_id, BOOL ena, u32 poll);

mesa_rc port_icli_debug_loopback(i32 session_id, icli_stack_port_range_t *plist, mesa_port_lb_t lb, BOOL set);

#ifdef __cplusplus
}
#endif

#endif /* VTSS_ICLI_PORT_H */



/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
