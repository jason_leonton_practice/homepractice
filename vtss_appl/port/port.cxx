/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include <vtss/basics/vector.hxx>
#include <vtss/basics/utility.hxx>
#include <fcntl.h>

#include <sys/stat.h>
#include "mscc/ethernet/switch/api.h"
#include "main.h"
#include "misc_api.h"
#include "vtss_api_if_api.h"
#include "conf_api.h"
#include "port_api.h"
#include "board_if.h"
#include "msg_api.h"
#include "critd_api.h"
#include "port.h"
#include "control_api.h"
#include "interrupt_api.h"
#ifdef VTSS_SW_OPTION_PHY
#include "phy_api.h"
#endif
#ifdef VTSS_SW_OPTION_SYSLOG
#include "syslog_api.h"
#endif
#include "topo_api.h"
#ifdef VTSS_SW_OPTION_POE
#include "poe_api.h"
#endif
#ifdef VTSS_SW_OPTION_MEP
#include "vtss/appl/mep.h"
#endif
#ifdef VTSS_SW_OPTION_ICFG
#include "port_icli_functions.h" // For port_icfg_init
#if defined(VTSS_SW_OPTION_PORT_POWER_SAVINGS)
#include "port_power_savings_icli_functions.h"  /* For port_power_savings_icfg_init() */
#endif
#endif

#include "port_trace.h"
#include "vtss/basics/trace.hxx"
#include "vtss_os_wrapper.h"
#include "vtss/appl/port.h"
#include "vtss/basics/expose/table-status.hxx" // For vtss::expose::TableStatus
#include "vtss/basics/memcmp-operator.hxx"  // For VTSS_BASICS_MEMCMP_OPERATOR




/****************************************************************************/
/*  Global variables                                                        */
/****************************************************************************/

#if defined(VTSS_SW_OPTION_PRIVATE_MIB) || defined(VTSS_SW_OPTION_JSON_RPC)
extern const vtss_enum_descriptor_t vtss_sfp_transceiver_txt[];
#endif

/* Structure for global variables */
static port_global_t port;
#define PHY_INTERRUPT 0x01
static vtss_flag_t    interrupt_wait_flag;
static mesa_port_list_t fast_link_int;    /* this is indicating that a fast link failure interrupt has been received */
CapArray<vtss_appl_port_descr_t, MESA_CAP_PORT_CNT> port_desc; /* Interface Description */
CapArray<BOOL, MESA_CAP_PORT_CNT> first_link_up; /*Required for JR2 PTP reload problem*/

/* Port configuration change flags */
#define PORT_CONF_CHANGE_NONE                   0x00000000
#define PORT_CONF_CHANGE_PHY                    0x00000001
#define PORT_CONF_CHANGE_MAC                    0x00000002
#define PORT_CONF_CHANGE_FIBER                  0x00000004
#define PORT_CONF_CHANGE_PHY_SGMII              0x00000008
#define PORT_CONF_CHANGE_SPEED_OR_MEDIA_TYPE    0x00000010
#define PORT_CONF_CHANGE_ALL                    0XFFFFFFFF

#define SFP_MSA_BASE_PX          0x80
#define SFP_MSA_BASE_BX10        0x40
#define SFP_MSA_100BASE_FX       0x20
#define SFP_MSA_100BASE_LX       0x10
#define SFP_MSA_1000BASE_T       0x08
#define SFP_MSA_1000BASE_CX      0x04
#define SFP_MSA_1000BASE_LX      0x02
#define SFP_MSA_1000BASE_SX      0x01
#define SFP_MSA_10GBASE_ER       0x80
#define SFP_MSA_10GBASE_LRM      0x40
#define SFP_MSA_10GBASE_LR       0x20
#define SFP_MSA_10GBASE_SR       0x10
#define SFP_MSA_SFP_PLUS_PASSIVE 0x04
#define SFP_MSA_SFP_PLUS_ACTIVE  0x08

/* AQR PHY's firmware file */
#define AQR_24_FW               "/etc/mscc/phy/firmware/aqr_24.bin"
#define AQR_4_FW               "/etc/mscc/phy/firmware/aqr_4.bin"


static BOOL is_port_10g_copper_phy(mesa_port_no_t port_no);

/* Determine if the port is dual media port with auto-detected mode */
#define PORT_IS_DUAL_MEDIA_AMS(x) ((port_custom_table[(x)].cap & MEBA_PORT_CAP_SPEED_DUAL_ANY_FIBER) == MEBA_PORT_CAP_SPEED_DUAL_ANY_FIBER)

/* We have two different hardware layout for dual media ports.
 * 1. One is the switch chip connect to a PHY first and the PHY connect to RJ45 and SFP interfaces.
 *    For example, Serval-1 Ref. board (PCB106)
 *    The media ports on PCB106 use the SGMII to connect a VSC8574(Quad PHY) first and then 4x1000Base-T and 4x1G Serdes.
 *            VSC7418(Switch chip)
 *                  |
 *            VSC8574(Quad PHY)
 *                  |
 *    +---------------------------+
 *    |                           |
 *   4x1000Base-T(RJ45)       4x1G Serdes(SFP)
 *
 * 2. The other is the switch chip internal PHY connect to RJ45 and SFP interfaces.
 *    For example, Serval-T Ref. board (PCB116)
 *    The media port on PCB116 uses S/W to switch 2x10000Base-T and 2x1G Serdes.
 *            VSC7437(Switch chip with internal PHY)
 *                  |
 *    +---------------------------+
 *    |                           |
 *   2x1000Base-T(RJ45)       2x1G Serdes(SFP)
 *
 * The macro PORT_PHY_IS_INT_AMS() is only suitable for case 2: Serval-T Ref. board (PCB116)
 * The Port software modules needs to switch the media port MAC interface correctly
 * according the current port configuration and real connected situation.
 *
 * Notes:
 * 1. If the dual media port is connected with an internal PHY, the port MAC
 *    interface should based on the software configration. For example,
 *    1.a Use "MESA_PORT_INTERFACE_SGMII" for copper mode.
 *    1.b Use "MESA_PORT_INTERFACE_SERDES" for 1G fiber mode.
 * 2. If the dual media port is connected with an external PHY, the port MAC
 *    interface always equals "MESA_PORT_INTERFACE_SGMII" and use mesa_phy_media_interface_t
 *    (See phy_reset->media_if in phy_fiber_media()) to set the speed on API layer.
 */
#define PORT_PHY_IS_INT_AMS(x) (PORT_IS_DUAL_MEDIA_AMS(x) && (port_custom_table[(x)].cap & MEBA_PORT_CAP_INT_PHY))
#define PORT_PHY_IS_EXT_AMS(x) (PORT_IS_DUAL_MEDIA_AMS(x) && !PORT_PHY_IS_INT_AMS(x))

#if (VTSS_TRACE_ENABLED)
static vtss_trace_reg_t trace_reg =
{
    VTSS_TRACE_MODULE_ID, "port", "Port module."
};

static vtss_trace_grp_t trace_grps[TRACE_GRP_CNT] =
{
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_ITER */ {
        "iter",
        "Iterations ",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CRIT */ {
        "crit",
        "Critical regions ",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_SFP */ {
        "sfp",
        "SFP",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_ICLI */ {
        "iCLI",
        "ICLI",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CONF */ {
        "conf",
        "Configuration",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_MIB */ {
        "mib",
        "MIB",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_AMS */ {
        "ams",
        "AMS",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_LINK_FLAP_DETECT */ {
        "link_flap_det",
        "Link Flap Detection",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
};

#define PORT_CRIT_ENTER()    critd_enter(&port.crit,    TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define PORT_CRIT_EXIT()     critd_exit( &port.crit,    TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define PORT_CB_CRIT_ENTER() critd_enter(&port.cb_crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define PORT_CB_CRIT_EXIT()  critd_exit( &port.cb_crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#else
#define PORT_CRIT_ENTER()    critd_enter(&port.crit)
#define PORT_CRIT_EXIT()     critd_exit( &port.crit)
#define PORT_CB_CRIT_ENTER() critd_enter(&port.cb_crit)
#define PORT_CB_CRIT_EXIT()  critd_exit( &port.cb_crit)
#endif /* VTSS_TRACE_ENABLED */


//
// Define the Entry names
//
vtss_enum_descriptor_t vtss_port_speed_txt[] {
    {VTSS_APPL_PORT_SPEED_10M_FDX,  "force10ModeFdx"},
    {VTSS_APPL_PORT_SPEED_10M_HDX,  "force10ModeHdx"},
    {VTSS_APPL_PORT_SPEED_100M_FDX, "force100ModeFdx"},
    {VTSS_APPL_PORT_SPEED_100M_HDX, "force100ModeHdx"},
    {VTSS_APPL_PORT_SPEED_1G_FDX,   "force1GModeFdx"},
    {VTSS_APPL_PORT_SPEED_AUTO,     "autoNegMode"},
    {VTSS_APPL_PORT_SPEED_2500M_FDX,"force2G5ModeFdx"},
    {VTSS_APPL_PORT_SPEED_5G_FDX,   "force5GModeFdx"},
    {VTSS_APPL_PORT_SPEED_10G_FDX,  "force10GModeFdx"},
    {VTSS_APPL_PORT_SPEED_12G_FDX,  "force12GModeFdx"},
    {0, 0},
};

vtss_enum_descriptor_t vtss_port_phy_veriphy_status_txt[] {
    {MESA_VERIPHY_STATUS_OK,      "correctlyTerminatedPair"},
    {MESA_VERIPHY_STATUS_OPEN,    "openPair"},
    {MESA_VERIPHY_STATUS_SHORT,   "shortPair"},
    {MESA_VERIPHY_STATUS_ABNORM,  "abnormalTermination"},
    {MESA_VERIPHY_STATUS_SHORT_A, "crossPairShortToPairA"},
    {MESA_VERIPHY_STATUS_SHORT_B, "crossPairShortToPairB"},
    {MESA_VERIPHY_STATUS_SHORT_C, "crossPairShortToPairC"},
    {MESA_VERIPHY_STATUS_SHORT_D, "crossPairShortToPairD"},
    {MESA_VERIPHY_STATUS_COUPL_A, "abnormalCrossPairCouplingToPairA"},
    {MESA_VERIPHY_STATUS_COUPL_B, "abnormalCrossPairCouplingToPairB"},
    {MESA_VERIPHY_STATUS_COUPL_C, "abnormalCrossPairCouplingToPairC"},
    {MESA_VERIPHY_STATUS_COUPL_D, "abnormalCrossPairCouplingToPairD"},
    {MESA_VERIPHY_STATUS_UNKNOWN, "unknownResult"},
    {MESA_VERIPHY_STATUS_RUNNING, "veriPhyRunning"},
    {0, 0},
};

vtss_enum_descriptor_t vtss_port_status_speed_txt[] {
    {MESA_SPEED_UNDEFINED, "undefined"},
    {MESA_SPEED_10M,       "speed10M"},
    {MESA_SPEED_100M,      "speed100M"},
    {MESA_SPEED_1G,        "speed1G"},
    {MESA_SPEED_2500M,     "speed2G5"},
    {MESA_SPEED_5G,        "speed5G" },
    {MESA_SPEED_10G,       "speed10G"},
    {MESA_SPEED_12G,       "speed12G"},
    {0, 0},
};

vtss_enum_descriptor_t vtss_port_fc_txt[] {
    {VTSS_APPL_PORT_FC_OFF, "off"},
    {VTSS_APPL_PORT_FC_ON,  "on"},
    {0, 0},
};

vtss_enum_descriptor_t vtss_port_media_txt[] {
    {VTSS_APPL_PORT_MEDIA_CU,   "rj45"},
    {VTSS_APPL_PORT_MEDIA_SFP,  "sfp"},
    {VTSS_APPL_PORT_MEDIA_DUAL, "dual"},
    {0, 0},
};

/* JSON notifications  */
VTSS_BASICS_MEMCMP_OPERATOR(vtss_appl_port_mib_status_t);
vtss::expose::TableStatus<
    vtss::expose::ParamKey<vtss_ifindex_t>,
    vtss::expose::ParamVal<vtss_appl_port_mib_status_t *>
> port_status_update;

/****************************************************************************/
/*                                                                          */
/*  MODULE INTERNAL FUNCTIONS - board dependent functions                   */
/*                                                                          */
/****************************************************************************/

/****************************************************************************/
/*  Various local functions                                                 */
/****************************************************************************/

static BOOL port_sfp_sgmii_set(mesa_port_no_t port_no, BOOL *phy_present);

BOOL is_base_t(mesa_port_no_t port_no) {
  return (port.status[VTSS_ISID_LOCAL][port_no].sfp.type == VTSS_APPL_PORT_SFP_1000BASE_T ||
          port.status[VTSS_ISID_LOCAL][port_no].sfp.type == VTSS_APPL_PORT_SFP_100BASE_T);
}
/* Determine if port has a PHY */
BOOL is_port_phy(mesa_port_no_t port_no)
{
    if (PORT_PHY_IS_INT_AMS(port_no)) {
        return port.mac_sfp_if[port_no] == MESA_PORT_INTERFACE_SGMII ? TRUE : FALSE;
    }
    return (port_custom_table[port_no].mac_if == MESA_PORT_INTERFACE_SGMII ||
            port_custom_table[port_no].mac_if == MESA_PORT_INTERFACE_QSGMII ||
            port_custom_table[port_no].mac_if == MESA_PORT_INTERFACE_RGMII ? TRUE : FALSE);

}

// Returns if a port is PHY (if the PHY is in pass through mode, it shall act as it is not a PHY)
BOOL port_phy(mesa_port_no_t port_no)
{
    if (PORT_PHY_IS_INT_AMS(port_no)) {
        return is_port_phy(port_no);
    }
    return ((is_port_phy(port_no) && !is_base_t(port_no)) || is_port_10g_copper_phy(port_no));
}

// Used to skip ports, which has no capabilities when looping through all ports.
#define PORT_HAS_CAP(isid, port_no)  if (port_isid_port_cap(isid, port_no) == 0) {continue;}

meba_port_cap_t port_isid_port_cap(vtss_isid_t isid, mesa_port_no_t port_no)
{
    T_RG_PORT(TRACE_GRP_CONF, port_no, "port.cap_valid[isid][port_no]:%d, port.status[isid][port_no].cap:0x%X, port.isid_added[isid]:%d, vtss_board_port_cap(port):0x%X",
              port.cap_valid[isid][port_no], port.status[isid][port_no].cap, port.isid_added[isid], vtss_board_port_cap(port_no));
    return (port.cap_valid[isid][port_no] ? port.status[isid][port_no].cap :
            port.isid_added[isid] ? vtss_board_port_cap(port_no) : 0);
}

static BOOL port_isid_phy(vtss_isid_t isid, mesa_port_no_t port_no)
{
    return (port.status[isid][port_no].cap & MEBA_PORT_CAP_1G_PHY ? 1 : 0);
}

BOOL port_10g_phy(mesa_port_no_t port_no)
{
    return (port_custom_table[port_no].cap & MEBA_PORT_CAP_10G_FDX ? 1 : 0);
}

// See port_api.h
BOOL is_port_capability_stack_supported(meba_port_cap_t cap) {
  switch_iter_t   sit;
  port_iter_t pit;
  (void)switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_USID_CFG);

  while (switch_iter_getnext(&sit)) {
      // Loop through all ports
      if (port_iter_init(&pit, NULL, sit.isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_STACK  | PORT_ITER_FLAGS_NORMAL | PORT_ITER_FLAGS_NPI) != VTSS_RC_OK) {
          T_E("iter_init error");
      }

      while (port_iter_getnext(&pit)) {
          meba_port_cap_t port_cap = port_isid_port_cap(sit.isid, pit.iport);
          T_D("port_cap:" VPRI64x ", port:%d, iport:%d, cap:" VPRI64x ", bool:%d", port_cap, pit.uport, pit.iport, cap, !!(port_cap & cap));
          if (port_cap & cap) {
              T_I("port_cap:" VPRI64x ", port:%d, iport:%d, cap:" VPRI64x ", bool:%d", port_cap, pit.uport, pit.iport, cap, !!(port_cap & cap));
              return TRUE;
          }
      }
  }
  T_I("FALSE");
  return FALSE;
}

const char *mac_if2txt(mesa_port_interface_t if_type)
{
    switch (if_type) {
    case MESA_PORT_INTERFACE_NO_CONNECTION: return "NO_CONNECTION";
    case MESA_PORT_INTERFACE_LOOPBACK:      return "LOOPBACK";
    case MESA_PORT_INTERFACE_INTERNAL:      return "INTERNAL";
    case MESA_PORT_INTERFACE_MII:           return "MII";
    case MESA_PORT_INTERFACE_GMII:          return "GMII";
    case MESA_PORT_INTERFACE_RGMII:         return "RGMII";
    case MESA_PORT_INTERFACE_TBI:           return "TBI";
    case MESA_PORT_INTERFACE_RTBI:          return "RTBI";
    case MESA_PORT_INTERFACE_SGMII:         return "SGMII";
    case MESA_PORT_INTERFACE_SGMII_CISCO:   return "SGMII_CISCO";
    case MESA_PORT_INTERFACE_SERDES:        return "SERDES";
    case MESA_PORT_INTERFACE_VAUI:          return "VAUI";
    case MESA_PORT_INTERFACE_100FX:         return "100FX";
    case MESA_PORT_INTERFACE_XAUI:          return "XAUI";
    case MESA_PORT_INTERFACE_RXAUI:         return "RXAUI";
    case MESA_PORT_INTERFACE_XGMII:         return "XGMII";
    case MESA_PORT_INTERFACE_SPI4:          return "SPI4";
    case MESA_PORT_INTERFACE_QSGMII:        return "QSGMII";
    case MESA_PORT_INTERFACE_SFI:           return "SFI";
    default:                                return "???";
    }
}

/* Determine port MAC interface */
static mesa_port_interface_t port_mac_interface(mesa_port_no_t port_no)
{
    mesa_port_interface_t if_type = port_custom_table[port_no].mac_if;

    T_NG_PORT(TRACE_GRP_SFP, port_no, "mac_if: %s", mac_if2txt(if_type));
    return if_type;
}

/* Determine  MAC interface to SFP */
static mesa_port_interface_t port_mac_sfp_interface(mesa_port_no_t port_no)
{
    mesa_port_interface_t if_type = port.mac_sfp_if[port_no];

    T_NG_PORT(TRACE_GRP_SFP, port_no, "mac_sfp_if: %s", mac_if2txt(if_type));
    return if_type;
}

static mesa_rc phy_10g_speed_setup(mesa_port_no_t port_no, mesa_port_speed_t speed, BOOL autoneg, BOOL fc, BOOL ena)
{
    mesa_phy_10g_mode_t mode;
    BOOL skip_1g_setup = 0, force_setup_10g = 0;
    mesa_phy_10g_clause_37_control_t ctrl;
    mesa_phy_10g_power_t power;

    if (mesa_phy_10g_mode_get(PHY_INST, port_no, &mode) != VTSS_RC_OK) {
        T_E("mesa_phy_10g_mode_get failed");
        return VTSS_RC_ERROR;
    }

    if (mesa_phy_10g_clause_37_control_get(PHY_INST, port_no, &ctrl) != VTSS_RC_OK) {
        T_E("mesa_phy_10g_mode_get failed");
        return VTSS_RC_ERROR;
    }

    if (mesa_phy_10g_power_get(PHY_INST, port_no, &power) != VTSS_RC_OK) {
        T_E("mesa_phy_10g_power_get failed");
    }

    if ((ena && power != MESA_PHY_10G_POWER_ENABLE) || (!ena && power == MESA_PHY_10G_POWER_ENABLE)) {
       power = ena ? MESA_PHY_10G_POWER_ENABLE : MESA_PHY_10G_POWER_DISABLE;
        if (mesa_phy_10g_power_set(PHY_INST, port_no, &power) != VTSS_RC_OK) {
            T_E("mesa_phy_10g_power_set failed");
        }
        force_setup_10g = (power == MESA_PHY_10G_POWER_ENABLE) ? 1 : 0; // if the power has been down the we must initilize the Phy again.
    }

    if (!force_setup_10g) {
        if ((mode.oper_mode == MESA_PHY_1G_MODE && speed == MESA_SPEED_1G && ctrl.enable == autoneg && ctrl.advertisement.symmetric_pause == fc)
            || (mode.oper_mode == MESA_PHY_LAN_MODE && speed == MESA_SPEED_10G)) {
            return VTSS_RC_OK; /* Nothing to do */
        }
    }

    if (speed == MESA_SPEED_1G) {
        if (mode.oper_mode == MESA_PHY_1G_MODE) {
            skip_1g_setup = 1;;
        } else {
            mode.oper_mode = MESA_PHY_1G_MODE;
        }
        mode.xaui_lane_flip = 1;  /* Need to flip the lanes to match JR XAUI-lane-0 and 8487 XAUI-lane-0  */
    } else {
        mode.oper_mode = MESA_PHY_LAN_MODE;
        mode.xaui_lane_flip = 0;
    }

    if (!skip_1g_setup) {
        if (mesa_phy_10g_mode_set(PHY_INST, port_no, &mode) != VTSS_RC_OK) {
            T_E("mesa_phy_10g_mode_set failed");
            return VTSS_RC_ERROR;
        }
    }

    if (speed == MESA_SPEED_1G) {
        memset(&ctrl, 0, sizeof(mesa_phy_10g_clause_37_control_t));
        ctrl.enable = autoneg;
        ctrl.advertisement.fdx = 1;
        ctrl.advertisement.symmetric_pause = fc;
        ctrl.advertisement.asymmetric_pause = fc;
        ctrl.advertisement.remote_fault = (ena ? MESA_PHY_10G_CLAUSE_37_RF_LINK_OK : MESA_PHY_10G_CLAUSE_37_RF_OFFLINE);
        ctrl.l_h = TRUE;
        if (mesa_phy_10g_clause_37_control_set(PHY_INST, port_no, &ctrl)) {
            T_E("mesa_phy_10g_clause_37_control_set");
            return VTSS_RC_ERROR;
        }
    }
    return VTSS_RC_OK;
}

static mesa_sd10g_media_type_t sfp2media(vtss_appl_sfp_tranceiver_t sfp)
{
    switch (sfp) {
    case VTSS_APPL_PORT_SFP_10G:    return(MESA_SD10G_MEDIA_PR_NONE);
    case VTSS_APPL_PORT_SFP_10G_SR: return(MESA_SD10G_MEDIA_SR);
    case VTSS_APPL_PORT_SFP_10G_LR: return(MESA_SD10G_MEDIA_ZR);
    case VTSS_APPL_PORT_SFP_10G_LRM:return(MESA_SD10G_MEDIA_SR);
    case VTSS_APPL_PORT_SFP_10G_ER: return(MESA_SD10G_MEDIA_ZR);
    default:
    return(MESA_SD10G_MEDIA_DAC);
    }
}

// Function for determine the auto neg speed and flow control configuration
// In : port_no - port in question
// In/Out : phy - Pointer to where to put the phy setup.
static void port_phy_setup_aneg_get(mesa_port_no_t port_no, mesa_phy_conf_t *phy) {
    vtss_appl_port_conf_t *conf = &port.config[VTSS_ISID_LOCAL][port_no];
    meba_port_cap_t cap = port_custom_table[port_no].cap;

    /* Auto negotiation */
    phy->mode = MESA_PHY_MODE_ANEG;
    phy->aneg.speed_2g5_fdx = conf->speed == MESA_SPEED_AUTO && (cap & MEBA_PORT_CAP_2_5G_FDX) && !(conf->adv_dis & VTSS_APPL_PORT_ADV_DIS_2500M);
    phy->aneg.speed_5g_fdx  = conf->speed == MESA_SPEED_AUTO && (cap & MEBA_PORT_CAP_5G_FDX)   && !(conf->adv_dis & VTSS_APPL_PORT_ADV_DIS_5G);
    phy->aneg.speed_10g_fdx = conf->speed == MESA_SPEED_AUTO && (cap & MEBA_PORT_CAP_10G_FDX)  && !(conf->adv_dis & VTSS_APPL_PORT_ADV_DIS_10G);
    phy->aneg.speed_10m_hdx = conf->speed == MESA_SPEED_AUTO && (cap & MEBA_PORT_CAP_10M_HDX) &&
        !(conf->adv_dis & VTSS_APPL_PORT_ADV_DIS_HDX) && !(conf->adv_dis & VTSS_APPL_PORT_ADV_DIS_10M);
    phy->aneg.speed_10m_fdx = conf->speed == MESA_SPEED_AUTO && (cap & MEBA_PORT_CAP_10M_FDX) &&
        !(conf->adv_dis & VTSS_APPL_PORT_ADV_DIS_FDX) && !(conf->adv_dis & VTSS_APPL_PORT_ADV_DIS_10M);
    phy->aneg.speed_100m_hdx = conf->speed == MESA_SPEED_AUTO && (cap & MEBA_PORT_CAP_100M_HDX) &&
                               !(conf->adv_dis & VTSS_APPL_PORT_ADV_DIS_HDX) && !(conf->adv_dis & VTSS_APPL_PORT_ADV_DIS_100M);
    phy->aneg.speed_100m_fdx = conf->speed == MESA_SPEED_AUTO && (cap & MEBA_PORT_CAP_100M_FDX) &&
                               !(conf->adv_dis & VTSS_APPL_PORT_ADV_DIS_FDX) && !(conf->adv_dis & VTSS_APPL_PORT_ADV_DIS_100M);
    phy->aneg.speed_1g_fdx = ((cap & MEBA_PORT_CAP_1G_FDX) &&
                              !(conf->speed == MESA_SPEED_AUTO &&
                                (conf->adv_dis & VTSS_APPL_PORT_ADV_DIS_1G)));
    phy->aneg.speed_1g_hdx = FALSE; // We don't support 1G half duplex
    phy->aneg.symmetric_pause  = conf->flow_control;
    phy->aneg.asymmetric_pause = conf->flow_control;
}















typedef enum {
    // Apply to API layer according to the current port configuration
    PORT_API_APPLY_NO_CHG,

    // Apply to API layer with the highest speed of the port capability
    PORT_API_APPLY_PORT_CAP,

    // Apply to API layer with the SFP module capability and consider with port capabilities
    PORT_API_APPLY_PORT_AND_MODULE_CAP,

    // Apply to API layer with 1G auto mode
    PORT_API_APPLY_AUTO_1G
} port_api_apply_type_t;

/* Adjust the port configuraiton based on the current situations
 * (combine with port configured spped and module speed)
 * Because we allow all posiable speed configraution on SFP interface
 * before module inserted.
 */
static void port_sfp_speed_adjust(mesa_port_no_t port_no, vtss_appl_port_conf_t *conf)
{
    port_api_apply_type_t   api_apply_type = PORT_API_APPLY_NO_CHG;
    mesa_port_interface_t   sfp_mac_if = port_mac_sfp_interface(port_no);
    meba_port_cap_t         cap = port_custom_table[port_no].cap;
    vtss_appl_sfp_t         *sfp = &port.status[VTSS_ISID_LOCAL][port_no].sfp;

    /* BZs 22457/22111
     *
     * When a port supports the Serdes interface (pure SFP port or combo port),
     * we allow all speed configuration in the application layer and the lower
     * layer will be applied according the following rules.
     *
     * A. Combo port:
     *    A.1 - Copper port: PORT_API_APPLY_NO_CHG
     *    A.2 - SFP port: the same as B (pure SFP port)
     *
     * B. Pure SFP port:
     *    B.1 - Copper SFP module: PORT_API_APPLY_AUTO_1G
     *    B.2 - DAC cable: PORT_API_APPLY_NO_CHG
     *    B.3 - Fiber module:
     *      B.3.1 - When port configured speed is 'AUTO 1G mode': PORT_API_APPLY_AUTO_1G
     *      B.3.2 - When port configured speed is force mode:
     *        B.3.2.a - When the SFP module capability matched the port capabilities: PORT_API_APPLY_PORT_AND_MODULE_CAP
     *        B.3.2.b - When the SFP module capability unmatched the port capabilities(e.g., 10G SFP module inserted  on 1G port): PORT_API_APPLY_PORT_CAP
     */
    if ((port_phy(port_no) && !PORT_PHY_IS_EXT_AMS(port_no)) /* Pure SFP port */
        || !sfp->status.present                              /* SFP moudle must presented */
       ) {
        return;
    }

    T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "SFP module present");

    if (sfp_mac_if == MESA_PORT_INTERFACE_SGMII_CISCO ||
        (PORT_PHY_IS_EXT_AMS(port_no) && sfp->type == VTSS_APPL_PORT_SFP_1000BASE_T)) {
        /* B.1 - Copper SFP module: PORT_API_APPLY_AUTO_1G */
        /* BZs 22457/22829/22974:
         *
         * For the Copper SFP module, we allow user to set forced mode configuration
         * but always set auto-neg mode on chip API.
         */
        T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "Found Copper SFP module");
        api_apply_type = PORT_API_APPLY_AUTO_1G;
    } else if (((conf->speed == MESA_SPEED_AUTO)  && (cap & MEBA_PORT_CAP_1G_FDX))   ||
               ((conf->speed == MESA_SPEED_10M)   && (cap & MEBA_PORT_CAP_10M_FDX))  ||
               ((conf->speed == MESA_SPEED_100M)  && (cap & MEBA_PORT_CAP_100M_FDX)) ||
               ((conf->speed == MESA_SPEED_1G)    && (cap & MEBA_PORT_CAP_1G_FDX))   ||
               ((conf->speed == MESA_SPEED_2500M) && (cap & MEBA_PORT_CAP_2_5G_FDX)) ||
               ((conf->speed == MESA_SPEED_5G)    && (cap & MEBA_PORT_CAP_5G_FDX))   ||
               ((conf->speed == MESA_SPEED_10G)   && (cap & MEBA_PORT_CAP_10G_FDX))) {
        /* B.2 - DAC cable: PORT_API_APPLY_NO_CHG */
        api_apply_type = PORT_API_APPLY_NO_CHG;
        T_DG_PORT(TRACE_GRP_SFP, port_no, "Follow the port interface capability, %s", (conf->speed == MESA_SPEED_AUTO) ? "1G with ANEG" : "Force mode");
    } else {
        /* B.3 - Fiber module:
         *      B.3.1 - When port configured speed is 'AUTO 1G mode': PORT_API_APPLY_AUTO_1G
         *      B.3.2 - When port configured speed is force mode:
         *          B.3.2.a - When the SFP module capability matched the port capabilities: PORT_API_APPLY_PORT_AND_MODULE_CAP
         *          B.3.2.b - When the SFP module capability unmatched the port capabilities(e.g., 10G SFP module inserted on 1G port): PORT_API_APPLY_PORT_CAP
         */
        BOOL is_1g_sfp   = sfp->type == VTSS_APPL_PORT_SFP_1000BASE_BX10 ||
                           sfp->type == VTSS_APPL_PORT_SFP_1000BASE_T ||
                           sfp->type == VTSS_APPL_PORT_SFP_1000BASE_CX ||
                           sfp->type == VTSS_APPL_PORT_SFP_1000BASE_SX ||
                           sfp->type == VTSS_APPL_PORT_SFP_1000BASE_LX ||
                           sfp->type == VTSS_APPL_PORT_SFP_1000BASE_X;
        api_apply_type = (conf->speed == MESA_SPEED_AUTO && is_1g_sfp) ? PORT_API_APPLY_AUTO_1G : PORT_API_APPLY_PORT_AND_MODULE_CAP;
        T_DG_PORT(TRACE_GRP_SFP, port_no, "Follow the SFP module capability, %s", (conf->speed == MESA_SPEED_AUTO && is_1g_sfp) ? "1G with ANEG" : "Force mode");
    }

    T_DG_PORT(TRACE_GRP_SFP, port_no, "Before: api_apply_type=%d", api_apply_type);
    switch(api_apply_type) {
        case PORT_API_APPLY_AUTO_1G:
            // Apply to API layer with auto mode
            conf->speed = MESA_SPEED_AUTO;
            break;
        case PORT_API_APPLY_PORT_AND_MODULE_CAP: {
            // Apply to API layer with the highest speed of the SFP module capability
            BOOL is_10g_sfp  = (cap & MEBA_PORT_CAP_10G_FDX) ?
                                (sfp->type == VTSS_APPL_PORT_SFP_10G ||
                                 sfp->type == VTSS_APPL_PORT_SFP_10G_SR ||
                                 sfp->type == VTSS_APPL_PORT_SFP_10G_LR ||
                                 sfp->type == VTSS_APPL_PORT_SFP_10G_LRM ||
                                 sfp->type == VTSS_APPL_PORT_SFP_10G_ER ||
                                 sfp->type == VTSS_APPL_PORT_SFP_10G_DAC) : FALSE;
            BOOL is_5g_sfp   = (cap & MEBA_PORT_CAP_5G_FDX) ?
                                (sfp->type == VTSS_APPL_PORT_SFP_5G) : FALSE;
            BOOL is_2_5g_sfp = (cap & MEBA_PORT_CAP_2_5G_FDX) ?
                                (sfp->type == VTSS_APPL_PORT_SFP_2G5) : FALSE;
            BOOL is_1g_sfp   = (cap & MEBA_PORT_CAP_1G_FDX) ?
                                (sfp->type == VTSS_APPL_PORT_SFP_1000BASE_BX10 ||
                                 sfp->type == VTSS_APPL_PORT_SFP_1000BASE_T ||
                                 sfp->type == VTSS_APPL_PORT_SFP_1000BASE_CX ||
                                 sfp->type == VTSS_APPL_PORT_SFP_1000BASE_SX ||
                                 sfp->type == VTSS_APPL_PORT_SFP_1000BASE_LX ||
                                 sfp->type == VTSS_APPL_PORT_SFP_1000BASE_X) : FALSE;
            BOOL is_100m_sfp = (cap & MEBA_PORT_CAP_100M_FDX) ?
                                (sfp->type == VTSS_APPL_PORT_SFP_100FX ||
                                 sfp->type == VTSS_APPL_PORT_SFP_100BASE_LX ||
                                 sfp->type == VTSS_APPL_PORT_SFP_100BASE_ZX ||
                                 sfp->type == VTSS_APPL_PORT_SFP_100BASE_BX10 ||
                                 sfp->type == VTSS_APPL_PORT_SFP_100BASE_T) : FALSE;
            if (is_10g_sfp || is_5g_sfp || is_2_5g_sfp || is_1g_sfp || is_100m_sfp) {
                conf->speed = is_10g_sfp  ? MESA_SPEED_10G :
                              is_5g_sfp   ? MESA_SPEED_5G :
                              is_2_5g_sfp ? MESA_SPEED_2500M :
                              is_1g_sfp   ? MESA_SPEED_1G :MESA_SPEED_100M;
                T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", conf->speed == MESA_SPEED_AUTO ? "AUTO_1G" : vtss_port_status_speed_txt[conf->speed].valueName);
                break;
            } else {
                T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "Cannot find a proprietary SFP module capability");
                /* Notice!!! Don't break the switch case in this condition
                 *
                 * If we cannot find a proprietary SFP module capability, use case
                 *  'PORT_API_APPLY_PORT_CAP' to continue the process (as the default setting).
                 *
                 * B.3.2 - When port configured speed is force mode:
                 *      B.3.2.b - When the SFP module capability unmatched the port capabilities(e.g., 10G SFP module inserted on 1G port): PORT_API_APPLY_PORT_CAP
                 */
            }
        }
        case PORT_API_APPLY_PORT_CAP:
            // Apply to API layer with the highest speed of the port capability
            conf->speed = (cap & MEBA_PORT_CAP_10G_FDX)  ? MESA_SPEED_10G :
                          (cap & MEBA_PORT_CAP_5G_FDX)   ? MESA_SPEED_5G :
                          (cap & MEBA_PORT_CAP_2_5G_FDX) ? MESA_SPEED_2500M :
                          (cap & MEBA_PORT_CAP_1G_FDX)   ? MESA_SPEED_1G :
                          (cap & MEBA_PORT_CAP_100M_FDX) ? MESA_SPEED_100M : MESA_SPEED_10M;
            T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", conf->speed == MESA_SPEED_AUTO ? "AUTO_1G" : vtss_port_status_speed_txt[conf->speed].valueName);
            break;
        case PORT_API_APPLY_NO_CHG:
        default:
            T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "Apply to API layer according the current port configuration");
            // Apply to API layer according the current port configuration
            break;
    }
    T_DG_PORT(TRACE_GRP_SFP, port_no, "After: %s", conf->speed == MESA_SPEED_AUTO ? "AUTO_1G" : vtss_port_status_speed_txt[conf->speed].valueName);
}

static BOOL sfp_mac_if_is_10g_mode(mesa_port_interface_t sfp_mac_if)
{
    return (sfp_mac_if == MESA_PORT_INTERFACE_SFI ||
            sfp_mac_if == MESA_PORT_INTERFACE_XAUI ||
            sfp_mac_if == MESA_PORT_INTERFACE_RXAUI) ? TRUE : FALSE;
}

static BOOL port_is_in_10g_mode(mesa_port_no_t port_no)
{
    mesa_port_interface_t sfp_mac_if = port_mac_sfp_interface(port_no);
    return (sfp_mac_if == MESA_PORT_INTERFACE_SFI ||
            sfp_mac_if == MESA_PORT_INTERFACE_XAUI ||
            sfp_mac_if == MESA_PORT_INTERFACE_RXAUI) ? TRUE : FALSE;
}

static mesa_rc port_admin_state_set(meba_inst_t inst,
                                    mesa_port_no_t port_no,
                                    const meba_port_admin_state_t *state)
{
    mesa_rc rc;

    vtss_appl_api_lock();   //Protect SGIO conf
    rc = meba_port_admin_state_set(inst, port_no, state);
    vtss_appl_api_unlock();
    VTSS_MSLEEP(50); // Wait for the new setting completed

    return rc;
}

static mesa_rc port_setup(mesa_port_no_t port_no, u32 change)
{
    mesa_phy_conf_t                 phy_setup, *phy = NULL;
    mesa_port_conf_t                port_setup, *ps;
    mesa_port_clause_37_control_t   control;
    mesa_port_clause_37_adv_t       *adv;
    mesa_rc                         rc = VTSS_OK, rc2;
    int                             j;
    vtss_appl_port_conf_t           port_conf, *conf = &port_conf;
    vtss_appl_port_status_t         *port_status = &port.status[VTSS_ISID_LOCAL][port_no];
    mesa_port_status_t              *status = &port_status->status;
    vtss_appl_sfp_t                 *sfp = &port_status->sfp;
    meba_port_cap_t                 cap = port_custom_table[port_no].cap; // The original capabilities
    port_shutdown_reg_t             *reg;
    vtss_tick_count_t               ticks;
    ulong                           msec;
    BOOL                            fc, pcs_force = 0, dummy;
    uint32_t                        mep_loop_port = MESA_CAP(VTSS_APPL_CAP_MEP_LOOP_PORT);

    T_R("enter, port_no: %u, change:%d", port_no, change);

    // Adjust port sfp speed configuration
    // Copy one for local changing and don't impact on the original database
    port_conf = port.config[VTSS_ISID_LOCAL][port_no];
    T_DG_PORT(TRACE_GRP_SFP, port_no, "port_phy = %s, PORT_PHY_IS_INT_AMS() = %s, sfp->status.present = %d, sfp->status.los = %d",
              port_phy(port_no) ? "T" : "F",
              PORT_PHY_IS_INT_AMS(port_no) ? "T" : "F",
              sfp->status.present,
              sfp->status.los);

    if ((!port_phy(port_no) /* Pure SFP port or dual media port(which is connected with internal PHY) under fiber mode */
         || (PORT_PHY_IS_EXT_AMS(port_no) && (conf->dual_media_fiber_speed != MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED)) /* Dual media port(which is connected with external PHY) under fiber mode */)
        && sfp->status.present /* SFP module must be present */
       ) {
        port_sfp_speed_adjust(port_no, conf);
    }

    /* Flow control always disabled for stack ports */
    fc = conf->flow_control;

    T_D_PORT(port_no, "mac_if:%s, change:%d, speed:%d, mac_sfpif:%s",
             mac_if2txt(port_status->mac_if), change, conf->speed, mac_if2txt(port_mac_sfp_interface(port_no)));

    if (change & PORT_CONF_CHANGE_PHY) {
        /* Configure port */
        if (conf->admin.enable == 0 && status->link) {
            /* Do port shutdown callbacks */
            PORT_CRIT_EXIT(); /* Exit main region before entering CB region */
            PORT_CB_CRIT_ENTER();
            for (j = 0; j < port.shutdown_table.count; j++) {
                reg = &port.shutdown_table.reg[j];
                T_D("callback, j: %d (%s), port_no: %u",
                    j, vtss_module_names[reg->module_id], port_no);
                ticks = vtss_current_time();

                /* Leave critical region before callback */
                reg->callback(port_no);

                ticks = (vtss_current_time() - ticks);
                if (ticks > reg->max_ticks)
                    reg->max_ticks = ticks;
                msec = VTSS_OS_TICK2MSEC(ticks);
                T_D("callback done, j: %d (%s), port_no: %u, " VPRIlu" msec",
                    j, vtss_module_names[reg->module_id], port_no, msec);
            }
            PORT_CB_CRIT_EXIT();
            PORT_CRIT_ENTER();
        }
        if (is_port_phy(port_no) || is_port_10g_copper_phy(port_no)) {
            phy = &phy_setup;
            memset(phy, 0, sizeof(phy_setup)); // Initialize whole struct
            phy->mdi = MESA_PHY_MDIX_AUTO; // always enable auto detection of crossed/non-crossed cables
            T_IG_PORT(TRACE_GRP_CONF, port_no, "dual_spd:%d, spd:%d, enable:%d, adv_dis=0x%x",
                      conf->dual_media_fiber_speed, conf->speed, conf->admin.enable, conf->adv_dis);
            if (conf->admin.enable) {
                if ((cap & MEBA_PORT_CAP_AUTONEG) &&
                    (conf->speed == MESA_SPEED_AUTO || conf->speed == MESA_SPEED_1G ||
                     conf->dual_media_fiber_speed == MESA_SPEED_FIBER_100FX ||
                     conf->dual_media_fiber_speed == MESA_SPEED_FIBER_1000X)) {
                    /* Auto negotiation */
                    port_phy_setup_aneg_get(port_no, phy);
                } else {
                    /* Forced CU mode */
                    phy->mode = MESA_PHY_MODE_FORCED;
                    phy->forced.speed = conf->speed;
                    phy->forced.fdx = conf->fdx;
                }
            } else {
                /* Power down */
                phy->mode = MESA_PHY_MODE_POWER_DOWN;
            }
            if (phy->mode != MESA_PHY_MODE_POWER_DOWN) {
                if ((rc = mesa_phy_conf_set(PHY_INST, port_no, phy)) != VTSS_OK) {
                    T_E("mesa_phy_conf_set failed, port_no: %u", port_no);
                }
            }
            T_IG_PORT(TRACE_GRP_CONF, port_no, "mode:%d", phy->mode);
#ifdef VTSS_SW_OPTION_PHY_POWER_CONTROL
            {
                mesa_phy_power_conf_t power;
                power.mode = conf->power_mode;
                T_IG_PORT(TRACE_GRP_CONF, port_no, "power.mode:%d", power.mode);
                if ((rc2 = mesa_phy_power_conf_set(PHY_INST, port_no, &power)) != VTSS_OK) {
                    T_E("mesa_phy_power_conf_set failed, port_no: %u", port_no);
                    rc = rc2;
                }
            }
        }
#endif /* VTSS_SW_OPTION_PHY_POWER_CONTROL */
    }

    if (change & PORT_CONF_CHANGE_MAC) {
        memset(&control, 0, sizeof(control));
        if (conf->admin.enable &&
            (cap & MEBA_PORT_CAP_AUTONEG) &&
            (port_mac_sfp_interface(port_no) == MESA_PORT_INTERFACE_SERDES ||
             port_mac_sfp_interface(port_no) == MESA_PORT_INTERFACE_VAUI ||
             port_is_in_10g_mode(port_no))) {
            T_NG_PORT(TRACE_GRP_SFP, port_no, "%s", "Clause 37 setup");
            /* PCS auto negotiation */
            pcs_force = 1;
            control.enable = conf->speed == MESA_SPEED_AUTO ? TRUE : FALSE;
            adv = &control.advertisement;
            adv->fdx = 1;
            adv->hdx = 0;
            adv->symmetric_pause = fc;
            adv->asymmetric_pause = fc;
            adv->remote_fault = (conf->admin.enable ? MESA_PORT_CLAUSE_37_RF_LINK_OK :
                                 MESA_PORT_CLAUSE_37_RF_OFFLINE);
            adv->acknowledge = 0;
            adv->next_page = 0;

            mesa_port_clause_37_control_t   ctrl_now;
            (void)mesa_port_clause_37_control_get(NULL, port_no, &ctrl_now);
            if (memcmp(&control, &ctrl_now, sizeof(control)) != 0) {
                if ((rc2 = mesa_port_clause_37_control_set(NULL, port_no, &control)) != VTSS_OK) {
                    T_E("mesa_port_clause_37_control_set failed, port_no: %u", port_no);
                    rc = rc2;
                }
            }
        } else if ((cap & MEBA_PORT_CAP_AUTONEG) &&
                   (conf->speed == MESA_SPEED_2500M || conf->speed == MESA_SPEED_10G) &&
                   (port_mac_sfp_interface(port_no) == MESA_PORT_INTERFACE_VAUI ||
                    port_is_in_10g_mode(port_no))) {
            // Disable clause 37 aneg for 2.5/10G ports
            control.enable = FALSE;
            if ((rc2 = mesa_port_clause_37_control_set(NULL, port_no, &control)) != VTSS_OK) {
                T_E("mesa_port_clause_37_control_set failed, port_no: %u", port_no);
                rc = rc2;
            }
        }
    }

    if (change & PORT_CONF_CHANGE_SPEED_OR_MEDIA_TYPE) {
        /* BZ#23291:
         * Remote port auto-negotiation doesn't restart after the local port
         * speed is changed from auto-negotiation to forced speed mode.
         *
         * For example,
         * 1. The local switch(DUT-1) connected with remote switch(DUT-2) and
         *    both ports are link-up under auto 1G mode.
         *    +-----------------+     +----------------+
         *    | DUT-1           |     |       DUT-2    |
         *    |  Port-1(Auto-1G)|<--->|Port-2(Auto-1G) |
         *    +-----------------+     +----------------+
         * 2. Now, after chaged the DUT-1 port speed from 'auto 1G' mode to
         *    1G forced speed mode.
         *
         * If we only disable the auto-negotiation on local side. The remote
         * switch(DUT-2) will still in link-up state since the change doesn't
         * be notified on the remote side. Which is the unexpected result.
         * The remote port should be in link-down state due to the auto-negotiation
         * is failed after local port speed change.
         *
         * Solution:
         * Use the hardware "tx-disable" to notify the remote side.
         */
        meba_port_admin_state_t new_admin = conf->admin;
        new_admin.enable = FALSE;
        (void) port_admin_state_set(board_instance, port_no, &new_admin);

        /* BZ#23176, 23244:
         * Observed the API error message when switching the different speed on SFP port.
         *
         * In normal case, it is fine that the parameter value of 'ps->speed' will
         * refer to the current link speed when the port is link up under auto-negotiation.
         * But there is an exception when changing the port speed configuration
         * under link-up state, the action will cause an API tracing error which is
         * came from vrfy_spd_iface().
         *
         * Solution:
         * The parameter of 'ps->speed' should not refer to to the current link
         * speed in such exception condition.
         */
        status->link = FALSE;
    }

    /* Port setup */
    ps = &port_setup;
    memset(ps, 0, sizeof(*ps));

    // ps->if_type(mesa_port_interface_t) should refer to vtss_api\mesa\include\mscc\ethernet\switch\api\types.h
    ps->if_type = port_phy(port_no) ? port_mac_interface(port_no) : port_mac_sfp_interface(port_no);
    T_DG_PORT(TRACE_GRP_SFP, port_no, "Initial state: port_phy() = %s, ps->if_type = %s", port_phy(port_no) ? "T" : "F", mac_if2txt(ps->if_type));
    if (port.phy_id_1g[port_no].part_number == MESA_PHY_TYPE_AQR_407
        || port.phy_id_1g[port_no].part_number == MESA_PHY_TYPE_AQR_409
        || port.phy_id_1g[port_no].part_number == MESA_PHY_TYPE_AQR_411
        || port.phy_id_1g[port_no].part_number == MESA_PHY_TYPE_AQR_412) {
        if (status->speed == MESA_SPEED_100M && port.phy_id_1g[port_no].part_number == MESA_PHY_TYPE_AQR_409) {
            /* According to AQR_G2_v3_2_3_AQCS_GS_2626QX_K2_MDIOAddrAdj_24Ports_8_1F_3210_ID16100_VER495_cld,
               100M mode operates on SGMII ANEG. We don't update port.status[][].mac_if because
               port module will use this information to determine it should get status via mesa_phy_status_get()
               or mesa_port_status_get(). For AQR PHY, we keep port.status[][].mac_if to SGMII mode to
               get status via mesa_phy_status_get.
            */
            T_I("port %d: set PCS to SGMII mode", port_no);
            ps->if_type = MESA_PORT_INTERFACE_SGMII_CISCO;
        } else if (port.phy_id_1g[port_no].part_number != MESA_PHY_TYPE_AQR_409) {
            /* All AQR PHYs' system side are SFI except AQR 409. AQR 409's host side is SGMII, but if the link
               is 100fdx, the system side must be set to Cisco SGMII. */
            ps->if_type = MESA_PORT_INTERFACE_SFI;
        } else {
            ps->if_type = MESA_PORT_INTERFACE_SGMII;
        }
    } else if (conf->speed == MESA_SPEED_AUTO /* Auto means 1G-Aneg */ &&
        (ps->if_type == MESA_PORT_INTERFACE_VAUI || sfp_mac_if_is_10g_mode(ps->if_type))) {
        ps->if_type = MESA_PORT_INTERFACE_SERDES;
        conf->speed = MESA_SPEED_1G;
    } else if (ps->if_type == MESA_PORT_INTERFACE_SERDES &&
               conf->speed == MESA_SPEED_100M) {   // e.g. DAC cable on 1G port and force to 100M
        ps->if_type = MESA_PORT_INTERFACE_100FX;
    } else if (ps->if_type == MESA_PORT_INTERFACE_VAUI &&
               conf->speed == MESA_SPEED_1G) {     // e.g. DAC cable on 2.5G port and force to 1G
        ps->if_type = MESA_PORT_INTERFACE_SERDES;
    } else if (ps->if_type == MESA_PORT_INTERFACE_VAUI &&
               conf->speed == MESA_SPEED_100M) {   // e.g. DAC cable on 2.5G port and force to 100M
        ps->if_type = MESA_PORT_INTERFACE_100FX;
    } else if (sfp_mac_if_is_10g_mode(ps->if_type) &&
               conf->speed == MESA_SPEED_2500M) { // e.g. DAC cable on 10G port and force to 2.5G
        ps->if_type = MESA_PORT_INTERFACE_VAUI;
    } else if (sfp_mac_if_is_10g_mode(ps->if_type) &&
               conf->speed == MESA_SPEED_1G) {    // e.g. DAC cable on 10G port and force to 1G
        ps->if_type = MESA_PORT_INTERFACE_SERDES;
    } else if (sfp_mac_if_is_10g_mode(ps->if_type) &&
               conf->speed == MESA_SPEED_100M) {  // e.g. DAC cable on 10G port and force to 100M
        ps->if_type = MESA_PORT_INTERFACE_100FX;
    }
    T_DG_PORT(TRACE_GRP_SFP, port_no, "Final state: ps->if_type = %s", mac_if2txt(ps->if_type));
    port_status->mac_if = ps->if_type;

    ps->power_down = (conf->admin.enable ? 0 : 1);
    conf_mgmt_mac_addr_get(ps->flow_control.smac.addr, port_no + 1);
    ps->max_frame_length = conf->max_length;
    T_DG_PORT(TRACE_GRP_SFP, port_no, "MEP loop port: %d", mep_loop_port);
    if (mep_loop_port != MESA_PORT_NO_NONE) {
        // Cannot make header check on MEP loop port, because the looped frames are prefixed by an IFH
        // instead of an Ethernet header, and the IFH may contain something that gets interpreted as
        // a length field in the EtherType field.
        ps->frame_length_chk = (port_no != mep_loop_port) && conf->frame_length_chk;
    } else {
        ps->frame_length_chk = conf->frame_length_chk;
    }
    ps->max_tags = conf->max_tags;
    ps->exc_col_cont = conf->exc_col_cont;
    ps->sd_enable = (cap & MEBA_PORT_CAP_SD_ENABLE ? 1 : 0);
    ps->sd_active_high = (cap & MEBA_PORT_CAP_SD_HIGH ? 1 : 0);
    ps->sd_internal = (cap & MEBA_PORT_CAP_SD_INTERNAL ? 1 : 0);
    ps->xaui_rx_lane_flip = (cap & MEBA_PORT_CAP_XAUI_LANE_FLIP ? 1 : 0);
    ps->xaui_tx_lane_flip = (cap & MEBA_PORT_CAP_XAUI_LANE_FLIP ? 1 : 0);
    ps->serdes.rx_invert = (cap & MEBA_PORT_CAP_SERDES_RX_INVERT ? 1 : 0);
    ps->serdes.tx_invert = (cap & MEBA_PORT_CAP_SERDES_TX_INVERT ? 1 : 0);
    ps->serdes.media_type = sfp2media(sfp->type);
    for (int i = 0; i < VTSS_PRIOS; i++) {
        ps->flow_control.pfc[i] = conf->pfc[i];
    }
    if (fc == 0) {
        pcs_force = 1; /* If fc is disabled by mgmt then it should always be disabled in the chip regardless of phy status */
    }
    T_IG_PORT(TRACE_GRP_SFP, port_no, "speed:%s, link:%d, cap:0x%X", conf->speed == MESA_SPEED_AUTO ? "AUTO_1G" : vtss_port_status_speed_txt[conf->speed].valueName, status->link, cap);

    if ((port_phy(port_no) || is_base_t(port_no)) && conf->speed == MESA_SPEED_AUTO && status->link) {
        /* If autoneg and link up, status values are used */
        ps->speed = status->speed; // BZ#23176,23244
        T_IG_PORT(TRACE_GRP_SFP, port_no, "speed:%d, fc:%d, pcs_force:%d, obey_pause:%d, generate_pause:%d",
                  ps->speed, fc, pcs_force, status->aneg.obey_pause, status->aneg.generate_pause);
        ps->fdx = status->fdx;
        if (ps->if_type == MESA_PORT_INTERFACE_SGMII_CISCO) {
            ps->flow_control.obey = fc;
            ps->flow_control.generate = fc;
        } else {
            /* Chip FC should only be enabled if the information is coming from the Phy (through status) on SGMII ports */
            /* If fc=0, i.e. disabled then it should always be disabled regardless of phy status */
            ps->flow_control.obey =     pcs_force ? fc : status->aneg.obey_pause;
            ps->flow_control.generate = pcs_force ? fc : status->aneg.generate_pause;
        }
    } else {
        /* If forced mode or link down, configured values are used */
        ps->speed = (conf->speed == MESA_SPEED_AUTO ? MESA_SPEED_1G : conf->speed);
        if (ps->if_type == MESA_PORT_INTERFACE_SGMII_CISCO) {
            if (port.phy_id_1g[port_no].part_number == MESA_PHY_TYPE_AQR_407 && status->link) {
                ps->speed = status->speed;
                T_I("port %d set speed to %d", port_no, ps->speed);
            } else {
                ps->speed = MESA_SPEED_1G;
            }
        } else if (ps->if_type == MESA_PORT_INTERFACE_100FX) {
            // 100FX port interface only supports 100M speed (refer to API layer vrfy_spd_iface())
            ps->speed = MESA_SPEED_100M;
        } else if (ps->if_type == MESA_PORT_INTERFACE_SERDES) {
            // Serdes port interface only supports 1G speed (refer to API layer vrfy_spd_iface())
            ps->speed = MESA_SPEED_1G;
        } else if (ps->if_type == MESA_PORT_INTERFACE_VAUI) {
            // VAUI port interface only supports 2.5G speed (refer to API layer vrfy_spd_iface())
            ps->speed = MESA_SPEED_2500M;
        } else if (sfp_mac_if_is_10g_mode(ps->if_type)) {
            // SFI port interface only supports 10G speed (refer to API layer vrfy_spd_iface())
            ps->speed = MESA_SPEED_10G;
        } else if (!port_phy(port_no) && sfp->status.present) {
            // When SFP module is presented, it is determined in port_sfp_speed_adjust()
            ps->speed = (conf->speed == MESA_SPEED_AUTO && is_port_phy(port_no) && !status->link) ? MESA_SPEED_1G : conf->speed;
        }
        ps->fdx = conf->fdx;
        ps->flow_control.obey = fc;
        ps->flow_control.generate = fc;
    }

    /* If port is 10G Copper PHY, speed is auto mode and interface is SFI interface:,
       1. SFI only support 10G mode.
       2. Flow control function won't refer to PHY status.
    */
    if (is_port_10g_copper_phy(port_no) && conf->speed == MESA_SPEED_AUTO && ps->if_type == MESA_PORT_INTERFACE_SFI) {
        ps->speed = MESA_SPEED_10G;
        ps->flow_control.obey = fc;
        ps->flow_control.generate = fc;
    }

    ps->loop = (conf->adv_dis & VTSS_APPL_PORT_ADV_UP_MEP_LOOP ? MESA_PORT_LOOP_PCS_HOST : MESA_PORT_LOOP_DISABLE);

    if (conf->oper_up) {
        /* Force operational state up */
        mesa_port_state_set(NULL, port_no, 1);
    } else if (!status->link || conf->oper_down) {
        /* Set operational state down if link is down */
        mesa_port_state_set(NULL, port_no, 0);
    }

    if (conf->admin.enable) {
        T_IG_PORT(TRACE_GRP_AMS, port_no, "AMS %s: link %s with %s connecting",
            conf->dual_media_fiber_speed == MESA_SPEED_FIBER_100FX ? "FORCE-100FX" :
            conf->dual_media_fiber_speed == MESA_SPEED_FIBER_1000X ? "FORCE-1000X" :
            conf->dual_media_fiber_speed == MESA_SPEED_FIBER_AUTO ? "AUTO" : "DISABLED",
                  status->link ? "UP" : "DOWN",
                  status->fiber ? "FIBER" : "COPPER");
        // Do configuration stuff that are board specific - before the port is enabled
        if (ps->loop) {
            /* Disable loop port externally */
            conf->admin.enable = 0;
        }
        // Enable means mean tx-disable GPIO for the SFP when calling port_custom_conf.
        if (conf->dual_media_fiber_speed == MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED &&
            (cap & MEBA_PORT_CAP_DUAL_COPPER)) {
            // Turn off SFP module - Bugzilla#20997
            T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "Disable SFP port admin state");
            conf->admin.enable = 0;
        }

        // Turn off SFP laser in case we are configured in a forced speed which the current SFP doesn't support
        meba_port_cap_t cap_status = port.status[VTSS_ISID_LOCAL][port_no].cap;
        if (conf->speed == MESA_SPEED_100M) {
            if ((cap_status & MEBA_PORT_CAP_100M_FDX) == 0) {
                conf->admin.enable = 0;
            }
            T_IG_PORT(TRACE_GRP_CONF, port_no, "enable:%d, 0x%X, 0x%X", conf->admin.enable, cap_status, MEBA_PORT_CAP_1G_FDX);
        }

        if (conf->speed == MESA_SPEED_1G) {
            if ((cap_status & MEBA_PORT_CAP_1G_FDX) == 0) {
                conf->admin.enable = 0;
            }
            T_IG_PORT(TRACE_GRP_CONF, port_no, "enable:%d, 0x%X, 0x%X", conf->admin.enable, cap_status, MEBA_PORT_CAP_1G_FDX);
        }
        T_IG_PORT(TRACE_GRP_CONF, port_no, "enable:%d, ps->speed:%d", conf->admin.enable, conf->speed);
        (void) port_admin_state_set(board_instance, port_no, &conf->admin);

        /* Turn off the PHY if the port is configure to fiber mode */
        if (cap & MEBA_PORT_CAP_DUAL_COPPER && !is_port_phy(port_no)) {
            mesa_phy_conf_t               phy_dis;
            if ((rc = mesa_phy_conf_get(PHY_INST, port_no, &phy_dis)) != VTSS_OK) {
                T_E("mesa_phy_conf_set failed, port_no: %u", port_no);
            }
            phy_dis.mode = (conf->dual_media_fiber_speed == MESA_SPEED_FIBER_100FX ||
                            conf->dual_media_fiber_speed == MESA_SPEED_FIBER_1000X ||
                            conf->dual_media_fiber_speed == MESA_SPEED_FIBER_ONLY_AUTO) ?
                            MESA_PHY_MODE_POWER_DOWN : MESA_PHY_MODE_ANEG;
            T_IG_PORT(TRACE_GRP_AMS, port_no, "turn %s PHY's power",
                    phy_dis.mode == MESA_PHY_MODE_POWER_DOWN ? "OFF" : "ON");

            if ((rc = mesa_phy_conf_set(PHY_INST, port_no, &phy_dis)) != VTSS_OK) {
                T_E("mesa_phy_conf_set failed, port_no: %u", port_no);
            }
        }

        conf->admin.enable = 1;
    }

    T_IG_PORT(TRACE_GRP_SFP, port_no, "if_type = %s, speed:%d %s %s %s", mac_if2txt(ps->if_type), ps->speed,
        ps->fdx ? "FDX" : "HDX", ps->flow_control.obey ? "OBEY" : "", ps->flow_control.generate ? "GENERATE" : "" );

    // 20180808 LNTN workaround
    // port_led_update will turn on wrong LED when SFP speed changed
    // so the speed should be updated early
    port.status[VTSS_ISID_LOCAL][port_no].status.speed = ps->speed;

    if ((rc2 = mesa_port_conf_set(NULL, port_no, ps)) != VTSS_OK) {
        T_E("mesa_port_conf_set failed, port_no: %u", port_no);
        rc = rc2;
    }

    if (MESA_CAP(MESA_CAP_PHY_TS) || MESA_CAP(MESA_CAP_TS)) {
        if (mesa_ts_status_change(NULL, port_no) != VTSS_RC_OK) {
            T_I("mesa_ts_status_change failed  port %d, speed %d", port_no, status->speed);
        }
    }

    if (phy != NULL && phy->mode == MESA_PHY_MODE_POWER_DOWN) {
        /* Note that the phy must be power down after the switch port is disabled */
        if ((rc = mesa_phy_conf_set(PHY_INST, port_no, phy)) != VTSS_OK) {
            T_E("mesa_phy_conf_set failed, port_no: %u", port_no);
        }
    }

    /* XAUI and VSC848x can run on 1G/10G mode */

    if ((cap & MEBA_PORT_CAP_VTSS_10G_PHY) && (cap & MEBA_PORT_CAP_1G_FDX)) {
        if (phy_10g_speed_setup(port_no, (conf->speed == MESA_SPEED_2500M || (conf->speed == MESA_SPEED_AUTO)) ? MESA_SPEED_1G : conf->speed,
                                conf->speed == MESA_SPEED_AUTO, conf->flow_control, conf->admin.enable) != VTSS_RC_OK) {
            rc = VTSS_RC_ERROR;
        }
    }

    if (!conf->admin.enable) {
        // Do configuration stuff that are board specific - after the port is disabled.
        T_IG_PORT(TRACE_GRP_CONF, port_no, "enable:%d", conf->admin.enable);
        (void) port_admin_state_set(board_instance, port_no, &conf->admin);
    }

    if ((change & PORT_CONF_CHANGE_PHY_SGMII) && ps->if_type == MESA_PORT_INTERFACE_SGMII_CISCO) {
        T_D("Set port %d to SGMII mode", port_no);
        if (port_sfp_sgmii_set(port_no, &dummy) == FALSE) {
            T_NG_PORT(TRACE_GRP_SFP, port_no, "%s", "Failed to configure to SGMII mode via I2C");
            rc = VTSS_RC_ERROR;
        }
    }

    /* The aneg status must get updated after port change - therefore we clear it here */
    if (cap & MEBA_PORT_CAP_AUTONEG)  {
        memset(&port.aneg_status[port_no], 0, sizeof(mesa_port_status_t));
    }

    T_D("exit, port_no: %u", port_no);
    return rc;
}

/* Convert port status to port info */
static void port_status2info(vtss_isid_t isid, mesa_port_no_t port_no, port_info_t *info)
{
    vtss_appl_port_status_t *status = &port.status[isid][port_no];

    info->link = status->status.link;
    info->speed = status->status.speed;
    info->fdx = status->status.fdx;
    info->fiber = status->status.fiber;
    info->stack = port_isid_port_no_is_stack(isid, port_no);
    info->phy = port_isid_phy(isid, port_no);
    info->chip_no   = status->chip_no;
    info->chip_port = status->chip_port;
}

/* Convert port status to MIB/JSON status */
static void port_status2mib(vtss_isid_t isid, mesa_port_no_t port_no, vtss_appl_port_mib_status_t *mib_status)
{
    vtss_appl_port_status_t *status = &port.status[isid][port_no];

    mib_status->link     = status->status.link;
    mib_status->speed    = status->status.speed;
    mib_status->fdx      = status->status.fdx;
    mib_status->fiber    = status->status.fiber;
    mib_status->sfp_info = status->sfp;
}

/* Port change event callbacks */
/* 'link_transition' means that the link went from up to down (0) or down to up (1). This does not need to be the same as the current link state */
static void port_change_event(mesa_port_no_t port_no, BOOL link_transition, BOOL changed)
{
    int               i;
    port_change_reg_t *reg;
    port_info_t       info;
    vtss_tick_count_t  ticks;
    ulong             msec;

    /* Leave critical region before doing callbacks */
    port_status2info(VTSS_ISID_LOCAL, port_no, &info);
    info.link = link_transition;
    PORT_CRIT_EXIT();

    PORT_CB_CRIT_ENTER();
    for (i = 0; i < PORT_CHANGE_REG_MAX; i++) {
        reg = &port.change_table.reg[i];
        if (i < port.change_table.count && (changed || reg->port_done[port_no] == 0)) {
            /* Port state changed or initial event not done */
            reg->port_done[port_no] = 1;
            T_N("callback, i: %d (%s), port_no: %u",  i, vtss_module_names[reg->module_id], port_no);
            ticks = vtss_current_time();
            reg->callback(port_no, &info);
            ticks = (vtss_current_time() - ticks);
            if (ticks > reg->max_ticks)
                reg->max_ticks = ticks;
            msec = VTSS_OS_TICK2MSEC(ticks);
            T_N("callback done, i: %d (%s), port_no: %u, " VPRIlu " msec", i, vtss_module_names[reg->module_id], port_no, msec);
        }
    }
    PORT_CB_CRIT_EXIT();

    /* Enter critical region again */
    PORT_CRIT_ENTER();
}

/* Port global change event callbacks */
static void port_global_change_event(vtss_isid_t isid, mesa_port_no_t port_no,
                                     port_info_t *info)
{
    int                      i;
    port_global_change_reg_t *reg;
    vtss_tick_count_t         ticks;
    ulong                    msec;

#ifdef VTSS_SW_OPTION_SYSLOG
    char buf[128], *p = &buf[0];

    p += sprintf(p, "LINK-UPDOWN: ");
    p += sprintf(p, "Interface %s", SYSLOG_PORT_INFO_REPLACE_KEYWORD);
    p += sprintf(p, ", changed state to %s.", info->link ? "up" : "down");
    S_PORT_N(isid, port_no, "%s", buf);
#endif /* VTSS_SW_OPTION_SYSLOG */

    PORT_CB_CRIT_ENTER();
    for (i = 0; i < port.global_change_table.count; i++) {
        reg = &port.global_change_table.reg[i];
        T_R("callback, i: %d (%s), isid: %d, port_no: %u",
            i, vtss_module_names[reg->module_id], isid, port_no);
        ticks = vtss_current_time();
        reg->callback(isid, port_no, info);
        ticks = (vtss_current_time() - ticks);
        if (ticks > reg->max_ticks)
            reg->max_ticks = ticks;
        msec = VTSS_OS_TICK2MSEC(ticks);
        T_R("callback done, i: %d (%s), isid: %d, port_no: %u, " VPRIlu" msec",
            i, vtss_module_names[reg->module_id], isid, port_no, msec);
    }
    PORT_CB_CRIT_EXIT();
}

/* Check and generate global port change events */
static void port_global_change_events(void)
{
    vtss_isid_t    isid;
    mesa_port_no_t port_no;
    port_info_t    info;
    uchar          flags;
    u32            setup_change;

    PORT_CRIT_ENTER();
    for (isid = VTSS_ISID_START; isid < VTSS_ISID_END; isid++) {

        if (!msg_switch_is_master() || !msg_switch_exists(isid))
            continue;

        for (port_no = 0; port_no < port.port_count[isid]; port_no++) {
            flags = port.change_flags[port_no];
            if (flags & PORT_CHANGE_ANY) {
                port.change_flags[port_no] = 0;
                port_status2info(isid, port_no, &info);

                /* Leave critical region before doing callbacks */
                PORT_CRIT_EXIT();

                if (info.link) {
                    /* Link is up now, check for link down event first */
                    if (flags & PORT_CHANGE_DOWN) {
                        info.link = 0;
                        port_global_change_event(isid, port_no, &info);
                        info.link = 1;
                    }
                    if (flags & PORT_CHANGE_UP)
                        port_global_change_event(isid, port_no, &info);
                } else {
                    /* Link is down now, check for link up event first */
                    if (flags & PORT_CHANGE_UP) {
                        info.link = 1;
                        port_global_change_event(isid, port_no, &info);
                        info.link = 0;
                    }
                    if (flags & PORT_CHANGE_DOWN)
                        port_global_change_event(isid, port_no, &info);
                }

                /* Enter critical region again */
                PORT_CRIT_ENTER();
            }

            /* Check for asynchronous port setup */
            setup_change = port.setup_change[port_no];
            if (setup_change) {
                port.setup_change[port_no] = 0;
                port_setup(port_no, setup_change);
            }
        }
    }
    PORT_CRIT_EXIT();
}

// Read PHY register via I2C
static mesa_rc port_sfp_phy_read(mesa_port_no_t port_no, u8 addr, u16 *value)
{
    u8 data[2];

    VTSS_RC(meba_sfp_i2c_xfer(board_instance, port_no, false, 0x56, addr, data, 2, TRUE));
    *value = ((data[0] << 8) | data[1]);
    return VTSS_RC_OK;
}

// Write PHY register via I2C
static mesa_rc port_sfp_phy_write(mesa_port_no_t port_no, u8 addr, u16 value)
{
    u8 data[2];

    data[0] = ((value >> 8) & 0xff);
    data[1] = (value & 0xff);
    return meba_sfp_i2c_xfer(board_instance, port_no, true, 0x56, addr, data, 2, TRUE);
}

/*  Read AQR SFP+ mmd register through I2C
    Refer to AN-S1201- Accessing Internal Registers
    Aquantia provides the flexibility for users to access the module's internal registers
    through the I2C/SMBus interface address 0x25 for additional control and status information.
    ** Rough Datagram of Read/Write Word **
       SlaveAddr - Comman Codes - DataBytes - Stop
          Command Codes = 8 bits
            [7] = 0
            [6:5] = 0x0 Set Address
                    0x1 Write
                    0x2 Read
            [4:0] = MMD Address (1,3,4,7,...)

    ** To issue a Read command: **
    1. Set Address
        Write Command: SlaveAddr, CMD, MMD, Register Address Low, Register Address High
    2. Read Data
        Read Command: SlaveAddr, CMD, MMD, Data Low, Data High

*/
static mesa_rc port_aqr_sfp_mmd_reg_read(mesa_port_no_t port_no, u8 mmd, u16 reg, u16 *value)
{
    u8 addr, data[2], cmd_addr = 0;
    mesa_rc rc = VTSS_RC_ERROR;
    cmd_addr = 0; //set address
    addr = (cmd_addr << 5) | (mmd & 0x1F);
    data[0] = (reg >> 0) & 0xFF;
    data[1] = (reg >> 8) & 0xFF;
    //set address
    if ((rc = meba_sfp_i2c_xfer(board_instance, port_no, true, 0x25, addr, data, 3, FALSE)) == VTSS_RC_OK) {
        //do read mmd
        data[0] = data[1] = 0;
        cmd_addr = 2; //read
        addr = (cmd_addr << 5) | (mmd & 0x1F);
        if ((rc = meba_sfp_i2c_xfer(board_instance, port_no, false, 0x25, addr, data, 2, FALSE)) == VTSS_RC_OK) {
            if (value) {
                *value = (data[1] << 8) | data[0];
            }
        }
    }
    return rc;
}

// Translate mmd register value to mesa speed, the register value should be read from 7.C800
static mesa_port_speed_t port_aqr_sfp_mmd_val_2_speed(u16 reg_value)
{
    u8 speed;
    speed = (reg_value & 0xE) >> 1;
    switch (speed) {
    case 0x1:
        return MESA_SPEED_100M;
    case 0x2:
        return MESA_SPEED_1G;
    case 0x3:
        return MESA_SPEED_10G;
    case 0x4:
        return MESA_SPEED_2500M;
    case 0x5:
        return MESA_SPEED_5G;
    default:
        break;
    }
    return MESA_SPEED_UNDEFINED;
}

/* Poll port status */
static void port_status_poll(mesa_port_no_t port_no, BOOL *changed, mesa_event_t *link_down, BOOL fast_link_failure)
{
    vtss_appl_port_status_t *port_status;
    mesa_port_status_t      *status, old_status, *old_aneg_status;
    vtss_appl_port_conf_t   *conf;
    int                i;
    mesa_rc            rc = VTSS_RC_OK;
    meba_sfp_status_t  sfp_status;

    T_R_PORT(port_no, "%s", "enter");

    /* Get current status */
    i = port_no;
    port_status = &port.status[VTSS_ISID_LOCAL][i];
    conf = &port.config[VTSS_ISID_LOCAL][i];
    status = &port_status->status;
    old_aneg_status = &port.aneg_status[i];
    old_status = *status;

    /* Must break up the port_status_get into a Phy or a Switch call to support the PHY_INST */
    T_R_PORT(port_no, "phy status get, spd:%d, port_p:%d", conf->speed, port_phy(port_no));

    // If there is connected a cu SFP then first figure out if the cu SFP is used
    // or if it is the PHY cu port that is used (status->copper)
    if (is_base_t(port_no) && is_port_phy(port_no)) {
        rc = mesa_phy_status_get(PHY_INST, port_no, status);
    }

    if (port_phy(port_no) ||
        (port.ams_mode[port_no] != VTSS_APPL_AMS_FIBER_FORCED && status->copper)) {
        if (port.status[VTSS_ISID_LOCAL][port_no].cap != 0) { // Do not poll status for port with no capabilities
            rc = mesa_phy_status_get(PHY_INST, port_no, status);
            T_NG_PORT(TRACE_GRP_AMS, port_no, "%s(%s)", status->link ? "UP": "DOWN", status->fiber ? "FI" : "PHY");
        }
    } else if (port_custom_table[port_no].mac_if == MESA_PORT_INTERFACE_INTERNAL) {
    } else if (port_custom_table[port_no].mac_if == MESA_PORT_INTERFACE_NO_CONNECTION) {
        status->link = FALSE;
        status->speed = MESA_SPEED_UNDEFINED;
        status->fdx = FALSE;
    } else {
        if (mesa_port_status_get(NULL, port_no, status) != VTSS_OK) {
            T_E_PORT(port_no, "%s", "status_get failed");
            return;
        }

        T_RG_PORT(TRACE_GRP_SFP, port_no, "link:%d", status->link);

        // Well, it was not a PHY then it is a SFP
        status->fiber = TRUE;

        char *vendor = port.status[VTSS_ISID_LOCAL][port_no].sfp.vendor_name;
        if (!strcmp(vendor, "AQUANTIA")) {
            u16 value = 0;
            mesa_rc ret;
            if ((ret = port_aqr_sfp_mmd_reg_read(port_no, 1, 0xE800, &value)) == MESA_RC_OK) { //read the current link status
                if (value & 0x1) {// if link up
                    if ((ret = port_aqr_sfp_mmd_reg_read(port_no, 7, 0xC800, &value)) == MESA_RC_OK) { //read the current line-rate
                        status->speed = port_aqr_sfp_mmd_val_2_speed(value);
                        T_NG_PORT(TRACE_GRP_SFP, port_no, "update AQ link speed:%d", status->speed);
                    }
                }
            }
        }

        T_RG_PORT(TRACE_GRP_SFP, port_no, "sfp.type:%d", port.status[VTSS_ISID_LOCAL][port_no].sfp.type);

        // Determine Flow control for CU SFPs
        if (conf->admin.enable && is_base_t(port_no)) {
            // Is the a CU SFP - If it is then we need to read the phy registers in the SFP in order to determine speed and flow control.
            u16 reg5;

            if ((rc = port_sfp_phy_read(port_no, 5, &reg5)) == VTSS_RC_OK) { // Reading flow control advertisement. Note : SFP register at address 0x56 are 16 bits registers
                // Determining flow control based upon SFP registers values.
                mesa_phy_conf_t phy_setup;

                port_phy_setup_aneg_get(port_no, &phy_setup); // Get the current phy setup

                mesa_phy_flowcontrol_decode_status(NULL, port_no, reg5, phy_setup, status); // Update Flow control status

                T_NG_PORT(TRACE_GRP_SFP, port_no, "reg5:0x%X  phy_setup.aneg.speed_1g_fdx:%d, rc:%d",
                          reg5, phy_setup.aneg.speed_1g_fdx, rc);

            } else {
                T_IG_PORT(TRACE_GRP_SFP, port_no, "rc:%s", error_txt(rc));
            }

            T_NG_PORT(TRACE_GRP_SFP, port_no, "rc:%d", rc);
        }
    }

    T_RG_PORT(TRACE_GRP_SFP, port_no, "fast_link_failure:%d, link:%d", fast_link_failure, status->link);
    /* Force link state to false as long as fast link failure 'hold' timer is running */
    if (fast_link_failure)
        status->link = false;

    do {
        // LNTN added, detect loss signal too, otherwise there is a debounce problem.:
        lntn_global_conf_t lntn_conf = lntn_conf_get();
        lntn_port_conf_t *port_conf = &lntn_conf.port_conf[port_no];
        if(port_conf_sfp_check(port_conf)) {
            if (meba_sfp_status_get(board_instance, port_no, &sfp_status) == VTSS_RC_OK) {
                if(!sfp_status.los || !sfp_status.present) {
                    status->link = 0;
                }
            }
        }
    } while(0);

    /* Detect link down and disable port */
    if ((!status->link || status->link_down) && old_status.link) {
        T_I("link down event on port_no: %u, status->link:%d, status->link_down:%d, old_status.link:%d", port_no, status->link, status->link_down, old_status.link);
        *changed = TRUE;
        *link_down = 1;
        old_status.link = 0;

        if (!conf->oper_up) {
            /* Set operational state down unless forced up */
            mesa_port_state_set(NULL, port_no, 0);
        }
        port_change_event(port_no, 0, 1); // Link went from up to down (0)
        port_status->port_down_count++;

        /* Workaround that the link speed is 1G once the port has been link to 2.5G
           then repluging in cable or re-enabling the port. The issue only occurs on
           AQR FW 3.0.E and needs further investigation about the root cause.
         */
        if (port.phy_id_1g[port_no].part_number == MESA_PHY_TYPE_AQR_407) {
            mesa_port_conf_t              port_conf;
            (void) mesa_port_conf_get(NULL, port_no, &port_conf);
            port_conf.if_type = MESA_PORT_INTERFACE_SGMII;
            port_conf.speed = MESA_SPEED_1G;
            port_conf.fdx = 1;

            /* Make sure port_setup() is invoked when the port is up. */
            old_aneg_status->speed = MESA_SPEED_UNDEFINED;
            T_D("port %d back to SGMII mode", port_no);
            if (mesa_port_conf_set(NULL, port_no, &port_conf) != VTSS_OK) {
                T_E("mesa_port_conf_set failed, port_no: %u", port_no);
            }
        }
    }
        //After the board boots up and PTP is initialized OOS API needs to be called for Viper B.
    if(!first_link_up[port_no]) {
            T_N("Board type JR2 REF detected\n");
            //Check if the PHY is viper B
            mesa_phy_type_t viper_phy;
            mesa_phy_ts_fifo_conf_t fifo_conf_viper;
            memset(&fifo_conf_viper, 0, sizeof(mesa_phy_ts_fifo_conf_t));

            rc = mesa_phy_id_get(PHY_INST, port_no, &viper_phy);
            if((viper_phy.part_number == MESA_PHY_TYPE_8584)){
                //Check if 1588 Configuration has been re-applied from running config.
                mesa_bool_t ts_en = FALSE;
                rc = mesa_phy_ts_mode_get(PHY_INST, port_no, &ts_en);
                if((first_link_up[port_no] == FALSE) && ts_en){
                    T_D("PTP configuration from Startup config applied, Running OOS Algorithm\n");
                    first_link_up[port_no] = TRUE;
                    mesa_phy_ts_viper_fifo_reset(PHY_INST, port_no, &fifo_conf_viper);
              }
          } else {
              first_link_up[port_no] = TRUE;
          }
        }

    /* Detect link up or speed/duplex change and setup port */
    if (status->link && (!old_status.link ||
                         status->speed != old_status.speed ||
                         status->fdx != old_status.fdx)) {
        if (old_status.link) {
            T_D("speed/duplex change event on port_no: %u", port_no);
        } else {
            T_I("link up event on port_no: %u, speed: %u gen_pause:%d obey_pause:%d", port_no, status->speed, status->aneg.generate_pause, status->aneg.obey_pause);
            if (conf->speed == MESA_SPEED_AUTO || port.mac_sfp_if[port_no] == MESA_PORT_INTERFACE_SGMII_CISCO) {
                /* A port setup is only performed if there is something new to setup  */
                if ((status->aneg.generate_pause != old_aneg_status->aneg.generate_pause) ||
                    (status->aneg.obey_pause     != old_aneg_status->aneg.obey_pause) ||
                    (status->speed               != old_aneg_status->speed) ||
                    (status->fdx                 != old_aneg_status->fdx)) {
                    port_setup(port_no, PORT_CONF_CHANGE_MAC);
                    *old_aneg_status = *status;
                }
            }
            if (!conf->oper_down) {
                mesa_port_state_set(NULL, port_no, 1);
            }

            T_I_PORT(port_no, "cap:0x%X, fiber:%d", port_custom_table[port_no].cap, status->fiber);
        }
        *changed = TRUE;
        port_change_event(port_no, 1, 1); // Link went from down to up (1)
        port_status->port_up_count++;
        T_IG_PORT(TRACE_GRP_AMS, port_no, "Detect linkup or speed/duplex chage to %s",
                  port_status->status.fiber ? "FIBER" : "COPPER");

    }


    T_R_PORT(port_no, "port_status->status.fiber:%d", port_status->status.fiber);

    // Default expecting no power savings and don't expect power capable
    port_status->power.actiphy_capable       = FALSE;
    port_status->power.actiphy_power_savings = FALSE;
    port_status->power.perfectreach_capable  = FALSE;
    port_status->power.perfectreach_power_savings = FALSE;

#ifdef VTSS_SW_OPTION_PHY_POWER_CONTROL
    // When ActiPhy is enabled power is saved when the link is down
    if (port_phy(port_no)) {
        port_status->power.actiphy_capable = TRUE;
        port_status->power.perfectreach_capable = TRUE;
    }

    if (port_phy(port_no) && !status->link && (conf->power_mode == MESA_PHY_POWER_ACTIPHY || conf->power_mode == MESA_PHY_POWER_ENABLED)) {
        port_status->power.actiphy_power_savings = TRUE;
    }

    // When PerfectReach is enabled power is saved when the link is up
    if (status->link && (conf->power_mode == MESA_PHY_POWER_DYNAMIC || conf->power_mode == MESA_PHY_POWER_ENABLED)) {
        port_status->power.perfectreach_power_savings = TRUE;
    }
    T_R_PORT(port_no, "PerSav:%d, PerCap:%d, actiSav:%d, actCap:%d, link:%d, power:%d",
        port_status->power.perfectreach_power_savings, port_status->power.perfectreach_capable, port_status->power.actiphy_power_savings, port_status->power.actiphy_capable, status->link, conf->power_mode);


#endif

    /* Initial port change event */
    port_change_event(port_no, status->link, 0);

    T_RG_PORT(TRACE_GRP_SFP, port_no, "link:%d", status->link);
}

static mesa_rc port_veriphy_start(mesa_port_no_t port_no, port_veriphy_mode_t mode)
{
    u8             i;
    port_veriphy_t *veriphy = &port.veriphy[port_no];

    veriphy->running = 1;
    veriphy->valid = 0;
    veriphy->mode = mode;

    // Bugzilla#8911, Sometimes cable length is measured too long (but never too short), so veriphy is done multiple times and the shortest length is found. Start with the longest possible length.
    for (i = 0; i  < 4; i++) {
        veriphy->result.status[i] = MESA_VERIPHY_STATUS_UNKNOWN;
        veriphy->result.length[i] = 255;
        veriphy->variate_cnt = 0;
        T_D_PORT(port_no, "Len:%d",  veriphy->result.length[i]);
    }
    veriphy->repeat_cnt = VERIPHY_REPEAT_CNT;

    return mesa_phy_veriphy_start(PHY_INST, port_no,
                                  mode == PORT_VERIPHY_MODE_BASIC ? 2 :
                                  mode == PORT_VERIPHY_MODE_NO_LENGTH ? 1 : 0);
}

/* Determine if port configuration has changed */
static u32 port_conf_change(vtss_appl_port_conf_t *old, const vtss_appl_port_conf_t *new_)
{
    u32 change;

    if (new_->speed != old->speed ||
        new_->dual_media_fiber_speed != old->dual_media_fiber_speed) {
        /* Full change */
        T_IG(TRACE_GRP_SFP, "new_dual:%d, old_daul:%d, new_spd:%d, old_spd:%d",
             new_->dual_media_fiber_speed, old->dual_media_fiber_speed, new_->speed, old->speed);
        change = PORT_CONF_CHANGE_ALL;
    } else if (new_->admin.enable != old->admin.enable ||
               new_->speed != old->speed ||
               new_->fdx != old->fdx ||
               new_->flow_control != old->flow_control ||
               new_->frame_length_chk != old->frame_length_chk ||
#ifdef VTSS_SW_OPTION_PHY_POWER_CONTROL
               new_->power_mode != old->power_mode ||
#endif /* VTSS_SW_OPTION_PHY_POWER_CONTROL */
               new_->adv_dis != old->adv_dis) {
        /* PHY and MAC change */
        change = (PORT_CONF_CHANGE_PHY | PORT_CONF_CHANGE_MAC);
        /* since port disable have power down SFP, once port is configured to enable, SFP needs to be configured again */
        if ( new_->admin.enable != old->admin.enable && new_->admin.enable ) {
            T_D("change PHY to SGMII");
            change |= PORT_CONF_CHANGE_PHY_SGMII;
        }
        T_IG(TRACE_GRP_CONF, "PHY change");
    } else if (new_->exc_col_cont != old->exc_col_cont ||
               new_->max_tags != old->max_tags ||
               new_->max_length != old->max_length ||
               new_->oper_up != old->oper_up ||
               (memcmp(new_->pfc, old->pfc, sizeof(new_->pfc)) != 0)) {
        /* MAC change only */
        T_IG(TRACE_GRP_CONF, "MAC change");
        change = PORT_CONF_CHANGE_MAC;
    } else {
        /* No change */
        T_DG(TRACE_GRP_CONF, "No change");
        change = PORT_CONF_CHANGE_NONE;
    }
    return change;
}

static BOOL port_vol_conf_changed(const port_vol_conf_t *old, const port_vol_conf_t *new_)
{
    return (old->disable             != new_->disable ||
            old->loop                != new_->loop    ||
            old->oper_up             != new_->oper_up ||
            old->oper_down           != new_->oper_down ||
            old->disable_adm_recover != new_->disable_adm_recover ? 1 : 0);
}

/* Clear local port counters */
static mesa_rc port_counters_clear(mesa_port_no_t port_no)
{
    vtss_appl_port_status_t *port_status = &port.status[VTSS_ISID_LOCAL][port_no];

    port_status->port_up_count = 0;
    port_status->port_down_count = 0;
    return mesa_port_counters_clear(NULL, port_no);
}

#if defined(VTSS_SW_OPTION_I2C)
/* Try to complete a SGMII-CISCO, to predict if the PCS should be configured to that mode
 *
 * TODO:
 * We observed that the dual media ports cannot coming link-up satus on Serval-1 Ref. board
 * when Copper module(EXCOM SFP-T, 1000Base-T) is inserted before the DUT booting.
 * The predicted method doesn't work unless the port_sfp_sgmii_set() is called before.
 * Otherwise, we may get the different result between booting and runtime stage.
 * It works with the current software but may worth to dig it deeper.
 *
 * Note: The method doesn't wrok when the port is in power_down mode or codition 'tx_enable=0'
 */
static BOOL cisco_aneg_complete(mesa_port_no_t port_no)
{
    mesa_port_conf_t        conf, original_conf;
    mesa_port_status_t      status;
    u32                     loop = 0;

    memset(&status, 0, sizeof(mesa_port_status_t));
    if (mesa_port_conf_get(NULL, port_no, &conf) != VTSS_OK) {
        T_E("mesa_port_conf_get failed, port_no: %u", port_no);
        return FALSE;
    }
    original_conf = conf;

    if (conf.power_down ||
        (is_port_phy(port_no) && port.config[VTSS_ISID_LOCAL][port_no].dual_media_fiber_speed == MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED)) {
        // Powered down or tx_enable = 0, aneg does not work, BZ#20997
        return FALSE;
    }

    // Apply 'SGMII_CISCO' and then get aneg_complete status
    conf.if_type = MESA_PORT_INTERFACE_SGMII_CISCO;
    conf.speed = MESA_SPEED_1G;
    conf.fdx = 1;

    if (mesa_port_conf_set(NULL, port_no, &conf) != VTSS_OK) {
        T_E("mesa_port_conf_set failed, port_no: %u", port_no);
        return FALSE;
    }
    while (loop < 10) {
        VTSS_OS_MSLEEP(10); // Wait for mesa_port_conf_set(), it must be sleeped one time at least
        if (mesa_port_status_get(NULL, port_no, &status) != VTSS_OK) {
            T_E("mesa_port_status_get, port_no: %u", port_no);
            break;
        }
        T_IG(TRACE_GRP_SFP, "loop: %u, status.aneg_complete: %d", loop, status.aneg_complete);
        if (status.aneg_complete) {
            break;
        }
        loop++;
    }

    /* The function is just used to predict if the PCS should be configured to 'SGMII_CISCO' mode
     * Restore the original setting here and the port thread will handle the correct setting later.
     */
    (void)mesa_port_conf_set(NULL, port_no, &original_conf);

    T_DG(TRACE_GRP_SFP, "status.aneg_complete:%d", status.aneg_complete);
    return status.aneg_complete;
}

static BOOL port_sfp_sgmii_set(mesa_port_no_t port_no, BOOL *phy_present)
{
    u16 i, reg2, reg3;

    // Read PHY ID registers
    *phy_present = FALSE;
    for (i = 0; i < 10; i++) {
        if (port_sfp_phy_read(port_no, 2, &reg2) == MESA_RC_OK &&
            port_sfp_phy_read(port_no, 3, &reg3) == MESA_RC_OK) {
            *phy_present = TRUE;
            break;
        }
        VTSS_MSLEEP(50); // Wait while the SFP module wakes up
    }

    /* BZ#22772: If Cu-SFP is default using SGMII mode, we don't need to setup PHY
                 Else this Cu-SFP must be set to SGMII mode via I2C. However,
                 this kind of configuration is not standard and implemented by different SFP manufacturers.
                 Currently, we only set it for Marvell 88EE1111. */
    if (*phy_present == FALSE || reg2 != 0x0141 || (reg3 & 0xfff0) != 0x0cc0) {
        /* The PHY is not Marvell 88E1111 */
        T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "Not Marvell 88E1111");
        return FALSE;
    }

    /* The SFP's PHY supports SGMII(at present, only Marvell 88E1111 is supported), execute follwoing setting to configure to SGMII mode */
    T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "Setting SGMII MODE");
    for (i = 0; i < 10; i++) {
        if (port_sfp_phy_write(port_no, 27, 0x9084) == VTSS_RC_OK && // SGMII mode
            port_sfp_phy_write(port_no,  9, 0x0f00) == VTSS_RC_OK && // Advertise 1000BASE-T Full/Half-Duplex
            port_sfp_phy_write(port_no,  0, 0x8140) == VTSS_RC_OK && // Apply Software reset
            port_sfp_phy_write(port_no,  4, 0x0de1) == VTSS_RC_OK && // Advertise 10/100BASE-T Full/Half-Duplex
            port_sfp_phy_write(port_no,  0, 0x9140) == VTSS_RC_OK) { // Apply Software reset
            return TRUE;
        }
        VTSS_MSLEEP(50); // Wait while the SFP module wakes up
    }
    T_NG_PORT(TRACE_GRP_SFP, port_no, "%s", "Failed to configure to SGMII mode via I2C");
    return FALSE;
}

/* The size of destination array argument should be atleast (len + 1) */
static void sfp_strncpy(char *dest, u8 *rom, u32 len)
{
    int i, c, replace = '\0';

    for (i = len - 1; i >= 0; i--) {
        c = rom[i];

        if (isprint(c) && (c != ' ')) {
            replace = ' ';
            dest[i] = c;
        } else {
            dest[i] = replace;
        }
    }
    dest[len] = '\0';
}

/* The function detects the SFP through standard SFP EEPROM    */
/* The detection process is according to the SFP MSA agreement */
/**
 * \brief Function that determines the SFP type by reading the SFP ROM via i2c
 * \param port_no [IN]        The port number at which to detect SFP
 * \param ptr_to_i2c_rd_function [IN]   Pointer to the i2c read function to be used to read the SFP ROM
 * \return SFP type.
*/
static mesa_rc sfp_detect(mesa_port_no_t port_no, vtss_appl_sfp_t *sfp, BOOL *sgmii_cisco, BOOL *approved, BOOL cisco_to_1000x_allow)
{
    u8   i, sfp_rom[88], rom_10g, rom_eth, rom_speed;
    BOOL phy_present;

    memset(sfp, 0, sizeof(*sfp));

    /* Some SPFs require delay when hot-plugged in */
    for (i = 10; i != 0; i--) {
        if ((meba_sfp_i2c_xfer(board_instance, port_no, false, 0x50, 0, &sfp_rom[0], 88, FALSE) == VTSS_RC_OK)) {
            break;
        }
        T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "Could not read from I2C");
        VTSS_OS_MSLEEP(100);
    }
    if (i == 0) {
        return VTSS_RC_OK;
    }

    T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "I2C read succes, sfp_rom:");
    T_DG_HEX(TRACE_GRP_SFP, sfp_rom, 88);

    /* Get the Vendor codes */
    sfp_strncpy(sfp->vendor_name, &sfp_rom[20], 16);
    sfp_strncpy(sfp->vendor_pn, &sfp_rom[40], 16);
    sfp_strncpy(sfp->vendor_rev, &sfp_rom[56], 4);
    sfp_strncpy(sfp->vendor_sn, &sfp_rom[68], 16);

    T_DG_PORT(TRACE_GRP_SFP, port_no, "Vendor: %s", sfp->vendor_name);

    /* Is it a SFP module? */
    if (sfp_rom[0] != 0x03) {
        T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "sfp_rom = PORT_SFP_NONE");
        return VTSS_RC_OK;
    }

    rom_10g = sfp_rom[3];    // 10G Ethernet Compliance Codes
    rom_eth = sfp_rom[6];    // Ethernet Compliance Codes
    rom_speed = sfp_rom[12]; // Nominal speed [100MBd]
    T_DG_PORT(TRACE_GRP_SFP, port_no, "rom_10g: 0x%02x, rom_eth: 0x%02x, rom_speed: %u", rom_10g, rom_eth, rom_speed);

    /* Is it a SFP+ DAC (Cu) passive module? Work-around for AQUANTIA 10G CuSFP */
    if (((sfp_rom[8] & SFP_MSA_SFP_PLUS_PASSIVE) && rom_speed >= 100) || !strncmp(sfp->vendor_name, "AQUANTIA", 8)) {
        sfp->type = VTSS_APPL_PORT_SFP_10G_DAC;
    } else if (rom_speed >= 100 && (rom_10g & 0xf0) > 0) {
        /* SFP+ optical active module */
        if (rom_10g & SFP_MSA_10GBASE_ER) {
            sfp->type = VTSS_APPL_PORT_SFP_10G_ER;
        } else if (rom_10g & SFP_MSA_10GBASE_LRM) {
            sfp->type = VTSS_APPL_PORT_SFP_10G_LRM;
        } else if (rom_10g & SFP_MSA_10GBASE_LR) {
            sfp->type = VTSS_APPL_PORT_SFP_10G_LR;
        } else {
            sfp->type = VTSS_APPL_PORT_SFP_10G_SR;
        }
    } else if (sfp_rom[2] == 0x22 || (rom_eth & SFP_MSA_1000BASE_T)) {
        /* 0x22 indicates RJ45 connector type, */
        T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "sfp_rom = PORT_SFP_1000BASE_T");
        sfp->type = VTSS_APPL_PORT_SFP_1000BASE_T;

        *sgmii_cisco = port_sfp_sgmii_set(port_no, &phy_present);
        if (phy_present) {
            // Call cisco_aneg_complete() to predict if the PCS should be configured to 'SGMII_CISCO' mode
            if (*sgmii_cisco == FALSE) {
                *sgmii_cisco = cisco_aneg_complete(port_no);
            }
            T_DG_PORT(TRACE_GRP_SFP, port_no, "host side Aneg is %s mode", *sgmii_cisco ? "SGMII_CISCO" : "1000BASE-X");
        } else {
            T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "no PHY there");
        }

        if (is_port_phy(port_no) && !*sgmii_cisco && cisco_to_1000x_allow) {
            // Not a SGMII interface, then we must run BASE-X in order to get PHY pass through to work
            sfp->type = VTSS_APPL_PORT_SFP_1000BASE_X;
        }
        T_IG_PORT(TRACE_GRP_SFP, port_no, "sgmii_cisco: %u, type: %s", *sgmii_cisco, sfp_if2txt(sfp->type));
    } else if (rom_eth & SFP_MSA_1000BASE_SX) {
        sfp->type = VTSS_APPL_PORT_SFP_1000BASE_SX;
    } else if (rom_eth & SFP_MSA_1000BASE_LX) {
        sfp->type = VTSS_APPL_PORT_SFP_1000BASE_LX;
    } else if (rom_eth & SFP_MSA_1000BASE_CX) {
        sfp->type = VTSS_APPL_PORT_SFP_1000BASE_CX;
    } else if (rom_eth & SFP_MSA_100BASE_LX) {
        sfp->type = VTSS_APPL_PORT_SFP_100BASE_LX;
    } else if (rom_eth & SFP_MSA_100BASE_FX) {
        sfp->type = VTSS_APPL_PORT_SFP_100FX;
    } else if (rom_eth == 0 || (rom_eth & (SFP_MSA_BASE_BX10 | SFP_MSA_BASE_PX))) {
        /* It does not support Ethernet Compliance Codes. Find the max supported signaling rate */

        // Table 4-4 in SFF-8472
        T_DG_PORT(TRACE_GRP_SFP, port_no, "sfp_rom:0x%X, 0x%X, 0x%X, 0x%X, 0x%X, 0x%X",
                  rom_speed, sfp_rom[14], sfp_rom[15], sfp_rom[16], sfp_rom[17], sfp_rom[18]);

        if (rom_speed == 0) {
            /* Ignore (PORT_SFP_NONE) */
        } else if (rom_speed == 1 && sfp_rom[14] == 0x50 && sfp_rom[15] == 0xFF) {
            // This is a special SFP which is not defined in SFF-8472, but is requested by a customer. See bugzilla#E2020
            sfp->type = VTSS_APPL_PORT_SFP_100BASE_ZX;
        } else if (rom_speed == 0x1 && sfp_rom[14] == 0x0 && sfp_rom[15] == 0x0  && sfp_rom[16] == 0xC8 && sfp_rom[17] == 0xC8 && sfp_rom[18] == 0x0) {
            // This is a special SFP which is not defined in SFF-8472, but is requested by a customer. See bugzilla#E2146
            sfp->type = VTSS_APPL_PORT_SFP_100FX;
        } else if (rom_speed < 10) {
            sfp->type = VTSS_APPL_PORT_SFP_100BASE_LX;
        } else if (rom_speed < 25) {
            sfp->type = VTSS_APPL_PORT_SFP_1000BASE_X;
        } else if (rom_speed < 50) {
            sfp->type = VTSS_APPL_PORT_SFP_2G5;
        } else if (rom_speed < 100)  {
            sfp->type = VTSS_APPL_PORT_SFP_5G;
        } else {
            sfp->type = VTSS_APPL_PORT_SFP_10G;
        }
    }
    T_DG_PORT(TRACE_GRP_SFP, port_no, "sfp_rom = %s", sfp_if2txt(sfp->type));

    return VTSS_RC_OK;
}

/* Detect the SFP and convert to 'mesa_port_interface_t' type */
static mesa_rc sfp_detect_if(mesa_port_no_t port_no, mesa_port_interface_t *sfp_if, BOOL cisco_to_1000x_allow)
{
    vtss_appl_sfp_t sfp;
    BOOL cisco_aneg=0, approved=1;
    meba_port_cap_t cap;
    vtss_appl_port_status_t *status = &port.status[VTSS_ISID_LOCAL][port_no];

    /* Determine SFP type. If the port is a PHY port we expect that the PHYs i2c connection is used. */
    if (sfp_detect(port_no, &sfp, &cisco_aneg, &approved, cisco_to_1000x_allow) != VTSS_RC_OK) {
        T_E("SFP detect failed port_no:%u",port_no);
    }

    T_IG_PORT(TRACE_GRP_SFP, port_no, "sfp_type:%s, cisco_aneg:%d", sfp_if2txt(sfp.type), cisco_aneg);

    /* Check if the SFP supports CISCO-SGMII-ANEG on the host side */
    if (cisco_aneg) {
        *sfp_if = MESA_PORT_INTERFACE_SGMII_CISCO;
    } else if ((!PORT_PHY_IS_INT_AMS(port_no))  && is_port_phy(port_no)) { // Do not change sfp_if for PHY ports that are not CU-SFPs (Pass through mode)
        *sfp_if = port_mac_interface(port_no);
    } else {
        /* Convert from SFP type to the appropriate mesa_port_interface_t type  */
        T_IG_PORT(TRACE_GRP_SFP, port_no, "sfp_type:%s, cisco_aneg:%d", sfp_if2txt(sfp.type), cisco_aneg);
        cap = port_custom_table[port_no].cap;
        switch (sfp.type) {
        case VTSS_APPL_PORT_SFP_NOT_SUPPORTED:
            *sfp_if = MESA_PORT_INTERFACE_NO_CONNECTION;
            break;
        case VTSS_APPL_PORT_SFP_100FX:
        case VTSS_APPL_PORT_SFP_100BASE_BX10:
        case VTSS_APPL_PORT_SFP_100BASE_T:
        case VTSS_APPL_PORT_SFP_100BASE_LX:
        case VTSS_APPL_PORT_SFP_100BASE_ZX:
            *sfp_if = MESA_PORT_INTERFACE_100FX;
            break;
        case VTSS_APPL_PORT_SFP_2G5:
        case VTSS_APPL_PORT_SFP_5G:
            *sfp_if = (cap & MEBA_PORT_CAP_2_5G_FDX ? MESA_PORT_INTERFACE_VAUI : MESA_PORT_INTERFACE_SERDES);
            break;
        case VTSS_APPL_PORT_SFP_10G:
        case VTSS_APPL_PORT_SFP_10G_SR:
        case VTSS_APPL_PORT_SFP_10G_LR:
        case VTSS_APPL_PORT_SFP_10G_LRM:
        case VTSS_APPL_PORT_SFP_10G_ER:
        case VTSS_APPL_PORT_SFP_10G_DAC:
            *sfp_if = (cap & MEBA_PORT_CAP_10G_FDX ? MESA_PORT_INTERFACE_SFI :
                       cap & MEBA_PORT_CAP_2_5G_FDX ? MESA_PORT_INTERFACE_VAUI : MESA_PORT_INTERFACE_SERDES);
            break;
        default:
            // NONE/1000BASE_*
            *sfp_if = MESA_PORT_INTERFACE_SERDES;
            break;
        }
    }

    T_IG_PORT(TRACE_GRP_SFP, port_no, "update port capabilities, sfp_if: %s, sfp.type: %s", mac_if2txt(*sfp_if), sfp_if2txt(sfp.type));
    // Calculate excluded speed capabilities (speeds above 1G by default)
    cap = (MEBA_PORT_CAP_10G_FDX | MEBA_PORT_CAP_5G_FDX | MEBA_PORT_CAP_2_5G_FDX);
    switch (sfp.type) {
    case VTSS_APPL_PORT_SFP_100FX:
    case VTSS_APPL_PORT_SFP_100BASE_LX:
    case VTSS_APPL_PORT_SFP_100BASE_ZX:
        // Only 100FDX allowed
        cap |= (MEBA_PORT_CAP_1G_FDX | MEBA_PORT_CAP_100M_HDX | MEBA_PORT_CAP_10M_FDX | MEBA_PORT_CAP_10M_HDX);
        break;
    case VTSS_APPL_PORT_SFP_1000BASE_T:
        // Only 1000FDX allowed
        cap |= (MEBA_PORT_CAP_100M_FDX | MEBA_PORT_CAP_100M_HDX | MEBA_PORT_CAP_10M_FDX | MEBA_PORT_CAP_10M_HDX);
        break;
    case VTSS_APPL_PORT_SFP_1000BASE_X:
    case VTSS_APPL_PORT_SFP_1000BASE_CX:
    case VTSS_APPL_PORT_SFP_1000BASE_LX:
    case VTSS_APPL_PORT_SFP_1000BASE_SX:
        // Only speeds above 1G excluded
        break;
    default:
        cap = 0;
        break;
    }
    status->cap = (port_custom_table[port_no].cap & (0xffffffff - cap));
    sfp.status = status->sfp.status;
    status->sfp = sfp;

    T_IG_PORT(TRACE_GRP_SFP, port_no, "exclude_cap: 0x%08x, port_phy(port_no): %d, status_cap: 0x%08x",
              cap, port_phy(port_no), status->cap);
    return VTSS_RC_OK;
}
#endif /* VTSS_SW_OPTION_I2C */


/****************************************************************************/
/*  Misc support functions                                                  */
/****************************************************************************/

mesa_rc vtss_appl_ifc_desc_set(vtss_ifindex_t ifindex, const vtss_appl_port_descr_t *const ifc_des) {
    int                    len;
    vtss_ifindex_elm_t     ife;

    if (ifc_des->description == NULL) {
        return VTSS_RC_ERROR;
    }

    VTSS_RC(port_is_vtss_ifindex_valid(ifindex));
    VTSS_RC(vtss_ifindex_decompose(ifindex, &ife));
    len = strlen(ifc_des->description);
    if (len >= VTSS_APPL_PORT_IF_DESCR_MAX_LEN) {
        return VTSS_RC_ERROR;
    }

    strcpy(port_desc[ife.ordinal].description, ifc_des->description);
    return VTSS_RC_OK;
}

mesa_rc vtss_appl_ifc_desc_get(vtss_ifindex_t ifindex, vtss_appl_port_descr_t *const descr) {
    vtss_ifindex_elm_t  ife;

    VTSS_RC(port_is_vtss_ifindex_valid(ifindex));
    VTSS_RC(vtss_ifindex_decompose(ifindex, &ife));
    if (strlen(port_desc[ife.ordinal].description)) {
        strcpy(descr->description, port_desc[ife.ordinal].description);
    } else {
        strcpy(descr->description, "");
    }
    return VTSS_RC_OK;
}

// See port.h
mesa_rc vtss_port_media2conf(BOOL rj45, BOOL sfp, vtss_appl_port_conf_t *port_conf, vtss_isid_t isid, mesa_port_no_t port_no)
{
    meba_port_cap_t        cap;
    cap = port_isid_port_cap(isid, port_no);

    // User setup to dual media. That is only supported in auto negotiation mode (for the cu interface).
    if (sfp && rj45) {
        if (!(cap & (MEBA_PORT_CAP_COPPER | MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER))) {
            return VTSS_APPL_PORT_ERROR_CU_NOT_SUPPORT;
        }

        if (!(cap & MEBA_PORT_CAP_SPEED_DUAL_ANY_FIBER)) {
            return VTSS_APPL_PORT_ERROR_FIBER_NOT_SUPPORT;
        }

        // Setup dual media SFP speed, we only support auto 1G mode currently
        port_conf->speed = MESA_SPEED_AUTO;
        T_IG_PORT(TRACE_GRP_SFP, port_no, "%s", "Setting port_conf->dual_media_fiber_speed to MESA_SPEED_FIBER_AUTO ");
        port_conf->dual_media_fiber_speed = MESA_SPEED_FIBER_AUTO; // Default to auto detect
    } else if (sfp && !rj45) {
        if (!(cap & MEBA_PORT_CAP_ANY_FIBER)) { // IT's a pure copper port
            return VTSS_APPL_PORT_ERROR_FIBER_NOT_SUPPORT;
        }

        // Setup SFP only speed
        T_IG(TRACE_GRP_SFP, "speed: %d, type: %s", port_conf->speed, sfp_if2txt(port.status[VTSS_ISID_LOCAL][port_no].sfp.type));
        port_sfp_speed_adjust(port_no, port_conf);
        switch (port_conf->speed) {
        case MESA_SPEED_100M:
            port_conf->dual_media_fiber_speed = MESA_SPEED_FIBER_100FX;
            T_IG_PORT(TRACE_GRP_SFP, port_no, "port_conf->dual_media_fiber_speed:%d", port_conf->dual_media_fiber_speed);
            break;
        case MESA_SPEED_1G:
            port_conf->dual_media_fiber_speed = MESA_SPEED_FIBER_1000X;
            T_IG_PORT(TRACE_GRP_SFP, port_no, "port_conf->dual_media_fiber_speed:%d", port_conf->dual_media_fiber_speed);
            break;
        default:
            T_IG(TRACE_GRP_CONF, "Defaulting to Auto");
            port_conf->speed = MESA_SPEED_AUTO;
            port_conf->dual_media_fiber_speed = MESA_SPEED_FIBER_ONLY_AUTO;
            T_IG_PORT(TRACE_GRP_SFP, port_no, "port_conf->dual_media_fiber_speed:%d", port_conf->dual_media_fiber_speed);
            break;
        }

    } else if (!sfp && rj45) {
        // This is rj45 only
        if (!(cap & (MEBA_PORT_CAP_COPPER | MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER))) {
            return VTSS_APPL_PORT_ERROR_CU_NOT_SUPPORT;
        }
        // User has not selected SFP so disable
        port_conf->dual_media_fiber_speed = MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED;
        T_IG_PORT(TRACE_GRP_SFP, port_no, "port_conf->dual_media_fiber_speed:%d", port_conf->dual_media_fiber_speed);

    } else {
        port_conf->dual_media_fiber_speed = MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED;
    }

    T_IG(TRACE_GRP_CONF, "rj45:%d, sfp:%d, port_conf->speed:%d, port_conf->dual_media_fiber_speed:%d", rj45, sfp, port_conf->speed, port_conf->dual_media_fiber_speed);
    return VTSS_RC_OK;
}

//See port.h
mesa_rc vtss_port_conf2media(BOOL *rj45, BOOL *sfp, vtss_appl_port_conf_t *port_conf, vtss_isid_t isid, mesa_port_no_t port_no) {
    meba_port_cap_t        cap;
    cap = port_isid_port_cap(isid, port_no);

    if (cap & (MEBA_PORT_CAP_SFP_ONLY)) {
        *rj45 = FALSE;
        *sfp  = TRUE;
    } else if (port_conf->speed == MESA_SPEED_AUTO && (port_conf->dual_media_fiber_speed == MESA_SPEED_FIBER_AUTO || port_conf->dual_media_fiber_speed == MESA_SPEED_FIBER_ONLY_AUTO)) {
        *sfp  = TRUE;
        *rj45 = TRUE;
    } else if (port_conf->dual_media_fiber_speed == MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED) {
        *sfp  = FALSE;
        *rj45 = TRUE;
    } else {
        if (port_conf->speed == MESA_SPEED_AUTO) { // Autonneg = TRUE, means that we are running AMS mode
            *rj45 = TRUE;
        } else {
            *rj45 = FALSE;
        }

        *sfp  = TRUE;
    }

    T_IG(TRACE_GRP_CONF, "rj45:%d, sfp:%d, port_conf->speed:%d, port_conf->dual_media_fiber_speed:%d", *rj45, *sfp, port_conf->speed, port_conf->dual_media_fiber_speed);
    return VTSS_RC_OK;
}


//
// Converts error to printable text
//
// In : rc - The error type
//
// Retrun : Error text
//
const char *port_error_txt(mesa_rc rc)
{
    switch (rc) {
    case VTSS_APPL_PORT_ERROR_VERIPHY_RUNNING:
        return "VeriPHY still running";

    case VTSS_APPL_PORT_ERROR_PARM:
        return "Illegal parameter";

    case VTSS_APPL_PORT_ERROR_REG_TABLE_FULL:
        return "Registration table full";

    case VTSS_APPL_PORT_ERROR_REQ_TIMEOUT:
        return "Timeout on message request";

    case VTSS_APPL_PORT_ERROR_STACK_STATE:
        return "Illegal MASTER/SLAVE state";

    case VTSS_APPL_PORT_ERROR_MUST_BE_MASTER:
        return "This is not allow at slave switch. Switch must be master";

    case VTSS_APPL_PORT_ERROR_ILLEGAL_SPEED:
        return "Illegal speed for the current mode";

    case VTSS_APPL_PORT_ERROR_ILLEGAL_ADVERTISE:
        return "Illegal advertise disabling for current mode";

    case VTSS_APPL_PORT_ERROR_INVALID_MTU:
        return "MTU is outside the valid range";

    case VTSS_APPL_PORT_ERROR_FIBER_NOT_SUPPORT:
        return "Fiber not supported";

    case VTSS_APPL_PORT_ERROR_INVALID_SID:
        return "Invalid switch id";

    case VTSS_APPL_PORT_ERROR_INVALID_PORT:
        return "Port number is out of range";

    case VTSS_APPL_PORT_ERROR_CU_NOT_SUPPORT:
        return "cu not supported";

    case VTSS_APPL_PORT_ERROR_IFINDEX_NOT_PORT:
        return "Interface is not a port interface";

    case VTSS_APPL_PORT_ERROR_FLOWCONTROL:
        return "Standard and priority flowcontrol cannot both be enabled or PFC not supported";

    case VTSS_RC_OK:
        return "";
    }

    T_I("rc:%d", rc);
    return "Unknown Port error";
}

// Function for converting sfp_tranceiver_t to a printable string
// In : sfp - SFP type to print.
const char *sfp_if2txt(vtss_appl_sfp_tranceiver_t sfp)
{
    switch (sfp) {
    case VTSS_APPL_PORT_SFP_NONE:          return "None";
    case VTSS_APPL_PORT_SFP_NOT_SUPPORTED: return "Not supported";
    case VTSS_APPL_PORT_SFP_100FX:         return "100FX";
    case VTSS_APPL_PORT_SFP_100BASE_LX:    return "100LX";
    case VTSS_APPL_PORT_SFP_100BASE_ZX:    return "100ZX";
    case VTSS_APPL_PORT_SFP_100BASE_BX10:  return "100BASE_BX10";
    case VTSS_APPL_PORT_SFP_100BASE_T:     return "100BASE_T";
    case VTSS_APPL_PORT_SFP_1000BASE_BX10: return "1000BASE_BX10";
    case VTSS_APPL_PORT_SFP_1000BASE_T:    return "1000BASE_T";
    case VTSS_APPL_PORT_SFP_1000BASE_X:    return "1000BASE_X";
    case VTSS_APPL_PORT_SFP_1000BASE_SX:   return "1000BASE_SX";
    case VTSS_APPL_PORT_SFP_1000BASE_LX:   return "1000BASE_LX";
    case VTSS_APPL_PORT_SFP_1000BASE_CX:   return "1000BASE_CX";
    case VTSS_APPL_PORT_SFP_2G5:           return "2.5G";
    case VTSS_APPL_PORT_SFP_5G:            return "5G";
    case VTSS_APPL_PORT_SFP_10G:           return "10G";
    case VTSS_APPL_PORT_SFP_10G_DAC:       return "10G_DAC";
    case VTSS_APPL_PORT_SFP_10G_SR:        return "10G_SR";
    case VTSS_APPL_PORT_SFP_10G_LR:        return "10G_LR";
    case VTSS_APPL_PORT_SFP_10G_LRM:       return "10G_LRM";
    case VTSS_APPL_PORT_SFP_10G_ER:        return "10G_ER";
    default: return "?";
    }
}
static void phy_reset_init(mesa_port_no_t port_no, mesa_phy_reset_conf_t *phy_reset)
{
    if (mesa_phy_reset_get(NULL, port_no, phy_reset) != VTSS_RC_OK) {
        T_E("mesa_phy_reset_get() failed for iport = %u. Memsetting it instead", port_no);
        memset(phy_reset, 0, sizeof(*phy_reset));
    }
}

static void sw_ams_mode_update(mesa_port_no_t port_no, mesa_phy_reset_conf_t *phy_reset)
{
    vtss_appl_ams_port_t ams_mode = VTSS_APPL_AMS_COPPER_PREFERRED;
    vtss_appl_ams_port_t ams_mode_old = port.ams_mode[port_no];

    switch (phy_reset->media_if) {
    case MESA_PHY_MEDIA_IF_CU:                      /**< Copper -> SGMII Forced  */
        ams_mode = VTSS_APPL_AMS_COPPER_FORCED;
        break;
    case MESA_PHY_MEDIA_IF_FI_1000BX:               /**< Fiber -> SPF IF */
        ams_mode = VTSS_APPL_AMS_FIBER_FORCED;
        break;
    case MESA_PHY_MEDIA_IF_FI_100FX:                /**< Fiber -> SPF IF */
        ams_mode = VTSS_APPL_AMS_FIBER_FORCED;
        break;
    case MESA_PHY_MEDIA_IF_AMS_CU_PASSTHRU:         /**< AMS - Copper preferred */
    case MESA_PHY_MEDIA_IF_AMS_CU_1000BX:           /**< AMS - Copper preferred */
    case MESA_PHY_MEDIA_IF_AMS_CU_100FX:            /**< AMS - Copper preferred */
        ams_mode = VTSS_APPL_AMS_COPPER_PREFERRED;
        break;
    case MESA_PHY_MEDIA_IF_SFP_PASSTHRU:            /**< Cu SFP -> Cisco SGMII */
        ams_mode = VTSS_APPL_AMS_FIBER_FORCED;
        break;
    case MESA_PHY_MEDIA_IF_AMS_FI_PASSTHRU:         /**< AMS - Fiber preferred */
    case MESA_PHY_MEDIA_IF_AMS_FI_1000BX:           /**< AMS - Fiber preferred */
    case MESA_PHY_MEDIA_IF_AMS_FI_100FX:            /**< AMS - Fiber preferred */
        ams_mode = VTSS_APPL_AMS_FIBER_PREFERRED;
        break;
    default:
        break;
    }
    port.ams_mode[port_no] = ams_mode;
    T_NG_PORT(TRACE_GRP_AMS, port_no, "old ams_mode: %s, new: %s",
        ams_mode_old == VTSS_APPL_AMS_COPPER_PREFERRED ? "COPPER PREFERRED" :
        ams_mode_old == VTSS_APPL_AMS_FIBER_PREFERRED ? "FIBER PREFERRED" : ams_mode_old == VTSS_APPL_AMS_COPPER_FORCED ? "COPPER FORCED" :
        ams_mode_old == VTSS_APPL_AMS_FIBER_FORCED ? "FIBER FORCED" : "?",
        ams_mode == VTSS_APPL_AMS_COPPER_PREFERRED ? "COPPER PREFERRED" :
        ams_mode == VTSS_APPL_AMS_FIBER_PREFERRED ? "FIBER PREFERRED" : ams_mode == VTSS_APPL_AMS_COPPER_FORCED ? "COPPER FORCED" :
        ams_mode == VTSS_APPL_AMS_FIBER_FORCED ? "FIBER FORCED" : "?");

    /* Don't change the SFP MAC interface at the moment
     * The port thread will change the SFP MAC interface automatically by calling check_dual_ams_ports()
     */
}

// Function for setting the port_media in mesa_phy_reset_conf_t struct, according to configuration (aneg and preferred AMS)
// In: port_no - Port in question
//     phy_reset - Pointer to the struct to update
//     conf      - Pointer to current port configuration.
static void phy_fiber_media(const mesa_port_no_t port_no, mesa_phy_reset_conf_t *phy_reset, const vtss_appl_port_conf_t *conf, mesa_fiber_port_speed_t fiber_speed)
{
    BOOL gigabit = FALSE;
    vtss_appl_port_conf_t api_port_conf = *conf;
    vtss_appl_sfp_tranceiver_t sfp_type = port.status[VTSS_ISID_LOCAL][port_no].sfp.type;

    T_DG_PORT(TRACE_GRP_SFP, port_no, "fiber_speed:%d", fiber_speed);

    // Set the SFP media interface
    port_sfp_speed_adjust(port_no, &api_port_conf);
    switch (fiber_speed) {
    case MESA_SPEED_FIBER_1000X:
    case MESA_SPEED_FIBER_100FX:
        T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "phy_reset.media_if:MESA_SPEED_FIBER_1000X");
        if (is_base_t(port_no)) {
            phy_reset->media_if = MESA_PHY_MEDIA_IF_SFP_PASSTHRU;
            return;
        } else if (api_port_conf.speed == MESA_SPEED_100M) {
            // Update the value of 'fiber_speed' according to the port speed which is applied on API layer
            fiber_speed = MESA_SPEED_FIBER_100FX;
        } else {
            gigabit = TRUE;
        }
        break;

    case MESA_SPEED_FIBER_AUTO:
        T_DG_PORT(TRACE_GRP_SFP, port_no, "media_if: MESA_SPEED_FIBER_AUTO, sfp_type: %s", sfp_if2txt(sfp_type));
        if (sfp_type == VTSS_APPL_PORT_SFP_100FX ||
            sfp_type == VTSS_APPL_PORT_SFP_100BASE_LX ||
            sfp_type == VTSS_APPL_PORT_SFP_100BASE_ZX) {
            gigabit = FALSE;
        } else if (is_base_t(port_no)) {
            if (port_custom_table[port_no].cap & MEBA_PORT_CAP_DUAL_COPPER) {
                phy_reset->media_if = MESA_PHY_MEDIA_IF_AMS_CU_PASSTHRU;
            } else if (port_custom_table[port_no].cap & MEBA_PORT_CAP_DUAL_FIBER) {
                phy_reset->media_if = MESA_PHY_MEDIA_IF_AMS_FI_PASSTHRU;
            } else {
                phy_reset->media_if = MESA_PHY_MEDIA_IF_SFP_PASSTHRU;
            }
            return;
        } else {
              // We treat all others a 1000BASE-X
            gigabit = TRUE;
        }
        break;

    case MESA_SPEED_FIBER_ONLY_AUTO:
        T_DG_PORT(TRACE_GRP_SFP, port_no, "media_if: MESA_SPEED_FIBER_AUTO, sfp_type: %s", sfp_if2txt(sfp_type));
        if (sfp_type == VTSS_APPL_PORT_SFP_100FX ||
            sfp_type == VTSS_APPL_PORT_SFP_100BASE_LX ||
            sfp_type == VTSS_APPL_PORT_SFP_100BASE_ZX) {
            gigabit = FALSE;
        } else if (is_base_t(port_no)) {
            phy_reset->media_if = MESA_PHY_MEDIA_IF_SFP_PASSTHRU;
            return;
        } else {
              // We treat all others a 1000BASE-X
            gigabit = TRUE;
        }
        break;

    case MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED:
        phy_reset->media_if = MESA_PHY_MEDIA_IF_CU;
        return;

    default:
        T_E_PORT(port_no, "Unknown Fiber mode: %d", fiber_speed) ;
        return;
        break;
    }



  if (gigabit) {
    //
    // 1000-BASE-X
    //
    if (fiber_speed == MESA_SPEED_FIBER_AUTO) { // AMS mode
      if (port_custom_table[port_no].cap & MEBA_PORT_CAP_DUAL_COPPER) {
        // Cobber preferred
        phy_reset->media_if = MESA_PHY_MEDIA_IF_AMS_CU_1000BX;
      } else {
        // Fiber preferred
        phy_reset->media_if = MESA_PHY_MEDIA_IF_AMS_FI_1000BX;
      }
    } else {
      // Forced Fiber
      phy_reset->media_if = MESA_PHY_MEDIA_IF_FI_1000BX;
    }
  } else {
    //
    // 100-BASE-FX
    //
    if (fiber_speed == MESA_SPEED_FIBER_AUTO) { // AMS mode
      if (port_custom_table[port_no].cap & MEBA_PORT_CAP_DUAL_COPPER) {
        // Cobber preferred
        phy_reset->media_if = MESA_PHY_MEDIA_IF_AMS_CU_100FX;
      } else {
        // Fiber preferred
        phy_reset->media_if = MESA_PHY_MEDIA_IF_AMS_FI_100FX;
      }
    }  else {
      // Forced Fiber
      phy_reset->media_if = MESA_PHY_MEDIA_IF_FI_100FX;
    }
  }
  T_IG_PORT(TRACE_GRP_SFP, port_no, "phy_reset.media_if:%d, gigabit:%d", phy_reset->media_if, gigabit);
}


// Function for setting the SFP fiber mode for a given port (Only PHY ports).
// IN : port_no - The port for which to change fiber mode.
//      fiber_speed - The fiber speed at which the port shall be set to.
static void phy_fiber_speed_update(mesa_port_no_t port_no, mesa_fiber_port_speed_t fiber_speed) {
    mesa_phy_reset_conf_t phy_reset;
    vtss_appl_port_conf_t *conf;

    phy_reset_init(port_no, &phy_reset);
    phy_reset.mac_if = port_mac_interface(port_no);

    u8 i = port_no;
    conf = &port.config[VTSS_ISID_LOCAL][i];
    T_DG_PORT(TRACE_GRP_AMS, port_no, "fiber_speed:%s, port_conf: %s, sfp: %s, phy_reset.mac_if: %s",
              fiber_speed == MESA_SPEED_FIBER_1000X ? "1000X" :
              fiber_speed == MESA_SPEED_FIBER_100FX ? "100FX" :
              fiber_speed == MESA_SPEED_FIBER_AUTO ? "AUTO" :
              fiber_speed == MESA_SPEED_FIBER_ONLY_AUTO ? "ONLY_AUTO" :
              fiber_speed == MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED ? "Fiber disabled" : "?",
              conf->speed == MESA_SPEED_AUTO ? "AUTO" : "FORCE",
              sfp_if2txt(port.status[VTSS_ISID_LOCAL][port_no].sfp.type),
              mac_if2txt(phy_reset.mac_if));
    if (is_port_phy(port_no) || PORT_PHY_IS_INT_AMS(port_no)) {
        phy_fiber_media(port_no, &phy_reset, conf, fiber_speed);

        T_DG_PORT(TRACE_GRP_AMS, port_no, "mac_sfp_if: %d, phy_reset.media_if: %d", port.mac_sfp_if[port_no], phy_reset.media_if);

        if (PORT_PHY_IS_INT_AMS(port_no)) {
            sw_ams_mode_update(port_no, &phy_reset); // update the AMS mode for internal ports
        } else {
            port_setup(port_no, PORT_CONF_CHANGE_PHY);
            T_IG_PORT(TRACE_GRP_AMS, port_no, "phy reset, phy_reset.media_if: %d, phy_reset.media_if: %d", port.mac_sfp_if[port_no], phy_reset.media_if);
            (void)mesa_phy_reset(PHY_INST, port_no, &phy_reset); // PHY reset needed in order to change mode
        }
    }
    T_IG_PORT(TRACE_GRP_SFP,port_no, "fiber_speed:%d, speed:%d, phy_reset.media:%d", fiber_speed, conf->speed, phy_reset.media_if);
}

static BOOL sfp_detect_module(mesa_port_no_t port_no)
{
    if (port_custom_table[port_no].cap & MEBA_PORT_CAP_SFP_DETECT  ||
        port_custom_table[port_no].cap & MEBA_PORT_CAP_DUAL_SFP_DETECT) {
        T_RG_PORT(TRACE_GRP_SFP, port_no, "%s", "Detection");
        return TRUE;
    }
    T_RG_PORT(TRACE_GRP_SFP, port_no, "%s", "No detection");
    return FALSE;
}

static void notification_event(vtss_isid_t isid, mesa_port_no_t port_no, BOOL *los, BOOL event)
{
    vtss_appl_port_mib_status_t mib_status;
    vtss_ifindex_t ifindex;
    if (vtss_ifindex_from_port(isid, port_no, &ifindex) == VTSS_RC_OK) {
        memset(&mib_status, 0, sizeof(vtss_appl_port_mib_status_t));
        (void)port_status2mib(isid, port_no, &mib_status);
    }

    memset(&mib_status, 0, sizeof(vtss_appl_port_mib_status_t));
    port_status2mib(isid, port_no, &mib_status);

    /* Link down event */
    if (port.status[isid][port_no].status.link_down && event) {
        mib_status.link = 0;
    }

    /* Loss of signal event */
    if (los[port_no] && (mib_status.sfp_info.status.los == 0) && event) {
        mib_status.sfp_info.status.los = 1;
    }

    /* Updating */
    port_status_update.set(ifindex, &mib_status);
}

/* Set port configuration */
static mesa_rc port_stack_conf_set(vtss_isid_t isid, BOOL sync)
{
    mesa_port_no_t        port_no;
    vtss_appl_port_conf_t new_conf, *conf;
    u32                   user, change, port_cnt;
    port_vol_conf_t       *vol_conf;

    T_D("isid: %d", isid);

    PORT_CRIT_ENTER();
    port_cnt = (msg_switch_exists(isid) && port.isid_added[isid] ? port.port_count[isid] : 0);
    for (port_no = 0; port_no < port_cnt; port_no++) {
        new_conf = port.config[isid][port_no];
        conf = &port.config[VTSS_ISID_LOCAL][port_no];

        /* Take ISID volatile configuration into account */
        for (user = PORT_USER_STATIC; user < PORT_USER_CNT; user++) {
            vol_conf = &port.vol_conf[user][port_no];
            if (vol_conf->disable || vol_conf->disable_adm_recover)
                new_conf.admin.enable = 0;
            if (vol_conf->loop == MESA_PORT_LOOP_PCS_HOST) {
                new_conf.adv_dis |= VTSS_APPL_PORT_ADV_UP_MEP_LOOP;
            }
            if (vol_conf->oper_up) {
                new_conf.oper_up = 1;
            }
        }
        change = port_conf_change(conf, &new_conf);
        *conf = new_conf;
        T_N_PORT(port_no, "changed:%x, speed:%d", change, conf->speed);

        // Fiber speed changed - update and reset PHY
        if (change & PORT_CONF_CHANGE_FIBER) {
            phy_fiber_speed_update(port_no, conf->dual_media_fiber_speed);
        }

        if (change) {
            T_D("%s port setup on port_no: %u", sync ? "SYNC" : "ASYNC", port_no);
            if (sync) {
                port_setup(port_no, change);
            } else {
                port.setup_change[port_no] |= change;
            }
        }
    }

    // Check and store the oper_down state.  Do not call port_setup.
    // Only use the port.config[VTSS_ISID_LOCAL][port_no] storage
    for (port_no = 0; port_no < port_cnt; port_no++) {
        conf = &port.config[VTSS_ISID_LOCAL][port_no];
        conf->oper_down = 0;
        for (user = PORT_USER_STATIC; user < PORT_USER_CNT; user++) {
            vol_conf = &port.vol_conf[user][port_no];
            if (vol_conf->oper_down) {
                conf->oper_down = 1;
            }
        }
    }

    /* Leave module INIT state */
    if (port.module_state == PORT_MODULE_STATE_INIT) {
        T_I("INIT -> CONF state");
        port.module_state = PORT_MODULE_STATE_CONF;
    }

    PORT_CRIT_EXIT();

    return VTSS_OK;
}

/* Get port status */
static mesa_rc port_stack_status_get(vtss_isid_t isid)
{
    mesa_port_no_t          port_no;
    vtss_appl_port_status_t *port_status;
    mesa_port_status_t      *status, old_status;
    u32                     port_cnt;

    PORT_CRIT_ENTER();
    port_cnt = (VTSS_ISID_LEGAL(isid) && port.isid_added[isid] ? port.port_count[isid] : 0);
    for (port_no = 0; port_no < port_cnt; port_no++) {
        port_status = &port.status[isid][port_no];
        status = &port_status->status;
        old_status = *status;
        *port_status = port.status[VTSS_ISID_LOCAL][port_no];
        port.cap_valid[isid][port_no] = 1;

        /* send notification for >events< */
        notification_event(isid, port_no, port.sfp_los_int.data(), 1);

        /* Detect link down event */
        if ((!status->link || status->link_down) && old_status.link) {
            T_D("link down event on isid: %d, port_no: %u", isid, port_no);
            port.change_flags[port_no] |= PORT_CHANGE_DOWN;
        }

        /* Detect link up or link_down or speed/duplex change event */
        if (status->link && (status->link_down || !old_status.link ||
                             status->speed != old_status.speed || status->fdx != old_status.fdx)) {
            T_D("%s event on isid: %d, port_no: %u",
                old_status.link ? "speed/duplex change" : "link up", isid, port_no);
            port.change_flags[port_no] |= PORT_CHANGE_UP;
        }
    }

    for (port_no = 0; port_no < port_cnt; port_no++) {
        /* send notification for >status< */
        notification_event(isid, port_no, port.sfp_los_int.data(), 0);
    }

    PORT_CRIT_EXIT();
    T_N("exit, isid: %d", isid);

    return VTSS_OK;
}

/* Send port status reply to master */
static mesa_rc port_stack_status_reply(mesa_event_t *link_down)
{
    mesa_port_no_t port_no;
    u32            port_cnt;

    T_N("enter");

    /* Update link down status flags */
    PORT_CRIT_ENTER();
    port_cnt = port.port_count[VTSS_ISID_LOCAL];
    for (port_no = 0; port_no < port_cnt; port_no++) {
        port.status[VTSS_ISID_LOCAL][port_no].status.link_down = link_down[port_no];
    }
    PORT_CRIT_EXIT();

    /* Generate port change events */
    port_stack_status_get(VTSS_ISID_START);

    /* Clear SFP LOS interrupt flags */
    PORT_CRIT_ENTER();
    for (port_no = 0; port_no < port_cnt; port_no++) {
        port.sfp_los_int[port_no] = 0;
    }
    PORT_CRIT_EXIT();

    T_N("exit");
    return VTSS_OK;
}

/* Send VeriPHY reply to master */
static mesa_rc port_stack_veriphy_reply(void)
{
    return VTSS_OK;
}

static void port_los_interrupt_function(meba_event_t        source_id,
                                        u32                            port_no)
{
    /* Check port number */
    if (port_no >= mesa_port_cnt(NULL)) {
        T_E("illegal port_no: %u from interrupt routine (0x%x)", port_no, source_id);
        return;
    }

    T_I("Interrupt %s (%d) to port_no %u", source_id == 0 ? "SOURCE_LOS" : source_id == 1 ? "SOURCE_FLNK" : "?",source_id, port_no);

    /* hook up to the next interrupt */
    /* to achieve backpressure this should be done in the thread just before taken action (reading the actual status) on the interrupt */
    if (vtss_interrupt_source_hook_set(VTSS_MODULE_ID_PORT, port_los_interrupt_function, source_id, INTERRUPT_PRIORITY_NORMAL) != VTSS_OK)
        T_E("vtss_interrupt_source_hook_set failed");

    /* loss of signal event */
    if (source_id == MEBA_EVENT_LOS) {
        if (sfp_detect_module(port_no)) { // only for sfp los signal
            port.sfp_los_int[port_no] = true;
        }
    }

    /* Fast link interrupt only applies if the source is MEBA_EVENT_FLNK */
    if (source_id == MEBA_EVENT_FLNK) {
        /* rising edge of fast link failure detected  */
        fast_link_int[port_no] = true;
    }

    /* wake up thread */
    vtss_flag_setbits(&interrupt_wait_flag, PHY_INTERRUPT);
}

static void port_ams_interrupt_function(meba_event_t        source_id,
                                        u32                            port_no)
{
    /* hook up to the next interrupt */
    /* to achieve backpressure this should be done in the thread just before taken action (reading the actual status) on the interrupt */
    if (vtss_interrupt_source_hook_set(VTSS_MODULE_ID_PORT, port_ams_interrupt_function, MEBA_EVENT_AMS, INTERRUPT_PRIORITY_NORMAL) != VTSS_OK)
        T_E("vtss_interrupt_source_hook_set failed");

    /* wake up thread */
    vtss_flag_setbits(&interrupt_wait_flag, PHY_INTERRUPT);
}

/****************************************************************************/
/*  Port interface                                                          */
/****************************************************************************/

void port_phy_wait_until_ready(void)
{
    /* All PHYs have been reset when the port module is ready */
    PORT_CRIT_ENTER();
    PORT_CRIT_EXIT();
}

/* Get port capability */
mesa_rc port_cap_get(mesa_port_no_t port_no, meba_port_cap_t *cap)
{
    /* Check port number */
    if (port_no >= mesa_port_cnt(NULL)) {
        return VTSS_APPL_PORT_ERROR_PARM;
    }

    *cap = port_custom_table[port_no].cap;

    return VTSS_OK;
}

/* Get local port information */
mesa_rc port_info_get(mesa_port_no_t port_no, port_info_t *info)
{
    T_N("enter, port_no: %u", port_no);

    /* Check port number */
    if (port_no >= mesa_port_cnt(NULL)) {
        T_E("illegal port_no: %u", port_no);
        return VTSS_APPL_PORT_ERROR_PARM;
    }

    PORT_CRIT_ENTER();
    port_status2info(VTSS_ISID_LOCAL, port_no, info);
    PORT_CRIT_EXIT();
    T_N("exit");
    return VTSS_OK;
}

/* Determine if port and ISID are valid */
static mesa_rc port_isid_port_no_valid(vtss_isid_t isid, mesa_port_no_t port_no)
{
    /* Check ISID */
    if (isid >= VTSS_ISID_END){
        T_I("illegal isid: %u", isid);
        return VTSS_APPL_PORT_ERROR_INVALID_SID;
    }

    /* Check port number */
    if (port_no >= port.port_count[isid]) {
        T_I("illegal port_no: %u, isid: %u", port_no, isid);
        return VTSS_APPL_PORT_ERROR_INVALID_PORT;
    }

    return VTSS_RC_OK;
}

/* Determine if ifindex valid */
mesa_rc port_is_vtss_ifindex_valid(vtss_ifindex_t ifindex) {
    if (!vtss_ifindex_is_port(ifindex)) {
        return VTSS_APPL_PORT_ERROR_IFINDEX_NOT_PORT;
    }

    // Check that port and isid are within valid range.
    vtss_ifindex_elm_t ife;
    VTSS_RC(vtss_ifindex_decompose(ifindex, &ife));
    VTSS_RC(port_isid_port_no_valid(ife.isid, ife.ordinal));

    return VTSS_RC_OK;
}

// Function returning the board type for a given isid
// In : isid - The switch id for which to return the board type.
// Return - Board type.
vtss_board_type_t port_isid_info_board_type_get(vtss_isid_t isid) {
    return (vtss_board_type_t)port.board_type[isid];
}

mesa_rc port_isid_info_get(vtss_isid_t isid, port_isid_info_t *info)
{
    mesa_port_no_t port_no;

    T_N("enter, isid: %u", isid);

    VTSS_RC(port_isid_port_no_valid(isid, 0));

    info->port_count = port.port_count[isid];
    info->board_type = port.board_type[isid];
    info->stack_port_0 = port_no_stack(0);
    info->stack_port_1 = port_no_stack(1);

    info->cap = 0;
    for (port_no = 0; port_no < port.port_count[isid]; port_no++) {
        info->cap |= port_isid_port_cap(isid, port_no);
    }
    return VTSS_RC_OK;
}

mesa_rc port_isid_port_info_get(vtss_isid_t isid, mesa_port_no_t port_no,
                                port_isid_port_info_t *info)
{
    vtss_appl_port_status_t *status;

    VTSS_RC(port_isid_port_no_valid(isid, 0));

    status = &port.status[isid][port_no];
    info->cap = status->cap;
    info->chip_no = status->chip_no;
    info->chip_port = status->chip_port;
    return VTSS_OK;
}

u32 port_isid_port_count(vtss_isid_t isid)
{
    return (isid < VTSS_ISID_END ? port.port_count[isid] : 0);
}

BOOL port_isid_port_no_is_stack(vtss_isid_t isid, mesa_port_no_t port_no)
{
    return FALSE;
}


// Function for determine is a port is a front port.
// In : isid - The switch id for the switch to check.
//    : port_no - Port number to check.
// Return : TRUE if the port at the given switch is a front port else FALSE
BOOL port_isid_port_no_is_front_port(vtss_isid_t isid, mesa_port_no_t port_no)
{
    // Check if port is within the number of ports for the switch and that it isn't a stack port.
    return ((port_isid_port_count(isid) > port_no) &&
            port.isid_added[isid] &&
            !port_isid_port_no_is_stack(isid, port_no) &&
            (port.status[isid][port_no].cap != 0));
}

u32 port_count_max(void)
{
    u32         port_count = 0, count;
    vtss_isid_t isid;

    if (msg_switch_is_master()) {
        for (isid = VTSS_ISID_START; isid < VTSS_ISID_END; isid++) {
            count = port.port_count[isid];
            if (msg_switch_exists(isid) && count > port_count)
                port_count = count;
        }
    } else {
        port_count = port.port_count[VTSS_ISID_LOCAL];
    }
    return port_count;
}

mesa_rc switch_iter_init(switch_iter_t *sit, vtss_isid_t isid, switch_iter_sort_order_t sort_order)
{
    T_NG(TRACE_GRP_ITER, "enter: isid: %u, sort_order: %d", isid, sort_order);

    if (sit == NULL) {
        T_E("Invalid sit");
        return VTSS_INVALID_PARAMETER;
    }

    memset(sit, 0, sizeof(*sit)); // Initialize this before checking for errors. This will enable getnext() to work as expected.

    if ((isid != VTSS_ISID_GLOBAL) && !VTSS_ISID_LEGAL(isid)) {
        T_E("Invalid isid:%d", isid);
        return VTSS_INVALID_PARAMETER;
    }

    if (sort_order > SWITCH_ITER_SORT_ORDER_END) {
        T_E("Invalid sort_order");
        return VTSS_INVALID_PARAMETER;
    }

    sit->m_order = sort_order;

    if (isid == VTSS_ISID_GLOBAL) {
        switch (sort_order) {
        case SWITCH_ITER_SORT_ORDER_USID:
        case SWITCH_ITER_SORT_ORDER_USID_CFG:
            {
                u32 exists_mask = sort_order == SWITCH_ITER_SORT_ORDER_USID_CFG ? msg_configurable_switches() : msg_existing_switches();
                vtss_usid_t s;
                for (s = VTSS_USID_START; s < VTSS_USID_END; s++) {
                    if (exists_mask & (1LU << topo_usid2isid(s))) {
                        sit->remaining++;
                        sit->m_switch_mask |= 1LU << s;
                        sit->m_exists_mask |= 1LU << s;
                        T_DG(TRACE_GRP_ITER, "add usid %u to m_xxxxx_mask", s);
                    }
                }
                break;
            }
        case SWITCH_ITER_SORT_ORDER_ISID:
        case SWITCH_ITER_SORT_ORDER_ISID_CFG:
        case SWITCH_ITER_SORT_ORDER_ISID_ALL:
            {
                u32 exists_mask = sort_order == SWITCH_ITER_SORT_ORDER_ISID_CFG ? msg_configurable_switches() : msg_existing_switches();
                vtss_isid_t s;
                for (s = VTSS_ISID_START; s < VTSS_ISID_END; s++) {
                    BOOL exists = (exists_mask & (1LU << s)) != 0;
                    if ((sort_order == SWITCH_ITER_SORT_ORDER_ISID_ALL) || exists) {
                        sit->remaining++;
                        sit->m_switch_mask |= 1LU << s;
                        T_DG(TRACE_GRP_ITER, "add isid %u to m_switch_mask", s);
                        if (exists) {
                            sit->m_exists_mask |= 1LU << s;
                            T_DG(TRACE_GRP_ITER, "add isid %u to m_exists_mask", s);
                        }
                    }
                }
                break;
            }
        } /* switch */
    } else {
        BOOL exists = FALSE;
        switch (sort_order) {
        case SWITCH_ITER_SORT_ORDER_USID:
        case SWITCH_ITER_SORT_ORDER_ISID:
            exists = msg_switch_exists(isid);
            break;
        case SWITCH_ITER_SORT_ORDER_USID_CFG:
        case SWITCH_ITER_SORT_ORDER_ISID_CFG:
            exists = msg_switch_configurable(isid);
            break;
        case SWITCH_ITER_SORT_ORDER_ISID_ALL:
            exists = TRUE;
        }
        if (exists) {
            sit->remaining = 1;
            sit->m_switch_mask = 1LU << isid;
            T_DG(TRACE_GRP_ITER, "add isid %u to m_switch_mask", isid);
        }
    }
    T_NG(TRACE_GRP_ITER, "exit");
    return VTSS_OK;
}

BOOL switch_iter_getnext(switch_iter_t *sit)
{
    if (sit == NULL) {
        T_E("Invalid sit");
        return FALSE;
    }

    T_NG(TRACE_GRP_ITER, "enter %d", sit->m_state);

    switch (sit->m_state) {
    case SWITCH_ITER_STATE_FIRST:
    case SWITCH_ITER_STATE_NEXT:
        if (sit->m_switch_mask) {
            // Handle the first call
            if (sit->m_state == SWITCH_ITER_STATE_FIRST) {
                sit->first = TRUE;
                sit->last = FALSE;
                sit->m_state = SWITCH_ITER_STATE_NEXT;
            } else {
                sit->first = FALSE;
            }

            // Skip non-existing switches
            while (!(sit->m_switch_mask & 1)) {
                sit->m_switch_mask >>= 1;
                sit->m_exists_mask >>= 1;
                sit->m_sid++;
            }

            // Update isid, usid and exists with info about the found switch
            if (sit->m_order == SWITCH_ITER_SORT_ORDER_USID || sit->m_order == SWITCH_ITER_SORT_ORDER_USID_CFG) {
                sit->isid = topo_usid2isid(sit->m_sid);
                sit->usid = sit->m_sid;
            } else {
                sit->isid = sit->m_sid;
                sit->usid = (sit->m_order == SWITCH_ITER_SORT_ORDER_ISID_ALL) ? 0 : topo_isid2usid(sit->m_sid);
            }
            sit->exists = (sit->m_exists_mask & 1);

            // Skip this switch
            sit->m_switch_mask >>= 1;
            sit->m_exists_mask >>= 1;
            sit->m_sid++;

            // Update the last flag
            if (!sit->m_switch_mask) {
                sit->last = TRUE;
                sit->m_state = SWITCH_ITER_STATE_LAST;
            }

            // Update the remaining counter
            if (sit->remaining) {
                sit->remaining--;
            } else {
                T_E("Internal error in remaining counter");
            }

            T_DG(TRACE_GRP_ITER, "isid %u, usid %u, first %u, last %u, exists %u, remaining %u", sit->isid, sit->usid, sit->first, sit->last, sit->exists, sit->remaining);
            return TRUE; // We have a switch
        } else {
            sit->m_state = SWITCH_ITER_STATE_DONE;
        }
        break;
    case SWITCH_ITER_STATE_LAST:
        sit->m_state = SWITCH_ITER_STATE_DONE;
        break;
    case SWITCH_ITER_STATE_DONE:
        // Fall through
    default:
        T_E("Invalid state");
    }

    T_NG(TRACE_GRP_ITER, "exit FALSE");
    return FALSE;
}

static mesa_rc port_iter_init_internal(port_iter_t *pit, switch_iter_t *sit, vtss_isid_t isid, port_iter_sort_order_t sort_order, u32 flags)
{
    u32            port_count_max;
    u32            port_count_real;
    mesa_port_no_t iport;

    T_NG(TRACE_GRP_ITER, "enter: isid: %u, sort_order: %d, flags: %02x", isid, sort_order, flags);

    if (pit == NULL) {
        T_E("Invalid pit");
        return VTSS_INVALID_PARAMETER;
    }

    memset(pit, 0, sizeof(*pit)); // Initialize this before checking for errors. This will enable getnext() to work as expected.

    if (sit) {
        if (!msg_switch_is_master()) {
            T_DG(TRACE_GRP_ITER, "Not master");
            return VTSS_UNSPECIFIED_ERROR;
        }

        if ((isid != VTSS_ISID_GLOBAL) && !VTSS_ISID_LEGAL(isid)) {
            T_E("Invalid isid");
            return VTSS_INVALID_PARAMETER;
        }
    } else {
        if ((isid != VTSS_ISID_LOCAL) && !msg_switch_is_master()) {
            T_DG(TRACE_GRP_ITER, "Not master");
            return VTSS_UNSPECIFIED_ERROR;
        }

        if ((isid != VTSS_ISID_LOCAL) && !VTSS_ISID_LEGAL(isid)) {
            T_E("Invalid isid (%u)", isid);
            return VTSS_INVALID_PARAMETER;
        }
    }

    if (sort_order > PORT_ITER_SORT_ORDER_IPORT_ALL) {
        T_E("Invalid sort_order");
        return VTSS_INVALID_PARAMETER;
    }

    pit->m_sit = sit;
    pit->m_isid = isid;
    pit->m_order = sort_order;
    pit->m_flags = (port_iter_flags_t) flags;

    if (sit && (isid == VTSS_ISID_GLOBAL)) {
        pit->m_state = PORT_ITER_STATE_INIT;
        T_NG(TRACE_GRP_ITER, "exit - has sit and global");
        return VTSS_OK; // We will be called again with a 'legal' isid
    }

    port_count_max  = (sort_order == PORT_ITER_SORT_ORDER_IPORT_ALL) ? mesa_port_cnt(NULL) : port_isid_port_count(isid);
    port_count_real = port_isid_port_count(isid);

    // There is currently no difference in sorting order for iport and uport. Use same algorithm.
    for (iport = 0; iport < port_count_max; iport++) {

        // The following must be modified when we add support for other types than front and stack:
        port_iter_type_t port_type = port_isid_port_no_is_stack(isid, iport) ? PORT_ITER_TYPE_STACK : PORT_ITER_TYPE_FRONT;

        if ((flags & (1 << port_type)) == 0) {
            continue; // Port type not present in flags - skip it
        }
        if (port.status[isid][iport].status.link) { // Port is up
            if (flags & PORT_ITER_FLAGS_DOWN) {
                continue; // We only want ports that are down - skip it
            }
        } else { // Port is down
            if (flags & PORT_ITER_FLAGS_UP) {
                continue; // We only want ports that are up - skip it
            }
        }
        pit->m_port_mask |= 1LLU << iport;
        T_DG(TRACE_GRP_ITER, "add iport %u type %d to m_port_mask", iport, port_type);
        if (iport < port_count_real) {
            pit->m_exists_mask |= 1LLU << iport;
            T_DG(TRACE_GRP_ITER, "add iport %u type %d to m_exists_mask", iport, port_type);
        }
    }
    T_NG(TRACE_GRP_ITER, "exit");
    return VTSS_OK;
}

mesa_rc port_iter_init(port_iter_t *pit, switch_iter_t *sit, vtss_isid_t isid, port_iter_sort_order_t sort_order, u32 flags)
{
    // If sit != NULL then use isid = VTSS_ISID_GLOBAL no matter what isid was.
    return port_iter_init_internal(pit, sit, (sit) ? VTSS_ISID_GLOBAL : isid, sort_order, flags);
}

void port_iter_init_local(port_iter_t *pit)
{
    (void)port_iter_init(pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL);
}

void port_iter_init_local_all(port_iter_t *pit)
{
    (void)port_iter_init(pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT_ALL, PORT_ITER_FLAGS_NORMAL);
}

BOOL port_iter_getnext(port_iter_t *pit)
{
    if (pit == NULL) {
        T_E("Invalid pit");
        return FALSE;
    }

    T_NG(TRACE_GRP_ITER, "enter %d", pit->m_state);

    while (TRUE) {
        T_DG(TRACE_GRP_ITER, "state %d", pit->m_state);
        switch (pit->m_state) {
        case PORT_ITER_STATE_INIT:
            if (switch_iter_getnext(pit->m_sit)) {
                // Reinitialize the port iterator with isid from current switch
                (void) port_iter_init_internal(pit, pit->m_sit, pit->m_sit->isid, pit->m_order, pit->m_flags);
                // No return here. Take another round
            } else {
                pit->m_state = PORT_ITER_STATE_DONE;
                return FALSE;
            }
            break;
        case PORT_ITER_STATE_FIRST:
        case PORT_ITER_STATE_NEXT:
            if (pit->m_port_mask) {

                // Handle the first call
                if (pit->m_state == PORT_ITER_STATE_FIRST) {
                    pit->first = TRUE;
                    pit->last = FALSE;
                    pit->m_state = PORT_ITER_STATE_NEXT;
                } else {
                    pit->first = FALSE;
                }

                // Skip non-existing ports
                while (!(pit->m_port_mask & 1)) {
                    pit->m_port_mask >>= 1;
                    pit->m_exists_mask >>= 1;
                    pit->m_port++;
                }

                // Update iport, uport and exists with info about the found port
                pit->iport  = pit->m_port;
                pit->uport  = iport2uport(pit->m_port);
                pit->exists = (pit->m_exists_mask & 1);
                pit->link   = port.status[pit->m_isid][pit->m_port].status.link;

                // The following must be modified when we add support for other types than front and stack:
                pit->type   = port_isid_port_no_is_stack(pit->m_isid, pit->m_port) ? PORT_ITER_TYPE_STACK : PORT_ITER_TYPE_FRONT;

                // Skip this port
                pit->m_port_mask >>= 1;
                pit->m_exists_mask >>= 1;
                pit->m_port++;

                // Update the last flag
                if (!pit->m_port_mask) {
                    pit->last = TRUE;
                    pit->m_state = PORT_ITER_STATE_LAST;
                }

                T_DG(TRACE_GRP_ITER, "iport %u, uport %u, first %u, last %u, exists %u", pit->iport, pit->uport, pit->first, pit->last, pit->exists);
                return TRUE; // We have a port
            } else {
                if (pit->m_sit) {
                    pit->m_state = PORT_ITER_STATE_INIT; // Try next switch
                    // No return here. Take another round
                } else {
                    pit->m_state = PORT_ITER_STATE_DONE;
                    return FALSE;
                }
            }
            break;
        case PORT_ITER_STATE_LAST:
            if (pit->m_sit) {
                pit->m_state = PORT_ITER_STATE_INIT; // Try next switch
                // No return here. Take another round
            } else {
                pit->m_state = PORT_ITER_STATE_DONE;
                return FALSE;
            }
            break;
        case PORT_ITER_STATE_DONE:
        default:
            T_E("Invalid state:%d", pit->m_state);
            return FALSE;
        }
    }
}

/* Convert chip port to logical port */
mesa_port_no_t port_physical2logical(mesa_chip_no_t chip_no, uint chip_port, mesa_glag_no_t *glag_no)
{
    mesa_port_no_t port_no;
    *glag_no = VTSS_GLAG_NO_NONE; // Undefined aggregation number

    for (port_no = 0; port_no < port.port_count[VTSS_ISID_LOCAL]; port_no++)
        if (port_custom_table[port_no].map.chip_no == chip_no && port_custom_table[port_no].map.chip_port == (i32)chip_port)
            return port_no;

    T_E("illegal chip_port: %d", chip_port);
    return 0;
}

static mesa_rc port_module_invalid(vtss_module_id_t module_id)
{
    if (module_id < VTSS_MODULE_ID_NONE)
        return 0;

    T_E("invalid module_id: %d", module_id);
    return 1;
}

/* Port change registration */
mesa_rc port_change_register(vtss_module_id_t module_id, port_change_callback_t callback)
{
    mesa_rc             rc = VTSS_OK;
    port_change_table_t *table;
    port_change_reg_t   *reg;

    if (port_module_invalid(module_id))
        return VTSS_APPL_PORT_ERROR_PARM;

    PORT_CB_CRIT_ENTER();
    table = &port.change_table;
    if (table->count < PORT_CHANGE_REG_MAX) {
        reg = &port.change_table.reg[table->count];
        reg->callback = callback;
        reg->module_id = module_id;
        table->count++;
    } else {
        T_E("port change table full");
        rc = VTSS_APPL_PORT_ERROR_REG_TABLE_FULL;
    }
    PORT_CB_CRIT_EXIT();

    return rc;
}

mesa_rc port_change_reg_get(port_change_reg_t *entry, BOOL clear)
{
    mesa_rc           rc = VTSS_APPL_PORT_ERROR_GEN;
    int               i;
    port_change_reg_t *reg;

    PORT_CB_CRIT_ENTER();
    for (i = 0; i < port.change_table.count; i++) {
        reg = &port.change_table.reg[i];
        if (clear) {
            /* Clear all entries */
            reg->max_ticks = 0;
        } else if (entry->module_id == VTSS_MODULE_ID_NONE) {
            /* Get first */
            *entry = *reg;
            rc = VTSS_RC_OK;
            break;
        } else if (entry->module_id == reg->module_id && entry->callback == reg->callback) {
            /* Get next */
            entry->module_id = VTSS_MODULE_ID_NONE;
        }
    }
    PORT_CB_CRIT_EXIT();

    return rc;
}

/* Port global change registration */
mesa_rc port_global_change_register(vtss_module_id_t module_id,
                                    port_global_change_callback_t callback)
{
    mesa_rc                    rc = VTSS_OK;
    port_global_change_table_t *table;
    port_global_change_reg_t   *reg;

    if (port_module_invalid(module_id))
        return VTSS_APPL_PORT_ERROR_PARM;

    PORT_CB_CRIT_ENTER();
    table = &port.global_change_table;
    if (table->count < PORT_CHANGE_REG_MAX) {
        reg = &port.global_change_table.reg[table->count];
        reg->callback = callback;
        reg->module_id = module_id;
        table->count++;
    } else {
        T_E("port change table full");
        rc = VTSS_APPL_PORT_ERROR_REG_TABLE_FULL;
    }
    PORT_CB_CRIT_EXIT();

    return rc;
}

mesa_rc port_global_change_reg_get(port_global_change_reg_t *entry, BOOL clear)
{
    mesa_rc                  rc = VTSS_APPL_PORT_ERROR_GEN;
    int                      i;
    port_global_change_reg_t *reg;

    PORT_CB_CRIT_ENTER();
    for (i = 0; i < port.global_change_table.count; i++) {
        reg = &port.global_change_table.reg[i];
        if (clear) {
            /* Clear all entries */
            reg->max_ticks = 0;
        } else if (entry->module_id == VTSS_MODULE_ID_NONE) {
            /* Get first */
            *entry = *reg;
            rc = VTSS_RC_OK;
            break;
        } else if (entry->module_id == reg->module_id && entry->callback == reg->callback) {
            /* Get next */
            entry->module_id = VTSS_MODULE_ID_NONE;
        }
    }
    PORT_CB_CRIT_EXIT();

    return rc;
}

/* Port shutdown registration */
mesa_rc port_shutdown_register(vtss_module_id_t module_id, port_shutdown_callback_t callback)
{
    mesa_rc               rc = VTSS_OK;
    port_shutdown_table_t *table;
    port_shutdown_reg_t   *reg;

    if (port_module_invalid(module_id))
        return VTSS_APPL_PORT_ERROR_PARM;

    PORT_CB_CRIT_ENTER();
    table = &port.shutdown_table;
    if (table->count < PORT_SHUTDOWN_REG_MAX) {
        reg = &port.shutdown_table.reg[table->count];
        reg->callback = callback;
        reg->module_id = module_id;
        table->count++;
    } else {
        T_E("shutdown table full");
        rc = VTSS_APPL_PORT_ERROR_REG_TABLE_FULL;
    }
    PORT_CB_CRIT_EXIT();

    return rc;
}

mesa_rc port_shutdown_reg_get(port_shutdown_reg_t *entry, BOOL clear)
{
    mesa_rc             rc = VTSS_APPL_PORT_ERROR_GEN;
    int                 i;
    port_shutdown_reg_t *reg;

    PORT_CB_CRIT_ENTER();
    for (i = 0; i < port.shutdown_table.count; i++) {
        reg = &port.shutdown_table.reg[i];
        if (clear) {
            /* Clear all entries */
            reg->max_ticks = 0;
        } else if (entry->module_id == VTSS_MODULE_ID_NONE) {
            /* Get first */
            *entry = *reg;
            rc = VTSS_RC_OK;
            break;
        } else if (entry->module_id == reg->module_id && entry->callback == reg->callback) {
            /* Get next */
            entry->module_id = VTSS_MODULE_ID_NONE;
        }
    }
    PORT_CB_CRIT_EXIT();

    return rc;
}

/* Global port status update loop*/
mesa_rc port_global_status_update_register(vtss_module_id_t module_id,
                                           port_global_status_update_callback_t callback)
{
    mesa_rc                           rc = VTSS_OK;
    port_global_status_update_table_t *table;
    port_global_status_update_t       *reg;

    if (port_module_invalid(module_id))
        return VTSS_APPL_PORT_ERROR_PARM;

    PORT_CB_CRIT_ENTER();
    table = &port.global_status_update_table;
    if (table->count < PORT_STATUS_UPDATE_REG_MAX) {
        reg = &port.global_status_update_table.reg[table->count];
        reg->callback = callback;
        reg->module_id = module_id;
        table->count++;
    } else {
        T_E("shutdown table full");
        rc = VTSS_APPL_PORT_ERROR_REG_TABLE_FULL;
    }
    PORT_CB_CRIT_EXIT();

    return rc;
}

mesa_rc port_global_status_update_get(port_global_status_update_t *entry, BOOL clear)
{
    mesa_rc             rc = VTSS_APPL_PORT_ERROR_GEN;
    int                 i;
    port_global_status_update_t *reg;

    PORT_CB_CRIT_ENTER();
    for (i = 0; i < port.global_status_update_table.count; i++) {
        reg = &port.global_status_update_table.reg[i];
        if (clear) {
            /* Clear all entries */
            reg->max_ticks = 0;
        } else if (entry->module_id == VTSS_MODULE_ID_NONE) {
            /* Get first */
            *entry = *reg;
            rc = VTSS_RC_OK;
            break;
        } else if (entry->module_id == reg->module_id && entry->callback == reg->callback) {
            /* Get next */
            entry->module_id = VTSS_MODULE_ID_NONE;
        }
    }
    PORT_CB_CRIT_EXIT();

    return rc;
}

/* Port global status update callbacks */
static void port_global_status_update_event()
{
    int                      i;
    port_global_status_update_t *reg;
    vtss_tick_count_t         ticks;
    ulong                    msec;

    PORT_CB_CRIT_ENTER();
    for (i = 0; i < port.global_status_update_table.count; i++) {
        reg = &port.global_status_update_table.reg[i];
        T_R("callback, i: %d (%s)",
            i, vtss_module_names[reg->module_id]);
        ticks = vtss_current_time();
        reg->callback();
        ticks = (vtss_current_time() - ticks);
        if (ticks > reg->max_ticks)
            reg->max_ticks = ticks;
        msec = VTSS_OS_TICK2MSEC(ticks);
        T_R("callback done, i: %d (%s), " VPRIlu" msec",
            i, vtss_module_names[reg->module_id], msec);
    }
    PORT_CB_CRIT_EXIT();
}

/****************************************************************************/
/*  Volatile port configuration interface                                   */
/****************************************************************************/

static const char *port_vol_user_txt(port_user_t user)
{
    return (user == PORT_USER_STATIC ? "Static" :
            user == PORT_USER_ACL ? "ACL" :
            user == PORT_USER_THERMAL_PROTECT ? "Thermal" :
            user == PORT_USER_LOOP_PROTECT ? "Loop" :
            user == PORT_USER_MEP ? "MEP" :
            user == PORT_USER_UDLD ? "UDLD" :
            user == PORT_USER_EVC ? "EVC" :
            user == PORT_USER_PSEC ? "Port Security" :
            user == PORT_USER_ERRDISABLE ? "ErrDisable" :
            user == PORT_USER_AGGR ? "AGGR" :
            "Unknown");
}

static void port_state_log(port_user_t user, vtss_isid_t isid, mesa_port_no_t port_no, BOOL admin_state)
{
#if defined(VTSS_SW_OPTION_SYSLOG)
    char buf[128], *p = &buf[0];

    p += sprintf(p, "LINK-CHANGED: ");
    //p += sprintf(p, "Interface %s", SYSLOG_PORT_INFO_REPLACE_KEYWORD);
    p += sprintf(p, "interface 1/%d", port_no+1);
    if (user == PORT_USER_STATIC) {
        p += sprintf(p, ", changed state to administratively %s.", admin_state ? "up" : "down");
    } else {
        p += sprintf(p, ", changed state to %s (%s).", admin_state ? "up" : "down", port_vol_user_txt(user));
    }
    /* Hide this log information for bug 0002117 */
    //S_PORT_N(isid, port_no, "%s", buf);
    T_D("%s", buf);
#endif /* VTSS_SW_OPTION_SYSLOG */
}

static mesa_rc port_vol_invalid(port_user_t user, vtss_isid_t isid,
                                mesa_port_no_t port_no, BOOL config)
{
    /* Check user */
    if (user > PORT_USER_CNT || (config && user == PORT_USER_CNT)) {
        T_E("illegal user: %d", user);
        return 1;
    }

    /* Check ISID and port */
    if (isid == VTSS_ISID_LOCAL || port_isid_port_no_valid(isid, port_no) != VTSS_RC_OK) {
        T_D("illegal isid: %u or port_no: %u", isid, port_no);
        return 1;
    }

    /* Check that we are master and ISID exists */
    if (!msg_switch_is_master()) {
        T_D("not master");
        return 1;
    }

    if (!msg_switch_exists(isid)) {
        T_D("isid %d not active", isid);
        return 1;
    }

    return 0;
}

/* Get volatile port configuration */
mesa_rc port_vol_conf_get(port_user_t user, vtss_isid_t isid,
                          mesa_port_no_t port_no, port_vol_conf_t *conf)
{
    if (port_vol_invalid(user, isid, port_no, 1))
        return VTSS_APPL_PORT_ERROR_PARM;

    PORT_CRIT_ENTER();
    *conf = port.vol_conf[user][port_no];
    PORT_CRIT_EXIT();

    return VTSS_RC_OK;
}

/* Set volatile port configuration */
static mesa_rc vol_conf_set(port_user_t user, vtss_isid_t isid, mesa_port_no_t port_no, const port_vol_conf_t *conf, BOOL sync)
{
    port_vol_conf_t *vol_conf;
    BOOL            changed = 0;
    vtss_rc         rc = VTSS_RC_OK;

    if (port_vol_invalid(user, isid, port_no, 1))
        return VTSS_APPL_PORT_ERROR_PARM;

    if (conf->oper_up && conf->oper_down) {
        return VTSS_APPL_PORT_ERROR_PARM; // oper_up and oper_down cannot be enabled simulatanously
    }

    T_I("isid: %u, port_no: %u, user: %s, disable: %u, loop: %s, oper_up: %u, oper_down: %u,, disable w/ recover: %u",
        isid, port_no, port_vol_user_txt(user),
        conf->disable, conf->loop == MESA_PORT_LOOP_DISABLE ? "disabled" : "enabled",
        conf->oper_up, conf->oper_down, conf->disable_adm_recover);

    port_state_log(user, isid, port_no, !(conf->disable || conf->disable_adm_recover));

    PORT_CRIT_ENTER();
    vol_conf = &port.vol_conf[user][port_no];
    if (port_vol_conf_changed(vol_conf, conf)) {
        /* Configuration changed */
        if ((vol_conf->oper_up && conf->oper_down) || (conf->oper_up && vol_conf->oper_down)) {
            rc = VTSS_APPL_PORT_ERROR_PARM;
        } else {
            changed = 1;
            *vol_conf = *conf;
        }
    }
    PORT_CRIT_EXIT();

    return (rc == VTSS_RC_OK) ? (changed ? port_stack_conf_set(isid, sync) : VTSS_OK) : rc;
}

mesa_rc port_vol_conf_set(port_user_t user, vtss_isid_t isid, mesa_port_no_t port_no, const port_vol_conf_t *conf)
{
    return vol_conf_set(user, isid, port_no, conf, FALSE);
}

mesa_rc port_vol_conf_set_sync(port_user_t user, vtss_isid_t isid, mesa_port_no_t port_no, const port_vol_conf_t *conf)
{
    return vol_conf_set(user, isid, port_no, conf, TRUE);
}

mesa_rc vtss_port_vol_status_get(port_user_t user, vtss_isid_t isid,
                                 mesa_port_no_t port_no, port_vol_status_t *status)
{
    u32                   usr;
    vtss_appl_port_conf_t *conf;
    BOOL                  disable, oper_up, oper_down, disable_adm_recover;
    mesa_port_loop_t      loop;
    port_vol_conf_t       *vol_conf;

    if (port_vol_invalid(user, isid, port_no, 0))
        return VTSS_APPL_PORT_ERROR_PARM;

    memset(&status->conf, 0, sizeof(status->conf));
    status->user = PORT_USER_STATIC;
    strcpy(status->name, "Static");

    PORT_CRIT_ENTER();
    for (usr = PORT_USER_STATIC; usr < PORT_USER_CNT; usr++) {
        if (usr != user && user != PORT_USER_CNT)
            continue;

        vol_conf = &port.vol_conf[usr][port_no];
        if (usr == PORT_USER_STATIC) {
            conf = &port.config[isid][port_no];
            disable = (conf->admin.enable ? 0 : 1);
            disable_adm_recover = FALSE;
            loop = ((conf->adv_dis & VTSS_APPL_PORT_ADV_UP_MEP_LOOP) ? MESA_PORT_LOOP_PCS_HOST :
                    MESA_PORT_LOOP_DISABLE);
            oper_up = conf->oper_up;
            oper_down = conf->oper_down;
        } else {
            disable = vol_conf->disable;
            disable_adm_recover = vol_conf->disable_adm_recover;
            loop = vol_conf->loop;
            oper_up = vol_conf->oper_up;
            oper_down = vol_conf->oper_down;
        }

        vol_conf = &status->conf;

        /* If user matches or port has been disabled, use admin status */
        if (usr == user || (vol_conf->disable == 0 && disable) || (vol_conf->disable_adm_recover == 0 && disable_adm_recover)) {
            vol_conf->disable = disable;
            vol_conf->disable_adm_recover = disable_adm_recover;
            status->user = (port_user_t)usr;
            strcpy(status->name, port_vol_user_txt((port_user_t)usr));
        }

        /* If user matches or loop is enabled, use loop */
        if (usr == user || (vol_conf->loop == MESA_PORT_LOOP_DISABLE && vol_conf->loop != loop)) {
            vol_conf->loop = loop;
        }

        /* If user matches or operational mode is forced, use operational mode */
        if (usr == user || oper_up) {
            vol_conf->oper_up = oper_up;
        }
        if (usr == user || oper_down) {
            vol_conf->oper_down = oper_down;
        }
    }
    PORT_CRIT_EXIT();

    return VTSS_RC_OK;
}

/****************************************************************************/
/*  Management functions                                                                                                           */
/****************************************************************************/

/* Port mode text string */
const char *vtss_port_mgmt_mode_txt(mesa_port_no_t port_no, mesa_port_speed_t speed, BOOL fdx, BOOL fiber)
{
    switch (speed) {
    case MESA_SPEED_10M:
        if (fiber) {
            if (is_base_t(port_no) && port.status[VTSS_ISID_LOCAL][port_no].status.fiber) {
                if (fdx) {
                    return ("10fdx (Cu SFP)");
                }
                return ("10hdx (Cu SFP)");
            }
        }
        return (fdx ? "10fdx" : "10hdx");
    case MESA_SPEED_100M:
        if (fiber) {
            if (is_base_t(port_no) && port.status[VTSS_ISID_LOCAL][port_no].status.fiber) {
                if (fdx) {
                    return ("100fdx (Cu SFP)");
                }
                return ("100hdx (Cu SFP)");
            }
            return ("100fdx Fiber");
        } else {
            return (fdx ? "100fdx" : "100hdx");
        }
    case MESA_SPEED_1G:
        T_IG(TRACE_GRP_SFP, "copper:%d", port.status[VTSS_ISID_LOCAL][port_no].status.copper);
        if (fiber) {
            if (is_base_t(port_no) && !port.status[VTSS_ISID_LOCAL][port_no].status.copper) {
                return ("1Gfdx (Cu SFP)");
            }
            return ("1Gfdx Fiber");
        } else {
            return (fdx ? "1Gfdx" : "1Ghdx");
        }
    case MESA_SPEED_2500M:
        return (fdx ? "2.5Gfdx" : "2.5Ghdx");
    case MESA_SPEED_5G:
        return (fdx ? "5Gfdx" : "5Ghdx");
    case MESA_SPEED_10G:
        return (fdx ? "10Gfdx" : "10Ghdx");
    default:
        T_I("Speed:%d", speed);
        return "?";
    }
}

/* Fiber Port mode text string */
const char *port_fiber_mgmt_mode_txt(mesa_port_no_t port_no,mesa_fiber_port_speed_t speed, BOOL auto_neg)
{

    switch (speed) {
    case MESA_SPEED_FIBER_1000X:
        if (is_base_t(port_no)){
            return ("CU SFP");
        } else if (auto_neg) {
            return ("1000x_ams");
        } else {
            return ("1000x");
        }
    case MESA_SPEED_FIBER_100FX:
        if (auto_neg) {
            return ("100fx_ams");
        } else {
            return ("100fx");
        }
    case MESA_SPEED_FIBER_AUTO:
        return ("sfp_auto_ams");

    case MESA_SPEED_FIBER_ONLY_AUTO:
        return ("sfp_auto");

    default:
        return ("Not supported");
    }
}


/* Get port configuration from VTSS_ISID_LOCAL */
mesa_rc vtss_port_local_isid_conf_get(mesa_port_no_t port_no, vtss_appl_port_conf_t *conf) {
    PORT_CRIT_ENTER();
    *conf = port.config[VTSS_ISID_LOCAL][port_no];
    PORT_CRIT_EXIT();
    return VTSS_OK;
}

/* Get port configuration */
mesa_rc vtss_appl_port_conf_get(vtss_ifindex_t ifindex, vtss_appl_port_conf_t *conf)
{
    VTSS_RC(port_is_vtss_ifindex_valid(ifindex));

    vtss_ifindex_elm_t ife;
    VTSS_RC(vtss_ifindex_decompose(ifindex, &ife));

    PORT_CRIT_ENTER();
    *conf = port.config[ife.isid][ife.ordinal];
    PORT_CRIT_EXIT();
    T_N("exit, isid: %d, port_no: %u", ife.isid, ife.ordinal);
    return VTSS_OK;
}

mesa_rc do_phy_reset(mesa_port_no_t port_no) {

    mesa_rc rc = VTSS_OK;
    mesa_phy_reset_conf_t phy_reset;

    phy_reset_init(port_no, &phy_reset);
    phy_reset.force = MESA_PHY_FORCE_RESET;

    if (port_custom_table[port_no].cap & MEBA_PORT_CAP_COPPER) {
        phy_reset.media_if = MESA_PHY_MEDIA_IF_CU;
    } else if (port_custom_table[port_no].cap & MEBA_PORT_CAP_FIBER) {
        phy_reset.media_if = MESA_PHY_MEDIA_IF_FI_1000BX;
    } else if (port_custom_table[port_no].cap & MEBA_PORT_CAP_DUAL_COPPER) {
        phy_reset.media_if = MESA_PHY_MEDIA_IF_AMS_CU_1000BX;
    } else if (port_custom_table[port_no].cap & MEBA_PORT_CAP_DUAL_FIBER) {
        phy_reset.media_if = MESA_PHY_MEDIA_IF_AMS_FI_1000BX;
    }

/* Reset PHY */
    if (PORT_PHY_IS_INT_AMS(port_no)) {
        phy_reset.mac_if = MESA_PORT_INTERFACE_GMII;
    } else {
        phy_reset.mac_if = port_mac_interface(port_no);
    }

    phy_reset.i_cpu_en = false;
    rc = mesa_phy_reset(PHY_INST, port_no, &phy_reset);
    if (rc != VTSS_OK) {
        T_E("mesa_phy_reset failed, port_no: %u, rc:%d", port_no, rc);
    }
    return rc;
}

/* Set port configuration */
mesa_rc vtss_appl_port_conf_set(vtss_ifindex_t ifindex, vtss_appl_port_conf_t *conf)
{
    VTSS_RC(port_is_vtss_ifindex_valid(ifindex));
    vtss_ifindex_elm_t ife;
    VTSS_RC(vtss_ifindex_decompose(ifindex, &ife));

    mesa_rc           rc = VTSS_OK;
    u32               i, change;
    vtss_appl_port_conf_t *port_conf;
    meba_port_cap_t        cap, cap_org;
    mesa_port_speed_t speed = conf->speed;
    mesa_fiber_port_speed_t dual_media_fiber_speed = conf->dual_media_fiber_speed;
    BOOL              mode_ok, fdx = conf->fdx;

    T_DG(TRACE_GRP_CONF, "enter, isid: %d, port_no: %u, %s, %s mode, %d, spd:%d, adv_dis:0x%x",
         ife.isid,
         ife.ordinal,
         conf->admin.enable ? (conf->speed == MESA_SPEED_AUTO ? "auto" :
                               vtss_port_mgmt_mode_txt(ife.ordinal, speed, fdx, FALSE)) : "disabled",
         conf->flow_control ? "fc" : "drop",
         conf->max_length,
         speed,
         conf->adv_dis);

    if (conf->max_length > MESA_CAP(MESA_CAP_PORT_FRAME_LENGTH_MAX) || conf->max_length < MESA_MAX_FRAME_LENGTH_STANDARD) {
        T_IG_PORT(TRACE_GRP_CONF, ife.ordinal, "Invalid MTU:%d", conf->max_length);
        return VTSS_APPL_PORT_ERROR_INVALID_MTU;
    }

    /* Check port capabilities */
    cap = port_isid_port_cap(ife.isid, ife.ordinal);

    /* Check Flowcontrol */
    for (i = 0; i < VTSS_PRIOS; i++) {
        if (conf->pfc[i] &&
            (conf->flow_control || MESA_CAP(MESA_CAP_PORT_PFC) == 0)) {
            return VTSS_APPL_PORT_ERROR_FLOWCONTROL;
        }
    }

    // Checking Speed for fiber
    T_DG_PORT(TRACE_GRP_CONF, ife.ordinal, "fiber_speed: %s, cap:0x%X, MEBA_PORT_CAP_DUAL_FIBER_1000X:0x%X", port_fiber_mgmt_mode_txt(ife.ordinal, dual_media_fiber_speed, conf->speed == MESA_SPEED_AUTO),
            cap, MEBA_PORT_CAP_DUAL_FIBER_1000X);
    if (cap & MEBA_PORT_CAP_SPEED_DUAL_ANY_FIBER) { // Only check dual media speed if the port has dual media capability
        switch (dual_media_fiber_speed) {
        case MESA_SPEED_FIBER_1000X:
        case MESA_SPEED_FIBER_100FX:
            mode_ok = ((cap & MEBA_PORT_CAP_SPEED_DUAL_ANY_FIBER) != 0);
            T_DG_PORT(TRACE_GRP_CONF, ife.ordinal, "mode_ok:%d, VTSS_SPEED_FIBER_105A00X", mode_ok);
            break;

        case MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED:
            mode_ok = TRUE;
            T_DG_PORT(TRACE_GRP_CONF, ife.ordinal, "mode_ok:%d, MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED", mode_ok);
            break;

        case MESA_SPEED_FIBER_AUTO:
            mode_ok = TRUE;
            T_DG_PORT(TRACE_GRP_CONF, ife.ordinal, "mode_ok:%d, MESA_SPEED_FIBER_AUTO", mode_ok);
            break;

        case MESA_SPEED_FIBER_ONLY_AUTO:
            mode_ok = TRUE;
            T_DG_PORT(TRACE_GRP_CONF, ife.ordinal, "mode_ok:%d, MESA_SPEED_ONLY_AUTO", mode_ok);
            break;

        default:
            mode_ok = FALSE;
            T_DG_PORT(TRACE_GRP_CONF, ife.ordinal, "mode_ok:%d", mode_ok);
            break;
        }

        if (!mode_ok) {
            T_DG_PORT(TRACE_GRP_CONF, ife.ordinal, "Fiber mode %d not supported", dual_media_fiber_speed);
            return VTSS_APPL_PORT_ERROR_FIBER_NOT_SUPPORT;
        }
    }

    /* Ignore Aneg capability check for SFP ports (only 100fx actually) */
    if (!((cap & MEBA_PORT_CAP_SFP_DETECT) && !(cap & MEBA_PORT_CAP_AUTONEG))) {
        if (conf->speed == MESA_SPEED_AUTO && !(cap & MEBA_PORT_CAP_AUTONEG)) {
            T_DG(TRACE_GRP_CONF, "aneg not supported on port_no %u", ife.ordinal);
            return VTSS_APPL_PORT_ERROR_ILLEGAL_SPEED;
        }
    }

    if (conf->flow_control && !(cap & MEBA_PORT_CAP_FLOW_CTRL)) {
        T_DG(TRACE_GRP_CONF, "flow control not supported on port_no %u", ife.ordinal);
        return VTSS_APPL_PORT_ERROR_PARM;
    }

    // Checking Speed
    if (conf->speed != MESA_SPEED_AUTO) {
        /* Use the origanal board capabilites */
        cap_org = vtss_board_port_cap(ife.ordinal);

        if (cap_org & MEBA_PORT_CAP_NO_FORCE) {
            T_DG(TRACE_GRP_CONF, "port_no %u not support force mode", ife.ordinal);
            return VTSS_APPL_PORT_ERROR_ILLEGAL_SPEED;
        }

        switch (speed) {
        case MESA_SPEED_10G:
            mode_ok = (fdx && (cap_org & MEBA_PORT_CAP_10G_FDX));
            break;
        case MESA_SPEED_5G:
            mode_ok = (fdx && (cap_org & MEBA_PORT_CAP_5G_FDX));
            break;
        case MESA_SPEED_2500M:
            mode_ok = (fdx && (cap_org & MEBA_PORT_CAP_2_5G_FDX));
            break;
        case MESA_SPEED_1G:
            mode_ok = (fdx && (cap_org & MEBA_PORT_CAP_1G_FDX));
            break;
        case MESA_SPEED_100M:
            mode_ok = ((!fdx && (cap_org & MEBA_PORT_CAP_100M_HDX)) || (fdx && (cap_org & MEBA_PORT_CAP_100M_FDX)));
            break;
        case MESA_SPEED_10M:
            mode_ok = ((!fdx && (cap_org & MEBA_PORT_CAP_10M_HDX)) || (fdx && (cap_org & MEBA_PORT_CAP_10M_FDX)));
            break;
        default:
            mode_ok = 0;
            break;
        }

        if (!mode_ok && (!(conf->adv_dis & VTSS_APPL_PORT_ADV_UP_MEP_LOOP))) {
            T_DG(TRACE_GRP_CONF, "mode %s not supported on port_no %u, adv-dis:0x%X, mode_ok:%d", vtss_port_mgmt_mode_txt(ife.ordinal, speed, fdx, FALSE), ife.ordinal, conf->adv_dis, mode_ok);
            return VTSS_APPL_PORT_ERROR_ILLEGAL_SPEED;
        }
    } else {
        if (cap & MEBA_PORT_CAP_SFP_DETECT) {
            if (conf->adv_dis != 0) {
                T_DG(TRACE_GRP_CONF, "SFP ports only supports full aneg (port_no %u)", ife.ordinal);
                return VTSS_APPL_PORT_ERROR_ILLEGAL_ADVERTISE;
            }
        }
    }

    if (conf->exc_col_cont && !(cap & MEBA_PORT_CAP_HDX)) {
        T_DG(TRACE_GRP_CONF, "exc col not supported on port_no %u", ife.ordinal);
        return VTSS_APPL_PORT_ERROR_PARM;
    }

#ifdef VTSS_SW_OPTION_PHY_POWER_CONTROL
    if (conf->power_mode != MESA_PHY_POWER_NOMINAL) {
        if (!port_isid_phy(ife.isid, ife.ordinal) && (msg_switch_exists(ife.isid))) { // If the switch doesn't exist in the stack we assume that the PHY for the switch is green Ethernet capable.
            T_DG(TRACE_GRP_CONF, "PHY power control not supported on port_no %u", ife.ordinal);
            return VTSS_APPL_PORT_ERROR_PARM;
        }
    }
#endif /* VTSS_SW_OPTION_PHY_POWER_CONTROL */

    if (ife.isid == VTSS_ISID_LOCAL) {
        T_W("SET not allowed, isid: %d", ife.isid);
        return VTSS_APPL_PORT_ERROR_PARM;
    } else {
        if (!msg_switch_is_master()) {
            T_W("not master");
            return VTSS_APPL_PORT_ERROR_MUST_BE_MASTER;
        }
        if (!msg_switch_configurable(ife.isid)) {
            T_W("isid %d not active", ife.isid);
            return VTSS_APPL_PORT_ERROR_STACK_STATE;
        }
    }

    PORT_CRIT_ENTER();
    port_conf = &port.config[ife.isid][ife.ordinal];
    change = port_conf_change(port_conf, conf);
#if defined(VTSS_SW_OPTION_SYSLOG) && defined(VTSS_SW_OPTION_ICLI)
    if (port_conf->admin.enable != conf->admin.enable) {
        port_state_log(PORT_USER_STATIC, ife.isid, ife.ordinal, conf->admin.enable);
    }
#endif /* VTSS_SW_OPTION_SYSLOG && VTSS_SW_OPTION_ICLI */

    T_IG_PORT(TRACE_GRP_CONF, ife.ordinal, "adv_dis:0x%X", conf->adv_dis);
    if (conf->speed != MESA_SPEED_AUTO) { // If we are not running in auto negotiation mode, we don't advertise anything
        conf->adv_dis = VTSS_APPL_PORT_ADV_DIS_DUPLEX | VTSS_APPL_PORT_ADV_DIS_SPEED;
    } else {
        T_NG_PORT(TRACE_GRP_CONF, ife.ordinal, "%s", "We are in auto-neg mode.");
        // Note : When we go from forced speed to auto-neg mode all advertise bits are disabled
        if ((conf->adv_dis & VTSS_APPL_PORT_ADV_DIS_DUPLEX) == VTSS_APPL_PORT_ADV_DIS_DUPLEX) { // Check if both advertisement of hdx and fdx is disabled
            T_NG_PORT(TRACE_GRP_CONF, ife.ordinal, "It doesn't make sense not to advertise duplex, so we advertise both, conf->adv_dis:0x%X", conf->adv_dis);
            conf->adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_DUPLEX;
            T_NG_PORT(TRACE_GRP_CONF, ife.ordinal, "conf->adv_dis:0x%X", conf->adv_dis);
        }

        if ((conf->adv_dis & VTSS_APPL_PORT_ADV_DIS_SPEED) == VTSS_APPL_PORT_ADV_DIS_SPEED) { // Check if advertisement of all speed are disabled
            T_NG_PORT(TRACE_GRP_CONF, ife.ordinal, "It doesn't make sense not to advertise any speed, so we advertise all, adv_dis:0x%X", conf->adv_dis);
            conf->adv_dis &= ~VTSS_APPL_PORT_ADV_DIS_SPEED;
            T_NG_PORT(TRACE_GRP_CONF, ife.ordinal, "conf->adv_dis:0x%X", conf->adv_dis);
        }
    }

    T_DG_PORT(TRACE_GRP_CONF, ife.ordinal, "adv_dis:0x%X, spd:%d", conf->adv_dis, conf->speed);

    // Volatile users that have set the disable_adm_recover flag
    // will get it reset as soon as the end-user performs a "shutdown".
    // If at the same time, the volatile user has specified a callback,
    // he will get notified about the change. This allows the user to
    // change her own state.
    if (!conf->admin.enable) {
        u32 user;
        for (user = 0; user < PORT_USER_CNT; user++) {
            port_vol_conf_t *vol_conf = &port.vol_conf[user][ife.ordinal];

            if (vol_conf->disable_adm_recover) {
                // Only call back when a state-change occurs.
                vol_conf->disable_adm_recover = FALSE;
                if (vol_conf->on_adm_recover_clear) {
                    vol_conf->on_adm_recover_clear(ife.isid, ife.ordinal, vol_conf);
                }
            }
        }
    }

    *port_conf = *conf;
    PORT_CRIT_EXIT();

    if (change) {
        if (ife.isid == VTSS_ISID_LOCAL) {
            /* Unmanaged, the following process should the same as in port_stack_conf_set() */
            PORT_CRIT_ENTER();
            // Fiber speed changed - update and reset PHY
            if (change & PORT_CONF_CHANGE_FIBER) {
                phy_fiber_speed_update(ife.ordinal, conf->dual_media_fiber_speed);
            }

            port_setup(ife.ordinal, change);
            PORT_CRIT_EXIT();
        } else {
            /* Activate changed configuration */
            rc = port_stack_conf_set(ife.isid, TRUE);
        }
    }
    T_DG(TRACE_GRP_CONF, "isid:%d, port_no:%u, rc:0x%X", ife.isid, ife.ordinal, rc);
    return rc;
}

/* Get port status */
static mesa_rc port_status_get(vtss_isid_t isid, mesa_port_no_t port_no, vtss_appl_port_status_t *status, BOOL req)
{
    mesa_rc rc;

    T_N("enter, isid: %d, port_no: %u", isid, port_no);

    VTSS_RC(port_isid_port_no_valid(isid, port_no));

    if (isid != VTSS_ISID_LOCAL) {
        if (!msg_switch_is_master()) {
            T_W("not master");
            return VTSS_APPL_PORT_ERROR_MUST_BE_MASTER;
        } else if (!msg_switch_exists(isid)) {
            T_W("isid %d not active", isid);
            return VTSS_APPL_PORT_ERROR_STACK_STATE;
        }
    }

    rc = (req && isid != VTSS_ISID_LOCAL ? port_stack_status_get(isid) : VTSS_OK);

    PORT_CRIT_ENTER();
    *status = port.status[isid][port_no];

    T_NG_PORT(TRACE_GRP_SFP, port_no, "port.mac_sfp_if:%d", port.mac_sfp_if[port_no]);
    PORT_CRIT_EXIT();

    T_N("exit, isid: %d, port_no: %u", isid, port_no);
    return rc;
}

mesa_rc vtss_appl_port_status_get(vtss_ifindex_t ifindex, vtss_appl_port_status_t *status)
{
    VTSS_RC(port_is_vtss_ifindex_valid(ifindex));
    vtss_ifindex_elm_t ife;
    VTSS_RC(vtss_ifindex_decompose(ifindex, &ife));

    return port_status_get(ife.isid, ife.ordinal, status, 1);
}

mesa_rc port_ctrl_status_get(vtss_isid_t isid, mesa_port_no_t port_no, vtss_appl_port_status_t *status)
{
    return port_status_get(isid, port_no, status, 0);
}

/* Get port counters for local isid */
mesa_rc vtss_port_local_isid_counters_get(mesa_port_no_t port_no, mesa_port_counters_t *counters)
{
    PORT_CRIT_ENTER();
    *counters = port.counters[port_no];
    PORT_CRIT_EXIT();
    return VTSS_RC_OK;
}

/* Get port counters */
mesa_rc vtss_appl_port_counters_get(vtss_ifindex_t ifindex, mesa_port_counters_t *counters)
{
    VTSS_RC(port_is_vtss_ifindex_valid(ifindex));
    vtss_ifindex_elm_t ife;
    VTSS_RC(vtss_ifindex_decompose(ifindex, &ife));

    mesa_rc rc = VTSS_OK;

    T_D("enter, isid:%d, port_no:%u", ife.isid, ife.ordinal);

    if (!msg_switch_is_master()) {
        T_W("not master");
        return VTSS_APPL_PORT_ERROR_MUST_BE_MASTER;
    } else if (!msg_switch_exists(ife.isid)) {
        T_W("isid:%d not active", ife.isid);
            return VTSS_APPL_PORT_ERROR_STACK_STATE;
    }
    rc = vtss_port_local_isid_counters_get(ife.ordinal, counters);

    T_D("exit, isid:%d, port_no:%u", ife.isid, ife.ordinal);

    return rc;
}

/* Clear port counters */
mesa_rc vtss_appl_port_counters_clear(vtss_ifindex_t ifindex)
{
    VTSS_RC(port_is_vtss_ifindex_valid(ifindex));
    vtss_ifindex_elm_t ife;
    VTSS_RC(vtss_ifindex_decompose(ifindex, &ife));

    mesa_rc rc = VTSS_OK;

    T_D("enter, isid: %d, port_no: %u", ife.isid, ife.ordinal);

    if (ife.isid != VTSS_ISID_LOCAL) {
        if (!msg_switch_is_master()) {
            T_W("not master");
            return VTSS_APPL_PORT_ERROR_MUST_BE_MASTER;
        } else if (!msg_switch_exists(ife.isid)) {
            T_W("isid %d not active", ife.isid);
            return VTSS_APPL_PORT_ERROR_STACK_STATE;
        }
    }
    rc = port_counters_clear(ife.ordinal);

    T_D("exit, isid: %d, port_no: %u", ife.isid, ife.ordinal);

    return rc;
}

/* VeriPHY state text string */
const char *vtss_port_mgmt_veriphy_txt(mesa_phy_veriphy_status_t status)
{
    const char *txt;

    switch (status) {
    case MESA_VERIPHY_STATUS_OK:
        txt = "OK";
        break;
    case MESA_VERIPHY_STATUS_OPEN:
        txt = "Open";
        break;
    case MESA_VERIPHY_STATUS_SHORT:
        txt = "Short";
        break;
    case MESA_VERIPHY_STATUS_ABNORM:
        txt = "Abnormal";
        break;
    case MESA_VERIPHY_STATUS_SHORT_A:
        txt = "Short A";
        break;
    case MESA_VERIPHY_STATUS_SHORT_B:
        txt = "Short B";
        break;
    case MESA_VERIPHY_STATUS_SHORT_C:
        txt = "Short C";
        break;
    case MESA_VERIPHY_STATUS_SHORT_D:
        txt = "Short D";
        break;
    case MESA_VERIPHY_STATUS_COUPL_A:
        txt = "Cross A";
        break;
    case MESA_VERIPHY_STATUS_COUPL_B:
        txt = "Cross B";
        break;
    case MESA_VERIPHY_STATUS_COUPL_C:
        txt = "Cross C";
        break;
    case MESA_VERIPHY_STATUS_COUPL_D:
        txt = "Cross D";
        break;
    case MESA_VERIPHY_STATUS_UNKNOWN:
        txt = "N/A";
        break;
    default:
        txt = "?";
        break;
    }
    return txt;
}


/* Start VeriPHY */
mesa_rc port_mgmt_veriphy_start(vtss_isid_t isid,
                                port_veriphy_mode_t *mode)
{
    mesa_port_no_t port_no;
    port_veriphy_t *veriphy;

    T_D("enter, isid: %d", isid);

    if (!msg_switch_is_master()) {
        return VTSS_APPL_PORT_ERROR_MUST_BE_MASTER;
    }

    VTSS_RC(port_isid_port_no_valid(isid, 0));

    PORT_CRIT_ENTER();
    for (port_no = 0; port_no < port.port_count[isid]; port_no++) {
        PORT_HAS_CAP(isid, port_no);

        veriphy = &port.veriphy[port_no];
        if (mode[port_no] != PORT_VERIPHY_MODE_NONE && port_isid_phy(isid, port_no) &&
            !veriphy->running) {
            T_I("starting veriPHY on isid %d, port_no %u", isid, port_no);
            port_veriphy_start(port_no, mode[port_no]);
        }
    }
    PORT_CRIT_EXIT();

    T_D("exit");

    return VTSS_OK;
}


/* Get VeriPHY result */
mesa_rc port_mgmt_veriphy_get(vtss_isid_t isid, mesa_port_no_t port_no,
                              mesa_phy_veriphy_result_t *result, uint timeout)
{
    mesa_rc        rc = VTSS_APPL_PORT_ERROR_VERIPHY_RUNNING;
    uint           timer;
    port_veriphy_t *veriphy;


    T_N_PORT(port_no, "enter, isid: %d", isid);

    VTSS_RC(port_isid_port_no_valid(isid, port_no));

    for (timer = 0; ; timer++) {
        veriphy = &port.veriphy[port_no];
        PORT_CRIT_ENTER();
        if (!veriphy->running) {
            *result = veriphy->result;
            T_D_PORT(port_no, "veriphy->valid =%d",veriphy->valid);
            if (veriphy->valid) {
                rc = (mesa_rc)VTSS_OK;
            } else {
                rc = (mesa_rc)VTSS_APPL_PORT_ERROR_GEN;
                result->status[0] = MESA_VERIPHY_STATUS_UNKNOWN;
                result->status[1] = MESA_VERIPHY_STATUS_UNKNOWN;
                result->status[2] = MESA_VERIPHY_STATUS_UNKNOWN;
                result->status[3] = MESA_VERIPHY_STATUS_UNKNOWN;
            }
        } else {
            result->status[0] = MESA_VERIPHY_STATUS_RUNNING;
            result->status[1] = MESA_VERIPHY_STATUS_RUNNING;
            result->status[2] = MESA_VERIPHY_STATUS_RUNNING;
            result->status[3] = MESA_VERIPHY_STATUS_RUNNING;
        }
        PORT_CRIT_EXIT();
        if (rc != VTSS_APPL_PORT_ERROR_VERIPHY_RUNNING || timer >= timeout)
            break;
        VTSS_OS_MSLEEP(1000);
    }

    T_D("exit");

    return rc;
}

/****************************************************************************/
/*  Initialization functions                                                */
/****************************************************************************/
/* Set default volatile port configuration */
static BOOL port_vol_conf_default(vtss_isid_t isid, mesa_port_no_t port_no)
{
    u32     user;
    BOOL            changed = FALSE;
    port_vol_conf_t *vol_conf, def_conf;

    memset(&def_conf, 0, sizeof(def_conf));

    /* Clear volatile port configuration */
    for (user = PORT_USER_STATIC; user < PORT_USER_CNT; user++) {
        vol_conf = &port.vol_conf[user][port_no];
        if (port_vol_conf_changed(vol_conf, &def_conf)) {
            *vol_conf = def_conf;
            changed = TRUE;
        }
    }
    return changed;
}

/* Get defaults for local port */
mesa_rc vtss_port_conf_default(vtss_isid_t isid, mesa_port_no_t port_no, vtss_appl_port_conf_t *conf)
{
    meba_port_cap_t cap = vtss_board_port_cap(port_no);

    memset(conf, 0, sizeof(*conf));
    conf->admin.enable = 1;
    if ((cap & MEBA_PORT_CAP_10G_FDX) && !(cap & MEBA_PORT_CAP_COPPER)) {
        conf->speed = MESA_SPEED_10G;
        conf->fdx = 1;
    } else if (cap & MEBA_PORT_CAP_AUTONEG) {
        conf->speed = MESA_SPEED_AUTO;
        conf->fdx = 1;
    } else if (cap & MEBA_PORT_CAP_5G_FDX) {
        conf->speed = MESA_SPEED_5G;
        conf->fdx = 1;
    } else if (cap & MEBA_PORT_CAP_1G_FDX) {
        /* 1G is preferred over 2.5G, as 1G is standardized and supports Autoneg */
        conf->speed = MESA_SPEED_1G;
        conf->fdx = 1;
    } else if (cap & MEBA_PORT_CAP_2_5G_FDX) {
        conf->speed = MESA_SPEED_2500M;
        conf->fdx = 1;
    } else if (cap & MEBA_PORT_CAP_100M_FDX) {
        conf->speed = MESA_SPEED_100M;
        conf->fdx = 1;
    } else if (cap & MEBA_PORT_CAP_100M_HDX) {
        conf->speed = MESA_SPEED_100M;
    } else if (cap & MEBA_PORT_CAP_10M_FDX) {
        conf->speed = MESA_SPEED_10M;
        conf->fdx = 1;
    } else if (cap & MEBA_PORT_CAP_10M_HDX) {
        conf->speed = MESA_SPEED_10M;
    }

    T_D_PORT(port_no, "cap:0x%X, speed:%d", cap, conf->speed);
    if ((cap & MEBA_PORT_CAP_DUAL_SFP_DETECT) || (cap & MEBA_PORT_CAP_SPEED_DUAL_ANY_FIBER_FIXED_SPEED)) {
        T_NG_PORT(TRACE_GRP_CONF, port_no, "%s", "Setting port_conf->dual_media_fiber_speed to MESA_SPEED_FIBER_AUTO ");
        conf->dual_media_fiber_speed = MESA_SPEED_FIBER_AUTO;
    } else if (cap & MEBA_PORT_CAP_DUAL_FIBER_1000X) {
        conf->dual_media_fiber_speed = MESA_SPEED_FIBER_1000X;
    } else {
        conf->dual_media_fiber_speed = MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED;
    }

    conf->max_length = MESA_CAP(MESA_CAP_PORT_FRAME_LENGTH_MAX);
    conf->frame_length_chk = FALSE;  // Default disable frame length check per request from Bugzilla#18559
    conf->power_mode = MESA_PHY_POWER_NOMINAL;
    return VTSS_RC_OK;
}

BOOL is_phy_found(u16 model)
{
    switch(model) {
        case 0x8488:
        case 0x8489:
        case 0x8490:
        case 0x8491:
        case 0x8258:
        case 0x8257:
        case 0x8256:
            return TRUE;
            break;
        default:
            return FALSE;
    }
    return FALSE;
}

void port_10g_phy_setup(mesa_port_no_t port_no, mesa_port_list_t &port_skip)
{
    mesa_phy_10g_mode_t oper_mode;
    u16                 model=0,tmp,value=0;

    /* Skip ports already setup and ports without 10G PHYs */
    if (port_skip[port_no] ||
            !(port_custom_table[port_no].cap & MEBA_PORT_CAP_VTSS_10G_PHY) ||
            mesa_port_mmd_read(NULL, port_no, 30, 0, &model) != VTSS_RC_OK) {
        port_skip.clr(port_no);
        return;
    } else {
        /* 1Ex0293.0 bit decides whether VSC8489 has one channel */
        mesa_port_mmd_read(NULL, port_no, 30, 0x0293, &value);
    }

    if (is_phy_found(model) == FALSE) {
        if (port_custom_table[port_no+1].cap & MEBA_PORT_CAP_VTSS_10G_PHY) {
            if (mesa_port_mmd_read(NULL, port_no+1, 30, 0, &tmp) == VTSS_RC_OK) {
                /* 1Ex0293.0 bit decides whether VSC8489 has one channel */
                mesa_port_mmd_read(NULL, port_no+1, 30, 0x0293, &value);
                if (tmp == 0x8489 || tmp == 0x8490) {
                    model = tmp;
                }
            }
        }
    }

    if (model == 0x8256 || model == 0x8257 || model == 0x8258 || model == 0x8254) {
        //  Malibu initilization is handled in MEBA (meba.c).
        return;
    }

    /* Initialize setup */
    memset(&oper_mode, 0, sizeof(oper_mode));
    /* Note that WAN mode must be also be supported on the board (currently only Phy-ESTAX boards) */
    oper_mode.oper_mode = MESA_PHY_LAN_MODE;

    if (model == 0x8484 || model == 0x8487 || model == 0x8488) {
        oper_mode.xfi_pol_invert = 0;
    } else {
        oper_mode.xfi_pol_invert = 1;
    }

    if (model == 0x8489 || model == 0x8490 || model == 0x8491) {
        /* enable LOS work around for Venice */
        oper_mode.venice_rev_a_los_detection_workaround = TRUE;
        oper_mode.apc_ib_regulator = MESA_APC_IB_BACKPLANE;
        oper_mode.l_clk_src.is_high_amp = TRUE;
    }
    /* If PHY with two ports is found, the last port (channel 0) is setup first */
    if (model == 0x8484 || model == 0x8487 || model == 0x8488 || model == 0x8489 || model == 0x8490) {
        if ((model == 0x8489) && (value & VTSS_BIT(0))) {
            T_D("PHY 0x%0x is of one channel", model);
        } else {
            port_skip.set(port_no + 1);
            if (mesa_phy_10g_mode_set(PHY_INST, port_no + 1, &oper_mode) != VTSS_RC_OK) {
                T_E("mesa_phy_10g_mode_set failed, port_no %u", port_no + 1);
            }
            if(vtss_board_type() == VTSS_BOARD_JAGUAR2_REF) {
                /* If venice is found at 10gi 1/1,2 10g 1/3,4 are not used */
                port_skip.set(port_no + 2);
                port_skip.set(port_no + 3);
            }
        }
    }

    if (is_phy_found(model) != FALSE) {
        /* Setup port */
        if (mesa_phy_10g_mode_set(PHY_INST, port_no, &oper_mode) != VTSS_RC_OK) {
            T_E("mesa_phy_10g_mode_set failed, port_no %u", port_no);
        }
    }
}

static void port_fiber_speed_check(mesa_fiber_port_speed_t *fiber_speed)
{
    // MESA_SPEED_FIBER_DISABLED is obsolete, use VTSS_SPEED_FIBER_NOT_SUPPORTED (backward compatibility)
    if (*fiber_speed == MESA_SPEED_FIBER_DISABLED) {
        *fiber_speed = MESA_SPEED_FIBER_NOT_SUPPORTED_OR_DISABLED;
        T_IG(TRACE_GRP_SFP, "port_conf->dual_media_fiber_speed");
    }
}

/* Create and activate default port configuration */
static void port_create_default(vtss_isid_t isid, BOOL *changed)
{
    mesa_port_no_t        port_no;
    vtss_appl_port_conf_t *port_conf, conf;

    T_I("enter, isid: %d", isid);

    *changed = 0;
    PORT_CRIT_ENTER();
    for (port_no = 0; port_no < port.port_count[isid]; port_no++) {
        PORT_HAS_CAP(isid, port_no);

        /* Use default values */
        vtss_port_conf_default(isid, port_no, &conf);
        if (port_vol_conf_default(isid, port_no)) {
            *changed = 1;
        }
        port_conf = &port.config[isid][port_no];

        /* Check/update fiber speed fields */
        port_fiber_speed_check(&port_conf->dual_media_fiber_speed);
        port_fiber_speed_check(&conf.dual_media_fiber_speed);

        if (port_conf_change(port_conf, &conf))
            *changed = 1;
        *port_conf = conf;
        strcpy(port_desc[port_no].description, "");
    }
    PORT_CRIT_EXIT();

    T_D("exit");
}

static BOOL read_firmware_file(const i8 *file, u8 **fw, u32 *fw_len)
{
    i32         rc;
    struct stat fw_stat;
    u32         file_size;
    if ((rc = stat(file, &fw_stat)) && errno != ENOENT) {
        perror("check FW");
        return FALSE;
    } else if (!rc) {
        i32 fd;
        size_t count;
        file_size = fw_stat.st_size;
        if ((fd = open(file, O_RDONLY))) {
            if (VTSS_MALLOC_CAST(*fw, file_size) == NULL) {
                T_E("malloc fw_len %u bytes failed", file_size);
            } else {
                if ((count = read(fd, *fw, file_size)) != file_size) {
                    T_E("read failed: %u != %u", (u32)count, (u32)file_size);
                    VTSS_FREE(*fw);
                    *fw = NULL;
                    close(fd);
                    return FALSE;
                }
                *fw_len = file_size;
                T_I("Store FW %u@%p", *fw_len, *fw);
            }
            close(fd);
        } else {
            perror("open FW");
            return FALSE;
        }
    }
    return TRUE;
}

static BOOL is_port_10g_copper_phy(mesa_port_no_t port_no)
{
    meba_port_cap_t cap = 0;
    cap = vtss_board_port_cap(port_no);

    if (((cap & (MEBA_PORT_CAP_10G_FDX | MEBA_PORT_CAP_5G_FDX)) && (cap & MEBA_PORT_CAP_COPPER))
            || ((cap & MEBA_PORT_CAP_2_5G_FDX) && (cap & MEBA_PORT_CAP_COPPER)
            && port_mac_interface(port_no) == MESA_PORT_INTERFACE_SFI)) {
        /* we support 2 kind of 2.5G PHY: one use SFI interface,
         another uses SGMII, we only care about the PHY using SFI interface here. */
        return TRUE;
    }

    return FALSE;
}

/* Initialize ports */
static void port_init_ports(void)
{
    mesa_port_no_t        port_no;
    uint                  i;
    vtss_appl_port_status_t *status;
    vtss_appl_port_conf_t conf;
    BOOL                  reset_phys = 1;
    mesa_port_list_t      port_skip, member;
    BOOL                  loop_port;
    u32                   port_count, port_max = mesa_port_cnt(NULL);
    u8                    *fw = NULL, *side_board_fw = NULL;
    u32                   fw_len = 0, side_board_fw_len = 0;
    uint32_t              mep_loop_port = MESA_CAP(VTSS_APPL_CAP_MEP_LOOP_PORT);

    T_D("enter");












    /* Release ports from reset */
    if (reset_phys) {
        if (phy_warm_start_test_enabled()) {
            if (MESA_CAP(MESA_CAP_PHY_10G)) {
                /* Malibu work around */
                phy_inst_create_after_malibu_detect();
            }
        }
        meba_reset(board_instance, MEBA_PORT_RESET);
    }

    /* Initialize all ports */
    port_count = port.port_count[VTSS_ISID_LOCAL];

    /* Remove loop ports from default VLAN before we potentially enable forwarding */
    if (mesa_vlan_port_members_get(NULL, VTSS_VID_DEFAULT, &member) == VTSS_RC_OK) {
        for (port_no = port_count; port_no < port_max; port_no++) {
            member[port_no] = 0;
        }
        (void)mesa_vlan_port_members_set(NULL, VTSS_VID_DEFAULT, &member);
    }

    if (mep_loop_port != MESA_PORT_NO_NONE) {
        /* Include MEP loop port */
        port_count++;
    }
#if defined(VTSS_SW_OPTION_MIRROR_LOOP_PORT)
    /* Include mirror loop port */
    port_count++;
#endif /* VTSS_SW_OPTION_MIRROR_LOOP_PORT */

    (void) read_firmware_file(AQR_24_FW, &fw, &fw_len);
    (void) read_firmware_file(AQR_4_FW, &side_board_fw, &side_board_fw_len);

    for (port_no = 0; port_no < port_count && port_no < port_max; port_no++) {

        PORT_HAS_CAP(VTSS_ISID_LOCAL, port_no);


        // mesa_phy_firmware_update() can determine if firmware udpate is needed
        // according PHY ID, and firmware version. APPL just needs to blob the different firmware
        // according to the port which is in main board or daughter board.
        if (is_port_10g_copper_phy(port_no)) {
            (void)mesa_phy_firmware_update(PHY_INST, port_no, side_board_fw, &side_board_fw_len);
        } else if ((port_custom_table[port_no].cap & (MEBA_PORT_CAP_COPPER | MEBA_PORT_CAP_2_5G_TRI_SPEED_FDX))
                == (MEBA_PORT_CAP_COPPER | MEBA_PORT_CAP_2_5G_TRI_SPEED_FDX)) {
            (void)mesa_phy_firmware_update(PHY_INST, port_no, fw, &fw_len);
        }

        if (port_phy(port_no)) {
            (void)do_phy_reset(port_no);

            // Make sure we don't have any left-over PHY interrupts
            // enabled from previous build (that is, if this boot is due to
            // a firmware upgrade/ramload).
            (void)mesa_phy_event_enable_set(PHY_INST, port_no, 0xFFFFFFFF, FALSE);
        } else {
            port_10g_phy_setup(port_no, port_skip);
        }

        (void)mesa_phy_id_get(PHY_INST, port_no, &port.phy_id_1g[port_no]);

        /* Set port status down */
        i = port_no;
        status = &port.status[VTSS_ISID_LOCAL][i];
        status->status.link_down = 0;
        status->status.link = 0;
        status->status.fiber = 0;
        status->cap = port_custom_table[port_no].cap;
        status->chip_no = port_custom_table[i].map.chip_no;
        status->chip_port = port_custom_table[i].map.chip_port;
        memset(&port.aneg_status[i], 0, sizeof(mesa_port_status_t));

        /* Initialize VeriPHY status */
        port.veriphy[i].running = 0;
        port.veriphy[i].valid = 0;

        /* Setup port with default configuration */
        vtss_port_conf_default(VTSS_ISID_LOCAL, port_no, &conf);
        loop_port = 0;
        if (mep_loop_port != MESA_PORT_NO_NONE) {
            if (port_no == mep_loop_port) {
 #if defined(VTSS_SW_OPTION_MEP)
                uint            max_length = (MESA_CAP(MESA_CAP_PORT_FRAME_LENGTH_MAX) + MESA_PACKET_HDR_SIZE_BYTES + 4);
                mesa_port_ifh_t ifh_conf;

                /* Ensure room for TST PDU and injection header and dummy tag */
                if (conf.max_length < max_length) {
                    conf.max_length = max_length;
                }
    
                /* Enable injection header */
                ifh_conf.ena_inj_header = TRUE;
                ifh_conf.ena_xtr_header = FALSE;
                (void)mesa_port_ifh_conf_set(NULL, port_no, &ifh_conf);
#endif /* VTSS_SW_OPTION_MEP */
                loop_port = 1;
            }
        }

#if defined(VTSS_SW_OPTION_MIRROR_LOOP_PORT)
        if (port_no == VTSS_SW_OPTION_MIRROR_LOOP_PORT) {
            mesa_learn_mode_t mode;

            /* Disable learning on loop port */
            memset(&mode, 0, sizeof(mode));
            (void)mesa_learn_port_mode_set(NULL, port_no, &mode);

            loop_port = 1;
        }
#endif /* VTSS_SW_OPTION_MIRROR_LOOP_PORT */
        if (loop_port) {
            mesa_packet_rx_port_conf_t packet_port_conf;










            /* Disable CPU copy/redirect on loop port */
            if (mesa_packet_rx_port_conf_get(NULL, port_no, &packet_port_conf) == VTSS_RC_OK) {
                int j;

                packet_port_conf.ipmc_ctrl_reg = MESA_PACKET_REG_FORWARD;
                packet_port_conf.igmp_reg = MESA_PACKET_REG_FORWARD;
                packet_port_conf.mld_reg = MESA_PACKET_REG_FORWARD;
                for (j = 0; j < 16; j++) {
                    packet_port_conf.bpdu_reg[j] = MESA_PACKET_REG_FORWARD;
                    packet_port_conf.garp_reg[j] = MESA_PACKET_REG_FORWARD;
                }
                (void)mesa_packet_rx_port_conf_set(NULL, port_no, &packet_port_conf);
            }

            /* Enable loopback for loop port and force operational mode up */
            conf.adv_dis |= VTSS_APPL_PORT_ADV_UP_MEP_LOOP;
            conf.oper_up = 1;
        }
        (void) port_vol_conf_default(VTSS_ISID_LOCAL, port_no);
        port.config[VTSS_ISID_LOCAL][i] = conf;
        port_setup(port_no, PORT_CONF_CHANGE_ALL);
    } /* Port loop */

    if (fw) {
        VTSS_FREE(fw);
    }

    if (side_board_fw) {
        VTSS_FREE(side_board_fw);
    }

    // Do post reset
    meba_reset(board_instance, MEBA_PORT_RESET_POST);

    /* Initialize port LEDs */
    meba_reset(board_instance, MEBA_PORT_LED_INITIALIZE);

    /* Open up port API only after init */
    // THIS IS THE FIRST UNLOCK OF PORT_CRIT
    T_I("Unlocking port mutex for the first time");
    PORT_CRIT_EXIT();

    T_D("exit");
}

static mesa_rc sfp_status_poll(mesa_port_list_t *sfp_status_new, BOOL *changed) {
    mesa_port_no_t p;

    VTSS_RC(meba_sfp_insertion_status_get(board_instance, sfp_status_new));
    for (p = 0; p < port.port_count[VTSS_ISID_LOCAL]; p++) {
        if ((*sfp_status_new)[p]) {
            meba_sfp_status_t sfp_status;
            if (meba_sfp_status_get(board_instance, p, &sfp_status) == VTSS_RC_OK) {
                if ((port.status[VTSS_ISID_LOCAL][p].sfp.status.los != sfp_status.los) ||
                    (port.status[VTSS_ISID_LOCAL][p].sfp.status.tx_fault != sfp_status.tx_fault) ||
                    (port.status[VTSS_ISID_LOCAL][p].sfp.status.present  != sfp_status.present) ||
                    port.sfp_los_int[p]) {
                    *changed = TRUE;
                }
                port.status[VTSS_ISID_LOCAL][p].sfp.status.present  = sfp_status.present;
                port.status[VTSS_ISID_LOCAL][p].sfp.status.los      = sfp_status.los;
                port.status[VTSS_ISID_LOCAL][p].sfp.status.tx_fault = sfp_status.tx_fault;
            }
        } else {
            // No SFP now, was it there before?
            if (port.status[VTSS_ISID_LOCAL][p].sfp.status.present) {
                *changed = TRUE;
                port.status[VTSS_ISID_LOCAL][p].sfp.status.present = FALSE;
            }
        }
    }
    return VTSS_RC_OK;
}

// Software supported Automated Media Sense,
// i.e. find out which of the dual ports should be used
static void check_dual_ams_ports(mesa_port_no_t port_no, BOOL *fiber, BOOL *copper, u32 *wtr)
{
    mesa_port_status_t      phy_status;
    vtss_appl_port_status_t *status = &port.status[VTSS_ISID_LOCAL][port_no];
    mesa_port_conf_t        port_conf;

    if (!PORT_PHY_IS_INT_AMS(port_no)) {
        return; // Not internal AMS port
    }

    if (*wtr > 0) {
        *wtr = *wtr - 1;
        return;
    }

    /* The function is used to check if the MAC interface of dual media port
     * need to change copper or fiber mode according the real connected status.
     * And then the port thread will change the SFP MAC interface automatically
     * after called this function.
     *
     * We should refer to the current MAC interface in the API layer instead of
     * refer to the database of 'port.mac_sfp_if[port_no]'. Since the value can
     * be changed after the SFP detection/re-detection process.
     */
    if (mesa_port_conf_get(NULL, port_no, &port_conf) != VTSS_OK) {
        T_E("mesa_port_conf_get failed, port_no: %u", port_no);
        return;
    }

    if (port.ams_mode[port_no] == VTSS_APPL_AMS_COPPER_FORCED &&
        port_conf.if_type != MESA_PORT_INTERFACE_SGMII) {
        *copper = 1;
        return;
    } else if (port.ams_mode[port_no] == VTSS_APPL_AMS_FIBER_FORCED &&
               port_conf.if_type == MESA_PORT_INTERFACE_SGMII) {
        *fiber = 1;
        return;
    }

    if ((port.ams_mode[port_no] == VTSS_APPL_AMS_COPPER_FORCED) ||
        (port.ams_mode[port_no] == VTSS_APPL_AMS_FIBER_FORCED ||
         port.ams_mode[port_no] == VTSS_APPL_AMS_DISABLED)) {
        return; // The internal AMS port shall not change media
    }

    T_NG_PORT(TRACE_GRP_AMS, port_no, "AMS mode is %d, link %s, fiber signal %s",
            port.ams_mode[port_no],
            status->status.link ? "UP" : "DOWN", status->sfp.status.los ? "LOST" : "DETECTED");
    if (port_phy(port_no)) {
        phy_status.link = status->status.link;
    } else {
        (void)mesa_phy_status_get(PHY_INST, port_no, &phy_status);
    }

    // Check which of the dual ports has link, start with preferred media

    if (port.ams_mode[port_no] == VTSS_APPL_AMS_FIBER_PREFERRED) { // Fiber preferred
        if (!status->sfp.status.los) {
            if (port_conf.if_type == MESA_PORT_INTERFACE_SGMII) {
                *fiber = 1;
            }
        } else if (phy_status.link) {
            if (port_conf.if_type != MESA_PORT_INTERFACE_SGMII) {
                *copper = 1;
            }
        }
    } else { // Copper preferred
        if (phy_status.link) {
            if (port_conf.if_type != MESA_PORT_INTERFACE_SGMII) {
                *copper = 1;
            }
        } else {
            if (!status->sfp.status.los) {
                if (port_conf.if_type == MESA_PORT_INTERFACE_SGMII) {
                    *fiber = 1;
                }
            }
        }
    }
    if (*fiber && (port.ams_mode[port_no] == VTSS_APPL_AMS_FIBER_PREFERRED)) {
        // Wait To Revert. Wait 10 poll rounds (few seconds) before change to another media.
        // Some SFPs are slow to reset the los signal after activation.
        *wtr = 10;
    }

    return;
}

// If a port has been powered down, we need to do re-detection of the SFPs in order to
// make sure that the MAC interface is configured correctly (Bugzilla#22270)
static bool sfp_power_down_redetect(mesa_port_no_t port_no) {
    static mesa_port_list_t had_power_down; // Remember if port was powered down.

    mesa_port_conf_t        conf;
    (void) mesa_port_conf_get(NULL, port_no, &conf);

    // If changed from power down to power up the we need to do redetect
    if (had_power_down[port_no] && !conf.power_down) {
        had_power_down[port_no] = conf.power_down;
        return TRUE;
    }
    had_power_down[port_no] = conf.power_down;
    return FALSE;
}


// Work around for that we have seen that "EXCOM" CU SFPs doesn't come up
// after having been turned off with tx_enable. The work-around is to run re-detection of
// the SFP after we have seen link down on the RJ45 interface.
//
// This only apply for dual media ports
// At the same time the laser for SFPs are turned off if it is currently the RJ45 that has link.
static bool sfp_redetect(mesa_port_no_t port_no) {
    BOOL redetect_sfp = FALSE;

    if (PORT_IS_DUAL_MEDIA_AMS(port_no)) {
        static mesa_port_list_t rj45_had_link; // Remember last link state.
        vtss_appl_port_status_t *port_status = &port.status[VTSS_ISID_LOCAL][port_no];
        mesa_port_status_t *status = &port_status->status;

        BOOL rj45_link = (port_status->status.copper && status->link);
        BOOL apply_tx_enable = rj45_had_link[port_no] == rj45_link ? 0 : 1;

        if (rj45_had_link[port_no] && !rj45_link) {
            if (!PORT_PHY_IS_INT_AMS(port_no)) { // Not needed for internal phys
                redetect_sfp = TRUE;
            }
            rj45_had_link[port_no]  = FALSE;
        } else {
            rj45_had_link[port_no]  = rj45_link;
        }

        T_RG_PORT(TRACE_GRP_AMS, port_no, "rj45_had_link:%d, redetect:%d, link:%d, copper:%d",
                  rj45_had_link.get(port_no), redetect_sfp, status->link, port_status->status.copper);

        if (apply_tx_enable) {
            // Disable the Laser (tx_enable) if we are currently using the RJ45 port
            meba_port_admin_state_t admin;
            memset(&admin, 0, sizeof(admin));
            admin.enable = rj45_link ? FALSE : TRUE;
            (void) port_admin_state_set(board_instance, port_no, &admin);
        }
    }
    return redetect_sfp;
}

/* Module thread */
static void port_thread(vtss_addrword_t data)
{
    mesa_rc               rc;
    mesa_port_no_t        port_no, p, fast_port_no;
    vtss_mtimer_t         optimize_timer, aneg_timer;
    mesa_port_counters_t  *counters;
    port_veriphy_t        *veriphy;
    int                   i, i2, veriphy_running = 0, veriphy_done = 0;
    BOOL                  changed = FALSE;
    BOOL                  any_port_status_changed = FALSE;
    CapArray<mesa_event_t, MESA_CAP_PORT_CNT> link_down;
    vtss_flag_value_t      flag_value;
    CapArray<uint, MESA_CAP_PORT_CNT> fast_link_failure; /* fast link failure 'hold' timer */
    u32                   port_count = port.port_count[VTSS_ISID_LOCAL], wtr = 0;
    CapArray<mesa_port_interface_t, MESA_CAP_PORT_CNT> sfp_if_bk;
    mesa_port_list_t      sfp_status_new;
    mesa_port_list_t      sfp_status_old;
    BOOL                  first_time = 1, chg_to_fiber = 0, chg_to_copper = 0;
    mesa_port_interface_t sfp_if = MESA_PORT_INTERFACE_SERDES;
    mesa_port_no_t        decection_port = 0;
    vtss_appl_port_status_t *port_status;
    mesa_port_status_t    *status;
    CapArray<mesa_bool_t, MESA_CAP_PORT_CNT> last_status_fiber;

    /* The switch API has been set up by the vtss_api module */
    VTSS_ASSERT(vtss_board_type() != VTSS_BOARD_UNKNOWN);
    port_init_ports();
    VTSS_MTIMER_START(&optimize_timer, 1000);

     /* This is used to indicate that a fast link failure interrupt has
       requested a full run through all ports */
    fast_port_no = MESA_PORT_NO_NONE;

    for (port_no = 0; port_no < port_count; port_no++) {
        // Given a default value
        fast_link_failure[port_no] = 0; /* No fast link failure detected */
        link_down[port_no] = 0;
        last_status_fiber[port_no] = FALSE;
        sfp_if_bk[port_no] = MESA_PORT_INTERFACE_NO_CONNECTION;
    }

    vtss::Vector<vtss::Pair<meba_event_t,vtss_interrupt_function_hook_t>> irqs = {
        {MEBA_EVENT_FLNK, port_los_interrupt_function},
        {MEBA_EVENT_LOS,  port_los_interrupt_function},
        {MEBA_EVENT_AMS,  port_ams_interrupt_function},
    };
    for (int i = 0; i < irqs.size(); i++) {
        if (vtss_interrupt_source_hook_set(VTSS_MODULE_ID_PORT, irqs[i].second, irqs[i].first, INTERRUPT_PRIORITY_NORMAL) != VTSS_OK)
            T_I("Note: IRQ %s not supported", vtss_interrupt_source_text(irqs[i].first));
    }

    for (;;) {
        port.thread_lock.wait();
        any_port_status_changed = FALSE;

        /* Port module state machine used to detect if ready for warm start */
        PORT_CRIT_ENTER();
        switch (port.module_state) {
        case PORT_MODULE_STATE_CONF:
            T_I("CONF -> ANEG state");
            port.module_state = PORT_MODULE_STATE_ANEG;
            VTSS_MTIMER_START(&aneg_timer, 6000);
            break;
        case PORT_MODULE_STATE_ANEG:
            if (VTSS_MTIMER_TIMEOUT(&aneg_timer)) {
                T_I("ANEG -> POLL state");
                port.module_state = PORT_MODULE_STATE_POLL;
            }
            break;
        case PORT_MODULE_STATE_POLL:
            T_I("POLL -> READY state");
            port.module_state = PORT_MODULE_STATE_READY;
            break;
        case PORT_MODULE_STATE_READY:
        default:
            break;
        }
        PORT_CRIT_EXIT();

        for (port_no = 0; port_no < port_count; port_no++)
            link_down[port_no] = 0;
        for (port_no = 0; port_no < port_count; port_no++) {
            changed = FALSE;

            PORT_HAS_CAP(VTSS_ISID_LOCAL, port_no);

            i = port_no;
            if (fast_port_no == MESA_PORT_NO_NONE || fast_link_int[i]) {
                /* NOT a fast link fail detection run or rising edge is detected on this port */
                if (fast_link_int[i]) {
                    /* Rising edge of fast link failure detected */
                    fast_link_int[i] = false;            /* Clear rising edge */
                    fast_link_failure[i] = (port_count - 2); /* Now initialize fast link failure 'hold' timer counter to 'allmost' 1 s. */
                }

                /* Poll port status */
                PORT_CRIT_ENTER();
                port_status_poll(port_no, &changed, &link_down[i], fast_link_failure[i]);

                if (fast_port_no == MESA_PORT_NO_NONE) {

                    /* Read port counters */
                    counters = &port.counters[i];
                    rc = mesa_port_counters_get(NULL, port_no, counters);

#if defined(VTSS_SW_OPTION_I2C) /* SFP auto detection through I2C */
                    T_RG_PORT(TRACE_GRP_SFP, port_no, "port_custom_table[port_no].cap:0x%X, changed:%d",
                              port_custom_table[port_no].cap, changed);

                    // The speed can have changed if a dual speed port change from a fiber port to
                    // cu port, if e.g. the fiber port is 1G and the cu port has auto negotiated to 100M.
                    // We need to make sure that the MAC is updated accordingly, so we call port_setup.
                    port_status = &port.status[VTSS_ISID_LOCAL][port_no];
                    status = &port_status->status;

                    if (changed) {
                        if (status->fiber != last_status_fiber[port_no]) {
                            port_setup(port_no, PORT_CONF_CHANGE_PHY);
                        }
                    }
                    last_status_fiber[port_no] = status->fiber;

                    //
                    // SFP handling
                    //
                    if (sfp_detect_module(port_no)) {
                        if (first_time) {
                            decection_port = port_no;
                            first_time = 0;
                        }

                        /* Read SFP slot status once each port poll round */
                        if (port_no == decection_port || changed) {
                            if (sfp_status_poll(&sfp_status_new, &changed) != VTSS_RC_OK) {
                                T_E("Could not read from SFPs");
                            }
                        }

                        T_RG_PORT(TRACE_GRP_SFP, port_no, "New SFP :%d, old:%d, new%d,  link_down[i]:%d",
                                  sfp_status_old[port_no] != sfp_status_new[port_no], sfp_status_old.get(port_no), sfp_status_new.get(port_no), link_down[i]);

                        // If a SFP is removed and reinserted very quickly (faster than the poll time) the link down is detected,
                        // but the SFP is not detected (Bugzilla#21778). The fix is to re-detect when a link down event is detected.
                        if (sfp_status_old[port_no] != sfp_status_new[port_no] || sfp_redetect(port_no) || sfp_power_down_redetect(port_no) || link_down[i]) {
                            mesa_port_conf_t port_conf;
                            T_IG_PORT(TRACE_GRP_SFP, port_no, "SFP is %s", sfp_status_new[port_no] ? "INSERTED" : "REMOVED");
                            // Don't call sfp_detect_if() when the port is power down mode
                            if ((mesa_port_conf_get(NULL, port_no, &port_conf) == VTSS_OK &&
                                !port_conf.power_down) &&
                                sfp_status_new[port_no]) {
                                /* New SFP module is inserted. Figure out the type */

                                // We need to redo the detection twice, because the we can not detect if it is a cisco sgmii interface before the
                                // phy is configured to pass through mode, and we should not set the phy in pass through mode before we know
                                // that it is cu SFP. Chicken and the egg.
                                for (auto redo = 0; redo < (is_port_phy(port_no) && !MEBA_PORT_CAP_INT_PHY ? 3 : 1); redo++) {
                                    if (sfp_detect_if(port_no, &sfp_if, redo == 2) == VTSS_RC_OK) {
                                        // Save the detected MAC interface
                                        port.mac_sfp_if[port_no] = sfp_if;

                                        if ((sfp_status_old[port_no] != sfp_status_new[port_no]) &&
                                            sfp_status_new[port_no] &&
                                            !port_phy(port_no)) {
                                            // Initialize port setup when a new SFP module is inserted
                                            // Use parameter PORT_CONF_CHANGE_MAC to chage the SFP MAC interface
                                            T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "Detect a new SFP module is inserted");
                                            port_setup(port_no, PORT_CONF_CHANGE_MAC);
                                        }

                                        // Backup the current status and MAC interface
                                        sfp_status_old[port_no] = sfp_status_new[port_no];
                                        sfp_if_bk[port_no] = sfp_if;

                                        T_DG_PORT(TRACE_GRP_SFP, port_no, "sfp_if: %s, port.mac_sfp_if[port_no]: %s, PORT_PHY_IS_INT_AMS(port_no):%d, dual_media_fiber_speed:%d",
                                                  mac_if2txt(sfp_if),
                                                  mac_if2txt(port.mac_sfp_if[port_no]),
                                                  PORT_PHY_IS_INT_AMS(port_no),
                                                  port.config[VTSS_ISID_LOCAL][port_no].dual_media_fiber_speed);
                                        if (is_port_phy(port_no) || PORT_PHY_IS_INT_AMS(port_no)) {
                                            phy_fiber_speed_update(port_no, port.config[VTSS_ISID_LOCAL][port_no].dual_media_fiber_speed);
                                        }
                                    }
                                }
                                T_DG_PORT(TRACE_GRP_SFP, port_no, "sfp_if: %s, port.mac_sfp_if[port_no]: %s", mac_if2txt(sfp_if), mac_if2txt(port.mac_sfp_if[port_no]));
                            } else {
                                /* SFP module is removed. */
                                memset(&port.status[VTSS_ISID_LOCAL][port_no].sfp, 0, sizeof(vtss_appl_sfp_t));
                                port.status[VTSS_ISID_LOCAL][port_no].sfp.status.los = 1;
                                port.status[VTSS_ISID_LOCAL][port_no].sfp.status.tx_fault = 1;
                                sfp_status_old[port_no] = sfp_status_new[port_no] = 0;
                            }
                        }
                    }

                    if (PORT_PHY_IS_INT_AMS(port_no)) {
                        // Poll the SW supported AMS and do a media changeover if needed
                        // Note: Only for supported ports and architechtures (ServalT)
                        check_dual_ams_ports(port_no, &chg_to_fiber, &chg_to_copper, &wtr);
                        if (chg_to_fiber) {
                            // Change to fiber media
                            T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "Change to fiber media");
                            T_DG_PORT(TRACE_GRP_AMS, port_no, "%s", "Change to fiber media");
                            port.status[VTSS_ISID_LOCAL][port_no].status.copper = 0;
                            port.mac_sfp_if[port_no] = sfp_if_bk[port_no] != MESA_PORT_INTERFACE_NO_CONNECTION ? sfp_if_bk[port_no] : MESA_PORT_INTERFACE_SERDES /* default MAC interface */;

                            if (port_setup(port_no, PORT_CONF_CHANGE_ALL) != VTSS_RC_OK) {
                                T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "Could not perform port_setup");
                            }
                            chg_to_fiber = 0;
                        } else if (chg_to_copper) {
                            // Change to copper (phy) media
                            T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "Change to copper media");
                            T_DG_PORT(TRACE_GRP_AMS, port_no, "Change to copper media, set status->fiber to %s",
                                      port.status[VTSS_ISID_LOCAL][port_no].status.fiber ? "OFF" : "ON");
                            port.status[VTSS_ISID_LOCAL][port_no].status.fiber = 0;
                            port.mac_sfp_if[port_no] = MESA_PORT_INTERFACE_SGMII;

                            if (port_setup(port_no, PORT_CONF_CHANGE_ALL) != VTSS_RC_OK) {
                                T_DG_PORT(TRACE_GRP_SFP, port_no, "%s", "Could not perform port_setup");
                            }
                            chg_to_copper = 0;
                        }
                    }

#endif /* VTSS_SW_OPTION_I2C */
                    /* Update port LED */
                    meba_port_led_update(board_instance, port_no, &port.status[VTSS_ISID_LOCAL][i].status, counters, &port.config[VTSS_ISID_LOCAL][port_no].admin);
                    /* VeriPHY processing for all ports */
                    veriphy_running = 0;
                    veriphy_done = 0;

#ifdef VTSS_SW_OPTION_POE
                    // Needed due to bugzilla#8911, see the other comments regarding this.
                    poe_status_t poe_port_status;
                    poe_mgmt_get_status(&poe_port_status);
#endif
                    for (p = 0; p < port_count; p++) {
                        veriphy = &port.veriphy[p];
                        if (veriphy->running) {
                            veriphy_running++;
                            mesa_phy_veriphy_result_t temp_result;  /* Temp Result */
                            rc = mesa_phy_veriphy_get(PHY_INST, p, &temp_result);
                            if (rc != VTSS_RC_INCOMPLETE) {

                                veriphy->result.link = temp_result.link;
                                for (i = 0; i  < 4; i++) {
                                    T_I_PORT(p, "status[%d]:0x%X, temp_status:0x%X, length:0x%X, temp_length:0x%X",
                                             i, veriphy->result.status[i], temp_result.status[i], veriphy->result.length[i], temp_result.length[i]);


                                    // Bugzilla#8911, Sometimes cable length is measured too long (but never too short), so veriphy is done multiple times and the shortest length is stored.

                                    // First time we simply use the result we got from the PHY
                                    if (veriphy->repeat_cnt < VERIPHY_REPEAT_CNT) {
                                        // We have seen that the results variates when no cable is plugged in so if that
                                        // happens we say that the port is unconnected. (adding 3 because the resolution is 3 m, so that variation is OK).
                                        if ((veriphy->result.length[i] + 3) < temp_result.length[i]) {
                                            veriphy->variate_cnt++;
                                        } else {
                                            veriphy->result.length[i] = temp_result.length[i];
                                            veriphy->result.status[i] = temp_result.status[i];
                                        }
                                    } else {
                                        veriphy->result.length[i] = temp_result.length[i];
                                        veriphy->result.status[i] = temp_result.status[i];
                                    }

                                    T_I_PORT(p, "status[%d]:0x%X, temp_status:0x%X, length:0x%X, temp_length:0x%X",
                                             i, veriphy->result.status[i], temp_result.status[i], veriphy->result.length[i], temp_result.length[i]);


#ifdef VTSS_SW_OPTION_POE
                                    // Bugzilla#8911, we have that PoE boards can "confuse" VeriPhy, to wrongly report ABNORMAL, SHORT and OPEN, so when we have a PD, we only report OK (else it is set as unknown)
                                    if (poe_port_status.port_status[p] != VTSS_APPL_POE_NO_PD_DETECTED &&
                                        poe_port_status.port_status[p] != VTSS_APPL_POE_NOT_SUPPORTED &&
                                        poe_port_status.port_status[p] != VTSS_APPL_POE_DISABLED) {
                                        T_I_PORT(p, "PoE Port status:%d", poe_port_status.port_status[p]);
                                        if (temp_result.status[i] != MESA_VERIPHY_STATUS_OK) {
                                            T_I_PORT(p, "temp_result.status[%d]:%d", i, temp_result.status[i]);
                                            veriphy->result.status[i] = MESA_VERIPHY_STATUS_UNKNOWN;
                                        }
                                    }
#endif
                                    T_I_PORT(p, "status[%d]:0x%X, length:0x%X variate_cnt:%d", i, veriphy->result.status[i], veriphy->result.length[i], veriphy->variate_cnt);
                                }
                                T_N_PORT(p, "veriPHY done, rc = %d", rc);

                                // Work-around of bugzilla#8911 - If the result variates it is an indication of that the port in not connected
                                if (veriphy->variate_cnt > 1) {
                                    for (i2 = 0; i2  < 4; i2++) {
                                        if (veriphy->result.status[i2] != MESA_VERIPHY_STATUS_UNKNOWN) {
                                            veriphy->result.status[i2] = MESA_VERIPHY_STATUS_OPEN;
                                            veriphy->result.length[i2] = 0;
                                        }
                                    }
                                    T_D_PORT(p, "Forcing status to open, vaiate_cnt:%d", veriphy->variate_cnt);
                                }

                                if (veriphy->repeat_cnt > 1) {
                                    // Bugzilla#8911- Repeat veriphy a number of times in order to get correct result.
                                    veriphy->repeat_cnt--;
                                    mesa_phy_veriphy_start(PHY_INST, p,
                                                           veriphy->mode == PORT_VERIPHY_MODE_BASIC ? 2 :
                                                           veriphy->mode == PORT_VERIPHY_MODE_NO_LENGTH ? 1 : 0);
                                } else {
                                    veriphy->repeat_cnt = 0;
                                    veriphy->running = 0;
                                    veriphy->valid = (rc == VTSS_OK ? 1 : 0);
                                    veriphy_done++;
                                }
                            }
                        }
                    }
                }

                if (changed) {
                    any_port_status_changed = TRUE;
                }

                PORT_CRIT_EXIT();
                if (fast_port_no == MESA_PORT_NO_NONE) {
                    if (veriphy_running && veriphy_running == veriphy_done)
                        port_stack_veriphy_reply();
                }
            }

            if (fast_port_no == MESA_PORT_NO_NONE) {
                /* Only do this decrement of fast link fail 'hold' timer counter
                   if it is a 'normal' run (not initiated by interrupt) */
                for (p = 0; p < port_count; p++)
                    if (fast_link_failure[p])   /* If fast link failure is active, the counter is decremented to run the 'hold' timer */
                        fast_link_failure[p]--;
            }

            if (fast_port_no == port_no || fast_port_no == MESA_PORT_NO_NONE) {
                /* Either a fast link fail run is not activated or it has been done */
                fast_port_no = MESA_PORT_NO_NONE; /* Fast link fail run has been done */
                flag_value = vtss_flag_timed_waitfor(&interrupt_wait_flag,
                                                     PHY_INTERRUPT,
                                                     VTSS_FLAG_WAITMODE_OR_CLR,
                                                     VTSS_OS_MSEC2TICK(1000 / port_count));
                if (flag_value == PHY_INTERRUPT) {
                    fast_port_no = port_no;  /* When interrupt received, loop has to take a fast link failure run, to take action on rising edge  */
                }
            }

            if (VTSS_MTIMER_TIMEOUT(&optimize_timer)) {
                rc = mesa_poll_1sec(NULL);
                VTSS_MTIMER_START(&optimize_timer, VTSS_OS_MSEC2TICK(1000));
            }

            if (fast_port_no == MESA_PORT_NO_NONE) {
                /* Global port change events */
                port_global_change_events();
            }
        } /* Port loop */

        if (any_port_status_changed) {
            port_stack_status_reply(link_down.data());
        }

        port_global_status_update_event();
    } /* for (;;) */

}

/****************************************************************************/
// port_pre_reset_callback()
// Called when system is reset.
/****************************************************************************/
static void port_pre_reset_callback(mesa_restart_t restart)
{
    ;    // TBD: Something was needed for Lu26?
}

static void port_isid_info_update(vtss_isid_t isid, vtss_init_data_t *data, BOOL add)
{
    init_switch_info_t *info = &data->switch_info[isid];

    if (info->configurable) {
        T_D("%s isid: %u, board_type: %u, port_cnt: %u, stack_0: %u, stack_1: %u",
            add ? "added" : "config",
            isid, info->board_type, info->port_cnt, info->stack_ports[0], info->stack_ports[1]);
        port.board_type[isid] = info->board_type;
        port.port_count[isid] = info->port_cnt;
        port.isid_added[isid] = 1;
    } else if (add) {
        T_E("isid: %u added, but not configurable", isid);
    }
}

extern "C" int port_power_savings_icli_cmd_register();
extern "C" int port_icli_cmd_register();

mesa_rc port_set_poe_pg(mesa_port_no_t port_no, mesa_bool_t poe_pg)
{
    mesa_port_status_t *status = &port.status[VTSS_ISID_LOCAL][port_no].status;

    // If want to add a new member into `mesa_port_status_t`, must to modify two files at the same time
    // `vtss_api/include/vtss/api/port.h` and `vtss_api/mesa/include/mscc/ethernet/switch/api/port.h`
    status->poe_pg = poe_pg;
    return MESA_RC_OK;
}

/* Initialize module */
mesa_rc port_init(vtss_init_data_t *data)
{
    vtss_isid_t           isid = data->isid;
    BOOL                  changed, new_;
    mesa_port_no_t        port_no;
    port_vol_conf_t       *vol_conf;
    u32           user;
    mesa_port_status_t    *status;
    port_veriphy_t        *veriphy;
    u32                   port_cnt = mesa_port_cnt(NULL);

    if (data->cmd == INIT_CMD_EARLY_INIT) {
        /* Initialize and register trace ressources */
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);
    }

    T_D("enter, cmd: %d, isid: %u, flags: 0x%x", data->cmd, data->isid, data->flags);





    switch (data->cmd) {
    case INIT_CMD_INIT:
        T_I("INIT");

        /* Initialize port change table */
        port.change_table.count = 0;
        port.global_change_table.count = 0;
        port.shutdown_table.count = 0;
        for (port_no = 0; port_no < port_cnt; port_no++) {
            /* Update sfp interface with the default mac interface */
            port.mac_sfp_if[port_no] = port_mac_interface(port_no);
        }

        /* Create critical region variables */
        critd_init(&port.crit, "port.crit", VTSS_MODULE_ID_PORT, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);

        /* Create critical region protecting callbacks and their registrations  */
        critd_init(&port.cb_crit, "port.cb_crit", VTSS_MODULE_ID_PORT, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);

        /* Let go of the callback crit immediately, so that modules can start registering ASAP */
        PORT_CB_CRIT_EXIT();

        /* Create event flag for interrupt to signal and port_thread to wait for */
        vtss_flag_init(&interrupt_wait_flag);

#ifdef VTSS_SW_OPTION_PRIVATE_MIB
        /* Register our private mib */
        port_mib_init();
#endif

#ifdef VTSS_SW_OPTION_JSON_RPC
        vtss_appl_port_json_init();
#endif
        port_icli_cmd_register();

#ifdef VTSS_SW_OPTION_PORT_POWER_SAVINGS
#ifdef VTSS_SW_OPTION_PRIVATE_MIB
        /* Register our private mib */
        vtss_appl_port_power_savings_mib_init();
#endif // endif VTSS_SW_OPTION_PRIVATE_MIB

#ifdef VTSS_SW_OPTION_JSON_RPC
        vtss_appl_port_power_savings_json_init();
#endif //endif VTSS_SW_OPTION_JSON_RPC
        port_power_savings_icli_cmd_register();
#endif //endif VTSS_SW_OPTION_PORT_POWER_SAVINGS

        /* Move thread creation to INIT_CMD_START */

        // Register system reset callback
        control_system_reset_register(port_pre_reset_callback);

        /* Initialize ISID information for all switches */
        for (isid = VTSS_ISID_LOCAL; isid < VTSS_ISID_END; isid++) {
            /* By default, the local information is used for all switches */
            port.board_type[isid] = vtss_board_type();
            port.port_count[isid] = MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT);
        }
        port.isid_added[VTSS_ISID_LOCAL] = 1; /* Local always there */

#ifdef VTSS_SW_OPTION_ICFG
        // Initialize ICLI "show running" configuration
        VTSS_RC(port_icfg_init());

#if defined(VTSS_SW_OPTION_PORT_POWER_SAVINGS)
        VTSS_RC(port_power_savings_icfg_init());
#endif
#endif
        break;
    case INIT_CMD_START:
        /* Create thread */
        VTSS_ASSERT(port.port_count[VTSS_ISID_LOCAL]>0);
        port.thread_lock.lock(false);
        vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
                          port_thread,
                          0,
                          "Port Control",
                          nullptr,
                          0,
                          &port.thread_handle,
                          &port.thread_block);

        break;
    case INIT_CMD_CONF_DEF:
        T_I("CONF_DEF, isid: %d", isid);
        if (isid == VTSS_ISID_LOCAL) {
            /* Reset local configuration */
        } else if (isid == VTSS_ISID_GLOBAL) {
            /* Reset global configuration */
        } else if (VTSS_ISID_LEGAL(isid)) {
            /* Reset switch configuration */
            if (!data->switch_info[isid].configurable) {
                /* Switch no longer configurable */
                port.isid_added[isid] = 0;
            }
            if (port.isid_added[isid]) {
                port_create_default(isid, &changed);
                if (changed) {
                    port_stack_conf_set(isid, TRUE);
                }
            }
        }
        break;
    case INIT_CMD_MASTER_UP:
        T_I("MASTER_UP");

        PORT_CRIT_ENTER();
        for (isid = VTSS_ISID_START; isid < VTSS_ISID_END; isid++) {
            /* Update switch information for configurable switches */
            port_isid_info_update(isid, data, 0);

            /* Initialize port states to link down */
            for (port_no = 0; port_no < port_cnt; port_no++)
                port.status[isid][port_no].status.link = 0;
        }
        PORT_CRIT_EXIT();

        /* Create default configuration */
        for (isid = VTSS_ISID_START; isid < VTSS_ISID_END; isid++) {
            if (port.isid_added[isid]) {
                port_create_default(isid, &changed);
            }
        }
        T_I("MASTER_UP EXIT");
        break;
    case INIT_CMD_MASTER_DOWN:
        T_I("MASTER_DOWN");
        break;
    case INIT_CMD_SWITCH_ADD:
        T_I("SWITCH_ADD, isid: %d", isid);

        PORT_CRIT_ENTER();

        /* Determine if new switch and store ISID information */
        new_ = (port.isid_added[isid] == 0);
        port_isid_info_update(isid, data, 1);

        /* Initialize VeriPHY state */
        for (port_no = 0; port_no < port_cnt; port_no++) {
            veriphy = &port.veriphy[port_no];
            veriphy->running = 0;
            veriphy->valid = 0;
        }

        PORT_CRIT_EXIT();

        if (new_) {
            /* Create default configuration if new switch */
            port_create_default(isid, &changed);
        }
        port_stack_conf_set(isid, TRUE);
        port_stack_status_get(isid);
        break;
    case INIT_CMD_SWITCH_DEL:
        T_I("SWITCH_DEL, isid: %d", isid);

        PORT_CRIT_ENTER();
        for (port_no = 0; port_no < port_cnt; port_no++) {
            /* Generate port change events */
            status = &port.status[isid][port_no].status;
            if (status->link) {
                status->link = 0;
                port.change_flags[port_no] |= PORT_CHANGE_DOWN;
            }
            port.cap_valid[isid][port_no] = 0;

            /* Delete volatile port configuration */
            for (user = PORT_USER_STATIC; user < PORT_USER_CNT; user++) {
                vol_conf = &port.vol_conf[user][port_no];
                memset(vol_conf, 0, sizeof(*vol_conf));
            }
        }
        PORT_CRIT_EXIT();
        break;
    case INIT_CMD_SUSPEND_RESUME:
        PORT_CRIT_ENTER();
        port.thread_lock.lock(!data->resume);
        PORT_CRIT_EXIT();
        break;
    case INIT_CMD_WARMSTART_QUERY:
        /* Check if ready for warm start */
        if (port.module_state != PORT_MODULE_STATE_READY)
            data->warmstart = 0;
        break;
    default:
        break;
    }

    T_D("exit");
    return VTSS_OK;
}
/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
