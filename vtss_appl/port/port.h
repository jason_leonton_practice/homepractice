/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
*/

#ifndef _VTSS_PORT_H_
#define _VTSS_PORT_H_

#include "vtss_os_wrapper.h"
#include "critd_api.h" // for critd_t
#include "port_api.h"
#include "lock.hxx"

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_PORT

/* ================================================================= *
 *  Port change events
 * ================================================================= */

#define PORT_CHANGE_REG_MAX 24
#ifdef __cplusplus
extern "C" {
#endif

/* Port change registration table */
typedef struct {
    int               count;
    port_change_reg_t reg[PORT_CHANGE_REG_MAX];
} port_change_table_t;

#define PORT_CHANGE_USER_LENGTH 16

/* Port global change registration table */
typedef struct {
    int                      count;
    port_global_change_reg_t reg[PORT_CHANGE_REG_MAX];
} port_global_change_table_t;

#define PORT_STATUS_UPDATE_REG_MAX 16

/* Port global status update registration table */
typedef struct {
    int                      count;
    port_global_status_update_t reg[PORT_STATUS_UPDATE_REG_MAX];
} port_global_status_update_table_t;

#define PORT_SHUTDOWN_REG_MAX 16

/* Port shutdown registration table */
typedef struct {
    int                 count;
    port_shutdown_reg_t reg[PORT_SHUTDOWN_REG_MAX];
} port_shutdown_table_t;

/* ================================================================= *
 *  Port veriPHY state
 * ================================================================= */

typedef struct {
    BOOL                      running; /* Running or idle */
    BOOL                      valid;   /* Result valid */
    mesa_phy_veriphy_result_t result;  /* Result */
    port_veriphy_mode_t       mode;    /* The mode to run VeriPhy. */
    u8                        repeat_cnt; /* Number of time VeriPhy should be repeated */
    u8                        variate_cnt; /* Number of time the result is not the same */
} port_veriphy_t;

/* Port change flags */
#define PORT_CHANGE_UP   0x01
#define PORT_CHANGE_DOWN 0x02
#define PORT_CHANGE_ANY  (PORT_CHANGE_UP | PORT_CHANGE_DOWN)

/* Port module state */
typedef enum {
    PORT_MODULE_STATE_INIT, /* Initial state */
    PORT_MODULE_STATE_CONF, /* Configuration applied after first SWITCH_ADD */
    PORT_MODULE_STATE_ANEG, /* Auto negotiating ports (some seconds) */
    PORT_MODULE_STATE_POLL, /* Polling all ports */
    PORT_MODULE_STATE_READY /* Warm start ready  */
} port_module_state_t;

typedef enum {
    VTSS_APPL_AMS_DISABLED,
    VTSS_APPL_AMS_COPPER_FORCED,
    VTSS_APPL_AMS_FIBER_FORCED,
    VTSS_APPL_AMS_COPPER_PREFERRED,
    VTSS_APPL_AMS_FIBER_PREFERRED,
} vtss_appl_ams_port_t;

/* ================================================================= *
 *  Port global variables structure
 * ================================================================= */

typedef struct {
    /* Thread variables */
    vtss_handle_t              thread_handle;
    vtss_thread_t              thread_block;
    vtss::Lock                 thread_lock;

    /* Critical region protection protecting callbacks, i.e.
       #change_table, #global_change_table, and #shutdown_table */
    critd_t                    cb_crit;

    /* Critical region protection protecting the remaining variables in this struct */
    critd_t                    crit;

    port_module_state_t        module_state;                                             /* Port module state */
    BOOL                       isid_added[VTSS_ISID_END];                                /* SWITCH_ADD received */
    int                        board_type[VTSS_ISID_END];                                /* Board type */
    u32                        port_count[VTSS_ISID_END];                                /* Number of ports */
    CapArray<vtss_appl_port_conf_t, VTSS_APPL_CAP_ISID_END, MESA_CAP_PORT_CNT> config;   /* Port configuration */
    CapArray<port_vol_conf_t, VTSS_APPL_CAP_PORT_USER_CNT, MESA_CAP_PORT_CNT> vol_conf;  /* Volatile conf */
    CapArray<vtss_appl_port_status_t, VTSS_APPL_CAP_ISID_END, MESA_CAP_PORT_CNT> status; /* Port status */
    CapArray<BOOL, VTSS_APPL_CAP_ISID_END, MESA_CAP_PORT_CNT> cap_valid;                 /* Port capability valid */
    CapArray<mesa_port_counters_t, MESA_CAP_PORT_CNT> counters;                          /* Port counters */
    port_change_table_t        change_table;                                             /* Port change registrations */
    port_global_change_table_t global_change_table;                                      /* Port global change registrations */
    CapArray<uchar, MESA_CAP_PORT_CNT> change_flags;                                     /* Port change flags */
    CapArray<u32, MESA_CAP_PORT_CNT> setup_change;                                       /* Port setup change flags */
    port_shutdown_table_t      shutdown_table;                                           /* Port shutdown registrations */
    port_global_status_update_table_t global_status_update_table;                        /* Port global status update registrations */
    CapArray<port_veriphy_t, MESA_CAP_PORT_CNT> veriphy;                                 /* Port veriPHY */
    CapArray<mesa_port_interface_t, MESA_CAP_PORT_CNT> mac_sfp_if;                       /* MAC Interface to SFP module */
    CapArray<mesa_port_status_t, MESA_CAP_PORT_CNT> aneg_status;                         /* Current aneg status results for the port */
    CapArray<BOOL, MESA_CAP_PORT_CNT> sfp_los_int;                                       /* SFP LOS event */
    CapArray<vtss_appl_ams_port_t, MESA_CAP_PORT_CNT> ams_mode;                          /* AMS mode of the internal ports */
    CapArray<mesa_phy_type_t, MESA_CAP_PORT_CNT> phy_id_1g;                              /* PHY ID */
} port_global_t;

typedef struct {
    char description[VTSS_APPL_PORT_IF_DESCR_MAX_LEN];
} vtss_appl_port_descr_t;

void port_mib_init();
void vtss_appl_port_json_init();

#ifdef VTSS_SW_OPTION_PORT_POWER_SAVINGS
#ifdef VTSS_SW_OPTION_PRIVATE_MIB
void vtss_appl_port_power_savings_mib_init(void);
#endif // endif VTSS_SW_OPTION_PRIVATE_MIB

#ifdef VTSS_SW_OPTION_JSON_RPC
void vtss_appl_port_power_savings_json_init(void);
#endif //endif VTSS_SW_OPTION_JSON_RPC
#endif //endif VTSS_SW_OPTION_PORT_POWER_SAVINGS

/**
 * \brief Function that can check if at least one port in a stack contains a specific capability (capacities are specified in the board file).
 * \param cap [IN] The capability to check for.
 * \return TRUE if at least one port support the specified capability, else FALSE
*/
BOOL is_port_capability_stack_supported(meba_port_cap_t cap);

/**
 * \brief Function for modifying the port configuration if you only have information about if the media type is a CU or SFP (if both is set the the port is set in dual media mode). This is needed because the port module doesn't work with RJ45/SFP as such, but uses a combination of speed, auto-neg and fiber speed to configure the chip
 * \param rj45      [IN]  Set to TRUE if the port should be set to RJ45 only (or dual-media mode)
 * \param sfp       [IN]  Set to TRUE if the port should be set to SFP only (or dual-media mode)
 * \param port_conf [OUT] Pointer to where to the configuration to modify
 * \return VTSS_RC_OK if configuration was modified else error code
*/
mesa_rc vtss_port_media2conf(BOOL rj45, BOOL sfp, vtss_appl_port_conf_t *port_conf, vtss_isid_t isid, mesa_port_no_t port_no);

/**
 * \brief Reverse of vtss_port_media2conf - NOTE - Only for dual media ports
 * \param rj45      [OUT]  Is set to to TRUE if the media is to RJ45;
 * \param sfp       [OUT]  Is set to to TRUE if the media is to SFP;
 * \param port_conf [IN]   Pointer to the configuration.
 * \return VTSS_RC_OK if configuration was modified else error code
 */
mesa_rc vtss_port_conf2media(BOOL *rj45, BOOL *sfp, vtss_appl_port_conf_t *port_conf, vtss_isid_t isid, mesa_port_no_t port_no);

/**
 * \brief Setting the description of the interface
 * \param ifindex [IN] Interface number
 * \param descr   [IN] Pointer to description of the interface
 * \return VTSS_RC_OK if interface is set with proper description
 */
mesa_rc vtss_appl_ifc_desc_set(vtss_ifindex_t ifindex, const vtss_appl_port_descr_t *const descr);

/**
 * \brief Getting the description of the interface
 * \param ifindex [IN] Interface number
 * \param descr   [OUT] Pointer to description of the interface
 * \return VTSS_RC_OK if required interface description getting is done
 */
mesa_rc vtss_appl_ifc_desc_get(vtss_ifindex_t ifindex, vtss_appl_port_descr_t *const descr);


#ifdef __cplusplus
}  // extern C
#endif
#endif /* _VTSS_PORT_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
