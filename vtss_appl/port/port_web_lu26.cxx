/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

#include <mscc/ethernet/board/api.h>
#include "web_api.h"
#include "port_api.h"
#include "board_if.h"
#include "msg_api.h"
#include "led_api.h"
#include "vtss_api_if_api.h"

#include "conf_api.h"  // For lntn_conf_get

/* Stock resources in sw_mgd_html/images/jack_.... */
#define PORT_ICON_WIDTH         35
#define PORT_ICON_HEIGHT        40
#define PORT_LED_WIDTH          7
#define PORT_LED_HEIGHT         7
#define PORT_ICON_SFP_WIDTH     30

/* Port config for web */
typedef struct {
	char*	panel;
	int		port_xoff[12];
	int		port_yoff[12];
	int		icon_dir[12];
	int		led_xoff[6];
	int		led_yoff[6];
} port_web_conf_t;

static const char *port_type(mesa_port_no_t iport)
{
    switch(port_custom_table[iport].mac_if) {
    case MESA_PORT_INTERFACE_SGMII:
    case MESA_PORT_INTERFACE_QSGMII:
        return "copper";
    case MESA_PORT_INTERFACE_XAUI:
        return port_custom_table[iport].cap & MEBA_PORT_CAP_VTSS_10G_PHY ? "sfp" : "x2";
    case MESA_PORT_INTERFACE_SERDES:
    default:
        return "sfp";
    }
}

static void get_web_port_conf(port_web_conf_t* conf)
{
	int copper_num;
	int fiber_num;
	int total_num;
	bool scconnector;
	port_web_conf_t pconf;
	lntn_global_conf_t lntn_conf = lntn_conf_get();

	memset(&pconf, 0, sizeof(port_web_conf_t));
	copper_num  = lntn_conf.mgmt_conf.amount_of_copper;
	fiber_num   = lntn_conf.mgmt_conf.amount_of_fiber;
	scconnector = lntn_conf.mgmt_conf.scconnector;

	if(copper_num == 8 && fiber_num == 4 && scconnector == 0) {
		//1204
		pconf = {"switch_lntn.png",
				{22,58,22,58,22,58,22,58,22,22,22,22},
				{340,340,300,300,260,260,220,220,150,110,70,30},
				{1,0,1,0,1,0,1,0,1,1,1,1},
				{0, 93, 59, 93, 59, 76},
				{0, 29, 48, 48, 29, 29} };
		*conf = pconf;
	} else if(copper_num == 10 && fiber_num == 2 && scconnector == 0) {
		//1202
		pconf = {"switch_lntn.png",
				{22,58,22,58,22,58,22,58,22,58,22,22},
				{340,340,300,300,260,260,220,220,180,180,70,30},
				{1,0,1,0,1,0,1,0,1,0,1,1},
				{0, 93, 59, 93, 59, 76},
				{0, 29, 48, 48, 29, 29} };
		*conf = pconf;
	} else if(copper_num == 8 && fiber_num == 2 && scconnector == 0) {
		//1002
		pconf = {"switch_lntn.png",
				{22,58,22,58,22,58,22,58,22,22,0,0},
				{340,340,300,300,260,260,220,220,70,30,0,0},
				{1,0,1,0,1,0,1,0,1,1,0,0},
				{0, 93, 59, 93, 59, 76},
				{0, 29, 48, 48, 29, 29} };
		*conf = pconf;
	} else if(copper_num == 6 && fiber_num == 2 && scconnector == 0) {
		//0802
		pconf = {"switch_lntn.png",
				{22,58,22,22,22,22,22,22,0,0,0,0},
				{340,340,290,250,210,170,70,30,0,0,0,0},
				{1,0,1,1,1,1,1,1,0,0,0,0},
				{0, 93, 59, 93, 59, 76},
				{0, 29, 48, 48, 29, 29} };
		*conf = pconf;
	} else if(copper_num == 8 && fiber_num == 0 && scconnector == 0) {
		//0800
		pconf = {"switch_lntn.png",
				{22,22,22,22,22,22,22,22,0,0,0,0},
				{340,300,260,220,150,110,70,30,0,0,0,0},
				{1,1,1,1,1,1,1,1,0,0,0,0},
				{0, 93, 59, 93, 59, 76},
				{0, 29, 48, 48, 29, 29} };
		*conf = pconf;
	} else if(copper_num == 4 && fiber_num == 4 && scconnector == 0) {
		//0804
		pconf = {"switch_lntn.png",
				{22,22,22,22,22,22,22,22,0,0,0,0},
				{340,300,260,220,150,110,70,30,0,0,0,0},
				{1,1,1,1,1,1,1,1,0,0,0,0},
				{0, 93, 59, 93, 59, 76},
				{0, 29, 48, 48, 29, 29} };
		*conf = pconf;
	} else if(copper_num == 6 && fiber_num == 2 && scconnector == 1) {
		//0602-M
		pconf = {"switch_lntn.png",
				{22,58,22,58,22,58,22,22,0,0,0,0},
				{340,340,300,300,260,260,70,30,0,0,0,0},
				{1,0,1,0,1,0,1,1,0,0,0,0},
				{0, 93, 59, 93, 59, 76},
				{0, 29, 48, 48, 29, 29} };
		*conf = pconf;
	}

	return;
}

/*
 * The (basic) portstate handler
 */
#if 1
void stat_portstate_switch_lu26(CYG_HTTPD_STATE* p, vtss_usid_t usid, vtss_isid_t isid)
{
    mesa_port_no_t          iport;
    vtss_uport_no_t         uport;
    vtss_appl_port_conf_t   conf;
    vtss_appl_port_status_t port_status;
    port_web_conf_t			pconf;	//port mapping config on web
    int                     ct, xoff, yoff, dir;
    u32                     port_cnt = MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT);

    /* LNTN LED System Status */
    int                     led_type;
    meba_led_color_t        led_color;

    // char               combo_port = 0;
    // char               sfp_port = 0;
    // BOOL               lu26 = (vtss_board_type() == VTSS_BOARD_LUTON26_REF);

    memset(&pconf, 0, sizeof(port_web_conf_t));
    get_web_port_conf(&pconf);

    /* Backround, SID, managed */
    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d|%s|",
                  usid,
                  pconf.panel);
    cyg_httpd_write_chunked(p->outbuffer, ct);

    /* Emit port icons */
    for (iport = 0; iport < port_cnt; iport++) {
        uport = iport2uport(iport);

        vtss_ifindex_t ifindex;
        if (vtss_ifindex_from_port(isid, iport, &ifindex) != VTSS_RC_OK) {
            return;
        }

        if(vtss_appl_port_conf_get(ifindex, &conf) < 0 ||
            vtss_appl_port_status_get(ifindex, &port_status) < 0)
            break;              /* Most likely stack error - bail out */

        const char *state = conf.admin.enable ? (port_status.status.link ? "link" : "down") : "disabled";
        const char *speed = conf.admin.enable ?
            (port_status.status.link ?
             port_mgmt_mode_txt(iport, port_status.status.speed, port_status.status.fdx, port_status.status.fiber) :
             "Down") : "Disabled";
        if (port_custom_table[iport].cap & MEBA_PORT_CAP_SFP_DETECT) {
            xoff = pconf.port_xoff[iport];
            yoff = pconf.port_yoff[iport];
            dir = pconf.icon_dir[iport];
        } else {
            xoff = pconf.port_xoff[iport];
            yoff = pconf.port_yoff[iport];
            dir = pconf.icon_dir[iport];
        }

        // If the active link is fiber and the port has link then
        // change the state to down for the copper port.
        if (port_custom_table[iport].cap & (MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER) &&
            port_status.status.fiber && port_status.status.link)
            state = "down";

        // Update the copper ports
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                      "%u/jack_%s_%s_%s.png/Port %u: %s/%u/%u/%u/%u/%d|",
                      uport,
                      port_type(iport),
                      state,
                      dir == 1 ? "top" : "bottom",
                      uport, speed,
                      xoff,     /* xoff */
                      yoff,     /* yoff */
                      strcmp(port_type(iport),"copper") == 0? PORT_ICON_WIDTH:PORT_ICON_SFP_WIDTH,
                      PORT_ICON_HEIGHT,
                      dir == 1 ? -1 : 1);
        cyg_httpd_write_chunked(p->outbuffer, ct);
    }

    /* LNTN LED System Status */
    cyg_httpd_write_chunked("#", 1);
    for(led_type = MEBA_LED_TYPE_DEBUG; led_type < MEBA_LED_TYPE_MAX; led_type++) {
        if(MESA_RC_OK == lntn_sysled_get_color((meba_led_type_t)led_type, &led_color)) {
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d/%s/%d/%d/%d/%d|",
                        led_type,
                        led_color == MEBA_LED_COLOR_YELLOW? "sys_led_red.png": led_color == MEBA_LED_COLOR_GREEN? "sys_led_green.png": "none",
                        pconf.led_xoff[led_type],
                        pconf.led_yoff[led_type],
                        PORT_LED_WIDTH,
                        PORT_LED_HEIGHT);
            cyg_httpd_write_chunked(p->outbuffer, ct);
        }
    }
    cyg_httpd_write_chunked(",", 1);
}
#else
void stat_portstate_switch_lu26(CYG_HTTPD_STATE* p, vtss_usid_t usid, vtss_isid_t isid)
{
    mesa_port_no_t     iport;
    vtss_uport_no_t    uport;
    vtss_appl_port_conf_t conf;
    vtss_appl_port_status_t port_status;
    int                ct, xoff, yoff, row, col;
    char               combo_port = 0;
    char               sfp_port = 0;
    BOOL               lu26 = (vtss_board_type() == VTSS_BOARD_LUTON26_REF);
    u32                port_cnt = mesa_port_cnt(NULL);

    /* Backround, SID, managed */
    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d|%s|",
                  usid,
                  lu26 ? "switch_enzo.png" : "switch_small.png");
    cyg_httpd_write_chunked(p->outbuffer, ct);

    /* Emit port icons */
    for (iport = 0; iport < port_cnt; iport++) {
        uport = iport2uport(iport);

        vtss_ifindex_t ifindex;
        if (vtss_ifindex_from_port(isid, iport, &ifindex) != VTSS_RC_OK) {
            return;
        }

        if(vtss_appl_port_conf_get(ifindex, &conf) < 0 ||
            vtss_appl_port_status_get(ifindex, &port_status) < 0)
            break;              /* Most likely stack error - bail out */

        const char *state = conf.admin.enable ? (port_status.status.link ? "link" : "down") : "disabled";
        const char *speed = conf.admin.enable ?
            (port_status.status.link ?
             port_mgmt_mode_txt(iport, port_status.status.speed, port_status.status.fdx, port_status.status.fiber) :
             "Down") : "Disabled";
        if (port_custom_table[iport].cap & MEBA_PORT_CAP_SFP_DETECT) {
            if(lu26) {
                row = 0;
                col = 18 + (iport + 2 - mesa_port_cnt(nullptr)); // 2x SFP ports
                xoff = PORT_ICON_WIDTH + col * PORT_ICON_WIDTH + 20;
                yoff = PORT_ICON_HEIGHT * (2 - row);
            } else {
                row = 0;
                col = 10 + sfp_port;
                xoff = PORT_ICON_WIDTH + col * PORT_ICON_WIDTH + (12 * (col / 4 + (sfp_port++)));
                yoff = PORT_ICON_HEIGHT * 2;
            }
        } else {
            if(lu26) {
                /* Two columns */
                row = iport % 2;
                col = iport / 2;
                xoff = PORT_ICON_WIDTH + col * PORT_ICON_WIDTH + (12 * (col / 6));
                yoff = PORT_ICON_HEIGHT * (2 - row);
            } else {
                /* Single column */
                row = 0;
                col = iport;
                xoff = PORT_ICON_WIDTH + col * PORT_ICON_WIDTH + (12 * (col / 4));
                yoff = PORT_ICON_HEIGHT * 2;
            }
        }

        // If the active link is fiber and the port has link then
        // change the state to down for the copper port.
        if (port_custom_table[iport].cap & (MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER) &&
            port_status.status.fiber && port_status.status.link)
            state = "down";

        // Update the copper ports
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                      "%u/jack_%s_%s_%s.png/Port %u: %s/%u/%u/%u/%u/%d|",
                      uport,
                      port_type(iport),
                      state,
                      lu26 && row != 0 ? "top" : "bottom",
                      uport, speed,
                      xoff,     /* xoff */
                      yoff,     /* yoff */
                      PORT_ICON_WIDTH,
                      PORT_ICON_HEIGHT,
                      row == 0 ? 1 : -1);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        // Update the fiber ports
        if (port_custom_table[iport].cap & (MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER)) {
            col = 12 + (combo_port);
            xoff = PORT_ICON_WIDTH + col * PORT_ICON_WIDTH + (12 * (col / 4));
            yoff = PORT_ICON_HEIGHT * 2;
            combo_port++;

            // If the active link is cobber and the port has link then
            // change the state to down for the fiber port.
            if (!conf.admin.enable) {
                state = "disabled";
            } else if (!port_status.status.fiber || !port_status.status.link) {
                state = "down";
            } else {
                state = "link";
            }

            // Transmit fiber port infomation.
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                          "%u/jack_%s_%s_%s.png/Port %u: %s/%u/%u/%u/%u/%d|",
                          uport,
                          "sfp",
                          state,
                          "bottom",
                          uport, speed,
                          xoff,     /* xoff */
                          yoff,     /* yoff */
                          PORT_ICON_WIDTH,
                          PORT_ICON_HEIGHT,
                          1);
            cyg_httpd_write_chunked(p->outbuffer, ct);
        }
    }

    cyg_httpd_write_chunked(",", 1);
}
#endif
