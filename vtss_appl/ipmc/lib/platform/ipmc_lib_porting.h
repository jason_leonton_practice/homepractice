/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#ifndef _IPMC_LIB_PORTING_H_
#define _IPMC_LIB_PORTING_H_

#include "ipmc_lib_porting_defs.h"

typedef enum {
    IPMC_ACTIVATE_DEP = 0,
    IPMC_ACTIVATE_RUN,
    IPMC_ACTIVATE_OFF
} ipmc_activate_t;

/* Messages IDs */
typedef enum {
    IPMCLIB_MSG_ID_PROFILE_INIT_DONE,       /* Indicate all profile related databases are initialized */
    IPMCLIB_MSG_ID_PROFILE_CLEAR_REQ,       /* Clear all profile related databases */
    IPMCLIB_MSG_ID_FLTR_STATE_SET_REQ,      /* Set Control State */
    IPMCLIB_MSG_ID_FLTR_ENTRY_SET_REQ,      /* Set Entry Object */
    IPMCLIB_MSG_ID_FLTR_PROFILE_SET_REQ,    /* Set Profile Object */
    IPMCLIB_MSG_ID_FLTR_RULE_SET_REQ        /* Set Rule Object */
} ipmc_lib_msg_id_t;

typedef enum {
    IPMC_LOG_TYPE_NONE = 0,
    IPMC_LOG_TYPE_PF,
    IPMC_LOG_TYPE_MSG
} ipmc_lib_log_type_t;

#define IPMC_LIB_MSG_REQ_BUFS               1
#define IPMC_LIB_MSG_ISID_VALID_SLV(x)      (!ipmc_lib_isid_is_local((x)) && msg_switch_exists((x)))
#define IPMC_LIB_MSG_ISID_VALID_ALL(x)      (msg_switch_exists((x)))
#define IPMC_LIB_MSG_ISID_PASS_SLV(x, y)    (IPMC_LIB_MSG_ISID_VALID_SLV((y)) ? (((x) != VTSS_ISID_GLOBAL) ? ((x) == (y)) : TRUE) : FALSE)
#define IPMC_LIB_MSG_ISID_PASS_ALL(x, y)    (IPMC_LIB_MSG_ISID_VALID_ALL((y)) ? (((x) != VTSS_ISID_GLOBAL) ? ((x) == (y)) : TRUE) : FALSE)

/* Request message */
typedef struct {
    /* Message ID */
    ipmc_lib_msg_id_t                   msg_id;

    /* Message data */
    union {
        /* IPMCLIB_MSG_ID_PROFILE_INIT_DONE */
        struct {
            BOOL                        flag;
        } init_notify;

        /* IPMCLIB_MSG_ID_PROFILE_CLEAR_REQ */
        struct {
            BOOL                        clr_cfg;
        } clear_ctrl;

        /* IPMCLIB_MSG_ID_FLTR_STATE_SET_REQ */
        struct {
            BOOL                        mode;
        } ctrl_state;

        /* IPMCLIB_MSG_ID_FLTR_ENTRY_SET_REQ */
        struct {
            ipmc_operation_action_t     action;
            ipmc_lib_grp_fltr_entry_t   entry;
        } pf_entry;

        /* IPMCLIB_MSG_ID_FLTR_PROFILE_SET_REQ */
        struct {
            ipmc_operation_action_t     action;
            ipmc_lib_profile_t          data;
        } pf_data;

        /* IPMCLIB_MSG_ID_FLTR_RULE_SET_REQ */
        struct {
            ipmc_operation_action_t     action;
            u32                         pf_idx;
            ipmc_lib_rule_t             rule;
        } pf_rule;
    } req;
} ipmc_lib_msg_req_t;

typedef struct {
    ipmc_lib_log_type_t     type;

    mesa_vid_t              vid;
    u8                      port;
    ipmc_ip_version_t       version;
    mesa_ipv6_t             *dst;
    mesa_ipv6_t             *src;

    union {
        struct {
            ipmc_action_t   action;
            char            *name;
            char            *entry;
        } profile;

        struct {
            char            *data;
        } message;
    } event;
} ipmc_lib_log_t;

typedef struct {
    int  totalmem;
    int  freemem;
    int  size;
    int  blocksize;
    int  maxfree;   // The largest free block
} ipmc_lib_memory_info_t;
#ifdef __cplusplus
extern "C" {
#endif
u8 *ipmc_lib_memory_allocate(size_t size);
BOOL ipmc_lib_memory_free(u8 *ptr);

BOOL ipmc_lib_unregistered_flood_set(ipmc_owner_t owner,
                                     ipmc_activate_t activate,
                                     mesa_port_list_t member);
BOOL ipmc_lib_mc6_ctrl_flood_set(ipmc_owner_t owner, BOOL status);
mesa_rc vtss_ipmc_lib_rx_unregister(ipmc_ip_version_t version);
mesa_rc vtss_ipmc_lib_rx_register(BOOL (*cb)(void *contxt, const u8 *const frm, const mesa_packet_rx_info_t *const rx_info), ipmc_ip_version_t version);

BOOL ipmc_lib_log(const ipmc_lib_log_t *content, const ipmc_log_severity_t severity);
#ifdef __cplusplus
}
#endif

#endif /* _IPMC_LIB_PORTING_H_ */
