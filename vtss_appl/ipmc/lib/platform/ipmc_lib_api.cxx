/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "main.h"
#include "critd_api.h"
#include "msg_api.h"
#include "misc_api.h"

#include "ipmc_lib.h"
#include "ipmc_lib_api.h"
#include "ipmc_lib_porting.h"
#include "vtss/appl/ipmc_profile.h"

#include "ipmc_lib_conf.h"
#ifdef VTSS_SW_OPTION_ICFG
#include "ipmc_lib_icfg.h"
#endif /* VTSS_SW_OPTION_ICFG */
#include "ipmc_trace.h"
#if defined(VTSS_SW_OPTION_MVR)
#include "mvr_api.h"
#endif /* VTSS_SW_OPTION_MVR */

/* ************************************************************************ **
 *
 * Defines
 *
 * ************************************************************************ */
#define VTSS_TRACE_MODULE_ID        VTSS_MODULE_ID_IPMC_LIB


/* ************************************************************************ **
 *
 * Public data
 *
 * ************************************************************************ */

/* ************************************************************************ **
 *
 * Local data
 *
 * ************************************************************************ */
#if VTSS_TRACE_ENABLED
static vtss_trace_reg_t             ipmc_lib_trace_reg = {
    VTSS_MODULE_ID_IPMC_LIB, "IPMC_LIB", "IPMC Library"
};
static vtss_trace_grp_t             ipmc_lib_trace_grps[TRACE_GRP_CNT] = {
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CRIT */ {
        "crit",
        "Critical regions",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_RX */ {
        "rx",
        "Packet receive flow",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_TX */ {
        "tx",
        "Packet tramsnit flow",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_QUERIER_STATE */ {
        "querier",
        "Querier state",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    }
};
#endif /* VTSS_TRACE_ENABLED */

static BOOL                         init_local_ipmc_profile = FALSE;
static struct {
    critd_t                         crit;

    ipmc_lib_configuration_t        configuration;

    /* Request message buffer pool */
    void                            *request_buf_pool;

    BOOL                            stackable;
    vtss_isid_t                     my_isid;
} ipmc_lib_global;

#if VTSS_TRACE_ENABLED
#define IPMC_LIB_ENTER()            critd_enter(&ipmc_lib_global.crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define IPMC_LIB_EXIT()             critd_exit(&ipmc_lib_global.crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#else
#define IPMC_LIB_ENTER()            critd_enter(&ipmc_lib_global.crit)
#define IPMC_LIB_EXIT()             critd_exit(&ipmc_lib_global.crit)
#endif /* VTSS_TRACE_ENABLED */

#define IPMC_LIB_STACKABLE          ipmc_lib_global.stackable
#define IPMC_LIB_ISID_INIT          0xFF
#define IPMC_LIB_ISID_ANY           0xFE
#define IPMC_LIB_ISID_ASYNC         0xFD
#define IPMC_LIB_ISID_TRANSIT       VTSS_ISID_START
#define IPMC_LIB_MY_ISID            ipmc_lib_global.my_isid


/* Allocate a message buffer */
static ipmc_lib_msg_req_t *ipmc_lib_msg_alloc(ipmc_lib_msg_id_t msg_id)
{
    ipmc_lib_msg_req_t *msg = (ipmc_lib_msg_req_t *)msg_buf_pool_get(ipmc_lib_global.request_buf_pool);

    VTSS_ASSERT(msg);
    msg->msg_id = msg_id;
    return msg;
}

/* Release stack message buffer */
static void ipmc_lib_msg_tx_done(void *contxt, void *msg, msg_tx_rc_t rc)
{
    (void) msg_buf_pool_put(msg);
}

/* Transmit message buffer */
static void ipmc_lib_msg_tx(void *msg, vtss_isid_t isid, size_t len)
{
    msg_tx_adv(NULL, ipmc_lib_msg_tx_done, MSG_TX_OPT_DONT_FREE, VTSS_MODULE_ID_IPMC_LIB, isid, msg, len + MSG_TX_DATA_HDR_LEN(ipmc_lib_msg_req_t, req));
}

static mesa_rc
_ipmc_lib_clear_running_profile(void)
{
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *profile, *p;
    ipmc_lib_grp_fltr_entry_t   *entry, *q;
    mesa_rc                     rc;

    if (!IPMC_MEM_PROFILE_MTAKE(pfm)) {
        T_D("Failed in IPMC_MEM_PROFILE_MTAKE()");
        return VTSS_RC_ERROR;
    }
    if (!IPMC_MEM_SYSTEM_MTAKE(entry, sizeof(ipmc_lib_grp_fltr_entry_t))) {
        IPMC_MEM_PROFILE_MGIVE(pfm);
        T_D("Failed in IPMC_MEM_SYSTEM_MTAKE()");
        return VTSS_RC_ERROR;
    }

    rc = VTSS_RC_OK;
    profile = &pfm->profile;
    memset(profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
    memset(entry, 0x0, sizeof(ipmc_lib_grp_fltr_entry_t));

    IPMC_LIB_ENTER();
    while ((p = ipmc_lib_fltr_profile_get_next(profile, FALSE)) != NULL) {
        memcpy(profile, p, sizeof(ipmc_lib_grp_fltr_profile_t));
        rc |= ipmc_lib_fltr_profile_set(IPMC_OP_DEL, profile);
    }

    while ((q = ipmc_lib_fltr_entry_get_next(entry, FALSE)) != NULL) {
        memcpy(entry, q, sizeof(ipmc_lib_grp_fltr_entry_t));
        rc |= ipmc_lib_fltr_entry_set(IPMC_OP_DEL, entry);
    }
    IPMC_LIB_EXIT();

    IPMC_MEM_SYSTEM_MGIVE(entry);
    IPMC_MEM_PROFILE_MGIVE(pfm);

    T_I("Complete with RC=%d", rc);
    return rc;
}

/* Receive stack message */
static BOOL ipmc_lib_msg_rx(void *contxt, const void *const rx_msg, const size_t len, const vtss_module_id_t modid, const u32 isid)
{
    ipmc_lib_msg_req_t          *msg;
    mesa_rc                     rc;
    BOOL                        mode, bflg;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *profile, *p;
    ipmc_lib_rule_t             *r;
    ipmc_time_t                 exe_time_base, exe_time_diff;

    (void) ipmc_lib_time_curr_get(&exe_time_base);

    msg = (ipmc_lib_msg_req_t *)rx_msg;
    if (!msg) {
        return FALSE;
    }
    T_D("id: %d, len: %zd, isid: %u", msg->msg_id, len, isid);

    if (msg_switch_is_master()) {
        return TRUE;
    }

    rc = VTSS_RC_ERROR;
    switch ( msg->msg_id ) {
    case IPMCLIB_MSG_ID_PROFILE_INIT_DONE:
        bflg = msg->req.init_notify.flag;

        T_D("INIT Flag: %s", bflg ? "TRUE" : "FALSE");

        IPMC_LIB_ENTER();
        init_local_ipmc_profile = bflg;
        IPMC_LIB_EXIT();

        rc = VTSS_RC_OK;

        break;
    case IPMCLIB_MSG_ID_PROFILE_CLEAR_REQ:
        bflg = msg->req.clear_ctrl.clr_cfg;

        T_D("Clear Flag: %s", bflg ? "TRUE" : "FALSE");

        if (bflg) {
            rc = _ipmc_lib_clear_running_profile();
        } else {
            rc = VTSS_RC_OK;
        }

        T_D("Complete with RC=%d", rc);

        break;
    case IPMCLIB_MSG_ID_FLTR_STATE_SET_REQ:
        IPMC_LIB_ENTER();
        rc = ipmc_lib_profile_state_get(&mode);
        if ((rc == VTSS_OK) && (mode == msg->req.ctrl_state.mode)) {
            IPMC_LIB_EXIT();
            break;
        }

        mode = msg->req.ctrl_state.mode;
        rc = ipmc_lib_profile_state_set(mode);
        IPMC_LIB_EXIT();

        break;
    case IPMCLIB_MSG_ID_FLTR_ENTRY_SET_REQ:
        T_D("%s %s", ipmc_lib_op_action_txt(msg->req.pf_entry.action), msg->req.pf_entry.entry.name);

        IPMC_LIB_ENTER();
        rc = ipmc_lib_fltr_entry_set(msg->req.pf_entry.action, &msg->req.pf_entry.entry);
        IPMC_LIB_EXIT();

        break;
    case IPMCLIB_MSG_ID_FLTR_PROFILE_SET_REQ:
        T_D("%s %s", ipmc_lib_op_action_txt(msg->req.pf_data.action), msg->req.pf_data.data.name);

        if (!IPMC_MEM_PROFILE_MTAKE(pfm)) {
            break;
        }

        profile = &pfm->profile;
        memcpy(&profile->data, &msg->req.pf_data.data, sizeof(ipmc_lib_profile_t));

        IPMC_LIB_ENTER();
        if ((p = ipmc_lib_fltr_profile_get(profile, FALSE)) != NULL) {
            memcpy(profile->rule, p->rule, sizeof(p->rule));
        } else {
            u32 rdx;

            memset(profile->rule, 0x0, sizeof(profile->rule));
            for (rdx = 0; rdx < IPMC_LIB_FLTR_ENTRY_MAX_CNT; rdx++) {
                r = &profile->rule[rdx];

                r->idx = rdx;
                r->entry_index = IPMC_LIB_FLTR_RULE_IDX_INIT;
                r->next_rule_idx = IPMC_LIB_FLTR_RULE_IDX_INIT;
            }
        }

        rc = ipmc_lib_fltr_profile_set(msg->req.pf_data.action, profile);
        IPMC_LIB_EXIT();
        IPMC_MEM_PROFILE_MGIVE(pfm);

        break;
    case IPMCLIB_MSG_ID_FLTR_RULE_SET_REQ:
        T_D("%s PDX:%u/RDX:%u/EDX:%u/NDX:%u/%s/%s",
            ipmc_lib_op_action_txt(msg->req.pf_rule.action),
            msg->req.pf_rule.pf_idx,
            msg->req.pf_rule.rule.idx,
            msg->req.pf_rule.rule.entry_index,
            msg->req.pf_rule.rule.next_rule_idx,
            ipmc_lib_fltr_action_txt(msg->req.pf_rule.rule.action, IPMC_TXT_CASE_CAPITAL),
            msg->req.pf_rule.rule.log ? "LOG" : "BYPASS");

        IPMC_LIB_ENTER();
        rc = ipmc_lib_fltr_profile_rule_set(msg->req.pf_rule.action, msg->req.pf_rule.pf_idx, &msg->req.pf_rule.rule);
        IPMC_LIB_EXIT();

        break;
    default:
        T_E("illegal msg_id: %d", msg->msg_id);

        break;
    }

    (void) ipmc_lib_time_diff_get(TRUE, FALSE, "IPMC_LIB_MSG_RX", &exe_time_base, &exe_time_diff);

    if (rc == VTSS_RC_OK) {
        return TRUE;
    } else {
        return FALSE;
    }
}

static void ipmc_lib_conf_default(void)
{
    u32                             i, j;
    ipmc_lib_conf_profile_t         *profile;
    ipmc_lib_conf_fltr_entry_t      *pf_entry;
    ipmc_lib_conf_fltr_profile_t    *pf_object;
    ipmc_lib_profile_t              *pf_data;
    ipmc_lib_rule_t                 *pf_rule;
    ipmc_lib_configuration_t        *conf;

    IPMC_LIB_ENTER();

    conf = &ipmc_lib_global.configuration;

    /* Use default configuration */
    memset(conf, 0x0, sizeof(ipmc_lib_configuration_t));
    profile = &conf->profile;
    profile->global_ctrl_state = IPMC_LIB_FLTR_PROFILE_DEF_STATE;

    for (i = 0; i < IPMC_LIB_FLTR_ENTRY_MAX_CNT; i++) {
        pf_entry = &profile->fltr_entry_pool[i];

        pf_entry->index = i + 1;
        pf_entry->version = IPMC_IP_VERSION_INIT;
    }

    for (i = 0; i < IPMC_LIB_FLTR_PROFILE_MAX_CNT; i++) {
        pf_object = &profile->fltr_profile_pool[i];
        pf_data = &pf_object->data;

        pf_data->index = i + 1;
        pf_data->rule_head_idx = IPMC_LIB_FLTR_RULE_IDX_INIT;
        pf_data->version = IPMC_IP_VERSION_INIT;

        for (j = 0; j < IPMC_LIB_FLTR_ENTRY_MAX_CNT; j++) {
            pf_rule = &pf_object->rule[j];

            pf_rule->idx = j;
            pf_rule->entry_index = IPMC_LIB_FLTR_RULE_IDX_INIT;
            pf_rule->next_rule_idx = IPMC_LIB_FLTR_RULE_IDX_INIT;
        }
    }

    IPMC_LIB_EXIT();
}

static mesa_rc ipmc_lib_stacking_profile_entry_set(vtss_isid_t isid_add,
                                                   ipmc_operation_action_t action,
                                                   ipmc_lib_grp_fltr_entry_t *entry)
{
    ipmc_lib_msg_req_t  *msg;
    switch_iter_t       sit;
    vtss_isid_t         isid;

    if (!entry || !msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if (!IPMC_LIB_MSG_ISID_PASS_SLV(isid_add, isid)) {
            continue;
        }

        /* Allocate and send message */
        T_D("sending to isid %d %s %s", isid, ipmc_lib_op_action_txt(action), entry->name);
        msg = ipmc_lib_msg_alloc(IPMCLIB_MSG_ID_FLTR_ENTRY_SET_REQ);
        msg->req.pf_entry.action = action;
        memcpy(&msg->req.pf_entry.entry, entry, sizeof(ipmc_lib_grp_fltr_entry_t));
        ipmc_lib_msg_tx(msg, isid, sizeof(msg->req.pf_entry));
    }

    return VTSS_OK;
}

static mesa_rc ipmc_lib_stacking_profile_rule_set(vtss_isid_t isid_add,
                                                  ipmc_operation_action_t action,
                                                  u32 pdx,
                                                  ipmc_lib_rule_t *entry)
{
    ipmc_lib_msg_req_t  *msg;
    switch_iter_t       sit;
    vtss_isid_t         isid;

    if (!entry || !msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if (!IPMC_LIB_MSG_ISID_PASS_SLV(isid_add, isid)) {
            continue;
        }

        /* Allocate and send message */
        T_D("sending to isid %d %s PDX:%u/RDX:%u/EDX:%u/NDX:%u/%s/%s",
            isid, ipmc_lib_op_action_txt(action), pdx,
            entry->idx,
            entry->entry_index,
            entry->next_rule_idx,
            ipmc_lib_fltr_action_txt(entry->action, IPMC_TXT_CASE_CAPITAL),
            entry->log ? "LOG" : "BYPASS");

        msg = ipmc_lib_msg_alloc(IPMCLIB_MSG_ID_FLTR_RULE_SET_REQ);
        msg->req.pf_rule.action = action;
        msg->req.pf_rule.pf_idx = pdx;
        memcpy(&msg->req.pf_rule.rule, entry, sizeof(ipmc_lib_rule_t));
        ipmc_lib_msg_tx(msg, isid, sizeof(msg->req.pf_entry));
    }

    return VTSS_OK;
}

static mesa_rc ipmc_lib_stacking_profile_object_set(vtss_isid_t isid_add,
                                                    ipmc_operation_action_t action,
                                                    ipmc_lib_grp_fltr_profile_t *entry)
{
    ipmc_lib_msg_req_t  *msg;
    switch_iter_t       sit;
    vtss_isid_t         isid;

    if (!entry || !msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if (!IPMC_LIB_MSG_ISID_PASS_SLV(isid_add, isid)) {
            continue;
        }

        /* Allocate and send message */
        T_D("sending to isid %d %s %s", isid, ipmc_lib_op_action_txt(action), entry->data.name);
        msg = ipmc_lib_msg_alloc(IPMCLIB_MSG_ID_FLTR_PROFILE_SET_REQ);
        msg->req.pf_data.action = action;
        memcpy(&msg->req.pf_data.data, &entry->data, sizeof(ipmc_lib_profile_t));
        ipmc_lib_msg_tx(msg, isid, sizeof(msg->req.pf_data));
    }

    return VTSS_OK;
}

static mesa_rc ipmc_lib_stacking_profile_mode_set(vtss_isid_t isid_add, BOOL mode)
{
    ipmc_lib_msg_req_t  *msg;
    switch_iter_t       sit;
    vtss_isid_t         isid;

    if (!msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if (!IPMC_LIB_MSG_ISID_PASS_SLV(isid_add, isid)) {
            continue;
        }

        /* Allocate and send message */
        T_D("sending to isid %d %s", isid, mode ? "Enable" : "Disable");
        msg = ipmc_lib_msg_alloc(IPMCLIB_MSG_ID_FLTR_STATE_SET_REQ);
        msg->req.ctrl_state.mode = mode;
        ipmc_lib_msg_tx(msg, isid, sizeof(msg->req.ctrl_state));
    }

    return VTSS_OK;
}

static void ipmc_lib_stacking_profile_initialized(vtss_isid_t isid, BOOL flag)
{
    ipmc_lib_msg_req_t  *msg;

    if (!msg_switch_is_master() || ipmc_lib_isid_is_local(isid)) {
        return;
    }

    /* Allocate and send message */
    T_I("sending to isid %d", isid);
    msg = ipmc_lib_msg_alloc(IPMCLIB_MSG_ID_PROFILE_INIT_DONE);
    msg->req.init_notify.flag = flag;
    ipmc_lib_msg_tx(msg, isid, sizeof(msg->req.init_notify));
}

static void ipmc_lib_stacking_profile_clear(vtss_isid_t isid, BOOL clr_cfg)
{
    ipmc_lib_msg_req_t  *msg;

    if (!msg_switch_is_master() || ipmc_lib_isid_is_local(isid)) {
        return;
    }

    /* Allocate and send message */
    T_I("sending to isid %d", isid);
    msg = ipmc_lib_msg_alloc(IPMCLIB_MSG_ID_PROFILE_CLEAR_REQ);
    msg->req.clear_ctrl.clr_cfg = clr_cfg;
    ipmc_lib_msg_tx(msg, isid, sizeof(msg->req.clear_ctrl));
}

static void ipmc_lib_stacking_switch_add_set(vtss_isid_t isid)
{
    mesa_rc                     rc;
    u32                         pdx;
    BOOL                        ctrl_state, do_local;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *fltr_profile, *p;
    ipmc_lib_rule_t             fltr_rule, *q;

    if (!msg_switch_is_master() || !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        return;
    }

    fltr_profile = &pfm->profile;
    do_local = FALSE;
    if (ipmc_lib_isid_is_local(isid)) {
        IPMC_LIB_ENTER();
        if (init_local_ipmc_profile) {
            IPMC_LIB_EXIT();
            IPMC_MEM_PROFILE_MGIVE(pfm);
            return;
        }

        do_local = init_local_ipmc_profile = TRUE;
        IPMC_LIB_EXIT();
    }

    IPMC_LIB_ENTER();
    rc = ipmc_lib_profile_state_get(&ctrl_state);
    IPMC_LIB_EXIT();
    if (rc == VTSS_OK) {
        (void) ipmc_lib_stacking_profile_mode_set(isid, ctrl_state);
    }

    if (!do_local) {
        ipmc_lib_grp_fltr_entry_t   entry, *ptr;

        ipmc_lib_stacking_profile_clear(isid, TRUE);

        memset(&entry, 0x0, sizeof(ipmc_lib_grp_fltr_entry_t));
        IPMC_LIB_ENTER();
        while ((ptr = ipmc_lib_fltr_entry_get_next(&entry, FALSE)) != NULL) {
            memcpy(&entry, ptr, sizeof(ipmc_lib_grp_fltr_entry_t));
            IPMC_LIB_EXIT();
            if (ipmc_lib_stacking_profile_entry_set(isid, IPMC_OP_ADD, &entry) != VTSS_RC_OK) {
                T_D("Failed to add isid %u profile %s", isid, entry.name);
            }
            IPMC_LIB_ENTER();
        }
        IPMC_LIB_EXIT();
    }

    memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
    p = fltr_profile;
    IPMC_LIB_ENTER();
    while ((p = ipmc_lib_fltr_profile_get_next(p, FALSE)) != NULL) {
        pdx = p->data.index;
        if (do_local) {
            rc = ipmc_lib_fltr_profile_set(IPMC_OP_SET, p);
        } else {
            memcpy(fltr_profile, p, sizeof(ipmc_lib_grp_fltr_profile_t));
            IPMC_LIB_EXIT();
            rc = ipmc_lib_stacking_profile_object_set(isid, IPMC_OP_ADD, fltr_profile);
            IPMC_LIB_ENTER();
        }

        if ((rc == VTSS_OK) && !do_local) {
            if ((q = ipmc_lib_fltr_profile_rule_get_first(pdx)) != NULL) {
                do {
                    memcpy(&fltr_rule, q, sizeof(ipmc_lib_rule_t));
                    IPMC_LIB_EXIT();
                    /*
                        In order to pass _ipmc_lib_fltr_rule_sanity checking in slave's initialization,
                        Let the rules to be added one after one by not assigning the next rule idx
                    */
                    fltr_rule.next_rule_idx = IPMC_LIB_FLTR_RULE_IDX_INIT;

                    (void) ipmc_lib_stacking_profile_rule_set(isid, IPMC_OP_ADD, pdx, &fltr_rule);
                    IPMC_LIB_ENTER();
                } while ((q = ipmc_lib_fltr_profile_rule_get_next(pdx, &fltr_rule)) != NULL);
            }
        }
    }
    IPMC_LIB_EXIT();

    IPMC_MEM_PROFILE_MGIVE(pfm);

    if (!do_local) {
        ipmc_lib_stacking_profile_initialized(isid, TRUE);
    }
}

/* Set Global Filtering Profile State */
mesa_rc ipmc_lib_mgmt_profile_state_set(BOOL profiling)
{
    mesa_rc rc;
    BOOL    mode;

    if (!msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    IPMC_LIB_ENTER();
    rc = ipmc_lib_profile_state_get(&mode);
    if ((rc == VTSS_OK) && (mode == profiling)) {
        IPMC_LIB_EXIT();
        return VTSS_OK;
    }
    rc = ipmc_lib_profile_state_set(profiling);
    IPMC_LIB_EXIT();

    if (rc == VTSS_OK) {
        if (ipmc_lib_stacking_profile_mode_set(VTSS_ISID_GLOBAL, profiling) != VTSS_RC_OK) {
            T_D("ipmc_lib_stacking_profile_mode_set(FAIL)");
        }
    }

#if defined(VTSS_SW_OPTION_MVR)
    /* Clear the MVR database when the global profile mode is changed
     * from 'Enabled' to 'Disabled'
     */
    if (!profiling && mode != profiling) {
        BOOL mvr_mode;
        if (mvr_mgmt_get_mode(&mvr_mode) == VTSS_OK && mvr_mode) { // Current MVR global mode is enabled
            // Clear the MVR database by re-enable MVR global mode
            mvr_mode = FALSE;
            (void) mvr_mgmt_set_mode(&mvr_mode);
            mvr_mode = TRUE;
            (void) mvr_mgmt_set_mode(&mvr_mode);
        }
    }
#endif /* VTSS_SW_OPTION_MVR */

    return rc;
}

/* Get Global Filtering Profile State */
mesa_rc ipmc_lib_mgmt_profile_state_get(BOOL *profiling)
{
    mesa_rc rc;

    IPMC_LIB_ENTER();
    rc = ipmc_lib_profile_state_get(profiling);
    IPMC_LIB_EXIT();

    return rc;
}

/* Add/Delete/Update IPMC Profile Entry */
mesa_rc ipmc_lib_mgmt_fltr_entry_set(ipmc_operation_action_t action, ipmc_lib_grp_fltr_entry_t *fltr_entry)
{
    mesa_rc                     rc;
    ipmc_lib_grp_fltr_entry_t   entry, *ptr;
    ipmc_operation_action_t     op;

    if (!fltr_entry || !msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    if (!strlen(fltr_entry->name)) {
        return VTSS_RC_ERROR;
    }

    op = action;
    memcpy(&entry, fltr_entry, sizeof(ipmc_lib_grp_fltr_entry_t));
    IPMC_LIB_ENTER();
    ptr = ipmc_lib_fltr_entry_get(&entry, TRUE);

    if (action != IPMC_OP_SET) {
        if (ptr != NULL) {
            if (action == IPMC_OP_ADD) {
                IPMC_LIB_EXIT();
                return VTSS_RC_ERROR;
            }
        } else {
            if (action != IPMC_OP_ADD) {
                IPMC_LIB_EXIT();
                return VTSS_RC_ERROR;
            }
        }
    } else {
        if (ptr != NULL) {
            op = IPMC_OP_UPD;
        } else {
            op = IPMC_OP_ADD;
        }
    }

    if (op == IPMC_OP_ADD) {
        ptr = &entry;
    } else if (op == IPMC_OP_UPD) {
        if (ptr == NULL) {
            IPMC_LIB_EXIT();
            return VTSS_RC_ERROR;
        }

        if (memcmp(ptr->name, fltr_entry->name, sizeof(fltr_entry->name))) {
            IPMC_LIB_EXIT();
            return VTSS_RC_ERROR;
        }

        if ((ptr->version != fltr_entry->version) ||
            IPMC_LIB_ADRS_CMP6(ptr->grp_bgn, fltr_entry->grp_bgn) ||
            IPMC_LIB_ADRS_CMP6(ptr->grp_end, fltr_entry->grp_end)) {
            ptr = &entry;
        } else {
            IPMC_LIB_EXIT();
            return VTSS_OK;
        }
    } else if (op == IPMC_OP_DEL) {
        ptr = &entry;
    } else {
        IPMC_LIB_EXIT();
        return VTSS_RC_ERROR;
    }

    rc = ipmc_lib_fltr_entry_set(op, ptr);
    IPMC_LIB_EXIT();

    if (rc == VTSS_OK) {
        if (ipmc_lib_stacking_profile_entry_set(VTSS_ISID_GLOBAL, op, ptr) != VTSS_RC_OK) {
            T_D("ipmc_lib_stacking_profile_entry_set(FAIL)");
        }
    }

    return rc;
}

/* Get IPMC Profile Entry */
mesa_rc ipmc_lib_mgmt_fltr_entry_get(ipmc_lib_grp_fltr_entry_t *fltr_entry, BOOL by_name)
{
    mesa_rc                     rc = VTSS_RC_ERROR;
    ipmc_lib_grp_fltr_entry_t   entry, *ptr;

    if (!fltr_entry) {
        return rc;
    }

    memcpy(&entry, fltr_entry, sizeof(ipmc_lib_grp_fltr_entry_t));
    IPMC_LIB_ENTER();
    if ((ptr = ipmc_lib_fltr_entry_get(&entry, by_name)) != NULL) {
        rc = VTSS_OK;
        memcpy(fltr_entry, ptr, sizeof(ipmc_lib_grp_fltr_entry_t));
    }
    IPMC_LIB_EXIT();

    return rc;
}

/* GetNext IPMC Profile Entry */
mesa_rc ipmc_lib_mgmt_fltr_entry_get_next(ipmc_lib_grp_fltr_entry_t *fltr_entry, BOOL by_name)
{
    mesa_rc                     rc = VTSS_RC_ERROR;
    ipmc_lib_grp_fltr_entry_t   entry, *ptr;

    if (!fltr_entry) {
        return rc;
    }

    memcpy(&entry, fltr_entry, sizeof(ipmc_lib_grp_fltr_entry_t));
    IPMC_LIB_ENTER();
    if ((ptr = ipmc_lib_fltr_entry_get_next(&entry, by_name)) != NULL) {
        rc = VTSS_OK;
        memcpy(fltr_entry, ptr, sizeof(ipmc_lib_grp_fltr_entry_t));
    }
    IPMC_LIB_EXIT();

    return rc;
}

/* Add/Delete/Update IPMC Profile */
mesa_rc ipmc_lib_mgmt_fltr_profile_set(ipmc_operation_action_t action, ipmc_lib_grp_fltr_profile_t *fltr_profile)
{
    mesa_rc                     rc;
    ipmc_lib_profile_t          *data;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *ptr;
    ipmc_operation_action_t     op;

    if (!fltr_profile || !msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    data = &fltr_profile->data;
    if (!strlen(data->name)) {
        return VTSS_RC_ERROR;
    }

    if (!IPMC_MEM_PROFILE_MTAKE(pfm)) {
        return VTSS_RC_ERROR;
    }

    op = action;
    memcpy(&pfm->profile.data, data, sizeof(ipmc_lib_profile_t));
    IPMC_LIB_ENTER();
    ptr = ipmc_lib_fltr_profile_get(&pfm->profile, TRUE);

    if (action != IPMC_OP_SET) {
        if (ptr != NULL) {
            if (action == IPMC_OP_ADD) {
                IPMC_LIB_EXIT();
                IPMC_MEM_PROFILE_MGIVE(pfm);
                return VTSS_RC_ERROR;
            }
        } else {
            if (action != IPMC_OP_ADD) {
                IPMC_LIB_EXIT();
                IPMC_MEM_PROFILE_MGIVE(pfm);
                return VTSS_RC_ERROR;
            }
        }
    } else {
        if (ptr != NULL) {
            op = IPMC_OP_UPD;
        } else {
            op = IPMC_OP_ADD;
        }
    }

    if (op == IPMC_OP_ADD) {
        ptr = &pfm->profile;
        data = &ptr->data;
        data->rule_head_idx = IPMC_LIB_FLTR_RULE_IDX_INIT;
        data->version = IPMC_IP_VERSION_INIT;
    } else if (op == IPMC_OP_UPD) {
        ipmc_lib_profile_t  *p;

        if (ptr == NULL) {
            IPMC_LIB_EXIT();
            IPMC_MEM_PROFILE_MGIVE(pfm);
            return VTSS_RC_ERROR;
        }

        data = &ptr->data;
        if (memcmp(data->name, fltr_profile->data.name, sizeof(fltr_profile->data.name))) {
            IPMC_LIB_EXIT();
            IPMC_MEM_PROFILE_MGIVE(pfm);
            return VTSS_RC_ERROR;
        }

        p = &fltr_profile->data;
        if (memcmp(data->desc, p->desc, sizeof(p->desc))) {
            ptr = &pfm->profile;
        } else {
            IPMC_LIB_EXIT();
            IPMC_MEM_PROFILE_MGIVE(pfm);
            return VTSS_OK;
        }
    } else if (op == IPMC_OP_DEL) {
        ptr = &pfm->profile;
    } else {
        IPMC_LIB_EXIT();
        IPMC_MEM_PROFILE_MGIVE(pfm);
        return VTSS_RC_ERROR;
    }

    rc = ipmc_lib_fltr_profile_set(op, ptr);
    IPMC_LIB_EXIT();

    if (rc == VTSS_OK) {
        if (ipmc_lib_stacking_profile_object_set(VTSS_ISID_GLOBAL, op, ptr) != VTSS_RC_OK) {
            T_D("ipmc_lib_stacking_profile_object_set(FAIL)");
        }
    }

    IPMC_MEM_PROFILE_MGIVE(pfm);
    return rc;
}

/* Get IPMC Profile */
mesa_rc ipmc_lib_mgmt_fltr_profile_get(ipmc_lib_grp_fltr_profile_t *fltr_profile, BOOL by_name)
{
    mesa_rc                     rc = VTSS_RC_ERROR;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *ptr;

    if (!fltr_profile || !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        return rc;
    }

    memcpy(&pfm->profile, fltr_profile, sizeof(ipmc_lib_grp_fltr_profile_t));
    IPMC_LIB_ENTER();
    if ((ptr = ipmc_lib_fltr_profile_get(&pfm->profile, by_name)) != NULL) {
        rc = VTSS_OK;
        memcpy(fltr_profile, ptr, sizeof(ipmc_lib_grp_fltr_profile_t));
    }
    IPMC_LIB_EXIT();

    IPMC_MEM_PROFILE_MGIVE(pfm);
    return rc;
}

/* GetNext IPMC Profile */
mesa_rc ipmc_lib_mgmt_fltr_profile_get_next(ipmc_lib_grp_fltr_profile_t *fltr_profile, BOOL by_name)
{
    mesa_rc                     rc = VTSS_RC_ERROR;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *ptr;

    if (!fltr_profile || !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        return rc;
    }

    memcpy(&pfm->profile, fltr_profile, sizeof(ipmc_lib_grp_fltr_profile_t));
    IPMC_LIB_ENTER();
    if ((ptr = ipmc_lib_fltr_profile_get_next(&pfm->profile, by_name)) != NULL) {
        rc = VTSS_OK;
        memcpy(fltr_profile, ptr, sizeof(ipmc_lib_grp_fltr_profile_t));
    }
    IPMC_LIB_EXIT();

    IPMC_MEM_PROFILE_MGIVE(pfm);
    return rc;
}

/* Add/Delete/Update IPMC Profile Rule */
mesa_rc ipmc_lib_mgmt_fltr_profile_rule_set(ipmc_operation_action_t action, u32 profile_index, ipmc_lib_rule_t *fltr_rule)
{
    mesa_rc                     rc;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_rule_t             entry, *ptr;
    ipmc_operation_action_t     op;

    if (!fltr_rule ||
        !msg_switch_is_master() ||
        !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        return VTSS_RC_ERROR;
    }

    pfm->profile.data.index = profile_index;
    IPMC_LIB_ENTER();
    if (!ipmc_lib_fltr_profile_get(&pfm->profile, FALSE)) {
        IPMC_LIB_EXIT();
        IPMC_MEM_PROFILE_MGIVE(pfm);
        return VTSS_RC_ERROR;
    }
    IPMC_MEM_PROFILE_MGIVE(pfm);

    op = action;
    memcpy(&entry, fltr_rule, sizeof(ipmc_lib_rule_t));
    ptr = ipmc_lib_fltr_profile_rule_search(profile_index, entry.entry_index);

    if (action != IPMC_OP_SET) {
        if (ptr != NULL) {
            if (action == IPMC_OP_ADD) {
                IPMC_LIB_EXIT();
                return VTSS_RC_ERROR;
            }
        } else {
            if (action != IPMC_OP_ADD) {
                IPMC_LIB_EXIT();
                return VTSS_RC_ERROR;
            }
        }
    } else {
        if (ptr != NULL) {
            op = IPMC_OP_UPD;
        } else {
            op = IPMC_OP_ADD;
        }
    }

    if (op == IPMC_OP_ADD) {
        ptr = &entry;
    } else if (op == IPMC_OP_UPD) {
        if (ptr == NULL) {
            IPMC_LIB_EXIT();
            return VTSS_RC_ERROR;
        }

        if ((ptr->next_rule_idx != fltr_rule->next_rule_idx) ||
            (ptr->action != fltr_rule->action) ||
            (ptr->log != fltr_rule->log)) {
            ptr = &entry;
        } else {
            IPMC_LIB_EXIT();
            return VTSS_OK;
        }
    } else if (op == IPMC_OP_DEL) {
        ptr = &entry;
    } else {
        IPMC_LIB_EXIT();
        return VTSS_RC_ERROR;
    }

    rc = ipmc_lib_fltr_profile_rule_set(op, profile_index, ptr);
    IPMC_LIB_EXIT();

    if (rc == VTSS_OK) {
        if (ipmc_lib_stacking_profile_rule_set(VTSS_ISID_GLOBAL, op, profile_index, ptr) != VTSS_RC_OK) {
            T_D("ipmc_lib_stacking_profile_rule_set(FAIL)");
        }
    }

    return rc;
}

/* Search IPMC Profile Rule by Entry Index */
mesa_rc ipmc_lib_mgmt_fltr_profile_rule_search(u32 profile_index, u32 entry_index, ipmc_lib_rule_t *fltr_rule)
{
    mesa_rc         rc = VTSS_RC_ERROR;
    ipmc_lib_rule_t *ptr;

    if (!fltr_rule) {
        return rc;
    }

    IPMC_LIB_ENTER();
    if ((ptr = ipmc_lib_fltr_profile_rule_search(profile_index, entry_index)) != NULL) {
        rc = VTSS_OK;
        memcpy(fltr_rule, ptr, sizeof(ipmc_lib_rule_t));
    }
    IPMC_LIB_EXIT();

    return rc;
}

/* Get IPMC Profile Rule */
mesa_rc ipmc_lib_mgmt_fltr_profile_rule_get(u32 profile_index, ipmc_lib_rule_t *fltr_rule)
{
    mesa_rc         rc = VTSS_RC_ERROR;
    ipmc_lib_rule_t entry, *ptr;

    if (!fltr_rule) {
        return rc;
    }

    memcpy(&entry, fltr_rule, sizeof(ipmc_lib_rule_t));
    IPMC_LIB_ENTER();
    if ((ptr = ipmc_lib_fltr_profile_rule_get(profile_index, &entry)) != NULL) {
        rc = VTSS_OK;
        memcpy(fltr_rule, ptr, sizeof(ipmc_lib_rule_t));
    }
    IPMC_LIB_EXIT();

    return rc;
}

/* GetFirst IPMC Profile Rule */
mesa_rc ipmc_lib_mgmt_fltr_profile_rule_get_first(u32 profile_index, ipmc_lib_rule_t *fltr_rule)
{
    mesa_rc         rc = VTSS_RC_ERROR;
    ipmc_lib_rule_t *ptr;

    if (!fltr_rule) {
        return rc;
    }

    IPMC_LIB_ENTER();
    if ((ptr = ipmc_lib_fltr_profile_rule_get_first(profile_index)) != NULL) {
        rc = VTSS_OK;
        memcpy(fltr_rule, ptr, sizeof(ipmc_lib_rule_t));
    }
    IPMC_LIB_EXIT();

    return rc;
}

/* GetNext IPMC Profile Rule */
mesa_rc ipmc_lib_mgmt_fltr_profile_rule_get_next(u32 profile_index, ipmc_lib_rule_t *fltr_rule)
{
    mesa_rc         rc = VTSS_RC_ERROR;
    ipmc_lib_rule_t entry, *ptr;

    if (!fltr_rule) {
        return rc;
    }

    memcpy(&entry, fltr_rule, sizeof(ipmc_lib_rule_t));
    IPMC_LIB_ENTER();
    if ((ptr = ipmc_lib_fltr_profile_rule_get_next(profile_index, &entry)) != NULL) {
        rc = VTSS_OK;
        memcpy(fltr_rule, ptr, sizeof(ipmc_lib_rule_t));
    }
    IPMC_LIB_EXIT();

    return rc;
}

/* Clear all the profile settings and running databases */
mesa_rc ipmc_lib_mgmt_clear_profile(vtss_isid_t isid_add)
{
    switch_iter_t               sit;
    vtss_isid_t                 isid;
    mesa_rc                     rc;

    if (!msg_switch_is_master() ||
        (isid_add != VTSS_ISID_GLOBAL)) {
        return VTSS_RC_ERROR;
    }

    if ((rc = _ipmc_lib_clear_running_profile()) != VTSS_RC_OK) {
        return rc;
    }

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if (!IPMC_LIB_MSG_ISID_PASS_SLV(isid_add, isid)) {
            continue;
        }

        ipmc_lib_stacking_profile_clear(isid, TRUE);
    }

    return rc;
}

/* Get Internal IPMC Profile Tree */
BOOL ipmc_lib_mgmt_profile_tree_get(u32 tdx, ipmc_profile_rule_t *entry, BOOL *is_avl)
{
    ipmc_profile_rule_t *ptr;

    if (!entry || !is_avl) {
        return FALSE;
    }

    IPMC_LIB_ENTER();
    if ((ptr = ipmc_lib_profile_tree_get(tdx, entry, is_avl)) == NULL) {
        IPMC_LIB_EXIT();
        return FALSE;
    }
    memcpy(entry, ptr, sizeof(ipmc_profile_rule_t));
    IPMC_LIB_EXIT();

    return TRUE;
}

/* GetNext Internal IPMC Profile Tree */
BOOL ipmc_lib_mgmt_profile_tree_get_next(u32 tdx, ipmc_profile_rule_t *entry, BOOL *is_avl)
{
    ipmc_profile_rule_t *ptr;

    if (!entry || !is_avl) {
        return FALSE;
    }

    IPMC_LIB_ENTER();
    if ((ptr = ipmc_lib_profile_tree_get_next(tdx, entry, is_avl)) == NULL) {
        IPMC_LIB_EXIT();
        return FALSE;
    }
    memcpy(entry, ptr, sizeof(ipmc_profile_rule_t));
    IPMC_LIB_EXIT();

    return TRUE;
}

/* Get Specific Internal IPMC Profile Tree VID */
BOOL ipmc_lib_mgmt_profile_tree_vid_get(u32 tdx, mesa_vid_t *pf_vid)
{
    BOOL    rc = FALSE;

    if (!pf_vid) {
        return rc;
    }

    IPMC_LIB_ENTER();
    rc = ipmc_lib_profile_tree_vid_get(tdx, pf_vid);
    IPMC_LIB_EXIT();

    return rc;
}

/* Get delta time for timers (filter timer & source timer) */
int ipmc_lib_mgmt_calc_delta_time(vtss_isid_t isid, ipmc_time_t *time_v)
{
    ipmc_time_t current_time;

    if (!time_v) {
        return -1;
    }

    if (!ipmc_lib_time_curr_get(&current_time)) {
        return 0;
    }

    if (ipmc_lib_isid_is_local(isid)) {
        if (IPMC_TIMER_LESS(&current_time, time_v)) {
            return (time_v->sec - current_time.sec);
        }
    } else {
        if (msg_switch_is_master()) {
            return time_v->sec;
        }
    }

    return 0;
}

BOOL ipmc_lib_isid_is_local(vtss_isid_t idx)
{
    if (IPMC_LIB_MY_ISID == IPMC_LIB_ISID_ASYNC) {
        return msg_switch_is_local(idx);
    } else if (IPMC_LIB_MY_ISID == IPMC_LIB_ISID_INIT) {
        return FALSE;
    } else if (IPMC_LIB_MY_ISID == IPMC_LIB_ISID_ANY) {
        return ((idx < VTSS_ISID_END) && (VTSS_ISID_START <= idx));
    } else {
        if (msg_switch_is_master()) {
            return (IPMC_LIB_MY_ISID == idx);
        } else {
            return FALSE;
        }
    }
}

/* GetNext(Walk) ifIndex based on given isid/port */
BOOL ipmc_lib_mgmt_port_ifindex_get_next(
    const vtss_isid_t       *const usidx,
    const mesa_port_no_t    *const uportx,
    vtss_isid_t             *const usid,
    mesa_port_no_t          *const uport
)
{
    switch_iter_t   sit;

    T_D("enter: usid[%d]/uport[%d]", usidx ? *usidx : -1, uportx ? *uportx : -1);
    if (!usid || !uport) {
        T_D("invalid");
        return FALSE;
    }

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_USID_CFG);
    if (!usidx && !uportx) {    /* FIRST */
        if (switch_iter_getnext(&sit)) {
            *usid = sit.usid;
            *uport = VTSS_PORT_NO_START;
        } else {
            T_D("No first ISID?!");
            return FALSE;
        }
    } else {                    /* NEXT */
        if (!usidx) {
            if (switch_iter_getnext(&sit)) {
                *usid = sit.usid;
                *uport = VTSS_PORT_NO_START;
            } else {
                T_D("No first ISID?!");
                return FALSE;
            }
        } else {
            if (!uportx) {
                while (switch_iter_getnext(&sit)) {
                    if (sit.usid >= *usidx) {
                        *usid = sit.usid;
                        *uport = VTSS_PORT_NO_START;
                        break;
                    }
                }
            } else {
                BOOL        chk, fnd;
                port_iter_t pit;

                chk = fnd = FALSE;
                while (switch_iter_getnext(&sit)) {
                    if (sit.usid >= *usidx) {
                        if (chk) {
                            *usid = sit.usid;
                            *uport = VTSS_PORT_NO_START;
                            fnd = TRUE;
                            break;
                        }

                        chk = TRUE;
                        (void) port_iter_init(&pit, NULL, sit.isid, PORT_ITER_SORT_ORDER_UPORT, PORT_ITER_FLAGS_FRONT);
                        while (port_iter_getnext(&pit)) {
                            if (pit.uport > *uportx) {
                                fnd = TRUE;
                                break;
                            }
                        }

                        if (fnd) {
                            *usid = sit.usid;
                            *uport = pit.uport;
                            break;
                        }
                    }
                }

                if (!chk || !fnd) {
                    T_D("exit: END");
                    return FALSE;
                }
            }
        }
    }

    T_D("exit: usid[%u]/uport[%u]", *usid, *uport);
    return TRUE;
}

/* Determine the OVPT based on given compatibility value */
u32 ipmc_lib_mgmt_ovpt_get(ipmc_compatibility_t *ovpt)
{
    u32 ptimeout = 0;

    if (!ovpt) {
        return ptimeout;
    }

    switch ( ovpt->mode ) {
    case VTSS_IPMC_COMPAT_MODE_OLD:
        ptimeout = ovpt->old_present_timer;

        break;
    case VTSS_IPMC_COMPAT_MODE_GEN:
        ptimeout = ovpt->gen_present_timer;

        break;
    case VTSS_IPMC_COMPAT_MODE_SFM:
        ptimeout = ovpt->sfm_present_timer;

        break;
    case VTSS_IPMC_COMPAT_MODE_AUTO:
    default:

        break;
    }

    return ptimeout;
}

/****************************************************************************/
/*  Initialization functions                                                */
/****************************************************************************/
#if defined(VTSS_SW_OPTION_PRIVATE_MIB)
VTSS_PRE_DECLS void ipmc_profile_mib_init(void);
#endif
#if defined(VTSS_SW_OPTION_JSON_RPC) && defined(VTSS_SW_OPTION_SMB_IPMC)
VTSS_PRE_DECLS void vtss_appl_ipmc_lib_profile_json_init(void);
#endif
extern "C" int ipmc_lib_profile_icli_cmd_register();


mesa_rc ipmc_lib_init(vtss_init_data_t *data)
{
    vtss_isid_t                    isid = data->isid;
    BOOL                           flg;
    msg_rx_filter_t                filter;

    switch ( data->cmd ) {
    case INIT_CMD_EARLY_INIT:
        /* Initialize and register trace ressources */
        VTSS_TRACE_REG_INIT(&ipmc_lib_trace_reg, ipmc_lib_trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&ipmc_lib_trace_reg);
        break;
    case INIT_CMD_INIT:
        critd_init(&ipmc_lib_global.crit, "ipmc_lib_crit", VTSS_MODULE_ID_IPMC_LIB, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
        IPMC_LIB_EXIT();

        IPMC_LIB_MY_ISID = IPMC_LIB_ISID_INIT;

#ifdef VTSS_SW_OPTION_ICFG
        if (ipmc_lib_icfg_init() != VTSS_OK) {
            T_E("ipmc_lib_icfg_init failed!");
        }
#endif /* VTSS_SW_OPTION_ICFG */

        if (ipmc_lib_profile_init() != VTSS_OK) {
            T_W("ipmc_lib_profile_init failed!");
        } else {
            if (!ipmc_lib_profile_conf_ptr_set((void *)&ipmc_lib_global.configuration.profile)) {
                T_W("ipmc_lib_profile_conf_ptr_set failed!");
            }
        }

        ipmc_lib_global.request_buf_pool = msg_buf_pool_create(VTSS_MODULE_ID_IPMC_LIB,
                                                               "Request",
                                                               IPMC_LIB_MSG_REQ_BUFS,
                                                               sizeof(ipmc_lib_msg_req_t));
#if defined(VTSS_SW_OPTION_PRIVATE_MIB)
        ipmc_profile_mib_init();    /* Register IPMC Profile private mib */
#endif
#if defined(VTSS_SW_OPTION_JSON_RPC) && defined(VTSS_SW_OPTION_SMB_IPMC)
        vtss_appl_ipmc_lib_profile_json_init();
#endif
#if ((defined(VTSS_SW_OPTION_SMB_IPMC) || defined(VTSS_SW_OPTION_MVR)) && defined(VTSS_SW_OPTION_IPMC_LIB))
        ipmc_lib_profile_icli_cmd_register();
#endif
        T_I("IPMC_LIB-INIT_CMD_INIT Done(%d)", isid);

        break;
    case INIT_CMD_START:
        T_I("START: ISID->%d", isid);

        /* Register for stack messages */
        memset(&filter, 0, sizeof(filter));
        filter.cb = ipmc_lib_msg_rx;
        filter.modid = VTSS_MODULE_ID_IPMC_LIB;
        (void)msg_rx_filter_register(&filter);

        break;
    case INIT_CMD_CONF_DEF:
        T_I("CONF_DEF: ISID->%d", isid);

        if (isid == VTSS_ISID_GLOBAL) {
            /* Clear Running Trees */
            (void) ipmc_lib_mgmt_clear_profile(VTSS_ISID_GLOBAL);

            /* Reset stack configuration */
            ipmc_lib_conf_default();
        }

        break;
    case INIT_CMD_MASTER_UP:
        T_I("MASTER_UP: ISID->%d", isid);


        IPMC_LIB_ENTER();
        IPMC_LIB_STACKABLE = FALSE;
        IPMC_LIB_MY_ISID = IPMC_LIB_ISID_ANY;
        flg = init_local_ipmc_profile;
        init_local_ipmc_profile = FALSE;
        IPMC_LIB_EXIT();

        if (flg) {
            (void) _ipmc_lib_clear_running_profile();
        }

        /* Read configuration */
        ipmc_lib_conf_default();

        break;
    case INIT_CMD_MASTER_DOWN:
        T_I("MASTER_DOWN: ISID->%d", isid);

        IPMC_LIB_ENTER();
        IPMC_LIB_MY_ISID = IPMC_LIB_ISID_ASYNC;
        IPMC_LIB_EXIT();

        break;
    case INIT_CMD_SWITCH_ADD:
        T_I("SWITCH_ADD: ISID->%d", isid);

        IPMC_LIB_ENTER();
        IPMC_LIB_MY_ISID = IPMC_LIB_ISID_ASYNC;
        IPMC_LIB_EXIT();

        ipmc_lib_stacking_switch_add_set(isid);

        break;
    case INIT_CMD_SWITCH_DEL:
        T_I("SWITCH_DEL: ISID->%d", isid);

        break;
    default:
        break;
    }

    return 0;
}

const char *ipmc_lib_error_txt(mesa_rc rc)
{
    switch ( rc ) {
    case IPMC_ERROR_GEN:
        return "IPMC: Generic error";
    case IPMC_ERROR_PARM:
        return "IPMC: Illegal parameter";
    case IPMC_ERROR_VLAN_NOT_FOUND:
        return "IPMC: VLAN not found";
    case IPMC_ERROR_VLAN_ACTIVE:
        return "IPMC: VLAN active";
    case IPMC_ERROR_VLAN_NOT_ACTIVE:
        return "IPMC: VLAN not active";
    case IPMC_ERROR_STACK_STATE:
        return "IPMC: Illegal MASTER/SLAVE state";
    case IPMC_ERROR_REQ_TIMEOUT:
        return "IPMC: Request Timeout";
    case IPMC_ERROR_ENTRY_NOT_FOUND:
        return "IPMC: Entry not found";
    case IPMC_ERROR_ENTRY_NOT_ACTIVE:
        return "IPMC: Entry not active";
    case IPMC_ERROR_ENTRY_OVERLAPPED:
        return "IPMC: Overlapped Entry";
    case IPMC_ERROR_ENTRY_INVALID:
        return "IPMC: Invalid Entry";
    case IPMC_ERROR_ENTRY_EXISTED:
        return "IPMC: Existed Entry";
    case IPMC_ERROR_ENTRY_NAME_DUPLICATED:
        return "IPMC: Duplicated Entry Name";
    case IPMC_ERROR_TABLE_IS_FULL:
        return "IPMC: Table Full";
    case IPMC_ERROR_MEMORY_NG:
        return "IPMC: Something wrong in Memory Allocate or Free";
    case IPMC_ERROR_PKT_IS_QUERY:
        return "IPMC-PKT: Specific frame is Query";
    case IPMC_ERROR_PKT_GROUP_FILTER:
        return "IPMC-PKT: Error in filtering group";
    case IPMC_ERROR_PKT_GROUP_NOT_FOUND:
        return "IPMC-PKT: Group is not found";
    case IPMC_ERROR_PKT_COMPATIBILITY:
        return "IPMC-PKT: Invalid compatibility";
    case IPMC_ERROR_PKT_TOO_MUCH_QUERY:
        return "IPMC-PKT: Too much Query";
    case IPMC_ERROR_PKT_CHECKSUM:
        return "IPMC-PKT: Checksum error";
    case IPMC_ERROR_PKT_INGRESS_FILTER:
        return "IPMC-PKT: Failed in ingress filter";
    case IPMC_ERROR_PKT_CONTENT:
        return "IPMC-PKT: Invalid content";
    case IPMC_ERROR_PKT_FORMAT:
        return "IPMC-PKT: Invalid format";
    case IPMC_ERROR_PKT_ADDRESS:
        return "IPMC-PKT: Invalid address";
    case IPMC_ERROR_PKT_RESERVED:
        return "IPMC-PKT: Reserved address";
    case IPMC_ERROR_PKT_VERSION:
        return "IPMC-PKT: Invalid version";
    case IPMC_ERROR_PKT_QUERY_FROM_RECV_PORT:
        return "IPMC-PKT: Query packet coming from the received port";
    default:
        return "IPMC: Unknown Error!";
    }
}

/*****************************************************************************
    Public API section for IPMC Profile
    from vtss_appl/include/vtss/appl/ipmc_profile.h
*****************************************************************************/
#if ((defined(VTSS_SW_OPTION_SMB_IPMC) || defined(VTSS_SW_OPTION_MVR)) && defined(VTSS_SW_OPTION_IPMC_LIB))
#define VTSS_APPL_IPMC_OCTSTR_ITR_NXT(x, y)     do {    \
    if (strlen((y))) {                                  \
        if (strlen((x)) < strlen((y))) {                \
            memset((y), 0x0, sizeof((y)));              \
            strncpy((y), (x), strlen((x)));             \
        } else if (strlen((x)) == strlen((y))) {        \
            if (memcmp((x), (y), sizeof((y))) < 0) {    \
                memset((y), 0x0, sizeof((y)));          \
                strncpy((y), (x), strlen((x)));         \
            }                                           \
        }                                               \
    } else {                                            \
        strncpy((y), (x), strlen((x)));                 \
    }                                                   \
} while (0);

#define VTSS_APPL_IPMC_OCTSTR_ITR_CHK(x, y, z)  do {    \
    if (strlen((y)) > strlen((x))) {                    \
        VTSS_APPL_IPMC_OCTSTR_ITR_NXT((y), (z))         \
    } else if (strlen((y)) == strlen((x))) {            \
        if (memcmp((y), (x), sizeof((x))) > 0) {        \
            VTSS_APPL_IPMC_OCTSTR_ITR_NXT((y), (z))     \
        }                                               \
    }                                                   \
} while (0);

static mesa_rc
_vtss_appl_ipmc_profile_management_entry_itr(
    const vtss_appl_ipmc_name_index_t           *const prev,
    vtss_appl_ipmc_name_index_t                 *const next
)
{
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *fltr_profile;
    vtss_appl_ipmc_name_index_t idx;
    i8                          nxt[VTSS_IPMC_NAME_MAX_LEN];

    if (!next || !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        T_D("Invalid Input/MTAKE!");
        return VTSS_RC_ERROR;
    }

    memset(&idx, 0x0, sizeof(vtss_appl_ipmc_name_index_t));
    if (prev && strlen(prev->n)) {
        strncpy(idx.n, prev->n, strlen(prev->n));
    }
    memset(nxt, 0x0, sizeof(nxt));
    fltr_profile = &pfm->profile;
    memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
    while (ipmc_lib_mgmt_fltr_profile_get_next(fltr_profile, TRUE) == VTSS_RC_OK) {
        VTSS_APPL_IPMC_OCTSTR_ITR_CHK(idx.n, fltr_profile->data.name, nxt);
    }

    IPMC_MEM_PROFILE_MGIVE(pfm);

    if (strlen(nxt)) {
        memset(next, 0x0, sizeof(vtss_appl_ipmc_name_index_t));
        strncpy(next->n, nxt, strlen(nxt));
        T_D("ITR-NXT=%s", next->n);
        return VTSS_RC_OK;
    }

    return VTSS_RC_ERROR;
}

static mesa_rc
_vtss_appl_ipmc_profile_range_entry_itr(
    const ipmc_ip_version_t                     version,
    const vtss_appl_ipmc_name_index_t           *const prev,
    vtss_appl_ipmc_name_index_t                 *const next
)
{
    ipmc_lib_grp_fltr_entry_t   fltr_entry;
    vtss_appl_ipmc_name_index_t idx;
    i8                          nxt[VTSS_IPMC_NAME_MAX_LEN];

    if (!next) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    memset(&idx, 0x0, sizeof(vtss_appl_ipmc_name_index_t));
    if (prev && strlen(prev->n)) {
        strncpy(idx.n, prev->n, strlen(prev->n));
    }
    memset(nxt, 0x0, sizeof(nxt));
    memset(&fltr_entry, 0x0, sizeof(ipmc_lib_grp_fltr_entry_t));
    while (ipmc_lib_mgmt_fltr_entry_get_next(&fltr_entry, TRUE) == VTSS_RC_OK) {
        if (fltr_entry.version == version) {
            VTSS_APPL_IPMC_OCTSTR_ITR_CHK(idx.n, fltr_entry.name, nxt);
        }
    }

    if (strlen(nxt)) {
        memset(next, 0x0, sizeof(vtss_appl_ipmc_name_index_t));
        strncpy(next->n, nxt, strlen(nxt));
        T_D("ITR-NXT=%s", next->n);
        return VTSS_RC_OK;
    }

    return VTSS_RC_ERROR;
}

static mesa_rc
_vtss_appl_ipmc_profile_rule_name_nxt(
    const vtss_appl_ipmc_name_index_t           *const idx,
    const ipmc_lib_profile_t                    *const profile,
    vtss_appl_ipmc_name_index_t                 *const nxt_rng
)
{
    ipmc_lib_rule_t             fltr_rule;
    ipmc_lib_grp_fltr_entry_t   fltr_entry;
    i8                          nxt[VTSS_IPMC_NAME_MAX_LEN];

    if (!idx || !profile || !nxt_rng) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    memset(nxt, 0x0, sizeof(nxt));
    if (ipmc_lib_mgmt_fltr_profile_rule_get_first(profile->index, &fltr_rule) == VTSS_RC_OK) {
        do {
            fltr_entry.index = fltr_rule.entry_index;
            if (ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, FALSE) == VTSS_RC_OK) {
                VTSS_APPL_IPMC_OCTSTR_ITR_CHK(idx->n, fltr_entry.name, nxt);
            }
        } while (ipmc_lib_mgmt_fltr_profile_rule_get_next(profile->index, &fltr_rule) == VTSS_RC_OK);
    }

    if (strlen(nxt)) {
        memset(nxt_rng, 0x0, sizeof(vtss_appl_ipmc_name_index_t));
        strncpy(nxt_rng->n, nxt, strlen(nxt));
        T_D("ITR-NXT=%s", nxt_rng->n);
        return VTSS_RC_OK;
    }

    return VTSS_RC_ERROR;
}

static mesa_rc
_vtss_appl_ipmc_profile_rule_precedence_nxt(
    const u32                                   *const idx,
    const ipmc_lib_profile_t                    *const profile,
    u32                                         *const nxt_rng
)
{
    ipmc_lib_rule_t             fltr_rule;
    ipmc_lib_grp_fltr_entry_t   fltr_entry;
    u32                         nxt;

    if (!idx || !profile || !nxt_rng) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    nxt = 0;
    if (ipmc_lib_mgmt_fltr_profile_rule_get_first(profile->index, &fltr_rule) == VTSS_RC_OK) {
        do {
            fltr_entry.index = fltr_rule.entry_index;
            if (ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, FALSE) == VTSS_RC_OK) {
                if (++nxt > *idx) {
                    *nxt_rng = nxt;
                    T_D("ITR-NXT=%u", *nxt_rng);
                    return VTSS_RC_OK;
                }
            }
        } while (ipmc_lib_mgmt_fltr_profile_rule_get_next(profile->index, &fltr_rule) == VTSS_RC_OK);
    }

    return VTSS_RC_ERROR;
}

/**
 * \brief Get IPMC Profile Parameters
 *
 * To read current system parameters in IPMC Profile.
 *
 * \param conf      [OUT]    The IPMC Profile system configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_system_config_get(
    vtss_appl_ipmc_profile_global_t             *const conf
)
{
    BOOL    pf_st;
    mesa_rc rc = VTSS_RC_ERROR;

    if (!conf) {
        T_D("Invalid Input!");
        return rc;
    }

    if ((rc = ipmc_lib_mgmt_profile_state_get(&pf_st)) == VTSS_RC_OK) {
        conf->mode = pf_st;
        T_D("GET %s",
            conf->mode ? "Enabled" : "Disabled");
    }

    return rc;
}

/**
 * \brief Set IPMC Profile Parameters
 *
 * To modify current system parameters in IPMC Profile.
 *
 * \param conf      [IN]     The IPMC Profile system configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_system_config_set(
    const vtss_appl_ipmc_profile_global_t       *const conf
)
{
    BOOL    pf_st;
    mesa_rc rc = VTSS_RC_ERROR;

    if (!conf) {
        T_D("Invalid Input!");
        return rc;
    }

    if ((rc = ipmc_lib_mgmt_profile_state_get(&pf_st)) == VTSS_RC_OK) {
        pf_st = conf->mode;
        rc = ipmc_lib_mgmt_profile_state_set(pf_st);
        T_D("SET-%s %s",
            rc == VTSS_RC_OK ? "OK" : "NG",
            conf->mode ? "Enabled" : "Disabled");
    }

    return rc;
}

/**
 * \brief Iterator for retrieving IPMC Profile management table key/index
 *
 * To walk name index of the management table in IPMC Profile.
 *
 * \param prev      [IN]    Name to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_management_table_itr(
    const vtss_appl_ipmc_name_index_t           *const prev,
    vtss_appl_ipmc_name_index_t                 *const next
)
{
    mesa_rc rc = VTSS_RC_ERROR;

    if (!next) {
        T_D("Invalid Input!");
        return rc;
    }

    T_D("enter: PREV=%s", prev ? prev->n : "NULL");
    rc = _vtss_appl_ipmc_profile_management_entry_itr(prev, next);
    T_D("exit(%s): NEXT=%s", rc == VTSS_RC_OK ? "OK" : "NG", next->n);

    return rc;
}

/**
 * \brief Get IPMC Profile specific management configuration
 *
 * To get configuration of the specific entry in IPMC Profile management table.
 *
 * \param name      [IN]    (key) Name of the profile.
 * \param entry     [OUT]   The current configuration of the profile in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_management_table_get(
    const vtss_appl_ipmc_name_index_t           *const name,
    vtss_appl_ipmc_profile_mgmt_t               *const entry
)
{
    mesa_rc                     rc;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *fltr_profile;
    ipmc_lib_profile_t          *data;

    if (!name || !entry || !(strlen(name->n) > 0) ||
        !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        T_D("Invalid Input/MTAKE!");
        return VTSS_RC_ERROR;
    }

    fltr_profile = &pfm->profile;
    memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
    data = &fltr_profile->data;
    strncpy(data->name, name->n, strlen(name->n));
    if ((rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE)) == VTSS_RC_OK) {
        memset(entry->profile_description, 0x0, sizeof(entry->profile_description));
        if (strlen(data->desc)) {
            strncpy(entry->profile_description, data->desc, strlen(data->desc));
        }
        T_D("GET %s:%s",
            name->n,
            entry->profile_description);
    }

    IPMC_MEM_PROFILE_MGIVE(pfm);
    return rc;
}

/**
 * \brief Set IPMC Profile specific management configuration
 *
 * To modify configuration of the specific entry in IPMC Profile management table.
 *
 * \param name      [IN]    (key) Name of the profile.
 * \param entry     [IN]    The revised configuration of the profile in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_management_table_set(
    const vtss_appl_ipmc_name_index_t           *const name,
    const vtss_appl_ipmc_profile_mgmt_t         *const entry
)
{
    mesa_rc                     rc;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *fltr_profile;
    ipmc_lib_profile_t          *data;

    if (!name || !entry || !(strlen(name->n) > 0) ||
        !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        T_D("Invalid Input/MTAKE!");
        return VTSS_RC_ERROR;
    }

    fltr_profile = &pfm->profile;
    memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
    data = &fltr_profile->data;
    strncpy(data->name, name->n, strlen(name->n));
    if ((rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE)) == VTSS_RC_OK) {
        memset(data->desc, 0x0, sizeof(data->desc));
        if (strlen(entry->profile_description)) {
            strncpy(data->desc, entry->profile_description, strlen(entry->profile_description));
        }

        rc = ipmc_lib_mgmt_fltr_profile_set(IPMC_OP_SET, fltr_profile);
        T_D("SET-%s %s:%s",
            rc == VTSS_RC_OK ? "OK" : "NG",
            name->n,
            entry->profile_description);
    } else {
        T_D("Update non-existing profile %s", name->n);
    }

    IPMC_MEM_PROFILE_MGIVE(pfm);
    return rc;
}

/**
 * \brief Delete IPMC Profile specific management configuration
 *
 * To delete configuration of the specific entry in IPMC Profile management table.
 *
 * \param name      [IN]    (key) Name of the profile.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_management_table_del(
    const vtss_appl_ipmc_name_index_t           *const name
)
{
    mesa_rc                     rc;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *fltr_profile;

    if (!name || !(strlen(name->n) > 0) ||
        !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        T_D("Invalid Input/MTAKE!");
        return VTSS_RC_ERROR;
    }

    fltr_profile = &pfm->profile;
    memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
    strncpy(fltr_profile->data.name, name->n, strlen(name->n));
    if ((rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE)) == VTSS_RC_OK) {
        rc = ipmc_lib_mgmt_fltr_profile_set(IPMC_OP_DEL, fltr_profile);
        T_D("DEL-%s %s",
            rc == VTSS_RC_OK ? "OK" : "NG",
            name->n);
    } else {
        T_D("Delete non-existing profile %s", name->n);
    }

    IPMC_MEM_PROFILE_MGIVE(pfm);
    return rc;
}

/**
 * \brief Add IPMC Profile specific management configuration
 *
 * To add configuration of the specific entry in IPMC Profile management table.
 *
 * \param name      [IN]    (key) Name of the profile.
 * \param entry     [IN]    The new configuration of the profile in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_management_table_add(
    const vtss_appl_ipmc_name_index_t           *const name,
    const vtss_appl_ipmc_profile_mgmt_t         *const entry
)
{
    mesa_rc                     rc;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *fltr_profile;
    ipmc_lib_profile_t          *data;

    if (!name || !entry || !(strlen(name->n) > 0) ||
        !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        T_D("Invalid Input/MTAKE!");
        return VTSS_RC_ERROR;
    }

    fltr_profile = &pfm->profile;
    memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
    data = &fltr_profile->data;
    strncpy(data->name, name->n, strlen(name->n));
    if ((rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE)) != VTSS_RC_OK) {
        memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
        strncpy(data->name, name->n, strlen(name->n));
        if (strlen(entry->profile_description)) {
            strncpy(data->desc, entry->profile_description, strlen(entry->profile_description));
        }

        rc = ipmc_lib_mgmt_fltr_profile_set(IPMC_OP_ADD, fltr_profile);
        T_D("ADD-%s %s:%s",
            rc == VTSS_RC_OK ? "OK" : "NG",
            name->n,
            entry->profile_description);
    } else {
        rc = VTSS_RC_ERROR;
        T_D("Add existing profile %s", name->n);
    }

    IPMC_MEM_PROFILE_MGIVE(pfm);
    return rc;
}

/**
 * \brief Iterator for retrieving IPMC Profile IPv4 range table key/index
 *
 * To walk name index of the IPv4 range table in IPMC Profile.
 *
 * \param prev      [IN]    Name to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv4_range_table_itr(
    const vtss_appl_ipmc_name_index_t           *const prev,
    vtss_appl_ipmc_name_index_t                 *const next
)
{
    mesa_rc rc = VTSS_RC_ERROR;

    if (!next) {
        T_D("Invalid Input!");
        return rc;
    }

    T_D("enter: PREV=%s", prev ? prev->n : "NULL");
    rc = _vtss_appl_ipmc_profile_range_entry_itr(IPMC_IP_VERSION_IGMP, prev, next);
    T_D("exit(%s): NEXT=%s", rc == VTSS_RC_OK ? "OK" : "NG", next->n);

    return rc;
}

/**
 * \brief Get IPMC Profile specific IPv4 range configuration
 *
 * To read configuration of the specific entry in IPMC Profile IPv4 range table.
 *
 * \param name      [IN]    (key) Name of the range.
 * \param entry     [OUT]   The current configuration of the range in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv4_range_table_get(
    const vtss_appl_ipmc_name_index_t           *const name,
    vtss_appl_ipmc_profile_ipv4_range_t         *const entry
)
{
    mesa_rc                     rc;
    ipmc_lib_grp_fltr_entry_t   fltr_entry;

    if (!name || !entry || !(strlen(name->n) > 0)) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    memset(&fltr_entry, 0x0, sizeof(ipmc_lib_grp_fltr_entry_t));
    strncpy(fltr_entry.name, name->n, strlen(name->n));
    if ((rc = ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, TRUE)) == VTSS_RC_OK) {
        if (fltr_entry.version == IPMC_IP_VERSION_IGMP) {
            i8  bgnString[40], endString[40];

            IPMC_LIB_ADRS_6TO4_SET(fltr_entry.grp_bgn, entry->start_address);
            IPMC_LIB_ADRS_6TO4_SET(fltr_entry.grp_end, entry->end_address);
            entry->start_address = htonl(entry->start_address);
            entry->end_address = htonl(entry->end_address);

            memset(bgnString, 0x0, sizeof(bgnString));
            memset(endString, 0x0, sizeof(endString));
            T_D("GET %s:%s->%s",
                name->n,
                misc_ipv4_txt(entry->start_address, bgnString),
                misc_ipv4_txt(entry->end_address, endString));
        } else {
            rc = VTSS_RC_ERROR;
            T_D("%s is not for IPv4", name->n);
        }
    }

    return rc;
}

/**
 * \brief Set IPMC Profile specific IPv4 range configuration
 *
 * To modify configuration of the specific entry in IPMC Profile IPv4 range table.
 *
 * \param name      [IN]    (key) Name of the range.
 * \param entry     [IN]    The revised configuration of the range in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv4_range_table_set(
    const vtss_appl_ipmc_name_index_t           *const name,
    const vtss_appl_ipmc_profile_ipv4_range_t   *const entry
)
{
    mesa_rc                     rc;
    ipmc_lib_grp_fltr_entry_t   fltr_entry;

    if (!name || !entry || !(strlen(name->n) > 0)) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    if (((entry->start_address >> 24) & 0xFF) > 239 ||
        ((entry->start_address >> 24) & 0xFF) < 224) {
        T_D("Invalid Start Range!");
        return VTSS_RC_ERROR;
    }

    if (((entry->end_address >> 24) & 0xFF) > 239 ||
        ((entry->end_address >> 24) & 0xFF) < 224) {
        T_D("Invalid End Range!");
        return VTSS_RC_ERROR;
    }

    memset(&fltr_entry, 0x0, sizeof(ipmc_lib_grp_fltr_entry_t));
    strncpy(fltr_entry.name, name->n, strlen(name->n));
    if ((rc = ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, TRUE)) == VTSS_RC_OK) {
        if (fltr_entry.version == IPMC_IP_VERSION_IGMP) {
            mesa_ipv4_t grp4;
            i8          bgnString[40], endString[40];

            IPMC_LIB_ADRS_SET(&fltr_entry.grp_bgn, 0x0);
            grp4 = htonl(entry->start_address);
            IPMC_LIB_ADRS_4TO6_SET(grp4, fltr_entry.grp_bgn);
            IPMC_LIB_ADRS_SET(&fltr_entry.grp_end, 0x0);
            grp4 = htonl(entry->end_address);
            IPMC_LIB_ADRS_4TO6_SET(grp4, fltr_entry.grp_end);

            memset(bgnString, 0x0, sizeof(bgnString));
            memset(endString, 0x0, sizeof(endString));
            if (IPMC_LIB_ADRS_GREATER(&fltr_entry.grp_bgn, &fltr_entry.grp_end)) {
                rc = VTSS_RC_ERROR;
                T_D("Start %s is greater than End %s",
                    misc_ipv4_txt(entry->start_address, bgnString),
                    misc_ipv4_txt(entry->end_address, endString));
            } else {
                rc = ipmc_lib_mgmt_fltr_entry_set(IPMC_OP_SET, &fltr_entry);
                T_D("SET-%s %s:%s->%s",
                    rc == VTSS_RC_OK ? "OK" : "NG",
                    name->n,
                    misc_ipv4_txt(entry->start_address, bgnString),
                    misc_ipv4_txt(entry->end_address, endString));
            }
        } else {
            rc = VTSS_RC_ERROR;
            T_D("%s is not for IPv4", name->n);
        }
    } else {
        T_D("Update non-existing range %s", name->n);
    }

    return rc;
}

/**
 * \brief Delete IPMC Profile specific IPv4 range configuration
 *
 * To delete configuration of the specific entry in IPMC Profile IPv4 range table.
 *
 * \param name      [IN]    (key) Name of the range.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv4_range_table_del(
    const vtss_appl_ipmc_name_index_t           *const name
)
{
    mesa_rc                     rc;
    ipmc_lib_grp_fltr_entry_t   fltr_entry;

    if (!name || !(strlen(name->n) > 0)) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    memset(&fltr_entry, 0x0, sizeof(ipmc_lib_grp_fltr_entry_t));
    strncpy(fltr_entry.name, name->n, strlen(name->n));
    if ((rc = ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, TRUE)) == VTSS_RC_OK) {
        if (fltr_entry.version == IPMC_IP_VERSION_IGMP) {
            rc = ipmc_lib_mgmt_fltr_entry_set(IPMC_OP_DEL, &fltr_entry);
            T_D("DEL-%s %s",
                rc == VTSS_RC_OK ? "OK" : "NG",
                name->n);
        } else {
            rc = VTSS_RC_ERROR;
            T_D("%s is not for IPv4", name->n);
        }
    } else {
        T_D("Delete non-existing range %s", name->n);
    }

    return rc;
}

/**
 * \brief Add IPMC Profile specific IPv4 range configuration
 *
 * To add configuration of the specific entry in IPMC Profile IPv4 range table.
 *
 * \param name      [IN]    (key) Name of the range.
 * \param entry     [IN]    The new configuration of the range in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv4_range_table_add(
    const vtss_appl_ipmc_name_index_t           *const name,
    const vtss_appl_ipmc_profile_ipv4_range_t   *const entry
)
{
    mesa_rc                     rc;
    ipmc_lib_grp_fltr_entry_t   fltr_entry;

    if (!name || !entry || !(strlen(name->n) > 0)) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    if (((entry->start_address >> 24) & 0xFF) > 239 ||
        ((entry->start_address >> 24) & 0xFF) < 224) {
        T_D("Invalid Start Range!");
        return VTSS_RC_ERROR;
    }

    if (((entry->end_address >> 24) & 0xFF) > 239 ||
        ((entry->end_address >> 24) & 0xFF) < 224) {
        T_D("Invalid End Range!");
        return VTSS_RC_ERROR;
    }

    memset(&fltr_entry, 0x0, sizeof(ipmc_lib_grp_fltr_entry_t));
    strncpy(fltr_entry.name, name->n, strlen(name->n));
    if ((rc = ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, TRUE)) != VTSS_OK) {
        mesa_ipv4_t grp4;
        i8          bgnString[40], endString[40];

        memset(&fltr_entry, 0x0, sizeof(ipmc_lib_grp_fltr_entry_t));

        fltr_entry.version = IPMC_IP_VERSION_IGMP;
        strncpy(fltr_entry.name, name->n, strlen(name->n));
        grp4 = htonl(entry->start_address);
        IPMC_LIB_ADRS_4TO6_SET(grp4, fltr_entry.grp_bgn);
        grp4 = htonl(entry->end_address);
        IPMC_LIB_ADRS_4TO6_SET(grp4, fltr_entry.grp_end);

        memset(bgnString, 0x0, sizeof(bgnString));
        memset(endString, 0x0, sizeof(endString));
        if (IPMC_LIB_ADRS_GREATER(&fltr_entry.grp_bgn, &fltr_entry.grp_end)) {
            rc = VTSS_RC_ERROR;
            T_D("Start %s is greater than End %s",
                misc_ipv4_txt(entry->start_address, bgnString),
                misc_ipv4_txt(entry->end_address, endString));
        } else {
            rc = ipmc_lib_mgmt_fltr_entry_set(IPMC_OP_ADD, &fltr_entry);
            T_D("ADD-%s %s:%s->%s",
                rc == VTSS_RC_OK ? "OK" : "NG",
                name->n,
                misc_ipv4_txt(entry->start_address, bgnString),
                misc_ipv4_txt(entry->end_address, endString));
        }
    } else {
        rc = VTSS_RC_ERROR;
        T_D("Add existing range %s", name->n);
    }

    return rc;
}

#ifdef VTSS_SW_OPTION_IPV6
/**
 * \brief Iterator for retrieving IPMC Profile IPv4 range table key/index
 *
 * To walk name index of the IPv6 range table in IPMC Profile.
 *
 * \param prev      [IN]    Name to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv6_range_table_itr(
    const vtss_appl_ipmc_name_index_t           *const prev,
    vtss_appl_ipmc_name_index_t                 *const next
)
{
    mesa_rc rc = VTSS_RC_ERROR;

    if (!next) {
        T_D("Invalid Input!");
        return rc;
    }

    T_D("enter: PREV=%s", prev ? prev->n : "NULL");
    rc = _vtss_appl_ipmc_profile_range_entry_itr(IPMC_IP_VERSION_MLD, prev, next);
    T_D("exit(%s): NEXT=%s", rc == VTSS_RC_OK ? "OK" : "NG", next->n);

    return rc;
}

/**
 * \brief Get IPMC Profile specific IPv6 range configuration
 *
 * To read configuration of the specific entry in IPMC Profile IPv4 range table.
 *
 * \param name      [IN]    (key) Name of the range.
 * \param entry     [OUT]   The current configuration of the range in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv6_range_table_get(
    const vtss_appl_ipmc_name_index_t           *const name,
    vtss_appl_ipmc_profile_ipv6_range_t         *const entry
)
{
    mesa_rc                     rc;
    ipmc_lib_grp_fltr_entry_t   fltr_entry;

    if (!name || !entry || !(strlen(name->n) > 0)) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    memset(&fltr_entry, 0x0, sizeof(ipmc_lib_grp_fltr_entry_t));
    strncpy(fltr_entry.name, name->n, strlen(name->n));
    if ((rc = ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, TRUE)) == VTSS_RC_OK) {
        if (fltr_entry.version == IPMC_IP_VERSION_MLD) {
            i8  bgnString[40], endString[40];

            IPMC_LIB_ADRS_CPY(&entry->start_address, &fltr_entry.grp_bgn);
            IPMC_LIB_ADRS_CPY(&entry->end_address, &fltr_entry.grp_end);

            memset(bgnString, 0x0, sizeof(bgnString));
            memset(endString, 0x0, sizeof(endString));
            T_D("GET %s:%s->%s",
                name->n,
                misc_ipv6_txt(&entry->start_address, bgnString),
                misc_ipv6_txt(&entry->end_address, endString));
        } else {
            rc = VTSS_RC_ERROR;
            T_D("%s is not for IPv6", name->n);
        }
    }

    return rc;
}

/**
 * \brief Set IPMC Profile specific IPv6 range configuration
 *
 * To modify configuration of the specific entry in IPMC Profile IPv6 range table.
 *
 * \param name      [IN]    (key) Name of the range.
 * \param entry     [IN]    The revised configuration of the range in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv6_range_table_set(
    const vtss_appl_ipmc_name_index_t           *const name,
    const vtss_appl_ipmc_profile_ipv6_range_t   *const entry
)
{
    mesa_rc                     rc;
    ipmc_lib_grp_fltr_entry_t   fltr_entry;

    if (!name || !entry || !(strlen(name->n) > 0)) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    memset(&fltr_entry, 0x0, sizeof(ipmc_lib_grp_fltr_entry_t));
    strncpy(fltr_entry.name, name->n, strlen(name->n));
    if ((rc = ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, TRUE)) == VTSS_RC_OK) {
        if (fltr_entry.version == IPMC_IP_VERSION_MLD) {
            i8  bgnString[40], endString[40];

            memset(bgnString, 0x0, sizeof(bgnString));
            memset(endString, 0x0, sizeof(endString));

            /* IPv6 MC: FFXX:..., so addr[0] must be 0xFF */
            if (entry->start_address.addr[0] != 0xFF ||
                entry->end_address.addr[0] != 0xFF) {
                rc = VTSS_RC_ERROR;
                T_D("Start %s OR End %s is not a valid IPv6 MC",
                    misc_ipv6_txt(&entry->start_address, bgnString),
                    misc_ipv6_txt(&entry->end_address, endString));
            } else {
                IPMC_LIB_ADRS_CPY(&fltr_entry.grp_bgn, &entry->start_address);
                IPMC_LIB_ADRS_CPY(&fltr_entry.grp_end, &entry->end_address);

                if (IPMC_LIB_ADRS_GREATER(&fltr_entry.grp_bgn, &fltr_entry.grp_end)) {
                    rc = VTSS_RC_ERROR;
                    T_D("Start %s is greater than End %s",
                        misc_ipv6_txt(&entry->start_address, bgnString),
                        misc_ipv6_txt(&entry->end_address, endString));
                } else {
                    rc = ipmc_lib_mgmt_fltr_entry_set(IPMC_OP_SET, &fltr_entry);
                    T_D("SET-%s %s:%s->%s",
                        rc == VTSS_RC_OK ? "OK" : "NG",
                        name->n,
                        misc_ipv6_txt(&entry->start_address, bgnString),
                        misc_ipv6_txt(&entry->end_address, endString));
                }
            }
        } else {
            rc = VTSS_RC_ERROR;
            T_D("%s is not for IPv6", name->n);
        }
    } else {
        T_D("Update non-existing range %s", name->n);
    }

    return rc;
}

/**
 * \brief Delete IPMC Profile specific IPv6 range configuration
 *
 * To delete configuration of the specific entry in IPMC Profile IPv6 range table.
 *
 * \param name      [IN]    (key) Name of the range.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv6_range_table_del(
    const vtss_appl_ipmc_name_index_t           *const name
)
{
    mesa_rc                     rc;
    ipmc_lib_grp_fltr_entry_t   fltr_entry;

    if (!name || !(strlen(name->n) > 0)) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    memset(&fltr_entry, 0x0, sizeof(ipmc_lib_grp_fltr_entry_t));
    strncpy(fltr_entry.name, name->n, strlen(name->n));
    if ((rc = ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, TRUE)) == VTSS_RC_OK) {
        if (fltr_entry.version == IPMC_IP_VERSION_MLD) {
            rc = ipmc_lib_mgmt_fltr_entry_set(IPMC_OP_DEL, &fltr_entry);
            T_D("DEL-%s %s",
                rc == VTSS_RC_OK ? "OK" : "NG",
                name->n);
        } else {
            rc = VTSS_RC_ERROR;
            T_D("%s is not for IPv6", name->n);
        }
    } else {
        T_D("Delete non-existing range %s", name->n);
    }

    return rc;
}

/**
 * \brief Add IPMC Profile specific IPv6 range configuration
 *
 * To add configuration of the specific entry in IPMC Profile IPv6 range table.
 *
 * \param name      [IN]    (key) Name of the range.
 * \param entry     [IN]    The new configuration of the range in Name.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_ipv6_range_table_add(
    const vtss_appl_ipmc_name_index_t           *const name,
    const vtss_appl_ipmc_profile_ipv6_range_t   *const entry
)
{
    mesa_rc                     rc;
    ipmc_lib_grp_fltr_entry_t   fltr_entry;

    if (!name || !entry || !(strlen(name->n) > 0)) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    memset(&fltr_entry, 0x0, sizeof(ipmc_lib_grp_fltr_entry_t));
    strncpy(fltr_entry.name, name->n, strlen(name->n));
    if ((rc = ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, TRUE)) != VTSS_RC_OK) {
        i8  bgnString[40], endString[40];

        memset(bgnString, 0x0, sizeof(bgnString));
        memset(endString, 0x0, sizeof(endString));

        /* IPv6 MC: FFXX:..., so addr[0] must be 0xFF */
        if (entry->start_address.addr[0] != 0xFF ||
            entry->end_address.addr[0] != 0xFF) {
            rc = VTSS_RC_ERROR;
            T_D("Start %s OR End %s is not a valid IPv6 MC",
                misc_ipv6_txt(&entry->start_address, bgnString),
                misc_ipv6_txt(&entry->end_address, endString));
        } else {
            memset(&fltr_entry, 0x0, sizeof(ipmc_lib_grp_fltr_entry_t));

            fltr_entry.version = IPMC_IP_VERSION_MLD;
            strncpy(fltr_entry.name, name->n, strlen(name->n));
            IPMC_LIB_ADRS_CPY(&fltr_entry.grp_bgn, &entry->start_address);
            IPMC_LIB_ADRS_CPY(&fltr_entry.grp_end, &entry->end_address);

            if (IPMC_LIB_ADRS_GREATER(&fltr_entry.grp_bgn, &fltr_entry.grp_end)) {
                rc = VTSS_RC_ERROR;
                T_D("Start %s is greater than End %s",
                    misc_ipv6_txt(&entry->start_address, bgnString),
                    misc_ipv6_txt(&entry->end_address, endString));
            } else {
                rc = ipmc_lib_mgmt_fltr_entry_set(IPMC_OP_ADD, &fltr_entry);
                T_D("ADD-%s %s:%s->%s",
                    rc == VTSS_RC_OK ? "OK" : "NG",
                    name->n,
                    misc_ipv6_txt(&entry->start_address, bgnString),
                    misc_ipv6_txt(&entry->end_address, endString));
            }
        }
    } else {
        rc = VTSS_RC_ERROR;
        T_D("Add existing range %s", name->n);
    }

    return rc;
}
#endif /* VTSS_SW_OPTION_IPV6 */

/**
 * \brief Iterator for retrieving IPMC Profile rule table key/index
 *
 * To walk indexes of the rule table in IPMC Profile.
 *
 * \param prf_prev  [IN]    Profile name to be used for indexing determination.
 * \param rng_prev  [IN]    Range name to be used for indexing determination.
 *
 * \param prf_next  [OUT]   The key/index of profile should be used for the GET operation.
 * \param rng_next  [OUT]   The key/index of range should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_rule_table_itr(
    const vtss_appl_ipmc_name_index_t           *const prf_prev,
    vtss_appl_ipmc_name_index_t                 *const prf_next,
    const vtss_appl_ipmc_name_index_t           *const rng_prev,
    vtss_appl_ipmc_name_index_t                 *const rng_next
)
{
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *fltr_profile;
    ipmc_lib_profile_t          *data;
    vtss_appl_ipmc_name_index_t idx, nxt_prf, nxt_rng;
    mesa_rc                     rc = VTSS_RC_ERROR;

    if (!prf_next || !rng_next ||
        !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        T_D("Invalid Input/MTAKE!");
        return rc;
    }

    T_D("enter: PREV=%s/%s",
        prf_prev ? prf_prev->n : "NULL",
        rng_prev ? rng_prev->n : "NULL");

    fltr_profile = &pfm->profile;
    memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
    data = &fltr_profile->data;
    memset(&idx, 0x0, sizeof(vtss_appl_ipmc_name_index_t));
    if (prf_prev) {
        if (rng_prev && strlen(rng_prev->n)) {
            strncpy(idx.n, rng_prev->n, strlen(rng_prev->n));
        }

        strncpy(data->name, prf_prev->n, strlen(prf_prev->n));
        rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE);
    }
    if (rc != VTSS_RC_OK) {
        if ((rc = _vtss_appl_ipmc_profile_management_entry_itr(prf_prev, &nxt_prf)) == VTSS_RC_OK) {
            memset(data->name, 0x0, sizeof(data->name));
            strncpy(data->name, nxt_prf.n, strlen(nxt_prf.n));
            rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE);
        }
    }

    data = NULL;
    memset(&nxt_rng, 0x0, sizeof(vtss_appl_ipmc_name_index_t));
    if (rc == VTSS_RC_OK) {
        do {
            data = &fltr_profile->data;
            if ((rc = _vtss_appl_ipmc_profile_rule_name_nxt(&idx, data, &nxt_rng)) == VTSS_RC_OK) {
                break;
            }

            memset(&idx, 0x0, sizeof(vtss_appl_ipmc_name_index_t));
            rc = _vtss_appl_ipmc_profile_management_entry_itr((vtss_appl_ipmc_name_index_t *)data->name, &nxt_prf);
            if (rc == VTSS_RC_OK) {
                memset(data->name, 0x0, sizeof(data->name));
                strncpy(data->name, nxt_prf.n, strlen(nxt_prf.n));
                rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE);
            }
        } while (rc == VTSS_RC_OK);
    }

    if (rc == VTSS_RC_OK && data && strlen(nxt_rng.n)) {
        memset(prf_next->n, 0x0, sizeof(prf_next->n));
        strncpy(prf_next->n, data->name, strlen(data->name));
        memset(rng_next->n, 0x0, sizeof(rng_next->n));
        strncpy(rng_next->n, nxt_rng.n, strlen(nxt_rng.n));
        T_D("ITR-PFL-NXT=%s/ITR-RUL-NXT=%s", prf_next->n, rng_next->n);
    }

    T_D("exit(%s): NEXT=%s/%s", rc == VTSS_RC_OK ? "OK" : "NG",
        rc == VTSS_RC_OK ? prf_next->n : "!INVALID!",
        rc == VTSS_RC_OK ? rng_next->n : "!INVALID!");
    IPMC_MEM_PROFILE_MGIVE(pfm);
    return rc;
}

/**
 * \brief Get IPMC Profile specific rule configuration
 *
 * To get configuration of the specific entry in IPMC Profile rule table.
 *
 * \param prf_name  [IN]    (key) Name of the profile.
 * \param rng_name  [IN]    (key) Name of the range.
 * \param entry     [OUT]   The current configuration of the rule in a specific profile.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_rule_table_get(
    const vtss_appl_ipmc_name_index_t           *const prf_name,
    const vtss_appl_ipmc_name_index_t           *const rng_name,
    vtss_appl_ipmc_profile_rule_t               *const entry
)
{
    mesa_rc                     rc;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *fltr_profile;
    ipmc_lib_grp_fltr_entry_t   fltr_entry;
    u32                         pdx, edx;

    if (!prf_name || !rng_name || !entry ||
        !(strlen(prf_name->n) > 0) || !(strlen(rng_name->n) > 0) ||
        !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        T_D("Invalid Input/MTAKE!");
        return VTSS_RC_ERROR;
    }

    pdx = edx = 0;
    fltr_profile = &pfm->profile;
    memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
    strncpy(fltr_profile->data.name, prf_name->n, strlen(prf_name->n));
    if ((rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE)) == VTSS_RC_OK) {
        pdx = fltr_profile->data.index;

        memset(fltr_entry.name, 0x0, sizeof(fltr_entry.name));
        strncpy(fltr_entry.name, rng_name->n, strlen(rng_name->n));
        if ((rc = ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, TRUE)) == VTSS_RC_OK) {
            edx = fltr_entry.index;
        }
    }
    IPMC_MEM_PROFILE_MGIVE(pfm);

    if (rc == VTSS_RC_OK) {
        ipmc_lib_rule_t fltr_rule;

        if ((rc = ipmc_lib_mgmt_fltr_profile_rule_search(pdx, edx, &fltr_rule)) == VTSS_RC_OK) {
            entry->rule_action = fltr_rule.action;
            entry->rule_log = fltr_rule.log;
            memset(entry->next_rule_range.n, 0x0, sizeof(entry->next_rule_range.n));
            if (ipmc_lib_mgmt_fltr_profile_rule_get_next(pdx, &fltr_rule) == VTSS_RC_OK) {
                fltr_entry.index = fltr_rule.entry_index;
                if (ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, FALSE) == VTSS_RC_OK) {
                    strncpy(entry->next_rule_range.n, fltr_entry.name, strlen(fltr_entry.name));
                }
            }

            T_D("GET %s[%s]:%s/%sLog->%s",
                prf_name->n,
                rng_name->n,
                ipmc_lib_fltr_action_txt(entry->rule_action, IPMC_TXT_CASE_CAPITAL),
                entry->rule_log ? "Do" : "No",
                entry->next_rule_range.n);
        }
    }

    return rc;
}

/**
 * \brief Set IPMC Profile specific rule configuration
 *
 * To modify configuration of the specific entry in IPMC Profile rule table.
 *
 * \param prf_name  [IN]    (key) Name of the profile.
 * \param rng_name  [IN]    (key) Name of the range.
 * \param entry     [OUT]   The revised configuration of the rule in a specific profile.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_rule_table_set(
    const vtss_appl_ipmc_name_index_t           *const prf_name,
    const vtss_appl_ipmc_name_index_t           *const rng_name,
    const vtss_appl_ipmc_profile_rule_t         *const entry
)
{
    mesa_rc                     rc;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *fltr_profile;
    ipmc_lib_grp_fltr_entry_t   fltr_entry;
    u32                         pdx, edx;

    if (!prf_name || !rng_name || !entry ||
        !(strlen(prf_name->n) > 0) || !(strlen(rng_name->n) > 0) ||
        !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        T_D("Invalid Input/MTAKE!");
        return VTSS_RC_ERROR;
    }

    pdx = edx = 0;
    fltr_profile = &pfm->profile;
    memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
    strncpy(fltr_profile->data.name, prf_name->n, strlen(prf_name->n));
    if ((rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE)) == VTSS_RC_OK) {
        pdx = fltr_profile->data.index;

        memset(fltr_entry.name, 0x0, sizeof(fltr_entry.name));
        strncpy(fltr_entry.name, rng_name->n, strlen(rng_name->n));
        if ((rc = ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, TRUE)) == VTSS_RC_OK) {
            edx = fltr_entry.index;
        }
    }
    IPMC_MEM_PROFILE_MGIVE(pfm);

    if (rc == VTSS_RC_OK) {
        ipmc_lib_rule_t fltr_rule;

        if ((rc = ipmc_lib_mgmt_fltr_profile_rule_search(pdx, edx, &fltr_rule)) == VTSS_RC_OK) {
            u32 ndx = IPMC_LIB_FLTR_RULE_IDX_INIT;

            if (strlen(entry->next_rule_range.n)) {
                memset(fltr_entry.name, 0x0, sizeof(fltr_entry.name));
                strncpy(fltr_entry.name, entry->next_rule_range.n, strlen(entry->next_rule_range.n));
                if ((rc = ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, TRUE)) == VTSS_RC_OK) {
                    ipmc_lib_rule_t next_rule;

                    ndx = fltr_entry.index;
                    if ((rc = ipmc_lib_mgmt_fltr_profile_rule_search(pdx, ndx, &next_rule)) == VTSS_RC_OK) {
                        ndx = next_rule.idx;
                    }
                }
                T_D("Check(%s) NxtRng-%s",
                    rc == VTSS_RC_OK ? "OK" : "NG",
                    entry->next_rule_range.n);
            }

            if (rc == VTSS_RC_OK) {
                fltr_rule.action = entry->rule_action;
                fltr_rule.log = entry->rule_log;
                if (ndx != IPMC_LIB_FLTR_RULE_IDX_INIT) {
                    fltr_rule.next_rule_idx = ndx;
                }

                rc = ipmc_lib_mgmt_fltr_profile_rule_set(IPMC_OP_SET, pdx, &fltr_rule);
            }

            T_D("SET-%s %s[%s]:%s/%sLog->%s",
                rc == VTSS_RC_OK ? "OK" : "NG",
                prf_name->n,
                rng_name->n,
                ipmc_lib_fltr_action_txt(entry->rule_action, IPMC_TXT_CASE_CAPITAL),
                entry->rule_log ? "Do" : "No",
                entry->next_rule_range.n);
        } else {
            T_D("Update non-existing rule %s[%s]", prf_name->n, rng_name->n);
        }
    }

    return rc;
}

/**
 * \brief Delete IPMC Profile specific rule configuration
 *
 * To delete configuration of the specific entry in IPMC Profile rule table.
 *
 * \param prf_name  [IN]    (key) Name of the profile.
 * \param rng_name  [IN]    (key) Name of the range.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_rule_table_del(
    const vtss_appl_ipmc_name_index_t           *const prf_name,
    const vtss_appl_ipmc_name_index_t           *const rng_name
)
{
    mesa_rc                     rc;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *fltr_profile;
    ipmc_lib_grp_fltr_entry_t   fltr_entry;
    u32                         pdx, edx;

    if (!prf_name || !rng_name ||
        !(strlen(prf_name->n) > 0) || !(strlen(rng_name->n) > 0) ||
        !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        T_D("Invalid Input/MTAKE!");
        return VTSS_RC_ERROR;
    }

    pdx = edx = 0;
    fltr_profile = &pfm->profile;
    memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
    strncpy(fltr_profile->data.name, prf_name->n, strlen(prf_name->n));
    if ((rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE)) == VTSS_RC_OK) {
        pdx = fltr_profile->data.index;

        memset(fltr_entry.name, 0x0, sizeof(fltr_entry.name));
        strncpy(fltr_entry.name, rng_name->n, strlen(rng_name->n));
        if ((rc = ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, TRUE)) == VTSS_RC_OK) {
            edx = fltr_entry.index;
        }
    }
    IPMC_MEM_PROFILE_MGIVE(pfm);

    if (rc == VTSS_RC_OK) {
        ipmc_lib_rule_t fltr_rule;

        if ((rc = ipmc_lib_mgmt_fltr_profile_rule_search(pdx, edx, &fltr_rule)) == VTSS_RC_OK) {
            rc = ipmc_lib_mgmt_fltr_profile_rule_set(IPMC_OP_DEL, pdx, &fltr_rule);

            T_D("DEL-%s %s[%s]",
                rc == VTSS_RC_OK ? "OK" : "NG",
                prf_name->n,
                rng_name->n);
        } else {
            T_D("Delete non-existing rule %s[%s]", prf_name->n, rng_name->n);
        }
    }

    return rc;
}

/**
 * \brief Add IPMC Profile specific rule configuration
 *
 * To add configuration of the specific entry in IPMC Profile rule table.
 *
 * \param prf_name  [IN]    (key) Name of the profile.
 * \param rng_name  [IN]    (key) Name of the range.
 * \param entry     [OUT]   The new configuration of the rule in a specific profile.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_rule_table_add(
    const vtss_appl_ipmc_name_index_t           *const prf_name,
    const vtss_appl_ipmc_name_index_t           *const rng_name,
    const vtss_appl_ipmc_profile_rule_t         *const entry
)
{
    mesa_rc                     rc;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *fltr_profile;
    ipmc_lib_grp_fltr_entry_t   fltr_entry;
    u32                         pdx, edx;

    if (!prf_name || !rng_name || !entry ||
        !(strlen(prf_name->n) > 0) || !(strlen(rng_name->n) > 0) ||
        !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        T_D("Invalid Input/MTAKE!");
        return VTSS_RC_ERROR;
    }

    pdx = edx = 0;
    fltr_profile = &pfm->profile;
    memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
    strncpy(fltr_profile->data.name, prf_name->n, strlen(prf_name->n));
    if ((rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE)) == VTSS_RC_OK) {
        pdx = fltr_profile->data.index;

        memset(fltr_entry.name, 0x0, sizeof(fltr_entry.name));
        strncpy(fltr_entry.name, rng_name->n, strlen(rng_name->n));
        if ((rc = ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, TRUE)) == VTSS_RC_OK) {
            edx = fltr_entry.index;
        }
    }
    IPMC_MEM_PROFILE_MGIVE(pfm);

    if (rc == VTSS_RC_OK) {
        ipmc_lib_rule_t fltr_rule;

        if ((rc = ipmc_lib_mgmt_fltr_profile_rule_search(pdx, edx, &fltr_rule)) != VTSS_RC_OK) {
            u32 ndx = IPMC_LIB_FLTR_RULE_IDX_INIT;

            memset(&fltr_rule, 0x0, sizeof(ipmc_lib_rule_t));
            fltr_rule.idx = IPMC_LIB_FLTR_RULE_IDX_INIT;
            fltr_rule.entry_index = edx;
            fltr_rule.next_rule_idx = IPMC_LIB_FLTR_RULE_IDX_INIT;
            fltr_rule.action = IPMC_LIB_FLTR_RULE_DEF_ACTION;
            fltr_rule.log = IPMC_LIB_FLTR_RULE_DEF_LOG;
            rc = VTSS_RC_OK;

            if (strlen(entry->next_rule_range.n)) {
                memset(fltr_entry.name, 0x0, sizeof(fltr_entry.name));
                strncpy(fltr_entry.name, entry->next_rule_range.n, strlen(entry->next_rule_range.n));
                if ((rc = ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, TRUE)) == VTSS_RC_OK) {
                    ipmc_lib_rule_t next_rule;

                    ndx = fltr_entry.index;
                    if ((rc = ipmc_lib_mgmt_fltr_profile_rule_search(pdx, ndx, &next_rule)) == VTSS_RC_OK) {
                        ndx = next_rule.idx;
                    }
                }
                T_D("Check(%s) NxtRng-%s",
                    rc == VTSS_RC_OK ? "OK" : "NG",
                    entry->next_rule_range.n);
            }

            if (rc == VTSS_RC_OK) {
                fltr_rule.action = entry->rule_action;
                fltr_rule.log = entry->rule_log;
                if (ndx != IPMC_LIB_FLTR_RULE_IDX_INIT) {
                    fltr_rule.next_rule_idx = ndx;
                }

                rc = ipmc_lib_mgmt_fltr_profile_rule_set(IPMC_OP_SET, pdx, &fltr_rule);
            }

            T_D("ADD-%s %s[%s]:%s/%sLog->%s",
                rc == VTSS_RC_OK ? "OK" : "NG",
                prf_name->n,
                rng_name->n,
                ipmc_lib_fltr_action_txt(entry->rule_action, IPMC_TXT_CASE_CAPITAL),
                entry->rule_log ? "Do" : "No",
                entry->next_rule_range.n);
        } else {
            rc = VTSS_RC_ERROR;
            T_D("Add existing rule %s[%s]", prf_name->n, rng_name->n);
        }
    }

    return rc;
}

/**
 * \brief Iterator for retrieving IPMC Profile rule precedence table key/index
 *
 * To walk the IPMC Profile rule table in precedence order.
 *
 * \param prf_prev  [IN]    Profile name to be used for indexing determination.
 * \param idx_prev  [IN]    Precedence value to be used for indexing determination.
 *
 * \param prf_next  [OUT]   The key/index of profile should be used for the GET operation.
 * \param idx_next  [OUT]   The key/index of precedence should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_precedence_table_itr(
    const vtss_appl_ipmc_name_index_t           *const prf_prev,
    vtss_appl_ipmc_name_index_t                 *const prf_next,
    const u32                                   *const idx_prev,
    u32                                         *const idx_next
)
{
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *fltr_profile;
    ipmc_lib_profile_t          *data;
    vtss_appl_ipmc_name_index_t nxt_prf;
    u32                         idx, nxt_rng;
    mesa_rc                     rc = VTSS_RC_ERROR;

    if (!prf_next || !idx_next ||
        !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        T_D("Invalid Input/MTAKE!");
        return rc;
    }

    T_D("enter: PREV=%s/%u",
        prf_prev ? prf_prev->n : "NULL",
        idx_prev ? *idx_prev : 0);

    fltr_profile = &pfm->profile;
    memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
    data = &fltr_profile->data;
    idx = 0;
    if (prf_prev) {
        if (idx_prev) {
            idx = *idx_prev;
        }

        strncpy(data->name, prf_prev->n, strlen(prf_prev->n));
        rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE);
    }
    if (rc != VTSS_RC_OK) {
        if ((rc = _vtss_appl_ipmc_profile_management_entry_itr(prf_prev, &nxt_prf)) == VTSS_RC_OK) {
            memset(data->name, 0x0, sizeof(data->name));
            strncpy(data->name, nxt_prf.n, strlen(nxt_prf.n));
            rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE);
        }
    }

    data = NULL;
    nxt_rng = 0;
    if (rc == VTSS_RC_OK) {
        do {
            data = &fltr_profile->data;
            if ((rc = _vtss_appl_ipmc_profile_rule_precedence_nxt(&idx, data, &nxt_rng)) == VTSS_RC_OK) {
                break;
            }

            idx = 0;
            rc = _vtss_appl_ipmc_profile_management_entry_itr((vtss_appl_ipmc_name_index_t *)data->name, &nxt_prf);
            if (rc == VTSS_RC_OK) {
                memset(data->name, 0x0, sizeof(data->name));
                strncpy(data->name, nxt_prf.n, strlen(nxt_prf.n));
                rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE);
            }
        } while (rc == VTSS_RC_OK);
    }

    if (rc == VTSS_RC_OK && data && nxt_rng) {
        memset(prf_next->n, 0x0, sizeof(prf_next->n));
        strncpy(prf_next->n, data->name, strlen(data->name));
        *idx_next = nxt_rng;
        T_D("ITR-PFL-NXT=%s/ITR-PCN-NXT=%u", prf_next->n, *idx_next);
    }

    T_D("exit(%s): NEXT=%s/%s%u%s", rc == VTSS_RC_OK ? "OK" : "NG",
        rc == VTSS_RC_OK ? prf_next->n : "!INVALID!",
        rc == VTSS_RC_OK ? "" : "!",
        rc == VTSS_RC_OK ? *idx_next : 0,
        rc == VTSS_RC_OK ? "" : "!");
    IPMC_MEM_PROFILE_MGIVE(pfm);
    return rc;
}

/**
 * \brief Get IPMC Profile specific rule configuration w.r.t. precedence
 *
 * To get configuration of the specific entry in IPMC Profile rule table w.r.t. precedence.
 *
 * \param prf_name  [IN]    (key) Name of the profile.
 * \param rule_idx  [IN]    (key) Precedence of the rule.
 * \param entry     [OUT]   The current configuration of the rule in a specific profile.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_profile_precedence_table_get(
    const vtss_appl_ipmc_name_index_t           *const prf_name,
    const u32                                   *const rule_idx,
    vtss_appl_ipmc_profile_precedence_t         *const entry
)
{
    mesa_rc                     rc;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *fltr_profile;
    u32                         pdx;

    if (!prf_name || !rule_idx || !entry ||
        !(strlen(prf_name->n) > 0) || !(*rule_idx) ||
        !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        T_D("Invalid Input/MTAKE!");
        return VTSS_RC_ERROR;
    }

    pdx = 0;
    fltr_profile = &pfm->profile;
    memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
    strncpy(fltr_profile->data.name, prf_name->n, strlen(prf_name->n));
    if ((rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE)) == VTSS_RC_OK) {
        pdx = fltr_profile->data.index;
    }
    IPMC_MEM_PROFILE_MGIVE(pfm);

    if (rc == VTSS_RC_OK) {
        u32             pcx;
        ipmc_lib_rule_t fltr_rule;

        pcx = 0;
        rc = VTSS_RC_ERROR;
        if (ipmc_lib_mgmt_fltr_profile_rule_get_first(pdx, &fltr_rule) == VTSS_RC_OK) {
            ipmc_lib_grp_fltr_entry_t   fltr_entry;

            do {
                fltr_entry.index = fltr_rule.entry_index;
                if (ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, FALSE) == VTSS_OK) {
                    if (++pcx == *rule_idx) {
                        memset(entry->rule_range.n, 0x0, sizeof(entry->rule_range.n));
                        strncpy(entry->rule_range.n, fltr_entry.name, strlen(fltr_entry.name));
                        entry->rule_action = fltr_rule.action;
                        entry->rule_log = fltr_rule.log;
                        memset(entry->next_rule_range.n, 0x0, sizeof(entry->next_rule_range.n));
                        if (ipmc_lib_mgmt_fltr_profile_rule_get_next(pdx, &fltr_rule) == VTSS_RC_OK) {
                            fltr_entry.index = fltr_rule.entry_index;
                            if (ipmc_lib_mgmt_fltr_entry_get(&fltr_entry, FALSE) == VTSS_RC_OK) {
                                strncpy(entry->next_rule_range.n, fltr_entry.name, strlen(fltr_entry.name));
                            }
                        }

                        rc = VTSS_RC_OK;
                        break;
                    }
                }
            } while (ipmc_lib_mgmt_fltr_profile_rule_get_next(pdx, &fltr_rule) == VTSS_RC_OK);
        }

        T_D("GET-%s(%u) %s[%u]:%s/%s/%sLog->%s",
            rc == VTSS_RC_OK ? "OK" : "NG",
            pcx,
            prf_name->n,
            *rule_idx,
            entry->rule_range.n,
            ipmc_lib_fltr_action_txt(entry->rule_action, IPMC_TXT_CASE_CAPITAL),
            entry->rule_log ? "Do" : "No",
            entry->next_rule_range.n);
    }

    return rc;
}
#endif /* ((defined(VTSS_SW_OPTION_SMB_IPMC) || defined(VTSS_SW_OPTION_MVR)) && defined(VTSS_SW_OPTION_IPMC_LIB)) */
