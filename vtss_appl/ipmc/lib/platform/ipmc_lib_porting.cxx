/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "main.h"

#include "mscc/ethernet/switch/api.h"
#include "critd_api.h"
#include "mgmt_api.h"
#include "misc_api.h"
#if defined(VTSS_SW_OPTION_SYSLOG)
#include "syslog_api.h"
#endif /* VTSS_SW_OPTION_SYSLOG */

#include "ipmc_lib.h"
#include "ipmc_lib_porting.h"
#include "ipmc_trace.h"


/* ************************************************************************ **
 *
 * Defines
 *
 * ************************************************************************ */
#define VTSS_TRACE_MODULE_ID    VTSS_MODULE_ID_IPMC_LIB
#define VTSS_ALLOC_MODULE_ID    VTSS_MODULE_ID_IPMC_LIB

/* ************************************************************************ **
 *
 * Public data
 *
 * ************************************************************************ */


/* ************************************************************************ **
 *
 * Local data
 *
 * ************************************************************************ */
static BOOL                     ipmc_lib_ptg_done_init = FALSE;
static critd_t                  ipmc_ptg_crit;
static u8                       mc6_ctrl_flood_set;
static mesa_port_list_t         flooding4_member;
static mesa_port_list_t         flooding6_member;
static char                     bufPort[MGMT_PORT_BUF_SIZE];
static char                     bufSip4[40], bufDip4[40];
static char                     bufSip6[40], bufDip6[40];

#if VTSS_TRACE_ENABLED
#define IPMC_PTG_CRIT_ENTER()   critd_enter(&ipmc_ptg_crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define IPMC_PTG_CRIT_EXIT()    critd_exit(&ipmc_ptg_crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#else
#define IPMC_PTG_CRIT_ENTER()   critd_enter(&ipmc_ptg_crit)
#define IPMC_PTG_CRIT_EXIT()    critd_exit(&ipmc_ptg_crit)
#endif /* VTSS_TRACE_ENABLED */

static void                     *vtss_igmp_filter_id = NULL;    // Filter id for subscribing igmp packet.
static void                     *vtss_mld_filter_id = NULL;     // Filter id for subscribing mld packet.

static u8                       *ipmc_lib_mem_area[IPMC_LIB_MEM_MAX_POOL];
static vtss_handle_t             ipmc_lib_memory[IPMC_LIB_MEM_MAX_POOL];

#define IPMC_LIB_TIME_USE_CLOCK 1


BOOL ipmc_lib_time_curr_get(ipmc_time_t *now_t)
{
#if IPMC_LIB_TIME_USE_CLOCK
    u64 ssec, msec, usec;

    if (!now_t) {
        return FALSE;
    }

    usec = hal_time_get();
    now_t->sec = ssec = (usec / 1000000ULL);
    now_t->msec = msec = (usec - (1000000ULL * ssec)) / 1000ULL;
    now_t->usec = (usec - (1000000ULL * ssec) - (1000ULL * msec));
//    now_t->nsec = 0;
#else
    vtss_tick_count_t    curr_time;

    if (!now_t) {
        return FALSE;
    }

    curr_time = VTSS_OS_TICK2MSEC(vtss_current_time());
    now_t->sec = curr_time / IPMC_LIB_TIME_MSEC_BASE;
    now_t->msec = curr_time % IPMC_LIB_TIME_MSEC_BASE;
    now_t->usec = 0;
//    now_t->usec = now_t->nsec = 0;
#endif /* IPMC_LIB_TIME_USE_CLOCK */

    return TRUE;
}

BOOL ipmc_lib_time_diff_get(BOOL prt, BOOL info_prt, const char *str, ipmc_time_t *base_t, ipmc_time_t *diff_t)
{
#if IPMC_LIB_TIME_USE_CLOCK
    ipmc_time_t now_t;
    BOOL        warp_t;
    u32         under_sec_calc;

    if (!base_t || !diff_t) {
        return FALSE;
    }

    if (!ipmc_lib_time_curr_get(&now_t)) {
        return FALSE;
    }

    warp_t = FALSE;
    if (base_t->sec > now_t.sec) {
        warp_t = TRUE;

        memset(&diff_t->sec, 0xFF, sizeof(u32));
        diff_t->sec = diff_t->sec - base_t->sec + 1;
    } else if (base_t->sec == now_t.sec) {
        under_sec_calc = base_t->msec * IPMC_LIB_TIME_REF_MSEC_VAL + base_t->usec * IPMC_LIB_TIME_REF_USEC_VAL;
//        under_sec_calc = base_t->msec * IPMC_LIB_TIME_REF_MSEC_VAL + base_t->usec * IPMC_LIB_TIME_REF_USEC_VAL + base_t->nsec;

        if (under_sec_calc > (now_t.msec * IPMC_LIB_TIME_REF_MSEC_VAL + now_t.usec * IPMC_LIB_TIME_REF_USEC_VAL)) {
//        if (under_sec_calc > (now_t.msec * IPMC_LIB_TIME_REF_MSEC_VAL + now_t.usec * IPMC_LIB_TIME_REF_USEC_VAL + now_t.nsec)) {
            /* OVER 136 years?! */
            memset(diff_t, 0xFF, sizeof(ipmc_time_t));
            T_W("%s consumes OVER 136 years?!", str ? str : "Caller");
            return FALSE;
        } else {
            diff_t->sec = 0;
        }
    } else {
        diff_t->sec = now_t.sec - base_t->sec;
    }

    if (diff_t->sec) {
        under_sec_calc = IPMC_LIB_TIME_REF_UNIT_VAL;
        diff_t->sec--;

        under_sec_calc -= (base_t->msec * IPMC_LIB_TIME_REF_MSEC_VAL + base_t->usec * IPMC_LIB_TIME_REF_USEC_VAL);
        under_sec_calc += (now_t.msec * IPMC_LIB_TIME_REF_MSEC_VAL + now_t.usec * IPMC_LIB_TIME_REF_USEC_VAL);
//        under_sec_calc -= (base_t->msec * IPMC_LIB_TIME_REF_MSEC_VAL + base_t->usec * IPMC_LIB_TIME_REF_USEC_VAL + base_t->nsec);
//        under_sec_calc += (now_t.msec * IPMC_LIB_TIME_REF_MSEC_VAL + now_t.usec * IPMC_LIB_TIME_REF_USEC_VAL + now_t.nsec);

        if (under_sec_calc >= IPMC_LIB_TIME_REF_UNIT_VAL) {
            diff_t->sec += under_sec_calc / IPMC_LIB_TIME_REF_UNIT_VAL;
            under_sec_calc = under_sec_calc % IPMC_LIB_TIME_REF_UNIT_VAL;
        }

        if (warp_t) {
            diff_t->sec += now_t.sec;
        }
    } else {
        under_sec_calc = (now_t.msec * IPMC_LIB_TIME_REF_MSEC_VAL + now_t.usec * IPMC_LIB_TIME_REF_USEC_VAL);
        under_sec_calc -= (base_t->msec * IPMC_LIB_TIME_REF_MSEC_VAL + base_t->usec * IPMC_LIB_TIME_REF_USEC_VAL);
//        under_sec_calc = (now_t.msec * IPMC_LIB_TIME_REF_MSEC_VAL + now_t.usec * IPMC_LIB_TIME_REF_USEC_VAL + now_t.nsec);
//        under_sec_calc -= (base_t->msec * IPMC_LIB_TIME_REF_MSEC_VAL + base_t->usec * IPMC_LIB_TIME_REF_USEC_VAL + base_t->nsec);
    }
    diff_t->msec = under_sec_calc / IPMC_LIB_TIME_REF_MSEC_VAL;
    diff_t->usec = (under_sec_calc % IPMC_LIB_TIME_REF_MSEC_VAL) / IPMC_LIB_TIME_REF_USEC_VAL;
//    diff_t->nsec = under_sec_calc % IPMC_LIB_TIME_REF_USEC_VAL;
#else
    vtss_tick_count_t    curr_time, base_time, diff_time;

    if (!base_t || !diff_t) {
        return FALSE;
    }

    curr_time = VTSS_OS_TICK2MSEC(vtss_current_time());
    base_time = base_t->sec;
    base_time = base_time * IPMC_LIB_TIME_MSEC_BASE + base_t->msec;

    if (base_time > curr_time) {
        memset(&diff_time, 0xFF, sizeof(vtss_tick_count_t));
        diff_time = diff_time - base_time + 1 + curr_time;
    } else {
        diff_time = curr_time - base_time;
    }

    diff_t->sec = diff_time / IPMC_LIB_TIME_MSEC_BASE;
    diff_t->msec = diff_time % IPMC_LIB_TIME_MSEC_BASE;
//    diff_t->usec = diff_t->nsec = 0;
#endif /* IPMC_LIB_TIME_USE_CLOCK */

    if (prt &&
        (diff_t->sec || diff_t->msec || diff_t->usec)) {
        if (info_prt) {
            T_I("%s consumes %u.%um%uu",
                str ? str : "Caller",
                diff_t->sec, diff_t->msec, diff_t->usec);
        } else {
            T_D("%s consumes %u.%um%uu",
                str ? str : "Caller",
                diff_t->sec, diff_t->msec, diff_t->usec);
        }
    }

    return TRUE;
}

u8 *ipmc_lib_memory_allocate(size_t size)
{
    if (!size) {
        return NULL;
    }
    return (u8 *)VTSS_MALLOC(size);
}

BOOL ipmc_lib_memory_free(u8 *ptr)
{
    if (!ptr) {
        return FALSE;
    }

    VTSS_FREE(ptr);
    return TRUE;
}

static void fwd_map_to_txt(mesa_port_list_t *fwd_map, char *fwd_txt)
{
    if (!fwd_map || !fwd_txt) {
        return;
    }

    int i = 0;
    for (i = 0; i < mesa_port_cnt(nullptr); i++) {
        if ((*fwd_map)[i]) {
            strcat(fwd_txt, "1");
        } else {
            strcat(fwd_txt, "0");
        }
    }

    return ;
}
static mesa_rc  _ipmc_lib_porting_do_chip_sfm(BOOL op, BOOL is_sfm,
                                              ipmc_group_entry_t *grp_op,
                                              mesa_ipv4_t ip4sip, mesa_ipv4_t ip4dip,
                                              mesa_ipv6_t ip6sip, mesa_ipv6_t ip6dip,
                                              mesa_port_list_t *fwd_map)
{
    ipmc_ip_version_t   version;
    mesa_vid_t          ifid;
    mesa_rc             rc;
    u32                 ipv4_mc_sip = MESA_CAP(MESA_CAP_L2_IPV4_MC_SIP);
    u32                 ipv6_mc_sip = MESA_CAP(MESA_CAP_L2_IPV6_MC_SIP);

    if (!grp_op) {
        return VTSS_RC_ERROR;
    }

    version = grp_op->ipmc_version;
    ifid = grp_op->vid;
    rc = VTSS_OK;

    if (op) {   /* Add or Update */
        mesa_port_list_t empty_member;
        memset(&empty_member, 0, sizeof(empty_member));

        if ((version == IPMC_IP_VERSION_MLD || version == IPMC_IP_VERSION_IPV6Z) && ipv6_mc_sip) {
            if ((rc = mesa_ipv6_mc_add(NULL, ifid, ip6sip, ip6dip, fwd_map ? fwd_map : &empty_member)) != VTSS_OK) {
                T_D("mesa_ipv6_mc_add failed");
            }
        } else if ((version == IPMC_IP_VERSION_IGMP || version == IPMC_IP_VERSION_IPV4Z) && ipv4_mc_sip) {
            if (fwd_map) {
                CapArray<char, MESA_CAP_PORT_CNT> fwd_txt;
                fwd_map_to_txt(fwd_map, fwd_txt.data());
                T_D("ifid = %d, sip = %s, dip = %s, fwd_txt = %s", ifid,
                    misc_ipv4_txt(htonl(ip4sip), bufSip4),
                    misc_ipv4_txt(htonl(ip4dip), bufDip4),
                    fwd_txt.data());
            }
            if ((rc = mesa_ipv4_mc_add(NULL, ifid, ntohl(ip4sip), ntohl(ip4dip), fwd_map ? fwd_map : &empty_member)) != VTSS_OK) {
                T_D("mesa_ipv4_mc_add failed");
            }
        } else {
            rc = VTSS_RC_ERROR;
            T_D("Version-%d ADD failed", version);
        }
    } else {    /* Delete */
        ipmc_group_db_t *grp_db = &grp_op->info->db;

        if (is_sfm) {
            BOOL                in_chip;
            ipmc_sfm_srclist_t  *ptr, check;

            memset(&check, 0x0, sizeof(ipmc_sfm_srclist_t));
            if ((version == IPMC_IP_VERSION_MLD) || (version == IPMC_IP_VERSION_IPV6Z)) {
                memcpy(&check.src_ip, &ip6sip, sizeof(mesa_ipv6_t));
            } else if ((version == IPMC_IP_VERSION_IGMP) || (version == IPMC_IP_VERSION_IPV4Z)) {
                memcpy(&check.src_ip.addr[12], (u8 *)&ip4sip, sizeof(mesa_ipv4_t));
            } else {
                rc = VTSS_RC_ERROR;
                T_D("Version-%d DEL failed", version);
                return rc;
            }

            in_chip = FALSE;
            ptr = &check;
            if (IPMC_LIB_DB_GET(grp_db->ipmc_sf_do_forward_srclist, ptr)) {
                in_chip = ptr->sfm_in_hw;
            }
            if (!in_chip) {
                ptr = &check;
                if (IPMC_LIB_DB_GET(grp_db->ipmc_sf_do_not_forward_srclist, ptr)) {
                    in_chip = ptr->sfm_in_hw;
                }
            }

            if (!in_chip) {
                return rc;
            }
        } else {
            if (!grp_db->asm_in_hw) {
                return rc;
            }
        }

        if ((version == IPMC_IP_VERSION_MLD || version == IPMC_IP_VERSION_IPV6Z) && ipv6_mc_sip) {
            if ((rc = mesa_ipv6_mc_del(NULL, ifid, ip6sip, ip6dip)) != VTSS_OK) {
                T_D("mesa_ipv6_mc_del failed");
            }
        } else if ((version == IPMC_IP_VERSION_IGMP || version == IPMC_IP_VERSION_IPV4Z) && ipv4_mc_sip) {
            if ((rc = mesa_ipv4_mc_del(NULL, ifid, ntohl(ip4sip), ntohl(ip4dip))) != VTSS_OK) {
                T_D("mesa_ipv4_mc_del failed");
            }
        } else {
            rc = VTSS_RC_ERROR;
            T_D("Version-%d DEL failed", version);
        }
    }

    return rc;
}


mesa_rc ipmc_lib_porting_set_chip(BOOL op, ipmc_db_ctrl_hdr_t *p,
                                  ipmc_group_entry_t *grp_op,
                                  ipmc_ip_version_t version, mesa_vid_t ifid,
                                  mesa_ipv4_t ip4sip, mesa_ipv4_t ip4dip,
                                  mesa_ipv6_t ip6sip, mesa_ipv6_t ip6dip,
                                  mesa_port_list_t *fwd_map)
{
    mesa_rc                 rc;
    ipmc_group_db_t         *grp_db;
    ipmc_sfm_srclist_t      *element_allow, *element_deny, *check, *check_bak;
    mesa_ipv4_t             rsrvd_mac;
    ipmc_port_bfs_t         rp4_bitmask, rp6_bitmask;
    BOOL                    is_sfm, bypassing;
    u32                     i, local_port_cnt;
    mesa_mac_t              sys_mac;
    ipmc_group_entry_t      *grp, *grp_ptr;

    BOOL                    fwd_found, hash_found;
    mesa_ipv4_t             ip2mac;
    mesa_vid_mac_t          vid_mac_entry;
    mesa_mac_table_entry_t  mac_table_entry;
    u32                     ipHashChk1, ipHashChk2;
    u32                     ipv4_mc_sip = MESA_CAP(MESA_CAP_L2_IPV4_MC_SIP);
    u32                     ipv6_mc_sip = MESA_CAP(MESA_CAP_L2_IPV6_MC_SIP);

    if (!p || !grp_op) {
        return VTSS_RC_ERROR;
    }

    rc = VTSS_OK;
    local_port_cnt = ipmc_lib_get_system_local_port_cnt();

    is_sfm = FALSE;
    if (version == IPMC_IP_VERSION_MLD) {
        if (!ipmc_lib_isaddr6_all_zero((mesa_ipv6_t *)&ip6sip)) {
            is_sfm = TRUE;
        }
    }
    if (version == IPMC_IP_VERSION_IGMP) {
        if (!ipmc_lib_isaddr4_all_zero((ipmcv4addr *)&ip4sip)) {
            is_sfm = TRUE;
        }
    }

    T_D("%s-%s Ver/IfId=%s/%u, SIP4=%s, DIP4=%s, SIP6=%s, DIP6=%s, Ports=%s",
        op ? "ADD" : "DEL",
        is_sfm ? "SFM" : "ASM",
        ipmc_lib_version_txt(version, IPMC_TXT_CASE_UPPER), ifid,
        misc_ipv4_txt(htonl(ip4sip), bufSip4),
        misc_ipv4_txt(htonl(ip4dip), bufDip4),
        misc_ipv6_txt(&ip6sip, bufSip6),
        misc_ipv6_txt(&ip6dip, bufDip6),
        fwd_map ? mgmt_iport_list2txt(*fwd_map, bufPort) : "NULL");

    bypassing = FALSE;
    switch ( version ) {
    case IPMC_IP_VERSION_MLD:
    case IPMC_IP_VERSION_IPV6Z:
        memcpy((u8 *)&rsrvd_mac, &ip6dip.addr[12], sizeof(ipmcv4addr));
        rsrvd_mac = ntohl(rsrvd_mac);
        rsrvd_mac = rsrvd_mac << IPMC_IP2MAC_V6SHIFT_LEN;
        rsrvd_mac = rsrvd_mac >> IPMC_IP2MAC_V6SHIFT_LEN;
        /* We should bypass ALL_NODE & ALL_RTR */
        if ((((rsrvd_mac >> IPMC_IP2MAC_ARRAY5_SHIFT_LEN) & IPMC_IP2MAC_ARRAY_MASK) == 0x1) ||
            (((rsrvd_mac >> IPMC_IP2MAC_ARRAY5_SHIFT_LEN) & IPMC_IP2MAC_ARRAY_MASK) == 0x2)) {
            if (!((rsrvd_mac >> IPMC_IP2MAC_ARRAY2_SHIFT_LEN) & IPMC_IP2MAC_ARRAY_MASK) &&
                !((rsrvd_mac >> IPMC_IP2MAC_ARRAY3_SHIFT_LEN) & IPMC_IP2MAC_ARRAY_MASK) &&
                !((rsrvd_mac >> IPMC_IP2MAC_ARRAY4_SHIFT_LEN) & IPMC_IP2MAC_ARRAY_MASK)) {
                bypassing = TRUE;
                T_D("Bypass M:...:0000:000%s",
                    (((rsrvd_mac >> IPMC_IP2MAC_ARRAY5_SHIFT_LEN) & IPMC_IP2MAC_ARRAY_MASK) == 0x1) ?
                    "1 (ALL_NODE)" : "2 (ALL_RTR)");
                break;
            }
        }
        /* We should bypass same hash result as system MAC */
        if (ip6dip.addr[12] == 0xFF &&
            ipmc_lib_get_system_mgmt_macx((u8 *)&sys_mac)) {
            if (sys_mac.addr[3] == ip6dip.addr[13] &&
                sys_mac.addr[4] == ip6dip.addr[14] &&
                sys_mac.addr[5] == ip6dip.addr[15]) {
                bypassing = TRUE;
                T_D("Bypass same hash result as system MAC->FF%X:%X%X",
                    ip6dip.addr[13], ip6dip.addr[14], ip6dip.addr[15]);
                break;
            }
        }

        if (ipv6_mc_sip) {
            if (op && fwd_map) {
                VTSS_PORT_BF_CLR(rp6_bitmask.member_ports);
                ipmc_lib_get_discovered_router_port_mask(IPMC_IP_VERSION_MLD, &rp6_bitmask);

                for (i = 0; i < local_port_cnt; i++) {
                    (*fwd_map)[i] |= VTSS_PORT_BF_GET(rp6_bitmask.member_ports, i);
                }
            }
            rc = _ipmc_lib_porting_do_chip_sfm(op, is_sfm, grp_op, ip4sip, ip4dip, ip6sip, ip6dip, fwd_map);
            break;
        }

        if (is_sfm) {
            bypassing = TRUE;
            break;
        }

        memcpy((u8 *)&ip2mac, &ip6dip.addr[12], sizeof(ipmcv4addr));
        ip2mac = ntohl(ip2mac);
        ip2mac = ip2mac << IPMC_IP2MAC_V6SHIFT_LEN;
        ip2mac = ip2mac >> IPMC_IP2MAC_V6SHIFT_LEN;
        vid_mac_entry.mac.addr[0] = IPMC_IP2MAC_V6MAC_ARRAY0;
        vid_mac_entry.mac.addr[1] = IPMC_IP2MAC_V6MAC_ARRAY1;
        vid_mac_entry.mac.addr[2] = (ip2mac >> IPMC_IP2MAC_ARRAY2_SHIFT_LEN) & IPMC_IP2MAC_ARRAY_MASK;
        vid_mac_entry.mac.addr[3] = (ip2mac >> IPMC_IP2MAC_ARRAY3_SHIFT_LEN) & IPMC_IP2MAC_ARRAY_MASK;
        vid_mac_entry.mac.addr[4] = (ip2mac >> IPMC_IP2MAC_ARRAY4_SHIFT_LEN) & IPMC_IP2MAC_ARRAY_MASK;
        vid_mac_entry.mac.addr[5] = (ip2mac >> IPMC_IP2MAC_ARRAY5_SHIFT_LEN) & IPMC_IP2MAC_ARRAY_MASK;
        vid_mac_entry.vid = ifid;

        fwd_found = FALSE;
        memset(&mac_table_entry, 0x0, sizeof(mesa_mac_table_entry_t));
        if (mesa_mac_table_get(NULL, &vid_mac_entry, &mac_table_entry) != VTSS_OK) {
            memcpy(&mac_table_entry.vid_mac, &vid_mac_entry, sizeof(mesa_vid_mac_t));
        } else {
            fwd_found = TRUE;
        }
        mac_table_entry.copy_to_cpu = FALSE;
        mac_table_entry.locked = TRUE;
        mac_table_entry.aged = FALSE;

        if (op) {   /* Add or Update */
            if (fwd_map) {
                VTSS_PORT_BF_CLR(rp6_bitmask.member_ports);
                ipmc_lib_get_discovered_router_port_mask(IPMC_IP_VERSION_MLD, &rp6_bitmask);

                for (i = 0; i < local_port_cnt; i++) {
                    mac_table_entry.destination[i] = ((*fwd_map)[i] | VTSS_PORT_BF_GET(rp6_bitmask.member_ports, i));
                }
            }

            if (fwd_found) {
                grp_ptr = NULL;
                while ((grp = ipmc_lib_group_ptr_get_next(p, grp_ptr)) != NULL) {
                    grp_ptr = grp;

                    if (grp->vid != ifid ||
                        grp->ipmc_version != version) {
                        continue;
                    } else {
                        if (!memcmp(&grp->group_addr,  &ip6dip, sizeof(mesa_ipv6_t))) {
                            continue;
                        }
                    }

                    /* Same Hash in MAC? */
                    memcpy((u8 *)&ipHashChk1, &grp->group_addr.addr[12], sizeof(ipmcv4addr));
                    memcpy((u8 *)&ipHashChk2, &ip6dip.addr[12], sizeof(ipmcv4addr));
                    if ((ntohl(ipHashChk1) << IPMC_IP2MAC_V6SHIFT_LEN) !=
                        (ntohl(ipHashChk2) << IPMC_IP2MAC_V6SHIFT_LEN)) {
                        continue;
                    }

                    grp_db = &grp->info->db;
                    for (i = 0; i < local_port_cnt; i++) {
                        mac_table_entry.destination[i] |= VTSS_PORT_BF_GET(grp_db->port_mask, i);
                    }
                }
            } /* if (fwd_found) */
            rc = mesa_mac_table_add(NULL, &mac_table_entry);
        } else {    /* Delete */
            if (fwd_found) {    /* Delete */
                hash_found = FALSE;
                for (i = 0; i < local_port_cnt; i++) {
                    mac_table_entry.destination[i] = FALSE;
                }

                grp_ptr = NULL;
                while ((grp = ipmc_lib_group_ptr_get_next(p, grp_ptr)) != NULL) {
                    grp_ptr = grp;

                    if (grp->vid != ifid ||
                        grp->ipmc_version != version) {
                        continue;
                    } else {
                        if (!memcmp(&grp->group_addr,  &ip6dip, sizeof(mesa_ipv6_t))) {
                            continue;
                        }
                    }

                    /* Same Hash in MAC? */
                    memcpy((u8 *)&ipHashChk1, &grp->group_addr.addr[12], sizeof(ipmcv4addr));
                    memcpy((u8 *)&ipHashChk2, &ip6dip.addr[12], sizeof(ipmcv4addr));
                    if ((ntohl(ipHashChk1) << IPMC_IP2MAC_V6SHIFT_LEN) !=
                        (ntohl(ipHashChk2) << IPMC_IP2MAC_V6SHIFT_LEN)) {
                        continue;
                    }

                    hash_found = TRUE;
                    grp_db = &grp->info->db;
                    for (i = 0; i < local_port_cnt; i++) {
                        mac_table_entry.destination[i] |= VTSS_PORT_BF_GET(grp_db->port_mask, i);
                    }
                }

                if (hash_found) {
                    rc = mesa_mac_table_add(NULL, &mac_table_entry);
                } else {
                    rc = mesa_mac_table_del(NULL, &vid_mac_entry);
                }
            } else {            /* Invalid */
                rc = VTSS_RC_ERROR;
            }
        }
        break;
    case IPMC_IP_VERSION_IGMP:
    case IPMC_IP_VERSION_IPV4Z:
#ifdef VTSS_SW_OPTION_UPNP
        /* Filter UPNP-Like group address X.127.255.250, since UPNP may rely on MAC for TX */
        rsrvd_mac = ntohl(ip4dip);
        rsrvd_mac = rsrvd_mac << IPMC_IP2MAC_V4SHIFT_LEN;
        rsrvd_mac = rsrvd_mac >> IPMC_IP2MAC_V4SHIFT_LEN;
        /* We should bypass UPNP-Like group address */
        if ((((rsrvd_mac >> IPMC_IP2MAC_ARRAY5_SHIFT_LEN) & IPMC_IP2MAC_ARRAY_MASK) == 0xFA) &&
            (((rsrvd_mac >> IPMC_IP2MAC_ARRAY4_SHIFT_LEN) & IPMC_IP2MAC_ARRAY_MASK) == 0xFF) &&
            (((rsrvd_mac >> IPMC_IP2MAC_ARRAY3_SHIFT_LEN) & IPMC_IP2MAC_ARRAY_MASK) == 0x7F)) {
            bypassing = TRUE;
            T_D("Bypass UPNP-Like 01-00-5E-7F-FF-FA");
            break;
        }
#endif /* VTSS_SW_OPTION_UPNP */

        if (ipv4_mc_sip) {
            if (op && fwd_map) {
                VTSS_PORT_BF_CLR(rp4_bitmask.member_ports);
                ipmc_lib_get_discovered_router_port_mask(IPMC_IP_VERSION_IGMP, &rp4_bitmask);

                for (i = 0; i < local_port_cnt; i++) {
                    (*fwd_map)[i] |= VTSS_PORT_BF_GET(rp4_bitmask.member_ports, i);
                }
            }
            rc = _ipmc_lib_porting_do_chip_sfm(op, is_sfm, grp_op, ip4sip, ip4dip, ip6sip, ip6dip, fwd_map);
            break;
        }

        if (is_sfm) {
            bypassing = TRUE;
            break;
        }

        ip2mac = ntohl(ip4dip);
        ip2mac = ip2mac << IPMC_IP2MAC_V4SHIFT_LEN;
        ip2mac = ip2mac >> IPMC_IP2MAC_V4SHIFT_LEN;
        vid_mac_entry.mac.addr[0] = IPMC_IP2MAC_V4MAC_ARRAY0;
        vid_mac_entry.mac.addr[1] = IPMC_IP2MAC_V4MAC_ARRAY1;
        vid_mac_entry.mac.addr[2] = IPMC_IP2MAC_V4MAC_ARRAY2;
        vid_mac_entry.mac.addr[3] = (ip2mac >> IPMC_IP2MAC_ARRAY3_SHIFT_LEN) & IPMC_IP2MAC_ARRAY_MASK;
        vid_mac_entry.mac.addr[4] = (ip2mac >> IPMC_IP2MAC_ARRAY4_SHIFT_LEN) & IPMC_IP2MAC_ARRAY_MASK;
        vid_mac_entry.mac.addr[5] = (ip2mac >> IPMC_IP2MAC_ARRAY5_SHIFT_LEN) & IPMC_IP2MAC_ARRAY_MASK;
        vid_mac_entry.vid = ifid;

        fwd_found = FALSE;
        memset(&mac_table_entry, 0x0, sizeof(mesa_mac_table_entry_t));
        if (mesa_mac_table_get(NULL, &vid_mac_entry, &mac_table_entry) != VTSS_OK) {
            memcpy(&mac_table_entry.vid_mac, &vid_mac_entry, sizeof(mesa_vid_mac_t));
        } else {
            fwd_found = TRUE;
        }
        mac_table_entry.copy_to_cpu = FALSE;
        mac_table_entry.locked = TRUE;
        mac_table_entry.aged = FALSE;

        if (op) {   /* Add or Update */
            if (fwd_map) {
                VTSS_PORT_BF_CLR(rp4_bitmask.member_ports);
                ipmc_lib_get_discovered_router_port_mask(IPMC_IP_VERSION_IGMP, &rp4_bitmask);

                for (i = 0; i < local_port_cnt; i++) {
                    mac_table_entry.destination[i] = ((*fwd_map)[i] | VTSS_PORT_BF_GET(rp4_bitmask.member_ports, i));
                }
            }

            if (fwd_found) {
                grp_ptr = NULL;
                while ((grp = ipmc_lib_group_ptr_get_next(p, grp_ptr)) != NULL) {
                    grp_ptr = grp;

                    if (grp->vid != ifid ||
                        grp->ipmc_version != version) {
                        continue;
                    } else {
                        if (!memcmp(&grp->group_addr.addr[12], (u8 *)&ip4dip, sizeof(ipmcv4addr))) {
                            continue;
                        }
                    }

                    /* Same Hash in MAC? */
                    memcpy((u8 *)&ipHashChk1, &grp->group_addr.addr[12], sizeof(ipmcv4addr));
                    memcpy((u8 *)&ipHashChk2, (u8 *)&ip4dip, sizeof(ipmcv4addr));
                    if ((ntohl(ipHashChk1) << IPMC_IP2MAC_V4SHIFT_LEN) !=
                        (ntohl(ipHashChk2) << IPMC_IP2MAC_V4SHIFT_LEN)) {
                        continue;
                    }

                    grp_db = &grp->info->db;
                    for (i = 0; i < local_port_cnt; i++) {
                        mac_table_entry.destination[i] |= VTSS_PORT_BF_GET(grp_db->port_mask, i);
                    }
                }
            } /* if (fwd_found) */

            rc = mesa_mac_table_add(NULL, &mac_table_entry);
        } else {    /* Delete */
            if (fwd_found) {    /* Delete */
                hash_found = FALSE;
                for (i = 0; i < local_port_cnt; i++) {
                    mac_table_entry.destination[i] = FALSE;
                }

                grp_ptr = NULL;
                while ((grp = ipmc_lib_group_ptr_get_next(p, grp_ptr)) != NULL) {
                    grp_ptr = grp;

                    if (grp->vid != ifid ||
                        grp->ipmc_version != version) {
                        continue;
                    } else {
                        if (!memcmp(&grp->group_addr.addr[12], (u8 *)&ip4dip, sizeof(ipmcv4addr))) {
                            continue;
                        }
                    }

                    /* Same Hash in MAC? */
                    memcpy((u8 *)&ipHashChk1, &grp->group_addr.addr[12], sizeof(ipmcv4addr));
                    memcpy((u8 *)&ipHashChk2, (u8 *)&ip4dip, sizeof(ipmcv4addr));
                    if ((ntohl(ipHashChk1) << IPMC_IP2MAC_V4SHIFT_LEN) !=
                        (ntohl(ipHashChk2) << IPMC_IP2MAC_V4SHIFT_LEN)) {
                        continue;
                    }

                    hash_found = TRUE;
                    grp_db = &grp->info->db;
                    for (i = 0; i < local_port_cnt; i++) {
                        mac_table_entry.destination[i] |= VTSS_PORT_BF_GET(grp_db->port_mask, i);
                    }
                }

                if (hash_found) {
                    rc = mesa_mac_table_add(NULL, &mac_table_entry);
                } else {
                    rc = mesa_mac_table_del(NULL, &vid_mac_entry);
                }
            } else {            /* Invalid */
                rc = VTSS_RC_ERROR;
            }
        }

        break;
    default:
        T_D("Invalid version: %s(%d)",
            ipmc_lib_version_txt(version, IPMC_TXT_CASE_UPPER), version);
        rc = VTSS_RC_ERROR;

        break;
    }

    grp_db = &grp_op->info->db;
    element_allow = element_deny = NULL;
    if (is_sfm && IPMC_MEM_SYSTEM_MTAKE(check, sizeof(ipmc_sfm_srclist_t))) {
        check_bak = check;

        if (version == IPMC_IP_VERSION_MLD) {
            if (ipv6_mc_sip) {
                memcpy(&check->src_ip, &ip6sip, sizeof(mesa_ipv6_t));
            }
        } else {
            if (ipv4_mc_sip) {
                memset(&check->src_ip, 0x0, sizeof(mesa_ipv6_t));
                memcpy(&check->src_ip.addr[12], (u8 *)&ip4sip, sizeof(ipmcv4addr));
            }
        }

        element_allow = ipmc_lib_srclist_adr_get(grp_db->ipmc_sf_do_forward_srclist, check);
        element_deny = ipmc_lib_srclist_adr_get(grp_db->ipmc_sf_do_not_forward_srclist, check);
        IPMC_MEM_SYSTEM_MGIVE(check_bak);
    }

    if (rc == VTSS_OK) {
        if (is_sfm) {
            if (op) {
                if (version == IPMC_IP_VERSION_MLD) {
                    if (element_allow) {
                        element_allow->sfm_in_hw = ipv6_mc_sip;
                    }
                    if (element_deny) {
                        element_deny->sfm_in_hw = ipv6_mc_sip;
                    }
                } else {
                    if (element_allow) {
                        element_allow->sfm_in_hw = ipv4_mc_sip;
                    }
                    if (element_deny) {
                        element_deny->sfm_in_hw = ipv4_mc_sip;
                    }
                }
            } else {
                if (element_allow) {
                    element_allow->sfm_in_hw = FALSE;
                }
                if (element_deny) {
                    element_deny->sfm_in_hw = FALSE;
                }
            }
        } else {
            if (op) {
                grp_db->asm_in_hw = TRUE;
            } else {
                grp_db->asm_in_hw = FALSE;
            }
        }
    }

    T_D("%s to %s %s-%s-FWD (API-[SSM4->%s|SSM6->%s]/RC-[%d])",
        (rc == VTSS_OK) ? "OK" : "NG",
        bypassing ? "bypass" : "config",
        ipmc_lib_version_txt(version, IPMC_TXT_CASE_UPPER),
        is_sfm ? "SFM" : "ASM",
        ipv4_mc_sip ? "Y" : "N",
        ipv6_mc_sip ? "Y" : "N",
        rc);

    return rc;
}

/* Register/Unregister for frames via VTSS API */
static mesa_rc vtss_ipmc_lib_rcv_reg(BOOL enable, ipmc_ip_version_t rcv_version)
{
    mesa_rc                 rc = VTSS_OK;
    mesa_packet_rx_conf_t   conf;

    memset(&conf, 0x0, sizeof(mesa_packet_rx_conf_t));
    vtss_appl_api_lock();
    if ((rc = mesa_packet_rx_conf_get(NULL, &conf)) == VTSS_OK) {
        switch ( rcv_version ) {
        case IPMC_IP_VERSION_IGMP:
            conf.reg.igmp_cpu_only = enable;

            break;
        case IPMC_IP_VERSION_MLD:
            conf.reg.mld_cpu_only = enable;

            break;
        default:

            break;
        }

        T_D("mesa_packet_rx_conf_set(%s/%s)",
            ipmc_lib_version_txt(rcv_version, IPMC_TXT_CASE_UPPER),
            enable ? "TRUE" : "FALSE");
        rc = mesa_packet_rx_conf_set(NULL, &conf);
    }
    vtss_appl_api_unlock();

    return rc;
}

mesa_rc vtss_ipmc_lib_rx_unregister(ipmc_ip_version_t version)
{
#ifdef VTSS_SW_OPTION_PACKET
    mesa_rc rc = VTSS_OK;

    if ((version == IPMC_IP_VERSION_MLD) ? (vtss_mld_filter_id != NULL) : (vtss_igmp_filter_id != NULL)) {
        if (version == IPMC_IP_VERSION_IGMP) {
            if ((rc = packet_rx_filter_unregister(vtss_igmp_filter_id)) == VTSS_OK) {
                vtss_igmp_filter_id = NULL;
            }
        }

        if (version == IPMC_IP_VERSION_MLD) {
            if ((rc = packet_rx_filter_unregister(vtss_mld_filter_id)) == VTSS_OK) {
                vtss_mld_filter_id = NULL;
            }
        }

        /* Un-register for stopping to capture IPMC(IGMP/MLD) frames via switch API */
        if (vtss_ipmc_lib_rcv_reg(FALSE, version) != VTSS_RC_OK) {
            // TODO, this error may not be ignored! please handle instead of
            // printing an error message
            T_E("vtss_ipmc_lib_rcv_reg failed");
        }
    }

    return rc;
#else
    return VTSS_OK;
#endif /* VTSS_SW_OPTION_PACKET */
}

mesa_rc vtss_ipmc_lib_rx_register(BOOL (*cb)(void *contxt, const u8 *const frm, const mesa_packet_rx_info_t *const rx_info), ipmc_ip_version_t version)
{
#ifdef VTSS_SW_OPTION_PACKET
    packet_rx_filter_t filter;

    /* Register for MLD frames via packet API */
    if ((version == IPMC_IP_VERSION_MLD) ? (vtss_mld_filter_id == NULL) : (vtss_igmp_filter_id == NULL)) {
        /* Register for starting to capture IPMC(IGMP/MLD) frames via switch API */
        if (vtss_ipmc_lib_rcv_reg(TRUE, version) != VTSS_RC_OK) {
            // TODO, this error may not be ignored! please handle instead of
            // printing an error message
            T_E("vtss_ipmc_lib_rcv_reg failed");
        }

        packet_rx_filter_init(&filter);
        switch ( version ) {
        case IPMC_IP_VERSION_IGMP:
            filter.modid    = VTSS_MODULE_ID_IPMC_LIB;
            filter.match    = PACKET_RX_FILTER_MATCH_IP_PROTO | PACKET_RX_FILTER_MATCH_ETYPE;
            filter.prio     = PACKET_RX_FILTER_PRIO_NORMAL;
            filter.ip_proto = IP_MULTICAST_IGMP_PROTO_ID;
            filter.etype    = ETYPE_IPV4;
            filter.cb       = cb;

            if (vtss_igmp_filter_id) {
                return packet_rx_filter_change(&filter, &vtss_igmp_filter_id);
            } else {
                return packet_rx_filter_register(&filter, &vtss_igmp_filter_id);
            }
        case IPMC_IP_VERSION_MLD:
            filter.modid = VTSS_MODULE_ID_IPMC_LIB;
            filter.match = PACKET_RX_FILTER_MATCH_DMAC | PACKET_RX_FILTER_MATCH_ETYPE;;
            filter.prio  = PACKET_RX_FILTER_PRIO_NORMAL;
            filter.etype = IP_MULTICAST_V6_ETHER_TYPE; /* IPv6 */
            memset(&filter.dmac_mask[0], 0xFF, sizeof(filter.dmac_mask));
            filter.dmac[0] = 0x33; /* 0x3333 */
            filter.dmac_mask[0] = 0x0;
            filter.dmac[1] = 0x33; /* 0x3333 */
            filter.dmac_mask[1] = 0x0;
            filter.cb = (BOOL (*)(void *contxt, const u8 * const frm, const mesa_packet_rx_info_t *const rx_info)) cb;

            if (vtss_mld_filter_id) {
                return packet_rx_filter_change(&filter, &vtss_mld_filter_id);
            } else {
                return packet_rx_filter_register(&filter, &vtss_mld_filter_id);
            }
        default:
            return VTSS_RC_ERROR;
        }
    } else {
        return VTSS_OK;
    }
#else
    return VTSS_OK;
#endif /* VTSS_SW_OPTION_PACKET */
}

static BOOL ipmc_lib_flooding_member_changed(mesa_port_list_t &member, mesa_port_list_t &port_list)
{
    BOOL changed = FALSE;
    mesa_port_no_t iport;
    u32 port_cnt = mesa_port_cnt(NULL);

    ipmc_lib_lock();
    for (iport = 0; iport < port_cnt; iport++) {
        if (member[iport] != port_list[iport]) {
            port_list[iport] = member[iport];
            changed = TRUE;
        }
    }
    ipmc_lib_unlock();
    return changed;
}

static mesa_rc _ipmc_lib_flooding_set(ipmc_ip_version_t version, ipmc_activate_t activate)
{
    mesa_rc          rc = VTSS_RC_OK;
    BOOL             ipv4 = FALSE, ipv6 = FALSE;
    mesa_port_list_t port_list;

    switch (version) {
    case IPMC_IP_VERSION_IGMP:
        ipv4 = TRUE;
        break;
    case IPMC_IP_VERSION_MLD:
        ipv6 = TRUE;
        break;
    case IPMC_IP_VERSION_ALL:
        ipv4 = TRUE;
        ipv6 = TRUE;
        break;
    default:
        rc = VTSS_RC_ERROR;
        break;
    }

    if (ipv4) {
        if ((rc = mesa_ipv4_mc_flood_members_get(NULL, &port_list)) == VTSS_OK &&
            ipmc_lib_flooding_member_changed(flooding4_member, port_list)) {
            rc = mesa_ipv4_mc_flood_members_set(NULL, &port_list);
        }
    }

    if (ipv6 && rc == VTSS_RC_OK) {
        if ((rc = mesa_ipv6_mc_flood_members_get(NULL, &port_list)) == VTSS_OK &&
            ipmc_lib_flooding_member_changed(flooding6_member, port_list)) {
            rc = mesa_ipv6_mc_flood_members_set(NULL, &port_list);
        }
    }
    return rc;
}

BOOL ipmc_lib_unregistered_flood_set(ipmc_owner_t owner,
                                     ipmc_activate_t activate,
                                     mesa_port_list_t member)
{
    ipmc_ip_version_t   version;

    ipmc_lib_lock();
    switch ( owner ) {
    case IPMC_OWNER_IGMP:
    case IPMC_OWNER_SNP4:
    case IPMC_OWNER_MVR4:
        version = IPMC_IP_VERSION_IGMP;
        memcpy(flooding4_member, member, sizeof(flooding4_member));

        break;
    case IPMC_OWNER_MLD:
    case IPMC_OWNER_SNP6:
    case IPMC_OWNER_MVR6:
        version = IPMC_IP_VERSION_MLD;
        memcpy(flooding6_member, member, sizeof(flooding6_member));

        break;
    case IPMC_OWNER_MVR:
    case IPMC_OWNER_SNP:
        version = IPMC_IP_VERSION_ALL;
        memcpy(flooding4_member, member, sizeof(flooding4_member));
        memcpy(flooding6_member, member, sizeof(flooding6_member));

        break;
    case IPMC_OWNER_INIT:
    case IPMC_OWNER_ALL:
    case IPMC_OWNER_MAX:
    default:
        ipmc_lib_unlock();
        return FALSE;
    }
    ipmc_lib_unlock();

    if (_ipmc_lib_flooding_set(version, activate) == VTSS_OK) {
        return TRUE;
    } else {
        return FALSE;
    }
}

BOOL ipmc_lib_mc6_ctrl_flood_set(ipmc_owner_t owner, BOOL status)
{
    BOOL    retVal;
    u8      mask, original_regs;

    switch ( owner ) {
    case IPMC_OWNER_MLD:
    case IPMC_OWNER_SNP:
    case IPMC_OWNER_SNP6:
    case IPMC_OWNER_MVR:
    case IPMC_OWNER_MVR6:
        original_regs = mc6_ctrl_flood_set;
        mask = 0;

        if (status) {
            mc6_ctrl_flood_set |= (1 << owner);
        } else {
            mask |= (1 << owner);
            mc6_ctrl_flood_set &= (~mask);
        }

        /* Only for the first one to turn it ON */
        if (status && !original_regs && mc6_ctrl_flood_set) {
#if 0   /* EBZ#1310: We should always leave link scope IPv6 CTRL-MC flooding */
            /* Enable local scope MLD */
            if (mesa_ipv6_mc_ctrl_flood_set(NULL, TRUE) != VTSS_OK) {
                T_D("\n\rvtss_ipv6_mc_ctrl_flood_set failed->0xFF02::/16 will be ignored");
            }
#endif  /* EBZ#1310 */
        }

        /* Only for the last one to turn it OFF */
        if (!status && original_regs && !mc6_ctrl_flood_set) {
#if 0   /* EBZ#1310: We should always leave link scope IPv6 CTRL-MC flooding */
            /* Disable local scope MLD */
            if (mesa_ipv6_mc_ctrl_flood_set(NULL, FALSE) != VTSS_OK) {
                T_D("\n\rvtss_ipv6_mc_ctrl_flood_set failed->0xFF02::/16 will be flooded");
            }
#endif  /* EBZ#1310 */
        }

        retVal = TRUE;

        break;
    case IPMC_OWNER_IGMP:
    case IPMC_OWNER_SNP4:
    case IPMC_OWNER_MVR4:
        retVal = TRUE;

        break;
    case IPMC_OWNER_INIT:
    case IPMC_OWNER_ALL:
    case IPMC_OWNER_MAX:
    default:
        retVal = FALSE;

        break;
    }

    return retVal;
}

BOOL ipmc_lib_log(const ipmc_lib_log_t *content, const ipmc_log_severity_t severity)
{
    char        log_buf[IPMC_LIB_LOG_BUF_SIZE];
    char        dip_buf[IPV6_ADDR_IBUF_MAX_LEN], sip_buf[IPV6_ADDR_IBUF_MAX_LEN];
    char        pname[VTSS_IPMC_NAME_MAX_LEN], ename[VTSS_IPMC_NAME_MAX_LEN];
    mesa_ipv6_t dip6, sip6;
    mesa_ipv4_t dip4, sip4;

    /*
        1.  Date and Time: A time string for stamping.
        2.  Severity: A string for specifying the severity.
        3.  IPMC Version: IPv4 or IPv6.
        4.  (Destination) Group: Address that matches the filtering condition.
        5.  (Source) IP: Address of the host that expects to register the group.
        6.  VLAN: VLAN ID that receives report frames.
        7.  Port: Port number that receives report frames.
        8.  Filtering Action: Allow or deny operation that matches the filtering condition.
        9.  Associated Profile: Profile object's identifier.
        10. Associated Address Entry: Address object's identifier.
    */
    if (!content) {
        T_D("content is %s", content ? "OK" : "NULL");

        return FALSE;
    }

    memset(log_buf, 0x0, sizeof(log_buf));
    switch ( content->type ) {
    case IPMC_LOG_TYPE_PF:
        if (!content->dst || !content->src) {
            T_D("content->dst is %s; content->src is %s",
                (content && content->dst) ? "OK" : "NULL",
                (content && content->src) ? "OK" : "NULL");

            break;
        }

        dip4 = sip4 = 0;
        IPMC_LIB_ADRS_CPY(&dip6, content->dst);
        IPMC_LIB_ADRS_CPY(&sip6, content->src);
        if (content->version == IPMC_IP_VERSION_IGMP) {
            IPMC_LIB_ADRS_6TO4_SET(dip6, dip4);
            IPMC_LIB_ADRS_6TO4_SET(sip6, sip4);
            dip4 = htonl(dip4);
            sip4 = htonl(sip4);
        }

        memset(pname, 0x0, sizeof(pname));
        memset(ename, 0x0, sizeof(ename));
        if (content->event.profile.name) {
            sprintf(pname, "%s", content->event.profile.name);
        } else {
            sprintf(pname, "Unknown");
        }
        if (content->event.profile.entry) {
            sprintf(ename, "%s", content->event.profile.entry);
        } else {
            sprintf(ename, "Unknown");
        }

        memset(dip_buf, 0x0, sizeof(dip_buf));
        memset(sip_buf, 0x0, sizeof(sip_buf));

#if defined(VTSS_SW_OPTION_SYSLOG)
        sprintf(log_buf,
                "IPMC-INFO: Date&Time:%s; Severity:%s; Version:%s; DIP:%s; SIP:%s; VID:%u; Interface:%s%2d/%2d%s; Action:%s; Profile:%s; Entry:%s.",
                misc_time2str(time(NULL)),
                ipmc_lib_severity_txt(severity, IPMC_TXT_CASE_CAPITAL),
                ipmc_lib_version_txt(content->version, IPMC_TXT_CASE_UPPER),
                (content->version == IPMC_IP_VERSION_IGMP) ? misc_ipv4_txt(dip4, dip_buf) : misc_ipv6_txt(&dip6, dip_buf),
                (content->version == IPMC_IP_VERSION_IGMP) ? misc_ipv4_txt(sip4, sip_buf) : misc_ipv6_txt(&sip6, sip_buf),
                content->vid,
                SYSLOG_PORT_INFO_REPLACE_TAG_START, VTSS_ISID_LOCAL, content->port, SYSLOG_PORT_INFO_REPLACE_TAG_END,
                ipmc_lib_fltr_action_txt(content->event.profile.action, IPMC_TXT_CASE_CAPITAL),
                pname, ename);
#else
        sprintf(log_buf,
                "IPMC-INFO: Date&Time:%s; Severity:%s; Version:%s; DIP:%s; SIP:%s; VID:%u; Interface:%2d/%2d; Action:%s; Profile:%s; Entry:%s.",
                misc_time2str(time(NULL)),
                ipmc_lib_severity_txt(severity, IPMC_TXT_CASE_CAPITAL),
                ipmc_lib_version_txt(content->version, IPMC_TXT_CASE_UPPER),
                (content->version == IPMC_IP_VERSION_IGMP) ? misc_ipv4_txt(dip4, dip_buf) : misc_ipv6_txt(&dip6, dip_buf),
                (content->version == IPMC_IP_VERSION_IGMP) ? misc_ipv4_txt(sip4, sip_buf) : misc_ipv6_txt(&sip6, sip_buf),
                content->vid,
                VTSS_ISID_LOCAL, content->port,
                ipmc_lib_fltr_action_txt(content->event.profile.action, IPMC_TXT_CASE_CAPITAL),
                pname, ename);
#endif /* VTSS_SW_OPTION_SYSLOG */

        break;
    case IPMC_LOG_TYPE_MSG:
    default:
#if defined(VTSS_SW_OPTION_SYSLOG)
        sprintf(log_buf,
                "IPMC-INFO: Date&Time:%s; Severity:%s; Version:%s; VID:%u; Interface:%s%2d/%2d%s; Message: %s.",
                misc_time2str(time(NULL)),
                ipmc_lib_severity_txt(severity, IPMC_TXT_CASE_CAPITAL),
                ipmc_lib_version_txt(content->version, IPMC_TXT_CASE_UPPER),
                content->vid,
                SYSLOG_PORT_INFO_REPLACE_TAG_START, VTSS_ISID_LOCAL, content->port, SYSLOG_PORT_INFO_REPLACE_TAG_END,
                content->event.message.data);
#else
        sprintf(log_buf,
                "IPMC-INFO: Date&Time:%s; Severity:%s; Version:%s; VID:%u; Interface:%2d/%2d; Message: %s.",
                misc_time2str(time(NULL)),
                ipmc_lib_severity_txt(severity, IPMC_TXT_CASE_CAPITAL),
                ipmc_lib_version_txt(content->version, IPMC_TXT_CASE_UPPER),
                content->vid,
                VTSS_ISID_LOCAL, content->port,
                content->event.message.data);
#endif /* VTSS_SW_OPTION_SYSLOG */

        break;
    }

    T_D("Severity:%d-> %s", severity, log_buf);

#if defined(VTSS_SW_OPTION_SYSLOG)
    switch ( severity ) {
    case IPMC_SEVERITY_Normal:
    case IPMC_SEVERITY_InfoDebug:
        S_I("%s", log_buf);

        break;
    case IPMC_SEVERITY_Notice:
    case IPMC_SEVERITY_Warning:
        S_W("%s", log_buf);

        break;
    case IPMC_SEVERITY_Error:
    case IPMC_SEVERITY_Critical:
    case IPMC_SEVERITY_Alert:
    case IPMC_SEVERITY_Emergency:
    default:
        S_E("%s", log_buf);

        break;
    }
#endif /* VTSS_SW_OPTION_SYSLOG */

    return TRUE;
}

mesa_rc ipmc_lib_porting_init(void)
{
    int i;

    if (ipmc_lib_ptg_done_init) {
        return VTSS_OK;
    }

    critd_init(&ipmc_ptg_crit, "ipmc_ptg_crit", VTSS_MODULE_ID_IPMC_LIB, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
    IPMC_PTG_CRIT_EXIT();

    for (i = 0; i < IPMC_LIB_MEM_MAX_POOL; i++) {
        ipmc_lib_mem_area[i] = NULL;
        memset(&ipmc_lib_memory[i], 0x0, sizeof(vtss_handle_t));
    }

    mc6_ctrl_flood_set = 0x0;
    memset(flooding4_member, 0x0, sizeof(flooding4_member));
    memset(flooding6_member, 0x0, sizeof(flooding6_member));

    ipmc_lib_ptg_done_init = TRUE;
    return VTSS_OK;
}
