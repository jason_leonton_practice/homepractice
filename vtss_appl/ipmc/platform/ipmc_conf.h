/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _IPMC_CONF_H_
#define _IPMC_CONF_H_

#include "ipmc_lib_base.h"


#define IPMC_VLAN_MAX                   IP_MAX_INTERFACES
#define IPMC_VID_INIT                   VTSS_IPMC_VID_VOID

#define VTSS_IPMC_IGMP_VERSION_DEF      VTSS_IPMC_VERSION_DEFAULT
#define VTSS_IPMC_IGMP_VERSION1         VTSS_IPMC_VERSION1
#define VTSS_IPMC_IGMP_VERSION2         VTSS_IPMC_VERSION2
#define VTSS_IPMC_IGMP_VERSION3         VTSS_IPMC_VERSION3
#define VTSS_IPMC_MLD_VERSION_DEF       VTSS_IPMC_VERSION_DEFAULT
#define VTSS_IPMC_MLD_VERSION1          VTSS_IPMC_VERSION2
#define VTSS_IPMC_MLD_VERSION2          VTSS_IPMC_VERSION3

#define IPMC_THROLLTING_MAX_VALUE       10

typedef struct {
    mesa_port_list_t ports;
} ipmc_conf_router_port_t;

typedef struct {
    mesa_port_list_t ports;
} ipmc_conf_fast_leave_port_t;

typedef struct {
    CapArray<BOOL, MESA_CAP_PORT_CNT> ports;
} ipmc_conf_throttling_max_no_t;

inline int vtss_memcmp(const ipmc_conf_throttling_max_no_t &a, const ipmc_conf_throttling_max_no_t &b)
{
    VTSS_MEMCMP_ELEMENT_CAP(a, b, ports);
    return 0;
}

typedef struct {
    int         port_no;

    /* when used for getnext func and "group_address" is set to "0", it means getting the first entry */
    union {
        struct {
            mesa_ipv6_t group_address;
        } v6;

        struct {
            mesa_ipv4_t reserved[3];
            mesa_ipv4_t group_address;
        } v4;

        struct {
            char        name[VTSS_IPMC_NAME_MAX_LEN];
        } profile;
    } addr;
} ipmc_conf_port_group_filtering_t;

typedef struct {
    BOOL                    valid;
    BOOL                    protocol_status;
    BOOL                    querier_status;
    u8                      priority;

    mesa_ipv4_t             querier4_address;
    u32                     compatibility;

    u32                     robustness_variable;
    u32                     query_interval;
    u32                     query_response_interval;
    u32                     last_listener_query_interval;
    u32                     unsolicited_report_interval;

    mesa_vid_t              vid;
    u8                      reserved[6];
} ipmc_conf_intf_entry_t;

typedef struct {
    BOOL                    ipmc_mode_enabled;
    BOOL                    ipmc_unreg_flood_enabled;
    BOOL                    ipmc_leave_proxy_enabled;
    BOOL                    ipmc_proxy_enabled;

    ipmc_prefix_t           ssm_range;
} ipmc_conf_global_t;

typedef struct {
    ipmc_ip_version_t       version;

    ipmc_conf_global_t      global;

    CapArray<ipmc_conf_intf_entry_t, VTSS_APPL_CAP_IP_INTERFACE_CNT> ipmc_conf_intf_entries;

    CapArray<BOOL, VTSS_APPL_CAP_ISID_CNT, MESA_CAP_PORT_CNT> ipmc_router_ports;        /* Dest. ports */
    CapArray<BOOL, VTSS_APPL_CAP_ISID_CNT, MESA_CAP_PORT_CNT> ipmc_fast_leave_ports;    /* Dest. ports */
    CapArray<int, VTSS_APPL_CAP_ISID_CNT, MESA_CAP_PORT_CNT> ipmc_throttling_max_no;    /* "0" means disabled */
    CapArray<u32, VTSS_APPL_CAP_ISID_CNT, MESA_CAP_PORT_CNT> ipmc_port_group_filtering;
} ipmc_configuration_t;

#endif /* _IPMC_CONF_H_ */
