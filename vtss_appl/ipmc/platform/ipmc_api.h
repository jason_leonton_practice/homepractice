/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _IPMC_API_H_
#define _IPMC_API_H_

#include "ipmc_conf.h"

#ifdef __cplusplus
extern "C" {
#endif
/* Initialize module */
mesa_rc ipmc_init(vtss_init_data_t *data);

/* Set IPMC Global Mode */
mesa_rc ipmc_mgmt_set_mode(BOOL *mode, ipmc_ip_version_t ipmc_version);

/* Get IPMC Global Mode */
mesa_rc ipmc_mgmt_get_mode(BOOL *mode, ipmc_ip_version_t ipmc_version);

/* Set IPMC Unregisterred Flooding */
mesa_rc ipmc_mgmt_set_unreg_flood(BOOL *mode, ipmc_ip_version_t ipmc_version);

/* Get IPMC Unregisterred Flooding */
mesa_rc ipmc_mgmt_get_unreg_flood(BOOL *mode, ipmc_ip_version_t ipmc_version);

/* Set IPMC Proxy */
mesa_rc ipmc_mgmt_set_proxy(BOOL *mode, ipmc_ip_version_t ipmc_version);

/* Get IPMC Proxy */
mesa_rc ipmc_mgmt_get_proxy(BOOL *mode, ipmc_ip_version_t ipmc_version);

/* Set IPMC Leave Proxy */
mesa_rc ipmc_mgmt_set_leave_proxy(BOOL *mode, ipmc_ip_version_t ipmc_version);

/* Get IPMC Leave Proxy */
mesa_rc ipmc_mgmt_get_leave_proxy(BOOL *mode, ipmc_ip_version_t ipmc_version);

/* Set IPMC SSM Range */
mesa_rc ipmc_mgmt_set_ssm_range(ipmc_ip_version_t ipmc_version, ipmc_prefix_t *range);

/* Get IPMC SSM Range */
mesa_rc ipmc_mgmt_get_ssm_range(ipmc_ip_version_t ipmc_version, ipmc_prefix_t *range);

/* Check whether default IPMC SSM Range is changed for not  */
BOOL ipmc_mgmt_def_ssm_range_chg(ipmc_ip_version_t version, ipmc_prefix_t *range);

/* Check whether the ADD/DEL operation is allowed or not */
BOOL ipmc_mgmt_intf_op_allow(vtss_isid_t isid);

/* Set IPMC VLAN state and querier */
mesa_rc ipmc_mgmt_set_intf_state_querier(mesa_vid_t vid,
                                         BOOL *state_enable,
                                         BOOL *querier_enable,
                                         ipmc_ip_version_t ipmc_version);

/* Get IPMC Interface(VLAN) State and Querier Status */
mesa_rc ipmc_mgmt_get_intf_state_querier(BOOL conf, mesa_vid_t *vid, u8 *state_enable, u8 *querier_enable, BOOL next, ipmc_ip_version_t ipmc_version);

/* Set IPMC Interface(VLAN) Info */
mesa_rc ipmc_mgmt_set_intf_info(vtss_isid_t isid, ipmc_prot_intf_entry_param_t *intf_param, ipmc_ip_version_t ipmc_version);

/* Get IPMC Interface(VLAN) Info */
mesa_rc ipmc_mgmt_get_intf_info(vtss_isid_t isid, mesa_vid_t vid, ipmc_prot_intf_entry_param_t *intf_param, ipmc_ip_version_t ipmc_version);

/* Set IPMC Throttling */
mesa_rc ipmc_mgmt_get_throttling_max_count(vtss_isid_t isid, ipmc_conf_throttling_max_no_t *ipmc_throttling_max_no, ipmc_ip_version_t ipmc_version);

/* Get IPMC Throttling */
mesa_rc ipmc_mgmt_set_throttling_max_count(vtss_isid_t isid, ipmc_conf_throttling_max_no_t *ipmc_throttling_max_no, ipmc_ip_version_t ipmc_version);

/* Add IPMC Group Filtering Entry */
mesa_rc ipmc_mgmt_set_port_group_filtering(vtss_isid_t isid, ipmc_conf_port_group_filtering_t *ipmc_port_group_filtering, ipmc_ip_version_t ipmc_version);

/* Del IPMC Group Filtering Entry */
mesa_rc ipmc_mgmt_del_port_group_filtering(vtss_isid_t isid, ipmc_conf_port_group_filtering_t *ipmc_port_group_filtering, ipmc_ip_version_t ipmc_version);

/* GetNext IPMC Group Filtering Entry */
mesa_rc ipmc_mgmt_get_port_group_filtering(vtss_isid_t isid, ipmc_conf_port_group_filtering_t *ipmc_port_group_filtering, ipmc_ip_version_t ipmc_version);

/* Set IPMC Router Ports */
mesa_rc ipmc_mgmt_set_router_port(vtss_isid_t isid, ipmc_conf_router_port_t *router_port, ipmc_ip_version_t ipmc_version);

/* Get IPMC Router Ports */
mesa_rc ipmc_mgmt_get_static_router_port(vtss_isid_t isid, ipmc_conf_router_port_t *router_port, ipmc_ip_version_t ipmc_version);

/* Set IPMC Fast leave Ports */
mesa_rc ipmc_mgmt_set_fast_leave_port(vtss_isid_t isid, ipmc_conf_fast_leave_port_t *fast_leave_port, ipmc_ip_version_t ipmc_version);

/* Get IPMC Fast leave Ports */
mesa_rc ipmc_mgmt_get_fast_leave_port(vtss_isid_t isid, ipmc_conf_fast_leave_port_t *fast_leave_port, ipmc_ip_version_t ipmc_version);

/* Clear IPMC Statistics Counters */
mesa_rc ipmc_mgmt_clear_stat_counter(vtss_isid_t isid, ipmc_ip_version_t ipmc_version, mesa_vid_t vid);

/* Get IPMC Dynamic Router Ports */
mesa_rc ipmc_mgmt_get_dynamic_router_ports(vtss_isid_t isid, ipmc_dynamic_router_port_t *router_port, ipmc_ip_version_t ipmc_version);

/* Get IPMC Working Versions */
mesa_rc ipmc_mgmt_get_intf_version(vtss_isid_t isid, ipmc_intf_query_host_version_t *vlan_version_entry, ipmc_ip_version_t ipmc_version);

/* GetNext IPMC Interface(VLAN) Group Info */
mesa_rc ipmc_mgmt_get_next_intf_group_info(vtss_isid_t isid, mesa_vid_t vid, ipmc_prot_intf_group_entry_t *intf_group_entry, ipmc_ip_version_t ipmc_version);

/* Get IPMC Group Info */
mesa_rc
ipmc_mgmt_group_info_get(
    const vtss_isid_t               sidx,
    const ipmc_ip_version_t         verx,
    const mesa_vid_t                *const vidx,
    const mesa_ipv6_t               *const grpx,
    ipmc_prot_intf_group_entry_t    *const intf_group_entry
);

/* GetNext IPMC Group Info */
mesa_rc
ipmc_mgmt_group_info_get_next(
    const vtss_isid_t               sidx,
    const ipmc_ip_version_t         verx,
    const mesa_vid_t                *const vidx,
    const mesa_ipv6_t               *const grpx,
    ipmc_prot_intf_group_entry_t    *const intf_group_entry
);

/* GetNext SourceList Entry per Group */
mesa_rc ipmc_mgmt_get_next_group_srclist_info(
    const vtss_isid_t           isid,
    const ipmc_ip_version_t     ipmc_version,
    const mesa_vid_t            vid,
    const mesa_ipv6_t           *const addr,
    ipmc_prot_group_srclist_t   *const group_srclist_entry
);

/* Thread manipulation */
mesa_rc ipmc_mgmt_sm_thread_suspend(void);
mesa_rc ipmc_mgmt_sm_thread_resume(void);

#ifdef __cplusplus
}
#endif
#endif /* _IPMC_API_H_ */
