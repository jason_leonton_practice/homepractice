/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "main.h"
#include "msg_api.h"
#include "port_api.h"
#include "critd_api.h"
#include "vlan_api.h"
#include "mgmt_api.h"
#include "vtss_timer_api.h"

#include "ipmc.h"
#include "ipmc_api.h"
#include "vtss_ipmc.h"
#include "vtss/appl/ipmc_snooping.h"

#include "misc_api.h"
#include "vtss_common_iterator.hxx"
#ifdef VTSS_SW_OPTION_ICFG
#include "ipmc_snp_icfg.h"
#endif /* VTSS_SW_OPTION_ICFG */
#include "ipmc_trace.h"

#define VTSS_TRACE_MODULE_ID        VTSS_MODULE_ID_IPMC
#define VTSS_ALLOC_MODULE_ID        VTSS_MODULE_ID_IPMC
#define IPMC_THREAD_EXE_SUPP        0

static struct {
    critd_t                         crit;
    critd_t                         get_crit;

    ipmc_configuration_t            ipv6_conf;                              /* Current configuration for MLD */
    ipmc_configuration_t            ipv4_conf;                              /* Current configuration for IGMP */

    vtss_flag_t                     vlan_entry_flags;
    ipmc_prot_intf_entry_param_t    interface_entry;
    ipmc_prot_intf_group_entry_t    intf_group_entry;
    ipmc_prot_group_srclist_t       group_srclist_entry;

    vtss_flag_t                     grp_entry_flags;
    vtss_flag_t                     grp_nxt_entry_flags;
    ipmc_prot_intf_group_entry_t    group_entry;
    ipmc_prot_intf_group_entry_t    group_next_entry;

    vtss_flag_t                     dynamic_router_ports_getting_flags;
    CapArray<BOOL, VTSS_APPL_CAP_ISID_CNT, MESA_CAP_PORT_CNT> dynamic_router_ports;

    /* Request semaphore */
    struct {
        vtss_sem_t sem;
    } request;

    /* Reply semaphore */
    struct {
        vtss_sem_t sem;
    } reply;

    /* MSG Buffer */
    u8                  *msg[IPMC_MSG_MAX_ID];
    u32                 msize[IPMC_MSG_MAX_ID];
} ipmc_global;

/* Thread variables */
/* IPMC Timer Thread */
static vtss_handle_t ipmc_sm_thread_handle;
static vtss_thread_t ipmc_sm_thread_block;

#if VTSS_TRACE_ENABLED

static vtss_trace_reg_t snp_trace_reg = {
    VTSS_MODULE_ID_IPMC, "IPMC", "IPMC Snooping"
};

static vtss_trace_grp_t snp_trace_grps[TRACE_GRP_CNT] = {
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CRIT */ {
        "crit",
        "Critical regions",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_RX */ {
        "rx",
        "Packet receive flow",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_TX */ {
        "tx",
        "Packet tramsnit flow",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_QUERIER_STATE */ {
        "querier",
        "Querier state",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    }
};

#endif /* VTSS_TRACE_ENABLED */

#if VTSS_TRACE_ENABLED

#define IPMC_CRIT_ENTER() critd_enter(&ipmc_global.crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE,  __FILE__, __LINE__)
#define IPMC_CRIT_EXIT()  critd_exit(&ipmc_global.crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE,  __FILE__, __LINE__)

#define IPMC_GET_CRIT_ENTER() critd_enter(&ipmc_global.get_crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE,  __FILE__, __LINE__)
#define IPMC_GET_CRIT_EXIT()  critd_exit(&ipmc_global.get_crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE,  __FILE__, __LINE__)

#else

#define IPMC_CRIT_ENTER() critd_enter(&ipmc_global.crit)
#define IPMC_CRIT_EXIT()  critd_exit(&ipmc_global.crit)

#define IPMC_GET_CRIT_ENTER() critd_enter(&ipmc_global.get_crit)
#define IPMC_GET_CRIT_EXIT()  critd_exit(&ipmc_global.get_crit)

#endif /* VTSS_TRACE_ENABLED */


#ifdef VTSS_SW_OPTION_PACKET
static mesa_packet_rx_info_t  ipmc_snp_rx_info;
#endif /* VTSS_SW_OPTION_PACKET */
static u8                   ipmcsnp_frame[IPMC_LIB_PKT_BUF_SZ];
static ipmc_lib_log_t       ipmcsnp_rx_packet_log;
static char                 ipmcsnp_rx_pktlog_msg[IPMC_LIB_LOG_MSG_SZ_VAL];
static BOOL ipmcsnp_rx_packet_callback(void *contxt, const u8 *const frm, const mesa_packet_rx_info_t *const rx_info, BOOL next_ipmc)
{
    mesa_rc             rc = VTSS_OK;
    ipmc_ip_eth_hdr     *etherHdr;
    ipmc_ip_version_t   version = IPMC_IP_VERSION_INIT;
    ipmc_port_bfs_t     ipmcsnp_fwd_map;
    BOOL                ingress_chk;
    mesa_port_list_t    ingress_member;
    mesa_vid_t          ingress_vid;
    mesa_glag_no_t      glag_no;
    u32                 idx, local_port_cnt;
    /* BZ#20254: Check port state is blocking by STP or not */
    mesa_packet_frame_info_t info = {0};
    mesa_packet_filter_t     filter = MESA_PACKET_FILTER_DISCARD;
    /* BZ#214224: Check IPv6 packet is need to snooping or not */
    mld_ip6_hbh_hdr *ip6HbH = NULL;
    ipmc_mld_packet_t *mld = NULL;

    T_D("%s Frame with length %d received on port %d with vid %d",
        ipmc_lib_frm_tagtype_txt(rx_info->tag_type, IPMC_TXT_CASE_CAPITAL),
        rx_info->length, rx_info->port_no, rx_info->tag.vid);
    T_D_HEX(frm, rx_info->length);

    glag_no = rx_info->glag_no;
    T_D("With glag_no=%u in SNP", glag_no);

    memset(&ipmcsnp_fwd_map, 0x0, sizeof(ipmc_port_bfs_t));
    memset(ingress_member, 0x0, sizeof(ingress_member));
    etherHdr = (ipmc_ip_eth_hdr *) frm;
    if (ntohs(etherHdr->type) == IP_MULTICAST_V6_ETHER_TYPE) {
        version = IPMC_IP_VERSION_MLD;
    } else {
        if (ntohs(etherHdr->type) == IP_MULTICAST_V4_ETHER_TYPE) {
            version = IPMC_IP_VERSION_IGMP;
        }
    }

    ingress_chk = TRUE;
    /* (VLAN) Ingress Filtering */
    ingress_vid = rx_info->tag.vid;
    if (ingress_vid == VTSS_APPL_IPMC_VID_NULL ||
        mesa_vlan_port_members_get(NULL, ingress_vid, &ingress_member) != VTSS_RC_OK ||
        !ingress_member[rx_info->port_no]) {
        rc = IPMC_ERROR_PKT_INGRESS_FILTER;
        ingress_chk = FALSE;
    }

    /* BZ#214224: Check IPv6 packet is need to snooping or not */
    /* Ingress filtering */
    if (version == IPMC_IP_VERSION_MLD) {
        if (rx_info->length > (sizeof(ipmc_ip_eth_hdr) + IPV6_HDR_FIXED_LEN + 8)) {
            ip6HbH = (mld_ip6_hbh_hdr *)(frm + sizeof(ipmc_ip_eth_hdr) + IPV6_HDR_FIXED_LEN);
            if (ip6HbH->HdrExtLen) {
                mld = (ipmc_mld_packet_t *)(frm + sizeof(ipmc_ip_eth_hdr) + IPV6_HDR_FIXED_LEN + ip6HbH->HdrExtLen);
            } else {
                mld = (ipmc_mld_packet_t *)(frm + sizeof(ipmc_ip_eth_hdr) + IPV6_HDR_FIXED_LEN + 8);
            }

            if (mld->common.type == IPMC_MLD_MSG_TYPE_QUERY || mld->common.type == IPMC_MLD_MSG_TYPE_V1REPORT ||
                mld->common.type == IPMC_MLD_MSG_TYPE_V2REPORT || mld->common.type == IPMC_MLD_MSG_TYPE_DONE) {
                ingress_chk = TRUE;
            } else {
                ingress_chk = FALSE;
            }
        } else {
            ingress_chk = FALSE;
        }
    }

    /* BZ#20254: Check port state is blocking by STP or not */
    /* Ingress filtering */
    info.port_no = rx_info->port_no;
    info.vid = rx_info->tag.vid;
    info.port_tx = VTSS_PORT_NO_NONE;
    if (mesa_packet_frame_filter(NULL, &info, &filter) == VTSS_RC_OK &&
        filter == MESA_PACKET_FILTER_DISCARD) {
        ingress_chk = FALSE;
        T_D("port %d with vid %d is discarding port!", info.port_no, info.vid);
    }


    local_port_cnt = ipmc_lib_get_system_local_port_cnt();

#ifdef VTSS_SW_OPTION_PACKET
    /* Igress checking */
    if (ingress_chk) {
        char    snpBufPort[MGMT_PORT_BUF_SIZE];

        IPMC_CRIT_ENTER();
        memcpy(&ipmc_snp_rx_info, rx_info, sizeof(ipmc_snp_rx_info));
        ipmc_lib_packet_strip_vtag(frm, &ipmcsnp_frame[0], rx_info->tag_type, &ipmc_snp_rx_info);

        rc = RX_ipmcsnp(version, contxt, &ipmcsnp_frame[0], &ipmc_snp_rx_info, &ipmcsnp_fwd_map);
        IPMC_CRIT_EXIT();

        for (idx = 0; idx < local_port_cnt; idx++) {
            if (idx != rx_info->port_no) {
                ingress_member[idx] &= VTSS_PORT_BF_GET(ipmcsnp_fwd_map.member_ports, idx);
            } else {
                ingress_member[idx] = FALSE;
            }

            VTSS_PORT_BF_SET(ipmcsnp_fwd_map.member_ports,
                             idx,
                             ingress_member[idx]);
        }
        T_D("ipmcsnp_fwd_map(%s) is %s", error_txt(rc), mgmt_iport_list2txt(ingress_member, snpBufPort));
    }
#endif /* VTSS_SW_OPTION_PACKET */

    /* Handle error code */
    switch ( rc ) {
    case IPMC_ERROR_PKT_INGRESS_FILTER:
    case IPMC_ERROR_PKT_CHECKSUM:
    case IPMC_ERROR_PKT_FORMAT:
    case IPMC_ERROR_PKT_QUERY_FROM_RECV_PORT:
        /* silently ignored */
        return FALSE;
    case IPMC_ERROR_PKT_CONTENT:
    case IPMC_ERROR_PKT_ADDRESS:
    case IPMC_ERROR_PKT_VERSION:
    case IPMC_ERROR_PKT_RESERVED:
        /* we shall flood this one */
        IPMC_CRIT_ENTER();
        vtss_ipmc_calculate_dst_ports(ingress_vid, rx_info->port_no, &ipmcsnp_fwd_map, version);

        memset(&ipmcsnp_rx_packet_log, 0x0, sizeof(ipmc_lib_log_t));
        ipmcsnp_rx_packet_log.vid = ingress_vid;
        ipmcsnp_rx_packet_log.port = rx_info->port_no;
        ipmcsnp_rx_packet_log.version = version;
        ipmcsnp_rx_packet_log.event.message.data = ipmcsnp_rx_pktlog_msg;

        memset(ipmcsnp_rx_pktlog_msg, 0x0, sizeof(ipmcsnp_rx_pktlog_msg));
        if (rc == IPMC_ERROR_PKT_CONTENT) {
            sprintf(ipmcsnp_rx_pktlog_msg, "(IPMC_ERROR_PKT_CONTENT)->RX frame with bad content!");
        }
        if (rc == IPMC_ERROR_PKT_RESERVED) {
            sprintf(ipmcsnp_rx_pktlog_msg, "(IPMC_ERROR_PKT_RESERVED)->RX reserved group address registration!");
        }
        if (rc == IPMC_ERROR_PKT_ADDRESS) {
            sprintf(ipmcsnp_rx_pktlog_msg, "(IPMC_ERROR_PKT_ADDRESS)->RX frame with bad address!");
        }
        if (rc == IPMC_ERROR_PKT_VERSION) {
            sprintf(ipmcsnp_rx_pktlog_msg, "(IPMC_ERROR_PKT_ADDRESS)->RX frame with bad version!");
        }
        IPMC_LIB_LOG_MSG(&ipmcsnp_rx_packet_log, IPMC_SEVERITY_Warning);
        IPMC_CRIT_EXIT();

        break;
    default:
        break;
    }


    if (IPMC_LIB_BFS_HAS_MEMBER(ipmcsnp_fwd_map.member_ports)) {
        BOOL    src_fast_leave;

        IPMC_CRIT_ENTER();
        src_fast_leave = vtss_ipmc_get_static_fast_leave_ports(rx_info->port_no, version);
        IPMC_CRIT_EXIT();

        (void) ipmc_lib_packet_tx(&ipmcsnp_fwd_map,
                                  FALSE,
                                  src_fast_leave,
                                  rx_info->port_no,
                                  IPMC_PKT_SRC_SNP,
                                  rx_info->tag.vid,
                                  rx_info->tag.dei,
                                  rx_info->tag.pcp,
                                  glag_no,
                                  frm,
                                  rx_info->length);
    }

    if (rc != VTSS_OK) {
        return FALSE;
    } else {
        /* SNP has done the processing; Others shouldn't take care of it. */
        return TRUE;
    }
}


/* DEBUG */
BOOL ipmc_debug_pkt_send(ipmc_ctrl_pkt_t type,
                         ipmc_ip_version_t version,
                         mesa_vid_t vid,
                         mesa_ipv6_t *grp,
                         u8 iport,
                         u8 cnt,
                         BOOL untag)
{
    ipmc_intf_entry_t   *entry, intf;
    u8                  idx;
    BOOL                retVal;
    char                buf[40];

    if (!grp) {
        return FALSE;
    }

    IPMC_CRIT_ENTER();
    entry = vtss_ipmc_get_intf_entry(vid, version);
    if (entry == NULL) {
        IPMC_CRIT_EXIT();
        return FALSE;
    }
    memcpy(&intf, entry, sizeof(ipmc_intf_entry_t));
    IPMC_CRIT_EXIT();

    memset(intf.vlan_ports, 0x0, sizeof(intf.vlan_ports));
    VTSS_PORT_BF_SET(intf.vlan_ports, iport, TRUE);
    retVal = TRUE;
    for (idx = 0; idx < cnt; idx++) {
        memset(buf, 0x0, sizeof(buf));
        T_D("vtss_ipmc_debug_pkt_tx(%u/%d, %d, %s, %u, %s)",
            intf.param.vid, intf.ipmc_version,
            type,
            misc_ipv6_txt(grp, buf),
            iport,
            untag ? "TRUE" : "FALSE");
        IPMC_CRIT_ENTER();
        if (!vtss_ipmc_debug_pkt_tx(&intf, type, grp, iport, untag)) {
            retVal = FALSE;
        }
        IPMC_CRIT_EXIT();
    }

    return retVal;
}

BOOL ipmc_debug_mem_get_info(ipmc_mem_info_t *info)
{
    ipmc_mem_t  idx;

    if (!info) {
        return FALSE;
    }

    info->mem_current_cnt = ipmc_lib_mem_debug_get_cnt();
    for (idx = (ipmc_mem_t)0; idx < (ipmc_mem_t)IPMC_MEM_TYPE_MAX; idx++) {
        ipmc_lib_mem_debug_get_info(idx, &info->mem_pool_info[idx]);
    }

    return TRUE;
}

#define IPMC_THREAD_SECOND_DEF      1000        /* 1000 msec = 1 second */
#define IPMC_THREAD_GC_TIME         11000       /* 11000 msec = 11 second */
#define IPMC_THREAD_TICK_TIME       1000        /* 1000 msec */
#define IPMC_THREAD_TIME_UNIT_BASE  (IPMC_THREAD_SECOND_DEF / IPMC_THREAD_TICK_TIME)
#define IPMC_THREAD_GCT_UNIT_BASE   (IPMC_THREAD_GC_TIME / IPMC_THREAD_TICK_TIME)
#define IPMC_THREAD_START_DELAY_CNT 15
#define IPMC_THREAD_MIN_DELAY_CNT   3

#define IPMC_EVENT_ANY              0xFFFFFFFF  // Any possible bit...
#define IPMC_EVENT_SM_TIME_WAKEUP   0x00000001
#define IPMC_EVENT_SWITCH_ADD       0x00000002
#define IPMC_EVENT_SWITCH_DEL       0x00000004
#define IPMC_EVENT_DB_TIME_WAKEUP   0x00000010
#define IPMC_EVENT_PKT4_HANDLER     0x00000100
#define IPMC_EVENT_PKT6_HANDLER     0x00001000
#define IPMC_EVENT_PROXY_LOCAL      0x00010000
#define IPMC_EVENT_GARBAGE_COLLECT4 0x00100000
#define IPMC_EVENT_GARBAGE_COLLECT6 0x00200000
#define IPMC_EVENT_MASTER_UP        0x01000000
#define IPMC_EVENT_MASTER_DOWN      0x02000000
#define IPMC_EVENT_THREAD_RESUME    0x10000000
#define IPMC_EVENT_THREAD_SUSPEND   0x20000000

/* IPMC Event Values */
#define IPMC_EVENT_VALUE_INIT       0x0
#define IPMC_EVENT_VALUE_SW_ADD     0x00000001
#define IPMC_EVENT_VALUE_SW_DEL     0x00000010

/*lint -esym(459, ipmc_sm_events) */
static vtss_flag_t         ipmc_sm_events;
static vtss::Timer         ipmc_sm_timer;
static u32                 ipmc_switch_event_value[VTSS_ISID_END];

static void ipmc_sm_event_set(vtss_flag_value_t flag)
{
    vtss_flag_setbits(&ipmc_sm_events, flag);
}

static void ipmc_sm_timer_isr(struct vtss::Timer *timer)
{
    ipmc_sm_event_set(IPMC_EVENT_SM_TIME_WAKEUP);
}

static void _ipmc_trans_cfg_intf_info(ipmc_prot_intf_entry_param_t *intf_param, ipmc_conf_intf_entry_t *entry)
{
    ipmc_querier_sm_t   *querier;

    if (!entry || !entry->valid || !intf_param) {
        return;
    }

    intf_param->vid = entry->vid;
    intf_param->cfg_compatibility = (ipmc_compat_mode_t)entry->compatibility;
    intf_param->priority = entry->priority;
    querier = &intf_param->querier;
    querier->querier_enabled = entry->querier_status;
    querier->QuerierAdrs4 = entry->querier4_address;
    querier->RobustVari = entry->robustness_variable;
    querier->QueryIntvl = entry->query_interval;;
    querier->MaxResTime = entry->query_response_interval;
    querier->LastQryItv = entry->last_listener_query_interval;
    querier->LastQryCnt = querier->RobustVari;
    querier->UnsolicitR = entry->unsolicited_report_interval;
}

static void _ipmc_reset_conf_intf(ipmc_conf_intf_entry_t *entry, BOOL valid, mesa_vid_t vid)
{
    entry->valid = valid;
    if (valid) {
        entry->vid = vid;
    } else {
        entry->vid = 0x0;
    }
    entry->protocol_status = IPMC_DEF_INTF_STATE_VALUE;
    entry->querier_status = IPMC_DEF_INTF_QUERIER_VALUE;
    entry->compatibility = IPMC_DEF_INTF_COMPAT;
    entry->priority = IPMC_DEF_INTF_PRI;
    entry->querier4_address = IPMC_DEF_INTF_QUERIER_ADRS4;
    entry->robustness_variable = IPMC_DEF_INTF_RV;
    entry->query_interval = IPMC_DEF_INTF_QI;
    entry->query_response_interval = IPMC_DEF_INTF_QRI;
    entry->last_listener_query_interval = IPMC_DEF_INTF_LMQI;
    entry->unsolicited_report_interval = IPMC_DEF_INTF_URI;
}

static u16 ipmc_conf_intf_entry_get_next(mesa_vid_t *vid, ipmc_ip_version_t ipmc_version)
{
    u16                     idx, ret = IPMC_VID_INIT;
    mesa_vid_t              next;
    ipmc_configuration_t    *conf;
    ipmc_conf_intf_entry_t  *intf;

    if (!vid || (*vid > VTSS_IPMC_VID_MAX)) {
        return ret;
    }

    conf = NULL;
    switch ( ipmc_version ) {
    case IPMC_IP_VERSION_IGMP:
        conf = &ipmc_global.ipv4_conf;

        break;
    case IPMC_IP_VERSION_MLD:
        conf = &ipmc_global.ipv6_conf;

        break;
    default:
        break;
    }

    if (!conf) {
        return ret;
    }

    next = 0;
    for (idx = 0; idx < IPMC_VLAN_MAX; idx++) {
        intf = &conf->ipmc_conf_intf_entries[idx];

        if (intf && intf->valid && (intf->vid > *vid)) {
            if (next) {
                if (intf->vid < next) {
                    next = intf->vid;
                    ret = idx;
                }
            } else {
                next = intf->vid;
                ret = idx;
            }
        }
    }

    if (next) {
        *vid = next;
    }

    return ret;
}

static u16 ipmc_conf_intf_entry_get(mesa_vid_t vid, ipmc_ip_version_t ipmc_version)
{
    u16                     idx, ret = IPMC_VID_INIT;
    ipmc_configuration_t    *conf;
    ipmc_conf_intf_entry_t  *intf;

    // The maximum number of VLANs is currently mapped to a capability function call.
    // We store this number in a variable to improve the performance, for example
    // when called from the VLAN change callback function
    u32                     vlan_max = IPMC_VLAN_MAX;

    if (vid > VTSS_IPMC_VID_MAX) {
        return ret;
    }

    conf = NULL;
    switch ( ipmc_version ) {
    case IPMC_IP_VERSION_IGMP:
        conf = &ipmc_global.ipv4_conf;

        break;
    case IPMC_IP_VERSION_MLD:
        conf = &ipmc_global.ipv6_conf;

        break;
    default:
        break;
    }

    if (!conf) {
        return ret;
    }

    for (idx = 0; idx < vlan_max; idx++) {
        intf = &conf->ipmc_conf_intf_entries[idx];

        if (intf && intf->valid && (intf->vid == vid)) {
            ret = idx;
            break;
        }
    }

    return ret;
}

static const char *ipmc_msg_id_txt(ipmc_msg_id_t msg_id)
{
    const char    *txt;

    switch ( msg_id ) {
    case IPMC_MSG_ID_MODE_SET_REQ:
        txt = "IPMC_MSG_ID_MODE_SET_REQ";
        break;
    case IPMC_MSG_ID_LEAVE_PROXY_SET_REQ:
        txt = "IPMC_MSG_ID_LEAVE_PROXY_SET_REQ";
        break;
    case IPMC_MSG_ID_PROXY_SET_REQ:
        txt = "IPMC_MSG_ID_PROXY_SET_REQ";
        break;
    case IPMC_MSG_ID_SSM_RANGE_SET_REQ:
        txt = "IPMC_MSG_ID_SSM_RANGE_SET_REQ";
        break;
    case IPMC_MSG_ID_ROUTER_PORT_SET_REQ:
        txt = "IPMC_MSG_ID_ROUTER_PORT_SET_REQ";
        break;
    case IPMC_MSG_ID_FAST_LEAVE_PORT_SET_REQ:
        txt = "IPMC_MSG_ID_FAST_LEAVE_PORT_SET_REQ";
        break;
    case IPMC_MSG_ID_UNREG_FLOOD_SET_REQ:
        txt = "IPMC_MSG_ID_UNREG_FLOOD_SET_REQ";
        break;
    case IPMC_MSG_ID_THROLLTING_MAX_NO_SET_REQ:
        txt = "IPMC_MSG_ID_THROLLTING_MAX_NO_SET_REQ";
        break;
    case IPMC_MSG_ID_PORT_GROUP_FILTERING_SET_REQ:
        txt = "IPMC_MSG_ID_PORT_GROUP_FILTERING_SET_REQ";
        break;
    case IPMC_MSG_ID_VLAN_SET_REQ:
        txt = "IPMC_MSG_ID_VLAN_SET_REQ";
        break;
    case IPMC_MSG_ID_VLAN_ENTRY_SET_REQ:
        txt = "IPMC_MSG_ID_VLAN_ENTRY_SET_REQ";
        break;
    case IPMC_MSG_ID_VLAN_ENTRY_GET_REQ:
        txt = "IPMC_MSG_ID_VLAN_ENTRY_GET_REQ";
        break;
    case IPMC_MSG_ID_VLAN_ENTRY_GET_REP:
        txt = "IPMC_MSG_ID_VLAN_ENTRY_GET_REP";
        break;
    case IPMC_MSG_ID_VLAN_GROUP_ENTRY_WALK_REQ:
        txt = "IPMC_MSG_ID_VLAN_GROUP_ENTRY_WALK_REQ";
        break;
    case IPMC_MSG_ID_VLAN_GROUP_ENTRY_WALK_REP:
        txt = "IPMC_MSG_ID_VLAN_GROUP_ENTRY_WALK_REP";
        break;
    case IPMC_MSG_ID_GROUP_SRCLIST_WALK_REQ:
        txt = "IPMC_MSG_ID_GROUP_SRCLIST_WALK_REQ";
        break;
    case IPMC_MSG_ID_GROUP_SRCLIST_WALK_REP:
        txt = "IPMC_MSG_ID_GROUP_SRCLIST_WALK_REP";
        break;
    case IPMC_MSG_ID_GROUP_ENTRY_GET_REQ:
        txt = "IPMC_MSG_ID_GROUP_ENTRY_GET_REQ";
        break;
    case IPMC_MSG_ID_GROUP_ENTRY_GET_REP:
        txt = "IPMC_MSG_ID_GROUP_ENTRY_GET_REP";
        break;
    case IPMC_MSG_ID_GROUP_ENTRY_GETNEXT_REQ:
        txt = "IPMC_MSG_ID_GROUP_ENTRY_GETNEXT_REQ";
        break;
    case IPMC_MSG_ID_GROUP_ENTRY_GETNEXT_REP:
        txt = "IPMC_MSG_ID_GROUP_ENTRY_GETNEXT_REP";
        break;
    case IPMC_MSG_ID_DYNAMIC_ROUTER_PORTS_GET_REQ:
        txt = "IPMC_MSG_ID_DYNAMIC_ROUTER_PORTS_GET_REQ";
        break;
    case IPMC_MSG_ID_DYNAMIC_ROUTER_PORTS_GET_REP:
        txt = "IPMC_MSG_ID_DYNAMIC_ROUTER_PORTS_GET_REP";
        break;
    case IPMC_MSG_ID_STAT_COUNTER_CLEAR_REQ:
        txt = "IPMC_MSG_ID_STAT_COUNTER_CLEAR_REQ";
        break;
    case IPMC_MSG_ID_STP_PORT_CHANGE_REQ:
        txt = "IPMC_MSG_ID_STP_PORT_CHANGE_REQ";
        break;
    default:
        txt = "?";
        break;
    }

    return txt;
}

/* Allocate request/reply buffer */
static void ipmc_msg_alloc(vtss_isid_t isid, ipmc_msg_id_t msg_id, ipmc_msg_buf_t *buf, BOOL from_get, BOOL request)
{
    u32 msg_size;

    T_I("(ISID%d)msg-%s (%d)", isid, ipmc_msg_id_txt(msg_id), msg_id);

    if (from_get) {
        IPMC_GET_CRIT_ENTER();
    } else {
        IPMC_CRIT_ENTER();
    }
    buf->sem = (request ? &ipmc_global.request.sem : &ipmc_global.reply.sem);
    buf->msg = ipmc_global.msg[msg_id];
    msg_size = ipmc_global.msize[msg_id];
    if (from_get) {
        IPMC_GET_CRIT_EXIT();
    } else {
        IPMC_CRIT_EXIT();
    }

    vtss_sem_wait(buf->sem);
    T_I("(ISID%d)msg-%s %s-LOCK", isid, ipmc_msg_id_txt(msg_id), request ? "REQ" : "REP");
    memset(buf->msg, 0x0, msg_size);
}

static void ipmc_msg_tx_done(void *contxt, void *msg, msg_tx_rc_t rc)
{
    ipmc_msg_id_t   msg_id = *(ipmc_msg_id_t *)msg;

    vtss_sem_post((vtss_sem_t *)contxt);
    T_I("msg_id: %d->%s-UNLOCK", msg_id, ipmc_msg_id_txt(msg_id));
}

static void ipmc_msg_tx(ipmc_msg_buf_t *buf, vtss_isid_t isid, size_t len)
{
    ipmc_msg_id_t   msg_id = *(ipmc_msg_id_t *)buf->msg;

    T_I("msg_id: %d->%s, len: %zd, isid: %s-%d",
        msg_id,
        ipmc_msg_id_txt(msg_id),
        len,
        ipmc_lib_isid_is_local(isid) ? "LOC" : "REM",
        isid);
    msg_tx_adv(buf->sem, ipmc_msg_tx_done, MSG_TX_OPT_DONT_FREE, VTSS_MODULE_ID_IPMC, isid, buf->msg, len);
}

static BOOL ipmc_msg_rx(void *contxt, const void *const rx_msg, size_t len, vtss_module_id_t modid, u32 isid)
{
    ipmc_msg_id_t       msg_id = *(ipmc_msg_id_t *)rx_msg;
    port_iter_t         pit;
    int                 i;
    vtss_tick_count_t   exe_time_base = vtss_current_time();
    T_D("msg_id: %d->%s, len: %zd, isid: %u", msg_id, ipmc_msg_id_txt(msg_id), len, isid);

    switch ( msg_id ) {
    case IPMC_MSG_ID_MODE_SET_REQ: {
        ipmc_msg_mode_set_req_t *msg;
        BOOL                    apply_mode;

        msg = (ipmc_msg_mode_set_req_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_MODE_SET_REQ(Version=%d|Mode=%s)",
            msg->version,
            msg->ipmc_mode_enabled ? "TRUE" : "FALSE");

        if (msg->ipmc_mode_enabled == VTSS_IPMC_ENABLE) {
            apply_mode = TRUE;
        } else {
            apply_mode = FALSE;
        }

        IPMC_CRIT_ENTER();
        if (msg->version == IPMC_IP_VERSION_IGMP) {
            ipmc_global.ipv4_conf.global.ipmc_mode_enabled = apply_mode;
        } else {
            ipmc_global.ipv6_conf.global.ipmc_mode_enabled = apply_mode;
        }
        vtss_ipmc_set_mode(apply_mode, msg->version);
        IPMC_CRIT_EXIT();

        if (msg->version == IPMC_IP_VERSION_IGMP) {
            ipmc_sm_event_set(IPMC_EVENT_PKT4_HANDLER);
        } else {
            ipmc_sm_event_set(IPMC_EVENT_PKT6_HANDLER);
        }

        break;
    }
    case IPMC_MSG_ID_LEAVE_PROXY_SET_REQ: {
        ipmc_msg_leave_proxy_set_req_t  *msg;

        msg = (ipmc_msg_leave_proxy_set_req_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_LEAVE_PROXY_SET_REQ");

        IPMC_CRIT_ENTER();
        if (msg->ipmc_leave_proxy_enabled == VTSS_IPMC_ENABLE) {
            vtss_ipmc_set_leave_proxy(TRUE, msg->version);
        } else {
            vtss_ipmc_set_leave_proxy(FALSE, msg->version);
        }
        IPMC_CRIT_EXIT();

        break;
    }
    case IPMC_MSG_ID_PROXY_SET_REQ: {
        ipmc_msg_proxy_set_req_t    *msg;

        msg = (ipmc_msg_proxy_set_req_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_PROXY_SET_REQ");

        IPMC_CRIT_ENTER();
        if (msg->ipmc_proxy_enabled == VTSS_IPMC_ENABLE) {
            vtss_ipmc_set_proxy(TRUE, msg->version);
        } else {
            vtss_ipmc_set_proxy(FALSE, msg->version);
        }
        IPMC_CRIT_EXIT();

        break;
    }
    case IPMC_MSG_ID_SSM_RANGE_SET_REQ: {
        ipmc_msg_ssm_range_set_req_t    *msg;

        msg = (ipmc_msg_ssm_range_set_req_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_SSM_RANGE_SET_REQ");

        IPMC_CRIT_ENTER();
        vtss_ipmc_set_ssm_range(msg->version, &msg->prefix);
        IPMC_CRIT_EXIT();

        break;
    }
    case IPMC_MSG_ID_ROUTER_PORT_SET_REQ: {
        ipmc_msg_router_port_set_req_t  *msg;
        ipmc_port_bfs_t                 static_router_port_mask_temp;

        msg = (ipmc_msg_router_port_set_req_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_ROUTER_PORT_SET_REQ");

        VTSS_PORT_BF_CLR(static_router_port_mask_temp.member_ports);
        if (msg_switch_is_master()) {
            (void) port_iter_init(&pit, NULL, msg->isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        } else {
            (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        }
        while (port_iter_getnext(&pit)) {
            i = pit.iport;
            if (msg->ipmc_router_ports[i]) {
                VTSS_PORT_BF_SET(static_router_port_mask_temp.member_ports, i, TRUE);
            }
        }

        IPMC_CRIT_ENTER();
        vtss_ipmc_set_static_router_ports(&static_router_port_mask_temp, msg->version);
        IPMC_CRIT_EXIT();

        break;
    }

    case IPMC_MSG_ID_FAST_LEAVE_PORT_SET_REQ: {
        ipmc_msg_fast_leave_port_set_req_t  *msg;
        ipmc_port_bfs_t                     static_fast_leave_port_mask_temp;

        msg = (ipmc_msg_fast_leave_port_set_req_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_FAST_LEAVE_PORT_SET_REQ");

        VTSS_PORT_BF_CLR(static_fast_leave_port_mask_temp.member_ports);
        if (msg_switch_is_master()) {
            (void) port_iter_init(&pit, NULL, msg->isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        } else {
            (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        }
        while (port_iter_getnext(&pit)) {
            i = pit.iport;
            if (msg->ipmc_fast_leave_ports[i]) {
                VTSS_PORT_BF_SET(static_fast_leave_port_mask_temp.member_ports, i, TRUE);
            }
        }

        IPMC_CRIT_ENTER();
        vtss_ipmc_set_static_fast_leave_ports(&static_fast_leave_port_mask_temp, msg->version);
        IPMC_CRIT_EXIT();

        break;
    }
    case IPMC_MSG_ID_THROLLTING_MAX_NO_SET_REQ: {
        ipmc_msg_throllting_max_no_set_req_t    *msg;
        ipmc_port_throttling_t                  static_ipmc_port_throttling;

        msg = (ipmc_msg_throllting_max_no_set_req_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_THROLLTING_MAX_NO_SET_REQ");

        vtss_clear(static_ipmc_port_throttling);
        if (msg_switch_is_master()) {
            (void) port_iter_init(&pit, NULL, msg->isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        } else {
            (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        }
        while (port_iter_getnext(&pit)) {
            static_ipmc_port_throttling.max_no[pit.iport] = msg->ipmc_throttling_max_no[pit.iport];
        }

        IPMC_CRIT_ENTER();
        vtss_ipmc_set_static_port_throttling_max_no(&static_ipmc_port_throttling, msg->version);
        IPMC_CRIT_EXIT();

        break;
    }
    case IPMC_MSG_ID_PORT_GROUP_FILTERING_SET_REQ: {
        ipmc_msg_port_group_filtering_set_req_t *msg;
        ipmc_port_group_filtering_t             static_ipmc_port_group_filtering_entry;

        msg = (ipmc_msg_port_group_filtering_set_req_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_PORT_GROUP_FILTERING_SET_REQ");

        vtss_clear(static_ipmc_port_group_filtering_entry);
        if (msg_switch_is_master()) {
            (void) port_iter_init(&pit, NULL, msg->isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        } else {
            (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        }
        while (port_iter_getnext(&pit)) {
            i = pit.iport;
            static_ipmc_port_group_filtering_entry.profile_index[i] = msg->ipmc_port_group_filtering[i];
        }

        IPMC_CRIT_ENTER();
        vtss_ipmc_set_static_port_group_filtering(&static_ipmc_port_group_filtering_entry, msg->version);
        IPMC_CRIT_EXIT();

        break;
    }
    case IPMC_MSG_ID_UNREG_FLOOD_SET_REQ: {
        ipmc_msg_unreg_flood_set_req_t  *msg;

        msg = (ipmc_msg_unreg_flood_set_req_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_UNREG_FLOOD_SET_REQ");

        IPMC_CRIT_ENTER();
        vtss_ipmc_set_unreg_flood(msg->ipmc_unreg_flood_enabled, msg->version);
        IPMC_CRIT_EXIT();

        break;
    }
    case IPMC_MSG_ID_VLAN_SET_REQ: {
        ipmc_msg_vlan_set_req_t *msg;

        msg = (ipmc_msg_vlan_set_req_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_VLAN_SET_REQ");
        T_D("Action: %s (%d->%s/%s[VER-%d])",
            msg->action ? "ADD" : "DEL",
            msg->vlan_entry.vid,
            msg->vlan_entry.protocol_status ? "Enable" : "Disable",
            msg->vlan_entry.querier_status ? "Q" : "!Q",
            msg->version);

        IPMC_CRIT_ENTER();
        if (msg->action == IPMC_OP_DEL) {
            (void) vtss_ipmc_del_intf_entry(msg->vlan_entry.vid, msg->version);
        } else {
            (void) vtss_ipmc_set_intf_entry(msg->vlan_entry.vid,
                                            msg->vlan_entry.protocol_status,
                                            msg->vlan_entry.querier_status,
                                            &msg->vlan_member,
                                            msg->version);
        }
        IPMC_CRIT_EXIT();

        break;
    }
    case IPMC_MSG_ID_VLAN_ENTRY_SET_REQ: {
        ipmc_msg_vlan_set_req_t *msg;

        msg = (ipmc_msg_vlan_set_req_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_VLAN_ENTRY_SET_REQ");

        IPMC_CRIT_ENTER();
        if (!vtss_ipmc_upd_intf_entry(&msg->vlan_entry, msg->version)) {
            T_D("IPMC_MSG_ID_VLAN_ENTRY_SET_REQ Failed!");
        }
        IPMC_CRIT_EXIT();

        break;
    }
    case IPMC_MSG_ID_VLAN_ENTRY_GET_REQ: {
        ipmc_msg_vlan_entry_get_req_t   *msg;
        ipmc_intf_entry_t               *intf;
        ipmc_msg_vlan_entry_get_rep_t   *msg_rep;
        ipmc_msg_buf_t                  buf;
        BOOL                            valid = FALSE;

        msg = (ipmc_msg_vlan_entry_get_req_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_VLAN_ENTRY_GET_REQ (VID:%d/VER:%d)", msg->vid, msg->version);

        ipmc_msg_alloc(isid, IPMC_MSG_ID_VLAN_ENTRY_GET_REP, &buf, FALSE, FALSE);
        IPMC_CRIT_ENTER();
        msg_rep = (ipmc_msg_vlan_entry_get_rep_t *)buf.msg;
        if ((intf = vtss_ipmc_get_intf_entry(msg->vid, msg->version)) != NULL) {
            memcpy(&msg_rep->interface_entry, &intf->param, sizeof(ipmc_prot_intf_entry_param_t));
            valid = TRUE;
        } else {
            ipmc_conf_intf_entry_t  cur_intf;
            u16                     intf_idx;

            if ((intf_idx = ipmc_conf_intf_entry_get(msg->vid, msg->version)) != IPMC_VID_INIT) {
                switch ( msg->version ) {
                case IPMC_IP_VERSION_IGMP:
                    memcpy(&cur_intf, &ipmc_global.ipv4_conf.ipmc_conf_intf_entries[intf_idx], sizeof(ipmc_conf_intf_entry_t));
                    valid = TRUE;

                    break;
                case IPMC_IP_VERSION_MLD:
                    memcpy(&cur_intf, &ipmc_global.ipv6_conf.ipmc_conf_intf_entries[intf_idx], sizeof(ipmc_conf_intf_entry_t));
                    valid = TRUE;

                    break;
                default:

                    break;
                }

                if (valid) {
                    memset(&msg_rep->interface_entry, 0x0, sizeof(ipmc_prot_intf_entry_param_t));
                    _ipmc_trans_cfg_intf_info(&msg_rep->interface_entry, &cur_intf);
                }
            }
        }
        IPMC_CRIT_EXIT();

        msg_rep->msg_id = IPMC_MSG_ID_VLAN_ENTRY_GET_REP;
        if (!valid) {
            msg_rep->interface_entry.vid = 0;
            T_D("no such vlan (%d)", msg->vid);
        }
        ipmc_msg_tx(&buf, isid, sizeof(*msg_rep));

        break;
    }
    case IPMC_MSG_ID_VLAN_ENTRY_GET_REP: {
        ipmc_msg_vlan_entry_get_rep_t   *msg;

        msg = (ipmc_msg_vlan_entry_get_rep_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_VLAN_ENTRY_GET_REP");

        IPMC_CRIT_ENTER();
        memcpy(&ipmc_global.interface_entry, &msg->interface_entry, sizeof(ipmc_prot_intf_entry_param_t));
        vtss_flag_setbits(&ipmc_global.vlan_entry_flags, 1 << isid);
        IPMC_CRIT_EXIT();

        break;
    }
    case IPMC_MSG_ID_VLAN_GROUP_ENTRY_WALK_REQ: {
        ipmc_msg_vlan_entry_get_req_t       *msg;
        ipmc_msg_vlan_group_entry_get_rep_t *msg_rep;
        ipmc_msg_buf_t                      buf;
        ipmc_group_entry_t                  grp;
        BOOL                                valid = FALSE;

        msg = (ipmc_msg_vlan_entry_get_req_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_VLAN_GROUP_ENTRY_WALK_REQ");

        grp.ipmc_version = msg->version;
        grp.vid = msg->vid;
        memcpy(&grp.group_addr, &msg->group_addr, sizeof(mesa_ipv6_t));

        ipmc_msg_alloc(isid, IPMC_MSG_ID_VLAN_GROUP_ENTRY_WALK_REP, &buf, FALSE, FALSE);
        IPMC_CRIT_ENTER();
        if (vtss_ipmc_get_next_intf_group_entry(msg->vid, &grp, msg->version)) {
            valid = TRUE;
        }
        IPMC_CRIT_EXIT();

        msg_rep = (ipmc_msg_vlan_group_entry_get_rep_t *)buf.msg;
        msg_rep->msg_id = IPMC_MSG_ID_VLAN_GROUP_ENTRY_WALK_REP;
        msg_rep->intf_group_entry.valid = FALSE;
        if (valid) {
            msg_rep->intf_group_entry.ipmc_version = grp.ipmc_version;
            msg_rep->intf_group_entry.vid = grp.vid;
            memcpy(&msg_rep->intf_group_entry.group_addr, &grp.group_addr, sizeof(mesa_ipv6_t));
            memcpy(&msg_rep->intf_group_entry.db, &grp.info->db, sizeof(ipmc_group_db_t));

            if (!msg_switch_is_master()) {
                msg_rep->intf_group_entry.db.ipmc_sf_do_forward_srclist = NULL;
                msg_rep->intf_group_entry.db.ipmc_sf_do_not_forward_srclist = NULL;
                IPMC_FLTR_TIMER_DELTA_GET(&msg_rep->intf_group_entry.db);
            }

            msg_rep->intf_group_entry.valid = TRUE;
        } else {
            T_D("no more group entry in vlan-%d", msg->vid);
        }
        ipmc_msg_tx(&buf, isid, sizeof(*msg_rep));

        break;
    }
    case IPMC_MSG_ID_VLAN_GROUP_ENTRY_WALK_REP: {
        ipmc_msg_vlan_group_entry_get_rep_t *msg;

        msg = (ipmc_msg_vlan_group_entry_get_rep_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_VLAN_GROUP_ENTRY_WALK_REP");

        IPMC_CRIT_ENTER();
        memcpy(&ipmc_global.intf_group_entry, &msg->intf_group_entry, sizeof(ipmc_prot_intf_group_entry_t));
        vtss_flag_setbits(&ipmc_global.vlan_entry_flags, 1 << isid);
        IPMC_CRIT_EXIT();

        break;
    }
    case IPMC_MSG_ID_GROUP_ENTRY_GET_REQ:
    case IPMC_MSG_ID_GROUP_ENTRY_GETNEXT_REQ: {
        ipmc_msg_group_entry_itr_req_t  *msg;
        ipmc_msg_group_entry_itr_rep_t  *msg_rep;
        ipmc_msg_buf_t                  buf;
        ipmc_group_entry_t              grp;
        BOOL                            valid = FALSE;

        msg = (ipmc_msg_group_entry_itr_req_t *)rx_msg;
        T_D("Receiving %s", ipmc_msg_id_txt(msg_id));

        grp.ipmc_version = msg->version;
        grp.vid = msg->vid;
        IPMC_LIB_ADRS_CPY(&grp.group_addr, &msg->group_addr);
        if (msg_id == IPMC_MSG_ID_GROUP_ENTRY_GET_REQ) {
            ipmc_msg_alloc(isid, IPMC_MSG_ID_GROUP_ENTRY_GET_REP, &buf, FALSE, FALSE);
        } else {
            ipmc_msg_alloc(isid, IPMC_MSG_ID_GROUP_ENTRY_GETNEXT_REP, &buf, FALSE, FALSE);
        }

        IPMC_CRIT_ENTER();
        if (msg_id == IPMC_MSG_ID_GROUP_ENTRY_GET_REQ) {
            if (vtss_ipmc_group_entry_get(&msg->version, &msg->vid, &msg->group_addr, &grp) == VTSS_RC_OK) {
                valid = TRUE;
            }
        } else {
            if (vtss_ipmc_group_entry_get_next(&msg->version, &msg->vid, &msg->group_addr, &grp) == VTSS_RC_OK) {
                if (grp.ipmc_version == msg->version) {
                    valid = TRUE;
                }
            }
        }
        IPMC_CRIT_EXIT();

        msg_rep = (ipmc_msg_group_entry_itr_rep_t *)buf.msg;
        if (msg_id == IPMC_MSG_ID_GROUP_ENTRY_GET_REQ) {
            msg_rep->msg_id = IPMC_MSG_ID_GROUP_ENTRY_GET_REP;
        } else {
            msg_rep->msg_id = IPMC_MSG_ID_GROUP_ENTRY_GETNEXT_REP;
        }

        msg_rep->group_entry.valid = valid;
        if (valid) {
            msg_rep->group_entry.ipmc_version = grp.ipmc_version;
            msg_rep->group_entry.vid = grp.vid;
            IPMC_LIB_ADRS_CPY(&msg_rep->group_entry.group_addr, &grp.group_addr);
            memcpy(&msg_rep->group_entry.db, &grp.info->db, sizeof(ipmc_group_db_t));

            if (!msg_switch_is_master()) {
                msg_rep->group_entry.db.ipmc_sf_do_forward_srclist = NULL;
                msg_rep->group_entry.db.ipmc_sf_do_not_forward_srclist = NULL;
                IPMC_FLTR_TIMER_DELTA_GET(&msg_rep->group_entry.db);
            }
        } else {
            T_D("no more group entry in vlan-%u", msg->vid);
        }
        ipmc_msg_tx(&buf, isid, sizeof(*msg_rep));

        break;
    }
    case IPMC_MSG_ID_GROUP_ENTRY_GET_REP:
    case IPMC_MSG_ID_GROUP_ENTRY_GETNEXT_REP: {
        ipmc_msg_group_entry_itr_rep_t  *msg;

        msg = (ipmc_msg_group_entry_itr_rep_t *)rx_msg;
        T_D("Receiving %s", ipmc_msg_id_txt(msg_id));

        IPMC_CRIT_ENTER();
        if (msg_id == IPMC_MSG_ID_GROUP_ENTRY_GET_REP) {
            memcpy(&ipmc_global.group_entry, &msg->group_entry, sizeof(ipmc_prot_intf_group_entry_t));
            vtss_flag_setbits(&ipmc_global.grp_entry_flags, 1 << isid);
        } else {
            memcpy(&ipmc_global.group_next_entry, &msg->group_entry, sizeof(ipmc_prot_intf_group_entry_t));
            vtss_flag_setbits(&ipmc_global.grp_nxt_entry_flags, 1 << isid);
        }
        IPMC_CRIT_EXIT();

        break;
    }
    case IPMC_MSG_ID_GROUP_SRCLIST_WALK_REQ: {
        ipmc_msg_vlan_entry_get_req_t       *msg;
        ipmc_msg_group_srclist_get_rep_t    *msg_rep;
        ipmc_msg_buf_t                      buf;
        ipmc_group_entry_t                  grp;
        BOOL                                valid = FALSE;

        msg = (ipmc_msg_vlan_entry_get_req_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_GROUP_SRCLIST_WALK_REQ");

        grp.ipmc_version = msg->version;
        grp.vid = msg->vid;
        memcpy(&grp.group_addr, &msg->group_addr, sizeof(mesa_ipv6_t));

        ipmc_msg_alloc(isid, IPMC_MSG_ID_GROUP_SRCLIST_WALK_REP, &buf, FALSE, FALSE);
        IPMC_CRIT_ENTER();
        if (vtss_ipmc_get_intf_group_entry(msg->vid, &grp, msg->version)) {
            valid = TRUE;
        }
        IPMC_CRIT_EXIT();

        msg_rep = (ipmc_msg_group_srclist_get_rep_t *)buf.msg;
        msg_rep->msg_id = IPMC_MSG_ID_GROUP_SRCLIST_WALK_REP;

        msg_rep->group_srclist_entry.valid = FALSE;
        if (valid) {
            ipmc_db_ctrl_hdr_t  *slist;

            if (msg->srclist_type) {
                slist = grp.info->db.ipmc_sf_do_forward_srclist;
            } else {
                slist = grp.info->db.ipmc_sf_do_not_forward_srclist;
            }

            if (slist &&
                ((msg_rep->group_srclist_entry.cntr = IPMC_LIB_DB_GET_COUNT(slist)) != 0)) {
                ipmc_sfm_srclist_t  entry;

                memcpy(&entry.src_ip, &msg->srclist_addr, sizeof(mesa_ipv6_t));
                if (ipmc_lib_srclist_buf_get_next(slist, &entry)) {
                    msg_rep->group_srclist_entry.type = msg->srclist_type;
                    memcpy(&msg_rep->group_srclist_entry.srclist, &entry, sizeof(ipmc_sfm_srclist_t));
                    IPMC_SRCT_TIMER_DELTA_GET(&msg_rep->group_srclist_entry.srclist);

                    msg_rep->group_srclist_entry.valid = TRUE;
                }
            }
        }
        ipmc_msg_tx(&buf, isid, sizeof(*msg_rep));

        break;
    }
    case IPMC_MSG_ID_GROUP_SRCLIST_WALK_REP: {
        ipmc_msg_group_srclist_get_rep_t    *msg;

        msg = (ipmc_msg_group_srclist_get_rep_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_GROUP_SRCLIST_WALK_REP");

        IPMC_CRIT_ENTER();
        memcpy(&ipmc_global.group_srclist_entry, &msg->group_srclist_entry, sizeof(ipmc_prot_group_srclist_t));
        vtss_flag_setbits(&ipmc_global.vlan_entry_flags, 1 << isid);
        IPMC_CRIT_EXIT();

        break;
    }
    case IPMC_MSG_ID_DYNAMIC_ROUTER_PORTS_GET_REQ: {
        ipmc_msg_dyn_rtpt_get_req_t             *msg;
        ipmc_msg_dynamic_router_ports_get_rep_t *msg_rep;
        ipmc_msg_buf_t                          buf;
        ipmc_port_bfs_t                         router_port_bitmask;

        msg = (ipmc_msg_dyn_rtpt_get_req_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_DYNAMIC_ROUTER_PORTS_GET_REQ");

        ipmc_msg_alloc(isid, IPMC_MSG_ID_DYNAMIC_ROUTER_PORTS_GET_REP, &buf, FALSE, FALSE);
        IPMC_GET_CRIT_ENTER();
        (void)vtss_ipmc_get_dynamic_router_port_mask(msg->version, &router_port_bitmask);
        IPMC_GET_CRIT_EXIT();

        msg_rep = (ipmc_msg_dynamic_router_ports_get_rep_t *)buf.msg;
        msg_rep->msg_id = IPMC_MSG_ID_DYNAMIC_ROUTER_PORTS_GET_REP;
        msg_rep->version = msg->version;
        if (msg_switch_is_master()) {
            (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        } else {
            (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        }
        while (port_iter_getnext(&pit)) {
            i = pit.iport;
            if (VTSS_PORT_BF_GET(router_port_bitmask.member_ports, i)) {
                msg_rep->dynamic_router_ports.ports[i] = TRUE;
            } else {
                msg_rep->dynamic_router_ports.ports[i] = FALSE;
            }
        }
        ipmc_msg_tx(&buf, isid, sizeof(*msg_rep));

        break;
    }
    case IPMC_MSG_ID_DYNAMIC_ROUTER_PORTS_GET_REP: {
        ipmc_msg_dynamic_router_ports_get_rep_t *msg;

        msg = (ipmc_msg_dynamic_router_ports_get_rep_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_DYNAMIC_ROUTER_PORTS_GET_REP");

        IPMC_CRIT_ENTER();
        if (msg_switch_is_master()) {
            (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        } else {
            (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        }
        while (port_iter_getnext(&pit)) {
            i = pit.iport;
            ipmc_global.dynamic_router_ports[isid - VTSS_ISID_START][i] = msg->dynamic_router_ports.ports[i];
        }
        vtss_flag_setbits(&ipmc_global.dynamic_router_ports_getting_flags, 1 << isid);
        IPMC_CRIT_EXIT();

        break;
    }
    case IPMC_MSG_ID_STAT_COUNTER_CLEAR_REQ: {
        ipmc_msg_stat_counter_clear_req_t   *msg;

        msg = (ipmc_msg_stat_counter_clear_req_t *)rx_msg;
        T_D("Receiving IPMC_MSG_ID_STAT_COUNTER_CLEAR_REQ");

        IPMC_CRIT_ENTER();
        vtss_ipmc_clear_stat_counter(msg->version, msg->vid);
        IPMC_CRIT_EXIT();

        break;
    }
    case IPMC_MSG_ID_STP_PORT_CHANGE_REQ: {
        ipmc_msg_stp_port_change_set_req_t  *msg;
        ipmc_port_bfs_t                     router_port_bitmask;

        msg = (ipmc_msg_stp_port_change_set_req_t *) rx_msg;
        T_D("Receiving IPMC_MSG_ID_STP_PORT_CHANGE_REQ");

        VTSS_PORT_BF_CLR(router_port_bitmask.member_ports);

        IPMC_CRIT_ENTER();
        /* We should filter sendout process if this port belongs to dynamic router port. */
        switch ( msg->version ) {
        case IPMC_IP_VERSION_ALL:
            ipmc_lib_get_discovered_router_port_mask(IPMC_IP_VERSION_IGMP, &router_port_bitmask);
            if (!VTSS_PORT_BF_GET(router_port_bitmask.member_ports, msg->port)) {
                vtss_ipmc_stp_port_state_change_handle(IPMC_IP_VERSION_IGMP, msg->port, msg->new_state);
            }

            VTSS_PORT_BF_CLR(router_port_bitmask.member_ports);
            ipmc_lib_get_discovered_router_port_mask(IPMC_IP_VERSION_MLD, &router_port_bitmask);
            if (!VTSS_PORT_BF_GET(router_port_bitmask.member_ports, msg->port)) {
                vtss_ipmc_stp_port_state_change_handle(IPMC_IP_VERSION_MLD, msg->port, msg->new_state);
            }

            break;
        case IPMC_IP_VERSION_IGMP:
        case IPMC_IP_VERSION_MLD:
            ipmc_lib_get_discovered_router_port_mask(msg->version, &router_port_bitmask);
            if (!VTSS_PORT_BF_GET(router_port_bitmask.member_ports, msg->port)) {
                vtss_ipmc_stp_port_state_change_handle(msg->version, msg->port, msg->new_state);
            }

            break;
        default:

            break;
        }
        IPMC_CRIT_EXIT();

        break;
    }
    default:
        T_E("unknown message ID: %d", msg_id);

        break;
    }

    T_D("ipmc_msg_rx(%u) consumes ID%u:%u ticks", (u32)msg_id, isid, (u32)(vtss_current_time() - exe_time_base));

    return TRUE;
}

static u16 _ipmc_conf_intf_entry_add(mesa_vid_t vid, ipmc_ip_version_t ipmc_version)
{
    u16                     idx, ret;
    ipmc_configuration_t    *conf;
    ipmc_conf_intf_entry_t  *intf;

    if (vid > VTSS_IPMC_VID_MAX) {
        return IPMC_VID_INIT;
    }

    conf = NULL;
    switch ( ipmc_version ) {
    case IPMC_IP_VERSION_IGMP:
        conf = &ipmc_global.ipv4_conf;

        break;
    case IPMC_IP_VERSION_MLD:
        conf = &ipmc_global.ipv6_conf;

        break;
    default:
        break;
    }

    if (!conf) {
        return IPMC_VID_INIT;
    }

    ret = IPMC_VID_INIT;
    for (idx = 0; idx < IPMC_VLAN_MAX; idx++) {
        if ((intf = &conf->ipmc_conf_intf_entries[idx]) == NULL) {
            continue;
        }

        if (intf->valid) {
            if (intf->vid == vid) {
                return IPMC_VID_INIT;
            }
        } else {
            ret = idx;  /* pick last available index */
        }
    }

    if (ret == IPMC_VID_INIT) {
        return IPMC_VID_INIT;   /* Table Full */
    }

    _ipmc_reset_conf_intf(&conf->ipmc_conf_intf_entries[ret], TRUE, vid);
    return ret;
}

static BOOL _ipmc_conf_intf_entry_del(mesa_vid_t vid, ipmc_ip_version_t ipmc_version)
{
    u16                     idx, chk;
    ipmc_configuration_t    *conf;
    ipmc_conf_intf_entry_t  *intf;

    if (vid > VTSS_IPMC_VID_MAX) {
        return FALSE;
    }

    conf = NULL;
    switch ( ipmc_version ) {
    case IPMC_IP_VERSION_IGMP:
        conf = &ipmc_global.ipv4_conf;

        break;
    case IPMC_IP_VERSION_MLD:
        conf = &ipmc_global.ipv6_conf;

        break;
    default:
        break;
    }

    if (!conf) {
        return FALSE;
    }

    chk = IPMC_VID_INIT;
    for (idx = 0; idx < IPMC_VLAN_MAX; idx++) {
        if ((intf = &conf->ipmc_conf_intf_entries[idx]) == NULL) {
            continue;
        }

        if (intf->valid && (intf->vid == vid)) {
            chk = idx;
            break;
        }
    }

    if (chk == IPMC_VID_INIT) {
        return FALSE;   /* Not Found */
    }

    _ipmc_reset_conf_intf(&conf->ipmc_conf_intf_entries[chk], FALSE, vid);
    vtss_ipmc_del_intf_entry(vid, ipmc_version);
    return TRUE;
}

static BOOL _ipmc_conf_intf_entry_upd(ipmc_conf_intf_entry_t *entry, ipmc_ip_version_t ipmc_version)
{
    u16                     idx, chk;
    mesa_vid_t              vid;
    ipmc_configuration_t    *conf;
    ipmc_conf_intf_entry_t  *intf;

    if (!entry || ((vid = entry->vid) > VTSS_IPMC_VID_MAX)) {
        return FALSE;
    }

    conf = NULL;
    switch ( ipmc_version ) {
    case IPMC_IP_VERSION_IGMP:
        conf = &ipmc_global.ipv4_conf;

        break;
    case IPMC_IP_VERSION_MLD:
        conf = &ipmc_global.ipv6_conf;

        break;
    default:
        break;
    }

    if (!conf) {
        return FALSE;
    }

    chk = IPMC_VID_INIT;
    for (idx = 0; idx < IPMC_VLAN_MAX; idx++) {
        if ((intf = &conf->ipmc_conf_intf_entries[idx]) == NULL) {
            continue;
        }

        if (intf->valid && (intf->vid == vid)) {
            chk = idx;
            break;
        }
    }

    if (chk == IPMC_VID_INIT) {
        return FALSE;   /* Not Found */
    }

    intf = &conf->ipmc_conf_intf_entries[chk];
    memcpy(intf, entry, sizeof(ipmc_conf_intf_entry_t));
    memset(intf->reserved, 0x0, sizeof(intf->reserved));
    intf->valid = TRUE;
    return TRUE;
}

/* Set STACK IPMC VLAN ENTRY */
static mesa_rc ipmc_stacking_set_intf(vtss_isid_t isid_add, ipmc_operation_action_t action, ipmc_conf_intf_entry_t *vlan_entry, ipmc_port_bfs_t *vlan_ports, ipmc_ip_version_t ipmc_version)
{
    ipmc_msg_buf_t              buf;
    ipmc_msg_vlan_set_req_t     *msg;
    switch_iter_t               sit;
    vtss_isid_t                 isid;

    if (!msg_switch_is_master() || !vlan_entry || !vlan_ports) {
        return VTSS_RC_ERROR;
    }

    T_D("enter, isid: %d", isid_add);

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if ((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) {
            continue;
        }

        if (ipmc_lib_isid_is_local(isid)) {
            IPMC_CRIT_ENTER();
            if (action == IPMC_OP_DEL) {
                (void) vtss_ipmc_del_intf_entry(vlan_entry->vid, ipmc_version);
            } else {
                (void) vtss_ipmc_set_intf_entry(vlan_entry->vid,
                                                vlan_entry->protocol_status,
                                                vlan_entry->querier_status,
                                                vlan_ports,
                                                ipmc_version);
            }
            IPMC_CRIT_EXIT();
        } else {
            ipmc_msg_alloc(isid, IPMC_MSG_ID_VLAN_SET_REQ, &buf, FALSE, TRUE);
            msg = (ipmc_msg_vlan_set_req_t *)buf.msg;

            msg->msg_id = IPMC_MSG_ID_VLAN_SET_REQ;
            msg->action = action;
            msg->vlan_entry.valid = vlan_entry->valid;
            msg->vlan_entry.vid = vlan_entry->vid;
            msg->vlan_entry.protocol_status = vlan_entry->protocol_status;
            msg->vlan_entry.querier_status = vlan_entry->querier_status;
            msg->vlan_entry.compatibility = vlan_entry->compatibility;
            msg->vlan_entry.priority = vlan_entry->priority;
            msg->vlan_entry.querier4_address = vlan_entry->querier4_address;
            msg->vlan_entry.robustness_variable = vlan_entry->robustness_variable;
            msg->vlan_entry.query_interval = vlan_entry->query_interval;
            msg->vlan_entry.query_response_interval = vlan_entry->query_response_interval;
            msg->vlan_entry.last_listener_query_interval = vlan_entry->last_listener_query_interval;
            msg->vlan_entry.unsolicited_report_interval = vlan_entry->unsolicited_report_interval;
            msg->vlan_member = *vlan_ports;
            msg->version = ipmc_version;
            ipmc_msg_tx(&buf, isid, sizeof(*msg));
        }
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

/* IPMC error text */
const char *ipmc_error_txt(ipmc_error_t rc)
{
    const char *txt;

    switch ( rc ) {
    case IPMC_ERROR_GEN:
        txt = "IPMC generic error";
        break;
    case IPMC_ERROR_PARM:
        txt = "IPMC parameter error";
        break;
    case IPMC_ERROR_VLAN_NOT_FOUND:
        txt = "IPMC no such VLAN ID";
        break;
    default:
        txt = "IPMC unknown error";
        break;
    }

    return txt;
}

static mesa_rc ipmc_stacking_register(void)
{
    msg_rx_filter_t filter;

    memset(&filter, 0, sizeof(filter));
    filter.cb = ipmc_msg_rx;
    filter.modid = VTSS_MODULE_ID_IPMC;
    return msg_rx_filter_register(&filter);
}

/* Set STACK IPMC MODE */
static BOOL mode_setting;
static mesa_rc ipmc_stacking_set_mode(vtss_isid_t isid_add, ipmc_ip_version_t ipmc_version, BOOL force_off)
{
    ipmc_msg_buf_t          buf;
    ipmc_msg_mode_set_req_t *msg;
    switch_iter_t           sit;
    vtss_isid_t             isid;
    BOOL                    apply_mode;

    if (!msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    T_D("enter, isid: %d", isid_add);

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if ((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) {
            continue;
        }

        if (ipmc_lib_isid_is_local(isid)) {
            IPMC_CRIT_ENTER();
            switch ( ipmc_version ) {
            case IPMC_IP_VERSION_IGMP:
                if (force_off) {
                    mode_setting = VTSS_IPMC_DISABLE;
                    ipmc_global.ipv4_conf.global.ipmc_mode_enabled = mode_setting;
                } else {
                    mode_setting = ipmc_global.ipv4_conf.global.ipmc_mode_enabled;
                }

                break;
            case IPMC_IP_VERSION_MLD:
                if (force_off) {
                    mode_setting = VTSS_IPMC_DISABLE;
                    ipmc_global.ipv6_conf.global.ipmc_mode_enabled = mode_setting;
                } else {
                    mode_setting = ipmc_global.ipv6_conf.global.ipmc_mode_enabled;
                }

                break;
            default:
                mode_setting = IPMC_DEF_GLOBAL_STATE_VALUE;

                break;
            }
            apply_mode = mode_setting;
            IPMC_CRIT_EXIT();

            if (apply_mode == VTSS_IPMC_ENABLE) {
                IPMC_CRIT_ENTER();
                vtss_ipmc_set_mode(TRUE, ipmc_version);
                IPMC_CRIT_EXIT();
            } else {
                IPMC_CRIT_ENTER();
                vtss_ipmc_set_mode(FALSE, ipmc_version);
                IPMC_CRIT_EXIT();
            }

            if (ipmc_version == IPMC_IP_VERSION_IGMP) {
                ipmc_sm_event_set(IPMC_EVENT_PKT4_HANDLER);
            } else {
                ipmc_sm_event_set(IPMC_EVENT_PKT6_HANDLER);
            }

            continue;
        }

        ipmc_msg_alloc(isid, IPMC_MSG_ID_MODE_SET_REQ, &buf, FALSE, TRUE);
        msg = (ipmc_msg_mode_set_req_t *)buf.msg;

        if (force_off) {
            msg->ipmc_mode_enabled = VTSS_IPMC_DISABLE;
        } else {
            IPMC_CRIT_ENTER();
            switch ( ipmc_version ) {
            case IPMC_IP_VERSION_IGMP:
                msg->ipmc_mode_enabled = ipmc_global.ipv4_conf.global.ipmc_mode_enabled;

                break;
            case IPMC_IP_VERSION_MLD:
                msg->ipmc_mode_enabled = ipmc_global.ipv6_conf.global.ipmc_mode_enabled;

                break;
            default:
                msg->ipmc_mode_enabled = IPMC_DEF_GLOBAL_STATE_VALUE;

                break;
            }
            IPMC_CRIT_EXIT();
        }

        msg->msg_id = IPMC_MSG_ID_MODE_SET_REQ;
        msg->version = ipmc_version;
        T_D("SND IPMC_MSG_ID_MODE_SET_REQ(Version=%d|Mode=%s)",
            msg->version,
            msg->ipmc_mode_enabled ? "TRUE" : "FALSE");
        ipmc_msg_tx(&buf, isid, sizeof(*msg));
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

/* Set STACK IPMC Leave Proxy */
static BOOL leaveproxy_setting;
static mesa_rc ipmc_stacking_set_leave_proxy(vtss_isid_t isid_add, ipmc_ip_version_t ipmc_version)
{
    ipmc_msg_buf_t                  buf;
    ipmc_msg_leave_proxy_set_req_t  *msg;
    switch_iter_t                   sit;
    vtss_isid_t                     isid;

    if (!msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    T_D("enter, isid: %d", isid_add);

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if ((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) {
            continue;
        }

        if (ipmc_lib_isid_is_local(isid)) {
            IPMC_CRIT_ENTER();
            switch ( ipmc_version ) {
            case IPMC_IP_VERSION_IGMP:
                leaveproxy_setting = ipmc_global.ipv4_conf.global.ipmc_leave_proxy_enabled;

                break;
            case IPMC_IP_VERSION_MLD:
                leaveproxy_setting = ipmc_global.ipv6_conf.global.ipmc_leave_proxy_enabled;

                break;
            default:
                leaveproxy_setting = IPMC_DEF_LEAVE_PROXY_VALUE;

                break;
            }

            if (leaveproxy_setting == VTSS_IPMC_ENABLE) {
                vtss_ipmc_set_leave_proxy(TRUE, ipmc_version);
            } else {
                vtss_ipmc_set_leave_proxy(FALSE, ipmc_version);
            }
            IPMC_CRIT_EXIT();

            continue;
        }

        ipmc_msg_alloc(isid, IPMC_MSG_ID_LEAVE_PROXY_SET_REQ, &buf, FALSE, TRUE);
        msg = (ipmc_msg_leave_proxy_set_req_t *)buf.msg;

        IPMC_CRIT_ENTER();
        switch ( ipmc_version ) {
        case IPMC_IP_VERSION_IGMP:
            msg->ipmc_leave_proxy_enabled = ipmc_global.ipv4_conf.global.ipmc_leave_proxy_enabled;

            break;
        case IPMC_IP_VERSION_MLD:
            msg->ipmc_leave_proxy_enabled = ipmc_global.ipv6_conf.global.ipmc_leave_proxy_enabled;

            break;
        default:
            msg->ipmc_leave_proxy_enabled = IPMC_DEF_LEAVE_PROXY_VALUE;

            break;
        }
        IPMC_CRIT_EXIT();

        msg->msg_id = IPMC_MSG_ID_LEAVE_PROXY_SET_REQ;
        msg->version = ipmc_version;
        ipmc_msg_tx(&buf, isid, sizeof(*msg));
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

/* Set STACK IPMC Proxy */
static BOOL proxy_setting;
static mesa_rc ipmc_stacking_set_proxy(vtss_isid_t isid_add, ipmc_ip_version_t ipmc_version)
{
    ipmc_msg_buf_t              buf;
    ipmc_msg_proxy_set_req_t    *msg;
    switch_iter_t               sit;
    vtss_isid_t                 isid;

    if (!msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    T_D("enter, isid: %d", isid_add);

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if ((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) {
            continue;
        }

        if (ipmc_lib_isid_is_local(isid)) {
            IPMC_CRIT_ENTER();
            switch ( ipmc_version ) {
            case IPMC_IP_VERSION_IGMP:
                proxy_setting = ipmc_global.ipv4_conf.global.ipmc_proxy_enabled;

                break;
            case IPMC_IP_VERSION_MLD:
                proxy_setting = ipmc_global.ipv6_conf.global.ipmc_proxy_enabled;

                break;
            default:
                proxy_setting = IPMC_DEF_PROXY_VALUE;

                break;
            }

            if (proxy_setting == VTSS_IPMC_ENABLE) {
                vtss_ipmc_set_proxy(TRUE, ipmc_version);
            } else {
                vtss_ipmc_set_proxy(FALSE, ipmc_version);
            }
            IPMC_CRIT_EXIT();

            continue;
        }

        ipmc_msg_alloc(isid, IPMC_MSG_ID_PROXY_SET_REQ, &buf, FALSE, TRUE);
        msg = (ipmc_msg_proxy_set_req_t *)buf.msg;

        IPMC_CRIT_ENTER();
        switch ( ipmc_version ) {
        case IPMC_IP_VERSION_IGMP:
            msg->ipmc_proxy_enabled = ipmc_global.ipv4_conf.global.ipmc_proxy_enabled;

            break;
        case IPMC_IP_VERSION_MLD:
            msg->ipmc_proxy_enabled = ipmc_global.ipv6_conf.global.ipmc_proxy_enabled;

            break;
        default:
            msg->ipmc_proxy_enabled = IPMC_DEF_PROXY_VALUE;

            break;
        }
        IPMC_CRIT_EXIT();

        msg->msg_id = IPMC_MSG_ID_PROXY_SET_REQ;
        msg->version = ipmc_version;
        ipmc_msg_tx(&buf, isid, sizeof(*msg));
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

/* Set STACK IPMC SSM Range */
static ipmc_prefix_t    prefix_setting;
static mesa_rc ipmc_stacking_set_ssm_range(vtss_isid_t isid_add, ipmc_ip_version_t ipmc_version)
{
    ipmc_msg_buf_t                  buf;
    ipmc_msg_ssm_range_set_req_t    *msg;
    switch_iter_t                   sit;
    vtss_isid_t                     isid;

    if (!msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    T_D("enter, isid: %d", isid_add);

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if ((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) {
            continue;
        }

        if (ipmc_lib_isid_is_local(isid)) {
            IPMC_CRIT_ENTER();
            switch ( ipmc_version ) {
            case IPMC_IP_VERSION_IGMP:
                memcpy(&prefix_setting, &ipmc_global.ipv4_conf.global.ssm_range, sizeof(ipmc_prefix_t));

                break;
            case IPMC_IP_VERSION_MLD:
                memcpy(&prefix_setting, &ipmc_global.ipv6_conf.global.ssm_range, sizeof(ipmc_prefix_t));

                break;
            default:
                memset(&prefix_setting, 0x0, sizeof(ipmc_prefix_t));

                break;
            }

            vtss_ipmc_set_ssm_range(ipmc_version, &prefix_setting);
            IPMC_CRIT_EXIT();

            continue;
        }

        ipmc_msg_alloc(isid, IPMC_MSG_ID_SSM_RANGE_SET_REQ, &buf, FALSE, TRUE);
        msg = (ipmc_msg_ssm_range_set_req_t *)buf.msg;

        IPMC_CRIT_ENTER();
        switch ( ipmc_version ) {
        case IPMC_IP_VERSION_IGMP:
            memcpy(&msg->prefix, &ipmc_global.ipv4_conf.global.ssm_range, sizeof(ipmc_prefix_t));

            break;
        case IPMC_IP_VERSION_MLD:
            memcpy(&msg->prefix, &ipmc_global.ipv6_conf.global.ssm_range, sizeof(ipmc_prefix_t));

            break;
        default:
            memset(&msg->prefix, 0x0, sizeof(ipmc_prefix_t));

            break;
        }
        IPMC_CRIT_EXIT();

        msg->msg_id = IPMC_MSG_ID_SSM_RANGE_SET_REQ;
        msg->version = ipmc_version;
        ipmc_msg_tx(&buf, isid, sizeof(*msg));
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

/* Set STACK IPMC ROUTER PORT */
static ipmc_port_bfs_t  routerport_setting;
static mesa_rc ipmc_stacking_set_router_port(vtss_isid_t isid_add, ipmc_ip_version_t ipmc_version)
{
    ipmc_msg_buf_t                  buf;
    ipmc_msg_router_port_set_req_t  *msg;
    switch_iter_t                   sit;
    vtss_isid_t                     isid;
    port_iter_t                     pit;
    u32                             temp_port_mask;
    mesa_port_no_t                  i;

    if (!msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    T_D("enter, isid: %d", isid_add);

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if ((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) {
            continue;
        }

        if (ipmc_lib_isid_is_local(isid)) {
            IPMC_CRIT_ENTER();
            VTSS_PORT_BF_CLR(routerport_setting.member_ports);
            switch ( ipmc_version ) {
            case IPMC_IP_VERSION_IGMP:
                (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
                while (port_iter_getnext(&pit)) {
                    i = pit.iport;
                    if (ipmc_global.ipv4_conf.ipmc_router_ports[isid - VTSS_ISID_START][i]) {
                        VTSS_PORT_BF_SET(routerport_setting.member_ports, i, TRUE);
                    }
                }

                break;
            case IPMC_IP_VERSION_MLD:
                (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
                while (port_iter_getnext(&pit)) {
                    i = pit.iport;
                    if (ipmc_global.ipv6_conf.ipmc_router_ports[isid - VTSS_ISID_START][i]) {
                        VTSS_PORT_BF_SET(routerport_setting.member_ports, i, TRUE);
                    }
                }

                break;
            default:
                break;
            }

            vtss_ipmc_set_static_router_ports(&routerport_setting, ipmc_version);
            IPMC_CRIT_EXIT();

            continue;
        }

        ipmc_msg_alloc(isid, IPMC_MSG_ID_ROUTER_PORT_SET_REQ, &buf, FALSE, TRUE);
        msg = (ipmc_msg_router_port_set_req_t *)buf.msg;

        IPMC_CRIT_ENTER();
        switch ( ipmc_version ) {
        case IPMC_IP_VERSION_IGMP:
            for (i = 0; i < mesa_port_cnt(0); i++) {
                msg->ipmc_router_ports[i] = ipmc_global.ipv4_conf.ipmc_router_ports[isid - VTSS_ISID_START][i];
            }

            break;
        case IPMC_IP_VERSION_MLD:
            for (i = 0; i < mesa_port_cnt(0); i++) {
                msg->ipmc_router_ports[i] = ipmc_global.ipv6_conf.ipmc_router_ports[isid - VTSS_ISID_START][i];
            }

            break;
        default:
            memset(msg->ipmc_router_ports, 0x0, sizeof(msg->ipmc_router_ports));

            break;
        }
        IPMC_CRIT_EXIT();

        temp_port_mask = 0;
        (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        while (port_iter_getnext(&pit)) {
            i = pit.iport;
            if (msg->ipmc_router_ports[i]) {
                temp_port_mask |= (1 << i);
            }
        }

        msg->msg_id = IPMC_MSG_ID_ROUTER_PORT_SET_REQ;
        msg->isid = isid;
        msg->version = ipmc_version;
        ipmc_msg_tx(&buf, isid, sizeof(*msg));

        T_D("(TX) (ISID = %d)temp_port_mask = 0x%x", isid_add, temp_port_mask);
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

/* Set STACK IPMC FAST LEAVE PORT */
static ipmc_port_bfs_t fastleave_setting;
static mesa_rc ipmc_stacking_set_fastleave_port(vtss_isid_t isid_add, ipmc_ip_version_t ipmc_version)
{
    ipmc_msg_buf_t                      buf;
    ipmc_msg_fast_leave_port_set_req_t  *msg;
    switch_iter_t                       sit;
    vtss_isid_t                         isid;
    port_iter_t                         pit;
    ulong                               temp_port_mask;
    mesa_port_no_t                      i;

    if (!msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    T_D("enter, isid: %d", isid_add);

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if ((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) {
            continue;
        }

        if (ipmc_lib_isid_is_local(isid)) {
            IPMC_CRIT_ENTER();
            VTSS_PORT_BF_CLR(fastleave_setting.member_ports);
            switch ( ipmc_version ) {
            case IPMC_IP_VERSION_IGMP:
                (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
                while (port_iter_getnext(&pit)) {
                    i = pit.iport;
                    if (ipmc_global.ipv4_conf.ipmc_fast_leave_ports[isid - VTSS_ISID_START][i]) {
                        VTSS_PORT_BF_SET(fastleave_setting.member_ports, i, TRUE);
                    }
                }

                break;
            case IPMC_IP_VERSION_MLD:
                (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
                while (port_iter_getnext(&pit)) {
                    i = pit.iport;
                    if (ipmc_global.ipv6_conf.ipmc_fast_leave_ports[isid - VTSS_ISID_START][i]) {
                        VTSS_PORT_BF_SET(fastleave_setting.member_ports, i, TRUE);
                    }
                }

                break;
            default:
                break;
            }

            vtss_ipmc_set_static_fast_leave_ports(&fastleave_setting, ipmc_version);
            IPMC_CRIT_EXIT();

            continue;
        }

        ipmc_msg_alloc(isid, IPMC_MSG_ID_FAST_LEAVE_PORT_SET_REQ, &buf, FALSE, TRUE);
        msg = (ipmc_msg_fast_leave_port_set_req_t *)buf.msg;

        IPMC_CRIT_ENTER();
        switch ( ipmc_version ) {
        case IPMC_IP_VERSION_IGMP:
            for (i = 0; i < mesa_port_cnt(0); i++) {
                msg->ipmc_fast_leave_ports[i] = ipmc_global.ipv4_conf.ipmc_fast_leave_ports[isid - VTSS_ISID_START][i];
            }

            break;
        case IPMC_IP_VERSION_MLD:
            for (i = 0; i < mesa_port_cnt(0); i++) {
                msg->ipmc_fast_leave_ports[i] = ipmc_global.ipv6_conf.ipmc_fast_leave_ports[isid - VTSS_ISID_START][i];
            }


            break;
        default:
            memset(msg->ipmc_fast_leave_ports, 0x0, sizeof(msg->ipmc_fast_leave_ports));

            break;
        }
        IPMC_CRIT_EXIT();

        temp_port_mask = 0;
        (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        while (port_iter_getnext(&pit)) {
            i = pit.iport;
            if (msg->ipmc_fast_leave_ports[i]) {
                temp_port_mask |= (1 << i);
            }
        }

        msg->msg_id = IPMC_MSG_ID_FAST_LEAVE_PORT_SET_REQ;
        msg->isid = isid;
        msg->version = ipmc_version;
        ipmc_msg_tx(&buf, isid, sizeof(*msg));

        T_D("(TX) (ISID = %d)temp_port_mask = 0x" VPRIlx, isid_add, temp_port_mask);
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

/* Set STACK IPMC Throttling Max NO. */
static ipmc_port_throttling_t   throttling_setting;
static mesa_rc ipmc_stacking_set_throttling_number(vtss_isid_t isid_add, ipmc_ip_version_t ipmc_version)
{
    ipmc_msg_buf_t                          buf;
    ipmc_msg_throllting_max_no_set_req_t    *msg;
    switch_iter_t                           sit;
    vtss_isid_t                             isid;
    mesa_port_no_t                          i;

    if (!msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    T_D("enter, isid: %d", isid_add);

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if ((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) {
            continue;
        }

        if (ipmc_lib_isid_is_local(isid)) {
            port_iter_t pit;

            IPMC_CRIT_ENTER();
            vtss_clear(throttling_setting);
            switch ( ipmc_version ) {
            case IPMC_IP_VERSION_IGMP:
                (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
                while (port_iter_getnext(&pit)) {
                    i = pit.iport;
                    throttling_setting.max_no[i] = ipmc_global.ipv4_conf.ipmc_throttling_max_no[isid - VTSS_ISID_START][i];
                }

                break;
            case IPMC_IP_VERSION_MLD:
                (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
                while (port_iter_getnext(&pit)) {
                    i = pit.iport;
                    throttling_setting.max_no[i] = ipmc_global.ipv6_conf.ipmc_throttling_max_no[isid - VTSS_ISID_START][i];
                }

                break;
            default:
                break;
            }

            vtss_ipmc_set_static_port_throttling_max_no(&throttling_setting, ipmc_version);
            IPMC_CRIT_EXIT();

            continue;
        }

        ipmc_msg_alloc(isid, IPMC_MSG_ID_THROLLTING_MAX_NO_SET_REQ, &buf, FALSE, TRUE);
        msg = (ipmc_msg_throllting_max_no_set_req_t *)buf.msg;

        IPMC_CRIT_ENTER();
        switch ( ipmc_version ) {
        case IPMC_IP_VERSION_IGMP:
            for (i = 0; i < mesa_port_cnt(0); i++) {
                msg->ipmc_throttling_max_no[i] = ipmc_global.ipv4_conf.ipmc_throttling_max_no[isid - VTSS_ISID_START][i];
            }

            break;
        case IPMC_IP_VERSION_MLD:
            for (i = 0; i < mesa_port_cnt(0); i++) {
                msg->ipmc_throttling_max_no[i] = ipmc_global.ipv6_conf.ipmc_throttling_max_no[isid - VTSS_ISID_START][i];
            }

            break;
        default:
            memset(msg->ipmc_throttling_max_no, 0x0, sizeof(msg->ipmc_throttling_max_no));

            break;
        }
        IPMC_CRIT_EXIT();

        msg->msg_id = IPMC_MSG_ID_THROLLTING_MAX_NO_SET_REQ;
        msg->isid = isid;
        msg->version = ipmc_version;
        ipmc_msg_tx(&buf, isid, sizeof(*msg));
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

/* Set STACK IPMC port group filtering */
static ipmc_port_group_filtering_t  grpfilter_setting;
static mesa_rc ipmc_stacking_set_grp_filtering(vtss_isid_t isid_add, ipmc_ip_version_t ipmc_version)
{
    int                                     i;
    ipmc_msg_buf_t                          buf;
    ipmc_msg_port_group_filtering_set_req_t *msg;
    switch_iter_t                           sit;
    vtss_isid_t                             isid;
    port_iter_t                             pit;
    ipmc_configuration_t                    *conf;

    if (!msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    T_D("enter, isid: %d", isid_add);

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if ((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) {
            continue;
        }

        if (ipmc_lib_isid_is_local(isid)) {
            IPMC_CRIT_ENTER();
            vtss_clear(grpfilter_setting);
            if (ipmc_version == IPMC_IP_VERSION_IGMP) {
                conf = &ipmc_global.ipv4_conf;
            } else {
                conf = &ipmc_global.ipv6_conf;
            }

            (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
            while (port_iter_getnext(&pit)) {
                i = pit.iport;
                grpfilter_setting.profile_index[i] = conf->ipmc_port_group_filtering[isid - VTSS_ISID_START][i];
            }

            vtss_ipmc_set_static_port_group_filtering(&grpfilter_setting, ipmc_version);
            IPMC_CRIT_EXIT();

            continue;
        }

        ipmc_msg_alloc(isid, IPMC_MSG_ID_PORT_GROUP_FILTERING_SET_REQ, &buf, FALSE, TRUE);
        msg = (ipmc_msg_port_group_filtering_set_req_t *)buf.msg;

        IPMC_CRIT_ENTER();
        if (ipmc_version == IPMC_IP_VERSION_IGMP) {
            conf = &ipmc_global.ipv4_conf;
        } else {
            conf = &ipmc_global.ipv6_conf;
        }

        (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        while (port_iter_getnext(&pit)) {
            i = pit.iport;
            msg->ipmc_port_group_filtering[i] = conf->ipmc_port_group_filtering[isid - VTSS_ISID_START][i];
        }
        IPMC_CRIT_EXIT();

        msg->msg_id = IPMC_MSG_ID_PORT_GROUP_FILTERING_SET_REQ;
        msg->isid = isid;
        msg->version = ipmc_version;
        ipmc_msg_tx(&buf, isid, sizeof(*msg));
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

/* Set STACK IPMC UNREG FLOOD */
static BOOL flood_setting;
static mesa_rc ipmc_stacking_set_unreg_flood(vtss_isid_t isid_add, ipmc_ip_version_t ipmc_version)
{
    ipmc_msg_buf_t                  buf;
    ipmc_msg_unreg_flood_set_req_t  *msg;
    switch_iter_t                   sit;
    vtss_isid_t                     isid;

    if (!msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    T_D("enter, isid: %d", isid_add);

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if ((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) {
            continue;
        }

        if (ipmc_lib_isid_is_local(isid)) {
            IPMC_CRIT_ENTER();
            switch ( ipmc_version ) {
            case IPMC_IP_VERSION_IGMP:
                flood_setting = ipmc_global.ipv4_conf.global.ipmc_unreg_flood_enabled;

                break;
            case IPMC_IP_VERSION_MLD:
                flood_setting = ipmc_global.ipv6_conf.global.ipmc_unreg_flood_enabled;

                break;
            default:
                flood_setting = IPMC_DEF_UNREG_FLOOD_VALUE;

                break;
            }

            vtss_ipmc_set_unreg_flood(flood_setting, ipmc_version);
            IPMC_CRIT_EXIT();

            continue;
        }

        ipmc_msg_alloc(isid, IPMC_MSG_ID_UNREG_FLOOD_SET_REQ, &buf, FALSE, TRUE);
        msg = (ipmc_msg_unreg_flood_set_req_t *)buf.msg;

        IPMC_CRIT_ENTER();
        switch ( ipmc_version ) {
        case IPMC_IP_VERSION_IGMP:
            msg->ipmc_unreg_flood_enabled = ipmc_global.ipv4_conf.global.ipmc_unreg_flood_enabled;

            break;
        case IPMC_IP_VERSION_MLD:
            msg->ipmc_unreg_flood_enabled = ipmc_global.ipv6_conf.global.ipmc_unreg_flood_enabled;

            break;
        default:
            msg->ipmc_unreg_flood_enabled = IPMC_DEF_UNREG_FLOOD_VALUE;

            break;
        }
        IPMC_CRIT_EXIT();

        msg->msg_id = IPMC_MSG_ID_UNREG_FLOOD_SET_REQ;
        msg->version = ipmc_version;
        ipmc_msg_tx(&buf, isid, sizeof(*msg));
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

/* Send request and wait for response */
static vtss_flag_value_t
ipmc_stacking_send_req(
    const vtss_isid_t       isid,
    const ipmc_ip_version_t ipmc_version,
    const mesa_vid_t        vid,
    const mesa_ipv6_t       *const group_addr,
    const ipmc_msg_id_t     msg_id,
    const BOOL              type,
    const mesa_ipv6_t       *const srclist
)
{
    vtss_flag_t       *flags;
    ipmc_msg_buf_t    buf;
    vtss_flag_value_t flag, retVal;
    vtss_tick_count_t time_wait;

    T_D("enter(isid: %d)", isid);

    retVal = 0;
    flag = (1 << isid);
    ipmc_msg_alloc(isid, msg_id, &buf, TRUE, TRUE);

    if (msg_id == IPMC_MSG_ID_DYNAMIC_ROUTER_PORTS_GET_REQ) {
        ipmc_msg_dyn_rtpt_get_req_t *msg_rtpt;

        msg_rtpt = (ipmc_msg_dyn_rtpt_get_req_t *)buf.msg;
        msg_rtpt->msg_id = msg_id;
        msg_rtpt->version = ipmc_version;

        IPMC_GET_CRIT_ENTER();
        flags = &ipmc_global.dynamic_router_ports_getting_flags;
        vtss_flag_maskbits(flags, ~flag);
        IPMC_GET_CRIT_EXIT();

        ipmc_msg_tx(&buf, isid, sizeof(*msg_rtpt));
    } else if (msg_id == IPMC_MSG_ID_GROUP_ENTRY_GET_REQ ||
               msg_id == IPMC_MSG_ID_GROUP_ENTRY_GETNEXT_REQ) {
        ipmc_msg_group_entry_itr_req_t  *msg_grp;

        msg_grp = (ipmc_msg_group_entry_itr_req_t *)buf.msg;
        msg_grp->msg_id = msg_id;
        msg_grp->version = ipmc_version;
        msg_grp->vid = vid;
        memcpy(&msg_grp->group_addr, group_addr, sizeof(mesa_ipv6_t));

        IPMC_GET_CRIT_ENTER();
        if (msg_id == IPMC_MSG_ID_GROUP_ENTRY_GETNEXT_REQ) {
            flags = &ipmc_global.grp_nxt_entry_flags;
        } else {
            flags = &ipmc_global.grp_entry_flags;
        }
        vtss_flag_maskbits(flags, ~flag);
        IPMC_GET_CRIT_EXIT();

        ipmc_msg_tx(&buf, isid, sizeof(*msg_grp));
    } else {
        ipmc_msg_vlan_entry_get_req_t   *msg_vlan;

        msg_vlan = (ipmc_msg_vlan_entry_get_req_t *)buf.msg;
        msg_vlan->msg_id = msg_id;
        msg_vlan->version = ipmc_version;

        /* Default: IPMC_MSG_ID_VLAN_ENTRY_GET_REQ */
        msg_vlan->vid = vid;
        if (msg_id == IPMC_MSG_ID_VLAN_GROUP_ENTRY_WALK_REQ) {
            if (group_addr) {
                memcpy(&msg_vlan->group_addr, group_addr, sizeof(mesa_ipv6_t));
            }
        }
        if (msg_id == IPMC_MSG_ID_GROUP_SRCLIST_WALK_REQ) {
            if (group_addr) {
                memcpy(&msg_vlan->group_addr, group_addr, sizeof(mesa_ipv6_t));
            }
            if (srclist) {
                memcpy(&msg_vlan->srclist_addr, srclist, sizeof(mesa_ipv6_t));
            }
            msg_vlan->srclist_type = type;
        }

        IPMC_GET_CRIT_ENTER();
        flags = &ipmc_global.vlan_entry_flags;
        vtss_flag_maskbits(flags, ~flag);
        IPMC_GET_CRIT_EXIT();

        ipmc_msg_tx(&buf, isid, sizeof(*msg_vlan));
    }

    time_wait = vtss_current_time() + VTSS_OS_MSEC2TICK(IPMC_REQ_TIMEOUT);
    IPMC_GET_CRIT_ENTER();
    if (!(vtss_flag_timed_wait(flags, flag, VTSS_FLAG_WAITMODE_OR, time_wait) & flag)) {
        retVal = 1;
    }
    IPMC_GET_CRIT_EXIT();

    return retVal;
}

static mesa_rc ipmc_stacking_set_intf_entry(vtss_isid_t isid_add, ipmc_prot_intf_entry_param_t *intf_param, ipmc_ip_version_t ipmc_version)
{
    ipmc_msg_buf_t              buf;
    ipmc_msg_vlan_set_req_t     *msg;
    switch_iter_t               sit;
    vtss_isid_t                 isid;

    if (!msg_switch_is_master() || !intf_param) {
        return VTSS_RC_ERROR;
    }

    T_D("enter, isid: %d", isid_add);

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if ((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) {
            continue;
        }

        if (ipmc_lib_isid_is_local(isid)) {
            ipmc_prot_intf_basic_t  vlan_entry;

            memset(&vlan_entry, 0x0, sizeof(ipmc_prot_intf_basic_t));
            vlan_entry.valid = TRUE;
            vlan_entry.vid = intf_param->vid;
            vlan_entry.compatibility = intf_param->cfg_compatibility;
            vlan_entry.priority = intf_param->priority;
            vlan_entry.querier4_address = intf_param->querier.QuerierAdrs4;
            vlan_entry.robustness_variable = intf_param->querier.RobustVari;
            vlan_entry.query_interval = intf_param->querier.QueryIntvl;
            vlan_entry.query_response_interval = intf_param->querier.MaxResTime;
            vlan_entry.last_listener_query_interval = intf_param->querier.LastQryItv;
            vlan_entry.unsolicited_report_interval = intf_param->querier.UnsolicitR;

            IPMC_CRIT_ENTER();
            if (!vtss_ipmc_upd_intf_entry(&vlan_entry, ipmc_version)) {
                T_D("IPMC_MSG_ID_VLAN_ENTRY_SET_REQ Failed!");
            }
            IPMC_CRIT_EXIT();
        } else {
            ipmc_msg_alloc(isid, IPMC_MSG_ID_VLAN_ENTRY_SET_REQ, &buf, FALSE, TRUE);
            msg = (ipmc_msg_vlan_set_req_t *)buf.msg;

            msg->msg_id = IPMC_MSG_ID_VLAN_ENTRY_SET_REQ;
            msg->version = ipmc_version;
            msg->vlan_entry.valid = TRUE;
            msg->vlan_entry.vid = intf_param->vid;
            msg->vlan_entry.compatibility = intf_param->cfg_compatibility;
            msg->vlan_entry.priority = intf_param->priority;
            msg->vlan_entry.querier4_address = intf_param->querier.QuerierAdrs4;
            msg->vlan_entry.robustness_variable = intf_param->querier.RobustVari;
            msg->vlan_entry.query_interval = intf_param->querier.QueryIntvl;
            msg->vlan_entry.query_response_interval = intf_param->querier.MaxResTime;
            msg->vlan_entry.last_listener_query_interval = intf_param->querier.LastQryItv;
            msg->vlan_entry.unsolicited_report_interval = intf_param->querier.UnsolicitR;
            ipmc_msg_tx(&buf, isid, sizeof(*msg));
        }
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

static ipmc_conf_intf_entry_t   cur_intf_g_s;
static mesa_rc ipmc_stacking_get_intf_entry(vtss_isid_t isid_add, ushort vid, ipmc_prot_intf_entry_param_t *intf_param, ipmc_ip_version_t ipmc_version)
{
    mesa_ipv6_t       zero_addr;
    vtss_tick_count_t exe_time_base;

    if (!msg_switch_is_master() || !intf_param) {
        return VTSS_RC_ERROR;
    }
    exe_time_base = vtss_current_time();

    T_D("enter, isid: %d, vid_no:%d/ver:%d", isid_add, vid, ipmc_version);

    if ((isid_add == VTSS_ISID_GLOBAL) ||
        (isid_add == VTSS_ISID_LOCAL) ||
        ipmc_lib_isid_is_local(isid_add)) {
        ipmc_intf_entry_t   *entry;
        u16                 intf_idx;

        IPMC_GET_CRIT_ENTER();
        if ((entry = vtss_ipmc_get_intf_entry(vid, ipmc_version)) != NULL) {
            memcpy(intf_param, &entry->param, sizeof(ipmc_prot_intf_entry_param_t));
        } else {
            if ((intf_idx = ipmc_conf_intf_entry_get(vid, ipmc_version)) != IPMC_VID_INIT) {
                switch ( ipmc_version ) {
                case IPMC_IP_VERSION_IGMP:
                    memcpy(&cur_intf_g_s, &ipmc_global.ipv4_conf.ipmc_conf_intf_entries[intf_idx], sizeof(ipmc_conf_intf_entry_t));

                    break;
                case IPMC_IP_VERSION_MLD:
                    memcpy(&cur_intf_g_s, &ipmc_global.ipv6_conf.ipmc_conf_intf_entries[intf_idx], sizeof(ipmc_conf_intf_entry_t));

                    break;
                default:
                    IPMC_GET_CRIT_EXIT();
                    return VTSS_RC_ERROR;
                }

                memset(intf_param, 0x0, sizeof(ipmc_prot_intf_entry_param_t));
                _ipmc_trans_cfg_intf_info(intf_param, &cur_intf_g_s);
            }
        }
        IPMC_GET_CRIT_EXIT();

        T_D("Done-Local(isid=%d)", isid_add);
    } else {
        if (!IPMC_LIB_ISID_EXIST(isid_add)) {
            return VTSS_RC_ERROR;
        }

        IPMC_GET_CRIT_ENTER();
        ipmc_lib_get_all_zero_ipv6_addr(&zero_addr);
        memset(&ipmc_global.interface_entry, 0x0, sizeof(ipmc_prot_intf_entry_param_t));
        IPMC_GET_CRIT_EXIT();

        if (ipmc_stacking_send_req(isid_add, ipmc_version,
                                   vid, &zero_addr,
                                   IPMC_MSG_ID_VLAN_ENTRY_GET_REQ,
                                   FALSE, NULL)) {
            T_D("timeout, IPMC_MSG_ID_VLAN_ENTRY_GET_REQ(isid=%d)", isid_add);
            return VTSS_RC_ERROR;
        }

        IPMC_GET_CRIT_ENTER();
        memcpy(intf_param, &ipmc_global.interface_entry, sizeof(ipmc_prot_intf_entry_param_t));
        IPMC_GET_CRIT_EXIT();

        T_D("Done-Remote(isid=%d)", isid_add);
    }

    T_D("exit, isid: %d, vid_no: %d (%u-Ticks)", isid_add, vid, (u32)(vtss_current_time() - exe_time_base));
    return VTSS_OK;
}

static mesa_rc
ipmc_stacking_get_next_group_srclist_entry(
    const vtss_isid_t         isid_add,
    const ipmc_ip_version_t   ipmc_version,
    const mesa_vid_t          vid,
    const mesa_ipv6_t         *const addr,
    ipmc_prot_group_srclist_t *const srclist
)
{
    char              buf[40];
    vtss_tick_count_t exe_time_base;

    if (!msg_switch_is_master() || !addr || !srclist) {
        return VTSS_RC_ERROR;
    }
    exe_time_base = vtss_current_time();

    memset(buf, 0x0, sizeof(buf));
    T_D("enter->isid:%d, VID:%d/VER:%d, SRC=%s[%s]",
        isid_add,
        vid,
        ipmc_version,
        srclist->type ? "ALLOW" : "BLOCK",
        misc_ipv6_txt(&srclist->srclist.src_ip, buf));

    if ((isid_add == VTSS_ISID_GLOBAL) ||
        (isid_add == VTSS_ISID_LOCAL) ||
        ipmc_lib_isid_is_local(isid_add)) {
        ipmc_group_entry_t  grp;
        BOOL                grp_found;

        grp.ipmc_version = ipmc_version;
        grp.vid = vid;
        memcpy(&grp.group_addr, addr, sizeof(mesa_ipv6_t));

        grp_found = FALSE;
        IPMC_GET_CRIT_ENTER();
        if (vtss_ipmc_get_intf_group_entry(vid, &grp, ipmc_version)) {
            grp_found = TRUE;
        }
        IPMC_GET_CRIT_EXIT();

        srclist->valid = FALSE;
        if (grp_found) {
            ipmc_db_ctrl_hdr_t  *slist;

            if (srclist->type) {
                slist = grp.info->db.ipmc_sf_do_forward_srclist;
            } else {
                slist = grp.info->db.ipmc_sf_do_not_forward_srclist;
            }

            if (slist &&
                ((srclist->cntr = IPMC_LIB_DB_GET_COUNT(slist)) != 0)) {
                ipmc_sfm_srclist_t  entry;

                memcpy(&entry.src_ip, &srclist->srclist.src_ip, sizeof(mesa_ipv6_t));
                if (ipmc_lib_srclist_buf_get_next(slist, &entry)) {
                    /* srclist->type remains unchanged */
                    memcpy(&srclist->srclist, &entry, sizeof(ipmc_sfm_srclist_t));
                    srclist->valid = TRUE;
                }
            }
        }
    } else {
        if (!IPMC_LIB_ISID_EXIST(isid_add)) {
            return VTSS_RC_ERROR;
        }

        IPMC_GET_CRIT_ENTER();
        memset(&ipmc_global.group_srclist_entry, 0x0, sizeof(ipmc_prot_group_srclist_t));
        IPMC_GET_CRIT_EXIT();

        if (ipmc_stacking_send_req(isid_add, ipmc_version,
                                   vid, addr,
                                   IPMC_MSG_ID_GROUP_SRCLIST_WALK_REQ,
                                   srclist->type, &srclist->srclist.src_ip)) {
            T_E("timeout, IPMC_MSG_ID_GROUP_SRCLIST_WALK_REQ(isid=%d)", isid_add);
            return VTSS_RC_ERROR;
        }

        IPMC_GET_CRIT_ENTER();
        memcpy(srclist, &ipmc_global.group_srclist_entry, sizeof(ipmc_prot_group_srclist_t));
        IPMC_GET_CRIT_EXIT();
    }

    T_D("exit->isid:%d, (%u-Ticks-GOT-%s)%s-vid:%d/source_address = %s",
        isid_add,
        (u32)(vtss_current_time() - exe_time_base),
        srclist->valid ? "VALID" : "INVALID",
        srclist->type ? "ALLOW" : "BLOCK",
        vid,
        misc_ipv6_txt(&srclist->srclist.src_ip, buf));
    return VTSS_OK;
}

static mesa_rc ipmc_stacking_get_next_intf_group_entry(vtss_isid_t isid_add, mesa_vid_t vid, ipmc_prot_intf_group_entry_t *intf_group, ipmc_ip_version_t ipmc_version)
{
    char              buf[40];
    vtss_tick_count_t exe_time_base;

    if (!msg_switch_is_master() || !intf_group) {
        return VTSS_RC_ERROR;
    }
    exe_time_base = vtss_current_time();

    memset(buf, 0x0, sizeof(buf));
    T_D("enter->isid: %d, vid_no:%d/ver:%d, group_address = %s",
        isid_add,
        vid,
        ipmc_version,
        misc_ipv6_txt(&intf_group->group_addr, buf));

    if ((isid_add == VTSS_ISID_GLOBAL) ||
        (isid_add == VTSS_ISID_LOCAL) ||
        ipmc_lib_isid_is_local(isid_add)) {
        ipmc_group_entry_t  grp;

        intf_group->valid = FALSE;
        grp.ipmc_version = ipmc_version;
        grp.vid = vid;
        memcpy(&grp.group_addr, &intf_group->group_addr, sizeof(mesa_ipv6_t));
        IPMC_GET_CRIT_ENTER();
        if (vtss_ipmc_get_next_intf_group_entry(vid, &grp, ipmc_version)) {
            intf_group->valid = TRUE;
        }
        IPMC_GET_CRIT_EXIT();

        if (intf_group->valid) {
            intf_group->ipmc_version = grp.ipmc_version;
            intf_group->vid = grp.vid;
            memcpy(&intf_group->group_addr, &grp.group_addr, sizeof(mesa_ipv6_t));
            memcpy(&intf_group->db, &grp.info->db, sizeof(ipmc_group_db_t));

            if (!msg_switch_is_master()) {
                intf_group->db.ipmc_sf_do_forward_srclist = NULL;
                intf_group->db.ipmc_sf_do_not_forward_srclist = NULL;
            }
        } else {
            T_D("no more group entry in vlan-%d", vid);
        }
    } else {
        if (!IPMC_LIB_ISID_EXIST(isid_add)) {
            return VTSS_RC_ERROR;
        }

        IPMC_GET_CRIT_ENTER();
        memset(&ipmc_global.intf_group_entry, 0x0, sizeof(ipmc_prot_intf_group_entry_t));
        IPMC_GET_CRIT_EXIT();

        if (ipmc_stacking_send_req(isid_add, ipmc_version,
                                   vid, &intf_group->group_addr,
                                   IPMC_MSG_ID_VLAN_GROUP_ENTRY_WALK_REQ,
                                   FALSE, NULL)) {
            T_D("timeout, IPMC_MSG_ID_VLAN_GROUP_ENTRY_WALK_REQ(isid=%d)", isid_add);
            return VTSS_RC_ERROR;
        }

        IPMC_GET_CRIT_ENTER();
        memcpy(intf_group, &ipmc_global.intf_group_entry, sizeof(ipmc_prot_intf_group_entry_t));
        IPMC_GET_CRIT_EXIT();
    }

    T_D("exit->isid: %d, (%u-Ticks-GOT)%s-vid:%d/group_address = %s",
        isid_add,
        (u32)(vtss_current_time() - exe_time_base),
        intf_group->valid ? "VALID" : "INVALID",
        vid,
        misc_ipv6_txt(&intf_group->group_addr, buf));
    return VTSS_OK;
}

#ifdef VTSS_SW_OPTION_SMB_IPMC
static BOOL ipmc_util_ipv6_address_zero(const mesa_ipv6_t *const adrx)
{
    mesa_ipv6_t adrs;

    if (!adrx) {
        return FALSE;
    }

    IPMC_LIB_ADRS_CPY(&adrs, adrx);
    return ipmc_lib_isaddr6_all_zero(&adrs);
}
#endif /* VTSS_SW_OPTION_SMB_IPMC */

static mesa_rc
ipmc_stacking_group_entry_get(
    const vtss_isid_t            sidx,
    const ipmc_ip_version_t      verx,
    const mesa_vid_t             *const vidx,
    const mesa_ipv6_t            *const grpx,
    ipmc_prot_intf_group_entry_t *const entry)
{
    char              buf[40];
    vtss_tick_count_t exe_time_base;
    ipmc_ip_version_t version;
    mesa_rc           rc = VTSS_RC_ERROR;

    if (!msg_switch_is_master() ||
        !vidx || !grpx || !entry) {
        T_D("Invalid Input!");
        return rc;
    }
    exe_time_base = vtss_current_time();

    memset(buf, 0x0, sizeof(buf));
    T_D("enter->isid:%d/ver:%d/vid:%u/grp:%s",
        sidx,
        verx,
        *vidx,
        misc_ipv6_txt(grpx, buf));

    version = verx;
    if ((sidx == VTSS_ISID_GLOBAL) ||
        (sidx == VTSS_ISID_LOCAL) ||
        ipmc_lib_isid_is_local(sidx)) {
        ipmc_group_entry_t  grp;

        entry->valid = FALSE;
        grp.ipmc_version = version;
        grp.vid = *vidx;
        IPMC_LIB_ADRS_CPY(&grp.group_addr, grpx);
        IPMC_GET_CRIT_ENTER();
        if (vtss_ipmc_group_entry_get(&version, vidx, grpx, &grp) == VTSS_RC_OK) {
            entry->valid = TRUE;
        }
        IPMC_GET_CRIT_EXIT();

        if (entry->valid) {
            entry->ipmc_version = grp.ipmc_version;
            entry->vid = grp.vid;
            IPMC_LIB_ADRS_CPY(&entry->group_addr, &grp.group_addr);
            memcpy(&entry->db, &grp.info->db, sizeof(ipmc_group_db_t));

            if (!msg_switch_is_master()) {
                entry->db.ipmc_sf_do_forward_srclist = NULL;
                entry->db.ipmc_sf_do_not_forward_srclist = NULL;
            }

            rc = VTSS_RC_OK;
        }
    } else {
        if (IPMC_LIB_ISID_EXIST(sidx)) {
            IPMC_GET_CRIT_ENTER();
            memset(&ipmc_global.group_entry, 0x0, sizeof(ipmc_prot_intf_group_entry_t));
            IPMC_GET_CRIT_EXIT();

            if (ipmc_stacking_send_req(sidx, version, *vidx, grpx,
                                       IPMC_MSG_ID_GROUP_ENTRY_GET_REQ,
                                       FALSE, NULL)) {
                T_D("timeout, IPMC_MSG_ID_GROUP_ENTRY_GET_REQ(isid=%d)", sidx);
            } else {
                IPMC_GET_CRIT_ENTER();
                memcpy(entry, &ipmc_global.group_entry, sizeof(ipmc_prot_intf_group_entry_t));
                IPMC_GET_CRIT_EXIT();

                rc = entry->valid ? VTSS_RC_OK : VTSS_RC_ERROR;
            }
        }
    }

    memset(buf, 0x0, sizeof(buf));
    T_D("exit(%s:%u-Ticks-GOT)->isid:%d/vid:%u/grp:%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        (u32)(vtss_current_time() - exe_time_base),
        sidx,
        entry->vid,
        misc_ipv6_txt(&entry->group_addr, buf));
    return rc;
}

static mesa_rc
ipmc_stacking_group_entry_get_next(
    const vtss_isid_t            sidx,
    const ipmc_ip_version_t      verx,
    const mesa_vid_t             *const vidx,
    const mesa_ipv6_t            *const grpx,
    ipmc_prot_intf_group_entry_t *const entry)
{
    char              buf[40];
    vtss_tick_count_t exe_time_base;
    ipmc_ip_version_t version;
    mesa_rc           rc = VTSS_RC_ERROR;

    if (!msg_switch_is_master() ||
        !vidx || !grpx || !entry) {
        T_D("Invalid Input!");
        return rc;
    }
    exe_time_base = vtss_current_time();

    memset(buf, 0x0, sizeof(buf));
    T_D("enter->isid:%d/ver:%d/vid:%u/grp:%s",
        sidx,
        verx,
        *vidx,
        misc_ipv6_txt(grpx, buf));

    version = verx;
    if ((sidx == VTSS_ISID_GLOBAL) ||
        (sidx == VTSS_ISID_LOCAL) ||
        ipmc_lib_isid_is_local(sidx)) {
        ipmc_group_entry_t  grp;

        entry->valid = FALSE;
        grp.ipmc_version = version;
        grp.vid = *vidx;
        IPMC_LIB_ADRS_CPY(&grp.group_addr, grpx);
        IPMC_GET_CRIT_ENTER();
        if (vtss_ipmc_group_entry_get_next(&version, vidx, grpx, &grp) == VTSS_RC_OK) {
            if (grp.ipmc_version == version) {
                entry->valid = TRUE;
            }
        }
        IPMC_GET_CRIT_EXIT();

        if (entry->valid) {
            entry->ipmc_version = grp.ipmc_version;
            entry->vid = grp.vid;
            IPMC_LIB_ADRS_CPY(&entry->group_addr, &grp.group_addr);
            memcpy(&entry->db, &grp.info->db, sizeof(ipmc_group_db_t));

            if (!msg_switch_is_master()) {
                entry->db.ipmc_sf_do_forward_srclist = NULL;
                entry->db.ipmc_sf_do_not_forward_srclist = NULL;
            }

            rc = VTSS_RC_OK;
        }
    } else {
        if (IPMC_LIB_ISID_EXIST(sidx)) {
            IPMC_GET_CRIT_ENTER();
            memset(&ipmc_global.group_next_entry, 0x0, sizeof(ipmc_prot_intf_group_entry_t));
            IPMC_GET_CRIT_EXIT();

            if (ipmc_stacking_send_req(sidx, version, *vidx, grpx,
                                       IPMC_MSG_ID_GROUP_ENTRY_GETNEXT_REQ,
                                       FALSE, NULL)) {
                T_D("timeout, IPMC_MSG_ID_GROUP_ENTRY_GETNEXT_REQ(isid=%d)", sidx);
            } else {
                IPMC_GET_CRIT_ENTER();
                memcpy(entry, &ipmc_global.group_next_entry, sizeof(ipmc_prot_intf_group_entry_t));
                IPMC_GET_CRIT_EXIT();

                rc = entry->valid ? VTSS_RC_OK : VTSS_RC_ERROR;
            }
        }
    }

    memset(buf, 0x0, sizeof(buf));
    T_D("exit(%s:%u-Ticks-GOT)->isid:%d/vid:%u/grp:%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        (u32)(vtss_current_time() - exe_time_base),
        sidx,
        entry->vid,
        misc_ipv6_txt(&entry->group_addr, buf));
    return rc;
}

static mesa_rc ipmc_stacking_get_dynamic_router_port(vtss_isid_t isid_add, ipmc_dynamic_router_port_t *router_port, ipmc_ip_version_t ipmc_version)
{
    port_iter_t       pit;
    u32               idx;
    BOOL              stacking;
    ipmc_port_bfs_t   router_port_bitmask;
    mesa_ipv6_t       zero_addr;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!msg_switch_is_master() || !router_port) {
        return VTSS_RC_ERROR;
    }

    T_D("enter, isid: %d", isid_add);

    VTSS_PORT_BF_CLR(router_port_bitmask.member_ports);
    if ((isid_add == VTSS_ISID_GLOBAL) ||
        (isid_add == VTSS_ISID_LOCAL) ||
        ipmc_lib_isid_is_local(isid_add)) {
        stacking = FALSE;

        IPMC_GET_CRIT_ENTER();
        (void)vtss_ipmc_get_dynamic_router_port_mask(ipmc_version, &router_port_bitmask);
        IPMC_GET_CRIT_EXIT();
    } else {
        if (!IPMC_LIB_ISID_EXIST(isid_add)) {
            return VTSS_RC_ERROR;
        }

        stacking = TRUE;

        IPMC_GET_CRIT_ENTER();
        ipmc_lib_get_all_zero_ipv6_addr(&zero_addr);
        vtss_clear(ipmc_global.dynamic_router_ports);
        IPMC_GET_CRIT_EXIT();

        if (ipmc_stacking_send_req(isid_add, ipmc_version,
                                   0, &zero_addr,
                                   IPMC_MSG_ID_DYNAMIC_ROUTER_PORTS_GET_REQ,
                                   FALSE, NULL)) {
            T_E("timeout, IPMC_MSG_ID_DYNAMIC_ROUTER_PORTS_GET_REQ(isid=%d)", isid_add);
            return VTSS_RC_ERROR;
        }
    }

    if (msg_switch_is_master()) {
        (void) port_iter_init(&pit, NULL, isid_add, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
    } else {
        (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
    }
    while (port_iter_getnext(&pit)) {
        idx = pit.iport;

        if (stacking) {
            IPMC_GET_CRIT_ENTER();
            router_port->ports[idx] = ipmc_global.dynamic_router_ports[isid_add - 1][idx];
            IPMC_GET_CRIT_EXIT();
        } else {
            if (VTSS_PORT_BF_GET(router_port_bitmask.member_ports, idx)) {
                router_port->ports[idx] = TRUE;
            } else {
                router_port->ports[idx] = FALSE;
            }
        }
    }

    T_D("exit, isid: %d (%u-Ticks)", isid_add, (u32)(vtss_current_time() - exe_time_base));
    return VTSS_OK;
}

/* STACK IPMC clear STAT Counter */
static mesa_rc ipmc_stacking_clear_statistics(vtss_isid_t isid_add,
                                              ipmc_ip_version_t ipmc_version,
                                              mesa_vid_t vid)
{
    ipmc_msg_buf_t                      buf;
    ipmc_msg_stat_counter_clear_req_t   *msg;
    switch_iter_t                       sit;
    vtss_isid_t                         isid;

    if (!msg_switch_is_master()) {
        return VTSS_RC_ERROR;
    }

    T_D("enter, isid: %d", isid_add);

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if ((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) {
            continue;
        }

        ipmc_msg_alloc(isid, IPMC_MSG_ID_STAT_COUNTER_CLEAR_REQ, &buf, FALSE, TRUE);
        msg = (ipmc_msg_stat_counter_clear_req_t *)buf.msg;

        msg->msg_id = IPMC_MSG_ID_STAT_COUNTER_CLEAR_REQ;
        msg->version = ipmc_version;
        msg->vid = vid;
        ipmc_msg_tx(&buf, isid, sizeof(*msg));
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

static mesa_rc ipmc_stacking_sync_mgmt_conf(vtss_isid_t isid_add, ipmc_lib_mgmt_info_t *sys_mgmt)
{
    ipmc_msg_sys_mgmt_set_req_t buf, *msg = &buf;
    switch_iter_t               sit;
    vtss_isid_t                 isid;
    ipmc_mgmt_ipif_t            *ifp;
    u32                         ifx;
    ipmc_lib_mgmt_info_t        info, *ipmc_info = &info;

    if (!msg_switch_is_master() || !sys_mgmt) {
        return VTSS_RC_ERROR;
    }

    T_I("SYNC(isid: %d):MAC[%s]", isid_add, misc_mac2str(sys_mgmt->mac_addr));

    /* Fill out message */
    IPMC_MGMT_MAC_ADR_GET(sys_mgmt, msg->mgmt_mac);
    msg->intf_cnt = 0;
    for (ifx = 0; ifx < VTSS_IPMC_MGMT_IPIF_MAX_CNT; ifx++) {
        if (IPMC_MGMT_IPIF_VALID(sys_mgmt, ifx)) {
            T_I("SYNC(%u) INTF-ADR-%u(VID:%u) is %s",
                (msg->intf_cnt + 1),
                IPMC_MGMT_IPIF_ADRS4(sys_mgmt, ifx),
                IPMC_MGMT_IPIF_IDVLN(sys_mgmt, ifx),
                IPMC_MGMT_IPIF_STATE(sys_mgmt, ifx) ? "Up" : "Down");

            ifp = &msg->ip_addr[msg->intf_cnt++];
            ipmc_mgmt_intf_vidx(ifp) = IPMC_MGMT_IPIF_IDVLN(sys_mgmt, ifx);
            ipmc_mgmt_intf_live(ifp) = IPMC_MGMT_IPIF_VALID(sys_mgmt, ifx);
            ipmc_mgmt_intf_adr4(ifp) = IPMC_MGMT_IPIF_ADRS4(sys_mgmt, ifx);
            ipmc_mgmt_intf_opst(ifp) = IPMC_MGMT_IPIF_STATE(sys_mgmt, ifx);
        }
    }

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if (((isid_add != VTSS_ISID_GLOBAL) && (isid_add != isid)) ||
            ipmc_lib_isid_is_local(isid)) {
            continue;
        }

        /* Receive message */
        IPMC_MGMT_SYSTEM_CHANGE(ipmc_info) = TRUE;
        IPMC_MGMT_MAC_ADR_SET(ipmc_info, msg->mgmt_mac);
        for (ifx = 0; ifx < msg->intf_cnt; ifx++) {
            ifp = &msg->ip_addr[ifx];
            if (ipmc_mgmt_intf_live(ifp)) {
                T_I("RCV(%u) INTF-ADR-%u(VID:%u)/%s",
                    (ifx + 1),
                    ipmc_mgmt_intf_adr4(ifp),
                    ipmc_mgmt_intf_vidx(ifp),
                    ipmc_mgmt_intf_opst(ifp) ? "Up" : "Down");

                IPMC_MGMT_IPIF_IDVLN(ipmc_info, ifx) = ipmc_mgmt_intf_vidx(ifp);
                IPMC_MGMT_IPIF_VALID(ipmc_info, ifx) = ipmc_mgmt_intf_live(ifp);
                IPMC_MGMT_IPIF_ADRS4(ipmc_info, ifx) = ipmc_mgmt_intf_adr4(ifp);
                IPMC_MGMT_IPIF_STATE(ipmc_info, ifx) = ipmc_mgmt_intf_opst(ifp);
            }
        }

        if (!ipmc_lib_system_mgmt_info_set(ipmc_info)) {
            T_D("Failure in ipmc_lib_system_mgmt_info_set()");
        }
    }

    T_D("exit, isid: %d", isid_add);
    return VTSS_OK;
}

static CapArray<BOOL, VTSS_APPL_CAP_ISID_END, MESA_CAP_PORT_CNT> ipmc_port_status;

/* Port state callback function  This function is called if a GLOBAL port state change occur.  */
static void ipmc_global_port_state_change_callback(vtss_isid_t isid, mesa_port_no_t port_no, port_info_t *info)
{
    T_D("enter: Port(%u/Stack:%s)->%s", port_no, info->stack ? "TRUE" : "FALSE", info->link ? "UP" : "DOWN");

    IPMC_CRIT_ENTER();
    if (msg_switch_is_master() && !info->stack) {
        if (info->link) {
            ipmc_port_status[ipmc_lib_isid_convert(TRUE, isid)][port_no] = TRUE;
        } else {
            ipmc_port_status[ipmc_lib_isid_convert(TRUE, isid)][port_no] = FALSE;
        }

        if (ipmc_lib_isid_is_local(isid)) {
            ipmc_port_status[VTSS_ISID_LOCAL][port_no] = ipmc_port_status[ipmc_lib_isid_convert(TRUE, isid)][port_no];
        }
    }
    IPMC_CRIT_EXIT();

    T_D("exit: Port(%u/Stack:%s)->%s", port_no, info->stack ? "TRUE" : "FALSE", info->link ? "UP" : "DOWN");
}

static mesa_rc ipmc_resolve_vlan_member(vtss_isid_t isid_in, vtss_appl_vlan_entry_t *vlan_conf, BOOL next)
{
    switch_iter_t   sit;
    vtss_isid_t     isid;
    mesa_vid_t      vid;

    if (!vlan_conf) {
        return VTSS_RC_ERROR;
    }

    /*
        Only msg_switch_is_master() will come here
        VTSS_ISID_GLOBAL cannot resolve VLAN member!
    */
    if (isid_in == VTSS_ISID_GLOBAL) {
        return VTSS_RC_ERROR;
    }

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;
        if (!IPMC_LIB_ISID_PASS(isid_in, isid)) {
            continue;
        }

        /* Expecting to retrieve all the information in vtss_appl_vlan_entry_t */
        vid = vlan_conf->vid;
#ifdef VTSS_SW_OPTION_MVR
        if (next) {
            vtss_appl_vlan_entry_t chk_mvr;
            BOOL                   found = FALSE;

            if (isid == VTSS_ISID_LOCAL) {
                while (vtss_appl_vlan_get(VTSS_ISID_LOCAL, vid, vlan_conf, TRUE, VTSS_APPL_VLAN_USER_ALL) == VTSS_RC_OK) {
                    vid = vlan_conf->vid;

                    chk_mvr.vid = vid;
                    if (vtss_appl_vlan_get(VTSS_ISID_LOCAL, vid, &chk_mvr, FALSE, VTSS_APPL_VLAN_USER_MVR) != VTSS_RC_OK) {
                        found = TRUE;
                        break;
                    }
                }
            } else {
                while (vtss_appl_vlan_get(isid, vid, vlan_conf, TRUE, VTSS_APPL_VLAN_USER_STATIC) == VTSS_RC_OK) {
                    vid = vlan_conf->vid;

                    chk_mvr.vid = vid;
                    if (vtss_appl_vlan_get(isid, vid, &chk_mvr, FALSE, VTSS_APPL_VLAN_USER_MVR) != VTSS_RC_OK) {
                        found = TRUE;
                        break;
                    }
                }
            }

            if (found) {
                return VTSS_RC_OK;
            } else {
                return VTSS_RC_ERROR;
            }
        } else {
            if (isid == VTSS_ISID_LOCAL) {
                if (vtss_appl_vlan_get(VTSS_ISID_LOCAL, vid, vlan_conf, FALSE, VTSS_APPL_VLAN_USER_MVR) == VTSS_RC_OK) {
                    return VTSS_RC_ERROR;
                }

                return vtss_appl_vlan_get(VTSS_ISID_LOCAL, vid, vlan_conf, FALSE, VTSS_APPL_VLAN_USER_ALL);
            } else {
                if (vtss_appl_vlan_get(isid, vid, vlan_conf, FALSE, VTSS_APPL_VLAN_USER_MVR) == VTSS_RC_OK) {
                    return VTSS_RC_ERROR;
                }

                return vtss_appl_vlan_get(isid, vid, vlan_conf, FALSE, VTSS_APPL_VLAN_USER_STATIC);
            }
        }
#else
        if (isid == VTSS_ISID_LOCAL) {
            return vtss_appl_vlan_get(VTSS_ISID_LOCAL, vid, vlan_conf, next, VTSS_APPL_VLAN_USER_ALL);
        } else {
            return vtss_appl_vlan_get(isid, vid, vlan_conf, next, VTSS_APPL_VLAN_USER_STATIC);
        }
#endif /* VTSS_SW_OPTION_MVR */
    }

    return VTSS_RC_ERROR;
}

static void _ipmc_intf_walk_translate(ipmc_intf_map_t *entry, ipmc_intf_entry_t *intf)
{
    if (!entry || !intf) {
        return;
    }

    entry->ipmc_version = intf->ipmc_version;
    memcpy(&entry->param, &intf->param, sizeof(ipmc_prot_intf_entry_param_t));
    if (entry->ipmc_version == IPMC_IP_VERSION_MLD) {
        if (!ipmc_lib_get_eui64_linklocal_addr(&entry->QuerierAdrs.mld.adrs)) {
            IPMC_LIB_ADRS_SET(&entry->QuerierConf.mld.adrs, 0x0);
        }
    } else {
        if (!ipmc_lib_get_ipintf_igmp_adrs(intf, &entry->QuerierAdrs.igmp.adrs)) {
            entry->QuerierAdrs.igmp.adrs = 0;
        }
    }
    entry->op_state = intf->op_state;
    entry->proxy_report_timeout = intf->proxy_report_timeout;
    memcpy(entry->vlan_ports, intf->vlan_ports, sizeof(entry->vlan_ports));
}

BOOL ipmc_mgmt_intf_op_allow(vtss_isid_t isid)
{
    if (msg_switch_is_master() && ipmc_lib_isid_is_local(isid)) {
        return TRUE;
    } else {
        return FALSE;
    }
}

mesa_rc ipmc_mgmt_intf_walk(BOOL next, ipmc_intf_map_t *entry)
{
    mesa_rc             rc;
    ipmc_intf_entry_t   *intf;

    if (!entry) {
        return VTSS_RC_ERROR;
    }

    rc = VTSS_RC_ERROR;
    IPMC_CRIT_ENTER();
    if (next) {
        if ((intf = vtss_ipmc_get_next_intf_entry(entry->param.vid, entry->ipmc_version)) != NULL) {
            _ipmc_intf_walk_translate(entry, intf);
            rc = VTSS_OK;
        }
    } else {
        if ((intf = vtss_ipmc_get_intf_entry(entry->param.vid, entry->ipmc_version)) != NULL) {
            _ipmc_intf_walk_translate(entry, intf);
            rc = VTSS_OK;
        }
    }
    IPMC_CRIT_EXIT();

    return rc;
}

mesa_rc ipmc_mgmt_set_intf_state_querier(mesa_vid_t vid,
                                         BOOL *state_enable,
                                         BOOL *querier_enable,
                                         ipmc_ip_version_t ipmc_version)
{
    switch_iter_t          sit;
    vtss_isid_t            isid;
    port_iter_t            pit;
    vtss_appl_vlan_entry_t vlan_conf;
    ipmc_port_bfs_t        vlan_ports;
    u16                    intf_idx;
    BOOL                   create_flag, apply_flag;
    ipmc_conf_intf_entry_t cur_intf;
    vtss_tick_count_t      exe_time_base;

    if (!msg_switch_is_master()) {
        return VTSS_OK;
    }
    exe_time_base = vtss_current_time();

    IPMC_CRIT_ENTER();
    intf_idx = ipmc_conf_intf_entry_get(vid, ipmc_version);
    IPMC_CRIT_EXIT();

    create_flag = FALSE;
    if (intf_idx == IPMC_VID_INIT) {
        return VTSS_RC_ERROR;
    }

    if (intf_idx >= IPMC_VLAN_MAX) {
        return VTSS_RC_ERROR;
    }

    IPMC_CRIT_ENTER();
    switch ( ipmc_version ) {
    case IPMC_IP_VERSION_IGMP:
        memcpy(&cur_intf, &ipmc_global.ipv4_conf.ipmc_conf_intf_entries[intf_idx], sizeof(ipmc_conf_intf_entry_t));

        break;
    case IPMC_IP_VERSION_MLD:
        memcpy(&cur_intf, &ipmc_global.ipv6_conf.ipmc_conf_intf_entries[intf_idx], sizeof(ipmc_conf_intf_entry_t));

        break;
    default:
        IPMC_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }
    IPMC_CRIT_EXIT();

    if ((cur_intf.protocol_status == *state_enable) &&
        (cur_intf.querier_status == *querier_enable)) {
        return VTSS_OK;
    }

    VTSS_PORT_BF_CLR(vlan_ports.member_ports);
    if (*state_enable) {
        vlan_conf.vid = vid;
        if (ipmc_resolve_vlan_member(VTSS_ISID_LOCAL, &vlan_conf, FALSE) == VTSS_OK) {
            (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
            while (port_iter_getnext(&pit)) {
                VTSS_PORT_BF_SET(vlan_ports.member_ports, pit.iport, vlan_conf.ports[pit.iport]);
            }
        }
    }

    apply_flag = TRUE;
    cur_intf.valid = TRUE;
    cur_intf.vid = vid;
    cur_intf.protocol_status = *state_enable;
    cur_intf.querier_status = *querier_enable;

    IPMC_CRIT_ENTER();
    if (vtss_ipmc_set_intf_entry(vid, *state_enable, *querier_enable, &vlan_ports, ipmc_version) == NULL) {
        cur_intf.protocol_status = IPMC_DEF_INTF_STATE_VALUE;
        apply_flag = FALSE;
    }

    if (apply_flag) {
        (void) _ipmc_conf_intf_entry_upd(&cur_intf, ipmc_version);
    }
    IPMC_CRIT_EXIT();

    if (!apply_flag) {
        return IPMC_ERROR_TABLE_IS_FULL;
    }

    /* apply this setting to stacking only */
    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
    while (switch_iter_getnext(&sit)) {
        isid = sit.isid;

        if (ipmc_lib_isid_is_local(isid)) {
            continue;
        } else {
            VTSS_PORT_BF_CLR(vlan_ports.member_ports);
            if (*state_enable) {
                vlan_conf.vid = vid;
                if (ipmc_resolve_vlan_member(isid, &vlan_conf, FALSE) == VTSS_OK) {
                    (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
                    while (port_iter_getnext(&pit)) {
                        VTSS_PORT_BF_SET(vlan_ports.member_ports, pit.iport, vlan_conf.ports[pit.iport]);
                    }
                }
            }

            (void) ipmc_stacking_set_intf(isid, IPMC_OP_SET, &cur_intf, &vlan_ports, ipmc_version);
        }
    }

    T_D("VID-%u consumes %u ticks", vid, (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc ipmc_mgmt_get_intf_state_querier(BOOL conf, mesa_vid_t *vid, u8 *state_enable, u8 *querier_enable, BOOL next, ipmc_ip_version_t ipmc_version)
{
    mesa_rc           rc;
    BOOL              found;
    mesa_vid_t        chk_vid, intf_idx;
    vtss_tick_count_t exe_time_base;
    vtss_ifindex_t    ifidx;

    if (!vid || !state_enable || !querier_enable) {
        return VTSS_RC_ERROR;
    }

    found = FALSE;
    chk_vid = *vid;
    intf_idx = IPMC_VID_INIT;
    exe_time_base = vtss_current_time();

    IPMC_CRIT_ENTER();
    if (next) {
        if (conf) {
            if ((intf_idx = ipmc_conf_intf_entry_get_next(&chk_vid, ipmc_version)) != IPMC_VID_INIT) {
                found = TRUE;
            }
        } else {
            ipmc_intf_entry_t   *ipmc_vid_entry = NULL;

            while ((ipmc_vid_entry = vtss_ipmc_get_next_intf_entry(chk_vid, ipmc_version)) != NULL) {
                chk_vid = ipmc_vid_entry->param.vid;
                if (ipmc_vid_entry->op_state) {
                    break;
                }
            }

            if (ipmc_vid_entry != NULL) {
                found = TRUE;
                intf_idx = ipmc_conf_intf_entry_get(chk_vid, ipmc_version);
            }
        }
    } else {
        if (conf) {
            if ((intf_idx = ipmc_conf_intf_entry_get(chk_vid, ipmc_version)) != IPMC_VID_INIT) {
                found = TRUE;
            } else {
                /*
                   The callback function registered on the IP module
                   is not invoked immediately when an IP interface is created.
                   It causes that the IPMC module refuses to enable IGMP/MLD snooping
                   on that interface. It is now only seen when upload configuration
                   and bootup which runs the startup configuration too.
                   The workaround is to check if the IP interace exits with the api
                   vtss_appl_ip_if_conf_get when ipmc_conf_intf_entry_get return
                   not-found.
                 */
                T_D("chk_vid: %d", chk_vid);
                if (vtss_ifindex_from_vlan(chk_vid, &ifidx) == VTSS_OK) {
                    if (vtss_appl_ip_if_conf_get(ifidx) == VTSS_OK) {
                        T_D("vtss_ip_if_exists");
                        if (_ipmc_conf_intf_entry_add((mesa_vid_t)chk_vid, IPMC_IP_VERSION_IGMP) == IPMC_VID_INIT) {
                            T_D("_ipmc_conf_intf_entry_add vid:%d IGMP failed to create VLAN config.", chk_vid);
                        }
                        if (_ipmc_conf_intf_entry_add((mesa_vid_t)chk_vid, IPMC_IP_VERSION_MLD) == IPMC_VID_INIT) {
                            T_D("_ipmc_conf_intf_entry_add vid:%d MLD failed to create VLAN config.", chk_vid);
                        }
                        if ((intf_idx = ipmc_conf_intf_entry_get(chk_vid, ipmc_version)) != IPMC_VID_INIT) {
                            found = TRUE;
                        }
                    }
                }
            }
        } else {
            ipmc_intf_entry_t   *ipmc_vid_entry = vtss_ipmc_get_intf_entry(chk_vid, ipmc_version);

            if ((ipmc_vid_entry != NULL) && ipmc_vid_entry->op_state) {
                found = TRUE;
                intf_idx = ipmc_conf_intf_entry_get(chk_vid, ipmc_version);
            }
        }
    }

    if ((intf_idx >= IPMC_VLAN_MAX) || (intf_idx == IPMC_VID_INIT)) {
        IPMC_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }

    if (!found) {
        IPMC_CRIT_EXIT();
        return VTSS_RC_ERROR;
    } else {
        *vid = chk_vid;
    }

    rc = VTSS_OK;
    switch ( ipmc_version ) {
    case IPMC_IP_VERSION_IGMP:
        *state_enable = ipmc_global.ipv4_conf.ipmc_conf_intf_entries[intf_idx].protocol_status;
        *querier_enable = ipmc_global.ipv4_conf.ipmc_conf_intf_entries[intf_idx].querier_status;

        break;
    case IPMC_IP_VERSION_MLD:
        *state_enable = ipmc_global.ipv6_conf.ipmc_conf_intf_entries[intf_idx].protocol_status;
        *querier_enable = ipmc_global.ipv6_conf.ipmc_conf_intf_entries[intf_idx].querier_status;

        break;
    default:
        rc = VTSS_RC_ERROR;

        break;
    }

    IPMC_CRIT_EXIT();

    T_D("Consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return rc;
}

mesa_rc ipmc_mgmt_set_intf_info(vtss_isid_t isid, ipmc_prot_intf_entry_param_t *intf_param, ipmc_ip_version_t ipmc_version)
{
    mesa_rc                 rc = VTSS_OK;
    u16                     intf_idx;
    ipmc_conf_intf_entry_t  *cur_intf;
    ipmc_prot_intf_basic_t  entry;
    vtss_tick_count_t        exe_time_base;

    if (!msg_switch_is_master()) {
        return rc;
    }
    exe_time_base = vtss_current_time();

    IPMC_CRIT_ENTER();

    if ((intf_idx = ipmc_conf_intf_entry_get(intf_param->vid, ipmc_version)) == IPMC_VID_INIT) {
        rc = VTSS_RC_ERROR;

        IPMC_CRIT_EXIT();
        return rc;
    }

    switch ( ipmc_version ) {
    case IPMC_IP_VERSION_IGMP:
        cur_intf = &ipmc_global.ipv4_conf.ipmc_conf_intf_entries[intf_idx];

        break;
    case IPMC_IP_VERSION_MLD:
        if (intf_param->querier.QuerierAdrs4) {
            rc = VTSS_RC_ERROR;
            IPMC_CRIT_EXIT();
            return rc;
        }

        cur_intf = &ipmc_global.ipv6_conf.ipmc_conf_intf_entries[intf_idx];

        break;
    default:
        rc = VTSS_RC_ERROR;
        IPMC_CRIT_EXIT();
        return rc;
    }

    IPMC_CRIT_EXIT();

    entry.valid = TRUE;
    entry.vid = intf_param->vid;
    entry.compatibility = intf_param->cfg_compatibility;
    entry.priority = intf_param->priority;
    entry.querier4_address = intf_param->querier.QuerierAdrs4;
    entry.robustness_variable = intf_param->querier.RobustVari;
    entry.query_interval = intf_param->querier.QueryIntvl;
    entry.query_response_interval = intf_param->querier.MaxResTime;
    entry.last_listener_query_interval = intf_param->querier.LastQryItv;
    entry.unsolicited_report_interval = intf_param->querier.UnsolicitR;

    if (ipmc_lib_isid_is_local(isid)) {
        IPMC_CRIT_ENTER();
        if (!vtss_ipmc_upd_intf_entry(&entry, ipmc_version)) {
            rc = VTSS_RC_ERROR;
            IPMC_CRIT_EXIT();
            return rc;
        }
        IPMC_CRIT_EXIT();
    } else {
        rc = ipmc_stacking_set_intf_entry(isid, intf_param, ipmc_version);
        if (rc != VTSS_OK) {
            return rc;
        }
    }

    IPMC_CRIT_ENTER();
    if ((cur_intf->compatibility == intf_param->cfg_compatibility) &&
        (cur_intf->priority == intf_param->priority) &&
        (cur_intf->querier4_address == intf_param->querier.QuerierAdrs4) &&
        (cur_intf->robustness_variable == intf_param->querier.RobustVari) &&
        (cur_intf->query_interval == intf_param->querier.QueryIntvl) &&
        (cur_intf->query_response_interval == intf_param->querier.MaxResTime) &&
        (cur_intf->last_listener_query_interval == intf_param->querier.LastQryItv) &&
        (cur_intf->unsolicited_report_interval == intf_param->querier.UnsolicitR)) {
        IPMC_CRIT_EXIT();
        return rc;
    }

    cur_intf->compatibility = entry.compatibility;
    cur_intf->priority = entry.priority;
    cur_intf->querier4_address = entry.querier4_address;
    cur_intf->robustness_variable = entry.robustness_variable;
    cur_intf->query_interval = entry.query_interval;
    cur_intf->query_response_interval = entry.query_response_interval;
    cur_intf->last_listener_query_interval = entry.last_listener_query_interval;
    cur_intf->unsolicited_report_interval = entry.unsolicited_report_interval;
    IPMC_CRIT_EXIT();

    T_D("ipmc_mgmt_set_intf_info(%u) consumes ID%d:%u ticks", intf_idx, isid, (u32)(vtss_current_time() - exe_time_base));

    return rc;
}

mesa_rc ipmc_mgmt_get_intf_info(vtss_isid_t isid, mesa_vid_t vid, ipmc_prot_intf_entry_param_t *intf_param, ipmc_ip_version_t ipmc_version)
{
    if (!intf_param) {
        return VTSS_RC_ERROR;
    }

    memset(intf_param, 0x0, sizeof(ipmc_prot_intf_entry_param_t));
    if ((isid == VTSS_ISID_GLOBAL) || ipmc_lib_isid_is_local(isid)) {
        ipmc_conf_intf_entry_t  *cur_intf_get, *intf_get_bak;
        ipmc_intf_entry_t       *entry;
        u16                     intf_idx;

        if (!IPMC_MEM_SYSTEM_MTAKE(cur_intf_get, sizeof(ipmc_conf_intf_entry_t))) {
            return VTSS_RC_ERROR;
        }
        intf_get_bak = cur_intf_get;

        IPMC_GET_CRIT_ENTER();
        if ((entry = vtss_ipmc_get_intf_entry(vid, ipmc_version)) != NULL) {
            memcpy(intf_param, &entry->param, sizeof(ipmc_prot_intf_entry_param_t));
        } else {
            if ((intf_idx = ipmc_conf_intf_entry_get(vid, ipmc_version)) != IPMC_VID_INIT) {
                switch ( ipmc_version ) {
                case IPMC_IP_VERSION_IGMP:
                    memcpy(cur_intf_get, &ipmc_global.ipv4_conf.ipmc_conf_intf_entries[intf_idx], sizeof(ipmc_conf_intf_entry_t));

                    break;
                case IPMC_IP_VERSION_MLD:
                    memcpy(cur_intf_get, &ipmc_global.ipv6_conf.ipmc_conf_intf_entries[intf_idx], sizeof(ipmc_conf_intf_entry_t));

                    break;
                default:
                    IPMC_GET_CRIT_EXIT();
                    IPMC_MEM_SYSTEM_MGIVE(intf_get_bak);
                    return VTSS_RC_ERROR;
                }

                _ipmc_trans_cfg_intf_info(intf_param, cur_intf_get);
            }
        }
        IPMC_GET_CRIT_EXIT();
        IPMC_MEM_SYSTEM_MGIVE(intf_get_bak);
    } else {
        if (ipmc_stacking_get_intf_entry(isid, vid, intf_param, ipmc_version) != VTSS_OK) {
            return VTSS_RC_ERROR;
        }
    }

    if (intf_param->vid == 0) {
        return VTSS_RC_ERROR;
    }

    return VTSS_OK;
}

mesa_rc ipmc_mgmt_get_next_group_srclist_info(
    const vtss_isid_t           isid,
    const ipmc_ip_version_t     ipmc_version,
    const mesa_vid_t            vid,
    const mesa_ipv6_t           *const addr,
    ipmc_prot_group_srclist_t   *const group_srclist_entry
)
{
    mesa_rc rc = VTSS_OK;

    if (!addr || !group_srclist_entry) {
        return VTSS_RC_ERROR;
    }

    if ((rc = ipmc_stacking_get_next_group_srclist_entry(isid, ipmc_version, vid, addr, group_srclist_entry)) == VTSS_OK) {
        if (group_srclist_entry->valid == FALSE) {
            rc = VTSS_RC_ERROR;
        }
    }

    return rc;
}

mesa_rc ipmc_mgmt_get_next_intf_group_info(vtss_isid_t isid, mesa_vid_t vid, ipmc_prot_intf_group_entry_t *intf_group_entry, ipmc_ip_version_t ipmc_version)
{
    mesa_rc rc = VTSS_OK;

    if (!intf_group_entry) {
        return VTSS_RC_ERROR;
    }

    if ((rc = ipmc_stacking_get_next_intf_group_entry(isid, vid, intf_group_entry, ipmc_version)) == VTSS_OK) {
        if (intf_group_entry->valid == FALSE) {
            rc = VTSS_RC_ERROR;
        }
    }

    return rc;
}

/* Get IPMC Group Info */
mesa_rc
ipmc_mgmt_group_info_get(
    const vtss_isid_t               sidx,
    const ipmc_ip_version_t         verx,
    const mesa_vid_t                *const vidx,
    const mesa_ipv6_t               *const grpx,
    ipmc_prot_intf_group_entry_t    *const intf_group_entry
)
{
    return ipmc_stacking_group_entry_get(sidx, verx, vidx, grpx, intf_group_entry);
}

/* GetNext IPMC Group Info */
mesa_rc
ipmc_mgmt_group_info_get_next(
    const vtss_isid_t               sidx,
    const ipmc_ip_version_t         verx,
    const mesa_vid_t                *const vidx,
    const mesa_ipv6_t               *const grpx,
    ipmc_prot_intf_group_entry_t    *const intf_group_entry
)
{
    return ipmc_stacking_group_entry_get_next(sidx, verx, vidx, grpx, intf_group_entry);
}

mesa_rc ipmc_mgmt_set_router_port(vtss_isid_t isid, ipmc_conf_router_port_t *router_port, ipmc_ip_version_t ipmc_version)
{
    port_iter_t       pit;
    BOOL              apply_flag = FALSE, *router;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!router_port) {
        return VTSS_RC_ERROR;
    }

    if (!msg_switch_is_master()) {
        return VTSS_OK;
    }

    IPMC_CRIT_ENTER();
    if (ipmc_version == IPMC_IP_VERSION_IGMP) {
        router = ipmc_global.ipv4_conf.ipmc_router_ports[isid - VTSS_ISID_START].data();
    } else if (ipmc_version == IPMC_IP_VERSION_MLD) {
        router = ipmc_global.ipv6_conf.ipmc_router_ports[isid - VTSS_ISID_START].data();
    } else {
        IPMC_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }

    (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
    while (port_iter_getnext(&pit)) {
        if (router_port->ports[pit.iport] != router[pit.iport]) {
            apply_flag = TRUE;
            break;
        }
    }

    if (!apply_flag && ipmc_lib_isid_is_local(isid)) {
        IPMC_CRIT_EXIT();
        return VTSS_OK;
    }

    if (apply_flag) {
        (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        while (port_iter_getnext(&pit)) {
            router[pit.iport] = router_port->ports[pit.iport];
        }
    }
    IPMC_CRIT_EXIT();

    (void) ipmc_stacking_set_router_port(isid, ipmc_version);

    T_D("ipmc_mgmt_set_router_port consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc ipmc_mgmt_get_static_router_port(vtss_isid_t isid, ipmc_conf_router_port_t *router_port, ipmc_ip_version_t ipmc_version)
{
    port_iter_t       pit;
    BOOL              *router;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!router_port) {
        return VTSS_RC_ERROR;
    }

    IPMC_CRIT_ENTER();
    if (ipmc_version == IPMC_IP_VERSION_IGMP) {
        router = ipmc_global.ipv4_conf.ipmc_router_ports[isid - VTSS_ISID_START].data();
    } else if (ipmc_version == IPMC_IP_VERSION_MLD) {
        router = ipmc_global.ipv6_conf.ipmc_router_ports[isid - VTSS_ISID_START].data();
    } else {
        IPMC_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }

    if (msg_switch_is_master()) {
        (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
    } else {
        (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
    }
    while (port_iter_getnext(&pit)) {
        router_port->ports[pit.iport] = router[pit.iport];
    }
    IPMC_CRIT_EXIT();

    T_D("ipmc_mgmt_get_static_router_port consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc ipmc_mgmt_get_dynamic_router_ports(vtss_isid_t isid, ipmc_dynamic_router_port_t *router_port, ipmc_ip_version_t ipmc_version)
{
    mesa_rc rc;

    if (!router_port) {
        return VTSS_RC_ERROR;
    }

    rc = ipmc_stacking_get_dynamic_router_port(isid, router_port, ipmc_version);

    return rc;
}

mesa_rc ipmc_mgmt_set_fast_leave_port(vtss_isid_t isid, ipmc_conf_fast_leave_port_t *fast_leave_port, ipmc_ip_version_t ipmc_version)
{
    port_iter_t       pit;
    BOOL              apply_flag = FALSE, *fast_leave;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!fast_leave_port) {
        return VTSS_RC_ERROR;
    }

    if (!msg_switch_is_master()) {
        return VTSS_OK;
    }

    IPMC_CRIT_ENTER();
    if (ipmc_version == IPMC_IP_VERSION_IGMP) {
        fast_leave = ipmc_global.ipv4_conf.ipmc_fast_leave_ports[isid - VTSS_ISID_START].data();
    } else if (ipmc_version == IPMC_IP_VERSION_MLD) {
        fast_leave = ipmc_global.ipv6_conf.ipmc_fast_leave_ports[isid - VTSS_ISID_START].data();
    } else {
        IPMC_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }

    (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
    while (port_iter_getnext(&pit)) {
        if (fast_leave_port->ports[pit.iport] != fast_leave[pit.iport]) {
            apply_flag = TRUE;
            break;
        }
    }

    if (!apply_flag && ipmc_lib_isid_is_local(isid)) {
        IPMC_CRIT_EXIT();
        return VTSS_OK;
    }

    if (apply_flag) {
        (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        while (port_iter_getnext(&pit)) {
            fast_leave[pit.iport] = fast_leave_port->ports[pit.iport];
        }
    }
    IPMC_CRIT_EXIT();

    (void) ipmc_stacking_set_fastleave_port(isid, ipmc_version);

    T_D("ipmc_mgmt_set_fast_leave_port consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc ipmc_mgmt_get_fast_leave_port(vtss_isid_t isid, ipmc_conf_fast_leave_port_t *fast_leave_port, ipmc_ip_version_t ipmc_version)
{
    port_iter_t       pit;
    BOOL              *fast_leave;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!fast_leave_port) {
        return VTSS_RC_ERROR;
    }

    IPMC_CRIT_ENTER();
    if (ipmc_version == IPMC_IP_VERSION_IGMP) {
        fast_leave = ipmc_global.ipv4_conf.ipmc_fast_leave_ports[isid - VTSS_ISID_START].data();
    } else if (ipmc_version == IPMC_IP_VERSION_MLD) {
        fast_leave = ipmc_global.ipv6_conf.ipmc_fast_leave_ports[isid - VTSS_ISID_START].data();
    } else {
        IPMC_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }

    if (msg_switch_is_master()) {
        (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
    } else {
        (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
    }
    memset(fast_leave_port, 0x0, sizeof(ipmc_conf_fast_leave_port_t));
    while (port_iter_getnext(&pit)) {
        fast_leave_port->ports[pit.iport] = fast_leave[pit.iport];
    }
    IPMC_CRIT_EXIT();

    T_D("ipmc_mgmt_get_fast_leave_port consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc ipmc_mgmt_set_throttling_max_count(vtss_isid_t isid, ipmc_conf_throttling_max_no_t *ipmc_throttling_max_no, ipmc_ip_version_t ipmc_version)
{
    int               *throttling_number;
    port_iter_t       pit;
    BOOL              apply_flag = FALSE;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!ipmc_throttling_max_no) {
        return VTSS_RC_ERROR;
    }

    if (!msg_switch_is_master()) {
        return VTSS_OK;
    }

    IPMC_CRIT_ENTER();
    if (ipmc_version == IPMC_IP_VERSION_IGMP) {
        throttling_number = ipmc_global.ipv4_conf.ipmc_throttling_max_no[isid - VTSS_ISID_START].data();
    } else if (ipmc_version == IPMC_IP_VERSION_MLD) {
        throttling_number = ipmc_global.ipv6_conf.ipmc_throttling_max_no[isid - VTSS_ISID_START].data();
    } else {
        IPMC_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }

    (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
    while (port_iter_getnext(&pit)) {
        if (ipmc_throttling_max_no->ports[pit.iport] != throttling_number[pit.iport]) {
            apply_flag = TRUE;
            break;
        }
    }

    if (!apply_flag && ipmc_lib_isid_is_local(isid)) {
        IPMC_CRIT_EXIT();
        return VTSS_OK;
    }

    if (apply_flag) {
        (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
        while (port_iter_getnext(&pit)) {
            throttling_number[pit.iport] = ipmc_throttling_max_no->ports[pit.iport];
        }
    }
    IPMC_CRIT_EXIT();

    (void) ipmc_stacking_set_throttling_number(isid, ipmc_version);

    T_D("ipmc_mgmt_set_throttling_max_count consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc ipmc_mgmt_get_throttling_max_count(vtss_isid_t isid, ipmc_conf_throttling_max_no_t *ipmc_throttling_max_no, ipmc_ip_version_t ipmc_version)
{
    int               *throttling_number;
    port_iter_t       pit;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!ipmc_throttling_max_no) {
        return VTSS_RC_ERROR;
    }

    IPMC_CRIT_ENTER();
    if (ipmc_version == IPMC_IP_VERSION_IGMP) {
        throttling_number = ipmc_global.ipv4_conf.ipmc_throttling_max_no[isid - VTSS_ISID_START].data();
    } else if (ipmc_version == IPMC_IP_VERSION_MLD) {
        throttling_number = ipmc_global.ipv6_conf.ipmc_throttling_max_no[isid - VTSS_ISID_START].data();
    } else {
        IPMC_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }

    if (msg_switch_is_master()) {
        (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
    } else {
        (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
    }
    while (port_iter_getnext(&pit)) {
        ipmc_throttling_max_no->ports[pit.iport] = throttling_number[pit.iport];
    }
    IPMC_CRIT_EXIT();

    T_D("ipmc_mgmt_get_throttling_max_count consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc ipmc_mgmt_set_port_group_filtering(vtss_isid_t isid, ipmc_conf_port_group_filtering_t *ipmc_port_group_filtering, ipmc_ip_version_t ipmc_version)
{
    int                         str_len;
    u32                         pdx;
    ipmc_configuration_t        *conf;
    vtss_tick_count_t           exe_time_base;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *profile;
    ipmc_lib_profile_t          *data;

    if (!ipmc_port_group_filtering) {
        return VTSS_RC_ERROR;
    }

    if (!msg_switch_is_master()) {
        return VTSS_OK;
    }

    if (!IPMC_MEM_PROFILE_MTAKE(pfm)) {
        return VTSS_RC_ERROR;
    }

    exe_time_base = vtss_current_time();
    profile = &pfm->profile;
    data = &profile->data;

    memset(profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
    memcpy(data->name, &ipmc_port_group_filtering->addr.profile.name[0], VTSS_IPMC_NAME_MAX_LEN);
    if (ipmc_lib_mgmt_fltr_profile_get(profile, TRUE) != VTSS_OK) {
        IPMC_MEM_PROFILE_MGIVE(pfm);
        return VTSS_RC_ERROR;
    }
    pdx = data->index;
    memset(profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));

    IPMC_CRIT_ENTER();
    if (ipmc_version == IPMC_IP_VERSION_IGMP) {
        conf = &ipmc_global.ipv4_conf;
    } else {
        conf = &ipmc_global.ipv6_conf;
    }

    str_len = 0;
    data->index = conf->ipmc_port_group_filtering[isid - VTSS_ISID_START][ipmc_port_group_filtering->port_no];
    if (ipmc_lib_mgmt_fltr_profile_get(profile, FALSE) == VTSS_OK) {
        str_len = strlen(data->name);
    }
    /* check whether entry existing or not */
    if (str_len) {
        if (!strncmp(data->name, ipmc_port_group_filtering->addr.profile.name, str_len)) {
            /* if this entry is found, just do nothing */
            if (ipmc_lib_isid_is_local(isid)) {
                IPMC_CRIT_EXIT();
                IPMC_MEM_PROFILE_MGIVE(pfm);
                return VTSS_OK;
            }
        }

        conf->ipmc_port_group_filtering[isid - VTSS_ISID_START][ipmc_port_group_filtering->port_no] = IPMC_LIB_FLTR_PROFILE_IDX_NONE;
    }

    conf->ipmc_port_group_filtering[isid - VTSS_ISID_START][ipmc_port_group_filtering->port_no] = pdx;
    IPMC_CRIT_EXIT();
    IPMC_MEM_PROFILE_MGIVE(pfm);

    /* sync this update to the corresponding switch */
    (void) ipmc_stacking_set_grp_filtering(isid, ipmc_version);

    T_D("ipmc_mgmt_set_port_group_filtering consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc ipmc_mgmt_del_port_group_filtering(vtss_isid_t isid, ipmc_conf_port_group_filtering_t *ipmc_port_group_filtering, ipmc_ip_version_t ipmc_version)
{
    u32                         pdx;
    ipmc_configuration_t        *conf;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *profile;
    ipmc_lib_profile_t          *data;
    vtss_tick_count_t           exe_time_base;

    if (!ipmc_port_group_filtering) {
        return VTSS_RC_ERROR;
    }

    if (!msg_switch_is_master()) {
        return VTSS_OK;
    }

    if (!IPMC_MEM_PROFILE_MTAKE(pfm)) {
        return VTSS_RC_ERROR;
    }

    exe_time_base = vtss_current_time();

    profile = &pfm->profile;
    data = &profile->data;
    memset(profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
    memcpy(data->name, &ipmc_port_group_filtering->addr.profile.name[0], VTSS_IPMC_NAME_MAX_LEN);
    if (ipmc_lib_mgmt_fltr_profile_get(profile, TRUE) != VTSS_OK) {
        IPMC_MEM_PROFILE_MGIVE(pfm);
        return VTSS_RC_ERROR;
    }

    IPMC_CRIT_ENTER();
    if (ipmc_version == IPMC_IP_VERSION_IGMP) {
        conf = &ipmc_global.ipv4_conf;
    } else {
        conf = &ipmc_global.ipv6_conf;
    }

    pdx = conf->ipmc_port_group_filtering[isid - VTSS_ISID_START][ipmc_port_group_filtering->port_no];
    if ((pdx == IPMC_LIB_FLTR_PROFILE_IDX_NONE) ||
        (pdx != data->index)) {
        IPMC_CRIT_EXIT();
        IPMC_MEM_PROFILE_MGIVE(pfm);
        return VTSS_RC_ERROR;
    }

    conf->ipmc_port_group_filtering[isid - VTSS_ISID_START][ipmc_port_group_filtering->port_no] = IPMC_LIB_FLTR_PROFILE_IDX_NONE;
    IPMC_CRIT_EXIT();
    IPMC_MEM_PROFILE_MGIVE(pfm);

    /* sync this update to the corresponding switch */
    (void) ipmc_stacking_set_grp_filtering(isid, ipmc_version);

    T_D("ipmc_mgmt_del_port_group_filtering consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc ipmc_mgmt_get_port_group_filtering(vtss_isid_t isid, ipmc_conf_port_group_filtering_t *ipmc_port_group_filtering, ipmc_ip_version_t ipmc_version)
{
    ipmc_configuration_t        *conf;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *profile;
    ipmc_lib_profile_t          *data;
    vtss_tick_count_t           exe_time_base;

    if (!ipmc_port_group_filtering || !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        return VTSS_RC_ERROR;
    }

    exe_time_base = vtss_current_time();
    profile = &pfm->profile;
    data = &profile->data;

    IPMC_CRIT_ENTER();
    if (ipmc_version == IPMC_IP_VERSION_IGMP) {
        conf = &ipmc_global.ipv4_conf;
    } else {
        conf = &ipmc_global.ipv6_conf;
    }

    data->index = conf->ipmc_port_group_filtering[isid - VTSS_ISID_START][ipmc_port_group_filtering->port_no];
    if (ipmc_lib_mgmt_fltr_profile_get(profile, FALSE) == VTSS_OK) {
        memcpy(&ipmc_port_group_filtering->addr.profile.name[0], &data->name[0], VTSS_IPMC_NAME_MAX_LEN);
    } else {
        IPMC_CRIT_EXIT();
        IPMC_MEM_PROFILE_MGIVE(pfm);
        memset(&ipmc_port_group_filtering->addr.profile.name[0], 0x0, VTSS_IPMC_NAME_MAX_LEN);
        return VTSS_RC_ERROR;
    }
    IPMC_CRIT_EXIT();
    IPMC_MEM_PROFILE_MGIVE(pfm);

    T_D("ipmc_mgmt_get_port_group_filtering consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc ipmc_mgmt_get_intf_version(vtss_isid_t isid, ipmc_intf_query_host_version_t *vlan_version_entry, ipmc_ip_version_t ipmc_version)
{
    ipmc_prot_intf_entry_param_t    intf_param;

    if (!vlan_version_entry) {
        return VTSS_RC_ERROR;
    }

    memset(&intf_param, 0x0, sizeof(ipmc_prot_intf_entry_param_t));
    if ((isid == VTSS_ISID_GLOBAL) || ipmc_lib_isid_is_local(isid)) {
        ipmc_intf_entry_t   *entry;

        IPMC_GET_CRIT_ENTER();
        if ((entry = vtss_ipmc_get_intf_entry(vlan_version_entry->vid, ipmc_version)) != NULL) {
            memcpy(&intf_param, &entry->param, sizeof(ipmc_prot_intf_entry_param_t));
        }
        IPMC_GET_CRIT_EXIT();
    } else {
        if (ipmc_stacking_get_intf_entry(isid, vlan_version_entry->vid, &intf_param, ipmc_version) != VTSS_OK) {
            return VTSS_RC_ERROR;
        }
    }

    if (intf_param.vid == 0) {
        return VTSS_RC_ERROR;
    }

    switch ( ipmc_version ) {
    case IPMC_IP_VERSION_IGMP:
        if (intf_param.cfg_compatibility != VTSS_IPMC_COMPAT_MODE_AUTO) {
            if (intf_param.cfg_compatibility == VTSS_IPMC_COMPAT_MODE_OLD) {
                vlan_version_entry->host_version = VTSS_IPMC_IGMP_VERSION1;
                vlan_version_entry->query_version = VTSS_IPMC_IGMP_VERSION1;
            } else if (intf_param.cfg_compatibility == VTSS_IPMC_COMPAT_MODE_GEN) {
                vlan_version_entry->host_version = VTSS_IPMC_IGMP_VERSION2;
                vlan_version_entry->query_version = VTSS_IPMC_IGMP_VERSION2;
            } else if (intf_param.cfg_compatibility == VTSS_IPMC_COMPAT_MODE_SFM) {
                vlan_version_entry->host_version = VTSS_IPMC_IGMP_VERSION3;
                vlan_version_entry->query_version = VTSS_IPMC_IGMP_VERSION3;
            } else {
                vlan_version_entry->host_version = VTSS_IPMC_IGMP_VERSION_DEF;
                vlan_version_entry->query_version = VTSS_IPMC_IGMP_VERSION_DEF;
            }

            break;
        }

        if (intf_param.hst_compatibility.old_present_timer) {
            vlan_version_entry->host_version = VTSS_IPMC_IGMP_VERSION1;
        } else {
            if (intf_param.hst_compatibility.gen_present_timer) {
                vlan_version_entry->host_version = VTSS_IPMC_IGMP_VERSION2;
            } else {
                if (intf_param.hst_compatibility.sfm_present_timer) {
                    vlan_version_entry->host_version = VTSS_IPMC_IGMP_VERSION3;
                } else {
                    vlan_version_entry->host_version = VTSS_IPMC_IGMP_VERSION_DEF;
                }
            }
        }

        if (intf_param.rtr_compatibility.old_present_timer) {
            vlan_version_entry->query_version = VTSS_IPMC_IGMP_VERSION1;
        } else {
            if (intf_param.rtr_compatibility.gen_present_timer) {
                vlan_version_entry->query_version = VTSS_IPMC_IGMP_VERSION2;
            } else {
                if (intf_param.rtr_compatibility.sfm_present_timer) {
                    vlan_version_entry->query_version = VTSS_IPMC_IGMP_VERSION3;
                } else {
                    vlan_version_entry->query_version = VTSS_IPMC_IGMP_VERSION_DEF;
                }
            }
        }

        break;
    case IPMC_IP_VERSION_MLD:
        if (intf_param.cfg_compatibility != VTSS_IPMC_COMPAT_MODE_AUTO) {
            if (intf_param.cfg_compatibility == VTSS_IPMC_COMPAT_MODE_GEN) {
                vlan_version_entry->host_version = VTSS_IPMC_MLD_VERSION1;
                vlan_version_entry->query_version = VTSS_IPMC_MLD_VERSION1;
            } else if (intf_param.cfg_compatibility == VTSS_IPMC_COMPAT_MODE_SFM) {
                vlan_version_entry->host_version = VTSS_IPMC_MLD_VERSION2;
                vlan_version_entry->query_version = VTSS_IPMC_MLD_VERSION2;
            } else {
                vlan_version_entry->host_version = VTSS_IPMC_MLD_VERSION_DEF;
                vlan_version_entry->query_version = VTSS_IPMC_MLD_VERSION_DEF;
            }

            break;
        }

        if (intf_param.hst_compatibility.gen_present_timer) {
            vlan_version_entry->host_version = VTSS_IPMC_MLD_VERSION1;
        } else {
            if (intf_param.hst_compatibility.sfm_present_timer) {
                vlan_version_entry->host_version = VTSS_IPMC_MLD_VERSION2;
            } else {
                vlan_version_entry->host_version = VTSS_IPMC_MLD_VERSION_DEF;
            }
        }

        if (intf_param.rtr_compatibility.gen_present_timer) {
            vlan_version_entry->query_version = VTSS_IPMC_MLD_VERSION1;
        } else {
            if (intf_param.rtr_compatibility.sfm_present_timer) {
                vlan_version_entry->query_version = VTSS_IPMC_MLD_VERSION2;
            } else {
                vlan_version_entry->query_version = VTSS_IPMC_MLD_VERSION_DEF;
            }
        }

        break;
    default:
        break;
    }

    return VTSS_OK;
}

static void ipmc_update_vlan_tab(vtss_isid_t isid, ipmc_ip_version_t ipmc_version)
{
    vtss_appl_vlan_entry_t vlan_conf;
    ipmc_port_bfs_t        vlan_ports;
    port_iter_t            pit;
    BOOL                   found;
    mesa_vid_t             vid;
    u16                    intf_idx;
    ipmc_conf_intf_entry_t *conf_intf, ipmc_intf;

    if (!msg_switch_is_master()) {
        return;
    }

    T_D("enter ipmc_update_vlan_tab (isid:%d/ver:%d)", isid, ipmc_version);

    vid = 0;
    IPMC_CRIT_ENTER();
    found = ((intf_idx = ipmc_conf_intf_entry_get_next(&vid, ipmc_version)) != IPMC_VID_INIT);
    IPMC_CRIT_EXIT();
    while (found) {
        IPMC_CRIT_ENTER();
        switch ( ipmc_version ) {
        case IPMC_IP_VERSION_IGMP:
            conf_intf = &ipmc_global.ipv4_conf.ipmc_conf_intf_entries[intf_idx];

            break;
        case IPMC_IP_VERSION_MLD:
            conf_intf = &ipmc_global.ipv6_conf.ipmc_conf_intf_entries[intf_idx];

            break;
        default:
            found = ((intf_idx = ipmc_conf_intf_entry_get_next(&vid, ipmc_version)) != IPMC_VID_INIT);
            IPMC_CRIT_EXIT();

            continue;
        }

        memcpy(&ipmc_intf, conf_intf, sizeof(ipmc_conf_intf_entry_t));
        IPMC_CRIT_EXIT();

        T_D("ipmc_update_vlan_tab idx:%u", intf_idx);

        VTSS_PORT_BF_CLR(vlan_ports.member_ports);
        vlan_conf.vid = vid;
        if (ipmc_resolve_vlan_member(isid, &vlan_conf, FALSE) == VTSS_OK) {
            (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
            while (port_iter_getnext(&pit)) {
                VTSS_PORT_BF_SET(vlan_ports.member_ports, pit.iport, vlan_conf.ports[pit.iport]);
            }
        }

        if (ipmc_intf.protocol_status) {
            ipmc_prot_intf_entry_param_t    intf_param;

            (void) ipmc_stacking_set_intf(isid, IPMC_OP_SET, &ipmc_intf, &vlan_ports, ipmc_version);

            memset(&intf_param, 0x0, sizeof(ipmc_prot_intf_entry_param_t));
            intf_param.vid = vid;
            intf_param.cfg_compatibility = (ipmc_compat_mode_t)ipmc_intf.compatibility;
            intf_param.priority = ipmc_intf.priority;
            intf_param.querier.QuerierAdrs4 = ipmc_intf.querier4_address;
            intf_param.querier.RobustVari = ipmc_intf.robustness_variable;
            intf_param.querier.QueryIntvl = ipmc_intf.query_interval;
            intf_param.querier.MaxResTime = ipmc_intf.query_response_interval;
            intf_param.querier.LastQryItv = ipmc_intf.last_listener_query_interval;
            intf_param.querier.LastQryCnt = intf_param.querier.RobustVari;
            intf_param.querier.UnsolicitR = ipmc_intf.unsolicited_report_interval;

            (void) ipmc_stacking_set_intf_entry(isid, &intf_param, ipmc_version);
        } else {
            (void) ipmc_stacking_set_intf(isid, IPMC_OP_SET, &ipmc_intf, &vlan_ports, ipmc_version);
        }

        IPMC_CRIT_ENTER();
        found = ((intf_idx = ipmc_conf_intf_entry_get_next(&vid, ipmc_version)) != IPMC_VID_INIT);
        IPMC_CRIT_EXIT();
    }

    T_D("exit ipmc_update_vlan_tab (isid:%d/ver:%d)", isid, ipmc_version);
}

static void ipmc_vlan_changed(vtss_isid_t isid, mesa_vid_t vid, vlan_membership_change_t *changes)
{
    u16                     intf_idx4, intf_idx6;
    ipmc_port_bfs_t         vlan_ports;
    ipmc_conf_intf_entry_t  ipmc_intf;
    vtss_tick_count_t       exe_time_base;
    mesa_port_no_t          iport;

    if (!VTSS_ISID_LEGAL(isid)) {
        return;
    }

    /*
        VLAN Change
        CfgExists + VlanExists:   ipmc_stacking_set_intf (VLAN Member Change)
        CfgExists + !VlanExists:  ipmc_stacking_set_intf (Delete Running Tables)
        !CfgExists + VlanExists:  Do Nothing
        !CfgExists + !VlanExists: Do Nothing
    */
    IPMC_CRIT_ENTER();
    intf_idx4 = ipmc_conf_intf_entry_get(vid, IPMC_IP_VERSION_IGMP);
    intf_idx6 = ipmc_conf_intf_entry_get(vid, IPMC_IP_VERSION_MLD);
    IPMC_CRIT_EXIT();

    if (intf_idx4 == IPMC_VID_INIT && intf_idx6 == IPMC_VID_INIT) {
        // !CfgExists
        return;
    }

    exe_time_base = vtss_current_time();
    T_I("Enter ISID:%d/VID:%u", isid, vid);

    memset(&vlan_ports, 0x0, sizeof(ipmc_port_bfs_t));
    if (changes->static_vlan_exists) {   /* CfgExists + VlanExists */
        // Resulting configuration for static user must be computed based on both the static and the forbidden VLAN users' configuration.
        // However, forbidden ports should only work for GVRP and thus IPMC-SNP won't refer to forbidden membership.
        for (iport = 0; iport < mesa_port_cnt(NULL); iport++) {
            VTSS_PORT_BF_SET(vlan_ports.member_ports, iport, changes->static_ports.ports[iport]);
        }
        T_D("IPMC(%s/%s) update VID %u for ISID %d",
            intf_idx4 != IPMC_VID_INIT ? "IGMP" : "NA",
            intf_idx6 != IPMC_VID_INIT ? "MLD" : "NA",
            vid,
            isid);
    } else {                        /* CfgExists + !VlanExists */
        T_D("IPMC(%s/%s) delete VID %u for ISID %d",
            intf_idx4 != IPMC_VID_INIT ? "IGMP" : "NA",
            intf_idx6 != IPMC_VID_INIT ? "MLD" : "NA",
            vid,
            isid);
    }

    /* IGMP */
    if (intf_idx4 != IPMC_VID_INIT) {
        IPMC_CRIT_ENTER();
        memcpy(&ipmc_intf, &ipmc_global.ipv4_conf.ipmc_conf_intf_entries[intf_idx4], sizeof(ipmc_conf_intf_entry_t));
        IPMC_CRIT_EXIT();

        if (ipmc_intf.protocol_status &&
            ipmc_stacking_set_intf(isid, IPMC_OP_SET, &ipmc_intf, &vlan_ports, IPMC_IP_VERSION_IGMP) == VTSS_OK) {
            T_D("Done for IGMP VID-%u/ISID-%d", vid, isid);
        }
    }

    /* MLD */
    if (intf_idx6 != IPMC_VID_INIT) {
        IPMC_CRIT_ENTER();
        memcpy(&ipmc_intf, &ipmc_global.ipv6_conf.ipmc_conf_intf_entries[intf_idx6], sizeof(ipmc_conf_intf_entry_t));
        IPMC_CRIT_EXIT();

        if (ipmc_intf.protocol_status &&
            ipmc_stacking_set_intf(isid, IPMC_OP_SET, &ipmc_intf, &vlan_ports, IPMC_IP_VERSION_MLD) == VTSS_OK) {
            T_D("Done for MLD VID-%u/ISID-%d", vid, isid);
        }
    }

    T_I("Exit %u ticks", (u32)(vtss_current_time() - exe_time_base));
}

void ipmc_refresh_enable(ipmc_ip_version_t ipmc_version)
{
    port_iter_t                     pit;
    u16                             intf_idx;
    ipmc_intf_entry_t               *ipmc_vid_entry;
    ipmc_conf_intf_entry_t          ipmc_intf;
    ipmc_prot_intf_entry_param_t    intf_param;
    ipmc_port_bfs_t                 vlan_ports_temp;
    u16                             i = 0;
    ipmc_ip_version_t               version = ipmc_version;
    BOOL                            global_enable_status, found;

    IPMC_CRIT_ENTER();
    switch ( ipmc_version ) {
    case IPMC_IP_VERSION_IGMP:
        global_enable_status = ipmc_global.ipv4_conf.global.ipmc_mode_enabled;

        break;
    case IPMC_IP_VERSION_MLD:
        global_enable_status = ipmc_global.ipv6_conf.global.ipmc_mode_enabled;

        break;
    default:
        IPMC_CRIT_EXIT();
        return;
    }

    ipmc_vid_entry = vtss_ipmc_get_next_intf_entry(i, version);
    if (ipmc_vid_entry) {
        found = TRUE;

        version = ipmc_vid_entry->ipmc_version;
        i = ipmc_vid_entry->param.vid;

        if (global_enable_status) {
            memcpy(&intf_param, &ipmc_vid_entry->param, sizeof(ipmc_prot_intf_entry_param_t));
        } else {
            VTSS_PORT_BF_CLR(vlan_ports_temp.member_ports);

            (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
            while (port_iter_getnext(&pit)) {
                VTSS_PORT_BF_SET(vlan_ports_temp.member_ports,
                                 pit.iport,
                                 VTSS_PORT_BF_GET(ipmc_vid_entry->vlan_ports, pit.iport));
            }
        }
    } else {
        found = FALSE;
    }

    IPMC_CRIT_EXIT();

    while (found) {
        IPMC_CRIT_ENTER();
        if ((intf_idx = ipmc_conf_intf_entry_get(i, ipmc_version)) != IPMC_VID_INIT) {
            if (ipmc_version == IPMC_IP_VERSION_IGMP) {
                memcpy(&ipmc_intf, &ipmc_global.ipv4_conf.ipmc_conf_intf_entries[intf_idx], sizeof(ipmc_conf_intf_entry_t));
            } else {
                memcpy(&ipmc_intf, &ipmc_global.ipv6_conf.ipmc_conf_intf_entries[intf_idx], sizeof(ipmc_conf_intf_entry_t));
            }
        } else {
            memset(&ipmc_intf, 0x0, sizeof(ipmc_conf_intf_entry_t));
        }
        IPMC_CRIT_EXIT();

        if (intf_idx != IPMC_VID_INIT) {
            if (!global_enable_status) {
                /* deleting all VLANs and adding them again (restoring ipmc vlan tab to default state) */
                (void) ipmc_stacking_set_intf(VTSS_ISID_GLOBAL, IPMC_OP_DEL, &ipmc_intf, &vlan_ports_temp, ipmc_version);
                (void) ipmc_stacking_set_intf(VTSS_ISID_GLOBAL, IPMC_OP_SET, &ipmc_intf, &vlan_ports_temp, ipmc_version);
            } /* if (!global_enable_status) */

            intf_param.vid = i;
            intf_param.cfg_compatibility = (ipmc_compat_mode_t)ipmc_intf.compatibility;
            intf_param.priority = ipmc_intf.priority;
            intf_param.querier.QuerierAdrs4 = ipmc_intf.querier4_address;
            intf_param.querier.RobustVari = ipmc_intf.robustness_variable;
            intf_param.querier.QueryIntvl = ipmc_intf.query_interval;
            intf_param.querier.MaxResTime = ipmc_intf.query_response_interval;
            intf_param.querier.LastQryItv = ipmc_intf.last_listener_query_interval;
            intf_param.querier.LastQryCnt = intf_param.querier.RobustVari;
            intf_param.querier.UnsolicitR = ipmc_intf.unsolicited_report_interval;

            (void) ipmc_stacking_set_intf_entry(VTSS_ISID_GLOBAL, &intf_param, ipmc_version);
        } /* if (intf_idx != IPMC_VID_INIT) */

        IPMC_CRIT_ENTER();
        ipmc_vid_entry = vtss_ipmc_get_next_intf_entry(i, version);
        if (ipmc_vid_entry) {
            found = TRUE;

            version = ipmc_vid_entry->ipmc_version;
            i = ipmc_vid_entry->param.vid;

            if (global_enable_status) {
                memcpy(&intf_param, &ipmc_vid_entry->param, sizeof(ipmc_prot_intf_entry_param_t));
            } else {
                VTSS_PORT_BF_CLR(vlan_ports_temp.member_ports);

                (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
                while (port_iter_getnext(&pit)) {
                    VTSS_PORT_BF_SET(vlan_ports_temp.member_ports,
                                     pit.iport,
                                     VTSS_PORT_BF_GET(ipmc_vid_entry->vlan_ports, pit.iport));
                }
            }
        } else {
            found = FALSE;
        }
        IPMC_CRIT_EXIT();
    } /* while (ipmc_vid_entry != NULL) */
}

mesa_rc ipmc_mgmt_set_mode(BOOL *mode, ipmc_ip_version_t ipmc_version)
{
    vtss_tick_count_t exe_time_base = vtss_current_time();

    IPMC_CRIT_ENTER();

    switch (ipmc_version) {
    case IPMC_IP_VERSION_IGMP:
        if (ipmc_global.ipv4_conf.global.ipmc_mode_enabled == *mode) {
            IPMC_CRIT_EXIT();
            return VTSS_OK;
        }

        break;
    case IPMC_IP_VERSION_MLD:
        if (ipmc_global.ipv6_conf.global.ipmc_mode_enabled == *mode) {
            IPMC_CRIT_EXIT();
            return VTSS_OK;
        }

        break;
    default:
        IPMC_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }

    if (ipmc_version == IPMC_IP_VERSION_IGMP) {
        ipmc_global.ipv4_conf.global.ipmc_mode_enabled = *mode;
    } else {
        ipmc_global.ipv6_conf.global.ipmc_mode_enabled = *mode;
    }

    IPMC_CRIT_EXIT();

    (void) ipmc_stacking_set_mode(VTSS_ISID_GLOBAL, ipmc_version, FALSE);
    (void) ipmc_stacking_set_unreg_flood(VTSS_ISID_GLOBAL, ipmc_version);
    ipmc_refresh_enable(ipmc_version);

    T_D("ipmc_mgmt_set_mode consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc ipmc_mgmt_get_mode(BOOL *mode, ipmc_ip_version_t ipmc_version)
{
    mesa_rc           rc = VTSS_RC_OK;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!mode) {
        return VTSS_RC_ERROR;
    }

    IPMC_CRIT_ENTER();
    switch (ipmc_version) {
    case IPMC_IP_VERSION_IGMP:
        *mode = ipmc_global.ipv4_conf.global.ipmc_mode_enabled;

        break;
    case IPMC_IP_VERSION_MLD:
        *mode = ipmc_global.ipv6_conf.global.ipmc_mode_enabled;

        break;
    default:
        rc = VTSS_RC_ERROR;

        break;
    }
    IPMC_CRIT_EXIT();

    T_D("ipmc_mgmt_get_mode consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return rc;
}

mesa_rc ipmc_mgmt_set_leave_proxy(BOOL *mode, ipmc_ip_version_t ipmc_version)
{
    vtss_tick_count_t exe_time_base = vtss_current_time();

    IPMC_CRIT_ENTER();

    switch (ipmc_version) {
    case IPMC_IP_VERSION_IGMP:
        if (ipmc_global.ipv4_conf.global.ipmc_leave_proxy_enabled == *mode) {
            IPMC_CRIT_EXIT();
            return VTSS_OK;
        }

        break;
    case IPMC_IP_VERSION_MLD:
        if (ipmc_global.ipv6_conf.global.ipmc_leave_proxy_enabled == *mode) {
            IPMC_CRIT_EXIT();
            return VTSS_OK;
        }

        break;
    default:
        IPMC_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }

    if (ipmc_version == IPMC_IP_VERSION_IGMP) {
        ipmc_global.ipv4_conf.global.ipmc_leave_proxy_enabled = *mode;
    } else {
        ipmc_global.ipv6_conf.global.ipmc_leave_proxy_enabled = *mode;
    }
    IPMC_CRIT_EXIT();

    (void)ipmc_stacking_set_leave_proxy(VTSS_ISID_GLOBAL, ipmc_version);

    T_D("ipmc_mgmt_set_leave_proxy consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc ipmc_mgmt_get_leave_proxy(BOOL *mode, ipmc_ip_version_t ipmc_version)
{
    mesa_rc           rc = VTSS_RC_OK;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!mode) {
        return VTSS_RC_ERROR;
    }

    IPMC_CRIT_ENTER();
    switch ( ipmc_version ) {
    case IPMC_IP_VERSION_IGMP:
        *mode = ipmc_global.ipv4_conf.global.ipmc_leave_proxy_enabled;

        break;
    case IPMC_IP_VERSION_MLD:
        *mode = ipmc_global.ipv6_conf.global.ipmc_leave_proxy_enabled;

        break;
    default:
        rc = VTSS_RC_ERROR;

        break;
    }
    IPMC_CRIT_EXIT();

    T_D("ipmc_mgmt_get_leave_proxy consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return rc;
}

mesa_rc ipmc_mgmt_set_proxy(BOOL *mode, ipmc_ip_version_t ipmc_version)
{
    vtss_tick_count_t exe_time_base = vtss_current_time();

    IPMC_CRIT_ENTER();

    switch ( ipmc_version ) {
    case IPMC_IP_VERSION_IGMP:
        if (ipmc_global.ipv4_conf.global.ipmc_proxy_enabled == *mode) {
            IPMC_CRIT_EXIT();
            return VTSS_OK;
        }

        break;
    case IPMC_IP_VERSION_MLD:
        if (ipmc_global.ipv6_conf.global.ipmc_proxy_enabled == *mode) {
            IPMC_CRIT_EXIT();
            return VTSS_OK;
        }

        break;
    default:
        IPMC_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }

    if (ipmc_version == IPMC_IP_VERSION_IGMP) {
        ipmc_global.ipv4_conf.global.ipmc_proxy_enabled = *mode;
    } else {
        ipmc_global.ipv6_conf.global.ipmc_proxy_enabled = *mode;
    }
    IPMC_CRIT_EXIT();

    (void)ipmc_stacking_set_proxy(VTSS_ISID_GLOBAL, ipmc_version);

    T_D("ipmc_mgmt_set_proxy consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc ipmc_mgmt_get_proxy(BOOL *mode, ipmc_ip_version_t ipmc_version)
{
    mesa_rc           rc = VTSS_RC_OK;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!mode) {
        return VTSS_RC_ERROR;
    }

    IPMC_CRIT_ENTER();
    switch ( ipmc_version ) {
    case IPMC_IP_VERSION_IGMP:
        *mode = ipmc_global.ipv4_conf.global.ipmc_proxy_enabled;

        break;
    case IPMC_IP_VERSION_MLD:
        *mode = ipmc_global.ipv6_conf.global.ipmc_proxy_enabled;

        break;
    default:
        rc = VTSS_RC_ERROR;

        break;
    }
    IPMC_CRIT_EXIT();

    T_D("ipmc_mgmt_get_proxy consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return rc;
}

mesa_rc ipmc_mgmt_set_unreg_flood(BOOL *mode, ipmc_ip_version_t ipmc_version)
{
    vtss_tick_count_t exe_time_base = vtss_current_time();

    IPMC_CRIT_ENTER();

    switch (ipmc_version) {
    case IPMC_IP_VERSION_IGMP:
        if (ipmc_global.ipv4_conf.global.ipmc_unreg_flood_enabled == *mode) {
            IPMC_CRIT_EXIT();
            return VTSS_OK;
        }

        break;
    case IPMC_IP_VERSION_MLD:
        if (ipmc_global.ipv6_conf.global.ipmc_unreg_flood_enabled == *mode) {
            IPMC_CRIT_EXIT();
            return VTSS_OK;
        }

        break;
    default:
        IPMC_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }

    if (ipmc_version == IPMC_IP_VERSION_IGMP) {
        ipmc_global.ipv4_conf.global.ipmc_unreg_flood_enabled = *mode;
    } else {
        ipmc_global.ipv6_conf.global.ipmc_unreg_flood_enabled = *mode;
    }
    IPMC_CRIT_EXIT();

    (void)ipmc_stacking_set_unreg_flood(VTSS_ISID_GLOBAL, ipmc_version);

    T_D("ipmc_mgmt_set_unreg_flood consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc ipmc_mgmt_get_unreg_flood(BOOL *mode, ipmc_ip_version_t ipmc_version)
{
    mesa_rc           rc = VTSS_RC_OK;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!mode) {
        return VTSS_RC_ERROR;
    }

    IPMC_CRIT_ENTER();
    switch (ipmc_version) {
    case IPMC_IP_VERSION_IGMP:
        *mode = ipmc_global.ipv4_conf.global.ipmc_unreg_flood_enabled;

        break;
    case IPMC_IP_VERSION_MLD:
        *mode = ipmc_global.ipv6_conf.global.ipmc_unreg_flood_enabled;

        break;
    default:
        rc = VTSS_RC_ERROR;

        break;
    }
    IPMC_CRIT_EXIT();

    T_D("ipmc_mgmt_get_unreg_flood consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return rc;
}

mesa_rc ipmc_mgmt_set_ssm_range(ipmc_ip_version_t ipmc_version, ipmc_prefix_t *range)
{
    vtss_tick_count_t exe_time_base = vtss_current_time();
    ipmc_prefix_t     prefix;

    if (!msg_switch_is_master()) {
        return VTSS_OK;
    }

    if (!range) {
        return VTSS_RC_ERROR;
    }

    IPMC_CRIT_ENTER();

    switch (ipmc_version) {
    case IPMC_IP_VERSION_IGMP:
        memset(range->addr.value.reserved, 0x0, sizeof(range->addr.value.reserved));
        if (!memcmp(&ipmc_global.ipv4_conf.global.ssm_range, range, sizeof(ipmc_prefix_t))) {
            IPMC_CRIT_EXIT();
            return VTSS_OK;
        }

        break;
    case IPMC_IP_VERSION_MLD:
        if (!memcmp(&ipmc_global.ipv6_conf.global.ssm_range, range, sizeof(ipmc_prefix_t))) {
            IPMC_CRIT_EXIT();
            return VTSS_OK;
        }

        break;
    default:
        IPMC_CRIT_EXIT();
        return VTSS_RC_ERROR;
    }

    IPMC_CRIT_EXIT();

    if (!ipmc_lib_prefix_maskingNchecking(ipmc_version, TRUE, range, &prefix)) {
        return VTSS_RC_ERROR;
    }

    IPMC_CRIT_ENTER();
    if (ipmc_version == IPMC_IP_VERSION_IGMP) {
        memcpy(&ipmc_global.ipv4_conf.global.ssm_range, &prefix, sizeof(ipmc_prefix_t));
    } else {
        memcpy(&ipmc_global.ipv6_conf.global.ssm_range, &prefix, sizeof(ipmc_prefix_t));
    }
    IPMC_CRIT_EXIT();

    (void) ipmc_stacking_set_ssm_range(VTSS_ISID_GLOBAL, ipmc_version);

    T_D("ipmc_mgmt_set_ssm_range consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return VTSS_OK;
}

mesa_rc ipmc_mgmt_get_ssm_range(ipmc_ip_version_t ipmc_version, ipmc_prefix_t *range)
{
    mesa_rc           rc = VTSS_RC_OK;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    if (!range) {
        return VTSS_RC_ERROR;
    }

    IPMC_CRIT_ENTER();
    switch (ipmc_version) {
    case IPMC_IP_VERSION_IGMP:
        memcpy(range, &ipmc_global.ipv4_conf.global.ssm_range, sizeof(ipmc_prefix_t));

        break;
    case IPMC_IP_VERSION_MLD:
        memcpy(range, &ipmc_global.ipv6_conf.global.ssm_range, sizeof(ipmc_prefix_t));

        break;
    default:
        rc = VTSS_RC_ERROR;

        break;
    }
    IPMC_CRIT_EXIT();

    T_D("ipmc_mgmt_get_ssm_range consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

    return rc;
}

/* Check whether default IPMC SSM Range is changed for not  */
BOOL ipmc_mgmt_def_ssm_range_chg(ipmc_ip_version_t version, ipmc_prefix_t *range)
{
    BOOL chg = FALSE;

    if (!range) {
        return chg;
    }

    switch (version) {
    case IPMC_IP_VERSION_IGMP:
        if (range->len != VTSS_IPMC_SSM4_RANGE_LEN) {
            chg = TRUE;
        } else {
            if (range->addr.value.prefix != VTSS_IPMC_SSM4_RANGE_PREFIX) {
                chg = TRUE;
            }
        }

        break;
    case IPMC_IP_VERSION_MLD:
        if (range->len != VTSS_IPMC_SSM6_RANGE_LEN) {
            chg = TRUE;
        } else {
            u8  idx;

            for (idx = 0; idx < 15; idx++) {
                if (idx == 0) {
                    if (range->addr.array.prefix.addr[0] != ((VTSS_IPMC_SSM6_RANGE_PREFIX >> 24) & 0xFF)) {
                        chg = TRUE;
                    }
                } else if (idx == 1) {
                    if (range->addr.array.prefix.addr[1] != ((VTSS_IPMC_SSM6_RANGE_PREFIX >> 16) & 0xFF)) {
                        chg = TRUE;
                    }
                } else if (idx == 2) {
                    if (range->addr.array.prefix.addr[2] != ((VTSS_IPMC_SSM6_RANGE_PREFIX >> 8) & 0xFF)) {
                        chg = TRUE;
                    }
                } else if (idx == 3) {
                    if (range->addr.array.prefix.addr[3] != ((VTSS_IPMC_SSM6_RANGE_PREFIX >> 0) & 0xFF)) {
                        chg = TRUE;
                    }
                } else {
                    if (range->addr.array.prefix.addr[idx]) {
                        chg = TRUE;
                    }
                }

                if (chg) {
                    break;
                }
            }
        }

        break;
    default:

        break;
    }

    return chg;
}

mesa_rc ipmc_mgmt_clear_stat_counter(vtss_isid_t isid, ipmc_ip_version_t ipmc_version, mesa_vid_t vid)
{
    mesa_rc           rc;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    rc = ipmc_stacking_clear_statistics(isid, ipmc_version, vid);

    T_D("ipmc_mgmt_clear_stat_counter consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));

    return rc;
}

static void ipmc_conf_default(ipmc_ip_version_t ipmc_version)
{
    int                  i;
    ipmc_conf_global_t   *global;
    ipmc_configuration_t *conf;

    IPMC_CRIT_ENTER();

    if (ipmc_version == IPMC_IP_VERSION_IGMP) {
        conf = &ipmc_global.ipv4_conf;
        vtss_clear(*conf);
        conf->version = IPMC_IP_VERSION_IGMP;
    } else if (ipmc_version == IPMC_IP_VERSION_MLD) {
        conf = &ipmc_global.ipv6_conf;
        vtss_clear(*conf);
        conf->version = IPMC_IP_VERSION_MLD;
    } else {
        IPMC_CRIT_EXIT();
        return;
    }

    global = &conf->global;
    global->ipmc_mode_enabled = IPMC_DEF_GLOBAL_STATE_VALUE;
    global->ipmc_unreg_flood_enabled = IPMC_DEF_UNREG_FLOOD_VALUE;
    global->ipmc_leave_proxy_enabled = IPMC_DEF_LEAVE_PROXY_VALUE;
    global->ipmc_proxy_enabled = IPMC_DEF_PROXY_VALUE;
    if (ipmc_version == IPMC_IP_VERSION_IGMP) {
        global->ssm_range.addr.value.prefix = VTSS_IPMC_SSM4_RANGE_PREFIX;
        global->ssm_range.len = VTSS_IPMC_SSM4_RANGE_LEN;
    } else {
        global->ssm_range.addr.array.prefix.addr[0] = (VTSS_IPMC_SSM6_RANGE_PREFIX >> 24) & 0xFF;
        global->ssm_range.addr.array.prefix.addr[1] = (VTSS_IPMC_SSM6_RANGE_PREFIX >> 16) & 0xFF;
        global->ssm_range.addr.array.prefix.addr[2] = (VTSS_IPMC_SSM6_RANGE_PREFIX >> 8) & 0xFF;
        global->ssm_range.addr.array.prefix.addr[3] = (VTSS_IPMC_SSM6_RANGE_PREFIX >> 0) & 0xFF;
        global->ssm_range.len = VTSS_IPMC_SSM6_RANGE_LEN;
    }

    for (i = 0; i < IPMC_VLAN_MAX; i++) {
        _ipmc_reset_conf_intf(&conf->ipmc_conf_intf_entries[i], FALSE, 0x0);
    }

    IPMC_CRIT_EXIT();
}

void ipmc_port_state_change_callback(mesa_port_no_t port_no, port_info_t *info)
{
    vtss_tick_count_t exe_time_base = vtss_current_time();

    IPMC_CRIT_ENTER();
    vtss_ipmc_port_state_change_handle(port_no, info);
    IPMC_CRIT_EXIT();

    T_D("ipmc_port_state_change_callback consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));
}

void ipmc_stp_port_state_change_callback(vtss_common_port_t l2port, vtss_common_stpstate_t new_state)
{
    switch_iter_t                      sit;
    vtss_isid_t                        isid, l2_isid;
    mesa_port_no_t                     iport;
    ipmc_msg_buf_t                     buf;
    ipmc_msg_stp_port_change_set_req_t *msg;
    BOOL                               v4_mode, v6_mode;
    BOOL                               v4_proxy, v6_proxy;
    BOOL                               v4_rtr_port, v6_rtr_port;
    ipmc_ip_version_t                  ipmc_version = IPMC_IP_VERSION_INIT;
    vtss_tick_count_t                  exe_time_base = vtss_current_time();

    /* Only process it when master and forwarding state */
    if (!msg_switch_is_master() || (new_state != VTSS_COMMON_STPSTATE_FORWARDING)) {
        return;
    }

    IPMC_CRIT_ENTER();
    /* MODE */
    v4_mode = (ipmc_global.ipv4_conf.global.ipmc_mode_enabled == VTSS_IPMC_ENABLE);
    v6_mode = (ipmc_global.ipv6_conf.global.ipmc_mode_enabled == VTSS_IPMC_ENABLE);
    IPMC_CRIT_EXIT();

    /* Continue only when IGMP/MLD SNP administration enabled. */
    if (!v4_mode && !v6_mode) {
        return;
    }

    isid = VTSS_ISID_GLOBAL;
    T_D("Convert %s to isid and iport", l2port2str(l2port));
    if (!l2port2port(l2port, &isid, &iport)) {
        T_D("ipmc_stp_port_state_change_callback(%s): Ignore non-STP L2 logical interface", l2port2str(l2port));
        return;
    }

    if (!IPMC_LIB_ISID_VALID(isid)) {
        T_D("Failed to get valid ISID(%d) on %s", isid, l2port2str(l2port));
        return;
    }
    if (!IPMC_LIB_ISID_CHECK(isid)) {
        T_D("Failed to translate ISID(%d) on %s", isid, l2port2str(l2port));
        return;
    }
    l2_isid = isid;

    /*
        If global/proxy mode are enabled and it isn't static router port,
        sendout a specfic query for collecting the group information.
        Note: We should filter sendout process if this port belong to
        dynamic router port.  Check it when receiving the message on each switch.
    */
    IPMC_CRIT_ENTER();
    /* PROXY */
    v4_proxy = (ipmc_global.ipv4_conf.global.ipmc_proxy_enabled == VTSS_IPMC_ENABLE);
    v6_proxy = (ipmc_global.ipv6_conf.global.ipmc_proxy_enabled == VTSS_IPMC_ENABLE);
    /* ROUTER-PORT */
    if ((isid == VTSS_ISID_LOCAL) || (isid == VTSS_ISID_GLOBAL)) {
        v4_rtr_port = ipmc_global.ipv4_conf.ipmc_router_ports[msg_master_isid() - VTSS_ISID_START][iport];
        v6_rtr_port = ipmc_global.ipv6_conf.ipmc_router_ports[msg_master_isid() - VTSS_ISID_START][iport];
    } else {
        v4_rtr_port = ipmc_global.ipv4_conf.ipmc_router_ports[isid - VTSS_ISID_START][iport];
        v6_rtr_port = ipmc_global.ipv6_conf.ipmc_router_ports[isid - VTSS_ISID_START][iport];
    }
    IPMC_CRIT_EXIT();

    if (v4_mode && v4_proxy && !v4_rtr_port) {
        ipmc_version = IPMC_IP_VERSION_IGMP;
    }

    if (v6_mode && v6_proxy && !v6_rtr_port) {
        if (ipmc_version != IPMC_IP_VERSION_INIT) {
            ipmc_version = IPMC_IP_VERSION_ALL;
        } else {
            ipmc_version = IPMC_IP_VERSION_MLD;
        }
    }

    if (ipmc_version != IPMC_IP_VERSION_INIT) {
        (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID);
        while (switch_iter_getnext(&sit)) {
            isid = sit.isid;
            if (!IPMC_LIB_ISID_PASS(l2_isid, isid)) {
                continue;
            }

            ipmc_msg_alloc(isid, IPMC_MSG_ID_STP_PORT_CHANGE_REQ, &buf, FALSE, TRUE);
            msg = (ipmc_msg_stp_port_change_set_req_t *) buf.msg;

            msg->msg_id = IPMC_MSG_ID_STP_PORT_CHANGE_REQ;
            msg->port = iport;
            msg->new_state = new_state;
            msg->version = ipmc_version;
            ipmc_msg_tx(&buf, isid, sizeof(*msg));
        }
    }

    T_D("ipmc_stp_port_state_change_callback consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));
}

static BOOL ipmcReady = FALSE;
static BOOL ipmc_sm_thread_suspend = FALSE;
static BOOL ipmc_sm_thread_done_init = FALSE;

void ipmc_sm_thread(vtss_addrword_t data)
{
    vtss_flag_value_t    events;
#if IPMC_THREAD_EXE_SUPP
    vtss_tick_count_t    last_exe_time;
#endif /* IPMC_THREAD_EXE_SUPP */
    ipmc_time_t          exe_time_base, exe_time_diff;
    switch_iter_t        sit;
    port_iter_t          pit;
    BOOL                 i_am_suspend, sync_mgmt_flag, sync_mgmt_done, proxy_done, ipmc_link = FALSE;
    u32                  idx, ticks = 0, ticks_overflow = 0, delay_cnt = 0;
    ipmc_lib_mgmt_info_t info, *sys_mgmt = &info;

    T_D("enter ipmc_sm_thread");

    // Wait until INIT_CMD_INIT is complete.
    msg_wait(MSG_WAIT_UNTIL_INIT_DONE, VTSS_MODULE_ID_IPMC);

    IPMC_CRIT_ENTER();
    vtss_flag_init(&ipmc_global.vlan_entry_flags);
    vtss_flag_init(&ipmc_global.grp_entry_flags);
    vtss_flag_init(&ipmc_global.grp_nxt_entry_flags);
    vtss_flag_init(&ipmc_global.dynamic_router_ports_getting_flags);

    /* Initialize running data structure */
    vtss_ipmc_init();
    /* Initialize MSG-RX */
    (void) ipmc_stacking_register();
    IPMC_CRIT_EXIT();

    /* wait for IP task/stack is ready */
    T_I("ipmc_sm_thread init delay start");
    while (!ipmc_link && (delay_cnt < IPMC_THREAD_START_DELAY_CNT)) {

        if (delay_cnt > IPMC_THREAD_MIN_DELAY_CNT) {
            IPMC_CRIT_ENTER();
            (void) port_iter_init(&pit, NULL, VTSS_ISID_LOCAL, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_ALL);
            while (port_iter_getnext(&pit)) {
                if (ipmc_port_status[VTSS_ISID_LOCAL][pit.iport]) {
                    ipmc_link = TRUE;
                    break;
                }
            }
            IPMC_CRIT_EXIT();
        }

        if (!ipmc_link) {
            VTSS_OS_MSLEEP(IPMC_THREAD_SECOND_DEF);
            delay_cnt++;
        }
    }
    T_I("ipmc_sm_thread init delay done");

    /* Initialize Periodical Wakeup Timer  */
    ipmc_sm_timer.set_repeat(true);
    ipmc_sm_timer.set_period(vtss::milliseconds(IPMC_THREAD_TICK_TIME));
    ipmc_sm_timer.callback = ipmc_sm_timer_isr;
    ipmc_sm_timer.modid = VTSS_MODULE_ID_IPMC;
    if (vtss_timer_start(&ipmc_sm_timer) != VTSS_RC_OK) {
        T_D("vtss_timer_start failed");
    }

    T_D("ipmc_sm_thread start");
    ipmcReady = TRUE;
    sync_mgmt_flag = TRUE;
    IPMC_CRIT_ENTER();
    i_am_suspend = ipmc_sm_thread_suspend;
    ipmc_sm_thread_done_init = ipmcReady;
    IPMC_CRIT_EXIT();

#if IPMC_THREAD_EXE_SUPP
    last_exe_time = vtss_current_time();
#endif /* IPMC_THREAD_EXE_SUPP */
    while (ipmcReady && sys_mgmt) {
        events = vtss_flag_wait(&ipmc_sm_events, IPMC_EVENT_ANY, VTSS_FLAG_WAITMODE_OR_CLR);

        if (events & IPMC_EVENT_MASTER_UP) {
            sync_mgmt_flag = TRUE;
        }

        if (events & IPMC_EVENT_MASTER_DOWN) {
            sync_mgmt_flag = FALSE;
        }

        if (events & IPMC_EVENT_PKT4_HANDLER) {
            BOOL v4_mode;

            IPMC_CRIT_ENTER();
            v4_mode = ipmc_global.ipv4_conf.global.ipmc_mode_enabled;
            IPMC_CRIT_EXIT();

            if (v4_mode) {
                (void)ipmc_lib_packet_register(IPMC_OWNER_SNP4, ipmcsnp_rx_packet_callback);
            } else {
                (void)ipmc_lib_packet_unregister(IPMC_OWNER_SNP4);
            }
        }

        if (events & IPMC_EVENT_PKT6_HANDLER) {
            BOOL v6_mode;

            IPMC_CRIT_ENTER();
            v6_mode = ipmc_global.ipv6_conf.global.ipmc_mode_enabled;
            IPMC_CRIT_EXIT();

            if (v6_mode) {
                (void)ipmc_lib_packet_register(IPMC_OWNER_SNP6, ipmcsnp_rx_packet_callback);
            } else {
                (void)ipmc_lib_packet_unregister(IPMC_OWNER_SNP6);
            }
        }

        if (events & IPMC_EVENT_SWITCH_DEL) {
            vtss_isid_t isid_del;

            T_D("IPMC_EVENT_SWITCH_DEL");

            (void)ipmc_lib_time_curr_get(&exe_time_base);

            IPMC_CRIT_ENTER();

            (void)switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID);
            while (switch_iter_getnext(&sit)) {
                isid_del = sit.isid;

                if (!(ipmc_switch_event_value[ipmc_lib_isid_convert(TRUE, isid_del)] & IPMC_EVENT_VALUE_SW_DEL)) {
                    continue;
                }

                ipmc_switch_event_value[ipmc_lib_isid_convert(TRUE, isid_del)] &= ~IPMC_EVENT_VALUE_SW_DEL;
                if (ipmc_lib_isid_is_local(isid_del)) {
                    ipmc_switch_event_value[VTSS_ISID_LOCAL] &= ~IPMC_EVENT_VALUE_SW_DEL;
                }
            }

            IPMC_CRIT_EXIT();

            (void)ipmc_lib_time_diff_get(TRUE, FALSE, "IPMC_EVENT_SWITCH_DEL", &exe_time_base, &exe_time_diff);
        }

        if (events & IPMC_EVENT_SWITCH_ADD) {
            vtss_isid_t isid_add;
            u32         isid_set;

            T_D("IPMC_EVENT_SWITCH_ADD");

            (void)ipmc_lib_time_curr_get(&exe_time_base);

            sync_mgmt_done = FALSE;
            if (!ipmc_lib_system_mgmt_info_cpy(sys_mgmt)) {
                sync_mgmt_done = TRUE;  /* Not Ready Yet! */
            }

            (void)switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID);
            while (switch_iter_getnext(&sit)) {
                isid_add = sit.isid;

                IPMC_CRIT_ENTER();
                isid_set = ipmc_switch_event_value[ipmc_lib_isid_convert(TRUE, isid_add)] & IPMC_EVENT_VALUE_SW_ADD;
                IPMC_CRIT_EXIT();

                if (isid_set == 0) {
                    continue;
                }

                /* IGMP */
                (void)ipmc_stacking_set_leave_proxy(isid_add, IPMC_IP_VERSION_IGMP);
                (void)ipmc_stacking_set_proxy(isid_add, IPMC_IP_VERSION_IGMP);
                (void)ipmc_stacking_set_router_port(isid_add, IPMC_IP_VERSION_IGMP);
                (void)ipmc_stacking_set_fastleave_port(isid_add, IPMC_IP_VERSION_IGMP);
                (void)ipmc_stacking_set_ssm_range(isid_add, IPMC_IP_VERSION_IGMP);
                ipmc_update_vlan_tab(isid_add, IPMC_IP_VERSION_IGMP);
                (void)ipmc_stacking_set_throttling_number(isid_add, IPMC_IP_VERSION_IGMP);
                (void)ipmc_stacking_set_grp_filtering(isid_add, IPMC_IP_VERSION_IGMP);
                (void)ipmc_stacking_set_mode(isid_add, IPMC_IP_VERSION_IGMP, FALSE);
                (void)ipmc_stacking_set_unreg_flood(isid_add, IPMC_IP_VERSION_IGMP);
                /* MLD */
                (void)ipmc_stacking_set_leave_proxy(isid_add, IPMC_IP_VERSION_MLD);
                (void)ipmc_stacking_set_proxy(isid_add, IPMC_IP_VERSION_MLD);
                (void)ipmc_stacking_set_router_port(isid_add, IPMC_IP_VERSION_MLD);
                (void)ipmc_stacking_set_fastleave_port(isid_add, IPMC_IP_VERSION_MLD);
                (void)ipmc_stacking_set_ssm_range(isid_add, IPMC_IP_VERSION_MLD);
                ipmc_update_vlan_tab(isid_add, IPMC_IP_VERSION_MLD);
                (void)ipmc_stacking_set_throttling_number(isid_add, IPMC_IP_VERSION_MLD);
                (void)ipmc_stacking_set_grp_filtering(isid_add, IPMC_IP_VERSION_MLD);
                (void)ipmc_stacking_set_mode(isid_add, IPMC_IP_VERSION_MLD, FALSE);
                (void)ipmc_stacking_set_unreg_flood(isid_add, IPMC_IP_VERSION_MLD);

                if (!sync_mgmt_done) {
                    (void)ipmc_stacking_sync_mgmt_conf(isid_add, sys_mgmt);
                }

                IPMC_CRIT_ENTER();
                ipmc_switch_event_value[ipmc_lib_isid_convert(TRUE, isid_add)] &= ~IPMC_EVENT_VALUE_SW_ADD;
                if (ipmc_lib_isid_is_local(isid_add)) {
                    ipmc_switch_event_value[VTSS_ISID_LOCAL] &= ~IPMC_EVENT_VALUE_SW_ADD;
                }
                IPMC_CRIT_EXIT();
            }

            (void)ipmc_lib_time_diff_get(TRUE, FALSE, "IPMC_EVENT_SWITCH_ADD", &exe_time_base, &exe_time_diff);
        }

        if (events & IPMC_EVENT_THREAD_SUSPEND) {
            IPMC_CRIT_ENTER();
            i_am_suspend = ipmc_sm_thread_suspend = TRUE;
            IPMC_CRIT_EXIT();
        }

        if (events & IPMC_EVENT_THREAD_RESUME) {
            IPMC_CRIT_ENTER();
            i_am_suspend = ipmc_sm_thread_suspend = FALSE;
            IPMC_CRIT_EXIT();
        }

        if (i_am_suspend) {
            if (events & IPMC_EVENT_SM_TIME_WAKEUP) {
                (void) ipmc_lib_calc_thread_tick(&ticks, 1, IPMC_THREAD_TIME_UNIT_BASE, &ticks_overflow);
            }

            continue;
        }

        sync_mgmt_done = proxy_done = FALSE;
        if (events & IPMC_EVENT_SM_TIME_WAKEUP) {
            u8  exe_round = 1;
#if IPMC_THREAD_EXE_SUPP
            u32 supply_ticks = 0, diff_exe_time = ipmc_lib_diff_u32_wrap_around((u32)last_exe_time, (u32)vtss_current_time());

            if (VTSS_OS_TICK2MSEC(diff_exe_time) > (2 * IPMC_THREAD_TICK_TIME)) {
                supply_ticks = VTSS_OS_TICK2MSEC(diff_exe_time) / IPMC_THREAD_TICK_TIME - 1;
            }

            if (supply_ticks > 0) {
                exe_round += ipmc_lib_calc_thread_tick(&ticks, supply_ticks, IPMC_THREAD_TIME_UNIT_BASE, &ticks_overflow);
            }

            last_exe_time = vtss_current_time();
#endif /* IPMC_THREAD_EXE_SUPP */

            if (msg_switch_is_master() && ipmc_lib_system_mgmt_info_chg(sys_mgmt)) {
                sync_mgmt_done = (ipmc_stacking_sync_mgmt_conf(VTSS_ISID_GLOBAL, sys_mgmt) == VTSS_OK);
            }

            if (!(ticks % IPMC_THREAD_TIME_UNIT_BASE)) {
                for (; exe_round > 0; exe_round--) {
                    if (msg_switch_is_master() && sync_mgmt_flag) {
                        if (!sync_mgmt_done) {
                            if (ipmc_lib_system_mgmt_info_cpy(sys_mgmt)) {
                                sync_mgmt_flag = (ipmc_stacking_sync_mgmt_conf(VTSS_ISID_GLOBAL, sys_mgmt) != VTSS_OK);
                            }
                        } else {
                            sync_mgmt_flag = FALSE;
                        }
                    }

                    T_N("ipmc_sm_thread: %dMSEC-TICKS(%u | %u)", IPMC_THREAD_TICK_TIME, ticks_overflow, ticks);

                    IPMC_CRIT_ENTER();
                    (void) ipmc_lib_time_curr_get(&exe_time_base);

                    vtss_ipmc_tick_gen();

                    if (ipmc_global.ipv4_conf.global.ipmc_mode_enabled ||
                        ipmc_global.ipv6_conf.global.ipmc_mode_enabled) {
                        for (idx = 0; idx < SNP_NUM_OF_SUPPORTED_INTF; idx++) {
                            vtss_ipmc_tick_intf_tmr(idx);
                        }

                        vtss_ipmc_tick_intf_rxmt();
                        vtss_ipmc_tick_group_tmr();
                    }

                    if (events & IPMC_EVENT_PROXY_LOCAL) {
                        vtss_ipmc_tick_proxy(TRUE);
                    } else {
                        vtss_ipmc_tick_proxy(FALSE);
                    }
                    proxy_done = TRUE;

                    (void) ipmc_lib_time_diff_get(FALSE, FALSE, NULL, &exe_time_base, &exe_time_diff);
                    IPMC_CRIT_EXIT();

                    T_N("IPMC_EVENT_SM_TIME_WAKEUP consumes %u.%um%uu",
                        exe_time_diff.sec, exe_time_diff.msec, exe_time_diff.usec);
                }
            }

            (void) ipmc_lib_calc_thread_tick(&ticks, 1, IPMC_THREAD_TIME_UNIT_BASE, &ticks_overflow);
        }

        if (events & IPMC_EVENT_PROXY_LOCAL) {
            if (!proxy_done) {
                IPMC_CRIT_ENTER();
                vtss_ipmc_tick_proxy(TRUE);
                IPMC_CRIT_EXIT();
            }
        }
    }

    ipmcReady = FALSE;
    T_W("exit ipmc_sm_thread");
}

mesa_rc ipmc_mgmt_sm_thread_resume(void)
{
    BOOL    i_can_go;

    IPMC_CRIT_ENTER();
    i_can_go = ipmc_sm_thread_suspend;
    IPMC_CRIT_EXIT();

    if (!i_can_go) {
        return VTSS_RC_OK;
    }

    i_can_go = FALSE;
    while (!i_can_go) {
        IPMC_CRIT_ENTER();
        i_can_go = ipmc_sm_thread_done_init;
        IPMC_CRIT_EXIT();

        if (!i_can_go) {
            VTSS_OS_MSLEEP(333);
        }
    }

    ipmc_sm_event_set(IPMC_EVENT_THREAD_RESUME);
    return VTSS_RC_OK;
}

mesa_rc ipmc_mgmt_sm_thread_suspend(void)
{
    BOOL    i_can_go;

    IPMC_CRIT_ENTER();
    i_can_go = ipmc_sm_thread_suspend;
    IPMC_CRIT_EXIT();

    if (i_can_go) {
        return VTSS_RC_OK;
    }

    while (!i_can_go) {
        IPMC_CRIT_ENTER();
        i_can_go = ipmc_sm_thread_done_init;
        IPMC_CRIT_EXIT();

        if (!i_can_go) {
            VTSS_OS_MSLEEP(333);
        }
    }

    ipmc_sm_event_set(IPMC_EVENT_THREAD_SUSPEND);
    i_can_go = FALSE;
    while (!i_can_go) {
        IPMC_CRIT_ENTER();
        i_can_go = ipmc_sm_thread_suspend;
        IPMC_CRIT_EXIT();

        if (!i_can_go) {
            VTSS_OS_MSLEEP(333);
        }
    }

    return VTSS_RC_OK;
}

/* Callback to let the IP module notify us when vlan interfaces are added/deleted. */
void ipmc_ip_vlan_interface_callback(vtss_if_id_vlan_t vid)
{
    // Depending on whether the vlan interface should exist, we add or delete it,
    // exploiting that the add/delete functions do handle adding existing/deleting non-existing
    // vlan interfaces correctly.
    IPMC_CRIT_ENTER();
    if (vtss_ip_if_exists(vid)) {
        if (_ipmc_conf_intf_entry_add((mesa_vid_t)vid, IPMC_IP_VERSION_IGMP) == IPMC_VID_INIT) {
            T_D("_ipmc_conf_intf_entry_add vid:%d IGMP failed to create VLAN config.", vid);
        }
        if (_ipmc_conf_intf_entry_add((mesa_vid_t)vid, IPMC_IP_VERSION_MLD) == IPMC_VID_INIT) {
            T_D("_ipmc_conf_intf_entry_add vid:%d MLD failed to create VLAN config.", vid);
        }
    } else {
        if (_ipmc_conf_intf_entry_del((mesa_vid_t)vid, IPMC_IP_VERSION_IGMP) == FALSE) {
            T_D("_ipmc_conf_intf_entry_del vid:%d IGMP failed to delete VLAN config.", vid);
        }
        if (_ipmc_conf_intf_entry_del((mesa_vid_t)vid, IPMC_IP_VERSION_MLD) == FALSE) {
            T_D("_ipmc_conf_intf_entry_del vid:%d MLD failed to delete VLAN config.", vid);
        }
    }
    IPMC_CRIT_EXIT();
}

/****************************************************************************/
/*  Initialization functions                                                */
/****************************************************************************/
#if defined(VTSS_SW_OPTION_PRIVATE_MIB) && defined(VTSS_SW_OPTION_SMB_IPMC)
VTSS_PRE_DECLS void ipmc_snooping_mib_init(void);
#endif /* VTSS_SW_OPTION_PRIVATE_MIB && VTSS_SW_OPTION_SMB_IPMC */
#if defined(VTSS_SW_OPTION_JSON_RPC)
VTSS_PRE_DECLS void vtss_appl_ipmc_snp_json_init(void);
#endif
extern "C" int ipmc_snp_igmp_icli_cmd_register();
extern "C" int ipmc_snp_mld_icli_cmd_register();

mesa_rc ipmc_init(vtss_init_data_t *data)
{
    ipmc_msg_id_t     idx;
    vtss_isid_t       isid = data->isid;
    vtss_tick_count_t exe_time_base = vtss_current_time();

    switch (data->cmd) {
    case INIT_CMD_EARLY_INIT:
        /* Initialize and register trace ressources */
        VTSS_TRACE_REG_INIT(&snp_trace_reg, snp_trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&snp_trace_reg);
        break;

    case INIT_CMD_INIT:
        for (idx = IPMC_MSG_ID_MODE_SET_REQ; idx < IPMC_MSG_MAX_ID; idx++) {
            switch ( idx ) {
            case IPMC_MSG_ID_MODE_SET_REQ:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_mode_set_req_t);
                break;
            case IPMC_MSG_ID_LEAVE_PROXY_SET_REQ:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_leave_proxy_set_req_t);
                break;
            case IPMC_MSG_ID_PROXY_SET_REQ:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_proxy_set_req_t);
                break;
            case IPMC_MSG_ID_SSM_RANGE_SET_REQ:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_ssm_range_set_req_t);
                break;
            case IPMC_MSG_ID_UNREG_FLOOD_SET_REQ:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_unreg_flood_set_req_t);
                break;
            case IPMC_MSG_ID_ROUTER_PORT_SET_REQ:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_router_port_set_req_t);
                break;
            case IPMC_MSG_ID_FAST_LEAVE_PORT_SET_REQ:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_fast_leave_port_set_req_t);
                break;
            case IPMC_MSG_ID_THROLLTING_MAX_NO_SET_REQ:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_throllting_max_no_set_req_t);
                break;
            case IPMC_MSG_ID_VLAN_SET_REQ:
            case IPMC_MSG_ID_VLAN_ENTRY_SET_REQ:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_vlan_set_req_t);
                break;
            case IPMC_MSG_ID_STAT_COUNTER_CLEAR_REQ:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_stat_counter_clear_req_t);
                break;
            case IPMC_MSG_ID_STP_PORT_CHANGE_REQ:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_stp_port_change_set_req_t);
                break;
            case IPMC_MSG_ID_VLAN_ENTRY_GET_REQ:
            case IPMC_MSG_ID_VLAN_GROUP_ENTRY_WALK_REQ:
            case IPMC_MSG_ID_GROUP_SRCLIST_WALK_REQ:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_vlan_entry_get_req_t);
                break;
            case IPMC_MSG_ID_GROUP_ENTRY_GET_REQ:
            case IPMC_MSG_ID_GROUP_ENTRY_GETNEXT_REQ:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_group_entry_itr_req_t);
                break;
            case IPMC_MSG_ID_VLAN_ENTRY_GET_REP:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_vlan_entry_get_rep_t);
                break;
            case IPMC_MSG_ID_VLAN_GROUP_ENTRY_WALK_REP:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_vlan_group_entry_get_rep_t);
                break;
            case IPMC_MSG_ID_GROUP_ENTRY_GET_REP:
            case IPMC_MSG_ID_GROUP_ENTRY_GETNEXT_REP:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_group_entry_itr_rep_t);
                break;
            case IPMC_MSG_ID_GROUP_SRCLIST_WALK_REP:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_group_srclist_get_rep_t);
                break;
            case IPMC_MSG_ID_DYNAMIC_ROUTER_PORTS_GET_REQ:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_dyn_rtpt_get_req_t);
                break;
            case IPMC_MSG_ID_DYNAMIC_ROUTER_PORTS_GET_REP:
                ipmc_global.msize[idx] = sizeof(ipmc_msg_dynamic_router_ports_get_rep_t);
                break;
            case IPMC_MSG_ID_PORT_GROUP_FILTERING_SET_REQ:
            default:
                /* Give the MAX */
                ipmc_global.msize[idx] = sizeof(ipmc_msg_port_group_filtering_set_req_t);
                break;
            }

            VTSS_MALLOC_CAST(ipmc_global.msg[idx], ipmc_global.msize[idx]);
            if (ipmc_global.msg[idx] == NULL) {
                T_W("IPMC_ASSERT(INIT_CMD_INIT)");
                for (;;) {}
            }
        }

        /* Create semaphore for critical regions */
        critd_init(&ipmc_global.crit, "IPMC_global.crit", VTSS_MODULE_ID_IPMC, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
        IPMC_CRIT_EXIT();

        critd_init(&ipmc_global.get_crit, "IPMC_global.get_crit", VTSS_MODULE_ID_IPMC, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
        IPMC_GET_CRIT_EXIT();

        (void) ipmc_lib_common_init();
        /* Initialized RX semaphore */
        (void) ipmc_lib_packet_init();
        /* Subscribe to VLAN-inteface changes. */
        VTSS_RC(vtss_ip_if_callback_add(ipmc_ip_vlan_interface_callback));

#ifdef VTSS_SW_OPTION_ICFG
        if (ipmc_snp_icfg_init() != VTSS_OK) {
            T_E("ipmc_snp_icfg_init failed!");
        }
#endif /* VTSS_SW_OPTION_ICFG */

#if defined(VTSS_SW_OPTION_PRIVATE_MIB) && defined(VTSS_SW_OPTION_SMB_IPMC)
        ipmc_snooping_mib_init();   /* Register IPMC Snooping private mib */
#endif
#if defined(VTSS_SW_OPTION_JSON_RPC)
        vtss_appl_ipmc_snp_json_init();
#endif
#if defined(VTSS_SW_OPTION_IPMC)
        ipmc_snp_igmp_icli_cmd_register();
#endif
#if defined(VTSS_SW_OPTION_SMB_IPMC)
        ipmc_snp_mld_icli_cmd_register();
#endif

        vlan_membership_change_register(VTSS_MODULE_ID_IPMC, ipmc_vlan_changed);

        /* Initialize message buffers */
        vtss_sem_init(&ipmc_global.request.sem, 1);
        vtss_sem_init(&ipmc_global.reply.sem, 1);

        memset(ipmc_switch_event_value, 0x0, sizeof(ipmc_switch_event_value));

        /* Initialize IPMC-EVENT groups */
        vtss_flag_init(&ipmc_sm_events);
#if 0 /* etliang */
        vtss_flag_init(&ipmc_db_events);
#endif /* etliang */

        vtss_clear(ipmc_port_status);

        vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
                           ipmc_sm_thread,
                           0,
                           "IPMC_SNP",
                           nullptr,
                           0,
                           &ipmc_sm_thread_handle,
                           &ipmc_sm_thread_block);

        T_I("IPMC-INIT_CMD_INIT consumes ID%d:%u ticks", isid, (u32)(vtss_current_time() - exe_time_base));

        break;

    case INIT_CMD_START:
        T_I("START: ISID->%d", isid);

        (void)ipmc_lib_packet_resume();
        (void)port_change_register(VTSS_MODULE_ID_IPMC, ipmc_port_state_change_callback);
        (void)l2_stp_state_change_register(ipmc_stp_port_state_change_callback);
        /* Register for Port GLOBAL change callback */
        (void)port_global_change_register(VTSS_MODULE_ID_IPMC, ipmc_global_port_state_change_callback);

        T_D("IPMC-INIT_CMD_START consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

        break;

    case INIT_CMD_CONF_DEF:
        T_I("CONF_DEF: ISID->%d", isid);

        if (isid == VTSS_ISID_GLOBAL) {
            BOOL                    snp4_enable, snp6_enable;
            mesa_vid_t              intf_idx;
            ipmc_port_bfs_t         vlan_ports;
            ipmc_conf_intf_entry_t  ipmc_intf, *entry;

            for (intf_idx = 0; intf_idx < IPMC_VLAN_MAX; intf_idx++) {
                IPMC_CRIT_ENTER();
                entry = &ipmc_global.ipv4_conf.ipmc_conf_intf_entries[intf_idx];
                if (!entry->valid) {
                    IPMC_CRIT_EXIT();
                    continue;
                }
                if (vtss_ipmc_get_intf_entry(entry->vid, IPMC_IP_VERSION_IGMP) == NULL) {
                    IPMC_CRIT_EXIT();
                    continue;
                }
                memcpy(&ipmc_intf, entry, sizeof(ipmc_conf_intf_entry_t));
                IPMC_CRIT_EXIT();

                memset(&vlan_ports, 0x0, sizeof(ipmc_port_bfs_t));
                (void) ipmc_stacking_set_intf(VTSS_ISID_GLOBAL, IPMC_OP_DEL, &ipmc_intf, &vlan_ports, IPMC_IP_VERSION_IGMP);
            }

            for (intf_idx = 0; intf_idx < IPMC_VLAN_MAX; intf_idx++) {
                IPMC_CRIT_ENTER();
                entry = &ipmc_global.ipv6_conf.ipmc_conf_intf_entries[intf_idx];
                if (!entry->valid) {
                    IPMC_CRIT_EXIT();
                    continue;
                }
                if (vtss_ipmc_get_intf_entry(entry->vid, IPMC_IP_VERSION_MLD) == NULL) {
                    IPMC_CRIT_EXIT();
                    continue;
                }
                memcpy(&ipmc_intf, entry, sizeof(ipmc_conf_intf_entry_t));
                IPMC_CRIT_EXIT();

                memset(&vlan_ports, 0x0, sizeof(ipmc_port_bfs_t));
                (void) ipmc_stacking_set_intf(VTSS_ISID_GLOBAL, IPMC_OP_DEL, &ipmc_intf, &vlan_ports, IPMC_IP_VERSION_MLD);
            }

            /* Turn-Off protocol anyway */
            (void) ipmc_stacking_set_mode(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_IGMP, TRUE);
            (void) ipmc_stacking_set_mode(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_MLD, TRUE);

            /* Reset stack configuration */
            ipmc_conf_default(IPMC_IP_VERSION_IGMP);
            ipmc_conf_default(IPMC_IP_VERSION_MLD);

            /* Turn-On protocol, if necessary */
            IPMC_CRIT_ENTER();
            snp4_enable = ipmc_global.ipv4_conf.global.ipmc_mode_enabled;
            snp6_enable = ipmc_global.ipv6_conf.global.ipmc_mode_enabled;
            IPMC_CRIT_EXIT();

            /* IGMP */
            (void) ipmc_stacking_set_leave_proxy(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_IGMP);
            (void) ipmc_stacking_set_proxy(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_IGMP);
            (void) ipmc_stacking_set_ssm_range(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_IGMP);
            (void) ipmc_stacking_set_router_port(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_IGMP);
            (void) ipmc_stacking_set_fastleave_port(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_IGMP);
            (void) ipmc_stacking_set_throttling_number(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_IGMP);
            (void) ipmc_stacking_set_grp_filtering(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_IGMP);
            if (snp4_enable) {
                (void) ipmc_stacking_set_mode(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_IGMP, FALSE);
            }
            (void) ipmc_stacking_set_unreg_flood(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_IGMP);
            /* MLD */
            (void) ipmc_stacking_set_leave_proxy(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_MLD);
            (void) ipmc_stacking_set_proxy(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_MLD);
            (void) ipmc_stacking_set_ssm_range(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_MLD);
            (void) ipmc_stacking_set_router_port(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_MLD);
            (void) ipmc_stacking_set_fastleave_port(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_MLD);
            (void) ipmc_stacking_set_throttling_number(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_MLD);
            (void) ipmc_stacking_set_grp_filtering(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_MLD);
            if (snp6_enable) {
                (void) ipmc_stacking_set_mode(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_MLD, FALSE);
            }
            (void) ipmc_stacking_set_unreg_flood(VTSS_ISID_GLOBAL, IPMC_IP_VERSION_MLD);
        }

        T_D("IPMC-INIT_CMD_CONF_DEF consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

        break;
    case INIT_CMD_MASTER_UP:
        T_I("MASTER_UP: ISID->%d", isid);

        /* Read configuration */
        ipmc_conf_default(IPMC_IP_VERSION_IGMP);
        ipmc_conf_default(IPMC_IP_VERSION_MLD);
        ipmc_sm_event_set(IPMC_EVENT_MASTER_UP);

        T_D("IPMC-INIT_CMD_MASTER_UP consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

        break;
    case INIT_CMD_MASTER_DOWN:
        T_I("MASTER_DOWN: ISID->%d", isid);

        ipmc_sm_event_set(IPMC_EVENT_MASTER_DOWN);

        T_D("IPMC-INIT_CMD_MASTER_DOWN consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

        break;
    case INIT_CMD_SWITCH_ADD:
        T_I("SWITCH_ADD: ISID->%d", isid);

        IPMC_CRIT_ENTER();
        ipmc_switch_event_value[ipmc_lib_isid_convert(TRUE, isid)] |= IPMC_EVENT_VALUE_SW_ADD;
        if (ipmc_lib_isid_is_local(isid)) {
            ipmc_switch_event_value[VTSS_ISID_LOCAL] |= IPMC_EVENT_VALUE_SW_ADD;
        }
        IPMC_CRIT_EXIT();

        ipmc_sm_event_set(IPMC_EVENT_SWITCH_ADD);


        T_D("IPMC-INIT_CMD_SWITCH_ADD consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

        break;
    case INIT_CMD_SWITCH_DEL:
        T_I("SWITCH_DEL: ISID->%d", isid);

        IPMC_CRIT_ENTER();
        ipmc_switch_event_value[ipmc_lib_isid_convert(TRUE, isid)] |= IPMC_EVENT_VALUE_SW_DEL;
        if (ipmc_lib_isid_is_local(isid)) {
            ipmc_switch_event_value[VTSS_ISID_LOCAL] |= IPMC_EVENT_VALUE_SW_DEL;
        }
        IPMC_CRIT_EXIT();

        ipmc_sm_event_set(IPMC_EVENT_SWITCH_DEL);

        T_D("IPMC-INIT_CMD_SWITCH_DEL consumes %u ticks", (u32)(vtss_current_time() - exe_time_base));

        break;
    default:
        break;
    }

    return 0;
}

/*****************************************************************************
    Public API section for IPMC Snooping
    from vtss_appl/include/vtss/appl/ipmc_snooping.h
*****************************************************************************/
static mesa_rc
_vtss_appl_ipmc_snp_system_config_get(
    const ipmc_ip_version_t                     version,
    ipmc_conf_global_t                          *const conf
)
{
    if (!conf) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    if ((ipmc_mgmt_get_mode(&conf->ipmc_mode_enabled, version) == VTSS_RC_OK) &&
        (ipmc_mgmt_get_unreg_flood(&conf->ipmc_unreg_flood_enabled, version) == VTSS_RC_OK) &&
        (ipmc_mgmt_get_leave_proxy(&conf->ipmc_leave_proxy_enabled, version) == VTSS_RC_OK) &&
        (ipmc_mgmt_get_proxy(&conf->ipmc_proxy_enabled, version) == VTSS_RC_OK) &&
        (ipmc_mgmt_get_ssm_range(version, &conf->ssm_range) == VTSS_RC_OK)) {
        return VTSS_RC_OK;
    }

    return VTSS_RC_ERROR;
}

static mesa_rc
_vtss_appl_ipmc_snp_system_config_set(
    const ipmc_ip_version_t                     version,
    ipmc_conf_global_t                          *const conf
)
{
    ipmc_conf_global_t  cfg;
    mesa_rc             rc = VTSS_RC_ERROR;

    if (!conf) {
        T_D("Invalid Input!");
        return rc;
    }

    if ((rc = _vtss_appl_ipmc_snp_system_config_get(version, &cfg)) == VTSS_RC_OK) {
        if (conf->ipmc_mode_enabled != cfg.ipmc_mode_enabled) {
            rc = ipmc_mgmt_set_mode(&conf->ipmc_mode_enabled, version);
        }
        if (rc == VTSS_RC_OK && conf->ipmc_unreg_flood_enabled != cfg.ipmc_unreg_flood_enabled) {
            if ((rc = ipmc_mgmt_set_unreg_flood(&conf->ipmc_unreg_flood_enabled, version)) != VTSS_RC_OK) {
                rc = ipmc_mgmt_set_mode(&cfg.ipmc_mode_enabled, version);
                rc = VTSS_RC_ERROR;
            }
        }
#ifdef VTSS_SW_OPTION_SMB_IPMC
        if (rc == VTSS_RC_OK && conf->ipmc_proxy_enabled != cfg.ipmc_proxy_enabled) {
            if ((rc = ipmc_mgmt_set_proxy(&conf->ipmc_proxy_enabled, version)) != VTSS_RC_OK) {
                rc = ipmc_mgmt_set_mode(&cfg.ipmc_mode_enabled, version);
                rc = ipmc_mgmt_set_unreg_flood(&cfg.ipmc_unreg_flood_enabled, version);
                rc = VTSS_RC_ERROR;
            }
        }
        if (rc == VTSS_RC_OK && conf->ipmc_leave_proxy_enabled != cfg.ipmc_leave_proxy_enabled) {
            if ((rc = ipmc_mgmt_set_leave_proxy(&conf->ipmc_leave_proxy_enabled, version)) != VTSS_RC_OK) {
                rc = ipmc_mgmt_set_mode(&cfg.ipmc_mode_enabled, version);
                rc = ipmc_mgmt_set_unreg_flood(&cfg.ipmc_unreg_flood_enabled, version);
                rc = ipmc_mgmt_set_proxy(&cfg.ipmc_proxy_enabled, version);
                rc = VTSS_RC_ERROR;
            }
        }
        if (rc == VTSS_RC_OK && memcmp(&conf->ssm_range, &cfg.ssm_range, sizeof(ipmc_prefix_t))) {
            if ((rc = ipmc_mgmt_set_ssm_range(version, &conf->ssm_range)) != VTSS_RC_OK) {
                rc = ipmc_mgmt_set_mode(&cfg.ipmc_mode_enabled, version);
                rc = ipmc_mgmt_set_unreg_flood(&cfg.ipmc_unreg_flood_enabled, version);
                rc = ipmc_mgmt_set_proxy(&cfg.ipmc_proxy_enabled, version);
                rc = ipmc_mgmt_set_leave_proxy(&cfg.ipmc_leave_proxy_enabled, version);
                rc = VTSS_RC_ERROR;
            }
        }
#endif /* VTSS_SW_OPTION_SMB_IPMC */
    }

    return rc;
}

static mesa_rc _vtss_appl_ipmc_snp_port_config_default(
    const ipmc_ip_version_t                     version,
    vtss_ifindex_t                              *const ifidx,
    void                                        *const entry
)
{
    vtss_appl_ipmc_snp4_port_conf_t *p = NULL;
#ifdef VTSS_SW_OPTION_SMB_IPMC
    vtss_appl_ipmc_snp6_port_conf_t *q = NULL;
#endif /* VTSS_SW_OPTION_SMB_IPMC */
    vtss_ifindex_elm_t              ife;

    if (!ifidx || !entry) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    if (vtss_ifindex_decompose(*ifidx, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_PORT ||
        ife.ordinal >= mesa_port_cnt(0)) {
        if (vtss_appl_iterator_ifindex_front_port_exist(NULL, ifidx) != VTSS_RC_OK) {
            T_D("Failed to retrieve first PORT-IfIndex");
            return VTSS_RC_ERROR;
        }
    }

#ifdef VTSS_SW_OPTION_SMB_IPMC
    if (version == IPMC_IP_VERSION_IGMP) {
        p = (vtss_appl_ipmc_snp4_port_conf_t *)entry;
    } else {
        q = (vtss_appl_ipmc_snp6_port_conf_t *)entry;
    }
#else
    p = (vtss_appl_ipmc_snp4_port_conf_t *)entry;
#endif /* VTSS_SW_OPTION_SMB_IPMC */

    if (p) {
        memset(p, 0x0, sizeof(vtss_appl_ipmc_snp4_port_conf_t));
        p->as_router_port = IPMC_DEF_RTR_PORT_VALUE;
        p->do_fast_leave = IPMC_DEF_IMMEDIATE_LEAVE_VALUE;
#ifdef VTSS_SW_OPTION_SMB_IPMC
        p->throttling_number = IPMC_DEF_THROLLTING_VALUE;
#endif /* VTSS_SW_OPTION_SMB_IPMC */
    }

#ifdef VTSS_SW_OPTION_SMB_IPMC
    if (q) {
        memset(q, 0x0, sizeof(vtss_appl_ipmc_snp6_port_conf_t));
        q->as_router_port = IPMC_DEF_RTR_PORT_VALUE;
        q->do_fast_leave = IPMC_DEF_IMMEDIATE_LEAVE_VALUE;
        q->throttling_number = IPMC_DEF_THROLLTING_VALUE;
    }
#endif /* VTSS_SW_OPTION_SMB_IPMC */

    return VTSS_RC_OK;
}

static mesa_rc
_vtss_appl_ipmc_snp_port_config_get(
    const ipmc_ip_version_t                     version,
    const vtss_isid_t                           isid,
    const mesa_port_no_t                        iport,
    ipmc_conf_port_group_filtering_t            *const snp_port_profile,
    ipmc_conf_throttling_max_no_t               *const snp_port_throttling,
    ipmc_conf_router_port_t                     *const snp_port_mrouter,
    ipmc_conf_fast_leave_port_t                 *const snp_port_immediate_leave
)
{
    mesa_rc rc = VTSS_RC_ERROR;

    if (!snp_port_profile || !snp_port_throttling ||
        !snp_port_mrouter || !snp_port_immediate_leave) {
        T_D("Invalid Input!");
        return rc;
    }

    rc = ipmc_mgmt_get_static_router_port(isid, snp_port_mrouter, version);
    if (rc == VTSS_RC_OK) {
        rc = ipmc_mgmt_get_fast_leave_port(isid, snp_port_immediate_leave, version);
    }
    if (rc == VTSS_RC_OK) {
        snp_port_profile->port_no = iport;
        if (ipmc_mgmt_get_port_group_filtering(isid, snp_port_profile, version) != VTSS_RC_OK) {
            memset(snp_port_profile, 0x0, sizeof(ipmc_conf_port_group_filtering_t));
            snp_port_profile->port_no = iport;
        }
    }
    if (rc == VTSS_RC_OK) {
        rc = ipmc_mgmt_get_throttling_max_count(isid, snp_port_throttling, version);
    }

    return rc;
}

static mesa_rc
_vtss_appl_ipmc_snp_port_config_set(
    const ipmc_ip_version_t                     version,
    const vtss_isid_t                           isid,
    const mesa_port_no_t                        iport,
    ipmc_conf_port_group_filtering_t            *const snp_port_profile,
    ipmc_conf_throttling_max_no_t               *const snp_port_throttling,
    ipmc_conf_router_port_t                     *const snp_port_mrouter,
    ipmc_conf_fast_leave_port_t                 *const snp_port_immediate_leave
)
{
    int                         str_len;
    ipmc_lib_profile_mem_t      *pfm;
    ipmc_lib_grp_fltr_profile_t *fltr_profile;
    mesa_rc                     rc = VTSS_RC_ERROR;

    if (!snp_port_profile || !snp_port_throttling ||
        !snp_port_mrouter || !snp_port_immediate_leave ||
        snp_port_throttling->ports[iport] > IPMC_THROLLTING_MAX_VALUE ||
        !IPMC_LIB_NAME_CHECK(snp_port_profile->addr.profile.name) ||
        !IPMC_MEM_PROFILE_MTAKE(pfm)) {
        T_D("Invalid Input!");
        return rc;
    }

    rc = VTSS_RC_OK;
    if ((str_len = strlen(snp_port_profile->addr.profile.name)) > 0) {
        fltr_profile = &pfm->profile;
        memset(fltr_profile, 0x0, sizeof(ipmc_lib_grp_fltr_profile_t));
        strncpy(fltr_profile->data.name, snp_port_profile->addr.profile.name, str_len);
        rc = ipmc_lib_mgmt_fltr_profile_get(fltr_profile, TRUE);
    }
    IPMC_MEM_PROFILE_MGIVE(pfm);

    if (rc == VTSS_RC_OK) {
        rc = ipmc_mgmt_set_router_port(isid, snp_port_mrouter, version);
    }
    if (rc == VTSS_RC_OK) {
        rc = ipmc_mgmt_set_fast_leave_port(isid, snp_port_immediate_leave, version);
    }
    if (rc == VTSS_RC_OK) {
        snp_port_profile->port_no = iport;
        if (str_len > 0) {
            rc = ipmc_mgmt_set_port_group_filtering(isid, snp_port_profile, version);
        } else {
            ipmc_conf_port_group_filtering_t    fltr_chk;

            memcpy(&fltr_chk, snp_port_profile, sizeof(ipmc_conf_port_group_filtering_t));
            if (ipmc_mgmt_get_port_group_filtering(isid, &fltr_chk, version) == VTSS_RC_OK) {
                rc = ipmc_mgmt_del_port_group_filtering(isid, &fltr_chk, version);
            }
        }
    }
    if (rc == VTSS_RC_OK) {
        rc = ipmc_mgmt_set_throttling_max_count(isid, snp_port_throttling, version);
    }

    return rc;
}

static mesa_rc
_vtss_appl_ipmc_snp_port_mrouter_get(
    const ipmc_ip_version_t                     version,
    const vtss_isid_t                           isid,
    const mesa_port_no_t                        iport,
    void                                        *const entry
)
{
    ipmc_conf_router_port_t     snp_srports;
    ipmc_dynamic_router_port_t  snp_drports;
    i8                          srBuf[MGMT_PORT_BUF_SIZE], drBuf[MGMT_PORT_BUF_SIZE];
    mesa_rc                     rc = VTSS_RC_ERROR;

    if (!entry) {
        T_D("Invalid Input!");
        return rc;
    }

    memset(&snp_srports, 0x0, sizeof(ipmc_conf_router_port_t));
    memset(&snp_drports, 0x0, sizeof(ipmc_dynamic_router_port_t));
    if ((rc = ipmc_mgmt_get_static_router_port(isid, &snp_srports, version)) == VTSS_RC_OK) {
        rc = ipmc_mgmt_get_dynamic_router_ports(isid, &snp_drports, version);
    }

    memset(srBuf, 0x0, sizeof(srBuf));
    memset(drBuf, 0x0, sizeof(drBuf));
    T_I("RC(%d)/VER(%d)/%d/%u -> Static:%s / Dynamic:%s",
        rc, version, isid, iport,
        mgmt_iport_list2txt(snp_srports.ports, srBuf),
        mgmt_iport_list2txt(snp_drports.ports, drBuf));

    if (rc == VTSS_RC_OK) {
        vtss_appl_ipmc_snp4_mrouter_t   *p = NULL;
#ifdef VTSS_SW_OPTION_SMB_IPMC
        vtss_appl_ipmc_snp6_mrouter_t   *q = NULL;
#endif /* VTSS_SW_OPTION_SMB_IPMC */

#ifdef VTSS_SW_OPTION_SMB_IPMC
        if (version == IPMC_IP_VERSION_IGMP) {
            p = (vtss_appl_ipmc_snp4_mrouter_t *)entry;
        } else {
            q = (vtss_appl_ipmc_snp6_mrouter_t *)entry;
        }
#else
        p = (vtss_appl_ipmc_snp4_mrouter_t *)entry;
#endif /* VTSS_SW_OPTION_SMB_IPMC */

        if (snp_srports.ports[iport] || snp_drports.ports[iport]) {
            if (snp_srports.ports[iport] && snp_drports.ports[iport]) {
                if (p) {
                    p->status = VTSS_APPL_IPMC_MROUTER4_BOTH;
                }
#ifdef VTSS_SW_OPTION_SMB_IPMC
                if (q) {
                    q->status = VTSS_APPL_IPMC_MROUTER6_BOTH;
                }
#endif /* VTSS_SW_OPTION_SMB_IPMC */
            } else {
                if (snp_srports.ports[iport]) {
                    if (p) {
                        p->status = VTSS_APPL_IPMC_MROUTER4_STATIC;
                    }
#ifdef VTSS_SW_OPTION_SMB_IPMC
                    if (q) {
                        q->status = VTSS_APPL_IPMC_MROUTER6_STATIC;
                    }
#endif /* VTSS_SW_OPTION_SMB_IPMC */
                }

                if (snp_drports.ports[iport]) {
                    if (p) {
                        p->status = VTSS_APPL_IPMC_MROUTER4_DYNAMIC;
                    }
#ifdef VTSS_SW_OPTION_SMB_IPMC
                    if (q) {
                        q->status = VTSS_APPL_IPMC_MROUTER6_DYNAMIC;
                    }
#endif /* VTSS_SW_OPTION_SMB_IPMC */
                }
            }
        } else {
            if (p) {
                p->status = VTSS_APPL_IPMC_MROUTER4_NONE;
            }
#ifdef VTSS_SW_OPTION_SMB_IPMC
            if (q) {
                q->status = VTSS_APPL_IPMC_MROUTER6_NONE;
            }
#endif /* VTSS_SW_OPTION_SMB_IPMC */
        }
    }

    return rc;
}

static mesa_rc
_vtss_appl_ipmc_snp_general_vlan_itr(
    const ipmc_ip_version_t                     version,
    const vtss_ifindex_t                        *const prev,
    vtss_ifindex_t                              *const next
)
{
    mesa_vid_t  vidx;
    BOOL        p, q;

    if (!next) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    vidx = VTSS_APPL_IPMC_VID_NULL;
    if (prev && *prev >= VTSS_IFINDEX_VLAN_OFFSET) {
        vtss_ifindex_elm_t  ife;

        /* get next */
        T_D("Find next IfIndex");

        /* get VID from given IfIndex */
        if (vtss_ifindex_decompose(*prev, &ife) != VTSS_RC_OK ||
            ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
            T_D("Failed to decompose IfIndex %u!\n\r", VTSS_IFINDEX_PRINTF_ARG(*prev));
            return VTSS_RC_ERROR;
        }

        vidx = (mesa_vid_t)ife.ordinal;
    } else {
        /* get first */
        T_D("Find first IfIndex");
    }

    if (ipmc_mgmt_get_intf_state_querier(TRUE, &vidx, &p, &q, TRUE, version) != VTSS_RC_OK) {
        T_D("No next VIDX w.r.t %u", vidx);
        return VTSS_RC_ERROR;
    }

    T_I("Found next VIDX-%u", vidx);
    /* convert VIDX to IfIndex */
    if (vtss_ifindex_from_vlan(vidx, next) != VTSS_RC_OK) {
        T_D("Failed to convert IfIndex from %u!\n\r", vidx);
        return VTSS_RC_ERROR;
    }

    return VTSS_RC_OK;
}

static mesa_rc
_vtss_appl_ipmc_snp_general_vlan_config_get(
    const ipmc_ip_version_t                     version,
    const vtss_ifindex_t                        *const ifidx,
    void                                        *const entry
)
{
    vtss_ifindex_elm_t              ife;
    mesa_vid_t                      vidx;
    ipmc_prot_intf_entry_param_t    intf;
    BOOL                            snp_admin, snp_qrier;
    mesa_rc                         rc = VTSS_RC_ERROR;

    if (!ifidx || !entry ||
        vtss_ifindex_decompose(*ifidx, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid Input!");
        return rc;
    }

    vidx = (mesa_vid_t)ife.ordinal;
    if (ipmc_mgmt_get_intf_state_querier(TRUE, &vidx, &snp_admin, &snp_qrier, FALSE, version) != VTSS_RC_OK) {
        T_D("No such VIDX %u in %s snooping", vidx, ipmc_lib_version_txt(version, IPMC_TXT_CASE_LOWER));
        return rc;
    }

    if ((rc = ipmc_mgmt_get_intf_info(VTSS_ISID_GLOBAL, vidx, &intf, version)) == VTSS_RC_OK) {
        ipmc_querier_sm_t                   *qrier = &intf.querier;
        vtss_appl_ipmc_snp_ipv4_interface_t *p = NULL;
#ifdef VTSS_SW_OPTION_SMB_IPMC
        vtss_appl_ipmc_snp_ipv6_interface_t *q = NULL;
#endif /* VTSS_SW_OPTION_SMB_IPMC */

#ifdef VTSS_SW_OPTION_SMB_IPMC
        if (version == IPMC_IP_VERSION_IGMP) {
            p = (vtss_appl_ipmc_snp_ipv4_interface_t *)entry;
        } else {
            q = (vtss_appl_ipmc_snp_ipv6_interface_t *)entry;
        }
#else
        p = (vtss_appl_ipmc_snp_ipv4_interface_t *)entry;
#endif /* VTSS_SW_OPTION_SMB_IPMC */

        if (p) {
#ifdef VTSS_SW_OPTION_SMB_IPMC
            vtss_appl_ipmc_compati4_t   compat = VTSS_APPL_IPMC_COMPATI4_AUTO;
#endif /* VTSS_SW_OPTION_SMB_IPMC */

            p->admin_state = snp_admin;
            p->querier_election = snp_qrier;
            p->querier_address = qrier->QuerierAdrs4;
#ifdef VTSS_SW_OPTION_SMB_IPMC
            switch ( intf.cfg_compatibility ) {
            case VTSS_APPL_IPMC_COMPATIBILITY_OLD:
                compat = VTSS_APPL_IPMC_COMPATI4_V1;
                break;
            case VTSS_APPL_IPMC_COMPATIBILITY_GEN:
                compat = VTSS_APPL_IPMC_COMPATI4_V2;
                break;
            case VTSS_APPL_IPMC_COMPATIBILITY_SFM:
                compat = VTSS_APPL_IPMC_COMPATI4_V3;
                break;
            default:
                break;
            }
            p->compatibility = compat;
            p->priority = intf.priority;
            p->robustness_variable = qrier->RobustVari;
            p->query_interval = qrier->QueryIntvl;
            p->query_response_interval = qrier->MaxResTime;
            p->last_listener_query_interval = qrier->LastQryItv;
            p->unsolicited_report_interval = qrier->UnsolicitR;
#endif /* VTSS_SW_OPTION_SMB_IPMC */
        }

#ifdef VTSS_SW_OPTION_SMB_IPMC
        if (q) {
            vtss_appl_ipmc_compati6_t   compat = VTSS_APPL_IPMC_COMPATI6_AUTO;

            q->admin_state = snp_admin;
            q->querier_election = snp_qrier;
            switch ( intf.cfg_compatibility ) {
            case VTSS_APPL_IPMC_COMPATIBILITY_GEN:
                compat = VTSS_APPL_IPMC_COMPATI6_V1;
                break;
            case VTSS_APPL_IPMC_COMPATIBILITY_SFM:
                compat = VTSS_APPL_IPMC_COMPATI6_V2;
                break;
            default:
                break;
            }
            q->compatibility = compat;
            q->priority = intf.priority;
            q->robustness_variable = qrier->RobustVari;
            q->query_interval = qrier->QueryIntvl;
            q->query_response_interval = qrier->MaxResTime;
            q->last_listener_query_interval = qrier->LastQryItv;
            q->unsolicited_report_interval = qrier->UnsolicitR;
        }
#endif /* VTSS_SW_OPTION_SMB_IPMC */
    }

    return rc;
}

static mesa_rc _vtss_appl_ipmc_snp_general_vlan_config_default(
    const ipmc_ip_version_t                     version,
    vtss_ifindex_t                              *const ifidx,
    void                                        *const entry
)
{
    vtss_appl_ipmc_snp_ipv4_interface_t *p = NULL;
#ifdef VTSS_SW_OPTION_SMB_IPMC
    vtss_appl_ipmc_snp_ipv6_interface_t *q = NULL;
#endif /* VTSS_SW_OPTION_SMB_IPMC */
    vtss_ifindex_elm_t                  ife;

    if (!ifidx || !entry) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    if (vtss_ifindex_decompose(*ifidx, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        if (vtss_ifindex_from_vlan(VTSS_APPL_VLAN_ID_DEFAULT, ifidx) != VTSS_RC_OK) {
            T_D("Failed to convert VLAN-IfIndex from VTSS_APPL_VLAN_ID_DEFAULT");
            return VTSS_RC_ERROR;
        }
    }

#ifdef VTSS_SW_OPTION_SMB_IPMC
    if (version == IPMC_IP_VERSION_IGMP) {
        p = (vtss_appl_ipmc_snp_ipv4_interface_t *)entry;
    } else {
        q = (vtss_appl_ipmc_snp_ipv6_interface_t *)entry;
    }
#else
    p = (vtss_appl_ipmc_snp_ipv4_interface_t *)entry;
#endif /* VTSS_SW_OPTION_SMB_IPMC */

    if (p) {
        memset(p, 0x0, sizeof(vtss_appl_ipmc_snp_ipv4_interface_t));
        p->admin_state = IPMC_DEF_INTF_STATE_VALUE;
        p->querier_election = IPMC_DEF_INTF_QUERIER_VALUE;
        p->querier_address = IPMC_DEF_INTF_QUERIER_ADRS4;
#ifdef VTSS_SW_OPTION_SMB_IPMC
        p->compatibility = VTSS_APPL_IPMC_COMPATI4_AUTO;
        p->priority = IPMC_DEF_INTF_PRI;
        p->robustness_variable = IPMC_DEF_INTF_RV;
        p->query_interval = IPMC_DEF_INTF_QI;
        p->query_response_interval = IPMC_DEF_INTF_QRI;
        p->last_listener_query_interval = IPMC_DEF_INTF_LMQI;
        p->unsolicited_report_interval = IPMC_DEF_INTF_URI;
#endif /* VTSS_SW_OPTION_SMB_IPMC */
    }

#ifdef VTSS_SW_OPTION_SMB_IPMC
    if (q) {
        memset(q, 0x0, sizeof(vtss_appl_ipmc_snp_ipv6_interface_t));
        q->admin_state = IPMC_DEF_INTF_STATE_VALUE;
        q->querier_election = IPMC_DEF_INTF_QUERIER_VALUE;
        q->compatibility = VTSS_APPL_IPMC_COMPATI6_AUTO;
        q->priority = IPMC_DEF_INTF_PRI;
        q->robustness_variable = IPMC_DEF_INTF_RV;
        q->query_interval = IPMC_DEF_INTF_QI;
        q->query_response_interval = IPMC_DEF_INTF_QRI;
        q->last_listener_query_interval = IPMC_DEF_INTF_LMQI;
        q->unsolicited_report_interval = IPMC_DEF_INTF_URI;
    }
#endif /* VTSS_SW_OPTION_SMB_IPMC */

    return VTSS_RC_OK;
}

static mesa_rc
_vtss_appl_ipmc_snp_general_vlan_status_get(
    const ipmc_ip_version_t                     version,
    const vtss_ifindex_t                        *const ifidx,
    void                                        *const entry
)
{
    vtss_ifindex_elm_t              ife;
    mesa_vid_t                      vidx;
    ipmc_prot_intf_entry_param_t    intf;
    ipmc_intf_query_host_version_t  compver;
    BOOL                            snp_admin, snp_qrier;
    BOOL                            ctrl, admin;
    mesa_rc                         rc = VTSS_RC_ERROR;

    if (!ifidx || !entry ||
        vtss_ifindex_decompose(*ifidx, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid Input!");
        return rc;
    }

    vidx = (mesa_vid_t)ife.ordinal;
    if (ipmc_mgmt_get_intf_state_querier(TRUE, &vidx, &snp_admin, &snp_qrier, FALSE, version) != VTSS_RC_OK) {
        T_D("No such VIDX %u in %s snooping", vidx, ipmc_lib_version_txt(version, IPMC_TXT_CASE_LOWER));
        return rc;
    }

    ctrl = FALSE;
    memset(&compver, 0x0, sizeof(ipmc_intf_query_host_version_t));
    compver.vid = vidx;
    if (ipmc_mgmt_get_intf_version(VTSS_ISID_GLOBAL, &compver, version) == VTSS_RC_OK) {
        rc = ipmc_mgmt_get_mode(&ctrl, version);
    }
    if (rc == VTSS_RC_OK &&
        (rc = ipmc_mgmt_get_intf_info(VTSS_ISID_GLOBAL, vidx, &intf, version)) == VTSS_RC_OK) {
        ipmc_querier_sm_t                       *qrier = &intf.querier;
        ipmc_statistics_t                       *stats = &intf.stats;
        vtss_appl_ipmc_snp_ipv4_intf_status_t   *p = NULL;
#ifdef VTSS_SW_OPTION_SMB_IPMC
        vtss_appl_ipmc_snp_ipv6_intf_status_t   *q = NULL;
#endif /* VTSS_SW_OPTION_SMB_IPMC */

#ifdef VTSS_SW_OPTION_SMB_IPMC
        if (version == IPMC_IP_VERSION_IGMP) {
            p = (vtss_appl_ipmc_snp_ipv4_intf_status_t *)entry;
        } else {
            q = (vtss_appl_ipmc_snp_ipv6_intf_status_t *)entry;
        }
#else
        p = (vtss_appl_ipmc_snp_ipv4_intf_status_t *)entry;
#endif /* VTSS_SW_OPTION_SMB_IPMC */

        if (ctrl && snp_admin) {
            admin = TRUE;
        } else {
            admin = FALSE;
        }

        if (p) {
            memset(p, 0x0, sizeof(vtss_appl_ipmc_snp_ipv4_intf_status_t));
            if (admin) {
                mesa_ipv4_t                     qadr;
                vtss_appl_ipmc_qry4_states_t    qs = VTSS_APPL_IPMC_QUERIER4_ACTIVE;

                switch ( qrier->state ) {
                case IPMC_QUERIER_IDLE:
                    qs = VTSS_APPL_IPMC_QUERIER4_IDLE;
                    p->querier_expiry_time = qrier->OtherQuerierTimeOut;
                    p->startup_query_count = qrier->RobustVari;
                    break;
                case IPMC_QUERIER_INIT:
                    qs = VTSS_APPL_IPMC_QUERIER4_INIT;
                    p->query_interval = qrier->timeout;
                    if (qrier->RobustVari < qrier->StartUpCnt) {
                        p->startup_query_count = qrier->RobustVari;
                    } else {
                        p->startup_query_count = qrier->RobustVari - qrier->StartUpCnt;
                    }
                    break;
                default:    /* VTSS_APPL_IPMC_QUERIER4_ACTIVE */
                    p->querier_up_time = qrier->QuerierUpTime;
                    p->query_interval = qrier->timeout;
                    p->startup_query_count = qrier->RobustVari;
                    break;
                }
                p->querier_state = qs;
                IPMC_LIB_ADRS_6TO4_SET(intf.active_querier, qadr);
                p->active_querier_address = ntohl(qadr);
                if (compver.query_version == VTSS_IPMC_VERSION1) {
                    p->querier_version = 1;
                } else if (compver.query_version == VTSS_IPMC_VERSION2) {
                    p->querier_version = 2;
                } else if (compver.query_version == VTSS_IPMC_VERSION3) {
                    p->querier_version = 3;
                }
                if (compver.host_version == VTSS_IPMC_VERSION1) {
                    p->host_version = 1;
                } else if (compver.host_version == VTSS_IPMC_VERSION2) {
                    p->host_version = 2;
                } else if (compver.host_version == VTSS_IPMC_VERSION3) {
                    p->host_version = 3;
                }
                if (intf.cfg_compatibility == VTSS_IPMC_COMPAT_MODE_AUTO) {
                    if ((compver.query_version == VTSS_IPMC_VERSION1) || (compver.query_version == VTSS_IPMC_VERSION2)) {
                        p->querier_present_timeout = ipmc_lib_mgmt_ovpt_get(&intf.rtr_compatibility);
                    }
                    if ((compver.host_version == VTSS_IPMC_VERSION1) || (compver.host_version == VTSS_IPMC_VERSION2)) {
                        p->host_present_timeout = ipmc_lib_mgmt_ovpt_get(&intf.hst_compatibility);
                    }
                }

                p->counter_tx_query = qrier->ipmc_queries_sent;
                p->counter_tx_specific_query = qrier->group_queries_sent;
                p->counter_rx_query = stats->igmp_queries;
                p->counter_rx_v1_join = stats->igmp_v1_membership_join;
                p->counter_rx_v2_join = stats->igmp_v2_membership_join;
                p->counter_rx_v2_leave = stats->igmp_v2_membership_leave;
                p->counter_rx_v3_join = stats->igmp_v3_membership_join;
                p->counter_rx_errors = stats->igmp_error_pkt;
            } else {
                p->querier_state = VTSS_APPL_IPMC_QUERIER4_DISABLED;
            }
        }

#ifdef VTSS_SW_OPTION_SMB_IPMC
        if (q) {
            memset(q, 0x0, sizeof(vtss_appl_ipmc_snp_ipv6_intf_status_t));
            if (admin) {
                vtss_appl_ipmc_qry6_states_t    qs = VTSS_APPL_IPMC_QUERIER6_ACTIVE;

                switch ( qrier->state ) {
                case IPMC_QUERIER_IDLE:
                    qs = VTSS_APPL_IPMC_QUERIER6_IDLE;
                    q->querier_expiry_time = qrier->OtherQuerierTimeOut;
                    q->startup_query_count = qrier->RobustVari;
                    break;
                case IPMC_QUERIER_INIT:
                    qs = VTSS_APPL_IPMC_QUERIER6_INIT;
                    q->query_interval = qrier->timeout;
                    if (qrier->RobustVari < qrier->StartUpCnt) {
                        q->startup_query_count = qrier->RobustVari;
                    } else {
                        q->startup_query_count = qrier->RobustVari - qrier->StartUpCnt;
                    }
                    break;
                default:    /* VTSS_APPL_IPMC_QUERIER6_ACTIVE */
                    q->querier_up_time = qrier->QuerierUpTime;
                    q->query_interval = qrier->timeout;
                    q->startup_query_count = qrier->RobustVari;
                    break;
                }
                q->querier_state = qs;
                IPMC_LIB_ADRS_CPY(&q->active_querier_address, &intf.active_querier);
                if (compver.query_version == VTSS_IPMC_VERSION2) {
                    q->querier_version = 1;
                } else if (compver.query_version == VTSS_IPMC_VERSION3) {
                    q->querier_version = 2;
                }
                if (compver.host_version == VTSS_IPMC_VERSION2) {
                    q->host_version = 1;
                } else if (compver.host_version == VTSS_IPMC_VERSION3) {
                    q->host_version = 2;
                }
                if (intf.cfg_compatibility == VTSS_IPMC_COMPAT_MODE_AUTO) {
                    if ((compver.query_version == VTSS_IPMC_VERSION1) || (compver.query_version == VTSS_IPMC_VERSION2)) {
                        q->querier_present_timeout = ipmc_lib_mgmt_ovpt_get(&intf.rtr_compatibility);
                    }
                    if ((compver.host_version == VTSS_IPMC_VERSION1) || (compver.host_version == VTSS_IPMC_VERSION2)) {
                        q->host_present_timeout = ipmc_lib_mgmt_ovpt_get(&intf.hst_compatibility);
                    }
                }
                q->counter_tx_query = qrier->ipmc_queries_sent;
                q->counter_tx_specific_query = qrier->group_queries_sent;
                q->counter_rx_query = stats->mld_queries;
                q->counter_rx_v1_report = stats->mld_v1_membership_report;
                q->counter_rx_v1_done = stats->mld_v1_membership_done;
                q->counter_rx_v2_report = stats->mld_v2_membership_report;
                q->counter_rx_errors = stats->mld_error_pkt;
            } else {
                q->querier_state = VTSS_APPL_IPMC_QUERIER6_DISABLED;
            }
        }
#endif /* VTSS_SW_OPTION_SMB_IPMC */
    }

    return rc;
}

static mesa_rc
_vtss_appl_ipmc_snp_set(
    const ipmc_ip_version_t                     version,
    const vtss_ifindex_t                        *const ifidx,
    const void                                  *const entry
)
{
    vtss_ifindex_elm_t                  ife;
    mesa_vid_t                          vidx;
    ipmc_prot_intf_entry_param_t        intf;
    BOOL                                snp_admin, snp_qrier;
    BOOL                                cfg_admin, cfg_qrier;
    vtss_appl_ipmc_snp_ipv4_interface_t *p = NULL;
#ifdef VTSS_SW_OPTION_SMB_IPMC
    vtss_appl_ipmc_snp_ipv6_interface_t *q = NULL;
#endif /* VTSS_SW_OPTION_SMB_IPMC */
    vtss_appl_ipmc_compatibility_t      cfg_cpt;
    mesa_ipv4_t                         cfg_qra;
    u32                                 cfg_pri, cfg_rv, cfg_qi, cfg_qri, cfg_lqi, cfg_uri;

    if (!ifidx ||
        vtss_ifindex_decompose(*ifidx, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    vidx = (mesa_vid_t)ife.ordinal;
    if (ipmc_mgmt_get_intf_state_querier(TRUE, &vidx, &snp_admin, &snp_qrier, FALSE, version) != VTSS_RC_OK) {
        T_D("No such VIDX %u in %s snooping", vidx, ipmc_lib_version_txt(version, IPMC_TXT_CASE_LOWER));
        return VTSS_RC_ERROR;
    }

    if (!entry) {
        T_D("Null Input!");
        return VTSS_RC_ERROR;
    }

#ifdef VTSS_SW_OPTION_SMB_IPMC
    if (version == IPMC_IP_VERSION_IGMP) {
        p = (vtss_appl_ipmc_snp_ipv4_interface_t *)entry;
    } else {
        q = (vtss_appl_ipmc_snp_ipv6_interface_t *)entry;
    }
#else
    p = (vtss_appl_ipmc_snp_ipv4_interface_t *)entry;
#endif /* VTSS_SW_OPTION_SMB_IPMC */

    cfg_admin = IPMC_DEF_INTF_STATE_VALUE;
    cfg_qrier = IPMC_DEF_INTF_QUERIER_VALUE;
    cfg_cpt = IPMC_DEF_INTF_COMPAT;
    cfg_qra = IPMC_DEF_INTF_QUERIER_ADRS4;
    cfg_pri = IPMC_DEF_INTF_PRI;
    cfg_rv  = IPMC_DEF_INTF_RV;
    cfg_qi  = IPMC_DEF_INTF_QI;
    cfg_qri = IPMC_DEF_INTF_QRI;
    cfg_lqi = IPMC_DEF_INTF_LMQI;
    cfg_uri = IPMC_DEF_INTF_URI;
    if (p) {
        cfg_admin = p->admin_state;
        cfg_qrier = p->querier_election;
        cfg_qra = p->querier_address;
#ifdef VTSS_SW_OPTION_SMB_IPMC
        switch ( p->compatibility ) {
        case VTSS_APPL_IPMC_COMPATI4_V1:
            cfg_cpt = VTSS_APPL_IPMC_COMPATIBILITY_OLD;
            break;
        case VTSS_APPL_IPMC_COMPATI4_V2:
            cfg_cpt = VTSS_APPL_IPMC_COMPATIBILITY_GEN;
            break;
        case VTSS_APPL_IPMC_COMPATI4_V3:
            cfg_cpt = VTSS_APPL_IPMC_COMPATIBILITY_SFM;
            break;
        default:
            break;
        }
        cfg_pri = (p->priority & 0xFF);
        cfg_rv  = p->robustness_variable;
        cfg_qi  = p->query_interval;
        cfg_qri = p->query_response_interval;
        cfg_lqi = p->last_listener_query_interval;
        cfg_uri = p->unsolicited_report_interval;
#endif /* VTSS_SW_OPTION_SMB_IPMC */
    }
#ifdef VTSS_SW_OPTION_SMB_IPMC
    if (q) {
        cfg_admin = q->admin_state;
        cfg_qrier = q->querier_election;
        switch ( q->compatibility ) {
        case VTSS_APPL_IPMC_COMPATI6_V1:
            cfg_cpt = VTSS_APPL_IPMC_COMPATIBILITY_GEN;
            break;
        case VTSS_APPL_IPMC_COMPATI6_V2:
            cfg_cpt = VTSS_APPL_IPMC_COMPATIBILITY_SFM;
            break;
        default:
            break;
        }
        cfg_pri = (q->priority & 0xFF);
        cfg_rv  = q->robustness_variable;
        cfg_qi  = q->query_interval;
        cfg_qri = q->query_response_interval;
        cfg_lqi = q->last_listener_query_interval;
        cfg_uri = q->unsolicited_report_interval;
    }

    if (cfg_rv < 2 || cfg_rv > 255 ||
        cfg_qi > 31744 || cfg_qri > 31744 ||
        cfg_qi < 1 || cfg_qri >= (cfg_qi * 10) ||
        cfg_lqi > 31744 || cfg_uri > 31744 ||
        cfg_pri > IPMC_PARAM_MAX_PRIORITY) {
        T_D("Invalid Parameters!");
        return VTSS_RC_ERROR;
    }
    if (cfg_qra) {
        u8  adrc = (cfg_qra >> 24) & 0xFF;

        if ((adrc == 127) || (adrc > 223)) {
            T_D("Invalid QuerierAddress!");
            return VTSS_RC_ERROR;
        }
    }
#endif /* VTSS_SW_OPTION_SMB_IPMC */

    if (snp_admin != cfg_admin || snp_qrier != cfg_qrier) {
        mesa_rc rc = ipmc_mgmt_set_intf_state_querier(vidx, &cfg_admin, &cfg_qrier, version);
        if (rc != VTSS_RC_OK) {
            T_D("Failed in ipmc_mgmt_set_intf_state_querier(%d) for VIDX %u", rc, vidx);
            return rc;
        }
    }

    if (ipmc_mgmt_get_intf_info(VTSS_ISID_GLOBAL, vidx, &intf, version) == VTSS_RC_OK) {
        ipmc_querier_sm_t   *qrier  = &intf.querier;

        intf.cfg_compatibility = cfg_cpt;
        intf.priority = cfg_pri;
        qrier->QuerierAdrs4 = cfg_qra;
        qrier->RobustVari = cfg_rv;
        qrier->QueryIntvl = cfg_qi;
        qrier->MaxResTime = cfg_qri;
        qrier->LastQryItv = cfg_lqi;
        qrier->UnsolicitR = cfg_uri;

        return ipmc_mgmt_set_intf_info(VTSS_ISID_GLOBAL, &intf, version);
    } else {
        (void) ipmc_mgmt_set_intf_state_querier(vidx, &snp_admin, &snp_qrier, version);
        return VTSS_RC_ERROR;
    }
}

static mesa_rc
_vtss_appl_ipmc_snp_statistics_clear_by_vlan(
    const ipmc_ip_version_t                     version,
    const vtss_ifindex_t                        *const ifindex
)
{
    vtss_ifindex_elm_t              ife;
    mesa_vid_t                      vidx;
    BOOL                            snp_admin, snp_qrier;

    if (!ifindex) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    if (*ifindex == VTSS_IFINDEX_VLAN_OFFSET) {
        return VTSS_RC_OK;
    }

    if (vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid IfIndex!");
        return VTSS_RC_ERROR;
    }

    vidx = (mesa_vid_t)ife.ordinal;
    if (ipmc_mgmt_get_intf_state_querier(TRUE, &vidx, &snp_admin, &snp_qrier, FALSE, version) != VTSS_RC_OK) {
        T_D("No such VIDX %u in %s snooping", vidx, ipmc_lib_version_txt(version, IPMC_TXT_CASE_LOWER));
        return VTSS_RC_ERROR;
    }

    return ipmc_mgmt_clear_stat_counter(VTSS_ISID_GLOBAL, version, vidx);
}

static mesa_rc
_vtss_appl_ipmc_snp_group_address_itr(
    const ipmc_ip_version_t                     version,
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const void                                  *const grps_prev,
    void                                        *const grps_next
)
{
    mesa_vid_t                      vidx;
    mesa_ipv6_t                     grpx;
    ipmc_prot_intf_group_entry_t    intf_group_entry;
    i8                              adrString[40];
    mesa_rc                         rc = VTSS_RC_ERROR;

    if (!ifid_next || !grps_next) {
        T_D("Invalid Input!");
        return rc;
    }

    vidx = VTSS_APPL_IPMC_VID_NULL;
    if (ifid_prev && *ifid_prev >= VTSS_IFINDEX_VLAN_OFFSET) {
        vtss_ifindex_elm_t  ife;

        T_D("Get VID from given IfIndex");
        if (vtss_ifindex_decompose(*ifid_prev, &ife) != VTSS_RC_OK ||
            ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
            T_D("Failed to decompose VLAN-IfIndex %u!\n\r", VTSS_IFINDEX_PRINTF_ARG(*ifid_prev));
            return rc;
        }

        vidx = (mesa_vid_t)ife.ordinal;
    }

    IPMC_LIB_ADRS_SET(&grpx, 0x0);
    if (ifid_prev && grps_prev != NULL) {
        if (version == IPMC_IP_VERSION_IGMP) {
            mesa_ipv4_t grp4;

            grp4 = htonl(*((mesa_ipv4_t *)grps_prev));
            IPMC_LIB_ADRS_4TO6_SET(grp4, grpx);
        }
        if (version == IPMC_IP_VERSION_MLD) {
            IPMC_LIB_ADRS_CPY(&grpx, ((mesa_ipv6_t *)grps_prev));
        }
    }

    memset(&intf_group_entry, 0x0, sizeof(ipmc_prot_intf_group_entry_t));
    rc = ipmc_mgmt_group_info_get_next(VTSS_ISID_GLOBAL, version, &vidx, &grpx, &intf_group_entry);

    memset(adrString, 0x0, sizeof(adrString));
    T_I("%sFound next VIDX-%u / GRPS-%s",
        rc == VTSS_RC_OK ? "" : "Not",
        intf_group_entry.vid,
        misc_ipv6_txt(&intf_group_entry.group_addr, adrString));
    if (rc == VTSS_RC_OK) {
        /* convert VIDX to IfIndex */
        if (vtss_ifindex_from_vlan(intf_group_entry.vid, ifid_next) != VTSS_RC_OK) {
            T_D("Failed to convert IfIndex from %u!\n\r", vidx);
            return VTSS_RC_ERROR;
        }
        if (version == IPMC_IP_VERSION_IGMP) {
            IPMC_LIB_ADRS_6TO4_SET(intf_group_entry.group_addr, (*((mesa_ipv4_t *)grps_next)));
            *((mesa_ipv4_t *)grps_next) = ntohl(*((mesa_ipv4_t *)grps_next));
        }
        if (version == IPMC_IP_VERSION_MLD) {
            IPMC_LIB_ADRS_CPY(((mesa_ipv6_t *)grps_next), &intf_group_entry.group_addr);
        }
    }

    return rc;
}

/* Refer to MAC sample */
static BOOL snp_portlist_index_set(u32 i, vtss_port_list_stackable_t *pl)
{
    if (i >= 8 * 128) {
        return false;
    }

    u8 val = 1;
    u32 idx_bit = i & 7;
    u32 idx = i >> 3;
    val <<= idx_bit;
    pl->data[idx] |= val;
    return true;
}

static BOOL snp_portlist_index_clear(u32 i, vtss_port_list_stackable_t *pl)
{
    if (i >= 8 * 128) {
        return false;
    }

    u8 val = 1;
    u32 idx_bit = i & 7;
    u32 idx = i >> 3;
    val <<= idx_bit;
    pl->data[idx] &= (~val);
    return true;
}

static u32 snp_isid_port_to_index(vtss_isid_t i,  mesa_port_no_t p)
{
    VTSS_ASSERT(VTSS_PORT_NO_START == 0);
    VTSS_ASSERT(VTSS_ISID_START == 1);
    VTSS_ASSERT(mesa_port_cnt(0) - 1 <= 64);
    return (i - 1) * 64 + iport2uport(p);
}
/* MAC sample END */

static mesa_rc
_vtss_appl_ipmc_snp_group_address_get(
    const ipmc_ip_version_t                     version,
    const mesa_vid_t                            *const vidx,
    const mesa_ipv6_t                           *const grpx,
    void                                        *const entry
)
{
    switch_iter_t                   sit;
    port_iter_t                     pit;
    ipmc_prot_intf_group_entry_t    intf_group_entry;
    vtss_isid_t                     isid;
    mesa_port_no_t                  iport;
    mesa_rc                         rc = VTSS_RC_ERROR;

    if (!vidx || !grpx || !entry) {
        T_D("Invalid Input!");
        return rc;
    }

    if ((rc = ipmc_mgmt_group_info_get(VTSS_ISID_GLOBAL, version, vidx, grpx, &intf_group_entry)) == VTSS_RC_OK) {
        ipmc_group_db_t                     *grp_db = &intf_group_entry.db;
        vtss_appl_ipmc_snp_grp4_address_t   *p = NULL;
#ifdef VTSS_SW_OPTION_SMB_IPMC
        vtss_appl_ipmc_snp_grp6_address_t   *q = NULL;

        if (version == IPMC_IP_VERSION_IGMP) {
            p = (vtss_appl_ipmc_snp_grp4_address_t *)entry;
            p->hardware_switch = grp_db->asm_in_hw;
            memset(&p->member_ports, 0x0, sizeof(vtss_port_list_stackable_t));
        } else {
            q = (vtss_appl_ipmc_snp_grp6_address_t *)entry;
            q->hardware_switch = grp_db->asm_in_hw;
            memset(&q->member_ports, 0x0, sizeof(vtss_port_list_stackable_t));
        }
#else
        p = (vtss_appl_ipmc_snp_grp4_address_t *)entry;
        p->hardware_switch = grp_db->asm_in_hw;
        memset(&p->member_ports, 0x0, sizeof(vtss_port_list_stackable_t));
#endif /* VTSS_SW_OPTION_SMB_IPMC */

        (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
        while (switch_iter_getnext(&sit)) {
            isid = sit.isid;
            if (ipmc_mgmt_group_info_get(isid, version, vidx, grpx, &intf_group_entry) != VTSS_RC_OK) {
                continue;
            }

            (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_FRONT);
            while (port_iter_getnext(&pit)) {
                iport = pit.iport;
                if (VTSS_PORT_BF_GET(grp_db->port_mask, iport)) {
#ifdef VTSS_SW_OPTION_SMB_IPMC
                    if (p) {
                        (void) snp_portlist_index_set(snp_isid_port_to_index(isid, iport), &p->member_ports);
                    }
                    if (q) {
                        (void) snp_portlist_index_set(snp_isid_port_to_index(isid, iport), &q->member_ports);
                    }
#else
                    (void) snp_portlist_index_set(snp_isid_port_to_index(isid, iport), &p->member_ports);
#endif /* VTSS_SW_OPTION_SMB_IPMC */
                } else {
#ifdef VTSS_SW_OPTION_SMB_IPMC
                    if (p) {
                        (void) snp_portlist_index_clear(snp_isid_port_to_index(isid, iport), &p->member_ports);
                    }
                    if (q) {
                        (void) snp_portlist_index_clear(snp_isid_port_to_index(isid, iport), &q->member_ports);
                    }
#else
                    (void) snp_portlist_index_clear(snp_isid_port_to_index(isid, iport), &p->member_ports);
#endif /* VTSS_SW_OPTION_SMB_IPMC */
                }
            }
        }
    }

    return rc;
}

#ifdef VTSS_SW_OPTION_SMB_IPMC
static mesa_rc
_vtss_appl_ipmc_snp_group_srclist_itr(
    const ipmc_ip_version_t                     version,
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const void                                  *const grps_prev,
    void                                        *const grps_next,
    const vtss_ifindex_t                        *const port_prev,
    vtss_ifindex_t                              *const port_next,
    const void                                  *const adrs_prev,
    void                                        *const adrs_next
)
{
    ipmc_prot_intf_group_entry_t    intf_group_entry;
    ipmc_group_db_t                 *grp_db;
    ipmc_prot_group_srclist_t       group_srclist_entry;
    mesa_vid_t                      vidx;
    vtss_isid_t                     sidx, isid;
    mesa_port_no_t                  prtx, iport;
    mesa_ipv6_t                     grpx, adrx, *adrp;
    vtss_ifindex_elm_t              ife;
    mesa_ipv4_t                     ipa4;
    BOOL                            fnd;
    switch_iter_t                   sit;
    port_iter_t                     pit;
    i8                              grpString[40], adrString[40];
    mesa_rc                         rc = VTSS_RC_ERROR;

    if (!ifid_next || !grps_next ||
        !port_next || !adrs_next) {
        T_D("Invalid Input!");
        return rc;
    }

    vidx = VTSS_APPL_IPMC_VID_NULL;
    IPMC_LIB_ADRS_SET(&grpx, 0x0);      // takes care of grps_prev = null
    sidx = VTSS_ISID_GLOBAL;
    prtx = VTSS_PORT_NO_START;          // takes care of port_prev = null
    IPMC_LIB_ADRS_SET(&adrx, 0x0);
    adrp = NULL;
    memset(&intf_group_entry, 0x0, sizeof(ipmc_prot_intf_group_entry_t));
    grp_db = &intf_group_entry.db;
    fnd = FALSE;

    if (ifid_prev && *ifid_prev >= VTSS_IFINDEX_VLAN_OFFSET) {
        T_D("Get VID from given IfIndex");
        if (vtss_ifindex_decompose(*ifid_prev, &ife) != VTSS_RC_OK ||
            ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
            T_D("Failed to decompose VLAN-IfIndex %u!\n\r", VTSS_IFINDEX_PRINTF_ARG(*ifid_prev));
            return rc;
        }
        vidx = (mesa_vid_t)ife.ordinal;

        if (grps_prev != NULL) {
            if (version == IPMC_IP_VERSION_IGMP) {
                ipa4 = htonl(*((mesa_ipv4_t *)grps_prev));
                IPMC_LIB_ADRS_4TO6_SET(ipa4, grpx);
            }
            if (version == IPMC_IP_VERSION_MLD) {
                IPMC_LIB_ADRS_CPY(&grpx, ((mesa_ipv6_t *)grps_prev));
            }
        }

        if ((rc = ipmc_mgmt_group_info_get(VTSS_ISID_GLOBAL, version, &vidx, &grpx, &intf_group_entry)) == VTSS_RC_OK) {
            // Got group based on ifid, grpx
            if (port_prev) {
                T_D("Get ISID/PORT from given IfIndex");
                if (vtss_ifindex_decompose(*port_prev, &ife) != VTSS_RC_OK ||
                    ife.iftype != VTSS_IFINDEX_TYPE_PORT) {
                    T_D("Failed to decompose PORT-IfIndex %u!\n\r", VTSS_IFINDEX_PRINTF_ARG(*port_prev));
                    rc = ipmc_mgmt_group_info_get_next(VTSS_ISID_GLOBAL, version, &vidx, &grpx, &intf_group_entry);
                    // FIXME Incorrect if *port_prev < first port index: should then get first entry with matching (vidx, grpx) but instead skips to next row
                } else {
                    sidx = ife.isid;
                    prtx = (mesa_vid_t)ife.ordinal;
                }
            }

            if (rc == VTSS_RC_OK && sidx != VTSS_ISID_GLOBAL &&
                ipmc_mgmt_group_info_get(sidx, version, &vidx, &grpx, &intf_group_entry) == VTSS_RC_OK) {
                if (VTSS_PORT_BF_GET(grp_db->port_mask, prtx) &&
                    IPMC_LIB_GRP_PORT_DO_SFM(grp_db, prtx)) {
                    // Got group based on ifid, grpx, prtx
                    if (adrs_prev != NULL) {
                        if (version == IPMC_IP_VERSION_IGMP) {
                            ipa4 = htonl(*((mesa_ipv4_t *)adrs_prev));
                            IPMC_LIB_ADRS_4TO6_SET(ipa4, adrx);
                        }
                        if (version == IPMC_IP_VERSION_MLD) {
                            IPMC_LIB_ADRS_CPY(&adrx, ((mesa_ipv6_t *)adrs_prev));
                        }
                        adrp = &adrx;
                    }
                }
            }
        } else {
            // No exact match on ifid, grpx: get next
            rc = ipmc_mgmt_group_info_get_next(VTSS_ISID_GLOBAL, version, &vidx, &grpx, &intf_group_entry);
        }
    } else {
        // ifid null: get-first
        rc = ipmc_mgmt_group_info_get_next(VTSS_ISID_GLOBAL, version, &vidx, &grpx, &intf_group_entry);
    }

    memset(&group_srclist_entry, 0x0, sizeof(ipmc_prot_group_srclist_t));
    while (!fnd && rc == VTSS_RC_OK) {
        vidx = intf_group_entry.vid;
        IPMC_LIB_ADRS_CPY(&grpx, &intf_group_entry.group_addr);

        if (sidx == VTSS_ISID_GLOBAL) {
            // First iteration of enclosing while(): port_prev was null, so need to look through all switches, starting with lowest ISID
            // Subsequent iteration of while(): Didn't find port, so start over: All switches, first port
            (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
            while (!fnd && switch_iter_getnext(&sit)) {
                isid = sit.isid;
                if (ipmc_mgmt_group_info_get(isid, version, &vidx, &grpx, &intf_group_entry) != VTSS_RC_OK) {
                    T_D("GLOBAL branch, skipping isid %d", isid);
                    continue;
                }

                (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_FRONT);
                while (port_iter_getnext(&pit)) {
                    iport = pit.iport;
                    if (VTSS_PORT_BF_GET(grp_db->port_mask, iport) &&
                        IPMC_LIB_GRP_PORT_DO_SFM(grp_db, iport)) {
                        memset(&group_srclist_entry, 0x0, sizeof(ipmc_prot_group_srclist_t));
                        group_srclist_entry.type = IPMC_LIB_GRP_PORT_SFM_EX(grp_db, iport) ? FALSE : TRUE;
                        while (ipmc_mgmt_get_next_group_srclist_info(isid, version, vidx,
                                                                     &grpx,
                                                                     &group_srclist_entry) == VTSS_OK) {
                            if (VTSS_PORT_BF_GET(group_srclist_entry.srclist.port_mask, iport)) {
                                T_D("GLOBAL branch, match on srclist!");
                                IPMC_LIB_ADRS_CPY(&adrx, &group_srclist_entry.srclist.src_ip);
                                fnd = TRUE;
                                break;
                            }
                        }

                        if (!fnd) { /* Source is None */
                            T_D("GLOBAL branch, not found, adrx := 0");
                            IPMC_LIB_ADRS_SET(&adrx, 0x0);
                        }

                        sidx = isid;
                        prtx = iport;
                        fnd = TRUE;
                        break;
                    }
                }
                T_D("GLOBAL branch done");
            }
        } else {
            // port_prev was non-null, so we got an ISID from the port: Look at specific switch
            T_D("Non-global ISID (%d) branch", sidx);

            if (ipmc_mgmt_group_info_get(sidx, version, &vidx, &grpx, &intf_group_entry) == VTSS_RC_OK) {
                if (adrp && ipmc_util_ipv6_address_zero(adrp)) {
                    if (++prtx >= mesa_port_cnt(0)) {
                        prtx = mesa_port_cnt(0);
                    }
                    adrp = NULL;
                }

                (void) port_iter_init(&pit, NULL, sidx, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_FRONT);
                while (prtx < mesa_port_cnt(0) && port_iter_getnext(&pit)) {
                    if ((iport = pit.iport) < prtx) {
                        continue;
                    }

                    if (VTSS_PORT_BF_GET(grp_db->port_mask, iport) &&
                        IPMC_LIB_GRP_PORT_DO_SFM(grp_db, iport)) {
                        memset(&group_srclist_entry, 0x0, sizeof(ipmc_prot_group_srclist_t));
                        group_srclist_entry.type = IPMC_LIB_GRP_PORT_SFM_EX(grp_db, iport) ? FALSE : TRUE;
                        if (adrp) {
                            IPMC_LIB_ADRS_CPY(&group_srclist_entry.srclist.src_ip, adrp);
                        }
                        T_D("Searching VIDX %u / GRPS %s / PORT %u:%u / ADDR-%s",
                            vidx,
                            misc_ipv6_txt(&grpx, grpString),
                            sidx, iport,
                            adrp ? misc_ipv6_txt(&adrx, adrString) : "*No address*");

                        while (ipmc_mgmt_get_next_group_srclist_info(sidx, version, vidx,
                                                                     &grpx,
                                                                     &group_srclist_entry) == VTSS_OK) {
                            T_D("   loop: Got srclist addr %s", misc_ipv6_txt(&group_srclist_entry.srclist.src_ip, adrString));
                            if (VTSS_PORT_BF_GET(group_srclist_entry.srclist.port_mask, iport)) {
                                IPMC_LIB_ADRS_CPY(&adrx, &group_srclist_entry.srclist.src_ip);
                                fnd = TRUE;
                                T_D("Got srclist match! Addr %s", misc_ipv6_txt(&adrx, adrString));
                                break;
                            }
                        }

                        if (!fnd) { /* Source is None */
                            T_D("No srclist match");
                            IPMC_LIB_ADRS_SET(&adrx, 0x0);
                        }

                        prtx = iport;
                        fnd = fnd || !adrp;
                        break;
                    }

                    if (!fnd && iport == prtx) {
                        adrp = NULL;
                    }
                }
            }

            // !fnd: Hm, got a port but didn't find it used for IGMP on the ISID associated with it. So, need to search remaining, higher, ISIDs (all ports)

            (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_ISID_CFG);
            while (!fnd && switch_iter_getnext(&sit)) {
                T_D("Checking remaining ISID (%d) branch\n", sit.isid);

                isid = sit.isid;
                if (isid <= sidx) {
                    continue;
                }
                if (ipmc_mgmt_group_info_get(isid, version, &vidx, &grpx, &intf_group_entry) != VTSS_RC_OK) {
                    continue;
                }

                (void) port_iter_init(&pit, NULL, isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_FRONT);
                while (port_iter_getnext(&pit)) {
                    iport = pit.iport;
                    if (VTSS_PORT_BF_GET(grp_db->port_mask, iport) &&
                        IPMC_LIB_GRP_PORT_DO_SFM(grp_db, iport)) {
                        T_D("Got grp_db match");
                        memset(&group_srclist_entry, 0x0, sizeof(ipmc_prot_group_srclist_t));
                        group_srclist_entry.type = IPMC_LIB_GRP_PORT_SFM_EX(grp_db, iport) ? FALSE : TRUE;
                        while (ipmc_mgmt_get_next_group_srclist_info(isid, version, vidx,
                                                                     &grpx,
                                                                     &group_srclist_entry) == VTSS_OK) {
                            if (VTSS_PORT_BF_GET(group_srclist_entry.srclist.port_mask, iport)) {
                                IPMC_LIB_ADRS_CPY(&adrx, &group_srclist_entry.srclist.src_ip);
                                fnd = TRUE;
                                T_D("Got match!");
                                break;
                            }
                        }

                        if (!fnd) { /* Source is None */
                            T_D("No srclist match");
                            IPMC_LIB_ADRS_SET(&adrx, 0x0);
                        }

                        sidx = isid;
                        prtx = iport;
                        fnd = TRUE;
                        break;
                    }
                }
            }

            if (!fnd) {
                T_D("NO MATCH, restarting from GLOBAL");
                sidx = VTSS_ISID_GLOBAL;
                prtx = VTSS_PORT_NO_START;
            }
        }

        if (!fnd) {
            T_D("No match, get-next group!\n");
            rc = ipmc_mgmt_group_info_get_next(VTSS_ISID_GLOBAL, version, &vidx, &grpx, &intf_group_entry);
        }
    }  // while (!fnd && rc == VTSS_RC_OK)

    memset(grpString, 0x0, sizeof(adrString));
    memset(adrString, 0x0, sizeof(adrString));
    T_I("%sFound next VIDX-%u / GRPS-%s / PORT-%u:%u / ADDR-%s",
        fnd ? "" : "Not",
        vidx,
        misc_ipv6_txt(&grpx, grpString),
        sidx, prtx,
        misc_ipv6_txt(&adrx, adrString));
    if (fnd) {
        vtss_ifindex_t  idxv, idxp;

        /* convert VIDX to IfIndex */
        if (vtss_ifindex_from_vlan(vidx, &idxv) != VTSS_RC_OK) {
            T_D("Failed to convert VLAN-IfIndex from %u!\n\r", vidx);
            return VTSS_RC_ERROR;
        }
        /* convert SIDX/PRTX to IfIndex */
        if (vtss_ifindex_from_port(sidx, prtx, &idxp) != VTSS_RC_OK) {
            T_D("Failed to convert PORT-IfIndex from %u/%u!\n\r", sidx, prtx);
            return VTSS_RC_ERROR;
        }

        *ifid_next = idxv;
        *port_next = idxp;
        if (version == IPMC_IP_VERSION_IGMP) {
            IPMC_LIB_ADRS_6TO4_SET(grpx, (*((mesa_ipv4_t *)grps_next)));
            *((mesa_ipv4_t *)grps_next) = ntohl(*((mesa_ipv4_t *)grps_next));
            IPMC_LIB_ADRS_6TO4_SET(adrx, (*((mesa_ipv4_t *)adrs_next)));
            *((mesa_ipv4_t *)adrs_next) = ntohl(*((mesa_ipv4_t *)adrs_next));
        }

        if (version == IPMC_IP_VERSION_MLD) {
            IPMC_LIB_ADRS_CPY(((mesa_ipv6_t *)grps_next), &grpx);
            IPMC_LIB_ADRS_CPY(((mesa_ipv6_t *)adrs_next), &adrx);
        }
        return VTSS_RC_OK;
    } else {
        return VTSS_RC_ERROR;
    }
}

static mesa_rc
_vtss_appl_ipmc_snp_group_srclist_get(
    const ipmc_ip_version_t                     version,
    const mesa_vid_t                            *const vidx,
    const mesa_ipv6_t                           *const grpx,
    const vtss_isid_t                           *const sidx,
    const mesa_port_no_t                        *const prtx,
    const mesa_ipv6_t                           *const adrx,
    void                                        *const entry
)
{
    ipmc_prot_intf_group_entry_t    intf_group_entry;
    mesa_rc                         rc = VTSS_RC_ERROR;

    if (!vidx || !grpx || !sidx ||
        !prtx || !adrx || !entry) {
        T_D("Invalid Input!");
        return rc;
    }

    if ((rc = ipmc_mgmt_group_info_get(*sidx, version, vidx, grpx, &intf_group_entry)) == VTSS_RC_OK) {
        ipmc_group_db_t                     *grp_db = &intf_group_entry.db;
        ipmc_prot_group_srclist_t           group_srclist_entry;
        vtss_appl_ipmc_snp_grp4_srclist_t   *p = NULL;
        vtss_appl_ipmc_snp_grp6_srclist_t   *q = NULL;

        if (version == IPMC_IP_VERSION_IGMP) {
            p = (vtss_appl_ipmc_snp_grp4_srclist_t *)entry;
        } else {
            q = (vtss_appl_ipmc_snp_grp6_srclist_t *)entry;
        }

        if (VTSS_PORT_BF_GET(grp_db->port_mask, *prtx) &&
            IPMC_LIB_GRP_PORT_DO_SFM(grp_db, *prtx)) {
            i32                 fto, sto;
            BOOL                sfm, hws = FALSE;
            ipmc_sfm_srclist_t  *sfm_src = &group_srclist_entry.srclist;

            memset(&group_srclist_entry, 0x0, sizeof(ipmc_prot_group_srclist_t));
            if (IPMC_LIB_GRP_PORT_SFM_EX(grp_db, *prtx)) {
                fto = ipmc_lib_mgmt_calc_delta_time(*sidx, &grp_db->tmr.delta_time.v[*prtx]);
                group_srclist_entry.type = FALSE;
            } else {
                fto = -1;
                group_srclist_entry.type = TRUE;
            }
            sfm = group_srclist_entry.type;

            sto = -1;
            if (ipmc_util_ipv6_address_zero(adrx)) {
                hws = TRUE;
                while (ipmc_mgmt_get_next_group_srclist_info(*sidx, version, *vidx,
                                                             grpx,
                                                             &group_srclist_entry) == VTSS_OK) {
                    if (VTSS_PORT_BF_GET(sfm_src->port_mask, *prtx)) {
                        hws = FALSE;
                        rc = VTSS_RC_ERROR;
                        break;
                    }
                }
            } else {
                rc = VTSS_RC_ERROR;
                while (ipmc_mgmt_get_next_group_srclist_info(*sidx, version, *vidx,
                                                             grpx,
                                                             &group_srclist_entry) == VTSS_OK) {
                    if (IPMC_LIB_ADRS_CMP(adrx, &sfm_src->src_ip)) {
                        continue;
                    }

                    if (VTSS_PORT_BF_GET(sfm_src->port_mask, *prtx)) {
                        sto = ipmc_lib_mgmt_calc_delta_time(*sidx, &sfm_src->tmr.delta_time.v[*prtx]);
                        hws = sfm_src->sfm_in_hw;
                        rc = VTSS_RC_OK;
                        break;
                    }
                }
            }

            if (rc == VTSS_RC_OK) {
                if (p) {
                    p->group_filter_mode = (fto < 0) ? VTSS_APPL_IPMC_SF4_MODE_INCLUDE : VTSS_APPL_IPMC_SF4_MODE_EXCLUDE;
                    p->filter_timer = (fto > 0) ? (u32)fto : 0;
                    p->source_type = sfm ? VTSS_APPL_IPMC_ACTION4_PERMIT : VTSS_APPL_IPMC_ACTION4_DENY;
                    p->source_timer = (sto > 0) ? (u32)sto : 0;
                    p->hardware_switch = hws;
                }
                if (q) {
                    q->group_filter_mode = (fto < 0) ? VTSS_APPL_IPMC_SF6_MODE_INCLUDE : VTSS_APPL_IPMC_SF6_MODE_EXCLUDE;
                    q->filter_timer = (fto > 0) ? (u32)fto : 0;
                    q->source_type = sfm ? VTSS_APPL_IPMC_ACTION6_PERMIT : VTSS_APPL_IPMC_ACTION6_DENY;
                    q->source_timer = (sto > 0) ? (u32)sto : 0;
                    q->hardware_switch = hws;
                }
            }
        } else {
            rc = VTSS_RC_ERROR;
        }
    }

    return rc;
}
#endif /* VTSS_SW_OPTION_SMB_IPMC */

/**
 * \brief Get IGMP Snooping Global Parameters
 *
 * To read current system parameters in IGMP snooping.
 *
 * \param conf      [OUT]    The IGMP snooping system configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_system_config_get(
    vtss_appl_ipmc_snp_ipv4_global_t            *const conf
)
{
    ipmc_conf_global_t  cfg;
    char                adrString[40];
    mesa_rc             rc = VTSS_RC_ERROR;

    if (!conf) {
        T_D("Invalid Input!");
        return rc;
    }

    memset(&cfg, 0x0, sizeof(ipmc_conf_global_t));
    if ((rc = _vtss_appl_ipmc_snp_system_config_get(IPMC_IP_VERSION_IGMP, &cfg)) == VTSS_RC_OK) {
        conf->mode = cfg.ipmc_mode_enabled;
        conf->unregistered_flooding = cfg.ipmc_unreg_flood_enabled;
#ifdef VTSS_SW_OPTION_SMB_IPMC
        conf->proxy = cfg.ipmc_proxy_enabled;
        conf->leave_proxy = cfg.ipmc_leave_proxy_enabled;
        conf->ssm_address = cfg.ssm_range.addr.value.prefix;
        conf->ssm_mask_len = cfg.ssm_range.len;
#endif /* VTSS_SW_OPTION_SMB_IPMC */
    }

    memset(adrString, 0x0, sizeof(adrString));
    T_D("GET(%s)-%s[%s/%u]/%sFlooding/%sProxy/%sLeaveProxy",
        rc == VTSS_RC_OK ? "OK" : "NG",
        cfg.ipmc_mode_enabled ? "Enabled" : "Disabled",
        misc_ipv4_txt(cfg.ssm_range.addr.value.prefix, adrString),
        cfg.ssm_range.len,
        cfg.ipmc_unreg_flood_enabled ? "" : "No",
        cfg.ipmc_proxy_enabled ? "" : "No",
        cfg.ipmc_leave_proxy_enabled ? "" : "No");

    return rc;
}

/**
 * \brief Set IGMP Snooping Global Parameters
 *
 * To modify current system parameters in IGMP snooping.
 *
 * \param conf      [IN]     The IGMP snooping system configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_system_config_set(
    const vtss_appl_ipmc_snp_ipv4_global_t      *const conf
)
{
    ipmc_conf_global_t  cfg;
    char                adrString[40];
    mesa_rc             rc = VTSS_RC_ERROR;

    if (!conf) {
        T_D("Invalid Input!");
        return rc;
    }

    memset(&cfg, 0x0, sizeof(ipmc_conf_global_t));
#ifdef VTSS_SW_OPTION_SMB_IPMC
    if (((conf->ssm_address >> 24) & 0xFF) > 239 ||
        ((conf->ssm_address >> 24) & 0xFF) < 224) {
        T_D("Invalid SSM-Range!");
        return rc;
    }

    cfg.ssm_range.addr.value.prefix = conf->ssm_address;
    cfg.ssm_range.len = conf->ssm_mask_len;
    cfg.ipmc_proxy_enabled = conf->proxy;
    cfg.ipmc_leave_proxy_enabled = conf->leave_proxy;
#endif /* VTSS_SW_OPTION_SMB_IPMC */
    cfg.ipmc_mode_enabled = conf->mode;
    cfg.ipmc_unreg_flood_enabled = conf->unregistered_flooding;

    rc = _vtss_appl_ipmc_snp_system_config_set(IPMC_IP_VERSION_IGMP, &cfg);

    memset(adrString, 0x0, sizeof(adrString));
    T_D("SET(%s)-%s[%s/%u]/%sFlooding/%sProxy/%sLeaveProxy",
        rc == VTSS_RC_OK ? "OK" : "NG",
        conf->mode ? "Enabled" : "Disabled",
        misc_ipv4_txt(cfg.ssm_range.addr.value.prefix, adrString),
        cfg.ssm_range.len,
        conf->unregistered_flooding ? "" : "No",
        cfg.ipmc_proxy_enabled ? "" : "No",
        cfg.ipmc_leave_proxy_enabled ? "" : "No");

    return rc;
}

/**
 * \brief Get IGMP Snooping global default parameters
 *
 * To get default system parameters in IGMP snooping.
 *
 * \param conf      [OUT]    The IGMP snooping system default configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc vtss_appl_ipmc_snp_igmp_system_config_default(
    vtss_appl_ipmc_snp_ipv4_global_t            *const conf
)
{
    if (!conf) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    memset(conf, 0x0, sizeof(vtss_appl_ipmc_snp_ipv4_global_t));
    conf->mode = IPMC_DEF_GLOBAL_STATE_VALUE;
    conf->unregistered_flooding = IPMC_DEF_UNREG_FLOOD_VALUE;
#ifdef VTSS_SW_OPTION_SMB_IPMC
    conf->proxy = IPMC_DEF_PROXY_VALUE;
    conf->leave_proxy = IPMC_DEF_LEAVE_PROXY_VALUE;
    conf->ssm_address = IPMC_DEF_SSM_PREFIX4_VALUE;
    conf->ssm_mask_len = IPMC_DEF_SSM_PREFIX4_LEN_VALUE;
#endif /* VTSS_SW_OPTION_SMB_IPMC */

    return VTSS_RC_OK;
}

/**
 * \brief Iterator for retrieving IPMC Snooping port information key/index
 *
 * To walk information (configuration and status) index of the port in IPMC snooping.
 *
 * \param prev      [IN]    Interface index to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_general_port_itr(
    const vtss_ifindex_t                        *const prev,
    vtss_ifindex_t                              *const next
)
{
    return vtss_appl_iterator_ifindex_front_port_exist(prev, next);
}

/**
 * \brief Get IGMP Snooping specific port configuration
 *
 * To get configuration of the specific port in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 *
 * \param entry     [OUT]   The current configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_port_config_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_snp4_port_conf_t             *const entry
)
{
    vtss_ifindex_elm_t                  ife;
    vtss_isid_t                         isid;
    u32                                 ptx;
    ipmc_conf_port_group_filtering_t    snp_port_profile;
    ipmc_conf_throttling_max_no_t       snp_port_throttling;
    ipmc_conf_router_port_t             snp_port_mrouter;
    ipmc_conf_fast_leave_port_t         snp_port_immediate_leave;
    mesa_rc                             rc = VTSS_RC_ERROR;

    /* get isid/iport from given IfIndex */
    if (!entry || !ifindex ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_PORT ||
        (ptx = ife.ordinal) >= mesa_port_cnt(0)) {
        T_D("Invalid Input!");
        return rc;
    }

    isid = ife.isid;
    T_D("GET ISID:%u PORT:%u", isid, ptx);

    memset(&snp_port_mrouter, 0x0, sizeof(ipmc_conf_router_port_t));
    memset(&snp_port_immediate_leave, 0x0, sizeof(ipmc_conf_fast_leave_port_t));
    memset(&snp_port_profile, 0x0, sizeof(ipmc_conf_port_group_filtering_t));
    vtss_clear(snp_port_throttling);
    rc = _vtss_appl_ipmc_snp_port_config_get(IPMC_IP_VERSION_IGMP, isid, ptx,
                                             &snp_port_profile,
                                             &snp_port_throttling,
                                             &snp_port_mrouter,
                                             &snp_port_immediate_leave);

    if (rc == VTSS_RC_OK) {
        entry->as_router_port = snp_port_mrouter.ports[ptx];
        entry->do_fast_leave = snp_port_immediate_leave.ports[ptx];
#ifdef VTSS_SW_OPTION_SMB_IPMC
        entry->throttling_number = snp_port_throttling.ports[ptx];
        memset(entry->filtering_profile.n, 0x0, sizeof(entry->filtering_profile.n));
        if (strlen(snp_port_profile.addr.profile.name)) {
            strncpy(entry->filtering_profile.n, snp_port_profile.addr.profile.name, strlen(snp_port_profile.addr.profile.name));
        }
#endif /* VTSS_SW_OPTION_SMB_IPMC */
    }

    T_D("GET(%s:%d:%u)-%sAsRtr/%sDoFstLve/%u/%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        isid, ptx,
        snp_port_mrouter.ports[ptx] ? "" : "Not",
        snp_port_immediate_leave.ports.get(ptx) ? "" : "Not",
        snp_port_throttling.ports[ptx],
        snp_port_profile.addr.profile.name);

    return rc;
}

/**
 * \brief Set IGMP Snooping specific port configuration
 *
 * To modify configuration of the specific port in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 * \param entry     [IN]    The revised configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_port_config_set(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_appl_ipmc_snp4_port_conf_t       *const entry
)
{
    vtss_ifindex_elm_t                  ife;
    vtss_isid_t                         isid;
    u32                                 ptx;
    ipmc_conf_port_group_filtering_t    snp_port_profile;
    ipmc_conf_throttling_max_no_t       snp_port_throttling;
    ipmc_conf_router_port_t             snp_port_mrouter;
    ipmc_conf_fast_leave_port_t         snp_port_immediate_leave;
    mesa_rc                             rc = VTSS_RC_ERROR;

    /* get isid/iport from given IfIndex */
    if (!entry || !ifindex ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_PORT ||
        (ptx = ife.ordinal) >= mesa_port_cnt(0)) {
        T_D("Invalid Input!");
        return rc;
    }

    isid = ife.isid;
    T_D("GET ISID:%u PORT:%u", isid, ptx);

    memset(&snp_port_mrouter, 0x0, sizeof(ipmc_conf_router_port_t));
    memset(&snp_port_immediate_leave, 0x0, sizeof(ipmc_conf_fast_leave_port_t));
    memset(&snp_port_profile, 0x0, sizeof(ipmc_conf_port_group_filtering_t));
    vtss_clear(snp_port_throttling);
    rc = _vtss_appl_ipmc_snp_port_config_get(IPMC_IP_VERSION_IGMP, isid, ptx,
                                             &snp_port_profile,
                                             &snp_port_throttling,
                                             &snp_port_mrouter,
                                             &snp_port_immediate_leave);

    if (rc == VTSS_RC_OK) {
        snp_port_mrouter.ports[ptx] = entry->as_router_port;
        snp_port_immediate_leave.ports[ptx] = entry->do_fast_leave;
#ifdef VTSS_SW_OPTION_SMB_IPMC
        snp_port_throttling.ports[ptx] = entry->throttling_number;
        memset(snp_port_profile.addr.profile.name, 0x0, sizeof(snp_port_profile.addr.profile.name));
        if (strlen(entry->filtering_profile.n)) {
            strncpy(snp_port_profile.addr.profile.name, entry->filtering_profile.n, strlen(entry->filtering_profile.n));
        }
#endif /* VTSS_SW_OPTION_SMB_IPMC */

        rc = _vtss_appl_ipmc_snp_port_config_set(IPMC_IP_VERSION_IGMP, isid, ptx,
                                                 &snp_port_profile,
                                                 &snp_port_throttling,
                                                 &snp_port_mrouter,
                                                 &snp_port_immediate_leave);
    }

    T_D("SET(%s:%d:%u)-%sAsRtr/%sDoFstLve/%u/%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        isid, ptx,
        snp_port_mrouter.ports[ptx] ? "" : "Not",
        snp_port_immediate_leave.ports[ptx] ? "" : "Not",
        snp_port_throttling.ports[ptx],
        snp_port_profile.addr.profile.name);

    return rc;
}

/**
 * \brief Get IGMP Snooping specific port default configuration
 *
 * To get default configuration of the specific port in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 *
 * \param entry     [OUT]   The default configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc vtss_appl_ipmc_snp_igmp_port_config_default(
    vtss_ifindex_t                              *const ifindex,
    vtss_appl_ipmc_snp4_port_conf_t             *const entry
)
{
    return _vtss_appl_ipmc_snp_port_config_default(IPMC_IP_VERSION_IGMP, ifindex, (void *)entry);
}

/**
 * \brief Get IGMP Snooping specific port's mrouter status
 *
 * To get mrouter status of the specific port in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 *
 * \param entry     [OUT]   The current mrouter status of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_port_mrouter_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_snp4_mrouter_t               *const entry
)
{
    vtss_ifindex_elm_t  ife;
    vtss_isid_t         isid;
    u32                 ptx;
    mesa_rc             rc = VTSS_RC_ERROR;

    /* get isid/iport from given IfIndex */
    if (!entry || !ifindex ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_PORT ||
        (ptx = ife.ordinal) >= mesa_port_cnt(0)) {
        T_D("Invalid Input!");
        return rc;
    }

    isid = ife.isid;
    T_D("GET ISID:%u PORT:%u", isid, ptx);

    rc = _vtss_appl_ipmc_snp_port_mrouter_get(IPMC_IP_VERSION_IGMP, isid, ptx, (void *)entry);

    T_D("GET(%s)-%d",
        rc == VTSS_RC_OK ? "OK" : "NG",
        entry->status);

    return rc;
}

/**
 * \brief Iterator for retrieving IGMP Snooping VLAN information key/index
 *
 * To walk information (configuration and status) index of the VLAN in IGMP snooping.
 *
 * \param prev      [IN]    Interface index to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_vlan_general_itr(
    const vtss_ifindex_t                        *const prev,
    vtss_ifindex_t                              *const next
)
{
    T_D("enter(prev:%d/next%d)", prev ? vtss_ifindex_cast_to_u32(*prev) : -1, next ? vtss_ifindex_cast_to_u32(*next) : -1);
    return _vtss_appl_ipmc_snp_general_vlan_itr(IPMC_IP_VERSION_IGMP, prev, next);
}

/**
 * \brief Get IGMP Snooping specific VLAN configuration
 *
 * To get configuration of the specific VLAN in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The current configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_vlan_config_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_snp_ipv4_interface_t         *const entry
)
{
    T_D("enter(ifindex:%d)", ifindex ? vtss_ifindex_cast_to_u32(*ifindex) : -1);
    return _vtss_appl_ipmc_snp_general_vlan_config_get(IPMC_IP_VERSION_IGMP, ifindex, (void *)entry);
}

/**
 * \brief Set IGMP Snooping specific VLAN configuration
 *
 * To modify configuration of the specific VLAN in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param entry     [IN]    The revised configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_vlan_config_set(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_appl_ipmc_snp_ipv4_interface_t   *const entry
)
{
    T_D("enter(ifindex:%d)", ifindex ? vtss_ifindex_cast_to_u32(*ifindex) : -1);
    return _vtss_appl_ipmc_snp_set(IPMC_IP_VERSION_IGMP, ifindex, (void *)entry);
}

/**
 * \brief Get IGMP Snooping specific VLAN default configuration
 *
 * To get default configuration of the specific VLAN in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The default configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc vtss_appl_ipmc_snp_igmp_vlan_config_default(
    vtss_ifindex_t                              *const ifindex,
    vtss_appl_ipmc_snp_ipv4_interface_t         *const entry
)
{
    return _vtss_appl_ipmc_snp_general_vlan_config_default(IPMC_IP_VERSION_IGMP, ifindex, (void *)entry);
}

/**
 * \brief Get IGMP Snooping specific VLAN interface's status
 *
 * To get interface's status of the specific VLAN in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The current configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_vlan_status_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_snp_ipv4_intf_status_t       *const entry
)
{
    T_D("enter(ifindex:%d)", ifindex ? vtss_ifindex_cast_to_u32(*ifindex) : -1);
    return _vtss_appl_ipmc_snp_general_vlan_status_get(IPMC_IP_VERSION_IGMP, ifindex, (void *)entry);
}

/**
 * \brief Get current IPMC Snooping multicast group registration count
 *
 * To read the entry count of registered multicast group address from IPMC snooping.
 *
 * \param grp_cnt   [OUT]   The current registered multicast group address count.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_grp_adr_cnt_get(
    vtss_appl_ipmc_snp_grp_adrs_cnt_t           *const grp_cnt
)
{
    ipmc_prot_intf_group_entry_t    intf_group_entry;
    ipmc_ip_version_t               version;
    mesa_vid_t                      vidx;
    BOOL                            p, q;

    if (!grp_cnt) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    memset(grp_cnt, 0x0, sizeof(vtss_appl_ipmc_snp_grp_adrs_cnt_t));

    vidx = VTSS_APPL_IPMC_VID_NULL;
    version = IPMC_IP_VERSION_IGMP;
    if (ipmc_mgmt_get_intf_state_querier(TRUE, &vidx, &p, &q, TRUE, version) != VTSS_RC_OK) {
#ifdef VTSS_SW_OPTION_SMB_IPMC
        vidx = VTSS_APPL_IPMC_VID_NULL;
        version = IPMC_IP_VERSION_MLD;
        if (ipmc_mgmt_get_intf_state_querier(TRUE, &vidx, &p, &q, TRUE, version) != VTSS_RC_OK) {
            T_D("GET IGMP_GRP_CNT-0! / MLD_GRP_CNT-0!");
            return VTSS_RC_OK;
        }
#else
        T_D("GET IGMP_GRP_CNT-0!");
        return VTSS_RC_OK;
#endif /* VTSS_SW_OPTION_SMB_IPMC */
    }

    do {
        memset(&intf_group_entry, 0x0, sizeof(ipmc_prot_intf_group_entry_t));
        intf_group_entry.ipmc_version = version;
        intf_group_entry.vid = vidx;
        while (ipmc_mgmt_get_next_intf_group_info(VTSS_ISID_GLOBAL, vidx, &intf_group_entry, version) == VTSS_OK) {
            if (version == IPMC_IP_VERSION_IGMP) {
                ++grp_cnt->igmp_snp_grps;
            }
#ifdef VTSS_SW_OPTION_SMB_IPMC
            if (version == IPMC_IP_VERSION_MLD) {
                ++grp_cnt->mld_snp_grps;
            }
#endif /* VTSS_SW_OPTION_SMB_IPMC */
        }
    } while (ipmc_mgmt_get_intf_state_querier(TRUE, &vidx, &p, &q, TRUE, version) == VTSS_RC_OK);

#ifdef VTSS_SW_OPTION_SMB_IPMC
    if (version == IPMC_IP_VERSION_IGMP) {
        vidx = VTSS_APPL_IPMC_VID_NULL;
        version = IPMC_IP_VERSION_MLD;
        while (ipmc_mgmt_get_intf_state_querier(TRUE, &vidx, &p, &q, TRUE, version) == VTSS_RC_OK) {
            memset(&intf_group_entry, 0x0, sizeof(ipmc_prot_intf_group_entry_t));
            intf_group_entry.ipmc_version = version;
            intf_group_entry.vid = vidx;
            while (ipmc_mgmt_get_next_intf_group_info(VTSS_ISID_GLOBAL, vidx, &intf_group_entry, version) == VTSS_OK) {
                ++grp_cnt->mld_snp_grps;
            }
        }
    }

    T_D("GET IGMP_GRP_CNT-%u / MLD_GRP_CNT-%u",
        grp_cnt->igmp_snp_grps,
        grp_cnt->mld_snp_grps);
#else
    T_D("GET IGMP_GRP_CNT-%u",
        grp_cnt->igmp_snp_grps);
#endif /* VTSS_SW_OPTION_SMB_IPMC */

    return VTSS_RC_OK;
}

/**
 * \brief Iterator for retrieving IGMP Snooping group address table key/index
 *
 * To walk indexes of the group address table in IGMP snooping.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param grps_prev [IN]    IPv4 Address of multicast group to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param grps_next [OUT]   The key/index of multicast group address should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_group_address_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const mesa_ipv4_t                           *const grps_prev,
    mesa_ipv4_t                                 *const grps_next
)
{
    T_D("enter(IGMP-GRPADDR)");
    return _vtss_appl_ipmc_snp_group_address_itr(IPMC_IP_VERSION_IGMP,
                                                 ifid_prev,
                                                 ifid_next,
                                                 (void *)grps_prev,
                                                 (void *)grps_next);
}

/**
 * \brief Get IGMP Snooping specific registered multicast address on a VLAN
 *
 * To get registered multicast group address data of the specific VLAN in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param grpadrs   [IN]    (key) Multicast group address.
 *
 * \param entry     [OUT]   The current status of the registered multicast group address.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_group_address_get(
    const vtss_ifindex_t                        *const ifindex,
    const mesa_ipv4_t                           *const grpadrs,
    vtss_appl_ipmc_snp_grp4_address_t           *const entry
)
{
    vtss_ifindex_elm_t  ife;
    mesa_vid_t          vidx;
    mesa_ipv6_t         grpx;
    mesa_ipv4_t         ipa4;
    i8                  grpString[40];
    mesa_rc             rc = VTSS_RC_ERROR;

    /* get VID from given IfIndex */
    if (!entry || !ifindex || !grpadrs ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid Input!");
        return rc;
    }

    vidx = (mesa_vid_t)ife.ordinal;
    IPMC_LIB_ADRS_SET(&grpx, 0x0);
    ipa4 = htonl(*grpadrs);
    IPMC_LIB_ADRS_4TO6_SET(ipa4, grpx);
    rc = _vtss_appl_ipmc_snp_group_address_get(IPMC_IP_VERSION_IGMP, &vidx, &grpx, (void *)entry);

    memset(grpString, 0x0, sizeof(grpString));
    T_D("GET(%s)-VIDX:%u/GRPX:%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        vidx, misc_ipv6_txt(&grpx, grpString));

    return rc;
}

#ifdef VTSS_SW_OPTION_SMB_IPMC
/**
 * \brief Iterator for retrieving IGMP Snooping group source list table key/index
 *
 * To walk indexes of the group source list table in IGMP snooping.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param grps_prev [IN]    IPv4 Address of multicast group to be used for indexing determination.
 * \param port_prev [IN]    Ifindex of port to be used for indexing determination.
 * \param adrs_prev [IN]    IPv4 Address of multicasting source to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param grps_next [OUT]   The key/index of multicast group address should be used for the GET operation.
 * \param port_next [OUT]   The key/index of port's Ifindex should be used for the GET operation.
 * \param adrs_next [OUT]   The key/index of multicasting source address should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_group_srclist_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const mesa_ipv4_t                           *const grps_prev,
    mesa_ipv4_t                                 *const grps_next,
    const vtss_ifindex_t                        *const port_prev,
    vtss_ifindex_t                              *const port_next,
    const mesa_ipv4_t                           *const adrs_prev,
    mesa_ipv4_t                                 *const adrs_next
)
{
    T_D("enter(IGMP-SRCLIST)");
    return _vtss_appl_ipmc_snp_group_srclist_itr(IPMC_IP_VERSION_IGMP,
                                                 ifid_prev,
                                                 ifid_next,
                                                 (void *)grps_prev,
                                                 (void *)grps_next,
                                                 port_prev,
                                                 port_next,
                                                 (void *)adrs_prev,
                                                 (void *)adrs_next);
}

/**
 * \brief Get IGMP Snooping specific registered multicast address and its source list entry
 *
 * To get registered multicast group source list data of the specific VLAN and port in IGMP snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param grpadrs   [IN]    (key) Multicast group address.
 * \param stkport   [IN]    (key) Interface index - the logical interface index of the port.
 * \param srcadrs   [IN]    (key) Multicasting source address.
 *
 * \param entry     [OUT]   The current status of the registered multicast group source list entry.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_group_srclist_get(
    const vtss_ifindex_t                        *const ifindex,
    const mesa_ipv4_t                           *const grpadrs,
    const vtss_ifindex_t                        *const stkport,
    const mesa_ipv4_t                           *const srcadrs,
    vtss_appl_ipmc_snp_grp4_srclist_t           *const entry
)
{
    vtss_ifindex_elm_t  ife;
    mesa_vid_t          vidx;
    mesa_ipv6_t         grpx, adrx;
    vtss_isid_t         sidx;
    mesa_port_no_t      prtx;
    mesa_ipv4_t         ipa4;
    i8                  grpString[40], adrString[40];
    mesa_rc             rc = VTSS_RC_ERROR;

    /* get VID from given IfIndex */
    if (!entry || !ifindex || !grpadrs || !stkport || !srcadrs ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid Input!");
        return rc;
    }
    vidx = (mesa_vid_t)ife.ordinal;
    /* get ISID/PORT from given IfIndex */
    if (vtss_ifindex_decompose(*stkport, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_PORT ||
        (prtx = ife.ordinal) >= mesa_port_cnt(0)) {
        T_D("Invalid Input!");
        return rc;
    }
    sidx = ife.isid;

    IPMC_LIB_ADRS_SET(&grpx, 0x0);
    ipa4 = htonl(*grpadrs);
    IPMC_LIB_ADRS_4TO6_SET(ipa4, grpx);
    IPMC_LIB_ADRS_SET(&adrx, 0x0);
    ipa4 = htonl(*srcadrs);
    IPMC_LIB_ADRS_4TO6_SET(ipa4, adrx);
    rc = _vtss_appl_ipmc_snp_group_srclist_get(IPMC_IP_VERSION_IGMP, &vidx, &grpx, &sidx, &prtx, &adrx, (void *)entry);

    memset(grpString, 0x0, sizeof(grpString));
    memset(adrString, 0x0, sizeof(adrString));
    T_D("GET(%s)-VIDX:%u/GRPX:%s/SIDX:%d/PRTX:%d/ADRX:%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        vidx, misc_ipv6_txt(&grpx, grpString),
        sidx, prtx, misc_ipv6_txt(&adrx, adrString));

    return rc;
}
#endif /* VTSS_SW_OPTION_SMB_IPMC */

/**
 * \brief IGMP Snooping Control ACTION for clearing statistics
 *
 * To clear the statistics based on the given VLAN ifIndex of IGMP interface.
 *
 * \param ifindex   [IN]    Interface index - the logical interface index of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_igmp_statistics_clear_by_vlan_act(
    const vtss_ifindex_t                        *const ifindex
)
{
    T_D("enter(ifindex:%d)", ifindex ? vtss_ifindex_cast_to_u32(*ifindex) : -1);
    return _vtss_appl_ipmc_snp_statistics_clear_by_vlan(IPMC_IP_VERSION_IGMP, ifindex);
}

#ifdef VTSS_SW_OPTION_SMB_IPMC
/**
 * \brief Get MLD Snooping Global Parameters
 *
 * To read current system parameters in MLD snooping.
 *
 * \param conf      [OUT]    The MLD snooping system configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_system_config_get(
    vtss_appl_ipmc_snp_ipv6_global_t            *const conf
)
{
    ipmc_conf_global_t  cfg;
    char                adrString[40];
    mesa_rc             rc = VTSS_RC_ERROR;

    if (!conf) {
        T_D("Invalid Input!");
        return rc;
    }

    memset(&cfg, 0x0, sizeof(ipmc_conf_global_t));
    if ((rc = _vtss_appl_ipmc_snp_system_config_get(IPMC_IP_VERSION_MLD, &cfg)) == VTSS_RC_OK) {
        conf->mode = cfg.ipmc_mode_enabled;
        conf->unregistered_flooding = cfg.ipmc_unreg_flood_enabled;
        conf->proxy = cfg.ipmc_proxy_enabled;
        conf->leave_proxy = cfg.ipmc_leave_proxy_enabled;
        IPMC_LIB_ADRS_CPY(&conf->ssm_address, &cfg.ssm_range.addr.array.prefix);
        conf->ssm_mask_len = cfg.ssm_range.len;
    }

    memset(adrString, 0x0, sizeof(adrString));
    T_D("GET(%s)-%s[%s/%u]/%sFlooding/%sProxy/%sLeaveProxy",
        rc == VTSS_RC_OK ? "OK" : "NG",
        cfg.ipmc_mode_enabled ? "Enabled" : "Disabled",
        misc_ipv6_txt(&cfg.ssm_range.addr.array.prefix, adrString),
        cfg.ssm_range.len,
        cfg.ipmc_unreg_flood_enabled ? "" : "No",
        cfg.ipmc_proxy_enabled ? "" : "No",
        cfg.ipmc_leave_proxy_enabled ? "" : "No");

    return rc;
}

/**
 * \brief Set MLD Snooping Global Parameters
 *
 * To modify current system parameters in MLD snooping.
 *
 * \param conf      [IN]     The MLD snooping system configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_system_config_set(
    const vtss_appl_ipmc_snp_ipv6_global_t      *const conf
)
{
    ipmc_conf_global_t  cfg;
    char                adrString[40];
    mesa_rc             rc = VTSS_RC_ERROR;

    if (!conf) {
        T_D("Invalid Input!");
        return rc;
    }

    cfg.ipmc_mode_enabled = conf->mode;
    cfg.ipmc_unreg_flood_enabled = conf->unregistered_flooding;
    cfg.ipmc_proxy_enabled = conf->proxy;
    cfg.ipmc_leave_proxy_enabled = conf->leave_proxy;
    IPMC_LIB_ADRS_CPY(&cfg.ssm_range.addr.array.prefix, &conf->ssm_address);
    cfg.ssm_range.len = conf->ssm_mask_len;

    rc = _vtss_appl_ipmc_snp_system_config_set(IPMC_IP_VERSION_MLD, &cfg);

    memset(adrString, 0x0, sizeof(adrString));
    T_D("SET(%s)-%s[%s/%u]/%sFlooding/%sProxy/%sLeaveProxy",
        rc == VTSS_RC_OK ? "OK" : "NG",
        conf->mode ? "Enabled" : "Disabled",
        misc_ipv6_txt(&cfg.ssm_range.addr.array.prefix, adrString),
        cfg.ssm_range.len,
        conf->unregistered_flooding ? "" : "No",
        conf->proxy ? "" : "No",
        conf->leave_proxy ? "" : "No");

    return rc;
}

/**
 * \brief Get MLD Snooping global default parameters
 *
 * To get default system parameters in MLD snooping.
 *
 * \param conf      [OUT]    The MLD snooping system default configuration data.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc vtss_appl_ipmc_snp_mld_system_config_default(
    vtss_appl_ipmc_snp_ipv6_global_t            *const conf
)
{
    if (!conf) {
        T_D("Invalid Input!");
        return VTSS_RC_ERROR;
    }

    memset(conf, 0x0, sizeof(vtss_appl_ipmc_snp_ipv6_global_t));
    conf->mode = IPMC_DEF_GLOBAL_STATE_VALUE;
    conf->unregistered_flooding = IPMC_DEF_UNREG_FLOOD_VALUE;
    conf->proxy = IPMC_DEF_PROXY_VALUE;
    conf->leave_proxy = IPMC_DEF_LEAVE_PROXY_VALUE;
    conf->ssm_address.addr[0] = (IPMC_DEF_SSM_PREFIX6_VALUE >> 24) & 0xFF;
    conf->ssm_address.addr[1] = (IPMC_DEF_SSM_PREFIX6_VALUE >> 16) & 0xFF;
    conf->ssm_address.addr[2] = (IPMC_DEF_SSM_PREFIX6_VALUE >> 8) & 0xFF;
    conf->ssm_address.addr[3] = (IPMC_DEF_SSM_PREFIX6_VALUE >> 0) & 0xFF;
    conf->ssm_mask_len = IPMC_DEF_SSM_PREFIX6_LEN_VALUE;

    return VTSS_RC_OK;
}

/**
 * \brief Get MLD Snooping specific port configuration
 *
 * To get configuration of the specific port in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 *
 * \param entry     [OUT]   The current configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_port_config_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_snp6_port_conf_t             *const entry
)
{
    vtss_ifindex_elm_t                  ife;
    vtss_isid_t                         isid;
    u32                                 ptx;
    ipmc_conf_port_group_filtering_t    snp_port_profile;
    ipmc_conf_throttling_max_no_t       snp_port_throttling;
    ipmc_conf_router_port_t             snp_port_mrouter;
    ipmc_conf_fast_leave_port_t         snp_port_immediate_leave;
    mesa_rc                             rc = VTSS_RC_ERROR;

    /* get isid/iport from given IfIndex */
    if (!entry || !ifindex ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_PORT ||
        (ptx = ife.ordinal) >= mesa_port_cnt(0)) {
        T_D("Invalid Input!");
        return rc;
    }

    isid = ife.isid;
    T_D("GET ISID:%u PORT:%u", isid, ptx);

    memset(&snp_port_mrouter, 0x0, sizeof(ipmc_conf_router_port_t));
    memset(&snp_port_immediate_leave, 0x0, sizeof(ipmc_conf_fast_leave_port_t));
    memset(&snp_port_profile, 0x0, sizeof(ipmc_conf_port_group_filtering_t));
    vtss_clear(snp_port_throttling);
    rc = _vtss_appl_ipmc_snp_port_config_get(IPMC_IP_VERSION_MLD, isid, ptx,
                                             &snp_port_profile,
                                             &snp_port_throttling,
                                             &snp_port_mrouter,
                                             &snp_port_immediate_leave);

    if (rc == VTSS_RC_OK) {
        entry->as_router_port = snp_port_mrouter.ports[ptx];
        entry->do_fast_leave = snp_port_immediate_leave.ports[ptx];
        entry->throttling_number = snp_port_throttling.ports[ptx];
        memset(entry->filtering_profile.n, 0x0, sizeof(entry->filtering_profile.n));
        if (strlen(snp_port_profile.addr.profile.name)) {
            strncpy(entry->filtering_profile.n, snp_port_profile.addr.profile.name, strlen(snp_port_profile.addr.profile.name));
        }
    }

    T_D("GET(%s:%d:%u)-%sAsRtr/%sDoFstLve/%u/%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        isid, ptx,
        entry->as_router_port ? "" : "Not",
        entry->do_fast_leave ? "" : "Not",
        entry->throttling_number,
        entry->filtering_profile.n);

    return rc;
}

/**
 * \brief Set MLD Snooping specific port configuration
 *
 * To modify configuration of the specific port in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 * \param entry     [IN]    The revised configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_port_config_set(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_appl_ipmc_snp6_port_conf_t       *const entry
)
{
    vtss_ifindex_elm_t                  ife;
    vtss_isid_t                         isid;
    u32                                 ptx;
    ipmc_conf_port_group_filtering_t    snp_port_profile;
    ipmc_conf_throttling_max_no_t       snp_port_throttling;
    ipmc_conf_router_port_t             snp_port_mrouter;
    ipmc_conf_fast_leave_port_t         snp_port_immediate_leave;
    mesa_rc                             rc = VTSS_RC_ERROR;

    /* get isid/iport from given IfIndex */
    if (!entry || !ifindex ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_PORT ||
        (ptx = ife.ordinal) >= mesa_port_cnt(0)) {
        T_D("Invalid Input!");
        return rc;
    }

    isid = ife.isid;
    T_D("GET ISID:%u PORT:%u", isid, ptx);

    memset(&snp_port_mrouter, 0x0, sizeof(ipmc_conf_router_port_t));
    memset(&snp_port_immediate_leave, 0x0, sizeof(ipmc_conf_fast_leave_port_t));
    memset(&snp_port_profile, 0x0, sizeof(ipmc_conf_port_group_filtering_t));
    vtss_clear(snp_port_throttling);
    rc = _vtss_appl_ipmc_snp_port_config_get(IPMC_IP_VERSION_MLD, isid, ptx,
                                             &snp_port_profile,
                                             &snp_port_throttling,
                                             &snp_port_mrouter,
                                             &snp_port_immediate_leave);

    if (rc == VTSS_RC_OK) {
        snp_port_mrouter.ports[ptx] = entry->as_router_port;
        snp_port_immediate_leave.ports[ptx] = entry->do_fast_leave;
        snp_port_throttling.ports[ptx] = entry->throttling_number;
        memset(snp_port_profile.addr.profile.name, 0x0, sizeof(snp_port_profile.addr.profile.name));
        if (strlen(entry->filtering_profile.n)) {
            strncpy(snp_port_profile.addr.profile.name, entry->filtering_profile.n, strlen(entry->filtering_profile.n));
        }

        rc = _vtss_appl_ipmc_snp_port_config_set(IPMC_IP_VERSION_MLD, isid, ptx,
                                                 &snp_port_profile,
                                                 &snp_port_throttling,
                                                 &snp_port_mrouter,
                                                 &snp_port_immediate_leave);
    }

    T_D("SET(%s:%d:%u)-%sAsRtr/%sDoFstLve/%u/%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        isid, ptx,
        entry->as_router_port ? "" : "Not",
        entry->do_fast_leave ? "" : "Not",
        entry->throttling_number,
        entry->filtering_profile.n);

    return rc;
}

/**
 * \brief Get MLD Snooping specific port default configuration
 *
 * To get default configuration of the specific port in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 *
 * \param entry     [OUT]   The default configuration of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc vtss_appl_ipmc_snp_mld_port_config_default(
    vtss_ifindex_t                              *const ifindex,
    vtss_appl_ipmc_snp6_port_conf_t             *const entry
)
{
    return _vtss_appl_ipmc_snp_port_config_default(IPMC_IP_VERSION_MLD, ifindex, (void *)entry);
}

/**
 * \brief Get MLD Snooping specific port's mrouter status
 *
 * To get mrouter status of the specific port in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the physical port.
 *
 * \param entry     [OUT]   The current mrouter status of the port.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_port_mrouter_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_snp6_mrouter_t               *const entry
)
{
    vtss_ifindex_elm_t  ife;
    vtss_isid_t         isid;
    u32                 ptx;
    mesa_rc             rc = VTSS_RC_ERROR;

    /* get isid/iport from given IfIndex */
    if (!entry || !ifindex ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_PORT ||
        (ptx = ife.ordinal) >= mesa_port_cnt(0)) {
        T_D("Invalid Input!");
        return rc;
    }

    isid = ife.isid;
    T_D("GET ISID:%u PORT:%u", isid, ptx);

    rc = _vtss_appl_ipmc_snp_port_mrouter_get(IPMC_IP_VERSION_MLD, isid, ptx, (void *)entry);

    T_D("GET(%s)-%d",
        rc == VTSS_RC_OK ? "OK" : "NG",
        entry->status);

    return rc;
}

/**
 * \brief Iterator for retrieving MLD Snooping VLAN information key/index
 *
 * To walk information (configuration and status) index of the VLAN in MLD snooping.
 *
 * \param prev      [IN]    Interface index to be used for indexing determination.
 *
 * \param next      [OUT]   The key/index should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_vlan_general_itr(
    const vtss_ifindex_t                        *const prev,
    vtss_ifindex_t                              *const next
)
{
    T_D("enter(prev:%d/next%d)", prev ? vtss_ifindex_cast_to_u32(*prev) : -1, next ? vtss_ifindex_cast_to_u32(*next) : -1);
    return _vtss_appl_ipmc_snp_general_vlan_itr(IPMC_IP_VERSION_MLD, prev, next);
}

/**
 * \brief Get MLD Snooping specific VLAN configuration
 *
 * To get configuration of the specific VLAN in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The current configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_vlan_config_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_snp_ipv6_interface_t         *const entry
)
{
    T_D("enter(ifindex:%d)", ifindex ? vtss_ifindex_cast_to_u32(*ifindex) : -1);
    return _vtss_appl_ipmc_snp_general_vlan_config_get(IPMC_IP_VERSION_MLD, ifindex, (void *)entry);
}

/**
 * \brief Set MLD Snooping specific VLAN configuration
 *
 * To modify configuration of the specific VLAN in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param entry     [IN]    The revised configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_vlan_config_set(
    const vtss_ifindex_t                        *const ifindex,
    const vtss_appl_ipmc_snp_ipv6_interface_t   *const entry
)
{
    T_D("enter(ifindex:%d)", ifindex ? vtss_ifindex_cast_to_u32(*ifindex) : -1);
    return _vtss_appl_ipmc_snp_set(IPMC_IP_VERSION_MLD, ifindex, (void *)entry);
}

/**
 * \brief Get MLD Snooping specific VLAN default configuration
 *
 * To get default configuration of the specific VLAN in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The default configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc vtss_appl_ipmc_snp_mld_vlan_config_default(
    vtss_ifindex_t                              *const ifindex,
    vtss_appl_ipmc_snp_ipv6_interface_t         *const entry
)
{
    return _vtss_appl_ipmc_snp_general_vlan_config_default(IPMC_IP_VERSION_MLD, ifindex, (void *)entry);
}

/**
 * \brief Get MLD Snooping specific VLAN interface's status
 *
 * To get interface's status of the specific VLAN in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 *
 * \param entry     [OUT]   The current configuration of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_vlan_status_get(
    const vtss_ifindex_t                        *const ifindex,
    vtss_appl_ipmc_snp_ipv6_intf_status_t       *const entry
)
{
    T_D("enter(ifindex:%d)", ifindex ? vtss_ifindex_cast_to_u32(*ifindex) : -1);
    return _vtss_appl_ipmc_snp_general_vlan_status_get(IPMC_IP_VERSION_MLD, ifindex, (void *)entry);
}

/**
 * \brief Iterator for retrieving MLD Snooping group address table key/index
 *
 * To walk indexes of the group address table in MLD snooping.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param grps_prev [IN]    IPv6 Address of multicast group to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param grps_next [OUT]   The key/index of multicast group address should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_group_address_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const mesa_ipv6_t                           *const grps_prev,
    mesa_ipv6_t                                 *const grps_next
)
{
    T_D("enter(MLD-GRPADDR)");
    return _vtss_appl_ipmc_snp_group_address_itr(IPMC_IP_VERSION_MLD,
                                                 ifid_prev,
                                                 ifid_next,
                                                 (void *)grps_prev,
                                                 (void *)grps_next);
}

/**
 * \brief Get MLD Snooping specific registered multicast address on a VLAN
 *
 * To get registered multicast group address data of the specific VLAN in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param grpadrs   [IN]    (key) Multicast group address.
 *
 * \param entry     [OUT]   The current status of the registered multicast group address.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_group_address_get(
    const vtss_ifindex_t                        *const ifindex,
    const mesa_ipv6_t                           *const grpadrs,
    vtss_appl_ipmc_snp_grp6_address_t           *const entry
)
{
    vtss_ifindex_elm_t  ife;
    mesa_vid_t          vidx;
    mesa_ipv6_t         grpx;
    i8                  grpString[40];
    mesa_rc             rc = VTSS_RC_ERROR;

    /* get VID from given IfIndex */
    if (!entry || !ifindex || !grpadrs ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid Input!");
        return rc;
    }

    vidx = (mesa_vid_t)ife.ordinal;
    IPMC_LIB_ADRS_CPY(&grpx, grpadrs);
    rc = _vtss_appl_ipmc_snp_group_address_get(IPMC_IP_VERSION_MLD, &vidx, &grpx, (void *)entry);

    memset(grpString, 0x0, sizeof(grpString));
    T_D("GET(%s)-VIDX:%u/GRPX:%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        vidx, misc_ipv6_txt(&grpx, grpString));

    return rc;
}

/**
 * \brief Iterator for retrieving MLD Snooping group source list table key/index
 *
 * To walk indexes of the group source list table in MLD snooping.
 *
 * \param ifid_prev [IN]    Ifindex of VLAN to be used for indexing determination.
 * \param grps_prev [IN]    IPv6 Address of multicast group to be used for indexing determination.
 * \param port_prev [IN]    Ifindex of port to be used for indexing determination.
 * \param adrs_prev [IN]    IPv6 Address of multicasting source to be used for indexing determination.
 *
 * \param ifid_next [OUT]   The key/index of VLAN's Ifindex should be used for the GET operation.
 * \param grps_next [OUT]   The key/index of multicast group address should be used for the GET operation.
 * \param port_next [OUT]   The key/index of port's Ifindex should be used for the GET operation.
 * \param adrs_next [OUT]   The key/index of multicasting source address should be used for the GET operation.
 *                          When IN is NULL, assign the first index.
 *                          When IN is not NULL, assign the next index according to the given IN value.
 *                          The precedence of IN key/index is in given sequential order.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_group_srclist_itr(
    const vtss_ifindex_t                        *const ifid_prev,
    vtss_ifindex_t                              *const ifid_next,
    const mesa_ipv6_t                           *const grps_prev,
    mesa_ipv6_t                                 *const grps_next,
    const vtss_ifindex_t                        *const port_prev,
    vtss_ifindex_t                              *const port_next,
    const mesa_ipv6_t                           *const adrs_prev,
    mesa_ipv6_t                                 *const adrs_next
)
{
    T_D("enter(MLD-SRCLIST)");
    return _vtss_appl_ipmc_snp_group_srclist_itr(IPMC_IP_VERSION_MLD,
                                                 ifid_prev,
                                                 ifid_next,
                                                 (void *)grps_prev,
                                                 (void *)grps_next,
                                                 port_prev,
                                                 port_next,
                                                 (void *)adrs_prev,
                                                 (void *)adrs_next);
}

/**
 * \brief Get MLD Snooping specific registered multicast address and its source list entry
 *
 * To get registered multicast group source list data of the specific VLAN and port in MLD snooping.
 *
 * \param ifindex   [IN]    (key) Interface index - the logical interface index of the VLAN.
 * \param grpadrs   [IN]    (key) Multicast group address.
 * \param stkport   [IN]    (key) Interface index - the logical interface index of the port.
 * \param srcadrs   [IN]    (key) Multicasting source address.
 *
 * \param entry     [OUT]   The current status of the registered multicast group source list entry.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_group_srclist_get(
    const vtss_ifindex_t                        *const ifindex,
    const mesa_ipv6_t                           *const grpadrs,
    const vtss_ifindex_t                        *const stkport,
    const mesa_ipv6_t                           *const srcadrs,
    vtss_appl_ipmc_snp_grp6_srclist_t           *const entry
)
{
    vtss_ifindex_elm_t  ife;
    mesa_vid_t          vidx;
    mesa_ipv6_t         grpx, adrx;
    vtss_isid_t         sidx;
    mesa_port_no_t      prtx;
    i8                  grpString[40], adrString[40];
    mesa_rc             rc = VTSS_RC_ERROR;

    /* get VID from given IfIndex */
    if (!entry || !ifindex || !grpadrs || !stkport || !srcadrs ||
        vtss_ifindex_decompose(*ifindex, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_VLAN) {
        T_D("Invalid Input!");
        return rc;
    }
    vidx = (mesa_vid_t)ife.ordinal;
    /* get ISID/PORT from given IfIndex */
    if (vtss_ifindex_decompose(*stkport, &ife) != VTSS_RC_OK ||
        ife.iftype != VTSS_IFINDEX_TYPE_PORT ||
        (prtx = ife.ordinal) >= mesa_port_cnt(0)) {
        T_D("Invalid Input!");
        return rc;
    }
    sidx = ife.isid;

    IPMC_LIB_ADRS_CPY(&grpx, grpadrs);
    IPMC_LIB_ADRS_CPY(&adrx, srcadrs);
    rc = _vtss_appl_ipmc_snp_group_srclist_get(IPMC_IP_VERSION_MLD, &vidx, &grpx, &sidx, &prtx, &adrx, (void *)entry);

    memset(grpString, 0x0, sizeof(grpString));
    memset(adrString, 0x0, sizeof(adrString));
    T_D("GET(%s)-VIDX:%u/GRPX:%s/SIDX:%d/PRTX:%d/ADRX:%s",
        rc == VTSS_RC_OK ? "OK" : "NG",
        vidx, misc_ipv6_txt(&grpx, grpString),
        sidx, prtx, misc_ipv6_txt(&adrx, adrString));

    return rc;
}

/**
 * \brief MLD Snooping Control ACTION for clearing statistics
 *
 * To clear the statistics based on the given VLAN ifIndex of MLD interface.
 *
 * \param ifindex   [IN]    Interface index - the logical interface index of the VLAN.
 *
 * \return VTSS_RC_OK for success operation.
 */
mesa_rc
vtss_appl_ipmc_snp_mld_statistics_clear_by_vlan_act(
    const vtss_ifindex_t                        *const ifindex
)
{
    T_D("enter(ifindex:%d)", ifindex ? vtss_ifindex_cast_to_u32(*ifindex) : -1);
    return _vtss_appl_ipmc_snp_statistics_clear_by_vlan(IPMC_IP_VERSION_MLD, ifindex);
}
#endif /* VTSS_SW_OPTION_SMB_IPMC */
