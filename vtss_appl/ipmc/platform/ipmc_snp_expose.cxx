/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "ipmc_snp_serializer.hxx"

vtss_enum_descriptor_t ipmc_snp4_mrouterType_txt[] {
    {VTSS_APPL_IPMC_MROUTER4_NONE,      "none"},
    {VTSS_APPL_IPMC_MROUTER4_STATIC,    "static"},
    {VTSS_APPL_IPMC_MROUTER4_DYNAMIC,   "dynamic"},
    {VTSS_APPL_IPMC_MROUTER4_BOTH,      "both"},
    {0, 0}
};

vtss_enum_descriptor_t ipmc_snp4_querierType_txt[] {
    {VTSS_APPL_IPMC_QUERIER4_DISABLED,  "disabled"},
    {VTSS_APPL_IPMC_QUERIER4_INIT,      "initial"},
    {VTSS_APPL_IPMC_QUERIER4_IDLE,      "idle"},
    {VTSS_APPL_IPMC_QUERIER4_ACTIVE,    "active"},
    {0, 0}
};

#ifdef VTSS_SW_OPTION_SMB_IPMC
vtss_enum_descriptor_t ipmc_snp4_compatiType_txt[] {
    {VTSS_APPL_IPMC_COMPATI4_AUTO,      "auto"},
    {VTSS_APPL_IPMC_COMPATI4_V1,        "igmpv1"},
    {VTSS_APPL_IPMC_COMPATI4_V2,        "igmpv2"},
    {VTSS_APPL_IPMC_COMPATI4_V3,        "igmpv3"},
    {0, 0}
};

vtss_enum_descriptor_t ipmc_snp4_sfmType_txt[] {
    {VTSS_APPL_IPMC_SF4_MODE_EXCLUDE,   "exclude"},
    {VTSS_APPL_IPMC_SF4_MODE_INCLUDE,   "include"},
    {VTSS_APPL_IPMC_SF4_MODE_NONE,      "none"},
    {0, 0}
};

vtss_enum_descriptor_t ipmc_snp4_srclistType_txt[] {
    {VTSS_APPL_IPMC_ACTION4_DENY,       "deny"},
    {VTSS_APPL_IPMC_ACTION4_PERMIT,     "permit"},
    {0, 0}
};

vtss_enum_descriptor_t ipmc_snp6_mrouterType_txt[] {
    {VTSS_APPL_IPMC_MROUTER6_NONE,      "none"},
    {VTSS_APPL_IPMC_MROUTER6_STATIC,    "static"},
    {VTSS_APPL_IPMC_MROUTER6_DYNAMIC,   "dynamic"},
    {VTSS_APPL_IPMC_MROUTER6_BOTH,      "both"},
    {0, 0}
};

vtss_enum_descriptor_t ipmc_snp6_querierType_txt[] {
    {VTSS_APPL_IPMC_QUERIER6_DISABLED,  "disabled"},
    {VTSS_APPL_IPMC_QUERIER6_INIT,      "initial"},
    {VTSS_APPL_IPMC_QUERIER6_IDLE,      "idle"},
    {VTSS_APPL_IPMC_QUERIER6_ACTIVE,    "active"},
    {0, 0}
};

vtss_enum_descriptor_t ipmc_snp6_compatiType_txt[] {
    {VTSS_APPL_IPMC_COMPATI6_AUTO,      "auto"},
    {VTSS_APPL_IPMC_COMPATI6_V1,        "mldv1"},
    {VTSS_APPL_IPMC_COMPATI6_V2,        "mldv2"},
    {0, 0}
};

vtss_enum_descriptor_t ipmc_snp6_sfmType_txt[] {
    {VTSS_APPL_IPMC_SF6_MODE_EXCLUDE,   "exclude"},
    {VTSS_APPL_IPMC_SF6_MODE_INCLUDE,   "include"},
    {VTSS_APPL_IPMC_SF6_MODE_NONE,      "none"},
    {0, 0}
};

vtss_enum_descriptor_t ipmc_snp6_srclistType_txt[] {
    {VTSS_APPL_IPMC_ACTION6_DENY,       "deny"},
    {VTSS_APPL_IPMC_ACTION6_PERMIT,     "permit"},
    {0, 0}
};

mesa_rc vtss_appl_ipmc_snooping_control_statistics_dummy_get(vtss_ifindex_t *const act_value) {
    if (act_value) {
        *act_value = VTSS_IFINDEX_VLAN_OFFSET;
    }

    return VTSS_RC_OK;
}

#endif /* VTSS_SW_OPTION_SMB_IPMC */
