/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#ifndef __VTSS_IPMC_SNOOPING_SERIALIZER_HXX__
#define __VTSS_IPMC_SNOOPING_SERIALIZER_HXX__

#include "vtss_appl_serialize.hxx"
#include "vtss/appl/ipmc_snooping.h"
#include "vtss/appl/types.hxx"


#if defined(VTSS_SW_OPTION_PRIVATE_MIB) && !defined(VTSS_SW_OPTION_SMB_IPMC)
// the serializer must always emit the same mib/spec for all platforms, and this
// is not the case when if-defs are used. It is not a problem here as
// VTSS_SW_OPTION_SMB_IPMC is always defined when the serializer are used
#error "The ipmc serializer does not work without VTSS_SW_OPTION_SMB_IPMC"
#endif

mesa_rc vtss_appl_ipmc_snooping_control_statistics_dummy_get(vtss_ifindex_t *const act_value);

/*****************************************************************************
    Enumerator serializer
*****************************************************************************/

extern vtss_enum_descriptor_t ipmc_snp4_mrouterType_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ipmc_mrtr4_status_t, 
                         "IpmcSnoopingIgmpRouterPortStatusEnum",
                         ipmc_snp4_mrouterType_txt,
                         "This enumeration indicates the router port status from IGMP snooping.");

extern vtss_enum_descriptor_t ipmc_snp4_querierType_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ipmc_qry4_states_t, 
                         "IpmcSnoopingIgmpVlanStatusQuerierStatusEnum",
                         ipmc_snp4_querierType_txt,
                         "This enumeration indicates the querier status for IGMP snooping VLAN interface.");

#ifdef VTSS_SW_OPTION_SMB_IPMC
extern vtss_enum_descriptor_t ipmc_snp4_compatiType_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ipmc_compati4_t, 
                         "IpmcSnoopingIgmpInterfaceCompatibilityEnum",
                         ipmc_snp4_compatiType_txt,
                         "This enumeration indicates the version compatibility for IGMP snooping VLAN interface.");

extern vtss_enum_descriptor_t ipmc_snp4_sfmType_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ipmc_sfm4_mode_t, 
                         "IpmcSnoopingIgmpGroupSrcListGroupFilterModeEnum",
                         ipmc_snp4_sfmType_txt,
                         "This enumeration indicates the group filter mode for a IGMP group address.");

extern vtss_enum_descriptor_t ipmc_snp4_srclistType_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ipmc_action4_t, 
                         "IpmcSnoopingIgmpGroupSrcListSourceTypeEnum",
                         ipmc_snp4_srclistType_txt,
                         "This enumeration indicates the source filtering type from IGMP snooping.");

extern vtss_enum_descriptor_t ipmc_snp6_mrouterType_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ipmc_mrtr6_status_t, 
                         "IpmcSnoopingMldRouterPortStatusEnum",
                         ipmc_snp6_mrouterType_txt,
                         "This enumeration indicates the router port status from MLD snooping.");

extern vtss_enum_descriptor_t ipmc_snp6_querierType_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ipmc_qry6_states_t, 
                         "IpmcSnoopingMldVlanStatusQuerierStatusEnum",
                         ipmc_snp6_querierType_txt,
                         "This enumeration indicates the querier status for MLD snooping VLAN interface.");

extern vtss_enum_descriptor_t ipmc_snp6_compatiType_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ipmc_compati6_t, 
                         "IpmcSnoopingMldInterfaceCompatibilityEnum",
                         ipmc_snp6_compatiType_txt,
                         "This enumeration indicates the version compatibility for MLD snooping VLAN interface.");

extern vtss_enum_descriptor_t ipmc_snp6_sfmType_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ipmc_sfm6_mode_t, 
                         "IpmcSnoopingMldGroupSrcListGroupFilterModeEnum",
                         ipmc_snp6_sfmType_txt,
                         "This enumeration indicates the group filter mode for a MLD group address.");

extern vtss_enum_descriptor_t ipmc_snp6_srclistType_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ipmc_action6_t, 
                         "IpmcSnoopingMldGroupSrcListSourceEnum",
                         ipmc_snp6_srclistType_txt,
                         "This enumeration indicates the source filtering type from MLD snooping.");
#endif /* VTSS_SW_OPTION_SMB_IPMC */

/*****************************************************************************
    Index serializer
*****************************************************************************/
VTSS_SNMP_TAG_SERIALIZE(ipmc_snp_port_ifindex_idx, vtss_ifindex_t, a, s) {
    a.add_leaf(
        vtss::AsInterfaceIndex(s.inner),
        vtss::tag::Name("PortIndex"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(0),
        vtss::tag::Description("Logical interface number of the physical port.")
    );
}

VTSS_SNMP_TAG_SERIALIZE(ipmc_snp_vlan_ifindex_idx, vtss_ifindex_t, a, s) {
    a.add_leaf(
        vtss::AsInterfaceIndex(s.inner),
        vtss::tag::Name("IfIndex"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(0),
        vtss::tag::Description("Logical interface number of the VLAN interface.")
    );
}

VTSS_SNMP_TAG_SERIALIZE(ipmc_snp_ipv4_grp_addr_idx, mesa_ipv4_t, a, s) {
    a.add_leaf(
        vtss::AsIpv4(s.inner),
        vtss::tag::Name("GroupAddress"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(0),
        vtss::tag::Description("Assigned IPv4 multicast address.")
    );
}

#ifdef VTSS_SW_OPTION_SMB_IPMC
VTSS_SNMP_TAG_SERIALIZE(ipmc_snp_ipv4_src_addr_idx, mesa_ipv4_t, a, s) {
    a.add_leaf(
        vtss::AsIpv4(s.inner),
        vtss::tag::Name("HostAddress"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(0),
        vtss::tag::Description("Assigned IPv4 source address.")
    );
}

VTSS_SNMP_TAG_SERIALIZE(ipmc_snp_ipv6_grp_addr_idx, mesa_ipv6_t, a, s ) {
    a.add_leaf(
        s.inner,
        vtss::tag::Name("GroupAddress"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(0),
        vtss::tag::Description("Assigned IPv6 multicast address.")
    );
}

VTSS_SNMP_TAG_SERIALIZE(ipmc_snp_ipv6_src_addr_idx, mesa_ipv6_t, a, s ) {
    a.add_leaf(
        s.inner,
        vtss::tag::Name("HostAddress"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(0),
        vtss::tag::Description("Assigned IPv6 source address.")
    );
}
#endif /* VTSS_SW_OPTION_SMB_IPMC */

/*****************************************************************************
    Data serializer
*****************************************************************************/
template<typename T>
void serialize(T &a, vtss_appl_ipmc_snp_ipv4_global_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_snp_ipv4_global_t"));
    int ix = 0;

    m.add_leaf(
        vtss::AsBool(s.mode),
        vtss::tag::Name("AdminState"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the IGMP snooping global functionality.")
    );

    m.add_leaf(
        vtss::AsBool(s.unregistered_flooding),
        vtss::tag::Name("UnregisteredFlooding"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the control for flooding unregistered IPv4 multicast traffic.")
    );

#ifdef VTSS_SW_OPTION_SMB_IPMC
    m.add_leaf(
        vtss::AsIpv4(s.ssm_address),
        vtss::tag::Name("SsmRangeAddress"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The address prefix value defined for IGMP SSM service model.")
    );

    m.add_leaf(
        s.ssm_mask_len,
        vtss::tag::Name("SsmRangeMask"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The address prefix length defined for IGMP SSM service model.")
    );

    m.add_leaf(
        vtss::AsBool(s.proxy),
        vtss::tag::Name("Proxy"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the IGMP proxy functionality.")
    );

    m.add_leaf(
        vtss::AsBool(s.leave_proxy),
        vtss::tag::Name("LeaveProxy"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the IGMP leave-proxy functionality.")
    );
#endif /* VTSS_SW_OPTION_SMB_IPMC */
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_snp4_port_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_snp4_port_conf_t"));
    int ix = 0;

    m.add_leaf(
        vtss::AsBool(s.as_router_port),
        vtss::tag::Name("AsRouterPort"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the IGMP static router port functionality.")
    );

    m.add_leaf(
        vtss::AsBool(s.do_fast_leave),
        vtss::tag::Name("DoFastLeave"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the IGMP fast leave functionality.")
    );

#ifdef VTSS_SW_OPTION_SMB_IPMC
    m.add_leaf(
        s.throttling_number,
        vtss::tag::Name("ThrottlingNumber"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The maximum number of groups to be registered on the specific port.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.filtering_profile.n, sizeof(s.filtering_profile.n)),
        vtss::tag::Name("FilteringProfile"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The profile used for IGMP filtering per-port basis.")
    );
#endif /* VTSS_SW_OPTION_SMB_IPMC */
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_snp_ipv4_interface_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_snp_ipv4_interface_t"));
    int ix = 0;

    m.add_leaf(
        vtss::AsBool(s.admin_state),
        vtss::tag::Name("AdminState"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the IGMP snooping per-VLAN functionality.")
    );

    m.add_leaf(
        vtss::AsBool(s.querier_election),
        vtss::tag::Name("QuerierElection"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the capability to run IGMP Querier election per-VLAN basis.")
    );

#ifdef VTSS_SW_OPTION_SMB_IPMC
    m.add_leaf(
        vtss::AsIpv4(s.querier_address),
        vtss::tag::Name("QuerierAddress"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The static IPv4 source address of the specific IGMP interface "
            "for seding IGMP Query message with respect to IGMP Querier election.")
    );

    m.add_leaf(
        s.compatibility,
        vtss::tag::Name("Compatibility"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The compatibility control for IGMP snooping to run the "
            "corresponding protocol version.")
    );

    m.add_leaf(
        s.priority,
        vtss::tag::Name("Priority"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used for prioritizing the IGMP control frames "
            "to be sent for IGMP snooping.")
    );

    m.add_leaf(
        s.robustness_variable,
        vtss::tag::Name("Rv"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting Robustness Variable is used to control IGMP "
            "protocol stack as stated in RFC-3376 8.1.")
    );

    m.add_leaf(
        s.query_interval,
        vtss::tag::Name("Qi"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting Query Interval is used to control IGMP "
            "protocol stack as stated in RFC-3376 8.2.")
    );

    m.add_leaf(
        s.query_response_interval,
        vtss::tag::Name("Qri"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting Query Response Intervalis used to control "
            "IGMP protocol stack as stated in RFC-3376 8.3.")
    );

    m.add_leaf(
        s.last_listener_query_interval,
        vtss::tag::Name("Lmqi"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting Last Member Query Interval is used to "
            "control IGMP protocol stack as stated in RFC-3376 8.8.")
    );

    m.add_leaf(
        s.unsolicited_report_interval,
        vtss::tag::Name("Uri"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting Unsolicited Report Interval is used to "
            "control IGMP protocol stack as stated in RFC-3376 8.11.")
    );
#endif /* VTSS_SW_OPTION_SMB_IPMC */
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_snp4_mrouter_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_snp4_mrouter_t"));
    int ix = 0;

    m.add_leaf(
        s.status,
        vtss::tag::Name("Status"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The IGMP snooping router port status.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_snp_ipv4_intf_status_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_snp_ipv4_intf_status_t"));
    int ix = 0;

    m.add_leaf(
        s.querier_state,
        vtss::tag::Name("QuerierStatus"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The IGMP Querier status of the specific VLAN interface.")
    );

    m.add_leaf(
        vtss::AsIpv4(s.active_querier_address),
        vtss::tag::Name("ActiveQuerierAddress"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The active IGMP Querier address on the specific VLAN interface.")
    );

    m.add_leaf(
        s.querier_up_time,
        vtss::tag::Name("QuerierUptime"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the in operation timer for the specific "
            "interface act as a IGMP Querier.")
    );

    m.add_leaf(
        s.query_interval,
        vtss::tag::Name("QueryInterval"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used to control IGMP protocol stack "
            "as stated in RFC-3376 8.2.")
    );

    m.add_leaf(
        s.startup_query_count,
        vtss::tag::Name("StartupQueryCount"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used to control IGMP protocol stack "
            "as stated in RFC-3376 8.7.")
    );

    m.add_leaf(
        s.querier_expiry_time,
        vtss::tag::Name("QuerierExpiryTime"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used to control IGMP protocol stack "
            "as stated in RFC-3376 8.5.")
    );

    m.add_leaf(
        s.querier_version,
        vtss::tag::Name("QuerierVersion"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current IGMP version that the IGMP "
            "interface should behave in running IGMP protocol as a router.")
    );

    m.add_leaf(
        s.querier_present_timeout,
        vtss::tag::Name("QuerierPresentTimeout"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used to control IGMP protocol stack "
            "as stated in RFC-3376 8.12.")
    );

    m.add_leaf(
        s.host_version,
        vtss::tag::Name("HostVersion"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current IGMP version that the IGMP "
            "interface should behave in running IGMP protocol as a host.")
    );

    m.add_leaf(
        s.host_present_timeout,
        vtss::tag::Name("HostPresentTimeout"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used to control IGMP protocol stack "
            "as stated in RFC-3376 8.13.")
    );

    m.add_leaf(
        s.counter_tx_query,
        vtss::tag::Name("CounterTxQuery"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "IGMP interface for transmitting IGMP Query control frames.")
    );

    m.add_leaf(
        s.counter_tx_specific_query,
        vtss::tag::Name("CounterTxSpecificQuery"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "IGMP interface for transmitting IGMP Specific Query control frames.")
    );

    m.add_leaf(
        s.counter_rx_query,
        vtss::tag::Name("CounterRxQuery"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "IGMP interface for receiving IGMP Query control frames.")
    );

    m.add_leaf(
        s.counter_rx_v1_join,
        vtss::tag::Name("CounterRxV1Join"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "IGMP interface for receiving IGMPv1 Join control frames.")
    );

    m.add_leaf(
        s.counter_rx_v2_join,
        vtss::tag::Name("CounterRxV2Join"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "IGMP interface for receiving IGMPv2 Join control frames.")
    );

    m.add_leaf(
        s.counter_rx_v2_leave,
        vtss::tag::Name("CounterRxV2Leave"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "IGMP interface for receiving IGMPv2 Leave control frames.")
    );

    m.add_leaf(
        s.counter_rx_v3_join,
        vtss::tag::Name("CounterRxV3Join"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "IGMP interface for receiving IGMPv3 Join control frames.")
    );

    m.add_leaf(
        s.counter_rx_errors,
        vtss::tag::Name("CounterRxErrors"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "IGMP interface for receiving invalid IGMP control frames.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_snp_grp4_address_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_snp_grp4_address_t"));
    int                     ix = 0;
    vtss::PortListStackable &list = (vtss::PortListStackable &)s.member_ports;

    m.add_leaf(
        list,
        vtss::tag::Name("MemberPorts"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote the memberships of the registered "
            "multicast group address from IGMP snooping.")
    );

    m.add_leaf(
        vtss::AsBool(s.hardware_switch),
        vtss::tag::Name("HardwareSwitch"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote whether the multicast traffic "
            "destined to the registered group address could be forwarding by switch "
            "hardware or not.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_snp_grp_adrs_cnt_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_snp_grp_adrs_cnt_t"));
    int ix = 0;

    m.add_leaf(
        s.igmp_snp_grps,
        vtss::tag::Name("FromIgmp"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote the total number of registered "
            "multicast group address from IGMP snooping.")
    );

#ifdef VTSS_SW_OPTION_SMB_IPMC
    m.add_leaf(
        s.mld_snp_grps,
        vtss::tag::Name("FromMld"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote the total number of registered "
            "multicast group address from MLD snooping.")
    );
#endif /* VTSS_SW_OPTION_SMB_IPMC */
}

#ifdef VTSS_SW_OPTION_SMB_IPMC
template<typename T>
void serialize(T &a, vtss_appl_ipmc_snp_grp4_srclist_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_snp_grp4_srclist_t"));
    int ix = 0;

    m.add_leaf(
        s.group_filter_mode,
        vtss::tag::Name("GroupFilterMode"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote the source filtering mode of "
            "the specific registered multicast group address from IGMP snooping.")
    );

    m.add_leaf(
        s.filter_timer,
        vtss::tag::Name("FilterTimer"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to count down the timer for the specific "
            "multicast group's filtering mode transition.")
    );

    m.add_leaf(
        s.source_type,
        vtss::tag::Name("SourceType"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote the filtering type of the specific "
            "source address in multicasting to the registered group address.")
    );

    m.add_leaf(
        s.source_timer,
        vtss::tag::Name("SourceTimer"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to count down the timer for purging the specific "
            "source address from the registered multicast group's source list.")
    );

    m.add_leaf(
        vtss::AsBool(s.hardware_switch),
        vtss::tag::Name("HardwareFilter"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote whether the traffic destined to "
            "the multicast group address from the specific source address could be "
            "forwarding by switch hardware or not.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_snp_ipv6_global_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_snp_ipv6_global_t"));
    int ix = 0;

    m.add_leaf(
        vtss::AsBool(s.mode),
        vtss::tag::Name("AdminState"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the MLD snooping global functionality.")
    );

    m.add_leaf(
        vtss::AsBool(s.unregistered_flooding),
        vtss::tag::Name("UnregisteredFlooding"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the control for flooding unregistered IPv6 multicast traffic.")
    );

    m.add_leaf(
        s.ssm_address,
        vtss::tag::Name("SsmRangeAddress"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The address prefix value defined for MLD SSM service model.")
    );

    m.add_leaf(
        s.ssm_mask_len,
        vtss::tag::Name("SsmRangeMask"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The address prefix length defined for MLD SSM service model.")
    );

    m.add_leaf(
        vtss::AsBool(s.proxy),
        vtss::tag::Name("Proxy"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the MLD proxy functionality.")
    );

    m.add_leaf(
        vtss::AsBool(s.leave_proxy),
        vtss::tag::Name("LeaveProxy"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the MLD leave-proxy functionality.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_snp6_port_conf_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_snp6_port_conf_t"));
    int ix = 0;

    m.add_leaf(
        vtss::AsBool(s.as_router_port),
        vtss::tag::Name("AsRouterPort"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the MLD static router port functionality.")
    );

    m.add_leaf(
        vtss::AsBool(s.do_fast_leave),
        vtss::tag::Name("DoFastLeave"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the MLD fast leave functionality.")
    );

    m.add_leaf(
        s.throttling_number,
        vtss::tag::Name("ThrottlingNumber"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The maximum number of groups to be registered on the specific port.")
    );

    m.add_leaf(
        vtss::AsDisplayString(s.filtering_profile.n, sizeof(s.filtering_profile.n)),
        vtss::tag::Name("FilteringProfile"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The profile used for MLD filtering per-port basis.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_snp_ipv6_interface_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_snp_ipv6_interface_t"));
    int ix = 0;

    m.add_leaf(
        vtss::AsBool(s.admin_state),
        vtss::tag::Name("AdminState"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the MLD snooping per-VLAN functionality.")
    );

    m.add_leaf(
        vtss::AsBool(s.querier_election),
        vtss::tag::Name("QuerierElection"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("Enable/Disable the capability to run MLD Querier election per-VLAN basis.")
    );

    m.add_leaf(
        s.compatibility,
        vtss::tag::Name("Compatibility"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The compatibility control for MLD snooping to run the "
            "corresponding protocol version.")
    );

    m.add_leaf(
        s.priority,
        vtss::tag::Name("Priority"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used for prioritizing the MLD control frames "
            "to be sent for MLD snooping.")
    );

    m.add_leaf(
        s.robustness_variable,
        vtss::tag::Name("Rv"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting Robustness Variableis used to control "
            "MLD protocol stack as stated in RFC-3810 9.1.")
    );

    m.add_leaf(
        s.query_interval,
        vtss::tag::Name("Qi"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting Query Intervalis used to control "
            "MLD protocol stack as stated in RFC-3810 9.2.")
    );

    m.add_leaf(
        s.query_response_interval,
        vtss::tag::Name("Qri"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting Query Response Intervalis used to "
            "control MLD protocol stack as stated in RFC-3810 9.3.")
    );

    m.add_leaf(
        s.last_listener_query_interval,
        vtss::tag::Name("Llqi"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting Last Listener Query Interval is used "
            "to control MLD protocol stack as stated in RFC-3810 9.8.")
    );

    m.add_leaf(
        s.unsolicited_report_interval,
        vtss::tag::Name("Uri"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting Unsolicited Report Intervalis used to "
            "control MLD protocol stack as stated in RFC-3810 9.11.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_snp6_mrouter_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_snp6_mrouter_t"));
    int ix = 0;

    m.add_leaf(
        s.status,
        vtss::tag::Name("Status"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The MLD snooping router port status.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_snp_ipv6_intf_status_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_snp_ipv6_intf_status_t"));
    int ix = 0;

    m.add_leaf(
        s.querier_state,
        vtss::tag::Name("QuerierStatus"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The MLD Querier status of the specific VLAN interface.")
    );

    m.add_leaf(
        s.active_querier_address,
        vtss::tag::Name("ActiveQuerierAddress"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("The active MLD Querier address on the specific VLAN interface.")
    );

    m.add_leaf(
        s.querier_up_time,
        vtss::tag::Name("QuerierUptime"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the in operation timer for the specific "
            "interface act as a MLD Querier.")
    );

    m.add_leaf(
        s.query_interval,
        vtss::tag::Name("QueryInterval"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used to control MLD protocol stack "
            "as stated in RFC-3810 9.2.")
    );

    m.add_leaf(
        s.startup_query_count,
        vtss::tag::Name("StartupQueryCount"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used to control MLD protocol stack "
            "as stated in RFC-3810 9.7.")
    );

    m.add_leaf(
        s.querier_expiry_time,
        vtss::tag::Name("QuerierExpiryTime"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used to control MLD protocol stack "
            "as stated in RFC-3810 9.5.")
    );

    m.add_leaf(
        s.querier_version,
        vtss::tag::Name("QuerierVersion"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current MLD version that the MLD "
            "interface should behave in running MLD protocol as a router.")
    );

    m.add_leaf(
        s.querier_present_timeout,
        vtss::tag::Name("QuerierPresentTimeout"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used to control MLD protocol stack "
            "as stated in RFC-3810 9.12.")
    );

    m.add_leaf(
        s.host_version,
        vtss::tag::Name("HostVersion"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current MLD version that the MLD "
            "interface should behave in running MLD protocol as a host.")
    );

    m.add_leaf(
        s.host_present_timeout,
        vtss::tag::Name("HostPresentTimeout"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("This setting is used to control MLD protocol stack "
            "as stated in RFC-3810 9.13.")
    );

    m.add_leaf(
        s.counter_tx_query,
        vtss::tag::Name("CounterTxQuery"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "MLD interface for transmitting MLD Query control frames.")
    );

    m.add_leaf(
        s.counter_tx_specific_query,
        vtss::tag::Name("CounterTxSpecificQuery"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "MLD interface for transmitting MLD Specific Query control frames.")
    );

    m.add_leaf(
        s.counter_rx_query,
        vtss::tag::Name("CounterRxQuery"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "MLD interface for receiving MLD Query control frames.")
    );

    m.add_leaf(
        s.counter_rx_v1_report,
        vtss::tag::Name("CounterRxV1Report"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "MLD interface for receiving MLDv1 Report control frames.")
    );

    m.add_leaf(
        s.counter_rx_v1_done,
        vtss::tag::Name("CounterRxV1Done"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "MLD interface for receiving MLDv1 Done control frames.")
    );

    m.add_leaf(
        s.counter_rx_v2_report,
        vtss::tag::Name("CounterRxV2Report"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "MLD interface for receiving MLDv2 Report control frames.")
    );

    m.add_leaf(
        s.counter_rx_errors,
        vtss::tag::Name("CounterRxErrors"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It presents the current packet count on the specific "
            "MLD interface for receiving invalid MLD control frames.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_snp_grp6_address_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_snp_grp6_address_t"));
    int                     ix = 0;
    vtss::PortListStackable &list = (vtss::PortListStackable &)s.member_ports;

    m.add_leaf(
        list,
        vtss::tag::Name("MemberPorts"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote the memberships of the registered "
            "multicast group address from MLD snooping.")
    );

    m.add_leaf(
        vtss::AsBool(s.hardware_switch),
        vtss::tag::Name("HardwareSwitch"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote whether the multicast traffic "
            "destined to the registered group address could be forwarding by switch "
            "hardware or not.")
    );
}

template<typename T>
void serialize(T &a, vtss_appl_ipmc_snp_grp6_srclist_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_ipmc_snp_grp6_srclist_t"));
    int ix = 0;

    m.add_leaf(
        s.group_filter_mode,
        vtss::tag::Name("GroupFilterMode"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote the source filtering mode of "
            "the specific registered multicast group address from MLD snooping.")
    );

    m.add_leaf(
        s.filter_timer,
        vtss::tag::Name("FilterTimer"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to count down the timer for the specific "
            "multicast group's filtering mode transition.")
    );

    m.add_leaf(
        s.source_type,
        vtss::tag::Name("SourceType"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote the filtering type of the specific "
            "source address in multicasting to the registered group address.")
    );

    m.add_leaf(
        s.source_timer,
        vtss::tag::Name("SourceTimer"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to count down the timer for purging the specific "
            "source address from the registered multicast group's source list.")
    );

    m.add_leaf(
        vtss::AsBool(s.hardware_switch),
        vtss::tag::Name("HardwareFilter"),
        vtss::expose::snmp::Status::Current,
        vtss::expose::snmp::OidElementValue(ix++),
        vtss::tag::Description("It is used to denote whether the traffic destined to "
            "the multicast group address from the specific source address could be "
            "forwarding by switch hardware or not.")
    );
}
#endif /* VTSS_SW_OPTION_SMB_IPMC */

namespace vtss {
namespace appl {
namespace ipmc_snooping {
namespace interfaces {

struct IpmcSnoopingIgmpGlobalsConfig {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_appl_ipmc_snp_ipv4_global_t *>
    > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ipmc_snp_ipv4_global_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snp_igmp_system_config_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ipmc_snp_igmp_system_config_set);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_ipmc_snp_igmp_system_config_default);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_IPMC);
};

struct IpmcSnoopingIgmpPortConfigTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_snp4_port_conf_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for managing extra IGMP snooping helper features per port basis";

    static constexpr const char *index_description =
        "Each port has a set of parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_snp_port_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ipmc_snp4_port_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snp_igmp_port_config_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_snp_general_port_itr);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ipmc_snp_igmp_port_config_set);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_ipmc_snp_igmp_port_config_default);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_IPMC);
};

struct IpmcSnoopingIgmpVlanConfigTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_snp_ipv4_interface_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for managing IGMP Snooping VLAN interface entries.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_snp_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ipmc_snp_ipv4_interface_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snp_igmp_vlan_config_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_snp_igmp_vlan_general_itr);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ipmc_snp_igmp_vlan_config_set);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_ipmc_snp_igmp_vlan_config_default);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_IPMC);
};

#ifdef VTSS_SW_OPTION_SMB_IPMC
struct IpmcSnoopingMldGlobalsConfig {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_appl_ipmc_snp_ipv6_global_t *>
    > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ipmc_snp_ipv6_global_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snp_mld_system_config_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ipmc_snp_mld_system_config_set);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_ipmc_snp_mld_system_config_default);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_IPMC);
};

struct IpmcSnoopingMldPortConfigTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_snp6_port_conf_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for managing extra MLD snooping helper features per port basis";

    static constexpr const char *index_description =
        "Each port has a set of parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_snp_port_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ipmc_snp6_port_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snp_mld_port_config_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_snp_general_port_itr);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ipmc_snp_mld_port_config_set);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_ipmc_snp_mld_port_config_default);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_IPMC);
};

struct IpmcSnoopingMldVlanConfigTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_snp_ipv6_interface_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for managing MLD Snooping VLAN interface entries.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_snp_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ipmc_snp_ipv6_interface_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snp_mld_vlan_config_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_snp_mld_vlan_general_itr);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ipmc_snp_mld_vlan_config_set);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_ipmc_snp_mld_vlan_config_default);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_IPMC);
};
#endif /* VTSS_SW_OPTION_SMB_IPMC */

/* xxxIpmcSnoopingStatus:xxxIpmcSnoopingGroupAddressCount */

struct IpmcSnoopingStatusGrpCnt {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_appl_ipmc_snp_grp_adrs_cnt_t *>
    > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ipmc_snp_grp_adrs_cnt_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snp_grp_adr_cnt_get);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_IPMC);
};

struct IpmcSnoopingIgmpPortStatusTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_snp4_mrouter_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for displaying the router port status from IGMP snooping configuration.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_snp_port_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ipmc_snp4_mrouter_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snp_igmp_port_mrouter_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_snp_general_port_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_IPMC);
};

struct IpmcSnoopingIgmpVlanStatusTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_snp_ipv4_intf_status_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for displaying the per VLAN interface status in IGMP snooping configuration.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_snp_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ipmc_snp_ipv4_intf_status_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snp_igmp_vlan_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_snp_igmp_vlan_general_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_IPMC);
};

struct IpmcSnoopingIgmpGrpadrsTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamKey<mesa_ipv4_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_snp_grp4_address_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for displaying the registered IPv4 multicast group address status from IGMP snooping.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_snp_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(mesa_ipv4_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, ipmc_snp_ipv4_grp_addr_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_ipmc_snp_grp4_address_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snp_igmp_group_address_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_snp_igmp_group_address_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_IPMC);
};

#ifdef VTSS_SW_OPTION_SMB_IPMC
struct IpmcSnoopingIgmpSrclistTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamKey<mesa_ipv4_t *>,
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamKey<mesa_ipv4_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_snp_grp4_srclist_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for displaying the address SFM (a.k.a Source List Multicast) status in source list of the registered IPv4 multicast group from IGMP snooping.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_snp_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(mesa_ipv4_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, ipmc_snp_ipv4_grp_addr_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3));
        serialize(h, ipmc_snp_port_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_4(mesa_ipv4_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(4));
        serialize(h, ipmc_snp_ipv4_src_addr_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_5(vtss_appl_ipmc_snp_grp4_srclist_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(5));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snp_igmp_group_srclist_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_snp_igmp_group_srclist_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_IPMC);
};

struct IpmcSnoopingMldPortStatusTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_snp6_mrouter_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for displaying the router port status from MLD snooping configuration.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_snp_port_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ipmc_snp6_mrouter_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snp_mld_port_mrouter_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_snp_general_port_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_IPMC);
};

struct IpmcSnoopingMldVlanStatusTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_snp_ipv6_intf_status_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for displaying the per VLAN interface status in MLD snooping configuration.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_snp_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ipmc_snp_ipv6_intf_status_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snp_mld_vlan_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_snp_mld_vlan_general_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_IPMC);
};

struct IpmcSnoopingMldGrpadrsTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamKey<mesa_ipv6_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_snp_grp6_address_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for displaying the registered IPv6 multicast group address status from MLD snooping.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_snp_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(mesa_ipv6_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, ipmc_snp_ipv6_grp_addr_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_ipmc_snp_grp6_address_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snp_mld_group_address_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_snp_mld_group_address_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_IPMC);
};

struct IpmcSnoopingMldSrclistTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamKey<mesa_ipv6_t *>,
        vtss::expose::ParamKey<vtss_ifindex_t *>,
        vtss::expose::ParamKey<mesa_ipv6_t *>,
        vtss::expose::ParamVal<vtss_appl_ipmc_snp_grp6_srclist_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table for displaying the address SFM (a.k.a Source List Multicast) status in source list of the registered IPv6 multicast group from MLD snooping.";

    static constexpr const char *index_description =
        "Each entry has a set of parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_snp_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(mesa_ipv6_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, ipmc_snp_ipv6_grp_addr_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3));
        serialize(h, ipmc_snp_port_ifindex_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_4(mesa_ipv6_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(4));
        serialize(h, ipmc_snp_ipv6_src_addr_idx(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_5(vtss_appl_ipmc_snp_grp6_srclist_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(5));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snp_mld_group_srclist_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ipmc_snp_mld_group_srclist_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_IPMC);
};
#endif /* VTSS_SW_OPTION_SMB_IPMC */

struct IpmcSnoopingControlIgmpStatistics {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_ifindex_t *>
    > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_snp_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snooping_control_statistics_dummy_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ipmc_snp_igmp_statistics_clear_by_vlan_act);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_IPMC);
};

#ifdef VTSS_SW_OPTION_SMB_IPMC
struct IpmcSnoopingControlMldStatistics {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_ifindex_t *>
    > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, ipmc_snp_vlan_ifindex_idx(i));
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_ipmc_snooping_control_statistics_dummy_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ipmc_snp_mld_statistics_clear_by_vlan_act);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_IPMC);
};
#endif /* VTSS_SW_OPTION_SMB_IPMC */

}  // namespace interfaces
}  // namespace ipmc_snooping
}  // namespace appl
}  // namespace vtss
#endif /* __VTSS_IPMC_SNOOPING_SERIALIZER_HXX__ */
