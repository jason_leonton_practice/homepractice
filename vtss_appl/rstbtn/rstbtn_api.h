/***********************************************************
 *
 * Copyright (c) 2018 LEONTON Technologies, Co.Ltd All Rights Reserved.
 *
 * Reset Button Module
 *
 ***********************************************************/

#ifndef __RSTBTN_API_H__
#define __RSTBTN_API_H__

/* Initialize module */
vtss_rc rstbtn_init(vtss_init_data_t *data);
#endif /* __RSTBTN_API_H__ */
