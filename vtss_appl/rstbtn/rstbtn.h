/***********************************************************
 *
 * Copyright (c) 2018 LEONTON Technologies, Co.Ltd All Rights Reserved.
 *
 * Reset Button Module
 *
 ***********************************************************/

#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>

#define VTSS_TRACE_MODULE_ID	VTSS_MODULE_ID_RSTBTN
#define VTSS_TRACE_GRP_DEFAULT	0
#define VTSS_TRACE_GRP_CRIT		1
#define VTSS_TRACE_GRP_CNT		2

#define RSTBTN_PRESS					(0)
#define RSTBTN_NOT_PRESS				(1)
#define RSTBTN_SLEEP_MSEC				(100) //msleep

#define RSTBTN_STAGE_SEC				(4)
#define RSTBTN_RUNNING_CONF_SAVE_RETRY	(5)

typedef enum {
	RSTBTN_NOT_ACTION = 0,
	RSTBTN_ACTION_REBOOT,
	RSTBTN_ACTION_RELOAD_DEF,
	RSTBTN_ACTION_MAX
} retbtn_action_t;
