/***********************************************************
 *
 * Copyright (c) 2018 LEONTON Technologies, Co.Ltd All Rights Reserved.
 *
 * Reset Button Module
 *
 ***********************************************************/

#include "main.h"
#include "conf_api.h"
#include "msg_api.h"
#include "critd_api.h"
#include "rstbtn.h"
#include "icli_api.h"
#include "control_api.h"
#include "led_api.h"
#include "icfg_api.h"

static critd_t crit;  // Critical region for global variables

/****************************************************************************
 *  TRACE system
 ****************************************************************************/
#if (VTSS_TRACE_ENABLED)
static vtss_trace_reg_t trace_reg = {
	VTSS_TRACE_MODULE_ID, "rstbtn", "Reset button Module"
};

static vtss_trace_grp_t trace_grps[VTSS_TRACE_GRP_CNT] = {
	/* VTSS_TRACE_GRP_DEFAULT */ {
		"default",
		"Default",
		VTSS_TRACE_LVL_ERROR,
		VTSS_TRACE_FLAGS_TIMESTAMP
	},
	/* VTSS_TRACE_GRP_CRIT */ {
		"crit",
		"Critical regions",
		VTSS_TRACE_LVL_ERROR,
		VTSS_TRACE_FLAGS_TIMESTAMP
	},
};
#define RSTBTN_CRIT_ENTER() critd_enter(&crit, VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define RSTBTN_CRIT_EXIT()  critd_exit( &crit, VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#else
#define RSTBTN_CRIT_ENTER() critd_enter(&crit)
#define RSTBTN_CRIT_EXIT()  critd_exit( &crit)
#endif /* VTSS_TRACE_ENABLED */

static vtss_handle_t rstbtn_thread_handle;
static vtss_thread_t rstbtn_thread_block;

static void rstbtn_action_reboot(void)
{
	printf("%% Cold reload in progress, please stand by.\n");
	if (control_system_reset(TRUE, VTSS_USID_ALL, MESA_RESTART_COOL) != VTSS_OK) {
		printf("%% Cold reload failed! System is being updated by another process.\n");
	}
}

static void rstbtn_action_reload_default(void)
{
	int retry;
	const char *msg;

	printf("%% Reloading defaults. Please stand by.\n");
	control_config_reset(VTSS_USID_ALL, 0);

	for (retry=RSTBTN_RUNNING_CONF_SAVE_RETRY; retry>0; retry--) {
		VTSS_OS_MSLEEP(100); // wait default-config done

		if ((msg = vtss_icfg_running_config_save())) {
			if (retry == 1) {
				printf("%s\n", msg);
			} else {
				T_D("Retry[%d]", retry);
			}
		} else {
			T_D("Reload default success");
			break;
		}
	}

	if (control_system_reset(TRUE, VTSS_USID_ALL, MESA_RESTART_COOL) != VTSS_OK) {
		printf("%% Cold reload failed! System is being updated by another process.\n");
	}
}

// Detection of the stage of reset button
static void rstbtn_detect_stage(int press_cnt, retbtn_action_t *action)
{
	uint pressed_sec = 0;
	BOOL is_pressed = false;

#define MSEC_PER_SEC (1000)
	// press_cnt increases 1 per 100ms
	pressed_sec = press_cnt / (MSEC_PER_SEC / RSTBTN_SLEEP_MSEC);
	is_pressed = !(press_cnt % (MSEC_PER_SEC / RSTBTN_SLEEP_MSEC));

	if ((pressed_sec == RSTBTN_STAGE_SEC) && is_pressed) { // stage2
		lntn_sysled_system_fast_blink_biclrswitch_color();
		*action = RSTBTN_ACTION_RELOAD_DEF;
	} else if ((pressed_sec) == 0 && is_pressed) { // stage1
		lntn_sysled_system_fast_blink_reserve_color();
		*action = RSTBTN_ACTION_REBOOT;
	}
}

static void rstbtn_handle_stage(retbtn_action_t action)
{
	switch (action) {
		case RSTBTN_ACTION_REBOOT:
			rstbtn_action_reboot();
			break;
		case RSTBTN_ACTION_RELOAD_DEF:
			rstbtn_action_reload_default();
			break;
		default:
			break;
	}
}

void rstbtn_thread(vtss_addrword_t data)
{
	int press_cnt = -1;
	BOOL val;
	BOOL end = false;
	retbtn_action_t action = RSTBTN_NOT_ACTION;

	// Set gpio input
	mesa_gpio_direction_set(NULL, 0, MEBA_GPIO_I_PUSH_BUTTON, false);

	// Wait for startup-config load to complete
	msg_wait(MSG_WAIT_UNTIL_MASTER_UP_POST, VTSS_MODULE_ID_RSTBTN);

	while(1) {
		// Get reset button data
		mesa_gpio_read(NULL, 0, MEBA_GPIO_I_PUSH_BUTTON, &val);

		if (val == RSTBTN_PRESS) {
			press_cnt++;
			end = false;
		} else { // val == RSTBTN_NOT_PRESS
			press_cnt = -1;
			end = true;
		}

		if (press_cnt == -1 && action == RSTBTN_NOT_ACTION) {
			VTSS_OS_MSLEEP(RSTBTN_SLEEP_MSEC);
			continue;
		}

		rstbtn_detect_stage(press_cnt, &action);

		// Release button
		if (end) {
			lntn_sysled_system_stop_blink_biclrswitch_color();
			rstbtn_handle_stage(action);
			end = false;
			action = RSTBTN_NOT_ACTION;
		}
		VTSS_OS_MSLEEP(RSTBTN_SLEEP_MSEC);
	}
}

void rstbtn_mp_set_mpflag(int mpflag)
{
	if (conf_mgmt_board_mpflag_set(mpflag) != MESA_RC_OK) {
		printf("Set conf failed\n");
		return;
	} else {
		if (conf_mgmt_board_save() != MESA_RC_OK) {
			printf("Board set operation failed\n");
		}
	}

	printf("mpflag set T%d\n", mpflag);
}

void rstbtn_mp_thread(vtss_addrword_t data)
{
	BOOL val;

	// Set gpio input
	mesa_gpio_direction_set(NULL, 0, MEBA_GPIO_I_PUSH_BUTTON, false);

	while(1) {
		// Get reset button data
		mesa_gpio_read(NULL, 0, MEBA_GPIO_I_PUSH_BUTTON, &val);

		if (val == RSTBTN_PRESS) {
			rstbtn_mp_set_mpflag(2); // set mpflag 2
			rstbtn_action_reboot();
		}
		VTSS_OS_MSLEEP(RSTBTN_SLEEP_MSEC);
	}
}

/* Initialize module */
vtss_rc rstbtn_init(vtss_init_data_t *data)
{
	vtss_isid_t isid = data->isid;
	vtss_rc rc = VTSS_OK;
	int mpflag;

	if (data->cmd == INIT_CMD_EARLY_INIT) {
        /* Initialize and register trace ressources */
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, VTSS_TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);
        return rc;
    }

	(void) conf_mgmt_mpflag_get(&mpflag);
	T_D("MPFLAG[%d]", mpflag);

	// mpflag != 7, reset button test
	if (mpflag != 7 && mpflag != 21) {
		return rc;
	}

	switch (data->cmd) {
		case INIT_CMD_INIT:
			critd_init(&crit, "rstbtn crit", VTSS_MODULE_ID_RSTBTN, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
			RSTBTN_CRIT_EXIT();

			if (mpflag == 21) {
				vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
								rstbtn_mp_thread,
								0,
								"mp_rstbtn",
								nullptr,
								0,
								&rstbtn_thread_handle,
								&rstbtn_thread_block);
			} else {
				vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
								rstbtn_thread,
								0,
								"rst_button",
								nullptr,
								0,
								&rstbtn_thread_handle,
								&rstbtn_thread_block);
			}
			T_N("INIT");
			break;
		case INIT_CMD_START:
			T_N("START");
			break;
		case INIT_CMD_CONF_DEF:
			T_N("CONF_DEF, isid: %d", isid);
			break;
		case INIT_CMD_MASTER_UP:
			T_N("MASTER_UP");
			break;
		case INIT_CMD_MASTER_DOWN:
			T_N("MASTER_DOWN");
			break;
		case INIT_CMD_SWITCH_ADD:
			T_N("SWITCH_ADD, isid: %d", isid);
			break;
		case INIT_CMD_SWITCH_DEL:
			T_N("SWITCH_DEL, isid: %d", isid);
			break;
		default:
			break;
	}

	T_N("RSTBTN EXIT");
	return rc;
}
