

/*******************************************************************************
 *
 *  $Id: zl303xx_Example34x.h 14115 2016-09-02 21:09:28Z RF $
 *
 *  Copyright 2006-2016 Microsemi Semiconductor Limited.
 *  All rights reserved.
 *
 *  Module Description:
 *     Supporting interfaces for the 34x examples
 *
 ******************************************************************************/

#ifndef _ZL303XX_EXAMPLE_34X_H_
#define _ZL303XX_EXAMPLE_34X_H_

#ifdef __cplusplus
extern "C" {
#endif

/*****************   INCLUDE FILES   ******************************************/
#include "zl303xx_Global.h"
#include "zl303xx_Error.h"
#include "zl303xx.h"
#include "zl303xx_ApiConfig.h"

/*****************   DEFINES   ************************************************/

/*****************   DATA TYPES   *********************************************/

/*****************   DATA STRUCTURES   ****************************************/

typedef struct
{
    Uint8T pllId;
   zl303xx_DeviceModeE deviceMode;
   zl303xx_ParamsS *zl303xx_Params;
} example34xClockCreateS;


/*****************   EXPORTED GLOBAL VARIABLE DECLARATIONS   ******************/

/*****************   EXTERNAL FUNCTION DECLARATIONS   *************************/
zlStatusE example34xEnvInit(void);
zlStatusE example34xEnvClose(void);

zlStatusE example34xClockCreateStructInit(example34xClockCreateS *pClock);
zlStatusE example34xClockCreate(example34xClockCreateS *pClock);
zlStatusE example34xClockRemove(example34xClockCreateS *pClock);

zlStatusE example34xClockTimeGet(zl303xx_ParamsS *zl303xx_Params, Uint16T *epoch,
                                   Uint64S *tod);
zlStatusE example34xClockTimeSet(zl303xx_ParamsS *zl303xx_Params, Uint16T epoch,
                                   Uint32T seconds);

#if defined ZLS30341_INCLUDED 
zlStatusE zl303xx_Dpll34xStopIntrTimer(void);
#endif

#ifdef __cplusplus
}
#endif

#endif /* MULTIPLE INCLUDE BARRIER */
