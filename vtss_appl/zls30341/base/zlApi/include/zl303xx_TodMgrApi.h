

/*******************************************************************************
*
*  $Id: zl303xx_TodMgrApi.h 13892 2016-07-25 20:26:33Z RF $
*
*  Copyright 2006-2016 Microsemi Semiconductor Limited.
*  All rights reserved.
*
*  Module Description:
*     This file contains function prototypes for the Time-of-Day management
*     task. It should be included by any module that needs to use the
*     Time-of-Day management routines.
*
*******************************************************************************/

#ifndef ZL303XX_TOD_MGR_API_H_
#define ZL303XX_TOD_MGR_API_H_

#ifdef __cplusplus
extern "C" {
#endif

/*****************   INCLUDE FILES   ******************************************/

#include "zl303xx_Global.h"
#include "zl303xx_TodMgrTypes.h"
#include "zl303xx_TodMgrInternal.h"
#include "zl303xx.h"

/*****************   DEFINES   ************************************************/

/*****************   DATA TYPES   *********************************************/

/*****************   DATA STRUCTURES   ****************************************/

/*****************   EXPORTED GLOBAL VARIABLE DECLARATIONS   ******************/

/*****************   EXTERNAL FUNCTION DECLARATIONS   *************************/

/******* Routines for Time-of-Day Task Management *********/
/* Routine to initialize the Time-of-Day Manager Task */
zlStatusE zl303xx_TodMgrInit(zl303xx_TodMgrInitParamsS *initParams);
zlStatusE zl303xx_TodMgrClose(void);

/* Determine if the Task is initialized. */
zlStatusE zl303xx_TodMgrStatus(void);

/******* Routines to Set, Adjust and Re-align the  ****/
/* Reset the Current Time-of-Day. */
zlStatusE zl303xx_TodResetTime(
      zl303xx_ParamsS *zl303xx_Params,
      Uint16T resetEpoch,
      Uint64S resetTime,
      zl303xx_TodMgrWaitE wait);

/* Sync or Reset the TOD to the External (input) SYNC pulse. */
zlStatusE zl303xx_TodSyncToExtPulse(
      zl303xx_ParamsS *zl303xx_Params,
      zl303xx_TodMgrWaitE wait);
zlStatusE zl303xx_TodResetOnExtPulse(
      zl303xx_ParamsS *zl303xx_Params,
      Uint16T resetEpoch,
      Uint64S resetTime,
      zl303xx_TodMgrWaitE wait);

/* Routines to Set or Step the Time-of-Day on a device. */
zlStatusE zl303xx_SetTime(
      zl303xx_ParamsS *zl303xx_Params,
      Uint32T todSeconds);
zlStatusE zl303xx_StepTime(
      zl303xx_ParamsS *zl303xx_Params,
      Sint32T nanoStep);

/******* Routines to control IF and HOW the Time-of-Day Aligns to the DCO Frame Pulses ****/
zlStatusE zl303xx_TodPulseAlignmentGet(
      zl303xx_ParamsS *zl303xx_Params,
      zl303xx_TodAlignmentE *align);
zlStatusE zl303xx_TodPulseAlignmentSet(
      zl303xx_ParamsS *zl303xx_Params,
      zl303xx_TodAlignmentE align);

/******* Routines to configure the 1Hz Auto-realign functionality ****/
zlStatusE zl303xx_TodAutoRealign1HzEnable(
      zl303xx_ParamsS *zl303xx_Params,
      zl303xx_BooleanE enable);
zlStatusE zl303xx_TodAutoRealign1HzPeriod(
      zl303xx_ParamsS *zl303xx_Params,
      Uint32T periodSeconds);

/******* Routine to help Debug the ToD configuration and progress of a device. ****/
zlStatusE zl303xx_TodMgrConfigShow(
      zl303xx_ParamsS *zl303xx_Params);

/******* Routines to set a customer insert time stamp offset ****/
zlStatusE zl303xx_TodSetInsertTsOffset(
      zl303xx_ParamsS *zl303xx_Params,
      Sint32T offsetNs);
zlStatusE zl303xx_TodEnableInsertTsOffset(
      zl303xx_ParamsS *zl303xx_Params,
      zl303xx_BooleanE enable);

/* Hardware porting functions for APR's setTime() and stepTime(). */
Sint32T zl303xx_TodAprSetTime(void *hwParams, Uint64S deltaTimeSec, Uint32T deltaTimeNanoSec,
                              zl303xx_BooleanE negative);
Sint32T zl303xx_TodAprStepTime(void *hwParams, Sint32T deltaTimeNs);

zlStatusE zl303xx_TodMgrRegisterTODDoneFunc(zl303xx_ParamsS *zl303xx_Params, hwFuncPtrTODDone TODdoneFnPtr);

#ifdef __cplusplus
}
#endif

#endif /* MULTIPLE INCLUDE BARRIER */
