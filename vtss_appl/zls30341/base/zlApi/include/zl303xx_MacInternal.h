

/*******************************************************************************
*
*  $Id: zl303xx_MacInternal.h 14014 2016-08-18 18:24:48Z RF $
*  Copyright 2006-2016 Microsemi Semiconductor Limited.
*  All rights reserved.
*
*  Module Description:
*     Internal device constants for the MAC
*
*******************************************************************************/

#ifndef ZL303XX_MAC_INTERNAL_H_
#define ZL303XX_MAC_INTERNAL_H_

#ifdef __cplusplus
extern "C" {
#endif

/*****************   INCLUDE FILES   ******************************************/
#include "zl303xx_Global.h"
#include "zl303xx_AddressMap.h"
#include "zl303xx.h"

/*****************   DEFINES   ************************************************/
/* The addresses of the individual registers in the Ethernet bridge */
#define  ZL303XX_MAC_CONFIG_IDX_REG   ZL303XX_MAKE_MEM_ADDR(0x0B, 0x65, ZL303XX_MEM_SIZE_2_BYTE, ZL303XX_MEM_OVRLY_NONE)
#define  ZL303XX_MAC_CONFIG_DATA_REG  ZL303XX_MAKE_MEM_ADDR(0x0B, 0x67, ZL303XX_MEM_SIZE_4_BYTE, ZL303XX_MEM_OVRLY_NONE)
#define  ZL303XX_MAC_MEM_RD_DATA_REG  ZL303XX_MAKE_MEM_ADDR(0x0B, 0x6B, ZL303XX_MEM_SIZE_4_BYTE, ZL303XX_MEM_OVRLY_NONE)
#define  ZL303XX_MAC_MEM_WR_DATA_REG  ZL303XX_MAKE_MEM_ADDR(0x0B, 0x6F, ZL303XX_MEM_SIZE_4_BYTE, ZL303XX_MEM_OVRLY_NONE)
#define  ZL303XX_MAC_MEM_CMD_REG      ZL303XX_MAKE_MEM_ADDR(0x0B, 0x73, ZL303XX_MEM_SIZE_4_BYTE, ZL303XX_MEM_OVRLY_NONE)
#define  ZL303XX_MAC_CMD_STATUS_REG   ZL303XX_MAKE_MEM_ADDR(0x0B, 0x77, ZL303XX_MEM_SIZE_2_BYTE, ZL303XX_MEM_OVRLY_NONE)

#define  ZL303XX_MAC_TSR_REG          ZL303XX_MAKE_MEM_ADDR(0x00, 0x0D, ZL303XX_MEM_SIZE_4_BYTE, ZL303XX_MEM_OVRLY_BRIDGE_CONFIG)
#define  ZL303XX_MAC_RSR_REG          ZL303XX_MAKE_MEM_ADDR(0x00, 0x0E, ZL303XX_MEM_SIZE_4_BYTE, ZL303XX_MEM_OVRLY_BRIDGE_CONFIG)
#define  ZL303XX_MAC_DPST_REG         ZL303XX_MAKE_MEM_ADDR(0x00, 0x11, ZL303XX_MEM_SIZE_4_BYTE, ZL303XX_MEM_OVRLY_BRIDGE_CONFIG)

/* Bit masks for memory command register (ZL303XX_MAC_MEM_CMD_REG) */
#define  ZL303XX_MAC_CMD_WR           (Uint32T)0x00000000
#define  ZL303XX_MAC_CMD_RD           (Uint32T)0x80000000

/* Bit masks for config command register (ZL303XX_MAC_CMD_STATUS_REG) */
#define  ZL303XX_MAC_CMD_MEM_STAT     (Uint32T)0x01
#define  ZL303XX_MAC_CMD_CFG_STAT     (Uint32T)0x02

/* Bit masks for Tx Timestamp Location Register (ZL303XX_MAC_TSR_REG) */
#define  ZL303XX_MAC_TSR_TX_TSLOC_MASK      (Uint32T)0x0000007F
#define  ZL303XX_MAC_TSR_TX_TSLOC_SHIFT     (Uint32T)0
#define  ZL303XX_MAC_TSR_TX_SIZE_BIT        (Uint32T)0x00000080

#define  ZL303XX_MAC_TSR_TX_CSUM_DISABLE_BIT (Uint32T)0x00008000
#define  ZL303XX_MAC_TSR_TX_CSUMLOC_MASK    (Uint32T)0x0000007F
#define  ZL303XX_MAC_TSR_TX_CSUMLOC_SHIFT   (Uint32T)8

#define  ZL303XX_MAC_TSR_TX_INFOLOC_MASK    (Uint32T)0x000000FF
#define  ZL303XX_MAC_TSR_TX_INFOLOC_SHIFT   (Uint32T)16

/* Bit masks for Rx Timestamp Location Register (ZL303XX_MAC_RSR_REG) */
#define  ZL303XX_MAC_RSR_RX_TSLOC_MASK      (Uint32T)0x00003FFF
#define  ZL303XX_MAC_RSR_RX_TSLOC_SHIFT     (Uint32T)0
#define  ZL303XX_MAC_RSR_RX_HI_PRTY_BIT     (Uint32T)0x00008000
#define  ZL303XX_MAC_RSR_RX_TS_ENABLE_BIT   (Uint32T)0x00004000

#define  ZL303XX_MAC_RSR_RX_SCHLOC_MASK     (Uint32T)0x0000007F
#define  ZL303XX_MAC_RSR_RX_SCHLOC_SHIFT    (Uint32T)16

#define  ZL303XX_MAC_RSR_SCHED_ENABLE_BIT   (Uint32T)0x00800000
#define  ZL303XX_MAC_RSR_SCHED_Q_FLSH_BIT   (Uint32T)0x01000000
#define  ZL303XX_MAC_RSR_SCHED_PKT_DRP_BIT  (Uint32T)0x02000000

/* Bit masks for Device Port Status Register (ZL303XX_MAC_DPST_REG) */
#define  ZL303XX_MAC_DPST_PORT_SHIFT(n)      ((Uint32T)(n)*8)  /* The shift factor to use
                                                               to locate the port status for port n */
   /* Per port bit masks */
#define  ZL303XX_MAC_DPST_FLCTRL_BIT        (Uint32T)0x00000001
#define  ZL303XX_MAC_DPST_FDPLX_BIT         (Uint32T)0x00000002
#define  ZL303XX_MAC_DPST_100M_BIT          (Uint32T)0x00000004  /* Only applies if ZL303XX_MAC_DPST_1G_BIT is 0 */
#define  ZL303XX_MAC_DPST_LINK_STAT_BIT     (Uint32T)0x00000008
#define  ZL303XX_MAC_DPST_1G_BIT            (Uint32T)0x00000020
#define  ZL303XX_MAC_DPST_TBI_BIT           (Uint32T)0x00000040
   /* non-per port bit masks */
#define  ZL303XX_MAC_DPST_SCHED_FLSH_DONE_BIT (Uint32T)0x80000000

/* The base register address for the Match-Mask rules */
/* Rule 1 = 0x180-0x19F   */
/* Rule 2 = 0x1A0-0x1BF   */
#define  ZL303XX_MAC_PKT_RULE_BASE_ADDR  0x180

/* Size of packet match definition table for each port */
#define  ZL303XX_MAC_PKT_RULE_MEM_SIZE    0x20
/* Base address of packet match definition table for each rule (=0,1) */
#define  ZL303XX_MAC_PKT_RULE_MEM_BASE(rule)    ZL303XX_MAKE_MEM_ADDR(0x00, (Uint32T)(ZL303XX_MAC_PKT_RULE_BASE_ADDR + rule*ZL303XX_MAC_PKT_RULE_MEM_SIZE), ZL303XX_MEM_SIZE_4_BYTE, ZL303XX_MEM_OVRLY_BRIDGE_MEMORY)

/*****************   DATA TYPES   *********************************************/

/*****************   DATA STRUCTURES   ****************************************/

/*****************   EXPORTED GLOBAL VARIABLE DECLARATIONS   ******************/

/*****************   INTERNAL FUNCTION DECLARATIONS   *************************/
/* Low-level SPI Read & Write functions for indirect register access */
zlStatusE zl303xx_AccessMacConfigReg(zl303xx_ParamsS *zl303xx_Params,
                                     zl303xx_ChipSelectCtrlS *chipSelect,
                                     Uint32T regAddr, Uint32T *data,
                                     Uint32T rw);

zlStatusE zl303xx_AccessMacMemoryReg(zl303xx_ParamsS *zl303xx_Params,
                                     zl303xx_ChipSelectCtrlS *chipSelect,
                                     Uint32T regAddr, Uint32T *data,
                                     Uint32T rw);

#ifdef __cplusplus
}
#endif

#endif /* MULTIPLE INCLUDE BARRIER */
