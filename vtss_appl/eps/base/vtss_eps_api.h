/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 

*/

#ifndef _VTSS_EPS_API_H_
#define _VTSS_EPS_API_H_

#include "main_types.h"
#include "vtss/appl/eps.h"

#define VTSS_EPS_RC_OK                      0   /* Management operation is ok */
#define VTSS_EPS_RC_NOT_CREATED             1   /* Operating on an instance not created */
#define VTSS_EPS_RC_CREATED                 2   /* Creating an instance already created */
#define VTSS_EPS_RC_INVALID_PARAMETER       3   /* invalid parameter */
#define VTSS_EPS_RC_NOT_CONFIGURED          4   /* Instance is not configured */
#define VTSS_EPS_RC_APS                     5   /* Invalid APS request */
#define VTSS_EPS_RC_ARCHITECTURE            6   /* 1 plus 1 architecture is only for port domain */
#define VTSS_EPS_RC_W_P_EQUAL               7   /* Working and protecting is equal */
#define VTSS_EPS_RC_WORKING_USED            8   /* Working is used by other instance */
#define VTSS_EPS_RC_PROTECTING_USED         9   /* Protecting is used by other instance */
#define VTSS_EPS_RC_INVALID_COMMAND         10  /* Invalid command for this configuration */
#define VTSS_EPS_RC_INVALID_WTR             11  /* Invalid WTR time */
#define VTSS_EPS_RC_INVALID_HOLD_OFF        12  /* Invalid HOLD OFF time */

#define VTSS_EPS_APS_DATA_LENGTH     4

typedef struct
{
    vtss_appl_eps_domain_t          domain;             /**< Domain.                                     */
    vtss_appl_eps_architecture_t    architecture;       /**< Architecture.                               */
    u32                             w_flow;             /**< Working flow instance number.               */
    u32                             p_flow;             /**< Working flow instance number.               */
} eps_create_param_t;

typedef struct
{
    eps_create_param_t             param;   /**< Instance create default parameters */
    vtss_appl_eps_conf_t           config;  /**< Instance configuration default     */
    vtss_appl_eps_command_t        command; /**< Command default                    */
} eps_default_conf_t;

mesa_rc eps_create_conf_add(const u32                 instance,
                            const eps_create_param_t *const conf);
mesa_rc eps_create_conf_get(u32                       instance,
                            eps_create_param_t       *const conf);
mesa_rc eps_create_conf_set(u32                       instance,
                            const eps_create_param_t *const conf);

void eps_default_conf_get(eps_default_conf_t  *const conf);

/****************************************************************************/
/*  EPS management interface                                                */
/****************************************************************************/

/* instance:        Instance number of EPS              */
/* param:           Parameters for this create          */
/* An EPS instance is created                           */
u32 vtss_eps_mgmt_instance_create(const u32                    instance,
                                  const eps_create_param_t    *const param);

/* instance:        Instance number of EPS              */
/* param:           Parameters for this create          */
/* An EPS instance is set                           */
u32 vtss_eps_mgmt_instance_set(const u32                    instance,
                               const eps_create_param_t    *const param);


/* instance:        Instance number of EPS. */
/* An EPS instance is now deleted.          */
u32 vtss_eps_mgmt_instance_delete(const u32     instance);

/* instance:    Instance number of EPS.                 */
/* conf:        Configuration data for this instance    */
u32 vtss_eps_mgmt_conf_set(const u32                    instance,
                           const vtss_appl_eps_conf_t   *const conf);

/* instance:        Instance number of EPS.                 */
/* param:           Create parameters for this instance     */
/* conf:            Configuration data for this instance    */
u32 vtss_eps_mgmt_conf_get(const u32                         instance,
                           eps_create_param_t                *const param,
                           vtss_appl_eps_conf_t              *const conf);

/* instance: Instance number of EPS.                                     */
/* command:  One of the possible management commands to the APS protocol */
u32 vtss_eps_mgmt_command_set(const u32                         instance,
                              const vtss_appl_eps_command_t     command);

/* instance:    Instance number of EPS.                                     */
/* command:     One of the possible management commands to the APS protocol */
u32 vtss_eps_mgmt_command_get(const u32                   instance,
                              vtss_appl_eps_command_t     *const command);

/* instance:    Instance number of EPS. */
/* Get the state of this EPS            */
u32 vtss_eps_mgmt_state_get(const u32                 instance,
                            vtss_appl_eps_state_t     *const state);


void  vtss_eps_mgmt_default_conf_get(eps_default_conf_t  *const def_conf);



/****************************************************************************/
/*  EPS module call in interface                                            */
/****************************************************************************/

typedef enum
{
    VTSS_EPS_FLOW_WORKING,
    VTSS_EPS_FLOW_PROTECTING
} vtss_eps_flow_type_t;

/* instance:    Instance number of EPS.                 */
/* flow:        Flow type                               */
/* aps_info:    Array[4] with the received APS info.    */
/* External is calling this to deliver latest received APS on a flow */
u32 vtss_eps_rx_aps_info_set(const u32                     instance,
                             const vtss_eps_flow_type_t    flow,
                             const u8                      aps[VTSS_EPS_APS_DATA_LENGTH]);


/* instance:    Instance number of EPS.       */
/* This is called by external to activate EPS calling out transmitting APS */
u32 vtss_eps_signal_in(const u32   instance);


/* instance:    Instance number of EPS.                                    */
/* flow:        Flow type                                                  */
/* sf_state:    Lates state of SF for this instance.                       */
/* sd_state:    Lates state of SD for this instance.                       */
/* External is calling this to deliver latest SF/SD state on a flow.       */
u32 vtss_eps_sf_sd_state_set(const u32                     instance,
                             const vtss_eps_flow_type_t    flow,
                             const BOOL                    sf_state,
                             const BOOL                    sd_state);





/****************************************************************************/
/*  EPS module call out interface                                           */
/****************************************************************************/

/* instance:    Instance number of EPS.                    */
/* domain:      Domain.                                    */
/* aps_info:    Array[4] with the transmitted APS info.    */
/* This is called by EPS to transmit APS info in a flow    */
void vtss_eps_tx_aps_info_set(const u32                       instance,
                              const vtss_appl_eps_domain_t    domain,
                              const u8                        aps[VTSS_EPS_APS_DATA_LENGTH]);



/* instance:    Instance number of EPS.                    */
/* This is called by EPS on change in port protection traffic selector     */
void vtss_eps_port_protection_set(const u32         w_port,
                                  const u32         p_port,
                                  const BOOL        active);



/* instance:    Instance number of EPS.                    */
/* domain:      Domain.                                    */
/* This is called by EPS to activate call in on all in functions     */
void vtss_eps_signal_out(const u32                       instance,
                         const vtss_appl_eps_domain_t    domain);





/****************************************************************************/
/*  EPS platform call in interface                                          */
/****************************************************************************/

/* stop:    Return value indicating if calling this thread has to stop.                                                   */
/* This is the thread driving timing in the EPS. Has to be called every 'timer_resolution' ms. by platform until 'stop'  */
/* Initially this thread is not called until EPS callout on vtss_eps_timer_start()                                       */
void vtss_eps_timer_thread(BOOL  *const stop);


/* This is the thread driving the state machine. Has to be call when EPS is calling out on vtss_eps_run() */ 
void vtss_eps_run_thread(void);


/* timer_resolution:    This is the interval of calling vtss_eps_run_thread() in ms.    */
/* This is the initializing of EPS. Has to be called by platform                        */
u32 vtss_eps_init(const u32  timer_resolution);






/****************************************************************************/
/*  EPS platform call out interface                                         */
/****************************************************************************/

/* This is called by EPS when vtss_eps_run_thread(void) has to be called */
void vtss_eps_run(void);

/* This is called by EPS when vtss_eps_timer_thread(BOOL  *const stop) has to be called until 'stop' is indicated */
void vtss_eps_timer_start(void);

/* This is called by EPS in order to do debug tracing */
void vtss_eps_trace(const char   *const string,
                    const u32    param1,
                    const u32    param2,
                    const u32    param3,
                    const u32    param4);


#endif /* _VTSS_EPS_API_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
