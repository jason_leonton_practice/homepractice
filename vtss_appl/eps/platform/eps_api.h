/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 

*/

#ifndef _EPS_API_H_
#define _EPS_API_H_

#include "main.h"
#include "main_types.h"
#include "vtss_eps_api.h"
#include "mep_api.h"

#ifdef __cplusplus
extern "C" {
#endif

const char *eps_error_txt(mesa_rc rc);


/****************************************************************************/
/*  EPS module interface                                                    */
/****************************************************************************/

/* instance:    Instance number of EPS.                 */
/* mep_inst:    Instance number of MEP                  */
/* aps_info:    Array[4] with the received APS info.    */
/* MEP is calling this to deliver latest received APS on a flow */
mesa_rc eps_rx_aps_info_set(const u32    instance,
                            const u32    mep_inst,
                            const u8     aps[VTSS_EPS_APS_DATA_LENGTH]);


/* instance:    Instance number of EPS.                               */
/* mep_inst:    Instance number of MEP                                */
/* This is called by MEP to activate EPS calling out transmitting APS */
mesa_rc eps_signal_in(const u32   instance,
                      const u32   mep_inst);


/* instance:    Instance number of EPS.                             */
/* mep_inst:    Instance number of MEP                              */
/* sf_state:    Lates state of SF for this instance.                */
/* sd_state:    Lates state of SD for this instance.                */
/* MEP is calling this to deliver latest SF/SD state on a flow.     */
mesa_rc eps_sf_sd_state_set(const u32   instance,
                            const u32   mep_inst,
                            const BOOL  sf_state,
                            const BOOL  sd_state);

/* Initialize module */
mesa_rc eps_init(vtss_init_data_t *data);

#ifdef __cplusplus
}
#endif

#endif /* _EPS_API_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
