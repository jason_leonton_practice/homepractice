/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 

*/

#include "main.h"
#include "conf_api.h"
#include "critd_api.h"
#include "mscc/ethernet/switch/api.h"
#include "vtss/appl/eps.h"
#include "eps_api.h"
#include "mep_api.h"
#include "misc_api.h"
#include "eps.h"

#ifdef VTSS_SW_OPTION_ICFG
#include "eps_icli_functions.h"
#endif

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_EPS

/****************************************************************************/
/*  Global variables                                                        */
/****************************************************************************/

#if (VTSS_TRACE_ENABLED)
static vtss_trace_reg_t trace_reg =
{ 
    VTSS_TRACE_MODULE_ID, "EPS", "EPS module."
};

static vtss_trace_grp_t trace_grps[TRACE_GRP_CNT] =
{
    /* VTSS_TRACE_GRP_DEFAULT */ { 
        "default",
        "Default",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CRIT */ { 
        "crit",
        "Critical regions ",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_API */ { 
        "api",
        "Switch API printout",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    }
};
#endif /* VTSS_TRACE_ENABLED */


/****************************************************************************/
/*  Various local functions                                                 */
/****************************************************************************/
#define FLAG_TIMER     0x0001

static vtss_handle_t timer_thread_handle;
static vtss_thread_t timer_thread_block;
static vtss_handle_t run_thread_handle;
static vtss_thread_t run_thread_block;

critd_t              eps_crit;    /* Base critd */
static critd_t       crit_p;      /* Platform critd */

static vtss_sem_t    run_wait_sem;
static vtss_flag_t   timer_wait_flag;

typedef struct
{
    BOOL created;
    u32  w_mep;   /* Working MEP instance number.                */
    u32  p_mep;   /* Protecting MEP instance number.             */
    u32  aps_mep; /* APS MEP instance number.                    */
} eps_data_t;

static eps_data_t  eps_data[VTSS_APPL_EPS_CREATED_MAX];

static void restore_to_default(void)
{
    uint i;

    for (i=0; i<VTSS_APPL_EPS_CREATED_MAX; ++i) {
        (void)vtss_appl_eps_create_conf_delete(i);
    }

    VTSS_OS_MSLEEP(100);    /* Wait for async EPS delete */

    CRIT_ENTER(crit_p);
    memset(eps_data, 0, sizeof(eps_data));
    CRIT_EXIT(crit_p);
}


static void eps_timer_thread(vtss_addrword_t data)
{
    BOOL               stop;
    struct timeval     now;
    u32                now_ms, next_ms, t = 10;

    (void)gettimeofday(&now, NULL);
    now_ms = 1000 * now.tv_sec + now.tv_usec / 1000;
    next_ms = now_ms + 10;
    while (1) {
        (void)vtss_flag_wait(&timer_wait_flag, FLAG_TIMER, VTSS_FLAG_WAITMODE_OR_CLR);
        do
        {
            if (t > 0) VTSS_OS_MSLEEP(t);
            (void)gettimeofday(&now, NULL);
            now_ms = 1000 * now.tv_sec + now.tv_usec / 1000;
            if ((now_ms - next_ms) > 1000) {
                next_ms = now_ms + 10;
                t = 10;
            } else {
                t = 10 - (now_ms - next_ms);
                if (t > 10) {
                    t = 0;
                }
                next_ms += 10;
            }

            vtss_eps_timer_thread(&stop);
        } while(!stop);
    }
}

static void eps_run_thread(vtss_addrword_t data)
{
    while (1) {
        vtss_sem_wait(&run_wait_sem);
        vtss_eps_run_thread();
    }
}

static mesa_rc rc_conv(u32 rc)
{
    switch (rc)
    {
        case VTSS_EPS_RC_OK:                     return(VTSS_RC_OK);
        case VTSS_EPS_RC_NOT_CREATED:            return(VTSS_APPL_EPS_RC_NOT_CREATED);
        case VTSS_EPS_RC_CREATED:                return(VTSS_APPL_EPS_RC_CREATED);
        case VTSS_EPS_RC_INVALID_PARAMETER:      return(VTSS_APPL_EPS_RC_INVALID_PARAMETER);
        case VTSS_EPS_RC_NOT_CONFIGURED:         return(VTSS_APPL_EPS_RC_NOT_CONFIGURED);
        case VTSS_EPS_RC_ARCHITECTURE:           return(VTSS_APPL_EPS_RC_ARCHITECTURE);
        case VTSS_EPS_RC_W_P_EQUAL:              return(VTSS_APPL_EPS_RC_W_P_FLOW_EQUAL);
        case VTSS_EPS_RC_WORKING_USED:           return(VTSS_APPL_EPS_RC_WORKING_USED);
        case VTSS_EPS_RC_PROTECTING_USED:        return(VTSS_APPL_EPS_RC_PROTECTING_USED);
        case VTSS_EPS_RC_INVALID_COMMAND:        return(VTSS_APPL_EPS_RC_INVALID_COMMAND);
        case VTSS_EPS_RC_INVALID_WTR:            return(VTSS_APPL_EPS_RC_INVALID_WTR);
        case VTSS_EPS_RC_INVALID_HOLD_OFF:       return(VTSS_APPL_EPS_RC_INVALID_HOLD_OFF);
        // By not specifying a "default:" we get lint or compiler warnings when not all are specified.
    }

    return (VTSS_RC_OK);
}

/****************************************************************************/
/*  EPS management interface                                                */
/****************************************************************************/
static mesa_rc conv_param_ext_int(const vtss_appl_eps_create_param_t *a, eps_create_param_t *b)
{
    vtss_ifindex_elm_t w_flow, p_flow;
    b->domain = a->domain; 
    b->architecture = a->architecture;
    VTSS_RC(vtss_ifindex_decompose(a->p_flow, &p_flow))
    VTSS_RC(vtss_ifindex_decompose(a->w_flow, &w_flow))
    if (p_flow.iftype != w_flow.iftype) return VTSS_RC_ERROR;

/*    switch (a->domain) {
        case VTSS_APPL_EPS_PORT:
		b->domain = 
        case VTSS_APPL_EPS_EVC:
        case VTSS_APPL_EPS_MPLS_TUNNEL: 
        case VTSS_APPL_EPS_MPLS_PW: 
            break;

        default:
            return VTSS_RC_ERROR;
    } */

    b->p_flow = p_flow.ordinal;
    b->w_flow = w_flow.ordinal;
    return VTSS_RC_OK;
}

static mesa_rc conv_param_int_ext(const eps_create_param_t *a, vtss_appl_eps_create_param_t *b)
{
    b->domain = a->domain;
    b->architecture = a->architecture;
    b->p_flow = VTSS_IFINDEX_NONE;    /* Indication that the following ifindex could not be calculated */
    b->w_flow = VTSS_IFINDEX_NONE;    /* Indication that the following ifindex could not be calculated */

    switch (a->domain) {
        case VTSS_APPL_EPS_PORT:
            (void)vtss_ifindex_from_port(VTSS_ISID_LOCAL, a->p_flow, &b->p_flow);
            (void)vtss_ifindex_from_port(VTSS_ISID_LOCAL, a->w_flow, &b->w_flow);
            break;

        case VTSS_APPL_EPS_EVC:
            (void)vtss_ifindex_from_evc(a->p_flow, &b->p_flow);
            (void)vtss_ifindex_from_evc(a->w_flow, &b->w_flow);
            break;

        case VTSS_APPL_EPS_MPLS_TUNNEL:
            (void)vtss_ifindex_from_mpls_tunnel(a->p_flow, &b->p_flow);
            (void)vtss_ifindex_from_mpls_tunnel(a->w_flow, &b->w_flow);
            break;

        case VTSS_APPL_EPS_MPLS_PW:
            (void)vtss_ifindex_from_mpls_pw(a->p_flow, &b->p_flow);
            (void)vtss_ifindex_from_mpls_pw(a->w_flow, &b->w_flow);
            break;

        default:
            return VTSS_RC_ERROR;
    }

    return VTSS_RC_OK;
}

static mesa_rc conv_def_int_ext(const eps_default_conf_t *a, vtss_appl_eps_default_conf_t *b)
{
    (void)conv_param_int_ext(&a->param, &b->param);
    b->config = a->config;
    b->command = a->command;
    return VTSS_RC_OK;
}


static void cancel_reg(const u32                  instance,
                       const vtss_appl_eps_mep_t  *const mep)
{
    (void)mep_eps_aps_register(mep->aps_mep, instance, MEP_EPS_TYPE_ELPS, FALSE);
    (void)mep_eps_sf_register(mep->w_mep, instance, MEP_EPS_TYPE_ELPS, FALSE);
    (void)mep_eps_sf_register(mep->p_mep, instance, MEP_EPS_TYPE_ELPS, FALSE);
}

mesa_rc vtss_appl_eps_capabilities_get(vtss_appl_eps_capabilities_t *const conf)
{
    T_N("conf %p", conf);

    conf->created_max = VTSS_APPL_EPS_CREATED_MAX;
    conf->wtr_max = VTSS_APPL_EPS_WTR_MAX;
    conf->hoff_off = VTSS_APPL_EPS_HOFF_OFF;
    conf->hoff_max = VTSS_APPL_EPS_HOFF_MAX;
    conf->mep_max = VTSS_APPL_EPS_MEP_MAX;
    conf->mep_invalid = VTSS_APPL_EPS_MEP_INST_INVALID;

    return (VTSS_RC_OK);
}


void eps_default_conf_get(eps_default_conf_t  *const def_conf)
{
    vtss_eps_mgmt_default_conf_get(def_conf);
}

void vtss_appl_eps_default_conf_get(vtss_appl_eps_default_conf_t  *const def_conf)
{
    eps_default_conf_t c;
    vtss_eps_mgmt_default_conf_get(&c);
    if (conv_def_int_ext(&c, def_conf) != VTSS_RC_OK)
        T_E("Convertion failed");
}

mesa_rc eps_create_conf_add(const u32                  instance,
                            const eps_create_param_t  *const param)
{
    mesa_rc             rc;

    if (instance >= VTSS_APPL_EPS_CREATED_MAX)        return VTSS_APPL_EPS_RC_INVALID_PARAMETER;
    if (param == NULL)                                return VTSS_RC_OK;

    if ((rc = rc_conv(vtss_eps_mgmt_instance_create(instance, param))) == VTSS_RC_OK)
    {
        if (param->domain == VTSS_APPL_EPS_PORT) {    /* This in order to inform MEP about port conversion in case of port protection */
            mep_port_protection_create((param->architecture == VTSS_APPL_EPS_ARCHITECTURE_1P1) ? MEP_EPS_ARCHITECTURE_1P1 : MEP_EPS_ARCHITECTURE_1F1,  param->w_flow,  param->p_flow);
        }
        CRIT_ENTER(crit_p);
        eps_data[instance].created = TRUE;
        eps_data[instance].w_mep = VTSS_APPL_EPS_MEP_INST_INVALID;
        eps_data[instance].p_mep = VTSS_APPL_EPS_MEP_INST_INVALID;
        eps_data[instance].aps_mep = VTSS_APPL_EPS_MEP_INST_INVALID;
        CRIT_EXIT(crit_p);
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return(rc);
}

mesa_rc vtss_appl_eps_create_conf_add(const u32                           instance,
                                      const vtss_appl_eps_create_param_t  *const param)
{
    eps_create_param_t p;
    VTSS_RC(conv_param_ext_int(param, &p));
    return eps_create_conf_add(instance, &p);
}

mesa_rc vtss_appl_eps_create_conf_delete(const u32     instance)
{
    u32                            rc, w_mep, p_mep, aps_mep;
    eps_create_param_t             param;
    vtss_appl_eps_conf_t           conf;
    mesa_rc                        mep_rc = VTSS_RC_OK;

    if (instance >= VTSS_APPL_EPS_CREATED_MAX)            return(VTSS_APPL_EPS_RC_INVALID_PARAMETER);

    rc = vtss_eps_mgmt_conf_get(instance, &param, &conf);
    if ((rc != VTSS_EPS_RC_OK) && (rc != VTSS_EPS_RC_NOT_CONFIGURED))            return(rc_conv(rc));
    if ((rc = rc_conv(vtss_eps_mgmt_instance_delete(instance))) == VTSS_RC_OK)
    {
        if (param.domain == VTSS_APPL_EPS_PORT) {    /* This in order to in form MEP about port conversion in case of port protection */
            mep_port_protection_delete(param.w_flow, param.p_flow);
        }

        CRIT_ENTER(crit_p);
        w_mep = eps_data[instance].w_mep;
        p_mep = eps_data[instance].p_mep;
        aps_mep = eps_data[instance].aps_mep;
        eps_data[instance].created = FALSE;
        CRIT_EXIT(crit_p);

        if (aps_mep != VTSS_APPL_EPS_MEP_INST_INVALID) mep_rc += mep_eps_aps_register(aps_mep, instance, MEP_EPS_TYPE_ELPS, FALSE);
        if (w_mep != VTSS_APPL_EPS_MEP_INST_INVALID) mep_rc += mep_eps_sf_register(w_mep, instance, MEP_EPS_TYPE_ELPS, FALSE);
        if (p_mep != VTSS_APPL_EPS_MEP_INST_INVALID) mep_rc += mep_eps_sf_register(p_mep, instance, MEP_EPS_TYPE_ELPS, FALSE);
        if (mep_rc != VTSS_RC_OK)   return(VTSS_APPL_EPS_RC_INVALID_PARAMETER);
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return(rc);
}

mesa_rc eps_create_conf_get(u32                  instance,
                            eps_create_param_t  *const param)
{
    u32 rc;
    vtss_appl_eps_conf_t  conf;

    if (instance >= VTSS_APPL_EPS_CREATED_MAX)            return(VTSS_APPL_EPS_RC_INVALID_PARAMETER);

    rc = vtss_eps_mgmt_conf_get(instance, param, &conf);
    if ((rc != VTSS_EPS_RC_OK) && (rc != VTSS_EPS_RC_NOT_CONFIGURED))
        return (rc_conv(rc)); /* Not able to get EPS */

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_eps_create_conf_get(u32                            instance,
                                      vtss_appl_eps_create_param_t  *const param)
{

    mesa_rc  rc; 
    eps_create_param_t p;
    VTSS_RC(eps_create_conf_get(instance, &p));
    rc = conv_param_int_ext(&p, param);
    return rc;
}

mesa_rc eps_create_conf_set(u32                        instance,
                            const eps_create_param_t  *const param)
{
    return(VTSS_RC_ERROR);
}

mesa_rc vtss_appl_eps_create_conf_set(u32                                  instance,
                                      const vtss_appl_eps_create_param_t  *const param)
{
    eps_create_param_t p;
    VTSS_RC(conv_param_ext_int(param, &p));
    return eps_create_conf_set(instance, &p);
}

mesa_rc vtss_appl_eps_create_conf_iter(const u32    *const instance,
                                       u32          *const next)
{
    eps_create_param_t  param;

    if (instance) { /* Calculate the starting point for the search for next enabled EPS */
        *next = *instance + 1;
    } else {
        *next = 0;
    }

    if (*next >= VTSS_APPL_EPS_CREATED_MAX)  /* The end is reached */
        return VTSS_RC_ERROR;

    for (*next=*next; *next<VTSS_APPL_EPS_CREATED_MAX; ++*next) {    /* search for the next enabled EPS instance */
        if ((eps_create_conf_get(*next, &param)) != VTSS_RC_OK)
            continue;
        return (VTSS_RC_OK);
    }

    return (VTSS_RC_ERROR); /* No enabled MEP was found */
}

mesa_rc vtss_appl_eps_mep_conf_set(const u32                  instance,
                                   const vtss_appl_eps_mep_t  *const mep)
{
    u32 i, rc;
    BOOL                           mep_conf_done, mep_conf_ok;
    eps_create_param_t             param;
    vtss_appl_eps_conf_t           conf;
    vtss_appl_eps_mep_t            mep_conf;

    if (instance >= VTSS_APPL_EPS_CREATED_MAX)                                        return(VTSS_APPL_EPS_RC_INVALID_PARAMETER);

    CRIT_ENTER(crit_p);
    mep_conf_done = ((eps_data[instance].w_mep < VTSS_APPL_MEP_INSTANCE_MAX) && (eps_data[instance].p_mep < VTSS_APPL_MEP_INSTANCE_MAX) &&
                     (eps_data[instance].aps_mep < VTSS_APPL_MEP_INSTANCE_MAX)) ? TRUE : FALSE;

    mep_conf_ok = ((mep->w_mep < VTSS_APPL_MEP_INSTANCE_MAX) && (mep->p_mep < VTSS_APPL_MEP_INSTANCE_MAX) &&
                   (mep->aps_mep < VTSS_APPL_MEP_INSTANCE_MAX)) ? TRUE : FALSE;

    if (!mep_conf_done && !mep_conf_ok) { /* Check if all MEP relations are configured or will be with this *mep */
        /* Save the MEP configuration if it is not completed yet - then return OK. This is to satisfy SNMP as it is not able to set all MEP in once */
        eps_data[instance].w_mep = mep->w_mep;
        eps_data[instance].p_mep = mep->p_mep;
        eps_data[instance].aps_mep = mep->aps_mep;
        CRIT_EXIT(crit_p);
        return(VTSS_RC_OK);
    }

    /* All MEP relations are valid - do the normal check */
    if ((mep->w_mep >= VTSS_APPL_MEP_INSTANCE_MAX) || (mep->p_mep >= VTSS_APPL_MEP_INSTANCE_MAX) ||
        (mep->aps_mep >= VTSS_APPL_MEP_INSTANCE_MAX))                                 {CRIT_EXIT(crit_p); return(VTSS_APPL_EPS_RC_INVALID_PARAMETER);}
    if (mep->w_mep == mep->p_mep)                                                     {CRIT_EXIT(crit_p); return(VTSS_APPL_EPS_RC_W_P_SSF_MEP_EQUAL);}
    if (mep->w_mep == mep->aps_mep)                                                   {CRIT_EXIT(crit_p); return(VTSS_APPL_EPS_RC_INVALID_APS_MEP);}
    if (vtss_eps_mgmt_conf_get(instance, &param, &conf) == VTSS_EPS_RC_NOT_CREATED)   {CRIT_EXIT(crit_p); return(VTSS_APPL_EPS_RC_NOT_CREATED);}

    for (i=0; i<VTSS_APPL_EPS_CREATED_MAX; ++i)
    {
        if ((i != instance) && eps_data[i].created) {
            if  (eps_data[i].aps_mep == mep->aps_mep)       {CRIT_EXIT(crit_p); return(VTSS_APPL_EPS_RC_INVALID_APS_MEP);}
        }
    }

    /* Cancel any existing registered MEP */
    mep_conf.w_mep = eps_data[instance].w_mep;
    mep_conf.p_mep = eps_data[instance].p_mep;
    mep_conf.aps_mep = eps_data[instance].aps_mep;
    cancel_reg(instance ,&mep_conf);
    CRIT_EXIT(crit_p);

    /* Register EPS in MEP module */
    if (mep_eps_aps_register(mep->aps_mep, instance, MEP_EPS_TYPE_ELPS, TRUE) != VTSS_RC_OK)   {cancel_reg(instance ,mep); return(VTSS_APPL_EPS_RC_INVALID_APS_MEP);}
    if (mep_eps_sf_register(mep->w_mep, instance, MEP_EPS_TYPE_ELPS, TRUE) != VTSS_RC_OK)      {cancel_reg(instance ,mep); return(VTSS_APPL_EPS_RC_INVALID_W_MEP);}
    if (mep_eps_sf_register(mep->p_mep, instance, MEP_EPS_TYPE_ELPS, TRUE) != VTSS_RC_OK)      {cancel_reg(instance ,mep); return(VTSS_APPL_EPS_RC_INVALID_P_MEP);}

    CRIT_ENTER(crit_p);
    eps_data[instance].w_mep = mep->w_mep;
    eps_data[instance].p_mep = mep->p_mep;
    eps_data[instance].aps_mep = mep->aps_mep;
    CRIT_EXIT(crit_p);

    /* Now signal to MEP to give latest info - received APS and SF */
    rc = mep_signal_in(mep->w_mep, instance);
    rc += mep_signal_in(mep->p_mep, instance);
    if (mep->aps_mep != mep->p_mep)   rc += mep_signal_in(mep->aps_mep, instance);
    if (rc)        T_D("Error during MEP signal %u", instance);

    /* Now signal to EPS to activate transmission of current APS */
    rc = vtss_eps_signal_in(instance);
    if (rc)        T_D("Error during EPS signal %u", instance);

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_eps_mep_conf_get(const u32            instance,
                                   vtss_appl_eps_mep_t  *const mep)
{
    if (instance >= VTSS_APPL_EPS_CREATED_MAX)            return(VTSS_APPL_EPS_RC_INVALID_PARAMETER);

    CRIT_ENTER(crit_p);
    mep->w_mep = eps_data[instance].w_mep;
    mep->p_mep = eps_data[instance].p_mep;
    mep->aps_mep = eps_data[instance].aps_mep;
    CRIT_EXIT(crit_p);

    return(VTSS_RC_OK);
}

mesa_rc vtss_appl_eps_conf_set(const u32                    instance,
                               const vtss_appl_eps_conf_t   *const conf)
{
    u32         rc;

    if (instance >= VTSS_APPL_EPS_CREATED_MAX)       return(VTSS_APPL_EPS_RC_INVALID_PARAMETER);
    if (conf == NULL)                                return VTSS_RC_OK;

    if ((rc = vtss_eps_mgmt_conf_set(instance, conf)) == VTSS_RC_OK)
    {
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return(rc_conv(rc));
}

mesa_rc vtss_appl_eps_conf_get(const u32                instance,
                               vtss_appl_eps_conf_t     *const conf)
{
    eps_create_param_t   param;

    if (instance >= VTSS_APPL_EPS_CREATED_MAX)      return(VTSS_APPL_EPS_RC_INVALID_PARAMETER);
    if (conf == NULL)                               return VTSS_RC_OK;

    return(rc_conv(vtss_eps_mgmt_conf_get(instance, &param, conf)));
}

mesa_rc vtss_appl_eps_command_conf_set(const u32                           instance,
                                       const vtss_appl_eps_command_conf_t  *const conf)
{
    u32         rc;

    if (instance >= VTSS_APPL_EPS_CREATED_MAX)         return(VTSS_APPL_EPS_RC_INVALID_PARAMETER);

    if ((rc = vtss_eps_mgmt_command_set(instance, conf->command)) == VTSS_EPS_RC_OK)
    {
    } else
        T_D("Instance %u  Error returned %s", instance, error_txt(rc));

    return(rc_conv(rc));
}

mesa_rc vtss_appl_eps_command_conf_get(const u32                      instance,
                                       vtss_appl_eps_command_conf_t   *const conf)
{
    if (instance >= VTSS_APPL_EPS_CREATED_MAX)      return(VTSS_APPL_EPS_RC_INVALID_PARAMETER);
    if (conf == NULL)                               return VTSS_RC_OK;

    return(rc_conv(vtss_eps_mgmt_command_get(instance, &conf->command)));
}

mesa_rc vtss_appl_eps_status_get(const u32               instance,
                                 vtss_appl_eps_state_t   *const status)
{
    if (instance >= VTSS_APPL_EPS_CREATED_MAX)        return(VTSS_APPL_EPS_RC_INVALID_PARAMETER);
    if (status == NULL)                               return VTSS_RC_OK;

    return(rc_conv(vtss_eps_mgmt_state_get(instance, status)));
}





/****************************************************************************/
/*  EPS module interface - call out                                         */
/****************************************************************************/
void vtss_eps_tx_aps_info_set(const u32                       instance,
                              const vtss_appl_eps_domain_t    domain,
                              const u8                        *const aps_info)
{
    u32 rc, mep;
    u8  aps[MEP_APS_DATA_LENGTH];

    CRIT_ENTER(crit_p);
    mep = eps_data[instance].aps_mep;
    CRIT_EXIT(crit_p);

    if (mep == VTSS_APPL_EPS_MEP_INST_INVALID)
        return; /* No need to call MEP if aps_mep is invalid */

    memcpy(aps, aps_info, VTSS_EPS_APS_DATA_LENGTH);    /* This is only to satisfy LINT as a copy of VTSS_EPS_APS_DATA_LENGTH bytes will be copied from 'aps' array */
    rc = mep_tx_aps_info_set(mep, instance, aps, FALSE);
    if (rc)        T_D("Error during APS tx set %u", instance);
}

void vtss_eps_signal_out(const u32                       instance,
                         const vtss_appl_eps_domain_t    domain)
{
    u32 rc, w_mep, p_mep, aps_mep;

    CRIT_ENTER(crit_p);
    w_mep = eps_data[instance].w_mep;
    p_mep = eps_data[instance].p_mep;
    aps_mep = eps_data[instance].aps_mep;
    CRIT_EXIT(crit_p);

    rc = mep_signal_in(w_mep, instance);
    rc += mep_signal_in(p_mep, instance);
    if (aps_mep != p_mep)   rc += mep_signal_in(aps_mep, instance);
    if (rc)        T_D("Error during MEP signal %u", instance);
}

void vtss_eps_port_protection_set(const u32         w_port,
                                  const u32         p_port,
                                  const BOOL        active)
{
    /* Change in port protection selector state */
    mep_port_protection_change(w_port, p_port, active);
}




/****************************************************************************/
/*  EPS module interface - call in                                          */
/****************************************************************************/
mesa_rc eps_rx_aps_info_set(const u32    instance,
                            const u32    mep_inst,
                            const u8     aps[VTSS_EPS_APS_DATA_LENGTH])
{
    mesa_rc                 rc=VTSS_RC_OK;
    vtss_eps_flow_type_t    flow=VTSS_EPS_FLOW_WORKING;

    CRIT_ENTER(crit_p);
    if (eps_data[instance].aps_mep == mep_inst)    flow = VTSS_EPS_FLOW_PROTECTING;
    else
    if (eps_data[instance].w_mep == mep_inst)      flow = VTSS_EPS_FLOW_WORKING;
    else
        rc = VTSS_APPL_EPS_RC_INVALID_PARAMETER;
    CRIT_EXIT(crit_p);

    if ((rc==VTSS_RC_OK) && (vtss_eps_rx_aps_info_set(instance, flow, aps) != VTSS_EPS_RC_OK))
        rc = VTSS_APPL_EPS_RC_INVALID_PARAMETER;

    return(rc);
}


mesa_rc eps_signal_in(const u32   instance,
                      const u32   mep_inst)
{
    mesa_rc rc=VTSS_RC_OK;

    CRIT_ENTER(crit_p);
    if ((eps_data[instance].aps_mep != mep_inst) && (eps_data[instance].w_mep != mep_inst) && (eps_data[instance].p_mep != mep_inst))
        rc = VTSS_APPL_EPS_RC_INVALID_PARAMETER;
    CRIT_EXIT(crit_p);

    if ((rc==VTSS_RC_OK) && (vtss_eps_signal_in(instance) != VTSS_EPS_RC_OK))
        rc = VTSS_APPL_EPS_RC_INVALID_PARAMETER;

    return(rc);
}



mesa_rc eps_sf_sd_state_set(const u32   instance,
                            const u32   mep_inst,
                            const BOOL  sf_state,
                            const BOOL  sd_state)
{
    mesa_rc                 rc=VTSS_RC_OK;
    vtss_eps_flow_type_t    flow=VTSS_EPS_FLOW_WORKING;

    CRIT_ENTER(crit_p);
    if (eps_data[instance].p_mep == mep_inst)      flow = VTSS_EPS_FLOW_PROTECTING;
    else
    if (eps_data[instance].w_mep == mep_inst)      flow = VTSS_EPS_FLOW_WORKING;
    else
        rc = VTSS_APPL_EPS_RC_INVALID_PARAMETER;
    CRIT_EXIT(crit_p);

    if ((rc==VTSS_RC_OK) && (vtss_eps_sf_sd_state_set(instance, flow, sf_state, sd_state) != VTSS_EPS_RC_OK))
        rc = VTSS_APPL_EPS_RC_INVALID_PARAMETER;

    return(rc);
}


const char *eps_error_txt(mesa_rc error)
{
    switch (error) {
        case VTSS_APPL_EPS_RC_NOT_CREATED:       return ("EPS instance is not created");
        case VTSS_APPL_EPS_RC_CREATED:           return ("EPS instance is already created");
        case VTSS_APPL_EPS_RC_INVALID_PARAMETER: return ("Invalid parameter");
        case VTSS_APPL_EPS_RC_NOT_CONFIGURED:    return ("EPS instance is not yet configured - still default configuration");
        case VTSS_APPL_EPS_RC_ARCHITECTURE:      return ("Invalid architecture for this domain");
        case VTSS_APPL_EPS_RC_W_P_FLOW_EQUAL:    return ("The working and protection flows are equal");
        case VTSS_APPL_EPS_RC_WORKING_USED:      return ("The working flow is used by other EPS instance");
        case VTSS_APPL_EPS_RC_PROTECTING_USED:   return ("The protecting flow is used by other EPS instance");
        case VTSS_APPL_EPS_RC_W_P_SSF_MEP_EQUAL: return ("Working MEP and protecting SF MEP is same instance");
        case VTSS_APPL_EPS_RC_INVALID_APS_MEP:   return ("Invalid APS MEP instance");
        case VTSS_APPL_EPS_RC_INVALID_W_MEP:     return ("Invalid working SF MEP instance");
        case VTSS_APPL_EPS_RC_INVALID_P_MEP:     return ("Invalid protection SF MEP instance");
        case VTSS_APPL_EPS_RC_INVALID_COMMAND:   return ("Invalid command for this configuration");
        case VTSS_APPL_EPS_RC_INVALID_WTR:       return ("Invalid WTR time");
        case VTSS_APPL_EPS_RC_INVALID_HOLD_OFF:  return ("Invalid Hold off time");
        // By not specifying a "default:" we get lint or compiler warnings when not all are specified.
    }

    return "Unknown error code";
}

/****************************************************************************/
/*  EPS platform interface                                                  */
/****************************************************************************/

void vtss_eps_run(void)
{
    vtss_sem_post(&run_wait_sem);
}

void vtss_eps_timer_start(void)
{
    vtss_flag_setbits(&timer_wait_flag, FLAG_TIMER);
}

void vtss_eps_trace(const char  *const string,
                    const u32   param1,
                    const u32   param2,
                    const u32   param3,
                    const u32   param4)
{
    if ((param1 & 0xF0000000) == 0xF0000000)
        T_DG(TRACE_GRP_API, "%s - %u, %u, %u, %u", string, param1&~0xF0000000, param2, param3, param4);
    else
        T_D("%s - %u, %u, %u, %u", string, param1, param2, param3, param4);
}





/****************************************************************************/
/*  EPS Initialize module                                                   */
/****************************************************************************/
#ifdef VTSS_SW_OPTION_PRIVATE_MIB
VTSS_PRE_DECLS void eps_mib_init(void);
#endif
#ifdef VTSS_SW_OPTION_JSON_RPC
VTSS_PRE_DECLS void vtss_appl_eps_json_init(void);
#endif
extern "C" int eps_icli_cmd_register();

mesa_rc eps_init(vtss_init_data_t *data)
{
    u32           rc, i;
    vtss_isid_t   isid = data->isid;
    
    if (data->cmd == INIT_CMD_EARLY_INIT) {
        /* Initialize and register trace ressources */
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);
    }

    T_D("enter, cmd: %d, isid: %u, flags: 0x%x", data->cmd, data->isid, data->flags);
    switch (data->cmd)
    {
        case INIT_CMD_INIT:
            T_D("INIT");

            /* initialize critd */
            critd_init(&eps_crit, "EPS Crit",          VTSS_MODULE_ID_EPS, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
            critd_init(&crit_p,   "EPS Platform Crit", VTSS_MODULE_ID_EPS, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);

            vtss_sem_init(&run_wait_sem, 0);
            vtss_flag_init(&timer_wait_flag);

            vtss_thread_create(VTSS_THREAD_PRIO_HIGHER,
                              eps_timer_thread,
                              0,
                              "EPS Timer",
                              nullptr, 
                              0,
                              &timer_thread_handle,
                              &timer_thread_block);

            vtss_thread_create(VTSS_THREAD_PRIO_HIGHER,
                              eps_run_thread,
                              0,
                              "EPS State Machine",
                              nullptr, 
                              0,
                              &run_thread_handle,
                              &run_thread_block);

            rc = vtss_eps_init(10);
            if (rc)        T_D("Error during init");

            for (i=0; i<VTSS_APPL_EPS_CREATED_MAX; ++i)
                memset(&eps_data[i], 0, sizeof(eps_data_t));

            CRIT_EXIT(eps_crit);
            CRIT_EXIT(crit_p);

#ifdef VTSS_SW_OPTION_ICFG
            // Initialize ICLI "show running" configuration
            VTSS_RC(eps_icfg_init());
#endif
#ifdef VTSS_SW_OPTION_PRIVATE_MIB
            eps_mib_init(); /* Currently the MEP MIB is not allowed on Cisco target */
#endif
#ifdef VTSS_SW_OPTION_JSON_RPC
            vtss_appl_eps_json_init();
#endif
            eps_icli_cmd_register();
            break;
        case INIT_CMD_START:
            T_D("START");
            break;
        case INIT_CMD_CONF_DEF:
            T_D("CONF_DEF");
            if (isid == VTSS_ISID_LOCAL)
            {
                restore_to_default();
            }
            break;
        case INIT_CMD_MASTER_UP:
            T_D("MASTER_UP");
            restore_to_default();
            break;
        case INIT_CMD_MASTER_DOWN:
            T_D("MASTER_DOWN");
            break;
        case INIT_CMD_SWITCH_ADD:
            T_D("SWITCH_ADD");
            break;
        case INIT_CMD_SWITCH_DEL:
            T_D("SWITCH_DEL");
            break;
        case INIT_CMD_SUSPEND_RESUME:
            T_D("SUSPEND_RESUME");
            break;
        default:
            break;
    }

    T_D("exit");
    return 0;
}

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
