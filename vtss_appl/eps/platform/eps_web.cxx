/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

#include "web_api.h"
#include "eps_api.h"
#ifdef VTSS_SW_OPTION_PRIV_LVL
#include "vtss_privilege_api.h"
#include "vtss_privilege_web_api.h"
#endif

static const char *eps_web_error_txt(mesa_rc error)
{
    if (error == VTSS_RC_OK)
        return("");
    else
        return(error_txt(error));
}


/****************************************************************************/
/*  Web Handler  functions                                                  */
/****************************************************************************/

static i32 handler_config_eps_create(CYG_HTTPD_STATE *p)
{
    vtss_appl_eps_mep_t           mep;
    eps_create_param_t            param;
    vtss_appl_eps_state_t         state;
    int                           ct;
    int                           eps_id, domain, architecture, w_flow, p_flow, w_mep, p_mep, aps_mep;
    char                          buf[200];
    u32                           rc;
    static u32                    error = VTSS_RC_OK;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_EPS))
        return -1;
#endif

    if (p->method == CYG_HTTPD_METHOD_POST)
    {
        /* Delete EPS */
        for (eps_id=0; eps_id<VTSS_APPL_EPS_CREATED_MAX; ++eps_id)
        {
            if ((rc = eps_create_conf_get(eps_id, &param)) == VTSS_RC_OK)
            {   /* Created */
                sprintf(buf, "del_%u", eps_id+1);
                if (cyg_httpd_form_varable_find(p, buf))
                    if ((rc = vtss_appl_eps_create_conf_delete(eps_id)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;
            }
            else
                if ((rc != VTSS_APPL_EPS_RC_NOT_CREATED) && (error == VTSS_RC_OK))     error = rc;
        }

        /* Add EPS */
        if (cyg_httpd_form_varable_int(p, "new_eps", &eps_id))
        {
            if (cyg_httpd_form_varable_int(p, "dom", &domain))
            if (cyg_httpd_form_varable_int(p, "arch", &architecture))
            if (cyg_httpd_form_varable_int(p, "w_flow", &w_flow))
            if (cyg_httpd_form_varable_int(p, "p_flow", &p_flow))
            if (cyg_httpd_form_varable_int(p, "w_mep", &w_mep))
            if (cyg_httpd_form_varable_int(p, "p_mep", &p_mep))
            if (cyg_httpd_form_varable_int(p, "aps_mep", &aps_mep))
            {
                param.domain = (vtss_appl_eps_domain_t)domain;
                param.architecture = (vtss_appl_eps_architecture_t)architecture;
                param.w_flow = w_flow-1;
                param.p_flow = p_flow-1;
                mep.w_mep = w_mep-1;
                mep.p_mep = p_mep-1;
                mep.aps_mep = aps_mep-1;







                if ((rc = eps_create_conf_add(eps_id-1, &param)) != VTSS_RC_OK) { /* Create the EPS instance */
                    if (error == VTSS_RC_OK)     error = rc;
                } else {
                    if ((rc = vtss_appl_eps_mep_conf_set(eps_id-1, &mep)) != VTSS_RC_OK) { /* Add the MEP instances */
                        if (error == VTSS_RC_OK)     error = rc;
                        (void)vtss_appl_eps_create_conf_delete(eps_id-1);   /* Add of MEP instances failed - delete the EPS instance */
                    }
                }
            }
        }

        sprintf(buf, "/eps.htm");
        if (error != VTSS_RC_OK)
            sprintf(buf, "%s?error=%s", buf, eps_web_error_txt(error));
        redirect(p, buf);
    }
    else
    {
        /* CYG_HTTPD_METHOD_GET (+HEAD) */
        (void)cyg_httpd_start_chunked("html");




        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", 0);

        cyg_httpd_write_chunked(p->outbuffer, ct);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", VTSS_APPL_EPS_CREATED_MAX);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        for (eps_id=0; eps_id<VTSS_APPL_EPS_CREATED_MAX; ++eps_id)
        {
            if ((rc = eps_create_conf_get(eps_id, &param)) != VTSS_RC_OK) {
                if ((rc != VTSS_APPL_EPS_RC_NOT_CREATED) && (error == VTSS_RC_OK))     error = rc;
                continue;
            }
            if ((rc = vtss_appl_eps_mep_conf_get(eps_id, &mep)) != VTSS_RC_OK) {
                if (error == VTSS_RC_OK)     error = rc;
                continue;
            }
            w_flow = param.w_flow;
            p_flow = param.p_flow;

            /* Created */
            memset(&state, 0, sizeof(state));
            if ((rc = vtss_appl_eps_status_get(eps_id, &state)) != VTSS_RC_OK) {
                if (error == VTSS_RC_OK)     error = rc;
                continue;
            }
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u/%u/%u/%u/%u/%u/%u/%s/%u|",
                          eps_id+1,
                          (uint)param.domain,
                          (uint)param.architecture,




                          w_flow+1,
                          p_flow+1,

                          mep.w_mep+1,
                          mep.p_mep+1,
                          mep.aps_mep+1,
                          ((state.w_state != VTSS_APPL_EPS_DEFECT_STATE_OK) || (state.p_state != VTSS_APPL_EPS_DEFECT_STATE_OK) ||
                            state.dFop_pm || state.dFop_cm || state.dFop_nr || state.dFop_NoAps) ? "Down" : "Up",
                          VTSS_APPL_EPS_MEP_INST_INVALID+1);
            cyg_httpd_write_chunked(p->outbuffer, ct);
        }

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s", eps_web_error_txt(error));
        cyg_httpd_write_chunked(p->outbuffer, ct);

        error = VTSS_RC_OK;

        cyg_httpd_end_chunked();
    }

    return -1; // Do not further search the file system.
}

static i32 handler_config_eps(CYG_HTTPD_STATE* p)
{
    vtss_isid_t                   sid = web_retrieve_request_sid(p); /* Includes USID = ISID */
    u32                           rc, w_flow, p_flow;
    int                           eps_id, direct, wtr, hold, comm;
    BOOL                          aps, revert;
    vtss_appl_eps_mep_t           mep;
    eps_create_param_t  param;
    vtss_appl_eps_conf_t          conf;
    vtss_appl_eps_command_conf_t  control;
    vtss_appl_eps_state_t         state;
    int                           ct;
    char                          buf[32];
    static u32                    error = VTSS_RC_OK;

    if(redirectUnmanagedOrInvalid(p, sid)) /* Redirect unmanaged/invalid access to handler */
        return -1;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_EPS))
        return -1;
#endif

    //
    // Setting new configuration
    //
    if(p->method == CYG_HTTPD_METHOD_POST)
    {
        if (cyg_httpd_form_varable_int(p, "eps_id_hidden", &eps_id))
        {
            if ((rc = eps_create_conf_get(eps_id-1, &param)) != VTSS_RC_OK) {
                if (error == VTSS_RC_OK)     error = rc;
            } else if (((rc = vtss_appl_eps_conf_get(eps_id-1, &conf)) != VTSS_RC_OK) && (rc != VTSS_APPL_EPS_RC_NOT_CONFIGURED)) {
                if (error == VTSS_RC_OK)     error = rc;
            } else {
                /* Created */
                aps = revert = FALSE;
                direct = VTSS_APPL_EPS_BIDIRECTIONAL;
                (void)cyg_httpd_form_varable_int(p, "direct", &direct);
                if (cyg_httpd_form_varable_find(p, "aps"))   aps = TRUE;
                if (cyg_httpd_form_varable_find(p, "revert"))    revert = TRUE;

                if (cyg_httpd_form_varable_int(p, "wtr", &wtr))
                if (cyg_httpd_form_varable_int(p, "hold", &hold))
                {
                    conf.directional = (param.architecture == VTSS_APPL_EPS_ARCHITECTURE_1F1) ? VTSS_APPL_EPS_BIDIRECTIONAL : (vtss_appl_eps_directional_t)direct;
                    conf.aps = ((conf.directional == VTSS_APPL_EPS_BIDIRECTIONAL) || aps);
                    conf.revertive = revert;
                    conf.restore_timer = (u32)wtr;
                    conf.hold_off_timer = (u32)hold;
                    if ((rc = vtss_appl_eps_conf_set(eps_id-1,   &conf)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;
                }
                if (cyg_httpd_form_varable_int(p, "comm", &comm)) {
                    control.command = (vtss_appl_eps_command_t)comm;
                    if ((rc = vtss_appl_eps_command_conf_set(eps_id-1, &control)) != VTSS_RC_OK)
                        if (error == VTSS_RC_OK)     error = rc;
                }
            }
        }

        sprintf(buf, "/eps_config.htm?eps=%u", eps_id);
        if (error != VTSS_RC_OK)
            sprintf(buf, "%s&error=%s", buf, eps_web_error_txt(error));
        redirect(p, buf);
    }
    else
    {
        (void)cyg_httpd_start_chunked("html");

        memset(&conf, 0, sizeof(conf));
        memset(&param, 0, sizeof(param));
        memset(&state, 0, sizeof(state));
        control.command = VTSS_APPL_EPS_COMMAND_NONE;

        if (!cyg_httpd_form_varable_int(p, "eps", &eps_id)) {
            eps_id = 0;
        } else {
            eps_id--; // EPS appl instance start at 0 while web instance starts at 1
        }
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|",  eps_id+1);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        rc = eps_create_conf_get(eps_id, &param);
        if (rc != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        rc = vtss_appl_eps_conf_get(eps_id, &conf);
        if ((rc != VTSS_RC_OK) && (rc != VTSS_APPL_EPS_RC_NOT_CONFIGURED))
            if (error == VTSS_RC_OK)     error = rc;
        rc = vtss_appl_eps_mep_conf_get(eps_id, &mep);
        if (rc != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;

        w_flow = param.w_flow;
        p_flow = param.p_flow;

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u/%u/%u/%u/%u/%u/%u|",
                      (uint)param.domain,
                      (uint)param.architecture,




                      w_flow+1,
                      p_flow+1,

                      mep.w_mep+1,
                      mep.p_mep+1,
                      mep.aps_mep+1,
                      VTSS_APPL_EPS_MEP_INST_INVALID+1);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u/%u/%u/%u|",
                      (uint)conf.directional,
                      conf.aps,
                      conf.revertive,
                      conf.restore_timer,
                      conf.hold_off_timer);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        if ((rc = vtss_appl_eps_command_conf_get(eps_id, &control)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;
        if ((rc == VTSS_RC_OK) && (rc = vtss_appl_eps_status_get(eps_id, &state)) != VTSS_RC_OK)
            if (error == VTSS_RC_OK)     error = rc;

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|",
                      (uint)control.command);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u/%u/%u/%u/%u/%u/%u/%u/%s/%s/%s/%s|",
                      (uint)state.protection_state,
                      (uint)state.w_state,
                      (uint)state.p_state,
                      (uint)state.tx_aps.request,
                      state.tx_aps.re_signal,
                      state.tx_aps.br_signal,
                      (uint)state.rx_aps.request,
                      state.rx_aps.re_signal,
                      state.rx_aps.br_signal,
                      (state.dFop_pm) ? "Down" : "Up",
                      (state.dFop_cm) ? "Down" : "Up",
                      (state.dFop_nr) ? "Down" : "Up",
                      (state.dFop_NoAps) ? "Down" : "Up");
        cyg_httpd_write_chunked(p->outbuffer, ct);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s",  eps_web_error_txt(error));

        cyg_httpd_write_chunked(p->outbuffer, ct);

        error = VTSS_RC_OK;

        cyg_httpd_end_chunked();
    }
    return -1; // Do not further search the file system.
}

// Status
static i32 handler_status_eps(CYG_HTTPD_STATE* p)
{
#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_EPS))
        return -1;
#endif

    if(p->method == CYG_HTTPD_METHOD_GET) {
        (void)cyg_httpd_start_chunked("html");
        cyg_httpd_end_chunked();
    }
    return -1; // Do not further search the file system.
}


/****************************************************************************/
/*  Module JS lib config routine                                            */
/****************************************************************************/

#define EPS_WEB_BUF_LEN 512

static size_t eps_lib_config_js(char **base_ptr, char **cur_ptr, size_t *length)
{
    char buff[EPS_WEB_BUF_LEN];
    (void) snprintf(buff, EPS_WEB_BUF_LEN,
                    "var configEpsMin = %d;\n"
                    "var configEpsMax = %d;\n",
                    1,
                    VTSS_APPL_EPS_CREATED_MAX
        );
    return webCommonBufferHandler(base_ptr, cur_ptr, length, buff);
}

/****************************************************************************/
/*  JS lib config table entry                                               */
/****************************************************************************/

web_lib_config_js_tab_entry(eps_lib_config_js);


/****************************************************************************/
/*  HTTPD Table Entries                                                     */
/****************************************************************************/

CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_config_eps_create, "/config/epsCreate", handler_config_eps_create);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_config_eps, "/config/epsConfig", handler_config_eps);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_status_eps, "/stat/eps_status", handler_status_eps);

/****************************************************************************/
/*  End of file.                                                            */
/****************************************************************************/
