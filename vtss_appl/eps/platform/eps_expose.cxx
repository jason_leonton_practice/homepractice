/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/


#include "eps_serializer.hxx"
#include "vtss/appl/eps.h"

vtss_enum_descriptor_t eps_domain_txt[] {
    {VTSS_APPL_EPS_PORT, "port"},
    {VTSS_APPL_EPS_EVC, "evc"},
    {VTSS_APPL_EPS_MPLS_TUNNEL, "mplsTunnel"},
    {VTSS_APPL_EPS_MPLS_PW, "mplsPw"},
    {0, 0},
};


vtss_enum_descriptor_t eps_architecture_txt[] {
    {VTSS_APPL_EPS_ARCHITECTURE_1P1, "onePlusOne"},
    {VTSS_APPL_EPS_ARCHITECTURE_1F1, "oneForOne"},
    {0, 0},
};

vtss_enum_descriptor_t eps_directional_txt[] {
    {VTSS_APPL_EPS_UNIDIRECTIONAL, "uniDirectional"},
    {VTSS_APPL_EPS_BIDIRECTIONAL, "biDirectional"},
    {0, 0},
};

vtss_enum_descriptor_t eps_command_txt[] {
    {VTSS_APPL_EPS_COMMAND_NONE, "none"},
    {VTSS_APPL_EPS_COMMAND_CLEAR, "clear"},
    {VTSS_APPL_EPS_COMMAND_LOCK_OUT, "lockOut"},
    {VTSS_APPL_EPS_COMMAND_FORCED_SWITCH, "forcedSwitch"},
    {VTSS_APPL_EPS_COMMAND_MANUAL_SWITCH_P, "manualSwitchProtection"},
    {VTSS_APPL_EPS_COMMAND_MANUAL_SWITCH_W, "manualSwitchWorking"},
    {VTSS_APPL_EPS_COMMAND_EXERCISE, "exercise"},
    {VTSS_APPL_EPS_COMMAND_FREEZE, "localFreeze"},
    {VTSS_APPL_EPS_COMMAND_LOCK_OUT_LOCAL, "localLockOut"},
    {0, 0},
};

vtss_enum_descriptor_t eps_prot_state_txt[] {
    {VTSS_APPL_EPS_PROT_STATE_DISABLED, "disabled"},
    {VTSS_APPL_EPS_PROT_STATE_NO_REQUEST_W, "noRequestWorking"},
    {VTSS_APPL_EPS_PROT_STATE_NO_REQUEST_P, "noRequestProtecting"},
    {VTSS_APPL_EPS_PROT_STATE_LOCKOUT, "lockOut"},
    {VTSS_APPL_EPS_PROT_STATE_FORCED_SWITCH, "forcedSwitch"},
    {VTSS_APPL_EPS_PROT_STATE_SIGNAL_FAIL_W, "signalFailWorking"},
    {VTSS_APPL_EPS_PROT_STATE_SIGNAL_FAIL_P, "signalFailProtecting"},
    {VTSS_APPL_EPS_PROT_STATE_MANUAL_SWITCH_W, "manualSwitchWorking"},
    {VTSS_APPL_EPS_PROT_STATE_MANUAL_SWITCH_P, "manualSwitchProtecting"},
    {VTSS_APPL_EPS_PROT_STATE_WAIT_TO_RESTORE, "waitToRestore"},
    {VTSS_APPL_EPS_PROT_STATE_EXERCISE_W, "exerciseWorking"},
    {VTSS_APPL_EPS_PROT_STATE_EXERCISE_P, "exerciseProtecting"},
    {VTSS_APPL_EPS_PROT_STATE_REVERSE_REQUEST_W, "reverseRequestWorking"},
    {VTSS_APPL_EPS_PROT_STATE_REVERSE_REQUEST_P, "reverseRequestProtecting"},
    {VTSS_APPL_EPS_PROT_STATE_DO_NOT_REVERT, "doNotRevert"},
    {0, 0},
};

vtss_enum_descriptor_t eps_defect_state_txt[] {
    {VTSS_APPL_EPS_DEFECT_STATE_OK, "ok"},
    {VTSS_APPL_EPS_DEFECT_STATE_SD, "sd"},
    {VTSS_APPL_EPS_DEFECT_STATE_SF, "sf"},
    {0, 0},
};

vtss_enum_descriptor_t eps_request_txt[] {
    {VTSS_APPL_EPS_REQUEST_NR, "nr"},
    {VTSS_APPL_EPS_REQUEST_DNR, "dnr"},
    {VTSS_APPL_EPS_REQUEST_RR, "rr"},
    {VTSS_APPL_EPS_REQUEST_EXER, "exer"},
    {VTSS_APPL_EPS_REQUEST_WTR, "wtr"},
    {VTSS_APPL_EPS_REQUEST_MS_W, "msW"},
    {VTSS_APPL_EPS_REQUEST_MS_P, "msP"},
    {VTSS_APPL_EPS_REQUEST_SD, "sd"},
    {VTSS_APPL_EPS_REQUEST_SF_W, "sfW"},
    {VTSS_APPL_EPS_REQUEST_FS, "fs"},
    {VTSS_APPL_EPS_REQUEST_SF_P, "sfP"},
    {VTSS_APPL_EPS_REQUEST_LO, "lo"},
    {0, 0},
};

mesa_rc vtss_appl_eps_create_conf_default(u32 *k, vtss_appl_eps_create_param_t *s) {
    *k = 0;
    vtss_appl_eps_default_conf_t  def_conf;

    vtss_appl_eps_default_conf_get(&def_conf);

    *s = def_conf.param;

    return VTSS_RC_OK;
}
