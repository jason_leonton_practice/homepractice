/* Switch API software.

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef VTSS_ICLI_EPS_H
#define VTSS_ICLI_EPS_H

#include "icli_api.h"
#ifdef VTSS_SW_OPTION_ICFG
#include "icfg_api.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

BOOL eps_runtime_range_instance(u32                   session_id,
                                icli_runtime_ask_t    ask,
                                icli_runtime_t        *runtime);

BOOL eps_show_eps(i32 session_id, icli_range_t *inst,
                  BOOL has_detail);

BOOL eps_clear_eps(i32 session_id, u32 inst);

BOOL eps_eps(i32 session_id, u32 inst,
             BOOL has_port, BOOL has_tunnel_tp, BOOL has_pw, BOOL has_1p1, BOOL has_1f1, u32 flow_w, icli_switch_port_range_t port_w, u32 flow_p, icli_switch_port_range_t port_p);

BOOL eps_no_eps(i32 session_id, u32 inst);

BOOL eps_eps_mep(i32 session_id, u32 inst,
                 u32 mep_w, u32 mep_p, u32 mep_aps);

BOOL eps_eps_revertive(i32 session_id, u32 inst,
                       BOOL has_10s, BOOL has_30s, BOOL has_5m, BOOL has_6m, BOOL has_7m, BOOL has_8m, BOOL has_9m, BOOL has_10m, BOOL has_11m, BOOL has_12m, BOOL has_wtr_value, u32 wtr_value);

BOOL eps_no_eps_revertive(i32 session_id, u32 inst);

BOOL eps_eps_holdoff(i32 session_id, u32 inst,
                     u32 hold);

BOOL eps_no_eps_holdoff(i32 session_id, u32 inst);

BOOL eps_eps_1p1(i32 session_id, u32 inst,
                 BOOL has_bidirectional, BOOL has_unidirectional, BOOL has_aps);

BOOL eps_eps_command(i32 session_id, u32 inst,
                     BOOL has_lockout, BOOL has_forced, BOOL has_manualp, BOOL has_manualw, BOOL has_exercise, BOOL has_freeze, BOOL has_lockoutlocal);

BOOL eps_no_eps_command(i32 session_id, u32 inst);

mesa_rc eps_icfg_init(void);

#ifdef __cplusplus
}
#endif

#endif /* VTSS_ICLI_eps_H */



/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
