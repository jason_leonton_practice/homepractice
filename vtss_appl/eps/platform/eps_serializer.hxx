/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#ifndef __VTSS_EPS_SERIALIZER_HXX__
#define __VTSS_EPS_SERIALIZER_HXX__

#include "vtss_appl_serialize.hxx"
#include "vtss/appl/eps.h"

mesa_rc vtss_appl_eps_create_conf_default(u32 *k, vtss_appl_eps_create_param_t *s);

#define VTSS_PLING(x) #x
#define VTSS_I_TO_S(x) VTSS_PLING(x)

extern vtss_enum_descriptor_t eps_domain_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_eps_domain_t,
                         "EpsDomain", 
                         eps_domain_txt,
                         "The EPS protection domain.");


extern vtss_enum_descriptor_t eps_architecture_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_eps_architecture_t,
                         "EpsArchitecture", 
                         eps_architecture_txt,
                         "The EPS protection architecture.");

extern vtss_enum_descriptor_t eps_directional_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_eps_directional_t,
                         "EpsDirectional", 
                         eps_directional_txt,
                         "The EPS 1+1 directional.");

extern vtss_enum_descriptor_t eps_command_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_eps_command_t,
                         "EpsCommand", 
                         eps_command_txt,
                         "protection group command.");

extern vtss_enum_descriptor_t eps_prot_state_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_eps_prot_state_t,
                         "EpsProtectionState", 
                         eps_prot_state_txt,
                         "protection group state.");

extern vtss_enum_descriptor_t eps_defect_state_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_eps_defect_state_t,
                         "EpsDefectState", 
                         eps_defect_state_txt,
                         "Flow defect state.");

extern vtss_enum_descriptor_t eps_request_txt[];
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_eps_request_t,
                         "EpsRequest", 
                         eps_request_txt,
                         "APS request/state.");

VTSS_SNMP_TAG_SERIALIZE(EpsInstance, uint32_t, a, s) {
    a.add_leaf(vtss::AsInt(s.inner), vtss::tag::Name("Id"),
               vtss::expose::snmp::RangeSpec<uint32_t>(0, 2147483647),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(1),
               vtss::tag::Description("The EPS instance ID"));
}

template<typename T>
void serialize(T &a, vtss_appl_eps_capabilities_t &s)
{
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_eps_capabilities_t"));
    int ix = 1;

    m.add_leaf(s.created_max,
               vtss::tag::Name("InstanceMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Maximum number of created EPS instances"));
    m.add_leaf(s.wtr_max,
               vtss::tag::Name("WtrMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Maximum WTR timer value"));
    m.add_leaf(s.hoff_off,
               vtss::tag::Name("HoldOffOff"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Hold Off timer OFF value"));
    m.add_leaf(s.hoff_max,
               vtss::tag::Name("HoldOffMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Maximum Hold Off timer value"));
    m.add_leaf(s.mep_max,
               vtss::tag::Name("MepMax"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Maximum MEP relation instance value"));
    m.add_leaf(s.mep_invalid,
               vtss::tag::Name("MepInvalid"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Invalid MEP indication. This is returned when EPS has no MEP relations"));
}

template<typename T>
void serialize(T &a, vtss_appl_eps_create_param_t &s)
{
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_eps_create_param_t"));
    int ix = 1;
    m.add_leaf(s.architecture,
               vtss::tag::Name("Architecture"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The protection architecture. Cannot be changed after creation."));
    m.add_leaf(vtss::AsInterfaceIndex(s.w_flow),
               vtss::tag::Name("WorkingFlow"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The working flow. Cannot be changed after creation."));
    m.add_leaf(vtss::AsInterfaceIndex(s.p_flow),
               vtss::tag::Name("ProtectingFlow"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The protecting flow. Cannot be changed after creation."));
    m.add_leaf(s.domain,
               vtss::tag::Name("Domain"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The protection domain. Cannot be changed after creation."));
}

template<typename T>
void serialize(T &a, vtss_appl_eps_conf_t &s)
{
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_eps_conf_t"));
    int ix = 1;

    m.add_leaf(s.directional,
               vtss::tag::Name("Directional"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Protection is uni or bi directional. Only for 1+1."));
    m.add_leaf(vtss::AsBool(s.aps),
               vtss::tag::Name("ApsEnable"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("APS can be enabled or disabled. Only for 1+1."));
    m.add_leaf(vtss::AsBool(s.revertive),
               vtss::tag::Name("Revertive"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Revertive operation can enabled or disabled."));
    m.add_leaf(s.restore_timer,
               vtss::tag::Name("RestoreTimer"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Wait to restore timer in seconds - max. " VTSS_I_TO_S(VTSS_APPL_EPS_WTR_MAX) " - min. 1."));
    m.add_leaf(s.hold_off_timer,
               vtss::tag::Name("HoldOffTimer"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Hold off timer in 100 ms - max. " VTSS_I_TO_S(VTSS_APPL_EPS_HOFF_MAX) " - " VTSS_I_TO_S(VTSS_APPL_EPS_HOFF_OFF) " means no hold off"));
}

template<typename T>
void serialize(T &a, vtss_appl_eps_mep_t &s)
{
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_eps_mep_t"));
    int ix = 1;

    m.add_leaf(s.w_mep,
               vtss::tag::Name("WorkingMep"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Working SF MEP reference - max. " VTSS_I_TO_S(VTSS_APPL_EPS_MEP_MAX) " - min. 0. During get " VTSS_I_TO_S(VTSS_APPL_EPS_MEP_INST_INVALID) " indicate no reference"));
    m.add_leaf(s.p_mep,
               vtss::tag::Name("ProtectingMep"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Protecting SF MEP reference - max. " VTSS_I_TO_S(VTSS_APPL_EPS_MEP_MAX) " - min. 0. During get " VTSS_I_TO_S(VTSS_APPL_EPS_MEP_INST_INVALID) " indicate no reference"));
    m.add_leaf(s.aps_mep,
               vtss::tag::Name("ApsMep"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("APS MEP reference - max. " VTSS_I_TO_S(VTSS_APPL_EPS_MEP_MAX) " - min. 0. During get " VTSS_I_TO_S(VTSS_APPL_EPS_MEP_INST_INVALID) " indicate no reference"));
}

template<typename T>
void serialize(T &a, vtss_appl_eps_command_conf_t &s)
{
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_eps_command_conf_t"));
    int ix = 1;

    m.add_leaf(s.command,
               vtss::tag::Name("Command"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("protection group command"));
}


template<typename T>
void serialize(T &a, vtss_appl_eps_state_t &s)
{
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_eps_state_t"));
    int ix = 1;

    m.add_leaf(s.protection_state,
               vtss::tag::Name("ProtectionState"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Protection state according to to G.8031 Annex A"));
    m.add_leaf(s.w_state,
               vtss::tag::Name("WorkingState"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Working flow defect state"));
    m.add_leaf(s.p_state,
               vtss::tag::Name("ProtectingState"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Protecting flow defect state"));
    m.add_leaf(s.tx_aps.request,
               vtss::tag::Name("TransmittedApsRequest"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Transmitted APS request"));
    m.add_leaf(s.tx_aps.re_signal,
               vtss::tag::Name("TransmittedApsReSignal"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Transmitted APS requested signal"));
    m.add_leaf(s.tx_aps.br_signal,
               vtss::tag::Name("TransmittedApsBrSignal"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Transmitted APS bridged signal"));
    m.add_leaf(s.rx_aps.request,
               vtss::tag::Name("ReceivedApsRequest"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Received APS request"));
    m.add_leaf(s.rx_aps.re_signal,
               vtss::tag::Name("ReceivedApsReSignal"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Received APS requested signal"));
    m.add_leaf(s.rx_aps.br_signal,
               vtss::tag::Name("ReceivedApsBrSignal"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Received APS bridged signal"));
    m.add_leaf(vtss::AsBool(s.dFop_pm),
               vtss::tag::Name("DfopPm"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("FOP Protection type Mismatch - unexpected B bit"));
    m.add_leaf(vtss::AsBool(s.dFop_cm),
               vtss::tag::Name("DfopCm"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("FOP Configuration Mismatch - APS received on working"));
    m.add_leaf(vtss::AsBool(s.dFop_nr),
               vtss::tag::Name("DfopNr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("FOP Not expected Request - receiving request is not the expected (transmitted)"));
    m.add_leaf(vtss::AsBool(s.dFop_NoAps),
               vtss::tag::Name("DfopNoAps"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("FOP No APS received"));
}

namespace vtss {
namespace appl {
namespace eps {
namespace interfaces {

struct EpsCapabilitiesImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_appl_eps_capabilities_t *>
    > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_eps_capabilities_t &i) {
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_eps_capabilities_get);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_EPS);
};

struct EpsConfigInstanceTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_eps_create_param_t *>
    > P;

    static constexpr uint32_t snmpRowEditorOid = 100;
    static constexpr const char *table_description =
        "This is a table of created EPS instance parameters.";

    static constexpr const char *index_description =
        "This is a created EPS instance parameters. No parameters can be changed after create.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, EpsInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_eps_create_param_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_eps_create_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_eps_create_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_eps_create_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_eps_create_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_eps_create_conf_delete);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_eps_create_conf_default);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_EPS);
};

struct EpsConfigConfigTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_eps_conf_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created EPS instance configuration parameters.\
        When an EPS instance is created in the 'InstanceTable', an entry is automatically created here with default value.";

    static constexpr const char *index_description =
        "This is a created EPS instance configuration parameters";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, EpsInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_eps_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_eps_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_eps_create_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_eps_conf_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_EPS);
};

struct EpsConfigMepTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_eps_mep_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created EPS instance MEP configuration parameters.                              When an EPS instance is created in the 'InstanceTable', an entry is automatically created here with                              default value 100 - meaning no MEP instance related.";

    static constexpr const char *index_description =
        "This is a created EPS instance MEP configuration parameters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, EpsInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_eps_mep_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_eps_mep_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_eps_create_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_eps_mep_conf_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_EPS);
};

struct EpsControlTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_eps_command_conf_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created EPS instance command.                           When an EPS instance is created in the 'InstanceTable', an entry is automatically created here with 'no command'.";

    static constexpr const char *index_description =
        "This is a created EPS instance command";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, EpsInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_eps_command_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_eps_command_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_eps_create_conf_iter);
    VTSS_EXPOSE_SET_PTR(vtss_appl_eps_command_conf_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_EPS);
};

struct EpsStatusTableImpl {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_eps_state_t *>
    > P;

    static constexpr const char *table_description =
        "This is a table of created EPS instance status.";

    static constexpr const char *index_description =
        "This is a created EPS instance status.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, EpsInstance(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_eps_state_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_eps_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_eps_create_conf_iter);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_EPS);
};

}  // namespace interfaces
}  // namespace eps
}  // namespace appl
}  // namespace vtss

#endif
