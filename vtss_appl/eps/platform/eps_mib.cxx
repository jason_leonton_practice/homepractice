
/*
 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
*/

#include "eps_serializer.hxx"
#include "vtss/appl/eps.h"
VTSS_MIB_MODULE("EpsMib", "EPS", eps_mib_init, VTSS_MODULE_ID_EPS, root, h) {
    h.add_history_element("201407010000Z", "Initial version");
    h.add_history_element("201411050000Z", "Remove the vtss_appl_eps_domain_t type and the domain from vtss_appl_eps_create_param_t");
    h.add_history_element("201509220000Z", "Domain configuration added.");
    h.add_history_element("201511050000Z", "Domain configuration added at the end of configuration table");
    h.add_history_element("201705220000Z", "Removed definition of unused OID ...EpsControl");
    h.add_history_element("201711270000Z", "Editorial");
    h.description("This is a private Linear EPS (G.8031) MIB");
}

using namespace vtss;
using namespace expose::snmp;

#define NS(VAR, P, ID, NAME) static NamespaceNode VAR(&P, OidElement(ID, NAME))
namespace vtss {
namespace appl {
namespace eps {
namespace interfaces {
NS(eps, root, 1, "epsMibObjects");
NS(eps_config, eps, 2, "epsConfig");
NS(eps_status, eps, 3, "epsStatus");
// NS(eps_control, eps, 4, "epsControl"); // Not used, see Bz#22933

static StructRO2<EpsCapabilitiesImpl> eps_capabilities_impl(
        &eps, vtss::expose::snmp::OidElement(1, "epsCapabilities"));

static TableReadWriteAddDelete2<EpsConfigInstanceTableImpl> eps_config_instance_table_impl(
        &eps_config, vtss::expose::snmp::OidElement(1, "epsConfigInstanceTable"), vtss::expose::snmp::OidElement(2, "epsConfigInstanceRowEditor"));

static TableReadWrite2<EpsConfigConfigTableImpl> eps_config_config_table_impl(
        &eps_config, vtss::expose::snmp::OidElement(3, "epsConfigTable"));

static TableReadWrite2<EpsConfigMepTableImpl> eps_config_mep_table_impl(
        &eps_config, vtss::expose::snmp::OidElement(4, "epsConfigMepTable"));

static TableReadWrite2<EpsControlTableImpl> eps_control_table_impl(
        &eps_config, vtss::expose::snmp::OidElement(5, "epsConfigCommandTable"));

static TableReadOnly2<EpsStatusTableImpl> eps_status_table_impl(
        &eps_status, vtss::expose::snmp::OidElement(1, "epsStatusTable"));

}  // namespace interfaces
}  // namespace eps
}  // namespace appl
}  // namespace vtss

