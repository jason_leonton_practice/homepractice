/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

#include "web_api.h"
#ifdef VTSS_SW_OPTION_PRIV_LVL
#include "vtss_privilege_api.h"
#include "vtss_privilege_web_api.h"
#endif

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_MODBUS
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_MODBUS

/* =================
 * Trace definitions
 * -------------- */
#include <vtss_module_id.h>
/* ============== */
#include "vtss_modbus_api.h"



static i32 handler_config_modbus(CYG_HTTPD_STATE *p)
{
    int        ct;
    int mode, newmode;
    int        var_value;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_MODBUS)) {
        return -1;
    }
#endif

    if (p->method == CYG_HTTPD_METHOD_POST) {
        /* store form data */
        if (modbus_mgmt_conf_get(&mode) == VTSS_OK) {
            newmode = mode;

            //modbus_mode
            if (cyg_httpd_form_varable_int(p, "modbus_mode", &var_value)) {
                newmode = var_value;
            }

            if (newmode!=mode) {
                T_D("Calling modbus_mgmt_conf_set()");
                if (modbus_mgmt_conf_set(newmode) < 0) {
                    T_E("modbus_mgmt_conf_set(): failed");
                }
            }
        }

        redirect(p, "/modbus_config.htm");
    } else {                    /* CYG_HTTPD_METHOD_GET (+HEAD) */
        (void)cyg_httpd_start_chunked("html");

        /* get form data
           Format: [modbus_mode]
        */
        if (modbus_mgmt_conf_get(&mode) == VTSS_OK) {
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d", mode);
            (void)cyg_httpd_write_chunked(p->outbuffer, ct);
        }
        (void)cyg_httpd_end_chunked();
    }

    return -1; // Do not further search the file system.
}

/*  Module JS lib config routine                                            */
/****************************************************************************/

/****************************************************************************/
/*  JS lib config table entry                                               */
/****************************************************************************/

CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_config_modbus, "/config/modbus", handler_config_modbus);

