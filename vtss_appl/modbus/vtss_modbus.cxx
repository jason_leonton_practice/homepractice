/***********************************************************
 *
 * Copyright (c) 2018 LEONTON Technologies, Co.Ltd All Rights Reserved.
 *
 * Modbus Module
 *
 ***********************************************************/
#include "main.h"
#include "conf_api.h"
#include "critd_api.h"
#include "vtss_modbus.h"
#include "vtss_modbus_api.h"
#include "modbus_reg_table.h"
#include "icfg_api.h" // For vtss_icfg_query_request_t
#include <sys/time.h> 
#include "vtss_timer_api.h"

/****************************************************************************
 *  TRACE system
 ****************************************************************************/
#if (VTSS_TRACE_ENABLED)
static vtss_trace_reg_t trace_reg = {
	VTSS_TRACE_MODULE_ID, "Modbus", "Modbus Module"
};

static vtss_trace_grp_t trace_grps[VTSS_TRACE_GRP_CNT] = {
	/* VTSS_TRACE_GRP_DEFAULT */ {
		"default",
		"Default",
		VTSS_TRACE_LVL_WARNING,
		VTSS_TRACE_FLAGS_TIMESTAMP
	},
	/* VTSS_TRACE_GRP_CRIT */ {
		"crit",
		"Critical regions ",
		VTSS_TRACE_LVL_ERROR,
		VTSS_TRACE_FLAGS_TIMESTAMP
		},
	/* VTSS_TRACE_GRP_ICLI */ {
		"iCLI",
		"iCLI",
		VTSS_TRACE_LVL_ERROR,
		VTSS_TRACE_FLAGS_TIMESTAMP
	},
};
#define MODBUS_CRIT_ENTER() critd_enter(&crit, VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define MODBUS_CRIT_EXIT() critd_exit(&crit, VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)

#else /* !VTSS_TRACE_ENABLED */

#define MODBUS_CRIT_ENTER() critd_enter(&crit)
#define MODBUS_CRIT_EXIT() critd_exit(&crit)

#endif /* VTSS_TRACE_ENABLED */

extern "C" int modbus_icli_cmd_register();

static vtss::Timer modbus_cln_timer_local[MODBUS_TCP_SOCKET_ALLOW_NUM];


/****************************************************************************
 *  Pre-defined functions
 ****************************************************************************/
static vtss_rc modbus_tcp_srv_open(modbus_global_t *modbus);
static vtss_rc modbus_tcp_srv_close(modbus_global_t *modbus);

/****************************************************************************
 *  Global variables
 ****************************************************************************/

static critd_t crit;
static modbus_global_t g_modbus;


/***************************************************************************/
/* ICFG (Show running)                                                     */
/***************************************************************************/
#ifdef VTSS_SW_OPTION_ICFG

static void modbus_icfg_reset_to_default(void)
{
	T_D("Do reset conf to default");

	modbus_mgmt_conf_set(MODBUS_MGMT_MODE_DEFAULT);
}

// Function called by ICFG.

/* ICFG callback functions */
static mesa_rc modbus_icfg_global_conf(const vtss_icfg_query_request_t *req,
                                     vtss_icfg_query_result_t *result)
{
    mesa_rc     rc = VTSS_OK;
    int mode;

    if ((rc = modbus_mgmt_conf_get(&mode)) != VTSS_OK) {
        return rc;
    }
    
    if (req->all_defaults ||(mode!=MODBUS_MGMT_MODE_DEFAULT)) {
        rc = vtss_icfg_printf(result, "%s%s\n",mode == MODBUS_MGMT_ENABLED? "" : "no ","modbus-tcp");
    }

    return rc;
}


/* ICFG Initialization function */
static mesa_rc modbus_icfg_init(void)
{
	VTSS_RC(vtss_icfg_query_register(VTSS_ICFG_MODBUS_GLOBAL_CONF, "modbus-tcp", modbus_icfg_global_conf));
	return VTSS_RC_OK;
}

#endif

/*************************************************************************
 *  Public functions
 *************************************************************************/

mesa_rc modbus_mgmt_conf_get(int * mode)
{
    MODBUS_CRIT_ENTER();
    *mode = g_modbus.mode;
    MODBUS_CRIT_EXIT();
    
    return VTSS_OK;

}

mesa_rc modbus_mgmt_conf_set(int mode)
{
    if(mode!=MODBUS_MGMT_ENABLED && mode!=MODBUS_MGMT_DISABLED){
        return VTSS_RC_ERROR;
    }
    MODBUS_CRIT_ENTER();
    if(g_modbus.mode==mode){
        MODBUS_CRIT_EXIT();
        return VTSS_OK;
    }
    
    g_modbus.mode=mode;
   
    if(mode){
        modbus_tcp_srv_open(&g_modbus);
    }else{
        modbus_tcp_srv_close(&g_modbus);
    }
    
    MODBUS_CRIT_EXIT();
    return VTSS_OK;
}

vtss_rc modbus_srv_state_get(modbus_srv_state_t *state)
{
    MODBUS_CRIT_ENTER();
    if(g_modbus.srv->fd >= 0) {
        *state = MODBUS_SRV_ENABLED;
    } else {
        *state = MODBUS_SRV_DISABLED;
    }
    MODBUS_CRIT_EXIT();

    return VTSS_OK;
}

/*************************************************************************
 *  Private functions
 *************************************************************************/
    
static void modbus_timer_cb(vtss::Timer *timer)
{
    int           cln_idx = (int)timer->user_data;
    modbus_cln_t  *mbs_cln = NULL;
    
    MODBUS_CRIT_ENTER();
    mbs_cln= &g_modbus.cln[cln_idx];

    close(mbs_cln->fd);
    mbs_cln->fd = -1;
    mbs_cln->flags &= (~MODBUS_CLN_FLAG_NONE);
    MODBUS_CRIT_EXIT();
}


static void modbus_cln_timer_start(int cln_idx)
{
    vtss::Timer *timer = &modbus_cln_timer_local[cln_idx];
    timer->set_period(vtss::seconds(MODBUS_CLN_CONECTION_TIMEOUT));
    timer->set_repeat(FALSE);
    T_D("%s cln_idx: %d", __func__,cln_idx);
    if (vtss_timer_start(timer) != VTSS_RC_OK) {
        T_D("Unable to start timer");
    }        
}


static void modbus_cln_timer_stop(int cln_idx)
{
    vtss::Timer *timer = &modbus_cln_timer_local[cln_idx];
    T_D("%s cln_idx: %d", __func__,cln_idx);
    if (vtss_timer_cancel(timer) != VTSS_RC_OK) {
        T_D("Unable to stop timer");
    }
}


/* modbus cln timer init */
static void modbus_cln_timer_init()
{
    int cln_idx=0;

    for(cln_idx = 0; cln_idx < MODBUS_TCP_SOCKET_ALLOW_NUM; ++cln_idx) {
        vtss::Timer *timer = &modbus_cln_timer_local[cln_idx];
        timer->callback    = modbus_timer_cb;
        timer->modid       = VTSS_MODULE_ID_MODBUS;
        timer->user_data   = (void *)(cln_idx);
    }
}



static vtss_rc modbus_tcp_srv_open(modbus_global_t *modbus)
{
    modbus_srv_t            *mbs_srv = modbus->srv;

    if(mbs_srv->fd >= 0) {
        return VTSS_OK;
    }

    if((mbs_srv->fd = modbus_tcp_listen(modbus->ctx, MODBUS_TCP_SOCKET_ALLOW_NUM)) < 0) {       
        T_D("Modbus TCP listening failed! fd: %d", mbs_srv->fd);
        return VTSS_RC_ERROR;
    }

    return VTSS_OK;
}


static vtss_rc modbus_tcp_srv_close(modbus_global_t *modbus)
{
    modbus_srv_t            *mbs_srv = modbus->srv;
    modbus_cln_t            *mbs_cln = NULL;

    if(mbs_srv->fd < 0) {
        return VTSS_OK;
    }

    for(int i = 0; i < MODBUS_TCP_SOCKET_ALLOW_NUM; ++i) {
        mbs_cln = modbus->cln + i;

        if(mbs_cln->fd >= 0) {
            close(mbs_cln->fd);
            mbs_cln->fd = -1;
            mbs_cln->flags = MODBUS_CLN_FLAG_NONE;
        }
    }

    modbus_set_socket(modbus->ctx, mbs_srv->fd);
    modbus_close(modbus->ctx);
    mbs_srv->fd = -1;
    mbs_srv->flags = MODBUS_SRV_FLAG_NONE;

    return VTSS_OK;
}

static vtss_rc modbus_socket_handle(int fd, modbus_mapping_t *reg_tbl, modbus_t *ctx, uint8_t *adu)
{
    int                     ret = 0;
    
    memset(adu, 0, (sizeof(uint8_t) * MODBUS_TCP_MAX_ADU_LENGTH));
    modbus_set_socket(ctx, fd);

    if((ret = modbus_receive(ctx, adu)) < 0) {
        return VTSS_RC_ERROR;
    }

    if(MODBUS_TCP_GET_ADU_ADDR(adu) > MODBUS_REGTBL_MAX_REGISTER_NUM) {
        if((ret = modbus_reply_exception(ctx, adu, MODBUS_EXCEPTION_ILLEGAL_DATA_ADDRESS)) < 0) {
            T_D("Server reply response error! [%s]", strerror(errno));
        }
        return (ret < 0) ? VTSS_RC_ERROR : VTSS_OK;
    }

    T_D("func: %d, addr: %d, num: %d\n", MODBUS_TCP_GET_ADU_FUNC(adu), MODBUS_TCP_GET_ADU_ADDR(adu), MODBUS_TCP_GET_ADU_NUM(adu));

    switch(MODBUS_TCP_GET_ADU_FUNC(adu)) {
        case MODBUS_FUNC_READ_INPUT_REGISTERS:
            switch(MODBUS_REGTBL_GET_TYPE_FROM_ADDR(MODBUS_TCP_GET_ADU_ADDR(adu))) {
                case MODBUS_REGTBL_TYPE_SYSINFO:
                    if(modbus_regtbl_sysinfo_update(reg_tbl) < 0) {
                        T_W("Modbus update registers table failed!");
                    }
                    break;
                case MODBUS_REGTBL_TYPE_PORTINFO:
                    if(modbus_regtbl_portinfo_update(reg_tbl) < 0) {
                        T_W("Modbus update registers table failed!");
                    }
                    break;
                case MODBUS_REGTBL_TYPE_PACKETINFO:
                    if(modbus_regtbl_pktinfo_update(reg_tbl) < 0) {
                        T_W("Modbus update registers table failed!");
                    }
                    break;
                default:
                    T_D("Should not be here! type: %u", MODBUS_TCP_GET_ADU_FUNC(adu));
                    break;
            }

            if((ret = modbus_reply(ctx, adu, ret, reg_tbl)) < 0) {
                T_D("Server reply response error! [%s]", strerror(errno));
            }
            break;
        default:
            T_D("ModbusTCP function code does not support! func_code: %u", MODBUS_TCP_GET_ADU_FUNC(adu));
            if(modbus_reply_exception(ctx, adu, MODBUS_EXCEPTION_ILLEGAL_FUNCTION) < 0) {
                T_D("Server reply response error! [%s]", strerror(errno));
                return VTSS_RC_ERROR;
            }
            break;
    }

    return VTSS_OK;
}

static void modbus_process_socket(modbus_global_t *modbus)
{
    modbus_srv_t            *mbs_srv = modbus->srv;
    modbus_cln_t            *mbs_cln = NULL;

    if(mbs_srv->flags & MODBUS_SRV_FLAG_ACCEPT) {
        for(int i = 0; i < MODBUS_TCP_SOCKET_ALLOW_NUM; ++i) {
            mbs_cln = modbus->cln + i;

            if(mbs_cln->fd < 0) {
                if((mbs_cln->fd = modbus_tcp_accept(modbus->ctx, &(mbs_srv->fd))) >= 0) {
                    mbs_cln->flags = MODBUS_CLN_FLAG_NONE;
                }
                break;
            }
        }
        mbs_srv->flags &= (~MODBUS_SRV_FLAG_ACCEPT);
    }

    for(int i = 0; i < MODBUS_TCP_SOCKET_ALLOW_NUM; ++i) {
        mbs_cln = modbus->cln + i;

        if(mbs_cln->fd >= 0 && (mbs_cln->flags & MODBUS_CLN_FLAG_READABLED)) {
            modbus_cln_timer_stop(i);
            if(modbus_socket_handle(mbs_cln->fd, modbus->reg_tbl, modbus->ctx, modbus->adu) < 0) {
                close(mbs_cln->fd);
                mbs_cln->fd = -1;
            }else{
                modbus_cln_timer_start(i);
            }
            mbs_cln->flags = MODBUS_CLN_FLAG_NONE;
        }
    }
}

static int modbus_socket_select(modbus_global_t *modbus)
{
    modbus_srv_t            *mbs_srv = modbus->srv;
    modbus_cln_t            *mbs_cln = NULL;
    int                     events = 0;
    int                     highest_fd = 0;
    fd_set                  rdset;
    struct timeval          tv;

    FD_ZERO(&rdset);
    tv.tv_sec = (MODBUS_TCP_SOCKET_TIMEOUT) / 1000;
    tv.tv_usec = (MODBUS_TCP_SOCKET_TIMEOUT % 1000) * 1000;

    if(mbs_srv->fd < 0) {
        T_D("Modbus state error! Should not be here!");
        return VTSS_RC_ERROR;
    }

    FD_SET(mbs_srv->fd, &rdset);
    highest_fd = mbs_srv->fd;

    for(int i = 0; i < MODBUS_TCP_SOCKET_ALLOW_NUM; ++i) {
        mbs_cln = modbus->cln + i;

        if(mbs_cln->fd >= 0) {
            FD_SET(mbs_cln->fd, &rdset);
            highest_fd = MAX(highest_fd, mbs_cln->fd);
        }
    }

    if((events = select(highest_fd + 1, &rdset, NULL, NULL, &tv)) <= 0) {
        return events;
    }

    if(FD_ISSET(mbs_srv->fd, &rdset)) {
        mbs_srv->flags |= MODBUS_SRV_FLAG_ACCEPT;
    }

    for(int i = 0; i < MODBUS_TCP_SOCKET_ALLOW_NUM; ++i) {
        mbs_cln = modbus->cln + i;

        if(mbs_cln->fd >= 0 && FD_ISSET(mbs_cln->fd, &rdset)) {
            mbs_cln->flags |= MODBUS_CLN_FLAG_READABLED;
        }
    }

    return events;
}


static void modbus_cleanup(modbus_global_t *modbus)
{
    if(modbus->reg_tbl) {
        modbus_mapping_free(modbus->reg_tbl);
    }

    if(modbus->srv) {
        free(modbus->srv);
    }

    if(modbus->ctx) {
        modbus_free(modbus->ctx);
    }

    if(modbus->adu) {
        free(modbus->adu);
    }

    if(modbus->cln) {
        free(modbus->cln);
    }
}



static vtss_rc modbus_setup(modbus_global_t *modbus)
{
    if(!(modbus->reg_tbl = modbus_mapping_new(0, 0, 0, MODBUS_REGTBL_MAX_REGISTER_NUM))) {
        T_E("Modbus mapping allocate failed! size: %u", (MODBUS_REGTBL_MAX_REGISTER_NUM * sizeof(uint16_t)));
        return VTSS_RC_ERROR;
    }
    
    if(!VTSS_CALLOC_CAST(modbus->srv, 1, sizeof(modbus_srv_t))) {
        T_E("Memory allocate failed! size: %u", (1 * sizeof(modbus_srv_t)));
        modbus_cleanup(modbus);
        return VTSS_RC_ERROR;
    }

    if(!(modbus->ctx = modbus_new_tcp("127.0.0.1", MODBUS_TCP_DEFAULT_PORT))) {
        T_E("Create modbus tcp handle failed!");
        modbus_cleanup(modbus);
        return VTSS_RC_ERROR;
    }

    if(!VTSS_CALLOC_CAST(modbus->adu, MODBUS_TCP_MAX_ADU_LENGTH, sizeof(uint8_t))) {
        T_E("Memory allocate failed! size: %u", (MODBUS_TCP_MAX_ADU_LENGTH * sizeof(uint8_t)));
        modbus_cleanup(modbus);
        return VTSS_RC_ERROR;
    }

    if(!VTSS_CALLOC_CAST(modbus->cln, MODBUS_TCP_SOCKET_ALLOW_NUM, sizeof(modbus_cln_t))) {
        T_E("Memory allocate failed! size: %u", (MODBUS_TCP_SOCKET_ALLOW_NUM * sizeof(modbus_cln_t)));
        modbus_cleanup(modbus);
        return VTSS_RC_ERROR;
    }

    if(modbus_regtbl_fetch(modbus->reg_tbl) < 0) {
        T_E("Modbus registers table getting failed!");
        modbus_cleanup(modbus);
        return VTSS_RC_ERROR;
    }

    for(int i = 0; i < MODBUS_TCP_SOCKET_ALLOW_NUM; ++i) {
        modbus->cln[i].fd = -1;
    }
    modbus->srv->fd = -1;

    /*  180615 Kira Hsieh: Remember remove by default   */
#if 0
    if(modbus_tcp_srv_open(modbus) < 0) {
        
        modbus_cleanup(modbus);
        return VTSS_RC_ERROR;
    }
#endif
    return VTSS_OK;
}


/**********************************************************************
 *  Thread
 **********************************************************************/
static vtss_handle_t modbus_thread_handle;
static vtss_thread_t modbus_thread_block;

/*  Thread function    */
static void modbus_thread(vtss_addrword_t data)
{
    if(modbus_setup(&g_modbus) < 0) {
        return;
    }

    modbus_cln_timer_init();

    while(1) {
        MODBUS_CRIT_ENTER();

        if(g_modbus.srv->fd >= 0) {
            if(modbus_socket_select(&g_modbus) > 0) {
                modbus_process_socket(&g_modbus);
            }
        }
        
        MODBUS_CRIT_EXIT();
        
        VTSS_OS_MSLEEP(500);
    }

    modbus_tcp_srv_close(&g_modbus);
    modbus_cleanup(&g_modbus);
}


/****************************************************************************
 *  Initialization functions
 ****************************************************************************/

/* Initialize module */
mesa_rc modbus_init(vtss_init_data_t *data)
{
	switch (data->cmd) {
	case INIT_CMD_EARLY_INIT:
		/* Initialize and register trace ressources */
		VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, VTSS_TRACE_GRP_CNT);
		VTSS_TRACE_REGISTER(&trace_reg);
		break;

	case INIT_CMD_INIT:
		T_D("INIT");
        modbus_icli_cmd_register();
        
		critd_init(&crit, "Modbus crit", VTSS_MODULE_ID_MODBUS, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
		MODBUS_CRIT_EXIT();
      
#ifdef VTSS_SW_OPTION_ICFG
		modbus_icfg_init();
#endif
		vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
						modbus_thread,
						0,
						"MODBUS",
						nullptr,
						0,
						&modbus_thread_handle,
						&modbus_thread_block);

		break;
	case INIT_CMD_START:
		T_D("START");
		break;
	case INIT_CMD_CONF_DEF:
		T_D("CONF");
        modbus_icfg_reset_to_default();
		break;
	case INIT_CMD_MASTER_UP:
		T_D("MASTER UP");
		break;
	case INIT_CMD_MASTER_DOWN:
		T_D("MASTER DOWN");
		break;
	case INIT_CMD_SWITCH_ADD:
		T_D("SWITCH ADD");
		break;
	case INIT_CMD_SWITCH_DEL:
		T_D("SWITCH DEL");
		break;
	case INIT_CMD_SUSPEND_RESUME:
		T_D("SUSPEND RESUME");
		break;
	default:
		break;
	}

	T_D("EVENT EXIT");
	return 0;
}
