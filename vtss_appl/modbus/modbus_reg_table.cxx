/***********************************************************
 *
 * Copyright (c) 2018 LEONTON Technologies, Co.Ltd All Rights Reserved.
 *
 * Modbus Module
 *
 ***********************************************************/
#include "main.h"
     
#include "vtss_modbus.h"
#include "modbus_reg_table.h"

#include "snmp_custom_api.h"
#include "conf_api.h"
#include "event_warning_api.h"
#include "sysutil_api.h"
#include "vtss/appl/firmware.h"



/***************************************************************************
 * Private functions
 ***************************************************************************/
static void str2reg(uint16_t *dest_addr, char *str, unsigned int len)
{
    for(int i = 0; i < len; i++) {
        if((i % 2) == 0) {
            dest_addr[(i / 2)] = (uint16_t)((str[i] & 0xFF) << 8);
        } else {
            dest_addr[(i / 2)] = dest_addr[(i / 2)] | (uint16_t)((str[i] & 0xFF));
        }
    }
}

static void ul2reg(uint16_t *dest_addr, u64 value)
{
    *(dest_addr) = (uint16_t)(value >> 48);
    *(dest_addr + 1) = (uint16_t)(value >> 32);
    *(dest_addr + 2) = (uint16_t)(value >> 16);
    *(dest_addr + 3) = (uint16_t)(value);
}

static vtss_rc modbus_regtbl_get_vendor_id(uint16_t *tab_input_registers, unsigned int num)
{
    uint16_t            vendor_id = 0;
    u_long              *snmp_enterprise_id = snmp_private_mib_sysobjectid_early_get();

    vendor_id = (uint16_t)(snmp_enterprise_id[6] & 0xFFFFFFFF);

    T_D("vendor_id: 0x%x", vendor_id);

    memcpy(tab_input_registers, &vendor_id, (sizeof(uint16_t) * num));

    return VTSS_OK;
}

static vtss_rc modbus_regtbl_get_modelname(uint16_t *tab_input_registers, unsigned int num)
{
    unsigned int            len = 0;
    char                    modelname[MODBUS_DEFAULT_BUFF_LEN] = {0};

    memset(modelname, 0, sizeof(modelname));
    conf_mgmt_modelname_get(modelname, LNTN_BOARDENV_MODEL_NAME_LEN);

    T_D("modelname: %s", modelname);
    len = (strlen(modelname) > MODBUS_REGTBL_GET_BYTE_FROM_WORD(num) ? MODBUS_REGTBL_GET_BYTE_FROM_WORD(num) : strlen(modelname));

    str2reg(tab_input_registers, modelname, len);

    return VTSS_OK;
}

static vtss_rc modbus_regtbl_get_sysname(uint16_t *tab_input_registers, unsigned int num)
{
    int                     rc = VTSS_OK;
    unsigned int            len = 0;
    system_conf_t           sys_conf;

    memset(&sys_conf, 0, sizeof(system_conf_t));

    if((rc = system_get_config(&sys_conf)) < 0) {
        T_W("Get system config from sysutil module failed! rc: %d\n", rc);
        return VTSS_RC_ERROR;
    }

    T_D("sysname: %s", sys_conf.sys_name);
    len = (strlen(sys_conf.sys_name) > MODBUS_REGTBL_GET_BYTE_FROM_WORD(num) ? MODBUS_REGTBL_GET_BYTE_FROM_WORD(num) : strlen(sys_conf.sys_name));

    str2reg(tab_input_registers, sys_conf.sys_name, len);

    return VTSS_OK;
}

static vtss_rc modbus_regtbl_get_syslocation(uint16_t *tab_input_registers, unsigned int num)
{
    int                     rc = VTSS_OK;
    unsigned int            len = 0;
    system_conf_t           sys_conf;

    memset(&sys_conf, 0, sizeof(system_conf_t));

    if((rc = system_get_config(&sys_conf)) < 0) {
        T_W("Get system config from sysutil module failed! rc: %d\n", rc);
        return VTSS_RC_ERROR;
    }

    T_D("location: %s", sys_conf.sys_location);
    len = (strlen(sys_conf.sys_location) > MODBUS_REGTBL_GET_BYTE_FROM_WORD(num) ? MODBUS_REGTBL_GET_BYTE_FROM_WORD(num) : strlen(sys_conf.sys_location));

    str2reg(tab_input_registers, sys_conf.sys_location,len);


    return VTSS_OK;
}

static vtss_rc modbus_regtbl_get_version(uint16_t *tab_input_registers, unsigned int num)
{
    int                     rc = VTSS_OK;
    unsigned int            len = 0;
    vtss_appl_firmware_status_image_t   imageEntry;

    memset(&imageEntry, 0, sizeof(vtss_appl_firmware_status_image_t));

    if((rc = vtss_appl_firmware_status_image_entry_get(VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ACTIVE_FIRMWARE, &imageEntry)) < 0) {
        T_W("Get firmware status from firmware module failed! rc: %d\n", rc);
        return VTSS_RC_ERROR;
    }

    T_D("ver code: %s", imageEntry.version + 1);
    len = (strlen(imageEntry.version + 1) > MODBUS_REGTBL_GET_BYTE_FROM_WORD(num) ? MODBUS_REGTBL_GET_BYTE_FROM_WORD(num) : strlen(imageEntry.version + 1));

    str2reg(tab_input_registers, imageEntry.version + 1, len);

    return VTSS_OK;
}

static vtss_rc modbus_regtbl_get_release_date(uint16_t *tab_input_registers, unsigned int num)
{
    int                     rc = VTSS_OK;
    unsigned int            len = 0;
    vtss_appl_firmware_status_image_t   imageEntry;
    char                    buff[MODBUS_DEFAULT_BUFF_LEN] = {0};

    memset(&imageEntry, 0, sizeof(vtss_appl_firmware_status_image_t));

    if((rc = vtss_appl_firmware_status_image_entry_get(VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ACTIVE_FIRMWARE, &imageEntry)) < 0) {
        T_W("Get firmware status from firmware module failed! rc: %d\n", rc);
        return VTSS_RC_ERROR;
    }

    strncpy(buff, imageEntry.built_date + 2, 2);       //  yy
    strncpy(buff + 2, imageEntry.built_date + 5, 2);   //  MM
    strncpy(buff + 4, imageEntry.built_date + 8, 2);   //  dd
    strncpy(buff + 6, imageEntry.built_date + 11, 2);   //  HH
    strncpy(buff + 8, imageEntry.built_date + 14, 2);   //  mm

    T_D("buildtime: %s", buff);
    len = (strlen(buff) > MODBUS_REGTBL_GET_BYTE_FROM_WORD(num) ? MODBUS_REGTBL_GET_BYTE_FROM_WORD(num) : strlen(buff));

    str2reg(tab_input_registers, buff, len);

    return VTSS_OK;
}

static vtss_rc modbus_regtbl_get_serial_num(uint16_t *tab_input_registers, unsigned int num)
{
    int                     rc = VTSS_OK;
    unsigned int            len = 0;
    char                    board_id[MODBUS_DEFAULT_BUFF_LEN] = {0};

    memset(board_id, 0, sizeof(board_id));
    if((rc=conf_mgmt_sn_get(board_id, LNTN_BOARDENV_SN_LEN))<0){
        T_W("Get serial_num from conf module failed! rc: %d\n", rc);
        return VTSS_RC_ERROR;
    }

    T_D("serial_num: %s", board_id);
    len = (strlen(board_id) > MODBUS_REGTBL_GET_BYTE_FROM_WORD(num) ? MODBUS_REGTBL_GET_BYTE_FROM_WORD(num) : strlen(board_id));

    str2reg(tab_input_registers, board_id, len);

    return VTSS_OK;
}

static vtss_rc modbus_regtbl_get_mac_addr(uint16_t *tab_input_registers, unsigned int num)
{
    conf_board_t            board_conf;

    memset(&board_conf, 0, sizeof(conf_board_t));

    conf_mgmt_board_get(&board_conf);

    T_D("mac_addr: %02x:%02x:%02x:%02x:%02x:%02x",
        board_conf.mac_address[0], board_conf.mac_address[1], board_conf.mac_address[2],
        board_conf.mac_address[3], board_conf.mac_address[4], board_conf.mac_address[5]);

    str2reg(tab_input_registers, (char *)board_conf.mac_address, sizeof(uint16_t) * num);

    return VTSS_OK;
}

static vtss_rc modbus_regtbl_get_power_p1_status(uint16_t *tab_input_registers, unsigned int num)
{
    uint16_t                status = (event_warning_power_status_get(EVENT_POWER_P1) ? MODBUS_POWER_STATUS_ON : MODBUS_POWER_STATUS_OFF);

    T_D("Power 1: %s", (status == MODBUS_POWER_STATUS_OFF) ? "OFF" : "ON");

    memcpy(tab_input_registers, &status, sizeof(uint16_t) * num);

    return VTSS_OK;
}

static vtss_rc modbus_regtbl_get_power_p2_status(uint16_t *tab_input_registers, unsigned int num)
{
    uint16_t                status = (event_warning_power_status_get(EVENT_POWER_P2) ? MODBUS_POWER_STATUS_ON : MODBUS_POWER_STATUS_OFF);

    T_D("Power 2: %s", (status == MODBUS_POWER_STATUS_OFF) ? "OFF" : "ON");

    memcpy(tab_input_registers, &status, sizeof(uint16_t) * num);

    return VTSS_OK;
}

static vtss_rc modbus_regtbl_get_relay_stat(uint16_t *tab_input_registers, unsigned int num)
{
    uint16_t                status = ((event_warning_relay_status_get() == EVENT_RELAY_OPEN) ? MODBUS_RELAY_STATUS_OPEN : MODBUS_RELAY_STATUS_CLOSE);

    T_D("Relay: %s", (status == MODBUS_RELAY_STATUS_CLOSE) ? "CLOSE" : "OPEN");

    memcpy(tab_input_registers, &status, sizeof(uint16_t) * num);

    return VTSS_OK;
}

/***************************************************************************
 * Public functions
 ***************************************************************************/
vtss_rc modbus_regtbl_fetch(modbus_mapping_t *reg_tbl)
{
    uint16_t                val = 0xFFFF;

    if(modbus_regtbl_get_vendor_id(reg_tbl->tab_input_registers + MODBUS_REGTBL_VENDER_ID, 1) < 0) {
        return VTSS_RC_ERROR;
    }

    if(modbus_regtbl_get_modelname(reg_tbl->tab_input_registers + MODBUS_REGTBL_MODEL_NAME, 32) < 0) {
        return VTSS_RC_ERROR;
    }

    if(modbus_regtbl_get_version(reg_tbl->tab_input_registers + MODBUS_REGTBL_VERSION, 20) < 0) {
        return VTSS_RC_ERROR;
    }

    if(modbus_regtbl_get_release_date(reg_tbl->tab_input_registers + MODBUS_REGTBL_RELEASE_DATE, 5) < 0) {
        return VTSS_RC_ERROR;
    }

    if(modbus_regtbl_get_serial_num(reg_tbl->tab_input_registers + MODBUS_REGTBL_SERIAL_NUM, 32) < 0) {
        return VTSS_RC_ERROR;
    }

    if(modbus_regtbl_get_mac_addr(reg_tbl->tab_input_registers + MODBUS_REGTBL_MAC_ARR, 3) < 0) {
        return VTSS_RC_ERROR;
    }
    
    for(int i = 0; i < 64; i++) {
        memcpy(reg_tbl->tab_input_registers + MODBUS_REGTBL_PORT_STATUS + i, &val, sizeof(uint16_t));
        memcpy(reg_tbl->tab_input_registers + MODBUS_REGTBL_PORT_SPEED + i, &val, sizeof(uint16_t));
        memcpy(reg_tbl->tab_input_registers + MODBUS_REGTBL_PORT_FLOW_CTRL + i, &val, sizeof(uint16_t));
    }

    return VTSS_OK;
}

vtss_rc modbus_regtbl_sysinfo_update(modbus_mapping_t *reg_tbl)
{
    if(modbus_regtbl_get_sysname(reg_tbl->tab_input_registers + MODBUS_REGTBL_SYSTEM_NAME, 32) < 0) {
        return VTSS_RC_ERROR;
    }

    if(modbus_regtbl_get_syslocation(reg_tbl->tab_input_registers + MODBUS_REGTBL_SYSTEM_LOCATION, 32) < 0) {
        return VTSS_RC_ERROR;
    }

    if(modbus_regtbl_get_power_p1_status(reg_tbl->tab_input_registers + MODBUS_REGTBL_POWER_1_STATUS, 1) < 0) {
        return VTSS_RC_ERROR;
    }

    if(modbus_regtbl_get_power_p2_status(reg_tbl->tab_input_registers + MODBUS_REGTBL_POWER_2_STATUS, 1) < 0) {
        return VTSS_RC_ERROR;
    }

    if(modbus_regtbl_get_relay_stat(reg_tbl->tab_input_registers + MODBUS_REGTBL_RELAY_STATUS, 1) < 0) {
        return VTSS_RC_ERROR;
    }

    return VTSS_OK;
}

vtss_rc modbus_regtbl_portinfo_update(modbus_mapping_t *reg_tbl)
{
    uint16_t                *tab_input_registers = reg_tbl->tab_input_registers;
    mesa_port_no_t          iport = 0;
    port_isid_info_t        info = {0};
    vtss_appl_port_conf_t   conf = {0};
    vtss_appl_port_status_t port_status;
    mesa_port_status_t      *status = &port_status.status;
    vtss_ifindex_t          ifindex;
    meba_port_cap_t         cap = {0};
    BOOL                    rx_pause;
    BOOL                    tx_pause;
    u8                      fc = 0;
    uint16_t                link;
    uint16_t                speed;
    uint16_t                flow_control = MODBUS_PORT_FLOW_CONTROL_OFF;
    int                     port_count = MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT);
    mesa_port_speed_t       speed_show;
    mesa_bool_t             fdx_show;

    cap = (port_isid_info_get(1, &info) == VTSS_RC_OK ? info.cap : 0);
    

    for(iport = 0; iport < port_count; iport++) {
        memset(&ifindex, 0, sizeof(vtss_ifindex_t));
        memset(&conf, 0, sizeof(vtss_appl_port_conf_t));
        memset(&port_status, 0, sizeof(vtss_appl_port_status_t));

        if (vtss_ifindex_from_port(1, iport, &ifindex) != VTSS_RC_OK) {
            T_W("Could not get ifindex");
            return VTSS_RC_ERROR;
        };

        if(vtss_appl_port_conf_get(ifindex, &conf) < 0 || vtss_appl_port_status_get(ifindex, &port_status) < 0) {
            T_W("Could not get port config data");
            return VTSS_RC_ERROR;
        }

        //  Link status
        if(conf.admin.enable) {
            if(status->link) {
                link = MODBUS_PORT_STATUS_LINK_UP;
            } else {
                link = MODBUS_PORT_STATUS_LINK_DOWN;
            }
        } else {
            link = MODBUS_PORT_STATUS_DISABLE;
        }
        T_D("link: %u", link);
        memcpy(tab_input_registers + MODBUS_REGTBL_PORT_STATUS + iport, &link, sizeof(uint16_t));


        //  Speed
        if(status->link){
            speed_show=status->speed;
            fdx_show=status->fdx;
        }else{
            speed_show=conf.speed;
            fdx_show=conf.fdx;
        }
        switch(speed_show) {
            case MESA_SPEED_10M:
                speed = (fdx_show) ? MODBUS_PORT_SPEED_10M_FULL : MODBUS_PORT_SPEED_10M_HALF;
                break;
            case MESA_SPEED_100M:
                speed = (fdx_show) ? MODBUS_PORT_SPEED_100M_FULL : MODBUS_PORT_SPEED_100M_HALF;
                break;
            case MESA_SPEED_1G:
                speed = MODBUS_PORT_SPEED_1G;
                break;
            case MESA_SPEED_10G:
                speed = MODBUS_PORT_SPEED_10G;
                break;
            default:
                speed = 0xFFFF;
                T_D("Should not be here! port: %d, speed: %d", iport, status->speed);
                break;
        }

        T_D("speed: %u", speed);
        memcpy(tab_input_registers + MODBUS_REGTBL_PORT_SPEED + iport, &speed, sizeof(uint16_t));

        //  Flow control
        rx_pause = ((conf.speed==MESA_SPEED_AUTO) ? (status->link ? status->aneg.obey_pause : 0) : conf.flow_control);
        tx_pause = ((conf.speed==MESA_SPEED_AUTO) ? (status->link ? status->aneg.generate_pause : 0) : conf.flow_control);

        if((fc = ((cap & MEBA_PORT_CAP_FLOW_CTRL) ? ((port_status.cap & MEBA_PORT_CAP_FLOW_CTRL) ? 2 : 1) : 0))) {
            if(fc == 2 && conf.flow_control) {
                if(tx_pause && rx_pause) {
                    flow_control = MODBUS_PORT_FLOW_CONTROL_BOTH;
                } else {
                    if(tx_pause) {
                        flow_control = MODBUS_PORT_FLOW_CONTROL_TX;
                    }else if(rx_pause) {
                        flow_control = MODBUS_PORT_FLOW_CONTROL_RX;
                    }else{
                        flow_control = MODBUS_PORT_FLOW_CONTROL_OFF;
                    }
                }
            } else {
                flow_control = MODBUS_PORT_FLOW_CONTROL_OFF;
            }
        } else {
            flow_control = MODBUS_PORT_FLOW_CONTROL_NO_PORT;
        }

        T_D("flow_control: %u", flow_control);
        memcpy(tab_input_registers + MODBUS_REGTBL_PORT_FLOW_CTRL + iport, &flow_control, sizeof(uint16_t));
    }

    return VTSS_OK;
}

vtss_rc modbus_regtbl_pktinfo_update(modbus_mapping_t *reg_tbl)
{
    uint16_t                *tab_input_registers = reg_tbl->tab_input_registers;
    mesa_port_no_t          iport = 0;
    vtss_ifindex_t          ifindex;
    mesa_port_counters_t    counters;
    mesa_port_counter_t     tx_packets = 0;
    mesa_port_counter_t     rx_packets = 0;
    mesa_port_counter_t     tx_error_packets = 0;
    mesa_port_counter_t     rx_error_packets = 0;
    int                     port_count = MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT);

    for(iport = 0; iport < port_count; iport++) {
        if (vtss_ifindex_from_port(0, iport, &ifindex) != VTSS_RC_OK) {
            T_W("Could not get ifindex");
            return VTSS_RC_ERROR;
        }
        if (vtss_appl_port_counters_get(ifindex, &counters) != VTSS_OK) {
            T_W("Could not get port counters!");
            return VTSS_RC_ERROR;
        }

        tx_packets = (counters.rmon.tx_etherStatsPkts);
        rx_packets = (counters.rmon.rx_etherStatsPkts);
        tx_error_packets = (counters.if_group.ifOutErrors);
        rx_error_packets = (counters.if_group.ifInErrors);

        ul2reg(tab_input_registers + MODBUS_REGTBL_TX_PACKTES + (iport * 4), tx_packets);
        ul2reg(tab_input_registers + MODBUS_REGTBL_RX_PACKETS + (iport * 4), rx_packets);
        ul2reg(tab_input_registers + MODBUS_REGTBL_TX_ERR_PACKET + (iport * 4), tx_error_packets);
        ul2reg(tab_input_registers + MODBUS_REGTBL_RX_ERR_PACKET + (iport * 4), rx_error_packets);
    }

    return VTSS_OK;
}

