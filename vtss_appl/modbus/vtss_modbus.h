/***********************************************************
 *
 * Copyright (c) 2018 LEONTON Technologies, Co.Ltd All Rights Reserved.
 *
 * Modbus Module
 *
 ***********************************************************/

/* ================================================================= *
 * Trace definitions
 * ================================================================= */
#include "vtss_module_id.h"
#include "vtss_trace_lvl_api.h"
#include "vtss_trace_api.h"
#include <modbus.h>


#define VTSS_TRACE_MODULE_ID	VTSS_MODULE_ID_MODBUS
#define VTSS_TRACE_GRP_DEFAULT	0
#define VTSS_TRACE_GRP_CRIT		1
#define VTSS_TRACE_GRP_ICLI		2
#define VTSS_TRACE_GRP_CNT		3

#define MODBUS_DEFAULT_BUFF_LEN             (128)
#define MODBUS_TCP_SOCKET_ALLOW_NUM         (8)
#define MODBUS_TCP_SOCKET_TIMEOUT           (3000)
#define MODBUS_CLN_CONECTION_TIMEOUT        1800 //second

#define MODBUS_TCP_GET_ADU_FUNC(x)          (x[7])
#define MODBUS_TCP_GET_ADU_ADDR(x)          (MODBUS_GET_INT16_FROM_INT8((x), 8))
#define MODBUS_TCP_GET_ADU_NUM(x)           (MODBUS_GET_INT16_FROM_INT8((x), 10))


#define MODBUS_CLN_FLAG_NONE                0x00
#define MODBUS_CLN_FLAG_READABLED           0x01

#define MODBUS_SRV_FLAG_NONE                0x00
#define MODBUS_SRV_FLAG_ACCEPT              0x01

#define MODBUS_MGMT_DISABLED                0
#define MODBUS_MGMT_ENABLED                 1
#define MODBUS_MGMT_MODE_DEFAULT            MODBUS_MGMT_DISABLED

/* Modbus function codes */
#define MODBUS_FUNC_READ_COILS                0x01
#define MODBUS_FUNC_READ_DISCRETE_INPUTS      0x02
#define MODBUS_FUNC_READ_HOLDING_REGISTERS    0x03
#define MODBUS_FUNC_READ_INPUT_REGISTERS      0x04
#define MODBUS_FUNC_WRITE_SINGLE_COIL         0x05
#define MODBUS_FUNC_WRITE_SINGLE_REGISTER     0x06
#define MODBUS_FUNC_READ_EXCEPTION_STATUS     0x07
#define MODBUS_FUNC_WRITE_MULTIPLE_COILS      0x0F
#define MODBUS_FUNC_WRITE_MULTIPLE_REGISTERS  0x10
#define MODBUS_FUNC_REPORT_SLAVE_ID           0x11
#define MODBUS_FUNC_MASK_WRITE_REGISTER       0x16
#define MODBUS_FUNC_WRITE_AND_READ_REGISTERS  0x17


typedef struct {
    int              fd;
    unsigned char    flags;
} modbus_srv_t;

typedef struct {
    int              fd;
    unsigned char    flags;
} modbus_cln_t;

/* Structure for global variables */
typedef struct {
    /*modbus conf*/
	int mode; //MODBUS global mode

    /*  Feature variables   */
    modbus_mapping_t *reg_tbl;
    modbus_srv_t     *srv;
    modbus_t         *ctx;
    uint8_t          *adu;
    modbus_cln_t     *cln;
} modbus_global_t;

/*  Register table relate API   */
vtss_rc modbus_regtbl_fetch(modbus_mapping_t *reg_tbl);
vtss_rc modbus_regtbl_sysinfo_update(modbus_mapping_t *reg_tbl);
vtss_rc modbus_regtbl_portinfo_update(modbus_mapping_t *reg_tbl);
vtss_rc modbus_regtbl_pktinfo_update(modbus_mapping_t *reg_tbl);


