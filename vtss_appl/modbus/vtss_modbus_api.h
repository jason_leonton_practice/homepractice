/***********************************************************
 *
 * Copyright (c) 2018 LEONTON Technologies, Co.Ltd All Rights Reserved.
 *
 * Modbus Module
 *
 ***********************************************************/

#ifndef _MODBUS_API_H_
#define _MODBUS_API_H_

/*  Modbus Server State */
typedef enum {
    MODBUS_SRV_DISABLED = 0,
    MODBUS_SRV_ENABLED
} modbus_srv_state_t;

vtss_rc modbus_init(vtss_init_data_t *data);
mesa_rc modbus_mgmt_conf_get(int * mode);
mesa_rc modbus_mgmt_conf_set(int mode);
vtss_rc modbus_srv_state_get(modbus_srv_state_t *state);


#endif  /* _MODBUS_API_H_ */

