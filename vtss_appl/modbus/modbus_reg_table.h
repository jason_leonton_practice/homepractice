/***********************************************************
 *
 * Copyright (c) 2018 LEONTON Technologies, Co.Ltd All Rights Reserved.
 *
 * Modbus Module
 *
 ***********************************************************/

#ifndef _MODBUS_REG_TABLE_H_
#define _MODBUS_REG_TABLE_H_

#define MODBUS_REGTBL_GET_TYPE_FROM_ADDR(addr)  (((addr) & 0xF000) >> 12)
#define MODBUS_REGTBL_GET_BYTE_FROM_WORD(num)   ((num) * sizeof(uint16_t))

typedef enum {
    MODBUS_REGTBL_TYPE_SYSINFO = 0,
    MODBUS_REGTBL_TYPE_PORTINFO,
    MODBUS_REGTBL_TYPE_PACKETINFO,
} modbus_regtbl_type_t;

typedef enum {
    MODBUS_POWER_STATUS_ON = 0,
    MODBUS_POWER_STATUS_OFF
} modbus_power_status_t;

typedef enum {
    MODBUS_RELAY_STATUS_OPEN = 0,
    MODBUS_RELAY_STATUS_CLOSE
} modbus_relay_status_t;

typedef enum {
    MODBUS_PORT_STATUS_LINK_DOWN = 0,
    MODBUS_PORT_STATUS_LINK_UP,
    MODBUS_PORT_STATUS_DISABLE,
    MODBUS_PORT_STATUS_NO_PORT = 0xFFFF
} modbus_port_status_t;

typedef enum {
    MODBUS_PORT_SPEED_10M_HALF = 0,
    MODBUS_PORT_SPEED_10M_FULL,
    MODBUS_PORT_SPEED_100M_HALF,
    MODBUS_PORT_SPEED_100M_FULL,
    MODBUS_PORT_SPEED_1G,
    MODBUS_PORT_SPEED_10G,
    MODBUS_PORT_SPEED_NO_PORT = 0xFFFF
} modbus_port_speed_t;

typedef enum {
    MODBUS_PORT_FLOW_CONTROL_OFF = 0,
    MODBUS_PORT_FLOW_CONTROL_TX,
    MODBUS_PORT_FLOW_CONTROL_RX,
    MODBUS_PORT_FLOW_CONTROL_BOTH,
    MODBUS_PORT_FLOW_CONTROL_NO_PORT = 0xFFFF
} modbus_port_flow_control_t;

/*  System Infomation   */
#define MODBUS_REGTBL_VENDER_ID             (0x0)      // Size: 1
#define MODBUS_REGTBL_MODEL_NAME            (0x1)      // Size: 32
#define MODBUS_REGTBL_SYSTEM_NAME           (0x21)     // Size: 32
#define MODBUS_REGTBL_SYSTEM_LOCATION       (0x41)     // Size: 32
#define MODBUS_REGTBL_VERSION               (0x61)     // Size: 20
#define MODBUS_REGTBL_RELEASE_DATE          (0x75)     // Size: 5
#define MODBUS_REGTBL_SERIAL_NUM            (0x7A)     // Size: 32
#define MODBUS_REGTBL_MAC_ARR               (0x9A)     // Size: 3
#define MODBUS_REGTBL_POWER_1_STATUS        (0x9D)     // Size: 1
#define MODBUS_REGTBL_POWER_2_STATUS        (0x9E)     // Size: 1
#define MODBUS_REGTBL_RELAY_STATUS          (0x9F)     // Size: 1

#define MODBUS_REGTBL_SYSINFO_LEN           (160)      // Words

/*  Port Infomation */
#define MODBUS_REGTBL_PORT_STATUS           (0x1000)   // Size: 64
#define MODBUS_REGTBL_PORT_SPEED            (0x1100)   // Size: 64
#define MODBUS_REGTBL_PORT_FLOW_CTRL        (0x1200)   // Size: 64

#define MODBUS_REGTBL_PORTINFO_LEN          (192)      //  Words

/*  Packets Infomation  */
#define MODBUS_REGTBL_TX_PACKTES            (0x2000)   // Size: 256
#define MODBUS_REGTBL_RX_PACKETS            (0x2200)   // Size: 256
#define MODBUS_REGTBL_TX_ERR_PACKET         (0x2400)   // Size: 256
#define MODBUS_REGTBL_RX_ERR_PACKET         (0x2600)   // Size: 256

#define MODBUS_REGTBL_PACKETINFO_LEN        (1024)      //  Words

#define MODBUS_REGTBL_MAX_REGISTER_NUM      (0x2700)

#endif /*   _MODBUS_REG_TABLE_H_   */



