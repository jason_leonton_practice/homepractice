#!/usr/bin/env ruby

require 'pp'
require 'open3'
require 'logger'
require 'optparse'
require 'fileutils'
require_relative 'resultnode.rb'

$arch_conf = {
    'mips' => { :tc => "2017.02-041", :prefix => "mipsel" },
    'x86'  => {  }
}

$src_conf = {
    "c-ares" => { :version => "1.12.0",
                  :file => "c-ares-1.12.0.tar.gz",
                  :url  => "https://c-ares.haxx.se/download/c-ares-1.12.0.tar.gz",
                  :md5  => "2ca44be1715cd2c5666a165d35788424"
                },

    "frr"    => { :version => "3.0",
                  :file => "frr-3.0.tar.gz",
                  :url  => "https://github.com/FRRouting/frr/archive/frr-3.0.tar.gz",
                  :md5  => "3e99a7b3b88a374c5683c2eb21efa214",
                  :patches => ["frr-3.0-enable-vty-socket-without-libreadline.patch",
                               "frr-3.0-fix-retransmit-intervals.patch",
                               "frr-3.0-fix-virtual-link-authentication.patch",
                               "frr-3.0-fix-neighbor-status.patch"]
                }
}

$base = "#{File.expand_path(File.dirname(File.dirname(__FILE__)))}"
$dl = "#{$base}/src"

$l = Logger.new(STDOUT)
$l.level = Logger::INFO
log_fmt = proc do |severity, datetime, progname, msg|
    "#{severity} [#{Time.now.strftime('%H:%M:%S')}] #{Thread.current.object_id.to_s}: #{msg}\n"
end
$l.formatter = log_fmt

def run(cmd)
  $l.info "Running '#{cmd}' ..."
  o=%x(#{cmd})
  if $? != 0
    $l.fatal "Running '#{cmd}' failed"
    puts o if(o)
    raise "Running '#{cmd}' failed"
  end
  return o.chomp
end

def sys(cmd)
  $l.info "Running '#{cmd}' ..."
  system "sh -c \"#{cmd}\""
  if $? != 0
    $l.fatal "Running '#{cmd}' failed"
    raise "Running '#{cmd}' failed"
  end
end

def md5chk f, md5
    return false if not File.exist?(f)
    return run("md5sum #{f}").split[0] == md5
end

$arch_conf.each do |k, v|
    run "sudo /usr/local/bin/mscc-install-pkg -t bsp/#{v[:tc]} mscc-brsdk-#{k}-#{v[:tc]};" if v[:tc]
end

run "mkdir -p #{$dl}"
$src_conf.each do |k, v|
    if not md5chk "#{$dl}/#{v[:file]}", v[:md5]
        sys "wget -O #{$dl}/#{v[:file]} #{v[:url]}"
        if not md5chk "#{$dl}/#{v[:file]}", v[:md5]
            raise "MD5 sum of #{k} did not match!"
        end
    end
end

def def_env arch, install_root
    return {} if arch == "x86"
    tc_base = "/opt/mscc/mscc-brsdk-#{arch}-#{$arch_conf[arch][:tc]}/stage2/smb/x86_64-linux"
    tc_sr = "#{tc_base}/usr/#{$arch_conf[arch][:prefix]}-buildroot-linux-gnu/sysroot"
    return {
        "PATH" =>             "/usr/bin:/bin:/usr/local/bin:/usr/local/sbin:#{tc_base}/usr/bin",
        "LD" =>               "#{$arch_conf[arch][:prefix]}-linux-ld",
        "CC" =>               "#{$arch_conf[arch][:prefix]}-linux-gcc",
        "GCC" =>              "#{$arch_conf[arch][:prefix]}-linux-gcc",
        "STRIP" =>            "#{$arch_conf[arch][:prefix]}-linux-strip",
        "CFLAGS" =>           "-Os",
        "CPPFLAGS" =>         "-I#{install_root}/usr/include/",
        "LDFLAGS" =>          "-L#{install_root}/usr/lib/",
        "PKG_CONFIG_PATH" =>  "#{install_root}/usr/lib/pkgconfig",
    }
end

def env2str env
    env.to_a.map{|x| "#{x[0]}=#{x[1]}"}.join(" ")
end

def build_c_ares o, arch
    pre = $arch_conf[arch][:prefix]
    root = "#{o}/#{arch}/root"
    run "rm -rf  #{o}/#{arch}/c-ares"
    run "mkdir -p #{o}/#{arch}/c-ares"
    run "mkdir -p #{o}/#{arch}/license/c-ares"
    Dir.chdir("#{o}/#{arch}/c-ares") do
        env = def_env arch, "#{o}/#{arch}/root"
        run "tar --strip-components=1 -xf #{$dl}/#{$src_conf["c-ares"][:file]}"
        run "cp LICENSE.md #{o}/#{arch}/license/c-ares"
        envs = env2str(env)
        c = " ./configure"
        c += " --target=#{pre}-linux"
        c += " --host=#{pre}-linux"
        c += " --build=x86_64-unknown-linux-gnu"
        c += " --prefix=#{root}/usr"
        c += " --sysconfdir=#{root}/etc"
        sys "pwd"
        sys "#{envs} #{c}"
        sys "#{envs} make -j12"
        sys "#{envs} make install"
    end
end

def build_frr o, arch, flags
    pre = $arch_conf[arch][:prefix]
    root = "#{o}/#{arch}/root"
    run "rm -rf  #{o}/#{arch}/frr"
    run "mkdir -p #{o}/#{arch}/frr"
    run "mkdir -p #{o}/#{arch}/license/frr"
    Dir.chdir("#{o}/#{arch}/frr") do
        env = def_env arch, "#{o}/#{arch}/root"
        run "tar --strip-components=1 -xf #{$dl}/#{$src_conf["frr"][:file]}"
        $src_conf["frr"][:patches].each do |p|
            run "patch -p1 < #{$base}/#{p}"
        end
        run "cp COPYING #{o}/#{arch}/license/frr"
        envs = env2str(env)
        sys "autoreconf -i"
        c = " ./configure"
        c += " --target=#{pre}-linux"
        c += " --host=#{pre}-linux"
        c += " --build=x86_64-unknown-linux-gnu"
        c += " --prefix=/usr"
        c += " --sysconfdir=/etc"
        c += " --localstatedir=/tmp"
        c += " --sharedstatedir=/tmp"
        c += " --enable-user=root"
        c += " --enable-group=root"
        c += " --program-prefix=\"\""
        c += " --disable-libtool-lock"
        c += " --disable-vtysh"
        c += " --disable-doc"

        if flags.include? :zebra
            c += " --enable-zebra"
        else
            c += " --disable-zebra"
        end

        c += " --disable-bgpd"
        c += " --disable-ripd"
        c += " --disable-ripngd"

        if flags.include? :ospf
            c += " --enable-ospfd"
        else
            c += " --disable-ospfd"
        end

        c += " --disable-ospf6d"
        c += " --disable-nhrpd"
        c += " --disable-watchquagga"
        c += " --disable-isisd"
        c += " --disable-pimd"
        c += " --disable-ldpd"
        c += " --disable-bgp-announce"
        c += " --disable-tcp-zebra"
        c += " --disable-ospfapi"
        c += " --disable-ospfclient"
        c += " --disable-watchfrr"


        sys "#{envs} #{c}"
        sys "#{envs} make -j12"
        sys "#{envs} make DESTDIR=#{root} install"
    end

    run "mkdir -p #{root}/etc/quagga"
    File.open("#{root}/etc/quagga/zebra.conf", 'w') do |f|
        f.puts("hostname Router")
        f.puts("password zebra")
        f.puts("enable password zebra")
        f.puts("interface lo")
        f.puts("interface sit0")
        f.puts("log file /tmp/zebra.log")
    end

    File.open("#{root}/etc/quagga/ospfd.conf", 'w') do |f|
        f.puts("hostname ospfd")
        f.puts("password zebra")
        f.puts("log file /tmp/ospfd.log")
    end

    File.open("#{root}/etc/quagga/daemons", 'w') do |f|
        f.puts("zebra=yes")
        f.puts("ospfd=yes")
        f.puts("bgpd=no")
        f.puts("ospf6d=no")
        f.puts("ripd=no")
        f.puts("ripngd=no")
        f.puts("isisd=no")
    end
end

def clean_up o, arch
    root = "#{o}/#{arch}/root"
    run "rm -rf #{root}/usr/share"
    run "rm -rf #{root}/usr/include"
    run "rm -rf #{root}/usr/lib/*.a"
    run "rm -rf #{root}/usr/lib/*.la"
    run "rm -rf #{root}/etc/*.sample"
    run "rm -rf #{root}/usr/lib/pkgconfig"
    run "rm -rf #{root}/usr/sbin/frr-reload.py"
    run "rm -rf #{root}/usr/sbin/frr"
    run "rm -rf #{root}/usr/sbin/ssd"
    run "rm -rf #{root}/usr/sbin/rfptest"
    run "rm -rf #{root}/usr/bin/permutations"
end

def build o, arch, flags
    root = "#{o}/#{arch}/root"
    run "rm -rf #{o}/#{arch}"
    run "mkdir -p #{o}/#{arch}"
    build_c_ares o, arch
    build_frr o, arch, flags
    clean_up o, arch

    Dir.chdir("#{root}") do
        run "tar -cvzf ../root.tar *"
        run "mksquashfs * ../root.squashfs -comp xz -all-root"
    end

    run "rm -rf #{root}"
    run "rm -rf #{o}/#{arch}/frr"
    run "rm -rf #{o}/#{arch}/c-ares"
end

run "rm -f status.json"
run "rm -rf ./build"
build "#{$base}/build/zebra_only", "mips", [:zebra]
build "#{$base}/build/all",        "mips", [:zebra, :ospf]

$git_sha = %x(git rev-parse --short HEAD).chop
$git_sha_long = %x(git rev-parse HEAD).chop
begin
  $git_id = run "git describe --tags --long"
rescue
  $git_id = $git_sha
end
if ENV['BRANCH_NAME']
  $git_branch = ENV['BRANCH_NAME']
else
  $git_branch = %x(git symbolic-ref --short -q HEAD).chop
end

$ver = "#{$src_conf["frr"][:version]}-#{$git_id}@#{$git_branch}"
$out_name = "frr-#{$ver}"

run "rm -rf *.tar.gz"
run "tar --transform \"s/^\./#{$out_name}/\" -cvzf #{$out_name}.tar.gz ./* ./.module_info"

$topRes = ResultNode.new('build', "OK",
                         { "version" => $ver,
                           "c-ares-version" => $src_conf["c-ares"][:version],
                           "frr-version" => $src_conf["frr"][:version]})

$topRes.reCalc
$topRes.to_file("status.json")
puts "Status: #{$topRes.status}"
exit 0 if $topRes.status == "OK"
$topRes.dump
exit -1

