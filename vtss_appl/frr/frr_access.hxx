/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _FRR_ACCESS_H_
#define _FRR_ACCESS_H_

#include <unistd.h>
#ifdef VTSS_BASICS_STANDALONE
#include <vtss/basics/api_types.h>
#else
#include <vtss/appl/interface.h>
#include <vtss/appl/types.h>
#endif
#include <string>
#include <vtss/basics/map.hxx>
#include <vtss/basics/optional.hxx>
#include <vtss/basics/string.hxx>
#include <vtss/basics/time.hxx>
#include <vtss/basics/vector.hxx>

namespace vtss {

template <typename Type>
struct FrrRes {
    // explicit not explicit, because we want the conversion
    FrrRes(Type &t) : rc{MESA_RC_OK}, val{t} {}
    FrrRes(Type &&t) : rc{MESA_RC_OK}, val{std::move(t)} {}
    FrrRes(mesa_rc r) : rc{r} {}

    explicit operator bool() { return rc == MESA_RC_OK; }
    Type *operator->() { return &val; }

    mesa_rc rc;
    Type val;
};

void initialize_critd();
mesa_rc zebra_start();
mesa_rc ospfd_start(bool clean);
mesa_rc ospfd_stop();
mesa_rc frr_stop_all();
bool frr_has_zebra();
bool frr_has_ospfd();
bool is_frr_ospfd_started();


// - Sx "ip route <network> <dest>" - 4d PGS-34
struct FrrIpRoute {
    mesa_routing_entry_t net;
    uint8_t distance;
    uint8_t tag;
};

struct FrrConf {
    std::string zebra;
    std::string ospfd;
};

Vector<FrrIpRoute> frr_ip_route_conf_get(FrrConf &conf);
mesa_rc frr_ip_route_conf_set(const FrrIpRoute &r);
mesa_rc frr_ip_route_conf_del(const FrrIpRoute &r);

// - Sx "show ip route [json]" - 3d PGS-26
struct FrrRouteHopStatus {
    bool fib;
    mesa_ipv4_t ip;
    bool directly_connected;
    vtss_ifindex_t interface_index;
    uint32_t os_ifindex;
    bool active;
    bool selected;
};

enum FrrRouteProtocol_t {
    Route_Kernel = 1,
    Route_Connected = 2,
    Route_Static = 3,
    Route_Ospf = 6,
};

struct FrrRouteStatus {
    mesa_ipv4_network_t prefix;
    FrrRouteProtocol_t protocol;
    bool selected;
    uint32_t distance;
    uint32_t metric;
    seconds up_time;
    Vector<FrrRouteHopStatus> next_hops;
};

struct FrrIpRouteStatus {
    mesa_ipv4_network_t net;
    Vector<FrrRouteStatus> routes;
};
FrrRes<Vector<FrrIpRouteStatus>> frr_ip_route_status_get();

// - Sx "show ip ospf [json]" - 3d PGS-27
enum FrrIpOspfAreaAuthentication {
    Authentication_None = 0,
    Authentication_SimplePassword = 1,
    Authentication_MessageDigest = 2
};

struct FrrIpOspfArea {
    bool backbone;
    bool stub_no_summary;
    bool stub_shortcut;
    int32_t area_if_total_counter;
    int32_t area_if_activ_counter;
    int32_t full_adjancet_counter;
    FrrIpOspfAreaAuthentication authentication;
    int32_t spf_executed_counter;
    int32_t lsa_nr;
    int32_t lsa_router_nr;
    int32_t lsa_router_checksum;
    int32_t lsa_network_nr;
    int32_t lsa_network_checksum;
    int32_t lsa_summary_nr;
    int32_t lsa_summary_checksum;
    int32_t lsa_asbr_nr;
    int32_t lsa_asbr_checksum;
    int32_t lsa_nssa_nr;
    int32_t lsa_nssa_checksum;
    int32_t lsa_opaque_link_nr;
    int32_t lsa_opaque_link_checksum;
    int32_t lsa_opaque_area_nr;
    int32_t lsa_opaque_area_checksum;
};

struct FrrIpOspfStatus {
    mesa_ipv4_t router_id;
    bool tos_routes_only;
    bool rfc2328;
    milliseconds spf_schedule_delay;
    milliseconds hold_time_min;
    milliseconds hold_time_max;
    int32_t hold_time_multiplier;
    milliseconds spf_last_executed;
    milliseconds spf_last_duration;
    milliseconds lsa_min_interval;
    milliseconds lsa_min_arrival;
    int32_t write_multiplier;
    milliseconds refresh_timer;
    int32_t lsa_external_counter;
    int32_t lsa_external_checksum;
    int32_t lsa_asopaque_counter;
    int32_t lsa_asopaque_checksums;
    int32_t attached_area_counter;
    Map<mesa_ipv4_t, FrrIpOspfArea> areas;
};
FrrRes<FrrIpOspfStatus> frr_ip_ospf_status_get();

// - Sx "show ip ospf interface [INTERFACE] [json]" - 3d PGS-28
enum AreaDescriptionType {
    AreaDescriptionType_Default,
    AreaDescriptionType_Stub,
    AreaDescriptionType_Nssa
};

struct AreaDescription {
    AreaDescription() = default;
    AreaDescription(mesa_ipv4_t a, AreaDescriptionType t) : area{a}, type{t} {}

    mesa_ipv4_t area;
    AreaDescriptionType type;
};

enum FrrIpOspfIfNetworkType {
    NetworkType_Null = 0,
    NetworkType_PointToPoint = 1,
    NetworkType_Broadcast = 2,
    NetworkType_Nbma = 3,
    NetworkType_PointToMultipoint = 4,
    NetworkType_VirtualLink = 5,
    NetworkType_LoopBack = 6
};

enum FrrIpOspfIfISMState {
    ISM_DependUpon = 0,
    ISM_Down = 1,
    ISM_Loopback = 2,
    ISM_Waiting = 3,
    ISM_PointToPoint = 4,
    ISM_DROther = 5,
    ISM_Backup = 6,
    ISM_DR = 7
};

enum FrrIpOspfIfType { IfType_Peer = 0, IfType_Broadcast = 1 };

struct FrrIpOspfIfStatus {
    bool if_up;
    int32_t os_ifindex;
    vtss_ifindex_t ifindex;
    int32_t mtu_bytes;
    int32_t bandwidth_mbit;
    std::string if_flags;
    bool ospf_enabled;
    mesa_ipv4_network_t net;
    FrrIpOspfIfType if_type;
    mesa_ipv4_t local_if_used;
    AreaDescription area;
    mesa_ipv4_t router_id;
    FrrIpOspfIfNetworkType network_type;
    uint32_t cost;
    milliseconds transmit_delay;
    FrrIpOspfIfISMState state;
    int32_t priority;
    mesa_ipv4_t bdr_id;
    mesa_ipv4_t bdr_address;
    int32_t network_lsa_sequence;
    bool mcast_member_ospf_all_routers;
    bool mcast_member_ospf_designated_routers;
    milliseconds timer;
    milliseconds timer_dead;
    milliseconds timer_wait;
    milliseconds timer_retransmit;
    milliseconds timer_hello;
    bool timer_passive_iface;
    int32_t nbr_count;
    int32_t nbr_adjacent_count;
    mesa_ipv4_t vlink_peer_addr;
};
FrrRes<Map<vtss_ifindex_t, FrrIpOspfIfStatus>> frr_ip_ospf_interface_status_get();

// - Sx "show ip ospf neighbor [json]" - 3d PGS-29 - conf
enum FrrIpOspfNeighborNSMState {
    NSM_DependUpon = 0,
    NSM_Deleted = 1,
    NSM_Down = 2,
    NSM_Attempt = 3,
    NSM_Init = 4,
    NSM_TwoWay = 5,
    NSM_ExStart = 6,
    NSM_Exchange = 7,
    NSM_Loading = 8,
    NSM_Full = 9
};

struct FrrIpOspfNeighborStatus {
    mesa_ipv4_t if_address;
    AreaDescription area;
    AreaDescription transit_id;
    int32_t os_ifindex;
    vtss_ifindex_t ifindex;
    int32_t nbr_priority;
    FrrIpOspfNeighborNSMState nbr_state;
    mesa_ipv4_t router_designated_id;
    mesa_ipv4_t router_designated_bck_id;
    std::string options_list;
    int32_t options_counter;
    milliseconds router_dead_interval_timer_due;
};
// the key is the neighbor router id
FrrRes<Map<mesa_ipv4_t, Vector<FrrIpOspfNeighborStatus>>>
frr_ip_ospf_neighbor_status_get();

FrrRes<FrrConf> frr_running_config_get();
mesa_rc frr_ospfd_reload();

// OSPF Router configuration ---------------------------------------------------
typedef uint32_t OspfInst;  // TODO, apply int-tag

enum AbrType { AbrType_Cisco, AbrType_IBM, AbrType_Shortcut, AbrType_Standard };
struct FrrOspfRouterConf {
    vtss::Optional<mesa_ipv4_t> ospf_router_id;
    vtss::Optional<AbrType> ospf_router_abr_type;
    vtss::Optional<bool> ospf_router_rfc1583;
};

FrrRes<FrrOspfRouterConf> frr_ospf_router_conf_get(FrrConf &conf,
                                                   const OspfInst &id);
mesa_rc frr_ospf_router_conf_set(const OspfInst &id);
mesa_rc frr_ospf_router_conf_set(const OspfInst &id,
                                 const FrrOspfRouterConf &conf);
mesa_rc frr_ospf_router_conf_del(const OspfInst &id);

mesa_rc frr_ospf_router_passive_if_default_set(const OspfInst &id, bool enable);
FrrRes<bool> frr_ospf_router_passive_if_default_get(FrrConf &conf,
                                                    const OspfInst &id);
mesa_rc frr_ospf_router_passive_if_conf_set(const OspfInst &id,
                                            const vtss_ifindex_t &i,
                                            bool passive);
FrrRes<bool> frr_ospf_router_passive_if_conf_get(FrrConf &conf,
                                                 const OspfInst &id,
                                                 const vtss_ifindex_t &i);

FrrRes<Optional<uint32_t>> frr_ospf_router_default_metric_conf_get(
        FrrConf &conf, const OspfInst &id);
mesa_rc frr_ospf_router_default_metric_conf_set(const OspfInst &id, uint32_t val);
mesa_rc frr_ospf_router_default_metric_conf_del(const OspfInst &id);

enum FrrOspfRouterRedistributeProtocol {
    Protocol_Kernel,
    Protocol_Connected,
    Protocol_Static,
    Protocol_Rip,
    Protocol_Bgp
};

enum FrrOspfRouterMetricType { MetricType_One = 1, MetricType_Two };

struct FrrOspfRouterRedistribute {
    FrrOspfRouterRedistributeProtocol protocol;
    FrrOspfRouterMetricType metric_type = MetricType_Two;
    vtss::Optional<uint32_t> metric;
    vtss::Optional<std::string> route_map;
};

Vector<FrrOspfRouterRedistribute> frr_ospf_router_redistribute_conf_get(
        FrrConf &conf, const OspfInst &id);
mesa_rc frr_ospf_router_redistribute_conf_set(const OspfInst &id,
                                              const FrrOspfRouterRedistribute &val);
mesa_rc frr_ospf_router_redistribute_conf_del(
        const OspfInst &id, FrrOspfRouterRedistributeProtocol val);

struct FrrOspfRouterDefaultInformation {
    bool always;
    vtss::Optional<uint32_t> metric;
    FrrOspfRouterMetricType metric_type = MetricType_Two;
    vtss::Optional<std::string> route_map;
};

FrrRes<FrrOspfRouterDefaultInformation>
frr_ospf_router_default_information_conf_get(FrrConf &conf, const OspfInst &id);
mesa_rc frr_ospf_router_default_information_conf_set(
        const OspfInst &id, const FrrOspfRouterDefaultInformation &val);
mesa_rc frr_ospf_router_default_information_conf_del(const OspfInst &id);

// OSPF Area configuration -----------------------------------------------------
struct FrrOspfAreaNetwork {
    FrrOspfAreaNetwork() = default;
    FrrOspfAreaNetwork(const mesa_ipv4_network_t &n, const mesa_ipv4_t &a)
        : net{n}, area{a} {}
    mesa_ipv4_network_t net;
    mesa_ipv4_t area;
};

Vector<FrrOspfAreaNetwork> frr_ospf_area_network_conf_get(FrrConf &config,
                                                          const OspfInst &id);
mesa_rc frr_ospf_area_network_conf_set(const OspfInst &id,
                                       const FrrOspfAreaNetwork &val);
mesa_rc frr_ospf_area_network_conf_del(const OspfInst &id,
                                       const FrrOspfAreaNetwork &net);

Vector<FrrOspfAreaNetwork> frr_ospf_area_range_conf_get(FrrConf &config,
                                                        const OspfInst &id);
mesa_rc frr_ospf_area_range_conf_set(const OspfInst &id,
                                     const FrrOspfAreaNetwork &val);
mesa_rc frr_ospf_area_range_conf_del(const OspfInst &id,
                                     const FrrOspfAreaNetwork &val);

Vector<FrrOspfAreaNetwork> frr_ospf_area_range_not_advertise_conf_get(
        FrrConf &config, const OspfInst &id);
mesa_rc frr_ospf_area_range_not_advertise_conf_set(const OspfInst &id,
                                                   const FrrOspfAreaNetwork &val);
mesa_rc frr_ospf_area_range_not_advertise_conf_del(const OspfInst &id,
                                                   const FrrOspfAreaNetwork &val);

struct FrrOspfAreaNetworkCost {
    FrrOspfAreaNetworkCost() = default;
    FrrOspfAreaNetworkCost(const mesa_ipv4_network_t &n, const mesa_ipv4_t &a,
                           uint32_t c)
        : net{n}, area{a}, cost{c} {}
    mesa_ipv4_network_t net;
    mesa_ipv4_t area;
    uint32_t cost;
};

Vector<FrrOspfAreaNetworkCost> frr_ospf_area_range_cost_conf_get(
        FrrConf &config, const OspfInst &id);
mesa_rc frr_ospf_area_range_cost_conf_set(const OspfInst &id,
                                          const FrrOspfAreaNetworkCost &val);
mesa_rc frr_ospf_area_range_cost_conf_del(const OspfInst &id,
                                          const FrrOspfAreaNetworkCost &val);

struct FrrOspfAreaVirtualLink {
    FrrOspfAreaVirtualLink() = default;
    FrrOspfAreaVirtualLink(const mesa_ipv4_t &a, const mesa_ipv4_t &dst_ip)
        : area{a}, dst{dst_ip} {}
    mesa_ipv4_t area;
    mesa_ipv4_t dst;
    vtss::Optional<uint16_t> hello_interval;
    vtss::Optional<uint16_t> retransmit_interval;
    vtss::Optional<uint16_t> transmit_delay;
    vtss::Optional<uint16_t> dead_interval;
};

Vector<FrrOspfAreaVirtualLink> frr_ospf_area_virtual_link_conf_get(
        FrrConf &config, const OspfInst &id);
mesa_rc frr_ospf_area_virtual_link_conf_set(const OspfInst &id,
                                            const FrrOspfAreaVirtualLink &val);
mesa_rc frr_ospf_area_virtual_link_conf_del(const OspfInst &id,
                                            const FrrOspfAreaVirtualLink &val);

Vector<mesa_ipv4_t> frr_ospf_area_stub_conf_get(FrrConf &config,
                                                const OspfInst &id);
mesa_rc frr_ospf_area_stub_conf_set(const OspfInst &id, const mesa_ipv4_t &area);
mesa_rc frr_ospf_area_stub_conf_del(const OspfInst &id, const mesa_ipv4_t &area);

Vector<mesa_ipv4_t> frr_ospf_area_stub_no_summary_conf_get(FrrConf &config,
                                                           const OspfInst &id);
mesa_rc frr_ospf_area_stub_no_summary_conf_set(const OspfInst &id,
                                               const mesa_ipv4_t &area);
mesa_rc frr_ospf_area_stub_no_summary_conf_del(const OspfInst &id,
                                               const mesa_ipv4_t &area);

enum FrrOspfAuthMode {
    FRR_OSPF_AUTH_MODE_AREA_CFG,
    FRR_OSPF_AUTH_MODE_NULL,
    FRR_OSPF_AUTH_MODE_PWD,
    FRR_OSPF_AUTH_MODE_MSG_DIGEST,
};

struct FrrOspfAreaAuth {
    FrrOspfAreaAuth() = default;
    FrrOspfAreaAuth(const mesa_ipv4_t &a, FrrOspfAuthMode mode)
        : area{a}, auth_mode{mode} {}
    mesa_ipv4_t area;
    FrrOspfAuthMode auth_mode;
};

Vector<FrrOspfAreaAuth> frr_ospf_area_authentication_conf_get(FrrConf &config,
                                                              const OspfInst &id);
mesa_rc frr_ospf_area_authentication_conf_set(const OspfInst &id,
                                              const FrrOspfAreaAuth &val);

struct FrrOspfDigestData {
    FrrOspfDigestData(const uint8_t kid, const std::string &kval)
        : keyid{kid}, key{kval} {}
    uint8_t keyid;
    std::string key;
};

struct FrrOspfAreaVirtualLinkAuth {
    FrrOspfAreaVirtualLinkAuth() = default;
    FrrOspfAreaVirtualLinkAuth(const FrrOspfAreaVirtualLink &link,
                               FrrOspfAuthMode mode)
        : virtual_link{link}, auth_mode{mode} {}

    FrrOspfAreaVirtualLink virtual_link;
    FrrOspfAuthMode auth_mode;
};

Vector<FrrOspfAreaVirtualLinkAuth> frr_ospf_area_virtual_link_authentication_conf_get(
        FrrConf &config, const OspfInst &id);
mesa_rc frr_ospf_area_virtual_link_authentication_conf_set(
        const OspfInst &id, const FrrOspfAreaVirtualLinkAuth &val);

struct FrrOspfAreaVirtualLinkDigest {
    FrrOspfAreaVirtualLinkDigest() = default;
    FrrOspfAreaVirtualLinkDigest(const FrrOspfAreaVirtualLink &link,
                                 const FrrOspfDigestData &data)
        : virtual_link{link}, digest_data{data} {}

    FrrOspfAreaVirtualLink virtual_link;
    FrrOspfDigestData digest_data;
};

Vector<FrrOspfAreaVirtualLinkDigest>
frr_ospf_area_virtual_link_message_digest_conf_get(FrrConf &config,
                                                   const OspfInst &id);
mesa_rc frr_ospf_area_virtual_link_message_digest_conf_set(
        const OspfInst &id, const FrrOspfAreaVirtualLink &val,
        const FrrOspfDigestData &data);
mesa_rc frr_ospf_area_virtual_link_message_digest_conf_del(
        const OspfInst &id, const FrrOspfAreaVirtualLink &val,
        const FrrOspfDigestData &data);

struct FrrOspfAreaVirtualLinkKey {
    FrrOspfAreaVirtualLinkKey() = default;
    FrrOspfAreaVirtualLinkKey(const FrrOspfAreaVirtualLink &link,
                              const std::string key)
        : virtual_link{link}, key_data{key} {}

    FrrOspfAreaVirtualLink virtual_link;
    std::string key_data;
};

Vector<FrrOspfAreaVirtualLinkKey>
frr_ospf_area_virtual_link_authentication_key_conf_get(FrrConf &config,
                                                       const OspfInst &id);
mesa_rc frr_ospf_area_virtual_link_authentication_key_conf_set(
        const OspfInst &id, const FrrOspfAreaVirtualLink &val,
        const std::string &auth_key);
mesa_rc frr_ospf_area_virtual_link_authentication_key_conf_del(
        const OspfInst &id, const FrrOspfAreaVirtualLinkKey &val);

// OSPF Interface Configuration
// ------------------------------------------------
enum FrrOspfIfMtuMode {
    FRR_OSPF_IF_MTU_MODE_IGNORE,
    FRR_OSPF_IF_MTU_MODE_ENFORCE,
};

mesa_rc frr_ospf_if_mtu_mode_conf_set(vtss_ifindex_t i, FrrOspfIfMtuMode);

struct FrrOspfDeadInterval {
    FrrOspfDeadInterval() = default;
    FrrOspfDeadInterval(bool mul, uint32_t v) : multiplier{mul}, val{v} {}
    bool multiplier;
    uint32_t val;
};

FrrRes<FrrOspfDeadInterval> frr_ospf_if_dead_interval_conf_get(FrrConf &config,
                                                               vtss_ifindex_t i);
mesa_rc frr_ospf_if_dead_interval_conf_set(vtss_ifindex_t i, uint32_t val);
mesa_rc frr_ospf_if_dead_interval_minimal_conf_set(vtss_ifindex_t i,
                                                   uint32_t val);
mesa_rc frr_ospf_if_dead_interval_conf_del(vtss_ifindex_t i);

FrrRes<uint32_t> frr_ospf_if_hello_interval_conf_get(FrrConf &config,
                                                     vtss_ifindex_t i);
mesa_rc frr_ospf_if_hello_interval_conf_set(vtss_ifindex_t i, uint32_t val);
mesa_rc frr_ospf_if_hello_interval_conf_del(vtss_ifindex_t i);

FrrRes<uint32_t> frr_ospf_if_retransmit_interval_conf_get(FrrConf &config,
                                                          vtss_ifindex_t i);
mesa_rc frr_ospf_if_retransmit_interval_conf_set(vtss_ifindex_t i, uint32_t val);
mesa_rc frr_ospf_if_retransmit_interval_conf_del(vtss_ifindex_t i);

FrrRes<uint32_t> frr_ospf_if_cost_conf_get(FrrConf &config, vtss_ifindex_t i);
mesa_rc frr_ospf_if_cost_conf_set(vtss_ifindex_t i, uint32_t val);
mesa_rc frr_ospf_if_cost_conf_del(vtss_ifindex_t i);

FrrRes<std::string> frr_ospf_if_authentication_key_conf_get(FrrConf &config,
                                                            vtss_ifindex_t i);
mesa_rc frr_ospf_if_authentication_key_conf_set(vtss_ifindex_t i,
                                                const std::string &auth_key);
mesa_rc frr_ospf_if_authentication_key_conf_del(vtss_ifindex_t i);

FrrRes<FrrOspfAuthMode> frr_ospf_if_authentication_conf_get(FrrConf &config,
                                                            vtss_ifindex_t i);
mesa_rc frr_ospf_if_authentication_conf_set(vtss_ifindex_t i,
                                            FrrOspfAuthMode mode);

Vector<FrrOspfDigestData> frr_ospf_if_message_digest_conf_get(FrrConf &config,
                                                              vtss_ifindex_t i);
mesa_rc frr_ospf_if_message_digest_conf_set(vtss_ifindex_t i,
                                            const FrrOspfDigestData &data);
mesa_rc frr_ospf_if_message_digest_conf_del(vtss_ifindex_t i, uint8_t keyid);

FrrRes<uint8_t> frr_ospf_if_priority_conf_get(FrrConf &config, vtss_ifindex_t i);
mesa_rc frr_ospf_if_priority_conf_set(vtss_ifindex_t i, uint8_t val);

mesa_rc frr_ospf_if_del(vtss_ifindex_t i);

FrrRes<std::string> get_interface_name(const vtss_ifindex_t &i);

/******************************************************************************/
/** Module icli commands                                                      */
/******************************************************************************/
mesa_rc frr_zebra_icli_cmd(const char *cmds, bool hex);
mesa_rc frr_ospfd_icli_cmd(const char *cmds, bool hex);


/******************************************************************************/
/** For unit-test purpose                                                     */
/******************************************************************************/
Vector<std::string> to_vty_ip_route_conf_set(const FrrIpRoute &r);
Vector<std::string> to_vty_ip_route_conf_del(const FrrIpRoute &r);
Vector<std::string> to_vty_ospf_router_conf_del();
Vector<std::string> to_vty_ospf_router_conf_set();
Vector<std::string> to_vty_ospf_router_conf_set(const FrrOspfRouterConf &conf);
Vector<std::string> to_vty_ospf_area_network_conf_set(const FrrOspfAreaNetwork &val);
Vector<std::string> to_vty_ospf_area_network_conf_del(const FrrOspfAreaNetwork &val);
Vector<std::string> to_vty_ospf_area_authentication_conf_set(
        const mesa_ipv4_t &area, FrrOspfAuthMode mode);
Vector<std::string> to_vty_ospf_router_passive_if_default(bool enable);
Vector<std::string> to_vty_ospf_router_passive_if_conf_set(const std::string &ifname,
                                                           bool passive);
Vector<std::string> to_vty_ospf_if_mtu_mode_conf_set(const std::string &ifname,
                                                     FrrOspfIfMtuMode mode);
Vector<std::string> to_vty_ospf_if_field_conf_set(const std::string &ifname,
                                                  const std::string &field,
                                                  uint16_t val);
Vector<std::string> to_vty_ospf_if_field_conf_del(const std::string &ifname,
                                                  const std::string &field);
Vector<std::string> to_vty_ospf_area_range_conf_set(const FrrOspfAreaNetwork &val);
Vector<std::string> to_vty_ospf_area_range_conf_del(const FrrOspfAreaNetwork &val);
Vector<std::string> to_vty_ospf_area_range_not_advertise_conf_set(
        const FrrOspfAreaNetwork &val);
Vector<std::string> to_vty_ospf_area_range_not_advertise_conf_del(
        const FrrOspfAreaNetwork &val);
Vector<std::string> to_vty_ospf_area_range_cost_conf_set(
        const FrrOspfAreaNetworkCost &val);
Vector<std::string> to_vty_ospf_area_range_cost_conf_del(
        const FrrOspfAreaNetworkCost &val);
Vector<std::string> to_vty_ospf_area_virtual_link_conf_set(
        const FrrOspfAreaVirtualLink &val);
Vector<std::string> to_vty_ospf_area_virtual_link_conf_del(
        const FrrOspfAreaVirtualLink &val);
Vector<std::string> to_vty_ospf_area_stub_conf_set(const mesa_ipv4_t &area);
Vector<std::string> to_vty_ospf_area_stub_conf_del(const mesa_ipv4_t &area);
Vector<std::string> to_vty_ospf_if_authentication_key_del(const std::string &ifname);
Vector<std::string> to_vty_ospf_if_authentication_key_set(
        const std::string &ifname, const std::string &key);
Vector<std::string> to_vty_ospf_if_authentication_set(const std::string &ifname,
                                                      FrrOspfAuthMode mode);
Vector<std::string> to_vty_ospf_if_message_digest_set(
        const std::string &ifname, const FrrOspfDigestData &data);
Vector<std::string> to_vty_ospf_if_message_digest_del(const std::string &ifname,
                                                      uint8_t keyid);
Vector<std::string> to_vty_ospf_area_stub_no_summary_conf_set(
        const mesa_ipv4_t &area);
Vector<std::string> to_vty_ospf_area_stub_no_summary_conf_del(
        const mesa_ipv4_t &area);
Vector<std::string> to_vty_ospf_area_virtual_link_authentication_conf_set(
        const FrrOspfAreaVirtualLink &val, FrrOspfAuthMode mode);
Vector<std::string> to_vty_ospf_area_virtual_link_message_digest_conf_set(
        const FrrOspfAreaVirtualLink &val, const FrrOspfDigestData &data);
Vector<std::string> to_vty_ospf_area_virtual_link_message_digest_conf_del(
        const FrrOspfAreaVirtualLink &val, const FrrOspfDigestData &data);
Vector<std::string> to_vty_ospf_area_virtual_link_authentication_key_conf_set(
        const FrrOspfAreaVirtualLink &val, const std::string &auth_key);
Vector<std::string> to_vty_ospf_area_virtual_link_authentication_key_conf_del(
        const FrrOspfAreaVirtualLinkKey &val);
Vector<std::string> to_vty_ospf_router_default_metric_conf_set(uint32_t val);
Vector<std::string> to_vty_ospf_router_default_metric_conf_del();
Vector<std::string> to_vty_ospf_router_default_information_conf_set(
        const FrrOspfRouterDefaultInformation &val);
Vector<std::string> to_vty_ospf_router_default_information_conf_del();
Vector<std::string> to_vty_ospf_router_redistribute_conf_set(
        const FrrOspfRouterRedistribute &val);
Vector<std::string> to_vty_ospf_router_redistribute_conf_del(
        FrrOspfRouterRedistributeProtocol val);

struct FrrConfStreamParserCB {
    virtual void root(const str &line) {}
    virtual void interface(const std::string &ifname, const str &line) {}
    virtual void router(const std::string &name, const str &line) {}
    virtual ~FrrConfStreamParserCB() = default;
};

void frr_conf_parser(std::string conf, FrrConfStreamParserCB &cb);

Vector<FrrIpRouteStatus> frr_ip_route_status_parse(const std::string &s);
FrrIpOspfStatus frr_ip_ospf_status_parse(const std::string &s);
Map<vtss_ifindex_t, FrrIpOspfIfStatus> frr_ip_ospf_interface_status_parse(
        const std::string &s);
Map<mesa_ipv4_t, Vector<FrrIpOspfNeighborStatus>> frr_ip_ospf_neighbor_status_parse(
        const std::string &s);
}  // namespace vtss

#endif
