/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _FRR_TRACE_H_
#define _FRR_TRACE_H_

/**
 * \file frr_trace.hxx
 * \brief This file contains the definitions for module trace purpose.
 * When this file is included in the header file, you can use the following
 * macros.
 * C++: VTSS_TRACE(<sub_group>, <trace_level>)
 * C  : Original way, e.g. T_E(...),T_W(...), T_D(...).
 */

/******************************************************************************/
/** Includes                                                                  */
/******************************************************************************/
#include <vtss_trace_lvl_api.h>
#include "vtss/basics/trace.hxx"  // For VTSS_TRACE()
#include "vtss_module_id.h"       // For module ID

/******************************************************************************/
/** Trace module ID                                                           */
/******************************************************************************/
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_FRR

/******************************************************************************/
/** Trace module groups                                                       */
/******************************************************************************/
// Default module group
#define VTSS_TRACE_FRR_GRP_DEFAULT 0

// Critical group
#define TRACE_FRR_GRP_CRIT 1

// Application group
#define TRACE_FRR_GRP_APPL 2

// ICLI group
#define TRACE_FRR_GRP_ICLI 3

// Application group
#define TRACE_FRR_GRP_ICFG 4

// SNMP group
#define TRACE_FRR_GRP_SNMP 5

// Internal framework group
#define TRACE_FRR_GRP_FW 6

// Total counts of trace groups
#define TRACE_FRR_GRP_CNT 7

#endif /* _FRR_TRACE_H_ */
