/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

/******************************************************************************/
/** Includes                                                                  */
/******************************************************************************/
#include "frr_icli_functions.hxx"
#include "frr_api.hxx"
#include "icli_api.h"
#include "icli_porting_util.h"
#include "ip_utils.h"  // For vtss_conv_ipv4mask_to_prefix()
#include "misc_api.h"  // For misc_ipv4_txt()
#include "vtss/appl/ospf.h"
#include "vtss/basics/stream.hxx"  // For vtss::BufStream

/****************************************************************************/
/** Module default trace group declaration                                  */
/****************************************************************************/
/* All traces that does not explicitly specify a group, will use the
 * TRACE_FRR_GRP_ICLI group.
 *
 * Notice that the definition 'VTSS_TRACE_DEFAULT_GROUP' must declared before
 * adding "frr_trace.hxx" in the include header.
 */
#define VTSS_TRACE_DEFAULT_GROUP TRACE_FRR_GRP_ICLI
#include "frr_trace.hxx"  // For module trace group definitions

/******************************************************************************/
/** Namespaces using-declaration                                              */
/******************************************************************************/
using namespace vtss;

/******************************************************************************/
/** Internal variables and APIs                                               */
/******************************************************************************/
static mesa_rc FRR_ICLI_ipv4_wildcard_mask_to_prefix(const mesa_ipv4_t wildcard_mask,
                                                     u32 *const prefix) {
    /*  The IPv4 wildcard-mask contains 32 bits where 0 is a match and 1 is a
     * "do not care" bit e.g. 0.0.0.255 indicates a match in the first byte of
     * the network number.
     *
     * Although this CLI input argument allows setting 1 for any bits, but we
     * will map it's value to a network mask length. (It is required in the
     * OSPF public header. So there is limitation for the input format as
     * following.
     *
     * The "do not care"(value 1) bits must continuously and the "match"
     * (value 0) bits MUST not presented in its right-most bits.
     * For example, 0.0.0.255 means the network mask length is 8.
     *
     * A simple way that we use to convernt the "wildcard_mask" value to a
     *   network mask length is below.
     * a. Convert the input value with bitwise NOT algorithm (~).
     * b. Check the converted value with the same rule as IPv4 mask.
     */
    return vtss_conv_ipv4mask_to_prefix(~wildcard_mask, prefix);
}

/*
example:
Routing Process, with ID 61.0.0.86
 Initial SPF schedule delay 0 msecs
 Minimum hold time between two consecutive SPFs 50 msecs
 Maximum wait time between two consecutive SPFs 5000 msecs
 SPF algorithm last executed 1d 21:30:24 ago
 Minimum LSA interval 5 secs
 Minimum LSA arrival 1000 msecs
 Number of external LSA 0. Checksum Sum 0x00000000
 Number of areas in this router is 1
*/
static void FRR_ICLI_ospf_router_print(u32 session_id, vtss_appl_ospf_id_t id) {
    vtss_appl_ospf_router_status_t status;
    i8 str_buf[32];

    if (vtss_appl_ospf_router_status_get(id, &status) == VTSS_RC_OK) {
        ICLI_PRINTF("Routing Process, with ID %s\n",
                    misc_ipv4_txt(status.ospf_router_id, str_buf));
        ICLI_PRINTF(" Initial SPF schedule delay %d msecs\n", status.spf_delay);
        ICLI_PRINTF(
                " Minimum hold time between two consecutive SPFs %d msecs\n",
                status.spf_holdtime);
        ICLI_PRINTF(
                " Maximum wait time between two consecutive SPFs %d msecs\n",
                status.spf_max_waittime);
        if (status.last_executed_spf_ts) {
            ICLI_PRINTF(" SPF algorithm last executed %s ago\n",
                        misc_time_txt(status.last_executed_spf_ts / 1000));
        } else {
            ICLI_PRINTF(" SPF algorithm has not been run\n");
        }
        ICLI_PRINTF(" Minimum LSA interval %d secs\n", status.min_lsa_interval);
        ICLI_PRINTF(" Minimum LSA arrival %d msecs\n", status.min_lsa_arrival);
        ICLI_PRINTF(" Number of external LSA %d. Checksum Sum 0x%08x\n",
                    status.external_lsa_count, status.external_lsa_checksum);
        ICLI_PRINTF(" Number of areas in this router is %d\n",
                    status.attached_area_count);
    }
}

/* Output area range status/configuraiton.
 *
 * The 'Active(<route_metric>)' term is added when it is actively advertising
 * the area range. Otherwise, the 'Passive' term is added when the area range
 * doesn't take effect, e.g. the number of active interfaces in backbone area is
 * 0 or it is not in the routing table.
 *
 * Example:
 *       Area ranges are
 *           192.168.1.0/24 Passive Advertise
 *           192.168.2.0/24 Passive DoNotAdvertise
 *           192.168.3.0/24 Active(1) Advertise
 */
static void FRR_ICLI_ospf_area_range_print(u32 session_id, vtss_appl_ospf_id_t id,
                                           vtss_appl_ospf_area_id_t area_id) {
    vtss_appl_ospf_id_t cur_id = id, next_id;
    vtss_appl_ospf_area_id_t cur_area_id = area_id, next_area_id;
    mesa_ipv4_network_t *cur_network_p = NULL, cur_network, next_network;
    vtss_appl_ospf_area_range_conf_t area_range_conf;
    bool first_entry = true;
    vtss::Map<vtss_appl_route_ipv4_key_t, vtss_appl_route_ipv4_status_t> ipv4_routes;
    bool got_ipv4_routes = false;
    StringStream str_buf;

    /* Iterate through all existing area range entries. */
    while (vtss_appl_ospf_area_range_conf_itr(&cur_id, &next_id, &cur_area_id,
                                              &next_area_id, cur_network_p,
                                              &next_network) == VTSS_OK) {
        if (next_id != id || next_area_id != area_id) {
            break;  // Only current instance ID and area ID are required here
        }

        // Switch to current data for next loop
        if (!cur_network_p) {              // Get-First operation
            cur_network_p = &cur_network;  // Switch to Get-Next operation
        }
        cur_id = next_id;
        cur_area_id = next_area_id;
        cur_network = next_network;

        if (vtss_appl_ospf_area_range_conf_get(cur_id, cur_area_id, cur_network,
                                               &area_range_conf) != VTSS_OK) {
            continue;
        }

        // Get backbone area status
        vtss_appl_ospf_area_status_t backbone_area_status;
        vtss_clear(backbone_area_status);
        (void)vtss_appl_ospf_area_status_get(id, FRR_OSPF_BACKBONE_AREA_ID,
                                             &backbone_area_status);

        /* Iterate through all existing route entries.
         * Notice that the iteration is processed only when the backbone area
         * active interfaces count not equals 0 */
        bool is_active = false;
        vtss_appl_route_ipv4_status_t route_status = {};
        if (area_range_conf.is_advertised &&
            backbone_area_status.attached_intf_active_count && !got_ipv4_routes) {
            if (vtss_appl_route_ipv4_status_get_all(ipv4_routes) == VTSS_OK) {
                got_ipv4_routes = true;
            }
        }

        if (area_range_conf.is_advertised &&
            backbone_area_status.attached_intf_active_count) {
            vtss_appl_route_ipv4_key_t route_ipv4_key = {};
            route_ipv4_key.network = cur_network;
            route_ipv4_key.protocol = VTSS_APPL_ROUTE_PROTO_OSPF;

            for (auto it = ipv4_routes.greater_than_or_equal(route_ipv4_key);
                 it != ipv4_routes.end(); ++it) {
                if (it->first.protocol != VTSS_APPL_ROUTE_PROTO_OSPF) {
                    continue;
                }

                is_active = vtss_ipv4_net_include(cur_network_p,
                                                  &it->first.network.address);

                if (is_active) {
                    route_status = it->second;
                }

                break;  // Got the final 'Active' state.
            }
        }

        str_buf.clear();
        if (first_entry) {
            first_entry = false;  // This line is printed once
            ICLI_PRINTF("        Area ranges are\n");
        }
        str_buf << "            " << cur_network
                << (is_active ? " Active" : " Passive");
        if (is_active) {
            str_buf << "(" << route_status.metric << ")";
        }

        str_buf << (area_range_conf.is_advertised ? " Advertise"
                                                  : " DoNotAdvertise");
        ICLI_PRINTF("%s\n", str_buf.cstring());
    }
}

/*
example:
    Area BACKBONE(0.0.0.0)
        Number of active interfaces in this area is 3
        Area has no authentication
        SPF algorithm executed 158 times
        Number of LSA 26
        Number of router LSA 6. Checksum Sum 0x000214de
        Number of network LSA 4. Checksum Sum 0x0001919a
        Number of summary LSA 15. Checksum Sum 0x00080bc2
        Number of ASBR summary LSA 1. Checksum Sum 0x000087af

    Area ID: 0.0.0.1
        Number of active interfaces in this area is 1
        It is a stub area
        Area has no authentication
        SPF algorithm executed 8 times
        Number of LSA 19
        Number of router LSA 2. Checksum Sum 0x0000ed19
        Number of network LSA 1. Checksum Sum 0x00008683
        Number of summary LSA 16. Checksum Sum 0x00087319
        Number of ASBR summary LSA 0. Checksum Sum 0x00000000
        Area ranges are
            192.168.1.0/24 Passive DoNotAdvertise
*/
static void FRR_ICLI_ospf_router_area_print(u32 session_id,
                                            vtss_appl_ospf_id_t id,
                                            vtss_appl_ospf_area_id_t area_id) {
    vtss_appl_ospf_area_status_t status;
    i8 buf[32];

    if (vtss_appl_ospf_area_status_get(id, area_id, &status) == VTSS_RC_OK) {
        if (status.is_backbone) {
            ICLI_PRINTF("    Area BACKBONE(%s)\n", misc_ipv4_txt(area_id, buf));
        } else {
            ICLI_PRINTF("    Area ID: %s\n", misc_ipv4_txt(area_id, buf));
        }
        ICLI_PRINTF("        Number of active interfaces in this area is %d\n",
                    status.attached_intf_active_count);
        if (status.area_type == VTSS_APPL_OSPF_AREA_STUB) {
            ICLI_PRINTF("        It is a stub area\n");
        } else if (status.area_type == VTSS_APPL_OSPF_AREA_TOTALLY_STUB) {
            ICLI_PRINTF("        It is a totally stub area\n");
        }

        // authentication
        ICLI_PRINTF("        Area has ");
        if (status.auth_type == VTSS_APPL_OSPF_AUTH_TYPE_NULL) {
            ICLI_PRINTF("no");
        } else if (status.auth_type == VTSS_APPL_OSPF_AUTH_TYPE_SIMPLE_PASSWORD) {
            ICLI_PRINTF("simple password");
        } else if (status.auth_type == VTSS_APPL_OSPF_AUTH_TYPE_MD5) {
            ICLI_PRINTF("message digest");
        } else {
            ICLI_PRINTF("unknown");
        }
        ICLI_PRINTF(" authentication\n");

        ICLI_PRINTF("        SPF algorithm executed %d times\n",
                    status.spf_executed_count);

        ICLI_PRINTF("        Number of LSA %d\n", status.lsa_count);
        ICLI_PRINTF("        Number of router LSA %d. Checksum Sum 0x%08x\n",
                    status.router_lsa_count, status.router_lsa_checksum);
        ICLI_PRINTF("        Number of network LSA %d. Checksum Sum 0x%08x\n",
                    status.network_lsa_count, status.network_lsa_checksum);
        ICLI_PRINTF("        Number of summary LSA %d. Checksum Sum 0x%08x\n",
                    status.summary_lsa_count, status.summary_lsa_checksum);
        ICLI_PRINTF(
                "        Number of ASBR summary LSA %d. Checksum Sum 0x%08x\n",
                status.asbr_summary_lsa_count, status.asbr_summary_lsa_checksum);

        FRR_ICLI_ospf_area_range_print(session_id, id, area_id);
    }
}

/******************************************************************************/
/** Module ICLI APIs                                                          */
/******************************************************************************/
/* Set OSPF router ID */
mesa_rc FRR_ICLI_ospf_router_id_set(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;
    vtss_appl_ospf_router_conf_t router_conf;

    if (vtss_appl_ospf_router_conf_get(req.inst_id, &router_conf) != VTSS_OK) {
        ICLI_PRINTF("%% Get OSPF router configuration failed.\n");
        return ICLI_RC_ERROR;
    }

    router_conf.router_id.is_specific_id = true;
    router_conf.router_id.id = req.router_id;
    rc = vtss_appl_ospf_router_conf_set(req.inst_id, &router_conf);
    if (rc == VTSS_APPL_FRR_OSPF_ERROR_INVALID_ROUTER_ID) {
        ICLI_PRINTF("%% The OSPF router ID is invalid.\n");
    } else if (rc == VTSS_APPL_FRR_OSPF_ERROR_ROUTER_ID_CHANGE_NOT_TAKE_EFFECT) {
        ICLI_PRINTF("%% %s. (Command: 'clear ip ospf process')\n",
                    frr_error_txt(rc));
    } else if (rc != VTSS_OK) {
        ICLI_PRINTF("%% Set OSPF router ID failed.\n");
        return ICLI_RC_ERROR;
    }

    return ICLI_RC_OK;
}

/* Delete OSPF router ID */
mesa_rc FRR_ICLI_ospf_router_id_del(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;
    vtss_appl_ospf_router_conf_t router_conf;

    if (vtss_appl_ospf_router_conf_get(req.inst_id, &router_conf) != VTSS_OK) {
        ICLI_PRINTF("%% Get OSPF router configuration failed.\n");
        return ICLI_RC_ERROR;
    }

    router_conf.router_id.is_specific_id = false;
    router_conf.router_id.id = 0;
    rc = vtss_appl_ospf_router_conf_set(req.inst_id, &router_conf);
    if (rc == VTSS_APPL_FRR_OSPF_ERROR_ROUTER_ID_CHANGE_NOT_TAKE_EFFECT) {
        ICLI_PRINTF("%% %s. (Command: 'clear ip ospf process')\n",
                    frr_error_txt(rc));
    } else if (rc != VTSS_OK) {
        ICLI_PRINTF("%% Set OSPF router ID failed.\n");
        return ICLI_RC_ERROR;
    }

    return ICLI_RC_OK;
}

/* Set OSPF all interfaces as passive-interface by default */
mesa_rc FRR_ICLI_ospf_passive_interface_default(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;
    vtss_appl_ospf_router_conf_t router_conf;

    /* Get the original configuration */
    if (vtss_appl_ospf_router_conf_get(req.inst_id, &router_conf) != VTSS_OK) {
        ICLI_PRINTF("%% Get OSPF router configuration failed.\n");
        return ICLI_RC_ERROR;
    }

    /* Apply the new configuration */
    router_conf.default_passive_interface = req.passive_enabled;
    rc = vtss_appl_ospf_router_conf_set(req.inst_id, &router_conf);
    if (rc != VTSS_OK) {
        ICLI_PRINTF("%% Set OSPF passive-interface by default failed.\n");
        return ICLI_RC_ERROR;
    }

    return ICLI_RC_OK;
}

/* Set OSPF network area */
mesa_rc FRR_ICLI_ospf_network_area_set(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;
    uint32_t prefix;
    mesa_ipv4_network_t network;

    if (FRR_ICLI_ipv4_wildcard_mask_to_prefix(req.wildcard_mask, &prefix) !=
        VTSS_RC_OK) {
        ICLI_PRINTF(
                "%% Cannot map the wildcard mask vaule to a valid network"
                " mask length.\n"
                "The 'do not care'(value 1) bits must continuously and "
                "the 'match'(value 0) bits MUST not presented in its "
                "right-most bits.\n"
                "For example, 0.0.0.255 means the network mask length is 8.\n");
        return ICLI_RC_ERROR;
    }

    network.address = req.ip_address;
    network.prefix_size = prefix;
    rc = vtss_appl_ospf_area_conf_add(req.inst_id, &network,
                                      (vtss_appl_ospf_area_id_t *)&req.area_id);
    if (rc != VTSS_OK) {
        if (rc == VTSS_APPL_FRR_ERROR_ENTRY_ALREADY_EXISTING) {
            ICLI_PRINTF(
                    "%% The same network IP segment is already existing.\n");
        } else if (rc == VTSS_APPL_FRR_ERROR_ADDR_RANGE_OVERLAP) {
            ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
        } else if (rc == VTSS_APPL_FRR_OSPF_ERROR_AREA_ID_CHANGE_NOT_TAKE_EFFECT) {
            ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
        } else {
            ICLI_PRINTF("%% Set OSPF network area failed.\n");
        }
    }

    return ICLI_RC_OK;
}

/* Delete OSPF network area */
mesa_rc FRR_ICLI_ospf_network_area_del(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;
    uint32_t prefix;
    mesa_ipv4_network_t network;
    vtss_appl_ospf_area_id_t orig_area_id;
    char str_buf[16];

    if (FRR_ICLI_ipv4_wildcard_mask_to_prefix(req.wildcard_mask, &prefix) !=
        VTSS_RC_OK) {
        ICLI_PRINTF(
                "%% Cannot map the wildcard mask vaule to a valid network"
                " mask length.\n"
                "The 'do not care'(value 1) bits must continuously and "
                "the 'match'(value 0) bits MUST not presented in its "
                "right-most bits.\n"
                "For example, 0.0.0.255 means the network mask length is 8.\n");
        return ICLI_RC_ERROR;
    }

    network.address = req.ip_address;
    network.prefix_size = prefix;
    if (req.has_area_id) {
        if (vtss_appl_ospf_area_conf_get(req.inst_id, &network, &orig_area_id) ==
                    VTSS_OK &&
            orig_area_id != req.area_id) {
            ICLI_PRINTF(
                    "%% This network IP segment is associated with another "
                    "area ID %s.\n",
                    misc_ipv4_txt(orig_area_id, str_buf));
            return ICLI_RC_ERROR;
        }
    }
    rc = vtss_appl_ospf_area_conf_del(req.inst_id, &network);
    if (rc == VTSS_APPL_FRR_OSPF_ERROR_AREA_ID_CHANGE_NOT_TAKE_EFFECT) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
    }

    return ICLI_RC_OK;
}

/* Set OSPF passive-interface */
mesa_rc FRR_ICLI_ospf_passive_interface_set(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;
    vtss_ifindex_t ifindex;
    vtss_appl_ospf_router_intf_conf_t conf;
    StringStream str_buf;

    /* Iterate through all vlan IDs. */
    for (u32 idx = 0; idx < req.vlan_list->cnt; ++idx) {
        for (mesa_vid_t vid = req.vlan_list->range[idx].min;
             vid <= req.vlan_list->range[idx].max; ++vid) {
            if (vtss_ifindex_from_vlan(vid, &ifindex) != VTSS_OK) {
                continue;
            }

            str_buf.clear();
            str_buf << ifindex;

            /* Get the original configuration */
            rc = vtss_appl_ospf_router_intf_conf_get(req.inst_id, ifindex, &conf);
            if (rc == VTSS_APPL_FRR_OSPF_ERROR_VLAN_INTF_NOT_EXIST) {
                ICLI_PRINTF(
                        "%% VLAN interface %s does not exist. Use command "
                        "'interface vlan <vid>' in global configuration mode "
                        "to "
                        "create an interface.\n",
                        str_buf.cstring());
                continue;
            } else if (rc != VTSS_OK) {
                ICLI_PRINTF(
                        "%% Get OSPF router interface configuration "
                        "failed on %s.\n",
                        str_buf.cstring());
                continue;
            }

            /* Apply the new configuration */
            conf.passive_enabled = req.passive_enabled;
            rc = vtss_appl_ospf_router_intf_conf_set(req.inst_id, ifindex, &conf);
            if (rc != VTSS_OK) {
                ICLI_PRINTF("%% Set OSPF passive-interface failed on %s.\n",
                            str_buf.cstring());
                continue;
            }
        }
    }

    return ICLI_RC_OK;
}

//------------------------------------------------------------------------------
//** OSPF route redistribution
//------------------------------------------------------------------------------
/* Set OSPF route redistribution */
mesa_rc FRR_ICLI_ospf_redist_set(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    vtss_appl_ospf_router_conf_t router_conf;

    /* Get the original configuration */
    if (vtss_appl_ospf_router_conf_get(req.inst_id, &router_conf) != VTSS_OK) {
        ICLI_PRINTF("%% Get OSPF router configuration failed.\n");
        return ICLI_RC_ERROR;
    }

    /* Update the new setting */
    router_conf.redist_conf[req.redist_protocol].type = req.metric_type;
    router_conf.redist_conf[req.redist_protocol].metric = req.metric;

    /* Apply the new configuration */
    mesa_rc rc = vtss_appl_ospf_router_conf_set(req.inst_id, &router_conf);
    if (rc != VTSS_OK) {
        ICLI_PRINTF("%% Set OSPF route redistribution failed.\n");
        return rc;
    }

    return ICLI_RC_OK;
}

//------------------------------------------------------------------------------
//** OSPF default metric
//------------------------------------------------------------------------------
/* Set OSPF default metric */
mesa_rc FRR_ICLI_ospf_def_metric_set(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    vtss_appl_ospf_router_conf_t router_conf;

    /* Get the original configuration */
    if (vtss_appl_ospf_router_conf_get(req.inst_id, &router_conf) != VTSS_OK) {
        ICLI_PRINTF("%% Get OSPF router configuration failed.\n");
        return ICLI_RC_ERROR;
    }

    /* Update the new setting */
    router_conf.is_specific_def_metric = req.has_metric ? true : false;
    router_conf.def_metric = req.has_metric ? req.metric : 0;

    /* Apply the new configuration */
    mesa_rc rc = vtss_appl_ospf_router_conf_set(req.inst_id, &router_conf);
    if (rc != VTSS_OK) {
        ICLI_PRINTF("%% Set OSPF default metric failed.\n");
        return rc;
    }

    return ICLI_RC_OK;
}

//----------------------------------------------------------------------------
//** OSPF area range
//----------------------------------------------------------------------------
/* Set OSPF area range */
mesa_rc FRR_ICLI_ospf_area_range_set(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;
    mesa_ipv4_network_t network;
    vtss_appl_ospf_area_range_conf_t conf;

    /* Convert entry keys */
    network.address = req.ip_address;
    if (vtss_conv_ipv4mask_to_prefix(req.ip_address_mask,
                                     &network.prefix_size) != VTSS_RC_OK) {
        ICLI_PRINTF(
                "%% Cannot map the IP address mask vaule to a valid network"
                " mask length.\n");
        return ICLI_RC_ERROR;
    }

    /* Check if it is a new entry or not */
    bool new_entry;
    rc = vtss_appl_ospf_area_range_conf_get(req.inst_id, req.area_id, network,
                                            &conf);
    if (rc == VTSS_OK) {  // already existing
        new_entry = false;
    } else if (rc != VTSS_APPL_FRR_ERROR_ENTRY_NOT_FOUND) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
        return rc;
    } else {  // New entry
        new_entry = true;

        // Get the default setting
        rc = frr_ospf_area_range_conf_def(
                (vtss_appl_ospf_id_t *)&req.inst_id,
                (vtss_appl_ospf_area_id_t *)&req.area_id, &network, &conf);
        if (rc != VTSS_OK) {
            ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
        }
    }

    /* Update the new values */
    conf.is_advertised = req.area_range_advertise;
    if (req.has_cost) {
        conf.is_specific_cost = true;
        conf.cost = req.cost;
        conf.is_advertised = true;  // setting cost also sets advertise
    }

    if (!req.area_range_advertise) {
        conf.is_specific_cost =
                false;  // setting not-advertise also sets no-cost
    }

    /* Apply the new configuration */
    if (new_entry) {
        rc = vtss_appl_ospf_area_range_conf_add(req.inst_id, req.area_id,
                                                network, &conf);
    } else {
        rc = vtss_appl_ospf_area_range_conf_set(req.inst_id, req.area_id,
                                                network, &conf);
    }
    if (rc != VTSS_OK) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
    }

    return ICLI_RC_OK;
}

/* Delete/Restore OSPF area range */
mesa_rc FRR_ICLI_ospf_area_range_del_or_restore(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;
    mesa_ipv4_network_t network;

    /* Convert entry keys */
    network.address = req.ip_address;
    if (vtss_conv_ipv4mask_to_prefix(req.ip_address_mask,
                                     &network.prefix_size) != VTSS_RC_OK) {
        ICLI_PRINTF(
                "%% Cannot map the IP address mask vaule to a valid network"
                " mask length.\n");
        return ICLI_RC_ERROR;
    }

    /* 'no cost' is to reset cost
       'no advertise/not-advertise' will delete area entry */

    /* Restore the default setting */
    if (req.has_cost) {
        vtss_appl_ospf_area_range_conf_t conf, def_conf;

        // Get the original configuration
        rc = vtss_appl_ospf_area_range_conf_get(req.inst_id, req.area_id,
                                                network, &conf);
        if (rc != VTSS_OK) {
            // For the deleting operation, quit silently when it does not exists
            return ICLI_RC_OK;
        }

        // Update the default configuration
        rc = frr_ospf_area_range_conf_def(
                (vtss_appl_ospf_id_t *)&req.inst_id,
                (vtss_appl_ospf_area_id_t *)&req.area_id, &network, &def_conf);
        conf.is_specific_cost = def_conf.is_specific_cost;
        conf.cost = def_conf.cost;

        /* Update the entry */
        rc = vtss_appl_ospf_area_range_conf_set(req.inst_id, req.area_id,
                                                network, &conf);
    } else {
        /* Delete the entry */
        rc = vtss_appl_ospf_area_range_conf_del(req.inst_id, req.area_id,
                                                network);
    }

    if (rc != VTSS_OK) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
    }

    return ICLI_RC_OK;
}

//----------------------------------------------------------------------------
//** OSPF interface authentication
//----------------------------------------------------------------------------
/* Set OSPF interface authentication */
mesa_rc FRR_ICLI_ospf_intf_auth_set(const FrrIcliReq &req) {
    mesa_rc rc;
    vtss_ifindex_t ifindex;
    u32 session_id = req.session_id;
    StringStream str_buf;
    vtss_appl_ospf_intf_conf_t conf;

    // Iterate through all vlan IDs.
    for (u32 idx = 0; idx < req.vlan_list->cnt; ++idx) {
        for (mesa_vid_t vid = req.vlan_list->range[idx].min;
             vid <= req.vlan_list->range[idx].max; ++vid) {
            if (vtss_ifindex_from_vlan(vid, &ifindex) != VTSS_OK ||
                vtss_appl_ospf_intf_conf_get(ifindex, &conf) != VTSS_OK) {
                continue;
            }

            conf.auth_type = req.auth_type;
            rc = vtss_appl_ospf_intf_conf_set(ifindex, &conf);
            if (rc != VTSS_OK) {
                str_buf.clear();
                str_buf << ifindex;
                ICLI_PRINTF(" %% %s on %s.\n", frr_error_txt(rc),
                            str_buf.cstring());
            }
        }
    }

    return ICLI_RC_OK;
}

/* Set OSPF interface authentication simple password */
mesa_rc FRR_ICLI_ospf_intf_auth_simple_pwd_set(const FrrIcliReq &req) {
    mesa_rc rc;
    vtss_ifindex_t ifindex;
    u32 session_id = req.session_id;
    StringStream str_buf;
    vtss_appl_ospf_intf_conf_t conf;

    // Iterate through all vlan IDs.
    for (u32 idx = 0; idx < req.vlan_list->cnt; ++idx) {
        for (mesa_vid_t vid = req.vlan_list->range[idx].min;
             vid <= req.vlan_list->range[idx].max; ++vid) {
            if (vtss_ifindex_from_vlan(vid, &ifindex) != VTSS_OK ||
                vtss_appl_ospf_intf_conf_get(ifindex, &conf) != VTSS_OK) {
                continue;
            }

            strncpy(conf.auth_key, req.password, sizeof(conf.auth_key) - 1);
            conf.auth_key[sizeof(conf.auth_key) - 1] = '\0';
            conf.is_encrypted = req.has_encrypted;
            rc = vtss_appl_ospf_intf_conf_set(ifindex, &conf);
            if (rc != VTSS_OK) {
                str_buf.clear();
                str_buf << ifindex;
                ICLI_PRINTF(" %% %s on %s.\n", frr_error_txt(rc),
                            str_buf.cstring());
            }
        }
    }

    return ICLI_RC_OK;
}

/* Set OSPF interface authentication message digest key */
mesa_rc FRR_ICLI_ospf_intf_auth_md_key_set(const FrrIcliReq &req) {
    mesa_rc rc;
    vtss_ifindex_t ifindex;
    u32 session_id = req.session_id;
    StringStream str_buf;

    // Iterate through all vlan IDs.
    for (u32 idx = 0; idx < req.vlan_list->cnt; ++idx) {
        for (mesa_vid_t vid = req.vlan_list->range[idx].min;
             vid <= req.vlan_list->range[idx].max; ++vid) {
            if (vtss_ifindex_from_vlan(vid, &ifindex) != VTSS_OK) {
                continue;
            }

            vtss_appl_ospf_auth_digest_key_t conf;
            rc = vtss_appl_ospf_intf_auth_digest_key_get(ifindex, req.md_key_id,
                                                         &conf);
            str_buf.clear();
            str_buf << ifindex;
            if (rc != VTSS_APPL_FRR_ERROR_ENTRY_NOT_FOUND) {
                if (rc == VTSS_RC_OK) {
                    ICLI_PRINTF(" %% Key %d already exists on %s.\n",
                                req.md_key_id, str_buf.cstring());
                } else {
                    ICLI_PRINTF(" %% %s on %s.\n", frr_error_txt(rc),
                                str_buf.cstring());
                }
                continue;
            }

            strncpy(conf.digest_key, req.password, sizeof(conf.digest_key) - 1);
            conf.digest_key[sizeof(conf.digest_key) - 1] = '\0';
            conf.is_encrypted = req.has_encrypted;
            rc = vtss_appl_ospf_intf_auth_digest_key_add(ifindex, req.md_key_id,
                                                         &conf);
            if (rc != VTSS_RC_OK) {
                ICLI_PRINTF(" %% %s on %s.\n", frr_error_txt(rc),
                            str_buf.cstring());
            }
        }
    }

    return ICLI_RC_OK;
}

/* Delete OSPF interface authentication message digest key */
mesa_rc FRR_ICLI_ospf_intf_auth_md_key_del(const FrrIcliReq &req) {
    vtss_ifindex_t ifindex;
    u32 session_id = req.session_id;

    // Iterate through all vlan IDs.
    for (u32 idx = 0; idx < req.vlan_list->cnt; ++idx) {
        for (mesa_vid_t vid = req.vlan_list->range[idx].min;
             vid <= req.vlan_list->range[idx].max; ++vid) {
            if (vtss_ifindex_from_vlan(vid, &ifindex) != VTSS_OK) {
                continue;
            }

            auto rc = vtss_appl_ospf_intf_auth_digest_key_del(ifindex,
                                                              req.md_key_id);
            if (rc != VTSS_RC_OK) {
                StringStream str_buf;
                str_buf << ifindex;
                ICLI_PRINTF(" %% %s on %s.\n", frr_error_txt(rc),
                            str_buf.cstring());
            }
        }
    }

    return ICLI_RC_OK;
}

//----------------------------------------------------------------------------
//** OSPF area authentication
//----------------------------------------------------------------------------
/* Set OSPF area authentication */
mesa_rc FRR_ICLI_ospf_area_auth_set(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;
    vtss_appl_ospf_auth_type_t auth_type;

    /* Check if it is a new entry or not */
    bool new_entry = true;
    rc = vtss_appl_ospf_area_auth_conf_get(req.inst_id, req.area_id, &auth_type);
    if (rc == VTSS_OK) {  // already existing
        new_entry = false;
    } else if (rc != VTSS_APPL_FRR_ERROR_ENTRY_NOT_FOUND) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
        return rc;
    }

    /* Apply the new configuration */
    rc = VTSS_OK;
    if (new_entry) {
        rc = vtss_appl_ospf_area_auth_conf_add(req.inst_id, req.area_id,
                                               req.auth_type);
    } else if (auth_type != req.auth_type) {
        rc = vtss_appl_ospf_area_auth_conf_set(req.inst_id, req.area_id,
                                               req.auth_type);
    }
    if (rc != VTSS_OK) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
    }

    return ICLI_RC_OK;
}

/* Delete OSPF area authentication */
mesa_rc FRR_ICLI_ospf_area_auth_del(const FrrIcliReq &req) {
    u32 session_id = req.session_id;

    /* Delete the entry */
    mesa_rc rc = vtss_appl_ospf_area_auth_conf_del(req.inst_id, req.area_id);
    if (rc != VTSS_OK) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
    }

    return ICLI_RC_OK;
}

//----------------------------------------------------------------------------
//** OSPF virtual link
//----------------------------------------------------------------------------

/* Set OSPF virtual link */
mesa_rc FRR_ICLI_ospf_area_vlink_set(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;
    vtss_appl_ospf_vlink_conf_t conf;
    bool new_entry;

    /* Check if it is a new entry or not */
    rc = vtss_appl_ospf_vlink_conf_get(req.inst_id, req.area_id, req.router_id,
                                       &conf);
    if (rc == VTSS_OK) {  // already existing
        new_entry = false;
    } else if (rc != VTSS_APPL_FRR_ERROR_ENTRY_NOT_FOUND) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
        return rc;
    } else {  // New entry
        new_entry = true;

        // Get the default setting
        rc = frr_ospf_vlink_conf_def(
                (vtss_appl_ospf_id_t *)&req.inst_id,
                (vtss_appl_ospf_area_id_t *)&req.area_id,
                (vtss_appl_ospf_router_id_t *)&req.router_id, &conf);
        if (rc != VTSS_OK) {
            ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
        }
    }

    /* Update the new values */
    if (req.hello_interval) {
        conf.hello_interval = req.hello_interval;
    }
    if (req.dead_interval) {
        conf.dead_interval = req.dead_interval;
    }
    if (req.retransmit_interval) {
        conf.retransmit_interval = req.retransmit_interval;
    }

    /* Apply the new configuration */
    if (new_entry) {
        rc = vtss_appl_ospf_vlink_conf_add(req.inst_id, req.area_id,
                                           req.router_id, &conf);
    } else {
        rc = vtss_appl_ospf_vlink_conf_set(req.inst_id, req.area_id,
                                           req.router_id, &conf);
    }
    if (rc != VTSS_OK) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
    }

    return ICLI_RC_OK;
}

/* Restore OSPF virtual link default setting */
mesa_rc FRR_ICLI_ospf_vlink_conf_restore(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;

    /* Restore the default setting */
    if (req.has_hello_interval || req.has_dead_interval ||
        req.has_retransmit_interval) {
        vtss_appl_ospf_vlink_conf_t conf, def_conf;

        // Get the original configuration
        rc = vtss_appl_ospf_vlink_conf_get(req.inst_id, req.area_id,
                                           req.router_id, &conf);
        if (rc != VTSS_OK) {
            // Quit silently when it does not exists
            return ICLI_RC_OK;
        }

        // Update the default configuration
        rc = frr_ospf_vlink_conf_def(
                (vtss_appl_ospf_id_t *)&req.inst_id,
                (vtss_appl_ospf_area_id_t *)&req.area_id,
                (vtss_appl_ospf_router_id_t *)&req.router_id, &def_conf);


        if (req.has_hello_interval) {
            conf.hello_interval = def_conf.hello_interval;
        }
        if (req.has_dead_interval) {
            conf.dead_interval = def_conf.dead_interval;
        }
        if (req.has_retransmit_interval) {
            conf.retransmit_interval = def_conf.retransmit_interval;
        }

        /* Update the entry */
        rc = vtss_appl_ospf_vlink_conf_set(req.inst_id, req.area_id,
                                           req.router_id, &conf);
    } else {
        /* Delete the entry */
        rc = vtss_appl_ospf_vlink_conf_del(req.inst_id, req.area_id,
                                           req.router_id);
    }

    if (rc != VTSS_OK) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
    }

    return ICLI_RC_OK;
}

/* Set OSPF virtual link authentication type */
mesa_rc FRR_ICLI_ospf_vlink_auth_type_set(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;
    vtss_appl_ospf_vlink_conf_t conf;
    bool new_entry;

    /* Check if it is a new entry or not */
    rc = vtss_appl_ospf_vlink_conf_get(req.inst_id, req.area_id, req.router_id,
                                       &conf);
    if (rc == VTSS_OK) {  // already existing
        new_entry = false;
    } else if (rc != VTSS_APPL_FRR_ERROR_ENTRY_NOT_FOUND) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
        return rc;
    } else {  // New entry
        new_entry = true;

        // Get the default setting
        rc = frr_ospf_vlink_conf_def(
                (vtss_appl_ospf_id_t *)&req.inst_id,
                (vtss_appl_ospf_area_id_t *)&req.area_id,
                (vtss_appl_ospf_router_id_t *)&req.router_id, &conf);
        if (rc != VTSS_RC_OK) {
            ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
        }
    }

    /* Update the new values */
    conf.auth_type = req.auth_type;

    /* Apply the new configuration */
    if (new_entry) {
        rc = vtss_appl_ospf_vlink_conf_add(req.inst_id, req.area_id,
                                           req.router_id, &conf);
    } else {
        rc = vtss_appl_ospf_vlink_conf_set(req.inst_id, req.area_id,
                                           req.router_id, &conf);
    }
    if (rc != VTSS_OK) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
    }

    return ICLI_RC_OK;
}

/* Set OSPF virtual link authentication simple password */
mesa_rc FRR_ICLI_ospf_vlink_auth_simple_pwd_set(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;
    vtss_appl_ospf_vlink_conf_t conf;
    bool new_entry;

    /* Check if it is a new entry or not */
    rc = vtss_appl_ospf_vlink_conf_get(req.inst_id, req.area_id, req.router_id,
                                       &conf);
    if (rc == VTSS_OK) {  // already existing
        new_entry = false;
    } else if (rc != VTSS_APPL_FRR_ERROR_ENTRY_NOT_FOUND) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
        return rc;
    } else {  // New entry
        new_entry = true;

        // Get the default setting
        rc = frr_ospf_vlink_conf_def(
                (vtss_appl_ospf_id_t *)&req.inst_id,
                (vtss_appl_ospf_area_id_t *)&req.area_id,
                (vtss_appl_ospf_router_id_t *)&req.router_id, &conf);
        if (rc != VTSS_OK) {
            ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
        }
    }

    /* Update the new values */
    conf.is_encrypted = req.has_encrypted;
    strcpy(conf.simple_pwd, req.password);

    /* Apply the new configuration */
    if (new_entry) {
        rc = vtss_appl_ospf_vlink_conf_add(req.inst_id, req.area_id,
                                           req.router_id, &conf);
    } else {
        rc = vtss_appl_ospf_vlink_conf_set(req.inst_id, req.area_id,
                                           req.router_id, &conf);
    }
    if (rc != VTSS_OK) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
    }

    return ICLI_RC_OK;
}

/* Set OSPF virtual link authentication message digest */
mesa_rc FRR_ICLI_ospf_vlink_md_key_set(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;
    vtss_appl_ospf_auth_digest_key_t key;

    /* Check if it is a new entry or not */
    rc = vtss_appl_ospf_vlink_md_key_conf_get(
            req.inst_id, req.area_id, req.router_id, req.md_key_id, &key);
    if (rc == VTSS_OK) {  // already existing
        rc = VTSS_APPL_FRR_ERROR_ENTRY_ALREADY_EXISTING;
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
        return rc;
    }

    /* Update the new values */
    key.is_encrypted = req.has_encrypted;
    strcpy(key.digest_key, req.password);

    /* Apply the new configuration */
    rc = vtss_appl_ospf_vlink_md_key_conf_add(
            req.inst_id, req.area_id, req.router_id, req.md_key_id, &key);
    if (rc != VTSS_OK) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
    }

    return ICLI_RC_OK;
}

/* Delete OSPF virtual link authentication message digest */
mesa_rc FRR_ICLI_ospf_vlink_md_key_del(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;

    rc = vtss_appl_ospf_vlink_md_key_conf_del(req.inst_id, req.area_id,
                                              req.router_id, req.md_key_id);
    if (rc != VTSS_OK) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
    }

    return ICLI_RC_OK;
}

//----------------------------------------------------------------------------
//** OSPF stub area
//----------------------------------------------------------------------------
/* Set the OSPF stub area */
mesa_rc FRR_ICLI_ospf_area_stub_set(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;
    vtss_appl_ospf_stub_area_conf_t conf;

    /* Check if it is a new entry or not */
    bool new_entry;
    rc = vtss_appl_ospf_stub_area_conf_get(req.inst_id, req.area_id, &conf);
    if (rc == VTSS_OK) {  // already existing
        new_entry = false;
    } else if (rc != VTSS_APPL_FRR_ERROR_ENTRY_NOT_FOUND) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
        return rc;
    } else {  // New entry
        new_entry = true;

        // Get the default setting
        rc = frr_ospf_stub_area_conf_def(
                (vtss_appl_ospf_id_t *)&req.inst_id,
                (vtss_appl_ospf_area_id_t *)&req.area_id, &conf);
        if (rc != VTSS_RC_OK) {
            ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
        }
    }

    /* Update the new values */
    conf.no_summary = req.has_no_summary ? true : false;

    /* Apply the new configuration */
    if (new_entry) {
        rc = vtss_appl_ospf_stub_area_conf_add(req.inst_id, req.area_id, &conf);
    } else {
        rc = vtss_appl_ospf_stub_area_conf_set(req.inst_id, req.area_id, &conf);
    }
    if (rc != VTSS_RC_OK) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
    }

    return ICLI_RC_OK;
}

/* Delete the OSPF stub area or set totally stub area as a stub area */
mesa_rc FRR_ICLI_ospf_area_stub_no(const FrrIcliReq &req) {
    u32 session_id = req.session_id;
    mesa_rc rc;

    /* req.has_no_summary is 'true' means setting the existent totally stub area
     * to stub area,
     * otherwise deleting it.
     */
    if (req.has_no_summary) {
        vtss_appl_ospf_stub_area_conf_t conf;
        rc = vtss_appl_ospf_stub_area_conf_get(req.inst_id, req.area_id, &conf);
        if (rc == VTSS_APPL_FRR_ERROR_ENTRY_NOT_FOUND) {
            return VTSS_RC_OK;
        }
        if (rc == VTSS_RC_OK) {
            conf.no_summary = false;
            rc = vtss_appl_ospf_stub_area_conf_set(req.inst_id, req.area_id,
                                                   &conf);
        }
    } else {
        rc = vtss_appl_ospf_stub_area_conf_del(req.inst_id, req.area_id);
    }

    if (rc != VTSS_RC_OK) {
        ICLI_PRINTF("%% %s.\n", frr_error_txt(rc));
    }

    return ICLI_RC_OK;
}

//----------------------------------------------------------------------------
//** OSPF interface parameter tuning
//----------------------------------------------------------------------------
/* Set OSPF vlan interface parameters */
mesa_rc FRR_ICLI_ospf_vlan_interface_set(const FrrIcliReq &req) {
    mesa_rc rc;
    vtss_ifindex_t ifindex;
    vtss_appl_ospf_intf_conf_t conf;
    u32 session_id = req.session_id; /* used for ICLI_PRINTF */

    // Iterate through all vlan IDs.
    for (u32 idx = 0; idx < req.vlan_list->cnt; ++idx) {
        for (mesa_vid_t vid = req.vlan_list->range[idx].min;
             vid <= req.vlan_list->range[idx].max; ++vid) {
            if (vtss_ifindex_from_vlan(vid, &ifindex) != VTSS_OK ||
                vtss_appl_ospf_intf_conf_get(ifindex, &conf) != VTSS_OK) {
                continue;
            }

            /* Update new parameters */
            // priority
            if (req.has_priority) {
                conf.priority = req.priority;
            }

            // cost
            if (req.has_cost) {
                conf.is_specific_cost = true;
                conf.cost = req.cost;
            }

            // hello interval
            if (req.has_hello_interval) {
                conf.hello_interval = req.hello_interval;
            }

            // retransmit interval
            if (req.has_retransmit_interval) {
                conf.retransmit_interval = req.retransmit_interval;
            }

            // dead interval
            if (req.has_dead_interval) {
                // if you set 'dead-interval' means 'fast hello' is disabled
                conf.is_fast_hello_enabled = false;
                conf.dead_interval = req.dead_interval;
            }

            // fast hello packets
            if (req.has_fast_hello) {
                conf.is_fast_hello_enabled = true;
                conf.fast_hello_packets = req.fast_hello_packets;
            }

            /* Apply the new configuration */
            if ((rc = vtss_appl_ospf_intf_conf_set(ifindex, &conf)) != VTSS_OK) {
                ICLI_PRINTF("Set OSPF interface configuration failed");
                return rc;
            }
        }
    }

    return ICLI_RC_OK;
}

/* Reset OSPF vlan interface parameters to default */
mesa_rc FRR_ICLI_ospf_vlan_interface_set_default(const FrrIcliReq &req) {
    mesa_rc rc;
    vtss_ifindex_t ifindex;
    vtss_appl_ospf_intf_conf_t conf, def_conf;


    // Iterate through all vlan IDs.
    for (u32 idx = 0; idx < req.vlan_list->cnt; ++idx) {
        for (mesa_vid_t vid = req.vlan_list->range[idx].min;
             vid <= req.vlan_list->range[idx].max; ++vid) {
            if (vtss_ifindex_from_vlan(vid, &ifindex) != VTSS_OK ||
                vtss_appl_ospf_intf_conf_get(ifindex, &conf) != VTSS_OK ||
                frr_ospf_intf_conf_def(&ifindex, &def_conf) != VTSS_OK) {
                continue;
            }

            /* Reset parameters */
            // priority
            if (req.has_priority) {
                conf.priority = def_conf.priority;
            }

            // cost
            if (req.has_cost) {
                conf.is_specific_cost = false;
                conf.cost = def_conf.cost;
            }

            // hello interval
            if (req.has_hello_interval) {
                conf.hello_interval = def_conf.hello_interval;
            }

            // retransmit interval
            if (req.has_retransmit_interval) {
                conf.retransmit_interval = def_conf.retransmit_interval;
            }

            // dead interval and fast hello packets
            if (req.has_dead_interval) {
                conf.dead_interval = def_conf.dead_interval;
                conf.is_fast_hello_enabled = def_conf.is_fast_hello_enabled;
                conf.fast_hello_packets = def_conf.fast_hello_packets;
            }

            /* Apply the new configuration */
            if ((rc = vtss_appl_ospf_intf_conf_set(ifindex, &conf)) != VTSS_OK) {
                return rc;
            }
        }
    }
    return ICLI_RC_OK;
}

/* Show OSPF information */
mesa_rc FRR_ICLI_ospf_show_info(const FrrIcliReq &req) {
    vtss_appl_ospf_id_t key_1, *ptr_1 = NULL;
    mesa_ipv4_t key_2, *ptr_2 = NULL;
    u32 session_id = req.session_id; /* used for ICLI_PRINTF */

    while (vtss_appl_ospf_inst_itr(ptr_1, &key_1) == VTSS_RC_OK) {
        if (!ptr_1) {
            ptr_1 = &key_1;
        }
        /* print router status */
        FRR_ICLI_ospf_router_print(req.session_id, key_1);

        auto current_id = key_1;
        while (vtss_appl_ospf_area_status_itr(ptr_1, &key_1, ptr_2, &key_2) ==
                       VTSS_RC_OK &&
               key_1 == current_id) {
            if (!ptr_2) {
                ptr_2 = &key_2;
            } else {
                /* newline to separeate from previous area */
                ICLI_PRINTF("\n");
            }

            /* print router status */
            FRR_ICLI_ospf_router_area_print(req.session_id, key_1, key_2);
        }
    }

    return VTSS_RC_OK;
}

/* Display single entry of OSPF interface status */
static mesa_rc FRR_ICLI_ospf_interface_print(u32 session_id,
                                             vtss_ifindex_t ifindex,
                                             bool *add_new_line) {
    vtss_appl_ospf_interface_status_t entry;
    char buf[128];

    mesa_rc rc = vtss_appl_ospf_interface_status_get(ifindex, &entry);
    if (rc != VTSS_RC_OK) {
        return rc;
    }

    if (*add_new_line) {
        ICLI_PRINTF("\n");
    } else {
        *add_new_line = true;
    }

    // Syntax : Vlan60 is up
    ICLI_PRINTF("%s is %s\n", vtss_ifindex2str(buf, sizeof(buf), ifindex),
                entry.status ? "up" : "down");

    // Syntax : Internet Address 60.0.0.89/8, Area 0
    ICLI_PRINTF("  Internet Address %s/%d",
                misc_ipv4_txt(entry.network.address, buf),
                entry.network.prefix_size);
    ICLI_PRINTF(", Area %s\n", misc_ipv4_txt(entry.area_id, buf));

    // Syntax : Router ID 1.2.3.5, Cost: 1
    ICLI_PRINTF("  Router ID %s, Cost: %d\n",
                misc_ipv4_txt(entry.router_id, buf), entry.cost);

    // Syntax : Transmit Delay is 1 sec, State BDR, Priority 1
    ICLI_PRINTF("  Transmit Delay is %d sec", entry.transmit_delay);
    ICLI_PRINTF(", State ");
    if (entry.status) {
        switch (entry.state) {
        case VTSS_APPL_OSPF_INTERFACE_DR_OTHER:
            ICLI_PRINTF("DROther");
            break;
        case VTSS_APPL_OSPF_INTERFACE_BDR:
            ICLI_PRINTF("BDR");
            break;
        case VTSS_APPL_OSPF_INTERFACE_DR:
            ICLI_PRINTF("DR");
            break;
        case VTSS_APPL_OSPF_INTERFACE_POINT2POINT:
            ICLI_PRINTF("POINT_TO_POINT");
            break;
        case VTSS_APPL_OSPF_INTERFACE_DOWN:
            ICLI_PRINTF("DOWN");
            break;
        case VTSS_APPL_OSPF_INTERFACE_WAITING:
            ICLI_PRINTF("WAITING");
            break;
        default:
            ICLI_PRINTF("Unknown");
            break;
        }
    } else {
        ICLI_PRINTF("DOWN");
    }
    ICLI_PRINTF(", Priority %d\n", entry.priority);

    // Syntax : Designated Router (ID) 61.0.0.86, Interface address
    // 60.0.0.86
    if (entry.dr_id == 0) {
        ICLI_PRINTF("  No designated router on this network\n");
    } else {
        ICLI_PRINTF("  Designated Router (ID) %s",
                    misc_ipv4_txt(entry.dr_id, buf));
        ICLI_PRINTF(", Interface address %s\n",
                    misc_ipv4_txt(entry.dr_addr, buf));
    }

    // Syntax : Backup Designated router (ID) 1.2.3.5, Interface address
    // 60.0.0.89
    if (entry.bdr_addr == 0) {
        ICLI_PRINTF("  No backup designated router on this network\n");
    } else {
        ICLI_PRINTF("  Backup Designated router (ID) %s",
                    misc_ipv4_txt(entry.bdr_id, buf));
        ICLI_PRINTF(", Interface address %s\n",
                    misc_ipv4_txt(entry.bdr_addr, buf));
    }

    // Syntax : Timer intervals configured, Hello 10s, Dead 40s, Wait 40s,
    // Retransmit 5
    ICLI_PRINTF(
            "  Timer intervals configured, Hello %d, Dead %d, Wait %d, "
            "Retransmit %d\n",
            entry.hello_time, entry.dead_time, entry.dead_time,
            entry.retransmit_time);

    // Syntax :   Hello due in 8.685s
    if (entry.is_passive) {
        ICLI_PRINTF("    No Hellos (Passive interface)\n");
    } else {
        ICLI_PRINTF("    Hello due in %s\n", misc_time_txt(entry.hello_due_time));
    }

    // Syntax : Neighbor Count is 1, Adjacent neighbor count is 1
    ICLI_PRINTF("  Neighbor Count is %d, Adjacent neighbor count is %d\n",
                entry.neighbor_count, entry.adj_neighbor_count);

    return ICLI_RC_OK;
}

/* Display all entries of OSPF interface status */
static mesa_rc FRR_ICLI_ospf_interface_print_all(u32 session_id) {
    vtss_ifindex_t ifindex;
    vtss_appl_ospf_interface_status_t entry;
    char buf[128];
    vtss::Map<vtss_ifindex_t, vtss_appl_ospf_interface_status_t> interface;
    int cnt = 0;

    mesa_rc rc = vtss_appl_ospf_interface_status_get_all(interface);
    if (rc != VTSS_RC_OK) {
        return rc;
    }

    cnt = 0;
    vtss::Map<vtss_ifindex_t, vtss_appl_ospf_interface_status_t>::iterator itr;
    for (itr = interface.begin(); itr != interface.end(); ++itr) {
        ifindex = itr->first;
        entry = itr->second;

        if (cnt != 0) {
            ICLI_PRINTF("\n");
        }
        cnt++;

        // Syntax : Vlan60 is up
        ICLI_PRINTF("%s is %s\n", vtss_ifindex2str(buf, sizeof(buf), ifindex),
                    entry.status ? "up" : "down");

        // Syntax : Internet Address 60.0.0.89/8, Area 0
        ICLI_PRINTF("  Internet Address %s/%d",
                    misc_ipv4_txt(entry.network.address, buf),
                    entry.network.prefix_size);
        ICLI_PRINTF(", Area %s\n", misc_ipv4_txt(entry.area_id, buf));

        // Syntax : Router ID 1.2.3.5, Cost: 1
        ICLI_PRINTF("  Router ID %s, Cost: %d\n",
                    misc_ipv4_txt(entry.router_id, buf), entry.cost);

        // Syntax : Transmit Delay is 1 sec, State BDR, Priority 1
        ICLI_PRINTF("  Transmit Delay is %d sec", entry.transmit_delay);
        ICLI_PRINTF(", State ");
        if (entry.status) {
            switch (entry.state) {
            case VTSS_APPL_OSPF_INTERFACE_DR_OTHER:
                ICLI_PRINTF("DROther");
                break;
            case VTSS_APPL_OSPF_INTERFACE_BDR:
                ICLI_PRINTF("BDR");
                break;
            case VTSS_APPL_OSPF_INTERFACE_DR:
                ICLI_PRINTF("DR");
                break;
            case VTSS_APPL_OSPF_INTERFACE_POINT2POINT:
                ICLI_PRINTF("POINT_TO_POINT");
                break;
            case VTSS_APPL_OSPF_INTERFACE_DOWN:
                ICLI_PRINTF("DOWN");
                break;
            case VTSS_APPL_OSPF_INTERFACE_WAITING:
                ICLI_PRINTF("WAITING");
                break;
            default:
                ICLI_PRINTF("Unknown");
                break;
            }
        } else {
            ICLI_PRINTF("DOWN");
        }
        ICLI_PRINTF(", Priority %d\n", entry.priority);

        // Syntax : Designated Router (ID) 61.0.0.86, Interface address
        // 60.0.0.86
        if (entry.dr_id == 0) {
            ICLI_PRINTF("  No designated router on this network\n");
        } else {
            ICLI_PRINTF("  Designated Router (ID) %s",
                        misc_ipv4_txt(entry.dr_id, buf));
            ICLI_PRINTF(", Interface address %s\n",
                        misc_ipv4_txt(entry.dr_addr, buf));
        }

        // Syntax : Backup Designated router (ID) 1.2.3.5, Interface address
        // 60.0.0.89
        if (entry.bdr_addr == 0) {
            ICLI_PRINTF("  No backup designated router on this network\n");
        } else {
            ICLI_PRINTF("  Backup Designated router (ID) %s",
                        misc_ipv4_txt(entry.bdr_id, buf));
            ICLI_PRINTF(", Interface address %s\n",
                        misc_ipv4_txt(entry.bdr_addr, buf));
        }

        // Syntax : Timer intervals configured, Hello 10s, Dead 40s, Wait 40s,
        // Retransmit 5
        ICLI_PRINTF(
                "  Timer intervals configured, Hello %d, Dead %d, Wait %d, "
                "Retransmit %d\n",
                entry.hello_time, entry.dead_time, entry.dead_time,
                entry.retransmit_time);

        // Syntax :   Hello due in 8.685s
        if (entry.is_passive) {
            ICLI_PRINTF("    No Hellos (Passive interface)\n");
        } else {
            ICLI_PRINTF("    Hello due in %s\n",
                        misc_time_txt(entry.hello_due_time));
        }

        // Syntax : Neighbor Count is 1, Adjacent neighbor count is 1
        ICLI_PRINTF("  Neighbor Count is %d, Adjacent neighbor count is %d\n",
                    entry.neighbor_count, entry.adj_neighbor_count);
    }

    return ICLI_RC_OK;
}

/* Show OSPF interface information */
mesa_rc FRR_ICLI_ospf_show_interface(const FrrIcliReq &req) {
    mesa_rc rc;
    u32 session_id = req.session_id;
    vtss_ifindex_t ifindex;
    bool first_valid_entry = false;

    /* Is it 'show all interfaces' command? */
    if (!req.vlan_list) {
        (void)FRR_ICLI_ospf_interface_print_all(req.session_id);
        return ICLI_RC_OK;
    }

    /* Otherwise, show the specific interfaces */
    // Iterate through all vlan IDs.
    for (u32 idx = 0; idx < req.vlan_list->cnt; ++idx) {
        for (mesa_vid_t vid = req.vlan_list->range[idx].min;
             vid <= req.vlan_list->range[idx].max; ++vid) {
            if (vtss_ifindex_from_vlan(vid, &ifindex) != VTSS_OK) {
                continue;
            }

            rc = FRR_ICLI_ospf_interface_print(req.session_id, ifindex,
                                               &first_valid_entry);
            if (rc == VTSS_APPL_FRR_OSPF_ERROR_VLAN_INTF_NOT_EXIST) {
                ICLI_PRINTF(" %% VLAN interface %d does not exist.\n", vid);
            } else if (rc == VTSS_APPL_FRR_ERROR_ENTRY_NOT_FOUND) {
                ICLI_PRINTF(" %% OSPF not enabled on VLAN interface %d\n", vid);
            }
        }
    }
    return ICLI_RC_OK;
}

/* Show OSPF neighbor information */
mesa_rc FRR_ICLI_ospf_show_neighbor(const FrrIcliReq &req) {
    vtss_appl_ospf_neighbor_status_t entry;
    char buf[128];
    u32 session_id = req.session_id; /* used for ICLI_PRINTF */
    StringStream str_buf;
    bool first_entry = true;
    bool is_vlink = false;
    vtss::Vector<vtss_appl_ospf_neighbor_data_t> neighbors;

    // return silently
    if (vtss_appl_ospf_neighbor_status_get_all(neighbors) != VTSS_RC_OK) {
        return VTSS_RC_OK;
    }

    for (const auto &itr_status : neighbors) {
        entry = itr_status.status;
        is_vlink = vtss_ifindex_is_frr_vlink(entry.ifindex);

        if (!req.has_detail) {
            if (first_entry) {
                ICLI_PRINTF("%-15s  %-3s  %-18s  %-10s  %-15s  %s",
                            "Neighbor ID", "Pri", "State", "Dead Time",
                            "Address", "Interface\n");
                /* test maximum value display */
                /* ICLI_PRINTF("%-15s  %-3d  %-18s  %-10s  %-15s  %s",
                            "255.255.255.255", 255, "DEPENDUPON/DROTHER",
                   "120.123sec",
                            "255.255.255.255", "OSPF-VLINK 4096\n"); */
                first_entry = false;
            }

            ICLI_PRINTF("%-15s  %-3d", misc_ipv4_txt(entry.neighbor_id, buf),
                        entry.priority);

            str_buf.clear();
            switch (entry.state) {
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_DEPENDUPON:
                str_buf << "DEPENDUPON";
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_DELETED:
                str_buf << "DELETED";
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_DOWN:
                str_buf << "DOWN";
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_ATTEMPT:
                str_buf << "ATTEMPT";
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_INIT:
                str_buf << "INIT";
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_2WAY:
                str_buf << "2WAY";
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_EXSTART:
                str_buf << "EXSTART";
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_EXCHANGE:
                str_buf << "EXCHANGE";
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_LOADING:
                str_buf << "LOADING";
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_FULL:
                str_buf << "FULL";
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_UNKNOWN:
                str_buf << "UNKNOWN";
                break;
            }
            if (is_vlink) {
                str_buf << "/-";
            } else if (entry.ip_addr == entry.dr_addr) {
                str_buf << "/DR";
            } else if (entry.ip_addr == entry.bdr_addr) {
                str_buf << "/BDR";
            } else {
                str_buf << "/DROTHER";
            }
            ICLI_PRINTF("  %-18s", str_buf.cstring());

            str_buf.clear();
            if (is_vlink) {
                str_buf << "-";
            } else {
                str_buf << (entry.dead_time / 1000) << "."
                        << (entry.dead_time % 1000) << "sec";
            }
            ICLI_PRINTF("  %-10s", str_buf.cstring());

            str_buf.clear();
            str_buf << entry.ifindex;
            ICLI_PRINTF("  %-15s  %s\n", misc_ipv4_txt(entry.ip_addr, buf),
                        str_buf.cstring());
        } else {
            // Syntax : Neighbor 61.0.0.86, interface address 60.0.0.86
            ICLI_PRINTF("Neighbor %s", misc_ipv4_txt(entry.neighbor_id, buf));
            ICLI_PRINTF(", interface address %s\n",
                        misc_ipv4_txt(entry.ip_addr, buf));

            // Syntax :     In the area 0.0.0.0 via interface Vlan60
            str_buf.clear();
            if (is_vlink) {
                str_buf << "    In the transit area " << AsIpv4(entry.transit_id);
            } else {
                str_buf << "    In the area " << AsIpv4(entry.area_id);
            }
            str_buf << " via interface " << entry.ifindex;
            ICLI_PRINTF("%s\n", str_buf.cstring());

            // Syntax :     Neighbor priority is 1, State is FULL
            switch (entry.state) {
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_DEPENDUPON:
                ICLI_PRINTF(
                        "    Neighbor priority is %d, State is DEPENDUPON\n",
                        entry.priority);
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_DELETED:
                ICLI_PRINTF("    Neighbor priority is %d, State is DELETED\n",
                            entry.priority);
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_DOWN:
                ICLI_PRINTF("    Neighbor priority is %d, State is DOWN\n",
                            entry.priority);
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_ATTEMPT:
                ICLI_PRINTF("    Neighbor priority is %d, State is ATTEMPT\n",
                            entry.priority);
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_INIT:
                ICLI_PRINTF("    Neighbor priority is %d, State is INIT\n",
                            entry.priority);
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_2WAY:
                ICLI_PRINTF("    Neighbor priority is %d, State is 2WAY\n",
                            entry.priority);
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_EXSTART:
                ICLI_PRINTF("    Neighbor priority is %d, State is EXSTART\n",
                            entry.priority);
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_EXCHANGE:
                ICLI_PRINTF("    Neighbor priority is %d, State is EXCHANGE\n",
                            entry.priority);
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_LOADING:
                ICLI_PRINTF("    Neighbor priority is %d, State is LOADING\n",
                            entry.priority);
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_FULL:
                ICLI_PRINTF("    Neighbor priority is %d, State is FULL\n",
                            entry.priority);
                break;
            case VTSS_APPL_OSPF_NEIGHBOR_STATE_UNKNOWN:
                ICLI_PRINTF("    Neighbor priority is %d, State is UNKNOWN\n",
                            entry.priority);
                break;
            }

            // Syntax :     DR ID is 60.0.0.86, DR address is 60.0.0.86
            ICLI_PRINTF("    DR ID is %s", misc_ipv4_txt(entry.dr_id, buf));
            ICLI_PRINTF(", DR address is %s\n",
                        misc_ipv4_txt(entry.dr_addr, buf));

            // Syntax :     BDR ID is 60.0.0.89, BDR address is 60.0.0.89
            ICLI_PRINTF("    BDR ID is %s", misc_ipv4_txt(entry.bdr_id, buf));
            ICLI_PRINTF(", BDR address is %s\n",
                        misc_ipv4_txt(entry.bdr_addr, buf));
            // OSPF Option Bits
            // +---+---+---+---+---+---+---+---+
            // |DN |O  |DC |EA |NP |MC |E  |MT |
            // +---+---+---+---+---+---+---+---+
            // Syntax :     Options 18 *|-|-|EA|-|-|E|-
            ICLI_PRINTF(
                    "    Options %d %s|%s|%s|%s|%s|%s|%s|%s\n", entry.options,
                    /* bit 7(DN) is unsed. Use * to represent it */
                    "*",
                    (entry.options & VTSS_APPL_OSPF_OPTION_FIELD_O) ? "O" : "-",
                    (entry.options & VTSS_APPL_OSPF_OPTION_FIELD_DC) ? "DC" : "-",
                    (entry.options & VTSS_APPL_OSPF_OPTION_FIELD_EA) ? "EA" : "-",
                    (entry.options & VTSS_APPL_OSPF_OPTION_FIELD_NP) ? "NP" : "-",
                    (entry.options & VTSS_APPL_OSPF_OPTION_FIELD_MC) ? "MC" : "-",
                    (entry.options & VTSS_APPL_OSPF_OPTION_FIELD_E) ? "E" : "-",
                    (entry.options & VTSS_APPL_OSPF_OPTION_FIELD_MT) ? "MT"
                                                                     : "-");

            // Syntax :     Dead timer due in 35 sec
            if (!is_vlink)
                ICLI_PRINTF("    Dead timer due in %d.%03d sec\n",
                            entry.dead_time / 1000, entry.dead_time % 1000);
            ICLI_PRINTF("\n");
        }
    }

    return VTSS_RC_OK;
}
