/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifdef VTSS_BASICS_STANDALONE
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-variable"
#endif

#include "frr_access.hxx"
#include <net/if.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <initializer_list>
#include <vtss/basics/algorithm.hxx>
#include <vtss/basics/expose/json.hxx>
#include <vtss/basics/expose/json/enum-macros.hxx>
#include <vtss/basics/fd.hxx>
#include <vtss/basics/notifications/process-daemon.hxx>
#include <vtss/basics/notifications/subject-runner.hxx>
#include <vtss/basics/parse_group.hxx>
#include <vtss/basics/parser_impl.hxx>
#include <vtss/basics/string-utils.hxx>
#include <vtss/basics/time.hxx>
#include <vtss/basics/trace.hxx>
#include <vtss/basics/vector.hxx>
#ifdef VTSS_BASICS_STANDALONE
#include <signal.h>
#else
#include <vtss_timer_api.h>
#include "icli_api.h"
#include "subject.hxx"
#endif

/****************************************************************************/
/** Module default trace group declaration                                  */
/****************************************************************************/
/* All traces that does not explictly specify a group, will use the
 * TRACE_FRR_GRP_FW group.
 *
 * Notice that the definition 'VTSS_TRACE_DEFAULT_GROUP' must declared before
 * adding "frr_trace.hxx" in the include header.
 */
#ifndef VTSS_BASICS_STANDALONE
#define VTSS_TRACE_DEFAULT_GROUP TRACE_FRR_GRP_FW
#include "frr_trace.hxx"  // For module trace group definitions
#endif

vtss_enum_descriptor_t protocols_txt[]{{vtss::Route_Kernel, "kernel"},
                                       {vtss::Route_Connected, "connected"},
                                       {vtss::Route_Static, "static"},
                                       {vtss::Route_Ospf, "ospf"},
                                       {}};
VTSS_JSON_SERIALIZE_ENUM(FrrRouteProtocol_t, "protocol", protocols_txt, "-");

vtss_enum_descriptor_t ip_ospf_area_authentication_txt[]{
        {vtss::Authentication_None, "authenticationNone"},
        {vtss::Authentication_SimplePassword, "authenticationSimplePassword"},
        {vtss::Authentication_MessageDigest, "authenticationMessageDigest"},
        {}};
VTSS_JSON_SERIALIZE_ENUM(FrrIpOspfAreaAuthentication, "authentication",
                         ip_ospf_area_authentication_txt, "-");

vtss_enum_descriptor_t ip_ospf_neighbor_nsm_state_txt[]{
        {vtss::NSM_DependUpon, "DependUpon"},
        {vtss::NSM_Deleted, "Deleted"},
        {vtss::NSM_Down, "Down"},
        {vtss::NSM_Attempt, "Attempt"},
        {vtss::NSM_Init, "Init"},
        {vtss::NSM_TwoWay, "2-Way"},
        {vtss::NSM_ExStart, "ExStart"},
        {vtss::NSM_Exchange, "Exchange"},
        {vtss::NSM_Loading, "Loading"},
        {vtss::NSM_Full, "Full"},
        {}};
VTSS_JSON_SERIALIZE_ENUM(FrrIpOspfNeighborNSMState, "nbrState",
                         ip_ospf_neighbor_nsm_state_txt, "-");

vtss_enum_descriptor_t ip_ospf_interface_network_type_txt[]{
        {vtss::NetworkType_Null, "Null"},
        {vtss::NetworkType_PointToPoint, "POINTOPOINT"},
        {vtss::NetworkType_Broadcast, "BROADCAST"},
        {vtss::NetworkType_Nbma, "NBMA"},
        {vtss::NetworkType_PointToMultipoint, "POINTTOMULTIPOINT"},
        {vtss::NetworkType_VirtualLink, "VIRTUALLINK"},
        {vtss::NetworkType_LoopBack, "LOOPBACK"},
        {}};
VTSS_JSON_SERIALIZE_ENUM(FrrIpOspfIfNetworkType, "networkType",
                         ip_ospf_interface_network_type_txt, "-");

vtss_enum_descriptor_t ip_ospf_interface_ism_state_txt[]{
        {vtss::ISM_DependUpon, "DependUpon"},
        {vtss::ISM_Down, "Down"},
        {vtss::ISM_Loopback, "LoopBack"},
        {vtss::ISM_Waiting, "Waiting"},
        {vtss::ISM_PointToPoint, "Point-To-Point"},
        {vtss::ISM_DROther, "DROther"},
        {vtss::ISM_Backup, "Backup"},
        {vtss::ISM_DR, "DR"},
        {}};
VTSS_JSON_SERIALIZE_ENUM(FrrIpOspfIfISMState, "state",
                         ip_ospf_interface_ism_state_txt, "-");

vtss_enum_descriptor_t ip_ospf_interface_type_txt[]{
        {vtss::IfType_Peer, "Peer"}, {vtss::IfType_Broadcast, "Broadcast"}, {}};
VTSS_JSON_SERIALIZE_ENUM(FrrIpOspfIfType, "ospfIfType",
                         ip_ospf_interface_type_txt, "-");

namespace vtss {
#ifdef VTSS_BASICS_STANDALONE
namespace notifications {
::vtss::notifications::SubjectRunner subject_main_thread("main", "main");
}  // namespace notifications
#endif

namespace {

#ifndef VTSS_BASICS_STANDALONE
static critd_t FRR_access_crit;
struct LockAccess {
    LockAccess(int line) {
        critd_enter(&FRR_access_crit, TRACE_FRR_GRP_CRIT, VTSS_TRACE_LVL_NOISE,
                    __FILE__, line);
    }
    ~LockAccess() {
        critd_exit(&FRR_access_crit, TRACE_FRR_GRP_CRIT, VTSS_TRACE_LVL_NOISE,
                   __FILE__, 0);
    }
};

#define CRIT_SCOPE() LockAccess __lock_guard__(__LINE__)
#else
#define CRIT_SCOPE()
#endif

struct FrrDaemonState {
    bool is_prepared = {false};
    bool is_zebra_started = {false};
    bool is_ospfd_started = {false};
};
FrrDaemonState frr_daemon_state;

vtss::notifications::ProcessDaemon process_zebra(
        &vtss::notifications::subject_main_thread, "zebra");
vtss::notifications::ProcessDaemon process_ospfd(
        &vtss::notifications::subject_main_thread, "ospfd");

const char *zebra_socket = "/tmp/zebra.socket";
const char *zebra_vty = "/tmp/zebra.vty";
const char *ospfd_vty = "/tmp/ospfd.vty";
const char *zebra_bin = "/usr/sbin/zebra";
const char *ospfd_bin = "/usr/sbin/ospfd";

static FrrConf cached_running_config;

void clear_cached_running_config() {
    CRIT_SCOPE();
    cached_running_config.zebra = "";
    cached_running_config.ospfd = "";
}

int create_socket(const char *socket_name) {
    int s = socket(AF_UNIX, SOCK_STREAM, 0);
    if (s == -1) {
        return s;
    }

    struct sockaddr_un saddr;
    memset(&saddr, 0, sizeof(saddr));
    saddr.sun_family = AF_UNIX;
    strncpy(saddr.sun_path, socket_name, sizeof(saddr.sun_path));

    if (connect(s, (struct sockaddr *)&saddr, sizeof(saddr)) < 0) {
        close(s);
        return -1;
    }
    return s;
}

ssize_t send_socket(int socket, const char *buff, size_t len) {
    size_t transmitted = 0;
    while (transmitted < len) {
        size_t to_transmit = len < 4096 ? len : 4096;
        ssize_t tmp = send(socket, &buff[transmitted], to_transmit, 0);
        transmitted += tmp;
    }
    return transmitted;
}

ssize_t recv_socket(int socket, void *buff, size_t len) {
    return recv(socket, buff, len, 0);
}

inline void copy_file(const char *src, const char *dst) {
    auto buf = vtss::read_file_into_buf(src);
    vtss::write_buf_into_file(buf.begin(), buf.size(), dst);
}

inline ssize_t write_to_file(const char *filename, const char *data, size_t size) {
    return vtss::write_buf_into_file(data, size, filename);
}

mesa_rc zebra_prepare() {
    if (!frr_daemon_state.is_prepared) {
        write_to_file("/proc/sys/net/ipv4/igmp_max_memberships", "200", 3);
        copy_file("/etc/quagga/ospfd.conf", "/tmp/ospfd.conf");
        frr_daemon_state.is_prepared = true;
        return MESA_RC_OK;
    }
    return MESA_RC_ERROR;
}

inline bool check_file_exists(const char *filename) {
    struct stat buffer;
    return stat(filename, &buffer) == 0;
}

inline int get_pid_value(const char *filename) {
    auto buffer = vtss::read_file_into_buf(filename);
    auto result = std::atoi(buffer.begin());
    return result == 0 ? -1 : result;
}

inline bool check_pid_exists(int pid_id) { return ::kill(pid_id, 0) == 0; }

inline void remove_file(const char *filename) { ::remove(filename); }

inline void kill_pid(int pid_id) { ::kill(pid_id, 9); }

inline void start_daemon(vtss::notifications::ProcessDaemon &process,
                         const std::string &executable,
                         const std::initializer_list<std::string> &args) {
    process.executable = executable;
    process.arguments = vtss::Vector<std::string>(args);
    process.kill_policy(true);
    process.adminMode(vtss::notifications::ProcessDaemon::ENABLE);
}

template <typename F, typename... Args>
bool check_condition(vtss::seconds delay, F f, Args... args) {
    static_assert(std::is_same<std::result_of_t<F(Args...)>, bool>::value == true,
                  "Function doesn't return a bool");
    auto timeout = vtss::LinuxClock::now() + vtss::LinuxClock::to_time_t(delay);
    while (timeout > vtss::LinuxClock::now()) {
        if (f(args...)) return true;
        vtss::LinuxClock::sleep(vtss::milliseconds{200});
    }
    return false;
}

inline void stop_process(vtss::notifications::ProcessDaemon &process,
                         const char *name) {
    process.stop();
    if (process.status().alive()) {
        process.stop(true);
    }
    vtss::BufStream<vtss::SBuf32> buf;
    buf << "/tmp/" << name << ".pid";
    if (check_file_exists(buf.cstring())) {
        auto pid_id = get_pid_value(buf.cstring());
        kill_pid(pid_id);
        for (const char *extension : {".pid", ".log", ".socket", ".vty"}) {
            buf.clear();
            buf << "/tmp/" << name << extension;
            remove_file(buf.cstring());
        }
    }
}

bool end_of_message(const std::string &msg) {
    // checks the bytes 2,3 and 4 from end to be 0, first byte from end is the
    // return code
    if (msg.size() < 4) {
        return false;
    }
    return vtss::equal(msg.rbegin() + 1, msg.rbegin() + 4, "\0\0\0");
}

std::string frr_daemon_cmd_impl(int socket, const char *cmd) {
    static constexpr int buff_size = 4096;
    std::string result;

    VTSS_TRACE(DEBUG) << "Send command: " << cmd;
    send_socket(socket, cmd, strlen(cmd) + 1);
    while (true) {
        char buf[buff_size];
        memset(buf, '\0', buff_size);
        int bytes = recv_socket(socket, buf, buff_size - 1);
        if (bytes < 0) {
            VTSS_TRACE(ERROR) << "Socket error: " << cmd;
            return std::string();
        }
#ifndef VTSS_BASICS_STANDALONE
        T_D_HEX((unsigned char *)buf, bytes);
#endif
        VTSS_TRACE(DEBUG) << "Received bytes: " << bytes;
        VTSS_TRACE(DEBUG) << "Buff: " << buf;
        result.append(buf, bytes);

        if (end_of_message(result)) {
            break;
        }
    }
    // removes the end of message part
    result.erase(result.size() - 4, 4);
    return result;
}

// Obs - all commands in cmds are expected to be null-terminated
mesa_rc frr_daemon_cmd(const char *vty_path, vtss::Vector<std::string> cmds,
                       std::string &result) {
    vtss::Fd socket = create_socket(vty_path);
    if (socket.raw() == -1) {
        return MESA_RC_ERROR;
    }
    frr_daemon_cmd_impl(socket.raw(), "enable\0");
    std::string buff;
    for (auto &cmd : cmds) {
        auto tmp = frr_daemon_cmd_impl(socket.raw(), cmd.c_str());
        if (!tmp.empty()) {
            buff += tmp;
        }
    }
    result = std::move(buff);
    return MESA_RC_OK;
}

vtss::Vector<std::string> split_cmd(const char *cmds) {
    vtss::Vector<std::string> vec_cmds;
    char *saveptr;  // Local strtok_r() context
    char *token = strtok_r((char *)cmds, ";", &saveptr);
    while (token != NULL) {
        vec_cmds.push_back(std::string(token));
        token = strtok_r(NULL, ";", &saveptr);
    }
    return vec_cmds;
}

}  // namespace


void initialize_critd() {
#ifndef VTSS_BASICS_STANDALONE
    critd_init(&FRR_access_crit, "frr_access.crit", VTSS_MODULE_ID_FRR,
               VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
    critd_exit(&FRR_access_crit, TRACE_FRR_GRP_CRIT, VTSS_TRACE_LVL_NOISE,
               __FILE__, __LINE__);
#endif
}

// before starting a daemon, check if the pid file exists, if it does, read the
// pid from it, see if that pid actual exists. If it does return error, if not
// delete the pid and start
mesa_rc zebra_start() {
    if (frr_daemon_state.is_zebra_started || !frr_has_zebra()) {
        return MESA_RC_ERROR;
    }

    if (!frr_daemon_state.is_prepared) {
        zebra_prepare();
    }

    if (check_file_exists("/tmp/zebra.pid")) {
        auto pid_id = get_pid_value("/tmp/zebra.pid");
        if (check_pid_exists(pid_id)) {
            frr_daemon_state.is_zebra_started = true;
            return MESA_RC_ERROR;
        } else {
            remove_file("/tmp/zebra.pid");
        }
    }
    start_daemon(process_zebra, zebra_bin,
                 {"-f", "/etc/quagga/zebra.conf", "-i", "/tmp/zebra.pid", "-P",
                  "0", "-z", zebra_socket});
    if (check_condition(vtss::seconds{5}, check_file_exists, "/tmp/zebra.pid"))
        return MESA_RC_ERROR;

    frr_daemon_state.is_zebra_started = true;
    return MESA_RC_OK;
}

mesa_rc ospfd_start(bool clean) {
    if (frr_daemon_state.is_ospfd_started || !frr_has_ospfd()) {
        return MESA_RC_ERROR;
    }

    if (!frr_daemon_state.is_prepared) {
        zebra_prepare();
    }

    if (!frr_daemon_state.is_zebra_started) {
        zebra_start();
    }

    if (clean) copy_file("/etc/quagga/ospfd.conf", "/tmp/ospfd.conf");

    if (check_file_exists("/tmp/ospfd.pid")) {
        auto pid_id = get_pid_value("/tmp/ospfd.pid");
        if (check_pid_exists(pid_id)) {
            frr_daemon_state.is_ospfd_started = true;
            return MESA_RC_ERROR;
        } else {
            remove_file("/tmp/ospfd.pid");
        }
    }

    start_daemon(process_ospfd, ospfd_bin,
                 {"-f", "/tmp/ospfd.conf", "-i", "/tmp/ospfd.pid", "-P", "0",
                  "-z", zebra_socket});
    if (!check_condition(vtss::seconds{5}, check_file_exists, "/tmp/ospfd.pid"))
        return MESA_RC_ERROR;

    frr_daemon_state.is_ospfd_started = true;
    return MESA_RC_OK;
}

/* NOTICE:
 *   The OSPF cache is clear when OSPF daemon is not running since there is no
 *   any configuration saving in the deamon process.
 */
mesa_rc ospfd_stop() {
    process_ospfd.adminMode(vtss::notifications::ProcessDaemon::DISABLE);
    stop_process(process_ospfd, "ospfd");
    frr_daemon_state.is_ospfd_started = false;
    // clear cache after stopping ospf process.
    clear_cached_running_config();
    return MESA_RC_OK;
}

mesa_rc frr_stop_all() {
    process_ospfd.adminMode(vtss::notifications::ProcessDaemon::DISABLE);
    stop_process(process_ospfd, "ospfd");
    frr_daemon_state.is_ospfd_started = false;
    stop_process(process_zebra, "zebra");
    frr_daemon_state.is_zebra_started = false;
    return MESA_RC_OK;
}

bool frr_has_zebra() { return check_file_exists(zebra_bin); }

bool frr_has_ospfd() { return check_file_exists(ospfd_bin); }

bool is_frr_ospfd_started() { return frr_daemon_state.is_ospfd_started; }


#ifndef VTSS_BASICS_STANDALONE
std::string to_hex(const unsigned char *p, size_t size) {
    StringStream buffer;
    size_t i = 0;
    while (i < size) {
        size_t j = 0;
        buffer << p + i << "/" << hex_fixed<4>(i) << ": ";
        while (i + j < size && j < 16) {
            buffer << " ";
            buffer << hex_fixed<2>(p[i + j]);
            ++j;
        }
        buffer << "\n";
        i += 16;
    }
    return buffer.cstring();
}

mesa_rc frr_zebra_icli_cmd(const char *cmds, bool hex) {
    auto comands = split_cmd(cmds);

    std::string result;
    if (frr_daemon_cmd(zebra_vty, comands, result) != MESA_RC_OK)
        return MESA_RC_ERROR;

    (void)icli_session_self_printf("%s\n", result.c_str());
    if (hex) {
        std::string result_hex =
                to_hex((unsigned char *)result.c_str(), result.size());
        (void)icli_session_self_printf("%s\n", result_hex.c_str());
    }
    return MESA_RC_OK;
}

mesa_rc frr_ospfd_icli_cmd(const char *cmds, bool hex) {
    auto comands = split_cmd(cmds);

    std::string result;
    if (frr_daemon_cmd(ospfd_vty, comands, result) != MESA_RC_OK)
        return MESA_RC_ERROR;

    (void)icli_session_self_printf("%s\n", result.c_str());
    if (hex) {
        std::string result_hex =
                to_hex((unsigned char *)result.c_str(), result.size());
        (void)icli_session_self_printf("%s\n", result_hex.c_str());
    }
    return MESA_RC_OK;
}
#endif

Vector<std::string> to_vty_ip_route_conf_set(const FrrIpRoute &r) {
    Vector<std::string> res;
    StringStream buf;
    res.push_back("configure terminal");
    if (r.net.type == MESA_ROUTING_ENTRY_TYPE_IPV4_UC) {
        buf << "ip route " << r.net.route.ipv4_uc.network << " "
            << Ipv4Address(r.net.route.ipv4_uc.destination);
    }
    if (r.net.type == MESA_ROUTING_ENTRY_TYPE_IPV6_UC) {
        buf << "ipv6 route " << r.net.route.ipv6_uc.network << " "
            << Ipv6Address(r.net.route.ipv6_uc.destination);
    }
    if (r.distance != 1) {
        buf << " " << r.distance;
    }
    res.push_back(vtss::move(buf.buf));

    return res;
}

Vector<std::string> to_vty_ip_route_conf_del(const FrrIpRoute &r) {
    Vector<std::string> res;
    StringStream buf;
    res.push_back("configure terminal");
    if (r.net.type == MESA_ROUTING_ENTRY_TYPE_IPV4_UC) {
        buf << "no ip route " << r.net.route.ipv4_uc.network << " "
            << Ipv4Address(r.net.route.ipv4_uc.destination);
    }
    if (r.net.type == MESA_ROUTING_ENTRY_TYPE_IPV6_UC) {
        buf << "no ipv6 route " << r.net.route.ipv6_uc.network << " "
            << Ipv6Address(r.net.route.ipv6_uc.destination);
    }
    res.emplace_back(vtss::move(buf.buf));
    return res;
}

FrrRes<Vector<std::string>> to_vty_ip_route_status_get() {
    Vector<std::string> res;
    res.push_back("show ip route json");
    return res;
}

bool is_comment(str line) {
    parser::ZeroOrMoreSpaces spaces;
    parser::Lit comment("!");
    const char *b = &*line.begin();
    const char *e = line.end();
    return parser::Group(b, e, spaces, comment);
}

bool is_interface(str line) {
    parser::Lit interface("interface");
    const char *b = &*line.begin();
    const char *e = line.end();
    return parser::Group(b, e, interface);
}

bool is_router(str line) {
    parser::Lit router("router");
    const char *b = &*line.begin();
    const char *e = line.end();
    return parser::Group(b, e, router);
}

bool is_root(str line) {
    parser::OneOrMoreSpaces space;
    const char *b = &*line.begin();
    const char *e = line.end();
    return !parser::Group(b, e, space);
}

void frr_conf_parser(std::string conf, FrrConfStreamParserCB &cb) {
    static const std::string if_start = "interface";
    bool interface = false;
    bool router = false;
    std::string if_name = "";
    std::string router_name = "";
    for (str line : LineIterator(conf)) {
        if (is_comment(line)) {
            continue;
        }
        if (is_root(line)) {
            if (is_interface(line)) {
                str name = split(line, ' ')[1];
                if_name = std::string(name.begin(), name.end());
                interface = true;
                router = false;
                cb.interface(if_name, line);
                continue;
            } else if (is_router(line)) {
                str name = split(line, ' ')[1];
                router_name = std::string(name.begin(), name.end());
                interface = false;
                router = true;
                cb.router(router_name, line);
                continue;
            } else {
                cb.root(line);
            }
        }
        if (interface) {
            if (is_root(line)) {
                if_name = "";
                interface = false;
                router = false;
                continue;
            } else {
                cb.interface(if_name, line);
            }
        }
        if (router) {
            if (is_root(line)) {
                interface = false;
                router = false;
                continue;
            } else {
                cb.router(router_name, line);
            }
        }
    }
}

bool group_spaces(const str &line,
                  std::initializer_list<parser::ParserBase *> args,
                  std::initializer_list<parser::ParserBase *> optional = {}) {
    bool result = true;
    const char *b = &*line.begin();
    const char *c = &*line.end();
    parser::ZeroOrMoreSpaces space;
    for (auto *p : args) {
        // consume white spaces
        space(b, c);

        result &= (*p)(b, c);
        if (result == false) {
            return false;
        }
    }
    if (optional.size() == 0) return true;

    Vector<parser::ParserBase *> opt{optional};
    while (true) {
        bool tmp_result = false;
        Vector<parser::ParserBase *>::iterator i = opt.begin();
        while (i != opt.end()) {
            space(b, c);
            bool tmp = (**i)(b, c);
            tmp_result |= tmp;
            if (tmp) {
                opt.erase(i);
            } else {
                ++i;
            }
        }
        if (!tmp_result) break;
    }
    return true;
}

struct FrrParseIpRoute : public FrrConfStreamParserCB {
    bool parse_ipv4(const str &line) {
        parser::Lit lit_ip("ip");
        parser::Lit lit_rt("route");
        parser::Ipv4Network net;
        parser::IPv4 dst;
        parser::IntUnsignedBase10<uint32_t, 1, 10> distance(1);
        parser::TagValue<parser::IntUnsignedBase10<uint32_t, 1, 100>, int> tag(
                "tag", 0);
        if (group_spaces(line, {&lit_ip, &lit_rt, &net, &dst}, {&distance, &tag})) {
            FrrIpRoute r = {};
            r.net.route.ipv4_uc.network = net.get().as_api_type();
            r.net.route.ipv4_uc.destination = dst.get().as_api_type();
            r.net.type = MESA_ROUTING_ENTRY_TYPE_IPV4_UC;
            r.distance = distance.get();
            r.tag = tag.get().get();
            res.push_back(r);
            return true;
        }
        return false;
    }

    bool parse_ipv6(const str &line) {
        parser::Lit lit_ip("ipv6");
        parser::Lit lit_rt("route");
        parser::Ipv6Network net;
        parser::IPv6 dst;
        parser::IntUnsignedBase10<uint32_t, 1, 10> distance(1);
        parser::TagValue<parser::IntUnsignedBase10<uint32_t, 1, 100>, int> tag(
                "tag", 0);
        if (group_spaces(line, {&lit_ip, &lit_rt, &net, &dst}, {&distance, &tag})) {
            FrrIpRoute r = {};
            r.net.route.ipv6_uc.network = net.get().as_api_type();
            r.net.route.ipv6_uc.destination = dst.get().as_api_type();
            r.net.type = MESA_ROUTING_ENTRY_TYPE_IPV6_UC;
            r.distance = distance.get();
            r.tag = tag.get().get();
            res.push_back(r);
            return true;
        }
        return false;
    }

    void root(const str &line) override {
        if (parse_ipv4(line)) return;
        if (parse_ipv6(line)) return;
    }

    Vector<FrrIpRoute> res;
};

Vector<FrrIpRoute> frr_ip_route_conf_get(FrrConf &conf) {
    FrrParseIpRoute cb;
    frr_conf_parser(conf.zebra, cb);
    return move(cb.res);
}

mesa_rc frr_ip_route_conf_set(const FrrIpRoute &r) {
    auto cmds = to_vty_ip_route_conf_set(r);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(zebra_vty, cmds, vty_res);
}

mesa_rc frr_ip_route_conf_del(const FrrIpRoute &r) {
    auto cmds = to_vty_ip_route_conf_del(r);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(zebra_vty, cmds, vty_res);
}

#define CHECK_LOAD(X, error_label) \
    if (!X) {                      \
        goto error_label;          \
    }

#define ADD_LEAF_DEFAULT(X, TAG, default_value) \
    if (!m.add_leaf(X, TAG)) {                  \
        X = default_value;                      \
    }

template <typename KEY, typename VAL>
struct MapSortedCb {
    virtual void entry(const KEY &key, const VAL &val) {}
    virtual ~MapSortedCb() = default;
};

template <typename VAL>
void serialize(vtss::expose::json::Loader &l, Vector<VAL> &rez) {
    const char *b = l.pos_;

    CHECK_LOAD(l.load(vtss::expose::json::array_start), Error);
    while (true) {
        VAL v{};
        CHECK_LOAD(l.load(v), Error);
        rez.push_back(v);
        if (!l.load(vtss::expose::json::delimetor)) {
            l.reset_error();
            break;
        }
    }
    CHECK_LOAD(l.load(vtss::expose::json::array_end), Error);
    return;
Error:
    l.pos_ = b;
}

#ifndef VTSS_BASICS_STANDALONE
vtss_ifindex_t get_vtss_ifindex(int32_t os_ifindex) {
    if (os_ifindex == 0) {
        return vtss_ifindex_cast_from_u32(0, VTSS_IFINDEX_TYPE_NONE);
    }

    char buf[4096];
    if (if_indextoname(os_ifindex, buf) == NULL) {
        return vtss_ifindex_cast_from_u32(os_ifindex, VTSS_IFINDEX_TYPE_FOREIGN);
    }

    parser::Lit lit("vtss.vlan.");
    parser::IntUnsignedBase10<uint32_t, 1, 4096> vlan;
    const char *b = buf;
    const char *e = buf + 4096;
    if (!parser::Group(b, e, lit, vlan)) {
        return vtss_ifindex_cast_from_u32(os_ifindex, VTSS_IFINDEX_TYPE_FOREIGN);
    }

    vtss_ifindex_t vtss_ifindex;
    if (vtss_ifindex_from_vlan(vlan.get(), &vtss_ifindex) != MESA_RC_OK) {
        return vtss_ifindex_cast_from_u32(os_ifindex, VTSS_IFINDEX_TYPE_FOREIGN);
    }
    return vtss_ifindex;
}
vtss_ifindex_t get_vtss_ifindex_from_vlink(const str &vlink_name) {
    parser::Lit lit("VLINK");
    parser::IntUnsignedBase10<uint32_t, 1, 10> index;
    const char *b = &*vlink_name.begin();
    const char *e = &*vlink_name.end();
    if (parser::Group(b, e, lit, index)) {
        vtss_ifindex_t ifindex;
        vtss_ifindex_from_frr_vlink(index.get() + 1, &ifindex);
        return ifindex;
    }
    return vtss_ifindex_cast_from_u32(0, VTSS_IFINDEX_TYPE_NONE);
}
#else
vtss_ifindex_t get_vtss_ifindex(int32_t os_ifindex) {
    vtss_ifindex_t tmp = {0};
    return tmp;
}

vtss_ifindex_t get_vtss_ifindex_from_vlink(const str &vlink_name) {
    return vtss_ifindex_t{800000000};
}

bool ifindex_is_frr_vlink(vtss_ifindex_t) { return true; }
#endif

template <typename KEY, typename VAL>
void serialize(vtss::expose::json::Loader &l, MapSortedCb<KEY, VAL> &m) {
    const char *b = l.pos_;

    CHECK_LOAD(l.load(vtss::expose::json::map_start), Error);
    while (true) {
        KEY k{};
        CHECK_LOAD(l.load(k), Error);
        CHECK_LOAD(l.load(vtss::expose::json::map_assign), Error);
        VAL v{};
        CHECK_LOAD(l.load(v), Error);
        m.entry(k, v);
        if (!l.load(vtss::expose::json::delimetor)) {
            l.reset_error();
            break;
        }
    }
    CHECK_LOAD(l.load(vtss::expose::json::map_end), Error);
    return;
Error:
    l.pos_ = b;
}

template <typename VAL>
void serialize(vtss::expose::json::Loader &l, vtss::Map<mesa_ipv4_t, VAL> &m) {
    const char *b = l.pos_;
    CHECK_LOAD(l.load(vtss::expose::json::map_start), Error);
    while (true) {
        mesa_ipv4_t k;
        CHECK_LOAD(l.load(AsIpv4(k)), Error);
        CHECK_LOAD(l.load(vtss::expose::json::map_assign), Error);
        VAL v{};
        CHECK_LOAD(l.load(v), Error);
        m[k] = v;
        if (!l.load(vtss::expose::json::delimetor)) {
            l.reset_error();
            break;
        }
    }
    CHECK_LOAD(l.load(vtss::expose::json::map_end), Error);
    return;
Error:
    l.pos_ = b;
}

void serialize(vtss::expose::json::Loader &l, vtss::FrrRouteHopStatus &s) {
    vtss::expose::json::Loader::Map_t m =
            l.as_map(vtss::tag::Typename("vtss_frr_route_hop_status"));

    if (!m.add_leaf(AsIpv4(s.ip), vtss::tag::Name("ip"))) s.ip = 0;
    ADD_LEAF_DEFAULT(s.fib, vtss::tag::Name("fib"), false);
    ADD_LEAF_DEFAULT(s.directly_connected, vtss::tag::Name("directlyConnected"),
                     false);
    ADD_LEAF_DEFAULT(s.active, vtss::tag::Name("active"), false);
    ADD_LEAF_DEFAULT(s.os_ifindex, vtss::tag::Name("interfaceIndex"), 0);
    s.selected = false;
#ifndef VTSS_BASICS_STANDALONE
    s.interface_index = get_vtss_ifindex(s.os_ifindex);
#endif
}

seconds uptime_to_seconds(std::string &up_time) {
    const char *b = &*up_time.begin();
    const char *e = &*up_time.end();

    parser::Lit sep(":");
    parser::IntUnsignedBase10<uint32_t, 1, 2> h;
    parser::IntUnsignedBase10<uint32_t, 1, 2> m;
    parser::IntUnsignedBase10<uint32_t, 1, 2> s;
    if (parser::Group(b, e, h, sep, m, sep, s)) {
        return seconds(h.get() * 3600 + m.get() * 60 + s.get());
    }

    parser::Lit sep_d("d");
    parser::Lit sep_h("h");
    parser::IntUnsignedBase10<uint32_t, 1, 2> d;
    if (parser::Group(b, e, d, sep_d, h, sep_h, m)) {
        return seconds(d.get() * 86400 + h.get() * 3600 + m.get() * 60);
    }

    parser::Lit sep_w("wd");
    parser::IntUnsignedBase10<uint32_t, 1, 5> wd;
    if (parser::Group(b, e, wd, sep_w, d, sep_d, h)) {
        return seconds(wd.get() * 604800 + d.get() * 86400 + h.get() * 3600);
    }
    return seconds(0);
}

void serialize(vtss::expose::json::Loader &l, vtss::FrrRouteStatus &s) {
    vtss::expose::json::Loader::Map_t m =
            l.as_map(vtss::tag::Typename("vtss_frr_route_status"));

    m.add_leaf(s.prefix, vtss::tag::Name("prefix"));
    m.add_leaf(s.protocol, vtss::tag::Name("protocol"));
    ADD_LEAF_DEFAULT(s.selected, vtss::tag::Name("selected"), false);
    ADD_LEAF_DEFAULT(s.distance, vtss::tag::Name("distance"), 1);
    std::string up_time;
    if (m.add_leaf(up_time, vtss::tag::Name("uptime"))) {
        s.up_time = uptime_to_seconds(up_time);
    }
    ADD_LEAF_DEFAULT(s.metric, vtss::tag::Name("metric"), 0);
    m.add_leaf(s.next_hops, vtss::tag::Name("nexthops"));
    if (s.selected == true && !s.next_hops.empty()) {
        size_t i = 0;
        while (i < s.next_hops.size()) {
            if (s.next_hops[i].active == true) {
                s.next_hops[i].selected = true;
                break;
            }
            ++i;
        }
    }
}

struct RouteMapCb
        : public MapSortedCb<mesa_ipv4_network_t, Vector<FrrRouteStatus>> {
    void entry(const mesa_ipv4_network_t &key,
               const Vector<FrrRouteStatus> &val) override {
        FrrIpRouteStatus result;
        result.net = key;
        result.routes = val;
        res.push_back(result);
    }

    Vector<FrrIpRouteStatus> res;
};

Vector<FrrIpRouteStatus> frr_ip_route_status_parse(const std::string &s) {
    RouteMapCb r;
    vtss::expose::json::Loader loader(&*s.begin(), &*s.end());
    loader.patch_mode_ = true;
    loader.load(r);
    return std::move(r.res);
}

FrrRes<Vector<FrrIpRouteStatus>> frr_ip_route_status_get() {
    auto cmds = to_vty_ip_route_status_get();
    if (!cmds) return cmds.rc;
    std::string vty_res;
    mesa_rc res = frr_daemon_cmd(zebra_vty, cmds.val, vty_res);
    if (res != MESA_RC_OK) return res;
    return frr_ip_route_status_parse(vty_res);
}

// frr_ip_ospf_status_get ------------------------------------------------------
void serialize(vtss::expose::json::Loader &l, vtss::FrrIpOspfArea &s) {
    vtss::expose::json::Loader::Map_t m =
            l.as_map(vtss::tag::Typename("vtss_ip_ospf_area"));
    ADD_LEAF_DEFAULT(s.backbone, vtss::tag::Name("backbone"), false);
    ADD_LEAF_DEFAULT(s.stub_no_summary, vtss::tag::Name("stubNoSummary"), false);
    ADD_LEAF_DEFAULT(s.stub_shortcut, vtss::tag::Name("stubShortcut"), false);
    m.add_leaf(s.area_if_total_counter, vtss::tag::Name("areaIfTotalCounter"));
    m.add_leaf(s.area_if_activ_counter, vtss::tag::Name("areaIfActiveCounter"));
    m.add_leaf(s.full_adjancet_counter,
               vtss::tag::Name("nbrFullAdjacentCounter"));
    m.add_leaf(s.authentication, vtss::tag::Name("authentication"));
    m.add_leaf(s.spf_executed_counter, vtss::tag::Name("spfExecutedCounter"));
    m.add_leaf(s.lsa_nr, vtss::tag::Name("lsaNumber"));
    m.add_leaf(s.lsa_router_nr, vtss::tag::Name("lsaRouterNumber"));
    m.add_leaf(s.lsa_router_checksum, vtss::tag::Name("lsaRouterChecksum"));
    m.add_leaf(s.lsa_network_nr, vtss::tag::Name("lsaNetworkNumber"));
    m.add_leaf(s.lsa_network_checksum, vtss::tag::Name("lsaNetworkChecksum"));
    m.add_leaf(s.lsa_summary_nr, vtss::tag::Name("lsaSummaryNumber"));
    m.add_leaf(s.lsa_summary_checksum, vtss::tag::Name("lsaSummaryChecksum"));
    m.add_leaf(s.lsa_asbr_nr, vtss::tag::Name("lsaAsbrNumber"));
    m.add_leaf(s.lsa_asbr_checksum, vtss::tag::Name("lsaAsbrChecksum"));
    m.add_leaf(s.lsa_nssa_nr, vtss::tag::Name("lsaNssaNumber"));
    m.add_leaf(s.lsa_nssa_checksum, vtss::tag::Name("lsaNssaChecksum"));
    m.add_leaf(s.lsa_opaque_link_nr, vtss::tag::Name("lsaOpaqueLinkNumber"));
    m.add_leaf(s.lsa_opaque_link_checksum,
               vtss::tag::Name("lsaOpaqueLinkChecksum"));
    m.add_leaf(s.lsa_opaque_area_nr, vtss::tag::Name("lsaOpaqueAreaNumber"));
    m.add_leaf(s.lsa_opaque_area_checksum,
               vtss::tag::Name("lsaOpaqueAreaChecksum"));
}

void serialize(vtss::expose::json::Loader &l, vtss::FrrIpOspfStatus &s) {
    vtss::expose::json::Loader::Map_t m =
            l.as_map(vtss::tag::Typename("vtss_ip_ospf_status"));
    m.add_leaf(AsIpv4(s.router_id), vtss::tag::Name("routerId"));
    m.add_leaf(s.tos_routes_only, vtss::tag::Name("tosRoutesOnly"));
    m.add_leaf(s.rfc2328, vtss::tag::Name("rfc2328Conform"));
    m.add_leaf(s.spf_schedule_delay.raw(),
               vtss::tag::Name("spfScheduleDelayMsecs"));
    m.add_leaf(s.hold_time_min.raw(), vtss::tag::Name("holdtimeMinMsecs"));
    m.add_leaf(s.hold_time_max.raw(), vtss::tag::Name("holdtimeMaxMsecs"));
    m.add_leaf(s.hold_time_multiplier,
               vtss::tag::Name("holdtimeMultplier"));  // No typo
    m.add_leaf(s.spf_last_executed.raw(),
               vtss::tag::Name("spfLastExecutedMsecs"));
    m.add_leaf(s.spf_last_duration.raw(),
               vtss::tag::Name("spfLastDurationMsecs"));
    m.add_leaf(s.lsa_min_interval.raw(),
               vtss::tag::Name("lsaMinIntervalMsecs"));
    m.add_leaf(s.lsa_min_arrival.raw(), vtss::tag::Name("lsaMinArrivalMsecs"));
    m.add_leaf(s.write_multiplier, vtss::tag::Name("writeMultiplier"));
    m.add_leaf(s.refresh_timer.raw(), vtss::tag::Name("refreshTimerMsecs"));
    m.add_leaf(s.lsa_external_counter, vtss::tag::Name("lsaExternalCounter"));
    m.add_leaf(s.lsa_external_checksum, vtss::tag::Name("lsaExternalChecksum"));
    m.add_leaf(s.lsa_asopaque_counter,
               vtss::tag::Name("lsaAsopaqueCounter"));  // No typo
    m.add_leaf(s.lsa_asopaque_checksums,
               vtss::tag::Name("lsaAsOpaqueChecksum"));
    m.add_leaf(s.attached_area_counter, vtss::tag::Name("attachedAreaCounter"));
    m.add_leaf(s.areas, vtss::tag::Name("areas"));
}

FrrIpOspfStatus frr_ip_ospf_status_parse(const std::string &s) {
    FrrIpOspfStatus result;
    vtss::expose::json::Loader loader(&*s.begin(), &*s.end());
    loader.patch_mode_ = true;
    loader.load(result);

    VTSS_TRACE(INFO) << "s " << s;
    VTSS_TRACE(INFO) << "got " << result.areas.size() << " areas";
    VTSS_TRACE(INFO) << "lsa_min_interval = " << result.lsa_min_interval.raw();

    return result;
}

FrrRes<Vector<std::string>> to_vty_ip_ospf_status_get() {
    Vector<std::string> res;
    res.emplace_back("show ip ospf json");
    return res;
}

FrrRes<FrrIpOspfStatus> frr_ip_ospf_status_get() {
    auto cmds = to_vty_ip_ospf_status_get();
    if (!cmds) return cmds.rc;
    std::string vty_res;
    mesa_rc res = frr_daemon_cmd(ospfd_vty, cmds.val, vty_res);
    VTSS_TRACE(INFO) << "execute cmd " << res;
    if (res != MESA_RC_OK) return res;
    return frr_ip_ospf_status_parse(vty_res);
}

// frr_ip_ospf_interface_status_get---------------------------------------------
AreaDescription parse_area(const str &id) {
    parser::IPv4 ip_lit;
    parser::Lit stub("[Stub]");
    parser::Lit nssa("[NSSA]");

    if (group_spaces(id, {&ip_lit, &stub})) {
        return {ip_lit.get().as_api_type(), AreaDescriptionType_Stub};
    }
    if (group_spaces(id, {&ip_lit, &nssa})) {
        return {ip_lit.get().as_api_type(), AreaDescriptionType_Nssa};
    }
    if (group_spaces(id, {&ip_lit})) {
        return {ip_lit.get().as_api_type(), AreaDescriptionType_Default};
    }
    return {0, AreaDescriptionType_Default};
}

void serialize(vtss::expose::json::Loader &l, vtss::FrrIpOspfIfStatus &s) {
    vtss::expose::json::Loader::Map_t m =
            l.as_map(vtss::tag::Typename("vtss_ip_ospf_interface_status"));
    ADD_LEAF_DEFAULT(s.if_up, vtss::tag::Name("ifUp"), false);
    m.add_leaf(s.os_ifindex, vtss::tag::Name("ifIndex"));
#ifndef VTSS_BASICS_STANDALONE
    s.ifindex = get_vtss_ifindex(s.os_ifindex);
#endif
    m.add_leaf(s.mtu_bytes, vtss::tag::Name("mtuBytes"));
    m.add_leaf(s.bandwidth_mbit, vtss::tag::Name("bandwidthMbit"));
    m.add_leaf(s.if_flags, vtss::tag::Name("ifFlags"));
    ADD_LEAF_DEFAULT(s.ospf_enabled, vtss::tag::Name("ospfEnabled"), false);
    if (!s.if_up) {
        return;
    }
    mesa_ipv4_network_t net;
    m.add_leaf(AsIpv4(net.address), vtss::tag::Name("ipAddress"));
    m.add_leaf(net.prefix_size, vtss::tag::Name("ipAddressPrefixlen"));
    s.net = net;
    m.add_leaf(s.if_type, vtss::tag::Name("ospfIfType"));
    m.add_leaf(AsIpv4(s.local_if_used), vtss::tag::Name("localIfUsed"));

    std::string buf;
    m.add_leaf(buf, vtss::tag::Name("area"));
    s.area = parse_area(buf);

    m.add_leaf(AsIpv4(s.router_id), vtss::tag::Name("routerId"));
    m.add_leaf(s.network_type, vtss::tag::Name("networkType"));
    m.add_leaf(s.cost, vtss::tag::Name("cost"));
    m.add_leaf(s.transmit_delay.raw(), vtss::tag::Name("transmitDelayMsecs"));
    m.add_leaf(s.state, vtss::tag::Name("state"));
    m.add_leaf(s.priority, vtss::tag::Name("priority"));
    m.add_leaf(AsIpv4(s.bdr_id), vtss::tag::Name("bdrId"));
    m.add_leaf(AsIpv4(s.bdr_address), vtss::tag::Name("bdrAddress"));
    m.add_leaf(s.network_lsa_sequence, vtss::tag::Name("networkLsaSequence"));
    m.add_leaf(s.mcast_member_ospf_all_routers,
               vtss::tag::Name("mcastMemberOspfAllRouters"));
    m.add_leaf(s.mcast_member_ospf_designated_routers,
               vtss::tag::Name("mcastMemberOspfDesignatedRouters"));
    m.add_leaf(s.timer.raw(), vtss::tag::Name("timerMsecs"));
    m.add_leaf(s.timer_dead.raw(), vtss::tag::Name("timerDeadMsecs"));
    m.add_leaf(s.timer_wait.raw(), vtss::tag::Name("timerWaitMsecs"));
    m.add_leaf(s.timer_retransmit.raw(), vtss::tag::Name("timerRetransmit"));
    m.add_leaf(s.timer_hello.raw(), vtss::tag::Name("timerHelloInMsecs"));
    ADD_LEAF_DEFAULT(s.timer_passive_iface,
                     vtss::tag::Name("timerPassiveIface"), false);
    m.add_leaf(s.nbr_count, vtss::tag::Name("nbrCount"));
    m.add_leaf(s.nbr_adjacent_count, vtss::tag::Name("nbrAdjacentCount"));
    m.add_leaf(AsIpv4(s.vlink_peer_addr), vtss::tag::Name("vlinkPeer"));
}

struct InterfaceMapCb : public MapSortedCb<std::string, FrrIpOspfIfStatus> {
    void entry(const std::string &key, const FrrIpOspfIfStatus &val) override {
        uint32_t os_ifindex = if_nametoindex(key.data());
        if (os_ifindex != 0) {
            res[get_vtss_ifindex(os_ifindex)] = val;
            return;
        }

        vtss_ifindex_t ifindex = get_vtss_ifindex_from_vlink(key);
        if (ifindex_is_frr_vlink(ifindex)) {
            res[get_vtss_ifindex_from_vlink(key)] = val;
            return;
        }
    }

    Map<vtss_ifindex_t, FrrIpOspfIfStatus> res;
};

Map<vtss_ifindex_t, FrrIpOspfIfStatus> frr_ip_ospf_interface_status_parse(
        const std::string &s) {
    InterfaceMapCb res;
    vtss::expose::json::Loader loader(&*s.begin(), &*s.end());
    loader.patch_mode_ = true;
    loader.load(res);
    return res.res;
}

FrrRes<Vector<std::string>> to_vty_ip_ospf_interface_status_get() {
    Vector<std::string> res;
    res.emplace_back("show ip ospf interface json");
    return res;
}

FrrRes<Map<vtss_ifindex_t, FrrIpOspfIfStatus>> frr_ip_ospf_interface_status_get() {
    auto cmds = to_vty_ip_ospf_interface_status_get();
    if (!cmds) return cmds.rc;
    std::string vty_res;
    mesa_rc res = frr_daemon_cmd(ospfd_vty, cmds.val, vty_res);
    if (res != MESA_RC_OK) return res;
    return frr_ip_ospf_interface_status_parse(vty_res);
}

// frr_ip_ospf_neighbor_status_get----------------------------------------------
void serialize(vtss::expose::json::Loader &l, vtss::FrrIpOspfNeighborStatus &s) {
    vtss::expose::json::Loader::Map_t m =
            l.as_map(vtss::tag::Typename("vtss_ip_ospf_neightbor_status"));
    m.add_leaf(AsIpv4(s.if_address), vtss::tag::Name("address"));

    std::string buf;
    m.add_leaf(buf, vtss::tag::Name("areaId"));
    s.area = parse_area(buf);
    buf.clear();

    if (m.add_leaf(buf, vtss::tag::Name("transitAreaId")))
        s.transit_id = parse_area(buf);

    std::string iface;
    m.add_leaf(iface, vtss::tag::Name("ifaceName"));
    s.os_ifindex = if_nametoindex(iface.data());
#ifndef VTSS_BASICS_STANDALONE
    if (s.os_ifindex == 0) {
        s.ifindex = get_vtss_ifindex_from_vlink(iface);
    } else {
        s.ifindex = get_vtss_ifindex(s.os_ifindex);
    }
#endif

    m.add_leaf(s.nbr_priority, vtss::tag::Name("priority"));
    m.add_leaf(s.nbr_state, vtss::tag::Name("nbrState"));
    m.add_leaf(AsIpv4(s.router_designated_id),
               vtss::tag::Name("routerDesignatedId"));
    m.add_leaf(AsIpv4(s.router_designated_bck_id),
               vtss::tag::Name("routerDesignatedBackupId"));
    m.add_leaf(s.options_list, vtss::tag::Name("optionsList"));
    m.add_leaf(s.options_counter, vtss::tag::Name("optionsCounter"));
    m.add_leaf(s.router_dead_interval_timer_due.raw(),
               vtss::tag::Name("deadTimeMsecs"));
}

Map<mesa_ipv4_t, Vector<FrrIpOspfNeighborStatus>> frr_ip_ospf_neighbor_status_parse(
        const std::string &s) {
    Map<mesa_ipv4_t, Vector<FrrIpOspfNeighborStatus>> result;
    vtss::expose::json::Loader loader(&*s.begin(), &*s.end());
    loader.patch_mode_ = true;
    loader.load(result);
    return result;
}

FrrRes<Vector<std::string>> to_vty_ip_ospf_neighbor_status_get() {
    Vector<std::string> res;
    res.emplace_back("show ip ospf neighbor json");
    return res;
}

FrrRes<Map<mesa_ipv4_t, Vector<FrrIpOspfNeighborStatus>>>
frr_ip_ospf_neighbor_status_get() {
    auto cmds = to_vty_ip_ospf_neighbor_status_get();
    if (!cmds) return cmds.rc;
    std::string vty_res;
    mesa_rc res = frr_daemon_cmd(ospfd_vty, cmds.val, vty_res);
    if (res != MESA_RC_OK) return res;
    return frr_ip_ospf_neighbor_status_parse(vty_res);
}

// frr_running_config_get ------------------------------------------------------
static mesa_rc cache_running_config_impl() {
    auto cmds = Vector<std::string>{"show running-config"};
    std::string vty_zebra_res;
    if (frr_daemon_cmd(zebra_vty, cmds, vty_zebra_res) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }
    std::string vty_ospfd_res;
    if (frr_daemon_state.is_ospfd_started &&
        frr_daemon_cmd(ospfd_vty, cmds, vty_ospfd_res) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }

    cached_running_config.zebra = vty_zebra_res;
    cached_running_config.ospfd = vty_ospfd_res;
    return MESA_RC_OK;
}

FrrRes<FrrConf> frr_running_config_get() {
    static LinuxClock::time_point last_get = LinuxClock::now();
    CRIT_SCOPE();
    if (cached_running_config.zebra == "" ||
        (LinuxClock::to_seconds(LinuxClock::now() - last_get) > seconds(30))) {
        if (cache_running_config_impl() != MESA_RC_OK) {
            return MESA_RC_ERROR;
        }
        last_get = LinuxClock::now();
    }

    return cached_running_config;
}

mesa_rc frr_ospfd_reload() {
    FrrRes<FrrConf> config = frr_running_config_get();
    if (!config) return MESA_RC_ERROR;
    size_t written_size = write_to_file(
            "/tmp/ospfd.conf", config.val.ospfd.c_str(), config.val.ospfd.size());
    if (written_size != config.val.ospfd.size()) return MESA_RC_ERROR;
    if (ospfd_stop() != MESA_RC_OK) return MESA_RC_ERROR;
    return ospfd_start(false);
}

mesa_rc check_and_start_ospfd() {
    if (frr_daemon_state.is_ospfd_started)
        return MESA_RC_OK;
    else
        return ospfd_start(true);
}

// OSPF Router configuration ---------------------------------------------------
Vector<std::string> to_vty_ospf_router_conf_set() {
    Vector<std::string> res;
    res.push_back("configure terminal");
    res.push_back("no router ospf");
    res.push_back("router ospf");
    return res;
}

mesa_rc frr_ospf_router_conf_set(const OspfInst &id) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_router_conf_set();
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

std::string abr_type_to_str(AbrType type) {
    switch (type) {
    case AbrType_Cisco:
        return "cisco";
    case AbrType_IBM:
        return "ibm";
    case AbrType_Shortcut:
        return "shortcut";
    case AbrType_Standard:
        return "standard";
    default:
        return "cisco";
    }
}

Vector<std::string> to_vty_ospf_router_conf_set(const FrrOspfRouterConf &conf) {
    Vector<std::string> res;
    res.push_back("configure terminal");
    res.push_back("router ospf");
    StringStream buf;
    if (conf.ospf_router_id.valid()) {
        buf << "ospf router-id " << Ipv4Address(conf.ospf_router_id.get());
        res.emplace_back(vtss::move(buf.buf));
        buf.clear();
    }
    if (conf.ospf_router_rfc1583.valid()) {
        buf << (conf.ospf_router_rfc1583.get() ? "" : "no ")
            << "ospf rfc1583compatibility";
        res.emplace_back(vtss::move(buf.buf));
        buf.clear();
    }
    if (conf.ospf_router_abr_type.valid()) {
        buf << "ospf abr-type "
            << abr_type_to_str(conf.ospf_router_abr_type.get());
        res.emplace_back(vtss::move(buf.buf));
        buf.clear();
    }
    return res;
}

mesa_rc frr_ospf_router_conf_set(const OspfInst &id,
                                 const FrrOspfRouterConf &conf) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_router_conf_set(conf);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

Vector<std::string> to_vty_ospf_router_conf_del() {
    Vector<std::string> res;
    res.push_back("configure terminal");
    res.push_back("no router ospf");
    return res;
}

mesa_rc frr_ospf_router_conf_del(const OspfInst &id) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_router_conf_del();
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct EatAll {
    bool operator()(char c) { return true; }
};

struct FrrParseOspfRouter : public FrrConfStreamParserCB {
  private:
    bool try_parse_id(const str &line) {
        parser::Lit lit_ospf("ospf");
        parser::Lit lit_router("router-id");
        parser::IPv4 router_id;
        if (group_spaces(line, {&lit_ospf, &lit_router, &router_id})) {
            res.ospf_router_id = router_id.get().as_api_type();
            return true;
        }
        return false;
    }

    bool try_parse_rfc(const str &line) {
        parser::Lit lit_compatible("compatible");
        parser::Lit lit_rfc("rfc1583");
        if (group_spaces(line, {&lit_compatible, &lit_rfc})) {
            res.ospf_router_rfc1583 = true;
            return true;
        }
        return false;
    }

    AbrType abr_parser(const std::string &type) {
        if (type == "cisco") return AbrType_Cisco;
        if (type == "ibm") return AbrType_IBM;
        if (type == "shortcut") return AbrType_Shortcut;
        if (type == "standard") return AbrType_Standard;
        return AbrType_Cisco;
    }

    bool try_parse_abr(const str &line) {
        parser::Lit lit_ospf("ospf");
        parser::Lit lit_abr("abr-type");
        parser::OneOrMore<EatAll> lit_type;
        if (group_spaces(line, {&lit_ospf, &lit_abr, &lit_type})) {
            res.ospf_router_abr_type = abr_parser(
                    std::string(lit_type.get().begin(), lit_type.get().end()));
            return true;
        }
        return false;
    }

  public:
    void router(const std::string &name, const str &line) override {
        if (try_parse_id(line)) return;
        if (try_parse_abr(line)) return;
        if (try_parse_rfc(line)) return;
    }
    FrrOspfRouterConf res;
};

FrrRes<FrrOspfRouterConf> frr_ospf_router_conf_get(FrrConf &conf,
                                                   const OspfInst &id) {
    if (id != 1) return MESA_RC_ERROR;
    FrrParseOspfRouter cb;
    frr_conf_parser(conf.ospfd, cb);
    return cb.res;
}

// OSPF Router Passive interface configuration ---------------------------------
#ifndef VTSS_BASICS_STANDALONE
FrrRes<std::string> get_interface_name(const vtss_ifindex_t &i) {
    if (!vtss_ifindex_is_vlan(i)) return MESA_RC_ERROR;
    vtss_ifindex_elm_t ife;
    if (vtss_ifindex_decompose(i, &ife) != MESA_RC_OK) return MESA_RC_ERROR;
    StringStream buf;
    buf << "vtss.vlan." << ife.ordinal;
    return FrrRes<std::string>(buf.cstring());
}
#else
FrrRes<std::string> get_interface_name(const vtss_ifindex_t &i) {
    return FrrRes<std::string>("wlan0");
}
#endif

Vector<std::string> to_vty_ospf_router_passive_if_default(bool enable) {
    Vector<std::string> res;
    res.push_back("configure terminal");
    res.push_back("router ospf");
    StringStream buf;
    buf << (enable ? "" : "no ") << "passive-interface default";
    res.emplace_back(vtss::move(buf.buf));
    return res;
}

mesa_rc frr_ospf_router_passive_if_default_set(const OspfInst &id, bool enable) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_router_passive_if_default(enable);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfPassiveIfDefault : public FrrConfStreamParserCB {
    void router(const std::string &name, const str &line) override {
        parser::Lit lit_passive_default("passive-interface default");
        if (group_spaces(line, {&lit_passive_default})) {
            res = true;
        }
    }
    bool res = {false};
};

FrrRes<bool> frr_ospf_router_passive_if_default_get(FrrConf &conf,
                                                    const OspfInst &id) {
    if (id != 1) return MESA_RC_ERROR;
    FrrParseOspfPassiveIfDefault cb;
    frr_conf_parser(conf.ospfd, cb);
    return cb.res;
}

Vector<std::string> to_vty_ospf_router_passive_if_conf_set(const std::string &ifname,
                                                           bool passive) {
    Vector<std::string> res;
    res.push_back("configure terminal");
    res.push_back("router ospf");
    StringStream buf;
    buf << (passive ? "" : "no ") << "passive-interface " << ifname;
    res.emplace_back(vtss::move(buf.buf));
    return res;
}

mesa_rc frr_ospf_router_passive_if_conf_set(const OspfInst &id,
                                            const vtss_ifindex_t &i,
                                            bool passive) {
    if (id != 1) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds =
            to_vty_ospf_router_passive_if_conf_set(interface_name.val, passive);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfPassiveIf : public FrrConfStreamParserCB {
    FrrParseOspfPassiveIf(const std::string &name, bool enabled)
        : if_name{name}, default_enabled{enabled}, res{enabled} {}

    void router(const std::string &name, const str &line) override {
        parser::Lit lit_passive(default_enabled ? "no passive-interface"
                                                : "passive-interface");
        parser::OneOrMore<EatAll> lit_name;
        if (group_spaces(line, {&lit_passive, &lit_name})) {
            if (vtss::equal(lit_name.get().begin(), lit_name.get().end(),
                            if_name.begin()) &&
                (size_t)(lit_name.get().end() - lit_name.get().begin()) ==
                        if_name.size()) {
                res = !default_enabled;
            }
        }
    }

    const std::string &if_name;
    const bool default_enabled;
    bool res;
};

FrrRes<bool> frr_ospf_router_passive_if_conf_get(FrrConf &conf,
                                                 const OspfInst &id,
                                                 const vtss_ifindex_t &i) {
    if (id != 1) return MESA_RC_ERROR;
    FrrRes<std::string> if_name = get_interface_name(i);
    if (!if_name) return MESA_RC_ERROR;
    FrrParseOspfPassiveIfDefault cb_default;
    frr_conf_parser(conf.ospfd, cb_default);
    FrrParseOspfPassiveIf cb(if_name.val, cb_default.res);
    frr_conf_parser(conf.ospfd, cb);
    return cb.res;
}

// frr_ospf_router_default_metric_conf
Vector<std::string> to_vty_ospf_router_default_metric_conf_set(uint32_t val) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "default-metric " << val;
    res.emplace_back(vtss::move(buf.buf));
    return res;
}

mesa_rc frr_ospf_router_default_metric_conf_set(const OspfInst &id, uint32_t val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_router_default_metric_conf_set(val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

Vector<std::string> to_vty_ospf_router_default_metric_conf_del() {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    res.emplace_back("no default-metric");
    return res;
}

mesa_rc frr_ospf_router_default_metric_conf_del(const OspfInst &id) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_router_default_metric_conf_del();
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfRouterDefaultMetric : public FrrConfStreamParserCB {
    void router(const std::string &name, const str &line) override {
        parser::Lit default_lit("default-metric");
        parser::IntUnsignedBase10<uint32_t, 1, 9> val;
        if (group_spaces(line, {&default_lit, &val})) {
            res = val.get();
        }
    }

    Optional<uint32_t> res;
};

FrrRes<Optional<uint32_t>> frr_ospf_router_default_metric_conf_get(
        FrrConf &conf, const OspfInst &id) {
    if (id != 1) return MESA_RC_ERROR;
    FrrParseOspfRouterDefaultMetric cb;
    frr_conf_parser(conf.ospfd, cb);
    return cb.res;
}

// frr_ospf_router_distribute_conf
std::string to_string(FrrOspfRouterRedistributeProtocol val) {
    switch (val) {
    case Protocol_Kernel:
        return "kernel";
    case Protocol_Connected:
        return "connected";
    case Protocol_Static:
        return "static";
    case Protocol_Rip:
        return "rip";
    case Protocol_Bgp:
        return "bgp";
    }
    VTSS_ASSERT(false);
    return "";
}

FrrOspfRouterRedistributeProtocol to_protocol(const std::string &val) {
    if (val == "kernel") return Protocol_Kernel;
    if (val == "connected") return Protocol_Connected;
    if (val == "static") return Protocol_Static;
    if (val == "rip") return Protocol_Rip;
    if (val == "bgp") return Protocol_Bgp;
    VTSS_ASSERT(true);
    return Protocol_Kernel;
}

std::string to_string(FrrOspfRouterMetricType val) {
    switch (val) {
    case MetricType_One:
        return "1";
    case MetricType_Two:
        return "2";
    }
    VTSS_ASSERT(false);
    return "";
}

Vector<std::string> to_vty_ospf_router_redistribute_conf_set(
        const FrrOspfRouterRedistribute &val) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "redistribute " << to_string(val.protocol);

    if (val.metric.valid()) {
        buf << " metric " << val.metric.get();
    } else if (val.route_map.valid()) {
        buf << " route-map " << val.route_map.get();
    } else {
        buf << " metric-type " << to_string(val.metric_type);
    }

    res.emplace_back(vtss::move(buf.buf));
    return res;
}

mesa_rc frr_ospf_router_redistribute_conf_set(
        const OspfInst &id, const FrrOspfRouterRedistribute &val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_router_redistribute_conf_set(val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

Vector<std::string> to_vty_ospf_router_redistribute_conf_del(
        FrrOspfRouterRedistributeProtocol val) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "no redistribute " << to_string(val);
    res.emplace_back(vtss::move(buf.buf));
    return res;
}

mesa_rc frr_ospf_router_redistribute_conf_del(
        const OspfInst &id, FrrOspfRouterRedistributeProtocol val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_router_redistribute_conf_del(val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfRouterRedistribute : public FrrConfStreamParserCB {
    void parse_protocol(const std::string &protocol, const str &line) {
        parser::Lit redistribute_lit{"redistribute"};
        parser::Lit protocol_lit{protocol};
        parser::TagValue<parser::IntUnsignedBase10<uint32_t, 1, 9>, int> metric(
                "metric", 16777215);
        parser::TagValue<parser::IntUnsignedBase10<uint8_t, 1, 2>, int> metric_type(
                "metric-type", 2);
        parser::TagValue<parser::OneOrMore<EatAll>> route_map("route-map");

        if (group_spaces(line, {&redistribute_lit, &protocol_lit},
                         {&metric, &metric_type, &route_map})) {
            FrrOspfRouterRedistribute redistribute;
            redistribute.protocol = to_protocol(protocol);

            if (metric.get().get() != 16777215) {
                redistribute.metric = metric.get().get();
            }

            redistribute.metric_type = metric_type.get().get() == 1
                                               ? MetricType_One
                                               : MetricType_Two;

            if (route_map.get().get().begin() != route_map.get().get().end()) {
                redistribute.route_map =
                        std::string(route_map.get().get().begin(),
                                    route_map.get().get().end());
            }

            res.push_back(redistribute);
        }
    }

    void router(const std::string &name, const str &line) override {
        for (auto protocol : protocols) {
            parse_protocol(protocol, line);
        }
    }

    const Vector<std::string> protocols{"kernel", "connected", "static", "rip",
                                        "bgp"};
    Vector<FrrOspfRouterRedistribute> res;
};

Vector<FrrOspfRouterRedistribute> frr_ospf_router_redistribute_conf_get(
        FrrConf &conf, const OspfInst &id) {
    if (id != 1) return Vector<FrrOspfRouterRedistribute>{};
    FrrParseOspfRouterRedistribute cb;
    frr_conf_parser(conf.ospfd, cb);
    return cb.res;
}

// frr_ospf_router_default_information
Vector<std::string> to_vty_ospf_router_default_information_conf_set(
        const FrrOspfRouterDefaultInformation &val) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "default-information originate";
    if (val.always) buf << " always";

    if (val.metric.valid()) {
        buf << " metric " << val.metric.get();
    }

    buf << " metric-type " << to_string(val.metric_type);

    if (val.route_map.valid()) {
        buf << " route-map " << val.route_map.get();
    }

    res.emplace_back(vtss::move(buf.buf));
    return res;
}

mesa_rc frr_ospf_router_default_information_conf_set(
        const OspfInst &id, const FrrOspfRouterDefaultInformation &val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_router_default_information_conf_set(val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

Vector<std::string> to_vty_ospf_router_default_information_conf_del() {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    res.emplace_back("no default-information");
    return res;
}

mesa_rc frr_ospf_router_default_information_conf_del(const OspfInst &id) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_router_default_information_conf_del();
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfRouterDefaultInformation : public FrrConfStreamParserCB {
    void parser_leftovers(
            const parser::TagValue<parser::IntUnsignedBase10<uint32_t, 1, 9>, int>
                    &metric,
            const parser::TagValue<parser::IntUnsignedBase10<uint8_t, 1, 2>, int>
                    &metric_type,
            const parser::TagValue<parser::OneOrMore<EatAll>> &route_map) {
        if (metric.get().get() != 16777215) {
            res.metric = metric.get().get();
        }

        res.metric_type =
                metric_type.get().get() == 1 ? MetricType_One : MetricType_Two;

        if (route_map.get().get().begin() != route_map.get().get().end()) {
            res.route_map = std::string(route_map.get().get().begin(),
                                        route_map.get().get().end());
        }
    }

    void router(const std::string &name, const str &line) override {
        parser::Lit default_info_lit{"default-information originate"};
        parser::Lit always_lit{"always"};
        parser::TagValue<parser::IntUnsignedBase10<uint32_t, 1, 9>, int> metric(
                "metric", 16777215);
        parser::TagValue<parser::IntUnsignedBase10<uint8_t, 1, 2>, int> metric_type(
                "metric-type", 2);
        parser::TagValue<parser::OneOrMore<EatAll>> route_map("route-map");

        if (group_spaces(line, {&default_info_lit, &always_lit},
                         {&metric, &metric_type, &route_map})) {
            res.always = true;
            parser_leftovers(metric, metric_type, route_map);
            return;
        }
        if (group_spaces(line, {&default_info_lit},
                         {&metric, &metric_type, &route_map})) {
            res.always = false;
            parser_leftovers(metric, metric_type, route_map);
            return;
        }
    }

    FrrOspfRouterDefaultInformation res;
};

FrrRes<FrrOspfRouterDefaultInformation>
frr_ospf_router_default_information_conf_get(FrrConf &conf, const OspfInst &id) {
    if (id != 1) return MESA_RC_ERROR;
    FrrParseOspfRouterDefaultInformation cb;
    frr_conf_parser(conf.ospfd, cb);
    return cb.res;
}

// OSPF Area configuration -----------------------------------------------------
Vector<std::string> to_vty_ospf_area_network_conf_set(const FrrOspfAreaNetwork &val) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "network " << val.net << " area " << Ipv4Address(val.area);
    res.emplace_back(std::move(buf.buf));
    return res;
}

mesa_rc frr_ospf_area_network_conf_set(const OspfInst &id,
                                       const FrrOspfAreaNetwork &val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_network_conf_set(val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

Vector<std::string> to_vty_ospf_area_network_conf_del(const FrrOspfAreaNetwork &val) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "no network " << val.net << " area " << Ipv4Address(val.area);
    res.emplace_back(std::move(buf.buf));
    return res;
}

mesa_rc frr_ospf_area_network_conf_del(const OspfInst &id,
                                       const FrrOspfAreaNetwork &val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_network_conf_del(val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfAreaNetwork : public FrrConfStreamParserCB {
    void router(const std::string &name, const str &line) override {
        parser::Lit net("network");
        parser::Ipv4Network net_val;
        parser::Lit area("area");
        parser::IPv4 area_val;
        if (group_spaces(line, {&net, &net_val, &area, &area_val})) {
            res.emplace_back(net_val.get().as_api_type(),
                             area_val.get().as_api_type());
        }
    }
    Vector<FrrOspfAreaNetwork> res;
};

Vector<FrrOspfAreaNetwork> frr_ospf_area_network_conf_get(FrrConf &config,
                                                          const OspfInst &id) {
    Vector<FrrOspfAreaNetwork> res;
    if (id != 1) return res;
    FrrParseOspfAreaNetwork cb;
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// frr_ospf_area_range_conf
Vector<std::string> to_vty_ospf_area_range_conf_set(const FrrOspfAreaNetwork &val) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "area " << Ipv4Address(val.area) << " range " << val.net;
    res.emplace_back(std::move(buf.buf));
    return res;
}

mesa_rc frr_ospf_area_range_conf_set(const OspfInst &id,
                                     const FrrOspfAreaNetwork &val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_range_conf_set(val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

Vector<std::string> to_vty_ospf_area_range_conf_del(const FrrOspfAreaNetwork &val) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "no area " << Ipv4Address(val.area) << " range " << val.net;
    res.emplace_back(std::move(buf.buf));
    return res;
}

mesa_rc frr_ospf_area_range_conf_del(const OspfInst &id,
                                     const FrrOspfAreaNetwork &val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_range_conf_del(val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfAreaRange : public FrrConfStreamParserCB {
    void router(const std::string &name, const str &line) override {
        parser::Lit area_lit("area");
        parser::IPv4 area_val;
        parser::Lit range_lit("range");
        parser::Ipv4Network net_val;
        if (group_spaces(line, {&area_lit, &area_val, &range_lit, &net_val})) {
            res.emplace_back(net_val.get().as_api_type(),
                             area_val.get().as_api_type());
        }
    }
    Vector<FrrOspfAreaNetwork> res;
};

Vector<FrrOspfAreaNetwork> frr_ospf_area_range_conf_get(FrrConf &config,
                                                        const OspfInst &id) {
    Vector<FrrOspfAreaNetwork> res;
    if (id != 1) return res;
    FrrParseOspfAreaRange cb;
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// frr_ospf_area_range_not_advertise_conf
Vector<std::string> to_vty_ospf_area_range_not_advertise_conf_set(
        const FrrOspfAreaNetwork &val) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "area " << Ipv4Address(val.area) << " range " << val.net
        << " not-advertise";
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_area_range_not_advertise_conf_set(const OspfInst &id,
                                                   const FrrOspfAreaNetwork &val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_range_not_advertise_conf_set(val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

Vector<std::string> to_vty_ospf_area_range_not_advertise_conf_del(
        const FrrOspfAreaNetwork &val) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "no area " << Ipv4Address(val.area) << " range " << val.net
        << " not-advertise";
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_area_range_not_advertise_conf_del(const OspfInst &id,
                                                   const FrrOspfAreaNetwork &val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_range_not_advertise_conf_del(val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfAreaRangeNotAdvertise : public FrrConfStreamParserCB {
    void router(const std::string &name, const str &line) override {
        parser::Lit area_lit("area");
        parser::IPv4 area_val;
        parser::Lit range_lit("range");
        parser::Ipv4Network net_val;
        parser::Lit cost_lit("cost");
        parser::IntUnsignedBase10<uint32_t, 0, 24> cost_val;
        parser::Lit not_advertise_lit("not-advertise");
        /* for configuration 'area range not-advertise' */
        if (group_spaces(line,
                         {&area_lit, &area_val, &range_lit, &net_val,
                          &not_advertise_lit})) {
            res.emplace_back(net_val.get().as_api_type(),
                             area_val.get().as_api_type());
            return;
        }
        /* for configuration 'area range cost <cost> not-advertise' */
        if (group_spaces(line,
                         {&area_lit, &area_val, &range_lit, &net_val, &cost_lit,
                          &cost_val, &not_advertise_lit})) {
            res.emplace_back(net_val.get().as_api_type(),
                             area_val.get().as_api_type());
            return;
        }
    }
    Vector<FrrOspfAreaNetwork> res;
};

Vector<FrrOspfAreaNetwork> frr_ospf_area_range_not_advertise_conf_get(
        FrrConf &config, const OspfInst &id) {
    Vector<FrrOspfAreaNetwork> res;
    if (id != 1) return res;
    FrrParseOspfAreaRangeNotAdvertise cb;
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// frr_ospf_area_range_const_conf
Vector<std::string> to_vty_ospf_area_range_cost_conf_set(
        const FrrOspfAreaNetworkCost &val) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "area " << Ipv4Address(val.area) << " range " << val.net << " cost "
        << val.cost;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_area_range_cost_conf_set(const OspfInst &id,
                                          const FrrOspfAreaNetworkCost &val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_range_cost_conf_set(val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

Vector<std::string> to_vty_ospf_area_range_cost_conf_del(
        const FrrOspfAreaNetworkCost &val) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "no area " << Ipv4Address(val.area) << " range " << val.net
        << " cost 0";
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_area_range_cost_conf_del(const OspfInst &id,
                                          const FrrOspfAreaNetworkCost &val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_range_cost_conf_del(val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfAreaRangeCost : public FrrConfStreamParserCB {
    void router(const std::string &name, const str &line) override {
        parser::Lit area_lit("area");
        parser::IPv4 area_val;
        parser::Lit range_lit("range");
        parser::Ipv4Network net_val;
        parser::Lit cost_lit("cost");
        parser::IntUnsignedBase10<uint32_t, 1, 9> cost_val;
        if (group_spaces(line,
                         {&area_lit, &area_val, &range_lit, &net_val, &cost_lit,
                          &cost_val})) {
            res.emplace_back(net_val.get().as_api_type(),
                             area_val.get().as_api_type(), cost_val.get());
        }
    }
    Vector<FrrOspfAreaNetworkCost> res;
};

Vector<FrrOspfAreaNetworkCost> frr_ospf_area_range_cost_conf_get(
        FrrConf &config, const OspfInst &id) {
    Vector<FrrOspfAreaNetworkCost> res;
    if (id != 1) return res;
    FrrParseOspfAreaRangeCost cb;
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// frr_ospf_area_virtual_link_conf
Vector<std::string> to_vty_ospf_area_virtual_link_conf_set(
        const FrrOspfAreaVirtualLink &val) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "area " << Ipv4Address(val.area) << " virtual-link "
        << Ipv4Address(val.dst);
    if (val.hello_interval.valid()) {
        buf << " hello-interval " << val.hello_interval.get();
    }
    if (val.retransmit_interval.valid()) {
        buf << " retransmit-interval " << val.retransmit_interval.get();
    }
    if (val.transmit_delay.valid()) {
        buf << " transmit-delay " << val.transmit_delay.get();
    }
    if (val.dead_interval.valid()) {
        buf << " dead-interval " << val.dead_interval.get();
    }
    res.emplace_back(std::move(buf.buf));
    return res;
}

mesa_rc frr_ospf_area_virtual_link_conf_set(const OspfInst &id,
                                            const FrrOspfAreaVirtualLink &val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_virtual_link_conf_set(val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

Vector<std::string> to_vty_ospf_area_virtual_link_conf_del(
        const FrrOspfAreaVirtualLink &val) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "no area " << Ipv4Address(val.area) << " virtual-link "
        << Ipv4Address(val.dst);
    res.emplace_back(vtss::move(buf.buf));
    return res;
}

mesa_rc frr_ospf_area_virtual_link_conf_del(const OspfInst &id,
                                            const FrrOspfAreaVirtualLink &val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_virtual_link_conf_del(val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfAreaVirtualLink : public FrrConfStreamParserCB {
    void router(const std::string &name, const str &line) override {
        parser::Lit area_lit("area");
        parser::IPv4 area_val;
        parser::Lit virtual_link("virtual-link");
        parser::IPv4 dst;
        parser::Lit hello_lit("hello-interval");
        parser::IntUnsignedBase10<uint16_t, 1, 6> hello_val;
        parser::Lit retransmit_lit("retransmit-interval");
        parser::IntUnsignedBase10<uint16_t, 1, 6> retransmit_val;
        parser::Lit transmit_lit("transmit-delay");
        parser::IntUnsignedBase10<uint16_t, 1, 6> transmit_val;
        parser::Lit dead_lit("dead-interval");
        parser::IntUnsignedBase10<uint16_t, 1, 6> dead_val;
        if (group_spaces(line,
                         {&area_lit, &area_val, &virtual_link, &dst, &hello_lit,
                          &hello_val, &retransmit_lit, &retransmit_val,
                          &transmit_lit, &transmit_val, &dead_lit, &dead_val})) {
            FrrOspfAreaVirtualLink tmp(area_val.get().as_api_type(),
                                       dst.get().as_api_type());
            tmp.hello_interval = hello_val.get();
            tmp.retransmit_interval = retransmit_val.get();
            tmp.transmit_delay = transmit_val.get();
            tmp.dead_interval = dead_val.get();
            res.push_back(vtss::move(tmp));
            return;
        }

        if (group_spaces(line, {&area_lit, &area_val, &virtual_link, &dst})) {
            FrrOspfAreaVirtualLink tmp(area_val.get().as_api_type(),
                                       dst.get().as_api_type());
            // default values
            tmp.hello_interval = 10;
            tmp.retransmit_interval = 5;
            tmp.transmit_delay = 1;
            tmp.dead_interval = 40;
            res.push_back(vtss::move(tmp));
        }
    }
    Vector<FrrOspfAreaVirtualLink> res;
};

Vector<FrrOspfAreaVirtualLink> frr_ospf_area_virtual_link_conf_get(
        FrrConf &config, const OspfInst &id) {
    Vector<FrrOspfAreaVirtualLink> res;
    if (id != 1) return res;
    FrrParseOspfAreaVirtualLink cb;
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// frr_ospf_area_stub_conf
Vector<std::string> to_vty_ospf_area_stub_conf_set(const mesa_ipv4_t &area) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "area " << Ipv4Address(area) << " stub";
    res.emplace_back(vtss::move(buf.buf));
    return res;
}

mesa_rc frr_ospf_area_stub_conf_set(const OspfInst &id, const mesa_ipv4_t &area) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_stub_conf_set(area);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

Vector<std::string> to_vty_ospf_area_stub_conf_del(const mesa_ipv4_t &area) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "no area " << Ipv4Address(area) << " stub";
    res.emplace_back(vtss::move(buf.buf));
    return res;
}

mesa_rc frr_ospf_area_stub_conf_del(const OspfInst &id, const mesa_ipv4_t &area) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_stub_conf_del(area);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfAreaStub : public FrrConfStreamParserCB {
    void router(const std::string &name, const str &line) override {
        parser::Lit area_lit("area");
        parser::IPv4 area_val;
        parser::Lit stub_lit("stub");
        parser::OneOrMore<EatAll> other;
        if (!group_spaces(line, {&area_lit, &area_val, &stub_lit, &other}) &&
            group_spaces(line, {&area_lit, &area_val, &stub_lit})) {
            res.emplace_back(area_val.get().as_api_type());
        }
    }
    Vector<mesa_ipv4_t> res;
};

Vector<mesa_ipv4_t> frr_ospf_area_stub_conf_get(FrrConf &config,
                                                const OspfInst &id) {
    Vector<mesa_ipv4_t> res;
    if (id != 1) return res;
    FrrParseOspfAreaStub cb;
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// frr_ospf_area_authentication_conf
Vector<std::string> to_vty_ospf_area_authentication_conf_set(
        const mesa_ipv4_t &area, FrrOspfAuthMode mode) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << (mode == FRR_OSPF_AUTH_MODE_NULL ? "no " : "");
    buf << "area " << Ipv4Address(area) << " authentication";
    buf << (mode == FRR_OSPF_AUTH_MODE_MSG_DIGEST ? " message-digest" : "");
    res.emplace_back(std::move(buf.buf));
    return res;
}

mesa_rc frr_ospf_area_authentication_conf_set(const OspfInst &id,
                                              const FrrOspfAreaAuth &val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_authentication_conf_set(val.area, val.auth_mode);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfAreaAuthentication : public FrrConfStreamParserCB {
    void router(const std::string &name, const str &line) override {
        parser::Lit area_lit("area");
        parser::IPv4 area_val;
        parser::Lit auth_lit("authentication");
        parser::Lit digest_lit("message-digest");
        if (group_spaces(line, {&area_lit, &area_val, &auth_lit, &digest_lit})) {
            res.emplace_back(area_val.get().as_api_type(),
                             FRR_OSPF_AUTH_MODE_MSG_DIGEST);
            return;
        }

        if (group_spaces(line, {&area_lit, &area_val, &auth_lit})) {
            res.emplace_back(area_val.get().as_api_type(),
                             FRR_OSPF_AUTH_MODE_PWD);
            return;
        }
    }

    Vector<FrrOspfAreaAuth> res;
};

Vector<FrrOspfAreaAuth> frr_ospf_area_authentication_conf_get(FrrConf &config,
                                                              const OspfInst &id) {
    if (id != 1) return Vector<FrrOspfAreaAuth>{};
    FrrParseOspfAreaAuthentication cb;
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// frr_ospf_area_no_summary_conf
Vector<std::string> to_vty_ospf_area_stub_no_summary_conf_set(
        const mesa_ipv4_t &area) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "area " << Ipv4Address(area) << " stub no-summary";
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_area_stub_no_summary_conf_set(const OspfInst &id,
                                               const mesa_ipv4_t &area) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_stub_no_summary_conf_set(area);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

Vector<std::string> to_vty_ospf_area_stub_no_summary_conf_del(
        const mesa_ipv4_t &area) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "no area " << Ipv4Address(area) << " stub no-summary";
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_area_stub_no_summary_conf_del(const OspfInst &id,
                                               const mesa_ipv4_t &area) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_stub_no_summary_conf_del(area);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfAreaNoSummary : public FrrConfStreamParserCB {
    void router(const std::string &name, const str &line) override {
        parser::Lit area_lit("area");
        parser::IPv4 area_val;
        parser::Lit stub_no_summary_lit("stub no-summary");
        if (group_spaces(line, {&area_lit, &area_val, &stub_no_summary_lit})) {
            res.emplace_back(area_val.get().as_api_type());
        }
    }
    Vector<mesa_ipv4_t> res;
};

Vector<mesa_ipv4_t> frr_ospf_area_stub_no_summary_conf_get(FrrConf &config,
                                                           const OspfInst &id) {
    Vector<mesa_ipv4_t> res;
    if (id != 1) return res;
    FrrParseOspfAreaNoSummary cb;
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// frr_ospf_area_virtual_link_authentication
Vector<std::string> to_vty_ospf_area_virtual_link_authentication_conf_set(
        const FrrOspfAreaVirtualLink &val, FrrOspfAuthMode mode) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << (mode == FRR_OSPF_AUTH_MODE_AREA_CFG ? "no " : "");
    buf << "area " << Ipv4Address(val.area) << " virtual-link "
        << Ipv4Address(val.dst) << " authentication";
    if (mode != FRR_OSPF_AUTH_MODE_AREA_CFG) {
        buf << (mode == FRR_OSPF_AUTH_MODE_NULL
                        ? " null"
                        : mode == FRR_OSPF_AUTH_MODE_PWD ? ""
                                                         : " message-digest");
    }
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_area_virtual_link_authentication_conf_set(
        const OspfInst &id, const FrrOspfAreaVirtualLinkAuth &val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_virtual_link_authentication_conf_set(
            val.virtual_link, val.auth_mode);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfAreaVirtualLinkAuthentication : public FrrConfStreamParserCB {
    void router(const std::string &name, const str &line) override {
        parser::Lit area_lit("area");
        parser::IPv4 area_val;
        parser::Lit link_lit("virtual-link");
        parser::IPv4 dst_val;
        parser::Lit auth_lit("authentication");
        parser::Lit digest_lit("message-digest");
        parser::Lit null_lit("null");
        if (group_spaces(line,
                         {&area_lit, &area_val, &link_lit, &dst_val, &auth_lit,
                          &digest_lit})) {
            res.emplace_back(FrrOspfAreaVirtualLink(area_val.get().as_api_type(),
                                                    dst_val.get().as_api_type()),
                             FRR_OSPF_AUTH_MODE_MSG_DIGEST);
            return;
        }

        if (group_spaces(line,
                         {&area_lit, &area_val, &link_lit, &dst_val, &auth_lit,
                          &null_lit})) {
            res.emplace_back(FrrOspfAreaVirtualLink(area_val.get().as_api_type(),
                                                    dst_val.get().as_api_type()),
                             FRR_OSPF_AUTH_MODE_NULL);
            return;
        }

        parser::EndOfInput eol;
        if (group_spaces(line,
                         {&area_lit, &area_val, &link_lit, &dst_val, &auth_lit,
                          &eol})) {
            res.emplace_back(FrrOspfAreaVirtualLink(area_val.get().as_api_type(),
                                                    dst_val.get().as_api_type()),
                             FRR_OSPF_AUTH_MODE_PWD);
            return;
        }

        if (group_spaces(line, {&area_lit, &area_val, &link_lit, &dst_val, &eol})) {
            res.emplace_back(FrrOspfAreaVirtualLink(area_val.get().as_api_type(),
                                                    dst_val.get().as_api_type()),
                             FRR_OSPF_AUTH_MODE_AREA_CFG);
            return;
        }
    }

    Vector<FrrOspfAreaVirtualLinkAuth> res;
};

Vector<FrrOspfAreaVirtualLinkAuth> frr_ospf_area_virtual_link_authentication_conf_get(
        FrrConf &config, const OspfInst &id) {
    if (id != 1) return Vector<FrrOspfAreaVirtualLinkAuth>{};
    FrrParseOspfAreaVirtualLinkAuthentication cb;
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// frr_ospf_area_virtual_link_message_digest
Vector<std::string> to_vty_ospf_area_virtual_link_message_digest_conf_set(
        const FrrOspfAreaVirtualLink &val, const FrrOspfDigestData &data) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "area " << Ipv4Address(val.area) << " virtual-link "
        << Ipv4Address(val.dst) << " message-digest-key " << data.keyid
        << " md5 " << data.key;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_area_virtual_link_message_digest_conf_set(
        const OspfInst &id, const FrrOspfAreaVirtualLink &val,
        const FrrOspfDigestData &data) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_virtual_link_message_digest_conf_set(val, data);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

Vector<std::string> to_vty_ospf_area_virtual_link_message_digest_conf_del(
        const FrrOspfAreaVirtualLink &val, const FrrOspfDigestData &data) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "no area " << Ipv4Address(val.area) << " virtual-link "
        << Ipv4Address(val.dst) << " message-digest-key " << data.keyid
        << " md5 " << data.key;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_area_virtual_link_message_digest_conf_del(
        const OspfInst &id, const FrrOspfAreaVirtualLink &val,
        const FrrOspfDigestData &data) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_virtual_link_message_digest_conf_del(val, data);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfAreaVirtualLinkMessageDigest : public FrrConfStreamParserCB {
    void router(const std::string &name, const str &line) override {
        parser::Lit area_lit("area");
        parser::IPv4 area_val;
        parser::Lit virtual_link_lit("virtual-link");
        parser::IPv4 dst_val;
        parser::Lit message_lit("message-digest-key");
        parser::IntUnsignedBase10<uint8_t, 1, 4> key_id;
        parser::Lit md5_lit("md5");
        parser::OneOrMore<EatAll> key_val;
        if (group_spaces(line,
                         {&area_lit, &area_val, &virtual_link_lit, &dst_val,
                          &message_lit, &key_id, &md5_lit, &key_val})) {
            res.emplace_back(
                    FrrOspfAreaVirtualLink{area_val.get().as_api_type(),
                                           dst_val.get().as_api_type()},
                    FrrOspfDigestData{key_id.get(),
                                      std::string(key_val.get().begin(),
                                                  key_val.get().end())});
        }
    }

    Vector<FrrOspfAreaVirtualLinkDigest> res;
};

Vector<FrrOspfAreaVirtualLinkDigest>
frr_ospf_area_virtual_link_message_digest_conf_get(FrrConf &config,
                                                   const OspfInst &id) {
    if (id != 1) return Vector<FrrOspfAreaVirtualLinkDigest>{};
    FrrParseOspfAreaVirtualLinkMessageDigest cb;
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// frr_ospf_area_virtual_link_authentication_key
Vector<std::string> to_vty_ospf_area_virtual_link_authentication_key_conf_set(
        const FrrOspfAreaVirtualLink &val, const std::string &auth_key) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "area " << Ipv4Address(val.area) << " virtual-link "
        << Ipv4Address(val.dst) << " authentication-key " << auth_key;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_area_virtual_link_authentication_key_conf_set(
        const OspfInst &id, const FrrOspfAreaVirtualLink &val,
        const std::string &auth_key) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_virtual_link_authentication_key_conf_set(
            val, auth_key);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

Vector<std::string> to_vty_ospf_area_virtual_link_authentication_key_conf_del(
        const FrrOspfAreaVirtualLinkKey &val) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    res.emplace_back("router ospf");
    StringStream buf;
    buf << "no area " << Ipv4Address(val.virtual_link.area) << " virtual-link "
        << Ipv4Address(val.virtual_link.dst) << " authentication-key "
        << val.key_data;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_area_virtual_link_authentication_key_conf_del(
        const OspfInst &id, const FrrOspfAreaVirtualLinkKey &val) {
    if (id != 1) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_area_virtual_link_authentication_key_conf_del(val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfAreaVirtualLinkAuthenticationKey
        : public FrrConfStreamParserCB {
    void router(const std::string &name, const str &line) override {
        parser::Lit area_lit("area");
        parser::IPv4 area_val;
        parser::Lit virtual_link_lit("virtual-link");
        parser::IPv4 dst_val;
        parser::Lit key_lit("authentication-key");
        parser::OneOrMore<EatAll> key_val;
        if (group_spaces(line,
                         {&area_lit, &area_val, &virtual_link_lit, &dst_val,
                          &key_lit, &key_val})) {
            res.emplace_back(
                    FrrOspfAreaVirtualLink{area_val.get().as_api_type(),
                                           dst_val.get().as_api_type()},
                    std::string(key_val.get().begin(), key_val.get().end()));
        }
    }

    Vector<FrrOspfAreaVirtualLinkKey> res;
};

Vector<FrrOspfAreaVirtualLinkKey>
frr_ospf_area_virtual_link_authentication_key_conf_get(FrrConf &config,
                                                       const OspfInst &id) {
    if (id != 1) return Vector<FrrOspfAreaVirtualLinkKey>{};
    FrrParseOspfAreaVirtualLinkAuthenticationKey cb;
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// OSPF Interface Configuration ------------------------------------------------

Vector<std::string> to_vty_ospf_if_mtu_mode_conf_set(const std::string &ifname,
                                                     FrrOspfIfMtuMode mode) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    StringStream buf;
    buf << "interface " << ifname;
    res.emplace_back(std::move(buf.buf));
    buf.clear();
    buf << ((mode == FRR_OSPF_IF_MTU_MODE_IGNORE) ? "" : "no ")
        << "ip ospf mtu-ignore";
    res.emplace_back(std::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_if_mtu_mode_conf_set(vtss_ifindex_t i, FrrOspfIfMtuMode mode) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_if_mtu_mode_conf_set(interface_name.val, mode);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

// frr_ospf_if_authentication_key_conf
Vector<std::string> to_vty_ospf_if_authentication_key_set(
        const std::string &ifname, const std::string &auth_key) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    StringStream buf;
    buf << "interface " << ifname;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    buf << "ip ospf authentication-key " << auth_key;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_if_authentication_key_conf_set(vtss_ifindex_t i,
                                                const std::string &auth_key) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds =
            to_vty_ospf_if_authentication_key_set(interface_name.val, auth_key);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

Vector<std::string> to_vty_ospf_if_authentication_key_del(const std::string &ifname) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    StringStream buf;
    buf << "interface " << ifname;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    res.emplace_back("no ip ospf authentication-key");
    return res;
}

mesa_rc frr_ospf_if_authentication_key_conf_del(vtss_ifindex_t i) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_if_authentication_key_del(interface_name.val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfIfAuthenticationKey : public FrrConfStreamParserCB {
    FrrParseOspfIfAuthenticationKey(const std::string &ifname) : name{ifname} {}
    void interface(const std::string &ifname, const str &line) override {
        if (name == ifname) {
            parser::Lit ip("ip");
            parser::Lit ospf("ospf");
            parser::Lit auth("authentication-key");
            parser::OneOrMore<EatAll> key;
            if (group_spaces(line, {&ip, &ospf, &auth, &key})) {
                res = std::string(key.get().begin(), key.get().end());
            }
        }
    }
    FrrRes<std::string> res = std::string("");
    const std::string &name;
};

FrrRes<std::string> frr_ospf_if_authentication_key_conf_get(FrrConf &config,
                                                            vtss_ifindex_t i) {
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    FrrParseOspfIfAuthenticationKey cb(interface_name.val);
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// frr_ospf_if_authentication_conf
Vector<std::string> to_vty_ospf_if_authentication_set(const std::string &ifname,
                                                      FrrOspfAuthMode mode) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    StringStream buf;
    buf << "interface " << ifname;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    buf << (mode == FRR_OSPF_AUTH_MODE_AREA_CFG ? "no " : "");
    buf << "ip ospf authentication";
    buf << (mode == FRR_OSPF_AUTH_MODE_MSG_DIGEST
                    ? " message-digest"
                    : mode == FRR_OSPF_AUTH_MODE_NULL ? " null" : "");
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_if_authentication_conf_set(vtss_ifindex_t i,
                                            FrrOspfAuthMode mode) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_if_authentication_set(interface_name.val, mode);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfIfAuthentication : public FrrConfStreamParserCB {
    FrrParseOspfIfAuthentication(const std::string &ifname) : name{ifname} {}
    void interface(const std::string &ifname, const str &line) {
        if (ifname == name) {
            parser::Lit ip("ip");
            parser::Lit ospf("ospf");
            parser::Lit auth("authentication");
            parser::Lit digest("message-digest");
            parser::Lit auth_null("null");
            if (group_spaces(line, {&ip, &ospf, &auth, &digest})) {
                res = FRR_OSPF_AUTH_MODE_MSG_DIGEST;
                return;
            }

            if (group_spaces(line, {&ip, &ospf, &auth, &auth_null})) {
                res = FRR_OSPF_AUTH_MODE_NULL;
                return;
            }

            parser::OneOrMore<EatAll> others;
            if (group_spaces(line, {&ip, &ospf, &auth}) &&
                !group_spaces(line, {&ip, &ospf, &auth, &others})) {
                res = FRR_OSPF_AUTH_MODE_PWD;
                return;
            }
        }
    }

    const std::string &name;
    FrrOspfAuthMode res = FRR_OSPF_AUTH_MODE_AREA_CFG;
};

FrrRes<FrrOspfAuthMode> frr_ospf_if_authentication_conf_get(FrrConf &config,
                                                            vtss_ifindex_t i) {
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    FrrParseOspfIfAuthentication cb(interface_name.val);
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// frr_ospf_if_message_digest_conf
Vector<std::string> to_vty_ospf_if_message_digest_set(
        const std::string &ifname, const FrrOspfDigestData &data) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    StringStream buf;
    buf << "interface " << ifname;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    buf << "ip ospf message-digest-key " << data.keyid << " md5 " << data.key;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_if_message_digest_conf_set(vtss_ifindex_t i,
                                            const FrrOspfDigestData &data) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_if_message_digest_set(interface_name.val, data);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

Vector<std::string> to_vty_ospf_if_message_digest_del(const std::string &ifname,
                                                      uint8_t keyid) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    StringStream buf;
    buf << "interface " << ifname;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    buf << "no ip ospf message-digest-key " << keyid;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_if_message_digest_conf_del(vtss_ifindex_t i, uint8_t keyid) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_if_message_digest_del(interface_name.val, keyid);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

struct FrrParseOspfIfMessageDigest : public FrrConfStreamParserCB {
    FrrParseOspfIfMessageDigest(const std::string &ifname) : name{ifname} {}
    void interface(const std::string &ifname, const str &line) {
        if (name == ifname) {
            parser::Lit ip("ip");
            parser::Lit ospf("ospf");
            parser::Lit key("message-digest-key");
            parser::IntUnsignedBase10<uint8_t, 1, 4> key_id;
            parser::Lit md5("md5");
            parser::OneOrMore<EatAll> key_val;
            if (group_spaces(line, {&ip, &ospf, &key, &key_id, &md5, &key_val})) {
                res.emplace_back(key_id.get(), std::string(key_val.get().begin(),
                                                           key_val.get().end()));
            }
        }
    }

    Vector<FrrOspfDigestData> res;
    const std::string name;
};

Vector<FrrOspfDigestData> frr_ospf_if_message_digest_conf_get(FrrConf &config,
                                                              vtss_ifindex_t i) {
    FrrRes<std::string> interface_name = get_interface_name(i);
    Vector<FrrOspfDigestData> res;
    if (!interface_name) return res;
    FrrParseOspfIfMessageDigest cb(interface_name.val);
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// helper frr_ospf_if_field
Vector<std::string> to_vty_ospf_if_field_conf_set(const std::string &ifname,
                                                  const std::string &field,
                                                  uint16_t val) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    StringStream buf;
    buf << "interface " << ifname;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    buf << "ip ospf " << field << " " << val;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

Vector<std::string> to_vty_ospf_if_field_conf_del(const std::string &ifname,
                                                  const std::string &field) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    StringStream buf;
    buf << "interface " << ifname;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    buf << "no ip ospf " << field;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

struct FrrParseOspfIfField : public FrrConfStreamParserCB {
    FrrParseOspfIfField(const std::string &name, const std::string &word,
                        uint32_t val)
        : if_name{name}, key_word{word}, res{val} {}

    void interface(const std::string &ifname, const str &line) override {
        if (if_name == ifname) {
            parser::Lit ip("ip ospf");
            parser::Lit word(key_word.c_str());
            parser::IntUnsignedBase10<uint32_t, 1, 10> val;
            if (group_spaces(line, {&ip, &word, &val})) {
                res = val.get();
            }
        }
    }
    const std::string if_name;
    const std::string key_word;
    uint32_t res;
};

// frr_ospf_if_dead_interval_conf
mesa_rc frr_ospf_if_dead_interval_conf_set(vtss_ifindex_t i, uint32_t val) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_if_field_conf_set(interface_name.val,
                                              "dead-interval", val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

mesa_rc frr_ospf_if_dead_interval_minimal_conf_set(vtss_ifindex_t i,
                                                   uint32_t val) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_if_field_conf_set(
            interface_name.val, "dead-interval minimal hello-multiplier", val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

mesa_rc frr_ospf_if_dead_interval_conf_del(vtss_ifindex_t i) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds =
            to_vty_ospf_if_field_conf_del(interface_name.val, "dead-interval");
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

FrrRes<FrrOspfDeadInterval> frr_ospf_if_dead_interval_conf_get(FrrConf &config,
                                                               vtss_ifindex_t i) {
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    FrrParseOspfIfField cb_multiplier{
            interface_name.val, "dead-interval minimal hello-multiplier", 0};
    frr_conf_parser(config.ospfd, cb_multiplier);
    if (cb_multiplier.res != 0) {
        return FrrOspfDeadInterval{true, cb_multiplier.res};
    }
    FrrParseOspfIfField cb{interface_name.val, "dead-interval", 40};
    frr_conf_parser(config.ospfd, cb);
    return FrrOspfDeadInterval{false, cb.res};
}

// frr_ospf_if_hello_interface_conf
mesa_rc frr_ospf_if_hello_interval_conf_set(vtss_ifindex_t i, uint32_t val) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_if_field_conf_set(interface_name.val,
                                              "hello-interval", val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

mesa_rc frr_ospf_if_hello_interval_conf_del(vtss_ifindex_t i) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds =
            to_vty_ospf_if_field_conf_del(interface_name.val, "hello-interval");
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

FrrRes<uint32_t> frr_ospf_if_hello_interval_conf_get(FrrConf &config,
                                                     vtss_ifindex_t i) {
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    FrrParseOspfIfField cb{interface_name.val, "hello-interval", 10};
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// frr_ospf_if_retransmit_interval_conf
mesa_rc frr_ospf_if_retransmit_interval_conf_set(vtss_ifindex_t i, uint32_t val) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_if_field_conf_set(interface_name.val,
                                              "retransmit-interval", val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

mesa_rc frr_ospf_if_retransmit_interval_conf_del(vtss_ifindex_t i) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_if_field_conf_del(interface_name.val,
                                              "retransmit-interval");
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

FrrRes<uint32_t> frr_ospf_if_retransmit_interval_conf_get(FrrConf &config,
                                                          vtss_ifindex_t i) {
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    FrrParseOspfIfField cb{interface_name.val, "retransmit-interval", 5};
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// frr_ospf_if_cost_conf
mesa_rc frr_ospf_if_cost_conf_set(vtss_ifindex_t i, uint32_t val) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_if_field_conf_set(interface_name.val, "cost", val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

mesa_rc frr_ospf_if_cost_conf_del(vtss_ifindex_t i) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_if_field_conf_del(interface_name.val, "cost");
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

FrrRes<uint32_t> frr_ospf_if_cost_conf_get(FrrConf &config, vtss_ifindex_t i) {
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;

    FrrParseOspfIfField cb{interface_name.val, "cost", 0};
    frr_conf_parser(config.ospfd, cb);
    return cb.res;
}

// frr_ospf_if_priority
mesa_rc frr_ospf_if_priority_conf_set(vtss_ifindex_t i, uint8_t val) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds =
            to_vty_ospf_if_field_conf_set(interface_name.val, "priority", val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}

FrrRes<uint8_t> frr_ospf_if_priority_conf_get(FrrConf &config, vtss_ifindex_t i) {
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    FrrParseOspfIfField cb{interface_name.val, "priority", 1};
    frr_conf_parser(config.ospfd, cb);
    return static_cast<uint8_t>(cb.res);
}

Vector<std::string> to_vty_ospf_if_del(const std::string &ifname) {
    Vector<std::string> res;
    res.emplace_back("configure terminal");
    StringStream buf;
    buf << "no interface " << ifname;
    res.emplace_back(vtss::move(buf.buf));
    buf.clear();
    return res;
}

mesa_rc frr_ospf_if_del(vtss_ifindex_t i) {
    if (check_and_start_ospfd() != MESA_RC_OK) return MESA_RC_ERROR;
    FrrRes<std::string> interface_name = get_interface_name(i);
    if (!interface_name) return MESA_RC_ERROR;
    auto cmds = to_vty_ospf_if_del(interface_name.val);
    std::string vty_res;
    clear_cached_running_config();
    return frr_daemon_cmd(ospfd_vty, cmds, vty_res);
}


}  // namespace vtss
