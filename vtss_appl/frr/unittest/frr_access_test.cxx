/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include <string.h>
#include <vtss/basics/api_types.h>
#include <frr_access.hxx>
#include <vtss/basics/string.hxx>
#include "catch.hpp"

TEST_CASE("frr_access_ip_route_set", "[frr]") {
    SECTION("set config ipv4 with distance") {
        vtss::FrrIpRoute r;
        r.net.route.ipv4_uc.network.address = 0;
        r.net.route.ipv4_uc.network.prefix_size = 24;
        r.net.route.ipv4_uc.destination = 1;
        r.net.type = MESA_ROUTING_ENTRY_TYPE_IPV4_UC;
        r.distance = 2;

        auto result = to_vty_ip_route_conf_set(r);
        CHECK(result.size() == 2);
        CHECK(result[0] == "configure terminal");
        CHECK(result[1] == "ip route 0.0.0.0/24 0.0.0.1 2");
    }
    SECTION("set config ipv4 without distance") {
        vtss::FrrIpRoute r;
        r.net.route.ipv4_uc.network.address = 3;
        r.net.route.ipv4_uc.network.prefix_size = 16;
        r.net.route.ipv4_uc.destination = 4;
        r.net.type = MESA_ROUTING_ENTRY_TYPE_IPV4_UC;
        r.distance = 1;

        auto result = to_vty_ip_route_conf_set(r);
        CHECK(result.size() == 2);
        CHECK(result[0] == "configure terminal");
        CHECK(result[1] == "ip route 0.0.0.3/16 0.0.0.4");
    }
    SECTION("set config ipv6 with distance") {
        vtss::FrrIpRoute r;
        r.net.route.ipv6_uc.network.address = {1, 2, 3};
        r.net.route.ipv6_uc.network.prefix_size = 60;
        r.net.route.ipv6_uc.destination = {5, 6, 7};
        r.net.type = MESA_ROUTING_ENTRY_TYPE_IPV6_UC;
        r.distance = 2;

        auto result = to_vty_ip_route_conf_set(r);
        CHECK(result.size() == 2);
        CHECK(result[0] == "configure terminal");
        CHECK(result[1] == "ipv6 route 102:300::/60 506:700:: 2");
    }

    SECTION("set config ipv6 withtout distance") {
        vtss::FrrIpRoute r;
        r.net.route.ipv6_uc.network.address = {1, 5, 8};
        r.net.route.ipv6_uc.network.prefix_size = 20;
        r.net.route.ipv6_uc.destination = {5, 6, 7};
        r.net.type = MESA_ROUTING_ENTRY_TYPE_IPV6_UC;
        r.distance = 1;

        auto result = to_vty_ip_route_conf_set(r);
        CHECK(result.size() == 2);
        CHECK(result[0] == "configure terminal");
        CHECK(result[1] == "ipv6 route 105:800::/20 506:700::");
    }
}

TEST_CASE("frr_access_ip_route_del", "[frr]") {
    SECTION("delete config ipv4") {
        vtss::FrrIpRoute r;
        r.net.route.ipv4_uc.network.address = 5;
        r.net.route.ipv4_uc.network.prefix_size = 8;
        r.net.route.ipv4_uc.destination = 7;
        r.net.type = MESA_ROUTING_ENTRY_TYPE_IPV4_UC;

        auto result = to_vty_ip_route_conf_del(r);
        CHECK(result.size() == 2);
        CHECK(result[0] == "configure terminal");
        CHECK(result[1] == "no ip route 0.0.0.5/8 0.0.0.7");
    }
    SECTION("delete config ipv6") {
        vtss::FrrIpRoute r;
        r.net.route.ipv6_uc.network.address = {1, 2, 3, 0, 5};
        r.net.route.ipv6_uc.network.prefix_size = 40;
        r.net.route.ipv6_uc.destination = {2, 6, 7};
        r.net.type = MESA_ROUTING_ENTRY_TYPE_IPV6_UC;

        auto result = to_vty_ip_route_conf_del(r);
        CHECK(result.size() == 2);
        CHECK(result[0] == "configure terminal");
        CHECK(result[1] == "no ipv6 route 102:300:500::/40 206:700::");
    }
}

TEST_CASE("frr_access_ip_route_get", "[frr]") {
    SECTION("get config without distance") {
        vtss::FrrConf conf;
        conf.zebra = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
router ospf network 192.168.1.0/24 area 0.0.0.0
!
ip route 0.0.0.1/8 0.0.0.5
!
line vty
!
)";
        auto result = frr_ip_route_conf_get(conf);
        CHECK(result.size() == 1);
        CHECK(result[0].net.route.ipv4_uc.network.address == 1);
        CHECK(result[0].net.route.ipv4_uc.network.prefix_size == 8);
        CHECK(result[0].net.route.ipv4_uc.destination == 5);
        CHECK(result[0].distance == 1);
        CHECK(result[0].tag == 0);
    }

    SECTION("get config ipv5 without distance") {
        vtss::FrrConf conf;
        conf.zebra = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
router ospf network 192.168.1.0/24 area 0.0.0.0
!
ipv6 route 2001:2::/96 fe80::2
!
line vty
!
)";
        auto result = frr_ip_route_conf_get(conf);
        CHECK(result[0].net.route.ipv6_uc.network.address.addr[0] == 0x20);
        CHECK(result[0].net.route.ipv6_uc.network.address.addr[1] == 0x1);
        CHECK(result[0].net.route.ipv6_uc.network.address.addr[3] == 0x2);
        CHECK(result[0].net.route.ipv6_uc.network.prefix_size == 96);
        CHECK(result[0].net.route.ipv6_uc.destination.addr[0] == 0xFE);
        CHECK(result[0].net.route.ipv6_uc.destination.addr[1] == 0x80);
        CHECK(result[0].net.route.ipv6_uc.destination.addr[15] == 0x2);
        CHECK(result[0].distance == 1);
        CHECK(result[0].tag == 0);
    }

    SECTION("get config with distance") {
        vtss::FrrConf conf;
        conf.zebra = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
router ospf network 192.168.1.0/24 area 0.0.0.0
!
ip route 0.0.0.1/8 0.0.0.6 9
!
line vty
!
)";
        auto result = frr_ip_route_conf_get(conf);
        CHECK(result.size() == 1);
        CHECK(result[0].net.route.ipv4_uc.network.address == 1);
        CHECK(result[0].net.route.ipv4_uc.network.prefix_size == 8);
        CHECK(result[0].net.route.ipv4_uc.destination == 6);
        CHECK(result[0].distance == 9);
        CHECK(result[0].tag == 0);
    }

    SECTION("get config with distance and tag") {
        vtss::FrrConf conf;
        conf.zebra = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
router ospf network 192.168.1.0/24 area 0.0.0.0
!
ip route 0.0.0.1/8 0.0.0.6 tag 4 50
!
line vty
!
)";
        auto result = frr_ip_route_conf_get(conf);
        CHECK(result.size() == 1);
        CHECK(result[0].net.route.ipv4_uc.network.address == 1);
        CHECK(result[0].net.route.ipv4_uc.network.prefix_size == 8);
        CHECK(result[0].net.route.ipv4_uc.destination == 6);
        CHECK(result[0].distance == 50);
        CHECK(result[0].tag == 4);
    }

    SECTION("get config ipv4 and ipv6") {
        vtss::FrrConf conf;
        conf.zebra = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
ip route 0.0.0.1/8 0.0.0.6
ipv6 route 2041:5::/20 fe80::2
!
line vty
!
)";

        auto result = frr_ip_route_conf_get(conf);
        CHECK(result.size() == 2);
        CHECK(result[0].net.route.ipv4_uc.network.address == 1);
        CHECK(result[0].net.route.ipv4_uc.network.prefix_size == 8);
        CHECK(result[0].net.route.ipv4_uc.destination == 6);
        CHECK(result[0].distance == 1);
        CHECK(result[0].tag == 0);
        CHECK(result[1].net.route.ipv6_uc.network.address.addr[0] == 0x20);
        CHECK(result[1].net.route.ipv6_uc.network.address.addr[1] == 0x41);
        CHECK(result[1].net.route.ipv6_uc.network.address.addr[3] == 0x5);
        CHECK(result[1].net.route.ipv6_uc.network.prefix_size == 20);
        CHECK(result[1].net.route.ipv6_uc.destination.addr[0] == 0xFE);
        CHECK(result[1].net.route.ipv6_uc.destination.addr[1] == 0x80);
        CHECK(result[1].net.route.ipv6_uc.destination.addr[15] == 0x2);
        CHECK(result[1].distance == 1);
        CHECK(result[1].tag == 0);
    }
}

struct FrrParserRouteTest : public vtss::FrrConfStreamParserCB {
    void root(const vtss::str &line) override {
        root_lines.emplace_back(std::string(line.begin(), line.end()));
    }
    void interface(const std::string &ifname, const vtss::str &line) override {
        interfaces_lines.emplace_back(std::string(line.begin(), line.end()));
    }
    void router(const std::string &name, const vtss::str &line) override {
        router_lines.emplace_back(std::string(line.begin(), line.end()));
    }
    vtss::Vector<std::string> root_lines;
    vtss::Vector<std::string> interfaces_lines;
    vtss::Vector<std::string> router_lines;
};

TEST_CASE("frr_conf_parser", "[frr]") {
    std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
router ospf
    network 192.168.1.0/24 area 0.0.0.0
!
ip route 0.0.0.1/8 0.0.0.6 9
!
line vty
!
)";

    SECTION("frr_conf_parser route") {
        FrrParserRouteTest parser;
        frr_conf_parser(config, parser);
        CHECK(parser.root_lines.size() == 7);
        CHECK(parser.root_lines[0] == "frr version 2.0");
        CHECK(parser.root_lines[1] == "frr defaults traditional");
        CHECK(parser.root_lines[2] == "hostname ospfd");
        CHECK(parser.root_lines[3] == "password zebra");
        CHECK(parser.root_lines[4] == "log file /tmp/ospfd.log");
        CHECK(parser.root_lines[5] == "ip route 0.0.0.1/8 0.0.0.6 9");
        CHECK(parser.root_lines[6] == "line vty");
    }

    SECTION("frr_conf_parser interface") {
        FrrParserRouteTest parser;
        frr_conf_parser(config, parser);
        CHECK(parser.interfaces_lines.size() == 1);
        CHECK(parser.interfaces_lines[0] == "interface vtss.ifh");
    }

    SECTION("frr_conf parser router") {
        FrrParserRouteTest parser;
        frr_conf_parser(config, parser);
        CHECK(parser.router_lines.size() == 2);
        CHECK(parser.router_lines[0] == "router ospf");
        CHECK(parser.router_lines[1] ==
              "    network 192.168.1.0/24 area 0.0.0.0");
    }
}

TEST_CASE("frr_ip_route_status", "[frr]") {
    std::string ip_route = R"({
"0.0.0.0\/0":[
    {
    "prefix":"0.0.0.0\/0",
    "protocol":"static",
    "distance":3,
    "metric":0,
    "nexthops":[
        {
        "ip":"0.0.0.1",
        "afi":"ipv4"
        }
    ]
    },
    {
    "prefix":"0.0.0.1/4",
    "protocol":"kernel",
    "selected":true,
    "nexthops":[
        {
        "fib":true,
        "ip":"10.10.128.1",
        "afi":"ipv4",
        "interfaceIndex":2,
        "interfaceName":"enp2s0",
        "active":true
        }
    ]
    }
],
"10.10.128.0/22":[
    {
    "prefix":"10.10.128.0/22",
    "protocol":"connected",
    "selected":true,
    "nexthops":[
        {
        "fib":true,
        "directlyConnected":true,
        "interfaceIndex":2,
        "interfaceName":"enp2s0",
        "active":true
        }
    ]
    }
],
"10.10.138.15/32":[
    {
    "prefix":"10.10.138.15/32",
    "protocol":"kernel",
    "selected":true,
    "nexthops":[
        {
        "fib":true,
        "ip":"10.10.128.1",
        "afi":"ipv4",
        "interfaceIndex":2,
        "interfaceName":"enp2s0",
        "active":true
        }
    ]
    }
],
"96.0.0.0/30":[
    {
    "prefix":"96.0.0.0/30",
    "protocol":"static",
    "distance":255,
    "metric":0,
    "nexthops":[
        {
        "ip":"1.2.3.4",
        "afi":"ipv4"
        }
    ]
    }
],
"97.0.0.0/30":[
    {
    "prefix":"97.0.0.0/30",
    "protocol":"static",
    "distance":1,
    "metric":0,
    "nexthops":[
        {
        "ip":"1.2.3.4",
        "afi":"ipv4"
        }
    ]
    }
],
"98.0.0.0/30":[
    {
    "prefix":"98.0.0.0/30",
    "protocol":"static",
    "distance":4,
    "metric":0,
    "nexthops":[
        {
        "ip":"1.2.3.4",
        "afi":"ipv4"
        }
    ]
    }
],
"99.0.0.0/30":[
    {
    "prefix":"99.0.0.0/30",
    "protocol":"static",
    "distance":1,
    "metric":0,
    "nexthops":[
        {
        "ip":"1.2.3.4",
        "afi":"ipv4"
        }
    ]
    }
],
"169.254.0.0/16":[
    {
    "prefix":"169.254.0.0/16",
    "protocol":"kernel",
    "selected":true,
    "nexthops":[
        {
        "fib":true,
        "directlyConnected":true,
        "interfaceIndex":2,
        "interfaceName":"enp2s0",
        "active":true
        }
    ]
    }
],
"192.168.1.0/24":[
    {
    "prefix":"192.168.1.0/24",
    "protocol":"ospf",
    "distance":110,
    "metric":1,
    "uptime":"01:16:52",
    "nexthops":[
        {
        "directlyConnected":true,
        "interfaceIndex":5,
        "interfaceName":"enp9s0f0",
        "active":true
        }
    ]
    },
    {
    "prefix":"192.168.1.0/24",
    "protocol":"connected",
    "selected":true,
    "nexthops":[
        {
        "fib":true,
        "directlyConnected":true,
        "interfaceIndex":5,
        "interfaceName":"enp9s0f0",
        "active":true
        }
    ]
    }
]
}
)";

    auto result = vtss::frr_ip_route_status_parse(ip_route);
    CHECK(result.size() == 9);
    CHECK(result[0].routes.size() == 2);
    CHECK(result[0].routes[0].prefix.address == 0);
    CHECK(result[0].routes[0].prefix.prefix_size == 0);
    CHECK(result[0].routes[0].distance == 3);
    CHECK(result[0].routes[0].protocol == vtss::Route_Static);
    CHECK(result[0].routes[0].selected == false);
    CHECK(result[0].routes[0].metric == 0);
    CHECK(result[0].routes[0].next_hops.size() == 1);
    CHECK(result[0].routes[0].next_hops[0].ip == 1);
    CHECK(result[0].routes[0].next_hops[0].os_ifindex == 0);
    CHECK(result[0].routes[1].prefix.address == 1);
    CHECK(result[0].routes[1].prefix.prefix_size == 4);
    CHECK(result[0].routes[1].distance == 1);
    CHECK(result[0].routes[1].protocol == vtss::Route_Kernel);
    CHECK(result[0].routes[1].selected == true);
    CHECK(result[0].routes[1].metric == 0);
    CHECK(result[0].routes[1].next_hops.size() == 1);
    CHECK(result[0].routes[1].next_hops[0].ip == 0xa0a8001);
    CHECK(result[0].routes[1].next_hops[0].selected == true);
    CHECK(result[1].routes[0].distance == 1);
    CHECK(result[8].routes[0].up_time.raw() == 4612);
}

TEST_CASE("frr_ip_route_status_selected", "[frr]") {
    std::string ip_route = R"({
"192.168.5.0\/24":[
    {
        "prefix":"192.168.5.0\/24",
        "protocol":"ospf",
        "distance":110,
        "metric":30,
        "uptime":"00:02:47",
        "nexthops":[
        {
            "ip":"192.168.4.1",
            "afi":"ipv4",
            "interfaceIndex":5,
            "interfaceName":"vtss.vlan.40",
            "active":true
        },
        {
            "ip":"192.168.3.1",
            "afi":"ipv4",
            "interfaceIndex":4,
            "interfaceName":"vtss.vlan.30",
            "active":true
        }
        ]
    },
    {
        "prefix":"192.168.5.0\/24",
        "protocol":"static",
        "selected":true,
        "distance":1,
        "metric":0,
        "nexthops":[
        {
            "ip":"192.168.4.1",
            "afi":"ipv4"
        },
        {
            "fib":true,
            "ip":"192.168.3.1",
            "afi":"ipv4",
            "interfaceIndex":4,
            "interfaceName":"vtss.vlan.30",
            "active":true
        }
        ]
    }
]
}
)";
    auto result = vtss::frr_ip_route_status_parse(ip_route);
    CHECK(result.size() == 1);
    CHECK(result[0].routes.size() == 2);
    CHECK(result[0].routes[0].selected == false);
    CHECK(result[0].routes[1].selected == true);
    CHECK(result[0].routes[1].next_hops.size() == 2);
    CHECK(result[0].routes[1].next_hops[0].selected == false);
    CHECK(result[0].routes[1].next_hops[1].selected == true);
}

TEST_CASE("frr_ip_ospf_status", "[frr]") {
    std::string ip_ospf = R"({
"routerId":"192.168.2.1",
"tosRoutesOnly":true,
"rfc2328Conform":true,
"spfScheduleDelayMsecs":0,
"holdtimeMinMsecs":50,
"holdtimeMaxMsecs":5000,
"holdtimeMultplier":1,
"spfLastExecutedMsecs":766096,
"spfLastDurationMsecs":0,
"lsaMinIntervalMsecs":5000,
"lsaMinArrivalMsecs":1000,
"writeMultiplier":20,
"refreshTimerMsecs":10000,
"lsaExternalCounter":0,
"lsaExternalChecksum":0,
"lsaAsopaqueCounter":0,
"lsaAsOpaqueChecksum":0,
"attachedAreaCounter":1,
"areas":{
    "0.0.0.1":{
    "backbone":true,
    "areaIfTotalCounter":2,
    "areaIfActiveCounter":1,
    "nbrFullAdjacentCounter":0,
    "authentication":"authenticationNone",
    "spfExecutedCounter":1,
    "lsaNumber":1,
    "lsaRouterNumber":1,
    "lsaRouterChecksum":31025,
    "lsaNetworkNumber":1,
    "lsaNetworkChecksum":2,
    "lsaSummaryNumber":3,
    "lsaSummaryChecksum":4,
    "lsaAsbrNumber":5,
    "lsaAsbrChecksum":6,
    "lsaNssaNumber":7,
    "lsaNssaChecksum":8,
    "lsaOpaqueLinkNumber":9,
    "lsaOpaqueLinkChecksum":10,
    "lsaOpaqueAreaNumber":11,
    "lsaOpaqueAreaChecksum":12
    }
}
}
)";
    auto result = vtss::frr_ip_ospf_status_parse(ip_ospf);
    CHECK(result.router_id == 0xc0a80201);
    CHECK(result.tos_routes_only == true);
    CHECK(result.rfc2328 == true);
    CHECK(result.spf_schedule_delay.raw() == 0);
    CHECK(result.hold_time_min.raw() == 50);
    CHECK(result.hold_time_max.raw() == 5000);
    CHECK(result.hold_time_multiplier == 1);
    CHECK(result.spf_last_executed.raw() == 766096);
    CHECK(result.spf_last_duration.raw() == 0);
    CHECK(result.lsa_min_interval.raw() == 5000);
    CHECK(result.lsa_min_arrival.raw() == 1000);
    CHECK(result.write_multiplier == 20);
    CHECK(result.refresh_timer.raw() == 10000);
    CHECK(result.lsa_external_counter == 0);
    CHECK(result.lsa_external_checksum == 0);
    CHECK(result.lsa_asopaque_counter == 0);
    CHECK(result.lsa_asopaque_checksums == 0);
    CHECK(result.attached_area_counter == 1);
    CHECK(result.areas.size() == 1);
    vtss::FrrIpOspfArea area = result.areas[0x01];
    CHECK(area.backbone == true);
    CHECK(area.stub_no_summary == false);
    CHECK(area.stub_shortcut == false);
    CHECK(area.area_if_total_counter == 2);
    CHECK(area.area_if_activ_counter == 1);
    CHECK(area.full_adjancet_counter == 0);
    CHECK(area.authentication == vtss::Authentication_None);
    CHECK(area.spf_executed_counter == 1);
    CHECK(area.lsa_nr == 1);
    CHECK(area.lsa_router_nr == 1);
    CHECK(area.lsa_router_checksum == 31025);
    CHECK(area.lsa_network_nr == 1);
    CHECK(area.lsa_network_checksum == 2);
    CHECK(area.lsa_summary_nr == 3);
    CHECK(area.lsa_summary_checksum == 4);
    CHECK(area.lsa_asbr_nr == 5);
    CHECK(area.lsa_asbr_checksum == 6);
    CHECK(area.lsa_nssa_nr == 7);
    CHECK(area.lsa_nssa_checksum == 8);
    CHECK(area.lsa_opaque_link_nr == 9);
    CHECK(area.lsa_opaque_link_checksum == 10);
    CHECK(area.lsa_opaque_area_nr == 11);
    CHECK(area.lsa_opaque_area_checksum == 12);
}

TEST_CASE("frr_ip_ospf_status_get2", "[frr]") {
    std::string ip_ospf = R"({
"routerId":"12.0.0.122",
"tosRoutesOnly":true,
"rfc2328Conform":true,
"spfScheduleDelayMsecs":0,
"holdtimeMinMsecs":50,
"holdtimeMaxMsecs":5000,
"holdtimeMultplier":1,
"spfLastExecutedMsecs":104238,
"spfLastDurationMsecs":1,
"lsaMinIntervalMsecs":5000,
"lsaMinArrivalMsecs":1000,
"writeMultiplier":20,
"refreshTimerMsecs":10000,
"lsaExternalCounter":0,
"lsaExternalChecksum":0,
"lsaAsopaqueCounter":0,
"lsaAsOpaqueChecksum":0,
"attachedAreaCounter":1,
"areas":{
    "0.0.0.2":{
        "backbone":false,
        "stubNoSummary":true,
        "stubShortcut":true,
        "areaIfTotalCounter":2,
        "areaIfActiveCounter":2,
        "nbrFullAdjacentCounter":2,
        "authentication":"authenticationNone",
        "spfExecutedCounter":6,
        "lsaNumber":6,
        "lsaRouterNumber":3,
        "lsaRouterChecksum":90108,
        "lsaNetworkNumber":3,
        "lsaNetworkChecksum":64139,
        "lsaSummaryNumber":0,
        "lsaSummaryChecksum":0,
        "lsaAsbrNumber":0,
        "lsaAsbrChecksum":0,
        "lsaNssaNumber":0,
        "lsaNssaChecksum":0,
        "lsaOpaqueLinkNumber":0,
        "lsaOpaqueLinkChecksum":0,
        "lsaOpaqueAreaNumber":0,
        "lsaOpaqueAreaChecksum":0
        }
    }
}
)";

    auto result = vtss::frr_ip_ospf_status_parse(ip_ospf);
    CHECK(result.router_id == 0x0c00007A);
    CHECK(result.tos_routes_only == true);
    CHECK(result.rfc2328 == true);
    CHECK(result.spf_schedule_delay.raw() == 0);
    CHECK(result.hold_time_min.raw() == 50);
    CHECK(result.hold_time_max.raw() == 5000);
    CHECK(result.hold_time_multiplier == 1);
    CHECK(result.spf_last_executed.raw() == 104238);
    CHECK(result.spf_last_duration.raw() == 1);
    CHECK(result.lsa_min_interval.raw() == 5000);
    CHECK(result.lsa_min_arrival.raw() == 1000);
    CHECK(result.write_multiplier == 20);
    CHECK(result.refresh_timer.raw() == 10000);
    CHECK(result.lsa_external_counter == 0);
    CHECK(result.lsa_external_checksum == 0);
    CHECK(result.lsa_asopaque_counter == 0);
    CHECK(result.lsa_asopaque_checksums == 0);
    CHECK(result.attached_area_counter == 1);
    CHECK(result.areas.size() == 1);
    vtss::FrrIpOspfArea area = result.areas[0x02];
    CHECK(area.backbone == false);
    CHECK(area.stub_no_summary == true);
    CHECK(area.stub_shortcut == true);
}

TEST_CASE("frr_ip_ospf_interface_parse", "[frr]") {
    SECTION("interface is up") {
        std::string ip_ospf_if = R"({
"yellow":{
    "ifUp":true,
    "ifIndex":3,
    "mtuBytes":1500,
    "bandwidthMbit":1000,
    "ifFlags":"<UP,BROADCAST,RUNNING,MULTICAST>",
    "ospfEnabled":true,
    "ipAddress":"192.168.1.1",
    "ipAddressPrefixlen":24,
    "ospfIfType":"Broadcast",
    "localIfUsed":"192.168.1.255",
    "area":"0.0.0.0",
    "routerId":"0.0.0.1",
    "networkType":"BROADCAST",
    "cost":100,
    "transmitDelayMsecs":1000,
    "state":"Backup",
    "priority":1,
    "bdrId":"0.0.0.1",
    "bdrAddress":"192.168.1.1",
    "networkLsaSequence":-2147483646,
    "mcastMemberOspfAllRouters":true,
    "mcastMemberOspfDesignatedRouters":true,
    "timerMsecs":100,
    "timerDeadMsecs":25,
    "timerWaitMsecs":25,
    "timerRetransmit":200,
    "timerHelloInMsecs":5726,
    "timerPassiveIface":true,
    "nbrCount":1,
    "nbrAdjacentCount":1
}
}
)";

        auto result = vtss::frr_ip_ospf_interface_status_parse(ip_ospf_if);
        vtss_ifindex_t if_index{800000000};
        vtss::FrrIpOspfIfStatus status = result[if_index];
        CHECK(status.if_up == true);
        CHECK(status.os_ifindex == 3);
        CHECK(status.mtu_bytes == 1500);
        CHECK(status.bandwidth_mbit == 1000);
        CHECK(status.if_flags == "<UP,BROADCAST,RUNNING,MULTICAST>");
        CHECK(status.ospf_enabled == true);
        CHECK(status.net.address == 0xc0a80101);
        CHECK(status.net.prefix_size == 24);
        CHECK(status.if_type == vtss::IfType_Broadcast);
        CHECK(status.local_if_used == 0xc0a801FF);
        CHECK(status.area.area == 0x0);
        CHECK(status.router_id == 0x1);
        CHECK(status.network_type == vtss::NetworkType_Broadcast);
        CHECK(status.cost == 100);
        CHECK(status.transmit_delay.raw() == 1000);
        CHECK(status.state == vtss::ISM_Backup);
        CHECK(status.priority == 1);
        CHECK(status.bdr_id == 0x01);
        CHECK(status.bdr_address == 0xc0a80101);
        CHECK(status.network_lsa_sequence == -2147483646);
        CHECK(status.mcast_member_ospf_all_routers == true);
        CHECK(status.mcast_member_ospf_designated_routers == true);
        CHECK(status.timer.raw() == 100);
        CHECK(status.timer_dead.raw() == 25);
        CHECK(status.timer_wait.raw() == 25);
        CHECK(status.timer_retransmit.raw() == 200);
        CHECK(status.timer_hello.raw() == 5726);
        CHECK(status.timer_passive_iface == true);
        CHECK(status.nbr_count == 1);
        CHECK(status.nbr_adjacent_count == 1);
    }

    SECTION("interface is down") {
        std::string ip_ospf_if = R"({
"yellow":{
    "ifDown":false,
    "ifIndex":6,
    "mtuBytes":1500,
    "bandwidthMbit":0,
    "ifFlags":"<BROADCAST,MULTICAST>",
    "ospfRunning":false
}
}
)";
        auto result = vtss::frr_ip_ospf_interface_status_parse(ip_ospf_if);
        vtss_ifindex_t if_index{800000000};
        vtss::FrrIpOspfIfStatus status = result[if_index];
        CHECK(status.if_up == false);
        CHECK(status.os_ifindex == 6);
        CHECK(status.mtu_bytes == 1500);
        CHECK(status.bandwidth_mbit == 0);
        CHECK(status.if_flags == "<BROADCAST,MULTICAST>");
        CHECK(status.ospf_enabled == false);
    }

    SECTION("interface frr vlink") {
        const std::string ip_ospf_if = R"({
"VLINK0":{
    "ifDown":false,
    "ifIndex":0,
    "mtuBytes":3500,
    "bandwidthMbit":0,
    "ifFlags":"<BROADCAST,MULTICAST>",
    "ospfRunning":false
}
}
)";
        auto result = vtss::frr_ip_ospf_interface_status_parse(ip_ospf_if);
        vtss_ifindex_t if_index{800000000};
        vtss::FrrIpOspfIfStatus status = result[if_index];
        CHECK(status.if_up == false);
        CHECK(status.os_ifindex == 0);
        CHECK(status.mtu_bytes == 3500);
        CHECK(status.bandwidth_mbit == 0);
        CHECK(status.if_flags == "<BROADCAST,MULTICAST>");
        CHECK(status.ospf_enabled == false);
    }
}

TEST_CASE("frr_ip_ospf_neighbor_parse", "[frr]") {
    std::string ip_ospf_neighbor = R"({
"192.168.1.3": [
    {
    "address":"192.168.1.3",
    "areaId":"0.0.0.0",
    "ifaceName":"enp9s0f0",
    "priority":1,
    "nbrState":"Full",
    "stateChangeCounter":5,
    "routerDesignatedId":"192.168.1.3",
    "routerDesignatedBackupId":"192.168.1.1",
    "optionsCounter":3,
    "optionsList":"*|-|-|-|-|-|E|-"
  }
  ]
}
)";

    auto result = vtss::frr_ip_ospf_neighbor_status_parse(ip_ospf_neighbor);
    vtss::FrrIpOspfNeighborStatus neighbor = result[0xc0a80103][0];
    CHECK(neighbor.if_address == 0xc0a80103);
    CHECK(neighbor.area.area == 0);
    CHECK(neighbor.os_ifindex == 0);
    CHECK(neighbor.nbr_priority == 1);
    CHECK(neighbor.nbr_state == vtss::NSM_Full);
    CHECK(neighbor.router_designated_id == 0xc0a80103);
    CHECK(neighbor.router_designated_bck_id == 0xc0a80101);
    CHECK(neighbor.options_counter == 3);
    CHECK(neighbor.options_list == "*|-|-|-|-|-|E|-");
}

TEST_CASE("frr_ip_ospf_neighbor_status_parse", "[frr]") {
    const std::string ip_ospf_neighbor = R"({
"192.168.1.3": [
    {
    "address":"192.168.1.3",
    "areaId":"0.0.0.1 [Stub]",
    "ifaceName":"vtss.vlan.1",
    "priority":1,
    "nbrState":"Full",
    "stateChangeCounter":5,
    "routerDesignatedId":"192.168.1.3",
    "routerDesignatedBackupId":"192.168.1.2",
    "optionsCounter":2,
    "optionsList":"*||||||E|"
  }
  ]
}
)";

    auto result = vtss::frr_ip_ospf_neighbor_status_parse(ip_ospf_neighbor);
    vtss::FrrIpOspfNeighborStatus neighbor = result[0xc0a80103][0];
    CHECK(neighbor.if_address == 0xc0a80103);
    CHECK(neighbor.area.area == 1);
    CHECK(neighbor.os_ifindex == 0);
    CHECK(neighbor.nbr_priority == 1);
    CHECK(neighbor.nbr_state == vtss::NSM_Full);
    CHECK(neighbor.router_designated_id == 0xc0a80103);
    CHECK(neighbor.router_designated_bck_id == 0xc0a80102);
    CHECK(neighbor.options_counter == 2);
    CHECK(neighbor.options_list == "*||||||E|");
    CHECK(neighbor.transit_id.area == 0x0);
}

TEST_CASE("frr_ip_ospf_neighbor_status_parse_transit_id", "[frr]") {
    const std::string ip_ospf_neighbor = R"({
"192.168.1.3": [
    {
    "priority":1,
    "state":"Full\/DROther",
    "nbrState":"Full",
    "deadTimeMsecs":31273,
    "areaId":"0.0.0.0",
    "transitAreaId":"0.0.0.4",
    "ifaceName":"VLINK0",
    "address":"192.168.1.3",
    "routerDesignatedId":"0.0.0.0",
    "routerDesignatedBackupId":"0.0.0.0",
    "optionsList":"*|-|-|-|-|-|E|-",
    "optionsCounter":2,
    "retransmitCounter":0,
    "requestCounter":0,
    "dbSummaryCounter":0
    }
    ]
})";

    auto result = vtss::frr_ip_ospf_neighbor_status_parse(ip_ospf_neighbor);
    vtss::FrrIpOspfNeighborStatus neighbor = result[0xc0a80103][0];
    CHECK(neighbor.if_address == 0xc0a80103);
    CHECK(neighbor.transit_id.area == 0x04);
}

TEST_CASE("frr_ip_ospf_neighbor_status_parse_multiple", "[frr]") {
    const std::string ip_ospf_neighbor = R"({
"192.168.1.3": [
    {
    "address":"192.168.1.3",
    "areaId":"0.0.0.1 [Stub]",
    "ifaceName":"vtss.vlan.1",
    "priority":1,
    "nbrState":"Full",
    "stateChangeCounter":5,
    "routerDesignatedId":"192.168.1.3",
    "routerDesignatedBackupId":"192.168.1.2",
    "optionsCounter":2,
    "optionsList":"*||||||E|"
  },
  {
    "address":"192.168.1.4",
    "areaId":"0.0.0.2",
    "ifaceName":"vtss.vlan.2",
    "priority":2,
    "nbrState":"Full",
    "stateChangeCounter":5,
    "routerDesignatedId":"192.168.1.4",
    "routerDesignatedBackupId":"192.168.1.5",
    "optionsCounter":2,
    "optionsList":"*||||||E|"
  }
  ]
}
)";
    auto result = vtss::frr_ip_ospf_neighbor_status_parse(ip_ospf_neighbor);
    CHECK(result[0xc0a80103].size() == 2);
    vtss::FrrIpOspfNeighborStatus neighbor = result[0xc0a80103][0];
    CHECK(neighbor.if_address == 0xc0a80103);
    CHECK(neighbor.area.area == 1);
    CHECK(neighbor.os_ifindex == 0);
    CHECK(neighbor.nbr_priority == 1);
    CHECK(neighbor.nbr_state == vtss::NSM_Full);
    CHECK(neighbor.router_designated_id == 0xc0a80103);
    CHECK(neighbor.router_designated_bck_id == 0xc0a80102);
    CHECK(neighbor.options_counter == 2);
    CHECK(neighbor.options_list == "*||||||E|");
    neighbor = result[0xc0a80103][1];
    CHECK(neighbor.if_address == 0xc0a80104);
    CHECK(neighbor.area.area == 2);
    CHECK(neighbor.os_ifindex == 0);
    CHECK(neighbor.nbr_priority == 2);
    CHECK(neighbor.nbr_state == vtss::NSM_Full);
    CHECK(neighbor.router_designated_id == 0xc0a80104);
    CHECK(neighbor.router_designated_bck_id == 0xc0a80105);
    CHECK(neighbor.options_counter == 2);
    CHECK(neighbor.options_list == "*||||||E|");
}

TEST_CASE("to_vty_ospf_router_conf_del", "[frr]") {
    auto result = vtss::to_vty_ospf_router_conf_del();
    CHECK(result.size() == 2);
    CHECK(result[0] == "configure terminal");
    CHECK(result[1] == "no router ospf");
}

TEST_CASE("to_vty_ospf_router_conf_set", "[frr]") {
    SECTION("default FrrOspfRouterConf") {
        auto result = vtss::to_vty_ospf_router_conf_set();
        CHECK(result.size() == 3);
        CHECK(result[0] == "configure terminal");
        CHECK(result[1] == "no router ospf");
        CHECK(result[2] == "router ospf");
    }
    SECTION("using FrrOspfRouterConf") {
        vtss::FrrOspfRouterConf conf;
        conf.ospf_router_id = 3;
        conf.ospf_router_rfc1583 = true;
        auto result = vtss::to_vty_ospf_router_conf_set(conf);
        CHECK(result.size() == 4);
        CHECK(result[0] == "configure terminal");
        CHECK(result[1] == "router ospf");
        CHECK(result[2] == "ospf router-id 0.0.0.3");
        CHECK(result[3] == "ospf rfc1583compatibility");
    }
    SECTION("set all fields in FrrOspRouterConf") {
        vtss::FrrOspfRouterConf conf;
        conf.ospf_router_id = 4;
        conf.ospf_router_rfc1583 = false;
        conf.ospf_router_abr_type = vtss::AbrType_Standard;
        auto result = vtss::to_vty_ospf_router_conf_set(conf);
        CHECK(result.size() == 5);
        CHECK(result[0] == "configure terminal");
        CHECK(result[1] == "router ospf");
        CHECK(result[2] == "ospf router-id 0.0.0.4");
        CHECK(result[3] == "no ospf rfc1583compatibility");
        CHECK(result[4] == "ospf abr-type standard");
    }
}

TEST_CASE("frr_ospf_router_conf_get", "[frr]") {
    std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
router ospf
    ospf router-id 0.1.2.3
    ospf abr-type shortcut
    refresh timer 40
    compatible rfc1583
    network 192.168.1.0/24 area 0.0.0.0
!
ip route 0.0.0.1/8 0.0.0.6 9
!
line vty
!
)";

    SECTION("invalid instance") {
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = config;
        CHECK(bool(frr_ospf_router_conf_get(frr_conf, 0)) == false);
    }

    SECTION("parse ospf router") {
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = config;
        auto result = frr_ospf_router_conf_get(frr_conf, 1);
        CHECK(result->ospf_router_id.valid() == true);
        CHECK(result->ospf_router_id.get() == 0x10203);
        CHECK(result->ospf_router_rfc1583.valid() == true);
        CHECK(result->ospf_router_rfc1583.get() == true);
        CHECK(result->ospf_router_abr_type.valid() == true);
        CHECK(result->ospf_router_abr_type.get() == vtss::AbrType_Shortcut);
    }

    std::string empty_config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
router ospf
!
ip route 0.0.0.1/8 0.0.0.6 9
!
line vty
!
)";

    SECTION("check if no values") {
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = empty_config;
        auto result = frr_ospf_router_conf_get(frr_conf, 1);
        CHECK(result->ospf_router_id.valid() == false);
        CHECK(result->ospf_router_rfc1583.valid() == false);
        CHECK(result->ospf_router_abr_type.valid() == false);
    }

    std::string no_config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
ip route 0.0.0.1/8 0.0.0.6 9
!
line vty
!
)";

    SECTION("check if no router ospf") {
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = no_config;
        auto result = frr_ospf_router_conf_get(frr_conf, 0);
        CHECK(result->ospf_router_id.valid() == false);
        CHECK(result->ospf_router_rfc1583.valid() == false);
        CHECK(result->ospf_router_abr_type.valid() == false);
    }
}

// frr_ospf_router_default_metric
TEST_CASE("to_vty_ospf_router_default_metric_conf_set", "[frr]") {
    auto cmds = vtss::to_vty_ospf_router_default_metric_conf_set(42);
    CHECK(cmds.size() == 3);
    CHECK(cmds[0] == "configure terminal");
    CHECK(cmds[1] == "router ospf");
    CHECK(cmds[2] == "default-metric 42");
}

TEST_CASE("to_vty_ospf_router_default_metric_conf_del", "[frr]") {
    auto cmds = vtss::to_vty_ospf_router_default_metric_conf_del();
    CHECK(cmds.size() == 3);
    CHECK(cmds[0] == "configure terminal");
    CHECK(cmds[1] == "router ospf");
    CHECK(cmds[2] == "no default-metric");
}

TEST_CASE("frr_ospf_router_default_metric_conf_get", "[frr]") {
    SECTION("invalid instance") {
        vtss::FrrConf config;
        config.ospfd = "";
        auto res = vtss::frr_ospf_router_default_metric_conf_get(config, {0});
        CHECK(res.rc == MESA_RC_ERROR);
    }
    SECTION("parse default-metric") {
        std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    default-metric 42
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_router_default_metric_conf_get(frr_config, {1});
        CHECK(res.rc == MESA_RC_OK);
        CHECK(res.val.valid() == true);
        CHECK(res.val.get() == 42);
    }

    SECTION("parse no default-metric") {
        std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_router_default_metric_conf_get(frr_config, {1});
        CHECK(res.rc == MESA_RC_OK);
        CHECK(res.val.valid() == false);
    }
}

// frr_ospf_router_distribute_conf
TEST_CASE("frr_ospf_router_redistribute_conf_set", "[frr]") {
    vtss::FrrOspfRouterRedistribute val;
    val.protocol = vtss::Protocol_Connected;
    val.metric_type = vtss::MetricType_One;
    val.metric = 42;
    val.route_map = "route_map";

    auto cmds = vtss::to_vty_ospf_router_redistribute_conf_set(val);
    CHECK(cmds.size() == 3);
    CHECK(cmds[0] == "configure terminal");
    CHECK(cmds[1] == "router ospf");
    CHECK(cmds[2] ==
          "redistribute connected metric 42");
}

TEST_CASE("frr_ospf_router_redistribute_conf_del", "[frr]") {
    vtss::FrrOspfRouterRedistributeProtocol protocol = vtss::Protocol_Kernel;
    auto cmds = vtss::to_vty_ospf_router_redistribute_conf_del(protocol);
    CHECK(cmds.size() == 3);
    CHECK(cmds[0] == "configure terminal");
    CHECK(cmds[1] == "router ospf");
    CHECK(cmds[2] == "no redistribute kernel");
}

TEST_CASE("frr_ospf_router_redistribute_conf_get", "[frr]") {
    SECTION("invalid instance") {
        vtss::FrrConf conf;
        conf.ospfd = "";
        auto res = vtss::frr_ospf_router_redistribute_conf_get(conf, {0});
        CHECK(res.size() == 0);
    }

    SECTION("parse multiple protocols") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    redistribute kernel metric 1000 metric-type 1 route-map ASD
    redistribute connected metric 1000 metric-type 1
    redistribute rip
    redistribute static route-map ADD
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_router_redistribute_conf_get(frr_config, {1});
        CHECK(res.size() == 4);
        CHECK(res[0].protocol == vtss::Protocol_Kernel);
        CHECK(res[0].metric_type == vtss::MetricType_One);
        CHECK(res[0].metric.valid() == true);
        CHECK(res[0].metric.get() == 1000);
        CHECK(res[0].route_map.valid() == true);
        CHECK(res[0].route_map.get() == "ASD");
        CHECK(res[1].protocol == vtss::Protocol_Connected);
        CHECK(res[1].metric_type == vtss::MetricType_One);
        CHECK(res[1].metric.valid() == true);
        CHECK(res[1].metric.get() == 1000);
        CHECK(res[1].route_map.valid() == false);
        CHECK(res[2].protocol == vtss::Protocol_Rip);
        CHECK(res[2].metric_type == vtss::MetricType_Two);
        CHECK(res[2].metric.valid() == false);
        CHECK(res[2].route_map.valid() == false);
        CHECK(res[3].protocol == vtss::Protocol_Static);
        CHECK(res[3].metric_type == vtss::MetricType_Two);
        CHECK(res[3].metric.valid() == false);
        CHECK(res[3].route_map.valid() == true);
        CHECK(res[3].route_map.get() == "ADD");
    }
}

// frr_ospf_router_default_information
TEST_CASE("to_vty_ospf_router_default_information_conf_set", "[frr]") {
    vtss::FrrOspfRouterDefaultInformation info;
    info.always = true;
    info.metric_type = vtss::MetricType_One;
    info.metric = 1000;
    info.route_map = "test_route_map";

    auto cmds = vtss::to_vty_ospf_router_default_information_conf_set(info);
    CHECK(cmds.size() == 3);
    CHECK(cmds[0] == "configure terminal");
    CHECK(cmds[1] == "router ospf");
    CHECK(cmds[2] ==
          "default-information originate always metric 1000 metric-type 1 "
          "route-map test_route_map");
}

TEST_CASE("to_vty_ospf_router_default_information_conf_del", "[frr]") {
    auto cmds = vtss::to_vty_ospf_router_default_information_conf_del();
    CHECK(cmds.size() == 3);
    CHECK(cmds[0] == "configure terminal");
    CHECK(cmds[1] == "router ospf");
    CHECK(cmds[2] == "no default-information");
}

TEST_CASE("frr_ospf_router_default_information_conf_get", "[frr]") {
    SECTION("invalid instance") {
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = "";
        auto res = frr_ospf_router_default_information_conf_get(frr_conf, {0});
        CHECK(res.rc == MESA_RC_ERROR);
    }

    SECTION("parse with always") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
!
router ospf
    default-information originate always route-map NNNN
!
!
)";
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = config;
        auto res = frr_ospf_router_default_information_conf_get(frr_conf, {1});
        CHECK(res.rc == MESA_RC_OK);
        CHECK(res.val.always == true);
        CHECK(res.val.metric.valid() == false);
        CHECK(res.val.metric_type == vtss::MetricType_Two);
        CHECK(res.val.route_map.valid() == true);
    }

    SECTION("parse without always") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
!
router ospf
    default-information originate metric 1230 metric-type 1
!
!
)";
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = config;
        auto res = frr_ospf_router_default_information_conf_get(frr_conf, {1});
        CHECK(res.rc == MESA_RC_OK);
        CHECK(res.val.always == false);
        CHECK(res.val.metric.valid() == true);
        CHECK(res.val.metric.get() == 1230);
        CHECK(res.val.metric_type == vtss::MetricType_One);
        CHECK(res.val.route_map.valid() == false);
    }

    SECTION("parse without anything") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
!
router ospf
    default-information originate
!
!
)";
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = config;
        auto res = frr_ospf_router_default_information_conf_get(frr_conf, {1});
        CHECK(res.rc == MESA_RC_OK);
        CHECK(res.val.always == false);
        CHECK(res.val.metric.valid() == false);
        CHECK(res.val.metric_type == vtss::MetricType_Two);
        CHECK(res.val.route_map.valid() == false);
    }

    SECTION("parse only route-map") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
!
router ospf
    default-information originate route-map ASBCDREE21
!
!
)";
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = config;
        auto res = frr_ospf_router_default_information_conf_get(frr_conf, {1});
        CHECK(res.rc == MESA_RC_OK);
        CHECK(res.val.always == false);
        CHECK(res.val.metric.valid() == false);
        CHECK(res.val.metric_type == vtss::MetricType_Two);
        CHECK(res.val.route_map.valid() == true);
        CHECK(res.val.route_map.get() == "ASBCDREE21");
    }
}

TEST_CASE("to_vty_ospf_area_network_conf_set", "[frr]") {
    vtss::FrrOspfAreaNetwork val;
    val.net.address = 0x01;
    val.net.prefix_size = 8;
    val.area = 0x02;

    auto result = to_vty_ospf_area_network_conf_set(val);
    CHECK(result.size() == 3);
    CHECK(result[0] == "configure terminal");
    CHECK(result[1] == "router ospf");
    CHECK(result[2] == "network 0.0.0.1/8 area 0.0.0.2");
}

TEST_CASE("to_vty_ospf_area_network_conf_del", "[frr]") {
    vtss::FrrOspfAreaNetwork val;
    val.net.address = 0x03;
    val.net.prefix_size = 16;
    val.area = 0x05;

    auto result = to_vty_ospf_area_network_conf_del(val);
    CHECK(result.size() == 3);
    CHECK(result[0] == "configure terminal");
    CHECK(result[1] == "router ospf");
    CHECK(result[2] == "no network 0.0.0.3/16 area 0.0.0.5");
}

TEST_CASE("frr_ospf_area_network_conf_get", "[frr]") {
    std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
router ospf
    ospf router-id 0.1.2.3
    ospf abr-type shortcut
    refresh timer 40
    compatible rfc1583
    network 192.168.1.0/24 area 0.0.0.0
!
ip route 0.0.0.1/8 0.0.0.6 9
!
line vty
!
)";

    SECTION("invalid instance") {
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = config;
        CHECK(frr_ospf_area_network_conf_get(frr_conf, 0).size() == 0);
    }

    SECTION("parse network area") {
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = config;
        auto result = frr_ospf_area_network_conf_get(frr_conf, 1);
        CHECK(result.size() == 1);
        CHECK(result[0].net.address == 0xc0a80100);
        CHECK(result[0].net.prefix_size == 24);
        CHECK(result[0].area == 0);
    }

    std::string no_network = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
router ospf
    ospf router-id 0.1.2.3
    ospf abr-type shortcut
    refresh timer 40
    compatible rfc1583
!
ip route 0.0.0.1/8 0.0.0.6 9
!
line vty
!
)";

    SECTION("parse no network area") {
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = config;
        auto result = frr_ospf_area_network_conf_get(frr_conf, 1);
        CHECK(result.size() == 1);
    }
}

// frr_ospf_area_range
TEST_CASE("to_vty_ospf_area_range_conf_set", "[frr]") {
    vtss::FrrOspfAreaNetwork val;
    val.net.address = 10;
    val.net.prefix_size = 24;
    val.area = 2;

    auto res = vtss::to_vty_ospf_area_range_conf_set(val);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "router ospf");
    CHECK(res[2] == "area 0.0.0.2 range 0.0.0.10/24");
}

TEST_CASE("to_vty_ospf_area_range_conf_del", "[frr]") {
    vtss::FrrOspfAreaNetwork val;
    val.net.address = 20;
    val.net.prefix_size = 16;
    val.area = 3;

    auto res = vtss::to_vty_ospf_area_range_conf_del(val);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "router ospf");
    CHECK(res[2] == "no area 0.0.0.3 range 0.0.0.20/16");
}

TEST_CASE("frr_ospf_area_range_conf_get", "[frr]") {
    SECTION("parse invalid instance") {
        vtss::FrrConf frr_config;
        frr_config.ospfd = "";
        auto res = frr_ospf_area_range_conf_get(frr_config, {0});
        CHECK(res.size() == 0);
    }

    SECTION("parse no area") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = frr_ospf_area_range_conf_get(frr_config, {1});
        CHECK(res.size() == 0);
    }

    SECTION("parse multiple areas") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    area 0.0.0.0 range 10.0.0.0/8
    area 0.0.0.1 range 20.0.0.0/16
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = frr_ospf_area_range_conf_get(frr_config, {1});
        CHECK(res.size() == 2);
        CHECK(res[0].area == 0);
        CHECK(res[0].net.address == 0xa000000);
        CHECK(res[0].net.prefix_size == 8);
        CHECK(res[1].area == 1);
        CHECK(res[1].net.address == 0x14000000);
        CHECK(res[1].net.prefix_size == 16);
    }

    SECTION("parse different areas") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    area 0.0.0.0 range 10.0.0.0/8
    area 0.0.0.1 range 20.0.0.0/16 not-advertise
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_range_conf_get(frr_config, {1});
        CHECK(res.size() == 2);
        CHECK(res[0].area == 0);
        CHECK(res[0].net.address == 0xa000000);
        CHECK(res[0].net.prefix_size == 8);
        CHECK(res[1].area == 1);
        CHECK(res[1].net.address == 0x14000000);
        CHECK(res[1].net.prefix_size == 16);
    }
}

// frr_ospf_area_range_not_advertise
TEST_CASE("to_vty_ospf_area_range_not_advertise_set", "[frr]") {
    vtss::FrrOspfAreaNetwork net;
    net.area = 1;
    net.net.address = 2;
    net.net.prefix_size = 16;

    auto res = vtss::to_vty_ospf_area_range_not_advertise_conf_set(net);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "router ospf");
    CHECK(res[2] == "area 0.0.0.1 range 0.0.0.2/16 not-advertise");
}

TEST_CASE("to_vty_ospf_area_range_not_advertise_del", "[frr]") {
    vtss::FrrOspfAreaNetwork net;
    net.area = 2;
    net.net.address = 3;
    net.net.prefix_size = 24;

    auto res = vtss::to_vty_ospf_area_range_not_advertise_conf_del(net);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "router ospf");
    CHECK(res[2] == "no area 0.0.0.2 range 0.0.0.3/24 not-advertise");
}

TEST_CASE("frr_ospf_area_range_not_advertise_conf_get", "[frr]") {
    SECTION("invalid instance") {
        vtss::FrrConf frr_config;
        frr_config.ospfd = "";

        auto res = vtss::frr_ospf_area_range_not_advertise_conf_get(frr_config,
                                                                    {0});
        CHECK(res.size() == 0);
    }

    SECTION("parse no area") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_range_not_advertise_conf_get(frr_config,
                                                                    {1});
        CHECK(res.size() == 0);
    }

    SECTION("parse not advertise area") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    area 0.0.0.6 range 0.0.0.7/24 not-advertise
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_range_not_advertise_conf_get(frr_config,
                                                                    {1});
        CHECK(res.size() == 1);
        CHECK(res[0].area == 6);
        CHECK(res[0].net.address == 0x07);
        CHECK(res[0].net.prefix_size == 24);
    }

    SECTION("parse different areas") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    area 0.0.0.6 range 0.0.0.7/24 not-advertise
    area 0.0.0.8 range 0.0.0.9/24
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_range_not_advertise_conf_get(frr_config,
                                                                    {1});
        CHECK(res.size() == 1);
        CHECK(res[0].area == 6);
        CHECK(res[0].net.address == 0x07);
        CHECK(res[0].net.prefix_size == 24);
    }
}

// frr_ospf_area_range_cost
TEST_CASE("to_vty_ospf_area_range_cost_conf_set", "[frr]") {
    vtss::FrrOspfAreaNetworkCost val;
    val.net.address = 2;
    val.net.prefix_size = 16;
    val.area = 1;
    val.cost = 20;

    auto res = to_vty_ospf_area_range_cost_conf_set(val);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "router ospf");
    CHECK(res[2] == "area 0.0.0.1 range 0.0.0.2/16 cost 20");
}

TEST_CASE("to_vty_ospf_area_range_cost_conf_del", "[frr]") {
    vtss::FrrOspfAreaNetworkCost val;
    val.net.address = 3;
    val.net.prefix_size = 8;
    val.area = 2;
    val.cost = 30;

    auto res = to_vty_ospf_area_range_cost_conf_del(val);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "router ospf");
    CHECK(res[2] == "no area 0.0.0.2 range 0.0.0.3/8 cost 0");
}

TEST_CASE("frr_ospf_area_range_cost_conf_get", "[frr]") {
    SECTION("invalid instance") {
        vtss::FrrConf frr_config;
        frr_config.ospfd = "";
        auto res = vtss::frr_ospf_area_range_cost_conf_get(frr_config, {0});
        CHECK(res.size() == 0);
    }

    SECTION("parse no area") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_range_cost_conf_get(frr_config, {1});
        CHECK(res.size() == 0);
    }

    SECTION("parse cost area") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    area 0.0.0.6 range 0.0.0.7/24 cost 80
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_range_cost_conf_get(frr_config, {1});
        CHECK(res.size() == 1);
        CHECK(res[0].area == 6);
        CHECK(res[0].net.address == 0x07);
        CHECK(res[0].net.prefix_size == 24);
        CHECK(res[0].cost == 80);
    }
}

// frr_ospf_virtual_link
TEST_CASE("to_vty_ospf_area_virtual_link_conf_set", "[frr]") {
    SECTION("all default") {
        vtss::FrrOspfAreaVirtualLink val;
        val.area = 1;
        val.dst = 2;

        auto res = vtss::to_vty_ospf_area_virtual_link_conf_set(val);
        CHECK(res.size() == 3);
        CHECK(res[0] == "configure terminal");
        CHECK(res[1] == "router ospf");
        CHECK(res[2] == "area 0.0.0.1 virtual-link 0.0.0.2");
    }

    SECTION("set interval") {
        vtss::FrrOspfAreaVirtualLink val;
        val.area = 1;
        val.dst = 2;
        val.hello_interval = 20;
        val.dead_interval = 30;

        auto res = vtss::to_vty_ospf_area_virtual_link_conf_set(val);
        CHECK(res.size() == 3);
        CHECK(res[0] == "configure terminal");
        CHECK(res[1] == "router ospf");
        CHECK(res[2] ==
              "area 0.0.0.1 virtual-link 0.0.0.2 hello-interval 20 "
              "dead-interval 30");
    }
}

TEST_CASE("to_vty_ospf_area_virtual_link_conf_del", "[frr]") {
    vtss::FrrOspfAreaVirtualLink val;
    val.area = 3;
    val.dst = 4;

    auto res = vtss::to_vty_ospf_area_virtual_link_conf_del(val);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "router ospf");
    CHECK(res[2] == "no area 0.0.0.3 virtual-link 0.0.0.4");
}

TEST_CASE("frr_ospf_area_virtual_link_conf_get", "[frr]") {
    SECTION("parse invalid instace") {
        vtss::FrrConf frr_config;
        frr_config.ospfd = "";
        auto res = vtss::frr_ospf_area_virtual_link_conf_get(frr_config, {0});
        CHECK(res.size() == 0);
    }

    SECTION("parse no virtual link") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_virtual_link_conf_get(frr_config, {1});
        CHECK(res.size() == 0);
    }

    SECTION("parse multiple virtual links") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    area 0.0.0.3 virtual-link 0.0.0.4
    area 0.0.0.2 virtual-link 0.0.0.5
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_virtual_link_conf_get(frr_config, {1});
        CHECK(res.size() == 2);
        CHECK(res[0].area == 3);
        CHECK(res[0].dst == 4);
        CHECK(res[1].area == 2);
        CHECK(res[1].dst == 5);
    }

    SECTION("parser virtual links with intervals") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    area 0.0.0.3 virtual-link 0.0.0.4
    area 0.0.0.4 virtual-link 0.0.0.6 hello-interval 20 retransmit-interval 50 transmit-delay 10 dead-interval 100
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_virtual_link_conf_get(frr_config, {1});
        CHECK(res.size() == 2);
        CHECK(res[0].area == 3);
        CHECK(res[0].dst == 4);
        CHECK(res[1].area == 4);
        CHECK(res[1].dst == 6);
        CHECK(res[1].hello_interval.get() == 20);
        CHECK(res[1].retransmit_interval.get() == 50);
        CHECK(res[1].transmit_delay.get() == 10);
        CHECK(res[1].dead_interval.get() == 100);
    }
}

// frr_ospf_area_stub
TEST_CASE("to_vty_ospf_area_stub_conf_set", "[frr]") {
    auto res = vtss::to_vty_ospf_area_stub_conf_set(1);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "router ospf");
    CHECK(res[2] == "area 0.0.0.1 stub");
}

TEST_CASE("to_vty_ospf_area_stub_conf_del", "[frr]") {
    auto res = vtss::to_vty_ospf_area_stub_conf_del(2);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "router ospf");
    CHECK(res[2] == "no area 0.0.0.2 stub");
}

TEST_CASE("frr_ospf_area_stub_conf_get", "[frr]") {
    SECTION("parse invalid instance") {
        vtss::FrrConf frr_config;
        frr_config.ospfd = "";
        auto res = vtss::frr_ospf_area_stub_conf_get(frr_config, {0});
        CHECK(res.size() == 0);
    }

    SECTION("parse no stub area") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_stub_conf_get(frr_config, {1});
        CHECK(res.size() == 0);
    }

    SECTION("parse multiple stub areas") {
        const std::string config = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    area 0.0.0.4 stub
    area 0.0.0.2 stub
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_stub_conf_get(frr_config, {1});
        CHECK(res.size() == 2);
        CHECK(res[0] == 4);
        CHECK(res[1] == 2);
    }

    SECTION("parse stub and stub no-summary") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    area 0.0.0.4 stub
    area 0.0.0.3 stub no-summary
    area 0.0.0.2 stub
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_stub_conf_get(frr_config, 1);
        CHECK(res.size() == 2);
    }
}

// frr_ospf_area_authentication
TEST_CASE("to_vty_ospf_area_authentication_set", "[frr]") {
    SECTION("set mode pwd") {
        const mesa_ipv4_t area = 3;
        vtss::FrrOspfAuthMode mode = vtss::FRR_OSPF_AUTH_MODE_PWD;

        auto res = to_vty_ospf_area_authentication_conf_set(area, mode);
        CHECK(res.size() == 3);
        CHECK(res[0] == "configure terminal");
        CHECK(res[1] == "router ospf");
        CHECK(res[2] == "area 0.0.0.3 authentication");
    }

    SECTION("set mod msg digest") {
        const mesa_ipv4_t area = 4;
        vtss::FrrOspfAuthMode mode = vtss::FRR_OSPF_AUTH_MODE_MSG_DIGEST;

        auto res = to_vty_ospf_area_authentication_conf_set(area, mode);
        CHECK(res.size() == 3);
        CHECK(res[0] == "configure terminal");
        CHECK(res[1] == "router ospf");
        CHECK(res[2] == "area 0.0.0.4 authentication message-digest");
    }

    SECTION("set mode null") {
        const mesa_ipv4_t area = 5;
        vtss::FrrOspfAuthMode mode = vtss::FRR_OSPF_AUTH_MODE_NULL;

        auto res = to_vty_ospf_area_authentication_conf_set(area, mode);
        CHECK(res.size() == 3);
        CHECK(res[0] == "configure terminal");
        CHECK(res[1] == "router ospf");
        CHECK(res[2] == "no area 0.0.0.5 authentication");
    }
}

TEST_CASE("frr_ospf_area_authentication_conf_get", "[frr]") {
    const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
router ospf
    area 0.0.0.0 authentication
!
line vty
!
)";

    SECTION("invalid instance") {
        vtss::FrrConf conf;
        conf.ospfd = config;
        auto res = vtss::frr_ospf_area_authentication_conf_get(conf, 0);
        CHECK(res.size() == 0);
    }

    SECTION("parse required area") {
        vtss::FrrConf conf;
        conf.ospfd = config;
        auto res = vtss::frr_ospf_area_authentication_conf_get(conf, 1);
        CHECK(res.size() == 1);
        CHECK(res[0].area == 0);
        CHECK(res[0].auth_mode == vtss::FRR_OSPF_AUTH_MODE_PWD);
    }

    const std::string config_digest = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
router ospf
    area 0.0.0.0 authentication message-digest
!
line vty
!
)";
    SECTION("parse message-digest") {
        vtss::FrrConf conf;
        conf.ospfd = config_digest;
        auto res = vtss::frr_ospf_area_authentication_conf_get(conf, 1);
        CHECK(res.size() == 1);
        CHECK(res[0].area == 0);
        CHECK(res[0].auth_mode == vtss::FRR_OSPF_AUTH_MODE_MSG_DIGEST);
    }
}

// frr_ospf_area_stub_no_summary_conf
TEST_CASE("to_vty_area_stub_no_summary_conf_set", "[frr]") {
    auto res = vtss::to_vty_ospf_area_stub_no_summary_conf_set(1);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "router ospf");
    CHECK(res[2] == "area 0.0.0.1 stub no-summary");
}

TEST_CASE("to_vty_area_stub_no_summary_conf_del", "[frr]") {
    auto res = vtss::to_vty_ospf_area_stub_no_summary_conf_del(2);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "router ospf");
    CHECK(res[2] == "no area 0.0.0.2 stub no-summary");
}

TEST_CASE("frr_ospf_area_stub_no_summary_conf_get", "[frr]") {
    SECTION("invalid instance") {
        vtss::FrrConf frr_config;
        frr_config.ospfd = "";
        auto res = vtss::frr_ospf_area_stub_no_summary_conf_get(frr_config, 0);
        CHECK(res.size() == 0);
    }

    SECTION("nothing to parse") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_stub_no_summary_conf_get(frr_config, 1);
        CHECK(res.size() == 0);
    }

    SECTION("parse multiple") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    area 0.0.0.2 stub no-summary
    area 0.0.0.4 stub no-summary
    area 0.0.0.5 stub
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_stub_no_summary_conf_get(frr_config, 1);
        CHECK(res.size() == 2);
        CHECK(res[0] == 2);
        CHECK(res[1] == 4);
    }
}

TEST_CASE("frr_ospf_area_virtual_link_authentication_conf_set", "[frr]") {
    vtss::FrrOspfAreaVirtualLink link;
    link.area = 1;
    link.dst = 2;
    SECTION("no authentication") {
        auto res = vtss::to_vty_ospf_area_virtual_link_authentication_conf_set(
                link, vtss::FRR_OSPF_AUTH_MODE_NULL);
        CHECK(res.size() == 3);
        CHECK(res[0] == "configure terminal");
        CHECK(res[1] == "router ospf");
        CHECK(res[2] ==
              "area 0.0.0.1 virtual-link 0.0.0.2 authentication null");
    }

    SECTION("pass authentication") {
        auto res = vtss::to_vty_ospf_area_virtual_link_authentication_conf_set(
                link, vtss::FRR_OSPF_AUTH_MODE_PWD);
        CHECK(res.size() == 3);
        CHECK(res[0] == "configure terminal");
        CHECK(res[1] == "router ospf");
        CHECK(res[2] == "area 0.0.0.1 virtual-link 0.0.0.2 authentication");
    }

    SECTION("message-digest authentication") {
        auto res = vtss::to_vty_ospf_area_virtual_link_authentication_conf_set(
                link, vtss::FRR_OSPF_AUTH_MODE_MSG_DIGEST);
        CHECK(res.size() == 3);
        CHECK(res[0] == "configure terminal");
        CHECK(res[1] == "router ospf");
        CHECK(res[2] ==
              "area 0.0.0.1 virtual-link 0.0.0.2 authentication "
              "message-digest");
    }
}

TEST_CASE("frr_ospf_area_virtual_link_authentication_conf_get", "[frr]") {

    SECTION("invalid instance") {
        vtss::FrrConf config;
        config.ospfd = "";
        auto res = vtss::frr_ospf_area_virtual_link_authentication_conf_get(
                config, {0});
        CHECK(res.size() == 0);
    }

    SECTION("no authentication") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    area 0.0.0.1 virtual-link 0.0.0.2 authentication null
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_virtual_link_authentication_conf_get(
                frr_config, {1});
        CHECK(res.size() == 1);
        CHECK(res[0].virtual_link.area == 1);
        CHECK(res[0].virtual_link.dst == 2);
        CHECK(res[0].auth_mode == vtss::FRR_OSPF_AUTH_MODE_NULL);
    }

    SECTION("password authentication") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    area 0.0.0.1 virtual-link 0.0.0.2 authentication
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_virtual_link_authentication_conf_get(
                frr_config, {1});
        CHECK(res.size() == 1);
        CHECK(res[0].virtual_link.area == 1);
        CHECK(res[0].virtual_link.dst == 2);
        CHECK(res[0].auth_mode == vtss::FRR_OSPF_AUTH_MODE_PWD);
    }

    SECTION("password authentication-key (authentication type should not be changed)") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    area 0.0.0.1 virtual-link 0.0.0.2
    area 0.0.0.1 virtual-link 0.0.0.2 authentication-key 123
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_virtual_link_authentication_conf_get(
                frr_config, {1});
        CHECK(res.size() == 1);
        CHECK(res[0].virtual_link.area == 1);
        CHECK(res[0].virtual_link.dst == 2);
        CHECK(res[0].auth_mode == vtss::FRR_OSPF_AUTH_MODE_AREA_CFG);
    }

    SECTION("message-digest authentication") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    area 0.0.0.1 virtual-link 0.0.0.2 authentication message-digest
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_virtual_link_authentication_conf_get(
                frr_config, {1});
        CHECK(res.size() == 1);
        CHECK(res[0].virtual_link.area == 1);
        CHECK(res[0].virtual_link.dst == 2);
        CHECK(res[0].auth_mode == vtss::FRR_OSPF_AUTH_MODE_MSG_DIGEST);
    }
}

TEST_CASE("to_vty_ospf_area_virtual_link_message_digest_conf_set", "[frr]") {
    vtss::FrrOspfAreaVirtualLink val;
    val.area = 1;
    val.dst = 2;
    vtss::FrrOspfDigestData data(3, "vtss");

    auto res = vtss::to_vty_ospf_area_virtual_link_message_digest_conf_set(
            val, data);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "router ospf");
    CHECK(res[2] ==
          "area 0.0.0.1 virtual-link 0.0.0.2 message-digest-key 3 md5 vtss");
}

TEST_CASE("frr_ospf_area_virtual_link_authentication_key_conf_del", "[frr]") {
    vtss::FrrOspfAreaVirtualLink val;
    val.area = 1;
    val.dst = 2;
    vtss::FrrOspfDigestData data(3, "test123");

    auto res =
            vtss::to_vty_ospf_area_virtual_link_message_digest_conf_del(val, data);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "router ospf");
    CHECK(res[2] ==
          "no area 0.0.0.1 virtual-link 0.0.0.2 message-digest-key 3 md5 test123");
}

TEST_CASE("frr_ospf_area_virtual_link_message_digest_conf_get", "[frr]") {
    SECTION("invalid instance") {
        vtss::FrrConf frr_config;
        frr_config.ospfd = "";
        auto res = vtss::frr_ospf_area_virtual_link_message_digest_conf_get(
                frr_config, {0});
        CHECK(res.size() == 0);
    }

    SECTION("parse one") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    area 0.0.0.1 virtual-link 0.0.0.2 message-digest-key 1 md5 vtss
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_virtual_link_message_digest_conf_get(
                frr_config, {1});
        CHECK(res.size() == 1);
        CHECK(res[0].virtual_link.area == 1);
        CHECK(res[0].virtual_link.dst == 2);
        CHECK(res[0].digest_data.keyid == 1);
        CHECK(res[0].digest_data.key == "vtss");
    }
}

TEST_CASE("to_vty_ospf_area_virtual_link_authentication_key_conf_set",
          "[frr]") {
    vtss::FrrOspfAreaVirtualLink val;
    val.area = 1;
    val.dst = 2;
    std::string key = "vtss";

    auto res = vtss::to_vty_ospf_area_virtual_link_authentication_key_conf_set(
            val, key);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "router ospf");
    CHECK(res[2] ==
          "area 0.0.0.1 virtual-link 0.0.0.2 authentication-key vtss");
}

TEST_CASE("to_vty_ospf_area_virtual_link_authentication_key_conf_del",
          "[frr]") {
    vtss::FrrOspfAreaVirtualLinkKey val;
    val.virtual_link.area = 1;
    val.virtual_link.dst = 2;
    val.key_data = "test123";

    auto res =
            vtss::to_vty_ospf_area_virtual_link_authentication_key_conf_del(val);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "router ospf");
    CHECK(res[2] == "no area 0.0.0.1 virtual-link 0.0.0.2 authentication-key test123");
}

TEST_CASE("frr_ospf_area_virtual_link_authentication_key_conf_get", "[frr]") {
    SECTION("invalid instance") {
        vtss::FrrConf frr_config;
        frr_config.ospfd = "";
        auto res = vtss::frr_ospf_area_virtual_link_authentication_key_conf_get(frr_config, {0});
        CHECK(res.size() == 0);
    }

    SECTION("parse one") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
    area 0.0.0.1 virtual-link 0.0.0.2 authentication-key ASD
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_virtual_link_authentication_key_conf_get(frr_config, {1});
        CHECK(res.size() == 1);
        CHECK(res[0].virtual_link.area == 1);
        CHECK(res[0].virtual_link.dst == 2);
        CHECK(res[0].key_data == "ASD");
    }

    SECTION("parse none") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
router ospf
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = vtss::frr_ospf_area_virtual_link_authentication_key_conf_get(frr_config, {1});
        CHECK(res.size() == 0);
    }
}

TEST_CASE("to_vty_ospf_if_mtu_mode_conf_set", "[frr]") {
    SECTION("ignore") {
        vtss::FrrOspfIfMtuMode mode = vtss::FRR_OSPF_IF_MTU_MODE_IGNORE;
        auto result = vtss::to_vty_ospf_if_mtu_mode_conf_set("test1", mode);
        CHECK(result.size() == 3);
        CHECK(result[0] == "configure terminal");
        CHECK(result[1] == "interface test1");
        CHECK(result[2] == "ip ospf mtu-ignore");
    }

    SECTION("enforce") {
        vtss::FrrOspfIfMtuMode mode = vtss::FRR_OSPF_IF_MTU_MODE_ENFORCE;
        auto result = vtss::to_vty_ospf_if_mtu_mode_conf_set("test1", mode);
        CHECK(result.size() == 3);
        CHECK(result[0] == "configure terminal");
        CHECK(result[1] == "interface test1");
        CHECK(result[2] == "no ip ospf mtu-ignore");
    }
}

TEST_CASE("to_vty_ospf_router_passive_if_default", "[frr]") {
    SECTION("enable") {
        auto result = vtss::to_vty_ospf_router_passive_if_default(true);
        CHECK(result.size() == 3);
        CHECK(result[0] == "configure terminal");
        CHECK(result[1] == "router ospf");
        CHECK(result[2] == "passive-interface default");
    }

    SECTION("disable") {
        auto result = vtss::to_vty_ospf_router_passive_if_default(false);
        CHECK(result.size() == 3);
        CHECK(result[0] == "configure terminal");
        CHECK(result[1] == "router ospf");
        CHECK(result[2] == "no passive-interface default");
    }
}

TEST_CASE("to_vty_ospf_router_passive_if_conf_set", "[frr]") {
    SECTION("not passive") {
        auto result =
                vtss::to_vty_ospf_router_passive_if_conf_set("test1", false);
        CHECK(result.size() == 3);
        CHECK(result[0] == "configure terminal");
        CHECK(result[1] == "router ospf");
        CHECK(result[2] == "no passive-interface test1");
    }
    SECTION("passive") {
        auto result = vtss::to_vty_ospf_router_passive_if_conf_set("test1", true);
        CHECK(result.size() == 3);
        CHECK(result[0] == "configure terminal");
        CHECK(result[1] == "router ospf");
        CHECK(result[2] == "passive-interface test1");
    }
}

TEST_CASE("frr_ospf_router_passive_if_default_get", "[frr]") {
    SECTION("invalid istance") {
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = "";
        auto result = vtss::frr_ospf_router_passive_if_default_get(frr_conf, 0);
        CHECK(result.rc == MESA_RC_ERROR);
    }

    std::string config_default_if = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
router ospf
    passive-interface default
    ospf router-id 0.1.2.3
    passive-interface wlan0
!
ip route 0.0.0.1/8 0.0.0.6 9
!
line vty
!
)";
    SECTION("parse passive-interface default") {
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = config_default_if;
        auto result = vtss::frr_ospf_router_passive_if_default_get(frr_conf, 1);
        CHECK(result.val == true);
    }
}

TEST_CASE("frr_ospf_router_passive_if_conf_get", "[frr]") {
    vtss_ifindex_t index;
    SECTION("invalid instance") {
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = "";
        auto result =
                vtss::frr_ospf_router_passive_if_conf_get(frr_conf, 0, index);
        CHECK(result.rc == MESA_RC_ERROR);
    }

    std::string config_valid_if = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
router ospf
    ospf router-id 0.1.2.3
    passive-interface wlan0
!
ip route 0.0.0.1/8 0.0.0.6 9
!
line vty
!
)";

    SECTION("parse passive-interface") {
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = config_valid_if;
        auto result =
                vtss::frr_ospf_router_passive_if_conf_get(frr_conf, 1, index);
        CHECK(result.val == true);
    }

    std::string config_no_valid_if = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
router ospf
    ospf router-id 0.1.2.3
    passive-interface wlan1
!
ip route 0.0.0.1/8 0.0.0.6 9
!
line vty
!
)";

    SECTION("parse no valid passive-interface") {
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = config_no_valid_if;
        auto result =
                vtss::frr_ospf_router_passive_if_conf_get(frr_conf, 1, index);
        CHECK(result.val == false);
    }

    std::string config_passive_default = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
router ospf
    passive-interface default
!
ip route 0.0.0.1/8 0.0.0.6 9
!
line vty
!
)";
    SECTION("parse default passive-interface") {
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = config_passive_default;
        auto result =
                vtss::frr_ospf_router_passive_if_conf_get(frr_conf, 1, index);
        CHECK(result.val == true);
    }

    std::string config_passive_default_no_passive = R"(frr version 2.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface vtss.ifh
!
router ospf
    passive-interface default
    no passive-interface wlan0
!
ip route 0.0.0.1/8 0.0.0.6 9
!
line vty
!
)";
    SECTION("parse default no passive-interface") {
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = config_passive_default_no_passive;
        auto result =
                vtss::frr_ospf_router_passive_if_conf_get(frr_conf, 1, index);
        CHECK(result.val == false);
    }
}

TEST_CASE("to_vty_ospf_if_authentication_key_del", "[frr]") {
    auto res = vtss::to_vty_ospf_if_authentication_key_del("test1");

    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "interface test1");
    CHECK(res[2] == "no ip ospf authentication-key");
}

TEST_CASE("to_vty_ospf_if_authentication_key_set", "[frr]") {
    auto res = vtss::to_vty_ospf_if_authentication_key_set("test1", "key123");

    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "interface test1");
    CHECK(res[2] == "ip ospf authentication-key key123");
}

TEST_CASE("frr_ospf_if_authentication_key_conf_get", "[frr]") {
    SECTION("expected interface") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface wlan0
    ip ospf authentication-key 1234
!
line vty
!
)";
        vtss::FrrConf conf;
        conf.ospfd = config;
        auto res = vtss::frr_ospf_if_authentication_key_conf_get(conf, {0});
        CHECK(res.val == std::string("1234"));
    }

    SECTION("different interface") {
        const std::string config_wrong_if = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface wlan0_wrong
    ip ospf authentication-key 1234
!
line vty
!
)";
        vtss::FrrConf conf;
        conf.ospfd = config_wrong_if;
        auto res = vtss::frr_ospf_if_authentication_key_conf_get(conf, {0});
        CHECK(res.rc == MESA_RC_OK);
        CHECK(res.val == "");
    }

    SECTION("no key authentication") {
        const std::string config_no_key = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface wlan0
!
line vty
!
)";
        vtss::FrrConf conf;
        conf.ospfd = config_no_key;
        auto res = vtss::frr_ospf_if_authentication_key_conf_get(conf, {0});
        CHECK(res.rc == MESA_RC_OK);
        CHECK(res.val == "");
    }
}

TEST_CASE("to_vty_ospf_if_authentication_set", "[frr]") {
    SECTION("set mode null") {
        auto res = vtss::to_vty_ospf_if_authentication_set(
                "test1", vtss::FRR_OSPF_AUTH_MODE_NULL);
        CHECK(res.size() == 3);
        CHECK(res[0] == "configure terminal");
        CHECK(res[1] == "interface test1");
        CHECK(res[2] == "ip ospf authentication null");
    }
    SECTION("set mode pwd") {
        auto res = vtss::to_vty_ospf_if_authentication_set(
                "test2", vtss::FRR_OSPF_AUTH_MODE_PWD);
        CHECK(res.size() == 3);
        CHECK(res[0] == "configure terminal");
        CHECK(res[1] == "interface test2");
        CHECK(res[2] == "ip ospf authentication");
    }
    SECTION("set mode msg digest") {
        auto res = vtss::to_vty_ospf_if_authentication_set(
                "test3", vtss::FRR_OSPF_AUTH_MODE_MSG_DIGEST);
        CHECK(res.size() == 3);
        CHECK(res[0] == "configure terminal");
        CHECK(res[1] == "interface test3");
        CHECK(res[2] == "ip ospf authentication message-digest");
    }
    SECTION("set mode area cfg") {
        auto res = vtss::to_vty_ospf_if_authentication_set(
            "test4", vtss::FRR_OSPF_AUTH_MODE_AREA_CFG);
        CHECK(res.size() == 3);
        CHECK(res[0] == "configure terminal");
        CHECK(res[1] == "interface test4");
        CHECK(res[2] == "no ip ospf authentication");
    }
}

TEST_CASE("frr_ospf_if_authentication_conf_get", "[frr]") {
    SECTION("parse no authentication") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface wlan0
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = frr_ospf_if_authentication_conf_get(frr_config, {0});
        CHECK(res.rc == MESA_RC_OK);
        CHECK(res.val == vtss::FRR_OSPF_AUTH_MODE_AREA_CFG);
    }

    SECTION("parse no interface") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface wlan0_wrong
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = frr_ospf_if_authentication_conf_get(frr_config, {0});
        CHECK(res.rc == MESA_RC_OK);
        CHECK(res.val == vtss::FRR_OSPF_AUTH_MODE_AREA_CFG);
    }

    SECTION("parse multiple authentications") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface wlan0
   ip ospf authentication message-digest
   ip ospf authentication-key 123mara1
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto res = frr_ospf_if_authentication_conf_get(frr_config, {0});
        CHECK(res.rc == MESA_RC_OK);
        CHECK(res.val == vtss::FRR_OSPF_AUTH_MODE_MSG_DIGEST);
    }
}

TEST_CASE("to_vty_ospf_if_field_conf_set", "[frr]") {
    auto res = vtss::to_vty_ospf_if_field_conf_set("test1", "field2", 2);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "interface test1");
    CHECK(res[2] == "ip ospf field2 2");
}

TEST_CASE("to_vty_ospf_if_field_conf_del", "[frr]") {
    auto res = vtss::to_vty_ospf_if_field_conf_del("test2", "field3");
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "interface test2");
    CHECK(res[2] == "no ip ospf field3");
}

TEST_CASE("frr_ospf_if_XXX_conf_get", "[frr]") {
    SECTION("frr_ospf_if_XXX_conf_get") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface wlan0
    ip ospf hello-interval 2
    ip ospf dead-interval 3
    ip ospf retransmit-interval 4000
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto hello_res =
                vtss::frr_ospf_if_hello_interval_conf_get(frr_config, {0});
        CHECK(hello_res.rc == MESA_RC_OK);
        CHECK(hello_res.val == 2);
        auto dead_res = vtss::frr_ospf_if_dead_interval_conf_get(frr_config, {0});
        CHECK(dead_res.rc == MESA_RC_OK);
        CHECK(dead_res.val.val == 3);
        auto retransmit_res =
                vtss::frr_ospf_if_retransmit_interval_conf_get(frr_config, {0});
        CHECK(retransmit_res.rc == MESA_RC_OK);
        CHECK(retransmit_res.val == 4000);
    }

    SECTION("dead-interval minimal set") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface wlan0
    ip ospf dead-interval minimal hello-multiplier 10
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto dead_res = vtss::frr_ospf_if_dead_interval_conf_get(frr_config, {0});
        CHECK(dead_res.val.val == 10);
        CHECK(dead_res.val.multiplier == true);
        auto hello_res =
                vtss::frr_ospf_if_hello_interval_conf_get(frr_config, {0});
        CHECK(hello_res.val == 10);
    }

    SECTION("dead-interval minimal and hello-interval") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface wlan0
    ip ospf hello-interval 30
    ip ospf dead-interval minimal hello-multiplier 20
!
line vty
!
)";
        vtss::FrrConf frr_config;
        frr_config.ospfd = config;
        auto hello_res =
                vtss::frr_ospf_if_hello_interval_conf_get(frr_config, {0});
        CHECK(hello_res.val == 30);
    }
}

TEST_CASE("to_vty_ospf_if_message_digest_set", "[frr]") {
    vtss::FrrOspfDigestData data(1, "asd");
    auto res = vtss::to_vty_ospf_if_message_digest_set("test1", data);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "interface test1");
    CHECK(res[2] == "ip ospf message-digest-key 1 md5 asd");
}

TEST_CASE("to_vty_ospf_if_message_digest_del", "[frr]") {
    auto res = vtss::to_vty_ospf_if_message_digest_del("test2", 2);
    CHECK(res.size() == 3);
    CHECK(res[0] == "configure terminal");
    CHECK(res[1] == "interface test2");
    CHECK(res[2] == "no ip ospf message-digest-key 2");
}

TEST_CASE("frr_ospf_if_message_digest_conf_set", "[frr]") {
    SECTION("wrong interface") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface wlan0_wrong
    ip ospf authentication message-digest
    ip ospf message-digest-key 100 md5 123141
    ip ospf message-digest-key 255 md5 123141
!
line vty
!
)";
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = config;
        auto res = vtss::frr_ospf_if_message_digest_conf_get(frr_conf, {0});
        CHECK(res.size() == 0);
    }

    SECTION("interface") {
        const std::string config = R"(frr version 3.0
frr defaults traditional
!
hostname ospfd
password zebra
log file /tmp/ospfd.log
!
interface wlan0
    ip ospf authentication message-digest
    ip ospf message-digest-key 100 md5 13
    ip ospf message-digest-key 255 md5 12
!
line vty
!
)";
        vtss::FrrConf frr_conf;
        frr_conf.ospfd = config;
        auto res = vtss::frr_ospf_if_message_digest_conf_get(frr_conf, {0});
        CHECK(res.size() == 2);
        CHECK(res[0].keyid == 100);
        CHECK(res[0].key == "13");
        CHECK(res[1].keyid == 255);
        CHECK(res[1].key == "12");
    }
}