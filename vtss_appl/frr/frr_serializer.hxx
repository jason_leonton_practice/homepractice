/*
 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
*/

#ifndef _FRR_SERIALIZER_HXX_
#define _FRR_SERIALIZER_HXX_

/**
 * \file frr_serializer.hxx
 * \brief This file contains the definitions of module serializer.
 *
 * In this file all interface-descriptors and all templated serialize functions
 * must be defind and implemented.
 *
 * The central part is the interface-descriptors, it is a C++ struct/class which
 * provides various bits of information. And a key part on a given interface is
 * serialization and de-serialization.
 *
 * Serialization:
 * It is the act of converting an internal data structure into a public data
 * structure using the encoding dened by the interface.
 *
 * De-serialization:
 * It is the reverse part, where the public data structures represented in the
 * encoding used by the interface is converted into the internal data
 * structures.
 *
 * For more deatil information, please refer to 'TN1255-vtss-expose' document.
 */

/******************************************************************************/
/** Includes                                                                  */
/******************************************************************************/
#include "frr_api.hxx"  // For frr_ospf_control_globals_dummy_get()
#include "frr_expose.hxx"
#include "vtss/appl/ospf.h"
#include "vtss/appl/types.hxx"
#include "vtss_appl_serialize.hxx"

/**
 * \brief Get status for all neighbor information.
 * \param req [IN]  Json request.
 * \param os  [OUT] Json output string.
 * \return Error code.
  */
mesa_rc vtss_appl_ospf_neighbor_status_get_all_json(
        const vtss::expose::json::Request *req, vtss::ostreamBuf *os);

/**
 * \brief Get status for all interface information.
 * \param req [IN]  Json request.
 * \param os  [OUT] Json output string.
 * \return Error code.
  */
mesa_rc vtss_appl_ospf_interface_status_get_all_json(
        const vtss::expose::json::Request *req, vtss::ostreamBuf *os);


/******************************************************************************/
/** enum serializer (enum vlaue-string mapping)                               */
/******************************************************************************/
VTSS_XXXX_SERIALIZE_ENUM(
        vtss_appl_ospf_auth_type_t, "OspfAuthType", vtss_appl_ospf_auth_type_txt,
        "This enumeration indicates OSPF authentication type.");

VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ospf_interface_state_t, "OspfInterfaceState",
                         vtss_appl_ospf_interface_state_txt,
                         "The state of the link.");

VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ospf_neighbor_state_t, "OspfNeighborState",
                         vtss_appl_ospf_neighbor_state_txt,
                         "The state of the neighbor node.");

VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ospf_area_type_t, "OspfAreaType",
                         vtss_appl_ospf_area_type_txt, "The area type.");

VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_ospf_redist_metric_type_t,
                         "OspfRedistributedMetricType",
                         vtss_appl_ospf_redist_metric_type_txt,
                         "The OSPF redistributed metric type.");

/******************************************************************************/
/** Table index/key serializer                                                */
/******************************************************************************/

VTSS_SNMP_TAG_SERIALIZE(ospf_key_instance_id, vtss_appl_ospf_id_t, a, s) {
    a.add_leaf(vtss::AsInt(s.inner), vtss::tag::Name("InstanceId"),
               vtss::expose::snmp::RangeSpec<uint32_t>(1, 1),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(0),
               vtss::tag::Description("The OSPF process instance ID."));
}

VTSS_SNMP_TAG_SERIALIZE(ospf_key_area_id, vtss_appl_ospf_area_id_t, a, s) {
    a.add_leaf(vtss::AsIpv4(s.inner), vtss::tag::Name("AreaId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(0),
               vtss::tag::Description("The OSPF area ID."));
}

VTSS_SNMP_TAG_SERIALIZE(ospf_val_area_id, vtss_appl_ospf_area_id_t, a, s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("ospf_val_area_id"));

    m.add_leaf(vtss::AsIpv4(s.inner), vtss::tag::Name("AreaId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(0),
               vtss::tag::Description("The OSPF area ID."));
}

VTSS_SNMP_TAG_SERIALIZE(ospf_key_router_id, mesa_ipv4_t, a, s) {
    a.add_leaf(vtss::AsIpv4(s.inner), vtss::tag::Name("RouterId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(0),
               vtss::tag::Description("The OSPF router ID."));
}

VTSS_SNMP_TAG_SERIALIZE(ospf_key_ipv4_network, mesa_ipv4_network_t, a, s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("mesa_ipv4_network_t"));
    m.add_leaf(vtss::AsIpv4(s.inner.address), vtss::tag::Name("Network"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(0),
               vtss::tag::Description("IPv4 network address."));

    m.add_leaf(vtss::AsInt(s.inner.prefix_size),
               vtss::tag::Name("IpSubnetMaskLength"),
               vtss::expose::snmp::RangeSpec<u32>(0, 32),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(1),
               vtss::tag::Description("IPv4 network mask length."));
}

VTSS_SNMP_TAG_SERIALIZE(ospf_key_intf_index, vtss_ifindex_t, a, s) {
    a.add_leaf(vtss::AsInterfaceIndex(s.inner), vtss::tag::Name("IfIndex"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(0),
               vtss::tag::Description("Logical interface number."));
}

VTSS_SNMP_TAG_SERIALIZE(ospf_key_ipv4_addr, mesa_ipv4_t, a, s) {
    a.add_leaf(vtss::AsIpv4(s.inner), vtss::tag::Name("Ipv4Addr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(0),
               vtss::tag::Description("The IPv4 address."));
}

VTSS_SNMP_TAG_SERIALIZE(ospf_key_md_key_id, vtss_appl_ospf_md_key_id_t, a, s) {
    a.add_leaf(s.inner, vtss::tag::Name("MdKeyId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(0),
               vtss::tag::Description(
                       "The key ID for message digest authentication."));
}

VTSS_SNMP_TAG_SERIALIZE(ospf_key_md_key_precedence, uint32_t, a, s) {
    a.add_leaf(s.inner, vtss::tag::Name("Precedence"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(0),
               vtss::tag::Description("The precedence of message digest key."));
}

VTSS_SNMP_TAG_SERIALIZE(ospf_val_md_key_id, vtss_appl_ospf_md_key_id_t, a, s) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ospf_md_key_id_t"));
    m.add_leaf(s.inner, vtss::tag::Name("MdKeyId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(0),
               vtss::tag::Description(
                       "The key ID for message digest authentication."));
}

VTSS_SNMP_TAG_SERIALIZE(ospf_val_area_type, vtss_appl_ospf_auth_type_t, a, s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("ospf_val_area_type"));

    m.add_leaf(s.inner, vtss::tag::Name("AreaAuthType"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(0),
               vtss::tag::Description("The OSPF authentication type."));
}


/******************************************************************************/
/** Table entry serializer                                                    */
/******************************************************************************/
template <typename T>
void serialize(T &a, vtss_appl_ospf_capabilities_t &p) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ospf_capabilities_t"));

    int ix = 0;

    m.add_leaf(p.instance_id_max, vtss::tag::Name("MaxInstanceId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("OSPF maximum instance ID"));
}

template <typename T>
void serialize(T &a, vtss_appl_ospf_router_conf_t &p) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ospf_router_conf_t"));

    int ix = 0;

    m.add_leaf(vtss::AsBool(p.router_id.is_specific_id),
               vtss::tag::Name("IsSpecificRouterId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Indicate the 'RouterId' argument is a "
                                      "specific configured value or not."));

    m.add_leaf(vtss::AsIpv4(p.router_id.id), vtss::tag::Name("RouterId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The OSPF router ID"));

    m.add_leaf(vtss::AsBool(p.default_passive_interface),
               vtss::tag::Name("DefPassiveInterface"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Configure all interfaces as "
                                      "passive-interface by default."));

    m.add_leaf(vtss::AsBool(p.is_specific_def_metric),
               vtss::tag::Name("IsSpecificDefMetric"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Indicate the default metric is a "
                                      "specific configured value or not."));

    m.add_leaf(
            p.def_metric, vtss::tag::Name("DefMetricVal"),
            vtss::expose::snmp::RangeSpec<u32>(0, VTSS_APPL_OSPF_METRIC_MAX),
            vtss::expose::snmp::Status::Current,
            vtss::expose::snmp::OidElementValue(ix++),
            vtss::tag::Description(
                    "User specified default metric value for the OSPF routing "
                    "protocol. The field is significant only when the arugment "
                    "'IsSpecificDefMetric' is TRUE"));

    m.add_leaf(p.redist_conf[VTSS_APPL_OSPF_REDIST_PROTOCOL_CONNECTED].type,
               vtss::tag::Name("ConnectedRedistMetricType"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The OSPF redistributed metric type for "
                                      "the connected interfaces."));

    m.add_leaf(p.redist_conf[VTSS_APPL_OSPF_REDIST_PROTOCOL_CONNECTED].metric,
               vtss::tag::Name("ConnectedRedistMetricVal"),
               vtss::expose::snmp::RangeSpec<u32>(0, VTSS_APPL_OSPF_METRIC_MAX),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "User specified metric value for the connected "
                       "interfaces. The field is significant only when the "
                       "arugment "
                       "'ConnectedRedistMetricType' is configured as "
                       "'metricTypeSpecified'"));

    m.add_leaf(p.redist_conf[VTSS_APPL_OSPF_REDIST_PROTOCOL_STATIC].type,
               vtss::tag::Name("StaticRedistMetricType"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The OSPF redistributed metric type for "
                                      "the static routes."));

    m.add_leaf(p.redist_conf[VTSS_APPL_OSPF_REDIST_PROTOCOL_STATIC].metric,
               vtss::tag::Name("StaticRedistMetricVal"),
               vtss::expose::snmp::RangeSpec<u32>(0, VTSS_APPL_OSPF_METRIC_MAX),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "User specified metric value for the static routes."
                       "The field is significant only when the arugment "
                       "'StaticRedistMetricType' is configured as "
                       "'metricTypeSpecified'"));
}

template <typename T>
void serialize(T &a, vtss_appl_ospf_router_status_t &p) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ospf_router_status_t"));

    int ix = 0;

    m.add_leaf(vtss::AsIpv4(p.ospf_router_id), vtss::tag::Name("RouterId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("OSPF router ID"));

    m.add_leaf(p.spf_delay, vtss::tag::Name("SpfDelay"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "Delay time (in seconds)of SPF calculations."));

    m.add_leaf(p.spf_holdtime, vtss::tag::Name("SpfHoldTime"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Minimum hold time (in milliseconds) "
                                      "between consecutive SPF calculations."));

    m.add_leaf(p.spf_max_waittime, vtss::tag::Name("SpfMaxWaitTime"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Maximum wait time (in milliseconds) "
                                      "between consecutive SPF calculations."));

    m.add_leaf(vtss::AsCounter(p.last_executed_spf_ts),
               vtss::tag::Name("LastExcutedSpfTs"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Time (in milliseconds) that has passed "
                                      "between the start of the SPF algorithm "
                                      "execution and the current time."));

    m.add_leaf(p.min_lsa_interval, vtss::tag::Name("MinLsaInterval"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Minimum interval (in seconds) between "
                                      "link-state advertisements."));

    m.add_leaf(p.min_lsa_arrival, vtss::tag::Name("MinLsaArrival"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Maximum arrival time (in milliseconds) "
                                      "of link-state advertisements."));

    m.add_leaf(p.external_lsa_count, vtss::tag::Name("ExternalLsaCount"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "Number of external link-state advertisements."));

    m.add_leaf(
            p.external_lsa_checksum, vtss::tag::Name("ExternalLsaChecksum"),
            vtss::expose::snmp::Status::Current,
            vtss::expose::snmp::OidElementValue(ix++),
            vtss::tag::Description("Number of external link-state checksum."));

    m.add_leaf(
            p.attached_area_count, vtss::tag::Name("AttachedAreaCount"),
            vtss::expose::snmp::Status::Current,
            vtss::expose::snmp::OidElementValue(ix++),
            vtss::tag::Description("Number of areas attached for the router."));
}

template <typename T>
void serialize(T &a, vtss_appl_ospf_router_intf_conf_t &p) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ospf_router_intf_conf_t"));

    int ix = 0;

    m.add_leaf(vtss::AsBool(p.passive_enabled),
               vtss::tag::Name("PassiveInterface"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "Enable the interface as OSPF passive-interface."));
}

template <typename T>
void serialize(T &a, vtss_appl_ospf_auth_type_t &p) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ospf_auth_type_t"));

    int ix = 0;
    m.add_leaf(p, vtss::tag::Name("AuthType"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The authentication type on an area."));
}

template <typename T>
void serialize(T &a, vtss_appl_ospf_intf_conf_t &p) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ospf_intf_conf_t"));

    int ix = 0;

    m.add_leaf(p.priority, vtss::tag::Name("Priority"),
               vtss::expose::snmp::RangeSpec<u32>(0, 255),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "User specified router priority for the interface."));

    m.add_leaf(vtss::AsBool(p.is_specific_cost),
               vtss::tag::Name("IsSpecificCost"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "Indicate the 'cost' argument is a specific configured "
                       "value or not."));

    m.add_leaf(p.cost, vtss::tag::Name("Cost"),
               vtss::expose::snmp::RangeSpec<u32>(1, 65535),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "User specified cost for this interface. "
                       "It's link state metric for the interface. "
                       "The field is significant only when 'IsSpecificCost' is "
                       "TRUE."));

    m.add_leaf(vtss::AsBool(p.is_fast_hello_enabled),
               vtss::tag::Name("IsFastHelloEnabled"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "Enable the feature of fast hello packets or not."));

    m.add_leaf(p.fast_hello_packets, vtss::tag::Name("FastHelloPackets"),
               vtss::expose::snmp::RangeSpec<u32>(2, 20),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "How many Hello packets will be sent per second."));

    m.add_leaf(
            p.dead_interval, vtss::tag::Name("DeadInterval"),
            vtss::expose::snmp::RangeSpec<u32>(1, 65535),
            vtss::expose::snmp::Status::Current,
            vtss::expose::snmp::OidElementValue(ix++),
            vtss::tag::Description(
                    "The time interval (in seconds) between hello packets."));

    m.add_leaf(p.hello_interval, vtss::tag::Name("HelloInterval"),
               vtss::expose::snmp::RangeSpec<u32>(2, 20),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "How many Hello packets will be sent per second."));

    m.add_leaf(p.retransmit_interval, vtss::tag::Name("RetransmitInterval"),
               vtss::expose::snmp::RangeSpec<u32>(1, 65535),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The time interval (in seconds) between "
                                      "link-state advertisement(LSA) "
                                      "retransmissions for adjacencies."));

    m.add_leaf(p.auth_type, vtss::tag::Name("AuthType"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The authentication type."));

    m.add_leaf(vtss::AsBool(p.is_encrypted), vtss::tag::Name("IsEncrypted"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "The flag indicates the simple password is encrypted or "
                       "not."
                       " TRUE means the simple password is encrypted."
                       " FALSE means the simple password is plain text."));

    m.add_leaf(vtss::AsDisplayString(p.auth_key, sizeof(p.auth_key)),
               vtss::tag::Name("SimplePwd"), vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The simple password."));
}

template <typename T>
void serialize(T &a, vtss_appl_ospf_auth_digest_key_t &p) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ospf_auth_digest_key_t"));

    int ix = 0;

    m.add_leaf(vtss::AsBool(p.is_encrypted), vtss::tag::Name("IsEncrypted"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "The flag indicates themessage digest key is encrypted "
                       "or not."
                       " TRUE means the message digest key is encrypted."
                       " FALSE means the message digest key is plain text."));

    m.add_leaf(vtss::AsDisplayString(p.digest_key, sizeof(p.digest_key)),
               vtss::tag::Name("MdKey"), vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The message digest key."));
}

template <typename T>
void serialize(T &a, vtss_appl_ospf_area_range_conf_t &p) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ospf_area_range_conf_t"));

    int ix = 0;

    m.add_leaf(vtss::AsBool(p.is_advertised), vtss::tag::Name("Advertised"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "When the value is true, it summarizes intra area paths "
                       "from the address range in one summary-LSA(Type-3) and "
                       "advertised to other areas.\n"
                       "Otherwise, the intra area paths from the address range "
                       "are not advertised to other areas."));

    m.add_leaf(vtss::AsBool(p.is_specific_cost),
               vtss::tag::Name("IsSpecificCost"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "Indicate the 'cost' argument is a specific configured "
                       "value or not."));

    m.add_leaf(
            p.cost, vtss::tag::Name("Cost"), vtss::expose::snmp::Status::Current,
            vtss::expose::snmp::OidElementValue(ix++),
            vtss::tag::Description(
                    "User specified cost (or metric) for this summary route. "
                    "The field is significant only when 'IsSpecificCost' is "
                    "TRUE."));
}

template <typename T>
void serialize(T &a, vtss_appl_ospf_vlink_conf_t &p) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ospf_vlink_conf_t"));

    int ix = 0;

    m.add_leaf(
            p.hello_interval, vtss::tag::Name("HelloInterval"),
            vtss::expose::snmp::Status::Current,
            vtss::expose::snmp::OidElementValue(ix++),
            vtss::tag::Description(
                    "The time interval (in seconds) between hello packets."));

    m.add_leaf(p.dead_interval, vtss::tag::Name("DeadInterval"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The number of seconds to wait until the "
                                      "neighbour is decalred to be dead."));

    m.add_leaf(p.retransmit_interval, vtss::tag::Name("RetransmitInterval"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The time interval (in seconds) between "
                                      "link-state advertisement(LSA) "
                                      "retransmissions for adjacencies."));

    m.add_leaf(p.auth_type, vtss::tag::Name("AuthType"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The authentication type on an area."));

    m.add_leaf(vtss::AsBool(p.is_encrypted), vtss::tag::Name("IsEncrypted"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "The flag indicates the simple password is encrypted or "
                       "not."
                       " TRUE means the simple password is encrypted."
                       " FALSE means the simple password is plain text."));

    m.add_leaf(vtss::AsDisplayString(p.simple_pwd, sizeof(p.simple_pwd)),
               vtss::tag::Name("SimplePwd"), vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The simple password."));
}

template <typename T>
void serialize(T &a, vtss_appl_ospf_stub_area_conf_t &p) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ospf_stub_area_conf_t"));

    int ix = 0;

    m.add_leaf(
            vtss::AsBool(p.no_summary), vtss::tag::Name("NoSummary"),
            vtss::expose::snmp::Status::Current,
            vtss::expose::snmp::OidElementValue(ix++),
            vtss::tag::Description(
                    "The value is true means the area is a totally stub "
                    "area, which summary-LSAs(Type-3) except for the default "
                    "route and AS-external-LSAs(Type-5) are blocked. "
                    "The value is false means the area is a stub area, "
                    "which summary-LSAs(Type-3) except for the default "
                    "route are blocked."));
}

template <typename T>
void serialize(T &a, vtss_appl_ospf_area_status_t &p) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ospf_area_status_t"));

    int ix = 0;

    m.add_leaf(
            vtss::AsBool(p.is_backbone), vtss::tag::Name("IsBackbone"),
            vtss::expose::snmp::Status::Current,
            vtss::expose::snmp::OidElementValue(ix++),
            vtss::tag::Description("Indicate if it's backbone area or not."));

    m.add_leaf(p.area_type, vtss::tag::Name("AreaType"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The area type."));

    m.add_leaf(p.attached_intf_active_count,
               vtss::tag::Name("AttachedIntfActiveCount"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "Number of active interfaces attached in the area."));

    m.add_leaf(p.auth_type, vtss::tag::Name("AuthType"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The authentication type in the area."));

    m.add_leaf(p.spf_executed_count, vtss::tag::Name("SpfExecutedCount"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number of times SPF algorithm has been "
                                      "executed for the particular area."));

    m.add_leaf(p.lsa_count, vtss::tag::Name("LsaCount"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number of the total LSAs for the "
                                      "particular area."));

    m.add_leaf(p.router_lsa_count, vtss::tag::Name("RouterLsaCount"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number of the router-LSAs(Type-1) of a "
                                      "given type for the particular area."));

    m.add_leaf(p.router_lsa_checksum, vtss::tag::Name("RouterLsaChecksum"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The the router-LSAs(Type-1) checksum."));

    m.add_leaf(p.network_lsa_count, vtss::tag::Name("NetworkLsaCount"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number of the network-LSAs(Type-2) of a "
                                      "given type for the particular area."));

    m.add_leaf(
            p.network_lsa_checksum, vtss::tag::Name("NetworkLsaChecksum"),
            vtss::expose::snmp::Status::Current,
            vtss::expose::snmp::OidElementValue(ix++),
            vtss::tag::Description("The the network-LSAs(Type-2) checksum."));

    m.add_leaf(p.summary_lsa_count, vtss::tag::Name("SummaryLsaCount"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Number of the summary-LSAs(Type-3) of a "
                                      "given type for the particular area."));

    m.add_leaf(
            p.summary_lsa_checksum, vtss::tag::Name("SummaryLsaChecksum"),
            vtss::expose::snmp::Status::Current,
            vtss::expose::snmp::OidElementValue(ix++),
            vtss::tag::Description("The the summary-LSAs(Type-3) checksum."));

    m.add_leaf(
            p.asbr_summary_lsa_count, vtss::tag::Name("AsbrSummaryLsaCount"),
            vtss::expose::snmp::Status::Current,
            vtss::expose::snmp::OidElementValue(ix++),
            vtss::tag::Description("Number of the ASBR-summary-LSAs(Type-4) "
                                   "of a given type for the particular area."));

    m.add_leaf(p.asbr_summary_lsa_checksum,
               vtss::tag::Name("AsbrSummaryLsaChecksum"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "The the ASBR-summary-LSAs(Type-4) checksum."));
}

template <typename T>
void serialize(T &a, vtss_appl_ospf_interface_status_t &p) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ospf_interface_status_t"));

    int ix = 0;

    m.add_leaf(vtss::AsBool(p.status), vtss::tag::Name("Status"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("It's used to indicate if the interface "
                                      "is up or down."));

    m.add_leaf(vtss::AsBool(p.is_passive), vtss::tag::Name("Passive"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Indicate if the interface "
                                      "is passive interface."));

    m.add_leaf(vtss::AsIpv4(p.network.address), vtss::tag::Name("Network"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("IPv4 network address."));

    m.add_leaf(vtss::AsInt(p.network.prefix_size),
               vtss::tag::Name("IpSubnetMaskLength"),
               vtss::expose::snmp::RangeSpec<u32>(0, 32),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("IPv4 network mask length."));

    m.add_leaf(vtss::AsIpv4(p.area_id), vtss::tag::Name("AreaId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The OSPF area ID."));

    m.add_leaf(vtss::AsIpv4(p.router_id), vtss::tag::Name("RouterId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The OSPF router ID."));

    m.add_leaf(vtss::AsInt(p.cost), vtss::tag::Name("Cost"),
               vtss::expose::snmp::RangeSpec<u32>(0, 16777214),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The cost of the interface."));

    m.add_leaf(p.state, vtss::tag::Name("State"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The state of the link."));

    m.add_leaf(vtss::AsInt(p.priority), vtss::tag::Name("Priority"),
               vtss::expose::snmp::RangeSpec<u8>(0, 255),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The OSPF priority. It helps determine "
                                      "the DR and BDR on the network to which "
                                      "this interface is connected."));

    m.add_leaf(vtss::AsIpv4(p.dr_id), vtss::tag::Name("DrId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The router ID of DR."));

    m.add_leaf(vtss::AsIpv4(p.dr_addr), vtss::tag::Name("DrAddr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The IP address of DR."));

    m.add_leaf(vtss::AsIpv4(p.bdr_id), vtss::tag::Name("BdrId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The router ID of BDR."));

    m.add_leaf(vtss::AsIpv4(p.bdr_addr), vtss::tag::Name("BdrAddr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The IP address of BDR."));

    m.add_leaf(p.hello_time, vtss::tag::Name("HelloTime"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Hello timer. A time interval that a "
                                      "router sends an OSPF hello packet."));

    m.add_leaf(
            p.dead_time, vtss::tag::Name("DeadTime"),
            vtss::expose::snmp::Status::Current,
            vtss::expose::snmp::OidElementValue(ix++),
            vtss::tag::Description("Dead timer. Dead timer is a time interval "
                                   "to wait before declaring a neighbor dead. "
                                   "The unit of time is the second."));

    m.add_leaf(p.dead_time, vtss::tag::Name("WaitTime"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "This interval is used in Wait Timer. "
                       "Wait timer is a single shot timer that "
                       "causes the interface to exit waiting "
                       "and select a DR on the network. Wait "
                       "Time interval is the same as Dead time interval."));

    m.add_leaf(p.retransmit_time, vtss::tag::Name("RetransmitTime"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Retransmit timer. A time interval to "
                                      "wait before retransmitting a database "
                                      "description packet when it has not been "
                                      "acknowledged."));

    m.add_leaf(p.hello_due_time, vtss::tag::Name("HelloDueTime"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Hello due timer. An OSPF hello packet "
                                      "will be sent on this interface after "
                                      "this due time."));

    m.add_leaf(
            p.neighbor_count, vtss::tag::Name("NeighborCount"),
            vtss::expose::snmp::Status::Current,
            vtss::expose::snmp::OidElementValue(ix++),
            vtss::tag::Description("Neighbor count. This is the number of OSPF "
                                   "neighbors discovered on this interface."));

    m.add_leaf(p.adj_neighbor_count, vtss::tag::Name("AdjNeighborCount"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Adjacent neighbor count. This is the "
                                      "number of routers running OSPF that are "
                                      "fully adjacent with this router."));

    m.add_leaf(p.transmit_delay, vtss::tag::Name("TransmitDelay"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The estimated time to transmit a "
                                      "link-state update packet on the "
                                      "interface."));
}

template <typename T>
void serialize(T &a, vtss_appl_ospf_neighbor_status_t &p) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ospf_neighbor_status_t"));

    int ix = 0;

    m.add_leaf(vtss::AsIpv4(p.ip_addr), vtss::tag::Name("IpAddr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The IP address."));

    m.add_leaf(vtss::AsIpv4(p.area_id), vtss::tag::Name("AreaId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The OSPF area ID."));

    m.add_leaf(vtss::AsInt(p.priority), vtss::tag::Name("Priority"),
               vtss::expose::snmp::RangeSpec<u8>(0, 255),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "The priority of OSPF neighbor. It indicates the "
                       "priority of the neighbor router. This item is used "
                       "when selecting the DR for the network. The router with "
                       "the highest priority becomes the DR."));

    // vtss_appl_ospf_neighbor_state_t
    m.add_leaf(p.state, vtss::tag::Name("State"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The state of OSPF neighbor. It "
                                      "indicates the functional state of the "
                                      "neighbor router."));

    m.add_leaf(vtss::AsIpv4(p.dr_id), vtss::tag::Name("DrId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The router ID of DR."));

    m.add_leaf(vtss::AsIpv4(p.dr_addr), vtss::tag::Name("DrAddr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The IP address of DR."));

    m.add_leaf(vtss::AsIpv4(p.bdr_id), vtss::tag::Name("BdrId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The router ID of BDR."));

    m.add_leaf(vtss::AsIpv4(p.bdr_addr), vtss::tag::Name("BdrAddr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The IP address of BDR."));

    m.add_leaf(p.options, vtss::tag::Name("Options"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "The OSPF option field which is present in "
                       "OSPF hello packets, which enables OSPF routers to "
                       "support (or not support) optional capabilities, and to "
                       "communicate their capability level to other OSPF "
                       "routers."));

    m.add_leaf(p.dead_time, vtss::tag::Name("DeadTime"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description(
                       "Dead timer. It indicates the amount of time "
                       "remaining that the router waits to receive "
                       "an OSPF hello packet from the neighbor "
                       "before declaring the neighbor down."));

    m.add_leaf(vtss::AsIpv4(p.transit_id), vtss::tag::Name("TransitAreaId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("The OSPF transit area ID for the "
                                      "neighbor on virtual link interface."));
}

template <typename T>
void serialize(T &a, vtss_appl_ospf_control_globals_t &s) {
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_ospf_control_globals_t"));
    int ix = 0;

    m.add_leaf(vtss::AsBool(s.reload_process), vtss::tag::Name("ReloadProcess"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Set true to reload OSPF process."));
}

/******************************************************************************/
/** Table-entry/data structure serializer                                     */
/******************************************************************************/
namespace vtss {
namespace appl {
namespace ospf {
namespace interfaces {

//------------------------------------------------------------------------------
//** OSPF module capabilities
//------------------------------------------------------------------------------
struct OspfCapabilitiesTabular {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamVal<vtss_appl_ospf_capabilities_t *>>
            P;

    /* Description */
    static constexpr const char *table_description =
            "This is OSPF capabilities tabular. It provides the capabilities "
            "of OSPF configuration.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ospf_capabilities_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("Capabilities"));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_capabilities_get);
};

//------------------------------------------------------------------------------
//** OSPF instance configuration
//------------------------------------------------------------------------------
struct OspfConfigProcessEntry {
    typedef vtss::expose::ParamList<vtss::expose::ParamKey<vtss_appl_ospf_id_t>> P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is OSPF process configuration table. It is used to enable or "
            "disable the routing process on a specific process ID.";
    static constexpr const char *index_description =
            "Each entry in this table represents OSPF routing process.";

    /* SNMP row editor OID (Provides for SNMP Add/Delete operations) */
    static constexpr uint32_t snmpRowEditorOid = 100;

    /* Entry key */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ospf_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("InstanceId"));
        serialize(h, ospf_key_instance_id(i));
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_inst_itr);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ospf_add);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_ospf_add);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_ospf_def);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_ospf_del);
};

//------------------------------------------------------------------------------
//** OSPF router configuration
//------------------------------------------------------------------------------
struct OspfConfigRouterEntry {
    typedef vtss::expose::ParamList<vtss::expose::ParamKey<vtss_appl_ospf_id_t>,
                                    vtss::expose::ParamVal<vtss_appl_ospf_router_conf_t *>>
            P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is OSPF router configuration table. It is a general group to "
            "configure the OSPF common router parameters.";
    static constexpr const char *index_description =
            "Each entry in this table represents the OSPF router interface "
            "configuration.";

    /* Entry key */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ospf_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("InstanceId"));
        serialize(h, ospf_key_instance_id(i));
    }

    /* Entry data */
    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ospf_router_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2),
                              tag::Name("RouterConfig"));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_inst_itr);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_router_conf_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ospf_router_conf_set);
    VTSS_EXPOSE_DEF_PTR(frr_ospf_router_conf_def);
};

//------------------------------------------------------------------------------
//** OSPF router status
//------------------------------------------------------------------------------
struct OspfStatusRouterEntry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamKey<vtss_appl_ospf_id_t>,
            vtss::expose::ParamVal<vtss_appl_ospf_router_status_t *>>
            P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is OSPF router status table. It is used to provide the "
            "OSPF router status information.";
    static constexpr const char *index_description =
            "Each entry in this table represents OSPF router status "
            "information.";

    /* Entry key */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ospf_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("InstanceId"));
        serialize(h, ospf_key_instance_id(i));
    }

    /* Entry data */
    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ospf_router_status_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2),
                              tag::Name("RouterStatus"));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_inst_itr);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_router_status_get);
};

//------------------------------------------------------------------------------
//** OSPF network area configuration
//------------------------------------------------------------------------------
struct OspfConfigAreaEntry {
    typedef vtss::expose::ParamList<vtss::expose::ParamKey<vtss_appl_ospf_id_t>,
                                    vtss::expose::ParamKey<mesa_ipv4_network_t *>,
                                    vtss::expose::ParamVal<vtss_appl_ospf_area_id_t *>>
            P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is OSPF area configuration table. It is used to specify the "
            "OSPF enabled interface(s). When OSPF is enabled on the specific "
            "interface(s), the router can provide the network information to "
            "the other OSPF routers via those interfaces.";
    static constexpr const char *index_description =
            "Each entry in this table represents the OSPF enabled interface(s) "
            "and its area ID.";

    /* SNMP row editor OID (Provides for SNMP Add/Delete operations) */
    static constexpr uint32_t snmpRowEditorOid = 100;

    /* Entry keys */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ospf_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("InstanceId"));
        serialize(h, ospf_key_instance_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_2(mesa_ipv4_network_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2),
                              tag::Name("NetworkAddress"));
        serialize(h, ospf_key_ipv4_network(i));
    }

    /* Entry data */
    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_ospf_area_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(4),
                              tag::Name("AreaId"));
        serialize(h, ospf_val_area_id(i));
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_area_conf_itr);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_area_conf_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ospf_area_conf_add);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_ospf_area_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_ospf_area_conf_del);
    VTSS_EXPOSE_DEF_PTR(frr_ospf_area_conf_def);
};

//------------------------------------------------------------------------------
//** OSPF router interface configuration
//------------------------------------------------------------------------------
struct OspfConfigRouterInterfaceEntry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamKey<vtss_appl_ospf_id_t>,
            vtss::expose::ParamKey<vtss_ifindex_t>,
            vtss::expose::ParamVal<vtss_appl_ospf_router_intf_conf_t *>>
            P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is OSPF router interface configuration table.";
    static constexpr const char *index_description =
            "Each router interface has a set of parameters.";

    /* Entry keys */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ospf_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("InstanceId"));
        serialize(h, ospf_key_instance_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2),
                              tag::Name("Interface"));
        serialize(h, ospf_key_intf_index(i));
    }

    /* Entry data */
    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_ospf_router_intf_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3),
                              tag::Name("RouterInterfaceConf"));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_router_intf_conf_itr);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_router_intf_conf_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ospf_router_intf_conf_set);
};

//----------------------------------------------------------------------------
//** OSPF authentication
//----------------------------------------------------------------------------
struct OspfConfigAreaAuthEntry {
    typedef vtss::expose::ParamList<vtss::expose::ParamKey<vtss_appl_ospf_id_t>,
                                    vtss::expose::ParamKey<vtss_appl_ospf_area_id_t>,
                                    vtss::expose::ParamVal<vtss_appl_ospf_auth_type_t>>
            P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is OSPF area authentication configuration table. It is used "
            "to applied the authentication to all the interfaces belong to the "
            " area.";
    static constexpr const char *index_description =
            "Each entry in this table represents OSPF area authentication "
            "configuration. Notice that the authentication type setting on the "
            "sepecific interface overrides the area's setting.";

    /* SNMP row editor OID (Provides for SNMP Add/Delete operations) */
    static constexpr uint32_t snmpRowEditorOid = 100;

    /* Entry keys */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ospf_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("InstanceId"));
        serialize(h, ospf_key_instance_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ospf_area_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2),
                              tag::Name("AreaId"));
        serialize(h, ospf_key_area_id(i));
    }

    /* Entry data */
    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_ospf_auth_type_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3),
                              tag::Name("AreaAuthType"));
        serialize(h, ospf_val_area_type(i));
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_area_auth_conf_itr);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_area_auth_conf_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ospf_area_auth_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_ospf_area_auth_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_ospf_area_auth_conf_del);
    VTSS_EXPOSE_DEF_PTR(frr_ospf_area_auth_conf_def);
};

struct OspfConfigInterfaceAuthMdKeyEntry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamKey<vtss_ifindex_t>,
            vtss::expose::ParamKey<vtss_appl_ospf_md_key_id_t>,
            vtss::expose::ParamVal<vtss_appl_ospf_auth_digest_key_t *>>
            P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is interface authentication message digest key configuration "
            "able.";
    static constexpr const char *index_description =
            "Each interface has a set of parameters.";

    /* SNMP row editor OID (Provides for SNMP Add/Delete operations) */
    static constexpr uint32_t snmpRowEditorOid = 100;

    /* Entry keys */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("Interface"));
        serialize(h, ospf_key_intf_index(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ospf_md_key_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2),
                              tag::Name("MdKeyId"));
        serialize(h, ospf_key_md_key_id(i));
    }

    /* Entry data */
    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_ospf_auth_digest_key_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3),
                              tag::Name("MdConfig"));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_intf_auth_digest_key_itr);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_intf_auth_digest_key_get);
    VTSS_EXPOSE_SET_PTR(frr_ospf_intf_auth_digest_key_dummy_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_ospf_intf_auth_digest_key_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_ospf_intf_auth_digest_key_del);
    VTSS_EXPOSE_DEF_PTR(frr_ospf_intf_auth_digest_key_def);
};

//----------------------------------------------------------------------------
//** OSPF area range
//----------------------------------------------------------------------------
struct OspfConfigAreaRangeEntry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamKey<vtss_appl_ospf_id_t>,
            vtss::expose::ParamKey<vtss_appl_ospf_area_id_t>,
            vtss::expose::ParamKey<mesa_ipv4_network_t>,
            vtss::expose::ParamVal<vtss_appl_ospf_area_range_conf_t *>>
            P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is OSPF area range configuration table. It is used to "
            "summarize the intra area paths from a specific address range in "
            "one summary-LSA(Type-3) and advertised to other areas or "
            "configure the address range status as 'DoNotAdvertise' which the "
            "summary-LSA(Type-3) is suppressed.\n"
            "The area range configuration is used for Area Border Routers "
            "(ABRs) and only router-LSAs(Type-1) and network-LSAs (Type-2) can "
            "be summarized. The AS-external-LSAs(Type-5) cannot be summarized "
            "because the scope is OSPF autonomous system (AS). The "
            "AS-external-LSAs(Type-7) cannot be summarized because the feature "
            "is not supported yet.";
    static constexpr const char *index_description =
            "Each entry in this table represents OSPF area range configuration."
            " The overlap configuration of address range is not allowed in "
            "order to avoid the conflict.";

    /* SNMP row editor OID (Provides for SNMP Add/Delete operations) */
    static constexpr uint32_t snmpRowEditorOid = 100;

    /* Entry keys */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ospf_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("InstanceId"));
        serialize(h, ospf_key_instance_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ospf_area_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2),
                              tag::Name("AreaId"));
        serialize(h, ospf_key_area_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_3(mesa_ipv4_network_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3),
                              tag::Name("NetworkAddress"));
        serialize(h, ospf_key_ipv4_network(i));
    }

    /* Entry data */
    VTSS_EXPOSE_SERIALIZE_ARG_4(vtss_appl_ospf_area_range_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(5),
                              tag::Name("AreaRangeConf"));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_area_range_conf_itr);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_area_range_conf_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ospf_area_range_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_ospf_area_range_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_ospf_area_range_conf_del);
    VTSS_EXPOSE_DEF_PTR(frr_ospf_area_range_conf_def);
};

//----------------------------------------------------------------------------
//** OSPF virtual link
//----------------------------------------------------------------------------
struct OspfConfigVlinkEntry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamKey<vtss_appl_ospf_id_t>,
            vtss::expose::ParamKey<vtss_appl_ospf_area_id_t>,
            vtss::expose::ParamKey<vtss_appl_ospf_router_id_t>,
            vtss::expose::ParamVal<vtss_appl_ospf_vlink_conf_t *>>
            P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is OSPF virtual link configuration table. The virtual link "
            "is established between 2 ABRs to overcome that all the areas have "
            "to be connected directly to the backbone area.";
    static constexpr const char *index_description =
            "Each entry in this table represents OSPF virtual link "
            "configuration.";

    /* SNMP row editor OID (Provides for SNMP Add/Delete operations) */
    static constexpr uint32_t snmpRowEditorOid = 100;

    /* Entry keys */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ospf_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("InstanceId"));
        serialize(h, ospf_key_instance_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ospf_area_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2),
                              tag::Name("AreaId"));
        serialize(h, ospf_key_area_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_ospf_router_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3),
                              tag::Name("RouterId"));
        serialize(h, ospf_key_router_id(i));
    }

    /* Entry data */
    VTSS_EXPOSE_SERIALIZE_ARG_4(vtss_appl_ospf_vlink_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(4),
                              tag::Name("VirtualLinkConf"));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_vlink_itr);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_vlink_conf_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ospf_vlink_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_ospf_vlink_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_ospf_vlink_conf_del);
    VTSS_EXPOSE_DEF_PTR(frr_ospf_vlink_conf_def);
};

//----------------------------------------------------------------------------
//** OSPF virtual link messge digest key
//----------------------------------------------------------------------------
struct OspfConfigVlinkMdKeyEntry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamKey<vtss_appl_ospf_id_t>,
            vtss::expose::ParamKey<vtss_appl_ospf_area_id_t>,
            vtss::expose::ParamKey<vtss_appl_ospf_router_id_t>,
            vtss::expose::ParamKey<vtss_appl_ospf_md_key_id_t>,
            vtss::expose::ParamVal<vtss_appl_ospf_auth_digest_key_t *>>
            P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is virtual link authentication message digest key "
            "configuration "
            "able.";
    static constexpr const char *index_description =
            "Each virtual link entry has a set of parameters.";

    /* SNMP row editor OID (Provides for SNMP Add/Delete operations) */
    static constexpr uint32_t snmpRowEditorOid = 100;

    /* Entry keys */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ospf_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("InstanceId"));
        serialize(h, ospf_key_instance_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ospf_area_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2),
                              tag::Name("AreaId"));
        serialize(h, ospf_key_area_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_ospf_router_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3),
                              tag::Name("RouterId"));
        serialize(h, ospf_key_router_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_4(vtss_appl_ospf_md_key_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(4),
                              tag::Name("MdKeyId"));
        serialize(h, ospf_key_md_key_id(i));
    }

    /* Entry data */
    VTSS_EXPOSE_SERIALIZE_ARG_5(vtss_appl_ospf_auth_digest_key_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(5),
                              tag::Name("MessageDigestConfig"));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_vlink_md_key_itr);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_vlink_md_key_conf_get);
    VTSS_EXPOSE_SET_PTR(frr_ospf_vlink_md_key_conf_dummy_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_ospf_vlink_md_key_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_ospf_vlink_md_key_conf_del);
    VTSS_EXPOSE_DEF_PTR(frr_ospf_vlink_md_key_conf_def);
};

//----------------------------------------------------------------------------
//** OSPF stub area
//----------------------------------------------------------------------------
struct OspfConfigStubAreaEntry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamKey<vtss_appl_ospf_id_t>,
            vtss::expose::ParamKey<vtss_appl_ospf_area_id_t>,
            vtss::expose::ParamVal<vtss_appl_ospf_stub_area_conf_t *>>
            P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is OSPF stub area configuration table. The configuration is "
            "used to reduce the link-state database size and therefore the "
            "memory and CPU requirement by forbidding some LSAs.";
    static constexpr const char *index_description =
            "Each entry in this table represents OSPF stub area configuration.";

    /* SNMP row editor OID (Provides for SNMP Add/Delete operations) */
    static constexpr uint32_t snmpRowEditorOid = 100;

    /* Entry keys */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ospf_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("InstanceId"));
        serialize(h, ospf_key_instance_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ospf_area_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2),
                              tag::Name("AreaId"));
        serialize(h, ospf_key_area_id(i));
    }

    /* Entry data */
    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_ospf_stub_area_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3),
                              tag::Name("StubAreaConf"));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_stub_area_conf_itr);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_stub_area_conf_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ospf_stub_area_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_ospf_stub_area_conf_add);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_ospf_stub_area_conf_del);
    VTSS_EXPOSE_DEF_PTR(frr_ospf_stub_area_conf_def);
};

//----------------------------------------------------------------------------
//** OSPF interface parameter tuning
//----------------------------------------------------------------------------
struct OspfConfigInterfaceEntry {
    typedef vtss::expose::ParamList<vtss::expose::ParamKey<vtss_ifindex_t>,
                                    vtss::expose::ParamVal<vtss_appl_ospf_intf_conf_t *>>
            P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is interface configuration parameter table.";
    static constexpr const char *index_description =
            "Each interface has a set of parameters.";

    /* Entry keys */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("Interface"));
        serialize(h, ospf_key_intf_index(i));
    }

    /* Entry data */
    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ospf_intf_conf_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2),
                              tag::Name("Parameter"));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_intf_conf_itr);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_intf_conf_get);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ospf_intf_conf_set);
};


//------------------------------------------------------------------------------
//** OSPF status
//------------------------------------------------------------------------------
struct OspfStatusAreaEntry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamKey<vtss_appl_ospf_id_t>,
            vtss::expose::ParamKey<vtss_appl_ospf_area_id_t>,
            vtss::expose::ParamVal<vtss_appl_ospf_area_status_t *>>
            P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is OSPF network area status table. It is used to provide the "
            "OSPF network area status information.";
    static constexpr const char *index_description =
            "Each entry in this table represents OSPF network area status "
            "information.";

    /* Entry keys */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ospf_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("InstanceId"));
        serialize(h, ospf_key_instance_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ospf_area_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2),
                              tag::Name("AreaId"));
        serialize(h, ospf_key_area_id(i));
    }

    /* Entry data */
    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_ospf_area_status_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3),
                              tag::Name("AreaStatus"));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_area_status_itr);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_area_status_get);
};

//------------------------------------------------------------------------------
//** OSPF interface status
//------------------------------------------------------------------------------
struct OspfStatusInterfaceEntry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamKey<vtss_ifindex_t>,
            vtss::expose::ParamVal<vtss_appl_ospf_interface_status_t *>>
            P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is OSPF interface status table. It is used to provide the "
            "OSPF interface status information.";
    static constexpr const char *index_description =
            "Each entry in this table represents OSPF interface status "
            "information.";

    /* Entry keys */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("Ifindex"));
        serialize(h, ospf_key_intf_index(i));
    }

    /* Entry data */
    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ospf_interface_status_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2),
                              tag::Name("InterfaceStatus"));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_interface_itr);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_interface_status_get);
    VTSS_JSON_GET_ALL_PTR(vtss_appl_ospf_interface_status_get_all_json);
};

//------------------------------------------------------------------------------
//** OSPF neighbor status
//------------------------------------------------------------------------------
struct OspfStatusNeighborIpv4Entry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamKey<vtss_appl_ospf_id_t>,
            vtss::expose::ParamKey<vtss_appl_ospf_router_id_t>,
            vtss::expose::ParamKey<mesa_ipv4_t>, vtss::expose::ParamKey<vtss_ifindex_t>,
            vtss::expose::ParamVal<vtss_appl_ospf_neighbor_status_t *>>
            P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is OSPF IPv4 neighbor status table. It is used to provide "
            "the OSPF neighbor status information.";
    static constexpr const char *index_description =
            "Each entry in this table represents OSPF IPv4 neighbor "
            "status information.";

    /* Entry keys */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ospf_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("InstanceId"));
        serialize(h, ospf_key_instance_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ospf_router_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2),
                              tag::Name("NeighborId"));
        serialize(h, ospf_key_router_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_3(mesa_ipv4_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3),
                              tag::Name("NeighborIP"));
        serialize(h, ospf_key_ipv4_addr(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_4(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(4),
                              tag::Name("Interface"));
        serialize(h, ospf_key_intf_index(i));
    }

    /* Entry data */
    VTSS_EXPOSE_SERIALIZE_ARG_5(vtss_appl_ospf_neighbor_status_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(5),
                              tag::Name("NeighborStatus"));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_neighbor_status_itr2);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_neighbor_status_get);
    VTSS_JSON_GET_ALL_PTR(vtss_appl_ospf_neighbor_status_get_all_json);
};

//------------------------------------------------------------------------------
//** OSPF interface message digest key precedence
//------------------------------------------------------------------------------
struct OspfStatusInterfaceMdKeyPrecedenceEntry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamKey<vtss_ifindex_t>, vtss::expose::ParamKey<uint32_t>,
            vtss::expose::ParamVal<vtss_appl_ospf_md_key_id_t *>>
            P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is virtual link authentication message digest key precedence"
            "configuration "
            "able.";
    static constexpr const char *index_description =
            "Each row contains the corresponding message digest key ID.";

    /* Entry keys */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_ifindex_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("Interface"));
        serialize(h, ospf_key_intf_index(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_2(uint32_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2),
                              tag::Name("Precedence"));
        serialize(h, ospf_key_md_key_precedence(i));
    }

    /* Entry data */
    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_ospf_md_key_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3),
                              tag::Name("MdKeyId"));
        serialize(h, ospf_val_md_key_id(i));
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_intf_md_key_precedence_itr);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_intf_md_key_precedence_get);
};

//------------------------------------------------------------------------------
//** OSPF virtual link message digest key precedence
//------------------------------------------------------------------------------
struct OspfStatusVlinkMdKeyPrecedenceEntry {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamKey<vtss_appl_ospf_id_t>,
            vtss::expose::ParamKey<vtss_appl_ospf_area_id_t>,
            vtss::expose::ParamKey<vtss_appl_ospf_router_id_t>,
            vtss::expose::ParamKey<uint32_t>,
            vtss::expose::ParamVal<vtss_appl_ospf_md_key_id_t *>>
            P;

    /* Descriptions */
    static constexpr const char *table_description =
            "This is virtual link authentication message digest key precedence"
            "configuration "
            "able.";
    static constexpr const char *index_description =
            "Each row contains the corresponding message digest key ID.";

    /* Entry keys */
    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ospf_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1),
                              tag::Name("InstanceId"));
        serialize(h, ospf_key_instance_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_ospf_area_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2),
                              tag::Name("AreaId"));
        serialize(h, ospf_key_area_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_3(vtss_appl_ospf_router_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(3),
                              tag::Name("RouterId"));
        serialize(h, ospf_key_router_id(i));
    }
    VTSS_EXPOSE_SERIALIZE_ARG_4(uint32_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(4),
                              tag::Name("Precedence"));
        serialize(h, ospf_key_md_key_precedence(i));
    }

    /* Entry data */
    VTSS_EXPOSE_SERIALIZE_ARG_5(vtss_appl_ospf_md_key_id_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(5),
                              tag::Name("MdKeyId"));
        serialize(h, ospf_val_md_key_id(i));
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_ospf_vlink_md_key_precedence_itr);
    VTSS_EXPOSE_GET_PTR(vtss_appl_ospf_vlink_md_key_precedence_get);
};

//------------------------------------------------------------------------------
//** OSPF global controls
//------------------------------------------------------------------------------
struct OspfControlGlobalsTabular {
    typedef vtss::expose::ParamList<
            vtss::expose::ParamVal<vtss_appl_ospf_control_globals_t *>>
            P;

    /* Description */
    static constexpr const char *table_description =
            "This is OSPF global control tabular. It is used to set the OSPF "
            "global control options.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_ospf_control_globals_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_FRR);
    VTSS_EXPOSE_SET_PTR(vtss_appl_ospf_control_globals);
    VTSS_EXPOSE_GET_PTR(frr_ospf_control_globals_dummy_get);
};

}  // namespace interfaces
}  // namespace ospf
}  // namespace appl
}  // namespace vtss

#endif  // _FRR_SERIALIZER_HXX_
