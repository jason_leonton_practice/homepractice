/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _PSEC_LIMIT_API_H_
#define _PSEC_LIMIT_API_H_

#include "port_api.h" /* For VTSS_PORTS              */
#include "psec_api.h"
#include <vtss/appl/psec.h>

#ifdef __cplusplus
extern "C" {
#endif
/**
 * \file psec_limit_api.h
 * \brief This file defines the Limit Control API for the Port Security module
 */

/**
 * \brief API Error Return Codes (mesa_rc)
 */
enum {
    PSEC_LIMIT_ERROR_INV_PARAM = MODULE_ERROR_START(VTSS_MODULE_ID_PSEC_LIMIT), /**< Invalid user parameter.                                   */
    PSEC_LIMIT_ERROR_MUST_BE_MASTER,                                            /**< Operation is only allowed on the master switch.           */
    PSEC_LIMIT_ERROR_INV_ISID,                                                  /**< isid parameter is invalid.                                */
    PSEC_LIMIT_ERROR_INV_PORT,                                                  /**< Port parameter is invalid.                                */
    PSEC_LIMIT_ERROR_INV_AGING_PERIOD,                                          /**< The supplied aging period is invalid.                     */
    PSEC_LIMIT_ERROR_INV_HOLD_TIME,                                             /**< The supplied hold time is invalid.                        */
    PSEC_LIMIT_ERROR_INV_LIMIT,                                                 /**< The supplied MAC address limit is out of range.           */
    PSEC_LIMIT_ERROR_INV_VIOLATE_LIMIT,                                         /**< The supplied violating MAC address limit is out of range. */
    PSEC_LIMIT_ERROR_INV_VIOLATION_MODE,                                        /**< The supplied violation mode is out of range.              */
    PSEC_LIMIT_ERROR_STATIC_AGGR_ENABLED,                                       /**< Cannot enable if also static aggregated.                  */
    PSEC_LIMIT_ERROR_DYNAMIC_AGGR_ENABLED,                                      /**< Cannot enable if also dynamic aggregated.                 */
}; // Anonymous enum to satisfy Lint.

//
// Other public Port Security Limit Control functions.
//

/**
 * \brief Retrieve an error string based on a return code
 *        from one of the Port Security Limit Control API functions.
 *
 * \param rc [IN]: Error code that must be in the PSEC_LIMIT_ERROR_xxx range.
 */
const char *psec_limit_error_txt(mesa_rc rc);

/**
 * \brief Initialize the Port Security Limit Control module
 *
 * \param cmd [IN]: Reason why calling this function.
 * \param p1  [IN]: Parameter 1. Usage varies with cmd.
 * \param p2  [IN]: Parameter 2. Usage varies with cmd.
 *
 * \return
 *    VTSS_RC_OK.
 */
mesa_rc psec_limit_init(vtss_init_data_t *data);

#ifdef __cplusplus
}
#endif
#endif /* _PSEC_LIMIT_API_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
