/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "critd_api.h"                 /* For semaphore wrapper      */
#include <vtss/appl/psec.h>            /* For ourselves              */
#include <psec_limit_api.h>            /* For ourselves              */
#include "msg_api.h"                   /* For msg_switch_is_master() */
#include "main.h"                      /* For vtss_xstr()            */
#include "misc_api.h"                  /* For iport2uport()          */
#ifdef VTSS_SW_OPTION_SYSLOG
#include "syslog_api.h"                /* For S_xxx() macros         */
#endif
#include "psec_limit_trace.h"
#ifdef VTSS_SW_OPTION_ICFG
#include "psec_limit_icli_functions.h" /* For psec_limit_icfg_init() */
#endif
#if defined(VTSS_SW_OPTION_SNMP)
#include "vtss_os_wrapper_snmp.h"
#endif

/****************************************************************************/
// Trace definitions
/****************************************************************************/

#if (VTSS_TRACE_ENABLED)
/* Trace registration. Initialized by psec_init() */
static vtss_trace_reg_t trace_reg = {
    VTSS_TRACE_MODULE_ID, "psec_limit", "Port Security Limit Control module"
};

#ifndef PSEC_LIMIT_DEFAULT_TRACE_LVL
#define PSEC_LIMIT_DEFAULT_TRACE_LVL VTSS_TRACE_LVL_WARNING
#endif

static vtss_trace_grp_t trace_grps[TRACE_GRP_CNT] = {
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default",
        PSEC_LIMIT_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CRIT */ {
        "crit",
        "Critical regions",
        PSEC_LIMIT_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_ICLI */ {
        "iCLI",
        "ICLI",
        PSEC_LIMIT_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP,
    },
};
#endif /* VTSS_TRACE_ENABLED */

/******************************************************************************/
// Semaphore stuff.
/******************************************************************************/
static critd_t PSEC_LIMIT_crit;

// Macros for accessing semaphore functions
// -----------------------------------------
#if VTSS_TRACE_ENABLED
#define PSEC_LIMIT_CRIT_ENTER()         critd_enter(        &PSEC_LIMIT_crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define PSEC_LIMIT_CRIT_EXIT()          critd_exit(         &PSEC_LIMIT_crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define PSEC_LIMIT_CRIT_ASSERT_LOCKED() critd_assert_locked(&PSEC_LIMIT_crit, TRACE_GRP_CRIT,                       __FILE__, __LINE__)
#else
// Leave out function and line arguments
#define PSEC_LIMIT_CRIT_ENTER()         critd_enter(        &PSEC_LIMIT_crit)
#define PSEC_LIMIT_CRIT_EXIT()          critd_exit(         &PSEC_LIMIT_crit)
#define PSEC_LIMIT_CRIT_ASSERT_LOCKED() critd_assert_locked(&PSEC_LIMIT_crit)
#endif

/**
  * \brief Per-switch Port Security Limit Control Configuration.
  */
typedef struct  {
    /**
      * \brief Array of port configurations - one per front port on the switch.
      *
      * Index 0 is the first port.
      */
    CapArray<vtss_appl_psec_interface_conf_t, MESA_CAP_PORT_CNT> port_conf;
} psec_limit_switch_conf_t;

// Overall configuration (valid on master only).
typedef struct {
    // One instance of the global configuration
    vtss_appl_psec_global_conf_t global_conf;

    // One instance per port in the stack of the per-port configuration.
    // Index 0 corresponds to VTSS_ISID_START. Used by the master
    // for the per-switch configuration.
    psec_limit_switch_conf_t switch_conf[VTSS_ISID_CNT];
} psec_limit_stack_conf_t;

static psec_limit_stack_conf_t PSEC_LIMIT_stack_conf;

// Reference counting per port.
// Index 0 holds number of MACs that we told the PSEC module to put in forwarding state.
// Index 1 holds number of MACs that we told the PSEC module to put in blocked state.
static u32 PSEC_LIMIT_ref_cnt[VTSS_ISID_CNT][VTSS_MAX_PORTS_LEGACY_CONSTANT_USE_CAPARRAY_INSTEAD][2];

// If not debugging, set PSEC_LIMIT_INLINE to inline
#define PSEC_LIMIT_INLINE inline

/******************************************************************************/
//
// Module Private Functions
//
/******************************************************************************/

/******************************************************************************/
// PSEC_LIMIT_master_isid_port_check()
// Returns VTSS_RC_OK if we're master and isid and port are legal.
/******************************************************************************/
static mesa_rc PSEC_LIMIT_master_isid_port_check(vtss_isid_t isid, mesa_port_no_t port, BOOL check_port)
{
    if (!msg_switch_is_master()) {
        return PSEC_LIMIT_ERROR_MUST_BE_MASTER;
    }

    if (!VTSS_ISID_LEGAL(isid)) {
        return PSEC_LIMIT_ERROR_INV_ISID;
    }

    // In case someone changes VTSS_PORT_NO_START back to 1, we better survive that,
    // so tell lint to not report "Relational operator '<' always evaluates to 'false'"
    // and "non-negative quantity is never less than zero".
    /*lint -e{685, 568} */
    if (check_port && (port < VTSS_PORT_NO_START || port >= port_isid_port_count(isid) + VTSS_PORT_NO_START || port_isid_port_no_is_stack(isid, port))) {
        // port is only set to something different from VTSS_PORT_NO_START in case
        // we really need to check that the port exists on a given switch and not a stack port,
        // so it's safe to check against actual number of ports and stack ports, rather than
        // checking against mesa_port_cnt(nullptr).
        return PSEC_LIMIT_ERROR_INV_PORT;
    }

    return VTSS_RC_OK;
}

/******************************************************************************/
// PSEC_LIMIT_on_mac_add_callback()
// This function will be called by the PSEC module whenever a new MAC address
// is to be added.
/******************************************************************************/
static psec_add_method_t PSEC_LIMIT_on_mac_add_callback(vtss_isid_t isid, mesa_port_no_t port, mesa_vid_mac_t *vid_mac, u32 mac_cnt_before_callback, psec_add_action_t *action)
{
    mesa_rc                         rc;
    vtss_appl_psec_interface_conf_t *port_conf;
    psec_add_method_t               result = PSEC_ADD_METHOD_FORWARD;
    u32                             *fwd_cnt, *blk_cnt;

#ifdef VTSS_SW_OPTION_SYSLOG
    char macstr[18];
    char prefix_str[150];

    (void)misc_mac_txt(vid_mac->mac.addr, macstr);
    sprintf(prefix_str, "PORT-SECURITY: Interface %s, MAC %s. ", SYSLOG_PORT_INFO_REPLACE_KEYWORD, macstr);
#endif /* VTSS_SW_OPTION_SYSLOG */

    if ((rc = PSEC_LIMIT_master_isid_port_check(isid, port, TRUE)) != VTSS_RC_OK || action == NULL) {
        if (rc != PSEC_LIMIT_ERROR_MUST_BE_MASTER) {
            T_E("Internal error: Invalid parameter (rc = %d)", rc);
        }

        return PSEC_ADD_METHOD_FORWARD; // There's a bug that must be fixed, so we can return anything.
    }

    PSEC_LIMIT_CRIT_ENTER();

    port_conf = &PSEC_LIMIT_stack_conf.switch_conf[isid - VTSS_ISID_START].port_conf[port - VTSS_PORT_NO_START];
    if (!port_conf->enabled) {
        // Limit control is not enabled on that port. Allow it.
        goto do_exit;
    }

    // PSEC_LIMIT_ref_cnt[][][] uses zero-based indexing.
    fwd_cnt = &PSEC_LIMIT_ref_cnt[isid - VTSS_ISID_START][port - VTSS_PORT_NO_START][0];
    blk_cnt = &PSEC_LIMIT_ref_cnt[isid - VTSS_ISID_START][port - VTSS_PORT_NO_START][1];

    T_I("%u:%u: Enter ADD MAC. Cur Fwd = %u, Cur Blk = %u", isid, iport2uport(port), *fwd_cnt, *blk_cnt);

    if (*fwd_cnt + *blk_cnt != mac_cnt_before_callback) {
        // This may occur because - from a PSEC module perspective - the "on-mac-add" call is not protected by PSEC module's mutex, whereas the "on-mac-del" call is.
        T_I("%u:%u: Disagreement between PSEC and PSEC Limit: mac_cnt = %u, fwd = %u, blk = %u", isid, iport2uport(port), mac_cnt_before_callback, *fwd_cnt, *blk_cnt);
    }

    switch (port_conf->violation_mode) {
    case VTSS_APPL_PSEC_VIOLATION_MODE_PROTECT:

        // If violation_mode == PROTECT, then we shouldn't be called if the number of MAC addresses before this
        // one gets added is at or above the limit, since we've turned off CPU copy in previous call.
        if (mac_cnt_before_callback >= port_conf->limit) {
            T_E("%u:%u: Called with invalid mac_cnt (%u, %u, %u). Limit=%u, violation_mode=%d", isid, iport2uport(port), mac_cnt_before_callback, *fwd_cnt, *blk_cnt, port_conf->limit, port_conf->violation_mode);
        }

    // Fall through
    case VTSS_APPL_PSEC_VIOLATION_MODE_RESTRICT:
        // In protect and restrict mode, we must always return limit reached as long as the number of
        // forwarding MAC addresses is at the limit.
        *action = *fwd_cnt + 1 >= port_conf->limit ? PSEC_ADD_ACTION_LIMIT_REACHED : PSEC_ADD_ACTION_NONE;

#ifdef VTSS_SW_OPTION_SYSLOG
        // Send a message to the syslog when reaching the limit.
        if (*fwd_cnt + 1 == port_conf->limit) {
            S_PORT_I(isid, port, "%sLimit reached (%s mode).", prefix_str, port_conf->violation_mode == VTSS_APPL_PSEC_VIOLATION_MODE_PROTECT ? "protect" : "restrict");
        }
#endif
        break;

    case VTSS_APPL_PSEC_VIOLATION_MODE_SHUTDOWN:
        // If violation_mode == SHUTDOWN, then we shouldn't be called if the number of MAC addresses before this
        // one gets added is above the limit, since we've shut down the port (and removed all MAC addresses)
        // prior to this call.
        if (mac_cnt_before_callback > port_conf->limit) {
            T_E("%u:%u: Called with invalid mac_cnt (%u, %u, %u). Limit=%u, violation_mode=%d", isid, iport2uport(port), mac_cnt_before_callback, *fwd_cnt, *blk_cnt, port_conf->limit, port_conf->violation_mode);
        }

        // When *reaching* the limit, set limit reached. The PSEC module will keep CPU-copying enabled
        // because we've set the port mode to PSEC_PORT_MODE_RESTRICT.
        // When *exceeding* the limit, shut down the port.
        *action = *fwd_cnt + 1 == port_conf->limit ? PSEC_ADD_ACTION_LIMIT_REACHED : *fwd_cnt == port_conf->limit ? PSEC_ADD_ACTION_SHUT_DOWN : PSEC_ADD_ACTION_NONE;

        // When shutting down the port, the forward decision doesn't really matter to the PSEC module,
        // but it doesn't harm to set it "correctly".
        result = *fwd_cnt < port_conf->limit ? PSEC_ADD_METHOD_FORWARD : PSEC_ADD_METHOD_BLOCK;

#ifdef VTSS_SW_OPTION_SYSLOG
        if (*action == PSEC_ADD_ACTION_SHUT_DOWN) {
            S_PORT_W(isid, port, "%sLimit exceeded. Shutting down the port.", prefix_str);
        }
#endif
        break;

    default:
        T_E("Unknown action (%d)", port_conf->violation_mode);
        break;
    }

    // No matter the violation_mode, we allow this MAC address as long as we haven't reached the limit
    // and block it if limit is reached
    result = *fwd_cnt < port_conf->limit ? PSEC_ADD_METHOD_FORWARD : PSEC_ADD_METHOD_BLOCK;

    if (result == PSEC_ADD_METHOD_FORWARD) {
        (*fwd_cnt)++;
    } else {
        (*blk_cnt)++;
    }

    T_D("%u:%u: ADD MAC. New Fwd = %u, New Blk = %u: %s - %s)",
        isid, iport2uport(port),
        *fwd_cnt, *blk_cnt,
        psec_add_method_to_str(result),
        *action == PSEC_ADD_ACTION_LIMIT_REACHED ? "Limit reached" : *action == PSEC_ADD_ACTION_SHUT_DOWN ? "Shutdown" : "Ready");

do_exit:
    PSEC_LIMIT_CRIT_EXIT();
    return result;
}

/******************************************************************************/
// PSEC_LIMIT_on_mac_del_callback()
/******************************************************************************/
static void PSEC_LIMIT_on_mac_del_callback(vtss_isid_t isid, mesa_port_no_t port, const mesa_vid_mac_t *vid_mac, psec_del_reason_t reason, psec_add_method_t add_method)
{
    int                             idx = add_method == PSEC_ADD_METHOD_FORWARD ? 0 : 1;
    u32                             *val, *fwd_cnt, *blk_cnt;
    vtss_appl_psec_interface_conf_t *port_conf;

    PSEC_LIMIT_CRIT_ENTER();

    // PSEC_LIMIT_ref_cnt[][][] uses zero-based indexing.
    fwd_cnt   = &PSEC_LIMIT_ref_cnt[isid - VTSS_ISID_START][port - VTSS_PORT_NO_START][0];
    blk_cnt   = &PSEC_LIMIT_ref_cnt[isid - VTSS_ISID_START][port - VTSS_PORT_NO_START][1];
    val       = &PSEC_LIMIT_ref_cnt[isid - VTSS_ISID_START][port - VTSS_PORT_NO_START][idx];
    port_conf = &PSEC_LIMIT_stack_conf.switch_conf[isid - VTSS_ISID_START].port_conf[port - VTSS_PORT_NO_START];

    T_I("%u:%u: Enter DEL MAC. Cur Fwd = %u, Cur Blk = %u", isid, iport2uport(port), *fwd_cnt, *blk_cnt);

    if (add_method != PSEC_ADD_METHOD_FORWARD && add_method != PSEC_ADD_METHOD_BLOCK) {
        // Odd to get called with an add_method that this module doesn't support.
        // We can only add with forward or block.
        T_E("%u:%u: Odd add_method %d", isid, iport2uport(port), add_method);
        goto do_exit;
    }

    if (*val == 0) {
        T_E("%u:%u: Reference count for add_method %s is 0", isid, iport2uport(port), psec_add_method_to_str(add_method));
        goto do_exit;
    }

    (*val)--;
    T_D("%u:%u: DEL MAC. New Fwd = %u, New Blk = %u (was added with Method = %s)", isid, iport2uport(port), *fwd_cnt, *blk_cnt, psec_add_method_to_str(add_method));

do_exit:
    PSEC_LIMIT_CRIT_EXIT();
}

/******************************************************************************/
// PSEC_LIMIT_conf_valid_port()
/******************************************************************************/
static mesa_rc PSEC_LIMIT_conf_valid_port(const vtss_appl_psec_interface_conf_t *const port_conf)
{
#if PSEC_LIMIT_MIN > 0
    if (port_conf->limit < PSEC_LIMIT_MIN) {
        return PSEC_LIMIT_ERROR_INV_LIMIT;
    }
#endif

    if (port_conf->limit > PSEC_LIMIT_MAX) {
        return PSEC_LIMIT_ERROR_INV_LIMIT;
    }

    if (port_conf->violate_limit < PSEC_VIOLATE_LIMIT_MIN || port_conf->violate_limit > PSEC_VIOLATE_LIMIT_MAX) {
        return PSEC_LIMIT_ERROR_INV_VIOLATE_LIMIT;
    }

    if (port_conf->violation_mode >= VTSS_APPL_PSEC_VIOLATION_MODE_LAST) {
        return PSEC_LIMIT_ERROR_INV_VIOLATION_MODE;
    }

    return VTSS_RC_OK;
}

/******************************************************************************/
// PSEC_LIMIT_conf_default_switch()
/******************************************************************************/
static void PSEC_LIMIT_conf_default_switch(psec_limit_switch_conf_t *switch_conf)
{
    vtss_appl_psec_interface_conf_t def_port_conf;
    mesa_port_no_t                  iport;
    mesa_rc                         rc;

    if ((rc = vtss_appl_psec_interface_conf_default_get(&def_port_conf)) != VTSS_RC_OK) {
        T_E("Internal error: %s", error_txt(rc));
    }

    for (iport = 0; iport < switch_conf->port_conf.size(); iport++) {
        switch_conf->port_conf[iport] = def_port_conf;
    }
}

/******************************************************************************/
// PSEC_LIMIT_conf_default_all()
/******************************************************************************/
static void PSEC_LIMIT_conf_default_all(psec_limit_stack_conf_t *stack_conf)
{
    vtss_isid_t isid;

    (void)vtss_appl_psec_global_conf_default_get(&stack_conf->global_conf);
    for (isid = 0; isid < VTSS_ISID_CNT; isid++) {
        PSEC_LIMIT_conf_default_switch(&stack_conf->switch_conf[isid]);
    }
}

/******************************************************************************/
// PSEC_LIMIT_conf_apply()
/******************************************************************************/
static mesa_rc PSEC_LIMIT_conf_apply(vtss_isid_t isid, psec_limit_stack_conf_t *new_conf, BOOL interface_conf_may_be_changed)
{
    vtss_isid_t             isid_start, isid_end;
    mesa_port_no_t          port;
    mesa_rc                 rc;
    psec_limit_stack_conf_t *old_conf = &PSEC_LIMIT_stack_conf;

    PSEC_LIMIT_CRIT_ASSERT_LOCKED();

    // Change the age- and hold-times if requested to.
    if (isid == VTSS_ISID_GLOBAL) {
        if ((rc = psec_mgmt_time_conf_set(VTSS_APPL_PSEC_USER_ADMIN, new_conf->global_conf.enable_aging ? new_conf->global_conf.aging_period_secs : 0, new_conf->global_conf.hold_time_secs)) != VTSS_RC_OK) {
            return rc;
        }
    }

    if (!interface_conf_may_be_changed) {
        // Nothing more to do.
        return VTSS_RC_OK;
    }

    // In the following, we use zero-based port- and isid-counters.
    if (isid == VTSS_ISID_GLOBAL) {
        // Apply the enable thing to all ports.
        isid_start = 0;
        isid_end   = VTSS_ISID_CNT - 1;
    } else {
        isid_start = isid_end = isid - VTSS_ISID_START;
    }

    for (isid = isid_start; isid <= isid_end; isid++) {
        psec_limit_switch_conf_t *old_switch_conf = &old_conf->switch_conf[isid];
        psec_limit_switch_conf_t *new_switch_conf = &new_conf->switch_conf[isid];

        for (port = 0; port < mesa_port_cnt(nullptr); port++) {
            vtss_appl_psec_interface_conf_t *old_port_conf  = &old_switch_conf->port_conf[port];
            vtss_appl_psec_interface_conf_t *new_port_conf  = &new_switch_conf->port_conf[port];
            BOOL                            old_was_enabled = old_port_conf->enabled;
            BOOL                            new_is_enabled  = new_port_conf->enabled;
            BOOL                            call_ena_func   = FALSE;
            BOOL                            reopen_port     = FALSE;
            BOOL                            limit_reached   = FALSE;

            if (!old_was_enabled) {
                if (new_is_enabled) {
                    // Old was not enabled, but we're going to enable.
                    // Remove all entries from the port.
                    call_ena_func = TRUE;
                    reopen_port   = FALSE;
                    limit_reached = new_port_conf->limit == 0;
                }
            } else {
                if (!new_is_enabled) {
                    // We're going from enabled to disabled.
                    // Delete limits and shutdown properties on the port.
                    call_ena_func = TRUE;
                    reopen_port   = TRUE;
                } else {
                    // Old was enabled and new is enabled.
                    // According to DS, we should remove all entries on the port if the limit or the violation_mode changes
                    // (a bit silly I think, but they get what they want).
                    if (new_port_conf->limit          != old_port_conf->limit          ||
                        new_port_conf->violation_mode != old_port_conf->violation_mode ||
                        new_port_conf->violate_limit  != old_port_conf->violate_limit) {
                        call_ena_func = TRUE;
                        reopen_port   = TRUE;
                        limit_reached = new_port_conf->limit == 0;
                    }
                }
            }

            if (call_ena_func) {
                // Both when violation_mode is VTSS_APPL_PSEC_VIOLATION_MODE_RESTRICT and VTSS_APPL_PSEC_VIOLATION_MODE_SHUTDOWN,
                // we ask for keeping CPU copying enabled even though the limit is reached. In the first case
                // because we need to count violating MAC addresses, and in the second case, because we
                // need an extra MAC address beyond the limit to trigger the shut down.
                psec_port_mode_t port_mode = new_port_conf->violation_mode == VTSS_APPL_PSEC_VIOLATION_MODE_PROTECT ? PSEC_PORT_MODE_NORMAL : PSEC_PORT_MODE_RESTRICT;

                PSEC_LIMIT_ref_cnt[isid][port][0] = 0;
                PSEC_LIMIT_ref_cnt[isid][port][1] = 0;

                if ((rc = psec_mgmt_port_conf_set(VTSS_APPL_PSEC_USER_ADMIN, isid + VTSS_ISID_START, port + VTSS_PORT_NO_START, new_is_enabled, reopen_port, limit_reached, port_mode, new_port_conf->violate_limit)) != VTSS_RC_OK) {
                    return rc;
                }
            }
        }
    }

    return VTSS_RC_OK;
}

/******************************************************************************/
// PSEC_LIMIT_default()
// Create defaults for either
//   1) the global section (#default_all == FALSE && isid == VTSS_ISID_GLOBAL)
//   2) one switch (#default_all == FALSE && VTSS_ISID_LEGAL(isid), or for both
//   3) for both the global section and all switches (#default_all == TRUE).
/******************************************************************************/
static void PSEC_LIMIT_default(vtss_isid_t isid, BOOL default_all)
{
    psec_limit_stack_conf_t tmp_stack_conf;
    BOOL                    interface_conf_may_be_changed;

    T_D("enter, isid: %d, default_all = %d", isid, default_all);

    PSEC_LIMIT_CRIT_ENTER();
    // Get the current settings into tmp_stack_conf, so that we can compare changes later on.
    tmp_stack_conf = PSEC_LIMIT_stack_conf;

    if (default_all) {
        PSEC_LIMIT_conf_default_all(&tmp_stack_conf);
        isid = VTSS_ISID_GLOBAL;
        interface_conf_may_be_changed = TRUE;
    } else if (isid == VTSS_ISID_GLOBAL) {
        // Default the global settings.
        (void)vtss_appl_psec_global_conf_default_get(&tmp_stack_conf.global_conf);
        interface_conf_may_be_changed = FALSE;
    } else {
        PSEC_LIMIT_conf_default_switch(&tmp_stack_conf.switch_conf[isid - VTSS_ISID_START]);
        interface_conf_may_be_changed = TRUE;
    }

    // Apply the new configuration
    (void)PSEC_LIMIT_conf_apply(isid, &tmp_stack_conf, interface_conf_may_be_changed);

    // Copy our temporary settings to the real settings.
    PSEC_LIMIT_stack_conf = tmp_stack_conf;

    PSEC_LIMIT_CRIT_EXIT();
}

/******************************************************************************/
//
// Module Public Functions
//
/******************************************************************************/

/******************************************************************************/
// vtss_appl_psec_global_conf_default_get()
/******************************************************************************/
mesa_rc vtss_appl_psec_global_conf_default_get(vtss_appl_psec_global_conf_t *const global_conf)
{
    if (!global_conf) {
        return PSEC_LIMIT_ERROR_INV_PARAM;
    }

    memset(global_conf, 0, sizeof(*global_conf));
    global_conf->aging_period_secs = 3600; // 1 hour
    global_conf->hold_time_secs    =  300; // 5 minutes

    return VTSS_RC_OK;
}

/******************************************************************************/
// vtss_appl_psec_global_conf_get()
/******************************************************************************/
mesa_rc vtss_appl_psec_global_conf_get(vtss_appl_psec_global_conf_t *const global_conf)
{
    mesa_rc rc;

    if (!global_conf) {
        return PSEC_LIMIT_ERROR_INV_PARAM;
    }

    if ((rc = PSEC_LIMIT_master_isid_port_check(VTSS_ISID_START, VTSS_PORT_NO_START, FALSE)) != VTSS_RC_OK) {
        return rc;
    }

    PSEC_LIMIT_CRIT_ENTER();
    *global_conf = PSEC_LIMIT_stack_conf.global_conf;
    PSEC_LIMIT_CRIT_EXIT();

    return VTSS_RC_OK;
}

/******************************************************************************/
// vtss_appl_psec_global_conf_set()
/******************************************************************************/
mesa_rc vtss_appl_psec_global_conf_set(const vtss_appl_psec_global_conf_t *const global_conf)
{
    mesa_rc                rc;
    psec_limit_stack_conf_t tmp_stack_conf;

    if (!global_conf) {
        return PSEC_LIMIT_ERROR_INV_PARAM;
    }

    if ((rc = PSEC_LIMIT_master_isid_port_check(VTSS_ISID_START, VTSS_PORT_NO_START, FALSE)) != VTSS_RC_OK) {
        return rc;
    }

    if (global_conf->aging_period_secs < PSEC_AGE_TIME_MIN || global_conf->aging_period_secs > PSEC_AGE_TIME_MAX) {
        return PSEC_LIMIT_ERROR_INV_AGING_PERIOD;
    }

    if (global_conf->hold_time_secs < PSEC_HOLD_TIME_MIN || global_conf->hold_time_secs > PSEC_HOLD_TIME_MAX) {
        return PSEC_LIMIT_ERROR_INV_HOLD_TIME;
    }

    PSEC_LIMIT_CRIT_ENTER();

    // We need to create a new structure with the current config
    // and only replace the global_conf member.
    tmp_stack_conf = PSEC_LIMIT_stack_conf;
    tmp_stack_conf.global_conf = *global_conf;

    // Apply the configuration to the PSEC module. The function
    // will check differences between old and new config
    if ((rc = PSEC_LIMIT_conf_apply(VTSS_ISID_GLOBAL, &tmp_stack_conf, FALSE)) == VTSS_RC_OK) {
        // Copy the user's configuration to our configuration
        PSEC_LIMIT_stack_conf.global_conf = *global_conf;
    } else {
        // Roll back to previous settings without checking the return code
        (void)PSEC_LIMIT_conf_apply(VTSS_ISID_GLOBAL, &PSEC_LIMIT_stack_conf, FALSE);
    }

    PSEC_LIMIT_CRIT_EXIT();
    return rc;
}


/******************************************************************************/
// vtss_appl_psec_interface_conf_default_get()
/******************************************************************************/
mesa_rc vtss_appl_psec_interface_conf_default_get(vtss_appl_psec_interface_conf_t *const port_conf)
{
    if (!port_conf) {
        return PSEC_LIMIT_ERROR_INV_PARAM;
    }

    memset(port_conf, 0, sizeof(*port_conf));
    port_conf->limit = 4;
    port_conf->violate_limit = 4;

    return VTSS_RC_OK;
}

/******************************************************************************/
// vtss_appl_psec_interface_conf_get()
/******************************************************************************/
mesa_rc vtss_appl_psec_interface_conf_get(vtss_ifindex_t ifindex, vtss_appl_psec_interface_conf_t *const port_conf)
{
    vtss_ifindex_elm_t ife;
    mesa_rc            rc;

    if (!port_conf) {
        return PSEC_LIMIT_ERROR_INV_PARAM;
    }

    if (vtss_ifindex_decompose(ifindex, &ife) != VTSS_RC_OK) {
        return PSEC_LIMIT_ERROR_INV_PARAM;
    }

    if (ife.iftype != VTSS_IFINDEX_TYPE_PORT) {
        return PSEC_LIMIT_ERROR_INV_PARAM;
    }

    if ((rc = PSEC_LIMIT_master_isid_port_check(ife.isid, ife.ordinal, TRUE)) != VTSS_RC_OK) {
        return rc;
    }

    PSEC_LIMIT_CRIT_ENTER();
    *port_conf = PSEC_LIMIT_stack_conf.switch_conf[ife.isid - VTSS_ISID_START].port_conf[ife.ordinal - VTSS_PORT_NO_START];
    PSEC_LIMIT_CRIT_EXIT();

    return VTSS_RC_OK;
}

/******************************************************************************/
// vtss_appl_psec_interface_conf_set()
/******************************************************************************/
mesa_rc vtss_appl_psec_interface_conf_set(vtss_ifindex_t ifindex, const vtss_appl_psec_interface_conf_t *const port_conf)
{
    psec_limit_stack_conf_t tmp_stack_conf;
    vtss_ifindex_elm_t      ife;
    mesa_rc                 rc;

    if (!port_conf) {
        return PSEC_LIMIT_ERROR_INV_PARAM;
    }

    if (vtss_ifindex_decompose(ifindex, &ife) != VTSS_RC_OK) {
        return PSEC_LIMIT_ERROR_INV_PARAM;
    }

    if (ife.iftype != VTSS_IFINDEX_TYPE_PORT) {
        return PSEC_LIMIT_ERROR_INV_PARAM;
    }

    if ((rc = PSEC_LIMIT_master_isid_port_check(ife.isid, ife.ordinal, TRUE)) != VTSS_RC_OK) {
        return rc;
    }

    if ((rc = PSEC_LIMIT_conf_valid_port(port_conf)) != VTSS_RC_OK) {
        return rc;
    }

    PSEC_LIMIT_CRIT_ENTER();

    tmp_stack_conf = PSEC_LIMIT_stack_conf;
    tmp_stack_conf.switch_conf[ife.isid - VTSS_ISID_START].port_conf[ife.ordinal - VTSS_PORT_NO_START] = *port_conf;

    // Apply the configuration to the PSEC module. The function will check differences between old and new config
    if ((rc = PSEC_LIMIT_conf_apply(ife.isid, &tmp_stack_conf, TRUE)) == VTSS_RC_OK) {
        // Copy the user's configuration to our configuration
        PSEC_LIMIT_stack_conf.switch_conf[ife.isid - VTSS_ISID_START].port_conf[ife.ordinal - VTSS_PORT_NO_START] = *port_conf;
    } else {
        // Roll back to previous settings without checking the return code
        (void)PSEC_LIMIT_conf_apply(VTSS_ISID_GLOBAL, &PSEC_LIMIT_stack_conf, TRUE);
    }

    PSEC_LIMIT_CRIT_EXIT();

    return rc;
}

/******************************************************************************/
// psec_limit_error_txt()
/******************************************************************************/
const char *psec_limit_error_txt(mesa_rc rc)
{
    switch (rc) {
    case PSEC_LIMIT_ERROR_INV_PARAM:
        return "Invalid parameter";

    case PSEC_LIMIT_ERROR_MUST_BE_MASTER:
        return "Operation only valid on master switch";

    case PSEC_LIMIT_ERROR_INV_ISID:
        return "Invalid Switch ID";

    case PSEC_LIMIT_ERROR_INV_PORT:
        return "Invalid port number";

    case PSEC_LIMIT_ERROR_INV_AGING_PERIOD:
        return "Aging period out of bounds";

    case PSEC_LIMIT_ERROR_INV_HOLD_TIME:
        return "Hold time out of bounds";

    case PSEC_LIMIT_ERROR_INV_LIMIT:
        return "Invalid limit";

    case PSEC_LIMIT_ERROR_INV_VIOLATE_LIMIT:
        return "Invalid violation limit";

    case PSEC_LIMIT_ERROR_INV_VIOLATION_MODE:
        return "The violation mode is out of bounds";

    case PSEC_LIMIT_ERROR_STATIC_AGGR_ENABLED:
        return "Limit control cannot be enabled for ports that are enabled for static aggregation";

    case PSEC_LIMIT_ERROR_DYNAMIC_AGGR_ENABLED:
        return "Limit control cannot be enabled for ports that are enabled for LACP";

    default:
        return "Port Security Limit Control: Unknown error code";
    }
}

extern "C" int psec_limit_icli_cmd_register();

/******************************************************************************/
// psec_limit_init()
/******************************************************************************/
mesa_rc psec_limit_init(vtss_init_data_t *data)
{
    vtss_isid_t isid = data->isid;
    mesa_rc     rc;

    switch (data->cmd) {
    case INIT_CMD_EARLY_INIT:
        // Initialize and register trace resources
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);
        break;

    case INIT_CMD_INIT:
        // Initialize sempahores.
        critd_init(&PSEC_LIMIT_crit, "crit_psec_limit", VTSS_MODULE_ID_PSEC_LIMIT, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);

        psec_limit_icli_cmd_register();

        // When created, the semaphore was initially locked.
        PSEC_LIMIT_CRIT_EXIT();
        break;

    case INIT_CMD_START:
        // We must also install callback handlers in the psec module. We will then be called
        // whenever the psec module adds MAC addresses to the MAC table. We don't care
        // when the psec module deletes a MAC address from the MAC table (if the port limit is
        // currently reached, the PSEC module will autonomously clear the reached limit flag
        // when deleting a MAC address).
        // Do this as soon as possible in the boot process.
        if ((rc = psec_mgmt_register_callbacks(VTSS_APPL_PSEC_USER_ADMIN, PSEC_LIMIT_on_mac_add_callback, PSEC_LIMIT_on_mac_del_callback)) != VTSS_RC_OK) {
            T_E("Unable to register callbacks (%s)", error_txt(rc));
        }

#ifdef VTSS_SW_OPTION_ICFG
        VTSS_RC(psec_limit_icfg_init()); // ICFG initialization (Show running)
#endif
        break;

    case INIT_CMD_CONF_DEF:
        if (isid == VTSS_ISID_LOCAL) {
            // Reset local configuration
            // No such configuration for this module
        } else if (VTSS_ISID_LEGAL(isid) || isid == VTSS_ISID_GLOBAL) {
            // Reset switch or stack configuration
            PSEC_LIMIT_default(isid, FALSE);
        }
        break;

    case INIT_CMD_MASTER_UP:
        PSEC_LIMIT_default(VTSS_ISID_GLOBAL, TRUE);
        break;

    case INIT_CMD_MASTER_DOWN:
        break;

    case INIT_CMD_SWITCH_ADD:
        break;

    case INIT_CMD_SWITCH_DEL:
        break;

    default:
        break;
    }

    return VTSS_RC_OK;
}

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
