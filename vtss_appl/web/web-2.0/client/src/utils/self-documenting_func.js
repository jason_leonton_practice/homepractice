/* type "%" into decodeURIComponent will cause error
   Ref: https://stackoverflow.com/questions/7449588/why-does-decodeuricomponent-lock-up-my-browser */
export function decodeURIComponentSafe(s) {
  if (!s) return s;

  return decodeURIComponent(s.replace(/%(?![0-9][0-9a-fA-F]+)/g, "%25"));
}

export function compensateZero(val, separator) {
  // i.e. 192.168.10.5 -> return 192168010005
  let str = val ? val.toString() : "";
  const splitIP = str.split(separator);

  let newStr = splitIP.map(val => {
    if (val.length < 3) {
      // if (val.length === 0) return "000";
      if (val.length === 1) return "00" + val;
      if (val.length === 2) return "0" + val;
    }

    return val;
  });

  let int = newStr.join("") !== "" ? parseInt(newStr.join("")) : 0;
  if (!Number.isInteger(int)) int = 0;

  return int;
}

export function valOrderCompare(type, _order, aVal, bVal) {
  if (type === "str") {
    if (_order === "asc") {
      if (aVal > bVal) return 1;
      if (aVal < bVal) return -1;
      return 0;
    }
    if (_order === "desc") {
      if (aVal > bVal) return -1;
      if (aVal < bVal) return 1;
      return 0;
    }
  } else if (type === "num") {
    if (_order === "asc") return aVal - bVal;
    if (_order === "desc") return bVal - aVal;
  }
}

export function directionConverter(order) {
  if (order === "asc") {
    return "up";
  } else if (order === "desc") {
    return "down";
  } else {
    return null;
  }
}
