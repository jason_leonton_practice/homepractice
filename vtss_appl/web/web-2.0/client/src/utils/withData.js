import React from "react";
import { toast } from "react-toastify";
import qs from "querystring";
import Fade from "react-reveal/Fade";

import { InfoMsg, msgConfig, displayReadOnlyToast } from "../components/messages";
import { LoadingSegment } from "../components/loading_segment";

const withData = (WrappedComponent, url, isAutoRefresh = false, queryParams = []) => {
  return class WithData extends React.Component {
    refreshID = null;
    state = { data: null, noRead: false, readOnly: false, url, isAutoRefresh, shouldUpdateURI: true };

    addQuery = api => {
      const query = this.props.location.search.slice(1);
      const parsed = qs.parse(query);

      for (let key in parsed) {
        if (parsed.hasOwnProperty(key) && parsed[key]) {
          // Only search required query, otherwise skip
          if (!queryParams.includes(key)) continue;

          // Check whether the same query includes in api string
          const apiQuery = api.indexOf("?") !== -1 ? api.slice(api.indexOf("?") + 1) : "";

          if (!apiQuery.includes(key)) {
            if (!api.includes("?")) {
              api = `${api}?${key}=${parsed[key]}`;
            } else {
              api = `${api}&${key}=${parsed[key]}`;
            }
          }
        }
      }

      return api;
    };

    httpRequest = (url, autoAddQuery = true) => {
      let req = new XMLHttpRequest();
      let api = this.state.shouldUpdateURI ? this.state.url : url;

      if (autoAddQuery && this.props.location) {
        api = this.addQuery(api);
      }

      req.open("GET", api, true);
      req.onreadystatechange = () => {
        if (req.readyState === 4) {
          if (req.status && req.status === 200) {
            // you can't see any content
            if (req.getResponseHeader("X-ReadOnly") === "null") {
              this.setState({ noRead: true });
            } else {
              let readOnly = false;

              if (req.getResponseHeader("X-ReadOnly") === "true") {
                displayReadOnlyToast(true);
                readOnly = true;
              }

              // if responseText Contain any error
              if (req.responseText && req.responseText.match(/^Error:\s+/)) {
                this.setState({ data: "" });
                const info =
                  "A stack communication error occurred. Configuration changes may not have been applied. Please reload the switch selector list - the selected switch may have left the stack.";
                return toast.error(InfoMsg("Stack Communication Error", info), msgConfig(false));
              }

              // A 200ms setTimout can prevent flashing spinner
              setTimeout(() => {
                this.setState({ data: req.response, readOnly }, () => {
                  clearInterval(this.refreshID);
                  if (this.state.isAutoRefresh) {
                    this.refreshID = setTimeout(() => this.httpRequest(this.state.url), 3000);
                  }
                });
              }, 200);
            }
          } else {
            try {
              if (req.status === 401) {
                // unauthorized should logout
                toast.error(InfoMsg("Error", null, req.status, req.statusText), msgConfig(false));
              } else {
                if (req.status) {
                  const info = `There was a problem getting <code >${req.responseURL}</code> data.`;
                  toast.error(InfoMsg("Error", info, req.status, req.statusText), msgConfig(false));
                } else {
                  alert(
                    "There was a problem getting page data.\nThe firmware probably has been reset, try refresh page."
                  );
                }
              }
            } catch (e) {
              //Nothing to do - the page is probably unloading
            }
            req = null; // MSIE leak avoidance
          }
        }
      };

      req.setRequestHeader("If-Modified-Since", "0"); // none cached
      req.send("");
    };

    handleAutoRefresh = isAutoRefresh => {
      this.setState({ isAutoRefresh }, () => {
        if (this.state.isAutoRefresh) {
          return this.httpRequest(this.state.url);
        }
        clearInterval(this.refreshID);
      });
    };

    handleRefresh = isRefresh => {
      this.httpRequest(this.state.url);
    };

    handleQuerySearch = (fetchURI, shouldUpdateURI = true, autoAddQuery = false) => {
      /* shouldUpdateURI parameter是為了應付按下clear鍵後state隨之改變，若使用者接著按了auto refresh
         會每三秒clear一次data，違背使用情境。加入shouldUpdateURI則為一次性的更新this.props.data      */

      if (!shouldUpdateURI) {
        return this.setState({ shouldUpdateURI: false }, () => this.httpRequest(fetchURI, autoAddQuery));
      }

      this.setState({ url: fetchURI }, () => this.httpRequest(this.state.url, autoAddQuery));
    };

    shouldComponentUpdate(prevProps, prevState) {
      return (
        JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
        JSON.stringify(this.state) !== JSON.stringify(prevState)
      );
    }

    componentWillUnmount() {
      toast.dismiss();
      clearInterval(this.refreshID);
    }

    componentDidMount() {
      this.httpRequest(this.state.url);
    }

    render() {
      const { data, noRead, readOnly } = this.state;

      if (noRead) return <div>Permission Deny</div>;
      return (
        <React.Fragment>
          {data === null ? (
            <LoadingSegment />
          ) : url !== "/stat/portstate" ? ( // API '/stat/portstate/' is for panel, which doesn't need animation
            <Fade bottom distance="40px" duration={600}>
              <WrappedComponent
                {...this.props}
                data={data}
                readOnly={readOnly}
                autoRefresh={this.handleAutoRefresh}
                refresh={this.handleRefresh}
                querySearch={this.handleQuerySearch}
              />
            </Fade>
          ) : (
            <WrappedComponent
              {...this.props}
              data={data}
              readOnly={readOnly}
              autoRefresh={this.handleAutoRefresh}
              refresh={this.handleRefresh}
              querySearch={this.handleQuerySearch}
            />
          )}
        </React.Fragment>
      );
    }
  };
};

export default withData;
