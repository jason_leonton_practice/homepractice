import { isMacAddress } from "./validate";

export function toMacAddress(value) {
  if (!isMacAddress(value)) {
    return "00-00-00-00-00-00";
  }

  let macAddr = [];

  // Split the max address up in 6 part ( Allowed format is 00-11-22-33-44-55 or 001122334455 )
  if (value.indexOf("-") !== -1) {
    macAddr = value.split("-");
  } else {
    for (var j = 0; j <= 5; j++) {
      macAddr[j] = value.substring(j * 2, j * 2 + 2);
    }
  }

  // Generate return var with format XX-XX-XX-XX-XX-XX
  let returnMacAddr = "";
  for (var i = 0; i <= 5; i++) {
    // Pad for 0 in case of MAC address format ( 1-2-23-44-45-46 )
    if (macAddr[i].length === 1) {
      macAddr[i] = "0" + macAddr[i];
    }

    if (i === 5) {
      returnMacAddr += macAddr[i];
    } else {
      returnMacAddr += macAddr[i] + "-";
    }
  }

  return returnMacAddr.toUpperCase();
}
