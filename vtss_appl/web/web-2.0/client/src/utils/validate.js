/* eslint-disable */
import { configDomainNameLengthMax, configHostNameLengthMax } from "./config";

// Check if a number is a valid integer
export const isInt = value => {
  return (
    !isNaN(value) &&
    parseInt(Number(value)) == value &&
    !isNaN(parseInt(value, 10))
  );
};

function IsHex(Value, AlertIsOn) {
  // Default value
  var AlertOn = AlertIsOn == null ? 0 : AlertIsOn;
  var ValueIsHex = Value.match(/^[0-9a-f]+$/i);

  if (!ValueIsHex && AlertOn) {
    alert("Value " + Value + " is not a valid hex number");
  }

  return ValueIsHex;
}

export function isWithinRange(val, MinVal, MaxVal, start_text, end_text) {
  // Default values
  var minval = MinVal == null ? 0 : MinVal;
  var maxval = MaxVal == null ? 65535 : MaxVal;

  if (!start_text) start_text = "";
  if (!end_text) end_text = "";
  val = typeof val === "undefined" ? "" : val.toString();

  // check for hex (0x<value>) notation
  if (
    val.toString().length >= 3 &&
    val[0] === "0" &&
    val[1].toLowerCase() === "x"
  ) {
    var valpart = val.slice(2);
    if (!IsHex(valpart, 0)) {
      return {
        error: true,
        msg: `Invalid hexadecimal number for ${start_text}`
      };
    }

    val = parseInt(valpart, 16);
  }

  //
  if (!isInt(val) || val < minval || val > maxval) {
    // 已驗證
    return {
      error: true,
      msg: `${start_text} must be an integer value between ${minval} and ${maxval} ${end_text}`
    };
  }

  return { error: false, msg: "" };
}

export function isMacAddress(Value, AlertIsOn) {
  // Default value
  var AlertOn = AlertIsOn == null ? 0 : AlertIsOn;

  // Split the max address up in 6 part. The format is 'xx-xx-xx-xx-xx-xx' or 'xx.xx.xx.xx.xx.xx' or 'xxxxxxxxxxxx' (x is a hexadecimal digit).
  if (Value.indexOf("-") !== -1 || Value.indexOf(".") !== -1 || Value.indexOf(":") !== -1) {
    var MACAddr;
    if (Value.indexOf("-") !== -1) {
      MACAddr = Value.split("-");
    } else if (Value.indexOf(".") !== -1) {
      MACAddr = Value.split(".");
    } else {
      MACAddr = Value.split(":");
    }

    if (MACAddr.length === 6) {
      for (var i = 0; i < MACAddr.length; i++) {
        if (MACAddr[i].length > 2) {
          if (AlertOn) {
            alert("MAC address contains more than 2 digits");
          }
          return false;
        } else {
          if (!IsHex(MACAddr[i], AlertOn)) {
            return false;
          }
        }
      }
    } else {
      if (AlertOn) {
        alert("MAC should have 6 digit groups");
      }
      return false;
    }
  } else {
    if (Value.length !== 12) {
      if (AlertOn) {
        alert("MAC address must be 12 characters long");
      }
      return false;
    }

    if (!IsHex(Value, AlertOn)) {
      return false;
    }
  }

  return true;
}

function _isValidHostname(hostname, ignore_null) {
  /* The hostname is defined in RFC 1123 and 952. But we implement the below scenario to fit with eCos DNS package.
       A valid hostname is a string drawn from the alphabet (A-Za-z), digits (0-9), dot (.), hyphen (-).
       Spaces are not allowed, the first character must be an alphanumeric character,
       and the first and last characters must not be a dot or a hyphen.
    */
  if (ignore_null && hostname.length === 0) {
    return true;
  } else if (
    hostname.match(
      /^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/
    )
  ) {
    return true;
  }
  return false;
}

// passing value inside
export function isValidDomainOrIP(
  value = "",
  fld_text,
  allow_fqdn,
  ip_version,
  allow_what4,
  allow_what6,
  is_dns_support,
  at_least_1char
) {
  /* If configDomainNameLengthMax is "undefined", the value is equal to configHostNameLengthMax */
  let _ConfigDomainNameLengthMax = configDomainNameLengthMax;
  let _ConfigHostNameLengthMax = configHostNameLengthMax;

  if (typeof copyConfigDomainNameLengthMax == "undefined") {
    _ConfigDomainNameLengthMax = _ConfigHostNameLengthMax;
  }

  // User-input error
  if (value.length > _ConfigDomainNameLengthMax) {
    return {
      error: true,
      msg: `The length of ${fld_text} is restricted to ${configDomainNameLengthMax}`
    };
  }

  if (at_least_1char) {
    let str = value;
    // var str = fld.value;
    str = str.replace(/^\s+/, "");
    str = str.replace(/\s+$/, "");

    if (str.length <= 0) {
      return {
        error: true,
        msg: `${fld_text} must include at least one character, excluding space.`
      };
    }
  } else if (!value.length) {
    // not really understand!!
    return true; // allow empty string
  }

  let vfy_addr = false;
  let chk_pass = { error: false, msg: "" };

  if (ip_version) {
    vfy_addr = true;
    if (ip_version > 6) {
      chk_pass = isIpStr(
        value,
        false,
        fld_text,
        allow_what4,
        _isIpStrFormat(value) ? false : true
      );

      if (chk_pass["error"] === true) {
        if (_isIpStrFormat(value)) return chk_pass;

        var myReg = /[\:]+/;
        chk_pass = isIpv6Str(
          value,
          fld_text,
          allow_what6,
          !is_dns_support || (is_dns_support && myReg.test(value))
            ? false
            : true
        );

        if (chk_pass["error"] === true) {
          if (!is_dns_support || (is_dns_support && myReg.test(value))) {
            return false;
          }
        }
      }
    } else if (ip_version < 6) {
      // IPv4 only
      chk_pass = isIpStr(
        value,
        false,
        fld_text,
        allow_what4,
        !is_dns_support || (is_dns_support && _isIpStrFormat(value))
          ? false
          : true
      );

      if (
        chk_pass["error"] === true &&
        (!is_dns_support || (is_dns_support && _isIpStrFormat(value)))
      ) {
        return chk_pass;
      }
    } else {
      let myReg = /[\:]+/;
      chk_pass = isIpv6Str(
        value,
        fld_text,
        allow_what6,
        !is_dns_support || (is_dns_support && myReg.test(value)) ? false : true
      );

      if (
        chk_pass["error"] === true &&
        (!is_dns_support || (is_dns_support && myReg.test(value)))
      ) {
        return chk_pass;
      }
    }
  }

  if (is_dns_support && (vfy_addr === false || chk_pass["error"] === true)) {
    if (!value.length) {
      chk_pass = { error: false, msg: "" }; // Allow empty string for the domain name input field
    } else {
      chk_pass = { error: false, msg: "" };
      for (
        var subdomains = value.split("."), i = 0;
        i < subdomains.length;
        i++
      ) {
        if (subdomains[i].length < 1) {
          if (i + 1 !== subdomains.length || !allow_fqdn) {
            chk_pass = {
              error: true,
              msg: `The subdomain length of ${fld_text} is restricted to 1 - 63`
            };
          } //FQDN ends with last dot
          break;
        }
        if (subdomains[i].length > 63) {
          chk_pass = {
            error: true,
            msg: `The subdomain length of ${fld_text} is restricted to 1 - 63`
          };
          break;
        }
        if (!_isValidHostname(subdomains[i])) {
          chk_pass = {
            error: true,
            msg: `The format of ${fld_text} is invalid.<br><br>
            A valid name consist of a sequence of domain labels separated by "." ,each domain label starting and ending with an alphanumeric character and possibly also containing "-" characters.<br>
            The length of a domain label must be 63 characters or less.`
          };
          break;
        }
      }
    }
  }

  return chk_pass;
}

// isIpStr()
// Function that checks that input value is a valid IP string (xxx.xxx.xxx.xxx).
// @fld_id:     is the name of the field. It's value is used as input in the
//              check. It's also used to set focus if an error occurs.
// @is_mask:    Set to true if the input must be a mask, i.e. all-ones on the left and all-zeros on the right of the boundary.
// @start_text: In case of an error, focus is set to @fld_id and one of the following strings is shown:
//              "The value of " + start_text + " is not a valid IP address.\nValid IP strings have the format 'xxx.xxx.xxx.xxx' where 'xxx' is a number between 0 and 255."
//              "The value of " + start_text + " is not a valid IP mask.\nValid IP masks have the format 'xxx.xxx.xxx.xxx' where 'xxx' is a number between 0 and 255."
// @allow_what. Used only if @is_mask is false. 'Enumeration' of which IP addresses (x.y.z.w) are allowed.
//      @allow_what    || Allow x > 223 | Allow x == 127 | Allow 0.x.x.x | Allow 0.0.0.0 | Allow unicast
//      ---------------||---------------|----------------|---------------|---------------|---------------
//      Undefined or 0 ||      No       |      No        |      No       |      Yes      |     Yes
//                   1 ||      No       |      No        |      No       |      No       |     Yes
//                   2 ||      Yes      |      Yes       |      Yes      |      Yes      |     Yes
//                   3 ||      Yes      |      No        |      No       |      No       |     Yes
//                   4 ||      Yes      |      Yes       |      Yes      |      No       |     Yes
export function isIpStr(value, is_mask, start_text, allow_what) {
  if (!allow_what || allow_what < 0) {
    // In case allow_what is undefined
    allow_what = 0;
  } else if (allow_what > 4) {
    allow_what = 2;
  }

  var isok = _isValidIpStr(value, is_mask, allow_what);
  if (!isok) {
    if (is_mask) {
      return {
        error: true,
        msg: `The value of ${start_text} is not a valid IP mask.<br>
          A valid IP mask is a dotted decimal string ('x.y.z.w'), where<br>
          1. x, y, z, and w are decimal numbers between 0 and 255<br>
          2. When converted to a 32-bit binary string and read from left to right all bits following the first zero must also be zero.`
      };
    } else {
      if (allow_what === 0 || allow_what === 1) {
        return {
          error: true,
          msg:
            "The value of " +
            start_text +
            " must be a valid IP address in dotted decimal notation ('x.y.z.w').<br />" +
            "The following restrictions apply:<br />" +
            "  1) x, y, z, and w must be decimal numbers between 0 and 255,<br />" +
            "  2) x must not be 0" +
            (allow_what === 0 ? " unless also y, z, and w are 0" : "") +
            ",<br />" +
            "  3) x must not be 127, and<br />" +
            "  4) x must not be greater than 223."
        };
      } else if (allow_what === 3) {
        return {
          error: true,
          msg: `The value of ${start_text} must be a valid IP address in dotted decimal notation ('x.y.z.w').<br>
          The following restrictions apply:<br>
          <br>
          1. x must be a decimal number between <code>224</code> and 239<br>
          2. y, z, and w must be decimal numbers between 0 and 255`
        };
      } else {
        return {
          error: true,
          msg:
            "The value of " +
            start_text +
            " must be a valid IP address in dotted decimal notation ('x.y.z.w'),<br />" +
            "where x, y, z, and w are decimal numbers between 0 and 255."
        };
      }
    }
  } else {
    // not really understand here!!!
    // Get rid of insignificant preceding zeros - if any.
    // Well, in fact, if a string is preceded by a zero, then it's perceived as an octal number (e.g. parseInt("010") == 8)!!
    value =
      parseInt(RegExp.$1) +
      "." +
      parseInt(RegExp.$2) +
      "." +
      parseInt(RegExp.$3) +
      "." +
      parseInt(RegExp.$4);
  }

  return { error: false, msg: "" };
}

function _isIpStrFormat(val) {
  var myReg = /^((?:0x)[0-9A-Fa-f]{1,2}|[0][0-7]{0,3}|[1-9]\d{0,2})\.((?:0x)[0-9A-Fa-f]{1,2}|[0][0-7]{0,3}|[1-9]\d{0,2})\.((?:0x)[0-9A-Fa-f]{1,2}|[0][0-7]{0,3}|[1-9]\d{0,2})\.((?:0x)[0-9A-Fa-f]{1,2}|[0][0-7]{0,3}|[1-9]\d{0,2})$/;

  return val.length && myReg.test(val);
}

// The main validation parts
function _isValidIpStr(val, is_mask, allow_what) {
  var error = false;

  if (val.length === 0) {
    // User-input error
    error = true;
  } else {
    var myReg;
    var show_error = false;
    myReg = /^((?:0x)[0-9A-Fa-f]{1,2}|[0][0-7]{0,3}|[1-9]\d{0,2})\.((?:0x)[0-9A-Fa-f]{1,2}|[0][0-7]{0,3}|[1-9]\d{0,2})\.((?:0x)[0-9A-Fa-f]{1,2}|[0][0-7]{0,3}|[1-9]\d{0,2})\.((?:0x)[0-9A-Fa-f]{1,2}|[0][0-7]{0,3}|[1-9]\d{0,2})$/;
    if (myReg.test(val)) {
      var ip1, ip2, ip3, ip4;
      if (RegExp.$1[0] == "0" && RegExp.$1[1] != "x") {
        ip1 = parseInt(RegExp.$1, 8);
      } else {
        ip1 = parseInt(RegExp.$1);
      }
      if (RegExp.$2[0] == "0" && RegExp.$2[1] != "x") {
        ip2 = parseInt(RegExp.$2, 8);
      } else {
        ip2 = parseInt(RegExp.$2);
      }
      if (RegExp.$3[0] == "0" && RegExp.$3[1] != "x") {
        ip3 = parseInt(RegExp.$3, 8);
      } else {
        ip3 = parseInt(RegExp.$3);
      }
      if (RegExp.$4[0] == "0" && RegExp.$4[1] != "x") {
        ip4 = parseInt(RegExp.$4, 8);
      } else {
        ip4 = parseInt(RegExp.$4);
      }
      if (ip1 > 255 || ip2 > 255 || ip3 > 255 || ip4 > 255) {
        // At least one of ip1, ip2, ip3, or ip4 is not in interval 0-255.
        error = true;
      } else if (is_mask) {
        // Check that the mask is contiguous
        var ip_as_int = (ip1 << 24) | (ip2 << 16) | (ip3 << 8) | ip4;
        var zero_found = false,
          i;
        for (i = 31; i >= 0; i--) {
          if ((ip_as_int & (1 << i)) == 0) {
            // Cleared bit was found.
            zero_found = 1;
          } else if (zero_found) {
            // Set bit was found and cleared bit was previously found => Error
            error = true;
            break;
          }
        }
      } else {
        if (allow_what == 0 || allow_what == 1) {
          // Disallow x > 223, x == 127, and 0.x.x.x
          if (ip1 == 127 || ip1 > 223) {
            // ip1 indicates loopback (127), a multicast address (224-239), or an experimental address (240-255).
            error = true;
          } else if (ip1 == 0 && (ip2 != 0 || ip3 != 0 || ip4 != 0)) {
            // Class 0 is not allowed
            error = true;
          } else if (
            allow_what == 1 &&
            ip1 == 0 &&
            ip2 == 0 &&
            ip3 == 0 &&
            ip4 == 0
          ) {
            // 0.0.0.0 is not allowed
            error = true;
          }
        } else if (allow_what == 3) {
          // Disallow x < 223, x == 127, and 0.x.x.x
          if (ip1 == 127 || ip1 < 224 || ip1 > 239) {
            // ip1 indicates loopback (127), or an experimental address (240-255).
            error = true;
          } else if (ip1 == 0 && (ip2 != 0 || ip3 != 0 || ip4 != 0)) {
            // Class 0 is not allowed
            error = true;
          } else if (ip1 == 0 && ip2 == 0 && ip3 == 0 && ip4 == 0) {
            // 0.0.0.0 is not allowed
            error = true;
          }
        } else if (allow_what == 4) {
          if (ip1 == 0 && ip2 == 0 && ip3 == 0 && ip4 == 0) {
            // 0.0.0.0 is not allowed
            error = true;
          }
        }
      }
    } else {
      // Didn't match regexp
      error = true;
    }
  }

  return !error;
}

/*
    allow_what:
    0->Allow any valid address
    1->unicast address only
    2->multicast address only
    3->unicast address including unspecified
    4->unicast address except link-local
    5->unicast IPv4-Mapped
    6->unicast address except loopback

    Follow RFC-4291 & RFC-5952
*/
function isIpv6Str(value, caller_text, allow_what) {
  var idx,
    myReg,
    inputTextCheck,
    inputTextCheck2,
    inputText = value.split("::");
  if (inputText[0].charAt(0) === ":") {
    return {
      error: true,
      msg: `The input value ${caller_text} (${value}) is not a valid IPv6 address.`
    };
  }

  if (inputText.length > 2) {
    return {
      error: true,
      msg: `The value of ${caller_text}(${value}) must be a valid IPv6 address.<br>
      The symbol '::' can appear only once.`
    };
  } else if (inputText.length === 1) {
    if (isIpv6AddrZero(value)) {
      return {
        error: true,
        msg: `The input value ${caller_text} (${value}) is not a valid IPv6 address`
      };
    }

    inputTextCheck = inputText[0].split(":");

    /* x:x:x:x:x:x:x:x */
    // 第一個x等於四個符號時進入此if statement
    if (inputTextCheck[0].length === 4) {
      if (
        inputTextCheck[0].charAt(0).toLowerCase() === "f" &&
        inputTextCheck[0].charAt(1).toLowerCase() === "f"
      ) {
        // 已驗證: ffxx:xxx:xxx allow_what_6 !== 0 or 2
        if (allow_what !== 0 && allow_what !== 2) {
          return {
            error: true,
            msg: `Using IPv6 multicast address is not allowed here.`
          };
        }
      } else {
        if (allow_what === 2) {
          // 已驗證
          return {
            error: true,
            msg: `Using IPv6 unicast address is not allowed here.`
          };
        }

        // console.log(isIpv6AddrUnspecified(value));
        if (isIpv6AddrUnspecified(value)) {
          // 尚未驗證 -> isIpv6AddrUnspecified should be true
          if (allow_what === 0 || allow_what === 3) {
            return { error: false, msg: "" };
          } else {
            return {
              error: true,
              msg: `The input vlaue ${caller_text} (${value}) is not a valid IPv6 address.<br>
              The Unspecified Address must never be assigned to any node.`
            };
          }
        }

        if (
          allow_what === 4 &&
          inputTextCheck[0].charAt(0).toLowerCase() === "f" &&
          inputTextCheck[0].charAt(1).toLowerCase() === "e" &&
          inputTextCheck[0].charAt(2) === "8" &&
          inputTextCheck[0].charAt(3) === "0"
        ) {
          // 已驗證
          return {
            error: true,
            msg: "Using IPv6 link-local address is not allowed here."
          };
        }

        if (allow_what === 5) {
          return {
            error: true,
            msg: `The input value ${caller_text} (${value}) is not a valid IPv4-Mapped address.`
          };
        }
      }
    } else {
      // 第一個區間不等於4個字元時進入這
      if (allow_what === 2) {
        // 已驗證
        return {
          error: true,
          msg: "Using IPv6 unicast address is not allowed here."
        };
      }

      if (isIpv6AddrUnspecified(value)) {
        // 尚未驗證
        if (allow_what === 0 || allow_what === 3) {
          return { error: false, msg: "" };
        } else {
          return {
            error: true,
            msg: `The input value ${caller_text} (${value}}) is not a valiid IPv6 address.<br>
            The Unspecified Address must never be assigned to any node.`
          };
        }
      }

      if (allow_what === 5) {
        // 已驗證
        return {
          error: true,
          msg: `The input value ${caller_text} (${value}}) is not a valid IPv4-Mapped address.`
        };
      }
    }

    myReg = /^([\da-fA-F]{1,4}\:){7}[\da-fA-F]{1,4}$/;

    if (myReg.test(value)) {
      // 尚未驗證
      return { error: false, msg: "" };
    }
  } else if (inputText.length === 2) {
    myReg = /[^\da-fA-F\:\.]/;
    if (myReg.test(value)) {
      return {
        error: true,
        msg: `The value of ${caller_text} (${value})<br>
         Must be a valid IPv6 address in 128-bit records represented as Eight fields of up to four hexadecimal digits with a colon (:) separating each field.`
      };
    }

    /* Pick up loopback address ::1 */
    if (inputText[0].length === 0) {
      //::xx
      inputTextCheck = inputText[1].split(":");

      if (inputTextCheck.length === 1 && inputTextCheck2.length === 1) {
        // allow_waht: 6 format: ::1 will reach here
        if (allow_what === 6) {
          if (parseInt(inputTextCheck, 16) === 1) {
            // 已驗證
            return {
              error: true,
              msg: `Using the input value in ${caller_text} (${value}) as IPv6 loopback address is not allowed here.`
            };
          }
        }
      }
    }

    if (isIpv6AddrZero(value)) {
      // 尚未驗證
      return {
        error: true,
        msg: `The input value ${caller_text} (${value}) is not a valid IPv6 address.`
      };
    }

    inputTextCheck = inputText[0].split(":");

    /* x::x */
    if (inputTextCheck[0].length === 4) {
      if (
        inputTextCheck[0].charAt(0).toLowerCase() === "f" &&
        inputTextCheck[0].charAt(1).toLowerCase() === "f"
      ) {
        if (allow_what !== 0 && allow_what !== 2) {
          return {
            error: true,
            msg: `Using IPv6 multicast address is not allowed here.`
          };
        }
      } else {
        if (allow_what === 2) {
          return {
            error: true,
            msg: `Using IPv6 unicast address is not allowed here.`
          };
        }

        if (isIpv6AddrUnspecified(value)) {
          if (allow_what === 0 || allow_what === 3) {
            return { error: false, msg: "" };
          } else {
            return {
              error: true,
              msg: `The input value ${caller_text} (${value}) is not a valid IPv6 address.<br>
              The Unspecified Address must never be assigned to any node.`
            };
          }
        }

        if (allow_what === 4) {
          if (
            inputTextCheck[0].charAt(0).toLowerCase() === "f" &&
            inputTextCheck[0].charAt(1).toLowerCase() === "e" &&
            inputTextCheck[0].charAt(2) === "8" &&
            inputTextCheck[0].charAt(3) === "0"
          ) {
            return {
              error: true,
              msg: `Using IPv6 link-local address is not allowed here.`
            };
          }
        }

        if (allow_what === 5) {
          return {
            error: true,
            msg: `The input value ${caller_text} (${value}) is not a valid IPv4-Mapped address.`
          };
        }
      }

      myReg = /[\.]/;
      if (myReg.test(value)) {
        if (allow_what === 2) {
          return {
            error: true,
            msg: `The input value ${caller_text} (${value}) is an invalid IPv6 multicast address.`
          };
        }

        return {
          error: true,
          msg: `The input value ${caller_text} (${value}) is not a valid IPv6 address.`
        };
      }
    } else {
      if (allow_what === 2) {
        return {
          error: true,
          msg: `Using IPv6 unicast address is not allowed here.`
        };
      }

      if (isIpv6AddrUnspecified(value)) {
        if (allow_what === 0 || allow_what === 3) {
          return { error: false, msg: "" };
        } else {
          return {
            error: true,
            msg: `The input value ${caller_text} (${value}) is not a valid IPv6 address.<br>
            The Unspecified Address must never be assigned to any node.`
          };
        }
      }

      myReg = /[\.]/;
      if (myReg.test(value)) {
        /* Check for IPv4 Compatible/Mapped Address ONLY */
        inputTextCheck = inputText[1].split(".");
        myReg = /[^\dfF\:\.]/;
        if (!myReg.test(value) && inputTextCheck.length === 4) {
          var addr4TokenCnt =
            inputText[0].split(":").length + inputText[1].split(":").length;
          inputTextCheck = inputText[0].split(":");
          if (inputTextCheck[0] === "") {
            addr4TokenCnt--;
          }
          inputTextCheck = inputText[1].split(":");
          if (inputTextCheck[0] === "") {
            addr4TokenCnt = 0; /* Use 0 to present invalid */
          }

          /*
            IPv6 ZERO-Compression consumes at least 1 token + 1 for IPv4-Format
            Thus
            #TokenMax.=6
            #TokenMin.=1
          */
          if (addr4TokenCnt > 0 && addr4TokenCnt < 7) {
            var pass4 = 1;

            /* Left side of :: should be all zero */
            inputTextCheck = inputText[0].split(":");
            if (inputTextCheck[0] !== "") {
              for (idx = 0; idx < inputTextCheck.length; idx++) {
                if (
                  inputTextCheck[idx].length < 1 ||
                  inputTextCheck[idx].length > 4
                ) {
                  pass4 = 0;
                  break;
                }

                if (parseInt(inputTextCheck[idx], 16)) {
                  pass4 = 0;
                  break;
                }
              }
            }

            /* Right side of :: should be 0:a.b.c.d OR 0:ffff:a.b.c.d */
            if (pass4) {
              var prev4,
                mapFound = 0;
              inputTextCheck = inputText[1].split(":");

              for (idx = 0; idx < inputTextCheck.length; idx++) {
                if (_isValidIpStr(inputTextCheck[idx], false, 1)) {
                  if (idx + 1 !== inputTextCheck.length) {
                    /* The last token is not IPv4 */
                    pass4 = 0;
                  }

                  break;
                } else {
                  if (mapFound) {
                    /* FFFF should be adjacent to IPv4 */
                    pass4 = 0;
                    break;
                  }
                }

                if (
                  inputTextCheck[idx].length < 1 ||
                  inputTextCheck[idx].length > 4
                ) {
                  pass4 = 0;
                  break;
                }

                prev4 = parseInt(inputTextCheck[idx], 16);
                if (prev4) {
                  if (prev4 !== 0xffff) {
                    pass4 = 0;
                    break;
                  } else {
                    mapFound = 1;
                  }
                }
              }
            }

            if (pass4) {
              return { error: false, msg: "" };
            }
          }
        }

        return {
          error: true,
          msg: `The input value ${caller_text} (${value}) is not a valid IPv4-Mapped address.`
        };
      } else {
        if (allow_what === 5) {
          return {
            error: true,
            msg: `The input value ${caller_text} (${value}) is not a valid IPv4-Mapped address.`
          };
        }
      }
    }

    var addrTokenCnt =
      inputText[0].split(":").length + inputText[1].split(":").length;
    inputTextCheck = inputText[0].split(":");
    if (inputTextCheck[0] === "") {
      addrTokenCnt--;
    }
    inputTextCheck = inputText[1].split(":");
    if (inputTextCheck[0] === "") {
      addrTokenCnt--;
    }

    /*
            IPv6 ZERO-Compression consumes at least 1 token
            Thus
            #TokenMax.=7
            #TokenMin.=1
        */
    if (addrTokenCnt > 0 && addrTokenCnt < 8) {
      var pass6 = 1;

      myReg = /[\da-fA-F]{1,4}/;

      inputTextCheck = inputText[0].split(":");
      if (inputTextCheck[0] !== "") {
        for (idx = 0; idx < inputTextCheck.length; idx++) {
          if (
            inputTextCheck[idx].length < 1 ||
            inputTextCheck[idx].length > 4
          ) {
            pass6 = 0;
            break;
          }

          if (!myReg.test(inputTextCheck[idx].value)) {
            pass6 = 0;
            break;
          }
        }
      }

      inputTextCheck = inputText[1].split(":");
      if (pass6 && inputTextCheck[0] !== "") {
        for (idx = 0; idx < inputTextCheck.length; idx++) {
          if (
            inputTextCheck[idx].length < 1 ||
            inputTextCheck[idx].length > 4
          ) {
            pass6 = 0;
            break;
          }

          if (!myReg.test(inputTextCheck[idx].value)) {
            pass6 = 0;
            break;
          }
        }
      }

      if (pass6) {
        return { error: false, msg: "" };
      }
    }
  } else {
    return {
      error: true,
      msg: `The input value ${caller_text} (${value}) is not a valid IPv6 address.`
    };
  }

  return {
    error: true,
    msg: `The value of ${caller_text} (${value})<br>Must be a valid IPv6 address in 128-bit records represented as Eight fields of up to four hexadecimal digits with a colon (:) separating each field.`
  };
}

/* Return true IFF Valid && Unspecified */
function isIpv6AddrZero(value) {
  var idx,
    myReg,
    inputTextCheck,
    zero_cnt = 0,
    inputText = value.split("::");
  myReg = /[^0\:]/;

  if (inputText.length > 2 || inputText.length === 0) {
    return false;
  } else if (inputText.length === 1) {
    /* x:x:x:x:x:x:x:x */
    if (myReg.test(value)) {
      return false;
    } else {
      inputTextCheck = inputText[0].split(":");
      if (inputTextCheck.length !== 8) {
        return false;
      } else {
        for (idx = 0; idx < inputTextCheck.length; idx++) {
          if (parseInt(inputTextCheck[idx]) === 0) {
            zero_cnt++;
          }
        }
        if (zero_cnt == 8) {
          return true;
        }
      }
    }
  } else if (inputText.length === 2) {
    if (myReg.test(value)) {
      return false;
    }

    if (value === "::") {
      return true;
    }

    inputTextCheck = inputText[0].split(":");
    for (idx = 0; idx < inputTextCheck.length; idx++) {
      if (parseInt(inputTextCheck[idx]) !== 0) {
        return false;
      }
    }
    inputTextCheck = inputText[1].split(":");
    for (idx = 0; idx < inputTextCheck.length; idx++) {
      if (parseInt(inputTextCheck[idx]) !== 0) {
        return false;
      }
    }

    return true;
  }

  return false;
}

/* Return true IFF Valid && Unspecified */
function isIpv6AddrUnspecified(value) {
  var idx,
    myReg,
    inputTextCheck,
    pass = 1,
    inputText = value.split("::");

  if (inputText.length > 2) {
    pass = 0;
  } else if (inputText.length === 1) {
    /* x:x:x:x:x:x:x:x */
    myReg = /[^0\:]/;
    if (myReg.test(value)) {
      pass = 0;
    } else {
      inputTextCheck = inputText[0].split(":");
      if (inputTextCheck.length !== 8) {
        pass = 0;
      } else {
        for (idx = 0; idx < inputTextCheck.length; idx++) {
          if (
            inputTextCheck[idx].length < 1 ||
            inputTextCheck[idx].length > 4
          ) {
            pass = 0;
            break;
          }
        }
      }
    }
  } else if (inputText.length === 2) {
    myReg = /[^0\:]/;
    if (myReg.test(value)) {
      return false;
    }

    if (value === "::") {
      return true;
    }

    /* x::x */
    var addrTokenCnt =
      inputText[0].split(":").length + inputText[1].split(":").length;
    inputTextCheck = inputText[0].split(":");
    if (inputTextCheck[0] === "") {
      addrTokenCnt--;
    }
    inputTextCheck = inputText[1].split(":");
    if (inputTextCheck[0] === "") {
      addrTokenCnt--;
    }

    /*
            IPv6 ZERO-Compression consumes at least 1 token
            Thus
            #TokenMax.=7
            #TokenMin.=1
        */
    if (addrTokenCnt > 0 && addrTokenCnt < 8) {
      inputTextCheck = inputText[0].split(":");
      if (inputTextCheck[0] !== "") {
        for (idx = 0; idx < inputTextCheck.length; idx++) {
          if (
            inputTextCheck[idx].length < 1 ||
            inputTextCheck[idx].length > 4
          ) {
            pass = 0;
            break;
          }
        }
      }

      inputTextCheck = inputText[1].split(":");
      if (pass && inputTextCheck[0] !== "") {
        for (idx = 0; idx < inputTextCheck.length; idx++) {
          if (
            inputTextCheck[idx].length < 1 ||
            inputTextCheck[idx].length > 4
          ) {
            pass = 0;
            break;
          }
        }
      }
    } else {
      pass = 0;
    }
  } else {
    pass = 0;
  }

  if (pass) {
    return true;
  } else {
    return false;
  }
}
