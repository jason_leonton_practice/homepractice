import React from "react";
import Fade from "react-reveal/Fade";

import { LoadingSegment } from "../components/loading_segment";

const WithJsonData = (WrappedComponent, url, body) => {
  return class WithJsonData extends React.Component {
    constructor(props) {
      super(props);
      this.state = { data: null, noRead: false, errorMsg: "" };
    }

    handleAutoRefresh = isAutoRefresh => {
      if (isAutoRefresh) {
        this.refreshID = setInterval(() => this.httpRequest(url, body), 3000);
      } else if (!isAutoRefresh) {
        clearInterval(this.refreshID);
      }
    };

    httpRequest = (url, body) => {
      let xmlHttp = new XMLHttpRequest();
      console.log(url);
      xmlHttp.open("POST", url);
      xmlHttp.setRequestHeader("Content-Type", "application/json");
      xmlHttp.send(JSON.stringify(body));
      xmlHttp.onreadystatechange = () => {
        if (xmlHttp.readyState === 4) {
          if (xmlHttp.status && xmlHttp.status === 200) {
            const data = JSON.parse(xmlHttp.response);

            if (data.error) {
              if (data.error.message) {
                if (data.error.message === "Access denied" && body.method.match(/.get$/)) {
                  this.setState({ noRead: true });
                } else {
                  this.setState({ errorMsg: data.error.message });
                }
              } else {
                console.log("JSON RPC Error. (" + data.error.code + ")");
              }
            } else {
              // success
              this.setState({ data });
            }
          }
        }
      };
    };

    shouldComponentUpdate(prevProps, prevState) {
      return (
        JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
        JSON.stringify(this.state) !== JSON.stringify(prevState)
      );
    }

    componentWillUnmount() {
      clearInterval(this.refreshID);
    }

    componentDidMount() {
      this.httpRequest(url, body);
    }

    render() {
      const { data, noRead, readOnly } = this.state;

      return noRead ? (
        <div>Permmission Deny</div>
      ) : data === null ? (
        <LoadingSegment />
      ) : (
        <Fade bottom distance="40px" duration={600}>
          <WrappedComponent {...this.props} data={data} autoRefresh={this.handleAutoRefresh} />
        </Fade>
      );
    }
  };
};

export default WithJsonData;
