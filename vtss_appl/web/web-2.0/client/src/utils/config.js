export const configPortMin = 1;
export const configNormalPortMax = 20;
export const configDomainNameLengthMax = 253;
export const configHostNameLengthMax = 45;
export const configIPDNSSupport = 1;
export const configPingLenMin = 2;
export const configPingLenMax = 1452;
export const configPingPdataMin = 0;
export const configPingPdataMax = 255;
export const configPingCntMin = 1;
export const configPingCntMax = 60;
export const configPingTtlMin = 0;
export const configPingTtlMax = 255;
export const configVidMin = 1;
export const configVidMax = 4095;
export const configDefaultUserNameNullStr = 0;
export const configSwitchName = "EG5-2004-SFP";
export const configSwitchSv = "V6.0.1";
export const configSwitchDescription = "20 port DIN-Rail Managed Ethernet Switch";
export const configSwitchSn = "0000000000000000000000000000000";
export const configSwitchMac = "00-11-22-33-12-04";

export function confighasPost() {
  return true;
}
export function configHasZtp() {
  return true;
}
