export const genOptions = ({ text, value }) => {
  let options = [];

  for (let i = 0; i < text.length; i++) {
    options.push({ key: value[i], text: text[i], value: value[i] });
  }

  return options;
};

export const genArrayInt = (start, end) => {
  var a = new Array(end - start + 1);
  var i;
  for (i = 0; i < a.length; i++) {
    a[i] = i + start;
  }
  return a;
};

export const genArrayStr = (start, end, prefix) => {
  var a = new Array(end - start + 1);
  var i;
  for (i = 0; i < a.length; i++) {
    var val = String(i + start);
    a[i] = prefix ? prefix + val : val;
  }
  return a;
};
