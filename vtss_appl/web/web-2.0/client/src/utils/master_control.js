import React from "react";
import { Field, FormSpy } from "react-final-form";
import { OnChange } from "react-final-form-listeners";

import { genOptions } from "../utils/dyform";
import { SelectInput } from "../components/input-field";

const WhenFieldChanges = ({ field, becomes, set, to }) => {
  return (
    <Field name={set} subscription={{}}>
      {(
        // No subscription. We only use Field to get to the change function
        { input: { onChange } }
      ) => (
        <FormSpy subscription={{}}>
          {({ form }) => (
            <OnChange name={field}>
              {value => {
                if (typeof becomes === "number" && typeof to === "number" && value === becomes) {
                  onChange(to);
                } else if (typeof becomes === "undefined" && value.trim() !== "") {
                  onChange(value);
                }
              }}
            </OnChange>
          )}
        </FormSpy>
      )}
    </Field>
  );
};

export class MCDropdown {
  constructor(name, ids, listenerOptions, value, text, features, countFromZero = false) {
    this.name = name;
    this.ids = ids;
    this.listenerOptions = listenerOptions;
    this.optionValue = value;
    this.optionText = text;
    this.features = features;
    this.countFromZero = countFromZero;
  }

  genComponents() {
    return (
      <Field
        name={`master_control_${this.name}`}
        component={attr => SelectInput(attr, this.genDropdownOptions(), this.features)}
      />
    );
  }

  genDropdownOptions() {
    const masterControlOptions = genOptions({
      value: this.optionValue,
      text: this.optionText
    });

    return masterControlOptions;
  }

  construct() {
    let components = [];
    this.ids.forEach(id => {
      if (id !== "") {
        const genComponentsWithOptions = this.listenerOptions.map((value, index) => (
          <WhenFieldChanges
            key={`${this.name}_${id}_${index}`}
            field={`master_control_${this.name}`}
            becomes={value}
            set={`${this.name}_${id}`}
            to={value}
          />
        ));
        components.push(genComponentsWithOptions);
      }
    });

    return components;
  }
}

export class MCTextInput {
  constructor(name, ids) {
    this.name = name;
    this.ids = ids;
  }

  construct() {
    let components = [];

    this.ids.forEach(id => {
      if (id !== "") {
        components.push(
          <WhenFieldChanges
            key={`${this.name}_${id}`}
            field={`master_control_${this.name}`}
            set={`${this.name}_${id}`}
          />
        );
      }
    });

    return components;
  }
}
