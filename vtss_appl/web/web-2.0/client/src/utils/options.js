export const pageLimitOptions = [
  { key: 1, text: "1", value: 1 },
  { key: 5, text: "5", value: 5 },
  { key: 10, text: "10", value: 10 },
  { key: 15, text: "15", value: 15 },
  { key: 20, text: "20", value: 20 },
  { key: 25, text: "25", value: 25 },
  { key: 30, text: "30", value: 30 },
  { key: 35, text: "35", value: 35 },
  { key: 40, text: "40", value: 40 },
  { key: 45, text: "45", value: 45 },
  { key: 50, text: "50", value: 50 }
];

export const protocolOptions = [
  { key: "all", text: "All", value: "all" },
  { key: "protoDhcp", text: "DHCP", value: "protoDhcp" },
  { key: "protoConneted", text: "Connected", value: "protoConneted" },
  { key: "protoStatic", text: "Static", value: "protoStatic" },
  { key: "protoOspf", text: "OSPF", value: "protoOspf" }
];
