import { lazy } from "react";

import CustomSidebar from "../page/custom-sidebar";
import Wizard from "../page/wizard/wizard";

const Dashboard = lazy(() => import("../page/dashboard"));

// Monitor
const Sys = lazy(() => import("../page/monitor/sys"));
const PortPowerSavingsStatus = lazy(() => import("../page/monitor/port_power_savings_status"));
const PerfCpuload = lazy(() => import("../page/monitor/perf_cpuload"));
const SysLedStatus = lazy(() => import("../page/monitor/sys_led_status"));
const StatPorts = lazy(() => import("../page/monitor/stat_ports"));
const StatDetailed = lazy(() => import("../page/monitor/stat_detailed"));
const DhcpServerStatDeclined = lazy(() => import("../page/monitor/dhcp_server_stat_declined"));
const DhcpRelayStatistics = lazy(() => import("../page/monitor/dhcp_relay_statistics"));
const DhcpHelperStatistics = lazy(() => import("../page/monitor/dhcp_helper_statistics"));
const IpRoutingInfoBaseStatus = lazy(() => import("../page/monitor/ip_routing_info_base_status"));
const DynaMac = lazy(() => import("../page/monitor/dyna_mac"));
const VlanMembershipStat = lazy(() => import("../page/monitor/vlan_membership_stat"));
const VlanPortStat = lazy(() => import("../page/monitor/vlan_port_stat"));
const MstpStatusPorts = lazy(() => import("../page/monitor/mstp_status_ports"));
const MstpStatistics = lazy(() => import("../page/monitor/mstp_statistics"));
const MstpStatusBridges = lazy(() => import("../page/monitor/mstp_status_bridges"));
const MstpStatusBridge = lazy(() => import("../page/monitor/mstp_status_bridge"));
const MvrpStat = lazy(() => import("../page/monitor/mvrp_stat"));
const SflowStatus = lazy(() => import("../page/monitor/sflow_status"));
const DDMIoverview = lazy(() => import("../page/monitor/ddmi_overview"));
const DdmiDetailed = lazy(() => import("../page/monitor/ddmi_detailed"));
const AggregationStatus = lazy(() => import("../page/monitor/aggregation_status"));
const LacpSysStatus = lazy(() => import("../page/monitor/lacp_sys_status"));
const LacpInternalStatus = lazy(() => import("../page/monitor/lacp_internal_status"));
const LacpNeighborStatus = lazy(() => import("../page/monitor/lacp_neighbor_status"));
const LacpStatistics = lazy(() => import("../page/monitor/lacp_statistics"));
const LoopStatus = lazy(() => import("../page/monitor/loop_protection"));
const IpStatus = lazy(() => import("../page/monitor/ip_status"));
const PoeStatus = lazy(() => import("../page/monitor/poe_status"));
const IpmcIgmpsStatus = lazy(() => import("../page/monitor/ipmc_igmp_status"));
const AccessMgmtStatistics = lazy(() => import("../page/monitor/access_mgmt_statistics"));

// Configuration
const SysinfoConfig = lazy(() => import("../page/configuration/sysinfo_config"));
const Users = lazy(() => import("../page/configuration/users"));
const UserConfig = lazy(() => import("../page/configuration/user_config/user_config"));
const PrivLvl = lazy(() => import("../page/configuration/priv_lvl"));
const SshConfig = lazy(() => import("../page/configuration/ssh_config"));
const DDMI = lazy(() => import("../page/configuration/ddmi"));
const LACPPortConfig = lazy(() => import("../page/configuration/lacp_port_config"));
const DhcpRelay = lazy(() => import("../page/configuration/dhcp_relay"));
const AggrCommon = lazy(() => import("../page/configuration/aggr_common"));
const AggrGroups = lazy(() => import("../page/configuration/aggr_groups"));
const ModbusConfig = lazy(() => import("../page/configuration/modbus_config"));
const DhcpSnooping = lazy(() => import("../page/configuration/dhcp_snooping"));
const MstpMstiConfig = lazy(() => import("../page/configuration/mstp_msti_config"));
const MstpMapConfig = lazy(() => import("../page/configuration/mstp_msti_map_config"));

// Diagnostics
const Ping4 = lazy(() => import("../page/diagnostics/ping4"));
const Ping4Result = lazy(() => import("../page/diagnostics/ping4_result"));
const Traceroute4 = lazy(() => import("../page/diagnostics/traceroute4"));
const Traceroute4Result = lazy(() => import("../page/diagnostics/traceroute4_result"));
const Traceroute6 = lazy(() => import("../page/diagnostics/traceroute6"));
const Traceroute6Result = lazy(() => import("../page/diagnostics/traceroute6_result"));
const Ping6 = lazy(() => import("../page/diagnostics/ping6"));
const Ping6Result = lazy(() => import("../page/diagnostics/ping6_result"));
const Veriphy = lazy(() => import("../page/diagnostics/veriphy"));

// Maintenance
const Upload = lazy(() => import("../page/maintenance/upload/upload"));
const SwSelect = lazy(() => import("../page/maintenance/sw_select"));
const Wreset = lazy(() => import("../page/maintenance/wreset/wreset"));
const IcfgConfDownload = lazy(() => import("../page/maintenance/icfg_conf_download"));
const IcfgConfDelete = lazy(() => import("../page/maintenance/icfg_conf_delete"));
const IcfgConfSave = lazy(() => import("../page/maintenance/icfg_conf_save"));
const IcfgConfActivate = lazy(() => import("../page/maintenance/icfg_conf_activate"));
const IcfgConfRunning = lazy(() => import("../page/maintenance/icfg_conf_running"));
const IcfgConfUpload = lazy(() => import("../page/maintenance/icfg_conf_upload/icfg_conf_upload"));
const Factory = lazy(() => import("../page/maintenance/factory"));

const menuData = [
  {
    name: "Dashboard",
    title: "Dashboad",
    id: "dashboard",
    level: "level-1",
    component: Dashboard,
    help: "help_dashboard",
    url: "/"
  },
  {
    name: "Custom Sidebar",
    id: "custom_sidebar",
    level: "level-1",
    visible: false,
    component: CustomSidebar,
    url: "/custom-sidebar"
  },
  {
    name: "Quick Setup",
    id: "quick_setup",
    level: "level-1",
    visible: false,
    component: Wizard,
    url: "/quick-setup"
  },
  {
    name: "Configuration",
    id: "configuration",
    level: "level-1",
    children: [
      {
        name: "system",
        id: "configuration_system",
        level: "level-2",
        children: [
          {
            name: "Information",
            title: "System Information Configuration",
            id: "sysinfo_config",
            level: "level-3",
            component: SysinfoConfig,
            help: "help_sysinfo_config",
            url: "/configuration/system/information"
          }
        ]
      },
      {
        name: "DHCP",
        id: "configuration_dhcp",
        level: "level-2",
        children: [
          {
            name: "Snooping",
            title: "DHCP Snooping Configuration",
            id: "dhcp_snooping",
            level: "level-3",
            component: DhcpSnooping,
            url: "/configuration/dhcp/snooping"
          },
          {
            name: "Relay",
            title: "DHCP Relay Configuration",
            id: "dhcp_relay",
            level: "level-3",
            component: DhcpRelay,
            help: "help_dhcp_relay",
            url: "/configuration/dhcp/relay"
          }
        ]
      },
      {
        name: "Security",
        id: "configuration_security",
        level: "level-2",
        children: [
          {
            name: "Switch",
            id: "configuration_security_switch",
            level: "level-3",
            children: [
              {
                name: "Users",
                title: "Users Configuration",
                id: "users",
                level: "level-4",
                component: Users,
                help: "help_users",
                url: "/configuration/security/switch/users"
              },
              {
                name: "User Config",
                title: "Add / Edit User",
                id: "user_config",
                level: "level-4",
                visible: false,
                component: UserConfig,
                help: "help_user_config",
                url: "/configuration/security/switch/user_config"
              },
              {
                name: "Privilege Levels",
                title: "Privilege Level Configuration",
                id: "priv_lvl",
                level: "level-4",
                component: PrivLvl,
                help: "help_priv_lvl",
                url: "/configuration/security/switch/privilege_levels"
              },
              {
                name: "SSH",
                title: "SSH Configuration",
                id: "ssh_config",
                level: "level-4",
                component: SshConfig,
                help: "help_ssh_config",
                url: "/configuration/security/switch/ssh"
              }
            ]
          }
        ]
      },
      {
        name: "Aggregation",
        id: "configuration_aggregation",
        level: "level-2",
        children: [
          {
            name: "Common",
            title: "Common Aggregation Configuration",
            id: "aggr_common",
            level: "level-3",
            component: AggrCommon,
            help: "help_aggr_common",
            url: "/configuration/aggregation/common"
          },
          {
            name: "Groups",
            title: "Aggregation Group Configuration",
            id: "aggr_groups",
            level: "level-3",
            component: AggrGroups,
            help: "help_aggr_groups",
            url: "/configuration/aggregation/groups"
          },
          {
            name: "LACP",
            title: "LACP System & Port Configuration",
            id: "lacp_port_config",
            level: "level-3",
            component: LACPPortConfig,
            help: "help_lacp_port_config",
            url: "/configuration/aggregation/lacp"
          }
        ]
      },
      {
        name: "Spanning Tree",
        id: "configuration_spanning_tree",
        level: "level-2",
        children: [
          {
            name: "MSTI Mapping",
            title: "MSTI Identification & Mapping Configuration",
            id: "mstp_msti_map_config",
            level: "level-3",
            component: MstpMapConfig,
            url: "/configuration/spanning_tree/msti_mapping"
          },
          {
            name: "MSTI Priorities",
            title: "MSTI Priority Configuration",
            id: "mstp_msti_config",
            level: "level-3",
            component: MstpMstiConfig,
            url: "/configuration/spanning_tree/msti_priorities"
          }
        ]
      },
      {
        name: "DDMI",
        title: "DDMI Configuration",
        help: "help_ddmi_config",
        id: "ddmi_config",
        level: "level-2",
        component: DDMI,
        url: "/configuration/ddmi"
      },
      {
        name: "MODBUS TCP",
        title: "MODBUS TCP Configuration",
        id: "modbus_config",
        level: "level-2",
        component: ModbusConfig,
        help: "help_modbus_config",
        url: "/configuration/modbus_tcp"
      }
    ]
  },
  {
    name: "Monitor",
    id: "monitor",
    level: "level-1",
    children: [
      {
        name: "System",
        id: "monitor_system",
        level: "level-2",
        children: [
          {
            name: "Information",
            title: "System Information",
            id: "sys",
            level: "level-3",
            component: Sys,
            help: "help_sys",
            url: "/monitor/system/information"
          },
          {
            name: "LED Status",
            title: "System LED Status",
            id: "sus_led_status",
            level: "level-3",
            component: SysLedStatus,
            url: "/monitor/system/led_status"
          },
          {
            name: "CPU Load",
            title: "CPU load",
            id: "perf_cpuload",
            level: "level-3",
            component: PerfCpuload,
            help: "help_perf_cpuload",
            url: "/monitor/system/cpu_load"
          },
          {
            name: "IP Status",
            title: "IP Interfaces",
            id: "ip_status",
            level: "level-3",
            component: IpStatus,
            help: "help_ip_status",
            url: "/monitor/system/ip_status"
          },
          {
            name: "Routing Info. Base",
            title: "Routing Information Base",
            id: "ip_routing_info_base_status",
            level: "level-3",
            component: IpRoutingInfoBaseStatus,
            help: "help_ip_routing_info_base_status",
            url: "/monitor/system/routing_info_base"
          }
        ]
      },
      {
        name: "Green Ethernet",
        id: "monitor_green_ethernet",
        level: "level-2",
        children: [
          {
            name: "Port Power Savings",
            title: "Port Power Savings Status",
            id: "port_power_savings_status",
            level: "level-3",
            component: PortPowerSavingsStatus,
            help: "help_port_power_savings_status",
            url: "/monitor/green_ethernet/port_power_savings"
          }
        ]
      },
      {
        name: "Ports",
        id: "monitor_ports",
        level: "level-2",
        children: [
          {
            name: "Traffic Overview",
            title: "Port Statistics Overview",
            id: "stat_ports",
            level: "level-3",
            component: StatPorts,
            help: "help_stat_ports",
            url: "/monitor/ports/traffic_overview"
          },
          {
            name: "Detailed Statistics",
            title: "Detailed Port Statistics",
            id: "stat_detailed",
            level: "level-3",
            component: StatDetailed,
            help: "help_stat_detailed",
            url: "/monitor/ports/detailed_statistics"
          }
        ]
      },
      {
        name: "DHCP",
        id: "monitor_dhcp",
        level: "level-2",
        children: [
          {
            name: "Server",
            id: "monitor_dhcp_server",
            level: "level-3",
            children: [
              {
                name: "Declined IP",
                title: "DHCP Server Declined IP",
                id: "dhcp_server_stat_declined",
                level: "level-4",
                component: DhcpServerStatDeclined,
                help: "help_dhcp_server_stat_declined",
                url: "/monitor/dhcp/server/declined_ip"
              }
            ]
          },
          {
            name: "Relay Statistics",
            title: "DHCP Relay Statistics",
            id: "dhcp_relay_statistics",
            level: "level-3",
            component: DhcpRelayStatistics,
            help: "help_dhcp_relay_statistics",
            url: "/monitor/dhcp/relay_statistics"
          },
          {
            name: "Detailed Statistics",
            title: "DHCP Detailed Statistics",
            id: "dhcp_helper_statistics",
            level: "level-3",
            component: DhcpHelperStatistics,
            help: "help_dhcp_helper_statistics",
            url: "/monitor/dhcp/detailed_statistics"
          }
        ]
      },
      {
        name: "Security",
        id: "monitor_security",
        level: "level-2",
        children: [
          {
            name: "Access Management",
            title: "Access Management Statistics",
            id: "access_mgmt_statistics",
            level: "level-4",
            component: AccessMgmtStatistics,
            help: "help_access_mgmt_statistics",
            url: "/monitor/security/access_management"
          }
        ]
      },
      {
        name: "Aggregation",
        id: "monitor_aggregation",
        level: "level-2",
        children: [
          {
            name: "Status",
            title: "Aggregation Status",
            id: "aggregation_status",
            level: "level-3",
            component: AggregationStatus,
            help: "help_aggregation_status",
            url: "/monitor/aggregation/status"
          },
          {
            name: "LACP",
            id: "monitor_aggregation_lacp",
            level: "level-3",
            children: [
              {
                name: "System Status",
                title: "LACP System Status",
                id: "lacp_sys_status",
                level: "level-4",
                component: LacpSysStatus,
                help: "help_lacp_sysstatus",
                url: "/monitor/aggregation/lacp/system_status"
              },
              {
                name: "Internal Status",
                title: "LACP Internal Port Status",
                id: "lacp_internal_status",
                level: "level-4",
                component: LacpInternalStatus,
                help: "help_lacp_internal_status",
                url: "/monitor/aggregation/lacp/internal_status"
              },
              {
                name: "Neighbor Status",
                title: "LACP Neighbor Port Status",
                id: "lacp_neighbor_status",
                level: "level-4",
                component: LacpNeighborStatus,
                help: "help_lacp_neighbor_status",
                url: "/monitor/aggregation/lacp/neighbor_status"
              },
              {
                name: "Port Statistics",
                title: "LACP Statistics",
                id: "lacp_statistics",
                level: "level-4",
                component: LacpStatistics,
                help: "help_lacp_statistics",
                url: "/monitor/aggregation/lacp/port_statistics"
              }
            ]
          }
        ]
      },
      {
        name: "Loop Protection",
        title: "Loop Protection Status",
        id: "loop_status",
        level: "level-2",
        component: LoopStatus,
        help: "help_loop_status",
        url: "/monitor/loop_protection"
      },
      {
        name: "Spanning Tree",
        id: "configuration_spanning_tree",
        level: "level-2",
        children: [
          {
            name: "Bridge Status",
            id: "mstp_status_bridges",
            title: "STP Bridges",
            level: "level-3",
            component: MstpStatusBridges,
            help: "help_mstp_status_bridges",
            url: "/monitor/spanning_tree/bridge_status"
          },
          {
            name: "Bridge Status detailed",
            visible: false,
            id: "mstp_status_bridge",
            title: "STP Detailed Bridge Status",
            level: "level-3",
            component: MstpStatusBridge,
            help: "help_mstp_status_bridge",
            url: "/monitor/spanning_tree/bridge_status_detailed"
          },
          {
            name: "Port Status",
            id: "mstp_status_ports",
            title: "STP Port Status",
            level: "level-3",
            component: MstpStatusPorts,
            help: "help_mstp_status_ports",
            url: "/monitor/spanning_tree/port_status"
          },
          {
            name: "Port Statistics",
            id: "mstp_statistics",
            title: "STP Statistics",
            level: "level-3",
            component: MstpStatistics,
            help: "help_mstp_statistics",
            url: "/monitor/spanning_tree/mstp_statistics"
          }
        ]
      },
      {
        name: "IPMC",
        id: "monitor_ipmc",
        level: "level-2",
        children: [
          {
            name: "IGMP Snooping",
            id: "monitor_ipmc_igmp_snooping",
            level: "level-3",
            children: [
              {
                name: "Status",
                title: "IGMP Snooping Status",
                id: "ipmc_igmps_status",
                level: "level-4",
                component: IpmcIgmpsStatus,
                help: "help_ipmc_igmps_status",
                url: "/monitor/ipmc/igmp_snooping/status"
              }
            ]
          }
        ]
      },
      {
        name: "PoE",
        title: "Power Over Ethernet Status",
        id: "poe_status",
        level: "level-2",
        component: PoeStatus,
        help: "help_poe_status",
        url: "/monitor/poe"
      },
      {
        name: "MAC Table",
        title: "MAC Address Table",
        id: "dyna_mac",
        level: "level-2",
        component: DynaMac,
        help: "help_dyn_mac_table",
        url: "/monitor/mac_table"
      },
      {
        name: "VLANs",
        id: "monitor_vlans",
        level: "level-2",
        children: [
          {
            name: "Membership",
            title: "VLAN Membership Status",
            id: "vlan_membership_stat",
            level: "level-3",
            component: VlanMembershipStat,
            help: "help_vlan_membership",
            url: "/monitor/vlans/membership"
          },
          {
            name: "Ports",
            title: "VLAN Port Status",
            id: "vlan_port_stat",
            level: "level-3",
            component: VlanPortStat,
            help: "help_vlan_port_stat",
            url: "/monitor/vlans/ports"
          }
        ]
      },
      {
        name: "MVRP",
        title: "MVRP Statistics",
        id: "mvrp_stat",
        level: "level-2",
        component: MvrpStat,
        help: "help_mvrp_stat",
        url: "/monitor/mvrp"
      },
      {
        name: "sFlow",
        title: "sFlow Statistics & Receiver Statistics & Port Statistics",
        id: "sflow_status",
        level: "level-2",
        component: SflowStatus,
        help: "help_sflow_status",
        url: "/monitor/sflow"
      },
      {
        name: "DDMI",
        id: "monitor_ddmi",
        level: "level-2",
        children: [
          {
            name: "Overview",
            title: "DDMI Overview",
            id: "ddmi_overview",
            level: "level-3",
            component: DDMIoverview,
            help: "help_ddmi_overview",
            url: "/monitor/ddmi/overview"
          },
          {
            name: "Detailed",
            title: "Transceiver & DDMI Information",
            id: "ddmi_detailed",
            level: "level-3",
            component: DdmiDetailed,
            help: "help_ddmi_detailed",
            url: "/monitor/ddmi/detailed"
          }
        ]
      }
    ]
  },
  {
    name: "Diagnostics",
    id: "diagnostics",
    level: "level-1",
    children: [
      {
        name: "Ping (IPv4)",
        title: "Ping (IPv4)",
        id: "ping4",
        level: "level-2",
        component: Ping4,
        help: "help_diagnostics#ping",
        url: "/diagnostics/ping_ipv4"
      },
      {
        name: "Ping (IPv4) Output",
        title: "Ping (IPv4) Output",
        id: "ping4_result",
        visible: false,
        component: Ping4Result,
        url: "/diagnostics/ping4_result"
      },
      {
        name: "Ping (IPv6)",
        title: "Ping (IPv6)",
        id: "ping6",
        level: "level-2",
        component: Ping6,
        help: "help_diagnostics#ping6",
        url: "/diagnostics/ping_ipv6"
      },
      {
        name: "Ping (IPv6) Output",
        title: "Ping (IPv6) Output",
        id: "ping6_result",
        visible: false,
        component: Ping6Result,
        url: "/diagnostics/ping6_result"
      },
      {
        name: "Traceroute (IPv4)",
        title: "Traceroute(IPv4)",
        id: "traceroute4",
        level: "level-4",
        component: Traceroute4,
        help: "help_diagnostics#traceroute",
        url: "/diagnostics/traceroute_ipv4"
      },
      {
        name: "Traceroute (IPv4) Output",
        title: "Ping (IPv4) Output",
        id: "traceroute4_result",
        visible: false,
        component: Traceroute4Result,
        url: "/diagnostics/traceroute4_result"
      },
      {
        name: "Traceroute (IPv6)",
        title: "Traceroute (IPv6)",
        id: "traceroute6",
        level: "level-4",
        component: Traceroute6,
        help: "help_diagnostics#traceroute6",
        url: "/diagnostics/traceroute_ipv6"
      },
      {
        name: "Traceroute (IPv6)",
        title: "Traceroute (IPv6)",
        id: "traceroute6_result",
        visible: false,
        level: "level-4",
        component: Traceroute6Result,
        url: "/diagnostics/traceroute6_result"
      },
      {
        name: "VeriPHY",
        title: "VeriPHY Cable Diagnostics",
        id: "veriphy",
        level: "level-4",
        component: Veriphy,
        help: "help_veriphy",
        url: "/diagnostics/veriphy"
      }
    ]
  },
  {
    name: "Maintenance",
    id: "maintenance",
    level: "level-1",
    children: [
      {
        name: "Restart Device",
        title: "Restart Device",
        id: "wreset",
        level: "level-2",
        component: Wreset,
        help: "help_maintenance_restart",
        url: "/maintenance/restart_device"
      },
      {
        name: "Factory Defaults",
        title: "Factory Defaults",
        id: "factory",
        level: "level-2",
        component: Factory,
        help: "help_maintenance_factory",
        url: "/maintenance/factory_defaults"
      },
      {
        name: "Software",
        id: "maintenance_software",
        level: "level-2",
        children: [
          {
            name: "Upload",
            title: "Software Upload",
            id: "upload",
            level: "level-3",
            component: Upload,
            help: "help_maintenance_upload",
            url: "/maintenance/software/upload"
          },
          {
            name: "Image Select",
            title: "Software Image Selection",
            id: "sw_select",
            level: "level-3",
            component: SwSelect,
            help: "help_sw_select",
            url: "/maintenance/software/image_select"
          }
        ]
      },
      {
        name: "Configurartion",
        id: "maintenance_configuration",
        level: "level-2",
        children: [
          {
            name: "Save startup-config",
            title: "Save Running Configuration to startup-config",
            id: "icfg_conf_save",
            level: "level-4",
            component: IcfgConfSave,
            help: "help_maintenance_icfg_conf#save",
            url: "/maintenance/configuration/save_startup_config"
          },
          {
            name: "Download",
            title: "Download Configuration",
            id: "icfg_conf_download",
            level: "level-4",
            component: IcfgConfDownload,
            help: "help_maintenance_icfg_conf#download",
            url: "/maintenance/configuration/download"
          },
          {
            name: "Upload",
            title: "Upload Configuration",
            id: "icfg_conf_upload",
            level: "level-4",
            component: IcfgConfUpload,
            help: "help_maintenance_icfg_conf#upload",
            url: "/maintenance/configuration/upload"
          },
          {
            name: "Activate",
            title: "Activation Configuration",
            id: "icfg_conf_activate",
            level: "level-4",
            component: IcfgConfActivate,
            help: "help_maintenance_icfg_conf#activate",
            url: "/maintenance/configuration/activate"
          },
          {
            name: "Delete",
            title: "Delete Configuration File",
            id: "icfg_conf_delete",
            level: "level-4",
            component: IcfgConfDelete,
            help: "help_maintenance_icfg_conf#delete",
            url: "/maintenance/configuration/delete"
          },
          {
            name: "running",
            title: "Activating New Configuration",
            id: "icfg_conf_running",
            visible: false,
            level: "level-4",
            component: IcfgConfRunning,
            url: "/maintenance/configuration/running"
          }
        ]
      }
    ]
  }
];

function stortOpenModuleID(compileTimeTree = [], runtimeDisabledList = []) {
  let openIDList = [];

  // Transform and store all the id in "tree" properties in /navbar REST API into an array
  compileTimeTree.forEach(function m(item) {
    if (item.id) openIDList.push(item.id);
    if (item.children) {
      item.children.forEach(m);
    }
  });

  // Splice "runtime-disabled" list id
  runtimeDisabledList.forEach(runtimedisabledID => {
    if (openIDList.includes(runtimedisabledID)) {
      const index = openIDList.indexOf(runtimedisabledID);
      openIDList.splice(index, 1);
    }
  });

  return openIDList;
}

function deleteUnusedModule(menuData = [], openModuleIDList = []) {
  let _menuData = menuData;

  // Decrement index to prevent re-indexed while splice array
  for (let i = _menuData.length - 1; i >= 0; i--) {
    const testID = _menuData[i].id;
    const isModuleExist = openModuleIDList.some(existID => testID === existID); // Check the id exists in openModuleIDList

    if (!isModuleExist) {
      _menuData.splice(i, 1);
    } else if (isModuleExist) {
      if (_menuData[i].children) deleteUnusedModule(_menuData[i].children, openModuleIDList); // recursive if property has children
    }
  }

  return _menuData;
}

const openModuleAsync = async () => {
  let response = await fetch("/navbar", { headers: { "Lntn-Web-Ver": "2.0" } });
  let data = await response.json();
  let openModuleIDList = stortOpenModuleID(data.tree, data["runtime-disabled"]);
  let openModuleTree = deleteUnusedModule(menuData, openModuleIDList);

  return openModuleTree;
};

export default openModuleAsync;
