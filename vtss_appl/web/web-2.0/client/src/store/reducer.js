const defaultState = { isSidebarOpen: true };

export default (state = defaultState, action) => {
  if (action.type === "change_sidebar_open_state") {
    const newState = JSON.parse(JSON.stringify(state));

    newState.isSidebarOpen = action.value;

    return newState;
  }

  return state;
};
