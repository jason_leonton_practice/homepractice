import { CHANGE_SIDEBAR_OPEN_STATE } from "./actionTypes";

export const sidebarToggleAction = value => ({
  type: CHANGE_SIDEBAR_OPEN_STATE,
  value
});
