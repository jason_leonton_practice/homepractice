import React from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "semantic-ui-css-offline";
import "react-toastify/dist/ReactToastify.css";
import "semantic-ui-css/semantic.min.css";

import SidebarWrapper from "./components/sidebar";
import TopNav from "./components/top_nav";
import BreadcrumbComponent from "./components/breadcrumb";
import CustomDimmer from "./components/custom-dimmer";
import getOpenModuleList from "./utils/modules_tree";
import Panel from "./components/panel";
import { LoadingSegment } from "./components/loading_segment";
import "./App.scss";
import store from "./store";

const Error = React.lazy(() => import("./page/error"));

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { redux: store.getState(), routes: "", loading: false };

    store.subscribe(this.handleStoreChange);
  }

  handleStoreChange = () => {
    this.setState({ redux: store.getState() });
  };

  handleCustomSidebar = array => {
    this.setState({ customSidebar: array });
  };

  generateRoute = (children = []) => {
    const content = children.map(({ children, url, component: Component, name } = {}) => {
      if (!children) {
        if (name === "Custom Sidebar") {
          return (
            <Route
              exact
              path={url}
              key={name}
              render={props => <Component {...props} customSidebar={this.handleCustomSidebar.bind(this)} />}
            />
          );
        }

        return <Route exact path={url} component={Component ? Component : Error} key={name} />;
      }

      return this.generateRoute(children);
    });

    return content;
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  async componentDidMount() {
    this.setState({ loading: true });
    const openModuleList = await getOpenModuleList();
    const routes = this.generateRoute(openModuleList);

    this.setState({ routes, loading: false });
  }

  render() {
    const { isSidebarOpen } = this.state.redux;
    const { loading } = this.state;

    return (
      <BrowserRouter>
        <div className={`app-container ${!isSidebarOpen && "sidebar-close"}`}>
          <SidebarWrapper customSidebar={this.state.customSidebar} />

          <div className="main-panel">
            <div className="topnav-container">
              <TopNav />
            </div>

            <div className="breadcrumb-container">
              <BreadcrumbComponent />
            </div>

            <div className="main-content-container">
              {/* main content */}
              <div className="main-content">
                <React.Suspense fallback={<LoadingSegment />}>
                  <Switch>
                    {this.state.routes}
                    <Route render={props => <Error {...props} loading={loading} />} />
                  </Switch>
                </React.Suspense>
              </div>

              {/* Mylar panel */}
              <div className="mylar-panel">
                <Panel />
              </div>
            </div>
          </div>

          <CustomDimmer isSidebarOpen={isSidebarOpen} />
        </div>

        <ToastContainer />
      </BrowserRouter>
    );
  }
}

export default App;
