import React from "react";
import "./breadcrumb.scss";
import { withRouter } from "react-router-dom";

function BreadcrumbComponent(props) {
  const path = props.location.pathname;
  const title = path === "/" ? "Dashboard" : "";

  return (
    <div className="breadcrumb-wrapper">
      <div className="page-title-wrapper">
        <h2>{title}</h2>
      </div>
    </div>
  );
}

export default withRouter(BreadcrumbComponent);
