import React from "react";
import { withRouter } from "react-router-dom";
import { Menu, Icon, Dropdown } from "semantic-ui-react";
import "./top_nav.scss";
import store from "../store";
import SearchCategory from "./search_category";
import getOpenModuleList from "../utils/modules_tree";
import { sidebarToggleAction } from "../store/actionCreators";

class TopNav extends React.Component {
  constructor(props) {
    super(props);
    const redux = store.getState();
    this.state = {
      redux,
      sidebarExpand: true,
      help: "help_dashboard",
      fragment: "",
      showHelp: false
    };

    store.subscribe(this.handleStoreChange);
    this.moduleList = [];
  }

  openHelp = () => {
    const currentURL = this.props.location.pathname;
    let that = this;

    this.moduleList.find(function find({ children, url, help }) {
      if (children) {
        children.find(find);
        return false;
      }

      if (url === currentURL) {
        if (help) {
          if (help.includes("#")) {
            const fragmentIdx = help.indexOf("#");
            const helpUrl = help.slice(0, fragmentIdx);
            const fragment = help.slice(fragmentIdx);

            return that.setState({ help: helpUrl, fragment, showHelp: true });
          } else {
            return that.setState({ help, fragment: "", showHelp: true });
          }
        }
        return alert("Sorry, this page does not have any help.");
      }

      return false;
    });
  };

  redirect = () => (document.location.href = "/index.html");

  basicAuthLogout = () => {
    let req,
      agent = navigator.userAgent.toLowerCase(),
      rc = false;

    try {
      if (agent.includes("msie") || agent.includes("trident") || agent.includes("edge")) {
        console.log("IE or Edge detected");
        // Only MS provide syntax for clear authentication cache
        document.execCommand("ClearAuthenticationCache", "false");
        this.redirect();
        rc = true;
      } else if (agent.includes("firefox")) {
        console.log("Firefox detected");

        req = new XMLHttpRequest();
        req.open("GET", "/logout", true, "~", "");
        req.send(null);
        req.abort();

        rc = true;
      } else if (
        agent.includes("chrome") ||
        agent.includes("chromium") ||
        agent.includes("safari") ||
        agent.includes("opr")
      ) {
        console.log("Chrome, Chromium, Safari or Opera 15+ detected");
        req = new XMLHttpRequest();
        req.open("GET", "/logout");
        req.setRequestHeader("Authorization", "Basic " + btoa("~:"));
        req.timeout = 500;
        req.ontimeout = () => this.redirect();
        req.send(null);

        rc = true;
      } else {
        console.log("Usupported browser: " + navigator.userAgent);
      }
    } catch (e) {
      console.log("error: " + e);
    }

    req = null;
    return rc;
  };

  handleLogout = () => {
    if (window.confirm("Do you want to log out the web site?")) {
      if (!this.basicAuthLogout()) {
        alert("Logout is not supported in the current browser.\nPleaase close all browser windows.");
      }
    }
  };

  handleHelpItemClick = (e, { name }) => {
    this.setState({ activeItem: name });
  };

  handleSidebarExpand = () => {
    const action = sidebarToggleAction(!this.state.redux.isSidebarOpen);

    store.dispatch(action);
  };

  handleStoreChange = () => {
    this.setState({
      redux: store.getState()
    });
  };

  async componentDidMount() {
    this.moduleList = await getOpenModuleList();
  }

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    const DropdownOptions = [
      {
        key: "user",
        text: (
          <span>
            Signed in as <strong>Admin</strong>
          </span>
        ),
        disabled: true
      },
      {
        key: "wizard",
        text: "Quick Setup",
        onClick: () => {
          this.props.history.push("/quick-setup");
        }
      },
      // {
      //   key: "custom-sidebar",
      //   text: "Customize Sidebar",
      //   onClick: () => {
      //     this.props.history.push("/custom-sidebar");
      //   }
      // },
      {
        key: "log-out",
        text: "Log Out",
        onClick: () => this.handleLogout()
      }
    ];

    const fragment = this.state.fragment !== "" ? this.state.fragment : "";

    return (
      <Menu className="topnav-menu">
        <Menu.Item className="hamburger-bar" onClick={this.handleSidebarExpand}>
          <Icon name="sidebar" />
        </Menu.Item>

        <div className="model-info">
          {/* eslint-disable-next-line no-undef */}
          <h2>{configSwitchDescription}</h2>
          <div className="model-detailed-info">
            <p>
              {/* eslint-disable-next-line no-undef */}
              MAC: <span>{configSwitchMac}</span>
            </p>
            <p>
              {/* eslint-disable-next-line no-undef */}
              Serial Number: <span>{configSwitchSn}</span>
            </p>
            <p>
              {/* eslint-disable-next-line no-undef */}
              Firmware Version: <span>{configSwitchSv}</span>
            </p>
          </div>
        </div>

        <Menu.Menu position="right">
          <Menu.Item>
            <SearchCategory />
          </Menu.Item>

          <Menu.Item onClick={this.openHelp}>
            <Icon name="help circle" />
          </Menu.Item>
          {this.state.showHelp && (
            <div className={`help-iframe-wrapper ${this.state.showHelp && "open"}`}>
              <Icon
                name="close"
                onClick={() => this.setState({ showHelp: false })}
                className="help-page-close-icon"
              />
              <iframe title="help page" src={`/help_2.0/${this.state.help}.html${fragment}`}></iframe>
            </div>
          )}

          <Menu.Item>
            <Dropdown
              trigger={
                <span>
                  <Icon name="user" />
                </span>
              }
              options={DropdownOptions}
              pointing="top right"
            />
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    );
  }
}

export default withRouter(TopNav);
