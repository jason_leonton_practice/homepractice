import React from "react";
import _ from "lodash";
import Fuse from "fuse.js";
import { withRouter } from "react-router";
import { Search } from "semantic-ui-react";
import getOpenModuleList from "../utils/modules_tree";

const source = {
  Configuration: { name: "Configuration" },
  Monitor: { name: "Monitor" },
  Diagnostics: { name: "Diagnostics" },
  Maintenance: { name: "Maintenance" }
};

const initialState = {
  isLoading: false,
  results: [],
  value: "",
  source: source
};

const fuseOptions = {
  shouldSort: true,
  threshold: 0.6,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: ["title", "name"]
};

class SearchCategory extends React.Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  handleResultSelect = (e, { result }) => {
    this.setState({ value: result.title });
    this.props.history.push(result.url);
  };

  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, value });

    setTimeout(() => {
      if (this.state.value.length < 1) return this.setState(initialState);

      const filteredResults = _.reduce(
        this.state.source,
        (memo, data, name) => {
          let fuse = new Fuse(data.results, fuseOptions);
          const results = fuse.search(value);

          if (results.length) memo[name] = { name, results };

          return memo;
        },
        {}
      );

      this.setState({ isLoading: false, results: filteredResults });
    }, 300);
  };

  generateSearchResults = async () => {
    let self = this; // the scope will change while calling a new function
    let openModules = await getOpenModuleList();

    openModules.forEach(function map(item) {
      let that = self;
      if (item.id === "dashboard" || item.id === "custom-sidebar" || item.id === "quick-setup") {
        return false;
      }

      if (
        item.id === "configuration" ||
        item.id === "monitor" ||
        item.id === "diagnostics" ||
        item.id === "maintenance"
      ) {
        let results = [];

        item.children.forEach(function m(item) {
          if (item.children) {
            item.children.forEach(m);
            return false;
          }

          if (typeof item.visible !== "undefined" && !item.visible) return false;

          let obj = {};

          obj.title = item.title;
          obj.url = item.url;

          results.push(obj);
        });

        let newSource = that.state.source;
        newSource[item.name].results = results;
        that.setState({ source: newSource });
      }
    });
  };

  componentDidMount() {
    this.generateSearchResults();
  }

  render() {
    const { isLoading, value, results } = this.state;

    return (
      <Search
        category
        loading={isLoading}
        onResultSelect={this.handleResultSelect}
        onSearchChange={_.debounce(this.handleSearchChange, 500, {
          leading: true
        })}
        value={value}
        results={results}
      />
    );
  }
}

export default withRouter(SearchCategory);
