import React from "react";
import { Table } from "semantic-ui-react";

import { directionConverter } from "../utils/self-documenting_func";
import { SortIcon } from "./styled_component";

export const TableHeader = props => {
  const { column, order, handleSort, colTitles, sortable } = props;
  const direction = directionConverter(order);

  return (
    <Table.Header>
      <Table.Row>
        {colTitles.map((title, i) => {
          if (!sortable) return <Table.HeaderCell key={i}>{title}</Table.HeaderCell>;

          return (
            <Table.HeaderCell key={i} onClick={() => handleSort(title)}>
              {title}
              <SortIcon name={column === title ? `caret ${direction}` : "sort"} />
            </Table.HeaderCell>
          );
        })}
      </Table.Row>
    </Table.Header>
  );
};
