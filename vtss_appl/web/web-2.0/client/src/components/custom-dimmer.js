import React from "react";
import "./custom-dimmer.scss";

export default function CustomDimmer(props) {
  const { isSidebarOpen, isMylarOpen } = props;
  const activeDimmer = () => {
    if (window.matchMedia(`(max-width: 1366px)`).matches && isMylarOpen) return "active";
    if (window.matchMedia("(max-width: 1024px)").matches && isSidebarOpen) return "active";

    return "";
  };

  return <div className={`custom-dimmer ${activeDimmer()}`} />;
}
