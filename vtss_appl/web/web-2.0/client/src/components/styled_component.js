import styled from "styled-components";
import {
  Icon,
  Segment,
  Table as SemanticTable,
  Dropdown as SemanticDropdown,
  Button as SemanticButton
} from "semantic-ui-react";

export const MsgLists = styled.ul`
  margin: 1em 0;
  padding: 1em 1.5em;
  background-color: #fff6f6;
  color: #9f3a38;
  border-radius: 0.28571429rem;
  box-shadow: 0 0 0 1px #e0b4b4 inset, 0 0 0 0 transparent;
`;
export const List = styled.li`
  margin-left: 1em;
`;
export const SortIcon = styled(Icon)({
  marginLeft: "3px !important"
});
export const SegmentHeader = styled(Segment)({
  fontWeight: "bold"
  // borderTop: "2px solid #ff6f00 !important"
});
export const InnerTable = styled(SemanticTable)`
  &.table {
    font-size: 13px !important;
    
    &.compact.celled {
    th {
      padding: 0.9em 0.5em !important;
    }
    td {
      padding: 0.4em 0.5em !important;
    }
    
    &.definition {}
  }
`;
export const OuterTable = styled(SemanticTable)({
  borderTop: ".2em solid #f2711c !important"
});
export const Dropdown = styled(SemanticDropdown)``;

export const Button = styled(SemanticButton)`
  background-color: #e0e1e2 !important;
  color: rgba(0, 0, 0, 0.6) !important;
  :hover {
    background-color: #cacbcd !important;
  }
  &.save,
  &.select-file,
  &.start {
    background-color: #2185d0 !important;
    color: #fff !important;

    :hover {
      background-color: #1678c2 !important;
    }
  }
  &.reset,
  &.yes,
  &.activate-alternate-image {
    background-color: #1b1c1d !important;
    color: #fff !important;

    :hover {
      background-color: #27292a !important;
    }
  }
  &.delete-user {
    background-color: #db2828 !important;
    color: #fff !important;

    :hover {
      background-color: #d01919 !important;
    }
  }
`;
