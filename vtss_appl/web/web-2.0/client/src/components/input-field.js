import React from "react";
import { Dropdown, Input, Checkbox, Radio, Form } from "semantic-ui-react";
import "./input-field.scss";

export function SelectInput({ input, meta }, options, features = {}) {
  const { touched, error, submitError } = meta;
  const displayErrMsg = error ? error : submitError;

  return (
    <div className="field dropdown">
      <Dropdown
        error={error && touched}
        {...features}
        {...input}
        value={input ? input.value : ""}
        options={options}
        onChange={(param, data) => input.onChange(data.value)}
      />
      {(error || submitError) && touched && <div className="prompt label">{displayErrMsg}</div>}
    </div>
  );
}

export function TextInput({ input, meta }, features = {}) {
  // error與submitError之差別為一個是 field validation 一個是 submit validation
  const { touched, error, submitError, dirtySinceLastSubmit } = meta;
  const displayErrMsg = error ? error : submitError;

  return (
    <div className="field">
      <Input {...input} error={error && touched} {...features} />
      {(error || (submitError && !dirtySinceLastSubmit)) && touched && (
        <div className="prompt label" dangerouslySetInnerHTML={{ __html: displayErrMsg }} />
      )}
    </div>
  );
}

export const CustomCheckbox = props => {
  const { input, meta } = props.fieldProps;

  return (
    <Form.Field>
      <Checkbox
        checked={input.value ? true : false}
        name={input.name}
        label={props.label}
        disabled={props.disabled}
        onChange={(e, { checked }) => input.onChange(checked)}
      />
      {meta.error && meta.touched && <div className="prompt label">{meta.error}</div>}
    </Form.Field>
  );
};

export const CustomRadio = props => {
  const { input } = props.fieldProps;

  return (
    <Radio
      value={input.value}
      name={input.name}
      checked={input.checked}
      disabled={props.disabled}
      label={props.label}
      onChange={(e, { value }) => input.onChange(value)}
    />
  );
};

export const TextArea = props => {
  const { input, meta } = props;
  const { touched, error, submitError, dirtySinceLastSubmit } = meta;
  const displayErrMsg = error ? error : submitError;
  // 在submit完出現error後，若使用者再次觸碰input，則showErr為false
  const showErr = (error || (submitError && !dirtySinceLastSubmit)) && touched;

  return (
    <div className="field">
      <textarea {...input} className={`custom-textarea ${showErr ? "warning" : ""}`} />
      {showErr && <div className="inline">{displayErrMsg}</div>}
    </div>
  );
};

// Semantic Form Field
export const CustomInput = props => {
  const { input, meta } = props.fieldProps;
  const required = props.required ? true : false;

  return (
    <Form.Field error={meta.error && meta.touched} required={required}>
      <label>{props.label}</label>
      <input {...input} {...props.features} />
      {meta.error && meta.touched && (
        <p className="markdown-error-msg" dangerouslySetInnerHTML={{ __html: meta.error }}></p>
      )}
    </Form.Field>
  );
};

export const CustomSelectInput = props => {
  const { input, meta } = props.fieldProps;
  const required = props.required ? true : false;

  return (
    <div className="field dropdown">
      <Form.Select
        {...input}
        value={input ? input.value : ""}
        options={props.options}
        label={props.label}
        required={required}
        disabled={props.disabled}
        error={meta.error && meta.touched}
        onChange={(param, data) => input.onChange(data.value)}
      />
      {meta.error && meta.touched && <div className="prompt label">{meta.error}</div>}
    </div>
  );
};
