import React from "react";
import { Icon } from "semantic-ui-react";
import { toast, Slide } from "react-toastify";

import "./messages.scss";

export function msgConfig(autoClose = 10000, position = "bottom-right") {
  return {
    position,
    autoClose,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    transition: Slide,
    className: "custom-toast",
    progressClassName: "custom-progress-bar"
  };
}

export function InfoMsg(type, des, status = null, statusText = null) {
  const iconName = () => {
    const typeToLowercase = type && type.toLowerCase();
    switch (typeToLowercase) {
      case "success":
        return "thumbs up outline";
      default:
        return "exclamation circle";
    }
  };
  const description = () => {
    // Enter scope while parmeter equal to null
    if (typeof des === "object") {
      const typeToLowercase = type && type.toLowerCase();
      switch (typeToLowercase) {
        case "success":
          return "Form has been submitted successfully!";
        case "error":
          return "Something went wrong! Try again later.";
        default:
          return des;
      }
    }

    return des;
  };

  return (
    <div>
      <h3 style={{ textTransform: "capitalize" }}>
        <Icon name={iconName()}></Icon> {type}
      </h3>
      {status && <span>HTTP Status: {status} </span>}
      {statusText && <span>{statusText}</span>}
      <p dangerouslySetInnerHTML={{ __html: description() }}></p>
    </div>
  );
}

export function displayReadOnlyToast(isReadOnly = false) {
  if (isReadOnly) {
    const info =
      "<b>Read-Only page</b>. If you want to modify page config, change the security privilege level.";
    toast.info(InfoMsg("Note", info), msgConfig(false));
  }
}
