import React, { Component } from "react";
import { Dropdown, Checkbox } from "semantic-ui-react";
import { Button as StyledBtn } from "./styled_component";
import "./semantic-component.scss";

const capitalizeFirstLetter = string => {
  return string.replace(/\w\S*/g, function(txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
};

export function SemanticDropdown(props) {
  const { name, options, value, attr, feature } = props;

  return (
    <Dropdown
      {...feature}
      {...attr}
      options={options}
      value={value}
      name={name}
      onChange={props.onChange}
    ></Dropdown>
  );
}

export class SemanticButton extends Component {
  state = { loading: false };

  handleClick = () => {
    this.setState({ loading: true }, () => {
      this.timer = setTimeout(() => this.setState({ loading: false }), 300);
    });

    this.props.onClick();
  };

  componentWillUnmount() {
    this.timer = clearTimeout(this.timer);
  }

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    const { title, attr, disabled } = this.props;
    const className = title
      .split(" ")
      .join("-")
      .toLowerCase();
    const { loading } = this.state;
    const lowercaseTitle = title.toLowerCase();
    let restoreBtn = false;

    if (
      disabled &&
      (lowercaseTitle === "save" ||
        lowercaseTitle === "addnewentry" ||
        lowercaseTitle === "addnewentry1" ||
        lowercaseTitle === "next >" ||
        lowercaseTitle === "clear" ||
        lowercaseTitle === "clear all" ||
        lowercaseTitle === "clear this" ||
        lowercaseTitle === "remove all" ||
        lowercaseTitle === "delelte user" ||
        lowercaseTitle === "start" ||
        lowercaseTitle === "yes" ||
        lowercaseTitle === "no" ||
        lowercaseTitle === "configuration" ||
        lowercaseTitle === "save configuration" ||
        lowercaseTitle === "download configuration" ||
        lowercaseTitle === "upload configuration" ||
        lowercaseTitle === "activate configuration" ||
        lowercaseTitle === "delete configuration file" ||
        lowercaseTitle === "translate dynamic to static" ||
        lowercaseTitle === "upload")
    ) {
      restoreBtn = true;
    } else {
      restoreBtn = false;
    }

    return (
      <StyledBtn
        {...attr}
        disabled={disabled || restoreBtn}
        className={className}
        loading={loading}
        onClick={this.handleClick}
      >
        {capitalizeFirstLetter(title)}
      </StyledBtn>
    );
  }
}

export function BtnWrapper(props) {
  return <div style={{ textAlign: "right" }}>{props.children}</div>;
}

export function HeaderRefreshBar(props) {
  return (
    <div className={`refreshbar ${props.className}`}>
      <Checkbox
        className="auto-refresh-checkbox"
        toggle
        checked={props.checked}
        onChange={props.handleAutoRefresh}
      />
    </div>
  );
}
