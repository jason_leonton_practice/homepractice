import React from "react";
import { Header, Icon, Progress } from "semantic-ui-react";
import styled from "styled-components";

import { Button } from "./styled_component";

/*
  DragAndDrop component is inspired by
  https://medium.com/@650egor/simple-drag-and-drop-file-upload-in-react-2cb409d88929

  Passing Props:
  uploadStatus: "uploading" || "success" || "error"
  valdiation: { error: boolean, msg: string }
    > if you don't need display error message, just passing
    > validation={{ error: false, msg: "" }} in props
  progress: Number
*/

class DragAndDrop extends React.Component {
  constructor(props) {
    super(props);

    this.state = { dragging: false };
    this.fileInputRef = React.createRef();
    this.dragCounter = 0;
  }

  handleDrag = e => {
    e.preventDefault();
    e.stopPropagation();
  };

  handleDragIn = e => {
    e.preventDefault();
    e.stopPropagation();

    this.dragCounter++;
    if (e.dataTransfer.items && e.dataTransfer.items.length > 0) {
      this.setState({ dragging: true });
    }
  };

  handleDragOut = e => {
    e.preventDefault();
    e.stopPropagation();

    this.dragCounter--;
    if (this.dragCounter > 0) return;
    this.setState({ dragging: false });
  };

  handleDrop = e => {
    e.preventDefault();
    e.stopPropagation();

    this.setState({ dragging: false });
    if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
      this.props.handleDrop(e.dataTransfer.files);
      this.setState({ file: e.dataTransfer.files[0] });
      e.dataTransfer.clearData();
      this.dragCounter = 0;
    }
  };

  onChangeFileInput = e => {
    const files = e.target.files;

    this.props.handleDrop(files);
    this.setState({ file: files[0] });
  };

  removeFile = () => {
    this.setState({ file: null });
  };

  render() {
    const { dragging, file } = this.state;
    const { validation, uploadStatus, progress } = this.props;
    const errorTheme = { background: "#fff6f6", border: "1px solid #e0b4b4" };

    return (
      <DragAndDropContainer
        theme={validation.error && errorTheme}
        onDragEnter={this.handleDragIn}
        onDragLeave={this.handleDragOut}
        onDragOver={this.handleDrag}
        onDrop={this.handleDrop}
      >
        {/* show this while user are dragging the file into the container */}
        {dragging && (
          <DaggingWrapper>
            <DraggingDimmer>
              <Header as="h3" icon>
                <Icon name="file code outline" />
                Dropping File to Upload
              </Header>
            </DraggingDimmer>
          </DaggingWrapper>
        )}

        {/* show this while uesr dropped the file into the container */}
        {!dragging && file ? (
          <FileInfoWrapper>
            {uploadStatus !== "uploading" && (
              <RemoveFileWrapper onClick={this.removeFile}>
                <Icon name="trash alternate" />
              </RemoveFileWrapper>
            )}

            <h3>
              <Icon name="file code outline" />
              File Name: <span style={{ fontWeight: "normal" }}>{file.name}</span>
            </h3>
            <h3>
              File Size:{" "}
              <span style={{ fontWeight: "normal" }}>{Math.round((file.size / 1000000) * 100) / 100} MB</span>
            </h3>
            {validation.error && <ErrorMsg>{validation.msg}</ErrorMsg>}
            <Progress
              progress
              percent={progress}
              active={uploadStatus === "uploading"}
              success={uploadStatus === "success"}
              error={uploadStatus === "error"}
            />
          </FileInfoWrapper>
        ) : (
          <Header icon>
            <Icon name="cloud upload" />
            <p>Drop File to Upload</p>

            {/* [Ref] - input type file Semantic-ui react 
                https://stackoverflow.com/questions/55464274/react-input-type-file-semantic-ui-react  */}
            <Button className="select-file" onClick={() => this.fileInputRef.current.click()}>
              or Click Here
            </Button>
            <input ref={this.fileInputRef} type="file" hidden onChange={this.onChangeFileInput} />
          </Header>
        )}
      </DragAndDropContainer>
    );
  }
}

const DragAndDropContainer = styled.div`
  position: relative;
  border-color: rgba(34, 36, 38, 0.15);
  box-shadow: 0 2px 25px 0 rgba(34, 36, 38, 0.05) inset;
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 1em 1em;
  min-height: 18rem;
  border-radius: 0.28571429rem;

  border: ${props => props.theme.border};
  background: ${props => props.theme.background};
`;
DragAndDropContainer.defaultProps = {
  theme: {
    background: "none",
    border: "1px solid rgba(34, 36, 38, 0.15)"
  }
};
const DaggingWrapper = styled.div({
  border: "dashed grey 4px",
  backgroundColor: "rgba(255,255,255,.8)",
  position: "absolute",
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  zIndex: 9999
});
const DraggingDimmer = styled.div({
  position: "absolute",
  top: "50%",
  right: 0,
  left: 0,
  transform: "translateY(-50%)",
  textAlign: "center",
  color: "grey",
  fontSize: 36
});
const FileInfoWrapper = styled.div({
  textAlign: "center"
});
const RemoveFileWrapper = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  cursor: pointer;
  padding: 1em 0.5em;
  :hover i {
    color: #9f3a38;
  }

  i {
    font-size: 26px;
  }
`;
const ErrorMsg = styled.p`
  color: #912d2b;
`;

export default DragAndDrop;
