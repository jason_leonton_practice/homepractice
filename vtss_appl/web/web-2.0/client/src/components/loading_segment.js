import React from "react";
import { Segment, Dimmer, Loader } from "semantic-ui-react";

export function LoadingSegment() {
  return (
    <Segment style={{ height: "400px", zIndex: 90 }}>
      <Dimmer active inverted>
        <Loader inverted>Loading</Loader>
      </Dimmer>
    </Segment>
  );
}
