import React from "react";
import { Icon } from "semantic-ui-react";
import styled from "styled-components";
import _ from "lodash";

import WithData from "../utils/withData";
import CustomDimmer from "./custom-dimmer";
import "./panel.scss";

// const { configSwitchName } = require("../utils/config");
const mql = window.matchMedia(`(max-width: 1366px)`);

class DeviceMonitor extends React.Component {
  state = { isMylarOpen: false, toggleMylarBtnDisplay: true, update: 0 };

  addSwitch(portData, ledConfig) {
    const portConfig = portData.split("|");
    const sid = parseInt(portConfig[0], 10);
    const id = `switch_${sid}`;
    const mylar = portConfig[1];

    return (
      <div id={`container_${id}`} className="switch-monitor">
        <img src={`/media/images/${mylar}`} id={`switch_${id}`} alt="Switch LTN" />
        {portConfig.map((portValue, index) => {
          if (index < 2) return false;

          if (portValue) {
            let pval = portValue.split("/");

            if (!isNaN(parseInt(pval[0], 10))) {
              let portStyles = {
                left: `${pval[3]}px`,
                top: `${pval[4]}px`,
                width: `${pval[5]}px`,
                height: `${pval[6]}px`
              };
              let labelStyles = {
                top: `${parseInt(pval[4], 10) + 9}px`,
                left: `${parseInt(pval[3], 10) + parseInt(pval[7], 10) * 27}px`
              };

              return (
                // label
                <div key={pval[0]}>
                  <span>
                    <img
                      src={`/media/images/${pval[1]}`}
                      alt={pval[2]}
                      className="port-label"
                      style={portStyles}
                    />
                  </span>
                  {pval[0].length ? (
                    <div className="port-label port-number" style={labelStyles}>
                      {pval[0]}
                    </div>
                  ) : null}
                </div>
              );
            }
          }

          return false;
        })}
        {ledConfig.map((ledData, index) => {
          let eachValue = ledData.split("/");
          let number = parseInt(eachValue[0], 10),
            src = eachValue[1],
            left = parseInt(eachValue[2], 10),
            top = parseInt(eachValue[3], 10),
            width = parseInt(eachValue[4], 10),
            height = parseInt(eachValue[5], 10);

          if (src !== "none" && src !== undefined) {
            let ledStyles = {
              left,
              top,
              width,
              height
            };
            return (
              <span key={index}>
                <img
                  src={`/media/images/${src}`}
                  alt={`LED_${number}`}
                  id={`led_${number}`}
                  style={ledStyles}
                  className="led-status"
                />
              </span>
            );
          }

          return false;
        })}
      </div>
    );
  }

  processUpdate() {
    const { data } = this.props;
    let content = "";
    let fields = [];
    if (data) {
      // parse alert
      fields = data.split(";");

      /* display alerts */
      if (fields[1]) window.alert(fields[1]);

      const switchData = fields[0].split("#")[0];
      // LNTN LED Status
      const status = fields[0].split("#")[1];
      const ledStatus = status.split(",")[0].split("|");

      if (switchData && ledStatus) {
        content = this.addSwitch(switchData, ledStatus);
        return content;
      }

      return "";
    }

    return "";
  }

  resetToZero = () => !mql.matches && this.setState({ update: 0 });

  toggleBtn = () => {
    if (mql.matches) this.setState({ isMylarOpen: true, toggleMylarBtnDisplay: false, update: 0 });
  };

  handleClickOutsideMylar = e => {
    if (this.node && !this.node.contains(e.target) && mql.matches)
      this.setState({ isMylarOpen: false, toggleMylarBtnDisplay: true });
  };

  componentDidUpdate(prevProps) {
    if (this.state.isMylarOpen) {
      document.addEventListener("mousedown", this.handleClickOutsideMylar);
    } else {
      document.removeEventListener("mousedown", this.handleClickOutsideMylar);
    }

    if (JSON.stringify(this.props) !== JSON.stringify(prevProps) && !this.state.isMylarOpen && mql.matches) {
      this.setState(prevState => {
        return { update: prevState.update + 1 };
      });
    }
  }

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  componentDidMount() {
    window.addEventListener("resize", _.debounce(this.resetToZero, 300));
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.resetToZero);
  }

  render() {
    const { isMylarOpen, toggleMylarBtnDisplay, update } = this.state;
    const content = () => this.processUpdate();

    return (
      <React.Fragment>
        <div
          className={`switch-monitor-wrapper ${isMylarOpen ? "open" : ""}`}
          ref={node => (this.node = node)}
        >
          <div className="panel-container">
            {/* eslint-disable-next-line no-undef */}
            <div className="switch-name-outter">{configSwitchName}</div>
            <div className="panel">
              {/* .switch-name-inner only display below $desketopPortrait width*/}
              {/* eslint-disable-next-line no-undef */}
              <p className="switch-name-inner">{configSwitchName}</p>
              {content()}
            </div>
          </div>
        </div>
        <div className={`mylar-toggle-btn ${toggleMylarBtnDisplay ? "" : "close"}`} onClick={this.toggleBtn}>
          <Icon name="hdd" />
          {update !== 0 && <Info>{update}</Info>}
        </div>
        <CustomDimmer isMylarOpen={isMylarOpen} />
      </React.Fragment>
    );
  }
}

const Info = styled.div`
  position: absolute;
  min-width: 2em;
  min-height: 2em;
  text-align: center;
  top: -6px;
  left: -11px;
  background-color: #db2828;
  color: #fff;
  padding: 0.5em;
  line-height: 1em;
  border-radius: 500rem;
  transition: background 0.1s ease;
  font-weight: 700;
  font-size: 12px;
`;

export default WithData(DeviceMonitor, "/stat/portstate", true);
