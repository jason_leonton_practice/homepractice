import React, { memo, useState, useRef, useEffect } from "react";
import ResizeObserver from "resize-observer-polyfill";
import { NavLink } from "react-router-dom";
import { Icon } from "semantic-ui-react";
import { useSpring, a, animated } from "react-spring";
import styled from "styled-components";

/*
  Sidebar menu is inspired by react-spring
  [ref]: https://www.react-spring.io/docs/hooks/examples
         https://codesandbox.io/embed/lp80n9z7v9
*/

const Frame = styled("div")`
  position: relative;
  padding: 4px 0px 0px 0px;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow-x: hidden;
  vertical-align: middle;
  color: white;
  fill: white;
  overflow-y: hidden;
`;

const Content = styled(animated.div)`
  will-change: transform, opacity, height;
  margin-left: 6px;
  padding: 0px 0px 0px 14px;
  border-left: 1px dashed rgba(255, 255, 255, 0.4);
  overflow: hidden;
`;

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => void (ref.current = value), [value]);
  return ref.current;
}

function useMeasure() {
  const ref = useRef();
  const [bounds, set] = useState({ left: 0, top: 0, width: 0, height: 0 });
  const [ro] = useState(() => new ResizeObserver(([entry]) => set(entry.contentRect)));
  useEffect(() => {
    if (ref.current) ro.observe(ref.current);
    return () => ro.disconnect();
  }, [ro]);
  return [{ ref }, bounds];
}

export const Tree = memo(({ children, name, url, level, hideSidebar }) => {
  const [isOpen, setOpen] = useState(false);
  const previous = usePrevious(isOpen);
  const [bind, { height: viewHeight }] = useMeasure();
  const { height, opacity, transform } = useSpring({
    from: { height: 0, opacity: 0, transform: "translate3d(10px,0,0)" },
    to: {
      opacity: isOpen ? 1 : 0,
      height: isOpen ? viewHeight : 0,
      transform: `translate3d(${isOpen ? 0 : 20}px,0,0)`
    }
  });

  return (
    <Frame className="treeview">
      <div onClick={() => setOpen(!isOpen)} className={`submenu ${level}`}>
        {children ? (
          <span>
            {name}
            <Icon name="caret down" className={`caret ${isOpen ? "active" : ""}`} />
          </span>
        ) : (
          <NavLink to={url} exact className="nav-link" onClick={() => hideSidebar()}>
            {name}
          </NavLink>
        )}
      </div>
      <Content style={{ opacity, height: isOpen && previous === isOpen ? "auto" : height }}>
        <a.div style={{ transform }} {...bind} children={children} />
      </Content>
    </Frame>
  );
});

export default Tree;
