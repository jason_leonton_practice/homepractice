import React from "react";
import { NavLink } from "react-router-dom";

import store from "../store";
import { sidebarToggleAction } from "../store/actionCreators";
import openModuleAsync from "../utils/modules_tree";
import Tree from "./tree";
import "./sidebar.scss";

const mqSmallScreen = window.matchMedia(`(max-width: 1024px)`);
const mqBigScreen = window.matchMedia(`(min-width: 1024px)`);

class SidebarWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = { redux: store.getState(), content: "" };

    store.subscribe(this.handleStoreChange);
  }

  handleStoreChange = () => this.setState({ redux: store.getState() });

  generateMenu(children = []) {
    const self = this;
    return children.map(function m({ children, name, url, level, id, visible } = {}) {
      if (typeof visible !== "undefined" && !visible) return false;

      // level-1 menu items always display on screen
      if (level === "level-1") {
        return (
          <li key={id} className={level}>
            {url ? (
              <NavLink to={url} exact className="nav-link" onClick={self.hideSidebar}>
                <div className="level-1-title">{name}</div>
              </NavLink>
            ) : (
              <div className="level-1-title">{name}</div>
            )}
            {children ? children.map(m) : null}
          </li>
        );
      }

      return (
        <Tree key={id} name={name} url={url} level={level} hideSidebar={self.hideSidebar}>
          {children ? children.map(m) : null}
        </Tree>
      );
    });
  }

  handleClickOutsideSidebar = e => {
    if (this.node && !this.node.contains(e.target)) {
      this.hideSidebar();
    }
  };

  hideSidebar = () => {
    if (mqSmallScreen.matches && this.state.redux.isSidebarOpen) {
      const action = sidebarToggleAction(false);
      store.dispatch(action);
    }
  };

  browserWidthChanged = () => {
    if (mqSmallScreen.matches) {
      const action = sidebarToggleAction(false);
      store.dispatch(action);
    } else if (mqBigScreen) {
      const action = sidebarToggleAction(true);
      store.dispatch(action);
    }
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  componentDidUpdate() {
    if (this.state.redux.isSidebarOpen && mqSmallScreen.matches) {
      document.addEventListener("mousedown", this.handleClickOutsideSidebar);
    } else {
      document.removeEventListener("mousedown", this.handleClickOutsideSidebar);
    }
  }

  componentDidMount() {
    if (mqSmallScreen.matches) {
      const action = sidebarToggleAction(false);
      store.dispatch(action);
    }

    mqSmallScreen.addListener(this.browserWidthChanged);
    mqBigScreen.addListener(this.browserWidthChanged);

    openModuleAsync()
      .then(data => this.generateMenu(data))
      .then(content => this.setState({ routes: content }));
  }

  componentWillUnmount() {
    mqSmallScreen.removeListener(this.browserWidthChanged);
    mqBigScreen.removeListener(this.browserWidthChanged);
  }

  render() {
    return (
      <nav className="sidebar-menu" ref={node => (this.node = node)}>
        <div className="logo-container">
          <img src="/media/images/Leonton_Logo_px-03.png" style={{ width: "100%" }} alt="antaria-logo" />
        </div>
        <ul className="nav-container">{this.state.routes}</ul>
      </nav>
    );
  }
}

export default SidebarWrapper;
