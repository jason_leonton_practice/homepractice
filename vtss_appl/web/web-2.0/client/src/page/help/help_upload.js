import React from "react";
import { Message, Item, Button, Table } from "semantic-ui-react";

export default function HelpUpload() {
  return (
    <div>
      <Message
        icon="help"
        header="Maintenance Help - Software Upload"
        content="This page facilitates an update of the firmware controlling the switch."
        color="yellow"
      />

      <Item.Group divided>
        <Item>
          <Table basic="very" celled collapsing>
            <Table.Body>
              <Table.Row>
                <Table.Cell>
                  <Button>Brwoser</Button>
                </Table.Cell>
                <Table.Cell>to the location of a software image</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <Button>Upload</Button>
                </Table.Cell>
                <Table.Cell>to upload the software image</Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </Item>
        <Item>
          After the software image is uploaded, a page announces that the firmware update is initiated. After
          about a minute, the firmware is updated and the switch restarts.
        </Item>
        <Item>
          <Message
            error
            header="Warning"
            list={[
              "While the firmware is being updated, Web access appears to be defunct. The front LED flashes Green/Off with a frequency of 10 Hz while the firmware update is in progress.",
              "Do not restart or power off the device at this time or the switch may fail to function afterwards."
            ]}
          />
        </Item>
      </Item.Group>
    </div>
  );
}
