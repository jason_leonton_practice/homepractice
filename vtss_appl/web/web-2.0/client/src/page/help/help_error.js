import React from "react";
import { NavLink } from "react-router-dom";
import { Message } from "semantic-ui-react";

function HelpError() {
  return (
    <div>
      <Message icon="help" header="Error Help" content="this page doesn't exist" color="red" />
      <NavLink to="/">Redirect back to dashboard page</NavLink>
    </div>
  );
}

export default HelpError;
