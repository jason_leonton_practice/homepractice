import React from "react";
import { Message, Item } from "semantic-ui-react";

function HelpDashboard() {
  return (
    <div>
      <Message
        icon="help"
        header="Dashboard Help"
        content="The dashboard information is provided in here"
        color="yellow"
      />
      <Item.Group divided>
        <Item>
          <Item.Content>
            <Item.Header>System Contact</Item.Header>
            <Item.Description>
              The textual identification of the contact person for this managed node, together with
              information on how to contact this person. The allowed string length is 0 to 255, and the
              allowed content is the ASCII characters from 32 to 126.
            </Item.Description>
          </Item.Content>
        </Item>
        <Item>
          <Item.Content>
            <Item.Header>System Name</Item.Header>
            <Item.Description>
              An administratively assigned name for this managed node. By convention, this is the node's
              fully-qualified domain name. A domain name is a text string drawn from the alphabet (A-Za-z),
              digits (0-9), minus sign (-). No space characters are permitted as part of a name. The first
              character must be an alpha character. And the first or last character must not be a minus sign.
              The allowed string length is 0 to 255.
            </Item.Description>
          </Item.Content>
        </Item>
        <Item>
          <Item.Content>
            <Item.Header>System Location</Item.Header>
            <Item.Description>
              The physical location of this node(e.g., telephone closet, 3rd floor). The allowed string length
              is 0 to 255, and the allowed content is the ASCII characters from 32 to 126.
            </Item.Description>
          </Item.Content>
        </Item>
      </Item.Group>
    </div>
  );
}

export default HelpDashboard;
