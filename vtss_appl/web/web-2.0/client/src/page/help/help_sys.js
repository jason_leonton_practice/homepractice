import React from "react";
import { Message, Item } from "semantic-ui-react";
// import "./help-normal.scss";

function HelpSys() {
  return (
    <div>
      <Message
        icon="help"
        header="System Information Help"
        content="The switch system information is provided here."
        color="yellow"
      />
      <Item.Group divided>
        <Item>
          <Item.Content>
            <Item.Header>Contact</Item.Header>
            <Item.Description>
              The system contact configured in Configuration | System | Information | System Contact.
            </Item.Description>
          </Item.Content>
        </Item>
        <Item>
          <Item.Content>
            <Item.Header>Name</Item.Header>
            <Item.Description>
              The system name configured in Configuration | System | Information | System Name.
            </Item.Description>
          </Item.Content>
        </Item>
        <Item>
          <Item.Content>
            <Item.Header>Location</Item.Header>
            <Item.Description>
              The system location configured in Configuration | System | Information | System Location.
            </Item.Description>
          </Item.Content>
        </Item>
        <Item>
          <Item.Content>
            <Item.Header>MAC Address</Item.Header>
            <Item.Description>The MAC Address of this switch.</Item.Description>
          </Item.Content>
        </Item>
        <Item>
          <Item.Content>
            <Item.Header>Chip ID</Item.Header>
            <Item.Description>The Chip ID of this switch.</Item.Description>
          </Item.Content>
        </Item>
        <Item>
          <Item.Content>
            <Item.Header>System Date</Item.Header>
            <Item.Description>
              The current (GMT) system time and date. The system time is obtained through the Timing server
              running on the switch, if any.
            </Item.Description>
          </Item.Content>
        </Item>
        <Item>
          <Item.Content>
            <Item.Header>System Uptime</Item.Header>
            <Item.Description>The period of time the device has been operational</Item.Description>
          </Item.Content>
        </Item>
        <Item>
          <Item.Content>
            <Item.Header>Software Version</Item.Header>
            <Item.Description>The software version of this switch.</Item.Description>
          </Item.Content>
        </Item>
        <Item>
          <Item.Content>
            <Item.Header>Software Date</Item.Header>
            <Item.Description>he date when the switch software was produced.</Item.Description>
          </Item.Content>
        </Item>
      </Item.Group>
    </div>
  );
}

export default HelpSys;
