import React from "react";
import { Message, Icon } from "semantic-ui-react";
import { Link } from "react-router-dom";

import { LoadingSegment } from "../components/loading_segment";

// since we generate route asynchronous,
// the user will reach the error page every time typing the URL or refresh page.
// Therefore, I add a loader waiting for route complete generating.

export default function Error({ loading }) {
  return loading ? (
    <LoadingSegment />
  ) : (
    <Message icon color="brown" size="big">
      <Icon name="search" />
      <Message.Content>
        <Message.Header>Page Not Found</Message.Header>
        <Message.List>
          <Message.Item>We can't find the page your're looking for.</Message.Item>
          <Message.Item>
            You can either return to the previous page, back <Link to="/">dashboard</Link> or contact our
            support team.
          </Message.Item>
        </Message.List>
      </Message.Content>
    </Message>
  );
}
