import React, { Component } from "react";
import { Segment, Form, Select, Input, Divider, Pagination, Icon } from "semantic-ui-react";
import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { HeaderRefreshBar, SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { pageLimitOptions } from "../../utils/options";
import { toMacAddress } from "../../utils/format";
import { isMacAddress } from "../../utils/validate";
import WithData from "../../utils/withData";
import styled from "styled-components";

class DynaMac extends Component {
  state = {
    isAutoRefresh: false,
    currentPosts: [],
    tableHeaderSecRow: [],
    totalPosts: 0,
    currentPage: 1,
    startFromVlan: 1,
    // eslint-disable-next-line no-undef
    portCount: configNormalPortMax,
    startFromMac: "00-00-00-00-00-00",
    entriesPerPage: 20
  };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  onChangeStartFromVlan = (e, { name, value }) => {
    //限制只能輸入數字，且須大於0
    let textValue = value.replace(/^[0]|[^\d]/g, "");
    if (value !== this.state.startFromVlan) {
      this.handlePagination({
        [name]: textValue.trim(),
        currentPage: 1
      });
    }
  };

  onChangeStartFromMacAddress = (e, { name, value }) => {
    value = value.trim();
    if (value !== this.state.startFromMac) {
      this.handlePagination({ [name]: value, currentPage: 1 });
    }
  };

  onChangeEntriesPerPage = (e, { name, value }) => {
    this.handlePagination({ [name]: value, currentPage: 1 });
  };

  onChangePage = (event, { activePage }) => {
    if (activePage !== this.state.currentPage) {
      this.handlePagination({ currentPage: activePage });
    }
  };

  handleClear = () => {
    this.props.querySearch(`/config/dynamic_mac_table?Flush=1&sid=-1`, false);
  };

  IsHex = (Value, AlertIsOn) => {
    // Default value
    let AlertOn = AlertIsOn == null ? 0 : AlertIsOn;
    let ValueIsHex = Value.match(/^[0-9a-f]+$/i);

    if (!ValueIsHex && AlertOn) {
      alert("Value " + Value + " is not a valid hex number");
    }

    return ValueIsHex;
  };

  //篩選出mac address 大於 Start from MAC address的條件
  filterMac = (datumMac, macArray) => {
    return macArray.filter((macList, i) => {
      const datumMacArray = this.macAddressToDecimal(datumMac);
      const newMacArray = this.macAddressToDecimal(macList);
      let bool = false;
      for (let j = 0; j < datumMacArray.length; j++) {
        if (datumMacArray[j] < newMacArray[j]) {
          bool = true;
          break;
        }
        if (datumMacArray[j] > newMacArray[j]) {
          bool = false;
          break;
        }
        if (j === datumMacArray.length - 1 && datumMacArray[j] <= newMacArray[j]) {
          bool = true;
          break;
        }
      }
      return bool ? macList : false;
    });
  };

  //將mac轉換成十進制並以陣列封裝
  macAddressToDecimal = macAddress => {
    let decimal_macAddress_array = macAddress.split("-").map((onceMac, i, newList) => {
      return parseInt(`0x${onceMac}`, 16);
    });

    return decimal_macAddress_array;
  };

  filterRepeat(resultObj) {
    let repeat = []; //重複的項目即為AND過濾後的結果
    for (let i = 0; i < Object.keys(resultObj).length; i++) {
      let resultKey = Object.keys(resultObj)[i];
      let resultNextKey = Object.keys(resultObj)[i + 1];

      if (i === 0) {
        repeat = resultObj[resultKey]
          .concat(resultObj[resultNextKey])
          .map(item => {
            return JSON.stringify(item);
          })
          .filter((item, j, arr) => {
            return arr.indexOf(item) !== j;
          })
          .map(item => {
            return JSON.parse(item);
          });
      } else {
        if (resultNextKey) {
          repeat = repeat
            .concat(resultObj[resultNextKey])
            .map(item => {
              return JSON.stringify(item);
            })
            .filter((item, j, arr) => {
              return arr.indexOf(item) !== j;
            })
            .map(item => {
              return JSON.parse(item);
            });
        }
      }
    }
    return repeat;
  }
  // 處理當前頁面該顯示的post 以及 經過fileter後的總post數
  handlePagination = (params = {}) => {
    const newState = { ...this.state, ...params };
    const macconfig = this.props.data.split("|");
    const tbodyDataRows = macconfig.slice(1).filter(list => {
      return list.length > 0;
    });
    const vlanRows = tbodyDataRows.map(list => {
      return list.split("/")[1];
    });
    const macAddressRows = tbodyDataRows.map(list => {
      return list.split("/")[0];
    });

    let filterVlanIndexArray = [];
    let filterMacIndexArray = [];
    let filetered = newState.tableBodyRows; //實際tbody會填入的HTML結果

    /*vlan 過濾*/
    if (!newState.startFromVlan) {
      vlanRows.forEach((vlan, index) => {
        filterVlanIndexArray.push(index);
      });
    } else {
      vlanRows.forEach((vlan, index) => {
        if (parseInt(newState.startFromVlan) <= vlan) {
          filterVlanIndexArray.push(index);
        }
      });
    }

    /*mac address 過濾*/
    const matchMacs = this.filterMac(newState.startFromMac, macAddressRows); //篩選出start from mac address 之後的mac
    if (!isMacAddress(newState.startFromMac) || newState.startFromMac === "00-00-00-00-00-00") {
      newState.tableBodyRows.forEach((tbodyRow, i) => {
        filterMacIndexArray.push(i);
      });
    } else {
      macAddressRows.forEach((mac, macIndex) => {
        for (let matchMacIndex = 0; matchMacIndex < matchMacs.length; matchMacIndex++) {
          const matchMac = matchMacs[matchMacIndex];
          if (matchMac === mac) {
            filterMacIndexArray.push(macIndex);
            break;
          }
        }
      });
    }

    //綜合過濾結果
    let resultObj = {
      filterVlanIndexArray,
      filterMacIndexArray
    };
    let repeatArray = this.filterRepeat(resultObj);
    filetered = filetered.filter((tobdyRow, i) => {
      return repeatArray.indexOf(i) >= 0;
    });

    // Get current post
    const indexOfLastPost = newState.currentPage * newState.entriesPerPage;
    const indexOfFirstPost = indexOfLastPost - newState.entriesPerPage;
    let currentPosts = filetered.slice(indexOfFirstPost, indexOfLastPost);

    this.setState({ ...newState, currentPosts, totalPosts: filetered.length });
  };

  createTableBodyRows = (portCount, mac_infos) => {
    const pval = mac_infos[0].split("/");
    const sr_support = pval[2];
    const psfp_support = pval[3];

    let tableBodyRows = [];
    let bodyRowsData = mac_infos.slice(1);

    if (bodyRowsData.length < 1) {
      tableBodyRows.push(
        <Table.Row key="no_data">
          <Table.Cell colSpan={portCount + 1} textAlign="center">
            No data exists for the selected user
          </Table.Cell>
        </Table.Row>
      );
    } else {
      let type, vlan, macAddress;
      bodyRowsData.forEach((rowData, i) => {
        if (rowData.length > 0) {
          const tdsData = rowData.split("/");
          tdsData.forEach((tdData, i) => {
            if (i < 3) {
              switch (i) {
                case 0:
                  macAddress = toMacAddress(tdData);
                  break;
                case 1:
                  vlan = tdData;
                  break;
                case 2:
                  type = tdData === "1" ? "Static" : "Dynamic";
                  break;
                default:
                  break;
              }
            }
          });
          const status = type => {
            switch (type) {
              case 1:
                return "check";
              default:
                return "";
            }
          };
          //1是因CPU，3是因type vlan mac
          const portMembersRow = tdsData.slice(3, portCount + 1 + 3);
          tableBodyRows.push(
            <Table.Row key={`tbodyRow_${i}`} textAlign="center">
              <Table.Cell>{type}</Table.Cell>
              <Table.Cell>{vlan}</Table.Cell>
              <Table.Cell>{macAddress}</Table.Cell>
              {portMembersRow.map((portMember, i) => {
                return (
                  <Table.Cell key={`portMember_${i}`}>
                    <StatusIcon name={status(parseInt(portMember))} circular />
                  </Table.Cell>
                );
              })}
              {sr_support !== "0" && <Table.Cell>{tdsData[portMembersRow.length + 3]}</Table.Cell>}
              {psfp_support !== "0" && <Table.Cell>{tdsData[portMembersRow.length + 3 + 1]}</Table.Cell>}
            </Table.Row>
          );
        }
      });
    }
    this.setState(
      {
        tableBodyRows: tableBodyRows
      },
      () => {
        this.handlePagination();
      }
    );
  };

  createTableHeader = (portCount, mac_infos) => {
    const pval = mac_infos[0].split("/");
    const sr_support = pval[2];
    const psfp_support = pval[3];

    let tableHeaderSecRow = [];
    let staticTh = ["Type", "VLAN", "MAC Address", "CPU"];

    staticTh.forEach(thData => {
      tableHeaderSecRow.push(<Table.HeaderCell key={`theadth_${thData}`}>{thData}</Table.HeaderCell>);
    });
    for (let i = 0; i < portCount; i++) {
      tableHeaderSecRow.push(
        <Table.HeaderCell key={`portnumber_${i}`} textAlign="center">
          {i + 1}
        </Table.HeaderCell>
      );
    }
    if (sr_support !== "0") {
      tableHeaderSecRow.push(<Table.HeaderCell key={`sr_support`}>SR ID</Table.HeaderCell>);
    }
    if (psfp_support !== "0") {
      tableHeaderSecRow.push(<Table.HeaderCell key={`psfp_support`}>PSFP ID</Table.HeaderCell>);
    }
    const _tableHead = (
      <React.Fragment>
        <Table.Row textAlign="center">
          <Table.HeaderCell colSpan={3} />
          {/* +1 為包含CPU欄位 */}
          <Table.HeaderCell colSpan={portCount + 1}>Port Members</Table.HeaderCell>
          {sr_support !== "0" && <Table.HeaderCell />}
          {psfp_support !== "0" && <Table.HeaderCell />}
        </Table.Row>
        <Table.Row textAlign="center">{tableHeaderSecRow}</Table.Row>
      </React.Fragment>
    );
    this.setState({
      tableHeadRows: _tableHead
    });
  };
  createMacTable = mac_infos => {
    const { portCount } = this.state;

    this.createTableHeader(portCount, mac_infos);
    this.createTableBodyRows(portCount, mac_infos);
  };

  processUpdate = () => {
    const macconfig = this.props.data.split("|");
    this.createMacTable(macconfig);
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  componentDidUpdate(prevProps) {
    if (JSON.stringify(this.props) !== JSON.stringify(prevProps)) this.processUpdate();
  }

  componentDidMount() {
    this.processUpdate();
  }

  render() {
    const { tableHeadRows, currentPosts, portCount, currentPage, totalPosts, entriesPerPage } = this.state;
    const { readOnly } = this.props;
    return (
      <Segment.Group>
        <SegmentHeader>
          MAC Address Table
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <FormFeature
            featureOption={{
              state: { ...this.state },
              event: {
                onChangeStartFromVlan: this.onChangeStartFromVlan,
                onChangeStartFromMacAddress: this.onChangeStartFromMacAddress,
                onChangeEntriesPerPage: this.onChangeEntriesPerPage
              }
            }}
          />

          <Divider />

          <Table celled compact>
            <Table.Header>{tableHeadRows}</Table.Header>
            <Table.Body>{currentPosts}</Table.Body>
            <Table.Footer>
              <TablePagination
                portCount={portCount}
                currentPage={currentPage}
                totalPosts={totalPosts}
                entriesPerPage={entriesPerPage}
                onChangePage={this.onChangePage}
              />
            </Table.Footer>
          </Table>
          <BtnWrapper>
            <SemanticButton title="clear" disabled={readOnly} onClick={this.handleClear} />
          </BtnWrapper>
        </Segment>
      </Segment.Group>
    );
  }
}

/*function component*/
function FormFeature(props) {
  const { state, event } = props.featureOption;
  return (
    <Form size="mini">
      <Form.Group>
        <Form.Field
          width="4"
          control={Input}
          name="startFromVlan"
          label="Start from VLAN"
          value={state.startFromVlan}
          autoComplete="off"
          onChange={event.onChangeStartFromVlan}
        />
        <Form.Field
          width="4"
          maxLength="17"
          control={Input}
          name="startFromMac"
          label="Start from MAC address"
          value={state.startFromMac}
          autoComplete="off"
          onChange={event.onChangeStartFromMacAddress}
        />
        <Form.Field
          width="4"
          compact
          control={Select}
          name="entriesPerPage"
          value={state.entriesPerPage}
          options={pageLimitOptions}
          onChange={event.onChangeEntriesPerPage}
          label={{ children: "Entries per page" }}
        />
      </Form.Group>
    </Form>
  );
}

function TablePagination(props) {
  const { portCount, currentPage, totalPosts, entriesPerPage } = props;
  const { onChangePage } = props; //event
  return (
    <Table.Row textAlign="right">
      <Table.HeaderCell colSpan={portCount + 1 + 3}>
        <Pagination
          activePage={currentPage}
          totalPages={Math.ceil(totalPosts / entriesPerPage)}
          size="mini"
          onPageChange={onChangePage}
          firstItem={{ content: <Icon name="angle double left" />, icon: true }}
          lastItem={{ content: <Icon name="angle double right" />, icon: true }}
          prevItem={{ content: <Icon name="angle left" />, icon: true }}
          nextItem={{ content: <Icon name="angle right" />, icon: true }}
        />
      </Table.HeaderCell>
    </Table.Row>
  );
}

const StatusIcon = styled(Icon)`
  margin-right: 0 !important;
  &.check {
    color: #21ba45 !important;
  }
  &.x,
  &.exclamation.triangle {
    color: #db2828 !important;
  }
`;
export default WithData(DynaMac, "/config/dynamic_mac_table", false);
