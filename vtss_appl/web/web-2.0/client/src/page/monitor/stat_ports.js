import React, { Component } from "react";
import { Segment } from "semantic-ui-react";
import { NavLink } from "react-router-dom";

import WithData from "../../utils/withData";
import { HeaderRefreshBar, SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { InnerTable as Table, SegmentHeader } from "../../components/styled_component";

const doesInitialAutoRefresh = false;

class StatPort extends Component {
  constructor(props) {
    super(props);
    this.state = {
      btnLoading: false,
      isAutoRefresh: doesInitialAutoRefresh
    };
  }

  handleClear = () => {
    this.props.querySearch("/stat/ports?clear=1", false);
  };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    const values = this.props.data.split("|");
    const content = values.map((value, index) => {
      const data = value.split("/");
      const [
        port,
        packetsRec,
        packetsTran,
        bytesRec,
        bytesTran,
        errorsRec,
        errorsTran,
        dropsRec,
        dropsTran,
        filteredRec
      ] = [...data];

      return (
        <Table.Row key={index}>
          <Table.Cell>
            <NavLink to={`/monitor/ports/detailed_statistics?port=${port}`}>{port}</NavLink>
          </Table.Cell>
          <Table.Cell>{packetsRec}</Table.Cell>
          <Table.Cell>{packetsTran}</Table.Cell>
          <Table.Cell>{bytesRec}</Table.Cell>
          <Table.Cell>{bytesTran}</Table.Cell>
          <Table.Cell>{errorsRec}</Table.Cell>
          <Table.Cell>{errorsTran}</Table.Cell>
          <Table.Cell>{dropsRec}</Table.Cell>
          <Table.Cell>{dropsTran}</Table.Cell>
          <Table.Cell>{filteredRec}</Table.Cell>
        </Table.Row>
      );
    });
    return (
      <Segment.Group>
        <SegmentHeader>
          Port Statistics Overview
          <HeaderRefreshBar checked={this.state.isAutoRefresh} handleAutoRefresh={this.handleAutoRefresh} />
        </SegmentHeader>
        <Segment>
          <Table celled structured striped compact>
            <Table.Header>
              <Table.Row textAlign="center">
                <Table.HeaderCell rowSpan="2">Port</Table.HeaderCell>
                <Table.HeaderCell colSpan="2">Packets</Table.HeaderCell>
                <Table.HeaderCell colSpan="2">Bytes</Table.HeaderCell>
                <Table.HeaderCell colSpan="2">Erros</Table.HeaderCell>
                <Table.HeaderCell colSpan="2">Drops</Table.HeaderCell>
                <Table.HeaderCell>Filtered</Table.HeaderCell>
              </Table.Row>
              <Table.Row textAlign="center">
                <Table.HeaderCell>Received</Table.HeaderCell>
                <Table.HeaderCell>Transmmited</Table.HeaderCell>
                <Table.HeaderCell>Received</Table.HeaderCell>
                <Table.HeaderCell>Transmmited</Table.HeaderCell>
                <Table.HeaderCell>Received</Table.HeaderCell>
                <Table.HeaderCell>Transmmited</Table.HeaderCell>
                <Table.HeaderCell>Received</Table.HeaderCell>
                <Table.HeaderCell>Transmmited</Table.HeaderCell>
                <Table.HeaderCell>Received</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{content}</Table.Body>
          </Table>
          <BtnWrapper>
            <SemanticButton title="clear" disabled={this.props.readOnly} onClick={this.handleClear} />
          </BtnWrapper>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(StatPort, "/stat/ports", false);
