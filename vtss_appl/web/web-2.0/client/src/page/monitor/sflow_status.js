import React, { Component } from "react";
import { Segment, TableRow, Divider } from "semantic-ui-react";

import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";
import { decodeURIComponentSafe } from "../../utils/self-documenting_func";

let isAutoRefresh = false;

let PARM_IDX_RCVR_OWNER = 0;
let PARM_IDX_RCVR_TIMEOUT = 1;
let PARM_IDX_RCVR_HOSTNAME = 2;
let PARM_IDX_RCVR_OK_DATAGRAM_CNT = 3;
let PARM_IDX_RCVR_ERR_DATAGRAM_CNT = 4;
let PARM_IDX_RCVR_FLOW_SAMPLE_CNT = 5;
let PARM_IDX_RCVR_COUNTER_SAMPLE_CNT = 6;
let PARM_RCVR_CNT = PARM_IDX_RCVR_COUNTER_SAMPLE_CNT + 1;

var PARM_IDX_PORT_NUMBER = 0;
var PARM_IDX_PORT_TX_FLOW_SAMPLE_CNT = 1;
var PARM_IDX_PORT_COUNTER_SAMPLE_CNT = 2;
var PARM_PORT_CNT = PARM_IDX_PORT_COUNTER_SAMPLE_CNT + 1;

class SflowStatus extends Component {
  state = { isAutoRefresh };

  addReceiver = rcvr_values => {
    let rcvr_stat_table_body = [];
    let owner_str = decodeURIComponentSafe(rcvr_values[PARM_IDX_RCVR_OWNER]);

    if (owner_str.length === 0) {
      owner_str = "<none>";
    }

    rcvr_stat_table_body.push(
      <React.Fragment key="rcvr_stat_table_body">
        <Table.Row>
          <Table.Cell>Owner</Table.Cell>
          <Table.Cell>{owner_str}</Table.Cell>
        </Table.Row>
        <Table.Row>
          {/* eslint-disable-next-line no-undef */}
          <Table.Cell>{`IP Address ${configIPDNSSupport && "/Hostname"}`}</Table.Cell>
          <Table.Cell>{decodeURIComponentSafe(rcvr_values[PARM_IDX_RCVR_HOSTNAME])}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Timeout</Table.Cell>
          <Table.Cell>{rcvr_values[PARM_IDX_RCVR_TIMEOUT]}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Tx Successes</Table.Cell>
          <Table.Cell>{rcvr_values[PARM_IDX_RCVR_OK_DATAGRAM_CNT]}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Tx Errors</Table.Cell>
          <Table.Cell>{rcvr_values[PARM_IDX_RCVR_ERR_DATAGRAM_CNT]}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Flow Samples</Table.Cell>
          <Table.Cell>{rcvr_values[PARM_IDX_RCVR_FLOW_SAMPLE_CNT]}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Counter Samples</Table.Cell>
          <Table.Cell>{rcvr_values[PARM_IDX_RCVR_COUNTER_SAMPLE_CNT]}</Table.Cell>
        </Table.Row>
      </React.Fragment>
    );

    return rcvr_stat_table_body;
  };

  clearRcvr = () => {
    this.props.querySearch("/stat/sflow_status?clear=1", false);
  };

  clearPorts = () => {
    this.props.querySearch("/stat/sflow_status?port=0", false);
  };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    let rcvr_stat_table_body = [];
    let port_stat_table_body = [];

    if (this.props.data) {
      let values = this.props.data.split("#");
      let rcvr_values = values[0].split("/");

      if (rcvr_values.length === PARM_RCVR_CNT) {
        rcvr_stat_table_body = this.addReceiver(rcvr_values);
      }

      for (let i = 1; i < values.length; i++) {
        let port_values = values[i].split("/");

        if (port_values.length === PARM_PORT_CNT) {
          port_stat_table_body.push(
            <TableRow key={`port_stat_table_row_${i}`}>
              <Table.Cell textAlign="center">{parseInt(port_values[PARM_IDX_PORT_NUMBER], 10)}</Table.Cell>
              <Table.Cell>{port_values[PARM_IDX_PORT_TX_FLOW_SAMPLE_CNT]}</Table.Cell>
              <Table.Cell>{port_values[PARM_IDX_PORT_COUNTER_SAMPLE_CNT]}</Table.Cell>
            </TableRow>
          );
        }
      }
    }

    return (
      <Segment.Group>
        <SegmentHeader>
          sFlow Statistics
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <h4>Receiver Statistics</h4>
          <Table definition compact unstackable celled textAlign="left">
            <Table.Body>{rcvr_stat_table_body}</Table.Body>
          </Table>
          <BtnWrapper>
            <SemanticButton title="Clear Receiver" disabled={this.props.readOnly} onClick={this.clearRcvr} />
          </BtnWrapper>
          <Divider />
          <h4>Port Statistics</h4>
          <Table striped unstackable compact celled textAlign="right">
            <Table.Header>
              <Table.Row textAlign="center">
                <Table.HeaderCell>Port</Table.HeaderCell>
                <Table.HeaderCell>Flow Samples</Table.HeaderCell>
                <Table.HeaderCell>Counter Samples</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{port_stat_table_body}</Table.Body>
          </Table>
          <BtnWrapper>
            <SemanticButton title="Clear Ports" disabled={this.props.readOnly} onClick={this.clearPorts} />
          </BtnWrapper>
        </Segment>
      </Segment.Group>
    );
  }
}
export default WithData(SflowStatus, "/stat/sflow_status", isAutoRefresh);
