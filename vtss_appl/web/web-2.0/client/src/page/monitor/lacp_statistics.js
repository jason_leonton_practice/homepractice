import React, { Component } from "react";
import { Segment } from "semantic-ui-react";

import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";

const isAutoRefresh = false;

class LacpStatistics extends Component {
  state = { isAutoRefresh };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    let tableBody = [];
    let rows = 0;

    if (this.props.data) {
      let values = this.props.data.split("|");
      values.forEach((value, i) => {
        if (value.length) {
          let pval = value.split("/");
          rows++;

          tableBody.push(
            <Table.Row key={"table_row_" + i}>
              <Table.Cell>{pval[0]}</Table.Cell>
              <Table.Cell>{pval[1]}</Table.Cell>
              <Table.Cell>{pval[2]}</Table.Cell>
              <Table.Cell>{pval[3]}</Table.Cell>
              <Table.Cell>{pval[4]}</Table.Cell>
            </Table.Row>
          );
        }
      });
    }

    if (rows === 0) {
      tableBody.push(
        <Table.Row key="table_row_no_ports_enabled">
          <Table.Cell colSpan="5">No ports enabled</Table.Cell>
        </Table.Row>
      );
    }

    return (
      <Segment.Group>
        <SegmentHeader>
          LACP Statistics
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Table striped unstackable celled compact textAlign="center" structured>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell rowSpan="2">Port</Table.HeaderCell>
                <Table.HeaderCell rowSpan="2">
                  LACP <br></br>Received
                </Table.HeaderCell>
                <Table.HeaderCell rowSpan="2">
                  LACP <br></br>Transmitted
                </Table.HeaderCell>
                <Table.HeaderCell colSpan="2">Discarded</Table.HeaderCell>
              </Table.Row>
              <Table.Row>
                <Table.HeaderCell>Unknown</Table.HeaderCell>
                <Table.HeaderCell>Illegal</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{tableBody}</Table.Body>
          </Table>
        </Segment>
      </Segment.Group>
    );
  }
}
export default WithData(LacpStatistics, "/stat/lacp_statistics", false);
