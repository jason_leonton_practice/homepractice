import React, { Component } from "react";
import { Segment, Table } from "semantic-ui-react";
import { Link } from "react-router-dom";

import { SegmentHeader } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";

const isAutoRefresh = false;

class MstpStatusBridges extends Component {
  state = { isAutoRefresh };

  handleAutoRefresh = () => {
    this.setState(
      prevState => ({ isAutoRefresh: !prevState.isAutoRefresh }),
      () => this.props.autoRefresh(this.state.isAutoRefresh)
    );
  };

  render() {
    const data = this.props.data.split("|");
    let table_body = [];
    let rows = 0;

    if (data) {
      data.forEach((row, i) => {
        if (row) {
          const value = row.split("/");
          rows = i + 1; // i start from 0

          table_body.push(
            <Table.Row key={`row_${i}`} textAlign="center">
              <Table.Cell>
                <Link to={`/monitor/spanning_tree/bridge_status_detailed?bridge=${value[0]}`}>
                  {value[1]}
                </Link>
              </Table.Cell>
              <Table.Cell>{value[2]}</Table.Cell>
              <Table.Cell>{value[3]}</Table.Cell>
              <Table.Cell>{value[4]}</Table.Cell>
              <Table.Cell>{value[5]}</Table.Cell>
              <Table.Cell>{value[6]}</Table.Cell>
              <Table.Cell>{value[7]}</Table.Cell>
            </Table.Row>
          );
        }
      });
    }

    if (rows === 0) {
      table_body.push(
        <Table.Row key="table_row_no_ports" textAlign="center">
          <Table.Cell colSpan="7">No STP bridges</Table.Cell>
        </Table.Row>
      );
    }

    return (
      <Segment.Group>
        <SegmentHeader>
          STP Bridges
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Table celled compact structured striped>
            <Table.Header>
              <Table.Row textAlign="center">
                <Table.HeaderCell rowSpan="2">MSTI</Table.HeaderCell>
                <Table.HeaderCell rowSpan="2">Bridge ID</Table.HeaderCell>
                <Table.HeaderCell colSpan="3">Root</Table.HeaderCell>
                <Table.HeaderCell rowSpan="2">Topology Flag</Table.HeaderCell>
                <Table.HeaderCell rowSpan="2">Topology Change Last</Table.HeaderCell>
              </Table.Row>
              <Table.Row textAlign="center">
                <Table.HeaderCell>ID</Table.HeaderCell>
                <Table.HeaderCell>Port</Table.HeaderCell>
                <Table.HeaderCell>Cost</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{table_body}</Table.Body>
          </Table>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(MstpStatusBridges, "/stat/rstp_status_bridges", false);
