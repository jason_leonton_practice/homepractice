import React, { Component } from "react";
import { Segment } from "semantic-ui-react";

import WithData from "../../utils/withData";
import { SemanticButton, HeaderRefreshBar, BtnWrapper } from "../../components/semantic-component";
import { SegmentHeader, Dropdown, InnerTable as Table } from "../../components/styled_component";
import { genOptions } from "../../utils/dyform";

const { confighasPost, configHasZtp, configHasStackFwChk } = require("../../utils/config");

class SysLedStatus extends Component {
  state = { clear_type: 0, isAutoRefresh: false };

  handleDropdownChange = (e, { name, value }) => {
    this.setState({ [name]: value });
  };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  handleClear = () => {
    this.props.querySearch(`/stat/sys_led_status?clear=${this.state.clear_type}`);
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  static getOptions() {
    let text = ["All", "Fatal", "Software"];
    let value = [0, 1, 2];
    let obj = { text, value };

    if (typeof confighasPost == "function" && confighasPost()) {
      text.push("POST");
      value.push(3);
      obj = { text, value };
    }
    if (typeof configHasZtp == "function" && configHasZtp()) {
      text.push("ZTP");
      value.push(4);
      obj = { text, value };
    }
    if (typeof configHasStackFwChk == "function" && configHasStackFwChk()) {
      text.push("Stack Firmware");
      value.push(5);
      obj = { text, value };
    }

    const options = genOptions(obj);

    return options;
  }

  render() {
    const { data } = this.props;

    return (
      <Segment.Group>
        <SegmentHeader>
          LED Status
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Table definition>
            <Table.Body>
              <Table.Row>
                <Table.Cell collapsing>Clear Type</Table.Cell>
                <Table.Cell>
                  <Dropdown
                    name="clear_type"
                    value={this.state.clear_type}
                    onChange={this.handleDropdownChange}
                    selection
                    options={SysLedStatus.getOptions()}
                  />
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Description</Table.Cell>
                <Table.Cell>{data ? data : null}</Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
          <BtnWrapper children={<SemanticButton title="clear" onClick={this.handleClear} />} />
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(SysLedStatus, "/stat/sys_led_status", false);
