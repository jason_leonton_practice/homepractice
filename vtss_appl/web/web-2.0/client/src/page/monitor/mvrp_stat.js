import React, { Component } from "react";
import { Segment } from "semantic-ui-react";

import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";
import { toMacAddress } from "../../utils/format";

const isAutoRefresh = false;

class MvrpStat extends Component {
  state = { isAutoRefresh };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    let tableBody = [];

    /* Format:
     * <port 1>,<port 2>,<port 3>,...<port n>
     *
     * port x :== <port_no>/<FailedRegistrations>/<LastPduOrigin>
     *   port_no              :== 1..max
     *   FailedRegistrations  :== 0..2^64-1
     *   LastPduOrigin        :== MAC address
     */
    if (this.props.data) {
      let values = this.props.data.split(",");

      values.forEach((value, i) => {
        if (value.length) {
          let pval = value.split("/");

          tableBody.push(
            <Table.Row key={`table_row_${i}`}>
              <Table.Cell>{pval[0]}</Table.Cell>
              <Table.Cell>{pval[1]}</Table.Cell>
              <Table.Cell>{toMacAddress(pval[2])}</Table.Cell>
            </Table.Row>
          );
        }
      });
    } else {
      tableBody.push(
        <Table.Row key="table_data_row">
          <Table.Cell>No data exists</Table.Cell>
        </Table.Row>
      );
    }

    return (
      <Segment.Group>
        <SegmentHeader>
          MVRP Statistics
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Table unstackable celled compact striped textAlign="center">
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Port</Table.HeaderCell>
                <Table.HeaderCell>Failed Registrations</Table.HeaderCell>
                <Table.HeaderCell>Last PDU Origin</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{tableBody}</Table.Body>
          </Table>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(MvrpStat, "/stat/mvrp_stat", false);
