import React, { Component } from "react";
import { Segment } from "semantic-ui-react";

import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";

const isAutoRefresh = false;

export class IpStatus extends Component {
  state = { isAutoRefresh };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  ifMunge = data => {
    var values = [];
    var lines = data.split("\n");
    var iface;
    for (var i = 0; i < lines.length; i++) {
      var m,
        line = lines[i];

      if (line.match(/^(\S+)/)) {
        iface = line;
      } else if ((m = line.match(/^\s+(LINK):\s*(\S+).+(<[^>]+>)/))) {
        values.push([iface, m[1], m[2], m[3]]);
      } else if ((m = line.match(/^\s+(IPv[46]):\s*(\S+)/))) {
        values.push([iface, m[1], m[2], ""]);
      }
    }
    return values;
  };

  rtMunge = data => {
    let values = [];
    let lines = data.split("\n");

    for (let i = 0; i < lines.length; i++) {
      let m,
        line = lines[i];
      if ((m = line.match(/^(\S+) via (\S+) (<[^>]+>)/))) {
        values.push([m[1], m[2], m[3]]);
      }
    }
    return values;
  };

  nbcMunge = data => {
    let values = [];
    let lines = data.split("\n");

    for (let i = 0; i < lines.length; i++) {
      let m,
        line = lines[i];
      if ((m = line.match(/^(\S+) via (\S+)/))) {
        values.push([m[1], m[2]]);
      }
    }
    return values;
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    let IpInterfaceTableBody = [];
    let IpRouteTableBody = [];
    let neigbourTableBody = [];

    if (this.props.data) {
      let cfg = this.props.data.split("^@^@^");

      if (cfg[0]) {
        const tableRows = this.ifMunge(cfg[0]);

        tableRows.forEach((value, i) => {
          if (value.length > 0) {
            IpInterfaceTableBody.push(
              <Table.Row textAlign="center" key={`ip_interface_row_${i}`}>
                <Table.Cell>{value[0]}</Table.Cell>
                <Table.Cell>{value[1]}</Table.Cell>
                <Table.Cell>{value[2]}</Table.Cell>
                <Table.Cell>{value[3]}</Table.Cell>
              </Table.Row>
            );
          }
        });
      }
      if (cfg[1]) {
        const tableRows = this.rtMunge(cfg[1]);

        if (tableRows.length !== 0) {
          tableRows.forEach((value, i) => {
            if (value.length > 0) {
              IpRouteTableBody.push(
                <Table.Row textAlign="center" key={`ip_routes_row_${i}`}>
                  <Table.Cell>{value[0]}</Table.Cell>
                  <Table.Cell>{value[1]}</Table.Cell>
                  <Table.Cell>{value[2]}</Table.Cell>
                </Table.Row>
              );
            }
          });
        } else {
          IpRouteTableBody.push(
            <Table.Row textAlign="center" key={`ip_routes_no_row`}>
              <Table.Cell colSpan="3">No routes group</Table.Cell>
            </Table.Row>
          );
        }
      }

      if (cfg[2]) {
        const tableRows = this.nbcMunge(cfg[2]);

        if (tableRows.length !== 0) {
          tableRows.forEach((value, i) => {
            if (value.length > 0) {
              neigbourTableBody.push(
                <Table.Row textAlign="center" key={`neigbour_row_${i}`}>
                  <Table.Cell>{value[0]}</Table.Cell>
                  <Table.Cell>{value[1]}</Table.Cell>
                </Table.Row>
              );
            }
          });
        } else {
          neigbourTableBody.push(
            <Table.Row textAlign="center" key={`neigbour_row_no_row`}>
              <Table.Cell colSpan="2">No neighbour cache group</Table.Cell>
            </Table.Row>
          );
        }
      }
    }

    return (
      <Segment.Group>
        <SegmentHeader>
          IP Interfaces
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Table celled compact>
            <Table.Header>
              <Table.Row textAlign="center">
                <Table.HeaderCell>Interface</Table.HeaderCell>
                <Table.HeaderCell>Type</Table.HeaderCell>
                <Table.HeaderCell>Address</Table.HeaderCell>
                <Table.HeaderCell>Status</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{IpInterfaceTableBody}</Table.Body>
          </Table>

          <h4>IPv6 Routes</h4>
          <Table celled compact fixed>
            <Table.Header>
              <Table.Row textAlign="center">
                <Table.HeaderCell>Network</Table.HeaderCell>
                <Table.HeaderCell>Gateway</Table.HeaderCell>
                <Table.HeaderCell>Status</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{IpRouteTableBody}</Table.Body>
          </Table>

          <h4>Neighbour cache</h4>
          <Table celled compact fixed>
            <Table.Header>
              <Table.Row textAlign="center">
                <Table.HeaderCell>IP Address</Table.HeaderCell>
                <Table.HeaderCell>Link Address</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{neigbourTableBody}</Table.Body>
          </Table>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(IpStatus, "/stat/ip2_status", isAutoRefresh);
