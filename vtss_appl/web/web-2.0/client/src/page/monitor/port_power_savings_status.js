import React, { Component } from "react";
import { Segment, Icon } from "semantic-ui-react";
import styled from "styled-components";

import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";

const isAutoRefresh = false;

class PortPowerSavingsStatus extends Component {
  constructor(props) {
    super(props);
    this.errorMessage = "";
    this.identifyEEESavingType = 1;
    this.tableth = [
      "Port",
      "Link",
      "EEE Cap",
      "EEE Ena",
      "LP EEE Cap",
      "EEE In power save",
      "ActiPhy Savings",
      "PerfectReach Savings"
    ];
    this.yesno_image = ["images/no.gif", "images/yes.gif"];
    this.link_image = ["images/led-down.gif", "images/led-up.gif"];
  }
  state = { isAutoRefresh };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    let tableHead;
    let tableBody = [];

    /* Format:
     * <port 1>,<port 2>,<port 3>,...<port n>
     *
     * port x :== <port_no>/<FailedRegistrations>/<LastPduOrigin>
     *   port_no              :== 1..max
     *   FailedRegistrations  :== 0..2^64-1
     *   LastPduOrigin        :== MAC address
     */
    if (this.props.data) {
      const status = type => {
        // 0: forbidden, 1: support
        switch (type) {
          case 0:
            return "x";
          case 1:
            return "check";
          default:
            return "";
        }
      };
      let values = this.props.data.split("|");
      this.errorMessage = values[0];
      if (this.errorMessage) {
        alert(this.errorMessage);
      }

      this.identifyEEESavingType = values[1];
      let tableData = values[2].split("&");

      //tableHead
      if (this.identifyEEESavingType !== "1") {
        this.tableth = this.tableth.filter((value, i) => {
          return !(i > 1 && i < 6);
        });
      }
      tableHead = (
        <Table.Row>
          {this.tableth.map((content, i) => {
            return <Table.HeaderCell key={`th${i}`}>{content}</Table.HeaderCell>;
          })}
        </Table.Row>
      );

      //tableBody
      tableData.map((value, i, newarray) => {
        if (value.length) {
          let pval = value.split("/");

          if (this.identifyEEESavingType !== "1") {
            tableBody.push(
              <Table.Row key={`table_row_${i}`}>
                <Table.Cell>{pval[0]}</Table.Cell>
                <Table.Cell>
                  <PortIcon name="circle" className={pval[3] === "1" ? "active" : ""} />
                </Table.Cell>
                <Table.Cell>
                  <StatusIcon name={status(parseInt(pval[7]))} circular />
                </Table.Cell>
                <Table.Cell>
                  <StatusIcon name={status(parseInt(pval[8]))} circular />
                </Table.Cell>
              </Table.Row>
            );
          } else {
            tableBody.push(
              <Table.Row key={`table_row_${i}`}>
                <Table.Cell>{pval[0]}</Table.Cell>
                <Table.Cell>
                  <PortIcon name="circle" className={pval[3] === "1" ? "active" : ""} />
                </Table.Cell>
                <Table.Cell>
                  <StatusIcon name={status(parseInt(pval[1]))} circular />
                </Table.Cell>
                <Table.Cell>
                  <StatusIcon name={status(parseInt(pval[2]))} circular />
                </Table.Cell>
                <Table.Cell>
                  <StatusIcon name={status(parseInt(pval[4]))} circular />
                </Table.Cell>
                <Table.Cell>
                  <StatusIcon name={status(parseInt(pval[5] || pval[6]))} circular />
                </Table.Cell>
                <Table.Cell>
                  <StatusIcon name={status(parseInt(pval[7]))} circular />
                </Table.Cell>
                <Table.Cell>
                  <StatusIcon name={status(parseInt(pval[8]))} circular />
                </Table.Cell>
              </Table.Row>
            );
          }
        }
        return newarray;
      });
    } else {
      tableBody.push(
        <Table.Row key="table_data_row">
          <Table.Cell>No data exists</Table.Cell>
        </Table.Row>
      );
    }

    return (
      <Segment.Group>
        <SegmentHeader>
          Port Power Savings Status
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Table unstackable celled compact striped textAlign="center">
            <Table.Header>{tableHead}</Table.Header>
            <Table.Body>{tableBody}</Table.Body>
          </Table>
        </Segment>
      </Segment.Group>
    );
  }
}
const PortIcon = styled(Icon)`
  &.circle {
    color: #db2828;
    &.active {
      color: #21ba45;
    }
  }
`;
const StatusIcon = styled(Icon)`
  margin-right: 0 !important;
  &.check {
    color: #21ba45 !important;
  }
  &.x,
  &.exclamation.triangle {
    color: #db2828 !important;
  }
`;
export default WithData(PortPowerSavingsStatus, "/stat/port_power_savings_status", false);
