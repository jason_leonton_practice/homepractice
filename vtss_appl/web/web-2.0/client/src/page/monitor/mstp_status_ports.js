import React, { Component } from "react";
import { Segment } from "semantic-ui-react";

import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";

const isAutoRefresh = false;

class MStpStatusPorts extends Component {
  state = { isAutoRefresh };

  handleAutoRefresh = () => {
    this.setState(
      prevState => ({ isAutoRefresh: !prevState.isAutoRefresh }),
      () => this.props.autoRefresh(this.state.isAutoRefresh)
    );
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    const data = this.props.data.split("|");
    let table_body = [];
    let rows = 0;

    if (data) {
      data.forEach((row, i) => {
        if (row) {
          const cell_value = row.split("/");
          rows = i + 1; // i start from 0
          table_body.push(
            <Table.Row key={`table_row_${i}`}>
              <Table.Cell textAlign="center">{cell_value[0]}</Table.Cell>
              <Table.Cell textAlign="center">{cell_value[1]}</Table.Cell>
              <Table.Cell textAlign="center">{cell_value[2]}</Table.Cell>
              <Table.Cell textAlign="right">{cell_value[3]}</Table.Cell>
            </Table.Row>
          );
        }
      });
    }

    if (rows === 0) {
      table_body.push(
        <Table.Row key="table_row_no_ports" textAlign="center">
          <Table.Cell colSpan="4">No ports enabled</Table.Cell>
        </Table.Row>
      );
    }

    return (
      <Segment.Group>
        <SegmentHeader>
          STP Port Status
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Table celled compact striped>
            <Table.Header>
              <Table.Row textAlign="center">
                <Table.HeaderCell>Port</Table.HeaderCell>
                <Table.HeaderCell>CIST Role</Table.HeaderCell>
                <Table.HeaderCell>CIST State</Table.HeaderCell>
                <Table.HeaderCell>Uptime</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{table_body}</Table.Body>
          </Table>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(MStpStatusPorts, "/stat/rstp_status_ports", isAutoRefresh);
