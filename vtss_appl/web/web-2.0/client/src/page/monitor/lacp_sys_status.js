import React, { Component } from "react";
import { Segment } from "semantic-ui-react";

import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";

const isAutoRefresh = false;

class LacpSysStatus extends Component {
  state = { isAutoRefresh };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    let LocalSysID = [];
    let partnerSysStatusTable = [];
    let rows = 0;
    if (this.props.data) {
      let cfg = this.props.data.split("|");
      if (cfg[0].length) {
        let val = cfg[0].split("/");
        rows++;

        LocalSysID.push(
          <Table.Row key={"table_row1_" + rows}>
            <Table.Cell>{val[0]}</Table.Cell>
            <Table.Cell>{val[1]}</Table.Cell>
          </Table.Row>
        );
      }

      rows = 0;

      for (let itmNum = 1; itmNum < cfg.length; itmNum++) {
        if (cfg[itmNum]) {
          let val = cfg[itmNum].split("/");
          rows++;

          partnerSysStatusTable.push(
            <Table.Row key={"table_row2_" + rows}>
              <Table.Cell>{val[0]}</Table.Cell>
              <Table.Cell>{val[1]}</Table.Cell>
              <Table.Cell>{val[2]}</Table.Cell>
              <Table.Cell>{val[3]}</Table.Cell>
              <Table.Cell>{val[4]}</Table.Cell>
              <Table.Cell>{val[5]}</Table.Cell>
            </Table.Row>
          );
        }
      }

      if (rows === 0) {
        partnerSysStatusTable.push(
          <Table.Row key="No ports enabled or no existing partners">
            <Table.Cell colSpan="6">No ports enabled or no existing partners</Table.Cell>
          </Table.Row>
        );
      }
      return (
        <Segment.Group>
          <SegmentHeader>
            LACP System Status
            <HeaderRefreshBar
              className="segment"
              checked={this.state.isAutoRefresh}
              handleAutoRefresh={this.handleAutoRefresh}
            />
          </SegmentHeader>

          <Segment>
            <h4>Local System ID</h4>
            <Table celled compact fixed textAlign="center">
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Priority</Table.HeaderCell>
                  <Table.HeaderCell>MAC Address</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>{LocalSysID}</Table.Body>
            </Table>

            <h4>Partner System Status</h4>
            <Table celled compact fixed textAlign="center">
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Aggr ID</Table.HeaderCell>
                  <Table.HeaderCell>Partner System ID</Table.HeaderCell>
                  <Table.HeaderCell>Partner Prio</Table.HeaderCell>
                  <Table.HeaderCell>Partner Key</Table.HeaderCell>
                  <Table.HeaderCell>Last Changed</Table.HeaderCell>
                  <Table.HeaderCell>Local Ports</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>{partnerSysStatusTable}</Table.Body>
            </Table>
          </Segment>
        </Segment.Group>
      );
    }
  }
}

export default WithData(LacpSysStatus, "/stat/lacp_sys_status", false);
