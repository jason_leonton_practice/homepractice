import React from "react";
import { Segment, Icon } from "semantic-ui-react";

import { HeaderRefreshBar } from "../../components/semantic-component";
import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import WithData from "../../utils/withData";
import "./sys.scss";

const doesInitialAutoRefresh = false;

class Sys extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      btnLoading: false,
      isAutoRefresh: doesInitialAutoRefresh
    };
  }

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    const data = this.props.data;
    const splitData = data.split("/");

    let content = (
      <Table compact basic="very" celled className="info-table">
        <Table.Body>
          <Table.Row>
            <Table.Cell colSpan="2" className="table-header">
              <Icon name="compass" />
              System
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell textAlign="right">
              <h5>Serial Number</h5>
            </Table.Cell>
            <Table.Cell className="value">{splitData[10]}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell colSpan="2" className="table-header">
              <Icon name="hdd" />
              Hardware
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell textAlign="right">
              <h5>MAC Address</h5>
            </Table.Cell>
            <Table.Cell className="value">{splitData[0]}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell textAlign="right">
              <h5>Chip ID</h5>
            </Table.Cell>
            <Table.Cell className="value">{splitData[5]}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell colSpan="2" className="table-header">
              <Icon name="compass" />
              Time
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell textAlign="right">
              <h5>System Date</h5>
            </Table.Cell>
            <Table.Cell className="value">{splitData[2]}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell textAlign="right">
              <h5>System Uptime</h5>
            </Table.Cell>
            <Table.Cell className="value">{splitData[1]}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell colSpan="2" className="table-header">
              <Icon name="compass" />
              Software
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell textAlign="right">
              <h5>Software Version</h5>
            </Table.Cell>
            <Table.Cell className="value">{splitData[3]}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell textAlign="right">
              <h5>Software Date</h5>
            </Table.Cell>
            <Table.Cell className="value">{unescape(splitData[4])}</Table.Cell>
          </Table.Row>
          <Table.Row>
            {/* <Table.Cell>Code Revision</Table.Cell>
              <Table.Cell>{splitData[9]}</Table.Cell> */}
            <Table.Cell textAlign="right">
              <h5>Acknowledgments</h5>
            </Table.Cell>
            <Table.Cell className="value">Details</Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>
    );

    return (
      <Segment.Group>
        <SegmentHeader>
          System Information
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>{content}</Segment>
      </Segment.Group>
    );
  }
}

export default WithData(Sys, "/stat/sys", doesInitialAutoRefresh);
