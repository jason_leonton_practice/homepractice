import React, { Component } from "react";
import { Segment } from "semantic-ui-react";

import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";

const isAutoRefresh = false;

class LacpNeighborStatus extends Component {
  state = { isAutoRefresh };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    let tableBody = [];
    let rows = 0;

    if (this.props.data) {
      let values = this.props.data.split("|");
      values.forEach((value, i) => {
        if (value.length) {
          let pval = value.split("/");
          rows++;

          tableBody.push(
            <Table.Row key={"table_row_" + i}>
              <Table.Cell>{pval[0]}</Table.Cell>
              <Table.Cell>{pval[1]}</Table.Cell>
              <Table.Cell>{pval[2]}</Table.Cell>
              <Table.Cell>{pval[3]}</Table.Cell>
              <Table.Cell>{pval[4]}</Table.Cell>
              <Table.Cell>{pval[5]}</Table.Cell>
              <Table.Cell>{pval[6]}</Table.Cell>
              <Table.Cell>{pval[7]}</Table.Cell>
              <Table.Cell>{pval[8]}</Table.Cell>
              <Table.Cell>{pval[9]}</Table.Cell>
              <Table.Cell>{pval[10]}</Table.Cell>
              <Table.Cell>{pval[11]}</Table.Cell>
              <Table.Cell>{pval[12]}</Table.Cell>
              <Table.Cell>{pval[13]}</Table.Cell>
            </Table.Row>
          );
        }
      });
    }

    if (rows === 0) {
      tableBody.push(
        <Table.Row key="table_row_no_ports_enabled">
          <Table.Cell colSpan="14">No LACP neighbor status available</Table.Cell>
        </Table.Row>
      );
    }

    return (
      <Segment.Group>
        <SegmentHeader>
          LACP Neighbor Port Status
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Table striped unstackable celled compact textAlign="center">
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Port</Table.HeaderCell>
                <Table.HeaderCell>State</Table.HeaderCell>
                <Table.HeaderCell>Aggr ID</Table.HeaderCell>
                <Table.HeaderCell>Partner Key</Table.HeaderCell>
                <Table.HeaderCell>Partner Port</Table.HeaderCell>
                <Table.HeaderCell>Partner Port Prio</Table.HeaderCell>
                <Table.HeaderCell>Activity</Table.HeaderCell>
                <Table.HeaderCell>Timeout</Table.HeaderCell>
                <Table.HeaderCell>Aggregation</Table.HeaderCell>
                <Table.HeaderCell>Synchronization</Table.HeaderCell>
                <Table.HeaderCell>Collecting</Table.HeaderCell>
                <Table.HeaderCell>Distributing</Table.HeaderCell>
                <Table.HeaderCell>Defaulted</Table.HeaderCell>
                <Table.HeaderCell>Expired</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{tableBody}</Table.Body>
          </Table>
        </Segment>
      </Segment.Group>
    );
  }
}
export default WithData(LacpNeighborStatus, "/stat/lacp_neighbor_status", false);
