import React, { Component } from "react";
import { Segment } from "semantic-ui-react";

import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";

const isAutoRefresh = false;

class LoopStatus extends Component {
  state = { isAutoRefresh };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    let tableBody = [];
    let rows = 0;

    if (this.props.data) {
      let values = this.props.data.split("|");
      values.forEach((value, i) => {
        if (value.length) {
          let pval = value.split("/");
          rows++;

          tableBody.push(
            <Table.Row key={"table_row_" + i}>
              <Table.Cell>{pval[0]}</Table.Cell>
              <Table.Cell>{pval[1]}</Table.Cell>
              <Table.Cell>{pval[2]}</Table.Cell>
              <Table.Cell>{pval[3]}</Table.Cell>
              <Table.Cell>{pval[4]}</Table.Cell>
              <Table.Cell>{pval[5]}</Table.Cell>
              <Table.Cell>{pval[6]}</Table.Cell>
            </Table.Row>
          );
        }
      });
    }

    if (rows === 0) {
      tableBody.push(
        <Table.Row key="table_row_no_ports_enabled">
          <Table.Cell colSpan="7">No ports enabled</Table.Cell>
        </Table.Row>
      );
    }

    return (
      <Segment.Group>
        <SegmentHeader>
          Loop Protection Status
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Table striped unstackable celled compact fixed textAlign="center">
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Port</Table.HeaderCell>
                <Table.HeaderCell>Action</Table.HeaderCell>
                <Table.HeaderCell>Transmit</Table.HeaderCell>
                <Table.HeaderCell>Loops</Table.HeaderCell>
                <Table.HeaderCell>Status</Table.HeaderCell>
                <Table.HeaderCell>Loop</Table.HeaderCell>
                <Table.HeaderCell>Time of Last Loop</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{tableBody}</Table.Body>
          </Table>
        </Segment>
      </Segment.Group>
    );
  }
}
export default WithData(LoopStatus, "/stat/loop_status", false);
