import React, { Component } from "react";
import { Segment } from "semantic-ui-react";

import WithData from "../../utils/withData";
import { HeaderRefreshBar, SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { InnerTable as Table, SegmentHeader } from "../../components/styled_component";

const isAutoRefresh = false;

class AccessMgmtStatistics extends Component {
  state = { isAutoRefresh };

  handleClear = () => {
    this.props.querySearch("/stat/access_mgmt_statistics?clear=1", false);
  };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    let table_body = [];

    if (this.props.data) {
      const data = this.props.data.split("|");
      for (let i = 0; i < data.length; i++) {
        if (data[i]) {
          let pval = data[i].split("/");

          table_body.push(
            <Table.Row key={`row_${i}`}>
              <Table.Cell textAlign="center">{pval[0]}</Table.Cell>
              <Table.Cell textAlign="right">{pval[1]}</Table.Cell>
              <Table.Cell textAlign="right">{pval[2]}</Table.Cell>
              <Table.Cell textAlign="right">{pval[3]}</Table.Cell>
            </Table.Row>
          );
        }
      }
    }

    return (
      <Segment.Group>
        <SegmentHeader>
          Port Statistics Overview
          <HeaderRefreshBar checked={this.state.isAutoRefresh} handleAutoRefresh={this.handleAutoRefresh} />
        </SegmentHeader>
        <Segment>
          <Table celled structured striped compact>
            <Table.Header>
              <Table.Row textAlign="center">
                <Table.HeaderCell>Interface</Table.HeaderCell>
                <Table.HeaderCell>Received Packets</Table.HeaderCell>
                <Table.HeaderCell>Allowed Packets</Table.HeaderCell>
                <Table.HeaderCell>Discarded Packets</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{table_body}</Table.Body>
          </Table>
          <BtnWrapper>
            <SemanticButton title="clear" disabled={this.props.readOnly} onClick={this.handleClear} />
          </BtnWrapper>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(AccessMgmtStatistics, "/stat/access_mgmt_statistics", isAutoRefresh);
