import React, { Component } from "react";
import { Segment } from "semantic-ui-react";

import WithData from "../../utils/withData";
import { InnerTable as Table, Dropdown, SegmentHeader } from "../../components/styled_component";
import { HeaderRefreshBar, SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { genOptions, genArrayInt, genArrayStr } from "../../utils/dyform";

const isAutoRefresh = false;

class StatDetailed extends Component {
  constructor(props) {
    super(props);
    this.state = { isAutoRefresh, portno: "" };
    this.columnWith = 6;
    this.dropdownOption = genOptions({
      value: genArrayInt(1, 20),
      text: genArrayStr(1, 20, "Port ")
    });
  }

  handleDropdownChange = (e, { value }) => {
    this.setState({ portno: value }, () => {
      this.props.history.push(`/monitor/ports/detailed_statistics?port=${this.state.portno}`);
      this.props.querySearch(`/stat/port?port=${this.state.portno}`);
    });
  };

  handleAutoRefresh = () => {
    this.setState(
      prevState => ({ isAutoRefresh: !prevState.isAutoRefresh }),
      () => this.props.autoRefresh(this.state.isAutoRefresh)
    );
  };

  handleClear = () => {
    this.props.querySearch(`/stat/port?clear=1&port=${this.state.portno}`, false);
  };

  processUpdate = () => {
    const values = this.props.data ? this.props.data.split(",") : [];
    const portno = values.length === 3 ? parseInt(values[0]) : NaN;

    this.setState({ portno });
  };

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps) !== JSON.stringify(this.props)) {
      this.processUpdate();
    }
  }

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  componentDidMount() {
    this.processUpdate();
  }

  render() {
    const { data, readOnly } = this.props;
    const values = data ? data.split(",") : [];
    let obj = {};

    values.forEach(value => {
      if (values.length === 3) {
        // let maxport = parseInt(values[1]);
        let statData = values[2].split("|");

        statData.forEach(data => {
          let counters = data.split("/");
          let group = counters[0];

          for (var j = 1; j < counters.length; j++) {
            let id = `T_${group}_${j}`;
            obj[id] = counters[j];
          }
        });
      }
    });

    const table = (
      <React.Fragment>
        <Table celled compact striped>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell colSpan="2">Receive Total</Table.HeaderCell>
              <Table.HeaderCell colSpan="2">Transmit Total</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Table.Row>
              <Table.Cell width={this.columnWith}>Rx Packets</Table.Cell>
              <Table.Cell>{obj.T_1_1}</Table.Cell>
              <Table.Cell width={this.columnWith}>Tx Packets</Table.Cell>
              <Table.Cell>{obj.T_1_2}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rx Octets</Table.Cell>
              <Table.Cell>{obj.T_1_3}</Table.Cell>
              <Table.Cell>Tx Octets</Table.Cell>
              <Table.Cell>{obj.T_1_4}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rx Unicast</Table.Cell>
              <Table.Cell>{obj.T_2_1}</Table.Cell>
              <Table.Cell>Tx Unicast</Table.Cell>
              <Table.Cell>{obj.T_2_2}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rx Multicast</Table.Cell>
              <Table.Cell>{obj.T_2_3}</Table.Cell>
              <Table.Cell>Tx Multicast</Table.Cell>
              <Table.Cell>{obj.T_2_4}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rx Broadcast</Table.Cell>
              <Table.Cell>{obj.T_2_5}</Table.Cell>
              <Table.Cell>Tx Broadcast</Table.Cell>
              <Table.Cell>{obj.T_2_6}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rx Pause</Table.Cell>
              <Table.Cell>{obj.T_3_1}</Table.Cell>
              <Table.Cell>Tx Pause</Table.Cell>
              <Table.Cell>{obj.T_3_2}</Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
        <Table celled compact striped>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell colSpan="2">Receive Size Counters</Table.HeaderCell>
              <Table.HeaderCell colSpan="2">Transmit Size Counters</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Table.Row>
              <Table.Cell width={this.columnWith}>Rx 64 Bytes</Table.Cell>
              <Table.Cell>{obj.T_4_1}</Table.Cell>
              <Table.Cell width={this.columnWith}>Tx 65-127 Bytes</Table.Cell>
              <Table.Cell>{obj.T_4_2}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rx 128-255 Bytes</Table.Cell>
              <Table.Cell>{obj.T_4_5}</Table.Cell>
              <Table.Cell>Tx 128-255 Bytes</Table.Cell>
              <Table.Cell>{obj.T_4_6}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rx 256-511 Bytes</Table.Cell>
              <Table.Cell>{obj.T_4_7}</Table.Cell>
              <Table.Cell>Tx 256-511 Bytes</Table.Cell>
              <Table.Cell>{obj.T_4_8}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rx 512-1023 Bytes</Table.Cell>
              <Table.Cell>{obj.T_4_9}</Table.Cell>
              <Table.Cell>Tx 512-1023 Bytes</Table.Cell>
              <Table.Cell>{obj.T_4_10}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rx 1024-1526 Bytes</Table.Cell>
              <Table.Cell>{obj.T_4_11}</Table.Cell>
              <Table.Cell>Tx 1024-1526 Bytes</Table.Cell>
              <Table.Cell>{obj.T_4_12}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rx 1527- Bytes</Table.Cell>
              <Table.Cell>{obj.T_5_1}</Table.Cell>
              <Table.Cell>Tx 1527- Bytes</Table.Cell>
              <Table.Cell>{obj.T_5_2}</Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
        <Table celled compact striped>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell colSpan="2">Receive Error Counters</Table.HeaderCell>
              <Table.HeaderCell colSpan="2">Transmit Error Counters</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Table.Row>
              <Table.Cell width={this.columnWith}>Rx Drops</Table.Cell>
              <Table.Cell>{obj.T_1_5}</Table.Cell>
              <Table.Cell width={this.columnWith}>Tx Drops</Table.Cell>
              <Table.Cell>{obj.T_1_6}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rx CRC/Alignment</Table.Cell>
              <Table.Cell>{obj.T_4_13}</Table.Cell>
              <Table.Cell width={this.columnWith}>Tx Late/Exc. Coll.</Table.Cell>
              <Table.Cell>{obj.T_1_8}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rx Undersize</Table.Cell>
              <Table.Cell>{obj.T_4_15}</Table.Cell>
              <Table.Cell />
              <Table.Cell />
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rx Oversize</Table.Cell>
              <Table.Cell>{obj.T_4_16}</Table.Cell>
              <Table.Cell />
              <Table.Cell />
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rx Fragments</Table.Cell>
              <Table.Cell>{obj.T_4_17}</Table.Cell>
              <Table.Cell />
              <Table.Cell />
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rx Jabber</Table.Cell>
              <Table.Cell>{obj.T_4_18}</Table.Cell>
              <Table.Cell />
              <Table.Cell />
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rx Filtered</Table.Cell>
              <Table.Cell>{obj.T_8_1}</Table.Cell>
              <Table.Cell />
              <Table.Cell />
            </Table.Row>
          </Table.Body>
        </Table>
      </React.Fragment>
    );

    return (
      <Segment.Group>
        <SegmentHeader>
          {`Detailed Port Statistics - `}
          <Dropdown
            compact
            selection
            options={this.dropdownOption}
            value={this.state.portno}
            onChange={this.handleDropdownChange}
          />
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          {table}
          <BtnWrapper>
            <SemanticButton title="clear" disabled={readOnly} onClick={this.handleClear} />
          </BtnWrapper>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(StatDetailed, "/stat/port", isAutoRefresh, ["port"]);
