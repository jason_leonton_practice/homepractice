import React from "react";
import { Segment } from "semantic-ui-react";

import { HeaderRefreshBar } from "../../components/semantic-component";
import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import WithData from "../../utils/withData";

import "./poe_status.scss";

const isAutoRefresh = false;

class PoeStatus extends React.Component {
  state = { isAutoRefresh };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    return (
      <Segment.Group>
        <SegmentHeader>
          Power Over Ethernet Status
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Table striped unstackable celled compact textAlign="center">
            <TableHeader />
            <TableBody data={this.props.data} />
          </Table>
        </Segment>
      </Segment.Group>
    );
  }
}

/*function component*/

function TableHeader() {
  const tableth = [
    "Local Port",
    "PD class",
    "Power Requested",
    "Power Allocated",
    "Power Used",
    "Current Used",
    "Priority",
    "Port Status"
  ];
  let tableHead = (
    <Table.Row>
      {tableth.map((content, i) => {
        return <Table.HeaderCell key={`th${i}`}>{content}</Table.HeaderCell>;
      })}
    </Table.Row>
  );
  return <Table.Header>{tableHead}</Table.Header>;
}

function TableBody(props) {
  const translatePriority = value => {
    let translateValue = "";
    switch (value) {
      case "0":
        translateValue = "Low";
        break;
      case "1":
        translateValue = "High";
        break;
      case "2":
        translateValue = "Critical";
        break;
      default:
        // Shall never happen
        translateValue = "Unknown";
        break;
    }
    return translateValue;
  };
  const portStatusStyle = value => {
    if (value === "PoE not available - No PoE chip found") {
      return "warning";
    }
  };
  let tableBody = [];
  let errorMessage = "";

  //tableBody
  if (props.data) {
    let values = props.data.split("!");
    errorMessage = values[0];
    if (errorMessage) {
      alert(errorMessage);
    }

    let tableData = values[1].split("|");
    let totalPowerUsed = parseInt(0, 10);
    let totalPowerRequested = parseInt(0, 10);
    let totalPowerAllocated = parseInt(0, 10);
    let totalCurrentUsed = parseInt(0, 10);

    tableData.forEach((value, i, newarray) => {
      if (value.length) {
        let pval = value.split("/");

        let [
          localPort,
          powerRequested,
          powerUsed,
          priority,
          currentUsed,
          portStatus,
          pdClass,
          powerAllocated
        ] = pval;

        totalPowerUsed += parseInt(powerUsed, 10);
        totalCurrentUsed += parseInt(currentUsed, 10);
        totalPowerRequested += parseInt(powerRequested, 10);
        totalPowerAllocated += parseInt(powerAllocated, 10);

        tableBody.push(
          <Table.Row key={`table_row_${i}`}>
            <Table.Cell>{localPort}</Table.Cell>
            <Table.Cell>{pdClass}</Table.Cell>
            <Table.Cell>{`${powerRequested} [W]`}</Table.Cell>
            <Table.Cell>{`${powerAllocated} [W]`}</Table.Cell>
            <Table.Cell>{`${powerUsed} [W]`}</Table.Cell>
            <Table.Cell>{`${currentUsed} [mA]`}</Table.Cell>
            <Table.Cell>{`${translatePriority(priority)}`}</Table.Cell>
            <Table.Cell className={portStatusStyle(portStatus) ? "warning-font-red" : ""}>
              {portStatus}
            </Table.Cell>
          </Table.Row>
        );
        return newarray;
      }
    });
    //Total Field
    tableBody.push(
      <Table.Row key={"total-row"}>
        <Table.Cell>Total</Table.Cell>
        <Table.Cell></Table.Cell>
        <Table.Cell>{`${totalPowerRequested} [W]`}</Table.Cell>
        <Table.Cell>{`${totalPowerAllocated} [W]`}</Table.Cell>
        <Table.Cell>{`${totalPowerUsed} [W]`}</Table.Cell>
        <Table.Cell>{`${totalCurrentUsed} [mA]`}</Table.Cell>
        <Table.Cell></Table.Cell>
        <Table.Cell className={`warning-font-red`}></Table.Cell>
      </Table.Row>
    );
  }
  return <Table.Body>{tableBody}</Table.Body>;
}
export default WithData(PoeStatus, "/stat/poe_status", false);
