import React, { Component } from "react";
import { Segment } from "semantic-ui-react";
import { Link } from "react-router-dom";

import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";

const isAutoRefresh = false;

class DDMIoverview extends Component {
  state = { isAutoRefresh };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    let tableBody = [];
    let rows = 0;

    if (this.props.data) {
      // Format:
      /* 1/ZyXEL/SFP-SX-D/S111132000061/V1.0/2011-08-10/1000BASE-SX|2/ZyXEL/SFP-SX-D/S111132000061/V1.0/2011-08-10/1000BASE-SX|3/ZyXEL/SFP-SX-D/S111132000061/V1.0/2011-08-10/1000BASE-SX|4/-/-/-/-/-/- */
      let values = this.props.data.split("|");

      values.forEach((value, i) => {
        if (value.length) {
          let pval = value.split("/");
          rows++;

          tableBody.push(
            <Table.Row key={"table_row_" + i}>
              <Table.Cell>
                <Link to={`/monitor/ddmi/detailed?port=${pval[0]}`}>{pval[0]}</Link>
              </Table.Cell>
              <Table.Cell>{pval[1]}</Table.Cell>
              <Table.Cell>{pval[2]}</Table.Cell>
              <Table.Cell>{pval[3]}</Table.Cell>
              <Table.Cell>{pval[4]}</Table.Cell>
              <Table.Cell>{pval[5]}</Table.Cell>
              <Table.Cell>{pval[6]}</Table.Cell>
            </Table.Row>
          );
        }
      });
    }

    if (rows === 0) {
      tableBody.push(
        <Table.Row key="no_sfp">
          <Table.Cell colSpan="7">No SFP detected</Table.Cell>
        </Table.Row>
      );
    }

    return (
      <Segment.Group>
        <SegmentHeader>
          DDMI Overview
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Table striped unstackable celled compact textAlign="center">
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Port</Table.HeaderCell>
                <Table.HeaderCell>Vendor</Table.HeaderCell>
                <Table.HeaderCell>Part Number</Table.HeaderCell>
                <Table.HeaderCell>Serial Number</Table.HeaderCell>
                <Table.HeaderCell>Revision</Table.HeaderCell>
                <Table.HeaderCell>Data Code</Table.HeaderCell>
                <Table.HeaderCell>Transceiver</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{tableBody}</Table.Body>
          </Table>
        </Segment>
      </Segment.Group>
    );
  }
}
export default WithData(DDMIoverview, "/stat/ddmi_overview", isAutoRefresh);
