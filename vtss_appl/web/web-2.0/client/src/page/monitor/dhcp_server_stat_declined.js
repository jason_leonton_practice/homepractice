import React, { Component } from "react";
import { Segment } from "semantic-ui-react";

import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";

const isAutoRefresh = false;

class DhcpServerStatDeclined extends Component {
  state = { isAutoRefresh };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    let tableBody = [];
    let rows = 0;

    if (this.props.data) {
      let values = this.props.data.split("|");
      values.forEach((value, i) => {
        if (value.length) {
          rows++;

          tableBody.push(
            <Table.Row key={"table_row_" + i}>
              <Table.Cell>{value}</Table.Cell>
            </Table.Row>
          );
        }
      });
    }

    if (rows === 0) {
      tableBody.push(
        <Table.Row key="disable list">
          <Table.Cell>No disable IP</Table.Cell>
        </Table.Row>
      );
    }

    return (
      <Segment.Group>
        <SegmentHeader>
          DHCP Server Declined IP
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Table striped unstackable celled compact textAlign="center">
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Declined IP Address</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{tableBody}</Table.Body>
          </Table>
        </Segment>
      </Segment.Group>
    );
  }
}
export default WithData(DhcpServerStatDeclined, "/stat/dhcp_server_declined", false);
