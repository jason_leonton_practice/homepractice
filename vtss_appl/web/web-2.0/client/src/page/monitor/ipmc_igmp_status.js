import React, { Component } from "react";
import { Segment, Divider } from "semantic-ui-react";
import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";

const isAutoRefresh = false;

class IpmcIgmpsStatus extends Component {
  state = { isAutoRefresh };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  statisticsQuerierStatus = option => {
    switch (parseInt(option, 10)) {
      case 1:
        return "Static";
      case 2:
        return "Dynamic";
      case 3:
        return "Both";
      case 0:
      default:
        return "-";
    }
  };


  rpStatus = option => {
    switch (parseInt(option,10)) {
      case 0:
        return "DISABLE";
      case 2:
        return "ACTIVE";
      default:
        return "IDLE";
    }
  };

  clearCounters = () => {
    this.props.querySearch("/stat/ipmc_status?ipmc_version=1&clear=1", false);
  };

  render() {
    /* get form data
       Format: [vid],[querier_ver],[host_ver],[querier_status],[querier_transmitted],[received_v1_reports],[received_v2_reports],[received_v3_reports],[received_v2_leave]/...
               | [port_no],[status]/[port_no],[status]/...
       status 0: None     1: Static      2: Dynamic      3: Both
    */
    let igmp_vlan_data = [];
    let igmp_vlan_table = [];
    let router_port_data = [];
    let router_port_table = [];
    //igmp_vlan_table
    if (this.props.data) {
      let igmpConfig = this.props.data.split("|");

      if (igmpConfig[0]) {
        igmp_vlan_data = igmpConfig[0].split("/");
        igmp_vlan_data.forEach((val, i) => {
          if (val) {
            let pval = val.split(",");
            igmp_vlan_table.push(
              <Table.Row key={`igmp_vlan_table_row_${i}`}>
                <Table.Cell>{pval[0]}</Table.Cell>
                <Table.Cell>{pval[1]}</Table.Cell>
                <Table.Cell>{pval[2]}</Table.Cell>
                <Table.Cell>{this.rpStatus(pval[3])}</Table.Cell>
                <Table.Cell>{pval[4]}</Table.Cell>
                <Table.Cell>{pval[5]}</Table.Cell>
                <Table.Cell>{pval[6]}</Table.Cell>
                <Table.Cell>{pval[7]}</Table.Cell>
                <Table.Cell>{pval[8]}</Table.Cell>
                <Table.Cell>{pval[9]}</Table.Cell>
              </Table.Row>
            );
          }
        });
      } else {
        igmp_vlan_table.push(
          <Table.Row key={`No entries`} colSpan="10">
            <Table.Cell colSpan="10">No entries</Table.Cell>
          </Table.Row>
        );

      }
      //router_port_table
      if (igmpConfig[1]) {
        router_port_data = igmpConfig[1].split("/");
        router_port_data.forEach((val, i) => {
          if (val) {
            let pval = val.split(",");
            router_port_table.push(
              <Table.Row key={`router_port_table_row_${i}`}>
                <Table.Cell>{pval[0]}</Table.Cell>
                <Table.Cell style={{ minWidth: "200px" }}>{this.statisticsQuerierStatus(pval[1])}</Table.Cell>
              </Table.Row>
            );
          }
        });
      }

      return (
        <Segment.Group>
          <SegmentHeader>
            IGMP Snooping Status
            <HeaderRefreshBar
              className="segment"
              checked={this.state.isAutoRefresh}
              handleAutoRefresh={this.handleAutoRefresh}
            />
          </SegmentHeader>
          <Segment>
            <h4>Statistics</h4>
            <Table striped unstackable celled compact textAlign="center">
              <Table.Header>
                <StatisticsHeader />
              </Table.Header>
              <Table.Body>{igmp_vlan_table}</Table.Body>
            </Table>
            <h4>Router Port</h4>
            <Table collapsing striped unstackable celled compact textAlign="center">
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Port Status</Table.HeaderCell>
                  <Table.HeaderCell>Status</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>{router_port_table}</Table.Body>
            </Table>
            <Divider />
            <BtnWrapper>
              <SemanticButton title="Clear" disabled={this.props.readOnly} onClick={this.clearCounters} />
            </BtnWrapper>
          </Segment>
        </Segment.Group>
      );
    }
  }
}
function StatisticsHeader() {
  return(
    <Table.Row>
                  <Table.HeaderCell>
                    VLAN<br></br>ID
                  </Table.HeaderCell>
                  <Table.HeaderCell>
                    Querier<br></br>Version
                  </Table.HeaderCell>
                  <Table.HeaderCell>
                    Host<br></br>Version
                  </Table.HeaderCell>
                  <Table.HeaderCell>
                    Querier<br></br>Status
                  </Table.HeaderCell>
                  <Table.HeaderCell>
                    Queries<br></br>Transmitted
                  </Table.HeaderCell>
                  <Table.HeaderCell>
                    Queries<br></br>Received
                  </Table.HeaderCell>
                  <Table.HeaderCell>
                    V1 Reports<br></br>Received
                  </Table.HeaderCell>
                  <Table.HeaderCell>
                    V2 Reports<br></br>Received
                  </Table.HeaderCell>
                  <Table.HeaderCell>
                    V3 Reports<br></br>Received
                  </Table.HeaderCell>
                  <Table.HeaderCell>
                    V2 Leaves<br></br>Received
                  </Table.HeaderCell>
                </Table.Row>
  );
}
export default WithData(IpmcIgmpsStatus, "/stat/ipmc_status?ipmc_version=1", isAutoRefresh);