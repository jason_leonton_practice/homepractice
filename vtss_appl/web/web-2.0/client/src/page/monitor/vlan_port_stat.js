import React, { Component } from "react";
import { Segment, Form, Select, Checkbox } from "semantic-ui-react";

import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";

class VlanPortStat extends Component {
  state = {
    isAutoRefresh: false,
    displayUser: "",
    userCombo: 1,
    userComboOptions: [
      { key: 1, text: "Combined", value: 1 },
      { key: 2, text: "Admin", value: 2 }
    ],
    tableBodyRows: []
  };

  handleAutoRefresh = () => {
    this.setState(
      prevState => ({ isAutoRefresh: !prevState.isAutoRefresh }),
      () => this.props.autoRefresh(this.state.isAutoRefresh)
    );
  };

  onChangeUserCombo = (e, { value }) => {
    this.setState({ userCombo: value });
    this.props.querySearch(`/stat/vlan_port_stat?user=${value}&sid=-1`);
  };

  createUserCombo = (all_user_names_and_ids, currently_selected) => {
    let userComboOptions = [];

    all_user_names_and_ids.forEach(username_and_id => {
      // Format: user_name|user_id
      const username = username_and_id.split("|")[0];
      const id = username_and_id.split("|")[1];

      userComboOptions.push({ key: id, text: username, value: parseInt(id, 10) });
    });

    this.setState({
      userComboOptions,
      displayUser: `${currently_selected} User${currently_selected === "Combined" ? "s" : ""}`
    });
  };

  createPortInfoTable = port_infos => {
    let tableBodyRows = [];

    if (port_infos.length < 2) {
      tableBodyRows.push(
        <Table.Row key="no_data" textAlign="center">
          <Table.Cell colSpan="8">No data exists for the selected user</Table.Cell>
        </Table.Row>
      );
    } else {
      port_infos.forEach((port_infos, i) => {
        if (i === 0) return false;

        const port_info = port_infos.split("|");

        //------------------------------------------------------------------------------
        // [port_info_N] = uport|conflicts_exist|pvid|pvid_set|ingress_filter|ingress_filter_set|frame_type|frame_type_set|tx_tag_type|tx_tag_type_set|port_type|port_type_set|untagged_vid
        //                   0         1           2     3            4             5               6           7               8              9           10           11          12
        //------------------------------------------------------------------------------

        tableBodyRows.push(
          <Table.Row textAlign="center" key={`port_info_${i}`}>
            {/* Port number */}
            <Table.Cell>{port_info[0]}</Table.Cell>
            {/*  Port Type (unaware, C-port, S-port, S-custom-port) */}
            <Table.Cell>{parseInt(port_info[11], 10) ? port_info[10] : ""}</Table.Cell>
            {/* Ingress filtering */}
            <Table.Cell>
              {parseInt(port_info[5], 10) ? (
                <Checkbox checked={parseInt(port_info[4], 10) ? true : false} readOnly />
              ) : (
                <Checkbox checked={false} readOnly />
              )}
            </Table.Cell>
            {/* Acceptable Frame type (All, Tagged, Untagged) */}
            <Table.Cell>{parseInt(port_info[7], 10) ? port_info[6] : ""}</Table.Cell>
            {/* PVID */}
            <Table.Cell>{parseInt(port_info[3], 10) ? port_info[2] : ""}</Table.Cell>
            {/* Tx Tag (Untag PVID, Untag All, Tag All - and unlike everywhere else also the option of untagging a special VID, not equal to PVID. */}
            <Table.Cell>{parseInt(port_info[9], 10) ? port_info[8] : ""}</Table.Cell>
            {/* UVID. Will always be 0 unless Tx Tag type is overridden and it's either Untag This or Tag This. */}
            <Table.Cell>{parseInt(port_info[12], 10) ? port_info[12] : ""}</Table.Cell>
            {/* Conflicts */}
            <Table.Cell>{parseInt(port_info[1], 10) ? "Yes" : "No"}</Table.Cell>
          </Table.Row>
        );
      });
    }
    this.setState({ tableBodyRows });
  };

  processUpdate = () => {
    /* 
      Format: [all_user_names_and_ids]#[port_infos]
      Where
      [all_user_names_and_ids] = [user_name_id_1]/[user_name_id_2]/.../[user_name_id_n]
      [port_infos]             = requested_user_name/[port_info_1]/[port_info_2]/.../[port_info_n]
    */
    const values = this.props.data.split("#");
    const all_user_names_and_ids = values[0].split("/");
    const port_infos = values[1].split("/");

    this.createUserCombo(all_user_names_and_ids, port_infos[0] /* Currently selected */);
    this.createPortInfoTable(port_infos);
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.prevState) !== JSON.stringify(prevState)
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (JSON.stringify(this.props) !== JSON.stringify(prevProps)) this.processUpdate();
  }

  componentDidMount() {
    this.processUpdate();
  }

  render() {
    return (
      <Segment.Group>
        <SegmentHeader>
          VLAN Port Status for {this.state.displayUser}
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Form size="mini">
            <Form.Group>
              <Form.Field
                width="4"
                compact
                control={Select}
                name="userCombo"
                value={this.state.userCombo}
                label="User Combo"
                options={this.state.userComboOptions}
                onChange={this.onChangeUserCombo}
              />
            </Form.Group>
          </Form>
          <Table celled compact>
            <Table.Header>
              <Table.Row textAlign="center">
                <Table.HeaderCell>Port</Table.HeaderCell>
                <Table.HeaderCell>Port Type</Table.HeaderCell>
                <Table.HeaderCell>Ingress Filtering</Table.HeaderCell>
                <Table.HeaderCell>Frame Type</Table.HeaderCell>
                <Table.HeaderCell>Port VLAN ID</Table.HeaderCell>
                <Table.HeaderCell>Tx Tag</Table.HeaderCell>
                <Table.HeaderCell>Untagged VLAN ID</Table.HeaderCell>
                <Table.HeaderCell>Conflicts</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{this.state.tableBodyRows}</Table.Body>
          </Table>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(VlanPortStat, "/stat/vlan_port_stat?user=0&sid=-1", false);
