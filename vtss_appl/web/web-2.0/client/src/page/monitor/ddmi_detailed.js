import React, { Component } from "react";
import { Segment } from "semantic-ui-react";
import qs from "querystring";

import { SegmentHeader, InnerTable as Table, Dropdown } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";

const isAutoRefresh = false;

class DDMIdetailed extends Component {
  constructor(props) {
    super(props);
    this.state = { isAutoRefresh, portOptions: [], portno: "" };
  }

  handleDropdownChange = (e, { value }) => {
    this.setState({ portno: value }, () => {
      // 更新網址
      this.props.history.push(`/monitor/ddmi/detailed?port=${this.state.portno}`);
      // 發出http request
      this.props.querySearch(`/stat/ddmi_detailed?port=${this.state.portno}`);
    });
  };

  handleAutoRefresh = () => {
    this.setState(
      prevState => ({ isAutoRefresh: !prevState.isAutoRefresh }),
      () => this.props.autoRefresh(this.state.isAutoRefresh)
    );
  };

  // 由於dropdown 需要由state來控制
  // 因此componentDidmount, componentDidUpdat皆須執行updateSelect()
  updateSelect = () => {
    if (this.props.data) {
      let data = this.props.data.split("|");
      let selPort = data[0].split(",");
      let options = [];

      // 建立dropdown options
      for (let i = 0; i < selPort.length; i++) {
        options.push({ key: selPort[i], value: selPort[i], text: `Port ${selPort[i]}` });
      }

      // 根據url port=x 設為x為 initial value
      const values = qs.parse(this.props.location.search.slice(1));
      if (values.port) {
        this.setState({ portno: values.port });
      } else {
        this.setState({ portno: selPort[0] });
      }

      this.setState({ portOptions: options });
    }
  };

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps) !== JSON.stringify(this.props)) {
      this.updateSelect();
    }
  }

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  componentDidMount() {
    this.updateSelect();
  }

  render() {
    let overview_table_body = [];
    let ptpData_table_body = [];

    if (this.props.data) {
      // Format:
      /* 8,12,14|id/ZyXEL/SFP-SX-D/S111132000061/V1.0/2011-08-10/1000BASE-SX|1/2/3/4/5|18/2/1/4/5|65535/2/4/4/2|1/2/3/4/5|18/2/1/4/5 */
      let data = this.props.data.split("|");
      let overview = data[1].split("/"); // ["12", "-", "-", "-", "-", "-", "-"]
      let temp, voltage, tx_bias, tx_pwr, rx_pwr;
      let overview_head = ["Vendor", "Part Number", "Serial Number", "Revision", "Data Code", "Transeiver"];
      let param_head = ["Temperature(C)", "Voltage(V)", "Tx Bias(mA)", "Tx Power(mW)", "Rx Power(mW)"];
      let param_data = [temp, voltage, tx_bias, tx_pwr, rx_pwr];

      // Transeiver Info Table
      overview_head.forEach((cell_value, i) => {
        overview_table_body.push(
          <Table.Row key={`overview_row_${i}`}>
            <Table.Cell style={{ minWidth: "100px" }}>{cell_value}</Table.Cell>
            <Table.Cell style={{ minWidth: "100px" }} textAlign="right">
              {overview[i + 1]}
            </Table.Cell>
          </Table.Row>
        );
      });

      // DDMI Information Table
      // -2 是因為前兩項為 portSel和overview
      for (let idx = 0; idx < data.length - 2; idx++) {
        param_data[idx] = data[idx + 2].split("/");
        let values = param_data[idx];

        ptpData_table_body.push(
          <Table.Row key={`ptp_row_${idx}`}>
            <Table.Cell>{param_head[idx]}</Table.Cell>
            {values.map((value, i) => (
              <Table.Cell key={`ptp_cell_${idx}_${i}`}>{value}</Table.Cell>
            ))}
          </Table.Row>
        );
      }
    }

    return (
      <div>
        <Segment.Group>
          <SegmentHeader>
            {`Transceiver Information - `}
            <HeaderRefreshBar
              className="segment"
              checked={this.state.isAutoRefresh}
              handleAutoRefresh={this.handleAutoRefresh}
            />
            <Dropdown
              compact
              selection
              options={this.state.portOptions}
              value={this.state.portno}
              onChange={this.handleDropdownChange}
            />
          </SegmentHeader>
          <Segment>
            <Table definition celled compact collapsing striped>
              <Table.Body>{overview_table_body}</Table.Body>
            </Table>

            <h4>DDMI Information</h4>
            <p>
              <span>
                ++ : high alarm, + : high warning, - : low warning, -- : low alarm.
                <br />
              </span>
              <span>Tx: transmit, Rx: receive, mA: milliamperes, mW: milliwatts.</span>
            </p>

            <Table celled striped compact textAlign="center">
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Type</Table.HeaderCell>
                  <Table.HeaderCell>Current</Table.HeaderCell>
                  <Table.HeaderCell>High Alarm Threshold</Table.HeaderCell>
                  <Table.HeaderCell>High Warn Threshold</Table.HeaderCell>
                  <Table.HeaderCell>Low Warn Threshold</Table.HeaderCell>
                  <Table.HeaderCell>Low Alarm Threshold</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>{ptpData_table_body}</Table.Body>
            </Table>
          </Segment>
        </Segment.Group>
      </div>
    );
  }
}

export default WithData(DDMIdetailed, "/stat/ddmi_detailed", false, ["port"]);
