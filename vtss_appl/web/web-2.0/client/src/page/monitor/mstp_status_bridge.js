import React, { Component } from "react";
import { Segment, Table } from "semantic-ui-react";

import { SegmentHeader } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";

const isAutoRefresh = false;
const firstCellStyle = {};

class MstpStatusBridge extends Component {
  state = { isAutoRefresh };

  handleAutoRefresh = () => {
    this.setState(
      prevState => ({ isAutoRefresh: !prevState.isAutoRefresh }),
      () => this.props.autoRefresh(this.state.isAutoRefresh)
    );
  };

  genBridgeStatusBody = (pval = []) => {
    const msti = parseInt(pval[0], 10);

    return (
      <React.Fragment key="bridge_status_table_body">
        <Table.Row>
          <Table.Cell style={firstCellStyle}>Bridge Instance</Table.Cell>
          <Table.Cell>{pval[1]}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell style={firstCellStyle}>Bridge ID</Table.Cell>
          <Table.Cell>{pval[2]}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell style={firstCellStyle}>Root ID</Table.Cell>
          <Table.Cell>{pval[3]}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell style={firstCellStyle}>Root Cost</Table.Cell>
          <Table.Cell>{pval[5]}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell style={firstCellStyle}>Root Port</Table.Cell>
          <Table.Cell>{pval[4]}</Table.Cell>
        </Table.Row>
        {msti === 0 && (
          <>
            <Table.Row>
              <Table.Cell style={firstCellStyle}>Regional Root</Table.Cell>
              <Table.Cell>{pval[10]}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell style={firstCellStyle}>Internal Root Cost</Table.Cell>
              <Table.Cell>{pval[9]}</Table.Cell>
            </Table.Row>
          </>
        )}
        <Table.Row>
          <Table.Cell style={firstCellStyle}>Topology Flag</Table.Cell>
          <Table.Cell>{pval[6]}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell style={firstCellStyle}>Topology Change Count</Table.Cell>
          <Table.Cell>{pval[8]}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell style={firstCellStyle}>Topology Change Last</Table.Cell>
          <Table.Cell>{pval[7]}</Table.Cell>
        </Table.Row>
      </React.Fragment>
    );
  };

  genCistAggrBody = (pval = [], i) => {
    return (
      <React.Fragment key={`cist_aggregation_state_table_body_${i}`}>
        <Table.Row textAlign="center">
          {pval.map((cell_value, i) => {
            if (i === 0) {
              /* <Table.Cell>{pval[0]}</Table.Cell> // SPOM only */
              return false;
            }
            return <Table.Cell key={`cist_aggr_cell_${i}`}>{cell_value}</Table.Cell>;
          })}
        </Table.Row>
      </React.Fragment>
    );
  };

  render() {
    const data = this.props.data.split("|");
    let table_bridge_body = [];
    let table_cist_aggr_body = [];
    let rows = 0;
    let port_type = "";

    if (data) {
      data.forEach((row, i) => {
        if (row) {
          const pval = row.split("/");

          if (i === 0) {
            port_type = pval[1];
            table_bridge_body.push(this.genBridgeStatusBody(pval));
          } else {
            table_cist_aggr_body.push(this.genCistAggrBody(pval, i));
            rows = i + 1; // i start from 0
          }
        }
      });
    }

    if (rows === 0) {
      table_bridge_body.push(
        <Table.Row key="stp_bridge_table_no_row">
          <Table.Cell colSpan="2">No STP bridge status active</Table.Cell>
        </Table.Row>
      );

      table_cist_aggr_body.push(
        <Table.Row key="port_aggr_table_no_row" textAlign="center">
          <Table.Cell colSpan="8">No ports or aggregations active</Table.Cell>
        </Table.Row>
      );
    }

    return (
      <Segment.Group>
        <SegmentHeader>
          STP Detailed Bridge Status
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Table compact celled collapsing striped>
            <Table.Header>
              <Table.Row textAlign="center">
                <Table.HeaderCell colSpan="2">STP Bridge Status</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{table_bridge_body}</Table.Body>
          </Table>

          <Table compact celled striped>
            <Table.Header>
              <Table.Row textAlign="center">
                <Table.HeaderCell colSpan="8">{port_type} Ports & Aggregations State</Table.HeaderCell>
              </Table.Row>
              <Table.Row textAlign="center">
                <Table.HeaderCell>Port</Table.HeaderCell>
                <Table.HeaderCell>Port ID</Table.HeaderCell>
                <Table.HeaderCell>Role</Table.HeaderCell>
                <Table.HeaderCell>State</Table.HeaderCell>
                <Table.HeaderCell>Path Cost</Table.HeaderCell>
                <Table.HeaderCell>Edge</Table.HeaderCell>
                <Table.HeaderCell>Point-to-Point</Table.HeaderCell>
                <Table.HeaderCell>Uptime</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{table_cist_aggr_body}</Table.Body>
          </Table>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(MstpStatusBridge, "/stat/rstp_status_bridge", false, ["bridge"]);
