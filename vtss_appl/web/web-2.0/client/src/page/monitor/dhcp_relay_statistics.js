import React, { Component } from "react";
import { Segment } from "semantic-ui-react";

import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { HeaderRefreshBar, SemanticButton, BtnWrapper } from "../../components/semantic-component";
import WithData from "../../utils/withData";

const isAutoRefresh = false;

class DhcpRelayStatistics extends Component {
  state = { isAutoRefresh };

  handleAutoRefresh = () => {
    this.setState({ isAutoRefresh: !this.state.isAutoRefresh }, () => {
      this.props.autoRefresh(this.state.isAutoRefresh);
    });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  handleClear = () => {
    this.props.querySearch("/stat/dhcp_relay_statistics?clear=1", false);
  };

  render() {
    let serverStatTable = [];
    let clientStatTable = [];

    if (this.props.data) {
      let values = this.props.data.split(",");
      //serverStatTable
      let serverStatData = values[0].split("/");
      serverStatTable.push(
        <Table.Row key="serverStatData">
          <Table.Cell>{serverStatData[0]}</Table.Cell>
          <Table.Cell>{serverStatData[1]}</Table.Cell>
          <Table.Cell>{serverStatData[2]}</Table.Cell>
          <Table.Cell>{serverStatData[3]}</Table.Cell>
          <Table.Cell>{serverStatData[4]}</Table.Cell>
          <Table.Cell>{serverStatData[5]}</Table.Cell>
          <Table.Cell>{serverStatData[6]}</Table.Cell>
          <Table.Cell>{serverStatData[7]}</Table.Cell>
        </Table.Row>
      );

      //clientStatTable
      let clientStatData = values[1].split("/");
      clientStatTable.push(
        <Table.Row key="clientStatData">
          <Table.Cell>{clientStatData[0]}</Table.Cell>
          <Table.Cell>{clientStatData[1]}</Table.Cell>
          <Table.Cell>{clientStatData[2]}</Table.Cell>
          <Table.Cell>{clientStatData[3]}</Table.Cell>
          <Table.Cell>{clientStatData[4]}</Table.Cell>
          <Table.Cell>{clientStatData[5]}</Table.Cell>
          <Table.Cell>{clientStatData[6]}</Table.Cell>
        </Table.Row>
      );
    }

    return (
      <Segment.Group>
        <SegmentHeader>
          DHCP Relay Statistics
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <h4>Server Statistics</h4>
          <Table striped unstackable celled compact textAlign="center">
            <ServerStatisticsHeader />
            <Table.Body>{serverStatTable}</Table.Body>
          </Table>

          <h4>Server Statistics</h4>
          <Table striped unstackable celled compact textAlign="center">
            <ClientStatisticsHeader />
            <Table.Body>{clientStatTable}</Table.Body>
          </Table>
          <BtnWrapper>
            <SemanticButton title="clear" disabled={this.props.readOnly} onClick={this.handleClear} />
          </BtnWrapper>
        </Segment>
      </Segment.Group>
    );
  }
}

function ServerStatisticsHeader() {
  return (
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>
          Transmit
          <br />
          to Server
        </Table.HeaderCell>
        <Table.HeaderCell>
          Transmit
          <br />
          Error
        </Table.HeaderCell>
        <Table.HeaderCell>
          Receive
          <br />
          from Server
        </Table.HeaderCell>
        <Table.HeaderCell>
          Receive Missing
          <br />
          Agent Option
        </Table.HeaderCell>
        <Table.HeaderCell>
          Receive Missing
          <br />
          Circuit ID
        </Table.HeaderCell>
        <Table.HeaderCell>
          Receive Missing
          <br />
          Remote ID
        </Table.HeaderCell>
        <Table.HeaderCell>
          Receive Bad
          <br />
          Circuit ID
        </Table.HeaderCell>
        <Table.HeaderCell>
          Receive Bad
          <br />
          Remote ID
        </Table.HeaderCell>
      </Table.Row>
    </Table.Header>
  );
}

function ClientStatisticsHeader() {
  return (
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>
          Transmit
          <br />
          to Client
        </Table.HeaderCell>
        <Table.HeaderCell>
          Transmit
          <br />
          Error
        </Table.HeaderCell>
        <Table.HeaderCell>
          Receive
          <br />
          from Client
        </Table.HeaderCell>
        <Table.HeaderCell>
          Receive
          <br />
          Agent Option
        </Table.HeaderCell>
        <Table.HeaderCell>
          Replace
          <br />
          Agent Option
        </Table.HeaderCell>
        <Table.HeaderCell>
          Keep
          <br />
          Agent Option
        </Table.HeaderCell>
        <Table.HeaderCell>
          Drop
          <br />
          Agent Option
        </Table.HeaderCell>
      </Table.Row>
    </Table.Header>
  );
}

export default WithData(DhcpRelayStatistics, "/stat/dhcp_relay_statistics", isAutoRefresh);
