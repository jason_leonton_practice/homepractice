import React from "react";
import { Segment, Table } from "semantic-ui-react";

import { SegmentHeader } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import ReactApexChart from "react-apexcharts";
import ApexCharts from "apexcharts";
import WithData from "../../utils/withData";
import "./perf_cpuload.scss";

const doesInitialAutoRefresh = true;
let lastDate = 0;
let hundredmsDataForChart = [];
let onesDataForChart = [];
let tensDataForChart = [];
let XAXISRANGE = 60000;
let TICKINTERVAL = 3000;

function getDayWiseTimeSeries(baseval) {
  let x = baseval;
  let y = 0;

  hundredmsDataForChart.push({ x, y });
  onesDataForChart.push({ x, y });
  tensDataForChart.push({ x, y });
  lastDate = baseval;
  baseval += TICKINTERVAL;
}

getDayWiseTimeSeries(new Date().getTime());

class PerfCpuLoad extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAutoRefresh: doesInitialAutoRefresh,
      data: [],
      options: {
        chart: {
          id: "realtime",
          animations: {
            enabled: true,
            easing: "linear",
            dynamicAnimation: {
              speed: 1000
            }
          },
          toolbar: {
            show: true
          },
          zoom: {
            enabled: false
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "smooth",
          width: 1
        },
        markers: {
          size: 0
        },
        xaxis: {
          type: "datetime",
          range: XAXISRANGE,
          labels: {
            formatter: value => {
              const categoryTime = new Date(value);
              const hours = categoryTime.getHours();
              const minutes = categoryTime.getMinutes();
              const minutesString = minutes === 0 ? `${minutes}0` : minutes < 10 ? `0${minutes}` : minutes;
              const seconds = categoryTime.getSeconds();
              const secondsString = seconds === 0 ? `${seconds}0` : seconds < 10 ? `0${seconds}` : seconds;

              return `${hours}:${minutesString}:${secondsString}`;
            }
          }
        },
        yaxis: {
          title: {
            text: "Percentage(%)"
          },
          min: 0,
          max: 100
        },
        legend: {
          show: true,
          position: "bottom",
          horizontalAlign: "right"
        }
      },
      series: [
        {
          name: "100ms",
          data: hundredmsDataForChart.slice()
        },
        {
          name: "1s",
          data: onesDataForChart.slice()
        },
        {
          name: "10s",
          data: tensDataForChart.slice()
        }
      ]
    };
  }

  handleAutoRefresh = () => {
    this.setState(
      prevState => ({ isAutoRefresh: !prevState.isAutoRefresh }),
      () => this.props.autoRefresh(this.state.isAutoRefresh)
    );
  };

  getNewSeries(baseval, yrange) {
    let newDate = baseval + TICKINTERVAL;
    lastDate = newDate;

    if (hundredmsDataForChart.length >= 100) {
      hundredmsDataForChart.splice(0, 50);
      onesDataForChart.splice(0, 50);
      tensDataForChart.splice(0, 50);
    }
    hundredmsDataForChart.push({
      x: newDate,
      y: this.state.hundredMSecData
    });

    onesDataForChart.push({
      x: newDate,
      y: this.state.oneSecData
    });

    tensDataForChart.push({
      x: newDate,
      y: this.state.tenSecData
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.data !== this.props.data || nextState !== this.state) return true;

    return false;
  }

  componentDidUpdate(prevProps) {
    const { data } = this.props;
    const splitData = data.split("|");

    if (data !== prevProps.data) {
      this.setState({
        hundredMSecData: splitData[0],
        oneSecData: splitData[1],
        tenSecData: splitData[2]
      });
    }
  }

  intervals() {
    this.refreshID = setInterval(() => {
      this.getNewSeries(lastDate);

      ApexCharts.exec("realtime", "updateSeries", [
        {
          data: hundredmsDataForChart
        },
        {
          data: onesDataForChart
        },
        {
          data: tensDataForChart
        }
      ]);
    }, 3000);
  }

  componentDidMount() {
    this.intervals();
  }

  componentWillUnmount() {
    clearInterval(this.refreshID);
  }

  render() {
    const { hundredMSecData, oneSecData, tenSecData, options, series } = this.state;
    return (
      <Segment.Group className="cpu-load">
        <SegmentHeader>
          CPU Load
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment className="device-info-segment">
          <Table basic="very" celled collapsing>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Time</Table.HeaderCell>
                <Table.HeaderCell>Percentage</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row className="hundredms-color">
                <Table.Cell>100ms</Table.Cell>
                <Table.Cell>{hundredMSecData}%</Table.Cell>
              </Table.Row>
              <Table.Row className="ones-color">
                <Table.Cell>1s</Table.Cell>
                <Table.Cell>{oneSecData}%</Table.Cell>
              </Table.Row>
              <Table.Row className="tens-color">
                <Table.Cell>10s</Table.Cell>
                <Table.Cell>{tenSecData}%</Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
          <ReactApexChart options={options} series={series} type="line" height="auto" />
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(PerfCpuLoad, "/stat/cpuload", doesInitialAutoRefresh);
