import React, { Component } from "react";
import { Segment, Table } from "semantic-ui-react";

import { SegmentHeader } from "../../components/styled_component";
import { SemanticButton, BtnWrapper, HeaderRefreshBar } from "../../components/semantic-component";
import WithData from "../../utils/withData";

const isAutoRefresh = false;

class IcfgConfSave extends Component {
  state = { isAutoRefresh };

  handleAutoRefresh = () => {
    this.setState(
      prevState => ({ isAutoRefresh: !prevState.isAutoRefresh }),
      () => this.props.autoRefresh(this.state.isAutoRefresh)
    );
  };

  handleClear = () => {
    this.props.querySearch("/stat/rstp_statistics?clear=1", false);
  };

  render() {
    const data = this.props.data.split("|");
    let table_body = [];
    let rows = 0;

    if (data) {
      data.forEach((row, i) => {
        if (row) {
          const cell_value = row.split("/");
          rows = i + 1; // i start from 0

          table_body.push(
            <Table.Row key={`row_${i}`} textAlign="center">
              {cell_value.map((value, i) => {
                if (i === 0) {
                  return <Table.Cell key={`cell_${i}`}>{value}</Table.Cell>;
                }

                return <Table.Cell key={`cell_${i}`}>{value}</Table.Cell>;
              })}
            </Table.Row>
          );
        }
      });
    }

    if (rows === 0) {
      table_body.push(
        <Table.Row key="table_row_no_ports" textAlign="center">
          <Table.Cell colSpan="11">No ports enabled</Table.Cell>
        </Table.Row>
      );
    }

    return (
      <Segment.Group>
        <SegmentHeader>
          STP Statistics
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Table celled compact striped structured>
            <Table.Header>
              <Table.Row textAlign="center">
                <Table.HeaderCell rowSpan="2">Port</Table.HeaderCell>
                <Table.HeaderCell colSpan="4">Transmitted</Table.HeaderCell>
                <Table.HeaderCell colSpan="4">Received</Table.HeaderCell>
                <Table.HeaderCell colSpan="4">Discarded</Table.HeaderCell>
              </Table.Row>
              <Table.Row textAlign="center">
                <Table.HeaderCell>MSTP</Table.HeaderCell>
                <Table.HeaderCell>RSTP</Table.HeaderCell>
                <Table.HeaderCell>STP</Table.HeaderCell>
                <Table.HeaderCell>TCN</Table.HeaderCell>
                <Table.HeaderCell>MSTP</Table.HeaderCell>
                <Table.HeaderCell>RSTP</Table.HeaderCell>
                <Table.HeaderCell>STP</Table.HeaderCell>
                <Table.HeaderCell>TCN</Table.HeaderCell>
                <Table.HeaderCell>Unknown</Table.HeaderCell>
                <Table.HeaderCell>Illegal</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{table_body}</Table.Body>
          </Table>
          <BtnWrapper>
            <SemanticButton title="Clear" disabled={this.props.readOnly} onClick={this.handleClear} />
          </BtnWrapper>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(IcfgConfSave, "/stat/rstp_statistics", false);
