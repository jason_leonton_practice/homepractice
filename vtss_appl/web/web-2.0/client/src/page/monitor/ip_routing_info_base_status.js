import React, { Component } from "react";
import { Segment, Pagination, Form, Select, Divider, Icon, Input } from "semantic-ui-react";

import { HeaderRefreshBar } from "../../components/semantic-component";
import WithJsonData from "../../utils/withJsonData";
import { isIpStr, isWithinRange } from "../../utils/validate";
import { SegmentHeader, MsgLists, List, InnerTable as Table } from "../../components/styled_component";
import { pageLimitOptions, protocolOptions } from "../../utils/options";
import { compensateZero, valOrderCompare } from "../../utils/self-documenting_func";
import { TableHeader } from "../../components/table_header";

// todo list
// 1. filter input validation [checked!]
// 2. refreshbar (can't use withData since method are different) [checked!]
// 3. can I use resuable component in here [chekced!]
// 4. debouce when filtering []
// 5. sorting table by alphanumeric [checked!]
// 6. should modify compensateZero function [checked!]
// 7. if the input is invalid, should stop calling filter()
// 8. add Total count [checked!]

class IpRoutingInfoBaseStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAutoRefresh: false,
      totalCount: 0,
      _sort: "",
      _order: null,
      _startFromIP: "",
      _prefix: "",
      _protocol: "all",
      _nextHop: "",
      _limit: 20,
      _page: 1,
      pageResults: []
    };
  }

  filter = params => {
    let newState = { ...this.state, ...params };
    const _results = this.props.data.result;

    // filter protocol
    let filtered = _results.filter(({ key }) => {
      if (newState._protocol === "all") return true;

      return key.Protocol === newState._protocol;
    });

    // filter start from network
    filtered = filtered.filter(({ key }) => {
      if (newState._startFromIP === "") return true;

      return compensateZero(key.IpNetwork, ".") >= compensateZero(newState._startFromIP, ".");
    });

    // filter prefix
    filtered = filtered.filter(({ key }) => {
      if (newState._prefix === "") return true;

      return key.IpSubnetMaskLength === parseInt(newState._prefix);
    });

    // filter nextHop
    filtered = filtered.filter(({ key }) => {
      if (newState._nextHop === "") return true;
      if (newState._nextHop === "0.0.0.0") return key.Protocol === "protoConneted";

      return key.NextHop === newState._nextHop;
    });

    // sort table order in alphanumeric
    if (newState._sort && newState._order) {
      filtered.sort((a, b) => {
        //key: IpNetwork, IpSubnetMaskLength, Protocol, NextHop,
        //val: Selected, Metric, Ifindex, Distance, Uptime, Active(state)
        let _sort = newState._sort;
        let _order = newState._order;

        if (_sort === "Protocol") {
          let aVal = a.key.Protocol + a.val.Selected;
          let bVal = b.key.Protocol + b.val.Selected;

          return valOrderCompare("str", _order, aVal, bVal);
        }

        if (_sort === "Network/Prefix") {
          let aIpInt = compensateZero(a.key.IpNetwork, ".");
          let bIpInt = compensateZero(b.key.IpNetwork, ".");
          let aVal = compensateZero(`${aIpInt}/${a.key.IpSubnetMaskLength}`, "/");
          let bVal = compensateZero(`${bIpInt}/${b.key.IpSubnetMaskLength}`, "/");

          // i.e. ipInt: 192168010000 val: 192168010000018
          return valOrderCompare("num", _order, aVal, bVal);
        }

        if (_sort === "NextHop") {
          let aVal = compensateZero(a.key.NextHop, ".");
          let bVal = compensateZero(b.key.NextHop, ".");

          return valOrderCompare("num", _order, aVal, bVal);
        }

        if (_sort === "Distance") {
          let aVal = a.val.Distance;
          let bVal = b.val.Distance;

          return valOrderCompare("num", _order, aVal, bVal);
        }

        if (_sort === "Metric") {
          let aVal = a.val.Metric;
          let bVal = b.val.Metric;

          return valOrderCompare("num", _order, aVal, bVal);
        }

        if (_sort === "Interface") {
          let aVal = a.val.Ifindex;
          let bVal = b.val.Ifindex;

          return valOrderCompare("str", _order, aVal, bVal);
        }

        if (_sort === "Uptime (hh:mm:ss)") {
          let aVal = compensateZero(a.val.Uptime, ":");
          let bVal = compensateZero(b.val.Uptime, ":");

          return valOrderCompare("num", _order, aVal, bVal);
        }

        if (_sort === "State") {
          let aVal = a.val.Active;
          let bVal = b.val.Active;

          return valOrderCompare("num", _order, aVal, bVal);
        }

        return 0;
      });
    }

    // entries per page & pagination
    const rowsPerPage = newState._limit;
    const page = newState._page ? parseInt(newState._page) : this.state._page;
    const pageResults = filtered.slice((page - 1) * rowsPerPage, page * rowsPerPage);

    newState = Object.assign(newState, { pageResults, totalCount: filtered.length });

    this.setState(newState);
  };

  validate = (startFromIP, prefix, nextHop) => {
    let chkStartFromIp = isIpStr(startFromIP, false, "Network", 2);
    let chkPrefix = isWithinRange(prefix, 0, 32, "Prefix Length", "");
    let chkNextHop = isIpStr(nextHop, false, "NextHop", 2);

    if (!startFromIP) chkStartFromIp = { error: false, msg: "" };
    if (!prefix) chkPrefix = { error: false, msg: "" };
    if (!nextHop) chkNextHop = { error: false, msg: "" };

    return {
      error: {
        startFromIP: chkStartFromIp.error,
        prefix: chkPrefix.error,
        nextHop: chkNextHop.error
      },
      errMsg: {
        startFromIP: chkStartFromIp.msg,
        prefix: chkPrefix.msg,
        nextHop: chkNextHop.msg
      }
    };
  };

  onChangeTextInput = (e, { name, value }) => {
    if (value !== this.state[name]) {
      this.filter({ [name]: value, _page: 1 });
    }
  };

  onChangeSelectInput = (e, { name, value }) => {
    if (value !== this.state[name]) this.filter({ [name]: value, _page: 1 });
  };

  onChangePage = (event, data) => {
    const { activePage } = data;
    if (activePage !== this.state._page) {
      this.filter({ _page: activePage });
    }
  };

  handleSort = clickedColumn => {
    const { _sort, _order } = this.state;
    let newOrder = _order === "asc" ? "desc" : "asc";

    if (_sort !== clickedColumn) newOrder = "asc";

    this.filter({ _sort: clickedColumn, _page: 1, _order: newOrder });
  };

  handleAutoRefresh = () => {
    this.setState(
      prevState => ({ isAutoRefresh: !prevState.isAutoRefresh }),
      () => this.props.autoRefresh(this.state.isAutoRefresh)
    );
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  componentDidUpdate() {
    this.filter();
  }

  UNSAFE_componentWillMount() {
    this.filter();
  }

  render() {
    const pageResults = this.state.pageResults;
    const errors = this.validate(this.state._startFromIP, this.state._prefix, this.state._nextHop);
    const shouldMarkError = field => errors.error[field];

    return (
      <Segment.Group>
        <SegmentHeader>
          Routing Information Base
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Form size="mini">
            <Form.Group>
              <Form.Field
                width="4"
                control={Input}
                name="_startFromIP"
                label="Start from Network"
                value={this.state._startFromIP}
                onChange={this.onChangeTextInput}
                error={shouldMarkError("startFromIP")}
              />
              <Form.Field
                width="2"
                control={Input}
                name="_prefix"
                label="Prefix"
                value={this.state._prefix}
                onChange={this.onChangeTextInput}
                error={shouldMarkError("prefix")}
              />
              <Form.Field
                width="4"
                compact
                control={Select}
                name="_protocol"
                value={this.state._protocol}
                options={protocolOptions}
                onChange={this.onChangeSelectInput}
                label={{ children: "Protocol" }}
              />
              <Form.Field
                width="3"
                control={Input}
                name="_nextHop"
                label="NextHop"
                value={this.state._nextHop}
                onChange={this.onChangeTextInput}
                placeholder="0.0.0.0"
                error={shouldMarkError("nextHop")}
              />
              <Form.Field
                width="3"
                compact
                control={Select}
                name="_limit"
                value={this.state._limit}
                options={pageLimitOptions}
                onChange={this.onChangeSelectInput}
                label={{ children: "Entries per page" }}
              />
            </Form.Group>
          </Form>

          {/* Validated error message*/}
          {(shouldMarkError("prefix") || shouldMarkError("nextHop") || shouldMarkError("startFromIP")) && (
            <MsgLists>
              {shouldMarkError("startFromIP") && <List>{errors.errMsg.startFromIP}</List>}
              {shouldMarkError("prefix") && <List>{errors.errMsg.prefix}</List>}
              {shouldMarkError("nextHop") && <List>{errors.errMsg.nextHop}</List>}
            </MsgLists>
          )}

          <Divider />

          <div style={{ display: "flex", justifyContent: "space-between", fontSize: "13px" }}>
            <p style={{ marginBottom: "0" }}>
              <b>C</b> - connected, <b>S</b> - static, <b>O</b> - OSPF, <b>*</b> - selected route, <b>D</b> -
              DHCP installed route
            </p>
            <p>
              Total Count: <strong>{this.state.totalCount}</strong>
            </p>
          </div>

          <Table celled compact sortable>
            <TableHeader
              colTitles={[
                "Protocol",
                "Network/Prefix",
                "NextHop",
                "Distance",
                "Metric",
                "Interface",
                "Uptime (hh:mm:ss)",
                "State"
              ]}
              column={this.state._sort}
              order={this.state._order}
              sortable={true}
              handleSort={this.handleSort}
            />
            <Table.Body>
              {!pageResults
                ? null
                : pageResults.map((result, i) => {
                    const { key, val } = result;
                    const protocol = protoSelectedPrint(key.Protocol, val.Selected);
                    const networkPrefix = `${key.IpNetwork}/${key.IpSubnetMaskLength}`;
                    const nextHop = key.NextHop.match(/0.0.0.0/) ? "-" : key.NextHop;
                    const distance = val.Distance;
                    const metric = val.Metric;
                    const ifIndex = val.Ifindex;
                    const uptime = val.Uptime ? val.Uptime : "-";
                    const state = val.Active ? "Active" : "Inactive";

                    if (i === this.state._limit) {
                      return false;
                    }

                    return (
                      <Table.Row key={i}>
                        <Table.Cell>{protocol}</Table.Cell>
                        <Table.Cell>{networkPrefix}</Table.Cell>
                        <Table.Cell>{nextHop}</Table.Cell>
                        <Table.Cell>{distance}</Table.Cell>
                        <Table.Cell>{metric}</Table.Cell>
                        <Table.Cell>{ifIndex}</Table.Cell>
                        <Table.Cell>{uptime}</Table.Cell>
                        <Table.Cell>{state}</Table.Cell>
                      </Table.Row>
                    );
                  })}
            </Table.Body>
            <Table.Footer>
              <Table.Row textAlign="right">
                <Table.HeaderCell colSpan="8">
                  <Pagination
                    activePage={this.state._page}
                    totalPages={Math.ceil(this.state.totalCount / this.state._limit)}
                    size="mini"
                    onPageChange={this.onChangePage}
                    firstItem={{ content: <Icon name="angle double left" />, icon: true }}
                    lastItem={{ content: <Icon name="angle double right" />, icon: true }}
                    prevItem={{ content: <Icon name="angle left" />, icon: true }}
                    nextItem={{ content: <Icon name="angle right" />, icon: true }}
                  />
                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </Table>
        </Segment>
      </Segment.Group>
    );
  }
}

function protoSelectedPrint(proto, selected) {
  let output = selected ? " *" : "";

  switch (proto) {
    case "protoDhcp":
      return "D" + output;
    case "protoConneted":
      return "C" + output;
    case "protoStatic":
      return "S" + output;
    case "protoOspf":
      return "O " + output;
    case "protoUnknown":
    default:
      return "?";
  }
}

const body = { method: "ip.status.route_info.ipv4.get", params: [], id: "jsonrpc" };
export default WithJsonData(IpRoutingInfoBaseStatus, "/json_rpc", body);
