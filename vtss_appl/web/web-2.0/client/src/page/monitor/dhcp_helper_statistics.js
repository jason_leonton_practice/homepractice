import React, { Component } from "react";
import { Segment } from "semantic-ui-react";

import WithData from "../../utils/withData";
import { InnerTable as Table, Dropdown, SegmentHeader } from "../../components/styled_component";
import { HeaderRefreshBar, SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { genOptions, genArrayStr } from "../../utils/dyform";

const isAutoRefresh = false;

class DhcpHelperStatistics extends Component {
  constructor(props) {
    super(props);

    //select port value
    let selectedPort = this.props.data.split(",")[1];
    //select port option generate: port的最大數是從config.js的configNormalPortMax取出的
    const portOption = genOptions({
      // eslint-disable-next-line no-undef
      value: genArrayStr(1, configNormalPortMax, ""),
      // eslint-disable-next-line no-undef
      text: genArrayStr(1, configNormalPortMax, "Port ")
    });

    //mode option 處理
    //slice處理回傳封包多傳一個/在結尾，所以會多一個
    let modeData = this.props.data
      .split(",")[0]
      .split("/")
      .filter(val => val.length > 0);
    console.log("modeData");
    console.table(modeData);

    let selectedMode = parseInt(modeData.splice(0, 1));
    console.log(" selectedMode", selectedMode);

    if (selectedMode === 5) {
      selectedMode = -1;
    }
    console.log(" selectedMode", selectedMode);
    let modeOption = modeData.map((val, index) => {
      return { key: `mode option ${index}`, value: index, text: val };
    });
    modeOption = [{ key: `mode option -1`, value: -1, text: "Combined" }].concat(modeOption);

    this.state = {
      isAutoRefresh,
      selectedPort,
      portOption,
      selectedMode,
      modeOption,
      tableBody: []
    };
  }

  //這裡會更新props
  handleDropdownChange = (e, { name, value }) => {
    if (name === "selectedMode") {
    }
    this.setState({ [name]: value }, () => {
      this.props.history.push(`?port=${this.state.selectedPort}&user=${this.state.selectedMode}`);
      this.props.querySearch(
        `/stat/dhcp_helper_statistics?port=${this.state.selectedPort}&user=${this.state.selectedMode}`
      );
    });
  };

  handleAutoRefresh = () => {
    this.setState(
      prevState => ({ isAutoRefresh: !prevState.isAutoRefresh }),
      () => this.props.autoRefresh(this.state.isAutoRefresh)
    );
  };

  handleClear = () => {
    this.props.querySearch(
      `/stat/dhcp_helper_statistics?clear=1&port=${this.state.selectedPort}&user=${this.state.selectedMode}`,
      false
    );
  };

  processUpdate = () => {
    this.columnWith1 = 7;
    this.columnWith2 = 1;

    let [modePkg, selectedPort, tablePkg] = this.props.data.split(",");

    //dropdown 處理
    //slice處理回傳封包多傳一個/在結尾，所以會多一個
    let selectedMode = parseInt(modePkg.split("/")[0]);
    if (selectedMode === 5) {
      selectedMode = -1;
    }
    console.log("TCL: processUpdate -> selectedMode", selectedMode);

    //table
    let tableBody = [];
    const tableRowmName = [
      "Discover",
      "Offer",
      "Request",
      "Decline",
      "ACK",
      "NAK",
      "Release",
      "Inform",
      "Lease Query",
      "Lease Unassigned",
      "Lease Unknown",
      "Lease Active"
    ];
    let tableRowVal = tablePkg.split("/");
    const optionalTableRowName = ["Discarded Checksum Error", "Discarded from Untrusted"];
    let optionalTableRowVal = tableRowVal.splice(24); //splice(24)保留前24個，剩下的給optionalTableRowVal

    for (let i = 0; i < 12; i++) {
      tableBody.push(
        <Table.Row key={`table row ${i}`}>
          <Table.Cell width={this.columnWith1}>{`Rx ${tableRowmName[i]}`}</Table.Cell>
          <Table.Cell width={this.columnWith2}>{tableRowVal[2 * i]}</Table.Cell>
          <Table.Cell width={this.columnWith1}>{`Tx ${tableRowmName[i]}`}</Table.Cell>
          <Table.Cell width={this.columnWith2}>{tableRowVal[2 * i + 1]}</Table.Cell>
        </Table.Row>
      );
    }

    if (optionalTableRowVal.length === 2) {
      tableBody.push(
        <Table.Row key={`optional table row 1`}>
          <Table.Cell width={this.columnWith1}>{`Rx ${optionalTableRowName[0]}`}</Table.Cell>
          <Table.Cell width={this.columnWith2}>{optionalTableRowVal[0]}</Table.Cell>
          <Table.Cell colSpan={2} />
        </Table.Row>
      );
      tableBody.push(
        <Table.Row key={`optional table row 2`}>
          <Table.Cell width={this.columnWith1}>{`Rx ${optionalTableRowName[1]}`}</Table.Cell>
          <Table.Cell width={this.columnWith2}>{optionalTableRowVal[1]}</Table.Cell>
          <Table.Cell colSpan={2} />
        </Table.Row>
      );
    } else {
      tableBody.push(
        <Table.Row key={`optional table row 1`}>
          <Table.Cell width={this.columnWith1}>{`Rx ${optionalTableRowName[0]}`}</Table.Cell>
          <Table.Cell width={this.columnWith2}>{optionalTableRowVal[0]}</Table.Cell>
          <Table.Cell colSpan={2} />
        </Table.Row>
      );
    }

    this.setState({ selectedMode, selectedPort, tableBody });
  };

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps) !== JSON.stringify(this.props)) {
      this.processUpdate();
    }
  }

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  componentDidMount() {
    this.processUpdate();
  }

  render() {
    const table = (
      <React.Fragment>
        <Table celled compact striped>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell colSpan="2">Receive Packets</Table.HeaderCell>
              <Table.HeaderCell colSpan="2">Transmit Packets</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>{this.state.tableBody}</Table.Body>
        </Table>
      </React.Fragment>
    );
    return (
      <Segment.Group>
        <SegmentHeader>
          {`DHCP Detailed Statistics ------`}
          <Dropdown
            selection
            name={"selectedMode"}
            value={this.state.selectedMode}
            options={this.state.modeOption}
            onChange={this.handleDropdownChange}
          />
          <Dropdown
            selection
            name={"selectedPort"}
            // value={this.state.selectedPort}
            value={this.state.selectedPort}
            options={this.state.portOption}
            onChange={this.handleDropdownChange}
          />

          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          {table}
          <BtnWrapper>
            <SemanticButton title="clear" disabled={this.props.readOnly} onClick={this.handleClear} />
          </BtnWrapper>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(DhcpHelperStatistics, "/stat/dhcp_helper_statistics", isAutoRefresh, [
  "port",
  "user",
  "clear"
]);
