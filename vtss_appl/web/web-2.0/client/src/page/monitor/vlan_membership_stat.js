import React, { Component } from "react";
import { Segment, Form, Select, Input, Divider, Icon, Pagination } from "semantic-ui-react";
import styled from "styled-components";

import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";
import { pageLimitOptions } from "../../utils/options";
import WithData from "../../utils/withData";

class VlanMembershipStat extends Component {
  state = {
    isAutoRefresh: false,
    tableHeaderSecRow: [],
    currentPosts: [],
    displayUser: "",
    totalPosts: 0,
    currentPage: 1,
    entriesPerPage: 20,
    startFromVlan: 1,
    userCombo: 1,
    sid: -1,
    portCount: 0,
    userComboOptions: [
      { key: 1, text: "Combined", value: 1 },
      { key: 2, text: "Admin", value: 2 }
    ]
  };

  handleAutoRefresh = () => {
    this.setState(
      prevState => ({ isAutoRefresh: !prevState.isAutoRefresh }),
      () => this.props.autoRefresh(this.state.isAutoRefresh)
    );
  };

  onChangeUserCombo = (e, { value }) => {
    this.setState({ userCombo: value });
    this.props.querySearch(`/stat/vlan_membership_stat?user=${value}`);
  };

  onChangeTextInput = (e, { name, value }) => {
    if (value !== this.state.startFromVlan) {
      this.handlePagination({ [name]: value, currentPage: 1 });
    }
  };

  onChangePage = (event, { activePage }) => {
    if (activePage !== this.state.currentPage) {
      this.handlePagination({ currentPage: activePage });
    }
  };

  onChangeEntriesPerPage = (e, { name, value }) => {
    this.handlePagination({ [name]: value, currentPage: 1 });
  };

  // 處理當前頁面該顯示的post 以及 經過fileter後的總post數
  handlePagination = (params = {}) => {
    const newState = { ...this.state, ...params };
    let filetered = newState.tableBodyRows;

    if (newState.startFromVlan > 1) {
      filetered = filetered.slice(parseInt(newState.startFromVlan - 1));
    }

    // Get current post
    const indexOfLastPost = newState.currentPage * newState.entriesPerPage;
    const indexOfFirstPost = indexOfLastPost - newState.entriesPerPage;
    const currentPosts = filetered.slice(indexOfFirstPost, indexOfLastPost);

    this.setState({ ...newState, currentPosts, totalPosts: filetered.length });
  };

  createUserCombo = (all_user_names_and_ids, currently_selected) => {
    let userComboOptions = [];

    all_user_names_and_ids.forEach(username_and_id => {
      // Format: user_name|user_id
      const username = username_and_id.split("|")[0];
      const id = username_and_id.split("|")[1];

      userComboOptions.push({ key: id, text: username, value: parseInt(id) });
    });

    this.setState({
      userComboOptions,
      displayUser: `${currently_selected} User${currently_selected === "Combined" ? "s" : ""}`
    });
  };

  /* Generate Table */
  createTableHeader = portCount => {
    let tableHeaderSecRow = [];

    tableHeaderSecRow.push(
      <Table.HeaderCell key="vlan_id" textAlign="center">
        VLAN ID
      </Table.HeaderCell>
    );
    for (let i = 0; i < portCount; i++) {
      tableHeaderSecRow.push(
        <Table.HeaderCell key={`vlan_${i}`} textAlign="center">
          {i + 1}
        </Table.HeaderCell>
      );
    }

    this.setState({ tableHeaderSecRow });
  };

  createTableBodyRows = (portCount, vlan_infos) => {
    let tableBodyRows = [];

    if (vlan_infos.length < 2) {
      tableBodyRows.push(
        <Table.Row key="no_data">
          <Table.Cell colSpan={portCount + 1} textAlign="center">
            No data exists for the selected user
          </Table.Cell>
        </Table.Row>
      );
    } else {
      vlan_infos.forEach((vlan_info, i) => {
        if (i === 0) return false;

        // Format: [vlan_info_N] = vid|port_val_0|port_val_1|...|port_val_n
        const vlan_port_infos = vlan_info.split("|");

        tableBodyRows.push(
          <Table.Row key={`vlan_info_${i}`}>
            {vlan_port_infos.map((vlan_port_info, i) => {
              if (i === 0) {
                return (
                  <Table.Cell textAlign="center" key={`vlan_port_info_${i}`}>
                    {vlan_port_info}
                  </Table.Cell>
                );
              }

              const status = () => {
                // 0: not member, 1: member, 2: forbidden, 3: conflict
                switch (parseInt(vlan_port_info)) {
                  case 1:
                    return "check";
                  case 2:
                    return "x";
                  case 3:
                    return "exclamation triangle";
                  default:
                    return "";
                }
              };

              return (
                <Table.Cell key={`vlan_port_info_${i}`} textAlign="center">
                  <StatusIcon name={status()} circular />
                </Table.Cell>
              );
            })}
          </Table.Row>
        );
      });
    }

    this.setState({ tableBodyRows }, () => this.handlePagination());
  };

  createMembershipTable = vlan_infos => {
    // eslint-disable-next-line no-undef
    const portCount = configNormalPortMax;

    this.createTableHeader(portCount);
    this.createTableBodyRows(portCount, vlan_infos);

    this.setState({ portCount });
  };

  processUpdate = () => {
    /*
       Format: [all_user_names_and_ids]#[vlan_infos]
       Where
       [all_user_names_and_ids] = [user_name_id_1]/[user_name_id_2]/.../[user_name_id_n]
       [vlan_infos]             = requested_user_name/[vlan_info_1]/[vlan_info_2]/.../[vlan_info_n]
    */

    const values = this.props.data.split("#");
    const all_user_names_and_ids = values[0].split("/");
    const vlan_infos = values[1].split("/");

    this.createUserCombo(all_user_names_and_ids, vlan_infos[0] /* Currently selected */);
    this.createMembershipTable(vlan_infos);
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.prevState) !== JSON.stringify(prevState)
    );
  }

  componentDidUpdate(prevProps) {
    if (JSON.stringify(this.props) !== JSON.stringify(prevProps)) this.processUpdate();
  }

  componentDidMount() {
    this.processUpdate();
  }

  render() {
    const { portCount } = this.state;

    return (
      <Segment.Group>
        <SegmentHeader>
          VLAN Membership Status for {this.state.displayUser}
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Form size="mini">
            <Form.Group>
              <Form.Field
                width="4"
                compact
                control={Select}
                name="userCombo"
                value={this.state.userCombo}
                label="Options"
                options={this.state.userComboOptions}
                onChange={this.onChangeUserCombo}
              />
              <Form.Field
                width="4"
                control={Input}
                name="startFromVlan"
                label="Start from VLAN"
                value={this.state.startFromVlan}
                onChange={this.onChangeTextInput}
              />
              <Form.Field
                width="4"
                compact
                control={Select}
                name="entriesPerPage"
                value={this.state.entriesPerPage}
                options={pageLimitOptions}
                onChange={this.onChangeEntriesPerPage}
                label={{ children: "Entries per page" }}
              />
            </Form.Group>
          </Form>

          <Divider />

          <Table celled compact>
            <Table.Header>
              <Table.Row textAlign="center">
                <Table.HeaderCell />
                <Table.HeaderCell colSpan={portCount}>Port Members</Table.HeaderCell>
              </Table.Row>
              <Table.Row>{this.state.tableHeaderSecRow}</Table.Row>
            </Table.Header>
            <Table.Body>{this.state.currentPosts}</Table.Body>
            <Table.Footer>
              <Table.Row textAlign="right">
                <Table.HeaderCell colSpan={portCount + 1}>
                  <Pagination
                    activePage={this.state.currentPage}
                    totalPages={Math.ceil(this.state.totalPosts / this.state.entriesPerPage)}
                    size="mini"
                    onPageChange={this.onChangePage}
                    firstItem={{ content: <Icon name="angle double left" />, icon: true }}
                    lastItem={{ content: <Icon name="angle double right" />, icon: true }}
                    prevItem={{ content: <Icon name="angle left" />, icon: true }}
                    nextItem={{ content: <Icon name="angle right" />, icon: true }}
                  />
                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </Table>
        </Segment>
      </Segment.Group>
    );
  }
}

const StatusIcon = styled(Icon)`
  margin-right: 0 !important;
  &.check {
    color: #21ba45 !important;
  }
  &.x,
  &.exclamation.triangle {
    color: #db2828 !important;
  }
`;

export default WithData(VlanMembershipStat, "/stat/vlan_membership_stat?user=1", false);
