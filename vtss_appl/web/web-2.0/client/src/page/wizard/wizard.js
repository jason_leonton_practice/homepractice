import React from "react";
import { Segment, Step } from "semantic-ui-react";
import "./wizard.scss";

import FirstStep from "./first-step";
import SecondStep from "./second-step";
import Confirm from "./confirm";

class Success extends React.Component {
  render() {
    return (
      <React.Fragment>
        <h1>Success</h1>
      </React.Fragment>
    );
  }
}

class Wizard extends React.Component {
  state = {
    step: 1,
    firstName: "Jay",
    lastName: "Pan",
    city: "Taipei",
    occupation: "Web Developer"
  };

  nextStep = () => {
    const { step } = this.state;
    this.setState({ step: step + 1 });
  };

  prevStep = () => {
    const { step } = this.state;
    this.setState({ step: step - 1 });
  };

  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
  };

  render() {
    const { step } = this.state;
    const { firstName, lastName, city, occupation } = this.state;
    const values = { firstName, lastName, city, occupation };

    const currentStep = () => {
      switch (step) {
        case 1:
          return (
            <FirstStep
              nextStep={this.nextStep}
              handleChange={this.handleChange}
              values={values}
            />
          );
        case 2:
          return (
            <SecondStep
              nextStep={this.nextStep}
              prevStep={this.prevStep}
              handleChange={this.handleChange}
              values={values}
            />
          );
        case 3:
          return (
            <Confirm
              nextStep={this.nextStep}
              prevStep={this.prevStep}
              values={values}
            />
          );
        case 4:
          return <Success />;
        default:
          return true;
      }
    };

    return (
      <React.Fragment>
        <Step.Group attached="top" stackable="tablet" ordered>
          <Step completed={step > 1} active={step === 1}>
            <Step.Content>
              <Step.Title>Normal Info</Step.Title>
              <Step.Description>Choose your shipping options</Step.Description>
            </Step.Content>
          </Step>
          <Step completed={step > 2} active={step === 2} disabled={step < 2}>
            <Step.Content>
              <Step.Title>Personal Info</Step.Title>
              <Step.Description>Enter billing information</Step.Description>
            </Step.Content>
          </Step>
          <Step completed={step > 3} active={step === 3} disabled={step < 3}>
            <Step.Content>
              <Step.Title>Confirm Info</Step.Title>
            </Step.Content>
          </Step>
        </Step.Group>

        <Segment>{currentStep()}</Segment>
      </React.Fragment>
    );
  }
}

export default Wizard;
