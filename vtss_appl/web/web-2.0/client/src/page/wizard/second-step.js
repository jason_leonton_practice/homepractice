import React from "react";
import { Button, Form } from "semantic-ui-react";

class SecondStep extends React.Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const { handleChange, values } = this.props;
    return (
      <React.Fragment>
        <h1>Second Step</h1>
        <Form>
          <Form.Field onChange={handleChange("city")}>
            <label>City</label>
            <input placeholder={values.city} />
          </Form.Field>
          <Form.Field onChange={handleChange("occupation")}>
            <label>Occupation</label>
            <input placeholder={values.occupation} />
          </Form.Field>
        </Form>
        <br />
        <div id="wizard-btn-container">
          <Button onClick={this.back}>Previous</Button>
          <Button secondary onClick={this.continue}>
            Contintue
          </Button>
        </div>
      </React.Fragment>
    );
  }
}

export default SecondStep;
