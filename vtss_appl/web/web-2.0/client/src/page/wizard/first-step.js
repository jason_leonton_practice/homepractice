import React from "react";
import { Button, Form } from "semantic-ui-react";
import "./wizard.scss";

class FirstStep extends React.Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  render() {
    const { values, handleChange } = this.props;
    return (
      <React.Fragment>
        <h1>Personal Info</h1>
        <Form>
          <Form.Field onChange={handleChange("firstName")}>
            <label>First Name</label>
            <input placeholder="First Name" />
          </Form.Field>
          <Form.Field onChange={handleChange("lastName")}>
            <label>Last Name</label>
            <input placeholder={values.lastName} />
          </Form.Field>
        </Form>
        <br />
        <div style={{ height: "36px" }}>
          <Button secondary onClick={this.continue} style={{ float: "right" }}>
            Contintue
          </Button>
        </div>
      </React.Fragment>
    );
  }
}

export default FirstStep;
