import React from "react";
import { Button, List } from "semantic-ui-react";
import "./wizard.scss";

class Confirm extends React.Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const {
      values: { firstName, lastName, city, occupation }
    } = this.props;
    return (
      <React.Fragment>
        <h1>Confirm Information</h1>
        <List>
          <List.Item>
            <List.Header>First Name</List.Header>
            {firstName}
          </List.Item>
          <List.Item>
            <List.Header>Last Name</List.Header>
            {lastName}
          </List.Item>
          <List.Item>
            <List.Header>City</List.Header>
            {city}
          </List.Item>
          <List.Item>
            <List.Header>Occupation</List.Header>
            {occupation}
          </List.Item>
        </List>
        <div id="wizard-btn-container">
          <Button onClick={this.back}>Previous</Button>
          <Button secondary onClick={this.continue}>
            Confirm
          </Button>
        </div>
      </React.Fragment>
    );
  }
}

export default Confirm;
