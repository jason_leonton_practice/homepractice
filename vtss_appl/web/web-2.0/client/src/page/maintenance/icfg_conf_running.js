import React, { Component } from "react";
import { Segment, Message, Icon, Form } from "semantic-ui-react";
import TextareaAutosize from "react-textarea-autosize";

import { SegmentHeader } from "../../components/styled_component";
import { HeaderRefreshBar } from "../../components/semantic-component";

import WithData from "../../utils/withData";

class IcfgConfRunning extends Component {
  state = { status: "", statusMsg: "" };

  handleAutoRefresh = () => {
    this.setState(
      prevState => ({ isAutoRefresh: !prevState.isAutoRefresh }),
      () => this.props.autoRefresh(this.state.isAutoRefresh)
    );
  };

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps) !== JSON.stringify(this.props)) {
      this.processUpdate();
    }
  }

  processUpdate = () => {
    let lines = this.props.data.split("\n");
    const status = lines[0];
    const trunc = lines.length > 200;

    lines = lines.slice(1, 200);

    if (trunc) {
      lines.push("! (Large amount of output; truncated.)\n");
    }

    switch (status) {
      case "DONE":
        this.setState({ statusMsg: "Activation completed successfully." });
        this.props.autoRefresh(false);
        break;
      case "ERR":
        this.setState({ statusMsg: "Warning: Activation completed with errors." });
        this.props.autoRefresh(false);
        break;
      case "SYNERR":
        this.setState({
          statusMsg: "Warning: Syntax check completed with errors; configuration has not been activated."
        });
        this.props.autoRefresh(false);
        break;
      case "IDLE":
        this.setState({ statusMsg: "No activation in progress yet." });
        break;
      case "RUN":
        this.setState({ statusMsg: "In progress, please stand by." });
        break;
      default:
        break;
    }

    this.setState({ status, content: lines.join("\n") });
  };

  componentDidMount() {
    this.processUpdate();
  }

  render() {
    const { status, statusMsg, content } = this.state;
    const transformStatus = () => {
      switch (status) {
        case "DONE":
          return [{ success: true }, "check circle"];
        case "ERR":
          return [{ error: true }, "times circle"];
        case "SYNERR":
          return [{ error: true }, "times circle"];
        case "IDLE":
          return [{ info: true }, "toggle off"];
        case "RUN":
          return [{ info: true }, "spinner loading"];
        default:
          return [{ info: true }, "thumbs up"];
      }
    };

    return (
      <Segment.Group>
        <SegmentHeader>
          Activating New Configuration
          <HeaderRefreshBar
            className="segment"
            checked={this.state.isAutoRefresh}
            handleAutoRefresh={this.handleAutoRefresh}
          />
        </SegmentHeader>
        <Segment>
          <Message warning>
            <Icon name="exclamation circle" />
            Please note: If the configuration changes IP settings, management connectivity may be lost.
          </Message>
          <Message icon {...transformStatus()[0]}>
            <Icon name={transformStatus()[1]} />
            <Message.Content>
              <Message.Header>{status}</Message.Header>
              {statusMsg}
            </Message.Content>
          </Message>
          <Form>
            <Form.Field
              control={TextareaAutosize}
              label="Output"
              value={content}
              style={{ maxHeight: "none" }}
            />
          </Form>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(IcfgConfRunning, "/config/icfg_conf_activate", true);
