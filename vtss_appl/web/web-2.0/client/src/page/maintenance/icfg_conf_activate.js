import React, { Component } from "react";
import { Segment, Message, Icon, Table, Radio, Divider } from "semantic-ui-react";
import { toast } from "react-toastify";

import { SegmentHeader } from "../../components/styled_component";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { InfoMsg, msgConfig } from "../../components/messages";
import WithData from "../../utils/withData";
import { decodeURIComponentSafe } from "../../utils/self-documenting_func";

class IcfgConfActivate extends Component {
  state = { files: [], fileRadio: "", message: "", error: false };

  handleRadioChange = (e, { name, value }) => this.setState({ [name]: value });

  isValid = () => {
    if (this.state.fileRadio) {
      this.setState({ message: "" });
      return true;
    }

    if (!this.state.fileRadio) {
      this.setState({ message: "Please select a file name." });
      return false;
    }

    return false;
  };

  handleSubmit = async () => {
    if (!this.isValid()) return false;

    this.setState({ loading: true });

    const res = await fetch("/config/icfg_conf_activate", {
      method: "POST",
      body: `file_name=${this.state.fileRadio}`,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    });
    const [status, statusText, body] = await Promise.all([res.status, res.statusText, res.text()]);

    if (status === 200) {
      return this.props.history.push("/maintenance/configuration/running");
    } else {
      toast.error(InfoMsg("error", body, status, statusText), msgConfig());
    }

    this.setState({ loading: false });
  };

  processUpdate = () => {
    const fields = this.props.data.split("*");
    const status = fields[0];
    const files = fields.slice(1);

    switch (status) {
      case "OK":
        if (files.length < 2) {
          this.setState({ message: "No files available for deletion.", error: true });
        } else {
          this.setState({ files });
        }
        break;
      case "ERR_LOCK":
        this.setState({
          message: "Another I/O operation is in progress. Please try again later.",
          error: true
        });
        break;
      case "ERR":
        this.setState({
          message: "Could not retrieve file list from switch. Please try again later.",
          error: true
        });
        break;
      default:
        return false;
    }
  };

  componentDidUpdate(prevProps) {
    if (JSON.stringify(this.props) !== JSON.stringify(prevProps)) {
      this.processUpdate();
    }
  }

  componentDidMount() {
    this.processUpdate();
  }

  render() {
    const { files, fileRadio, message, error, loading } = this.state;

    return (
      <Segment.Group>
        <SegmentHeader>Activate Configuration</SegmentHeader>
        <Segment loading={loading}>
          <Message warning icon>
            <Icon name="exclamation circle" />
            <Message.Content>
              <Message.List>
                <Message.Item>
                  Select configuration file to activate. The previous configuration will be completely
                  replaced, potentially leading to loss of management connectivity.
                </Message.Item>
                <Message.Item>
                  Please note: The activated configuration file will not be saved to startup-config
                  automatically.
                </Message.Item>
              </Message.List>
            </Message.Content>
          </Message>

          {!error && (
            <>
              <Table key="table" collapsing striped style={{ minWidth: "300px" }}>
                <Table.Header>
                  <Table.Row textAlign="center">
                    <Table.HeaderCell colSpan={files.length - 1}>File Name</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  {files.map((filename, i) => {
                    if (filename.length === 0) return false;
                    filename = decodeURIComponentSafe(filename);
                    return (
                      <Table.Row key={`file_cell_${i}`}>
                        <Table.Cell>
                          <Radio
                            checked={fileRadio === filename}
                            label={filename}
                            value={filename}
                            name="fileRadio"
                            onChange={this.handleRadioChange}
                          />
                        </Table.Cell>
                      </Table.Row>
                    );
                  })}
                </Table.Body>
              </Table>
            </>
          )}

          {message.length > 0 && (
            <Message error size="small">
              <Icon name="exclamation circle" size="small" />
              {message}
            </Message>
          )}

          <Divider />
          <BtnWrapper>
            <SemanticButton
              title="Activate Configuration"
              disabled={this.props.readOnly}
              onClick={this.handleSubmit}
            />
          </BtnWrapper>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(IcfgConfActivate, "/config/icfg_conf_get_file_list?op=activate", false);
