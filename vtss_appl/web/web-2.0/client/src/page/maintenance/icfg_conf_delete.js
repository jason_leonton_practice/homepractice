import React, { Component } from "react";
import { Segment, Message, Divider, Table, Radio, Icon } from "semantic-ui-react";
import { toast } from "react-toastify";

import { SegmentHeader } from "../../components/styled_component";
import { BtnWrapper, SemanticButton } from "../../components/semantic-component";
import { InfoMsg, msgConfig } from "../../components/messages";
import WithData from "../../utils/withData";
import { decodeURIComponentSafe } from "../../utils/self-documenting_func";

class IcfgConfDelete extends Component {
  state = { files: [], fileRadio: "", message: "", error: false };

  handleRadioChange = (e, { name, value }) => this.setState({ [name]: value });

  isValid = () => {
    if (this.state.fileRadio) {
      this.setState({ message: "" });
      return true;
    }

    if (!this.state.fileRadio) {
      this.setState({ message: "Please select a file name." });
      return false;
    }

    return false;
  };

  handleSubmit = () => {
    if (!this.isValid()) return false;

    fetch("/config/icfg_conf_delete", {
      method: "POST",
      body: `file_name=${this.state.fileRadio}`,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    })
      .then(res => {
        if (res.status === 200) {
          toast.success(InfoMsg("success", null), {
            ...msgConfig(),
            onOpen: () => this.props.refresh(true)
          });
        } else {
          toast.error(InfoMsg("error", null, res.status, res.statusText), msgConfig());
        }
      })
      .catch(error => console.log(error));
  };

  processUpdate = () => {
    const fields = this.props.data.split("*");
    const status = fields[0];
    const files = fields.slice(1);

    switch (status) {
      case "OK":
        if (files.length < 2) {
          this.setState({ message: "No files available for deletion.", error: true });
        } else {
          this.setState({ files });
        }
        break;
      case "ERR_LOCK":
        this.setState({
          message: "Another I/O operation is in progress. Please try again later.",
          error: true
        });
        break;
      case "ERR":
        this.setState({
          message: "Could not retrieve file list from switch. Please try again later.",
          error: true
        });
        break;
      default:
        return false;
    }
  };

  componentDidUpdate(prevProps) {
    if (JSON.stringify(this.props) !== JSON.stringify(prevProps)) {
      this.processUpdate();
    }
  }

  componentDidMount() {
    this.processUpdate();
  }

  render() {
    const { files, message, error, fileRadio } = this.state;

    return (
      <Segment.Group>
        <SegmentHeader>Delete Configuration File</SegmentHeader>
        <Segment>
          <Message size="small" warning>
            Select configuration file to delete.
          </Message>
          {!error && (
            <>
              <Table key="table" collapsing striped style={{ minWidth: "300px" }}>
                <Table.Header>
                  <Table.Row textAlign="center">
                    <Table.HeaderCell colSpan={files.length - 1}>File Name</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  {files.map((filename, i) => {
                    if (filename.length === 0) return false;
                    filename = decodeURIComponentSafe(filename);
                    return (
                      <Table.Row key={`file_cell_${i}`}>
                        <Table.Cell>
                          <Radio
                            checked={fileRadio === filename}
                            label={filename}
                            value={filename}
                            name="fileRadio"
                            onChange={this.handleRadioChange}
                          />
                        </Table.Cell>
                      </Table.Row>
                    );
                  })}
                </Table.Body>
              </Table>
            </>
          )}

          {message.length > 0 && (
            <Message error size="small">
              <Icon name="exclamation circle" size="small" />
              {message}
            </Message>
          )}

          <Divider />

          <BtnWrapper>
            <SemanticButton
              title="Delete Configuration File"
              disabled={this.props.readOnly || error}
              onClick={this.handleSubmit}
            />
          </BtnWrapper>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(IcfgConfDelete, "/config/icfg_conf_get_file_list?op=delete", false);
