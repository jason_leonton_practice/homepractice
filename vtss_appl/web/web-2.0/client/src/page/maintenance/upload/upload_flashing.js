import React from "react";
import { Message, Icon } from "semantic-ui-react";

class UploadFlashing extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      updateStatus: "waiting",
      updateInfo: ""
    };
    this.timerID = 0;
    this.ajax = {};
  }

  doRetry = responseText => {
    this.startUpdate(responseText, 2000);
  };

  done = (status, updateInfo) => {
    if (status) {
      this.setState({ updateStatus: "success" });
    } else {
      this.setState({ updateStatus: "failed" });
    }

    this.setState({ updateInfo });
  };

  pollTimeout = () => {
    this.ajax.abort();
    this.doRetry("Polling timeout, retry...");
  };

  doPoll = () => {
    this.setState({ updateStatus: "polling", updateInfo: "Polling..." });

    this.ajax = new XMLHttpRequest();
    let ajax = this.ajax;

    ajax.open("GET", "/config/firmware_status", true);
    ajax.onreadystatechange = () => {
      clearTimeout(this.timerID);
      try {
        if (ajax.readyState === 4) {
          if (ajax.status && ajax.status === 200) {
            let match_reg = "\\[USB]Err:";

            if (ajax.responseText === "idle") {
              this.done(true, "Firmware Update Completed!");
            } else if (ajax.responseText.match("Firmware Upload Error")) {
              this.done(false, ajax.responseText);
            } else if (ajax.responseText.match(match_reg)) {
              this.done(false, ajax.responseText.substr(match_reg.length - 1));
            } else if (ajax.response.match(/^Error/)) {
              this.done(false, ajax.responseText);
            } else {
              this.doRetry(ajax.responseText);
            }
          }
        } else {
          let status;
          try {
            status = ajax.statusText;
          } catch (e) {
            status = "Unknown Error";
          }
          this.doRetry(status);
        }
      } catch (e) {
        this.doRetry("Request timeout");
      }
    };

    ajax.setRequestHeader("Lntn-Web-Ver", "2.0");
    ajax.setRequestHeader("If-Modified-Since", "0"); // None cache
    ajax.send(null);

    this.timerID = setTimeout(this.pollTimeout, 2000);
  };

  startUpdate = (msg, when) => {
    this.setState({ updateInfo: msg });
    this.timerID = setTimeout(this.doPoll, when);
  };

  componentDidMount() {
    this.startUpdate("Waiting for update, please stand by...", 5000);
  }

  render() {
    const { updateStatus, updateInfo } = this.state;
    const updateMsgIconName = () => {
      switch (updateStatus) {
        case "waiting":
        case "polling":
          return "circle notched";
        case "success":
          return "check circle";
        case "failed":
          return "times circle";
        default:
          return "circle notched";
      }
    };

    return (
      <React.Fragment>
        <Message icon warning>
          <Icon name="info circle" />
          <Message.Content>
            <Message.Header>Firmware Update in Progress</Message.Header>
            <Message.List>
              <Message.Item>The uploaded firmware image is being transferred to flash.</Message.Item>
              <Message.Item>The system will restart after the update.</Message.Item>
              <Message.Item>Until then, do not reset or power off the device!</Message.Item>
            </Message.List>
          </Message.Content>
        </Message>
        <Message icon success={updateStatus === "success"} error={updateStatus === "failed"}>
          <Icon
            name={updateMsgIconName()}
            loading={updateStatus === "waiting" || updateStatus === "polling"}
          />
          <Message.Content>
            <Message.Header>{updateInfo}</Message.Header>
          </Message.Content>
        </Message>
      </React.Fragment>
    );
  }
}

export default UploadFlashing;
