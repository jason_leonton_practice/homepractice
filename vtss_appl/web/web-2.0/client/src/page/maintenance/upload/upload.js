import React from "react";
import { Form, Segment } from "semantic-ui-react";

import DragAndDrop from "../../../components/drag_and_drop";
import { SegmentHeader } from "../../../components/styled_component";
import { BtnWrapper, SemanticButton } from "../../../components/semantic-component";
import UploadFlashing from "./upload_flashing";
import withData from "../../../utils/withData";

class Upload extends React.Component {
  state = {
    file: null,
    src_type: "Local",
    usbInputValue: "",
    validation: { error: false, msg: "" },
    uploadStatus: "none", // none -> uploading -> waitServerResponse -> success || error
    progress: 0
  };

  validate = (src_type, value) => {
    switch (src_type) {
      case "Local":
        if (!value) {
          return { error: true, msg: "File not exist. Please upload a file." };
        }

        if (!value.name.endsWith(".rom")) {
          return { error: true, msg: "Invalid file type. Please upload .rom file type." };
        }

        return { error: false, msg: "" };

      case "USB":
        if (!value.length) {
          return { error: true, msg: "Please specify a file name." };
        }

        if (!value.endsWith(".rom")) {
          return { error: true, msg: "The upload file must be a file of type: .rom" };
        }

        return { error: false, msg: "" };

      default:
        return { error: true, msg: "Invalid source type." };
    }
  };

  onSubmit = () => {
    let validation = { error: false, msg: "" };

    // check input field again before submission
    switch (this.state.src_type) {
      case "Local":
        validation = this.validate("Local", this.state.file);
        break;
      case "USB":
        validation = this.validate("USB", this.state.usbInputValue);
        break;
      default:
        break;
    }

    if (validation.error) {
      this.setState({ validation });
      return false;
    }

    this.uploadFile();
  };

  uploadFile = () => {
    this.setState({ uploadStatus: "uploading" });

    let ajax = new XMLHttpRequest();
    let formData = new FormData();
    formData.append("file_src", this.state.src_type);

    switch (this.state.src_type) {
      case "Local":
        formData.append("firmware", this.state.file);
        break;
      case "USB":
        formData.append("usb_filename", this.state.usbInputValue);
        break;
      default:
        return false;
    }

    ajax.upload.addEventListener("progress", this.progressHandler);
    ajax.upload.addEventListener("error", this.httpReqErrorHandler);
    ajax.upload.addEventListener("load", e => this.httpReqLoadHandler);
    ajax.open("post", "/config/firmware");
    ajax.setRequestHeader("Lntn-Web-Ver", "2.0");
    ajax.send(formData);
    ajax.onreadystatechange = () => {
      if (ajax.readyState === 4) {
        if (ajax.status && ajax.status === 200) {
          this.setState({ uploadStatus: "success" });
        } else {
          this.setState({
            uploadStatus: "error",
            validation: { error: true, msg: `Status code: ${ajax.status}. ${ajax.responseText}` }
          });
        }
      } else {
      }
    };
  };

  httpReqLoadHandler = e => {
    // server should return response to trigger listener
    console.log(e.target);
  };

  httpReqErrorHandler = e => {
    this.setState({ validation: { error: true, msg: `Something went wrong` } });
  };

  progressHandler = e => {
    const precentage = Math.floor((e.loaded / e.total) * 100);
    this.setState({ progress: precentage });
    if (precentage === 100) this.setState({ uploadStatus: "waitServerResponse" });
  };

  onChangeRadio = (e, { value }) => {
    this.setState({ src_type: value, validation: { error: false, msg: "" } });
  };

  onChangeLocalFile = e => {
    const file = e.target.files[0];
    const validation = this.validate("Local", file);

    this.setState({ file, validation });
  };

  onChangeUSBInput = e => {
    const value = e.target.value;
    const validation = this.validate("USB", value);

    this.setState({ usbInputValue: value, validation });
  };

  handleDropFile = files => {
    const file = files[0];
    const validation = this.validate("Local", files[0]);

    this.setState({ file, validation });
  };

  render() {
    const { uploadStatus, src_type, usbInputValue, validation, progress } = this.state;
    const { readOnly } = this.props;

    return (
      <Segment.Group>
        <SegmentHeader>Software Upload</SegmentHeader>
        {/* uploadStatus === "waitServerResponse" will load the dimmer since the server needs extra time to send back success or error status.
            As a result, the user won't feel confused while the progress bar is 100% but has no further action */}
        <Segment loading={uploadStatus === "waitServerResponse"}>
          {uploadStatus !== "success" ? (
            <Form>
              <Form.Field>
                <label>Choose Upload File Type:</label>
                <Form.Group inline>
                  <Form.Radio
                    radio
                    label="Local"
                    checked={src_type === "Local"}
                    onChange={this.onChangeRadio}
                    value="Local"
                  />
                  <Form.Radio
                    radio
                    label="USB"
                    checked={src_type === "USB"}
                    onChange={this.onChangeRadio}
                    value="USB"
                  />
                </Form.Group>
              </Form.Field>
              <Form.Field error={validation.error}>
                {src_type === "Local" ? (
                  <DragAndDrop
                    handleDrop={this.handleDropFile}
                    validation={validation}
                    uploadStatus={uploadStatus}
                    progress={progress}
                  />
                ) : (
                  <React.Fragment>
                    <label>Type your USB file name:</label>
                    <input placeholder="File name" value={usbInputValue} onChange={this.onChangeUSBInput} />
                    {validation.error && <span style={{ color: "#9f3a38" }}>{validation.msg}</span>}
                  </React.Fragment>
                )}
              </Form.Field>

              <BtnWrapper>
                <SemanticButton
                  title="upload"
                  disabled={readOnly || uploadStatus === "uploading"}
                  onClick={this.onSubmit}
                />
              </BtnWrapper>
            </Form>
          ) : (
            <UploadFlashing />
          )}
        </Segment>
      </Segment.Group>
    );
  }
}

export default withData(Upload, "/config/firmware", false);
