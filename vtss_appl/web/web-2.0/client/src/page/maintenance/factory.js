import React, { Component } from "react";
import { Segment, Message, Icon } from "semantic-ui-react";
import { toast } from "react-toastify";

import { SegmentHeader } from "../../components/styled_component";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { InfoMsg, msgConfig } from "../../components/messages";
import WithData from "../../utils/withData";

class Factory extends Component {
  state = { loading: false };

  handleSubmit = async () => {
    this.setState({ loading: true });

    const res = await fetch("/config/misc", {
      method: "post",
      body: "factory=yes",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    });
    const [status, statusText, body] = await Promise.all([res.status, res.statusText, res.text()]);

    if (status === 200) {
      toast.success(InfoMsg("success", body), {
        ...msgConfig(),
        onOpen: this.props.refresh(true)
      });
    } else {
      toast.error(InfoMsg("error", body, status, statusText), msgConfig());
    }

    this.setState({ loading: false });
  };

  render() {
    return (
      <Segment.Group>
        <SegmentHeader>Facotry Defaults</SegmentHeader>
        <Segment loading={this.state.loading}>
          <Message negative icon>
            <Icon name="exclamation circle" />
            <Message.Content>
              <Message.Header>
                Are you sure you want to reset the configuration to Factory Defaults?
              </Message.Header>
            </Message.Content>
          </Message>
          <BtnWrapper>
            <SemanticButton
              title="No"
              disabled={this.props.readOnly}
              onClick={() => this.props.history.push("/")}
            />
            <SemanticButton title="Yes" disabled={this.props.readOnly} onClick={this.handleSubmit} />
          </BtnWrapper>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(Factory, "/config/misc", false);
