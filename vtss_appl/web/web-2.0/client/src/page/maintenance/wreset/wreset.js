import React from "react";
import { Segment, Message, Icon } from "semantic-ui-react";
import { toast } from "react-toastify";

import { SemanticButton, BtnWrapper } from "../../../components/semantic-component";
import WresetBooting from "./wreset_booting";
import withData from "../../../utils/withData";
import { SegmentHeader } from "../../../components/styled_component";
import { InfoMsg, msgConfig } from "../../../components/messages";

class Wreset extends React.Component {
  state = { success: false, loading: false };

  handleNo = () => this.props.history.push("/");

  handleReset = () => {
    this.setState({ loading: true });

    fetch("/config/misc", {
      method: "POST",
      body: `warm=Yes`,
      headers: { "Content-Type": "application/x-www-form-urlencoded" }
    })
      .then(res => {
        try {
          if (res.status && res.status === 200) {
            this.setState({ success: true });
          } else {
            throw new Error(`Status code: ${res.status}. ${res.statusText}`);
          }
        } catch (err) {
          toast.error(InfoMsg("error", null), msgConfig());
        }

        this.setState({ loading: false });
      })
      .catch(err => toast.error(InfoMsg("error", err.message), msgConfig()));
  };

  render() {
    const { readOnly } = this.props;

    return (
      <Segment.Group>
        <SegmentHeader>Restart Device</SegmentHeader>
        <Segment loading={this.state.loading}>
          {!this.state.success ? (
            <React.Fragment>
              <Message error>
                <Icon name="warning sign" />
                Are you sure you want to perform a restart?
              </Message>
              <BtnWrapper>
                <SemanticButton title="No" disabled={readOnly} onClick={this.handleNo} />
                <SemanticButton title="Yes" disabled={readOnly} onClick={this.handleReset} />
              </BtnWrapper>
            </React.Fragment>
          ) : (
            <WresetBooting />
          )}
        </Segment>
      </Segment.Group>
    );
  }
}

export default withData(Wreset, "/config/wreset", false);
