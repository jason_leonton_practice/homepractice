import React from "react";
import { Message, Icon } from "semantic-ui-react";

export default class WresetBooting extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      updateStatus: "waiting",
      updateInfo: ""
    };
    this.timerID = 0;
    this.req = {};
  }

  doRetry = responseText => {
    this.startUpdate(responseText, 2000);
  };

  pollTimeout = () => {
    this.req.abort();
    this.doRetry("Polling timeout, retry...");
  };

  done = (status, updateInfo) => {
    if (status) {
      this.setState({ updateStatus: "success" });
    } else {
      this.setState({ updateStatus: "failed" });
    }

    this.setState({ updateInfo });
  };

  doPoll = () => {
    this.setState({ updateStatus: "polling", updateInfo: "Polling..." });

    this.req = new XMLHttpRequest();
    let req = this.req;

    req.open("GET", "/config/wreset_status", true);
    req.onreadystatechange = () => {
      clearTimeout(this.timerID);
      try {
        if (req.readyState === 4) {
          if (req.status && req.status === 200) {
            if (req.responseText === "idle") {
              this.done(true, "Fimrware has been restarted!");
            } else if (req.responseText.match(/fail/)) {
              this.done(req.responseText);
            } else {
              this.doRetry(req.responseText);
            }
          }
        } else {
          let status;
          try {
            status = req.statusText;
          } catch (e) {
            status = "Unknown Error";
          }
          this.doRetry(status);
        }
      } catch (e) {
        this.doRetry("Request timeout");
      }
    };

    req.setRequestHeader("If-Modified-Since", "0"); // None cache
    req.send(null);

    this.timerID = setTimeout(this.pollTimeout, 2000);
  };

  startUpdate = (msg, when) => {
    this.setState({ updateInfo: msg });
    this.timerID = setTimeout(this.doPoll, when);
  };

  componentWillUnmount() {
    clearTimeout(this.timerID);
  }

  componentDidMount() {
    this.startUpdate("Waiting for update, please stand by...", 5000);
  }

  render() {
    const { updateInfo, updateStatus } = this.state;
    const updateMsgIconName = () => {
      switch (updateStatus) {
        case "waiting":
        case "polling":
          return "circle notched";
        case "success":
          return "check circle";
        case "failed":
          return "times circle";
        default:
          return "circle notched";
      }
    };
    return (
      <React.Fragment>
        <Message warning>
          <Icon name="info circle" />
          System Restart in Progress. The system is now restarting.
        </Message>

        <Message icon success={updateStatus === "success"} error={updateStatus === "failed"}>
          <Icon
            name={updateMsgIconName()}
            loading={updateStatus === "waiting" || updateStatus === "polling"}
          />
          <Message.Content>{updateInfo}</Message.Content>
        </Message>
      </React.Fragment>
    );
  }
}
