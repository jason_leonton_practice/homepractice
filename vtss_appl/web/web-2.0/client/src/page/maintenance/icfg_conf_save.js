import React, { Component } from "react";
import { Segment, Message, Icon } from "semantic-ui-react";
import { toast } from "react-toastify";

import { SegmentHeader } from "../../components/styled_component";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { InfoMsg, msgConfig } from "../../components/messages";
import WithData from "../../utils/withData";

class IcfgConfSave extends Component {
  state = { loading: false };

  handleSubmit = async () => {
    this.setState({ loading: true });

    const res = await fetch("/config/icfg_conf_save", {
      method: "post",
      body: "save=0",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    });
    const [status, statusText, body] = await Promise.all([res.status, res.statusText, res.text()]);

    if (status === 200) {
      toast.success(InfoMsg("success", body), msgConfig());
    } else {
      toast.error(InfoMsg("error", body, status, statusText), msgConfig());
    }

    this.setState({ loading: false });
  };

  render() {
    return (
      <Segment.Group>
        <SegmentHeader>Save Running Configuration to startup-config</SegmentHeader>
        <Segment loading={this.state.loading}>
          <Message warning icon>
            <Icon name="exclamation circle" />
            <Message.Content>
              <Message.Header>Please note</Message.Header>
              The generation of the configuration file may be time consuming, depending on the amount of
              non-default configuration.
            </Message.Content>
          </Message>
          <BtnWrapper>
            <SemanticButton
              title="Save Configuration"
              disabled={this.props.readOnly}
              onClick={this.handleSubmit}
            />
          </BtnWrapper>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(IcfgConfSave, "/config/icfg_conf_save", false);
