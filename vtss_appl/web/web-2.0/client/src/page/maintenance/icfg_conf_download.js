import React from "react";
import { Segment, Table, Radio, Message, Divider, Icon } from "semantic-ui-react";
import { toast } from "react-toastify";
import styled from "styled-components";

import { SegmentHeader } from "../../components/styled_component";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { InfoMsg, msgConfig } from "../../components/messages";
import WithData from "../../utils/withData";
import { decodeURIComponentSafe } from "../../utils/self-documenting_func";

class IcfgConfDownload extends React.Component {
  state = { value: "", files: [], fileRadio: "", dest: [], destRadio: "", message: "", error: false };

  handleRadioChange = (e, { name, value }) => this.setState({ [name]: value });

  isValid = () => {
    // check user has selected a filename and file destination
    if (this.state.fileRadio && this.state.destRadio) {
      this.setState({ message: "" });
      return true;
    }

    if (!this.state.fileRadio) {
      this.setState({ message: "Please select a file name." });
      return false;
    }

    if (!this.state.destRadio) {
      this.setState({ message: "Please select a destination file." });
      return false;
    }

    return false;
  };

  handleSubmit = () => {
    const { destRadio, fileRadio } = this.state;

    if (!this.isValid()) return false;

    this.setState({ loading: true });

    fetch("/config/icfg_conf_download", {
      method: "POST",
      body: `file_name=${fileRadio}&file_dest=${destRadio}`,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    })
      .then(res => {
        if (res.status === 200) {
          switch (destRadio) {
            case "Local":
              return Promise.all([res.status, res.blob()]);
            default:
              return Promise.all([res.status, res.text()]);
          }
        }
        return Promise.all([res.status, res.text()]);
      })
      .then(([status, body]) => {
        this.setState({ loading: false });

        if (status === 200) {
          if (destRadio === "Local") {
            /* Create blob link to download
               provide type: "application/x-download"
               so download attr will not automatically detect file extensions */

            const url = URL.createObjectURL(new Blob([body], { type: "application/x-download" }));
            const link = document.createElement("a");
            link.href = url;
            link.setAttribute("style", "display: none");

            /* Download attribute is compatible in 93.55% browser 2019/12/19
               https://caniuse.com/#search=attribute%20download */

            if (typeof link.download !== "undefined") {
              link.setAttribute("download", fileRadio || "download");
            } else {
              window.navigator.msSaveOrOpenBlob(url, fileRadio);
            }

            document.body.appendChild(link);
            link.click(); // Force download
            link.parentNode.removeChild(link); // Clean up and remove the link

            toast.success(InfoMsg("success", "Download successfully completed."), msgConfig());
          } else if (destRadio === "USB") {
            toast.success(InfoMsg("success", body), msgConfig());
          }
        } else {
          throw Error(`Status Code: ${status}, ${body}`);
        }
      })
      .catch(error => {
        toast.error(InfoMsg("error", error.message), msgConfig());
        this.setState({ loading: false });
      });
  };

  componentDidMount() {
    const { data } = this.props;
    const fields = data.split("*");
    const status = fields[0];
    const dest = fields.slice(1, 3);
    const files = fields.slice(3);

    switch (status) {
      case "OK":
        if (dest.length < 2) {
          this.setState({ message: "NO destinations available for download.", error: true });
        } else {
          this.setState({ dest });
        }

        if (files.length < 2) {
          this.setState({ message: "No files available for downlaod.", error: true });
        } else {
          this.setState({ files });
        }
        break;
      case "ERR_LOCK":
        this.setState({
          message: "Another I/O operation is in progress. Please try again later.",
          error: true
        });
        break;
      case "ERR":
        this.setState({
          message: "Could not retrieve file list from switch. Please try again later.",
          error: true
        });
        break;
      default:
        return false;
    }
  }

  render() {
    const { files, fileRadio, dest, destRadio, message, error, loading } = this.state;
    return (
      <Segment.Group>
        <SegmentHeader>Download Configuration</SegmentHeader>
        <Segment loading={loading}>
          <Message size="small" warning>
            <Message.List>
              <Message.Item>Select configuration file to save</Message.Item>
              <Message.Item>
                Please note: running-config may take a while to prepare for download
              </Message.Item>
            </Message.List>
          </Message>
          {!error && (
            <div style={{ display: "flex", alignItems: "flex-start", flexWrap: "wrap" }}>
              <StyledTable key="files_rows_group" collapsing striped>
                <Table.Header>
                  <Table.Row textAlign="center">
                    <Table.HeaderCell colSpan={files.length - 1}>File Name</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  {files.map((filename, i) => {
                    if (filename.length === 0) return false;
                    filename = decodeURIComponentSafe(filename);
                    return (
                      <Table.Row key={`file_cell_${i}`}>
                        <Table.Cell>
                          <Radio
                            checked={fileRadio === filename}
                            label={filename}
                            value={filename}
                            name="fileRadio"
                            onChange={this.handleRadioChange}
                          />
                        </Table.Cell>
                      </Table.Row>
                    );
                  })}
                </Table.Body>
              </StyledTable>
              <StyledTable key="dest_rows_group" collapsing>
                <Table.Header>
                  <Table.Row textAlign="center">
                    <Table.HeaderCell colSpan={dest.length}>Destination Files</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  <Table.Row>
                    {dest.map((value, i) => (
                      <Table.Cell key={`dest_cell_${i}`} textAlign="center">
                        <Radio
                          checked={destRadio === value}
                          label={value}
                          value={value}
                          name="destRadio"
                          onChange={this.handleRadioChange}
                        />
                      </Table.Cell>
                    ))}
                  </Table.Row>
                </Table.Body>
              </StyledTable>
            </div>
          )}

          {message.length > 0 && (
            <Message error size="small">
              <Icon name="exclamation circle" size="small" />
              {message}
            </Message>
          )}

          <Divider />

          <BtnWrapper>
            <SemanticButton
              title="Download Configuration"
              onClick={this.handleSubmit}
              disabled={this.props.readOnly || error}
            />
          </BtnWrapper>
        </Segment>
      </Segment.Group>
    );
  }
}

const StyledTable = styled(Table)`
  min-width: 300px;
  margin: 0 !important;
  margin-right: 1em !important;
`;

export default WithData(IcfgConfDownload, "/config/icfg_conf_get_file_list?op=download", false);
