import React, { Component } from "react";
import { Form } from "semantic-ui-react";

import DragAndDrop from "../../../components/drag_and_drop";

let max_file_name_len = 63;
const initialState = {
  file_src: "Local",
  usb_filename: "running-config",
  source_file: {}
};

class UploadFile extends Component {
  state = initialState;

  handleDropFile = files => {
    this.setState({ source_file: files[0] }, () => this.passingStateToParent());
  };

  onChangeInput = (e, { name, value }) => {
    this.setState({ [name]: value }, () => this.passingStateToParent());
  };

  passingStateToParent = () => {
    const { file_src, usb_filename, source_file } = this.state;
    this.props.handleStateChange({ file_src, usb_filename, source_file });
  };

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps) !== JSON.stringify(this.props)) {
      if (this.props.shouldInitialize) {
        this.setState(initialState, () => this.passingStateToParent());
      }
    }
  }

  componentDidMount() {
    this.passingStateToParent();
  }

  render() {
    const { file_src, usb_filename } = this.state;
    const { sources, progress } = this.props;

    return (
      <>
        <Form.Field>
          <label>Choose Upload File Type:</label>
          <Form.Group inline>
            <Form.Radio
              radio
              label="Local"
              checked={file_src === "Local"}
              onChange={this.onChangeInput}
              name="file_src"
              value="Local"
            />
            <Form.Radio
              radio
              label="USB"
              checked={file_src === "USB"}
              onChange={this.onChangeInput}
              name="file_src"
              value="USB"
            />
          </Form.Group>
        </Form.Field>

        <Form.Field>
          {file_src === "Local"
            ? sources.includes("Local") && (
                <DragAndDrop
                  validation={{ error: false, msg: "" }}
                  handleDrop={this.handleDropFile}
                  uploadStatus={""}
                  progress={progress}
                />
              )
            : sources.includes("USB") && (
                <Form.Input
                  label="Type your USB file name:"
                  placeholder="file name"
                  value={usb_filename}
                  name="usb_filename"
                  onChange={this.onChangeInput}
                  maxLength={max_file_name_len}
                />
              )}
        </Form.Field>
      </>
    );
  }
}

export default UploadFile;
