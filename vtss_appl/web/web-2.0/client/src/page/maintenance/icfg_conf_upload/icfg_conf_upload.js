import React, { Component } from "react";
import { Segment, Message, Icon, Form } from "semantic-ui-react";
import { toast } from "react-toastify";

import { SegmentHeader } from "../../../components/styled_component";
import { SemanticButton, BtnWrapper } from "../../../components/semantic-component";
import WithData from "../../../utils/withData";
import DestinationTable from "./destination_table";
import UploadFile from "./upload_file";
import { InfoMsg, msgConfig } from "../../../components/messages";

const initialState = {
  sources: [],
  files: [],
  show_create_file_row: false,
  message: "",
  error: false,
  upload_status: "none", // none -> uploading -> waitServerResponse -> success || error
  progress: 0,
  init_child_component: false
  /*
  　Post Request input initital values are handle by children component
  　file_name: "running-config", file_src: "Local", source_file: {}, usb_filename: "running-config", merge: false　　　　　　　　　　　　
  */
};

class IcfgConfUpload extends Component {
  state = initialState;

  getDestVal = obj => {
    this.setState(obj);
  };

  getSrcFile = obj => {
    this.setState(obj);
  };

  isValid = () => {
    const { file_src, usb_filename, source_file, file_name } = this.state;

    if (file_src === "Local" && typeof source_file.name === "undefined") {
      this.setState({ message: "Please select a source file." });
      return false;
    }

    if (file_src === "USB" && usb_filename.length === 0) {
      this.setState({ message: "Please enter USB file name." });
      return false;
    }

    if (!file_name) {
      this.setState({ message: "Please select a file source." });
      return false;
    }

    if (file_name === "create_file") {
      this.setState({ message: "create_file name is reserved. Please change to other file name." });
      return false;
    }

    return true;
  };

  handleSubmit = () => {
    if (!this.isValid()) return false;

    this.setState({ upload_status: "uploading", message: "" });

    // form data can only contain "file_name", "merge", "source file", "file_src", "usb_filename"
    const { file_src, source_file, usb_filename, file_name, merge } = this.state;
    let ajax = new XMLHttpRequest();
    let formData = new FormData();

    formData.append("file_src", file_src);
    switch (file_src) {
      case "Local":
        formData.append("source_file", source_file);
        break;
      case "USB":
        formData.append("usb_filename", usb_filename);
        break;
      default:
        return false;
    }
    formData.append("file_name", file_name);
    formData.append("merge", merge);

    ajax.upload.addEventListener("progress", this.progressHandler);
    ajax.upload.addEventListener("load", e => this.httpReqLoadHandler);
    ajax.open("post", "/config/icfg_conf_upload");
    ajax.setRequestHeader("Lntn-Web-Ver", "2.0");
    ajax.send(formData);
    ajax.onreadystatechange = () => {
      if (ajax.readyState === 4) {
        if (ajax.status && ajax.status === 201) {
          this.setState({ upload_status: "success" });
          toast.success(InfoMsg("success", ajax.responseText), {
            ...msgConfig(),
            onOpen: () => this.props.refresh(true)
          });
        } else if (ajax.status && ajax.status === 200) {
          this.setState({ upload_status: "success" });
          return this.props.history.push("/maintenance/configuration/running");
        } else {
          this.setState({ upload_status: "error" });
          toast.error(InfoMsg("error", ajax.responseText, ajax.status, ajax.statusText), msgConfig());
        }
      }
      // going back to initial state
      this.setState(initialState, () => this.processUpdate());
    };
  };

  progressHandler = e => {
    const precentage = Math.floor((e.loaded / e.total) * 100);
    this.setState({ progress: precentage });
  };

  processUpdate = () => {
    const fields = this.props.data.split("*");
    const status = fields[0];
    const sources = fields.slice(1, 3);
    const files = fields.slice(3);

    switch (status) {
      case "OK":
        if (sources.length < 2) {
          return this.setState({ message: "NO destinations available for download.", error: true });
        }

        // eslint-disable-next-line no-undef
        if (files.length - 1 >= configIcfgRwFilesMax + (files.includes("running-config") ? 1 : 0)) {
          this.setState({
            message:
              "Note: File system is full; either delete a file to make room, or select an existing file."
          });
        } else {
          this.setState({ show_create_file_row: true });
        }

        this.setState({ sources, files });
        break;
      case "ERR_LOCK":
        this.setState({
          message: "Another I/O operation is in progress. Please try again later.",
          error: true
        });
        break;
      case "ERR":
        this.setState({
          message: "Could not retrieve file list from switch. Please try again later.",
          error: true
        });
        break;
      default:
        return false;
    }
  };

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps) !== JSON.stringify(this.props)) {
      this.processUpdate();
    }
  }

  componentDidMount() {
    this.processUpdate();
  }

  render() {
    const { sources, files, message, error, show_create_file_row, upload_status, progress } = this.state;

    return (
      <Segment.Group>
        <SegmentHeader>Upload Configuration</SegmentHeader>
        <Segment loading={upload_status === "uploading"}>
          <Message warning>
            <Icon name="exclamation circle" />
            Please note: If the configuration changes IP settings, management connectivity may be lost.
          </Message>
          <Form>
            {!error && (
              <>
                <UploadFile
                  sources={sources}
                  progress={progress}
                  handleStateChange={this.getSrcFile}
                  shouldInitialize={upload_status === "success" || upload_status === "error"}
                />
                <DestinationTable
                  availableFiles={files}
                  showCreateFileRow={show_create_file_row}
                  handleStateChange={this.getDestVal}
                  shouldInitialize={upload_status === "success" || upload_status === "error"}
                />
              </>
            )}

            {message.length > 0 && (
              <Message negative size="small">
                <Icon name="exclamation circle" size="small" />
                {message}
              </Message>
            )}

            <BtnWrapper>
              <SemanticButton
                title="Upload Configuration"
                disabled={this.props.readOnly || error}
                onClick={this.handleSubmit}
              />
            </BtnWrapper>
          </Form>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(IcfgConfUpload, "/config/icfg_conf_get_file_list?op=upload", false);
