import React, { Component } from "react";
import { Table, Form, Checkbox } from "semantic-ui-react";
import { decodeURIComponentSafe } from "../../../utils/self-documenting_func";

let max_file_name_len = 63;
const initialState = {
  file_name: "running-config",
  merge_mode: "merge_mode_radio_r",
  new_file_name: ""
};

class DestinationTable extends Component {
  state = initialState;

  onChangeInput = (e, { name, value }) => {
    this.setState({ [name]: value }, () => {
      this.passingStateToParent();
    });
  };

  passingStateToParent() {
    // Passing object must contain { filename: String, merge: boolean }
    let file_name = this.state.file_name;
    let merge = false;

    if (this.state.file_name === "create_file") {
      file_name = this.state.new_file_name;
    }

    if (this.state.merge_mode === "merge_mode_radio_r") {
      merge = false;
    } else if (this.state.merge_mode === "merge_mode_radio_m") {
      merge = true;
    }

    this.props.handleStateChange({ file_name, merge });
  }

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps) !== JSON.stringify(this.props)) {
      if (this.props.shouldInitialize) {
        console.log("hi");
        this.setState(initialState, () => this.passingStateToParent());
      }
    }
  }

  componentDidMount() {
    this.passingStateToParent();
  }

  render() {
    const files = this.props.availableFiles;
    const { file_name, merge_mode, new_file_name } = this.state;

    return (
      <Form.Field>
        <label>Choose Destination File:</label>
        <Table compact celled definition striped>
          <Table.Header>
            <Table.Row textAlign="center">
              <Table.HeaderCell />
              <Table.HeaderCell>File Name</Table.HeaderCell>
              <Table.HeaderCell>Parmameter</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {files.map(file => {
              file = decodeURIComponentSafe(file);
              if (file === "") return false;
              if (file === "running-config") {
                return (
                  <Table.Row textAlign="center" key={file}>
                    <Table.Cell collapsing>
                      <Checkbox
                        slider
                        name="file_name"
                        value="running-config"
                        checked={file_name === "running-config"}
                        onChange={this.onChangeInput}
                      />
                    </Table.Cell>
                    <Table.Cell disabled={file_name !== "running-config"}>running-config</Table.Cell>
                    <Table.Cell>
                      <Form.Group inline style={{ margin: "0", justifyContent: "center" }}>
                        <Form.Radio
                          radio
                          label="Replace"
                          name="merge_mode"
                          checked={merge_mode === "merge_mode_radio_r"}
                          onChange={this.onChangeInput}
                          value="merge_mode_radio_r"
                          disabled={file_name !== "running-config"}
                        />
                        <Form.Radio
                          radio
                          label="Merge"
                          name="merge_mode"
                          checked={merge_mode === "merge_mode_radio_m"}
                          onChange={this.onChangeInput}
                          value="merge_mode_radio_m"
                          disabled={file_name !== "running-config"}
                        />
                      </Form.Group>
                    </Table.Cell>
                  </Table.Row>
                );
              }
              return (
                <Table.Row textAlign="center" key={file}>
                  <Table.Cell collapsing>
                    <Checkbox
                      slider
                      name="file_name"
                      value={file}
                      checked={file_name === file}
                      onChange={this.onChangeInput}
                    />
                  </Table.Cell>
                  <Table.Cell disabled={file_name !== file}>{file}</Table.Cell>
                  <Table.Cell disabled={file_name !== file} />
                </Table.Row>
              );
            })}

            {this.props.showCreateFileRow && (
              <Table.Row textAlign="center">
                <Table.Cell collapsing>
                  <Checkbox
                    slider
                    name="file_name"
                    value="create_file"
                    checked={file_name === "create_file"}
                    onChange={this.onChangeInput}
                  />
                </Table.Cell>
                <Table.Cell disabled={file_name !== "create_file"}>Create new file</Table.Cell>
                <Table.Cell>
                  <Form.Input
                    disabled={file_name !== "create_file"}
                    name="new_file_name"
                    value={new_file_name}
                    onChange={this.onChangeInput}
                    maxLength={max_file_name_len}
                  />
                </Table.Cell>
              </Table.Row>
            )}
          </Table.Body>
        </Table>
      </Form.Field>
    );
  }
}

export default DestinationTable;
