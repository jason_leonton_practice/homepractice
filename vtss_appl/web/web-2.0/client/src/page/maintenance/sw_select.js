import React, { Component } from "react";
import { Segment, Icon } from "semantic-ui-react";

import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { decodeURIComponentSafe } from "../../utils/self-documenting_func";
import WresetBooting from "../../page/maintenance/wreset/wreset_booting";
import withData from "../../utils/withData";

export class SwSelect extends Component {
  state = { content: "", disabled: true, submitted: false };

  onCancel = () => this.props.history.push("/");

  onSubmit = () => {
    if (!window.confirm("Are you sure you want to activate the alternate software image?")) return false;

    fetch("/config/misc", {
      method: "post",
      body: "altimage=Activate Alternate Image",
      headers: { "Content-Type": "application/x-www-form-urlencoded" }
    })
      .then(res => {
        try {
          if (res.status === 200) {
            this.setState({ submitted: true });
          } else {
            throw new Error(res.status);
          }
        } catch (e) {
          console.log(e.message);
        }
      })
      .catch(error => console.log(error));
  };

  processUpdate = () => {
    const { data } = this.props;

    if (data) {
      const img = data.split("^");
      const content = img.map((ele, index) => {
        let props = ele.split("|");

        if (index === 0) return <ImageTable imageType="Active Image" values={props} key={`pri_${index}`} />;
        return <ImageTable imageType="Alternative Image" values={props} key={`alt_${index}`} />;
      });

      if (img.length === 2) {
        this.setState({ disabled: false }); // Since there are two images can be used, submit is allowed
      } else {
        this.setState({ disabled: true });
      }

      this.setState({ content });
    }
  };

  componentDidMount() {
    this.processUpdate();
  }

  render() {
    const { readOnly } = this.props;

    return (
      <Segment.Group>
        <SegmentHeader>Sofware Image Selection</SegmentHeader>
        <Segment>
          {!this.state.submitted ? (
            <React.Fragment>
              {this.state.content}
              <BtnWrapper>
                <SemanticButton title="Cancel" onClick={this.onCancel} />
                <SemanticButton
                  title="Activate Alternate Image"
                  disabled={readOnly || this.state.disabled}
                  onClick={this.onSubmit}
                />
              </BtnWrapper>
            </React.Fragment>
          ) : (
            <WresetBooting />
          )}
        </Segment>
      </Segment.Group>
    );
  }
}

function ImageTable(props) {
  return (
    <Table celled striped compact>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell colSpan="2">{props.imageType}</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        <Table.Row>
          <Table.Cell width={3}>
            <Icon name="file code outline" /> Image
          </Table.Cell>
          <Table.Cell>{decodeURIComponentSafe(props.values[0])}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>
            <Icon name="sync" /> Version
          </Table.Cell>
          <Table.Cell>{decodeURIComponentSafe(props.values[1])}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>
            <Icon name="calendar outline" /> Date
          </Table.Cell>
          <Table.Cell>{decodeURIComponentSafe(props.values[2])}</Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  );
}

export default withData(SwSelect, "/config/sw_select", false);
