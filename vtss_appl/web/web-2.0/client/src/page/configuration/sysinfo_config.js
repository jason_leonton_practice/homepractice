import React from "react";
import { Segment, Form } from "semantic-ui-react";
import { Form as FinalForm } from "react-final-form";
import { toast } from "react-toastify";
import qs from "querystring";

import WithData from "../../utils/withData";
import Field from "../../components/final_form_field_adapter";
import { CustomInput } from "../../components/input-field";
import { InfoMsg, msgConfig } from "../../components/messages";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { isWithinRange } from "../../utils/validate";
import { decodeURIComponentSafe } from "../../utils/self-documenting_func";
import { SegmentHeader } from "../../components/styled_component";

function isDisplayString(str) {
  let myReg = /^[ -~]{0,}$/;

  return myReg.test(str);
}

function isAdministrativelyName(str) {
  let myReg;

  if (!str) return true;

  myReg = /^[A-Za-z]$/;

  if (myReg.test(str)) return true;

  myReg = /^[A-Za-z][A-Za-z0-9-]{0,}[A-Za-z0-9]$/;

  return myReg.test(str);
}

class SysinfoConfig extends React.Component {
  constructor(props) {
    super(props);

    this.maxLen = 255;
    this.maxTzLen = 5;
  }

  validate = values => {
    const errors = {};

    // check sys_contact
    if (values["sys_contact"].toString().length > 255)
      errors["sys_contact"] = "The length of 'System Contact' is restricted to 0 - 255";

    if (!isDisplayString(values["sys_contact"]))
      errors["sys_contact"] = "The 'System Contact' is restricted to ASCII characters from 32 to 126";

    // check sys_name
    if (values["sys_name"].toString().length > 255)
      errors["sys_name"] = "The length of 'System Name' is restricted to 0 - 255";

    if (!isAdministrativelyName(values["sys_name"]))
      errors["sys_name"] = `'System Name' is not valid. Please refer to the help page for the valid format.`;

    // check sys_location
    if (values["sys_location"].toString().length > 255)
      errors["sys_location"] = `The length of 'System Location' is restricted to 0 - 255`;
    if (!isDisplayString(values["sys_location"]))
      errors["sys_location"] = `The 'System Location' is restricted to ASCII characters from 32 to 126`;

    // check timezone
    if (values["timezone"]) {
      const chk_timezone = isWithinRange(values["timezone"], -1439, 1439, "Timezone offset", "minutes");
      if (chk_timezone["error"]) errors["timezone"] = chk_timezone["msg"];
    }

    return errors;
  };

  onSubmit = values => {
    const payload = qs.stringify(values);

    if (values["sys_contact"] === "") {
      if (!window.confirm("System Contact is empty.\nDo you want to proceed anyway?")) return false;
    }
    if (values["sys_name"] === "") {
      if (!window.confirm("System Name is empty.\nDo you want to proceed anyway?")) return false;
    }
    if (values["sys_location"] === "") {
      if (!window.confirm("System Location is empty.\nDo you want to proceed anyway?")) return false;
    }

    fetch("/config/sysinfo", {
      method: "POST",
      body: payload,
      headers: { "Content-Type": "application/x-www-form-urlencoded" }
    })
      .then(res => {
        if (res.status === 200) {
          this.props.refresh();
          const info = "Form has been submitted successfully!";
          toast.success(InfoMsg("Success", info, res.status, res.statusText), msgConfig());
        } else {
          toast.error(InfoMsg("Error", "", res.status, res.statusText), msgConfig());
        }
      })
      .catch(error => {
        toast.error(InfoMsg("Error", error.msg), msgConfig());
      });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  setInitialValue = () => {
    const sysInfo = this.props.data.split(",");

    if (!sysInfo) {
      return { sys_contact: "", sys_name: "", sys_location: "" };
    }

    const firstThreeValues = {
      sys_contact: decodeURIComponentSafe(sysInfo[0]),
      sys_name: decodeURIComponentSafe(sysInfo[1]),
      sys_location: decodeURIComponentSafe(sysInfo[2])
    };

    if (sysInfo.length === 4) return { ...firstThreeValues, timezone: decodeURIComponentSafe(sysInfo[3]) };

    return { ...firstThreeValues };
  };

  render() {
    const { data, readOnly } = this.props;
    const sysInfo = data.split(",");
    const initialValues = this.setInitialValue();

    return (
      <Segment.Group>
        <SegmentHeader>System Information Configuration</SegmentHeader>
        <Segment>
          <FinalForm
            onSubmit={this.onSubmit}
            initialValues={initialValues}
            validate={this.validate}
            subscription={{
              submitting: true,
              pristine: true,
              submitSucceeded: true
            }}
          >
            {({ form, submitting, pristine, handleSubmit, submitSucceeded, submitError }) => {
              return (
                <Form size="tiny">
                  <Field name="sys_contact">
                    {props => (
                      <CustomInput
                        fieldProps={props}
                        label="System Contact"
                        features={{ maxLength: this.maxLen }}
                      />
                    )}
                  </Field>

                  <Field name="sys_name">
                    {props => (
                      <CustomInput
                        fieldProps={props}
                        label="System Name"
                        features={{ maxLength: this.maxLen }}
                      />
                    )}
                  </Field>

                  <Field name="sys_location">
                    {props => (
                      <CustomInput
                        fieldProps={props}
                        label="System Location"
                        features={{ maxLength: this.maxLen }}
                      />
                    )}
                  </Field>

                  {sysInfo.length === 4 ? (
                    <Field name="timezone">
                      {props => (
                        <CustomInput
                          fieldProps={props}
                          label="System Timezone Offset (minutes)"
                          features={{ maxLength: this.maxTzLen }}
                        />
                      )}
                    </Field>
                  ) : null}

                  <BtnWrapper>
                    <SemanticButton title="reset" disabled={submitting || pristine} onClick={form.reset} />
                    <SemanticButton title="save" disabled={submitting || readOnly} onClick={handleSubmit} />
                  </BtnWrapper>
                </Form>
              );
            }}
          </FinalForm>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(SysinfoConfig, "/config/sysinfo", false);
