import React, { Component } from "react";
import { Segment, Divider } from "semantic-ui-react";
import { Form as FinalForm, Field } from "react-final-form";
import qs from "querystring";
import { toast } from "react-toastify";

import WithData from "../../utils/withData";
import { InfoMsg, msgConfig } from "../../components/messages";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { SelectInput } from "../../components/input-field";
import { SegmentHeader } from "../../components/styled_component";

const dropdownFeatures = { selection: true };
const dropdownOptions = [
  { key: 0, value: 0, text: "Disabled" },
  { key: 1, value: 1, text: "Enabled" }
];

class SshConfig extends Component {
  onSubmit = values => {
    const body = qs.stringify(values);

    fetch("/config/ssh", {
      method: "post",
      body,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    })
      .then(res => {
        if (res.status === 200) {
          toast.success(InfoMsg("success", null, res.status, res.statusText), {
            ...msgConfig(),
            onOpen: () => this.props.refresh(true)
          });
        } else {
          toast.error(InfoMsg("error", null, res.status, res.statusText), msgConfig());
        }
      })
      .catch(e => {
        toast.error(InfoMsg("error", null), msgConfig());
      });
  };

  render() {
    const { data, readOnly } = this.props;
    const initVal = data ? { ssh_mode: parseInt(data) } : 0;

    return (
      <Segment.Group>
        <SegmentHeader>SSH Configuration</SegmentHeader>
        <Segment>
          <FinalForm
            onSubmit={this.onSubmit}
            initialValues={initVal}
            subscription={{
              submitting: true,
              pristine: true,
              submitSucceeded: true
            }}
          >
            {({ form, submitting, pristine, handleSubmit, submitSucceeded, submitError }) => {
              return (
                <>
                  <Field
                    name="ssh_mode"
                    component={attr => SelectInput(attr, dropdownOptions, dropdownFeatures)}
                  />

                  <Divider />

                  <BtnWrapper>
                    <SemanticButton
                      title="reset"
                      type="button"
                      disabled={pristine || submitting}
                      onClick={form.reset}
                    />
                    <SemanticButton
                      type="submit"
                      title="save"
                      disabled={submitting || readOnly}
                      onClick={handleSubmit}
                    />
                  </BtnWrapper>
                </>
              );
            }}
          </FinalForm>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(SshConfig, "/config/ssh", false);
