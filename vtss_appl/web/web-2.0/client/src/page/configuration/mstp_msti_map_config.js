import React, { Component } from "react";
import { Form } from "react-final-form";
import { Segment, Message } from "semantic-ui-react";
import { toast } from "react-toastify";
import qs from "querystring";

import { InfoMsg, msgConfig } from "../../components/messages";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { TextInput, TextArea } from "../../components/input-field";
import { decodeURIComponentSafe } from "../../utils/self-documenting_func";
import { isWithinRange } from "../../utils/validate";
import Field from "../../components/final_form_field_adapter";
import WithData from "../../utils/withData";

class MstpMstiMapConfig extends Component {
  state = { loading: false };

  onSubmit = async (values, form) => {
    console.log(form.getState());
    let errorsObj = this.validate(values);

    // 判斷是否有 emtpy object，如果為emtpy代表沒有表格值沒有錯誤
    if (Object.keys(errorsObj).length !== 0 && errorsObj.constructor === Object) {
      return errorsObj;
    }

    for (let i = 1; i <= 7; i++) {
      let elms = values[`map_${i}`].split(/\s*,+\s*|\s+/);
      values[`map_${i}`] = elms.join(",");
    }

    this.setState({ loading: true });
    let payload = qs.stringify(values);
    const res = await fetch("/config/rstp_msti_map", {
      method: "POST",
      body: payload,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    });
    const [status, statusText, body] = await Promise.all([res.status, res.statusText, res.text()]);
    if (status === 200) {
      toast.success(InfoMsg("success", null, status, statusText), msgConfig());
      this.props.refresh(true);
    } else {
      toast.error(InfoMsg("error", body, status, statusText), msgConfig());
    }
    this.setState({ loading: false });
  };

  validate = values => {
    let error = {};

    if (values["name"].length === 0 && !window.confirm("Leave 'Configuration Name' empty?")) {
      error.name = "Please provide configuraiton name or confirm empty configuration name.";
    }

    const chkRevision = isWithinRange(values["revision"], 0, 0xffff, "'Configuration revision'");
    if (chkRevision.error) {
      error.revision = chkRevision.msg;
    }

    let i,
      map = new Array(4096);

    for (i = 0; i < map.length; i++) {
      map[i] = 0;
    }

    for (let i = 1; i <= 7; i++) {
      let elms = values[`map_${i}`].split(/\s*,+\s*|\s+/);

      for (let j = 0; j < elms.length; j++) {
        let elm = elms[j];

        if (elm.length) {
          let parts = elm.match(/^(\d+)(-(\d+))?$/);

          if (!parts) {
            // 測試方式: 輸入 adsfasdf
            error[`map_${i}`] = `MST ${i}: VLAN ${elm} should be either a single VLAN or a range`;
            return error;
          }
          let vlan = parseInt(parts[1], 10);
          if (vlan === 0 || vlan >= 4095) {
            // 測試方式: 輸入 數值等於0 or >= 4095
            error[`map_${i}`] = `MST ${i}: VLAN ${parts[1]} is invalid"`;
            return error;
          }
          let vlan_end;
          if (parts[3]) {
            vlan_end = parseInt(parts[3], 10);
            if (vlan_end === 0 || vlan_end >= 4095) {
              error[`map_${i}`] = `MST ${i}: VLAN ${parts[3]} is invalid`;
              return error;
            }
          } else {
            vlan_end = vlan;
          }
          if (vlan > vlan_end) {
            error[`map_${i}`] = `MST ${i}: VLAN range ${elm} is invalid`;
            return error;
          }
          for (let v = vlan; v <= vlan_end; v++) {
            if (map[v] !== "undefined" && map[v] !== 0) {
              // 測試方式: 於兩個textarea輸入相同數字
              error[`map_${i}`] = `MST ${i} : VLAN ${v} is already mapped to MST ${map[v]}`;
              return error;
            }
            map[v] = i;
          }
        }
      }
    }

    return error;
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    const { data, readOnly } = this.props;
    let tableBody = [];
    let initialValues = {};

    if (data) {
      let msticonfig = data.split("|");

      initialValues["name"] = decodeURIComponentSafe(msticonfig.shift()); // get the first array element "mac address"
      initialValues["revision"] = msticonfig.shift();

      msticonfig.forEach((row, i) => {
        if (row) {
          let pval = msticonfig[i].split("/");
          let mstino = parseInt(pval[0]);
          let msti = pval[1];
          let vlan = pval[2];

          // Initial Values
          initialValues[`map_${mstino}`] = vlan;

          tableBody.push(
            <Table.Row key={`row_${i}`}>
              <Table.Cell textAlign="center" width="2">
                {msti}
              </Table.Cell>
              <Table.Cell>
                <Field name={`map_${mstino}`} render={props => TextArea(props)}></Field>
              </Table.Cell>
            </Table.Row>
          );
        }
      });
    }

    return (
      <Segment.Group>
        <SegmentHeader>MSTI Identification & Mapping Configuration</SegmentHeader>
        <Segment loading={this.state.loading}>
          <Message warning>
            <Message.List>
              <Message.Item>Add VLANs separated by spaces or comma.</Message.Item>
              <Message.Item>
                <strong>Unmapped VLANs are mapped to the CIST</strong>. (The default bridge instance).
              </Message.Item>
            </Message.List>
          </Message>

          <Form
            onSubmit={this.onSubmit}
            initialValues={initialValues}
            subscription={{ pristine: true, submitSucceeded: true }}
          >
            {({ handleSubmit, pristine, form, submitting, submitError, submitSucceeded }) => (
              <React.Fragment>
                <ConfIdentification />

                <h4>MSTI Mapping</h4>
                <Table celled compact striped>
                  <Table.Header>
                    <Table.Row textAlign="center">
                      <Table.HeaderCell>MSTI</Table.HeaderCell>
                      <Table.HeaderCell>Mapped</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>{tableBody}</Table.Body>
                </Table>

                <BtnWrapper>
                  <SemanticButton
                    title="reset"
                    type="button"
                    disabled={pristine || submitting}
                    onClick={form.reset}
                  />
                  <SemanticButton
                    title="save"
                    type="submit"
                    disabled={submitting || readOnly}
                    onClick={handleSubmit}
                  />
                </BtnWrapper>
              </React.Fragment>
            )}
          </Form>
        </Segment>
      </Segment.Group>
    );
  }
}

function ConfIdentification() {
  return (
    <>
      <h4>Configuration Identification</h4>
      <Table celled compact collapsing definition>
        <Table.Body>
          <Table.Row>
            <Table.Cell>Configuration Name</Table.Cell>
            <Table.Cell>
              <Field name="name" render={props => TextInput(props, { maxLength: 32 })} />
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>Configuration Revision</Table.Cell>
            <Table.Cell>
              <Field name="revision" render={props => TextInput(props, { maxLength: 5 })} />
            </Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>
    </>
  );
}

export default WithData(MstpMstiMapConfig, "/config/rstp_msti_map", false);
