import React, { Component } from "react";
import { Form, Field } from "react-final-form";
import { Segment, Divider } from "semantic-ui-react";
import { toast } from "react-toastify";
import qs from "querystring";

import { isIpStr } from "../../utils/validate";
import { InfoMsg, msgConfig } from "../../components/messages";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { SelectInput } from "../../components/input-field";
import { MCDropdown } from "../../utils/master_control";
import WithData from "../../utils/withData";

const modeOptions = [
  { key: 0, value: 0, text: "Disabled" },
  { key: 1, value: 1, text: "Enabled" }
];
const portMode = [
  { key: 0, value: 0, text: "Trusted" },
  { key: 1, value: 1, text: "Untrusted" }
];
const dropdownFeatures = { selection: true };
let dhcp_snooping_supported = 0;

class DhcpSnooping extends Component {
  state = { loading: false };

  onSubmit = async values => {
    this.setState({ loading: true });
    let payload = qs.stringify(values);

    const res = await fetch("/config/dhcp_snooping", {
      method: "POST",
      body: payload,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    });
    const [status, statusText, body] = await Promise.all([res.status, res.statusText, res.text()]);

    if (status === 200) {
      toast.success(InfoMsg("success", null, status, statusText), msgConfig());
      this.props.refresh(true);
    } else {
      toast.error(InfoMsg("error", body, status, statusText), msgConfig());
    }

    this.setState({ loading: false });
  };

  validate = values => {
    let obj = {};

    // relay_server
    let chkRelayServer = isIpStr(values["relay_server"], false, "'Relay Server'", 0);
    if (chkRelayServer.error) {
      obj = { relay_server: chkRelayServer.msg };
    }

    if (dhcp_snooping_supported && parseInt(values["relay_mode"]) === 1) {
      if (!window.confirm("Please make sure the DHCP server connected on trust port?")) {
        obj = { relay_mode: "Make sure the DHCP server connected on trust port." };
      }
    }

    // relay_info_mode & relay_info_policy
    // Invalid when relay_info_mode === "Disabled" && relay_info_policy === "Replace"
    if (parseInt(values["relay_info_mode"]) === 0 && parseInt(values["relay_info_policy"]) === 0) {
      obj = { relay_info_policy: "Don't allow 'Replace policy' when relay information mode is disabled." };
    }

    return obj;
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    const { data, readOnly } = this.props;
    let snoopingModeDropdown = [];
    let portModeTableBody = [];
    let initialValues = {};
    let masterDropdown;

    if (data) {
      let configData = this.props.data.split(",");

      // Snooping mode dropdown
      snoopingModeDropdown.push(
        <Field
          key="snooping_mode_dropdown"
          name="snooping_mode"
          render={props => SelectInput(props, modeOptions, dropdownFeatures)}
        />
      );

      // Port mode configuration
      let portConfigData = configData[1].split("|");
      let portCount = []; // 紀錄總port數用於master control上

      portConfigData.forEach((port, i) => {
        if (port) {
          let parts = port.split("/");

          // 紀錄每個port的id在array中，給master control使用
          portCount[parts[0]] = parts[0];

          // Iniitial Value
          initialValues[`port_mode_${parts[0]}`] = parseInt(parts[1]);

          portModeTableBody.push(
            <Table.Row key={`port_mode_row_${i}`}>
              <Table.Cell textAlign="center">{parts[0]}</Table.Cell>
              <Table.Cell>
                <Field
                  name={`port_mode_${parts[0]}`}
                  render={props => SelectInput(props, portMode, dropdownFeatures)}
                />
              </Table.Cell>
            </Table.Row>
          );
        }
      });

      // Initial value
      initialValues["snooping_mode"] = parseInt(configData[0]);
      initialValues["master_control_port_mode"] = -1;

      masterDropdown = new MCDropdown(
        "port_mode",
        portCount,
        [0, 1],
        [-1, 0, 1],
        ["<>", "Trusted", "Untrusted"],
        dropdownFeatures
      );
    }

    return (
      <Segment.Group>
        <SegmentHeader>DHCP Snooping Configuration</SegmentHeader>
        <Segment loading={this.state.loading}>
          <Form onSubmit={this.onSubmit} initialValues={initialValues} subscription={{ pristine: true }}>
            {({ handleSubmit, pristine, form, submitting }) => (
              <React.Fragment>
                <Table celled compact definition collapsing>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell>Snooping Mode</Table.Cell>
                      <Table.Cell>{snoopingModeDropdown}</Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>

                {/* master control for port mode configuration*/}
                {masterDropdown.construct()}

                <h4>Port Mode Configuration</h4>
                <Table celled compact striped collapsing>
                  {data && (
                    <Table.Header>
                      <Table.Row textAlign="center">
                        <Table.HeaderCell>Port</Table.HeaderCell>
                        <Table.HeaderCell>Mode</Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                  )}
                  <Table.Body>
                    <Table.Row>
                      {/* master control dropdown */}
                      <Table.Cell textAlign="center">*</Table.Cell>
                      <Table.Cell>{masterDropdown.genComponents()}</Table.Cell>
                    </Table.Row>
                    {portModeTableBody}
                  </Table.Body>
                </Table>

                <Divider />

                <BtnWrapper>
                  <SemanticButton
                    title="reset"
                    type="button"
                    disabled={pristine || submitting}
                    onClick={form.reset}
                  />
                  <SemanticButton
                    title="save"
                    type="submit"
                    disabled={submitting || readOnly}
                    onClick={handleSubmit}
                  />
                </BtnWrapper>
              </React.Fragment>
            )}
          </Form>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(DhcpSnooping, "/config/dhcp_snooping", false);
