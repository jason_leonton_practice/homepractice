import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Segment } from "semantic-ui-react";

import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import withData from "../../utils/withData";

const { configDefaultUserNameNullStr } = require("../../utils/config");

export class Users extends Component {
  constructor(props) {
    super(props);
    this.state = { max_configs: 0, users: [], filter_link_href: 0 };
  }

  handleAddUser = () => {
    const { users, max_configs } = this.state;

    if (users.length - 1 > max_configs) {
      alert(`You cannot add more than ${max_configs} users!`);
    } else {
      this.props.history.push("/configuration/security/switch/user_config");
    }
  };

  processUpdate = () => {
    //Format: [max_num]|[user_idx],[user_name],[priv_level]|...
    const { data, readOnly } = this.props;

    if (data) {
      if (readOnly) {
        this.setState({ filter_link_href: 1 });
      }

      let values = data.split("|");
      let [max_configs, ...users] = values;

      this.setState({
        max_configs,
        users
      });
    }
  };

  componentDidMount() {
    this.processUpdate();
  }

  render() {
    const { readOnly } = this.props;
    const usersTable = this.state.users.map((userRow, index) => {
      if (userRow) {
        let user = userRow.split(",");
        let userNum = user[0];
        let username = "";
        let privLevel = 0;

        for (let i = 0; i < user.length; i++) {
          /* eslint-disable-next-line no-undef */
          if (configDefaultUserNameNullStr && user[1].length === 0) {
            username = "[default-administrator]";
          } else {
            username = decodeURI(user[1]);
          }

          if (user[2]) {
            privLevel = user[2];
          }
        }

        return (
          <Table.Row key={user[1]}>
            <Table.Cell>
              {readOnly ? (
                `${username}`
              ) : (
                <NavLink to={`/configuration/security/switch/user_config?user=${userNum}`}>
                  {username}
                </NavLink>
              )}
            </Table.Cell>
            <Table.Cell>{privLevel}</Table.Cell>
          </Table.Row>
        );
      } else {
        return false;
      }
    });

    return (
      <Segment.Group>
        <SegmentHeader>Users Configuration</SegmentHeader>
        <Segment>
          <Table>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Username</Table.HeaderCell>
                <Table.HeaderCell>Privilege</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{usersTable}</Table.Body>
          </Table>
          <BtnWrapper
            children={
              <SemanticButton title="Add New User" disabled={readOnly} onClick={this.handleAddUser} />
            }
          />
        </Segment>
      </Segment.Group>
    );
  }
}

export default withData(Users, "/stat/users", false);
