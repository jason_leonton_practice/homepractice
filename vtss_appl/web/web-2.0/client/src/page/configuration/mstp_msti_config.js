import React, { Component } from "react";
import { Form, Field } from "react-final-form";
import { Segment, Divider } from "semantic-ui-react";
import { toast } from "react-toastify";
import qs from "querystring";

import { InfoMsg, msgConfig } from "../../components/messages";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { SelectInput } from "../../components/input-field";
import { genOptions } from "../../utils/dyform";
import { MCDropdown } from "../../utils/master_control";
import WithData from "../../utils/withData";

let ppT = [];
let ppV = [];

for (var i = 0; i < 16; i++) {
  ppV[i] = i << 4;
  ppT[i] = String(ppV[i] * 256);
}

const dropdownOptions = genOptions({ text: ppT, value: ppV });
const dropdownFeatures = { selection: true };

class MstpMstiConfig extends Component {
  state = { loading: false };

  onSubmit = async values => {
    this.setState({ loading: true });
    let payload = qs.stringify(values);

    const res = await fetch("/config/rstp_msti", {
      method: "POST",
      body: payload,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    });
    const [status, statusText, body] = await Promise.all([res.status, res.statusText, res.text()]);

    if (status === 200) {
      toast.success(InfoMsg("success", null, status, statusText), msgConfig());
      this.props.refresh(true);
    } else {
      toast.error(InfoMsg("error", body, status, statusText), msgConfig());
    }

    this.setState({ loading: false });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    const { data, readOnly } = this.props;
    let tableBody = [];
    let initialValues = {};
    let masterDropdown;
    let idCount = [];

    if (data) {
      let msticonfigs = data.split("|");

      msticonfigs.forEach((msticonfig, i) => {
        if (msticonfig) {
          let pval = msticonfig.split("/");
          let mstino = parseInt(pval[0]);
          let msti = pval[1];
          let mstiprio = parseInt(pval[2]);

          // 紀錄每個iput的index在array中，給master control使用
          idCount[mstino] = mstino;

          // Initial Values
          initialValues[`mstiprio_${mstino}`] = parseInt(mstiprio);
          initialValues[`master_control_mstiprio`] = -1;

          tableBody.push(
            <Table.Row key={`row_${i}`}>
              <Table.Cell textAlign="center">{msti}</Table.Cell>
              <Table.Cell>
                <Field
                  name={`mstiprio_${mstino}`}
                  render={props => SelectInput(props, dropdownOptions, dropdownFeatures)}
                />
              </Table.Cell>
            </Table.Row>
          );
        }
      });

      masterDropdown = new MCDropdown(
        "mstiprio",
        idCount,
        ppV,
        [-1, ...ppV],
        ["<>", ...ppT],
        dropdownFeatures
      );
    }

    return (
      <Segment.Group>
        <SegmentHeader>MSTI Priority Configuration</SegmentHeader>
        <Segment loading={this.state.loading}>
          <Form
            onSubmit={this.onSubmit}
            initialValues={initialValues}
            subscription={{
              submitting: true,
              pristine: true,
              submitSucceeded: true
            }}
          >
            {({ handleSubmit, pristine, form, submitting, submitError, submitSucceeded }) => (
              <React.Fragment>
                {/* master control for msti priority configuration*/}
                {masterDropdown.construct()}

                <Table celled compact collapsing striped>
                  <Table.Header>
                    <Table.Row textAlign="center">
                      <Table.HeaderCell>MSTI</Table.HeaderCell>
                      <Table.HeaderCell>Priority</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell textAlign="center">*</Table.Cell>
                      <Table.Cell>{masterDropdown.genComponents()}</Table.Cell>
                    </Table.Row>
                    {tableBody}
                  </Table.Body>
                </Table>
                <Divider />

                <BtnWrapper>
                  <SemanticButton
                    title="reset"
                    type="button"
                    disabled={pristine || submitting}
                    onClick={form.reset}
                  />
                  <SemanticButton
                    title="save"
                    type="submit"
                    disabled={submitting || readOnly}
                    onClick={handleSubmit}
                  />
                </BtnWrapper>
              </React.Fragment>
            )}
          </Form>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(MstpMstiConfig, "/config/rstp_msti", false);
