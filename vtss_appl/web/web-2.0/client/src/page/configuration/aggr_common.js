import React, { Component } from "react";
import { Segment, Form } from "semantic-ui-react";
import { Form as FinalForm, Field } from "react-final-form";
import qs from "querystring";
import { toast } from "react-toastify";

import WithData from "../../utils/withData";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { InfoMsg, msgConfig } from "../../components/messages";
import { InnerTable as Table } from "../../components/styled_component";
import { CustomCheckbox } from "../../components/input-field";

const validate = values => {
  let errors = {};
  let falseCount = 0;
  for (const inputName in values) {
    if (!values[inputName]) {
      falseCount++;
    }
  }
  if (falseCount === Object.keys(values).length) {
    errors[Object.keys(values)[0]] = "At least one hash code must be chosen";
  }
  return errors;
};

class AggrCommon extends Component {
  state = { loading: false };

  onSubmit = async values => {
    this.setState({ loading: true });

    //boolean轉回字串來post
    const formValues = this.checkBoxValueTranslate(values);

    const payload = qs.stringify(formValues);

    const res = await fetch("/config/aggr_common", {
      method: "POST",
      body: payload,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    });
    const [status, statusText, body] = await Promise.all([res.status, res.statusText, res.text()]);

    if (status === 200) {
      toast.success(InfoMsg("success", null, res.status, res.statusText), msgConfig());
      this.props.refresh(true);
    } else {
      toast.error(InfoMsg("error", body, status, statusText), msgConfig());
    }

    this.setState({ loading: false });
  };

  checkBoxValueTranslate(values) {
    //未勾選的不傳入後端
    const translateValues = JSON.parse(JSON.stringify(values));
    for (let inputName in translateValues) {
      const value = translateValues[inputName];
      if (value === true) {
        translateValues[inputName] = "on";
      } else {
        delete translateValues[inputName];
      }
    }

    return translateValues;
  }

  setInitialValue = () => {
    const { data } = this.props;
    const aggrconfig = data.split("|");
    const pval = aggrconfig[0].split("/").map(val => {
      return (val = val === "1" ? true : false);
    });

    return {
      src_mac: pval[0],
      det_mac: pval[1],
      mode_ip: pval[2],
      mode_port: pval[3]
    };
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    const { readOnly } = this.props;
    const initialValues = this.setInitialValue();

    return (
      <Segment.Group>
        <Segment className="segment_page_title">Common Aggregation Configuration</Segment>
        <Segment loading={this.state.loading}>
          <FinalForm
            onSubmit={this.onSubmit}
            initialValues={initialValues}
            validate={validate}
            subscription={{
              submitting: true,
              pristine: true
            }}
          >
            {({ form, pristine, handleSubmit, submitting }) => {
              return (
                <Form size="tiny">
                  <Table unstackable celled compact striped textAlign="center">
                    <TableHeader />
                    <TableBody data={this.props.data} />
                  </Table>
                  <BtnWrapper>
                    <SemanticButton
                      title="reset"
                      type="button"
                      disabled={pristine || submitting}
                      onClick={form.reset}
                    />
                    <SemanticButton
                      type="submit"
                      title="save"
                      disabled={submitting || readOnly}
                      onClick={handleSubmit}
                    />
                  </BtnWrapper>
                </Form>
              );
            }}
          </FinalForm>
        </Segment>
      </Segment.Group>
    );
  }
}

/*function component*/

function TableHeader() {
  const tableth = [
    {
      colSpan: 2,
      content: "Hash Code Contributors"
    }
  ];
  let tableHead = (
    <Table.Row>
      {tableth.map((data, i) => {
        return (
          <Table.HeaderCell key={`theade_th_${i}`} colSpan={data.colSpan}>
            {data.content}
          </Table.HeaderCell>
        );
      })}
    </Table.Row>
  );
  return <Table.Header>{tableHead}</Table.Header>;
}

function TableBody(props) {
  const { data } = props;
  const tbodyConfig = [
    {
      title: "Source MAC Address",
      inputName: "src_mac"
    },
    {
      title: "Destination MAC Address",
      inputName: "det_mac"
    },
    {
      title: "IP Address",
      inputName: "mode_ip"
    },
    {
      title: "TCP/UDP Port Number",
      inputName: "mode_port"
    }
  ];
  return (
    <Table.Body>
      {data.split("/").map((value, i) => {
        if (tbodyConfig[i]) {
          return (
            <Table.Row key={`tbody_row_${i}`}>
              <Table.Cell width={8}>{tbodyConfig[i].title}</Table.Cell>
              <Table.Cell width={8}>
                <Field name={tbodyConfig[i].inputName} value="on">
                  {props => <CustomCheckbox fieldProps={props} />}
                </Field>
              </Table.Cell>
            </Table.Row>
          );
        }
        return null;
      })}
    </Table.Body>
  );
}
export default WithData(AggrCommon, "/config/aggr_common", false);
