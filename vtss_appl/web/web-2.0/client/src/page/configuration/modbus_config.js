import React, { Component } from "react";
import { Form, Field } from "react-final-form";
import { Segment, Divider } from "semantic-ui-react";
import { toast } from "react-toastify";
import qs from "querystring";

import { InfoMsg, msgConfig } from "../../components/messages";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { SegmentHeader } from "../../components/styled_component";
import { SelectInput } from "../../components/input-field";
import WithData from "../../utils/withData";

const DropdownOptions = [
  { key: 0, value: 0, text: "Disabled" },
  { key: 1, value: 1, text: "Enabled" }
];
const DropdownFeatures = { selection: true };

class ModbusConfig extends Component {
  onSubmit = values => {
    let payload = qs.stringify(values);

    fetch("/config/modbus", {
      method: "POST",
      body: payload,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    })
      .then(res => {
        if (res.status === 200) {
          toast.success(InfoMsg("success", null), {
            ...msgConfig(),
            onOpen: () => this.props.refresh(true)
          });
        } else {
          toast.error(InfoMsg("error", null, res.status, res.statusText), msgConfig());
        }
      })
      .catch(error => {
        toast.error(InfoMsg("error", null), msgConfig());
      });
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    const { data, readOnly } = this.props;
    const initialValue = parseInt(data);

    return (
      <Segment.Group>
        <SegmentHeader>MODBUS TCP Configuration</SegmentHeader>
        <Segment>
          <Form
            onSubmit={this.onSubmit}
            initialValues={{ modbus_mode: initialValue }}
            subscription={{
              submitting: true,
              pristine: true,
              submitSucceeded: true
            }}
          >
            {({ handleSubmit, pristine, form, submitting, submitError, submitSucceeded }) => (
              <React.Fragment>
                <Field
                  name="modbus_mode"
                  component={attr => SelectInput(attr, DropdownOptions, DropdownFeatures)}
                />

                <Divider />

                <BtnWrapper>
                  <SemanticButton
                    title="reset"
                    type="button"
                    disabled={pristine || submitting}
                    onClick={form.reset}
                  />
                  <SemanticButton
                    title="save"
                    type="submit"
                    disabled={submitting || readOnly}
                    onClick={handleSubmit}
                  />
                </BtnWrapper>
              </React.Fragment>
            )}
          </Form>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(ModbusConfig, "/config/modbus", false);
