/* eslint-disable array-callback-return */
import React, { Component } from "react";
import { Segment } from "semantic-ui-react";
import { Form, Field } from "react-final-form";
import { toast } from "react-toastify";
import qs from "querystring";

import WithData from "../../utils/withData";
import { InfoMsg, msgConfig } from "../../components/messages";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { genArrayInt, genArrayStr, genOptions } from "../../utils/dyform";
import { SelectInput } from "../../components/input-field";
import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";

const dropdownFeatures = { compact: true, selection: true };

class PrivLvl extends Component {
  constructor(props) {
    super(props);
    this.DropdownOptions = genOptions({
      value: genArrayInt(0, 15),
      text: genArrayStr(0, 15)
    });
  }

  validate = (values, groupNameArray, groupNameIndex, adminPrivLevel) => {
    const errors = {};

    let fld1, fld2, fld3, fld4;
    for (let i = 0; i < groupNameIndex; i++) {
      fld1 = `cro_${groupNameArray[i]}`;
      fld2 = `crw_${groupNameArray[i]}`;
      fld3 = `sro_${groupNameArray[i]}`;
      fld4 = `srw_${groupNameArray[i]}`;

      if (values[fld1] > values[fld2]) {
        errors[fld1] = `The privilege level of 'Read-only' should be less or equal 'Read/write'.`;
        errors[fld2] = `The privilege level of 'Read/write' should be great or equal 'Read-only'.`;
      }

      if (values[fld3] > values[fld4]) {
        errors[fld3] = `The privilege level of 'Read-only' should be less or equal 'Read/write'.`;
        errors[fld4] = `The privilege level of 'Read/write' should be great or equal 'Read-Only'.`;
      }

      if (values[fld2] < values[fld3]) {
        errors[
          fld2
        ] = `The privilege level of 'Configuration/Execute Read/write' should be great or equal 'Status/Statistics Read-only'`;
        errors[
          fld3
        ] = `The privilege level of 'Status/Statistics Read-only' should be less or equal 'Configuration/Execute Read/write'`;
      }

      if (groupNameArray[i] === "Maintenance") {
        if (values[fld2] > adminPrivLevel) {
          errors[
            fld2
          ] = `The privilege level of 'admin' is ${adminPrivLevel}. Change to lower privilege will lock yourself out!`;
        }
      }
    }

    return errors;
  };

  postRequest = body => {
    fetch("/config/priv_lvl", {
      method: "POST",
      body,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    })
      .then(res => {
        if (res.status === 200) {
          this.props.refresh();
          toast.success(InfoMsg("success", null, res.status, res.statusText), msgConfig());
        }
      })
      .catch(error => {
        toast.error(InfoMsg("error", null), msgConfig());
      });
  };

  onSubmit = values => {
    // loop through state, store all property into object
    let requestBody = {};

    for (let value in values) {
      if (
        values.hasOwnProperty(value) &&
        (value.startsWith("cro_") ||
          value.startsWith("crw_") ||
          value.startsWith("sro_") ||
          value.startsWith("srw_"))
      )
        requestBody[value] = values[value];
    }
    // create a x-www-form-urlencoded payload
    let payload = qs.stringify(requestBody);
    payload = payload.replace(/\(/g, "%28").replace(/\)/g, "%29");

    this.postRequest(payload);
  };

  setInitialValue = () => {
    let mergeObj = {};
    const inputData = this.props.data.split(",");
    const values = inputData[1].split("|");
    const adminPrivLevel = parseInt(inputData[0], 10);

    values.map(item => {
      if (item) {
        const value = item.split("/");
        const groupName = decodeURI(value[0]);
        const configRoPriv = parseInt(value[1]);
        const configRwPriv = parseInt(value[2]);
        const statusRoPriv = parseInt(value[3]);
        const statusRwPriv = parseInt(value[4]);

        const cro = `cro_${groupName}`;
        const crw = `crw_${groupName}`;
        const sro = `sro_${groupName}`;
        const srw = `srw_${groupName}`;

        const updateState = {};

        updateState[cro] = configRoPriv;
        updateState[crw] = configRwPriv;
        updateState[sro] = statusRoPriv;
        updateState[srw] = statusRwPriv;

        // store all the initial value inside mergeObj
        mergeObj = Object.assign(updateState, mergeObj);
      }
    });
    // dynamic render initial state
    return [mergeObj, adminPrivLevel];
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    const inputData = this.props.data.split(",");
    const propsValues = inputData[1].split("|");
    const initialValues = this.setInitialValue()[0];
    const adminPrivLevel = this.setInitialValue()[1];
    const { readOnly } = this.props;

    let groupNameArray = [];
    let groupNameIndex = 0;

    return (
      <Form
        onSubmit={this.onSubmit}
        initialValues={initialValues}
        validate={values => this.validate(values, groupNameArray, groupNameIndex, adminPrivLevel)}
        subscription={{
          submitting: true,
          pristine: true,
          submitSucceeded: true
        }}
      >
        {({ form, submitting, pristine, values, handleSubmit, submitError, submitSucceeded }) => {
          // dynamic render dropdown
          const node = propsValues.map((item, index) => {
            if (item) {
              const value = item.split("/");
              const groupName = decodeURI(value[0]);
              const cro = `cro_${groupName}`;
              const crw = `crw_${groupName}`;
              const sro = `sro_${groupName}`;
              const srw = `srw_${groupName}`;

              groupNameArray[groupNameIndex++] = groupName;

              return (
                <Table.Row key={groupName} textAlign="center">
                  <Table.Cell>{groupName}</Table.Cell>
                  <Table.Cell>
                    <Field
                      name={cro}
                      component={attr => SelectInput(attr, this.DropdownOptions, dropdownFeatures)}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <Field
                      name={crw}
                      component={attr => SelectInput(attr, this.DropdownOptions, dropdownFeatures)}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <Field
                      name={sro}
                      component={attr => SelectInput(attr, this.DropdownOptions, dropdownFeatures)}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <Field
                      name={srw}
                      component={attr => SelectInput(attr, this.DropdownOptions, dropdownFeatures)}
                    />
                  </Table.Cell>
                </Table.Row>
              );
            }
          });

          return (
            <Segment.Group>
              <SegmentHeader>Privilege Levels Configuration</SegmentHeader>
              <Segment>
                <Table celled structured striped compact>
                  <Table.Header>
                    <Table.Row textAlign="center">
                      <Table.HeaderCell rowSpan="2">Group Name</Table.HeaderCell>
                      <Table.HeaderCell colSpan="4">Privilege Levels</Table.HeaderCell>
                    </Table.Row>
                    <Table.Row textAlign="center">
                      <Table.HeaderCell>Configuration Read-only</Table.HeaderCell>
                      <Table.HeaderCell>Configuration/Execute Read/write</Table.HeaderCell>
                      <Table.HeaderCell>Status/Statistics Read-only</Table.HeaderCell>
                      <Table.HeaderCell>Status/Statistics Read/write</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>{node}</Table.Body>
                </Table>

                <BtnWrapper>
                  <SemanticButton
                    title="reset"
                    type="button"
                    disabled={pristine || submitting}
                    onClick={form.reset}
                  />
                  <SemanticButton
                    type="submit"
                    title="save"
                    disabled={submitting || readOnly}
                    onClick={handleSubmit}
                  />
                </BtnWrapper>
              </Segment>
            </Segment.Group>
          );
        }}
      </Form>
    );
  }
}

export default WithData(PrivLvl, "/config/priv_lvl", false);
