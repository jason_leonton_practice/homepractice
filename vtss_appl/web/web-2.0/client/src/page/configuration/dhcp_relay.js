import React, { Component } from "react";
import { Form, Field } from "react-final-form";
import { Segment, Divider, Table } from "semantic-ui-react";
import { toast } from "react-toastify";
import qs from "querystring";

import { isIpStr } from "../../utils/validate";
import { InfoMsg, msgConfig } from "../../components/messages";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { SegmentHeader } from "../../components/styled_component";
import { SelectInput, TextInput } from "../../components/input-field";
import WithData from "../../utils/withData";

const modeOptions = [
  { key: 0, value: 0, text: "Disabled" },
  { key: 1, value: 1, text: "Enabled" }
];
const policyOptions = [
  { key: 0, value: 0, text: "Replace" },
  { key: 1, value: 1, text: "Keep" },
  { key: 2, value: 2, text: "Drop" }
];
const dropdownFeatures = { selection: true };
let dhcp_snooping_supported = 0;

class DhcpRelay extends Component {
  state = { loading: false };

  onSubmit = async values => {
    let errorsObj = this.validate(values);

    // 判斷是否有 emtpy object，如果為emtpy代表沒有表格值沒有錯誤
    if (Object.keys(errorsObj).length !== 0 && errorsObj.constructor === Object) {
      return errorsObj;
    }

    this.setState({ loading: true });
    let payload = qs.stringify(values);

    const res = await fetch("/config/dhcp_relay", {
      method: "POST",
      body: payload,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    });
    const [status, statusText, body] = await Promise.all([res.status, res.statusText, res.text()]);

    if (status === 200) {
      toast.success(InfoMsg("success", null, res.status, res.statusText), msgConfig());
      this.props.refresh(true);
    } else {
      toast.error(InfoMsg("error", body, status, statusText), msgConfig());
    }

    this.setState({ loading: false });
  };

  validate = values => {
    let obj = {};

    // relay_server
    let chkRelayServer = isIpStr(values["relay_server"], false, "'Relay Server'", 0);
    if (chkRelayServer.error) {
      obj = { relay_server: chkRelayServer.msg };
    }

    if (dhcp_snooping_supported && parseInt(values["relay_mode"]) === 1) {
      if (!window.confirm("Please make sure the DHCP server connected on trust port?")) {
        obj = { relay_mode: "Make sure the DHCP server connected on trust port." };
      }
    }

    // relay_info_mode & relay_info_policy
    // Invalid when relay_info_mode === "Disabled" && relay_info_policy === "Replace"
    if (parseInt(values["relay_info_mode"]) === 0 && parseInt(values["relay_info_policy"]) === 0) {
      obj = { relay_info_policy: "Don't allow 'Replace policy' when relay information mode is disabled." };
    }

    return obj;
  };

  addTableBody = values => {
    // relay_mode
    return (
      <Table.Body key="relay_table_body">
        <Table.Row>
          <TextCellAlignCenter>Relay Mode</TextCellAlignCenter>
          <Table.Cell>
            <Field name="relay_mode" component={attr => SelectInput(attr, modeOptions, dropdownFeatures)} />
          </Table.Cell>
        </Table.Row>
        <Table.Row>
          <TextCellAlignCenter>Relay Server</TextCellAlignCenter>
          <Table.Cell>
            <Field name="relay_server" render={props => TextInput(props, { maxLength: 15, fluid: true })} />
          </Table.Cell>
        </Table.Row>
        <Table.Row>
          <TextCellAlignCenter>Relay Information Mode</TextCellAlignCenter>
          <Table.Cell>
            <Field
              name="relay_info_mode"
              component={attr => SelectInput(attr, modeOptions, dropdownFeatures)}
            />
          </Table.Cell>
        </Table.Row>
        <Table.Row>
          <TextCellAlignCenter>Relay Information Policy</TextCellAlignCenter>
          <Table.Cell>
            <Field
              name="relay_info_policy"
              component={attr => SelectInput(attr, policyOptions, dropdownFeatures)}
            />
          </Table.Cell>
        </Table.Row>
      </Table.Body>
    );
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    const { data, readOnly } = this.props;
    let tableBody = [];
    let initialValue = {};

    //Format: [dhcp_snooping_supported]/[relay_mode]/[relay_server]/[relay_info_mode]/[relay_info_policy]
    if (data) {
      let values = data.split("/");

      dhcp_snooping_supported = values[0];

      // Initial value
      initialValue["relay_mode"] = parseInt(values[1]);
      initialValue["relay_server"] = values[2];
      initialValue["relay_info_mode"] = parseInt(values[3]);
      initialValue["relay_info_policy"] = parseInt(values[4]);

      tableBody.push(this.addTableBody(values));
    }

    return (
      <Segment.Group>
        <SegmentHeader>DHCP Relay Configuration</SegmentHeader>
        <Segment loading={this.state.loading}>
          <Form onSubmit={this.onSubmit} initialValues={initialValue} subscription={{ pristine: true }}>
            {({ handleSubmit, pristine, form, submitting }) => (
              <React.Fragment>
                <Table celled compact definition collapsing>
                  {tableBody}
                </Table>

                <Divider />

                <BtnWrapper>
                  <SemanticButton
                    title="reset"
                    type="button"
                    disabled={pristine || submitting}
                    onClick={form.reset}
                  />
                  <SemanticButton
                    title="save"
                    type="submit"
                    disabled={submitting || readOnly}
                    onClick={handleSubmit}
                  />
                </BtnWrapper>
              </React.Fragment>
            )}
          </Form>
        </Segment>
      </Segment.Group>
    );
  }
}

function TextCellAlignCenter({ children }) {
  return <Table.Cell textAlign="center">{children}</Table.Cell>;
}

export default WithData(DhcpRelay, "/config/dhcp_relay", false);
