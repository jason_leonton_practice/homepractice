import React, { Component } from "react";
import { Form, Field } from "react-final-form";
import { toast } from "react-toastify";
import qs from "querystring";
import { Segment } from "semantic-ui-react";

import WithData from "../../utils/withData";
import { MCTextInput, MCDropdown } from "../../utils/master_control";
import { InfoMsg, msgConfig } from "../../components/messages";
import { genOptions } from "../../utils/dyform";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { SelectInput, TextInput } from "../../components/input-field";
import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { isInt } from "../../utils/validate";

const validate = value => {
  if (value < 1 || value > 65535) return "Value must between 1 and 65535";
  if (!isInt(value)) return "Value must be an integer";
};

const DropdownFeatures = { compact: true, selection: true };

class LACPPortConfig extends Component {
  constructor() {
    super();
    this.selectInputOpt = genOptions({ value: [0, 1], text: ["Slow", "Fast"] });
    this.portCount = [];
  }

  onSubmit = values => {
    let payload = qs.stringify(values);

    fetch("/config/lacp_ports", {
      method: "POST",
      body: payload,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    })
      .then(res => {
        if (res.status === 200) {
          this.props.refresh();
          toast.success(InfoMsg("success", null, res.status, res.statusText), msgConfig());
        }
      })
      .catch(error => {
        toast.error(InfoMsg("error", null), msgConfig());
      });
  };

  setLACPPortInit = () => {
    let obj = {};

    // port configuration init
    const conf = this.props.data.split("#");
    const portConfig = conf[0].split("|");
    const systemConfig = conf[1];

    portConfig.forEach(value => {
      if (!value) return false;

      let pval = value.split("/");
      let portno = parseInt(pval[0]);
      let timeout = parseInt(pval[2]);
      let timeoutName = `timeout_${portno}`;
      let prio = parseInt(pval[3]);
      let prioName = `prio_${portno}`;
      let updateObj = { [timeoutName]: timeout, [prioName]: prio };

      // update total port count
      this.portCount[portno] = portno;

      obj = { ...updateObj, ...obj };
    });

    // Master control initial Values
    let mcInitVals = { master_control_timeout: -1 };

    // system configuration init
    if (systemConfig) obj = { system_priority: systemConfig, ...obj, ...mcInitVals };

    return obj;
  };

  render() {
    const { data, readOnly } = this.props;
    const conf = data.split("#");
    const portConfig = conf[0].split("|");
    const lacpPortInitialValues = this.setLACPPortInit();
    const masterTextInput = new MCTextInput("prio", this.portCount);
    const masterDropdown = new MCDropdown(
      "timeout",
      this.portCount,
      [0, 1],
      [-1, 0, 1],
      ["<>", "Slow", "Fast"],
      DropdownFeatures
    );

    return (
      <Form
        onSubmit={this.onSubmit}
        initialValues={lacpPortInitialValues}
        subscription={{
          submitting: true,
          pristine: true,
          submitSucceeded: true
        }}
      >
        {({ form, submitting, pristine, values, handleSubmit, submitError, submitSucceeded }) => {
          const node = portConfig.map(value => {
            if (!value) return false;

            let pval = value.split("/");
            let portno = parseInt(pval[0]);
            let enable = parseInt(pval[1]);

            return (
              <Table.Row key={portno} textAlign="center">
                <Table.Cell>{portno}</Table.Cell>
                <Table.Cell>{enable ? "Yes" : "No"}</Table.Cell>
                <Table.Cell>
                  <Field
                    name={`timeout_${portno}`}
                    component={attr => SelectInput(attr, this.selectInputOpt, DropdownFeatures)}
                  />
                </Table.Cell>
                <Table.Cell>
                  <Field name={`prio_${portno}`} component={TextInput} validate={validate} />
                </Table.Cell>
              </Table.Row>
            );
          });

          return (
            <Segment.Group>
              <SegmentHeader>LACP System & Port Configuration</SegmentHeader>
              <Segment>
                {/* create LACP system configuratoin */}
                <Table celled structured compact>
                  <Table.Header>
                    <Table.Row textAlign="center">
                      <Table.HeaderCell colSpan="2">LACP System Configuration</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    <Table.Row textAlign="center">
                      <Table.Cell>System Prioity</Table.Cell>
                      <Table.Cell>
                        <Field name="system_priority" component={TextInput} validate={validate} />
                      </Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>

                {/* master control for LACP port configuration*/}
                {masterTextInput.construct()}
                {masterDropdown.construct()}

                {/* create LACP port configuration */}
                <Table celled structured striped compact>
                  <Table.Header>
                    <Table.Row textAlign="center">
                      <Table.HeaderCell colSpan="4">LACP Port Configuration</Table.HeaderCell>
                    </Table.Row>
                    <Table.Row textAlign="center">
                      <Table.HeaderCell>Port</Table.HeaderCell>
                      <Table.HeaderCell>LACP</Table.HeaderCell>
                      <Table.HeaderCell>Timeout</Table.HeaderCell>
                      <Table.HeaderCell>Prio</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>

                  <Table.Body>
                    <Table.Row textAlign="center">
                      <Table.Cell>*</Table.Cell>
                      <Table.Cell></Table.Cell>
                      <Table.Cell>{masterDropdown.genComponents()}</Table.Cell>
                      <Table.Cell>
                        <Field name="master_control_prio" component={TextInput} />
                      </Table.Cell>
                    </Table.Row>
                    {node}
                  </Table.Body>
                </Table>

                <BtnWrapper
                  children={
                    <React.Fragment>
                      <SemanticButton
                        title="reset"
                        type="button"
                        disabled={pristine || submitting}
                        onClick={form.reset}
                      />
                      <SemanticButton
                        type="submit"
                        title="save"
                        disabled={submitting || readOnly}
                        onClick={handleSubmit}
                      />
                    </React.Fragment>
                  }
                />
              </Segment>
            </Segment.Group>
          );
        }}
      </Form>
    );
  }
}

export default WithData(LACPPortConfig, "/config/lacp_ports", false);
