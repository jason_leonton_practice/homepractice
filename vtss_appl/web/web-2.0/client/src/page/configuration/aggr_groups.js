import React, { Component } from "react";
import { Form, Field, FormSpy } from "react-final-form";
import { OnChange } from "react-final-form-listeners";
import { Segment } from "semantic-ui-react";
import { toast } from "react-toastify";
import qs from "querystring";

import { InfoMsg, msgConfig } from "../../components/messages";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { SegmentHeader, InnerTable as Table } from "../../components/styled_component";
import { SelectInput, CustomCheckbox, CustomRadio, TextInput } from "../../components/input-field";
import WithData from "../../utils/withData";

let aggrconfig = [];
let gGroup;
let glagCount = [];
// 設定Dropdown寬度為137px，使所有的option不會因為寬度不夠而變成兩行
const dropdownFeatures = { selection: true, fluid: true, style: { minWidth: "137px" } };
const modeOptions = [
  { key: 0, value: 0, text: "Disabled" },
  { key: 2, value: 2, text: "Static" },
  { key: 3, value: 3, text: "LACP (Active)" },
  { key: 4, value: 4, text: "LACP (Passive)" }
];
const reject = () => new Promise(resolve => setTimeout(resolve, 1));

class AggrGroups extends Component {
  state = { loading: false };

  onSubmit = async values => {
    if (!this.validate(values)) return reject();

    this.setState({ loading: true });
    let payload = qs.stringify(values);

    const res = await fetch("/config/aggr_groups", {
      method: "POST",
      body: payload,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    });
    const [status, statusText, body] = await Promise.all([res.status, res.statusText, res.text()]);

    if (status === 200) {
      toast.success(InfoMsg("success", null, res.status, res.statusText), msgConfig());
      this.props.refresh(true);
    } else {
      toast.error(InfoMsg("error", body, status, statusText), msgConfig());
    }

    this.setState({ loading: false });
  };

  validate = values => {
    let no_of_ports = 16;
    let count = [];

    // eslint-disable-next-line no-undef
    if (configNormalPortMax < no_of_ports) {
      // eslint-disable-next-line no-undef
      no_of_ports = configNormalPortMax;
    }

    let checkbox_group_value = [];
    // eslint-disable-next-line no-undef
    for (let i = 0; i < configNormalPortMax; i++) {
      let test = `aggr_port_${i}`;
      let selectGroupID = values[test].slice(5, 6);
      checkbox_group_value[i] = parseInt(selectGroupID);
    }

    // 計數相同的值，並記錄在object中
    // 每一行有幾個radio checkbox are checked
    let group_counts = {};
    checkbox_group_value.forEach(function(x) {
      group_counts[x] = (group_counts[x] || 0) + 1;
    });

    for (let group = 1; group < gGroup; group++) {
      // 有其他的validation皆與SPOM，目前沒有此功能，因此刪去沒有寫

      // eslint-disable-next-line no-undef
      if (group_counts[group] === 1 || count[group] > 16) {
        toast.error(
          InfoMsg(
            "error",
            `<b>Group ${group} member</b> counts error!! Local aggregation must include <b>2-${no_of_ports} ports</b>`
          ),
          msgConfig(false, "top-right")
        );

        return false;
      }
    }

    return true;
  };

  addRow = (group, pval, initialValues) => {
    // eslint-disable-next-line no-undef
    let port_count = configNormalPortMax;
    let checkbox_cells = [];

    for (let port = 0; port < port_count; port++) {
      // Checkbox Initial value
      if (parseInt(pval[port]) === 1) {
        initialValues[`aggr_port_${port}`] = `aggr_${group}_port_${port}`;
      }

      checkbox_cells.push(
        <TextCellAlignCenter key={`aggr_${group}_port_${port}`}>
          <Field
            name={`aggr_port_${port}`}
            type="radio"
            value={`aggr_${group}_port_${port}`}
            component={props => <CustomRadio fieldProps={props} disabled={parseInt(pval[port]) === 2} />}
          />
        </TextCellAlignCenter>
      );
    }

    let config_cells = [];
    if (group === 0) {
      config_cells.push(
        <React.Fragment key={`group_conf_${group}`}>
          <TextCellAlignCenter></TextCellAlignCenter>
          <TextCellAlignCenter></TextCellAlignCenter>
          <TextCellAlignCenter></TextCellAlignCenter>
        </React.Fragment>
      );
    } else {
      let modeVal = parseInt(pval[port_count]);
      initialValues[`groupmode_${group}`] = modeVal;

      let revertVal = parseInt(pval[port_count + 1]);
      initialValues[`revertive_${group}`] = revertVal;

      let maxbundleVal = parseInt(pval[port_count + 2]);
      initialValues[`maxbundle_${group}`] = maxbundleVal;

      config_cells.push(
        <React.Fragment key={`group_conf_${group}`}>
          {/* 利用FormSpy監聽values的改變，當Mode Options為Disabled or Static時，將Revertive & Max Bundle disabled */}
          <FormSpy subscription={{ values: true }}>
            {({ values }) => (
              <React.Fragment>
                <TextCellAlignCenter>
                  <Field
                    name={`groupmode_${group}`}
                    component={props => SelectInput(props, modeOptions, dropdownFeatures)}
                  />
                </TextCellAlignCenter>
                <TextCellAlignCenter>
                  <Field
                    name={`revertive_${group}`}
                    component={props => (
                      <CustomCheckbox fieldProps={props} disabled={values[`groupmode_${group}`] < 3} />
                    )}
                  />
                </TextCellAlignCenter>
                <TextCellAlignCenter>
                  {/* 監聽事件: 當maxbundle input 值小於1時，會強制將值改為1 */}
                  <WhenMaxbundleLowerThanOne field={`maxbundle_${group}`} to={1} />
                  <Field
                    name={`maxbundle_${group}`}
                    render={props =>
                      TextInput(props, {
                        maxLength: 5,
                        fluid: true,
                        disabled: values[`groupmode_${group}`] < 3
                      })
                    }
                  />
                </TextCellAlignCenter>
              </React.Fragment>
            )}
          </FormSpy>
        </React.Fragment>
      );
    }

    return (
      <Table.Row key={`row_${group}`}>
        <TextCellAlignCenter>{group === 0 ? "Normal" : group}</TextCellAlignCenter>
        {checkbox_cells}
        {config_cells}
      </Table.Row>
    );
  };

  genTableBody = initialValues => {
    const { data } = this.props;
    let tableBody = [];

    // Format:
    // 1/1/1/0/0/..../1/0| Group Normal
    // 1/1/1/0/0/..../1/0| Group 1
    // 1/1/1/0/0/..../1/0| Group 2
    //..................................
    // 0/1 GLGA1 member count(not include this unit)/GLAG2 member count(not include this unit)
    if (data) {
      aggrconfig = data.split("|");

      // -1 為減去第一行Group Normal
      gGroup = aggrconfig.length - 1;

      let i;
      for (i = 0; i < gGroup; i++) {
        if (aggrconfig[i]) {
          let pval = aggrconfig[i].split("/");
          tableBody.push(this.addRow(i, pval, initialValues));
        }
      }
      let pmode = aggrconfig[i].split("/");

      glagCount[1] = parseInt(pmode[0]); /* exclude the switch itself */
      glagCount[2] = parseInt(pmode[1]); /* exclude the switch itself */
    }

    return tableBody;
  };

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    const { readOnly } = this.props;
    let initialValues = {};
    let tableBody = this.genTableBody(initialValues);

    return (
      <Segment.Group>
        <SegmentHeader>Aggregation Group Configuration</SegmentHeader>
        <Segment loading={this.state.loading}>
          <Form
            onSubmit={this.onSubmit}
            initialValues={initialValues}
            // 避免不必要的re-render
            subscription={{ pristine: true, submitting: true }}
          >
            {({ handleSubmit, pristine, form, submitting }) => (
              <React.Fragment>
                <Table celled compact striped>
                  <TableHeader />
                  <Table.Body>{tableBody}</Table.Body>
                </Table>
                <BtnWrapper>
                  <SemanticButton
                    title="reset"
                    type="button"
                    disabled={pristine || submitting}
                    onClick={form.reset}
                  />
                  <SemanticButton
                    title="save"
                    type="submit"
                    disabled={submitting || readOnly}
                    onClick={handleSubmit}
                  />
                </BtnWrapper>
              </React.Fragment>
            )}
          </Form>
        </Segment>
      </Segment.Group>
    );
  }
}

// 當max bundle input值小於1時，自動改為1
const WhenMaxbundleLowerThanOne = ({ field, to }) => (
  <Field name={field} subscription={{}}>
    {({ input: { onChange } }) => (
      <FormSpy subscription={{}}>
        {({ form }) => (
          <OnChange name={field}>
            {value => {
              if (parseInt(value, 10) < 1) {
                onChange(to);
              }
            }}
          </OnChange>
        )}
      </FormSpy>
    )}
  </Field>
);

function TableHeader() {
  const genPortnoCell = () => {
    let cell = [];
    // eslint-disable-next-line no-undef
    for (let i = configPortMin; i <= configNormalPortMax; i++) {
      cell.push(<Table.HeaderCell key={`table_header_port_${i}`}>{i}</Table.HeaderCell>);
    }

    return cell;
  };

  return (
    <Table.Header>
      <Table.Row textAlign="center">
        <Table.HeaderCell></Table.HeaderCell>
        {/* eslint-disable-next-line no-undef */}
        <Table.HeaderCell colSpan={configNormalPortMax}>Port Members</Table.HeaderCell>
        <Table.HeaderCell colSpan="3">Group Configuration</Table.HeaderCell>
      </Table.Row>
      <Table.Row textAlign="center">
        <Table.HeaderCell>Group ID</Table.HeaderCell>
        {genPortnoCell()}
        <Table.HeaderCell>Mode</Table.HeaderCell>
        <Table.HeaderCell>Revertive</Table.HeaderCell>
        <Table.HeaderCell>Max Bundle</Table.HeaderCell>
      </Table.Row>
    </Table.Header>
  );
}

function TextCellAlignCenter({ children }) {
  return <Table.Cell textAlign="center">{children}</Table.Cell>;
}

export default WithData(AggrGroups, "/config/aggr_groups", false);
