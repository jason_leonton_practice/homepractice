import React, { useState } from "react";
import { Form } from "semantic-ui-react";
import { Field } from "react-final-form";

import { CustomInput } from "../../../components/input-field";
import { genArrayInt, genOptions } from "../../../utils/dyform";
import { SemanticButton, BtnWrapper } from "../../../components/semantic-component";
import { CustomSelectInput } from "../../../components/input-field";

// eslint-disable-next-line no-undef
const [usernameNaxLen, passwordMaxLen] = [configUsernameMaxLen, configPasswordMaxLen];

function EditUser(props) {
  const { form, handleSubmit, submitting, readOnly, pristine, isReservedUsername } = props;
  const [upadatePassword, setUpdatePassword] = useState(0);
  const handleUpdatePassword = (e, { value }) => setUpdatePassword(value);

  return (
    <Form size="mini">
      <Field name="username">
        {props => (
          <CustomInput
            fieldProps={props}
            label="Username (Read Only)"
            features={{ readOnly: true, placeholder: "Read Only", maxLength: usernameNaxLen }}
          />
        )}
      </Field>
      <Form.Select
        label="Upadate Password"
        onChange={handleUpdatePassword}
        value={upadatePassword}
        options={[
          { key: 0, text: "No", value: 0 },
          { key: 1, text: "Yes", value: 1 }
        ]}
      />
      {!upadatePassword ? null : (
        <Form.Group widths="equal">
          <Field name="password1">
            {props => (
              <CustomInput
                fieldProps={props}
                label="New Password"
                required={true}
                features={{ placeholder: "password", type: "password", maxLength: passwordMaxLen }}
              />
            )}
          </Field>
          <Field name="password2">
            {props => (
              <CustomInput
                fieldProps={props}
                label="Confirm New Password"
                required={true}
                features={{ placeholder: "password", type: "password", maxLength: passwordMaxLen }}
              />
            )}
          </Field>
        </Form.Group>
      )}

      <Field name="priv_level">
        {props => (
          <CustomSelectInput
            fieldProps={props}
            label="Privilege Level"
            required={true}
            disabled={isReservedUsername}
            options={genOptions({ value: genArrayInt(0, 15), text: genArrayInt(0, 15) })}
          />
        )}
      </Field>

      <BtnWrapper
        children={
          <React.Fragment>
            <SemanticButton title="cancel" disabled={readOnly} onClick={props.handleCancel} />
            {!isReservedUsername ? (
              <SemanticButton title="delete user" disabled={readOnly} onClick={props.handleDelete} />
            ) : null}
            <SemanticButton
              title="reset"
              type="button"
              disabled={pristine || submitting}
              onClick={form.reset}
            />
            <SemanticButton
              type="submit"
              title="save"
              disabled={submitting || readOnly}
              onClick={handleSubmit}
            />
          </React.Fragment>
        }
      />
    </Form>
  );
}

export default EditUser;
