import React from "react";
import { Form } from "semantic-ui-react";
import { Field } from "react-final-form";

import { CustomInput } from "../../../components/input-field";
import { genArrayInt, genOptions } from "../../../utils/dyform";
import { SemanticButton, BtnWrapper } from "../../../components/semantic-component";
import { CustomSelectInput } from "../../../components/input-field";

// eslint-disable-next-line no-undef
const [usernameNaxLen, passwordMaxLen] = [configUsernameMaxLen, configPasswordMaxLen];

function AddNewUser(props) {
  const { form, handleSubmit, submitting, pristine, readOnly } = props;
  return (
    <Form size="mini">
      <Field name="username">
        {props => (
          <CustomInput
            fieldProps={props}
            label="Username"
            required={true}
            features={{ placeholder: "username", maxLength: usernameNaxLen }}
          />
        )}
      </Field>
      <Form.Group widths="equal">
        <Field name="password1">
          {props => (
            <CustomInput
              fieldProps={props}
              label="Password"
              required={true}
              features={{ placeholder: "password", type: "password", maxLength: passwordMaxLen }}
            />
          )}
        </Field>
        <Field name="password2">
          {props => (
            <CustomInput
              fieldProps={props}
              label="Confirm Password"
              required={true}
              features={{ placeholder: "password", type: "password", maxLength: passwordMaxLen }}
            />
          )}
        </Field>
      </Form.Group>

      <Field name="priv_level">
        {props => (
          <CustomSelectInput
            fieldProps={props}
            label="Privilege Level"
            required={true}
            options={genOptions({ value: genArrayInt(0, 15), text: genArrayInt(0, 15) })}
          />
        )}
      </Field>

      <BtnWrapper>
        <SemanticButton title="cancel" disabled={readOnly} onClick={props.handleCancel} />
        <SemanticButton title="reset" type="button" disabled={pristine || submitting} onClick={form.reset} />
        <SemanticButton type="submit" title="save" disabled={submitting || readOnly} onClick={handleSubmit} />
      </BtnWrapper>
    </Form>
  );
}

export default AddNewUser;
