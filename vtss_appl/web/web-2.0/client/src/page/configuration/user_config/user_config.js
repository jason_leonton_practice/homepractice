import React, { Component } from "react";
import { Segment } from "semantic-ui-react";
import { Form as FinalForm } from "react-final-form";
import { toast } from "react-toastify";
import qs from "querystring";

import AddNewUser from "./add_new_user";
import EditUser from "./edit_user";
import { SegmentHeader } from "../../../components/styled_component";
import { InfoMsg, msgConfig } from "../../../components/messages";
import withData from "../../../utils/withData";

// eslint-disable-next-line no-undef
const reservedUserName = configDefaultUserNameNullStr ? "" : "admin";

class UserConfig extends Component {
  state = {
    initVal: "",
    password_update: 0,
    selectedUser: -1,
    isReservedUsername: false
  };

  validate = values => {
    let errors = {};

    // eslint-disable-next-line no-undef
    if (values["username"] || (configDefaultUserNameNullStr && values["username"] === 0)) {
      if (values["username"].toLowerCase() === reservedUserName) {
        if (this.state.selectedUser === -1) {
          errors["username"] = "Blank username is not allowed.";
        } else if (this.state.initVal.username !== reservedUserName) {
          errors["username"] = `Username ${reservedUserName} is not allowed.`;
        }

        if (values["priv_level"] < this.state.miscPrivLevel) {
          errors[
            "priv_level"
          ] = `Warning! The privilege level of group 'Maintenance' is ${this.state.miscPrivLevel}.
            Change to lower privilege level will lock you out.`;
        }
      }

      const illegalChars = /\W/; // allow only letters, numbers, and underscores
      if (illegalChars.test(values["username"])) {
        errors[
          "username"
        ] = `The username contains illegal characters. Please use <strong>letters</strong>, <strong>numbers</strong> and <strong>underscores</strong>`;
      }
    } else {
      errors["username"] = "Please specify a username";
    }

    if (values["password1"] !== values["password2"]) {
      errors["password2"] = "Password do not match";
    }

    return errors;
  };

  postRequest = body => {
    fetch("/config/user_config", {
      method: "post",
      body,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    })
      .then(res => {
        if (res.status === 200) {
          this.props.history.push("/configuration/security/switch/users");
        } else {
          toast.error(InfoMsg("error", null, res.status, res.statusText), msgConfig());
        }
      })
      .catch(e => {
        toast.error(InfoMsg("error", null), msgConfig());
      });
  };

  onSubmit = values => {
    this.postRequest(qs.stringify(values));
  };

  deleteUser = () => {
    if (this.state.selectedUser >= 0 && window.confirm("Delete User?")) {
      const body = qs.stringify({ username: this.state.initVal.username, delete: 1 });

      this.postRequest(body);
    }
  };

  handleCancel = () => this.props.history.push("/configuration/security/switch/users");

  initialize = () => {
    if (this.props.data) {
      const { data } = this.props;
      const userData = data.split(",");

      if (userData[1] === reservedUserName) this.setState({ isReservedUsername: true });

      this.setState({
        selectedUser: parseInt(userData[0]),
        miscPrivLevel: parseInt(userData[3]),
        initVal: { username: userData[1], priv_level: parseInt(userData[2]) }
      });
    }
  };

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps) !== JSON.stringify(this.props)) {
      this.initialize();
    }
  }

  componentDidMount() {
    this.initialize();
  }

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  render() {
    const { selectedUser } = this.state;
    const { readOnly } = this.props;
    let content;

    content = (
      <FinalForm
        onSubmit={this.onSubmit}
        initialValues={this.state.initVal}
        subscription={{ submitting: true, submitSucceeded: true, pristine: true }}
        validate={this.validate}
      >
        {({ form, submitting, pristine, handleSubmit, submitSucceeded, submitError }) => {
          return (
            <React.Fragment>
              {selectedUser === -1 ? (
                <AddNewUser
                  form={form}
                  handleSubmit={handleSubmit}
                  submitting={submitting}
                  pristine={pristine}
                  readOnly={readOnly}
                  handleCancel={this.handleCancel}
                />
              ) : (
                <EditUser
                  form={form}
                  handleSubmit={handleSubmit}
                  submitting={submitting}
                  pristine={pristine}
                  readOnly={readOnly}
                  isReservedUsername={this.state.isReservedUsername}
                  handleCancel={this.handleCancel}
                  handleDelete={this.deleteUser}
                />
              )}
            </React.Fragment>
          );
        }}
      </FinalForm>
    );

    return (
      <Segment.Group>
        <SegmentHeader>{this.state.selectedUser === -1 ? "Add New User" : "Edit User"}</SegmentHeader>
        <Segment>{content}</Segment>
      </Segment.Group>
    );
  }
}

export default withData(UserConfig, `/config/user_config`, false, ["user"]);
