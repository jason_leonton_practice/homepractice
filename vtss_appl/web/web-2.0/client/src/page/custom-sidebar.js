import React from "react";
import { Checkbox, Icon, Segment, Message } from "semantic-ui-react";

import { SegmentHeader } from "../components/styled_component";
import { SemanticButton, BtnWrapper } from "../components/semantic-component";
import menuData from "../utils/modules_tree";
import "./custom-sidebar.scss";

class CustomSidebar extends React.Component {
  state = {
    itemsCheck: new Map(this.createCheckboxMap(menuData))
  };

  // this function create an array
  // store each checkbox checked, disabled, children element
  createCheckboxMap(menuData) {
    let array = [];

    menuData.map(function m(item) {
      array.push([item.id, { checked: true, disabled: false, children: item.children }]);

      if (item.children) item.children.map(m);

      return array;
    });

    return array;
  }

  handleCheckbox = (e: React.FormEvent<HTMLInputElement>, data) => {
    const id = data.name;
    const isChecked = !this.state.itemsCheck.get(id).checked;
    const currentIdObj = this.state.itemsCheck.get(id);
    const { disabled, children } = currentIdObj;

    this.setState(
      prevState => ({
        itemsCheck: prevState.itemsCheck.set(id, {
          checked: isChecked,
          disabled,
          children
        })
      }),
      () => {
        // control disabled status on children checkbox
        // 1. 當NTP不勾選時，test會便為disabled，但是system不勾選在勾選回來時，test便為able了
        if (children) {
          let toggleChildren = [];

          children.map(function m(child) {
            if (child.children) child.children.map(m);
            toggleChildren.push(child.id);

            return toggleChildren;
          });

          if (!isChecked) {
            // toggleChildren is an array contain the checkbox's id that need to be disabled
            // 除了children之外，孫子原本為disable則持續保持
            toggleChildren.map(childID => {
              const childObj = this.state.itemsCheck.get(childID);

              this.setState(prevState => ({
                itemsCheck: prevState.itemsCheck.set(childID, {
                  checked: childObj.checked,
                  disabled: true,
                  children: childObj.children
                })
              }));
            });
          } else if (isChecked) {
            toggleChildren.map(childID => {
              const childObj = this.state.itemsCheck.get(childID);

              this.setState(prevState => ({
                itemsCheck: prevState.itemsCheck.set(childID, {
                  checked: childObj.checked,
                  disabled: false,
                  children: childObj.children
                })
              }));
            });
          }
        }
      }
    );
  };

  handleSubmit = e => {
    e.preventDefault();

    const { itemsCheck } = this.state;
    let filtered = JSON.parse(JSON.stringify(menuData));

    // value => boolean, key => id
    itemsCheck.forEach((value, key) => {
      if (!value.checked) {
        filtered = filtered.filter(function f(item) {
          if (item.id === key) {
            return item.id !== key;
          } else if (item.children) {
            return (item.children = item.children.filter(f));
          }

          return item.id !== key;
        });

        return filtered;
      }
    });

    this.props.customSidebar(filtered);
  };

  generateMenu = children => {
    return children.map(({ children, name, id, level }) => {
      if (name === "Dashboard" || name === "Custom Sidebar" || name === "Quick Setup") return false;

      return level === "level-1" ? (
        <Segment key={name}>
          <div className="level-1-item">
            <h3>{name}</h3>
          </div>
          <div className={`${level}-wrapper`}>{children ? this.generateMenu(children) : null}</div>
        </Segment>
      ) : (
        <div key={name} className={`${level}-item`}>
          <Checkbox
            onChange={this.handleCheckbox}
            label={name}
            name={id ? id : null}
            className={`${level}-checkbox`}
            disabled={this.state.itemsCheck.get(id).disabled}
            defaultChecked
          />
          {children ? this.generateMenu(children) : null}
        </div>
      );
    });
  };

  render() {
    return (
      <Segment.Group>
        <SegmentHeader>Custom Sidebar Menu Configuration</SegmentHeader>
        <Segment>
          <Message info icon>
            <Icon name="edit" />
            <Message.Content>
              <Message.Header>Customize yoru sidbar menu items</Message.Header>
              <Message.List>You can hide unused or rarely use menu itmes</Message.List>
            </Message.Content>
          </Message>
          <form>
            {this.generateMenu(menuData)}
            <BtnWrapper>
              <SemanticButton title="reset" />
              <SemanticButton title="save" onClick={this.handleSubmit} />
            </BtnWrapper>
          </form>
        </Segment>
      </Segment.Group>
    );
  }
}

export default CustomSidebar;
