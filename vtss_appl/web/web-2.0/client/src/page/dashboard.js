import React from "react";
import "./dashboard.scss";
import MonitorSys from "./monitor/sys";
import PerfCpuload from "./monitor/perf_cpuload";

class Dashboard extends React.Component {
  render() {
    return (
      <div>
        <div className="middle-section">
          <div className="device-info-section">
            <MonitorSys />
          </div>
          <div className="cpu-load-section">
            <PerfCpuload />
          </div>
        </div>
      </div>
    );
  }
}

export default Dashboard;
