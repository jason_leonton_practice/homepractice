import React, { Component } from "react";
import { Segment, Message, Form } from "semantic-ui-react";
import { Form as FinalForm, Field } from "react-final-form";
import qs from "querystring";
import { toast } from "react-toastify";

import { SegmentHeader } from "../../components/styled_component";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import WithData from "../../utils/withData";
import { CustomInput, CustomCheckbox } from "../../components/input-field";
import { isValidDomainOrIP, isWithinRange } from "../../utils/validate";
import { InfoMsg, msgConfig } from "../../components/messages";

const isAutoRefresh = false;

class Traceroute4 extends Component {
  state = { isAutoRefresh, loading: false };

  validate = values => {
    let errors = {};

    // Check ip_addr must be valid unicast address, excluding 0.0.0.0
    const check_ip_addr = isValidDomainOrIP(
      values["ip_addr"],
      "Hostname or IP Address",
      false,
      4,
      4,
      null,
      // eslint-disable-next-line no-undef
      configIPDNSSupport,
      true
    );
    if (check_ip_addr["error"]) {
      errors["ip_addr"] = check_ip_addr["msg"];
    }

    // Check source VLAN and address
    if (
      values["src_vid"] &&
      values["src_vid"].length > 0 &&
      values["src_addr"] &&
      values["src_addr"].length > 0
    ) {
      errors["src_vid"] =
        "You may only specify either the VID or the Address for the Source Interface - not both";
      errors["src_addr"] =
        "You may only specify either the VID or the Address for the Source Interface - not both";
    }

    // Check source VLAN value
    if (values["src_vid"] && values["src_vid"].length > 0) {
      // eslint-disable-next-line no-undef
      const check_src_vid = isWithinRange(values["src_vid"], configVidMin, configVidMax, "'source VLAN'", "");
      if (check_src_vid["error"]) {
        errors["src_vid"] = check_src_vid["msg"];
      }
    }

    // Check source src_addr be valid unicast address, excluding 0.0.0.0

    const check_src_addr = isValidDomainOrIP(
      values["src_addr"],
      "'Source IP Address'",
      false,
      4,
      4,
      null,
      false,
      false
    );
    if (check_src_addr["error"]) {
      errors["src_addr"] = check_src_addr["msg"];
    }

    // Check DSCP
    const check_dscp = isWithinRange(
      values["dscp"],
      // eslint-disable-next-line no-undef
      configTracerouteDscpMin,
      // eslint-disable-next-line no-undef
      configTracerouteDscpMax,
      "'DSCP Value'",
      ""
    );
    if (check_dscp["error"]) {
      errors["dscp"] = check_dscp["msg"];
    }

    // Check Response Timeout
    const check_timeout = isWithinRange(
      values["timeout"],
      // eslint-disable-next-line no-undef
      configTracerouteTimeoutMin,
      // eslint-disable-next-line no-undef
      configTracerouteTimeoutMax,
      "'Response Timeout'",
      ""
    );
    if (check_timeout["error"]) {
      errors["timeout"] = check_timeout["msg"];
    }

    // Check Probes
    const check_probes = isWithinRange(
      values["probes"],
      // eslint-disable-next-line no-undef
      configTracerouteProbesMin,
      // eslint-disable-next-line no-undef
      configTracerouteProbesMax,
      "'Probes Per Hop'",
      ""
    );
    if (check_probes["error"]) {
      errors["probes"] = check_probes["msg"];
    }

    // Check First TTL
    const check_firstttl = isWithinRange(
      values["firstttl"],
      // eslint-disable-next-line no-undef
      configTracerouteFttlMin,
      // eslint-disable-next-line no-undef
      configTracerouteFttlMax,
      "'First TTL'",
      ""
    );
    if (check_firstttl["error"]) {
      errors["firstttl"] = check_firstttl["msg"];
    }

    // Check Max TTL
    const maxttl = isWithinRange(
      values["maxttl"],
      // eslint-disable-next-line no-undef
      configTracerouteMttlMin,
      // eslint-disable-next-line no-undef
      configTracerouteMttlMax,
      "'Max TTL'",
      ""
    );
    if (maxttl["error"]) {
      errors["maxttl"] = maxttl["msg"];
    }

    // Check that first TTL is lower than or equal to Max TTL
    if (parseInt(values["firstttl"], 10) > parseInt(values["maxttl"], 10)) {
      errors["firstttl"] = "First TTL' must be lower than or equal to 'Max TTL'";
    }

    return errors;
  };

  onSubmit = async values => {
    this.setState({ loading: true });

    const payload = qs.stringify(values);
    const res = await fetch("/config/traceroute4", {
      method: "POST",
      body: payload,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    });
    const [status, statusText, body] = await Promise.all([res.status, res.statusText, res.text()]);

    if (status === 200) {
      return this.props.history.push(`/diagnostics/traceroute4_result?ioIndex=${body}`);
    } else {
      toast.error(InfoMsg("error", body, status, statusText), msgConfig());
    }

    this.setState({ loading: false });
  };

  render() {
    const { readOnly } = this.props;
    const initialValues = { dscp: "0", probes: "3", timeout: "3", firstttl: "1", maxttl: "30" };

    return (
      <Segment.Group>
        <SegmentHeader>Traceroute (IPv4)</SegmentHeader>
        <Segment loading={this.state.loading}>
          <Message warning>
            Fill in the parameters as needed and press "Start" to initiate the Traceroute session.
          </Message>
          <FinalForm
            onSubmit={this.onSubmit}
            initialValues={initialValues}
            validate={this.validate}
            subscription={{
              submitting: true,
              submitSucceeded: true,
              pristine: true
            }}
          >
            {({ form, values, pristine, handleSubmit, submitting, submitSucceeded, submitError }) => (
              <Form size="tiny">
                <Field name="ip_addr">
                  {props => <CustomInput fieldProps={props} label="Hostname or IP Address" />}
                </Field>

                <Field name="dscp">{props => <CustomInput fieldProps={props} label="DSCP Value" />}</Field>

                <Field name="probes">
                  {props => <CustomInput fieldProps={props} label="Number of Probes Per Hop (packets)" />}
                </Field>

                <Field name="timeout">
                  {props => <CustomInput fieldProps={props} label="Response Timeout (seconds)" />}
                </Field>

                <Form.Group widths="equal">
                  <Field name="firstttl">
                    {props => <CustomInput fieldProps={props} label="First TTL Value" />}
                  </Field>
                  <Field name="maxttl">
                    {props => <CustomInput fieldProps={props} label="Max TTL Value" />}
                  </Field>
                </Form.Group>

                <Form.Group widths="equal">
                  <Field name="src_vid">
                    {props => <CustomInput fieldProps={props} label="VID for Source Interface" />}
                  </Field>
                  <Field name="src_addr">
                    {props => <CustomInput fieldProps={props} label="IP Address for Source Interface" />}
                  </Field>
                </Form.Group>

                <Form.Group>
                  <Form.Field>
                    <Field name="icmp">
                      {props => <CustomCheckbox fieldProps={props} label="Use ICMP instead of UDP" />}
                    </Field>
                  </Form.Field>
                  <Form.Field>
                    <Field name="numeric">
                      {props => <CustomCheckbox fieldProps={props} label="Print Numeric Addresses" />}
                    </Field>
                  </Form.Field>
                </Form.Group>

                <BtnWrapper>
                  <SemanticButton
                    title="reset"
                    type="button"
                    disabled={pristine || submitting}
                    onClick={form.reset}
                  />
                  <SemanticButton
                    type="submit"
                    title="Start"
                    disabled={submitting || readOnly}
                    onClick={handleSubmit}
                  />
                </BtnWrapper>
              </Form>
            )}
          </FinalForm>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(Traceroute4, "/config/traceroute6", false);
