import React, { Component } from "react";
import { Segment, Divider } from "semantic-ui-react";

import { SemanticButton as Button, BtnWrapper } from "../../components/semantic-component";
import WithData from "../../utils/withData";

class Ping4Result extends Component {
  constructor(props) {
    super(props);
    this.content = "";
  }

  componentDidUpdate() {
    const { data } = this.props;

    if (data.match(/Ping session complete/)) clearInterval(this.timerID);
  }

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  componentDidMount() {
    const { data } = this.props;

    if (!data.match(/Ping session complete/) && !data.match(/Error:/))
      this.timerID = setInterval(() => this.props.refresh(), 500);
  }

  render() {
    this.content += this.props.data.replace(/[\r\n]/g, "<br/>");

    return (
      <Segment.Group>
        <Segment className="segment_page_title">Ping (IPv4) Output</Segment>
        <Segment>
          <div dangerouslySetInnerHTML={{ __html: this.content }} />
          <Divider />
          <BtnWrapper>
            <Button
              title="New Ping"
              disabled={this.props.readOnly}
              onClick={() => this.props.history.push("/diagnostics/ping_ipv4")}
            />
          </BtnWrapper>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(Ping4Result, "/config/ping4", false, ["ioIndex"]);
