import React, { Component } from "react";
import { Segment, Message, Form } from "semantic-ui-react";
import { Form as FinalForm, Field } from "react-final-form";
import qs from "querystring";
import { toast } from "react-toastify";

import WithData from "../../utils/withData";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { InfoMsg, msgConfig } from "../../components/messages";
import { isInt, isValidDomainOrIP, isWithinRange } from "../../utils/validate";
import { CustomCheckbox, CustomInput } from "../../components/input-field";

const validate = values => {
  let errors = {};

  const check_ip_addr = isValidDomainOrIP(
    values["ip_addr"],
    "Hostname or IP Address",
    false,
    4,
    4,
    null,
    // eslint-disable-next-line no-undef
    configIPDNSSupport,
    true
  );
  if (check_ip_addr["error"]) {
    errors["ip_addr"] = check_ip_addr["msg"];
  }

  const check_length = isWithinRange(
    values["length"],
    // eslint-disable-next-line no-undef
    configPingLenMin,
    // eslint-disable-next-line no-undef
    configPingLenMax,
    "Payload Size",
    "bytes"
  );
  if (check_length["error"]) errors["length"] = check_length["msg"];

  const check_pdata = isWithinRange(
    values["pdata"],
    // eslint-disable-next-line no-undef
    configPingPdataMin,
    // eslint-disable-next-line no-undef
    configPingPdataMax,
    "Payload Data",
    ""
  );
  if (check_pdata["error"]) errors["pdata"] = check_pdata["msg"];

  const check_count = isWithinRange(
    values["count"],
    // eslint-disable-next-line no-undef
    configPingCntMin,
    // eslint-disable-next-line no-undef
    configPingCntMax,
    "Packet Count",
    " times"
  );
  if (check_count["error"]) errors["count"] = check_count["msg"];

  // eslint-disable-next-line no-undef
  const check_ttlvalue = isWithinRange(values["ttlvalue"], configPingTtlMin, configPingPdataMax, "TTL", "");
  if (check_ttlvalue["error"]) errors["ttlvalue"] = check_ttlvalue["msg"];

  // Check portno
  if (values["src_portno"] && values["src_portno"].length > 0) {
    if (!isInt(values["src_portno"]) || parseInt(values["src_portno"]) <= 0)
      errors["src_portno"] = "Port Number must be a positive integer value";
  }

  // Check VLAN and address and portno
  if (typeof values["src_portno"] !== "undefined" && typeof values["src_addr"] !== "undefined") {
    const msg = "You may only specify either the Source Interface Address or the Port Number - not both";

    errors["src_portno"] = msg;
    errors["src_addr"] = msg;
  }

  if (
    typeof values["src_vid"] !== "undefined" &&
    (typeof values["src_addr"] !== "undefined" || typeof values["src_portno"] !== "undefined")
  ) {
    const msg = "You may only specify either the VID or the PortNo or Address for the Source Interface";

    errors["src_vid"] = msg;

    if (typeof values["src_addr"] !== "undefined") {
      errors["src_vid"] = msg;
    }
    if (typeof values["src_portno"] !== "undefined") {
      errors["src_portno"] = msg;
    }
  }

  // Check source VLAN
  if (typeof values["src_vid"] !== "undefined") {
    // eslint-disable-next-line no-undef
    const check_src_vid = isWithinRange(values["src_vid"], configVidMin, configVidMax, "Source VLAN", "");
    if (check_src_vid["error"]) errors["src_vid"] = check_src_vid["msg"];
  }

  // Check source ip_addr must be valid unicast address, excluding 0.0.0.0
  const chk_src_addr = isValidDomainOrIP(
    values["src_addr"],
    "Source IP Address",
    false,
    4,
    4,
    null,
    false,
    false,
    false
  );
  if (chk_src_addr["error"]) errors["src_addr"] = chk_src_addr["msg"];

  return errors;
};

class Ping4 extends Component {
  state = { loading: false };

  onSubmit = async values => {
    this.setState({ loading: true });

    const payload = qs.stringify(values);
    const res = await fetch("/config/ping4", {
      method: "POST",
      body: payload,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    });
    const [status, statusText, body] = await Promise.all([res.status, res.statusText, res.text()]);

    if (status === 200) {
      return this.props.history.push(`/diagnostics/ping4_result?ioIndex=${body}`);
    } else {
      toast.error(InfoMsg("error", body, status, statusText), msgConfig());
    }

    this.setState({ loading: false });
  };

  render() {
    const { readOnly } = this.props;
    const initialValues = {
      length: "56",
      pdata: "0",
      count: "5",
      ttlvalue: "64"
    };

    return (
      <Segment.Group>
        <Segment className="segment_page_title">Ping (IPv4)</Segment>
        <Segment loading={this.state.loading}>
          <Message warning>
            Fill in the parameters as needed and press "Start" to initiate the Ping session.
          </Message>
          <FinalForm
            onSubmit={this.onSubmit}
            initialValues={initialValues}
            validate={validate}
            subscription={{
              submitting: true,
              submitSucceeded: true,
              pristine: true
            }}
          >
            {({ form, values, pristine, handleSubmit, submitting, submitSucceeded, submitError }) => {
              return (
                <Form size="tiny">
                  <Field name="ip_addr">
                    {props => (
                      <CustomInput
                        fieldProps={props}
                        label="Hostname or IP Address"
                        features={{ maxLength: 253 }}
                      />
                    )}
                  </Field>

                  <Field name="length">
                    {props => (
                      <CustomInput
                        fieldProps={props}
                        label="Payload Size (bytes)"
                        features={{ maxLength: 4 }}
                      />
                    )}
                  </Field>

                  <Field name="pdata">
                    {props => (
                      <CustomInput
                        fieldProps={props}
                        label={`Payload Data Pattern (single byte value; integer or hex with prefix '0x')`}
                        features={{ maxLength: 4 }}
                      />
                    )}
                  </Field>

                  <Field name="count">
                    {props => (
                      <CustomInput
                        fieldProps={props}
                        label={`Packet Count (packets)`}
                        features={{ maxLength: 2 }}
                      />
                    )}
                  </Field>

                  <Field name="ttlvalue">
                    {props => (
                      <CustomInput fieldProps={props} label="TTL Value" features={{ maxLength: 3 }} />
                    )}
                  </Field>

                  <Form.Group widths="equal">
                    <Field name="src_vid">
                      {props => (
                        <CustomInput
                          fieldProps={props}
                          label="VID for Source Interface"
                          features={{ maxLength: 64 }}
                        />
                      )}
                    </Field>

                    <Field name="src_portno">
                      {props => (
                        <CustomInput
                          fieldProps={props}
                          label="Source Port Number"
                          features={{ maxLength: 2 }}
                        />
                      )}
                    </Field>

                    <Field name="src_addr">
                      {props => (
                        <CustomInput
                          fieldProps={props}
                          label="IP Address for Source Interface"
                          features={{ maxLength: 64 }}
                        />
                      )}
                    </Field>
                  </Form.Group>

                  <Form.Field>
                    <Field name="quiet">
                      {props => <CustomCheckbox fieldProps={props} label="Quiet (only print result)" />}
                    </Field>
                  </Form.Field>

                  <BtnWrapper>
                    <SemanticButton
                      title="reset"
                      type="button"
                      disabled={pristine || submitting}
                      onClick={form.reset}
                    />
                    <SemanticButton
                      type="submit"
                      title="save"
                      disabled={submitting || readOnly}
                      onClick={handleSubmit}
                    />
                  </BtnWrapper>
                </Form>
              );
            }}
          </FinalForm>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(Ping4, "/config/ping4", false);
