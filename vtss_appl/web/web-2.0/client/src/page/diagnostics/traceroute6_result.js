import React, { Component } from "react";
import { Segment, Divider } from "semantic-ui-react";

import { SegmentHeader } from "../../components/styled_component";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import WithData from "../../utils/withData";

class Traceroute6Result extends Component {
  content = "";

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  componentDidUpdate() {
    const { data } = this.props;

    if (data.match(/Traceroute session complete/)) clearInterval(this.timerID);
  }

  shouldComponentUpdate(prevProps, prevState) {
    return (
      JSON.stringify(this.props) !== JSON.stringify(prevProps) ||
      JSON.stringify(this.state) !== JSON.stringify(prevState)
    );
  }

  componentDidMount() {
    const { data } = this.props;

    if (!data.match(/Traceroute session complete/) && !data.match(/Error:/))
      this.timerID = setInterval(() => this.props.refresh(), 1000);
  }

  render() {
    this.content += this.props.data.replace(/[\r\n]/g, "<br/>");

    return (
      <Segment.Group>
        <SegmentHeader>Traceroute (IPv6) Output</SegmentHeader>
        <Segment>
          <div dangerouslySetInnerHTML={{ __html: this.content }} />
          <Divider />
          <BtnWrapper>
            <SemanticButton
              title="New Traceroute"
              disabled={this.props.readOnly}
              onClick={() => this.props.history.push("/diagnostics/traceroute_ipv6")}
            />
          </BtnWrapper>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(Traceroute6Result, "/config/traceroute6", false, ["ioIndex"]);
