import React, { Component } from "react";
import { Segment, Table, Dropdown, Label } from "semantic-ui-react";

import { SegmentHeader } from "../../components/styled_component";
import { SemanticButton } from "../../components/semantic-component";
import WithData from "../../utils/withData";

const isAutoRefresh = false;
let currentReq;

//    VTSS_VERIPHY_STATUS_OK      = 0,
//    VTSS_VERIPHY_STATUS_OPEN    = 1,  /* Open pair */
//    VTSS_VERIPHY_STATUS_SHORT   = 2,  /* Short pair */
//    VTSS_VERIPHY_STATUS_ABNORM  = 4,  /* Abnormal termination */
//    VTSS_VERIPHY_STATUS_SHORT_A = 8,  /* Cross-pair short to pair A */
//    VTSS_VERIPHY_STATUS_SHORT_B = 9,  /* Cross-pair short to pair B */
//    VTSS_VERIPHY_STATUS_SHORT_C = 10, /* Cross-pair short to pair C */
//    VTSS_VERIPHY_STATUS_SHORT_D = 11, /* Cross-pair short to pair D */
//    VTSS_VERIPHY_STATUS_COUPL_A = 12, /* Abnormal cross-pair coupling, pair A */
//    VTSS_VERIPHY_STATUS_COUPL_B = 13, /* Abnormal cross-pair coupling, pair B */
//    VTSS_VERIPHY_STATUS_COUPL_C = 14, /* Abnormal cross-pair coupling, pair C */
//    VTSS_VERIPHY_STATUS_COUPL_D = 15  /* Abnormal cross-pair coupling, pair D */
let VeriPHYstatus = [
  "OK",
  "Open",
  "Short",
  "--",
  "Abnormal",
  "--",
  "--",
  "--",
  "Short A",
  "Short B",
  "Short C",
  "Short D",
  "Cross A",
  "Cross B",
  "Cross C",
  "Cross D"
];

class Veriphy extends Component {
  constructor(props) {
    super(props);
    this.state = { isAutoRefresh, portSelector: 0, tableBody: [], loading: false };
    this.portSelectorPopulated = false;
    this.portSelectorOptions = [];
    this.cellStyle = { textAlign: "right" };
    this.timerID = null;
  }

  onChangeSelectInput = (e, { name, value }) => {
    this.setState({ [name]: value });
  };

  onStart = () => {
    this.doPoll(this.state.portSelector);
  };

  addRow = (port_data, i) => {
    let running = false;
    let veriPHYData = [];

    // If veriphy_status_n is 0, VeriPHY has not been run in this port, and the remaining data is invalid.
    // If veriphy_status_n is 1, VeriPHY is in progress, and the remaining data is invalid.
    // If veriphy_status_n is 2, VeriPHY has been run on this port, and the remaining data is ok.
    const cell_data = () => {
      switch (parseInt(port_data[1], 10)) {
        case 1:
          running = true;
          return (
            <Table.Cell key={`veriphy_running_${i}`} colSpan="8" textAlign="center">
              VeriPHY is running...
            </Table.Cell>
          );
        case 2:
          // VeriPHY has previously been running. Present latest data
          let latest_data = [];

          for (let idx = 0; idx < 4; idx++) {
            // Status
            let j = 2 * idx + 2;
            if (port_data[j] > 15) {
              latest_data.push(
                <Table.Cell key={`data_status_${idx}`} style={this.cellStyle}>
                  --
                </Table.Cell>
              );
            } else {
              latest_data.push(
                <Table.Cell key={`data_status_${idx}`} style={this.cellStyle}>
                  {VeriPHYstatus[port_data[j]]}
                </Table.Cell>
              );
            }

            // Length
            if (port_data[j + 1] === 255) {
              latest_data.push(
                <Table.Cell key={`data_length_${idx}`} style={this.cellStyle}>
                  --
                </Table.Cell>
              );
            } else {
              latest_data.push(
                <Table.Cell key={`data_length_${idx}`} style={this.cellStyle}>
                  {port_data[j + 1]}
                </Table.Cell>
              );
            }
          }

          return latest_data;
        default:
          // VeriPHY has not been run (or invalid veriphy_status)
          let invalid_data = [];
          for (let idx = 0; idx < 4; idx++) {
            invalid_data.push(
              <React.Fragment key={`data_ready_${i}_${idx}`}>
                <Table.Cell>--</Table.Cell>
                <Table.Cell>--</Table.Cell>
              </React.Fragment>
            );
          }

          return invalid_data;
      }
    };

    veriPHYData.push(
      <Table.Row key={`row_${i}`}>
        <Table.Cell textAlign="center">{port_data[0]}</Table.Cell>
        {cell_data()}
      </Table.Row>
    );

    return [running, veriPHYData];
  };

  startUpdate = () => {
    this.timerID = setTimeout(() => this.doPoll(-1), 2000);
  };

  pollTimeout = () => {
    try {
      if (currentReq) {
        currentReq.abort();
      }
    } catch (e) {}

    this.processUpdate(null);
    this.startUpdate();
  };

  processUpdate = responseText => {
    let veriPHYData = [];

    if (!responseText) {
      veriPHYData.push(
        <Table.Row key="empty_switch_data">
          <Table.Cell colSpan="9" textAlign="center">
            Switch is currently not responding. Please wait...
          </Table.Cell>
        </Table.Row>
      );

      this.setState({ tableBody: veriPHYData });
      return;
    }

    let all_port_data = responseText.split(";");
    let at_least_one_running = false;

    if (!this.portSelectorPopulated) {
      // Create the "All" item for the Port Selector
      this.portSelectorOptions.push({ key: 0, text: "All", value: 0 });
    }

    // Holding the status table body
    let tableBody = [];
    all_port_data.forEach((value, i) => {
      let port_data = value.split("/");

      // Add a port selector item with this port number
      if (!this.portSelectorPopulated) {
        let port = port_data[0];
        this.portSelectorOptions.push({ key: port, text: port, value: port });
      }

      // Add a status table
      tableBody.push(this.addRow(port_data, i)[1]);

      if (this.addRow(port_data, i)[0]) {
        at_least_one_running = true;
      }
    });

    this.setState({ tableBody });

    if (at_least_one_running) {
      this.startUpdate();
    }

    // Will not re-populate dropdown options again
    this.portSelectorPopulated = true;
  };

  privateLoadXMLDoc = port => {
    this.setState({ loading: true });

    let ajax = new XMLHttpRequest();
    ajax.open("get", `/config/veriphy?port=${port}`);
    ajax.onreadystatechange = () => {
      try {
        clearTimeout(this.timerID);
      } catch (e) {}
      this.timerID = null;

      try {
        if (ajax.readyState === 4) {
          if (ajax.status && ajax.status === 200) {
            this.processUpdate(ajax.responseText);
            this.setState({ loading: false });
          }

          ajax = currentReq = null;
        }
      } catch (e) {}
    };

    ajax.send();
    currentReq = ajax;
    this.timerID = setTimeout(() => this.pollTimeout(), 2000);
  };

  doPoll = port => {
    this.privateLoadXMLDoc(port);
  };

  componentDidMount() {
    this.processUpdate(this.props.data);
  }

  render() {
    return (
      <Segment.Group>
        <SegmentHeader>VeriPHY Cable Diagnostics</SegmentHeader>
        <Segment loading={this.state.loading}>
          <Table celled compact collapsing>
            <Table.Body>
              <Table.Row>
                <Table.Cell>
                  <Label ribbon>Port</Label>
                </Table.Cell>
                <Table.Cell textAlign="center">
                  <Dropdown
                    selection
                    name="portSelector"
                    value={this.state.portSelector}
                    options={this.portSelectorOptions}
                    onChange={this.onChangeSelectInput}
                    style={{ marginRight: "2em" }}
                  />
                  <SemanticButton title="Start" disabled={this.props.readOnly} onClick={this.onStart} />
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
          <Table celled compact striped>
            <Table.Header>
              <Table.Row textAlign="center">
                <Table.HeaderCell colSpan="9">Cable Status</Table.HeaderCell>
              </Table.Row>
              <Table.Row textAlign="center">
                <Table.HeaderCell>Port</Table.HeaderCell>
                <Table.HeaderCell>Pair A</Table.HeaderCell>
                <Table.HeaderCell>Length A</Table.HeaderCell>
                <Table.HeaderCell>Pair B</Table.HeaderCell>
                <Table.HeaderCell>Length B</Table.HeaderCell>
                <Table.HeaderCell>Pair C</Table.HeaderCell>
                <Table.HeaderCell>Length C</Table.HeaderCell>
                <Table.HeaderCell>Pair D</Table.HeaderCell>
                <Table.HeaderCell>Length D</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{this.state.tableBody}</Table.Body>
          </Table>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(Veriphy, "/config/veriphy?port=-1", false);
