import React, { Component } from "react";
import { Segment, Message, Form } from "semantic-ui-react";
import { Form as FinalForm, Field } from "react-final-form";
import qs from "querystring";
import { toast } from "react-toastify";

import WithData from "../../utils/withData";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import { InfoMsg, msgConfig } from "../../components/messages";
import { isValidDomainOrIP, isWithinRange } from "../../utils/validate";
import { CustomCheckbox, CustomInput } from "../../components/input-field";

const validate = values => {
  let errors = {};

  const check_ipv6_addr = isValidDomainOrIP(
    values["ipv6_addr"],
    "Hostname or IP Address",
    false,
    6,
    null,
    1,
    // eslint-disable-next-line no-undef
    configIPDNSSupport,
    true
  );
  if (check_ipv6_addr["error"]) {
    errors["ipv6_addr"] = check_ipv6_addr["msg"];
  }

  const check_length = isWithinRange(
    values["length"],
    // eslint-disable-next-line no-undef
    configPingLenMin,
    // eslint-disable-next-line no-undef
    configPingLenMax,
    "Payload Size",
    "bytes"
  );
  if (check_length["error"]) {
    errors["length"] = check_length["msg"];
  }

  const check_pdata = isWithinRange(
    values["pdata"],
    // eslint-disable-next-line no-undef
    configPingPdataMin,
    // eslint-disable-next-line no-undef
    configPingPdataMax,
    "Payload Data",
    ""
  );
  if (check_pdata["error"]) {
    errors["pdata"] = check_pdata["msg"];
  }

  const check_count = isWithinRange(
    values["count"],
    // eslint-disable-next-line no-undef
    configPingCntMin,
    // eslint-disable-next-line no-undef
    configPingCntMax,
    "Packet Count",
    " times"
  );
  if (check_count["error"]) {
    errors["count"] = check_count["msg"];
  }

  const chk_src_addr = isValidDomainOrIP(
    values["src_addr"],
    "Source IP Address",
    false,
    6,
    null,
    1,
    false,
    false
  );
  if (chk_src_addr["error"]) {
    errors["src_addr"] = chk_src_addr["msg"];
  }

  // Check source VLAN
  if (values["src_vid"] && values["src_vid"].length > 0) {
    // eslint-disable-next-line no-undef
    const check_src_vid = isWithinRange(values["src_vid"], configVidMin, configVidMax, "Source VLAN", "");

    if (check_src_vid["error"]) {
      errors["src_vid"] = check_src_vid["msg"];
    }
  }

  // Check source VLAN and Address
  if (
    values["src_portno"] &&
    values["src_portno"].length > 0 &&
    values["src_addr"] &&
    values["src_addr"].length > 0
  ) {
    const msg = "You may only specify either the Source Interface Address or the Port Number - not both";
    errors["src_portno"] = msg;
    errors["src_addr"] = msg;
  }

  if (
    values["src_vid"] &&
    values["src_vid"].length > 0 &&
    values["src_portno"] &&
    values["src_portno"].length
  ) {
    const msg = "You may only specify either the VID or the PortNo or Address for the Source Interface";
    errors["src_vid"] = msg;
    errors["src_addr"] = msg;
  }

  if (
    values["src_vid"] &&
    values["src_vid"].length > 0 &&
    values["src_addr"] &&
    values["src_addr"].length > 0
  ) {
    const msg = "Source Address is not link-local address - cannot specify VLAN";
    errors["src_vid"] = msg;
    errors["src_addr"] = msg;
  }

  return errors;
};

class Ping6 extends Component {
  state = { loading: false };

  onSubmit = async values => {
    this.setState({ loading: true });

    const payload = qs.stringify(values);
    const res = await fetch("/config/ping6", {
      method: "post",
      body: payload,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    });
    const [status, statusText, body] = await Promise.all([res.status, res.statusText, res.text()]);

    if (status === 200) {
      return this.props.history.push(`/diagnostics/ping6_result?ioIndex=${body}`);
    } else {
      toast.error(InfoMsg("error", body, status, statusText), msgConfig());
    }

    this.setState({ loading: false });
  };

  render() {
    const { readOnly } = this.props;
    const initialValues = {
      length: "56",
      pdata: "0",
      count: "5",
      ttlvalue: "64"
    };

    return (
      <Segment.Group>
        <Segment className="segment_page_title">Ping (IPv6)</Segment>
        <Segment loading={this.state.loading}>
          <Message warning>
            Fill in the parameters as needed and press "Start" to initiate the Ping session.
          </Message>
          <FinalForm
            onSubmit={this.onSubmit}
            initialValues={initialValues}
            validate={validate}
            subscription={{
              submitting: true,
              submitSucceeded: true,
              pristine: true
            }}
          >
            {({ form, values, pristine, handleSubmit, submitting, submitSucceeded, submitError }) => {
              return (
                <Form size="tiny">
                  <Field name="ipv6_addr">
                    {props => (
                      <CustomInput
                        fieldProps={props}
                        label="Hostname or IP Address"
                        features={{ maxLength: 253 }}
                      />
                    )}
                  </Field>

                  <Field name="length">
                    {props => (
                      <CustomInput
                        fieldProps={props}
                        label="Payload Size (bytes)"
                        features={{ maxLength: 4 }}
                      />
                    )}
                  </Field>

                  <Field name="pdata">
                    {props => (
                      <CustomInput
                        fieldProps={props}
                        label={`Payload Data Pattern (single byte value; integer or hex with prefix '0x')`}
                        features={{ maxLength: 4 }}
                      />
                    )}
                  </Field>

                  <Field name="count">
                    {props => (
                      <CustomInput
                        fieldProps={props}
                        label={`Packet Count (packets)`}
                        features={{ maxLength: 2 }}
                      />
                    )}
                  </Field>

                  <Form.Group widths="equal">
                    <Field name="src_vid">
                      {props => (
                        <CustomInput
                          fieldProps={props}
                          label="VID for Source Interface"
                          features={{ maxLength: 5 }}
                        />
                      )}
                    </Field>

                    <Field name="src_portno">
                      {props => (
                        <CustomInput
                          fieldProps={props}
                          label="Source Port Number"
                          features={{ maxLength: 2 }}
                        />
                      )}
                    </Field>

                    <Field name="src_addr">
                      {props => (
                        <CustomInput
                          fieldProps={props}
                          label="IP Address for Source Interface"
                          features={{ maxLength: 39 }}
                        />
                      )}
                    </Field>
                  </Form.Group>

                  <Form.Field>
                    <Field name="quiet">
                      {props => <CustomCheckbox fieldProps={props} label="Quiet (only print result)" />}
                    </Field>
                  </Form.Field>

                  <BtnWrapper>
                    <SemanticButton
                      title="reset"
                      type="button"
                      disabled={pristine || submitting}
                      onClick={form.reset}
                    />
                    <SemanticButton
                      type="submit"
                      title="save"
                      disabled={submitting || readOnly}
                      onClick={handleSubmit}
                    />
                  </BtnWrapper>
                </Form>
              );
            }}
          </FinalForm>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(Ping6, "/config/ping6", false);
