import React, { Component } from "react";
import { Segment, Message, Form } from "semantic-ui-react";
import { Form as FinalForm, Field } from "react-final-form";
import qs from "querystring";
import { toast } from "react-toastify";

import { SegmentHeader } from "../../components/styled_component";
import { SemanticButton, BtnWrapper } from "../../components/semantic-component";
import WithData from "../../utils/withData";
import { CustomInput, CustomCheckbox } from "../../components/input-field";
import { isValidDomainOrIP, isWithinRange } from "../../utils/validate";
import { InfoMsg, msgConfig } from "../../components/messages";

const isAutoRefresh = false;

class Traceroute6 extends Component {
  state = { isAutoRefresh, loading: false };

  validate = values => {
    let errors = {};

    // Check ipv6_addr must be valid unicast address, excluding 0.0.0.0
    const check_ipv6_addr = isValidDomainOrIP(
      values["ipv6_addr"],
      "Hostname or IP Address",
      false,
      6,
      null,
      1,
      // eslint-disable-next-line no-undef
      configIPDNSSupport,
      true
    );
    if (check_ipv6_addr["error"]) {
      errors["ipv6_addr"] = check_ipv6_addr["msg"];
    }

    // Check source src_addr be valid unicast address, excluding 0.0.0.0
    const check_src_addr = isValidDomainOrIP(
      values["src_addr"],
      "'Source IP Address'",
      false,
      6,
      null,
      1,
      false,
      false
    );
    if (check_src_addr["error"]) {
      errors["src_addr"] = check_src_addr["msg"];
    }

    // Check source VLAN value
    if (values["src_vid"] && values["src_vid"].length > 0) {
      // eslint-disable-next-line no-undef
      const check_src_vid = isWithinRange(values["src_vid"], configVidMin, configVidMax, "'source VLAN'", "");
      if (check_src_vid["error"]) {
        errors["src_vid"] = check_src_vid["msg"];
      }
    }

    // Check source VLAN and address
    if (
      values["src_vid"] &&
      values["src_vid"].length > 0 &&
      values["src_addr"] &&
      values["src_addr"].length > 0
    ) {
      // eslint-disable-next-line no-undef
      if (!isIpv6LocalLinkAddressStr(values["src_addr"])) {
        // Both source address and source VLAN are specified - only valid if address is link-local
        errors["src_vid"] = "Source Address is not link-local address - cannot specify VLAN";
        errors["src_addr"] = "Source Address is not link-local address - cannot specify VLAN";
      }
    }

    // Check DSCP
    const check_dscp = isWithinRange(
      values["dscp"],
      // eslint-disable-next-line no-undef
      configTracerouteDscpMin,
      // eslint-disable-next-line no-undef
      configTracerouteDscpMax,
      "'DSCP Value'",
      ""
    );
    if (check_dscp["error"]) {
      errors["dscp"] = check_dscp["msg"];
    }

    // Check Response Timeout
    const check_timeout = isWithinRange(
      values["timeout"],
      // eslint-disable-next-line no-undef
      configTracerouteTimeoutMin,
      // eslint-disable-next-line no-undef
      configTracerouteTimeoutMax,
      "'Response Timeout'",
      ""
    );
    if (check_timeout["error"]) {
      errors["timeout"] = check_timeout["msg"];
    }

    // Check Probes
    const check_probes = isWithinRange(
      values["probes"],
      // eslint-disable-next-line no-undef
      configTracerouteProbesMin,
      // eslint-disable-next-line no-undef
      configTracerouteProbesMax,
      "'Probes Per Hop'",
      ""
    );
    if (check_probes["error"]) {
      errors["probes"] = check_probes["msg"];
    }

    // Check Max TTL
    const maxttl = isWithinRange(
      values["maxttl"],
      // eslint-disable-next-line no-undef
      configTracerouteMttlMin,
      // eslint-disable-next-line no-undef
      configTracerouteMttlMax,
      "'Max TTL'",
      ""
    );
    if (maxttl["error"]) {
      errors["maxttl"] = maxttl["msg"];
    }

    return errors;
  };

  onSubmit = async values => {
    this.setState({ loading: true });

    const payload = qs.stringify(values);
    const res = await fetch("/config/traceroute6", {
      method: "POST",
      body: payload,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Lntn-Web-Ver": "2.0"
      }
    });
    const [status, statusText, body] = await Promise.all([res.status, res.statusText, res.text()]);

    if (status === 200) {
      return this.props.history.push(`/diagnostics/traceroute6_result?ioIndex=${body}`);
    } else {
      toast.error(InfoMsg("error", body, status, statusText), msgConfig());
    }

    this.setState({ loading: false });
  };

  render() {
    const { readOnly } = this.props;
    const initialValues = { dscp: "0", probes: "3", timeout: "3", firstttl: "1", maxttl: "30" };

    return (
      <Segment.Group>
        <SegmentHeader>Traceroute (IPv6)</SegmentHeader>
        <Segment loading={this.state.loading}>
          <Message warning>
            Fill in the parameters as needed and press "Start" to initiate the Traceroute session.
          </Message>
          <FinalForm
            onSubmit={this.onSubmit}
            initialValues={initialValues}
            validate={this.validate}
            subscription={{
              submitting: true,
              submitSucceeded: true,
              pristine: true
            }}
          >
            {({ form, values, pristine, handleSubmit, submitting, submitSucceeded, submitError }) => (
              <Form size="tiny">
                <Field name="ipv6_addr">
                  {props => (
                    <CustomInput
                      fieldProps={props}
                      label="Hostname or IP Address"
                      features={{ maxLength: 253 }}
                    />
                  )}
                </Field>

                <Field name="dscp">
                  {props => <CustomInput fieldProps={props} label="DSCP Value" features={{ maxLength: 3 }} />}
                </Field>

                <Field name="probes">
                  {props => (
                    <CustomInput
                      fieldProps={props}
                      label="Number of Probes Per Hop (packets)"
                      features={{ maxLength: 2 }}
                    />
                  )}
                </Field>

                <Field name="timeout">
                  {props => (
                    <CustomInput
                      fieldProps={props}
                      label="Response Timeout (seconds)"
                      features={{ maxLength: 5 }}
                    />
                  )}
                </Field>

                <Field name="maxttl">
                  {props => (
                    <CustomInput fieldProps={props} label="Max TTL Value" features={{ maxLength: 3 }} />
                  )}
                </Field>

                <Form.Group widths="equal">
                  <Field name="src_vid">
                    {props => (
                      <CustomInput
                        fieldProps={props}
                        label="VID for Source Interface"
                        features={{ maxLength: 64 }}
                      />
                    )}
                  </Field>
                  <Field name="src_addr">
                    {props => (
                      <CustomInput
                        fieldProps={props}
                        label="IP Address for Source Interface"
                        features={{ maxLength: 64 }}
                      />
                    )}
                  </Field>
                </Form.Group>

                <Form.Field>
                  <Field name="numeric">
                    {props => <CustomCheckbox fieldProps={props} label="Print Numeric Addresses" />}
                  </Field>
                </Form.Field>

                <BtnWrapper>
                  <SemanticButton
                    title="reset"
                    type="button"
                    disabled={pristine || submitting}
                    onClick={form.reset}
                  />
                  <SemanticButton
                    type="submit"
                    title="Start"
                    disabled={submitting || readOnly}
                    onClick={handleSubmit}
                  />
                </BtnWrapper>
              </Form>
            )}
          </FinalForm>
        </Segment>
      </Segment.Group>
    );
  }
}

export default WithData(Traceroute6, "/config/traceroute4", false);
