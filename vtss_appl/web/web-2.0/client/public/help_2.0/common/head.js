//非css & js
const otherList = [
  `
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  `
];
let otherContent = "";
otherList.forEach(otherElement => {
  otherContent += otherElement;
});
//css
const cssIncludeList = [
  "./semantic/css/checkbox.min.css",
  "./semantic/css/button.min.css",
  "./semantic/css/icon.min.css",
  "./styles/semantic.css"
];
const cssFragment = document.createDocumentFragment();
cssIncludeList.forEach(cssPath => {
  const linkElement = document.createElement("link");
  linkElement.href = cssPath;
  linkElement.type = "text/css";
  linkElement.rel = "stylesheet";
  cssFragment.appendChild(linkElement);
});

//插入<head>中
document.addEventListener("DOMContentLoaded", function() {
  const headElement = document.documentElement.querySelector("head");
  //處理非css & js的引入
  headElement.innerHTML += otherContent;
  //處理css & js的引入
  [cssFragment].forEach(frameElement => {
    headElement.appendChild(frameElement);
  });

  setTimeout(() => {
    //控制loader顯示
    document.querySelector(".myloader-box").style.display = "none";

    //控制頁面內容顯示
    document.querySelector(".main-content").style.overflow = "auto";
    document.querySelector(".main-content").style.visibility = "visible";

    //控制scrollbutton
    const iframeHeight = window.top.document.querySelector("iframe")
      .clientHeight;
    if (document.body.offsetHeight > iframeHeight) {
      document.querySelector("#scrollTop").style.display = "flex";
    }
  }, 400);
});
//一進入頁面就要動作的JS行為寫在這
window.onload = function() {};
