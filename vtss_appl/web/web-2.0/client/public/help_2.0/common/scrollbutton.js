const scrollbutton = document.createElement("div");
scrollbutton.innerHTML = `<div id="scrollTop" style="display:none;"><i class="up"></i</div>`;

document.addEventListener("DOMContentLoaded", function() {
  const mainContent = document.querySelector(".main-content");
  document.body.insertBefore(scrollbutton, mainContent);

  document.querySelector("#scrollTop").onclick = function() {
    let scrollTop = document.documentElement.scrollTop;
    let timer = setInterval(() => {
      scrollTop -= Math.sqrt(scrollTop);
      window.scrollTo({
        top: scrollTop
      });
      if (scrollTop <= 0) {
        clearInterval(timer);
      }
    }, 1);
  };
  for (let i = 0; i < document.querySelectorAll("a").length; i++) {
    const element = document.querySelectorAll("a")[i];
    const elementHref = element.getAttribute("href");
    if (!elementHref) {
      continue;
    }
    const hashIndex = elementHref.indexOf("#");
    const id = element.getAttribute("href").substr(hashIndex + 1);
    if (id) {
      element.onclick = function(event) {
        const scrollTargetELement = document.querySelector(`[name=${id}]`);
        if (scrollTargetELement) {
          event.preventDefault();
          let scrollTargetTop = scrollTargetELement.offsetTop;
          let scrollTop = 0;
          let timer = setInterval(() => {
            scrollTop += Math.sqrt(scrollTargetTop);
            window.scrollTo({
              top: scrollTop
            });
            if (scrollTop >= scrollTargetTop - Math.sqrt(scrollTargetTop)) {
              clearInterval(timer);
            }
          }, 1);
        }
      };
    }
  }
});
