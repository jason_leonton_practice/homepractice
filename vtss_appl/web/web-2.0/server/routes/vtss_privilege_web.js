const express = require('express');

const router = express.Router();

// conf-priv-lvl
let privLevelPayload = '15,|Aggregation/5/10/5/10/|Alarm/5/10/5/10/|DDMI/5/10/5/10/|DHCP/5/10/5/10/|DHCPv6_Client/5/10/5/10/|Diagnostics/5/10/5/10/|ERPS/5/10/5/10/|EventWarning/5/10/5/10/|Firmware/5/10/5/10/|FRR/5/10/5/10/|Green_Ethernet/5/10/5/10/|IP/5/10/5/10/|IPMC_LIB/5/10/5/10/|IPMC_Snooping/5/10/5/10/|LACP/5/10/5/10/|LLDP/5/10/5/10/|Loop_Protect/5/10/5/10/|MAC_Table/5/10/5/10/|MEP/5/10/5/10/|Miscellaneous/15/15/15/15/|Modbus/5/10/5/10/|MRP/5/10/5/10/|MVR/5/10/5/10/|NTP/5/10/5/10/|POE/5/10/5/10/|Ports/5/10/1/10/|Private_VLANs/5/10/5/10/|QoS/5/10/5/10/|RMirror/5/10/5/10/|RMON/5/10/5/10/|Security%28access%29/10/10/5/10/|Security%28network%29/5/10/5/10/|sFlow/5/10/5/10/|SNMP/5/10/5/10/|Spanning_Tree/5/10/5/10/|System/5/10/1/10/|Traceroute/5/10/5/10/|uFDMA_AIL/5/10/5/10/|uFDMA_CIL/5/10/5/10/|VCL/5/10/5/10/|VLANs/5/10/5/10/|XXRP/5/10/5/10/|';

router.get('/priv_lvl', (req, res) => {
  res.send(privLevelPayload);
});

router.post('/priv_lvl', (req, res) => {
  privLevelPayload = `15,|Aggregation/5/10/5/10/|Alarm/5/10/5/10/|DDMI/5/10/5/10/|DHCP/5/10/5/10/|DHCPv6_Client/5/10/5/10/|Diagnostics/5/10/5/10/|ERPS/5/10/5/10/|EventWarning/5/10/5/10/|Firmware/5/10/5/10/|FRR/5/10/5/10/|Green_Ethernet/5/10/5/10/|IP/5/10/5/10/|IPMC_LIB/5/10/5/10/|IPMC_Snooping/5/10/5/10/|LACP/5/10/5/10/|LLDP/5/10/5/10/|Loop_Protect/5/10/5/10/|MAC_Table/5/10/5/10/|MEP/5/10/5/10/|Miscellaneous/15/15/15/15/|Modbus/5/10/5/10/|MRP/5/10/5/10/|MVR/5/10/5/10/|NTP/5/10/5/10/|POE/5/10/5/10/|Ports/5/10/1/10/|Private_VLANs/5/10/5/10/|QoS/5/10/5/10/|RMirror/5/10/5/10/|RMON/5/10/5/10/|Security%28access%29/10/10/5/10/|Security%28network%29/5/10/5/10/|sFlow/5/10/5/10/|SNMP/5/10/5/10/|Spanning_Tree/5/10/5/10/|System/5/10/1/10/|Traceroute/5/10/5/10/|uFDMA_AIL/5/10/5/10/|uFDMA_CIL/5/10/5/10/|VCL/5/10/5/10/|VLANs/5/10/5/10/|XXRP/${req.body.cro_XXRP}/10/5/10/|`;

  res.sendStatus(200);
});

module.exports = router;
