const express = require('express');

const router = express.Router();

router.get('/stat/loop_status', (req, res) => {
  res.send('1/Shutdown/Enabled/0/Down/-/-|2/Shutdown/Enabled/0/Down/-/-|3/Shutdown/Enabled/0/Down/-/-|4/Shutdown/Enabled/0/Up/-/-|5/Shutdown/Enabled/0/Down/-/-|6/Shutdown/Enabled/0/Down/-/-|7/Shutdown/Enabled/0/Down/-/-|8/Shutdown/Enabled/0/Down/-/-|9/Shutdown/Enabled/0/Down/-/-|10/Shutdown/Enabled/0/Down/-/-||');
});

module.exports = router;
