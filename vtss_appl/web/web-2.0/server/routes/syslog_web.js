const express = require('express');

const router = express.Router();

// conf-system
let sysInfoValue = 'jay,jay1,jay2';

router.get('/config/sysinfo', (req, res) => {
  res.set({ 'X-ReadOnly': true });
  res.send(sysInfoValue);
});

router.post('/config/sysinfo', (req, res) => {
  sysInfoValue = `${req.body.sys_contact},${req.body.sys_name},${
    req.body.sys_location
  }`;
  res.sendStatus(200);
});

module.exports = router;
