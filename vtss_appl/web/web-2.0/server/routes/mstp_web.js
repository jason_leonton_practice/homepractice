const express = require('express');

const router = express.Router();

router.get('/stat/rstp_status_ports', (req, res) => {
  res.send('1/Disabled/Discarding/-|2/Disabled/Discarding/-|3/Disabled/Discarding/-|4/Disabled/Discarding/-|5/Disabled/Discarding/-|6/Disabled/Discarding/-|7/DesignatedPort/Forwarding/  0d 00:25:05|8/Disabled/Discarding/-|9/Disabled/Discarding/-|10/Disabled/Discarding/-|11/Disabled/Discarding/-|12/Disabled/Discarding/-||');
  // res.send('Error: ');
});

router.get('/stat/rstp_statistics', (req, res) => {
  res.send('7/672/0/0/0/0/0/0/0/0/0||');
});

router.get('/stat/rstp_status_bridges', (req, res) => {
  res.send('0/CIST/32768.00-11-22-33-12-04/32768.00-11-22-33-12-04/-/0/Steady/-/0/0/32768.00-11-22-33-12-04||');
});

router.get('/stat/rstp_status_bridge', (req, res) => {
  const { bridge } = req.query;

  if (bridge) {
    res.send('0/CIST/32768.00-11-22-33-12-04/32768.00-11-22-33-12-04/-/0/Steady/-/0/0/32768.00-11-22-33-12-04|1/7/128:007/DesignatedPort/Forwarding/20000/Yes/Yes/  0d 03:17:26|1/8/128:008/DesignatedPort/Discarding/20000/No/Yes/  0d 00:00:02||');
  }
});

router.get('/config/rstp_msti', (req, res) => {
  res.send('0/CIST/128|1/MSTI1/128|2/MSTI2/128|3/MSTI3/128|4/MSTI4/128|5/MSTI5/128|6/MSTI6/128|7/MSTI7/128|');
});

router.get('/config/rstp_msti_map', (req, res) => {
  res.send('00-11-22-33-12-04|0|1/MSTI1/|2/MSTI2/111|3/MSTI3/|4/MSTI4/|5/MSTI5/|6/MSTI6/|7/MSTI7/|');
});

router.post('/config/rstp_msti', (req, res) => {
  res.sendStatus(200);
});

router.post('/config/rstp_msti_map', (req, res) => {
  res.sendStatus(200);
});
module.exports = router;
