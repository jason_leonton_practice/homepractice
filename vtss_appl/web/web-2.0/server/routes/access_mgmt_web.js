const express = require('express');

const router = express.Router();

router.get('/stat/access_mgmt_statistics', (req, res) => {
  res.send('HTTP/0/0/0|HTTPS/0/0/0|SNMP/0/0/0|TELNET/0/0/0|SSH/0/0/0');
});

module.exports = router;
