const express = require('express');

const router = express.Router();

// conf-ddmi
let configModBusPayload = '0';
router.get('/config/modbus', (req, res) => {
  res.send(configModBusPayload);
});

router.post('/config/modbus', (req, res) => {
  configModBusPayload = req.body.modbus_mode;
  res.sendStatus(200);
});


module.exports = router;
