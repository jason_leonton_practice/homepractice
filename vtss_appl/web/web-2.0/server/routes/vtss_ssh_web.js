const express = require('express');

const router = express.Router();

// conf-ssh-config
let sshConfigPayload = '1';

router.get('/ssh', (req, res) => {
  res.send(sshConfigPayload);
});

router.post('/ssh', (req, res) => {
  sshConfigPayload = req.body.ssh_mode;
  res.sendStatus(200);
});

module.exports = router;
