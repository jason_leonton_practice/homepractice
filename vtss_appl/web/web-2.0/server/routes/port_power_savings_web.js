const express = require('express');

const router = express.Router();

// moni-green ethernet
router.get('/stat/port_power_savings_status', (req, res) => {
  const monitorPortPowerSavingsStatusData = '|1|1/1/0/1/0/0/0/0/0&2/1/0/0/0/0/0/0/0&3/1/0/0/0/0/0/0/0&4/1/0/0/0/0/0/0/0&5/1/0/0/0/0/0/0/0&6/1/0/0/0/0/0/0/0&7/1/0/0/0/0/0/0/0&8/1/0/0/0/0/0/0/0&9/1/0/0/0/0/0/0/0&10/1/0/0/0/0/0/0/0&11/0/0/0/0/0/0/0/0&12/0/0/0/0/0/0/0/0&';

  res.send(monitorPortPowerSavingsStatusData);
});


module.exports = router;
