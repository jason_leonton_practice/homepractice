const express = require('express');
const multer = require('multer');

const router = express.Router();

// main-download
let icfgConfGetFileListDelete = 'OK*startup-config*';

router.get('/icfg_conf_get_file_list', (req, res) => {
  const { op } = req.query;

  if (op === 'download') return res.send('OK*Local*USB*running-config*default-config*startup-config*');
  if (op === 'delete') { return res.send(icfgConfGetFileListDelete); }
  if (op === 'activate') return res.send('OK*default-config*startup-config*');
  if (op === 'upload') return res.send('OK*Local*USB*running-config*jaypan*running_config*new%20file*startup-config*peterpan*');

  return res.status(404).send();
});

router.get('/icfg_conf_save', (req, res) => {
  res.send();
});

router.get('/icfg_conf_activate', (req, res) => {
  res.send('DONE\n(No output was generated.)');
});

router.post('/icfg_conf_download', (req, res) => {
  const file = `${__dirname}/../lib/running-config`;
  res.setHeader('Content-Type', 'application/x-download');
  res.download(file);
  // res.status(404).send('USB not detect.');
});

router.post('/icfg_conf_delete', (req, res) => {
  icfgConfGetFileListDelete = 'OK*';
  res.send('startup-config successfully deleted.');
  // res.status(404).send();
});

router.post('/icfg_conf_save', (req, res) => {
  icfgConfGetFileListDelete = 'OK*startup-config*';
  res.status(200).send('startup-config saved successfully.');
  // res.status(400).send();
});

router.post('/icfg_conf_activate', (req, res) => {
  setTimeout(() => res.sendStatus(200), 3000);
  // res.send();
});

const upload = multer({ dest: './server/uploads/' });

router.post('/icfg_conf_upload', upload.single('source_file'), (req, res) => {
  setTimeout(() => res.status(201).send(), 3000);
});

module.exports = router;
