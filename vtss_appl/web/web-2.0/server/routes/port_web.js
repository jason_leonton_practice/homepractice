const express = require('express');

const router = express.Router();

// device-monitor
router.get('/stat/portstate', (req, res) => {
  let status = 'green';
  const randomStatus = () => {
    const randomNum = Math.round(Math.random());
    return randomNum ? 'green' : 'red';
  };
  status = randomStatus();
  // 2004
  // `1|switch_lntn.png|1/jack_copper_down_top.png/Port 1: Down/21/337/38/43/-1|2/jack_copper_down_bottom.png/Port 2: Down/59/337/38/43/1|3/jack_copper_down_top.png/Port 3: Down/21/294/38/43/-1|4/jack_copper_down_bottom.png/Port 4: Down/59/294/38/43/1|5/jack_copper_down_top.png/Port 5: Down/21/251/38/43/-1|6/jack_copper_down_bottom.png/Port 6: Down/59/251/38/43/1|7/jack_copper_down_top.png/Port 7: Down/21/208/38/43/-1|8/jack_copper_down_bottom.png/Port 8: Down/59/208/38/43/1|9/jack_copper_link_top.png/Port 9: 100fdx/21/157/38/43/-1|10/jack_copper_down_bottom.png/Port 10: Down/59/157/38/43/1|11/jack_copper_down_top.png/Port 11: Down/21/114/38/43/-1|12/jack_copper_down_bottom.png/Port 12: Down/59/114/38/43/1|13/jack_copper_down_top.png/Port 13: Down/21/71/38/43/-1|14/jack_copper_down_bottom.png/Port 14: Down/59/71/38/43/1|15/jack_copper_down_top.png/Port 15: Down/21/28/38/43/-1|16/jack_copper_down_bottom.png/Port 16: Down/59/28/38/43/1|17/jack_sfp_down_bottom.png/Port 17: Down/123/334/30/43/1|18/jack_sfp_down_bottom.png/Port 18: Down/123/291/30/43/1|19/jack_sfp_down_bottom.png/Port 19: Down/123/238/30/43/1|20/jack_sfp_down_bottom.png/Port 20: Down/123/195/30/43/1|#0/none/0/0/7/7|1/sys_led_${status}.png/152/44/7/7|2/none/121/65/7/7|3/none/152/65/7/7|4/sys_led_green.png/121/44/7/7|5/none/136/44/7/7|,`;

  const switchLabelData = `1|switch_lntn.png|1/jack_copper_down_top.png/Port 1: Down/22/340/35/40/-1|2/jack_copper_down_bottom.png/Port 2: Down/58/340/35/40/1|3/jack_copper_down_top.png/Port 3: Down/22/300/35/40/-1|4/jack_copper_down_bottom.png/Port 4: Down/58/300/35/40/1|5/jack_copper_down_top.png/Port 5: Down/22/260/35/40/-1|6/jack_copper_down_bottom.png/Port 6: Down/58/260/35/40/1|7/jack_copper_link_top.png/Port 7: 1Gfdx/22/220/35/40/-1|8/jack_copper_down_bottom.png/Port 8: Down/58/220/35/40/1|9/jack_sfp_down_top.png/Port 9: Down/22/150/30/40/-1|10/jack_sfp_down_top.png/Port 10: Down/22/110/30/40/-1|11/jack_sfp_down_top.png/Port 11: Down/22/70/30/40/-1|12/jack_sfp_down_top.png/Port 12: Down/22/30/30/40/-1|#0/none/0/0/7/7|1/sys_led_${status}.png/93/29/7/7|2/none/59/48/7/7|3/none/93/48/7/7|4/sys_led_green.png/59/29/7/7|5/none/76/29/7/7|,`;
  res.send(switchLabelData);
});

// moni-stat-port & moni-stat-detailed

const portsStat = '1/0/0/0/0/0/0/0/0/0|2/0/0/0/0/0/0/0/0/0|3/0/0/0/0/0/0/0/0/0|4/0/0/0/0/0/0/0/0/0|5/0/0/0/0/0/0/0/0/0|6/0/0/0/0/0/0/0/0/0|7/4708/6719/834438/2104460/0/0/0/0/610|8/0/0/0/0/0/0/0/0/0|9/0/0/0/0/0/0/0/0/0|10/0/0/0/0/0/0/0/0/0|11/0/0/0/0/0/0/0/0/0|12/0/0/0/0/0/0/0/0/0|13/0/0/0/0/0/0/0/0/0|14/0/0/0/0/0/0/0/0/0|15/0/0/0/0/0/0/0/0/0|16/0/0/0/0/0/0/0/0/0|17/0/0/0/0/0/0/0/0/0|18/0/0/0/0/0/0/0/0/0|19/0/0/0/0/0/0/0/0/0|20/0/0/0/0/0/0/0/0/0';
let portStat = '1,12,1/15870/17814/3172520/6579645/0/0/0/0|2/14005/9921/1257/7888/608/5|3/0/0|4/9775/121/1086/8499/711/4697/20/88/4278/4171/0/238/0/0/0/0/0/0|5/0/0|7/15870/0/0/0/0/0/0/0/0/0/0/0/0/0/0/17814|8/1066';


router.get('/stat/ports', (req, res) => {
  res.send(portsStat);
});

router.get('/stat/port', (req, res) => {
  const portno = req.query.port;
  const clear = !!req.query.clear;

  if (portno) {
    portStat = `${portno},12,1/15870/17814/3172520/6579645/0/0/0/0|2/14005/9921/1257/7888/608/5|3/0/0|4/9775/121/1086/8499/711/4697/20/88/4278/4171/0/238/0/0/0/0/0/0|5/0/0|7/15870/0/0/0/0/0/0/0/0/0/0/0/0/0/0/17814|8/1066`;
  }

  if (portno === '2') {
    portStat = '2,20,1/0/0/0/0/0/0/0/0|2/0/0/0/0/0/0|3/0/0|4/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0|5/0/0|7/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0|8/0';
  }

  if (clear) {
    portStat = `${portno},20,1/0/0/0/0/0/0/0/0|2/0/0/0/0/0/0|3/0/0|4/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0|5/0/0|7/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0|8/0`;
  }

  res.send(portStat);
});

router.get('/config/veriphy', (req, res) => {
  // finished data
  if (req.query.port === '2') {
    return res.send('1/0/16/0/16/0/16/0/16/0;2/1/17/0/17/0/17/0/17/0;3/0/16/0/16/0/16/0/16/0;4/0/16/0/16/0/16/0/16/0;5/0/16/0/16/0/16/0/16/0;6/0/16/0/16/0/16/0/16/0;7/0/16/0/16/0/16/0/16/0;8/0/16/0/16/0/16/0/16/0;9/0/16/0/16/0/16/0/16/0;10/0/16/0/16/0/16/0/16/0');
  }

  // running data
  return res.send('1/0/16/0/16/0/16/0/16/0;2/0/16/0/16/0/16/0/16/0;3/0/16/0/16/0/16/0/16/0;4/0/16/0/16/0/16/0/16/0;5/0/16/0/16/0/16/0/16/0;6/0/16/0/16/0/16/0/16/0;7/0/16/0/16/0/16/0/16/0;8/0/16/0/16/0/16/0/16/0;9/0/16/0/16/0/16/0/16/0;10/0/16/0/16/0/16/0/16/0');
});

module.exports = router;
