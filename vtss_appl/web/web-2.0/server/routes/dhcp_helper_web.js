const express = require('express');

const router = express.Router();

// router.get('/stat/dhcp_helper_statistics', (req, res) => {
//   res.send('5/Normal Forward/Server/Client/Snooping/Relay/,3,0/4/1/0/0/1/0/0/1/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0');
// });

router.get('/stat/dhcp_helper_statistics', (req, res) => {
  // // console.table(req.query);
  const { clear = 0, port = 1, user: mode = -1 } = req.query;
  console.log('TCL: port', mode);


  // if (clear) {
  //   res.send(`${mode}/Normal Forward/Server/Client/Snooping/Relay/,${port},0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0`);
  // } else if (mode === '5' || mode === '3') { res.send(`${mode}/Normal Forward/Server/Client/Snooping/Relay/,${port},0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0`); } else {
  //   res.send(`${mode}/Normal Forward/Server/Client/Snooping/Relay/,${port},0/4/1/0/0/1/0/0/1/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0`);
  // }
  console.log(clear);
  if (clear === '1') {
    return (res.send(`${mode}/Normal Forward/Server/Client/Snooping/Relay/,${port},0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0`));
  }
  if (mode === '-1'
  ) {
    res.send(`5/Normal Forward/Server/Client/Snooping/Relay/,${port},0/4/1/0/0/1/0/0/1/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0`);
  }
  if (mode === '3') {
    res.send(`3/Normal Forward/Server/Client/Snooping/Relay/,${port},0/4/1/0/0/1/0/0/1/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0`);
  }

  res.send(`${mode}/Normal Forward/Server/Client/Snooping/Relay/,${port},0/9/9/0/0/1/0/0/1/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0`);
});

module.exports = router;

// dropdown list 總共會有6種，第一種是combined，

// 5/Normal Forward/Server/Client/Snooping/Relay/,3,0/4/1/0/0/1/0/0/1/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0
// 0/Normal Forward/Server/Client/Snooping/Relay/,3,0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0
// 1/Normal Forward/Server/Client/Snooping/Relay/,3,0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0
// 2/Normal Forward/Server/Client/Snooping/Relay/,3,0/4/1/0/0/1/0/0/1/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0
// 3/Normal Forward/Server/Client/Snooping/Relay/,3,0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0
// 4/Normal Forward/Server/Client/Snooping/Relay/,3,0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0


// Format: [select_user_id]/[user_name1]/[user_name2]/...,[port_no],[counter_1]/[counter_2]/.../[counter_n]
// var input = "0/Server/Client/Snooping/Relay,1,1/2/3/4/5/6/7/8/9/10/11/12/13/14/15/16/17/18/19/20/21/22/23/24";

// 總共會有 25 or 26 筆資料，所以我的作法會先用splice分成兩個子矩陣
