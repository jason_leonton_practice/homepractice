const express = require('express');

const router = express.Router();

// conf-users
const confUsers = '20|1,5,5|2,admin,15|';

router.get('/stat/users', (req, res) => {
  res.send(confUsers);
});

// conf-users-config
router.get('/config/user_config', (req, res) => {
  const { user } = req.query;

  if (user && user !== '-1') {
    res.send('1,test,15,15');
  } else {
    res.send('-1,,0,15');
  }
});

router.post('/config/user_config', (req, res) => {
  res.sendStatus(200);
});

module.exports = router;
