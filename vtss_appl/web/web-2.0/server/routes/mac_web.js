const express = require('express');

const router = express.Router();

// moni-green ethernet
router.get('/config/dynamic_mac_table', (req, res) => {
  const monitorDynaMacData = '10/00-00-00-00-00-00/0/0|00-11-22-33-12-04/1/1/1/0/0/0/0/0/0/0/0/0/0/0/0|33-33-00-00-00-01/1/1/1/1/1/1/1/1/1/1/1/1/1/1/1|33-33-ff-33-12-04/1/1/1/1/1/1/1/1/1/1/1/1/1/1/1|c0-4a-00-02-09-5a/1/0/0/1/0/0/0/0/0/0/0/0/0/0/0|ff-ff-ff-ff-ff-ff/1/1/1/1/1/1/1/1/1/1/1/1/1/1/1|';

  res.send(monitorDynaMacData);
});


module.exports = router;
