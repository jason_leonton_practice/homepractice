const express = require('express');

const router = express.Router();

router.get('/stat/dhcp_relay_statistics', (req, res) => {
  res.send('0/0/0/0/0/0/0/0,0/0/0/0/0/0/0');
});

module.exports = router;
