const express = require('express');

const router = express.Router();

// conf-ddmi
let configDdmiPayload = '1';
router.get('/config/ddmi', (req, res) => {
  // res.set({"X-ReadOnly":  true})
  res.send(configDdmiPayload);
});

router.post('/config/ddmi', (req, res) => {
  configDdmiPayload = req.body.ddmi_mode;
  res.sendStatus(200);
});

router.get('/stat/ddmi_overview', (req, res) => {
  // res.send('|');
  res.send('11/-/-/-/-/-/-|12/Antaira/SFP-10G-M/AAF2018110092//2018-11-15/10G');
});

router.get('/stat/ddmi_detailed', (req, res) => {
  res.send('11,12|12/Antaira/SFP-10G-M/AAF2018110092//2018-11-15/10G|34.000/90.000/85.000/-40.000/-45.000|3.3130/3.6000/3.5000/3.1000/3.0000|5.280/15.000/12.000/0.000/0.000|0.6330/1.1220/1.0000/0.1585/0.1413|0.0001 --/1.1220/1.0000/0.0631/0.0562');
});
module.exports = router;
