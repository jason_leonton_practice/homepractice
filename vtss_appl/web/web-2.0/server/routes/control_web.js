const express = require('express');

const router = express.Router();

// main-wreset
router.post('/misc', (req, res) => {
  res.status(200).send('<b>Configuration Factory Reset Done.</b> The configuration has been reset. The new configuration is available immediately.');
});

router.get('/wreset', (req, res) => {
  // res.set({"X-ReadOnly":  true})
  res.send('');
});

router.get('/wreset_status', (req, res) => {
  res.send('');
});

router.get('/misc', (req, res) => {
  res.send('');
});

module.exports = router;
