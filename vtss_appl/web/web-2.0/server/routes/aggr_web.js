const express = require('express');

const router = express.Router();

router.get('/stat/aggregation_status', (req, res) => {
  res.send(`1|LLAG1|STATIC|100M|GigabitEthernet 1/1,4|GigabitEthernet 1/4@
            2|LLAG2|STATIC|Undefined|GigabitEthernet 1/2,5-6,9-10|none@@`);
});

router.get('/config/aggr_common', (req, res) => {
  res.send('1/0/1/1/0');
});

router.post('/config/aggr_common', (req, res) => {
  res.status(200).send('1/0/1/1/0');
});

let aggrGroupPayload = '1/1/1/1/1/1/1/1/1/1/1/1/0/0/0|0/0/0/0/0/0/0/0/0/0/0/0/0/1/12|0/0/0/0/0/0/0/0/0/0/0/0/0/1/12|0/0/0/0/0/0/0/0/0/0/0/0/0/1/12|0/0/0/0/0/0/0/0/0/0/0/0/0/1/12|0/0/0/0/0/0/0/0/0/0/0/0/0/1/12|0/0/0/0/0/0/0/0/0/0/0/0/0/1/12|0/0';

router.get('/config/aggr_groups', (req, res) => {
  res.send(aggrGroupPayload);
});

router.post('/config/aggr_groups', (req, res) => {
  aggrGroupPayload = '1/1/1/0/0/0/1/1/1/1/1/1/0/0/0|0/0/0/1/1/1/0/0/0/0/0/0/3/1/12|0/0/0/0/0/0/0/0/0/0/0/0/0/1/12|0/0/0/0/0/0/0/0/0/0/0/0/0/1/12|0/0/0/0/0/0/0/0/0/0/0/0/0/1/12|0/0/0/0/0/0/0/0/0/0/0/0/0/1/12|0/0/0/0/0/0/0/0/0/0/0/0/0/1/12|0/0';
  res.sendStatus(200);
});

module.exports = router;
