const express = require('express');

const router = express.Router();

router.get('/stat/sflow_status', (req, res) => {
  if (req.query.clear === '1') {
    return res.send('%3CConfigured%20through%20local%20management%3E/588/192.168.10.50/0/0/0/0#1/0/196#2/0/98#3/0/65#4/45/49#5/0/39#6/0/32#7/0/28#8/0/24#9/0/22#10/0/20');
  }

  if (req.query.port === '0') {
    return res.send('%3CConfigured%20through%20local%20management%3E/588/192.168.10.50/206/0/45/592#1/0/0#2/0/0#3/0/0#4/0/0#5/0/0#6/0/0#7/0/0#8/0/0#9/0/0#10/0/0');
  }

  // return res.send('/0/0.0.0.0/0/0/0/0#1/0/0#2/0/0#3/0/0#4/0/0#5/0/0#6/0/0#7/0/0#8/0/0#9/0/0#10/0/0');
  return res.send('%3CConfigured%20through%20local%20management%3E/588/192.168.10.50/206/0/45/592#1/0/196#2/0/98#3/0/65#4/45/49#5/0/39#6/0/32#7/0/28#8/0/24#9/0/22#10/0/20');
});

module.exports = router;
