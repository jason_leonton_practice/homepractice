const express = require('express');

const router = express.Router();

router.get('/stat/vlan_membership_stat', (req, res) => {
  const { user } = req.query;

  if (parseInt(user, 10) > 2) {
    return res.send('Combined|1/Admin|2/NAS|4/MVRP|5/GVRP|6/MVR|7/MEP|11/RMirror|14#NAS');
  }

  return res.send('Combined|1/Admin|2/NAS|4/MVRP|5/GVRP|6/MVR|7/MEP|11/RMirror|14#Combined/1|1|1|1|1|1|1|1|1|1|1|1|1/2|1|0|1|1|1|1|1|1|2|3|0|0/3|1|1|1|1|1|1|1|1|1|1|1|1/4|1|0|1|1|1|1|1|1|2|3|0|0/5|1|1|1|1|1|1|1|1|1|1|1|1/6|1|0|1|1|1|1|1|1|2|3|0|0');
});

router.get('/stat/vlan_port_stat', (req, res) => {
  const { user } = req.query;

  const selectedUser = () => {
    switch (parseInt(user, 10)) {
      case 2: return 'Admin';
      case 4: return 'NAS';
      case 6: return 'GVRP';
      case 7: return 'MVR';
      case 9: return 'MSTP';
      case 10: return 'ERPS';
      case 11: return 'MEP';
      case 13: return 'VCL';
      case 14: return 'RMirror';
      default: return 'Combined';
    }
  };

  if (parseInt(user, 10) === 9) {
    return res.send(`Combined|1/Admin|2/NAS|4/GVRP|6/MVR|7/MSTP|9/ERPS|10/MEP|11/VCL|13/RMirror|14#${selectedUser()}/7|0|0|0|1|1|All|0|Untag PVID|0|Unaware|0|0`);
  }

  if (parseInt(user, 10) > 2) {
    return res.send(`Combined|1/Admin|2/NAS|4/GVRP|6/MVR|7/MSTP|9/ERPS|10/MEP|11/VCL|13/RMirror|14#${selectedUser()}`);
  }

  return res.send(`Combined|1/Admin|2/NAS|4/GVRP|6/MVR|7/MSTP|9/ERPS|10/MEP|11/VCL|13/RMirror|14#${selectedUser()}/1|0|1|1|1|1|All|1|Untag All|1|C-Port|1|0/2|0|1|1|1|1|All|1|Untag All|1|C-Port|1|0/3|0|1|1|1|1|All|1|Untag All|1|C-Port|1|0/4|0|1|1|1|1|All|1|Untag All|1|C-Port|1|0/5|0|1|1|1|1|All|1|Untag All|1|C-Port|1|0/6|0|1|1|1|1|All|1|Untag All|1|C-Port|1|0/7|0|1|1|1|1|All|1|Untag All|1|C-Port|1|0/8|0|1|1|1|1|All|1|Untag All|1|C-Port|1|0/9|0|1|1|1|1|All|1|Untag All|1|C-Port|1|0/10|0|1|1|1|1|All|1|Untag All|1|C-Port|1|0/11|0|1|1|1|1|All|1|Untag All|1|C-Port|1|0/12|0|1|1|1|1|All|1|Untag All|1|C-Port|1|0`);
});

module.exports = router;
