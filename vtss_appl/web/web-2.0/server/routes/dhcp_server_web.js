const express = require('express');

const router = express.Router();

router.get('/stat/dhcp_server_declined', (req, res) => {
  res.send('192.168.1.2|192.168.5.6||');
  // res.send('|');
});

router.get('/config/dhcp_relay', (req, res) => {
  res.send('1/0/0.0.0.0/0/1');
});

router.get('/config/dhcp_snooping', (req, res) => {
  res.send('0,1/0|2/0|3/0|4/0|5/0|6/0|7/0|8/0|9/0|10/0|11/0|12/0|');
});

router.post('/config/dhcp_relay', (req, res) => {
  res.send(200);
});

router.post('/config/dhcp_snooping', (req, res) => {
  res.sendStatus(200);
});

module.exports = router;
