const express = require('express');

const router = express.Router();
const data = require('../lib/navbar.json');

router.get('/navbar', (req, res) => {
  res.send(JSON.stringify(data));
});

module.exports = router;
