const express = require('express');

const router = express.Router();

// moni-system
router.get('/stat/sys', (req, res) => {
  const monitorSysData = '00-11-22-33-12-04/ 0d 22:22:52/1970-01-01T22:22:52+00:00/V6.0.1/2019-04-10T01%3A58%3A42%2B08%3A00/VSC7429////76a43b4%2B/0000000000000000000000000000000/';

  res.send(monitorSysData);
});

// moni-sys-led-status
const sysLedStatusPayload = 'System LED: green, solid, normal indication.';
router.get('/stat/sys_led_status', (req, res) => {
  res.send(sysLedStatusPayload);
});

// moni-perf-cpuload
router.get('/stat/cpuload', (req, res) => {
  const randomIntegerhundredsms = Math.floor(Math.random() * 100);
  const randomIntegerones = Math.floor(Math.random() * 100);
  const randomIntegertens = Math.floor(Math.random() * 100);

  const cpuLoad = `${randomIntegerhundredsms}|${randomIntegerones}|${randomIntegertens}`;
  // res.sendStatus(501)
  res.send(cpuLoad);
});

module.exports = router;
