const express = require('express');

const router = express.Router();

// lacp_port_config
const confLacpPorts = '1/0/1/32768|2/0/1/32768|3/0/1/32768|4/0/1/32768|5/0/1/32768|6/0/1/32768|7/0/1/32768|8/0/1/32768|9/0/1/32768|10/0/1/32768|11/0/1/32768|12/0/1/32768|13/0/1/32768|14/0/1/32768|15/0/1/32768|16/0/1/32768|17/0/1/32768|18/0/1/32768|19/0/1/32768|20/0/1/32768|#21321';
router.get('/config/lacp_ports', (req, res) => {
  res.send(confLacpPorts);
});

router.post('/config/lacp_ports', (req, res) => {
  res.sendStatus(200);
});

// module of Monitor/Aggregation/LACP
// 1.no partner info return
// 2.with partner info
router.get('/stat/lacp_sys_status', (req, res) => {
  res.send('32768/00-11-22-33-12-04|');
  // res.send('32768/00-11-22-33-12-04|'
  // + 'LLAG2/00-20-04-00-02-01/32768/1/  0d 00:00:09/5,7|'
  // + 'LLAG1/00-20-04-00-02-01/32768/3/  0d 00:00:09/1,3||');
});

// 1.with data
// 2.without
router.get('/stat/lacp_internal_status', (req, res) => {
  res.send('1/Down/1/32768/Active/Fast/Yes/Yes/No/No/Yes/No|3/Down/1/32768/Active/Fast/Yes/Yes/No/No/Yes/No|5/Down/2/32768/Active/Fast/Yes/Yes/No/No/Yes/No|7/Down/2/32768/Active/Fast/Yes/Yes/No/No/Yes/No||');
  // res.send('|');
});

router.get('/stat/lacp_neighbor_status', (req, res) => {
  res.send('1/Active/1/2/15/32768/Active/Fast/Yes/Yes/Yes/Yes/No/No|3/Active/1/2/16/32768/Active/Fast/Yes/Yes/Yes/Yes/No/No|5/Active/2/1/13/32768/Active/Fast/Yes/Yes/Yes/Yes/No/No|7/Active/2/1/14/32768/Active/Fast/Yes/Yes/Yes/Yes/No/No||');
  // res.send('|');
});

router.get('/stat/lacp_statistics', (req, res) => {
  res.send('1/579/571/0/0|3/587/570/0/0|5/578/570/0/0|7/578/570/0/0|9/0/0/0/0|10/0/0/0/0||');
  // res.send('|');
});

module.exports = router;
