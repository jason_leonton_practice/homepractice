const express = require('express');

const router = express.Router();

router.get('/config/ping4', (req, res) => {
  if (req.query.ioIndex) {
    const payload = `PING 192.168.10.5 (192.168.10.5): 56 data bytes
                      64 bytes from 192.168.10.5: seq=0 ttl=64 time=0.754 ms
                      64 bytes from 192.168.10.5: seq=1 ttl=64 time=0.485 ms
                      64 bytes from 192.168.10.5: seq=2 ttl=64 time=0.511 ms
                      64 bytes from 192.168.10.5: seq=3 ttl=64 time=0.496 ms
                      64 bytes from 192.168.10.5: seq=4 ttl=64 time=0.494 ms

                      --- 192.168.10.5 ping statistics ---
                      5 packets transmitted, 5 packets received, 0% packet loss
                      round-trip min/avg/max = 0.485/0.548/0.754 ms

                      Ping session completed.`;
    res.send(payload);
  } else {
    res.send('');
  }
});

router.get('/config/ping6', (req, res) => {
  if (req.query.ioIndex) {
    const payload = `ping6: bad address '192.168.10.5'

                      Ping session completed.`;
    res.send(payload);
  } else {
    res.send('');
  }
});

router.get('/config/traceroute4', (req, res) => {
  if (req.query.ioIndex) {
    const payload = `traceroute to 192.168.10.5 (192.168.10.5), 30 hops max, 38 byte packets
                      1  192.168.10.5 (192.168.10.5)  0.098 ms  0.103 ms  0.060 ms

                    Traceroute session completed.`;
    return res.send(payload);
  }

  return res.send('');
});

router.get('/config/traceroute6', (req, res) => {
  if (req.query.ioIndex) {
    const payload = `traceroute to 192.168.10.5 (192.168.10.5), 30 hops max, 38 byte packets
                      1  192.168.10.5 (192.168.10.5)  0.098 ms  0.103 ms  0.060 ms

                    Traceroute session completed.`;
    return res.send(payload);
  }

  return res.send('');
});

router.get('/stat/ip2_status', (req, res) => {
  res.send(`VLAN1
    LINK: 00-11-22-33-12-04 Mtu:1500 <UP BROADCAST MULTICAST>
    IPv4: 192.168.10.5/24 192.168.10.255
    IPv6: fe80::211:22ff:fe33:1204/64 <>
  ^@^@^Codes: C - connected, S - static, O - OSPF,
        * - selected route, D - DHCP installed route


  C* 192.168.10.0/24 is directly connected, VLAN 1
  ^@^@^192.168.10.1 via VLAN1:c0-4a-00-02-01-2a
  `);
});

router.post('/config/ping4', (req, res) => {
  res.send('0x21e9a98');
});

router.post('/config/ping6', (req, res) => {
  res.send('0x21e9a98');
});

router.post('/config/traceroute4', (req, res) => {
  setTimeout(() => res.send('0x21f3100'), 1000);
});

router.post('/config/traceroute6', (req, res) => {
  setTimeout(() => res.send('0x21f3100'), 1000);
});

module.exports = router;
