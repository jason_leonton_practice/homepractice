const express = require('express');

const router = express.Router();
const multer = require('multer');

const upload = multer();

router.post('/firmware', upload.single('file_src'), (req, res) => {
  res.send();
});

router.get('/firmware', (req, res) => {
  res.send('');
});

router.get('/firmware_status', (req, res) => {
  res.send('Error: USB not detect!');
});

router.get('/sw_select', (req, res) => {
  res.send('E5V40-01-20xx_6.0.1_19111818.rom|V6.0.1|2019-11-18T18:10:34+08:00^linux.bk|V6.0.1|2019-11-18T18:10:34+08:00');
});
module.exports = router;
