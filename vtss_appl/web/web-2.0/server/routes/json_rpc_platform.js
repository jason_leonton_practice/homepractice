const express = require('express');

const router = express.Router();

router.post('/', (req, res) => {
  if (req.body.method === 'ip.status.route_info.ipv4.get') {
    res.send(`{"id":"jsonrpc","error":null,"result":[{"key":{"IpNetwork":"192.168.10.0","IpSubnetMaskLength":24,"Protocol":"protoConneted","NextHop":"0.0.0.0"},"val":{"Selected":false,"Metric":1,"Distance":${Math.floor(Math.random() * 10)},"Uptime":"5:6:16","Ifindex":"VLAN 1","Active":true}},{"key":{"IpNetwork":"192.168.20.0","IpSubnetMaskLength":18,"Protocol":"protoStatic","NextHop":"18.18.18.18"},"val":{"Selected":false,"Metric":2,"Distance":1,"Uptime":0,"Ifindex":"NONE","Active":false}},{"key":{"IpNetwork":"192.168.30.0","IpSubnetMaskLength":24,"Protocol":"protoStatic","NextHop":"18.18.18.18"},"val":{"Selected":false,"Metric":3,"Distance":1,"Uptime":"16:15:2","Ifindex":"NONE","Active":true}},{"key":{"IpNetwork":"192.168.10.0","IpSubnetMaskLength":20,"Protocol":"protoConneted","NextHop":"0.0.0.0"},"val":{"Selected":true,"Metric":4,"Distance":1,"Uptime":0,"Ifindex":"VLAN 1","Active":true}},{"key":{"IpNetwork":"192.168.10.1","IpSubnetMaskLength":24,"Protocol":"protoConneted","NextHop":"0.0.0.0"},"val":{"Selected":false,"Metric":5,"Distance":1,"Uptime":0,"Ifindex":"VLAN 1","Active":true}},{"key":{"IpNetwork":"192.168.10.0","IpSubnetMaskLength":24,"Protocol":"protoConneted","NextHop":"0.0.0.0"},"val":{"Selected":false,"Metric":5,"Distance":1,"Uptime":0,"Ifindex":"VLAN 1","Active":true}},{"key":{"IpNetwork":"192.168.10.1","IpSubnetMaskLength":8,"Protocol":"protoConneted","NextHop":"0.0.0.0"},"val":{"Selected":false,"Metric":5,"Distance":1,"Uptime":0,"Ifindex":"VLAN 1","Active":true}},{"key":{"IpNetwork":"192.168.10.0","IpSubnetMaskLength":24,"Protocol":"protoConneted","NextHop":"0.0.0.0"},"val":{"Selected":false,"Metric":5,"Distance":1,"Uptime":0,"Ifindex":"VLAN 1","Active":true}},{"key":{"IpNetwork":"192.168.10.0","IpSubnetMaskLength":24,"Protocol":"protoConneted","NextHop":"0.0.0.0"},"val":{"Selected":false,"Metric":5,"Distance":1,"Uptime":0,"Ifindex":"VLAN 1","Active":true}},{"key":{"IpNetwork":"192.168.10.6","IpSubnetMaskLength":18,"Protocol":"protoConneted","NextHop":"192.168.10.5"},"val":{"Selected":true,"Metric":6,"Distance":1,"Uptime":0,"Ifindex":"VLAN 1","Active":true}}]}`);
  } else {
    res.send('{"id":"jsonrpc","result":null,"error":{"code":-32601,"message":"No such method","data":{"vtss-non-matched-method-part":"ge"}}}');
  }
});


module.exports = router;
