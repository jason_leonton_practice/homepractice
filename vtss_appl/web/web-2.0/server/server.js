const express = require('express');
const path = require('path');

const publicDirectoryPath = path.join(__dirname, 'client/build');
const bodyParser = require('body-parser');

// app
const app = express();

//  middlewares
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(publicDirectoryPath));
app.use(express.json());

// import routes
const syslogWeb = require('./routes/syslog_web');
const vtssUsersWeb = require('./routes/vtss_users_web');
const vtssPrivilegeWeb = require('./routes/vtss_privilege_web');
const vtssSshWeb = require('./routes/vtss_ssh_web');
const ddmiWeb = require('./routes/ddmi_web');
const lacpWeb = require('./routes/lacp_web');
const portWeb = require('./routes/port_web');
const systemWeb = require('./routes/system_web');
const firmwareWeb = require('./routes/firmware_web');
const controlWeb = require('./routes/control_web');
const icfgWeb = require('./routes/icfg_web');
const ipWeb = require('./routes/ip_web');
const jsonRpcPlatform = require('./routes/json_rpc_platform.js');
const web = require('./routes/web.js');
const vlanWeb = require('./routes/vlan_web');
const mstpWeb = require('./routes/mstp_web');
const xxrp = require('./routes/xxrp_web');
const loopProtectWeb = require('./routes/loop_protect_web');
const aggrWeb = require('./routes/aggr_web');
const portPowerSavingsWeb = require('./routes/port_power_savings_web');
const dhcpServerWeb = require('./routes/dhcp_server_web');
const poeWeb = require('./routes/poe_web');
const dynaMacWeb = require('./routes/mac_web');
const sflowWeb = require('./routes/sflow_web');
const dhcpRelayWeb = require('./routes/dhcp_relay_web');
const dhcpHelperWeb = require('./routes/dhcp_helper_web');
const ipmcWeb = require('./routes/ipmc_web');
const ModbusConfig = require('./routes/modbus_web');
const accessMgmtWeb = require('./routes/access_mgmt_web');

// routes middleware
app.use('/', web);
app.use('/', syslogWeb);
app.use('/', vtssUsersWeb);
app.use('/config', vtssPrivilegeWeb);
app.use('/config', vtssSshWeb);
app.use('/', ddmiWeb);
app.use('/', lacpWeb);
app.use('/', portWeb);
app.use('/', systemWeb);
app.use('/config', firmwareWeb);
app.use('/config', controlWeb);
app.use('/config', icfgWeb);
app.use('/', ipWeb);
app.use('/json_rpc', jsonRpcPlatform);
app.use('/', vlanWeb);
app.use('/', mstpWeb);
app.use('/', xxrp);
app.use('/', loopProtectWeb);
app.use('/', aggrWeb);
app.use('/', portPowerSavingsWeb);
app.use('/', dhcpServerWeb);
app.use('/', poeWeb);
app.use('/', dynaMacWeb);
app.use('/', sflowWeb);
app.use('/', dhcpRelayWeb);
app.use('/', dhcpHelperWeb);
app.use('/', ipmcWeb);
app.use('/', ModbusConfig);
app.use('/', accessMgmtWeb);

// Anything that doesn't match the above, send back index.html
app.get('*', (req, res) => {
  res.sendFile(path.join(`${__dirname}/client/build/index.html`));
});

const port = process.env.PORT || 5000;

app.listen(port, () => `Server running on port ${port}`);
