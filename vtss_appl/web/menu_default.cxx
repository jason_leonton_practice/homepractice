/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
*/

#include "web_api.h"
#if defined(VTSS_SW_OPTION_FAN)
#include "fan_api.h"
#else
#define fan_module_enabled() (false)
#endif /* VTSS_SW_OPTION_FAN */
#if defined(VTSS_SW_OPTION_LED_POW_REDUC)
#include "led_pow_reduc_api.h"
#else
#define led_pow_reduc_module_enabled() (false)
#endif /* VTSS_SW_OPTION_LED_POW_REDUC */
#if defined(VTSS_SW_OPTION_THERMAL_PROTECT)
#include "thermal_protect_api.h"
#else
#define thermal_protect_module_enabled() (false)
#endif /* VTSS_SW_OPTION_THERMAL_PROTECT */

#include "conf_api.h"


#if defined(VTSS_SW_OPTION_FRR)
#include "frr_access.hxx"
#endif /* VTSS_SW_OPTION_FRR */


int WEB_navbar(CYG_HTTPD_STATE *p, int &size) {
#define ITEM(X) WEB_navbar_menu_item(p, size, X, level)
    int level = -1;

    ITEM("Configuration");
    ITEM(" System");



#if defined(VTSS_SW_OPTION_SYSLOG)
    ITEM("  Information,sysinfo_config.htm");
#endif /* VTSS_SW_OPTION_SYSLOG */
    ITEM("  IP,ip_config.htm");
#if defined(VTSS_SW_OPTION_NTP)
    ITEM("  NTP,ntp.htm");
#endif
#if defined(VTSS_SW_OPTION_DAYLIGHT_SAVING)
    ITEM("  Time,daylight_saving_config.htm");
#endif
#if defined(VTSS_SW_OPTION_SYSLOG)
    ITEM("  Log,syslog_config.htm");
#endif /* VTSS_SW_OPTION_SYSLOG */
#if defined(VTSS_SW_OPTION_EVENT_WARNING)
    ITEM("  EventWarning");
    ITEM("   Relay,event_warning_relay.htm");
    // ITEM("   Email,event_warning_email.htm");
#endif /* VTSS_SW_OPTION_EVENT_WARNING */
#if defined(VTSS_SW_OPTION_LED_POW_REDUC) || defined(VTSS_SW_OPTION_FAN) || \
        defined(VTSS_SW_OPTION_EEE) ||                                      \
        defined(VTSS_SW_OPTION_PHY_POWER_CONTROL)
    ITEM(" Green Ethernet");
    if (fan_module_enabled()) {
        ITEM("  Fan,fan_config.htm");
    }
    if (led_pow_reduc_module_enabled()) {
        ITEM("  LED,led_pow_reduc_config.htm");
    }
#if defined(VTSS_SW_OPTION_PORT_POWER_SAVINGS)
    ITEM("  Port Power Savings,port_power_savings_config.htm");
#endif /* VTSS_SW_OPTION_PORT_POWER_SAVINGS */
#endif /* Green Ethernet */
    if (thermal_protect_module_enabled()) {
        ITEM(" Thermal Protection,thermal_protect_config.htm");
    }
    ITEM(" Ports,ports.htm");
#if defined(VTSS_SW_OPTION_DHCP_HELPER)
    ITEM(" DHCP");
#if defined(VTSS_SW_OPTION_DHCP_SERVER)
    ITEM("  Server");
    ITEM("   Mode,dhcp_server_mode.htm");
    ITEM("   Excluded IP,dhcp_server_excluded.htm");
    ITEM("   Pool,dhcp_server_pool.htm");
#endif /* VTSS_SW_OPTION_DHCP_SERVER */
#if defined(VTSS_SW_OPTION_DHCP_SNOOPING)
    ITEM("  Snooping,dhcp_snooping.htm");
#endif
#if defined(VTSS_SW_OPTION_DHCP_RELAY)
    ITEM("  Relay,dhcp_relay.htm");
#endif
#endif /* VTSS_SW_OPTION_DHCP_HELPER */
    ITEM(" Security");
    ITEM("  Switch");
#if defined(VTSS_SW_OPTION_USERS)
    ITEM("   Users,users.htm");
#else
    ITEM("   Password,passwd_config.htm");
#endif
#if defined(VTSS_SW_OPTION_PRIV_LVL)
    ITEM("   Privilege Levels,priv_lvl.htm");
#endif
#if defined(VTSS_SW_OPTION_AUTH)
    ITEM("   Auth Method,auth_method_config.htm");
#endif
#if defined(VTSS_SW_OPTION_SSH)
    ITEM("   SSH,ssh_config.htm");
#endif
#if defined(VTSS_SW_OPTION_HTTPS) || defined(VTSS_SW_OPTION_FAST_CGI)
    ITEM("   HTTPS,https_config.htm");
#endif
#if defined(VTSS_SW_OPTION_ACCESS_MGMT)
    ITEM("   Access Management,access_mgmt.htm");
#endif
#if defined(VTSS_SW_OPTION_SNMP)
    ITEM("   SNMP,,snmp_menu");
    ITEM("    System,snmp.htm");
#if defined(VTSS_SW_OPTION_JSON_RPC)
    ITEM("    Trap");
    ITEM("     Destinations,trap.htm");
    ITEM("     Sources,trap_source.htm");
#endif /* VTSS_SW_OPTION_JSON_RPC */
    ITEM("    Communities,snmpv3_communities.htm");
    ITEM("    Users,snmpv3_users.htm");
    ITEM("    Groups,snmpv3_groups.htm");
    ITEM("    Views,snmpv3_views.htm");
    ITEM("    Access,snmpv3_accesses.htm");
#if defined(VTSS_SW_OPTION_RMON)
    ITEM("   RMON");
    ITEM("    Statistics,rmon_statistics_config.htm");
    ITEM("    History,rmon_history_config.htm");
    ITEM("    Alarm,rmon_alarm_config.htm");
    ITEM("    Event,rmon_event_config.htm");
#endif /* VTSS_SW_OPTION_RMON */
#endif /* VTSS_SW_OPTION_SNMP */
    ITEM("  Network");
#if defined(VTSS_SW_OPTION_PSEC_LIMIT)
    ITEM("   Port Security,psec_limit.htm");
#endif
#if defined(VTSS_SW_OPTION_DOT1X)
    ITEM("   NAS,nas.htm");
#endif
#if defined(VTSS_SW_OPTION_ACL)
    ITEM("   ACL");
    ITEM("    Ports,acl_ports.htm");
    ITEM("    Rate Limiters,acl_ratelimiter.htm");
    ITEM("    Access Control List,acl.htm");
#endif
#if defined(VTSS_SW_OPTION_IP_SOURCE_GUARD)
    ITEM("   IP Source Guard");
    ITEM("    Configuration,ip_source_guard.htm");
    ITEM("    Static Table,ip_source_guard_static_table.htm");
#endif
#if defined(VTSS_SW_OPTION_ARP_INSPECTION)
    ITEM("   ARP Inspection");
    ITEM("    Port Configuration,arp_inspection.htm");
    ITEM("    VLAN Configuration,arp_inspection_vlan.htm");
    ITEM("    Static Table,arp_inspection_static_table.htm");
    ITEM("    Dynamic Table,dynamic_arp_inspection.htm");
#endif
#if defined(VTSS_SW_OPTION_AUTH)
#if defined(VTSS_SW_OPTION_RADIUS) || defined(VTSS_SW_OPTION_TACPLUS)
    ITEM("  AAA");
#endif /* defined(VTSS_SW_OPTION_RADIUS) || defined(VTSS_SW_OPTION_TACPLUS) */
#if defined(VTSS_SW_OPTION_RADIUS)
    ITEM("   RADIUS,auth_radius_config.htm");
#endif /* defined(VTSS_SW_OPTION_RADIUS) */
#if defined(VTSS_SW_OPTION_TACPLUS)
    ITEM("   TACACS+,auth_tacacs_config.htm");
#endif /* defined(VTSS_SW_OPTION_TACPLUS) */
#endif /* defined(VTSS_SW_OPTION_AUTH) */
#if defined(VTSS_SW_OPTION_AGGR) || defined(VTSS_SW_OPTION_LACP)
    ITEM(" Aggregation");
#if defined(VTSS_SW_OPTION_AGGR)
    ITEM("  Common,aggr_common.htm");
    ITEM("  Groups,aggr_groups.htm");
#endif
#if defined(VTSS_SW_OPTION_LACP)
    ITEM("  LACP,lacp_port_config.htm");
#endif
#endif /* defined(VTSS_SW_OPTION_AGGR) || defined(VTSS_SW_OPTION_LACP) */
#if defined(VTSS_SW_OPTION_ETH_LINK_OAM)
    ITEM(" Link OAM");
    ITEM("  Port Settings,eth_link_oam_port_config.htm");
    ITEM("  Event Settings,eth_link_oam_link_event_config.htm");
#endif
#if defined(VTSS_SW_OPTION_LOOP_PROTECTION)
    ITEM(" Loop Protection,loop_config.htm");
#endif /* defined(VTSS_SW_OPTION_LOOP_PROTECTION) */
#if defined(VTSS_SW_OPTION_MSTP)
#if defined(VTSS_SW_OPTION_BUILD_SMB) || defined(VTSS_SW_OPTION_BUILD_CE)
#define VTSS_MSTP_FULL 1
#endif
    ITEM(" Spanning Tree");
    ITEM("  Bridge Settings,mstp_sys_config.htm");
#if defined(VTSS_MSTP_FULL)
    ITEM("  MSTI Mapping,mstp_msti_map_config.htm");
    ITEM("  MSTI Priorities,mstp_msti_config.htm");
    ITEM("  CIST Ports,mstp_port_config.htm");
    ITEM("  MSTI Ports,mstp_msti_port_config.htm");
#else
    ITEM("  Bridge Ports,mstp_port_config.htm");
#endif  // VTSS_MSTP_FULL
#endif  // MSTP
#if ((defined(VTSS_SW_OPTION_SMB_IPMC) || defined(VTSS_SW_OPTION_MVR)) && \
     defined(VTSS_SW_OPTION_IPMC_LIB))
    ITEM(" IPMC Profile");
    ITEM("  Profile Table,ipmc_lib_profile_table.htm");
    ITEM("  Address Entry,ipmc_lib_entry_table.htm");
#endif /* (VTSS_SW_OPTION_SMB_IPMC || VTSS_SW_OPTION_MVR) && VTSS_SW_OPTION_IPMC_LIB */
#if defined(VTSS_SW_OPTION_MVR)
    ITEM(" MVR,mvr.htm");
#endif /* VTSS_SW_OPTION_MVR */
#if defined(VTSS_SW_OPTION_IPMC)
    ITEM(" IPMC");
    ITEM("  IGMP Snooping");
    ITEM("   Basic Configuration,ipmc_igmps.htm");
    ITEM("   VLAN Configuration,ipmc_igmps_vlan.htm");
#if defined(VTSS_SW_OPTION_SMB_IPMC)
    ITEM("   Port Filtering Profile,ipmc_igmps_filtering.htm");
    ITEM("  MLD Snooping");
    ITEM("   Basic Configuration,ipmc_mldsnp.htm");
    ITEM("   VLAN Configuration,ipmc_mldsnp_vlan.htm");
    ITEM("   Port Filtering Profile,ipmc_mldsnp_filtering.htm");
#endif /* VTSS_SW_OPTION_SMB_IPMC */
#endif /* VTSS_SW_OPTION_IPMC */
#if defined(VTSS_SW_OPTION_LLDP)
    ITEM(" LLDP");
    ITEM("  LLDP,lldp_config.htm");
#if defined(VTSS_SW_OPTION_LLDP_MED)
    ITEM("  LLDP-MED,lldp_med_config.htm");
#endif
#endif
#if defined(VTSS_SW_OPTION_POE)
    ITEM(" PoE,,poe_menu");
    ITEM("  Power Budget,poe_config.htm");
    ITEM("  Ping Alive,poe_ping_alive.htm");
    ITEM("  Schedule,poe_schedule.htm");
#endif
#if defined(VTSS_SW_OPTION_SYNCE)
    ITEM(" SyncE,synce_config.htm");
#endif
#if defined(VTSS_SW_OPTION_EPS)
    ITEM(" EPS,eps.htm");
#endif
#if defined(VTSS_SW_OPTION_MEP)
    ITEM(" MEP,mep.htm");
#endif
#if defined(VTSS_SW_OPTION_ERPS)
    ITEM(" ERPS,erps.htm");
#endif
#if defined(VTSS_SW_OPTION_MAC)
    ITEM(" MAC Table,mac.htm");
#endif /* VTSS_SW_OPTION_MAC */
#if defined(VTSS_SW_OPTION_VLAN)
    if (mesa_capability(0, MESA_CAP_L2_SVL_FID_CNT)) {
        ITEM(" VLANs");
        ITEM("  Configuration,vlan.htm");
        ITEM("  SVL,vlan_svl.htm");
    } else {
        ITEM(" VLANs,vlan.htm");
    }
#endif /* VTSS_SW_OPTION_VLAN  */
#if defined(VTSS_SW_OPTION_VLAN_TRANSLATION)
    ITEM(" VLAN Translation");
    ITEM("  Port to Group Configuration,vlan_trans_port_config.htm");
    ITEM("  VLAN Translation Mappings,map.htm");
#endif /* VTSS_SW_OPTION_VLAN_TRANSLATION */
#if defined(VTSS_SW_OPTION_PVLAN)
    ITEM(" Private VLANs");
#if !defined(VTSS_PERSONALITY_STACKABLE)
    ITEM("  Membership,pvlan.htm");
#endif /* !defined(VTSS_PERSONALITY_STACKABLE) */
    ITEM("  Port Isolation,port_isolation.htm");
#endif /* VTSS_SW_OPTION_PVLAN */
#if defined(VTSS_SW_OPTION_VCL)
    ITEM(" VCL");
    ITEM("  MAC-based VLAN,mac_based_vlan.htm");
    ITEM("  Protocol-based VLAN");
    ITEM("   Protocol to Group,vcl_protocol_grp_map.htm");
    ITEM("   Group to VLAN,vcl_grp_2_vlan_mapping.htm");
    ITEM("  IP Subnet-based VLAN,subnet_based_vlan.htm");
#endif /* VTSS_SW_OPTION_VCL */
#if defined(VTSS_SW_OPTION_VOICE_VLAN)
    ITEM(" Voice VLAN");
    ITEM("  Configuration,voice_vlan_config.htm");
    ITEM("  OUI,voice_vlan_oui.htm");
#endif /* VTSS_SW_OPTION_VOICE_VLAN */
#if defined(VTSS_SW_OPTION_QOS)
    ITEM(" QoS");
    ITEM("  Port Classification,qos_port_classification.htm");
    ITEM("  Port Policing,qos_port_policers.htm");
#if defined(VTSS_SW_OPTION_BUILD_SMB) || defined(VTSS_SW_OPTION_BUILD_CE)
    ITEM("  Queue Policing,qos_queue_policers.htm");
#endif /* defined(VTSS_SW_OPTION_BUILD_SMB) || defined(VTSS_SW_OPTION_BUILD_CE) */
    ITEM("  Port Scheduler,qos_port_schedulers.htm");
    ITEM("  Port Shaping,qos_port_shapers.htm");
#if defined(VTSS_SW_OPTION_BUILD_SMB) || defined(VTSS_SW_OPTION_BUILD_CE)
    ITEM("  Port Tag Remarking,qos_port_tag_remarking.htm");
    ITEM("  Port DSCP,qos_port_dscp_config.htm");
    ITEM("  DSCP-Based QoS,dscp_based_qos_ingr_classifi.htm");
    ITEM("  DSCP Translation,dscp_translation.htm");
    ITEM("  DSCP&nbsp;Classification,dscp_classification.htm");
    if (mesa_capability(0, MESA_CAP_QOS_INGRESS_MAP_CNT)) {
        ITEM("  Ingress Map,qos_ingress_map.htm");
    }
    if (mesa_capability(0, MESA_CAP_QOS_EGRESS_MAP_CNT)) {
        ITEM("  Egress Map,qos_egress_map.htm");
    }
#endif /* defined(VTSS_SW_OPTION_BUILD_SMB) || defined(VTSS_SW_OPTION_BUILD_CE) */
    ITEM("  QoS Control List,qcl_v2.htm");
    ITEM("  Storm Policing,stormctrl.htm");
    if (mesa_capability(0, MESA_CAP_QOS_WRED_GROUP_CNT)) {
        ITEM("  WRED,qos_wred.htm");
    }
#if (defined(VTSS_SW_OPTION_BUILD_SMB) || \
     defined(VTSS_SW_OPTION_BUILD_ISTAX) || defined(VTSS_SW_OPTION_BUILD_CE))
    if (mesa_capability(0, MESA_CAP_QOS_FRAME_PREEMPTION)) {
        ITEM("  Frame Preemption,qos_port_fp_config.htm");
    }
    if (mesa_capability(0, MESA_CAP_QOS_EGRESS_QUEUE_SHAPERS_TAS)) {
        ITEM("  TAS");
        ITEM("   Ports,qos_port_tas_config.htm");
        ITEM("   Max SDU,qos_port_tas_max_sdu.htm");
    }
    if (mesa_capability(0, MESA_CAP_QOS_PSFP)) {
        ITEM("  PSFP");
        ITEM("   FMI,qos_psfp_fmi_config.htm");
        ITEM("   SFI,qos_psfp_sfi_config.htm");
        ITEM("   SGI,qos_psfp_sgi_config.htm");
    }
#endif
#endif /* VTSS_SW_OPTION_QOS */

#if defined(VTSS_SW_OPTION_MIRROR) && defined(VTSS_SW_OPTION_JSON_RPC)
#if defined(VTSS_SW_OPTION_MIRROR_LOOP_PORT)
    ITEM(" Mirroring,mirror_no_reflector_port.htm");
#else
    ITEM(" Mirroring,mirror_ctrl.htm");
#endif
#endif /* VTSS_SW_OPTION_RMIRROR  && VTSS_SW_OPTION_JSON_RPC */
#if defined(VTSS_SW_OPTION_UPNP)
    ITEM(" UPnP,upnp.htm");
#endif /* VTSS_SW_OPTION_UPNP */
#if defined(VTSS_SW_OPTION_PTP)
    ITEM(" PTP,ptp_config.htm");
#endif /* VTSS_SW_OPTION_PTP */
#if defined(VTSS_SW_OPTION_MRP)
    ITEM(" MRP");
    ITEM("  Ports,mrp_port.htm");
#if defined(VTSS_SW_OPTION_MVRP)
    ITEM("  MVRP,mvrp_config.htm");
#endif /* VTSS_SW_OPTION_MVRP */
#endif /* VTSS_SW_OPTION_MRP */
#if defined(VTSS_SW_OPTION_GVRP)
    ITEM(" GVRP");
    ITEM("  Global config,gvrp_config.htm");
    ITEM("  Port config,gvrp_port.htm");
#endif /* VTSS_SW_OPTION_GVRP */
#if defined(VTSS_PERSONALITY_STACKABLE)
    ITEM(" Stack,stack_config.htm");
#endif /* VTSS_PERSONALITY_STACKABLE */
#if defined(VTSS_SW_OPTION_SFLOW)
    ITEM(" sFlow,sflow.htm");
#endif /* VTSS_SW_OPTION_SFLOW */
#if defined(VTSS_SW_OPTION_DDMI)
    if (lntn_conf_mgmt_sfp_count_get()) {
        ITEM(" DDMI,ddmi_config.htm");
    }
#endif /* VTSS_SW_OPTION_DDMI */
#if defined(VTSS_SW_OPTION_UDLD)
    ITEM(" UDLD,udld_config.htm");
#endif

#if defined(VTSS_SW_OPTION_MODBUS)
    ITEM(" MODBUS TCP,modbus_config.htm");
#endif /* VTSS_SW_OPTION_MODBUS */

#if defined(VTSS_SW_OPTION_FRR)
    if (vtss::frr_has_ospfd()) {
        ITEM(" OSPF");
        ITEM("  Configuration,frr_ospf_global_config.htm");
        ITEM("  Network Area,frr_ospf_network_area_config.htm");
        ITEM("  Passive Interface,frr_ospf_passive_intf_config.htm");
        ITEM("  Stub Area,frr_ospf_stub_area_config.htm");
        ITEM("  Area Authentication,frr_ospf_area_auth_config.htm");
        ITEM("  Area Range,frr_ospf_area_range_config.htm");
        ITEM("  Interfaces,frr_ospf_intf_config.htm");
        ITEM("  Virtual Link,frr_ospf_vlink_config.htm");
    }
#endif /* VTSS_SW_OPTION_FRR */

#if defined(VTSS_SW_OPTION_SR) && (defined(VTSS_SW_OPTION_BUILD_SMB) ||   \
                                   defined(VTSS_SW_OPTION_BUILD_ISTAX) || \
                                   defined(VTSS_SW_OPTION_BUILD_CE))
    ITEM(" Seamless Redundancy");
    ITEM("  Stream configuration,sr_stream_config.htm");
    ITEM("  Latent Error Configuration,sr_stream_latent_error_config.htm");
#endif /* if defined(VTSS_SW_OPTION_SR) && ......*/

    ITEM("Monitor");
    ITEM(" System");
    ITEM("  Information,sys.htm");
    ITEM("  LED status,sys_led_status.htm");
    ITEM("  CPU Load,perf_cpuload.htm");
#if defined(VTSS_SW_OPTION_IP)
    ITEM("  IP Status,ip_status.htm");
#endif
#if defined(VTSS_SW_OPTION_FRR)
    ITEM("  Routing Info. Base,ip_routing_info_base_status.htm");
#endif
#if defined(VTSS_SW_OPTION_SYSLOG)
    ITEM("  Log,syslog.htm");
    ITEM("  Detailed Log,syslog_detailed.htm");
#endif /* VTSS_SW_OPTION_SYSLOG */
#if defined(VTSS_SW_OPTION_PSU)
    ITEM("  Power Supply,sys_psu_status.htm");
#endif /* VTSS_SW_OPTION_PSU */
#if defined(VTSS_SW_OPTION_PORT_POWER_SAVINGS) || \
        defined(VTSS_SW_OPTION_FAN) || defined(VTSS_SW_OPTION_EEE)
    ITEM(" Green Ethernet");
#if defined(VTSS_SW_OPTION_PORT_POWER_SAVINGS)
    ITEM("  Port Power Savings,port_power_savings_status.htm");
#endif /* VTSS_SW_OPTION_PORT_POWER_SAVINGS */
    if (fan_module_enabled()) {
        ITEM("  Fan,fan_status.htm");
    }
#endif
    if (thermal_protect_module_enabled()) {
        ITEM(" Thermal Protection,thermal_protect_status.htm");
    }
    ITEM(" Ports");
    ITEM("  State,main.htm");
    ITEM("  Traffic Overview,stat_ports.htm");
#if defined(VTSS_SW_OPTION_QOS)
    ITEM("  QoS Statistics,qos_counter.htm");
#if defined(VTSS_SW_OPTION_QOS_QCL_INCLUDE)
#if VTSS_SW_OPTION_QOS_QCL_INCLUDE == 1
    ITEM("  QCL Status,qcl_v2_stat.htm");
#endif /* VTSS_SW_OPTION_QOS_QCL_INCLUDE == 1 */
#else
    ITEM("  QCL Status,qcl_v2_stat.htm");
#endif /* defined(VTSS_SW_OPTION_QOS_QCL_INCLUDE) */
#endif /* defined(VTSS_SW_OPTION_QOS) */
    ITEM("  Detailed Statistics,stat_detailed.htm");
#if defined(VTSS_SW_OPTION_ETH_LINK_OAM)
    ITEM(" Link OAM");
    ITEM("  Statistics,eth_link_oam_stat_detailed.htm");
    ITEM("  Port Status,eth_link_oam_port_status.htm");
    ITEM("  Event Status,eth_link_oam_link_status.htm");
#endif
#if defined(VTSS_SW_OPTION_DHCP_HELPER)
    ITEM(" DHCP");
#if defined(VTSS_SW_OPTION_DHCP_SERVER)
    ITEM("  Server");
    ITEM("   Statistics,dhcp_server_stat.htm");
    ITEM("   Binding,dhcp_server_stat_binding.htm");
    ITEM("   Declined IP,dhcp_server_stat_declined.htm");
#endif /* VTSS_SW_OPTION_DHCP_SERVER */
#if defined(VTSS_SW_OPTION_DHCP_SNOOPING)
    ITEM("  Snooping Table,dyna_dhcp_snooping.htm");
#endif
#if defined(VTSS_SW_OPTION_DHCP_RELAY)
    ITEM("  Relay Statistics,dhcp_relay_statistics.htm");
#endif /* VTSS_SW_OPTION_DHCP_RELAY */
    ITEM("  Detailed Statistics,dhcp_helper_statistics.htm");
#endif /* VTSS_SW_OPTION_DHCP_HELPER */
    ITEM(" Security");
#if defined(VTSS_SW_OPTION_ACCESS_MGMT)
    ITEM("  Access Management Statistics,access_mgmt_statistics.htm");
#endif
    ITEM("  Network");
#if defined(VTSS_SW_OPTION_PSEC)
    ITEM("   Port Security");
    ITEM("    Overview,psec_status_switch.htm");
    ITEM("    Details,psec_status_port.htm");
#endif
#if defined(VTSS_SW_OPTION_DOT1X)
    ITEM("   NAS");
    ITEM("    Switch,nas_status_switch.htm");
    ITEM("    Port,nas_status_port.htm");
#endif
#if defined(VTSS_SW_OPTION_ACL)
    ITEM("   ACL Status,acl_status.htm");
#endif /* VTSS_SW_OPTION_ACL */
#if defined(VTSS_SW_OPTION_ARP_INSPECTION)
    ITEM("   ARP Inspection,dyna_arp_inspection.htm");
#endif /* VTSS_SW_OPTION_ARP_INSPECTION */
#if defined(VTSS_SW_OPTION_IP_SOURCE_GUARD)
    ITEM("   IP Source Guard,dyna_ip_source_guard.htm");
#endif /* VTSS_SW_OPTION_IP_SOURCE_GUARD */
#if defined(VTSS_SW_OPTION_RADIUS)
    ITEM("  AAA");
    ITEM("   RADIUS Overview,auth_status_radius_overview.htm");
    ITEM("   RADIUS Details,auth_status_radius_details.htm");
#endif /* VTSS_SW_OPTION_RADIUS */
#if defined(VTSS_SW_OPTION_RMON)
    ITEM("  Switch");
    ITEM("   RMON");
    ITEM("    Statistics,rmon_statistics_status.htm");
    ITEM("    History,rmon_history_status.htm");
    ITEM("    Alarm,rmon_alarm_status.htm");
    ITEM("    Event,rmon_event_status.htm");
#endif /* VTSS_SW_OPTION_RMON */
#if defined(VTSS_SW_OPTION_AGGR) || defined(VTSS_SW_OPTION_LACP)
    ITEM(" Aggregation");
#if defined(VTSS_SW_OPTION_AGGR)
    ITEM("  Status,aggregation_status.htm");
#endif
#if defined(VTSS_SW_OPTION_LACP)
    ITEM("  LACP");
    ITEM("   System Status,lacp_sys_status.htm");
    ITEM("   Internal Status,lacp_internal_status.htm");
    ITEM("   Neighbor Status,lacp_neighbor_status.htm");
    ITEM("   Port Statistics,lacp_statistics.htm");
#endif
#endif /* defined(VTSS_SW_OPTION_AGGR) || defined(VTSS_SW_OPTION_LACP) */
#if defined(VTSS_SW_OPTION_LOOP_PROTECTION)
    ITEM(" Loop Protection,loop_status.htm");
#endif /* defined(VTSS_SW_OPTION_LOOP_PROTECTION) */
#if defined(VTSS_SW_OPTION_MSTP)
    ITEM(" Spanning Tree");
#if defined(VTSS_MSTP_FULL)
    ITEM("  Bridge Status,mstp_status_bridges.htm");
#else
    ITEM("  Bridge Status,mstp_status_bridge.htm");
#endif
    ITEM("  Port Status,mstp_status_ports.htm");
    ITEM("  Port Statistics,mstp_statistics.htm");
#endif /* VTSS_SW_OPTION_MSTP */
#if defined(VTSS_SW_OPTION_MVR)
    ITEM(" MVR");
    ITEM("  Statistics,mvr_status.htm");
    ITEM("  MVR Channel Groups,mvr_groups_info.htm");
    ITEM("  MVR SFM Information,mvr_groups_sfm.htm");
#endif /* VTSS_SW_OPTION_MVR */
#if defined(VTSS_SW_OPTION_IPMC)
    ITEM(" IPMC");
    ITEM("  IGMP Snooping");
    ITEM("   Status,ipmc_igmps_status.htm");
    ITEM("   Groups Information,ipmc_igmps_groups_info.htm");
#if defined(VTSS_SW_OPTION_SMB_IPMC)
    ITEM("   IPv4 SFM Information,ipmc_igmps_v3info.htm");
    ITEM("  MLD Snooping");
    ITEM("   Status,ipmc_mldsnp_status.htm");
    ITEM("   Groups Information,ipmc_mldsnp_groups_info.htm");
    ITEM("   IPv6 SFM Information,ipmc_mldsnp_v2info.htm");
#endif /* VTSS_SW_OPTION_SMB_IPMC */
#endif /* VTSS_SW_OPTION_IPMC */

#if defined(VTSS_SW_OPTION_LLDP)
    ITEM(" LLDP");
    ITEM("  Neighbors,lldp_neighbors.htm");
#if defined(VTSS_SW_OPTION_LLDP_MED)
    ITEM("  LLDP-MED Neighbors,lldp_neighbors_med.htm");
#endif /* VTSS_SW_OPTION_LLDP_MED */
#if defined(VTSS_SW_OPTION_POE)
    ITEM("  PoE,lldp_poe_neighbors.htm");
#endif /* VTSS_SW_OPTION_POE */
#if defined(VTSS_SW_OPTION_EEE)
    ITEM("  EEE,lldp_eee_neighbors.htm");
#endif
    ITEM("  Port Statistics,lldp_statistic.htm");
#endif /* VTSS_SW_OPTION_LLDP */
#if defined(VTSS_SW_OPTION_PTP)
    ITEM(" PTP");
    ITEM("  PTP,ptp.htm");
#endif /* VTSS_SW_OPTION_PTP */
#if defined(VTSS_SW_OPTION_POE)
    ITEM(" PoE,poe_status.htm");
#endif /* VTSS_SW_OPTION_POE */
#if defined(VTSS_SW_OPTION_MAC)
    ITEM(" MAC Table,dyna_mac.htm");
#endif /* VTSS_SW_OPTION_MAC */
#if defined(VTSS_SW_OPTION_VLAN)
    ITEM(" VLANs");
    ITEM("  Membership,vlan_membership_stat.htm");
    ITEM("  Ports,vlan_port_stat.htm");
#endif /* VTSS_SW_OPTION_VLAN */
#if defined(VTSS_SW_OPTION_MRP)
#endif /* VTSS_SW_OPTION_MRP */
#if defined(VTSS_SW_OPTION_MVRP)
    ITEM(" MVRP,mvrp_stat.htm");
#endif /* VTSS_SW_OPTION_MVRP */
#if defined(VTSS_PERSONALITY_STACKABLE)
    ITEM(" Stack,stackstate.htm");
#endif /* VTSS_PERSONALITY_STACKABLE */
#if defined(VTSS_SW_OPTION_SFLOW)
    ITEM(" sFlow,sflow_status.htm");
#endif /* VTSS_SW_OPTION_SFLOW */
#if defined(VTSS_SW_OPTION_DDMI)
    if (lntn_conf_mgmt_sfp_count_get()) {
        ITEM(" DDMI");
        ITEM("  Overview,ddmi_overview.htm");
        ITEM("  Detailed,ddmi_detailed.htm");
    }
#endif /* VTSS_SW_OPTION_DDMI */
#if defined(VTSS_SW_OPTION_UDLD)
    ITEM(" UDLD,udld_status.htm");
#endif
#if defined(VTSS_SW_OPTION_BUILD_SMB) || \
        defined(VTSS_SW_OPTION_BUILD_ISTAX) || defined(VTSS_SW_OPTION_BUILD_CE)
    if (mesa_capability(0, MESA_CAP_QOS_FRAME_PREEMPTION) ||
        mesa_capability(0, MESA_CAP_QOS_EGRESS_QUEUE_SHAPERS_TAS)) {
        ITEM(" QOS");
    }
    if (mesa_capability(0, MESA_CAP_QOS_FRAME_PREEMPTION)) {
        ITEM("  Frame Preemption,qos_port_fp_status.htm");
    }
    if (mesa_capability(0, MESA_CAP_QOS_EGRESS_QUEUE_SHAPERS_TAS)) {
        ITEM("  TAS");
        ITEM("   Status,qos_port_tas_status.htm");
    }
    if (mesa_capability(0, MESA_CAP_QOS_PSFP)) {
        ITEM("  PSFP");
        ITEM("   SPT Status,qos_psfp_stream_parameters_status.htm");
        ITEM("   SFI Statistics,qos_psfp_sfi_statistics.htm");
        ITEM("   SGI Status,qos_psfp_sgi_status.htm");
    }

#if defined(VTSS_SW_OPTION_FRR)
    if (vtss::frr_has_ospfd()) {
        ITEM(" OSPF");
        ITEM("  Status,frr_ospf_global_status.htm");
        ITEM("  Area,frr_ospf_area_status.htm");
        ITEM("  Neighbor,frr_ospf_nbr_status.htm");
        ITEM("  Interface,frr_ospf_intf_status.htm");
    }
#endif /* VTSS_SW_OPTION_FRR */

#endif /* defined(VTSS_SW_OPTION_BUILD_SMB) || defined(VTSS_SW_OPTION_BUILD_ISTAX) \
          || defined(VTSS_SW_OPTION_BUILD_CE) */

    ITEM("Diagnostics");
    ITEM(" Ping (IPv4),ping4.htm");
#if defined(VTSS_SW_OPTION_IPV6)
    ITEM(" Ping (IPv6),ping6.htm");
#endif /* VTSS_SW_OPTION_IPV6 */
    ITEM(" Traceroute (IPv4),traceroute4.htm");
#if defined(VTSS_SW_OPTION_IPV6)
    ITEM(" Traceroute (IPv6),traceroute6.htm");
#endif /* VTSS_SW_OPTION_IPV6 */
#if defined(VTSS_SW_OPTION_ETH_LINK_OAM)
    ITEM(" Link OAM");
    ITEM("  MIB Retrieval,eth_link_oam_mib_support.htm");
#endif
    ITEM(" VeriPHY,veriphy.htm");
    ITEM("Maintenance");
    ITEM(" Restart Device,wreset.htm");
    ITEM(" Factory Defaults,factory.htm");
#if defined(VTSS_PERSONALITY_STACKABLE)
    ITEM(" Software Upload,upload.htm");
#else
    ITEM(" Software");
    ITEM("  Upload,upload.htm");
    ITEM("  Image Select,sw_select.htm");
#endif
#if defined(VTSS_SW_OPTION_ICFG)
    ITEM(" Configuration");
    ITEM("  Save startup-config,icfg_conf_save.htm");
    ITEM("  Download,icfg_conf_download.htm");
    ITEM("  Upload,icfg_conf_upload.htm");
    ITEM("  Activate,icfg_conf_activate.htm");
    ITEM("  Delete,icfg_conf_delete.htm");
#endif /* VTSS_SW_OPTION_ICFG*/

    return level;
}

int WEB_navbar_json(CYG_HTTPD_STATE *p, int &size) {
#define GEN_JSON_ITEM(X) WEB_navbar_menu_item_json(p, size, X, level)
    int level = -1;
    GEN_JSON_ITEM("configuration");
    GEN_JSON_ITEM(" configuration_system");

#if defined(VTSS_SW_OPTION_SYSLOG)
    GEN_JSON_ITEM("  sysinfo_config");
#endif /* VTSS_SW_OPTION_SYSLOG */
    GEN_JSON_ITEM("  ip_config");
#if defined(VTSS_SW_OPTION_NTP)
    GEN_JSON_ITEM("  ntp");
#endif
#if defined(VTSS_SW_OPTION_DAYLIGHT_SAVING)
    GEN_JSON_ITEM("  daylight_saving_config");
#endif
#if defined(VTSS_SW_OPTION_SYSLOG)
    GEN_JSON_ITEM("  syslog_config");
#endif /* VTSS_SW_OPTION_SYSLOG */
#if defined(VTSS_SW_OPTION_EVENT_WARNING)
    GEN_JSON_ITEM("  configuration_system_event_warning");
    GEN_JSON_ITEM("   event_warning_relay");
    // GEN_JSON_ITEM("   event_warning_email");
#endif /* VTSS_SW_OPTION_EVENT_WARNING */
#if defined(VTSS_SW_OPTION_LED_POW_REDUC) || defined(VTSS_SW_OPTION_FAN) || \
        defined(VTSS_SW_OPTION_EEE) || defined(VTSS_SW_OPTION_PHY_POWER_CONTROL)

    GEN_JSON_ITEM(" configuration_green_ethernet");

    if (fan_module_enabled()) {
        GEN_JSON_ITEM("  fan_config");
    }

    if (led_pow_reduc_module_enabled()) {
        GEN_JSON_ITEM("  led_pow_reduc_config");
    }
#if defined(VTSS_SW_OPTION_PORT_POWER_SAVINGS)
    GEN_JSON_ITEM("  port_power_savings_config");
#endif /* VTSS_SW_OPTION_PORT_POWER_SAVINGS */
#endif /* Green Ethernet */
    if (thermal_protect_module_enabled()) {
        GEN_JSON_ITEM(" thermal_protect_config");
    }

    GEN_JSON_ITEM(" ports");
#if defined(VTSS_SW_OPTION_DHCP_HELPER)
    GEN_JSON_ITEM(" configuration_dhcp");
#if defined(VTSS_SW_OPTION_DHCP_SERVER)
    GEN_JSON_ITEM("  configuration_dhcp_server");
    GEN_JSON_ITEM("   dhcp_server_mode");
    GEN_JSON_ITEM("   dhcp_server_excluded");
    GEN_JSON_ITEM("   dhcp_server_pool");
#endif /* VTSS_SW_OPTION_DHCP_SERVER */
#if defined(VTSS_SW_OPTION_DHCP_SNOOPING)
    GEN_JSON_ITEM("  dhcp_snooping");
#endif
#if defined(VTSS_SW_OPTION_DHCP_RELAY)
    GEN_JSON_ITEM("  dhcp_relay");
#endif
#endif /* VTSS_SW_OPTION_DHCP_HELPER */
    GEN_JSON_ITEM(" configuration_security");
    GEN_JSON_ITEM("  configuration_security_switch");
#if defined(VTSS_SW_OPTION_USERS)
    GEN_JSON_ITEM("   users");
    GEN_JSON_ITEM("   user_config");
#else
    GEN_JSON_ITEM("   passwd_config");
#endif
#if defined(VTSS_SW_OPTION_PRIV_LVL)
    GEN_JSON_ITEM("   priv_lvl");
#endif
#if defined(VTSS_SW_OPTION_AUTH)
    GEN_JSON_ITEM("   auth_method_config");
#endif
#if defined(VTSS_SW_OPTION_SSH)
    GEN_JSON_ITEM("   ssh_config");
#endif
#if defined(VTSS_SW_OPTION_HTTPS) || defined(VTSS_SW_OPTION_FAST_CGI)
    GEN_JSON_ITEM("   https_config");
#endif
#if defined(VTSS_SW_OPTION_ACCESS_MGMT)
    GEN_JSON_ITEM("   access_mgmt");
#endif
#if defined(VTSS_SW_OPTION_SNMP)
    GEN_JSON_ITEM("   configuration_security_switch_snmp");
    GEN_JSON_ITEM("    snmp");
#if defined(VTSS_SW_OPTION_JSON_RPC)
    GEN_JSON_ITEM("    configuration_security_switch_snmp_trap");
    GEN_JSON_ITEM("     trap");
    GEN_JSON_ITEM("     trap_source");
#endif /* VTSS_SW_OPTION_JSON_RPC */
    GEN_JSON_ITEM("    snmpv3_communities");
    GEN_JSON_ITEM("    snmpv3_users");
    GEN_JSON_ITEM("    snmpv3_groups");
    GEN_JSON_ITEM("    snmpv3_views");
    GEN_JSON_ITEM("    snmpv3_accesses");
#if defined(VTSS_SW_OPTION_RMON)
    GEN_JSON_ITEM("   configuration_security_switch_rmon");
    GEN_JSON_ITEM("    rmon_statistics_config");
    GEN_JSON_ITEM("    rmon_history_config");
    GEN_JSON_ITEM("    rmon_alarm_config");
    GEN_JSON_ITEM("    rmon_event_config");
#endif /* VTSS_SW_OPTION_RMON */
#endif /* VTSS_SW_OPTION_SNMP */
    GEN_JSON_ITEM("  configuration_security_network");
#if defined(VTSS_SW_OPTION_PSEC_LIMIT)
    GEN_JSON_ITEM("   psec_limit");
#endif
#if defined(VTSS_SW_OPTION_DOT1X)
    GEN_JSON_ITEM("   nas");
#endif
#if defined(VTSS_SW_OPTION_ACL)
    GEN_JSON_ITEM("   configuration_security_network_acl");
    GEN_JSON_ITEM("    acl_ports");
    GEN_JSON_ITEM("    acl_ratelimiter");
    GEN_JSON_ITEM("    acl");
#endif
#if defined(VTSS_SW_OPTION_IP_SOURCE_GUARD)
    GEN_JSON_ITEM("   configuration_security_network_ip_source_guard");
    GEN_JSON_ITEM("    ip_source_guard");
    GEN_JSON_ITEM("    ip_source_guard_static_table");
#endif
#if defined(VTSS_SW_OPTION_ARP_INSPECTION)
    GEN_JSON_ITEM("   configuration_security_network_ip_arp_inspection");
    GEN_JSON_ITEM("    arp_inspection");
    GEN_JSON_ITEM("    arp_inspection_vlan");
    GEN_JSON_ITEM("    arp_inspection_static_table");
    GEN_JSON_ITEM("    dynamic_arp_inspection");
#endif
#if defined(VTSS_SW_OPTION_AUTH)
#if defined(VTSS_SW_OPTION_RADIUS) || defined(VTSS_SW_OPTION_TACPLUS)
    GEN_JSON_ITEM("  configuration_security_aaa");
#endif /* defined(VTSS_SW_OPTION_RADIUS) || defined(VTSS_SW_OPTION_TACPLUS) */
#if defined(VTSS_SW_OPTION_RADIUS)
    GEN_JSON_ITEM("   auth_radius_config");
#endif /* defined(VTSS_SW_OPTION_RADIUS) */
#if defined(VTSS_SW_OPTION_TACPLUS)
    GEN_JSON_ITEM("   auth_tacacs_config");
#endif /* defined(VTSS_SW_OPTION_TACPLUS) */
#endif /* defined(VTSS_SW_OPTION_AUTH) */
#if defined(VTSS_SW_OPTION_AGGR) || defined(VTSS_SW_OPTION_LACP)
    GEN_JSON_ITEM(" configuration_aggregation");
#if defined(VTSS_SW_OPTION_AGGR)
    GEN_JSON_ITEM("  aggr_common");
    GEN_JSON_ITEM("  aggr_groups");
#endif
#if defined(VTSS_SW_OPTION_LACP)
    GEN_JSON_ITEM("  lacp_port_config");
#endif
#endif /* defined(VTSS_SW_OPTION_AGGR) || defined(VTSS_SW_OPTION_LACP) */
#if defined(VTSS_SW_OPTION_ETH_LINK_OAM)
    GEN_JSON_ITEM(" configuration_link_oam");
    GEN_JSON_ITEM("  eth_link_oam_port_config");
    GEN_JSON_ITEM("eth_link_oam_link_event_config");
#endif
#if defined(VTSS_SW_OPTION_LOOP_PROTECTION)
    GEN_JSON_ITEM(" loop_config");
#endif /* defined(VTSS_SW_OPTION_LOOP_PROTECTION) */
#if defined(VTSS_SW_OPTION_MSTP)
#if defined(VTSS_SW_OPTION_BUILD_SMB) || defined(VTSS_SW_OPTION_BUILD_CE)
#define VTSS_MSTP_FULL 1
#endif
    GEN_JSON_ITEM(" configuration_spanning_tree");
    GEN_JSON_ITEM("  mstp_sys_config");
#if defined(VTSS_MSTP_FULL)
    GEN_JSON_ITEM("  mstp_msti_map_config");
    GEN_JSON_ITEM("  mstp_msti_config");
    GEN_JSON_ITEM("  mstp_port_config");
    GEN_JSON_ITEM("  mstp_msti_port_config");
#else
    GEN_JSON_ITEM("  mstp_port_config");
#endif  // VTSS_MSTP_FULL
#endif  // MSTP
#if ((defined(VTSS_SW_OPTION_SMB_IPMC) || defined(VTSS_SW_OPTION_MVR)) && \
     defined(VTSS_SW_OPTION_IPMC_LIB))

    GEN_JSON_ITEM(" configuration_ipmc_profile");
    GEN_JSON_ITEM("  ipmc_lib_profile_table");
    GEN_JSON_ITEM("  ipmc_lib_entry_table");
#endif /* (VTSS_SW_OPTION_SMB_IPMC || VTSS_SW_OPTION_MVR) && VTSS_SW_OPTION_IPMC_LIB */
#if defined(VTSS_SW_OPTION_MVR)
    GEN_JSON_ITEM(" mvr");
#endif /* VTSS_SW_OPTION_MVR */
#if defined(VTSS_SW_OPTION_IPMC)
    GEN_JSON_ITEM(" configuration_ipmc");
    GEN_JSON_ITEM("  configuration_ipmc_igmp_snooping");
    GEN_JSON_ITEM("   ipmc_igmps");
    GEN_JSON_ITEM("   ipmc_igmps_vlan");
#if defined(VTSS_SW_OPTION_SMB_IPMC)
    GEN_JSON_ITEM("   ipmc_igmps_filtering");
    GEN_JSON_ITEM("  configuration_ipmc_igmp_mld_snooping");
    GEN_JSON_ITEM("   ipmc_mldsnp");
    GEN_JSON_ITEM("   ipmc_mldsnp_vlan");
    GEN_JSON_ITEM("   ipmc_mldsnp_filtering");
#endif /* VTSS_SW_OPTION_SMB_IPMC */
#endif /* VTSS_SW_OPTION_IPMC */
#if defined(VTSS_SW_OPTION_LLDP)
    GEN_JSON_ITEM(" configuration_lldp");
    GEN_JSON_ITEM("  lldp_config");
#if defined(VTSS_SW_OPTION_LLDP_MED)
    GEN_JSON_ITEM("  lldp_med_config");
#endif
#endif
#if defined(VTSS_SW_OPTION_POE)
    GEN_JSON_ITEM(" configuration_poe");
    GEN_JSON_ITEM("  poe_config");
    GEN_JSON_ITEM("  poe_ping_alive");
    GEN_JSON_ITEM("  poe_schedule");
#endif
#if defined(VTSS_SW_OPTION_SYNCE)
    GEN_JSON_ITEM(" synce_config");
#endif
#if defined(VTSS_SW_OPTION_EPS)
    GEN_JSON_ITEM(" eps");
#endif
#if defined(VTSS_SW_OPTION_MEP)
    GEN_JSON_ITEM(" mep");
#endif
#if defined(VTSS_SW_OPTION_ERPS)
    GEN_JSON_ITEM(" erps");
#endif
#if defined(VTSS_SW_OPTION_MAC)
    GEN_JSON_ITEM(" mac");
#endif /* VTSS_SW_OPTION_MAC */
#if defined(VTSS_SW_OPTION_VLAN)
    if (mesa_capability(0, MESA_CAP_L2_SVL_FID_CNT)) {
        GEN_JSON_ITEM(" configuration_vlan");
        GEN_JSON_ITEM("  vlan");
        GEN_JSON_ITEM("  vlan_svl");
    } else {
        GEN_JSON_ITEM(" vlan");
    }
#endif /* VTSS_SW_OPTION_VLAN  */
#if defined(VTSS_SW_OPTION_VLAN_TRANSLATION)
    GEN_JSON_ITEM(" configuration_vlan_translation");
    GEN_JSON_ITEM("  vlan_trans_port_config");
    GEN_JSON_ITEM("  map");
#endif /* VTSS_SW_OPTION_VLAN_TRANSLATION */
#if defined(VTSS_SW_OPTION_PVLAN)
    GEN_JSON_ITEM(" configuration_private_vlans");
#if !defined(VTSS_PERSONALITY_STACKABLE)
    GEN_JSON_ITEM("  pvlan");
#endif /* !defined(VTSS_PERSONALITY_STACKABLE) */
    GEN_JSON_ITEM("  port_isolation");
#endif /* VTSS_SW_OPTION_PVLAN */
#if defined(VTSS_SW_OPTION_VCL)
    GEN_JSON_ITEM(" configuration_vcl");
    GEN_JSON_ITEM("  mac_based_vlan");
    GEN_JSON_ITEM("  configuration_vcl_protocol-based_vlan");
    GEN_JSON_ITEM("   vcl_protocol_grp_map");
    GEN_JSON_ITEM("   vcl_grp_2_vlan_mapping");
    GEN_JSON_ITEM("  subnet_based_vlan");
#endif /* VTSS_SW_OPTION_VCL */
#if defined(VTSS_SW_OPTION_VOICE_VLAN)
    GEN_JSON_ITEM(" configuration_voice_vlan");
    GEN_JSON_ITEM("  voice_vlan_config");
    GEN_JSON_ITEM("  voice_vlan_oui");
#endif /* VTSS_SW_OPTION_VOICE_VLAN */
#if defined(VTSS_SW_OPTION_QOS)
    GEN_JSON_ITEM(" configuration_qos");
    GEN_JSON_ITEM("  qos_port_classification");
    GEN_JSON_ITEM("  qos_port_policers");
#if defined(VTSS_SW_OPTION_BUILD_SMB) || defined(VTSS_SW_OPTION_BUILD_CE)
    GEN_JSON_ITEM("  qos_queue_policers");
#endif /* defined(VTSS_SW_OPTION_BUILD_SMB) || defined(VTSS_SW_OPTION_BUILD_CE) */
    GEN_JSON_ITEM("  qos_port_schedulers");
    GEN_JSON_ITEM("  qos_port_shapers");
#if defined(VTSS_SW_OPTION_BUILD_SMB) || defined(VTSS_SW_OPTION_BUILD_CE)
    GEN_JSON_ITEM("  qos_port_tag_remarking");
    GEN_JSON_ITEM("  qos_port_dscp_config");
    GEN_JSON_ITEM("  dscp_based_qos_ingr_classifi");
    GEN_JSON_ITEM("  dscp_translation");
    GEN_JSON_ITEM("  dscp_classification");

    if (mesa_capability(0, MESA_CAP_QOS_INGRESS_MAP_CNT)) {
        GEN_JSON_ITEM("  qos_ingress_map");
    }

    if (mesa_capability(0, MESA_CAP_QOS_EGRESS_MAP_CNT)) {
        GEN_JSON_ITEM("  qos_egress_map");
    }
#endif /* defined(VTSS_SW_OPTION_BUILD_SMB) || defined(VTSS_SW_OPTION_BUILD_CE) */
    GEN_JSON_ITEM("  qcl_v2");
    GEN_JSON_ITEM("  stormctrl");

    if (mesa_capability(0, MESA_CAP_QOS_WRED_GROUP_CNT)) {
        GEN_JSON_ITEM("  qos_wred");
    }
#if (defined(VTSS_SW_OPTION_BUILD_SMB) || \
     defined(VTSS_SW_OPTION_BUILD_ISTAX) || defined(VTSS_SW_OPTION_BUILD_CE))

    if (mesa_capability(0, MESA_CAP_QOS_FRAME_PREEMPTION)) {
        GEN_JSON_ITEM("  qos_port_fp_config");
    }

    if (mesa_capability(0, MESA_CAP_QOS_EGRESS_QUEUE_SHAPERS_TAS)) {
        GEN_JSON_ITEM("  configuration_qos_tas");
        GEN_JSON_ITEM("   qos_port_tas_config");
        GEN_JSON_ITEM("   qos_port_tas_max_sdu");
    }

    if (mesa_capability(0, MESA_CAP_QOS_PSFP)) {
        GEN_JSON_ITEM("  configuration_qos_psfp");
        GEN_JSON_ITEM("   qos_psfp_fmi_config");
        GEN_JSON_ITEM("   qos_psfp_sfi_config");
        GEN_JSON_ITEM("   qos_psfp_sgi_config");
    }
#endif
#endif /* VTSS_SW_OPTION_QOS */
#if defined(VTSS_SW_OPTION_MIRROR) && defined(VTSS_SW_OPTION_JSON_RPC)
#if defined(VTSS_SW_OPTION_MIRROR_LOOP_PORT)
    GEN_JSON_ITEM(" mirror_no_reflector_port");
#else
    GEN_JSON_ITEM(" mirror_ctrl");
#endif
#endif /* VTSS_SW_OPTION_RMIRROR  && VTSS_SW_OPTION_JSON_RPC */
#if defined(VTSS_SW_OPTION_UPNP)
    GEN_JSON_ITEM(" upnp");
#endif /* VTSS_SW_OPTION_UPNP */
#if defined(VTSS_SW_OPTION_PTP)
    GEN_JSON_ITEM(" ptp_config");
#endif /* VTSS_SW_OPTION_PTP */
#if defined(VTSS_SW_OPTION_MRP)
    GEN_JSON_ITEM(" configuration_mrp");
    GEN_JSON_ITEM("  mrp_port");
#if defined(VTSS_SW_OPTION_MVRP)
    GEN_JSON_ITEM("  mvrp_config");
#endif /* VTSS_SW_OPTION_MVRP */
#endif /* VTSS_SW_OPTION_MRP */
#if defined(VTSS_SW_OPTION_GVRP)
    GEN_JSON_ITEM(" configuration_gvrp");
    GEN_JSON_ITEM("  gvrp_config");
    GEN_JSON_ITEM("  Port config");
#endif /* VTSS_SW_OPTION_GVRP */
#if defined(VTSS_PERSONALITY_STACKABLE)
    GEN_JSON_ITEM(" stack_config");
#endif /* VTSS_PERSONALITY_STACKABLE */
#if defined(VTSS_SW_OPTION_SFLOW)
    GEN_JSON_ITEM(" sflow");
#endif /* VTSS_SW_OPTION_SFLOW */
#if defined(VTSS_SW_OPTION_DDMI)
    if (lntn_conf_mgmt_sfp_count_get()) {
        GEN_JSON_ITEM(" ddmi_config");
    }
#endif /* VTSS_SW_OPTION_DDMI */
#if defined(VTSS_SW_OPTION_UDLD)
    GEN_JSON_ITEM(" udld_config");
#endif
#if defined(VTSS_SW_OPTION_MODBUS)
    GEN_JSON_ITEM(" modbus_config");
#endif /* VTSS_SW_OPTION_MODBUS */
#if defined(VTSS_SW_OPTION_FRR)
    if (vtss::frr_has_ospfd()) {
        GEN_JSON_ITEM(" configuration_ospf");
        GEN_JSON_ITEM("  frr_ospf_global_config");
        GEN_JSON_ITEM("  frr_ospf_passive_intf_config");
        GEN_JSON_ITEM("  frr_ospf_passive_intf_config");
        GEN_JSON_ITEM("  frr_ospf_stub_area_config");
        GEN_JSON_ITEM("  frr_ospf_area_auth_config");
        GEN_JSON_ITEM("  frr_ospf_area_range_config");
        GEN_JSON_ITEM("  frr_ospf_intf_config");
        GEN_JSON_ITEM("  frr_ospf_vlink_config");
    }
#endif /* VTSS_SW_OPTION_FRR */
#if defined(VTSS_SW_OPTION_SR) && (defined(VTSS_SW_OPTION_BUILD_SMB) ||   \
                                   defined(VTSS_SW_OPTION_BUILD_ISTAX) || \
                                   defined(VTSS_SW_OPTION_BUILD_CE))

    GEN_JSON_ITEM(" configuration_seamless_redundancy");
    GEN_JSON_ITEM("  sr_stream_config");
    GEN_JSON_ITEM("  sr_stream_latent_error_config");
#endif /* if defined(VTSS_SW_OPTION_SR) && ......*/
    GEN_JSON_ITEM("monitor");
    GEN_JSON_ITEM(" monitor_system");
    GEN_JSON_ITEM("  sys");
    GEN_JSON_ITEM("  sys_led_status");
    GEN_JSON_ITEM("  perf_cpuload");
#if defined(VTSS_SW_OPTION_IP)
    GEN_JSON_ITEM("  ip_status");
#endif
#if defined(VTSS_SW_OPTION_FRR)
    GEN_JSON_ITEM("  ip_routing_info_base_status");
#endif
#if defined(VTSS_SW_OPTION_SYSLOG)
    GEN_JSON_ITEM("  syslog");
    GEN_JSON_ITEM("  syslog_detailed");
#endif /* VTSS_SW_OPTION_SYSLOG */
#if defined(VTSS_SW_OPTION_PSU)
    GEN_JSON_ITEM("  sys_psu_status");
#endif /* VTSS_SW_OPTION_PSU */
#if defined(VTSS_SW_OPTION_PORT_POWER_SAVINGS) || \
        defined(VTSS_SW_OPTION_FAN) || defined(VTSS_SW_OPTION_EEE)

    GEN_JSON_ITEM(" monitor_green_ethernet");
#if defined(VTSS_SW_OPTION_PORT_POWER_SAVINGS)
    GEN_JSON_ITEM("  port_power_savings_status");
#endif /* VTSS_SW_OPTION_PORT_POWER_SAVINGS */
    if (fan_module_enabled()) {
        GEN_JSON_ITEM("  fan_status");
    }
#endif
    if (thermal_protect_module_enabled()) {
        GEN_JSON_ITEM(" thermal_protect_status");
    }

    GEN_JSON_ITEM(" monitor_ports");
    GEN_JSON_ITEM("  stat_ports");
#if defined(VTSS_SW_OPTION_QOS)
    GEN_JSON_ITEM("  qos_counter");
#if defined(VTSS_SW_OPTION_QOS_QCL_INCLUDE)
#if VTSS_SW_OPTION_QOS_QCL_INCLUDE == 1
    GEN_JSON_ITEM("  qcl_v2_stat");
#endif /* VTSS_SW_OPTION_QOS_QCL_INCLUDE == 1 */
#else
    GEN_JSON_ITEM("  qcl_v2_stat");
#endif /* defined(VTSS_SW_OPTION_QOS_QCL_INCLUDE) */
#endif /* defined(VTSS_SW_OPTION_QOS) */
    GEN_JSON_ITEM("  stat_detailed");
#if defined(VTSS_SW_OPTION_ETH_LINK_OAM)
    GEN_JSON_ITEM(" monitor_link_oam");
    GEN_JSON_ITEM("  eth_link_oam_stat_detailed");
    GEN_JSON_ITEM("  eth_link_oam_port_status");
    GEN_JSON_ITEM("  eth_link_oam_link_status");
#endif
#if defined(VTSS_SW_OPTION_DHCP_HELPER)
    GEN_JSON_ITEM(" monitor_dhcp");
#if defined(VTSS_SW_OPTION_DHCP_SERVER)
    GEN_JSON_ITEM("  monitor_dhcp_server");
    GEN_JSON_ITEM("   dhcp_server_stat");
    GEN_JSON_ITEM("   dhcp_server_stat_binding");
    GEN_JSON_ITEM("   dhcp_server_stat_declined");
#endif /* VTSS_SW_OPTION_DHCP_SERVER */
#if defined(VTSS_SW_OPTION_DHCP_SNOOPING)
    GEN_JSON_ITEM("  dyna_dhcp_snooping");
#endif
#if defined(VTSS_SW_OPTION_DHCP_RELAY)
    GEN_JSON_ITEM("  dhcp_relay_statistics");
#endif /* VTSS_SW_OPTION_DHCP_RELAY */
    GEN_JSON_ITEM("  dhcp_helper_statistics");
#endif /* VTSS_SW_OPTION_DHCP_HELPER */
    GEN_JSON_ITEM(" monitor_security");
#if defined(VTSS_SW_OPTION_ACCESS_MGMT)
    GEN_JSON_ITEM("  access_mgmt_statistics");
#endif
    GEN_JSON_ITEM("  monitor_security_network");
#if defined(VTSS_SW_OPTION_PSEC)
    GEN_JSON_ITEM("   monitor_security_network_port_security");
    GEN_JSON_ITEM("    psec_status_switch");
    GEN_JSON_ITEM("    psec_status_port");
#endif
#if defined(VTSS_SW_OPTION_DOT1X)
    GEN_JSON_ITEM("   monitor_security_network_nas");
    GEN_JSON_ITEM("    nas_status_switch");
    GEN_JSON_ITEM("    nas_status_port");
#endif
#if defined(VTSS_SW_OPTION_ACL)
    GEN_JSON_ITEM("   acl_status");
#endif /* VTSS_SW_OPTION_ACL */
#if defined(VTSS_SW_OPTION_ARP_INSPECTION)
    GEN_JSON_ITEM("   dyna_arp_inspection");
#endif /* VTSS_SW_OPTION_ARP_INSPECTION */
#if defined(VTSS_SW_OPTION_IP_SOURCE_GUARD)
    GEN_JSON_ITEM("   dyna_ip_source_guard");
#endif /* VTSS_SW_OPTION_IP_SOURCE_GUARD */
#if defined(VTSS_SW_OPTION_RADIUS)
    GEN_JSON_ITEM("  monitor_security_aaa");
    GEN_JSON_ITEM("   auth_status_radius_overview");
    GEN_JSON_ITEM("   auth_status_radius_details");
#endif /* VTSS_SW_OPTION_RADIUS */
#if defined(VTSS_SW_OPTION_RMON)
    GEN_JSON_ITEM("  monitor_security_switch");
    GEN_JSON_ITEM("   monitor_security_switch_rmon");
    GEN_JSON_ITEM("    rmon_statistics_status");
    GEN_JSON_ITEM("    rmon_history_status");
    GEN_JSON_ITEM("    rmon_alarm_status");
    GEN_JSON_ITEM("    rmon_event_status");
#endif /* VTSS_SW_OPTION_RMON */
#if defined(VTSS_SW_OPTION_AGGR) || defined(VTSS_SW_OPTION_LACP)
    GEN_JSON_ITEM(" monitor_aggregation");
#if defined(VTSS_SW_OPTION_AGGR)
    GEN_JSON_ITEM("  aggregation_status");
#endif
#if defined(VTSS_SW_OPTION_LACP)
    GEN_JSON_ITEM("  monitor_aggregation_lacp");
    GEN_JSON_ITEM("   lacp_sys_status");
    GEN_JSON_ITEM("   lacp_internal_status");
    GEN_JSON_ITEM("   lacp_neighbor_status");
    GEN_JSON_ITEM("   lacp_statistics");
#endif
#endif /* defined(VTSS_SW_OPTION_AGGR) || defined(VTSS_SW_OPTION_LACP) */
#if defined(VTSS_SW_OPTION_LOOP_PROTECTION)
    GEN_JSON_ITEM(" loop_status");
#endif /* defined(VTSS_SW_OPTION_LOOP_PROTECTION) */
#if defined(VTSS_SW_OPTION_MSTP)
    GEN_JSON_ITEM(" monitor_spanning_tree");
#if defined(VTSS_MSTP_FULL)
    GEN_JSON_ITEM("  mstp_status_bridges");
    GEN_JSON_ITEM("  mstp_status_bridge");
#else
    GEN_JSON_ITEM("  mstp_status_bridge");
#endif
    GEN_JSON_ITEM("  mstp_status_ports");
    GEN_JSON_ITEM("  mstp_statistics");
#endif /* VTSS_SW_OPTION_MSTP */
#if defined(VTSS_SW_OPTION_MVR)
    GEN_JSON_ITEM(" monitor_mvr");
    GEN_JSON_ITEM("  mvr_status");
    GEN_JSON_ITEM("  mvr_groups_info");
    GEN_JSON_ITEM("  mvr_groups_sfm");
#endif /* VTSS_SW_OPTION_MVR */
#if defined(VTSS_SW_OPTION_IPMC)
    GEN_JSON_ITEM(" monitor_ipmc");
    GEN_JSON_ITEM("  monitor_ipmc_igmp_snooping");
    GEN_JSON_ITEM("   ipmc_igmps_status");
    GEN_JSON_ITEM("   ipmc_igmps_groups_info");
#if defined(VTSS_SW_OPTION_SMB_IPMC)
    GEN_JSON_ITEM("   ipmc_igmps_v3info");
    GEN_JSON_ITEM("  monitor_ipmc_igmp_mld_snooping");
    GEN_JSON_ITEM("   ipmc_mldsnp_status");
    GEN_JSON_ITEM("   ipmc_mldsnp_groups_info");
    GEN_JSON_ITEM("   ipmc_mldsnp_v2info");
#endif /* VTSS_SW_OPTION_SMB_IPMC */
#endif /* VTSS_SW_OPTION_IPMC */
#if defined(VTSS_SW_OPTION_LLDP)
    GEN_JSON_ITEM(" monitor_lldp");
    GEN_JSON_ITEM("  lldp_neighbors");
#if defined(VTSS_SW_OPTION_LLDP_MED)
    GEN_JSON_ITEM("  lldp_neighbors_med");
#endif /* VTSS_SW_OPTION_LLDP_MED */
#if defined(VTSS_SW_OPTION_POE)
    GEN_JSON_ITEM("  lldp_poe_neighbors");
#endif /* VTSS_SW_OPTION_POE */
#if defined(VTSS_SW_OPTION_EEE)
    GEN_JSON_ITEM("  lldp_eee_neighbors");
#endif
    GEN_JSON_ITEM("  lldp_statistic");
#endif /* VTSS_SW_OPTION_LLDP */
#if defined(VTSS_SW_OPTION_PTP)
    GEN_JSON_ITEM(" monitor_ptp");
    GEN_JSON_ITEM("  ptp");
#endif /* VTSS_SW_OPTION_PTP */
#if defined(VTSS_SW_OPTION_POE)
    GEN_JSON_ITEM(" poe_status");
#endif /* VTSS_SW_OPTION_POE */
#if defined(VTSS_SW_OPTION_MAC)
    GEN_JSON_ITEM(" dyna_mac");
#endif /* VTSS_SW_OPTION_MAC */
#if defined(VTSS_SW_OPTION_VLAN)
    GEN_JSON_ITEM(" monitor_vlans");
    GEN_JSON_ITEM("  vlan_membership_stat");
    GEN_JSON_ITEM("  vlan_port_stat");
#endif /* VTSS_SW_OPTION_VLAN */
#if defined(VTSS_SW_OPTION_MRP)
#endif /* VTSS_SW_OPTION_MRP */
#if defined(VTSS_SW_OPTION_MVRP)
    GEN_JSON_ITEM(" mvrp_stat");
#endif /* VTSS_SW_OPTION_MVRP */
#if defined(VTSS_PERSONALITY_STACKABLE)
    GEN_JSON_ITEM(" stackstate");
#endif /* VTSS_PERSONALITY_STACKABLE */
#if defined(VTSS_SW_OPTION_SFLOW)
    GEN_JSON_ITEM(" sflow_status");
#endif /* VTSS_SW_OPTION_SFLOW */
#if defined(VTSS_SW_OPTION_DDMI)
    if (lntn_conf_mgmt_sfp_count_get()) {
        GEN_JSON_ITEM(" monitor_ddmi");
        GEN_JSON_ITEM("  ddmi_overview");
        GEN_JSON_ITEM("  ddmi_detailed");
    }
#endif /* VTSS_SW_OPTION_DDMI */
#if defined(VTSS_SW_OPTION_UDLD)
    GEN_JSON_ITEM(" udld_status");
#endif
#if defined(VTSS_SW_OPTION_BUILD_SMB) || \
        defined(VTSS_SW_OPTION_BUILD_ISTAX) || defined(VTSS_SW_OPTION_BUILD_CE)

    if (mesa_capability(0, MESA_CAP_QOS_FRAME_PREEMPTION) ||
        mesa_capability(0, MESA_CAP_QOS_EGRESS_QUEUE_SHAPERS_TAS)) {
        GEN_JSON_ITEM(" monitor_qos");
    }

    if (mesa_capability(0, MESA_CAP_QOS_FRAME_PREEMPTION)) {
        GEN_JSON_ITEM("  qos_port_fp_status");
    }

    if (mesa_capability(0, MESA_CAP_QOS_EGRESS_QUEUE_SHAPERS_TAS)) {
        GEN_JSON_ITEM("  monitor_qos_tas");
        GEN_JSON_ITEM("   qos_port_tas_status");
    }

    if (mesa_capability(0, MESA_CAP_QOS_PSFP)) {
        GEN_JSON_ITEM("  monitor_qos_psfp");
        GEN_JSON_ITEM("   qos_psfp_stream_parameters_status");
        GEN_JSON_ITEM("   qos_psfp_sfi_statistics");
        GEN_JSON_ITEM("   qos_psfp_sgi_status");
    }
#if defined(VTSS_SW_OPTION_FRR)
    if (vtss::frr_has_ospfd()) {
        GEN_JSON_ITEM(" monitor_ospf");
        GEN_JSON_ITEM("  frr_ospf_global_status");
        GEN_JSON_ITEM("  frr_ospf_area_status");
        GEN_JSON_ITEM("  frr_ospf_nbr_status");
        GEN_JSON_ITEM("  frr_ospf_intf_status");
    }
#endif /* VTSS_SW_OPTION_FRR */
#endif /* defined(VTSS_SW_OPTION_BUILD_SMB) || defined(VTSS_SW_OPTION_BUILD_ISTAX) \
          || defined(VTSS_SW_OPTION_BUILD_CE) */

    GEN_JSON_ITEM("diagnostics");
    GEN_JSON_ITEM(" ping4");
    GEN_JSON_ITEM(" ping4_result");
#if defined(VTSS_SW_OPTION_IPV6)
    GEN_JSON_ITEM(" ping6");
    GEN_JSON_ITEM(" ping6_result");
#endif /* VTSS_SW_OPTION_IPV6 */
    GEN_JSON_ITEM(" traceroute4");
    GEN_JSON_ITEM(" traceroute4_result");
#if defined(VTSS_SW_OPTION_IPV6)
    GEN_JSON_ITEM(" traceroute6");
    GEN_JSON_ITEM(" traceroute6_result");
#endif /* VTSS_SW_OPTION_IPV6 */
#if defined(VTSS_SW_OPTION_ETH_LINK_OAM)
    GEN_JSON_ITEM(" diagnostics__link_oam");
    GEN_JSON_ITEM("  eth_link_oam_mib_support");
#endif
    GEN_JSON_ITEM(" veriphy");
    GEN_JSON_ITEM("maintenance");
    GEN_JSON_ITEM(" wreset");
    GEN_JSON_ITEM(" factory");
#if defined(VTSS_PERSONALITY_STACKABLE)
    GEN_JSON_ITEM(" upload");
#else
    GEN_JSON_ITEM(" maintenance_software");
    GEN_JSON_ITEM("  upload");
    GEN_JSON_ITEM("  sw_select");
#endif
#if defined(VTSS_SW_OPTION_ICFG)
    GEN_JSON_ITEM(" maintenance_configuration");
    GEN_JSON_ITEM("  icfg_conf_save");
    GEN_JSON_ITEM("  icfg_conf_download");
    GEN_JSON_ITEM("  icfg_conf_upload");
    GEN_JSON_ITEM("  icfg_conf_activate");
    GEN_JSON_ITEM("  icfg_conf_delete");
    GEN_JSON_ITEM("  icfg_conf_running");
#endif /* VTSS_SW_OPTION_ICFG*/

    return level;
}
