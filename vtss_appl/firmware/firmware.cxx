/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <mtd/mtd-user.h>
#include <dirent.h>
#include <zlib.h>

#include "main.h"
#include "firmware.h"
#include "msg_api.h"
#include "led_api.h"
#include "conf_api.h"

#include "firmware_mfi_info.hxx"
#include "standalone_api.h"

#include <vtss/basics/algorithm.hxx>

#include "firmware_ubi.hxx"     // For Firmware_ubi

#include "lock.hxx"             // For vtss::Lock
#include "usb_api.h"            // For usb_file_read

// NB: Linux is over-comitting, so a large number is OK....
size_t firmware_max_download = (20 * 1024 * 1024);

/* Thread variables */
static vtss_handle_t firmware_thread_handle;
static vtss_thread_t firmware_thread_block;

static vtss_handle_t firmware_transfer_thread_handle;
static vtss_thread_t firmware_transfer_thread_block;
static vtss::Lock    firmware_transfer_thread_lock;

static unsigned char *usb_filename;
static unsigned int  usb_filename_size;

/* firmware upload thread variables */
#define FIRMWARE_UPLOAD_THREAD_FLAG_UPLOAD          VTSS_BIT(1)    // upload Image

static vtss_handle_t g_firmware_upload_thread_handle;
static vtss_thread_t g_firmware_upload_thread_block;

vtss_appl_firmware_control_image_upload_t g_image_upload;
static vtss_appl_firmware_status_image_t         g_image_status[VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ALTERNATIVE_FIRMWARE + 1];
static vtss_flag_t                               g_firmware_upload_thread_flag;

/* "simple" lock functions */
#define SIMPLE_LOCK()   vtss_global_lock(__FILE__, __LINE__)
#define SIMPLE_UNLOCK() vtss_global_unlock(__FILE__, __LINE__)

extern mesa_rc check_bootloader(cli_iolayer_t *io, const u8 *buffer, size_t length);
extern mesa_rc firmware_icli_load_image(cli_iolayer_t *io,
                                     const char *mtd_name,
                                     const char *url,
                                     mesa_rc (*checker)(cli_iolayer_t *, const u8 *, size_t));

size_t firmware_section_size(const char *name)
{
    vtss_mtd_t mtd;

    if (vtss_mtd_open(&mtd, name) == VTSS_OK) {
        vtss_mtd_close(&mtd);
        return mtd.info.size;
    }
    return 0;
}


/******************************* For handling the FIS directory **************************************/

/*
 * Close and free after updating the fis table
 */
mesa_rc vtss_fis_close(vtss_fis_mtd_t *fis_mtd)
{
    /* free memory space */
    VTSS_ASSERT(fis_mtd->fis_data);
    VTSS_FREE(fis_mtd->fis_data);
    /* close file */
    return vtss_mtd_close(&(fis_mtd->mtd));
}

/*
 * Acquire the fis descriptor of the fis table in the flash
 * input : "FIS_directory" or "Redundant_FIS"
 * output:  fis descriptor
 */
mesa_rc vtss_fis_open(vtss_fis_mtd_t *fis_mtd, const char *name)
{
    if( vtss_mtd_open(&(fis_mtd->mtd), name) != VTSS_OK ) {
        T_D("Not found: %s\n", name);
        goto done;
    }
    T_D("%s size=0x%08x, erasesize=0x%08x, fd=%d", name, fis_mtd->mtd.info.size, fis_mtd->mtd.info.erasesize, fis_mtd->mtd.fd);

    fis_mtd->name = name;
    fis_mtd->fis_size = fis_mtd->mtd.info.size;

    if ((fis_mtd->fis_data = (u8*) VTSS_MALLOC(fis_mtd->fis_size)) == NULL) {
        T_D("malloc failed! [%s]", strerror(errno));
        goto done;
    }
    memset(fis_mtd->fis_data, 0, fis_mtd->fis_size);
    if (pread(fis_mtd->mtd.fd, fis_mtd->fis_data, fis_mtd->fis_size, 0) == -1) {
        T_D("pread from %s failed! [%s]", name, strerror(errno));
        goto done;
    }

    // Enumeration limits
    fis_mtd->fis_start = (vtss_fis_desc_t *) fis_mtd->fis_data;
    fis_mtd->fis_end   = (vtss_fis_desc_t *) (fis_mtd->fis_data + fis_mtd->fis_size);
    return VTSS_RC_OK;

done:
    vtss_mtd_close(&fis_mtd->mtd);
    return VTSS_RC_ERROR;
}

static vtss_rc firmware_fis_read(vtss_fis_mtd_t *fis_dir,
                                 vtss_fis_mtd_t *fis_bak,
                                 vtss_fis_mtd_t **act_fis,
                                 vtss_fis_mtd_t **alt_fis)
{
    *act_fis = NULL;  /* active fis table */
    *alt_fis = NULL;  /* alternative fis table */

    memset(fis_dir, 0x0, sizeof(vtss_fis_mtd_t));
    memset(fis_bak, 0x0, sizeof(vtss_fis_mtd_t));
    if ((vtss_fis_open(fis_dir, "FIS_directory")) != VTSS_OK) {
        T_E("open FIS_directory failed! [%s]\n", strerror(errno));
        goto done;
    }

    if ((vtss_fis_open(fis_bak, "Redundant_FIS")) != VTSS_OK) {
        T_E("open Redundant_FIS failed! [%s]\n", strerror(errno));
        goto done;
    }

    if (!fis_dir->fis_start || !fis_bak->fis_start) {
        T_E("descriptor pointer is NULL!\n");
        goto done;
    }

    if (fis_dir->fis_start->u.valid_info.valid_flag[0] != VTSS_REDBOOT_RFIS_VALID &&
        fis_bak->fis_start->u.valid_info.valid_flag[0] != VTSS_REDBOOT_RFIS_VALID) {
        T_E("Neither flash directories are valid, unable to update\n");
        goto done;
    }

    /* Detect which fis table we are currently running based on version_count */
    if (fis_dir->fis_start->u.valid_info.valid_flag[0] == VTSS_REDBOOT_RFIS_VALID &&
        fis_bak->fis_start->u.valid_info.valid_flag[0] == VTSS_REDBOOT_RFIS_VALID) {
        if (fis_dir->fis_start->u.valid_info.version_count > fis_bak->fis_start->u.valid_info.version_count) {
            /*  "FIS_directory" is currently active */
            T_D("Two good FIS - Primary is good (%d, %d)", fis_dir->fis_start->u.valid_info.version_count, fis_bak->fis_start->u.valid_info.version_count);
            *act_fis = fis_dir;
            *alt_fis = fis_bak;
        } else {
            /* "Redundant_FIS" is currently active */
            T_D("Two good FIS - Redundant is good (%d, %d)", fis_dir->fis_start->u.valid_info.version_count, fis_bak->fis_start->u.valid_info.version_count);
            *act_fis = fis_bak;
            *alt_fis = fis_dir;
        }
    } else {
        // Both Fis are bot valid, but one is
        if (fis_dir->fis_start->u.valid_info.valid_flag[0] == VTSS_REDBOOT_RFIS_VALID) {
            /*  "FIS_directory" is currently active */
            T_D("One good FIS - primary");
            *act_fis = fis_dir;
            *alt_fis = fis_bak;
        } else {
            /* "Redundant_FIS" is currently active */
            T_D("One good FIS - redundant");
            *act_fis = fis_bak;
            *alt_fis = fis_dir;
        }
    }

    return VTSS_RC_OK;

done:
    /* cleanup */
    vtss_fis_close(fis_dir);
    vtss_fis_close(fis_bak);
    return VTSS_RC_ERROR;
}

static vtss_rc firmware_fis_update(const vtss_fis_mtd_t *act_fis,
                                   const vtss_fis_mtd_t *alt_fis)
{
    vtss_rc rc = VTSS_RC_ERROR;    // Assume the worst

    /* Activating the alternative fis table by increasing version counter */
    act_fis->fis_start->u.valid_info.version_count += 0x1;

    /* MTD device: erase before writing */
    T_D("Erasing alt fis table %s...", alt_fis->name);
    if ( (vtss_mtd_erase(&alt_fis->mtd, alt_fis->fis_size)) == VTSS_OK ) {
        T_D(" done!");
        T_D("Programming alt fis table...");
        act_fis->fis_start->u.valid_info.valid_flag[0] =
                act_fis->fis_start->u.valid_info.valid_flag[1] =
                VTSS_REDBOOT_RFIS_IN_PROGRESS;
        if ( (vtss_mtd_program(&alt_fis->mtd, (const u8*)act_fis->fis_start, act_fis->fis_size)) == VTSS_OK ) {
            off_t flagpos = offsetof(vtss_fis_desc_t, u.valid_info.valid_flag[0]);
            u8 flags[2] = { VTSS_REDBOOT_RFIS_VALID, VTSS_REDBOOT_RFIS_VALID };
            T_D("Wrote in-progress FIS");
            // Now update valid bits - essentially clearing bits atomically (UNERASED)
            if ( (vtss_mtd_program_pos(&alt_fis->mtd, flags, sizeof(flags), flagpos)) == VTSS_OK ) {
                T_D("Wrote FIS valid flags");
                rc = VTSS_RC_OK;
            } else {
                T_D("Failure writing FIS valid flags");
            }
        } else {
            T_D("Failure writing FIS valid flags");
        }
    } else {
        T_D("Failure erasing FIS");
    }
    return rc;
}

#define FIS_ENTRY_INVALID(f) ((f)->u.name[0] == 0 || (f)->u.name[0] == '\xff')

vtss_fis_desc_t *firmware_fis_find(const vtss_fis_mtd_t *fis, const char *name)
{
    vtss_fis_desc_t *desc;
    for (desc = fis->fis_start; desc < fis->fis_end; desc++)  {
        /* Analyze the contents of the active fis table */
        if (FIS_ENTRY_INVALID(desc)) {
            T_N("%p: Bad entry %d", desc, desc->u.name[0]);
            continue;
        }
        T_N("%p: %s Looking for '%s'", desc, desc->u.name, name);
        if (strcmp(desc->u.name, name) == 0) {
            T_D("Found %s at offset %p", name, desc);
            return desc;
        }
    }
    T_D("Did not find '%s'", name);
    return NULL;
}

vtss_fis_desc_t *firmware_fis_insert(vtss_fis_desc_t *start, vtss_fis_desc_t *end)
{
    if ((start < end)) {
        if (FIS_ENTRY_INVALID(&end[-1])) {    // Assume last is invalid
            vtss_fis_desc_t *desc;
            desc = &start[1];    // Inserted after start
            memmove(desc+1, desc, ((char*)end) - ((char*)(desc+1)));
            return desc;
        }
    }
    return NULL;
}

void firmware_fis_list(const char *what, const vtss_fis_mtd_t *fis)
{
    vtss_fis_desc_t *desc;
    T_D("%s: %s - Entries %p-%p", what, fis->name, fis->fis_start, fis->fis_end);
    for (desc = fis->fis_start; desc < fis->fis_end; desc++)  {
        /* Analyze the contents of the active fis table */
        if (FIS_ENTRY_INVALID(desc)) {
            T_N("%p: %s", desc, "(invalid)");
            continue;
        }
        T_D("%p: %-16s: base 0x%08x, length %u", desc, desc->u.name, desc->flash_base, desc->size);
    }
}

vtss_rc firmware_fis_split(const char *primary, size_t primary_size,
                           const char *backup, size_t backup_size)
{
    vtss_fis_mtd_t fis_dir, fis_bak;  /* Two fis tables */
    vtss_fis_mtd_t *act_fis, *alt_fis;  /* active, backup fis table */
    vtss_rc rc;

    if ((rc = firmware_fis_read(&fis_dir, &fis_bak, &act_fis, &alt_fis)) == VTSS_RC_OK) {
        vtss_fis_desc_t *linux;
        T_D("Active: %s, Alt: %s", act_fis->name,  alt_fis->name);
        firmware_fis_list("Active", act_fis);
        if ((linux = firmware_fis_find(act_fis, "linux"))) {
            vtss_fis_desc_t *newent;
            if ((newent = firmware_fis_find(act_fis, "linux.bk")) != NULL) {
                rc = VTSS_RC_ERROR;
                T_E("Already have a %s FIS entry", newent->u.name);
            } else {
                newent = firmware_fis_insert(linux, act_fis->fis_end);
                T_D("Found linux FIS at %p, insert new @ %p", linux, newent);
                if (newent) {
                    linux->size = primary_size;
                    *newent = *linux;
                    strcpy(newent->u.name, "linux.bk");
                    newent->flash_base += primary_size;
                    linux->size = backup_size;
                    T_D("Split into %d, %d", primary_size, backup_size);
                    firmware_fis_list("Updated FIS", act_fis);
                    rc = firmware_fis_update(act_fis, alt_fis);
                }
            }
        } else {
            rc = VTSS_RC_ERROR;
            T_E("Unable to find linux FIS in %s", act_fis->name);
        }
        vtss_fis_close(&fis_dir);
        vtss_fis_close(&fis_bak);
    }

    return rc;
}

vtss_rc firmware_fis_resize(const char *name, size_t new_size)
{
    vtss_fis_mtd_t fis_dir, fis_bak;  /* Two fis tables */
    vtss_fis_mtd_t *act_fis, *alt_fis;  /* active, backup fis table */
    vtss_rc rc;
    if ((rc = firmware_fis_read(&fis_dir, &fis_bak, &act_fis, &alt_fis)) == VTSS_RC_OK) {
        vtss_fis_desc_t *desc;
        T_D("Active: %s, Alt: %s", act_fis->name, alt_fis->name);
        firmware_fis_list("Active", act_fis);
        if ((desc = firmware_fis_find(act_fis, name))) {
            desc->data_length = desc->size = new_size;
            firmware_fis_list("Updated", act_fis);
            rc = firmware_fis_update(act_fis, alt_fis);
        } else {
            rc = VTSS_RC_ERROR;
            T_W("Unable to find '%s' FIS in %s", name, act_fis->name);
        }
        vtss_fis_close(&fis_dir);
        vtss_fis_close(&fis_bak);
    }
    return rc;
}


vtss_rc firmware_swap_images(void)
{
    vtss_fis_mtd_t fis_dir, fis_bak;  /* Two fis tables */
    vtss_fis_mtd_t *act_fis, *alt_fis;  /* active, backup fis table */
    vtss_fis_desc_t *act_desc, *alt_desc;  /* active, alternative desc in active fis table */
    vtss_rc rc;
    if ((rc = firmware_fis_read(&fis_dir, &fis_bak, &act_fis, &alt_fis)) == VTSS_RC_OK) {
        act_desc = firmware_fis_find(act_fis, "linux");
        alt_desc = firmware_fis_find(act_fis, "linux.bk");
        /* only swap if both "linux" & "linux.bk" are available */
        if (act_desc && alt_desc) {
            /* swap name */
            vtss_fis_desc_t tmp;
            tmp = *act_desc;              // Save active
            act_desc->u = alt_desc->u;    // Act = Alt
            alt_desc->u = tmp.u;          // Alt = Act
            firmware_fis_list("Updated FIS", act_fis);
            rc = firmware_fis_update(act_fis, alt_fis);
        } else {
            T_D("Unable to swap firmware slots, did not find %s", act_desc == NULL ? "linux" : "linux.bk");
            rc = VTSS_RC_ERROR;
        }
        /* cleanup */
        vtss_fis_close(&fis_dir);
        vtss_fis_close(&fis_bak);
    }
    return rc;
}

/* Read the contents from linux kernel cmdline */
mesa_rc firmware_cmdline_get(char *str, int size)
{
    FILE* fp;
    if ( (fp = fopen ("/proc/cmdline", "r")) == NULL) {
        T_D("fopen /proc/cmdline FAILED! [%s]\n", strerror(errno));
        return VTSS_RC_ERROR;
    }
    if ( (fgets(str, size, fp)) == NULL) {
        T_D("fgets FAILED! [%s]\n", strerror(errno));
        fclose(fp);
        return VTSS_RC_ERROR;
    }
    /* cleanup */
    fclose(fp);
    return VTSS_OK;
}

/* Find out active fis section */
const char *firmware_fis_act(void)
{

    char str[1024];
    const char *mtd_act = "linux";

    if ( (firmware_cmdline_get(str, sizeof(str))) == VTSS_OK) {
        if ( strstr(str, "fis_act=linux.bk") ) {
            mtd_act = "linux.bk";
        } else {
            mtd_act = "linux";
        }
        return mtd_act;
    }
    return "linux";  /* fallback */
}

/* Find out alternate fis section, if applicable */
BOOL firmware_has_alt(void)
{
    vtss_mtd_t act, alt;

    if ( (vtss_mtd_open(&act, "linux")) == VTSS_OK && (vtss_mtd_open(&alt, "linux.bk")) == VTSS_OK) {
        if ( (vtss_mtd_close(&act)) == VTSS_OK && (vtss_mtd_close(&alt)) == VTSS_OK)
            return TRUE;
    }
    return FALSE;
}

/* Find out which fis section should be upgraded */
const char *firmware_fis_to_update(void)
{
    return ( strcmp((firmware_fis_act()), "linux") == 0 && (firmware_has_alt()) ) ? "linux.bk" : "linux";
}

/* Find out which fis section should be kept untouched */
const char *firmware_fis_to_keep(void)
{
    if (firmware_has_alt()) {
        return ( strcmp((firmware_fis_act()), "linux") == 0 ) ? "linux" : "linux.bk";
    } else {
        return nullptr;
    }
}

static bool firmware_is_board_compatible(const char *machine)
{
    vtss_mtd_t act;
    bool ret = true;    // Assume compatible by default
    if (vtss_mtd_open(&act, firmware_fis_act()) == VTSS_OK) {
        mscc_firmware_vimage_t fw;
        if (mscc_firmware_fw_vimage_get(&act, &fw) == VTSS_OK) {
            ret = (strncmp(machine, fw.machine, sizeof(fw.machine)) == 0);
            T_I("%s platform: Expect %s, have %s", ret ? "Compatible" : "Incompatible", fw.machine, machine);
        }
        vtss_mtd_close(&act);
    }
    return ret;
}

mesa_rc firmware_check(const unsigned char *buffer, size_t length)
{
    const char *msg;

    VTSS_ASSERT(buffer != NULL);

    {
        const mscc_firmware_vimage_t *fw = (const mscc_firmware_vimage_t *) buffer;
        mscc_firmware_vimage_tlv_t tlv;
        const u8 *tlvdata;

        if (fw->imglen > length) {
            T_D("Image too small: %u > %zu", fw->imglen, length);
            return FIRMWARE_ERROR_INVALID;
        }

        if (mscc_vimage_hdrcheck(fw, &msg) != 0) {
            T_D("Invalid image: %s", msg);
            return FIRMWARE_ERROR_INVALID;
        }

        // Check the machine name matches the current machine name
        if (!firmware_is_board_compatible(fw->machine)) {
            return FIRMWARE_ERROR_INCOMPATIBLE_TARGET;
        }

        // Authenticate
        if ((tlvdata = mscc_vimage_find_tlv(fw, &tlv, MSCC_STAGE1_TLV_SIGNATURE))) {
            if (!mscc_vimage_validate(fw, tlvdata, tlv.data_len)) {
                T_D("Validation fails");
                return FIRMWARE_ERROR_AUTHENTICATION;
            }
        } else {
            T_D("Invalid image: Signature missing");
            return FIRMWARE_ERROR_SIGNATURE;
        }

        // Ensure we have at least a kernel TLV
        if ((tlvdata = mscc_vimage_find_tlv(fw, &tlv, MSCC_STAGE1_TLV_KERNEL)) == NULL) {
            T_D("No kernel in image?");
            return FIRMWARE_ERROR_NO_CODE;
        }

        // Do we have MFI stage2 as well?
        if (length > fw->imglen) {
            // Yes - so check it
            u32 s2len = length - fw->imglen;
            const u8 *s2ptr = buffer + fw->imglen;
            const mscc_firmware_vimage_stage2_tlv_t *s2tlv;
            T_D("Stage2 at %p, length %d", s2ptr, s2len);
            while(s2len > sizeof(*s2tlv)) {
                s2tlv = (const mscc_firmware_vimage_stage2_tlv_t*) s2ptr;
                T_D("Stage2 TLV at %p, type %u", s2tlv, s2tlv->type);
                if (!mscc_vimage_stage2_check_tlv(s2tlv, s2len, true)) {
                    T_D("Stage2 TLV error at %p, offset %08zx", s2ptr, s2ptr - buffer);
                    return FIRMWARE_ERROR_INVALID;
                }
                s2ptr += s2tlv->tlv_len;
                s2len -= s2tlv->tlv_len;
            }
        }
    }

    return VTSS_OK;
}

// Return TRUE if mtd is already programmed - FALSE if update is needed
BOOL firmware_mtd_checksame(cli_iolayer_t *io,
                            vtss_mtd_t *mtd,
                            const char *name,
                            const u8 *buffer,
                            size_t length)
{
    size_t off;
    ssize_t nread;
    u8 readbuf[8*1024];
    cli_io_printf(io, "Checking old image %s... ", name);
    lseek(mtd->fd, 0, SEEK_SET);
    for (off = 0; off < length; off += nread) {
        size_t rreq = MIN(length - off, sizeof(readbuf));
        nread = read(mtd->fd, readbuf, rreq);
        if(nread == 0) {
            break;    // eof
        } else if(nread > 0) {
            if(memcmp(buffer+off, readbuf, nread) != 0) {
                cli_io_printf(io, "needs update\n");
                return FALSE;    // Difference found
            }
        } else {
            cli_io_printf(io, "read error at offset 0x%08zx\n", off);
            return FALSE;    // Bail out - pretend different
        }
    }
    cli_io_printf(io, "already updated\n");
    return TRUE;    // Read all, all the same
}

void firmware_setstate(cli_iolayer_t *io, fw_state_t state, const char *name)
{
    const char *msg;
    switch(state) {
        case FWS_BUSY:
            msg = "Error: Update already in progress";
            if (io)
                cli_io_printf(io, "%s\n", msg);
            else
                firmware_status_set(msg);
            break;
        case FWS_ERASING:
            msg = "Erasing flash...";
            if (io)
                cli_io_printf(io, "%s", msg);
            else
                firmware_status_set(msg);
            break;
        case FWS_ERASED:
            if (io)
                cli_io_printf(io, "done\n");
            else
                firmware_status_set("Erased flash");
            break;
        case FWS_PROGRAMMING:
            msg = "Programming flash...";
            if (io)
                cli_io_printf(io, "%s", msg);
            else
                firmware_status_set(msg);
            break;
        case FWS_PROGRAMMED:
            if (io)
                cli_io_printf(io, "done\n");
            else
                firmware_status_set("Programmed flash");
            break;
        case FWS_SWAPPING:
            msg = "Swapping images...";
            if (io)
                cli_io_printf(io, "%s", msg);
            else
                firmware_status_set(msg);
            break;
        case FWS_SWAP_DONE:
            if (io)
                cli_io_printf(io, "done\n");
            else
                firmware_status_set("Swapped swapped active and backup image");
            break;
        case FWS_SWAP_FAILED:
            if (io)
                cli_io_printf(io, "failed!\n");
            else
                firmware_status_set("Error: Swapping images failed");
            break;
        case FWS_PROGRAM_FAILED:
            if (io)
                cli_io_printf(io, "failed!\n");
            else
                firmware_status_set("Error: Programming flash failed");
            break;
        case FWS_ERASE_FAILED:
            if (io)
                cli_io_printf(io, "failed!\n");
            else
                firmware_status_set("Error: Erasing flash failed");
            break;
        case FWS_SAME_IMAGE:
            msg = "Error: Flash already updated";
            if (io)
                cli_io_printf(io, "%s\n", msg);
            else
                firmware_status_set(msg);
            break;
        case FWS_INVALID_IMAGE:
            msg = "Error: Invalid image";
            if (io)
                cli_io_printf(io, "%s\n", msg);
            else
                firmware_status_set(msg);
            break;
        case FWS_UNKNOWN_MTD:
            msg = "Error: Firmware flash device not found";
            if (io)
                cli_io_printf(io, "%s: %s\n", name, msg);
            else
                firmware_status_set(msg);
            break;
        case FWS_REBOOT:
            msg = "Restarting, please wait...";
            if (io)
                cli_io_printf(io, "%s", msg);
            else
                firmware_status_set(msg);
            break;
        case FWS_DONE:
            if (io)
                cli_io_printf(io, "Firmware update completed. Restart system to activate new firmware.\n");
            else
                firmware_status_set(NULL);
            break;
    }
}

void firmware_stage2_cleanup()
{
    DIR *dp;
    struct dirent *dirp;
    mesa_rc rc;
    const char *primary = "linux", *backup = "linux.bk";

    char slot_a[128] = {};
    char slot_b[128] = {};

    bool slot_a_valid = false;
    bool slot_b_valid = false;

    T_D("firmware_stage2_erase");

    // Figure out what files to keep
    rc = firmware_image_stage2_name_get(primary, slot_a, sizeof(slot_a));
    if (rc == VTSS_RC_OK) {
        T_D("Got stage2 filename for '%s': %s", primary, slot_a);
        slot_a_valid = true;
    } else {
        T_D("No stage2 filename for '%s'", primary);
    }

    rc = firmware_image_stage2_name_get(backup, slot_b, sizeof(slot_b));
    if (rc == VTSS_RC_OK) {
        T_D("Got stage2 filename for '%s': %s", backup, slot_b);
        slot_b_valid = true;
    } else {
        backup = "split_linux_hi";    // Special case for split bootstrap
        rc = firmware_image_stage2_name_get(backup, slot_b, sizeof(slot_b));
        if (rc == VTSS_RC_OK) {
            T_D("Got stage2 filename for '%s': %s", backup, slot_b);
            slot_b_valid = true;
        } else {
            T_D("No stage2 filename for '%s'", backup);
        }
    }

    // Now, delete everything in /switch/stage2/ except for 'slot_a' and
    // 'slot_b'

    // Open directory to iterate over the files
    dp = opendir(FIRWARE_STAGE2);
    if (!dp) {
        if (errno != ENOENT) {
            T_D("Failed to open %s - error %s", FIRWARE_STAGE2, strerror(errno));
        }
        goto EXIT;
    }

    // Iterate
    while ((dirp = readdir(dp)) != nullptr) {
        if (strcmp(dirp->d_name, ".") == 0 || strcmp(dirp->d_name, "..") == 0) {
            continue;
        }

        // Do not delete the stage2 file used by primary
        if (slot_a_valid && (strcmp(dirp->d_name, slot_a) == 0)) {
            T_D("Do not delete slot_a: %s", slot_a);
            continue;
        }

        // Do not delete the stage2 file used by backup
        if (slot_b_valid && (strcmp(dirp->d_name, slot_b) == 0)) {
            T_D("Do not delete slot_b: %s", slot_b);
            continue;
        }

        // Nobody is using this, delete it!
        if (unlinkat(dirfd(dp), dirp->d_name, 0) == -1) {
            T_W("Failed to erase the stage2 image: %s - error %s", dirp->d_name,
                strerror(errno));
        } else {
            T_D("Stage2 image %s was erased successfully", dirp->d_name);
        }
    }

EXIT:
    closedir(dp);
}

#if 0
// Check same image against the current active fis
static mesa_rc firmware_update_checksame(cli_iolayer_t *io,
                                         const unsigned char *buffer,
                                         size_t length)
{
    mesa_rc rc = VTSS_RC_OK;
    vtss_mtd_t mtd_act;   // mtd device that is currently running

    // open
    if ((rc = vtss_mtd_open(&mtd_act,firmware_fis_act())) != VTSS_OK) {
        firmware_setstate(io, FWS_UNKNOWN_MTD, firmware_fis_act());
        return VTSS_RC_ERROR;
    }

    if (firmware_mtd_checksame(io, &mtd_act, firmware_fis_act(), buffer, length)) {
        firmware_setstate(io, FWS_SAME_IMAGE, firmware_fis_act());
        rc = FIRMWARE_ERROR_SAME;
    }

    vtss_mtd_close(&mtd_act);  // cleanup
    return rc;
}
#endif

mesa_rc firmware_update_stage1(cli_iolayer_t *io,
                               const unsigned char *buffer,
                               size_t length,
                               const char *mtd_name,
                               const char *sb_file_name,
                               const char *sb_stage2_name)
{
    mesa_rc rc = VTSS_RC_OK;
    vtss_mtd_t mtd;       // mtd device that is going to be upgraded
    mscc_firmware_sideband_t *sb;

    T_D("Update %s length %zu", mtd_name, length);

    // TODO - This will become a problem. You may have two different images with
    // identical stage1 (different stage 2).
    // I suggest that we simply drop this feature.
    //
    // checksame first
#if 0    // Until fixed for stage1/stage2
    if ((rc = firmware_update_checksame(io, buffer, length)) != VTSS_RC_OK) {
        return rc;
    }
#endif

    // Pre-allocate sideband data blob (include NULL termination)
    // We always have a filename, optionally stage2 name
    VTSS_ASSERT(sb_file_name);
    sb = mscc_vimage_sideband_create();
    if (sb) {
        sb = mscc_vimage_sideband_add_tlv(sb, (u8 *) sb_file_name, strlen(sb_file_name)+1, MSCC_FIRMWARE_SIDEBAND_FILENAME);
    }
    if (sb && sb_stage2_name) {
        T_D("Adding sb_stage2_name: %s", sb_stage2_name);
        sb = mscc_vimage_sideband_add_tlv(sb, (u8 *) sb_stage2_name, strlen(sb_stage2_name)+1, MSCC_FIRMWARE_SIDEBAND_STAGE2_FILENAME);
    }
    if (!sb) {
        T_E("Failed to allocate sideband data");
        return FIRMWARE_ERROR_MALLOC;
    }

    if ((rc = vtss_mtd_open(&mtd, mtd_name)) != VTSS_OK) {
        firmware_setstate(io, FWS_UNKNOWN_MTD, mtd_name);
        goto EXIT;
    }

    firmware_setstate(io, FWS_ERASING, mtd_name);
    if ((rc = vtss_mtd_erase(&mtd, length)) != VTSS_OK) {
        firmware_setstate(io, FWS_ERASE_FAILED, mtd_name);
        T_D("FAILED: vtss_mtd_erase");
        goto EXIT;
    }

    firmware_setstate(io, FWS_ERASED, mtd_name);
    firmware_setstate(io, FWS_PROGRAMMING, mtd_name);
    if ((rc = vtss_mtd_program(&mtd, buffer, length)) != VTSS_OK) {
        firmware_setstate(io, FWS_PROGRAM_FAILED, mtd_name);
        T_D("FAILED: vtss_mtd_program");
        goto EXIT;
    }

    // Add sideband data
    mscc_vimage_sideband_update_crc(sb);
    if ((rc = mscc_vimage_sideband_write(&mtd, sb, MSCC_FIRMWARE_ALIGN(length, mtd.info.erasesize))) != VTSS_OK) {
        firmware_setstate(io, FWS_PROGRAM_FAILED, mtd_name);
        T_D("FAILED: mscc_vimage_sideband_write");
        goto EXIT;
    }

    T_D("Stage1: All done");
    firmware_setstate(io, FWS_PROGRAMMED, mtd_name);

EXIT:
    if (sb) {
        VTSS_FREE(sb);
    }
    vtss_mtd_close(&mtd);
    return rc;
}

mesa_rc firmware_check_bootloader_simg(const u8 *buffer,
                                       size_t length,
                                       u32 *type)
{
    mesa_rc rc = FIRMWARE_ERROR_INVALID;
    simage_t simg;
    if (simage_parse(&simg, buffer, length) == VTSS_OK) {
        u32 arch = 0, chip = 0, imgtype = 0;
        if (!simage_get_tlv_dword(&simg, SIMAGE_TLV_ARCH, &arch) ||
            arch != MY_CPU_ARC) {
            T_D("Invalid or missing architecture: %d", arch);
            rc = FIRMWARE_ERROR_WRONG_ARCH;
        } else if (!simage_get_tlv_dword(&simg, SIMAGE_TLV_CHIP, &chip) ||
                   misc_chip2family(chip) != misc_chipfamily()) {
            T_D("Firmware image architecture does not match system: CHIP was %04x, have %04x",
                misc_chip2family(chip), misc_chipfamily());
            rc = FIRMWARE_ERROR_WRONG_ARCH;
        } else if (!simage_get_tlv_dword(&simg, SIMAGE_TLV_IMGTYPE, &imgtype)) {
            T_D("Image type TLV not present");
        } else {
            *type = imgtype;
            rc = VTSS_OK;
        }
    } else {
        T_D("Bad Image, invalid format");
    }
    return rc;
}

static
mesa_rc firmware_check_bootloader(const u8 *buffer,
                                  size_t length,
                                  const char **mtd_name)
{
    u32 imgtype;
    mesa_rc rc = firmware_check_bootloader_simg(buffer, length, &imgtype);
    if (rc == VTSS_OK) {
        if (imgtype == SIMAGE_IMGTYPE_BOOT_LOADER) {
            *mtd_name = "RedBoot";
        } else {
            rc = FIRMWARE_ERROR_WRONG_ARCH;
        }
    }
    return rc;
}

mesa_rc firmware_flash_mtd(cli_iolayer_t *io,
                           const char *mtd_name,
                           const unsigned char *buffer,
                           size_t length)
{
    vtss_mtd_t mtd;
    mesa_rc rc;
    if ((rc = vtss_mtd_open(&mtd, mtd_name)) != VTSS_OK) {
        cli_io_printf(io, "Unable to open '%s' for Flash image update\n", mtd_name);
        return rc;
    }

    if (mtd.info.size < length) {
        vtss_mtd_close(&mtd);
        return FIRMWARE_ERROR_SIZE;
    }

    if (!firmware_mtd_checksame(io, &mtd, mtd_name, buffer, length)) {
        cli_io_printf(io, "Erasing '%s'...", mtd_name);
        if (vtss_mtd_erase(&mtd, length) == VTSS_OK) {
            cli_io_printf(io, " done!\n");
            cli_io_printf(io, "Programming '%s'...", mtd_name);
            if (vtss_mtd_program(&mtd, buffer, length) == VTSS_OK) {
                cli_io_printf(io, " done!\n");
            } else {
                cli_io_printf(io, " failed!\n");
                rc = FIRMWARE_ERROR_FLASH_PROGRAM;
            }
        } else {
            cli_io_printf(io, " failed!\n");
            rc = FIRMWARE_ERROR_FLASH_ERASE;
        }
    } else {
        rc = FIRMWARE_ERROR_SAME;
    }

    vtss_mtd_close(&mtd);
    return rc;
}

mesa_rc firmware_flash_mtd_if_needed(cli_iolayer_t *io,
                                     const char *mtd_name,
                                     const unsigned char *buffer,
                                     size_t length)
{
    mesa_rc rc = firmware_flash_mtd(io, mtd_name, buffer, length);
    // Same is also OK;
    return rc == FIRMWARE_ERROR_SAME ? VTSS_RC_OK : rc;
}

mesa_rc firmware_update_stage2_bootloader(cli_iolayer_t *io,
                                          const unsigned char *s2ptr,
                                          size_t s2len)
{
    const unsigned char *origin = s2ptr;
    const mscc_firmware_vimage_stage2_tlv_t *s2tlv, *s2bootloader = NULL;
    mesa_rc rc = VTSS_RC_OK;

    T_D("Stage2 at %p, length %zd", s2ptr, s2len);

    // Find *last* bootloader TLV, if any
    while(s2len > sizeof(*s2tlv)) {
        s2tlv = (const mscc_firmware_vimage_stage2_tlv_t*) s2ptr;
        T_D("Stage2 TLV at %p", s2tlv);
        // This may have been checked before, but...
        if (!mscc_vimage_stage2_check_tlv(s2tlv, s2len, true)) {
            T_D("Stage2 TLV error at %p, offset %08zx", s2ptr, s2ptr - origin);
            return FIRMWARE_ERROR_INVALID;
        } else {
            mscc_firmware_vimage_stage2_tlv_t tlvcopy;
            // TLV may be unaligned
            memcpy(&tlvcopy, s2tlv, sizeof(tlvcopy));
            // See if its a bootloader
            if (tlvcopy.type == MSCC_STAGE2_TLV_BOOTLOADER) {
                T_D("Stage2 bootloader TLV at %p, offset %08zx", s2ptr, s2ptr - origin);
                s2bootloader = s2tlv;
            }
        }
        s2ptr += s2tlv->tlv_len;
        s2len -= s2tlv->tlv_len;
    }

    if (s2bootloader) {
        mscc_firmware_vimage_stage2_tlv_t tlvcopy;
        const unsigned char *bootdata = s2bootloader->value;
        const char *mtd_name;
        // TLV may be unaligned
        memcpy(&tlvcopy, s2bootloader, sizeof(tlvcopy));
        T_D("Stage2 bootloader update, len = %d", tlvcopy.data_len);
        rc = firmware_check_bootloader(bootdata, tlvcopy.data_len, &mtd_name);
        if (rc == VTSS_RC_OK) {
            rc = firmware_flash_mtd_if_needed(io, mtd_name, bootdata, tlvcopy.data_len);
        }
    }

    return rc;
}

/*
 * API: Asynchronous firmware update.
 * Returns VTSS_OK if update is started, any other FIRMWARE_ERROR_xxx code otherwise
 */
mesa_rc firmware_update_async(cli_iolayer_t *io,
                              const unsigned char *buffer,
                              size_t length,
                              const char *filename,
                              mesa_restart_t restart,
                              int upgrade_dual)
{
    mesa_rc rc;
    auto fw = manager.get();
    if (fw.ok()) {
        fw->init(nullptr, firmware_max_download);
        fw->attach_cli(io);
        fw->write(buffer, length);
        fw->set_filename(filename);
        fw->append_filename_tlv();
		fw->lntn_upgrade_dual_flag_set(upgrade_dual);
        if (fw->check()) {
            manager.store_async(vtss::move(fw));
            rc = FIRMWARE_ERROR_IN_PROGRESS;
        } else {
            rc = FIRMWARE_ERROR_INVALID;
        }
    } else {
        rc = FIRMWARE_ERROR_BUSY;
    }

    return rc;
}

static bool firmware_is_bootstrapped(const char *fis)
{
    vtss_mtd_t mtd;
    bool result = false;

    if (vtss_mtd_open(&mtd, fis) == VTSS_OK) {
        off_t base = mscc_firmware_sideband_get_offset(&mtd);
        if (base) {
            mscc_firmware_sideband_t *sb = mscc_vimage_sideband_read(&mtd, base);
            if (sb) {
                char pathname[PATH_MAX];
                mscc_firmware_vimage_tlv_t tlv;
                const char *name;
                if ((name = (const char *) mscc_vimage_sideband_find_tlv(sb, &tlv, MSCC_FIRMWARE_SIDEBAND_STAGE2_FILENAME))) {
                    // TLV data *is* NULL terminated
                    (void) snprintf(pathname, sizeof(pathname), "/switch/stage2/%s", name);
                    if (access(name, F_OK)) {
                        T_D("stage2 file %s FOUND", name);
                        result = true;
                    } else {
                        T_D("stage2 file %s does not exist", name);
                    }
                } else {
                    T_D("No stage2 filename tlv found in %s sideband", fis);
                }
                VTSS_FREE(sb);
            } else {
                T_D("Sideband read error for in %s, offset %08llx", fis, base);
            }
        } else {
            T_D("No sideband for in %s", fis);
        }
        vtss_mtd_close(&mtd);
    }

    return result;
}

static mesa_rc firmware_bootstrap_ubi()
{
    mesa_rc rc = MESA_RC_OK;
    Firmware_ubi ubi;
    u8 cnt = 0;
    const u8 max_retry = 2;

    // retry once in case of failure.
    do {
        if ((rc = ubi.ubiumount()) != VTSS_OK) {
            T_D("Umount ubifs failed.\n");
            cnt++;
            continue;
        }

        if ((rc = ubi.ubidetach()) != VTSS_OK) {
            T_D("Detach ubi device failed.\n");
            cnt++;
            continue;
        }

        if ((rc = ubi.ubiformat()) != VTSS_OK) {
            T_D("Format ubi device failed.\n");
            cnt++;
            continue;
        }

        if ((rc = ubi.ubiattach()) != VTSS_OK) {
            T_D("Attach ubi device failed.\n");
            cnt++;
            continue;
        }

        if ((rc = ubi.ubimkvol(0)) != VTSS_OK) {
            T_D("Create ubi volume failed.\n");
            cnt++;
            continue;
        }

        if ((rc = ubi.ubimount()) != VTSS_OK) {
            T_D("Mount ubi device failed.\n");
            cnt++;
            continue;
        }
    } while ((cnt < max_retry) && (rc != VTSS_OK));

    T_D("Bootstrap ubi done %s.\n", (rc == VTSS_OK) ? "ok" : "failed");
    return rc;
}

mesa_rc lntn_firmware_bootstrap_async(cli_iolayer_t *io,
                                      const unsigned char *buffer,
                                      size_t length,
                                      const char *filename,
                                      int upgrade_dual)
{
    const char  *mtd_name = firmware_fis_to_update();
    size_t      mtdsize;
    auto        fw = manager.get();

    // 1. Confirm whether to execute the bootstrap
    if (firmware_is_bootstrapped(firmware_fis_act())) {
        T_W("System is already bootstrapped, please use normal firmware upgrade.\n");
        return MESA_RC_ERROR;
    }

    // 2. Confirm MTD size
    if ((mtdsize = firmware_section_size(mtd_name)) == 0) {
        T_W("Error: MTD name '%s' not found\n", mtd_name);
        return MESA_RC_ERROR;
    }

    // 3. Do bootstrap
    // Bootstrap both nand and nor with the corresponding image part

    // Bootstrap nand flash into ubi
    if (firmware_bootstrap_ubi() != MESA_RC_OK) {
        T_W("Flash format failed\n");
        return MESA_RC_ERROR;
    }

    if (!fw.ok()) {
        T_W("Firware update already in progress.\n");
        return FIRMWARE_ERROR_BUSY;
    }

    fw->init(nullptr, firmware_max_download);
    fw->attach_cli(io);
    fw->write(buffer, length);
    fw->set_filename(filename);

    if (!fw->check()) {
        T_W("Invalid image, need a MFI image\n");
        return FIRMWARE_ERROR_INVALID;
    }

    fw->lntn_upgrade_dual_flag_set(upgrade_dual);
    manager.store_async(vtss::move(fw));

    return FIRMWARE_ERROR_IN_PROGRESS;
}

static void firmware_thread(vtss_addrword_t data)
{
    // Wait until first INIT_DONE event.
    msg_wait(MSG_WAIT_UNTIL_INIT_DONE, VTSS_MODULE_ID_FIRMWARE);

    for(;;) {
        T_D("main loop");
        FwWrap fw = manager.get_async();
        T_D("got image");
        if (fw.ok()) {
            T_D("Firmware update: Image is %zu length filename %s", fw->length(), fw->filename());
            if (fw->lntn_upgrade_dual_flag_get() && !firmware_is_bootstrapped(firmware_fis_act())) {
                T_D("Do bootstrap.");
                fw->bootstrap();
            } else {
                T_D("Do update.");
                fw->update();
            }
            // Only returns if error occurred
            fw->reset();
        }
    }
}

mesa_rc firmware_update_from_usb_async(unsigned char *filename, unsigned int filename_size)
{
    usb_filename_size = filename_size;

    usb_filename = (unsigned char *) malloc(usb_filename_size);
    if (!usb_filename) {
        T_W("malloc failed!");
        return MESA_RC_ERROR;
    }
    memcpy(usb_filename, filename, usb_filename_size);

    firmware_transfer_thread_lock.lock(false);

    return MESA_RC_OK;
}

static void firmware_transfer_thread(vtss_addrword_t data)
{
    unsigned char   *filebuf = NULL;
    unsigned int    filesize;
    usb_rc_t        rc;
    mesa_rc         result;
    char            usb_err_buf[64];

    // Wait until first INIT_DONE event.
    msg_wait(MSG_WAIT_UNTIL_INIT_DONE, VTSS_MODULE_ID_FIRMWARE);

    for(;;) {
        firmware_transfer_thread_lock.wait();

        firmware_status_set("Transferring image...\n");
        lntn_sysled_system_fast_blink_red();

        if ((rc = usb_file_read(usb_filename, usb_filename_size, &filebuf, &filesize)) != USB_RC_OK) {
            T_W("%s\n", usb_error_txt(rc));
            memset(usb_err_buf, 0, sizeof(usb_err_buf));
            snprintf(usb_err_buf, sizeof(usb_err_buf), "[USB]Err:%s", usb_error_txt(rc));
            firmware_status_set(usb_err_buf);
            lntn_sysled_system_solid_green();
        } else {
            result = firmware_update_async(NULL, (const u8 *)filebuf, filesize, (const char *)usb_filename, MESA_RESTART_COOL, false);
            if(result == VTSS_OK || result == FIRMWARE_ERROR_IN_PROGRESS) {
                firmware_status_set("Flashing, please wait...");
            } else {
                firmware_status_set("Firmware Upload Error");
                lntn_sysled_system_solid_green();
            }
        }

        firmware_transfer_thread_lock.lock(true);
        free(filebuf);
        free(usb_filename);
    }
}

static void _upload_status_get(mesa_rc rc, BOOL b_firmware)
{
    SIMPLE_LOCK();

    switch(rc) {
        case VTSS_OK:
            if ( b_firmware ) {
                g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_IN_PROGRESS;
            } else {
                g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_SUCCESS;
            }
            break;

        case FIRMWARE_ERROR_IN_PROGRESS:
            g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_IN_PROGRESS;
            break;

        case FIRMWARE_ERROR_IP:
            g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_ERROR_IP;
            break;

        case FIRMWARE_ERROR_TFTP:
            g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_ERROR_TFTP;
            break;

        case FIRMWARE_ERROR_BUSY:
            g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_ERROR_BUSY;
            break;

        case FIRMWARE_ERROR_MALLOC:
            g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_ERROR_MEMORY_INSUFFICIENT;
            break;

        case FIRMWARE_ERROR_INVALID:
            g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_ERROR_INVALID_IMAGE;
            break;

        case FIRMWARE_ERROR_FLASH_PROGRAM:
            g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_ERROR_WRITE_FLASH;
            break;

        case FIRMWARE_ERROR_SAME:
            g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_ERROR_SAME_IMAGE_EXISTED;
            break;

        case FIRMWARE_ERROR_CURRENT_UNKNOWN:
            g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_ERROR_UNKNOWN_IMAGE;
            break;

        case FIRMWARE_ERROR_CURRENT_NOT_FOUND:
            g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_ERROR_FLASH_IMAGE_NOT_FOUND;
            break;

        case FIRMWARE_ERROR_UPDATE_NOT_FOUND:
            g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_ERROR_FLASH_ENTRY_NOT_FOUND;
            break;

        case FIRMWARE_ERROR_CRC:
            g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_ERROR_CRC;
            break;

        case FIRMWARE_ERROR_SIZE:
            g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_ERROR_IMAGE_SIZE;
            break;

        case FIRMWARE_ERROR_FLASH_ERASE:
            g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_ERROR_ERASE_FLASH;
            break;

        case FIRMWARE_ERROR_INCOMPATIBLE_TARGET:
            g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_ERROR_INCOMPATIBLE_TARGET;
            break;

        default:
            g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_NONE;
            break;
    }
    SIMPLE_UNLOCK();
}

static void _firmware_control_image_upload(void)
{
    mesa_rc rc = VTSS_RC_ERROR;

    switch( g_image_upload.type ) {
        case VTSS_APPL_FIRMWARE_UPLOAD_IMAGE_TYPE_BOOTLOADER:
            rc = firmware_icli_load_image(cli_get_io_handle(), "RedBoot", g_image_upload.url, check_bootloader);
            break;

        case VTSS_APPL_FIRMWARE_UPLOAD_IMAGE_TYPE_FIRMWARE:
            {
                auto fw = manager.get();
                if (fw.ok()) {
                    fw->init(nullptr, firmware_max_download);
                    if ((rc = fw->download(g_image_upload.url)) == MESA_RC_OK) {
                        if (!fw->check()) {
                            rc = FIRMWARE_ERROR_INVALID;
                        } else {
                            manager.store_async(vtss::move(fw));
                            rc = FIRMWARE_ERROR_IN_PROGRESS;
                        }
                    }
                } else {
                    rc = FIRMWARE_ERROR_BUSY;
                }
            }
            break;

        default:
            /* should not get there as vtss_appl_firmware_control_image_upload_set() has checked parameters */
            T_D("invalid g_image_upload.type (%u)", g_image_upload.type);
            return;
    }

    /* get upload status */
    _upload_status_get(rc, g_image_upload.type == VTSS_APPL_FIRMWARE_UPLOAD_IMAGE_TYPE_FIRMWARE);
}

static void _firmware_upload_thread(vtss_addrword_t data)
{
    vtss_flag_value_t flag;

    msg_wait(MSG_WAIT_UNTIL_MASTER_UP_POST, VTSS_MODULE_ID_FIRMWARE);

    /* read image status to initialize g_image_status[] */
    if (vtss_appl_firmware_status_image_entry_get(VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_BOOTLOADER,
                                                  &g_image_status[VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_BOOTLOADER]) != VTSS_OK) {
        //T_E("_firmware_status_image_entry_get( VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_BOOTLOADER )\n");
    }
    if (vtss_appl_firmware_status_image_entry_get(VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ACTIVE_FIRMWARE,
                                                  &g_image_status[VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ACTIVE_FIRMWARE]) != VTSS_OK) {
        //T_E("_firmware_status_image_entry_get( VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ACTIVE_FIRMWARE )\n");
    }
    if (vtss_appl_firmware_status_image_entry_get(VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ALTERNATIVE_FIRMWARE,
                                                  &g_image_status[VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ALTERNATIVE_FIRMWARE]) != VTSS_OK) {
        //T_E("_firmware_status_image_entry_get( VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ALTERNATIVE_FIRMWARE )\n");
    }

    /* process upload event */
    while (1) {
        while (msg_switch_is_master()) {
            flag = vtss_flag_wait(&g_firmware_upload_thread_flag,
                                  FIRMWARE_UPLOAD_THREAD_FLAG_UPLOAD,
                                  VTSS_FLAG_WAITMODE_OR_CLR);
            if (flag & FIRMWARE_UPLOAD_THREAD_FLAG_UPLOAD) {
                _firmware_control_image_upload();
            }
        }

        msg_wait(MSG_WAIT_UNTIL_MASTER_UP_POST, VTSS_MODULE_ID_FIRMWARE);
    }
}

/****************************************************************************/
/*  Initialization functions                                                */
/****************************************************************************/

static void clean_dir(const char * dirname)
{
    DIR *dp;
    struct dirent *dirp;

    // Open directory to iterate over the files
    dp = opendir(dirname);
    if (!dp) {
        return;
    }

    // Iterate
    while ((dirp = readdir(dp)) != nullptr) {
        if (strcmp(dirp->d_name, ".") == 0 || strcmp(dirp->d_name, "..") == 0) {
            continue;
        }
        // Remove all else
        (void) unlinkat(dirfd(dp), dirp->d_name, 0);
    }

    closedir(dp);
}


void
firmware_init_os(vtss_init_data_t *data)
{
    switch (data->cmd) {
        case INIT_CMD_INIT:
            clean_dir(DLD_DIR);
            vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
                               firmware_thread,
                               0,
                               "FIRMWARE",
                               nullptr,
                               0,
                               &firmware_thread_handle,
                               &firmware_thread_block);

            firmware_transfer_thread_lock.lock(true);
            vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
                               firmware_transfer_thread,
                               0,
                               "Transfer firmware image from USB",
                               nullptr,
                               0,
                               &firmware_transfer_thread_handle,
                               &firmware_transfer_thread_block);

            vtss_flag_init(&g_firmware_upload_thread_flag);  /* No initialization needed */
            vtss_flag_maskbits(&g_firmware_upload_thread_flag, 0);

            memset(&g_image_upload, 0, sizeof(g_image_upload));
            memset(&g_image_status, 0, sizeof(g_image_status));
            vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
                               _firmware_upload_thread,
                               0,
                               "Firmware Upload",
                               nullptr,
                               0,
                               &g_firmware_upload_thread_handle,
                               &g_firmware_upload_thread_block);
            break;
        default:
            break;
    }
}


/*
==============================================================================

    Public APIs in vtss_appl\include\vtss\appl\firmware.h

==============================================================================
*/
static const char *bin_findstr(const char *haystack, const char *haystack_end,
                               const char *needle, size_t nlen)
{
    /* memchr used here to scan memory for a special character */
    while(haystack && haystack < haystack_end &&
          (haystack = (const char *)memchr(haystack, needle[0], haystack_end -haystack))) {
        if (strncmp(haystack, needle, nlen) == 0)
            return haystack;
        haystack++;
    }
    return NULL;
}

static const char *firmware_bin_findstr(const char *haystack,
                                        size_t sz_buffer,
                                        const char *needle)
{
    return bin_findstr(haystack, haystack + sz_buffer, needle, strlen(needle));
}

static mesa_rc
_firmware_bootloader_status_get(vtss_mtd_t *mtd, vtss_appl_firmware_status_image_t  *image_entry)
{
    mesa_rc rc = FIRMWARE_ERROR_INVALID;
    image_entry->version[0] = image_entry->built_date[0] = image_entry->code_revision[0] = '\0';
    if (mtd->info.size) {
        char *blob = NULL;

        VTSS_MALLOC_CAST(blob, mtd->info.size);
        if (blob != NULL) {
            if (read(mtd->fd, blob, mtd->info.size) != -1) {
                const char *idstr = "Non-certified release, ", *hit;
                char *split;

                if ((hit = firmware_bin_findstr(blob, mtd->info.size, idstr)) != NULL) {
                    hit += strlen(idstr);  /* Adcance past marker */
                    strncpy(image_entry->version, hit, sizeof(image_entry->version));
                    image_entry->version[VTSS_APPL_FIRMWARE_VERSION_LEN] = 0;

                    if ((split = strstr(image_entry->version, (idstr = " - built "))) != NULL) {
                        *split = '\0';  /* Split version string */
                        split += strlen(idstr);  /* Advance past marker */
                        strncpy(image_entry->built_date, split, sizeof(image_entry->built_date));
                        image_entry->built_date[VTSS_APPL_FIRMWARE_DATE_LEN] = 0;
                        if ((split = strchr(image_entry->built_date, '\n'))) {
                            *split = '\0';  /* Nuke newline */
                        }
                        rc = VTSS_OK;
                    } else {
                        T_E("Split failed?");
                    }
                } else {
                    T_E("RedBoot special idstr [%s] not found!\n", idstr);
                }
            } else {
                T_E("Read RedBoot failed [%s]!\n", strerror(errno));
            }
            VTSS_FREE(blob);
        } else {
            T_E("blob malloc failed!\n");
            rc = FIRMWARE_ERROR_MALLOC;
        }
    } else {
        T_E("mtd:%s size is %d!\n", mtd->dev, mtd->info.size);
    }
    vtss_mtd_close(mtd);
    return rc;
}


mesa_rc _firmware_parse_metadata(vtss_appl_firmware_status_image_t *imageEntry, u8 *blob, size_t length)
{
    const char *delim = "\n";
    char *ptr;
    blob[length] = '\0';
    char *saveptr; // Local strtok_r() context
    for (ptr = strtok_r((char*)blob, delim, &saveptr); ptr != NULL; ptr = strtok_r(NULL, delim, &saveptr)) {
        char *data = strchr(ptr, ':');
        if (data) {
            *data++ = '\0';
            while(*data == ' ')
                data++;
            T_D("%s: metadata [%s,%s]", imageEntry->name, ptr, data);
            if        (strcmp(ptr, "Version") == 0) {
                strncpy(imageEntry->version, data, sizeof(imageEntry->version));
            } else if (strcmp(ptr, "Date") == 0) {
                strncpy(imageEntry->built_date, data, sizeof(imageEntry->built_date));
            } else if (strcmp(ptr, "Revision") == 0) {
                strncpy(imageEntry->code_revision, data, sizeof(imageEntry->code_revision));
            }
        }
    }
    return VTSS_RC_OK;
}

mesa_rc
_firmware_vimage_status_get(vtss_mtd_t                        *mtd,
                            vtss_appl_firmware_status_image_t *imageEntry)
{
    mesa_rc rc = FIRMWARE_ERROR_INVALID;
    imageEntry->version[0] = imageEntry->built_date[0] = imageEntry->code_revision[0] = '\0';
    size_t hdrlen = sizeof(mscc_firmware_vimage_t);
    u8 *hdrbuf = (u8*) VTSS_MALLOC(hdrlen);
    if (mtd->info.size && hdrbuf) {
        if (read(mtd->fd, hdrbuf, hdrlen) == hdrlen) {
            {
                // New style image
                mscc_firmware_vimage_t *fw = (mscc_firmware_vimage_t *) hdrbuf;
                const char *errmsg;
                if (mscc_vimage_hdrcheck(fw, &errmsg) == 0) {
                    mscc_firmware_vimage_tlv_t tlv;
                    off_t toff;
                    if ((toff = mscc_vimage_mtd_find_tlv(fw, mtd, &tlv, MSCC_STAGE1_TLV_METADATA)) != 0) {
                        // NB: Reusing hdrbuf/fw
                        hdrbuf = (u8 *) VTSS_REALLOC(hdrbuf, tlv.data_len + 1);
                        if (hdrbuf &&
                            pread(mtd->fd, hdrbuf, tlv.data_len, toff) == tlv.data_len) {
                            hdrbuf[tlv.data_len] = '\0';
                            rc = _firmware_parse_metadata(imageEntry, hdrbuf, tlv.data_len);
                        }
                    }
                } else {
                    T_D("Invalid unified image: %s", errmsg);
                }
            }
        } else {
            T_E("Read \"%s\" image header failed [%s]!\n", imageEntry->name, strerror(errno));
        }
    }
    if (hdrbuf) {
        VTSS_FREE(hdrbuf);
    }
    vtss_mtd_close(mtd);
    return rc;
}

/**
 * \brief Get firmware image entry
 *
 * To read status of each firmware image.
 *
 * \param image_id    [IN]  (key) Image ID
 * \param image_entry [OUT] The current status of the image
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_firmware_status_image_entry_get(u32 image_id,
                                                  vtss_appl_firmware_status_image_t *const image_entry)
{
    mesa_rc rc;
    vtss_mtd_t mtd;
    const char *fis_name;

    /* check parameter */
    if (image_id > VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ALTERNATIVE_FIRMWARE) {
        T_D("Invalid image_id( %u )", image_id);
        return VTSS_RC_ERROR;
    }

    memset(image_entry, 0, sizeof(vtss_appl_firmware_status_image_t));
    image_entry->type = static_cast<vtss_appl_firmware_status_image_type_t>(image_id);
    switch ( image_id ) {
        case VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_BOOTLOADER:
            if ( (rc = vtss_mtd_open(&mtd, (fis_name = "RedBoot"))) != VTSS_OK &&
                 (rc = vtss_mtd_open(&mtd, (fis_name = "Redboot"))) != VTSS_OK &&
                 (rc = vtss_mtd_open(&mtd, (fis_name = "redboot"))) != VTSS_OK) {
                T_D("%s (%u) cannot be located", fis_name, image_id);
                return FIRMWARE_ERROR_IMAGE_NOT_FOUND;
            }
            strcpy(image_entry->name, fis_name);
            return _firmware_bootloader_status_get(&mtd, image_entry);
            break;
        case VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ACTIVE_FIRMWARE:
        case VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ALTERNATIVE_FIRMWARE:
            fis_name = (image_id == VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ACTIVE_FIRMWARE) ? "linux" : "linux.bk";
            if ( (rc = vtss_mtd_open(&mtd, fis_name)) != VTSS_OK) {
                T_D("%s (%u) cannot be located", fis_name, image_id);
                return FIRMWARE_ERROR_IMAGE_NOT_FOUND;
            }
            strcpy(image_entry->name, fis_name);
            return _firmware_vimage_status_get(&mtd, image_entry);
            break;
        default:
            T_D("ImageId (%u) not available", image_id);
            return VTSS_RC_ERROR;
    }

    return VTSS_RC_OK;
}

/**
 * \brief Get firmware image entry
 *
 * To read firmware status of each switch.
 *
 * \param switch_id    [IN]  (key) Image number, starts from 1 and 1 is always the boot loader
 * \param imageEntry [OUT] The current status of the image
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_firmware_status_switch_entry_get(
    u32                                 switch_id,
    vtss_appl_firmware_status_switch_t  *const switch_entry
)
{
    vtss_usid_t         usid;
    msg_switch_info_t   info;

    usid = (vtss_usid_t)switch_id;

    if (usid < VTSS_USID_START || usid >= VTSS_USID_END) {
        T_D("usid out of range");
        return VTSS_RC_ERROR;
    }
    vtss_isid_t isid = topo_usid2isid(usid);
    if (msg_switch_info_get(isid, &info) != VTSS_RC_OK) {
        return VTSS_RC_ERROR;
    }

    if (switch_entry == NULL) {
        T_D("switch_entry == NULL");
        return VTSS_RC_ERROR;
    }

    memset(switch_entry, 0, sizeof(vtss_appl_firmware_status_switch_t));

    snprintf(switch_entry->chip_id, VTSS_APPL_FIRMWARE_CHIP_ID_LEN, "VSC%-5x", info.info.api_inst_id);
    snprintf(switch_entry->board_type, VTSS_APPL_FIRMWARE_BOARD_TYPE_LEN, "%s", sysutil_board_type_name(isid));
    switch_entry->port_cnt = info.info.port_cnt;
    snprintf(switch_entry->product, VTSS_APPL_FIRMWARE_PRODUCT_LEN, "%s", info.product_name);
    snprintf(switch_entry->version, VTSS_APPL_FIRMWARE_VERSION_LEN, "%s", misc_software_version_txt());
    snprintf(switch_entry->built_date, VTSS_APPL_FIRMWARE_DATE_LEN, "%s", misc_software_date_txt());

    return VTSS_RC_OK;
}

/**
 * \brief Get firmware status to image upload
 *
 * To get the status of current upload operation.
 *
 * \param status [OUT] The status of current upload operation.
 *
 * \return VTSS_OK if the operation succeeded.
 */
mesa_rc vtss_appl_firmware_status_image_upload_get(
    vtss_appl_firmware_status_image_upload_t    *const status
)
{
    const char *firmware_status = firmware_status_get();

    /* check parameter */
    if (status == NULL) {
        T_D("status == NULL");
        return VTSS_RC_ERROR;
    }

    T_D("Trying to get image_upload status!");
    SIMPLE_LOCK();
    if (!g_image_upload.upload) {
        if (!strcmp(firmware_status, "idle")) {
            status->status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_NONE;
        } else if (!strncmp(firmware_status, "Error", 5)) {
            status->status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_ERROR_UNKNOWN_IMAGE;
        } else {
            status->status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_IN_PROGRESS;
        }
    } else {
        status->status = g_image_upload.upload_status;
    }
    SIMPLE_UNLOCK();

    return VTSS_RC_OK;
}

/**
 * \brief Get global parameters of firmware control
 *
 * \param globals [OUT] The global parameters.
 *
 * \return VTSS_OK if the operation succeeded.
 */
mesa_rc vtss_appl_firmware_control_globals_get(
    vtss_appl_firmware_control_globals_t            *const globals
)
{
    /* check parameter */
    if ( globals == NULL ) {
        T_D("globals == NULL");
        return VTSS_RC_ERROR;
    }

    globals->swap_firmware = FALSE;
    return VTSS_RC_OK;
}

/**
 * \brief Set global parameters of firmware control
 *
 * \param globals [IN] The global parameters.
 *
 * \return VTSS_OK if the operation succeeded.
 */
mesa_rc vtss_appl_firmware_control_globals_set(
    const vtss_appl_firmware_control_globals_t      *const globals
)
{



    mesa_restart_t restart_type = MESA_RESTART_COOL;


    T_D("Trying to set control_globals!");
    /* check parameter */
    if ( globals == NULL ) {
        T_D("globals == NULL");
        return VTSS_RC_ERROR;
    }

    if ( globals->swap_firmware ) {
        /* works only when active image is "linux" &&
         * alternative image "linux.bk" is available
         */
        if (strcmp("linux", firmware_fis_act()) == 0) {
            T_D("Active image is: %s", "linux");
            if (firmware_has_alt()) {
                if (firmware_swap_images() == VTSS_RC_OK) {
                    T_D("Swap firmware ok!");
                    /* Now restart */
                    if (vtss_switch_standalone()) {
                        (void) control_system_reset(TRUE, VTSS_USID_ALL, restart_type);
                    } else {
                        (void) control_system_reset(FALSE, VTSS_USID_ALL, restart_type);
                    }
                    return VTSS_OK;
                } else {
                    T_D("Swap firmware failed!");
                }
            } else {
                T_D("%s not available", "linux.bk");
            }
        } else {
            T_D("Active image is %s, expect %s", firmware_fis_act(), "linux");
        }
        return VTSS_RC_ERROR;
    }
    return VTSS_RC_OK;
}

/**
 * \brief Get firmware image upload status
 *
 * To get the configuration and status of current upload operation.
 *
 * \param image [OUT] The configuration and status of current upload operation.
 *
 * \return VTSS_OK if the operation succeeded.
 */
mesa_rc vtss_appl_firmware_control_image_upload_get(
    vtss_appl_firmware_control_image_upload_t           *const image
)
{
    /* check parameter */
    if (image == NULL) {
        T_D("image == NULL\n");
        return VTSS_RC_ERROR;
    }

    SIMPLE_LOCK();

    g_image_upload.upload = FALSE;
    memcpy( image, &g_image_upload, sizeof(vtss_appl_firmware_control_image_upload_t) );

    SIMPLE_UNLOCK();

    return VTSS_RC_OK;
}

/**
 * \brief Set firmware image upload
 *
 * To set the configuration to upload image.
 *
 * \param image [IN] The configuration to upload image.
 *
 * \return VTSS_OK if the operation succeeded.
 */
mesa_rc vtss_appl_firmware_control_image_upload_set(const vtss_appl_firmware_control_image_upload_t *const image)
{
    /* check parameter */
    if (image == NULL) {
        T_D("image == NULL");
        return VTSS_RC_ERROR;
    }

    if (image->upload != TRUE && image->upload != FALSE) {
        T_D("invalid image->upload (%u)", image->upload);
        return VTSS_RC_ERROR;
    }

    if (image->type > VTSS_APPL_FIRMWARE_UPLOAD_IMAGE_TYPE_FIRMWARE) {
        T_D("invalid image->type (%u)", image->type);
        return VTSS_RC_ERROR;
    }

    SIMPLE_LOCK();

    /* in uploading */
    if (g_image_upload.upload_status == VTSS_APPL_FIRMWARE_UPLOAD_STATUS_IN_PROGRESS) {
        SIMPLE_UNLOCK();
        return VTSS_RC_ERROR;
    }

    memcpy(&g_image_upload, image, sizeof(g_image_upload));
    g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_NONE;

    if (image->upload == FALSE) {
        SIMPLE_UNLOCK();
        return VTSS_RC_OK;
    }

    /* start uploading */
    g_image_upload.upload_status = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_IN_PROGRESS;

    SIMPLE_UNLOCK();

    vtss_flag_maskbits(&g_firmware_upload_thread_flag, 0);
    vtss_flag_setbits(&g_firmware_upload_thread_flag, FIRMWARE_UPLOAD_THREAD_FLAG_UPLOAD);

    return VTSS_RC_OK;
}
