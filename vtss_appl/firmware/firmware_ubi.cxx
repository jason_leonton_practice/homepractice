/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
*/

#include "firmware_ubi.hxx"

Firmware_ubi::Firmware_ubi() : mtd_num(-1), ubi_num(UBI_NUM), vol_id(UBI_VOL_NUM_AUTO)
{
  FILE *fp;
  char buf[64];
  char *token;
  const char *delim = " :";

  fp = fopen(MTD_INFO, "r");
  if (fp == NULL) {
    T_D("Open %s failed [%s]", MTD_INFO, strerror(errno));
    return;
  }

  while ((fgets(buf, sizeof(buf), fp)) != NULL) {
    // T_D("%s", buf);  // uncomment this for debug
    // process each line
    if (strstr(buf, "rootfs_data")) {
      T_D("mtd device named rootfs_data is present, need format");

      // split targeted line
      char *saveptr; // Local strtok_r() context
      token = strtok_r(buf, delim, &saveptr);
      if (token != NULL) {
        T_D("%s is the rootfs_data", token);
        sscanf(token, "mtd%d", &mtd_num);
      }

      break;
    }
  }

  if (mtd_num == -1) {
    T_D("No rootfs_data");
  } else {
    T_D("mtd_num of rootfs_data is [%d]", mtd_num);
  }

  if (fclose(fp) == EOF) {
    T_D("Close %s failed [%s]", MTD_INFO, strerror(errno));
  }
}

mesa_rc Firmware_ubi::ubiumount()
{
  mesa_rc rc = VTSS_RC_OK;
  FILE *fp;
  char buf[128];
  BOOL need_umount = FALSE;
  char *token;
  const char *delim = " ";

  fp = fopen(MOUNTS_INFO, "r");
  if (fp == NULL) {
    T_D("fopen %s failed [%s]", MOUNTS_INFO, strerror(errno));
    return VTSS_RC_ERROR;
  }

  while ((fgets(buf, sizeof(buf), fp)) != NULL) {
    // T_D("%s", buf);  // uncomment this for debug
    // process each line
    if (strstr(buf, "ubifs")) {
      T_D("ubifs is present, need umount");
      need_umount = TRUE;
      break;
    }
  }

  if(need_umount) {
    // split targeted line
    char *saveptr; // Local strtok_r() context
    token = strtok_r(buf, delim, &saveptr);
    if (token != NULL) {
      token = strtok_r(NULL, delim, &saveptr);  // get mount point
    }
    // NOTE: invoke umount() from sys/mount.h
    if (::umount(token) == -1) {
      T_D("ubifs at %s umount failed [%s]", token, strerror(errno));
      rc = VTSS_RC_ERROR;
    }
    T_D("umount %s ok.", token);
  } else {
    T_D("No need to umount");
  }

  if (fclose(fp) == EOF) {
    T_D("Close %s failed [%s]", MOUNTS_INFO, strerror(errno));
    rc = VTSS_RC_ERROR;
  }
  return rc;
}

mesa_rc Firmware_ubi::ubidetach()
{
  mesa_rc rc = VTSS_RC_OK;
  int res;
  int fd;
  char str[16];  // ubi0
  struct stat stat_buf;
  BOOL need_detach = FALSE;
  int ubi_num = UBI_NUM;

  // check if ubi_dev is attached
  sprintf(str, "/dev/ubi%d", ubi_num);
  if (stat(str, &stat_buf) == -1) {
    T_D("Expect %s not exist [%s]", str, strerror(errno));
  } else {
    need_detach = TRUE;
  }

  if (need_detach) {
    T_D("Need to detach ubi device [%s]", str);

    fd = open(DEVICE_UBI_CTRL, O_RDONLY);
    if (fd == -1) {
      T_D("Open mtd device %s failed [(%d): %s]", DEVICE_UBI_CTRL, fd, strerror(errno));
      return VTSS_RC_ERROR;
    }

    res = ioctl(fd, UBI_IOCDET, &ubi_num);
    close(fd);
    if (res == -1) {
      T_D("Detach ubi device (%s:%d) failed [%s]", DEVICE_UBI_CTRL, fd, strerror(errno));
      rc = VTSS_RC_ERROR;
      goto EXIT;
    }
    T_D("ubidetach [%s] ok", str);

  } else {
    T_D("No need to detach ubi device [%s]", str);
  }

EXIT:
  return rc;
}

mesa_rc Firmware_ubi::ubiformat()
{
  mesa_rc rc = VTSS_RC_OK;
  char cmd[64];  // ubiformat /dev/mtdX -y

  if (mtd_num == -1) {
    T_D("rootfs_data not found in /proc/mtd, no need to format");
    rc = VTSS_RC_ERROR;
  } else {
    // ubiformat is rather complicated, just execute the shell command
    // ubiattach will catch the error, if ubiformat failed
    sprintf(cmd, "ubiformat /dev/mtd%d -y", mtd_num);
    if (system(cmd) == -1) {
      T_D("ubiformat /dev/mtd%d failed", mtd_num);
      rc = VTSS_RC_ERROR;
    }
  }
  return rc;
}

mesa_rc Firmware_ubi::ubiattach()
{
  mesa_rc rc = VTSS_RC_OK;
  struct ubi_attach_req req;
  int fd, res;

  memset(&req, 0x0, sizeof(ubi_attach_req));

  if (mtd_num < 0) {
    T_D("Invalid mtd number [%d] of rootfs_data", mtd_num);
    return VTSS_RC_ERROR;
  }

  // check ubi-user.h for structure info
  req.ubi_num = ubi_num;
  req.mtd_num = mtd_num;
  req.vid_hdr_offset = 0;
  req.max_beb_per1024 = 0;

  fd = open(DEVICE_UBI_CTRL, O_RDONLY);
  if (fd == -1) {
    T_D("Open %s failed [%s]", DEVICE_UBI_CTRL, strerror(errno));
    return VTSS_RC_ERROR;
  }

  res = ioctl(fd, UBI_IOCATT, &req);
  if (res == -1) {
    T_D("Attach %s failed", DEVICE_UBI_CTRL);
    rc = VTSS_RC_ERROR;
  }
  T_D("ubiattach ubi%d (mtd%d) ok.", req.ubi_num, req.mtd_num);

  close(fd);
  return rc;

}

static unsigned long ubi_get_int_prop(int ubi_num, const char *node)
{
    unsigned long value = 0;
    vtss::StringStream ss;
    FILE *fp;
    print_fmt(ss, "/sys/class/ubi/ubi%d/%s", ubi_num, node);
    if ((fp = fopen(ss.cstring(), "r"))) {
        if (fscanf(fp, "%lu", &value) == 0) {
            value = 0;
        }
        fclose(fp);
    }
    T_D("%s = %lu", ss.cstring(), value);
    return value;
}

long long Firmware_ubi::available_space()
{
    long long avail_erase_blocks = ubi_get_int_prop(ubi_num, "avail_eraseblocks");
    long long eraseblock_size = ubi_get_int_prop(ubi_num, "eraseblock_size");
    return avail_erase_blocks * eraseblock_size;
}

mesa_rc Firmware_ubi::ubimkvol(unsigned int mbytes_limit)
{
  mesa_rc rc = VTSS_RC_OK;
  int fd, res;
  struct ubi_mkvol_req req;
  size_t n;
  char str[64];

  memset(&req, 0x0, sizeof(struct ubi_mkvol_req));

  req.vol_id = vol_id;
  req.alignment = 1;  // default
  req.vol_type = UBI_DYNAMIC_VOLUME;
  if (mbytes_limit) {
      req.bytes = mbytes_limit * 1024UL * 1024UL;  // 64MiB
  } else {
      auto avail = available_space();
      req.bytes = avail;
  }

  T_D("ubimkvol, %llu bytes", (unsigned long long) req.bytes);

  strncpy(req.name, VOLUME_NAME, UBI_MAX_VOLUME_NAME + 1);
  n = strlen(req.name);
  req.name_len = n;

  sprintf(str, "/dev/ubi%d", ubi_num);
  fd = open(str, O_RDONLY);
  if (fd == -1) {
    T_D("Open %s failed [%s]", str, strerror(errno));
    return VTSS_RC_ERROR;
  }

  res = ioctl(fd, UBI_IOCMKVOL, &req);

  close(fd);

  if (res == -1) {
    T_D("ubimkvol %s failed.", str);
    return VTSS_RC_ERROR;
  }

  vol_id = req.vol_id;
  T_D("ubimkvol %s ok.", str);
  return rc;
}

mesa_rc Firmware_ubi::ubimount()
{
  mesa_rc rc = VTSS_RC_OK;
  char str[16];

  sprintf(str, "ubi%d:%s", ubi_num, VOLUME_NAME);
  if (::mount(str, MOUNT_DIR, "ubifs", 0, NULL) == -1) {
    T_D("mount ubifs %s failed [%s]", str, strerror(errno));
    return VTSS_RC_ERROR;
  }

  T_D("mount ubifs %s ok.", str);
  return rc;
}
