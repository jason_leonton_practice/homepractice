/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

#ifndef _FIRMWARE_INFO_API_H_
#define _FIRMWARE_INFO_API_H_

#include "vtss/appl/firmware.h"
#include "firmware.h"
#include <list>
#include <map>
#include <vector>
#include "vtss_os_wrapper_linux.hxx"

#ifdef VTSS_SW_OPTION_JSON_RPC_NOTIFICATION
#include "json_rpc_api.hxx"
#include <vtss/basics/json/stream-parser.hxx>
#include <vtss/basics/json/stream-parser-callback.hxx>
#endif
#include <vtss/basics/trace.hxx>

//*****************************************************************************************
//* Because our public header doesn't support c++ std library, the public header
//* types are defined below.
//******************************************************************************************
/**
 * \brief License description information
*/
typedef struct {
    std::string description;                /*!< License description */
} vtss_appl_firmware_license_description_t; /*!< License information */

/**
 * \brief Getting license description information.
 *
 * \param image_id      [IN]  Image ID
 * \param section_id    [IN]  Section ID
 * \param component_id  [IN]  Component ID
 * \param desciption    [OUT] The license description
 *
 * \return VTSS_RC_OK if the operation succeeded.
*/
mesa_rc vtss_appl_firmware_status_licenses_description_get(u32  image_id,
                                                           u32  section_id,
                                                           u32  component_id,
                                                           vtss_appl_firmware_license_description_t *const description);

/**
 * \brief Basic application/library license information
*/
typedef struct {
    u32         component_id;                                /*!< ID for the component */
    std::string component_name;                              /*!< Name of the component (application/library) - e.g. "uclibc". */
    std::string version;                                     /*!< Version number - e.g. "1.0.12". */
    std::string license_type;                                /*!< License type - e.g. "LGPLv2.1+". */
    std::string url;                                         /*!< URL for where to find the source code. */
} vtss_appl_firmware_status_basic_license_t; /*!< Basic application/library license information */


/**
 * \brief Get firmware basic licenses information.
 *
 * \param image_id       [IN]  (key) Image number, starts from 0 and 0 is always the boot loader
 * \param section_id     [IN]  (key) Section number for the section within the image.
 * \param component_id   [IN]  (key) Licenses component number.
 * \param basic_licenses [OUT] The basic license information.
 *
 * \return VTSS_OK if the operation succeeded.
 */
mesa_rc vtss_appl_firmware_status_licenses_basic_get(u32  image_id,
                                                     u32  section_id,
                                                     u32  component_id,
                                                     vtss_appl_firmware_status_basic_license_t *const basic_license);


/**
 * \brief Iterate function of licenses information in the MFI image.
 *
 * To get first or get next license information.
 *
 * \param prev_image_id     [IN]  Previous image ID     - Setting to NULL = Use first ID.
 * \param next_image_id     [OUT] Next image ID.
 * \param prev_section_id   [IN]  Previous section ID   - Setting to NULL = Use first ID.
 * \param next_section_id   [OUT] Next section ID.
 * \param prev_component_id [IN]  Previous component id - Setting to NULL = Use first.
 * \param next_component_id [OUT] Next component id.
 *
 * \return VTSS_RC_OK if the operation succeeded.
 */
mesa_rc vtss_appl_firmware_status_licenses_itr(const u32 *prev_image_id,     u32 *next_image_id,
                                               const u32 *prev_section_id,   u32 *next_section_id,
                                               const u32 *component_id,      u32 *next_component_id);


//*****************************************************************************************
//* Internal types
//******************************************************************************************

// The license information from a specific file
typedef struct {
    std::string file_name; /*!< Name for the file containing the license description */
    std::string text;      /*!< License description */
} vtss_appl_firmware_license_text_t; /*!< License information */

// Vector containing all the licenses text from one file
typedef std::vector<vtss_appl_firmware_license_text_t> firmware_license_text_vec_t;

// Vector containing all the licenses descriptions for all components within a section
typedef std::vector<firmware_license_text_vec_t> components_licenses_text_vec_t;

typedef std::vector<vtss_appl_firmware_status_basic_license_t> basic_licenses_vec_t;

// For getting a vector of firmware_license_text_vec_t
mesa_rc firmware_section_licenses_text_vec_get(
    u32  image_id,
    u32  section_id,
    u32  component_id,
    components_licenses_text_vec_t *const license_text_vec
);


// Key for the map containing the MFI information
struct firmware_info_key_t {
    u32 attribute_id;
    u32 section_id;
    u32 image_id;

    BOOL operator<(const firmware_info_key_t & n) const {
        if (this->image_id < n.image_id) return TRUE;
        if (this->image_id > n.image_id) return FALSE;

        if (this->section_id < n.section_id) return TRUE;
        if (this->section_id > n.section_id) return FALSE;

        if (this->attribute_id < n.attribute_id) return TRUE;
        return FALSE;
    }
};


// Key for the map containing the license information
struct firmware_info_license_section_key_t {
    u32 image_id;
    u32 section_id;

    BOOL operator<(const firmware_info_license_section_key_t & n) const {
        if (this->image_id < n.image_id) return TRUE;
        if (this->image_id > n.image_id) return FALSE;

        if (this->section_id < n.section_id) return TRUE;
        return FALSE;
    }
};

// MAP containing the MFI TLV information
typedef std::map<firmware_info_key_t, vtss_appl_firmware_image_status_tlv_t> tlv_map_t;

// MAP containing the MFI licenses information.
typedef std::map<firmware_info_license_section_key_t, basic_licenses_vec_t> basic_license_map_t;


// Getting the whole map with all the MFI TLV information.
mesa_rc firmware_image_tlv_map_get(tlv_map_t *tlv_map_i);

// Getting the whole map with all the MFI License information.
mesa_rc firmware_basic_license_map_get(basic_license_map_t *license_map_i);

struct FirmwareBuf {
    FirmwareBuf() : size(0), data(nullptr) {}

    FirmwareBuf(size_t s) : size(s) {
        data = (char *)malloc(size);
        if (!data) size = 0;
    }

    FirmwareBuf(const FirmwareBuf &) = delete;
    FirmwareBuf &operator=(const FirmwareBuf &) = delete;

    FirmwareBuf(FirmwareBuf &&rhs) : size(rhs.size), data(rhs.data) {
        rhs.data = nullptr;
        rhs.size = 0;
    }

    FirmwareBuf &operator=(FirmwareBuf &&rhs) {
        if (data) free(data);
        data = rhs.data;
        size = rhs.size;
        rhs.data = 0;
        rhs.size = 0;
        return *this;
    }

    void clear() {
        if (data) free(data);
        data = nullptr;
        size = 0;
    }

    ~FirmwareBuf() {
        if (data) free(data);
    }

    explicit operator bool() const { return data; }

    size_t size;
    char *data;
};

//
// JSON Parser for firmware
//

// Constant for getting all licenses for all components instead of a license from a specific component
#define ALL_COMPONENTS 0xFFFFFFFF

#ifdef VTSS_SW_OPTION_JSON_RPC_NOTIFICATION

using namespace vtss;
struct FirmwareLicenseParser : public json::StreamParserCallback {
    FirmwareLicenseParser(BOOL brief_ = TRUE, u32 component_id_get_ = ALL_COMPONENTS) {
        breif = brief_;
        component_id_get = component_id_get_;
        VTSS_TRACE(VTSS_TRACE_GRP_INFO, NOISE) << "Constructor";
    }

    Action array_start() override {
        VTSS_TRACE(VTSS_TRACE_GRP_INFO, DEBUG) << "Got array start";
        return StreamParserCallback::ACCEPT;
    }

    void array_end() override {
        VTSS_TRACE(VTSS_TRACE_GRP_INFO, DEBUG) << "Got array end";
    }

    Action object_start() override {
        VTSS_TRACE(VTSS_TRACE_GRP_INFO, DEBUG) << "Got object start. licenses_element_found:" << licenses_element_found
                                               << " component_id_get:" << component_id_get << " current_component_id:" << current_component_id;
        current_component_id++;
        if (licenses_element_found) {
            return StreamParserCallback::ACCEPT;
        }

        BOOL read_this = (component_id_get == current_component_id || component_id_get == ALL_COMPONENTS || breif);
        VTSS_TRACE(VTSS_TRACE_GRP_INFO, DEBUG) << "read this:" << read_this;
        if (read_this) {
            return StreamParserCallback::ACCEPT;
        }

        if (current_component_id < component_id_get) {
            return StreamParserCallback::SKIP;
        } else {
            return StreamParserCallback::STOP;
        }

    }

    void object_end() override
    {
        VTSS_TRACE(VTSS_TRACE_GRP_INFO, DEBUG) << "Got object end.";
        licenses_element_found = FALSE;
        basic_license.push_back(temp_license);
        temp_license = {};
    }

    Action object_element_start(const std::string &s) override {
       VTSS_TRACE(VTSS_TRACE_GRP_INFO, DEBUG) << "Got object element start: " << s << " breif:" << breif;
       element = s;

       if (element == "licenses") {
           licenses_element_found = TRUE;

            if (breif) {
               return StreamParserCallback::Action::SKIP;
            }
        }
        return StreamParserCallback::Action::ACCEPT;
    }

    void object_element_end() override {
        if (licenses_element_found) {
            components_licenses_text_vec.push_back(license_text_vec);
            license_text_vec = {};
        }


        VTSS_TRACE(VTSS_TRACE_GRP_INFO, DEBUG) << "Got object element end. Element found:" << licenses_element_found;
    }

    void null() override {
        VTSS_TRACE(VTSS_TRACE_GRP_INFO, DEBUG) << "Null";
    }

    void boolean(bool b) override {
        VTSS_TRACE(VTSS_TRACE_GRP_INFO, DEBUG) << "Got a bool: " << b;
    }

    void number_value(uint32_t i) override {
        VTSS_TRACE(VTSS_TRACE_GRP_INFO, DEBUG) << "Got a uint32_t: " << i;
    }

    void number_value(int32_t i) override {
        VTSS_TRACE(VTSS_TRACE_GRP_INFO, DEBUG) << "Got a int32_t: " << i;
    }

    void number_value(uint64_t i) override {
        VTSS_TRACE(VTSS_TRACE_GRP_INFO, DEBUG) << "Got a uint64_t: " << i;
    }

    void number_value(int64_t i) override {
        VTSS_TRACE(VTSS_TRACE_GRP_INFO, DEBUG) << "Got a int64_t: " << i;
    }

    void string_value(const std::string &&s) override {
        if (element == "name") {
            VTSS_TRACE(VTSS_TRACE_GRP_INFO, DEBUG) << "Got name:" << s
                                                  << " - licenses element found:" << licenses_element_found;
            if (licenses_element_found) {
                license_file_name = s;
            } else {
                temp_license.component_id = current_component_id - 1; // Subtract one because current_component_id is incremented when the object is found
                temp_license.component_name = s;
            }
            return;
        }

        if (element == "version") {
            VTSS_TRACE(VTSS_TRACE_GRP_INFO, NOISE) << "Got version:" << s;
            temp_license.version = s;
            return;
        }

        if (element == "license_type") {
            VTSS_TRACE(VTSS_TRACE_GRP_INFO, NOISE) << "Got license type:" << s;
            temp_license.license_type = s;
            return;
        }

        if (element == "url") {
            VTSS_TRACE(VTSS_TRACE_GRP_INFO, DEBUG) << "Got url:" << s;
            temp_license.url = s;
            return;
        }

        if (element == "license_text") {
            VTSS_TRACE(VTSS_TRACE_GRP_INFO, INFO) << "license_text found, breif:" << breif;
            if (!breif) {
                vtss_appl_firmware_license_text_t license_text;
                license_text.file_name = license_file_name;
                license_text.text = s;
                license_text_vec.push_back(license_text);
                VTSS_TRACE(VTSS_TRACE_GRP_INFO, NOISE) << "Got license_text:" << license_text.text;
            }
            return;
        }

        VTSS_TRACE(VTSS_TRACE_GRP_INFO, ERROR) << "Unknown element: "<< element << " string:" << s;
    }

    void stream_error() override {
        VTSS_TRACE(VTSS_TRACE_GRP_INFO, INFO) << "Got stream error";
    }

    std::string element;
    std::string license_file_name;
    BOOL licenses_element_found = FALSE;

    vtss_appl_firmware_status_basic_license_t temp_license;
    firmware_license_text_vec_t license_text_vec;
    components_licenses_text_vec_t components_licenses_text_vec;

    BOOL new_license_found = FALSE;
    BOOL breif;
    u32 current_component_id = 0;
    u32 component_id_get = 0;

public:
    basic_licenses_vec_t basic_license;

    components_licenses_text_vec_t components_licenses_text_vec_get(void) {
        return components_licenses_text_vec;
    }

    basic_licenses_vec_t basic_license_get(void) {
            return basic_license;
    }

    BOOL license_found(void) {
        return basic_license.size() > 0;
    }
};

#endif // VTSS_SW_OPTION_JSON_RPC_NOTIFICATION
#endif // _FIRMWARE_INFO_API_H_


// ***************************************************************************
//
//  End of file.
//
// ***************************************************************************
