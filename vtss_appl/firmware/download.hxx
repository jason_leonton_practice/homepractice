/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

#ifndef _FIRMWARE_DOWNLOAD_H_
#define _FIRMWARE_DOWNLOAD_H_

#include <string>

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include <mscc/ethernet/switch/api.h>
#include <vtss/basics/mutex.hxx>

#include "cli_io_api.h"
#include "led_api.h"
#include "firmware.h"    // XXX

#ifdef __cplusplus
extern "C" {
#endif

#define DLD_FWFN    "fwXXXXXX"
#define DLD_DIR     VTSS_FS_FLASH_DIR "dld/"
#define DLD_FILE    DLD_DIR DLD_FWFN
#define STAGE2_DIR  VTSS_FS_FLASH_DIR "stage2/"
#define STAGE2_FILE STAGE2_DIR DLD_FWFN

struct FirmwareDownload {
    ~FirmwareDownload() { reset(); }
    FirmwareDownload() { }

    void reset();
    void init(void *mem, size_t maxsize = 0);
    void attach_cli(cli_iolayer_t *io) { cli_ = io; }

    const size_t maxsize() { return maxsize_; }
    size_t length() { return length_; }
    bool write_error() { return write_error_; }
    void stderr_add(const char *p, ssize_t len) { errbuf_.append(p, len); }
    const char *last_error() { return errbuf_.c_str(); }
    void set_filename(const char *name);
    const char * filename() { return fname_.c_str(); }

    ssize_t write(const void *p, size_t);
    const unsigned char *map();
    mesa_rc download(const char *url);
    mesa_rc tftp_get(const char *file, const char *server, int *err);
    mesa_rc http_get(const char *url);

    bool check();
    mesa_rc update(mesa_restart_t restart = MESA_RESTART_COOL);
    mesa_rc load_nor(const char *mtdname, const char *type);
    mesa_rc load_raw(const char *mtdname);
    mesa_rc append_filename_tlv();
    int lntn_upgrade_dual_flag_get() { return lntn_upgrade_dual_flag; }
    void lntn_upgrade_dual_flag_set(int upgrade_dual) { lntn_upgrade_dual_flag=upgrade_dual; }

    mesa_rc update_mptest();
    mesa_rc bootstrap(mesa_restart_t restart = MESA_RESTART_COOL);

  private:
    void seal();    // Seal downloaded data
    mesa_rc firmware_update_stage2(const mscc_firmware_vimage_t *fw, const char *stage2_file);
    mesa_rc update_stage1_and_stage2(const char *mtd_name);
    uint8_t *mem_ = nullptr;
    size_t maxsize_ = 0;
    size_t length_ = 0;
    size_t mapsize_ = 0;
    char fsfile[128];
    std::string errbuf_, fname_;
    int fd_ = -1;
    void *map_ = NULL;
    bool write_error_ = false;
    cli_iolayer_t *cli_ = nullptr;
	int lntn_upgrade_dual_flag;
};

struct FwWrap;

struct FwManager {
    friend struct FwWrap;

    enum Status { FREE, IN_USE, IN_USE_ASYNC };

    FwManager();
    FwWrap get();
    FwWrap get_async();
    void   store_async(FwWrap &&fw);

  private:
    void give_it_back() {
        vtss::lock_guard<vtss::Critd> fwlock(__FILE__, __LINE__, m_);
        assert(status != FREE);
        obj.reset();
        status = FREE;
    }

    vtss::Critd m_;
    vtss::condition_variable<vtss::Critd> cv_;
    Status status = FREE;
    FirmwareDownload obj;
};

extern FwManager manager;

struct FwWrap {
    friend struct FwManager;

    FwWrap() : data(nullptr) {}
    FwWrap(const FwWrap &rhs) = delete;
    FwWrap(FwWrap &&rhs) : data(rhs.data) {
        rhs.data = nullptr;
    }

    void assign(FwWrap &&rhs) {
        release();
        data = rhs.data;
        rhs.data = nullptr;
    }

    void release() {
        if (data) {
            manager.give_it_back();
        }
    }

    bool ok() { return data; }

    FirmwareDownload *operator->() {
        return data;
    }

    ~FwWrap() {
        release();
    }

  private:
    FwWrap(FirmwareDownload *d) : data(d) {}
    FirmwareDownload *data;
};

#ifdef __cplusplus
}
#endif

#endif // _FIRMWARE_DOWNLOAD_H_


// ***************************************************************************
//
//  End of file.
//
// ***************************************************************************
