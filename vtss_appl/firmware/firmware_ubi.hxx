/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

#ifndef __FIRMWARE_UBI_HXX__
#define __FIRMWARE_UBI_HXX__

#include <mtd/ubi-user.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/reboot.h>
#include <linux/reboot.h>
#include <errno.h>

#include "main.h"
#include "firmware.h"

#define DEVICE_UBI_CTRL "/dev/ubi_ctrl"
#define MOUNTS_INFO     "/proc/mounts"
#define MTD_INFO        "/proc/mtd"
#define MOUNT_DIR       "/switch"
#define VOLUME_NAME     "switch"
#define UBI_NUM         0
#define UBI_MAX_VOLUME_NAME 127



class Firmware_ubi {
 public:
  // constructor
  Firmware_ubi();
  // umount filesystem
  mesa_rc ubiumount();
  // detaches MTD devices from UBI devices
  mesa_rc ubidetach();
  // formats empty flash, erases flash and preserves erase counters, flashes UBI
  // images to MTD devices.
  mesa_rc ubiformat();
  // attaches MTD devices
  mesa_rc ubiattach();
  // create volume with specific size limit. If mbytes_limit = 0 then all available space is used.
  mesa_rc ubimkvol(unsigned int mbytes_limit);
  // mount filesystem
  mesa_rc ubimount();
  // determine avail space
  long long available_space();
 private:
  // the mtd number corresponding to "rootfs_data"
  int mtd_num;
  // the ubi device number corresponding to mtd_num;
  int ubi_num;
  // volume number
  int vol_id;
};

#endif  // __FIRMWARE_UBI_HXX__
