/*
 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
*/

#include "firmware_serializer.hxx"
#include "vtss/basics/expose/json.hxx"

using namespace vtss;
using namespace vtss::json;
using namespace vtss::expose::json;
using namespace vtss::appl::firmware::interfaces;

// Special function for JSON get all, because the image has to be decompression every time a new
// component license should be found. With this all component is found at once, and the
// decompression only need to be done once.
mesa_rc vtss_appl_firmware_license_descr_get_all(const vtss::expose::json::Request *req,
                                                 vtss::ostreamBuf *os) {

    using namespace vtss;
    using namespace vtss::expose;
    using namespace vtss::expose::json;

    HandlerFunctionExporterParser handler_in(req);
    typedef ResponseMap<ResponseMapRowSingleKeySingleVal> response_type;

    // The handler type is derived from the response type.
    typedef typename response_type::handler_type handler_type;

    // Create an exporter which the output parameters is written to
    response_type response(os, req->id);

    u32 image_id;
    u32 prev_image_id = 0;
    u32 *prev_image_id_p = &prev_image_id;

    u32 section_id;
    u32 prev_section_id = 0;
    u32 *prev_section_id_p = &prev_section_id;

    u32 component_id;
    u32 *prev_component_id_p = NULL;

    // Looping through all images, sections and components.
    while (vtss_appl_firmware_status_licenses_itr(prev_image_id_p,     &image_id,
                                                  prev_section_id_p,   &section_id,
                                                  prev_component_id_p, &component_id) == VTSS_RC_OK) {

        T_IG(VTSS_TRACE_GRP_INFO, "Get licenses");

        components_licenses_text_vec_t components_licenses_text_vec;
        firmware_section_licenses_text_vec_get(image_id, section_id, ALL_COMPONENTS, &components_licenses_text_vec);

        T_IG(VTSS_TRACE_GRP_INFO, "Serialize");
        for (component_id = 0; component_id != components_licenses_text_vec.size(); ++component_id) {
            handler_type handler_out = response.resultHandler();

            vtss_appl_firmware_license_description_t description ;
            description.description = "";

            for(auto y =  components_licenses_text_vec[component_id].begin(); y !=  components_licenses_text_vec[component_id].end(); ++y) {
                description.description += y->text;
            }


            // add the conf.id member as key
            {
                auto &&key_handler = handler_out.keyHandler();

                serialize(key_handler, firmware_image_tlv_image_id_index(image_id));
                serialize(key_handler, firmware_image_tlv_section_id_index(section_id));
                serialize(key_handler, firmware_image_license_component_id_index(component_id));

                if (!key_handler.ok()) {
                    T_IG(VTSS_TRACE_GRP_INFO, "Key Handler error");
                    return VTSS_RC_ERROR;
                }
            }

            // serialize the value (as normal)
            {
                auto &&val_handler = handler_out.valHandler();
                serialize(val_handler, description);
                if (!val_handler.ok()) {
                    T_IG(VTSS_TRACE_GRP_INFO, "Handler error, description:%s", description.description.c_str());
                    return VTSS_RC_ERROR;
                }
            }
        }
        *prev_section_id_p = section_id + 1;
        *prev_image_id_p = image_id;
    }
    T_NG(VTSS_TRACE_GRP_INFO, "Done");
    return VTSS_RC_OK;
}

namespace vtss {
void json_node_add(Node *node);
}  // namespace vtss

#define NS(N, P, D) static vtss::expose::json::NamespaceNode N(&P, D);
static NamespaceNode ns_firmware("firmware");
extern "C" void vtss_appl_firmware_json_init() { json_node_add(&ns_firmware); }

NS(ns_status, ns_firmware, "status");
static TableReadOnly<FirmwareStatusImageEntry> firmware_status_image_entry(&ns_status, "image");
static StructReadOnly<FirmwareStatusImageUploadLeaf> firmware_status_image_upload_leaf(&ns_status, "imageUpload");
static TableReadOnly<FirmwareImageStatusTlvLeaf> firmware_status_image_tlv_leaf(&ns_status, "imageTlv");
static TableReadOnly<FirmwareStatusSwitchEntry> firmware_status_switch_entry(&ns_status, "switch");
static TableReadOnly<FirmwareImageStatusLicenseBasicLeaf> firmware_status_image_license_basic_leaf(&ns_status, "licenseBasic");
static TableReadOnly<FirmwareImageStatusLicenseDescriptionLeaf> firmware_status_image_license_description_leaf(&ns_status, "licenseDescription");

NS(ns_control, ns_firmware, "control");
static StructWriteOnly<FirmwareControlGlobalsLeaf> firmware_control_globals_leaf(&ns_control, "global");
static StructWriteOnly<FirmwareControlCopyConfigLeaf> firmware_control_copy_config_leaf(&ns_control, "imageUpload");
