/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

#include "main.h"
#include "web_api.h"
#include "firmware_api.h"
#ifdef VTSS_SW_OPTION_PRIV_LVL
#include "vtss_privilege_api.h"
#include "vtss_privilege_web_api.h"
#endif
#include "flash_mgmt_api.h"
#include "firmware.h"

#ifdef VTSS_SW_OPTION_USB
#include "usb_api.h"  // For usb_file_write
#endif

/* =================
 * Trace definitions
 * -------------- */
#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_FIRMWARE
#include <vtss_trace_api.h>
/* ============== */

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_FIRMWARE

/****************************************************************************/
/*  Web Handler  functions                                                  */
/****************************************************************************/
static i32 handler_firmware_status(CYG_HTTPD_STATE* p)
{
    const char *firmware_status = firmware_status_get();
    (void)cyg_httpd_start_chunked("html");
    (void)cyg_httpd_write_chunked(firmware_status, strlen(firmware_status));
    (void)cyg_httpd_end_chunked();

    return -1; // Do not further search the file system.
}

static i32 handler_firmware(CYG_HTTPD_STATE* p)
{
    form_data_t     formdata[8];
    int             cnt;
    char            filename[FIRMWARE_IMAGE_NAME_MAX];
    int             upgrade_dual=false; // just upgrade single
    size_t          len;
    char            usb_filename[FIRMWARE_IMAGE_NAME_MAX];
    char            filesrc[FIRMWARE_IMAGE_NAME_MAX];

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_FIRMWARE))
        return -1;
#endif

    if(p->method == CYG_HTTPD_METHOD_POST) {
        if((cnt = cyg_httpd_form_parse_formdata(p, formdata, ARRSZ(formdata))) > 0) {
            int i;
            form_data_t *firmware = NULL;
            mesa_restart_t restart_type = MESA_RESTART_WARM; // Default
            // Figure out which of the entries found in the POST request contains the firmware
            // and whether the coolstart entry exists (meaning that the checkbox is checked on the page).
            for (i = 0; i < cnt; i++) {
                if (!strcmp(formdata[i].name, "firmware")) {
                    firmware = &formdata[i];
                    if (cyg_httpd_form_parse_formdata_filename(p, firmware, filename, sizeof(filename)) == 0) {
                        strncpy(filename, "web-unknown", sizeof(filename));
                    }
                } else if (!strcmp(formdata[i].name, "coolstart")) {
                    restart_type = MESA_RESTART_COOL;
                } else if (!strcmp(formdata[i].name, "file_src")) {
                    len = MIN(ARRSZ(filesrc) - 1, formdata[i].value_len);
                    strncpy(filesrc, formdata[i].value, len);
                    filesrc[len] = 0;
                } else  if (!strcmp(formdata[i].name, "usb_filename")) {
                    len = MIN(ARRSZ(usb_filename) - 1, formdata[i].value_len);
                    strncpy(usb_filename, formdata[i].value, len);
                    usb_filename[len] = 0;
                }
            }

            if (!strcmp(filesrc, "Local")) {
                if (firmware) {
                    mesa_rc result = firmware_update_async(web_get_iolayer(WEB_CLI_IO_TYPE_FIRMWARE), (const u8 *)firmware->value, firmware->value_len, filename, restart_type, upgrade_dual);
                    if(result == VTSS_OK || result == FIRMWARE_ERROR_IN_PROGRESS) {
                        firmware_status_set("Flashing, please wait...");
                        redirect(p, "/upload_flashing.htm");
                    } else {
                        if (strcmp(web_ver(p), "2.0") == 0) {
                            send_custom_textplain_resp(p, 400, "Firmware Upload Error");
                        } else {
                            const char *err_buf_ptr = error_txt(result);
                            send_custom_error(p, "Firmware Upload Error", err_buf_ptr, strlen(err_buf_ptr));
                        }
                    }

                } else {
                    const char *err = "Firmware not found in data";
                    T_E("%s", err);
                    send_custom_error(p, err, err, strlen(err));
                }

            } else {  // USB
                mesa_rc result = firmware_update_from_usb_async((unsigned char *)usb_filename, strlen(usb_filename) + 1);
                if(result != MESA_RC_OK) {
                    const char *err_buf_ptr = error_txt(result);
                    send_custom_error(p, "Firmware Upload Error", err_buf_ptr, strlen(err_buf_ptr));
                } else {
                    if (strcmp(web_ver(p), "2.0") == 0) {
                        send_custom_textplain_resp(p, 200, "");
                    } else {
                        redirect(p, "/upload_flashing.htm");
                    }
                }
            }
        } else {
            cyg_httpd_send_error(CYG_HTTPD_STATUS_BAD_REQUEST);
        }
    } else {                    /* CYG_HTTPD_METHOD_GET (+HEAD) */
        web_send_iolayer_output(WEB_CLI_IO_TYPE_FIRMWARE, NULL, "html");
    }

    return -1; // Do not further search the file system.
}

static i32 handler_sw_select(CYG_HTTPD_STATE* p)
{
    int ct;
    const char *pri_name = "linux", *alt_name = "linux.bk";
    const char *fis_act = NULL, *fis_alt = NULL;
    BOOL have_alt;
    char filename[FIRMWARE_IMAGE_NAME_MAX];
#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_FIRMWARE))
        return -1;
#endif
    (void)cyg_httpd_start_chunked("html");

    fis_act = firmware_fis_act();
    fis_alt = firmware_fis_to_update();
    if (fis_act && strcmp((const char *)fis_act, (const char *)pri_name) == 0) {
        have_alt = (strcmp((const char *)fis_alt, (const char *)alt_name) == 0);
    } else {
        have_alt = FALSE;
    }

    if (firmware_image_name_get("linux", filename, sizeof(filename)) != VTSS_OK) {
        strncpy(filename, fis_act, sizeof(filename));
    }

    /* Current image info */
    ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                  "%s|%s|%s",
                  filename,
                  misc_software_version_txt(),
                  misc_software_date_txt());
    (void)cyg_httpd_write_chunked(p->outbuffer, ct);

    if (have_alt) {
        vtss_appl_firmware_status_image_t image_entry;
        vtss_mtd_t mtd;
        if ( (vtss_mtd_open(&mtd, fis_alt)) != VTSS_OK) {
            T_E("MTD open FAILED! [%s]", fis_alt);
        }

        if (firmware_image_name_get(FALSE, filename, sizeof(filename)) != VTSS_OK) {
            strncpy(filename, fis_alt, sizeof(filename));
        }

        if ( (vtss_appl_firmware_status_image_entry_get(VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ALTERNATIVE_FIRMWARE, &image_entry)) != VTSS_OK) {
            T_W("Image entry get FAILED! [%s]", fis_alt);
        }
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                      "^%s|%s|%s",
                      filename,
                      image_entry.version,
                      image_entry.built_date);
        (void)cyg_httpd_write_chunked(p->outbuffer, ct);
    } else {
        T_I("Expect to have alternate fis!");
    }
    (void)cyg_httpd_end_chunked();
    return -1; // Do not further search the file system.
}

/****************************************************************************/
/*  HTTPD Table Entries                                                     */
/****************************************************************************/
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_firmware_status, "/config/firmware_status", handler_firmware_status);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_firmware, "/config/firmware", handler_firmware);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_sw_select, "/config/sw_select", handler_sw_select);
