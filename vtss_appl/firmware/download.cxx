/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <linux/fs.h>

#include <vtss/basics/trace_basics.hxx>
#include <vtss/basics/time.hxx>
#include <vtss/basics/notifications/process.hxx>
#include <vtss/basics/notifications/timer.hxx>
#include <vtss/basics/notifications/subject-runner.hxx>

#include "download.hxx"
#include "subject.hxx"
#include "vtss_os_wrapper.h"
#include "vtss_tftp_api.h"
#include "firmware_api.h"
#include "conf_api.h"
#include "vtss_mtd_api.hxx"

#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>

#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_FIRMWARE

void FirmwareDownload::reset()
{
    maxsize_ = 0;
    length_ = 0;
    errbuf_.clear();
    fname_.clear();
    if (map_) {
        munmap(map_, mapsize_);
        map_ = nullptr;
    }
    if (fd_ >= 0) {
        close(fd_);
        if (unlink(fsfile)) {
            T_W("Unlink %s: %s", fsfile, strerror(errno));
        } else {
            T_I("Unlinked %s", fsfile);
        }
        fd_ = -1;
    }
    write_error_ = false;
    cli_ = nullptr;
}

void FirmwareDownload::init(void *mem, size_t maxsize)
{
    reset();
    maxsize_ = maxsize;
    mem_ = (uint8_t *) mem;
    if (mem == nullptr) {
        if (vtss_mtd_rootfs_is_nor()) {
            strncpy(fsfile, VTSS_FS_RUN_DIR DLD_FWFN, sizeof(fsfile));
            fd_ = mkstemp(fsfile);
        } else {
            int flags, res;
            mkdir(DLD_DIR, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
            strncpy(fsfile, DLD_FILE, sizeof(fsfile));
            fd_ = mkstemp(fsfile);
            // Disabling compression at filesystem layer
            res = ioctl(fd_, FS_IOC_GETFLAGS, &flags);
            if (res != 0) {
                T_I("Failed get FS_IOC_GETFLAGS %s", strerror(errno));
            } else {
                flags &= ~FS_COMPR_FL;
                res = ioctl(fd_, FS_IOC_SETFLAGS, &flags);
                if (res != 0) {
                    T_I("Failed get disable FS compression for stage2 file %s", strerror(errno));
                }
            }
        }
        T_D("file = %s", fsfile);
    }
}

bool FirmwareDownload::check()
{
    mesa_rc rc = MESA_RC_ERROR;
    const unsigned char *buffer = map();
    if (buffer) {
        rc = firmware_check(buffer, length_);
    }
    return rc == MESA_RC_OK;
}

ssize_t FirmwareDownload::write(const void *p, size_t dlen)
{
    ssize_t written;
    // Handle overrun
    if (write_error_ || (maxsize_ && dlen + length_ > maxsize_)) {
        write_error_ = true;
        return -1;
    }
    if (mem_) {
        memcpy(mem_ + length_, p, dlen);
        written = dlen;
        T_N("memory write offset %zd: %zd bytes", length_, dlen);
    } else {
        written = ::write(fd_, p, dlen);
        T_N("file write offset %zd: %zd/%zd bytes", length_, dlen, written);
        if (written != dlen) {
            write_error_ = true;
        }
    }
    if (written > 0) {
        length_ += written;
    }
    return written;
}

const unsigned char *FirmwareDownload::map()
{
    if (mem_) {
        return mem_;
    } else {
        if (map_ == NULL) {
            size_t len = length();
            if (len) {
                mapsize_ = (((len-1)/getpagesize())+1) * getpagesize(); // Round up
                map_ = mmap(NULL, mapsize_, PROT_READ, MAP_PRIVATE, fd_, 0);
            }
        }
        return (const unsigned char *) map_;
    }
}

namespace vtss {
namespace notifications {
enum class DownloadState { stopped, running, completed, timeout, writerror, readerror };
struct DownloadHandler : public EventHandler {
    DownloadHandler(SubjectRunner &sr, FirmwareDownload *dld, std::string name) :
            EventHandler(&sr),
            p(&sr, name),
            dld_(dld),
            e_(this),
            e_out(this),
            e_err(this),
            timeout_(this)
    {
    }

    // Callback method for "normal" events
    void execute(Event *e) {
        auto st = p.status(e_);
        // When the process is running, then we can attach the file descriptors
        T_I("State %d, exited = %d", (int) st.state(), st.exited());
        if (st.state() == ProcessState::running) {
            state_ = DownloadState::running;
            sr->timer_add(timeout_, maxwait_);
            e_out.assign(p.fd_out_release());
            sr->event_fd_add(e_out, EventFd::READ);
            e_err.assign(p.fd_err_release());
            sr->event_fd_add(e_err, EventFd::READ);
        }

        if (st.exited()) {
            if (state_ == DownloadState::running) {
                state_ = DownloadState::completed;
            }
            // Return value
            return_code = st.exit_value();
            sr->return_when_out_of_work();
        }
    }

    // Callback method for timer events
    void execute(Timer *e) {
        if (e == &timeout_) {
            T_W("Timeout waiting for data exceeded");
            p.stop_and_wait(false); // Force stop, can't use any data
            state_ = DownloadState::timeout;
        }
    }

    // Callback method for events on one of the file-descriptors
    void execute(EventFd *e) {
        ssize_t res = ::read(e->raw(), b_, sizeof(b_));
        T_N("Read %zd bytes on fd %d", res, e->raw());
        if (res > 0) {
            sr->event_fd_add(*e, EventFd::READ);    // Call again
            if (e == &e_out) {
                if (dld_->write(b_, res) != res) { // Append data, check for overrun
                    sr->event_fd_del(*e);         // Don't listen to this fd anymore
                    T_W("Data write error at %zd, kill process %s", dld_->length(), p.name().c_str());
                    p.stop_and_wait(false); // Force stop, can't use any data
                    state_ = DownloadState::writerror;
                } else {
                    // Restart timer
                    sr->timer_del(timeout_);
                    sr->timer_add(timeout_, maxwait_);
                }
            } else if (e == &e_err) {
                dld_->stderr_add((char*)b_, res);    // Append error message
            }
        } else {
            T_I("Read return %zd, fd %d, state %d", res, e->raw(), (int) state_);
            if (e == &e_out) {
                // Kill data receive timer
                T_D("Stop data timer");
                sr->timer_del(timeout_);
            }
            if (res < 0 && state_ == DownloadState::running) {
                state_ = DownloadState::readerror;
            }
            // Close the file descriptor
            e->close();
        }
    }
    DownloadState state() { return state_; }

    Process p;
    FirmwareDownload *dld_;
    int return_code = -1;
    Event e_;
    EventFd e_out;
    EventFd e_err;
    Timer timeout_;
    const nanoseconds maxwait_ = seconds(5);
    DownloadState state_ = DownloadState::stopped;
    unsigned char b_[32*1024];    // 32k read buffer
};

}  // namespace notifications
}  // namespace vtss


static int download(std::string cmd, vtss::Vector<std::string> &arguments, FirmwareDownload *dld, vtss::notifications::DownloadState *state)
{
    vtss::notifications::SubjectRunner sr("downloader", "downloaderEvents");
    vtss::notifications::DownloadHandler handler(sr, dld, cmd);

    handler.p.executable = cmd;
    handler.p.arguments = arguments;
    handler.p.status(handler.e_);
    handler.p.fd_out_capture = true;
    handler.p.fd_err_capture = true;
    handler.p.run();
    sr.run();

    *state = handler.state();

    T_D("%s done: ret %d, state %d", cmd.c_str(), handler.return_code, (int) handler.state());

    if (handler.state() == vtss::notifications::DownloadState::completed) {
        return handler.return_code;
    }

    const char *reason;
    switch (handler.state()) {
        case vtss::notifications::DownloadState::readerror:
            reason = "Read error";
            break;
        case vtss::notifications::DownloadState::writerror:
            reason = "Write overflow";
            break;
        case vtss::notifications::DownloadState::timeout:
            reason = "Timeout";
            break;
        default:
            reason = "Unknown error";
    }
    dld->stderr_add(reason, strlen(reason));

    // return non-zero always
    return handler.return_code ? handler.return_code : -1;
}

mesa_rc FirmwareDownload::tftp_get(const char *file, const char *server, int *err)
{
    vtss::Vector<std::string> args = {"-g", "-l", "-", "-r", file, server};
    vtss::notifications::DownloadState dlstate;

    T_D("tftp get %s %s", file, server);

    int ret = ::download("/usr/bin/tftp", args, this, &dlstate);

    if (dlstate == vtss::notifications::DownloadState::writerror) {
        *err = VTSS_TFTP_TOOLARGE;
        return MESA_RC_ERROR;
    }

    if (ret) {
        T_D("TFTP: command failed: %d", ret);
        *err = vtss_tftp_err(errbuf_.c_str());
        return MESA_RC_ERROR;
    }

    return MESA_RC_OK;
}

mesa_rc FirmwareDownload::http_get(const char *url)
{
    vtss::Vector<std::string> args = {"-q", "-O", "-", url};
    vtss::notifications::DownloadState dlstate;

    T_D("http get %s", url);

    int ret = ::download("/usr/bin/wget", args, this, &dlstate);

    if (dlstate == vtss::notifications::DownloadState::writerror) {
        return FIRMWARE_ERROR_SIZE;
    }

    if (ret) {
        T_D("HTTP: command failed: %d - %s", ret, errbuf_.c_str());
        return MESA_RC_ERROR;
    }

    return MESA_RC_OK;
}

void FirmwareDownload::seal()
{
    if (fd_ >= 0) {
        close(fd_);
        fd_ = open(fsfile, O_RDONLY);
        assert(fd_ >= 0);
        T_D("Sealed, fd = %d, file = %s", fd_, fname_.c_str());
    }
}

void FirmwareDownload::set_filename(const char *url)
{
    const char *fname = strrchr(url, '/');
    if (!fname) {
        fname = url;
    } else {
        fname += 1;
        if (!*fname) {
            fname = "<unknown>";
        }
    }
    fname_.assign(fname);
}

/*
 * Append a filename TLV for NOR-only systems as this information is otherwise
 * not present (on a NAND system it is saved in a sideband TLV)
 */
mesa_rc FirmwareDownload::append_filename_tlv()
{
    if (!vtss_mtd_rootfs_is_nor()) {
        // We only need to do this for a NOR-only system
        return MESA_RC_OK;
    }

    T_I("Appending TLV for filename %s", filename());
    mscc_firmware_vimage_stage2_tlv_t *s2tlv = nullptr;
    uint32_t tlvlen = 0;
    s2tlv = mscc_vimage_stage2_filename_tlv_create(filename(), &tlvlen);
    if (s2tlv != nullptr && tlvlen > 0) {
        write(s2tlv, tlvlen);
        mscc_vimage_stage2_filename_tlv_delete(s2tlv);
    }

    return MESA_RC_OK;
}

mesa_rc FirmwareDownload::download(const char *url)
{
    mesa_rc rc = MESA_RC_ERROR;
    misc_url_parts_t url_parts;
    // Save filename
    set_filename(url);
    if (cli_) {
        cli_io_printf(cli_, "Downloading...\n");
    } else {
        T_I("Downloading...\n");
    }
    // Parse url and download
    misc_url_parts_init(&url_parts, MISC_URL_PROTOCOL_TFTP);
    if (misc_url_decompose(url, &url_parts) &&
        strcmp(url_parts.protocol, "tftp") == 0) {
        int err;
        rc = tftp_get(url_parts.path, url_parts.host, &err);
    } else {
        rc = http_get(url);
    }

    // Append a TLV for the filename if necessary
    append_filename_tlv();

    // Seal data if in a file
    seal();
    // CLI or trace log
    if (rc == MESA_RC_OK) {
        if (cli_) {
            cli_io_printf(cli_, "Got %zd bytes\n", length_);
        } else {
            T_I("Got %zd bytes\n", length_);
        }
    } else {
        if (cli_) {
            cli_io_printf(cli_, "Download failed: %s\n", last_error());
        } else {
            T_I("Download failed: %s", last_error());
        }
    }
    return rc;
}

mesa_rc FirmwareDownload::firmware_update_stage2(const mscc_firmware_vimage_t *fw, const char *stage2_file)
{
    mesa_rc rc = VTSS_RC_OK;
    size_t stage2_size;
    const unsigned char *stage2_ptr;

    if (length_ < fw->imglen) {
        T_D("Invalid image %zu %u", length_, fw->imglen);
        return FIRMWARE_ERROR_INVALID;
    }

    stage2_ptr = ((const unsigned char *)fw) + fw->imglen;
    stage2_size = length_ - fw->imglen;

    if (!stage2_size) {
        return FIRMWARE_ERROR_NO_STAGE2;
    }

    // Update bootloader, if includedd
    rc = firmware_update_stage2_bootloader(cli_, stage2_ptr, stage2_size);
    if (rc != VTSS_RC_OK) {
        return rc;
    }

    (void)mkdir(FIRWARE_STAGE2, 0755);
    unlink(stage2_file);    // Be sure file does not exits prior to link
    if (fw->version == 1) {
        int fd = open(stage2_file, O_WRONLY|O_CREAT|O_TRUNC|O_EXCL, S_IRUSR|S_IWUSR);
        if (fd >= 0) {
            // Disabling compression at filesystem layer
            int flags, res;
            ssize_t written;
            res = ioctl(fd, FS_IOC_GETFLAGS, &flags);
            if (res != 0) {
                T_I("Failed get FS_IOC_GETFLAGS %s", strerror(errno));
            } else {
                flags &= ~FS_COMPR_FL;
                res = ioctl(fd, FS_IOC_SETFLAGS, &flags);
                if (res != 0) {
                    T_I("Failed get disable FS compression for stage2 file %s", strerror(errno));
                }
            }
            T_I("Writing stage2 file %s", stage2_file);
            written = ::write(fd, stage2_ptr, stage2_size);
            if (written != stage2_size) {
                T_E("Write error %s: Wrote %zd, expected %zd", strerror(errno), written, stage2_size);
                rc = FIRMWARE_ERROR_FLASH_PROGRAM;
            } else {
                T_I("Wrote %zd bytes for stage2 in %s", written, stage2_file);
                rc = VTSS_RC_OK;
            }
            close(fd);
        } else {
            T_E("%s: open(%s)", strerror(errno), stage2_file);
            return FIRMWARE_ERROR_FLASH_PROGRAM;
        }
    } else {
        T_I("Linking stage2 download %s to image file %s", fsfile, stage2_file);
        if (link(fsfile, stage2_file) != 0) {
            T_E("%s: link(%s,%s)", strerror(errno), fsfile, stage2_file);
            rc = FIRMWARE_ERROR_FLASH_PROGRAM;
        }
    }

    return rc;
}

// Update firmware image.  Depending on the rootfs_data type, the
// image is either stored in entirety (NOR only) - or split with stage
// 1+2 in NAND (where already downloaded to) - and stage1 copied to NOR.
mesa_rc FirmwareDownload::update_stage1_and_stage2(const char *mtd_name)
{
    int rc = VTSS_RC_OK;
    if (vtss_mtd_rootfs_is_nor()) {
        T_D("NOR rootfs detected - updating %s directly", mtd_name);
        rc = firmware_flash_mtd_if_needed(cli_, mtd_name, map(), length_);
    } else {
        int fno;
        char stage2_file[sizeof(STAGE2_FILE)+1];
        const char *s2_basename;

        (void)mkdir(FIRWARE_STAGE2, 0755);
        strncpy(stage2_file, STAGE2_FILE, sizeof(stage2_file));
        fno = mkstemp(stage2_file);
        if (fno < 0) {
            T_E("%s: %s", stage2_file, strerror(errno));
            return VTSS_RC_ERROR;
        }
        close(fno);    // Only using the filename, not the fd

        // Assumed caller has called 'firmware_check'
        mscc_firmware_vimage_t *fw = (mscc_firmware_vimage_t *) map();

        // Write stage2 part to nand
        rc = firmware_update_stage2(fw, stage2_file);
        if (rc == FIRMWARE_ERROR_NO_STAGE2) {
            s2_basename = NULL;
        } else if (rc == VTSS_RC_OK) {
            s2_basename = stage2_file + strlen(STAGE2_DIR);
        } else {
            goto EXIT;
        }

        // Write stage1 to NOR flash
        rc = firmware_update_stage1(cli_, map(), fw->imglen, mtd_name, filename(), s2_basename);

  EXIT:
        // Regardless of success, clean-up the stage2 components (simply delete
        // everything that has no pointers), for the MTD partitions applicable
        firmware_stage2_cleanup();
    }

    return rc;
}

mesa_rc FirmwareDownload::load_nor(const char *mtdname, const char *type)
{
    cli_io_printf(cli_, "Writing %s image\n", type);
    mesa_rc rc = update_stage1_and_stage2(mtdname);
    if (rc == MESA_RC_OK) {
        cli_io_printf(cli_, "  Done\n");
    } else {
        cli_io_printf(cli_, "  Failed: %s\n", error_txt(rc));
    }
    return rc;
}

mesa_rc FirmwareDownload::load_raw(const char *mtdname) {
    return firmware_flash_mtd(cli_, mtdname, map(), length_);
}

mesa_rc FirmwareDownload::update(mesa_restart_t restart) {
    const char *fis_name = firmware_fis_to_update();
	const char *fis_name_keep = firmware_fis_to_keep();
	int upgrade_dual = lntn_upgrade_dual_flag_get();

    T_D("Fis to update: %s", filename());

    /* LED state: Firmware update */
    led_front_led_state(LED_FRONT_LED_FLASHING_BOARD, TRUE);
    lntn_sysled_system_fast_blink_red();
    cli_io_printf(cli_, "Starting flash update - do not power off device!\n");

    control_system_flash_lock();

    mesa_rc rc = update_stage1_and_stage2(fis_name);

	/*upgrade dual FW*/
	if (rc == VTSS_RC_OK && upgrade_dual==TRUE) {
		rc = update_stage1_and_stage2(fis_name_keep);
    }
	
    if (rc == VTSS_RC_OK) {
        if (strcmp(fis_name, "linux.bk") == 0) {
            firmware_setstate(cli_, FWS_SWAPPING, fis_name);
            if ((firmware_swap_images()) == VTSS_RC_OK) {
                firmware_setstate(cli_, FWS_SWAP_DONE, fis_name);
            } else {
                firmware_setstate(cli_, FWS_SWAP_FAILED, fis_name);
            }
        }
    }

    /* Back to prevcli_us system LED state */
    led_front_led_state_clear(LED_FRONT_LED_FLASHING_BOARD);
    lntn_sysled_system_solid_green();

    if (rc == VTSS_OK) {
        reset();    // Loose any file associated
        firmware_setstate(cli_, FWS_REBOOT, fis_name);
        control_system_flash_unlock();
        conf_wait_flush();
        control_system_reset_sync(restart);
        /* NORETURN */
    } else {
        T_D("firmware upload status: %s", error_txt(rc));
        firmware_setstate(NULL, FWS_DONE, NULL);
    }

    /* Unlock reboot/config lock */
    control_system_flash_unlock();

    return rc;
}

mesa_rc FirmwareDownload::bootstrap(mesa_restart_t restart) {
    mesa_rc rc;

    /* LED state: Firmware update */
    lntn_sysled_system_fast_blink_red();

    if ((rc = load_nor("linux", "primary")) == MESA_RC_OK) {
        if (firmware_has_alt()) {
            if ((rc = load_nor("linux.bk", "backup") != MESA_RC_OK)) {
                T_D("Warning: backup partition do bootstrap failed!");
            }
        } else {
            T_D("Warning: No backup partition detected.");
        }
    }

    /* Back to prevcli_us system LED state */
    lntn_sysled_system_solid_green();

    control_system_reset_sync(restart);

    return rc;
}

static mesa_rc update_mptest_section(vtss_mtd_t *mtd,
                                     const unsigned char *buffer,
                                     size_t length,
                                     const char* name) {
    mesa_rc rc;

    if (vtss_mtd_open(mtd, name) != MESA_RC_OK) {
        T_W("FAILED: vtss_mtd_open[%s]", mtd->dev);
        return MESA_RC_ERROR;
    }

    if ((rc = vtss_mtd_erase(mtd, length)) == MESA_RC_OK) {
        if ((rc = vtss_mtd_program(mtd, buffer, length)) != MESA_RC_OK) {
            T_W("FAILED: vtss_mtd_program[%s]", mtd->dev);
        }
    } else {
        T_W("FAILED: vtss_mtd_erase[%s]", mtd->dev);
    }

    vtss_mtd_close(mtd);

    return rc;
}

// For mptest use, boot from NOR FLASH after execution
mesa_rc FirmwareDownload::update_mptest() {
    mesa_rc     rc;
    vtss_mtd_t  mtd;

    T_D("Fis to update: %s", filename());

    /* LED state: Firmware update */
    lntn_sysled_system_fast_blink_red();
    cli_io_printf(cli_, "Starting flash update - do not power off device!\n");

    control_system_flash_lock();

    // Upgrade primary
    cli_io_printf(cli_, "Upgrade primary...\n");
    if ((rc = update_mptest_section(&mtd, map(), length_, "linux")) != MESA_RC_OK) {
        cli_io_printf(cli_, "Upgrade primary fail!\n");
        goto UPDATE_MPTEST_EXIT;
    } else {
        cli_io_printf(cli_, "Upgrade primary done.\n");
    }

    if (firmware_has_alt()) {
        // Upgrade backup
        cli_io_printf(cli_, "Upgrade backup...\n");
        if ((rc = update_mptest_section(&mtd, map(), length_, "linux.bk")) != MESA_RC_OK) {
            cli_io_printf(cli_, "Upgrade backup fail!\n");
        } else {
            cli_io_printf(cli_, "Upgrade backup done.\n");
        }
    }

UPDATE_MPTEST_EXIT:

    /* Back to prevcli_us system LED state */
    lntn_sysled_system_solid_green();

    control_system_flash_unlock();

    return rc;
}

FwManager manager;

FwManager::FwManager() : m_("fw", VTSS_MODULE_ID_FIRMWARE) {}

FwWrap FwManager::get() {
    vtss::lock_guard<vtss::Critd> fwlock(__FILE__, __LINE__, m_);
    if (status == FREE) {
        status = IN_USE;
        return FwWrap(&obj);
    } else {
        return FwWrap(nullptr);
    }
}

FwWrap FwManager::get_async() {
    vtss::unique_lock<vtss::Critd> fwlock(__FILE__, __LINE__, m_);
    while (status != IN_USE_ASYNC) {
        cv_.wait(fwlock);
    }
    return FwWrap(&obj);
}

void FwManager::store_async(FwWrap &&rhs) {
    vtss::lock_guard<vtss::Critd> fwlock(__FILE__, __LINE__, m_);
    assert(status == IN_USE);
    assert(rhs.data == &obj);
    status = IN_USE_ASYNC;
    cv_.notify_one();
    // DO NOT RESET
    rhs.data = nullptr;
}
