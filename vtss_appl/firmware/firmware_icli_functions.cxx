/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

#include "icli_api.h"
#include "icli_porting_util.h"
#include "firmware_api.h"
#include "firmware_icli_functions.h"
#include "main.h"
#include "firmware.h"
#include "simage_api.h"
#include "firmware_vimage.h"
#include "firmware_api.h"
#include "firmware_ubi.hxx"
#include "firmware_mfi_info.hxx"
#include "vtss_os_wrapper_linux.hxx"
#include <unistd.h>
#include <dirent.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include "firmware_mfi_info.hxx"
#include "usb_api.h"  // For usb_file_read


/* Help function for printing the image information.
 * In : session_id - For printing error messages
 */
static void display_props(i32 session_id, const char *type, u32 imageNo)
{
    vtss_appl_firmware_status_image_t image_entry;
    char filename[FIRMWARE_IMAGE_NAME_MAX];
    const char *imagetype = NULL;
    mesa_rc rc;

    if ((rc = vtss_appl_firmware_status_image_entry_get(imageNo, &image_entry)) == VTSS_RC_OK) {
        switch (imageNo) {
        case VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_BOOTLOADER:
            imagetype = "bootloader";
            break;
        case VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ACTIVE_FIRMWARE:
            imagetype = "primary";
            break;
        case VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ALTERNATIVE_FIRMWARE:
            imagetype = "backup";
            break;
        default:
            imagetype = "unknown";
        }
    } else if (rc != FIRMWARE_ERROR_IMAGE_NOT_FOUND) {
        imagetype = error_txt(rc);
    }

    if (imagetype) {
        icli_parm_header(session_id, type);
        ICLI_PRINTF("%-17s: %s (%s)\n", "Image", image_entry.name, imagetype);
        ICLI_PRINTF("%-17s: %s\n", "Version", image_entry.version);
        ICLI_PRINTF("%-17s: %s\n", "Date", image_entry.built_date);
        if (imageNo != VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_BOOTLOADER) {
            if (firmware_image_name_get(imageNo == VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ACTIVE_FIRMWARE ? "linux" : "linux.bk",
                                        filename, sizeof(filename)) != VTSS_OK) {
                strncpy(filename, "(unknown)", sizeof(filename));
            }
            ICLI_PRINTF("%-17s: %s\n", "Upload filename", filename);
        }
    }
}

//  see firmware_icli_functions.h
void firmware_icli_show_version(i32 session_id)
{
    ICLI_PRINTF("\n");
    display_props(session_id, "Bootloader", VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_BOOTLOADER);
    ICLI_PRINTF("\n");
    display_props(session_id, "Active Image", VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ACTIVE_FIRMWARE);
    ICLI_PRINTF("\n");
    display_props(session_id, "Backup Image", VTSS_APPL_FIRMWARE_STATUS_IMAGE_TYPE_ALTERNATIVE_FIRMWARE);
}

#ifdef VTSS_SW_OPTION_JSON_RPC_NOTIFICATION
static mesa_rc firmware_icli_print_licenses(i32 session_id,                                                  // For printing
                                            const char *image_name,                                          // The mtd iamge name
                                            vtss_appl_firmware_status_basic_license_t *basic_license,  // Basic license information, set to NULL if no license.
                                            BOOL show_description,                                           // TRUE to show license desciption
                                            BOOL &print_hdr,                                                 // TRUE to print header, returns FALSE to signal that header has been printed
                                            u32 section_id,                                                  // Section ID for which license to print
                                            u32 image_id)                                                    // Image ID for which image to print
{
    if (print_hdr) {
        char str_buf[200];
        sprintf(str_buf, "%-12s %-12s %-12s %-25s %-25s %-25s %-40s",
                "Image Name", "SectionID", "ComponentID", "Component Name", "Version", "Type", "Url");
        icli_table_header(session_id, str_buf);
    }
    print_hdr = FALSE;

    //
    // Printing overview
    //
    ICLI_PRINTF("%-12s ", image_name);

    if (!basic_license) {
        ICLI_PRINTF("No licenses found\n");
        return VTSS_RC_OK;
    } else {
        ICLI_PRINTF("%-12d %-12d %-25s %-25s %-25s %-40s\n",
                    section_id, basic_license->component_id,
                    basic_license->component_name.c_str(), basic_license->version.c_str(),
                    basic_license->license_type.c_str(), basic_license->url.c_str());
    }

    //
    // Print License text
    //
    if (show_description) {
        components_licenses_text_vec_t components_licenses_text_vec;
        firmware_license_text_vec_t license_text_vec;
        firmware_section_licenses_text_vec_get(image_id, section_id, basic_license->component_id, &components_licenses_text_vec);
        for (auto it = components_licenses_text_vec.begin(); it != components_licenses_text_vec.end(); ++it) {
            for (auto y = (*it).begin(); y != (*it).end(); ++y) {
                ICLI_PRINTF("License files name:%s\n", y->file_name.c_str());
                ICLI_PRINTF("License text:");

                std::string decoded_txt = y->text;
                if (std_string_base64_decode(decoded_txt) != VTSS_RC_OK) {
                    VTSS_TRACE(VTSS_TRACE_GRP_INFO, INFO) << "Could not decode license_text";
                    return VTSS_RC_ERROR;
                }

                for (auto i = 0; i < decoded_txt.length(); i += ICLI_STR_MAX_LEN) {
                    ICLI_PRINTF("%s", decoded_txt.substr(i, ICLI_STR_MAX_LEN).c_str());
                }
            }
        }
    }
    return VTSS_RC_OK;
}

void firmware_license(u32 session_id, const u32 image_id,  const char *mtdname, BOOL &print_hdr, BOOL show_description,
                      BOOL has_secton, u32 section_id, BOOL has_component, u32 &component_id_get)
{
    basic_license_map_t license_map;
    firmware_basic_license_map_get(&license_map);

    BOOL found = FALSE;
    for (basic_license_map_t::iterator it = license_map.begin(); it != license_map.end(); ++it) {

        if (it->first.image_id != image_id) {
            continue;
        }
        firmware_info_license_section_key_t section_key;
        section_key.image_id = it->first.image_id;
        section_key.section_id = it->first.section_id;

        if (has_secton && section_id !=   section_key.section_id) {
            continue;
        }

        basic_licenses_vec_t section_licenses_vec = it->second;
        for (basic_licenses_vec_t::iterator vec_it = section_licenses_vec.begin(); vec_it != section_licenses_vec.end(); ++vec_it) {
            vtss_appl_firmware_status_basic_license_t basic_license;
            basic_license  = *vec_it;

            if (!has_component || basic_license.component_id == component_id_get) {
                firmware_icli_print_licenses(session_id, mtdname,  &basic_license, show_description, print_hdr, it->first.section_id, image_id);
            }
        }

        found = TRUE;
    }

    if (!found) {
        // Print no license found.
        firmware_icli_print_licenses(session_id, mtdname, NULL, show_description, print_hdr, 0, 0);
    }
}
//  see firmware_icli_functions.h
void firmware_icli_show_licenses(i32 session_id, const char *mtdname, BOOL show_description, BOOL has_section, u32 section_id, BOOL has_component, u32 component_id)
{
    BOOL print_hdr = TRUE;
    tlv_map_t tlv_map; // Map going to contain the information.
    firmware_image_tlv_map_get(&tlv_map);
    firmware_info_key_t firmware_info_key;

    firmware_info_key.image_id = 0;
    // Image name is places at section_id= 0, attribute_id=1
    firmware_info_key.section_id = 0;
    firmware_info_key.attribute_id = 1;

    u8 timeout = 255; // Just in case of bug, we don't want to run forever in the while loop

    BOOL found = FALSE;
    // Loop though show_description the images
    while (tlv_map.count(firmware_info_key) && timeout) {
        // If user has requested a specific image then find that.
        if (mtdname != NULL) {
            if (strcmp(tlv_map[firmware_info_key].value.str, mtdname) == 0) {
                firmware_license(session_id, firmware_info_key.image_id,
                                 tlv_map[firmware_info_key].value.str,
                                 print_hdr, show_description,
                                 has_section,
                                 section_id,
                                 has_component,
                                 component_id);
                found = TRUE;
                break;
            } else {
                firmware_info_key.image_id++;
                continue;
            }
        }
        firmware_license(session_id, firmware_info_key.image_id, tlv_map[firmware_info_key].value.str,
                         print_hdr, show_description, has_section, section_id, has_component, component_id);
        firmware_info_key.image_id++;
        timeout--;
    }

    if (mtdname != NULL && !found) {
        ICLI_PRINTF("mdt named:%s doesn't exists\n", mtdname);
    }
}


#endif // VTSS_SW_OPTION_JSON_RPC_NOTIFICATION
// see firmware_icli_functions.h
void firmware_icli_swap_image(i32 session_id)
{



    mesa_restart_t restart_type = MESA_RESTART_COOL;


    control_system_flash_lock();    // Locked until reboot of fail

    if (firmware_swap_images() == VTSS_RC_OK) {
        ICLI_PRINTF("Alternate image activated, now rebooting.\n");
        /* Now restart */
        if (vtss_switch_standalone()) {
            (void) control_system_reset(TRUE, VTSS_USID_ALL, restart_type);
        } else {
            (void) control_system_reset(FALSE, VTSS_USID_ALL, restart_type);
        }
    } else {
        ICLI_PRINTF("Alternate image activation failed.\n");
        control_system_flash_unlock();    // Only unlock if we failed
    }
}

//  see firmware_icli_functions.h
void firmware_icli_upgrade(i32 session_id, const char *url)
{
    auto fw = manager.get();
    if (fw.ok()) {
        fw->init(nullptr, firmware_max_download);
        fw->attach_cli(cli_get_io_handle());
        if (fw->download(url) == MESA_RC_OK) {
            if (!fw->check()) {
                ICLI_PRINTF("Invalid image, need a MFI image\n");
                return;
            }
            mesa_rc result;
            // This only returns if an error occurred
            if ((result = fw->update(MESA_RESTART_COOL)) != VTSS_OK) {
                ICLI_PRINTF("Error: %s\n", error_txt(result));
            }
        }
    } else {
        ICLI_PRINTF("Error: Firmware update already pending\n");
    }
}

void firmware_icli_upgrade_from_usb(i32 session_id, unsigned char *filename, unsigned int filename_size)
{
    unsigned char   *filebuf = NULL;
    unsigned int    filesize;
    usb_rc_t        rc;

    auto fw = manager.get();

    ICLI_PRINTF("Transferring image...\n");
    lntn_sysled_system_fast_blink_red();

    if ((rc = usb_file_read(filename, filename_size, &filebuf, &filesize)) != USB_RC_OK) {
        ICLI_PRINTF("%s\n", usb_error_txt(rc));
        lntn_sysled_system_solid_green();
        goto UPGRADE_EXIT;
    }

    if (fw.ok()) {
        fw->init(nullptr, firmware_max_download);
        fw->attach_cli(cli_get_io_handle());
        fw->write(filebuf, filesize);
        fw->set_filename((const char *)filename);

        if (!fw->check()) {
            ICLI_PRINTF("Invalid image, need a MFI image\n");
            lntn_sysled_system_solid_green();
            return;
        }
        mesa_rc result;
        // This only returns if an error occurred
        if ((result = fw->update(MESA_RESTART_COOL)) != VTSS_OK) {
            ICLI_PRINTF("Error: %s\n", error_txt(result));
        }
    } else {
        ICLI_PRINTF("Error: Firmware update already pending\n");
        lntn_sysled_system_solid_green();
    }

UPGRADE_EXIT:
    free((void*)filebuf);
}

static bool is_bootstrapped(i32 session_id, const char *fis)
{
    vtss_mtd_t mtd;
    bool result = false;

    if (vtss_mtd_open(&mtd, fis) == VTSS_OK) {
        off_t base = mscc_firmware_sideband_get_offset(&mtd);
        if (base) {
            mscc_firmware_sideband_t *sb = mscc_vimage_sideband_read(&mtd, base);
            if (sb) {
                char pathname[PATH_MAX];
                mscc_firmware_vimage_tlv_t tlv;
                const char *name;
                if ((name = (const char *) mscc_vimage_sideband_find_tlv(sb, &tlv, MSCC_FIRMWARE_SIDEBAND_STAGE2_FILENAME))) {
                    // TLV data *is* NULL terminated
                    (void) snprintf(pathname, sizeof(pathname), "/switch/stage2/%s", name);
                    if (access(name, F_OK)) {
                        T_D("stage2 file %s FOUND", name);
                        result = true;
                    } else {
                        T_D("stage2 file %s does not exist", name);
                    }
                } else {
                    T_D("No stage2 filename tlv found in %s sideband", fis);
                }
                VTSS_FREE(sb);
            } else {
                T_D("Sideband read error for in %s, offset %08llx", fis, base);
            }
        } else {
            T_D("No sideband for in %s", fis);
        }
        vtss_mtd_close(&mtd);
    }

    return result;
}

#define SZ_M(n) (1024*1024*n)

// pre-declaration
mesa_rc firmware_icli_bootstrap_ubi(i32 session_id, unsigned int mbytes_limit);
// see firmware_icli_functions.h
void firmware_icli_bootstrap(i32 session_id, const char *url, unsigned int nandsize, bool force)
{
    const char *mtd_name = firmware_fis_to_update();
    size_t mtdsize;
    if (is_bootstrapped(session_id, firmware_fis_act()) && !force) {
        ICLI_PRINTF("System is already bootstrapped, please use normal firmware upgrade.\n");
    } else if ((mtdsize = firmware_section_size(mtd_name)) == 0) {
        ICLI_PRINTF("Error: MTD name '%s' not found\n", mtd_name);
    } else { // Bootstrap both nand and nor with the corresponding image part
        // Bootstrap nand flash into ubi
        if (firmware_icli_bootstrap_ubi(session_id, nandsize) != MESA_RC_OK) {
            T_I("Flash format failed\n");
        } else {
            auto fw = manager.get();
            if (fw.ok()) {
                fw->init(nullptr, firmware_max_download);
                fw->attach_cli(cli_get_io_handle());
                if (fw->download(url) != MESA_RC_OK) {
                    return;
                }

                if (!fw->check()) {
                    ICLI_PRINTF("Invalid image, need a MFI image\n");
                    return;
                }

                if (fw->load_nor("linux", "primary") == MESA_RC_OK) {

                    // Determine if we need to do a main image split as well
                    if (mtdsize >= SZ_M(6) &&
                        firmware_section_size("linux.bk") == 0 &&
                        firmware_section_size("split_linux_lo") >= SZ_M(3) &&
                        firmware_section_size("split_linux_hi") >= SZ_M(3)) {
                        if (fw->load_nor("split_linux_hi", "backup") != MESA_RC_OK) {
                            return;
                        }
                        if (firmware_fis_split("linux", firmware_section_size("split_linux_lo"),
                                               "linux.bk", firmware_section_size("split_linux_hi")) != MESA_RC_OK) {
                            ICLI_PRINTF("Error: Unable to split primary image.\n");
                            return;
                        }
                    } else {
                        if (firmware_has_alt()) {
                            if (fw->load_nor("linux.bk", "backup") != MESA_RC_OK) {
                                return;
                            }
                        } else {
                            ICLI_PRINTF("Warning: No backup partition detected.\n");
                        }
                    }
                    // reboot only if everything is ok
                    ICLI_PRINTF("Rebooting ...\n");
                    sync();
                    sync();

                    /*
                     * AA-11113: On certain devices (Luton10 & Serval-T) the reboot below
                     * would emit SPI error messages from the Flash device. Apparently it
                     * helps to delay a few secons here. Why this is so is unknown but
                     * the Linux man pages for the sync() call says this:
                     *
                     * "On Linux, sync is only guaranteed to schedule the dirty blocks for writing;
                     * it can actually take a short time before all the blocks are finally written.
                     * The reboot(8) and halt(8) commands take this into account by sleeping for a
                     * few seconds after calling sync(2)."
                     */
                    sleep(2);

                    reboot(LINUX_REBOOT_CMD_RESTART);
                }
            } else {
                ICLI_PRINTF("Firware update already in progress.\n");
            }
        }
    }
}

mesa_rc firmware_icli_load_image(cli_iolayer_t *io,
                                 const char *mtd_name,
                                 const char *url,
                                 mesa_rc (*checker)(cli_iolayer_t *, const u8 *, size_t))
{
    mesa_rc rc = MESA_RC_ERROR;
    size_t mtdsize;
    if ((mtdsize = firmware_section_size(mtd_name)) == 0) {
        cli_io_printf(io, "Error: MTD name '%s' not found\n", mtd_name);
        rc = FIRMWARE_ERROR_UPDATE_NOT_FOUND;
    } else {
        auto fw = manager.get();
        if (fw.ok()) {
            fw->init(nullptr, mtdsize);
            fw->attach_cli(io);
            if ((rc = fw->download(url)) == MESA_RC_OK) {
                if (checker == NULL || ((rc = checker(io, fw->map(), fw->length())) == VTSS_OK )) {
                    rc = fw->load_raw(mtd_name);
                }
            }
            fw->reset();
        } else {
            cli_io_printf(io, "Error: Firmware update already pending\n");
            rc = VTSS_APPL_FIRMWARE_UPLOAD_STATUS_ERROR_BUSY;
        }
    }
    return rc;
}

mesa_rc check_bootloader(cli_iolayer_t *io, const u8 *buffer, size_t length)
{
    mesa_rc rc;
    u32 imgtype, valid_type = SIMAGE_IMGTYPE_BOOT_LOADER;
    rc = firmware_check_bootloader_simg(buffer, length, &imgtype);
    if (rc == VTSS_OK) {
        if (imgtype != valid_type) {
            cli_io_printf(io, "Image type mismatch (type %d is not bootloader type %d)\n", imgtype, valid_type);
            rc = FIRMWARE_ERROR_WRONG_ARCH;
        }
    } else {
        cli_io_printf(io, "Bad Image: %s\n", error_txt(rc));
    }
    return rc;
}

mesa_rc firmware_icli_bootstrap_ubi(i32 session_id, unsigned int mbytes_limit)
{
    mesa_rc rc = VTSS_RC_OK;
    Firmware_ubi ubi;
    u8 cnt = 0;
    const u8 max_retry = 2;

    if (mbytes_limit > 0) {
        ICLI_PRINTF("Bootstrapping UBIFS with max. %d Mbyte ...\n", mbytes_limit);
    } else {
        ICLI_PRINTF("Bootstrapping UBIFS with all available volume space ...\n");
    }

    // retry once in case of failure.
    do {
        if ((rc = ubi.ubiumount()) != VTSS_OK) {
            ICLI_PRINTF("Umount ubifs failed.\n");
            cnt++;
            continue;
        }

        if ((rc = ubi.ubidetach()) != VTSS_OK) {
            ICLI_PRINTF("Detach ubi device failed.\n");
            cnt++;
            continue;
        }

        if ((rc = ubi.ubiformat()) != VTSS_OK) {
            ICLI_PRINTF("Format ubi device failed.\n");
            cnt++;
            continue;
        }

        if ((rc = ubi.ubiattach()) != VTSS_OK) {
            ICLI_PRINTF("Attach ubi device failed.\n");
            cnt++;
            continue;
        }

        if ((rc = ubi.ubimkvol(mbytes_limit)) != VTSS_OK) {
            ICLI_PRINTF("Create ubi volume failed.\n");
            cnt++;
            continue;
        }

        if ((rc = ubi.ubimount()) != VTSS_OK) {
            ICLI_PRINTF("Mount ubi device failed.\n");
            cnt++;
            continue;
        }
    } while ((cnt < max_retry) && (rc != VTSS_OK));

    ICLI_PRINTF("Bootstrap ubi done %s.\n", (rc == VTSS_OK) ? "ok" : "failed");
    return rc;
}

void firmware_icli_bootloader(i32 session_id, const char *url)
{
    firmware_icli_load_image(cli_get_io_handle(), "RedBoot", url, check_bootloader);
}

void firmware_icli_load_fis(i32 session_id,
                            const char *mtd_name,
                            const char *url)
{
    firmware_icli_load_image(cli_get_io_handle(), mtd_name, url, NULL);
}

static BOOL find_fw_dev(const char *driver, char *iodev, size_t bufsz)
{
    const char *top = "/sys/class/uio";
    DIR *dir;
    struct dirent *dent;
    char fn[PATH_MAX], devname[PATH_MAX];
    FILE *fp;
    BOOL found = FALSE;

    if (!(dir = opendir (top))) {
        perror(top);
        exit (1);
    }

    while ((dent = readdir(dir)) != NULL) {
        if (dent->d_name[0] != '.') {
            snprintf(fn, sizeof(fn), "%s/%s/name", top, dent->d_name);
            if ((fp = fopen(fn, "r"))) {
                const char *rrd = fgets(devname, sizeof(devname), fp);
                fclose(fp);
                if (rrd && (strstr(devname, driver))) {
                    strncpy(iodev, dent->d_name, bufsz);
                    found = TRUE;
                    break;
                }
            }
        }
    }

    closedir(dir);

    return found;
}

static int get_uio_map_size(const char *device, int map_no)
{
    const char *top = "/sys/class/uio";
    char fn[PATH_MAX];
    FILE *fp;
    int val = -1;

    snprintf(fn, sizeof(fn), "%s/%s/maps/map%d/size", top, device, map_no);
    if ((fp = fopen(fn, "r"))) {
        if (fscanf(fp, "%i", &val) != 1) {
            T_I("Unable to read size of %s mapping %d", device, map_no);
        }
        fclose(fp);
    }

    return val;
}


void firmware_icli_load_ram(i32 session_id, const char *url)
{
    int dev_fd;
    char uiodev[16], devname[64];
    const char *driver = "vcfw_uio";
    int mmap_size = -1;

    if (!find_fw_dev(driver, uiodev, sizeof(uiodev))) {
        ICLI_PRINTF("Firmware buffer device not available.\n");
        ICLI_PRINTF("Execute 'fconfig -i' or 'fconfig linux_memmap <number>' in Redboot.\n");
        ICLI_PRINTF("Reserve 16 Mb or whatever is suitable for the firmware buffer.\n");
        return;
    }

    /* Open the UIO device file */
    T_D("Using UIO, found '%s' driver at %s", driver, uiodev);
    snprintf(devname, sizeof(devname), "/dev/%s", uiodev);
    dev_fd = open(devname, O_RDWR);
    if (dev_fd < 0) {
        ICLI_PRINTF("Error opening %s: %s\n", devname, strerror(errno));
        return;
    }

    mmap_size = get_uio_map_size(uiodev, 0);
    if (mmap_size <= 0) {
        ICLI_PRINTF("%s: Unable to determine mapping size (%s)\n", uiodev, strerror(errno));
    } else {
        /* mmap the UIO device */
        u8 *fw_mem = (u8 *) mmap(NULL, mmap_size, PROT_READ | PROT_WRITE, MAP_SHARED, dev_fd, 0);
        if (fw_mem != MAP_FAILED) {
            T_D("Mapped register memory @ %p, size %d", fw_mem, mmap_size);
            auto fw = manager.get();
            if (fw.ok()) {
                fw->init(fw_mem, firmware_max_download);
                fw->attach_cli(cli_get_io_handle());
                if (fw->download(url) == MESA_RC_OK) {
                    if (fw->check()) {
                        CPRINTF("Image is a valid firmware image, do the following in redboot to activate:\n");
                        CPRINTF("ramload\n");
                        (void) control_system_reset(TRUE, VTSS_USID_ALL, MESA_RESTART_COOL);
                    } else {
                        CPRINTF("Image was not valid firmware image, but is kept resident in memory until system reboot.\n");
                    }
                }
            } else {
                ICLI_PRINTF("Error: Firmware update already pending\n");
            }
            (void) munmap(fw_mem, mmap_size);
        } else {
            ICLI_PRINTF("Error: mmap(%s,%d): %s\n", uiodev, mmap_size, strerror(errno));
        }
    }
    close(dev_fd);
}

// Print MFI image information
void firmware_icli_show_info(i32 session_id)
{
    tlv_map_t tlv_map; // Map going to contain the information.
    firmware_image_tlv_map_get(&tlv_map);

    char str_buf[100];
    snprintf(str_buf, sizeof(str_buf), "%-12s %-12s %-12s %-40s %-20s", "ImageId", "SectionId", "Attr.Id", "Name", "Value");
    icli_table_header(session_id, str_buf);

    for (tlv_map_t::iterator it = tlv_map.begin(); it != tlv_map.end(); ++it) {
        ICLI_PRINTF("%-12d %-12d %-12d %-40s %-20s\n",
                    it->first.image_id, it->first.section_id,
                    it->first.attribute_id, it->second.attr_name, it->second.value.str);
    }
    return;
}
/****************************************************************************/
/*  End of file.                                                            */
/****************************************************************************/
