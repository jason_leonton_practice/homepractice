/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "critd.h"
#include "critd_api.h"

#ifdef VTSS_SW_OPTION_SYSUTIL
#include "sysutil_api.h"
#endif

#ifdef VTSS_SW_OPTION_DAYLIGHT_SAVING
#include "daylight_saving_api.h"
#endif

#ifdef VTSS_SW_OPTION_ICLI
#include "icli_api.h"
#endif

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_CRITD

// ===========================================================================
// Trace
// ---------------------------------------------------------------------------
#if (VTSS_TRACE_ENABLED)
/* Trace registration */
static vtss_trace_reg_t trace_reg =
{
    VTSS_TRACE_MODULE_ID, "critd", "Critd module"
};

#ifndef CRITD_DEFAULT_TRACE_LVL
#define CRITD_DEFAULT_TRACE_LVL VTSS_TRACE_LVL_ERROR
#endif

static vtss_trace_grp_t trace_grps[TRACE_GRP_CNT] =
{
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default",
        CRITD_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
};
#endif /* VTSS_TRACE_ENABLED */

// ###########################################################################
// Definitions and constants
// ---------------------------------------------------------------------------

#undef MAX
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#undef MIN
#define MIN(a,b) ((a) > (b) ? (b) : (a))

#if VTSS_TRACE_ENABLED
#define CRITD_ASSERT(expr, fmt, ...) { \
    if (!(expr)) { \
        T_EXPLICIT(crit_p->trace_module_id, trace_grp, VTSS_TRACE_LVL_ERROR, \
                   file, line, "ASSERTION FAILED"); \
        T_EXPLICIT(crit_p->trace_module_id, trace_grp, VTSS_TRACE_LVL_ERROR, \
                   file, line, fmt, ##__VA_ARGS__); \
        VTSS_ASSERT(expr); \
    } \
}
#else
#define CRITD_ASSERT(expr, fmt, ...) { \
    VTSS_ASSERT(expr); \
}
#endif

#define CRITD_THREAD_ID_NONE (0)

#define CRITD_T_COOKIE 0x0BADBABE

// ###########################################################################


// ###########################################################################
// Global variables
// ---------------------------------------------------------------------------

// Thread variables
static vtss_handle_t critd_thread_handle;
static vtss_thread_t critd_thread_block;

// Deadlock surveillance
// Global variable is easier to inspect from gdb
static BOOL critd_deadlock = 0;

struct CritdInternalLock {
    CritdInternalLock() {
        // Do not use mutex_init as it will use the trace model, and will not
        // work when called before the trace module is initialized. We want to
        // allow critd to be created in constructors that run before the main
        // function.

        int res;
        pthread_mutexattr_t attr;

        res = pthread_mutexattr_init(&attr);
        VTSS_ASSERT(res == 0);

        // Enable PTHREAD_PRIO_INHERIT
        res = pthread_mutexattr_setprotocol(&attr, PTHREAD_PRIO_INHERIT);
        VTSS_ASSERT(res == 0);

        res = pthread_mutex_init(&m, &attr);
        VTSS_ASSERT(res == 0);
    }

    void lock() {
        int res = pthread_mutex_lock(&m);
        VTSS_ASSERT(res == 0);
    }

    void unlock() {
        int res = pthread_mutex_unlock(&m);
        VTSS_ASSERT(res == 0);
    }

    pthread_mutex_t m;
};

struct CritdTblLock;

// We need one non-critd-based mutex for updating the table of critds a given
// module holds.
static struct CritdInternalTblLock : public CritdInternalLock {
    friend struct CritdTblLock;

  private:
    // Linked list of critds for each module
    critd_t *critd_tbl[VTSS_MODULE_ID_NONE];
} critd_internal_tbl_lock;

struct CritdTblLock {
    CritdTblLock(const CritdTblLock &rhs) = delete;

    CritdTblLock() {
        critd_internal_tbl_lock.lock();
    }

    critd_t *critd_tbl(uint32_t module_id) {
        return critd_internal_tbl_lock.critd_tbl[module_id];
    }

    void critd_tbl(uint32_t module_id, critd_t *p) {
        critd_internal_tbl_lock.critd_tbl[module_id] = p;
    }

    ~CritdTblLock() {
        critd_internal_tbl_lock.unlock();
    }
};

// And we need another mutex when updating attempts to lock a critd.
// We cannot use the same mutex as when updating the critd table, because that
// could result in deadlocks under rare circumstances.
static struct CritdInternalUpdateLock : public CritdInternalLock {
} critd_internal_update_lock;

struct CritdUpdateLock {
    CritdUpdateLock() {
        critd_internal_update_lock.lock();
    }

    ~CritdUpdateLock() {
        critd_internal_update_lock.unlock();
    }
};

// ###########################################################################


// ###########################################################################
// Internal functions
// ---------------------------------------------------------------------------
static void critd_dbg_list_(
    CritdTblLock             &locked,
    const critd_dbg_printf_t dbg_printf,
    const vtss_module_id_t   module_id,
    const BOOL               detailed,
    const BOOL               header,
          critd_t            *single_critd_p);

// Current number of tokens
// The function returns 0 if the mutex or semaphore is taken, > 0 otherwise
static int critd_peek(critd_t* const crit_p)
{
    int res = 0;

    // If we've not yet had the initial critd_exit(),
    // then "pretend" that we're locked.
    if (vtss_flag_peek(&crit_p->flag) == 0) {
        return 0;
    }

    switch (crit_p->type) {
        case CRITD_TYPE_MUTEX:
            // The initial critd_exit() has been called, so
            // now we can check to see if we can lock it (in the
            // lack of a peek function).
            if (vtss_mutex_trylock(&crit_p->m.mutex)) {
                // The mutex was not taken, but now it is. Undo that.
                vtss_mutex_unlock(&crit_p->m.mutex);
                return 1;
            }

            // The mutex is already taken.
            return 0;

        case CRITD_TYPE_SEMAPHORE:
            // We should only be looking at 'lock_cnt' when the mutex is expected
            // to be locked. So we must try to lock it, and if we succeed, we
            // must look at 'lock_cnt' to see if it was locked before we took
            // the lock.
            if (vtss_recursive_mutex_trylock(&crit_p->m.rmutex)) {
                if (crit_p->m.rmutex.lock_cnt <= 1) {
                    res = 1;
                }

                // We just took the mutex, and new we need to release it again,
                // otherwise it will be unbalanced.
                vtss_recursive_mutex_unlock(&crit_p->m.rmutex);
            }
            return res;

        case CRITD_TYPE_MUTEX_RECURSIVE:
            return (int)vtss_sem_peek(&crit_p->m.semaphore);

        default:
            VTSS_ASSERT(0);
            return 0;
    }
}

// Check whether critical region is locked
static BOOL critd_is_locked(
    critd_t* const crit_p)
{
    return (critd_peek(crit_p) == 0);
}

static void critd_trace(
    critd_t*    const crit_p,
    const int   trace_grp,
    const int   trace_lvl,
    const char* const file,
    const int   line)
{
} // critd_trace


static inline ulong critd_get_thread_id(void) {
    return vtss_thread_id_get();
} // critd_get_thread_id

// ===========================================================================
// Module thread - for semaphore surveillance
// ---------------------------------------------------------------------------

// Function used to have critd_dbg_list sprint output into deadlock_str buffer
static char deadlock_str[10000];
int deadlock_print(const char *fmt, ...)
{
    va_list args;
    int     rv;

    va_start(args, fmt);
    rv = vsprintf(deadlock_str + strlen(deadlock_str), fmt, args);
    va_end(args);

    return rv;
} // deadlock_print

static void critd_thread(vtss_addrword_t data)
{
    const uint       POLL_PERIOD = 5;
    vtss_module_id_t mid;
    BOOL             deadlock_found = 0;
    T_D("Enter");
    VTSS_OS_MSLEEP(10000); // Let everything start before starting surveillance

    // Semaphore surveillance
    while (1) {
        VTSS_OS_MSLEEP(POLL_PERIOD*1000);

        for (mid = 0; mid < VTSS_MODULE_ID_NONE; mid++) {
            CritdTblLock locked;
            critd_t* critd_p = locked.critd_tbl(mid);
            while (critd_p) {
                // Check cookie
                if (critd_p->cookie != CRITD_T_COOKIE &&
                    critd_p->cookie_illegal != 1) {
                    T_E("%s: Cookie=0x%08x, expected 0x%08x",
                        vtss_module_names[mid], critd_p->cookie, CRITD_T_COOKIE);
                    critd_p->cookie_illegal = 1;
                }

                if (!critd_deadlock) {
                    // Check for deadlock
                    if (critd_p->current_lock_thread_id && critd_p->max_lock_time != -1) {
                        if (critd_p->lock_cnt == critd_p->last_lock_cnt) {
                            // Same lock as last poll!
                            critd_p->lock_poll_cnt++;
                        } else {
                            // Store lock count, so we can see if same lock persists
                            critd_p->last_lock_cnt = critd_p->lock_cnt;
                            critd_p->lock_poll_cnt      = 0;
                        }
                    } else {
                        critd_p->lock_poll_cnt  = 0;
                    }

                    if (critd_p->lock_poll_cnt > (critd_p->max_lock_time / POLL_PERIOD)) {
                        // Semaphore has been locked for too long
                        critd_deadlock = 1;
                        deadlock_found = 1;

                        memset(deadlock_str, 0, sizeof(deadlock_str));
                        critd_dbg_list_(locked, &deadlock_print, mid, 1, 0, critd_p);
                        printf("Error: Semaphore deadlock:\n%s\n", deadlock_str);
                    }
                }

                critd_p = critd_p->nxt;
            }
        }

        if (deadlock_found) {
            // Write the state of all locked semaphores to syslog and
            // suspend any further surveillance
            printf("All locked semaphores listed below.\n");
            for (mid = 0; mid < VTSS_MODULE_ID_NONE; mid++) {
                CritdTblLock locked;
                critd_t* critd_p = locked.critd_tbl(mid);
                while (critd_p) {
                    if (critd_p->current_lock_thread_id || vtss_flag_peek(&critd_p->flag) == 0 /* never unlocked the first time */) {
                        memset(deadlock_str, 0, sizeof(deadlock_str));
                        critd_dbg_list_(locked, &deadlock_print, mid, 1, 0, critd_p);
                        // The following call to printf() used to be a call to
                        // T_E(), but if going through the trace module, we
                        // cannot guarantee that it comes out to the console,
                        // because it uses the syslog, which takes mutexes that
                        // may be locked (led module's and syslog's own). The
                        // drawback of not calling T_E() is that we don't get it
                        // saved to flash.
                        printf("\n%s\n", deadlock_str);
                    }

                    critd_p = critd_p->nxt;
                }
            }

            // For Linux we simply stop all threads, and make the assert print the thread details and reboot the switch.
            VTSS_ASSERT(0);

            // Unreachable
        }
    }
} // critd_thread

// ===========================================================================

// ---------------------------------------------------------------------------
// Internal functions
// ###########################################################################


// ###########################################################################
// API functions
// ---------------------------------------------------------------------------

// Initialize critd_t
void critd_init(
    critd_t*               const crit_p,
    const char*            const name,
    const vtss_module_id_t module_id,
    const int              trace_module_id,
    const critd_type_t     type)
{
    CritdTblLock locked;
    memset(crit_p, 0, sizeof(critd_t));

    crit_p->max_lock_time = CRITD_MAX_LOCK_TIME_DEFAULT;
    crit_p->cookie        = CRITD_T_COOKIE;
    crit_p->type          = type;

    // From the application point of view, the critd is created locked
    // to allow the application to do some initializations of the data
    // it protects before opening up for other modules to access the
    // data.
    // In reality, this is not possible with mutexes, so a flag is used
    // to indicate whether the critd has been exited the first time.
    // To make the implementation alike for both semaphores and mutexes,
    // the semaphore is created unlocked, and the flag is used to stop
    // access until the initial critd_exit() call.
    vtss_flag_init(&crit_p->flag);

    switch (type) {
        case CRITD_TYPE_MUTEX:
            // Always created unlocked.
            vtss_mutex_init(&crit_p->m.mutex);
            break;

        case CRITD_TYPE_SEMAPHORE:
            // Create it unlocked. The @flag holds back accesses.
            vtss_sem_init(&crit_p->m.semaphore, 1);
            break;

        case CRITD_TYPE_MUTEX_RECURSIVE:
            // Created unlocked
            vtss_recursive_mutex_init(&crit_p->m.rmutex);
            break;

        default:
            VTSS_ASSERT(0);
    }

#if VTSS_TRACE_ENABLED || CRITD_CHK
    strncpy(crit_p->name, name, MIN(CRITD_NAME_LEN, strlen(name)));
    crit_p->trace_module_id          = trace_module_id;
    crit_p->current_lock_thread_id   = CRITD_THREAD_ID_NONE;
    crit_p->nxt                      = NULL;

    // Insert into front of the linked list
    if (locked.critd_tbl(module_id)) {
        crit_p->nxt = locked.critd_tbl(module_id);
    }
    locked.critd_tbl(module_id, crit_p);
#endif

#if VTSS_TRACE_ENABLED
    // Initially, the critd is taken, so update the time for the first lock.
    // and pretend the user knows where this function is called from.
    crit_p->lock_tick_cnt            = vtss_current_time();
    crit_p->max_lock_tick_cnt        = 0;
    crit_p->total_lock_tick_cnt      = 0;
    crit_p->max_lock_thread_id       = critd_get_thread_id();
#endif

    crit_p->init_done = 1;
} // critd_init

void critd_delete(critd_t *const crit_p)
{
#if VTSS_TRACE_ENABLED || CRITD_CHK
    CritdTblLock locked;
    critd_t *i = nullptr, *prev = nullptr;

    // Find the entry located _before_ crit_p, as we need that to delete an
    // entry in a single linked list
    i = locked.critd_tbl(crit_p->trace_module_id);
    while (i && i != crit_p) {
        prev = i;
        i = i->nxt;
    }

    if (!i) {
        T_E("Critd (%p) was not found in the monitor list!", crit_p);
        return;
    }

    if (!prev) {
        // No previous pointer means that the entry was found in front of the
        // list, this means that we need to update the head pointer.
        locked.critd_tbl(crit_p->trace_module_id, crit_p->nxt);
    } else {
        prev->nxt = crit_p->nxt;
    }
#endif

    switch (crit_p->type) {
        case CRITD_TYPE_MUTEX:
        case CRITD_TYPE_MUTEX_RECURSIVE:
            vtss_mutex_destroy(&crit_p->m.mutex);
            break;

        case CRITD_TYPE_SEMAPHORE:
            vtss_sem_destroy(&crit_p->m.semaphore);
            break;

        default:
            VTSS_ASSERT(0);
    }

    vtss_flag_destroy(&crit_p->flag);
}

// Enter critical region
void critd_enter(critd_t*    const crit_p,
                 const int   trace_grp,
                 const int   trace_lvl,
                 const char* const file,
                 const int   line,
                 bool        dry_run)
{
    ulong my_thread_id = critd_get_thread_id();

#if VTSS_TRACE_ENABLED || CRITD_CHK
    VTSS_ASSERT(crit_p->init_done);

    // Assert that this thread has not already locked this critical region.
    // This check is only possible for mutexes. For semaphores, it's indeed
    // quite possible that the same thread attempts to take the semaphore twice
    // before the unlock (typically called from another thread) occurs. For
    // recursive mutexes, this must be allowed, that is the point in using a
    // recursive mutex.
    if (crit_p->type                   == CRITD_TYPE_MUTEX     &&
        crit_p->current_lock_thread_id != CRITD_THREAD_ID_NONE &&
        crit_p->current_lock_thread_id == my_thread_id) {
        critd_trace(crit_p, trace_grp, VTSS_TRACE_LVL_ERROR, file, line);
        CRITD_ASSERT(0,  "Critical region already locked by this thread id!");
    }
#endif

#if VTSS_TRACE_ENABLED
    // Store information about lock attempt
    uint idx;
    {
        CritdUpdateLock locked;
        ulong lock_cnt_new;
        lock_cnt_new = crit_p->lock_attempt_cnt;
        lock_cnt_new++;
        idx = (lock_cnt_new % CRITD_LOCK_ATTEMPT_SIZE);
        crit_p->lock_attempt_thread_id[idx] = my_thread_id;
        crit_p->lock_attempt_line[idx]      = line;
        crit_p->lock_attempt_file[idx]      = file;
        crit_p->lock_pending[idx]           = 1;
        crit_p->lock_attempt_cnt            = lock_cnt_new;
    }
#endif

    // Here is the trick that causes the locking of all
    // waiters until the very first critd_exit() call has
    // taken place.
    // The flag is initially 0, and will be set on the
    // first call to critd_exit(), and remain set ever after,
    // causing this call to be fast.
    vtss_flag_wait(&crit_p->flag, 1, VTSS_FLAG_WAITMODE_OR);

    if (!dry_run) {
        switch (crit_p->type) {
            case CRITD_TYPE_MUTEX:
                vtss_mutex_lock(&crit_p->m.mutex);
                break;

            case CRITD_TYPE_SEMAPHORE:
                vtss_sem_wait(&crit_p->m.semaphore);
                break;

            case CRITD_TYPE_MUTEX_RECURSIVE:
                vtss_recursive_mutex_lock(&crit_p->m.rmutex);
                break;

            default:
                VTSS_ASSERT(0);
        }
    }

#if VTSS_TRACE_ENABLED || CRITD_CHK
    if ((crit_p->type == CRITD_TYPE_MUTEX_RECURSIVE &&
         crit_p->m.rmutex.lock_cnt == 1) ||
        crit_p->type != CRITD_TYPE_MUTEX_RECURSIVE) {
        // Store information about lock
        crit_p->current_lock_thread_id = my_thread_id;
        crit_p->lock_thread_id         = my_thread_id;
        crit_p->lock_file              = file;
        crit_p->lock_line              = line;
        crit_p->lock_time              = time(NULL);
        crit_p->lock_tick_cnt          = vtss_current_time();
        crit_p->lock_pending[idx]      = 0;
        crit_p->lock_cnt++;
    }
#endif

} // critd_enter

// Exit critical region
void critd_exit(critd_t*   const crit_p,
                const int   trace_grp,
                const int   trace_lvl,
                const char* const file,
                const int   line,
                bool        dry_run)
{
#if VTSS_TRACE_ENABLED || CRITD_CHK
    ulong my_thread_id = critd_get_thread_id();
#endif

    if (vtss_flag_peek(&crit_p->flag) == 0) {
        // This is the very first call to critd_exit().
        // The call must open up for other threads waiting for the mutex.
        // This is accomplished by setting the event flag that others may
        // wait for in the critd_enter(). Once that is done, we simply return
        // because the mutex is not really taken, so we shouldn't unlock it.
        vtss_flag_setbits(&crit_p->flag, 1);
#if VTSS_TRACE_ENABLED
        // The very first time it's exited, we only update the time it took
        // and pretend that we know where it came from.
        crit_p->max_lock_file        = "critd_init";
        crit_p->max_lock_tick_cnt    = vtss_current_time() - crit_p->lock_tick_cnt;
        crit_p->total_lock_tick_cnt  = crit_p->max_lock_tick_cnt;
#endif
        goto do_exit; // Since the mutex was already unlocked, nothing more to do here, except some trace stuff.
    }

#if VTSS_TRACE_ENABLED || CRITD_CHK
    // Assert that critical region is currently locked
    if (!critd_is_locked(crit_p)) {
        critd_trace(crit_p, trace_grp, VTSS_TRACE_LVL_ERROR, file, line);
        CRITD_ASSERT(0, "Unlock called, but critical region is not locked!");
    }

    // If it's a mutex or recursive mutex, the unlocking thread can never differ from the locking thread.
    // If it's a semaphore, this is indeed typically the case.
    if (crit_p->type == CRITD_TYPE_MUTEX ||
        crit_p->type == CRITD_TYPE_MUTEX_RECURSIVE) {
        // Assert that any current lock is this thread
        if (crit_p->lock_thread_id != CRITD_THREAD_ID_NONE &&
            crit_p->lock_thread_id != my_thread_id) {
            critd_trace(crit_p, trace_grp, VTSS_TRACE_LVL_ERROR, file, line);
            CRITD_ASSERT(0, "Unlock called, but mutex is locked by different thread id!");
        }
    }

    // Clear current lock id
    crit_p->current_lock_thread_id = CRITD_THREAD_ID_NONE;

    // Store information about unlock
    crit_p->unlock_thread_id  = my_thread_id;
    crit_p->unlock_file       = file;
    crit_p->unlock_line       = line;
#endif
#if VTSS_TRACE_ENABLED
    {
        vtss_tick_count_t diff_ticks = vtss_current_time() - crit_p->lock_tick_cnt;
        crit_p->total_lock_tick_cnt += diff_ticks;
        if (diff_ticks > crit_p->max_lock_tick_cnt) {
            crit_p->max_lock_tick_cnt  = diff_ticks;
            crit_p->max_lock_file      = crit_p->lock_file;
            crit_p->max_lock_line      = crit_p->lock_line;
            crit_p->max_lock_thread_id = crit_p->lock_thread_id;
        }
    }
#endif

    if (!dry_run) {
        switch (crit_p->type) {
            case CRITD_TYPE_MUTEX:
                vtss_mutex_unlock(&crit_p->m.mutex);
                break;

            case CRITD_TYPE_SEMAPHORE:
                vtss_sem_post(&crit_p->m.semaphore);
                break;

            case CRITD_TYPE_MUTEX_RECURSIVE:
                vtss_recursive_mutex_unlock(&crit_p->m.rmutex);
                break;

            default:
                VTSS_ASSERT(0);
        }
    }

do_exit:
    ;
} // critd_exit

void critd_assert_locked(
    critd_t*   const crit_p
#if VTSS_TRACE_ENABLED
    ,
    const int   trace_grp,
    const char* const file,
    const int   line
#endif
    )
{
    if (critd_peek(crit_p) != 0) {
        critd_trace(crit_p, trace_grp, VTSS_TRACE_LVL_ERROR, file, line);
        CRITD_ASSERT(0, "Critical region not locked!");
    }
} // critd_assert_locked

// Debug functions
// ---------------
// To be used from CLI as well as to print a single critd instance in
// case of deadlock detection (in surveillance thread).
static void critd_dbg_list_(
    CritdTblLock             &locked,
    const critd_dbg_printf_t dbg_printf,
    const vtss_module_id_t   module_id,
    const BOOL               detailed,
    const BOOL               header,
          critd_t            *single_critd_p)
{
#if VTSS_TRACE_ENABLED || CRITD_CHK
    int i;
    const uint MODULE_NAME_WID = 21;
    char module_name_format[10];
    char critd_name_format[10];
    vtss_module_id_t mid_start = 0;
    vtss_module_id_t mid_end = VTSS_MODULE_ID_NONE-1;
    vtss_module_id_t mid;
    int crit_cnt = 0;
    BOOL first = header;

    /* Work-around for problem with printf("%*s", ...) */
    sprintf(module_name_format, "%%-%ds", MODULE_NAME_WID);
    sprintf(critd_name_format,  "%%-%ds", CRITD_NAME_LEN);

    if (module_id != VTSS_MODULE_ID_NONE) {
        mid_start = module_id;
        mid_end   = module_id;
    }

    for (mid = mid_start; mid <= mid_end; mid++) {
        critd_t *critd_p;
        if (single_critd_p) {
            critd_p = single_critd_p;
        } else {
            critd_p = locked.critd_tbl(mid);
        }
        while (critd_p) {
            if (critd_p->cookie != CRITD_T_COOKIE) {
                T_E("Cookie=0x%08x, expected 0x%08x", critd_p->cookie, CRITD_T_COOKIE);
            }

            crit_cnt++;
            if (first) {
                if (!detailed)
                    first = 0;

                dbg_printf(module_name_format, "Module");
                dbg_printf(" ");
                dbg_printf(critd_name_format, "Critd Name");
                dbg_printf(" ");
                dbg_printf("T");
                dbg_printf(" ");
                dbg_printf("State   "); // "Unlocked" or "Locked"
                dbg_printf(" ");
                dbg_printf("Lock Cnt  ");
                dbg_printf(" ");
                dbg_printf("LockTime ");
                dbg_printf("Latest Lock, Latest Unlock\n");
                for (i = 0; i < MODULE_NAME_WID; i++) dbg_printf("-");
                dbg_printf(" ");
                for (i = 0; i < CRITD_NAME_LEN; i++) dbg_printf("-");
                dbg_printf(" ");
                dbg_printf("- -------- ---------- -------- ------------------------------\n");
            }

            dbg_printf(module_name_format, vtss_module_names[mid]);
            dbg_printf(" ");
            dbg_printf(critd_name_format, critd_p->name);
            dbg_printf(" ");
            dbg_printf("%c", critd_p->type == CRITD_TYPE_MUTEX ? 'M' : critd_p->type == CRITD_TYPE_MUTEX_RECURSIVE ? 'R' : 'S');
            dbg_printf(" ");
            if (critd_p->current_lock_thread_id) {
                dbg_printf("Locked  ");
            } else if (vtss_flag_peek(&critd_p->flag) == 0) {
                dbg_printf("Unexited");
            } else {
                dbg_printf("Unlocked");
            }

            dbg_printf(" ");

            // Lock cnt
            if (detailed) {
                // Print it in hex in order to be able to compare with lock_attempt_cnt
                char buf[20];
                sprintf(buf, "0x%x", critd_p->lock_cnt);
                dbg_printf("%10s ", buf);
            } else {
                // Print it in decimal, which makes more sense when showing all critds in a list
                dbg_printf("%10u ", critd_p->lock_cnt);
            }

            {
                // Print lock time
                struct tm *timeinfo_p;
                time_t lock_time;

                lock_time = critd_p->lock_time;
#ifdef VTSS_SW_OPTION_SYSUTIL
                lock_time += (system_get_tz_off() * 60); /* Adjust for TZ minutes => seconds */
#endif
#ifdef VTSS_SW_OPTION_DAYLIGHT_SAVING
                lock_time += (time_dst_get_offset() * 60); /* Correct for DST */
#endif
                timeinfo_p = localtime(&lock_time);

                dbg_printf("%02d:%02d:%02d ",
                           timeinfo_p->tm_hour,
                           timeinfo_p->tm_min,
                           timeinfo_p->tm_sec);
            }
            {
                // Protect against concurrent update of lock information
                char file_lock[100] = "";
                char file_unlock[100] = "";
                strncpy(file_lock,   critd_p->lock_file   ? misc_filename(critd_p->lock_file)   : "",   98);
                strncpy(file_unlock, critd_p->unlock_file ? misc_filename(critd_p->unlock_file) : "", 98);
                file_lock[99]   = 0;
                file_unlock[99] = 0;

                dbg_printf(VPRIlu"/%s#%d, " VPRIlu"/%s#%d",
                           critd_p->lock_thread_id,
                           file_lock,
                           critd_p->lock_line,
                           critd_p->unlock_thread_id,
                           file_unlock,
                           critd_p->unlock_line);
            }

            if (detailed) {
                int i;
                char file_lock[100] = "";
                int j = 0;
                dbg_printf(", lock_attempt_cnt=0x" VPRIlx, critd_p->lock_attempt_cnt);
                for (i = 0; i < CRITD_LOCK_ATTEMPT_SIZE; i++) {
                    if (j++ % 4 == 0) {
                        dbg_printf("\n  ");
                    }
                    strncpy(file_lock,   critd_p->lock_attempt_file[i] ? misc_filename(critd_p->lock_attempt_file[i])   : "",   98);
                    dbg_printf("[%x]=" VPRIlu"/%s#%d%s ",
                               i,
                               critd_p->lock_attempt_thread_id[i],
                               file_lock,
                               critd_p->lock_attempt_line[i],
                               critd_p->lock_pending[i] ? "*" : " ");
                }
            }
            if (single_critd_p) {
                critd_p = NULL;
            } else {
                dbg_printf("\n");
                if (detailed) dbg_printf("\n");

                critd_p = critd_p->nxt;
            }
        }
    }
    if (module_id != VTSS_MODULE_ID_NONE && crit_cnt == 0) {
    } else if (detailed && header) {
        dbg_printf("Note that the logging of lock attempts could be incorrect, since it is not protected by any critical region.\n");
        dbg_printf("However in most cases the information will likely be correct.\n");
        dbg_printf("\n");
        dbg_printf("The last logged lock attempt is stored in entry [lock_attempt_cnt %% %d].\n",
                   CRITD_LOCK_ATTEMPT_SIZE);
    }
#else
    dbg_printf("Not supported.\n");
#endif

} // critd_dbg_list

void critd_dbg_list(
    const critd_dbg_printf_t dbg_printf,
    const vtss_module_id_t   module_id,
    const BOOL               detailed,
    const BOOL               header,
          critd_t            *single_critd_p)
{
    CritdTblLock locked;
    critd_dbg_list_(locked, dbg_printf, module_id, detailed, header,
                    single_critd_p);
}


#ifdef VTSS_SW_OPTION_ICLI
void critd_dbg_list_icli(
    const u32               session_id,
    const vtss_module_id_t  module_id,
    const BOOL              detailed,
    const BOOL              header,
    const critd_t           *single_critd_p
)
{
#if VTSS_TRACE_ENABLED || CRITD_CHK
    int                 i;
    const uint          MODULE_NAME_WID = 21;
    char                module_name_format[10];
    char                critd_name_format[10];
    vtss_module_id_t    mid_start = 0;
    vtss_module_id_t    mid_end = VTSS_MODULE_ID_NONE-1;
    vtss_module_id_t    mid;
    int                 crit_cnt = 0;
    BOOL                first = header;

    /* Work-around for problem with printf("%*s", ...) */
    sprintf(module_name_format, "%%-%ds", MODULE_NAME_WID);
    sprintf(critd_name_format,  "%%-%ds", CRITD_NAME_LEN);

    if (module_id != VTSS_MODULE_ID_NONE) {
        mid_start = module_id;
        mid_end   = module_id;
    }

    for (mid = mid_start; mid <= mid_end; mid++) {
        const critd_t *critd_p;
        CritdTblLock locked;
        if (single_critd_p) {
            critd_p = single_critd_p;
        } else {
            critd_p = locked.critd_tbl(mid);
        }
        while (critd_p) {
            if (critd_p->cookie != CRITD_T_COOKIE) {
                T_E("Cookie=0x%08x, expected 0x%08x", critd_p->cookie, CRITD_T_COOKIE);
            }

            crit_cnt++;
            if (first) {
                if (!detailed)
                    first = 0;
                ICLI_PRINTF(module_name_format, "Module");
                ICLI_PRINTF(" ");
                ICLI_PRINTF(critd_name_format, "Critd Name");
                ICLI_PRINTF(" ");
                ICLI_PRINTF("T");
                ICLI_PRINTF(" ");
                ICLI_PRINTF("State   "); // "Unlocked" or "Locked"
                ICLI_PRINTF(" ");
                ICLI_PRINTF("Lock Cnt  ");
                ICLI_PRINTF(" ");
                ICLI_PRINTF("LockTime ");
                ICLI_PRINTF("Latest Lock, Latest Unlock\n");
                for (i = 0; i < MODULE_NAME_WID; i++) ICLI_PRINTF("-");
                ICLI_PRINTF(" ");
                for (i = 0; i < CRITD_NAME_LEN; i++) ICLI_PRINTF("-");
                ICLI_PRINTF(" ");
                ICLI_PRINTF("- -------- ---------- -------- ------------------------------\n");
            }

            ICLI_PRINTF(module_name_format, vtss_module_names[mid]);
            ICLI_PRINTF(" ");
            ICLI_PRINTF(critd_name_format, critd_p->name);
            ICLI_PRINTF(" ");
            ICLI_PRINTF("%c", critd_p->type == CRITD_TYPE_MUTEX ? 'M' : critd_p->type == CRITD_TYPE_MUTEX_RECURSIVE ? 'R' : 'S');
            ICLI_PRINTF(" ");
            if (critd_p->current_lock_thread_id == CRITD_THREAD_ID_NONE) {
                ICLI_PRINTF("Unlocked");
            } else {
                ICLI_PRINTF("Locked  ");
            }
            ICLI_PRINTF(" ");

            // Lock cnt
            ICLI_PRINTF("%10u ", critd_p->lock_cnt);

            {
                // Print lock time
                struct tm *timeinfo_p;
                time_t lock_time;

                lock_time = critd_p->lock_time;
#ifdef VTSS_SW_OPTION_SYSUTIL
                lock_time += (system_get_tz_off() * 60); /* Adjust for TZ minutes => seconds */
#endif
#ifdef VTSS_SW_OPTION_DAYLIGHT_SAVING
                lock_time += (time_dst_get_offset() * 60); /* Correct for DST */
#endif
                timeinfo_p = localtime(&lock_time);

                ICLI_PRINTF("%02d:%02d:%02d ",
                           timeinfo_p->tm_hour,
                           timeinfo_p->tm_min,
                           timeinfo_p->tm_sec);
            }
            {
                // Protect against concurrent update of lock information
                char file_lock[100] = "";
                char file_unlock[100] = "";
                strncpy(file_lock,   critd_p->lock_file   ? misc_filename(critd_p->lock_file)   : "",   98);
                strncpy(file_unlock, critd_p->unlock_file ? misc_filename(critd_p->unlock_file) : "", 98);
                file_lock[99]   = 0;
                file_unlock[99] = 0;

                ICLI_PRINTF("%lu/%s#%d, %lu/%s#%d",
                           critd_p->lock_thread_id,
                           file_lock,
                           critd_p->lock_line,
                           critd_p->unlock_thread_id,
                           file_unlock,
                           critd_p->unlock_line);
            }

            if (detailed) {
                int i;
                char file_lock[100] = "";
                int j = 0;
                ICLI_PRINTF(", lock_attempt_cnt=0x%lx", critd_p->lock_attempt_cnt);
                for (i = 0; i < CRITD_LOCK_ATTEMPT_SIZE; i++) {
                    if (j++ % 4 == 0) {
                        ICLI_PRINTF("\n  ");
                    }
                    strncpy(file_lock,   critd_p->lock_attempt_file[i] ? misc_filename(critd_p->lock_attempt_file[i])   : "",   98);
                    ICLI_PRINTF("[%x]=%lu/%s#%d%s ",
                               i,
                               critd_p->lock_attempt_thread_id[i],
                               file_lock,
                               critd_p->lock_attempt_line[i],
                               critd_p->lock_pending[i] ? "*" : " ");
                }
            }
            if (single_critd_p) {
                critd_p = NULL;
            } else {
                ICLI_PRINTF("\n");
                if (detailed) ICLI_PRINTF("\n");

                critd_p = critd_p->nxt;
            }
        }
    }
    if (module_id != VTSS_MODULE_ID_NONE && crit_cnt == 0) {
    } else if (detailed && header) {
        ICLI_PRINTF("Note that the logging of lock attempts could be incorrect, since it is not protected by any critical region.\n");
        ICLI_PRINTF("However in most cases the information will likely be correct.\n");
        ICLI_PRINTF("\n");
        ICLI_PRINTF("The last logged lock attempt is stored in entry [lock_attempt_cnt %% %d].\n",
                   CRITD_LOCK_ATTEMPT_SIZE);
    }
#else
    ICLI_PRINTF("Not supported.\n");
#endif

}

extern "C"
void critd_dbg_max_lock_icli(
    const u32               session_id,
    const vtss_module_id_t  module_id,
    const BOOL              header,
    const BOOL              clear
)
{
#if VTSS_TRACE_ENABLED
    int                 i;
    const uint          MODULE_NAME_WID = 21;
    char                module_name_format[10];
    char                critd_name_format[10];
    vtss_module_id_t    mid_start = 0;
    vtss_module_id_t    mid_end = VTSS_MODULE_ID_NONE-1;
    vtss_module_id_t    mid;
    char                file_lock[100] = "";
    BOOL                first = header;

    /* Work-around for problem with printf("%*s", ...) */
    sprintf(module_name_format, "%%-%ds", MODULE_NAME_WID);
    sprintf(critd_name_format,  "%%-%ds", CRITD_NAME_LEN);

    if (module_id != VTSS_MODULE_ID_NONE) {
        mid_start = module_id;
        mid_end   = module_id;
    }

    for (mid = mid_start; mid <= mid_end; mid++) {
        CritdTblLock locked;
        critd_t *critd_p = locked.critd_tbl(mid);
        while (critd_p) {
            if (critd_p->cookie != CRITD_T_COOKIE) {
                T_E("Cookie=0x%08x, expected 0x%08x", critd_p->cookie, CRITD_T_COOKIE);
            }

            if (clear) {
                critd_p->max_lock_tick_cnt   = 0;
                critd_p->total_lock_tick_cnt = 0;
                critd_p->max_lock_file       = "";
                critd_p->max_lock_line       = 0;
                critd_p->max_lock_thread_id  = CRITD_THREAD_ID_NONE;
            } else {
                if (first) {
                    first = 0;
                    ICLI_PRINTF(module_name_format, "Module");
                    ICLI_PRINTF(" ");
                    ICLI_PRINTF(critd_name_format, "Critd Name");
                    ICLI_PRINTF(" T Max Lock [ms] Tot Lock [ms] Lock Position\n");

                    for (i = 0; i < MODULE_NAME_WID; i++) ICLI_PRINTF("-");
                    ICLI_PRINTF(" ");
                    for (i = 0; i < CRITD_NAME_LEN; i++) ICLI_PRINTF("-");
                    ICLI_PRINTF(" ");
                    ICLI_PRINTF("- ------------- ------------- ------------------------------\n");
                }

                ICLI_PRINTF(module_name_format, vtss_module_names[mid]);
                ICLI_PRINTF(" ");
                ICLI_PRINTF(critd_name_format, critd_p->name);
                ICLI_PRINTF(" %c", critd_p->type == CRITD_TYPE_MUTEX ? 'M' : critd_p->type == CRITD_TYPE_MUTEX_RECURSIVE ? 'R' : 'S');

                // Max. lock time
                ICLI_PRINTF(" " VPRI64Fu("13"), VTSS_OS_TICK2MSEC(critd_p->max_lock_tick_cnt));

                // Total lock time
                ICLI_PRINTF(" " VPRI64Fu("13") " ", VTSS_OS_TICK2MSEC(critd_p->total_lock_tick_cnt));

                // Lock file and line number
                // Protect against concurrent update of lock information
                strncpy(file_lock, critd_p->max_lock_file ? misc_filename(critd_p->max_lock_file) : "", 98);
                file_lock[99]   = 0;
                ICLI_PRINTF("%lu/%s#%d\n", critd_p->max_lock_thread_id, file_lock, critd_p->max_lock_line);
            }

            critd_p = critd_p->nxt;
        }
    }
#else
    ICLI_PRINTF("Not supported.\n");
#endif
}

#endif // VTSS_SW_OPTION_ICLI

mesa_rc critd_module_init(vtss_init_data_t *data)
{
    mesa_rc rc = VTSS_OK;

    if (data->cmd == INIT_CMD_EARLY_INIT) {
        // Initialize and register trace ressources
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);
    }

    switch (data->cmd) {
    case INIT_CMD_INIT:
        // Create thread
        vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
                           critd_thread,
                           0,
                           "Critd",
                           nullptr,
                           0,
                           &critd_thread_handle,
                           &critd_thread_block);
        break;

    default:
        break;
    }

    return rc;
} // critd_module_init


// ---------------------------------------------------------------------------
// API functions
// ###########################################################################


/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
