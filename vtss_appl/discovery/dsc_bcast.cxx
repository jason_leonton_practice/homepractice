/*************************************************************************************************
 * Copyright (C) Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: Leonton Discovery Broadcast Server
 *
 *************************************************************************************************/
#include "main.h"
#include <sys/socket.h>
#include <netinet/in.h>
//#include <network.h>


#include "discovery_api.h"
#include "dsc_def.h"
#include "dsc_bcast.h"
#include "dsc_packet.h"
#include "dsc_cdnt.h"

#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_DISCOVERY
/**************************************************************************************************
 * PRIVATE FUNCTIONS
 *************************************************************************************************/

static int dsc_bcast_recv_file(dsc_packet_info_t *pkt_info)
{
	int						events = 0;
	int						len = 0;
	socklen_t				addr_size = sizeof(struct sockaddr_in);
	fd_set					rdset;
	struct timeval			tv;

	while(pkt_info->packet_len > 0) {
		FD_ZERO(&rdset);
		tv.tv_sec = (DSC_SOCK_SELECT_TIMEOUT) / 1000;
		tv.tv_usec = (DSC_SOCK_SELECT_TIMEOUT % 1000) * 1000;
		FD_SET(pkt_info->sock_fd, &rdset);
		memset(pkt_info->sock_buff, 0, DSC_MAX_BUFF_LEN);

		events = select(pkt_info->sock_fd + 1, &rdset, NULL, NULL, &tv);

		if(events > 0) {
			if((len = recvfrom(pkt_info->sock_fd, pkt_info->sock_buff, DSC_MAX_BUFF_LEN, 0, pkt_info->peer_addr, &addr_size)) <= 0) {
				pkt_info->error = DSC_ERR_PKT_RECV;
				pkt_info->packet_len = len;
				return DSC_FAILURE;
			}
			if(!dsc_cdnt_write_buf(pkt_info, (unsigned int)len)) {
				return DSC_FAILURE;
			}
		} else if(events == 0) {
			pkt_info->error = DSC_ERR_PKT_TIMEOUT;
			return DSC_FAILURE;
		} else {
			pkt_info->error = DSC_ERR_PKT_SELECT;
			return DSC_FAILURE;
		}
	}

	return DSC_SUCCESS;
}

static int dsc_bcast_recv(dsc_packet_info_t *pkt_info)
{
	int					len = 0;
	socklen_t			addr_size = sizeof(struct sockaddr_in);

	if((len = recvfrom(pkt_info->sock_fd, pkt_info->sock_buff, DSC_MAX_BUFF_LEN, 0, pkt_info->peer_addr, &addr_size)) <= 0) {
		pkt_info->error = DSC_ERR_PKT_RECV;
		pkt_info->packet_len = len;
		return DSC_FAILURE;
	}

	if(!dsc_parse_head((dsc_pkt_head_t *)pkt_info->sock_buff, &(pkt_info->type), &(pkt_info->error), &(pkt_info->packet_len))) {
		T_D("Header parse failed! error: %d",pkt_info->error);
		VTSS_OS_MSLEEP(DSC_DELAY);
		return DSC_FAILURE;
	}

	return DSC_SUCCESS;
}

static int dsc_bcast_send(dsc_packet_info_t *pkt_info)
{
	int			addr_size = sizeof(struct sockaddr_in);
	int			len = 0;

	if(sendto(pkt_info->sock_fd, pkt_info->sock_buff, pkt_info->packet_len, 0, pkt_info->peer_addr, addr_size) < 0) {
		pkt_info->error = DSC_ERR_PKT_SEND;
		return DSC_FAILURE;
	}

	pkt_info->packet_len = pkt_info->data_len;

	if(pkt_info->type == DSC_TYPE_DO_EXPORT) {
		while(pkt_info->packet_len > 0) {
			memset(pkt_info->sock_buff, 0, DSC_MAX_BUFF_LEN);
			if(!dsc_cdnt_read_buf(pkt_info, (unsigned int *)&len)) {
				VTSS_FREE(pkt_info->data_buff);
				return DSC_FAILURE;
			}

			if(sendto(pkt_info->sock_fd, pkt_info->sock_buff, len, 0, pkt_info->peer_addr, addr_size) < 0) {
				pkt_info->error = DSC_ERR_PKT_SEND;
				VTSS_FREE(pkt_info->data_buff);
				return DSC_FAILURE;
			}

			pkt_info->packet_len -= len;
		}
		pkt_info->data_buff -= pkt_info->data_len;

		return dsc_action_dispatcher(pkt_info);
	}

    if(pkt_info->type == DSC_TYPE_CFG_IPADDR) {
        pkt_info->type =DSC_TYPE_DO_IPADDR;
        T_D("change DSC_TYPE_CFG_IPADDR -> DSC_TYPE_DO_IPADDR");
        dsc_action_dispatcher(pkt_info);
    }

	if(pkt_info->reboot) {
		dsc_cdnt_reboot();
	}

	return DSC_SUCCESS;
}


/**************************************************************************************************
 * PUBLIC FUNCTIONS
 *************************************************************************************************/
void dsc_bcast_handle(int sock_fd)
{
	char						sock_buff[DSC_MAX_BUFF_LEN];
	dsc_packet_info_t			pkt_info;
	struct sockaddr_in			peer_addr;

	memset(&pkt_info, 0, sizeof(dsc_packet_info_t));
	memset(sock_buff, 0, DSC_MAX_BUFF_LEN);
	memset(&peer_addr, 0, sizeof(struct sockaddr_in));

	pkt_info.sock_fd	= sock_fd;
	pkt_info.peer_addr	= (struct sockaddr *)&peer_addr;
	pkt_info.sock_buff	= sock_buff;

	if(!dsc_bcast_recv(&pkt_info)) {
		T_D("dsc_bcast_recv failed! [error: %d, len: %d]", pkt_info.error, pkt_info.packet_len);
		return;
	}

	if(!dsc_action_dispatcher(&pkt_info)) {
		T_D("dsc_action_dispatcher failed! [error: %d]",pkt_info.error);
		return;
	} else {
		if(pkt_info.type == DSC_TYPE_CFG_SINGLE_UPGRADE || pkt_info.type == DSC_TYPE_CFG_DUAL_UPGRADE) {
			if(pkt_info.error == DSC_ERR_NONE) {
				if(!dsc_bcast_recv_file(&pkt_info)) {
					T_D("dsc_bcast_recv_file failed! [error: %d, len: %d]\n", pkt_info.error, pkt_info.packet_len);
					VTSS_FREE(pkt_info.data_buff);
					pkt_info.data_buff = NULL;
					return;
				}

				if(pkt_info.type == DSC_TYPE_CFG_SINGLE_UPGRADE) {
					pkt_info.type = DSC_TYPE_DO_SINGLE_UPGRADE;
				} else {
					pkt_info.type = DSC_TYPE_DO_DUAL_UPGRADE;
				}

				if(!dsc_action_dispatcher(&pkt_info)) {
					T_D("Redo dsc_action_dispatcher failed! [error: %d]\n", pkt_info.error);
					VTSS_FREE(pkt_info.data_buff);
					pkt_info.data_buff = NULL;
					return;
				}
			}
		} else if(pkt_info.type == DSC_TYPE_CFG_IMPORT) {
			if(pkt_info.error == DSC_ERR_NONE) {
				if(!dsc_bcast_recv_file(&pkt_info)) {
					T_D("dsc_bcast_recv_file failed! [error: %d, len: %d]\n", pkt_info.error, pkt_info.packet_len);
					VTSS_FREE(pkt_info.data_buff);
					pkt_info.data_buff = NULL;
					return;
				}
				pkt_info.type = DSC_TYPE_DO_IMPORT;
				if(!dsc_action_dispatcher(&pkt_info)) {
					T_D("Redo dsc_action_dispatcher failed! [error: %d]\n", pkt_info.error);
					VTSS_FREE(pkt_info.data_buff);
					pkt_info.data_buff = NULL;
					return;
				}
			}
			VTSS_FREE(pkt_info.data_buff);
			pkt_info.data_buff = NULL;
		} else if(pkt_info.type == DSC_TYPE_CFG_EXPORT) {
			if(pkt_info.data_len > 0 && pkt_info.error == DSC_ERR_NONE) {
				pkt_info.type = DSC_TYPE_DO_EXPORT;
			}
		} else if(pkt_info.type == DSC_TYPE_PROBEMATCH) {
			return;
		}
	}

	if(!dsc_bcast_send(&pkt_info)) {
		T_D("dsc_bcast_send failed! [error: %d, len: %d]\n", pkt_info.error, pkt_info.packet_len);
	}
}


void dsc_bcast_srv_close(int sock_fd)
{
	dsc_sel_rm_fd(sock_fd);
}

int dsc_bcast_srv_open(int *sock_fd)
{
	int						sfd = 0;
	struct sockaddr_in		bcast_addr;
	int						opt = 1;

	if(!sock_fd) {
		return DSC_FAILURE;
	}

	if((sfd = vtss_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		T_D("Socket open failed!");
		return DSC_FAILURE;
	}

	memset(&bcast_addr, 0, sizeof(struct sockaddr_in));

	bcast_addr.sin_family = AF_INET;
	bcast_addr.sin_port = ntohs(DSC_BCAST_SRV_LISTEN_PORT);

	if(setsockopt(sfd, SOL_SOCKET, SO_BROADCAST, &opt, sizeof(int)) < 0) {
		T_D("Socket failed for broadcast!");
		dsc_bcast_srv_close(sfd);
		return DSC_FAILURE;
	}

	if(bind(sfd, (struct sockaddr *)(&bcast_addr), sizeof(bcast_addr)) < 0) {
		T_D("Binding of socket server failed!");
		dsc_bcast_srv_close(sfd);
		return DSC_FAILURE;
	}

	*sock_fd = sfd;

	return DSC_SUCCESS;
}


