 /*************************************************************************************************
 * Copyright (C) Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: Leonton Discovery Server Packet Header
 *
 *************************************************************************************************/
#ifndef _DSC_PACKET_H_
#define _DSC_PACKET_H_

#include "firmware_api.h"


#define DSC_MAX_MODEL_NAME_LEN					(32)
#define DSC_MAX_VENDOR_NAME_LEN					(32)
#define DSC_MAX_SN_STR_LEN						(32)
#define DSC_MAX_BUILDTIME_STR_LEN				(25)
#define DSC_MAX_MAC_STR_LEN						(18)
#define DSC_MAX_MAC_VER_LEN						(10)
#define DSC_MAX_MAC_LEN							(6)
#define DSC_MAX_SESSION_LEN						(6)
#define DSC_MAX_MAGIC_LEN						(2)
#define DSC_MAX_IP6_STRLEN						(40)
#define DSC_MAX_NAME_LEN						(64)
#define DSC_MAX_LOCATION_LEN					(64)
#define DSC_MAX_FWVER_LEN						(32)
#define DSC_MAX_ADDR_LEN						(32)
#define DSC_AUTH_HEAD_ID_LEN					(16)
#define DSC_AUTH_HEAD_PASSWD_LEN				(32)
#define DSC_MAX_SCRIPT_LEN						(400)
#define DSC_MAX_UPGRADE_ROM_SIZE				(64 * 1024 * 1024)	/*	64 MB	*/
#define DSC_MAX_CONFIG_BUFFER_SIZE				(512 * 1024)		/*	512 KB	*/
#define DSC_SRV_SERVICE_DELAY					(500)				/*	msec	*/


#define DSC_PKT_HEAD_MAGIC0						(0x41)	/* A */
#define DSC_PKT_HEAD_MAGIC1						(0x6C)	/* L */
#define DSC_PKT_HEAD_VERSION					(0x01)

#define DSC_IP_TLV_PKT_LEN						(16)
#define DSC_GW_TLV_PKT_LEN						(4)

#define DSC_PROBE_L2SWITCH 						(0x0001)

#define DSC_MGTNET_IPMOD_MANUAL					(1)
#define DSC_MGTNET_IPMOD_DHCP					(0)

#define DSC_UCAST_OPEN							(1)
#define DSC_UCAST_CLOSE							(0)

typedef enum {
	DSC_TYPE_PROBE = 0,
	DSC_TYPE_HELLO,

	//	Authentication info required. -------------------------- Begin
	DSC_TYPE_CFG_IPADDR,
	DSC_TYPE_CFG_GATEWAY,
	DSC_TYPE_CFG_MPFLAG,
	DSC_TYPE_CFG_CHGPASSWD,
	DSC_TYPE_CFG_SCRIPT,
	DSC_TYPE_CFG_SINGLE_UPGRADE,
	DSC_TYPE_CFG_DUAL_UPGRADE,
	DSC_TYPE_CFG_IMPORT,
	DSC_TYPE_CFG_EXPORT,
	DSC_TYPE_UCAST,
	//	Authentication info required. -------------------------- End
	DSC_TYPE_PROBEMATCH,
	DSC_TYPE_RESULT,

	DSC_TYPE_DO_SINGLE_UPGRADE,
	DSC_TYPE_DO_DUAL_UPGRADE,
	DSC_TYPE_DO_IMPORT,
	DSC_TYPE_DO_EXPORT,
	DSC_TYPE_DO_SAVE_CONFIG,
	DSC_TYPE_DO_RESET_DEFAULT,
	DSC_TYPE_DO_RESTART,
	DSC_TYPE_DO_IPADDR,
	DSC_TYPE_MAX
} dsc_type_t;

typedef enum {
	DSC_PLDTYPE_IPADDR = 0,
	DSC_PLDTYPE_GATEWAY
} dsc_payload_type_t;

typedef enum {
	DSC_RESET_DEFAULT_ALL = 0,
	DSC_RESET_DEFAULT_KEEP_IP
} dsc_reset_default_type_t;


typedef struct {
	char				model_name[DSC_MAX_MODEL_NAME_LEN + 1];
	char				vendor[DSC_MAX_VENDOR_NAME_LEN + 1];
	char				serial_no[DSC_MAX_SN_STR_LEN + 1];
	char				buildtime[DSC_MAX_BUILDTIME_STR_LEN + 1];
	char				mac[DSC_MAX_MAC_LEN];
	char				version[DSC_MAX_MAC_VER_LEN];
	unsigned int		mpflag;
} dsc_device_info_t;

typedef struct {
	unsigned char		magic[DSC_MAX_MAGIC_LEN];
	unsigned char		version;
	dsc_type_t			type;
} __attribute__((packed)) dsc_pkt_head_t;

typedef struct {
	char				target_mac[DSC_MAX_MAC_LEN];
	char				id[DSC_AUTH_HEAD_ID_LEN];
	char				passwd[DSC_AUTH_HEAD_PASSWD_LEN];
} __attribute__((packed)) dsc_auth_t;

typedef struct {
	dsc_pkt_head_t		head;
	unsigned short		flag;
	unsigned char		session[DSC_MAX_SESSION_LEN];
} __attribute__((packed)) dsc_pkt_probe_t;

typedef struct {
	dsc_pkt_head_t		head;
	unsigned short		flag;
	unsigned char		session[DSC_MAX_SESSION_LEN];
	char				model_name[DSC_MAX_MODEL_NAME_LEN];
	char				vendor[DSC_MAX_VENDOR_NAME_LEN];
	char				name[DSC_MAX_NAME_LEN];
	char				fw_version[DSC_MAX_FWVER_LEN];
	char				sn[DSC_MAX_SN_STR_LEN];
	char				location[DSC_MAX_LOCATION_LEN];
	char				mac[DSC_MAX_MAC_LEN];
	unsigned int        mpflag;
	unsigned int		payload_len;
} __attribute__((packed)) dsc_pkt_probematch_t;

typedef struct {
	dsc_pkt_head_t		head;
	unsigned short		flag;
	char				model_name[DSC_MAX_MODEL_NAME_LEN];
	char				vendor[DSC_MAX_VENDOR_NAME_LEN];
	char				name[DSC_MAX_NAME_LEN];
	char				fw_version[DSC_MAX_FWVER_LEN];
	char				sn[DSC_MAX_SN_STR_LEN];
	char				location[DSC_MAX_LOCATION_LEN];
	char				mac[DSC_MAX_MAC_LEN];
	unsigned int		payload_len;
} __attribute__((packed)) dsc_pkt_hello_t;

typedef struct {
	dsc_pkt_head_t		head;
	dsc_auth_t			auth;
	unsigned int		vlan_id;
	int					method;
	unsigned int		ipaddr;
	unsigned int		netmask;
} __attribute__((packed)) dsc_pkt_cfg_ipaddr_t;

typedef struct {
	dsc_pkt_head_t		head;
	dsc_auth_t			auth;
	unsigned int		gateway;
} __attribute__((packed)) dsc_pkt_cfg_gateway_t;

typedef struct {
	dsc_pkt_head_t		head;
	dsc_auth_t			auth;
	unsigned char		flag;
} __attribute__((packed)) dsc_pkt_cfg_mpflag_t;

typedef struct {
	dsc_pkt_head_t		head;
	dsc_auth_t			auth;
	char				passwd[DSC_AUTH_HEAD_PASSWD_LEN];
} __attribute__((packed)) dsc_pkt_cfg_passwd_t;

typedef struct {
	dsc_pkt_head_t		head;
	dsc_auth_t			auth;
	char				script[DSC_MAX_SCRIPT_LEN];
} __attribute__((packed)) dsc_pkt_cfg_scripte_t;

typedef struct {
	dsc_pkt_head_t		head;
	dsc_auth_t			auth;
	unsigned int		size;
	char 				filename[FIRMWARE_IMAGE_NAME_MAX];
} __attribute__((packed)) dsc_pkt_cfg_upgrade_t;

typedef struct {
	dsc_pkt_head_t		head;
	dsc_auth_t			auth;
	unsigned int		size;
} __attribute__((packed)) dsc_pkt_cfg_config_t;

typedef struct {
	dsc_pkt_head_t		head;
	dsc_auth_t			auth;
	unsigned char		flag;
} __attribute__((packed)) dsc_pkt_cfg_save_t;

typedef struct {
	dsc_pkt_head_t		head;
	dsc_auth_t			auth;
	unsigned char		flag;
} __attribute__((packed)) dsc_pkt_cfg_reset_t;

typedef struct {
	dsc_pkt_head_t		head;
	dsc_auth_t			auth;
} __attribute__((packed)) dsc_pkt_reboot_t;

typedef struct {
	dsc_pkt_head_t		head;
	dsc_auth_t			auth;
	unsigned char		flag;
} __attribute__((packed)) dsc_pkt_ucast_t;

typedef struct {
	dsc_pkt_head_t		head;
	char				configured_mac[DSC_MAX_MAC_LEN];
	unsigned int		result;
} __attribute__((packed)) dsc_pkt_cfg_result_t;

typedef struct {
	dsc_payload_type_t	type;
	unsigned int		len;
} __attribute__((packed)) dsc_tlv_head_t;

typedef struct {
	dsc_tlv_head_t		tlv_head;
	unsigned int		vlan_id;
	int					method;
	unsigned int		ipaddr;
	unsigned int		netmask;
} __attribute__((packed)) dsc_pkt_tlv_ip_t;

typedef struct {
	dsc_tlv_head_t		tlv_head;
	unsigned int		gw;
} __attribute__((packed)) dsc_pkt_tlv_gw_t;

#endif	/*	_DSC_PACKET_H_	*/
