/***********************************************************
 *
 * Copyright (c) 2017 Leonton.Co.Ltd All Rights Reserved.
 *
 * Discovery Module
 *
 ***********************************************************/

/* ================================================================= *
 * Trace definitions
 * ================================================================= */
#include "vtss_module_id.h"
#include "vtss_trace_lvl_api.h"
#include "vtss_trace_api.h"

#define VTSS_TRACE_MODULE_ID     VTSS_MODULE_ID_DISCOVERY
#define VTSS_TRACE_GRP_DEFAULT   0
#define VTSS_TRACE_GRP_CRIT      1
#define VTSS_TRACE_GRP_ICLI      2
#define VTSS_TRACE_GRP_CNT       3

