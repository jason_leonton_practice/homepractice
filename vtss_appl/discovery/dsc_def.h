/*************************************************************************************************
 * Copyright (C) Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: Leonton Discovery Server Definition Header
 *
 *************************************************************************************************/
#ifndef _DSC_DEF_H_
#define _DSC_DEF_H_

#include "stdio.h"

#include "dsc_packet.h"

#define DSC_SUCCESS							1
#define DSC_FAILURE							0

#define DSC_MAX_BUFF_LEN					(2048)
#define DSC_SOCK_SELECT_TIMEOUT				(1000)
#define DSC_DELAY							(500)
#define DSC_MAX_STR_LEN						(50)

typedef enum {
	DSC_ERR_NONE = 0,
	DSC_ERR_INTNL_PARAM,
	DSC_ERR_INTNL_MAGIC,
	DSC_ERR_HEAD_VERSION,
	DSC_ERR_PROBE_MISMATCH,
	DSC_ERR_TYPE,
	DSC_ERR_CFA,
	DSC_ERR_NETIP,
	DSC_ERR_PKT_RECV,
	DSC_ERR_PKT_TIMEOUT,
	DSC_ERR_PKT_SELECT,
	DSC_ERR_PKT_SIZE,
	DSC_ERR_PKT_SEND,
	DSC_ERR_AUTH_TARGET,
	DSC_ERR_AUTH_LVL,
	DSC_ERR_AUTH,
	DSC_ERR_CONFIG,
	DSC_ERR_FILE_OPEN,
	DSC_ERR_FILE_IO,
	DSC_ERR_FILE_RECV,
	DSC_ERR_CONFIG_OVERSIZE,
	DSC_ERR_FW_OVERSIZE,
	DSC_ERR_CONFIG_IMPORT,
	DSC_ERR_CONFIG_EXPORT,
	DSC_ERR_NOT_SUPPORT,
	DSC_ERR_UCAST_FAIL,
	DSC_ERR_CONFIG_SAVE,
	DSC_ERR_UKNOWN
} dsc_err_t;

typedef struct {
	int						sock_fd;
	struct sockaddr			*peer_addr;
	char					*sock_buff;
	int						packet_len;
	char					*data_buff;
	int						data_len;
	dsc_type_t				type;
	dsc_err_t				error;
	char					reboot;
} dsc_packet_info_t;


#endif	/*	_DSC_DEF_H_	*/
