 /*************************************************************************************************
 * Copyright (C) Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: Leonton Discovery Packet Library
 *
 *************************************************************************************************/

#include "discovery_api.h"
#include "dsc_def.h"


/**************************************************************************************************
 * PRIVATE FUNCTIONS
 *************************************************************************************************/

/**************************************************************************************************
 * PUBLIC FUNCTIONS
 *************************************************************************************************/
int dsc_assm_head(dsc_pkt_head_t *head, dsc_type_t type)
{
	if(!head) {
		return DSC_FAILURE;
	}

	head->magic[0] = DSC_PKT_HEAD_MAGIC0;
	head->magic[1] = DSC_PKT_HEAD_MAGIC1;
	head->version = DSC_PKT_HEAD_VERSION;
	head->type = type;

	return DSC_SUCCESS;
}

int dsc_parse_head(dsc_pkt_head_t *head, dsc_type_t *type, dsc_err_t *error, int *packet_len)
{
	if(!head || !type || !error) {
		if(error) {
			*error = DSC_ERR_INTNL_PARAM;
		}
		return DSC_FAILURE;
	}

	if((head->magic[0] != DSC_PKT_HEAD_MAGIC0) || (head->magic[1] != DSC_PKT_HEAD_MAGIC1)) {
		*error = DSC_ERR_INTNL_MAGIC;
		return DSC_FAILURE;
	}

	if(head->version < DSC_PKT_HEAD_VERSION) {
		*error = DSC_ERR_HEAD_VERSION;
		return DSC_FAILURE;
	}

	if(head->type >= DSC_TYPE_MAX) {
		*error = DSC_ERR_TYPE;
		return DSC_FAILURE;
	}
	switch(head->type) {
		case DSC_TYPE_PROBE:
			*packet_len = sizeof(dsc_pkt_probe_t);
			break;
		case DSC_TYPE_PROBEMATCH:
			*packet_len = sizeof(dsc_pkt_probematch_t);
			break;
		case DSC_TYPE_CFG_IPADDR:
			*packet_len = sizeof(dsc_pkt_cfg_ipaddr_t);
			break;
		case DSC_TYPE_CFG_GATEWAY:
			*packet_len = sizeof(dsc_pkt_cfg_gateway_t);
			break;
		case DSC_TYPE_CFG_MPFLAG:
			*packet_len = sizeof(dsc_pkt_cfg_mpflag_t);
			break;
		case DSC_TYPE_CFG_CHGPASSWD:
			*packet_len = sizeof(dsc_pkt_cfg_passwd_t);
			break;
		case DSC_TYPE_CFG_SCRIPT:
			*packet_len = sizeof(dsc_pkt_cfg_scripte_t);
			break;
		case DSC_TYPE_CFG_SINGLE_UPGRADE:
		case DSC_TYPE_CFG_DUAL_UPGRADE:
			*packet_len = sizeof(dsc_pkt_cfg_upgrade_t);
			break;
		case DSC_TYPE_CFG_IMPORT:
		case DSC_TYPE_CFG_EXPORT:
			*packet_len = sizeof(dsc_pkt_cfg_config_t);
			break;
		case DSC_TYPE_DO_SAVE_CONFIG:
			*packet_len = sizeof(dsc_pkt_cfg_save_t);
			break;
		case DSC_TYPE_DO_RESET_DEFAULT:
			*packet_len = sizeof(dsc_pkt_cfg_reset_t);
			break;
		case DSC_TYPE_DO_RESTART:
			*packet_len = sizeof(dsc_pkt_reboot_t);
			break;
		case DSC_TYPE_UCAST:
			*packet_len = sizeof(dsc_pkt_ucast_t);
			break;
		case DSC_TYPE_RESULT:
			*packet_len = sizeof(dsc_pkt_cfg_result_t);
			break;
		default:
			return DSC_FAILURE;
	}

	*type = (dsc_type_t)head->type;

	return DSC_SUCCESS;
}