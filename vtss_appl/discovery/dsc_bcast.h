/*************************************************************************************************
 * Copyright (C) Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: Leonton Discovery Broadcast Server Header
 *
 *************************************************************************************************/
#ifndef _DSC_BCAST_H_
#define _DSC_BCAST_H_

#define DSC_BCAST_SRV_LISTEN_PORT			(4010)

void dsc_bcast_handle(int sock_fd);

void dsc_bcast_srv_close(int sock_fd);

int dsc_bcast_srv_open(int *sock_fd);


#endif	/*	_DSC_BCAST_H_	*/

