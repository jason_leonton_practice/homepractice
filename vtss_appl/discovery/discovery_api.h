/***********************************************************
 *
 * Copyright (c) 2017 Leonton.Co.Ltd All Rights Reserved.
 *
 * Discovery Module
 *
 ***********************************************************/

#ifndef _DISCOVERY_API_H_
#define _DISCOVERY_API_H_

#include "conf_api.h"
//#include "lib_lntn_cfg.h"

#include "dsc_def.h"

#define SOCK_HANDLE_NUM			3
#define SOCK_SELECT_TIMEOUT     500

/***************************************************************************/
/* Struct                                                       */
/***************************************************************************/
typedef enum {
	BCAST_SERVER_FD = 0, /* broadcast server/client sockfd */
	UCAST_SERVER_FD = 1, /* unicast server sockfd */
	UCAST_CLIENT_FD = 2, /* unicast client sockfd */
} socket_fd_t;

/***************************************************************************/
/* Function                                                                */
/***************************************************************************/

/*	discovery.c	*/
vtss_rc discovery_init(vtss_init_data_t *data);
int dsc_send_ucast_open_event(void);
int dsc_send_ucast_close_event(void);
void dsc_sel_rm_fd(int sfd);
void *dsc_get_device_info(void);

/* dsc_packet.c */
int dsc_parse_head(dsc_pkt_head_t *head, dsc_type_t *type, dsc_err_t *error, int *packet_len);
int dsc_assm_head(dsc_pkt_head_t *head, dsc_type_t type);

/*	dsc_dispather.c	*/
int dsc_action_dispatcher(dsc_packet_info_t *packet_info);


#endif /* _DISCOVERY_API_H_ */
