/*************************************************************************************************
 * Copyright (C) Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: Leonton Discovery Unicast Server
 *
 *************************************************************************************************/
#include "main.h"
#include <sys/socket.h>
#include <netinet/in.h>

#include "discovery_api.h"
#include "dsc_def.h"
#include "dsc_ucast.h"
#include "dsc_cdnt.h"

/**************************************************************************************************
 * PRIVATE DEFINITION
 *************************************************************************************************/
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_DISCOVERY

#define DSC_UCAST_SRV_ACCEPT_NUM			(1)
#define DSC_UCAST_SELECT_TIMEOUT			(1000)

/**************************************************************************************************
 * PRIVATE FUNCTIONS
 *************************************************************************************************/
static int dsc_ucast_select(dsc_packet_info_t *pkt_info)
{
	int						ret = DSC_FAILURE;
	int						events = 0;
	fd_set					rdset;
	struct timeval			tv;

	FD_ZERO(&rdset);
	FD_SET(pkt_info->sock_fd, &rdset);
	tv.tv_sec = (DSC_UCAST_SELECT_TIMEOUT) / 1000;
	tv.tv_usec = (DSC_UCAST_SELECT_TIMEOUT % 1000) * 1000;

	events = select(pkt_info->sock_fd + 1, &rdset, NULL, NULL, &tv);

	if(events > 0) {
		ret = DSC_SUCCESS;
	} else if(events == 0) {
		pkt_info->error = DSC_ERR_PKT_TIMEOUT;
	} else {
		pkt_info->error = DSC_ERR_PKT_SELECT;
	}

	return ret;
}

static int dsc_ucast_recv_file(dsc_packet_info_t *pkt_info)
{
	int			len = 0;
	int			readed = 0;

	while(pkt_info->packet_len > 0) {
		if(dsc_ucast_select(pkt_info)) {
			len = (pkt_info->packet_len > DSC_MAX_BUFF_LEN) ? DSC_MAX_BUFF_LEN : pkt_info->packet_len;
			if((readed = read(pkt_info->sock_fd, pkt_info->sock_buff, len)) <= 0) {
				pkt_info->error = DSC_ERR_PKT_RECV;
				pkt_info->packet_len = readed;
				return DSC_FAILURE;
			}

			if(!dsc_cdnt_write_buf(pkt_info, (unsigned int)readed)) {
				return DSC_FAILURE;
			}
		} else {
			return DSC_FAILURE;
		}
	}

	pkt_info->data_buff -= pkt_info->data_len;  //recover data buff offest

	return DSC_SUCCESS;
}

static int dsc_ucast_recv(dsc_packet_info_t *pkt_info)
{
	int				readed = 0;
	int				total_len = 0;

	while(total_len != sizeof(dsc_pkt_head_t)) {
		if(dsc_ucast_select(pkt_info)) {
			if((readed = read(pkt_info->sock_fd, pkt_info->sock_buff + total_len, sizeof(dsc_pkt_head_t) - total_len)) <= 0) {
				pkt_info->error = DSC_ERR_PKT_RECV;
				pkt_info->packet_len = readed;
				return DSC_FAILURE;
			}
			total_len += readed;
		} else {
			return DSC_FAILURE;
		}
	}

	if(!dsc_parse_head((dsc_pkt_head_t *)pkt_info->sock_buff, &(pkt_info->type), &(pkt_info->error), &(pkt_info->packet_len))) {
		VTSS_OS_MSLEEP(DSC_DELAY);
		return DSC_FAILURE;
	}

	while(total_len != (int)(pkt_info->packet_len)) {
		if(dsc_ucast_select(pkt_info)) {
			if((readed = read(pkt_info->sock_fd, pkt_info->sock_buff + total_len, pkt_info->packet_len - total_len)) <= 0) {
				pkt_info->error = DSC_ERR_PKT_RECV;
				pkt_info->packet_len = readed;
				return DSC_FAILURE;
			}
			total_len += readed;
		} else {
			return DSC_FAILURE;
		}
	}

	return DSC_SUCCESS;
}

static int dsc_ucast_send(dsc_packet_info_t *pkt_info)
{
	int				len = 0;
	int				written = 0;
	int				offset = 0;
    int             data_len=0;

	T_D("packet_len: %d", pkt_info->packet_len);
	while(pkt_info->packet_len > 0) {
		if((written = write(pkt_info->sock_fd, pkt_info->sock_buff + offset, pkt_info->packet_len)) <= 0) {
			pkt_info->error = DSC_ERR_PKT_SEND;
			return DSC_FAILURE;
		}
		offset += written;
		pkt_info->packet_len -= written;
	}

	if(pkt_info->type == DSC_TYPE_DO_EXPORT) {
        data_len=pkt_info->data_len; //save data_len to recover data buff
		while(pkt_info->data_len > 0) {
			if(!dsc_cdnt_read_buf(pkt_info, (unsigned int *)&len)) {
				return DSC_FAILURE;
			}
            offset = 0;
			pkt_info->data_len -= len;
			while(len > 0) {
				if((written = write(pkt_info->sock_fd, pkt_info->sock_buff + offset, len)) <= 0) {
					pkt_info->error = DSC_ERR_PKT_SEND;
					return DSC_FAILURE;
				}
				offset += written;
				len -= written;
			}
		}

		pkt_info->data_buff -= data_len;  //Recover data buff offest

		return dsc_action_dispatcher(pkt_info);
	}

    if(pkt_info->type == DSC_TYPE_CFG_IPADDR) {
        pkt_info->type =DSC_TYPE_DO_IPADDR;
        T_D("change DSC_TYPE_CFG_IPADDR -> DSC_TYPE_DO_IPADDR");
        dsc_action_dispatcher(pkt_info);
    }

	if(pkt_info->reboot) {
		dsc_cdnt_reboot();
	}

	return DSC_SUCCESS;
}

/**************************************************************************************************
 * PUBLIC FUNCTIONS
 *************************************************************************************************/
int dsc_ucast_handle(int sock_fd)
{
	dsc_packet_info_t			pkt_info;
	char						sock_buff[DSC_MAX_BUFF_LEN];

	memset(&pkt_info, 0, sizeof(dsc_packet_info_t));
	memset(sock_buff, 0, DSC_MAX_BUFF_LEN);

	pkt_info.sock_fd = sock_fd;
	pkt_info.sock_buff = sock_buff;

	if(!dsc_ucast_recv(&pkt_info)) {
		T_D("dsc_ucast_recv failed! [error: %d, len: %d]", pkt_info.error, pkt_info.packet_len);
		return DSC_FAILURE;
	}

	if(!dsc_action_dispatcher(&pkt_info)) {
		T_D("dsc_action_dispatcher failed! [error: %d]", pkt_info.error);
		return DSC_FAILURE;
	} else {
		if(pkt_info.type == DSC_TYPE_CFG_SINGLE_UPGRADE || pkt_info.type == DSC_TYPE_CFG_DUAL_UPGRADE) {
			if(pkt_info.error == DSC_ERR_NONE) {
				if(!dsc_ucast_recv_file(&pkt_info)) {
					T_D("dsc_ucast_recv_file failed! [error: %d, len: %d]", pkt_info.error, pkt_info.packet_len);
					dsc_send_ucast_close_event();
					VTSS_FREE(pkt_info.data_buff);
					pkt_info.data_buff = NULL;
					return DSC_FAILURE;
				}

				if(pkt_info.type == DSC_TYPE_CFG_SINGLE_UPGRADE) {
					pkt_info.type = DSC_TYPE_DO_SINGLE_UPGRADE;
				} else {
					pkt_info.type = DSC_TYPE_DO_DUAL_UPGRADE;
				}

				if(!dsc_action_dispatcher(&pkt_info)) {
					T_D("Redo dsc_action_dispatcher failed! [error: %d]", pkt_info.error);
					dsc_send_ucast_close_event();
					VTSS_FREE(pkt_info.data_buff);
					pkt_info.data_buff = NULL;
					return DSC_FAILURE;
				}
			}
		} else if(pkt_info.type == DSC_TYPE_CFG_IMPORT) {
			if(pkt_info.error == DSC_ERR_NONE) {
				if(!dsc_ucast_recv_file(&pkt_info)) {
					T_D("dsc_ucast_recv_file failed! [error: %d, len: %d]", pkt_info.error, pkt_info.packet_len);
					dsc_send_ucast_close_event();
					VTSS_FREE(pkt_info.data_buff);
					pkt_info.data_buff = NULL;
					return DSC_FAILURE;
				}
				pkt_info.type = DSC_TYPE_DO_IMPORT;
				if(!dsc_action_dispatcher(&pkt_info)) {
					T_D("Redo dsc_action_dispatcher failed! [error: %d]", pkt_info.error);
					dsc_send_ucast_close_event();
					VTSS_FREE(pkt_info.data_buff);
					pkt_info.data_buff = NULL;
					return DSC_FAILURE;
				}
			}
			VTSS_FREE(pkt_info.data_buff);
			pkt_info.data_buff = NULL;
		} else if(pkt_info.type == DSC_TYPE_CFG_EXPORT) {
			if(pkt_info.data_len > 0) {
				pkt_info.type = DSC_TYPE_DO_EXPORT;
			}
		} else if(pkt_info.type == DSC_TYPE_PROBEMATCH) {
			return DSC_FAILURE;
		}
	}

	if(!dsc_ucast_send(&pkt_info)) {
		T_D("dsc_ucast_send failed! [error: %d, len: %d]", pkt_info.error, pkt_info.packet_len);
	}

    return DSC_SUCCESS;
}

int dsc_ucast_accept(int srv_fd, int *cln_fd)
{
	int					sfd = 0;

	if((sfd = vtss_accept(srv_fd, NULL, NULL)) < 0) {
		return DSC_FAILURE;
	}

	*cln_fd = sfd;

	return DSC_SUCCESS;
}

void dsc_ucast_socket_close(int sock_fd)
{
	dsc_sel_rm_fd(sock_fd);
}

int dsc_ucast_srv_open(int *sock_fd)
{
	int					sfd = 0;
	struct sockaddr_in		ucast_addr;
	int					opt = 1;

	if(!sock_fd) {
		return DSC_FAILURE;
	}

	if((sfd = vtss_socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		T_D("Socket open failed!");
		return DSC_FAILURE;
	}

	memset(&ucast_addr, 0, sizeof(struct sockaddr_in));
	ucast_addr.sin_family = AF_INET;
	ucast_addr.sin_addr.s_addr = INADDR_ANY;
	ucast_addr.sin_port = ntohs(DSC_UCAST_SRV_LISTEN_PORT);

	if(setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int)) < 0) {
		T_D("Set socket failed for SO_REUSEADDR!");
		dsc_ucast_socket_close(sfd);
		return DSC_FAILURE;
	}

	if(bind(sfd, (struct sockaddr *)(&ucast_addr), sizeof(ucast_addr)) < 0) {
		T_D("SBinding of socket server failed!");
		dsc_ucast_socket_close(sfd);
		return DSC_FAILURE;
	}

	if(listen(sfd, DSC_UCAST_SRV_ACCEPT_NUM) < 0) {
		T_D("Unicast server listen failed!");
		dsc_ucast_socket_close(sfd);
		return DSC_FAILURE;
	}

	*sock_fd = sfd;

	return DSC_SUCCESS;
}



