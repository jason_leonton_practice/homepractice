/*************************************************************************************************
 * Copyright (C) Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: Leonton Discovery Action Dispatcher
 *
 *************************************************************************************************/
 
#include "discovery_api.h"
#include "dsc_def.h"
#include "dsc_cdnt.h"
 
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_DISCOVERY

/**************************************************************************************************
 * PRIVATE FUNCTIONS
 *************************************************************************************************/
static int dsc_action_probematch(char *buff, int *packet_len, unsigned char *session, dsc_err_t *error)
{
	dsc_pkt_probematch_t		*probematch = (dsc_pkt_probematch_t *)buff;
	dsc_device_info_t			*dev_info = (dsc_device_info_t *)dsc_get_device_info();
	dsc_pkt_tlv_ip_t			*tlv_ip_pkt = NULL;
	dsc_pkt_tlv_gw_t			*tlv_gw_pkt = NULL;
	char						str[DSC_MAX_STR_LEN];
	char						*buff_offset = buff + sizeof(dsc_pkt_probematch_t);
	unsigned int				payload_len = 0;
	//IP
	vtss_if_id_vlan_t			vlan_id = VTSS_VID_NULL;
	mesa_ipv4_t					ipaddr_addr = 0;
	mesa_prefix_size_t			ipaddr_netmast = 0;
	int							ipaddr_mod = 0;
	//GATEWAY
	mesa_ipv4_t					mask;
	mesa_routing_entry_t		*routes = NULL;
	int gateway_cnt = 0;
	int gateway_index = 0;
	int ct = 0;


	memset(str, 0, DSC_MAX_STR_LEN);
	memset(buff, 0, DSC_MAX_BUFF_LEN);

	dsc_assm_head(&probematch->head, DSC_TYPE_PROBEMATCH);
	memcpy(probematch->session, session, DSC_MAX_SESSION_LEN);
	snprintf(probematch->model_name, DSC_MAX_MODEL_NAME_LEN, dev_info->model_name);

	snprintf(probematch->vendor, DSC_MAX_VENDOR_NAME_LEN, dev_info->vendor);

	dsc_cdnt_get_sys_name(str);
	snprintf(probematch->name, DSC_MAX_NAME_LEN, str);

	snprintf(probematch->fw_version, DSC_MAX_FWVER_LEN, "%s %s", dev_info->version, dev_info->buildtime);

	snprintf(probematch->sn, DSC_MAX_SN_STR_LEN, dev_info->serial_no);

	dsc_cdnt_get_sys_location(str);
	snprintf(probematch->location, DSC_MAX_LOCATION_LEN, str);

	memcpy(probematch->mac, dev_info->mac, DSC_MAX_MAC_LEN);

	probematch->mpflag = dev_info->mpflag;
	//IP
	while(dsc_cdnt_get_ipaddr_info(&vlan_id, &ipaddr_mod, &ipaddr_addr, &ipaddr_netmast, error)) {
		tlv_ip_pkt = (dsc_pkt_tlv_ip_t *)buff_offset;
		tlv_ip_pkt->tlv_head.type = DSC_PLDTYPE_IPADDR;
		tlv_ip_pkt->tlv_head.len = DSC_IP_TLV_PKT_LEN;
		tlv_ip_pkt->vlan_id = (unsigned int)vlan_id;
		tlv_ip_pkt->method = ipaddr_mod;
		tlv_ip_pkt->ipaddr = (unsigned int)ipaddr_addr;

		vtss_conv_prefix_to_ipv4mask(ipaddr_netmast, &mask);
		tlv_ip_pkt->netmask = mask;

		buff_offset += sizeof(dsc_pkt_tlv_ip_t);
		payload_len += sizeof(dsc_pkt_tlv_ip_t);
	}

	//GATEWAY
	if ((VTSS_CALLOC_CAST(routes, (ct = IP_MAX_ROUTES), sizeof(*routes))) != NULL) {
		if(dsc_cdnt_get_gateway(ct, routes, &gateway_cnt, error)) {
			for(gateway_index = 0; routes && gateway_index < gateway_cnt; gateway_index++) {

				mesa_routing_entry_t const *rt = &routes[gateway_index];
				if(rt->type == MESA_ROUTING_ENTRY_TYPE_IPV4_UC) {
					tlv_gw_pkt = (dsc_pkt_tlv_gw_t *)buff_offset;
					tlv_gw_pkt->tlv_head.type = DSC_PLDTYPE_GATEWAY;
					tlv_gw_pkt->tlv_head.len = DSC_GW_TLV_PKT_LEN;
					tlv_gw_pkt->gw = rt->route.ipv4_uc.destination;

					buff_offset += sizeof(dsc_pkt_tlv_gw_t);
					payload_len += sizeof(dsc_pkt_tlv_gw_t);
				}
			}

			VTSS_FREE(routes);
			routes = NULL;
		}
	}

	probematch->payload_len = payload_len;
	*error = DSC_ERR_NONE;
	*packet_len = sizeof(dsc_pkt_probematch_t) + probematch->payload_len;

	return DSC_SUCCESS;
}

static int dsc_action_result(char *buff, int *packet_len, unsigned int result)
{
	dsc_pkt_cfg_result_t		*cfg_result = (dsc_pkt_cfg_result_t *)buff;
	dsc_device_info_t			*dev_info = (dsc_device_info_t *)dsc_get_device_info();

	memset(buff, 0, DSC_MAX_BUFF_LEN);

	dsc_assm_head(&(cfg_result->head), DSC_TYPE_RESULT);

    //MAC verify fail. Do not reply.
	if(result == DSC_ERR_AUTH_TARGET) {
	    return DSC_FAILURE;
	}

	cfg_result->result = result;

	memcpy(&(cfg_result->configured_mac), dev_info->mac, DSC_MAX_MAC_LEN);

	*packet_len = sizeof(dsc_pkt_cfg_result_t);

	return DSC_SUCCESS;
}

static int dsc_authentication(dsc_auth_t *auth, dsc_err_t *error)
{
	dsc_device_info_t		*dev_info = (dsc_device_info_t*)dsc_get_device_info();

	if(memcmp(auth->target_mac, dev_info->mac, DSC_MAX_MAC_LEN) != 0) {
		*error = DSC_ERR_AUTH_TARGET;
		return DSC_FAILURE;
	}

	return dsc_cdnt_auth(auth->id, auth->passwd, error);
}

static int dsc_action_probe(char *buff, int *packet_len, dsc_err_t *error)
{
	dsc_pkt_probe_t			*probe = (dsc_pkt_probe_t *)buff;
	unsigned char			session[DSC_MAX_SESSION_LEN];

	memset(session, 0, DSC_MAX_SESSION_LEN);

	if(!(probe->flag & DSC_PROBE_L2SWITCH)) {
		*error = DSC_ERR_PROBE_MISMATCH;
		return DSC_FAILURE;
	}

	memcpy(session, probe->session, DSC_MAX_SESSION_LEN);

	return dsc_action_probematch(buff, packet_len, session, error);
}

static int dsc_action_conf_ipaddr(dsc_type_t pkt_type,char *buff, int *packet_len, dsc_err_t *error)
{
    dsc_pkt_cfg_ipaddr_t		*cfg_ipaddr = (dsc_pkt_cfg_ipaddr_t *)buff;
    static dsc_pkt_cfg_ipaddr_t do_ipaddr;

    if(pkt_type==DSC_TYPE_CFG_IPADDR){
        T_D("DSC_TYPE_CFG_IPADDR");
    if(dsc_authentication(&(cfg_ipaddr->auth), error)) {
            do_ipaddr.vlan_id=cfg_ipaddr->vlan_id;
            do_ipaddr.method=cfg_ipaddr->method;
            do_ipaddr.ipaddr=cfg_ipaddr->ipaddr;
            do_ipaddr.netmask=cfg_ipaddr->netmask;
        }
    }else if(pkt_type==DSC_TYPE_DO_IPADDR){
        T_D("DSC_TYPE_DO_IPADDR");

        if(dsc_cdnt_cfg_ipaddr(do_ipaddr.vlan_id,do_ipaddr.method, do_ipaddr.ipaddr, do_ipaddr.netmask, error)) {
            *error = DSC_ERR_NONE;
            memset(&do_ipaddr, 0, sizeof(do_ipaddr));
            return DSC_SUCCESS;
        }else{
            return DSC_FAILURE;
        }
    }else{
        T_D("pkt_type=%d Failure",pkt_type);
        return DSC_FAILURE;
    }

    return dsc_action_result(buff, packet_len, *error);
}

static int dsc_action_conf_gateway(char *buff, int *packet_len, dsc_err_t *error)
{
	dsc_pkt_cfg_gateway_t		*cfg_gw = (dsc_pkt_cfg_gateway_t *)buff;

	if(dsc_authentication(&(cfg_gw->auth), error)) {
		if(dsc_cdnt_set_gateway(cfg_gw->gateway, error)) {
			*error = DSC_ERR_NONE;
		}
	}

	return dsc_action_result(buff, packet_len, *error);
}

static int dsc_action_conf_mpflag(char *buff, int *packet_len, dsc_err_t *error)
{
	dsc_pkt_cfg_mpflag_t		*cfg_mpflag = (dsc_pkt_cfg_mpflag_t *)buff;

	if(dsc_authentication(&(cfg_mpflag->auth), error)) {
		if(dsc_cdnt_set_mpflag(cfg_mpflag->flag, error)) {
			*error = DSC_ERR_NONE;
		}
	}

	return dsc_action_result(buff, packet_len, *error);
}

static int dsc_action_chg_passwd(char *buff, int *packet_len, dsc_err_t *error)
{
	dsc_pkt_cfg_passwd_t		*cfg_passwd = (dsc_pkt_cfg_passwd_t *)buff;

	if(dsc_authentication(&(cfg_passwd->auth), error)) {
		if(dsc_cdnt_chg_passwd(cfg_passwd->auth.id, cfg_passwd->passwd, error)) {
			*error = DSC_ERR_NONE;
		}
	}

	return dsc_action_result(buff, packet_len, *error);
}

static int dsc_action_exec_script(char *buff, int *packet_len, dsc_err_t *error)
{
	dsc_pkt_cfg_scripte_t		*cfg_script = (dsc_pkt_cfg_scripte_t *)buff;

	if(dsc_authentication(&(cfg_script->auth), error)) {
		*error = DSC_ERR_NOT_SUPPORT;
	}

	return dsc_action_result(buff, packet_len, *error);
}

static int dsc_action_fwupg(dsc_packet_info_t *pkt_info)
{
	dsc_pkt_cfg_upgrade_t		*cfg_upg = (dsc_pkt_cfg_upgrade_t *)pkt_info->sock_buff;

	if(dsc_authentication(&(cfg_upg->auth), &(pkt_info->error))) {
		if(cfg_upg->size <= DSC_MAX_UPGRADE_ROM_SIZE) {
			pkt_info->packet_len = cfg_upg->size;
			pkt_info->data_len = cfg_upg->size;
			if(dsc_cdnt_create_buf(pkt_info)) {
				dsc_cdnt_fwfilename_set(cfg_upg->filename);
				pkt_info->error = DSC_ERR_NONE;
				return DSC_SUCCESS;
			}
		} else {
			pkt_info->error = DSC_ERR_FW_OVERSIZE;
		}
	}

	return dsc_action_result(pkt_info->sock_buff, &(pkt_info->packet_len), pkt_info->error);
}

static int dsc_action_do_fwupg(dsc_packet_info_t *pkt_info)
{

	if(dsc_cdnt_do_fwupg(pkt_info) == DSC_SUCCESS) {
		pkt_info->error = DSC_ERR_NONE;
	}

	return dsc_action_result(pkt_info->sock_buff, &(pkt_info->packet_len), pkt_info->error);
}

static int dsc_action_conf_imp(dsc_packet_info_t *pkt_info)
{
	dsc_pkt_cfg_config_t		*cfg_conf = (dsc_pkt_cfg_config_t *)pkt_info->sock_buff;

	if(dsc_authentication(&(cfg_conf->auth), &(pkt_info->error))) {
		if(cfg_conf->size <= DSC_MAX_CONFIG_BUFFER_SIZE) {
			pkt_info->data_len = cfg_conf->size;
			pkt_info->packet_len = cfg_conf->size;

			if(dsc_cdnt_create_buf(pkt_info)) {
				pkt_info->error = DSC_ERR_NONE;
				return DSC_SUCCESS;
			}
		} else {
			pkt_info->error = DSC_ERR_CONFIG_OVERSIZE;
		}
	}

	return dsc_action_result(pkt_info->sock_buff, &(pkt_info->packet_len), pkt_info->error);
}

static int dsc_action_do_imp(dsc_packet_info_t *pkt_info)
{
	if(pkt_info->data_len == strlen(pkt_info->data_buff)) {
		if(dsc_cdnt_do_cfg_imp(pkt_info)) {
			pkt_info->error = DSC_ERR_NONE;
		}
	} else {
		pkt_info->error = DSC_ERR_CONFIG_OVERSIZE;
	}

	return dsc_action_result(pkt_info->sock_buff, &(pkt_info->packet_len), pkt_info->error);
}

static int dsc_action_conf_exp(dsc_packet_info_t *pkt_info)
{
	dsc_pkt_cfg_config_t		*cfg_conf = (dsc_pkt_cfg_config_t *)pkt_info->sock_buff;

	if(dsc_authentication(&(cfg_conf->auth), &(pkt_info->error))) {
		dsc_cdnt_do_cfg_exp(pkt_info);
	}

	return dsc_action_result(pkt_info->sock_buff, &(pkt_info->packet_len), pkt_info->data_len); //sent export data length
}

static int dsc_action_do_exp(dsc_packet_info_t *pkt_info)
{
	VTSS_FREE(pkt_info->data_buff);
	pkt_info->data_buff = NULL;

	return DSC_SUCCESS;
}

static int dsc_action_do_cfgsave(dsc_packet_info_t *pkt_info)
{
	dsc_pkt_cfg_save_t		*cfg_conf = (dsc_pkt_cfg_save_t *)pkt_info->sock_buff;

	if(dsc_authentication(&(cfg_conf->auth), &(pkt_info->error))) {
		if(dsc_cdnt_do_cfg_save(cfg_conf->flag)) {
			pkt_info->error = DSC_ERR_NONE;
		}else{
			pkt_info->error = DSC_ERR_CONFIG_SAVE;
		}
	}

	return dsc_action_result(pkt_info->sock_buff, &(pkt_info->packet_len), pkt_info->error);
}

static int dsc_action_do_reset2default(dsc_packet_info_t *pkt_info)
{
	dsc_pkt_cfg_reset_t		*cfg_conf = (dsc_pkt_cfg_reset_t *)pkt_info->sock_buff;

	if(dsc_authentication(&(cfg_conf->auth), &(pkt_info->error))) {
		if(dsc_cdnt_do_cfg_reset2default(cfg_conf->flag)) {
			pkt_info->reboot = 1;
			pkt_info->error = DSC_ERR_NONE;
		}else{
			pkt_info->error = DSC_ERR_CONFIG_SAVE;
		}
	}

	return dsc_action_result(pkt_info->sock_buff, &(pkt_info->packet_len), pkt_info->error);
}

static int dsc_action_do_reboot(dsc_packet_info_t *pkt_info)
{
	dsc_pkt_reboot_t		*reboot = (dsc_pkt_reboot_t *)pkt_info->sock_buff;

	if(dsc_authentication(&(reboot->auth), &(pkt_info->error))) {
		pkt_info->reboot = 1;
		pkt_info->error = DSC_ERR_NONE;
	}
	
	return dsc_action_result(pkt_info->sock_buff, &(pkt_info->packet_len), pkt_info->error);
}

static int dsc_action_ucast_ctrl(char *buff, int *packet_len, dsc_err_t *error)
{
	//int					ucast_fd = 0;
	dsc_pkt_ucast_t		*ucast_ctrl = (dsc_pkt_ucast_t *)buff;

	*error = DSC_ERR_NONE;
	if(dsc_authentication(&(ucast_ctrl->auth), error)) {
		switch(ucast_ctrl->flag) {
			case DSC_UCAST_OPEN:
				if(dsc_send_ucast_open_event() != DSC_SUCCESS) {
					*error = DSC_ERR_UCAST_FAIL;
				}
				break;
			case DSC_UCAST_CLOSE:
				if(dsc_send_ucast_close_event() != DSC_SUCCESS) {
					*error = DSC_ERR_UCAST_FAIL;
				}
				break;
		}
	}

	return dsc_action_result(buff, packet_len, *error);
}

static int dsc_print_probematch(char *buff)
{
	return dsc_cdnt_probematch(buff);
}

/**************************************************************************************************
 * PUBLIC FUNCTIONS
 *************************************************************************************************/
int dsc_action_dispatcher(dsc_packet_info_t *pkt_info)
{
	int					ret = DSC_FAILURE;

	if(!pkt_info) {
		pkt_info->error = DSC_ERR_INTNL_PARAM;
		return ret;
	}
	switch(pkt_info->type) {
		case DSC_TYPE_PROBE:
			ret = dsc_action_probe(pkt_info->sock_buff, &(pkt_info->packet_len), &(pkt_info->error));
			break;
		case DSC_TYPE_CFG_IPADDR:
			ret = dsc_action_conf_ipaddr(pkt_info->type, pkt_info->sock_buff, &(pkt_info->packet_len), &(pkt_info->error));
			break;
		case DSC_TYPE_CFG_GATEWAY:
			ret = dsc_action_conf_gateway(pkt_info->sock_buff, &(pkt_info->packet_len), &(pkt_info->error));
			break;
		case DSC_TYPE_CFG_MPFLAG:
			ret = dsc_action_conf_mpflag(pkt_info->sock_buff, &(pkt_info->packet_len), &(pkt_info->error));
			break;
		case DSC_TYPE_CFG_CHGPASSWD:
			ret = dsc_action_chg_passwd(pkt_info->sock_buff, &(pkt_info->packet_len), &(pkt_info->error));
			break;
		case DSC_TYPE_CFG_SCRIPT:
			ret = dsc_action_exec_script(pkt_info->sock_buff, &(pkt_info->packet_len), &(pkt_info->error));
			break;
		case DSC_TYPE_CFG_SINGLE_UPGRADE:
		case DSC_TYPE_CFG_DUAL_UPGRADE:
			ret = dsc_action_fwupg(pkt_info);
			break;
		case DSC_TYPE_DO_SINGLE_UPGRADE:
		case DSC_TYPE_DO_DUAL_UPGRADE:
			ret = dsc_action_do_fwupg(pkt_info);
			break;
		case DSC_TYPE_CFG_IMPORT:
			ret = dsc_action_conf_imp(pkt_info);
			break;
		case DSC_TYPE_DO_IMPORT:
			ret = dsc_action_do_imp(pkt_info);
			break;
		case DSC_TYPE_CFG_EXPORT:
			ret = dsc_action_conf_exp(pkt_info);
			break;
		case DSC_TYPE_DO_EXPORT:
			ret = dsc_action_do_exp(pkt_info);
			break;
		case DSC_TYPE_DO_SAVE_CONFIG:
			ret = dsc_action_do_cfgsave(pkt_info);
			break;
		case DSC_TYPE_DO_RESET_DEFAULT:
			ret = dsc_action_do_reset2default(pkt_info);
			break;
		case DSC_TYPE_DO_RESTART:
			ret = dsc_action_do_reboot(pkt_info);
			break;
		case DSC_TYPE_UCAST:
			ret = dsc_action_ucast_ctrl(pkt_info->sock_buff, &(pkt_info->packet_len), &(pkt_info->error));
			break;
		case DSC_TYPE_PROBEMATCH:
			ret = dsc_print_probematch(pkt_info->sock_buff);
			break;
        case DSC_TYPE_DO_IPADDR:
            ret=dsc_action_conf_ipaddr(pkt_info->type, pkt_info->sock_buff, &(pkt_info->packet_len), &(pkt_info->error));
            break;
		default:
			pkt_info->error = DSC_ERR_TYPE;
	}

	return ret;
}

