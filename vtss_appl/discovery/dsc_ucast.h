/*************************************************************************************************
 * Copyright (C) Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: Leonton Discovery Unicast Server Header
 *
 *************************************************************************************************/
#ifndef _DSC_UCAST_H_
#define _DSC_UCAST_H_

#define DSC_UCAST_SRV_LISTEN_PORT			(4011)

int dsc_ucast_handle(int sock_fd);

int dsc_ucast_accept(int srv_fd, int *cln_fd);

void dsc_ucast_socket_close(int sock_fd);

int dsc_ucast_srv_open(int *sock_fd);

#endif	/*	_DSC_UCAST_H_	*/


