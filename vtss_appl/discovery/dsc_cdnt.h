/*************************************************************************************************
 * Copyright (C) Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: Leonton Discovery Server Coordinator Header
 *
 *************************************************************************************************/
#ifndef _DSC_CDNT_H_
#define _DSC_CDNT_H_

#include "ip_utils.h"

int dsc_cdnt_get_sys_name(char *string);

int dsc_cdnt_get_sys_location(char *string);

int dsc_cdnt_auth(char *username, char *passwd, dsc_err_t *error);

int dsc_cdnt_get_ipaddr_info(vtss_if_id_vlan_t *vlan_id, int *mod, mesa_ipv4_t *addr, mesa_prefix_size_t *netmask, dsc_err_t *error);

int dsc_cdnt_cfg_ipaddr(unsigned int vlan_id, int method, unsigned int ipaddr, unsigned int netmask, dsc_err_t *error);

int dsc_cdnt_get_gateway(const int max, mesa_routing_entry_t *rt, int *const cnt, dsc_err_t *error);

int dsc_cdnt_set_gateway(unsigned int gateway, dsc_err_t *error);

int dsc_cdnt_set_mpflag(unsigned char mpflag, dsc_err_t *error);

int dsc_cdnt_chg_passwd(char *username, char *new_passwd, dsc_err_t *error);

int dsc_cdnt_create_buf(dsc_packet_info_t *pkt_info);

int dsc_cdnt_write_buf(dsc_packet_info_t *pkt_info, unsigned int write_len);

int dsc_cdnt_read_buf(dsc_packet_info_t *pkt_info, unsigned int *read_len);

int dsc_cdnt_fwupg_valid(dsc_packet_info_t *pkt_info);

int dsc_cdnt_do_fwupg(dsc_packet_info_t *pkt_info);

int dsc_cdnt_do_cfg_imp(dsc_packet_info_t *pkt_info);

int dsc_cdnt_do_cfg_exp(dsc_packet_info_t *pkt_info);

int dsc_cdnt_do_cfg_save(unsigned char cfg_save_flag);

int dsc_cdnt_do_cfg_reset2default(unsigned char cfg_reset_flag);

int dsc_cdnt_reboot(void);

void dsc_cdnt_fwfilename_set(char *filename);

void dsc_cdnt_fwfilename_get(char *filename, int filename_length);

int dsc_cdnt_probematch(char *sock_buff);

#endif	/*	_DSC_CDNT_H_	*/



