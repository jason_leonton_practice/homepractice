/*************************************************************************************************
 * Copyright (C) Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: Leonton Discovery Action Coordinator
 *
 *************************************************************************************************/
#include "main.h"
#include "vtss_auth_api.h"
#include "discovery_api.h"
#include "sysutil_api.h"
#include "vtss_users_api.h"
#include "firmware_api.h"
#include "cli_io_api.h"
#include "icfg_api.h"
#include "icfg.h"
#include "ip_api.h"
#include "firmware.h"
#include "firmware_api.h"

#include "dsc_def.h"
#include "dsc_cdnt.h"


/***************************************************************************/
/* Pre-defined                                                             */
/***************************************************************************/
typedef struct {
	int						method;
	vtss_ifindex_t			ifidx;
	vtss_if_id_vlan_t		vlan_id;
	mesa_ipv4_t				ipaddr;
	mesa_prefix_size_t		netmask;
} mgtnet4_info_t;

/***************************************************************************/
/* Global variables                                                        */
/***************************************************************************/

static char fw_filename[FIRMWARE_IMAGE_NAME_MAX];

/**************************************************************************************************
 * PRIVATE DEFINITION
 *************************************************************************************************/
static int set_ipv4_conf(mgtnet4_info_t *ipv4_info)
{
	vtss_ifindex_t	ifidx;
	vtss_appl_ip_ipv4_conf_t ip_conf;

	memset(&ip_conf, 0, sizeof(vtss_appl_ip_ipv4_conf_t));

	ip_conf.active = true; //IPv4

	if(ipv4_info->method == DSC_MGTNET_IPMOD_DHCP) {
		ip_conf.dhcpc = TRUE;
	} else {
		// Normal address configuration
		if(vtss_conv_ipv4mask_to_prefix(ipv4_info->netmask, &(ip_conf.network.prefix_size)) != VTSS_RC_OK) {
			return DSC_FAILURE;
		}

		ip_conf.network.address = ipv4_info->ipaddr;
	}

	// Convert vlan into a interface index
	if (vtss_ifindex_from_vlan(ipv4_info->vlan_id, &ifidx) != VTSS_RC_OK) {
		return DSC_FAILURE;
	}

	/* check the IP interface does not exist? */
	if (vtss_appl_ip_if_conf_get(ifidx) != VTSS_OK) {
		if (vtss_appl_ip_if_conf_set(ifidx) != VTSS_OK) {
			return DSC_FAILURE;
		}
	}

	if (vtss_appl_ip_ipv4_conf_set(ifidx, &ip_conf) != VTSS_RC_OK) {
		return DSC_FAILURE;
	}

	return DSC_SUCCESS;
}
/**************************************************************************************************
 * PUBLIC FUNCTIONS
 *************************************************************************************************/
int dsc_cdnt_get_sys_name(char *string)
{
	system_conf_t  sys_conf;

	if(!string) {
		return DSC_FAILURE;
	}

	memset(string, 0, DSC_MAX_STR_LEN);
	system_get_config(&sys_conf);
	memcpy(string, &(sys_conf.sys_name), DSC_MAX_STR_LEN);
	return DSC_SUCCESS;
}

int dsc_cdnt_get_sys_location(char *string)
{
	system_conf_t  sys_conf;

	if(!string) {
		return DSC_FAILURE;
	}

	memset(string, 0, DSC_MAX_STR_LEN);
	system_get_config(&sys_conf);
	memcpy(string, &(sys_conf.sys_location), DSC_MAX_STR_LEN);

	return DSC_SUCCESS;
}

int dsc_cdnt_auth(char *username, char *passwd, dsc_err_t *error)
{
	u8			lvl;
	u16			agent_id;

	if(vtss_auth_login(VTSS_APPL_AUTH_AGENT_CONSOLE, NULL, username, passwd, &lvl, &agent_id) != VTSS_RC_OK) {
		*error = DSC_ERR_AUTH;
		return DSC_FAILURE;
	}

	if(lvl != 15) {
		*error = DSC_ERR_AUTH_LVL;
		return DSC_FAILURE;
	}

	return DSC_SUCCESS;
}

int dsc_cdnt_get_ipaddr_info(vtss_if_id_vlan_t *vlan_id, int *mod, mesa_ipv4_t *ipaddr, mesa_prefix_size_t *netmask, dsc_err_t *error)
{

	vtss_ifindex_t ifidx;
	vtss_appl_ip_ipv4_conf_t ipconf;

	memset(&ipconf, 0, sizeof(vtss_appl_ip_ipv4_conf_t));

	if(vtss_ip_if_id_next(*vlan_id, vlan_id) != VTSS_OK) {
		*error = DSC_ERR_CFA;
		return DSC_FAILURE;
	}

	(void) vtss_ifindex_from_vlan(*vlan_id, &ifidx);

	if(vtss_appl_ip_ipv4_conf_get(ifidx, &ipconf) != VTSS_OK ) {
		*error = DSC_ERR_CFA;
		return DSC_FAILURE;
	}

	/* dhcp + fallback */
	if (ipconf.dhcpc) {
		*mod = DSC_MGTNET_IPMOD_DHCP;
	} else {
		*mod = DSC_MGTNET_IPMOD_MANUAL;
	}

	*ipaddr  = ipconf.network.address;
	*netmask = ipconf.network.prefix_size;

	return DSC_SUCCESS;
}

int dsc_cdnt_cfg_ipaddr(unsigned int vlan_id, int method, unsigned int ipaddr, unsigned int netmask, dsc_err_t *error)
{
	mgtnet4_info_t		ipaddr_info;

	memset(&ipaddr_info, 0, sizeof(mgtnet4_info_t));

	ipaddr_info.vlan_id = vlan_id;
	ipaddr_info.method = method;
	ipaddr_info.ipaddr = ipaddr;
	ipaddr_info.netmask = netmask;

	if(!set_ipv4_conf(&ipaddr_info)) {
		*error = DSC_ERR_CFA;
		return DSC_FAILURE;
	}

	return DSC_SUCCESS;
}

int dsc_cdnt_get_gateway(const int max, mesa_routing_entry_t  *rt, int *const cnt, dsc_err_t *error)
{
	int ct = max;
	int route_ct = 0;
	mesa_routing_entry_t *routes = NULL;

    if ((VTSS_CALLOC_CAST(routes, ct, sizeof(*routes))) != NULL) {
        if ((vtss_ip_default_route_get(ct, routes, &route_ct)) != VTSS_OK) {
			*error = DSC_ERR_CFA;
			return DSC_FAILURE;
        }
    }

	memcpy(rt, routes, ct*sizeof(mesa_routing_entry_t));
	memcpy(cnt, &route_ct, sizeof(int));

	VTSS_FREE(routes);
	routes = NULL;

	return DSC_SUCCESS;
}

int dsc_cdnt_set_gateway(unsigned int gateway, dsc_err_t *error)
{
	vtss_rc rc;
	mesa_ipv4_uc_t conf;
	vtss_appl_ip_ipv4_route_conf_t route_conf;

	if(vtss_build_ipv4_uc(&conf, 0, 0, gateway) != VTSS_RC_OK) {
		*error = DSC_ERR_NETIP;
		return DSC_FAILURE;
	}
	route_conf.distance=1;

	rc = vtss_appl_ip_ipv4_route_conf_set(&conf,&route_conf);

	if (rc != VTSS_RC_OK && rc != IP_ERROR_EXISTS) {
		*error = DSC_ERR_NETIP;
		return DSC_FAILURE;
	}

	return DSC_SUCCESS;;
}

int dsc_cdnt_set_mpflag(unsigned char mpflag, dsc_err_t *error)
{
	conf_board_t conf;

	if(conf_mgmt_board_get(&conf) != 0) {
		*error = DSC_ERR_CONFIG;
		return DSC_FAILURE;
	}

	conf.mpflag = mpflag;

	if (conf_mgmt_board_set(&conf) != VTSS_OK) {
		*error = DSC_ERR_CONFIG;
		return DSC_FAILURE;
	}

	return DSC_SUCCESS;
}

int dsc_cdnt_chg_passwd(char *username, char *new_passwd, dsc_err_t *error)
{
	users_conf_t	conf;

	memset(&conf, 0x0, sizeof(conf));
	memcpy(&(conf.username), username, sizeof(conf.username));
	
	// Get user entry first if it existing
	if(vtss_users_mgmt_conf_get(&conf, FALSE) !=VTSS_OK) {
		*error = DSC_ERR_CONFIG;
		return DSC_FAILURE;
	}

	memcpy(&(conf.password), new_passwd, sizeof(conf.password));
    conf.encrypted=0;//encrypted
	if (vtss_users_mgmt_conf_set(&conf) != VTSS_OK) {
		*error = DSC_ERR_CONFIG;
		return DSC_FAILURE;
	}

	return DSC_SUCCESS;
}

int dsc_cdnt_create_buf(dsc_packet_info_t *pkt_info)
{
	if(pkt_info->type == DSC_TYPE_CFG_SINGLE_UPGRADE || pkt_info->type == DSC_TYPE_CFG_DUAL_UPGRADE) {
		pkt_info->data_buff = (char *)VTSS_CALLOC(pkt_info->data_len, sizeof(char));

		if(!pkt_info->data_buff) {
			pkt_info->error = DSC_ERR_FILE_OPEN;
			return DSC_FAILURE;
		}
	} else if(pkt_info->type == DSC_TYPE_CFG_IMPORT) {
		pkt_info->data_buff = (char *)VTSS_CALLOC(pkt_info->data_len, sizeof(char));
		if(!pkt_info->data_buff) {
			pkt_info->error = DSC_ERR_FILE_OPEN;
			return DSC_FAILURE;
		}
	} else if(pkt_info->type == DSC_TYPE_CFG_EXPORT) {
		pkt_info->data_buff = (char *)VTSS_CALLOC(pkt_info->data_len, sizeof(char));

		if(!pkt_info->data_buff) {
			pkt_info->error = DSC_ERR_FILE_OPEN;
			return DSC_FAILURE;
		}
	} else {
		pkt_info->error = DSC_ERR_TYPE;
		return DSC_FAILURE;
	}

	return DSC_SUCCESS;
}

int dsc_cdnt_write_buf(dsc_packet_info_t *pkt_info, unsigned int write_len)
{
	if(pkt_info->type == DSC_TYPE_CFG_SINGLE_UPGRADE || pkt_info->type == DSC_TYPE_CFG_DUAL_UPGRADE) {
		memcpy(pkt_info->data_buff, pkt_info->sock_buff, write_len);
	} else if(pkt_info->type == DSC_TYPE_CFG_IMPORT) {
		memcpy(pkt_info->data_buff, pkt_info->sock_buff, write_len);
	}

	pkt_info->packet_len -= write_len;
	pkt_info->data_buff += write_len;  //data buff offest

	return DSC_SUCCESS;
}

int dsc_cdnt_read_buf(dsc_packet_info_t *pkt_info, unsigned int *read_len)
{
	if(DSC_MAX_BUFF_LEN < pkt_info->data_len) {
		memset(pkt_info->sock_buff, 0, DSC_MAX_BUFF_LEN);
		memcpy(pkt_info->sock_buff, pkt_info->data_buff, DSC_MAX_BUFF_LEN);
		*read_len = DSC_MAX_BUFF_LEN;
	} else {
		memset(pkt_info->sock_buff, 0, pkt_info->data_len);
		memcpy(pkt_info->sock_buff, pkt_info->data_buff, pkt_info->data_len);
		*read_len = pkt_info->data_len;
	}

	pkt_info->data_buff += *read_len;

	return DSC_SUCCESS;
}

int dsc_cdnt_fwupg_valid(dsc_packet_info_t *pkt_info)
{
	//unsigned long image_version;

	//if(firmware_check((unsigned char *)pkt_info->data_buff, pkt_info->data_len, FIRMWARE_TRAILER_V1_TYPE_MANAGED, 0, &image_version, FALSE) != VTSS_OK) {
    if(firmware_check((unsigned char *)pkt_info->data_buff,pkt_info->data_len)){
	    pkt_info->error = DSC_ERR_CONFIG;
		return DSC_FAILURE;
	}

	return DSC_SUCCESS;
}

int dsc_cdnt_do_fwupg(dsc_packet_info_t *pkt_info)
{
	//unsigned long image_version;
	mesa_restart_t restart_type = MESA_RESTART_WARM;
	char filename[FIRMWARE_IMAGE_NAME_MAX];
	int upgrade_dual = false;

	dsc_cdnt_fwfilename_get(filename, sizeof(filename));

	upgrade_dual = (pkt_info->type == DSC_TYPE_DO_DUAL_UPGRADE) ? true : false;

	// bootstrap and dual upgrade are things that must be done during the mptest phase
	if(upgrade_dual &&
		lntn_firmware_bootstrap_async(NULL,
									(unsigned char *)pkt_info->data_buff,
									pkt_info->data_len,
									filename,
									upgrade_dual) == FIRMWARE_ERROR_IN_PROGRESS) {
		return DSC_SUCCESS;
	}

	// Upgrade single or dual images
	if(firmware_update_async(NULL,
							(unsigned char *)pkt_info->data_buff,
							pkt_info->data_len,
							filename,
							restart_type,
							upgrade_dual) != FIRMWARE_ERROR_IN_PROGRESS) {
		pkt_info->error = DSC_ERR_CONFIG;
		return DSC_FAILURE;
	}

	return DSC_SUCCESS;
}

int dsc_cdnt_do_cfg_imp(dsc_packet_info_t *pkt_info)
{
	vtss_icfg_query_result_t	res		= { NULL, NULL };

	if (vtss_icfg_overlay_query_result(pkt_info->data_buff, pkt_info->data_len, &res) != VTSS_RC_OK) {
		pkt_info->error = DSC_ERR_CONFIG_IMPORT;
		return DSC_FAILURE;
	}

	const char *msg = icfg_file_write("startup-config", &res);
	if (msg) {
		pkt_info->error = DSC_ERR_CONFIG_IMPORT;
		return DSC_FAILURE;
	}

	pkt_info->reboot = 1;

	return DSC_SUCCESS;
}

int dsc_cdnt_do_cfg_exp(dsc_packet_info_t *pkt_info)
{
	vtss_icfg_query_result_buf_t *buf;
	vtss_icfg_query_result_t	res		= { NULL, NULL };

	const char *msg = icfg_file_read("startup-config", &res);
	if (msg) {
		pkt_info->error = DSC_ERR_CONFIG_EXPORT;
		return DSC_FAILURE;
	}

	pkt_info->data_len = res.head->used;
	buf = res.head;

	if(!dsc_cdnt_create_buf(pkt_info)) {
		pkt_info->error = DSC_ERR_CONFIG_EXPORT;
		return DSC_FAILURE;
	}

	pkt_info->data_buff = buf->text;

	return DSC_SUCCESS;
}

int dsc_cdnt_do_cfg_save(unsigned char cfg_save_flag)
{
	const char *res;
	res = vtss_icfg_running_config_save();

	if(res) {
		return DSC_FAILURE;
	}
	return DSC_SUCCESS;
}

int dsc_cdnt_do_cfg_reset2default(unsigned char cfg_reset_flag)
{
	if(cfg_reset_flag==DSC_RESET_DEFAULT_ALL){
		control_config_reset(VTSS_USID_ALL, 0);
	}else if(cfg_reset_flag==DSC_RESET_DEFAULT_KEEP_IP){
		control_config_reset(VTSS_USID_ALL, INIT_CMD_PARM2_FLAGS_IP);
	}
	icfg_commit_complete_wait();//wait for config reset done
	if(vtss_icfg_running_config_save()){
		return DSC_FAILURE;
	}
	icfg_commit_complete_wait();//wait for config save done
	/*if(control_system_reset(TRUE, VTSS_USID_ALL, MESA_RESTART_COOL)==-1){
		return DSC_FAILURE;
	}*/
	return DSC_SUCCESS;

}
int dsc_cdnt_reboot(void)
{
	control_system_reset(TRUE, VTSS_USID_ALL, MESA_RESTART_COOL);
	return DSC_SUCCESS;
}

void dsc_cdnt_fwfilename_set(char *filename)
{
	strncpy(fw_filename, filename, FIRMWARE_IMAGE_NAME_MAX);
}

void dsc_cdnt_fwfilename_get(char *filename, int filename_length)
{
	strncpy(filename, fw_filename, filename_length);
}

static unsigned int lntn_get_eng_mode(void)
{
	dsc_device_info_t			*dev_info = (dsc_device_info_t *)dsc_get_device_info();

	return ((dev_info->mpflag != 7) ? 1 : 0);
}

int dsc_cdnt_probematch(char *sock_buff)
{
	dsc_pkt_probematch_t		*probematch = (dsc_pkt_probematch_t *)sock_buff;
	int							payload_len = 0;
	char						*tlv_offset = NULL;
	char						*tlv_type = NULL;
	char						tlv_len = 0;
	dsc_pkt_tlv_ip_t			*tlv_ip_pkt = NULL;
	dsc_pkt_tlv_gw_t			*tlv_gw_pkt = NULL;

	if(lntn_get_eng_mode()) {
		printf("\n[Probe Match]\n"
				"\tmodel_name: %s\n"
				"\tvendor: %s\n"
				"\tname: %s\n"
				"\tfw_version: %s\n"
				"\tsn: %s\n"
				"\tlocation: %s\n"
				"\tmac: %2x:%2x:%2x:%2x:%2x:%2x\n",
				probematch->model_name, probematch->vendor, probematch->name, probematch->fw_version, probematch->sn,
				probematch->location, probematch->mac[0], probematch->mac[1], probematch->mac[2], probematch->mac[3],
				probematch->mac[4], probematch->mac[5]);

		tlv_offset = sock_buff + sizeof(dsc_pkt_probematch_t);
		while(payload_len < probematch->payload_len) {
			tlv_type = tlv_offset;
			switch(*tlv_type) {
				case DSC_TYPE_CFG_IPADDR:
					tlv_ip_pkt = (dsc_pkt_tlv_ip_t *)tlv_offset;
					tlv_len = tlv_ip_pkt->tlv_head.len + 2;
					printf("\t [Ipaddr]\n"
							"\t  vlan_id: %u\n"
							"\t  method: %s\n"
							"\t  ipaddr: %u\n"
							"\t  netmask: %u\n",
							tlv_ip_pkt->vlan_id, (tlv_ip_pkt->method == DSC_MGTNET_IPMOD_MANUAL) ? "Manual" : "DHCP", tlv_ip_pkt->ipaddr, tlv_ip_pkt->netmask);
					break;
				case DSC_TYPE_CFG_GATEWAY:
					tlv_gw_pkt = (dsc_pkt_tlv_gw_t *)tlv_offset;
					tlv_len = tlv_gw_pkt->tlv_head.len + 2;
					printf("\t [Gateway]\n\t  gateway: %u\n", tlv_gw_pkt->gw);
					break;
				default:
					break;
			}
			payload_len += tlv_len;
			tlv_offset += tlv_len;
		}
	}

	return DSC_SUCCESS;
}

