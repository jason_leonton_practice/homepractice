/***********************************************************
 *
 * Copyright (c) 2017 Leonton.Co.Ltd All Rights Reserved.
 *
 * Discovery Module
 *
 ***********************************************************/
#include "main.h"
#include "conf_api.h"
#include "msg_api.h"
#include "critd_api.h"
#include "discovery.h"
#include "discovery_api.h"

#include "misc_api.h"
//#include <network.h>    // For select()
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/time.h> 
#include "vtss_timer_api.h"

#include "dsc_bcast.h"
#include "dsc_ucast.h"
#include "dsc_def.h"


/*****************************************************************************/
/*  TRACE system                                                             */
/*****************************************************************************/
#if (VTSS_TRACE_ENABLED)
static vtss_trace_reg_t trace_reg =
{
	VTSS_TRACE_MODULE_ID, "Discovery", "Discovery Module"
};

static vtss_trace_grp_t trace_grps[VTSS_TRACE_GRP_CNT] = {
	/* VTSS_TRACE_GRP_DEFAULT */ {
		"default",
		"Default",
		VTSS_TRACE_LVL_ERROR,
		VTSS_TRACE_FLAGS_TIMESTAMP
	},
	/* VTSS_TRACE_GRP_CRIT */ {
		"crit",
		"Critical regions ",
		VTSS_TRACE_LVL_ERROR,
		VTSS_TRACE_FLAGS_TIMESTAMP
		},
	/* VTSS_TRACE_GRP_ICLI */ {
		"iCLI",
		"iCLI",
		VTSS_TRACE_LVL_ERROR,
		VTSS_TRACE_FLAGS_TIMESTAMP
	},
};

#define DISCOVERY_CRIT_ENTER() critd_enter(&crit, VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define DISCOVERY_CRIT_EXIT() critd_exit(&crit, VTSS_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)

#else /* !VTSS_TRACE_ENABLED */

#define DISCOVERY_CRIT_ENTER() critd_enter(&crit)
#define DISCOVERY_CRIT_EXIT() critd_exit(&crit)

#endif /* VTSS_TRACE_ENABLED */

/***************************************************************************/
/* Pre-defined                                                             */
/***************************************************************************/
#define DSC_EVENT_DO_NOTHING				(0x00)
#define DSC_EVENT_BCAST_PKT					(0x01)
#define DSC_EVENT_UCAST_CLOSE				(0x02)
#define DSC_EVENT_UCAST_OPEN				(0x04)
#define DSC_EVENT_UCAST_ACCEPT				(0x08)
#define DSC_EVENT_UCAST_PKT					(0x10)
#define DSC_EVENT_SAY_HELLO					(0x20)

#define DSC_DEFAULT_FD						(-1)

typedef enum {
	BCAST = 0, 
	UCAST_SERVER = 1,
	UCAST_CLIENT = 2,
} socket_type_t;

typedef struct {
	int						fd[SOCK_HANDLE_NUM];
	unsigned char 			bflag;
	unsigned char 			uflag;
	vtss::Timer			    timer;
	dsc_device_info_t		dev_info;
} dsc_info_t;

/***************************************************************************/
/* Global variables                                                        */
/***************************************************************************/
static critd_t crit;
static dsc_info_t dsc_info;

/***************************************************************************/
/* Private functions                                                       */
/***************************************************************************/
static void dsc_clear_socket(socket_type_t type)
{
    T_I("[CLEAR SOCKET] socket type: %s",type == 0?"BCAST":type == 1?"UCAST SERVER":"UCAST CLIENT");
	switch(type) {
		case BCAST: /*set bcast ucast_server ucast_client default*/
			if(dsc_info.fd[BCAST_SERVER_FD] >= 0) {
				close(dsc_info.fd[BCAST_SERVER_FD]);
			}
			dsc_info.fd[BCAST_SERVER_FD] = DSC_DEFAULT_FD;
			dsc_info.bflag &= DSC_EVENT_DO_NOTHING;
            
		case UCAST_SERVER: /*set ucast_server ucast_client default*/
			if(dsc_info.fd[UCAST_SERVER_FD] >= 0) {
				close(dsc_info.fd[UCAST_SERVER_FD]);
			}
			dsc_info.fd[UCAST_SERVER_FD] = DSC_DEFAULT_FD;
			dsc_info.uflag &= DSC_EVENT_DO_NOTHING;
            
		case UCAST_CLIENT: /*set ucast_client default*/
			if(dsc_info.fd[UCAST_CLIENT_FD]>=0) {
				close(dsc_info.fd[UCAST_CLIENT_FD]);
			}
			dsc_info.fd[UCAST_CLIENT_FD] = DSC_DEFAULT_FD;
			dsc_info.uflag &= DSC_EVENT_DO_NOTHING;
			break;
	}
}

static void dsc_ucast_timer_cb(vtss::Timer *timer)
{
	dsc_clear_socket(UCAST_SERVER);
}

static void dsc_info_fetch(void)
{
	int				index;
	char			tmp[32];
	conf_board_t	board;

	memset(tmp, 0, sizeof(tmp));
	memset(&board, 0, sizeof(conf_board_t));

	if (conf_mgmt_board_get(&board) != 0) {
		T_D("Board conf get fail!");
		return;
	}

	// model name
	memcpy(&(dsc_info.dev_info.model_name), &(board.modelname), sizeof(dsc_info.dev_info.model_name));
	T_D("model name: %s",dsc_info.dev_info.model_name);

	//vendor
	memcpy(&(dsc_info.dev_info.vendor), &(board.vendor), sizeof(dsc_info.dev_info.vendor));
	T_D("vendor: %s",dsc_info.dev_info.vendor);

	//serial_no
	//conf_mgmt_data_get(CONF_MGMT_GET_SN, tmp, sizeof(tmp));
	conf_mgmt_sn_get(tmp, sizeof(tmp));
	memcpy(&(dsc_info.dev_info.serial_no), tmp, sizeof(dsc_info.dev_info.serial_no));
	T_D("serial_no: %s",dsc_info.dev_info.serial_no);	

	//buildtime
	memcpy(&(dsc_info.dev_info.buildtime), misc_software_date_txt(), sizeof(dsc_info.dev_info.buildtime));
	T_D("buildtime: %s",dsc_info.dev_info.buildtime);	

	//mpflag
	dsc_info.dev_info.mpflag = board.mpflag;
	T_D("mpflag: %d",dsc_info.dev_info.mpflag);	

	//version
	memcpy(&(dsc_info.dev_info.version), misc_software_version_txt(), sizeof(dsc_info.dev_info.version));
	T_D("version: %s",dsc_info.dev_info.version);

	//mac
	memcpy((unsigned char *)dsc_info.dev_info.mac, &(board.mac_address), sizeof(dsc_info.dev_info.mac));
	for(index = 0; index < sizeof(dsc_info.dev_info.mac); index++) {
		T_D("%02x",dsc_info.dev_info.mac[index]);
	}
}

static void dsc_update_flag(socket_type_t type, unsigned char flag)
{
    T_N("[UPDATE FLAG] socket type: %s",type == 0?"BCAST":type == 1?"UCAST SERVER":"UCAST CLIENT");
	switch(type) {
		case BCAST:
			dsc_info.bflag &= DSC_EVENT_DO_NOTHING;
			dsc_info.bflag |= flag;
			break;
		case UCAST_SERVER:
		case UCAST_CLIENT:
			dsc_info.uflag &= DSC_EVENT_DO_NOTHING;
			dsc_info.uflag |= flag;
			break;		
	}
    T_N("[UPDATE FLAG] bflag: %X, uflag:: %X",dsc_info.bflag,dsc_info.uflag);
}

static void dsc_ucast_start_timer(void)
{
    T_I("[TIMER] TIMER START");
	vtss::Timer *timer = &dsc_info.timer;

    timer->callback    = dsc_ucast_timer_cb;
    timer->set_period(vtss::milliseconds(30000));
	timer->set_repeat(FALSE);

	if (vtss_timer_start(timer) != VTSS_RC_OK) {
		T_D("Unable to start timer!");
	}
}

static void dsc_ucast_stop_timer(void)
{
    T_I("[TIMER] TIMER STOP");
	vtss::Timer *timer = &dsc_info.timer;

	if (vtss_timer_cancel(timer) != VTSS_RC_OK) {
		T_D("Unable to stop timer!");
	}
}

static void dsc_ucast_restart_timer(void)
{
    T_I("[TIMER] TIMER RESTART");
	dsc_ucast_stop_timer();
	dsc_ucast_start_timer();
}

static int dsc_sock_select(void)
{
	int					type_fd = 0; 
	int					highest_fd = 0;
	int					events = 0;
	fd_set				rdset;
	struct timeval		tv;

	FD_ZERO(&rdset);

	tv.tv_sec  = (SOCK_SELECT_TIMEOUT) / 1000;
	tv.tv_usec = (SOCK_SELECT_TIMEOUT % 1000) * 1000;

	for(type_fd = BCAST_SERVER_FD; type_fd < SOCK_HANDLE_NUM; type_fd++) {
        T_N("[SOCK SELECT] fd[%s]: %d",type_fd == 0? "BCAST":type_fd == 1? "UCAST SR":"UCAST CL",dsc_info.fd[type_fd]);
		if(dsc_info.fd[type_fd] >= 0) {
			FD_SET(dsc_info.fd[type_fd], &rdset);
			highest_fd = MAX(highest_fd, dsc_info.fd[type_fd]);
		}
	}

	T_N("[SOCK SELECT] highest_fd: %d",highest_fd);

	if((events = select(highest_fd + 1, &rdset, NULL, NULL, &tv)) > 0) {
		for(type_fd = BCAST_SERVER_FD; type_fd < SOCK_HANDLE_NUM; type_fd++) {
			if(dsc_info.fd[type_fd] >= 0) {
				switch(type_fd) {
					case BCAST_SERVER_FD:
						if(FD_ISSET(dsc_info.fd[type_fd], &rdset)) {
							dsc_update_flag(BCAST, DSC_EVENT_BCAST_PKT);
						}
						break;
					case UCAST_SERVER_FD:
						if(FD_ISSET(dsc_info.fd[type_fd], &rdset)) {
							dsc_update_flag(UCAST_SERVER, DSC_EVENT_UCAST_ACCEPT);
						}
						break;
					case UCAST_CLIENT_FD:
						if(FD_ISSET(dsc_info.fd[type_fd], &rdset)) {
							dsc_update_flag(UCAST_CLIENT, DSC_EVENT_UCAST_PKT);
						}
						break;
					default:
						T_N("Unknow fd type");
						return events;
				}
			}
		}
	}

	T_N("[SOCK SELECT] events: %d",events);
	return events;
}


/***************************************************************************/
/* Public function                                                         */
/***************************************************************************/
void dsc_sel_rm_fd(int sfd)
{
	int type;

	for(type = BCAST_SERVER_FD; type < SOCK_HANDLE_NUM; type++) {
		if(dsc_info.fd[type] == sfd) {
			if(type == BCAST_SERVER_FD) {
				dsc_clear_socket(BCAST);				
			} else if(UCAST_SERVER_FD){
				dsc_clear_socket(UCAST_SERVER);
			} else {
				dsc_clear_socket(UCAST_CLIENT);
			}
		}
	}
}

void *dsc_get_device_info(void)
{
	return &(dsc_info.dev_info);
}

int dsc_send_ucast_open_event(void)
{
	if(dsc_info.fd[UCAST_SERVER_FD] >= 0) {
		T_D("[OPEN UCAST] Ucast server exist");
		return DSC_FAILURE;
	}

	if(!dsc_ucast_srv_open(&(dsc_info.fd[UCAST_SERVER_FD]))) {
		return DSC_FAILURE;
	}

	dsc_ucast_restart_timer();

	return DSC_SUCCESS;
}

int dsc_send_ucast_close_event(void)
{
	dsc_clear_socket(UCAST_SERVER);
	dsc_ucast_stop_timer();
	return DSC_SUCCESS;
}


/***************************************************************************/
/* Default init                                                            */
/***************************************************************************/

static void dsc_conf_default_init(void)
{
	int fd_idx = 0;

	DISCOVERY_CRIT_ENTER();
	//memset(&dsc_info, 0, sizeof(dsc_info_t));

	//Init All FD to -1
	for(fd_idx = 0; fd_idx < SOCK_HANDLE_NUM; fd_idx++) {
		dsc_info.fd[fd_idx] = DSC_DEFAULT_FD;
	}

	//Timer Init
	vtss::Timer *timer = &dsc_info.timer;
	timer->modid       = VTSS_MODULE_ID_DISCOVERY;	
	DISCOVERY_CRIT_EXIT();
}

static void dsc_bcast_socket_open(void)
{
	T_N("dsc_bcast_socket_init");

	DISCOVERY_CRIT_ENTER();    
	if(dsc_info.fd[BCAST_SERVER_FD] >= 0) {
		T_D("[OPEN BCAST] Bcast server exist");
        DISCOVERY_CRIT_EXIT();
		return;
	}
    
	if(dsc_bcast_srv_open(&(dsc_info.fd[BCAST_SERVER_FD])) != DSC_SUCCESS) {
		T_D("Broadcast socket open fail!");
	}
	DISCOVERY_CRIT_EXIT();
}


/***************************************************************************/
/* Thread                                                                  */
/***************************************************************************/

static vtss_handle_t discovery_thread_handle;
static vtss_thread_t   discovery_thread_block;

static void discovery_thread(vtss_addrword_t data)
{
	T_N("Thread Enter");

	dsc_conf_default_init();
	dsc_bcast_socket_open();

	VTSS_OS_MSLEEP(DSC_DELAY);

	while(1) {	
		if(dsc_sock_select() > 0) {

			if(dsc_info.bflag & DSC_EVENT_BCAST_PKT) {
				dsc_bcast_handle(dsc_info.fd[BCAST_SERVER_FD]);
				dsc_update_flag(BCAST, DSC_EVENT_DO_NOTHING);
			}

			if(dsc_info.uflag & DSC_EVENT_UCAST_ACCEPT) {
				dsc_ucast_accept(dsc_info.fd[UCAST_SERVER_FD], &(dsc_info.fd[UCAST_CLIENT_FD]));
				dsc_update_flag(UCAST_SERVER, DSC_EVENT_DO_NOTHING);
			}

			if(dsc_info.uflag & DSC_EVENT_UCAST_PKT) {
				if(dsc_ucast_handle(dsc_info.fd[UCAST_CLIENT_FD]) == DSC_SUCCESS) {
					dsc_ucast_restart_timer();
				}
				dsc_clear_socket(UCAST_CLIENT);
				dsc_update_flag(UCAST_CLIENT, DSC_EVENT_DO_NOTHING);
			}

		}

		VTSS_OS_MSLEEP(DSC_DELAY); // Allow other threads to get access
	}
}


vtss_rc discovery_init(vtss_init_data_t *data)
{
	vtss_isid_t isid = data->isid;
	vtss_rc  rc = VTSS_OK;

	//T_D("enter, cmd: %d, isid: %u, flags: 0x%x", data->cmd, data->isid, data->flags);

	switch (data->cmd) {
		case INIT_CMD_EARLY_INIT:
			/* Initialize and register trace ressources */
			VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, VTSS_TRACE_GRP_CNT);
			VTSS_TRACE_REGISTER(&trace_reg);
			T_N("DISCOVERY INIT TRACE REGISTER\n");
			break;
			
		case INIT_CMD_INIT:
			critd_init(&crit, "Discovery crit", VTSS_MODULE_ID_DISCOVERY, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
			DISCOVERY_CRIT_EXIT();
			vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
								discovery_thread,
								0,
								"DISCOVERY",
								nullptr,
								0,
								&discovery_thread_handle,
								&discovery_thread_block);
			T_N("DISCOVERY INIT\n");
			break;
		case INIT_CMD_START:
			T_N("DISCOVERY START\n");
			break;
		case INIT_CMD_CONF_DEF:
			T_N("CONF_DEF, isid: %d", isid);
			if (isid == VTSS_ISID_GLOBAL) {
				dsc_bcast_socket_open();
				dsc_info_fetch();
			}
			T_N("INIT_CMD_CONF_DEF LEAVE");
			break;
		case INIT_CMD_MASTER_UP:
			T_N("MASTER_UP");
			dsc_info_fetch();
			break;
		case INIT_CMD_MASTER_DOWN:
			T_N("MASTER_DOWN");
			break;
		case INIT_CMD_SWITCH_ADD:
			T_N("SWITCH_ADD, isid: %d", isid);
			break;
		case INIT_CMD_SWITCH_DEL:
			T_N("SWITCH_DEL, isid: %d", isid);
			break;
		default:
			break;
	}
	T_N("exit");
	return rc;

}
