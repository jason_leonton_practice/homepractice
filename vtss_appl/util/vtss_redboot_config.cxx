/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

#if defined(VTSS_SW_OPTION_FIRMWARE)

#include "main.h"
#include <sys/ioctl.h>

#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_POST  /* Pick one... */
#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_POST

#include "vtss_trace_api.h"
#include "vtss_mtd_api.hxx"
#include "vtss_redboot_config.hxx"

static vtss_mtd_t redboot_config;  /* mtd device structure for redboot config */

/*
 * Read the config from the flash to the data base (in memory copy)
 * If available (i.e. good checksum, etc), initialize "known"
 * values for later use.
 */
mesa_rc
redboot_config_read(vtss_config_t **cfg) {

    /* Open redboot config */
    if ( (vtss_mtd_open(&redboot_config, "RedBoot_config")) == VTSS_OK ) {
        /* allocate dynamic memory based on the size of redboot config data */
        if ( (*cfg = (vtss_config_t *)VTSS_MALLOC(redboot_config.info.size)) ) {
            memset(*cfg, 0, redboot_config.info.size);  /* Initialize memory area */
            if (pread(redboot_config.fd, *cfg, redboot_config.info.size, 0) != -1) {
                // printf("\nDump Config:-------------------------------------------\n");
                // fwrite((&(*cfg)->config_data[0]), 0x1000, 1, stdout);
                // printf("\nDump end-----------------------------------------------\n");
                return VTSS_OK;
            } else {
                T_D("pread from RedBoot_config failed! [%s]\n", strerror(errno));
            }
        } else {
            T_D("malloc faild! [%s]\n", strerror(errno));
        }
    } else {
        T_D("Open RedBoot_config failed! [%s]\n", strerror(errno));
    }
    *cfg = NULL;
    return VTSS_RC_ERROR;
}


/* Manage config info with the FLASH */
static int
config_length(int type)
{
    switch (type) {
        case CONFIG_BOOL:
            return sizeof(unsigned long);  /* 4 */
        case CONFIG_INT:
            return sizeof(unsigned long);  /* 4 */
        case CONFIG_STRING:
            return MAX_STRING_LENGTH;
        case CONFIG_SCRIPT:
            return MAX_SCRIPT_LENGTH;
        default:
            return 0;
    }
}


/* Find the config entry for a specific key */
mesa_rc
redboot_config_lookup(vtss_config_t **cfg, char *key, char **dp)
{
    int len = 0;

    // printf("\nTrying to lookup: %s\n", key);
    if (*cfg) {
        *dp = &(*cfg)->config_data[0];

        while (*dp < &(*cfg)->config_data[sizeof((*cfg)->config_data)]) {
            len = 4 + CONFIG_OBJECT_KEYLEN(*dp) + CONFIG_OBJECT_ENABLE_KEYLEN(*dp) +
                    config_length(CONFIG_OBJECT_TYPE(*dp));
            if ( (strcmp("\0", CONFIG_OBJECT_KEY(*dp)) != 0) && (strlen(CONFIG_OBJECT_KEY(*dp)) < 100)) {
                // printf("\nFound: %s\n", CONFIG_OBJECT_KEY(*dp));

                // printf("CONFIG_OBJECT_KEYLEN(*dp) = 0x%02x\n", CONFIG_OBJECT_KEYLEN(*dp));
                // printf("CONFIG_OBJECT_ENABLE_KEYLEN(*dp) = 0x%02x\n", CONFIG_OBJECT_ENABLE_KEYLEN(*dp));
                // printf("config_length(CONFIG_OBJECT_TYPE(*dp) = 0x%02x\n", config_length(CONFIG_OBJECT_TYPE(*dp)));
                // printf("CONFIG_OBJECT_VALUE(*dp) = 0x%02x\n", *CONFIG_OBJECT_VALUE(*dp));
            }
            if (strcmp(key, CONFIG_OBJECT_KEY(*dp)) == 0) {
                // printf("Found \"%s\" in RedBoot_config\n", key);
                return VTSS_OK;
            }
            /* Sanity check */
            if (len <= 0) {
                break;
            }
            *dp += len;
        }
        T_D("Cannot find config data for '%s'\n", key);
    } else {
        T_D("RedBoot config in memory is invalid!\n");
    }
    *dp = NULL;
    return VTSS_RC_ERROR;
}

/* Retrieve a data object from the data base (in memory copy) */
mesa_rc
redboot_config_get(vtss_config_t **cfg, char *key, void *val, int type)
{
    char *dp = NULL;
    void *val_ptr = NULL;

    if ( (redboot_config_lookup(cfg, key, &dp)) == VTSS_OK ) {
        if ( CONFIG_OBJECT_TYPE(dp) == type ) {
            val_ptr = (void *)CONFIG_OBJECT_VALUE(dp);
            switch (type) {
                // Note: the data may be unaligned in the configuration data
                case CONFIG_BOOL:
                    memcpy(val, val_ptr, sizeof(bool));
                    break;
                case CONFIG_INT:
                    memcpy(val, val_ptr, sizeof(unsigned long));
                    break;
                case CONFIG_STRING:
                case CONFIG_SCRIPT:
                    // Just return a pointer to the script/line
                    *(unsigned char **)val = (unsigned char *)val_ptr;
                    break;
            }
        } else {
        T_D("Request for config value '%s' - wrong type\n", key);
    }
    return VTSS_OK;
    }
    return VTSS_RC_ERROR;
}


/******************************************************************************/
// The CRC32 code is borrowed from
//   $(Top)/eCos/packages/services/crc/current/src/crc32.c
/******************************************************************************/

// ======================================================================
//  COPYRIGHT (C) 1986 Gary S. Brown.  You may use this program, or
//  code or tables extracted from it, as desired without restriction.
//
//  First, the polynomial itself and its table of feedback terms.  The
//  polynomial is
//  X^32+X^26+X^23+X^22+X^16+X^12+X^11+X^10+X^8+X^7+X^5+X^4+X^2+X^1+X^0
//
// ======================================================================
static const u32 crc32_tab[] = {
      0x00000000L, 0x77073096L, 0xee0e612cL, 0x990951baL, 0x076dc419L,
      0x706af48fL, 0xe963a535L, 0x9e6495a3L, 0x0edb8832L, 0x79dcb8a4L,
      0xe0d5e91eL, 0x97d2d988L, 0x09b64c2bL, 0x7eb17cbdL, 0xe7b82d07L,
      0x90bf1d91L, 0x1db71064L, 0x6ab020f2L, 0xf3b97148L, 0x84be41deL,
      0x1adad47dL, 0x6ddde4ebL, 0xf4d4b551L, 0x83d385c7L, 0x136c9856L,
      0x646ba8c0L, 0xfd62f97aL, 0x8a65c9ecL, 0x14015c4fL, 0x63066cd9L,
      0xfa0f3d63L, 0x8d080df5L, 0x3b6e20c8L, 0x4c69105eL, 0xd56041e4L,
      0xa2677172L, 0x3c03e4d1L, 0x4b04d447L, 0xd20d85fdL, 0xa50ab56bL,
      0x35b5a8faL, 0x42b2986cL, 0xdbbbc9d6L, 0xacbcf940L, 0x32d86ce3L,
      0x45df5c75L, 0xdcd60dcfL, 0xabd13d59L, 0x26d930acL, 0x51de003aL,
      0xc8d75180L, 0xbfd06116L, 0x21b4f4b5L, 0x56b3c423L, 0xcfba9599L,
      0xb8bda50fL, 0x2802b89eL, 0x5f058808L, 0xc60cd9b2L, 0xb10be924L,
      0x2f6f7c87L, 0x58684c11L, 0xc1611dabL, 0xb6662d3dL, 0x76dc4190L,
      0x01db7106L, 0x98d220bcL, 0xefd5102aL, 0x71b18589L, 0x06b6b51fL,
      0x9fbfe4a5L, 0xe8b8d433L, 0x7807c9a2L, 0x0f00f934L, 0x9609a88eL,
      0xe10e9818L, 0x7f6a0dbbL, 0x086d3d2dL, 0x91646c97L, 0xe6635c01L,
      0x6b6b51f4L, 0x1c6c6162L, 0x856530d8L, 0xf262004eL, 0x6c0695edL,
      0x1b01a57bL, 0x8208f4c1L, 0xf50fc457L, 0x65b0d9c6L, 0x12b7e950L,
      0x8bbeb8eaL, 0xfcb9887cL, 0x62dd1ddfL, 0x15da2d49L, 0x8cd37cf3L,
      0xfbd44c65L, 0x4db26158L, 0x3ab551ceL, 0xa3bc0074L, 0xd4bb30e2L,
      0x4adfa541L, 0x3dd895d7L, 0xa4d1c46dL, 0xd3d6f4fbL, 0x4369e96aL,
      0x346ed9fcL, 0xad678846L, 0xda60b8d0L, 0x44042d73L, 0x33031de5L,
      0xaa0a4c5fL, 0xdd0d7cc9L, 0x5005713cL, 0x270241aaL, 0xbe0b1010L,
      0xc90c2086L, 0x5768b525L, 0x206f85b3L, 0xb966d409L, 0xce61e49fL,
      0x5edef90eL, 0x29d9c998L, 0xb0d09822L, 0xc7d7a8b4L, 0x59b33d17L,
      0x2eb40d81L, 0xb7bd5c3bL, 0xc0ba6cadL, 0xedb88320L, 0x9abfb3b6L,
      0x03b6e20cL, 0x74b1d29aL, 0xead54739L, 0x9dd277afL, 0x04db2615L,
      0x73dc1683L, 0xe3630b12L, 0x94643b84L, 0x0d6d6a3eL, 0x7a6a5aa8L,
      0xe40ecf0bL, 0x9309ff9dL, 0x0a00ae27L, 0x7d079eb1L, 0xf00f9344L,
      0x8708a3d2L, 0x1e01f268L, 0x6906c2feL, 0xf762575dL, 0x806567cbL,
      0x196c3671L, 0x6e6b06e7L, 0xfed41b76L, 0x89d32be0L, 0x10da7a5aL,
      0x67dd4accL, 0xf9b9df6fL, 0x8ebeeff9L, 0x17b7be43L, 0x60b08ed5L,
      0xd6d6a3e8L, 0xa1d1937eL, 0x38d8c2c4L, 0x4fdff252L, 0xd1bb67f1L,
      0xa6bc5767L, 0x3fb506ddL, 0x48b2364bL, 0xd80d2bdaL, 0xaf0a1b4cL,
      0x36034af6L, 0x41047a60L, 0xdf60efc3L, 0xa867df55L, 0x316e8eefL,
      0x4669be79L, 0xcb61b38cL, 0xbc66831aL, 0x256fd2a0L, 0x5268e236L,
      0xcc0c7795L, 0xbb0b4703L, 0x220216b9L, 0x5505262fL, 0xc5ba3bbeL,
      0xb2bd0b28L, 0x2bb45a92L, 0x5cb36a04L, 0xc2d7ffa7L, 0xb5d0cf31L,
      0x2cd99e8bL, 0x5bdeae1dL, 0x9b64c2b0L, 0xec63f226L, 0x756aa39cL,
      0x026d930aL, 0x9c0906a9L, 0xeb0e363fL, 0x72076785L, 0x05005713L,
      0x95bf4a82L, 0xe2b87a14L, 0x7bb12baeL, 0x0cb61b38L, 0x92d28e9bL,
      0xe5d5be0dL, 0x7cdcefb7L, 0x0bdbdf21L, 0x86d3d2d4L, 0xf1d4e242L,
      0x68ddb3f8L, 0x1fda836eL, 0x81be16cdL, 0xf6b9265bL, 0x6fb077e1L,
      0x18b74777L, 0x88085ae6L, 0xff0f6a70L, 0x66063bcaL, 0x11010b5cL,
      0x8f659effL, 0xf862ae69L, 0x616bffd3L, 0x166ccf45L, 0xa00ae278L,
      0xd70dd2eeL, 0x4e048354L, 0x3903b3c2L, 0xa7672661L, 0xd06016f7L,
      0x4969474dL, 0x3e6e77dbL, 0xaed16a4aL, 0xd9d65adcL, 0x40df0b66L,
      0x37d83bf0L, 0xa9bcae53L, 0xdebb9ec5L, 0x47b2cf7fL, 0x30b5ffe9L,
      0xbdbdf21cL, 0xcabac28aL, 0x53b39330L, 0x24b4a3a6L, 0xbad03605L,
      0xcdd70693L, 0x54de5729L, 0x23d967bfL, 0xb3667a2eL, 0xc4614ab8L,
      0x5d681b02L, 0x2a6f2b94L, 0xb40bbe37L, 0xc30c8ea1L, 0x5a05df1bL,
      0x2d02ef8dL
   };

/* This is the standard Gary S. Brown's 32 bit CRC algorithm, but
 * accumulate the CRC into the result of a previous CRC.
 */
static u32 cyg_crc32_accumulate(u32 crc32val, u8 *s, int len)
{
    int i;
    for (i=0; i<len; i++) {
        crc32val = crc32_tab[(crc32val ^ s[i]) & 0xff] ^(crc32val >> 8);
    }
    return crc32val;
}

/* This is the standard Gary S. Brown's 32 bit CRC algorithm */
static u32 cyg_crc32(u8 *s, int len)
{
    return (cyg_crc32_accumulate(0, s, len));
}

unsigned long
flash_crc(vtss_config_t **conf)
{
    unsigned long crc;
    crc = cyg_crc32((unsigned char *)*conf, sizeof(**conf)-sizeof((*conf)->cksum));
    return crc;
}

/* Write redboot config in memory to the flash device */
mesa_rc
redboot_config_write(vtss_config_t **cfg)
{
    (*cfg)->len = sizeof(vtss_config_t);
    (*cfg)->key1 = CONFIG_KEY1;
    (*cfg)->key2 = CONFIG_KEY2;
    (*cfg)->cksum = flash_crc(cfg);

    /* MTD device: erase first and then program */
    if ( (vtss_mtd_open(&redboot_config, "RedBoot_config")) == VTSS_OK ) {
        printf("Erasing RedBoot_config ...");
        if ( (vtss_mtd_erase(&redboot_config, redboot_config.info.size)) == VTSS_OK ) {
            printf(" done!\n");
            printf("Programming RedBoot_config ...");
            if ( (vtss_mtd_program(&redboot_config, (const u8*)(*cfg), redboot_config.info.size)) == VTSS_OK ) {
                printf(" done!\n");
                goto done;
            } else {
                T_D(" failed!\n");
                return VTSS_RC_ERROR;
            }
        } else {
            T_D("Erase RedBoot_config faild!\n");
            return VTSS_RC_ERROR;
        }
    } else {
        T_D("Open RedBoot_config failed! [%s]\n", strerror(errno));
        return VTSS_RC_ERROR;
    }
done:
    return VTSS_OK;
}

/* Update a data object in the data base (in memory copy) */
mesa_rc
redboot_config_set(vtss_config_t **cfg, char *key, void *val, int type)
{
    char *dp = NULL;
    void *val_ptr = NULL;

    if ( (redboot_config_lookup(cfg, key, &dp)) == VTSS_OK ) {
        if ( CONFIG_OBJECT_TYPE(dp) == type) {
            val_ptr = (void *)CONFIG_OBJECT_VALUE(dp);
            switch (type) {
                // Note: the data may be unaligned in the configuration data
            case CONFIG_BOOL:
                memcpy(val_ptr, val, sizeof(bool));
                break;
            case CONFIG_INT:
                memcpy(val_ptr, val, sizeof(unsigned long));
                break;
            case CONFIG_STRING:
            case CONFIG_SCRIPT:
                memcpy(val_ptr, val, config_length(CONFIG_STRING));
                break;
            }
        } else {
            printf("Can't set config value '%s' - wrong type\n", key);
            return VTSS_RC_ERROR;
        }
        return VTSS_OK;
    }
    return VTSS_RC_ERROR;
}

/* Cleanup */
mesa_rc
redboot_config_close(vtss_config_t **cfg)
{
    /* free memory space */
    if (*cfg)
        VTSS_FREE(*cfg);
    /* close file */
    return vtss_mtd_close(&redboot_config);
}

mesa_rc
vtss_call_if_flash_cfg_get(vtss_fconfig_t *fc)
{
    vtss_config_t *cfg = NULL;
    char *dp = NULL;

    mesa_rc rc = VTSS_RC_ERROR;
    bool tmp = false;

    if ( (redboot_config_read(&cfg)) == VTSS_OK) {
        if ( (redboot_config_lookup(&cfg, fc->key, &dp)) == VTSS_OK) {
            fc->val = &tmp;
            tmp = *CONFIG_OBJECT_VALUE(dp);
            T_D("%s= %u\n", fc->key, *(bool *)(fc->val));
            rc = VTSS_OK;
        } else {
            T_D("%s lookup failed!\n", fc->key);
        }
    } else {
        T_D("redboot config read failed!\n");
    }
    if ( (redboot_config_close(&cfg)) != VTSS_OK) {
        T_D("redboot config close failed!\n");
        rc = VTSS_RC_ERROR;
    }
    return rc;
}

mesa_rc vtss_call_if_flash_cfg_set(vtss_fconfig_t *fc)
{
    vtss_config_t *cfg = NULL;
    mesa_rc       rc   = VTSS_RC_ERROR;

    if ( (redboot_config_read(&cfg)) == VTSS_OK) {
        if ( (redboot_config_set(&cfg, fc->key, fc->val, fc->type)) == VTSS_OK) {
            if ( (redboot_config_write(&cfg)) == VTSS_OK) {
                T_D("Write %s into flash ok!\n", fc->key);
                rc = VTSS_OK;
            } else {
                T_D("Write %s into flash failed!\n", fc->key);
            }
        } else {
            T_D("Set %s in memory copy failed!\n", fc->key);
        }
    } else {
        T_D("Read redboot config into memory failed!\n");
    }
    if ( (redboot_config_close(&cfg)) != VTSS_OK) {
        T_D("Close redboot config failed!\n");
        rc = VTSS_RC_ERROR;
    }
    return rc;
}

#endif
