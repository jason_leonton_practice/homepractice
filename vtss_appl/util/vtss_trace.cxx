/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "main.h"
#include "vtss_trace.h"
#include "vtss_trace_api.h"
#include "vtss/basics/trace.hxx"
#include "vtss/basics/stream.hxx"
#include "vtss/basics/print_fmt.hxx"
#include "backtrace.hxx"

#if VTSS_SWITCH
#include <conf_api.h>
#include "vtss_os_wrapper.h"

#ifdef VTSS_SW_OPTION_SYSUTIL
#include <sysutil_api.h>
#endif

#ifdef VTSS_SW_OPTION_DAYLIGHT_SAVING
#include "daylight_saving_api.h"
#endif

#if defined(VTSS_SW_OPTION_SYSLOG)
#include <syslog_api.h>
#endif
#endif /* VTSS_SWITCH */

#include "misc_api.h"
#include "led_api.h"

// This defines a function which forces text to the console independent of context.
#define NONBLOCKING_PRINTF printf

// Printing of errors if no module is defined, else we will do recursive calls to vtss_trace_printf which makes the system hang.
#define PRINTE(args) { NONBLOCKING_PRINTF("ERROR: %s#%d: ", __FUNCTION__, __LINE__); NONBLOCKING_PRINTF args; NONBLOCKING_PRINTF("\n"); }

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_TRACE

// Where the trace configuration is written to. It is *no longer* written into
// 'conf', but into this file. We maintain the binary layout for now.
#define TRACE_CONF_FILE_PATH     VTSS_FS_FLASH_DIR "trace-conf"

/* ===========================================================================
 * Trace for the trace module itself
 *  ----------------------------------------------------------------------- */

#if (VTSS_TRACE_ENABLED)
static vtss_trace_reg_t trace_reg =
{
    VTSS_TRACE_MODULE_ID, "trace", "Trace module."
};

#ifndef TRACE_DEFAULT_TRACE_LVL
#define TRACE_DEFAULT_TRACE_LVL VTSS_TRACE_LVL_WARNING
#endif

static vtss_trace_grp_t trace_grps[TRACE_GRP_CNT] =
{
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default",
        TRACE_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_NONE
    },
};

/* A heap-allocated string for comparing with running trace messages. */
static char *vtss_trace_hunt_target = NULL;
#endif /* VTSS_TRACE_ENABLED */

#define HAS_FLAGS(p, f) ((p->flags & (f)) == f)

/* ======================================================================== */


/* ===========================================================================
 * Global variables
 *  ----------------------------------------------------------------------- */

static BOOL trace_init_done = 0;

/* Max number of module index (inherited from module_id.h) */
#define MODULE_ID_MAX VTSS_MODULE_ID_NONE

/* Array with pointers to registrations */
static vtss_trace_reg_t* trace_regs[MODULE_ID_MAX+1];

#if VTSS_TRACE_MULTI_THREAD
/* Semaphore for IO registrations */
vtss_sem_t trace_io_crit;

/* Semaphore for ring buffer access. */
vtss_sem_t trace_rb_crit;
#endif /* VTSS_TRACE_MULTI_THREAD */

BOOL vtss_trace_port_list[100]; // List that enables/disables trace per port for the T_*_PORT commands. *MUST* be global.

const vtss_trace_thread_t trace_thread_default = {
    .lvl      = VTSS_TRACE_LVL_RACKET,
    .stackuse = 0,
    .lvl_prv  = VTSS_TRACE_LVL_RACKET,
};

static vtss_trace_thread_t trace_threads[VTSS_TRACE_MAX_THREAD_ID+1];

// Global level for all modules and threads
static int global_lvl = VTSS_TRACE_LVL_RACKET;

int vtss_trace_global_lvl(void)
{
    return global_lvl;
}

/* ======================================================================== */

/* ###########################################################################
 * Internal functions
 * ------------------------------------------------------------------------ */

/* ----- Wrappers of vtss_trace_io functions to check for ring buf ----- */
template <typename... TS>
static int grp_trace_printf(const vtss_trace_grp_t* const trace_grp_p,
                            char* const err_buf,
                            const char *fmt, TS&& ... args) {
    vtss::StringStream stream;
    vtss::print_fmt(stream, fmt, args...);
    if (HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_RINGBUF)) {
        trace_rb_write_string(err_buf, stream.cstring());
    } else if (!HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_IRQ)) {
        trace_write_string(err_buf, stream.cstring());
    }
    return stream.end() - stream.begin();
} /* grp_trace_printf */

static int grp_trace_printf(const vtss_trace_grp_t* const trace_grp_p,
                            char* const err_buf,
                            const char *data, size_t len) {
    if (HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_RINGBUF)) {
        trace_rb_write_string(err_buf, data);
    } else if (!HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_IRQ)) {
        trace_write_string(err_buf, data);
    }
    return len;
} /* grp_trace_printf */

static int grp_trace_vprintf(const vtss_trace_grp_t* const trace_grp_p,
                             char* const err_buf,
                             const char *fmt, va_list ap)
{
    int rv = 0;

    if (HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_RINGBUF)) {
        rv = trace_rb_vprintf(err_buf, fmt, ap);
    } else if(!HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_IRQ)) {
        rv = trace_vprintf(err_buf, fmt, ap);
    }

    return rv;
} /* grp_trace_vprintf */


static void grp_trace_write_string(const vtss_trace_grp_t* const trace_grp_p,
                                   char* const err_buf,
                                   const char *str)
{
    if (HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_RINGBUF)) {
        trace_rb_write_string(err_buf, str);
    } else if (!HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_IRQ)) {
        trace_write_string(err_buf, str);
    }
} /* grp_trace_write_string */


static void grp_trace_write_char(const vtss_trace_grp_t* const trace_grp_p,
                                 char* const err_buf,
                                 const char c)
{
    if (HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_RINGBUF)) {
        trace_rb_write_char(err_buf, c);
    } else if (!HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_IRQ)) {
        trace_write_char(err_buf, c);
    }
} /* grp_trace_write_string */


static void grp_trace_flush(const vtss_trace_grp_t* const trace_grp_p)
{
    if (!HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_RINGBUF) && !HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_IRQ)) {
        trace_flush();
    }
} /* grp_trace_flush */


static char* trace_lvl_to_char(int lvl)
{
    static char txt[8];
    txt[1] = 0;

    switch (lvl) {
    case VTSS_TRACE_LVL_ERROR:
        txt[0] = 'E';
        break;
    case VTSS_TRACE_LVL_WARNING:
        txt[0] = 'W';
        break;
    case VTSS_TRACE_LVL_INFO:
        txt[0] = 'I';
        break;
    case VTSS_TRACE_LVL_DEBUG:
        txt[0] = 'D';
        break;
    case VTSS_TRACE_LVL_NOISE:
        txt[0] = 'N';
        break;
    case VTSS_TRACE_LVL_RACKET:
        txt[0] = 'R';
        break;
    default:
        txt[0] = '?';
    }
    return txt;
} /* trace_lvl_to_char */


static void time_str(char *buf)
{
    time_t t = time(NULL);
    struct tm *timeinfo_p;

#if VTSS_SWITCH && defined(VTSS_SW_OPTION_SYSUTIL)
    /* Correct for timezone */
    t += (system_get_tz_off() * 60);
#endif
#ifdef VTSS_SW_OPTION_DAYLIGHT_SAVING
    /* Correct for DST */
    t += (time_dst_get_offset() * 60);
#endif

    timeinfo_p = localtime(&t);

    sprintf(buf, "%02d:%02d:%02d",
            timeinfo_p->tm_hour,
            timeinfo_p->tm_min,
            timeinfo_p->tm_sec);
} /* time_str */


static void trace_grp_init(vtss_trace_grp_t* trace_grp_p)
{
    if (trace_grp_p->lvl == 0) {
        trace_grp_p->lvl    = VTSS_TRACE_LVL_ERROR;
    }
    TRACE_ASSERT((trace_grp_p->flags & VTSS_TRACE_FLAGS_INIT) == 0, ("grp already initialized"));
    trace_grp_p->flags |= VTSS_TRACE_FLAGS_INIT;
} /* trace_grp_init */


static void  trace_grps_init(vtss_trace_grp_t* trace_grp_p, int cnt)
{
    int i;

    for (i = 0; i < cnt; i++) {
        trace_grp_init(&trace_grp_p[i]);
    }
} /* trace_grps_init */


static void trace_threads_init(void)
{
    int i;

    for (i = 0; i <= VTSS_TRACE_MAX_THREAD_ID; i++) {
        trace_threads[i] = trace_thread_default;
    }
} /* trace_threads_init */


/* strip leading path from file */
static const char *trace_filename(const char *fn)
{
    int i, start;
    if(!fn)
        return NULL;
    for (start = 0, i = strlen(fn); i > 0; i--) {
        if (fn[i-1] == '/') {
            start = i;
            break;
        }
    }
    return fn+start;
} /* trace_filename */


/**
 * Compare the trace messages with the pattern we are looking for and dump
 * stack backtrace if there is a hit.
 */
static void vtss_trace_hunt_cmp(vtss_trace_grp_t *trace_grp_p, char *err_buf, const char *fmt, va_list ap)
{
    char b[256];

    vsnprintf(b, 255, fmt, ap);
    b[255] = '\0';
    if (strstr(b, vtss_trace_hunt_target)) {
        grp_trace_printf(trace_grp_p, err_buf, "Trace hunt hit: '%s'\n", vtss_trace_hunt_target);
        // Dump backtrace when target is found.
        misc_thread_status_print(NONBLOCKING_PRINTF, TRUE, TRUE);
    }
} /* vtss_trace_hunt_cmp */


static void trace_msg_prefix(
    /* Generate copy of trace message for storing errors in flash */
    char              *err_buf,
    vtss_trace_reg_t* trace_reg_p,
    vtss_trace_grp_t* trace_grp_p,
    int               grp_idx,
    int               lvl,
    const char        *location,
    uint              line_no)
{
    grp_trace_write_string(trace_grp_p, err_buf, trace_lvl_to_char(lvl)); /* Trace type */
    grp_trace_write_char(trace_grp_p, err_buf, ' ');
    grp_trace_write_string(trace_grp_p, err_buf, trace_reg_p->name);      /* Registration name */

    if (grp_idx > 0) {
        /* Not default group => print group name */
        grp_trace_write_char(trace_grp_p, err_buf, '/');
        grp_trace_write_string(trace_grp_p, err_buf, trace_grp_p->name);
    }
    grp_trace_write_char(trace_grp_p, err_buf, ' ');

    /* Wall clock time stamp hh:mm:ss */
    if (HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_TIMESTAMP) && !HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_IRQ)) {
        /* Calculate current time. This won't work if called from an interrupt handler
         * because time_str() calls time(), which inserts a waiting point. */
        char buf[strlen("hh:mm:ss") + 2];
        time_str(buf);

        grp_trace_printf(trace_grp_p, err_buf, "%s ", &buf[0]);
    }

    /* usec time stamp. Format: ss.mmm,uuu */
    /* Output a usec timestamp if either requested to or users wants a
     * normal timestamp while being marked as a caller from interrupt context */
    if (HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_USEC) || (HAS_FLAGS(trace_grp_p, (VTSS_TRACE_FLAGS_IRQ|VTSS_TRACE_FLAGS_TIMESTAMP)))) {
        u64               usecs, s, m, u;
        vtss_tick_count_t ticks = vtss_current_time() /* May be called from ISR/DSR context */;

        usecs = hal_time_get();

        s = (usecs / 1000000ULL);
        m = (usecs - 1000000ULL * s) / 1000ULL;
        u = (usecs - 1000000ULL * s - 1000ULL * m);
        s %= 100LLU;

        if (HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_IRQ)) {
            grp_trace_printf(trace_grp_p, err_buf, "(" VPRI64u" ticks) %02u.%03u,%03u ", ticks, (u32)s, (u32)m, (u32)u);
        } else {
            grp_trace_printf(trace_grp_p, err_buf, "%02u.%03u,%03u ", (u32)s, (u32)m, (u32)u);
        }
    }

    grp_trace_printf(trace_grp_p, err_buf, "%d/%s#%d: ", (int)vtss_thread_id_get(), trace_filename(location), line_no);
} /* trace_msg_prefix */

static void strn_tolower(char *str, int len)
{
    int i;
    for (i = 0; i < len; i++) {
        str[i] = tolower(str[i]);
    }
} /* str_tolower */

static BOOL str_contains_spaces(char *str)
{
    int i;
    for (i = 0; i < strlen(str); i++) {
        if (str[i] == ' ') return 1;
    }

    return 0;
} /* str_contains_spaces */

static void trace_store_prv_lvl(void)
{
    int mid, gidx;
    int i;

    /* Copy current trace levels to previous */
    for (mid = 0; mid <= MODULE_ID_MAX; mid++) {
        if (trace_regs[mid]) {
            for (gidx = 0; gidx < trace_regs[mid]->grp_cnt; gidx++) {
                trace_regs[mid]->grps[gidx].lvl_prv = trace_regs[mid]->grps[gidx].lvl;
            }
        }
    }

    for (i = 0; i <= VTSS_TRACE_MAX_THREAD_ID; i++) {
        trace_threads[i].lvl_prv = trace_threads[i].lvl;
    }
} /* trace_store_prv_lvl */

/* ===========================================================================
 * Configuration
 * ------------------------------------------------------------------------ */

#if VTSS_SWITCH
/*
   Configuration layout

   Header:
     version:      1 byte
     global_flags: 1 byte
     thread_cnt:   1 byte - default: 0 = No configuration saved
     module_cnt:   1 byte - default: 0 = No configuration saved

     Per thread:
       thread_id    1 byte
       thread_flags 1 byte
       thread_rsv   1 byte
       lvl          1 byte

     Per module:
       module_id    1 byte
       module_flags 1 byte
       module_rsv   1 byte
       grp_cnt      1 byte

       Per group:
         lvl        1 byte
         grp_flags  1 byte
*/

/* Size of each field in bytes */
/* Header */
#define TRACE_CFG_VER_SIZE             1
#define TRACE_CFG_GLOBAL_FLAGS_SIZE    1
#define TRACE_CFG_THREAD_CNT_SIZE      1
#define TRACE_CFG_MODULE_CNT_SIZE      1
#define TRACE_CFG_HDR_SIZE             4 /* Sum */

/* Thread */
#define TRACE_CFG_THREAD_ID_SIZE       1
#define TRACE_CFG_THREAD_FLAGS_SIZE    1
#define TRACE_CFG_THREAD_FLAG_STACKUSE 0x02
#define TRACE_CFG_THREAD_RSV_SIZE      1
#define TRACE_CFG_THREAD_LVL_SIZE      1
#define TRACE_CFG_THREAD_SIZE          4 /* Sum */

/* Module */
#define TRACE_CFG_MODULE_ID_SIZE       1
#define TRACE_CFG_MODULE_FLAGS_SIZE    1
#define TRACE_CFG_MODULE_RSV_SIZE      1
#define TRACE_CFG_MODULE_GRP_CNT_SIZE  1
#define TRACE_CFG_MODULE_SIZE          4 /* Sum */

/* Group */
#define TRACE_CFG_GRP_LVL_SIZE         1
#define TRACE_CFG_GRP_FLAGS_SIZE       1
#define TRACE_CFG_GRP_FLAG_TIMESTAMP   0x01
#define TRACE_CFG_GRP_FLAG_USEC        0x04
#define TRACE_CFG_GRP_FLAG_RINGBUF     0x08
#define TRACE_CFG_GRP_SIZE             2 /* Sum */

/* Offset within header/module/group */
#define TRACE_CFG_VER_OFFSET            0
#define TRACE_CFG_GLOBAL_FLAGS_OFFSET   1
#define TRACE_CFG_THREAD_CNT_OFFSET     2
#define TRACE_CFG_MODULE_CNT_OFFSET     3

#define TRACE_CFG_THREAD_ID_OFFSET      0
#define TRACE_CFG_THREAD_FLAGS_OFFSET   1
#define TRACE_CFG_THREAD_RSV_OFFSET     2
#define TRACE_CFG_THREAD_LVL_OFFSET     3

#define TRACE_CFG_MODULE_ID_OFFSET      0
#define TRACE_CFG_MODULE_FLAGS_OFFSET   1
#define TRACE_CFG_MODULE_RSV_OFFSET     2
#define TRACE_CFG_MODULE_GRP_CNT_OFFSET 3

#define TRACE_CFG_GRP_LVL_OFFSET        0
#define TRACE_CFG_GRP_FLAGS_OFFSET      1


/* Default values */
#define TRACE_CFG_VER_DEFAULT         2

/*-----------------------------------------------------------------------------
 * Trace configuration persistence.
 * 
 * This used to be based on 'conf', but on Linux this means that a good deal
 * of code has to run before 'conf' is ready to deliver the trace config
 * block to us.
 * 
 * Thus, we now use a dedicated file in the file system, and no 'conf' block.
 * We still use the same binary layout, though, as the code for that is BOTH
 * written AND tested. Those are good properties of code...
 * 
 * File file reading may fail in the case where the file system isn't mounted
 * by the time vtss_trace_cfg_wr() is called.
 */

mesa_rc vtss_trace_cfg_erase(void)
{
    /* Create conf. block with size=0 => Erase block. We do this to wipe any
     * legacy conf block that may be laying around. */
    conf_create(CONF_BLK_TRACE, 0);
    conf_close(CONF_BLK_TRACE);

    (void) unlink(TRACE_CONF_FILE_PATH);

    return VTSS_OK;
} /* vtss_trace_cfg_erase */

mesa_rc vtss_trace_cfg_wr(void)
{
    uchar   *conf_p    = NULL;;
    size_t  cfg_size   = TRACE_CFG_HDR_SIZE;
    int     fd         = -1;
    mesa_rc rc         = VTSS_UNSPECIFIED_ERROR;
    int     module_cnt = 0;
    int     m, g, t;
    int     pos;

    T_D("enter");

    for (int m = 0; m < MODULE_ID_MAX+1; m++) {
        if (trace_regs[m] != NULL) {
            module_cnt++;
            cfg_size += TRACE_CFG_MODULE_SIZE;
            cfg_size += trace_regs[m]->grp_cnt*TRACE_CFG_GRP_SIZE;
        }
    }
    cfg_size += VTSS_TRACE_MAX_THREAD_ID*TRACE_CFG_THREAD_SIZE;

    T_D("Creating configuration block CONF_BLK_TRACE=%d of size " VPRIz " bytes",
        CONF_BLK_TRACE, cfg_size);
    if (!(conf_p = (uchar *)VTSS_MALLOC(cfg_size))) {
        T_E("Creation of CONF_BLK_TRACE failed, cfg_size=" VPRIz, cfg_size);
        goto out;
    }

    /* Write configuration */
    /* Header */
    memset(conf_p, 0, cfg_size);
    conf_p[TRACE_CFG_VER_OFFSET]        = TRACE_CFG_VER_DEFAULT;
    conf_p[TRACE_CFG_THREAD_CNT_OFFSET] = VTSS_TRACE_MAX_THREAD_ID;
    conf_p[TRACE_CFG_MODULE_CNT_OFFSET] = module_cnt;
    pos = TRACE_CFG_HDR_SIZE;

    /* Threads */
    for (t = 1; t <= VTSS_TRACE_MAX_THREAD_ID; t++) {
        if (pos + TRACE_CFG_THREAD_SIZE > cfg_size) {
            /* We are about to write beyond the cfg_size?! */
            T_E("pos=%d, cfg_size=" VPRIz ", t=%d", pos, cfg_size, t);
            goto out;
        }

        T_D("Writing trace settings for thread_id=%d, pos=%d", t, pos);
        conf_p[pos + TRACE_CFG_THREAD_ID_OFFSET] = t;
        conf_p[pos + TRACE_CFG_THREAD_FLAGS_OFFSET] =
            (trace_threads[t].stackuse ? TRACE_CFG_THREAD_FLAG_STACKUSE : 0);
        conf_p[pos + TRACE_CFG_THREAD_LVL_OFFSET] =
            trace_threads[t].lvl;

        pos += TRACE_CFG_THREAD_SIZE;
    }

    /* Modules */
    for (m = 0; m < MODULE_ID_MAX+1; m++) {
        if (!trace_regs[m]) {
            continue;
        }

        if (pos + TRACE_CFG_MODULE_SIZE > cfg_size) {
            /* We are about to write beyond the cfg_size?! */
            T_E("pos=%d, cfg_size=" VPRIz ", m=%d", pos, cfg_size, m);
            goto out;
        }

        conf_p[pos + TRACE_CFG_MODULE_ID_OFFSET]      = trace_regs[m]->module_id;
        conf_p[pos + TRACE_CFG_MODULE_GRP_CNT_OFFSET] = trace_regs[m]->grp_cnt;
        pos += TRACE_CFG_MODULE_SIZE;

        T_D("Writing trace settings for module_id=%d (grp_cnt=%d), pos=%d",
            m, trace_regs[m]->grp_cnt, pos);

        for (g = 0; g < trace_regs[m]->grp_cnt; g++) {
            const vtss_trace_grp_t *grp = &trace_regs[m]->grps[g];
            if (pos + TRACE_CFG_GRP_SIZE > cfg_size) {
                T_E("pos=%d, cfg_size=" VPRIz ", m=%d, g=%d", pos, cfg_size, m, g);
                goto out;
            }

            conf_p[pos + TRACE_CFG_GRP_LVL_OFFSET]   = grp->lvl;
            conf_p[pos + TRACE_CFG_GRP_FLAGS_OFFSET] |=
                    ((HAS_FLAGS(grp, VTSS_TRACE_FLAGS_TIMESTAMP) ? TRACE_CFG_GRP_FLAG_TIMESTAMP : 0) |
                     (HAS_FLAGS(grp, VTSS_TRACE_FLAGS_USEC)      ? TRACE_CFG_GRP_FLAG_USEC      : 0) |
                     (HAS_FLAGS(grp, VTSS_TRACE_FLAGS_RINGBUF)   ? TRACE_CFG_GRP_FLAG_RINGBUF   : 0));

            pos += TRACE_CFG_GRP_SIZE;
        }
    }

    TRACE_ASSERT(pos == cfg_size, ("pos=%d, cfg_size=" VPRIz, pos, cfg_size));
    T_D("Completed building trace configuration. pos=%d, cfg_size=" VPRIz, pos, cfg_size);

    if ((fd = open(TRACE_CONF_FILE_PATH, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR)) < 0) {
        T_E("Creation of persistent trace config file %s failed", TRACE_CONF_FILE_PATH);
        goto out;
    }

    if (write(fd, conf_p, cfg_size) != cfg_size) {
        T_E("Failed to write all of " VPRIz " bytes to %s", cfg_size, TRACE_CONF_FILE_PATH);
        (void) unlink(TRACE_CONF_FILE_PATH);
        goto out;
    }

    rc = VTSS_OK;

out:
    if (fd >= 0) {
        (void) close(fd);
    }
    if (conf_p != NULL) {
        VTSS_FREE(conf_p);
    }

    return rc;
} /* vtss_trace_cfg_wr */

/*
   Read configuration from flash.
*/
mesa_rc vtss_trace_cfg_rd(void)
{
    mesa_rc     rc         = VTSS_UNSPECIFIED_ERROR;
    uchar       *conf_p    = NULL;
    size_t      cfg_size;
    int         thread_cnt;
    int         module_cnt;
    int         thread_id;
    int         module_id;
    int         grp_cnt;
    int         m, g, t;
    int         pos;
    int         fd;
    struct stat statbuf;

    T_D("enter");

    if ((fd = open(TRACE_CONF_FILE_PATH, O_RDONLY)) < 0) {
        T_D("no conf file");
        goto out;
    }
    if (fstat(fd, &statbuf) < 0) {
        T_D("stat fail");
        goto out;
    }

    cfg_size = statbuf.st_size;

    if (cfg_size > 2*1024) {
        T_W("Unreasonable size of trace config file: " VPRIz, cfg_size);
        goto out;
    }

    if (!(conf_p = (uchar *)VTSS_MALLOC(cfg_size))) {
        T_D("malloc fail");
        goto out;
    }

    if (read(fd, conf_p, cfg_size) != cfg_size) {
        T_D("read fail");
        goto out;
    }

    if (conf_p[TRACE_CFG_MODULE_CNT_OFFSET]) {
        /* Check version */
        if (conf_p[TRACE_CFG_VER_OFFSET] != TRACE_CFG_VER_DEFAULT) {
            T_W("Unexpected version: %d", conf_p[TRACE_CFG_VER_OFFSET]);
            goto out;
        }

        /* Copy settings from flash */
        module_cnt = conf_p[TRACE_CFG_MODULE_CNT_OFFSET];
        thread_cnt = conf_p[TRACE_CFG_THREAD_CNT_OFFSET];

        pos = TRACE_CFG_HDR_SIZE;

        for (t = 0; t < MIN(thread_cnt, VTSS_TRACE_MAX_THREAD_ID); t++) {
            thread_id = conf_p[pos + TRACE_CFG_THREAD_ID_OFFSET];

            T_D("Reading trace settings for thread_id=%d, pos=%d",
                thread_id, pos);

            if (thread_id > 0 && thread_id < VTSS_TRACE_MAX_THREAD_ID) {
                trace_threads[thread_id].stackuse =
                    ((conf_p[pos + TRACE_CFG_THREAD_FLAGS_OFFSET] &
                      TRACE_CFG_THREAD_FLAG_STACKUSE) != 0);
                trace_threads[thread_id].lvl = conf_p[pos + TRACE_CFG_THREAD_LVL_OFFSET];
            }

            pos += TRACE_CFG_THREAD_SIZE;
        }


        for (m = 0; m < module_cnt; m++) {
            module_id = conf_p[pos + TRACE_CFG_MODULE_ID_OFFSET];
            grp_cnt   = conf_p[pos + TRACE_CFG_MODULE_GRP_CNT_OFFSET];
            pos += TRACE_CFG_MODULE_SIZE;

            T_D("Reading trace settings for module_id=%d (grp_cnt=%d), pos=%d", module_id, grp_cnt, pos);

            if (module_id >= 0                &&
                module_id < ARRSZ(trace_regs) &&
                trace_regs[module_id] != NULL &&
                trace_regs[module_id]->grp_cnt == grp_cnt) {
                /* Module registered and group count matches */
                for (g = 0; g < grp_cnt; g++) {
                    vtss_trace_grp_t *grp = &trace_regs[module_id]->grps[g];
                    grp->lvl = conf_p[pos + TRACE_CFG_GRP_LVL_OFFSET];
                    if ((conf_p[pos + TRACE_CFG_GRP_FLAGS_OFFSET] & TRACE_CFG_GRP_FLAG_TIMESTAMP) != 0)
                        grp->flags |= VTSS_TRACE_FLAGS_TIMESTAMP;
                    if ((conf_p[pos + TRACE_CFG_GRP_FLAGS_OFFSET] & TRACE_CFG_GRP_FLAG_USEC) != 0)
                        grp->flags |= VTSS_TRACE_FLAGS_USEC;
                    if ((conf_p[pos + TRACE_CFG_GRP_FLAGS_OFFSET] & TRACE_CFG_GRP_FLAG_RINGBUF) != 0)
                        grp->flags |= VTSS_TRACE_FLAGS_RINGBUF;
                    pos += TRACE_CFG_GRP_SIZE;
                }
            } else {
                T_W("Unknown module or missing group count match: module_id=%d, grp_cnt=%d", module_id, grp_cnt);
                pos += grp_cnt*TRACE_CFG_GRP_SIZE;
            }
        }

        if (pos != cfg_size) {
            T_E("pos=%d, cfg_size=" VPRIz, pos, cfg_size);
            goto out;
        }

        T_D("Completed reading configuration, pos=%d, cfg_size=" VPRIz, pos, cfg_size);
        rc = VTSS_RC_OK;
    }

 out:
    if (fd >= 0) {
        (void) close(fd);
    }
    if (conf_p != NULL) {
        VTSS_FREE(conf_p);
    }

    return rc;
} /* vtss_trace_cfg_rd */

#endif /* VTSS_SWITCH */

static void trace_to_flash(vtss_trace_grp_t *trace_grp_p, char *err_buf)
{
#if VTSS_SWITCH
    if (err_buf) {
        if (HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_IRQ) || HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_RINGBUF)) {
            // The error might never be seen if it goes into the ring buffer
            // Use a function that will print on the UART even in DSR/IRQ context.
            NONBLOCKING_PRINTF("%s", err_buf);
        } else if (!HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_IRQ)) {
            /* Don't write to flash if caller is in ISR/DSR context. */
#if defined(VTSS_SW_OPTION_SYSLOG)
            syslog_flash_log(SYSLOG_CAT_DEBUG, VTSS_APPL_SYSLOG_LVL_ERROR, err_buf);
#endif /* defined(VTSS_SW_OPTION_SYSLOG) */
        }

        // It's nice to also get a stack backtrace of the running thread
        // when a trace error occurs.
        misc_thread_status_print(NONBLOCKING_PRINTF, TRUE, TRUE);
    }
#endif /* VTSS_SWITCH */
}

/* ======================================================================== */

/* ---------------------------------------------------------------------------
 * Internal functions
 * ######################################################################## */



/* ###########################################################################
 * API functions
 * ------------------------------------------------------------------------ */

void vtss_trace_port_set(int port_index,BOOL trace_disabled) {
    vtss_trace_port_list[port_index] = trace_disabled;
}

BOOL vtss_trace_port_get(int port_index) {
    return vtss_trace_port_list[port_index];
}

void vtss_trace_reg_init(vtss_trace_reg_t* trace_reg_p, vtss_trace_grp_t* trace_grp_p, int grp_cnt)
{
    if (!trace_init_done) {
        /* Initialize and register trace ressources */
        trace_init_done = 1;
        trace_threads_init();
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);

#if VTSS_TRACE_MULTI_THREAD
        /* Create critical region variables */
        vtss_sem_init(&trace_io_crit, 1);

        /* Ring-buf protection is handled through vtss_global_lock/unlock()
           calls because the caller may be calling from a locked context. */
        vtss_sem_init(&trace_rb_crit, 1);
#endif /* VTSS_TRACE_MULTI_THREAD */
    }

    TRACE_ASSERT(trace_reg_p->flags == 0, ("trace_reg_p->flags!=0"));
    trace_reg_p->flags = VTSS_TRACE_FLAGS_INIT;

    trace_grps_init(trace_grp_p, grp_cnt);
    trace_reg_p->grp_cnt = grp_cnt;
    trace_reg_p->grps    = trace_grp_p;
} /* vtss_trace_reg_init */


mesa_rc vtss_trace_register(vtss_trace_reg_t* trace_reg_p)
{
    int i;

    TRACE_ASSERT(trace_reg_p != NULL, ("trace_reg_p=NULL"));
    TRACE_ASSERT(trace_reg_p->module_id <= MODULE_ID_MAX,
                 ("module_id too large: %d", trace_reg_p->module_id));
    TRACE_ASSERT(trace_reg_p->module_id >= 0,
                 ("module_id too small: %d", trace_reg_p->module_id));


    /* Make sure to zero-terminate name and description string */
    trace_reg_p->name[VTSS_TRACE_MAX_NAME_LEN]   = 0;
    trace_reg_p->descr[VTSS_TRACE_MAX_DESCR_LEN] = 0;

    TRACE_ASSERT(trace_regs[trace_reg_p->module_id] == NULL,
                 ("module_id %d already registered:\n"
                  "Name of 1st registration: %s\n"
                  "Name of 2nd registration: %s",
                  trace_reg_p->module_id,
                  trace_regs[trace_reg_p->module_id]->name,
                  trace_reg_p->name));
    TRACE_ASSERT(trace_reg_p->grps != NULL,
                 ("No groups defined (at least one required), module_id=%d",
                  trace_reg_p->module_id));
    TRACE_ASSERT(trace_init_done,
                 ("trace_init_done=%d", trace_init_done));

    /* Check that registration name contains no spaces */
    TRACE_ASSERT(!str_contains_spaces(trace_reg_p->name),
                 ("Registration name contains spaces.\n"
                  "name: \"%s\"", trace_reg_p->name));

    /* Check that registration name is unique */
    for (i = 0; i <= MODULE_ID_MAX; i++) {
        if (trace_regs[i]) {
            TRACE_ASSERT(strcmp(trace_regs[i]->name, trace_reg_p->name),
                         ("Registration name is not unique. "
                          "Registrations %d and %d are both named \"%s\".",
                          i, trace_reg_p->module_id, trace_reg_p->name));
        }
    }

    /* Check group definitions */
    for (i = 0; i < trace_reg_p->grp_cnt; i++) {
        int j;

        /* Make sure to zero-terminate name and description string */
        trace_reg_p->grps[i].name[VTSS_TRACE_MAX_NAME_LEN]   = 0;
        trace_reg_p->grps[i].descr[VTSS_TRACE_MAX_DESCR_LEN] = 0;

        /* Check that group name contains no spaces */
        TRACE_ASSERT(!str_contains_spaces(trace_reg_p->grps[i].name),
                     ("Group name contains spaces.\n"
                      "module: %s\n"
                      "group: \"%s\"",
                      trace_reg_p->name,
                      trace_reg_p->grps[i].name));

        /* Check that group names within registration are unique */
        for (j = 0; j < trace_reg_p->grp_cnt; j++) {
            if (j != i) {
                TRACE_ASSERT(strcmp(trace_reg_p->grps[i].name,
                                    trace_reg_p->grps[j].name),
                             ("Group names are not unique for registration #%d (\"%s\"): "
                              "Two groups are named \"%s\".",
                              trace_reg_p->module_id, trace_reg_p->name,
                              trace_reg_p->grps[i].name));
            }
        }
    }

    /* Set lvl_prv to initial value */
    for (i = 0; i < trace_reg_p->grp_cnt; i++) {
        trace_reg_p->grps[i].lvl_prv = trace_reg_p->grps[i].lvl;
    }

    /* Convert module and group name to lowercase */
    str_tolower(trace_reg_p->name);
    for (i = 0; i < trace_reg_p->grp_cnt; i++) {
        str_tolower(trace_reg_p->grps[i].name);
    }

    trace_regs[trace_reg_p->module_id] = trace_reg_p;

    return VTSS_OK;
} /* vtss_trace_register */

vtss_trace_reg_t * vtss_trace_reg(int module_id, int grp_idx) {
    if (module_id < 0 || module_id >= VTSS_MODULE_ID_NONE) {
        return 0;
    }

    return trace_regs[module_id];
}

vtss_trace_grp_t * vtss_trace_grp(int module_id, int grp_idx) {
    vtss_trace_reg_t *trace_reg_p;

    if (module_id < 0 || module_id >= VTSS_MODULE_ID_NONE) {
        return 0;
    }

    trace_reg_p = trace_regs[module_id];

    if (!trace_reg_p) {
        return 0;
    }

    if (grp_idx >= trace_reg_p->grp_cnt) {
        return 0;
    }

    return &trace_reg_p->grps[grp_idx];
}

void vtss_trace_vprintf(int module_id,
                        int grp_idx,
                        int lvl,
                        const char *location,
                        uint line_no,
                        const char *fmt,
                        va_list args)
{
    vtss_trace_grp_t *trace_grp_p;
    vtss_trace_reg_t *trace_reg_p;
    char             *err_buf = NULL;

    trace_reg_p = trace_regs[module_id];
    if (trace_reg_p == NULL || &trace_reg_p->grps[grp_idx] == NULL) {
        trace_reg_p = trace_regs[VTSS_MODULE_ID_TRACE];
        if (trace_reg_p == NULL) { // There is no TRACE modules at the moment ????
            PRINTE(("module_id:%d, module_name:%s, grp_idx:%d, location: %s  line: %u\n", module_id, vtss_module_names[module_id], grp_idx, location, line_no));
            return;
        }
        TRACE_ASSERT(FALSE, ("trace_reg_p is NULL for module_id=%d %s#%d", module_id, location, line_no));
        return;
    }

    /* Get module's registration */
    TRACE_ASSERT(trace_reg_p != NULL, ("module_id=%d %s#%d", module_id, location, line_no));

    /* Get group pointer */
    trace_grp_p = &trace_reg_p->grps[grp_idx];
    TRACE_ASSERT(trace_grp_p != NULL, ("module_id=%d", module_id));

#if defined(_notdef_)           /* Check performed at outer level - see T_xxx() macros */
    /* Return if trace not enabled at this level for the group */
    if (lvl < trace_grp_p->lvl || lvl >= VTSS_TRACE_LVL_NONE) {
        return;
    }

    if (lvl < global_lvl) {
        return;
    }
#endif

    /* Check that level appears to be one of the well-defined ones */
    TRACE_ASSERT(
        lvl == VTSS_TRACE_LVL_NONE    ||
        lvl == VTSS_TRACE_LVL_ERROR   ||
        lvl == VTSS_TRACE_LVL_WARNING ||
        lvl == VTSS_TRACE_LVL_INFO    ||
        lvl == VTSS_TRACE_LVL_DEBUG   ||
        lvl == VTSS_TRACE_LVL_NOISE   ||
        lvl == VTSS_TRACE_LVL_RACKET,
        ("Unknown trace level used in %s#%d: lvl=%d",
         location, line_no, lvl));

    if (lvl == VTSS_TRACE_LVL_ERROR) {
#if VTSS_SWITCH
        if (!HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_IRQ)) {
            /* Don't set LED from ISR/DSR context. */
            led_front_led_state(LED_FRONT_LED_ERROR, FALSE);
        }
#endif /* VTSS_SWITCH */

        /* Allocate buffer in which to build error message for flash */
        if ((VTSS_MALLOC_CAST(err_buf, TRACE_ERR_BUF_SIZE))) {
            err_buf[0] = 0;
        }
    }

    /* Print prefix */
    trace_msg_prefix(err_buf,
                     trace_reg_p,
                     trace_grp_p,
                     grp_idx,
                     lvl,
                     location,
                     line_no);

    /* If error or warning add prefix which runtc can identify */
    if (lvl == VTSS_TRACE_LVL_ERROR) {
        grp_trace_printf(trace_grp_p, err_buf, "Error: ");
    }
    if (lvl == VTSS_TRACE_LVL_WARNING) {
        grp_trace_printf(trace_grp_p, err_buf, "Warning: ");
    }

    /* Print message */
    grp_trace_vprintf(trace_grp_p, err_buf, fmt, args);

    grp_trace_write_char(trace_grp_p, err_buf, '\n');
    grp_trace_flush(trace_grp_p);

    if (vtss_trace_hunt_target) {
        vtss_trace_hunt_cmp(trace_grp_p, err_buf, fmt, args);
    }

    trace_to_flash(trace_grp_p, err_buf);
    if (err_buf) {
        VTSS_FREE(err_buf);
    }
} /* vtss_trace_vprintf */

void vtss_trace_var_printf_impl(int module_id,
                        int grp_idx,
                        int lvl,
                        const char *location,
                        uint line_no,
                        const char* data,
                        size_t len) {
    vtss_trace_grp_t *trace_grp_p;
    vtss_trace_reg_t *trace_reg_p;
    char             *err_buf = NULL;

    trace_reg_p = trace_regs[module_id];
    if (trace_reg_p == NULL || &trace_reg_p->grps[grp_idx] == NULL) {
        trace_reg_p = trace_regs[VTSS_MODULE_ID_TRACE];
        if (trace_reg_p == NULL) { // There is no TRACE modules at the moment ????
            PRINTE(("module_id:%d, module_name:%s, grp_idx:%d, location: %s  line: %u\n", module_id, vtss_module_names[module_id], grp_idx, location, line_no));
            return;
        }
        TRACE_ASSERT(FALSE, ("trace_reg_p is NULL for module_id=%d %s#%d", module_id, location, line_no));
        return;
    }

    /* Get module's registration */
    TRACE_ASSERT(trace_reg_p != NULL, ("module_id=%d %s#%d", module_id, location, line_no));

    /* Get group pointer */
    trace_grp_p = &trace_reg_p->grps[grp_idx];
    TRACE_ASSERT(trace_grp_p != NULL, ("module_id=%d", module_id));

#if defined(_notdef_)           /* Check performed at outer level - see T_xxx() macros */
    /* Return if trace not enabled at this level for the group */
    if (lvl < trace_grp_p->lvl || lvl >= VTSS_TRACE_LVL_NONE) {
        return;
    }

    if (lvl < global_lvl) {
        return;
    }
#endif

    /* Check that level appears to be one of the well-defined ones */
    TRACE_ASSERT(
        lvl == VTSS_TRACE_LVL_NONE    ||
        lvl == VTSS_TRACE_LVL_ERROR   ||
        lvl == VTSS_TRACE_LVL_WARNING ||
        lvl == VTSS_TRACE_LVL_INFO    ||
        lvl == VTSS_TRACE_LVL_DEBUG   ||
        lvl == VTSS_TRACE_LVL_NOISE   ||
        lvl == VTSS_TRACE_LVL_RACKET,
        ("Unknown trace level used in %s#%d: lvl=%d",
         location, line_no, lvl));

    if (lvl == VTSS_TRACE_LVL_ERROR) {
#if VTSS_SWITCH
        if (!HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_IRQ)) {
            /* Don't set LED from ISR/DSR context. */
            led_front_led_state(LED_FRONT_LED_ERROR, FALSE);
        }
#endif /* VTSS_SWITCH */

        /* Allocate buffer in which to build error message for flash */
        if ((VTSS_MALLOC_CAST(err_buf, TRACE_ERR_BUF_SIZE))) {
            err_buf[0] = 0;
        }
    }

    /* Print prefix */
    trace_msg_prefix(err_buf,
                     trace_reg_p,
                     trace_grp_p,
                     grp_idx,
                     lvl,
                     location,
                     line_no);

    /* If error or warning add prefix which runtc can identify */
    if (lvl == VTSS_TRACE_LVL_ERROR) {
        grp_trace_printf(trace_grp_p, err_buf, "Error: ");
    }
    if (lvl == VTSS_TRACE_LVL_WARNING) {
        grp_trace_printf(trace_grp_p, err_buf, "Warning: ");
    }

    /* Print message */
    grp_trace_printf(trace_grp_p, err_buf, data, len);

    grp_trace_write_char(trace_grp_p, err_buf, '\n');
    grp_trace_flush(trace_grp_p);

    // if (vtss_trace_hunt_target) {
    //     vtss_trace_hunt_cmp(trace_grp_p, err_buf, fmt, args);
    // }

    trace_to_flash(trace_grp_p, err_buf);
    if (err_buf) {
        VTSS_FREE(err_buf);
    }
}

void vtss_trace_printf(int module_id,
                       int grp_idx,
                       int lvl,
                       const char *location,
                       uint line_no,
                       const char *fmt,
                       ...)
{
    va_list args;
    va_start(args, fmt);
    vtss_trace_vprintf(module_id, grp_idx, lvl, location, line_no, fmt, args);
    va_end(args);
} /* vtss_trace_printf */

void vtss_trace_hex_dump(int module_id,
                         int grp_idx,
                         int lvl,
                         const char *location,
                         uint line_no,
                         const uchar *byte_p,
                         int  byte_cnt)
{
    int i = 0;

    vtss_trace_grp_t*  trace_grp_p;
    vtss_trace_reg_t*  trace_reg_p;
    char          *err_buf = NULL;

    trace_reg_p = trace_regs[module_id];
    TRACE_ASSERT(trace_reg_p != NULL, ("module_id=%d", module_id));

    trace_grp_p = &trace_reg_p->grps[grp_idx];
    TRACE_ASSERT(trace_grp_p != NULL, ("module_id=%d", module_id));

    if (lvl < trace_grp_p->lvl ||
        lvl >= VTSS_TRACE_LVL_NONE) {
        return;
    }

    /* Check that level appears to be one of the well-defined ones */
    TRACE_ASSERT(
        lvl == VTSS_TRACE_LVL_NONE    ||
        lvl == VTSS_TRACE_LVL_ERROR   ||
        lvl == VTSS_TRACE_LVL_WARNING ||
        lvl == VTSS_TRACE_LVL_INFO    ||
        lvl == VTSS_TRACE_LVL_DEBUG   ||
        lvl == VTSS_TRACE_LVL_NOISE   ||
        lvl == VTSS_TRACE_LVL_RACKET,
        ("Unknown trace level used in %s#%d: lvl=%d",
         location, line_no, lvl));

    if (lvl == VTSS_TRACE_LVL_ERROR) {
#if VTSS_SWITCH
        if (!HAS_FLAGS(trace_grp_p, VTSS_TRACE_FLAGS_IRQ)) {
            /* Don't set LED from ISR/DSR context. */
            led_front_led_state(LED_FRONT_LED_ERROR, FALSE);
        }
#endif /* VTSS_SWITCH */

        /* Allocate buffer in which to build error message for flash */
        if ((VTSS_MALLOC_CAST(err_buf, TRACE_ERR_BUF_SIZE))) {
            err_buf[0] = 0;
        }
    }

    trace_msg_prefix(err_buf,
                     trace_reg_p,
                     trace_grp_p,
                     grp_idx,
                     lvl,
                     location,
                     line_no);
    if (byte_cnt > 16) { grp_trace_write_char(trace_grp_p, err_buf, '\n'); }

    i = 0;
    while (i < byte_cnt) {
        int j = 0;
        grp_trace_printf(trace_grp_p, err_buf, "%p/%04x: ", byte_p + i, i);
        while (j+i < byte_cnt && j < 16) {
            grp_trace_write_char(trace_grp_p, err_buf, ' ');
            grp_trace_printf(trace_grp_p, err_buf, "%02x", byte_p[i+j]);
            j++;
        }
        grp_trace_write_char(trace_grp_p, err_buf, '\n');
        i += 16;
    }
    grp_trace_flush(trace_grp_p);

    trace_to_flash(trace_grp_p, err_buf);
    if (err_buf) {
        VTSS_FREE(err_buf);
    }
} /* vtss_trace_hex_dump */

/* ===========================================================================
 * Management functions
 * ------------------------------------------------------------------------ */

extern "C" int util_icli_cmd_register();

#if VTSS_SWITCH
/* Initialization function for managed build */
mesa_rc vtss_trace_init(vtss_init_data_t *data)
{
    mesa_rc rc = VTSS_OK;

    switch (data->cmd) {
    case INIT_CMD_EARLY_INIT:
        vtss_basics_trace_register();
        break;

    case INIT_CMD_INIT:
        /* Read configuration from flash (if present) */
        vtss_trace_cfg_rd();
        util_icli_cmd_register();
        break;

    case INIT_CMD_START:
        /* Resume thread */
        T_D("enter, cmd=START");
        break;

    case INIT_CMD_CONF_DEF:
        /* Create, save and activate default configuration */
        T_D("enter, cmd=CONF_DEF, isid: %u, flags: 0x%08x", data->isid, data->flags);
        vtss_trace_cfg_erase();
        break;

    default:
        break;
    }

    return rc;
} /* vtss_trace_init */

#endif /* VTSS_SWITCH */


void vtss_trace_hunt_set(char *tgt)
{
    char *clone = NULL;

    if (tgt && strlen(tgt) && VTSS_MALLOC_CAST(clone, strlen(tgt) + 1)) {
        strcpy(clone, tgt);
    }

    if (vtss_trace_hunt_target) {
        VTSS_FREE(vtss_trace_hunt_target);
    }

    vtss_trace_hunt_target = clone;
} /* vtss_trace_hunt_set */


void vtss_trace_module_lvl_set(int module_id, int grp_idx, int lvl)
{
    int i, j;
    int module_id_start=0, module_id_stop=MODULE_ID_MAX;
    int grp_idx_start=0, grp_idx_stop=0;

    if (module_id != -1) {
        module_id_start = module_id;
        module_id_stop  = module_id;

        if (trace_regs[module_id] == NULL) {
            T_E("No registration for module_id=%d", module_id);
            return;
        }
    } else {
        /* Always wildcard group if module is wildcarded */
        grp_idx         = -1;
    }

    if (grp_idx != -1) {
        grp_idx_start = grp_idx;
        grp_idx_stop = grp_idx;

        if (grp_idx > trace_regs[module_id]->grp_cnt-1) {
            T_E("grp_idx=%d too big for module_id=%d (grp_cnt=%d)",
                grp_idx, module_id, trace_regs[module_id]->grp_cnt);
            return;
        }
    }

    /* Store current trace levels before changing */
    trace_store_prv_lvl();

    for (i = module_id_start; i <= module_id_stop; i++) {
        if (trace_regs[i] == NULL)
            continue;
        if (grp_idx == -1) {
            grp_idx_start = 0;
            grp_idx_stop = trace_regs[i]->grp_cnt-1;
        }
        for (j = grp_idx_start; j <= grp_idx_stop; j++) {
            trace_regs[i]->grps[j].lvl = lvl;
        }
    }
} /* vtss_trace_module_lvl_set */


void vtss_trace_module_parm_set(
    vtss_trace_module_parm_t parm,
    int module_id, int grp_idx, BOOL enable)
{
    int i, j;
    int module_id_start=0, module_id_stop=MODULE_ID_MAX;
    int grp_idx_start=0, grp_idx_stop=0;
    u32 flag = 0;

    if (module_id != -1) {
        module_id_start = module_id;
        module_id_stop  = module_id;

        if (trace_regs[module_id] == NULL) {
            vtss_backtrace(printf, 0);
            T_E("No registration for module_id=%d", module_id);
            return;
        }
    } else {
        /* Always wildcard group if module is wildcarded */
        grp_idx         = -1;
    }

    if (grp_idx != -1) {
        grp_idx_start = grp_idx;
        grp_idx_stop = grp_idx;

        if (grp_idx > trace_regs[module_id]->grp_cnt-1) {
            T_E("grp_idx=%d too big for module_id=%d (grp_cnt=%d)",
                grp_idx, module_id, trace_regs[module_id]->grp_cnt);
            return;
        }
    }

    switch (parm) {
        case VTSS_TRACE_MODULE_PARM_TIMESTAMP:
            flag = VTSS_TRACE_FLAGS_TIMESTAMP;
            break;
        case VTSS_TRACE_MODULE_PARM_USEC:
            flag = VTSS_TRACE_FLAGS_USEC;
            break;
        case VTSS_TRACE_MODULE_PARM_RINGBUF:
            flag = VTSS_TRACE_FLAGS_RINGBUF;
            break;
        default:
            T_E("Unknown parm: %d", parm);
            return;
    }

    for (i = module_id_start; i <= module_id_stop; i++) {
        if (trace_regs[i] == NULL)
            continue;
        if (grp_idx == -1) {
            grp_idx_start = 0;
            grp_idx_stop = trace_regs[i]->grp_cnt-1;
        }
        for (j = grp_idx_start; j <= grp_idx_stop; j++) {
            vtss_trace_grp_t *grp = &trace_regs[i]->grps[j];
            if (enable) {
                grp->flags |= flag;
            } else {
                grp->flags &= ~flag;
            }
        }
    }
} /* vtss_trace_module_parm_set */


void vtss_trace_lvl_revert(void)
{
    int mid, gidx;
    int lvl;
    int i;

    for (mid = 0; mid <= MODULE_ID_MAX; mid++) {
        if (trace_regs[mid]) {
            for (gidx = 0; gidx < trace_regs[mid]->grp_cnt; gidx++) {
                lvl = trace_regs[mid]->grps[gidx].lvl;
                trace_regs[mid]->grps[gidx].lvl     = trace_regs[mid]->grps[gidx].lvl_prv;
                trace_regs[mid]->grps[gidx].lvl_prv = lvl;
            }
        }
    }

    for (i = 0; i <= VTSS_TRACE_MAX_THREAD_ID; i++) {
        lvl = trace_threads[i].lvl;
        trace_threads[i].lvl = trace_threads[i].lvl_prv;
        trace_threads[i].lvl_prv = lvl;
    }
} /* vtss_trace_lvl_revert */


void vtss_trace_global_lvl_set(int lvl)
{
    global_lvl = lvl;
} // vtss_trace_global_lvl_set


int vtss_trace_global_lvl_get(void)
{
    return global_lvl;
} // vtss_trace_global_lvl_set

int vtss_trace_module_lvl_get(int module_id, int grp_idx)
{
    if (trace_regs[module_id] == NULL) {
        vtss_backtrace(printf, 0);
        T_E("No registration for module_id=%d", module_id);
        return 0;
    }
    if (grp_idx > trace_regs[module_id]->grp_cnt-1) {
        T_E("grp_idx=%d too big for module_id=%d (grp_cnt=%d)",
            grp_idx, module_id, trace_regs[module_id]->grp_cnt);
        return 0;
    }

    return trace_regs[module_id]->grps[grp_idx].lvl;
} /* vtss_trace_module_lvl_get */


int vtss_trace_global_module_lvl_get(int module_id, int grp_idx)
{
    if (trace_regs[module_id] == NULL) {
        // Add two exceptions here:
        // 1) VTSS_MODULE_ID_BASICS, because it is used during construction of static objects.
        // 2) VTSS_MODULE_ID_MAIN, because it is used when compiling with debug whenever VTSS_MALLOC() and friends are invoked by 1).
        if (module_id != VTSS_MODULE_ID_BASICS && module_id != VTSS_MODULE_ID_MAIN) {
            vtss_backtrace(printf, 0);
            PRINTE(("No registration for module_id=%d (%s)", module_id, vtss_module_names[module_id]));
        }
        return VTSS_TRACE_LVL_NONE;
    }

    if (grp_idx > trace_regs[module_id]->grp_cnt-1) {
        T_E("grp_idx=%d too big for module %s, (id=%d) (grp_cnt=%d)",
            grp_idx, vtss_module_names[module_id],  module_id, trace_regs[module_id]->grp_cnt);
        return 0;
    }

    return
        (global_lvl > trace_regs[module_id]->grps[grp_idx].lvl) ?
         global_lvl : trace_regs[module_id]->grps[grp_idx].lvl;
} /* vtss_trace_global_module_lvl_get */

char *vtss_trace_module_name_get_nxt(int *module_id_p)
{
    int i;

    TRACE_ASSERT(*module_id_p >= -1, ("*module_id_p=%d", *module_id_p));

    for (i = *module_id_p + 1; i < MODULE_ID_MAX; i++) {
        if (trace_regs[i]) {
            *module_id_p = i;
            return trace_regs[i]->name;
        }
    }

    return NULL;
} /* vtss_trace_module_name_get_nxt */


char *vtss_trace_grp_name_get_nxt(int module_id, int *grp_idx_p)
{
    TRACE_ASSERT(*grp_idx_p >= -1, (" "));

    if (!trace_regs[module_id]) return NULL;
    if (*grp_idx_p + 1 >= trace_regs[module_id]->grp_cnt) return NULL;

    (*grp_idx_p)++;

    /* Check flags */
    if (trace_regs[module_id]->flags != VTSS_TRACE_FLAGS_INIT) {
        T_E("module_id=%d: Flags=0x%08x, expected 0x%08x",
            module_id,
            trace_regs[module_id]->flags,
            VTSS_TRACE_FLAGS_INIT);
    }
    if (!(trace_regs[module_id]->grps[*grp_idx_p].flags & VTSS_TRACE_FLAGS_INIT)) {
        T_E("module_id=%d, grp_idx=%d: Flags=0x%08x, expected 0x%08x",
            module_id,
            *grp_idx_p,
            trace_regs[module_id]->grps[*grp_idx_p].flags,
            VTSS_TRACE_FLAGS_INIT);
    }

    return trace_regs[module_id]->grps[*grp_idx_p].name;
} /* vtss_trace_grp_name_get_nxt */


char *vtss_trace_module_get_descr(int module_id)
{
    if (!trace_regs[module_id]) return NULL;

    return trace_regs[module_id]->descr;
} /* vtss_trace_module_get_descr */


char *vtss_trace_grp_get_descr(int module_id, int grp_idx)
{
    if (!trace_regs[module_id]) return NULL;
    if (grp_idx > trace_regs[module_id]->grp_cnt-1) return NULL;

    return trace_regs[module_id]->grps[grp_idx].descr;
} /* vtss_trace_grp_get_descr */


BOOL vtss_trace_grp_get_parm(
    vtss_trace_module_parm_t parm,
    int module_id, int grp_idx)
{
    const vtss_trace_grp_t *grp;
    if (!trace_regs[module_id]) {
        T_E("trace_regs[module_id]=null");
        return 0;
    }
    if (grp_idx > trace_regs[module_id]->grp_cnt-1) {
        T_E("grp_idx=%d, grp_cnt=%d",
            grp_idx, trace_regs[module_id]->grp_cnt);
        return 0;
    }

    grp = &trace_regs[module_id]->grps[grp_idx];
    switch (parm) {
    case VTSS_TRACE_MODULE_PARM_TIMESTAMP:
        return !!HAS_FLAGS(grp, VTSS_TRACE_FLAGS_TIMESTAMP);
        break;
    case VTSS_TRACE_MODULE_PARM_USEC:
        return !!HAS_FLAGS(grp, VTSS_TRACE_FLAGS_USEC);
        break;
    case VTSS_TRACE_MODULE_PARM_RINGBUF:
        return !!HAS_FLAGS(grp, VTSS_TRACE_FLAGS_RINGBUF);
        break;
    case VTSS_TRACE_MODULE_PARM_IRQ:
        return !!HAS_FLAGS(grp, VTSS_TRACE_FLAGS_IRQ);
        break;
    default:
        T_E("Unknown parm: %d", parm);
        return 0;
        break;
    }
} /* vtss_trace_grp_get_parm */


BOOL vtss_trace_module_to_val(const char *name, int *module_id_p)
{
    int i;
    char name_lc[VTSS_TRACE_MAX_NAME_LEN+1];
    int  match_cnt = 0;

    /* Convert name to lowercase */
    strncpy(name_lc, name, VTSS_TRACE_MAX_NAME_LEN);
    name_lc[VTSS_TRACE_MAX_NAME_LEN] = 0;
    strn_tolower(name_lc, strlen(name_lc));

    for (i = 0; i < MODULE_ID_MAX; i++) {
        if (!trace_regs[i]) {
            continue;
        }

        // Test if the search string matches the module name
        if (strncmp(name_lc, trace_regs[i]->name, strlen(name_lc)) != 0) {
            continue;
        }

        if (strlen(trace_regs[i]->name) == strlen(name_lc)) {
            // Exact match found
            *module_id_p = i;
            return 1;
        }

        match_cnt ++;
        *module_id_p = i;
    }

    return match_cnt == 1;
} /* vtss_trace_module_to_val */


BOOL vtss_trace_grp_to_val(const char *name, int  module_id, int *grp_idx_p)
{
    int i;
    char name_lc[VTSS_TRACE_MAX_NAME_LEN+1];
    int gidx_match = -1;

    /* Check for valid module_id, in case CLI should call us with -2 */
    if (module_id < 0 ||
        module_id > MODULE_ID_MAX ||
        !trace_regs[module_id]) return 0;

    /* Convert name to lowercase */
    strncpy(name_lc, name, VTSS_TRACE_MAX_NAME_LEN);
    name_lc[VTSS_TRACE_MAX_NAME_LEN] = 0;
    strn_tolower(name_lc, strlen(name_lc));

    for (i = 0; i < trace_regs[module_id]->grp_cnt; i++) {
        if (strncmp(name_lc, trace_regs[module_id]->grps[i].name, strlen(name_lc)) == 0) {
            if (strlen(trace_regs[module_id]->grps[i].name) == strlen(name_lc)) {
                /* Exact match found */
                gidx_match = i;
                break;
            }
            if (gidx_match == -1) {
                /* First match found */
                gidx_match = i;
            } else {
                /* >1 match found */
                return 0;
            }
        }
    }

    if (gidx_match != -1) {
        *grp_idx_p = gidx_match;
        return 1;
    }

    return 0;
} /* vtss_trace_grp_to_val */

const char *vtss_trace_lvl_to_str(int lvl)
{
    switch (lvl) {
    case VTSS_TRACE_LVL_NONE:
        return "none";
        break;
    case VTSS_TRACE_LVL_ERROR:
        return "error";
        break;
    case VTSS_TRACE_LVL_WARNING:
        return "warning";
        break;
    case VTSS_TRACE_LVL_INFO:
        return "info";
        break;
    case VTSS_TRACE_LVL_DEBUG:
        return "debug";
        break;
    case VTSS_TRACE_LVL_NOISE:
        return "noise";
        break;
    case VTSS_TRACE_LVL_RACKET:
        return "racket";
        break;
    default:
        return "others";
    }
} /* vtss_trace_lvl_to_str */

BOOL vtss_trace_lvl_to_val(const char *name, int *level_p)
{
    static struct {
        int level;
        const char *name;
    } levels[] = {
        { VTSS_TRACE_LVL_NONE,    "none" },
        { VTSS_TRACE_LVL_ERROR,   "error" },
        { VTSS_TRACE_LVL_WARNING, "warning" },
        { VTSS_TRACE_LVL_INFO,    "info" },
        { VTSS_TRACE_LVL_DEBUG,   "debug" },
        { VTSS_TRACE_LVL_NOISE,   "noise" },
        { VTSS_TRACE_LVL_RACKET,  "racket" },
    };
    int i;
    for (i = 0; i < ARRSZ(levels); i++) {
        if (strcmp(levels[i].name, name) == 0) {
            *level_p = levels[i].level;
            return TRUE;
        }
    }
    return FALSE;
} /* vtss_trace_lvl_to_val */


void vtss_trace_thread_lvl_set(int thread_id, int lvl)
{
    TRACE_ASSERT(thread_id == -1 || thread_id <= VTSS_TRACE_MAX_THREAD_ID,
                 ("thread_id=%d", thread_id));

    /* Store current trace levels before changing */
    trace_store_prv_lvl();

    if (thread_id == -1) {
        int i;
        for (i = 0; i <= VTSS_TRACE_MAX_THREAD_ID; i++) {
            trace_threads[i].lvl = lvl;
        }
    } else {
        trace_threads[thread_id].lvl = lvl;
    }
} /* vtss_trace_thread_lvl_set */


void vtss_trace_thread_stackuse_set(int thread_id, BOOL enable)
{
    TRACE_ASSERT(thread_id == -1 || thread_id <= VTSS_TRACE_MAX_THREAD_ID,
                 ("thread_id=%d", thread_id));

    if (thread_id == -1) {
        int i;
        for (i = 0; i <= VTSS_TRACE_MAX_THREAD_ID; i++) {
            trace_threads[i].stackuse = enable;
        }
    } else {
        trace_threads[thread_id].stackuse = enable;
    }
} /* vtss_trace_thread_stackuse_set */


void vtss_trace_thread_get(int thread_id, vtss_trace_thread_t *trace_thread)
{
    TRACE_ASSERT(thread_id <= VTSS_TRACE_MAX_THREAD_ID, ("thread_id=%d", thread_id));

    *trace_thread = trace_threads[thread_id];
} /* vtss_trace_thread_get */


/* ======================================================================== */


/* ---------------------------------------------------------------------------
 * API functions
 * ######################################################################## */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
