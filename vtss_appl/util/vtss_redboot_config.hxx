/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

/**
 * By default, "RedBoot config" is 4kb while mtd erasesize is 64kb (depends on
 * the mtd device).
 * Hence we used 64kb instead of the real size 4kb to intialize "RedBoot config"
 * mtd device so as to read/write.
 */
#define MAX_SCRIPT_LENGTH  512 /* CYGNUM_REDBOOT_FLASH_SCRIPT_SIZE */
#define MAX_STRING_LENGTH  128 /* CYGNUM_REDBOOT_FLASH_STRING_SIZE */
#define MAX_CONFIG_DATA   4096 /* CYGNUM_REDBOOT_FLASH_CONFIG_SIZE */

#define MAX_CONFIG_DATA 4096

#define CONFIG_KEY1 0x0BADFACE
#define CONFIG_KEY2 0xDEADDEAD

typedef struct {
    unsigned long len;
    unsigned long key1;
    char config_data[MAX_CONFIG_DATA-(4*4)];
    unsigned long key2;
    unsigned long cksum;
} vtss_config_t;

//
// Layout of config data
// Each data item is variable length, with the name, type and dependencies
// encoded into the object.
//  offset   contents
//       0   data type
//       1   length of name (N)
//       2   enable sense
//       3   length of enable key (M)
//       4   key name
//     N+4   enable key
//   M+N+4   data value
//

#define CONFIG_OBJECT_TYPE(dp)          (dp)[0]
#define CONFIG_OBJECT_KEYLEN(dp)        (dp)[1]
#define CONFIG_OBJECT_ENABLE_SENSE(dp)  (dp)[2]
#define CONFIG_OBJECT_ENABLE_KEYLEN(dp) (dp)[3]
#define CONFIG_OBJECT_KEY(dp)           ((dp)+4)
#define CONFIG_OBJECT_ENABLE_KEY(dp)    ((dp)+4+CONFIG_OBJECT_KEYLEN(dp))
#define CONFIG_OBJECT_VALUE(dp)         ((dp)+4+CONFIG_OBJECT_KEYLEN(dp)+CONFIG_OBJECT_ENABLE_KEYLEN(dp))

#define LIST_OPT_LIST_ONLY (1)
#define LIST_OPT_NICKNAMES (2)
#define LIST_OPT_FULLNAMES (4)
#define LIST_OPT_DUMBTERM  (8)

#define CONFIG_EMPTY   0
#define CONFIG_BOOL    1
#define CONFIG_INT     2
#define CONFIG_STRING  3
#define CONFIG_SCRIPT  4

/****************************************************************************/
/*  fconfig structure                                                        */
/****************************************************************************/
typedef struct {
    char *key;      /* Datum 'key' */
    int   keylen;   /* Length of key */
    void *val;      /* Pointer to data */
    int   type;     /* Type of datum */
    int   offset;   /* Offset within data (used by _NEXT) */
} vtss_fconfig_t;


mesa_rc redboot_config_read(vtss_config_t **cfg);
mesa_rc redboot_config_lookup(vtss_config_t **cfg, char *key, char **dp);
mesa_rc redboot_config_set(vtss_config_t **cfg, char *key, void *val, int type);
mesa_rc redboot_config_write(vtss_config_t **cfg);
mesa_rc redboot_config_close(vtss_config_t **cfg);
mesa_rc vtss_call_if_flash_cfg_get(vtss_fconfig_t *fc);
mesa_rc vtss_call_if_flash_cfg_set(vtss_fconfig_t *fc);
