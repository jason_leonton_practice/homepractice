/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

#include <string>
#include <sys/types.h>
#include <sys/wait.h>

#include "main.h"
#include "vtss_sysexec.h"
#include "cli_io_api.h"
#include "vtss_trace_api.h"

#define SYSEXEC_POPEN_READ              0
#define SYSEXEC_POPEN_WRITE             1

#define SYSEXEC_CLIENT_POLL_INTERVAL    (500 * 1000)    // 0.5 second in microseconds
#define SYSEXEC_SLEEP_AFTER_KILL        (1000 * 1000)   // 1 second in microseconds
#define SYSEXEC_MAX_POLLS_CANCEL        6               // Max. no of polls after cancel

#define SYSEXEC_TRACE(mod, grp, lvl, fmt, ...) \
    T_EXPLICIT(mod, grp, lvl, __VTSS_LOCATION__, __LINE__, fmt, ##__VA_ARGS__);

// Safe print using provided output handle (which may be NULL)
#define SYSEXEC_PRINTF(...)                 \
    if(pr)                          \
        (void)(*pr)(__VA_ARGS__)


/*
 * popen altenative which returns the PID and stdin and stdout handles for
 * the child process. This can be used to perform unbuffered read from the
 * child and also to control the child process using the PID.
 *
 * argv:    Array of command line arguments for the child. The first argument
 *          must be the filename of the executable. This must be located in
 *          the system PATH.
 *
 * infp:    On return this parameter contains the stdin handle for the child.
 *
 * outfp:   On return this parameter contains the stdout handle for the child.
 *
 * errfp:   On return this parameter contains the stderr handle for the child.
 *
 * Returns: The PID of the child process or -1 if the lauch failed.
 */
static pid_t popen2(const char *const argv[], int *infp, int *outfp, int *errfp)
{
    int p_stdin[2], p_stdout[2], p_stderr[2];
    pid_t pid;

    if (pipe(p_stdin) != 0 || pipe(p_stdout) != 0 || pipe(p_stderr) != 0) {
        return -1;
    }

    // Fork the current process
    pid = fork();

    if (pid < 0) {
        // Error when forking - return error code
        return pid;
    } else if (pid == 0) {
        // This is the child process - prepare the pipes (handles)
        close(p_stdin[SYSEXEC_POPEN_WRITE]);
        dup2(p_stdin[SYSEXEC_POPEN_READ], STDIN_FILENO);

        close(p_stdout[SYSEXEC_POPEN_READ]);
        dup2(p_stdout[SYSEXEC_POPEN_WRITE], STDOUT_FILENO);

        close(p_stderr[SYSEXEC_POPEN_READ]);
        dup2(p_stderr[SYSEXEC_POPEN_WRITE], STDERR_FILENO);

        // Execute the command with arguments
        execvp(argv[0], (char *const *)argv);

        // We only get here if the execvp command failed.
        perror(argv[0]);
        exit(1);
    }

    // The parent process continues here - collect handles
    if (infp == NULL) {
        close(p_stdin[SYSEXEC_POPEN_WRITE]);
    } else {
        *infp = p_stdin[SYSEXEC_POPEN_WRITE];
    }
    if (outfp == NULL) {
        close(p_stdout[SYSEXEC_POPEN_READ]);
    } else {
        *outfp = p_stdout[SYSEXEC_POPEN_READ];
    }
    if (errfp == NULL) {
        close(p_stderr[SYSEXEC_POPEN_READ]);
    } else {
        *errfp = p_stderr[SYSEXEC_POPEN_READ];
    }

    return pid;
}

/*
 * Check if the user has pressed Ctrl-C on the CLI console
 */
static BOOL check_user_cancel(pid_t pid, vtss_sysexec_client_cancel_t cancelmode, vtss_sysexec_output_pr_t *pr)
{
    const char *user_cancel_message = "\n% User canceled - aborting\n";

    if (cli_getkey(CTLC)) {
        // We received Ctrl-C from CLI - kill the client
        SYSEXEC_PRINTF(user_cancel_message);
        int sigtype = cancelmode == VTSS_SYSEXEC_CLIENT_CANCEL_NICE ? SIGINT : SIGTERM;
        kill(pid, sigtype);
        return TRUE;
    }

    return FALSE;
}

/*
 * Kill process with given pid using SIGTERM
 */
static void kill_process(int pid)
{
    kill(pid, SIGTERM);
    usleep(SYSEXEC_SLEEP_AFTER_KILL);
}

BOOL vtss_sysexec_command(vtss::Vector<std::string> arguments, vtss_sysexec_output_pr_t *pr,
                          vtss_sysexec_client_cancel_t cancelmode, int tr_modid, int tr_grpid)
{
    const int linelen = 128;

    int rc = 0;
    char buffer[linelen + 1];
    ssize_t rbytes;
    BOOL user_has_canceled = FALSE;
    int cancel_polls = 0;

    if (arguments.size() == 0) {
        SYSEXEC_PRINTF("No arguments for commmand\n");
        return FALSE;
    }

    memset(buffer, 0, sizeof(buffer));

    // Format argument array for popen2
    const char *argv[arguments.size() + 1];
    memset(argv, 0, sizeof(argv));

    SYSEXEC_TRACE(tr_modid, tr_grpid, VTSS_TRACE_LVL_DEBUG, "Launcing command %s", arguments[0].c_str());

    for (int i = 0; i < arguments.size(); i++) {
        argv[i] = arguments[i].c_str();

        if (i > 0) {
            SYSEXEC_TRACE(tr_modid, tr_grpid, VTSS_TRACE_LVL_DEBUG, "Arg %d: %s", i, argv[i]);
        }
    }
    argv[arguments.size()] = nullptr;

    // Launch child process for command
    int infp = 0, outfp = 0, errfp = 0;
    pid_t pid = popen2(argv, &infp, &outfp, &errfp);
    if (pid < 0) {
        SYSEXEC_TRACE(tr_modid, tr_grpid, VTSS_TRACE_LVL_WARNING, "Error spawning child process, error:%d", pid);
        return FALSE;
    }

    SYSEXEC_TRACE(tr_modid, tr_grpid, VTSS_TRACE_LVL_DEBUG, "Spawned child process with pid:%d", pid);

    // Ensure that the client stdout and stderr handles are set to non-blocking
    rc = fcntl(outfp, F_SETFL, O_NONBLOCK);
    if (rc == 0) {
        rc = fcntl(errfp, F_SETFL, O_NONBLOCK);
    }
    if (rc != 0) {
        SYSEXEC_TRACE(tr_modid, tr_grpid, VTSS_TRACE_LVL_WARNING, "Error %d controlling handles for child process pid:%d", rc, pid);
        kill_process(pid);

        close(infp);
        close(outfp);
        close(errfp);

        return FALSE;
    }

    while (TRUE) {
        // poll client stdout for new data
        rbytes = read(outfp, buffer, linelen);
        if (rbytes == -1 && errno == EAGAIN) {
            // no data read

            // Check client process state
            siginfo_t infop;
            int res = waitid(P_PID, pid, &infop, WEXITED | WNOHANG);
            if (res != 0) {
                SYSEXEC_TRACE(tr_modid, tr_grpid, VTSS_TRACE_LVL_DEBUG, "Child process with PID %d no longer present", pid);
                // client process is no longer with us - break out of loop
                break;
            }

            if (user_has_canceled) {
                // check if the client has responded to a previous cancel request
                cancel_polls++;

                if (cancel_polls >= SYSEXEC_MAX_POLLS_CANCEL) {
                    // client process is not responding to our cancel - kill it and return
                    SYSEXEC_TRACE(tr_modid, tr_grpid, VTSS_TRACE_LVL_DEBUG, "Child process with PID %d not responding to cancel", pid);
                    kill_process(pid);
                    break;
                }

            } else {
                // Check if the user has cancelled session with Ctrl-C
                user_has_canceled = check_user_cancel(pid, cancelmode, pr);
            }

            // Sleep between polls
            usleep(SYSEXEC_CLIENT_POLL_INTERVAL);
        } else if (rbytes > 0) {
            // received data from client - print as is on console
            buffer[rbytes] = '\0';

            SYSEXEC_PRINTF("%s", buffer);

            if (!user_has_canceled) {
                // Check if the user has cancelled session with Ctrl-C
                user_has_canceled = check_user_cancel(pid, cancelmode, pr);
            }
        } else {
            // unexpected - break loop
            SYSEXEC_TRACE(tr_modid, tr_grpid, VTSS_TRACE_LVL_DEBUG, "Child read returned unexpected result:%d", rbytes);
            break;
        }
    }

    // Get potential dying gasp from client
    while (TRUE) {
        rbytes = read(outfp, buffer, linelen);
        if (rbytes > 0) {
            buffer[rbytes] = '\0';
            SYSEXEC_PRINTF("%s", buffer);
        } else {
            break;
        }
    }

    // Read stderr and print any output
    while (TRUE) {
        rbytes = read(errfp, buffer, linelen);
        if (rbytes > 0) {
            buffer[rbytes] = '\0';
            SYSEXEC_PRINTF("%s", buffer);
        } else {
            break;
        }
    }

    // Close all child handles
    close(infp);
    close(outfp);
    close(errfp);

    return TRUE;
}
