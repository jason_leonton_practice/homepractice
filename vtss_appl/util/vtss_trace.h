/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 

*/

#ifndef _VTSS_TRACE_H_
#define _VTSS_TRACE_H_

/* Check defines */
#if !defined(VTSS_SWITCH) 
#define VTSS_SWITCH 0
#elif (VTSS_SWITCH != 0 && VTSS_SWITCH != 1)
#error VTSS_SWITCH must be set to 0 or 1
#endif

#if !defined(VTSS_TRACE_MULTI_THREAD) || (VTSS_TRACE_MULTI_THREAD != 0 && VTSS_TRACE_MULTI_THREAD != 1)
#error VTSS_TRACE_MULTI_THREAD not specified. Must be set to 0 or 1.
#endif

#include <stdio.h>
#include <string.h>

#include "vtss_trace_lvl_api.h"
#include "vtss_trace_api.h"
#if VTSS_SWITCH
#include "vtss_trace_vtss_switch_api.h"
#endif
#include "vtss_trace_io.h"
#include "vtss_os_wrapper.h"

#if VTSS_TRACE_MULTI_THREAD
/* Semaphore for IO registrations */
extern vtss_sem_t trace_io_crit;
extern vtss_sem_t trace_rb_crit;
#endif /* VTSS_TRACE_MULTI_THREAD */

/* =================
 * Trace definitions
 * -------------- */
#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>

#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_TRACE

#define TRACE_GRP_DEFAULT 0
#define TRACE_GRP_CNT     1

#include <vtss_trace_api.h>
/* ============== */


#ifndef TRACE_ASSERT
#define TRACE_ASSERT(expr, msg) { \
    if (!(expr)) { \
        T_E("ASSERTION FAILED"); \
        T_E msg; \
        VTSS_ASSERT(expr); \
    } \
}

#endif

#undef MAX
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#undef MIN
#define MIN(a,b) ((a) > (b) ? (b) : (a))

/* Max size of Error message (size of buffer used to hand the message to syslog_flash_log) */
/* Make it large enough! */
#define TRACE_ERR_BUF_SIZE 4096

#endif /* _VTSS_TRACE_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
