/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

#ifndef _SYSEXEC_UTIL_H
#define _SYSEXEC_UTIL_H

#include <vtss/basics/vector.hxx>

/*
 * Function type for printing output from lauched process.
 */
typedef int vtss_sysexec_output_pr_t(const char *fmt, ...);

/*
 * Indicates how to cancel a client process
 */
typedef enum {
    // Send SIGINT and let the client process handle termination on its own
    VTSS_SYSEXEC_CLIENT_CANCEL_NICE,
    // Send SIGTERM and kill the client process hard
    VTSS_SYSEXEC_CLIENT_CANCEL_HARD
} vtss_sysexec_client_cancel_t;

/*
 * Execute external program in a child process using given arguments and
 * print output from the program.
 *
 * arguments:   List of command line arguments for process. The first argument
 *              must be the filename of the executable itself.
 *
 * pr:          printf-like function to print output from program.
 *
 * cancelmode:  Defines how clients are interrupted when user cancels session with Ctrl-C.
 *              - Use CLIENT_CANCEL_NICE if client is well-behaved and prints a reasonable
 *                exit message to stdout or stderr as response to Ctrl-C.
 *              - Use CLIENT_CANCEL_HARD if client prints confusing error message to
 *                stdout or stderr as response to Ctrl-C (traceroute, we're looking at you).
 *                These clients will be killed (hard) with SIGKILL.
 *
 * tr_modid:    Trace module ID of caller.
 *
 * tr_grpid:    Trace group ID of caller.
 *
 * Returns TRUE when program was lauched successfully and has terminated.
 * Any error messages will be printed using the provided pr function.
 */
BOOL vtss_sysexec_command(vtss::Vector<std::string> arguments, vtss_sysexec_output_pr_t *pr,
                          vtss_sysexec_client_cancel_t cancelmode, int tr_modid, int tr_grpid);

#endif /* _POPEN_UTIL_H */

