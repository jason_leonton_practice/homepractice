/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 

*/

/*
  This file contains API for Vitesse turnkey SW's access to the trace module
  and should not be used for other purposes.
*/


#ifndef _VTSS_TRACE_VTSS_SWITCH_API_H_
#define _VTSS_TRACE_VTSS_SWITCH_API_H_

#if !defined(VTSS_SWITCH) || !VTSS_SWITCH
#error This file should only be included as part of Vitesse turnkey SW
#endif

#include <mscc/ethernet/switch/api.h>
#include <main.h> /* Need typedef for init_cmt_t */

#ifdef __cplusplus
extern "C" {
#endif
/* Initialize module. Only to be called for managed build. */
/* The init function uses type int for argument cmd, since including
 * main.h will cause compilation problems, due to inclusion of trace_api.h
 * in switch API */
mesa_rc vtss_trace_init(vtss_init_data_t *data);

/* Write trace configuration to flash */
mesa_rc vtss_trace_cfg_wr(void);

/* Read trace configuration from flash */
mesa_rc vtss_trace_cfg_rd(void);

/* Erase trace configuration from flash */
mesa_rc vtss_trace_cfg_erase(void);

#ifdef __cplusplus
}
#endif
#endif /*_VTSS_TRACE_VTSS_SWITCH_API_H_ */


/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
