/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _DHCP_POOL_H_
#define _DHCP_POOL_H_

#include "dhcp_client.hxx"

#define VTSS_NEW(DST, TYPE, ...)                \
    DST = (TYPE *)VTSS_CALLOC(1, sizeof(TYPE)); \
    if (DST) {                                  \
        new (DST) TYPE(__VA_ARGS__);            \
    }

#define VTSS_DELETE(DST, DESTRUCTOR) \
    if (DST) {                       \
        DST->DESTRUCTOR();           \
        VTSS_FREE(DST);              \
    }


extern "C" {
#include "ip_utils.h"
}


#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_DHCP_CLIENT
#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_DHCP_CLIENT

#define OK_OR_RETURN_ERR(VID)     \
    do {                          \
        VTSS_ASSERT(VID < MAX);   \
        if (!enabled[VID]) {      \
            return VTSS_RC_ERROR; \
        }                         \
    } while (0)


namespace vtss {
namespace dhcp {

template <typename FrameTxService, typename TimerService, typename Lock,
          unsigned MAX>
class DhcpPool {
  public:
    typedef Client<FrameTxService, TimerService, Lock> DhcpClient_t;

    struct CStyleCB : public notifications::EventHandler {
        enum { MAX_CALLEES = 1 };

        CStyleCB(TimerService *ts, DhcpClient_t *c, mesa_vid_t v)
            : notifications::EventHandler(ts),
              trigger(this),
              client_(c),
              vlan_(v) {
            for (int i = 0; i < MAX_CALLEES; ++i) callees[i] = 0;
        }

        mesa_rc callback_add(client_callback_t cb) {
            int free = -1;

            for (int i = 0; i < MAX_CALLEES; ++i) {
                if (callees[i] == 0) {
                    free = i;
                }

                if (callees[i] == cb) {
                    T_I("Callback already exists");
                    return VTSS_RC_OK;
                }
            }

            if (free != -1) {
                callees[free] = cb;
                return VTSS_RC_OK;
            } else {
                return VTSS_RC_ERROR;
            }
        }

        mesa_rc callback_del(client_callback_t cb) {
            for (int i = 0; i < MAX_CALLEES; ++i) {
                if (callees[i] != cb) {
                    continue;
                }
                callees[i] = 0;
                return VTSS_RC_OK;
            }
            return VTSS_RC_ERROR;
        }

        void execute(notifications::Event *t) {
            client_->ack_conf(*t);

            T_I("Invoke callbacks %p", t);
            for (int i = 0; i < MAX_CALLEES; ++i) {
                client_callback_t cb = callees[i];
                if (cb == 0) {
                    continue;
                }
                T_I("  Invoke callback");
                cb(vlan_);
            }
        }

        void start() {
            client_->ack_conf(trigger);
        }

        void stop() {
            trigger.unlink();
            for (int i = 0; i < MAX_CALLEES; ++i) callees[i] = 0;
        }

        notifications::Event trigger;
        DhcpClient_t *client_;
        const mesa_vid_t vlan_;
        client_callback_t callees[MAX_CALLEES];
    };


    DhcpPool(FrameTxService &tx, TimerService &ts, Lock &l)
        : tx_(tx), ts_(ts), lock_(l) {
        for (unsigned i = 0; i < MAX; ++i) {
            client[i] = 0;
            c_style_cb[i] = 0;
            enabled[i] = false;
        }
    }

    mesa_rc start(mesa_vid_t vid, const vtss_appl_ip_dhcp_param_t *params) {
        VTSS_ASSERT(vid < MAX);

        if (enabled[vid]) {
            T_I("Restarting existing client");
        }

        if (client[vid] == 0) {
            T_I("Creating new client");
            VTSS_NEW(client[vid], DhcpClient_t, tx_, ts_, lock_, vid, *params);
            VTSS_NEW(c_style_cb[vid], CStyleCB, &ts_, client[vid], vid);
        } else {
            // Update the client parameteres.
            client[vid]->params_set(params);
        }

        if (client[vid] == 0 || c_style_cb[vid] == 0) {
            VTSS_DELETE(c_style_cb[vid], ~CStyleCB);
            VTSS_DELETE(client[vid], ~DhcpClient_t);
            c_style_cb[vid] = 0;
            client[vid] = 0;
            enabled[vid] = false;
            return VTSS_RC_ERROR;
        }

        enabled[vid] = true;
        c_style_cb[vid]->start();
        return client[vid]->start();
    }

    mesa_rc kill(mesa_vid_t vid) {
        OK_OR_RETURN_ERR(vid);

        mesa_rc rc;
        c_style_cb[vid]->stop();
        rc = client[vid]->stop();
        enabled[vid] = false;

        return rc;
    }

    mesa_rc stop(mesa_vid_t vid) {
        OK_OR_RETURN_ERR(vid);
        return client[vid]->stop();
    }

    mesa_rc fallback(mesa_vid_t vid) {
        OK_OR_RETURN_ERR(vid);
        return client[vid]->fallback();
    }

    mesa_rc if_down(mesa_vid_t vid) {
        OK_OR_RETURN_ERR(vid);
        return client[vid]->if_down();
    }

    mesa_rc if_up(mesa_vid_t vid) {
        OK_OR_RETURN_ERR(vid);
        return client[vid]->if_up();
    }

    mesa_rc release(mesa_vid_t vid) {
        OK_OR_RETURN_ERR(vid);
        return client[vid]->release();
    }

    mesa_rc decline(mesa_vid_t vid) {
        OK_OR_RETURN_ERR(vid);
        return client[vid]->decline();
    }

    mesa_rc bind(mesa_vid_t vid) {
        OK_OR_RETURN_ERR(vid);
        return client[vid]->bind();
    }

    DhcpClient_t *get(mesa_vid_t vid) { return client[vid]; }

    BOOL bound_get(mesa_vid_t vid) {
        OK_OR_RETURN_ERR(vid);
        vtss_appl_ip_dhcp4c_state_t s = client[vid]->state();

        return s == VTSS_APPL_IP_DHCP4C_STATE_BOUND ||
               s == VTSS_APPL_IP_DHCP4C_STATE_RENEWING ||
               s == VTSS_APPL_IP_DHCP4C_STATE_REBINDING;
    }

    mesa_rc offers_get(mesa_vid_t vid, size_t max_offers, size_t *valid_offers,
                       ConfPacket *list) {
        OK_OR_RETURN_ERR(vid);
        client[vid]->offers(max_offers, valid_offers, list);
        return VTSS_RC_OK;
    }

    mesa_rc accept(mesa_vid_t vid, unsigned idx) {
        OK_OR_RETURN_ERR(vid);

        if (client[vid]->accept(idx)) {
            return VTSS_RC_OK;
        } else {
            return VTSS_RC_ERROR;
        }
    }

    mesa_rc status(mesa_vid_t vid, vtss_appl_ip_dhcp_client_status_t *status) {
        OK_OR_RETURN_ERR(vid);
        vtss_appl_ip_dhcp_client_status_t s = client[vid]->status();
        *status = s;
        return VTSS_RC_OK;
    }

    mesa_rc callback_add(mesa_vid_t vid, client_callback_t cb) {
        OK_OR_RETURN_ERR(vid);
        return c_style_cb[vid]->callback_add(cb);
    }

    mesa_rc callback_del(mesa_vid_t vid, client_callback_t cb) {
        OK_OR_RETURN_ERR(vid);
        return c_style_cb[vid]->callback_del(cb);
    }

    mesa_rc fields_get(mesa_vid_t vid, ConfPacket *fields) {
        OK_OR_RETURN_ERR(vid);

        typename DhcpClient_t::AckConfPacket p(client[vid]->ack_conf());

        if (p.valid()) {
            *fields = p.get();
            return VTSS_RC_OK;
        } else {
            return VTSS_RC_ERROR;
        }
    }

    mesa_rc dns_option_domain_any_get(vtss::Buffer *name) {

        for (int vid = 1; vid < VTSS_VIDS; ++vid) {
            if (enabled[vid]) {
                ConfPacket fields;
                if (fields_get(vid, &fields) == VTSS_RC_OK) {
                    if (fields.domain_name.size()) {
                        *name = fields.domain_name;
                        return VTSS_RC_OK;
                    }
                }
            }
        }

        return VTSS_RC_ERROR;
    }

    mesa_rc dns_option_any_get(mesa_ipv4_t prefered, mesa_ipv4_t *ip) {
        mesa_ipv4_t some_ip = 0;

        for (int i = 0; i < VTSS_VIDS; ++i) {
            ConfPacket fields;

            if (fields_get(i, &fields) != VTSS_RC_OK) continue;

            if (!fields.domain_name_server.valid()) continue;

            if (fields.domain_name_server.get() == prefered) {
                *ip = fields.domain_name_server.get();
                return VTSS_RC_OK;
            } else {
                some_ip = fields.domain_name_server.get();
            }
        }

        if (some_ip != 0) {
            *ip = some_ip;
            return VTSS_RC_OK;
        }

        return VTSS_RC_ERROR;
    }

    ~DhcpPool() {
        for (unsigned i = 0; i < MAX; ++i) {
            VTSS_DELETE(c_style_cb[i], ~CStyleCB);
            c_style_cb[i] = 0;
            VTSS_DELETE(client[i], ~DhcpClient_t);
            client[i] = 0;
        }
    }

  private:
    FrameTxService &tx_;
    TimerService &ts_;
    Lock &lock_;
    DhcpClient_t *client[MAX];
    CStyleCB *c_style_cb[MAX];
    bool enabled[MAX];
};
};
};

#undef OK_OR_RETURN_ERR
#undef VTSS_TRACE_MODULE_ID
#endif /* _DHCP_POOL_H_ */
