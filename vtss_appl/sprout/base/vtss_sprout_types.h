/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

 This file is part of SPROUT - "Stack Protocol using ROUting Technology".
*/


/*****************************************************************************

Type definitions used internally within SPROUT.

*****************************************************************************/


#ifndef _VTSS_SPROUT_TYPES_H_
#define _VTSS_SPROUT_TYPES_H_

#include <time.h>

#define VTSS_SPROUT_UIT_SIZE           VTSS_SPROUT_SIT_SIZE

/* GLAGID. Same as vtss_glag_no - 1 */
typedef uint vtss_sprout__glagid_t;

/* Master election priority. Same as vtss_sprout_mst_elect_prio_t - 1  */
typedef uint vtss_sprout__mst_elect_prio_t;

/* CPU Extraction queue. Same as vtss_cpu_rx_queue_t - 1 */
typedef uint vtss_sprout__cpu_rx_qu_t;

/* Index of "unit" (i.e. chip) within switch */
/* Unit idx. Same as vtss_sprout_unit_idx_t - 1 */
typedef uint vtss_sprout__unit_idx_t;

/* Index of upsid within unit */
typedef uint vtss_sprout__upsid_idx_t;

/* Topology type */
char *vtss_sprout_topology_type_to_str(vtss_appl_topo_topology_type_t topology_type);
void vtss_sprout_topology_type_print(vtss_appl_topo_topology_type_t topology_type);

// Indices for stack ports
//
// For dual-unit switches VTSS_SPROUT__SP_A is the external port on
// each unit and VTSS_SPROUT__SP_B is the internal interconnect between
// the units.
#define VTSS_SPROUT__SP_UNDEF (-1)
#define VTSS_SPROUT__SP_A     0
#define VTSS_SPROUT__SP_B     1
typedef int vtss_sprout__stack_port_idx_t;


/*
 * SPROUT Return Codes (mesa_rc)
 * Currently only used internally within SPROUT.
 */
enum {
    /*
     * UPSID Depletion, i.e. all UPSIDs used.
     * May occur if attempting to build stack with too many UPSes
     */
    SPROUT_RC_UPSID_DEPLETION = MODULE_ERROR_START(VTSS_MODULE_ID_SPROUT),
};

/* ===========================================================================
 * RIT
 * ------------------------------------------------------------------------ */
typedef struct vtss_sprout__ri_t {
    /* INTERNAL - Fields only used internally in vtss_sprout_xit.c */
    BOOL                    vld;
    BOOL                    found;   /* Entry found in SPROUT update   */
    BOOL                    is_top_switch;

    /* EXTERNAL - Fields also used outside vtss_sprout_xit.c */
    vtss_sprout_switch_addr_t switch_addr;
    vtss_sprout__unit_idx_t   unit_idx;

    // UPSIDs within unit
    // Having UPSIDs in both UIT and RIT is redundant, but useful for
    // performance reasons.
    mesa_vstax_upsid_t        upsid[2];

    vtss_sprout_dist_t        stack_port_dist[2];

    BOOL                      tightly_coupled;
} vtss_sprout__ri_t;

/* Note: topo_rit_t must only be accessed by functions defined in vtss_sprout_xit.c */
typedef struct _vtss_sprout__rit_t {
    BOOL      changed; /* RIT has changed during processing of SPROUT update */
    vtss_sprout__ri_t ri[VTSS_SPROUT_RIT_SIZE];
} vtss_sprout__rit_t;

/* Initialize vtss_sprout_switch_addr_t */
void vtss_sprout__ri_init(    vtss_sprout__ri_t  *ri_p);
void vtss_sprout__rit_init(   vtss_sprout__rit_t *rit_p);
char *vtss_sprout__ri_to_str(const vtss_sprout__ri_t *ri_p);
void vtss_sprout__ri_print(   vtss_sprout__ri_t  *ri_p);
char *vtss_sprout__rit_to_str(vtss_sprout__rit_t *rit_p);
void vtss_sprout__rit_print(  vtss_sprout_dbg_printf_t dbg_printf,
                              vtss_sprout__rit_t *rit_p);

/* ======================================================================== */


/* ===========================================================================
 * UIT
 * ------------------------------------------------------------------------ */

typedef struct _vtss_sprout__ui_t {
    /* INTERNAL - Fields only used internally in vtss_sprout_xit.c */
    BOOL                          vld;
    BOOL                          found;   /* Entry found in SPROUT update   */

    /* EXTERNAL - Fields also used outside vtss_sprout_xit.c */
    vtss_sprout_switch_addr_t     switch_addr;
    // Multi-unit switches: Index of unit within switch
    // unit_idx=0 is always primary unit
    vtss_sprout__unit_idx_t       unit_idx;

    // UPSIDs within unit
    // If upsid[1] is unused, then it is set to VTSS_VSTAX_UPSID_UNDEF
    mesa_vstax_upsid_t            upsid[2];

    BOOL                          primary_unit;
    // Note: glag_mbr_cnt is not used by SPROUT v2
    uint                          glag_mbr_cnt[VTSS_GLAGS];
    BOOL                          have_mirror;

    /* Master election fields */
    BOOL                          mst_capable; /* Has master capability */
    vtss_sprout__mst_elect_prio_t mst_elect_prio;
    BOOL                          mst_time_ignore;
    ulong                         mst_time;

    /* IPv4 address. 0.0.0.0 = Unknown */
    ulong ip_addr;

    /* Switch base info */
    BOOL                          tightly_coupled;

    /* Application information */
    vtss_sprout_switch_appl_info_t switch_appl_info;

    /* Values in reserved bits */
    uchar unit_base_info_rsv;                /* Bits 7-7 */
    uchar unit_glag_mbr_cnt_rsv[VTSS_GLAGS]; /* Bits 7-5 */
    uchar ups_base_info_rsv[2];              /* Bits 7-5 */
    uchar switch_mst_elect_rsv;              /* Bits 6-3 of byte 4 */
    uchar switch_base_info_rsv;              /* Bits 7-1 */

    // Stack port used to reach this unit
    // Only valid for UIT, not for UI-L0/L1.
    // Used to selected which UI-L0/L1 to get UI information from.
    vtss_sprout__stack_port_idx_t sp_idx;

    // mesa_port_no_t values represented by each UPSID of unit
    u64 ups_port_mask[2];
} vtss_sprout__ui_t;

/* Note: vtss_sprout__uit_t must only be accessed by functions defined in vtss_sprout_xit.c */
typedef struct _vtss_sprout__uit_t {
    BOOL      changed;
    BOOL      mst_time_changed;
    u32       change_mask; /* Details on what has changed */

    vtss_sprout__ui_t ui[VTSS_SPROUT_UIT_SIZE];
} vtss_sprout__uit_t;

/* Initialize vtss_sprout__ui_t */
void vtss_sprout__ui_init(  vtss_sprout__ui_t  *ui_p);
void vtss_sprout__uit_init( vtss_sprout__uit_t *uit_p);
char *vtss_sprout__ui_to_str(const vtss_sprout__ui_t  *ui_p);
void vtss_sprout__ui_print( vtss_sprout__ui_t  *ui_p);
char *vtss_sprout__uit_to_str(vtss_sprout__uit_t *uit_p);
void vtss_sprout__uit_print(vtss_sprout_dbg_printf_t dbg_printf, vtss_sprout__uit_t *uit_p);

/* ======================================================================== */


/* ===========================================================================
 * Switch state, including setup from application
 * ------------------------------------------------------------------------ */

typedef struct _vtss_sprout__stack_port_state_t {
    /* EXTERNAL: Input from application */
    BOOL                       adm_up;
    BOOL                       link_up;
    mesa_port_no_t             port_no;

    uint                       update_age_time;
    /* Periodic update interval as slave and master */
    uint                       update_interval_slv;
    uint                       update_interval_mst;

    /* Maximum number of Updates/Alerts per sec */
    uint                       update_limit;
    uint                       alert_limit;


    /* INTERNAL */
    BOOL                       proto_up;
    vtss_sprout_switch_addr_t    nbr_switch_addr;
    vtss_sprout__uit_t           uit;
    uint                       ttl;
    uint                       ttl_new;
    /* Forward gmirror copies via this stack port */
    BOOL                       mirror_fwd;

    /* Stack port configuration change is pending, e.g. new TTL */
    BOOL                       cfg_change;

    uint                       deci_secs_since_last_tx_update;
    uint                       deci_secs_since_last_rx_update;

    uint                       update_bucket;
    uint                       alert_bucket;
    uint                       deci_secs_since_last_update_bucket_filling;
    uint                       deci_secs_since_last_alert_bucket_filling;

    /* SPROUT Update Counters */
    uint                       sprout_update_rx_cnt;              /* Rx'ed SPROUT Updates, including bad ones */
    uint                       sprout_update_triggered_tx_cnt;    /* Tx'ed SPROUT Updates, triggered */
    uint                       sprout_update_periodic_tx_cnt;     /* Tx'ed SPROUT Updates, periodic  */
    uint                       sprout_update_tx_policer_drop_cnt; /* Tx-drops by SPROUT Tx-policer */
    uint                       sprout_update_rx_err_cnt;          /* Rx'ed errornuous SPROUT Updates */
    uint                       sprout_update_tx_err_cnt;          /* Tx of SPROUT Update failed */

    /* SPROUT Alert Counters */
    uint                       sprout_alert_rx_cnt;              /* Rx'ed SPROUT Alerts, including bad ones */
    uint                       sprout_alert_tx_cnt;              /* Tx'ed SPROUT Alerts (tx OK) */
    uint                       sprout_alert_tx_policer_drop_cnt; /* Tx-drops by SPROUT Tx-policer */
    uint                       sprout_alert_rx_err_cnt;          /* Rx'ed errornuous SPROUT Alerts */
    uint                       sprout_alert_tx_err_cnt;          /* Tx of SPROUT Alert failed */
} vtss_sprout__stack_port_state_t;

void vtss_sprout__stack_port_state_init(vtss_sprout__stack_port_state_t *const sps_p);
char *vtss_sprout__stack_port_states_to_str(
    const vtss_sprout__stack_port_state_t *sps0_p,
    const vtss_sprout__stack_port_state_t *sps1_p, // Set to NULL if not used
    const vtss_sprout__stack_port_state_t *sps2_p, // Set to NULL if not used
    const vtss_sprout__stack_port_state_t *sps3_p, // Set to NULL if not used
    const char *prefix);

typedef struct _vtss_sprout__unit_state_t {
    /* EXTERNAL: Input from application */
    mesa_vstax_upsid_t              upsid[2];  /* VTSS_VSTAX_UPSID_UNDEF => Undefined */
    u64                             ups_port_mask[2];
    uint                            glag_mbr_cnt[VTSS_GLAGS];
    BOOL                            have_mirror_port;

    // Stack ports
    //
    // For dual-unit switches the stack_port[] array is only used on the
    // primary unit.
    // stack_port[0] represents the primary unit's external stack port,
    // whereas stack_port[1] represents the internal port, except
    // for stack_port[1].port_no, which is the external stack port on the
    // secondary unit.
    vtss_sprout__stack_port_state_t stack_port[2];

    /* INTERNAL */
} vtss_sprout__unit_state_t;

void vtss_sprout__unit_state_init(   vtss_sprout__unit_state_t *const us_p);

typedef struct _vtss_sprout__switch_state_t {
    /* EXTERNAL: Input from application */
    vtss_sprout_switch_addr_t     switch_addr;

    BOOL                          mst_capable;
    vtss_sprout__mst_elect_prio_t mst_elect_prio;
    BOOL                          mst_time_ignore;
    vtss_sprout_switch_addr_t     mst_switch_addr; /* Switch addr of master */
    mesa_vstax_upsid_t            mst_upsid;       /* UPSID of primary unit on master */
    BOOL                          mst;             /* I am master! */
    ulong                         mst_start_time;  /* Time at which I became master */
    vtss_sprout__cpu_rx_qu_t      cpu_qu;

    // unit[0] is primary unit (CPU active)
    // unit[1] is secondary unit
    vtss_sprout__unit_state_t     unit[VTSS_SPROUT_MAX_LOCAL_CHIP_CNT];

    /* INTERNAL */
    vtss_appl_topo_topology_type_t topology_type;
    vtss_appl_topo_topology_type_t topology_type_new;
    uint                           topology_n; /* Number of units in RIT */

    // Have never had a stack link in state ProtoUp
    BOOL                        virgin;

    time_t                      topology_change_time; /* (Up)time of last topology change (in seconds) */
    time_t                      mst_change_time;      /* (Up)time of last master change (in seconds) */

    /* IPv4 address. 0.0.0.0 = Unknown */
    ulong                       ip_addr;

    /* Switch info from application */
    uchar                       switch_appl_info[8];

#if defined(VTSS_SPROUT_FW_VER_CHK)
    uchar                       my_fw_ver[VTSS_SPROUT_FW_VER_LEN];
    vtss_sprout_fw_ver_mode_t   fw_ver_mode;
#endif
} vtss_sprout__switch_state_t;

char *vtss_sprout__unit_state_to_str(vtss_sprout__switch_state_t *ss_p,
                                     vtss_sprout__unit_state_t *const specific_us_p, // Set to NULL to print state of all local units
                                     const char *prefix);
void vtss_sprout__switch_state_init (vtss_sprout__switch_state_t *const ss_p);
char *vtss_sprout__switch_state_to_str(vtss_sprout__switch_state_t *ss_p);
void vtss_sprout__switch_state_print (vtss_sprout_dbg_printf_t dbg_printf,
                                      vtss_sprout__switch_state_t *ss_p);

/* ======================================================================== */


/* ===========================================================================
 * Other
 * ------------------------------------------------------------------------ */

/* vtss_sprout_switch_addr is defined in vtss_sprout_api.h */
void vtss_sprout_switch_addr_init(vtss_sprout_switch_addr_t *sa_p);
char *vtss_sprout_switch_addr_to_str(const vtss_sprout_switch_addr_t *sa_p);
void vtss_sprout_switch_addr_print(vtss_sprout_switch_addr_t *sa_p);


/* ======================================================================== */


#endif /* _VTSS_SPROUT_TYPES_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
