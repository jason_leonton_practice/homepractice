/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.



 This file is part of SPROUT - "Stack Protocol using ROUting Technology".
*/

#ifndef _VTSS_SPROUT_H_
#define _VTSS_SPROUT_H_

#include <vtss_module_id.h>
#include <mscc/ethernet/switch/api.h>
#include <board_if.h>
#include <vtss_api_if_api.h>

/* Check defines */
#if !defined(VTSS_SWITCH)
#define VTSS_SWITCH 0
#elif (VTSS_SWITCH != 0 && VTSS_SWITCH != 1)
#error VTSS_SWITCH must be set to 0 or 1
#endif

#if !defined(VTSS_SPROUT_UNMGD)
#define VTSS_SPROUT_UNMGD 0
#elif (VTSS_SPROUT_UNMGD != 0 && VTSS_SPROUT_UNMGD != 1)
#error VTSS_SPROUT_UNMGD must be set to 0 or 1
#endif

#if !defined(VTSS_SPROUT_MULTI_THREAD) || (VTSS_SPROUT_MULTI_THREAD != 0 && VTSS_SPROUT_MULTI_THREAD != 1)
#error VTSS_SPROUT_MULTI_THREAD must be set to 0 or 1.
#endif
#if !defined(VTSS_SPROUT_CRIT_CHK) || (VTSS_SPROUT_CRIT_CHK != 0 && VTSS_SPROUT_CRIT_CHK != 1)
#error VTSS_SPROUT_CRIT_CHK must be set to 0 or 1.
#endif

#include <main.h>

/* =================
 * Trace definitions
 * -------------- */
#include <vtss_trace_lvl_api.h>

#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_SPROUT

#define VTSS_TRACE_GRP_DEFAULT          0
#define TRACE_GRP_PKT_DUMP              1
#define VTSS_TRACE_GRP_TOP_CHG          2
#define TRACE_GRP_MST_ELECT             3
#define TRACE_GRP_UPSID                 4
#define TRACE_GRP_CRIT                  5
#define TRACE_GRP_FAILOVER              6
#define TRACE_GRP_STARTUP               7
#define TRACE_GRP_CNT                   8

#include <vtss_trace_api.h>
/* ============== */

#include "vtss_sprout_api.h"

/* Set maximum RIT size slightly larger to handle transitional unit counts during quick removal+insertion */
#define VTSS_SPROUT_RIT_SIZE           (VTSS_SPROUT_MAX_UNITS_IN_STACK + VTSS_SPROUT_MAX_UNITS_IN_STACK/2)

#include "vtss_sprout_types.h"
#include "vtss_sprout_crit.h"
#include "vtss_sprout_util.h"
#include "vtss_sprout_xit.h"

#ifndef VTSS_SPROUT_ASSERT
#define VTSS_SPROUT_ASSERT(expr, msg) { \
    if (!(expr)) { \
        T_E msg; \
        VTSS_ASSERT(expr); \
    } \
}
#endif

#ifndef VTSS_SPROUT_ASSERT_DBG_INFO
#define VTSS_SPROUT_ASSERT_DBG_INFO(expr, msg) { \
    if (!(expr)) { \
        T_E msg; \
        T_E("Switch state:\n%s\n", \
            vtss_sprout__switch_state_to_str(&switch_state));   \
        T_E("RIT:\n%s\n", \
            vtss_sprout__rit_to_str(&rit)); \
        T_E("UIT:\n%s\n", \
            vtss_sprout__uit_to_str(&uit)); \
        VTSS_ASSERT(expr); \
    } \
}
#endif

/* Minimum diff between master times (larger than min)                     */
/* If two master time values differ with less than min diff, then they are */
/* considered equal                                                        */
#define VTSS_SPROUT_MST_TIME_DIFF_MIN 10 /* seconds */

extern vtss_sprout__switch_state_t switch_state;
extern vtss_sprout__uit_t uit;
extern vtss_sprout__rit_t rit;

#endif /* _VTSS_SPROUT_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
