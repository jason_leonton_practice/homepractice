/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.



 This file is part of SPROUT - "Stack Protocol using ROUting Technology".
*/


/*****************************************************************************

Type definitions used internally within SPROUT.

*****************************************************************************/


#ifndef _VTSS_SPROUT_XIT_H_
#define _VTSS_SPROUT_XIT_H_


/* ===========================================================================
 * RIT - External functions
 * ------------------------------------------------------------------------ */

/*
 * Update existing entry or insert new entry.
 * Returns TRUE if any change was made.
 */
BOOL vtss_sprout__rit_update(
    vtss_sprout__rit_t                  *rit_p,
    const vtss_sprout__ri_t             *ri_p,
    const vtss_sprout__stack_port_idx_t sp_idx);

/*
 * Find RI based on switch address and unit index.
 * Returns NULL if not found.
 */
vtss_sprout__ri_t *vtss_sprout__ri_find(
    vtss_sprout__rit_t              *rit_p,
    const vtss_sprout_switch_addr_t *switch_addr_p,
    const vtss_sprout__unit_idx_t   unit_idx);

/*
 * Find RI located at specific distance via specific stack port on current switch.
 * Returns NULL if none found.
 */
vtss_sprout__ri_t *vtss_sprout__ri_find_at_dist(
    vtss_sprout__rit_t                  *rit_p,
    const vtss_sprout__stack_port_idx_t sp_idx,
    const vtss_sprout_dist_t            dist);


/*
 * Set dist to infinity for all entries in sp_idx part of RIT.
 * If both distances become infinity, entry is deleted.
 */
void vtss_sprout__rit_infinity_all(
    vtss_sprout__rit_t                  *rit_p,
    const vtss_sprout__stack_port_idx_t sp_idx);

/*
 * Set dist to infinity for all entries in sp_idx part of RIT, if
 * the dist is larger than max_dist.
 * If both distances become infinity, entry is deleted.
 */
void vtss_sprout__rit_infinity_beyond_dist(
    vtss_sprout__rit_t                  *rit_p,
    const vtss_sprout__stack_port_idx_t sp_idx,
    const vtss_sprout_dist_t            max_dist);

/*
 * Set dist to infinity for any entries not found in SPROUT update.
 * Returns number of entries, where distance changed to infinity.
 * If both distances become infinity, entry is deleted.
 */
uint vtss_sprout__rit_infinity_if_not_found(
    vtss_sprout__rit_t                  *rit_p,
    const vtss_sprout__stack_port_idx_t sp_idx);

/*
 * Clear flags "found" and "changed" in RIT table.
 * Must be called prior to updating due to SPROUT Update reception.
 */
void vtss_sprout__rit_clr_flags(
    vtss_sprout__rit_t               *rit_p);

/*
 * Get pointer to next RI.
 * If argument is NULL, pointer to 1st RI is returned.
 * NULL is returned when reaching end of table.
 */
vtss_sprout__ri_t *vtss_sprout__ri_get_nxt(
    vtss_sprout__rit_t *rit_p,
    vtss_sprout__ri_t  *ri_p);

/*
 * Get distance to unit through specific stack port.
 * VTSS_SPROUT_DIST_INFINITY is returned if no such path.
 */
vtss_sprout_dist_t vtss_sprout__get_dist(
    vtss_sprout__rit_t                  *rit_p,
    const vtss_sprout__stack_port_idx_t sp_idx,
    const vtss_sprout_switch_addr_t     *switch_addr,
    const vtss_sprout__unit_idx_t       unit_idx);


/* ======================================================================== */


/* ===========================================================================
 * UIT - External functions
 * ------------------------------------------------------------------------ */

/*
 * Update existing entry or insert new entry.
 * Returns TRUE if any change was made.
 */
BOOL vtss_sprout__uit_update(
    vtss_sprout__uit_t      *uit_p,
    const vtss_sprout__ui_t *ui_p,
    BOOL                    pdu_rx);


/*
 * Find UIT entry based on switch address and unit index.
 * Returns NULL if not found.
 */
vtss_sprout__ui_t *vtss_sprout__ui_find(
    vtss_sprout__uit_t              *uit_p,
    const vtss_sprout_switch_addr_t *switch_addr_p,
    const vtss_sprout__unit_idx_t   unit_idx);


void vtss_sprout__uit_clr_flags(
    vtss_sprout__uit_t                 *uit_p);


/*
 * Delete any entries not found in SPROUT update (i.e. found=0).
 * Return number of entries deleted.
 */
uint vtss_sprout__uit_del_unfound(
    vtss_sprout__uit_t *uit_p);


/*
 * Get pointer to next UI.
 * If argument is NULL, pointer to 1st UI is returned.
 * NULL is returned when reaching end of table.
 */
vtss_sprout__ui_t *vtss_sprout__ui_get_nxt(
    vtss_sprout__uit_t *uit_p,
    vtss_sprout__ui_t  *ui_p);
/* ======================================================================== */


/* ===========================================================================
 * External debug functions
 * ------------------------------------------------------------------------ */

/* DEBUG */

/* ======================================================================== */


#endif /* _VTSS_SPROUT_XIT_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
