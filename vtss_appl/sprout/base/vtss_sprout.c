/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

 This file is part of SPROUT - "Stack Protocol using ROUting Technology".
*/


/*
 * SPROUT Files
 * ------------
 * vtss_sprout_api.h:    API definitions.
 * vtss_sprout.c/h:      Main Topo functionality.
 * vtss_sprout_xit.c/h:  Functionality for handling UITs and RIT.
 * vtss_sprout_util.c/h: Various utility functions.
 *
 * SPROUT coding style
 * -------------------
 * "vtss_sprout__" is used as prefix for non-static functions, which are intended
 * only for use internally within the the Topo module
 *
 * "_p" is used as postfix for pointer variables.
 *
 * Use of trace levels
 * -------------------
 * ERROR:   Should never happen.
 * WARNING: Potentially an error, but manual inspection required.
 * INFO:    Topology changes.
 * DEBUG:   API function called + packet dump.
 * NOISE:   The rest.
 */

/*
 * Flow
 * ====
 *
 * Main flow for PDU reception
 * ---------------------------
 * vtss_sprout_rx_pkt() ->
 *   rx_sprout_update() ->
 *     vtss_sprout__uit_update(link uit)
 *     vtss_sprout__rit_update()
 *     vtss_sprout__uit_del_unfound(link uit)
 *     vtss_sprout__rit_infinity_if_not_found()
 *     upsids_chk_and_recalc() => uit.changed + uit.change_mask
 *   process_xit_changes() ->
 *     merge_uits() => Copies ui from sp with lowest dist. => change_mask lost
 *   state_change_callback() ->
 *     clr_xit_flags()
 *       vtss_sprout__uit_clr_flags(link uits) => found=0, changed=0, change_mask=0
 *       vtss_sprout__uit_clr_flags(&uit);
 *       vtss_sprout__rit_clr_flags(&rit);
 */

/*
 * Dual unit switches
 * ==================
 *
 * Primary/Secondary Unit
 * ----------------------
 * SPROUT supports switches with two units (i.e. two chips).
 * This can be used to build a 48 port switch based on two switching chips.
 * It is assumed that SPROUT only runs on _one_ CPU within a dual unit switch.
 * The unit that this CPU is attached to is termed the primary unit, and the
 * other unit is termed the secondary unit.
 *
 * Stack port numbering
 * --------------------
 * Stack ports are indexed 0 and 1, or VTSS_SPROUT__SP_A/B.
 * For dual unit 48 port switches, stack port 1 (=VTSS_SPROUT__SP_B)
 * refers to the internal stack port on both the primary and secondary unit.
 * Stack port 0 (=VTSS_SPROUT__SP_A) is thus the external stack port on both
 * units.
 *
 * Data structures
 * ---------------
 * In dual unit mode, the SPROUT code only calculates UITs/RIT for the primary
 * unit.
 * The stack port state for the secondary unit is unused, except for ttl and
 * mirror_fwd information.
 * The adm_up/link_up/proto_up information stored for the internal stack port
 * on the primary unit is in fact the information for the external stack port
 * on the secondary unit.
 * The SPROUT PDUs transmitted from the primary unit on the internal stack port
 * provide a topology view as seen from the secondary unit.
 */


/*
 * "TOETBD"s explained
 * -------------------
 * TOETBD-2U: Functionality missing to support dual chip 48 port
 *            (dual unit switch).
 * TOETBD-U2: Functionality missing to support single chip 48 port
 *            (one unit with two UPS).
 */

#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "vtss_sprout.h"

/* ===========================================================================
 * Compilation control
 * ------------------------------------------------------------------------ */

#ifndef VTSS_SPROUT_SPROUT_PERIODIC_UPDATES
#define VTSS_SPROUT_SPROUT_PERIODIC_UPDATES 1
#endif

#ifndef VTSS_SPROUT_SPROUT_TRIGGERED_UPDATES
#define VTSS_SPROUT_SPROUT_TRIGGERED_UPDATES 1
#endif

#ifndef VTSS_SPROUT_SPROUT_AGING
#define VTSS_SPROUT_SPROUT_AGING 1
#endif

// Debug feature
// When set, right-most byte in DMAC is incremented for each SPROUT PDU.
#ifndef VTSS_SPROUT_DEBUG_DMAC_INCR
#define VTSS_SPROUT_DEBUG_DMAC_INCR 0
#endif

/* ======================================================================== */


/* ===========================================================================
 * Static variables & constants
 * ------------------------------------------------------------------------ */

/*
 * MAC addresses used in SPROUT updates.
 * sprout_smac is initialized in vtss_sprout_switch_init.
 */
#if VTSS_SPROUT_DEBUG_DMAC_INCR
static       mesa_mac_t sprout_dmac = {{0x01, 0x01, 0xC1, 0x00, 0x00, 0x02}};
#else
static const mesa_mac_t sprout_dmac = {{0x01, 0x01, 0xC1, 0x00, 0x00, 0x02}};
#endif

static       mesa_mac_t sprout_smac = {{0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};

static const vtss_sprout_switch_addr_t null_switch_addr = {{0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};

static const uchar exbit_protocol_ssp[4]  = {0x88, 0x80, 0x00, 0x02};
static const uchar ssp_sprout[4]          = {0x00, 0x01, 0x00, 0x00};
#define            SPROUT_PDU_TYPE_UPDATE   1
static const uchar sprout_pdu_type_update = SPROUT_PDU_TYPE_UPDATE;

#define            SPROUT_PDU_TYPE_ALERT    2
static const uchar sprout_pdu_type_alert  = SPROUT_PDU_TYPE_ALERT;
#define            SPROUT_VER 2
static const uchar sprout_ver             = SPROUT_VER;

static const uchar zeros[VTSS_SPROUT_FW_VER_LEN] = {}; // Compiler will initialize to 0s

#define SPROUT_ALERT_TYPE_PROTODOWN 0x1

/* Section Types in SPROUT Update */
#define SPROUT_SECTION_TYPE_END    0x0 /* No more sections */
#define SPROUT_SECTION_TYPE_SWITCH 0x1

/* TLV Types in Switch Sections in SPROUT Updates */
#define SPROUT_SWITCH_TLV_TYPE_UNIT_BASE_INFO      0x1
#define SPROUT_SWITCH_TLV_TYPE_UPS_BASE_INFO       0x3
#define SPROUT_SWITCH_TLV_TYPE_SWITCH_BASE_INFO    0x4
#define SPROUT_SWITCH_TLV_TYPE_SWITCH_IP_ADDR      0x20 /* 32 */
#define SPROUT_SWITCH_TLV_TYPE_SWITCH_MST_ELECT    0x21 /* 33 */
#define SPROUT_SWITCH_TLV_TYPE_SWITCH_APPL_INFO    0x22 /* 34 */
#define SPROUT_SWITCH_TLV_TYPE_UPS_PORT_MASK       0x23 /* 35 */

/* Reserved bits within each TLV type */
#define SPROUT_SWITCH_TLV_UNIT_BASE_INFO_RSV_MASK         0x80 /* Bits 7 */
#define SPROUT_SWITCH_TLV_UPS_BASE_INFO_RSV_MASK          0xe0 /* Bits 7-5 */
#define SPROUT_SWITCH_TLV_SWITCH_BASE_INFO_RSV_MASK       0xfe /* Bits 7-1 */
#define SPROUT_SWITCH_TLV_SWITCH_MST_ELECT_BYTE4_RSV_MASK 0x78 /* Bits 6-3, byte 4 */

#define SPROUT_MAX_PDU_SIZE 1024 /* More than enough */

/* Configuration from application */
static vtss_sprout_init_t sprout_init;
static BOOL               sprout_init_done = 0;
static BOOL               switch_init_done = 0;

/* Internal setup */
typedef struct _internal_cfg_t {
    BOOL sprout_periodic_updates;
    BOOL sprout_triggered_updates;
    BOOL sprout_aging;
} internal_cfg_t;

static internal_cfg_t internal_cfg;

/* RIT and UIT */
vtss_sprout__rit_t rit;
vtss_sprout__uit_t uit;

/* Switch state, configuration and internal TOPO state */
vtss_sprout__switch_state_t switch_state;

#if (VTSS_TRACE_ENABLED)
/* Trace registration. Initialized by vtss_sprout_init() */
static vtss_trace_reg_t trace_reg = {
    VTSS_TRACE_MODULE_ID, "sprout", "SPROUT - Topology protocol (vtss_sprout.c)"
};

#ifndef VTSS_SPROUT_DEFAULT_TRACE_LVL
#define VTSS_SPROUT_DEFAULT_TRACE_LVL VTSS_TRACE_LVL_ERROR
#endif

static vtss_trace_grp_t trace_grps[TRACE_GRP_CNT] = {
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default",
        VTSS_SPROUT_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_PKT_DUMP */ {
        "pktdump",
        "Hex dump of tx'ed & rx'ed SPROUT packets (lvl=noise)",
        VTSS_SPROUT_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_TOP_CHG */ {
        "top_chg",
        "Topology change (lvl=info)",
        VTSS_SPROUT_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_MST_ELECT */ {
        "mstelect",
        "Master election",
        VTSS_SPROUT_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_UPSID */ {
        "upsid",
        "upsid assignment",
        VTSS_SPROUT_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CRIT */ {
        "crit",
        "Critical region enter/exit trace (lvl=racket)",
        VTSS_SPROUT_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_FAILOVER */ {
        "failover",
        "Selected failover related debug output with usec timestamp",
        VTSS_SPROUT_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP | VTSS_TRACE_FLAGS_USEC | VTSS_TRACE_FLAGS_RINGBUF
    },
    /* TRACE_GRP_STARTUP */ {
        "startup",
        "Selected startup related debug output",
        VTSS_SPROUT_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
};
#endif /* VTSS_TRACE_ENABLED */

// Some debug commands set this to explicitly ignore semaphores
// May be useful for debugging a system hanging due to semaphore issues
static BOOL ignore_semaphores = 0;

#define CRIT_IGNORE(cmd) {ignore_semaphores = 1; cmd; ignore_semaphores = 1;}

// Statically allocated SPROUT Alert ProtoDown PDU.
// Initialized in vtss_sprout_switch_init()
#define SPROUT_ALERT_PROTODOWN_PDU_LEN (24+8) // Excluding Exbit EType + VStaX2 header
static mesa_vstax_tx_header_t sprout_alert_protodown_vs2_hdr;
static uchar                  sprout_alert_protodown_pkt[SPROUT_ALERT_PROTODOWN_PDU_LEN];

/* ======================================================================== */


/* ===========================================================================
 * Internal functions
 * ------------------------------------------------------------------------ */

/* ====================
 * Forward declarations
 * ----------------- */
static mesa_rc process_xit_changes(
    BOOL allow_sprout_stack_port0,
    BOOL allow_sprout_stack_port1);

static char *dbg_info(void);

static void update_local_uis(void);

static mesa_rc stack_setup(void);
/* ================= */


static mesa_rc topo_protocol_error(
    const char *fmt, ...)
{
    va_list ap;
    char s[200] = "SPROUT protocol error: ";
    int len;

    len = strlen(s);

    va_start(ap, fmt);

    vsprintf(s + len, fmt, ap);
    T_E("%s\n", s);

    if (sprout_init.callback.log_msg) {
        sprout_init.callback.log_msg(s);
    }

    va_end(ap);

    return VTSS_OK;
} /* topo_protocol_error */


static BOOL is_odd(uint n)
{
    if ((n % 2) == 1) {
        return 1;
    } else {
        return 0;
    }
}

/*
 * Save configuration information to non-volatile memory
 */
static mesa_rc cfg_save(void)
{
    vtss_sprout_cfg_save_t  topo_cfg;
    vtss_sprout__unit_idx_t unit_idx;

    for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
        mesa_vstax_upsid_t upsid = switch_state.unit[unit_idx].upsid[0];
        VTSS_SPROUT_ASSERT_DBG_INFO(VTSS_VSTAX_UPSID_LEGAL(upsid) ||
                                    upsid == VTSS_VSTAX_UPSID_UNDEF,
                                    (" "));
        // Save base UPSID for each unit
        topo_cfg.upsid[unit_idx] = upsid;
    }

    return sprout_init.callback.cfg_save(&topo_cfg);
} /* cfg_save */


/* ======================
 * Topology Determination
 * ------------------- */

/*
 * Determine shortest path from
 *   (local switch, src_unit_idx)
 * to
 *   (dst_switch_addr, dst_unit_idx)
 *
 * Return value:
 * 0 => Stack port 0 of src_unit
 * 1 => Stack port 1 of src_unit
 * 2 => Local unit
 *
 * Note that for V2 equal cost paths may exist. In this case
 * the path returned is the multicast path.
 */
static uint get_shortest_path(
    const vtss_sprout__unit_idx_t          src_unit_idx,
    const vtss_sprout_switch_addr_t *const dst_switch_addr_p,
    const vtss_sprout__unit_idx_t          dst_unit_idx)
{
    vtss_sprout__ri_t *ri_p;
    vtss_sprout_dist_t sp_dist[2];

#if VTSS_SPROUT_MAX_LOCAL_CHIP_CNT == 1
    VTSS_SPROUT_ASSERT(src_unit_idx == 0, ("Impossible"));
#endif

    if (vtss_sprout__switch_addr_cmp(&switch_state.switch_addr,
                                     dst_switch_addr_p) == 0) {
        /* Local switch */
        if (src_unit_idx == dst_unit_idx) {
            /* Local unit */
            return 2;
        } else {
            /* Other unit, use internal port */
            return 1;
        }
    }

    /* Not local unit, so there has to be a path, via one or both stack ports */
    /* Determine shortest path to unit */
    ri_p = vtss_sprout__ri_find(&rit,
                                dst_switch_addr_p,
                                dst_unit_idx);
    VTSS_SPROUT_ASSERT_DBG_INFO(ri_p != NULL, (" "));

    sp_dist[0] = ri_p->stack_port_dist[0];
    sp_dist[1] = ri_p->stack_port_dist[1];

    VTSS_SPROUT_ASSERT_DBG_INFO(
        !(sp_dist[0] == VTSS_SPROUT_DIST_INFINITY &&
          sp_dist[1] == VTSS_SPROUT_DIST_INFINITY),
        ("get_shortest_path: RI dist is INFINITY for both stack ports?!\nri=%s",
         vtss_sprout__ri_to_str(ri_p)));

    /*
     * sp_dist from RIT is seen from primary unit
     * Adjust if source unit is sec. unit
     */
    if (src_unit_idx == 1) {
        /* Secondary unit's external stack port */
        sp_dist[0] =
            (ri_p->stack_port_dist[1] == VTSS_SPROUT_DIST_INFINITY) ?
            VTSS_SPROUT_DIST_INFINITY :
            ri_p->stack_port_dist[1] - 1;
        /* Secondary unit's internal stack port */
        sp_dist[1] =
            (ri_p->stack_port_dist[0] == VTSS_SPROUT_DIST_INFINITY) ?
            VTSS_SPROUT_DIST_INFINITY :
            ri_p->stack_port_dist[0] + 1;
    }


    if (sp_dist[0] != VTSS_SPROUT_DIST_INFINITY &&
        sp_dist[1] == VTSS_SPROUT_DIST_INFINITY) {
        /* Stack port 0 is only path */
        return 0;
    } else if (sp_dist[0] == VTSS_SPROUT_DIST_INFINITY &&
               sp_dist[1] != VTSS_SPROUT_DIST_INFINITY) {
        /* Stack port 1 is only path */
        return 1;
    } else if (switch_state.unit[src_unit_idx].stack_port[0].ttl_new >=
               sp_dist[0]) {
        /* TTL of stack port 0 reaches unit */
        return 0;
    } else if (switch_state.unit[src_unit_idx].stack_port[1].ttl_new >=
               sp_dist[1]) {
        /* TTL of stack port 1 reaches unit */
        return 1;
    } else {
        T_E("No shortest path found (src_u_idx=%u, dst_sa=%s, dst_u_idx=%u)?!\n%s",
            src_unit_idx,
            vtss_sprout_switch_addr_to_str(dst_switch_addr_p),
            dst_unit_idx,
            dbg_info());
#if 0
        T_E(vtss_sprout__rit_to_str(&rit));
        T_E("sp_dist[0/1]=%d/%d", sp_dist[0], sp_dist[1]);
        T_E("ri=%s", vtss_sprout__ri_to_str(ri_p));
#endif
        return 0;
    }
} /* get_shortest_path */


/*
 * Determine topology based on RIT and updated switch_state accordingly
 */
static void topology_determination(void)
{
    /*
     * Criterias for the 3 topology types:
     *
     * VtssTopoBack2Back
     * -----------------
     * Exactly two units in RIT and the other unit is observed via both stack
     * links with a distance of 1
     * OR
     * One BF unit observing different units of the same tightly coupled AF switch
     * on both stack ports, i.e. 3 units in RIT.
     *
     * For VTSS_SPROUT_V2 VtssTopoBack2Back is not used.
     * Instead equal length forwarding of VStaX2/AF is on topology ClosedLoop.
     *
     * VtssTopoClosedLoop
     * ------------------
     * At least one unit in RIT can be reached via both stack links
     * (with a distance smaller than infinity)
     * or
     * the current unit can see itself via one or both of its stack links
     * (with a distance smaller than infinity).
     * N equals the number of units in RIT.
     *
     * VtssTopoOpenLoop
     * ----------------
     * Any topology not matching the criterias for VtssTopoBack2Back or VtssTopoClosedLoop.
     * The startup topology is VtssTopoOpenLoop1 (i.e. stand-alone configuration).
     * N equals the number of units in RIT.
     */
    vtss_sprout__ri_t *ri_p;
    BOOL found_myself_unit0 = 0;
    BOOL found_myself_unit1 = 0;

    T_N("topology_determination");

    /* Determine N */
    switch_state.topology_n = 0;
    ri_p = NULL;
    while ((ri_p = vtss_sprout__ri_get_nxt(&rit, ri_p))) {
        switch_state.topology_n++;

        if (vtss_sprout__switch_addr_cmp(&switch_state.switch_addr,
                                         &ri_p->switch_addr) == 0) {
            if (ri_p->unit_idx == 0) {
                found_myself_unit0 = 1;
            } else {
                found_myself_unit1 = 1;
            }
        }
    }

    if (!found_myself_unit0) {
        switch_state.topology_n++;;
    }
    if (!found_myself_unit1 && vtss_board_chipcount() == 2) {
        switch_state.topology_n++;
    }


    if (switch_state.topology_n == 1) {
        switch_state.topology_type_new = VtssTopoOpenLoop;
        return;
    }
    if (switch_state.topology_n == 2) {
        while ((ri_p = vtss_sprout__ri_get_nxt(&rit, ri_p))) {
            if (ri_p->stack_port_dist[0] == 1 &&
                ri_p->stack_port_dist[1] == 1) {
                switch_state.topology_type_new = VtssTopoClosedLoop;
                return;
            }
        }

        // Two units and not VtssTopoBack2Back/VtssTopoClosedLoop =>
        // Has to be VtssTopoOpenLoop
        switch_state.topology_type_new = VtssTopoOpenLoop;
        return;
    }
    /* Check for other unit being tightly coupled dual unit switch */
    if (switch_state.topology_n == 3) {
        vtss_sprout__ri_t *ri_p[2];

        ri_p[0] = vtss_sprout__ri_find_at_dist(&rit, 0, 1);
        ri_p[1] = vtss_sprout__ri_find_at_dist(&rit, 1, 1);

        if (ri_p[0] && ri_p[1] &&
            (vtss_sprout__switch_addr_cmp(&ri_p[0]->switch_addr,
                                          &ri_p[1]->switch_addr) == 0) &&
            ri_p[0]->tightly_coupled == 1 &&
            ri_p[1]->tightly_coupled == 1) {
            /* Back2Back with 3 units */
            switch_state.topology_type_new = VtssTopoClosedLoop;
            return;
        }
    }

    /* N > 2 */
    VTSS_SPROUT_ASSERT_DBG_INFO(switch_state.topology_n > 2, (" "));

    if (!found_myself_unit0) {
        switch_state.topology_type_new = VtssTopoOpenLoop;
        return;
    }
    while ((ri_p = vtss_sprout__ri_get_nxt(&rit, ri_p))) {
        if (ri_p->stack_port_dist[0] == VTSS_SPROUT_DIST_INFINITY ||
            ri_p->stack_port_dist[1] == VTSS_SPROUT_DIST_INFINITY) {
            switch_state.topology_type_new = VtssTopoOpenLoop;
            return;
        }
    }
    /* All units (including myself) can be reached in both
       ring directions => VtssTopoClosedLoop */
    switch_state.topology_type_new = VtssTopoClosedLoop;

    return;
} /* topology_determination */


/*
 * Calculate TTL values for the two stack ports based on
 * - topology_type_new
 * - topology_n
 * - top_link
 * - top_dist
 */
static void ttl_calc(void)
{
    vtss_sprout__ri_t             *ri_p = NULL;
    vtss_sprout_dist_t            dist = 0;
    uint                          N = switch_state.topology_n;
    vtss_sprout__stack_port_idx_t sp_idx;
    uint                          ttl[2][2]; // [unit][sp_a/b]
    vtss_sprout__unit_idx_t       unit_idx;

    VTSS_SPROUT_ASSERT_DBG_INFO(N != 0, (" "));

    T_N("ttl_calc: topology_type_new=%d, N=%d",
        switch_state.topology_type_new, N);

    switch (switch_state.topology_type_new) {
    case VtssTopoBack2Back:
        // V2 is using ClosedLoop with equal length paths instead of Back2Back
        VTSS_SPROUT_ASSERT_DBG_INFO(0, (" "));

        if (switch_state.topology_n == 2) {
            ttl[0][VTSS_SPROUT__SP_A] = 1;
            ttl[0][VTSS_SPROUT__SP_B] = 1;
        } else {
            /* Back2Back with tightly coupled switch */
            VTSS_SPROUT_ASSERT_DBG_INFO(switch_state.topology_n == 3, (" "));

            ttl[0][VTSS_SPROUT__SP_A] = 2;
            ttl[0][VTSS_SPROUT__SP_B] = 2;
        }
        break;

    case VtssTopoOpenLoop:
        for (sp_idx = 0; sp_idx < 2; sp_idx++) {
            dist = 0;
            while ((ri_p = vtss_sprout__ri_get_nxt(&rit, ri_p))) {
                if (ri_p->stack_port_dist[sp_idx] != VTSS_SPROUT_DIST_INFINITY &&
                    vtss_sprout__switch_addr_cmp(&ri_p->switch_addr, &switch_state.switch_addr) != 0 &&
                    ri_p->stack_port_dist[sp_idx] > dist) {
                    dist = ri_p->stack_port_dist[sp_idx];
                }
            }
            ttl[0][sp_idx] = dist;

            if (vtss_board_chipcount() == 2 && sp_idx == VTSS_SPROUT__SP_B) {
                // Internal TTL must be at least 1
                ttl[0][sp_idx] = MAX(dist, 1);
            }

            VTSS_SPROUT_ASSERT_DBG_INFO(dist < switch_state.topology_n,
                                        ("dist=%d, topology_n=%d, sp_idx=%d",
                                         dist, switch_state.topology_n, sp_idx));
        }

        // Secondary unit (if two units)
        // External stack port on secondary unit
        ttl[1][VTSS_SPROUT__SP_A] = ttl[0][VTSS_SPROUT__SP_B] - 1;
        // Internal stack port on secondary unit
        ttl[1][VTSS_SPROUT__SP_B] = ttl[0][VTSS_SPROUT__SP_A] + 1;

        if (ttl[0][VTSS_SPROUT__SP_A] + ttl[0][VTSS_SPROUT__SP_B] >= switch_state.topology_n) {
            // Sum of TTLs >= N
            // Must be an OpenLoop, which is becoming a ClosedLoop. But since we cannot
            // see ourself yet, it is still considered an OpenLoop.
            // Use same TTL algorithm as for ClosedLoop.

            T_I("Appears to be an OpenLoop becoming a ClosedLoop => Adjusting TTLs");

            if (is_odd(N)) {
                /* N is odd */
                ttl[0][VTSS_SPROUT__SP_A] = (N - 1) / 2;
                ttl[0][VTSS_SPROUT__SP_B] = (N - 1) / 2;
                ttl[1][VTSS_SPROUT__SP_A] = (N - 1) / 2;
                ttl[1][VTSS_SPROUT__SP_B] = (N - 1) / 2;
            } else {
                ttl[0][VTSS_SPROUT__SP_A] = N / 2 - 1;
                ttl[0][VTSS_SPROUT__SP_B] = N / 2;

                ttl[1][VTSS_SPROUT__SP_A] = N / 2 - 1;
                ttl[1][VTSS_SPROUT__SP_B] = N / 2;
            }
        }

        break;

    case VtssTopoClosedLoop:
        if (is_odd(N)) {
            /* N is odd */
            ttl[0][VTSS_SPROUT__SP_A] = (N - 1) / 2;
            ttl[0][VTSS_SPROUT__SP_B] = (N - 1) / 2;
            ttl[1][VTSS_SPROUT__SP_A] = (N - 1) / 2;
            ttl[1][VTSS_SPROUT__SP_B] = (N - 1) / 2;
        } else {
            // TTL calculation for "ClosedLoop, even number of units" is different
            // for V1 and V2, ref. TN0445.
            // SPROUT v2 (simpler, since no top switch considerations)
            // For dual-unit switches, assign the higher TTL value to the internal port
            // since it supports higher bandwidth.
            ttl[0][VTSS_SPROUT__SP_A] = N / 2 - 1;
            ttl[0][VTSS_SPROUT__SP_B] = N / 2;

            ttl[1][VTSS_SPROUT__SP_A] = N / 2 - 1;
            ttl[1][VTSS_SPROUT__SP_B] = N / 2;
        }

        break;
    default:
        /* What?! */
        VTSS_SPROUT_ASSERT_DBG_INFO(0, (" "));
    }

    for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
        if (switch_state.unit[unit_idx].stack_port[VTSS_SPROUT__SP_A].ttl != ttl[unit_idx][VTSS_SPROUT__SP_A]) {
            switch_state.unit[unit_idx].stack_port[VTSS_SPROUT__SP_A].cfg_change = 1;
            switch_state.unit[unit_idx].stack_port[VTSS_SPROUT__SP_A].ttl_new    = ttl[unit_idx][VTSS_SPROUT__SP_A];
        }
        if (switch_state.unit[unit_idx].stack_port[VTSS_SPROUT__SP_B].ttl != ttl[unit_idx][VTSS_SPROUT__SP_B]) {
            switch_state.unit[unit_idx].stack_port[VTSS_SPROUT__SP_B].cfg_change = 1;
            switch_state.unit[unit_idx].stack_port[VTSS_SPROUT__SP_B].ttl_new    = ttl[unit_idx][VTSS_SPROUT__SP_B];
        }
    }
} /* ttl_calc */


/*
 * Calculate mirror_fwd setting for each of the stack ports.
 * If configuration changes, set cfg_change for the stack port.
 */
static void mirror_calc(void)
{
    vtss_sprout__stack_port_state_t *sps_pa[VTSS_SPROUT_MAX_LOCAL_CHIP_CNT][2];
    vtss_sprout__ui_t               *ui_p     = NULL;
    BOOL                            mirror_fwd_new[VTSS_SPROUT_MAX_LOCAL_CHIP_CNT][2];
    uint                            sp_idx;

    vtss_sprout__unit_idx_t unit_idx;
    memset(mirror_fwd_new, 0, sizeof(mirror_fwd_new));

    /* Get convenient pointers to stack port states */
    sps_pa[0][0] = &switch_state.unit[0].stack_port[0];
    sps_pa[0][1] = &switch_state.unit[0].stack_port[1];
#if VTSS_SPROUT_MAX_LOCAL_CHIP_CNT > 1
    sps_pa[1][0] = &switch_state.unit[1].stack_port[0];
    sps_pa[1][1] = &switch_state.unit[1].stack_port[1];
#endif

    /* Look through UIT. If any unit has have_mirror=1, then determine which
     * stack port is used to reach the unit */
    while ((ui_p = vtss_sprout__ui_get_nxt(&uit, ui_p))) {
        if (ui_p->have_mirror) {
            uint sp_path; /* stack port path */

            switch (switch_state.topology_type_new) {
            case VtssTopoBack2Back:
                VTSS_SPROUT_ASSERT_DBG_INFO(vtss_board_chipcount() == 1, (" "));
                sp_path =
                    get_shortest_path(
                        0,
                        &ui_p->switch_addr,
                        ui_p->unit_idx);
                if (sp_path != 2) {
                    /* Both stack ports shall forward mirror frames */
                    mirror_fwd_new[0][0] = 1;
                    mirror_fwd_new[0][1] = 1;
                }
                break;

            case VtssTopoOpenLoop:
            case VtssTopoClosedLoop:
#if VTSS_SPROUT_MAX_LOCAL_CHIP_CNT > 1
                for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
#else
                for (unit_idx = 0; unit_idx < 1; unit_idx++) {
#endif
                    sp_path =
                        get_shortest_path(
                            unit_idx,
                            &ui_p->switch_addr,
                            ui_p->unit_idx);
                    if (sp_path != 2) {
                        mirror_fwd_new[unit_idx][sp_path] = 1;
                    }
                }
                break;

            default:
                /* What?! */
                VTSS_SPROUT_ASSERT_DBG_INFO(0, (" "));
            }
        }
    }



    for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
        for (sp_idx = 0; sp_idx < 2; sp_idx++) {
            if (sps_pa[unit_idx][sp_idx]->mirror_fwd != mirror_fwd_new[unit_idx][sp_idx]) {
                sps_pa[unit_idx][sp_idx]->cfg_change = 1;
                sps_pa[unit_idx][sp_idx]->mirror_fwd = mirror_fwd_new[unit_idx][sp_idx];
            }
        }
    }
} /* mirror_calc */


/* =================== */


/* ===============
 * Master Election
 * ------------ */

/* Get own master time */
static ulong my_mst_time(void)
{
    if (switch_state.mst) {
        return (sprout_init.callback.secs_since_boot() - switch_state.mst_start_time);
    } else {
        return 0;
    }
} /* my_mst_time */

/*
 * Determine master switch.
 *
 * Returns true, if new master is found.
 */
static BOOL mst_calc(void)
{
    /* Best master candidate */
    vtss_sprout__ui_t             *best_mst_ui_p = NULL;
    /* Master time of best master canditate */
    ulong                         best_mst_time  = 0;

    vtss_sprout__ui_t             *ui_p          = NULL;
    BOOL                          mst_time_ignore = 0;
    int                           n = 0;

#if VTSS_SPROUT_UNMGD
    return 0;
#endif

    T_DG(TRACE_GRP_MST_ELECT, "enter");
    T_DG(TRACE_GRP_MST_ELECT, "Current master: %s",
         vtss_sprout_switch_addr_to_str(&switch_state.mst_switch_addr));

    /* Check if any switches have set mst_time_ignore */
    while ((ui_p = vtss_sprout__ui_get_nxt(&uit, ui_p))) {
        mst_time_ignore |= ui_p->mst_time_ignore;
        n++;
    }

    T_DG(TRACE_GRP_MST_ELECT, "%d unit(s) in stack. mst_time_ignore=%d",
         n, mst_time_ignore);

    /* Loop through all units to find best master switch */
    ui_p = NULL;
    while ((ui_p = vtss_sprout__ui_get_nxt(&uit, ui_p))) {
        ulong mst_time   = 0;
        BOOL  local_unit = 0;

        /* Only consider primary units */
        if (ui_p->unit_idx != 0) {
            continue;
        }

        /* Skip if not master capable */
        if (!ui_p->mst_capable) {
            continue;
        }

        local_unit =
            vtss_sprout__switch_addr_cmp(&switch_state.switch_addr,
                                         &ui_p->switch_addr) == 0;

        if (local_unit) {
            /* Use own master time */
            mst_time = my_mst_time();
        } else {
            /* Use master time in UIT */
            mst_time = ui_p->mst_time;
        }

        if (best_mst_ui_p == NULL) {
            best_mst_ui_p = ui_p;
            best_mst_time = mst_time;
            T_DG(TRACE_GRP_MST_ELECT,
                 "1st master candidate: %s (mst_time=%u, elect_prio=%d)",
                 vtss_sprout_switch_addr_to_str(&best_mst_ui_p->switch_addr),
                 best_mst_time,
                 best_mst_ui_p->mst_elect_prio);
            continue;
        }

        if (!mst_time_ignore) {
            /* Look at master time. Largest win (but must be larger than minimum) */
            if (mst_time >= VTSS_SPROUT_MST_TIME_MIN &&
                mst_time > best_mst_time + VTSS_SPROUT_MST_TIME_DIFF_MIN) {
                /* Better due to mst_time */
                best_mst_ui_p = ui_p;
                best_mst_time = mst_time;
                T_DG(TRACE_GRP_MST_ELECT,
                     "New master candidate due to mst_time: %s (mst_time=%u, elect_prio=%d)",
                     vtss_sprout_switch_addr_to_str(&best_mst_ui_p->switch_addr),
                     mst_time,
                     ui_p->mst_elect_prio);

                continue;
            }
            if (best_mst_time >= VTSS_SPROUT_MST_TIME_MIN) {
                if (mst_time < VTSS_SPROUT_MST_TIME_MIN) {
                    T_DG(TRACE_GRP_MST_ELECT,
                         "Skipping master candidate due to mst_time <min: %s (mst_time=%u)",
                         vtss_sprout_switch_addr_to_str(&ui_p->switch_addr), mst_time);
                    continue;
                }
                if (best_mst_time > mst_time + VTSS_SPROUT_MST_TIME_DIFF_MIN) {
                    T_DG(TRACE_GRP_MST_ELECT,
                         "Skipping master candidate due to mst_time: %s (mst_time=%u)",
                         vtss_sprout_switch_addr_to_str(&ui_p->switch_addr), mst_time);
                    continue;
                }
            }

            if (best_mst_time > 0 &&
                ui_p->mst_time > 0) {
                /* Two units have master time > 0 */
                /* This can occur if you e.g. have a chain of three with the middle switch booting later. */
                T_EXPLICIT(VTSS_TRACE_MODULE_ID,
                           TRACE_GRP_MST_ELECT,
                           VTSS_TRACE_LVL_INFO,
                           __FUNCTION__,
                           __LINE__,

                           "Two units have master time larger than min(!)\n"
                           "Switch addr=%s\n"
                           "  Master time: %u\n"
                           "  Master elect prio: %d\n"
                           "Switch addr=%s\n"
                           "  Master time: %u\n"
                           "  Master elect prio: %d\n"
                           "Current master addr=%s",

                           vtss_sprout_switch_addr_to_str(&best_mst_ui_p->switch_addr),
                           best_mst_time,
                           best_mst_ui_p->mst_elect_prio,

                           vtss_sprout_switch_addr_to_str(&ui_p->switch_addr),
                           mst_time,
                           ui_p->mst_elect_prio,

                           vtss_sprout_switch_addr_to_str(&switch_state.mst_switch_addr));
            }
        }

        /* Look at election priority, smallest win */
        if (ui_p->mst_elect_prio < best_mst_ui_p->mst_elect_prio) {
            /* Better due to elect_prio */
            best_mst_ui_p = ui_p;
            best_mst_time = mst_time;
            T_DG(TRACE_GRP_MST_ELECT,
                 "New master candidate due to elect_prio: %s (mst_time=%u, elect_prio=%d)",
                 vtss_sprout_switch_addr_to_str(&best_mst_ui_p->switch_addr),
                 mst_time,
                 ui_p->mst_elect_prio);
            continue;
        } else if (ui_p->mst_elect_prio > best_mst_ui_p->mst_elect_prio) {
            T_DG(TRACE_GRP_MST_ELECT,
                 "Skipping master candidate due to elect_prio: %s",
                 vtss_sprout_switch_addr_to_str(&ui_p->switch_addr));
            continue;
        }

        /* Look at switch address, smallest win */
        if (vtss_sprout__switch_addr_cmp(&ui_p->switch_addr,
                                         &best_mst_ui_p->switch_addr) < 0) {
            /* Better due to switch_addr */
            best_mst_ui_p = ui_p;
            best_mst_time = mst_time;
            T_DG(TRACE_GRP_MST_ELECT,
                 "New master candidate due to switch_addr: %s",
                 vtss_sprout_switch_addr_to_str(&best_mst_ui_p->switch_addr));
            continue;
        }
    }

    if (!best_mst_ui_p) {
        /* No master found! */
        if (vtss_sprout__switch_addr_cmp(&switch_state.mst_switch_addr,
                                         &switch_addr_undef) == 0) {
            /* No change */
            VTSS_SPROUT_ASSERT_DBG_INFO(
                (switch_state.mst_upsid == VTSS_VSTAX_UPSID_UNDEF),
                ("%s", vtss_sprout__switch_state_to_str(&switch_state)));
            return 0;
        } else {
            /* There used to be a master */
            T_IG(TRACE_GRP_MST_ELECT,
                 "No master found\n"
                 "Previuos master was: %s\n"
                 "Prevoius master UPSID was: %d",
                 vtss_sprout_switch_addr_to_str(&switch_state.mst_switch_addr),
                 switch_state.mst_upsid);
            switch_state.mst_change_time = sprout_init.callback.secs_since_boot();
            switch_state.mst_switch_addr = switch_addr_undef;
            switch_state.mst_upsid       = VTSS_VSTAX_UPSID_UNDEF;
            switch_state.mst = 0;

            vtss_vstax_master_upsid_set(NULL, VTSS_VSTAX_UPSID_UNDEF);

            return 1;
        }
    } else if (vtss_sprout__switch_addr_cmp(&best_mst_ui_p->switch_addr,
                                            &switch_state.mst_switch_addr)) {
        /* Best master is different from current. I.e. new master found! */
        T_IG(TRACE_GRP_MST_ELECT,
             "New master found: %s\n"
             "Master UPSID: %d\n"
             "Master time: %u\n"
             "Master elect prio: %d",
             vtss_sprout_switch_addr_to_str(&best_mst_ui_p->switch_addr),
             best_mst_ui_p->upsid[0],
             best_mst_time,
             best_mst_ui_p->mst_elect_prio);

        switch_state.mst_change_time = sprout_init.callback.secs_since_boot();
        switch_state.mst_switch_addr = best_mst_ui_p->switch_addr;
        switch_state.mst_upsid       = best_mst_ui_p->upsid[0];

        if (vtss_sprout__switch_addr_cmp(&switch_state.mst_switch_addr,
                                         &switch_state.switch_addr) == 0) {
            /* We have become master! */
            switch_state.mst = 1;
            switch_state.mst_start_time = sprout_init.callback.secs_since_boot();
            T_IG(TRACE_GRP_MST_ELECT,
                 "We have become master!\n"
                 "switch_state.mst_start_time=%u",
                 switch_state.mst_start_time);
        } else {
            if (switch_state.mst) {
                T_IG(TRACE_GRP_MST_ELECT, "We are no longer master!");
                switch_state.mst = 0;
            }
        }

        vtss_vstax_master_upsid_set(NULL, switch_state.mst_upsid);

        return 1; /* New master found */
    } else {
        /* No master change */

        /* Check for master UPSID change */
        if (switch_state.mst_upsid != best_mst_ui_p->upsid[0]) {
            T_IG(TRACE_GRP_MST_ELECT,
                 "No master change, but master's UPSID has changed:\n"
                 "%d -> %d",
                 switch_state.mst_upsid, best_mst_ui_p->upsid[0]);

            switch_state.mst_upsid = best_mst_ui_p->upsid[0];
            vtss_vstax_master_upsid_set(NULL, switch_state.mst_upsid);
        }

        return 0;
    }
} /* mst_calc */


/* =================== */


/* =================
 * UPSID Calculation
 * -------------- */

/*
 * Get a random, unused UPSID
 *
 * In case of UPSID depletion, VTSS_VSTAX_UPSID_UNDEF is returned.
 * Function will update *upsid_inuse_mask (unless depletion is encountered)
 */
static mesa_vstax_upsid_t upsid_get_random_unused(
    BOOL                           *upsid_inuse_mask,
    const BOOL                     require_even_upsid)
{
    static BOOL        srand_called = 0;
    mesa_vstax_upsid_t upsid_start, upsid;
    int                i;

    if (!srand_called) {
        /*
         * Set seed for pseudo-random algorith.
         * Do not use any other contributions than switch address. We want consistent
         * failures if two units have same switch address.
         */
        srand((switch_state.switch_addr.addr[0] <<  8 | switch_state.switch_addr.addr[1] <<  0) ^
              (switch_state.switch_addr.addr[2] << 24 | switch_state.switch_addr.addr[3] << 16 |
               switch_state.switch_addr.addr[4] <<  8 | switch_state.switch_addr.addr[5] <<  0));
    }

    // Random UPSID from which to start search
    upsid_start = rand() % (VTSS_VSTAX_UPSID_MAX + 1);

    // Search for unused, even UPSID
    i = 0;
    for (i = 0; i <= VTSS_VSTAX_UPSID_MAX; i++) {
        upsid = (upsid_start + i) % (VTSS_VSTAX_UPSID_MAX + 1);
        if (upsid_inuse_mask[upsid]) {
            continue;
        }
        if (require_even_upsid && is_odd(upsid)) {
            continue;
        }

        upsid_inuse_mask[upsid] = 1;
        return upsid;
    };

    return VTSS_VSTAX_UPSID_UNDEF;
} /* upsid_get_random_unused */



/*
 * Set UPSIDs to random unused values.
 *
 * Returns SPROUT_RC_UPSID_DEPLETION if UPSID depletion is encountered.
 * Function will update *upsid_inuse_mask (unless depletion is encountered)
 */
static mesa_rc upsids_set_random_unused(
    BOOL *upsid_inuse_mask)
{
    mesa_vstax_upsid_t       upsid;
    vtss_sprout__upsid_idx_t ups_idx;

    /* Get unused, even UPSID */
    upsid = upsid_get_random_unused(upsid_inuse_mask, 1);
    if (upsid == VTSS_VSTAX_UPSID_UNDEF) {
        T_I("UPSID depletion encountered");
        return SPROUT_RC_UPSID_DEPLETION;
    }
    // Set consecutive UPSIDs within unit (if multiple UPSes)
    for (ups_idx = 0; ups_idx < VTSS_SPROUT_UNIT_UPS_CNT; ups_idx++) {
        switch_state.unit[0].upsid[ups_idx] = upsid + ups_idx;
    }

#if VTSS_SPROUT_MAX_LOCAL_CHIP_CNT > 1
#if VTSS_SPROUT_UNIT_UPS_CNT == 1
    if (vtss_board_chipcount() == 2) {
        // Two units with each one UPS => Use consecutive UPSIDs (48 port based on JR1)
        switch_state.unit[1].upsid[0] = upsid + 1;
    }
#else
    if (vtss_board_chipcount() == 2) {
        // Two units with each two UPSes => Use consecutive UPSIDs within each unit
        upsid = upsid_get_random_unused(upsid_inuse_mask, 1);
        if (upsid == VTSS_VSTAX_UPSID_UNDEF) {
            T_I("UPSID depletion encountered");
            return SPROUT_RC_UPSID_DEPLETION;
        }
        switch_state.unit[1].upsid[0] = upsid;
        switch_state.unit[1].upsid[1] = upsid + 1;
    }
#endif
#endif

    return VTSS_OK;
} /* upsids_set_random_unused */


/*
 * Initial calculation of UPSIDs (upon possible reception of initial SPROUT update)
 *
 * Returns SPROUT_RC_UPSID_DEPLETION if UPSID depletion is encountered.
 */
static mesa_rc upsids_initial_calc(
    /* UPSIDs in use in initial SPROUT update */
    BOOL *upsid_inuse_mask
)
{
    vtss_sprout__unit_idx_t  unit_idx;
    vtss_sprout__upsid_idx_t ups_idx;
    mesa_rc                  rc = VTSS_OK;
    BOOL                     upsids_changed = 0;

    if (upsid_inuse_mask[switch_state.unit[0].upsid[0]]
#if VTSS_SPROUT_MAX_LOCAL_CHIP_CNT == 2
        || upsid_inuse_mask[switch_state.unit[1].upsid[0]]
#endif
       ) {
        if (upsids_set_random_unused(upsid_inuse_mask) == SPROUT_RC_UPSID_DEPLETION) {
            T_I("UPSID depletion encountered");
            return SPROUT_RC_UPSID_DEPLETION;
        } else {
            upsids_changed = 1;
        }
    } else {
        /* 2nd UPSIDs must not be in use either */
        for (ups_idx = 0; ups_idx < VTSS_SPROUT_UNIT_UPS_CNT; ups_idx++) {
            VTSS_SPROUT_ASSERT_DBG_INFO(
                !upsid_inuse_mask[switch_state.unit[0].upsid[ups_idx]], (" "));

#if VTSS_SPROUT_MAX_LOCAL_CHIP_CNT > 1
            if (vtss_board_chipcount() == 2) {
                VTSS_SPROUT_ASSERT_DBG_INFO(
                    !upsid_inuse_mask[switch_state.unit[1].upsid[ups_idx]], (" "));
            }
#endif
        }
    }

    /* Configure UPSID in hardware */
    if ((rc = stack_setup()) < 0) {
        return rc;
    }

    update_local_uis();

    /* Save resulting UPSIDs (if changes) */
    if (upsids_changed) {

        uit.change_mask |= VTSS_SPROUT_STATE_CHANGE_MASK_UPSID_LOCAL;
        uit.changed      = 1;
        rit.changed      = 1;

        T_DG(TRACE_GRP_UPSID, "Calling cfg_save()");

        if ((rc = cfg_save()) < 0) {
            return rc;
        }
    }

    for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
        T_DG(TRACE_GRP_UPSID, "unit[%d].upsid[0]=%d",
             unit_idx, switch_state.unit[unit_idx].upsid[0]);
    }

    return VTSS_OK;
} /* upsids_initial_calc */


/*
 * Check (and possibly recalc) UPSIDs based on content of stack port UITs
 *
 * Returns SPROUT_RC_UPSID_DEPLETION if UPSID depletion is encountered.
 */
static mesa_rc upsids_chk_and_recalc(
    BOOL *upsids_changed_p)
{
    vtss_sprout__ui_t               *ui_p = NULL;
    vtss_sprout__stack_port_state_t *sps_p;
    vtss_sprout__unit_idx_t         unit_idx;
    vtss_sprout__upsid_idx_t        ups_idx;
    mesa_vstax_upsid_t              upsid;
    vtss_sprout__stack_port_idx_t   sp_idx;
    vtss_sprout__uit_t              *uit_p;
    BOOL                            upsid_inuse_mask[VTSS_SPROUT_MAX_UNITS_IN_STACK];
    BOOL                            upsid_recalc_needed = 0;
    mesa_rc                         rc = VTSS_OK;

    *upsids_changed_p = 0;

    memset(upsid_inuse_mask, 0, sizeof(upsid_inuse_mask));

    // Mark any local UPSID(s) as "in use"
    for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
        for (ups_idx = 0; ups_idx < VTSS_SPROUT_UNIT_UPS_CNT; ups_idx++) {
            VTSS_SPROUT_ASSERT_DBG_INFO(
                switch_state.unit[unit_idx].upsid[ups_idx] != VTSS_VSTAX_UPSID_UNDEF, (" "));
            upsid_inuse_mask[switch_state.unit[unit_idx].upsid[ups_idx]] = 1;
        }
    }

    // Check whether UPSID(s) of local units are also used by anybody else
    upsid_recalc_needed = 0;
    for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
        for (ups_idx = 0; ups_idx < VTSS_SPROUT_UNIT_UPS_CNT; ups_idx++) {
            upsid = switch_state.unit[unit_idx].upsid[ups_idx];

            /* Check whether upsid is being used by other unit in the stack */
            for (sp_idx = 0; sp_idx < 2; sp_idx++) {
                sps_p = &switch_state.unit[unit_idx].stack_port[sp_idx];

                if (sps_p->proto_up) {
                    uit_p = &switch_state.unit[unit_idx].stack_port[sp_idx].uit;
                    while ((ui_p = vtss_sprout__ui_get_nxt(uit_p, ui_p))) {
                        if (vtss_sprout__switch_addr_cmp(&switch_state.switch_addr,
                                                         &ui_p->switch_addr) == 0) {
                            continue;
                        }

                        if (ui_p->upsid[0] != VTSS_VSTAX_UPSID_UNDEF) {
                            upsid_inuse_mask[ui_p->upsid[0]] = 1;
                        }
                        if (ui_p->upsid[1] != VTSS_VSTAX_UPSID_UNDEF) {
                            upsid_inuse_mask[ui_p->upsid[1]] = 1;
                        }
                        if (upsid == ui_p->upsid[0] || upsid == ui_p->upsid[1]) {
                            /* Conflict found! */
                            T_DG(TRACE_GRP_UPSID, "Conflict with own UPSID=%d found", upsid);
                            if (vtss_sprout__switch_addr_cmp(&switch_state.switch_addr,
                                                             &ui_p->switch_addr) > 0) {
                                /* Our switch addr is the biggest => Lost */
                                T_DG(TRACE_GRP_UPSID, "Lost UPSID=%d", upsid);
                                upsid_recalc_needed = 1;
                            } else {
                                T_DG(TRACE_GRP_UPSID, "We won. Will keep UPSID=%d", upsid);
                            }
                        }
                    }
                }
            }
        }
    }

    if (upsid_recalc_needed) {
        if (upsids_set_random_unused(upsid_inuse_mask) == SPROUT_RC_UPSID_DEPLETION) {
            T_I("UPSID depletion encountered");
            return SPROUT_RC_UPSID_DEPLETION;
        }

        *upsids_changed_p = 1;
    }

    if (*upsids_changed_p) {
        rc = stack_setup(); /* Configure UPSID in hardware */

        uit.change_mask |= VTSS_SPROUT_STATE_CHANGE_MASK_UPSID_LOCAL;
        uit.changed      = 1;
        rit.changed      = 1;

        T_DG(TRACE_GRP_UPSID, "Calling cfg_save()");

        /* Save resulting UPSIDs (if changes) */
        if ((rc = cfg_save()) < 0) {
            return rc;
        }
    }

    return rc;
} /* upsids_chk_and_recalc */

/* -----------------
 * UPSID Calculation
 * ============== */


/* =====================
 * SPROUT PDU Generation
 * ------------------ */

/* Add byte to packet */
static void pkt_add_byte(
    uchar            *pkt_p,
    uint             *i_p,
    const uchar       byte)
{
    pkt_p[*i_p] = byte;
    *i_p += 1;

    VTSS_SPROUT_ASSERT_DBG_INFO(*i_p < SPROUT_MAX_PDU_SIZE, (" "));
} /* pkt_add_bytes */


/* Add bytes to packet */
static void pkt_add_bytes(
    uchar            *pkt_p,
    uint             *i_p,
    const uchar      *bytes,
    uint              byte_cnt)
{
    uint i = 0;

    for (i = 0; i < byte_cnt; i++) {
        pkt_add_byte(pkt_p, i_p, bytes[i]);
    }
} /* pkt_add_bytes */


static void pkt_add_1byte_tlv(
    uchar                        *pkt_p,
    uint                         *i_p,
    const uchar                  type,
    const vtss_sprout__unit_idx_t  unit_idx,
    const vtss_sprout__upsid_idx_t ups_idx,
    const uchar                  val)
{
    uchar byte;

    VTSS_SPROUT_ASSERT(type < 32, ("type=%d", type));
    VTSS_SPROUT_ASSERT(unit_idx <= 1, ("unit_idx=%d", unit_idx));
    VTSS_SPROUT_ASSERT(ups_idx  <= 1, ("ups_idx=%d", ups_idx));

    byte = ((type << 2) | (unit_idx << 1) | ups_idx);
    pkt_add_byte(pkt_p, i_p, byte);
    pkt_add_byte(pkt_p, i_p, val);
} /* pkt_add_1byte_tlv */


static void pkt_add_nbyte_tlv(
    uchar                          *pkt_p,
    uint                           *i_p,
    const uchar                    type,
    const vtss_sprout__unit_idx_t  unit_idx,
    const vtss_sprout__upsid_idx_t ups_idx,
    const uchar                    len,
    const uchar                    *val)
{
    uchar byte;
    int i;

    VTSS_SPROUT_ASSERT(type >= 32, ("type=%d", type));
    VTSS_SPROUT_ASSERT(unit_idx <= 1, ("unit_idx=%d", unit_idx));
    VTSS_SPROUT_ASSERT(ups_idx  <= 1, ("ups_idx=%d", ups_idx));
    VTSS_SPROUT_ASSERT(len > 1, ("len=%d", len));

    byte = ((type << 2) | (unit_idx << 1) | ups_idx);
    pkt_add_byte(pkt_p, i_p, byte);
    pkt_add_byte(pkt_p, i_p, len);
    for (i = 0; i < len; i++) {
        pkt_add_byte(pkt_p, i_p, val[i]);
    }
} /* pkt_add_nbyte_tlv */


/* Add MAC address to packet */
static void pkt_add_mac(
    uchar            *pkt_p,
    uint             *i_p,
    const mesa_mac_t *mac_p)
{
    pkt_add_bytes(pkt_p, i_p, mac_p->addr, 6);
} /* pkt_add_mac */


/* Initialize PDU with Ethernet and SPROUT header part */
static void pkt_init_sprout_hdr(
    uchar            *pkt_p,
    uint             *i_p,
    uchar            sprout_pdu_type)

{
    /* ---------- DMAC + SMAC ---------- */
    pkt_add_mac(pkt_p, i_p, &sprout_dmac);
#if VTSS_SPROUT_DEBUG_DMAC_INCR
    sprout_dmac.addr[5]++;
#endif
    pkt_add_mac(pkt_p, i_p, &sprout_smac);

    /* SMAC must be unicast MAC */
    VTSS_SPROUT_ASSERT((sprout_smac.addr[0] & 0x01) == 0,
                       ("sprout_smac.addr[0]=%02x", sprout_smac.addr[0]));

    /* ---------- Exbit protocol + SSP ---------- */
    pkt_add_bytes(pkt_p, i_p, exbit_protocol_ssp, 4);
    pkt_add_bytes(pkt_p, i_p, ssp_sprout,         4);

    /* ---------- SPROUT PDU ---------- */
    pkt_add_bytes(pkt_p, i_p, &sprout_pdu_type,   1);
    pkt_add_bytes(pkt_p, i_p, &sprout_ver,        1);
    pkt_add_bytes(pkt_p, i_p, zeros,              2);
} /* pkt_init_sprout_hdr */


static void pkt_init_vs2_hdr(
    mesa_vstax_tx_header_t *vs2_hdr_p,
    mesa_vstax_ttl_t        ttl,
    BOOL                    keep_ttl)
{
    memset(vs2_hdr_p, 0, sizeof(mesa_vstax_tx_header_t));
    vs2_hdr_p->fwd_mode    = MESA_VSTAX_FWD_MODE_CPU_ALL;
    vs2_hdr_p->ttl         = ttl;
    vs2_hdr_p->prio        = VTSS_PRIO_SUPER;
    vs2_hdr_p->upsid       = 1;
    vs2_hdr_p->tci.vid     = 1;
    vs2_hdr_p->tci.cfi     = 0;
    vs2_hdr_p->tci.tagprio = 0;
    vs2_hdr_p->port_no     = 0;
    vs2_hdr_p->glag_no     = VTSS_GLAG_NO_NONE;
    vs2_hdr_p->queue_no    = switch_state.cpu_qu;
    vs2_hdr_p->keep_ttl    = keep_ttl;
} /* pkt_init_vs2_hdr */


/* Change VStaX2 TTL in already formatted SPROUT packet */
static void pkt_change_ttl(
    mesa_vstax_tx_header_t *vs2_hdr_p,
    mesa_vstax_ttl_t        ttl)
{
    vs2_hdr_p->ttl = ttl;
} /* pkt_change_ttl */


/*
 * Add SPROUT
 * Add switch section for specified switch.
 * sp_idx is the port, which the update will be transmitted on.
 */
static void pkt_add_switch_section(
    const vtss_sprout_switch_addr_t     *switch_addr_p,
    const vtss_sprout__stack_port_idx_t sp_idx,
    uchar                             *pkt_p,
    uint                              *i_p)
{
    vtss_sprout_dist_t       dist[2] = { VTSS_SPROUT_DIST_INFINITY, VTSS_SPROUT_DIST_INFINITY };
    uint                     unit_cnt;
    vtss_sprout__ri_t        *ri_p;
    vtss_sprout__ui_t        *ui_p[2] = { NULL, NULL };
    vtss_sprout__unit_idx_t  unit_idx;
    vtss_sprout__upsid_idx_t ups_idx;
    BOOL                     local_switch = 0;
    vtss_sprout__uit_t       *uit_p;
    uchar                    val;
    BOOL                     sp_int = 0;
    int                      j;
    uint  len_i; /* Store value of i for calculation of len field in switch header */

    sp_int = (vtss_board_chipcount() == 2) && (sp_idx == VTSS_SPROUT__SP_B);

    local_switch = (vtss_sprout__switch_addr_cmp(switch_addr_p,
                                                 &switch_state.switch_addr) == 0);

    /* ----- Section type (1 byte) ----- */
    pkt_add_byte(pkt_p, i_p, SPROUT_SECTION_TYPE_SWITCH);

    /* ----- Length (1 byte) ----- */
    len_i = *i_p;
    *i_p += 1;

    /* ----- Switch address ----- */
    pkt_add_bytes(pkt_p, i_p, switch_addr_p->addr, 6);

    /* Determine number of units for switch */
    if (vtss_sprout__switch_addr_cmp(switch_addr_p,
                                     &switch_state.switch_addr) == 0) {
        unit_cnt = vtss_board_chipcount();
    } else {
        if (vtss_sprout__ri_find(&rit, switch_addr_p, 1)) {
            /* Unit 1 found in RIT, so has to be dual unit switch */
            unit_cnt = 2;
        } else {
            unit_cnt = 1;
        }
    }

    /* Determine dist */
    if (local_switch) {
        if (unit_cnt == 1) {
            /* For this switch, dist is 0 */
            dist[0] = 0;
        } else {
            VTSS_SPROUT_ASSERT_DBG_INFO(unit_cnt == 2, (" "));

            if (!sp_int) {
                // External stack port
                dist[0] = 0;
                dist[1] = 1;
            } else {
                // Internal stack port
                dist[0] = 1;
                dist[1] = 0;
            }
        }
    } else {
        /* For other switches, get dists from RIT */
        for (unit_idx = 0; unit_idx < 2; unit_idx++) {
            ri_p = vtss_sprout__ri_find(&rit, switch_addr_p, unit_idx);
            VTSS_SPROUT_ASSERT_DBG_INFO(unit_idx == 1 || ri_p != NULL, (" "));

            if (ri_p) {
                dist[unit_idx] = ri_p->stack_port_dist[(sp_idx + 1) % 2];

                if (sp_int) {
                    // Units are one hop further away when seen from sec. unit
                    dist[unit_idx]++;
                }
            }
        }
    }

    /* Determine UIT from which to extract information */
    if (local_switch) {
        uit_p = &uit;
    } else {
        uit_p = &switch_state.unit[0].stack_port[(sp_idx + 1) % 2].uit;
    }

    /* Get pointers to UIs */
    for (unit_idx = 0; unit_idx < 2; unit_idx++) {
        if (dist[unit_idx] != VTSS_SPROUT_DIST_INFINITY) {
            ui_p[unit_idx] = vtss_sprout__ui_find(uit_p, switch_addr_p, unit_idx);
            VTSS_SPROUT_ASSERT_DBG_INFO(ui_p[unit_idx] != NULL, ("switch_addr=%s\n%s",
                                                                 vtss_sprout_switch_addr_to_str(switch_addr_p),
                                                                 " "));
        } else {
            ui_p[unit_idx] = NULL;
        }
    }

    /* ----- TLV: UnitBaseInfo ----- */
    for (unit_idx = 0; unit_idx < 2; unit_idx++) {
        if (dist[unit_idx] != VTSS_SPROUT_DIST_INFINITY) {
            val = ((ui_p[unit_idx]->primary_unit << 6) |
                   (ui_p[unit_idx]->have_mirror << 5) | dist[unit_idx] |
                   ui_p[unit_idx]->unit_base_info_rsv);
            pkt_add_1byte_tlv(pkt_p, i_p, SPROUT_SWITCH_TLV_TYPE_UNIT_BASE_INFO,
                              unit_idx, 0, val);
        }
    }

    /* ----- TLV: UpsBaseInfo ----- */
    for (unit_idx = 0; unit_idx < 2; unit_idx++) {
        if (dist[unit_idx] != VTSS_SPROUT_DIST_INFINITY) {
            for (ups_idx = 0; ups_idx < 2; ups_idx++) {
                if (ui_p[unit_idx]->upsid[ups_idx] != VTSS_VSTAX_UPSID_UNDEF) {
                    val = (ui_p[unit_idx]->upsid[ups_idx] |
                           ui_p[unit_idx]->ups_base_info_rsv[ups_idx]);
                    pkt_add_1byte_tlv(pkt_p, i_p, SPROUT_SWITCH_TLV_TYPE_UPS_BASE_INFO,
                                      unit_idx, ups_idx, val);
                }
            }
        }
    }

    /* ----- TLV: UpsPortMask ----- */
    {
        u64            ups_port_mask;
        uchar          val[8];

        // TLV supports port numbers up to 63
        VTSS_SPROUT_ASSERT(VTSS_PORT_ARRAY_SIZE <= 64, ("%d", VTSS_PORT_ARRAY_SIZE));

        for (unit_idx = 0; unit_idx < 2; unit_idx++) {
            if (dist[unit_idx] != VTSS_SPROUT_DIST_INFINITY) {
                for (ups_idx = 0; ups_idx < 2; ups_idx++) {
                    if (ui_p[unit_idx]->upsid[ups_idx] != VTSS_VSTAX_UPSID_UNDEF) {
                        // Get port mask for UPS
                        ups_port_mask = ui_p[unit_idx]->ups_port_mask[ups_idx];

                        // Add TLV to packet
                        for (j = 0; j < 8; j++) {
                            val[7 - j] = (ups_port_mask >> (j * 8)) & 0xff;
                        }
                        pkt_add_nbyte_tlv(pkt_p, i_p, SPROUT_SWITCH_TLV_TYPE_UPS_PORT_MASK,
                                          unit_idx, ups_idx, 8, val);
                    }
                }
            }
        }
    }

    /* ----- TLV: SwitchBaseInfo ----- */
    {
        val = (ui_p[0]->tightly_coupled |
               ui_p[0]->switch_base_info_rsv);
        pkt_add_1byte_tlv(pkt_p, i_p, SPROUT_SWITCH_TLV_TYPE_SWITCH_BASE_INFO,
                          0, 0, val);
    }

    /* ----- TLV: SwitchIpAddr ----- */
    {
        if (ui_p[0]->ip_addr != 0) {
            uchar ip_addr[4]; /* Network order */
            ip_addr[0] = (ui_p[0]->ip_addr & 0xff000000) >> 24;
            ip_addr[1] = (ui_p[0]->ip_addr & 0x00ff0000) >> 16;
            ip_addr[2] = (ui_p[0]->ip_addr & 0x0000ff00) >>  8;
            ip_addr[3] = (ui_p[0]->ip_addr & 0x000000ff) >>  0;

            pkt_add_nbyte_tlv(pkt_p, i_p, SPROUT_SWITCH_TLV_TYPE_SWITCH_IP_ADDR, 0, 0, 4, ip_addr);
        }
    }

    /* ----- TLV: SwitchMstElect ----- */
    {
        uchar val[5];
        ulong mst_time;

        if (local_switch) {
            /* Local unit */
            if (switch_state.mst_capable) {
                mst_time = my_mst_time();
                val[4] = ((switch_state.mst_time_ignore << 7) |
                          (switch_state.mst_capable << 2) |
                          switch_state.mst_elect_prio);
            } else {
                mst_time = 0;
                val[4] = ((switch_state.mst_time_ignore << 7) |
                          (switch_state.mst_capable << 2) |
                          0);
            }
            T_N("mst_time=%u", mst_time);
        } else {
            mst_time = ui_p[0]->mst_time;
            val[4] = ((ui_p[0]->mst_time_ignore << 7) |
                      (ui_p[0]->mst_capable << 2) |
                      ui_p[0]->mst_elect_prio |
                      ui_p[0]->switch_mst_elect_rsv);
        }
        val[0] = (mst_time & 0xff000000) >> 24;
        val[1] = (mst_time & 0x00ff0000) >> 16;
        val[2] = (mst_time & 0x0000ff00) >> 8;
        val[3] = (mst_time & 0x000000ff) >> 0;
        T_N("val=%02x%02x%02x%02x%02x",
            val[0], val[1], val[2], val[3], val[4]);
        pkt_add_nbyte_tlv(pkt_p, i_p, SPROUT_SWITCH_TLV_TYPE_SWITCH_MST_ELECT,
                          0, 0, 5, val);
    }

    /* ----- TLV: SwitchApplInfo ----- */
    // Only include TLV if value differs from all-zeros
    if (memcmp(ui_p[0]->switch_appl_info, zeros, VTSS_SPROUT_SWITCH_APPL_INFO_LEN) != 0) {
        pkt_add_nbyte_tlv(pkt_p, i_p, SPROUT_SWITCH_TLV_TYPE_SWITCH_APPL_INFO, 0, 0,
                          VTSS_SPROUT_SWITCH_APPL_INFO_LEN, ui_p[0]->switch_appl_info);
    }

    /* ----- len ----- */
    pkt_p[len_i] = (*i_p - len_i - 1);
} /* pkt_add_switch_section */


/*
 * Send SPROUT Update packet on specific stack port.
 */
static void tx_sprout_update(
    const vtss_sprout__stack_port_idx_t sp_idx,   /* tx port */
    const BOOL                          periodic
)
{
    uchar pkt_buf[SPROUT_MAX_PDU_SIZE];
    uchar *pkt_p;
    uint  byte_idx = 0;
    vtss_sprout__stack_port_state_t *sps_p;
    vtss_sprout__ri_t               *ri_p = NULL;
    mesa_vstax_tx_header_t          vs2_hdr;
    mesa_rc                         rc = VTSS_OK;

    pkt_p = pkt_buf;

    sps_p = &switch_state.unit[0].stack_port[sp_idx];

    T_N("tx_sprout_update: sp_idx=%d, adm_up=%d, link_up=%d, proto_up=%d",
        sp_idx,
        sps_p->adm_up, sps_p->link_up, sps_p->proto_up);

    VTSS_SPROUT_ASSERT_DBG_INFO(sps_p->link_up, (" "));

    pkt_init_vs2_hdr(&vs2_hdr, 1, 1);
    pkt_init_sprout_hdr(pkt_p, &byte_idx, sprout_pdu_type_update);

    /* ---------- SPROUT Update ---------- */
    /* SPROUT Update Header */
    pkt_add_bytes(pkt_p, &byte_idx, sps_p->nbr_switch_addr.addr,   6);
    pkt_add_bytes(pkt_p, &byte_idx, switch_state.switch_addr.addr, 6);
#if defined(VTSS_SPROUT_FW_VER_CHK)
    if (switch_state.fw_ver_mode == VTSS_SPROUT_FW_VER_MODE_NORMAL) {
        pkt_add_bytes(pkt_p, &byte_idx, switch_state.my_fw_ver,         80);
    } else {
        // VTSS_SPROUT_FW_VER_MODE_NULL
        pkt_add_bytes(pkt_p, &byte_idx, zeros,                          80);
    }
    pkt_add_bytes(pkt_p, &byte_idx, zeros,                          4);
#endif

    /* Switch Sections */
    /*
     * One section for this switch and one for each switch in RIT.
     * Start with this switch, then take the switches in RIT (RIT is sorted).
     */
    pkt_add_switch_section(&switch_state.switch_addr, sp_idx, pkt_p, &byte_idx);
    while ((ri_p = vtss_sprout__ri_get_nxt(&rit, ri_p))) {
        if (vtss_sprout__switch_addr_cmp(&ri_p->switch_addr,
                                         &switch_state.switch_addr)) {
            /* Not this switch */
            if (ri_p->stack_port_dist[(sp_idx + 1) % 2] != VTSS_SPROUT_DIST_INFINITY &&
                ri_p->stack_port_dist[(sp_idx + 1) % 2] != VTSS_SPROUT_MAX_UNITS_IN_STACK) {
                /* Use unit 0 entries as trigger for adding switch section */
                if (ri_p->unit_idx == 0) {
                    pkt_add_switch_section(&ri_p->switch_addr, sp_idx, pkt_p, &byte_idx);
                }
            }
        }
    }

    /* ---------- End section ---------- */
    pkt_add_byte(pkt_p, &byte_idx, SPROUT_SECTION_TYPE_END);
    pkt_add_byte(pkt_p, &byte_idx, 0);

    T_NG(    TRACE_GRP_PKT_DUMP, "Tx port: %u, size: %d", sps_p->port_no, byte_idx);
    T_NG_HEX(TRACE_GRP_PKT_DUMP, pkt_p, byte_idx);

    if (sps_p->update_bucket > 0) {
        T_N("Sending SPROUT Update on port=%u", sps_p->port_no);
        rc = sprout_init.callback.tx_vstax2_pkt(sps_p->port_no,
                                                &vs2_hdr,
                                                pkt_p,
                                                byte_idx);
        if (rc < 0) {
            sps_p->sprout_update_tx_err_cnt++;
        } else {
            if (periodic) {
                sps_p->sprout_update_periodic_tx_cnt++;
            } else {
                sps_p->sprout_update_triggered_tx_cnt++;
            }
        }

        T_IG(TRACE_GRP_STARTUP, "Sent SPROUT Update #%d (%s) on port=%u, rc=%d",
             sps_p->sprout_update_periodic_tx_cnt + sps_p->sprout_update_triggered_tx_cnt,
             periodic ? "periodic" : "triggered",
             sps_p->port_no, rc);

        sps_p->update_bucket--;
    } else {
        sps_p->sprout_update_tx_policer_drop_cnt++;
        T_N("tx_sprout_update: update_bucket empty");
    }

    if (rc != VTSS_OK) {
        T_N("tx_sprout_update: Unexpected return code: %d", rc);
    }

    sps_p->deci_secs_since_last_tx_update = 0;

    /* Do not return rc, it is already counted */
    return;
} /* tx_sprout_update */


/*
 * Send SPROUT Alert ProtoDown packet on specific stack port.
 */
static void tx_sprout_alert_protodown(
    const vtss_sprout__stack_port_idx_t sp_idx)
{
    mesa_vstax_tx_header_t          *vs2_hdr_p;
    uchar                           *pkt_p;
    vtss_sprout__stack_port_state_t *sps_p;
    mesa_rc                          rc = VTSS_OK;
    vtss_sprout__ri_t               *ri_p;
    uint                             dist;

    sps_p = &switch_state.unit[0].stack_port[sp_idx];

    vs2_hdr_p = &sprout_alert_protodown_vs2_hdr;
    pkt_p     = sprout_alert_protodown_pkt;

    VTSS_SPROUT_ASSERT_DBG_INFO(sps_p->link_up, (" "));

    // Calculate TTL to use for Alert:
    // Distance to remote unit furthest away on other stack link
    // TOETBD: Maybe this could be pre-calculated during sprout update reception
    dist = 0;
    ri_p = NULL;
    while ((ri_p = vtss_sprout__ri_get_nxt(&rit, ri_p))) {
        if (ri_p->stack_port_dist[sp_idx] != VTSS_SPROUT_DIST_INFINITY &&
            vtss_sprout__switch_addr_cmp(&ri_p->switch_addr, &switch_state.switch_addr) != 0 &&
            ri_p->stack_port_dist[sp_idx] > dist) {
            dist = ri_p->stack_port_dist[sp_idx];
        }
    }

    // Update TTL in pre-initialized VStaX2 header
    pkt_change_ttl(vs2_hdr_p, dist);

    T_D("sp_idx=%d, ttl=%d, adm_up=%d, link_up=%d, proto_up=%d",
        sp_idx,
        dist, sps_p->adm_up, sps_p->link_up, sps_p->proto_up);


    T_DG(    TRACE_GRP_PKT_DUMP, "Tx port: %u, size: %d", sps_p->port_no, SPROUT_ALERT_PROTODOWN_PDU_LEN);
    T_DG_HEX(TRACE_GRP_PKT_DUMP, pkt_p, SPROUT_ALERT_PROTODOWN_PDU_LEN);

    if (sps_p->alert_bucket > 0) {
        rc = sprout_init.callback.tx_vstax2_pkt(sps_p->port_no,
                                                vs2_hdr_p,
                                                pkt_p,
                                                SPROUT_ALERT_PROTODOWN_PDU_LEN);
        if (rc < 0) {
            sps_p->sprout_alert_tx_err_cnt++;
        } else {
            sps_p->sprout_alert_tx_cnt++;
        }

        sps_p->alert_bucket--;
    } else {
        sps_p->sprout_alert_tx_policer_drop_cnt++;
        T_D("alert_bucket empty");
    }

    if (rc != VTSS_OK) {
        T_D("Unexpected return code: %d", rc);
    }

    T_DG(TRACE_GRP_FAILOVER, "alert tx done");

    /* Do not return rc, it is already counted */
    return;
} /* tx_sprout_alert_protodown */


/* ---------------------
 * SPROUT PDU Generation
 * ================== */


/* =======================
 * SPROUT Update Reception
 * -------------------- */

static uchar pkt_get_byte(
    const uchar *pkt_p,
    uint *const i_p)
{
    uchar byte;

    byte = pkt_p[*i_p];
    *i_p += 1;

    return byte;
} /* pkt_get_byte */


static mesa_rc pkt_get_switch_addr(
    const uchar             *pkt_p,
    uint                    *i_p,
    const uint              len,
    vtss_sprout_switch_addr_t *switch_addr_p)
{
    uint i;

    if ((len - *i_p) < 6) {
        return VTSS_RC_ERROR;
    } else {
        for (i = 0; i < 6; i++) {
            switch_addr_p->addr[i] = pkt_get_byte(pkt_p, i_p);
        }
    }

    return VTSS_OK;
} /* pkt_get_switch_addr */


typedef struct _switch_section_t {
    /* Information per switch */
    vtss_sprout_switch_addr_t     switch_addr;
    BOOL                          mst_capable;
    vtss_sprout__mst_elect_prio_t mst_elect_prio;
    ulong                         mst_time;
    BOOL                          mst_time_ignore;

    /* Information per unit */
    BOOL               have_mirror[2];
    vtss_sprout_dist_t dist[2];
    BOOL               primary_unit[2];

    /* Information per unit, ups */
    mesa_vstax_upsid_t upsid[2][2];
    u64                ups_port_mask[2][2];

    /* Switch base info */
    BOOL                 tightly_coupled;

    /* IPv4 address. 0.0.0.0 = Unknown */
    ulong ip_addr;

    vtss_sprout_switch_appl_info_t switch_appl_info;

    /* Values in reserved bits */
    uchar unit_base_info_rsv[2];               /* Bits 7 */
    uchar ups_base_info_rsv[2][2];              /* Bits 7-5 */
    uchar switch_mst_elect_rsv;                 /* Bits 6-3 of byte 4 */
    uchar switch_base_info_rsv;                 /* Bits 7-1 */
} switch_section_t;


static void switch_section_init(
    switch_section_t *switch_section_p)
{
    vtss_sprout__unit_idx_t  unit_idx;
    vtss_sprout__upsid_idx_t ups_idx;

    memset(switch_section_p, 0, sizeof(switch_section_t));

    vtss_sprout_switch_addr_init(&switch_section_p->switch_addr);
    switch_section_p->mst_elect_prio = 0;

    for (unit_idx = 0; unit_idx < 2; unit_idx++) {

        switch_section_p->have_mirror[unit_idx] = 0;
        switch_section_p->dist[unit_idx] = VTSS_SPROUT_DIST_INFINITY;

        for (ups_idx = 0; ups_idx < 2; ups_idx++) {
            switch_section_p->upsid[unit_idx][ups_idx] = VTSS_VSTAX_UPSID_UNDEF;
        }
    }

    switch_section_p->ip_addr = 0;
} /* switch_section_init */


/*
 * Parse switch section of SPROUT update
 */
static mesa_rc pkt_get_switch_section(
    const mesa_port_no_t    port_no,
    const uchar             *pkt_p,
    uint                    *i_p,
    const uint              sect_len,
    switch_section_t        *switch_section_p)
{
    uchar byte;
    uint section_len;
    uint section_end; /* Index of 1st byte of next section */
    uchar tlv_cnt[64];

    memset(tlv_cnt, 0, sizeof(tlv_cnt));

    if (sect_len - *i_p < 8) {
        /* Not a full switch section header */
        topo_protocol_error(
            "Port %d: Incomplete section header",
            port_no);

        return VTSS_RC_ERROR;
    }

    section_len = pkt_get_byte(pkt_p, i_p);
    section_end = *i_p + section_len;

    if (sect_len - *i_p < section_len) {
        /* Section length reaches further than packet */
        topo_protocol_error(
            "Port %d: Packet length (%d) shorter than section length (%d)",
            port_no, sect_len - *i_p, section_len);

        return VTSS_RC_ERROR;
    }

    pkt_get_switch_addr(pkt_p, i_p, sect_len, &switch_section_p->switch_addr);

    /* TLVs */
    while (*i_p <= section_end - 2) {
        uint tlv_type;
        vtss_sprout__unit_idx_t  unit_idx;
        vtss_sprout__upsid_idx_t ups_idx;
        BOOL                     len;  /* Length of value field */

        byte = pkt_get_byte(pkt_p, i_p);

        tlv_type = (byte >> 2);
        unit_idx = ((byte & 0x2) >> 1);
        ups_idx  = (byte & 0x1);
        if (tlv_type < 32) {
            /* 1-byte TLV */
            len = 1;
        } else {
            /* n-byte TLV */
            len = pkt_get_byte(pkt_p, i_p);
        }

        switch (tlv_type) {
        case SPROUT_SWITCH_TLV_TYPE_UNIT_BASE_INFO:
            /* T_N("Found TLV: UnitBaseInfo"); */
            byte = pkt_get_byte(pkt_p, i_p);
            switch_section_p->primary_unit[unit_idx]       = ((byte & 0x40) >> 6);
            switch_section_p->have_mirror[unit_idx]        = ((byte & 0x20) >> 5);
            switch_section_p->dist[unit_idx]               = (byte & 0x1f);
            switch_section_p->unit_base_info_rsv[unit_idx] = (byte & SPROUT_SWITCH_TLV_UNIT_BASE_INFO_RSV_MASK);

            /* Practically, unit_idx=0 is always primary unit */
            VTSS_SPROUT_ASSERT_DBG_INFO(!unit_idx == switch_section_p->primary_unit[unit_idx],
                                        (" "));
            break;

        case SPROUT_SWITCH_TLV_TYPE_UPS_BASE_INFO:
            byte = pkt_get_byte(pkt_p, i_p);
            switch_section_p->upsid[unit_idx][ups_idx] = (byte & 0x1f);
            switch_section_p->ups_base_info_rsv[unit_idx][ups_idx] =
                (byte & SPROUT_SWITCH_TLV_UPS_BASE_INFO_RSV_MASK);
            break;

        case SPROUT_SWITCH_TLV_TYPE_UPS_PORT_MASK:
            if (len == 8) {
                int i;
                for (i = 7; i >= 0; i--) {
                    switch_section_p->ups_port_mask[unit_idx][ups_idx] |= ((u64)pkt_get_byte(pkt_p, i_p) << (i * 8));
                }
            } else {
                topo_protocol_error(
                    "Port %d: Syntax error: Length of switch info TLV is %d, expected 8",
                    port_no, len);
                return VTSS_RC_ERROR;
            }
            break;

        case SPROUT_SWITCH_TLV_TYPE_SWITCH_BASE_INFO:
            byte = pkt_get_byte(pkt_p, i_p);
            switch_section_p->tightly_coupled = (byte & 0x01);
            switch_section_p->switch_base_info_rsv =
                (byte & SPROUT_SWITCH_TLV_SWITCH_BASE_INFO_RSV_MASK);
            break;

        case SPROUT_SWITCH_TLV_TYPE_SWITCH_IP_ADDR:
            if (len == 4) {
                switch_section_p->ip_addr =
                    (pkt_get_byte(pkt_p, i_p) << 24 |
                     pkt_get_byte(pkt_p, i_p) << 16 |
                     pkt_get_byte(pkt_p, i_p) <<  8 |
                     pkt_get_byte(pkt_p, i_p) <<  0);
            } else {
                topo_protocol_error(
                    "Port %d: Syntax error: Length of IP Addr. TLV is %d, expected 4",
                    port_no, len);
                return VTSS_RC_ERROR;
            }
            break;

        case SPROUT_SWITCH_TLV_TYPE_SWITCH_APPL_INFO:
            if (len == 8) {
                int i;
                for (i = 0; i < VTSS_SPROUT_SWITCH_APPL_INFO_LEN; i++) {
                    switch_section_p->switch_appl_info[i] = pkt_get_byte(pkt_p, i_p);
                }
            } else {
                topo_protocol_error(
                    "Port %d: Syntax error: Length of switch info TLV is %d, expected 8",
                    port_no, len);
                return VTSS_RC_ERROR;
            }
            break;

        case SPROUT_SWITCH_TLV_TYPE_SWITCH_MST_ELECT:
            if (len == 5) {
                uchar byte;
                switch_section_p->mst_time =
                    (pkt_get_byte(pkt_p, i_p) << 24 |
                     pkt_get_byte(pkt_p, i_p) << 16 |
                     pkt_get_byte(pkt_p, i_p) <<  8 |
                     pkt_get_byte(pkt_p, i_p) <<  0);
                byte = pkt_get_byte(pkt_p, i_p);
                switch_section_p->mst_time_ignore = (byte & 0x80) >> 7;
                switch_section_p->mst_elect_prio  = (byte & 0x03);
                switch_section_p->mst_capable     = (byte & 0x04) >> 2;
                switch_section_p->switch_mst_elect_rsv =
                    (byte & SPROUT_SWITCH_TLV_SWITCH_MST_ELECT_BYTE4_RSV_MASK);
            } else {
                topo_protocol_error(
                    "Port %d: Syntax error: Length of Master Elect is %d, expected 5",
                    port_no, len);
                return VTSS_RC_ERROR;
            }
            break;

        default:
            /* Unknown TLV. Skip it.*/
            T_W("Port %u: Unknown TLV type: 0x%02x", port_no, tlv_type);

            *i_p = *i_p + len;
        }
        tlv_cnt[tlv_type]++;
    }

    if (*i_p != section_end) {
        topo_protocol_error(
            "Port %d: Syntax error in switch section.",
            port_no);
        T_N("*i_p=%d, section_end=%d", *i_p, section_end);

        return VTSS_RC_ERROR;
    }

    if (tlv_cnt[SPROUT_SWITCH_TLV_TYPE_UPS_BASE_INFO] > 0) {
        if (tlv_cnt[SPROUT_SWITCH_TLV_TYPE_UNIT_BASE_INFO] == 0) {
            topo_protocol_error(
                "Port %d: UnitBaseInfo TLV missing", port_no);
        }
    }

    {
        // Check that ups_port_masks do not overlap
        vtss_sprout__unit_idx_t  unit_idx;
        vtss_sprout__upsid_idx_t ups_idx;
        u64                      ups_port_mask = 0;
        for (unit_idx = 0; unit_idx < 2; unit_idx++) {
            for (ups_idx = 0; ups_idx < 2; ups_idx++) {
                if (ups_port_mask &
                    switch_section_p->ups_port_mask[unit_idx][ups_idx]) {
                    topo_protocol_error("Port %d: Overlapping ups_port_mask", port_no);
                    break;
                }
                ups_port_mask |= switch_section_p->ups_port_mask[unit_idx][ups_idx];
            }
        }
    }

    return VTSS_OK;
} /* pkt_get_switch_section */



static mesa_rc rx_sprout_update(
    const mesa_port_no_t                port_no,
    const vtss_sprout__stack_port_idx_t sp_idx,
    const uchar                         *pkt_p,
    const uint                          len)
{
    /* Variables for storing information until whole packet has been parsed */
    switch_section_t              switch_section[VTSS_SPROUT_MAX_UNITS_IN_STACK];
    vtss_sprout_switch_addr_t     sa_mine;
    vtss_sprout_switch_addr_t     sa_nbr;
    vtss_sprout_switch_addr_t     sa;
    uint                          ss_cnt = 0; /* Switch section count */
    uint                          byte_idx;
    mesa_rc                       rc = VTSS_OK;
    uint                          i, j;
    uchar                         section_type;
    BOOL                          found_end_section;
    BOOL                          found;
    vtss_sprout__stack_port_state_t *sps_p;
    vtss_sprout__ui_t               ui;
    vtss_sprout__ri_t               ri;
    vtss_sprout__unit_idx_t         u;
    uint                          cnt;
    BOOL                          upsids_changed = 0; /* Own UPSIDs have changed */
    /* Set this variable to force sprout update on port, no matter whether UIT/RIT
     * changes have occurred or protocol is up. */
    BOOL                          force_tx_sprout_update[2] = {0, 0};
    uchar                         byte;
    BOOL                          org_proto_up;
    vtss_sprout__upsid_idx_t      ups_idx;

    /* For dual unit switches, PDUs are always received on primary unit, i.e. unit 0 */
    sps_p = &switch_state.unit[0].stack_port[sp_idx];
    org_proto_up = sps_p->proto_up;

    T_IG(TRACE_GRP_STARTUP, "rx_sprout_update: sp_idx=%d, len=%d, adm_up=%d, link_up=%d",
         sp_idx, len, sps_p->adm_up, sps_p->link_up);
    T_N("rx_sprout_update: port_no=%u, sp_idx=%d, len=%d, adm_up=%d, link_up=%d",
        port_no, sp_idx, len, sps_p->adm_up, sps_p->link_up);
    T_NG(TRACE_GRP_PKT_DUMP, "rx_sprout_update: port_no=%u, sp_idx=%d, len=%d, adm_up=%d, link_up=%d",
         port_no, sp_idx, len, sps_p->adm_up, sps_p->link_up);
    T_NG_HEX(TRACE_GRP_PKT_DUMP, pkt_p, len);

    /* Any changes previously received must have been processed */
    VTSS_SPROUT_ASSERT_DBG_INFO(!rit.changed &&
                                !uit.changed &&
                                !switch_state.unit[0].stack_port[sp_idx].uit.changed,
                                (" "));

    sps_p->sprout_update_rx_cnt++;

    if (sps_p->adm_up == 0) {
        /* Don't process updates when AdmDown */
        return VTSS_OK;
    }

    if (sps_p->link_up == 0) {
        /* Ignore updates, if link has not been reported up */
        return VTSS_OK;
    }


    for (i = 0; i < VTSS_SPROUT_MAX_UNITS_IN_STACK; i++) {
        switch_section_init(&switch_section[i]);
    }

    /* Skip DMAC, SMAC, Exbit, SSP */
    byte_idx = 20;

    /* PDU Type must be "SPROUT Update" */
    byte = pkt_get_byte(pkt_p, &byte_idx);
    VTSS_SPROUT_ASSERT(byte == SPROUT_PDU_TYPE_UPDATE, ("PDU Type=%d", byte));

    /* Check SPROUT version */
    if ((byte = pkt_get_byte(pkt_p, &byte_idx)) != sprout_ver) {
        // Different SPROUT version, ignore packet.
        sps_p->sprout_update_rx_err_cnt++;
        topo_protocol_error(
            "Port %d: Unknown SPROUT version: 0x%02x",
            port_no, byte);
        return VTSS_OK;
    }

    /* Ignore reserved part of SPROUT Header */
    byte_idx += 2;

    /* Get my switch address(?) */
    if ((rc = pkt_get_switch_addr(pkt_p, &byte_idx, len, &sa_mine)) < 0) {
        sps_p->sprout_update_rx_err_cnt++;
        topo_protocol_error(
            "Port %d: Packet too short while parsing switch address in Update Header",
            port_no);
        return rc;
    }

    /* Get neighbour's switch address */
    if ((rc = pkt_get_switch_addr(pkt_p, &byte_idx, len, &sa_nbr)) < 0)  {
        sps_p->sprout_update_rx_err_cnt++;
        topo_protocol_error(
            "Port %d: Packet too short, while parsing neighbour switch address in Update Header",
            port_no);
        return rc;
    }

#if defined(VTSS_SPROUT_FW_VER_CHK)
    // Check fw_ver interoperability
    //
    // For performance reasons only check this when in state down, since
    // fw_ver should not change without having left state up.
    if (!sps_p->proto_up) {
        if (sprout_init.callback.fw_ver_chk(
                sps_p->port_no,
                1,
                (pkt_p + byte_idx)) != VTSS_RC_OK) {
            T_I("Ignoring SPROUT Update with not interoperable FW version.");
            return VTSS_OK;
        }
    }
    byte_idx += 84;
#endif

    /* Get sections */
    found_end_section = 0;
    while (!found_end_section &&
           byte_idx < len &&
           ss_cnt < VTSS_SPROUT_MAX_UNITS_IN_STACK) {
        section_type = pkt_get_byte(pkt_p, &byte_idx);

        if (section_type == SPROUT_SECTION_TYPE_SWITCH) {
            rc = pkt_get_switch_section(port_no, pkt_p, &byte_idx, len, &switch_section[ss_cnt++]);
            if (rc < 0) {
                sps_p->sprout_update_rx_err_cnt++;
                return rc;
            }
        } else if (section_type == SPROUT_SECTION_TYPE_END) {
            found_end_section = 1;
        } else {
            sps_p->sprout_update_rx_err_cnt++;
            T_W("Port %u: Unknown section type: %d", port_no, section_type);
            return VTSS_RC_ERROR;
        }
    }

    if (!found_end_section) {
        sps_p->sprout_update_rx_err_cnt++;
        topo_protocol_error(
            "Port %d: No end section before end-of-packet",
            port_no);
    }

    /* ---------- Check switch section content ---------- */
    /* Check that sa_nbr has a switch section with dist != INFINITY */
    found = 0;
    for (i = 0; i < ss_cnt && !found; i++) {
        if (vtss_sprout__switch_addr_cmp(&sa_nbr, &switch_section[i].switch_addr) == 0 &&
            switch_section[i].dist[0] != VTSS_SPROUT_DIST_INFINITY) {
            found = 1;
        }
    }
    if (!found) {
        sps_p->sprout_update_rx_err_cnt++;
        topo_protocol_error("Port %d: No switch section found for neighbour", port_no);
        return VTSS_RC_ERROR;
    }

    /* Check that there are not two switch sections for the same switch address */
    for (i = 0; i < ss_cnt; i++) {
        sa = switch_section[i].switch_addr;
        for (j = i + 1; j < ss_cnt; j++) {
            if (vtss_sprout__switch_addr_cmp(&sa, &switch_section[j].switch_addr) == 0) {
                sps_p->sprout_update_rx_err_cnt++;
                topo_protocol_error("Port %d: Two switch sections for same switch address.", port_no);
                return VTSS_RC_ERROR;
            }
        }
    }

    /* ---------- Process content of update ---------- */
    /* Check for proto_up */
    if (sps_p->proto_up) {
        VTSS_SPROUT_ASSERT_DBG_INFO(sps_p->link_up, (" "));

        /* Check whether address of neighbour is unchanged */
        if (vtss_sprout__switch_addr_cmp(&sps_p->nbr_switch_addr, &sa_nbr) != 0) {
            /* Neighbour address has changed! */
            T_I("Port %u: ProtoUp->ProtoDown, neighbour address changed (old: %s, new: %s)",
                port_no,
                vtss_sprout_switch_addr_to_str(&sps_p->nbr_switch_addr),
                vtss_sprout_switch_addr_to_str(&sa_nbr));
            sps_p->proto_up = 0;
        }

        /* Check whether neighbour still has me as his neighbour */
        if (vtss_sprout__switch_addr_cmp(&switch_state.switch_addr, &sa_mine) != 0) {
            /* Neighbour does not know me anymore! */
            T_I("Port %u: ProtoUp->ProtoDown, neighbour no longer knows me (he says I am %s)!",
                port_no, vtss_sprout_switch_addr_to_str(&sa_mine));
            sps_p->proto_up = 0;
        }

        if (vtss_sprout__switch_addr_cmp(&switch_state.switch_addr, &sa_nbr) == 0) {
            /*
             * Neighbour is myself.
             * Could be valid, but in such case we don't care about proto_up.
             * During testing loopback of own frames has been observed, thus
             * do not accept packets with myself as neighbour.
             */
            T_I("Port %u: ProtoUp->ProtoDown, neighbour is myself!",
                port_no);
            sps_p->proto_up = 0;
        }

        if (!sps_p->proto_up) {
            /* Flush UIT for the stack port */
            vtss_sprout__uit_init(&sps_p->uit);
            sps_p->uit.changed = 1;

            /* Set dist via stack port to infinity */
            vtss_sprout__rit_infinity_all(&rit, sp_idx);

            if (vtss_sprout__switch_addr_cmp(&sps_p->nbr_switch_addr, &null_switch_addr) != 0) {
                T_N("Send update to neighbour to try to reestablish neighbourship");
                force_tx_sprout_update[sp_idx] = 1;
            }
        }
    } else {
        /* proto_down. Check whether we are up now */

        if (switch_state.virgin) {
            /* First update after link up */
            BOOL upsid_mask[VTSS_SPROUT_MAX_UNITS_IN_STACK];

            if (switch_state.virgin) {
                T_I("Virgin");
            }

            /* Determine upsid to use */
            memset(upsid_mask, 0, sizeof(upsid_mask));
            for (i = 0; i < ss_cnt; i++) {
                for (u = 0; u < 2; u++) {
                    vtss_sprout__upsid_idx_t ups_idx;
                    for (ups_idx = 0; ups_idx < 2; ups_idx++) {
                        mesa_vstax_upsid_t upsid;
                        upsid = switch_section[i].upsid[u][ups_idx];
                        if (upsid != VTSS_VSTAX_UPSID_UNDEF) {
                            upsid_mask[upsid] = 1;
                        }
                    }
                }
            }

            VTSS_SPROUT_ASSERT_DBG_INFO(switch_state.unit[0].stack_port[0].proto_up == 0,
                                        (" "));
            VTSS_SPROUT_ASSERT_DBG_INFO(switch_state.unit[0].stack_port[1].proto_up == 0,
                                        (" "));

            rc = upsids_initial_calc(upsid_mask);
            if (rc == SPROUT_RC_UPSID_DEPLETION) {
                T_I("UPSID depletion encountered in first update reception");
                return VTSS_OK;
            } else if (rc < 0) {
                T_IG(TRACE_GRP_STARTUP, "upsids_initial_calc=%d", rc);
                return rc;
            }

            /* Now that we have a UPSID: Transmit update on other port, if link is up */
            if (switch_state.unit[0].stack_port[!sp_idx].link_up &&
                switch_state.unit[0].stack_port[!sp_idx].adm_up) {
                T_D("We now have an UPSID, so tx update on other stack port (sp_idx=%d)", !sp_idx);
                force_tx_sprout_update[!sp_idx] = 1;
            }
        }


        /* Ignore packets with myself as neighbour (see comment above) */
        if (vtss_sprout__switch_addr_cmp(&sa_nbr,  &switch_state.switch_addr) != 0) {
            if (vtss_sprout__switch_addr_cmp(&sa_nbr, &null_switch_addr) != 0 &&
                vtss_sprout__switch_addr_cmp(&sa_mine, &switch_state.switch_addr) == 0 &&
                sps_p->link_up == 1 &&
                sps_p->adm_up == 1) {
                /* Neighbour knows me => We are up! */
                T_I("Port %u: ProtoDown->ProtoUp (except if UPSID depletion)", port_no);

                sps_p->nbr_switch_addr = sa_nbr;
                force_tx_sprout_update[sp_idx] = 1;
                sps_p->proto_up = 1;
            } else if (sps_p->link_up == 1 && sps_p->adm_up == 1) {
                /* Neighbour does not know me, but may be trying to get protocol up => Respond */
                T_I("Neighbour does not know me. tx update sp_idx=%d\n"
                    "sa_nbr=%s sa_mine=%s sps_p->nbr_switch_addr=%s",
                    sp_idx,
                    vtss_sprout_switch_addr_to_str(&sa_nbr),
                    vtss_sprout_switch_addr_to_str(&sa_mine),
                    vtss_sprout_switch_addr_to_str(&sps_p->nbr_switch_addr));
                sps_p->nbr_switch_addr = sa_nbr;
                force_tx_sprout_update[sp_idx] = 1;
            }
        }
    } /* sps_p->proto_up */

    /* Always keep last seen neighbour address */
    sps_p->nbr_switch_addr = sa_nbr;

    if (sps_p->proto_up) {
        /* Count number of foreign units in update */
        uint foreign_unit_cnt = 0;

        for (i = 0; i < ss_cnt; i++) {
            for (u = 0; u < 2; u++) {
                if (switch_section[i].dist[u] != VTSS_SPROUT_DIST_INFINITY &&
                    vtss_sprout__switch_addr_cmp(&switch_section[i].switch_addr,
                                                 &switch_state.switch_addr) != 0) {
                    foreign_unit_cnt++;
                }
            }
        }
        T_N("foreign_unit_cnt=%d (ss_cnt=%d)", foreign_unit_cnt, ss_cnt);

        /* Update stack port UIT */
        for (i = 0; i < ss_cnt; i++) {
            BOOL switch_section_me =
                (vtss_sprout__switch_addr_cmp(&switch_section[i].switch_addr,
                                              &switch_state.switch_addr) == 0);

            for (u = 0; u < 2; u++) {
                switch_section_me = (vtss_sprout__switch_addr_cmp(&switch_section[i].switch_addr,
                                                                  &switch_state.switch_addr) == 0);
                if (switch_section[i].dist[u] != VTSS_SPROUT_DIST_INFINITY &&

                    /* Skip units w. path >= equal to number of units, unless ourself */
                    !(switch_section[i].dist[u] + 1 >= foreign_unit_cnt + 1 &&
                      !switch_section_me)) {
                    /*
                     * TOETBD:
                     * This code will cause generation of SPROUT update in situations where the
                     * only new information is that we can see ourself around the stack.
                     * The resulting SPROUT updates will thus be identical and could be left out.
                     */
                    vtss_sprout__ui_init(&ui);
                    ui.switch_addr = switch_section[i].switch_addr;
                    ui.unit_idx = u;
                    for (ups_idx = 0; ups_idx < 2; ups_idx++) {
                        ui.upsid[ups_idx]         = switch_section[i].upsid[u][ups_idx];
                        ui.ups_port_mask[ups_idx] = switch_section[i].ups_port_mask[u][ups_idx];
                    }

                    ui.have_mirror  = switch_section[i].have_mirror[u];
                    ui.primary_unit = switch_section[i].primary_unit[u];
                    /* Master elect fields */
                    ui.mst_capable     = switch_section[i].mst_capable;
                    ui.mst_elect_prio  = switch_section[i].mst_elect_prio;
                    ui.mst_time        = switch_section[i].mst_time;
                    ui.mst_time_ignore = switch_section[i].mst_time_ignore;

                    ui.tightly_coupled = switch_section[i].tightly_coupled;

                    ui.ip_addr = switch_section[i].ip_addr;

                    memcpy(ui.switch_appl_info, switch_section[i].switch_appl_info,
                           VTSS_SPROUT_SWITCH_APPL_INFO_LEN);

                    /* Reserved fields */
                    ui.unit_base_info_rsv = switch_section[i].unit_base_info_rsv[u];
                    memcpy(ui.ups_base_info_rsv,     switch_section[i].ups_base_info_rsv[u],     2);
                    ui.switch_mst_elect_rsv = switch_section[i].switch_mst_elect_rsv;
                    ui.switch_base_info_rsv = switch_section[i].switch_base_info_rsv;

                    T_N("dist=%d\n%s",
                        switch_section[i].dist[u],
                        vtss_sprout__ui_to_str(&ui));
                    vtss_sprout__uit_update(&sps_p->uit, &ui, 1);

                    vtss_sprout__ri_init(&ri);
                    ri.switch_addr     = switch_section[i].switch_addr;
                    ri.unit_idx        = u;
                    ri.upsid[0]        = switch_section[i].upsid[u][0];
                    ri.upsid[1]        = switch_section[i].upsid[u][1];
                    /* Add one to distance */
                    if (vtss_board_chipcount() == 1) {
                        ri.stack_port_dist[sp_idx] = switch_section[i].dist[u] + 1;
                    } else {
                        if (sp_idx == VTSS_SPROUT__SP_A) {
                            ri.stack_port_dist[sp_idx] = switch_section[i].dist[u] + 1;
                        } else {
                            // Received through external stack port on secondary unit
                            if (!switch_section_me) {
                                // Not this switch, i.e. just add 2
                                ri.stack_port_dist[sp_idx] = switch_section[i].dist[u] + 2;
                            } else {
                                // This switch.
                                if (u == 0) {
                                    // Primary unit
                                    ri.stack_port_dist[sp_idx] = switch_section[i].dist[u] + 2;
                                } else {
                                    // Secondary unit
                                    // Distance to secondary unit always 1.
                                    ri.stack_port_dist[sp_idx] = 1;
                                }
                            }
                        }

                    }
                    ri.tightly_coupled         = switch_section[i].tightly_coupled;

                    vtss_sprout__rit_update(&rit, &ri, sp_idx);
                }
            }
        } /* Update stack port UIT */

        /* Delete entries not found in SPROUT Update */
        vtss_sprout__uit_del_unfound(&sps_p->uit);
        cnt = vtss_sprout__rit_infinity_if_not_found(&rit, sp_idx);

        if (cnt > 0) {
            T_N("Dist changed to infinity for %d RIT entries", cnt);
        }
    }

    if (sps_p->proto_up && sps_p->uit.changed) {
        /* Check for UPSID collision */
        rc = upsids_chk_and_recalc(&upsids_changed);

        T_DG(TRACE_GRP_UPSID, "upsids_chk_and_recalc rc=%d sp_idx=%u", rc, sp_idx);

        if (rc == SPROUT_RC_UPSID_DEPLETION) {
            /* UPSID depletion => Do not enter ProtoUp */
            sps_p->proto_up = 0;

            /* Flush UIT for the stack port */
            vtss_sprout__uit_init(&sps_p->uit);

            /* Set dist via stack port to infinity */
            vtss_sprout__rit_infinity_all(&rit, sp_idx);

            /* Set neighbour sa to 0, so nbr knows we're down */
            vtss_sprout_switch_addr_init(&sps_p->nbr_switch_addr);

            if (org_proto_up) {
                /* Going down due to depletion */
                T_I("Going down due to UPSID depletion");
                sps_p->uit.changed = 1;

                force_tx_sprout_update[sp_idx] = 1;
                force_tx_sprout_update[!sp_idx] =
                    (switch_state.unit[0].stack_port[!sp_idx].link_up &&
                     switch_state.unit[0].stack_port[!sp_idx].adm_up);
            } else {
                /*
                 * We were not up and depletion causes us not to get up =>
                 * Do not tx forced updates
                 */
                T_I("Cancelling ProtoUp, due to UPSID depletion");
                force_tx_sprout_update[sp_idx]  = 0;
                force_tx_sprout_update[!sp_idx] = 0;
            }
        }
    }

    /* Process UIT/RIT changes and send update on other stack port */
    {
        BOOL allow_tx_sprout_update[2];

        if (switch_state.virgin && sps_p->proto_up) {
            switch_state.virgin = 0;
        }

        allow_tx_sprout_update[0] = ((sp_idx == 1 || upsids_changed) && !force_tx_sprout_update[0]);
        allow_tx_sprout_update[1] = ((sp_idx == 0 || upsids_changed) && !force_tx_sprout_update[1]);

        rc = process_xit_changes(allow_tx_sprout_update[0],
                                 allow_tx_sprout_update[1]);
        if (rc < 0) {
            return rc;
        }

        if (force_tx_sprout_update[0]) {
            T_N("Forced tx_sprout_update, sp_idx=0");
            tx_sprout_update(0, 0);
        }
        if (force_tx_sprout_update[1]) {
            T_N("Forced tx_sprout_update, sp_idx=1");
            tx_sprout_update(1, 0);
        }
        T_N("rx_sprout_update done");
    }


    /* Valid SPROUT update => Reset deci_secs_since_last_rx_update */
    sps_p->deci_secs_since_last_rx_update = 0;

    return rc;
} /* rx_sprout_update */


static mesa_rc rx_sprout_alert(
    const mesa_port_no_t                port_no,
    const vtss_sprout__stack_port_idx_t sp_idx,
    const uchar                         *pkt_p,
    const uint                          len)
{
    vtss_sprout__stack_port_state_t *sps_p;
    uint                             byte_idx;
    uchar                            byte;
    vtss_sprout_switch_addr_t        sa_sender;
    uchar                            alert_type;
    uchar                            alert_data;
    vtss_sprout__ri_t               *ri_p;
    vtss_sprout_dist_t               max_dist_to_sender;

    T_DG(TRACE_GRP_FAILOVER, "alert rx start %u", port_no);

    sps_p = &switch_state.unit[0].stack_port[sp_idx];

    T_NG(TRACE_GRP_PKT_DUMP, "sp_idx=%d, len=%d, adm_up=%d, link_up=%d",
         sp_idx, len, sps_p->adm_up, sps_p->link_up);
    T_NG_HEX(TRACE_GRP_PKT_DUMP, pkt_p, len);

    /* Any changes previously received must have been processed */
    VTSS_SPROUT_ASSERT_DBG_INFO(!rit.changed &&
                                !uit.changed &&
                                !switch_state.unit[0].stack_port[sp_idx].uit.changed,
                                (" "));

    sps_p->sprout_alert_rx_cnt++;

    if (sps_p->adm_up == 0) {
        /* Don't process Alerts when AdmDown */
        return VTSS_OK;
    }

    /* Skip DMAC, SMAC, Exbit, SSP */
    byte_idx = 20;

    /* PDU Type must be "SPROUT Alert" */
    byte = pkt_get_byte(pkt_p, &byte_idx);
    VTSS_SPROUT_ASSERT(byte == SPROUT_PDU_TYPE_ALERT, ("PDU Type=%d", byte));

    /* Check SPROUT version */
    if ((byte = pkt_get_byte(pkt_p, &byte_idx)) != SPROUT_VER) {
        // Different SPROUT version, ignore packet.
        topo_protocol_error("Port %d: Unknown SPROUT version: 0x%02x",
                            port_no, byte);
        sps_p->sprout_alert_rx_err_cnt++;
        return VTSS_OK;
    }

    /* Ignore reserved part of SPROUT Header */
    byte_idx += 2;

    /* Check min. packet length */
    if (len < byte_idx + 8 + 1) {
        /* Not enough to hold the Alert PDU */
        topo_protocol_error(
            "Port %d: Alert PDU too short, length=%d",
            len);
        sps_p->sprout_alert_rx_err_cnt++;
        return VTSS_OK;
    }

    /* Get sender's switch address */
    pkt_get_switch_addr(pkt_p, &byte_idx, len, &sa_sender);

    /* Get alert_type & alert_data */
    alert_type = pkt_get_byte(pkt_p, &byte_idx);
    alert_data = pkt_get_byte(pkt_p, &byte_idx);

    switch (alert_type) {
    case SPROUT_ALERT_TYPE_PROTODOWN:
        /* Ignore ProtoDown Alerts, if stack link is not in ProtoUp */
        T_I("sp_idx=%d, len=%d, adm_up=%d, link_up=%d, sender=%s",
            sp_idx, len, sps_p->adm_up, sps_p->link_up,
            vtss_sprout_switch_addr_to_str(&sa_sender));

        if (sps_p->proto_up == 0) {
            return VTSS_OK;
        }

        /* Ignore ProtoDown Alerts from ourself (highly unlikely!) */
        if (vtss_sprout__switch_addr_cmp(&sa_sender,
                                         &switch_state.switch_addr) == 0) {
            T_W("Received own ProtoDown Alert!?, sp_idx=%d", sp_idx);
            return VTSS_OK;
        }

        /* Determine largest distance to sender's unit(s) via receiving stack port */
        max_dist_to_sender = VTSS_SPROUT_DIST_INFINITY;
        ri_p = NULL;
        while ((ri_p = vtss_sprout__ri_get_nxt(&rit, ri_p))) {
            if (vtss_sprout__switch_addr_cmp(&sa_sender,
                                             &ri_p->switch_addr) == 0) {
                /* Sender found in RIT. Is it reachable via receiving stack port? */
                if (ri_p->stack_port_dist[sp_idx] != VTSS_SPROUT_DIST_INFINITY) {
                    /* Yes. Update max_dist if larger */
                    VTSS_SPROUT_ASSERT_DBG_INFO(max_dist_to_sender != ri_p->stack_port_dist[sp_idx],
                                                ("Two units with same distance=%d?!",
                                                 max_dist_to_sender));
                    if (max_dist_to_sender == VTSS_SPROUT_DIST_INFINITY ||
                        max_dist_to_sender < ri_p->stack_port_dist[sp_idx]) {
                        max_dist_to_sender = ri_p->stack_port_dist[sp_idx];
                    }
                }
            }
        }

        if (max_dist_to_sender == VTSS_SPROUT_DIST_INFINITY) {
            /* Ignore ProtoDown Alert */
            T_I("Ignoring ProtoDown Alert on sp_idx=%d, since sender unknown",
                sp_idx);
            return VTSS_OK;
        }

        /* Set the distance to any units further away than sender to infinity */
        vtss_sprout__rit_infinity_beyond_dist(&rit, sp_idx, max_dist_to_sender);

        /* Process changes, but do not trigger SPROUT Update */
        process_xit_changes(0, 0);

        T_DG(TRACE_GRP_FAILOVER,
             "rx_sprout_alert done sp_idx=%d",
             sp_idx);
        T_N("Done");
        break;

    default:
        topo_protocol_error("Unknown Alert type: 0x%02x", alert_type);
        return VTSS_OK;
        break;
    }

    T_DG(TRACE_GRP_FAILOVER, "alert rx end");

    return VTSS_OK;
} /* rx_sprout_alert */


/* -----------------------
 * SPROUT Update Reception
 * ==================== */

/*
 * Update configuration part of UIs for local units
 *
 * upsid is not affected by this, these must be updated using the
 * following functions:
 *   topo_ui_upsid_set
 */
static void update_local_uis(void)
{
    vtss_sprout__unit_idx_t  unit_idx;
    vtss_sprout__upsid_idx_t ups_idx;
    vtss_sprout__ui_t        ui;

    for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
        vtss_sprout__ui_init(&ui);

        ui.switch_addr     = switch_state.switch_addr;
        ui.unit_idx        = unit_idx;
        ui.mst_capable     = switch_state.mst_capable;
        ui.mst_elect_prio  = switch_state.mst_elect_prio;
        ui.mst_time_ignore = switch_state.mst_time_ignore;

        ui.have_mirror     = switch_state.unit[unit_idx].have_mirror_port;
        for (ups_idx = 0; ups_idx < VTSS_SPROUT_UNIT_UPS_CNT; ups_idx++) {
            ui.upsid[ups_idx]         = switch_state.unit[unit_idx].upsid[ups_idx];
            ui.ups_port_mask[ups_idx] = switch_state.unit[unit_idx].ups_port_mask[ups_idx];
        }
        if (unit_idx == 0) {
            // Unit 0 is always used as primary unit
            ui.primary_unit = 1;
        }
        if (unit_idx == 1) {
            // UIT is seen from primary unit, so secondary unit is reached
            // via internal stack port.
            ui.sp_idx = VTSS_SPROUT__SP_B;
        }


        ui.ip_addr = switch_state.ip_addr;

        memcpy(ui.switch_appl_info, switch_state.switch_appl_info,
               VTSS_SPROUT_SWITCH_APPL_INFO_LEN);

        // Update global UIT
        vtss_sprout__uit_update(&uit, &ui, 0);
    }
} /* update_local_uis */


/*
 * Merge stack port UITs into common UIT
 */
static void merge_uits(void)
{
    vtss_sprout__uit_t            *uit_sp_p[2];
    vtss_sprout__ui_t             *ui_p;
    vtss_sprout__ri_t             *ri_p;
    vtss_sprout__stack_port_idx_t sp_idx;
    u32                           change_mask;

    T_D("enter");

    // Get convenient pointers to the two UITs to merge.
    // Note: Dual unit switches only use UITs of stack ports of primary unit.
    uit_sp_p[0] = &switch_state.unit[0].stack_port[0].uit;
    uit_sp_p[1] = &switch_state.unit[0].stack_port[1].uit;

    /* Fast path */
    if (!rit.changed &&
        !uit_sp_p[0]->changed &&
        !uit_sp_p[1]->changed) {
        if (uit_sp_p[0]->mst_time_changed ||
            uit_sp_p[1]->mst_time_changed) {
            /* Only mst_time has changed */
            vtss_sprout__ui_t *cur_ui_p = NULL;
            vtss_sprout__ui_t *sp_ui_p = NULL;

            T_D("Only mst_time to be merged");

            /* Update mst_time in UIT with those from UIT-L0/L1 */
            while ((cur_ui_p = vtss_sprout__ui_get_nxt(&uit, cur_ui_p))) {
                if (vtss_sprout__switch_addr_cmp(&cur_ui_p->switch_addr,
                                                 &switch_state.switch_addr) != 0) {
                    /* Not local unit */
                    VTSS_SPROUT_ASSERT_DBG_INFO(cur_ui_p->sp_idx != VTSS_SPROUT__SP_UNDEF,
                                                (" "));
                    sp_ui_p = vtss_sprout__ui_find(uit_sp_p[cur_ui_p->sp_idx],
                                                   &cur_ui_p->switch_addr,
                                                   cur_ui_p->unit_idx);
                    VTSS_SPROUT_ASSERT_DBG_INFO(sp_ui_p != NULL, (" "));

                    cur_ui_p->mst_time = sp_ui_p->mst_time;
                }
            }
            uit.mst_time_changed = 1;
            return;
        } else {
            /* Nothing has changed! */
            T_D("Nothing to merge");
            return;
        }
    }
    /* End of fast path */

    /* Flush UIT */
    change_mask = uit.change_mask; // UPSID change may have caused mask to be set, so preserve.
    vtss_sprout__uit_init(&uit);
    uit.changed = 1;
    uit.change_mask = change_mask;

    /* Build new UIT */
    ui_p = NULL;
    ri_p = NULL;
    while ((ri_p = vtss_sprout__ri_get_nxt(&rit, ri_p))) {
        if (vtss_sprout__switch_addr_cmp(&ri_p->switch_addr,
                                         &switch_state.switch_addr) != 0) {
            /* Not local unit */
            VTSS_SPROUT_ASSERT_DBG_INFO(!(ri_p->stack_port_dist[VTSS_SPROUT__SP_A] == VTSS_SPROUT_DIST_INFINITY &&
                                          ri_p->stack_port_dist[VTSS_SPROUT__SP_B] == VTSS_SPROUT_DIST_INFINITY),
                                        (" "));

            /* Determine which stack port has shortest distance */
            if (ri_p->stack_port_dist[VTSS_SPROUT__SP_A] == VTSS_SPROUT_DIST_INFINITY) {
                sp_idx = VTSS_SPROUT__SP_B;
            } else if (ri_p->stack_port_dist[VTSS_SPROUT__SP_B] == VTSS_SPROUT_DIST_INFINITY) {
                sp_idx = VTSS_SPROUT__SP_A;
            } else if (ri_p->stack_port_dist[VTSS_SPROUT__SP_A] ==
                       ri_p->stack_port_dist[VTSS_SPROUT__SP_B]) {
                /* Same distance => Pick from stack port with lowest number */
                if (switch_state.unit[0].stack_port[VTSS_SPROUT__SP_A].port_no <
                    switch_state.unit[0].stack_port[VTSS_SPROUT__SP_B].port_no) {
                    sp_idx = VTSS_SPROUT__SP_A;
                } else {
                    sp_idx = VTSS_SPROUT__SP_B;
                }
            } else if (ri_p->stack_port_dist[VTSS_SPROUT__SP_A] <
                       ri_p->stack_port_dist[VTSS_SPROUT__SP_B]) {
                sp_idx = VTSS_SPROUT__SP_A;
            } else {
                sp_idx = VTSS_SPROUT__SP_B;
            }

            /* Pick information from stack port UIT */
            ui_p = vtss_sprout__ui_find(uit_sp_p[sp_idx],
                                        &ri_p->switch_addr,
                                        ri_p->unit_idx);
            VTSS_SPROUT_ASSERT_DBG_INFO(ui_p != NULL, (" "));
            /* topo_ui_print(ui_p); */

            /* Copy into UIT */
            ui_p->sp_idx = sp_idx;
            vtss_sprout__uit_update(&uit, ui_p, 0);
        }
    }

    update_local_uis();
} /* merge_uits */


// Force SPROUT down by sending SPROUT update to neighbour with
// wrong neighbour address
static mesa_rc force_proto_down(
    vtss_sprout__stack_port_idx_t sp_idx)
{
    mesa_rc                       rc = VTSS_OK;
    vtss_sprout__stack_port_state_t *sps_p;

    sps_p = &switch_state.unit[0].stack_port[sp_idx];

    VTSS_SPROUT_ASSERT_DBG_INFO(sps_p->proto_up, (" "));
    VTSS_SPROUT_ASSERT_DBG_INFO(sps_p->adm_up,   (" "));
    VTSS_SPROUT_ASSERT_DBG_INFO(sps_p->link_up,  (" "));

    /* Set neighbour sa to 0 to inform about adm down */
    vtss_sprout_switch_addr_init(&sps_p->nbr_switch_addr);

    /* Flush UIT for the stack port */
    vtss_sprout__uit_init(&sps_p->uit);
    sps_p->uit.changed = 1;

    /* Set dist via stack port to infinity */
    vtss_sprout__rit_infinity_all(&rit, sp_idx);

    /* Send updates */
    rc = process_xit_changes(1, 1);

    sps_p->proto_up = 0;

#if defined(VTSS_SPROUT_FW_VER_CHK)
    /* Notify application that previous FW check result is history */
    sprout_init.callback.fw_ver_chk(sps_p->port_no, 0, NULL);
#endif

    return rc;
} /* force_proto_down */


static mesa_rc stack_port_adm_state_change(
    vtss_sprout__stack_port_idx_t sp_idx,
    BOOL                          adm_up)
{
    vtss_sprout__stack_port_state_t *sps_p;
    mesa_rc                       rc = VTSS_OK;
    sps_p = &switch_state.unit[0].stack_port[sp_idx];

    T_N("stack_port_adm_state_change: sp_idx=%d, adm_up=%d",
        sp_idx, adm_up);

    if (sps_p->adm_up == adm_up) {
        /* No change => Ignore */
        return VTSS_OK;
    }

    if (!adm_up && sps_p->proto_up) {
        /* Going down */
        rc = force_proto_down(sp_idx);

#if defined(VTSS_SPROUT_FW_VER_CHK)
        /* Notify application that previous FW check result is history */
        sprout_init.callback.fw_ver_chk(sps_p->port_no, 0, NULL);
#endif
    } else {
        /* Coming up */
        VTSS_SPROUT_ASSERT_DBG_INFO(!sps_p->proto_up, (" "));

        if (sps_p->link_up) {
            /* Send 1st update */
            vtss_sprout_switch_addr_init(&sps_p->nbr_switch_addr);
            tx_sprout_update(sp_idx, 0);
        }
    }

    switch_state.unit[0].stack_port[sp_idx].adm_up = adm_up;

    return rc;
} /* stack_port_adm_state_change */


/*
 * Process link state change for stack link
 */
static mesa_rc stack_port_link_state_change(
    vtss_sprout__stack_port_idx_t sp_idx,
    BOOL                          link_up)
{
    vtss_sprout__stack_port_state_t *sps_p;
    vtss_sprout__stack_port_state_t *sps_other_p;
    mesa_rc                          rc = VTSS_OK;

    T_DG(TRACE_GRP_FAILOVER, "link state change, sp_idx=%d", sp_idx);

    sps_p       = &switch_state.unit[0].stack_port[sp_idx];
    sps_other_p = &switch_state.unit[0].stack_port[!sp_idx];

    T_DG(TRACE_GRP_FAILOVER,
         "stack_port_link_state_change:\n"
         "Args: sp_idx=%d, link_up=%d\n"
         "Current state: sps.adm_up=%d sps.link_up=%d",
         sp_idx, link_up,
         sps_p->adm_up, sps_p->link_up);

    T_I("stack_port_link_state_change:\n"
        "Args: sp_idx=%d, port_no=%u, link_up=%d\n"
        "Current state: sps.adm_up=%d sps.link_up=%d",
        sp_idx, sps_p->port_no, link_up,
        sps_p->adm_up, sps_p->link_up);

    if (sps_p->link_up == link_up) {
        /* No change => Ignore */
        T_N("Ignoring link state change - no change.\n%s", dbg_info());
        return VTSS_OK;
    }

    if (link_up) {
        /* Link coming up */
        T_I("Link coming up, sp_idx=%d, port_no=%u",
            sp_idx, sps_p->port_no);
        switch_state.unit[0].stack_port[sp_idx].link_up = link_up;

        VTSS_SPROUT_ASSERT_DBG_INFO(!sps_p->proto_up, (" "));

        if (sps_p->adm_up) {
            /* Send 1st update */
            vtss_sprout_switch_addr_init(&sps_p->nbr_switch_addr);
            tx_sprout_update(sp_idx, 0);
        }
    } else if (!link_up) {
        /* Link going down */
        T_I("Link going down, sp_idx=%d, port_no=%u",
            sp_idx, sps_p->port_no);
        switch_state.unit[0].stack_port[sp_idx].link_up = link_up;

        if (sps_p->proto_up) {
            /* Proto going down */
            VTSS_SPROUT_ASSERT_DBG_INFO(sps_p->adm_up, (" "));

            if (sps_other_p->proto_up) {
                /* The other stack link is up, so send SPROUT Alert ProtoDown */
                tx_sprout_alert_protodown(!sp_idx);
            }

            sprout_init.callback.thread_set_priority_normal();

            /* Flush UIT for the stack port */
            vtss_sprout__uit_init(&sps_p->uit);
            sps_p->uit.changed = 1;

            /* Set dist via stack port to infinity */
            vtss_sprout__rit_infinity_all(&rit, sp_idx);

            /* Send update on other port */
            rc = process_xit_changes((sp_idx != 0), (sp_idx != 1));

            sps_p->proto_up = 0;

            if (!sps_other_p->link_up) {
                // The other stack port is also down.
                // Go back to "virgin" state to start UPSID algorithm from scratch
                // on next Update PDU reception.
                switch_state.virgin = 1;
            }

            /* Delete knowledge of neighbour */
            vtss_sprout_switch_addr_init(&sps_p->nbr_switch_addr);
        }
#if defined(VTSS_SPROUT_FW_VER_CHK)
        /* Notify application that previous FW check result is history */
        sprout_init.callback.fw_ver_chk(sps_p->port_no, 0, NULL);
#endif
    }

    return VTSS_OK;
} /* stack_port_link_state_change */


/*
 * Clear changed flags in RIT/UITs
 */
static void clr_xit_flags(void)
{
    vtss_sprout__unit_idx_t       unit_idx;
    vtss_sprout__stack_port_idx_t sp_idx;

    T_R("clr_xit_flags: uit.changed=%d uit.change_mask=0x%x rit.changed=0x%x",
        uit.changed, uit.change_mask, rit.changed);

    for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
        for (sp_idx = 0; sp_idx < 2; sp_idx++) {
            vtss_sprout__uit_clr_flags(&switch_state.unit[unit_idx].stack_port[sp_idx].uit);
        }
    }

    vtss_sprout__uit_clr_flags(&uit);
    vtss_sprout__rit_clr_flags(&rit);

    return;
} /* clr_xit_flags */


/*
 * Configure stack
 */
static mesa_rc stack_setup(void)
{
    vtss_sprout__unit_idx_t unit_idx;
    vtss_vstax_conf_t       vstax_setup;
    mesa_rc                 rc;

    vtss_appl_api_lock();
    (void)vtss_vstax_conf_get(NULL, &vstax_setup);
    for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
        if (unit_idx == 0) {
            // Base UPSID of unit 0
            vstax_setup.upsid_0 = switch_state.unit[unit_idx].upsid[0];
        } else {
#if VTSS_SPROUT_MAX_LOCAL_CHIP_CNT > 1
            // Base UPSID of unit 1
            vstax_setup.upsid_1 = switch_state.unit[unit_idx].upsid[0];
#else
            VTSS_SPROUT_ASSERT(FALSE, ("Unreachable"));
#endif
        }
    }
    rc = vtss_vstax_conf_set(NULL, &vstax_setup);
    vtss_appl_api_unlock();
    return rc;
} /* stack_setup */


/*
 * Configure stacking port
 */
static mesa_rc stack_port_setup(
    vtss_sprout__unit_idx_t       unit_idx,
    vtss_sprout__stack_port_idx_t sp_idx)
{
    vtss_vstax_port_conf_t          vstax_port_setup;
    mesa_rc                         rc = VTSS_OK;
    vtss_sprout__stack_port_state_t *sps_p;
    BOOL                            stack_port_a = (sp_idx == VTSS_SPROUT__SP_A);
    mesa_chip_no_t                  chip_no = unit_idx;

    sps_p = &switch_state.unit[unit_idx].stack_port[sp_idx];

    T_D("enter, unit_idx=%d, sp_idx=%d, ttl_current=%d, ttl_new=%d, mirror_fwd=%d",
        unit_idx, sp_idx, sps_p->ttl, sps_p->ttl_new, sps_p->mirror_fwd);

    rc = vtss_vstax_port_conf_get(NULL, chip_no, stack_port_a, &vstax_port_setup);
    VTSS_SPROUT_ASSERT_DBG_INFO(rc >= 0, ("rc=%d", rc));

    vstax_port_setup.ttl    = sps_p->ttl_new;
    vstax_port_setup.mirror = sps_p->mirror_fwd;


    sps_p->cfg_change = 0;
    sps_p->ttl        = sps_p->ttl_new;

    rc = vtss_vstax_port_conf_set(NULL, chip_no, stack_port_a, &vstax_port_setup);
    VTSS_SPROUT_ASSERT_DBG_INFO(rc >= 0, ("stack_port_setup(%d, %d): rc=%d",
                                          unit_idx, sp_idx,
                                          rc));

    if (unit_idx == 0) {
        // Propagate stack port numbers to API
        vtss_vstax_conf_t vstax_setup;
        vtss_appl_api_lock();
        if (vtss_vstax_conf_get(NULL, &vstax_setup) == VTSS_RC_OK) {
            vstax_setup.port_0 = switch_state.unit[0].stack_port[0].port_no;
            vstax_setup.port_1 = switch_state.unit[0].stack_port[1].port_no;
            vtss_vstax_conf_set(NULL, &vstax_setup);
        }
        vtss_appl_api_unlock();
    }

    return rc;
} /* stack_port_setup */


/*
 * Process any changes to RIT/UITs
 */
static mesa_rc process_xit_changes(
    BOOL allow_sprout_stack_port0, /* Allow SPROUT update on stack port 0 */
    BOOL allow_sprout_stack_port1  /* Allow SPROUT update on stack port 1 */
)
{
    vtss_sprout__unit_idx_t          unit_idx;
    vtss_sprout__stack_port_idx_t    sp_idx;
    mesa_rc                          rc = VTSS_OK;
    vtss_sprout__stack_port_state_t *sps_pa[2];
    BOOL                             ttl_change = 0;

    sps_pa[0] = &switch_state.unit[0].stack_port[0];
    sps_pa[1] = &switch_state.unit[0].stack_port[1];

    T_N("process_xit_changes: allows=%d%d link_up=%d/%d "
        "uit_l.changed=%d/%d uit_l.mst_time_changed=%d/%d "
        "uit.changed/mask=%d/0x%x rit.changed=%d "
        "uit0.changed/mask=%d0x%x uit1.changed/mask=%d0x%x",
        allow_sprout_stack_port0, allow_sprout_stack_port1,
        sps_pa[0]->link_up, sps_pa[1]->link_up,
        sps_pa[0]->uit.changed, sps_pa[1]->uit.changed,
        sps_pa[0]->uit.mst_time_changed, sps_pa[1]->uit.mst_time_changed,
        uit.changed, uit.change_mask, rit.changed,
        sps_pa[0]->uit.changed, sps_pa[0]->uit.change_mask,
        sps_pa[1]->uit.changed, sps_pa[1]->uit.change_mask
       );

    /* Merge UIT-L0/L1 into UIT (if they contain any changes) */
    merge_uits();

    T_N("rit.change=%d, sps_pa[0/1]->uit.changed=%d/%d uit.changed=%d uit.change_mask=0x%x",
        rit.changed, sps_pa[0]->uit.changed, sps_pa[1]->uit.changed, uit.changed, uit.change_mask);

    /* Master calculation */
    if (uit.changed ||
        rit.changed ||
        uit.mst_time_changed) {
        BOOL mst = switch_state.mst;
        if (mst_calc()) {
            /* New master found */
            uit.change_mask |= VTSS_SPROUT_STATE_CHANGE_MASK_NEW_MST;
            if (mst != switch_state.mst) {
                if (switch_state.mst) {
                    /* We have become master */
                    uit.change_mask |= VTSS_SPROUT_STATE_CHANGE_MASK_ME_MST;
                } else {
                    /* We have become slave */
                    uit.change_mask |= VTSS_SPROUT_STATE_CHANGE_MASK_ME_SLV;
                }
            }
        }
    }

    T_N("rit.change=%d, sps_pa[0/1]->uit.changed=%d/%d uit.changed=%d uit.change_mask=0x%x",
        rit.changed, sps_pa[0]->uit.changed, sps_pa[1]->uit.changed, uit.changed, uit.change_mask);

    /* Fast path */
    if (!rit.changed &&
        !sps_pa[0]->uit.changed &&
        !sps_pa[1]->uit.changed &&
        !uit.changed &&
        !uit.change_mask /* Master change */
       ) {
        /* No changes to RIT/UITs, except possibly mst_time */

        if (sps_pa[0]->uit.mst_time_changed ||
            sps_pa[1]->uit.mst_time_changed ||
            uit.mst_time_changed) {
            T_N("process_xit_changes: Only mst_time change");
            /* Propagate mst_time change */
            clr_xit_flags();

            /*  Generate SPROUT updates */
            if (allow_sprout_stack_port0 && sps_pa[0]->proto_up) {
                VTSS_SPROUT_ASSERT_DBG_INFO(sps_pa[0]->adm_up && sps_pa[0]->link_up, (" "));
                if (internal_cfg.sprout_triggered_updates) {
                    tx_sprout_update(0, 0);
                }
            }
            if (allow_sprout_stack_port1 && sps_pa[1]->proto_up) {
                VTSS_SPROUT_ASSERT_DBG_INFO(sps_pa[1]->adm_up && sps_pa[1]->link_up, (" "));
                if (internal_cfg.sprout_triggered_updates) {
                    tx_sprout_update(1, 0);
                }
            }
        } else {
            T_N("process_xit_changes: No changes found, not even mst_time");
            clr_xit_flags();
        }
        return VTSS_OK;
    }
    /* End of fast path */

    if (rit.changed) {
        topology_determination();
        ttl_calc();
    }

    if (rit.changed || uit.changed) {
        mirror_calc();
    }

    if (uit.change_mask & VTSS_SPROUT_STATE_CHANGE_MASK_NEW_MST &&
        sprout_init.callback.mst_change) {
        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
        sprout_init.callback.mst_change(&switch_state.mst_switch_addr);
        VTSS_SPROUT_CRIT_ENTER_TBL_RD();
    }

    /* Stack port configuration changes with falling TTL */
    for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
        for (sp_idx = 0; sp_idx < 2; sp_idx++) {
            vtss_sprout__stack_port_state_t *sps_p;
            sps_p = &switch_state.unit[unit_idx].stack_port[sp_idx];

            if (sps_p->cfg_change &&
                sps_p->ttl_new < sps_p->ttl) {
                rc = stack_port_setup(unit_idx, sp_idx);
                ttl_change = 1;
                if (rc < 0) {
                    VTSS_SPROUT_ASSERT_DBG_INFO(rc < 0, ("rc=%d", rc));
                    return rc;
                }
            }
        }
    }

    /* Stack port configuration changes with rising or unchanged TTL */
    for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
        for (sp_idx = 0; sp_idx < 2; sp_idx++) {
            vtss_sprout__stack_port_state_t *sps_p;
            sps_p = &switch_state.unit[unit_idx].stack_port[sp_idx];

            if (sps_p->cfg_change &&
                sps_p->ttl_new >= sps_p->ttl) {
                ttl_change |= (sps_p->ttl_new != sps_p->ttl);
                rc = stack_port_setup(unit_idx, sp_idx);
                if (rc < 0) {
                    VTSS_SPROUT_ASSERT_DBG_INFO(rc < 0, ("rc=%d", rc));
                    return rc;
                }
            }
        }
    }

    /* Any outstanding stack port configuration changes */
    for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
        for (sp_idx = 0; sp_idx < 2; sp_idx++) {
            vtss_sprout__stack_port_state_t *sps_p;
            sps_p = &switch_state.unit[unit_idx].stack_port[sp_idx];

            if (sps_p->cfg_change) {
                rc = stack_port_setup(unit_idx, sp_idx);
                if (rc < 0) {
                    return rc;
                }
            }
        }
    }

    if (rit.changed) {
        /* Update UPSID routing table */
        mesa_vstax_upsid_t       upsid;
        vtss_sprout__unit_idx_t  unit_idx;
        vtss_sprout__upsid_idx_t ups_idx;
        vtss_sprout__ri_t        *ri_p = NULL;
        vtss_vstax_route_table_t upsid_route;

        // Configure topology settings for both units of switch
        for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
            // Default: No route to UPSIDs
            memset(&upsid_route, 0, sizeof(upsid_route));

            // Retrieve information from each unit in RIT
            while ((ri_p = vtss_sprout__ri_get_nxt(&rit, ri_p))) {
                // Values for UPSID route table for the UPSes of this unit
                BOOL use_stack_port_a = 0;
                BOOL use_stack_port_b = 0;

                // Distance to unit via stack port a/b
                vtss_sprout_dist_t dist_a, dist_b;

                BOOL switch_me =
                    (vtss_sprout__switch_addr_cmp(&ri_p->switch_addr,
                                                  &switch_state.switch_addr) == 0);

                BOOL unit_me = (switch_me && ri_p->unit_idx == unit_idx);

                if (unit_idx == 0) {
                    // Primary unit
                    dist_a = ri_p->stack_port_dist[VTSS_SPROUT__SP_A];
                    dist_b = ri_p->stack_port_dist[VTSS_SPROUT__SP_B];
                } else {
                    // Secondary unit
                    // dist_a is now distance through external port on sec. unit
                    // dist_b is now distance through internal port on sec. unit
                    if (!switch_me) {
                        // Not local primary unit
                        dist_b = (ri_p->stack_port_dist[VTSS_SPROUT__SP_A] == VTSS_SPROUT_DIST_INFINITY) ?
                                 VTSS_SPROUT_DIST_INFINITY :
                                 ri_p->stack_port_dist[VTSS_SPROUT__SP_A] + 1;
                        dist_a = (ri_p->stack_port_dist[VTSS_SPROUT__SP_B] == VTSS_SPROUT_DIST_INFINITY) ?
                                 VTSS_SPROUT_DIST_INFINITY :
                                 ri_p->stack_port_dist[VTSS_SPROUT__SP_B] - 1;
                    } else {
                        // Distances from local secondary unit to local primary unit
                        dist_b = 1;
                        dist_a = (ri_p->stack_port_dist[VTSS_SPROUT__SP_B] == VTSS_SPROUT_DIST_INFINITY) ?
                                 VTSS_SPROUT_DIST_INFINITY :
                                 ri_p->stack_port_dist[VTSS_SPROUT__SP_B] - 1;
                        T_I("dist_a=%d, dist_b=%d", dist_a, dist_b);
                    }
                }

                VTSS_SPROUT_ASSERT_DBG_INFO(dist_a != VTSS_SPROUT_DIST_INFINITY ||
                                            dist_b != VTSS_SPROUT_DIST_INFINITY, (" "));

                // Determine whether to use a or b for the UPSIDs of unit
                if (dist_b == VTSS_SPROUT_DIST_INFINITY) {
                    use_stack_port_a = 1;
                } else if (dist_a == VTSS_SPROUT_DIST_INFINITY) {
                    use_stack_port_b = 1;
                } else if (dist_a < dist_b) {
                    use_stack_port_a = 1;
                } else if (dist_a > dist_b) {
                    use_stack_port_b = 1;
                } else if (dist_a == dist_b) {
                    if (switch_state.topology_type_new == VtssTopoOpenLoop) {
                        // Unit is reachable by same distance via both ports, but we
                        // cannot (yet) see ourself, so (still) not a ClosedLoop.
                        // This should only be a temporary state.
                        // Use stack port with largest TTL
                        T_I("OpenLoop, but UPSID %d is reachable with same distance through both stack ports. "
                            "Using stack port with largest TTL.", ri_p->upsid[0]);
                        if (switch_state.unit[unit_idx].stack_port[VTSS_SPROUT__SP_A].ttl_new >
                            switch_state.unit[unit_idx].stack_port[VTSS_SPROUT__SP_B].ttl_new) {
                            use_stack_port_a = 1;
                        } else if (switch_state.unit[unit_idx].stack_port[VTSS_SPROUT__SP_A].ttl_new <
                                   switch_state.unit[unit_idx].stack_port[VTSS_SPROUT__SP_B].ttl_new) {
                            use_stack_port_b = 1;
                        } else {
                            // Open loop and same TTL on both stack ports?!
                            T_E("unit_idx=%d, dist_a=%d, dist_b=%d, rit=%s",
                                unit_idx,
                                dist_a, dist_b,
                                vtss_sprout__rit_to_str(&rit));
                            VTSS_SPROUT_ASSERT_DBG_INFO(0, (" "));
                        }
                    } else {
                        // ClosedLoop => Equal length paths
                        use_stack_port_a = 1;
                        use_stack_port_b = 1;
                    }
                } else {
                    T_E("What?! dist_a/b=%d/%d", dist_a, dist_b);
                }

                // Set table entries for all UPSIDs of unit
                // (UPSIDs in same unit will always be reached by same route)
                for (ups_idx = 0; ups_idx < 2; ups_idx++) {
                    upsid = ri_p->upsid[ups_idx];

                    // For local UPSIDs keep the init values (0)
                    if (unit_me) {
                        continue;
                    }

                    if (upsid != VTSS_VSTAX_UPSID_UNDEF) {
                        upsid_route.table[upsid].stack_port_a = use_stack_port_a;
                        upsid_route.table[upsid].stack_port_b = use_stack_port_b;
                    }
                }
            }
            upsid_route.topology_type =
                (switch_state.topology_type_new == VtssTopoOpenLoop) ?
                VTSS_VSTAX_TOPOLOGY_CHAIN : VTSS_VSTAX_TOPOLOGY_RING;

            if (vtss_board_chipcount() == 2) {
                // Need to configure UPSID table for internal connectivity
                // Route to other unit's UPSIDes through internal stack port only
                for (ups_idx = 0; ups_idx < VTSS_SPROUT_UNIT_UPS_CNT; ups_idx++) {
                    upsid_route.table[switch_state.unit[!unit_idx].upsid[ups_idx]].stack_port_a = 0;
                    upsid_route.table[switch_state.unit[!unit_idx].upsid[ups_idx]].stack_port_b = 1;
                }
            }

            vtss_vstax_topology_set(NULL,
                                    unit_idx,
                                    &upsid_route);
        }
    }

    if (ttl_change) {

        uit.change_mask |= VTSS_SPROUT_STATE_CHANGE_MASK_TTL;
    }

    switch_state.topology_type = switch_state.topology_type_new;

    /*  Generate SPROUT updates */
    if (allow_sprout_stack_port0 && sps_pa[0]->proto_up) {
        VTSS_SPROUT_ASSERT_DBG_INFO(sps_pa[0]->adm_up && sps_pa[0]->link_up, (" "));
        if (internal_cfg.sprout_triggered_updates) {
            tx_sprout_update(0, 0);
        }
    }
    if (allow_sprout_stack_port1 && sps_pa[1]->proto_up) {
        VTSS_SPROUT_ASSERT_DBG_INFO(sps_pa[1]->adm_up && sps_pa[1]->link_up, (" "));
        if (internal_cfg.sprout_triggered_updates) {
            tx_sprout_update(1, 0);
        }
    }

    /* If we got this far, there must have been some kind of change (other than mst_time) */
    T_IG(VTSS_TRACE_GRP_TOP_CHG,
         "UIT/RIT has changed (change_mask=0x%x). New topology information:\n"
         "topology_type=%s\n"
         "topology_n=%d\n"
         "master_switch=%s\n"
         "ttl[0]=%d\n"
         "ttl[1]=%d\n"
         "%s",
         uit.change_mask,
         vtss_sprout_topology_type_to_str(switch_state.topology_type),
         switch_state.topology_n,
         vtss_sprout_switch_addr_to_str(&switch_state.mst_switch_addr),
         switch_state.unit[0].stack_port[0].ttl,
         switch_state.unit[0].stack_port[1].ttl,
         vtss_sprout__rit_to_str(&rit));

    return VTSS_OK;
} /* process_xit_changes */


static mesa_rc state_change_callback(void)
{
    mesa_rc rc = VTSS_OK;
    uchar   topo_change_mask = 0;

    /* Calculate value of topo_change_mask */
    if (uit.changed || uit.change_mask) {
        topo_change_mask =
            (rit.changed * VTSS_SPROUT_STATE_CHANGE_MASK_STACK_MBR) |
            uit.change_mask;

        if (topo_change_mask & VTSS_SPROUT_STATE_CHANGE_MASK_TTL) {
            /* TTL has changed. Consider this a topology change */
            switch_state.topology_change_time = sprout_init.callback.secs_since_boot();
        }
    }

    clr_xit_flags();

    /* Call back to other module(s) */
    if (topo_change_mask) {
        T_D("sprout_init.callback.state_change(%x)", topo_change_mask);

        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
        rc = sprout_init.callback.state_change(topo_change_mask);
        VTSS_SPROUT_CRIT_ENTER_TBL_RD();
    }

    return rc;
} /* state_change_callback */


/* ---------------------------------------------------------------------------
 * Internal functions
 * ======================================================================== */

/* ===========================================================================
 * API functions
 * ------------------------------------------------------------------------ */

#if defined(VTSS_SPROUT_MAIN)
int main(void)
{
    return 0;
}
#endif /* VTSS_SPROUT_MAIN */


/*
 * Initialization of Topo.
 * Must be called only once
 */
mesa_rc vtss_sprout_init(
    const vtss_sprout_init_t *const init)
{
    /* Initialize trace ressources */
    VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, TRACE_GRP_CNT);
    VTSS_TRACE_REGISTER(&trace_reg);

    T_D("enter");

    VTSS_SPROUT_ASSERT(sprout_init_done == 0, ("?"));
    VTSS_SPROUT_ASSERT(switch_init_done == 0, ("?"));

    VTSS_SPROUT_ASSERT(init != NULL, (" "));

    VTSS_SPROUT_ASSERT(init->callback.log_msg               != NULL, ("?"));
    VTSS_SPROUT_ASSERT(init->callback.cfg_save              != NULL, ("?"));
    VTSS_SPROUT_ASSERT(init->callback.state_change          != NULL, ("?"));
    VTSS_SPROUT_ASSERT(init->callback.tx_vstax2_pkt         != NULL, ("?"));

    sprout_init      = *init;
    sprout_init_done = 1;

    vtss_sprout__switch_state_init(&switch_state);

#if VTSS_SPROUT_MAX_LOCAL_CHIP_CNT > 1
    if (vtss_board_chipcount() == 2) {
        // Setup TTL for internal ports
        switch_state.unit[0].stack_port[1].ttl     = 1;
        switch_state.unit[0].stack_port[1].ttl_new = 1;
        switch_state.unit[1].stack_port[1].ttl     = 1;
        switch_state.unit[1].stack_port[1].ttl_new = 1;
    }
#endif

    // Gotta initialize the only field that the init function couldn't initialize by itself.
    switch_state.topology_change_time = sprout_init.callback.secs_since_boot();

    vtss_sprout__rit_init(&rit);
    vtss_sprout__uit_init(&uit);

    internal_cfg.sprout_periodic_updates  = VTSS_SPROUT_SPROUT_PERIODIC_UPDATES;
    internal_cfg.sprout_triggered_updates = VTSS_SPROUT_SPROUT_TRIGGERED_UPDATES;
    internal_cfg.sprout_aging             = VTSS_SPROUT_SPROUT_AGING;

    /*
     * Initialize semaphores with no tokens
     * Token will be posted at end of vtss_sprout_switch_init
     */
    VTSS_SPROUT_CRIT_INIT_TBL_RD();
    VTSS_SPROUT_CRIT_INIT_STATE_DATA();
    VTSS_SPROUT_CRIT_INIT_DBG();

    return VTSS_OK;
} /* vtss_sprout_init */


/*
 * Setup switch.
 */
mesa_rc vtss_sprout_switch_init(
    const vtss_sprout_switch_init_t *const setup)
{
    uint i = 0;
    uint j = 0;
    mesa_rc                       rc = VTSS_OK;
    vtss_sprout__unit_idx_t         unit_idx;
    vtss_sprout__upsid_idx_t        ups_idx;
    vtss_sprout__stack_port_idx_t   sp_idx;
    vtss_sprout__stack_port_state_t *sps_p;
    mesa_port_no_t                   port;
    mesa_vstax_upsid_t               upsids_pref[2]; // Preferred base UPSID per unit.

    /* Default MAC address, when none assigned to reference board */
    vtss_sprout_switch_addr_t vtss_none = {{0x00, 0x01, 0xc1, 0x00, 0x00, 0x00}};
    vtss_sprout_switch_addr_t null_addr = {{0, 0, 0, 0, 0, 0}};

    BOOL upsids_changed = 0;
    BOOL upsid_recalc_needed = 0;

    T_D("enter");

    VTSS_SPROUT_CRIT_ASSERT_TBL_RD(1);
    VTSS_SPROUT_ASSERT(setup != NULL, (" "));

    VTSS_SPROUT_ASSERT_DBG_INFO(sprout_init_done == 1, (" "));
    VTSS_SPROUT_ASSERT_DBG_INFO(switch_init_done == 0, (" "));

    switch_state.switch_addr    = setup->switch_addr;

    if (vtss_sprout__switch_addr_cmp(&switch_state.switch_addr, &vtss_none) == 0 ||
        vtss_sprout__switch_addr_cmp(&switch_state.switch_addr, &null_addr) == 0) {
        T_E("Illegal switch address: %s. Appears not to be unique. Unique switch address required.",
            vtss_sprout_switch_addr_to_str(&switch_state.switch_addr));
        T_E("setup->switch_addr=%s",
            vtss_sprout_switch_addr_to_str(&setup->switch_addr));

        // Continue as if nothing has happened. SPROUT will not come up when a switch
        // has the same MAC address as the neighbor. However, in e.g. a chain of three with
        // number 1 and 3 having the same MAC address, number 2 will continuously reboot due
        // to assertions in SPROUT.
    }

    switch_state.cpu_qu         = setup->cpu_qu;
    switch_state.mst_capable    = setup->mst_capable;
#if VTSS_SPROUT_UNMGD
    VTSS_SPROUT_ASSERT_DBG_INFO(setup->mst_capable == 0, (" "));
#endif
    memcpy(switch_state.switch_appl_info, setup->switch_appl_info, VTSS_SPROUT_SWITCH_APPL_INFO_LEN);

#if defined(VTSS_SPROUT_FW_VER_CHK)
    memcpy(switch_state.my_fw_ver, setup->my_fw_ver, VTSS_SPROUT_FW_VER_LEN);
#endif

    for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
        for (sp_idx = 0; sp_idx < 2; sp_idx++) {
            sps_p = &switch_state.unit[unit_idx].stack_port[sp_idx];

            /* Fill up buckets to start with */
            sps_p->update_bucket = sps_p->update_limit;
            sps_p->alert_bucket  = sps_p->alert_limit;
        }
    }

    // Initialize upsids_pref
    // This is identical to the values provided by caller, except if these
    // are illegal.
    for (i = 0; i < ARRSZ(upsids_pref); i++) {
        upsids_pref[i] = VTSS_VSTAX_UPSID_UNDEF;
    }
    upsids_pref[0] = setup->chip[0].upsid_pref;
    upsids_pref[1] = setup->chip[1].upsid_pref;

    T_DG(TRACE_GRP_UPSID, "upsids_pref[0] = %d, upsids_pref[1] = %d", upsids_pref[0], upsids_pref[1]);

    // Validate UPSID preference
    //
    // Originally Jaguar1 designs was allowed to have arbitrary UPSIDs.
    // This was later changed to require
    // a) Single unit designs to use even UPSID.
    // b) Dual unit designs to use consecutive UPSIDs, starting with
    //    an even UPSID.
    // This change was enforced for easier interoperability with Jaguar2
    // which will only support consecutive UPSIDs (for single unit
    // 48p designs).
    if (upsids_pref[0] != VTSS_VSTAX_UPSID_UNDEF && is_odd(upsids_pref[0])) {
        // First UPSID is not even
        T_DG(TRACE_GRP_UPSID, "First UPSID (%d) is not even", upsids_pref[0]);
        for (i = 0; i < ARRSZ(upsids_pref); i++) {
            upsids_pref[i] = VTSS_VSTAX_UPSID_UNDEF;
        }
    }
    for (i = 1; i < ARRSZ(upsids_pref); i++) {
        if (upsids_pref[i] != VTSS_VSTAX_UPSID_UNDEF &&
            upsids_pref[i] != upsids_pref[i - 1] + 1) {
            T_DG(TRACE_GRP_UPSID, "UPSIDs are not consecutive (upsids_prev[%d - 0] = %d, upsids_prev[%d - 1] = %d)", i, upsids_pref[i], i, upsids_pref[i - 1]);
            // UPSIDs are not consecutive
            for (i = 0; i < ARRSZ(upsids_pref); i++) {
                upsids_pref[i] = VTSS_VSTAX_UPSID_UNDEF;
            }
            break;
        }
    }

    // Check for duplicate usage of preferred, local UPSIDs (should never occur)
    for (i = 0; i < ARRSZ(upsids_pref); i++) {
        if (upsids_pref[i] == VTSS_VSTAX_UPSID_UNDEF) {
            continue;
        }

        for (j = 0; j < ARRSZ(upsids_pref); j++) {
            if (i == j) {
                continue;
            }

            if (upsids_pref[i] == upsids_pref[j]) {
                T_W("Switch has same preferred UPSID %d for different local UPS?!", upsids_pref[i]);
                T_DG(TRACE_GRP_UPSID, "Same preferred UPSIDs (i = %d, j = %d, upsid = %d", i, j, upsids_pref[i]);
                upsids_pref[j] = VTSS_VSTAX_UPSID_UNDEF;
            }
        }
    }

    for (i = 0; i < ARRSZ(upsids_pref); i++) {
        // Check that preferred values are legal
        VTSS_SPROUT_ASSERT_DBG_INFO(VTSS_VSTAX_UPSID_LEGAL(upsids_pref[i]) ||
                                    upsids_pref[i] == VTSS_VSTAX_UPSID_UNDEF,
                                    (" "));
    }

    for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
        const vtss_sprout_chip_setup_t *chip;
        chip = &setup->chip[unit_idx];

        // Initialize ups_port_mask
        for (port = 0; port < VTSS_PORT_ARRAY_SIZE; port++) {
            if (port_custom_table[port].map.chip_port != CHIP_PORT_UNUSED &&
                port_custom_table[port].map.chip_no == unit_idx &&
                port != setup->chip[0].stack_port[0].port_no &&
                port != setup->chip[0].stack_port[1].port_no) {
                if (port_custom_table[port].map.chip_port < 32) {
                    switch_state.unit[unit_idx].ups_port_mask[0] |= ((u64)1 << port);
#if VTSS_SPROUT_UNIT_UPS_CNT > 1
                } else {
                    switch_state.unit[unit_idx].ups_port_mask[1] |= ((u64)1 << port);
#endif
                }
            }
        }

        if (upsids_pref[unit_idx] == VTSS_VSTAX_UPSID_UNDEF) {
            /*
             * Switch has never been connected to a stack and thus has no
             * preferred UPSID. However, we must have an UPSID, also in
             * single unit "stack", so get a random UPSID.
             */
            T_DG(TRACE_GRP_UPSID, "Recalc_needed[%d]", unit_idx);
            upsid_recalc_needed = 1;
        } else {
            for (ups_idx = 0; ups_idx < VTSS_SPROUT_UNIT_UPS_CNT; ups_idx++) {
                // Use consecutive UPSIDs within unit
                switch_state.unit[unit_idx].upsid[ups_idx] = upsids_pref[unit_idx] + ups_idx;
            }
        }

        for (j = 0; j < 2; j++) {
            /* Change of stack port numbers not supported */
            VTSS_SPROUT_ASSERT_DBG_INFO(switch_init_done == 0 ||
                                        (switch_state.unit[unit_idx].stack_port[j].port_no ==
                                         chip->stack_port[j].port_no), (" "));

            switch_state.unit[unit_idx].stack_port[j].port_no = chip->stack_port[j].port_no;
        }
    }

    if (upsid_recalc_needed) {
        /* Recalculate UPSIDs */
        BOOL upsid_inuse_mask[VTSS_SPROUT_MAX_UNITS_IN_STACK];
        memset(upsid_inuse_mask, 0, sizeof(upsid_inuse_mask));

        rc = upsids_set_random_unused(upsid_inuse_mask);

        /* UPSID depletion is not possible at init stage */
        VTSS_SPROUT_ASSERT_DBG_INFO(rc == VTSS_OK, (" "));

        upsids_changed = 1;
    }

    /* Set sprout_smac to switch_addr (just to ease debugging)
       But clear multicast bit. */
    for (i = 0; i < 6; i++) {
        sprout_smac.addr[i] = setup->switch_addr.addr[i];
    }
    sprout_smac.addr[0] = sprout_smac.addr[0] & 0xfe;

    {
        uchar *pkt_p;
        uint  byte_idx = 0;


        // Initialize SPROUT Alert Protodown packet
        // ttl must be changed later using pkt_change_ttl()
        pkt_init_vs2_hdr(&sprout_alert_protodown_vs2_hdr, 0, 0);
        pkt_p = sprout_alert_protodown_pkt;
        pkt_init_sprout_hdr(pkt_p, &byte_idx, sprout_pdu_type_alert);
        pkt_add_bytes(pkt_p, &byte_idx, switch_state.switch_addr.addr, 6);
        pkt_add_byte(pkt_p, &byte_idx, SPROUT_ALERT_TYPE_PROTODOWN);
        pkt_add_byte(pkt_p, &byte_idx, 0x0);

        VTSS_SPROUT_ASSERT(byte_idx == SPROUT_ALERT_PROTODOWN_PDU_LEN,
                           ("byte_idx=%d, len=%d", byte_idx, SPROUT_ALERT_PROTODOWN_PDU_LEN));
    }

    if (!switch_init_done) {
        /* Initial call to vtss_sprout_switch_init */
        /* Setup stack ports */
        for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
            for (sp_idx = 0; sp_idx < 2; sp_idx++) {
                stack_port_setup(unit_idx, sp_idx);
            }
        }

        switch_init_done = 1;
    }

    if (upsids_changed) {
        T_DG(TRACE_GRP_UPSID, "Calling cfg_save()");
        cfg_save();
    }

    // Setup local UPSID(s) in API (i.e. the preferred UPSID(s) provided to SPROUT in
    // in this init call).
    stack_setup();

    if (vtss_board_chipcount() == 2) {
        // Need to configure UPSID table for internal connectivity
        vtss_vstax_route_table_t upsid_route;
        vtss_sprout__unit_idx_t  unit_idx;
        for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
            memset(&upsid_route, 0, sizeof(upsid_route));
            upsid_route.topology_type = VTSS_VSTAX_TOPOLOGY_CHAIN;

            // Route to other unit's UPSIDes through internal stack port
            for (ups_idx = 0; ups_idx < VTSS_SPROUT_UNIT_UPS_CNT; ups_idx++) {
                upsid_route.table[switch_state.unit[!unit_idx].upsid[ups_idx]].stack_port_b = 1;
            }

            vtss_vstax_topology_set(NULL,
                                    unit_idx,
                                    &upsid_route);
        }
    }

    update_local_uis();

    /* Process any changes to UITs */
    /* Allow SPROUT updates on both stack ports */
    rc = process_xit_changes(1, 1);
    if (rc < 0) {
        clr_xit_flags();
        return rc;
    }

    rc = state_change_callback();

    /* Release semaphores */
    VTSS_SPROUT_CRIT_EXIT_TBL_RD();
    VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
    VTSS_SPROUT_CRIT_EXIT_DBG();

    return rc;
} /* vtss_sprout_switch_init */


/*
 * Set adm state of stack port
 */
mesa_rc vtss_sprout_stack_port_adm_state_set(
    const mesa_port_no_t port_no,
    const BOOL           adm_up)
{
    vtss_sprout__stack_port_idx_t sp_idx = 0;
    BOOL                        found = 0;
    mesa_rc                     rc = VTSS_OK;

    T_D("enter, port_no=%u, adm_up=%d", port_no, adm_up);

    VTSS_SPROUT_CRIT_ENTER_STATE_DATA();
    VTSS_SPROUT_CRIT_ENTER_TBL_RD();

    /* Check all port numbers to find corresponding sp_idx */
    while (!found && (sp_idx < 2)) {
        if (switch_state.unit[0].stack_port[sp_idx].port_no == port_no) {
            found = 1;
        }

        if (!found) {
            sp_idx++;
        }
    }

    if (!found) {
        T_E("Unknown port_no: %u", port_no);
        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
        VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
        return VTSS_INVALID_PARAMETER;
    }

    if (switch_state.unit[0].stack_port[sp_idx].adm_up == adm_up) {
        /* No change. Weird. Ignore it */
        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
        VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
        return VTSS_OK;
    }

    rc = stack_port_adm_state_change(sp_idx, adm_up);
    if (rc < 0) {
        clr_xit_flags();
        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
        VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
        return rc;
    }

    rc = state_change_callback();

    VTSS_SPROUT_CRIT_EXIT_TBL_RD();
    VTSS_SPROUT_CRIT_EXIT_STATE_DATA();

    return rc;
} /* vtss_sprout_stack_port_adm_state_set */


/*
 * Set link state of stack port.
 */
mesa_rc vtss_sprout_stack_port_link_state_set(
    const mesa_port_no_t port_no,
    const BOOL           link_up)
{
    vtss_sprout__stack_port_idx_t sp_idx = 0;
    BOOL                        found = 0;
    mesa_rc                     rc = VTSS_OK;

    T_I("enter, port_no=%u, link_up=%d", port_no, link_up);

    VTSS_SPROUT_CRIT_ENTER_STATE_DATA();
    VTSS_SPROUT_CRIT_ENTER_TBL_RD();

    // Determine sp_idx for port_no
    //
    // Note that for dual-chip configuration the two external stack port numbers are
    // kept in stack port states of the primary unit.
    for (sp_idx = 0; sp_idx < 2; sp_idx++) {
        if (switch_state.unit[0].stack_port[sp_idx].port_no ==
            port_no) {
            /* Found port number */
            found = 1;
            break;
        }
    }

    T_D("enter, port_no=%u, link_up=%d, sp_idx=%d", port_no, link_up, sp_idx);

    if (!found) {
        T_E("Unknown port_no: %u\n%s", port_no, dbg_info());
        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
        VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
        return VTSS_INVALID_PARAMETER;
    }

    if (switch_state.unit[0].stack_port[sp_idx].link_up == link_up) {
        /* No change. Weird. Ignore it */
        T_D("No link state change, weird. port_no=%u link_up=%d\n%s", port_no, link_up, dbg_info());
        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
        VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
        return VTSS_OK;
    }

    rc = stack_port_link_state_change(sp_idx, link_up);
    if (rc < 0) {
        clr_xit_flags();
        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
        VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
        return rc;
    }

    rc = state_change_callback();

    VTSS_SPROUT_CRIT_EXIT_TBL_RD();
    VTSS_SPROUT_CRIT_EXIT_STATE_DATA();

    return rc;
} /* vtss_sprout_stack_port_link_state_set */


mesa_rc vtss_sprout_have_mirror_port_set(
    const vtss_sprout_chip_idx_t chip_idx,
    const BOOL                   have_mirror_port)
{
    vtss_sprout__unit_idx_t unit_idx;
    mesa_rc                 rc = VTSS_OK;

#if VTSS_SPROUT_UNMGD
    return VTSS_UNSPECIFIED_ERROR;
#endif

    T_D("enter, have_mirror_port=%d", have_mirror_port);

    VTSS_SPROUT_ASSERT_DBG_INFO(chip_idx <= vtss_board_chipcount(), (" "));

    VTSS_SPROUT_CRIT_ENTER_STATE_DATA();
    VTSS_SPROUT_CRIT_ENTER_TBL_RD();

    unit_idx = chip_idx - 1;

    switch_state.unit[unit_idx].have_mirror_port = have_mirror_port;

    update_local_uis();

    /* Process any changes to UITs */
    /* Allow SPROUT updates on both stack ports */
    rc = process_xit_changes(1, 1);
    if (rc < 0) {
        T_E("rc=%d\n%s", rc, dbg_info());
        clr_xit_flags();
        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
        VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
        return rc;
    }

    rc = state_change_callback();

    VTSS_SPROUT_CRIT_EXIT_TBL_RD();
    VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
    return rc;
} /* vtss_sprout_have_mirror_port_set */


mesa_rc vtss_sprout_ipv4_addr_set(
    /* IPv4 address */
    const mesa_ipv4_t ipv4_addr)
{
    mesa_rc               rc = VTSS_OK;

#if VTSS_SPROUT_UNMGD
    return VTSS_UNSPECIFIED_ERROR;
#endif

    T_D("enter, ipv4_addr=%u.%u.%u.%u",
        (ipv4_addr >> 24) & 0xff,
        (ipv4_addr >> 16) & 0xff,
        (ipv4_addr >>  8) & 0xff,
        (ipv4_addr >>  0) & 0xff);


    VTSS_SPROUT_CRIT_ENTER_STATE_DATA();
    VTSS_SPROUT_CRIT_ENTER_TBL_RD();

    switch_state.ip_addr = ipv4_addr;

    update_local_uis();

    /* Process any changes to UITs */
    /* Allow SPROUT updates on both stack ports */
    rc = process_xit_changes(1, 1);
    if (rc < 0) {
        T_E("rc=%d\n%s", rc, dbg_info());
        clr_xit_flags();
        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
        VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
        return rc;
    }


    rc = state_change_callback();

    VTSS_SPROUT_CRIT_EXIT_TBL_RD();
    VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
    return rc;
} /* vtss_sprout_ipv4_addr_set */


mesa_rc vtss_sprout_switch_appl_info_set(
    const vtss_sprout_switch_appl_info_t switch_appl_info_val,
    const vtss_sprout_switch_appl_info_t switch_appl_info_mask)
{
    mesa_rc               rc = VTSS_OK;
    uint byte_idx;

    VTSS_SPROUT_CRIT_ENTER_STATE_DATA();
    VTSS_SPROUT_CRIT_ENTER_TBL_RD();

    for (byte_idx = 0; byte_idx < VTSS_SPROUT_SWITCH_APPL_INFO_LEN; byte_idx++) {
        if (switch_appl_info_mask[byte_idx] != 0) {
            switch_state.switch_appl_info[byte_idx] =
                ((switch_state.switch_appl_info[byte_idx] & ~switch_appl_info_mask[byte_idx]) |
                 switch_appl_info_val[byte_idx]);
        }
    }

    update_local_uis();

    /* Process any changes to UITs */
    /* Allow SPROUT updates on both stack ports */
    rc = process_xit_changes(1, 1);
    if (rc < 0) {
        T_E("rc=%d\n%s", rc, dbg_info());
        clr_xit_flags();
        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
        VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
        return rc;
    }


    rc = state_change_callback();

    VTSS_SPROUT_CRIT_EXIT_TBL_RD();
    VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
    return rc;
} /* vtss_sprout_switch_appl_info_set */


/* Must be called from application every ~100msec */
mesa_rc vtss_sprout_service_100msec(void)
{
    uint sp_idx;
    uint update_bucket_add = 0;
    uint alert_bucket_add  = 0;
    vtss_sprout__stack_port_state_t *sps_p;
    mesa_rc                       rc = VTSS_OK;

    T_R("enter");

    VTSS_SPROUT_CRIT_ENTER_STATE_DATA();
    VTSS_SPROUT_CRIT_ENTER_TBL_RD();

    for (sp_idx = 0; sp_idx < 2; sp_idx++) {
        sps_p = &switch_state.unit[0].stack_port[sp_idx];

        sps_p->deci_secs_since_last_tx_update++;

        if (internal_cfg.sprout_aging) {
            /* Aging of neighbours if no updates seen */
            sps_p->deci_secs_since_last_rx_update++;
            if (sps_p->deci_secs_since_last_rx_update >= sps_p->update_age_time * 10) {
                if (sps_p->proto_up) {
                    /*
                    * Have not seen any updates for too long
                    * Take protocol down.
                    */

                    T_I("No updates seen on sp_idx=%d => Taking proto down", sp_idx);
                    rc = force_proto_down(sp_idx);
                    if (rc < 0) {
                        clr_xit_flags();
                        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
                        VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
                        return rc;
                    }
                    VTSS_SPROUT_ASSERT_DBG_INFO(sps_p->proto_up == 0, (" "));
                }
            }
        }

        if (internal_cfg.sprout_periodic_updates) {
            /* Periodic update */
            if ((switch_state.mst == 0 &&
                 sps_p->deci_secs_since_last_tx_update >= sps_p->update_interval_slv * 10)
                ||
                (switch_state.mst == 1 &&
                 sps_p->deci_secs_since_last_tx_update >= sps_p->update_interval_mst * 10)) {
                if (sps_p->link_up) {
                    /* Transmit a periodic update */
                    T_N("Periodic SPROUT update on sp_idx=%d (secs since last update: %d, mst=%d)",
                        sp_idx, sps_p->deci_secs_since_last_tx_update, switch_state.mst);
                    tx_sprout_update(sp_idx, 1);
                }
            }
        }

        /* Update update buckets */
        if (sps_p->update_bucket < sps_p->update_limit) {
            sps_p->deci_secs_since_last_update_bucket_filling++;

            update_bucket_add =
                (sps_p->deci_secs_since_last_update_bucket_filling * sps_p->update_limit) / 10;
            if (update_bucket_add > 0) {
                sps_p->update_bucket += update_bucket_add;
                sps_p->deci_secs_since_last_update_bucket_filling =
                    (sps_p->deci_secs_since_last_update_bucket_filling * sps_p->update_limit) % 10;
            }
            if (sps_p->update_bucket >= sps_p->update_limit) {
                sps_p->update_bucket = sps_p->update_limit;
                sps_p->deci_secs_since_last_update_bucket_filling = 0;
            }
        } else {
            sps_p->deci_secs_since_last_update_bucket_filling = 0;
        }

        /* Update alert buckets */
        if (sps_p->alert_bucket < sps_p->alert_limit) {
            sps_p->deci_secs_since_last_alert_bucket_filling++;

            alert_bucket_add =
                (sps_p->deci_secs_since_last_alert_bucket_filling * sps_p->alert_limit) / 10;
            if (alert_bucket_add > 0) {
                sps_p->alert_bucket += alert_bucket_add;
                sps_p->deci_secs_since_last_alert_bucket_filling =
                    (sps_p->deci_secs_since_last_alert_bucket_filling * sps_p->alert_limit) % 10;
            }
            if (sps_p->alert_bucket >= sps_p->alert_limit) {
                sps_p->alert_bucket = sps_p->alert_limit;
                sps_p->deci_secs_since_last_alert_bucket_filling = 0;
            }
        } else {
            sps_p->deci_secs_since_last_alert_bucket_filling = 0;
        }
    }

    rc = state_change_callback();

    VTSS_SPROUT_CRIT_EXIT_TBL_RD();
    VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
    return rc;
} /* vtss_sprout_service_100msec */


/*
 * SPROUT packet received
 * Application calls this function when packet of the following type has
 * been extracted from CPU extraction queue:
 * - DMAC:           Any
 * - SMAC:           Any
 * - Exbit Protocol: EPID=0x2 (SSP)
 * - SSP:            SSPID=0x1 (SPROUT)
 */
mesa_rc vtss_sprout_rx_pkt(
    const mesa_port_no_t  port_no,
    const uchar *const    frame,
    const uint            length)
{
    mesa_rc                       rc;
    vtss_sprout__stack_port_idx_t sp_idx;
    BOOL                          found = 0;
    uchar                         pdu_type;
    uint                          byte_idx;

    T_N("vtss_sprout_rx_pkt: port_no=%u, length=%d", port_no, length);

    VTSS_SPROUT_CRIT_ENTER_STATE_DATA();
    VTSS_SPROUT_CRIT_ENTER_TBL_RD();

    /* Asssert expected packet format */
    {
        ushort word16;
        word16 = ((frame[12] << 8) | (frame[13] << 0));
        VTSS_SPROUT_ASSERT(word16 == 0x8880,
                           ("Unexpected EtherType=0x%08x, expected 0x8880", word16));
        word16 = ((frame[14] << 8) | (frame[15] << 0));
        VTSS_SPROUT_ASSERT(word16 == 0x0002,
                           ("Unexpected EPID=0x%08x, expected 0x0002", word16));
        word16 = ((frame[16] << 8) | (frame[17] << 0));
        VTSS_SPROUT_ASSERT(word16 == 0x0001,
                           ("Unexpected SSPID=0x%08x, expected 0x0001", word16));
        /* Byte 18+19 are reserved by SSP. No assertion on those. */
        /* SPROUT update starts at byte 20                        */
    }

    // Determine sp_idx for port
    //
    // Note that for dual-chip configuration the two external stack port numbers are
    // kept in stack port states of the primary unit.
    for (sp_idx = 0; sp_idx < 2; sp_idx++) {
        if (switch_state.unit[0].stack_port[sp_idx].port_no ==
            port_no) {
            /* Found port number */
            found = 1;
            break;
        }
    }

    if (found) {
        /* Determine SPROUT PDU type */
        byte_idx = 20;
        pdu_type = pkt_get_byte(frame, &byte_idx);

        switch (pdu_type) {
        case SPROUT_PDU_TYPE_UPDATE:
            rc = rx_sprout_update(
                     port_no,
                     sp_idx,
                     frame,
                     length);
            break;

        case SPROUT_PDU_TYPE_ALERT:
            rc = rx_sprout_alert(
                     port_no,
                     sp_idx,
                     frame,
                     length);
            break;

        default:
            T_W("Unknown PDU type: 0x%02x", pdu_type);
            T_W_HEX(frame, length);
            VTSS_SPROUT_CRIT_EXIT_TBL_RD();
            VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
            return VTSS_OK;
        }

        if (rc < 0) {
            clr_xit_flags();
            VTSS_SPROUT_CRIT_EXIT_TBL_RD();
            VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
            return rc;
        }

        rc = state_change_callback();

        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
        VTSS_SPROUT_CRIT_EXIT_STATE_DATA();

        return rc;
    } else {
        /* Unexpected port */
        T_N("Frame received on unexpected port: %u", port_no);
        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
        VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
        return VTSS_UNSPECIFIED_ERROR;
    }
} /* vtss_sprout_rx_pkt */


void vtss_sprout_sit_get(
    vtss_sprout_sit_t *const sit)
{
    vtss_sprout__ui_t *ui_p;
    vtss_sprout__ri_t *ri_p;
    uint i;
    uint sp_path; /* stack port path */
    vtss_sprout_switch_addr_t *prv_switch_addr_p = NULL;
    uint chip_idx;
    uint switch_cnt = 0;
    vtss_sprout__upsid_idx_t ups_idx;

    T_D("enter");

    if (!ignore_semaphores) {
        VTSS_SPROUT_CRIT_ENTER_TBL_RD();
    }

    VTSS_SPROUT_ASSERT(sit != NULL, (" "));

    memset(sit, 0, sizeof(vtss_sprout_sit_t));

    // Reset UPSIDs
    for (i = 0; i < ARRSZ(sit->si); i++) {
        for (chip_idx = 0; chip_idx < ARRSZ(sit->si[i].chip); chip_idx++) {
            for (ups_idx = 0; ups_idx < ARRSZ(sit->si[i].chip[chip_idx].upsid); ups_idx++) {
                sit->si[i].chip[chip_idx].upsid[ups_idx] = VTSS_VSTAX_UPSID_UNDEF;
            }
        }
    }

    sit->mst_switch_addr = switch_state.mst_switch_addr;
    sit->mst_change_time = switch_state.mst_change_time;
    sit->topology_type   = switch_state.topology_type;

    i = 0;
    ui_p = NULL;
    while ((ui_p = vtss_sprout__ui_get_nxt(&uit, ui_p))) {
        BOOL local_unit =
            vtss_sprout__switch_addr_cmp(&switch_state.switch_addr,
                                         &ui_p->switch_addr) == 0;
        BOOL same_switch_as_prv_ui =
            (prv_switch_addr_p != NULL &&
             (vtss_sprout__switch_addr_cmp(prv_switch_addr_p,
                                           &ui_p->switch_addr) == 0));

        if (same_switch_as_prv_ui) {
            // Different units of same switch are always placed consecutively in UIT
            i--;

            VTSS_SPROUT_ASSERT(sit->si[i].chip_cnt == 1,
                               ("sit->si[%d].chip_cnt=%d",
                                i, sit->si[i].chip_cnt));

            VTSS_SPROUT_ASSERT(ui_p->unit_idx == 1,
                               ("ui_p->unit_idx=%d", ui_p->unit_idx));
        }

        // Switch information
        // ------------------
        sit->si[i].vld = 1;

        sit->si[i].switch_addr = ui_p->switch_addr;

        // Take switch information from primary unit in UIT
        if (ui_p->primary_unit) {
            // Master election information
            sit->si[i].mst_capable     = ui_p->mst_capable;
            sit->si[i].mst_elect_prio  = ui_p->mst_elect_prio + 1;
            sit->si[i].mst_time_ignore = ui_p->mst_time_ignore;
            if (local_unit) {
                if (switch_state.mst) {
                    sit->si[i].mst_time =
                        sprout_init.callback.secs_since_boot() -
                        switch_state.mst_start_time;
                } else {
                    sit->si[i].mst_time = 0;
                }
            } else {
                sit->si[i].mst_time     = ui_p->mst_time;
            }

            // Application specific information, exchanged by SPROUT
            memcpy(sit->si[i].switch_appl_info, ui_p->switch_appl_info,
                   VTSS_SPROUT_SWITCH_APPL_INFO_LEN);

            // IPv4 address. Host order. 0.0.0.0 = Unknown
            sit->si[i].ip_addr         = ui_p->ip_addr;
        }

        sit->si[i].chip_cnt++;

        // Chip Information
        // ----------------
        // Always put primary chip in [0], secondary in [1]
        if (ui_p->primary_unit) {
            chip_idx = 0;
        } else {
            chip_idx = 1;
        }

        for (ups_idx = 0; ups_idx < 2; ups_idx++) {
            sit->si[i].chip[chip_idx].upsid[ups_idx]         = ui_p->upsid[ups_idx];
            sit->si[i].chip[chip_idx].ups_port_mask[ups_idx] = ui_p->ups_port_mask[ups_idx];
        }

        sp_path = get_shortest_path(0, &ui_p->switch_addr, ui_p->unit_idx);
        if (sp_path == 2) {
            // 2 => Local unit
            sit->si[i].chip[chip_idx].shortest_path = 0;
            sit->si[i].chip[chip_idx].dist[0]       = 0;
            sit->si[i].chip[chip_idx].dist[1]       = 0;
        } else {
            sit->si[i].chip[chip_idx].shortest_path =
                switch_state.unit[0].stack_port[sp_path].port_no;

            if ((ri_p = vtss_sprout__ri_find(&rit,
                                             &ui_p->switch_addr,
                                             chip_idx))) {

                sit->si[i].chip[chip_idx].dist[0] = ri_p->stack_port_dist[0];
                sit->si[i].chip[chip_idx].dist[1] = ri_p->stack_port_dist[1];
            } else {
                sit->si[i].chip[chip_idx].dist[0] = -1;
                sit->si[i].chip[chip_idx].dist[1] = -1;
            }
        }

        sit->si[i].chip[chip_idx].have_mirror     = ui_p->have_mirror;

        prv_switch_addr_p = &ui_p->switch_addr;

        i++;
    }
    switch_cnt = i;

    for (; i < VTSS_SPROUT_SIT_SIZE; i++) {
        sit->si[i].vld = 0;
    }

    if (!ignore_semaphores) {
        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
    }
    T_D("exit, switch_cnt=%d", switch_cnt);
} /* vtss_sprout_sit_get */


mesa_rc vtss_sprout_stat_get(
    vtss_appl_topo_switch_stat_t *const stat)
{
    /* Array of different switch addresses found in UIT */
    vtss_sprout_switch_addr_t     addr[VTSS_SPROUT_UIT_SIZE];
    uint                          addr_cnt = 0;
    vtss_sprout__ui_t             *ui_p = NULL;
    vtss_sprout__stack_port_idx_t sp_idx;

    T_D("enter");

    if (!ignore_semaphores) {
        VTSS_SPROUT_CRIT_ENTER_TBL_RD();
    }

    VTSS_SPROUT_ASSERT(stat != NULL, (" "));

    /* Determine number of switches in stack */
    while ((ui_p = vtss_sprout__ui_get_nxt(&uit, ui_p))) {
        BOOL found = 0;
        uint i     = 0;
        while (i < addr_cnt) {
            if (vtss_sprout__switch_addr_cmp(&ui_p->switch_addr, &addr[i]) == 0) {
                found = 1;
                break;
            }
            i++;
        }
        if (!found) {
            /* New switch address found */
            addr[addr_cnt++] = ui_p->switch_addr;
        }
    }

    stat->switch_cnt           = addr_cnt;
    stat->topology_type        = switch_state.topology_type;
    stat->topology_change_time = switch_state.topology_change_time;

    for (sp_idx = 0; sp_idx < 2; sp_idx++) {
        /* Note: For dual unit switches, statistics is stored in switch_state.unit[0] */
        stat->stack_port[sp_idx].proto_up                          = switch_state.unit[0].stack_port[sp_idx].proto_up;
        stat->stack_port[sp_idx].update_rx_cnt              = switch_state.unit[0].stack_port[sp_idx].sprout_update_rx_cnt;
        stat->stack_port[sp_idx].update_periodic_tx_cnt     = switch_state.unit[0].stack_port[sp_idx].sprout_update_periodic_tx_cnt;
        stat->stack_port[sp_idx].update_triggered_tx_cnt    = switch_state.unit[0].stack_port[sp_idx].sprout_update_triggered_tx_cnt;
        stat->stack_port[sp_idx].update_tx_policer_drop_cnt = switch_state.unit[0].stack_port[sp_idx].sprout_update_tx_policer_drop_cnt;
        stat->stack_port[sp_idx].update_rx_err_cnt          = switch_state.unit[0].stack_port[sp_idx].sprout_update_rx_err_cnt;
        stat->stack_port[sp_idx].update_tx_err_cnt          = switch_state.unit[0].stack_port[sp_idx].sprout_update_tx_err_cnt;

        stat->stack_port[sp_idx].alert_rx_cnt              = switch_state.unit[0].stack_port[sp_idx].sprout_alert_rx_cnt;
        stat->stack_port[sp_idx].alert_tx_cnt              = switch_state.unit[0].stack_port[sp_idx].sprout_alert_tx_cnt;
        stat->stack_port[sp_idx].alert_tx_policer_drop_cnt = switch_state.unit[0].stack_port[sp_idx].sprout_alert_tx_policer_drop_cnt;
        stat->stack_port[sp_idx].alert_rx_err_cnt          = switch_state.unit[0].stack_port[sp_idx].sprout_alert_rx_err_cnt;
        stat->stack_port[sp_idx].alert_tx_err_cnt          = switch_state.unit[0].stack_port[sp_idx].sprout_alert_tx_err_cnt;
    }

    if (!ignore_semaphores) {
        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
    }

    return VTSS_OK;
} /* vtss_sprout_stat_get */


mesa_rc vtss_sprout_parm_set(
    const BOOL             init_phase,
    const vtss_sprout_parm_t parm,
    const int              val)
{
    mesa_rc rc = VTSS_OK;
    vtss_sprout__stack_port_state_t *sps_p;
    vtss_sprout__stack_port_idx_t   sp_idx;
    vtss_sprout__unit_idx_t         unit_idx;

    T_D("enter");

    if (!init_phase) {
        VTSS_SPROUT_CRIT_ENTER_STATE_DATA();
        VTSS_SPROUT_CRIT_ENTER_TBL_RD();
    }

    switch (parm) {
    case VTSS_SPROUT_PARM_MST_ELECT_PRIO:
        if (!(VTSS_SPROUT_MST_ELECT_PRIO_START <= val &&
              val < VTSS_SPROUT_MST_ELECT_PRIO_END)) {
            T_E("Invalid val: %d", val);
            rc = VTSS_INVALID_PARAMETER;
        } else {
            switch_state.mst_elect_prio = val - 1;
        }
        break;

    case VTSS_SPROUT_PARM_MST_TIME_IGNORE:
        if (val != 0 && val != 1) {
            T_E("Invalid val: %d", val);
            rc = VTSS_INVALID_PARAMETER;
        } else {
            switch_state.mst_time_ignore = val;
        }
        break;

    case VTSS_SPROUT_PARM_SPROUT_UPDATE_INTERVAL_SLV:
        if (val <= 0) {
            T_E("Invalid val: %d", val);
            rc = VTSS_INVALID_PARAMETER;
        } else {
            for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
                for (sp_idx = 0; sp_idx < 2; sp_idx++) {
                    sps_p = &switch_state.unit[unit_idx].stack_port[sp_idx];
                    sps_p->update_interval_slv = val;
                }
            }
        }
        break;

    case VTSS_SPROUT_PARM_SPROUT_UPDATE_INTERVAL_MST:
        if (val <= 0) {
            T_E("Invalid val: %d", val);
            rc = VTSS_INVALID_PARAMETER;
        } else {
            for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
                for (sp_idx = 0; sp_idx < 2; sp_idx++) {
                    sps_p = &switch_state.unit[unit_idx].stack_port[sp_idx];
                    sps_p->update_interval_mst = val;
                }
            }
        }
        break;

    case VTSS_SPROUT_PARM_SPROUT_UPDATE_AGE_TIME:
        if (val <= 0) {
            T_E("Invalid val: %d", val);
            rc = VTSS_INVALID_PARAMETER;
        } else {
            for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
                for (sp_idx = 0; sp_idx < 2; sp_idx++) {
                    sps_p = &switch_state.unit[unit_idx].stack_port[sp_idx];
                    sps_p->update_age_time = val;
                }
            }
        }
        break;
    case VTSS_SPROUT_PARM_SPROUT_UPDATE_LIMIT:
        if (val <= 0) {
            T_E("Invalid val: %d", val);
            rc = VTSS_INVALID_PARAMETER;
        } else {
            for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
                for (sp_idx = 0; sp_idx < 2; sp_idx++) {
                    sps_p = &switch_state.unit[unit_idx].stack_port[sp_idx];
                    sps_p->update_limit = val;
                }
            }
        }
        break;
    default:
        T_E("Unknown parm: %d", parm);
        rc = VTSS_INVALID_PARAMETER;
    }

    if (!init_phase) {
        update_local_uis();

        /* Process any changes to UIT */
        rc = process_xit_changes(1, 1);
        if (rc < 0) {
            clr_xit_flags();
            VTSS_SPROUT_CRIT_EXIT_TBL_RD();
            VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
            return rc;
        }

        rc = state_change_callback();

        VTSS_SPROUT_CRIT_EXIT_TBL_RD();
        VTSS_SPROUT_CRIT_EXIT_STATE_DATA();
    }
    return rc;
} /* vtss_sprout_parm_set */


/* ---------------------------------------------------------------------------
 * API functions
 * ======================================================================== */



/* ===========================================================================
 * Debug functions
 * ------------------------------------------------------------------------ */


#if defined(VTSS_SPROUT_FW_VER_CHK)
void vtss_sprout_fw_ver_mode_set(
    vtss_sprout_fw_ver_mode_t mode)
{
    switch_state.fw_ver_mode = mode;
} // vtss_sprout_fw_ver_mode_set
#endif

static char *dbg_info(void)
{
    static char s[5000];
    const  int  size = 5000; // Size of s[]

    s[0] = 0;
    vtss_sprout__str_append(s, size, "Debug info:\n");
    vtss_sprout__str_append(s, size, "Switch state:\n%s", vtss_sprout__switch_state_to_str(&switch_state));

// TOE/2011-05-23:
// Console output cannot handle too long strings.
#if 0
    vtss_sprout__str_append(s, size, vtss_sprout__rit_to_str(&rit));
    vtss_sprout__str_append(s, size, "UIT:\n%s", vtss_sprout__uit_to_str(&uit));
#endif

    VTSS_SPROUT_ASSERT(strlen(s) < size,
                       ("strlen(s)=%d", strlen(s)));

    return s;
} /* dbg_info */


static void dbg_cmd_syntax_error(
    vtss_sprout_dbg_printf_t dbg_printf,
    const char *fmt, ...)
{
    va_list ap;
    char s[200] = "Command syntax error: ";
    int len;

    len = strlen(s);

    va_start(ap, fmt);

    vsprintf(s + len, fmt, ap);
    dbg_printf("%s\n", s);

    va_end(ap);
} /* dbg_cmd_syntax_error */


#define TOPO_DBG_CMD_RIT_PRINT               3
#define TOPO_DBG_CMD_UIT_PRINT               4
#define TOPO_DBG_CMD_UI_UPDATE               5
#define TOPO_DBG_CMD_UIT_CLR_FLAGS           6
#define TOPO_DBG_CMD_UIT_DEL_UNFOUND         7
#define TOPO_DBG_CMD_SWITCH_STATE_PRINT      8
#define TOPO_DBG_CMD_RIT_CLR                 9
#define TOPO_DBG_CMD_SET_NBR                 10
#define TOPO_DBG_CMD_UIT_CLR                 11
#define TOPO_DBG_CMD_ADM_UP                  12
#define TOPO_DBG_CMD_LINK_UP                 13
#define TOPO_DBG_CMD_TX_SPROUT_UPDATE        14
#define TOPO_DBG_CMD_SUMMARY                 15
#define TOPO_DBG_CMD_STACK_PORT_MIRROR       16
#define TOPO_DBG_CMD_LINK_DOWN_1WAY          17
#define TOPO_DBG_CMD_SPROUT_UPDATE_PERIODIC  18
#define TOPO_DBG_CMD_SPROUT_UPDATE_TRIGGERED 19
#define TOPO_DBG_CMD_SWITCH_STAT             20
#define TOPO_DBG_CMD_TRACE_LEVEL             23
#define TOPO_DBG_CMD_SET_MST_TIME            24
#define TOPO_DBG_CMD_SET_MST_TIME_IGNORE     25
#define TOPO_DBG_CMD_SET_MST_ELECT_PRIO      26
#define TOPO_DBG_CMD_TMP_TEST                80
#define TOPO_DBG_CMD_SET_UPSID               93
#define TOPO_DBG_CMD_SET_SWITCH_ADDR         94
#define TOPO_DBG_CMD_MERGE_UITS              96
#define TOPO_DBG_CMD_TEST_FUNC99             99
#define TOPO_DBG_CMD_END                     -1

typedef struct {
    int  cmd_num;
    char cmd_txt[80];
    char arg_syntax[90];
    uint arg_cnt;
} vtss_sprout_dbg_cmd_t;


void vtss_sprout_dbg(
    vtss_sprout_dbg_printf_t dbg_printf,
    const uint         parms_cnt,
    const ulong *const parms)
{
    vtss_sprout__ui_t             ui;
    vtss_sprout__ri_t             *ri_p = NULL;
    uint                          i;
    int                           sel;
    int                           cmd_num;
    vtss_sprout__stack_port_idx_t sp_idx;
    mesa_port_no_t                port_no;
    vtss_sprout__unit_idx_t       unit_idx;
    vtss_sprout__upsid_idx_t      ups_idx;
    static vtss_sprout_dbg_cmd_t  cmds[] = {
        {
            TOPO_DBG_CMD_RIT_PRINT,
            "Print RIT",
            "",
            0
        },
        {
            TOPO_DBG_CMD_UIT_PRINT,
            "Print UIT",
            "<uit select> - 0: stack port a   1: stack port b   2: UIT",
            1
        },
        {
            TOPO_DBG_CMD_UI_UPDATE,
            "Update/Insert UI on stack port UIT",
            "<stack_port_idx> <sa byte> <unit idx> <upsid0> <upsid1> <glag_mbr_cnt> <have_mirror>",
            6
        },
        {
            TOPO_DBG_CMD_UIT_CLR_FLAGS,
            "Clear UIT flags",
            "<stack_port>",
            1,
        },
        {
            TOPO_DBG_CMD_UIT_DEL_UNFOUND,
            "Delete UIs not found in SPROUT update",
            "<stack_port>",
            1,
        },
        {
            TOPO_DBG_CMD_SWITCH_STATE_PRINT,
            "Print switch state information",
            "",
            0,
        },
        {
            TOPO_DBG_CMD_RIT_CLR,
            "Clear RIT",
            "",
            0,
        },
        {
            TOPO_DBG_CMD_SET_NBR,
            "Set neighbour address",
            "<stack port> <sa byte>",
            2,
        },
        {
            TOPO_DBG_CMD_UIT_CLR,
            "Clear UIT",
            "<uit select>",
            1,
        },
        {
            TOPO_DBG_CMD_ADM_UP,
            "Adm up/down",
            "<stack port no (SA/SB)> <up>",
            2,
        },
        {
            TOPO_DBG_CMD_LINK_UP,
            "Link up/down",
            "<stack port no (SA/SB)> <up>",
            2,
        },
        {
            TOPO_DBG_CMD_TX_SPROUT_UPDATE,
            "Send SPROUT Update",
            "<stack port idx (0/1)>",
            1,
        },
        {
            TOPO_DBG_CMD_SUMMARY,
            "Print state summary",
            "",
            0,
        },
        {
            TOPO_DBG_CMD_STACK_PORT_MIRROR,
            "Mirror stack port to front port 1",
            "<stack port no (SA/SB)>",
            1,
        },
        {
            TOPO_DBG_CMD_LINK_DOWN_1WAY,
            "Set LANE_ENA=0, causing peer to see link as down",
            "<stack port no (SA/SB)>",
            1,
        },
        {
            TOPO_DBG_CMD_SPROUT_UPDATE_PERIODIC,
            "Enable/disable periodic SPROUT updates and SPROUT aging",
            "<0=disable, 1=enable>",
            1,
        },
        {
            TOPO_DBG_CMD_SPROUT_UPDATE_TRIGGERED,
            "Enable/disable triggered SPROUT updates",
            "<0=disable, 1=enable>",
            1,
        },
        {
            TOPO_DBG_CMD_SWITCH_STAT,
            "Print SPROUT switch statistics",
            "",
            0,
        },
        {
            TOPO_DBG_CMD_TRACE_LEVEL,
            "Set trace level minimum. <grp>=-1 => All groups.",
            "<grp> <lvl: 10=None, 9=Error, 8=Warning, 6=Info, 4=Debug, 2=Noise 1=Racket>",
            2,
        },
        {
            TOPO_DBG_CMD_SET_MST_TIME,
            "Set master time (and mst=1)",
            "<master_time>",
            1,
        },
        {
            TOPO_DBG_CMD_SET_MST_TIME_IGNORE,
            "Set master time ignore",
            "<1/0>",
            1,
        },
        {
            TOPO_DBG_CMD_SET_MST_ELECT_PRIO,
            "Set master elect prio",
            "<prio (0-3)>",
            1,
        },
        // -------------------------------------------
        {
            TOPO_DBG_CMD_TMP_TEST,
            "Tempoary test",
            "",
            2,
        },
        {
            TOPO_DBG_CMD_SET_UPSID,
            "Set UPSIDs of unit(s) and process",
            "<upsid (0-30)> <upsid (0-30)>",
            1,
        },
        {
            TOPO_DBG_CMD_SET_SWITCH_ADDR,
            "Set address of switch and tx updates",
            "<32 bit switch addr>",
            1,
        },
        {
            TOPO_DBG_CMD_MERGE_UITS,
            "Merge stack port UITs into common UIT",
            "",
            0,
        },
        {
            TOPO_DBG_CMD_TEST_FUNC99,
            "Test function",
            "",
            2,
        },
        {
            TOPO_DBG_CMD_END,
            "",
            "",
            0,
        }
    };

    if (parms_cnt == 0) {
        dbg_printf("Usage: D <cmd idx>\n");
        dbg_printf("\n");
        dbg_printf("Commands:\n");
        i = 0;
        while (cmds[i].cmd_num != TOPO_DBG_CMD_END) {
            dbg_printf("  %2d: %s\n", cmds[i].cmd_num, cmds[i].cmd_txt);
            if (cmds[i].arg_syntax[0]) {
                dbg_printf("      Arguments: %s.\n", cmds[i].arg_syntax);
            }
            i++;
        }
        char str[20];
        sprintf(str, "\nSA=%u, SB=%u\n",
                switch_state.unit[0].stack_port[0].port_no,
                switch_state.unit[0].stack_port[1].port_no);
        dbg_printf(str);
        return;
    }
    cmd_num = parms[0];

    /* Verify that command is known and argument count is correct */
    i = 0;
    while (cmds[i].cmd_num != TOPO_DBG_CMD_END) {
        if (cmds[i].cmd_num == cmd_num) {
            break;
        }

        i++;
    }
    if (cmds[i].cmd_num == TOPO_DBG_CMD_END) {
        dbg_cmd_syntax_error(dbg_printf, "Unknown command number: %d", cmd_num);
        return;
    }
    if (cmds[i].arg_cnt + 1 != parms_cnt) {
        dbg_cmd_syntax_error(dbg_printf, "Incorrect number of arguments (%d).\n"
                             "Arguments: %s",
                             parms_cnt - 1,
                             cmds[i].arg_syntax);
        return;
    }

    switch (parms[0]) {
    case TOPO_DBG_CMD_RIT_PRINT:
        vtss_sprout__rit_print(dbg_printf, &rit);
        break;

    case TOPO_DBG_CMD_UIT_PRINT:
        sel = parms[1];

        switch (sel) {
        case 0:
            dbg_printf("UIT for stack port 0\n");
            vtss_sprout__uit_print(dbg_printf, &switch_state.unit[0].stack_port[0].uit);
            break;
        case 1:
            dbg_printf("UIT for stack port 1\n");
            vtss_sprout__uit_print(dbg_printf, &switch_state.unit[0].stack_port[1].uit);
            break;
        case 2:
            dbg_printf("UIT\n");
            vtss_sprout__uit_print(dbg_printf, &uit);
            break;
        default:
            dbg_cmd_syntax_error(dbg_printf, "Unexpected UIT selector: %d", sel);
        }
        break;

    case TOPO_DBG_CMD_UI_UPDATE:
        sp_idx = parms[1];
        if (sp_idx > 1) {
            dbg_cmd_syntax_error(dbg_printf, "stack_port_idx==%d >1", sp_idx);
            return;
        }

        vtss_sprout__ui_init(&ui);
        ui.vld = 1;
        ui.switch_addr.addr[0]              = parms[2];
        ui.unit_idx                         = parms[3];
        ui.upsid[0]                         = parms[4];
        ui.upsid[1]                         = parms[5];
        ui.have_mirror                      = parms[7];
        vtss_sprout__uit_update(&switch_state.unit[0].stack_port[sp_idx].uit, &ui, 0);

        vtss_sprout__uit_print(dbg_printf, &switch_state.unit[0].stack_port[sp_idx].uit);
        break;

    case TOPO_DBG_CMD_UIT_CLR_FLAGS:
        sel = parms[1];

        switch (sel) {
        case 0:
            vtss_sprout__uit_clr_flags(&switch_state.unit[0].stack_port[0].uit);
            break;
        case 1:
            vtss_sprout__uit_clr_flags(&switch_state.unit[0].stack_port[1].uit);
            break;
        case 2:
            vtss_sprout__uit_clr_flags(&uit);
            break;
        default:
            dbg_cmd_syntax_error(dbg_printf, "Unexpected UIT selector: %d", sel);
        }
        break;

    case TOPO_DBG_CMD_UIT_DEL_UNFOUND:
        sel = parms[1];

        if (sel > 1) {
            dbg_cmd_syntax_error(dbg_printf, "Unexpected UIT selector: %d", sel);
            return;
        }

        vtss_sprout__uit_del_unfound(&switch_state.unit[0].stack_port[sel].uit);
        break;

    case TOPO_DBG_CMD_SWITCH_STATE_PRINT:
        vtss_sprout__switch_state_print(dbg_printf, &switch_state);
        break;

    case TOPO_DBG_CMD_RIT_CLR:
        while ((ri_p = vtss_sprout__ri_get_nxt(&rit, ri_p))) {
            vtss_sprout__ri_init(ri_p);
        }
        break;

    case TOPO_DBG_CMD_SET_NBR:
        sp_idx = parms[1];
        if (sp_idx > 1) {
            dbg_cmd_syntax_error(dbg_printf, "stack_port_idx==%d >1", sp_idx);
            return;
        }
        vtss_sprout_switch_addr_init(&switch_state.unit[0].stack_port[sp_idx].nbr_switch_addr);
        switch_state.unit[0].stack_port[sp_idx].nbr_switch_addr.addr[0] = parms[2];
        break;

    case TOPO_DBG_CMD_UIT_CLR:
        sel = parms[1];
        if (sel > 2) {
            dbg_cmd_syntax_error(dbg_printf, "Unexpected UIT selector: %d", sel);
            return;
        }
        if (sel < 2) {
            vtss_sprout__uit_init(&switch_state.unit[0].stack_port[sel].uit);
        } else {
            vtss_sprout__uit_init(&uit);
        }
        break;

    case TOPO_DBG_CMD_MERGE_UITS:
        merge_uits();
        break;

    case TOPO_DBG_CMD_ADM_UP:
        vtss_sprout_stack_port_adm_state_set(parms[1], parms[2]);
        break;

    case TOPO_DBG_CMD_LINK_UP:
        /* Force VAUI stack port up/down by setting LANE_CFG.LANE_ENA = 1/0 */
    {
        mesa_port_conf_t conf;
        port_no = parms[1];

        if (mesa_port_conf_get(NULL, port_no, &conf) == VTSS_RC_OK) {
            conf.power_down = ((parms[2] == 0) ? 1 : 0);
            mesa_port_conf_set(NULL, port_no, &conf);
        }
    }
    break;

    case TOPO_DBG_CMD_LINK_DOWN_1WAY:
        /*
         * Set LANE_CFG.LANE_ENA = 0.
         *
         * Address:
         * 3 bit block number
         * 4 bit subblock number
         * 8 bit register address
         */
        mesa_reg_write(NULL,
                       0,
                       (3 << 12) | (4 << 8) | ((parms[1] == 25) ? 0x04 : 0x0c),
                       0);
        mesa_reg_write(NULL,
                       0,
                       (3 << 12) | (4 << 8) | ((parms[1] == 25) ? 0x08 : 0x10),
                       0);
        break;
    case TOPO_DBG_CMD_SPROUT_UPDATE_PERIODIC:
        internal_cfg.sprout_periodic_updates = parms[1];
        internal_cfg.sprout_aging            = parms[1];
        break;

    case TOPO_DBG_CMD_SPROUT_UPDATE_TRIGGERED:
        internal_cfg.sprout_triggered_updates = parms[1];
        break;

    case TOPO_DBG_CMD_SWITCH_STAT: {
        int i;
        vtss_appl_topo_switch_stat_t stat;
        CRIT_IGNORE(vtss_sprout_stat_get(&stat));
        dbg_printf("switch_cnt=%d\n", stat.switch_cnt);
        dbg_printf("topology_type=%d\n", stat.topology_type);
        for (i = 0; i < 2; i++) {
            dbg_printf("Stack port %d: proto_up=%d\n",                i, stat.stack_port[i].proto_up);
            dbg_printf("Stack port %d: sprout_update_rx_cnt=%d\n",           i, stat.stack_port[i].update_rx_cnt);
            dbg_printf("Stack port %d: sprout_update_tx_periodic_cnt=%d\n",  i, stat.stack_port[i].update_periodic_tx_cnt);
            dbg_printf("Stack port %d: sprout_update_tx_triggered_cnt=%d\n", i, stat.stack_port[i].update_triggered_tx_cnt);
            dbg_printf("Stack port %d: sprout_update_rx_err_cnt=%d\n",       i, stat.stack_port[i].update_rx_err_cnt);
            dbg_printf("Stack port %d: sprout_update_tx_err_cnt=%d\n",       i, stat.stack_port[i].update_tx_err_cnt);
        }
    }
    break;

    case TOPO_DBG_CMD_TRACE_LEVEL:
#if (VTSS_TRACE_ENABLED)
        if ((int)parms[1] >= TRACE_GRP_CNT) {
            dbg_cmd_syntax_error(dbg_printf, "grp==%d >=%d", parms[1], TRACE_GRP_CNT);
            return;
        }

        if (parms[1] == -1) {
            /* All groups */
            int i;
            for (i = 0; i < TRACE_GRP_CNT; i++) {
                trace_grps[i].lvl = parms[2];
            }
        } else {
            trace_grps[parms[1]].lvl = parms[2];
        }
#endif
        break;

    case TOPO_DBG_CMD_SET_MST_TIME:
        switch_state.mst = 1;
        switch_state.mst_start_time = sprout_init.callback.secs_since_boot() - parms[1];
        switch_state.mst_switch_addr = switch_state.switch_addr;
        break;

    case TOPO_DBG_CMD_SET_MST_TIME_IGNORE:
        vtss_sprout_parm_set(0, VTSS_SPROUT_PARM_MST_TIME_IGNORE, parms[1]);
        break;

    case TOPO_DBG_CMD_SET_MST_ELECT_PRIO:
        vtss_sprout_parm_set(0, VTSS_SPROUT_PARM_MST_ELECT_PRIO, parms[1]);
        break;

    case TOPO_DBG_CMD_TX_SPROUT_UPDATE:
        tx_sprout_update(parms[1], 0);
        break;

    case TOPO_DBG_CMD_SUMMARY:
        dbg_printf("topology_type=%s\n", vtss_sprout_topology_type_to_str(switch_state.topology_type));
        dbg_printf("topology_n=%d\n", switch_state.topology_n);
        dbg_printf("ttl[0]=%d\n", switch_state.unit[0].stack_port[0].ttl);
        dbg_printf("ttl[1]=%d\n", switch_state.unit[0].stack_port[1].ttl);
        vtss_sprout__rit_print(dbg_printf, &rit);
        break;

    case TOPO_DBG_CMD_STACK_PORT_MIRROR:
        /* Always use port 1 as mirror port */
        mesa_mirror_monitor_port_set(NULL, 1);

        /* Configure port 1 to keep stack header */
        /*
         * Address:
         * 3 bit block number
         * 4 bit subblock number
         * 8 bit register address
         */
        mesa_reg_write(NULL, 0, (1 << 12) | (0 << 8) | 0x25, (1 << 7));

        {
            BOOL member[VTSS_PORT_ARRAY_SIZE];

            memset(member, 0, sizeof(member));
            member[parms[1]] = 1;

            mesa_mirror_ingress_ports_set(NULL, member);
            mesa_mirror_egress_ports_set(NULL, member);
        }
        break;

    case TOPO_DBG_CMD_TMP_TEST:
        dbg_printf(dbg_info());
//        T_I(vtss_sprout__switch_state_to_str(&switch_state));
#if 0
        {
            static int i = 0;
            static char s[1000];

            for (i = 0; i < parms[1]; i++) {
                s[i] = 'a';
            }
            s[i] = 0;
            T_I(s);
        }
#endif
        break;

    case TOPO_DBG_CMD_SET_UPSID:
        for (unit_idx = 0; unit_idx < vtss_board_chipcount(); unit_idx++) {
            for (ups_idx = 0; ups_idx < VTSS_SPROUT_UNIT_UPS_CNT; ups_idx++) {
                switch_state.unit[unit_idx].upsid[ups_idx] = (parms[unit_idx + 1] & 0x1E) + ups_idx; // Base always even
            }
        }
        T_DG(TRACE_GRP_UPSID, "Calling cfg_save()");
        cfg_save();
        stack_setup();
        update_local_uis();
        clr_xit_flags();
        // dbg_printf("UIT\n");
        // vtss_sprout__uit_print(dbg_printf, &uit);
        // process_xit_changes(1, 1);
        // Just wait for periodic update to propagate change
        break;

    case TOPO_DBG_CMD_SET_SWITCH_ADDR:
        memset(&switch_state.switch_addr, 0, sizeof(switch_state.switch_addr));
        switch_state.switch_addr.addr[2] = (parms[1] & 0xff000000) > 24;
        switch_state.switch_addr.addr[3] = (parms[1] & 0x00ff0000) > 16;
        switch_state.switch_addr.addr[4] = (parms[1] & 0x0000ff00) >  8;
        switch_state.switch_addr.addr[5] = (parms[1] & 0x000000ff);
        switch_state.unit[0].stack_port[VTSS_SPROUT__SP_A].proto_up = 0;
        switch_state.unit[0].stack_port[VTSS_SPROUT__SP_B].proto_up = 0;
        vtss_sprout__uit_init(&switch_state.unit[0].stack_port[VTSS_SPROUT__SP_A].uit);
        vtss_sprout__uit_init(&switch_state.unit[0].stack_port[VTSS_SPROUT__SP_B].uit);
        vtss_sprout__uit_init(&uit);

        vtss_sprout__uit_print(dbg_printf, &uit);
        T_N("--1--");
        update_local_uis();
        T_N("--2--");
        vtss_sprout__uit_print(dbg_printf, &uit);
        T_N("--3--");
        uit.changed = 0;

        vtss_sprout__rit_init(&rit);

#if 1
        if (switch_state.unit[0].stack_port[VTSS_SPROUT__SP_A].link_up) {
            tx_sprout_update(VTSS_SPROUT__SP_A, 0);
        }
        if (switch_state.unit[0].stack_port[VTSS_SPROUT__SP_B].link_up) {
            tx_sprout_update(VTSS_SPROUT__SP_B, 0);
        }
#endif
        break;

    case TOPO_DBG_CMD_TEST_FUNC99: {
//        T_I("%s", vtss_sprout__switch_state_to_str(&switch_state));
//        T_I("%s", vtss_sprout__rit_to_str(&rit));
        T_I("%s", vtss_sprout__uit_to_str(&uit));
    }
    break;

    default:
        dbg_cmd_syntax_error(dbg_printf, "Unknown command index: %d", (int)parms[0]);
        break;
    }
} /* vtss_sprout_dbg */
/* ======================================================================== */


/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
