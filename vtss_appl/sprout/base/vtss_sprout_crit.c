/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.



This file is part of SPROUT - "Stack Protocol using ROUting Technology".
*/

#include "vtss_sprout.h"
#include <string.h>

/* ===========================================================================
 * Type definitions
 * ------------------------------------------------------------------------ */

#define VTSS_SPROUT_CRIT_THREAD_ID_UNKNOWN (0)

/* ======================================================================== */


/* ===========================================================================
 * Internal functions
 * ------------------------------------------------------------------------ */

#if VTSS_SPROUT_MULTI_THREAD

#if VTSS_TRACE_ENABLED
static ulong vtss_sprout_crit_get_thread_id(void)
{
    return (ulong)vtss_thread_id_get();
}

/* Check whether critical region is locked */
static BOOL vtss_sprout_crit_is_locked(vtss_sprout_crit_t *crit_p)
{
    int cnt = vtss_sem_peek(&crit_p->sem);
    return (cnt == 0);
}

/* Print critical region state using trace */
static void vtss_sprout_crit_state_trace(
    vtss_sprout_crit_t *crit_p,
    int               trace_grp,
    int               trace_lvl,
    const char        *file,
    const int         line)
{
    int lock_file_start = 0;
    int unlock_file_start = 0;
    int i;


    /* Skip any leading path from lock/unlock file */
    if (crit_p->lock_file) {
        for (i = 0; i < strlen(crit_p->lock_file); i++) {
            if (crit_p->lock_file[i] == '/') {
                lock_file_start = i + 1;
            }
        }
    }
    if (crit_p->unlock_file) {
        for (i = 0; i < strlen(crit_p->unlock_file); i++) {
            if (crit_p->unlock_file[i] == '/') {
                unlock_file_start = i + 1;
            }
        }
    }

    T_EXPLICIT(VTSS_TRACE_MODULE_ID, trace_grp, trace_lvl, file, line,
               "Current state for critical region \"%s\":\n"
               "Locked=%d\n"
               "Latest lock: %u/%s#%d\n"
               "Latest unlock: %u/%s#%d",

               crit_p->name,
               vtss_sprout_crit_is_locked(crit_p),

               crit_p->lock_thread_id,
               crit_p->lock_file ? &crit_p->lock_file[lock_file_start] : crit_p->lock_file,
               crit_p->lock_line,

               crit_p->unlock_thread_id,
               crit_p->unlock_file ? &crit_p->unlock_file[unlock_file_start] : crit_p->unlock_file,
               crit_p->unlock_line);
} /* vtss_sprout_crit_state_trace */
#endif /* VTSS_TRACE_ENABLED */

#endif /* VTSS_SPROUT_MULTI_THREAD */


/* ---------------------------------------------------------------------------
 * Internal functions
 * ======================================================================== */



/* ===========================================================================
 * External functions
 * ------------------------------------------------------------------------ */


/* Initialize vtss_sprout_crit_t */
void vtss_sprout_crit_init(
    vtss_sprout_crit_t *crit_p,
    const char *name
#if VTSS_TRACE_ENABLED
    ,
    int        trace_grp,
    int        trace_lvl,
    const char *file,
    const int  line
#endif
)
{
#if VTSS_SPROUT_MULTI_THREAD
    T_EXPLICIT(VTSS_TRACE_MODULE_ID, trace_grp, trace_lvl, file, line,
               "Creating vtss_sprout_crit_t \"%s\"", crit_p->name);

    memset(crit_p, 0, sizeof(vtss_sprout_crit_t));

    vtss_sem_init(&crit_p->sem, 0);

    strncpy(crit_p->name, name, VTSS_SPROUT_CRIT_NAME_LEN);
    crit_p->name[VTSS_SPROUT_CRIT_NAME_LEN - 1] = 0;
#if VTSS_TRACE_ENABLED
    crit_p->current_lock_thread_id = VTSS_SPROUT_CRIT_THREAD_ID_UNKNOWN;
#endif
    crit_p->init_done = 1;
#endif /* VTSS_SPROUT_MULTI_THREAD */
} /* vtss_sprout_crit_init */


/* Lock vtss_sprout_crit_t */
void vtss_sprout_crit_lock(
    vtss_sprout_crit_t *crit_p
#if VTSS_TRACE_ENABLED
    ,
    int        trace_grp,
    int        trace_lvl,
    const char *file,
    const int  line
#endif
)
{
#if VTSS_SPROUT_MULTI_THREAD
#if VTSS_TRACE_ENABLED
    T_EXPLICIT(VTSS_TRACE_MODULE_ID, trace_grp, trace_lvl, file, line,
               "Locking critical region \"%s\"", crit_p->name);
    vtss_sprout_crit_state_trace(crit_p, trace_grp, trace_lvl, file, line);
#endif

#if VTSS_SPROUT_CRIT_CHK && VTSS_TRACE_ENABLED
    if (!crit_p->init_done) {
        vtss_sprout_crit_state_trace(crit_p, trace_grp, VTSS_TRACE_LVL_ERROR, file, line);
        VTSS_SPROUT_ASSERT(0,  ("crit_p not initialized (init_done == 0)!"));
    }

    /* Assert that this thread has not already locked this critical region */
    if (crit_p->current_lock_thread_id != VTSS_SPROUT_CRIT_THREAD_ID_UNKNOWN &&
        crit_p->current_lock_thread_id == vtss_sprout_crit_get_thread_id()) {
        vtss_sprout_crit_state_trace(crit_p, trace_grp, VTSS_TRACE_LVL_ERROR, file, line);
        VTSS_SPROUT_ASSERT(0,  ("Critical region already locked by this thread id!"));
    }
#endif

    vtss_sem_wait(&crit_p->sem);

#if VTSS_SPROUT_CRIT_CHK && VTSS_TRACE_ENABLED
    /* Store information about lock */
    crit_p->current_lock_thread_id = vtss_sprout_crit_get_thread_id();
    crit_p->lock_thread_id         = vtss_sprout_crit_get_thread_id();
    crit_p->lock_file              = file;
    crit_p->lock_line              = line;
#endif

    T_EXPLICIT(VTSS_TRACE_MODULE_ID, trace_grp, trace_lvl, file, line,
               "Locked critical region \"%s\"", crit_p->name);
#if VTSS_TRACE_ENABLED
    vtss_sprout_crit_state_trace(crit_p, trace_grp, trace_lvl, file, line);
#endif
#endif /* VTSS_SPROUT_MULTI_THREAD */
} /* vtss_sprout_crit_lock */


/* Unlock vtss_sprout_crit_t */
void vtss_sprout_crit_unlock(
    vtss_sprout_crit_t *crit_p
#if VTSS_TRACE_ENABLED
    ,
    int        trace_grp,
    int        trace_lvl,
    const char *file,
    const int  line
#endif
)
{
#if VTSS_SPROUT_MULTI_THREAD
#if VTSS_TRACE_ENABLED
    T_EXPLICIT(VTSS_TRACE_MODULE_ID, trace_grp, trace_lvl, file, line,
               "Unlocking critical region \"%s\"", crit_p->name);
    vtss_sprout_crit_state_trace(crit_p, trace_grp, trace_lvl, file, line);
#endif


#if VTSS_SPROUT_CRIT_CHK && VTSS_TRACE_ENABLED
    /* Assert that critical region is currently locked */
    if (!vtss_sprout_crit_is_locked(crit_p)) {
        vtss_sprout_crit_state_trace(crit_p, trace_grp, VTSS_TRACE_LVL_ERROR, file, line);
        VTSS_SPROUT_ASSERT(0, ("Unlock called, but critical region is not locked!"));
    }

    /* Assert that any current lock is this thread */
    if (crit_p->lock_thread_id != VTSS_SPROUT_CRIT_THREAD_ID_UNKNOWN &&
        crit_p->lock_thread_id != vtss_sprout_crit_get_thread_id()) {
#if VTSS_TRACE_ENABLED
        vtss_sprout_crit_state_trace(crit_p, trace_grp, VTSS_TRACE_LVL_ERROR, file, line);
#endif
        VTSS_SPROUT_ASSERT(0, ("Unlock called, but critical region is locked by different thread id!"));
    }

    /* Clear current lock id */
    crit_p->current_lock_thread_id = VTSS_SPROUT_CRIT_THREAD_ID_UNKNOWN;

    /* Store information about unlock */
    crit_p->unlock_thread_id  = vtss_sprout_crit_get_thread_id();
    crit_p->unlock_file       = file;
    crit_p->unlock_line       = line;
#endif

    vtss_sem_post(&crit_p->sem);

#if VTSS_TRACE_ENABLED
    T_EXPLICIT(VTSS_TRACE_MODULE_ID, trace_grp, trace_lvl, file, line,
               "Unlocked critical region \"%s\"", crit_p->name);
#endif
#endif /* VTSS_SPROUT_MULTI_THREAD */
} /* vtss_sprout_crit_unlock */


/* Assert whether critical region is unlocked/locked */
void vtss_sprout_crit_assert(
    vtss_sprout_crit_t *crit_p,
    BOOL              locked
#if VTSS_TRACE_ENABLED
    ,
    int        trace_grp,
    int        trace_lvl,
    const char *file,
    const int  line
#endif
)
{
#if VTSS_SPROUT_MULTI_THREAD

#if VTSS_SPROUT_CRIT_CHK && VTSS_TRACE_ENABLED
    if (vtss_sprout_crit_is_locked(crit_p) != locked) {
        vtss_sprout_crit_state_trace(crit_p, trace_grp, VTSS_TRACE_LVL_ERROR, file, line);
        VTSS_SPROUT_ASSERT(0, (locked ? "Critical region not locked!" : "Critical region locked!"));
    }
#endif

#endif /* VTSS_SPROUT_MULTI_THREAD */
} /* vtss_sprout_crit_assert */

vtss_sprout_crit_t  crit_state_data;
vtss_sprout_crit_t  crit_tbl_rd;
vtss_sprout_crit_t  crit_dbg;

/* ---------------------------------------------------------------------------
 * External functions
 * ======================================================================== */


/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
