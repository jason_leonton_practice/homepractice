/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "topo_serializer.hxx"
#include "vtss/appl/topo.h"
//****************************************
// Enum  and types definitions
//****************************************

#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_TOPO

vtss_enum_descriptor_t topo_topology_type_txt[] {
    {VtssTopoBack2Back,  "back2back"},
    {VtssTopoClosedLoop, "ring"},
    {VtssTopoOpenLoop,   "chain"},
    {0, 0}
};

/****************************************************************************
 * Get/Set functions38
 ****************************************************************************/
mesa_rc vtss_topo_ser_usid_assignment_set(vtss_usid_t usid, const vtss_topo_ser_stack_mac_t *mac_addr) {
    T_I("usid:%d", usid);
    VTSS_RC(vtss_appl_topo_usid_assignment_set(usid, mac_addr->smac));
    return VTSS_RC_OK;
}

mesa_rc vtss_topo_ser_usid_assignment_get(vtss_usid_t usid, vtss_topo_ser_stack_mac_t *mac_addr) {
    T_I("usid:%d", usid);
    if (!is_usid_exist(usid)) {
        return VTSS_RC_OK;
    }
    VTSS_RC(vtss_appl_topo_usid_assignment_get(usid, &mac_addr->smac));
    return VTSS_RC_OK;
}

mesa_rc vtss_topo_ser_usid_swap_set(vtss_usid_t usid, const vtss_topo_ser_stack_swap_t *swap) {
    VTSS_RC(vtss_appl_topo_usid_swap(usid, swap->sid));
    return VTSS_RC_OK;
}

// Dummy function because the "Write Only" OID is implemented with a ReadWrite
mesa_rc vtss_topo_ser_usid_swap_dummy_get(vtss_usid_t usid, vtss_topo_ser_stack_swap_t *swap) {
    if (swap) {
        swap->sid = usid;
    }
    return VTSS_RC_OK;
}

// Dummy function because the "Write Only" OID is implemented with a ReadWrite
mesa_rc vtss_topo_ser_usid_del_dummy_get(vtss_topo_ser_del_t *del) {
    if (del) {
        del->usid = 1; // Always give back usid 1. This is a dummy, because SNMP doesn't have a write only type.
    }
    return VTSS_RC_OK;
}

// Wrapper for delete set because there is no parameters to the "real" delete function
mesa_rc vtss_topo_ser_usid_del_set(const vtss_topo_ser_del_t *del) {
    
    if (del->usid) {
        VTSS_RC(vtss_appl_topo_usid_assignment_del(del->usid));
    }
    return VTSS_RC_OK;
}


// Dummy function because the "Write Only" OID is implemented with a ReadWrite
mesa_rc vtss_topo_ser_usid_reelect_dummy_get(vtss_topo_ser_reelect_t *reelect) {
    if (reelect) {
        reelect->reelect = FALSE;
    }
    return VTSS_RC_OK;
}

// Wrapper for reelect set because there is no parameters to the "real" re-elect function
mesa_rc vtss_topo_ser_usid_reelect_set(const vtss_topo_ser_reelect_t *reelect) {
    
    if (reelect->reelect) {
        VTSS_RC(vtss_appl_topo_mst_reelection_start());
    }
    return VTSS_RC_OK;
}
