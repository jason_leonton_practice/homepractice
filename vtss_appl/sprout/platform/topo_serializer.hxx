/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#ifndef __VTSS_TOPO_SERIALIZER_HXX__
#define __VTSS_TOPO_SERIALIZER_HXX__

#include "vtss_appl_serialize.hxx"
#include "vtss/appl/topo.h"

//****************************************
// Enum and types definitions
//****************************************
extern vtss_enum_descriptor_t topo_topology_type_txt[];

// For assigning stacking switch's mac address 
typedef struct {
    mesa_mac_addr_t smac; /**< Source mac address.*/
} vtss_topo_ser_stack_mac_t;

// For stacking swapping of switch IDs 
typedef struct {
    vtss_usid_t sid; /**< Switch id to be swapped*/
} vtss_topo_ser_stack_swap_t;

// For master re-election 
typedef struct {
    BOOL reelect; // TRUE to do re-election
} vtss_topo_ser_reelect_t;

// For deleting switch IDs
typedef struct {
    vtss_usid_t usid; // User switch id to delete
} vtss_topo_ser_del_t;

//****************************************
// Serializers 
//*******************x*********************
// Serialize vtss_appl_topo_stack_config_t
template<typename T> void serialize(T &a, vtss_appl_topo_stack_config_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_topo_stack_config_t"));

    int ix = 1;  
    m.add_leaf(vtss::AsBool(s.stacking), 
               vtss::tag::Name("EnableStacking"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("If the stack consists of one active switch or stacking is disabled (false), "
                                      "it is possible to configure whether stacking must be enabled (true). " 
                                      "When changing this setting, the switch must be rebooted before this takes effect."));

    m.add_leaf(vtss::AsInterfaceIndex(s.ifindex_0), 
               vtss::tag::Name("StackIfA"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("A stack uses two interfaces for stacking, which are named interface A and B. "
                                      "Selecting which physical interface to be used for stacking interface A. "
                                      "Note: Not all interfaces can be used as stacking interface."));

    m.add_leaf(vtss::AsInterfaceIndex(s.ifindex_1), 
               vtss::tag::Name("StackIfB"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("A stack uses two interfaces for stacking, which are named interface A and B. "
                                      "Selecting which physical interface to be used for stacking interface B. "
                                      "Note: Not all interfaces can be used as stacking interface."));
}

// Serialize vtss_appl_topo_topology_type_t
VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_topo_topology_type_t,
                         "topoTopologyType",
                         topo_topology_type_txt,
                         "This enumeration reflects the current topology.");

// Serialize vtss_appl_topo_mst_elect_prio_t
template<typename T> void serialize(T &a, vtss_appl_topo_mst_elect_prio_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_topo_stack_prio_t"));

    int ix = 1;  
    m.add_leaf(vtss::AsInt(s.prio), 
               vtss::expose::snmp::RangeSpec<uint32_t>(TOPO_MST_ELECT_PRIO_START, TOPO_MST_ELECT_PRIOS),
               vtss::tag::Name("Priority"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Each switch in the stack has a priority which is used to determine which switch "
                                      "in the stack that shall be the master. For setting the switch priority. "
                                      "The lower priority number the higher priority")); 
}

// Serialize vtss_topo_ser_stack_mac_t
template<typename T> void serialize(T &a, vtss_topo_ser_stack_mac_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_topo_ser_stack_mac_t"));
    int ix = 1;  // After interface index

    m.add_leaf(vtss::AsOctetString(s.smac, sizeof(s.smac)), 
               vtss::tag::Name("MacAddr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Used to assign a switch id to a switch with a specific MAC address. "
                                      "For changing switch id for the switches in the stack, you normally use the swap function. "
                                      "In the case where a switch has been added to a stack, but could not be assigned a "
                                      "switch id at detection time (e.g. due to switches which is no longer in the stack still "
                                      "have a switch id assigned), it might be necessary to assign the switch id based upon "
                                      "the MAC address. The following must be true in order to be able to assign the switch id. "
                                      "1) The switch id must not already be assigned to another switch. "
                                      "2) The switch must have been detected (the MAC address must be shown the status).")); 
}

// Serialize vtss_topo_ser_stack_swap_t
template<typename T> void serialize(T &a, vtss_topo_ser_stack_swap_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_topo_ser_stack_swap_t"));
    int ix = 1;  // After index

    m.add_leaf(vtss::AsInt(s.sid),
               vtss::expose::snmp::RangeSpec<uint32_t>(1, 16),
               vtss::tag::Name("SwitchId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Switch Id for the switch to swap id with. Each switch in the stack has a switch ID. "
                                      "Swapping switch id between two switches in the stack.")); 
}

// Serialize vtss_topo_ser_reelect_t
template<typename T> void serialize(T &a, vtss_topo_ser_reelect_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_topo_ser_reelect_t"));
    int ix = 1;  // After index

    m.add_leaf(vtss::AsBool(s.reelect),
               vtss::tag::Name("ReElect"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Makes the stack re-elect a master.")); 
}

// Serialize vtss_topo_ser_del_t
template<typename T> void serialize(T &a, vtss_topo_ser_del_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_topo_ser_del_t"));
    int ix = 1;  // After index

    m.add_leaf(vtss::AsInt(s.usid),
               vtss::expose::snmp::RangeSpec<uint32_t>(1, 16),
               vtss::tag::Name("Del"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Deletes a switch from the stack.")); 
}

template<typename T> void serialize(T &a, vtss_appl_topo_valid_interfaces_pair_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_topo_valid_interfaces_pair_t"));
    int ix = 1;  // After index

    char descr[150];
    char name_buf[50];
    for (auto i=0; i < s.pair.size(); i++) {
        snprintf(name_buf, sizeof(name_buf), "ValidPair%dInterfaceA", i + 1);
        snprintf(descr, sizeof(descr), "Interface A for valid combination %d. If this pair combination doesn't contain any valid interfaces, it is set 0", i + 1);

        m.add_leaf(vtss::AsInterfaceIndex(s.pair[i].interfaceA), 
                   vtss::tag::Name(name_buf),
                   vtss::expose::snmp::Status::Current,
                   vtss::expose::snmp::OidElementValue(ix++),
                   vtss::tag::Description(descr));

        
        snprintf(name_buf, sizeof(name_buf), "ValidPair%dInterfaceB", i + 1);
        snprintf(descr, sizeof(descr), "Interface B for valid combination %d. If this pair combination doesn't contain any valid interfaces, it is set 0", i + 1);

        m.add_leaf(vtss::AsInterfaceIndex(s.pair[i].interfaceB), 
                   vtss::tag::Name(name_buf),
                   vtss::expose::snmp::Status::Current,
                   vtss::expose::snmp::OidElementValue(ix++),
                   vtss::tag::Description(descr));
    }

    m.add_leaf(vtss::AsInt(s.selected_pair), 
                   vtss::tag::Name("SelectedPair"),
                   vtss::expose::snmp::Status::Current,
                   vtss::expose::snmp::OidElementValue(ix++),
                   vtss::tag::Description("The pair combination currently selected to form a stacking interface"));

    m.add_leaf(vtss::AsInt(s.count), 
                   vtss::tag::Name("CombinationCount"),
                   vtss::expose::snmp::Status::Current,
                   vtss::expose::snmp::OidElementValue(ix++),
                   vtss::tag::Description("Actually number of combinations for this switch"));

}

// Serializevtss_appl_topo_switch_stat_t

// Help MACRO for generating the name for the counters 
#define UPDATE_NAME_BUF(x)    snprintf(name_buf, sizeof(name_buf), "%s%s", x,  i == 0 ? "A" : "B");
#define UPDATE_DESCRIPTION(x) snprintf(buf, sizeof(buf), "%s counter for interface %s.", x,  i == 0 ? "A" : "B");
#define UPDATE_BUF(x, y) UPDATE_NAME_BUF(y); UPDATE_DESCRIPTION(x)

template<typename T>
void serialize(T &a, vtss_appl_topo_mst_stat_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_topo_mst_stat_t"));
    int ix = 1;  // After index

    char time_s[VTSS_APPL_RFC3339_TIME_STR_LEN];
    strcpy(time_s, misc_time2str(s.mst_change_time));
    m.add_leaf(vtss::AsDisplayString(time_s, VTSS_APPL_RFC3339_TIME_STR_LEN),
               vtss::tag::Name("MasterChangeTime"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Time of last master change since boot in seconds.")); 

    m.add_leaf(vtss::AsOctetString(s.mst_switch_mac_addr, sizeof(s.mst_switch_mac_addr)), 
               vtss::tag::Name("MacAddr"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("MAC address for the switch which is currently master.")); 
}
template<typename T>
void serialize(T &a, vtss_appl_topo_switch_stat_t &s) {
    typename T::Map_t m = a.as_map(vtss::tag::Typename("vtss_appl_topo_switch_stat_t"));
    int ix = 1;  // After index

    m.add_leaf(vtss::AsInt(s.switch_cnt),
               vtss::tag::Name("SwitchCnt"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Current number of switches in the stack.")); 

    m.add_leaf(s.topology_type,
               vtss::tag::Name("TopologyType"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Current topology type:\
                                       \nBack2Back - Two units connected with two stacking cables.\
                                       \nRing      - Ring topology with more than two units.\
                                       \nChain     - Chain topology.")); 

    char time_s[VTSS_APPL_RFC3339_TIME_STR_LEN];
    strcpy(time_s, misc_time2str(s.topology_change_time));
    m.add_leaf(vtss::AsDisplayString(time_s, VTSS_APPL_RFC3339_TIME_STR_LEN),
               vtss::tag::Name("ChangeTime"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(ix++),
               vtss::tag::Description("Time of last topology change since boot in seconds.")); 

    char buf[200];
    char name_buf[50];
    for (int i=0; i < 2; i++) { // Loop through interface A & B
        // Determine which of the statistics that contains interface A nad B
        u8 stat_if_index = (i == 0) ? s.if_a_index  : s.if_b_index ;

        UPDATE_BUF("Received updates counter, including bad ones", "UpdateRxCnt");
        m.add_leaf(vtss::AsInt(s.stack_port[stat_if_index].update_rx_cnt),
                   vtss::tag::Name(name_buf),
                   vtss::expose::snmp::Status::Current,
                   vtss::expose::snmp::OidElementValue(ix++),
                   vtss::tag::Description(buf)); 
        
        UPDATE_BUF("Transmitted Updates, periodic (tx OK)", "UpdatePeriodicTxCnt");
        m.add_leaf(vtss::AsInt(s.stack_port[stat_if_index].update_periodic_tx_cnt),
                   vtss::tag::Name(name_buf),
                   vtss::expose::snmp::Status::Current,
                   vtss::expose::snmp::OidElementValue(ix++),
                   vtss::tag::Description(buf)); 
        
        UPDATE_BUF("Transmitted Updates, triggered (tx OK)", "UpdateTriggeredTxCnt");
        m.add_leaf(vtss::AsInt(s.stack_port[stat_if_index].update_triggered_tx_cnt),
                   vtss::tag::Name(name_buf),
                   vtss::expose::snmp::Status::Current,
                   vtss::expose::snmp::OidElementValue(ix++),
                   vtss::tag::Description(buf)); 

        UPDATE_BUF("Transmitted Updates, triggered (tx OK)", "UpdateTxPolicerDrop");
        m.add_leaf(vtss::AsInt(s.stack_port[stat_if_index].update_tx_policer_drop_cnt),
                   vtss::tag::Name(name_buf),
                   vtss::expose::snmp::Status::Current,
                   vtss::expose::snmp::OidElementValue(ix++),
                   vtss::tag::Description(buf)); 

        UPDATE_BUF("Received errornuous Updates", "UpdateRxErrCnt");
        m.add_leaf(vtss::AsInt(s.stack_port[stat_if_index].update_rx_err_cnt),
                   vtss::tag::Name(name_buf),
                   vtss::expose::snmp::Status::Current,
                   vtss::expose::snmp::OidElementValue(ix++),
                   vtss::tag::Description(buf)); 
        
        UPDATE_BUF("Tx of Update failed", "UpdateTxErrCnt");
        m.add_leaf(vtss::AsInt(s.stack_port[stat_if_index].update_tx_err_cnt),
                   vtss::tag::Name(name_buf),
                   vtss::expose::snmp::Status::Current,
                   vtss::expose::snmp::OidElementValue(ix++),
                   vtss::tag::Description(buf)); 
        UPDATE_BUF("Received Alerts, including bad ones", "AlertRxCnt");
        m.add_leaf(vtss::AsInt(s.stack_port[stat_if_index].alert_rx_cnt),
                   vtss::tag::Name(name_buf),
                   vtss::expose::snmp::Status::Current,
                   vtss::expose::snmp::OidElementValue(ix++),
                   vtss::tag::Description(buf)); 
        
        UPDATE_BUF("Transmitted Alerts (tx OK)", "AlertTxCnt");
        m.add_leaf(vtss::AsInt(s.stack_port[stat_if_index].alert_tx_cnt),
                   vtss::tag::Name(name_buf),
                   vtss::expose::snmp::Status::Current,
                   vtss::expose::snmp::OidElementValue(ix++),
                   vtss::tag::Description(buf)); 

        UPDATE_BUF("Tx-drops by Tx-policer", "AlertTxPolicerDropCnt");
        m.add_leaf(vtss::AsInt(s.stack_port[stat_if_index].alert_tx_policer_drop_cnt),
                   vtss::tag::Name(name_buf),
                   vtss::expose::snmp::Status::Current,
                   vtss::expose::snmp::OidElementValue(ix++),
                   vtss::tag::Description(buf)); 

        UPDATE_BUF("Received errornuous Alerts", "AlertRxErrCnt");
        m.add_leaf(vtss::AsInt(s.stack_port[stat_if_index].alert_rx_err_cnt),
                   vtss::tag::Name(name_buf),
                   vtss::expose::snmp::Status::Current,
                   vtss::expose::snmp::OidElementValue(ix++),
                   vtss::tag::Description(buf)); 
        
        UPDATE_BUF("Tx of Alert failed", "AlertTxErrCnt");
        m.add_leaf(vtss::AsInt(s.stack_port[stat_if_index].alert_tx_err_cnt),
                   vtss::tag::Name(name_buf),
                   vtss::expose::snmp::Status::Current,
                   vtss::expose::snmp::OidElementValue(ix++),
                   vtss::tag::Description(buf)); 
    }
}

//
// Switch index
//
struct TOPO_usid_index_1 {
    TOPO_usid_index_1(vtss_usid_t &x) : inner(x) { }
    vtss_usid_t &inner;
};

template<typename T>
void serialize(T &a, TOPO_usid_index_1 s) {
    a.add_leaf(vtss::AsInt(s.inner), vtss::tag::Name("Sid"),
               vtss::expose::snmp::RangeSpec<uint32_t>(1, 16),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(1),
               vtss::tag::Description("Switch Id."));
}

/****************************************************************************
 * Get and get declarations  -- See topo_expose.cxx
 ****************************************************************************/
mesa_rc vtss_topo_ser_usid_assignment_set(vtss_usid_t usid, const vtss_topo_ser_stack_mac_t *mac_addr);
mesa_rc vtss_topo_ser_usid_assignment_get(vtss_usid_t usid, vtss_topo_ser_stack_mac_t *mac_addr);
mesa_rc vtss_topo_ser_usid_swap_set(vtss_usid_t usid, const vtss_topo_ser_stack_swap_t *swap);
mesa_rc vtss_topo_ser_usid_swap_dummy_get(vtss_usid_t usid, vtss_topo_ser_stack_swap_t *swap);
mesa_rc vtss_topo_ser_usid_del_dummy_get(vtss_topo_ser_del_t *dummy);
mesa_rc vtss_topo_ser_usid_del_set(const vtss_topo_ser_del_t *dummy);
mesa_rc vtss_topo_ser_usid_reelect_dummy_get(vtss_topo_ser_reelect_t *dummy);
mesa_rc vtss_topo_ser_usid_reelect_set(const vtss_topo_ser_reelect_t *dummy);

//
// Doing the serializing
//
namespace vtss {
namespace appl {
namespace topo {
namespace interfaces {

struct TopoLeaf {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_usid_t>,
        vtss::expose::ParamVal<vtss_appl_topo_stack_config_t *>
    > P;

    static constexpr const char *table_description =
        "The topology configuration table.";

    static constexpr const char *index_description =
        "Switch Id index.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_usid_t &i) {
        serialize(h, TOPO_usid_index_1(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_topo_stack_config_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_topo_stack_config_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_iterator_switch);
    VTSS_EXPOSE_SET_PTR(vtss_appl_topo_stack_config_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_TOPO);
};

struct TopoPrioLeaf {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_usid_t>,
        vtss::expose::ParamVal<vtss_appl_topo_mst_elect_prio_t *>
    > P;

    static constexpr const char *table_description =
        "For stack master re-election priority configuration.";

    static constexpr const char *index_description =
        "Switch Id index.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_usid_t &i) {
        serialize(h, TOPO_usid_index_1(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_topo_mst_elect_prio_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_topo_mst_prio_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_iterator_switch);
    VTSS_EXPOSE_SET_PTR(vtss_appl_topo_mst_prio_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_TOPO);
};

struct TopoMacLeaf {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_usid_t>,
        vtss::expose::ParamVal<vtss_topo_ser_stack_mac_t *>
    > P;

    static constexpr const char *table_description =
        "The topology configuration table for assigning a switch id to a switch with a specific MAC address.";

    static constexpr const char *index_description =
        "Switch Id index.";


    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_usid_t &i) {
        serialize(h, TOPO_usid_index_1(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_topo_ser_stack_mac_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_topo_ser_usid_assignment_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_iterator_switch);
    VTSS_EXPOSE_SET_PTR(vtss_topo_ser_usid_assignment_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_TOPO);
    };


struct TopoSwapLeaf {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_usid_t>,
        vtss::expose::ParamVal<vtss_topo_ser_stack_swap_t *>
    > P;

    static constexpr const char *table_description =
        "The topology configuration for swapping switch ids";

    static constexpr const char *index_description =
        "Switch Id index";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_usid_t &i) {
        serialize(h, TOPO_usid_index_1(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_topo_ser_stack_swap_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    //
    VTSS_EXPOSE_GET_PTR(vtss_topo_ser_usid_swap_dummy_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_iterator_switch_all);
    VTSS_EXPOSE_SET_PTR(vtss_topo_ser_usid_swap_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_TOPO);
};

struct TopoDelLeaf {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_topo_ser_del_t *>
    > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_topo_ser_del_t &i) {
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_topo_ser_usid_del_dummy_get);
    VTSS_EXPOSE_SET_PTR(vtss_topo_ser_usid_del_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_TOPO);
};

struct TopoStatusLeaf {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_usid_t>,
        vtss::expose::ParamVal<vtss_appl_topo_switch_stat_t *>
    > P;

    static constexpr const char *table_description =
        "The topology status and statistics counters.";

    static constexpr const char *index_description =
        "Switch Id index.";


    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_usid_t &i) {
        serialize(h, TOPO_usid_index_1(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_topo_switch_stat_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_ITR_PTR(vtss_appl_iterator_switch);
    VTSS_EXPOSE_GET_PTR(vtss_appl_topo_switch_stat_get);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_TOPO);
};

struct TopoStatusValidIfLeaf {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<vtss_usid_t>,
        vtss::expose::ParamVal<vtss_appl_topo_valid_interfaces_pair_t *>
    > P;

    static constexpr const char *table_description =
        "Valid interface pairs that can be used to form a stacking interface.";

    static constexpr const char *index_description =
        "Switch Id index.";


    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_usid_t &i) {
        serialize(h, TOPO_usid_index_1(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_topo_valid_interfaces_pair_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(2));
        serialize(h, i);
    }

    VTSS_EXPOSE_ITR_PTR(vtss_appl_iterator_switch);
    VTSS_EXPOSE_GET_PTR(vtss_appl_valid_interfaces_pairs_get);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_TOPO);
};

struct TopoMstStatusLeaf {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_appl_topo_mst_stat_t *>
    > P;

    static constexpr const char *table_description =
        "The master status.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_topo_mst_stat_t &i) {
        h.argument_properties(vtss::expose::snmp::OidOffset(1));
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_topo_mst_stat_get);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_TOPO);
};



struct TopoReelectLeaf {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_topo_ser_reelect_t *>
    > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_topo_ser_reelect_t &i) {
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_topo_ser_usid_reelect_dummy_get);
    VTSS_EXPOSE_SET_PTR(vtss_topo_ser_usid_reelect_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_TOPO);
};
   
}  // namespace interfaces
}  // namespace topo
}  // namespace appl
}  // namespace vtss
#endif
