/*
 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
*/

#include "json_rpc_api.hxx"
#include "topo_serializer.hxx"

using namespace vtss::json;
using namespace vtss::expose::json;

namespace vtss {
void json_node_add(Node *node);
}  // namespace vtss

#define NS(N, P, D) static vtss::expose::json::NamespaceNode N(&P, D);
static NamespaceNode ns_topo("topo");
extern "C" void vtss_appl_topo_json_init() { vtss::json_node_add(&ns_topo); }

NS(topo_config,     ns_topo,      "config"); 
NS(topo_status,     ns_topo,      "status");
NS(topo_control,    ns_topo,      "control");

namespace vtss {
namespace appl {
namespace topo {
namespace interfaces {

static StructWriteOnly<TopoReelectLeaf> topo_reelct_leaf(&topo_control, "reelect");
static StructWriteOnly<TopoDelLeaf>     topo_del_leaf   (&topo_control, "del");
static TableWriteOnly<TopoSwapLeaf>     topo_swap_leaf  (&topo_control, "swap");
static TableReadWrite<TopoMacLeaf>      topo_mac_leaf   (&topo_config, "mac");
static TableReadWrite<TopoPrioLeaf>     topo_prio_leaf  (&topo_config, "prio");
static TableReadWrite<TopoLeaf>         topo_leaf       (&topo_config, "stacking");
static TableReadOnly<TopoStatusLeaf>    topo_status_leaf(&topo_status, "info");
static StructReadOnly<TopoMstStatusLeaf>topo_mst_status_leaf(&topo_status, "masterInfo");

}  // namespace interfaces
}  // namespace topo
}  // namespace appl
}  // namespace vtss

