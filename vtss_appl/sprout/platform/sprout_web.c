/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

#include <stdio.h>
#include "main.h"
#include "web_api.h"
#include "topo_api.h"
#include "msg_api.h"
#include "port_api.h"
#include "conf_api.h"
#ifdef VTSS_SW_OPTION_PRIV_LVL
#include "vtss_privilege_api.h"
#include "vtss_privilege_web_api.h"
#endif
#include "vtss/basics/trace.hxx"

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_TOPO

/* =================
 * Trace definitions
 * -------------- */
#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_WEB
#include <vtss_trace_api.h>
/* ============== */

static const topo_switch_t *stack_find_dev(topo_switch_list_t *tsl_p, mesa_mac_t *mac)
{
    u32 i;
    for (i = 0; i < ARRSZ(tsl_p->ts); i++) {
        topo_switch_t *ts_p = &tsl_p->ts[i];
        if (ts_p->vld &&
            memcmp(&ts_p->mac_addr, &mac->addr, sizeof(mac->addr)) == 0) {
            return ts_p;
        }
    }
    return NULL;
}

/****************************************************************************/
/*  Web Handler  functions                                                  */
/****************************************************************************/
static i32 handler_config_stack(CYG_HTTPD_STATE *p)
{
    topo_switch_list_t            *tsl_p = NULL;
    char                          prod_name[MSG_MAX_PRODUCT_NAME_LEN], prod_name_encoded[3 * (MSG_MAX_PRODUCT_NAME_LEN)];
    mesa_mac_addr_t               mac_addr;
    char                          macstr[sizeof "aa-bb-cc-dd-dd-ff"];
    int                           ct, i, j;
    BOOL                          cur_stack_enabled, conf_stack_enabled = FALSE;
    vtss_isid_t                   isid, local_isid = VTSS_ISID_START;
    vtss_usid_t                   local_usid;
    vtss_appl_topo_valid_interfaces_pair_t conf;
    vtss_appl_topo_stack_config_t stack_conf;
    mesa_rc                       rc;
    BOOL                          dirty; // Signals if a reboot is required

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_TOPO)) {
        return -1;
    }
#endif
    for (isid = VTSS_ISID_START; isid < VTSS_ISID_END; isid++) {
        if (msg_switch_is_local(isid)) {
            local_isid = isid;
            break;
        }
    }

    local_usid = topo_isid2usid(local_isid);
    cur_stack_enabled = vtss_stacking_enabled();
    if (cur_stack_enabled && ((VTSS_MALLOC_CAST(tsl_p, sizeof(topo_switch_list_t))) == NULL || topo_switch_list_get(tsl_p) != VTSS_OK)) {
        goto end;
    }

    if (p->method == CYG_HTTPD_METHOD_POST) {
        char var_name[32];
        mesa_mac_t mac;
        memset(&mac, 0, sizeof(mac));

        /* Count number of switches to determine if stacking can be disabled */
        ct = 0;
        for (isid = VTSS_ISID_START; isid < VTSS_ISID_END; isid++) {
            if (msg_switch_exists(isid)) {
                ct++;
            }
        }
        if (ct < 2) {
            conf_stack_enabled = (cyg_httpd_form_varable_string(p, "conf_stack_enabled", NULL) ? TRUE : FALSE);
        } else {
            /* Stacking is always enabled if more than one switch is present */
            conf_stack_enabled = TRUE;
        }

        for (isid = VTSS_ISID_START; ct < 2 && isid < VTSS_ISID_END; isid++) {
            if ((rc = vtss_appl_topo_stack_config_get(topo_isid2usid(isid), &stack_conf)) == VTSS_OK  &&
                vtss_appl_topo_stack_reboot_required_get(topo_isid2usid(isid), &dirty) == VTSS_OK &&
                conf_stack_enabled != stack_conf.stacking) {
                stack_conf.stacking = conf_stack_enabled;
                if ((rc = vtss_appl_topo_stack_config_set(topo_isid2usid(isid), &stack_conf)) != VTSS_OK) {
                    T_W("topo_stack_conf_set failed for isid %u: %s", isid, error_txt(rc));
                }
            }
        }

        for (i = 0; (tsl_p != NULL || cur_stack_enabled == FALSE) && i < ARRSZ(tsl_p->ts); i++) {
            const topo_switch_t *ts_p = NULL;
            sprintf(var_name, "mac_%d", i);
            if (cur_stack_enabled == FALSE || (cyg_httpd_form_variable_mac(p, var_name, &mac) && topo_switch_list_get(tsl_p) == VTSS_OK && (ts_p = stack_find_dev(tsl_p, &mac)) != NULL)) {
                misc_mac_txt(&mac.addr[0], macstr);
                sprintf(var_name, "del_%d", i);
                if (cur_stack_enabled && cyg_httpd_form_varable_find(p, var_name)) {
                    rc = vtss_appl_topo_usid_assignment_del(ts_p->usid);
                    T_D("Delete switch %s: %s", macstr, rc ? error_txt(rc) : "OK");
                } else {
                    int usid, prio, ports;
                    sprintf(var_name, "sid_%d", i);
                    if (! (cyg_httpd_form_varable_int(p, var_name, &usid) && VTSS_USID_LEGAL(usid))) {
                        continue;
                    }
                    isid = topo_usid2isid(usid);
                    if (cur_stack_enabled) {
                        if (usid != ts_p->usid && VTSS_ISID_LEGAL(isid)) {
                            // The vtss_appl_topo_usid_assign()-case will only succeed on switches that are not currently assigned an ISID due to lack of free entries.
                            if ((rc = vtss_appl_topo_usid_assignment_set(usid, ts_p->mac_addr)) != VTSS_OK) {
                                /* Try to swap instead */
                                if ((rc == VTSS_APPL_TOPO_ERROR_SID_IN_USE || rc == VTSS_APPL_TOPO_ERROR_SWITCH_HAS_SID) && VTSS_USID_LEGAL(ts_p->usid)) {
                                    T_D("Assign of %d to %s failed, swap(%d,%d)", usid, macstr, usid, ts_p->usid);
                                    rc = vtss_appl_topo_usid_swap(usid, ts_p->usid);
                                    isid = ts_p->isid;
                                }
                            } else {
                                T_D("Assigned SID %d to %s (was %d)", usid, macstr, ts_p->usid);
                            }
                            if (rc != VTSS_OK) {
                                T_W("Assigning SID %d to %s failed: %s", usid, macstr, error_txt(rc));
                            }
                        } else {
                            T_D("%s sid %d already set to %d", macstr, usid, ts_p->usid);
                        }
                        sprintf(var_name, "prio_%d", i);
                        if (cyg_httpd_form_varable_int(p, var_name, &prio) && VTSS_ISID_LEGAL(isid) && prio != ts_p->mst_elect_prio) {
                            rc = topo_parm_set(isid, TOPO_PARM_MST_ELECT_PRIO, prio);
                            T_D("Set Master priority SID %d MAC %s to %d (was %d): %s", usid, macstr, prio, ts_p->mst_elect_prio, rc ? error_txt(rc) : "OK");
                        }
                    }

                    // Common both when stacking is currently enabled and disabled.
                    sprintf(var_name, "ports_%d", i);
                    if (VTSS_ISID_LEGAL(isid) && cyg_httpd_form_varable_int(p, var_name, &ports)) {
                        rc = vtss_appl_valid_interfaces_pairs_get(usid, &conf);
                        if (rc != VTSS_RC_OK && rc != VTSS_APPL_TOPO_ERROR_SWITCH_NOT_PRESENT) {
                            T_E("vtss_appl_valid_interfaces_pairs_get failed, isid:%d", isid);
                        }
                        if (conf.count && ports < conf.count && ports != conf.selected_pair) {
                            stack_conf.stacking = conf_stack_enabled;
                            stack_conf.ifindex_0 = conf.pair[ports].interfaceA;
                            stack_conf.ifindex_1 = conf.pair[ports].interfaceB;
                            if ((rc = vtss_appl_topo_stack_config_set(topo_isid2usid(isid), &stack_conf)) != VTSS_OK) {
                                T_W("topo_stack_conf_set failed for isid %u: %s", isid, error_txt(rc));
                            }
                        }
                    }
                }
            }
        }

        if (cyg_httpd_form_varable_find(p, "reelect")) {
            T_D("Forcing master election");
            (void) topo_parm_set(0, TOPO_PARM_MST_TIME_IGNORE, 1);
        }
        VTSS_OS_MSLEEP(500);
        redirect(p, "/stack_config.htm");
    } else {
        cyg_httpd_start_chunked("html");

        // Format         : cur_stack_enabled|conf_stack_enabled|<switch_conf_1>|<switch_conf_2>|...|<switch_conf_N>
        // <switch_conf_n>: present/macaddr/usid/master/mst_capable/mst_elect_prio/prod_name/dirty/stack_capable_port_count/cur_stack_port_idx/<port_pair_1>/<port_pair_2>/.../<port_pair_M>
        // <port_pair_m>  : port_0/port_1

        /* Write stacking_enabled */
        for (isid = VTSS_ISID_START; isid < VTSS_ISID_END; isid++) {
            if (msg_switch_is_local(isid) && vtss_appl_topo_stack_config_get(topo_isid2usid(isid), &stack_conf) == VTSS_OK &&
                vtss_appl_topo_stack_reboot_required_get(topo_isid2usid(isid), &dirty) == VTSS_OK) {
                conf_stack_enabled = stack_conf.stacking;
            }
        }
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|%u|", cur_stack_enabled, conf_stack_enabled);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        /* Write switch list */
        if (cur_stack_enabled) {
            for (i = 0; tsl_p != NULL && i < ARRSZ(tsl_p->ts); i++) {
                topo_switch_t *ts_p = &tsl_p->ts[i];
                if (ts_p->vld) {
                    misc_mac_txt(ts_p->mac_addr, macstr);
                    if (msg_switch_is_master()) {
                        if (VTSS_ISID_LEGAL(ts_p->isid)) {
                            msg_product_name_get(ts_p->isid, prod_name);
                        } else {
                            // An ISID may be illegal (0) if a new 17th switch
                            // has arrived in the stack.
                            strcpy(prod_name, "N/A");
                        }
                    } else {
                        strcpy(prod_name, "-");
                    }
                    (void)cgi_escape(prod_name, prod_name_encoded);
                    int master = (memcmp(&ts_p->mac_addr, &tsl_p->mst_switch_mac_addr, sizeof(mesa_mac_addr_t)) == 0);
                    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d/%s/%d/%d/%d/%d/%s",
                                  ts_p->present,
                                  macstr,
                                  ts_p->usid,
                                  master,
                                  ts_p->mst_capable,
                                  ts_p->mst_elect_prio,
                                  prod_name_encoded);
                    cyg_httpd_write_chunked(p->outbuffer, ct);

                    rc = vtss_appl_valid_interfaces_pairs_get(ts_p->usid, &conf);
                    if (rc != VTSS_RC_OK && rc != VTSS_APPL_TOPO_ERROR_SWITCH_NOT_PRESENT) {
                        T_E("vtss_appl_valid_interfaces_pairs_get failed, isid:%d, %s", ts_p->isid, error_txt(rc));
                    }

                    rc = vtss_appl_topo_stack_reboot_required_get(ts_p->usid, &dirty);

                    if (rc != VTSS_RC_OK && rc != VTSS_APPL_TOPO_ERROR_SWITCH_NOT_PRESENT) {
                        T_E("Could not get dirty information, sid:%d", ts_p->usid);
                    }
                    T_I("dirty:%d,  pair selected:%d count:%d, sid:%d",  dirty, conf.selected_pair, conf.count, ts_p->isid);
                    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "/%u/%u/%u",
                                  dirty, conf.count, conf.selected_pair);
                    cyg_httpd_write_chunked(p->outbuffer, ct);
                    for (j = 0; j < conf.count; j++) {
                        vtss_ifindex_elm_t ife_0, ife_1;
                        VTSS_RC(vtss_ifindex_decompose(conf.pair[j].interfaceA, &ife_0));
                        VTSS_RC(vtss_ifindex_decompose(conf.pair[j].interfaceB, &ife_1));
                        T_I("ifA:%d, ifb:%d", ife_0.ordinal, ife_1.ordinal);
                        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "/%u/%u",
                                      iport2uport(ife_0.ordinal),
                                      iport2uport(ife_1.ordinal));
                        cyg_httpd_write_chunked(p->outbuffer, ct);
                    }
                }
                if (i != ARRSZ(tsl_p->ts) - 1) {
                    cyg_httpd_write_chunked("|", 1);
                }
            }
        } else {
            // Stacking is currently disabled.
            msg_product_name_get(local_isid, prod_name);
            (void)cgi_escape(prod_name, prod_name_encoded);
            /* Get board MAC address */
            (void)conf_mgmt_mac_addr_get(mac_addr, 0);
            misc_mac_txt(mac_addr, macstr);
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d/%s/%d/%d/%d/%d/%s",
                          TRUE, /* Present */
                          macstr,
                          topo_isid2usid(local_isid),
                          TRUE, /* Master */
                          TRUE, /* Master capable */
                          0,    /* Master Elect Prio: N/A */
                          prod_name_encoded);
            cyg_httpd_write_chunked(p->outbuffer, ct);

            rc = vtss_appl_valid_interfaces_pairs_get(topo_isid2usid(local_isid), &conf);

            if (rc != VTSS_RC_OK && rc != VTSS_APPL_TOPO_ERROR_SWITCH_NOT_PRESENT) {
                T_E("vtss_appl_valid_interfaces_pairs_get failed, isid:%d", local_isid);
            }

            rc = vtss_appl_topo_stack_reboot_required_get(topo_isid2usid(local_isid), &dirty);

            if (rc != VTSS_RC_OK && rc != VTSS_APPL_TOPO_ERROR_SWITCH_NOT_PRESENT) {
                T_E("Could not get reboot info");
            }

            T_I("dirty:%d,  pair selected:%d count:%d, sid:%d",  dirty, conf.selected_pair, conf.count, isid);
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "/%u/%u/%u", dirty, conf.count, conf.selected_pair);
            cyg_httpd_write_chunked(p->outbuffer, ct);
            for (j = 0; j < conf.count; j++) {
                vtss_ifindex_elm_t ife_0, ife_1;
                VTSS_RC(vtss_ifindex_decompose(conf.pair[j].interfaceA, &ife_0));
                VTSS_RC(vtss_ifindex_decompose(conf.pair[j].interfaceB, &ife_1));
                VTSS_TRACE(VTSS_TRACE_MODULE_ID, INFO) << "ifA:" << conf.pair[j].interfaceA << " ifB:" << conf.pair[j].interfaceA;
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "/%u/%u",
                              iport2uport(ife_0.ordinal),
                              iport2uport(ife_1.ordinal));
                cyg_httpd_write_chunked(p->outbuffer, ct);
            }
        }
        cyg_httpd_end_chunked();
    }

end:
    if (tsl_p) {
        VTSS_FREE(tsl_p);
    }
    return -1; // Do not further search the file system.
}

static i32 handler_stat_topo(CYG_HTTPD_STATE *p)
{
    topo_switch_stat_t            ts;
    topo_switch_list_t            *tsl_p = NULL;
    char                          macstr[sizeof "aa-bb-cc-dd-dd-ff"];
    int                           ct, i, chip_idx, idx[2];
    BOOL                          cur_stack_enabled, dirty;
    vtss_isid_t                   isid, local_isid = VTSS_ISID_START;
    vtss_appl_topo_stack_config_t stack_conf;
    char                          prod_name[MSG_MAX_PRODUCT_NAME_LEN], prod_name_encoded[3 * (MSG_MAX_PRODUCT_NAME_LEN)];
    char                          ver_str[MSG_MAX_VERSION_STRING_LEN], ver_str_encoded[3 * (MSG_MAX_VERSION_STRING_LEN)];
    mesa_rc                       rc;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_TOPO)) {
        return -1;
    }
#endif

    cyg_httpd_start_chunked("html");

    cur_stack_enabled = vtss_stacking_enabled();

    if (cur_stack_enabled && (vtss_appl_topo_switch_stat_get(VTSS_USID_LOCAL, &ts) != VTSS_OK || !(VTSS_MALLOC_CAST(tsl_p, sizeof(topo_switch_list_t))) || topo_switch_list_get(tsl_p) != VTSS_OK)) {
        goto end;
    }

    for (isid = VTSS_ISID_START; isid < VTSS_ISID_END; isid++) {
        if (msg_switch_is_local(isid)) {
            local_isid = isid;
            break;
        }
    }
    if (vtss_appl_topo_stack_config_get(topo_isid2usid(local_isid), &stack_conf) != VTSS_OK) {
        T_E("Could not get stack configuration");
    }


    rc = vtss_appl_topo_stack_reboot_required_get(topo_isid2usid(local_isid), &dirty);
    if (rc != VTSS_RC_OK && rc != VTSS_APPL_TOPO_ERROR_SWITCH_NOT_PRESENT) {
        T_E("Could not get dirty information");
    }
    T_I("dirty:%d", dirty);

    // Format          : <general>[|<switch_state_1>|<switch_state_2>|...|<switch_state_N)] NOTE: <switch_state_X> is only present when stacking is enabled.
    // <general>       : cur_stack_enabled|conf_stack_enabled|dirty|topology|stack_member_cnt|last_topology_change|master_mac_addr|last_master_change|stack_port_0|stack_port_1
    // <switch_state_n>: 0/<device_state_0>[|1/<device_state_1>] NOTE: the second part is only present on 2-device switches.
    // <device_state_x>: mac_addr/usid/front_port_str/distance_0/distance_1/forward_0/forward_1/product_name/version_str/mst_elect_prio/mst_time/mst_reelect

    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d|%d|%d|",
                  cur_stack_enabled,
                  stack_conf.stacking,
                  dirty);
    cyg_httpd_write_chunked(p->outbuffer, ct);

    if (cur_stack_enabled) {
        misc_mac_txt(tsl_p->mst_switch_mac_addr, macstr);
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d|%d|%s|%s|%s|",
                      ts.topology_type,
                      ts.switch_cnt,
                      misc_time2str(ts.topology_change_time),
                      macstr,
                      misc_time2str(tsl_p->mst_change_time)
                     );
    } else {
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d|%d|%s|%s|%s|",
                      0,
                      0,
                      "0",
                      "0",
                      "0");
    }
    cyg_httpd_write_chunked(p->outbuffer, ct);

    /* Get master stack port numbers */
    if (port_no_stack(0) < port_no_stack(1)) {
        idx[0] = 0;
        idx[1] = 1;
    } else {
        idx[0] = 1;
        idx[1] = 0;
    }
    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|%u|", iport2uport(port_no_stack(idx[0])), iport2uport(port_no_stack(idx[1])));
    cyg_httpd_write_chunked(p->outbuffer, ct);

    if (cur_stack_enabled == FALSE) {
        goto end;
    }

    /* Switch list & forwarding table */
    for (i = 0; i < ARRSZ(tsl_p->ts); i++) {
        topo_switch_t *ts_p = &tsl_p->ts[i];
        topo_chip_t   *ts_c;
        if (ts_p->vld && ts_p->present) {
            for (chip_idx = 0; chip_idx < ts_p->chip_cnt; chip_idx++) {
                ts_c = &ts_p->chip[chip_idx];
                misc_mac_txt(ts_p->mac_addr, macstr);

                if (VTSS_ISID_LEGAL(ts_p->isid)) {
                    msg_product_name_get(ts_p->isid, prod_name);
                    msg_version_string_get(ts_p->isid, ver_str);
                } else {
                    // An ISID may be illegal (0) if a new 17th switch
                    // has arrived in the stack.
                    strcpy(prod_name, "N/A");
                    strcpy(ver_str, "N/A");
                }
                (void)cgi_escape(prod_name, prod_name_encoded);
                (void)cgi_escape(ver_str,   ver_str_encoded);
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer),
                              "%d/%s/%d/%s/%s/%s/%s/%s/%s/%s/",
                              chip_idx, macstr, ts_p->usid,
                              vtss_sprout_port_mask_to_str(ts_c->ups_port_mask[0] | ts_c->ups_port_mask[1]),
                              ts_c->dist_str[idx[0]],
                              ts_c->dist_str[idx[1]],
                              topo_stack_port_fwd_mode_to_str(ts_c->stack_port_fwd_mode[idx[0]]),
                              topo_stack_port_fwd_mode_to_str(ts_c->stack_port_fwd_mode[idx[1]]),
                              prod_name_encoded,
                              ver_str_encoded);
                cyg_httpd_write_chunked(p->outbuffer, ct);

                /* Master information */
                if (ts_p->mst_capable) {
                    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d/%s/%d",
                                  ts_p->mst_elect_prio,
                                  ts_p->mst_time ? misc_time2interval(ts_p->mst_time) : "-",
                                  ts_p->mst_time_ignore);
                    cyg_httpd_write_chunked(p->outbuffer, ct);
                } else {
                    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "-/-/2");
                    cyg_httpd_write_chunked(p->outbuffer, ct);
                }
                cyg_httpd_write_chunked("|", 1);
            }
        }
    }

end:
    cyg_httpd_end_chunked();
    if (tsl_p) {
        VTSS_FREE(tsl_p);
    }
    return -1; // Do not further search the file system.
}

static i32 handler_stat_sid(CYG_HTTPD_STATE *p)
{
    topo_switch_list_t *tsl_p;
    int ct, i;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_TOPO)) {
        return -1;
    }
#endif

    if (!(VTSS_MALLOC_CAST(tsl_p, sizeof(topo_switch_list_t)))) {
        T_W("VTSS_MALLOC() failed, size=%zu", sizeof(topo_switch_list_t));
        return -1;
    }

    cyg_httpd_start_chunked("html");

    if (msg_switch_is_master()) {
        if (topo_switch_list_get(tsl_p) == VTSS_OK) {
            for (i = 0; (i < ARRSZ(tsl_p->ts)) && tsl_p->ts[i].vld; i++) {
                topo_switch_t *ts_p = &tsl_p->ts[i];
                port_isid_info_t pinfo;
                if (ts_p->vld && ts_p->present && ts_p->mst_capable &&
                    (port_isid_info_get(ts_p->isid, &pinfo) == VTSS_OK)) {
                    const char *flags = (memcmp(tsl_p->mst_switch_mac_addr, ts_p->mac_addr, sizeof(mesa_mac_addr_t)) == 0) ? "M" /* Master */ : "";
                    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u:%s:%u:%u:%u,",
                                  ts_p->usid, flags, pinfo.port_count,
                                  iport2uport(pinfo.stack_port_0), iport2uport(pinfo.stack_port_1));
                    cyg_httpd_write_chunked(p->outbuffer, ct);
                }
            }
        } else {
            /* Standalone */
            vtss_isid_t           isid = VTSS_ISID_START;
            vtss_usid_t           usid;
            for (isid = VTSS_ISID_START; isid < VTSS_ISID_END; isid++) {
                if (msg_switch_is_local(isid)) {
                    break;
                }
            }
            usid = topo_isid2usid(isid);
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d:M:%u:0:0",
                          usid, port_isid_port_count(VTSS_ISID_LOCAL));
            cyg_httpd_write_chunked(p->outbuffer, ct);
        }
    }

    cyg_httpd_end_chunked();

    VTSS_FREE(tsl_p);

    return -1; // Do not further search the file system.
}

/****************************************************************************/
/*  HTTPD Table Entries                                                     */
/****************************************************************************/

CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_config_stack, "/config/stack", handler_config_stack);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_stat_topo, "/stat/topo", handler_stat_topo);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_stat_sid, "/stat/sid", handler_stat_sid);

/****************************************************************************/
/*  End of file.                                                            */
/****************************************************************************/
