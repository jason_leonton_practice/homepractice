/*
 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
*/
#include "topo_serializer.hxx"
#include "vtss/appl/topo.h"

using namespace vtss;
using namespace vtss::expose::snmp;

VTSS_MIB_MODULE("topoMib", "TOPO", topo_mib_init, VTSS_MODULE_ID_TOPO, root, h) {
    h.add_history_element("201410010000Z", "Initial version");
    h.description("This is the Vitesse stack topology private MIB.");
}

#define NS(VAR, P, ID, NAME) static NamespaceNode VAR(&P, OidElement(ID, NAME))
NS(topo, root, 1, "topoMibObjects");;
NS(topo_config,  topo, 2, "topoConfig");;
NS(topo_status,  topo, 3, "topoStatus");;
NS(topo_control, topo, 4, "topoControl");;

namespace vtss {
namespace appl {
namespace topo {
namespace interfaces {

static TableReadWrite2<TopoLeaf>      topo_enable_leaf (&topo_config,  vtss::expose::snmp::OidElement(1, "topoConfigStackingTable"));
static TableReadWrite2<TopoPrioLeaf>  topo_prio_leaf   (&topo_config,  vtss::expose::snmp::OidElement(2, "topoConfigMstPrio"));
static TableReadWrite2<TopoMacLeaf>   topo_mac_leaf    (&topo_config,  vtss::expose::snmp::OidElement(3, "topoConfigMac"));
static TableReadWrite2<TopoSwapLeaf>  topo_swap_leaf   (&topo_control, vtss::expose::snmp::OidElement(4, "topoControlSwapSid"));
static StructRW2<TopoReelectLeaf>     topo_reelect_leaf(&topo_control, vtss::expose::snmp::OidElement(5, "topoControlMstElectSid"));
static StructRW2<TopoDelLeaf>         topo_del_leaf    (&topo_control, vtss::expose::snmp::OidElement(6, "topoControlDelSid"));
static TableReadOnly2<TopoStatusLeaf> topo_status_lead (&topo_status,  vtss::expose::snmp::OidElement(1, "topoStatus"));
static StructRO2<TopoMstStatusLeaf>   topo_mst_status_lead (&topo_status,  vtss::expose::snmp::OidElement(2, "topoStatusMaster"));
static TableReadOnly2<TopoStatusValidIfLeaf> topo_status_valid_interface_lnead (&topo_status,  vtss::expose::snmp::OidElement(3, "topoStatusValidPairs"));

}  // namespace interfaces
}  // namespace topo
}  // namespace appl
}  // namespace vtss

