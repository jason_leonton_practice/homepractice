/*************************************************************************************************
 * Copyright (C) 2018 Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: MPTEST log
 *
 *************************************************************************************************/
#include "control_api.h"
#include "misc_api.h"
#include "mptest.h"
#include "mptest_log.h"

#include "vtss_mtd_api.hxx"  // vtss_mtd_open
#include "mptest_api.h"      // mptest_mtd_erase

/**************************************************************************************************
 * Private definition
 *************************************************************************************************/
#define MPTEST_LOG_MAGIC    "MPTEST_T2_LOG"

/**************************************************************************************************
 * Class
 *************************************************************************************************/
mesa_rc mptest_log::logging(const char *fmt, ...)
{
    va_list         arg_list;
    time_t          tm = time(NULL);
    vtss_mtd_t      mtd;
    mesa_rc         rc;
    char            *logbuf = NULL;

    T_N("Entering");

    if(_total == MPTEST_LOG_TOTAL_ENTRY_COUNT) {
        T_W("The mp logging space has been exhausted!\n");
        return MESA_RC_ERROR;
    }

    // Add log entry
    _log_entry[_total].time = tm;
    va_start(arg_list, fmt);
    vsnprintf(_log_entry[_total].content, MPTEST_LOG_MAX_CONTENT_LEN, fmt, arg_list);
    va_end(arg_list);
    if(_log_entry[_total].content[strlen(_log_entry[_total].content) - 1] == '\n') {
        _log_entry[_total].content[strlen(_log_entry[_total].content) - 1] = '\0';
    }
    _log_entry[_total].content_len = strlen(_log_entry[_total].content);

    // Open mtd7
    if ((rc = vtss_mtd_open(&mtd, "rootfs_data")) != MESA_RC_OK) {
        T_W("FAILED: vtss_mtd_open");
        return MESA_RC_ERROR;
    }

    // Buf initialize
    if ((logbuf = (char *) malloc(MPTEST_LOG_FLASH_PAGE_TOTAL_SIZE)) == NULL) {
        T_W("FAILED: logbuf alloc");
        rc = MESA_RC_ERROR;
        goto LOGGINGEXIT;
    }
    memset(logbuf, 0, MPTEST_LOG_FLASH_PAGE_TOTAL_SIZE);
    memcpy(logbuf, &_log_header, sizeof(_log_header));
    memcpy(logbuf + sizeof(_log_header), _log_entry, sizeof(_log_entry));

    // Write log to NAND flash
    if ((rc = mptest_mtd_erase(&mtd,
                               MPTEST_LOG_FLASH_PAGE_TOTAL_SIZE,
                               _log_header.offset)) != MESA_RC_OK) {
        T_W("FAILED: mptest_mtd_erase");
        rc = MESA_RC_ERROR;
        goto LOGGINGEXIT;
    }
    if ((rc = vtss_mtd_program_pos(&mtd,
                                   (u8 *)logbuf,
                                   MPTEST_LOG_FLASH_PAGE_TOTAL_SIZE,
                                   _log_header.offset)) != MESA_RC_OK) {
        T_W("FAILED: vtss_mtd_program_pos");
        rc = MESA_RC_ERROR;
        goto LOGGINGEXIT;
    }

    _total++;
    T_D("_total[%d]", _total);

LOGGINGEXIT:
    vtss_mtd_close(&mtd);
    if (logbuf) {
        free(logbuf);
    }

    return rc;
}

mptest_log::mptest_log()
{
    char        *logbuf = NULL;
    int         idx;
    vtss_mtd_t  mtd;
    mesa_rc     rc;
    int         block_idx;
    off_t       offset = 0;
    bool        is_bad;

    T_N("Entering");

    // Private variable initialization
    memset(&_log_header, 0, (sizeof(_log_header)));
    memset(_log_entry, 0, (sizeof(_log_entry)));
    _total = 0;

    // Open mtd7
    if ((rc = vtss_mtd_open(&mtd, "rootfs_data")) != MESA_RC_OK) {
        T_W("FAILED: vtss_mtd_open");
        return;
    }

    // Buf initialize
    if ((logbuf = (char *) malloc(MPTEST_LOG_FLASH_PAGE_TOTAL_SIZE)) == NULL) {
        T_W("FAILED: logbuf alloc");
        goto MPLOGEXIT;
    }
    memset(logbuf, 0, MPTEST_LOG_FLASH_PAGE_TOTAL_SIZE);

    // Get log from nand flash
    for (block_idx = 0; block_idx < (mtd.info.size / mtd.info.erasesize); block_idx++) {
        // Find a good block
        if (mptest_mtd_is_bad(&mtd, block_idx, &is_bad) == MESA_RC_OK) {
            if (is_bad) {
                continue;
            }
        }

        offset = block_idx * mtd.info.erasesize;

        if (pread(mtd.fd,
                  logbuf,
                  MPTEST_LOG_FLASH_PAGE_TOTAL_SIZE,
                  offset) != MPTEST_LOG_FLASH_PAGE_TOTAL_SIZE) {
            T_W("Read log failed [%s]!\n", strerror(errno));
            goto MPLOGEXIT;
        }

        // Confirm mptest log format
        memcpy(&_log_header, logbuf, sizeof(_log_header));
        if (strncmp(_log_header.magic, MPTEST_LOG_MAGIC, strlen(MPTEST_LOG_MAGIC)) != 0) {
            T_D("T2 log invalid! magic[%s], MPTEST_LOG_MAGIC[%s], strlen(MPTEST_LOG_MAGIC)[%d]",
                _log_header.magic, MPTEST_LOG_MAGIC, strlen(MPTEST_LOG_MAGIC));

            strncpy(_log_header.magic, MPTEST_LOG_MAGIC, sizeof(_log_header.magic));
        } else {
            T_D("T2 log valid.");

            memcpy(_log_entry, logbuf + MPTEST_LOG_FLASH_PAGE_HEADER_SIZE, sizeof(_log_entry));
            for (idx = 0; idx < MPTEST_LOG_TOTAL_ENTRY_COUNT; idx++) {
                if (_log_entry[idx].content_len <= MPTEST_LOG_MAX_CONTENT_LEN &&
                    _log_entry[idx].content_len > 0) {
                    _total++;
                }
            }
        }
        _log_header.offset = offset;
        break;
    }

    T_D("Save log block[%d], offset[%d]", block_idx, _log_header.offset);

MPLOGEXIT:
    vtss_mtd_close(&mtd);
    if (logbuf) {
        free(logbuf);
    }

    T_D("Exist Total Mp Log: %d\n", _total);
}

mesa_rc mptest_log::clear()
{
    char        *logbuf = NULL;
    vtss_mtd_t  mtd;
    mesa_rc     rc;

    T_N("Entering");

    memset(_log_entry, 0, (sizeof(_log_entry)));
    _total = 0;

    if (vtss_mtd_open(&mtd, "rootfs_data") != MESA_RC_OK) {
        T_W("FAILED: vtss_mtd_open");
        return MESA_RC_ERROR;
    }

    // Buf initialize
    if ((logbuf = (char *) malloc(MPTEST_LOG_FLASH_PAGE_TOTAL_SIZE)) == NULL) {
        T_W("FAILED: logbuf alloc");
        rc = MESA_RC_ERROR;
        goto CLEAR_EXIT;
    }
    memset(logbuf, 0, MPTEST_LOG_FLASH_PAGE_TOTAL_SIZE);
    memcpy(logbuf, &_log_header, sizeof(_log_header));

    // Clear log
    if ((rc = mptest_mtd_erase(&mtd,
                               MPTEST_LOG_FLASH_PAGE_TOTAL_SIZE,
                               _log_header.offset)) != MESA_RC_OK) {
        T_W("FAILED: mptest_mtd_erase");
        rc = MESA_RC_ERROR;
        goto CLEAR_EXIT;
    }
    if ((rc = vtss_mtd_program_pos(&mtd,
                                   (u8 *)logbuf,
                                   MPTEST_LOG_FLASH_PAGE_TOTAL_SIZE,
                                   _log_header.offset)) != MESA_RC_OK) {
        T_W("FAILED: vtss_mtd_program_pos");
        rc = MESA_RC_ERROR;
        goto CLEAR_EXIT;
    }

CLEAR_EXIT:
    vtss_mtd_close(&mtd);
    if (logbuf) {
        free(logbuf);
    }

    return rc;
}

void mptest_log::dump(u32 session_id, void (*_pr)(u32 session_id, const char *fmt, ...))
{
    unsigned int            idx = 0;
    struct tm               *time = NULL;

    _pr(session_id, " ---- MP log start ----\n");

    while(idx != _total) {
        time = localtime((time_t *)&(_log_entry[idx].time));
        _pr(session_id, "[%02d:%02d:%02d] %s\n",
                                time->tm_hour,
                                time->tm_min,
                                time->tm_sec,
                                _log_entry[idx].content);
        idx++;
    }

    _pr(session_id, " ---- MP log entry # = %d\n", _total);
}

off_t mptest_log::get_log_offset()
{
    return _log_header.offset;
}
