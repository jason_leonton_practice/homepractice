/*************************************************************************************************
 * Copyright (C) 2018 Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: MPTEST icli functions header
 *
 *************************************************************************************************/

#ifndef _MPTEST_ICLI_FUNCTIONS_H_
#define _MPTEST_ICLI_FUNCTIONS_H_

#include "conf_api.h"
#include "firmware.h"
#include "icli_api.h"
#include "led_api.h"
#include "mptest_api.h"  // For mptest_flash_type_t

#ifdef __cplusplus
extern "C" {
#endif

#include "vtss/appl/poe.h"

typedef enum {
	MPTEST_SFP_OP_PRESENT = 0,
	MPTEST_SFP_OP_LOS,
	MPTEST_SFP_OP_TX_FAULT
} mptest_sfp_op_t;

#define SFP_I2C_ADDRESS					(0x50)
#define SFP_MSA_REG_VENDOR_OUI			(37)
#define SFP_MSA_REG_VENDOR_OUI_LENGTH	(3)

mesa_rc mptest_icli_pktloop_test(u32 session_id, uint sec);
mesa_rc mptest_icli_sysled_color_set(meba_led_type_t type, meba_led_mode_t mode, meba_led_color_t color);
mesa_rc mptest_icli_sysled_color_get(meba_led_type_t port, meba_led_color_t *color);
mesa_rc mptest_icli_portled_color_set(mesa_port_no_t port_no, meba_led_color_t color);
mesa_rc mptest_icli_sfp_status_get(mesa_port_no_t port_no, mptest_sfp_op_t op, bool *data);
mesa_rc mptest_icli_sfp_msa_data_get(u32 session_id, mesa_port_no_t port_no, char offset, unsigned char *buf, int buf_len);
mesa_rc mptest_icli_sfp_status_check(u32 session_id, mesa_port_no_t port_no, mptest_sfp_op_t op, bool val);
mesa_rc mptest_icli_poe_port_conf_get(mesa_port_no_t port, vtss_appl_poe_port_conf_t *conf);
mesa_rc mptest_icli_poe_port_conf_set(mesa_port_no_t port, vtss_appl_poe_port_conf_t *conf);
mesa_rc mptest_icli_poe_global_conf_get(vtss_appl_poe_global_conf_t *conf);
mesa_rc mptest_icli_poe_global_conf_set(vtss_appl_poe_global_conf_t *conf);
mesa_rc mptest_icli_poe_global_supply_get(int *supply);
mesa_rc mptest_icli_poe_global_supply_set(int supply);
mesa_rc mptest_icli_poe_status_get(mesa_port_no_t port, vtss_appl_poe_port_status_t *status);
mesa_rc mptest_icli_poe_status_check(u32 session_id,  mesa_port_no_t start_port, mesa_port_no_t end_port, unsigned int status, int sec);
mesa_rc mptest_icli_poe_status_check_all(u32 session_id, unsigned int status, int sec);
mesa_rc mptest_icli_poe_intpse_check(u32 session_id, BOOL val);
mesa_rc mptest_icli_poe_power_check(u32 session_id, int power_val, mesa_port_no_t start_port, mesa_port_no_t end_port);
void mptest_icli_poe_intpse_enable(u32 session_id, mesa_port_no_t port_index, BOOL enable);
void mptest_icli_ddr_test(u32 session_id, unsigned int size_mb_cnt);
void mptest_icli_flash_test(u32 session_id, mptest_flash_type_t type);
mesa_rc mptest_icli_nand_bad_check(u32 session_id, unsigned int badblock_cnt);
mesa_rc mptest_icli_nand_size_check(u32 session_id, unsigned int nand_size);
mesa_rc mptest_icli_log_read(u32 session_id);
mesa_rc mptest_icli_log_write(void *msg, unsigned int len);
mesa_rc mptest_icli_log_clear(void);
mesa_rc mptest_icli_usb_rw(u32 session_id, int size);
mesa_rc mptest_icli_relay_set(BOOL val);
mesa_rc mptest_icli_rstbtn_get(BOOL *val);
mesa_rc mptest_icli_rstbtn_check(u32 session_id, BOOL val);
mesa_rc mptest_icli_power_test(u32 session_id, unsigned int sec);
mesa_rc mptest_icli_power_and_relay_test(u32 session_id, unsigned int sec);
mesa_rc mptest_icli_rstbtn_test(unsigned int sec);
mesa_rc mptest_icli_firmware_info_get(vtss_appl_firmware_status_image_type_t type, vtss_appl_firmware_status_image_t *imageEntry);
void mptest_icli_firmware_upgrade_dual_set(bool dual_image);
mesa_rc mptest_icli_firmware_upgrade(u32 session_id, const char *url);

#ifdef __cplusplus
}
#endif

#endif /* _MPTEST_ICLI_FUNCTIONS_H_ */
