/*************************************************************************************************
 * Copyright (C) 2018 Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: MPTEST API header
 *
 *************************************************************************************************/
#ifndef _MPTEST_API_H_
#define _MPTEST_API_H_

#include "vtss_mtd_api.hxx"     // For vtss_mtd_open

typedef enum {
    MPTEST_PKT_STATE_INIT,
    MPTEST_PKT_STATE_START,
    MPTEST_PKT_STATE_STOP,
    MPTEST_PKT_STATE_RESTART,
    MPTEST_PKT_STATE_SEND,
    MPTEST_PKT_STATE_CHECK_SPEED,
    MPTEST_PKT_STATE_CHECK_CRC,
    MPTEST_PKT_STATE_CHECK_TXRX,
    MPTEST_PKT_STATE_CHECK_COUNT,
    MPTEST_PKT_STATE_MAX
} mptest_pkt_state_t;

typedef enum {
    MPTEST_FLASH_TYPE_NOR,
    MPTEST_FLASH_TYPE_NAND,
    MPTEST_FLASH_TYPE_MAX
} mptest_flash_type_t;

mesa_rc mptest_init(vtss_init_data_t *data);

void mptest_log_read(u32 session_id);
mesa_rc mptest_log_write(void *msg, unsigned int len);
mesa_rc mptest_log_clear(void);

void mptest_packet_set_time(uint seconds);
void mptest_packet_set_tx_cnt(uint tx_cnt);
mesa_rc mptest_packet_testing(u32 session_id, mptest_pkt_state_t state, bool record);
mesa_rc mptest_flash_testing(u32 session_id, mptest_flash_type_t type, bool record);
mesa_rc mptest_packet_link_wait(u32 session_id, bool record);
mesa_rc mptest_ddr_testing(u32 session_id, unsigned int size_mb_cnt, bool record);
mesa_rc mptest_nand_bad_scan(unsigned int *badblock_cnt);
mesa_rc mptest_mtd_erase(const vtss_mtd_t *mtd, size_t length, off_t offset);
mesa_rc mptest_mtd_is_bad(const vtss_mtd_t *mtd, int eraseblock, bool *is_bad);

#endif  /* _MPTEST_API_H_ */
