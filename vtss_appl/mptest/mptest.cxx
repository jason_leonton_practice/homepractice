/*************************************************************************************************
 * Copyright (C) 2018 Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: MPTEST
 *
 *************************************************************************************************/
#include <string.h>
#include "conf_api.h"
#include "critd_api.h"
#include "icli_api.h"
#include "main.h"
#include "misc_api.h"
#include "mptest.h"             // For trace
#include "mptest_api.h"         // For mptest_flash_type_t
#include "mptest_log.h"
#include "msg_api.h"

#include "led_api.h"            // For lntn_sysled_set_color
#include "port.h"               // For mesa_port_counters_t
#include <sys/ioctl.h>          // For ioctl

/**************************************************************************************************
 * Private definition
 *************************************************************************************************/
#define MPTEST_MAX_TEMP_BUFF_LEN        (128)
#define MPTEST_SIZE_KB                  (1024)
#define MPTEST_SIZE_MB                  (MPTEST_SIZE_KB * 1024)

#define MPTEST_STATE_T2                 (2)
#define MPTEST_STATE_T2_ERR             (21)

#define MPTEST_MAX_BURNIN_TIMES         (14400)     //  4 hr
#define MPTEST_DDR_TEST_CYCLE           (60)        //  1 min
#define MPTEST_NOR_FLASH_TEST_CYCLE     (300)       //  5 min
#define MPTEST_NAND_FLASH_TEST_CYCLE    (300)       //  5 min
#define MPTEST_PACKET_TEST_CYCLE        (600)       // 10 min

#define MPTEST_DDR_TEST_BLOCK_NUM       (64)
#define MPTEST_DDR_TEST_SIZE            (64)        // MB
#define MPTEST_TESTING_PATTERN          (0x5AA5A55A)
#define MPTEST_FLASH_TEST_BUF           (2 * 1024)  // 2KB

#define MPTEST_CONF_VLAN_BASE           (2)
#define MPTEST_PACKET_COMPARE_TOLERANCE (10)
#define MPTEST_PACKET_LINK_TIMEOUT      (10)  // sec
#define MPTEST_PACKET_COUNT_RANGE       (0.65)

typedef struct {
    vtss_handle_t   thread_handle;
    vtss_thread_t   thread_block;
    int             flag;
    unsigned int    session_id;
    mptest_log      *log;
} mptest_global_t;

typedef enum {
    MPTEST_PORT_TYPE_NONE,
    MPTEST_PORT_TYPE_FAST_ETHERNET,
    MPTEST_PORT_TYPE_GIGABIT_ETHERNET,
    MPTEST_PORT_TYPE_2_5_GIGABIT_ETHERNET,
    MPTEST_PORT_TYPE_FIVE_GIGABIT_ETHERNET,
    MPTEST_PORT_TYPE_TEN_GIGABIT_ETHERNET,
    MPTEST_PORT_TYPE_MAX
} mptest_port_type_t;

char *mptest_interface_names[MPTEST_PORT_TYPE_MAX] = {
    "none",
    "FastEthernet",
    "GigabitEthernet",
    "2.5GigabitEthernet",
    "5GigabitEthernet",
    "10GigabitEthernet",
};

typedef enum {
    MPTEST_PKT_CHECK_CRC,
    MPTEST_PKT_CHECK_TXRX,
    MPTEST_PKT_CHECK_MAX
} mptest_pkt_check_t;

typedef enum {
    MPTEST_ERROE_NONE = 0,
    MPTEST_ERROE_PKT_TX_ZERO,
    MPTEST_ERROE_PKT_RX_ZERO,
    MPTEST_ERROE_PKT_RX_LOS,
    MPTEST_ERROE_PKT_CRC,
    MPTEST_ERROE_PKT_SPEED,
    MPTEST_ERROE_PKT_COUNT,
    MPTEST_ERROE_PKT_LINKDOWN,
    MPTEST_ERROE_MEMORY_ALLOCATE = 11,
    MPTEST_ERROE_MEMORY_COMPARE,
    MPTEST_ERROE_NOR_FLASH_OPEN = 21,
    MPTEST_ERROE_NOR_FLASH_READ,
    MPTEST_ERROE_NOR_FLASH_ERASE,
    MPTEST_ERROE_NOR_FLASH_PROGRAM,
    MPTEST_ERROE_NOR_FLASH_COMPARE,
    MPTEST_ERROE_NAND_FLASH_OPEN = 31,
    MPTEST_ERROE_NAND_FLASH_READ,
    MPTEST_ERROE_NAND_FLASH_ERASE,
    MPTEST_ERROE_NAND_FLASH_PROGRAM,
    MPTEST_ERROE_NAND_FLASH_COMPARE,
    MPTEST_ERROE_NAND_FLASH_BAD,
    MPTEST_ERROE_MAX
} mptest_error_t;

typedef struct {
    mptest_port_type_t  type;
    int                 index;
} mptest_port_info_t;

#define UNUSED_PARM(x)                      ((void)x)

/**************************************************************************************************
 * Global variables
 *************************************************************************************************/
#if (VTSS_TRACE_ENABLED)

static vtss_trace_reg_t trace_reg =
{
    VTSS_TRACE_MODULE_ID, "mptest", "Mass Production Testing"
};

static vtss_trace_grp_t trace_grps[VTSS_TRACE_GRP_CNT] = {
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default",
        VTSS_TRACE_LVL_INFO,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CRIT */ {
        "crit",
        "Critical regions",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
};

#define MPTEST_CRIT_ENTER() critd_enter(&mptest.crit, VTSS_TRACE_GRP_CRIT, \
                                        VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define MPTEST_CRIT_EXIT()  critd_exit( &mptest.crit, VTSS_TRACE_GRP_CRIT, \
                                        VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)

#else   /* VTSS_TRACE_ENABLED */
#define MPTEST_CRIT_ENTER() critd_enter(&mptest.crit)
#define MPTEST_CRIT_EXIT()  critd_exit( &mptest.crit)

#endif  /* VTSS_TRACE_ENABLED */

static mptest_global_t mptest;

/* g_seconds is used to average the packet count */
static uint g_seconds = 0;
static uint g_tx_cnt = 1;

static lntn_global_conf_t g_lntn_conf;
static CapArray<mptest_port_info_t, MESA_CAP_PORT_CNT> g_mptest_port_info;

/**************************************************************************************************
 * Private function
 *************************************************************************************************/
static void mptest_icli_printf_callback(u32 session_id, const char *fmt, ...)
{
    va_list     arg_list;
    char        strbuff[MPTEST_MAX_TEMP_BUFF_LEN];

    memset(strbuff, 0, MPTEST_MAX_TEMP_BUFF_LEN);

    va_start(arg_list, fmt);
    vsnprintf(strbuff, MPTEST_MAX_TEMP_BUFF_LEN, fmt, arg_list);
    va_end(arg_list);

    (void) icli_session_printf(session_id, strbuff);
}

static BOOL mptest_debug_char_put(icli_addrword_t id, char ch)
{
    UNUSED_PARM(id);
    fputc(ch, stdout);
    return TRUE;
}

static BOOL mptest_debug_str_put(icli_addrword_t id, const char *str)
{
    UNUSED_PARM(id);
    fputs(str, stdout);
    return TRUE;
}

static mesa_rc mptest_open_icli_session(unsigned int *id)
{
    mesa_rc                     rc = MESA_RC_OK;
    int                         cmd_mode_level = 0;
    icli_session_open_data_t    icli_data;

    memset(&icli_data, 0, sizeof(icli_session_open_data_t));
    icli_data.name      = "MPTEST";
    icli_data.way       = ICLI_SESSION_WAY_APP_EXEC;
    icli_data.char_put  = mptest_debug_char_put;
    icli_data.str_put   = mptest_debug_str_put;

    if(icli_session_open(&icli_data, id) != ICLI_RC_OK) {
        return MESA_RC_ERROR;
    }

    if((rc = icli_session_privilege_set(*id,
                (icli_privilege_t)((int)ICLI_PRIVILEGE_MAX - 1))) != ICLI_RC_OK) {
        T_W("Cannot set ICFG session privilege; rc = %d", rc);
    } else {
        if((cmd_mode_level = icli_session_mode_enter(*id, ICLI_CMD_MODE_EXEC)) < 0) {
            T_W("Cannot enter ICLI config mode; rc = %d", cmd_mode_level);
            rc = MESA_RC_ERROR;
        }
    }

    return rc;
}

static void mptest_lightup_t21_led(void)
{
    // Fault LED green solid
    lntn_sysled_set_color(MEBA_LED_TYPE_FAULT, MEBA_LED_COLOR_GREEN);
    lntn_sysled_set_mode(MEBA_LED_TYPE_FAULT, MEBA_LED_MODE_BLINK_SOLID);

    // Master LED yello fast blink
    lntn_sysled_set_color(MEBA_LED_TYPE_MASTER, MEBA_LED_COLOR_YELLOW);
    lntn_sysled_set_mode(MEBA_LED_TYPE_MASTER, MEBA_LED_MODE_BLINK_SOLID);
}

static void mptest_lightup_start_led(void)
{
    // Fault LED green solid
    lntn_sysled_set_color(MEBA_LED_TYPE_FAULT, MEBA_LED_COLOR_GREEN);
    lntn_sysled_set_mode(MEBA_LED_TYPE_FAULT, MEBA_LED_MODE_BLINK_SOLID);

    // Master LED green fast blink
    lntn_sysled_set_color(MEBA_LED_TYPE_MASTER, MEBA_LED_COLOR_GREEN);
    lntn_sysled_set_mode(MEBA_LED_TYPE_MASTER, MEBA_LED_MODE_BLINK_RESERVECLR_FAST);
}

static void mptest_lightup_ok_led(void)
{
    // Master LED green fast blink
    lntn_sysled_set_color(MEBA_LED_TYPE_MASTER, MEBA_LED_COLOR_GREEN);
    lntn_sysled_set_mode(MEBA_LED_TYPE_MASTER, MEBA_LED_MODE_BLINK_SOLID);
}

static void mptest_lightup_fail_led(void)
{
    // Fault LED yello fast blink
    lntn_sysled_set_color(MEBA_LED_TYPE_FAULT, MEBA_LED_COLOR_YELLOW);
    lntn_sysled_set_mode(MEBA_LED_TYPE_FAULT, MEBA_LED_MODE_BLINK_RESERVECLR_FAST);

    // Master LED yello fast blink
    lntn_sysled_set_color(MEBA_LED_TYPE_MASTER, MEBA_LED_COLOR_YELLOW);
    lntn_sysled_set_mode(MEBA_LED_TYPE_MASTER, MEBA_LED_MODE_BLINK_RESERVECLR_FAST);

    // Debug LED yello fast blink
    lntn_sysled_set_color(MEBA_LED_TYPE_DEBUG, MEBA_LED_COLOR_YELLOW);
    lntn_sysled_set_mode(MEBA_LED_TYPE_DEBUG, MEBA_LED_MODE_BLINK_RESERVECLR_FAST);

    // Ring LED yello fast blink
    lntn_sysled_set_color(MEBA_LED_TYPE_RING, MEBA_LED_COLOR_YELLOW);
    lntn_sysled_set_mode(MEBA_LED_TYPE_RING, MEBA_LED_MODE_BLINK_RESERVECLR_FAST);
}

static mptest_port_type_t mptest_port_type_get(unsigned int capability)
{
    mptest_port_type_t port_type = MPTEST_PORT_TYPE_NONE;

    T_D("Capability[0x%x]", capability);

    if (capability & MEBA_PORT_CAP_10G_FDX) {
        port_type = MPTEST_PORT_TYPE_TEN_GIGABIT_ETHERNET;
    } else if (capability & MEBA_PORT_CAP_5G_FDX) {
        port_type = MPTEST_PORT_TYPE_FIVE_GIGABIT_ETHERNET;
    } else if (capability & MEBA_PORT_CAP_2_5G_FDX) {
        port_type = MPTEST_PORT_TYPE_2_5_GIGABIT_ETHERNET;
    } else if (capability & MEBA_PORT_CAP_1G_FDX) {
        port_type = MPTEST_PORT_TYPE_GIGABIT_ETHERNET;
    } else if (capability & (MEBA_PORT_CAP_100M_FDX | MEBA_PORT_CAP_100M_HDX |
                             MEBA_PORT_CAP_10M_FDX | MEBA_PORT_CAP_10M_HDX)) {
        port_type = MPTEST_PORT_TYPE_FAST_ETHERNET;
    } else {
        port_type = MPTEST_PORT_TYPE_NONE;
        T_W("Capability unknown");
    }

    return port_type;
}

static void mptest_port_info_get()
{
    int cap_10geth_cnt  = 0;
    int cap_5geth_cnt   = 0;
    int cap_2_5geth_cnt = 0;
    int cap_1geth_cnt   = 0;
    int cap_fasteth_cnt = 0;
    int port_no;
    unsigned int capability;
    lntn_port_conf_t *port_conf;

    port_conf_for_each((&g_lntn_conf), port_conf, port_no) {
        capability = port_conf->capability;

        g_mptest_port_info[port_no].type  = mptest_port_type_get(capability);
        if (capability & MEBA_PORT_CAP_10G_FDX) {
            g_mptest_port_info[port_no].index = ++cap_10geth_cnt;
        } else if (capability & MEBA_PORT_CAP_5G_FDX) {
            g_mptest_port_info[port_no].index = ++cap_5geth_cnt;
        } else if (capability & MEBA_PORT_CAP_2_5G_FDX) {
            g_mptest_port_info[port_no].index = ++cap_2_5geth_cnt;
        } else if (capability & MEBA_PORT_CAP_1G_FDX) {
            g_mptest_port_info[port_no].index = ++cap_1geth_cnt;
        } else if (capability & (MEBA_PORT_CAP_100M_FDX | MEBA_PORT_CAP_100M_HDX |
                                 MEBA_PORT_CAP_10M_FDX | MEBA_PORT_CAP_10M_HDX)) {
            g_mptest_port_info[port_no].index = ++cap_fasteth_cnt;
        } else {
            T_W("Capability unknown");
            g_mptest_port_info[port_no].index = -1;
        }

        T_D("Port%d: type[%d], index[%d]", port_no, g_mptest_port_info[port_no].type, g_mptest_port_info[port_no].index);
    }
}

static mesa_rc mptest_t2fault_msg(u32 session_id, char *msg, bool record)
{
    static BOOL has_error = false;
    time_t      cur_time = time(NULL);
    struct tm   *tm_info = NULL;

    tm_info = localtime(&cur_time);
    icli_session_printf(session_id, "[%02d:%02d:%02d]%s\n",
        tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec, msg);

    if (!record) {
        return MESA_RC_OK;
    }

    mptest.log->logging("%s", msg);

    if (!has_error) {
        if (conf_mgmt_board_mpflag_set(MPTEST_STATE_T2_ERR) != MESA_RC_OK) {
            T_W("Set conf failed\n");
            return MESA_RC_OK;
        }
        if (conf_mgmt_board_save() != MESA_RC_OK) {
            T_W("Board set operation failed\n");
        }

        mptest_lightup_fail_led();
        has_error = true;
    }

    return MESA_RC_OK;
}

mesa_rc mptest_mtd_erase(const vtss_mtd_t *mtd, size_t length, off_t offset)
{
    struct erase_info_user mtdEraseInfo;
    off_t start;

    T_N("Entering");

    for (start = 0; start < length; start += mtd->info.erasesize) {
        mtdEraseInfo.start = start + offset;
        mtdEraseInfo.length = mtd->info.erasesize;
        (void) ioctl(mtd->fd, MEMUNLOCK, &mtdEraseInfo);
        if (ioctl (mtd->fd, MEMERASE, &mtdEraseInfo) < 0) {
            T_E("%s: Erase error at 0x%08zx: %s", mtd->dev, (size_t) offset, strerror(errno));
            return MESA_RC_ERROR;
        }
    }

    return MESA_RC_OK;
}

static void mptest_port_speed_set(u32 session_id)
{
    char                interface_str[64];
    mptest_port_type_t  port_type;
    int                 port_no;
    lntn_port_conf_t    *port_conf;

    T_N("Entering");

    (void)icli_session_cmd_exec(session_id, "configure terminal", TRUE);

    port_conf_for_each((&g_lntn_conf), port_conf, port_no) {
        memset(interface_str, 0, sizeof(interface_str));
        snprintf(interface_str, sizeof(interface_str), "interface %s 1/%d",
                    mptest_interface_names[g_mptest_port_info[port_no].type],
                    g_mptest_port_info[port_no].index);

        (void)icli_session_cmd_exec(session_id, interface_str, TRUE);

        port_type = mptest_port_type_get(port_conf->capability);
        if (port_type == MPTEST_PORT_TYPE_TEN_GIGABIT_ETHERNET) {
            (void)icli_session_cmd_exec(session_id, "speed 10G", TRUE);
        } else if (port_type == MPTEST_PORT_TYPE_GIGABIT_ETHERNET) {
            (void)icli_session_cmd_exec(session_id, "speed 1000", TRUE);
        } else if (port_type == MPTEST_PORT_TYPE_FAST_ETHERNET) {
            (void)icli_session_cmd_exec(session_id, "speed 100", TRUE);
        } else {
            (void)icli_session_cmd_exec(session_id, "speed auto", TRUE);
        }

        (void)icli_session_cmd_exec(session_id, "exit", TRUE);
    }

    (void)icli_session_cmd_exec(session_id, "end", TRUE);
}

static void mptest_port_vlan_set(u32 session_id)
{
    char    interface_str[64];
    char    vlan_str[16];
    char    access_str[32];
    char    enable_vlan_str[16];
    int     vlan = MPTEST_CONF_VLAN_BASE;
    int     port_no;
    lntn_port_conf_t *port_conf;

    T_N("Entering");

    (void)icli_session_cmd_exec(session_id, "configure terminal", TRUE);

    port_conf_for_each((&g_lntn_conf), port_conf, port_no) {

        memset(interface_str, 0, sizeof(interface_str));
        memset(vlan_str, 0, sizeof(vlan_str));
        memset(access_str, 0, sizeof(access_str));

        snprintf(interface_str, sizeof(interface_str), "interface %s 1/%d-%d",
                    mptest_interface_names[g_mptest_port_info[port_no].type],
                    g_mptest_port_info[port_no].index,
                    g_mptest_port_info[port_no+1].index);
        snprintf(vlan_str, sizeof(vlan_str), "pvlan %d", vlan);
        snprintf(access_str, sizeof(access_str), "switchport access vlan %d", vlan);

        (void)icli_session_cmd_exec(session_id, interface_str, TRUE);
        (void)icli_session_cmd_exec(session_id, "no pvlan 1", TRUE);
        (void)icli_session_cmd_exec(session_id, vlan_str, TRUE);
        (void)icli_session_cmd_exec(session_id, access_str, TRUE);
        (void)icli_session_cmd_exec(session_id, "exit", TRUE);
        vlan++;
        port_no++;
    }

    memset(enable_vlan_str, 0, sizeof(enable_vlan_str));
    snprintf(enable_vlan_str, sizeof(enable_vlan_str), "vlan 1-%d", vlan-1);
    (void)icli_session_cmd_exec(session_id, enable_vlan_str,TRUE);
    (void)icli_session_cmd_exec(session_id, "end", TRUE);
}

static void mptest_packet_init(u32 session_id)
{
    T_N("Entering");

    mptest_port_speed_set(session_id);
    mptest_port_vlan_set(session_id);
}

static void mptest_packet_send(u32 session_id)
{
    char cmd_buf[32];
    T_N("Entering");

    (void)icli_session_cmd_exec(session_id, "clear statistics *", TRUE);

    VTSS_OS_MSLEEP(1000);

    memset(cmd_buf, 0, sizeof(cmd_buf));
    snprintf(cmd_buf, sizeof(cmd_buf), "debug frame tx-cnt %d", g_tx_cnt);
    (void)icli_session_cmd_exec(session_id, cmd_buf, TRUE);
    (void)icli_session_cmd_exec(session_id, "debug frame tx interface *", TRUE);
}

static void mptest_packet_start(u32 session_id)
{
    T_N("Entering");

    (void)icli_session_cmd_exec(session_id, "configure terminal", TRUE);
    (void)icli_session_cmd_exec(session_id, "interface *", TRUE);
    (void)icli_session_cmd_exec(session_id, "no shutdown", TRUE);
    (void)icli_session_cmd_exec(session_id, "no pvlan 1", TRUE);
    (void)icli_session_cmd_exec(session_id, "end", TRUE);

    mptest.log->logging("Packet test start");
}

static void mptest_packet_stop(u32 session_id)
{
    T_N("Entering");

    (void)icli_session_cmd_exec(session_id, "configure terminal", TRUE);
    (void)icli_session_cmd_exec(session_id, "interface *", TRUE);
    (void)icli_session_cmd_exec(session_id, "shutdown", TRUE);
    (void)icli_session_cmd_exec(session_id, "end", TRUE);
}

static void mptest_packet_restart(u32 session_id)
{
    T_N("Entering");

    (void)icli_session_cmd_exec(session_id, "configure terminal", TRUE);
    (void)icli_session_cmd_exec(session_id, "interface *", TRUE);
    (void)icli_session_cmd_exec(session_id, "shutdown", TRUE);
    (void)icli_session_cmd_exec(session_id, "no shutdown", TRUE);
    (void)icli_session_cmd_exec(session_id, "end", TRUE);
}

static mesa_rc mptest_packet_compare(u32 session_id,
                                     int port1,
                                     int port2,
                                     int range,
                                     mptest_pkt_check_t type,
                                     bool record)
{
    vtss_ifindex_t          p1_ifindex;
    vtss_ifindex_t          p2_ifindex;
    mesa_port_counters_t    p1_counters;
    mesa_port_counters_t    p2_counters;
    mesa_rc                 rc = MESA_RC_OK;
    char                    log[MPTEST_LOG_ENTRY_MAX_SIZE];

    T_N("Entering");

    vtss_ifindex_from_port(1, port1, &p1_ifindex);
    vtss_ifindex_from_port(1, port2, &p2_ifindex);

    if (vtss_appl_port_counters_get(p1_ifindex, &p1_counters) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }
    if (vtss_appl_port_counters_get(p2_ifindex, &p2_counters) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }

    switch (type) {
        case MPTEST_PKT_CHECK_CRC:
            if (p1_counters.rmon.rx_etherStatsCRCAlignErrors) {
                memset(log, 0, sizeof(log));
                snprintf(log, sizeof(log), "Err(%d,%d,%llu)",
                    MPTEST_ERROE_PKT_CRC, port1, p1_counters.rmon.rx_etherStatsCRCAlignErrors);
                mptest_t2fault_msg(session_id, log, record);
                rc = MESA_RC_ERROR;
            }
            if (p2_counters.rmon.rx_etherStatsCRCAlignErrors) {
                memset(log, 0, sizeof(log));
                snprintf(log, sizeof(log), "Err(%d,%d,%llu)",
                    MPTEST_ERROE_PKT_CRC, port2, p2_counters.rmon.rx_etherStatsCRCAlignErrors);
                mptest_t2fault_msg(session_id, log, record);
                rc = MESA_RC_ERROR;
            }
            break;
        case MPTEST_PKT_CHECK_TXRX:
            if (!p1_counters.rmon.tx_etherStatsPkts) {
                memset(log, 0, sizeof(log));
                snprintf(log, sizeof(log), "Err(%d,%d)", MPTEST_ERROE_PKT_TX_ZERO, port1);
                mptest_t2fault_msg(session_id, log, record);
                rc = MESA_RC_ERROR;
            }
            if (!p2_counters.rmon.tx_etherStatsPkts) {
                memset(log, 0, sizeof(log));
                snprintf(log, sizeof(log), "Err(%d,%d)", MPTEST_ERROE_PKT_TX_ZERO, port2);
                mptest_t2fault_msg(session_id, log, record);
                rc = MESA_RC_ERROR;
            }

            if (!p1_counters.rmon.rx_etherStatsPkts) {
                memset(log, 0, sizeof(log));
                snprintf(log, sizeof(log), "Err(%d,%d)", MPTEST_ERROE_PKT_RX_ZERO, port1);
                mptest_t2fault_msg(session_id, log, record);
                rc = MESA_RC_ERROR;
            }
            if (!p2_counters.rmon.rx_etherStatsPkts) {
                memset(log, 0, sizeof(log));
                snprintf(log, sizeof(log), "Err(%d,%d)", MPTEST_ERROE_PKT_RX_ZERO, port2);
                mptest_t2fault_msg(session_id, log, record);
                rc = MESA_RC_ERROR;
            }

            if (p1_counters.rmon.tx_etherStatsPkts > p2_counters.rmon.rx_etherStatsPkts + g_tx_cnt) {
                memset(log, 0, sizeof(log));
                snprintf(log, sizeof(log), "Err(%d,%d,%llu)",
                    MPTEST_ERROE_PKT_RX_LOS, port2,
                    p1_counters.rmon.tx_etherStatsPkts - p2_counters.rmon.rx_etherStatsPkts);
                mptest_t2fault_msg(session_id, log, record);
                rc = MESA_RC_ERROR;
            }
            if (p2_counters.rmon.tx_etherStatsPkts > p1_counters.rmon.rx_etherStatsPkts + g_tx_cnt) {
                memset(log, 0, sizeof(log));
                snprintf(log, sizeof(log), "Err(%d,%d,%llu)",
                    MPTEST_ERROE_PKT_RX_LOS, port1,
                    p2_counters.rmon.tx_etherStatsPkts - p1_counters.rmon.rx_etherStatsPkts);
                mptest_t2fault_msg(session_id, log, record);
                rc = MESA_RC_ERROR;
            }

            if(g_seconds != 0) {
                // for debug, Bps to Mbps => 1 Bps= 1 * 8 / 1024 / 1024 = 1 / 131072 Mbps
                icli_session_printf(session_id, "port %d, avg(Mbps): %llu, %llu\n",
                    port1,
                    p1_counters.rmon.tx_etherStatsOctets / 131072 / g_seconds,
                    p1_counters.rmon.rx_etherStatsOctets / 131072 / g_seconds);
                icli_session_printf(session_id, "port %d, avg(Mbps): %llu, %llu\n",
                    port2,
                    p2_counters.rmon.tx_etherStatsOctets / 131072 / g_seconds,
                    p2_counters.rmon.rx_etherStatsOctets / 131072 / g_seconds);
            }

            break;
        default:
            T_W("Packet check tpye error(%d)", type);
            rc = MESA_RC_ERROR;
            break;
    }

    if (rc == MESA_RC_ERROR) {
        T_W("port[%d] tx[%llu] rx[%llu] crc[%llu]",
            port1, p1_counters.rmon.tx_etherStatsPkts,
            p1_counters.rmon.rx_etherStatsPkts, p1_counters.rmon.rx_etherStatsCRCAlignErrors);
        T_W("port[%d] tx[%llu] rx[%llu] crc[%llu]",
            port2, p2_counters.rmon.tx_etherStatsPkts,
            p2_counters.rmon.rx_etherStatsPkts, p2_counters.rmon.rx_etherStatsCRCAlignErrors);
    } else {
        T_D("port[%d] tx[%llu] rx[%llu] crc[%llu]",
            port1, p1_counters.rmon.tx_etherStatsPkts,
            p1_counters.rmon.rx_etherStatsPkts, p1_counters.rmon.rx_etherStatsCRCAlignErrors);
        T_D("port[%d] tx[%llu] rx[%llu] crc[%llu]",
            port2, p2_counters.rmon.tx_etherStatsPkts,
            p2_counters.rmon.rx_etherStatsPkts, p2_counters.rmon.rx_etherStatsCRCAlignErrors);
    }

    return rc;
}

static mesa_rc mptest_packet_count_check(u32 session_id, float range, bool record)
{
    unsigned int            port_no=0;
    vtss_ifindex_t          p_ifindex;
    mesa_port_counters_t    p_counters;
    mesa_port_counter_t     p_count_max=0, p_count_threshold=0;
    mesa_rc                 rc = MESA_RC_OK;
    char                    log[MPTEST_LOG_ENTRY_MAX_SIZE];

    T_N("Entering");

    for (port_no = 0; port_no < lntn_conf_copper_count((&g_lntn_conf)); port_no++) {
        vtss_ifindex_from_port(1, port_no, &p_ifindex);
        if (vtss_appl_port_counters_get(p_ifindex, &p_counters) != MESA_RC_OK) {
            return MESA_RC_ERROR;
        }
        if(p_counters.rmon.rx_etherStatsPkts > p_count_max){
            p_count_max = p_counters.rmon.rx_etherStatsPkts;
        }
    }

    p_count_threshold = p_count_max*range;
    T_D("max rx[%llu] threshold[%llu]",p_count_max, p_count_threshold);

    for (port_no = 0; port_no < lntn_conf_port_count((&g_lntn_conf)); port_no++) {
        vtss_ifindex_from_port(1, port_no, &p_ifindex);
        if (vtss_appl_port_counters_get(p_ifindex, &p_counters) != MESA_RC_OK) {
            return MESA_RC_ERROR;
        }
        T_D("port[%d] rx[%llu] threshold[%llu]",port_no, p_counters.rmon.rx_etherStatsPkts, p_count_threshold);

        if(p_counters.rmon.rx_etherStatsPkts < p_count_threshold){
            memset(log, 0, sizeof(log));
            snprintf(log, sizeof(log), "Err(%d,%d,%llu,%llu)",
                MPTEST_ERROE_PKT_COUNT, port_no, (uint64_t)(p_counters.rmon.rx_etherStatsPkts/131072), (uint64_t)(p_count_threshold/131072));
            /* Bps to MBps : (packets/1024/1024)*8 => packets/131072 */

            mptest_t2fault_msg(session_id, log, record);
            T_W("Err port[%d] rx[%llu] threshold[%llu]",port_no, p_counters.rmon.rx_etherStatsPkts, p_count_threshold);
            rc = MESA_RC_ERROR;
        }
    }

    return rc;
}

static mesa_rc mptest_packet_check(u32 session_id, mptest_pkt_check_t type, bool record)
{
    unsigned int    port_no;
    int             error = 0;

    T_N("Entering");

    for (port_no = 0; port_no < lntn_conf_port_count((&g_lntn_conf)); port_no+=2) {
        if (mptest_packet_compare(session_id, port_no, port_no + 1,
                                  MPTEST_PACKET_COMPARE_TOLERANCE, type, record) != MESA_RC_OK) {
            error++;
        }
    }

    if (error) {
        T_W("Error[%d]", error);
        return MESA_RC_ERROR;
    }

    return MESA_RC_OK;
}

static mesa_port_speed_t mptest_packet_maxspeed_get(u32 session_id, int port_no)
{
    mesa_port_speed_t   maxspeed;
    mptest_port_type_t  type = g_mptest_port_info[port_no].type;

    T_N("Entering");

    switch (type) {
        case MPTEST_PORT_TYPE_FAST_ETHERNET:
            maxspeed = MESA_SPEED_100M;
            break;
        case MPTEST_PORT_TYPE_GIGABIT_ETHERNET:
            maxspeed = MESA_SPEED_1G;
            break;
        case MPTEST_PORT_TYPE_2_5_GIGABIT_ETHERNET:
            maxspeed = MESA_SPEED_2500M;
            break;
        case MPTEST_PORT_TYPE_FIVE_GIGABIT_ETHERNET:
            maxspeed = MESA_SPEED_5G;
            break;
        case MPTEST_PORT_TYPE_TEN_GIGABIT_ETHERNET:
            maxspeed = MESA_SPEED_10G;
            break;
        default:
            maxspeed = MESA_SPEED_UNDEFINED;
            (void) icli_session_printf(session_id, "Type[%d] error!\n", type);
            break;
    }

   return maxspeed;
}

static mesa_rc mptest_packet_speed_check(u32 session_id, bool record)
{
    vtss_appl_port_status_t     port_status;
    mesa_port_status_t          *status = &port_status.status;
    vtss_ifindex_t              ifindex;
    lntn_port_conf_t            *port_conf;
    mesa_port_speed_t           maxspeed;
    int                         speed_err_cnt = 0;
    char                        log[MPTEST_LOG_ENTRY_MAX_SIZE];
    int                         port_no;

    T_N("Entering");

    port_conf_for_each((&g_lntn_conf), port_conf, port_no) {
        vtss_ifindex_from_port(1, port_no, &ifindex);
        vtss_appl_port_status_get(ifindex, &port_status);

        maxspeed = mptest_packet_maxspeed_get(session_id, port_no);

        // Check speed
        if (status->speed != maxspeed) {
            (void) icli_session_printf(session_id, "Port %d: Speed FAIL\n", port_no);
            (void) icli_session_printf(session_id, "Maxspeed[%d] vs. Link speed[%d]\n",
                                                    maxspeed, status->speed);
            memset(log, 0, sizeof(log));
            snprintf(log, sizeof(log), "Err(%d,%d)", MPTEST_ERROE_PKT_SPEED, port_no);
            mptest_t2fault_msg(session_id, log, record);
            speed_err_cnt++;
        }
    }

    return speed_err_cnt ? MESA_RC_ERROR : MESA_RC_OK;
}

/**************************************************************************************************
 * Public function
 *************************************************************************************************/
mesa_rc mptest_ddr_testing(u32 session_id, unsigned int size_mb_cnt, bool record)
{
    mptest_error_t  error = MPTEST_ERROE_NONE;
    int             i = 0;
    char            pattern_blk[MPTEST_SIZE_MB];
    char            cmp_target[MPTEST_SIZE_MB];
    char            *ram_block[MPTEST_DDR_TEST_BLOCK_NUM];
    int             ptn = MPTEST_TESTING_PATTERN;

    T_N("Entering");

    if (size_mb_cnt <= 0 || size_mb_cnt > MPTEST_DDR_TEST_BLOCK_NUM) {
        T_W("Testing range is 1~64(MB)\n");
        return MESA_RC_ERROR;
    }

    memset(ram_block, 0, sizeof(char *) * MPTEST_DDR_TEST_BLOCK_NUM);

    for (i = 0; i < MPTEST_SIZE_MB; i += 4) {
        memcpy(pattern_blk + i, &ptn, sizeof(int));
    }

    for (i = 0; i < size_mb_cnt; i++) {
        if (!(ram_block[i] = (char *)calloc(MPTEST_SIZE_MB, sizeof(char)))) {
            T_W("Memory allocate failed!\n");
            error = MPTEST_ERROE_MEMORY_ALLOCATE;
            break;
        }
        memcpy(ram_block[i], pattern_blk, MPTEST_SIZE_MB);

        memset(cmp_target, 0, MPTEST_SIZE_MB);
        memcpy(cmp_target, ram_block[i], MPTEST_SIZE_MB);

        if (memcmp(cmp_target, pattern_blk, MPTEST_SIZE_MB) != 0) {
            T_W("Memory compare failed! ram_addr: %p\n", ram_block[i]);
            error = MPTEST_ERROE_MEMORY_COMPARE;
            break;
        }
    }

    for (i = 0; i < size_mb_cnt; i++) {
        if (ram_block[i]) {
            free(ram_block[i]);
        } else {
            break;
        }
    }

    if (error != MPTEST_ERROE_NONE) {
        (void) icli_session_printf(session_id, "[RESULT]DDR: FAIL\n");
        if (record) {
            mptest.log->logging("Err(%d)", error);
        }
        return MESA_RC_ERROR;
    }

    (void) icli_session_printf(session_id, "[RESULT]DDR: OK\n");
    return MESA_RC_OK;
}

mesa_rc mptest_mtd_is_bad(const vtss_mtd_t *mtd, int eraseblock, bool *is_bad)
{
    int     ret;
    loff_t  seek;

    T_N("Entering");

    seek = (loff_t)eraseblock * mtd->info.erasesize;
    ret = ioctl(mtd->fd, MEMGETBADBLOCK, &seek);

    if (ret < 0) {
        T_W("%s: MTD get bad block failed.", mtd->dev);
        return MESA_RC_ERROR;

    } else if (ret == 1) {
        T_D("%s: Bad eraseblock %d", mtd->dev, eraseblock);
        *is_bad = true;
        return MESA_RC_OK;
    }

    *is_bad = false;
    return MESA_RC_OK;
}

mesa_rc mptest_nand_bad_scan(unsigned int *badblock_cnt)
{
    int             eraseblock;
    vtss_mtd_t      mtd;
    mesa_rc         rc;
    unsigned int    badblock = 0;
    bool            is_bad;

    T_N("Entering");

    if ((rc = vtss_mtd_open(&mtd, "rootfs_data")) != VTSS_OK) {
        T_W("FAILED: vtss_mtd_open");
        return MESA_RC_ERROR;
    }

    T_D("nand size(%u)", mtd.info.size);
    T_D("nand erasesize(%u)", mtd.info.erasesize);
    T_D("nand writesize(%u)", mtd.info.writesize);

    for (eraseblock = 0; eraseblock < (mtd.info.size / mtd.info.erasesize); eraseblock++) {
        if (mptest_mtd_is_bad(&mtd, eraseblock, &is_bad) == MESA_RC_OK) {
            if (is_bad) {
                badblock++;
            }
        }
    }
    *badblock_cnt = badblock;

    vtss_mtd_close(&mtd);

    return MESA_RC_OK;
}

mesa_rc mptest_flash_testing(u32 session_id, mptest_flash_type_t type, bool record)
{
    mptest_error_t  error = MPTEST_ERROE_NONE;
    int             i = 0;
    char            pattern_blk[MPTEST_FLASH_TEST_BUF];
    char            read_blk[MPTEST_FLASH_TEST_BUF];
    vtss_mtd_t      mtd;
    mesa_rc         rc;
    int             eraseblock;
    size_t          pagesize, erasesize;
    off_t           offset;
    int             ptn = MPTEST_TESTING_PATTERN;
    bool            is_bad;
    char            log[MPTEST_LOG_ENTRY_MAX_SIZE];

    T_N("Entering");

    memset(pattern_blk, 0, sizeof(pattern_blk));
    memset(read_blk, 0, sizeof(read_blk));

    for (i = 0; i < MPTEST_FLASH_TEST_BUF; i += 4) {
        memcpy(pattern_blk + i, &ptn, sizeof(int));
    }

    if (type == MPTEST_FLASH_TYPE_NOR) {
        if ((rc = vtss_mtd_open(&mtd, "conf")) != VTSS_OK) {
            T_W("FAILED: vtss_mtd_open");
            error = MPTEST_ERROE_NOR_FLASH_OPEN;
            goto FLASH_RESULT;
        }

        pagesize    = mtd.info.writesize;
        erasesize   = mtd.info.erasesize;
        offset      = 3 * erasesize;

        if ((rc = mptest_mtd_erase(&mtd, erasesize, offset)) != MESA_RC_OK) {
            T_W("FAILED: vtss_mtd_erase");
            error = MPTEST_ERROE_NOR_FLASH_ERASE;
            goto FLASH_EXIT;
        }
        if ((rc = vtss_mtd_program_pos(&mtd, (u8 *)pattern_blk, pagesize, offset)) != VTSS_OK) {
            T_W("FAILED: vtss_mtd_program_pos");
            error = MPTEST_ERROE_NOR_FLASH_PROGRAM;
            goto FLASH_EXIT;
        }
        if (pread(mtd.fd, read_blk, pagesize, offset) != pagesize) {
            T_W("Read read_blk failed [%s]!\n", strerror(errno));
            error = MPTEST_ERROE_NOR_FLASH_READ;
            goto FLASH_EXIT;
        }
        if (memcmp(pattern_blk, read_blk, pagesize) != 0) {
            T_W("NOR compare failed!");
            error = MPTEST_ERROE_NOR_FLASH_COMPARE;
        }

    } else {  // MPTEST_FLASH_TYPE_NAND
        if ((rc = vtss_mtd_open(&mtd, "rootfs_data")) != VTSS_OK) {
            T_W("FAILED: vtss_mtd_open");
            error = MPTEST_ERROE_NAND_FLASH_OPEN;
            goto FLASH_RESULT;
        }

        for (eraseblock = 0; eraseblock < (mtd.info.size / mtd.info.erasesize); eraseblock++) {
            if (mptest_mtd_is_bad(&mtd, eraseblock, &is_bad) == MESA_RC_OK) {
                if (eraseblock == (mptest.log->get_log_offset()/mtd.info.erasesize)) {
                    T_I("Skip log block[%d]", eraseblock);
                    continue;
                }
                if (!is_bad) {
                    break;
                }
            }
        }
        T_I("NAND test block[%d]", eraseblock);

        if (eraseblock >= (mtd.info.size / mtd.info.erasesize)) {
            error = MPTEST_ERROE_NAND_FLASH_BAD;
            goto FLASH_EXIT;
        }

        pagesize    = mtd.info.writesize;
        erasesize   = mtd.info.erasesize;
        offset      = eraseblock * erasesize;

        if ((rc = mptest_mtd_erase(&mtd, erasesize, offset)) != MESA_RC_OK) {
            T_W("FAILED: vtss_mtd_erase");
            error = MPTEST_ERROE_NAND_FLASH_ERASE;
            goto FLASH_EXIT;
        }
        if ((rc = vtss_mtd_program_pos(&mtd, (u8 *)pattern_blk, pagesize, offset)) != VTSS_OK) {
            T_W("FAILED: vtss_mtd_program_pos");
            error = MPTEST_ERROE_NAND_FLASH_PROGRAM;
            goto FLASH_EXIT;
        }
        if (pread(mtd.fd, read_blk, pagesize, eraseblock * mtd.info.erasesize) != pagesize) {
            T_W("Read read_blk failed [%s]!\n", strerror(errno));
            error = MPTEST_ERROE_NAND_FLASH_READ;
            goto FLASH_EXIT;
        }
        if (memcmp(pattern_blk, read_blk, pagesize) != 0) {
            T_W("NAND compare failed!");
            error = MPTEST_ERROE_NAND_FLASH_COMPARE;
        }
    }

FLASH_EXIT:
    vtss_mtd_close(&mtd);

FLASH_RESULT:
    if (error != MPTEST_ERROE_NONE) {
        if (type == MPTEST_FLASH_TYPE_NOR) {
        (void) icli_session_printf(session_id, "[RESULT]NOR Flash: FAIL\n");
        } else {  // MPTEST_FLASH_TYPE_NAND
            (void) icli_session_printf(session_id, "[RESULT]NAND Flash: FAIL\n");
        }

        memset(log, 0, sizeof(log));
        snprintf(log, sizeof(log), "Err(%d)",error);
        mptest_t2fault_msg(session_id, log, record);
        return MESA_RC_ERROR;
    }

    if (type == MPTEST_FLASH_TYPE_NOR) {
        (void) icli_session_printf(session_id, "[RESULT]NOR Flash: OK\n");
    } else {  // MPTEST_FLASH_TYPE_NAND
        (void) icli_session_printf(session_id, "[RESULT]NAND Flash: OK\n");
    }

    return MESA_RC_OK;
}

mesa_rc mptest_packet_link_wait(u32 session_id, bool record)
{
    vtss_appl_port_status_t     port_status;
    mesa_port_status_t          *status = &port_status.status;
    vtss_ifindex_t              ifindex;
    int                         port_no;
    int                         link_timeout = MPTEST_PACKET_LINK_TIMEOUT;
    int                         link_cnt = 0;
    mesa_rc                     rc = MESA_RC_OK;
    CapArray<bool, MESA_CAP_PORT_CNT> link;

    T_N("Entering");

    while(link_timeout--) {
        for (port_no = 0; port_no < lntn_conf_port_count((&g_lntn_conf)); port_no++) {
            if (link[port_no]) {
                continue;
            }

            vtss_ifindex_from_port(1, port_no, &ifindex);
            vtss_appl_port_status_get(ifindex, &port_status);

            // Check link status
            if (status->link) {
                link[port_no] = true;
                link_cnt++;
            }
        }

        if (link_cnt == lntn_conf_port_count((&g_lntn_conf))) {
            break;
        }

        VTSS_OS_MSLEEP(1000);
    }

    for (port_no = 0; port_no < lntn_conf_port_count((&g_lntn_conf)); port_no++) {
        if (!link[port_no]) {
            char log[MPTEST_LOG_ENTRY_MAX_SIZE];
            memset(log, 0, sizeof(log));
            snprintf(log, sizeof(log), "Err(%d,%d)", MPTEST_ERROE_PKT_LINKDOWN, port_no);
            mptest_t2fault_msg(session_id, log, record);
            (void) icli_session_printf(session_id, "Port %d Link down.\n", port_no);
            rc = MESA_RC_ERROR;
        } else {
            (void) icli_session_printf(session_id, "Port %d Link up.\n", port_no);
        }
    }

    return rc;
}

// mptest_packet_set_time is used to average packet count
void mptest_packet_set_time(uint seconds)
{
    g_seconds = seconds;
}

// mptest_packet_set_tx_cnt is used set the amount of packet want to send
void mptest_packet_set_tx_cnt(uint tx_cnt)
{
    g_tx_cnt = tx_cnt;
}

mesa_rc mptest_packet_testing(u32 session_id, mptest_pkt_state_t state, bool record)
{
    mesa_rc     ret = MESA_RC_OK;

    T_D("Entering, state[%d]", state);

    switch(state) {
        case MPTEST_PKT_STATE_INIT:
            mptest_packet_init(session_id);
            break;

        case MPTEST_PKT_STATE_START:
            mptest_packet_start(session_id);
            break;

        case MPTEST_PKT_STATE_STOP:
            mptest_packet_stop(session_id);
            break;

        case MPTEST_PKT_STATE_RESTART:
            mptest_packet_restart(session_id);
            break;

        case MPTEST_PKT_STATE_SEND:
            mptest_packet_send(session_id);
            break;

        case MPTEST_PKT_STATE_CHECK_SPEED:
            if ((ret = mptest_packet_speed_check(session_id, record)) != MESA_RC_OK) {
                (void) icli_session_printf(session_id, "[RESULT]Packet: Speed FAIL\n");
            } else {
                (void) icli_session_printf(session_id, "[RESULT]Packet: Speed OK\n");
            }
            break;

        case MPTEST_PKT_STATE_CHECK_CRC:
            if ((ret = mptest_packet_check(session_id, MPTEST_PKT_CHECK_CRC, record)) != MESA_RC_OK) {
                (void) icli_session_printf(session_id, "[RESULT]Packet: CRC FAIL\n");
            } else {
                (void) icli_session_printf(session_id, "[RESULT]Packet: CRC OK\n");
            }
            break;

        case MPTEST_PKT_STATE_CHECK_TXRX:
            if ((ret = mptest_packet_check(session_id, MPTEST_PKT_CHECK_TXRX, record)) != MESA_RC_OK) {
                (void) icli_session_printf(session_id, "[RESULT]Packet: TXRX FAIL\n");
            } else {
                (void) icli_session_printf(session_id, "[RESULT]Packet: TXRX OK\n");
            }
            break;
        case MPTEST_PKT_STATE_CHECK_COUNT:
            if ((ret = mptest_packet_count_check(session_id,MPTEST_PACKET_COUNT_RANGE,record)) != MESA_RC_OK){
                (void) icli_session_printf(session_id, "[RESULT]Packet: COUNT CHECK FAIL\n");
            } else {
                (void) icli_session_printf(session_id, "[RESULT]Packet: COUNT CHECK OK\n");
            }
            break;
        default:
            T_W("Packet check state error(%d)", state);
            ret = MESA_RC_ERROR;
            break;
    }

    return ret;
}

void mptest_log_read(u32 session_id)
{
    mptest.log->dump(session_id, mptest_icli_printf_callback);
}

mesa_rc mptest_log_write(void *msg, unsigned int len)
{
    return mptest.log->logging("%s", msg);
}

mesa_rc mptest_log_clear()
{
    return mptest.log->clear();
}

/**********************************************************************
** Thread
**********************************************************************/
static void mptest_thread(vtss_addrword_t data)
{
    int             times = MPTEST_MAX_BURNIN_TIMES;
    unsigned int    err_cnt = 0;

    msg_wait(MSG_WAIT_UNTIL_MASTER_UP_POST, VTSS_MODULE_ID_MPTEST);

    T_N("Entering");

    g_lntn_conf = lntn_conf_get();
    mptest_port_info_get();
    conf_mgmt_mpflag_get(&mptest.flag);

    // Open new ICLI session
    if(mptest_open_icli_session(&(mptest.session_id)) != MESA_RC_OK) {
        T_W("Open icli session failed!");
        return;
    }

    // Open debug mode
    icli_session_ctr_debug_mode(mptest.session_id, TRUE);

    switch(mptest.flag) {
        case MPTEST_STATE_T2_ERR:
            mptest_lightup_t21_led();
            mptest_packet_testing(mptest.session_id, MPTEST_PKT_STATE_STOP, false);
            break;
        case MPTEST_STATE_T2:
            mptest_lightup_start_led();

            mptest.log->clear();

            mptest_packet_set_tx_cnt(1);
            mptest_packet_testing(mptest.session_id, MPTEST_PKT_STATE_START, false);
            mptest_packet_testing(mptest.session_id, MPTEST_PKT_STATE_INIT, false);

            if (mptest_packet_link_wait(mptest.session_id, true) != MESA_RC_OK) {
                err_cnt++;
            }

            if (mptest_packet_testing(mptest.session_id, MPTEST_PKT_STATE_CHECK_SPEED, true) != MESA_RC_OK) {
                err_cnt++;
            }
            mptest_packet_testing(mptest.session_id, MPTEST_PKT_STATE_SEND, false);
            break;
        default:
            T_D("MP flag is invaild!! flag: %d", mptest.flag);
            goto exit;
            break;
    }

    while(times >= 0) {
        if(mptest.flag == MPTEST_STATE_T2_ERR) {
            mptest.log->dump(mptest.session_id, mptest_icli_printf_callback);
            VTSS_OS_MSLEEP(10000);
            continue;
        }

        // MPTEST_STATE_T2
        /* RAM */
        if(times % MPTEST_DDR_TEST_CYCLE == 0) {
            if(mptest_ddr_testing(mptest.session_id, MPTEST_DDR_TEST_SIZE, true) != MESA_RC_OK) {
                err_cnt++;
            }
        }

        /* NOR Flash */
        if(times % MPTEST_NOR_FLASH_TEST_CYCLE == 0) {
            if(mptest_flash_testing(mptest.session_id, MPTEST_FLASH_TYPE_NOR, true) != MESA_RC_OK) {
                err_cnt++;
            }
        }

        /* NAND Flash */
        if(times % MPTEST_NAND_FLASH_TEST_CYCLE == 0) {
            if(mptest_flash_testing(mptest.session_id, MPTEST_FLASH_TYPE_NAND, true) != MPTEST_ERROE_NONE) {
                err_cnt++;
            }
        }

        /* Packet */
        if(times % MPTEST_PACKET_TEST_CYCLE == 0 && times != MPTEST_MAX_BURNIN_TIMES) {
            if (mptest_packet_testing(mptest.session_id, MPTEST_PKT_STATE_CHECK_CRC, true) != MESA_RC_OK) {
                err_cnt++;
            }

            mptest_packet_testing(mptest.session_id, MPTEST_PKT_STATE_RESTART, false);
            if (mptest_packet_link_wait(mptest.session_id, true) != MESA_RC_OK) {
                err_cnt++;
            }
            if (mptest_packet_testing(mptest.session_id, MPTEST_PKT_STATE_CHECK_SPEED, true) != MESA_RC_OK) {
                err_cnt++;
            }

            if (mptest_packet_testing(mptest.session_id, MPTEST_PKT_STATE_CHECK_TXRX, true) != MESA_RC_OK) {
                err_cnt++;
            }

            if (mptest_packet_testing(mptest.session_id, MPTEST_PKT_STATE_CHECK_COUNT, true) != MESA_RC_OK) {
                err_cnt++;
            }

            if (times != 0) {
                mptest_packet_testing(mptest.session_id, MPTEST_PKT_STATE_INIT, false);
                mptest_packet_testing(mptest.session_id, MPTEST_PKT_STATE_SEND, false);
            }
        }

        VTSS_OS_MSLEEP(1000);

        (void) icli_session_printf(mptest.session_id, "Time[%d]\n", times);
        times--;
    }

    if(err_cnt == 0) {
        icli_session_cmd_exec(mptest.session_id, "debug board mpflag 3", TRUE);
        (void) icli_session_printf(mptest.session_id, "T2 Finished");
        mptest_lightup_ok_led();
    }

exit:
    icli_session_close(mptest.session_id);
}

/**************************************************************************************************
 * Main
 *************************************************************************************************/
extern "C" int mptest_icli_cmd_register();
mesa_rc mptest_init(vtss_init_data_t *data)
{
    switch (data->cmd) {
        case INIT_CMD_EARLY_INIT:
            VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, VTSS_TRACE_GRP_CNT);
            VTSS_TRACE_REGISTER(&trace_reg);
            break;
        case INIT_CMD_INIT:
            memset(&mptest, 0, sizeof(mptest_global_t));
            mptest.log = new mptest_log();
            mptest_icli_cmd_register();
            break;
        case INIT_CMD_START:
            vtss_thread_create(VTSS_THREAD_PRIO_BELOW_NORMAL, mptest_thread, 0,
                "MP Test", nullptr, 0, &mptest.thread_handle, &mptest.thread_block);
            break;
        case INIT_CMD_CONF_DEF:
        case INIT_CMD_MASTER_UP:
        case INIT_CMD_MASTER_DOWN:
        case INIT_CMD_SWITCH_ADD:
        case INIT_CMD_SWITCH_DEL:
        default:
            /*  Do nothing...   */
            break;
    }

    return MESA_RC_OK;
}
