/*************************************************************************************************
 * Copyright (C) 2018 Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: MPTEST log header
 *
 *************************************************************************************************/
#ifndef _MPTEST_LOG_H_
#define _MPTEST_LOG_H_

#define MPTEST_LOG_FLASH_PAGE_TOTAL_SIZE    (2048)  // 2KB
#define MPTEST_LOG_FLASH_PAGE_HEADER_SIZE   (32)
#define MPTEST_LOG_ENTRY_MAX_SIZE           (32)
#define MPTEST_LOG_TOTAL_ENTRY_COUNT        ((MPTEST_LOG_FLASH_PAGE_TOTAL_SIZE - MPTEST_LOG_FLASH_PAGE_HEADER_SIZE) / MPTEST_LOG_ENTRY_MAX_SIZE)
#define MPTEST_LOG_MAX_CONTENT_LEN          (MPTEST_LOG_ENTRY_MAX_SIZE - sizeof(int) - sizeof(unsigned int))

typedef struct {
    char    magic[MPTEST_LOG_FLASH_PAGE_HEADER_SIZE - sizeof(off_t)];
    off_t   offset;  // Save log offset
} mptest_log_header_t;

typedef struct {
    int             time;
    unsigned int    content_len;
    char            content[MPTEST_LOG_MAX_CONTENT_LEN];
} mptest_log_entry_t;

class mptest_log {
    public:
        mptest_log();
        mesa_rc logging(const char *fmt, ...);
        mesa_rc clear(void);
        void dump(u32 session_id, void (*_pr)(u32 session_id, const char *fmt, ...));
        off_t get_log_offset(void);
    private:
        mptest_log_header_t _log_header;
        mptest_log_entry_t  _log_entry[MPTEST_LOG_TOTAL_ENTRY_COUNT];
        FILE *_fp;
        unsigned int _total;
};

#endif  /* _MPTEST_LOG_H_ */
