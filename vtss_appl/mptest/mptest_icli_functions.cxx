/*************************************************************************************************
 * Copyright (C) 2018 Leonton Technologies, Inc - All Rights Reserved
 *
 * Abstract: MPTEST icli functions
 *
 *************************************************************************************************/
#include "icli_api.h"
#include "icli_porting_util.h"
#include "led_api.h"
#include "misc_api.h"
#include "mptest_api.h"
#include "mptest_icli_functions.h"
#include "port_api.h"       // For board_instance
#include "firmware.h"
#include "vtss/appl/poe.h"  // For vtss_appl_poe_port_conf_get
#include "usb_api.h"        // For usb_file_write
#include "poe_custom_ltc4271_api.h" // For ltc4271_pse_interrupt_enable
#include "mgmt_api.h" //mgmt_long2str_float

#define MPTEST_SIZE_KB              (1024)
#define MPTEST_TESTING_PATTERN      (0x5AA5A55A)
#define MPTEST_USB_BLOCK_NUM        (1024)
#define MPTEST_USB_FILENAME         "USB_TEST.txt"

mesa_rc mptest_icli_pktloop_test(u32 session_id, uint sec)
{
    i32 rc = MESA_RC_OK;

    // average packet count
    mptest_packet_set_time(sec);

    mptest_packet_testing(session_id, MPTEST_PKT_STATE_INIT, false);
    mptest_packet_testing(session_id, MPTEST_PKT_STATE_RESTART, false);

    if (mptest_packet_link_wait(session_id, false) != MESA_RC_OK) {
        rc = MESA_RC_ERROR;
    }
    if (mptest_packet_testing(session_id, MPTEST_PKT_STATE_CHECK_SPEED, false) != MESA_RC_OK) {
        rc = MESA_RC_ERROR;
    }
    mptest_packet_testing(session_id, MPTEST_PKT_STATE_SEND, false);
    VTSS_OS_MSLEEP(sec * 1000);
    if (mptest_packet_testing(session_id, MPTEST_PKT_STATE_CHECK_CRC, false) != MESA_RC_OK) {
        rc = MESA_RC_ERROR;
    }
    mptest_packet_testing(session_id, MPTEST_PKT_STATE_RESTART, false);
    VTSS_OS_MSLEEP(3000);
    if (mptest_packet_testing(session_id, MPTEST_PKT_STATE_CHECK_TXRX, false) != MESA_RC_OK) {
        rc = MESA_RC_ERROR;
    }
    if (mptest_packet_testing(session_id, MPTEST_PKT_STATE_CHECK_COUNT, false) != MESA_RC_OK) {
        rc = MESA_RC_ERROR;
    }

    // average packet count
    mptest_packet_set_time(0);

    return rc;
}

mesa_rc mptest_icli_sysled_color_set(meba_led_type_t type, meba_led_mode_t mode, meba_led_color_t color)
{
    if(color != MEBA_LED_COLOR_COUNT)
        lntn_sysled_set_color(type, color);

    if(mode != MEBA_LED_MODE_BLINK_MAX)
        lntn_sysled_set_mode(type, mode);

    return MESA_RC_OK;
}

mesa_rc mptest_icli_sysled_color_get(meba_led_type_t type, meba_led_color_t *color)
{
    if (lntn_sysled_get_color(type, color) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }
    return MESA_RC_OK;
}

mesa_rc mptest_icli_portled_color_set(mesa_port_no_t port_no, meba_led_color_t color)
{
    meba_mptest_port_led_set(board_instance, port_no, color);
    return MESA_RC_OK;
}

mesa_rc mptest_icli_sfp_status_get(mesa_port_no_t port_no, mptest_sfp_op_t op, bool *data)
{
    lntn_global_conf_t  lntn_conf = lntn_conf_get();
    meba_sfp_status_t   sfp_status;

    if (!port_conf_sfp_check((&lntn_conf.port_conf[port_no]))) {
        return MESA_RC_ERROR;
    }

    if (meba_sfp_status_get(board_instance, port_no, &sfp_status) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }

    switch (op) {
        case MPTEST_SFP_OP_PRESENT:
            *data = sfp_status.present;
            break;
        case MPTEST_SFP_OP_LOS:
            *data = sfp_status.los;
            break;
        case MPTEST_SFP_OP_TX_FAULT:
            *data = sfp_status.tx_fault;
            break;
        default:
            break;
    }
    return MESA_RC_OK;
}

mesa_rc mptest_icli_sfp_msa_data_get(u32 session_id, mesa_port_no_t port_no, char offset, unsigned char *buf, int buf_len)
{
    return meba_sfp_i2c_xfer(board_instance, port_no, false, SFP_I2C_ADDRESS, offset, buf, buf_len, FALSE);
}

mesa_rc mptest_icli_sfp_status_check(u32 session_id, mesa_port_no_t port_no, mptest_sfp_op_t op, bool val)
{
    lntn_global_conf_t  lntn_conf = lntn_conf_get();
    meba_sfp_status_t   sfp_status;
    int                 error = 0;

    if (!port_conf_sfp_check((&lntn_conf.port_conf[port_no]))) {
        return MESA_RC_ERROR;
    }

    if (meba_sfp_status_get(board_instance, port_no, &sfp_status) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }

    switch (op) {
        case MPTEST_SFP_OP_PRESENT:
            if (sfp_status.present != val) {
                ICLI_PRINTF("Port %d present[%d]\n", port_no, sfp_status.present);
                error++;
            }
            break;
        case MPTEST_SFP_OP_LOS:
            if (sfp_status.los != val) {
                ICLI_PRINTF("Port %d rxlos[%d]\n", port_no, sfp_status.los);
                error++;
            }
            break;
        case MPTEST_SFP_OP_TX_FAULT:
            if (sfp_status.tx_fault != val) {
                ICLI_PRINTF("Port %d txfault[%d]\n", port_no, sfp_status.tx_fault);
                error++;
            }
            break;
        default:
            break;
    }

    return error ? MESA_RC_ERROR : MESA_RC_OK;
}

mesa_rc mptest_icli_poe_port_conf_get(mesa_port_no_t port_no, vtss_appl_poe_port_conf_t *conf)
{
    lntn_global_conf_t  lntn_conf = lntn_conf_get();
    vtss_ifindex_t      ifindex;

    if (!port_conf_poe_check((&lntn_conf.port_conf[port_no]))) {
        return MESA_RC_ERROR;
    }

    vtss_ifindex_from_port(1, port_no, &ifindex);
    if (vtss_appl_poe_port_conf_get(ifindex, conf) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }
    return MESA_RC_OK;
}

mesa_rc mptest_icli_poe_port_conf_set(mesa_port_no_t port_no, vtss_appl_poe_port_conf_t *conf)
{
    lntn_global_conf_t  lntn_conf = lntn_conf_get();
    vtss_ifindex_t      ifindex;

    if (!port_conf_poe_check((&lntn_conf.port_conf[port_no]))) {
        return MESA_RC_ERROR;
    }

    vtss_ifindex_from_port(1, port_no, &ifindex);
    if (vtss_appl_poe_port_conf_set(ifindex, conf) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }
    return MESA_RC_OK;
}

mesa_rc mptest_icli_poe_global_conf_get(vtss_appl_poe_global_conf_t *conf)
{
    if (vtss_appl_poe_global_conf_get(conf) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }
    return MESA_RC_OK;
}

mesa_rc mptest_icli_poe_global_conf_set(vtss_appl_poe_global_conf_t *conf)
{
    if (vtss_appl_poe_global_conf_set(conf) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }
    return MESA_RC_OK;
}

mesa_rc mptest_icli_poe_global_supply_get(int *supply)
{
    vtss_appl_poe_conf_t conf;

    memset(&conf, 0, sizeof(vtss_appl_poe_conf_t));
    if (vtss_appl_poe_conf_get(VTSS_USID_START, &conf) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }

    *supply = conf.primary_power_supply;
    return MESA_RC_OK;
}

mesa_rc mptest_icli_poe_global_supply_set(int supply)
{
    vtss_appl_poe_conf_t conf;

    memset(&conf, 0, sizeof(vtss_appl_poe_conf_t));

    if (vtss_appl_poe_conf_get(VTSS_USID_START, &conf) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }
    conf.primary_power_supply = supply;

    if (vtss_appl_poe_conf_set(VTSS_USID_START, &conf) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }
    return MESA_RC_OK;
}

mesa_rc mptest_icli_poe_status_get(mesa_port_no_t port_no, vtss_appl_poe_port_status_t *status)
{
    lntn_global_conf_t  lntn_conf = lntn_conf_get();
    vtss_ifindex_t      ifindex;

    if (!port_conf_poe_check((&lntn_conf.port_conf[port_no]))) {
        return MESA_RC_ERROR;
    }

    vtss_ifindex_from_port(1, port_no, &ifindex);

    if (vtss_appl_poe_port_status_get(ifindex, status) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }
    return MESA_RC_OK;
}

mesa_rc mptest_icli_poe_status_check(u32 session_id, mesa_port_no_t start_port, mesa_port_no_t end_port, unsigned int status, int sec)
{
    CapArray<vtss_appl_poe_port_status_t, MESA_CAP_PORT_CNT> g_status;
    CapArray<int, MESA_CAP_PORT_CNT> got;
    vtss_ifindex_t      ifindex;
    int                 error = 0;
    int                 time = sec * 1000;
    int                 done_cnt = 0;
    int                 port_no;
    int                 port_cnt = end_port - start_port + 1;
    lntn_global_conf_t  lntn_conf = lntn_conf_get();
    lntn_port_conf_t    *port_conf;

    while (time) {
        port_conf_for_each((&lntn_conf), port_conf, port_no) {
            if (port_no < start_port || port_no > end_port) {
                continue;
            }

            if (!port_conf_poe_check(port_conf)) {
                continue;
            }

            if (got[port_no]) {
                continue;
            }

            vtss_ifindex_from_port(1, port_no, &ifindex);

            if (vtss_appl_poe_port_status_get(ifindex, &g_status[port_no]) != MESA_RC_OK) {
                return MESA_RC_ERROR;
            }

            if (g_status[port_no].port_status == status) {
                got[port_no] = 1;
                done_cnt ++;
            }
        }
        if (done_cnt == port_cnt) {
            break;
        }
        VTSS_OS_MSLEEP(250);
        time -= 250;
    }

    port_conf_for_each((&lntn_conf), port_conf, port_no) {
        if (port_no < start_port || port_no > end_port) {
            continue;
        }

        if (!port_conf_poe_check(port_conf)) {
            ICLI_PRINTF("[PoE]: Port%d PoE is not support\n", port_no);
            error++;
            continue;
        }

        if (!got[port_no]) {
            ICLI_PRINTF("[PoE]: Port%d status[%d]\n", port_no, g_status[port_no].port_status);
            error++;
        }
    }

    return error ? MESA_RC_ERROR : MESA_RC_OK;
}


mesa_rc mptest_icli_poe_status_check_all(u32 session_id, unsigned int status, int sec)
{
    CapArray<vtss_appl_poe_port_status_t, MESA_CAP_PORT_CNT> g_status;
    CapArray<int, MESA_CAP_PORT_CNT> got;
    vtss_ifindex_t      ifindex;
    int                 error = 0;
    int                 time = sec * 1000;
    int                 done_cnt = 0;
    int                 port_no;
    lntn_global_conf_t  lntn_conf = lntn_conf_get();
    lntn_port_conf_t    *port_conf;

    while (time) {
        port_conf_for_each((&lntn_conf), port_conf, port_no) {
            if (!port_conf_poe_check(port_conf)) {
                continue;
            }

            if (got[port_no]) {
                continue;
            }

            vtss_ifindex_from_port(1, port_no, &ifindex);

            if (vtss_appl_poe_port_status_get(ifindex, &g_status[port_no]) != MESA_RC_OK) {
                return MESA_RC_ERROR;
            }

            if (g_status[port_no].port_status == status) {
                got[port_no] = 1;
                done_cnt ++;
            }
        }
        if (done_cnt == lntn_conf_poe_count((&lntn_conf))) {
            break;
        }
        VTSS_OS_MSLEEP(250);
        time -= 250;
    }

    port_conf_for_each((&lntn_conf), port_conf, port_no) {
        if (!port_conf_poe_check(port_conf)) {
            continue;
        }

        if (!got[port_no]) {
            ICLI_PRINTF("[PoE]: Port%d status[%d]\n", port_no, g_status[port_no].port_status);
            error++;
        }
    }

    return error ? MESA_RC_ERROR : MESA_RC_OK;
}

mesa_rc mptest_icli_poe_intpse_check(u32 session_id, BOOL val)
{
    mesa_rc rc = MESA_RC_OK;
    BOOL    get_val;

    mesa_gpio_direction_set(NULL, 0, MEBA_GPIO_I_INT_PSE, false);
    if (mesa_gpio_read(NULL, 0, MEBA_GPIO_I_INT_PSE, &get_val) != MESA_RC_OK) {
        ICLI_PRINTF("PSE interrupt get value error\n");
        rc = MESA_RC_ERROR;
    } else {
        if (get_val != val) {
            rc = MESA_RC_ERROR;
        }
    }

    return rc;
}
mesa_rc mptest_icli_poe_power_check(u32 session_id, int power_val, mesa_port_no_t start_port, mesa_port_no_t end_port)
{
    CapArray<vtss_appl_poe_port_status_t, MESA_CAP_PORT_CNT> g_status;
    CapArray<int, MESA_CAP_PORT_CNT> check_result;
    vtss_ifindex_t      ifindex;
    int                 error = 0;
    int                 port_no;
    lntn_global_conf_t  lntn_conf = lntn_conf_get();
    lntn_port_conf_t    *port_conf;
    char txt_string[50];

    for (port_no = start_port, port_conf = &lntn_conf.port_conf[start_port];
     port_no <= end_port; port_conf = &lntn_conf.port_conf[++port_no]){
        if (!port_conf_poe_check(port_conf)) {
            continue;
        }
        vtss_ifindex_from_port(1, port_no, &ifindex);

        if (vtss_appl_poe_port_status_get(ifindex, &g_status[port_no]) != MESA_RC_OK) {
            return MESA_RC_ERROR;
        }
        if (g_status[port_no].power_consume >= power_val) {
            check_result[port_no]=1;
        }
        VTSS_OS_MSLEEP(100);
    }

    for (port_no = start_port, port_conf = &lntn_conf.port_conf[start_port];
         port_no <= end_port; port_conf = &lntn_conf.port_conf[++port_no]) {
        if (!port_conf_poe_check(port_conf)) {
            continue;
        }

        if (!check_result[port_no]) {
            mgmt_long2str_float(&txt_string[0],g_status[port_no].power_consume, 1);
            ICLI_PRINTF("[PoE]: Port%d power[%s]\n", port_no, txt_string);
            error++;
        }
    }

    return error ? MESA_RC_ERROR : MESA_RC_OK;

}

void mptest_icli_poe_intpse_enable(u32 session_id, mesa_port_no_t port_index, BOOL enable)
{
    ltc4271_pse_interrupt_enable(port_index, enable);
}

void mptest_icli_ddr_test(u32 session_id, unsigned int size_mb_cnt)
{
    (void) mptest_ddr_testing(session_id, size_mb_cnt, false);
}

void mptest_icli_flash_test(u32 session_id, mptest_flash_type_t type)
{
    (void) mptest_flash_testing(session_id, type, false);
}

mesa_rc mptest_icli_nand_bad_check(u32 session_id, unsigned int badblock_cnt)
{
    unsigned int get_bad = 0;

    if (mptest_nand_bad_scan(&get_bad) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }

    if (get_bad > badblock_cnt) {
        ICLI_PRINTF("[NAND]: Bad block count is %u\n", get_bad);
        return MESA_RC_ERROR;
    }

    return MESA_RC_OK;
}

mesa_rc mptest_icli_nand_size_check(u32 session_id, unsigned int nand_size)
{
    vtss_mtd_t  mtd;

    if (vtss_mtd_open(&mtd, "rootfs_data") != MESA_RC_OK) {
        ICLI_PRINTF("FAILED: vtss_mtd_open\n");
        return MESA_RC_ERROR;
    }

    if ((mtd.info.size/(1024 * 1024)) != nand_size) {
        ICLI_PRINTF("[NAND]: Size %u MB\n", mtd.info.size/(1024 * 1024));
        return MESA_RC_ERROR;
    }

    vtss_mtd_close(&mtd);
    return MESA_RC_OK;
}

mesa_rc mptest_icli_log_read(u32 session_id)
{
    mptest_log_read(session_id);
    return MESA_RC_OK;
}

mesa_rc mptest_icli_log_write(void *msg, unsigned int len)
{
    return mptest_log_write(msg, len);
}

mesa_rc mptest_icli_log_clear(void)
{
    return mptest_log_clear();
}

mesa_rc mptest_icli_usb_rw(u32 session_id, int size_kb_cnt)
{
    int             i = 0;
    char            pattern_blk[MPTEST_SIZE_KB];
    unsigned char   *rfilebuf = NULL;
    unsigned char   *wfilebuf = NULL;
    unsigned int    rfilesize;
    mesa_rc         rc = MESA_RC_OK;
    usb_rc_t        usb_rc;
    int             ptn = MPTEST_TESTING_PATTERN;

    if (size_kb_cnt <= 0 || size_kb_cnt > MPTEST_USB_BLOCK_NUM) {
        ICLI_PRINTF("Testing range is 1~1024(KB)\n");
        return MESA_RC_ERROR;
    }

    for (i = 0; i < MPTEST_SIZE_KB; i += 4) {
        memcpy(pattern_blk + i, &ptn, sizeof(int));
    }

    // create size_kb_cnt * KB pattern
    if (!(wfilebuf = (unsigned char *)malloc(MPTEST_SIZE_KB * size_kb_cnt))) {
        ICLI_PRINTF("malloc error.\n");
        rc = MESA_RC_ERROR;
        goto usb_rw_exit;
    }
    memset(wfilebuf, 0, MPTEST_SIZE_KB * size_kb_cnt);
    for (i = 0; i < size_kb_cnt; i++) {
        memcpy(wfilebuf + (i*MPTEST_SIZE_KB), pattern_blk, MPTEST_SIZE_KB);
    }

    // Write file to USB
    if ((usb_rc = usb_file_write((unsigned char*)MPTEST_USB_FILENAME,
                             strlen(MPTEST_USB_FILENAME) + 1,
                             wfilebuf,
                             (size_kb_cnt * MPTEST_SIZE_KB))) != USB_RC_OK) {
        ICLI_PRINTF("%s\n", usb_error_txt(usb_rc));
        rc = MESA_RC_ERROR;
        goto usb_rw_exit;
    }

    // Read file from USB
    if ((usb_rc = usb_file_read((unsigned char*)MPTEST_USB_FILENAME,
                            strlen(MPTEST_USB_FILENAME) + 1,
                            &rfilebuf,
                            &rfilesize)) != USB_RC_OK) {
        ICLI_PRINTF("%s\n", usb_error_txt(usb_rc));
        rc = MESA_RC_ERROR;
        goto usb_rw_exit;
    }

    // Check file size
    ICLI_PRINTF("rfilesize[%d]\n", rfilesize);
    if (rfilesize != (size_kb_cnt * MPTEST_SIZE_KB)) {
        ICLI_PRINTF("read len is too short[%d]\n", rfilesize);
        rc = MESA_RC_ERROR;
        goto usb_rw_exit;
    }

    // Compare file
    if (memcmp(rfilebuf, wfilebuf, rfilesize) != 0) {
        ICLI_PRINTF("memcmp error.\n");
        rc = MESA_RC_ERROR;
    }

usb_rw_exit:
    if (rfilebuf) {
        free(rfilebuf);
    }
    if (wfilebuf) {
        free(wfilebuf);
    }

    return rc;
}

mesa_rc mptest_icli_relay_set(BOOL val)
{
    mesa_gpio_direction_set(NULL, 0, MEBA_GPIO_O_RELAY, true);
    return mesa_gpio_write(NULL, 0, MEBA_GPIO_O_RELAY, val);
}

mesa_rc mptest_icli_rstbtn_get(BOOL *val)
{
    mesa_gpio_direction_set(NULL, 0, MEBA_GPIO_I_PUSH_BUTTON, false);
    return mesa_gpio_read(NULL, 0, MEBA_GPIO_I_PUSH_BUTTON, val);
}

mesa_rc mptest_icli_rstbtn_check(u32 session_id, BOOL val)
{
    mesa_rc rc = MESA_RC_OK;
    BOOL    get_val;

    if (mptest_icli_rstbtn_get(&get_val) != MESA_RC_OK) {
        ICLI_PRINTF("Reset bitton get value error\n");
        rc = MESA_RC_ERROR;
    } else {
        if (get_val != val) {
            rc = MESA_RC_ERROR;
        }
    }

    return rc;
}

mesa_rc mptest_icli_firmware_info_get(vtss_appl_firmware_status_image_type_t type, vtss_appl_firmware_status_image_t *imageEntry)
{
    if (vtss_appl_firmware_status_image_entry_get(type, imageEntry) != MESA_RC_OK) {
        return MESA_RC_ERROR;
    }
    return MESA_RC_OK;
}

mesa_rc mptest_icli_power_test(u32 session_id, unsigned int sec)
{
    meba_led_color_t    p1_color;
    meba_led_color_t    p2_color;
    bool                stage_done[5];
    int                 i;

    memset(stage_done, 0, sizeof(stage_done));
    for (i=sec*1000; i>=0; i-=250) {
        p1_color = MEBA_LED_COLOR_OFF;
        p2_color = MEBA_LED_COLOR_OFF;
        if (mptest_icli_sysled_color_get(MEBA_LED_TYPE_POWER1, &p1_color) != MESA_RC_OK ||
            mptest_icli_sysled_color_get(MEBA_LED_TYPE_POWER2, &p2_color) != MESA_RC_OK) {
            return MESA_RC_ERROR;
        }

        if (!stage_done[0] && p1_color == MEBA_LED_COLOR_GREEN && p2_color == MEBA_LED_COLOR_GREEN) {
            ICLI_PRINTF("stage_done[0]\n");
            stage_done[0] = true;
        } else if (stage_done[0] && !stage_done[1] && p1_color == MEBA_LED_COLOR_OFF && p2_color == MEBA_LED_COLOR_GREEN) {
            ICLI_PRINTF("stage_done[1]\n");
            stage_done[1] = true;
        } else if (stage_done[1] && !stage_done[2] && p1_color == MEBA_LED_COLOR_GREEN && p2_color == MEBA_LED_COLOR_GREEN) {
            ICLI_PRINTF("stage_done[2]\n");
            stage_done[2] = true;
        } else if (stage_done[0] && !stage_done[3] && p1_color == MEBA_LED_COLOR_GREEN && p2_color == MEBA_LED_COLOR_OFF) {
            ICLI_PRINTF("stage_done[3]\n");
            stage_done[3] = true;
        } else if (stage_done[3] && !stage_done[4] && p1_color == MEBA_LED_COLOR_GREEN && p2_color == MEBA_LED_COLOR_GREEN) {
            ICLI_PRINTF("stage_done[4]\n");
            stage_done[4] = true;
        }

        if (stage_done[0] && stage_done[1] && stage_done[2] && stage_done[3] && stage_done[4]) {
            return MESA_RC_OK;
        }

        VTSS_OS_MSLEEP(250);
    }
    if(!stage_done[1] || !stage_done[2]){
        ICLI_PRINTF("POWER 1 Fail\n");
    }
    if(!stage_done[3] || !stage_done[4]){
        ICLI_PRINTF("POWER 2 Fail\n");
    }
    return MESA_RC_ERROR;
}

mesa_rc mptest_icli_power_and_relay_test(u32 session_id, unsigned int sec)
{
    meba_led_color_t    p1_color;
    meba_led_color_t    p2_color;
    bool                stage_done[5];
    int                 detect_cnt = 0;
    int                 i;

    memset(stage_done, 0, sizeof(stage_done));
    if (mptest_icli_relay_set(0) != MESA_RC_OK) {    //relay close
        ICLI_PRINTF("Relay set[close] fail\n");
        return MESA_RC_ERROR;
    }

    for (i=sec*1000; i>=0; i-=250) {
        detect_cnt++;
        p1_color = MEBA_LED_COLOR_OFF;
        p2_color = MEBA_LED_COLOR_OFF;
        if (mptest_icli_sysled_color_get(MEBA_LED_TYPE_POWER1, &p1_color) != MESA_RC_OK ||
            mptest_icli_sysled_color_get(MEBA_LED_TYPE_POWER2, &p2_color) != MESA_RC_OK) {
            return MESA_RC_ERROR;
        }

        if (!stage_done[0] && p1_color == MEBA_LED_COLOR_GREEN && p2_color == MEBA_LED_COLOR_GREEN) {
            ICLI_PRINTF("stage_done[0]\n");
            stage_done[0] = true;
        } else if (stage_done[0] && !stage_done[1] && p1_color == MEBA_LED_COLOR_OFF && p2_color == MEBA_LED_COLOR_GREEN) {
            ICLI_PRINTF("stage_done[1]\n");
            stage_done[1] = true;
        } else if (stage_done[1] && !stage_done[2] && p1_color == MEBA_LED_COLOR_GREEN && p2_color == MEBA_LED_COLOR_GREEN) {
            ICLI_PRINTF("stage_done[2]\n");
            stage_done[2] = true;
        } else if (stage_done[0] && !stage_done[3] && p1_color == MEBA_LED_COLOR_GREEN && p2_color == MEBA_LED_COLOR_OFF) {
            ICLI_PRINTF("stage_done[3]\n");
            stage_done[3] = true;
        } else if (stage_done[3] && !stage_done[4] && p1_color == MEBA_LED_COLOR_GREEN && p2_color == MEBA_LED_COLOR_GREEN) {
            ICLI_PRINTF("stage_done[4]\n");
            stage_done[4] = true;
        }

        if (stage_done[0] && stage_done[1] && stage_done[2] && stage_done[3] && stage_done[4]) {
            if (mptest_icli_relay_set(0) != MESA_RC_OK) {    //relay close
                ICLI_PRINTF("Relay set[close] fail\n");
                return MESA_RC_ERROR;
            }
            return MESA_RC_OK;
        }

        if ((detect_cnt % 8) == 4) {
            if (mptest_icli_relay_set(1) != MESA_RC_OK) {    //relay open
                ICLI_PRINTF("Relay set[open] fail\n");
                return MESA_RC_ERROR;
            }
        } else if ((detect_cnt % 8) == 0){
            if (mptest_icli_relay_set(0) != MESA_RC_OK) {    //relay close
                ICLI_PRINTF("Relay set[close] fail\n");
                return MESA_RC_ERROR;
            }
        }
        VTSS_OS_MSLEEP(250);
    }
    return MESA_RC_ERROR;
}

mesa_rc mptest_icli_rstbtn_test(unsigned int sec)
{
    int     i;
    BOOL    prev_val = true;
    BOOL    current_val = true;

    for (i=sec*1000; i>=0; i-=250) {
        if (mptest_icli_rstbtn_get(&current_val) != MESA_RC_OK) {
            return MESA_RC_ERROR;
        }

        if (!prev_val && current_val) {
            return MESA_RC_OK;
        }

        prev_val = current_val;
        VTSS_OS_MSLEEP(250);
    }

    return MESA_RC_ERROR;
}

void mptest_icli_firmware_upgrade_dual_set(bool dual_image)
{
    auto fw = manager.get();
    fw->lntn_upgrade_dual_flag_set(dual_image);
}

mesa_rc mptest_icli_firmware_upgrade(u32 session_id, const char *url)
{
    auto fw = manager.get();
    if (fw.ok()) {
        fw->init(nullptr, firmware_max_download);
        fw->attach_cli(cli_get_io_handle());
        if (fw->download(url) == MESA_RC_OK) {
            if (!fw->check()) {
                ICLI_PRINTF("Invalid image, need a MFI image\n");
                return MESA_RC_ERROR;
            }
            mesa_rc result;
            // This only returns if an error occurred
            if ((result = fw->update_mptest()) != MESA_RC_OK) {
                ICLI_PRINTF("Error: %s\n", error_txt(result));
                return MESA_RC_ERROR;
            }
        } else {
            return MESA_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Error: Firmware update already pending\n");
        return MESA_RC_ERROR;
    }

    ICLI_PRINTF("MPTEST Firmware update completed.\n");
    control_system_reset_sync(MESA_RESTART_COOL);
    return MESA_RC_OK;
}
