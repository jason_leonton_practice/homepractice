/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
/* vtss_ptp_sys_timer.c */

#include "vtss_ptp_sys_timer.h"
#include "vtss_ptp_api.h"

#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_PTP

static vtss_timer_head_t my_timers;

void vtss_ptp_timer_start(vtss_ptp_sys_timer_t *t, i32 period, bool repeat)
{
    VTSS_ASSERT(t != 0);
    if (t != 0) {
        T_DG(VTSS_TRACE_GRP_PTP_BASE_TIMER,"period %d, repeat %d", period, repeat);
        if (t->co == 0) {
            T_WG(VTSS_TRACE_GRP_PTP_BASE_TIMER,"try to start uninitialized timer %p, period %d, repeat %d", t, period, repeat);
            return;
        }
        if (t->head.prev == 0 && t->head.next == 0) {
            t->head.prev = my_timers.prev;
            t->head.next = &my_timers;
            my_timers.prev->next = &t->head;
            my_timers.prev = &t->head;
        } else {
            T_DG(VTSS_TRACE_GRP_PTP_BASE_TIMER,"restart active timer");
        }
        t->periodic = repeat;
        t->period = period;
        t->left = period;
    }
}

void vtss_ptp_timer_stop(vtss_ptp_sys_timer_t *t) {
    VTSS_ASSERT(t != 0);
    if (t != 0) {
        if (t->head.prev != 0 && t->head.next != 0) {
            /* remove from list */
            T_DG(VTSS_TRACE_GRP_PTP_BASE_TIMER,"period %d, repeat %d", t->left, t->periodic);
            t->head.prev->next = t->head.next;
            t->head.next->prev = t->head.prev;
            t->head.prev = 0;
            t->head.next = 0;
        } else {
            T_DG(VTSS_TRACE_GRP_PTP_BASE_TIMER,"timer not active");
        }
    }
}

void vtss_ptp_timer_tick(i32 my_time)
{
    vtss_timer_head_t *h;
    vtss_ptp_sys_timer_t *t;
    h = my_timers.next;
    T_NG(VTSS_TRACE_GRP_PTP_BASE_TIMER,"head: next %p, prev %p", h->next, h->prev);
    while (h != &my_timers && h != 0) {
        t = (vtss_ptp_sys_timer_t *)h;
        t->left -= my_time;
        if (t->left <= 0) {
            if (t->periodic) {
                t->left += t->period;
                T_IG(VTSS_TRACE_GRP_PTP_BASE_TIMER,"left %d", t->left);
            } else {
                vtss_ptp_timer_stop(t);
            }
            t->co(t, t->context);
        }
        h = h->next;
        T_DG(VTSS_TRACE_GRP_PTP_BASE_TIMER,"head: next %p, prev %p", h->next, h->prev);
    }
}

void vtss_ptp_timer_dump(void)
{
    vtss_timer_head_t *h;
    vtss_ptp_sys_timer_t *t;
    h = my_timers.next;
    T_WG(VTSS_TRACE_GRP_PTP_BASE_TIMER,"head: next %p, prev %p", h->next, h->prev);
    while (h != &my_timers) {
        t = (vtss_ptp_sys_timer_t *)h;
        T_WG(VTSS_TRACE_GRP_PTP_BASE_TIMER,"period %d, left %d periodic %d, co %p, context %p", t->period, t->left, t->periodic, t->co, t->context);
        h = h->next;
        T_WG(VTSS_TRACE_GRP_PTP_BASE_TIMER,"head: next %p, prev %p", h->next, h->prev);
    }
}

void vtss_init_ptp_timer(vtss_ptp_sys_timer_t *t, vtss_timer_callout co, void *context)
{
    VTSS_ASSERT(t != 0);
    VTSS_ASSERT(co != 0);
    if (t != 0) {
        T_DG(VTSS_TRACE_GRP_PTP_BASE_TIMER,"init timer %p, co: %p, contect: %p", t, co, context);
        if (t->head.prev != 0 && t->head.next != 0) {
            T_IG(VTSS_TRACE_GRP_PTP_BASE_TIMER,"init active timer %p, co: %p, contect: %p", t, co, context);
            vtss_ptp_timer_stop(t);
        }
        VTSS_ASSERT(t->head.prev == 0);
        VTSS_ASSERT(t->head.next == 0);
        t->head.prev = 0;
        t->head.next = 0;
        t->period = 0;
        t->left = 0;
        t->co = co;
        t->context = context;
        t->periodic = false;
    }
}

void vtss_ptp_timer_initialize(void)
{
    my_timers.next = &my_timers;
    my_timers.prev = &my_timers;
}
