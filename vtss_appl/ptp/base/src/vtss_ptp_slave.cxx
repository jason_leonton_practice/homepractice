/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "vtss_ptp_api.h"
#include "vtss_ptp_types.h"
#include "vtss_ptp_os.h"
#include "vtss_ptp_slave.h"
#include "vtss_ptp_sys_timer.h"
#include "vtss_ptp_pack_unpack.h"
#include "vtss_ptp_local_clock.h"
#include "vtss_ptp_packet_callout.h"
#include "vtss_tod_api.h"
#include "vtss_ptp_internal_types.h"
#include "vtss/appl/ptp.h"
#include "ptp_api.h"
#include "vtss_ptp_unicast.hxx"
#include "vtss_ptp_802_1as.hxx"
#include "port_api.h"
#include "misc_api.h"
#include "conf_api.h"
#include "vtss_ptp_slave.h"

#if defined(VTSS_SW_OPTION_SYNCE)
#include "synce_ptp_if.h"
#endif

#define RATE_TO_U32_CONVERT_FACTOR (1LL<<41)

/* state defined as random numbers for consistency check */
#define SLAVE_STATE_PTP_OBJCT_ACTIVE  0x47835672
#define SLAVE_STATE_PTP_OBJCT_PASSIVE 0x58370465

/*
 * Forward declarations
 */
static void vtss_ptp_slave_sync_timer(vtss_timer_handle_t timer, void *s);
static void vtss_ptp_slave_delay_req_event_transmitted(void *context, uint portnum, u32 tx_time);
static void vtss_ptp_settle_sync_timer(vtss_timer_handle_t timer, void *s);
static void transmit_delay_req(vtss_timer_handle_t timer, void *s);
static void vtss_ptp_slave_log_timeout(vtss_timer_handle_t timer, void *s);

/*
 * private functions
 */
static const char *state2name(u32 state) {
    switch (state) {
        case SLAVE_STATE_PTP_OBJCT_ACTIVE:         return "ACTIVE";
        case SLAVE_STATE_PTP_OBJCT_PASSIVE:        return "PASSIVE";
        default:                                   return "invalid state";
    }
}

const char *clock_state_to_string(vtss_slave_clock_state_t s)
{
    switch (s) {
        case VTSS_PTP_SLAVE_CLOCK_FREERUN:        return "FREERUN";
        case VTSS_PTP_SLAVE_CLOCK_F_SETTLING:     return "F_SETTLING";
        case VTSS_PTP_SLAVE_CLOCK_FREQ_LOCK_INIT: return "FREQ_LOCK_INIT";
        case VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKING:   return "FREQ_LOCKING";
        case VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKED:    return "FREQ_LOCKED";
        case VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKING:  return "PHASE_LOCKING";
        case VTSS_PTP_SLAVE_CLOCK_P_SETTLING:     return "P_SETTLING";
        case VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKED:   return "PHASE_LOCKED";
        case VTSS_PTP_SLAVE_CLOCK_RECOVERING:     return "RECOVERING";
        default:                                  return "UNKNOWN";
    }
}

/*
 * functions for slave statistics management
 */
void master_to_slave_delay_stati(ptp_slave_t *slave)
{
    if (slave->master_to_slave_delay > slave->statistics.master_to_slave_max) {
        slave->statistics.master_to_slave_max = slave->master_to_slave_delay;
    }
    if (slave->master_to_slave_delay < slave->statistics.master_to_slave_min) {
        slave->statistics.master_to_slave_min = slave->master_to_slave_delay;
    }
    slave->statistics.master_to_slave_mean += slave->master_to_slave_delay;
    slave->statistics.master_to_slave_mean_count++;
    slave->statistics.master_to_slave_cur = slave->master_to_slave_delay;
}

void slave_to_master_delay_stati(ptp_slave_t *slave)
{
    if (slave->slave_to_master_delay > slave->statistics.slave_to_master_max) {
        slave->statistics.slave_to_master_max = slave->slave_to_master_delay;
    }
    if (slave->slave_to_master_delay < slave->statistics.slave_to_master_min) {
        slave->statistics.slave_to_master_min = slave->slave_to_master_delay;
    }
    slave->statistics.slave_to_master_mean += slave->slave_to_master_delay;
    slave->statistics.slave_to_master_mean_count++;
    slave->statistics.slave_to_master_cur = slave->slave_to_master_delay;
    
}

static void slave_statistics_clear(ptp_slave_t *slave)
{
    slave->statistics.master_to_slave_max       = -VTSS_MAX_TIMEINTERVAL;
    slave->statistics.master_to_slave_min       =  VTSS_MAX_TIMEINTERVAL;
    slave->statistics.master_to_slave_mean      = 0LL;
    slave->statistics.master_to_slave_mean_count = 0;
    slave->statistics.master_to_slave_cur       = 0LL;
    slave->statistics.slave_to_master_max       = -VTSS_MAX_TIMEINTERVAL;
    slave->statistics.slave_to_master_min       =  VTSS_MAX_TIMEINTERVAL;
    slave->statistics.slave_to_master_mean      = 0LL;
    slave->statistics.slave_to_master_mean_count = 0;
    slave->statistics.slave_to_master_cur       = 0LL;
    slave->statistics.sync_pack_rx_cnt          = 0;
    slave->statistics.sync_pack_timeout_cnt     = 0;
    slave->statistics.delay_req_pack_tx_cnt     = 0;
    slave->statistics.sync_pack_seq_err_cnt     = 0;
    slave->statistics.follow_up_pack_loss_cnt   = 0;
    slave->statistics.delay_resp_pack_loss_cnt  = 0;
}

mesa_rc vtss_ptp_clock_slave_statistics_enable(ptp_clock_t *ptp, bool enable)
{
    mesa_rc rc = VTSS_RC_ERROR;
    if (ptp != NULL) {
        ptp_slave_t *slave = &ptp->ssm;
        if (slave != NULL) {
            slave->statistics.enable = enable;
            slave_statistics_clear(slave);
            rc = VTSS_RC_OK;
        }
    }
    return rc;
}

mesa_rc vtss_ptp_clock_slave_statistics_get(ptp_clock_t *ptp, vtss_ptp_slave_statistics_t *statistics, bool clear)
{
    mesa_rc rc = VTSS_RC_ERROR;
    if (ptp != NULL) {
        ptp_slave_t *slave = &ptp->ssm;
        if (slave != NULL) {
            *statistics = slave->statistics;
            if (clear) {
                slave_statistics_clear(slave);
            }
            rc = VTSS_RC_OK;
        }
    }
    return rc;
}

/*
 * create a PTP slaveclock instance:
 * clear data and allocate buffer for delayReq transmission
 */
void vtss_ptp_slave_create(ptp_slave_t *slave)
{
    vtss_clear(*slave);
    slave->delay_req_buf.size = vtss_1588_packet_tx_alloc(&slave->delay_req_buf.handle, &slave->delay_req_buf.frame, ENCAPSULATION_SIZE + PACKET_SIZE);
    T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"handle %p", slave->delay_req_buf.handle);
    vtss_init_ptp_timer(&slave->sync_timer, vtss_ptp_slave_sync_timer, slave);
    slave->clock_settle_time = 6;  /* 6 sec settle time after clock setting */
    slave->in_settling_period = false;
    vtss_init_ptp_timer(&slave->settle_timer, vtss_ptp_settle_sync_timer, slave);
    vtss_init_ptp_timer(&slave->delay_req_sys_timer, transmit_delay_req, slave);
    slave_statistics_clear(slave);
    slave->master_to_slave_delay = 0;
    slave->master_to_slave_delay_valid = false;    
    T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"timer");
    slave->ptsf_loss_of_announce = true;
    slave->ptsf_loss_of_sync = true;
    slave->ptsf_unusable = true;
    vtss_init_ptp_timer(&slave->log_timer, vtss_ptp_slave_log_timeout, slave);
    T_D("*** SLAVE CREATE ***");
}

/*
 * create a PTP slaveclock instance
 */
void vtss_ptp_slave_init(ptp_slave_t *slave, vtss_appl_ptp_protocol_adr_t *ptp_dest, ptp_tag_conf_t *tag_conf)
{
    mesa_timestamp_t originTimestamp = {0,0,0};
    MsgHeader header;
    vtss_appl_ptp_config_port_ds_t *slave_config;
    // T_E("*** vtss_ptp_slave_init ***");
    if (slave->slave_port) {
        bool port_uses_two_step;

        slave_config = slave->slave_port->port_config;
        slave->servo->delay_filter_reset(0);  /* clears one-way E2E delay filter */
        slave->clock->currentDS.meanPathDelay = 0;
        slave->last_delay_req_event_sequence_number = 0;
        slave->random_relay_request_timer = (slave->protocol == VTSS_APPL_PTP_PROTOCOL_ETHERNET || slave->protocol == VTSS_APPL_PTP_PROTOCOL_IP4MULTI || slave->protocol == VTSS_APPL_PTP_PROTOCOL_ETHERNET_MIXED || slave->protocol == VTSS_APPL_PTP_PROTOCOL_IP4MIXED) ? true : false;
        /* DelayReq timer is chosen to ensure delay request after the first Sync packet */
        slave->delay_req_timer = 1;
        slave->first_sync_packet = true;
        slave->ptsf_loss_of_announce = false;  // Note: At this point a master is already nominated and selected.
        slave->ptsf_loss_of_sync = true;
        slave->ptsf_unusable = true;
        T_D("*** SLAVE INIT ***");
#if defined(VTSS_SW_OPTION_SYNCE)
        vtss_ptp_ptsf_state_set(slave->localClockId);
#endif
        T_NG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"DelayReq initial timer = %d", slave->delay_req_timer);

        header.versionPTP = slave->versionNumber;
        header.messageType = PTP_MESSAGE_TYPE_DELAY_REQ;;
        header.transportSpecific = slave->clock->majorSdoId;
        header.messageLength = DELAY_REQ_PACKET_LENGTH;
        header.domainNumber = slave->domainNumber;
        header.reserved1 = MINOR_SDO_ID;
        header.flagField[0] = (slave->protocol == VTSS_APPL_PTP_PROTOCOL_IP4UNI || slave->protocol == VTSS_APPL_PTP_PROTOCOL_ETHERNET_MIXED || slave->protocol == VTSS_APPL_PTP_PROTOCOL_IP4MIXED) ? PTP_UNICAST_FLAG : 0;
        header.flagField[1] = 0;
        header.correctionField = 0LL;
        memcpy(&header.sourcePortIdentity, slave->portIdentity_p, sizeof(vtss_appl_ptp_port_identity));
        T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"create slave for port %d", tag_conf->port);
        header.sequenceId = slave->last_delay_req_event_sequence_number;
        header.controlField = PTP_DELAY_REQ_MESSAGE;
        header.logMessageInterval = 0x7f;

        slave->state = SLAVE_STATE_PTP_OBJCT_ACTIVE;
        T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"state %x", slave->state);
        slave->delay_req_buf.header_length = vtss_1588_pack_encap_header(slave->delay_req_buf.frame, NULL, ptp_dest, DELAY_REQ_PACKET_LENGTH, true, slave->localClockId);
        vtss_1588_tag_get(tag_conf, slave->localClockId, &slave->delay_req_buf.tag);
        slave->delay_req_buf.size = slave->delay_req_buf.header_length + DELAY_REQ_PACKET_LENGTH;
        vtss_ptp_pack_msg44(slave->delay_req_buf.frame + slave->delay_req_buf.header_length, &header, &originTimestamp);
        slave->delay_req_buf.hw_time = 0;

        T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"frame %p", slave->delay_req_buf.frame);
        slave->logSyncInterval = slave_config->logSyncInterval;
        slave->sy_to = (slave->logSyncInterval > -2) ? 3*PTP_LOG_TIMEOUT(slave->logSyncInterval) : PTP_LOG_TIMEOUT(0);
        vtss_ptp_timer_start(&slave->sync_timer, slave->sy_to, false);
        slave->logSyncInterval = slave_config->logSyncInterval;
        T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"time %d", slave->sy_to);
        if ((slave->clock->clock_init->cfg.deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND)) {
            if ((slave->twoStepFlag && !(slave_config->twoStepOverride == VTSS_APPL_PTP_TWO_STEP_OVERRIDE_FALSE)) ||
                    (slave_config->twoStepOverride == VTSS_APPL_PTP_TWO_STEP_OVERRIDE_TRUE)) {
                port_uses_two_step = true;
            } else {
                port_uses_two_step = false;
            }
        } else {
            port_uses_two_step = slave->twoStepFlag;
        }

        // On JR2 we always use 2-step method if clock_domain != 0
        if (port_uses_two_step || (slave->clock->clock_init->cfg.clock_domain != 0)) {
            slave->delay_req_buf.msg_type = VTSS_PTP_MSG_TYPE_2_STEP;
            slave->del_req_ts_context.cb_ts = vtss_ptp_slave_delay_req_event_transmitted;
            slave->del_req_ts_context.context = slave;
            slave->delay_req_buf.ts_done = &slave->del_req_ts_context;
        } else {
            slave->delay_req_buf.msg_type = VTSS_PTP_MSG_TYPE_CORR_FIELD; /* one-step sync packet using correction field update */
            slave->delay_req_buf.ts_done = NULL;
        }

        memcpy(&slave->mac, &ptp_dest->mac, sizeof(mesa_mac_addr_t)); /* used to refresh the mac header before each transmission */
        slave->parent_last_sync_sequence_number = 0;
        slave->wait_follow_up  = false;
        slave->statistics.follow_up_pack_loss_cnt = 0;      /* count number of lost followup messages */
        slave->sync_correctionField = 0LL;
        slave->delay_req_send_time.seconds = 0;
        slave->delay_req_send_time.nanoseconds = 0;
        slave->delay_req_receive_time.seconds = 0;
        slave->delay_req_receive_time.nanoseconds = 0;
        slave->sync_tx_time.seconds = 0; /* used for debugging */
        slave->sync_tx_time.nanoseconds = 0; /* used for debugging */
        slave->delay_resp_correction = 0LL;
        slave->wait_delay_resp = false;
        slave->state = SLAVE_STATE_PTP_OBJCT_ACTIVE;
        T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"Instance %d, Slave active", slave->localClockId);
        if (slave->clock_state == VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKED || slave->clock_state == VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKED) {
            slave->clock_state = VTSS_PTP_SLAVE_CLOCK_RECOVERING;
        } else if (slave->clock_state != VTSS_PTP_SLAVE_CLOCK_RECOVERING) {
            slave->clock_state = VTSS_PTP_SLAVE_CLOCK_FREERUN;
        }
        slave->old_clock_state = slave->clock_state;
        T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"Instance %d, clock_state %s. Reason: Slave initialized", slave->localClockId, clock_state_to_string(slave->clock_state));
    } else {
        slave->state = SLAVE_STATE_PTP_OBJCT_PASSIVE;
        T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"Instance %d, Slave passive", slave->localClockId);
        slave->servo->clock_servo_reset(SET_VCXO_FREQ);
    }
}

/*
 * delete a PTP slaveclock instance
 * free packet buffer(s)
 */
void vtss_ptp_slave_delete(ptp_slave_t *slave)
{
    if (slave->state == SLAVE_STATE_PTP_OBJCT_ACTIVE) {
        if (slave->delay_req_buf.handle) {
            T_WG(VTSS_TRACE_GRP_PTP_BASE_MASTER,"buf.handle %p", slave->delay_req_buf.handle);
            vtss_1588_packet_tx_free(&slave->delay_req_buf.handle);
        }

        vtss_ptp_timer_stop(&slave->sync_timer);
        slave->state = SLAVE_STATE_PTP_OBJCT_PASSIVE;
        T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"delete slave");

        slave->ptsf_loss_of_announce = true;
        slave->ptsf_loss_of_sync = true;
        slave->ptsf_unusable = true;
        T_D("*** SLAVE DELETE ***");
#if defined(VTSS_SW_OPTION_SYNCE)
        vtss_ptp_ptsf_state_set(slave->localClockId);
#endif
    } else {
        T_WG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"Slave not active");
    }
}


/*
 * PTP slaveclock sync timout
 */
/*lint -esym(459, vtss_ptp_slave_sync_timer) */
static void vtss_ptp_slave_sync_timer(vtss_timer_handle_t timer, void *s)
{
    ptp_slave_t *slave = (ptp_slave_t *)s;
    vtss_appl_ptp_config_port_ds_t *slave_config;
    vtss_ptp_servo_status_t servo_status;
    T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"state %s, localClockId %d", state2name(slave->state), slave->localClockId);
    if (slave->clock->clock_init->cfg.profile == VTSS_APPL_PTP_PROFILE_IEEE_802_1AS) {
        T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"Timeout is handled elswhere in the 802.1AS profile");
        return;
    }
    switch (slave->state) {
        case SLAVE_STATE_PTP_OBJCT_ACTIVE:
            ++slave->statistics.sync_pack_timeout_cnt;
            if (slave->slave_port != NULL) {
                slave_config = slave->slave_port->port_config;
                vtss_ptp_timer_start(timer, slave->sy_to, false);
                slave->servo->clock_servo_status(&servo_status);
                if ((slave->clock_state == VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKED || slave->clock_state == VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKED) && servo_status.holdover_ok) {
                    slave->clock_state = VTSS_PTP_SLAVE_CLOCK_RECOVERING;
                } else if (slave->clock_state != VTSS_PTP_SLAVE_CLOCK_RECOVERING){
                    slave->clock_state = VTSS_PTP_SLAVE_CLOCK_FREERUN;
                }
                slave->servo->clock_servo_reset(SET_VCXO_FREQ);
            } else {
                vtss_ptp_timer_start(timer, PTP_LOG_TIMEOUT(0), false);
                T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s. holdover_trace_count %d", clock_state_to_string (slave->clock_state), slave->g8275_holdover_trace_count);

            }
            (void)clock_class_update(slave);
            T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s. Reason: Sync timeout", clock_state_to_string (slave->clock_state));
            break;
        default:
            if (clock_class_update(slave)) {
                vtss_ptp_timer_start(timer, PTP_LOG_TIMEOUT(0), false);
            }
            T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"Sync timeout in a Passive slave");
            break;
    }
    slave->ptsf_loss_of_sync = true;
    T_D("*** SLAVE SYNC TIMER ***");
#if defined(VTSS_SW_OPTION_SYNCE)
    vtss_ptp_ptsf_state_set(slave->localClockId);
#endif
}


/*
 * 2-step delayReq packet event transmitted. Save tx time
 *
 */
/*lint -esym(459, vtss_ptp_slave_delay_req_event_transmitted) */
static void vtss_ptp_slave_delay_req_event_transmitted(void *context, uint portnum, u32 tx_time)
{
    ptp_slave_t *slave = (ptp_slave_t *)context;
    if (slave->state == SLAVE_STATE_PTP_OBJCT_ACTIVE) {
        vtss_local_clock_convert_to_time( tx_time, &slave->delay_req_send_time, slave->clock->localClockId);
        T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"timestamp event received context %p, portnum %u, tx_time %u", context, portnum, tx_time);
        if (!slave->wait_delay_resp) {
            ptp_delay_calc(*slave->clockBase, slave->clock->localClockId, slave->delay_req_send_time, slave->delay_req_receive_time, slave->delay_resp_correction, slave->logDelayReqInterval);
            T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"DelayResp received before delayreq timestamp registered");
        }

    } else {
        T_WG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"unexpected timestamp event");
    }
}

/*
 * Transmit delay_Req
 *
 */
/*lint -esym(459, transmit_delay_req) */
static void transmit_delay_req(vtss_timer_handle_t timer, void *s)
{
    ptp_slave_t *slave = (ptp_slave_t *)s;
    mesa_timestamp_t originTimestamp;
    mesa_timeinterval_t corr = 0;
    i32 delay_req_period;
    i8 shifts;
    
    T_RG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"transmit_delay_req");
    if (slave->slave_port != NULL) {
        if ((slave->two_way) && (slave->slave_port->port_config->delayMechanism == DELAY_MECH_E2E) &&
                ((slave->clock->clock_init->cfg.deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND) ||
                 (slave->clock->clock_init->cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY))) {
        
            if (slave->clock_state != VTSS_PTP_SLAVE_CLOCK_FREERUN && slave->clock_state != VTSS_PTP_SLAVE_CLOCK_F_SETTLING && slave->clock_state != VTSS_PTP_SLAVE_CLOCK_P_SETTLING) {
                ++slave->statistics.delay_req_pack_tx_cnt;
                ++slave->last_delay_req_event_sequence_number;
                /* these fields are filled in when the message is transmitted and the response is received */
                slave->delay_req_send_time.seconds = 0;
                slave->delay_req_send_time.nanoseconds = 0;
                slave->delay_req_receive_time.seconds = 0;
                slave->delay_req_receive_time.nanoseconds = 0;

                vtss_tod_pack16(slave->last_delay_req_event_sequence_number,slave->delay_req_buf.frame + slave->delay_req_buf.header_length + PTP_MESSAGE_SEQUENCE_ID_OFFSET);
                vtss_local_clock_time_get(&originTimestamp, slave->localClockId, &slave->delay_req_buf.hw_time);
                vtss_ptp_pack_timestamp(slave->delay_req_buf.frame + slave->delay_req_buf.header_length, &originTimestamp);
                if ((slave->clock->clock_init->cfg.deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND) || (slave->clock->clock_init->cfg.deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY)) {
                    if (((slave->delay_req_buf.msg_type == VTSS_PTP_MSG_TYPE_2_STEP) && !(slave->slave_port->port_config->twoStepOverride == VTSS_APPL_PTP_TWO_STEP_OVERRIDE_FALSE)) ||
                        (slave->slave_port->port_config->twoStepOverride == VTSS_APPL_PTP_TWO_STEP_OVERRIDE_TRUE))
                    {
                        if (slave->slave_port->wd.enable && (2 == slave->slave_port->wd.delay_pre)) slave->slave_port->wd.delay_pre = 0;
                    } else {
                        slave->delay_req_send_time = originTimestamp;
                        vtss_ptp_pack_correctionfield(slave->delay_req_buf.frame + slave->delay_req_buf.header_length, &corr);  /* clear the correctionField because the buffer is reused, and the SW may update the field */
                    }
                } else {
                    if (slave->delay_req_buf.msg_type == VTSS_PTP_MSG_TYPE_2_STEP) {
                        if (slave->slave_port->wd.enable && (2 == slave->slave_port->wd.delay_pre)) slave->slave_port->wd.delay_pre = 0;
                    } else {
                        slave->delay_req_send_time = originTimestamp;
                        vtss_ptp_pack_correctionfield(slave->delay_req_buf.frame + slave->delay_req_buf.header_length, &corr);  /* clear the correctionField because the buffer is reused, and the SW may update the field */
                    }
                }

                if (slave->protocol == VTSS_APPL_PTP_PROTOCOL_ETHERNET_MIXED) {
                    vtss_1588_pack_eth_header(slave->delay_req_buf.frame, slave->sender.mac);
                } else if (slave->protocol == VTSS_APPL_PTP_PROTOCOL_IP4MIXED) {
                    (void)vtss_1588_pack_encap_header(slave->delay_req_buf.frame, NULL, &slave->sender, DELAY_REQ_PACKET_LENGTH, true, slave->localClockId);
                } else {
                    vtss_1588_pack_eth_header(slave->delay_req_buf.frame, slave->mac);
                }

                if (slave->wait_delay_resp) {
                    ++slave->statistics.delay_resp_pack_loss_cnt;
                }

                T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"Sending Delay_Req message");
                slave->wait_delay_resp = true;

                if (!vtss_1588_tx_msg(slave->port_mask, &slave->delay_req_buf, slave->clock->localClockId)) {
                    T_WG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"Delay_Req message transmission failed");
                    slave->wait_delay_resp = false;
                }
            } else {
                T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"No transmission in settling period");
            }

            T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"Delay Req timer reset");
            if (slave->slave_port->portDS.status.logMinDelayReqInterval < slave->logSyncInterval)
                slave->slave_port->portDS.status.logMinDelayReqInterval = slave->logSyncInterval;
            if (slave->slave_port->portDS.status.logMinDelayReqInterval > slave->logSyncInterval+5)
                slave->slave_port->portDS.status.logMinDelayReqInterval = slave->logSyncInterval+5;
            T_NG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"logMinDelayReqInterval %d, logSyncInterval %d", slave->slave_port->portDS.status.logMinDelayReqInterval, slave->logSyncInterval);
            if (!slave->random_relay_request_timer) {
                shifts = slave->slave_port->portDS.status.logMinDelayReqInterval - slave->logSyncInterval;
                slave->delay_req_timer = vtss_ptp_get_rand(&slave->random_seed)%((1<<(shifts+1))-1) + 1;
                T_NG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"delay_req_timer = %d, shifts = %d", slave->delay_req_timer, shifts);
            } else {
                if (slave->slave_port->portDS.status.logMinDelayReqInterval <= -7) {
                    delay_req_period = 0;
                } else {
                    if (slave->clock->clock_init->cfg.profile == VTSS_APPL_PTP_PROFILE_G_8275_1) {
                        // G.8275.1 profile is a randomization: see G.8275.1 section 6.2.8 Message rates
                        delay_req_period = (7*(1<<(slave->slave_port->portDS.status.logMinDelayReqInterval+7)) +
                                           (6*(vtss_ptp_get_rand(&slave->random_seed)%((1<<(slave->slave_port->portDS.status.logMinDelayReqInterval+7))-1)))
                                           )/10 +1;
                    } else {
                        delay_req_period = vtss_ptp_get_rand(&slave->random_seed)%((1<<(slave->slave_port->portDS.status.logMinDelayReqInterval+8))-1);
                    }
                }
                vtss_ptp_timer_start(timer, delay_req_period, false);
                T_NG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"delay_req_period = %d", delay_req_period);
            }
        }
    }
}

static char *vtss_ptp_clock_identity_to_str(const vtss_appl_clock_identity& clk_id)
{
    static char buf[3 * CLOCK_IDENTITY_LENGTH];

    for (int n = 0; n < CLOCK_IDENTITY_LENGTH; n++) {
       sprintf(&buf[n * 3], "%02X", clk_id[n]);
       buf[n * 3 + 2] = ':';
    }
    buf[3 * CLOCK_IDENTITY_LENGTH - 1] = 0;

    return buf;
}

vtss_rc vtss_ptp_port_speed_to_txt(mesa_port_speed_t speed, char **speed_txt)
{
    switch (speed) {
        case MESA_SPEED_10M:    *speed_txt = "10M";   break;
        case MESA_SPEED_100M:   *speed_txt = "100M";  break;
        case MESA_SPEED_1G:     *speed_txt = "1G";    break;
        case MESA_SPEED_2500M:  *speed_txt = "2G5";   break;
        case MESA_SPEED_10G:    *speed_txt = "10G";   break;
        default:                return VTSS_RC_ERROR;
    }

    return VTSS_RC_OK;
}

ptp_port_calibration_s ptp_port_calibration;
bool calib_t_plane_enabled = false;
bool calib_p2p_enabled = false;
bool calib_port_enabled = false;
bool calib_initiate = false;
i32  calib_cable_latency = 0;
i32  calib_pps_offset = 0;

/*
 * Handle sync request from a master
 *
 */
bool vtss_ptp_slave_sync(CapArray<ptp_clock_t *, VTSS_APPL_CAP_PTP_CLOCK_CNT> &ptpClock, int clock_inst, ptp_tx_buffer_handle_t *tx_buf, MsgHeader *header, vtss_appl_ptp_protocol_adr_t *sender)
{
    ptp_slave_t *slave = &ptpClock[clock_inst]->ssm;    
    bool forwarded = false;
    u8 *buf = tx_buf->frame + tx_buf->header_length;
    mesa_timestamp_t  originTimestamp;
    char str[50];
    char str1[50];

    T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"received sync, current parent is %s,%d",
         ClockIdentityToString(slave->parent_portIdentity_p->clockIdentity, str), slave->parent_portIdentity_p->portNumber);
    T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"received sync, expected seqNo %d", slave->parent_last_sync_sequence_number);
    if (slave->state == SLAVE_STATE_PTP_OBJCT_ACTIVE && slave->slave_port != NULL &&
            !PortIdentitycmp(&header->sourcePortIdentity, slave->parent_portIdentity_p)) {
        ++slave->statistics.sync_pack_rx_cnt;
        if ( SEQUENCE_ID_CHECK(header->sequenceId, slave->parent_last_sync_sequence_number)) {
            if (header->domainNumber == slave->domainNumber) { /* if transparent clock only syntonize to a master with the default domain number*/
                slave->parent_last_sync_sequence_number = header->sequenceId;
                slave->ptsf_loss_of_sync = false;
                T_D("*** SLAVE SYNC ***");
#if defined(VTSS_SW_OPTION_SYNCE)
                vtss_ptp_ptsf_state_set(slave->localClockId);
#endif
//                *slave->record_update = true;
                vtss_ptp_unpack_timestamp(buf, &originTimestamp);
                T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"received sync, originTimestamp: %s corr %s", TimeStampToString(&originTimestamp,str), 
                    vtss_tod_TimeInterval_To_String(&header->correctionField, str1, '.'));
                if (header->logMessageInterval != slave->logSyncInterval && header->logMessageInterval != 0x7F) {
                    T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"message's sync interval is %d, but clock's is %d", header->logMessageInterval, slave->logSyncInterval);
                    /* spec recommends handling a sync interval discrepancy as a fault, but we accept the interval from the master */
                    slave->slave_port->portDS.status.syncIntervalError = true;
                } else {
                    slave->slave_port->portDS.status.syncIntervalError = false;
                }
                if (header->logMessageInterval != 0x7F) {
                    slave->logSyncInterval = header->logMessageInterval;
                } else {
                    /* in the case where master sends 0x7f (unicast), we use the configured value in the slave */
                    slave->logSyncInterval = slave->slave_port->port_config->logSyncInterval;
                }
                vtss_local_clock_convert_to_time(tx_buf->hw_time,&slave->sync_receive_time,slave->localClockId);
                if (!getFlag(header->flagField[0], PTP_TWO_STEP_FLAG)) {
                    T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"received 1-step sync");
                    slave->wait_follow_up  = false;
                    slave->sync_tx_time = originTimestamp;

                    i8 logMsgIntv;
                    if (getFlag(header->flagField[0], PTP_UNICAST_FLAG)) {
                        int master_index = slaveTableEntryFind(ptpClock[clock_inst]->slave, sender->ip);
                        if (master_index >=0) {
                            logMsgIntv = ptpClock[clock_inst]->slave[master_index].log_msg_period;
                        }
                        else {
                            logMsgIntv = slave->slave_port->port_config->logSyncInterval;
                        }
                    }
                    else {
                        logMsgIntv = header->logMessageInterval;
                    }

                    if (calib_t_plane_enabled || calib_p2p_enabled || calib_port_enabled) {
                        mesa_timestamp_t t1 = originTimestamp;
                        mesa_timestamp_t t2 = slave->sync_receive_time;
                        mesa_timeinterval_t diff_t1_t2;
                        static int calib_iteration = 0;
                        static mesa_timeinterval_t diff_t1_t2_accumulated = 0;
                        static mesa_timeinterval_t meanPathDelay_accumulated = 0;

                        if (calib_initiate) {
                           calib_iteration = 0;
                           diff_t1_t2_accumulated = 0;
                           meanPathDelay_accumulated = 0;

                           calib_initiate = false;
                        }
                        calib_iteration++;

                        vtss_tod_sub_TimeInterval(&diff_t1_t2, &t2, &t1);
                        diff_t1_t2 -= header->correctionField;
                        diff_t1_t2_accumulated += diff_t1_t2;
                        meanPathDelay_accumulated += slave->clock->currentDS.meanPathDelay;

                        T_I("Difference from t1 to t2 : %d.%03d ns", diff_t1_t2 >> 16, ((diff_t1_t2 & 0xFFFF) * 1000) >> 16);
                       
                        T_I("Slave port is: %s %u", vtss_ptp_clock_identity_to_str(slave->portIdentity_p->clockIdentity), slave->portIdentity_p->portNumber);
                        T_I("Master port is: %s %u", vtss_ptp_clock_identity_to_str(slave->parent_portIdentity_p->clockIdentity), slave->parent_portIdentity_p->portNumber);

                        if (calib_iteration == 100) {
                            mesa_timeinterval_t diff_t1_t2_avg = diff_t1_t2_accumulated / calib_iteration - ((mesa_timeinterval_t) calib_cable_latency << 16);
                            mesa_timeinterval_t meanPathDelay_avg = meanPathDelay_accumulated / calib_iteration - ((mesa_timeinterval_t) calib_cable_latency << 16);

                            // Check that clock identity of master matches that of the slave (except for the last bit) and the port is the same (otherwise the port has not been
                            // properly looped back).
                            bool loopback_ok = true;
                            if (calib_t_plane_enabled || calib_p2p_enabled) {
                                for (int n = 0; n < CLOCK_IDENTITY_LENGTH - 1; n++) {
                                   if (slave->portIdentity_p->clockIdentity[n] != slave->parent_portIdentity_p->clockIdentity[n]) loopback_ok = false;
                                }
                                if ((slave->portIdentity_p->clockIdentity[CLOCK_IDENTITY_LENGTH - 1] & 0xFE) != (slave->parent_portIdentity_p->clockIdentity[CLOCK_IDENTITY_LENGTH - 1] & 0xFE)) loopback_ok = false;
                            }
                            if (calib_t_plane_enabled) {
                                if (slave->portIdentity_p->portNumber != slave->parent_portIdentity_p->portNumber) loopback_ok = false;
                            } else if (calib_p2p_enabled) {
                                // FIXME: Make a check here that the correct port from the master has been looped to the slave port 
                            } else if (calib_port_enabled) {  
                                // FIXME: In the case of calib_port_enabled just check that the slave ported matches the configured port
                            } else {
                                T_W("Unexpected case in calibration procedure.");
                            }
     
                            if (loopback_ok) {
                                T_I("Loopback is OK");
     
                                // Add 50% of diff_t1_t2 to ports egress latency and 50% of it to ports ingress latency in order to make t1 and t2 equal going forward
                                // This involves a number of steps below:
     
                                // Determine port mode
                                vtss_appl_port_status_t port_status;
                                vtss_ifindex_t ifindex;
                                (void) vtss_ifindex_from_port(0, uport2iport(slave->portIdentity_p->portNumber), &ifindex);
                                vtss_appl_port_status_get(ifindex, &port_status);
     
                                char *speed_txt;
                                vtss_ptp_port_speed_to_txt(port_status.status.speed, &speed_txt);
                                T_I("Port speed is: %s, duplex state is: %s", speed_txt, port_status.status.fdx ? "fdx" : "hdx");
          
                                // Read previous ingress and egress calibration values
                                mesa_timeinterval_t ingress_calib_value;
                                mesa_timeinterval_t egress_calib_value;

                                BOOL fiber = port_status.status.fiber;

                                switch (port_status.status.speed) {

                                    case MESA_SPEED_10M:   {
                                                               ingress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_10m_cu.ingress_latency;
                                                               egress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_10m_cu.egress_latency;
                                                               break;
                                                           }  
                                    case MESA_SPEED_100M:  {
                                                               ingress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_100m_cu.ingress_latency;
                                                               egress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_100m_cu.egress_latency;
                                                               break;
                                                           }  
                                    case MESA_SPEED_1G:    {
                                                               if (fiber) {
                                                                   ingress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_1g.ingress_latency;
                                                                   egress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_1g.egress_latency;
                                                               } else {
                                                                   ingress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_1g_cu.ingress_latency;
                                                                   egress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_1g_cu.egress_latency;
                                                               }
                                                               break;
                                                           }  
                                    case MESA_SPEED_2500M: {
                                                               ingress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_2g5.ingress_latency;
                                                               egress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_2g5.egress_latency;
                                                               break;
                                                           }
                                    case MESA_SPEED_5G:    {
                                                               ingress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_5g.ingress_latency;
                                                               egress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_5g.egress_latency;
                                                               break;
                                                           }
                                    case MESA_SPEED_10G:   {
                                                               ingress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_10g.ingress_latency;
                                                               egress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_10g.egress_latency;
                                                               break;
                                                           }
                                    default: {
                                                 ingress_calib_value = 0;
                                                 egress_calib_value = 0;
                                             }
                                }
                                T_I("Previous egress calibration value was: %d", egress_calib_value);
                                T_I("Previous ingress calibration value was: %d", ingress_calib_value);
     
                                // Calculate new calibration values
                                if (calib_t_plane_enabled) {
                                    ingress_calib_value += diff_t1_t2_avg / 2;
                                } else if (calib_p2p_enabled) {
                                    ingress_calib_value += diff_t1_t2_avg;
                                } else if (calib_port_enabled) {
                                    ingress_calib_value += meanPathDelay_avg - ((mesa_timeinterval_t) calib_pps_offset << 16);
                                } else {
                                    T_W("Unexpected case in calibration procedure.");
                                }
                                if (ingress_calib_value >= 0) {
                                    T_I("New ingress calibration value: %lld.%03lld", ingress_calib_value, ((ingress_calib_value % 0xffff) * 1000) >> 16);
                                } else {
                                    T_I("New ingress calibration value: %lld.%03lld", ingress_calib_value, ((-ingress_calib_value % 0xffff) * 1000) >> 16);
                                }

                                if (calib_t_plane_enabled) {
                                    egress_calib_value += diff_t1_t2_avg / 2;
                                } else if (calib_p2p_enabled) {
                                    egress_calib_value -= diff_t1_t2_avg;
                                } else if (calib_port_enabled) {
                                    egress_calib_value += meanPathDelay_avg + ((mesa_timeinterval_t) calib_pps_offset << 16);
                                } else {
                                    T_W("Unexpected case in calibration procedure.");
                                }
                                if (egress_calib_value >= 0) {
                                    T_I("New egress calibration value: %lld.%03lld", egress_calib_value, ((egress_calib_value % 0xffff) * 1000) >> 16);
                                } else {
                                    T_I("New egress calibration value: %lld.%03lld", egress_calib_value, ((-egress_calib_value % 0xffff) * 1000) >> 16);
                                }

                                // Store the updated calibration values
                                switch (port_status.status.speed) {

                                    case MESA_SPEED_10M:   {
                                                               ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_10m_cu.ingress_latency = ingress_calib_value;
                                                               ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_10m_cu.egress_latency = egress_calib_value;
                                                               break;
                                                           }
                                    case MESA_SPEED_100M:  {
                                                               ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_100m_cu.ingress_latency = ingress_calib_value;
                                                               ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_100m_cu.egress_latency = egress_calib_value;
                                                               break;
                                                           }
                                    case MESA_SPEED_1G:    {
                                                               if (fiber) {
                                                                   ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_1g.ingress_latency = ingress_calib_value;
                                                                   ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_1g.egress_latency = egress_calib_value;
                                                               } else {
                                                                   ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_1g_cu.ingress_latency = ingress_calib_value;
                                                                   ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_1g_cu.egress_latency = egress_calib_value;
                                                               }
                                                               break;
                                                           }  
                                    case MESA_SPEED_2500M: {
                                                               ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_2g5.ingress_latency = ingress_calib_value;
                                                               ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_2g5.egress_latency = egress_calib_value;
                                                               break;
                                                           }
                                    case MESA_SPEED_5G:    {
                                                               ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_5g.ingress_latency = ingress_calib_value;
                                                               ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_5g.egress_latency = egress_calib_value;
                                                               break;
                                                           }
                                    case MESA_SPEED_10G:   {
                                                               ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_10g.ingress_latency = ingress_calib_value;
                                                               ptp_port_calibration.port_calibrations[uport2iport(slave->portIdentity_p->portNumber)].port_latencies_10g.egress_latency = egress_calib_value;
                                                               break;
                                                           }
                                    default:;
                                }

                                // Write PTP port calibration to file on Linux filesystem
                                {
                                    int ptp_port_calib_file = open(PTP_CALIB_FILE_NAME, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
                                    if (ptp_port_calib_file != -1) {
                                        ptp_port_calibration.version = PTP_CURRENT_CALIB_FILE_VER;
                                        u32 crc32 = vtss_crc32((const unsigned char*)&ptp_port_calibration.version, sizeof(u32));
                                        crc32 = vtss_crc32_accumulate(crc32, (const unsigned char*)ptp_port_calibration.port_calibrations.data(), MESA_CAP(MESA_CAP_PORT_CNT) * sizeof(port_calibrations_s));
                                        ptp_port_calibration.crc32 = crc32;
                        
                                        ssize_t numwritten = write(ptp_port_calib_file, &ptp_port_calibration.version, sizeof(u32));
                                        numwritten += write(ptp_port_calib_file, &ptp_port_calibration.crc32, sizeof(u32));
                                        numwritten += write(ptp_port_calib_file, ptp_port_calibration.port_calibrations.data(), MESA_CAP(MESA_CAP_PORT_CNT) * sizeof(port_calibrations_s));
                        
                                        if (numwritten != 2 * sizeof(u32) + MESA_CAP(MESA_CAP_PORT_CNT) * sizeof(port_calibrations_s)) {
                                            T_W("Problem writing PTP port calibration data file.");
                                        }
                        
                                        if (close(ptp_port_calib_file) == -1) {
                                            T_W("Could not close PTP port calibration data file.");
                                        }
                                    } else {
                                        T_W("Could not create/open PTP port calibration data file");
                                    }
                                }

                                // Update ingrees and egress latencies in the hardware
                                {
                                    mesa_timeinterval_t egress_latency;
     
                                    vtss_1588_egress_latency_get(slave->portIdentity_p->portNumber, &egress_latency);
                                    vtss_1588_egress_latency_set(slave->portIdentity_p->portNumber, egress_latency);
                                }
                                {
                                    mesa_timeinterval_t ingress_latency;
     
                                    vtss_1588_ingress_latency_get(slave->portIdentity_p->portNumber, &ingress_latency);
                                    vtss_1588_ingress_latency_set(slave->portIdentity_p->portNumber, ingress_latency);
                                }
     
                            } else {
                                T_I("Loopback is NOT OK");
                            }
                            calib_t_plane_enabled = false;
                            calib_p2p_enabled = false;
                            calib_port_enabled = false;
                        }
                    }

                    if (ptp_offset_calc(ptpClock, clock_inst, originTimestamp, slave->sync_receive_time, header->correctionField, logMsgIntv, header->sequenceId)) {
                        // Advanced servo algorithm has asked for adjustment of time. Do not send delay request as response will be useless anyway                       
                        // and most importantly: do not retrigger sync_timer as servo algorithm has already programmed a timeout value of 3s.
                        return forwarded;
                    }
                    (void)clock_class_update(slave);
                } else {
                    if (slave->wait_follow_up  == true) {
                        T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"Follow_up message lost: cur.seq. %d", header->sequenceId);
                        slave->statistics.follow_up_pack_loss_cnt++;
                    }
                    slave->wait_follow_up  = true;
                    slave->sync_correctionField = header->correctionField;
                    T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"received 2-step sync");
                }
                if (slave->slave_port != NULL) {
                    slave->sender = *sender;
                    if (slave->first_sync_packet) {
                        slave->first_sync_packet = false;
                        if (slave->random_relay_request_timer) {
                            vtss_ptp_timer_start(&slave->delay_req_sys_timer, 1, false);
                        }
                    }
                    if (slave->two_way) {
                        if (slave->slave_port->port_config->delayMechanism == DELAY_MECH_E2E) {
                            if (!slave->random_relay_request_timer) {
                                if (((slave->clock->clock_init->cfg.deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND) || (slave->clock->clock_init->cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY)) &&
                                        !(--slave->delay_req_timer)) {
                                    transmit_delay_req(&slave->delay_req_sys_timer, slave);
                                }
                            }
                        } else {
                            slave->clock->currentDS.delayOk = slave->slave_port->portDS.status.peer_delay_ok;
                            T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"delayOk %d", slave->clock->currentDS.delayOk);
                        }
                    }
                    T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"SYNC_RECEIPT_TIMER reset");
                    if (slave->in_settling_period) {
                        slave->sy_to = (slave->clock_settle_time ) * PTP_LOG_TIMEOUT(0);
                    } else {
                        slave->sy_to = (slave->logSyncInterval > -2) ? 3*PTP_LOG_TIMEOUT(slave->logSyncInterval) : PTP_LOG_TIMEOUT(0);
                    }
                    vtss_ptp_timer_start(&slave->sync_timer, slave->sy_to, false);
                } else {
                    T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"port went disabled in offset calc");
                }
            }
        } else {
            T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"sync sequence error: last %d, this %d", slave->parent_last_sync_sequence_number, header->sequenceId);
            slave->parent_last_sync_sequence_number = header->sequenceId;
            ++slave->statistics.sync_pack_seq_err_cnt;
        }
    } else {
        T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"received Sync: slave not active");
    }
    return forwarded;
}

/*
 * Handle follow_up request from a master
 *
 */
bool vtss_ptp_slave_follow_up(CapArray<ptp_clock_t *, VTSS_APPL_CAP_PTP_CLOCK_CNT> &ptpClock, int clock_inst, ptp_tx_buffer_handle_t *tx_buf, MsgHeader *header, vtss_appl_ptp_protocol_adr_t *sender)
{
    ptp_slave_t *slave = &ptpClock[clock_inst]->ssm;
    bool forwarded = false;
    u8 *buf = tx_buf->frame + tx_buf->header_length;
    mesa_timestamp_t  preciseOriginTimestamp;
    

    if (slave->slave_port != NULL) {
        vtss_ptp_unpack_timestamp(buf, &preciseOriginTimestamp);

        T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"wait_follow_up %d", slave->wait_follow_up);
        if ( slave->wait_follow_up
                && header->sequenceId == slave->parent_last_sync_sequence_number
                && (!PortIdentitycmp(&header->sourcePortIdentity, slave->parent_portIdentity_p)
                   )) {
            slave->wait_follow_up  = false;
            slave->sync_tx_time = preciseOriginTimestamp;
            // check if Follow_Up TLV is wanted and present.





























            i8 logMsgIntv;
            if (getFlag(header->flagField[0], PTP_UNICAST_FLAG)) {
                int master_index = slaveTableEntryFind(ptpClock[clock_inst]->slave, sender->ip);
                if (master_index >=0) {
                    logMsgIntv = ptpClock[clock_inst]->slave[master_index].log_msg_period;
                }
                else {
                    logMsgIntv = slave->slave_port->port_config->logSyncInterval;
                }
            }
            else {
                logMsgIntv = header->logMessageInterval;
            }

            if (ptp_offset_calc(ptpClock, clock_inst, preciseOriginTimestamp, slave->sync_receive_time, header->correctionField + slave->sync_correctionField, logMsgIntv, header->sequenceId)) {
                // Advanced servo algorithm has asked for adjustment of time. Do not send delay request as response will be useless anyway                       
                // and most importantly: do not retrigger sync_timer as servo algorithm has already programmed a timeout value of 3s.
                return forwarded;
            }
            (void)clock_class_update(slave);
            if (slave->in_settling_period) {
                vtss_ptp_timer_start(&slave->sync_timer, (slave->clock_settle_time ) * PTP_LOG_TIMEOUT(0), false);
            }
            /* if delay req has been received before follow_up then the delay calculation is done now */
            if (slave->delay_req_send_time.seconds && slave->delay_req_receive_time.seconds) {
                ptp_delay_calc(ptpClock, clock_inst, slave->delay_req_send_time, slave->delay_req_receive_time, slave->delay_resp_correction, logMsgIntv);
                T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"DelayResp received before followup");
            }
        } else {
            T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"handleFollowUp: unwanted, wff = %d, header.seq = %d, sync.seq = %d",
                slave->wait_follow_up ,
                header->sequenceId,
                slave->parent_last_sync_sequence_number);
        }
    }
    return forwarded;
}

/*
 * Handle delayResp request from a master
 *
 */
bool vtss_ptp_slave_delay_resp(CapArray<ptp_clock_t *, VTSS_APPL_CAP_PTP_CLOCK_CNT> &ptpClock, int clock_inst, ptp_tx_buffer_handle_t *tx_buf, MsgHeader *header, vtss_appl_ptp_protocol_adr_t *sender)
{
    ptp_slave_t *slave = &ptpClock[clock_inst]->ssm;
    bool forwarded = false;
    u8 *buf = tx_buf->frame + tx_buf->header_length;
    MsgDelayResp resp;

    if (slave->slave_port != NULL) {
        vtss_ptp_unpack_delay_resp(buf, &resp);

        if (!PortIdentitycmp(&resp.requestingPortIdentity, slave->portIdentity_p) ) {
            if ( slave->wait_delay_resp
                    && header->sequenceId == slave->last_delay_req_event_sequence_number
                    && !PortIdentitycmp(&resp.requestingPortIdentity, slave->portIdentity_p)
                    && !PortIdentitycmp(&header->sourcePortIdentity, slave->parent_portIdentity_p) ) {
                if (!getFlag(header->flagField[0], PTP_UNICAST_FLAG)) {
                    slave->slave_port->portDS.status.logMinDelayReqInterval = header->logMessageInterval;
                } else {
                    slave->slave_port->portDS.status.logMinDelayReqInterval = slave->slave_port->port_config->logMinPdelayReqInterval;
                }
                slave->wait_delay_resp = false;
                slave->delay_req_receive_time = resp.receiveTimestamp;
                slave->delay_resp_correction = header->correctionField;

                if (slave->slave_port->wd.enable) {
                    /* add wireless delay to correctionField (delay_resp is 10 bytes longer than delay_req)*/
                    slave->delay_resp_correction = slave->slave_port->wd.base_delay + (tx_buf->size-10)*slave->slave_port->wd.incr_delay;
                    T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"transmit delay (%d.%03d ps) added to relay_resp correctionField", VTSS_INTERVAL_NS(slave->delay_resp_correction),VTSS_INTERVAL_PS(slave->delay_resp_correction));
                }


                if (getFlag(header->flagField[0], PTP_UNICAST_FLAG)) {
                    int master_index = slaveTableEntryFind(ptpClock[clock_inst]->slave, sender->ip);
                    if (master_index >=0) {
                        slave->logDelayReqInterval = ptpClock[clock_inst]->slave[master_index].log_msg_period;
                    }
                    else {
                        slave->logDelayReqInterval = slave->slave_port->port_config->logSyncInterval;
                    }
                }
                else {
                    slave->logDelayReqInterval = header->logMessageInterval;
                }

                if (slave->delay_req_send_time.seconds && !slave->wait_follow_up ) {
                    ptp_delay_calc(ptpClock, clock_inst, slave->delay_req_send_time, slave->delay_req_receive_time, slave->delay_resp_correction, slave->logDelayReqInterval);
                    T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"DelayResp received after followup and delayreqdone");

                } else {
                    T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"request sent time not registered yet");
                }
                // save context for event_transmitted
                slave->clockBase = &ptpClock;
            } else {
                T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"unwanted, expected seqId: %d, received seqId: %d", slave->last_delay_req_event_sequence_number, header->sequenceId);
            }
        } else {
            T_DG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"response not intended for me");
        }
    }
    return forwarded;
}

/*
 * PTP clock settling timer
 */
/*lint -esym(459, vtss_ptp_settle_sync_timer) */
static void vtss_ptp_settle_sync_timer(vtss_timer_handle_t timer, void *s)
{
    ptp_slave_t *slave = (ptp_slave_t *)s;
    if (slave->clock_state == VTSS_PTP_SLAVE_CLOCK_F_SETTLING) {
        slave->clock_state = VTSS_PTP_SLAVE_CLOCK_FREERUN;
        T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s. Reason: Settling time expired", clock_state_to_string (slave->clock_state));
        T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"Settle timeout in a Free run state");
    } else if (slave->clock_state == VTSS_PTP_SLAVE_CLOCK_P_SETTLING) {
        slave->clock_state = VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKING;
        T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s. Reason: Settling time expired", clock_state_to_string (slave->clock_state));
        T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"Settle timeout in a Phase locking state");
    } else if (slave->clock_state == VTSS_PTP_SLAVE_CLOCK_FREQ_LOCK_INIT) {
        slave->clock_state = VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKING;
        T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s. Reason: Settling time expired", clock_state_to_string (slave->clock_state));
        T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"Settle timeout in a FreqLockInit state");
    } else {
        T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"Settle timeout in an unexpected state");
    }

    slave->in_settling_period = false;
}

void debug_log_header_2_print(FILE *logFile)
{
    fprintf(logFile, "#MasterUUID 00:01:C1:FF:FE:xx:xx:xx\n");
    fprintf(logFile, "#MasterIP n.a.\n");
    fprintf(logFile, "#ProbeUUID 00:01:c1:FF:FE:xx:xx:xx\n");
    fprintf(logFile, "#ProbeIP n.a.\n");
    fprintf(logFile, "#Title: Microsemi Test Probe/1588 Timestamp Data/Transmit and receive Timestamp\n");
}

int ptp_offset_calc(CapArray<ptp_clock_t *, VTSS_APPL_CAP_PTP_CLOCK_CNT> &ptpClock, int clock_inst, const mesa_timestamp_t& send_time, const mesa_timestamp_t& recv_time, mesa_timeinterval_t correction, i8 logMsgIntv, u16 sequenceId)
{
    int r;
    bool peer_delay;
    // The Servo does not support oneway, unless we tell it that we use P2P delay mechanism, therefore the peer_delay parameter is also set in the oneWay case
    if (ptpClock[clock_inst]->ssm.slave_port == 0) {
        peer_delay = false;
    } else {
        peer_delay = ptpClock[clock_inst]->ssm.slave_port->port_config->delayMechanism == DELAY_MECH_P2P || ptpClock[clock_inst]->clock_init->cfg.oneWay;
    }
    r = ptpClock[clock_inst]->ssm.servo->offset_calc(ptpClock, clock_inst, send_time, recv_time, correction, logMsgIntv, sequenceId,
            peer_delay);
    if (ptpClock[clock_inst]->ssm.clock_state != ptpClock[clock_inst]->ssm.old_clock_state) {
        T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s", clock_state_to_string(ptpClock[clock_inst]->ssm.clock_state));
        ptpClock[clock_inst]->ssm.old_clock_state = ptpClock[clock_inst]->ssm.clock_state;
    }
    return r;
}

void ptp_delay_calc(CapArray<ptp_clock_t *, VTSS_APPL_CAP_PTP_CLOCK_CNT> &ptpClock, int clock_inst, const mesa_timestamp_t& send_time, const mesa_timestamp_t& recv_time, mesa_timeinterval_t correction, i8 logMsgIntv)
{
    ptpClock[clock_inst]->ssm.servo->delay_calc(ptpClock, clock_inst, send_time, recv_time, correction, logMsgIntv);
}

bool clock_class_update(ptp_slave_t *ssm)
{
    bool rc = true;
    vtss_appl_ptp_clock_slave_ds_t slave_ds;

    vtss_ptp_get_clock_slave_ds(ssm->clock, &slave_ds);

    T_D("slave_state %d",slave_ds.slave_state);
    switch (slave_ds.slave_state) {
        case VTSS_APPL_PTP_SLAVE_CLOCK_STATE_FREERUN:
            ssm->clock->announced_clock_quality = ssm->clock->defaultDS.status.clockQuality;
            ssm->clock->time_prop->frequencyTraceable = false;
            break;
        case VTSS_APPL_PTP_SLAVE_CLOCK_STATE_FREQ_LOCKING:
        case VTSS_APPL_PTP_SLAVE_CLOCK_STATE_PHASE_LOCKING:
            break; // unchanged clock class
        case VTSS_APPL_PTP_SLAVE_CLOCK_STATE_FREQ_LOCKED:
        case VTSS_APPL_PTP_SLAVE_CLOCK_STATE_PHASE_LOCKED:
            if (ssm->clock->clock_init->cfg.profile == VTSS_APPL_PTP_PROFILE_G_8275_1) {
                if (ssm->clock->clock_init->cfg.deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY) {
                    ssm->clock->announced_clock_quality.clockClass = G8275PRTC_GM_CLOCK_CLASS;
                    ssm->clock->announced_clock_quality.clockAccuracy = 0x21;
                    ssm->clock->announced_clock_quality.offsetScaledLogVariance = 0x4E5D;
                    ssm->clock->timepropertiesDS.frequencyTraceable = true;
                } else { // otherwise the announced clock class from the master is used
                    ssm->clock->announced_clock_quality.clockClass = ssm->clock->parentDS.grandmasterClockQuality.clockClass;
                    ssm->clock->announced_timeTraceable = ssm->clock->timepropertiesDS.timeTraceable;
                    ssm->clock->announced_currentUtcOffsetValid = ssm->clock->timepropertiesDS.currentUtcOffsetValid;
                }
                ssm->g8275_holdover_trace_count = 0;
            } else {
                ssm->clock->announced_clock_quality = ssm->clock->parentDS.grandmasterClockQuality;
            }
            break;
        case VTSS_APPL_PTP_SLAVE_CLOCK_STATE_HOLDOVER:
            if (ssm->clock->clock_init->cfg.profile == VTSS_APPL_PTP_PROFILE_G_8275_1) {
                T_I("g8275_holdover_trace_count %d, holdover time %d",ssm->g8275_holdover_trace_count, ssm->clock->holdover_timeout_spec);
                if (ssm->g8275_holdover_trace_count < ssm->clock->holdover_timeout_spec) {
                    ssm->g8275_holdover_trace_count++;
                    if (ssm->clock->clock_init->cfg.deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY) {
                        ssm->clock->announced_clock_quality.clockClass = G8275PRTC_GM_HO_CLOCK_CLASS;
                    } else {
                        // BC in holdover: only change the clockClass if it has  been locked to one with better class
                        if (ssm->clock->announced_clock_quality.clockClass < G8275PRTC_BC_HO_CLOCK_CLASS) {
                            ssm->clock->announced_clock_quality.clockClass = G8275PRTC_BC_HO_CLOCK_CLASS;
                        }
                        ssm->clock->timepropertiesDS.timeTraceable = ssm->clock->announced_timeTraceable;
                        ssm->clock->timepropertiesDS.currentUtcOffsetValid = ssm->clock->announced_currentUtcOffsetValid;
                    }
                } else {
                    if (ssm->clock->clock_init->cfg.deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY) {
                        ssm->clock->announced_clock_quality.clockClass = ssm->clock->local_clock_class;
                    } else {
                        // BC in holdover: only change the clockClass if it has  been locked to one with better class
                        if (ssm->clock->announced_clock_quality.clockClass < G8275PRTC_BC_OUT_OF_HO_CLOCK_CLASS) {
                            ssm->clock->announced_clock_quality.clockClass = G8275PRTC_BC_OUT_OF_HO_CLOCK_CLASS;
                        }
                        ssm->clock->timepropertiesDS.timeTraceable = false;
                        ssm->clock->timepropertiesDS.currentUtcOffsetValid = false;
                    }
                    rc = false;
                }
                if (ssm->clock->local_clock_class == G8275PRTC_GM_OUT_OF_HO_CLOCK_CLASS_CAT1 ||
                        ssm->clock->local_clock_class == G8275PRTC_BC_HO_CLOCK_CLASS) {
                    ssm->clock->timepropertiesDS.frequencyTraceable = true;
                } else {
                    ssm->clock->timepropertiesDS.frequencyTraceable = false;
                }
                ssm->clock->announced_clock_quality.clockAccuracy = 0xFE;
                ssm->clock->announced_clock_quality.offsetScaledLogVariance = 0xFFFF;
            } else {
                ssm->clock->announced_clock_quality = ssm->clock->defaultDS.status.clockQuality;
            }
            break;
        default:
            ssm->clock->announced_clock_quality = ssm->clock->defaultDS.status.clockQuality;
            ssm->clock->time_prop->frequencyTraceable = false;
            break;
    }
    return rc;
}

/*
 * Stop logging
 *
 */
/*lint -esym(459, vtss_ptp_slave_log_timeout) */
static void vtss_ptp_slave_log_timeout(vtss_timer_handle_t timer, void *s)
{
    ptp_slave_t *slave = (ptp_slave_t *)s;
    ptp_clock_t *clock = slave->clock;
    T_IG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"logging timed out");
    if (!vtss_ptp_debug_mode_set(clock, 0, FALSE, FALSE, 0)) {
        T_WG(VTSS_TRACE_GRP_PTP_BASE_SLAVE,"stop logging failed");
    }
}

