/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "vtss_ptp_api.h"
#include "vtss_ptp_types.h"
#include "vtss_ptp_os.h"
#include "vtss_ptp_peer_delay.h"
#include "vtss_ptp_sys_timer.h"
#include "vtss_ptp_pack_unpack.h"
#include "vtss_ptp_local_clock.h"
#include "vtss_ptp_packet_callout.h"
#include "vtss_tod_api.h"
#include "vtss_ptp_internal_types.h"




/* state defined as random numbers for consistency check */
#define PEER_DELAY_PTP_OBJCT_ACTIVE         0x47835672
#define PEER_DELAY_PTP_OBJCT_WAIT_TX_DONE   0x5a3be982
#define PEER_DELAY_PTP_OBJCT_PASSIVE        0x58370465









// Define Peer delay error status
#define PEER_DELAY_MORE_THAN_ONE_T1_ERROR 0x1
#define PEER_DELAY_MORE_THAN_ONE_RESPONDER_ERROR 0x2
#define PEER_DELAY_MORE_THAN_ONE_FOLLOWUP_ERROR 0x4
#define PEER_DELAY_MISSED_RESPONSE_ERROR 0x8
#define PEER_DELAY_DELTA_T3_ERROR 0x10
#define PEER_DELAY_DELTA_T4_ERROR 0x20
#define PEER_DELAY_INVALID_RATE_RATIO_ERROR 0x40
#define PEER_DELAY_ONESTEP_RESPONDER_ERROR 0x80
#define PEER_DELAY_MAX_ERROR 0x100
/*
 * Forward declarations
 */
static void vtss_ptp_peer_delay_req_event_transmitted(void *context, uint portnum, u32 tx_time);
static void vtss_ptp_peer_delay_req_timer(vtss_timer_handle_t timer, void *m);
static void vtss_ptp_peer_delay_resp_event_transmitted(void *context, uint portnum, u32 tx_time);

/*
 * private functions
 */
static void show_measure_status(u16 measure_status)
{
    const char * peer_delay_error_txt[] = {
        "more_than_one_t1",
        "more_than_one_responder",
        "more_than_one_followup or missing pDelayResp",
        "missed_responses",
        "delta_t3",
        "delta_t4",
        "invalid_rate_ratio",
        "onestep_responder",
    };
    u16 mask = 1;
    u16 i = 0;
    while (mask <= PEER_DELAY_MAX_ERROR && i < (sizeof(peer_delay_error_txt) / sizeof(char *))) {
        if (measure_status & mask) T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"Peer delay failed: %s", peer_delay_error_txt[i]);
        mask = mask<<1;
        i++;
    }
}

static const char *state2name(u32 state) {
    switch (state) {
        case PEER_DELAY_PTP_OBJCT_ACTIVE:         return "ACTIVE";
        case PEER_DELAY_PTP_OBJCT_WAIT_TX_DONE:   return "WAIT_TX_DONE";
        case PEER_DELAY_PTP_OBJCT_PASSIVE:        return "PASSIVE";
        default:                                  return "invalid state";
    }
}

static void init_pdelay_measurements(PeerDelayData *peer_delay)
{
    if (peer_delay->ptp_port->port_config->delayMechanism == DELAY_MECH_P2P) {
        peer_delay->last_pdelay_req_event_sequence_number = 0;
        peer_delay->pdelay_event_sequence_errors = 0;
        peer_delay->pdelay_missed_response_cnt = peer_delay->ptp_port->port_config->c_802_1as.allowedLostResponses+1; // initialy we assume lost response until the first response is received
        /* these fields are filled in when the message is transmitted and the response is received */
        peer_delay->t1.seconds = 0;
        peer_delay->t1.nanoseconds = 0;
        peer_delay->t4.seconds = 0;
        peer_delay->t4.nanoseconds = 0;
        peer_delay->clock->ssm.servo->delay_filter_reset(peer_delay->ptp_port->portDS.status.portIdentity.portNumber);  /* clears one-way P2P delay filter */
        T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"Port %d:Pdelay measurement initialized", peer_delay->ptp_port->portDS.status.portIdentity.portNumber);
    }
}

static bool port_uses_two_step(PeerDelayData *peer_delay)
{
    if ((peer_delay->clock->clock_init->cfg.deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND) || (peer_delay->clock->clock_init->cfg.deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY)) {
        return (peer_delay->clock->clock_init->cfg.twoStepFlag && !(peer_delay->ptp_port->port_config->twoStepOverride == VTSS_APPL_PTP_TWO_STEP_OVERRIDE_FALSE)) ||
                             (peer_delay->ptp_port->port_config->twoStepOverride == VTSS_APPL_PTP_TWO_STEP_OVERRIDE_TRUE) ||
                             (peer_delay->clock->clock_init->cfg.clock_domain != 0);
    }
    else {
        return peer_delay->clock->clock_init->cfg.twoStepFlag || peer_delay->clock->clock_init->cfg.clock_domain != 0;
    }
}

static void ptp_peer_delay_calc(PeerDelayData *peer_delay)
{
    char str [40];
    mesa_timeinterval_t t4minust1;
    vtss_ptp_offset_filter_param_t delay;
    T_RG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"ptp_peer_delay_calc");

    u32 cur_set_time_count = vtss_local_clock_set_time_count_get(peer_delay->clock->localClockId);
    if (cur_set_time_count != peer_delay->requester_set_time_count) {
        peer_delay->requester_set_time_count = cur_set_time_count;
        T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"set_time_count changed to %d", cur_set_time_count);
        return; // delay calculation is ignored if time has been changed since last pdelay_req, because this wil result in a wrong t4minust1
    }
    /* update 'one_way_delay' */
    if (peer_delay->t1_valid && peer_delay->t2_t3_valid && peer_delay->t4_valid && !peer_delay->peerDelayNotMeasured) {
        vtss_tod_sub_TimeInterval(&t4minust1, &peer_delay->t4, &peer_delay->t1);










        // if responder is 1 step, t3-t2 is included in the correction field in the PdelayResp
        // if responder is 2 step, t3-t2 is added to the correction field in the PdelayRespFollpw_Up
        delay.offsetFromMaster = (t4minust1 - peer_delay->correctionField)/2;
        T_DG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"Port %d, peer delay before filtering %s", peer_delay->ptp_port->portDS.status.portIdentity.portNumber,
             vtss_tod_TimeInterval_To_String(&delay.offsetFromMaster, str,','));

        // Setup data for this latency calculation
        delay.rcvTime = {0, 0};  // Not used when doing peer delay calculation
        if (0 == peer_delay->clock->ssm.servo->delay_filter(&delay, 0, peer_delay->ptp_port->portDS.status.portIdentity.portNumber)) {
            T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"Port %d: Delay too big for filtering", peer_delay->ptp_port->portDS.status.portIdentity.portNumber);
        }
        T_DG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"peer delay after filtering %s", vtss_tod_TimeInterval_To_String(&delay.offsetFromMaster, str,','));
        if (!port_uses_two_step(peer_delay)) {
            vtss_1588_p2p_delay_set(peer_delay->ptp_port->portDS.status.portIdentity.portNumber, peer_delay->ptp_port->portDS.status.peerMeanPathDelay);
        }

        peer_delay->pdelay_missed_response_cnt = 0;
        if (llabs(delay.offsetFromMaster) > peer_delay->ptp_port->port_config->c_802_1as.neighborPropDelayThresh) {
            T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"peerDelayNotMeasured = true");
            peer_delay->peerDelayNotMeasured = true;
        }
        peer_delay->ptp_port->portDS.status.peerMeanPathDelay = delay.offsetFromMaster;
        T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"delay_thresh %s, not_measured %s", vtss_tod_TimeInterval_To_String(&peer_delay->ptp_port->port_config->c_802_1as.neighborPropDelayThresh, str,','), peer_delay->peerDelayNotMeasured ? "true" : "false");
    }
    if (!peer_delay->t1_valid) {
        T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"Port %d: t1(%d) not measured yet", peer_delay->ptp_port->portDS.status.portIdentity.portNumber, peer_delay->t1.seconds);
    }
}

/*
 * create a PTP Peer Delay instance
 * Allocate packet buffer for PDelay_Req and if TwoStep: PDelay_Resp_FollowUp
 * Initialize encapsulation protocol
 * Initialize PTP header
 * Initialize PTP PDelayReq data.
 * start PDelay_Req timer
 */
void vtss_ptp_peer_delay_create(PeerDelayData *peer_delay, vtss_appl_ptp_protocol_adr_t *ptp_dest, ptp_tag_conf_t *tag_conf)
{
    mesa_timestamp_t originTimestamp = {0,0,0};
    vtss_appl_ptp_port_identity reserved = {{0,0,0,0,0,0,0,0},0};
    MsgHeader header;
    u32 to;
    peer_delay->last_pdelay_req_event_sequence_number = 0;
    peer_delay->ratio_clear_time = 0;

    header.versionPTP = peer_delay->ptp_port->port_config->versionNumber;
    header.messageType = PTP_MESSAGE_TYPE_P_DELAY_REQ;
    header.transportSpecific = peer_delay->clock->majorSdoId;
    header.messageLength = P_DELAY_REQ_PACKET_LENGTH;
    header.domainNumber = peer_delay->clock->clock_init->cfg.domainNumber;
    header.reserved1 = MINOR_SDO_ID;
    header.flagField[0] = 0;
    header.flagField[1] = 0;

    memcpy(&header.sourcePortIdentity, &peer_delay->ptp_port->portDS.status.portIdentity, sizeof(vtss_appl_ptp_port_identity));
    T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"create peer_delay for port %d", tag_conf->port);
    header.sequenceId = peer_delay->last_pdelay_req_event_sequence_number;
    header.controlField = PTP_ALL_OTHERS;
    header.logMessageInterval = 0x7f;

    if (peer_delay->requester_state == PEER_DELAY_PTP_OBJCT_ACTIVE || peer_delay->requester_state == PEER_DELAY_PTP_OBJCT_WAIT_TX_DONE) {
        vtss_ptp_peer_delay_delete(peer_delay);
    }

    if (peer_delay->requester_state != PEER_DELAY_PTP_OBJCT_ACTIVE && peer_delay->requester_state != PEER_DELAY_PTP_OBJCT_WAIT_TX_DONE) {
        peer_delay->requester_state = PEER_DELAY_PTP_OBJCT_ACTIVE;
        peer_delay->responder_state = PEER_DELAY_PTP_OBJCT_ACTIVE;
        T_DG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"state %s", state2name(peer_delay->requester_state));
        memcpy(&peer_delay->mac, &ptp_dest->mac, sizeof(mesa_mac_addr_t)); /* used to refresh the mac header before each transmission */
        peer_delay->pdel_req_buf.size =vtss_1588_packet_tx_alloc(&peer_delay->pdel_req_buf.handle, &peer_delay->pdel_req_buf.frame, ENCAPSULATION_SIZE + PACKET_SIZE);
        T_DG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"handle %p", peer_delay->pdel_req_buf.handle);
        peer_delay->pdel_req_buf.header_length = vtss_1588_pack_encap_header(peer_delay->pdel_req_buf.frame, NULL, ptp_dest, P_DELAY_REQ_PACKET_LENGTH, true, peer_delay->clock->localClockId);
        /* no tagging on Peer Delay messages */
        peer_delay->pdel_req_buf.tag.tpid = 0;
        peer_delay->pdel_req_buf.tag.vid = 0;
        peer_delay->pdel_req_buf.tag.pcp = 0;
        peer_delay->pdel_req_buf.size = peer_delay->pdel_req_buf.header_length + P_DELAY_REQ_PACKET_LENGTH;
        vtss_ptp_pack_msg_pdelay_xx(peer_delay->pdel_req_buf.frame + peer_delay->pdel_req_buf.header_length, &header, &originTimestamp, &reserved);
        peer_delay->pdel_req_buf.hw_time = 0;
        T_DG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"frame %p", peer_delay->pdel_req_buf.frame);
        vtss_init_ptp_timer(&peer_delay->pdelay_req_timer, vtss_ptp_peer_delay_req_timer, peer_delay);
        T_DG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"timer");
        to = PTP_LOG_TIMEOUT(peer_delay->ptp_port->portDS.status.s_802_1as.currentLogPDelayReqInterval) + (vtss_ptp_get_rand(&peer_delay->random_seed)%16);
        vtss_ptp_timer_start(&peer_delay->pdelay_req_timer, to, false);
        T_DG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"time %d", to);
        if (port_uses_two_step(peer_delay)) {
            peer_delay->pdel_req_buf.msg_type = VTSS_PTP_MSG_TYPE_2_STEP;
            peer_delay->pdel_req_ts_context.cb_ts = vtss_ptp_peer_delay_req_event_transmitted;
            peer_delay->pdel_req_ts_context.context = peer_delay;
            peer_delay->pdel_req_buf.ts_done = &peer_delay->pdel_req_ts_context;
            /* prepare followup packet*/
            peer_delay->follow_buf.size = vtss_1588_packet_tx_alloc(&peer_delay->follow_buf.handle, &peer_delay->follow_buf.frame, ENCAPSULATION_SIZE + PACKET_SIZE);
            peer_delay->follow_buf.header_length = vtss_1588_pack_encap_header(peer_delay->follow_buf.frame, NULL, ptp_dest, P_DELAY_RESP_FOLLOW_UP_PACKET_LENGTH, false, peer_delay->clock->localClockId);
            /* no tagging on Peer Delay messages */
            peer_delay->follow_buf.tag.tpid = 0;
            peer_delay->follow_buf.tag.vid = 0;
            peer_delay->follow_buf.tag.pcp = 0;
            peer_delay->follow_buf.size = peer_delay->follow_buf.header_length + P_DELAY_RESP_FOLLOW_UP_PACKET_LENGTH;
            header.messageType = PTP_MESSAGE_TYPE_P_DELAY_RESP_FOLLOWUP;
            clearFlag(header.flagField[0], PTP_TWO_STEP_FLAG);
            header.controlField = PTP_ALL_OTHERS;
            vtss_ptp_pack_msg_pdelay_xx(peer_delay->follow_buf.frame + peer_delay->follow_buf.header_length, &header, &originTimestamp, &reserved);
            peer_delay->follow_buf.hw_time = 0;
            peer_delay->follow_buf.ts_done = NULL;

        } else { /* one step */
            peer_delay->pdel_req_buf.msg_type = VTSS_PTP_MSG_TYPE_CORR_FIELD; /* one-step PDelay_Req packet using correction field update */
            peer_delay->pdel_req_buf.ts_done = NULL;
            peer_delay->follow_buf.handle = 0;
        }
    } else {
        T_DG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"state %s", state2name(peer_delay->requester_state));
        peer_delay->requester_state = PEER_DELAY_PTP_OBJCT_ACTIVE;
        peer_delay->responder_state = PEER_DELAY_PTP_OBJCT_ACTIVE;
        T_WG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"Could not re-create Peer delay process");
    }
    init_pdelay_measurements(peer_delay);
    peer_delay->ptp_port->portDS.status.peer_delay_ok = false;
    peer_delay->peerDelayNotMeasured = true;










}
/*
 * delete a PTP Peer Delay instance
 * free packet buffer(s)
 */
void vtss_ptp_peer_delay_delete(PeerDelayData *peer_delay)
{
    T_DG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"state %s", state2name(peer_delay->requester_state));
    if (peer_delay->requester_state == PEER_DELAY_PTP_OBJCT_ACTIVE || peer_delay->requester_state == PEER_DELAY_PTP_OBJCT_WAIT_TX_DONE) {
        if (peer_delay->pdel_req_buf.handle) {
            T_IG(VTSS_TRACE_GRP_PTP_BASE_MASTER,"buf.handle %p", peer_delay->pdel_req_buf.handle);
            vtss_1588_packet_tx_free(&peer_delay->pdel_req_buf.handle);
        }
        if (peer_delay->follow_buf.handle) {
            T_IG(VTSS_TRACE_GRP_PTP_BASE_MASTER,"buf.handle %p", peer_delay->follow_buf.handle);
            vtss_1588_packet_tx_free(&peer_delay->follow_buf.handle);
        }

        vtss_ptp_timer_stop(&peer_delay->pdelay_req_timer);
        peer_delay->requester_state = PEER_DELAY_PTP_OBJCT_PASSIVE;
        peer_delay->responder_state = PEER_DELAY_PTP_OBJCT_PASSIVE;
        peer_delay->ptp_port->portDS.status.peer_delay_ok = true;
        peer_delay->ptp_port->portDS.status.s_802_1as.isMeasuringDelay =  FALSE;




        T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"delete peer delay");
        T_DG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"state %s", state2name(peer_delay->requester_state));
    } else {
        T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"Peer Delay not active");
    }
}

/*
 * Peer Delay Requester part
 ***************************
 *
 * delayCalc
 *      if (t1_valid && t2_t3_valid && t4_valid && !peerDelayNotMeasured)
 *          t4minust1 = t4 - t1
 *          meanpathdelay = t4 - t1 -CF (t3-t2 is held in the CF)
 *          filter
 *          if (valid delay) peer_delay_ok
 *          else peerDelayNotMeasured.
 *
 *  peerDelayNotMeasured usage:
 *      Set to false when a PdelayReq is issued, set to false during the process if an error is detected.
 *     peerDelayNotMeasured is set if no peer node answers, or if more than one node answers
 *                            or the calculated mean path delay is > neighborPropDelayThresh
 * Requester sequence flow:
 * event        action
 * init         peerDelayNotMeasured
 * timer        if (missed responses) peerDelayNotMeasured
 *              if (peerDelayNotMeasured) ^asCapable
 *              else asCapable
 *              ^t1_valid, ^t2_t3_valid, ^t4_valid
 *              ^peerDelayNotMeasured
 *              1-step: read t1;send Pdelay_Req, t1_valid
 *              2-step: send Pdelay_Req
 *
 * event_trans  if (^peerDelayNotMeasured)
 *                  if (t1_valid) peerDelayNotMeasured (more than 1 timesstamps received)
 *                  else save t1; t1_valid; delayCalc
 *
 * delayResp    if (^peerDelayNotMeasured)
 *                  if (t4_valid) peerDelayNotMeasured (more than 1 PdelayResp received)
 *                  else save t2 and t4; t4_valid
 *                      if (response is 1-step) t3_t2 is included in CF; t3_t2_valid; delayCalc
 *                      else wait_follow
 *
 * followup     if (^peerDelayNotMeasured)
 *                  if (wait_follow) save t3; t3_t2 = t3 - t2; delayCalc
 *                  else peerDelayNotMeasured (more than 1 followup received)
 */


















































































/*
 * PTP peer delay request timer
 * update sequence number
 * send packet
 * mark packet buffer as in use
 * increment sequence number.
 * start timer
 */
/*lint -esym(459, vtss_ptp_peer_delay_req_timer) */
void vtss_ptp_peer_delay_req_timer(vtss_timer_handle_t timer, void *m)
{
    PeerDelayData *peer_delay = (PeerDelayData *)m;
    mesa_timeinterval_t new_cf = 0LL;
    u32 to;
    T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"Port mask = " VPRI64x ", state %s",peer_delay->ptp_port->port_mask, state2name(peer_delay->requester_state));




    u32 cur_set_time_count = vtss_local_clock_set_time_count_get(peer_delay->clock->localClockId);
    if (cur_set_time_count != peer_delay->requester_set_time_count) {
        peer_delay->requester_set_time_count = cur_set_time_count;
        T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"set_time_count changed to %d", cur_set_time_count);





    }
    if (++peer_delay->pdelay_missed_response_cnt > peer_delay->ptp_port->port_config->c_802_1as.allowedLostResponses+1) {
        T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"peerDelayNotMeasured = true");
        peer_delay->peerDelayNotMeasured = true;
        peer_delay->clock->ssm.servo->delay_filter_reset(peer_delay->ptp_port->portDS.status.portIdentity.portNumber);  /* clears one-way P2P delay filter */
        peer_delay->current_measure_status |= PEER_DELAY_MISSED_RESPONSE_ERROR;
        peer_delay->pdelay_missed_response_cnt--;
        peer_delay->ptp_port->port_statistics.pdelayAllowedLostResponsesExceededCount++;
    }
    bool new_delay_ok = peer_delay->peerDelayNotMeasured ? false : true;
    if (peer_delay->ptp_port->portDS.status.peer_delay_ok != new_delay_ok) {
        peer_delay->ptp_port->portDS.status.peer_delay_ok = new_delay_ok;



        T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"port %d: peer_delay_ok changed to %s", peer_delay->ptp_port->portDS.status.portIdentity.portNumber, new_delay_ok ? "true" : "false");
    }









    peer_delay->ptp_port->portDS.status.s_802_1as.isMeasuringDelay =  peer_delay->t2_t3_valid ? TRUE : FALSE;
    if (peer_delay->current_measure_status != 0) {
        show_measure_status(peer_delay->current_measure_status);
        if (!peer_delay->t2_t3_valid || peer_delay->waitingForPdelayRespFollow) {
        peer_delay->ptp_port->port_statistics.rxPTPPacketDiscardCount++;
        }
    }
    peer_delay->current_measure_status = 0;
    T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"Port mask = " VPRI64x ", 802.1as capable %s", peer_delay->ptp_port->port_mask, peer_delay->ptp_port->portDS.status.s_802_1as.asCapable ? "TRUE" : "FALSE");
    T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"pdelay_missed_response_cnt %d", peer_delay->pdelay_missed_response_cnt);
    T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"state %s", state2name(peer_delay->requester_state));
    switch (peer_delay->requester_state) {
        case PEER_DELAY_PTP_OBJCT_WAIT_TX_DONE:
            T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"Missed Event transmitted event, sequence no = %d",peer_delay->last_pdelay_req_event_sequence_number);
            /* fall through is intended */
        case PEER_DELAY_PTP_OBJCT_ACTIVE:
            to = PTP_LOG_TIMEOUT(0);
            if (peer_delay->ptp_port->p2p_state) {  /* transmit PdelayReq if link is up */
                if (peer_delay->ptp_port->portDS.status.s_802_1as.currentLogPDelayReqInterval != 127) {
                    if (peer_delay->ptp_port->portDS.status.s_802_1as.currentLogPDelayReqInterval == 126) {
                        T_EG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"currentLogPDelayReqInterval == 126 should never occur");
                    }

                    to = PTP_LOG_TIMEOUT(peer_delay->ptp_port->portDS.status.s_802_1as.currentLogPDelayReqInterval) + (vtss_ptp_get_rand(&peer_delay->random_seed)%16);
                    peer_delay->t1.sec_msb = 0;
                    peer_delay->t1.seconds = 0;
                    peer_delay->t1.nanoseconds = 0;
                    peer_delay->t4.sec_msb = 0;
                    peer_delay->t4.seconds = 0;
                    peer_delay->t4.nanoseconds = 0;
                    peer_delay->t2.sec_msb = 0;
                    peer_delay->t2.seconds = 0;
                    peer_delay->t2.nanoseconds = 0;
                    peer_delay->t3.sec_msb = 0;
                    peer_delay->t3.seconds = 0;
                    peer_delay->t3.nanoseconds = 0;
                    peer_delay->t1_valid = false;
                    peer_delay->t2_t3_valid = false;
                    peer_delay->t4_valid = false;
                    peer_delay->peerDelayNotMeasured = false;
                    vtss_tod_pack16(++peer_delay->last_pdelay_req_event_sequence_number,peer_delay->pdel_req_buf.frame + peer_delay->pdel_req_buf.header_length + PTP_MESSAGE_SEQUENCE_ID_OFFSET);
                    // domain number update
                    peer_delay->pdel_req_buf.frame[peer_delay->pdel_req_buf.header_length + PTP_MESSAGE_DOMAIN_OFFSET]= peer_delay->clock->clock_init->cfg.domainNumber;
                    peer_delay->waitingForPdelayRespFollow = false;

                    if (port_uses_two_step(peer_delay)) {
                        peer_delay->requester_state = PEER_DELAY_PTP_OBJCT_WAIT_TX_DONE;
                        /* in 2 step always do asymmetry in SW */
                        vtss_ptp_pack_correctionfield(peer_delay->pdel_req_buf.frame + peer_delay->pdel_req_buf.header_length, &peer_delay->ptp_port->port_config->delayAsymmetry);
                    } else {
                        vtss_ptp_pack_correctionfield(peer_delay->pdel_req_buf.frame + peer_delay->pdel_req_buf.header_length, &new_cf);
                        vtss_local_clock_time_get(&peer_delay->t1, 0, &peer_delay->pdel_req_buf.hw_time);
                        vtss_ptp_pack_timestamp(peer_delay->pdel_req_buf.frame + peer_delay->pdel_req_buf.header_length, &peer_delay->t1);
                        peer_delay->t1_valid = true;
                    }

                    vtss_1588_pack_eth_header(peer_delay->pdel_req_buf.frame, peer_delay->mac);
                    if (!vtss_1588_tx_msg(peer_delay->ptp_port->port_mask, &peer_delay->pdel_req_buf, peer_delay->clock->localClockId)) {
                        T_WG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"PDelay_Req message transmission failed");
                    } else {
                        T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"sent PDelay_Req message");
                        peer_delay->ptp_port->port_statistics.txPdelayRequestCount++;
                    }
                } else {
                    // requested to stop sending PdelayRequests
                    peer_delay->t2_t3_valid = false;
                    T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"peerDelayNotMeasured = true");
                    peer_delay->peerDelayNotMeasured = true;
                }
                vtss_ptp_timer_start(timer, to + (vtss_ptp_get_rand(&peer_delay->random_seed)%16), false);
            } else {
                T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"peerDelayNotMeasured = true");
                peer_delay->peerDelayNotMeasured = true;
                peer_delay->clock->ssm.servo->delay_filter_reset(peer_delay->ptp_port->portDS.status.portIdentity.portNumber);  /* clears one-way P2P delay filter */
            }
            break;
        default:
            T_WG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"invalid state");
            break;
    }
}

/*
 * 2-step Pdelay_Req packet event transmitted. Save tx_time
 *
 */
/*lint -esym(459, vtss_ptp_peer_delay_req_event_transmitted) */
static void vtss_ptp_peer_delay_req_event_transmitted(void *context, uint portnum, u32 tx_time)
{
    PeerDelayData *peer_delay = (PeerDelayData *)context;
    if (peer_delay != 0) {
        if (peer_delay->t1_valid) {  // more than one t1 on the same Pdelay_Req
            T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"peerDelayNotMeasured = true");
            peer_delay->peerDelayNotMeasured = true;
            peer_delay->current_measure_status |= PEER_DELAY_MORE_THAN_ONE_T1_ERROR;
        }
        if (!peer_delay->peerDelayNotMeasured) {
            /*save tx time */
            peer_delay->requester_state = PEER_DELAY_PTP_OBJCT_ACTIVE;
            vtss_local_clock_convert_to_time( tx_time, &peer_delay->t1, peer_delay->clock->localClockId);
            peer_delay->t1_valid = true;
            T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"Port %d: tx_time %u", peer_delay->ptp_port->portDS.status.portIdentity.portNumber, tx_time);
            ptp_peer_delay_calc(peer_delay);
        }
    } else {
        T_WG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"unexpected timestamp event");
    }
}

/*
 * Handle PDelay_Resp from a responder
 *
 */
bool vtss_ptp_peer_delay_resp(PeerDelayData *peer_delay, ptp_tx_buffer_handle_t *tx_buf, MsgHeader *header)
{
    bool forwarded = false;
    u8 *buf = tx_buf->frame + tx_buf->header_length;
    vtss_appl_ptp_port_identity port_id;
    char str1[40];
    char str2[40];
    char str3[40];

    VTSS_ASSERT(peer_delay != NULL);
    VTSS_ASSERT(tx_buf != NULL);
    VTSS_ASSERT(header != NULL);
    if (peer_delay->ptp_port != NULL && peer_delay->ptp_port->port_config->delayMechanism == DELAY_MECH_P2P) {
        if ((peer_delay->requester_state == PEER_DELAY_PTP_OBJCT_WAIT_TX_DONE || peer_delay->requester_state == PEER_DELAY_PTP_OBJCT_ACTIVE)) {
            if (peer_delay->t4_valid) {  // more than one PDelay_Resp on the same Pdelay_Req
                T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"peerDelayNotMeasured = true");
                peer_delay->peerDelayNotMeasured = true;
                peer_delay->current_measure_status |= PEER_DELAY_MORE_THAN_ONE_RESPONDER_ERROR;
            }
            if(!peer_delay->peerDelayNotMeasured) {
                T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"state %s", state2name(peer_delay->requester_state));
                if (peer_delay->ptp_port->p2p_state) {
                    vtss_ptp_unpack_msg_pdelay_xx(buf, &peer_delay->t2, &port_id);
                    if ( header->sequenceId == peer_delay->last_pdelay_req_event_sequence_number
                            && (!PortIdentitycmp(&port_id, &peer_delay->ptp_port->portDS.status.portIdentity)
                               )) {
                        vtss_local_clock_convert_to_time(tx_buf->hw_time,&peer_delay->t4,peer_delay->clock->localClockId);
                        peer_delay->t4_valid = true;
                        peer_delay->correctionField  = header->correctionField;
                        if (!getFlag(header->flagField[0], PTP_TWO_STEP_FLAG)) {
                            peer_delay->waitingForPdelayRespFollow = false;
                            peer_delay->t2_t3_valid = true;




                            ptp_peer_delay_calc(peer_delay);
                            T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"t4 = %s, t1 = %s, corr = %s", TimeStampToString(&peer_delay->t4,str1),
                                TimeStampToString(&peer_delay->t1,str2),
                                vtss_tod_TimeInterval_To_String(&header->correctionField,str3,'.'));
                        } else {
                            peer_delay->waitingForPdelayRespFollow = true;
                        }
                    } else {
                        ++peer_delay->pdelay_event_sequence_errors;
                        T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"handlePdelayResp: unexpected sequence no or requesting port id");
                        T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"received sequence no %d, expected sequence no %d", header->sequenceId, peer_delay->last_pdelay_req_event_sequence_number);
                        T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"requesting port id %s, my port id %s",ClockIdentityToString (port_id.clockIdentity, str1),
                            ClockIdentityToString (peer_delay->ptp_port->portDS.status.portIdentity.clockIdentity, str2));
                        return forwarded;
                    }
                }
            }
        } else {
            T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"unexpected PdelayResp, state = %s", state2name(peer_delay->requester_state));
        }
    } else {
        T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"unexpected PdelayResp");
    }
    return forwarded;
}

/*
 * Handle PDelay_Resp_Follow_Up from a responder
 *
 */
bool vtss_ptp_peer_delay_resp_follow_up(PeerDelayData *peer_delay, ptp_tx_buffer_handle_t *tx_buf, MsgHeader *header)
{
    bool forwarded = false;
    mesa_timeinterval_t t3minust2;
    u8 *buf = tx_buf->frame + tx_buf->header_length;
    vtss_appl_ptp_port_identity port_id;

    T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"state %s", state2name(peer_delay->requester_state));
    vtss_ptp_unpack_msg_pdelay_xx(buf, &peer_delay->t3, &port_id);
    if ((peer_delay->requester_state == PEER_DELAY_PTP_OBJCT_WAIT_TX_DONE || peer_delay->requester_state == PEER_DELAY_PTP_OBJCT_ACTIVE) &&
        !peer_delay->peerDelayNotMeasured)  {
        if (( header->sequenceId == peer_delay->last_pdelay_req_event_sequence_number) &&
                !PortIdentitycmp(&port_id, &peer_delay->ptp_port->portDS.status.portIdentity)) {
            if (peer_delay->waitingForPdelayRespFollow) {
                peer_delay->waitingForPdelayRespFollow = false;
                // add t3-t2 to CF
                vtss_tod_sub_TimeInterval(&t3minust2, &peer_delay->t3, &peer_delay->t2);
                peer_delay->correctionField += header->correctionField;
                peer_delay->correctionField += t3minust2;
                peer_delay->t2_t3_valid = true;
                ptp_peer_delay_calc(peer_delay);
            } else {
                if (peer_delay->t2_t3_valid) {
                    T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"peerDelayNotMeasured = true");
                    peer_delay->peerDelayNotMeasured = true;
                    peer_delay->current_measure_status |= PEER_DELAY_MORE_THAN_ONE_FOLLOWUP_ERROR;
                }
            }
        } else {
            ++peer_delay->pdelay_event_sequence_errors;
        }
    } else {
        T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"unexpected PdelayRespFollowUp, state = %s", state2name(peer_delay->requester_state));
    }

    return forwarded;
}

/*
 * Peer Delay Responder part
 ***************************
 */
/*
 * Handle delay request from a requester
 */
bool vtss_ptp_peer_delay_req(PeerDelayData *peer_delay, ptp_tx_buffer_handle_t *tx_buf)
{
    mesa_timestamp_t receive_timestamp = {0,0,0};
    bool forwarded = false;
    u8 *buf = tx_buf->frame + tx_buf->header_length;

    T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"responder state %s", state2name(peer_delay->responder_state));
    if (peer_delay->responder_state ==  PEER_DELAY_PTP_OBJCT_ACTIVE) {
        if (peer_delay->ptp_port->p2p_state) {
            T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"Port mask = " VPRI64x ", state %s",peer_delay->ptp_port->port_mask, state2name(peer_delay->responder_state));
            /*send pdelay resp */
            vtss_1588_prepare_tx_buffer(tx_buf, P_DELAY_RESP_PACKET_LENGTH);
            buf = tx_buf->frame + tx_buf->header_length;
            u32 cur_set_time_count = vtss_local_clock_set_time_count_get(peer_delay->clock->localClockId);
            if (cur_set_time_count != peer_delay->responder_set_time_count) {
                peer_delay->responder_set_time_count = cur_set_time_count;
                T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"set_time_count changed to %d", cur_set_time_count);
                if (peer_delay->clock->clock_init->cfg.profile == VTSS_APPL_PTP_PROFILE_IEEE_802_1AS) {
                    return forwarded; // dont respond if time has been changed since last pdelay_req, because this wil result in a wrong rateRatio
                }
            }



            peer_delay->t2_cnt = tx_buf->hw_time;

            /* update encapsulation header */
            vtss_1588_update_encap_header(tx_buf->frame, ((buf[PTP_MESSAGE_FLAG_FIELD_OFFSET] & PTP_UNICAST_FLAG) != 0), true, P_DELAY_RESP_PACKET_LENGTH, peer_delay->clock->localClockId);
            /* update messageType and length */
            buf[PTP_MESSAGE_MESSAGE_TYPE_OFFSET] &= 0xf0;
            buf[PTP_MESSAGE_MESSAGE_TYPE_OFFSET] |= (PTP_MESSAGE_TYPE_P_DELAY_RESP & 0x0f);
            buf[PTP_MESSAGE_CONTROL_FIELD_OFFSET] = PTP_ALL_OTHERS;  /* control */
            buf[PTP_MESSAGE_LOGMSG_INTERVAL_OFFSET] = 0x7f;  /* logMessageInterval */
            vtss_ptp_pack_timestamp(buf, &receive_timestamp);
            /* requestingPortIdentity = received sourcePortIdentity */
            memcpy(&buf[PTP_MESSAGE_REQ_PORT_ID_OFFSET], &buf[PTP_MESSAGE_SOURCE_PORT_ID_OFFSET],
                   sizeof(vtss_appl_ptp_port_identity));
            /* update sourcePortIdentity from static allocated PDelay_Req buffer*/
            memcpy(&buf[PTP_MESSAGE_SOURCE_PORT_ID_OFFSET],
                   peer_delay->pdel_req_buf.frame + peer_delay->pdel_req_buf.header_length + PTP_MESSAGE_SOURCE_PORT_ID_OFFSET,
                   sizeof(vtss_appl_ptp_port_identity));

            forwarded = true;

            if (port_uses_two_step(peer_delay)) {
                setFlag(buf[PTP_MESSAGE_FLAG_FIELD_OFFSET], PTP_TWO_STEP_FLAG);
                /* requestingPortIdentity = received sourcePortIdentity, also used in followup */
                memcpy(peer_delay->follow_buf.frame + peer_delay->follow_buf.header_length + PTP_MESSAGE_REQ_PORT_ID_OFFSET,
                       &buf[PTP_MESSAGE_REQ_PORT_ID_OFFSET], sizeof(vtss_appl_ptp_port_identity));
                /* save sequence number in the followup buffer */
                memcpy(peer_delay->follow_buf.frame + peer_delay->follow_buf.header_length + PTP_MESSAGE_SEQUENCE_ID_OFFSET,
                       &buf[PTP_MESSAGE_SEQUENCE_ID_OFFSET], sizeof(u16));
                /* save correction field in the followup buffer */
                memcpy(peer_delay->follow_buf.frame + peer_delay->follow_buf.header_length + PTP_MESSAGE_CORRECTION_FIELD_OFFSET,
                       &buf[PTP_MESSAGE_CORRECTION_FIELD_OFFSET], sizeof(mesa_timeinterval_t));
                /* clear correction field in the Pdelay_Resp buffer */
                memset(&buf[PTP_MESSAGE_CORRECTION_FIELD_OFFSET], 0, sizeof(mesa_timeinterval_t));
                T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"sequenceId %d", (buf[PTP_MESSAGE_SEQUENCE_ID_OFFSET]<<8) + buf[PTP_MESSAGE_SEQUENCE_ID_OFFSET+1]);
                peer_delay->pdel_resp_ts_context.cb_ts = vtss_ptp_peer_delay_resp_event_transmitted;
                peer_delay->pdel_resp_ts_context.context = peer_delay;
                tx_buf->ts_done = &peer_delay->pdel_resp_ts_context;

                tx_buf->msg_type = VTSS_PTP_MSG_TYPE_2_STEP;
                peer_delay->responder_state = PEER_DELAY_PTP_OBJCT_WAIT_TX_DONE;
                T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"2-step, context %p", peer_delay);
            } else {
                tx_buf->msg_type = VTSS_PTP_MSG_TYPE_CORR_FIELD;
                T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"1-step");
            }

            /* no tagging on Peer Delay messages */
            tx_buf->tag.tpid = 0;
            tx_buf->tag.vid = 0;
            tx_buf->tag.pcp = 0;
            tx_buf->size = tx_buf->header_length + P_DELAY_RESP_PACKET_LENGTH;
            if (!vtss_1588_tx_msg(peer_delay->ptp_port->port_mask, tx_buf, peer_delay->clock->localClockId)) {
                T_WG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"Delay_Resp message transmission failed");
            } else {
                T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"sent PDelay_Resp message");
                peer_delay->ptp_port->port_statistics.txPdelayResponseCount++;
            }
        }
    } else {
        if (peer_delay->responder_state == PEER_DELAY_PTP_OBJCT_WAIT_TX_DONE) {
            peer_delay->responder_state = PEER_DELAY_PTP_OBJCT_ACTIVE;
        }
        T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"unexpected Pdelay_Req");
    }
    return forwarded;
}

/*
 * 2-step PDelay_Resp packet event transmitted. Send followup
 *
 */
/*lint -esym(459, vtss_ptp_peer_delay_resp_event_transmitted) */
static void vtss_ptp_peer_delay_resp_event_transmitted(void *context, uint portnum, u32 tx_time)
{
    PeerDelayData *peer_delay = (PeerDelayData *)context;



    T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"context %p, state %x, port %u", peer_delay, peer_delay->responder_state, portnum);
    T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"responder state %s", state2name(peer_delay->responder_state));
    if (peer_delay != 0 && peer_delay->responder_state == PEER_DELAY_PTP_OBJCT_WAIT_TX_DONE) {
        /*send followup */
        peer_delay->responder_state = PEER_DELAY_PTP_OBJCT_ACTIVE;
        u32 cur_set_time_count = vtss_local_clock_set_time_count_get(peer_delay->clock->localClockId);
        if (cur_set_time_count != peer_delay->responder_set_time_count) {
            peer_delay->responder_set_time_count = cur_set_time_count;
            T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"set_time_count changed to %d", cur_set_time_count);
            return; // don't send followup if time has been set since the pdelay_resp was transmitted
        }




        u32 residenceTime;
        mesa_timeinterval_t r;
        vtss_1588_ts_cnt_sub(&residenceTime, tx_time, peer_delay->t2_cnt);
        vtss_1588_ts_cnt_to_timeinterval(&r,residenceTime);
        // add residence time to CF
        vtss_ptp_update_correctionfield(peer_delay->follow_buf.frame + peer_delay->follow_buf.header_length, &r);

        vtss_1588_pack_eth_header(peer_delay->follow_buf.frame, peer_delay->mac);
        if (!vtss_1588_tx_msg(peer_delay->ptp_port->port_mask, &peer_delay->follow_buf, peer_delay->clock->localClockId)) {
            T_WG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"Follow_Up message transmission failed");
        } else {
            T_NG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"sent follow_Up message");
            peer_delay->ptp_port->port_statistics.txPdelayResponseFollowUpCount++;
        }
    } else {
        T_IG(VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY,"unexpected timestamp event");
    }
}
