/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "vtss_ptp_api.h"
#include "vtss_ptp_os.h"
#include "vtss_ptp_802_1as.hxx"
#include "vtss_ptp_pack_unpack.h"
#include "vtss_tod_api.h"

static const u32 ORGANIZATION_ID = 0x0080C2;
static const u32 ORGANIZATION_SUBTYPE = 0x02;
static const u32 ORGANIZATION_SUBTYPE_FOLLOW_UP = 0x01;
/*
 * Forward declarations
 */

/*
 * private functions
 */

/**
 * pack and send signalling message: Message Interval Request.
 */
void issue_message_interval_request(PtpPort_t *ptpPort, i8 txAnv, i8 txSyv, i8 txMpr)
{
    ptp_clock_t *ptpClock = ptpPort->parent;
    TLV tlv;
    mesa_rc rc = VTSS_RC_ERROR;
    u8 tlv_value[12];
    u16 packetLength = SIGNALLING_MIN_PACKET_LENGTH + MESSAGE_INTERVAL_REQUEST_TLV_LENGTH;
    u8 *frame;
    vtss_ptp_tag_t tag;
    size_t buffer_size;
    size_t header_size;
    vtss_appl_ptp_port_identity targetPortIdentity;
    buffer_size = vtss_1588_prepare_general_packet(&frame, &ptpClock->ptp_primary, packetLength, &header_size, ptpClock->localClockId);
    vtss_1588_tag_get(get_tag_conf(ptpClock, ptpPort), ptpClock->localClockId, &tag);
    if (buffer_size) {
        memset(&targetPortIdentity, 0xff, sizeof(targetPortIdentity));
        vtss_ptp_pack_signalling(frame + header_size,&targetPortIdentity, &ptpPort->portDS.status.portIdentity, ptpClock, ptpPort, ptpPort->port_last_message_interval_request_sequence_number++, packetLength, 0);
        T_DG(VTSS_TRACE_GRP_PTP_BASE_802_1AS, "sent message_interval_request message");
        /* Insert TLV field */
        tlv.tlvType = TLV_ORGANIZATION_EXTENSION;
        tlv.lengthField = 12;
        tlv.valueField = tlv_value;
        vtss_tod_pack24(ORGANIZATION_ID, &tlv_value[0]); // organizationid is 00-80-C2
        vtss_tod_pack24(ORGANIZATION_SUBTYPE, &tlv_value[3]); // organization subtype
        tlv_value[6] = (u8)txMpr;   //linkDelayInterval;
        tlv_value[7] = (u8)txSyv;   //timeSyncInterval;
        tlv_value[8] = (u8)txAnv;   //announceInterval;
        tlv_value[9] = 0x03;    // bit 0 = computeNeighborRateRatio, bit 1 = computeNeighborPropDelay (Table 10-15 has not been changed according to the change in bit numbering from 1..8 to 0..7)
        vtss_tod_pack16(0, &tlv_value[10]); // reserved

        rc = vtss_ptp_pack_tlv(frame+header_size+SIGNALLING_MIN_PACKET_LENGTH, (u16)buffer_size-SIGNALLING_MIN_PACKET_LENGTH, &tlv);
        if (VTSS_RC_OK == rc) {
            if (!vtss_1588_tx_general(ptpPort->port_mask,frame, header_size + packetLength, &tag))
                T_WG(VTSS_TRACE_GRP_PTP_BASE_802_1AS, "sending MessageIntervalRequest failed");
            else
                T_DG(VTSS_TRACE_GRP_PTP_BASE_802_1AS, "sent MessageIntervalRequest message");
        } else {
            T_EG(VTSS_TRACE_GRP_PTP_BASE_802_1AS, "Transmit buffer too small");
            vtss_1588_release_general_packet(&frame);
        }
    }
}

/**
 * unpack the Message Interval Request, and update the current message intervals.
 */
void vtss_ptp_tlv_organization_extension_process(TLV *tlv, PtpPort_t *ptpPort)
{
    u32 organizationId;
    u32 organizationSubType;
    switch (tlv->tlvType) {
        case TLV_ORGANIZATION_EXTENSION:
            organizationId = vtss_tod_unpack24(&tlv->valueField[0]);
            organizationSubType = vtss_tod_unpack24(&tlv->valueField[3]);
            if (organizationId == ORGANIZATION_ID && organizationSubType == ORGANIZATION_SUBTYPE) {
                // set interval parameters
                ptp_802_1as_set_current_message_interval(ptpPort, tlv->valueField[8], tlv->valueField[7], tlv->valueField[6]);
                T_IG(VTSS_TRACE_GRP_PTP_BASE_802_1AS, "Setting message intervals:  Anv %d, Syv %d, Mpr %d",
                     (i8)tlv->valueField[8], (i8)tlv->valueField[7], (i8)tlv->valueField[6]);
            } else {
                T_WG(VTSS_TRACE_GRP_PTP_BASE_802_1AS, "Unknown organizationId 0x%x or organizationSubType 0x%x",
                     organizationId, organizationSubType);
            }
            break;
        default:
            T_WG(VTSS_TRACE_GRP_PTP_BASE_802_1AS, "Unknown signalling TLV type received: tlvType %d", tlv->tlvType);
            break;
    }
}

size_t vtss_ptp_tlv_follow_up_tlv_insert(u8 *tx_buf, size_t buflen, ptp_follow_up_tlv_info_t *follow_up_info)
{
    TLV tlv;
    u8 tlv_value[FOLLOW_UP_TLV_LENGTH];
    /* Insert TLV field */
    if (buflen >= FOLLOW_UP_TLV_LENGTH) {
        tlv.tlvType = TLV_ORGANIZATION_EXTENSION;
        tlv.lengthField = FOLLOW_UP_TLV_LENGTH -4;
        tlv.valueField = tlv_value;
        vtss_tod_pack24(ORGANIZATION_ID, &tlv_value[0]); // organizationid is 00-80-C2
        vtss_tod_pack24(ORGANIZATION_SUBTYPE_FOLLOW_UP, &tlv_value[3]); // organization subtype
        T_DG(VTSS_TRACE_GRP_PTP_BASE_802_1AS, "buflen " VPRIz ", rateOffset %d, gmTimeBaseIndicator %d", buflen, follow_up_info->cumulativeScaledRateOffset, follow_up_info->gmTimeBaseIndicator);
        vtss_tod_pack32(follow_up_info->cumulativeScaledRateOffset, &tlv_value[6]);
        vtss_tod_pack16(follow_up_info->gmTimeBaseIndicator, &tlv_value[10]);
        vtss_tod_pack32(follow_up_info->lastGmPhaseChange.scaled_ns_high, &tlv_value[12]);
        vtss_tod_pack64(follow_up_info->lastGmPhaseChange.scaled_ns_low, &tlv_value[16]);
        vtss_tod_pack32(follow_up_info->scaledLastGmFreqChange, &tlv_value[24]);
        if (VTSS_RC_OK == vtss_ptp_pack_tlv(tx_buf, buflen, &tlv)) {
            return FOLLOW_UP_TLV_LENGTH;
        }
    }
    return 0;
}

mesa_rc vtss_ptp_tlv_follow_up_tlv_process(u8 *tx_buf, size_t buflen, ptp_follow_up_tlv_info_t *follow_up_info)
{
    TLV tlv;
    const u8 *tlv_value;
    u32 org_id, sub_type;
    mesa_rc rc = VTSS_RC_ERROR;
    // Decode Follow_Up information TLV
    if (buflen >= FOLLOW_UP_TLV_LENGTH) {
        if (VTSS_RC_OK == vtss_ptp_unpack_tlv(tx_buf, buflen, &tlv)) {
            tlv_value = tlv.valueField;
            T_D("process Follow_up Tlv extension with type %d and length %d", tlv.tlvType, tlv.lengthField);
            if (tlv.tlvType == TLV_ORGANIZATION_EXTENSION && tlv.lengthField == FOLLOW_UP_TLV_LENGTH -4) {
                org_id = vtss_tod_unpack24(&tlv_value[0]); // organizationid is 00-80-C2
                sub_type = vtss_tod_unpack24(&tlv_value[3]); // organization subtype
                if (org_id == ORGANIZATION_ID && sub_type == ORGANIZATION_SUBTYPE_FOLLOW_UP) {
                    follow_up_info->cumulativeScaledRateOffset = vtss_tod_unpack32(&tlv_value[6]);
                    follow_up_info->gmTimeBaseIndicator = vtss_tod_unpack16(&tlv_value[10]);
                    follow_up_info->lastGmPhaseChange.scaled_ns_high = vtss_tod_unpack32(&tlv_value[12]);
                    follow_up_info->lastGmPhaseChange.scaled_ns_low = vtss_tod_unpack64(&tlv_value[16]);
                    follow_up_info->scaledLastGmFreqChange = vtss_tod_unpack32(&tlv_value[24]);
                    rc = VTSS_RC_OK;
                    T_D("Follow_Up Tlv: cumulativeScaledRateOffset %d, gmTimeBaseIndicator %d, lastGmPhaseChange high %d, low %" PRIu64 ", scaledLastGmFreqChange % d", follow_up_info->cumulativeScaledRateOffset,
                              follow_up_info->gmTimeBaseIndicator,
                              follow_up_info->lastGmPhaseChange.scaled_ns_high,
                              follow_up_info->lastGmPhaseChange.scaled_ns_low,
                              follow_up_info->scaledLastGmFreqChange);
                } else {
                    T_W("Unsupported Follow_Up Tlv subtype");
                }
            } else {
                T_W("Unsupported Follow_Up Tlv extension");
            }
        }
    } else {
        T_W("Follow_Up Tlv extension size error");
    }
    return rc;
}
