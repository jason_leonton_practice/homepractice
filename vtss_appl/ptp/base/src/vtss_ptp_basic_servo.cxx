/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable format
 (e.g. HEX file) and only in or with products utilizing the Microsemi switch and
 PHY products.  The source code of the software may not be disclosed, transmitted
 or distributed without the prior written permission of Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all ownership,
 copyright, trade secret and proprietary rights in the software and its source code,
 including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL WARRANTIES
 OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES ARE EXPRESS,
 IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION, WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND NON-INFRINGEMENT.

*/

#define SW_OPTION_BASIC_PTP_SERVO // Basic Servo needs to be encluded for the purpose of calibration

#ifdef SW_OPTION_BASIC_PTP_SERVO

#include "vtss_ptp_basic_servo.h"
#include "vtss_ptp_os.h"
#include "vtss_ptp_local_clock.h"
#include "vtss_ptp_api.h"
#include "vtss_tod_api.h"
#include "vtss_ptp_slave.h"
#include "vtss_ptp_clock.h"
#include "misc_api.h"

#if defined(VTSS_SW_OPTION_SYNCE)
#include "synce_ptp_if.h"
#include "synce_api.h"
#endif

#define LP_FILTER_0_1_HZ_CONSTANT 3

vtss_lowpass_filter_s::vtss_lowpass_filter_s() : my_name("anonym")
{
    T_IG(VTSS_TRACE_GRP_PTP_BASE_FILTER, "Filter %s ctor", my_name);
    reset();
}
vtss_lowpass_filter_s::vtss_lowpass_filter_s(const char * name) : my_name(name)
{
    T_IG(VTSS_TRACE_GRP_PTP_BASE_FILTER, "Filter %s ctor", my_name);
    reset();
}


void vtss_lowpass_filter_s::reset(void)
{
    nsec_prev = VTSS_MAX_TIMEINTERVAL;
    y = 0;
    s_exp = 0;
    skipped = 0;
    T_IG(VTSS_TRACE_GRP_PTP_BASE_FILTER, "Filter %s reset", my_name);
}

void vtss_lowpass_filter_s::filter(vtss_timeinterval_t *value, u32 period, bool min_delay_option)
{
    /* avoid overflowing filter: not needed after value is changed to 64 bit. */
    /* crank down filter cutoff by increasing 's_exp' */
    if (s_exp < 1)
        s_exp = 1;
    else if (s_exp < period)
        ++s_exp;
    else if (s_exp > period)
        s_exp = period;

    T_DG(VTSS_TRACE_GRP_PTP_BASE_FILTER, "delay before filtering %d", VTSS_INTERVAL_NS(*value));
    T_DG(VTSS_TRACE_GRP_PTP_BASE_FILTER, "OWD filt: nsec_prev %d, s_exp " VPRI64d ", y %d", VTSS_INTERVAL_NS(nsec_prev), s_exp, VTSS_INTERVAL_NS(y));

    /* filter 'one_way_delay' */
    /* nsec_prev is used as min delay */
    if (nsec_prev > *value) {
        nsec_prev = *value;
        T_IG(VTSS_TRACE_GRP_PTP_BASE_FILTER, "new min delay %d", VTSS_INTERVAL_NS(*value));
    }
    /* optionally ignore delays > 3 * min delay */
    if (*value < nsec_prev + max_acceptable_delay_variation || !min_delay_option) {
        /* low pass filter measured delay */
        y = ((s_exp-1)*y + *value)/s_exp;
        T_IG(VTSS_TRACE_GRP_PTP_BASE_FILTER, "Filtered delay " VPRI64d ", new delay " VPRI64d ", divisor " VPRI64d, y, *value, s_exp);
        skipped = 0;
    } else {  // if too many delay measurements have been skipped, then the min_delay may be too small
        if (++skipped > 5) {
            T_IG(VTSS_TRACE_GRP_PTP_BASE_FILTER, "too many delays skipped, old min %d, new min %d", VTSS_INTERVAL_NS(nsec_prev), VTSS_INTERVAL_NS(*value));
            nsec_prev = *value;
            s_exp = 0;
            y = *value;
        }
    }
    *value = y;
    T_DG(VTSS_TRACE_GRP_PTP_BASE_FILTER, "Filter %s: delay after filtering %d", my_name, VTSS_INTERVAL_NS(*value));

}


// FIXME: The definition of displayStats is equal to the one for ptp_advanced_servo. A common definition should be made in the base class.
// 
void ptp_basic_servo::displayStats(const vtss_timeinterval_t *meanPathDelay, const vtss_timeinterval_t *offsetFromMaster,
                                   i32 observedParentClockPhaseChangeRate, u16 observedParentOffsetScaledLogVariance, i32 adj)
{
    const int SCREEN_BUFSZ = 180;
    static int start = 1;
    static char sbuf[SCREEN_BUFSZ];
    unsigned int len = 0;
    char str1 [40];
    char str2 [40];

    if (display_stats()) {
        if (start) {
            start = 0;
            T_D("one way delay, offset from mst   adj  servo parameters ->");
            (void)fflush(stdout);
        }
        memset(sbuf, ' ', SCREEN_BUFSZ);
        len += sprintf(sbuf + len,
                       "%s, %s,",
                       vtss_tod_TimeInterval_To_String(meanPathDelay, str1,0),
                       vtss_tod_TimeInterval_To_String(offsetFromMaster, str2,0));

        len += sprintf(sbuf + len,
                       " %7d,", adj);

        len += display_parm(sbuf + len);

        //len += sprintf(sbuf + len,
        //               ", %d" ", %d",
        //               observedParentClockPhaseChangeRate,
        //               observedParentOffsetScaledLogVariance);

        len += sprintf(sbuf + len,"\n");
        printf("%s", sbuf);
    }
}


vtss_rc ptp_basic_servo::force_holdover_set(BOOL enable)
{
    T_I("force_holdover_set not implemented in basic_servo");
    return VTSS_RC_OK;
}

vtss_rc ptp_basic_servo::force_holdover_get(BOOL *enable)
{
    T_I("force_holdover_get not implemented in basic_servo");
    return VTSS_RC_OK;
}

/**
 * \brief Filter reset function.
 */
void ptp_basic_servo::delay_filter_reset(int port)
{
    VTSS_ASSERT(port < my_port_count);
    if (filt_conf->delay_filter == 0) {
        /* reset 'wl' type delay filter */
        if (filt_conf->dist > 1) {
            owd_wl_filt[port].act_min_delay = VTSS_MAX_TIMEINTERVAL;
        } else {
            owd_wl_filt[port].act_min_delay = 0;
        }
        owd_wl_filt[port].actual_period = 0;
        owd_wl_filt[port].prev_delay = 0;
        owd_wl_filt[port].prev_cnt = 0;
    }
    else {
        /* reset normal type delay filter */
        owd_filt[port].reset();;
    }
}

void ptp_basic_servo::delay_calc(CapArray<ptp_clock_t *, VTSS_APPL_CAP_PTP_CLOCK_CNT> &ptpClock, int clock_inst, const mesa_timestamp_t& send_time, const mesa_timestamp_t& recv_time, vtss_timeinterval_t correction, i8 logMsgIntv)
{
    ptp_slave_t *const slave = &ptpClock[clock_inst]->ssm;
    char str[50];
    vtss_timeinterval_t t3mt2;
    vtss_timeinterval_t t4mt1;
    T_RG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"ptp_delay_calc");

    /* calc 'slave_to_master_delay' */
    vtss_tod_sub_TimeInterval(&slave->slave_to_master_delay, &recv_time, &send_time);
    slave->slave_to_master_delay -= correction;
    if(slave->statistics.enable) {
        slave_to_master_delay_stati(slave);
    }

    T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"correction %s", vtss_tod_TimeInterval_To_String (&correction, str,0));
    T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"slave_to_master_delay %s", vtss_tod_TimeInterval_To_String (&slave->slave_to_master_delay, str,0));
    T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"master_to_slave_delay %s", vtss_tod_TimeInterval_To_String (&slave->master_to_slave_delay, str,0));


    T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock %p, clock_state %s", slave->clock, clock_state_to_string (slave->clock_state));
    if ((slave->clock_state == VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKED || slave->clock_state == VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKED || slave->clock_state == VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKING ||
         slave->clock_state == VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKING || slave->clock_state == VTSS_PTP_SLAVE_CLOCK_RECOVERING) && slave->master_to_slave_delay_valid)
    {
        slave->clock->currentDS.meanPathDelay = (slave->master_to_slave_delay + slave->slave_to_master_delay)/2;
        T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"meanPathDelay: %s", vtss_tod_TimeInterval_To_String (&slave->clock->currentDS.meanPathDelay, str,0));
        vtss_tod_sub_TimeInterval(&t3mt2, &slave->delay_req_send_time, &slave->sync_receive_time);
        vtss_tod_sub_TimeInterval(&t4mt1, &slave->delay_req_receive_time, &slave->sync_tx_time);
        T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"B,%05u,%010d,%09d,%010d,%09d,%c" VPRI64Fd("010") "\n",slave->last_delay_req_event_sequence_number, recv_time.seconds, recv_time.nanoseconds, send_time.seconds, send_time.nanoseconds,
               correction >= 0 ? '+' : '-', correction>>16);
        if ((slave->clock->currentDS.meanPathDelay < 0) || (t3mt2 > (vtss_timeinterval_t)10000000*(1LL<<16)) || (t4mt1 > (vtss_timeinterval_t)10000000*(1LL<<16)))
        {
            T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"t1: %s", TimeStampToString (&slave->sync_tx_time, str));
            T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"t2: %s", TimeStampToString (&slave->sync_receive_time, str));
            T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"t3: %s", TimeStampToString (&slave->delay_req_send_time, str));
            T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"t4: %s", TimeStampToString (&slave->delay_req_receive_time, str));
            T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"slave_to_master_delay %s", vtss_tod_TimeInterval_To_String (&slave->slave_to_master_delay, str,0));
            T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"master_to_slave_delay %s", vtss_tod_TimeInterval_To_String (&slave->master_to_slave_delay, str,0));
        }
        // Setup data for this sync latency calculation
        vtss_ptp_offset_filter_param_t delay;
        delay.offsetFromMaster = slave->clock->currentDS.meanPathDelay;
        delay.rcvTime = {0, 0};  // delay.rcvTime is only used by the advanced filter i.e. it is not used by basic filter and can be set to 0 (or actually not even be set at all).

        if (0 == delay_filter(&delay, logMsgIntv, 0)) {
            T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"Delay too big for filtering");
            slave->clock->currentDS.delayOk = false;
        } else {
            slave->clock->currentDS.delayOk = true;
            slave->clock->currentDS.meanPathDelay = delay.offsetFromMaster;
        }
    } else {
        T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"Not ready to delay measurements");
        slave->clock->currentDS.delayOk = false;
        slave->clock->currentDS.meanPathDelay = 0LL;
        delay_filter_reset(0);  /* clears one-way E2E delay filter */
    }

    slave->delay_req_send_time.seconds = 0;
    slave->delay_req_send_time.nanoseconds = 0;
    slave->delay_req_receive_time.seconds = 0;
    slave->delay_req_receive_time.nanoseconds = 0;
}

/**
 * \brief Filter execution function.
 */
int ptp_basic_servo::delay_filter(vtss_ptp_offset_filter_param_t *delay, i8 logMsgInterval, int port)
{
    VTSS_ASSERT(port < my_port_count);
    T_DG(VTSS_TRACE_GRP_PTP_BASE_FILTER, "delay_filter (port %d)", port);

    if (filt_conf->delay_filter == 0) {
        /* 'wl' type delay filter */
        if (VTSS_INTERVAL_SEC(delay->offsetFromMaster)) {
            /* cannot filter with secs, clear filter */
            delay_filter_reset(port);
            return 0;
        }
        if (owd_wl_filt->actual_period == 0) {
            owd_wl_filt->act_min_delay = VTSS_MAX_TIMEINTERVAL;
            owd_wl_filt->act_max_delay = -VTSS_MAX_TIMEINTERVAL;
            owd_wl_filt->act_mean_delay = 0;
        }
    
        if (owd_wl_filt->act_min_delay > delay->offsetFromMaster) {
            owd_wl_filt->act_min_delay = delay->offsetFromMaster;
        }
        if (owd_wl_filt->act_max_delay < delay->offsetFromMaster) {
            owd_wl_filt->act_max_delay = delay->offsetFromMaster;
        }
        owd_wl_filt->act_mean_delay +=delay->offsetFromMaster;
        if (owd_wl_filt->prev_cnt == 0) {
            owd_wl_filt->prev_delay = delay->offsetFromMaster;
        }
        if (++owd_wl_filt->prev_cnt > filt_conf->dist) {
            owd_wl_filt->prev_cnt = filt_conf->dist;
        }
        if (filt_conf->dist > 1) {
            /* min delay algorithm */
            if (++owd_wl_filt->actual_period >= filt_conf->period) {
                owd_wl_filt->act_mean_delay = owd_wl_filt->act_mean_delay / filt_conf->period;
                //delay->offsetFromMaster = (owd_wl_filt->prev_delay*(owd_wl_filt->prev_cnt-1) + owd_wl_filt->act_min_delay)/(owd_wl_filt->prev_cnt);
                delay->offsetFromMaster = owd_wl_filt->act_min_delay;
                owd_wl_filt->prev_delay = delay->offsetFromMaster;
                owd_wl_filt->actual_period = 0;
                T_I("delayfilter, min %d ns, max %d ns, delay %d ns, prev_cnt %d",
                    VTSS_INTERVAL_NS(owd_wl_filt->act_min_delay), VTSS_INTERVAL_NS(owd_wl_filt->act_max_delay),
                    VTSS_INTERVAL_NS(delay->offsetFromMaster), owd_wl_filt->prev_cnt);
                return 1;
            }
        } else {
            /* mean delay algorithm */
            if (++owd_wl_filt->actual_period >= filt_conf->period) {
                owd_wl_filt->act_mean_delay = owd_wl_filt->act_mean_delay / filt_conf->period;
                delay->offsetFromMaster = owd_wl_filt->act_mean_delay;
                owd_wl_filt->actual_period = 0;
                T_I("delayfilter, min %d ns, max %d ns, delay %d ns",
                    VTSS_INTERVAL_NS(owd_wl_filt->act_min_delay), VTSS_INTERVAL_NS(owd_wl_filt->act_max_delay),
                    VTSS_INTERVAL_NS(delay->offsetFromMaster));
                return 1;
            }
        }
        delay->offsetFromMaster = owd_wl_filt->prev_delay;
        return 1;
    } else if (filt_conf->dist != 0) {
        /* 'normal' low pass type delay filter */
        owd_filt[port].filter(&delay->offsetFromMaster, 1<<filt_conf->delay_filter, true);
        return 1;
    } else {
        /* 'g8275.1' 0,1Hz lowpass type delay filter */
        /* lowpass offset algorithm */
        u32 period;
        if (logMsgInterval < 0) {
            period = (1<<(-logMsgInterval)) * LP_FILTER_0_1_HZ_CONSTANT; // Filter bandwitdth = 0,1Hz => period = 10/pi sec ~= 3
        } else {
            period = LP_FILTER_0_1_HZ_CONSTANT / (1<<(logMsgInterval));
        }
        if (period == 0) period = 1;
        T_DG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"Setting Delay filter parameter: period to %d", period);
        owd_filt[port].filter(&delay->offsetFromMaster, period, true);
        return 1;
    }
}

/**
 * \brief Filter reset function.
 */
void ptp_basic_servo::offset_filter_reset()
{
    if (filt_conf->dist > 1) {
        act_min_offset = VTSS_MAX_TIMEINTERVAL;
    } else {
        act_min_offset = 0;
    }
    act_min_ts.seconds = 0;
    act_min_ts.nanoseconds = 0;
    actual_period = 0;
    prev_offset = 0;
    prev_offset_valid = false;
    integral = 0;
    prev_ts.seconds = 0;
    prev_ts.nanoseconds = 0;
    prev_ts_valid = false;
    prop = 0;
    diff = 0;
    delta_t = 0;
}

bool ptp_basic_servo::stable_offset_calc(ptp_slave_t *slave, vtss_timeinterval_t off)
{
    if (llabs(off - slave->clock->currentDS.offsetFromMaster) <= stable_offset_threshold ) {
        if (++stable_cnt >= stable_cnt_threshold) {
            stable_offset = true;
            unstable_cnt = 0;
            --stable_cnt;
        }
    } else {
        if (++unstable_cnt >= unstable_cnt_threshold) {
            stable_offset = false;
            stable_cnt = 0;
            --unstable_cnt;
        }
    }

    return stable_offset;
}

void ptp_basic_servo::stable_offset_clear()
{
    stable_offset = false;
    unstable_cnt = 0;
    stable_cnt = 0;
}

bool ptp_basic_servo::offset_ok(ptp_slave_t *slave)
{
    if (_offset_ok) {
        _offset_ok = (llabs(slave->clock->currentDS.offsetFromMaster) <= offset_fail_threshold || !slave->two_way);
    } else {
        _offset_ok = (llabs(slave->clock->currentDS.offsetFromMaster) <= offset_ok_threshold || !slave->two_way);
    }
    return _offset_ok;
}

/* returns 0 if adjustment has not been done, because we are waiting for a delay measurement */
/* returns 1 if time has been set, i.e. the offset is too large to adjust => statechange to UNCL*/
/* returns 2 if adjustment has been done  => state change to SLVE */
int ptp_basic_servo::offset_calc(CapArray<ptp_clock_t *, VTSS_APPL_CAP_PTP_CLOCK_CNT> &ptpClock, int clock_inst, const mesa_timestamp_t& send_time, const mesa_timestamp_t& recv_time, vtss_timeinterval_t correction, i8 logMsgIntv, u16 sequenceId, bool peer_delay)
{
    ptp_slave_t *const slave = &ptpClock[clock_inst]->ssm;
    char str [50];
    char str2 [50];
    char str3 [50];
    i32 adj = 0;
    vtss_ptp_offset_filter_param_t filt_par;
    bool offset_result = false;    
    int ret_val = 0;
    bool stable_offset = false;
    T_RG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"ptp_offset_calc, clock_state %s", clock_state_to_string (slave->clock_state));
    T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"F,%05u,%010d,%09d,%010d,%09d,%c" VPRI64Fd("010") "\n",sequenceId, send_time.seconds, send_time.nanoseconds, recv_time.seconds, recv_time.nanoseconds,
           correction >= 0 ? '+' : '-', correction>>16);
    
    prc_locked_state = false;
// #if defined(VTSS_SW_OPTION_SYNCE)
//     vtss_appl_synce_quality_level_t ql;
//     vtss_rc rc;
//     if (slave->slave_port != NULL) {
//         if ((rc = synce_mgmt_clock_locked_ql_get(slave->slave_port->portDS.status.portIdentity.portNumber-1, &ql)) != VTSS_RC_OK) {
//             T_WG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"synce_mgmt_clock_ql_get returned error");
//         }
//         T_DG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"ql = %d", ql);
//         prc_locked_state = ((slave->clock_state == VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKED || slave->clock_state == VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKED ||
//                              slave->clock_state == VTSS_PTP_SLAVE_CLOCK_RECOVERING) && (ql == VTSS_APPL_SYNCE_QL_PRC));
//     }
// #endif //defined(VTSS_SW_OPTION_SYNCE)

    // Basic offset/delay-filters and servo
    /* ignore packets while the clock is settling */
    if (slave->clock_state != VTSS_PTP_SLAVE_CLOCK_F_SETTLING && slave->clock_state != VTSS_PTP_SLAVE_CLOCK_P_SETTLING && slave->clock_state != VTSS_PTP_SLAVE_CLOCK_FREQ_LOCK_INIT) {
        /* calc 'master_to_slave_delay' */
        vtss_tod_sub_TimeInterval(&slave->master_to_slave_delay, &recv_time, &send_time);
        slave->master_to_slave_delay = slave->master_to_slave_delay - correction;
        slave->master_to_slave_delay_valid = true;
        T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"master_to_slave_delay %s", vtss_tod_TimeInterval_To_String (&slave->master_to_slave_delay, str,0));

        /* if huge delay (> 400ms sec) then set the local clock */
        if (llabs(slave->master_to_slave_delay) >= ((400000000LL)<<16)) {
            mesa_timestamp_t now;
            /* Set the clock in the slave to be better than one sec accuracy, in a later step we use the vtss_local_clock_adj_offset to set a more precise time */
            vtss_tod_add_TimeInterval(&now, &send_time, &correction);
            vtss_local_clock_time_set(&now, slave->localClockId);
            /* clear adjustment rate */
            clock_servo_reset(SET_VCXO_FREQ);
            /* cannot filter with secs, clear filter */
            offset_filter_reset();
            slave->master_to_slave_delay = 0;
            slave->master_to_slave_delay_valid = false;
            //ptpClock->owd_filt->delay_filter_reset(ptpClock->owd_filt,0);  /* clears one-way E2E delay filter */
            T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"Huge time difference, sendt: %s recvt: %s", TimeStampToString(&send_time, str),TimeStampToString(&recv_time, str2));
            T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"Huge time difference, corr: %s now: %s", vtss_tod_TimeInterval_To_String (&correction, str, '.'), TimeStampToString(&now, str2));
            slave->clock_state = VTSS_PTP_SLAVE_CLOCK_F_SETTLING;
            T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s. Reason: Set time of day", clock_state_to_string (slave->clock_state));
            vtss_ptp_timer_start(&slave->settle_timer, slave->clock_settle_time*PTP_LOG_TIMEOUT(0), false);
            slave->in_settling_period = true;
            T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"timer started");
   
            ret_val = 1;
        } else {
            /* |Offset| <= 0,4 sec */
            //if (slave->clock_state == VTSS_PTP_SLAVE_CLOCK_FREERUN) {
            //    slave->clock_state = VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKING;
            //    vtss_local_clock_mode_set(VTSS_PTP_CLOCK_LOCKING);
            //    stable_offset_clear( slave);
            //    T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s. Reason: start adjustment", clock_state_to_string (slave->clock_state));
            //}
            filt_par.offsetFromMaster = slave->master_to_slave_delay - slave->clock->currentDS.meanPathDelay;
            filt_par.rcvTime = recv_time;
            if (slave->clock_state == VTSS_PTP_SLAVE_CLOCK_FREERUN) {
                /* if big offset (> 100 usec) then set the local clock offset */
                if (llabs(filt_par.offsetFromMaster) > 100000LL<<16) {
                    /*do offset adjustment, max +/- 0.5 sec */
                    if (filt_par.offsetFromMaster > 500000000LL<<16) {
                        filt_par.offsetFromMaster = 500000000LL<<16;
                    }
                    if (filt_par.offsetFromMaster < -500000000LL<<16) {
                        filt_par.offsetFromMaster = -500000000LL<<16;
                    }
                    T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"Do offset adj master_to_slave_delay %s, meanPathDelay %s ",
                        vtss_tod_TimeInterval_To_String (&slave->master_to_slave_delay, str,0),
                        vtss_tod_TimeInterval_To_String (&slave->clock->currentDS.meanPathDelay, str2,0));
                    vtss_local_clock_adj_offset((filt_par.offsetFromMaster>>16), slave->localClockId);
                    T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"Old offset %s ns, new offset %s ns", vtss_tod_TimeInterval_To_String (&slave->clock->currentDS.offsetFromMaster, str,0), vtss_tod_TimeInterval_To_String (&filt_par.offsetFromMaster, str2,0));
                    slave->clock->currentDS.offsetFromMaster = filt_par.offsetFromMaster;
                    slave->master_to_slave_delay_valid = false;
                    T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"Offset adjustment done %s ns", vtss_tod_TimeInterval_To_String (&filt_par.offsetFromMaster, str,0));
                    offset_filter_reset();
                    stable_offset_clear();
                    slave->clock_state = VTSS_PTP_SLAVE_CLOCK_FREQ_LOCK_INIT;
                    T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s. Reason: Adjust time of day.", clock_state_to_string (slave->clock_state));
                    vtss_ptp_timer_start(&slave->settle_timer, slave->clock_settle_time*PTP_LOG_TIMEOUT(0), false);
                    slave->in_settling_period = true;
                    ret_val = 1;
                } else {
                    slave->clock_state = VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKING;
                    T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s. Offset within limits", clock_state_to_string (slave->clock_state));
                }
            }
            if (slave->clock_state == VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKING ||
                slave->clock_state == VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKING ||
                slave->clock_state == VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKED ||
                slave->clock_state == VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKED)
            {
                /* check if offset is stable */
                stable_offset = stable_offset_calc(slave, filt_par.offsetFromMaster);
   
                T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"stable_offset %d, new offset " VPRI64d ", cur offset " VPRI64d, stable_offset, filt_par.offsetFromMaster>>16, slave->clock->currentDS.offsetFromMaster>>16);
                offset_result = offset_filter(&filt_par, logMsgIntv);
                if (offset_result) {
                    slave->clock->currentDS.offsetFromMaster = filt_par.offsetFromMaster;
                }
                T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"offset_result %d", offset_result);
            }
            if (slave->clock_state == VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKING || slave->clock_state == VTSS_PTP_SLAVE_CLOCK_RECOVERING) {
                T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"delay_ok %d,  two_way %d", slave->clock->currentDS.delayOk, slave->two_way);
                if (slave->clock->currentDS.delayOk || !slave->two_way) {
                    if (slave->clock_state != VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKING) {
                        slave->clock_state = VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKING;
                        T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s. Delay measurement done", clock_state_to_string (slave->clock_state));
                    }
                    if (offset_result) {
                        /* if big offset (> 100 usec) then set the local clock offset */
                        if (llabs(filt_par.offsetFromMaster) > 100000LL<<16) {
                            /*do offset adjustment, max +/- 0.5 sec */
                            if (filt_par.offsetFromMaster > 500000000LL<<16) {
                                filt_par.offsetFromMaster = 500000000LL<<16;
                            }
                            if (filt_par.offsetFromMaster < -500000000LL<<16) {
                                filt_par.offsetFromMaster = -500000000LL<<16;
                            }
                            T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"Do offset adj master_to_slave_delay %s, meanPathDelay %s ",
                                 vtss_tod_TimeInterval_To_String (&slave->master_to_slave_delay, str,0),
                                 vtss_tod_TimeInterval_To_String (&slave->clock->currentDS.meanPathDelay, str2,0));
                            vtss_local_clock_adj_offset((filt_par.offsetFromMaster>>16), slave->localClockId);
                            slave->master_to_slave_delay_valid = false;
                            T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"Offset adjustment done %s ns", vtss_tod_TimeInterval_To_String (&filt_par.offsetFromMaster, str,0));
                            offset_filter_reset();
                            stable_offset_clear();
                            slave->clock_state = VTSS_PTP_SLAVE_CLOCK_P_SETTLING;
                            T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s. Reason: Adjust time of day", clock_state_to_string (slave->clock_state));
                            vtss_ptp_timer_start(&slave->settle_timer, slave->clock_settle_time*PTP_LOG_TIMEOUT(0), false);
                            slave->in_settling_period = true;
                            ret_val = 1;
                        } else {
                            adj = clock_servo(&filt_par, &slave->clock->parentDS.observedParentClockPhaseChangeRate, slave->localClockId, 1);
                            if (stable_offset && offset_ok(slave)) {
                                if (slave->two_way) {
                                    slave->clock_state = VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKED;
                                }
                                else {
                                    slave->clock_state = VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKED;
                                }
                                vtss_local_clock_mode_set(VTSS_PTP_CLOCK_LOCKED);
                                T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s. Reason. Offset within limits", clock_state_to_string (slave->clock_state));
                                ret_val = 2;
                            }
                        }
                    }
                }
            } else if (slave->clock_state == VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKING) {
                /* update 'offset_from_master' */
                if (offset_result) {
                    /* if big offset (> 1 msec) then set the local clock offset */
                    if (llabs(filt_par.offsetFromMaster) > 1000000LL<<16) {
                        /*do offset adjustment, max +/- 0.5 sec */
                        if (filt_par.offsetFromMaster > 500000000LL<<16) {
                            filt_par.offsetFromMaster = 500000000LL<<16;
                        }
                        if (filt_par.offsetFromMaster < -500000000LL<<16) {
                            filt_par.offsetFromMaster = -500000000LL<<16;
                        }
                        T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"Do offset adj master_to_slave_delay %s, meanPathDelay %s ",
                             vtss_tod_TimeInterval_To_String (&slave->master_to_slave_delay, str,0),
                             vtss_tod_TimeInterval_To_String (&slave->clock->currentDS.meanPathDelay, str2,0));
                        vtss_local_clock_adj_offset((filt_par.offsetFromMaster>>16), slave->localClockId);
                        slave->master_to_slave_delay_valid = false;
                        T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"Offset adjustment done %s ns", vtss_tod_TimeInterval_To_String (&filt_par.offsetFromMaster, str,0));
                        offset_filter_reset();
                        stable_offset_clear();
                        slave->clock_state = VTSS_PTP_SLAVE_CLOCK_F_SETTLING;
                        T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s. Reason: Adjust time of day.", clock_state_to_string (slave->clock_state));
                        vtss_ptp_timer_start(&slave->settle_timer, slave->clock_settle_time*PTP_LOG_TIMEOUT(0), false);
                        slave->in_settling_period = true;
                        ret_val = 1;
                    } else {
                        adj = clock_servo(&filt_par, &slave->clock->parentDS.observedParentClockPhaseChangeRate, slave->localClockId, 0);
                        if (stable_offset) {
                            slave->clock_state = VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKING;
                            stable_offset_clear();
                            T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s. Reason: Offset is stable", clock_state_to_string (slave->clock_state));
                        }
                    }
                }
            } else if (slave->clock_state == VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKED || slave->clock_state == VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKED) {
                /* update 'offset_from_master' */
                if (slave->clock->currentDS.delayOk || !slave->two_way) {
                    if (offset_result) {
                        adj = clock_servo(&filt_par, &slave->clock->parentDS.observedParentClockPhaseChangeRate, slave->localClockId, 2);
                        ret_val = 2;
                        if (!stable_offset || !offset_ok(slave)) {
                            slave->clock_state = VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKING;
                            stable_offset_clear();
                            vtss_local_clock_mode_set(VTSS_PTP_CLOCK_LOCKING);
                            T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s. Reason: %s", clock_state_to_string (slave->clock_state), stable_offset ? "Offset outside limits" : "Unstable Offset");
                            //ret_val = 1;
                        }
                    }
                } else {
                    slave->clock_state = VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKING;
                    stable_offset_clear();
                    vtss_local_clock_mode_set(VTSS_PTP_CLOCK_LOCKING);
                    T_IG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"clock_state %s. Reason: Awaiting Delay measurements", clock_state_to_string (slave->clock_state));
                    ret_val = 1;
                }
            }
        }

        /* log delays > 5 ms */
        if (llabs(slave->master_to_slave_delay) > (((vtss_timeinterval_t)VTSS_ONE_MIA)<<16)/200) {
            T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"Great master_to_slave_delay %s", vtss_tod_TimeInterval_To_String(&slave->master_to_slave_delay, str,0));
            T_DG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"Sendt: %s recvt: %s, correction %s", TimeStampToString(&send_time, str), TimeStampToString(&recv_time, str2),
                vtss_tod_TimeInterval_To_String (&correction, str3,0));
        }
        if (slave->slave_port != NULL) {
            T_NG(VTSS_TRACE_GRP_PTP_BASE_CLK_STATE,"delayOk %d, two_way %d, peer_delay_ok%d",
                 slave->clock->currentDS.delayOk, slave->two_way, slave->slave_port->portDS.status.peer_delay_ok);
        }
    }

    if (slave->statistics.enable) {
        master_to_slave_delay_stati(slave);
    }
    if (offset_result) {
        displayStats(&slave->clock->currentDS.meanPathDelay, &slave->clock->currentDS.offsetFromMaster,
                     slave->clock->parentDS.observedParentClockPhaseChangeRate,
                     slave->clock->parentDS.observedParentOffsetScaledLogVariance, adj);
    }

    if (slave->slave_port != NULL) {
        switch (ret_val) {
            case 1:
                if (slave->slave_port->portDS.status.portState != VTSS_APPL_PTP_UNCALIBRATED) {
                    vtss_ptp_state_set(VTSS_APPL_PTP_UNCALIBRATED, slave->clock, slave->slave_port);
                }
                break;
            case 2:
                if (slave->slave_port->portDS.status.portState != VTSS_APPL_PTP_SLAVE) {
                    vtss_ptp_state_set(VTSS_APPL_PTP_SLAVE, slave->clock, slave->slave_port);
                }
                break;
            default:
                break;
        }
    }
    return 0;
}

/**
 * \brief Filter execution function.
 *
 * Find the min offset from master within a period (defined in filter configuration)
 * At the end of the period, the min offset and the corresponding timestamp is returned.
 */
int ptp_basic_servo::offset_filter(vtss_ptp_offset_filter_param_t *offset, i8 logMsgInterval)
{
#if defined(VTSS_OPT_PHY_TIMESTAMP)
    if ((logMsgInterval < -2) && (filt_conf->dist != 0)) {
        u32 adj_pr_sec = 1<<(abs(logMsgInterval+2));
        if (filt_conf->period*filt_conf->dist < adj_pr_sec) {
            filt_conf->period = adj_pr_sec/filt_conf->dist;
            T_WG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"Increasing filter parameter: period to %d", filt_conf->period);
        }
    }
#endif
    if (actual_period == 0) {
        act_min_offset = VTSS_MAX_TIMEINTERVAL;
        act_max_offset = -VTSS_MAX_TIMEINTERVAL;
        act_mean_offset = 0;
    }
    if (act_min_offset > offset->offsetFromMaster) {
        act_min_offset = offset->offsetFromMaster;
        act_min_ts = offset->rcvTime;
    }
    if (act_max_offset < offset->offsetFromMaster) {
        act_max_offset = offset->offsetFromMaster;
    }
    act_mean_offset +=offset->offsetFromMaster;
    if (filt_conf->dist > 1) {
        /* min offset algorithm */
        if (++actual_period >= filt_conf->period) {
            act_mean_offset = act_mean_offset/filt_conf->period;
            offset->offsetFromMaster = act_min_offset;
            offset->rcvTime = act_min_ts;
            actual_period = 0;
            T_DG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"offsetfilter, min %d ns, max %d ns",VTSS_INTERVAL_NS(act_min_offset), VTSS_INTERVAL_NS(act_max_offset));
            if (++actual_dist >= filt_conf->dist) {
                actual_dist = 0;
                return 1;
            }
        }
    } else if (filt_conf->dist == 1) {
        /* mean offset algorithm */
        if (++actual_period >= filt_conf->period) {
            act_mean_offset = act_mean_offset/filt_conf->period;
            offset->offsetFromMaster = act_mean_offset;
            actual_period = 0;
            T_DG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"offsetfilter, mean %d ns",VTSS_INTERVAL_NS(offset->offsetFromMaster));
            return 1;
        }
    } else { // filt_conf->dist == 0
        /* lowpass offset algorithm */
        u32 period;
        if (logMsgInterval < 0) {
            filt_conf->period = 1;
            period = (1<<(-logMsgInterval)) * LP_FILTER_0_1_HZ_CONSTANT; // Filter bandwitdth = 0,1Hz => period = 10/pi sec ~= 3
        } else {
            period = LP_FILTER_0_1_HZ_CONSTANT / (1<<(logMsgInterval));
            filt_conf->period = 1;
        }
        if (!prc_locked_state) {
            //filt_conf->period = filt_conf->period/10;
            //period = period/10;
            filt_conf->period = 1;
            period = 1;
            T_DG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"Setting offset filter parameter: period to %d", period);
        }
        if (period == 0) period = 1;
        if (filt_conf->period == 0) filt_conf->period = 1;
        T_DG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"Setting offset filter parameter: period to %d", period);

        offset_lp_filt.filter(&offset->offsetFromMaster, period, false);
        if (++actual_period >= filt_conf->period) {
            actual_period = 0;
            T_IG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"offsetfilter, lowpass %d ns",VTSS_INTERVAL_NS(offset->offsetFromMaster));
            return 1;
        }
    }
    return 0;
}

/**
 * \brief Clock Servo reset function.
 *
 * Implemnts a PID algorithm, each element can be enabled or disabled.
 *
 * clockadjustment = if (p_reg) {offset/ap} +
 *                   if (i_reg) (integral(offset)*diff(t))/ai +
 *                   if (d_reg) diff(offset)/(diff(t)*ad)
 *
 * The internal calculation unit is TimeInterval, and the resulting adj is calculated in
 *  units of 0,1 ppb.
 */
void ptp_basic_servo::clock_servo_reset(vtss_ptp_set_vcxo_freq setVCXOfreq)
{
    if (setVCXOfreq == SET_VCXO_FREQ) {
        if (holdover_ok) {
            vtss_local_clock_ratio_set(-adj_average, clock_inst);
            T_IG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"reset to holdover freq " VPRI64d " ppb*10",(((u64)adj_average)*10)/(((u64)2)<<16));
        } else {
            vtss_local_clock_ratio_clear(clock_inst);
            T_IG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"reset to freerun freq");
        }
    }
}

void ptp_basic_servo::clock_servo_status(vtss_ptp_servo_status_t *status)
{
    status->holdover_ok = holdover_ok;
    status->holdover_adj = (adj_average * 10LL) / (1LL << 16);
}

/**
 * \brief Clock Servo execution function.
 *
 * Implemnts a PID algorithm, each element can be enabled or disabled.
 *
 * clockadjustment = if (p_reg) {offset/ap} +
 *                   if (i_reg) (integral(offset)*diff(t))/ai +
 *                   if (d_reg) diff(offset)/(diff(t)*ad)
 *
 * The internal calculation unit is TimeInterval, and the resulting adj is calculated in
 *  units of 0,1 ppb.
 */
i32 ptp_basic_servo::clock_servo(const vtss_ptp_offset_filter_param_t *offset, i32 *observedParentClockPhaseChangeRate, int localClockId, int phase_lock)
{
    vtss_timeinterval_t temp_t;
    i64 time_offset;
    i32 filt_div;
    i64 adj = 0;
    i64 cur_of = 0;
    vtss_timeinterval_t thr = ((vtss_timeinterval_t)servo_conf->synce_threshold) << 16;

    time_offset = VTSS_INTERVAL_NS(offset->offsetFromMaster);
    time_offset = time_offset/ADJ_OFFSET_MAX;
    if (time_offset) {
        T_IG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"offsetfromMaster %d ns, time_offset " VPRI64d " timer ticks",VTSS_INTERVAL_NS(offset->offsetFromMaster), time_offset);
        /* if secs, reset clock or set freq adjustment to max */
        adj = offset->offsetFromMaster > 0 ? (ADJ_FREQ_MAX_LL << 16) : -(ADJ_FREQ_MAX_LL << 16);
    } else {
        /* the PID controller */
        if (prev_ts_valid) {
            vtss_tod_sub_TimeInterval(&temp_t, &offset->rcvTime, &prev_ts);  /* delta T in ns */
            delta_t = VTSS_INTERVAL_US(temp_t);  /* delta T in us */
        } else delta_t = VTSS_ONE_MILL;
        prev_ts = offset->rcvTime;
        prev_ts_valid = true;

        cur_of = offset->offsetFromMaster;
        /* the P component */
        prop = 0;
        if (servo_conf->p_reg && servo_conf->ap != 0) {
            prop = offset->offsetFromMaster/servo_conf->ap;
        }

        if (servo_conf->i_reg && servo_conf->ai != 0 && (phase_lock > 0)) {
            /* the accumulator for the I component */
            integral += (cur_of*delta_t)/(servo_conf->ai*(longlong)VTSS_ONE_MILL);
            if (integral > (ADJ_FREQ_MAX_LL<<16))
                integral = (ADJ_FREQ_MAX_LL<<16);
            else if (integral < -(ADJ_FREQ_MAX_LL<<16))
                integral = -(ADJ_FREQ_MAX_LL<<16);
        } else {
            integral = 0;
        }
        *observedParentClockPhaseChangeRate = (integral>>16) * servo_conf->gain;

        /* the D component */
        if (servo_conf->d_reg && servo_conf->ad != 0 && delta_t != 0LL && (phase_lock > 0)) {
            u32 ad = servo_conf->ad;
            if (!prc_locked_state) {
                ad = ad * 3;
            }

            if (prev_offset_valid) diff = (cur_of - prev_offset)*(longlong)VTSS_ONE_MILL/(delta_t*ad);
            prev_offset = cur_of;
            prev_offset_valid = true;
        } else {
            diff = 0;
            prev_offset_valid = false;
        }

        adj = (prop + diff + integral) * servo_conf->gain;
        T_DG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"cur_offset " VPRI64d ", delta_t " VPRI64d " ms, P= " VPRI64d ", I= " VPRI64d ", D= " VPRI64d ", adj = " VPRI64d, cur_of, delta_t, prop, integral, diff, adj);
        T_DG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"gain %d", servo_conf->gain);

    }
    if ((servo_conf->srv_option == VTSS_APPL_PTP_CLOCK_FREE) || (offset->offsetFromMaster >= thr) || (offset->offsetFromMaster <= -thr)) {
        /* apply controller output as a clock tick rate adjustment */
        vtss_local_clock_ratio_set(-adj, localClockId);
        T_DG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"|offset from master| >= %d ns or free run mode, srv_option %d", servo_conf->synce_threshold, servo_conf->srv_option);
    } else {
        T_DG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"|offset from master| < %d ns and free run mode, srv_option %d", servo_conf->synce_threshold, servo_conf->srv_option);
        vtss_local_clock_ratio_set(0, localClockId);
        adj = adj*delta_t/VTSS_ONE_MILL;
        vtss_local_clock_fine_adj_offset(adj, localClockId);
        *observedParentClockPhaseChangeRate = 0;       
        //vtss_local_clock_adj_offset(offset->offsetFromMaster > 0 ? servo_conf->synce_ap : -servo_conf->synce_ap, localClockId);
    }
    /* check adjustment stability and update holdover frequency */
    if (phase_lock == 2) {
        filt_div = holdover_count+1 < servo_conf->ho_filter ? holdover_count+1 : servo_conf->ho_filter;
        if (filt_div == 0) filt_div = 1;
        adj_avefix = (adj_avefix * ((i64)filt_div - 1) + adj * 100000LL) / filt_div;
        if (!prc_locked_state) {
            adj_average = adj_avefix / 100000;
        } else {
            adj_average = 0LL;
        }
        adj_stable = llabs(adj_average - adj) <= llabs((servo_conf->stable_adj_threshold<<16)/10LL) ? true : false;
        T_IG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"adj_average " VPRI64d ", adj_stable %d, adj " VPRI64d ", filt_div %d", adj_average, adj_stable, adj, filt_div);
        if (adj_stable && !holdover_ok ) {
            holdover_ok = (holdover_count++ >= servo_conf->ho_filter);
            T_IG(VTSS_TRACE_GRP_PTP_BASE_FILTER,"holdover_count %d, holdover_ok %d", holdover_count, holdover_ok);
        }
    }
    if (!adj_stable || phase_lock != 2) {
        holdover_count = 0;
        holdover_ok = false;
    }

    return adj;
}

bool ptp_basic_servo::display_stats()
{
    return servo_conf->display_stats;
}

int ptp_basic_servo::display_parm(char *buf)
{
    return sprintf(buf, " " VPRI64Fd("15") ", " VPRI64Fd("15") ", " VPRI64Fd("15") ", %6d, %6d, %6d, " VPRI64Fd("15"),
                   prop, integral, diff, VTSS_INTERVAL_NS(act_min_offset), VTSS_INTERVAL_NS(act_max_offset), VTSS_INTERVAL_NS(act_mean_offset), delta_t);
}

void ptp_basic_servo::activate(uint32_t domain)
{
    T_DG(VTSS_TRACE_GRP_PTP_BASE_FILTER, "activate of BASIC servo was called");
}

void ptp_basic_servo::deactivate(uint32_t domain)
{
    T_DG(VTSS_TRACE_GRP_PTP_BASE_FILTER, "deactivate of BASIC servo was called");
}

/**
 * \brief Create a basic PTP offset filter instance.
 * Create an instance of the basic vtss_ptp offset filter
 *
 * \param of [IN]  pointer to a structure containing the default parameters for
 *                the offset filter
 * \param s [IN]  pointer to a structure containing the default parameters for
 *                the servo
 * \return (opaque) instance data reference or NULL.
 */
ptp_basic_servo::ptp_basic_servo(int inst, vtss_appl_ptp_clock_filter_config_t *of, const vtss_appl_ptp_clock_servo_config_t *s, int port_count)
 : offset_lp_filt("Offset LP filter"), my_port_count(port_count+1)
{
    int i;
    clock_inst = inst;
    filt_conf = of;
    servo_conf = s;
    vtss_lowpass_filter_s * f;
    owd_filt = (vtss_lowpass_filter_s *) vtss_ptp_calloc(port_count+1, sizeof(vtss_lowpass_filter_t));
    for (i = 0; i <= port_count; i++) {
        f = new (owd_filt+i) vtss_lowpass_filter_s("Delay LP filter");
        T_IG(VTSS_TRACE_GRP_PTP_BASE_FILTER, "delay_filter %d adr = %p size %u", i, f, (u32)sizeof(vtss_lowpass_filter_t));
    }
    owd_wl_filt = (vtss_wl_delay_filter_t*)vtss_ptp_calloc(port_count+1, sizeof(vtss_wl_delay_filter_t));
    T_IG(VTSS_TRACE_GRP_PTP_BASE_FILTER, "delay filter: delay_filter = %d", filt_conf->delay_filter);
    T_IG(VTSS_TRACE_GRP_PTP_BASE_FILTER, "delay filter: period = %d dist = %d", filt_conf->period, filt_conf->dist);
    T_IG(VTSS_TRACE_GRP_PTP_BASE_FILTER, "offset filter: display_stats %d", servo_conf->display_stats);
}

ptp_basic_servo::~ptp_basic_servo()
{
    if (owd_filt) vtss_ptp_free(owd_filt);
    if (owd_wl_filt) vtss_ptp_free(owd_wl_filt);
}

#endif // SW_OPTION_BASIC_PTP_SERVO
