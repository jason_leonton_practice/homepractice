/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
/* vtss_ptp_sys_timer.h */

#ifndef VTSS_PTP_SYS_TIMER_H
#define VTSS_PTP_SYS_TIMER_H

#include "mscc/ethernet/switch/api/types.h"

/**
 * \new timer implementation
 */

typedef struct vtss_ptp_sys_timer_s *vtss_timer_handle_t;

typedef void (*vtss_timer_callout)(vtss_timer_handle_t timer, void *context);

typedef struct vtss_timer_head_s{
    struct  vtss_timer_head_s *next;
    struct  vtss_timer_head_s *prev;
} vtss_timer_head_t;

typedef struct vtss_ptp_sys_timer_s {
    vtss_timer_head_t head;
    int32_t period;
    int32_t left;
    vtss_timer_callout co;
    void *context;
    bool periodic;
} vtss_ptp_sys_timer_t;


#ifdef __cplusplus
extern "C" {
#endif
void vtss_ptp_timer_start(vtss_ptp_sys_timer_t *t, int32_t period, bool repeat);
void vtss_ptp_timer_stop(vtss_ptp_sys_timer_t *t);

void vtss_ptp_timer_tick(int32_t my_time);

void vtss_init_ptp_timer(vtss_ptp_sys_timer_t *t, vtss_timer_callout co, void *context);

void vtss_ptp_timer_initialize(void);
#ifdef __cplusplus
}
#endif

#endif // VTSS_PTP_SYS_TIMER_H
