/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _VTSS_PTP_SERVO_H_
#define _VTSS_PTP_SERVO_H_

#include "vtss_ptp_offset_filter.h"

typedef enum {
    VTSS_PTP_HYBRID_TRANSIENT_QUICK,
    VTSS_PTP_HYBRID_TRANSIENT_OPTIONAL,
    VTSS_PTP_HYBRID_TRANSIENT_NOT_ACTIVE
} vtss_ptp_hybrid_transient;

// typedef struct ptp_slave_s ptp_slave_t;
typedef struct ptp_clock_s ptp_clock_t;  // Note: This is a forward declaration to solve a circular dependency.

struct ptp_servo {
    /* data members */
    const char *const IntervalTxt[13] = {"0.007812500", "0.015625","0.03125","0.0625","0.125","0.25","0.5", "1.000","2.000","4.000","8.000","16.000","32.000"};
    bool holdover_ok = false;                                                               // true indicates that the holdover stability period has expired
    int clock_inst;                                                                         // This member holds the instance number of the clock with which this servo is associated.

    i64 stable_offset_threshold = 100LL<<16;  // Only used by basic servo. Declared here for compatibility between servo variants.
    u32 stable_cnt_threshold = 10;     // as above
    u32 unstable_cnt_threshold = 20;   // as above
    i64 offset_ok_threshold = 200LL<<16;      // as above
    i64 offset_fail_threshold = 1000LL<<16;    // as above

    /* member functions */
    virtual mesa_rc switch_to_packet_mode(uint32_t domain) = 0;
    virtual mesa_rc switch_to_hybrid_mode(uint32_t domain) = 0;
    virtual bool mode_is_hybrid_mode(uint32_t domain) = 0;
    virtual mesa_rc set_active_ref(uint32_t domain, int stream) = 0;
    virtual mesa_rc set_active_electrical_ref(uint32_t domain, int input) = 0;
    virtual mesa_rc set_hybrid_transient(uint32_t domain, vtss_ptp_hybrid_transient state) = 0;
    virtual vtss_rc force_holdover_set(BOOL enable) = 0;
    virtual vtss_rc force_holdover_get(BOOL *enable) = 0;
    virtual int delay_filter(vtss_ptp_offset_filter_param_t *delay, i8 logMsgInterval, int port) = 0;
    virtual void delay_filter_reset(int port) = 0;
    virtual int servo_type() = 0;
    virtual int offset_filter(vtss_ptp_offset_filter_param_t *offset, i8 logMsgInterval) = 0;
    virtual void offset_filter_reset() = 0;
    virtual void delay_calc(CapArray<ptp_clock_t *, VTSS_APPL_CAP_PTP_CLOCK_CNT> &ptpClock, int clock_inst, const mesa_timestamp_t& send_time, const mesa_timestamp_t& recv_time, mesa_timeinterval_t correction, i8 logMsgIntv) = 0;
    virtual int offset_calc(CapArray<ptp_clock_t *, VTSS_APPL_CAP_PTP_CLOCK_CNT> &ptpClock, int clock_inst, const mesa_timestamp_t& send_time, const mesa_timestamp_t& recv_time, mesa_timeinterval_t correction, i8 logMsgIntv, u16 sequenceId, bool peer_delay) = 0;
    virtual void clock_servo_reset(vtss_ptp_set_vcxo_freq setVCXOfreq) = 0;
    virtual void clock_servo_status(vtss_ptp_servo_status_t *status) = 0;
    virtual bool display_stats() = 0;
    virtual int display_parm(char* buf) { return 0; };
    virtual void activate(uint32_t domain) = 0;
    virtual void deactivate(uint32_t domain) = 0;
};

#endif // _VTSS_PTP_SERVO_H_
