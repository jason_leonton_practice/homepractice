/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#ifdef VTSS_SW_OPTION_ICFG
#include "icfg_api.h"
#include "icli_porting_util.h"
#include "ptp_api.h"
#include "vtss_tod_api.h"
#include "ptp.h" // For Trace
#include "msg_api.h" // To check is an isid is valid
#include "misc_api.h"
#include "tod_api.h"
#include "ptp_1pps_sync.h"
#include "ptp_1pps_closed_loop.h"
#endif
#include "ptp_1pps_serial.h"

/***************************************************************************/
/*  Internal types                                                         */
/***************************************************************************/

/***************************************************************************/
/*  Internal functions                                                     */
/***************************************************************************/

static const char *device_type_2_string(u8 type)
{
    switch (type) {
        case VTSS_APPL_PTP_DEVICE_NONE:            return "none";
        case VTSS_APPL_PTP_DEVICE_ORD_BOUND:       return "boundary";
        case VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT: return "p2ptransparent";
        case VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT: return "e2etransparent";
        case VTSS_APPL_PTP_DEVICE_SLAVE_ONLY:      return "slave";
        case VTSS_APPL_PTP_DEVICE_MASTER_ONLY:     return "master";
        case VTSS_APPL_PTP_DEVICE_BC_FRONTEND:     return "bcfrontend";
        default:                         return "?";
    }
}

static const char *protocol_2_string(u8 p)
{
    switch (p) {
        case VTSS_APPL_PTP_PROTOCOL_ETHERNET:       return "ethernet";
        case VTSS_APPL_PTP_PROTOCOL_ETHERNET_MIXED: return "ethernet-mixed";
        case VTSS_APPL_PTP_PROTOCOL_IP4MULTI:       return "ip4multi";
        case VTSS_APPL_PTP_PROTOCOL_IP4MIXED:       return "ip4mixed";
        case VTSS_APPL_PTP_PROTOCOL_IP4UNI:         return "ip4uni";
        case VTSS_APPL_PTP_PROTOCOL_OAM:            return "oam";
        case VTSS_APPL_PTP_PROTOCOL_ONE_PPS:        return "onepps";
        default:                          return "?";
    }
}

static const char *filter_type_2_string(u32 p)
{
    switch (p) {
#if defined(VTSS_SW_OPTION_ZLS30387)
        case PTP_FILTERTYPE_ACI_DEFAULT:
            return "aci-default";
        case PTP_FILTERTYPE_ACI_FREQ_XO:
            return "aci-freq-xo";
        case PTP_FILTERTYPE_ACI_PHASE_XO:
            return "aci-phase-xo";
        case PTP_FILTERTYPE_ACI_FREQ_TCXO:
            return "aci-freq-tcxo";
        case PTP_FILTERTYPE_ACI_PHASE_TCXO:
            return "aci-phase-tcxo";
        case PTP_FILTERTYPE_ACI_FREQ_OCXO_S3E:
            return "aci-freq-ocxo-s3e";
        case PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E:
            return "aci-phase-ocxo-s3e";
        case PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_FREQ:
            return "aci-bc-partial-on-path-freq";
        case PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_PHASE:
            return "aci-bc-partial-on-path-phase";
        case PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_FREQ:
            return "aci-bc-full-on-path-freq";
        case PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE:
            return "aci-bc-full-on-path-phase";
        case PTP_FILTERTYPE_ACI_FREQ_ACCURACY_FDD:
            return "aci-freq-accuracy-fdd";
        case PTP_FILTERTYPE_ACI_FREQ_ACCURACY_XDSL:
            return "aci-freq-accuracy-xdsl";
        case PTP_FILTERTYPE_ACI_ELEC_FREQ:
            return "aci-elec-freq";
        case PTP_FILTERTYPE_ACI_ELEC_PHASE:
            return "aci-elec-phase";
        case PTP_FILTERTYPE_ACI_PHASE_TCXO_Q:
            return "aci-phase-tcxo-q";
        case PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_Q:
            return "aci-bc-full-on-path-phase-q";
        case PTP_FILTERTYPE_ACI_PHASE_RELAXED_C60W:
            return "aci-phase-relaxed-c60w";
        case PTP_FILTERTYPE_ACI_PHASE_RELAXED_C150:
            return "aci-phase-relaxed-c150";
        case PTP_FILTERTYPE_ACI_PHASE_RELAXED_C180:
             return "aci-phase-relaxed-c180";
        case PTP_FILTERTYPE_ACI_PHASE_RELAXED_C240:
             return "aci-phase-relaxed-c240";
        case PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_BETA:
             return "aci-bc-full-on-path-phase-beta";
        case PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E_R4_6_1:
             return "aci-phase-ocxo-s3e-r4-6-1";
        case PTP_FILTERTYPE_ACI_BASIC_PHASE:
             return "aci-basic-phase";
        case PTP_FILTERTYPE_ACI_BASIC_PHASE_LOW:
             return "aci-basic-phase-low";
        case PTP_FILTERTYPE_BASIC:            // Note: This is a special case. We are not allowed to support servo type 
             return "aci-basic-phase";        //       PTP_FILTERTYPE_BASIC except for calibration purposes.
#endif                                  
        default: return "?";
    }
}

static const char *one_pps_mode_2_string(vtss_appl_ptp_ext_clock_1pps_t m)
{
    switch (m) {
        case VTSS_APPL_PTP_ONE_PPS_DISABLE: return "";
        case VTSS_APPL_PTP_ONE_PPS_OUTPUT: return "output";
        case VTSS_APPL_PTP_ONE_PPS_INPUT: return "input";
        case VTSS_APPL_PTP_ONE_PPS_OUTPUT_INPUT: return "out-in";
        default: return "unknown";
    }
}

static const char *cli_rs422_baudrate_2_string(vtss_serial_baud_rate_t baudrate)
{
    switch(baudrate) {
        case VTSS_SERIAL_BAUD_50:     return     "50"; break;
        case VTSS_SERIAL_BAUD_75:     return     "75"; break;
        case VTSS_SERIAL_BAUD_110:    return    "110"; break;
        case VTSS_SERIAL_BAUD_134_5:  return  "134.5"; break;
        case VTSS_SERIAL_BAUD_150:    return    "150"; break;
        case VTSS_SERIAL_BAUD_200:    return    "200"; break;
        case VTSS_SERIAL_BAUD_300:    return    "300"; break;
        case VTSS_SERIAL_BAUD_600:    return    "600"; break;
        case VTSS_SERIAL_BAUD_1200:   return   "1200"; break;
        case VTSS_SERIAL_BAUD_1800:   return   "1800"; break;
        case VTSS_SERIAL_BAUD_2400:   return   "2400"; break;
        case VTSS_SERIAL_BAUD_3600:   return   "3600"; break;
        case VTSS_SERIAL_BAUD_4800:   return   "4800"; break;
        case VTSS_SERIAL_BAUD_7200:   return   "7200"; break;
        case VTSS_SERIAL_BAUD_9600:   return   "9600"; break;
        case VTSS_SERIAL_BAUD_14400:  return  "14400"; break;
        case VTSS_SERIAL_BAUD_19200:  return  "19200"; break;
        case VTSS_SERIAL_BAUD_38400:  return  "38400"; break;
        case VTSS_SERIAL_BAUD_57600:  return  "57600"; break;
        case VTSS_SERIAL_BAUD_115200: return "115200"; break;
        case VTSS_SERIAL_BAUD_230400: return "230400"; break;
        case VTSS_SERIAL_BAUD_460800: return "460800"; break;
        case VTSS_SERIAL_BAUD_921600: return "921600"; break;
        default: return "???";
    }
}

static const char *cli_rs422_parity_2_string(vtss_serial_parity_t parity)
{
    switch(parity) {
        case VTSS_SERIAL_PARITY_NONE:  return "none";  break;
        case VTSS_SERIAL_PARITY_EVEN:  return "even";  break;
        case VTSS_SERIAL_PARITY_ODD:   return "odd";   break;
        case VTSS_SERIAL_PARITY_MARK:  return "mark";  break;
        case VTSS_SERIAL_PARITY_SPACE: return "space"; break;
        default: return "???";
    }
}

static const char *cli_rs422_word_length_2_string(vtss_serial_word_length_t word_length)
{
    switch(word_length) {
        case VTSS_SERIAL_WORD_LENGTH_5: return "5"; break;
        case VTSS_SERIAL_WORD_LENGTH_6: return "6"; break;
        case VTSS_SERIAL_WORD_LENGTH_7: return "7"; break;
        case VTSS_SERIAL_WORD_LENGTH_8: return "8"; break;
        default: return "???";
    }
}

static const char *cli_rs422_stop_2_string(vtss_serial_stop_bits_t stop)
{
    switch(stop) {
        case VTSS_SERIAL_STOP_1:   return "1";   break;
        case VTSS_SERIAL_STOP_2:   return "2";   break;
        default: return "???";
    }
}

static const char *cli_rs422_flags_2_string(u32 flags)
{
    if ((flags & VTSS_SERIAL_FLOW_RTSCTS_RX) && (flags & VTSS_SERIAL_FLOW_RTSCTS_TX)) {
        return "rtscts";
    }
    else if (flags == 0) {
        return "none";
    }
    else {
        return "???";
    }
}

static const char *cli_rs422_mode_2_string(ptp_rs422_mode_t m)
{
    switch (m) {
        case VTSS_PTP_RS422_DISABLE: return "disable";
        case VTSS_PTP_RS422_MAIN_AUTO: return "main-auto";
        case VTSS_PTP_RS422_SUB: return "sub";
        case VTSS_PTP_RS422_MAIN_MAN: return "main-man";
        case VTSS_PTP_RS422_CALIB: return "calib";
        default: return "unknown";
    }
}

static const char *cli_rs422_proto_2_string(ptp_rs422_protocol_t m)
{
    switch (m) {
        case VTSS_PTP_RS422_PROTOCOL_SER_POLYT: return "ser";
        case VTSS_PTP_RS422_PROTOCOL_SER_ZDA  : return "ser proto zda";
        case VTSS_PTP_RS422_PROTOCOL_SER_RMC  : return "ser proto rmc";
        case VTSS_PTP_RS422_PROTOCOL_PIM      : return "pim";
        default: return "unknown";
    }
}

static const char *cli_pps_sync_mode_2_string(vtss_1pps_sync_mode_t m)
{
    switch (m) {
        case VTSS_PTP_1PPS_SYNC_MAIN_MAN: return "main-man";
        case VTSS_PTP_1PPS_SYNC_MAIN_AUTO: return "main-auto";
        case VTSS_PTP_1PPS_SYNC_SUB: return "sub";
        case VTSS_PTP_1PPS_SYNC_DISABLE: return "disable";
        default: return "unknown";
    }
}

static const char *cli_pps_ser_tod_2_string(vtss_1pps_ser_tod_mode_t m)
{
    switch (m) {
        case VTSS_PTP_1PPS_SER_TOD_MAN: return "ser-man";
        case VTSS_PTP_1PPS_SER_TOD_AUTO: return "ser-auto";
        case VTSS_PTP_1PPS_SER_TOD_DISABLE: return " ";
        default: return "unknown";
    }
}

static const char *cli_adj_method_2_string(vtss_appl_ptp_preferred_adj_t m)
{
    switch (m) {
        case VTSS_APPL_PTP_PREFERRED_ADJ_LTC: return "ltc";
        case VTSS_APPL_PTP_PREFERRED_ADJ_SINGLE: return "single";
        case VTSS_APPL_PTP_PREFERRED_ADJ_INDEPENDENT: return "independent";
        case VTSS_APPL_PTP_PREFERRED_ADJ_COMMON: return "common";
        case VTSS_APPL_PTP_PREFERRED_ADJ_AUTO: return "auto";
        default: return "unknown";
    }
}

static const char *pin_mode_2_string(vtss_appl_ptp_ext_io_pin_cfg_t pin)
{
    switch (pin) {
        case VTSS_APPL_PTP_IO_MODE_ONE_PPS_OUTPUT: return "pps-output";
        case VTSS_APPL_PTP_IO_MODE_WAVEFORM_OUTPUT: return "waveform-output";
        case VTSS_APPL_PTP_IO_MODE_ONE_PPS_LOAD: return "load";
        case VTSS_APPL_PTP_IO_MODE_ONE_PPS_SAVE: return "save";
        default: return "";
    }
}

/***************************************************************************/
/* ICFG callback functions */
/****************************************************************************/
#ifdef VTSS_SW_OPTION_ICFG
static mesa_rc ptp_icfg_global_conf(const vtss_icfg_query_request_t *req,
                                    vtss_icfg_query_result_t *result)
{
    char str1 [40];
    char buf1[20];
    char mep_txt[20];
    char clock_domain_txt[20];
    char dscp_txt[20];
    uint ix;

    int inst;
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    vtss_appl_ptp_clock_status_default_ds_t default_ds_status;

    vtss_appl_ptp_clock_config_default_ds_t default_default_ds;
    vtss_appl_ptp_clock_status_default_ds_t default_default_ds_status;
    vtss_appl_ptp_clock_timeproperties_ds_t prop;
    vtss_appl_ptp_clock_timeproperties_ds_t default_prop;
    vtss_appl_ptp_virtual_port_config_t virtual_port_cfg;
    vtss_appl_ptp_virtual_port_config_t default_virtual_port_cfg;
    vtss_appl_ptp_clock_filter_config_t default_filter_params;
    vtss_appl_ptp_clock_servo_config_t servo;
    vtss_appl_ptp_clock_servo_config_t default_servo;
    vtss_appl_ptp_unicast_slave_config_t uni_slave_cfg;
    vtss_appl_ptp_ext_clock_mode_t ext_clk_mode;
    vtss_appl_ptp_ext_clock_mode_t default_ext_clk_mode;
    vtss_appl_ptp_clock_slave_config_t slave_cfg;
    vtss_appl_ptp_clock_slave_config_t default_slave_cfg;

    vtss_ptp_rs422_conf_t rs422_mode;
    vtss_ptp_rs422_conf_t default_rs422_mode;
    vtss_serial_info_t rs422_serial_info;
    vtss_serial_info_t default_rs422_serial_info;

    ptp_clock_default_timeproperties_ds_get(&default_prop);
    vtss_appl_ptp_filter_default_parameters_get(&default_filter_params, VTSS_APPL_PTP_PROFILE_NO_PROFILE);
    vtss_appl_ptp_clock_servo_default_parameters_get(&default_servo, VTSS_APPL_PTP_PROFILE_NO_PROFILE);
    vtss_ext_clock_out_default_get(&default_ext_clk_mode);
    vtss_appl_ptp_clock_slave_default_config_get(&default_slave_cfg);
    ptp_1pps_get_default_baudrate(&default_rs422_serial_info);
    vtss_ext_clock_rs422_default_conf_get(&default_rs422_mode);

    /*Set internal TC mode */
    mesa_packet_internal_tc_mode_t tc_mode;
    if (!tod_tc_mode_get(&tc_mode)) {
        tc_mode = MESA_PACKET_INTERNAL_TC_MODE_30BIT;
    }
    if (tc_mode != MESA_PACKET_INTERNAL_TC_MODE_30BIT) {
        VTSS_RC(vtss_icfg_printf(result, "ptp tc-internal mode %d\n", tc_mode));
    }

    if (MESA_CAP(MESA_CAP_PHY_TS)) {
        if (MESA_CAP(MEBA_CAP_1588_REF_CLK_SEL)) {
            /* set 1588 referene clock */
            mesa_phy_ts_clockfreq_t freq;
    
            if (tod_ref_clock_freg_get(&freq)) {
                if (freq != MESA_PHY_TS_CLOCK_FREQ_250M) {
                VTSS_RC(vtss_icfg_printf(result, "ptp ref-clock %s\n",
                                         freq == MESA_PHY_TS_CLOCK_FREQ_125M ? "mhz125" :
                                         freq == MESA_PHY_TS_CLOCK_FREQ_15625M ? "mhz156p25" : "mhz250"));
                } else if(req->all_defaults) {
                    VTSS_RC(vtss_icfg_printf(result, "no ptp ref-clock\n"));
                }
            }
        }
    }

    /* show the local clock  */
    (void) vtss_appl_ptp_ext_clock_out_get(&ext_clk_mode);
    if (ext_clk_mode.one_pps_mode != default_ext_clk_mode.one_pps_mode ||
            ext_clk_mode.clock_out_enable != default_ext_clk_mode.clock_out_enable ||
            ext_clk_mode.adj_method != default_ext_clk_mode.adj_method ||
            ext_clk_mode.freq != default_ext_clk_mode.freq) {
        if (ext_clk_mode.clock_out_enable) {
            VTSS_RC(vtss_icfg_printf(result, "ptp ext %s ext %u %s\n",  one_pps_mode_2_string(ext_clk_mode.one_pps_mode),
                                     ext_clk_mode.freq, cli_adj_method_2_string(ext_clk_mode.adj_method)));
        } else {
            VTSS_RC(vtss_icfg_printf(result, "ptp ext %s %s\n", one_pps_mode_2_string(ext_clk_mode.one_pps_mode),
                                     cli_adj_method_2_string(ext_clk_mode.adj_method)));
        }
    } else if (req->all_defaults) {
        VTSS_RC(vtss_icfg_printf(result, "no ptp ext\n"));
    }

    for (inst = 0 ; inst < PTP_CLOCK_INSTANCES; inst++) {
        if ((vtss_appl_ptp_clock_config_default_ds_get(inst, &default_ds_cfg) == VTSS_RC_OK) &&
            (vtss_appl_ptp_clock_status_default_ds_get(inst, &default_ds_status) == VTSS_RC_OK))
        {
            ptp_get_default_clock_default_ds(&default_default_ds_status, &default_default_ds);

            if (default_ds_cfg.deviceType != default_default_ds.deviceType) {
                ptp_apply_profile_defaults_to_default_ds(&default_default_ds, default_ds_cfg.profile);
                sprintf(mep_txt, " mep %d", default_ds_cfg.mep_instance);
                sprintf(clock_domain_txt, " clock-domain %d", default_ds_cfg.clock_domain);
                sprintf(dscp_txt, " dscp %d", default_ds_cfg.dscp);
                VTSS_RC(vtss_icfg_printf(result, "ptp %d mode %s %s %s %s id %s vid %d %d%s%s%s%s%s\n",
                                             inst,
                                             device_type_2_string(default_ds_cfg.deviceType),
                                             default_ds_cfg.twoStepFlag ? "twostep" : "onestep",
                                             protocol_2_string(default_ds_cfg.protocol),
                                             default_ds_cfg.oneWay ? "oneway" : "twoway",
                                             ClockIdentityToString(default_ds_status.clockIdentity, str1),
                                             default_ds_cfg.configured_vid,
                                             default_ds_cfg.configured_pcp,
                                             default_ds_cfg.profile != VTSS_APPL_PTP_PROFILE_NO_PROFILE ? " profile " : "",
                                             default_ds_cfg.profile != VTSS_APPL_PTP_PROFILE_NO_PROFILE ? ClockProfileToString(default_ds_cfg.profile) : "",
                                             default_ds_cfg.mep_instance ? mep_txt : "",
                                             (default_ds_cfg.clock_domain != inst) ? clock_domain_txt : "",
                                             (default_ds_cfg.dscp != default_default_ds.dscp) ? dscp_txt : "" ));
                if ((default_ds_cfg.priority1 != default_default_ds.priority1) || (req->all_defaults)) {
                    VTSS_RC(vtss_icfg_printf(result, "ptp %d priority1 %d\n",
                                             inst, default_ds_cfg.priority1));
                }
                if ((default_ds_cfg.priority2 != default_default_ds.priority2) || (req->all_defaults)) {
                    VTSS_RC(vtss_icfg_printf(result, "ptp %d priority2 %d\n",
                                             inst, default_ds_cfg.priority2));
                }
                if ((default_ds_cfg.domainNumber != default_default_ds.domainNumber) || (req->all_defaults)) {
                    VTSS_RC(vtss_icfg_printf(result, "ptp %d domain %d\n",
                                             inst, default_ds_cfg.domainNumber));
                }
                if ((default_ds_cfg.localPriority != default_default_ds.localPriority) || (req->all_defaults)) {
                    if (default_ds_cfg.localPriority != default_default_ds.localPriority) {
                        VTSS_RC(vtss_icfg_printf(result, "ptp %d localpriority %d\n", inst, default_ds_cfg.localPriority));
                    } else {
                        VTSS_RC(vtss_icfg_printf(result, "no ptp %d localpriority\n", inst));
                    }
                }
                {
                    VTSS_RC(vtss_icfg_printf(result, " ptp %d filter-type %s\n",
                                             inst,
                                             filter_type_2_string(default_ds_cfg.filter_type)));
                }
                if ((default_ds_cfg.path_trace_enable != default_default_ds.path_trace_enable) || (req->all_defaults)) {
                    if (default_ds_cfg.path_trace_enable) {
                        VTSS_RC(vtss_icfg_printf(result, "ptp %d path-trace-enable\n", inst));
                    } else {
                        VTSS_RC(vtss_icfg_printf(result, "no ptp %d path-trace-enable\n", inst));
                    }
                }
                if (vtss_appl_ptp_clock_config_timeproperties_ds_get(inst, &prop) == VTSS_RC_OK) {
                    char leap_cfg_string[40];
                    if (prop.pendingLeap) {
                        struct tm* ptm;
                        time_t rawtime = (time_t) prop.leapDate * 86400;
                        ptm = gmtime(&rawtime);
                        snprintf(leap_cfg_string, sizeof(leap_cfg_string), " leap-pending %04d-%02d-%02d leap-%s",
                                 ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday, (prop.leapType == VTSS_APPL_PTP_LEAP_SECOND_59) ? "59" : "61");
                    }
                    else {
                        strcpy(leap_cfg_string, "");
                    }
                    if (memcmp(&default_prop,&prop, sizeof(prop)) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, "ptp %d time-property utc-offset %d %s%s%s%s%stime-source %u%s\n",
                                                 inst,
                                                 prop.currentUtcOffset,
                                                 prop.currentUtcOffsetValid ? "valid " : "",
                                                 prop.leap59 ? "leap-59 " : prop.leap61 ? "leap-61 " : "",
                                                 prop.timeTraceable ? "time-traceable " : "",
                                                 prop.frequencyTraceable ? "freq-traceable " : "",
                                                 prop.ptpTimescale ? "ptptimescale " : "",
                                                 prop.timeSource,
                                                 leap_cfg_string));
                    }
                }
                if (ptp_clock_config_virtual_port_config_get(inst, &virtual_port_cfg) == VTSS_RC_OK) {
                    vtss_appl_ptp_clock_config_default_virtual_port_config_get(&default_virtual_port_cfg);
                    if ((default_virtual_port_cfg.enable != virtual_port_cfg.enable) || req->all_defaults) {
                        if (virtual_port_cfg.enable) {
                            VTSS_RC(vtss_icfg_printf(result, "ptp %d virtual-port io-pin %d\n", inst, virtual_port_cfg.io_pin));
                        }
                        else {
                            VTSS_RC(vtss_icfg_printf(result, "no ptp %d virtual-port\n", inst));
                        }
                    }
                    if (default_virtual_port_cfg.clockQuality.clockClass != virtual_port_cfg.clockQuality.clockClass || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, "ptp %d virtual-port class %d\n", inst, virtual_port_cfg.clockQuality.clockClass));
                    }
                    if (default_virtual_port_cfg.clockQuality.clockAccuracy != virtual_port_cfg.clockQuality.clockAccuracy || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, "ptp %d virtual-port accuracy %d\n", inst, virtual_port_cfg.clockQuality.clockAccuracy));
                    }
                    if (default_virtual_port_cfg.clockQuality.offsetScaledLogVariance != virtual_port_cfg.clockQuality.offsetScaledLogVariance || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, "ptp %d virtual-port variance %d\n", inst, virtual_port_cfg.clockQuality.offsetScaledLogVariance));
                    }
                    if ((default_virtual_port_cfg.localPriority != virtual_port_cfg.localPriority) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, "ptp %d virtual-port local-priority %d\n", inst, virtual_port_cfg.localPriority));
                    }
                    if ((default_virtual_port_cfg.priority1 != virtual_port_cfg.priority1) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, "ptp %d virtual-port priority1 %d\n", inst, virtual_port_cfg.priority1));
                    }
                    if ((default_virtual_port_cfg.priority2 != virtual_port_cfg.priority2) || req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, "ptp %d virtual-port priority2 %d\n", inst, virtual_port_cfg.priority2));
                    }
                }
                if (vtss_appl_ptp_clock_servo_parameters_get(inst, &servo) == VTSS_RC_OK) {
                    if (servo.display_stats != default_servo.display_stats || req->all_defaults) {
                        if (servo.display_stats) {
                            VTSS_RC(vtss_icfg_printf(result, "ptp %d servo displaystates\n", inst));
                        } else {
                            VTSS_RC(vtss_icfg_printf(result, "no ptp %d servo displaystates\n", inst));
                        }
                    }
                    if (servo.srv_option != default_servo.srv_option || servo.synce_threshold != default_servo.synce_threshold ||
                            servo.synce_ap != default_servo.synce_ap || req->all_defaults) {
                        if (servo.srv_option) {
                            VTSS_RC(vtss_icfg_printf(result, "ptp %d clk sync %d ap %d\n", inst, servo.synce_threshold, servo.synce_ap ));
                        } else {
                            VTSS_RC(vtss_icfg_printf(result, "no ptp %d clk\n", inst));
                        }
                    }
                    ix = 0;
                    while (vtss_appl_ptp_clock_config_unicast_slave_config_get(inst, ix++, &uni_slave_cfg) == VTSS_RC_OK) {
                        if (uni_slave_cfg.ip_addr) {
                            VTSS_RC(vtss_icfg_printf(result, "ptp %d uni %d duration %d %s\n", inst, ix-1, uni_slave_cfg.duration, misc_ipv4_txt(uni_slave_cfg.ip_addr, buf1) ));
                        } else if (req->all_defaults) {
                            VTSS_RC(vtss_icfg_printf(result, "no ptp %d uni %d\n", inst, ix-1));
                        }
                    }
                }
                if (vtss_appl_ptp_clock_slave_config_get(inst, &slave_cfg) == VTSS_RC_OK) {
                    if (slave_cfg.offset_fail != default_slave_cfg.offset_fail ||
                        slave_cfg.offset_ok != default_slave_cfg.offset_ok ||
                        slave_cfg.stable_offset != default_slave_cfg.stable_offset ||
                        req->all_defaults)
                    {
                        VTSS_RC(vtss_icfg_printf(result, "ptp %d slave-cfg offset-fail %d offset-ok %d stable-offset %d\n", inst, slave_cfg.offset_fail, slave_cfg.offset_ok, slave_cfg.stable_offset));
                    }
                }
                
                if (MESA_CAP(MESA_CAP_SYNCE_ANN_AUTO_TRANSMIT)) {
                    bool afi_enable;
                    if (ptp_afi_mode_get(inst, true, &afi_enable) == VTSS_RC_OK) {
                        if (!afi_enable) {
                            VTSS_RC(vtss_icfg_printf(result, "no ptp %d afi-announce\n", inst));
                        }
                    }
                    if (ptp_afi_mode_get(inst, false, &afi_enable) == VTSS_RC_OK) {
                        if (!afi_enable) {
                            VTSS_RC(vtss_icfg_printf(result, "no ptp %d afi-sync\n", inst));
                        }
                    }
                }
                
            } else if (req->all_defaults) {
                VTSS_RC(vtss_icfg_printf(result, "no ptp %d mode %s\n",
                                         inst,
                                         device_type_2_string(default_ds_cfg.deviceType)));
            }
        }
    }

    /* holdover spec */
    {
        vtss_appl_ho_spec_conf_t ho_spec;
        vtss_ho_spec_conf_get(&ho_spec);
        if ((ho_spec.cat1 != 0 || ho_spec.cat2 != 0 || ho_spec.cat3 != 0) || (req->all_defaults)) {
            if (ho_spec.cat1 != 0 || ho_spec.cat2 != 0 || ho_spec.cat3 != 0) {
                VTSS_RC(vtss_icfg_printf(result, "ptp ho-spec cat1 %d cat2 %d cat3 %d\n", ho_spec.cat1, ho_spec.cat2, ho_spec.cat3));
            } else {
                VTSS_RC(vtss_icfg_printf(result, "no ptp ho-spec\n"));
            }
        }
    }

    /* global config */
    /* set System time <-> PTP time synchronization mode */
    vtss_appl_ptp_system_time_sync_conf_t conf;

    VTSS_RC(vtss_appl_ptp_system_time_sync_mode_get(&conf));
    if (conf.mode != VTSS_APPL_PTP_SYSTEM_TIME_NO_SYNC) {
        VTSS_RC(vtss_icfg_printf(result, "ptp system-time %s\n",
                                 conf.mode == VTSS_APPL_PTP_SYSTEM_TIME_SYNC_GET ? "get" : "set"));
    } else if(req->all_defaults) {
        VTSS_RC(vtss_icfg_printf(result, "no ptp system-time\n"));
    }

    if (MESA_CAP(MESA_CAP_TS_PTP_RS422)) {
        if (ptp_1pps_get_baudrate(&rs422_serial_info) == VTSS_RC_OK) {
            if (memcmp(&rs422_serial_info, &default_rs422_serial_info, sizeof(vtss_serial_info_t)) != 0 || req->all_defaults) {
                VTSS_RC(vtss_icfg_printf(result, "ptp rs422 baudrate %s parity %s wordlength %s stopbits %s flowctrl %s\n",
                                         cli_rs422_baudrate_2_string(rs422_serial_info.baud),
                                         cli_rs422_parity_2_string(rs422_serial_info.parity),
                                         cli_rs422_word_length_2_string(rs422_serial_info.word_length),
                                         cli_rs422_stop_2_string(rs422_serial_info.stop),
                                         cli_rs422_flags_2_string(rs422_serial_info.flags)));
            }
        }
        vtss_ext_clock_rs422_conf_get(&rs422_mode);
        char buf[75]; // Buffer for storage of string
        const char *port_txt;
        const char *int_txt;
        if (rs422_mode.mode != default_rs422_mode.mode) {
            if (rs422_mode.proto == VTSS_PTP_RS422_PROTOCOL_PIM) {
                int_txt = "interface ";
                port_txt = icli_port_info_txt(VTSS_USID_START, iport2uport(rs422_mode.port), buf);
            } else {
                port_txt = "";
                int_txt = "";
            }
            if (rs422_mode.mode == VTSS_PTP_RS422_MAIN_MAN) {
                VTSS_RC(vtss_icfg_printf(result, "ptp rs422 %s pps-delay %u %s %s%s\n",  cli_rs422_mode_2_string(rs422_mode.mode),
                                         rs422_mode.delay, cli_rs422_proto_2_string(rs422_mode.proto), int_txt, port_txt));
            } else {
                VTSS_RC(vtss_icfg_printf(result, "ptp rs422 %s %s %s%s\n",  cli_rs422_mode_2_string(rs422_mode.mode),
                                         cli_rs422_proto_2_string(rs422_mode.proto), int_txt, port_txt));
            }
        } else if (req->all_defaults) {
            VTSS_RC(vtss_icfg_printf(result, "no ptp rs422\n"));
        }
    }

    if (MESA_CAP(MESA_CAP_TS_PPS_VIA_CONFIGURABLE_IO_PINS)) {
        // Io-PIN configuration
        mesa_port_no_t one_pps_slave_port_no;
        vtss_appl_ptp_ext_io_mode_t mode;
        vtss_appl_ptp_ext_io_mode_t default_mode;
        int pin_idx;
        for (pin_idx = 0; pin_idx < MESA_CAP(MESA_CAP_TS_IO_CNT); pin_idx++) {
            if ((vtss_appl_ptp_io_pin_conf_get(pin_idx, &mode) == VTSS_RC_OK) &&
                    (vtss_appl_ptp_io_pin_conf_default_get(pin_idx, &default_mode) == VTSS_RC_OK))
            {
                if (mode.pin != default_mode.pin || mode.domain != default_mode.domain || mode.freq != default_mode.freq || req->all_defaults) {
                    char buf[75]; // Buffer for storage of string
                    const char *port_txt;
                    const char *int_txt;
                    one_pps_slave_port_no = uport2iport(mode.one_pps_slave_port_number);
                    if (one_pps_slave_port_no < MESA_CAP(MESA_CAP_PORT_CNT)) {
                        int_txt = "interface ";
                        port_txt = icli_port_info_txt(VTSS_USID_START, mode.one_pps_slave_port_number, buf);
                    } else {
                        port_txt = "";
                        int_txt = "";
                    }
                    if (mode.pin != VTSS_APPL_PTP_IO_MODE_ONE_PPS_DISABLE) {
                        VTSS_RC(vtss_icfg_printf(result, "ptp io-pin %d %s domain %d freq %d %s%s\n",  pin_idx, pin_mode_2_string(mode.pin), mode.domain, mode.freq,
                                             int_txt, port_txt));
                    } else if (req->all_defaults) {
                        VTSS_RC(vtss_icfg_printf(result, "no ptp io-pin %d\n",  pin_idx));
                    }
                }
            }
        }
    }

    return VTSS_RC_OK;
}

static mesa_rc ptp_icfg_interface_conf(const vtss_icfg_query_request_t *req,
                                       vtss_icfg_query_result_t *result)
{
    int inst;
    vtss_appl_ptp_config_port_ds_t ps;
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    vtss_isid_t isid;
    mesa_port_no_t uport;
    vtss_ifindex_t ifindex;
    char int_txt[10];
    vtss_1pps_sync_conf_t pps_sync_mode;
    vtss_1pps_closed_loop_conf_t pps_cl_mode;
    mesa_port_no_t iport;
    i8 buf[75]; // Buffer for storage of string

    isid = req->instance_id.port.isid;
    uport = req->instance_id.port.begin_uport;
    (void) vtss_ifindex_from_port(isid, uport2iport(uport), &ifindex);
    T_IG(VTSS_TRACE_GRP_PTP_ICLI, "isid %d, port %d", isid, uport);
    if (msg_switch_configurable(isid)) {
        for (inst = 0 ; inst < PTP_CLOCK_INSTANCES; inst++) {
            if (vtss_appl_ptp_clock_config_default_ds_get(inst, &default_ds_cfg) == VTSS_RC_OK) {
                if (default_ds_cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
                    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &ps) == VTSS_RC_OK) && (ps.enabled == 1)) {
                        VTSS_RC(vtss_icfg_printf(result, " ptp %d %s\n",
                                                 inst,
                                                 ps.portInternal ? "internal" : ""));
                        if (ps.logAnnounceInterval == 126) {
                            snprintf(int_txt, sizeof(int_txt), "default");
                        } else if (ps.logAnnounceInterval == 127) {
                            snprintf(int_txt, sizeof(int_txt), "stop");
                        } else {
                            snprintf(int_txt, sizeof(int_txt),"%d", ps.logAnnounceInterval);
                        }
                        VTSS_RC(vtss_icfg_printf(result, " ptp %d announce interval %s timeout %d\n",
                                                 inst,
                                                 int_txt,
                                                 ps.announceReceiptTimeout));
                        if (ps.logSyncInterval == 126) {
                            snprintf(int_txt, sizeof(int_txt), "default");
                        } else if (ps.logSyncInterval == 127) {
                            snprintf(int_txt, sizeof(int_txt), "stop");
                        } else {
                            snprintf(int_txt, sizeof(int_txt),"%d", ps.logSyncInterval);
                        }
                        VTSS_RC(vtss_icfg_printf(result, " ptp %d sync-interval %s\n",
                                                 inst,
                                                 int_txt));
                        VTSS_RC(vtss_icfg_printf(result, " ptp %d delay-mechanism %s\n",
                                                 inst,
                                                 ps.delayMechanism == DELAY_MECH_E2E ? "e2e" : "p2p"));
                        if (ps.logMinPdelayReqInterval == 126) {
                            snprintf(int_txt, sizeof(int_txt), "default");
                        } else if (ps.logMinPdelayReqInterval == 127) {
                            snprintf(int_txt, sizeof(int_txt), "stop");
                        } else {
                            snprintf(int_txt, sizeof(int_txt),"%d", ps.logMinPdelayReqInterval);
                        }
                        VTSS_RC(vtss_icfg_printf(result, " ptp %d delay-req interval %s\n",
                                                 inst,
                                                 int_txt));
                        VTSS_RC(vtss_icfg_printf(result, " ptp %d delay-asymmetry %d\n",
                                                 inst,
                                                 VTSS_INTERVAL_NS(ps.delayAsymmetry)));
                        VTSS_RC(vtss_icfg_printf(result, " ptp %d ingress-latency %d\n",
                                                 inst,
                                                 VTSS_INTERVAL_NS(ps.ingressLatency)));
                        VTSS_RC(vtss_icfg_printf(result, " ptp %d egress-latency %d\n",
                                                 inst,
                                                 VTSS_INTERVAL_NS(ps.egressLatency)));
                        if (ps.localPriority != 128 || req->all_defaults) {
                            if (ps.localPriority != 128) {
                                VTSS_RC(vtss_icfg_printf(result, " ptp %d localpriority %d\n", inst, ps.localPriority));
                            } else {
                                VTSS_RC(vtss_icfg_printf(result, " no ptp %d localpriority\n", inst));
                            }
                        }
                        if (ps.dest_adr_type != VTSS_APPL_PTP_PROTOCOL_SELECT_DEFAULT || req->all_defaults) {
                            if (ps.dest_adr_type == VTSS_APPL_PTP_PROTOCOL_SELECT_LINK_LOCAL) {
                                VTSS_RC(vtss_icfg_printf(result, " ptp %d mcast-dest link-local\n", inst));
                            } else {
                                VTSS_RC(vtss_icfg_printf(result, " ptp %d mcast-dest default\n", inst));
                            }
                        }
                        if (ps.notSlave || req->all_defaults) {
                            if (ps.notSlave) {
                                VTSS_RC(vtss_icfg_printf(result, " ptp %d not-slave\n", inst));
                            } else {
                                VTSS_RC(vtss_icfg_printf(result, " no ptp %d not-slave\n", inst));
                            }
                        }
                        if (ps.twoStepOverride != VTSS_APPL_PTP_TWO_STEP_OVERRIDE_NONE || req->all_defaults) {
                            if (ps.twoStepOverride != VTSS_APPL_PTP_TWO_STEP_OVERRIDE_NONE) {
                                if (ps.twoStepOverride == VTSS_APPL_PTP_TWO_STEP_OVERRIDE_TRUE) {
                                    VTSS_RC(vtss_icfg_printf(result, " ptp %d two-step true\n", inst));
                                } else {
                                    VTSS_RC(vtss_icfg_printf(result, " ptp %d two-step false\n", inst));
                                }
                            } else {
                                VTSS_RC(vtss_icfg_printf(result, " no ptp %d two-step\n", inst));
                            }
                        }
                        //port_delay_threshold
                        if (default_ds_cfg.profile == VTSS_APPL_PTP_PROFILE_IEEE_802_1AS && (ps.c_802_1as.neighborPropDelayThresh != (800LL<<16) || req->all_defaults)) {
                            if (ps.c_802_1as.neighborPropDelayThresh != VTSS_MAX_TIMEINTERVAL) {
                                VTSS_RC(vtss_icfg_printf(result, " ptp %d delay-thresh %d\n", inst, (u32)(ps.c_802_1as.neighborPropDelayThresh>>16)));
                            } else {
                                VTSS_RC(vtss_icfg_printf(result, " no ptp %d delay-thresh\n", inst));
                            }
                        }
                        //sync rx timeout
                        if (default_ds_cfg.profile == VTSS_APPL_PTP_PROFILE_IEEE_802_1AS && (ps.c_802_1as.syncReceiptTimeout != 3 || req->all_defaults)) {
                            if (ps.c_802_1as.syncReceiptTimeout != 3) {
                                VTSS_RC(vtss_icfg_printf(result, " ptp %d sync-rx-to %d\n", inst, ps.c_802_1as.syncReceiptTimeout));
                            } else {
                                VTSS_RC(vtss_icfg_printf(result, " no ptp %d sync-rx-to\n", inst));
                            }
                        }
                        //max allowed lost PDelay responses
                        if (default_ds_cfg.profile == VTSS_APPL_PTP_PROFILE_IEEE_802_1AS && (ps.c_802_1as.allowedLostResponses != DEFAULT_MAX_CONTIGOUS_MISSED_PDELAY_RESPONSE || req->all_defaults)) {
                            if (ps.c_802_1as.allowedLostResponses != DEFAULT_MAX_CONTIGOUS_MISSED_PDELAY_RESPONSE) {
                                VTSS_RC(vtss_icfg_printf(result, " ptp %d allow-lost-resp %d\n", inst, ps.c_802_1as.allowedLostResponses));
                            } else {
                                VTSS_RC(vtss_icfg_printf(result, " no ptp %d allow-lost-resp\n", inst));
                            }
                        }
                    }
                } else if (req->all_defaults) {
                    VTSS_RC(vtss_icfg_printf(result, " no ptp %d\n",
                                             inst));
                }
            }
        }
        if (MESA_CAP(MESA_CAP_PHY_TS)) {
            /* one pps configuration functions */
            iport = req->instance_id.port.begin_iport;
            VTSS_RC(ptp_1pps_sync_mode_get(iport, &pps_sync_mode));
            if (pps_sync_mode.mode != VTSS_PTP_1PPS_SYNC_DISABLE) {
                VTSS_RC(vtss_icfg_printf(result, " ptp pps-sync %s cable-asy %d pps-phase %d %s\n",
                                         cli_pps_sync_mode_2_string(pps_sync_mode.mode),
                                         pps_sync_mode.cable_asy,
                                         pps_sync_mode.pulse_delay,
                                         cli_pps_ser_tod_2_string(pps_sync_mode.serial_tod)));
            } else if (req->all_defaults) {
                VTSS_RC(vtss_icfg_printf(result, " no ptp pps-sync\n"));
            }
            VTSS_RC(ptp_1pps_closed_loop_mode_get(iport, &pps_cl_mode));
            if (pps_cl_mode.mode == VTSS_PTP_1PPS_CLOSED_LOOP_AUTO) {
                VTSS_RC(vtss_icfg_printf(result, " ptp pps-delay auto master-port interface %s\n",
                                         icli_port_info_txt(VTSS_USID_START, iport2uport(pps_cl_mode.master_port), buf)));
            } else if (pps_cl_mode.mode == VTSS_PTP_1PPS_CLOSED_LOOP_MAN) {
                    VTSS_RC(vtss_icfg_printf(result, " ptp pps-delay man cable-delay %d\n",
                                             pps_cl_mode.cable_delay));
            } else if (req->all_defaults) {
                VTSS_RC(vtss_icfg_printf(result, " no ptp pps-delay\n"));
            }
        }
    }
    return VTSS_RC_OK;
}

/* ICFG Initialization function */
mesa_rc ptp_icfg_init(void)
{
    VTSS_RC(vtss_icfg_query_register(VTSS_ICFG_PTP_GLOBAL_CONF, "ptp", ptp_icfg_global_conf));
    VTSS_RC(vtss_icfg_query_register(VTSS_ICFG_PTP_INTERFACE_CONF, "ptp", ptp_icfg_interface_conf));
    return VTSS_RC_OK;
}
#endif // VTSS_SW_OPTION_ICFG
/****************************************************************************/
/*  End of file.                                                            */
/****************************************************************************/
