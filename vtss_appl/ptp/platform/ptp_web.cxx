/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

#include "web_api.h"
#include "conf_api.h"
#include "port_api.h"
#include "ptp_api.h"
#include "vtss_ptp_types.h"
#include "vtss_ptp_local_clock.h"
#include "vtss_tod_api.h"
#include "time.h"

#ifdef VTSS_SW_OPTION_PRIV_LVL
#include "vtss_privilege_api.h"
#include "vtss_privilege_web_api.h"
#endif

#if defined(VTSS_SW_OPTION_ZLS30387)
#include "zl_3038x_api_pdv_api.h"
#endif

/* =================
 * Trace definitions
 * -------------- */
#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_WEB
#include <vtss_trace_api.h>
/* ============== */

#define PTP_WEB_BUF_LEN 300

const char *ptp_profiles[] = { "No Profile", "1588", "G8265.1", "G8275.1", "802.1AS" };

static const char *ptp_two_step_override_disp(vtss_appl_ptp_twostep_override_t m)
{
    switch (m) {
        case VTSS_APPL_PTP_TWO_STEP_OVERRIDE_NONE:
            return "Clock Def.";
        case VTSS_APPL_PTP_TWO_STEP_OVERRIDE_FALSE:
            return "False";
        case VTSS_APPL_PTP_TWO_STEP_OVERRIDE_TRUE:
            return "True";
        default:
            return "unknown";
    }
}

static const char *ptp_one_pps_mode_disp(u8 m)
{
    switch (m) {
        case VTSS_APPL_PTP_ONE_PPS_DISABLE:
            return "Disable";
        case VTSS_APPL_PTP_ONE_PPS_OUTPUT:
            return "Output";
        case VTSS_APPL_PTP_ONE_PPS_INPUT:
            return "Input";
        case VTSS_APPL_PTP_ONE_PPS_OUTPUT_INPUT:
            return "OutInput";
        default:
            return "unknown";
    }
}

static bool ptp_get_one_pps_mode_from_str(char *one_pps_mode_str, u8 *one_pps_mode)
{
    if(!strcmp(one_pps_mode_str, "Disable")) {
        *one_pps_mode = VTSS_APPL_PTP_ONE_PPS_DISABLE;
        return true;
    }
    if(!strcmp(one_pps_mode_str,"Output")) {
        *one_pps_mode = VTSS_APPL_PTP_ONE_PPS_OUTPUT;
        return true;
    }
    if(!strcmp(one_pps_mode_str,"Input")) {
        *one_pps_mode = VTSS_APPL_PTP_ONE_PPS_INPUT;
        return true;
    }
    if(!strcmp(one_pps_mode_str,"OutInput")) {
        *one_pps_mode = VTSS_APPL_PTP_ONE_PPS_OUTPUT_INPUT;
        return true;
    }
    return false;
}

static const char *ptp_bool_disp(bool b)
{
    return (b ? "True" : "False");
}

static int ptp_get_bool_value(char *bool_var)
{
    if (!strcmp(bool_var,"True"))
        return true;
    else
        return false;
}

static vtss_appl_ptp_leap_second_type_t ptp_get_leap_type_value(char *leap_type_var)
{
    if (!strcmp(leap_type_var,"leap59"))
        return VTSS_APPL_PTP_LEAP_SECOND_59;
    else
        return VTSS_APPL_PTP_LEAP_SECOND_61;
}

static const char *ptp_state_disp(u8 b)
{
    switch (b) {
        case VTSS_APPL_PTP_COMM_STATE_IDLE:
            return "IDLE";
        case VTSS_APPL_PTP_COMM_STATE_INIT:
            return "INIT";
        case VTSS_APPL_PTP_COMM_STATE_CONN:
            return "CONN";
        case VTSS_APPL_PTP_COMM_STATE_SELL:
            return "SELL";
        case VTSS_APPL_PTP_COMM_STATE_SYNC:
            return "SYNC";
        default:
            return "?";
    }
}

static const char *ptp_protocol_disp(u8 p)
{
    switch (p) {
        case VTSS_APPL_PTP_PROTOCOL_ETHERNET:
            return "Ethernet";
        case VTSS_APPL_PTP_PROTOCOL_ETHERNET_MIXED:
            return "EthernetMixed";
        case VTSS_APPL_PTP_PROTOCOL_IP4MULTI:
            return "IPv4Multi";
        case VTSS_APPL_PTP_PROTOCOL_IP4MIXED:
            return "IPv4Mixed";
        case VTSS_APPL_PTP_PROTOCOL_IP4UNI:
            return "IPv4Uni";
        case VTSS_APPL_PTP_PROTOCOL_OAM:
            return "Oam";
        case VTSS_APPL_PTP_PROTOCOL_ONE_PPS:
            return "OnePPS";
        default:
            return "?";
    }
}


static vtss_appl_ptp_protocol_t ptp_get_protocol(char *prot)
{
    if (!strcmp(prot,"Ethernet")) {
        return VTSS_APPL_PTP_PROTOCOL_ETHERNET;
    }
    else if (!strcmp(prot,"EthernetMixed")) {
        return VTSS_APPL_PTP_PROTOCOL_ETHERNET_MIXED;
    }
    else if (!strcmp(prot,"IPv4Multi")) {
        return VTSS_APPL_PTP_PROTOCOL_IP4MULTI;
    }
    else if (!strcmp(prot,"IPv4Mixed")) {
        return VTSS_APPL_PTP_PROTOCOL_IP4MIXED;
    }
    else if (!strcmp(prot,"IPv4Uni")) {
        return VTSS_APPL_PTP_PROTOCOL_IP4UNI;
    }
    else if (!strcmp(prot,"Oam")) {
        return VTSS_APPL_PTP_PROTOCOL_OAM;
    }
    else if (!strcmp(prot,"OnePPS")) {
        return VTSS_APPL_PTP_PROTOCOL_ONE_PPS;
    }
    return VTSS_APPL_PTP_PROTOCOL_ETHERNET;
}

static const char *ptp_filter_type_disp(u8 p)
{
    switch (p) {
#if defined(VTSS_SW_OPTION_ZLS30387)
        case PTP_FILTERTYPE_ACI_DEFAULT:
            return "ACI_DEFAULT";
        case PTP_FILTERTYPE_ACI_FREQ_XO:
            return "ACI_FREQ_XO";
        case PTP_FILTERTYPE_ACI_PHASE_XO:
            return "ACI_PHASE_XO";
        case PTP_FILTERTYPE_ACI_FREQ_TCXO:
            return "ACI_FREQ_TCXO";
        case PTP_FILTERTYPE_ACI_PHASE_TCXO:
            return "ACI_PHASE_TCXO";
        case PTP_FILTERTYPE_ACI_FREQ_OCXO_S3E:
            return "ACI_FREQ_OCXO_S3E";
        case PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E:
            return "ACI_PHASE_OCXO_S3E";
        case PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_FREQ:
            return "ACI_BC_PARTIAL_ON_PATH_FREQ";
        case PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_PHASE:
            return "ACI_BC_PARTIAL_ON_PATH_PHASE";
        case PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_FREQ:
            return "ACI_BC_FULL_ON_PATH_FREQ";
        case PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE:
            return "ACI_BC_FULL_ON_PATH_PHASE";
        case PTP_FILTERTYPE_ACI_FREQ_ACCURACY_FDD:
            return "ACI_FREQ_ACCURACY_FDD";
        case PTP_FILTERTYPE_ACI_FREQ_ACCURACY_XDSL:
            return "ACI_FREQ_ACCURACY_XDSL";
        case PTP_FILTERTYPE_ACI_ELEC_FREQ:
            return "ACI_ELEC_FREQ";
        case PTP_FILTERTYPE_ACI_ELEC_PHASE:
            return "ACI_ELEC_PHASE";
        case PTP_FILTERTYPE_ACI_PHASE_TCXO_Q:
            return "ACI_PHASE_TCXO_Q";
        case PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_Q:
            return "ACI_BC_FULL_ON_PATH_PHASE_Q";
        case PTP_FILTERTYPE_ACI_PHASE_RELAXED_C60W:
            return "ACI_PHASE_RELAXED_C60W";
        case PTP_FILTERTYPE_ACI_PHASE_RELAXED_C150:
            return "ACI_PHASE_RELAXED_C150";
        case PTP_FILTERTYPE_ACI_PHASE_RELAXED_C180:
             return "ACI_PHASE_RELAXED_C180";
        case PTP_FILTERTYPE_ACI_PHASE_RELAXED_C240:
             return "ACI_PHASE_RELAXED_C240";
        case PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_BETA:
             return "ACI_BC_FULL_ON_PATH_PHASE_BETA";
        case PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E_R4_6_1:
             return "ACI_PHASE_OCXO_S3E_R4_6_1";
        case PTP_FILTERTYPE_ACI_BASIC_PHASE:
             return "ACI_BASIC_PHASE";
        case PTP_FILTERTYPE_ACI_BASIC_PHASE_LOW:
             return "ACI_BASIC_PHASE_LOW";
#endif // defined(VTSS_SW_OPTION_ZLS30387)
        default:
            return "?";
    }
}

static int ptp_get_filter_type(char *filt)
{
#if defined(VTSS_SW_OPTION_ZLS30387)
    if (!strcmp(filt, "ACI_DEFAULT"))
        return PTP_FILTERTYPE_ACI_DEFAULT;
    if (!strcmp(filt, "ACI_FREQ_XO"))
        return PTP_FILTERTYPE_ACI_FREQ_XO;
    if (!strcmp(filt, "ACI_PHASE_XO"))
        return PTP_FILTERTYPE_ACI_PHASE_XO;
    if (!strcmp(filt, "ACI_FREQ_TCXO"))
        return PTP_FILTERTYPE_ACI_FREQ_TCXO;
    if (!strcmp(filt, "ACI_PHASE_TCXO"))
        return PTP_FILTERTYPE_ACI_PHASE_TCXO;
    if (!strcmp(filt, "ACI_FREQ_OCXO_S3E"))
        return PTP_FILTERTYPE_ACI_FREQ_OCXO_S3E;
    if (!strcmp(filt, "ACI_PHASE_OCXO_S3E"))
        return PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E;
    if (!strcmp(filt, "ACI_BC_PARTIAL_ON_PATH_FREQ"))
        return PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_FREQ;
    if (!strcmp(filt, "ACI_BC_PARTIAL_ON_PATH_PHASE"))
        return PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_PHASE;
    if (!strcmp(filt, "ACI_BC_FULL_ON_PATH_FREQ"))
        return PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_FREQ;
    if (!strcmp(filt, "ACI_BC_FULL_ON_PATH_PHASE"))
        return PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE;
    if (!strcmp(filt, "ACI_FREQ_ACCURACY_FDD"))
        return PTP_FILTERTYPE_ACI_FREQ_ACCURACY_FDD;
    if (!strcmp(filt, "ACI_FREQ_ACCURACY_XDSL"))
        return PTP_FILTERTYPE_ACI_FREQ_ACCURACY_XDSL;
    if (!strcmp(filt, "ACI_ELEC_FREQ"))
        return PTP_FILTERTYPE_ACI_ELEC_FREQ;
    if (!strcmp(filt, "ACI_ELEC_PHASE"))
        return PTP_FILTERTYPE_ACI_ELEC_PHASE;
    if (!strcmp(filt, "ACI_PHASE_TCXO_Q"))
        return PTP_FILTERTYPE_ACI_PHASE_TCXO_Q;
    if (!strcmp(filt, "ACI_BC_FULL_ON_PATH_PHASE_Q"))
        return PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_Q;
    if (!strcmp(filt, "ACI_PHASE_RELAXED_C60W"))
        return PTP_FILTERTYPE_ACI_PHASE_RELAXED_C60W;
    if (!strcmp(filt, "ACI_PHASE_RELAXED_C150"))
        return PTP_FILTERTYPE_ACI_PHASE_RELAXED_C150;
    if (!strcmp(filt, "ACI_PHASE_RELAXED_C180"))
        return PTP_FILTERTYPE_ACI_PHASE_RELAXED_C180;
    if (!strcmp(filt, "ACI_PHASE_RELAXED_C240"))
        return PTP_FILTERTYPE_ACI_PHASE_RELAXED_C240;
    if (!strcmp(filt, "ACI_BC_FULL_ON_PATH_PHASE_BETA"))
        return PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_BETA;
    if (!strcmp(filt, "ACI_PHASE_OCXO_S3E_R4_6_1"))
        return PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E_R4_6_1;
    if (!strcmp(filt, "ACI_BASIC_PHASE"))
        return PTP_FILTERTYPE_ACI_BASIC_PHASE;
    if (!strcmp(filt, "ACI_BASIC_PHASE_LOW"))
        return PTP_FILTERTYPE_ACI_BASIC_PHASE_LOW;
#endif // defined(VTSS_SW_OPTION_ZLS30387)
    return PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE;
}

static const char *ptp_delaymechanism_disp(uchar d)
{
    switch (d) {
        case 1:
            return "e2e";
        case 2:
            return "p2p";
        default:
            return "?\?\?";
    }
}

static bool ptp_get_delaymechanism_type(char *dmStr,u8 *mechType)
{
    bool rc = false;
    if ( 0 == strcmp(dmStr, "e2e")) {
        *mechType = 1;
        rc = true;
    } else if ( 0 == strcmp(dmStr, "p2p")) {
        *mechType = 2;
        rc =  true;
    }
    return rc;
}

static const char *clock_adjustment_method_txt(int m)
{
    switch (m) {
        case 0:
            return "Internal Timer";
        case 1:
            return "PTP DPLL";
        case 2:
            return "DAC option";
        case 3:
            return "Software";
        case 4:
            return "Synce DPLL";
        default:
            return "Unknown";
    }
}

static bool ptp_get_devtype_from_str(char * deviceTypeStr, vtss_appl_ptp_device_type_t *devType)
{
    bool rc = false;
    *devType = VTSS_APPL_PTP_DEVICE_NONE;
    if (0 == strcmp(deviceTypeStr,"Inactive")) {
        *devType = VTSS_APPL_PTP_DEVICE_NONE;
        rc = true;
    } else if (0 == strcmp(deviceTypeStr,"Ord-Bound")) {
        *devType = VTSS_APPL_PTP_DEVICE_ORD_BOUND;
        rc = true;
    } else if (0 == strcmp(deviceTypeStr,"P2pTransp")) {
        *devType = VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT;
        rc = true;
    } else if (0 == strcmp(deviceTypeStr,"E2eTransp")) {
        *devType = VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT;
        rc = true;
    } else if (0 == strcmp(deviceTypeStr,"Mastronly")) {
        *devType = VTSS_APPL_PTP_DEVICE_MASTER_ONLY;
        rc = true;
    } else if (0 == strcmp(deviceTypeStr,"Slaveonly")) {
        *devType = VTSS_APPL_PTP_DEVICE_SLAVE_ONLY;
        rc = true;
    } else if (0 == strcmp(deviceTypeStr,"BC-frontend")) {
        *devType = VTSS_APPL_PTP_DEVICE_BC_FRONTEND;
        rc = true;
    }

    return rc;
}
static const char *ptp_adj_method_disp(vtss_appl_ptp_preferred_adj_t m)
{
    switch (m) {
        case VTSS_APPL_PTP_PREFERRED_ADJ_LTC: return "LTC";
        case VTSS_APPL_PTP_PREFERRED_ADJ_SINGLE: return "Single";
        case VTSS_APPL_PTP_PREFERRED_ADJ_INDEPENDENT: return "Independent";
        case VTSS_APPL_PTP_PREFERRED_ADJ_COMMON: return "Common";
        case VTSS_APPL_PTP_PREFERRED_ADJ_AUTO: return "Auto";
        default: return "unknown";
    }
}

static bool ptp_get_adj_method_from_str(char * preferred_adjStr, vtss_appl_ptp_preferred_adj_t *m)
{
    bool rc = false;
    *m = VTSS_APPL_PTP_PREFERRED_ADJ_AUTO;
    if (0 == strcmp(preferred_adjStr,"LTC")) {
        *m = VTSS_APPL_PTP_PREFERRED_ADJ_LTC;
        rc = true;
    } else if (0 == strcmp(preferred_adjStr,"Single")) {
        *m = VTSS_APPL_PTP_PREFERRED_ADJ_SINGLE;
        rc = true;
    } else if (0 == strcmp(preferred_adjStr,"Independent")) {
        *m = VTSS_APPL_PTP_PREFERRED_ADJ_INDEPENDENT;
        rc = true;
    } else if (0 == strcmp(preferred_adjStr,"Common")) {
        *m = VTSS_APPL_PTP_PREFERRED_ADJ_COMMON;
        rc = true;
    } else if (0 == strcmp(preferred_adjStr,"Auto")) {
        *m = VTSS_APPL_PTP_PREFERRED_ADJ_AUTO;
        rc = true;
    }
    return rc;
}




















/****************************************************************************/
/*  Web Handler  functions                                                  */
/****************************************************************************/

static i32 handler_config_ptp(CYG_HTTPD_STATE* p)
{
    i32    ct;
    vtss_appl_ptp_device_type_t dev_type;
    u8     one_pps_mode;
    int    var_int;
    ulong  var_value;
    vtss_appl_ptp_preferred_adj_t adj_method;
    mesa_mac_t my_sysmac;
    uint inst;
    uint new_clock_inst;
    ptp_clock_default_ds_t default_ds;
    vtss_appl_ptp_clock_status_default_ds_t default_ds_status;
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;

    ptp_init_clock_ds_t clock_init_bs;
    vtss_appl_ptp_config_port_ds_t port_cfg;
    vtss_appl_ptp_ext_clock_mode_t mode;
    vtss_appl_ptp_config_port_ds_t new_port_cfg;
    vtss_ifindex_t ifindex;
    switch_iter_t sit;
    u32 port_no;
    bool dataSet = false;
    const i8 *var_string;
    size_t len = 0;
    const i8 *str;
    i8 search_str[32];
    i8 str2 [40];
    port_iter_t       pit;

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_USID_CFG);

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_PTP))
        return -1;
#endif

    memset(&clock_init_bs,0,sizeof(clock_init_bs));

    memset(&default_ds, 0, sizeof(default_ds));
    memset(&port_cfg, 0, sizeof(port_cfg));
    if (p->method == CYG_HTTPD_METHOD_POST) {
        /* show the local clock  */
        (void) vtss_appl_ptp_ext_clock_out_get(&mode);
        var_string = cyg_httpd_form_varable_string(p, "one_pps_mode", &len);
        if (len > 0) {
           (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
           /* Change the Device Type of Clock Instance */
           if (ptp_get_one_pps_mode_from_str(str2, &one_pps_mode)) {
               mode.one_pps_mode = (vtss_appl_ptp_ext_clock_1pps_t) one_pps_mode;
           }
        }
        var_string = cyg_httpd_form_varable_string(p, "external_enable", &len);
        if (len > 0) {
           (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
           mode.clock_out_enable = ptp_get_bool_value(str2);
        }
        var_value = 0;
        if (cyg_httpd_form_varable_long_int(p, "clock_freq", &var_value)) {
            mode.freq = var_value;
        }

        var_string = cyg_httpd_form_varable_string(p, "adj_method", &len);
        if (len > 0) {
           (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
           if (ptp_get_adj_method_from_str(str2, &adj_method)) {
               mode.adj_method = adj_method;
           }
        }

        if (MESA_CAP(MESA_CAP_TS_PPS_MODE_OVERRULES_CLK_OUT)) {
            if (mode.one_pps_mode != VTSS_APPL_PTP_ONE_PPS_DISABLE && mode.clock_out_enable) {
                mode.clock_out_enable = false;
            }
        }

        if (MESA_CAP(MESA_CAP_TS_PPS_OUT_OVERRULES_CLK_OUT)) {
            if (mode.one_pps_mode == VTSS_APPL_PTP_ONE_PPS_OUTPUT && mode.clock_out_enable) {
                mode.clock_out_enable = false;
            }
        }

        if (mode.clock_out_enable && !mode.freq) {
            mode.freq = 1;
        }

        if (VTSS_RC_OK != vtss_appl_ptp_ext_clock_out_set(&mode)) {
            T_W("The preferred adjust method is not supported");
        }


        for (inst = 0; inst < PTP_CLOCK_INSTANCES; inst++)
        {
            // Note: Delete/update of clock needs to come before creation of new clocks to avoid that clocks are erroneously modified right after creation
            sprintf(search_str, "delete_%d", inst);
            if (cyg_httpd_form_varable_find(p, search_str)){
                /* Delete the Clock Instance */
                (void) vtss_appl_ptp_clock_config_default_ds_del(inst);

                port_iter_init_local(&pit);
                while (port_iter_getnext(&pit)) {
                    (void) vtss_ifindex_from_port(sit.isid, pit.iport, &ifindex);
                    (void) ptp_ifindex_to_port(ifindex, &port_no);
                    if (vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &port_cfg) == VTSS_RC_OK) {
                        port_cfg.enabled = false;
                        if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &port_cfg) == VTSS_RC_ERROR) {
                            T_D("Clock instance %d : does not exist", inst);
                        }
                    }
                }
            }
        }

        for (inst = 0; inst < PTP_CLOCK_INSTANCES; inst++)
        {
            sprintf(search_str, "clock_inst_new_%d", inst);
            if (cyg_httpd_form_varable_int(p, search_str, &var_int)) {
                new_clock_inst = var_int;
                T_D(" Creating a new clock_instance-[%d]", new_clock_inst);

                vtss_appl_ptp_profile_t profile;
                sprintf(search_str, "ptp_profile_new_%d", inst);
                if (cyg_httpd_form_varable_int(p, search_str, &var_int)) {
                    profile = static_cast<vtss_appl_ptp_profile_t>(var_int);
                }
                else {
                    profile = VTSS_APPL_PTP_PROFILE_NO_PROFILE;
                }
                clock_init_bs.cfg.profile = profile;

                (void) ptp_get_default_clock_default_ds(&default_ds_status, &default_ds_cfg);
                (void) ptp_apply_profile_defaults_to_default_ds(&default_ds_cfg, profile);
                
                clock_init_bs.cfg = default_ds_cfg;

                sprintf(search_str, "devType_new_%d", inst);
                var_string = cyg_httpd_form_varable_string(p, search_str, &len);
                if (len > 0) {
                    (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                    /* Change the Device Type of Clock Instance */
                    if (ptp_get_devtype_from_str(str2, &dev_type)) {
                        T_D("dev_type-[%d]", dev_type);
                        clock_init_bs.cfg.deviceType = dev_type;
                    }
                }

                clock_init_bs.cfg.mep_instance = default_ds_cfg.mep_instance;
                
                // sprintf(search_str, "enforce_profile_new_%d", inst);
                // if (cyg_httpd_form_varable_int(p, search_str, &var_int)) {
                //     clock_init_bs.cfg.enforceProfile = var_int;
                // }
                T_D(" protocol-[%s (%d)]",str2, clock_init_bs.cfg.protocol);

                // Generate clock identity from clock instance number and base MAC address
                (void) conf_mgmt_mac_addr_get(my_sysmac.addr, 0);
                clock_init_bs.clockIdentity[0] = my_sysmac.addr[0];
                clock_init_bs.clockIdentity[1] = my_sysmac.addr[1];
                clock_init_bs.clockIdentity[2] = my_sysmac.addr[2];
                clock_init_bs.clockIdentity[3] = 0xff;
                clock_init_bs.clockIdentity[4] = 0xfe;
                clock_init_bs.clockIdentity[5] = my_sysmac.addr[3];
                clock_init_bs.clockIdentity[6] = my_sysmac.addr[4];
                clock_init_bs.clockIdentity[7] = my_sysmac.addr[5]+new_clock_inst;

                // VID is no longer configured as part of this dialog. Set it to 1 to make sure it gets a valid value.
                clock_init_bs.cfg.configured_vid = 1;
                
                // clock domain
                sprintf(search_str, "clk_dom_new_%d", inst);
                if (cyg_httpd_form_varable_int(p, search_str, &var_int)) {
                    clock_init_bs.cfg.clock_domain = var_int;
                }
                T_D(" clock_domain-[%d]",var_int);

                // The creation will fail if it is a transparent clock and a transparent clock already exists.
                if (ptp_clock_clockidentity_set(new_clock_inst, &clock_init_bs.clockIdentity) != VTSS_RC_OK) {
                    T_D("Cannot set clock identity");
                }
                if (vtss_appl_ptp_clock_config_default_ds_set(new_clock_inst, &clock_init_bs.cfg) != VTSS_RC_OK) {
                //if (ptp_clock_create(&clock_init_bs, new_clock_inst) != VTSS_RC_OK) {
                    T_D(" Cannot Create Clock: Tried to Create more than one transparent clock ");
                }

                /* Apply profile defaults to all ports of this clock (whether enabled or not) */
                port_iter_init_local(&pit);
                while (port_iter_getnext(&pit)) {
                    (void) vtss_ifindex_from_port(sit.isid, pit.iport, &ifindex);
                    (void) ptp_ifindex_to_port(ifindex, &port_no);

                    if (vtss_appl_ptp_config_clocks_port_ds_get(new_clock_inst, ifindex, &port_cfg) == VTSS_RC_OK) {
                        new_port_cfg = port_cfg;
                        ptp_apply_profile_defaults_to_port_ds(&new_port_cfg, profile);
    
                        if (memcmp(&new_port_cfg, &port_cfg, sizeof(vtss_appl_ptp_config_port_ds_t)) != 0) {
                            if (vtss_appl_ptp_config_clocks_port_ds_set(new_clock_inst, ifindex, &new_port_cfg) == VTSS_RC_ERROR) {
                                T_D("Clock instance %d : does not exist", new_clock_inst);
                            }
                        }
                    }
                }
            } 
        }
        redirect(p, "/ptp_config.htm");
    } else {
        if(!cyg_httpd_start_chunked("html"))
            return -1;

        T_D("Inside Else Part");
        /* default clock id */
        memset(str2, 0, sizeof(str2));

        /* Send the Dynamic Parameters */
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "^Disable/Output/Input/OutInput");
        (void)cyg_httpd_write_chunked(p->outbuffer, ct);

        (void)snprintf(p->outbuffer, sizeof(p->outbuffer), "^%s", ptp_adj_method_disp(VTSS_APPL_PTP_PREFERRED_ADJ_LTC));
        if (MESA_CAP(MEBA_CAP_SYNCE_DPLL_MODE_SINGLE)) {
            (void)strncat(p->outbuffer, "/", sizeof(p->outbuffer) - strlen(p->outbuffer) - 1);
            (void)strncat(p->outbuffer, ptp_adj_method_disp(VTSS_APPL_PTP_PREFERRED_ADJ_SINGLE), sizeof(p->outbuffer) - strlen(p->outbuffer) - 1);
        }
        if (MESA_CAP(MEBA_CAP_SYNCE_DPLL_MODE_DUAL)) {
            (void)strncat(p->outbuffer, "/", sizeof(p->outbuffer) - strlen(p->outbuffer) - 1);
            (void)strncat(p->outbuffer, ptp_adj_method_disp(VTSS_APPL_PTP_PREFERRED_ADJ_INDEPENDENT), sizeof(p->outbuffer) - strlen(p->outbuffer) - 1);
        }
        if (MESA_CAP(MEBA_CAP_SYNCE_DPLL_MODE_SINGLE)) {
            (void)strncat(p->outbuffer, "/", sizeof(p->outbuffer) - strlen(p->outbuffer) - 1);
            (void)strncat(p->outbuffer, ptp_adj_method_disp(VTSS_APPL_PTP_PREFERRED_ADJ_COMMON), sizeof(p->outbuffer) - strlen(p->outbuffer) - 1);
        }
        (void)strncat(p->outbuffer, "/", sizeof(p->outbuffer) - strlen(p->outbuffer) - 1);
        (void)strncat(p->outbuffer, ptp_adj_method_disp(VTSS_APPL_PTP_PREFERRED_ADJ_AUTO), sizeof(p->outbuffer) - strlen(p->outbuffer) - 1);
        (void)cyg_httpd_write_chunked(p->outbuffer, strlen(p->outbuffer));

        /* show the local clock  */
        (void) vtss_appl_ptp_ext_clock_out_get(&mode);

        (void)cyg_httpd_write_chunked("^", 1);
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s|%s|%d|%s",
                                      ptp_one_pps_mode_disp(mode.one_pps_mode),
                                      ptp_bool_disp(mode.clock_out_enable),
                                      mode.freq,
                                      ptp_adj_method_disp(mode.adj_method));
        (void)cyg_httpd_write_chunked(p->outbuffer, ct);

        (void)cyg_httpd_write_chunked("^", 1);

        dev_type = VTSS_APPL_PTP_DEVICE_NONE;
        str = DeviceTypeToString(dev_type);
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s", str);
        (void)cyg_httpd_write_chunked(p->outbuffer, ct);

        while (1) {
            dev_type = (vtss_appl_ptp_device_type_t)(dev_type + 1);
            str = DeviceTypeToString(dev_type);
            if (strcmp(str,"?") != 0 ) {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%s", str);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            } else {
                break;
            }
        }

        (void)cyg_httpd_write_chunked("#", 1);
        for (inst = 0; inst < PTP_CLOCK_INSTANCES; inst++)
        {
            T_D("Inst - %d",inst);
            if (vtss_appl_ptp_clock_config_default_ds_get(inst, &default_ds_cfg) == VTSS_RC_OK)
            {
                dataSet = true;
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%u", inst);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "/%u", default_ds_cfg.clock_domain);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "/%s", DeviceTypeToString(default_ds_cfg.deviceType));
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "/%u", default_ds_cfg.profile);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
//                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "/%u", default_ds_cfg.enforceProfile);
//                (void)cyg_httpd_write_chunked(p->outbuffer, ct);

//                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "/%s", ClockIdentityToString(default_ds_status.clockIdentity, str1));
//                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            }
        }
        if (!dataSet)
            (void)cyg_httpd_write_chunked("",1);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "^%s/%s/%s%s%s%s%s",
                ptp_profiles[0],
                ptp_profiles[1],
                ptp_profiles[2],
#if !defined(VTSS_SW_OPTION_ZLS30386)
                "/", ptp_profiles[3],
#else
                "", "",
#endif



                "", ""

                );
        (void)cyg_httpd_write_chunked(p->outbuffer, ct);

        cyg_httpd_end_chunked();
    }
    return -1; // Do not further search the file system.
}

#define HAS_SERVO_380(expr)     { \
                                    if (servo_type == VTSS_ZARLINK_SERVO_ZLS30380) { \
                                        ct += snprintf(p->outbuffer + ct, sizeof(p->outbuffer) - ct, "%s", expr); \
                                    } \
                                }
#define HAS_SERVO_380_384(expr) { \
                                    if (servo_type == VTSS_ZARLINK_SERVO_ZLS30380 || servo_type == VTSS_ZARLINK_SERVO_ZLS30384) { \
                                        ct += snprintf(p->outbuffer + ct, sizeof(p->outbuffer) - ct, "%s", expr); \
                                    } \
                                }
#define HAS_ANY_SERVO(expr)     { \
                                    if (servo_type == VTSS_ZARLINK_SERVO_ZLS30380 || servo_type == VTSS_ZARLINK_SERVO_ZLS30384 || servo_type == VTSS_ZARLINK_SERVO_ZLS30387) { \
                                        ct += snprintf(p->outbuffer + ct, sizeof(p->outbuffer) - ct, "%s", expr); \
                                    } \
                                }

static i32 handler_clock_config_ptp(CYG_HTTPD_STATE* p)
{
    i32    ct;
    char   str [14];
    size_t len;
    mesa_timestamp_t t;
    uint inst = 0;
    int    var_value = 0;
    int    var_int;    
    mesa_ipv4_t ip_address;
    vtss_appl_ptp_clock_status_default_ds_t default_ds_status;
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    vtss_appl_ptp_clock_current_ds_t clock_current_bs;
    vtss_appl_ptp_clock_parent_ds_t clock_parent_bs;
    vtss_appl_ptp_clock_timeproperties_ds_t clock_time_properties_bs;
    vtss_appl_ptp_clock_config_default_ds_t new_default_ds_cfg;
    vtss_appl_ptp_config_port_ds_t new_port_cfg;
    vtss_appl_ptp_virtual_port_config_t virtual_port_cfg;
#ifdef  PTP_OFFSET_FILTER_CUSTOM
    ptp_clock_servo_con_ds_t servo;
#else
    vtss_appl_ptp_clock_servo_config_t default_servo;
#endif /* PTP_OFFSET_FILTER_CUSTOM */
    vtss_appl_ptp_clock_filter_config_t filter_params;
    vtss_appl_ptp_unicast_slave_config_t uni_slave_cfg;
    vtss_appl_ptp_unicast_slave_table_t uni_slave_status;
    bool dataSet = false;
    bool sync_2_sys_clock = false;
    const i8 *var_string;
    i8 search_str[32];    
    i8 str1 [40];
    i8 str2 [40];
    i8 str3 [60]; /* Change this */
    u32 hw_time;
    u32 ix;
    vtss_appl_ptp_config_port_ds_t port_cfg;
    vtss_ifindex_t ifindex;
    uint port_no;
    port_iter_t pit;
    int first_port;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_PTP))
        return -1;
#endif
    if (cyg_httpd_form_varable_int(p, "clock_inst", &var_value)) {
        inst = var_value;
    }

    // bool="true" || "false"
    if ((var_string = cyg_httpd_form_varable_string(p, "bool", &len)) != NULL && len >= 4) {
      if (strncasecmp(var_string, "true", 4) == 0) {
        sync_2_sys_clock = true;
      } else if (strncasecmp(var_string, "false", 5) == 0) {
        sync_2_sys_clock = false;
      }
    }

    if (sync_2_sys_clock == true) {
        t.sec_msb = 0;
        t.seconds = time(NULL);
        t.nanoseconds = 0;
        ptp_local_clock_time_set(&t, ptp_instance_2_timing_domain(inst));
        T_D("True is set,clock_instance-[%d] ",inst);
    }

    if (p->method == CYG_HTTPD_METHOD_POST) {
        T_D("Inside CYG_HTTPD_METHOD_POST post_data-[%s]",p->post_data);
        if ((vtss_appl_ptp_clock_config_default_ds_get(inst, &default_ds_cfg) == VTSS_RC_OK) &&
            (vtss_appl_ptp_clock_status_default_ds_get(inst, &default_ds_status) == VTSS_RC_OK))
        {
            new_default_ds_cfg = default_ds_cfg;

            // sprintf(search_str, "ptp_profile_%d", inst);
            // if (cyg_httpd_form_varable_int(p, search_str, &var_int))
            //     new_default_ds_cfg.profile = var_int;
    
            // sprintf(search_str, "enforce_profile_%d", inst);
            // if (cyg_httpd_form_varable_int(p, search_str, &var_int))
            //     new_default_ds_cfg.enforceProfile = var_int;

            port_iter_init_local(&pit);
            while (port_iter_getnext(&pit)) {
                (void) vtss_ifindex_from_port(0, pit.iport, &ifindex);
                (void) ptp_ifindex_to_port(ifindex, &port_no);

                if (vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &port_cfg) == VTSS_RC_OK) {
                    sprintf(search_str, "mask_%d", (uint) iport2uport(port_no));
                    if (cyg_httpd_form_varable_find(p, search_str)){
                        port_cfg.enabled = true;
                    } else {
                        port_cfg.enabled = false;
                    }
                    if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &port_cfg) == VTSS_RC_ERROR) {
                        T_D("Clock instance %d : does not exist", inst);
                    }
                }
            }

            sprintf(str1,"2_step_flag_%d",inst);
            var_string = cyg_httpd_form_varable_string(p, str1, &len);
            if (len > 0)
                (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
            new_default_ds_cfg.twoStepFlag = ptp_get_bool_value(str2);

            sprintf(str1,"domain_%d",inst);
            if (cyg_httpd_form_varable_int(p, str1, &var_value) )
                new_default_ds_cfg.domainNumber = var_value;

            sprintf(str1,"prio_1_%d",inst);
            if (cyg_httpd_form_varable_int(p, str1, &var_value))
                new_default_ds_cfg.priority1 = var_value;

            sprintf(str1,"prio_2_%d",inst);
            if (cyg_httpd_form_varable_int(p, str1, &var_value))
                new_default_ds_cfg.priority2 = var_value;

            sprintf(str1,"local_prio_%d",inst);
            if (cyg_httpd_form_varable_int(p, str1, &var_value))
                new_default_ds_cfg.localPriority = var_value;

            sprintf(str1,"protocol_method_%d",inst);
            var_string = cyg_httpd_form_varable_string(p, str1, &len);
            if (len > 0)
                (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
            new_default_ds_cfg.protocol = ptp_get_protocol(str2);

            sprintf(str1,"one_way_%d",inst);
            var_string = cyg_httpd_form_varable_string(p, str1, &len);
            if (len > 0)
                (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
            new_default_ds_cfg.oneWay =  ptp_get_bool_value(str2);

            sprintf(search_str, "vid_%d", inst);
            if (cyg_httpd_form_varable_int(p, search_str, &var_int)) {
                new_default_ds_cfg.configured_vid = var_int;
            }

            sprintf(search_str, "pcp_%d", inst);
            if (cyg_httpd_form_varable_int(p, search_str, &var_int)) {
                new_default_ds_cfg.configured_pcp = var_int;
            }

            sprintf(search_str, "dscp_%d", inst);
            if (cyg_httpd_form_varable_int(p, search_str, &var_int)) {
                new_default_ds_cfg.dscp = var_int;
            }

            sprintf(str1, "filter_type_%d", inst);
            var_string = cyg_httpd_form_varable_string(p, str1, &len);
            if (len > 0)
                (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
            new_default_ds_cfg.filter_type = ptp_get_filter_type(str2);

            if (vtss_appl_ptp_clock_config_default_ds_set(inst, &new_default_ds_cfg) != VTSS_RC_OK) {
                T_D("Clock instance %d : does not exist", inst);
            }

            if (ptp_clock_config_virtual_port_config_get(inst, &virtual_port_cfg) == VTSS_RC_OK) {

                sprintf(str1, "virtual_port_enable_%d", inst);
                if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                    if (len > 0)
                        (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                    virtual_port_cfg.enable = ptp_get_bool_value(str2);
                }

                sprintf(str1, "virtual_port_io_pin_%d", inst);
                if (cyg_httpd_form_varable_int(p, str1, &var_value))
                    virtual_port_cfg.io_pin = var_value;

                sprintf(str1, "virtual_port_class_%d", inst);
                if (cyg_httpd_form_varable_int(p, str1, &var_value))
                    virtual_port_cfg.clockQuality.clockClass = var_value;

                sprintf(str1, "virtual_port_accuracy_%d", inst);
                if (cyg_httpd_form_varable_int(p, str1, &var_value))
                    virtual_port_cfg.clockQuality.clockAccuracy = var_value;

                sprintf(str1, "virtual_port_variance_%d", inst);
                if (cyg_httpd_form_varable_int(p, str1, &var_value))
                    virtual_port_cfg.clockQuality.offsetScaledLogVariance = var_value;

                sprintf(str1, "virtual_port_prio1_%d", inst);
                if (cyg_httpd_form_varable_int(p, str1, &var_value))
                    virtual_port_cfg.priority1 = var_value;

                sprintf(str1, "virtual_port_prio2_%d", inst);
                if (cyg_httpd_form_varable_int(p, str1, &var_value))
                    virtual_port_cfg.priority2 = var_value;

                sprintf(str1, "virtual_port_local_prio_%d", inst);
                if (cyg_httpd_form_varable_int(p, str1, &var_value))
                    virtual_port_cfg.localPriority = var_value;

                if (vtss_appl_ptp_clock_config_virtual_port_config_set(inst, &virtual_port_cfg) == VTSS_RC_OK) {
                    T_D("Could not update virtual port data set of clock instance %d", inst);
                }
            }

            if (vtss_appl_ptp_clock_config_timeproperties_ds_get(inst, &clock_time_properties_bs) == VTSS_RC_OK) {

                sprintf(str1,"uct_offset_%d",inst);
                if (cyg_httpd_form_varable_int(p, str1, &var_value))
                    clock_time_properties_bs.currentUtcOffset = var_value;


                sprintf(str1,"valid_%d",inst);
                if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                    if (len > 0)
                        (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                    clock_time_properties_bs.currentUtcOffsetValid = ptp_get_bool_value(str2);
                }
                sprintf(str1,"leap59_%d",inst);
                if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                    if (len > 0)
                        (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                    clock_time_properties_bs.leap59 = ptp_get_bool_value(str2);
                }

                sprintf(str1,"leap61_%d",inst);
                if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                    if (len > 0)
                        (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                    clock_time_properties_bs.leap61 = ptp_get_bool_value(str2);
                }

                sprintf(str1,"time_trac_%d",inst);
                if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                    if (len > 0)
                        (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                    clock_time_properties_bs.timeTraceable = ptp_get_bool_value(str2);
                }

                sprintf(str1,"freq_trac_%d",inst);
                if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                    if (len > 0)
                        (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                    clock_time_properties_bs.frequencyTraceable = ptp_get_bool_value(str2);
                }

                sprintf(str1,"ptp_time_scale_%d",inst);
                if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                    if (len > 0)
                        (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                    clock_time_properties_bs.ptpTimescale = ptp_get_bool_value(str2);
                }

                sprintf(str1,"time_source_%d",inst);
                if (cyg_httpd_form_varable_int(p, str1, &var_value))
                    clock_time_properties_bs.timeSource = var_value;

                sprintf(str1,"leap_pending_%d",inst);
                if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                    if (len > 0)
                        (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                    clock_time_properties_bs.pendingLeap = ptp_get_bool_value(str2);
                }

                sprintf(str1,"leap_date_%d",inst);
                var_string = cyg_httpd_form_varable_string(p, str1, &len);
                if (len > 0) {
                    (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                    (void)extract_date(str2, &clock_time_properties_bs.leapDate);
                }

                sprintf(str1,"leap_type_%d",inst);
                if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                    if (len > 0)
                        (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                    clock_time_properties_bs.leapType = ptp_get_leap_type_value(str2);
                }

                if (vtss_appl_ptp_clock_config_timeproperties_ds_set(inst, &clock_time_properties_bs) != VTSS_RC_OK) {
                    T_D("Clock instance %d : does not exist", inst);
                }
            }

            if (vtss_appl_ptp_clock_servo_parameters_get(inst, &default_servo) == VTSS_RC_OK) {
                sprintf(str1,"display_%d",inst);
                if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                    if (len > 0)
                        (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                    default_servo.display_stats =  ptp_get_bool_value(str2);
                }

                sprintf(str1,"p_enable_%d",inst);
                if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                    if (len > 0)
                        (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                    default_servo.p_reg =  ptp_get_bool_value(str2);
                }
                sprintf(str1,"i_enable_%d",inst);
                if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                    if (len > 0)
                        (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                    default_servo.i_reg =  ptp_get_bool_value(str2);
                }
                sprintf(str1,"d_enable_%d",inst);
                if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                    if (len > 0)
                        (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                    default_servo.d_reg =  ptp_get_bool_value(str2);
                }
                sprintf(str1,"p_const_%d",inst);
                if (cyg_httpd_form_varable_int(p, str1, &var_value))
                    default_servo.ap = var_value;

                sprintf(str1,"i_const_%d",inst);
                if (cyg_httpd_form_varable_int(p, str1, &var_value))
                    default_servo.ai = var_value;

                sprintf(str1,"d_const_%d",inst);
                if (cyg_httpd_form_varable_int(p, str1, &var_value))
                    default_servo.ad = var_value;

                sprintf(str1,"gain_%d",inst);
                if (cyg_httpd_form_varable_int(p, str1, &var_value))
                    default_servo.gain = var_value;

                if (vtss_appl_ptp_clock_servo_parameters_set(inst, &default_servo) == VTSS_RC_ERROR) {
                    T_D("Clock instance %d : does not exist", inst);
                }
            }
            if (vtss_appl_ptp_clock_filter_parameters_get(inst, &filter_params) == VTSS_RC_OK) {
                sprintf(str1,"delay_filter_%d",inst);
                if (cyg_httpd_form_varable_int(p, str1, &var_value)) {
                    filter_params.delay_filter = var_value;
                }

                sprintf(str1,"period_%d",inst);
                if (cyg_httpd_form_varable_int(p, str1, &var_value)) {
                    filter_params.period = var_value;
                }

                sprintf(str1,"dist_%d",inst);
                if (cyg_httpd_form_varable_int(p, str1, &var_value)) {
                    filter_params.dist = var_value;
                }

                if (vtss_appl_ptp_clock_filter_parameters_set(inst, &filter_params) == VTSS_RC_ERROR) {
                    T_D("Clock instance %d : does not exist", inst);
                }
            }
            ix = 0;
            while (vtss_appl_ptp_clock_config_unicast_slave_config_get(inst, ix++, &uni_slave_cfg) == VTSS_RC_OK) {
                sprintf(str1,"uc_dura_%d_%d",inst,(int)(ix-1));
                if (cyg_httpd_form_varable_int(p, str1, &var_value)) {
                    uni_slave_cfg.duration = var_value;
                }

                sprintf(str1,"uc_ip_%d_%d",inst,(int)(ix-1));
                if (cyg_httpd_form_varable_ipv4(p, str1, &ip_address)) {
                    uni_slave_cfg.ip_addr = ip_address;
                }

                if (vtss_appl_ptp_clock_config_unicast_slave_config_set(inst, (ix-1), &uni_slave_cfg) == VTSS_RC_ERROR) {
                    T_D("Clock instance %d : does not exist",inst);
                }
            }
        }
        sprintf(str1, "/ptp_clock_config.htm?clock_inst=%d", inst);
        redirect(p, str1);
    } else {                    /* CYG_HTTPD_METHOD_GET (+HEAD) */
        T_D("Get Method");
        if(!cyg_httpd_start_chunked("html"))
            return -1;

        if ((vtss_appl_ptp_clock_config_default_ds_get(inst, &default_ds_cfg) == VTSS_RC_OK) &&
            (vtss_appl_ptp_clock_status_default_ds_get(inst, &default_ds_status) == VTSS_RC_OK))
        {
            if ((var_string = cyg_httpd_form_varable_string(p, "apply_profile_defaults", &len)) != NULL && len >= 4) {
                if (strncasecmp(var_string, "true", 4) == 0) {
                    ptp_apply_profile_defaults_to_default_ds(&default_ds_cfg, default_ds_cfg.profile);
                    if (vtss_appl_ptp_clock_config_default_ds_set(inst, &default_ds_cfg) != VTSS_RC_OK) {
                        T_D("Clock instance %d : does not exist", inst);
                    }

                    /* Look for all the ports that have been configured
                       for this new Clock */
                    port_iter_init_local(&pit);
                    while (port_iter_getnext(&pit)) {
                        (void) vtss_ifindex_from_port(0, pit.iport, &ifindex);
                        (void) ptp_ifindex_to_port(ifindex, &port_no);  
                        if (vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &port_cfg) == VTSS_RC_OK) {
                            new_port_cfg = port_cfg;
                            ptp_apply_profile_defaults_to_port_ds(&new_port_cfg, default_ds_cfg.profile);
        
                            if (memcmp(&new_port_cfg, &port_cfg, sizeof(vtss_appl_ptp_config_port_ds_t)) != 0) {
                                if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &new_port_cfg) == VTSS_RC_ERROR) {
                                    T_D("Clock instance %d : does not exist", inst);
                                }
                            }
                        }
                    }

                    if (vtss_appl_ptp_clock_config_default_ds_get(inst, &default_ds_cfg) != VTSS_RC_OK) {
                        T_D("Clock instance %d : does not exist", inst);
                    }
                }
            }
            dataSet = true;

            /* Send the Dynamic Parameters */
#if defined(VTSS_SW_OPTION_MEP)
            if (MESA_CAP(MESA_CAP_TS_MISSING_PTP_ON_INTERNAL_PORTS)) {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "^Ethernet/EthernetMixed/IPv4Multi/IPv4Mixed/IPv4Uni/Oam/OnePPS");
            } else {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "^Ethernet/EthernetMixed/IPv4Multi/IPv4Mixed/IPv4Uni/OnePPS");
            }
#else
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "^Ethernet/EthernetMixed/IPv4Multi/IPv4Mixed/IPv4Uni/OnePPS");
#endif

            (void)cyg_httpd_write_chunked(p->outbuffer, ct);

            vtss_zarlink_servo_t servo_type = (vtss_zarlink_servo_t)MESA_CAP(VTSS_APPL_CAP_ZARLINK_SERVO_TYPE);
            /* Send the Dynamic Parameters relating to servo/filter types */
            if (servo_type == VTSS_ZARLINK_SERVO_NONE) {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "^");
            } else {
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "^%s",
                                                                  "ACI_BASIC_PHASE"
                                                                  "/ACI_BASIC_PHASE_SYNCE"
                                                                  "/ACI_BASIC_PHASE_LOW"
                                                                  "/ACI_BASIC_PHASE_LOW_SYNCE");
                HAS_SERVO_380(
                                                  "/ACI_DEFAULT"
                                                  "/ACI_FREQ_XO"
                                                  "/ACI_PHASE_XO"
                                                  "/ACI_FREQ_TCXO"
                                                  "/ACI_PHASE_TCXO"
                                                  "/ACI_FREQ_OCXO_S3E"
                                                  "/ACI_PHASE_OCXO_S3E"
                )
                HAS_SERVO_380_384(
                                                  "/ACI_BC_PARTIAL_ON_PATH_FREQ"
                                                  "/ACI_BC_PARTIAL_ON_PATH_PHASE"
                                                  "/ACI_BC_PARTIAL_ON_PATH_PHASE_SYNCE"
                )
                HAS_ANY_SERVO(
                                                  "/ACI_BC_FULL_ON_PATH_FREQ"
                )
                HAS_SERVO_380_384(
                                                  "/ACI_BC_FULL_ON_PATH_PHASE"
                                                  "/ACI_BC_FULL_ON_PATH_PHASE_SYNCE"
                )
                HAS_SERVO_380(
                                                  "/ACI_FREQ_ACCURACY_FDD"
                                                  "/ACI_FREQ_ACCURACY_XDSL"
                )
                HAS_SERVO_380_384(
                                                  "/ACI_ELEC_FREQ"
                                                  "/ACI_ELEC_PHASE"
                )
                HAS_SERVO_380(
                                                  "/ACI_PHASE_TCXO_Q"
                                                  "/ACI_BC_FULL_ON_PATH_PHASE_Q"
                )
                HAS_SERVO_380_384(
                                                  "/ACI_PHASE_RELAXED_C60W"
                                                  "/ACI_PHASE_RELAXED_C150"
                                                  "/ACI_PHASE_RELAXED_C180"
                                                  "/ACI_PHASE_RELAXED_C240"
                )
                HAS_SERVO_380(
                                                  "/ACI_BC_FULL_ON_PATH_PHASE_BETA"
                                                  "/ACI_PHASE_OCXO_S3E_R4_6_1"
                )
            }
            if (vtss_appl_ptp_clock_filter_parameters_get(inst, &filter_params) != VTSS_RC_OK) {
                T_W("Could not get filter parameters for PTP clock instance.");
            } else {
                for (int i = 0; i < PTP_CLOCK_INSTANCES; i++) {
                    vtss_appl_ptp_clock_config_default_ds_t tmp_default_ds_cfg;
    
                    if ((i != inst) && (vtss_appl_ptp_clock_config_default_ds_get(i, &tmp_default_ds_cfg) == VTSS_RC_OK)) {
                        if ((tmp_default_ds_cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) && (tmp_default_ds_cfg.clock_domain == default_ds_cfg.clock_domain)) {
                            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "^%s", ptp_filter_type_disp(default_ds_cfg.filter_type));
                            break;
                        }
                    }
                }
            }
            (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            (void)cyg_httpd_write_chunked("^", 1);

            /* send the local clock  */
            vtss_local_clock_time_get(&t, inst, &hw_time);
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "#%s %s", misc_time2str(t.seconds), vtss_tod_ns2str(t.nanoseconds, str,','));
            (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "/%s", clock_adjustment_method_txt(vtss_ptp_adjustment_method(inst)));
            (void)cyg_httpd_write_chunked(p->outbuffer, ct);

            /* Send the port configuration */
            first_port = 1;
            port_iter_init_local(&pit);
            while (port_iter_getnext(&pit)) {
                (void) vtss_ifindex_from_port(0, pit.iport, &ifindex);

                if (vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &port_cfg) == VTSS_RC_OK) {
                    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), first_port ? "#%u" : "/%u", port_cfg.enabled);
                } else {
                    ct = snprintf(p->outbuffer, sizeof(p->outbuffer), first_port ? "#%u" : "/%u", 0);
                }
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                first_port = 0;
            }
            
            /* Send the virtual port configuration */
            (void)ptp_clock_config_virtual_port_config_get(inst, &virtual_port_cfg);
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "#%s/%d/%d/%d/%d/%d/%d/%d",
                          (virtual_port_cfg.enable ? "True" : "False"),
                          virtual_port_cfg.io_pin,
                          virtual_port_cfg.clockQuality.clockClass,
                          virtual_port_cfg.clockQuality.clockAccuracy,
                          virtual_port_cfg.clockQuality.offsetScaledLogVariance,
                          virtual_port_cfg.priority1,
                          virtual_port_cfg.priority2,
                          virtual_port_cfg.localPriority);
            (void)cyg_httpd_write_chunked(p->outbuffer, ct);

            /* Send the Default clock Data Set */
            if (default_ds_cfg.deviceType == VTSS_APPL_PTP_DEVICE_NONE) {
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        " #%d /%s", inst,
                        DeviceTypeToString(default_ds_cfg.deviceType));
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            } else {
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "#%d/%d/%s/%s/%d/%s/%d",
                        inst, default_ds_cfg.clock_domain, DeviceTypeToString(default_ds_cfg.deviceType),
                        (default_ds_cfg.twoStepFlag) ? "True" : "False",
                         default_ds_status.numberPorts,
                         ClockIdentityToString(default_ds_status.clockIdentity, str1),
                         default_ds_cfg.domainNumber);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);

                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "/%s/%d/%d",
                        ClockQualityToString(&default_ds_status.clockQuality, str2),
                        default_ds_cfg.priority1,
                        default_ds_cfg.priority2);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "/%s/%s", ptp_protocol_disp(default_ds_cfg.protocol),
                        (default_ds_cfg.oneWay) ? "True" : "False");
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer), "/%s/%d/%d",
                         "False",   // was the tagging enable parameter, that is not used any more (the value is kept in the list, otherwise all the indexed in the command string must be changed in the HTM files
                         (default_ds_cfg.configured_vid),
                         (default_ds_cfg.configured_pcp));
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer), "/%d",
                              (default_ds_cfg.dscp));
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer), "/%d", default_ds_cfg.profile);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer), "/%d",
                         default_ds_cfg.localPriority);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);





            }

            /* Send the current Clock Data Set */
            if (vtss_appl_ptp_clock_status_current_ds_get(inst, &clock_current_bs) == VTSS_RC_OK) {
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),



                        "#%d/%s/%s",

                        clock_current_bs.stepsRemoved, vtss_tod_TimeInterval_To_String(&clock_current_bs.offsetFromMaster, str1,','),
                        vtss_tod_TimeInterval_To_String(&clock_current_bs.meanPathDelay, str2,',')









                        );
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            }
            if (vtss_appl_ptp_clock_status_parent_ds_get(inst, &clock_parent_bs) == VTSS_RC_OK) {
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "#%s/%d/%s/%d/%d",
                        ClockIdentityToString(clock_parent_bs.parentPortIdentity.clockIdentity, str1), clock_parent_bs.parentPortIdentity.portNumber,
                        ptp_bool_disp(clock_parent_bs.parentStats),
                        clock_parent_bs.observedParentOffsetScaledLogVariance,
                        clock_parent_bs.observedParentClockPhaseChangeRate);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);

                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),



                        "/%s/%s/%d/%d",


                        ClockIdentityToString(clock_parent_bs.grandmasterIdentity, str2),
                        ClockQualityToString(&clock_parent_bs.grandmasterClockQuality, str3),
                        clock_parent_bs.grandmasterPriority1,
                        clock_parent_bs.grandmasterPriority2



                        );
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            }
            if (vtss_appl_ptp_clock_config_timeproperties_ds_get(inst, &clock_time_properties_bs) == VTSS_RC_OK) {
                struct tm* ptm;
                time_t rawtime = (time_t) clock_time_properties_bs.leapDate * 86400;
                ptm = gmtime(&rawtime);
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "#%d/%s/%s/%s/%s/%s/%s/%d/%s/%04d-%02d-%02d/%s",
                        clock_time_properties_bs.currentUtcOffset,
                        ptp_bool_disp(clock_time_properties_bs.currentUtcOffsetValid),
                        ptp_bool_disp(clock_time_properties_bs.leap59),
                        ptp_bool_disp(clock_time_properties_bs.leap61),
                        ptp_bool_disp(clock_time_properties_bs.timeTraceable),
                        ptp_bool_disp(clock_time_properties_bs.frequencyTraceable),
                        ptp_bool_disp(clock_time_properties_bs.ptpTimescale),
                        clock_time_properties_bs.timeSource,
                        clock_time_properties_bs.pendingLeap ? "True" : "False",
                        ptm->tm_year + 1900,
                        ptm->tm_mon + 1,
                        ptm->tm_mday,
                        (clock_time_properties_bs.leapType == VTSS_APPL_PTP_LEAP_SECOND_59) ? "leap59" : "leap61");
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            }
            /* send the servo parameters  */
            if (vtss_appl_ptp_clock_servo_parameters_get(inst, &default_servo) == VTSS_RC_OK) {
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "#%s/%s/%s/%s/%d/%d/%d/%d",
                        ptp_bool_disp(default_servo.display_stats),
                        ptp_bool_disp(default_servo.p_reg),
                        ptp_bool_disp(default_servo.i_reg),
                        ptp_bool_disp(default_servo.d_reg),
                        default_servo.ap,
                        default_servo.ai,
                        default_servo.ad,
                        default_servo.gain);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            }
            /* send the filter parameters  */
            if (vtss_appl_ptp_clock_filter_parameters_get(inst, &filter_params) == VTSS_RC_OK) {
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "#%d/%s/%d/%d",
                        filter_params.delay_filter,
                        ptp_filter_type_disp(default_ds_cfg.filter_type),
                        filter_params.period,
                        filter_params.dist);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            }

            /* send the unicast slave parameters */
            (void)cyg_httpd_write_chunked("#", 1);
            ix = 0;
            while ((vtss_appl_ptp_clock_config_unicast_slave_config_get(inst, ix, &uni_slave_cfg) == VTSS_RC_OK) &&
                   (vtss_appl_ptp_clock_status_unicast_slave_table_get(inst, ix++, &uni_slave_status) == VTSS_RC_OK))
            {
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "%u/%d/%s/%d/%s|",
                        (ix-1),
                        uni_slave_cfg.duration,
                        misc_ipv4_txt(uni_slave_cfg.ip_addr, str1),
                        uni_slave_status.log_msg_period,
                        ptp_state_disp(uni_slave_status.comm_state));
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            }
        }
        if (!dataSet)
            (void)cyg_httpd_write_chunked("",1);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "^%s/%s/%s%s%s%s%s",
                ptp_profiles[0],
                ptp_profiles[1],
                ptp_profiles[2],
#if !defined(VTSS_SW_OPTION_ZLS30386)
                "/", ptp_profiles[3],
#else
                "", "",
#endif



                "", ""

                );
        (void)cyg_httpd_write_chunked(p->outbuffer, ct);

        cyg_httpd_end_chunked();

    }
    return -1; // Do not further search the file system.
}

static i32 handler_clock_ports_config_ptp(CYG_HTTPD_STATE* p)
{
    i32    ct;
    size_t len;
    u8 mech_type;
    uint inst = 0;
    int    var_value;
    mesa_timeinterval_t latency;
    vtss_appl_ptp_clock_status_default_ds_t default_ds_status;
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    vtss_appl_ptp_config_port_ds_t port_cfg;
    vtss_appl_ptp_config_port_ds_t new_port_cfg;
    vtss_appl_ptp_status_port_ds_t port_status;
    vtss_ifindex_t ifindex;
    switch_iter_t sit;
    u32 port_no;
    bool dataSet = false;
    const i8 *var_string;
    i8 str1 [50];
    i8 pmpd [50];



    i8 str2 [40];
    port_iter_t       pit;
    mesa_rc port_rc;
    static mesa_rc error_no = VTSS_RC_OK;
    static vtss_uport_no_t error_port = 0;
    i8 err_str [60];

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_USID_CFG);

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_PTP))
        return -1;
#endif

    if (cyg_httpd_form_varable_int(p, "clock_inst", &var_value)) {
        inst = var_value;
    }
    if (p->method == CYG_HTTPD_METHOD_POST) {
        T_D("Inside CYG_HTTPD_METHOD_POST post_data-[%s]",p->post_data);
        if ((vtss_appl_ptp_clock_config_default_ds_get(inst, &default_ds_cfg) == VTSS_RC_OK) &&
            (vtss_appl_ptp_clock_status_default_ds_get(inst, &default_ds_status) == VTSS_RC_OK))
        {

            error_no = VTSS_RC_OK;
            error_port = 0;
            port_iter_init_local(&pit);
            while (port_iter_getnext(&pit)) {
                (void) vtss_ifindex_from_port(sit.isid, pit.iport, &ifindex);
                (void) ptp_ifindex_to_port(ifindex, &port_no);
                if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &port_cfg) == VTSS_RC_OK) &&
                    (port_cfg.enabled == 1))
                {
                    new_port_cfg = port_cfg;

                    sprintf(str1, "anv_%d_%d", inst, (uint) iport2uport(port_no));
                    if (cyg_httpd_form_varable_int(p, str1, &var_value)) {
                        new_port_cfg.logAnnounceInterval = var_value;
                    }

                    sprintf(str1, "ato_%d_%d", inst, (uint) iport2uport(port_no));
                    if (cyg_httpd_form_varable_int(p, str1, &var_value)) {
                        new_port_cfg.announceReceiptTimeout = var_value;
                    }

                    sprintf(str1, "syv_%d_%d", inst, (uint) iport2uport(port_no));
                    if (cyg_httpd_form_varable_int(p, str1, &var_value)) {
                        new_port_cfg.logSyncInterval = var_value;
                    }

                    sprintf(str1, "dlm_%d_%d", inst, (uint) iport2uport(port_no));
                    if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                        if (len > 0)
                            (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                        if (ptp_get_delaymechanism_type(str2, &mech_type)) {
                            new_port_cfg.delayMechanism = mech_type;
                        }
                    }

                    sprintf(str1, "mpr_%d_%d", inst, (uint)iport2uport(port_no));
                    if (cyg_httpd_form_varable_int(p, str1, &var_value)) {
                        new_port_cfg.logMinPdelayReqInterval = var_value;
                    }
                    sprintf(str1,"delay_assymetry_%d_%d", inst, (uint) iport2uport(port_no));
                    var_value = 0;
                    if (cyg_httpd_form_varable_int(p, str1, &var_value)) {
                        latency = 0;
                        latency = (mesa_timeinterval_t) var_value << 16;
                        new_port_cfg.delayAsymmetry = latency;
                    }
                    sprintf(str1,"ingress_latency_%d_%d", inst, (uint) iport2uport(port_no));
                    var_value = 0;
                    if (cyg_httpd_form_varable_int(p, str1, &var_value) ) {
                        latency = 0;
                        latency = (mesa_timeinterval_t) var_value << 16;
                        new_port_cfg.ingressLatency = latency;
                    }
                    sprintf(str1,"egress_latency_%d_%d",inst,(uint)iport2uport(port_no));
                    var_value = 0;
                    if (cyg_httpd_form_varable_int(p, str1, &var_value)) {
                        latency = 0;
                        latency = (mesa_timeinterval_t) var_value << 16;
                        new_port_cfg.egressLatency = latency;
                    }
                    sprintf(str1, "mcast_addr_%d_%d", inst, (uint) iport2uport(port_no));
                    if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                        if (len > 0) {
                            (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                            if (!strcmp(str2, "Link-local")) {
                                new_port_cfg.dest_adr_type = VTSS_APPL_PTP_PROTOCOL_SELECT_LINK_LOCAL;
                            } else {
                                new_port_cfg.dest_adr_type = VTSS_APPL_PTP_PROTOCOL_SELECT_DEFAULT;
                            }
                        }
                    }
                    sprintf(str1, "not_slave_%d_%d", inst, (uint) iport2uport(port_no));
                    if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                        if (len > 0) {
                            (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                            if (!strcmp(str2, "True")) {
                                new_port_cfg.notSlave = true;
                            } else {
                                new_port_cfg.notSlave = false;
                            }
                        }
                    }
                    sprintf(str1, "local_prio_%d_%d", inst, (uint) iport2uport(port_no));
                    if (cyg_httpd_form_varable_int(p, str1, &var_value))
                        new_port_cfg.localPriority = var_value;
                    sprintf(str1, "2_step_flag_%d_%d", inst, (uint) iport2uport(port_no));
                    if ((var_string = cyg_httpd_form_varable_string(p, str1, &len))) {
                        if (len > 0) {
                            new_port_cfg.twoStepOverride = VTSS_APPL_PTP_TWO_STEP_OVERRIDE_NONE;
                            (void)cgi_unescape(var_string, &str2[0], len, sizeof(str2));
                            if (!strcmp(str2, "False")) {
                                new_port_cfg.twoStepOverride = VTSS_APPL_PTP_TWO_STEP_OVERRIDE_FALSE;
                            } else if (!strcmp(str2, "True")) {
                                new_port_cfg.twoStepOverride = VTSS_APPL_PTP_TWO_STEP_OVERRIDE_TRUE;
                            }
                        }
                    }













                    if (memcmp(&new_port_cfg, &port_cfg, sizeof(vtss_appl_ptp_config_port_ds_t)) != 0) {
                        port_rc = vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &new_port_cfg);
                        if (port_rc != VTSS_RC_OK) {
                            error_no = port_rc;
                            error_port = port_no;
                            T_I("Clock instance %d, port %u : error setting port data set", inst, iport2uport(port_no));
                        }
                    }
                }
            }
        }
        sprintf(str1, "/ptp_clock_ports_config.htm?clock_inst=%d", inst);
        redirect(p, str1);
    } else {                    /* CYG_HTTPD_METHOD_GET (+HEAD) */
        T_D("Get Method");
        if(!cyg_httpd_start_chunked("html"))
            return -1;
        if ((vtss_appl_ptp_clock_config_default_ds_get(inst, &default_ds_cfg) == VTSS_RC_OK) &&
            (vtss_appl_ptp_clock_status_default_ds_get(inst, &default_ds_status) == VTSS_RC_OK))
        {
            dataSet = true;
            /* Delay Mechanism */
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "^e2e/p2p");
            (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            /* Multicast address option */
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "^Default/Link-local");
            (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            /* 2-step option */
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "^Clock Def./False/True");
            (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            (void)cyg_httpd_write_chunked("^", 1);
            (void)cyg_httpd_write_chunked("#", 1);
            port_iter_init_local(&pit);
            while (port_iter_getnext(&pit)) {
                (void) vtss_ifindex_from_port(sit.isid, pit.iport, &ifindex);
                (void) ptp_ifindex_to_port(ifindex, &port_no);
                if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &port_cfg) == VTSS_RC_OK) &&
                    (port_cfg.enabled == 1) &&
                    (vtss_appl_ptp_status_clocks_port_ds_get(inst, ifindex, &port_status) == VTSS_RC_OK))
                {
                    if (error_no != VTSS_RC_OK && port_no == error_port) {
                        ct = snprintf(err_str, sizeof(err_str), "Error setting parameter for port no %d", iport2uport(error_port));
                        T_I("Clock instance %d, error 0x%x", inst, error_no);
                        error_no = VTSS_RC_OK;
                    } else {
                        err_str[0] = 0;
                    }

                    ct = snprintf(p->outbuffer,sizeof(p->outbuffer),



                            "$%d/%s/%d/%s/%d/%d/%d/%s/%d/" VPRI64d "/" VPRI64d "/" VPRI64d "/%d/%s/%s/%d/%s/%s",

                            port_status.portIdentity.portNumber,
                            PortStateToString(port_status.portState),
                            port_status.logMinDelayReqInterval,
                            vtss_tod_TimeInterval_To_String(&port_status.peerMeanPathDelay, pmpd,','),
                            port_cfg.logAnnounceInterval,
                            port_cfg.announceReceiptTimeout,
                            port_cfg.logSyncInterval,
                            ptp_delaymechanism_disp(port_cfg.delayMechanism),
                            port_cfg.logMinPdelayReqInterval,
                            (port_cfg.delayAsymmetry >> 16),
                            (port_cfg.ingressLatency >> 16),
                            (port_cfg.egressLatency >> 16),
                            port_cfg.versionNumber,
                            (port_cfg.dest_adr_type == VTSS_APPL_PTP_PROTOCOL_SELECT_LINK_LOCAL) ? "Link-local" : "Default",
                            port_cfg.notSlave ? "True" : "False",
                            port_cfg.localPriority,
                            ptp_two_step_override_disp(port_cfg.twoStepOverride),
                            err_str















                            );
                    (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                }
            }
        }
        if (!dataSet)
            (void)cyg_httpd_write_chunked("",1);

        if ((vtss_appl_ptp_clock_config_default_ds_get(inst, &default_ds_cfg) == VTSS_RC_OK)) {
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "^%s",
                    ptp_profiles[default_ds_cfg.profile]);
            (void)cyg_httpd_write_chunked(p->outbuffer, ct);
        }

        cyg_httpd_end_chunked();

    }
    return -1; // Do not further search the file system.
}


static i32 handler_stat_ptp(CYG_HTTPD_STATE* p)
{
    i32         ct;
    uint inst;
    vtss_appl_ptp_clock_status_default_ds_t default_ds_status;
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    vtss_appl_ptp_ext_clock_mode_t mode;
    vtss_appl_ptp_config_port_ds_t port_cfg;
    vtss_ifindex_t ifindex;
    switch_iter_t sit;
    bool dataSet = false;
    port_iter_t       pit;

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_USID_CFG);

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_PTP))
        return -1;
#endif
    if (p->method == CYG_HTTPD_METHOD_POST) {
       T_D("Inside CYG_HTTPD_METHOD_POST ");
       redirect(p, "/ptp.htm");
    } else {
        if(!cyg_httpd_start_chunked("html"))
            return -1;
        /* show the local clock  */
        (void) vtss_appl_ptp_ext_clock_out_get(&mode);

        (void)cyg_httpd_write_chunked("^", 1);
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s|%s|%d|%s",
                                      ptp_one_pps_mode_disp(mode.one_pps_mode),
                                      ptp_bool_disp(mode.clock_out_enable),
                                      mode.freq,
                                      ptp_adj_method_disp(mode.adj_method));
        (void)cyg_httpd_write_chunked(p->outbuffer, ct);

        (void)cyg_httpd_write_chunked("^", 1);
        for (inst = 0; inst < PTP_CLOCK_INSTANCES; inst++)
        {
            T_D("Inst - %d",inst);
            if ((vtss_appl_ptp_clock_config_default_ds_get(inst, &default_ds_cfg) == VTSS_RC_OK) &&
                (vtss_appl_ptp_clock_status_default_ds_get(inst, &default_ds_status) == VTSS_RC_OK))
            {
                dataSet = true;
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "|%u", inst);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "/%u", default_ds_cfg.clock_domain);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "/%s", DeviceTypeToString(default_ds_cfg.deviceType));
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                port_iter_init_local(&pit);
                while (port_iter_getnext(&pit)) {
                    (void) vtss_ifindex_from_port(sit.isid, pit.iport, &ifindex);
                    if (vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &port_cfg) == VTSS_RC_OK) {
                        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "/%u", port_cfg.enabled);
                    } else {
                        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "/%u", 0);
                    }
                    (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                }
            }
        }
        if (!dataSet)
            (void)cyg_httpd_write_chunked("No entries",10);
        cyg_httpd_end_chunked();
    }
    return -1; // Do not further search the file system.
}

static i32 handler_clock_stat_ptp(CYG_HTTPD_STATE* p)
{
    i32         ct;
    uint  request_inst = 0;
    mesa_timestamp_t t;
    i8 str [14];
    int    var_value;
    vtss_appl_ptp_clock_status_default_ds_t default_ds_status;
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    vtss_appl_ptp_clock_current_ds_t clock_current_bs;
    vtss_appl_ptp_clock_slave_ds_t clock_slave_bs;

    vtss_appl_ptp_clock_parent_ds_t clock_parent_bs;
    vtss_appl_ptp_clock_timeproperties_ds_t clock_time_properties_bs;
#ifdef  PTP_OFFSET_FILTER_CUSTOM
    ptp_clock_servo_con_ds_t servo;
#else
    vtss_appl_ptp_clock_servo_config_t default_servo;
#endif /* PTP_OFFSET_FILTER_CUSTOM */
    vtss_appl_ptp_clock_filter_config_t filter_params;
    vtss_appl_ptp_unicast_slave_config_t uni_slave_cfg;
    vtss_appl_ptp_unicast_slave_table_t uni_slave_status;
    bool dataSet = false;
    i8 str1 [40];
    i8 str2 [40];
    i8 str3 [60];
    u32 hw_time;
    i32 ix;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_PTP))
        return -1;
#endif
    if (cyg_httpd_form_varable_int(p, "clock_inst", &var_value)) {
        request_inst = var_value;
    }

    if (p->method == CYG_HTTPD_METHOD_POST) {
        T_D("Inside CYG_HTTPD_METHOD_POST ");
        redirect(p, "/ptp_clock.htm");
    } else {
        if(!cyg_httpd_start_chunked("html"))
            return -1;
        if ((vtss_appl_ptp_clock_config_default_ds_get(request_inst, &default_ds_cfg) == VTSS_RC_OK) &&
            (vtss_appl_ptp_clock_status_default_ds_get(request_inst, &default_ds_status) == VTSS_RC_OK))
        {
            dataSet = true;
            /* send the local clock  */
            vtss_local_clock_time_get(&t, request_inst, &hw_time);
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "#%s %s", misc_time2str(t.seconds), vtss_tod_ns2str(t.nanoseconds, str,','));
            (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "/%s", clock_adjustment_method_txt(vtss_ptp_adjustment_method(request_inst)));
            (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            /* Send the Default clock Data Set */
            if (default_ds_cfg.deviceType == VTSS_APPL_PTP_DEVICE_NONE) {
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        " #%d /%s", request_inst,
                        DeviceTypeToString(default_ds_cfg.deviceType));
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            } else {
                if (vtss_appl_ptp_clock_filter_parameters_get(request_inst, &filter_params) != VTSS_RC_OK) {
                    T_W("Could not read filter parameters");
                }
                vtss_ptp_servo_mode_ref_t mode_ref;
                if (vtss_ptp_get_servo_mode_ref(request_inst, &mode_ref) != VTSS_RC_OK) {
                    T_W("Could not read servo mode");
                }
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "#%d/%d/%s/%s/%s/%s",
                        request_inst,
                        default_ds_cfg.clock_domain,
                        DeviceTypeToString(default_ds_cfg.deviceType),
                        ptp_profiles[default_ds_cfg.profile],
                        ptp_filter_type_disp(default_ds_cfg.filter_type),
                        sync_servo_mode_2_txt(mode_ref.mode));
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "#%s/%s/%s/%d/%s/%d",
                        DeviceTypeToString(default_ds_cfg.deviceType),
                        (default_ds_cfg.oneWay) ? "True" : "False",
                        (default_ds_cfg.twoStepFlag) ? "True" : "False",
                         default_ds_status.numberPorts,
                         ClockIdentityToString(default_ds_status.clockIdentity, str1),
                         default_ds_cfg.domainNumber);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "/%s/%d/%d/%d",
                        ClockQualityToString(&default_ds_status.clockQuality, str2),
                        default_ds_cfg.priority1,
                        default_ds_cfg.priority2,
                        default_ds_cfg.localPriority);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "/%s", ptp_protocol_disp(default_ds_cfg.protocol));
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer), "/%d/%d",
                         (default_ds_cfg.configured_vid),
                         (default_ds_cfg.configured_pcp));
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer), "/%d",
                              (default_ds_cfg.dscp));
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);





            }
            /* Send the current Clock Data Set */
            if (vtss_appl_ptp_clock_status_current_ds_get(request_inst, &clock_current_bs) == VTSS_RC_OK) {
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),



                        "#%d/%s/%s/%s/%s/%s/%s/%s/%s/%s",

                        clock_current_bs.stepsRemoved, vtss_tod_TimeInterval_To_String(&clock_current_bs.offsetFromMaster, str1,','),
                        vtss_tod_TimeInterval_To_String(&clock_current_bs.meanPathDelay, str2,',')









                        , "", "", "", "", "", "", ""

                        );
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            }
            /* Send the Clock Slave Data Set */
            if (vtss_appl_ptp_clock_status_slave_ds_get(request_inst, &clock_slave_bs) == VTSS_RC_OK) {
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                              "/%d/%s/%s",
                              clock_slave_bs.port_number,
                              vtss_ptp_slave_state_2_text(clock_slave_bs.slave_state),
                              vtss_ptp_ho_state_2_text(clock_slave_bs.holdover_stable, clock_slave_bs.holdover_adj, str1));
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            }
            if (vtss_appl_ptp_clock_status_parent_ds_get(request_inst, &clock_parent_bs) == VTSS_RC_OK) {
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "#%s/%d/%s/%d/%d",
                        ClockIdentityToString(clock_parent_bs.parentPortIdentity.clockIdentity, str1), clock_parent_bs.parentPortIdentity.portNumber,
                        ptp_bool_disp(clock_parent_bs.parentStats),
                        clock_parent_bs.observedParentOffsetScaledLogVariance,
                        clock_parent_bs.observedParentClockPhaseChangeRate);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),



                        "/%s/%s/%d/%d",

                        ClockIdentityToString(clock_parent_bs.grandmasterIdentity, str2),
                        ClockQualityToString(&clock_parent_bs.grandmasterClockQuality, str3),
                        clock_parent_bs.grandmasterPriority1,
                        clock_parent_bs.grandmasterPriority2



                        );
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            }
            if (vtss_appl_ptp_clock_status_timeproperties_ds_get(request_inst, &clock_time_properties_bs) == VTSS_RC_OK) {
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "#%d/%s/%s/%s/%s/%s/%s/%d",
                        clock_time_properties_bs.currentUtcOffset,
                        ptp_bool_disp(clock_time_properties_bs.currentUtcOffsetValid),
                        ptp_bool_disp(clock_time_properties_bs.leap59),
                        ptp_bool_disp(clock_time_properties_bs.leap61),
                        ptp_bool_disp(clock_time_properties_bs.timeTraceable),
                        ptp_bool_disp(clock_time_properties_bs.frequencyTraceable),
                        ptp_bool_disp(clock_time_properties_bs.ptpTimescale),
                        clock_time_properties_bs.timeSource);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            }
            /* send the filter parameters  */
            if (vtss_appl_ptp_clock_filter_parameters_get(request_inst, &filter_params) == VTSS_RC_OK) {
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "#%d/%d/%d",
                        filter_params.delay_filter,
                        filter_params.period,
                        filter_params.dist);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            }
            /* send the servo parameters  */
            if (vtss_appl_ptp_clock_servo_parameters_get(request_inst, &default_servo) == VTSS_RC_OK) {
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "#%s/%s/%s/%s/%d/%d/%d/%d",
                        ptp_bool_disp(default_servo.display_stats),
                        ptp_bool_disp(default_servo.p_reg),
                        ptp_bool_disp(default_servo.i_reg),
                        ptp_bool_disp(default_servo.d_reg),
                        default_servo.ap,
                        default_servo.ai,
                        default_servo.ad,
                        default_servo.gain);
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            }
            /* send the unicast slave parameters */
            (void)cyg_httpd_write_chunked("#", 1);
            ix = 0;
            while ((vtss_appl_ptp_clock_config_unicast_slave_config_get(request_inst, ix, &uni_slave_cfg) == VTSS_RC_OK) &&
                   (vtss_appl_ptp_clock_status_unicast_slave_table_get(request_inst, ix++, &uni_slave_status) == VTSS_RC_OK))
             {
                ct = snprintf(p->outbuffer,sizeof(p->outbuffer),
                        "%u/%d/%s/%d/%s|",
                        (ix-1),
                        uni_slave_cfg.duration,
                        misc_ipv4_txt(uni_slave_cfg.ip_addr, str1),
                        uni_slave_status.log_msg_period,
                        ptp_state_disp(uni_slave_status.comm_state));
                (void)cyg_httpd_write_chunked(p->outbuffer, ct);
            }
        }
        if (!dataSet)
            (void)cyg_httpd_write_chunked("",1);
        cyg_httpd_end_chunked();
    }
    return -1; // Do not further search the file system.
}































































static i32 handler_clock_ports_stat_ptp(CYG_HTTPD_STATE* p)
{
    i32         ct;
    int   var_value;
    uint  request_inst = 0;
    vtss_appl_ptp_clock_status_default_ds_t default_ds_status;
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    vtss_appl_ptp_config_port_ds_t port_cfg;
    vtss_appl_ptp_status_port_ds_t port_status;
    vtss_ifindex_t ifindex;
    switch_iter_t sit;
    bool dataSet = false;
    i8 pmpd [50];




    i8 str2 [40];
    i8 str3 [40];
    i8 str4 [40];
    port_iter_t       pit;

    (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_USID_CFG);

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_PTP))
        return -1;
#endif
    if (cyg_httpd_form_varable_int(p, "clock_inst", &var_value)) {
        request_inst = var_value;
    }
    if (p->method == CYG_HTTPD_METHOD_POST) {
        T_D("Inside CYG_HTTPD_METHOD_POST ");
        redirect(p, "/ptp_clock_ports.htm");
    } else {
        if(!cyg_httpd_start_chunked("html"))
            return -1;

        if ((vtss_appl_ptp_clock_config_default_ds_get(request_inst, &default_ds_cfg) == VTSS_RC_OK) &&
            (vtss_appl_ptp_clock_status_default_ds_get(request_inst, &default_ds_status) == VTSS_RC_OK))
        {
            dataSet = true;
            (void) cyg_httpd_write_chunked("#", 1);
            port_iter_init_local(&pit);
            while (port_iter_getnext(&pit)) {
                (void) vtss_ifindex_from_port(sit.isid, pit.iport, &ifindex);
                if ((vtss_appl_ptp_config_clocks_port_ds_get(request_inst, ifindex, &port_cfg) == VTSS_RC_OK) &&
                    (port_cfg.enabled == 1) &&
                    (vtss_appl_ptp_status_clocks_port_ds_get(request_inst, ifindex, &port_status) == VTSS_RC_OK))
                {
                    ct = snprintf(p->outbuffer,sizeof(p->outbuffer),



                            "$%d/%s/%d/%s/%d/%d/%d/%s/%d/%s/%s/%s/%d/%s/%s/%d^",

                            port_status.portIdentity.portNumber,
                            PortStateToString(port_status.portState),
                            port_status.logMinDelayReqInterval,
                            vtss_tod_TimeInterval_To_String(&port_status.peerMeanPathDelay, pmpd, ','),
                            port_cfg.logAnnounceInterval,
                            port_cfg.announceReceiptTimeout,port_cfg.logSyncInterval,ptp_delaymechanism_disp(port_cfg.delayMechanism),
                            port_cfg.logMinPdelayReqInterval,
                            vtss_tod_TimeInterval_To_String(&port_cfg.delayAsymmetry, str2,','),
                            vtss_tod_TimeInterval_To_String(&port_cfg.ingressLatency, str3,','),
                            vtss_tod_TimeInterval_To_String(&port_cfg.egressLatency, str4,','),
                            port_cfg.versionNumber,
                            (port_cfg.dest_adr_type == VTSS_APPL_PTP_PROTOCOL_SELECT_LINK_LOCAL) ? "Link-local" : "Default",
                            port_cfg.notSlave ? "True" : "False",
                            port_cfg.localPriority















                            );
                    (void)cyg_httpd_write_chunked(p->outbuffer, ct);
                }
            }
        }
        if (!dataSet)
            (void)cyg_httpd_write_chunked("",1);
        if ((vtss_appl_ptp_clock_config_default_ds_get(request_inst, &default_ds_cfg) == VTSS_RC_OK)) {
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "#%s",
                    ptp_profiles[default_ds_cfg.profile]);
            (void)cyg_httpd_write_chunked(p->outbuffer, ct);
        }

        cyg_httpd_end_chunked();
    }
    return -1; // Do not further search the file system.
}

static i32 handler_logs_ptp(CYG_HTTPD_STATE* p, int instance)
{
    uint request_inst = 0;
    int var_value;

    if (cyg_httpd_form_varable_int(p, "id", &var_value)) {
        request_inst = var_value;
    }
    if (p->method == CYG_HTTPD_METHOD_POST) {
        // We do not support HTTP POST request (only GET is supported)
    } else {
        if (!cyg_httpd_start_chunked("txt"))
            return -1;

        char log_file_name[20];
        snprintf(log_file_name, sizeof(log_file_name), "/tmp/ptp_log_%d.tpk", instance);

        FILE *log_file;
        if ((log_file = fopen(log_file_name, "r")) != NULL) {
            char c[1];
            while ((c[0] = (char)fgetc(log_file)) != EOF) {
                if (c[0] == '\n')
                    (void)cyg_httpd_write_chunked("\r", sizeof(char));
                (void)cyg_httpd_write_chunked(c, sizeof(c));
            }
            fclose(log_file);
        } else {
            char buf[] = "No PTP log file to get.\n";
            (void)cyg_httpd_write_chunked(buf, strlen(buf));
        }

        cyg_httpd_end_chunked();
    }
    return -1; // Do not further search the file system.
}

static i32 handler_logs_ptp_0(CYG_HTTPD_STATE* p)
{
    return handler_logs_ptp(p, 0);
}

static i32 handler_logs_ptp_1(CYG_HTTPD_STATE* p)
{
    return handler_logs_ptp(p, 1);
}

static i32 handler_logs_ptp_2(CYG_HTTPD_STATE* p)
{
    return handler_logs_ptp(p, 2);
}

static i32 handler_logs_ptp_3(CYG_HTTPD_STATE* p)
{
    return handler_logs_ptp(p, 3);
}

/****************************************************************************/
/*  Module Filter CSS routine                                               */
/****************************************************************************/
static size_t ptp_lib_filter_css(char **base_ptr, char **cur_ptr, size_t *length)
{
    vtss_zarlink_servo_t servo_type = (vtss_zarlink_servo_t)MESA_CAP(VTSS_APPL_CAP_ZARLINK_SERVO_TYPE);
    char buff[PTP_WEB_BUF_LEN];
    int cnt = 0;

    cnt += snprintf(buff + cnt, PTP_WEB_BUF_LEN - cnt, ".has_ptp_1as { display: none; }\r\n");

    if (servo_type != VTSS_ZARLINK_SERVO_ZLS30380 && servo_type != VTSS_ZARLINK_SERVO_ZLS30384 && servo_type != VTSS_ZARLINK_SERVO_ZLS30387) {
        cnt += snprintf(buff + cnt , PTP_WEB_BUF_LEN - cnt, ".has_ptp_zls30380_or_zls30384_or_zls30387 { display: none; }\r\n");
    }
    if (servo_type != VTSS_ZARLINK_SERVO_ZLS30380) {
        cnt += snprintf(buff + cnt , PTP_WEB_BUF_LEN - cnt, ".has_ptp_zls30380 { display: none; }\r\n");
    }
    if (servo_type != VTSS_ZARLINK_SERVO_ZLS30380 && servo_type != VTSS_ZARLINK_SERVO_ZLS30384) {
        cnt += snprintf(buff + cnt , PTP_WEB_BUF_LEN - cnt, ".has_ptp_zls30380_or_zls30384 { display: none; }\r\n");
    }
    if (servo_type != VTSS_ZARLINK_SERVO_ZLS30380 && servo_type != VTSS_ZARLINK_SERVO_ZLS30384 && servo_type != VTSS_ZARLINK_SERVO_ZLS30387) {
        cnt += snprintf(buff + cnt , PTP_WEB_BUF_LEN - cnt, ".has_ptp_zls30380_or_zls30384_or_zls30386_or_zls30387 { display: none; }\r\n");
    }
    if (servo_type != VTSS_ZARLINK_SERVO_ZLS30380 && servo_type != VTSS_ZARLINK_SERVO_ZLS30384) {
        cnt += snprintf(buff + cnt , PTP_WEB_BUF_LEN - cnt, ".has_ptp_zls30380_or_zls30384_or_zls30386 { display: none; }\r\n");
    }
    return webCommonBufferHandler(base_ptr, cur_ptr, length, buff);
}

/****************************************************************************/
/*  Filter CSS table entry                                                  */
/****************************************************************************/
web_lib_filter_css_tab_entry(ptp_lib_filter_css);

/****************************************************************************/
/*  HTTPD Table Entries                                                     */
/****************************************************************************/

CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_config_ptp, "/config/ptp_config", handler_config_ptp);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_clock_config_ptp, "/config/ptp_clock_config", handler_clock_config_ptp);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_clock_ports_config_ptp, "/config/ptp_clock_ports_config", handler_clock_ports_config_ptp);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_stat_ptp, "/stat/ptp", handler_stat_ptp);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_stat_ptp_clock, "/stat/ptp_clock", handler_clock_stat_ptp);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_stat_ptp_clock_ports, "/stat/ptp_clock_ports", handler_clock_ports_stat_ptp);



CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_logs_ptp_0, "/logs/ptp_log_0.tpk", handler_logs_ptp_0);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_logs_ptp_1, "/logs/ptp_log_1.tpk", handler_logs_ptp_1);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_logs_ptp_2, "/logs/ptp_log_2.tpk", handler_logs_ptp_2);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_logs_ptp_3, "/logs/ptp_log_3.tpk", handler_logs_ptp_3);

/****************************************************************************/
/*  End of file.                                                            */
/****************************************************************************/
