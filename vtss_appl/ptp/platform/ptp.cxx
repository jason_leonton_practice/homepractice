/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

/* NOTE: FOR DEBUGGING THE lu26 TRANSPARENT CLOCK FORWARDING */
//#define PTP_LOG_TRANSPARENT_ONESTEP_FORWARDING

//#define TIMEOFDAY_TEST
//#define PHY_DATA_DUMP
// use custom wireless delay filter

#include "main.h"
#include "main_types.h"
#include "ip_api.h"
#include "port_api.h"
#include "ptp_api.h"           /* Our module API */
#include "ptp.h"               /* Our private definitions */
#include "ptp_local_clock.h"   /* platform part of local_clock if */
#include "vtss_ptp_os.h"
#include "ptp_1pps_serial.h"
#include "ptp_pim_api.h"
#if defined(VTSS_SW_OPTION_MEP)
#include "mep_api.h"
#endif
#ifdef VTSS_SW_OPTION_ICFG
#include "ptp_icfg.h"
#endif
#if defined(VTSS_SW_OPTION_SYNCE)
#include "synce_api.h"
#include "synce_ptp_if.h"
#endif
#include "ptp_afi.h"

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_PTP

// base PTP
#include "vtss_ptp_types.h"
#include "vtss_ptp_api.h"
#include "vtss_ptp_local_clock.h"
#include "vtss_ptp_offset_filter.h"
#include "vtss_ptp_packet_callout.h"
#include "vtss_ptp_sys_timer.h"
#include "vtss_ptp_unicast.hxx"

/* Used APIs */
#include "l2proto_api.h"
#include "critd_api.h"
#include "packet_api.h"
#include "conf_api.h"
#include "misc_api.h"           /* instantiate MAC */
#include "acl_api.h"            /* set up access rule */
#include "interrupt_api.h"      /* interrupt handling */
#include "msg_api.h"            /* message module */
#include "vtss_timer_api.h"             /* timer system with no better resolution than the OS timer, but it runs on Linux */

#ifdef VTSS_SW_OPTION_PHY
#include "phy_api.h"
#endif
//  TOD
#include "vtss_tod_api.h"
#include "vtss_tod_phy_engine.h"
#if defined(VTSS_SW_OPTION_NTP)
#include "vtss_ntp_api.h"
#endif

#include "vtss/basics/new.hxx"

#if defined(VTSS_SW_OPTION_ZLS30387)
#include "zl_3038x_api_pdv_api.h"
#include "synce_custom_clock_api.h"
#endif

#include "mscc/ethernet/switch/api.h"
#include "vtss_tod_mod_man.h"
#include "tod_api.h"
#include "ptp_1pps_sync.h"     /* platform part: 1pps synchronization */
#include "ptp_1pps_closed_loop.h"     /* platform part: 1pps closed loop delay measurement */

#include "vtss_os_wrapper.h"
#include "vtss_os_wrapper_network.h"










//#define PTP_OFFSET_FILTER_CUSTOM
//#define PTP_DELAYFILTER_CUSTOM

#define API_INST_DEFAULT PHY_INST

static const u16 ptp_ether_type = 0x88f7;

// Cached capabilities
static uint32_t meba_cap_synce_dpll_mode_dual;
static uint32_t meba_cap_synce_dpll_mode_single;
static uint32_t mesa_cap_misc_chip_family;
static uint32_t mesa_cap_oam_used_as_ptp_protocol;
static uint32_t mesa_cap_packet_auto_tagging;
static uint32_t mesa_cap_phy_ts;
static uint32_t mesa_cap_port_cnt;
static uint32_t mesa_cap_synce_ann_auto_transmit;
static uint32_t mesa_cap_ts;
static uint32_t mesa_cap_ts_asymmetry_comp;
static uint32_t mesa_cap_ts_bc_ts_combo_is_special;
static uint32_t mesa_cap_ts_c_dtc_supported;
static uint32_t mesa_cap_ts_delay_req_auto_resp;
static uint32_t mesa_cap_ts_domain_cnt;
static uint32_t mesa_cap_ts_has_alt_pin;
static uint32_t mesa_cap_ts_has_ptp_io_pin;
static uint32_t mesa_cap_ts_hw_fwd_e2e_1step_internal;
static uint32_t mesa_cap_ts_hw_fwd_p2p_1step;
static uint32_t mesa_cap_ts_io_cnt;
static uint32_t mesa_cap_ts_internal_mode_supported;
static uint32_t mesa_cap_ts_internal_ports_req_twostep;
static uint32_t mesa_cap_ts_missing_tx_interrupt;
static uint32_t mesa_cap_ts_org_time;
static uint32_t mesa_cap_ts_p2p_delay_comp;
static uint32_t mesa_cap_ts_pps_via_configurable_io_pins;
static uint32_t mesa_cap_ts_ptp_rs422;
static uint32_t mesa_cap_ts_twostep_always_required;
static uint32_t mesa_cap_ts_use_external_input_servo;
static uint32_t vtss_appl_cap_max_acl_rules_pr_ptp_clock;
static uint32_t vtss_appl_cap_ptp_clock_cnt;
static uint32_t vtss_appl_cap_zarlink_servo_type;

#define SW_OPTION_BASIC_PTP_SERVO // Basic Servo needs to be encluded for the purpose of calibration

#ifdef SW_OPTION_BASIC_PTP_SERVO
static CapArray<ptp_basic_servo *, VTSS_APPL_CAP_PTP_CLOCK_CNT> basic_servo;
#endif // SW_OPTION_BASIC_PTP_SERVO
#if defined(VTSS_SW_OPTION_ZLS30387)
static CapArray<ptp_ms_servo *, VTSS_APPL_CAP_PTP_CLOCK_CNT> advanced_servo;
#endif

/**
 * \brief PTP Clock Config Data Set structure
 */
typedef struct ptp_instance_config_t {
    ptp_init_clock_ds_t                                                            clock_init;
    vtss_appl_ptp_clock_timeproperties_ds_t                                        time_prop;
    CapArray<vtss_appl_ptp_config_port_ds_t, MESA_CAP_PORT_CNT_PTP_PHYS_AND_VIRT>  port_config;
    vtss_appl_ptp_clock_filter_config_t                                            filter_params; /* default offset filter config */
    vtss_appl_ptp_clock_servo_config_t                                             servo_params;  /* default servo config */
    vtss_appl_ptp_unicast_slave_config_t                                           unicast_slave[MAX_UNICAST_MASTERS_PR_SLAVE]; /* Unicast slave config, i.e. requested master(s) */
    vtss_appl_ptp_clock_slave_config_t                                             slave_cfg;
    vtss_appl_ptp_virtual_port_config_t                                            virtual_port_cfg;
} ptp_instance_config_t;

typedef struct ptp_config_t {
    CapArray<ptp_instance_config_t, VTSS_APPL_CAP_PTP_CLOCK_CNT> conf;
    vtss_appl_ptp_ext_clock_mode_t init_ext_clock_mode; /* luton26/Jaguar/Serval(Synce) external clock mode */
    vtss_ptp_rs422_conf_t init_ext_clock_mode_rs422; /* Serval(RS422) external clock mode */
    vtss_ho_spec_conf_t init_ho_spec;
} ptp_config_t;

static ptp_config_t config_data ;
/* indicates if a transparent clock exists (only one transparent clock may exist in a node)*/
static bool transparent_clock_exists = false;
/* indicates if a slave clock exists for a domain (only one slave clock may exist in a timing domain)*/
static CapArray<u32, VTSS_APPL_CAP_PTP_CLOCK_CNT> slave_clock_exists;  // Note: All elements are initialized to 0 by constructor.
/* indicates if the wireless mode is enabled*/
typedef struct {
    bool                mode_enabled; /* true is fireless mode enabled */
    bool                remote_pre;   /* remote pre_notification => true, remote delay => false */
    bool                local_pre;    /* local pre_notification => true, local delay => false */
    mesa_timeinterval_t remote_delay;
    mesa_timeinterval_t local_delay;
    int                 pre_cnt;
} ptp_wireless_status_t;
static ptp_wireless_status_t wireless_status = {false, false, false, 0LL,0LL};

static mesa_packet_internal_tc_mode_t phy_ts_mode = MESA_PACKET_INTERNAL_TC_MODE_30BIT;

/* IPV4 protocol definitions */
#define PTP_EVENT_PORT 319
#define PTP_GENERAL_PORT 320
#define IP_HEADER_SIZE 20
#define UDP_HEADER_SIZE 8


static const u16 ip_ether_type = 0x0800;

static const mesa_mac_t ptp_eth_mcast_adr[2] = {{{0x01, 0x1b, 0x19, 0x00, 0x00, 0x00}}, {{0x01, 0x80, 0xC2, 0x00, 0x00, 0x0E}}};
static const mesa_mac_t ptp_ip_mcast_adr[2] = {{{0x01, 0x00, 0x5e, 0, 1, 129}}, {{0x01, 0x00, 0x5e, 0, 0, 107}}};

/****************************************************************************/
/*  Reserved ACEs functions and MAC table setup                             */
/****************************************************************************/
/* If all ports have 1588 PHY, no ACL rules are needed for timestamping */
/* instead the forwarding is set up in the MAC table */
#define PTP_MAC_ENTRIES (PTP_CLOCK_INSTANCES*2)
static mesa_mac_table_entry_t mac_entry[PTP_MAC_ENTRIES];
static u32 mac_used = 0;

typedef struct {
    u8 domain;
    u8 deviceType;
    u8 protocol;
    mesa_port_list_t port_list;
    mesa_port_list_t external_port_list;
    mesa_port_list_t internal_port_list;
    CapArray<mesa_ace_id_t, VTSS_APPL_CAP_MAX_ACL_RULES_PR_PTP_CLOCK> id;
    bool      internal_port_exists;
} ptp_ace_rule_t;

static CapArray<ptp_ace_rule_t, VTSS_APPL_CAP_PTP_CLOCK_CNT> rules;
/*
    {.domain = 0, .deviceType = VTSS_APPL_PTP_DEVICE_NONE, .protocol = VTSS_APPL_PTP_PROTOCOL_ETHERNET,
     .id = {ACL_MGMT_ACE_ID_NONE, ACL_MGMT_ACE_ID_NONE,ACL_MGMT_ACE_ID_NONE, ACL_MGMT_ACE_ID_NONE, ACL_MGMT_ACE_ID_NONE,ACL_MGMT_ACE_ID_NONE}},
 {.domain = 0, .deviceType = VTSS_APPL_PTP_DEVICE_NONE, .protocol = VTSS_APPL_PTP_PROTOCOL_ETHERNET,
     .id = {ACL_MGMT_ACE_ID_NONE, ACL_MGMT_ACE_ID_NONE,ACL_MGMT_ACE_ID_NONE, ACL_MGMT_ACE_ID_NONE, ACL_MGMT_ACE_ID_NONE,ACL_MGMT_ACE_ID_NONE}},
 {.domain = 0, .deviceType = VTSS_APPL_PTP_DEVICE_NONE, .protocol = VTSS_APPL_PTP_PROTOCOL_ETHERNET,
     .id = {ACL_MGMT_ACE_ID_NONE, ACL_MGMT_ACE_ID_NONE,ACL_MGMT_ACE_ID_NONE, ACL_MGMT_ACE_ID_NONE, ACL_MGMT_ACE_ID_NONE,ACL_MGMT_ACE_ID_NONE}},
 {.domain = 0, .deviceType = VTSS_APPL_PTP_DEVICE_NONE, .protocol = VTSS_APPL_PTP_PROTOCOL_ETHERNET,
     .id = {ACL_MGMT_ACE_ID_NONE, ACL_MGMT_ACE_ID_NONE,ACL_MGMT_ACE_ID_NONE, ACL_MGMT_ACE_ID_NONE, ACL_MGMT_ACE_ID_NONE,ACL_MGMT_ACE_ID_NONE}},
};
*/

/*
 *  This variable is set to true if all external PTP ports have timestamp PHY.
 *  In this case we use MAC table instead og ACL rulse, because the switch does not need to do any timestamping.
 */
static bool ptp_all_external_port_phy_ts = false;

/*
 *  This variable defines how many ticks to increment the internal timer for each callback from the global timer function
 *  The Value is normally 1 indicating a period og 1/128 sec.
 *  In slow CPU systems the value can be incremented to lower the CPU load. The result of this is that the max packet rate decreases.
 */
static u32 tick_factor = 1;


/* ================================================================= *
 *  Trace definitions
 * ================================================================= */


#if (VTSS_TRACE_ENABLED)

static vtss_trace_reg_t trace_reg = {
    VTSS_TRACE_MODULE_ID, "ptp", "Precision Time Protocol"
};

static vtss_trace_grp_t trace_grps[TRACE_GRP_CNT] = {
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default (PTP core)",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP|VTSS_TRACE_FLAGS_USEC
    },
    /* VTSS_TRACE_GRP_PTP_BASE_TIMER */ {
        "timer",
        "PTP Internal timer",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP|VTSS_TRACE_FLAGS_USEC
    },
    /* VTSS_TRACE_GRP_PTP_BASE_PACK_UNPACK */ {
        "pack",
        "PTP Pack/Unpack functions",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PTP_BASE_MASTER */ {
        "master",
        "PTP Master Clock Trace",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PTP_BASE_SLAVE */ {
        "slave",
        "PTP Slave Clock Trace",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PTP_BASE_STATE */ {
        "state",
        "PTP Port State Trace",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PTP_BASE_FILTER */ {
        "filter",
        "PTP filter module Trace",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PTP_BASE_PEER_DELAY */ {
        "peer_delay",
        "PTP peer delay module Trace",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PTP_BASE_TC */ {
        "tc",
        "PTP Transparent clock module Trace",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PTP_BASE_BMCA */ {
        "bmca",
        "PTP BMCA module Trace",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PTP_BASE_CLK_STATE */ {
        "clk_state",
        "slave clock state",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PTP_BASE_802_1AS */ {
        "802_1AS",
        "802.1AS specific trace",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_SERVO */ {
        "servo",
        "PTP Local Clock Servo",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_INTERFACE */ {
        "interface",
        "PTP Core interfaces",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_CLOCK */ {
        "clock",
        "PTP Local clock functions",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_1_PPS */ {
        "1_pps",
        "PTP 1 pps input functions",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_EGR_LAT */ {
        "egr_lat",
        "Jaguar 1 step egress latency",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PHY_TS */ {
        "phy_ts",
        "PHY Timestamp feature",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_REM_PHY */ {
        "rem_phy",
        "Remote PHY Timestamp feature",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PTP_SER */ {
        "serial_1pps",
        "PTP Serial 1pps interface feature",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PTP_PIM */ {
        "pim_proto",
        "PTP PIM protocol for 1pps and Modem modulation",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PTP_ICLI */ {
        "icli",
        "PTP ICLI log function",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PHY_1PPS */ {
        "phy_1pps",
        "PTP PHY 1pps Synchronization",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_SYS_TIME */ {
        "sys_time",
        "PTP and system time Synchronization",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_ACE */ {
        "ace",
        "PTP switch ACE configuration",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_MS_SERVO */ {
        "ms_servo",
        "PTP - MS Servo interface",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },




};
#endif /* VTSS_TRACE_ENABLED */

/* Thread variables */
static vtss_handle_t ptp_thread_handle;
static vtss_thread_t ptp_thread_block;

/* PTP global data */
/*lint -esym(457, ptp_global_control_flags) */
static vtss_flag_t ptp_global_control_flags;   /* PTP thread control */
static struct {
    bool ready;                                                   /* PTP Initited  */
    critd_t coremutex;                                            /* PTP core library serialization */
    mesa_mac_t sysmac;                                            /* Switch system MAC address */
    CapArray<struct in_addr, VTSS_APPL_CAP_PTP_CLOCK_CNT> my_ip;  /* one ip pr PTP instance */
    CapArray<ptp_clock_t *, VTSS_APPL_CAP_PTP_CLOCK_CNT> ptpi;    /* PTP instance handle */
} ptp_global;

struct Lock_PTP {
    Lock_PTP(int line) {
        critd_enter(&ptp_global.coremutex, VTSS_TRACE_GRP_DEFAULT, LOCK_TRACE_LEVEL, __FILE__, line);
    }
    ~Lock_PTP() {
        critd_exit(&ptp_global.coremutex, VTSS_TRACE_GRP_DEFAULT, LOCK_TRACE_LEVEL, __FILE__, 0);
    }
};

#define PTP_CORE_LOCK_SCOPE() Lock_PTP __lock_guard__(__LINE__)

static mesa_phy_ts_fifo_sig_mask_t my_sig_mask = MESA_PHY_TS_FIFO_SIG_MSG_TYPE | MESA_PHY_TS_FIFO_SIG_DOMAIN_NUM |
                                                 MESA_PHY_TS_FIFO_SIG_SOURCE_PORT_ID | MESA_PHY_TS_FIFO_SIG_SEQ_ID;    /*  PHY timestamp tx fifo signature */

static const int header_size_ip4 = sizeof(mesa_mac_t) + sizeof(ptp_global.sysmac) + sizeof(ptp_ether_type) + IP_HEADER_SIZE + UDP_HEADER_SIZE;

static mesa_rc ptp_phy_ts_update(int inst, int port);

static size_t pack_ip_udp_header(uchar * buf, u32 dest_ip, u32 src_ip, u16 port, u16 len, u16 dscp);

static mesa_rc ptp_servo_instance_set(uint instance);

static void one_pps_external_input_servo_action(i32 freq_offset);
static void one_pps_external_input_servo_init(bool start);

#if defined(VTSS_SW_OPTION_PRIVATE_MIB)
    #ifdef __cplusplus
        extern "C" void vtss_ptp_mib_init();
    #else
        void vtss_ptp_mib_init();
    #endif
#endif

#if defined(VTSS_SW_OPTION_JSON_RPC)
    #ifdef __cplusplus
        extern "C" void vtss_appl_ptp_json_init();
    #else
        void vtss_appl_ptp_json_init();
    #endif
#endif

/* port ingress and egress data
 * this datastructure is used to hold the port data which depends on the HW
 * architecture.
 */
typedef struct {
    vtss_tod_ts_phy_topo_t topo;                                               /* Phy timestamp topology info */
    bool port_ts_in_sync_tmp;                                                  /* false if the port has PHY timestamper, and timestamper is not in sync with master timer, otherwise TRUE */
    u32 delay_cnt;                                                             /* peer delay value */
    u32 asymmetry_cnt;                                                         /* link asymmetry */
    mesa_timeinterval_t ingress_latency;
    mesa_timeinterval_t egress_latency;
    mesa_timeinterval_t asymmetry;
    bool link_state;                                                           /* false if link is down, otherwise TRUE */
    CapArray<mesa_packet_filter_t, VTSS_APPL_CAP_PTP_CLOCK_CNT> vlan_forward;  /* packet forwarding filter obtained from mesa_packet_port_filter_get */
    CapArray<mesa_etype_t, VTSS_APPL_CAP_PTP_CLOCK_CNT>         vlan_tpid;     /* packet forwarding filter tpid obtained from mesa_packet_port_filter_get */
    CapArray<bool, VTSS_APPL_CAP_PTP_CLOCK_CNT>                 vlan_forw;     /* packet forwarding filter tpid obtained from mesa_packet_port_filter_get */
    bool      backplane_port;                                                  /* True if the port is a backplane port, i.e no timestamping is done on the port */
    u32       port_domain;                                                     /* The timing domain number assigned to this port */
    u32       inst_count;                                                      /* The number of BC, MA, SL instances assigned to this port */

} port_data_t;

CapArray<port_data_t, MESA_CAP_PORT_CNT_PTP_PHYS_AND_VIRT> port_data;

static CapArray<bool, VTSS_APPL_CAP_PTP_CLOCK_CNT> ports_org_time;

/*
 * If a TC exists, that forwards all packets in HW, then my_tc_encapsulation is set to the TC encapsulation type
 * If this type of TC exists, and a SlaveOnly instance also exists, this slave is used for syntonization of the TC,
 * and the packets forwarded to the CPU must also be forwarded, i.e. the ACL rules for this slaveOnly instance depends
 * on the TC configuration.
 */
static u8 my_tc_encapsulation = VTSS_APPL_PTP_PROTOCOL_MAX_TYPE;   /* indicates that no forwarding in the SlaveOnly instance is needed */
static int my_tc_instance = -1;
static int my_2step_tc_instance = -1;

static CapArray<vtss_ptp_servo_mode_ref_t, VTSS_APPL_CAP_PTP_CLOCK_CNT> current_servo_mode_ref;

/*
 * Statistics for the 1PPS/ timeofday slave function
 */
static vtss_ptp_one_pps_tod_statistics_t pim_tod_statistics;

static void one_pps_tod_statistics_clear(void)
{
    pim_tod_statistics.one_pps_cnt          = 0;
    pim_tod_statistics.missed_one_pps_cnt   = 0;
    pim_tod_statistics.missed_tod_rx_cnt    = 0;
}

// actual RS422 board configuration
static meba_ptp_rs422_conf_t rs422_conf;

mesa_rc ptp_clock_one_pps_tod_statistics_get(vtss_ptp_one_pps_tod_statistics_t *statistics, bool clear)
{
    PTP_CORE_LOCK_SCOPE();

    *statistics = pim_tod_statistics;
    if (clear) {
        one_pps_tod_statistics_clear();
    }
    return VTSS_RC_OK;
}

#define DEFAULT_1PPS_LATENCY        (12LL<<16)

static void ptp_in_sync_callback(mesa_port_no_t port_no, BOOL in_sync);

typedef struct {
    mesa_port_no_t              one_pps_slave_port_no;  // actual port used as a slave port in a 1PPS slave (if PIM protocol used, this is the port)
    vtss_ptp_timestamps_t       one_pps_t1_t2;
    bool                        enable_t1;  // set to true if timeofday is expected via a serial interface or PIM (t1).
    bool                        new_t1;     // set to true when t1 (serial/PIM data)is received, set to false when it is used
    bool                        new_t2;     // set to true when t2 (1PPS) is received, set to false when it is used
} ptp_one_pps_slave_data_t;

/* forward declaration */

/* io-pin usage */
typedef enum
{
    PTP_IO_PIN_USAGE_NONE,      // IO PIN is not used
    PTP_IO_PIN_USAGE_MAIN,      // IO PIN is used by the main ptp application for E.g. PHY control or station clock output
    PTP_IO_PIN_USAGE_RS422,     // IO PIN is used by the RS422 feature
    PTP_IO_PIN_USAGE_IO_PIN,    // IO PIN is used by the general IO_PIN function
} ptp_io_pin_usage_t;

typedef struct {
    vtss_appl_ptp_ext_io_mode_t   io_pin;
    meba_event_t                  source_id;
    int                           ptp_inst;
    ptp_one_pps_slave_data_t      slave_data;
    ptp_io_pin_usage_t            usage;
} ptp_io_pin_mode_t;

#define MAX_VTSS_TS_IO_ARRAY_SIZE 4

static ptp_io_pin_mode_t ptp_io_pin[MAX_VTSS_TS_IO_ARRAY_SIZE];
static const meba_event_t init_int_source_id[MAX_VTSS_TS_IO_ARRAY_SIZE] = {MEBA_EVENT_PTP_PIN_0, MEBA_EVENT_PTP_PIN_1, MEBA_EVENT_PTP_PIN_2, MEBA_EVENT_PTP_PIN_3};

static mesa_rc ptp_pin_init(void);
static mesa_rc io_pin_instance_set(int instance, u32 io_pin, BOOL enable);

static void io_pin_interrupt_handler(meba_event_t source_id, u32 instance_id);

static void ptp_port_filter_change_callback(int instance, mesa_port_no_t port_no, bool forward);
static bool ptp_port_domain_check(bool internal, bool enable, mesa_port_no_t port_no, uint instance);
static mesa_rc vtss_ptp_update_selected_src(void);

static void port_data_conf_set(mesa_port_no_t port_no, bool link_up)
{
    mesa_port_no_t shared_port;
    if (link_up) {
        /* is this port a PHY TS port ? */

        tod_ts_phy_topo_get(port_no, &port_data[port_no].topo);

        T_DG(VTSS_TRACE_GRP_PHY_TS, "Port_no = %d, ts_feature %d, channel %d, shared_port %d, in_sync %d", port_no,
             port_data[port_no].topo.ts_feature,
             port_data[port_no].topo.channel_id,
             port_data[port_no].topo.shared_port_no,
             port_data[port_no].topo.port_ts_in_sync);
        if (port_data[port_no].topo.port_shared) {
            shared_port = port_data[port_no].topo.shared_port_no;
            tod_ts_phy_topo_get(shared_port, &port_data[shared_port].topo);
            T_DG(VTSS_TRACE_GRP_PHY_TS, "Shared Port_no = %d, ts_feature %d, channel %d, shared_port %d, in_sync %d", shared_port,
             port_data[shared_port].topo.ts_feature,
             port_data[shared_port].topo.channel_id,
             port_data[shared_port].topo.shared_port_no,
             port_data[shared_port].topo.port_ts_in_sync);
        }
    }
}

static void port_data_initialize(void)
{
    int i;
    port_iter_t       pit;
    
    if (mesa_cap_phy_ts) {
        /* add in-sync callback function */
        PTP_RC(vtss_module_man_tx_timestamp_in_sync_cb_add(ptp_in_sync_callback));

        port_iter_init_local(&pit);
        while (port_iter_getnext(&pit)) {
            i = pit.iport;
    
            tod_ts_phy_topo_get(i, &port_data[i].topo);
    
            T_IG(VTSS_TRACE_GRP_PHY_TS, "Port_no = %d, ts_feature(gen) %d(%d), channel %d, shared_port %d, in_sync %d",
                                        i,
                                        port_data[i].topo.ts_feature, 
                                        port_data[i].topo.ts_gen,
                                        port_data[i].topo.channel_id,
                                        port_data[i].topo.shared_port_no,
                                        port_data[i].topo.port_ts_in_sync);
        }

    }
}

static void port_data_pre_initialize(void)
{
    int i,j;
    port_iter_t       pit;
    port_iter_init_local(&pit);
    while (port_iter_getnext(&pit)) {
        i = pit.iport;

        port_data[i].topo.port_ts_in_sync = TRUE;
        port_data[i].port_ts_in_sync_tmp = TRUE;
        port_data[i].topo.ts_feature = VTSS_PTP_TS_NONE;
        port_data[i].delay_cnt = 0;
        port_data[i].asymmetry_cnt = 0;
        port_data[i].ingress_latency = DEFAULT_INGRESS_LATENCY<<16;
        port_data[i].egress_latency = DEFAULT_EGRESS_LATENCY<<16;
        port_data[i].link_state = false;
        for (j = 0; j < PTP_CLOCK_INSTANCES; j++) {
            port_data[i].vlan_forward[j] =  MESA_PACKET_FILTER_DISCARD;
            port_data[i].vlan_tpid[j] = 0;
            port_data[i].vlan_forw[j] = false;
            if (mesa_cap_ts_org_time) {
                ports_org_time[j] = true;
            } else {
                ports_org_time[j] = false;
            }
        }
        port_data[i].backplane_port = false;
        port_data[i].port_domain = 0;
        port_data[i].inst_count = 0;
    }
}

/* latency observed in onestep tx timestamping */
static observed_egr_lat_t observed_egr_lat = {0,0,0,0};


/*
 * Forward defs
 */
static void
ptp_port_link_state_initial(int instance);
static mesa_rc ptp_ace_update(int i);
static void ext_clock_out_set(const vtss_appl_ptp_ext_clock_mode_t *mode);

static mesa_rc ext_clock_rs422_conf_set(const vtss_ptp_rs422_conf_t *mode);
static void clock_default_timeproperties_ds_get(vtss_appl_ptp_clock_timeproperties_ds_t *timeproperties_ds);

/*
 *  Extract date from string. Result is represented as the number of days since 1970-01-01
 */
int extract_date(const char* s, u16* date) {
    int d, m, y;
    
    // Check that string pointed to by s is exactly 10 characters long and that the delimiters are correct
    if (strlen(s) != 10 || s[4] != '-' || s[7] != '-') return 1;

    // Scan the string
    if (sscanf(s, "%04d-%02d-%02d", &y, &m, &d) != 3) return 1;
    
    // Initialize a tm struct with the values parsed from the string
    struct tm t = {0};
    t.tm_mday = d;
    t.tm_mon = m - 1;
    t.tm_year = y - 1900;
    t.tm_isdst = -1;
    
    // Convert to a time_t structure and then back to a tm structure to normalize the date.
    time_t when = mktime(&t);
    const struct tm *norm = localtime(&when);

    // validate (is the normalized date still the same?)
    if ((norm->tm_mday == d) && (norm->tm_mon == m - 1) && (norm->tm_year == y - 1900)) {
        *date = when / 86400;
        return 0;
    }
    else {
        return 1;
    }
}

/*
 * Conversion between l2port numbers and PTP port numbers.
 */
/* Convert from l2port (0-based) to PTP API port (1-based) */
static uint ptp_l2port_to_api(l2_port_no_t l2port)
{
    return (l2port + 1);
}

/* Compute the easy part of the checksum on a range of bytes. */

static u_int32_t checksum (unsigned char *buf,
                           unsigned nbytes,
                           u_int32_t sum)
{
    unsigned i;


    /* Checksum all the pairs of bytes first... */
    for (i = 0; i < (nbytes & ~1U); i += 2) {
        sum += (u_int16_t) ntohs(*((u_int16_t *)(buf + i)));
        /* Add carry. */
        if (sum > 0xFFFF)
            sum -= 0xFFFF;
    }

    /* If there's a single byte left over, checksum it, too.   Network
       byte order is big-endian, so the remaining byte is the high byte. */
    if (i < nbytes) {
        sum += buf [i] << 8;
        /* Add carry. */
        if (sum > 0xFFFF)
            sum -= 0xFFFF;
    }
    return sum;
}

/* Finish computing the checksum, and then put it into network byte order. */

static u_int32_t wrapsum (u_int32_t sum)
{
    sum = ~sum & 0xFFFF;
    return htons(sum);
}

/* Convert vtss_ifindex_t to PTP API port (0-based) */
mesa_rc ptp_ifindex_to_port(vtss_ifindex_t i, u32 *v)
{
    vtss_ifindex_elm_t e;
    VTSS_RC(vtss_ifindex_decompose(i, &e));
    if (e.iftype != VTSS_IFINDEX_TYPE_PORT) {
        T_D("Interface %u is not a port interface", VTSS_IFINDEX_PRINTF_ARG(i));
        return VTSS_RC_ERROR;
    }
    *v = e.ordinal;

    return VTSS_RC_OK;
}

/* Convert from pTP API port (1-based) to l2port (0-based) */
static l2_port_no_t ptp_api_to_l2port(uint port)
{
    return (port - 1);
}

/* Convert from l2port (0-based) to pTP API port (1-based) */
static uint l2port_to_ptp_api(l2_port_no_t port)
{
    return (port + 1);
}

#if defined(VTSS_SW_OPTION_SYNCE)
static u8 g8275_ql_to_class(vtss_appl_synce_quality_level_t ql, vtss_appl_ptp_device_type_t device_type)
{
    switch (device_type) {
        case VTSS_APPL_PTP_DEVICE_ORD_BOUND:
            switch(ql) {
                case VTSS_APPL_SYNCE_QL_PRC:  return G8275PRTC_BC_HO_CLOCK_CLASS;
                case VTSS_APPL_SYNCE_QL_SSUA: return G8275PRTC_BC_OUT_OF_HO_CLOCK_CLASS;
                case VTSS_APPL_SYNCE_QL_SSUB: return G8275PRTC_BC_OUT_OF_HO_CLOCK_CLASS;
                default: return DEFAULT_CLOCK_CLASS;
            }
        case VTSS_APPL_PTP_DEVICE_MASTER_ONLY:
            switch(ql) {
                case VTSS_APPL_SYNCE_QL_PRC:  return G8275PRTC_GM_OUT_OF_HO_CLOCK_CLASS_CAT1;
                case VTSS_APPL_SYNCE_QL_SSUA: return G8275PRTC_GM_OUT_OF_HO_CLOCK_CLASS_CAT2;
                case VTSS_APPL_SYNCE_QL_SSUB: return G8275PRTC_GM_OUT_OF_HO_CLOCK_CLASS_CAT3;
                default: return DEFAULT_CLOCK_CLASS;
            }
        case VTSS_APPL_PTP_DEVICE_SLAVE_ONLY:
            return G8275PRTC_TSC_CLOCK_CLASS;
        default: return DEFAULT_CLOCK_CLASS;
    }
}

static u32 g8275_ho_spec(vtss_appl_synce_quality_level_t ql)
{
    switch(ql) {
        case VTSS_APPL_SYNCE_QL_PRC:  return config_data.init_ho_spec.cat1;
        case VTSS_APPL_SYNCE_QL_SSUA: return config_data.init_ho_spec.cat2;
        case VTSS_APPL_SYNCE_QL_SSUB: return config_data.init_ho_spec.cat3;
        default: return  0;
    }

}
#endif //defined(VTSS_SW_OPTION_SYNCE)
/****************************************************************************/
/*  Time adjustment period handling                                         */
/****************************************************************************/
static vtss_ptp_sys_timer_t ptp_time_settle_timer;
static bool ptp_time_settling = false;
                                /*
 * PTP system time to PTP time synchronization and vice versa timer function
 */
/*lint -esym(459, ptp_time_settle_function) */
/* to avoid lint warnings for the PTP_CORE_UNLOCK() ; PTP_CORE_LOCK when calling out from the ptp_time_settle_function */
/*lint -e{455} */
/*lint -e{454} */

static void ptp_time_settle_function(vtss_timer_handle_t timer, void *m)
{
    ptp_time_settling = false;

    PTP_CORE_UNLOCK(); /* unlock because ptp_in_sync_callback takes the lock */

    if (mesa_cap_phy_ts) {
        port_iter_t pit;
        port_iter_init_local(&pit);
        while (port_iter_getnext(&pit)) {
            ptp_in_sync_callback(pit.iport, port_data[pit.iport].port_ts_in_sync_tmp);
        }
    }

    PTP_CORE_LOCK();
}

void ptp_time_setting_start(void)
{
    int32_t ptp_time_settle_init_value = 0; /**< Timer value used by the settle timer */
    if (mesa_cap_phy_ts) {
        ptp_time_settle_init_value = 128*6;
    } else {
        ptp_time_settle_init_value = 128*2;
    }

    vtss_ptp_timer_start(&ptp_time_settle_timer, ptp_time_settle_init_value, false);
    ptp_time_settling = true;
    T_I("start settling timer");
}

static void ptp_time_setting_init(void)
{
    vtss_init_ptp_timer(&ptp_time_settle_timer, ptp_time_settle_function, &ptp_time_settling);
    ptp_time_settling = false;
}

#if defined(VTSS_SW_OPTION_MEP)
static vtss_ptp_sys_timer_t oam_timer;
#define OAM_TIMER_INIT_VALUE 128
/*
 * PTP OAM slave timer
 */
/*lint -esym(459, ptp_oam_slave_timer) */
static void ptp_oam_slave_timer(vtss_timer_handle_t timer, void *m)
{
    mep_dm_timestamp_t far_to_near;
    mep_dm_timestamp_t near_to_far;
    vtss_ptp_timestamps_t ts;
    int instance = 0;
    mesa_rc rc;
    int clock_inst = (u64)m;
    ptp_clock_t *ptp = ptp_global.ptpi[clock_inst];
    vtss_appl_mep_dm_conf_t   config;
    mep_conf_t  conf;
    static i32 oam_timer_value = OAM_TIMER_INIT_VALUE;
    u32 mep_inst = 0, res_port;

    T_N("ptp %p", ptp);
    if (config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY && config_data.conf[instance].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM) {
        if (vtss_non_ptp_slave_check_port(ptp) == NULL) {
            /* check if OAM instance is active */
            oam_timer_value = OAM_TIMER_INIT_VALUE;
            if (mep_instance_conf_get(mep_inst, &conf) != VTSS_RC_OK) {
                T_W("MISSING MEP instance");
            } else {
                res_port = conf.port;

                if (conf.enable && conf.mode == VTSS_APPL_MEP_MEP && conf.direction == VTSS_APPL_MEP_DOWN && conf.domain == VTSS_APPL_MEP_PORT && conf.voe) {
                    rc = vtss_appl_mep_dm_conf_get(mep_inst, &config);
                    if (rc == VTSS_APPL_MEP_RC_INVALID_PARAMETER) {
                        T_W("MISSING OAM monitor");
                    } else {
                        if (!config.enable || config.calcway != VTSS_APPL_MEP_FLOW || config.tunit != VTSS_APPL_MEP_NS || !config.synchronized) {
                            T_I("OAM monitor not set up correctly");
                            config.enable = true;
                            config.calcway = VTSS_APPL_MEP_FLOW;
                            config.tunit = VTSS_APPL_MEP_NS;
                            config.synchronized = true;
                            rc = vtss_appl_mep_dm_conf_set(mep_inst, &config);
                            if (rc != VTSS_RC_OK) {
                                T_W("OAM monitor configuration failed");
                            }
                        } else {
                            oam_timer_value = (config.interval*128)/100;
                            T_N("OAM timer rate %d msec pr packet, timer valus %d", config.interval*10, oam_timer_value);

                            (void) vtss_ptp_port_ena(ptp_global.ptpi[instance], l2port_to_ptp_api(res_port));
                            ptp_port_filter_change_callback(instance, res_port, true);  /* ignore VLAN discarding function */
                            ptp_port_link_state_initial(instance);
                            vtss_non_ptp_slave_init(ptp_global.ptpi[instance], l2port_to_ptp_api(res_port));
                        }
                    }
                } else {
                    T_W("Wrong mep configuration");
                }
            }
        } else {
            if (wireless_status.remote_pre || wireless_status.local_pre) {
                T_I("Inst %d, remote_pre %d, local_pre %d", instance, wireless_status.remote_pre, wireless_status.local_pre);
                    if (++wireless_status.pre_cnt > 3) {
                        wireless_status.remote_pre = false;
                        wireless_status.local_pre = false;
                        wireless_status.pre_cnt = 0;
                    }
            } else {
                wireless_status.pre_cnt = 0;
                rc = mep_dm_timestamp_get(instance, &far_to_near, &near_to_far);
                if (rc == VTSS_RC_OK) {
                    T_D("Inst %d, far_to_near, tx_time  %12d:%9d, rx_time %12d:%9d", instance, far_to_near.tx_time.seconds,
                        far_to_near.tx_time.nanoseconds, far_to_near.rx_time.seconds, far_to_near.rx_time.nanoseconds);
                    ts.tx_ts.sec_msb = far_to_near.tx_time.sec_msb;
                    ts.tx_ts.seconds = far_to_near.tx_time.seconds;
                    ts.tx_ts.nanoseconds = far_to_near.tx_time.nanoseconds;
                    ts.rx_ts.sec_msb = far_to_near.rx_time.sec_msb;
                    ts.rx_ts.seconds = far_to_near.rx_time.seconds;
                    ts.rx_ts.nanoseconds = far_to_near.rx_time.nanoseconds;
                    ts.corr = wireless_status.remote_delay;
                    vtss_non_ptp_slave_t1_t2_rx(ptp_global.ptpi, clock_inst, &ts, DEFAULT_CLOCK_CLASS, 0);

                    T_D("Inst %d, near_to_far, tx_time  %12d:%9d, rx_time %12d:%9d", instance, near_to_far.tx_time.seconds,
                        near_to_far.tx_time.nanoseconds, near_to_far.rx_time.seconds, near_to_far.rx_time.nanoseconds);
                    ts.tx_ts.sec_msb = near_to_far.tx_time.sec_msb;
                    ts.tx_ts.seconds = near_to_far.tx_time.seconds;
                    ts.tx_ts.nanoseconds = near_to_far.tx_time.nanoseconds;
                    ts.rx_ts.sec_msb = near_to_far.rx_time.sec_msb;
                    ts.rx_ts.seconds = near_to_far.rx_time.seconds;
                    ts.rx_ts.nanoseconds = near_to_far.rx_time.nanoseconds;
                    ts.corr = wireless_status.local_delay;
                    vtss_non_ptp_slave_t3_t4_rx(ptp_global.ptpi, clock_inst, &ts);
                } else if (rc == VTSS_APPL_MEP_RC_NO_TIMESTAMP_DATA) {
                    T_I("Inst %d, no valid timestamp data", instance);
                } else {
                    T_I("Inst %d, OAM monitoring not active", instance);
                }
                if (rc != VTSS_RC_OK) {
                    vtss_non_ptp_slave_timeout_rx(ptp);
                }
            }
        }
        vtss_ptp_timer_start(&oam_timer, oam_timer_value, false);
    }
}
#endif

#define ONE_PPS_TIMER_INIT_VALUE 384
static CapArray<vtss_ptp_sys_timer_t, VTSS_APPL_CAP_PTP_CLOCK_CNT> one_pps_sync_timer;         /* 1PPS timeout timer pr instance*/
/*
 * PTP one_pps_synchronization timer function
 */
/*lint -esym(459, ptp_one_pps_slave_timer) */
static void ptp_one_pps_slave_timer(vtss_timer_handle_t timer, void *m)
{
    ptp_clock_t *ptp = (ptp_clock_t *)m;
    vtss_non_ptp_slave_timeout_rx(ptp);
    if (0 == vtss_non_ptp_slave_check_port_no(ptp)) {
        vtss_ptp_timer_stop(timer);
    } else {
        vtss_ptp_timer_start(timer, ONE_PPS_TIMER_INIT_VALUE, false);
    }
}

/*
 * PTP one_pps_synchronization slave function
 */
void ptp_1pps_ptp_slave_t1_t2_rx(int instance, mesa_port_no_t port_no, vtss_ptp_timestamps_t *ts)
{
    T_IG(VTSS_TRACE_GRP_1_PPS, "Instance %d", instance);
        if (config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY ||
                (config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY &&
                 config_data.conf[instance].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_ONE_PPS)) {
            if (ptp_global.ptpi[instance]->ssm.slave_port == NULL) {
                (void) vtss_ptp_port_ena_virtual(ptp_global.ptpi[instance], l2port_to_ptp_api(port_no));
                vtss_non_ptp_slave_init(ptp_global.ptpi[instance], l2port_to_ptp_api(port_no));
                T_IG(VTSS_TRACE_GRP_1_PPS, "Enabling virtual slave port %d", l2port_to_ptp_api(port_no));
            }
            vtss_non_ptp_slave_t1_t2_rx(ptp_global.ptpi, instance, ts, G8275PRTC_GM_CLOCK_CLASS, 0);
            vtss_ptp_timer_start(&one_pps_sync_timer[instance],ONE_PPS_TIMER_INIT_VALUE,false);
        } else {
            /* Master only PTP clock required */
            T_WG(VTSS_TRACE_GRP_1_PPS, "1PPS input only works on master only PTP clocks");
        }
}

static void ptp_external_input_slave_function(ptp_one_pps_slave_data_t *external_input_data, int inst, const mesa_timestamp_t  *ts, bool t1)
{
    char str1 [40];
    char str2 [40];

    if (inst >= 0 && external_input_data->one_pps_slave_port_no != VTSS_PORT_NO_NONE) {
        if (t1) {
            if (external_input_data->new_t1) {
                ++pim_tod_statistics.missed_one_pps_cnt;
            }
            external_input_data->one_pps_t1_t2.tx_ts = *ts;
            external_input_data->new_t1 = true;
            T_DG(VTSS_TRACE_GRP_1_PPS,"t1 %s", TimeStampToString(&external_input_data->one_pps_t1_t2.tx_ts, str1));
        } else {
            if (external_input_data->new_t2) {
                ++pim_tod_statistics.missed_tod_rx_cnt;
                T_IG(VTSS_TRACE_GRP_1_PPS,"missing 1pps timeofday from PIM");
            }
            external_input_data->one_pps_t1_t2.rx_ts = *ts;
            external_input_data->one_pps_t1_t2.corr = 0LL;
            external_input_data->new_t2 = true;
            T_DG(VTSS_TRACE_GRP_1_PPS,"t2 %s", TimeStampToString(&external_input_data->one_pps_t1_t2.rx_ts, str2));
            if (!external_input_data->enable_t1) {
                external_input_data->one_pps_t1_t2.tx_ts.sec_msb = ts->sec_msb;
                external_input_data->one_pps_t1_t2.tx_ts.seconds = ts->seconds;
                external_input_data->one_pps_t1_t2.tx_ts.nanoseconds = 0;
                if (ts->nanoseconds >= 500000000L) {
                    if (++external_input_data->one_pps_t1_t2.tx_ts.seconds == 0) {
                        ++external_input_data->one_pps_t1_t2.tx_ts.sec_msb;
                    }
                }
                external_input_data->new_t1 = true;
            } else {
                if (external_input_data->new_t1) {
                    ++pim_tod_statistics.missed_one_pps_cnt;
                }
                external_input_data->new_t1 = false;
            }
        }
        if (external_input_data->new_t1 && external_input_data->new_t2) {
            T_DG(VTSS_TRACE_GRP_1_PPS,"t1 %s t2 %s", TimeStampToString(&external_input_data->one_pps_t1_t2.tx_ts, str1), TimeStampToString(&external_input_data->one_pps_t1_t2.rx_ts, str2));
            ptp_1pps_ptp_slave_t1_t2_rx(inst, external_input_data->one_pps_slave_port_no, &external_input_data->one_pps_t1_t2);
            external_input_data->new_t1 = FALSE;
            external_input_data->new_t2 = FALSE;
        }
    }
}

static vtss_ptp_sys_timer_t sys_time_sync_timer;
#define SYS_TIME_SYNC_INIT_VALUE 128
#define MAX_RTD 60  /* max accepted turn around time between two gettimeofday calls */
static vtss_appl_ptp_system_time_sync_conf_t system_time_sync_conf = {VTSS_APPL_PTP_SYSTEM_TIME_NO_SYNC};

/*
 * PTP system time to PTP time synchronization and vice versa timer function
 * Only PTP HW domain 0 can be synchronized to System Time
 */
/*lint -esym(459, ptp_sys_time_sync_function) */
static void ptp_sys_time_sync_function(vtss_timer_handle_t timer, void *m)
{
    long clock_inst = (long) m;

    struct timeval tv;
    struct timespec ts_sys;
    struct timespec t4_sys;
    u32 tc;
    i32 rtd;        /* round trip relay in us */
    i32 offset;     /* current offset in ns */
    vtss_ptp_timestamps_t t1_t2;
    vtss_ptp_timestamps_t t3_t4;

    switch (system_time_sync_conf.mode) {
        case VTSS_APPL_PTP_SYSTEM_TIME_SYNC_SET:
            vtss_ptp_timer_start(&sys_time_sync_timer, SYS_TIME_SYNC_INIT_VALUE , true);
            T_DG(VTSS_TRACE_GRP_SYS_TIME, "SET SYSTEM TIME");
            (void)gettimeofday(&tv, NULL);
            vtss_tod_gettimeofday(0, &t1_t2.rx_ts, &tc); // Only domain 0 
            TIMEVAL_TO_TIMESPEC(&tv, &ts_sys)
            if (ts_sys.tv_sec == t1_t2.rx_ts.seconds+1) {
                offset = -VTSS_ONE_MIA + t1_t2.rx_ts.nanoseconds - ts_sys.tv_nsec;
            } else if (ts_sys.tv_sec == t1_t2.rx_ts.seconds-1) {
                offset = VTSS_ONE_MIA + t1_t2.rx_ts.nanoseconds - ts_sys.tv_nsec;
            } else if (ts_sys.tv_sec == t1_t2.rx_ts.seconds) {
                offset = t1_t2.rx_ts.nanoseconds - ts_sys.tv_nsec;
            } else {offset = 0x7fffffff;}
            if (labs(offset) > 20*VTSS_ONE_MILL) {
#if defined(VTSS_SW_OPTION_NTP)
                ntp_conf_t ntp_conf;
                if ((VTSS_RC_OK == ntp_mgmt_conf_get(&ntp_conf)) && ntp_conf.mode_enabled) {
                    T_WG(VTSS_TRACE_GRP_SYS_TIME, "cannot set system time if ntp enabled");
                    system_time_sync_conf.mode = VTSS_APPL_PTP_SYSTEM_TIME_NO_SYNC;
                }
#endif
                if (system_time_sync_conf.mode == VTSS_APPL_PTP_SYSTEM_TIME_SYNC_SET) {
                    tv.tv_sec = t1_t2.rx_ts.seconds;
                    tv.tv_usec = t1_t2.rx_ts.nanoseconds/1000;
                    (void)settimeofday(&tv, NULL);
                    T_IG(VTSS_TRACE_GRP_SYS_TIME, "system time %lu s : %lu us", tv.tv_sec, tv.tv_usec);
                }
            }
            break;
        case VTSS_APPL_PTP_SYSTEM_TIME_SYNC_GET:
            if (config_data.conf[0].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY) {

                clock_gettime(CLOCK_REALTIME, &ts_sys);
                vtss_tod_gettimeofday(0, &t1_t2.rx_ts, &tc); // only domain 0
                clock_gettime(CLOCK_REALTIME, &t4_sys);

                /* calculate round trip delay in us */
                rtd = (t4_sys.tv_sec - ts_sys.tv_sec)*1000000 + (t4_sys.tv_nsec - ts_sys.tv_nsec) / 1000;
                T_DG(VTSS_TRACE_GRP_SYS_TIME, "rtd: %d", rtd);
                if (rtd > MAX_RTD || rtd < 0) {
                    T_IG(VTSS_TRACE_GRP_SYS_TIME, "big rtd: %d ignored", rtd);
                } else {
                    t1_t2.tx_ts.sec_msb = 0;
                    t1_t2.tx_ts.seconds = ts_sys.tv_sec;
                    t1_t2.tx_ts.nanoseconds = ts_sys.tv_nsec;
                    t1_t2.corr = 0LL;
                    vtss_non_ptp_slave_t1_t2_rx(ptp_global.ptpi, clock_inst, &t1_t2, DEFAULT_CLOCK_CLASS, 0);

                    t3_t4.tx_ts = t1_t2.rx_ts;
                    t3_t4.rx_ts.sec_msb = 0;
                    t3_t4.rx_ts.seconds = t4_sys.tv_sec;
                    t3_t4.rx_ts.nanoseconds = t4_sys.tv_nsec;
                    t3_t4.corr = 0LL;
                    vtss_non_ptp_slave_t3_t4_rx(ptp_global.ptpi, clock_inst, &t3_t4);
                    T_DG(VTSS_TRACE_GRP_SYS_TIME, "t1: %u:%u", t1_t2.tx_ts.seconds, t1_t2.tx_ts.nanoseconds);
                    T_DG(VTSS_TRACE_GRP_SYS_TIME, "t2: %u:%u", t1_t2.rx_ts.seconds, t1_t2.rx_ts.nanoseconds);
                    T_DG(VTSS_TRACE_GRP_SYS_TIME, "t3: %u:%u", t3_t4.tx_ts.seconds, t3_t4.tx_ts.nanoseconds);
                    T_DG(VTSS_TRACE_GRP_SYS_TIME, "t4: %u:%u", t3_t4.rx_ts.seconds, t3_t4.rx_ts.nanoseconds);
                }
            } else {
                vtss_ptp_timer_stop(timer);
                system_time_sync_conf.mode = VTSS_APPL_PTP_SYSTEM_TIME_NO_SYNC;
            }
            break;
        default:
            vtss_ptp_timer_stop(timer);
            break;
    }
}

static void poll_port_filter(int instance)
{
    mesa_packet_port_info_t   info;
    u16 port;
    CapArray<mesa_packet_port_filter_t, MESA_CAP_PORT_CNT> filter;
    static CapArray<bool, VTSS_APPL_CAP_PTP_CLOCK_CNT> done_first_time;  // Note: All elements are initialize to 0 i.e. FALSE by constructor
    port_iter_t       pit;

    mesa_vid_t vid  = config_data.conf[instance].clock_init.cfg.configured_vid;
    PTP_RC(mesa_packet_port_info_init(&info));
    info.vid = vid; /* Tx VLAN ID */
    PTP_RC(mesa_packet_port_filter_get(NULL, &info, filter.size(), filter.data()));
    /* dump filter info */
    port_iter_init_local(&pit);
    while (port_iter_getnext(&pit)) {
        port = pit.iport;
        T_I("Inst %d, Port %d, VID %d, filter %d, tpid %x", instance, port, vid, filter[port].filter, filter[port].tpid);
        if (!done_first_time[instance]) {
            ptp_port_filter_change_callback(instance, port, filter[port].filter != MESA_PACKET_FILTER_DISCARD);
        } else {
            if (filter[port].filter != port_data[port].vlan_forward[instance] || filter[port].tpid != port_data[port].vlan_tpid[instance]) {
                port_data[port].vlan_forward[instance] = filter[port].filter;
                port_data[port].vlan_tpid[instance] = filter[port].tpid;
                ptp_port_filter_change_callback(instance, port, filter[port].filter != MESA_PACKET_FILTER_DISCARD);
            }
        }
    }
    done_first_time[instance] = true;
}

static void
initial_port_filter_get(int instance)
{
    mesa_packet_port_info_t   info;
    u16 port;
    CapArray<mesa_packet_port_filter_t, MESA_CAP_PORT_CNT> filter;
    port_iter_t       pit;

    mesa_vid_t vid  = config_data.conf[instance].clock_init.cfg.configured_vid;
    PTP_RC(mesa_packet_port_info_init(&info));
    info.vid = vid; /* Tx VLAN ID */
    PTP_RC(mesa_packet_port_filter_get(NULL, &info, filter.size(), filter.data()));
    /* dump filter info */
    port_iter_init_local(&pit);
    while (port_iter_getnext(&pit)) {
        port = pit.iport;
        T_I("Inst %d, Port %d, VID %d, filter %d, tpid %x", instance, port, vid, filter[port].filter, filter[port].tpid);
        port_data[port].vlan_forward[instance] = filter[port].filter;
        port_data[port].vlan_tpid[instance] = filter[port].tpid;
    }
}

static size_t pack_ethernet_header(uchar * buf, const mesa_mac_t dest, const mesa_mac_t src, u16 ether_type, int instance)
{
    int i = 0;
    /* DA MAC */
    memcpy(&buf[i], &dest, sizeof(mesa_mac_t));
    i += sizeof(mesa_mac_t);
    /* SA MAC */
    memcpy(&buf[i], &src, sizeof(mesa_mac_t));
    i += sizeof(mesa_mac_t);

    /* ethertype  */
    buf[i++] = (ether_type>>8) & 0xff;
    buf[i++] = ether_type & 0xff;

    return i;
}

static size_t unpack_ethernet_header(const uchar *const buf, mesa_mac_t *src, u16 *ether_type)
{

    int i = sizeof(mesa_mac_t);

    /* SA MAC */
    memcpy(src, &buf[i], sizeof(mesa_mac_t));
    i += sizeof(mesa_mac_t);

    /* ethertype  */
    *ether_type = (buf[i]<<8) + buf[i+1];
    i += 2;

    /* skip vlan tag if present */
    if (*ether_type != ptp_ether_type && *ether_type != ip_ether_type) {
        T_W("skipping tag  %d", *ether_type);
        i+=2;
        *ether_type = (buf[i]<<8) + buf[i+1];
        i += 2;
    }

    return i;
}

void vtss_1588_update_encap_header(u8 *buf, bool uni, bool event, u16 len, int instance)
{

    int i;
    u16 ether_type;
    mesa_mac_t src;
    u32 dest_ip;
    u32 src_ip;
    u16 port;
    i = sizeof(mesa_mac_t);
    if (uni) {  /* swap addresses */
        /* SA MAC */
        memcpy(&src, &buf[i], sizeof(mesa_mac_t));
        memcpy(&buf[i], &buf[0], sizeof(mesa_mac_t));
        memcpy(&buf[0], &src, sizeof(mesa_mac_t));
    } else {  /* update src addres */
        memcpy(&buf[i], &ptp_global.sysmac, sizeof(mesa_mac_t));
    }

    i += sizeof(mesa_mac_t);
    /* ethertype  */
    ether_type = (buf[i]<<8) + buf[i+1];
    i += 2;
    if (ether_type != ptp_ether_type && ether_type != ip_ether_type) {
        T_W("skipping tag  %d", ether_type);
        i+=2;
        ether_type = (buf[i]<<8) + buf[i+1];
        i += 2;
    }
    if (ether_type == ip_ether_type) {
        if (uni) {  /* SWAP IP adresses */
            dest_ip = vtss_tod_unpack32(buf+i+12);
            src_ip = vtss_tod_unpack32(buf+i+16);
        } else {
            dest_ip = vtss_tod_unpack32(buf+i+16);
            src_ip = ptp_global.my_ip[instance].s_addr;
        }
        port = event ? PTP_EVENT_PORT : PTP_GENERAL_PORT;
        (void)pack_ip_udp_header(buf+i, dest_ip, src_ip, port, len, config_data.conf[instance].clock_init.cfg.dscp);
    }
}

static void update_ptp_ip_address(int instance, mesa_vid_t vid)
{
    vtss_appl_ip_if_status_t status;
    int port;
    struct in_addr ip;
    ip.s_addr = 0;
    bool err = false;
    vtss_if_id_vlan_t conf_vid;
    port_iter_t       pit;
    conf_vid = vid;


    vtss_ifindex_t ifidx;
    (void) vtss_ifindex_from_vlan(conf_vid, &ifidx);
    if (vtss_appl_ip_if_status_get_first(VTSS_APPL_IP_IF_STATUS_TYPE_IPV4, ifidx, &status) == VTSS_OK) {
        if(status.type != VTSS_APPL_IP_IF_STATUS_TYPE_IPV4) {
            err = true;
        } else {
            ip.s_addr = status.u.ipv4.net.address;
            err = false;
        }

        T_D("my IP address= %x", ip.s_addr);
        if (!err) {
            if (ptp_global.my_ip[instance].s_addr != ip.s_addr) {
                ptp_global.my_ip[instance].s_addr = ip.s_addr;
                if (ptp_ace_update(instance) != VTSS_OK) T_IG(0, "ACE update error");

            }
        } else T_I("No IPV4 address defined for VID %d", conf_vid);
    } else T_I("Failed to get my IP address for VID %d", conf_vid);
    /* we also must reinitialize the ports, if the IP address has changed */
    port_iter_init_local(&pit);
    while (port_iter_getnext(&pit)) {
        port = pit.iport;
        ptp_port_filter_change_callback(instance, port,
                                        port_data[port].vlan_forw[instance]);
    }
    T_I("instance: %d, IP address= %x, err %d", instance, ip.s_addr, err);
}

static void notify_ptp_ip_address(vtss_if_id_vlan_t if_id)
{
    PTP_CORE_LOCK_SCOPE();

    T_D("if_id: %d", if_id);
    for (int i = 0; i < PTP_CLOCK_INSTANCES; i++) {
        if (config_data.conf[i].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            if (config_data.conf[i].clock_init.cfg.configured_vid == if_id) {
                update_ptp_ip_address(i, if_id);
            }
        }
    }
}

/*
 * buf = address of data buffer
 *
 */
static void update_udp_checksum(uchar * buf)
{
    u16 ether_type;
    uchar * ip_buf;
    int ip_header_len;
    uchar * udp_buf;
    u16 uh_sum;
    u16 len;
    /* check if IPv4 encapsulation */
    int i = 2*sizeof(mesa_mac_t);
    /* ethertype  */
    ether_type = (buf[i]<<8) + buf[i+1];
    i += 2;
    if (ether_type != ptp_ether_type && ether_type != ip_ether_type) {
        T_N("skipping tag  %x", ether_type);
        i+=2;
        ether_type = (buf[i]<<8) + buf[i+1];
        i += 2;
    }
    if (ether_type == ip_ether_type) {
        ip_buf = buf+i;
        ip_header_len = (ip_buf[0] & 0x0f) << 2;
        if (ip_buf[9] == IPPROTO_UDP) { // check if UDP protocol
            i += ip_header_len; /* i = start of UDP protocol header */
            udp_buf = buf+i;

            /* if checksum != 0 then recalculate the checksum */
            uh_sum = vtss_tod_unpack16(udp_buf+6);
            if (uh_sum != 0) {
                len = vtss_tod_unpack16(udp_buf+4);

                /* Compute UDP checksums, including the ``pseudo-header'', the UDP
                    header and the data. */
                vtss_tod_pack16(0, udp_buf+6); /* clear checksum */
                uh_sum =
                    wrapsum (checksum (udp_buf, UDP_HEADER_SIZE,
                                       checksum (udp_buf+UDP_HEADER_SIZE, len-8,
                                                 checksum (ip_buf + 12, 2 * sizeof(u32),
                                                         IPPROTO_UDP +
                                                         (u_int32_t) len))));
                if (uh_sum == 0) uh_sum = 0xffff;
                T_R("uh_sum after: %x, len = %d", uh_sum, len);
                memcpy(udp_buf+6, &uh_sum, sizeof(uh_sum));
            }
        }
    }
}

static size_t pack_ip_udp_header(uchar * buf, u32 dest_ip, u32 src_ip, u16 port, u16 len, u16 dscp)
{
    u16 ip_sum;
    /* Fill out the IP header */
    buf[0] = (4<<4) | (IP_HEADER_SIZE>>2);
    buf[1] = dscp<<2;
    vtss_tod_pack16(IP_HEADER_SIZE + UDP_HEADER_SIZE + len, buf+2);
    vtss_tod_pack16(0, buf+4); // identification = 0
    vtss_tod_pack16(0, buf+6); // fragmentation = 0
    buf[8] = 128; // time to live
    buf[9] = IPPROTO_UDP;; // protocol
    vtss_tod_pack16(0, buf+10); // header checksum
    //update_ip_address();
    vtss_tod_pack32(src_ip, buf+12);
    vtss_tod_pack32(dest_ip, buf+16);
    /* Checksum the IP header... */
    ip_sum = wrapsum (checksum (buf, IP_HEADER_SIZE, 0));
    memcpy(buf+10, &ip_sum, sizeof(ip_sum));

    /* Fill out the UDP header */
    vtss_tod_pack16(port, buf+IP_HEADER_SIZE);  /* source port same as dest port */
    vtss_tod_pack16(port, buf+IP_HEADER_SIZE+2);  /* dest port */
    vtss_tod_pack16(UDP_HEADER_SIZE + len, buf+IP_HEADER_SIZE+4);  /* message length */
    vtss_tod_pack16(0, buf+IP_HEADER_SIZE+6); /* 0 checksum is allowed */

    return IP_HEADER_SIZE + UDP_HEADER_SIZE;
}

static size_t unpack_ip_udp_header(const uchar * buf, u32 *src_ip, u32 *dest_ip, u16 *port, u16 *len)
{

    int             ip_header_len = (buf[0] & 0x0f) << 2;

    /* Decode the IP header */
    *len = vtss_tod_unpack16(buf+2) - ip_header_len - UDP_HEADER_SIZE;
    *src_ip = vtss_tod_unpack32(buf+12);
    *dest_ip = vtss_tod_unpack32(buf+16);

    /* Decode the UDP header */
    if (buf[9] == IPPROTO_UDP) { // protocol
        *port = vtss_tod_unpack16(buf+IP_HEADER_SIZE+2);  /* dest port */
        return ip_header_len + UDP_HEADER_SIZE;
    } else {
        *port = 0;
        return ip_header_len;
    }
}

/*
 * pack the Encapsulation protocol into a frame buffer.
 *
 */
size_t vtss_1588_pack_encap_header(u8 * buf, vtss_appl_ptp_protocol_adr_t *sender, vtss_appl_ptp_protocol_adr_t *receiver, u16 data_size, bool event, int instance)
{
    size_t i;
    vtss_appl_ptp_protocol_adr_t my_sender;
    if (sender != 0) {
        memcpy(&my_sender, sender, sizeof(my_sender));
    } else {
        my_sender.ip = ptp_global.my_ip[instance].s_addr;
        memcpy(&my_sender.mac, &ptp_global.sysmac, sizeof(my_sender.mac));
    }

    if (receiver->ip == 0) {
        i = pack_ethernet_header(buf, receiver->mac, my_sender.mac, ptp_ether_type, instance);
    } else {
        i = pack_ethernet_header(buf, receiver->mac, my_sender.mac, ip_ether_type, instance);
        i += pack_ip_udp_header(buf+i,receiver->ip, my_sender.ip, event ? PTP_EVENT_PORT : PTP_GENERAL_PORT, data_size, config_data.conf[instance].clock_init.cfg.dscp);
    }
    return i;
}

void vtss_1588_pack_eth_header(u8 * buf, mesa_mac_t receiver)
{
    /* DA MAC */
    memcpy(&buf[0], &receiver, sizeof(mesa_mac_t));

    /* SA MAC */
    memcpy(&buf[sizeof(mesa_mac_t)], &ptp_global.sysmac, sizeof(mesa_mac_t));
}


void vtss_1588_tag_get(ptp_tag_conf_t *tag_conf, int instance, vtss_ptp_tag_t *tag)
{
    if ((port_data[ptp_api_to_l2port(tag_conf->port)].vlan_forward[instance] == MESA_PACKET_FILTER_TAGGED)) {
        tag->tpid = port_data[ptp_api_to_l2port(tag_conf->port)].vlan_tpid[instance];
        tag->vid = tag_conf->vid;
        tag->pcp = tag_conf->pcp;
    } else {
        tag->tpid = 0;
        tag->vid = 0;
        tag->pcp = 0;
    }
}

/*
 * Propagate the PTP (module) configuration to the PTP core
 * library.
 */
static void ptp_conf_propagate(void)
{
    int i, j;
    /* Make effective in PTP core */
    for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
        vtss_ptp_clock_create(ptp_global.ptpi[i]);
        if (config_data.conf[i].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT || config_data.conf[i].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT) {
            transparent_clock_exists = true;
        }
        for (j = 0; j < MAX_UNICAST_MASTERS_PR_SLAVE; j++) {
            vtss_ptp_uni_slave_conf_set(ptp_global.ptpi[i], j, &config_data.conf[i].unicast_slave[j]);
        }
        ptp_port_link_state_initial(i);
        vtss_ptp_clock_slave_config_set(ptp_global.ptpi[i]->ssm.servo, &config_data.conf[i].slave_cfg);
        if (config_data.conf[i].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            update_ptp_ip_address(i, config_data.conf[i].clock_init.cfg.configured_vid);
            if (VTSS_RC_OK != ptp_servo_instance_set(i)) {
                T_W("Failed to set servo instance");
            }
        }

#if defined(VTSS_SW_OPTION_MEP)
        if (mesa_cap_oam_used_as_ptp_protocol) {
            if (i == 0 && config_data.conf[i].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY && config_data.conf[i].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM) {
                vtss_ptp_timer_start(&oam_timer, OAM_TIMER_INIT_VALUE, false);
            }
        }
#endif
        for (j = 0; j < MAX_UNICAST_MASTERS_PR_SLAVE; j++) {
            vtss_ptp_uni_slave_conf_set(ptp_global.ptpi[i], j, &config_data.conf[i].unicast_slave[j]);
        }
        if (ptp_ace_update(i) != VTSS_OK) T_IG(0, "ACE del error");;
    }
    /* initial setup of external clock output */
    ext_clock_out_set(&config_data.init_ext_clock_mode);
    if (mesa_cap_ts_ptp_rs422) {
        if (VTSS_RC_OK != ext_clock_rs422_conf_set(&config_data.init_ext_clock_mode_rs422) && config_data.init_ext_clock_mode_rs422.mode != VTSS_PTP_RS422_DISABLE) {
            T_W("Failed to set rs422 conf");
        }
    }
}


/**
 * initialize run-time options to reasonable values
 * \param create indicates a new default configuration block should be created.
 *
 */
static void ptp_conf_init(ptp_instance_config_t *conf, u8 instance)
{
    int i, j;
    port_iter_t       pit;

    /* use the Encapsulated MAC-48 value as default clockIdentity
     * according to IEEE Guidelines for 64-bit Global Identifier (EUI-64) registration authority
     */
    conf->clock_init.cfg.deviceType = VTSS_APPL_PTP_DEVICE_NONE;
    conf->clock_init.cfg.twoStepFlag = CLOCK_FOLLOWUP;
    conf->clock_init.cfg.protocol = VTSS_APPL_PTP_PROTOCOL_ETHERNET;
    conf->clock_init.cfg.oneWay = false;
    conf->clock_init.cfg.configured_vid = 1;
    conf->clock_init.cfg.configured_pcp = 0;
    conf->clock_init.cfg.clock_domain = 0;
    conf->clock_init.cfg.dscp = 0; // the recommended DSCP for the default PHB
    conf->clock_init.numberPorts = port_isid_port_count(VTSS_ISID_LOCAL) + mesa_cap_ts_io_cnt;
    conf->clock_init.numberEtherPorts = port_isid_port_count(VTSS_ISID_LOCAL);
    conf->clock_init.max_foreign_records = DEFAULT_MAX_FOREIGN_RECORDS;
    conf->clock_init.max_outstanding_records = DEFAULT_MAX_OUTSTANDING_RECORDS;
    memcpy(conf->clock_init.clockIdentity, ptp_global.sysmac.addr, 3);
    conf->clock_init.clockIdentity[3] = 0xff;
    conf->clock_init.clockIdentity[4] = 0xfe;
    memcpy(conf->clock_init.clockIdentity+5, ptp_global.sysmac.addr+3, 3);
    conf->clock_init.clockIdentity[7] += instance;
    if (mesa_cap_synce_ann_auto_transmit) {
        conf->clock_init.afi_announce_enable = true;
        conf->clock_init.afi_sync_enable = true;

    } else {
        conf->clock_init.afi_announce_enable = false;
        conf->clock_init.afi_sync_enable = false;
    }
    conf->clock_init.cfg.priority1 = 128;
    conf->clock_init.cfg.priority2 = 128;
    conf->clock_init.cfg.domainNumber = instance; // default domain is the same as instance number
    conf->clock_init.cfg.localPriority = 128;

    clock_default_timeproperties_ds_get(&conf->time_prop);

    vtss_appl_ptp_clock_config_default_virtual_port_config_get(&conf->virtual_port_cfg);

    port_iter_init_local(&pit);
    while (port_iter_getnext(&pit)) {
        i = pit.iport;
        conf->port_config[i].enabled = false;
        conf->port_config[i].logSyncInterval = DEFAULT_SYNC_INTERVAL;
        conf->port_config[i].delayMechanism = DELAY_MECH_E2E;
        conf->port_config[i].logAnnounceInterval = DEFAULT_ANNOUNCE_INTERVAL;
        conf->port_config[i].logMinPdelayReqInterval = DEFAULT_DELAY_REQ_INTERVAL;
        conf->port_config[i].announceReceiptTimeout = DEFAULT_ANNOUNCE_RECEIPT_TIMEOUT;
        conf->port_config[i].delayAsymmetry = DEFAULT_DELAY_ASYMMETRY<<16;
        conf->port_config[i].versionNumber = VERSION_PTP;
        conf->port_config[i].portInternal = false;
        conf->port_config[i].ingressLatency = DEFAULT_INGRESS_LATENCY<<16;
        conf->port_config[i].egressLatency = DEFAULT_EGRESS_LATENCY<<16;
        conf->port_config[i].localPriority = 128;
        conf->port_config[i].dest_adr_type = VTSS_APPL_PTP_PROTOCOL_SELECT_DEFAULT;
        conf->port_config[i].notSlave = FALSE;
        conf->port_config[i].twoStepOverride = VTSS_APPL_PTP_TWO_STEP_OVERRIDE_NONE;
        conf->port_config[i].c_802_1as.allowedLostResponses = DEFAULT_MAX_CONTIGOUS_MISSED_PDELAY_RESPONSE;
        conf->port_config[i].c_802_1as.neighborPropDelayThresh = VTSS_MAX_TIMEINTERVAL; // max value set as default.
        conf->port_config[i].c_802_1as.syncReceiptTimeout = 3;
    }
    /* default offset filter config */
    vtss_appl_ptp_filter_default_parameters_get(&conf->filter_params, VTSS_APPL_PTP_PROFILE_NO_PROFILE);

    /* default servo config */
    vtss_appl_ptp_clock_servo_default_parameters_get(&conf->servo_params, VTSS_APPL_PTP_PROFILE_NO_PROFILE);

    /* initialize unicast slave table */
    for (j = 0; j < MAX_UNICAST_MASTERS_PR_SLAVE; j++) {
        conf->unicast_slave[j].duration = 100;
        conf->unicast_slave[j].ip_addr = 0;
    }
    vtss_appl_ptp_clock_slave_default_config_get(&conf->slave_cfg);

}


/**
 * Read the PTP configuration.
 * \param create indicates a new default configuration block should be created.
 *
 */
static void ptp_conf_read(bool create)
{
    int i;
    /* initialize run-time options to reasonable values */
    T_I("conf create");

    for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
        if (config_data.conf[i].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            ptp_global.ptpi[i]->ssm.servo->deactivate(config_data.conf[i].clock_init.cfg.clock_domain);  // Deactivate the servo associated with the clock before the clock is deleted
        }
        ptp_conf_init(&config_data.conf[i], i);
    }
    vtss_ext_clock_out_default_get(&config_data.init_ext_clock_mode);
    system_time_sync_conf.mode = VTSS_APPL_PTP_SYSTEM_TIME_NO_SYNC;
    config_data.init_ext_clock_mode_rs422.mode = VTSS_PTP_RS422_DISABLE;
    config_data.init_ext_clock_mode_rs422.delay = 0;
}

static bool ptp_tc_sw_forwarding (int inst)
{
    bool rv = false;

    if  (((config_data.conf[inst].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT ||
           config_data.conf[inst].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT) &&
           config_data.conf[inst].clock_init.cfg.twoStepFlag)
      || (config_data.conf[inst].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT &&
          !config_data.conf[inst].clock_init.cfg.twoStepFlag && rules[inst].internal_port_exists &&
          !mesa_cap_ts_hw_fwd_e2e_1step_internal)
      || (config_data.conf[inst].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT &&
          !config_data.conf[inst].clock_init.cfg.twoStepFlag && !mesa_cap_ts_hw_fwd_p2p_1step))
    {
        rv = true;
    }

    T_D("rv = %d", rv);
    return rv;
}

/** \brief ptp protocol level */
typedef enum
{
    VTSS_PTP_PROTOCOL_LEVEL_NONE,     /**< Any frame type */
    VTSS_PTP_PROTOCOL_LEVEL_ETYPE,   /**< Ethernet Type */
    VTSS_PTP_PROTOCOL_LEVEL_IPV4,    /**< IPv4 */
} vtss_ptp_protocol_level_t;

static vtss_ptp_protocol_level_t protocol_level(int protocol)
{
    if ((protocol == VTSS_APPL_PTP_PROTOCOL_IP4MULTI) || (protocol == VTSS_APPL_PTP_PROTOCOL_IP4MIXED) || (protocol == VTSS_APPL_PTP_PROTOCOL_IP4UNI))
        return VTSS_PTP_PROTOCOL_LEVEL_IPV4;
    if (protocol == VTSS_APPL_PTP_PROTOCOL_MAX_TYPE) return VTSS_PTP_PROTOCOL_LEVEL_NONE;
    return VTSS_PTP_PROTOCOL_LEVEL_ETYPE;
}



/****************************************************************************
 * Callbacks
 ****************************************************************************/

/*
 * Local port packet receive indication
 */
static BOOL packet_rx(void *contxt,
                      const u8 *const frm,
                      const mesa_packet_rx_info_t *const rx_info)
{
    int i;
    char str1[20];
    char str2[20];
    u16 etype;
    u16 udp_port = 0;
    u16 len;
    u32 rxTime = 0;
    mesa_bool_t timestamp_ok = false;
    vtss_appl_ptp_protocol_adr_t sender;
    bool release_fdma_buffers = true;
    ptp_tx_buffer_handle_t buf_handle;




    BOOL ongoing_adjustment = false;

    if (mesa_cap_ts_missing_tx_interrupt) {
        /* No timestamp interrupt, therefore do the timestamp update here */
        T_DG(_I,"update timestamps");
        PTP_RC(mesa_tx_timestamp_update(nullptr));
    }

    ulong j = unpack_ethernet_header(frm, &sender.mac, &etype);
    T_RG(_I, "RX_ptp, etype = %04x, port = %d, da= %s, sa = %s", etype, rx_info->port_no, misc_mac_txt(&frm[0],str1), misc_mac_txt(&frm[6], str2));
    T_RG_HEX(_I, frm, rx_info->length);
    sender.ip = 0;

    u32 dest_ip = 0;
    if (etype == ip_ether_type) {
        j += unpack_ip_udp_header(frm + j, &sender.ip, &dest_ip, &udp_port, &len);
        T_NG(_I, "RX_udp, udp_port = %d, port = %d", udp_port, rx_info->port_no);
        if (udp_port != PTP_GENERAL_PORT && udp_port != PTP_EVENT_PORT) {
            T_WG(_I, "This UDP packet is not intended for me, udp_port = %d, port = %d", udp_port, rx_info->port_no);
            return false; /* this IP packet was not intended for me: i.e forward to other subscribers. */
        }
    }
    if (((etype == ptp_ether_type) && (rx_info->length > j)) ||
            ((etype == ip_ether_type) && (rx_info->length > j) && (udp_port == PTP_GENERAL_PORT || udp_port == PTP_EVENT_PORT))) {
        u8 message_type = frm[j] & 0xf;

        u8 domain_number = frm[j + 4];
        uint rx_port = ptp_l2port_to_api(rx_info->port_no);
        T_DG(_I, "message_type: %d, domain %d, rx_port %d", message_type, domain_number, rx_port);
        PTP_CORE_LOCK();
        if (message_type <= 3) {
            mesa_packet_ptp_message_type_t ptp_message_type;
            if (message_type == PTP_MESSAGE_TYPE_SYNC) {
                ptp_message_type = MESA_PACKET_PTP_MESSAGE_TYPE_SYNC;
            } else if (message_type == PTP_MESSAGE_TYPE_P_DELAY_RESP) {
                ptp_message_type = MESA_PACKET_PTP_MESSAGE_TYPE_P_DELAY_RESP;
            } else {
                ptp_message_type = MESA_PACKET_PTP_MESSAGE_TYPE_GENERAL;
            }

            mesa_packet_timestamp_props_t ts_props;
            ts_props.ts_feature_is_PTS = (port_data[rx_info->port_no].topo.ts_feature == VTSS_PTP_TS_PTS);
            ts_props.phy_ts_mode = phy_ts_mode;
            ts_props.backplane_port = port_data[rx_info->port_no].backplane_port;
            ts_props.delay_comp.delay_cnt = port_data[rx_info->port_no].delay_cnt;
            ts_props.delay_comp.asymmetry_cnt = port_data[rx_info->port_no].asymmetry_cnt;

            (void)mesa_ptp_get_timestamp(NULL,
                                         frm + j + PTP_MESSAGE_RESERVED_FOR_TS_OFFSET,
                                         rx_info,
                                         ptp_message_type,
                                         ts_props,
                                         &rxTime,
                                         &timestamp_ok);
        }
        (void)mesa_ts_ongoing_adjustment(NULL, &ongoing_adjustment);
        for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
            /* ignore packets if protocol encapsulation does not match the configuration */
            if ((etype == ptp_ether_type && (config_data.conf[i].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_ETHERNET ||
                                             config_data.conf[i].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_ETHERNET_MIXED)) ||
                (etype == ip_ether_type && (config_data.conf[i].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_IP4MULTI ||
                                            config_data.conf[i].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_IP4MIXED ||
                                            config_data.conf[i].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_IP4UNI))) {
                if (config_data.conf[i].port_config[rx_info->port_no].enabled &&
                               ((config_data.conf[i].clock_init.cfg.domainNumber == domain_number)  ||
                                (((config_data.conf[i].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT ||
                                config_data.conf[i].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT) &&
                                ptp_tc_sw_forwarding(i)) &&
                                 (message_type != PTP_MESSAGE_TYPE_P_DELAY_REQ && message_type != PTP_MESSAGE_TYPE_P_DELAY_RESP && message_type != PTP_MESSAGE_TYPE_P_DELAY_RESP_FOLLOWUP)
                                 )))
                {
                    if (!release_fdma_buffers) {
                        T_DG(_I, "inst %d, message_type %d, buffer from domain %d has already been forwarded", i, message_type, domain_number);
                    } else {
                        if (mesa_cap_ts_bc_ts_combo_is_special) {
                            if (timestamp_ok) {
                                /* special case for combined BC and TC  */
                                /* in L26, the rxtime is added to the correctionField in this case, because L26 does both onestep and twostep on the sync packet */
                                if (protocol_level(my_tc_encapsulation) == protocol_level(config_data.conf[i].clock_init.cfg.protocol) && my_tc_instance > 0 && message_type == PTP_MESSAGE_TYPE_SYNC) {
                                    mesa_timeinterval_t corr = vtss_tod_unpack64(frm + j + PTP_MESSAGE_CORRECTION_FIELD_OFFSET);
                                    corr -= 4*(((mesa_timeinterval_t)rxTime)<<16);
                                    /*Update correction Field */
                                    vtss_tod_pack64(corr, (u8 *)(frm + j + PTP_MESSAGE_CORRECTION_FIELD_OFFSET));
                                }
                            }
                        }

                        T_NG(_I, "clock instance: %d, domain %d", i, domain_number);
                        if (rx_info->tag.vid == config_data.conf[i].clock_init.cfg.configured_vid) {
                            T_IG(_I,"Accepted Vlan tag: tag %x vid %d,", rx_info->tag_type, rx_info->tag.vid);
                            /* fill in the buffer structure */
                            vtss_1588_tx_handle_init(&buf_handle);
                            buf_handle.handle = 0;
                            buf_handle.frame = (u8 *)frm;                          /* pointer to frame buffer */
                            buf_handle.size = rx_info->length;                    /* length of data received, including encapsulation. */
                            buf_handle.header_length = j;                    /* length of encapsulation header */
                            buf_handle.hw_time = rxTime;                     /* internal HW time value used for correction field update. */
                            buf_handle.tag.vid = config_data.conf[i].clock_init.cfg.configured_vid; /* use configured tag for forwarding*/
                            buf_handle.tag.pcp = config_data.conf[i].clock_init.cfg.configured_pcp; /* use configured tag for forwarding*/
                            buf_handle.tag.tpid = port_data[rx_info->port_no].vlan_tpid[i];
                            T_IG(_I,"Handle Vlan tpid %x vid %d,", buf_handle.tag.tpid, buf_handle.tag.vid);

                            if (message_type <= 3) {
                                // EVENT_PDU:
                                T_IG(_I, "EVENT_PDU: %u bytes, port %u, msg_type %d, timestamp_ok %d", rx_info->length, rx_port, message_type, timestamp_ok);
                                /* if the port has PHY 1588 support, the timestamp is read from the reserved field */
                                if (timestamp_ok && port_data[rx_info->port_no].port_ts_in_sync_tmp && !ongoing_adjustment) {
                                    release_fdma_buffers = !vtss_ptp_event_rx(ptp_global.ptpi, i, rx_port, &buf_handle, &sender);
                                }
                            } else if (message_type >=8 && message_type <=0xd) {
                                // GENERAL_PDU:
                                T_NG(_I, "GENERAL_PDU: %u bytes, port %u", rx_info->length, rx_port);
                                release_fdma_buffers = !vtss_ptp_general_rx(ptp_global.ptpi, i, rx_port, &buf_handle, &sender);
                            } else {
                                T_EG(_I, "Invalid message type %d received", message_type);
                            }
                        } else {
                            T_IG(_I,"Not Accepted Vlan tag: tag %x vid %d,", rx_info->tag_type, rx_info->tag.vid);
                        }
                    }
                }
            }
        }
        PTP_CORE_UNLOCK();
    } else {
        T_EG(_I, "Invalid message length %u or etype %04x received", rx_info->length, etype);
    }

    return true; // Don't allow other subscribers to receive the packet
}

/****************************************************************************
 * PTP callouts - vtss_ptp_packet_callout.h
 ****************************************************************************/
// This macro must *not* evaluate to an empty macro, since it will be called an expecting to do useful stuff.
#define PARAMETER_CHECK(x, y) do {if (!(x)) {T_E("Assertion failed: " #x); y}} while (0)

size_t vtss_1588_prepare_tx_buffer(ptp_tx_buffer_handle_t *tx_buf, u32 length)
{
    u8 * frame;

    if (tx_buf->handle == 0) {
        frame = packet_tx_alloc(length + tx_buf->header_length + 4); // received packet length  may be up to 4 bytes longer than expected //
        if (frame && (tx_buf->size <= length + tx_buf->header_length + 4)) {
            memcpy(frame, tx_buf->frame, tx_buf->size);
            tx_buf->frame = frame;
            T_I("allocated a new buffer of size %u for transmission", length + tx_buf->header_length);
            return length + tx_buf->header_length;
        } else {
            tx_buf->frame = 0;
            T_E("new buffer adr %p, size %u, old size " VPRIz, frame, length + tx_buf->header_length, tx_buf->size);
            if (frame) packet_tx_free(frame);
            return 0;
        }
    } else {
        return tx_buf->size;
    }
}

size_t vtss_1588_prepare_general_packet(u8 **frame, vtss_appl_ptp_protocol_adr_t *receiver, size_t size, size_t *header_size, int instance)
{
    // Check parameters
    PARAMETER_CHECK(frame != NULL, return 0;);
    *frame = packet_tx_alloc(size + header_size_ip4);
    if (*frame) {
        *header_size = vtss_1588_pack_encap_header(*frame, NULL, receiver,  size, false, instance);
        return size + *header_size;
    } else {
        return 0;
    }
}

size_t vtss_1588_prepare_general_packet_2(u8 **frame, vtss_appl_ptp_protocol_adr_t *sender, vtss_appl_ptp_protocol_adr_t *receiver, size_t size, size_t *header_size, int instance)
{
    // Check parameters
    PARAMETER_CHECK(frame != NULL, return 0;);
    *frame = packet_tx_alloc(size + header_size_ip4);
    if (*frame) {
        *header_size = vtss_1588_pack_encap_header(*frame, sender, receiver,  size, false, instance);
        return size + *header_size;
    } else {
        return 0;
    }
}

void vtss_1588_release_general_packet(u8 **handle)
{
    // Check parameters
    PARAMETER_CHECK(handle != NULL, return;);
    PARAMETER_CHECK(*handle != NULL, return;); /* no buffer allocated */
    packet_tx_free(*handle);
    *handle = NULL;
}

size_t vtss_1588_packet_tx_alloc(void **handle, u8 **frame, size_t size)
{
    // Check parameters
    PARAMETER_CHECK(handle != NULL, return 0;);
    PARAMETER_CHECK(frame != NULL, return 0;);
    PARAMETER_CHECK(*handle == NULL, return 0;); /* buffer already allocated */

    *frame  = packet_tx_alloc(size);
    *handle = *frame;

    if (*frame) {
        return size;
    } else {
        return 0;
    }
}

void vtss_1588_packet_tx_free(void **handle)
{
    // Check parameters
    PARAMETER_CHECK(handle != NULL, return;);
    PARAMETER_CHECK(*handle != NULL, return;); /* no buffer allocated */
    packet_tx_free((u8 *)*handle);
    *handle = NULL;
}

/**
 * vtss_1588_tx_general - Transmit a general message.
 */
size_t vtss_1588_tx_general(u64 port_mask, u8 *frame, size_t size, vtss_ptp_tag_t *tag)
{
    packet_tx_props_t tx_props;

    T_RG_HEX(_I, frame, size);
    T_NG(_I, "Portmask " VPRI64x " , tx %zd bytes", port_mask, size);
    if (port_mask) {
        packet_tx_props_init(&tx_props);
        tx_props.packet_info.modid     = VTSS_MODULE_ID_PTP;
        tx_props.packet_info.frm       = frame;
        tx_props.packet_info.len       = size;
        tx_props.tx_info.dst_port_mask = port_mask;
        tx_props.tx_info.tag.vid       = tag->vid;
        tx_props.tx_info.tag.pcp       = tag->pcp;
        tx_props.tx_info.tag.tpid      = mesa_cap_packet_auto_tagging ? 0 : tag->tpid;

        T_IG(_I, "Tag tpid: %x , vid %d , pcp %d", tx_props.tx_info.tag.tpid, tx_props.tx_info.tag.vid, tx_props.tx_info.tag.pcp);

        update_udp_checksum(frame);
        if (packet_tx(&tx_props) == VTSS_RC_OK) {
            return size;
        } else {
            T_EG(_I,"Transmit general on non-port (" VPRI64x ")?", port_mask);
        }
    }
    return 0;
}

/**
 * Send a general or event PTP message.
 *
 * \param port_mask switch port mask.
 * \param ptp_buf_handle ptp transmit buffer handle
 *
 */

static void timestamp_cbx (void *context, u32 port_no, mesa_ts_timestamp_t *ts)
{
    T_DG(_I, "timestamp: port_no %d, ts %u", port_no, ts->ts);
    if (ts->ts_valid) {
        PTP_CORE_LOCK_SCOPE();
        
        ptp_tx_timestamp_context_t *h = (ptp_tx_timestamp_context_t *)context;
        if (h->cb_ts) {
            h->cb_ts(h->context, l2port_to_ptp_api(port_no), ts->ts);
        } else {
            T_WG(_I, "Missing callback: port_no %d", port_no);
        }
    } else {
        T_IG(_I, "Missed tx time: port_no %d", port_no);
    }
}

static mesa_rc allocate_phy_signaturex(u8 *ptp_frm, void *context, u64 port_mask)
{
    mesa_rc rc = VTSS_RC_OK;
    if (mesa_cap_phy_ts) {
        mesa_ts_timestamp_alloc_t alloc_parm;
        mesa_phy_ts_fifo_sig_t    ts_sig;
    
        ts_sig.sig_mask = my_sig_mask;
        ts_sig.msg_type = ptp_frm[PTP_MESSAGE_MESSAGE_TYPE_OFFSET] & 0xF;
        ts_sig.domain_num = ptp_frm[PTP_MESSAGE_DOMAIN_OFFSET];
        memcpy(ts_sig.src_port_identity, ptp_frm+PTP_MESSAGE_SOURCE_PORT_ID_OFFSET, 10);
        ts_sig.sequence_id = vtss_tod_unpack16(&ptp_frm[PTP_MESSAGE_SEQUENCE_ID_OFFSET]);
    
        alloc_parm.port_mask = port_mask;
        alloc_parm.context = context;
        alloc_parm.cb = timestamp_cbx;
        rc = vtss_module_man_tx_timestamp_sig_alloc(&alloc_parm, &ts_sig);
        T_DG(_I,"Timestamp Signature allocated: mask: %x, type %d, domain %d, seq: %d, rc = %d",
             ts_sig.sig_mask, ts_sig.msg_type, ts_sig.domain_num, ts_sig.sequence_id, rc);
    }
    return rc;
}

void vtss_1588_tx_handle_init(ptp_tx_buffer_handle_t *ptp_buf_handle)
{
    memset(ptp_buf_handle, 0, sizeof(ptp_tx_buffer_handle_t));
}

/**
 * vtss_1588_forw_one_step_event.
 */
typedef struct {
    u8 corr [8];
    u8 reserved[4];
} onestep_forw_t;

mesa_rc vtss_1588_afi(void ** afi, u64 port_mask,
                        ptp_tx_buffer_handle_t *ptp_buf_handle, i8 log_sync_interval, BOOL resume)
{
    if (mesa_cap_synce_ann_auto_transmit) {
        ptp_afi_conf_s* my_afi = (ptp_afi_conf_s*)*afi;
        ptp_afi_setup_t afi_conf;
        mesa_port_no_t port_no = VTSS_PORT_NO_NONE;
        afi_conf.log_sync_interval = log_sync_interval;
        mesa_rc rc = VTSS_RC_OK;
        afi_conf.switch_port = TRUE;
        port_iter_t       pit;
        port_iter_init_local(&pit);
        while (port_iter_getnext(&pit) && port_no == VTSS_PORT_NO_NONE) {
            if (port_mask & (1LL<<pit.iport)) {
                port_no = pit.iport;
                if (port_data[pit.iport].topo.ts_feature == VTSS_PTP_TS_PTS) {
                    afi_conf.switch_port = FALSE;
                }
            }
        }
        if (port_no != VTSS_PORT_NO_NONE) {
            if (resume) {
                if (my_afi == 0) {
                    PTP_RETURN(ptp_afi_alloc(&my_afi));
                }
                *afi = my_afi;
                T_IG(_I, "port_no %d, my_afi %p", port_no, my_afi);
                return my_afi->ptp_afi_sync_conf_start(port_no, ptp_buf_handle, &afi_conf);
            } else {
                if (my_afi != 0) {
                    rc = my_afi->ptp_afi_sync_conf_stop();
                    PTP_RC(ptp_afi_free(&my_afi));
                }
                *afi = my_afi;
                return rc;
            }
        } else {
            return VTSS_RC_ERROR;
        }
    } else {
        return VTSS_RC_ERROR;
    }
}

mesa_rc vtss_1588_afi_packet_tx(void ** afi, u32* cnt_delta)
{
    if (mesa_cap_synce_ann_auto_transmit) {
        ptp_afi_conf_s* my_afi = (ptp_afi_conf_s*)*afi;
        PTP_RC(my_afi->ptp_afi_packet_tx(cnt_delta));
        return VTSS_RC_OK;
    } else {
        *cnt_delta = 0;
        return VTSS_RC_ERROR;
    }
}

bool vtss_1588_check_transmit_resources(int instance)
{
    bool available = true;
    if (mesa_cap_synce_ann_auto_transmit) {
        u32 afi_request = 0;

        if (config_data.conf[instance].clock_init.afi_announce_enable) {
            afi_request++;
        }
        if (config_data.conf[instance].clock_init.afi_sync_enable) {
            afi_request++;
        }

        PTP_RC(ptp_afi_available(afi_request, &available));
        T_IG(_I, "instance %d, afi_request %d, available %d", instance, afi_request, available);
    }

    return available;
}

size_t vtss_1588_tx_msg(u64 port_mask,
                        ptp_tx_buffer_handle_t *ptp_buf_handle, int instance)
{
    packet_tx_props_t tx_props;
    mesa_rc           rc = VTSS_RC_OK;
    u64               pts_port_mask = 0;
    int               portnum = 0, last_port = 0, port_count = 0;
    u8                *my_buf = (u8 *)ptp_buf_handle->frame + ptp_buf_handle->header_length;
    u8                message_type = my_buf[PTP_MESSAGE_MESSAGE_TYPE_OFFSET] & 0xF;

    T_RG_HEX(_I, ptp_buf_handle->frame, ptp_buf_handle->size);
    T_IG(_I, "Portmask " VPRI64x " , tx %zd bytes", port_mask, ptp_buf_handle->size);
    if (port_mask) {
        packet_tx_props_init(&tx_props);
        tx_props.packet_info.modid     = VTSS_MODULE_ID_PTP;
        tx_props.packet_info.frm       = ptp_buf_handle->frame;
        tx_props.packet_info.len       = ptp_buf_handle->size;
        tx_props.tx_info.dst_port_mask = port_mask;
        tx_props.packet_info.no_free   = ptp_buf_handle->handle != NULL;
        tx_props.tx_info.tag.vid       = ptp_buf_handle->tag.vid;
        tx_props.tx_info.tag.pcp       = ptp_buf_handle->tag.pcp;
        tx_props.tx_info.pdu_offset    = ptp_buf_handle->header_length;  /* Jaguar 2 needs to know the PTP header offset within the packet */
        tx_props.tx_info.tag.tpid      = mesa_cap_packet_auto_tagging ? 0 : ptp_buf_handle->tag.tpid; /* In Serval or Jaguar2: must be 0 otherwise the FDMA inserts a tag, and the so do the Switch */

        T_IG(_I, "Tag tpid: %x , vid %d , pcp %d, offset %u", tx_props.tx_info.tag.tpid, tx_props.tx_info.tag.vid, tx_props.tx_info.tag.pcp, tx_props.tx_info.pdu_offset);

        /* split port mask into PHY TS ports and non PHY TS ports */
        port_iter_t       pit;
        port_iter_init_local(&pit);
        while (port_iter_getnext(&pit)) {
            portnum = pit.iport;
            if (port_mask & (1LL<<portnum)) {
                port_count++;
                last_port = portnum;
                if (port_data[portnum].topo.ts_feature == VTSS_PTP_TS_PTS) {
                    pts_port_mask |= 1LL<<portnum;
                    port_mask &= ~(1LL<<portnum);
                }
            }
        }
        switch (ptp_buf_handle->msg_type) {
            case VTSS_PTP_MSG_TYPE_GENERAL:
                T_IG(_I, "general packet");
                break;

            case VTSS_PTP_MSG_TYPE_2_STEP:
                T_IG(_I, "2-step packet");

                if (pts_port_mask) {
                    /* packet transmitted to PTS ports */
                    rc = allocate_phy_signaturex(ptp_buf_handle->frame + ptp_buf_handle->header_length, ptp_buf_handle->ts_done, pts_port_mask);
                }
                if (port_mask) {
                    if (mesa_cap_ts) {
                        mesa_ts_timestamp_alloc_t alloc_parm;
                        alloc_parm.port_mask = port_mask;
                        alloc_parm.context = ptp_buf_handle->ts_done;
                        alloc_parm.cb = timestamp_cbx;
                        mesa_ts_id_t ts_id;
                        rc = mesa_tx_timestamp_idx_alloc(0, &alloc_parm, &ts_id); /* allocate id for transmission*/
                        T_DG(_I,"Timestamp Id (%u)allocated", ts_id.ts_id);
                        tx_props.tx_info.ptp_action    = MESA_PACKET_PTP_ACTION_TWO_STEP; /* twostep action */
                        tx_props.tx_info.ptp_id        = ts_id.ts_id;
                        tx_props.tx_info.ptp_timestamp = 0;
                    }
                }
                break;

            case VTSS_PTP_MSG_TYPE_CORR_FIELD:
                if (!pts_port_mask) {
                    /* link asymmetry compensation for DelayReq and PDelayReq is done in SW for switch ports */
                    portnum = 0;
                    while ((port_mask & (1LL<<portnum)) == 0 && portnum < mesa_cap_port_cnt) {
                        portnum++;
                    }
                    if (portnum >= mesa_cap_port_cnt)  {
                        portnum = mesa_cap_port_cnt - 1;
                        T_WG(_I, "invalid portnum");
                    }
                    if ((message_type == PTP_MESSAGE_TYPE_DELAY_REQ || message_type == PTP_MESSAGE_TYPE_P_DELAY_REQ) &&
                    port_data[portnum].asymmetry_cnt != 0) {
                        vtss_1588_ts_cnt_add(&ptp_buf_handle->hw_time, ptp_buf_handle->hw_time, port_data[portnum].asymmetry_cnt);
                    }
                }
                T_DG(_I, "one step packet, hw_time %u", ptp_buf_handle->hw_time);
                tx_props.tx_info.ptp_action         = MESA_PACKET_PTP_ACTION_ONE_STEP; /* onestep action */
                tx_props.tx_info.ptp_timestamp      = ptp_buf_handle->hw_time; /* used for correction field update */
                break;

            case VTSS_PTP_MSG_TYPE_ORG_TIME:
                if (mesa_cap_ts_org_time) {
                    tx_props.tx_info.ptp_action = MESA_PACKET_PTP_ACTION_ORIGIN_TIMESTAMP;  /* origin PTP action */
                    T_IG(_I, "HW sets preciseOriginTimestamp in the packet");
                } else {
                    T_EG(_I, "org_time is not supported");
                    rc = VTSS_RC_ERROR;
                }
                break;

            default:
                T_EG(_I, "invalid message type");
                break;
        }

        if (rc == VTSS_RC_OK) {
            update_udp_checksum(ptp_buf_handle->frame);
            T_IG(_I,"packet tx, ptp_action = %d",tx_props.tx_info.ptp_action);
            T_DG(_I,"port_count %d, last port is %d", port_count, last_port);

            if (packet_tx(&tx_props) == VTSS_RC_OK) {
                // Check to see if there is a timestamp ready for us - if needed.
                if (ptp_buf_handle->msg_type == VTSS_PTP_MSG_TYPE_2_STEP && mesa_cap_ts_missing_tx_interrupt) {
                    // Two-step transmission w/o timestamp interrupt, so do the
                    // timestamp update here.
                    T_DG(_I,"update timestamps");
                    PTP_CORE_UNLOCK();  // Temporarily unlock PTP core - locking again below.
                    rc = mesa_tx_timestamp_update(nullptr);
                    PTP_CORE_LOCK();
                    PTP_RC(rc);
                }

                return ptp_buf_handle->size;
            } else {
                T_EG(_I,"Transmit message on non-port (" VPRI64x ")?", port_mask);
            }
        } else {
            T_WG(_I,"Could not get a timestamp ID");
        }
    }

    return 0;
}

static int ptp_sock = 0;

static void ptp_socket_init(void)
{
    struct sockaddr_in sender;
    u16 port = PTP_GENERAL_PORT;
    int length, retry = 0;

    if (ptp_sock == 0)  {
        // this command tells the IP stack to drop PTP over IP messages. This is done to avoid ICMP 'unreachable' messages.
        // this operation takes more than 100 ms, therefore it shall only be done once
        system("iptables -A INPUT -p udp -m multiport --dports 319,320 -j DROP");
    }
    if (ptp_sock > 0) close(ptp_sock);

    ptp_sock = vtss_socket(AF_INET, SOCK_DGRAM, 0);
    if (ptp_sock < 0) {
        T_EG(_I, "socket returned %d",ptp_sock);
        return;
    }
    length = sizeof(sender);
    bzero(&sender,length);
    sender.sin_family=AF_INET;
    sender.sin_addr.s_addr=INADDR_ANY;
    vtss_tod_pack16(port,(u8*)&sender.sin_port);
    T_IG(_I, "binding socket");
    while (bind(ptp_sock, (struct sockaddr *)&sender, length) < 0 ) {
        if (++retry > 30) {
            T_EG(_I, "binding error, err_no %d",errno);
            break;
        }
        T_IG(_I, "binding problem, err_no %d",errno);
        VTSS_OS_MSLEEP(1000); /* 1 sec */
    }
}

static void ptp_socket_close(void)
{
    if (ptp_sock > 0) {
        if (close(ptp_sock) == 0) ptp_sock = 0;
    }
}

size_t vtss_1588_tx_unicast_request(u32 dest_ip, const void *buffer, size_t size, int instance)
{
    struct sockaddr_in receiver;
    char buf1[20];
    int n;
    int length;
    struct sockaddr_in my_addr;
    socklen_t my_addr_len;
    u16 port = PTP_GENERAL_PORT;

    if (ptp_global.my_ip[instance].s_addr == 0) {
        T_IG(_I,"cannot send before my ip address is defined");
        return 0;
    }

    T_IG(_I, "Dest_ip %s, size " VPRIz, misc_ipv4_txt(dest_ip, buf1), size);
    //packet_dump(buffer, size);

    memset(&receiver, 0, sizeof(struct sockaddr_in));
    receiver.sin_family = AF_INET;
    vtss_tod_pack32(dest_ip,(u8*)&receiver.sin_addr);
    vtss_tod_pack16(port,(u8*)&receiver.sin_port);
    length = sizeof(struct sockaddr_in);

    /* Connect socket */
    if (connect(ptp_sock, (struct sockaddr *)&receiver, length) != 0) {
        T_IG(_I, "Failed to connect: %d - %s", errno, strerror(errno));
        return 0;
    }

    /* Get my address */
    my_addr_len = sizeof(my_addr);
    if (getsockname(ptp_sock, (struct sockaddr *)&my_addr, &my_addr_len) != 0) {
        T_WG(_I, "getsockname failed: %d - %s", errno, strerror(errno));
        return 0;
    }
    if (my_addr.sin_family != AF_INET) {
        T_WG(_I, "Got something else than as an IPv4 address: %d", my_addr.sin_family);
        return 0;
    }
    T_IG(_I, "Source_ip %s, size " VPRIz, misc_ipv4_txt(my_addr.sin_addr.s_addr, buf1), size);

    n = write(ptp_sock,buffer,size);
    if (n < 0) {
        T_IG(_I, "write returned %d", n);
        if (errno == EHOSTUNREACH) {
            T_I("Error no: %d, error message: %s", errno, strerror(errno));
        } else {
            T_W("Error no: %d, error message: %s", errno, strerror(errno));
        }
        return 0;
    }
    return n;
}

/****************************************************************************/
/*  Reserved ACEs functions and MAC table setup                             */
/****************************************************************************/
/* If all ports have 1588 PHY, no ACL rules are needed for timestamping */
/* instead the forwarding is set up in the MAC table */
static bool ptp_external_phys_get()
{
    bool ext_phy = true;
    int i, j;
    port_iter_t       pit;
    T_IG(VTSS_TRACE_GRP_ACE,"ext_phy before %d", ptp_all_external_port_phy_ts);
    for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
        if (config_data.conf[i].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            /* check if PHY timetsamper esists */
            port_iter_init_local(&pit);
            while (port_iter_getnext(&pit)) {
                j = pit.iport;
                if (config_data.conf[i].port_config[j].enabled && port_data[j].topo.ts_feature != VTSS_PTP_TS_PTS &&
                    (!config_data.conf[i].port_config[j].portInternal || mesa_cap_ts_c_dtc_supported))
                {
                    ext_phy = false;
                    T_DG(VTSS_TRACE_GRP_ACE,"port %d, enabled %d, internal %d, ts_feature %d", pit.uport, 
                         config_data.conf[i].port_config[j].enabled, 
                         config_data.conf[i].port_config[j].portInternal,
                         port_data[j].topo.ts_feature);
                }
            }
        }
    }
    T_IG(VTSS_TRACE_GRP_ACE,"ext_phy %d", ext_phy);
    return ext_phy;
}




mesa_ace_type_t conf_type(int protocol)
{
    if ((protocol == VTSS_APPL_PTP_PROTOCOL_IP4MULTI) || (protocol == VTSS_APPL_PTP_PROTOCOL_IP4MIXED) || (protocol == VTSS_APPL_PTP_PROTOCOL_IP4UNI))
        return MESA_ACE_TYPE_IPV4;

    return MESA_ACE_TYPE_ETYPE;
}

static void acl_rules_init(void)
{
    int i,j;
    for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
        rules[i].domain = 0;
        rules[i].deviceType = VTSS_APPL_PTP_DEVICE_NONE;
        rules[i].protocol = VTSS_APPL_PTP_PROTOCOL_ETHERNET;
        for (j = 0; j < vtss_appl_cap_max_acl_rules_pr_ptp_clock; j++) {
            rules[i].id[j] = ACL_MGMT_ACE_ID_NONE;
        }
    }
}

/*
 * ACL rule_id's are allocated this way:
 * if no TC instance exists, then the rules are inserted as last entries
 * if a tc exists, then the rules are inserted before the TC's rules.
 * the first TC rule id is saved in my_next_id.
 * This is because the BC's are more specific than the TC rules.
 * If a TC exists, that forwards all packets in HW, then my_tc_encapsulation is set to the TC encapsulation type
 * If this type of TC exists, and a SlaveOnly instance also exists, this slave is used for syntonization of the TC,
 * and the packets forwarded to the CPU must also be forwarded, i.e. the ACL rules for this slaveOnly instance depends
 * on the TC configuration. (my_tc_encapsulation is defined and described earlier in this file)
 */
static mesa_ace_id_t my_next_id = ACL_MGMT_ACE_ID_NONE;                  /* indicates that no TC exists */

static mesa_rc acl_rule_apply(acl_entry_conf_t    *conf, int instance, int rule_id)
{
    if (rule_id >= vtss_appl_cap_max_acl_rules_pr_ptp_clock) {
        T_WG(VTSS_TRACE_GRP_ACE,"too many ACL rules");
        return VTSS_RC_ERROR;
    }
    conf->id = rules[instance].id[rule_id];/* if id == ACL_MGMT_ACE_ID_NONE, it is auto assigned, otherwise it is reused */
    T_DG(VTSS_TRACE_GRP_ACE,"acl_mgmt_ace_add instance: %d, rule idx %d, conf->id %d, my_next_id %d", instance, rule_id, conf->id, my_next_id);
    PTP_RETURN(acl_mgmt_ace_add(ACL_USER_PTP, my_next_id, conf));
    if (conf->conflict) {
        T_WG(VTSS_TRACE_GRP_ACE,"acl_mgmt_ace_add conflict instance %d, rule %d", instance, rule_id);
        return PTP_RC_MISSING_ACL_RESOURCES;
    } else {
        rules[instance].id[rule_id] = conf->id;
    }
    T_DG(VTSS_TRACE_GRP_ACE,"ACL rule_id %d, conf->id %d", rule_id, conf->id);
    return VTSS_RC_OK;
}

static mesa_rc ptp_auto_resp_setup(int i, acl_entry_conf_t *pconf, int *rule_no) {
    port_iter_t       pit;
    int j;
    mesa_ace_ptp_t *pptp;

    // in Jr2 the DelayReq/DelayResp is handled in HW
    // set up the clockId for the domain
    acl_entry_conf_t my_conf = *pconf;      //make my own copy og the configuration before modifying it
    mesa_ts_autoresp_dom_cfg_t autoresp_cfg;
    autoresp_cfg.ptp_port_individual = TRUE;
    autoresp_cfg.ptp_port_msb = 7;       /* to be fixed */
    memcpy(autoresp_cfg.clock_identity, config_data.conf[i].clock_init.clockIdentity, VTSS_CLOCK_IDENTITY_LENGTH);
    autoresp_cfg.flag_field_update.value = 0x00;  /*clear flags */
    autoresp_cfg.flag_field_update.mask = 0x0b;  /*except unicast flag which is kept unchanged */
    PTP_RC(mesa_ts_autoresp_dom_cfg_set(0, config_data.conf[i].clock_init.cfg.clock_domain, &autoresp_cfg));

    mesa_port_list_t rsp_port_list;

    if (my_conf.type == MESA_ACE_TYPE_ETYPE) {
        pptp = &my_conf.frame.etype.ptp;
    } else if (my_conf.type == MESA_ACE_TYPE_IPV4) {
        pptp = &my_conf.frame.ipv4.ptp;
    } else {
        T_WG(VTSS_TRACE_GRP_ACE,"unsupported ACL frame type %d", my_conf.type);
        return PTP_RC_UNSUPPORTED_ACL_FRAME_TYPE;
    }
    // set up Source IP address used in auto response
    mesa_acl_sip_conf_t sip;
    sip.sip.type = MESA_IP_TYPE_IPV4;
    sip.sip.addr.ipv4 = ptp_global.my_ip[i].s_addr;

    PTP_RC(mesa_acl_sip_conf_set(0, i, &sip));
    port_iter_init_local(&pit);
    while (port_iter_getnext(&pit)) {
        j = pit.iport;
        rsp_port_list[j] = false;
    }
    port_iter_init_local(&pit);
    while (port_iter_getnext(&pit)) {
        j = pit.iport;
        if (rules[i].port_list[j]) {
            rsp_port_list[j] = true;
            my_conf.action.force_cpu = false;
            my_conf.action.port_action = MESA_ACL_PORT_ACTION_REDIR;
            my_conf.action.ptp_action = MESA_ACL_PTP_ACTION_ONE_STEP;
            if (port_data[j].topo.ts_feature == VTSS_PTP_TS_PTS) {
                my_conf.action.ptp.response = MESA_ACL_PTP_RSP_DLY_REQ_RSP_NO_TS;
                my_conf.action.ptp_action = MESA_ACL_PTP_ACTION_NONE;
            } else {
                my_conf.action.ptp.response = MESA_ACL_PTP_RSP_DLY_REQ_RSP_TS_UPD;
            }
            my_conf.action.ptp.log_message_interval = config_data.conf[i].port_config[j].logMinPdelayReqInterval; // message interval returned in the dely_resp
            my_conf.action.ptp.copy_smac_to_dmac = 0;  //SMAC/DMAC handling is controlled by my_conf.action.addr.update
            my_conf.action.ptp.set_smac_to_port_mac = 0;
            my_conf.action.ptp.dom_sel = config_data.conf[i].clock_init.cfg.clock_domain;   // HW domain
            my_conf.action.addr.sip_idx = i;
            my_conf.action.ptp.sport = 320;
            my_conf.action.ptp.dport = 320;
            my_conf.action.addr.update = MESA_ACL_ADDR_UPDATE_MAC_IP_SWAP_UC;   //TBD
            memcpy(&my_conf.action.addr.mac, &ptp_global.sysmac, sizeof(mesa_mac_t));
            pptp->header.mask[0] = 0x0f;
            pptp->header.value[0] = 0x01; /* messageType = [1] */
            my_conf.port_list = rsp_port_list;
            my_conf.action.port_list = rsp_port_list;
            my_conf.vid.value = config_data.conf[i].clock_init.cfg.configured_vid;
            my_conf.vid.mask = 0xfff;

            rsp_port_list[j] = false;
            T_IG(VTSS_TRACE_GRP_ACE,"inst %d: autoresponse applied for port %d, vid %d", i, j, my_conf.vid.value);
            PTP_RETURN(acl_rule_apply(&my_conf, i, (*rule_no)++));
        }
    }
    return VTSS_RC_OK;
}

static mesa_rc _ptp_ace_update(int i, CapArray<bool, VTSS_APPL_CAP_PTP_CLOCK_CNT>& ptp_inst_updated)
{
    mesa_rc             rc = VTSS_RC_OK;
    acl_entry_conf_t    conf;
    int rule_no = ACL_MGMT_ACE_ID_NONE;
    port_iter_t       pit;
    int portlist_size;
    mesa_ace_ptp_t *pptp = 0;
    u32 mac_idx;
    u32 inst, idx;
    char dest_txt[100];
    int j;
    CapArray<bool, VTSS_APPL_CAP_PTP_CLOCK_CNT> refresh_inst;  // Note: All elements initialize to 0 i.e. FALSE by constructor

    // Check if PTP instance has already been updated. If this is the case simply return with VTSS_RC_OK
    if (ptp_inst_updated[i]) return VTSS_RC_OK;
    ptp_inst_updated[i] = true;  // Make sure this PTP instance will only be updated once.

    if ((config_data.conf[i].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY ||
         config_data.conf[i].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY) &&
         config_data.conf[i].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM) {
        return rc;
    }
    if ((config_data.conf[i].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY) &&
            config_data.conf[i].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_ONE_PPS) {
        return rc;
    }
    // If the clock is a boundary clock then:  ptp_all_external_port_phy_ts is set to true if all external PTP ports have timestamp PHY and none of them are using "two step mode".
    // else:                                   ptp_all_external_port_phy_ts is set to true if all external PTP ports have timestamp PHY and clock is not configured to use "two step mode" (ignoring setting at port level).
    if ((rules[i].deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND) || (rules[i].deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY)) {
        bool a_port_uses_two_step = false;
        port_iter_init_local(&pit);
        while (port_iter_getnext(&pit)) {
            if (config_data.conf[i].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE &&
                ((config_data.conf[i].clock_init.cfg.twoStepFlag && !(config_data.conf[i].port_config[pit.iport].twoStepOverride == VTSS_APPL_PTP_TWO_STEP_OVERRIDE_FALSE)) ||
                (config_data.conf[i].port_config[pit.iport].twoStepOverride == VTSS_APPL_PTP_TWO_STEP_OVERRIDE_TRUE)))
            {
                a_port_uses_two_step = true;
            }
        }
        ptp_all_external_port_phy_ts = ptp_external_phys_get() && !a_port_uses_two_step;
    }
    else {
        ptp_all_external_port_phy_ts = ptp_external_phys_get() && !config_data.conf[i].clock_init.cfg.twoStepFlag;
    }
    port_iter_init_local(&pit);
    while (port_iter_getnext(&pit)) {
        PTP_RETURN(ptp_phy_ts_update(i, pit.iport));
    }
    if (rules[i].deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
        /* Remove rules for this clock instance */
        /* If All ports have 1588 PHY, forwarding is set up in the MAC table */
        for (mac_idx = 0; mac_idx < PTP_MAC_ENTRIES; mac_idx++) {
            if (mac_idx < mac_used) {
                PTP_RC(mesa_mac_table_del(NULL,&mac_entry[mac_idx].vid_mac));
                T_IG(VTSS_TRACE_GRP_ACE,"deleting mac table entry: mac %s, vid %d, copy to cpu %d", misc_mac2str(mac_entry[mac_idx].vid_mac.mac.addr),
                    mac_entry[mac_idx].vid_mac.vid, mac_entry[mac_idx].copy_to_cpu);
            }
            memset(&mac_entry[mac_idx], 0, sizeof(mesa_mac_table_entry_t));
        }
        if (mac_used > 0 && !ptp_all_external_port_phy_ts) {
            // mode is changed from mac table to ACL table, therefore other instances must be refreshed
            for (int ii = 0; ii < PTP_CLOCK_INSTANCES; ii++) {
                if (i != ii && rules[ii].deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
                    refresh_inst[ii] = true;
                    T_IG(VTSS_TRACE_GRP_ACE,"deleting mac table entry, therefore refresh instance %d", ii);
                }
            }
        }
        mac_used = 0;
        if (rules[i].deviceType == VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT || rules[i].deviceType == VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT) {
            my_next_id = ACL_MGMT_ACE_ID_NONE;
            if (i == my_tc_instance || i == my_2step_tc_instance) {
                for (int ii = 0; ii < PTP_CLOCK_INSTANCES; ii++) {
                    if (i != ii && protocol_level(rules[i].protocol) == protocol_level(rules[ii].protocol)) {
                        refresh_inst[ii] = true;
                    }
                }
            }
            my_tc_encapsulation = VTSS_APPL_PTP_PROTOCOL_MAX_TYPE;   /* indicates that no forwarding in the SlaveOnly instance is needed */
            my_tc_instance = -1;
            my_2step_tc_instance = -1;
        }
        rules[i].deviceType = VTSS_APPL_PTP_DEVICE_NONE;
        for (j = 0; j < vtss_appl_cap_max_acl_rules_pr_ptp_clock; j++) {
            if (rules[i].id[j] != ACL_MGMT_ACE_ID_NONE) {
                PTP_RC(acl_mgmt_ace_del(ACL_USER_PTP, rules[i].id[j]));
                rules[i].id[j] = ACL_MGMT_ACE_ID_NONE;
            }
            T_IG(VTSS_TRACE_GRP_ACE,"clear ACL rules for inst %d", i);
        }
    }


    if (config_data.conf[i].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
        /* Set up rules for this instance */
        rules[i].deviceType = config_data.conf[i].clock_init.cfg.deviceType;
        rules[i].protocol = config_data.conf[i].clock_init.cfg.protocol;
        rules[i].domain = config_data.conf[i].clock_init.cfg.domainNumber;
        if (rules[i].protocol == VTSS_APPL_PTP_PROTOCOL_IP4UNI && (ptp_global.my_ip[i].s_addr == 0)) {
            T_IG(VTSS_TRACE_GRP_ACE,"cannot set up unicast ACL before my ip address is defined");
            return PTP_RC_MISSING_IP_ADDRESS;
        }
        rules[i].internal_port_exists = 0;
        port_iter_init_local(&pit);
        while (port_iter_getnext(&pit)) {
            j = pit.iport;
            rules[i].port_list[j] = config_data.conf[i].port_config[j].enabled;
            rules[i].external_port_list[j] = config_data.conf[i].port_config[j].enabled && !config_data.conf[i].port_config[j].portInternal;
            rules[i].internal_port_list[j] = config_data.conf[i].port_config[j].portInternal;
            rules[i].internal_port_exists |= config_data.conf[i].port_config[j].portInternal;
            T_DG(VTSS_TRACE_GRP_ACE,"inst %d, port %d, list %d, xlist %d, ilist %d", i, j, rules[i].port_list.get(j), rules[i].external_port_list.get(j), rules[i].internal_port_list.get(j));
        }
        T_DG(VTSS_TRACE_GRP_ACE,"inst %d, internal port exists %d", i, rules[i].internal_port_exists);

        T_IG(_I, "ace_init protocol: %d", rules[i].protocol);
        if ((rc = acl_mgmt_ace_init(conf_type(rules[i].protocol), &conf)) != VTSS_OK) {
            return rc;
        }
        conf.action.cpu_queue = PACKET_XTR_QU_BPDU;  /* increase extraction priority to the same as bpdu packets */
        VTSS_BF_SET(conf.flags.value, ACE_FLAG_IP_FRAGMENT, 0);  /* ignore IPV4 fragmented packets */
        VTSS_BF_SET(conf.flags.mask, ACE_FLAG_IP_FRAGMENT, 1);
        conf.action.port_action = MESA_ACL_PORT_ACTION_FILTER;
        memset(conf.action.port_list, 0, sizeof(conf.action.port_list));
        conf.action.logging = false;
        conf.action.shutdown = false;
        conf.action.cpu_once = false;
        conf.isid = VTSS_ISID_LOCAL;
        if (conf.type == MESA_ACE_TYPE_ETYPE) {
            conf.frame.etype.etype.value[0] = (ptp_ether_type>>8) & 0xff;
            conf.frame.etype.etype.value[1] = ptp_ether_type & 0xff;
            conf.frame.etype.etype.mask[0] = 0xff;
            conf.frame.etype.etype.mask[1] = 0xff;
        } else if (conf.type == MESA_ACE_TYPE_IPV4) {
            conf.frame.ipv4.proto.value = 17; //UDP
            conf.frame.ipv4.proto.mask = 0xFF;
            conf.frame.ipv4.sport.in_range = conf.frame.ipv4.dport.in_range = true;
            conf.frame.ipv4.sport.low = 0;
            conf.frame.ipv4.sport.high = 65535;
            conf.frame.ipv4.dport.low = 319;
            conf.frame.ipv4.dport.high = 320;
            if ((VTSS_APPL_PTP_DEVICE_ORD_BOUND == rules[i].deviceType || VTSS_APPL_PTP_DEVICE_MASTER_ONLY  == rules[i].deviceType ||
                    VTSS_APPL_PTP_DEVICE_SLAVE_ONLY == rules[i].deviceType) && (rules[i].protocol == VTSS_APPL_PTP_PROTOCOL_IP4UNI)) {
                /* in unicast mode only terminate PTP messages with my address */
                conf.frame.ipv4.dip.value = ptp_global.my_ip[i].s_addr;
                conf.frame.ipv4.dip.mask = 0xffffffff;
            }
        }

        if (!ptp_all_external_port_phy_ts) {
#if (PTP_CLOCK_PORTS != VTSS_PORT_ARRAY_SIZE)
            T_WG(VTSS_TRACE_GRP_ACE,"inconsistenc port array sizes %d, %d", sizeof(conf.port_list), sizeof(rules[i].port_list));
#endif
            portlist_size = sizeof(conf.port_list);
            memcpy(conf.port_list,  rules[i].port_list, portlist_size);
            conf.action.port_action = MESA_ACL_PORT_ACTION_FILTER;
            if (conf.type == MESA_ACE_TYPE_ETYPE) {
                pptp = &conf.frame.etype.ptp;
            } else if (conf.type == MESA_ACE_TYPE_IPV4) {
                pptp = &conf.frame.ipv4.ptp;
            } else {
                T_WG(VTSS_TRACE_GRP_ACE,"unsupported ACL frame type %d", conf.type);
                return PTP_RC_UNSUPPORTED_ACL_FRAME_TYPE;
            }

            switch (rules[i].deviceType) {
                case VTSS_APPL_PTP_DEVICE_ORD_BOUND:
                case VTSS_APPL_PTP_DEVICE_MASTER_ONLY:
                case VTSS_APPL_PTP_DEVICE_SLAVE_ONLY:
                    memset(conf.action.port_list, 0, sizeof(conf.action.port_list));
                    conf.action.ptp_action = MESA_ACL_PTP_ACTION_NONE;
                    conf.action.force_cpu = true;
                    pptp->enable = true;
                    pptp->header.mask[0] = 0x0f;
                    pptp->header.value[0] = 0x0a; /* messageType = 10 Pdelay_Resp_Follow_Up*/
                    pptp->header.mask[1] = 0x0f;
                    pptp->header.value[1] = 0x02; /* versionPTP = 2 */
                    pptp->header.mask[2] = 0xff;
                    pptp->header.value[2] = rules[i].domain; /* domainNumber */
                    pptp->header.mask[3] = 0x00;
                    pptp->header.value[3] = 0x00; /* flagField[0] = don't care */
                    if (rules[i].protocol == VTSS_APPL_PTP_PROTOCOL_IP4UNI) {
                        /* always terminate PTP peer delay messages */
                        conf.frame.ipv4.dip.mask = 0x0;
                    }
                    /* ACL rule for Pdelay_Resp_Follow_Up (never forwarded) */
                    T_DG(VTSS_TRACE_GRP_ACE,"apply rule %d, domain %d", rule_no, rules[i].domain);
                    PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                    if (protocol_level(my_tc_encapsulation) == protocol_level(rules[i].protocol) && my_tc_instance > 0  && my_tc_instance != i) {
                        /* the other packets for this BC device are also forwarded to the TC ports */
                        /* only if the TC is a 1-step */
                        memcpy(conf.action.port_list,  rules[my_tc_instance].port_list, portlist_size);
                        T_DG(VTSS_TRACE_GRP_ACE,"inst %d: packets are forwarded to instance %d TC ports", i, my_tc_instance);
                    }
                    if (rules[i].protocol == VTSS_APPL_PTP_PROTOCOL_IP4UNI) {
                        /* in unicast mode only terminate PTP messages with my address */
                        conf.frame.ipv4.dip.mask = 0xffffffff;
                    }
                    conf.action.ptp_action = MESA_ACL_PTP_ACTION_NONE;
                    conf.action.force_cpu = true;
                    pptp->enable = true;
                    pptp->header.mask[0] = 0x0e;
                    pptp->header.value[0] = 0x08; /* messageType = [8..9]  */
                    pptp->header.mask[1] = 0x0f;
                    pptp->header.value[1] = 0x02; /* versionPTP = 2 */
                    pptp->header.mask[2] = 0xff;
                    pptp->header.value[2] = rules[i].domain; /* domainNumber */
                    pptp->header.mask[3] = 0x00;
                    pptp->header.value[3] = 0x00; /* flagField[0] = don't care */

                    /* ACL rule for Follow_up, DelayResp */
                    T_DG(VTSS_TRACE_GRP_ACE,"apply rule %d, domain %d", rule_no, rules[i].domain);
                    PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));

                    if (protocol_level(my_tc_encapsulation) == protocol_level(rules[i].protocol) && my_2step_tc_instance > 0  && my_2step_tc_instance != i) {
                        /* the other packets for this BC device are also forwarded to the TC ports */
                        /* also if the TC is a 2-step */
                        memcpy(conf.action.port_list,  rules[my_2step_tc_instance].port_list, portlist_size);
                        T_DG(VTSS_TRACE_GRP_ACE,"inst %d: packets are forwarded to instance %d TC ports", i, my_2step_tc_instance);
                    }
                    conf.action.ptp_action = MESA_ACL_PTP_ACTION_NONE;
                    conf.action.force_cpu = true;
                    pptp->enable = true;
                    pptp->header.mask[0] = 0x08;
                    pptp->header.value[0] = 0x08; /* messageType = [8..15] except 8,9,10, which are hit by the rules above */
                    pptp->header.mask[1] = 0x0f;
                    pptp->header.value[1] = 0x02; /* versionPTP = 2 */
                    pptp->header.mask[2] = 0xff;
                    pptp->header.value[2] = rules[i].domain; /* domainNumber */
                    pptp->header.mask[3] = 0x00;
                    pptp->header.value[3] = 0x00; /* flagField[0] = don't care */

                    /* ACL rule for Announce, Signalling and Management */
                    T_DG(VTSS_TRACE_GRP_ACE,"apply rule %d, domain %d", rule_no, rules[i].domain);
                    PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));

                    if (protocol_level(my_tc_encapsulation) == protocol_level(rules[i].protocol) && my_tc_instance > 0  && my_tc_instance != i) {
                        /* the other packets for this BC device are also forwarded to the TC ports */
                        /* only if the TC is a 1-step */
                        memcpy(conf.action.port_list,  rules[my_tc_instance].port_list, portlist_size);
                        T_DG(VTSS_TRACE_GRP_ACE,"inst %d: packets are forwarded to instance %d TC ports", i, my_tc_instance);
                    } else {
                        memset(conf.action.port_list, 0, sizeof(conf.action.port_list));
                    }

                    /* ACL rule for Sync events */
                    if (mesa_cap_ts_p2p_delay_comp && mesa_cap_ts_asymmetry_comp) {
                        conf.action.ptp_action = MESA_ACL_PTP_ACTION_ONE_STEP_SUB_DELAY_2; /* Asymmetry and p2p delay compensation */
                        pptp->header.mask[0] = 0x0f;
                        pptp->header.value[0] = 0x00; /* messageType = [0] */
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                    } else if (mesa_cap_ts_twostep_always_required) {
                        conf.action.ptp_action = MESA_ACL_PTP_ACTION_TWO_STEP; /* always twostep action on received packets */
                        if (protocol_level(my_tc_encapsulation) == protocol_level(rules[i].protocol) && my_tc_instance > 0  && my_tc_instance != i) {
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_ONE_AND_TWO_STEP; /* always twostep action on received packets, */
                                                                                           /* and also do 1-step because the packet is 'TC' forwarded  */
                        }
                        pptp->header.mask[0] = 0x0f;
                        pptp->header.value[0] = 0x00; /* messageType = [0] */
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                    } else {
                        T_EG(VTSS_TRACE_GRP_ACE, "Not supported!");
                    }

                    /* ACL rule for DelayReq events */
                    if (mesa_cap_ts_twostep_always_required) {
                        conf.action.ptp_action = MESA_ACL_PTP_ACTION_TWO_STEP; /* always twostep action on received packets */
                        pptp->header.mask[0] = 0x0f;
                        pptp->header.value[0] = 0x01; /* messageType = [1]*/
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                    } else {
                        if (mesa_cap_ts_delay_req_auto_resp && rules[i].deviceType != VTSS_APPL_PTP_DEVICE_SLAVE_ONLY) {
                            if (config_data.conf[i].clock_init.cfg.clock_domain < mesa_cap_ts_domain_cnt) {
                                //Auto response is only supported in HW clock domains
                                PTP_RC(ptp_auto_resp_setup(i, &conf, &rule_no));
                            }
                        } else {
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_ONE_STEP; /* Rx timestamp no compensation */
                            pptp->header.mask[0] = 0x0f;
                            pptp->header.value[0] = 0x01; /* messageType = [1] */
                            PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                        }
                    }

                    /* ACL rule for Pdelayxxx events (never forwarded) */
                    if (rules[i].protocol == VTSS_APPL_PTP_PROTOCOL_IP4UNI) {
                        /* always terminate PTP peer delay messages */
                        conf.frame.ipv4.dip.mask = 0x0;
                    }
                    if (mesa_cap_ts_asymmetry_comp) {
                        /* ACL rule for PdelayReq events */
                        conf.action.force_cpu = true;
                        conf.action.ptp_action = MESA_ACL_PTP_ACTION_ONE_STEP; /* Rx timestamp no compensation */
                        pptp->header.mask[0] = 0x0f;
                        pptp->header.value[0] = 0x02; /* messageType = [2] */
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
    
                        memcpy(conf.port_list,  rules[i].port_list, portlist_size);
                        memset(conf.action.port_list, 0, sizeof(conf.action.port_list));
    
                        /* ACL rule for PdelayResp events */
                        conf.action.ptp_action = MESA_ACL_PTP_ACTION_ONE_STEP_SUB_DELAY_1; /* Asymmetry delay compensation */
                        pptp->header.mask[0] = 0x0f;
                        pptp->header.value[0] = 0x03; /* messageType = [3] */
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                    } else {
                        /* ACL rule for PdelayReq and for PdelayResp events */
                        memset(conf.action.port_list, 0, sizeof(conf.action.port_list));
                        conf.action.ptp_action = MESA_ACL_PTP_ACTION_TWO_STEP; /* always twostep action on received packets */
                        pptp->header.mask[0] = 0x0e;
                        pptp->header.value[0] = 0x02; /* messageType = [2..3]*/
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                    }
                    break;
                case VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT:
                    if (config_data.conf[i].clock_init.cfg.twoStepFlag) {
                        /* two-step E2E transparent clock */
                        /**********************************/
                        memset(conf.action.port_list, 0x00, sizeof(conf.action.port_list));
                        conf.action.ptp_action = MESA_ACL_PTP_ACTION_NONE;
                        conf.action.force_cpu = true;
                        pptp->enable = true;
                        pptp->header.mask[0] = 0x0e;
                        pptp->header.value[0] = 0x08; /* messageType = [8..9] */
                        pptp->header.mask[1] = 0x0f;
                        pptp->header.value[1] = 0x02; /* versionPTP = 2 */
                        pptp->header.mask[2] = 0x00;
                        pptp->header.value[2] = 0x00; /* domainNumber = d.c */
                        pptp->header.mask[3] = 0x00;
                        pptp->header.value[3] = 0x00; /* flagField[0] = don't care */

                        /* ACL rule for Follow_up, Delay_resp */
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));

                        /* ACL rule for Pdelay_resp_follow_up */
                        pptp->header.mask[0] = 0x0f;
                        pptp->header.value[0] = 0x0a; /* messageType = [10] */
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));

                        /* ACL rule for OneStep Sync Events */
                        if (mesa_cap_ts_twostep_always_required) {
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_TWO_STEP;
                        } else {
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_NONE;
                        }

                        pptp->header.mask[0] = 0x0f;
                        pptp->header.value[0] = 0x00; /* messageType = [0] */
                        pptp->header.mask[3] = 0x02;
                        pptp->header.value[3] = 0x00; /* flagField[0] = twostep = 0 */
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));

                        /* ACL rule for 2step Sync, Delay_req and Pdelay_xx */
                        /* forwarded, and 2-step action, i.e. a timestamp id is reserved, and transmit time is saved in fifo */
                        conf.action.ptp_action = MESA_ACL_PTP_ACTION_TWO_STEP;
                        pptp->header.mask[0] = 0x0c;
                        pptp->header.value[0] = 0x00; /* messageType = [0..3] */
                        pptp->header.mask[3] = 0x00;
                        pptp->header.value[3] = 0x00; /* flagField[0] = don't care */
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));

                        /* ACL rule for All other PTP messages (forward) */
                        memcpy(conf.action.port_list,  rules[i].port_list, portlist_size);
                        conf.action.force_cpu = false;   /* forwarded for TC. If needed for a BC, then the BC rules must hit first */
                        conf.action.ptp_action = MESA_ACL_PTP_ACTION_NONE;
                        pptp->header.mask[0] = 0x00;
                        pptp->header.value[0] = 0x00; /* messageType = d.c. */
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                    } else {
                        /* one-step E2E transparent clock */
                        /**********************************/
                        if (mesa_cap_ts_asymmetry_comp) {
                            memcpy(conf.port_list,  rules[i].port_list, portlist_size);
                            memcpy(conf.action.port_list,  rules[i].port_list, portlist_size);
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_ONE_STEP_SUB_DELAY_2;
                            conf.action.force_cpu = false;
                            pptp->enable = true;
                            pptp->header.mask[0] = 0x0f;
                            pptp->header.value[0] = 0x00; /* messageType = [0] */
                            pptp->header.mask[1] = 0x0f;
                            pptp->header.value[1] = 0x02; /* versionPTP = 2 */
                            pptp->header.mask[2] = 0x00;
                            pptp->header.value[2] = 0x00; /* domainNumber = d.c */
                            pptp->header.mask[3] = 0x00;
                            pptp->header.value[3] = 0x00; /* flagField[0] = don't care */
                            /* ACL rule for Sync Events:  */
                            PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
    
                            /* ACL rule for PDelayResp Events:  */
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_ONE_STEP_SUB_DELAY_1;
                            pptp->header.mask[0] = 0x0f;
                            pptp->header.value[0] = 0x03; /* messageType = [3] */
                            PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
    
                            /* ACL rule for DelayReq, PDelayReq Events:  */
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_ONE_STEP_ADD_DELAY;
                            pptp->header.mask[0] = 0x0c;
                            pptp->header.value[0] = 0x00; /* messageType = [1,2]  (because 0 and 3 are hit by the previous rules */
                            PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                        } else if (mesa_cap_ts_internal_ports_req_twostep) {
                            memcpy(conf.port_list,  rules[i].external_port_list, portlist_size);
#ifdef PTP_LOG_TRANSPARENT_ONESTEP_FORWARDING
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_TWO_STEP;
                            conf.action.force_cpu = true;
#else
                            /* if no internal ports: forward to other external ports, if internal ports exista: copy to CPU */
                            if (rules[i].internal_port_exists) {
                                memset(conf.action.port_list, 0x00, sizeof(conf.action.port_list));
                                conf.action.ptp_action = MESA_ACL_PTP_ACTION_TWO_STEP;
                                conf.action.force_cpu = true;
                            } else {
                                memcpy(conf.action.port_list,  rules[i].external_port_list, portlist_size);
                                conf.action.ptp_action = MESA_ACL_PTP_ACTION_ONE_STEP;
                                conf.action.force_cpu = false;
                            }
#endif
                            pptp->enable = true;
                            pptp->header.mask[0] = 0x0c;
                            pptp->header.value[0] = 0x00; /* messageType = [0..3] */
                            pptp->header.mask[1] = 0x0f;
                            pptp->header.value[1] = 0x02; /* versionPTP = 2 */
                            pptp->header.mask[2] = 0x00;
                            pptp->header.value[2] = 0x00; /* domainNumber = d.c */
                            pptp->header.mask[3] = 0x00;
                            pptp->header.value[3] = 0x00; /* flagField[0] = don't care */
    
                            /* ACL rule for External port Events:  */
                            PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
        
                            if (rules[i].internal_port_exists) {
                                /* ACL rule for Internal port Events: Send to CPU, SW expects a timestamp on all events, though it is not used */
                                memcpy(conf.port_list,  rules[i].internal_port_list, portlist_size);
                                memset(conf.action.port_list, 0x00, sizeof(conf.action.port_list));
                                conf.action.ptp_action = MESA_ACL_PTP_ACTION_TWO_STEP;
                                conf.action.force_cpu = true;
                                PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
        
                                /* ACL rule for follow_Up and Delay_Resp messages. Forwarded in SW like Sync and DelayReq, otherwise the packet order may change */
                                memcpy(conf.port_list,  rules[i].port_list, portlist_size);
                                memset(conf.action.port_list, 0x00, sizeof(conf.action.port_list));
                                conf.action.ptp_action = MESA_ACL_PTP_ACTION_NONE;
                                conf.action.force_cpu = true;
                                pptp->enable = true;
                                pptp->header.mask[0] = 0x0e;
                                pptp->header.value[0] = 0x08; /* messageType = [8..9] */
                                PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                            }
                        } else {
                            T_EG(VTSS_TRACE_GRP_ACE, "Not supported!");
                        }
                        /* ACL rule for other general messages */
                        memcpy(conf.port_list,  rules[i].port_list, portlist_size);
                        memcpy(conf.action.port_list,  rules[i].port_list, portlist_size);
                        conf.action.ptp_action = MESA_ACL_PTP_ACTION_NONE;
                        conf.action.force_cpu = false;
                        pptp->header.mask[0] = 0x08;
                        pptp->header.value[0] = 0x08; /* messageType = [8..15] */
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                    }

                    break;
                case VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT:
                    if (config_data.conf[i].clock_init.cfg.twoStepFlag) {
                        /* two-step P2P transparent clock */
                        /**********************************/
                        memset(conf.action.port_list, 0x00, sizeof(conf.action.port_list));
                        conf.action.ptp_action = MESA_ACL_PTP_ACTION_NONE;
                        conf.action.force_cpu = true;
                        pptp->enable = true;
                        pptp->header.mask[0] = 0x0d;
                        pptp->header.value[0] = 0x08; /* messageType = [8,10] */
                        pptp->header.mask[1] = 0x0f;
                        pptp->header.value[1] = 0x02; /* versionPTP = 2 */
                        pptp->header.mask[2] = 0x00;
                        pptp->header.value[2] = 0x00; /* domainNumber = d.c */
                        pptp->header.mask[3] = 0x00;
                        pptp->header.value[3] = 0x00; /* flagField[0] = don't care */

                        /* ACL rule for Follow_up, Pdelay_resp_follow_up */
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));

                        /* ACL rule for Delay_Req, Delay_resp */
                        if (mesa_cap_ts_p2p_delay_comp) {
                            conf.action.force_cpu = false;
                        }
                        pptp->header.mask[0] = 0x07;
                        pptp->header.value[0] = 0x01; /* messageType = [1,9] */
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));

                        if (mesa_cap_ts_twostep_always_required) {
                            /* ACL rule for Sync, Pdelay_req, Pdelay_resp */
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_TWO_STEP;
                            pptp->header.mask[0] = 0x0c;
                            pptp->header.value[0] = 0x00; /* messageType = [0..3] */
                            PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                        } else {
                            /* ACL rule for TwoStep Sync Events */
                            memset(conf.action.port_list, 0x00, sizeof(conf.action.port_list));
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_NONE;
                            conf.action.force_cpu = true;
                            pptp->header.mask[0] = 0x0f;
                            pptp->header.value[0] = 0x00; /* messageType = [0] */
                            pptp->header.mask[3] = 0x02;
                            pptp->header.value[3] = 0x02; /* flagField[0] = twostep = 1 */
                            PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
    
                            /* ACL rule for PdelayResp events */
                            memset(conf.action.port_list, 0x00, sizeof(conf.action.port_list));
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_ONE_STEP_SUB_DELAY_1; /* Asymmetry delay compensation */
                            conf.action.force_cpu = true;
                            pptp->header.mask[0] = 0x0f;
                            pptp->header.value[0] = 0x03; /* messageType = [3] */
                            pptp->header.mask[3] = 0x00;
                            pptp->header.value[3] = 0x00; /* flagField[0] = don't care */
                            PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
    
                            /* ACL rule for OnesStep Sync, Pdelay_req */
                            memset(conf.action.port_list, 0x00, sizeof(conf.action.port_list));
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_NONE;
                            conf.action.force_cpu = true;
                            pptp->header.mask[0] = 0x0d;
                            pptp->header.value[0] = 0x00; /* messageType = [0,2] */
                            PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                        }

                        /* ACL rule for All other PTP messages */
                        memcpy(conf.action.port_list,  rules[i].port_list, portlist_size);
                        conf.action.ptp_action = MESA_ACL_PTP_ACTION_NONE;
                        pptp->header.mask[0] = 0x00;
                        pptp->header.value[0] = 0x00; /* messageType = d.c. */
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                    } else {
                        /* one-step P2P transparent clock */
                        /**********************************/
                        memcpy(conf.action.port_list,  rules[i].port_list, portlist_size);
                        if (mesa_cap_ts_p2p_delay_comp && mesa_cap_ts_asymmetry_comp) {
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_ONE_STEP_SUB_DELAY_2;
                        } else {
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_ONE_STEP;
                        }
                        conf.action.force_cpu = false;
                        pptp->enable = true;
                        pptp->header.mask[0] = 0x0f;
                        pptp->header.value[0] = 0x00; /* messageType = [0] */
                        pptp->header.mask[1] = 0x0f;
                        pptp->header.value[1] = 0x02; /* versionPTP = 2 */
                        pptp->header.mask[2] = 0x00;
                        pptp->header.value[2] = 0x00; /* domainNumber = d.c */
                        pptp->header.mask[3] = 0x00;
                        pptp->header.value[3] = 0x00; /* flagField[0] = don't care */

                        /* ACL rule for Sync Events */
                        if (!mesa_cap_ts_p2p_delay_comp) {
                            /* as Luton26 does not support path delay correction, the sync is handled in the CPU */
                            memset(conf.action.port_list, 0x00, sizeof(conf.action.port_list));
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_TWO_STEP;
                            conf.action.force_cpu = true;
                        }
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));

                        if (mesa_cap_ts_twostep_always_required) {
                            /* ACL rule for Pdelay Events */
                            memset(conf.action.port_list, 0x00, sizeof(conf.action.port_list));
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_TWO_STEP;
                            conf.action.force_cpu = true;
                            pptp->header.mask[0] = 0x0e;
                            pptp->header.value[0] = 0x02; /* messageType = [2..3] */
                            PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                        } else {
                            /* ACL rule for PdelayReq Events */
                            memset(conf.action.port_list, 0x00, sizeof(conf.action.port_list));
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_NONE;
                            conf.action.force_cpu = true;
                            pptp->header.mask[0] = 0x0f;
                            pptp->header.value[0] = 0x02; /* messageType = [2] */
                            PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
    
                            /* ACL rule for PdelayResp Events */
                            conf.action.ptp_action = MESA_ACL_PTP_ACTION_ONE_STEP_SUB_DELAY_1;
                            conf.action.force_cpu = true;
                            pptp->header.mask[0] = 0x0f;
                            pptp->header.value[0] = 0x03; /* messageType = [3] */
                            PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                        }

                        /* ACL rule for Pdelay_Resp_follow_Up messages */
                        conf.action.ptp_action = MESA_ACL_PTP_ACTION_NONE;
                        if (mesa_cap_ts_p2p_delay_comp) {
                            pptp->header.mask[0] = 0x0f;
                            pptp->header.value[0] = 0x0a; /* messageType = [10] */
                        } else {
                            /* in Luton26 the sync is handled in the CPU, and also the Followup, otherwise the Followup may arrive before the Sync */
                            pptp->header.mask[0] = 0x0d;
                            pptp->header.value[0] = 0x08; /* messageType = [8, 10] */
                        }
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));

                        /* ACL rule for Delay_Req and Delay_Resp messages */
                        conf.action.ptp_action = MESA_ACL_PTP_ACTION_NONE;
                        conf.action.force_cpu = false;
                        pptp->header.mask[0] = 0x07;
                        pptp->header.value[0] = 0x01; /* messageType = [1,9] */
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));

                        /* ACL rule for All other PTP messages */
                        memcpy(conf.action.port_list,  rules[i].port_list, portlist_size);
                        pptp->header.mask[0] = 0x00;
                        pptp->header.value[0] = 0x00; /* messageType = d.c. */
                        PTP_RETURN(acl_rule_apply(&conf, i, rule_no++));
                    }
                    break;
            }

            if (rules[i].deviceType == VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT || rules[i].deviceType == VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT) {
                my_next_id = rules[i].id[0];
                if (i != 0) {
                    my_tc_encapsulation = rules[i].protocol;   /* indicates that forwarding in the BC instance is needed */

                    if (mesa_cap_ts_p2p_delay_comp) {
                        if (config_data.conf[i].clock_init.cfg.twoStepFlag) {
                            my_2step_tc_instance = i;
                        } else {
                            my_tc_instance = i;
                        }
                    } else {
                        if ((rules[i].deviceType == VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT) || config_data.conf[i].clock_init.cfg.twoStepFlag) {
                            my_2step_tc_instance = i;
                        } else {
                            my_tc_instance = i;
                        }
                    }
                    T_DG(VTSS_TRACE_GRP_ACE,"my_tc_instance = %d", i);
                    for (int ii = 0; ii < PTP_CLOCK_INSTANCES; ii++) {
                        if (i != ii && protocol_level(my_tc_encapsulation) == protocol_level(rules[ii].protocol)) {
                            /* recalculate instance 0 ACL rules */
                            refresh_inst[ii] = true;
                        }
                    }
                } else {
                    my_tc_encapsulation = VTSS_APPL_PTP_PROTOCOL_MAX_TYPE;   /* indicates that no forwarding in the SlaveOnly instance is needed */
                    my_tc_instance = i;
                    my_2step_tc_instance = i;
                    T_DG(VTSS_TRACE_GRP_ACE,"my_tc_instance = %d", i);
                    for (int ii = 0; ii < PTP_CLOCK_INSTANCES; ii++) {
                        if (i != ii && protocol_level(rules[i].protocol) == protocol_level(rules[ii].protocol)) {
                            /* recalculate instance 0 ACL rules */
                            refresh_inst[ii] = true;
                        }
                    }
                }
            }
        }
        if (rules[i].deviceType == VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT || rules[i].deviceType == VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT) {
            my_next_id = rules[i].id[0];
        }

    }
    if (ptp_all_external_port_phy_ts) {
        /* All ports have 1588 PHY, therefore no ACL rules are needed for timestamping */
        /* instead the forwarding is set up in the MAC table */
        mac_used = 0;
        my_tc_instance = -1;
        for (inst = 0; inst < PTP_CLOCK_INSTANCES; inst++) {
            if (rules[inst].deviceType != VTSS_APPL_PTP_DEVICE_NONE &&
                    (rules[inst].protocol == VTSS_APPL_PTP_PROTOCOL_ETHERNET || rules[inst].protocol == VTSS_APPL_PTP_PROTOCOL_ETHERNET_MIXED
                     || rules[inst].protocol == VTSS_APPL_PTP_PROTOCOL_IP4MULTI || rules[inst].protocol == VTSS_APPL_PTP_PROTOCOL_IP4MIXED )) {
                /* check if the configuration matches previous instances */
                for (mac_idx = 0; mac_idx < mac_used; mac_idx++) {
                    if ((0 == memcmp(&mac_entry[mac_idx].vid_mac.mac,  /* match 1588 multicast Dest address */
                                     (rules[inst].protocol == VTSS_APPL_PTP_PROTOCOL_IP4MULTI || rules[inst].protocol == VTSS_APPL_PTP_PROTOCOL_IP4MIXED)
                                       ? &ptp_ip_mcast_adr[0] : &ptp_eth_mcast_adr[0],
                                     sizeof(mesa_mac_t))) && mac_entry[mac_idx].vid_mac.vid == config_data.conf[inst].clock_init.cfg.configured_vid) {
                        break;
                    }
                }
                if (mac_idx >= mac_used) {
                    ++mac_used;
                }
                T_IG(VTSS_TRACE_GRP_ACE,"mac_used %d, mac_idx %d", mac_used, mac_idx);
                memcpy(&mac_entry[mac_idx].vid_mac.mac,  /* match 1588 multicast Dest address */
                       (rules[inst].protocol == VTSS_APPL_PTP_PROTOCOL_IP4MULTI || rules[inst].protocol == VTSS_APPL_PTP_PROTOCOL_IP4MIXED)
                         ? &ptp_ip_mcast_adr[0] : &ptp_eth_mcast_adr[0],
                       sizeof(mesa_mac_t));
                mac_entry[mac_idx].vid_mac.vid = config_data.conf[inst].clock_init.cfg.configured_vid;
                if (rules[inst].deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND || rules[inst].deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY || rules[inst].deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY) {
                    mac_entry[mac_idx].copy_to_cpu = true;
                }
                if (rules[inst].deviceType == VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT || rules[inst].deviceType == VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT || rules[inst].deviceType == VTSS_APPL_PTP_DEVICE_BC_FRONTEND) {
                    port_iter_init_local(&pit);
                    my_tc_instance = inst;
                    while (port_iter_getnext(&pit)) {
                        mac_entry[mac_idx].destination[pit.iport] |= rules[inst].port_list[pit.iport];
                    }
                }
            }
            if (mesa_cap_ts_delay_req_auto_resp) {
                // the auto response rule shall also be set up even if all ports are PHY TS ports
                if (rules[i].deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND || rules[i].deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY) {
                    portlist_size = sizeof(conf.port_list);
                    memcpy(conf.port_list,  rules[i].port_list, portlist_size);
                    conf.action.port_action = MESA_ACL_PORT_ACTION_FILTER;
                    if (conf.type == MESA_ACE_TYPE_ETYPE) {
                        pptp = &conf.frame.etype.ptp;
                    } else if (conf.type == MESA_ACE_TYPE_IPV4) {
                        pptp = &conf.frame.ipv4.ptp;
                    } else {
                        T_WG(VTSS_TRACE_GRP_ACE,"unsupported ACL frame type %d", conf.type);
                        return PTP_RC_UNSUPPORTED_ACL_FRAME_TYPE;
                    }
                    pptp->enable = true;
                    pptp->header.mask[1] = 0x0f;
                    pptp->header.value[1] = 0x02; /* versionPTP = 2 */
                    pptp->header.mask[2] = 0xff;
                    pptp->header.value[2] = rules[i].domain; /* domainNumber */
                    pptp->header.mask[3] = 0x00;
                    pptp->header.value[3] = 0x00; /* flagField[0] = don't care */
                    if (rules[i].protocol == VTSS_APPL_PTP_PROTOCOL_IP4UNI) {
                        /* always terminate PTP peer delay messages */
                        conf.frame.ipv4.dip.mask = 0x0;
                    }
                    if (config_data.conf[i].clock_init.cfg.clock_domain < mesa_cap_ts_domain_cnt) {
                        //Auto response is only supported in HW clock domains
                        PTP_RC(ptp_auto_resp_setup(i, &conf, &rule_no));
                    }
                }
            }
        }
        /* repeat the same process for the Peer delay multicast addresses (always to sent to CPU) */
        for (inst = 0; inst < PTP_CLOCK_INSTANCES; inst++) {
            if (rules[inst].deviceType != VTSS_APPL_PTP_DEVICE_NONE &&
                    (rules[inst].protocol == VTSS_APPL_PTP_PROTOCOL_ETHERNET || rules[inst].protocol == VTSS_APPL_PTP_PROTOCOL_ETHERNET_MIXED
                     || rules[inst].protocol == VTSS_APPL_PTP_PROTOCOL_IP4MULTI || rules[inst].protocol == VTSS_APPL_PTP_PROTOCOL_IP4MIXED)) {
                /* check if the configuration matches previous instances */
                for (mac_idx = 0; mac_idx < mac_used; mac_idx++) {
                    if ((0 == memcmp(&mac_entry[mac_idx].vid_mac.mac,  /* match 1588 multicast Dest address */
                                     (rules[inst].protocol == VTSS_APPL_PTP_PROTOCOL_IP4MULTI || rules[inst].protocol == VTSS_APPL_PTP_PROTOCOL_IP4MIXED)
                                       ? &ptp_ip_mcast_adr[1] : &ptp_eth_mcast_adr[1],
                                     sizeof(mesa_mac_t))) && mac_entry[mac_idx].vid_mac.vid == config_data.conf[inst].clock_init.cfg.configured_vid) {
                        break;
                    }
                }
                if (mac_idx >= mac_used) {
                    ++mac_used;
                }
                T_IG(VTSS_TRACE_GRP_ACE,"mac_used %d, mac_idx %d", mac_used, mac_idx);
                memcpy(&mac_entry[mac_idx].vid_mac.mac,  /* match 1588 multicast Dest address */
                       (rules[inst].protocol == VTSS_APPL_PTP_PROTOCOL_IP4MULTI || rules[inst].protocol == VTSS_APPL_PTP_PROTOCOL_IP4MIXED)
                         ? &ptp_ip_mcast_adr[1] : &ptp_eth_mcast_adr[1],
                       sizeof(mesa_mac_t));
                mac_entry[mac_idx].vid_mac.vid = config_data.conf[inst].clock_init.cfg.configured_vid;

                mac_entry[mac_idx].copy_to_cpu = true;
            }
        }
        /* now apply the calculated mac teble entries */
        for (mac_idx = 0; mac_idx < mac_used; mac_idx++) {
            port_iter_init_local(&pit);
            idx = 0;
            dest_txt[0] = 0;
            while (port_iter_getnext(&pit)) {
                if (mac_entry[mac_idx].destination[pit.iport]) {
                    idx += snprintf(&dest_txt[idx], sizeof(dest_txt)-idx, "%d, ", pit.uport);
                }
            }
            T_IG(VTSS_TRACE_GRP_ACE,"Adding mac table entry: mac %s, vid %d, copy to cpu %d, destinations %s", misc_mac2str(mac_entry[mac_idx].vid_mac.mac.addr),
                mac_entry[mac_idx].vid_mac.vid, mac_entry[mac_idx].copy_to_cpu, dest_txt);
            mac_entry[mac_idx].locked = true;
            mac_entry[mac_idx].aged = false;
            mac_entry[mac_idx].cpu_queue = PACKET_XTR_QU_BPDU;
            PTP_RC(mesa_mac_table_add(NULL, &mac_entry[mac_idx]));
        }
    }

    for (int ii = 0; ii < PTP_CLOCK_INSTANCES; ii++) {
        if (refresh_inst[ii]) {
            T_IG(VTSS_TRACE_GRP_ACE,"ptp_ace_update %d", ii);
            PTP_RC(_ptp_ace_update(ii, ptp_inst_updated));
        }
    }

    return rc;
}

static mesa_rc ptp_ace_update(int i)
{
    CapArray<bool, VTSS_APPL_CAP_PTP_CLOCK_CNT> ptp_inst_updated;  // Note: All elements initialized to 0 i.e. FALSE by constructor

    return _ptp_ace_update(i, ptp_inst_updated);
}

/****************************************************************************/
/*  Allocated PHY Timestamp functions                                       */
/****************************************************************************/



typedef struct {
    bool                        phy_ts_port;  /* TRUE if this port is a PTP port and has the PHY timestamp feature */
    mesa_phy_ts_engine_t        engine_id;    /* MESA_PHY_TS_ENGINE_ID_INVALID indicates that no engine is allocated */
    u8                          flow_id_low;  /* identifies the flows allocated for this port */
    u8                          flow_id_high; /* identifies the flows allocated for this port */
    u32                         action_idx;   /* indicates if a PTP action is enabled for this port. (0xffffffff indicates no action)*/
} ptp_phy_ts_engine_alloc_t;

#define VTSS_PHY_PTP_ACTION_INVALID 0xffffffff

typedef struct {
    u8 domain;
    u8 deviceType;
    bool twoStepFlag;
    u8 protocol;
    ptp_phy_ts_engine_alloc_t port;
} ptp_phy_ts_rule_t;

/**
 * \brief PHY timestamp configuration for each PTP instance
 */
static CapArray<ptp_phy_ts_rule_t, VTSS_APPL_CAP_PTP_CLOCK_CNT, MESA_CAP_PORT_CNT> _phy_ts_rules;

/**
 * \brief The PHY timestamp rules can be shared among more PTP instances, if the same protocol and 1/2-step mode is used
 *        phy_ts_rules_index points for each PTP instance to the rule table that is used.
 */
static CapArray<int, VTSS_APPL_CAP_PTP_CLOCK_CNT, MESA_CAP_PORT_CNT> _phy_ts_rules_index;

static void phy_ts_rules_init(void)
{
    int inst;
    mesa_port_no_t j;
    port_iter_t       pit;
    for (inst = 0; inst < PTP_CLOCK_INSTANCES; inst++) {
        port_iter_init_local(&pit);
        while (port_iter_getnext(&pit)) {
            j = pit.iport;

            _phy_ts_rules[inst][j].domain = 0;
            _phy_ts_rules[inst][j].deviceType = VTSS_APPL_PTP_DEVICE_NONE;
            _phy_ts_rules[inst][j].protocol = VTSS_APPL_PTP_PROTOCOL_ETHERNET;
            _phy_ts_rules[inst][j].twoStepFlag = false;
            _phy_ts_rules[inst][j].port.phy_ts_port = false;
            _phy_ts_rules[inst][j].port.engine_id = MESA_PHY_TS_ENGINE_ID_INVALID;
            _phy_ts_rules[inst][j].port.flow_id_low = 0;
            _phy_ts_rules[inst][j].port.flow_id_high = 0;
            _phy_ts_rules[inst][j].port.action_idx = VTSS_PHY_PTP_ACTION_INVALID;

            _phy_ts_rules_index[inst][j] = vtss_appl_cap_ptp_clock_cnt;  // indicate that the instance is passive
        }
    }
}

#ifdef PHY_DATA_DUMP
static void phy_ts_dump(void)
{
    int i,p;
    for (i = 0 ; i < PTP_CLOCK_INSTANCES ; i++) {
        printf("inst %d: domain %d, type %d proto: %d\n", i, phy_ts_rules[i].domain, phy_ts_rules[i].deviceType, phy_ts_rules[i].protocol);
        port_iter_t       pit;
        port_iter_init_local(&pit);
        while (port_iter_getnext(&pit)) {
            p = pit.iport;
            if (phy_ts_rules[i].port[p].phy_ts_port) {
                printf("  port %d: eng_id %d, f_lo: %d f_hi: %d\n", p,
                       phy_ts_rules[i].port[p].engine_id,
                       phy_ts_rules[i].port[p].flow_id_low,
                       phy_ts_rules[i].port[p].flow_id_high);
            }
        }

    }
}

static void dump_conf(mesa_phy_ts_engine_flow_conf_t *flow_conf)
{
    int i;
    printf("flow conf: eng_mode %d ", flow_conf->eng_mode);
    printf("ptp comm: ppb_en %d, etype %x, tpid %x\n", flow_conf->flow_conf.ptp.eth1_opt.comm_opt.pbb_en,
           flow_conf->flow_conf.ptp.eth1_opt.comm_opt.etype,
           flow_conf->flow_conf.ptp.eth1_opt.comm_opt.tpid);
    for (i = 0; i < 8; i++) {
        printf(" channel_map[%d] %d ", i, flow_conf->channel_map[i]);
        printf(" ptp flow: flow_en %d, match_mode %d, match_select %d\n", flow_conf->flow_conf.ptp.eth1_opt.flow_opt[i].flow_en,
               flow_conf->flow_conf.ptp.eth1_opt.flow_opt[i].addr_match_mode,
               flow_conf->flow_conf.ptp.eth1_opt.flow_opt[i].addr_match_select);
        printf(" mac address %s\n",misc_mac2str(flow_conf->flow_conf.ptp.eth1_opt.flow_opt[i].mac_addr));
        printf(" vlan_check %d, num_tag %d, outer_tag_type %d, inner_tag_type %d, tag_range_mode %d\n",
               flow_conf->flow_conf.ptp.eth1_opt.flow_opt[i].vlan_check,
               flow_conf->flow_conf.ptp.eth1_opt.flow_opt[i].num_tag,
               flow_conf->flow_conf.ptp.eth1_opt.flow_opt[i].outer_tag_type,
               flow_conf->flow_conf.ptp.eth1_opt.flow_opt[i].inner_tag_type,
               flow_conf->flow_conf.ptp.eth1_opt.flow_opt[i].tag_range_mode);
    }
}

static void dump_ptp_action(mesa_phy_ts_engine_action_t *ptp_action)
{
    int i;
    printf("action_ptp %d\n", ptp_action->action_ptp);
    for (i = 0; i < 2; i++) {
        printf(" action[%d]: enable %d, channel_map %d\n", i, ptp_action->action.ptp_conf[i].enable, ptp_action->action.ptp_conf[i].channel_map);
        printf("  ptpconf: range_en %d, val/upper %d, mask/lower %d\n", ptp_action->action.ptp_conf[i].ptp_conf.range_en,
               ptp_action->action.ptp_conf[i].ptp_conf.domain.value.val,
               ptp_action->action.ptp_conf[i].ptp_conf.domain.value.mask);
        printf("   clk__mode %d, delaym_type %d\n",ptp_action->action.ptp_conf[i].clk_mode,
              ptp_action->action.ptp_conf[i].delaym_type);
    }
}

#endif

/**
 * \brief Update the PHY timestamp analyzers for a PTP engine.
 * \note This function is called whenever the PTP configuration for a ptp
 *  instance change.
 * The function reserves an Analyzer engine pr. PTP instance, if the protocol and 1/2-step mode are different.
 * If the protocol and 1/2-step mode are the same for more instances, then one engine is shared.
 * this has the advantage that a BC and a TC can coexist, assuming that the BC uses the Correction field update methos at egress.
 * If two BC's share the same engine, the analyzed ignores the domain id, but the domain id is checked in the PTP application, so this is no problem.
 * When more instances share the same engine, it is required that the same delay mechanism is used.
 * If An analyzer is shared between two ports, and both ports are enabled in the same PTP instance,
 * then the Analyzer is shared between the two ports.
 * The number of flows pr analyzer is variable (analyzer 0 and 1 have 8 flows each, while analyser 2a and 2b
 * shares 8 flows). To simplify the model, 4 flows are reserved for each analyser: 2 for channel 0 and 2 for channel 1.
 * I.e. the port that is connected to channel 0 uses flow 0..1, and
 * the port that is connected to channel 1 uses flow 2..3.
 *
 * \param inst        [IN]    PTP instance number [0..x]
 *
 * \return Return code.
 **/

static mesa_rc ptp_phy_ts_update(int ptp_inst, int port)
{
    if (mesa_cap_phy_ts) {    



//    int j;
        mesa_phy_ts_encap_t encap_type;
        mesa_port_no_t shared_port;
        u8  flow_id;
        u32 ai;
        int inst;
        bool engine_free = false;

        T_DG(VTSS_TRACE_GRP_PHY_TS,"ptp_inst %d deviceType %d, protocol %d", ptp_inst, config_data.conf[ptp_inst].clock_init.cfg.deviceType, config_data.conf[ptp_inst].clock_init.cfg.protocol);
        if (config_data.conf[ptp_inst].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE &&
            _phy_ts_rules_index[ptp_inst][port] != PTP_CLOCK_INSTANCES &&
            config_data.conf[ptp_inst].clock_init.cfg.protocol != _phy_ts_rules[_phy_ts_rules_index[ptp_inst][port]][port].protocol)
        {
            vtss_appl_ptp_device_type_t temp_devicetype = config_data.conf[ptp_inst].clock_init.cfg.deviceType;
            config_data.conf[ptp_inst].clock_init.cfg.deviceType = VTSS_APPL_PTP_DEVICE_NONE;
            // if protocol changes on a running PTP instance, the PHY engines must be deallocated,
            // this is done by a recurcive call to this function with deviceType = VTSS_APPL_PTP_DEVICE_NONE
            ptp_phy_ts_update(ptp_inst, port);
            config_data.conf[ptp_inst].clock_init.cfg.deviceType = temp_devicetype;
            T_IG(VTSS_TRACE_GRP_PHY_TS,"ptp_inst %d deviceType %d, protocol changed to %d", ptp_inst, temp_devicetype, config_data.conf[ptp_inst].clock_init.cfg.protocol);
        } else {
            T_IG(VTSS_TRACE_GRP_PHY_TS,"ptp_inst %d deviceType %d, protocol %d", ptp_inst, config_data.conf[ptp_inst].clock_init.cfg.deviceType, config_data.conf[ptp_inst].clock_init.cfg.protocol);
        }

        switch (config_data.conf[ptp_inst].clock_init.cfg.protocol) {
            case VTSS_APPL_PTP_PROTOCOL_ETHERNET:
            case VTSS_APPL_PTP_PROTOCOL_ETHERNET_MIXED:
                encap_type = MESA_PHY_TS_ENCAP_ETH_PTP;
                break;
            case VTSS_APPL_PTP_PROTOCOL_IP4MULTI:
            case VTSS_APPL_PTP_PROTOCOL_IP4MIXED:
            case VTSS_APPL_PTP_PROTOCOL_IP4UNI:
                encap_type = MESA_PHY_TS_ENCAP_ETH_IP_PTP;
                break;
            case VTSS_APPL_PTP_PROTOCOL_OAM:
            case VTSS_APPL_PTP_PROTOCOL_ONE_PPS:
                return VTSS_RC_OK;
            default:
                T_WG(VTSS_TRACE_GRP_PHY_TS,"unsupported encapsulation type");
                return PTP_RC_UNSUPPORTED_PTP_ENCAPSULATION_TYPE;
        }

        bool port_uses_two_step;
        if ((config_data.conf[ptp_inst].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND) || (config_data.conf[ptp_inst].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY)) {
            port_uses_two_step = ((config_data.conf[ptp_inst].clock_init.cfg.twoStepFlag && !(config_data.conf[ptp_inst].port_config[port].twoStepOverride == VTSS_APPL_PTP_TWO_STEP_OVERRIDE_FALSE))
                                  || (config_data.conf[ptp_inst].port_config[port].twoStepOverride == VTSS_APPL_PTP_TWO_STEP_OVERRIDE_TRUE));
        }
        else {
            port_uses_two_step = config_data.conf[ptp_inst].clock_init.cfg.twoStepFlag;
        }

        if (config_data.conf[ptp_inst].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            if (_phy_ts_rules_index[ptp_inst][port] == PTP_CLOCK_INSTANCES) {
                // find an index in the rules table
                for (inst = 0; inst < PTP_CLOCK_INSTANCES; inst++) {
                    if (_phy_ts_rules[inst][port].deviceType != VTSS_APPL_PTP_DEVICE_NONE &&
                        _phy_ts_rules[inst][port].protocol == config_data.conf[ptp_inst].clock_init.cfg.protocol &&
                        _phy_ts_rules[inst][port].twoStepFlag == port_uses_two_step) {
                        _phy_ts_rules_index[ptp_inst][port] = inst;
                        break;
                    }
                }
                if (_phy_ts_rules_index[ptp_inst][port] == PTP_CLOCK_INSTANCES) {
                    for (inst = 0; inst < PTP_CLOCK_INSTANCES; inst++) {
                        if (_phy_ts_rules[inst][port].deviceType == VTSS_APPL_PTP_DEVICE_NONE) {
                            _phy_ts_rules_index[ptp_inst][port] = inst;
                            break;
                        }
                    }
                }
            }
        } else {
            if (_phy_ts_rules_index[ptp_inst][port] == PTP_CLOCK_INSTANCES) {
                return VTSS_RC_OK;
            }
        }
        inst = _phy_ts_rules_index[ptp_inst][port];
        if (_phy_ts_rules_index[ptp_inst][port] == PTP_CLOCK_INSTANCES) {
            T_EG(VTSS_TRACE_GRP_PHY_TS,"Internal error");
            return VTSS_RC_ERROR;
        }
        if (config_data.conf[ptp_inst].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_NONE) {
            _phy_ts_rules_index[ptp_inst][port] = PTP_CLOCK_INSTANCES;
        }
        T_IG(VTSS_TRACE_GRP_PHY_TS,"ptp_inst %d, inst %d, deviceType %d", ptp_inst, inst, config_data.conf[ptp_inst].clock_init.cfg.deviceType);
        if (config_data.conf[ptp_inst].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            _phy_ts_rules[inst][port].deviceType = config_data.conf[ptp_inst].clock_init.cfg.deviceType;
            _phy_ts_rules[inst][port].protocol = config_data.conf[ptp_inst].clock_init.cfg.protocol;
            _phy_ts_rules[inst][port].domain = config_data.conf[ptp_inst].clock_init.cfg.domainNumber;
            _phy_ts_rules[inst][port].twoStepFlag = port_uses_two_step;
        }
        _phy_ts_rules[inst][port].deviceType = VTSS_APPL_PTP_DEVICE_NONE;
        for (int i = 0; i < PTP_CLOCK_INSTANCES; i++) {
            ports_org_time[i] = true;
            if (_phy_ts_rules_index[i][port] == inst) {
                /* if the rule is shared between a BC and a TC, the TC mode wins */
                if (_phy_ts_rules[inst][port].deviceType == VTSS_APPL_PTP_DEVICE_NONE ||
                    config_data.conf[i].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT ||
                    config_data.conf[i].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT)
                {
                    _phy_ts_rules[inst][port].deviceType = config_data.conf[i].clock_init.cfg.deviceType;
                }
            }
            T_IG(VTSS_TRACE_GRP_PHY_TS,", phy_ts_rules_index[%d] %d", i, _phy_ts_rules_index[i][port]);
        }
        for (int i = 0; i < PTP_CLOCK_INSTANCES; i++) {
            ports_org_time[i] = true;
            if (_phy_ts_rules[_phy_ts_rules_index[i][port]][port].deviceType == VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT ||
                _phy_ts_rules[_phy_ts_rules_index[i][port]][port].deviceType == VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT)
            {
                ports_org_time[i] = false;
            }
            T_IG(VTSS_TRACE_GRP_PHY_TS,"inst %d, idx %d, ports_org_time %d", i, _phy_ts_rules_index[i][port], ports_org_time[i]);
            T_IG(VTSS_TRACE_GRP_PHY_TS,"inst %d, deviceType %d, protocol %d, domain %d, twoStepFlag %d",
               i, _phy_ts_rules[i][port].deviceType, _phy_ts_rules[i][port].protocol,
                  _phy_ts_rules[i][port].domain, _phy_ts_rules[i][port].twoStepFlag);
        }
    
        T_IG(VTSS_TRACE_GRP_PHY_TS,"setting ts rules for instance %d", inst);
    
        _phy_ts_rules[inst][port].port.phy_ts_port = false;
        for (int i = 0; i < PTP_CLOCK_INSTANCES; i++) {
            if (_phy_ts_rules_index[i][port] == inst) {
                _phy_ts_rules[inst][port].port.phy_ts_port = _phy_ts_rules[inst][port].port.phy_ts_port ||
                    ((config_data.conf[i].port_config[port].enabled != 0 && !config_data.conf[i].port_config[port].portInternal) && (port_data[port].topo.ts_feature == VTSS_PTP_TS_PTS));
            }
        }
        T_IG(VTSS_TRACE_GRP_PHY_TS,"port %d active TS %d", port, _phy_ts_rules[inst][port].port.phy_ts_port);
    
        if (_phy_ts_rules[inst][port].port.phy_ts_port) {
            T_DG(VTSS_TRACE_GRP_PHY_TS,"allocate engines for port %d", port);
            /* Allocate engines for this instance */
            if (_phy_ts_rules[inst][port].port.engine_id == MESA_PHY_TS_ENGINE_ID_INVALID) { /* if not already allocated */
                _phy_ts_rules[inst][port].port.engine_id = tod_phy_eng_alloc(port, encap_type);
                if (_phy_ts_rules[inst][port].port.engine_id == MESA_PHY_TS_ENGINE_ID_INVALID) {
                    T_IG(VTSS_TRACE_GRP_PHY_TS,"No available TS engine for ptp instance %d, port %d", inst, port);
                    _phy_ts_rules[inst][port].port.phy_ts_port = false;
                    return PTP_RC_MISSING_PHY_TIMESTAMP_RESOURCE;
                }


















                PTP_RC(mesa_phy_ts_ingress_engine_init(API_INST_DEFAULT,
                        port,
                        _phy_ts_rules[inst][port].port.engine_id,
                        encap_type,
                        0, 3, /* 4 flows are always available (engine 2 can be shared between 2 API engines)  */
                        MESA_PHY_TS_ENG_FLOW_MATCH_STRICT));

                T_IG(VTSS_TRACE_GRP_PHY_TS,"ing eng init id: %d for port %d, encap_type %d", _phy_ts_rules[inst][port].port.engine_id, port, encap_type);
                /* if this engine is shared by an other port, then the other port is also marked as allocated */
                if (port_data[port].topo.port_shared) {
                    shared_port = port_data[port].topo.shared_port_no;
                    _phy_ts_rules[inst][shared_port].port.engine_id = _phy_ts_rules[inst][port].port.engine_id;
                    T_IG(VTSS_TRACE_GRP_PHY_TS,"shared engine: port %d, sharedPort %d, ing_id %d",
                         port, shared_port, _phy_ts_rules[inst][port].port.engine_id);
                    if (port_data[port].topo.channel_id == 0) {
                        _phy_ts_rules[inst][port].port.flow_id_low = 0;
                        _phy_ts_rules[inst][port].port.flow_id_high = 1;
                        _phy_ts_rules[inst][shared_port].port.flow_id_low = 2;
                        _phy_ts_rules[inst][shared_port].port.flow_id_high = 3;
                    } else {
                        _phy_ts_rules[inst][port].port.flow_id_low = 2;
                        _phy_ts_rules[inst][port].port.flow_id_high = 3;
                        _phy_ts_rules[inst][shared_port].port.flow_id_low = 0;
                        _phy_ts_rules[inst][shared_port].port.flow_id_high = 1;
                    }
                } else {
                    T_IG(VTSS_TRACE_GRP_PHY_TS,"non shared engine: port %d, ing_id %d", port, _phy_ts_rules[inst][port].port.engine_id);
                    _phy_ts_rules[inst][port].port.flow_id_low = 0;
                    _phy_ts_rules[inst][port].port.flow_id_high = 3;
                }

















                PTP_RC(mesa_phy_ts_egress_engine_init(API_INST_DEFAULT,
                                                      port,
                                                      _phy_ts_rules[inst][port].port.engine_id,
                                                      encap_type,
                                                      0, 3, /* 4 flows are always available (engine 2 can be shared between 2 API engines)  */
                                                      MESA_PHY_TS_ENG_FLOW_MATCH_STRICT));

    
                /* pr default no actiona  are enabled */
                mesa_phy_ts_engine_action_t ptp_action;
                PTP_RC(mesa_phy_ts_ingress_engine_action_get(API_INST_DEFAULT,
                        port,
                        _phy_ts_rules[inst][port].port.engine_id,
                        &ptp_action));
                for (ai = 0; ai < 2; ai++) {
                    ptp_action.action.ptp_conf[ai].enable = false;
                    ptp_action.action.ptp_conf[ai].channel_map = 0;
                }
                PTP_RC(mesa_phy_ts_ingress_engine_action_set(API_INST_DEFAULT,
                        port,
                        _phy_ts_rules[inst][port].port.engine_id,
                        &ptp_action));
                PTP_RC(mesa_phy_ts_egress_engine_action_set(API_INST_DEFAULT,
                        port,
                        _phy_ts_rules[inst][port].port.engine_id,
                        &ptp_action));
    
                T_IG(VTSS_TRACE_GRP_PHY_TS,"eg eng init id: %d for port %d, encap_type %d", _phy_ts_rules[inst][port].port.engine_id, port, encap_type);
    
            }
            /* Set up flow comparators */
            mesa_phy_ts_engine_flow_conf_t flow_conf;













            PTP_RC(mesa_phy_ts_ingress_engine_conf_get(API_INST_DEFAULT,
                    port,
                    _phy_ts_rules[inst][port].port.engine_id,
                    &flow_conf));

#ifdef PHY_DATA_DUMP
            T_WG(VTSS_TRACE_GRP_PHY_TS,"conf dump before:");
            dump_conf(&flow_conf);
#endif
            T_NG(VTSS_TRACE_GRP_PHY_TS,"get ing engine conf: %d", _phy_ts_rules[inst][port].port.engine_id);
            /* modify flow configuration */
            flow_conf.eng_mode = true;
    
            for (flow_id = _phy_ts_rules[inst][port].port.flow_id_low; flow_id <= _phy_ts_rules[inst][port].port.flow_id_high; flow_id++) {
                flow_conf.channel_map[flow_id] = port_data[port].topo.channel_id == 0 ? MESA_PHY_TS_ENG_FLOW_VALID_FOR_CH0 : MESA_PHY_TS_ENG_FLOW_VALID_FOR_CH1;
                flow_conf.flow_conf.ptp.eth1_opt.flow_opt[flow_id].flow_en = true;
                flow_conf.flow_conf.ptp.eth1_opt.flow_opt[flow_id].addr_match_mode = MESA_PHY_TS_ETH_ADDR_MATCH_ANY_MULTICAST | MESA_PHY_TS_ETH_ADDR_MATCH_ANY_UNICAST;
                flow_conf.flow_conf.ptp.eth1_opt.flow_opt[flow_id].addr_match_select = MESA_PHY_TS_ETH_MATCH_DEST_ADDR;
                flow_conf.flow_conf.ptp.eth1_opt.flow_opt[flow_id].vlan_check = false;
                T_DG(VTSS_TRACE_GRP_PHY_TS,"eth: flow_opt[%d]: channel_map %d, flow_en %d, match_mode %d, match:_sel %d",
                     flow_id,
                     flow_conf.channel_map[flow_id],
                     flow_conf.flow_conf.ptp.eth1_opt.flow_opt[flow_id].flow_en,
                     flow_conf.flow_conf.ptp.eth1_opt.flow_opt[flow_id].addr_match_mode,
                     flow_conf.flow_conf.ptp.eth1_opt.flow_opt[flow_id].addr_match_select);
    
                if (encap_type == MESA_PHY_TS_ENCAP_ETH_IP_PTP) {
                    flow_conf.flow_conf.ptp.ip1_opt.flow_opt[flow_id].flow_en = true;
                    flow_conf.flow_conf.ptp.ip1_opt.comm_opt.ip_mode = MESA_PHY_TS_IP_VER_4;
                    flow_conf.flow_conf.ptp.ip1_opt.comm_opt.dport_val = PTP_EVENT_PORT;
                    flow_conf.flow_conf.ptp.ip1_opt.comm_opt.dport_mask = 0xffff;
                    flow_conf.flow_conf.ptp.ip1_opt.comm_opt.sport_val = 0;
                    flow_conf.flow_conf.ptp.ip1_opt.comm_opt.sport_mask = 0;
                    /* multicast configuration */
                    flow_conf.flow_conf.ptp.ip1_opt.flow_opt[flow_id].match_mode = MESA_PHY_TS_IP_MATCH_DEST;
                    flow_conf.flow_conf.ptp.ip1_opt.flow_opt[flow_id].ip_addr.ipv4.addr = 0;
                    flow_conf.flow_conf.ptp.ip1_opt.flow_opt[flow_id].ip_addr.ipv4.mask = 0; /* match any IP address */
                    T_DG(VTSS_TRACE_GRP_PHY_TS,"ip: flow_opt[%d]: channel_map %d, flow_en %d, match_mode %d, ip_addr %X(%X)",
                         flow_id,
                         flow_conf.channel_map[flow_id],
                         flow_conf.flow_conf.ptp.ip1_opt.flow_opt[flow_id].flow_en,
                         flow_conf.flow_conf.ptp.ip1_opt.flow_opt[flow_id].match_mode,
                         flow_conf.flow_conf.ptp.ip1_opt.flow_opt[flow_id].ip_addr.ipv4.addr,
                         flow_conf.flow_conf.ptp.ip1_opt.flow_opt[flow_id].ip_addr.ipv4.mask);
                }
            }
#ifdef PHY_DATA_DUMP
            T_WG(VTSS_TRACE_GRP_PHY_TS,"conf dump after:");
            dump_conf(&flow_conf);
#endif
            T_NG(VTSS_TRACE_GRP_PHY_TS,"set ing engine conf: port %d, id %d", port, _phy_ts_rules[inst][port].port.engine_id);













            PTP_RC(mesa_phy_ts_ingress_engine_conf_set(API_INST_DEFAULT,
                    port,
                    _phy_ts_rules[inst][port].port.engine_id,
                    &flow_conf));















            PTP_RC(mesa_phy_ts_egress_engine_conf_get(API_INST_DEFAULT,
                    port,
                    _phy_ts_rules[inst][port].port.engine_id,
                    &flow_conf));

#ifdef PHY_DATA_DUMP
            T_WG(VTSS_TRACE_GRP_PHY_TS,"conf dump before:");
            dump_conf(&flow_conf);
#endif
            /* modify flow configuration */
            flow_conf.eng_mode = true;
    
            for (flow_id = _phy_ts_rules[inst][port].port.flow_id_low; flow_id <= _phy_ts_rules[inst][port].port.flow_id_high; flow_id++) {
                flow_conf.channel_map[flow_id] = port_data[port].topo.channel_id == 0 ? MESA_PHY_TS_ENG_FLOW_VALID_FOR_CH0 : MESA_PHY_TS_ENG_FLOW_VALID_FOR_CH1;
                flow_conf.flow_conf.ptp.eth1_opt.flow_opt[flow_id].flow_en = true;
                flow_conf.flow_conf.ptp.eth1_opt.flow_opt[flow_id].addr_match_mode = MESA_PHY_TS_ETH_ADDR_MATCH_ANY_MULTICAST | MESA_PHY_TS_ETH_ADDR_MATCH_ANY_UNICAST;
                flow_conf.flow_conf.ptp.eth1_opt.flow_opt[flow_id].addr_match_select = MESA_PHY_TS_ETH_MATCH_DEST_ADDR;
                flow_conf.flow_conf.ptp.eth1_opt.flow_opt[flow_id].vlan_check = false;
                T_DG(VTSS_TRACE_GRP_PHY_TS,"eth: flow_opt[%d]: channel_map %d, flow_en %d, match_mode %d, match:_sel %d",
                     flow_id,
                     flow_conf.channel_map[flow_id],
                     flow_conf.flow_conf.ptp.eth1_opt.flow_opt[flow_id].flow_en,
                     flow_conf.flow_conf.ptp.eth1_opt.flow_opt[flow_id].addr_match_mode,
                     flow_conf.flow_conf.ptp.eth1_opt.flow_opt[flow_id].addr_match_select);
    
                if (encap_type == MESA_PHY_TS_ENCAP_ETH_IP_PTP) {
                    flow_conf.flow_conf.ptp.ip1_opt.flow_opt[flow_id].flow_en = true;
                    flow_conf.flow_conf.ptp.ip1_opt.comm_opt.ip_mode = MESA_PHY_TS_IP_VER_4;
                    flow_conf.flow_conf.ptp.ip1_opt.comm_opt.dport_val = PTP_EVENT_PORT;
                    flow_conf.flow_conf.ptp.ip1_opt.comm_opt.dport_mask = 0xffff;
                    flow_conf.flow_conf.ptp.ip1_opt.comm_opt.sport_val = 0;
                    flow_conf.flow_conf.ptp.ip1_opt.comm_opt.sport_mask = 0;
                    /* multicast configuration */
                    flow_conf.flow_conf.ptp.ip1_opt.flow_opt[flow_id].match_mode = MESA_PHY_TS_IP_MATCH_DEST;
                    flow_conf.flow_conf.ptp.ip1_opt.flow_opt[flow_id].ip_addr.ipv4.addr = 0;
                    flow_conf.flow_conf.ptp.ip1_opt.flow_opt[flow_id].ip_addr.ipv4.mask = 0; /* match any IP address */
                    T_DG(VTSS_TRACE_GRP_PHY_TS,"ip: flow_opt[%d]: channel_map %d, flow_en %d, match_mode %d, ip_addr %X(%X)",
                         flow_id,
                         flow_conf.channel_map[flow_id],
                         flow_conf.flow_conf.ptp.ip1_opt.flow_opt[flow_id].flow_en,
                         flow_conf.flow_conf.ptp.ip1_opt.flow_opt[flow_id].match_mode,
                         flow_conf.flow_conf.ptp.ip1_opt.flow_opt[flow_id].ip_addr.ipv4.addr,
                         flow_conf.flow_conf.ptp.ip1_opt.flow_opt[flow_id].ip_addr.ipv4.mask);
                }
            }

#ifdef PHY_DATA_DUMP
            T_WG(VTSS_TRACE_GRP_PHY_TS,"egr conf dump after:");
            dump_conf(&flow_conf);
#endif
            T_NG(VTSS_TRACE_GRP_PHY_TS,"set eg engine conf: %d", _phy_ts_rules[inst][port].port.engine_id);













            PTP_RC(mesa_phy_ts_egress_engine_conf_set(API_INST_DEFAULT,
                    port,
                    _phy_ts_rules[inst][port].port.engine_id,
                    &flow_conf));


        /* set up TX FIFO signature */







            PTP_RC(mesa_phy_ts_fifo_sig_set(API_INST_DEFAULT, port, my_sig_mask));

            T_NG(VTSS_TRACE_GRP_PHY_TS,"tx_fifo_signature set, port: %d, mask: %x", port, my_sig_mask);

            /* set up actions */
            mesa_phy_ts_engine_action_t ptp_action;













            PTP_RC(mesa_phy_ts_ingress_engine_action_get(API_INST_DEFAULT,
                    port,
                    _phy_ts_rules[inst][port].port.engine_id,
                    &ptp_action));

#ifdef PHY_DATA_DUMP
            T_WG(VTSS_TRACE_GRP_PHY_TS,"action before:");
            dump_ptp_action(&ptp_action);
#endif
            T_NG(VTSS_TRACE_GRP_PHY_TS,"get ing action: %d", _phy_ts_rules[inst][port].port.engine_id);
            // in a BC the actions for the two ports can be different (P2P/E2E), therefore action[0] is used for channel 0 and action[1] is used for channel 1
            // in a TC the actions for the two ports are always the same, therefore action[0] is used for both channels, and action[1] is disabled
            // also an E2E TC uses 4 out of 6 actions in the PHY, therefore the actions needs to be shared between the two channels
            ptp_action.action_ptp = true;
            if (_phy_ts_rules[inst][port].port.flow_id_low == 0) {
                _phy_ts_rules[inst][port].port.action_idx = 0;
            } else {
                _phy_ts_rules[inst][port].port.action_idx = 1;
                if (_phy_ts_rules[inst][port].deviceType == VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT ||
                    _phy_ts_rules[inst][port].deviceType == VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT)
                {
                    _phy_ts_rules[inst][port].port.action_idx = 0;
                    ptp_action.action.ptp_conf[1].enable = false;
                }
    
            }
            ai = _phy_ts_rules[inst][port].port.action_idx;
            T_IG(VTSS_TRACE_GRP_PHY_TS, "Enable PTP action: %d in engine_id %d",
                                        _phy_ts_rules[inst][port].port.action_idx,
                                        _phy_ts_rules[inst][port].port.engine_id);
    
            ptp_action.action.ptp_conf[ai].enable = true;
            ptp_action.action.ptp_conf[ai].channel_map |= port_data[port].topo.channel_id == 0 ? MESA_PHY_TS_ENG_FLOW_VALID_FOR_CH0 : MESA_PHY_TS_ENG_FLOW_VALID_FOR_CH1;
    
            ptp_action.action.ptp_conf[ai].ptp_conf.range_en = true;
            ptp_action.action.ptp_conf[ai].ptp_conf.domain.range.upper = 0xff;
            ptp_action.action.ptp_conf[ai].ptp_conf.domain.range.lower = 0x00;
            if (_phy_ts_rules[inst][port].deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND ||
                _phy_ts_rules[inst][port].deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY ||
                _phy_ts_rules[inst][port].deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY ||
                _phy_ts_rules[inst][port].deviceType == VTSS_APPL_PTP_DEVICE_BC_FRONTEND)
            {
                ptp_action.action.ptp_conf[ai].clk_mode = _phy_ts_rules[inst][port].twoStepFlag ? MESA_PHY_TS_PTP_CLOCK_MODE_BC2STEP : MESA_PHY_TS_PTP_CLOCK_MODE_BC1STEP;
            } else {
                ptp_action.action.ptp_conf[ai].clk_mode = _phy_ts_rules[inst][port].twoStepFlag ? MESA_PHY_TS_PTP_CLOCK_MODE_TC2STEP : MESA_PHY_TS_PTP_CLOCK_MODE_TC1STEP;
            }
    
            ptp_action.action.ptp_conf[ai].delaym_type = config_data.conf[ptp_inst].port_config[port].delayMechanism == DELAY_MECH_E2E ? MESA_PHY_TS_PTP_DELAYM_E2E : MESA_PHY_TS_PTP_DELAYM_P2P;
            ptp_action.action.ptp_conf[ai].cf_update = (ports_org_time[ptp_inst] == false);
            if (mesa_cap_ts_delay_req_auto_resp) {  // in auto resp mode the PHY must insert received timestamp into the originTimestamp field
                ptp_action.action.ptp_conf[ai].delay_req_recieve_timestamp = true;
            } else {
                ptp_action.action.ptp_conf[ai].delay_req_recieve_timestamp = false;
            }
            T_DG(VTSS_TRACE_GRP_PHY_TS,"action[%d]: range_en %d, upper %d, lower %d, clk_mode %d, delaym_type %d, cf_update %d",
                 ai,
                 ptp_action.action.ptp_conf[ai].ptp_conf.range_en,
                 ptp_action.action.ptp_conf[ai].ptp_conf.domain.range.upper,
                 ptp_action.action.ptp_conf[ai].ptp_conf.domain.range.lower,
                 ptp_action.action.ptp_conf[ai].clk_mode,
                 ptp_action.action.ptp_conf[ai].delaym_type,
                 ptp_action.action.ptp_conf[ai].cf_update);
#ifdef PHY_DATA_DUMP
            T_WG(VTSS_TRACE_GRP_PHY_TS,"action after:");
            dump_ptp_action(&ptp_action);
#endif







            PTP_RC(mesa_phy_ts_ingress_engine_action_set(API_INST_DEFAULT, port, _phy_ts_rules[inst][port].port.engine_id, &ptp_action));

            T_NG(VTSS_TRACE_GRP_PHY_TS,"set ing action: %d", _phy_ts_rules[inst][port].port.engine_id);








            PTP_RC(mesa_phy_ts_egress_engine_action_set(API_INST_DEFAULT, port, _phy_ts_rules[inst][port].port.engine_id, &ptp_action));

            T_NG(VTSS_TRACE_GRP_PHY_TS,"set eg action: %d", _phy_ts_rules[inst][port].port.engine_id);

        } else {
    
            if (_phy_ts_rules[inst][port].port.action_idx != VTSS_PHY_PTP_ACTION_INVALID &&
                _phy_ts_rules[inst][port].port.engine_id != MESA_PHY_TS_ENGINE_ID_INVALID) {
                mesa_phy_ts_engine_action_t ptp_action;
                PTP_RC(mesa_phy_ts_ingress_engine_action_get(API_INST_DEFAULT,
                        port,
                        _phy_ts_rules[inst][port].port.engine_id,
                        &ptp_action));
#ifdef PHY_DATA_DUMP
                T_WG(VTSS_TRACE_GRP_PHY_TS,"clear action before:");
                dump_ptp_action(&ptp_action);
#endif

                ptp_action.action.ptp_conf[_phy_ts_rules[inst][port].port.action_idx].channel_map &= port_data[port].topo.channel_id == 0 ? ~MESA_PHY_TS_ENG_FLOW_VALID_FOR_CH0 : ~MESA_PHY_TS_ENG_FLOW_VALID_FOR_CH1;
                if (ptp_action.action.ptp_conf[_phy_ts_rules[inst][port].port.action_idx].channel_map == 0) {
                    ptp_action.action.ptp_conf[_phy_ts_rules[inst][port].port.action_idx].enable = false;
                }
                PTP_RC(mesa_phy_ts_ingress_engine_action_set(API_INST_DEFAULT, port, _phy_ts_rules[inst][port].port.engine_id, &ptp_action));
                PTP_RC(mesa_phy_ts_egress_engine_action_set(API_INST_DEFAULT, port, _phy_ts_rules[inst][port].port.engine_id, &ptp_action));
#ifdef PHY_DATA_DUMP
                T_WG(VTSS_TRACE_GRP_PHY_TS, "clear action after:");
                dump_ptp_action(&ptp_action);
#endif
                T_IG(VTSS_TRACE_GRP_PHY_TS, "Clear PTP action: %d in engine_id %d",
                                            _phy_ts_rules[inst][port].port.action_idx,
                                            _phy_ts_rules[inst][port].port.engine_id);
                _phy_ts_rules[inst][port].port.action_idx = VTSS_PHY_PTP_ACTION_INVALID;
            }
            /* not used by this port: is it used by the shared port ? */
            engine_free = true;
            if (port_data[port].topo.port_shared) {
                shared_port = port_data[port].topo.shared_port_no;
                if (_phy_ts_rules[inst][shared_port].port.phy_ts_port) {
                    engine_free = false;
                }
            } else {
                shared_port = 0;
            }
            if (engine_free && _phy_ts_rules[inst][port].port.engine_id != MESA_PHY_TS_ENGINE_ID_INVALID) { /* if not used any more */









                PTP_RC(mesa_phy_ts_ingress_engine_clear(API_INST_DEFAULT, port, _phy_ts_rules[inst][port].port.engine_id));
                PTP_RC(mesa_phy_ts_egress_engine_clear(API_INST_DEFAULT, port, _phy_ts_rules[inst][port].port.engine_id));

                tod_phy_eng_free(port, _phy_ts_rules[inst][port].port.engine_id);
                T_IG(VTSS_TRACE_GRP_PHY_TS,"eng free id: %d for port %d", _phy_ts_rules[inst][port].port.engine_id, port);
                _phy_ts_rules[inst][port].port.engine_id = MESA_PHY_TS_ENGINE_ID_INVALID;
            }
            if (engine_free && port_data[port].topo.port_shared) {
                _phy_ts_rules[inst][shared_port].port.engine_id = MESA_PHY_TS_ENGINE_ID_INVALID;
            }
        }
#ifdef PHY_DATA_DUMP
        phy_ts_dump();
#endif
    }

    return MESA_RC_OK;
}


void vtss_1588_org_time_option_get(int instance, u16 portnum, bool *org_time_option)
{
    *org_time_option = false;
    if (instance < PTP_CLOCK_INSTANCES) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            if (config_data.conf[instance].clock_init.cfg.clock_domain == 0) {
                *org_time_option = ports_org_time[instance];
            } else {
                // the HW holds clock_domain 0, therefore we don't use origin time in the sync messages when clock_domain != 0
                *org_time_option = false;
            }
        }
        T_DG(_I, "portnum: %d inst %d, org_time %d", portnum, instance, *org_time_option);
    }
}


/****************************************************************************/
/*  port state functions                                                    */
/****************************************************************************/

/*
 * Port in-sync state change indication
 */
static void ptp_in_sync_callback(mesa_port_no_t port_no, BOOL in_sync)
{
    PTP_CORE_LOCK_SCOPE();

    if (PTP_READY()) {
        port_data[port_no].port_ts_in_sync_tmp = in_sync;
        if (port_data[port_no].topo.port_ts_in_sync != (in_sync || ptp_time_settling)) {
            port_data[port_no].topo.port_ts_in_sync = in_sync || ptp_time_settling;
            for (int i = 0; i < PTP_CLOCK_INSTANCES; i++) {
                if (vtss_ptp_port_linkstate(ptp_global.ptpi[i], ptp_l2port_to_api(port_no),
                                            port_data[port_no].link_state && port_data[port_no].topo.port_ts_in_sync && port_data[port_no].vlan_forw[i]) == VTSS_RC_OK)
                {
                    T_IG(_I, "port_no: %d %s", ptp_l2port_to_api(port_no), in_sync ? "in_sync" : "out_of_sync");
                }
                else {
                    T_WG(_I, "invalid port_no: %d", ptp_l2port_to_api(port_no));
                }
                if (vtss_ptp_p2p_state(ptp_global.ptpi[i], ptp_l2port_to_api(port_no),
                                            port_data[port_no].link_state && port_data[port_no].topo.port_ts_in_sync) != VTSS_RC_OK) {
                    T_WG(_I, "invalid port_no: %d", ptp_l2port_to_api(port_no));
                }
            }
        }
    } else {
        T_WG(_I, "PTP not ready");
    }
}

/*
 * Port filter change indication
 */
static void ptp_port_filter_change_callback(int instance, mesa_port_no_t port_no, bool forward)
{
    port_data[port_no].vlan_forw[instance] = forward;
    if (vtss_ptp_port_linkstate(ptp_global.ptpi[instance], ptp_l2port_to_api(port_no),
                                port_data[port_no].link_state && port_data[port_no].topo.port_ts_in_sync && port_data[port_no].vlan_forw[instance]) == VTSS_RC_OK)
    {
        T_IG(_I, "port_no: %d filter %s", ptp_l2port_to_api(port_no), port_data[port_no].vlan_forw[instance] ? "Forward" : "Discard");
    } else {
        T_WG(_I, "invalid port_no: %d", ptp_l2port_to_api(port_no));
    }
}

/*
 * Port state change indication
 */
static void ptp_port_state_change_callback(vtss_isid_t isid, mesa_port_no_t port_no, port_info_t *info)
{
    PTP_CORE_LOCK_SCOPE();
    
    if (PTP_READY()) {
        if (!info->stack) {
            port_data[port_no].link_state = info->link;
            if (mesa_cap_phy_ts) {
                port_data_conf_set(port_no, info->link); /* check if the port has a 1588 PHY */
            }
            T_IG(_I, "port_no: %d link %s, speed %d", ptp_l2port_to_api(port_no), info->link ? "up" : "down", info->speed);
            for (int i = 0; i < PTP_CLOCK_INSTANCES; i++) {
                if (VTSS_RC_OK != ptp_phy_ts_update(i, port_no)) {
                    T_IG(_I, "phy ts initialization failed: %d", ptp_l2port_to_api(port_no));
                }
                if (vtss_ptp_port_linkstate(ptp_global.ptpi[i], ptp_l2port_to_api(port_no),
                                            port_data[port_no].link_state && port_data[port_no].topo.port_ts_in_sync && port_data[port_no].vlan_forw[i]) == VTSS_RC_OK)
                {
                    T_IG(_I, "port_no: %d link %s", ptp_l2port_to_api(port_no), info->link ? "up" : "down");
                } else {
                    T_WG(_I, "invalid port_no: %d", ptp_l2port_to_api(port_no));
                }
                if (vtss_ptp_p2p_state(ptp_global.ptpi[i], ptp_l2port_to_api(port_no), port_data[port_no].link_state && port_data[port_no].topo.port_ts_in_sync) != VTSS_RC_OK) {
                    T_WG(_I, "invalid port_no: %d", ptp_l2port_to_api(port_no));
                }
            }
        } else {
            T_WG(_I, "PTP does not support stackables (%d)", info->stack);
        }
    } else {
        /* port state may change before PTP process is ready */
        T_IG(_I, "PTP not ready");
    }
}

/*
 * Set initial port link down state
 */
static void ptp_port_link_state_initial(int instance)
{
    mesa_port_no_t portidx;
    vtss_appl_port_status_t ps;
    bool link_state, p2p_state;
    if (!msg_switch_exists(VTSS_ISID_START)) {
        T_NG(_I, "switch %d does not exist", VTSS_ISID_START);
        return;
    }
    for (portidx = 0; portidx < config_data.conf[instance].clock_init.numberPorts; portidx++) {
        if (port_ctrl_status_get(VTSS_ISID_START, portidx, &ps) == VTSS_OK) {
            if (port_data[portidx].link_state != ps.status.link) {
                port_data[portidx].link_state = ps.status.link;
                if (mesa_cap_phy_ts) {
                    port_data_conf_set(portidx, ps.status.link); /* check if the port has a 1588 PHY */
                }
            }
            link_state = port_data[portidx].link_state && port_data[portidx].topo.port_ts_in_sync && port_data[portidx].vlan_forw[instance];
            p2p_state = port_data[portidx].link_state && port_data[portidx].topo.port_ts_in_sync;
        } else {
            // virtual port
            ps.status.link = true;
            link_state = true;
            p2p_state = true;
        }
        if (vtss_ptp_port_linkstate(ptp_global.ptpi[instance], ptp_l2port_to_api(portidx),
                                    link_state) == VTSS_RC_ERROR)
        {
            T_EG(_I, "invalid port_no: %d", ptp_l2port_to_api(portidx));
        }
        if (vtss_ptp_p2p_state(ptp_global.ptpi[instance], ptp_l2port_to_api(portidx),
                    p2p_state) != VTSS_RC_OK) {
            T_WG(_I, "invalid port_no: %d,numberPoets %d", ptp_l2port_to_api(portidx), ptp_global.ptpi[instance]->clock_init->numberPorts);
        }
    }
}

static vtss_ptp_clock_mode_t my_mode = VTSS_PTP_CLOCK_FREERUN;

void vtss_local_clock_mode_set(vtss_ptp_clock_mode_t mode)
{
    int i;
    mesa_port_no_t portidx;
    if (mode != my_mode) {
        my_mode = mode;

        for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
            for (portidx = 0; portidx < config_data.conf[i].clock_init.numberPorts; portidx++) {
                if (vtss_ptp_port_internal_linkstate(ptp_global.ptpi[i], ptp_l2port_to_api(portidx)) == VTSS_RC_ERROR) {
                    T_EG(_I, "invalid port_no: %d", ptp_l2port_to_api(portidx));
                }
            }
        }
        T_IG(_C, "Clock mode %d", my_mode);
    }
}
void vtss_local_clock_mode_get(vtss_ptp_clock_mode_t *mode)
{
    *mode = my_mode;
}

static bool one_pps_los;
static bool one_pps_input_active;

#define ONE_SEC_PERIOD  (1000/PTP_SCHEDULER_RATE)

static void vtss_one_pps_external_input_timer(bool clear)
{
    static int one_pps_timer = 0, one_pps_period = 0;
    if (clear) {
        one_pps_timer = 0;
    } else {
        if (one_pps_input_active && !one_pps_los) {
            one_pps_timer++;
            if (one_pps_timer > 2*ONE_SEC_PERIOD) {
                one_pps_period = 0;
                one_pps_los = true;
            }
        }
        if (one_pps_input_active && one_pps_los) {
            one_pps_timer++;
            one_pps_period++;
            if (one_pps_period > 3*ONE_SEC_PERIOD && one_pps_timer < 1*ONE_SEC_PERIOD) {
                one_pps_timer = 0;
                one_pps_los = false;
                one_pps_external_input_servo_init(true);
            }
        }
    }
}

/*
 * 1 pps input servo functions
 */
#define MEASURING 0
#define PAUSING 1
#define PPS_MEASURE_PERIOD 10
#define PPS_PAUSE_PERIOD 3

static i32 one_pps_servo_pps_mean;
static i32 one_pps_servo_count;
static int one_pps_servo_controller_state;
static i32 one_pps_servo_ratio;
static bool one_pps_first_time;

static void one_pps_external_input_servo_init(bool start)
{
    one_pps_servo_pps_mean = 0;
    one_pps_servo_count = 0;
    one_pps_servo_controller_state = PAUSING;
    one_pps_servo_ratio = 0;
    if (start) {
        vtss_local_clock_ratio_clear(0);
    }
    one_pps_first_time = true;
    one_pps_input_active = start;
}

static void one_pps_external_input_servo_action(i32 freq_offset)
{
    if (one_pps_first_time) {
        one_pps_first_time = false; /* ignore the first reading (also if the master time has changed), as it will have some random value */
        one_pps_servo_pps_mean = 0;
        one_pps_servo_count = 0;
        one_pps_servo_controller_state = PAUSING;
        return;
    }
    if (!one_pps_los) {
        switch (one_pps_servo_controller_state) {
        case MEASURING:
            one_pps_servo_pps_mean += freq_offset;
            ++one_pps_servo_count;
            if (one_pps_servo_count == PPS_MEASURE_PERIOD) {
                one_pps_servo_ratio -= one_pps_servo_pps_mean/one_pps_servo_count;
                vtss_local_clock_ratio_set(((i64)one_pps_servo_ratio << 16) / 10, 0);
                //T_DG(VTSS_TRACE_GRP_1_PPS, "pps_mean: %d, ratio: %d", one_pps_servo_pps_mean/one_pps_servo_count, one_pps_servo_ratio);
                one_pps_servo_pps_mean = 0;
                one_pps_servo_count = 0;
                one_pps_servo_controller_state = PAUSING;
            }
            break;
        case PAUSING:
            ++one_pps_servo_count;
            if (one_pps_servo_count == PPS_PAUSE_PERIOD) {
                one_pps_servo_count = 0;
                one_pps_servo_controller_state = MEASURING;
            }
            break;
        default:
            T_WG(VTSS_TRACE_GRP_1_PPS, "invalid controller state: %d", one_pps_servo_controller_state);
            break;

        }
    }
    vtss_one_pps_external_input_timer(true);
}

bool calib_1pps_initiate = false;
bool calib_1pps_enabled = false;

static vtss::Timer pps_msg_timer;
static ptp_one_pps_slave_data_t alt_pin;

static void one_pps_external_input_interrupt_handler(meba_event_t source_id, u32 instance_id)
{
    {
        PTP_CORE_LOCK_SCOPE();
        
        T_NG(VTSS_TRACE_GRP_1_PPS, "One sec external clock event: source_id %d, instance_id %u", source_id, instance_id);
        if (mesa_cap_ts_has_alt_pin) {
            if ((mesa_cap_misc_chip_family == MESA_CHIP_FAMILY_SERVAL || mesa_cap_misc_chip_family == MESA_CHIP_FAMILY_OCELOT) && mesa_cap_ts_ptp_rs422) {
                u32 turnaround_latency;
                mesa_timestamp_t saved_tod;
    
                PTP_RC(mesa_ts_alt_clock_saved_timeofday_get(NULL, &saved_tod));
                /* main mode */
                if (config_data.init_ext_clock_mode_rs422.mode == VTSS_PTP_RS422_MAIN_AUTO || config_data.init_ext_clock_mode_rs422.mode == VTSS_PTP_RS422_CALIB) {
                    PTP_RC(mesa_ts_alt_clock_saved_get(NULL, &turnaround_latency));
                    T_IG(VTSS_TRACE_GRP_1_PPS, "Saved turn around or onesec counter value: %u", turnaround_latency);
                    // save calculated cable delay
                    if (config_data.init_ext_clock_mode_rs422.mode == VTSS_PTP_RS422_CALIB) {
                        static int calib_iteration = 0;
                        static u32 turnaround_latency_accumulated = 0;

                        if (calib_1pps_initiate) {
                           calib_iteration = 0;
                           turnaround_latency_accumulated = 0;
                           calib_1pps_initiate = false;
                        }
                        calib_iteration++;

                        turnaround_latency_accumulated += turnaround_latency;

                        if (calib_iteration == 10) {
                            config_data.init_ext_clock_mode_rs422.delay = turnaround_latency_accumulated / calib_iteration;  /* In calibration mode, store the raw measurement of the round-trip delay */
                            calib_1pps_enabled = false;
                        }
                    } else {
                        config_data.init_ext_clock_mode_rs422.delay = turnaround_latency / 2 + 19; /* 19 ns is the additional delay in the path to the SUB module */
                    }
                    // wait 40 msec before sending timing message
                    if (vtss_timer_start(&pps_msg_timer) != VTSS_RC_OK) {
                        T_EG(VTSS_TRACE_GRP_CLOCK, "Unable to start pps_msg_timer");
                    }
                }
                /* sub mode */
                if (config_data.init_ext_clock_mode_rs422.mode == VTSS_PTP_RS422_SUB) {
                    T_DG(VTSS_TRACE_GRP_1_PPS, "call slave with t2");
                    ptp_external_input_slave_function(&alt_pin, 0, &saved_tod, false);
                }
            }
        }
        if (mesa_cap_ts_use_external_input_servo) {
            i32 offset;
    
            if (config_data.init_ext_clock_mode.one_pps_mode ==  VTSS_APPL_PTP_ONE_PPS_INPUT) {
                PTP_RC(mesa_ts_freq_offset_get(0,&offset));
                one_pps_external_input_servo_action(offset);
            }
        }
    }
    PTP_RC(vtss_interrupt_source_hook_set(VTSS_MODULE_ID_PTP, one_pps_external_input_interrupt_handler, source_id, INTERRUPT_PRIORITY_NORMAL));
}

static void ptp_timer_expired(vtss::Timer *timer)
{
    vtss_flag_setbits(&ptp_global_control_flags, CTLFLAG_PTP_TIMER);
}

static vtss::Timer ptp_timer;

static void init_timer(void) {
    // Create ptp timer
#if defined (VTSS_MAX_LOG_MESSAGE_RATE)
#if (VTSS_MAX_LOG_MESSAGE_RATE >= -7 && VTSS_MAX_LOG_MESSAGE_RATE <= 0)
    tick_factor = 1 << (VTSS_MAX_LOG_MESSAGE_RATE + 7);
#else
    tick_factor = 1;
#endif
#else
    tick_factor = 1;
#endif
    vtss_ptp_timer_initialize();
    ptp_timer.set_repeat(true);
    ptp_timer.set_period(vtss::microseconds(tick_factor*TickTimeNs/1000));     /* 7813 us = 1/128 sec */
    ptp_timer.callback    = ptp_timer_expired;
    ptp_timer.modid       = VTSS_MODULE_ID_PTP;
    T_IG(VTSS_TRACE_GRP_PTP_BASE_TIMER, "tick_factor %d, period_us %d", tick_factor, tick_factor*TickTimeNs/1000);
    if (vtss_timer_start(&ptp_timer) != VTSS_RC_OK) {
        T_EG(VTSS_TRACE_GRP_PTP_BASE_TIMER, "Unable to start ptp timer");
    }
}

/*
 * TOD Synchronization 1 pps pulse update handler
 */
static void one_pps_pulse_interrupt_handler(meba_event_t source_id, u32 instance_id)
{
    PTP_CORE_LOCK_SCOPE();

    T_NG(VTSS_TRACE_GRP_1_PPS, "One sec internal clock event: source_id %d, instance_id %u", source_id, instance_id);

    if (config_data.init_ext_clock_mode_rs422.mode == VTSS_PTP_RS422_MAIN_MAN) {
        // wait 40 msec before sending timing message
        if (vtss_timer_start(&pps_msg_timer) != VTSS_RC_OK) {
            T_EG(VTSS_TRACE_GRP_CLOCK, "Unable to start pps_msg_timer");
        }

        PTP_RC(vtss_interrupt_source_hook_set(VTSS_MODULE_ID_PTP,
                                              one_pps_pulse_interrupt_handler,
                                              source_id,
                                              INTERRUPT_PRIORITY_NORMAL));
    }
}

static void one_pps_msg_timer_expired(vtss::Timer *timer)
{
    PTP_CORE_LOCK_SCOPE();

    mesa_timestamp_t t_next_pps;
    T_DG(VTSS_TRACE_GRP_1_PPS, "One sec message timeout");

    if (config_data.init_ext_clock_mode_rs422.mode == VTSS_PTP_RS422_MAIN_MAN ||
        config_data.init_ext_clock_mode_rs422.mode == VTSS_PTP_RS422_MAIN_AUTO ||
        config_data.init_ext_clock_mode_rs422.mode == VTSS_PTP_RS422_CALIB)
    {
        PTP_RC(mesa_ts_timeofday_prev_pps_get(NULL, &t_next_pps));
        t_next_pps.nanoseconds = config_data.init_ext_clock_mode_rs422.delay; /* Delay is manually configured or calculated */
        if (config_data.init_ext_clock_mode_rs422.proto >= VTSS_PTP_RS422_PROTOCOL_SER_POLYT && config_data.init_ext_clock_mode_rs422.proto <= VTSS_PTP_RS422_PROTOCOL_SER_RMC) {
            ptp_1pps_msg_send(&t_next_pps, config_data.init_ext_clock_mode_rs422.proto);
        } else if (config_data.init_ext_clock_mode_rs422.proto == VTSS_PTP_RS422_PROTOCOL_PIM) {
            ptp_pim_1pps_msg_send(config_data.init_ext_clock_mode_rs422.port, &t_next_pps);
        }
    }
}

/*
 * timer used for calling Time stamp age function
 */
static void init_pps_msg_timer(void) {
    pps_msg_timer.set_repeat(FALSE);
    pps_msg_timer.set_period(vtss::milliseconds(40));
    pps_msg_timer.callback    = one_pps_msg_timer_expired;
    pps_msg_timer.modid       = VTSS_MODULE_ID_PTP;
    T_IG(VTSS_TRACE_GRP_CLOCK, "pps_msg_timer initialized");
}


// Forward declaration of mesa_rc udp_rx_filter_manage
static mesa_rc udp_rx_filter_manage(bool init, uint clockId, bool usesUDP);

// Function for updating the UDP rx filter (i.e. installing/deinstalling depending on whether one or more PTP clocks use UDP or not).
/*lint -sem(udp_rx_filter_update, thread_protected) */
static mesa_rc udp_rx_filter_update(uint clockId, bool usesUDP)
{
    mesa_rc rc;
    bool init = 0;

    rc = udp_rx_filter_manage(init, clockId, usesUDP);
    return rc;
}

static mesa_rc udp_rx_filter_manage(bool init, uint clockId, bool usesUDP)
{
    static packet_rx_filter_t udp_rx_filter;
    static void *filter_id_udp = NULL;
    static CapArray<bool, VTSS_APPL_CAP_PTP_CLOCK_CNT> clock_uses_udp;  // Note: All elements intialized to 0 i.e. FALSE by constructor.
    bool some_clock_uses_udp;
    mesa_rc rc = VTSS_RC_ERROR;
    int n;

    if (init == 1) {
        // Setup udp_rx_filter for use later
        packet_rx_filter_init(&udp_rx_filter);
        udp_rx_filter.modid = VTSS_MODULE_ID_PTP;
        udp_rx_filter.match = PACKET_RX_FILTER_MATCH_UDP_DST_PORT; // ethertype, IP protocol and UDP port match filter
        udp_rx_filter.udp_dst_port_min = PTP_EVENT_PORT;
        udp_rx_filter.udp_dst_port_max = PTP_GENERAL_PORT;
        udp_rx_filter.prio  = PACKET_RX_FILTER_PRIO_HIGH;
        udp_rx_filter.contxt = &ptp_global.ptpi;
        udp_rx_filter.cb     = packet_rx;
        udp_rx_filter.etype = ip_ether_type;
        udp_rx_filter.ip_proto = IPPROTO_UDP;
        udp_rx_filter.thread_prio = VTSS_THREAD_PRIO_HIGHER;

        // Install call back function to be used by vtss_ptp_port_ena and vtss_ptp_port_dis to update the UDP rx filter.
        vtss_ptp_install_udp_rx_filter_update_cb(udp_rx_filter_update);

        rc = VTSS_RC_OK;
    }
    else {
        if ((usesUDP == 1) && (clock_uses_udp[clockId] == 0)) {
            clock_uses_udp[clockId] = 1;
            if (filter_id_udp != NULL) {
                rc = packet_rx_filter_change(&udp_rx_filter, &filter_id_udp);
            }
            else {
                rc = packet_rx_filter_register(&udp_rx_filter, &filter_id_udp);
            }
            ptp_socket_init();
        }
        else if ((usesUDP == 0) && (clock_uses_udp[clockId] == 1)) {
            clock_uses_udp[clockId] = 0;
            some_clock_uses_udp = 0;
            for (n = 0; n < PTP_CLOCK_INSTANCES; n++) some_clock_uses_udp = some_clock_uses_udp || (clock_uses_udp[n] != 0);
            if (filter_id_udp != NULL) {
                if (some_clock_uses_udp != 0) {
                    rc = packet_rx_filter_change(&udp_rx_filter, &filter_id_udp);
                }
                else {
                    rc = packet_rx_filter_unregister(filter_id_udp);
                    if (rc == VTSS_RC_OK) filter_id_udp = NULL;
                }
            }
            else {
                T_E("Trying to change or unregister udp_rx_filter when no one is registered.");
                rc = VTSS_RC_ERROR;
            }
            ptp_socket_close();
        }
        else rc = VTSS_RC_OK;  // Nothing to do as port was already enabled / diabled.
    }
    return rc;
}

static mesa_rc udp_rx_filter_init()
{
    mesa_rc rc;
    bool init = 1;

    rc = udp_rx_filter_manage(init, 0, 0);
    return rc;
}

static mesa_rc ptp_ts_operation_mode_set(mesa_port_no_t port_no)
{
    mesa_rc rc = VTSS_RC_OK;
    if (mesa_cap_ts && mesa_cap_ts_internal_mode_supported) {
        mesa_ts_operation_mode_t mode;
        int i;
        if (port_data[port_no].topo.ts_feature != VTSS_PTP_TS_PTS) {
            mode.mode = MESA_TS_MODE_EXTERNAL;
            for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
                if (config_data.conf[i].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE && config_data.conf[i].port_config[port_no].portInternal) {
                    mode.mode = MESA_TS_MODE_INTERNAL;
                }
            }
            if (mesa_cap_ts_domain_cnt > 1) {
                mode.domain = port_data[port_no].port_domain;
                T_I("set ts_operation mode to %d, domain %d for port %d", mode.mode, mode.domain, iport2uport(port_no));
            } else {
                T_I("set ts_operation mode to %d, for port %d", mode.mode, iport2uport(port_no));
            }
            PTP_RC(mesa_ts_operation_mode_set(NULL, port_no, &mode));
            port_data[port_no].backplane_port = (mode.mode == MESA_TS_MODE_INTERNAL) ? true : false;
        } else {
            T_D("ts_operation mode unchanged for port %d", iport2uport(port_no));
        }
    }
    return rc;
}

/****************************************************************************
 * Module thread
 ****************************************************************************/

static void ptp_thread(vtss_addrword_t data)
{
    mesa_rc rc;
    packet_rx_filter_t rx_filter;
    static void *filter_id_ether = NULL;
    int i, j;
    mesa_restart_status_t status;
    bool cold = true;
    vtss_flag_value_t flags;
    vtss_tick_count_t wakeup;

    if (mesa_restart_status_get(NULL, &status) == VTSS_RC_OK) {
        if (status.restart != MESA_RESTART_COLD) {
            cold = false;
        }
    }

    if (mesa_cap_phy_ts) {
        /* wait until TOD module is ready */
        wakeup = vtss_current_time() + VTSS_OS_MSEC2TICK(100);
        while (!tod_ready()) {
            T_I("wait until TOD module is ready");
            (void)vtss_flag_timed_wait(&ptp_global_control_flags, 0xffff, VTSS_FLAG_WAITMODE_AND, wakeup);
            wakeup += VTSS_OS_MSEC2TICK(100);
        }
    }

    PTP_CORE_LOCK();
    /* initialize PTP timer system */
    init_timer();

    if (mesa_cap_ts_ptp_rs422) {
        init_pps_msg_timer();
    }

    if (mesa_cap_phy_ts) {
        /* Initialize PHY ts rules */
        phy_ts_rules_init();
    }

    /* Initialize port data */
    port_data_initialize();

    /* Read configuration */
    (void) conf_mgmt_mac_addr_get((uchar *) &ptp_global.sysmac, 0);
    T_I("Read sysmac %02x-%02x-%02x-%02x-%02x-%02x", ptp_global.sysmac.addr[0],
        ptp_global.sysmac.addr[1], ptp_global.sysmac.addr[1], ptp_global.sysmac.addr[3],
        ptp_global.sysmac.addr[4], ptp_global.sysmac.addr[5]);

    /* Local clock initialization */
    for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
        vtss_local_clock_initialize(i, cold, config_data.init_ext_clock_mode.adj_method);
    }

    PTP_RC(vtss_ip_if_callback_add(notify_ptp_ip_address));

    if (mesa_cap_ts) {
        T_I("external input int id %d", rs422_conf.ptp_rs422_ldsv_int_id);
        PTP_RC(vtss_interrupt_source_hook_set(VTSS_MODULE_ID_PTP,
                                              one_pps_external_input_interrupt_handler,
                                              (meba_event_t)rs422_conf.ptp_rs422_ldsv_int_id,
                                              INTERRUPT_PRIORITY_NORMAL));
    }

#if defined(VTSS_SW_OPTION_MEP)
    vtss_init_ptp_timer(&oam_timer, ptp_oam_slave_timer, 0);  // Note: The value 0 indicates first ptp clock instance.
#endif /* defined(VTSS_ARCH_SERVAL) */
    for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
        vtss_init_ptp_timer(&one_pps_sync_timer[i], ptp_one_pps_slave_timer, ptp_global.ptpi[i]);
    }

    if (mesa_cap_phy_ts) {
        if (!tod_tc_mode_get(&phy_ts_mode)) {
            T_W("Missed tc_mode_get");
        }
    }
    vtss_init_ptp_timer(&sys_time_sync_timer, ptp_sys_time_sync_function, 0);  // Note: The value 0 indicates first ptp clock instance.
    ptp_time_setting_init();

    /* Ethernet PTP frames registration */
    packet_rx_filter_init(&rx_filter);
    rx_filter.modid       = VTSS_MODULE_ID_PTP;
    rx_filter.match       = PACKET_RX_FILTER_MATCH_ETYPE; // only ethertype filter
    rx_filter.prio        = PACKET_RX_FILTER_PRIO_HIGH;
    rx_filter.contxt      = &ptp_global.ptpi;
    rx_filter.cb          = packet_rx;
    rx_filter.etype       = ptp_ether_type;
    rx_filter.thread_prio = VTSS_THREAD_PRIO_HIGHER;

    rc = packet_rx_filter_register(&rx_filter, &filter_id_ether);
    VTSS_ASSERT(rc == VTSS_OK);

    /* UDP PTP frames registration */
    rc = udp_rx_filter_init();
    VTSS_ASSERT(rc == VTSS_OK);

    /* Port change callback */
    (void) port_global_change_register(VTSS_MODULE_ID_PTP, ptp_port_state_change_callback);

    /* wait 500 ms, otherwise the ACL setup may fail ! */
    wakeup = vtss_current_time() + VTSS_OS_MSEC2TICK(500);
    (void)vtss_flag_timed_wait(&ptp_global_control_flags, 0xffff, VTSS_FLAG_WAITMODE_AND, wakeup);

    /* Initialize the clock */
    for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
        initial_port_filter_get(i);
        /* update serval backplane mode */
        port_iter_t       pit;
        port_iter_init_local(&pit);
        while (port_iter_getnext(&pit)) {
            j = pit.iport;
            PTP_RC(ptp_ts_operation_mode_set(j));
        }

#if defined(VTSS_SW_OPTION_MEP)
        if (mesa_cap_oam_used_as_ptp_protocol) {
            if (i == 0 && config_data.conf[i].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY && config_data.conf[i].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM) {
                vtss_ptp_timer_start(&oam_timer, OAM_TIMER_INIT_VALUE, false);
            }
        }
#endif
    }
    ptp_conf_propagate();
    T_I("Clock initialized");

    ptp_global.ready = true; /* Ready to rock */
    /* Register Initialport state (portstate changes before setting ptp_global.ready are ignored) */
    for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
        ptp_port_link_state_initial(i);
    }
    PTP_CORE_UNLOCK();
    
    /* Start system-time <-> PTP time synchronization */
    if (system_time_sync_conf.mode != VTSS_APPL_PTP_SYSTEM_TIME_NO_SYNC) {
        PTP_RC(vtss_appl_ptp_system_time_sync_mode_set(&system_time_sync_conf));
    }

    // In order for the modulo logic below to work whether one tick is 1, 10 or <whatever> ms,
    // we need to align wakeup to a multiple of PTP_SCHEDULER_RATE.
    wakeup = VTSS_OS_MSEC2TICK(PTP_SCHEDULER_RATE) * (wakeup / VTSS_OS_MSEC2TICK(PTP_SCHEDULER_RATE));
    for (;;) {
        PTP_CORE_LOCK();
        while (ptp_global.ptpi[0]) {
            T_R( "%s: tick()", __FUNCTION__);
            wakeup += VTSS_OS_MSEC2TICK(PTP_SCHEDULER_RATE);
            PTP_CORE_UNLOCK();
            while ((flags = vtss_flag_timed_wait(&ptp_global_control_flags, 0xffff, VTSS_FLAG_WAITMODE_OR_CLR, wakeup))) {
                T_R("PTP thread event, flags %x", flags);

                PTP_CORE_LOCK();
                if (flags & CTLFLAG_PTP_DEFCONFIG) {
                    /* no existing transparent clock after reset */
                    transparent_clock_exists = false;
                    for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
                        slave_clock_exists[i] = 0;
                    }
                    ptp_conf_read(true); // clear configuration
                    /* Make PTP configuration effective in PTP core */
                    ptp_conf_propagate();
                }
                if (flags & CTLFLAG_PTP_SET_ACL) {
                    T_I("set ACL rules");
                    for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
                        if (ptp_ace_update(i) != VTSS_OK) T_IG(0, "ACE update error");
                    }
                }
                if (flags & CTLFLAG_PTP_TIMER) {
                    vtss_ptp_timer_tick(tick_factor);
                    T_NG(VTSS_TRACE_GRP_PTP_BASE_TIMER, "ptp timer");
                }
                PTP_CORE_UNLOCK();
            }
            /* timeout */
            PTP_CORE_LOCK();

            if (mesa_cap_ts) {
//                vtss_one_pps_external_input_timer(false);
            }
            for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
                if (config_data.conf[i].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
                    vtss_ptp_tick(ptp_global.ptpi[i]);
                    if (config_data.conf[i].clock_init.cfg.protocol != VTSS_APPL_PTP_PROTOCOL_OAM && config_data.conf[i].clock_init.cfg.protocol != VTSS_APPL_PTP_PROTOCOL_ONE_PPS) {
                        // Run the port filter every PTP_CLOCK_INSTANCES seconds for a particular instance.
                        if ((VTSS_OS_TICK2MSEC(wakeup) % (PTP_CLOCK_INSTANCES * 1000LLU)) == (u64)i * 1000LLU) {
                            T_I("Poll port filter");
                            poll_port_filter(i);
                        }
                    }
#if defined(VTSS_SW_OPTION_SYNCE)
                    if (config_data.conf[i].clock_init.cfg.profile == VTSS_APPL_PTP_PROFILE_G_8275_1) {
                        // Update clock class from current oscillator quality.
                        if ((VTSS_OS_TICK2MSEC(wakeup) % (1000LLU)) == 0LLU) {
                            T_D("Update clock class");
                            vtss_appl_synce_quality_level_t ql;
                            PTP_RC(synce_mgmt_clock_ql_get(&ql));
                            u8 clock_class = g8275_ql_to_class(ql, config_data.conf[i].clock_init.cfg.deviceType);
                            if (ptp_global.ptpi[i]->local_clock_class != clock_class || ptp_global.ptpi[i]->holdover_timeout_spec != g8275_ho_spec(ql)) {
                                T_I("Change local clock class to %d", clock_class);
                                ptp_global.ptpi[i]->local_clock_class = clock_class;
                                ptp_global.ptpi[i]->holdover_timeout_spec = g8275_ho_spec(ql);
                                defaultDSChanged(ptp_global.ptpi[i]);
                            }
                        }
                    }
#endif //defined(VTSS_SW_OPTION_SYNCE)
                }
            }
            // update servo mode according to Synce selector state, adjustment method filter type and profile
            T_NG(VTSS_TRACE_GRP_MS_SERVO, "update selected source");
            PTP_RC(vtss_ptp_update_selected_src());
            if ((VTSS_OS_TICK2MSEC(wakeup) % 21000LLU) == 0) {
                if (ptp_sock > 0) {
                    T_I("socket_init");
                    ptp_socket_init(); /* reinitialize socket each 21 sec, to reconnect if the vlan has been down */
                }
            }
        }
        ptp_global.ready = false; /* Done rocking */
        PTP_CORE_UNLOCK();

        /* De-Initialize PTP core */
        {
            PTP_CORE_LOCK_SCOPE();
            
            for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
                if (ptp_global.ptpi[i]) {
                    vtss_ptp_clock_remove(ptp_global.ptpi[i]);
#ifdef SW_OPTION_BASIC_PTP_SERVO
                    vtss::destroy<ptp_basic_servo>(basic_servo[i]);
#endif // SW_OPTION_BASIC_PTP_SERVO
#if defined(VTSS_SW_OPTION_ZLS30387)
                    vtss::destroy<ptp_ms_servo>(advanced_servo[i]);
#endif
                    if (ptp_ace_update(i) != VTSS_OK) T_IG(0, "ACE update error");
                    T_I("ptp_ace_update returned %d", rc);
                }
            }
        }
    }
}


/****************************************************************************/
/*  PTP callout functions                                                  */
/****************************************************************************/
/*
 * calculate difference between two ts counters.
 */
void vtss_1588_ts_cnt_sub(u32 *r, u32 x, u32 y)
{
    vtss_tod_ts_cnt_sub(r, x, y);
}

/*
 * calculate sum of two ts counters.
 */
void vtss_1588_ts_cnt_add(u32 *r, u32 x, u32 y)
{
    vtss_tod_ts_cnt_add(r, x, y);
}

void vtss_1588_timeinterval_to_ts_cnt(u32 *r, mesa_timeinterval_t x)
{
    vtss_tod_timeinterval_to_ts_cnt(r, x);
}

void vtss_1588_ts_cnt_to_timeinterval(mesa_timeinterval_t *r, u32 x)
{
    vtss_tod_ts_cnt_to_timeinterval(r, x);
}


mesa_rc vtss_1588_ingress_latency_set(u16 portnum, mesa_timeinterval_t ingress_latency)
{
    mesa_rc rc = VTSS_RC_OK;




    T_DG(VTSS_TRACE_GRP_PHY_TS, "Port_no = %d, ingress_latency " VPRI64d, portnum, ingress_latency);

    // Read ingress calibration and compensate ingress latency accordingly
    mesa_timeinterval_t ingress_calib_value = 0;
    {
        // Determine port mode
        vtss_appl_port_status_t port_status;
        vtss_ifindex_t ifindex;
        (void) vtss_ifindex_from_port(0, uport2iport(portnum), &ifindex);
        if (vtss_appl_port_status_get(ifindex, &port_status) == MESA_RC_OK) {

            BOOL fiber = port_status.status.fiber;

            switch (port_status.status.speed) {
                case MESA_SPEED_10M:   {
                                           ingress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(portnum)].port_latencies_10m_cu.ingress_latency;
                                           break;
                                       }
                case MESA_SPEED_100M:  {
                                           ingress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(portnum)].port_latencies_100m_cu.ingress_latency;
                                           break;
                                       }
                case MESA_SPEED_1G:    {
                                           if (fiber) {
                                               ingress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(portnum)].port_latencies_1g.ingress_latency;
                                           } else {
                                               ingress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(portnum)].port_latencies_1g_cu.ingress_latency;
                                           }
                                           break;
                                       }  
                case MESA_SPEED_2500M: {
                                           ingress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(portnum)].port_latencies_2g5.ingress_latency;
                                           break;
                                       }
                case MESA_SPEED_5G:    {
                                           ingress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(portnum)].port_latencies_5g.ingress_latency;
                                           break;
                                       }
                case MESA_SPEED_10G:   {
                                           ingress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(portnum)].port_latencies_10g.ingress_latency;
                                           break;
                                       }
                default: {
                             ingress_calib_value = 0;
                         }
            }
            ingress_latency += ingress_calib_value;
        }
    }

    if (port_data[ptp_api_to_l2port(portnum)].topo.ts_feature == VTSS_PTP_TS_PTS) {
        if (mesa_cap_phy_ts) {







            rc = mesa_phy_ts_ingress_latency_set(API_INST_DEFAULT, ptp_api_to_l2port(portnum), &ingress_latency);

            T_IG(VTSS_TRACE_GRP_PHY_TS, "Port_no = %d, ingress_latency = " VPRI64d " ns", portnum, ingress_latency>>16);
        } else {
            T_WG(VTSS_TRACE_GRP_PHY_TS, "Port_no = %d, PHY timestamping not supported", portnum);
        }
    } else {
        rc = mesa_ts_ingress_latency_set(0, ptp_api_to_l2port(portnum), &ingress_latency);
    }
    if (rc == VTSS_RC_OK) {
        port_data[ptp_api_to_l2port(portnum)].ingress_latency = ingress_latency - ingress_calib_value;
    } else {
        T_WG(VTSS_TRACE_GRP_PHY_TS, "Could not set ingress latency value in HW. The calibration for port %d is likely invalid. You may want to reset the calibration and/or recalibrate.", portnum);
    }
    return rc;
}

void vtss_1588_ingress_latency_get(u16 portnum, mesa_timeinterval_t *ingress_latency)
{
     *ingress_latency = port_data[ptp_api_to_l2port(portnum)].ingress_latency;
     T_DG(VTSS_TRACE_GRP_PHY_TS, "Port_no = %d, ingress_latency " VPRI64d, portnum, *ingress_latency);
}

mesa_rc vtss_1588_egress_latency_set(u16 portnum, mesa_timeinterval_t egress_latency)
{
    mesa_rc rc = VTSS_RC_OK;




    // Read egress calibration and compensate egress latency accordingly
    mesa_timeinterval_t egress_calib_value = 0;
    {
        // Determine port mode
        vtss_appl_port_status_t port_status;
        vtss_ifindex_t ifindex;
        (void) vtss_ifindex_from_port(0, uport2iport(portnum), &ifindex);

        if (vtss_appl_port_status_get(ifindex, &port_status) == MESA_RC_OK) {

            BOOL fiber = port_status.status.fiber;

            switch (port_status.status.speed) {
                case MESA_SPEED_10M:   {
                                           egress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(portnum)].port_latencies_10m_cu.egress_latency;
                                           break;
                                       }  
                case MESA_SPEED_100M:  {
                                           egress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(portnum)].port_latencies_100m_cu.egress_latency;
                                           break;
                                       }  
                case MESA_SPEED_1G:    {
                                           if (fiber) {
                                               egress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(portnum)].port_latencies_1g.egress_latency;
                                           } else {
                                               egress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(portnum)].port_latencies_1g_cu.egress_latency;
                                           }
                                           break;
                                       }  
                case MESA_SPEED_2500M: {
                                           egress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(portnum)].port_latencies_2g5.egress_latency;
                                           break;
                                       }
                case MESA_SPEED_5G:    {
                                           egress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(portnum)].port_latencies_5g.egress_latency;
                                           break;
                                       }
                case MESA_SPEED_10G:   {
                                           egress_calib_value = ptp_port_calibration.port_calibrations[uport2iport(portnum)].port_latencies_10g.egress_latency;
                                           break;
                                       }
                default: {
                             egress_calib_value = 0;
                         }
            }
            egress_latency += egress_calib_value;
        }
    }

    if (port_data[ptp_api_to_l2port(portnum)].topo.ts_feature == VTSS_PTP_TS_PTS) {
        if (mesa_cap_phy_ts) {







            rc = mesa_phy_ts_egress_latency_set(API_INST_DEFAULT, ptp_api_to_l2port(portnum), &egress_latency);

            T_DG(VTSS_TRACE_GRP_PHY_TS, "Port_no = %d, egress_latency = " VPRI64d " ns", portnum, egress_latency>>16);
        } else {
            T_WG(VTSS_TRACE_GRP_PHY_TS, "Port_no = %d, PHY timestamping not supported", portnum);
        }
    } else {
        rc = mesa_ts_egress_latency_set(0, ptp_api_to_l2port(portnum), &egress_latency);
    }
    if (rc == VTSS_RC_OK) {
        port_data[ptp_api_to_l2port(portnum)].egress_latency = egress_latency - egress_calib_value;
    } else {
        T_WG(VTSS_TRACE_GRP_PHY_TS, "Could not set egress latency value in HW. The calibration for port %d is likely invalid. You may want to reset the calibration and/or recalibrate.", portnum);
    }
    return rc;
}

void vtss_1588_egress_latency_get(u16 portnum, mesa_timeinterval_t *egress_latency)
{
    *egress_latency = port_data[ptp_api_to_l2port(portnum)].egress_latency;
}

void vtss_1588_p2p_delay_set(u16 portnum,
                             mesa_timeinterval_t p2p_delay)
{



    if (0 < portnum && portnum <= config_data.conf[0].clock_init.numberEtherPorts) {

        if (port_data[ptp_api_to_l2port(portnum)].topo.ts_feature == VTSS_PTP_TS_PTS) {
            if (mesa_cap_phy_ts) {    
                // the PHY's don't support negative p2p path delays
                if (p2p_delay < 0LL) {p2p_delay = 0LL;}







                PTP_RC(mesa_phy_ts_path_delay_set(API_INST_DEFAULT, ptp_api_to_l2port(portnum), &p2p_delay));

                T_DG(VTSS_TRACE_GRP_PHY_TS, "Port_no = %d, p2p_delay = " VPRI64d " ns", portnum, p2p_delay>>16);
            } else {
                T_WG(VTSS_TRACE_GRP_PHY_TS, "Port_no = %d, PHY timestamping not supported", portnum);
            }
        } else {

            u32 delay_cnt;
            vtss_1588_timeinterval_to_ts_cnt(&delay_cnt, p2p_delay);
            port_data[ptp_api_to_l2port(portnum)].delay_cnt = delay_cnt;
            T_DG(_I, "port %d, delay_cnt = %d, peer_delay = " VPRI64d " ns", portnum, delay_cnt, p2p_delay>>16);

            PTP_RC(mesa_ts_p2p_delay_set(0, ptp_api_to_l2port(portnum), &p2p_delay));
        }
    } else {
        T_IG(VTSS_TRACE_GRP_PHY_TS, "Port_no = %d, is a non ethernet port", portnum);
    }
}

mesa_rc vtss_1588_asymmetry_set(u16 portnum,
                             mesa_timeinterval_t asymmetry)
{
    mesa_rc rc = VTSS_RC_OK;




    if (port_data[ptp_api_to_l2port(portnum)].topo.ts_feature == VTSS_PTP_TS_PTS) {
        if (mesa_cap_phy_ts) {







            rc = mesa_phy_ts_delay_asymmetry_set(API_INST_DEFAULT, ptp_api_to_l2port(portnum), &asymmetry);

            T_DG(VTSS_TRACE_GRP_PHY_TS, "Port_no = %d, asymmetry = " VPRI64d " ns", portnum, asymmetry>>16);
        } else {
            T_WG(VTSS_TRACE_GRP_PHY_TS, "Port_no = %d, PHY timestamping not supported", portnum);
        }
    } else {

        u32 asy_cnt;
        vtss_1588_timeinterval_to_ts_cnt(&asy_cnt, asymmetry);
        port_data[ptp_api_to_l2port(portnum)].asymmetry_cnt = asy_cnt;
        T_IG(_I, "port %d, asymmetry = %d", portnum, asy_cnt);

        if (mesa_cap_ts_asymmetry_comp) {
            rc = mesa_ts_delay_asymmetry_set(0, ptp_api_to_l2port(portnum), &asymmetry);
        }
    }
    if (rc == VTSS_RC_OK) {
        port_data[ptp_api_to_l2port(portnum)].asymmetry = asymmetry;
    }
    return rc;
}

void vtss_1588_asymmetry_get(u16 portnum,
                             mesa_timeinterval_t *asymmetry)
{
    *asymmetry = port_data[ptp_api_to_l2port(portnum)].asymmetry;
}

u16 vtss_ptp_get_rand(u32 *seed)
{
    return rand_r((unsigned int*)seed);
}

void *vtss_ptp_calloc(size_t nmemb, size_t sz)
{
    return VTSS_CALLOC(nmemb, sz);
}

void vtss_ptp_free(void *ptr)
{
    VTSS_FREE(ptr);
}

/****************************************************************************/
/*  API functions                                                           */
/****************************************************************************/

static mesa_rc ptp_servo_instance_set(uint instance)
{
#ifdef SW_OPTION_BASIC_PTP_SERVO
    if (config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_BASIC) {
        (void) vtss_ptp_clock_servo_set(ptp_global.ptpi[instance], basic_servo[instance]);
    } else
#endif // SW_OPTION_BASIC_PTP_SERVO
#if defined(VTSS_SW_OPTION_ZLS30387)
    if (config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_DEFAULT ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_FREQ_XO ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_XO ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_FREQ_TCXO ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_TCXO ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_FREQ_OCXO_S3E ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_FREQ ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_PHASE ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_FREQ ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_FREQ_ACCURACY_FDD ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_FREQ_ACCURACY_XDSL ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_ELEC_FREQ ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_ELEC_PHASE ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_TCXO_Q ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_Q ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_RELAXED_C60W ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_RELAXED_C150 ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_RELAXED_C180 ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_RELAXED_C240 ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_BETA ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E_R4_6_1 ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_BASIC_PHASE ||
            config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_BASIC_PHASE_LOW
       )
    {
        (void) vtss_ptp_clock_servo_set(ptp_global.ptpi[instance], advanced_servo[instance]);
    } else
#endif
    {
        T_E("No PTP servo available.");
        return VTSS_RC_ERROR;
    }

    if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE && config_data.conf[instance].clock_init.cfg.clock_domain == 0) {
        if (VTSS_RC_OK != vtss_local_clock_adj_method(config_data.init_ext_clock_mode.adj_method, config_data.conf[instance].clock_init.cfg.profile)) {
            T_W("vtss_local_clock_adj_method returned error");
        }
    }
    ptp_global.ptpi[instance]->ssm.servo->activate(config_data.conf[instance].clock_init.cfg.clock_domain);
    return VTSS_RC_OK;
}

static mesa_rc ptp_clock_create(const ptp_init_clock_ds_t *initData, uint instance)
{
    mesa_rc rc = VTSS_RC_ERROR;
    int j;

    if ((initData->cfg.configured_vid < 1) || (initData->cfg.configured_vid > 4095)) {
        T_E("Cannot create clock with illegal VLAN ID");
        return VTSS_RC_ERROR;
    }
    /* after change to icfg, this function is initially called before the PTP application is ready */
    /* in this case we only save the configuration, and does not update the HW */
    T_I("PTP_READY %d, deviceType %d, transparent_clock_exists %d", PTP_READY(), initData->cfg.deviceType, transparent_clock_exists);
    if (instance < PTP_CLOCK_INSTANCES) {
        // apply default filter and servo parameters
        vtss_appl_ptp_filter_default_parameters_get(&config_data.conf[instance].filter_params, initData->cfg.profile);
        vtss_appl_ptp_clock_servo_default_parameters_get(&config_data.conf[instance].servo_params, initData->cfg.profile);

        // Check if other clocks than this one exist in the same clock domain.
        // If so then update the configuration of the new clock so that it uses the same servo type as the already existing clocks in the same domain.
        for (int i = 0; i < PTP_CLOCK_INSTANCES; i++) {
            if ((i != instance) &&
                (ptp_global.ptpi[i]->clock_init->cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) &&
                (ptp_global.ptpi[i]->clock_init->cfg.clock_domain == ptp_global.ptpi[instance]->clock_init->cfg.clock_domain))
            {
                config_data.conf[instance].clock_init.cfg.filter_type = config_data.conf[i].clock_init.cfg.filter_type;
                break;
            }
        }

        if (initData->cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY && initData->cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM) {
            /* check if OAM instance is active */
            if (instance == 0) {
                config_data.conf[instance].clock_init.cfg.mep_instance = initData->cfg.mep_instance;
                config_data.conf[instance].clock_init.cfg.priority1 = 255;
            }
        }
        if (PTP_READY()) {
            if (initData->cfg.deviceType == VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT || initData->cfg.deviceType == VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT) {
                if (transparent_clock_exists) {
                    config_data.conf[instance].clock_init.cfg.deviceType = VTSS_APPL_PTP_DEVICE_NONE;
                    return PTP_RC_MULTIPLE_TC;
                } else
                    transparent_clock_exists = true;
            }
            T_I("clock init set: portcount = %d, deviceType = %d",config_data.conf[instance].clock_init.numberPorts, config_data.conf[instance].clock_init.cfg.deviceType);
            (void)update_ptp_ip_address(instance, initData->cfg.configured_vid);
            if (initData->cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY && initData->cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM) {
#if defined(VTSS_SW_OPTION_MEP)
                if (mesa_cap_oam_used_as_ptp_protocol) {
                    /* check if OAM instance is active */
                    if (instance == 0) {
                        vtss_ptp_clock_create(ptp_global.ptpi[instance]);
                        /* start timer for reading OAM timestamps */
                        vtss_ptp_timer_start(&oam_timer, 128, false);
                        rc = VTSS_RC_OK;
                    }
                } else {
                    T_W("OAM protocol not supported.");
                }
#else
                T_W("OAM protocol not supported.");
#endif
            } else if (initData->cfg.deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY && initData->cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM) {
#if defined(VTSS_SW_OPTION_MEP)
                if (mesa_cap_oam_used_as_ptp_protocol) {
                    /* check if OAM instance is active */
                    if (instance == 0) {
                        config_data.conf[instance].clock_init.cfg.mep_instance = initData->cfg.mep_instance;
                        vtss_ptp_clock_create(ptp_global.ptpi[instance]);
                        rc = VTSS_RC_OK;
                    }
                } else {
                    T_W("OAM protocol not supported.");
                }
#else
                T_W("OAM protocol not supported.");
#endif
            } else {
                if ((rc = ptp_ace_update(instance)) != VTSS_OK) {
                    T_WG(0, "ACE add error");
                    config_data.conf[instance].clock_init.cfg.deviceType = VTSS_APPL_PTP_DEVICE_NONE;
                    return rc;
                }
                if (initData->cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY) {
                    config_data.conf[instance].clock_init.cfg.priority1 = 255;
                }
                vtss_ptp_clock_create(ptp_global.ptpi[instance]);
                for (j = 0; j < MAX_UNICAST_MASTERS_PR_SLAVE; j++) {
                    vtss_ptp_uni_slave_conf_set(ptp_global.ptpi[instance], j, &config_data.conf[instance].unicast_slave[j]);
                }
                ptp_port_link_state_initial(instance);
            }

            vtss_ptp_clock_slave_config_set(ptp_global.ptpi[instance]->ssm.servo, &config_data.conf[instance].slave_cfg);

            rc = ptp_servo_instance_set(instance);
        } else {
            rc = VTSS_RC_OK;
        }
    }
    return rc;
}

mesa_rc vtss_appl_ptp_clock_config_default_ds_del(uint instance)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_rc rc = VTSS_RC_ERROR;
    int j;
    port_iter_t       pit;
    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES) && (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE)) {
        ptp_global.ptpi[instance]->ssm.servo->deactivate(config_data.conf[instance].clock_init.cfg.clock_domain);  // Deactivate the servo associated with the clock before the clock is deleted

        if (config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT ||
            config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT) {
            transparent_clock_exists = false;
        }
        if (config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND ||
                config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY) {
            --slave_clock_exists[config_data.conf[instance].clock_init.cfg.clock_domain];
        }
        
        port_iter_init_local(&pit);
        while (port_iter_getnext(&pit)) {
            PTP_RC(ptp_port_domain_check(config_data.conf[instance].port_config[pit.iport].portInternal, FALSE, pit.iport, instance));
        }
        for (j = 0; j < MAX_UNICAST_MASTERS_PR_SLAVE; j++) {
            T_I("conf[%d].unicast_slave[%d].ip_addr= 0x%x", instance, j, config_data.conf[instance].unicast_slave[j].ip_addr);
            if (config_data.conf[instance].unicast_slave[j].ip_addr != 0) {
                config_data.conf[instance].unicast_slave[j].ip_addr = 0;
                vtss_ptp_uni_slave_conf_set(ptp_global.ptpi[instance], j, &config_data.conf[instance].unicast_slave[j]);
            }
        }
        ptp_conf_init(&config_data.conf[instance], instance);
        T_D("clock delete");

        port_iter_init_local(&pit);
        while (port_iter_getnext(&pit)) {
            PTP_RC(ptp_ts_operation_mode_set(pit.iport));
        }
        vtss_ptp_clock_create(ptp_global.ptpi[instance]);
        if (ptp_ace_update(instance) != VTSS_OK) T_IG(0, "ACE update error");;
        rc = VTSS_RC_OK;
    }
    return rc;
}

static bool ptp_port_domain_check(bool internal, bool enable, mesa_port_no_t port_no, uint instance)
{
    BOOL rc = TRUE;
    if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE && port_no < port_isid_port_count(VTSS_ISID_LOCAL)) {
        if (!internal) {
            if (enable) {
                if (config_data.conf[instance].port_config[port_no].enabled == FALSE) {
                    if (port_data[port_no].topo.ts_feature == VTSS_PTP_TS_PTS && config_data.conf[instance].clock_init.cfg.clock_domain != 0) {
                        rc = FALSE;
                    } else if (port_data[port_no].inst_count == 0) {
                        port_data[port_no].inst_count++;
                        port_data[port_no].port_domain = config_data.conf[instance].clock_init.cfg.clock_domain;
                    } else {
                        if (port_data[port_no].port_domain == config_data.conf[instance].clock_init.cfg.clock_domain) {
                            port_data[port_no].inst_count++;
                        } else {
                            rc = FALSE;
                        }
                    }
                }
            } else { //disable
                if (config_data.conf[instance].port_config[port_no].enabled == TRUE) {
                    if (port_data[port_no].inst_count != 0) {
                        port_data[port_no].inst_count--;
                        //if (port_data[port_no].inst_count == 0) {
                        //    port_data[port_no].port_domain = 0;
                        //}
                    } else {
                        T_EG(_I,"instance %d, port_no %d, domain %d, inst_count %d mesh", instance, iport2uport(port_no), port_data[port_no].port_domain, port_data[port_no].inst_count);
                    }
                }
            }
        }
    }
    T_IG(_I,"instance %d, port_no %d, domain %d, inst_count %d ", instance, iport2uport(port_no), port_data[port_no].port_domain, port_data[port_no].inst_count);
    return rc;
}

static mesa_rc ptp_port_ena(bool internal, uint portnum, uint instance)
{
    mesa_rc ok = PTP_ERROR_INV_PARAM;
    if (instance < PTP_CLOCK_INSTANCES) {
        if ((0 < portnum) && (portnum <= config_data.conf[instance].clock_init.numberPorts)) {
            if (ptp_port_domain_check(internal, TRUE, ptp_api_to_l2port(portnum), instance)) {
                if ((config_data.conf[instance].port_config[ptp_api_to_l2port(portnum)].portInternal != internal) || (config_data.conf[instance].port_config[ptp_api_to_l2port(portnum)].enabled != TRUE)) {
                    config_data.conf[instance].port_config[ptp_api_to_l2port(portnum)].portInternal = internal;
                    config_data.conf[instance].port_config[ptp_api_to_l2port(portnum)].enabled = TRUE;
            
                    if (PTP_READY()) {
                        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
                            if (0 < portnum && portnum <= config_data.conf[instance].clock_init.numberEtherPorts) {
                                vtss_1588_asymmetry_get(portnum, &config_data.conf[instance].port_config[ptp_api_to_l2port(portnum)].delayAsymmetry);
                                PTP_RC(ptp_ts_operation_mode_set(ptp_api_to_l2port(portnum)));
                            }
                            //if (config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT ||
                            //        config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT ||
                            //    !internal) {
                            config_data.conf[instance].port_config[ptp_api_to_l2port(portnum)].portInternal = internal;
                            ok = (vtss_ptp_port_ena(ptp_global.ptpi[instance], portnum) == VTSS_RC_OK) ? VTSS_RC_OK : (mesa_rc) PTP_RC_INVALID_PORT_NUMBER;
                            config_data.conf[instance].port_config[ptp_api_to_l2port(portnum)].enabled = TRUE;
                            //}
                        }
                        T_I("port enabled, instance %d, port %d, ok %x", instance, portnum, ok);
                    } else {
                        ok = VTSS_RC_OK;
                    }
                } else {
                    ok = VTSS_RC_OK;
                }
            } else {
                ok = PTP_RC_CLOCK_DOMAIN_CONFLICT;
            }
        }
    }
    return ok;
}

static mesa_rc ptp_port_dis( uint portnum, uint instance)
{
    mesa_rc rc = VTSS_RC_ERROR;
    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            if (0 < portnum && portnum <= config_data.conf[instance].clock_init.numberPorts) {
                if (ptp_port_domain_check(config_data.conf[instance].port_config[ptp_api_to_l2port(portnum)].portInternal, FALSE, ptp_api_to_l2port(portnum), instance)) {
                    if ((config_data.conf[instance].port_config[ptp_api_to_l2port(portnum)].enabled != FALSE) || (config_data.conf[instance].port_config[ptp_api_to_l2port(portnum)].portInternal != FALSE)) {
                        rc = vtss_ptp_port_dis(ptp_global.ptpi[instance], portnum);
                        config_data.conf[instance].port_config[ptp_api_to_l2port(portnum)].enabled = FALSE;
                        config_data.conf[instance].port_config[ptp_api_to_l2port(portnum)].portInternal = FALSE;
                        PTP_RC(ptp_ts_operation_mode_set(ptp_api_to_l2port(portnum)));
                    }
                    else {
                        rc = VTSS_RC_OK;
                    }
                }
            }
        }
    }
    return rc;
}

mesa_rc vtss_appl_ptp_clock_config_default_ds_get(uint instance, vtss_appl_ptp_clock_config_default_ds_t *const default_ds_cfg)
{
    PTP_CORE_LOCK_SCOPE();

    if (instance < PTP_CLOCK_INSTANCES) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            *default_ds_cfg = config_data.conf[instance].clock_init.cfg;
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

mesa_rc vtss_appl_ptp_clock_status_default_ds_get(uint instance, vtss_appl_ptp_clock_status_default_ds_t *const default_ds_status)
{
    PTP_CORE_LOCK_SCOPE();

    if (instance < PTP_CLOCK_INSTANCES) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            (void) vtss_ptp_get_clock_default_ds_status(ptp_global.ptpi[instance], default_ds_status);
            // now the numberPorts includes some virtual ports (for 1PPS synchronization), but we only make the ethernet ports visible for the user
            default_ds_status->numberPorts = config_data.conf[instance].clock_init.numberEtherPorts;
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

void ptp_apply_profile_defaults_to_default_ds(vtss_appl_ptp_clock_config_default_ds_t *default_ds_cfg, vtss_appl_ptp_profile_t profile)
{
    vtss_zarlink_servo_t servo_type = (vtss_zarlink_servo_t)vtss_appl_cap_zarlink_servo_type;
    default_ds_cfg->profile = profile;

    default_ds_cfg->path_trace_enable = FALSE;
    if (profile == VTSS_APPL_PTP_PROFILE_G_8265_1) {
        default_ds_cfg->protocol = VTSS_APPL_PTP_PROTOCOL_IP4UNI;
        default_ds_cfg->oneWay = false;  // Note: oneWay should actually be true but MS-PDV does not support uni-directional modes.
    }
    else if (profile != VTSS_APPL_PTP_PROFILE_NO_PROFILE) {
        default_ds_cfg->protocol = VTSS_APPL_PTP_PROTOCOL_ETHERNET;
        default_ds_cfg->oneWay = false;
    }

    if (profile == VTSS_APPL_PTP_PROFILE_G_8265_1) {
        default_ds_cfg->domainNumber = 4;
        if (servo_type == VTSS_ZARLINK_SERVO_ZLS30380) {
            default_ds_cfg->filter_type = PTP_FILTERTYPE_ACI_FREQ_ACCURACY_FDD;
        } else if (servo_type == VTSS_ZARLINK_SERVO_ZLS30384 || servo_type == VTSS_ZARLINK_SERVO_ZLS30387) {
            default_ds_cfg->filter_type = PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_FREQ;
        }
    }
    else if (profile == VTSS_APPL_PTP_PROFILE_G_8275_1) {
        default_ds_cfg->domainNumber = 24;
        default_ds_cfg->localPriority = 128;
        if (servo_type == VTSS_ZARLINK_SERVO_ZLS30380 || servo_type == VTSS_ZARLINK_SERVO_ZLS30384) {
            default_ds_cfg->filter_type = PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE;
        }
    }
    else if (profile == VTSS_APPL_PTP_PROFILE_IEEE_802_1AS) {
        default_ds_cfg->domainNumber = 0;
        default_ds_cfg->localPriority = 128;
        default_ds_cfg->twoStepFlag = TRUE;
        default_ds_cfg->path_trace_enable = TRUE;
    }
    else {
        default_ds_cfg->domainNumber = 0;
    }
}

void ptp_get_default_clock_default_ds(vtss_appl_ptp_clock_status_default_ds_t *default_ds_status,
                                      vtss_appl_ptp_clock_config_default_ds_t *default_ds_cfg)
{
    vtss_zarlink_servo_t servo_type = (vtss_zarlink_servo_t)vtss_appl_cap_zarlink_servo_type;
    default_ds_cfg->deviceType= VTSS_APPL_PTP_DEVICE_NONE;
    default_ds_cfg->twoStepFlag = false;
    default_ds_cfg->oneWay = false;
    default_ds_cfg->protocol = VTSS_APPL_PTP_PROTOCOL_ETHERNET;
    if (servo_type == VTSS_ZARLINK_SERVO_ZLS30380 || servo_type == VTSS_ZARLINK_SERVO_ZLS30384 || servo_type == VTSS_ZARLINK_SERVO_ZLS30387) {
        default_ds_cfg->filter_type = PTP_FILTERTYPE_ACI_BASIC_PHASE_LOW;
    }

    {
        PTP_CORE_LOCK_SCOPE();

        memcpy(default_ds_status->clockIdentity, ptp_global.sysmac.addr, 3);
        default_ds_status->clockIdentity[3] = 0xff;
        default_ds_status->clockIdentity[4] = 0xfe;
        memcpy(default_ds_status->clockIdentity+5, ptp_global.sysmac.addr+3, 3);
    }

    default_ds_cfg->configured_vid = 1;
    /* in IPv4 encapsulation mode, the management VLAN id is used */
    default_ds_cfg->configured_pcp = 0;
    default_ds_cfg->mep_instance = 1;
    default_ds_cfg->clock_domain = 0;
    default_ds_cfg->dscp = 0;

    default_ds_status->numberPorts = port_isid_port_count(VTSS_ISID_LOCAL) + mesa_cap_ts_io_cnt;
    /* dynamic */
    default_ds_status->clockQuality.clockAccuracy = 0;
    default_ds_status->clockQuality.clockClass = 0;
    default_ds_status->clockQuality.offsetScaledLogVariance = 0;

    /* configurable */
    default_ds_cfg->priority1 = 128;
    default_ds_cfg->priority2 = 128;
    default_ds_cfg->domainNumber = 0;
    default_ds_cfg->localPriority = 128;
    default_ds_cfg->path_trace_enable = FALSE;
}

mesa_rc ptp_set_virtual_port_clock_class(uint instance, u8 ptp_class)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        vtss_appl_ptp_virtual_port_config_t c;
        if (vtss_appl_ptp_clock_config_virtual_port_config_get(instance, &c) == VTSS_RC_OK) {
            c.clockQuality.clockClass = ptp_class;
            rc = vtss_appl_ptp_clock_config_virtual_port_config_set(instance, &c);
        }
    }
    return rc;
}

mesa_rc ptp_set_virtual_port_clock_accuracy(uint instance, u8 ptp_accuracy)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        vtss_appl_ptp_virtual_port_config_t c;
        if (vtss_appl_ptp_clock_config_virtual_port_config_get(instance, &c) == VTSS_RC_OK) {
            c.clockQuality.clockAccuracy = ptp_accuracy;
            rc = vtss_appl_ptp_clock_config_virtual_port_config_set(instance, &c);
        }
    }
    return rc;
}

mesa_rc ptp_set_virtual_port_clock_variance(uint instance, u16 ptp_variance)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        vtss_appl_ptp_virtual_port_config_t c;
        if (vtss_appl_ptp_clock_config_virtual_port_config_get(instance, &c) == VTSS_RC_OK) {
            c.clockQuality.offsetScaledLogVariance = ptp_variance;
            rc = vtss_appl_ptp_clock_config_virtual_port_config_set(instance, &c);
        }
    }
    return rc;
}

mesa_rc ptp_set_virtual_port_local_priority(uint instance, u8 local_priority)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        vtss_appl_ptp_virtual_port_config_t c;
        if (vtss_appl_ptp_clock_config_virtual_port_config_get(instance, &c) == VTSS_RC_OK) {
            c.localPriority = local_priority;
            rc = vtss_appl_ptp_clock_config_virtual_port_config_set(instance, &c);
        }
    }
    return rc;
}

mesa_rc ptp_set_virtual_port_priority1(uint instance, u8 priority1)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        vtss_appl_ptp_virtual_port_config_t c;
        if (vtss_appl_ptp_clock_config_virtual_port_config_get(instance, &c) == VTSS_RC_OK) {
            c.priority1 = priority1;
            rc = vtss_appl_ptp_clock_config_virtual_port_config_set(instance, &c);
        }
    }
    return rc;
}

mesa_rc ptp_set_virtual_port_priority2(uint instance, u8 priority2)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        vtss_appl_ptp_virtual_port_config_t c;
        if (vtss_appl_ptp_clock_config_virtual_port_config_get(instance, &c) == VTSS_RC_OK) {
            c.priority2 = priority2;
            rc = vtss_appl_ptp_clock_config_virtual_port_config_set(instance, &c);
        }
    }
    return rc;
}

mesa_rc ptp_set_virtual_port_io_pin(uint instance, u16 io_pin, BOOL enable)
{
    PTP_CORE_LOCK_SCOPE();

    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        vtss_appl_ptp_virtual_port_config_t c;
        if (vtss_appl_ptp_clock_config_virtual_port_config_get(instance, &c) == VTSS_RC_OK) {
            c.enable = enable;
            // io-pin is only valid when it is enabled, in disable, use the already enabled io_pin number
            if (enable) {
                c.io_pin = io_pin;
                //rc = vtss_ptp_port_ena_virtual(ptp_global.ptpi[instance], l2port_to_ptp_api(mesa_cap_port_cnt));
            }
            T_I("inst %d, io_pin %d enable %d", instance, io_pin, enable);
            rc = vtss_appl_ptp_clock_config_virtual_port_config_set(instance, &c);
            if (mesa_cap_ts_has_ptp_io_pin) {
                rc = io_pin_instance_set(instance, c.io_pin, enable);
                T_I("set hook: source_id %d", ptp_io_pin[io_pin].source_id);
                if (VTSS_RC_OK != vtss_interrupt_source_hook_set(VTSS_MODULE_ID_PTP, io_pin_interrupt_handler, ptp_io_pin[io_pin].source_id, INTERRUPT_PRIORITY_NORMAL)) {
                    T_I("source already hooked");
                }
            }
        }
    }
    return rc;
}

mesa_rc ptp_clear_virtual_port_clock_class(uint instance)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        vtss_appl_ptp_virtual_port_config_t c;
        if (vtss_appl_ptp_clock_config_virtual_port_config_get(instance, &c) == VTSS_RC_OK) {
            c.clockQuality.clockClass = DEFAULT_CLOCK_CLASS;
            rc = vtss_appl_ptp_clock_config_virtual_port_config_set(instance, &c);
        }
    }
    return rc;
}

mesa_rc ptp_clear_virtual_port_clock_accuracy(uint instance)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        vtss_appl_ptp_virtual_port_config_t c;
        if (vtss_appl_ptp_clock_config_virtual_port_config_get(instance, &c) == VTSS_RC_OK) {
            c.clockQuality.clockAccuracy = 0xFE;
            rc = vtss_appl_ptp_clock_config_virtual_port_config_set(instance, &c);
        }
    }
    return rc;
}

mesa_rc ptp_clear_virtual_port_clock_variance(uint instance)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        vtss_appl_ptp_virtual_port_config_t c;
        if (vtss_appl_ptp_clock_config_virtual_port_config_get(instance, &c) == VTSS_RC_OK) {
            c.clockQuality.offsetScaledLogVariance = 0xffff;
            rc = vtss_appl_ptp_clock_config_virtual_port_config_set(instance, &c);
        }
    }
    return rc;
}

mesa_rc ptp_clear_virtual_port_local_priority(uint instance)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        vtss_appl_ptp_virtual_port_config_t c;
        if (vtss_appl_ptp_clock_config_virtual_port_config_get(instance, &c) == VTSS_RC_OK) {
            c.localPriority = 128;
            rc = vtss_appl_ptp_clock_config_virtual_port_config_set(instance, &c);
        }
    }
    return rc;
}

mesa_rc ptp_clear_virtual_port_priority1(uint instance)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        vtss_appl_ptp_virtual_port_config_t c;
        if (vtss_appl_ptp_clock_config_virtual_port_config_get(instance, &c) == VTSS_RC_OK) {
            c.priority1 = 128;
            rc = vtss_appl_ptp_clock_config_virtual_port_config_set(instance, &c);
        }
    }
    return rc;
}

mesa_rc ptp_clear_virtual_port_priority2(uint instance)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        vtss_appl_ptp_virtual_port_config_t c;
        if (vtss_appl_ptp_clock_config_virtual_port_config_get(instance, &c) == VTSS_RC_OK) {
            c.priority2 = 128;
            rc = vtss_appl_ptp_clock_config_virtual_port_config_set(instance, &c);
        }
    }
    return rc;
}

mesa_rc ptp_set_clock_class(uint instance, u8 ptp_class)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        ptp_global.ptpi[instance]->announced_clock_quality.clockClass = ptp_class;
        ptp_global.ptpi[instance]->defaultDS.status.clockQuality.clockClass = ptp_class;
        defaultDSChanged(ptp_global.ptpi[instance]);
        rc = VTSS_RC_OK;
    }
    return rc;
}

mesa_rc ptp_set_clock_accuracy(uint instance, u8 ptp_accuracy)
{
    PTP_CORE_LOCK_SCOPE();

    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        ptp_global.ptpi[instance]->announced_clock_quality.clockAccuracy = ptp_accuracy;
        ptp_global.ptpi[instance]->defaultDS.status.clockQuality.clockAccuracy = ptp_accuracy;
        defaultDSChanged(ptp_global.ptpi[instance]);
        rc = VTSS_RC_OK;
    }
    return rc;
}

mesa_rc ptp_set_clock_variance(uint instance, u16 ptp_variance)
{
    PTP_CORE_LOCK_SCOPE();

    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        ptp_global.ptpi[instance]->announced_clock_quality.offsetScaledLogVariance = ptp_variance;
        ptp_global.ptpi[instance]->defaultDS.status.clockQuality.offsetScaledLogVariance = ptp_variance;
        defaultDSChanged(ptp_global.ptpi[instance]);
        rc = VTSS_RC_OK;
    }
    return rc;
}

mesa_rc ptp_clear_clock_class(uint instance)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        ptp_global.ptpi[instance]->announced_clock_quality.clockClass = DEFAULT_CLOCK_CLASS;
        ptp_global.ptpi[instance]->defaultDS.status.clockQuality.clockClass = DEFAULT_CLOCK_CLASS;
        defaultDSChanged(ptp_global.ptpi[instance]);
        rc = VTSS_RC_OK;
    }
    return rc;
}

mesa_rc ptp_clear_clock_accuracy(uint instance)
{
    PTP_CORE_LOCK_SCOPE();

    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        ptp_global.ptpi[instance]->announced_clock_quality.clockAccuracy = 0xFE;
        ptp_global.ptpi[instance]->defaultDS.status.clockQuality.clockAccuracy = 0xFE;
        defaultDSChanged(ptp_global.ptpi[instance]);
        rc = VTSS_RC_OK;
    }
    return rc;
}

mesa_rc ptp_clear_clock_variance(uint instance)
{
    PTP_CORE_LOCK_SCOPE();

    mesa_rc rc = VTSS_RC_ERROR;
    if (instance < PTP_CLOCK_INSTANCES) {
        ptp_global.ptpi[instance]->announced_clock_quality.offsetScaledLogVariance = 0xffff;
        ptp_global.ptpi[instance]->defaultDS.status.clockQuality.offsetScaledLogVariance = 0xffff;
        defaultDSChanged(ptp_global.ptpi[instance]);
        rc = VTSS_RC_OK;
    }
    return rc;
}

mesa_rc ptp_clock_clockidentity_set(uint instance, vtss_appl_clock_identity *clockIdentity)
{
    PTP_CORE_LOCK_SCOPE();
    memcpy(&config_data.conf[instance].clock_init.clockIdentity, clockIdentity, sizeof(*clockIdentity));
    return VTSS_RC_OK;
}


#define HAS_SERVO_380(expr) ((servo_type == VTSS_ZARLINK_SERVO_ZLS30380 && (expr)) || false)
#define HAS_SERVO_380_384(expr) (((servo_type == VTSS_ZARLINK_SERVO_ZLS30380 || servo_type == VTSS_ZARLINK_SERVO_ZLS30384) && (expr)) || false)
#define HAS_ANY_SERVO(expr) (((servo_type == VTSS_ZARLINK_SERVO_ZLS30380 || servo_type == VTSS_ZARLINK_SERVO_ZLS30384 || servo_type == VTSS_ZARLINK_SERVO_ZLS30387) && (expr)) || false)

mesa_rc vtss_appl_ptp_clock_config_default_ds_set(uint instance, const vtss_appl_ptp_clock_config_default_ds_t *default_ds_cfg)
{
    vtss_zarlink_servo_t servo_type = (vtss_zarlink_servo_t)vtss_appl_cap_zarlink_servo_type;
    /*lint -save -e685 -e568 */
    if (((default_ds_cfg->deviceType >= VTSS_APPL_PTP_DEVICE_NONE) && (default_ds_cfg->deviceType < VTSS_APPL_PTP_DEVICE_MAX_TYPE)) &&
        ((default_ds_cfg->twoStepFlag == 0) || (default_ds_cfg->twoStepFlag == 1)) &&
        ((default_ds_cfg->oneWay == 0) || (default_ds_cfg->oneWay == 1)) &&
        (default_ds_cfg->domainNumber <= 127) &&
#if defined(VTSS_SW_OPTION_MEP)
        ((mesa_cap_oam_used_as_ptp_protocol && (default_ds_cfg->protocol >= VTSS_APPL_PTP_PROTOCOL_ETHERNET) && (default_ds_cfg->protocol < VTSS_APPL_PTP_PROTOCOL_MAX_TYPE)) ||
         (!mesa_cap_oam_used_as_ptp_protocol && (default_ds_cfg->protocol >= VTSS_APPL_PTP_PROTOCOL_ETHERNET) && (default_ds_cfg->protocol < VTSS_APPL_PTP_PROTOCOL_MAX_TYPE) && (default_ds_cfg->protocol != VTSS_APPL_PTP_PROTOCOL_OAM))) &&
#else
        ((default_ds_cfg->protocol >= VTSS_APPL_PTP_PROTOCOL_ETHERNET) && (default_ds_cfg->protocol < VTSS_APPL_PTP_PROTOCOL_MAX_TYPE) && (default_ds_cfg->protocol != VTSS_APPL_PTP_PROTOCOL_OAM)) &&
#endif
        ((default_ds_cfg->configured_vid >= 1) && (default_ds_cfg->configured_vid <= 4095)) &&
        ((default_ds_cfg->configured_pcp >= 0) && (default_ds_cfg->configured_pcp <= 7)) &&
        ((default_ds_cfg->dscp >= 0) && (default_ds_cfg->dscp <= 0x3f)) &&
        ((default_ds_cfg->mep_instance >= 1) && (default_ds_cfg->mep_instance <= 100)) &&
        ((default_ds_cfg->clock_domain >= 0) && (default_ds_cfg->clock_domain <= 3)) &&
        (((default_ds_cfg->profile >= VTSS_APPL_PTP_PROFILE_NO_PROFILE) && (default_ds_cfg->profile <= VTSS_APPL_PTP_PROFILE_G_8265_1)) ||
         (default_ds_cfg->profile == VTSS_APPL_PTP_PROFILE_G_8275_1) ||



         false) &&
        (
         HAS_ANY_SERVO(default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_BASIC_PHASE || \
            default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_BASIC_PHASE_LOW) ||
         HAS_SERVO_380(default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_DEFAULT || \
             default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_FREQ_XO || \
             default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_XO || \
             default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_FREQ_TCXO || \
             default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_TCXO || \
             default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_FREQ_OCXO_S3E || \
             default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E) ||
         HAS_SERVO_380_384(default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_FREQ || \
             default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_PHASE) ||
         HAS_ANY_SERVO(default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_FREQ) ||
         HAS_SERVO_380_384(default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE) ||
         HAS_SERVO_380(default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_FREQ_ACCURACY_FDD || \
             default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_FREQ_ACCURACY_XDSL) ||
         HAS_SERVO_380_384(default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_ELEC_FREQ || \
             default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_ELEC_PHASE) ||
         HAS_SERVO_380(default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_TCXO_Q || \
             default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_Q) ||
         HAS_SERVO_380_384(default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_RELAXED_C60W || \
             default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_RELAXED_C150 || \
             default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_RELAXED_C180 || \
             default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_RELAXED_C240) ||
         HAS_SERVO_380(default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_BETA || \
            default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E_R4_6_1) ||
#ifdef SW_OPTION_BASIC_PTP_SERVO
         default_ds_cfg->filter_type == PTP_FILTERTYPE_BASIC ||
#endif
         false
        ))
    {
        /*lint -restore */
        mesa_rc rc = VTSS_RC_ERROR;
        if (instance < PTP_CLOCK_INSTANCES) {
            PTP_CORE_LOCK_SCOPE();
            if (config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_NONE && (default_ds_cfg->deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND || default_ds_cfg->deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY)) {
                T_I("slave clock exists ? %d", slave_clock_exists[default_ds_cfg->clock_domain]);
                // if (slave_clock_exists[default_ds_cfg->clock_domain]) {
                //     T_I("slave clock exists [%d]", default_ds_cfg->clock_domain);
                //     return PTP_RC_MULTIPLE_SLAVES;
                // } else {
                ++slave_clock_exists[default_ds_cfg->clock_domain];
                T_I("new slave clock [%d] inst_cnt %u", default_ds_cfg->clock_domain, slave_clock_exists[default_ds_cfg->clock_domain]);
                // }
            }

            if (PTP_READY()) {
                if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
                    rc = VTSS_RC_OK;
                    if ((config_data.conf[instance].clock_init.cfg.deviceType == default_ds_cfg->deviceType) &&          // Check that deviceType is not modified.
                        (config_data.conf[instance].clock_init.cfg.mep_instance == default_ds_cfg->mep_instance) &&      // Check that mep_instance is not modified (req. may be relaxed later).
                        (config_data.conf[instance].clock_init.cfg.clock_domain == default_ds_cfg->clock_domain))        // Check that clock_id is not modified (req. may be relaxed later).
                    {
                        if ((config_data.conf[instance].clock_init.cfg.domainNumber != default_ds_cfg->domainNumber) ||      // If the domainNumber is changed the clock needs to be recreated
                            (config_data.conf[instance].clock_init.cfg.twoStepFlag != default_ds_cfg->twoStepFlag) ||        // If the twoStepFlag flag is changed the clock needs to be recreated
                            (config_data.conf[instance].clock_init.cfg.oneWay != default_ds_cfg->oneWay) ||                  // If the oneWay flag is changed the clock needs to be recreated
                            (config_data.conf[instance].clock_init.cfg.protocol != default_ds_cfg->protocol) ||              // If the protocol is changed the clock needs to be recreated
                            (config_data.conf[instance].clock_init.cfg.configured_vid != default_ds_cfg->configured_vid) ||  // If the configured VID is changed the clock needs to be recreated
                            (config_data.conf[instance].clock_init.cfg.configured_pcp != default_ds_cfg->configured_pcp) ||  // If the configured PCP is changed the clock needs to be recreated
                            (config_data.conf[instance].clock_init.cfg.dscp != default_ds_cfg->dscp) ||                      // If the dscp is changed the clock needs to be recreated
                            (config_data.conf[instance].clock_init.cfg.mep_instance != default_ds_cfg->mep_instance))        // If the mep instance is changed the clock needs to be recreated
                        {
                            config_data.conf[instance].clock_init.cfg = *default_ds_cfg;
                            //update_ptp_ip_address(instance, default_ds_cfg->configured_vid);
                            //vtss_ptp_clock_create(ptp_global.ptpi[instance]);
                            ptp_global.ptpi[instance]->ssm.servo->deactivate(config_data.conf[instance].clock_init.cfg.clock_domain);  // ptp_clock_create is going to create this PTP clock again. Before doing this, de-activate the filter/servo instance.
                            current_servo_mode_ref[instance].mode = VTSS_PTP_SERVO_NONE;
                            // re-create clock instance, if it is a transparent clock, we need til simulate thet the clock does not exits
                            if (config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT || config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT) {
                                transparent_clock_exists = false;
                            }
                            rc = ptp_clock_create(&config_data.conf[instance].clock_init, instance);
                            if (rc != VTSS_RC_OK) {
                                config_data.conf[instance].clock_init.cfg.deviceType = VTSS_APPL_PTP_DEVICE_NONE;
                            }
                        }
                        else {
                            // If other clocks exist in the same hardware clock domain, the value filter_type must not be changed.
                            for (int i = 0; i < PTP_CLOCK_INSTANCES; i++) {
                                if ((i != instance) && 
                                    (config_data.conf[i].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) &&
                                    (config_data.conf[i].clock_init.cfg.clock_domain == config_data.conf[instance].clock_init.cfg.clock_domain) &&
                                    (default_ds_cfg->filter_type != config_data.conf[i].clock_init.cfg.filter_type))
                                {
                                    T_W("Another clock exists in the same hardware clock domain. Filter type cannot be changed.");
                                    return VTSS_RC_ERROR;
                                }
                            }
                            if (default_ds_cfg->filter_type != config_data.conf[instance].clock_init.cfg.filter_type) {
                                ptp_global.ptpi[instance]->ssm.servo->deactivate(config_data.conf[instance].clock_init.cfg.clock_domain);
                                current_servo_mode_ref[instance].mode = VTSS_PTP_SERVO_NONE;
#ifdef SW_OPTION_BASIC_PTP_SERVO
                                if (default_ds_cfg->filter_type == PTP_FILTERTYPE_BASIC) {
                                    (void) vtss_ptp_clock_servo_set(ptp_global.ptpi[instance], basic_servo[instance]);
                                } else
#endif // SW_OPTION_BASIC_PTP_SERVO
#if defined(VTSS_SW_OPTION_ZLS30387)
                                if (default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_DEFAULT ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_FREQ_XO ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_XO ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_FREQ_TCXO ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_TCXO ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_FREQ_OCXO_S3E ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_FREQ ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_PHASE ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_FREQ ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_FREQ_ACCURACY_FDD ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_FREQ_ACCURACY_XDSL ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_ELEC_FREQ ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_ELEC_PHASE ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_TCXO_Q ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_Q ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_RELAXED_C60W ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_RELAXED_C150 ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_RELAXED_C180 ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_RELAXED_C240 ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_BETA ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E_R4_6_1 ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_BASIC_PHASE ||
                                    default_ds_cfg->filter_type == PTP_FILTERTYPE_ACI_BASIC_PHASE_LOW )
                                {
                                    (void) vtss_ptp_clock_servo_set(ptp_global.ptpi[instance], advanced_servo[instance]);
                                } else
#endif
                                {
                                    return VTSS_RC_ERROR;
                                }
                                config_data.conf[instance].clock_init.cfg.filter_type = default_ds_cfg->filter_type;
                                ptp_global.ptpi[instance]->ssm.servo->activate(config_data.conf[instance].clock_init.cfg.clock_domain);
                            }
                            config_data.conf[instance].clock_init.cfg = *default_ds_cfg;
                            rc = vtss_ptp_set_clock_default_ds_cfg(ptp_global.ptpi[instance], default_ds_cfg);
                        }
                        if (rc == VTSS_RC_OK) {
                            if (VTSS_RC_OK != ptp_ace_update(instance)) {
                                T_IG(_I,"ptp_ace_update failed");
                            }
                        }
                    }
                }
                else {
                    if (default_ds_cfg->deviceType != VTSS_APPL_PTP_DEVICE_NONE) {  // Do not allow a clock of deviceType == VTSS_APPL_PTP_DEVICE_NONE to be created.
                        config_data.conf[instance].clock_init.cfg = *default_ds_cfg;
                        //if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE && config_data.conf[instance].clock_init.cfg.clock_domain == 0) {
                        //    rc = vtss_local_clock_adj_method(config_data.init_ext_clock_mode.adj_method, config_data.conf[instance].clock_init.cfg.profile);
                        //}
                        //ptp_global.ptpi[instance]->ssm.servo->activate(config_data.conf[instance].clock_init.cfg.clock_domain);
                        T_IG(_I,"activate new instance: filter type = %d, clock_domain = %d", default_ds_cfg->filter_type, config_data.conf[instance].clock_init.cfg.clock_domain);
                        rc = ptp_clock_create(&config_data.conf[instance].clock_init, instance);
                        if (rc != VTSS_RC_OK) {
                            config_data.conf[instance].clock_init.cfg.deviceType = VTSS_APPL_PTP_DEVICE_NONE;
                        }
                    }
                }
            } else {
                config_data.conf[instance].clock_init.cfg = *default_ds_cfg;
                rc = VTSS_RC_OK;
            }
        }
        return rc;
    }
    else {
        T_W("Invalid parameter");
        return VTSS_RC_ERROR;  // One or more paramters were invalid.
    }
}

mesa_rc vtss_appl_ptp_clock_slave_config_get(uint instance, vtss_appl_ptp_clock_slave_config_t *const cfg)
{
    PTP_CORE_LOCK_SCOPE();
    
    if (instance < PTP_CLOCK_INSTANCES) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            *cfg = config_data.conf[instance].slave_cfg;
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

mesa_rc vtss_appl_ptp_clock_slave_config_set(uint instance, const vtss_appl_ptp_clock_slave_config_t *const cfg)
{
    PTP_CORE_LOCK_SCOPE();
    
    /*lint -save -e685 -e568 */
    if (((cfg->stable_offset >= 0) && (cfg->stable_offset <= 1000000)) &&
        ((cfg->offset_ok >= 0) && (cfg->offset_ok <= 1000000)) &&
        ((cfg->offset_fail >= 0) && (cfg->offset_fail <= 1000000)))
    {
        /*lint -restore */
        if (instance < PTP_CLOCK_INSTANCES) {
            if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
                config_data.conf[instance].slave_cfg = *cfg;
                vtss_ptp_clock_slave_config_set(ptp_global.ptpi[instance]->ssm.servo, cfg);
                return VTSS_RC_OK;
            }
        }
    }
    return VTSS_RC_ERROR;
}

void vtss_appl_ptp_clock_slave_default_config_get(vtss_appl_ptp_clock_slave_config_t *cfg)
{
    cfg->offset_fail = 3000;
    cfg->offset_ok = 1000;
    cfg->stable_offset = 1000;
}

mesa_rc vtss_appl_ptp_clock_status_current_ds_get(uint instance, vtss_appl_ptp_clock_current_ds_t *const status)
{
    PTP_CORE_LOCK_SCOPE();
    
    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            vtss_ptp_get_clock_current_ds(ptp_global.ptpi[instance], status);
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

mesa_rc vtss_appl_ptp_clock_status_parent_ds_get(uint instance, vtss_appl_ptp_clock_parent_ds_t *const status)
{
    PTP_CORE_LOCK_SCOPE();
    
    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            vtss_ptp_get_clock_parent_ds(ptp_global.ptpi[instance], status);
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

mesa_rc vtss_appl_ptp_clock_status_timeproperties_ds_get(uint instance, vtss_appl_ptp_clock_timeproperties_ds_t *const timeproperties_ds)
{
    PTP_CORE_LOCK_SCOPE();

    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            vtss_ptp_get_clock_timeproperties_ds(ptp_global.ptpi[instance], timeproperties_ds);
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

mesa_rc vtss_appl_ptp_clock_config_timeproperties_ds_get(uint instance, vtss_appl_ptp_clock_timeproperties_ds_t *const timeproperties_ds)
{
    PTP_CORE_LOCK_SCOPE();

    if (instance < PTP_CLOCK_INSTANCES) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            *timeproperties_ds = config_data.conf[instance].time_prop;
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

static void clock_default_timeproperties_ds_get(vtss_appl_ptp_clock_timeproperties_ds_t *timeproperties_ds)
{
    timeproperties_ds->currentUtcOffset = DEFAULT_UTC_OFFSET;
    timeproperties_ds->currentUtcOffsetValid = false;
    timeproperties_ds->leap59 = false;
    timeproperties_ds->leap61 = false;
    timeproperties_ds->timeTraceable = false;
    timeproperties_ds->frequencyTraceable = false;
    timeproperties_ds->ptpTimescale = true; /* default in ITU profile */
    timeproperties_ds->timeSource = 0xa0; /* indicates internal oscillator */
    timeproperties_ds->pendingLeap = false;
    timeproperties_ds->leapDate = 0;
    timeproperties_ds->leapType = VTSS_APPL_PTP_LEAP_SECOND_61;
}

void ptp_clock_default_timeproperties_ds_get(vtss_appl_ptp_clock_timeproperties_ds_t *timeproperties_ds)
{
    PTP_CORE_LOCK_SCOPE();
    clock_default_timeproperties_ds_get(timeproperties_ds);
    return;
}

mesa_rc vtss_appl_ptp_clock_config_timeproperties_ds_set(uint instance, const vtss_appl_ptp_clock_timeproperties_ds_t *const timeproperties_ds)
{
    PTP_CORE_LOCK_SCOPE();
    
    if (((timeproperties_ds->currentUtcOffsetValid == 0) || (timeproperties_ds->currentUtcOffsetValid == 1)) &&
        ((timeproperties_ds->leap59 == 0) || (timeproperties_ds->leap59 == 1)) &&
        ((timeproperties_ds->leap61 == 0) || (timeproperties_ds->leap61 == 1)) &&
        ((timeproperties_ds->timeTraceable == 0) || (timeproperties_ds->timeTraceable == 1)) &&
        ((timeproperties_ds->frequencyTraceable == 0) || (timeproperties_ds->frequencyTraceable == 1)) &&
        ((timeproperties_ds->ptpTimescale == 0) || (timeproperties_ds->ptpTimescale == 1)) &&
        ((timeproperties_ds->pendingLeap == 0) || (timeproperties_ds->pendingLeap == 1)) &&
        ((timeproperties_ds->leapType == VTSS_APPL_PTP_LEAP_SECOND_59) || (timeproperties_ds->leapType == VTSS_APPL_PTP_LEAP_SECOND_61)))
    {
        if (instance < PTP_CLOCK_INSTANCES) {
            config_data.conf[instance].time_prop = *timeproperties_ds;
            if (PTP_READY()) {
                if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
                    vtss_ptp_set_clock_timeproperties_ds(ptp_global.ptpi[instance], timeproperties_ds);
                    return VTSS_RC_OK;
                }
            } else return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

mesa_rc vtss_appl_ptp_status_clocks_port_ds_get(uint instance, vtss_ifindex_t portnum, vtss_appl_ptp_status_port_ds_t *const port_ds)
{
    PTP_CORE_LOCK_SCOPE();
    
    u32 v;
    VTSS_RC(ptp_ifindex_to_port(portnum, &v));
    if ((instance < PTP_CLOCK_INSTANCES) && (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE)) {
        if (vtss_ptp_get_port_status(ptp_global.ptpi[instance], iport2uport(v), port_ds) == VTSS_RC_OK) {
            /* special handling for port state in a OAM Master */
            if (config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY &&
                config_data.conf[instance].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM)
            {
                if (port_data[v].link_state) {
                    port_ds->portState = VTSS_APPL_PTP_MASTER;
                } else {
                    port_ds->portState = VTSS_APPL_PTP_DISABLED;
                }
            }
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

mesa_rc vtss_appl_ptp_status_clocks_port_statistics_get(uint instance, vtss_ifindex_t portnum, vtss_appl_ptp_status_port_statistics_t *const port_statistics)
{
    PTP_CORE_LOCK_SCOPE();
    u32 v;
    VTSS_RC(ptp_ifindex_to_port(portnum, &v));
    if ((instance < PTP_CLOCK_INSTANCES) && (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE)) {
        if (vtss_ptp_get_port_statistics(ptp_global.ptpi[instance], iport2uport(v), port_statistics, FALSE) == VTSS_RC_OK) {
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

mesa_rc vtss_appl_ptp_status_clocks_port_statistics_get_clear(uint instance, vtss_ifindex_t portnum, vtss_appl_ptp_status_port_statistics_t *const port_statistics)
{
    PTP_CORE_LOCK_SCOPE();
    u32 v;
    VTSS_RC(ptp_ifindex_to_port(portnum, &v));
    if ((instance < PTP_CLOCK_INSTANCES) && (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE)) {
        if (vtss_ptp_get_port_statistics(ptp_global.ptpi[instance], iport2uport(v), port_statistics, TRUE) == VTSS_RC_OK) {
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

mesa_rc vtss_appl_ptp_status_clocks_virtual_port_ds_get(uint instance, u32 portnum, vtss_appl_ptp_status_port_ds_t *const port_ds)
{
    PTP_CORE_LOCK_SCOPE();

    if ((instance < PTP_CLOCK_INSTANCES) && (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE)) {
        if (vtss_ptp_get_port_status(ptp_global.ptpi[instance], iport2uport(portnum), port_ds) == VTSS_RC_OK) {
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

void vtss_appl_ptp_clock_config_default_virtual_port_config_get(vtss_appl_ptp_virtual_port_config_t *const c)
{
    c->clockQuality.clockClass = DEFAULT_CLOCK_CLASS;
    c->clockQuality.clockAccuracy = 0xfe;
    c->clockQuality.offsetScaledLogVariance = 0xFFFF;
    c->localPriority = 128;
    c->priority1 = 128;
    c->priority2 = 128;
    c->enable = FALSE;
    c->io_pin = 0;
}

mesa_rc vtss_appl_ptp_clock_config_virtual_port_config_get(uint instance, vtss_appl_ptp_virtual_port_config_t *const c)
{
    if (instance < PTP_CLOCK_INSTANCES) {
        *c = config_data.conf[instance].virtual_port_cfg;
        return VTSS_RC_OK;
    }
    return VTSS_RC_ERROR;
}

mesa_rc ptp_clock_config_virtual_port_config_get(uint instance, vtss_appl_ptp_virtual_port_config_t *const c)
{
    PTP_CORE_LOCK_SCOPE();
    return vtss_appl_ptp_clock_config_virtual_port_config_get(instance, c);
}

mesa_rc vtss_appl_ptp_clock_config_virtual_port_config_set(uint instance, const vtss_appl_ptp_virtual_port_config_t *const c)
{
    if ((instance < PTP_CLOCK_INSTANCES) &&
        (c->io_pin >= 0 && c->io_pin <= 3) &&
        (c->priority1 == 128))
    {
        config_data.conf[instance].virtual_port_cfg = *c;
        return VTSS_RC_OK;
    }
    return VTSS_RC_ERROR;
}

mesa_rc vtss_appl_ptp_config_clocks_port_ds_set(uint instance, vtss_ifindex_t portnum, const vtss_appl_ptp_config_port_ds_t *port_ds)
{
    PTP_CORE_LOCK_SCOPE();
    
    u32 v;
    VTSS_RC(ptp_ifindex_to_port(portnum, &v));
    if (((port_ds->enabled == 0) || (port_ds->enabled == 1)) &&
        (((port_ds->logAnnounceInterval >= -3) && (port_ds->logAnnounceInterval <= 4)) || (config_data.conf[instance].clock_init.cfg.profile == VTSS_APPL_PTP_PROFILE_IEEE_802_1AS && (port_ds->logAnnounceInterval == 126 || port_ds->logAnnounceInterval ==127))) &&
        ((port_ds->announceReceiptTimeout >= 1) && (port_ds->announceReceiptTimeout <= 10)) &&
        (((port_ds->logSyncInterval >= -7) && (port_ds->logSyncInterval <= 4)) || (config_data.conf[instance].clock_init.cfg.profile == VTSS_APPL_PTP_PROFILE_IEEE_802_1AS && (port_ds->logSyncInterval == 126 || port_ds->logSyncInterval ==127))) &&
        ((port_ds->delayMechanism == DELAY_MECH_E2E) || (port_ds->delayMechanism == DELAY_MECH_P2P) || (port_ds->delayMechanism == DELAY_MECH_DISABLED)) &&
        (((port_ds->logMinPdelayReqInterval >= -7) && (port_ds->logMinPdelayReqInterval <= 5)) || (config_data.conf[instance].clock_init.cfg.profile == VTSS_APPL_PTP_PROFILE_IEEE_802_1AS && (port_ds->logMinPdelayReqInterval == 126 || port_ds->logMinPdelayReqInterval ==127))) &&
        ((port_ds->delayAsymmetry >= (-100000LL<<16)) && (port_ds->delayAsymmetry <= (100000LL<<16))) &&
        ((port_ds->portInternal == 0) || (port_ds->portInternal == 1)) &&
        ((port_ds->ingressLatency >= (-100000LL<<16)) && (port_ds->ingressLatency <= (100000LL<<16))) &&
        ((port_ds->egressLatency >= (-100000LL<<16)) && (port_ds->egressLatency <= (100000LL<<16))) &&
        (port_ds->versionNumber ==  2) &&
        ((port_ds->twoStepOverride >= VTSS_APPL_PTP_TWO_STEP_OVERRIDE_NONE) && (port_ds->twoStepOverride <= VTSS_APPL_PTP_TWO_STEP_OVERRIDE_TRUE)))
    {
        mesa_rc rc = VTSS_RC_ERROR;
        bool temp_int = false;
        u8 temp;
        bool ena = false, disa = false;
        /* Before doing anything else, check if port needs to be enabled or disabled */
        T_DG(_I,"instance %d, port %d, new ena %d, old ena %d, domain %d", instance, v, port_ds->enabled, config_data.conf[instance].port_config[v].enabled,  port_data[v].port_domain);
        if ((port_ds->enabled == 1) && config_data.conf[instance].port_config[v].enabled == 0) {
            ena = true;
        }
        else if ((port_ds->enabled == 0) && config_data.conf[instance].port_config[v].enabled == 1) {
            disa= true;
        }
        if (ena) {
            if (ptp_port_ena(port_ds->portInternal, iport2uport(v), instance) == VTSS_RC_ERROR) return rc;
        }
        else if (disa) {
            if (ptp_port_dis(iport2uport(v), instance) == VTSS_RC_ERROR) return rc;
        }

        if (instance < PTP_CLOCK_INSTANCES) {
            /* preserve initportState, and portInternal state */
            temp_int = config_data.conf[instance].port_config[v].portInternal;
            temp = config_data.conf[instance].port_config[v].enabled;
            if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
                PTP_RETURN(vtss_1588_ingress_latency_set(l2port_to_ptp_api(v), port_ds->ingressLatency));
                PTP_RETURN(vtss_1588_egress_latency_set(l2port_to_ptp_api(v), port_ds->egressLatency));
                PTP_RETURN(vtss_1588_asymmetry_set(l2port_to_ptp_api(v), port_ds->delayAsymmetry));
            }
            config_data.conf[instance].port_config[v] = *port_ds;
            config_data.conf[instance].port_config[v].enabled = temp;
            config_data.conf[instance].port_config[v].portInternal = temp_int;
            if (PTP_READY()) {
                if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
                    rc = vtss_ptp_set_port_cfg(ptp_global.ptpi[instance], iport2uport(v), port_ds);
                }
                if (rc == VTSS_RC_OK) {
                    if ((rc = ptp_ace_update(instance)) != VTSS_RC_OK) {
                        if (rc != PTP_RC_MISSING_IP_ADDRESS) {
                            if (ptp_port_dis(iport2uport(v), instance) != VTSS_RC_OK) return rc;
                        }
                        T_IG(_I,"ptp_ace_update failed");
                    }
                }
            } else {
                rc = VTSS_RC_OK;
            }
        }
        return rc;
    }
    else return VTSS_RC_ERROR;
}

void ptp_apply_profile_defaults_to_port_ds(vtss_appl_ptp_config_port_ds_t *port_ds, vtss_appl_ptp_profile_t profile)
{
    vtss_ptp_apply_profile_defaults_to_port_ds(port_ds, profile);
}

mesa_rc vtss_appl_ptp_config_clocks_port_ds_get(uint instance, vtss_ifindex_t portnum, vtss_appl_ptp_config_port_ds_t *port_ds)
{
    PTP_CORE_LOCK_SCOPE();
    
    u32 v;
    VTSS_RC(ptp_ifindex_to_port(portnum, &v));
    if ((instance < PTP_CLOCK_INSTANCES) /* && (0 <= v) */ && (v < config_data.conf[instance].clock_init.numberPorts)) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            if (PTP_READY()) {
                vtss_1588_ingress_latency_get(l2port_to_ptp_api(v), &config_data.conf[instance].port_config[v].ingressLatency);
                vtss_1588_egress_latency_get(l2port_to_ptp_api(v), &config_data.conf[instance].port_config[v].egressLatency);
                vtss_1588_asymmetry_get(l2port_to_ptp_api(v), &config_data.conf[instance].port_config[v].delayAsymmetry);
            }
            *port_ds = config_data.conf[instance].port_config[v];
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

bool ptp_get_port_foreign_ds(ptp_foreign_ds_t *f_ds, int portnum, i16 ix, uint instance)
{
    PTP_CORE_LOCK_SCOPE();

    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            return vtss_ptp_get_port_foreign_ds(ptp_global.ptpi[instance], portnum, ix, f_ds);
        }
    }
    return false;
}

/**
 * \brief Set filter parameters for a Default PTP filter instance.
 *
 */
mesa_rc vtss_appl_ptp_clock_filter_parameters_set(uint instance, const vtss_appl_ptp_clock_filter_config_t *const c)
{
    PTP_CORE_LOCK_SCOPE();
    
    /*lint -save -e685 -e568 */
    if (((c->delay_filter >= 0) && (c->delay_filter <= 6)) &&
        ((c->period >= 1) && (c->period <= 10000)) &&
        ((c->dist >= 0) && (c->dist <= 10)))
    {
        /*lint -restore */
        if (PTP_READY() && (config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_NONE)) return VTSS_RC_ERROR;

        if (instance < PTP_CLOCK_INSTANCES) {
            config_data.conf[instance].filter_params = *c;
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

/**
 * \brief Get filter parameters for a PTP filter instance.
 *
 */
mesa_rc vtss_appl_ptp_clock_filter_parameters_get(uint instance, vtss_appl_ptp_clock_filter_config_t *const c)
{
    PTP_CORE_LOCK_SCOPE();

    if (instance < PTP_CLOCK_INSTANCES) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            *c = config_data.conf[instance].filter_params;
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

/**
 * \brief Get default filter parameters for a Default PTP filter instance.
 *
 */
void vtss_appl_ptp_filter_default_parameters_get(vtss_appl_ptp_clock_filter_config_t *c, vtss_appl_ptp_profile_t profile)
{
    switch (profile) {
        case VTSS_APPL_PTP_PROFILE_G_8275_1:
            c->delay_filter = DEFAULT_DELAY_S;
            c->period = 1;
            c->dist = 0;
            break;
        case VTSS_APPL_PTP_PROFILE_NO_PROFILE:
        case  VTSS_APPL_PTP_PROFILE_1588:
        case  VTSS_APPL_PTP_PROFILE_G_8265_1:
        case  VTSS_APPL_PTP_PROFILE_IEEE_802_1AS:
        default:
            c->delay_filter = DEFAULT_DELAY_S;
            c->period = 1;
            c->dist = 2;
            break;
    }
}

/**
 * \brief Set filter parameters for a Default PTP servo instance.
 *
 */
mesa_rc vtss_appl_ptp_clock_servo_parameters_set(uint instance, const vtss_appl_ptp_clock_servo_config_t *const c)
{
    PTP_CORE_LOCK_SCOPE();
    
    if (((c->display_stats == 0) || (c->display_stats == 1)) &&
        ((c->p_reg == 0) || (c->p_reg == 1)) &&
        ((c->i_reg == 0) || (c->i_reg == 1)) &&
        ((c->d_reg == 0) || (c->d_reg == 1)) &&
        ((c->ap >= 1) && (c->ap <= 1000)) &&
        ((c->ai >= 1) && (c->ai <= 10000)) &&
        ((c->ad >= 1) && (c->ad <= 10000)) &&
        ((c->gain >= 1) && (c->gain <= 10000)) &&
        ((c->srv_option == VTSS_APPL_PTP_CLOCK_FREE) || (c->srv_option == VTSS_APPL_PTP_CLOCK_SYNCE)) &&
        ((c->synce_threshold >= 1) && (c->synce_threshold <= 1000)) &&
        ((c->synce_ap >= 1) && (c->synce_ap <= 40)) &&
        ((c->ho_filter >= 10) && (c->ho_filter <= 86400)) &&
        ((c->stable_adj_threshold >= 1) && (c->stable_adj_threshold <= 1000)))
    {
        if (instance < PTP_CLOCK_INSTANCES) {
            config_data.conf[instance].servo_params = *c;
            if (PTP_READY()) {              
                if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) return VTSS_RC_OK;
            }
            else return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

/**
 * \brief Clear (i.e. reset) the servo of a PTP instance.
 *
 */
mesa_rc vtss_appl_ptp_clock_servo_clear(uint instance)
{
    PTP_CORE_LOCK_SCOPE();
    
    if (instance < PTP_CLOCK_INSTANCES) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
#ifdef SW_OPTION_BASIC_PTP_SERVO
            if (config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_BASIC) {
                basic_servo[instance]->clock_servo_reset(SET_VCXO_FREQ);
            }
#endif // SW_OPTION_BASIC_PTP_SERVO
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

/**
 * \brief Get filter parameters for a PTP servo instance.
 *
 */
mesa_rc vtss_appl_ptp_clock_servo_parameters_get(uint instance, vtss_appl_ptp_clock_servo_config_t *const c)
{
    PTP_CORE_LOCK_SCOPE();
    
    if (instance < PTP_CLOCK_INSTANCES) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            *c = config_data.conf[instance].servo_params;
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

/**
 * \brief Get default filter parameters for a Default PTP servo
 *
 */
void vtss_appl_ptp_clock_servo_default_parameters_get(vtss_appl_ptp_clock_servo_config_t *c, vtss_appl_ptp_profile_t profile)
{
    switch (profile) {
        case VTSS_APPL_PTP_PROFILE_G_8275_1:
            c->display_stats = false;
            c->p_reg = true;
            c->i_reg = true;
            c->d_reg = true;
            c->ap = 12;
            c->ai = 512;
            c->ad = 7;
            c->gain = 16;
            c->srv_option = VTSS_APPL_PTP_CLOCK_FREE;
            c->synce_threshold = 1000;
            c->synce_ap = 2;
            c->ho_filter = 960;
            c->stable_adj_threshold = 1000;  /* = 100 ppb */
            break;
        case VTSS_APPL_PTP_PROFILE_NO_PROFILE:
        case  VTSS_APPL_PTP_PROFILE_1588:
        case  VTSS_APPL_PTP_PROFILE_G_8265_1:
        case  VTSS_APPL_PTP_PROFILE_IEEE_802_1AS:
        default:
            c->display_stats = false;
            c->p_reg = true;
            c->i_reg = true;
            c->d_reg = true;
            c->ap = DEFAULT_AP;
            c->ai = DEFAULT_AI;
            c->ad = DEFAULT_AD;
            c->gain = 1;
            c->srv_option = VTSS_APPL_PTP_CLOCK_FREE;
            c->synce_threshold = 1000;
            c->synce_ap = 2;
            c->ho_filter = 60;
            c->stable_adj_threshold = 300;  /* = 30 ppb */
    }
}

mesa_rc vtss_appl_ptp_clock_servo_status_get(uint instance, vtss_ptp_servo_status_t *s)
{
    PTP_CORE_LOCK_SCOPE();

    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
#if defined(VTSS_SW_OPTION_ZLS30387)
            if (config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_DEFAULT ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_FREQ_XO ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_XO ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_FREQ_TCXO ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_TCXO ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_FREQ_OCXO_S3E ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_FREQ ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_PHASE ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_FREQ ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_FREQ_ACCURACY_FDD ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_FREQ_ACCURACY_XDSL ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_ELEC_FREQ ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_ELEC_PHASE ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_TCXO_Q ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_Q ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_RELAXED_C60W ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_RELAXED_C150 ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_RELAXED_C180 ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_RELAXED_C240 ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_BETA ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E_R4_6_1 ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_BASIC_PHASE ||
                config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_ACI_BASIC_PHASE_LOW
                )
            {
                advanced_servo[instance]->clock_servo_status(s);
            }
            else
#endif
#ifdef SW_OPTION_BASIC_PTP_SERVO
            if (config_data.conf[instance].clock_init.cfg.filter_type == PTP_FILTERTYPE_BASIC) {
                basic_servo[instance]->clock_servo_status(s);
            }
            else
#else
            {
                return VTSS_RC_ERROR;
            }
#endif // SW_OPTION_BASIC_PTP_SERVO
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

void vtss_ptp_clock_slave_config_set(ptp_servo *servo, const vtss_appl_ptp_clock_slave_config_t *cfg)
{
    servo->stable_offset_threshold = ((i64)cfg->stable_offset)<<16;
    servo->offset_ok_threshold = ((i64)cfg->offset_ok)<<16;
    servo->offset_fail_threshold = ((i64)cfg->offset_fail)<<16;
}

mesa_rc vtss_appl_ptp_clock_status_slave_ds_get(uint instance, vtss_appl_ptp_clock_slave_ds_t *const status)
{
    PTP_CORE_LOCK_SCOPE();
    
    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            vtss_ptp_get_clock_slave_ds(ptp_global.ptpi[instance], status);
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

/**
 * \brief Set unicast slave configuration parameters.
 *
 */
mesa_rc vtss_appl_ptp_clock_config_unicast_slave_config_set(uint instance, uint idx, const vtss_appl_ptp_unicast_slave_config_t *const c)
{
    PTP_CORE_LOCK_SCOPE();
    
    if (((c->duration >= 10) && (c->duration <= 1000)) &&
        (((c->ip_addr & 0xff000000U) != 0) || (c->ip_addr == 0)) &&
        ((c->ip_addr & 0xff000000U) != 0x7f000000U) &&
        ((c->ip_addr & 0xff000000U) <= 0xdf000000U))
    {
        if ((instance < PTP_CLOCK_INSTANCES) && (idx < MAX_UNICAST_MASTERS_PR_SLAVE)) {
            config_data.conf[instance].unicast_slave[idx] = *c;
            if (PTP_READY()) {
                if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
                    vtss_ptp_uni_slave_conf_set(ptp_global.ptpi[instance], idx, c);
                    return VTSS_RC_OK;
                }
            } else return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

/**
 * \brief Get unicast slave configuration parameters.
 *
 */
mesa_rc vtss_appl_ptp_clock_config_unicast_slave_config_get(uint instance, uint idx, vtss_appl_ptp_unicast_slave_config_t *const c)
{
    PTP_CORE_LOCK_SCOPE();

    if ((instance < PTP_CLOCK_INSTANCES) && idx < MAX_UNICAST_MASTERS_PR_SLAVE) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            vtss_ptp_uni_slave_conf_get(ptp_global.ptpi[instance], idx, c);
//            vtss_ptp_uni_slave_conf_state_get(ptp_global.ptpi[instance], idx, c);
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

/**
 * \brief Get unicast slave table data.
 *
 */
mesa_rc vtss_appl_ptp_clock_status_unicast_slave_table_get(uint instance, uint ix, vtss_appl_ptp_unicast_slave_table_t *const uni_slave_table)
{
    PTP_CORE_LOCK_SCOPE();
    
    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE) {
            return vtss_ptp_clock_unicast_table_get(ptp_global.ptpi[instance], uni_slave_table, ix);
        }
    }
    return VTSS_RC_ERROR;
}

/**
* \brief Get unicast master table data.
*
*/
mesa_rc vtss_appl_ptp_clock_slave_itr(const uint *const clock_prev, uint *const clock_next, const uint *const slave_prev, uint *const slave_next)
{
    PTP_CORE_LOCK_SCOPE();

    mesa_rc rc = VTSS_RC_ERROR;
    vtss_ptp_master_table_key_t prev_key, next_key;
    if (clock_prev == 0) {
        rc = vtss_appl_ptp_clock_slave_itr_get(0, &next_key);
    }
    else if (slave_prev == 0) {
        prev_key.ip = 0;
        prev_key.inst = *clock_prev;
        rc = vtss_appl_ptp_clock_slave_itr_get(&prev_key, &next_key);
    }
    else {
        prev_key.ip = *slave_prev;
        prev_key.inst = *clock_prev;
        rc = vtss_appl_ptp_clock_slave_itr_get(&prev_key, &next_key);
    }
    if (rc == VTSS_RC_OK) {
        *slave_next = next_key.ip;
        *clock_next = next_key.inst;
    }
    return rc;
}

mesa_rc vtss_appl_ptp_clock_status_unicast_master_table_get(uint instance, u32 ip, vtss_appl_ptp_unicast_master_table_t *uni_master_table)
{
    PTP_CORE_LOCK_SCOPE();

    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        vtss_ptp_master_table_key_t key;
        key.ip = ip;
        key.inst = instance;
        return vtss_ptp_clock_status_unicast_master_table_get(key, uni_master_table);
    }
    return VTSS_RC_ERROR;
}

void ptp_local_clock_time_set(mesa_timestamp_t *t, u32 domain)
{
    PTP_CORE_LOCK_SCOPE();
    
    vtss_local_clock_time_set(t, domain);
}

mesa_rc vtss_appl_ptp_clock_control_get(uint instance, vtss_appl_ptp_clock_control_t *const port_control)
{
    memset(port_control, 0, sizeof(vtss_appl_ptp_clock_control_t));
    return VTSS_RC_OK;
}

mesa_rc vtss_appl_ptp_clock_control_set(uint instance, const vtss_appl_ptp_clock_control_t *const port_control)
{
    mesa_timestamp_t t;

    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        if (port_control->syncToSystemClock == true) {
            t.sec_msb = 0;
            t.seconds = time(NULL);
            t.nanoseconds = 0;
            ptp_local_clock_time_set(&t, ptp_instance_2_timing_domain(instance));
        }
        return VTSS_RC_OK;
    }
    else {
        return VTSS_RC_ERROR;
    }
}

/**
 * \brief Get observed egress latency.
 *
 */
void ptp_clock_egress_latency_get(observed_egr_lat_t *lat)
{
    PTP_CORE_LOCK_SCOPE();

    *lat = observed_egr_lat;
}

/**
 * \brief Clear observer egress latency.
 *
 */
void ptp_clock_egress_latency_clear(void)
{
    PTP_CORE_LOCK_SCOPE();

    observed_egr_lat.cnt = 0;
    observed_egr_lat.min = 0;
    observed_egr_lat.mean = 0;
    observed_egr_lat.max = 0;
}

/* Get external clock output configuration. */
mesa_rc vtss_appl_ptp_ext_clock_out_get(vtss_appl_ptp_ext_clock_mode_t *mode)
{
    *mode = config_data.init_ext_clock_mode;
    return VTSS_RC_OK;
}

/* Get default external clock output configuration. */
void vtss_ext_clock_out_default_get(vtss_appl_ptp_ext_clock_mode_t *mode)
{
    mode->one_pps_mode = VTSS_APPL_PTP_ONE_PPS_DISABLE;
    mode->clock_out_enable = false;
    mode->adj_method = VTSS_APPL_PTP_PREFERRED_ADJ_AUTO;
    mode->freq = 1;
}

/* Set debug_mode. */
bool ptp_debug_mode_set(int debug_mode, uint instance, BOOL has_log_to_file, BOOL has_control, u32 log_time)
{
    PTP_CORE_LOCK_SCOPE();

    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        return vtss_ptp_debug_mode_set(ptp_global.ptpi[instance], debug_mode, has_log_to_file, has_control, log_time);
    }
    return false;
}

/* Get debug_mode. */
bool ptp_debug_mode_get(uint instance, vtss_ptp_logmode_t *log_mode)
{
    PTP_CORE_LOCK_SCOPE();

    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        return vtss_ptp_debug_mode_get(ptp_global.ptpi[instance], log_mode);
    }
    return false;
}

/* Delete PTP log file */
bool ptp_log_delete(uint instance)
{
    PTP_CORE_LOCK_SCOPE();

    return vtss_ptp_log_delete(ptp_global.ptpi[instance]);
}

/* Get afi mode. */
mesa_rc ptp_afi_mode_get(uint instance, bool ann, bool* enable)
{
    PTP_CORE_LOCK_SCOPE();
    if (instance < PTP_CLOCK_INSTANCES) {
        if (ann) {
            *enable = config_data.conf[instance].clock_init.afi_announce_enable;
        } else {
            *enable = config_data.conf[instance].clock_init.afi_sync_enable;
        }
        return VTSS_RC_OK;
    }
    return VTSS_RC_ERROR;
}

/* Set afi_mode. */
mesa_rc ptp_afi_mode_set(uint instance, bool ann, bool enable)
{
    PTP_CORE_LOCK_SCOPE();
    if (instance < PTP_CLOCK_INSTANCES) {
        if (ann) {
            config_data.conf[instance].clock_init.afi_announce_enable = enable;
        } else {
            config_data.conf[instance].clock_init.afi_sync_enable = enable;
        }
        return VTSS_RC_OK;
    }
    return VTSS_RC_ERROR;
}

/* Set external clock output configuration. */
static void ext_clock_out_set(const vtss_appl_ptp_ext_clock_mode_t *mode)
{
    if (mesa_cap_ts) {
        mesa_ts_ext_clock_one_pps_mode_t m;
        mesa_ts_ext_clock_mode_t clock_mode;
        
        m = (mode->one_pps_mode == VTSS_APPL_PTP_ONE_PPS_DISABLE) ? MESA_TS_EXT_CLOCK_MODE_ONE_PPS_DISABLE :
            (mode->one_pps_mode == VTSS_APPL_PTP_ONE_PPS_OUTPUT ? MESA_TS_EXT_CLOCK_MODE_ONE_PPS_OUTPUT :
            (mode->one_pps_mode == VTSS_APPL_PTP_ONE_PPS_INPUT ? MESA_TS_EXT_CLOCK_MODE_ONE_PPS_INPUT : MESA_TS_EXT_CLOCK_MODE_ONE_PPS_OUTPUT_INPUT));
        clock_mode.one_pps_mode = m;
        clock_mode.enable = mode->clock_out_enable;
        clock_mode.freq = mode->freq;

        vtss_appl_ptp_clock_status_default_ds_t default_get_status;
        if (mode->one_pps_mode == VTSS_APPL_PTP_ONE_PPS_INPUT) {
            vtss_appl_ptp_clock_config_default_ds_t default_set_cfg;            
            one_pps_external_input_servo_init(true);
            /* set vtss_appl_clock_quality.ClockClass to a low value (this may be made configurable in a later release */
            PTP_RC(vtss_ptp_get_clock_default_ds_cfg(ptp_global.ptpi[0], &default_set_cfg));
            PTP_RC(vtss_ptp_get_clock_default_ds_status(ptp_global.ptpi[0], &default_get_status));
            default_set_cfg.priority1 = 0;
            PTP_RC(vtss_ptp_set_clock_default_ds_cfg(ptp_global.ptpi[0], &default_set_cfg));
            default_get_status.clockQuality.clockClass = 6;
            PTP_RC(vtss_ptp_set_clock_quality(ptp_global.ptpi[0], &default_get_status.clockQuality));
        } else {
            one_pps_external_input_servo_init(false);
            PTP_RC(vtss_ptp_set_clock_default_ds_cfg(ptp_global.ptpi[0], &config_data.conf[0].clock_init.cfg));
            PTP_RC(vtss_ptp_get_clock_default_ds_status(ptp_global.ptpi[0], &default_get_status));
            default_get_status.clockQuality.clockClass = DEFAULT_CLOCK_CLASS;
            PTP_RC(vtss_ptp_set_clock_quality(ptp_global.ptpi[0], &default_get_status.clockQuality));
        }

        PTP_RC(mesa_ts_external_clock_mode_set( NULL, &clock_mode));
    } else {
        T_D("Clock mode set not supported, mode = %d, freq = %u",mode->clock_out_enable, mode->freq);
    }
}
/* Set external clock output configuration. */
mesa_rc vtss_appl_ptp_ext_clock_out_set(const vtss_appl_ptp_ext_clock_mode_t *mode)
{
    int instance;
    /*lint -save -e568 */
    if (((mode->one_pps_mode >= VTSS_APPL_PTP_ONE_PPS_DISABLE) && (mode->one_pps_mode <= VTSS_APPL_PTP_ONE_PPS_OUTPUT_INPUT)) &&
        ((mode->clock_out_enable == 0) || (mode->clock_out_enable == 1)) &&
        (
         (mode->adj_method == VTSS_APPL_PTP_PREFERRED_ADJ_LTC)                                            ||
         (mode->adj_method == VTSS_APPL_PTP_PREFERRED_ADJ_SINGLE      && meba_cap_synce_dpll_mode_single) ||
         (mode->adj_method == VTSS_APPL_PTP_PREFERRED_ADJ_INDEPENDENT && meba_cap_synce_dpll_mode_dual)   ||
         (mode->adj_method == VTSS_APPL_PTP_PREFERRED_ADJ_COMMON      && meba_cap_synce_dpll_mode_single) ||
         (mode->adj_method == VTSS_APPL_PTP_PREFERRED_ADJ_AUTO)
        ) &&
        ((mode->freq >= 1) && (mode->freq <= 25000000)))
    {
        T_IG(VTSS_TRACE_GRP_MS_SERVO,"mode: one_pps_mode %d, clock_out_enable %d, adj_method %d, freq %d", mode->one_pps_mode, mode->clock_out_enable, mode->adj_method, mode->freq);
        /*lint -restore */
        mesa_rc rc = VTSS_RC_OK;
        /* update clk options is only allowed if no active PTP instances are using clock_domain 0 */
        if (mode->adj_method != config_data.init_ext_clock_mode.adj_method) {
            for (instance = 0; instance < PTP_CLOCK_INSTANCES; instance++) {
                if (config_data.conf[instance].clock_init.cfg.deviceType != VTSS_APPL_PTP_DEVICE_NONE && config_data.conf[instance].clock_init.cfg.clock_domain == 0) {
                    rc = PTP_RC_ADJ_METHOD_CHANGE_NOT_ALLOWED;
                }
            }
        }
        if (rc != VTSS_RC_OK) {
            T_WG(VTSS_TRACE_GRP_MS_SERVO,"Error code: %s", error_txt(rc));
            return rc;
        }
        {
            PTP_CORE_LOCK_SCOPE();
            
            if (rc == VTSS_RC_OK) {
                config_data.init_ext_clock_mode = *mode;
                if (PTP_READY()) {
                    ext_clock_out_set(mode);
    
                }
            }
        }
        return rc;
    }
    else return PTP_ERROR_INV_PARAM;  // One or more paramters were invalid.
}

static void my_co_1pps(mesa_port_no_t port_no, const mesa_timestamp_t *ts)
{
    char str [40];
    T_DG(VTSS_TRACE_GRP_PTP_PIM,"got 1pps: %s",TimeStampToString (ts, str));
    if (config_data.init_ext_clock_mode_rs422.proto == VTSS_PTP_RS422_PROTOCOL_PIM) {
        vtss_ext_clock_rs422_time_set(ts);
    }
}

static void my_co_delay(mesa_port_no_t port_no, const mesa_timeinterval_t *ts)
{
    PTP_CORE_LOCK_SCOPE();
    
    char str [40];
    T_DG(VTSS_TRACE_GRP_PTP_PIM,"got delay: %s",vtss_tod_TimeInterval_To_String (ts, str, '.'));
    wireless_status.remote_pre = false;
    wireless_status.remote_delay = *ts;
}

static void my_co_pre_delay(mesa_port_no_t port_no)
{
    PTP_CORE_LOCK_SCOPE();

    T_DG(VTSS_TRACE_GRP_PTP_PIM,"got pre delay");
    wireless_status.remote_pre = true;
}

static void pim_proto_init(bool enable)
{
    ptp_pim_init_t pim_ini;
    pim_ini.co_1pps = my_co_1pps;
    pim_ini.co_delay = my_co_delay;
    pim_ini.co_pre_delay = my_co_pre_delay;
    pim_ini.tg = VTSS_TRACE_GRP_PTP_PIM;
    T_IG(VTSS_TRACE_GRP_PTP_PIM,"calling pim_init active: %d", enable);
    PTP_CORE_UNLOCK();      // to avoid deadlock because pim may call back to PTP
    ptp_pim_init(&pim_ini, enable);
    PTP_CORE_LOCK();        // to avoid deadlock because pim may call back to PTP
}

/* Get serval rs422 external clock configuration. */
void vtss_ext_clock_rs422_conf_get(vtss_ptp_rs422_conf_t *mode)
{
    PTP_CORE_LOCK_SCOPE();
    
    *mode = config_data.init_ext_clock_mode_rs422;
}

/* Get serval rs422 external clock protocol configuration. */
void vtss_ext_clock_rs422_protocol_get(ptp_rs422_protocol_t *proto)
{
    *proto = config_data.init_ext_clock_mode_rs422.proto;
}

/* Get serval default rs422 external clock configuration. */
void vtss_ext_clock_rs422_default_conf_get(vtss_ptp_rs422_conf_t *mode)
{
    mode->mode = VTSS_PTP_RS422_DISABLE;
    mode->delay = 0;
    mode->proto = VTSS_PTP_RS422_PROTOCOL_SER_POLYT;
    mode->port = 0;
}

static u32 ptp_1pps_port_2_io_pin(mesa_port_no_t port_no)
{
    u32 i;
    for (i = 0; i < mesa_cap_ts_io_cnt; i++) {
        if (ptp_io_pin[i].slave_data.one_pps_slave_port_no == port_no) {
            return i;
        }
    }
    T_IG(VTSS_TRACE_GRP_1_PPS, "unknown port_no %d", port_no);
    return i;
}


static void _my_slave_1pps(u32 io_pin, const mesa_timestamp_t *ts)
{
    char str [40];

    const mesa_timeinterval_t io_pin_input_latency = (8LL<<16);
    mesa_timestamp_t my_ts;
    vtss_tod_sub_TimeStamp(&my_ts, ts, &io_pin_input_latency);
    T_DG(VTSS_TRACE_GRP_1_PPS, "got 1pps: %s from io_pin %u", TimeStampToString (&my_ts, str), io_pin);
    if (io_pin < mesa_cap_ts_io_cnt) {
        ptp_external_input_slave_function(&ptp_io_pin[io_pin].slave_data, ptp_io_pin[io_pin].ptp_inst, &my_ts, true);

    } else {
        T_WG(VTSS_TRACE_GRP_1_PPS, "1pps from invalid io_pin %u", io_pin);
    }
}

static void my_slave_1pps(mesa_port_no_t port_no, const mesa_timestamp_t *ts)
{
    PTP_CORE_LOCK_SCOPE();
    
    _my_slave_1pps(ptp_1pps_port_2_io_pin(port_no), ts);
}

static mesa_ts_ext_io_pin_cfg_t vtss_appl_io_pin_mode_to_ts_io_pin_mode(vtss_appl_ptp_ext_io_pin_cfg_t pin_mode)
{
    switch (pin_mode) {
        case     VTSS_APPL_PTP_IO_MODE_ONE_PPS_DISABLE: return MESA_TS_EXT_IO_MODE_ONE_PPS_DISABLE;
        case     VTSS_APPL_PTP_IO_MODE_ONE_PPS_OUTPUT:  return MESA_TS_EXT_IO_MODE_ONE_PPS_OUTPUT;
        case     VTSS_APPL_PTP_IO_MODE_WAVEFORM_OUTPUT: return MESA_TS_EXT_IO_MODE_WAVEFORM_OUTPUT;
        case     VTSS_APPL_PTP_IO_MODE_ONE_PPS_LOAD:    return MESA_TS_EXT_IO_MODE_ONE_PPS_LOAD;
        case     VTSS_APPL_PTP_IO_MODE_ONE_PPS_SAVE:    return MESA_TS_EXT_IO_MODE_ONE_PPS_SAVE;
        default:                                        return MESA_TS_EXT_IO_MODE_MAX;
    }
}

static mesa_rc sgpio_bit_set(int port, int bit, bool set)
{
    vtss_appl_api_lock_unlock api_lock;       /* Protect SGIO conf */
    mesa_sgpio_conf_t conf;
    T_I("set SDPIO pin port %d, bit %d to %d", port, bit, set);
    VTSS_RC(mesa_sgpio_conf_get(NULL, 0, 0, &conf));
    conf.port_conf[port].mode[bit] = set ? MESA_SGPIO_MODE_OFF : MESA_SGPIO_MODE_ON;
    return mesa_sgpio_conf_set(NULL, 0, 0, &conf);
}

/* Set serval rs422 external clock configuration. */
static mesa_rc ext_clock_rs422_conf_set(const vtss_ptp_rs422_conf_t *mode)
{
    mesa_rc rc = VTSS_RC_OK;
    if (rs422_conf.gpio_rs422_1588_mstoen == -1 && rs422_conf.gpio_rs422_1588_slvoen == -1) {
        if (mode->mode != VTSS_PTP_RS422_DISABLE) {
            T_W("RS422 not supported on board type: %d", vtss_board_type());
        }
        return VTSS_RC_ERROR;
    }

    if (mesa_cap_ts_has_ptp_io_pin) {
        ptp_pim_init_t pim_ini;
        static bool pim_active = false;
        mesa_ts_ext_io_mode_t io_mode;
    
        /* if mode is VTSS_PTP_RS422_SUB, then GPIO mstoen  must be set active high, and slvoen must be set low.
         * this is done to tristate the driver connected to the TX connector feedback signal.
         * in VTSS_PTP_RS422_DISABLE mode the same setting is done  as in VTSS_PTP_RS422_SUB mode, i.e. the PPS input can also be used in VTSS_PTP_RS422_DISABLE mode */
        if (rs422_conf.gpio_rs422_1588_mstoen != -1 && rs422_conf.gpio_rs422_1588_mstoen < 64) {
            PTP_RC(mesa_gpio_mode_set(NULL, 0,   rs422_conf.gpio_rs422_1588_mstoen, MESA_GPIO_OUT));
            PTP_RC(mesa_gpio_write(NULL, 0, rs422_conf.gpio_rs422_1588_mstoen, mode->mode == VTSS_PTP_RS422_SUB || mode->mode == VTSS_PTP_RS422_CALIB || mode->mode == VTSS_PTP_RS422_DISABLE));
        } else if (rs422_conf.gpio_rs422_1588_mstoen != -1) {
            // use SGPIO pin
            PTP_RC(sgpio_bit_set(rs422_conf.gpio_rs422_1588_mstoen>>8,  rs422_conf.gpio_rs422_1588_mstoen &0x3, mode->mode == VTSS_PTP_RS422_SUB || mode->mode == VTSS_PTP_RS422_CALIB || mode->mode == VTSS_PTP_RS422_DISABLE));
        }
        if (rs422_conf.gpio_rs422_1588_slvoen != -1 && rs422_conf.gpio_rs422_1588_slvoen < 64) {
            PTP_RC(mesa_gpio_mode_set(NULL, 0,   rs422_conf.gpio_rs422_1588_slvoen, MESA_GPIO_OUT));
            PTP_RC(mesa_gpio_write(NULL, 0, rs422_conf.gpio_rs422_1588_slvoen, mode->mode != VTSS_PTP_RS422_SUB && mode->mode != VTSS_PTP_RS422_CALIB && mode->mode != VTSS_PTP_RS422_DISABLE));
        } else if (rs422_conf.gpio_rs422_1588_slvoen != -1) {
            // use SGPIO pin
            PTP_RC(sgpio_bit_set(rs422_conf.gpio_rs422_1588_slvoen>>8,  rs422_conf.gpio_rs422_1588_slvoen &0x3, mode->mode != VTSS_PTP_RS422_SUB && mode->mode != VTSS_PTP_RS422_CALIB && mode->mode != VTSS_PTP_RS422_DISABLE));
        }
        if ((ptp_io_pin[rs422_conf.ptp_pin_ldst].usage == PTP_IO_PIN_USAGE_NONE || ptp_io_pin[rs422_conf.ptp_pin_ldst].usage == PTP_IO_PIN_USAGE_RS422) &&
            (ptp_io_pin[rs422_conf.ptp_pin_ppso].usage == PTP_IO_PIN_USAGE_NONE || ptp_io_pin[rs422_conf.ptp_pin_ppso].usage == PTP_IO_PIN_USAGE_RS422)) {
            if (mesa_cap_phy_ts) {
                mesa_timeinterval_t one_pps_latency = (mesa_timeinterval_t)8LL<<16; /* default 8 ns including GPIO delay */
                if (mode->mode == VTSS_PTP_RS422_SUB) {
                    one_pps_latency += (((mesa_timeinterval_t)mode->delay)<<16); /* default one clock cycle */
                }
                PTP_RC(vtss_module_man_master_1pps_latency(one_pps_latency));
            }
            pim_ini.co_1pps = my_slave_1pps;
            pim_ini.co_delay = 0;
            pim_ini.co_pre_delay = 0;
            pim_ini.tg = VTSS_TRACE_GRP_PTP_PIM;
            if (mode->mode != VTSS_PTP_RS422_DISABLE && !pim_active) {
                PTP_CORE_UNLOCK();      // to avoid deadlock because pim may call back to PTP
                ptp_pim_init(&pim_ini, true);
                PTP_CORE_LOCK();        // to avoid deadlock because pim may call back to PTP
                one_pps_tod_statistics_clear();
            }
            if (mode->mode == VTSS_PTP_RS422_DISABLE && pim_active) {
                PTP_CORE_UNLOCK();      // to avoid deadlock because pim may call back to PTP
                ptp_pim_init(&pim_ini, false);
                PTP_CORE_LOCK();        // to avoid deadlock because pim may call back to PTP
            }
    
            ptp_io_pin[rs422_conf.ptp_pin_ldst].io_pin.pin = VTSS_APPL_PTP_IO_MODE_ONE_PPS_DISABLE;
            ptp_io_pin[rs422_conf.ptp_pin_ppso].io_pin.pin = VTSS_APPL_PTP_IO_MODE_ONE_PPS_DISABLE;
            ptp_io_pin[rs422_conf.ptp_pin_ldst].slave_data.enable_t1 = (mode->mode == VTSS_PTP_RS422_SUB) ? true : false;
            ptp_io_pin[rs422_conf.ptp_pin_ldst].slave_data.one_pps_slave_port_no =  VTSS_PORT_NO_NONE;
    
            if (mode->mode == VTSS_PTP_RS422_SUB || mode->mode == VTSS_PTP_RS422_MAIN_AUTO || mode->mode == VTSS_PTP_RS422_CALIB) {
                T_I("initialize IO PIN mode for pin 2");
                ptp_io_pin[rs422_conf.ptp_pin_ldst].io_pin.pin = VTSS_APPL_PTP_IO_MODE_ONE_PPS_SAVE;
                ptp_io_pin[rs422_conf.ptp_pin_ldst].io_pin.domain = 0;
                ptp_io_pin[rs422_conf.ptp_pin_ldst].io_pin.freq = 1;
                ptp_io_pin[rs422_conf.ptp_pin_ldst].ptp_inst = 0;
                ptp_io_pin[rs422_conf.ptp_pin_ldst].slave_data.one_pps_slave_port_no = mode->proto == VTSS_PTP_RS422_PROTOCOL_PIM ? mode->port : mesa_cap_port_cnt;
                ptp_io_pin[rs422_conf.ptp_pin_ldst].source_id = init_int_source_id[rs422_conf.ptp_pin_ldst];
                if (VTSS_RC_OK != vtss_interrupt_source_hook_set(VTSS_MODULE_ID_PTP, io_pin_interrupt_handler,
                               init_int_source_id[rs422_conf.ptp_pin_ldst],
                               INTERRUPT_PRIORITY_NORMAL)) {
                   T_I("source already hooked");
               }
                T_I("source %d", init_int_source_id[rs422_conf.ptp_pin_ldst]);
            }
    
            if (mode->mode == VTSS_PTP_RS422_MAIN_MAN || mode->mode == VTSS_PTP_RS422_MAIN_AUTO || mode->mode == VTSS_PTP_RS422_CALIB) {
                T_I("initialize IO PIN mode for pin 3");
                ptp_io_pin[rs422_conf.ptp_pin_ppso].io_pin.pin = VTSS_APPL_PTP_IO_MODE_ONE_PPS_OUTPUT;
                ptp_io_pin[rs422_conf.ptp_pin_ppso].io_pin.domain = 0;
                ptp_io_pin[rs422_conf.ptp_pin_ppso].io_pin.freq = 1;
                ptp_io_pin[rs422_conf.ptp_pin_ppso].ptp_inst = 0;
                if (mode->mode == VTSS_PTP_RS422_MAIN_MAN) {
                    ptp_io_pin[rs422_conf.ptp_pin_ppso].source_id = init_int_source_id[rs422_conf.ptp_pin_ppso];
                    if (VTSS_RC_OK != vtss_interrupt_source_hook_set(VTSS_MODULE_ID_PTP, one_pps_pulse_interrupt_handler,
                               init_int_source_id[rs422_conf.ptp_pin_ppso],
                               INTERRUPT_PRIORITY_NORMAL)) {
                       T_I("source already hooked");
                   }
                   T_I("source %d", init_int_source_id[rs422_conf.ptp_pin_ppso]);
                }
            }
            io_mode.freq = ptp_io_pin[rs422_conf.ptp_pin_ldst].io_pin.freq;
            io_mode.domain = ptp_io_pin[rs422_conf.ptp_pin_ldst].io_pin.domain;
            io_mode.pin = vtss_appl_io_pin_mode_to_ts_io_pin_mode(ptp_io_pin[rs422_conf.ptp_pin_ldst].io_pin.pin);
            PTP_RC(mesa_ts_external_io_mode_set(NULL, 2, &io_mode));
            io_mode.freq = ptp_io_pin[rs422_conf.ptp_pin_ppso].io_pin.freq;
            io_mode.domain = ptp_io_pin[rs422_conf.ptp_pin_ppso].io_pin.domain;
            io_mode.pin = vtss_appl_io_pin_mode_to_ts_io_pin_mode(ptp_io_pin[rs422_conf.ptp_pin_ppso].io_pin.pin);
            PTP_RC(mesa_ts_external_io_mode_set(NULL, 3, &io_mode));
            if (mode->mode == VTSS_PTP_RS422_DISABLE) {
                ptp_io_pin[rs422_conf.ptp_pin_ldst].usage = PTP_IO_PIN_USAGE_NONE;
                ptp_io_pin[rs422_conf.ptp_pin_ppso].usage = PTP_IO_PIN_USAGE_NONE;
                T_I("IO_PIN usage = NONE");
            } else {
                ptp_io_pin[rs422_conf.ptp_pin_ldst].usage = PTP_IO_PIN_USAGE_RS422;
                ptp_io_pin[rs422_conf.ptp_pin_ppso].usage = PTP_IO_PIN_USAGE_RS422;
                T_I("IO_PIN usage = RS422");
            }
        } else {
            if (mode->mode != VTSS_PTP_RS422_DISABLE) {
                T_W("IO pins are not available for the RS422 feature");
            }
            rc = VTSS_RC_ERROR;
        }
    } else if (mesa_cap_ts_has_alt_pin) {

        mesa_ts_alt_clock_mode_t clock_mode;
        mesa_timeinterval_t one_pps_latency = (mesa_timeinterval_t)8LL<<16; /* default 8 ns including GPIO delay */
        /* if mode is VTSS_PTP_RS422_SUB, then GPIO23 must be set active high.
         * this is done to tristate the driver connected to the TX connector feedback signal */
        if (rs422_conf.gpio_rs422_1588_mstoen != -1 && rs422_conf.gpio_rs422_1588_mstoen < 64) {
            PTP_RC(mesa_gpio_mode_set(NULL, 0,   rs422_conf.gpio_rs422_1588_mstoen, MESA_GPIO_OUT));
            PTP_RC(mesa_gpio_write(NULL, 0, rs422_conf.gpio_rs422_1588_mstoen, mode->mode == VTSS_PTP_RS422_SUB || mode->mode == VTSS_PTP_RS422_CALIB || mode->mode == VTSS_PTP_RS422_DISABLE));
        } else if (rs422_conf.gpio_rs422_1588_mstoen != -1) {
            // use SGPIO pin
            PTP_RC(sgpio_bit_set(rs422_conf.gpio_rs422_1588_mstoen>>8,  rs422_conf.gpio_rs422_1588_mstoen &0x3, mode->mode == VTSS_PTP_RS422_SUB || mode->mode == VTSS_PTP_RS422_CALIB || mode->mode == VTSS_PTP_RS422_DISABLE));
        }
        if (rs422_conf.gpio_rs422_1588_slvoen != -1 && rs422_conf.gpio_rs422_1588_slvoen < 64) {
            PTP_RC(mesa_gpio_mode_set(NULL, 0,   rs422_conf.gpio_rs422_1588_slvoen, MESA_GPIO_OUT));
            PTP_RC(mesa_gpio_write(NULL, 0, rs422_conf.gpio_rs422_1588_slvoen, mode->mode != VTSS_PTP_RS422_SUB && mode->mode != VTSS_PTP_RS422_CALIB && mode->mode != VTSS_PTP_RS422_DISABLE));
        } else if (rs422_conf.gpio_rs422_1588_slvoen != -1) {
            // use SGPIO pin
            PTP_RC(sgpio_bit_set(rs422_conf.gpio_rs422_1588_slvoen>>8,  rs422_conf.gpio_rs422_1588_slvoen &0x3, mode->mode != VTSS_PTP_RS422_SUB && mode->mode != VTSS_PTP_RS422_CALIB && mode->mode != VTSS_PTP_RS422_DISABLE));
        }
        one_pps_latency += (((mesa_timeinterval_t)mode->delay)<<16); /* default one clock cycle */

        if (mesa_cap_phy_ts) {
            PTP_RC(vtss_module_man_master_1pps_latency(one_pps_latency));
        }
    
        clock_mode.one_pps_out = (mode->mode == VTSS_PTP_RS422_DISABLE) ? false :
                                 ((mode->mode == VTSS_PTP_RS422_MAIN_AUTO || mode->mode == VTSS_PTP_RS422_MAIN_MAN || mode->mode == VTSS_PTP_RS422_CALIB) ? true : false);
        clock_mode.one_pps_in  = (mode->mode == VTSS_PTP_RS422_DISABLE) ? false : true;
        clock_mode.save        = (mode->mode == VTSS_PTP_RS422_DISABLE) ? false : true;
        clock_mode.load        = false;
        PTP_RC(mesa_ts_alt_clock_mode_set( NULL, &clock_mode));
        if (mode->mode == VTSS_PTP_RS422_SUB) {
            alt_pin.one_pps_slave_port_no = mode->proto == VTSS_PTP_RS422_PROTOCOL_PIM ? mode->port : mesa_cap_port_cnt;
            alt_pin.enable_t1 = true;
        }
        pim_proto_init((mode->proto == VTSS_PTP_RS422_PROTOCOL_PIM) || wireless_status.mode_enabled);
        if (mode->mode == VTSS_PTP_RS422_MAIN_MAN) {
            /* in manual mode there is no External input, therefore the transmission is triggered by the internal 1PPS */
            T_I("pps output int id %d", rs422_conf.ptp_rs422_pps_int_id);
            rc = vtss_interrupt_source_hook_set(VTSS_MODULE_ID_PTP, one_pps_pulse_interrupt_handler,
                                                  (meba_event_t)rs422_conf.ptp_rs422_pps_int_id,
                                                  INTERRUPT_PRIORITY_NORMAL);
            if (rc != VTSS_RC_OK && rc != VTSS_UNSPECIFIED_ERROR) {
                T_W("Error code: %x", rc);
            }
        }
    }

    return rc;
}

void vtss_ext_clock_rs422_conf_set(const vtss_ptp_rs422_conf_t *mode)
{
    PTP_CORE_LOCK_SCOPE();
    mesa_rc rc = VTSS_RC_OK;
    
    if (vtss_board_type() != VTSS_BOARD_JAGUAR2_CU48_REF && vtss_board_type() != VTSS_BOARD_JAGUAR2_AQR_REF) {
        if (PTP_READY()) {
            rc = ext_clock_rs422_conf_set(mode);
        }
        if (rc == VTSS_RC_OK) {
            config_data.init_ext_clock_mode_rs422 = *mode;
        }
    } else {
        if (mode->mode != VTSS_PTP_RS422_DISABLE)
            T_W("RS422 not supported on board type: %d", vtss_board_type());
    }
}

void vtss_ext_clock_rs422_time_set(const mesa_timestamp_t *t)
{
    PTP_CORE_LOCK_SCOPE();

    if (mesa_cap_ts_has_ptp_io_pin) {
        T_DG(VTSS_TRACE_GRP_PTP_SER,"got timestamp from io_pin %d, t->s = %u, t.ns = %u", rs422_conf.ptp_pin_ldst, t->seconds, t->nanoseconds);
        _my_slave_1pps(rs422_conf.ptp_pin_ldst, t);
    }
    if (mesa_cap_ts_has_alt_pin) {
        T_DG(VTSS_TRACE_GRP_PTP_SER,"got timestamp, t->s = %u, t.ns = %u", t->seconds, t->nanoseconds);
        ptp_external_input_slave_function(&alt_pin, 0, t, true);
    }
}

/* Get holdover spec configuration. */
void vtss_ho_spec_conf_get(vtss_ho_spec_conf_t *spec)
{

    PTP_CORE_LOCK();
    *spec = config_data.init_ho_spec;
    PTP_CORE_UNLOCK();
}

/* Set holdover spec configuration. */
void vtss_ho_spec_conf_set(const vtss_ho_spec_conf_t *spec)
{

    PTP_CORE_LOCK();
    config_data.init_ho_spec = *spec;
    PTP_CORE_UNLOCK();
}


mesa_rc ptp_get_port_link_state(uint instance, int portnum, vtss_ptp_port_link_state_t *ds)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_rc rc = VTSS_RC_ERROR;
    if (PTP_READY()) {
        ds->link_state = port_data[portnum - 1].link_state;
        ds->in_sync_state = port_data[portnum - 1].topo.port_ts_in_sync;
        ds->forw_state = port_data[portnum - 1].vlan_forw[instance];
        if (mesa_cap_phy_ts) {
            if (_phy_ts_rules_index[instance][portnum - 1] < PTP_CLOCK_INSTANCES) {
                ds->phy_timestamper = _phy_ts_rules[_phy_ts_rules_index[instance][portnum - 1]][portnum - 1].port.phy_ts_port;
            } else {
                ds->phy_timestamper = FALSE;
            }
        } else {
            ds->phy_timestamper = false;
        }
        rc = VTSS_RC_OK;
    }
    return rc;
}

/*
 * Enable/disable the wireless variable tx delay feature for a port.
 */
bool ptp_port_wireless_delay_mode_set(bool enable, int portnum, uint instance)
{
    PTP_CORE_LOCK_SCOPE();
    
    bool ok = false;
    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        if (((config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND) &&
              config_data.conf[instance].clock_init.cfg.twoStepFlag) ||
            (config_data.conf[instance].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM)) {
            ok = vtss_ptp_port_wireless_delay_mode_set(ptp_global.ptpi[instance], enable, portnum);
            if (mesa_cap_misc_chip_family == MESA_CHIP_FAMILY_SERVAL && mesa_cap_ts_ptp_rs422) {
                if (config_data.conf[instance].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM) {
                    wireless_status.mode_enabled = true;
                    pim_proto_init((config_data.init_ext_clock_mode_rs422.proto == VTSS_PTP_RS422_PROTOCOL_PIM) || wireless_status.mode_enabled);
                }
            }
        }
    }
    return ok;
}

/*
 * Get the Enable/disable mode for the wireless variable tx delay feature for a port.
 */
bool ptp_port_wireless_delay_mode_get(bool *enable, int portnum, uint instance)
{
    PTP_CORE_LOCK_SCOPE();
    
    bool ok = false;
    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        if (((config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND) &&
                config_data.conf[instance].clock_init.cfg.twoStepFlag) ||
                (config_data.conf[instance].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM)) {
            ok = vtss_ptp_port_wireless_delay_mode_get(ptp_global.ptpi[instance], enable, portnum);
        }
    }
    return ok;
}

/*
 * Pre notification sent from the wireless modem transmitter before the delay is changed.
 */
bool ptp_port_wireless_delay_pre_notif(int portnum, uint instance)
{
    PTP_CORE_LOCK_SCOPE();
    
    bool ok = false;
    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        if ((config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND) &&
                config_data.conf[instance].clock_init.cfg.twoStepFlag) {
            ok = vtss_ptp_port_wireless_delay_pre_notif(ptp_global.ptpi[instance], portnum);
        } else if ((config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY) &&
                 config_data.conf[instance].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM) {
            if (mesa_cap_misc_chip_family == MESA_CHIP_FAMILY_SERVAL && mesa_cap_ts_ptp_rs422) {
                ptp_pim_modem_pre_delay_msg_send(uport2iport(portnum));
            }
            ok = true;
        } else if ((config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY) &&
                    config_data.conf[instance].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM) {
             if (mesa_cap_misc_chip_family == MESA_CHIP_FAMILY_SERVAL && mesa_cap_ts_ptp_rs422) {
                wireless_status.local_pre = true;
            }
            ok = true;
        }
    }
    return ok;
}

/*
 * Set the delay configuration, sent from the wireless modem transmitter whenever the delay is changed.
 */
bool ptp_port_wireless_delay_set(const vtss_ptp_delay_cfg_t *delay_cfg, int portnum, uint instance)
{
    PTP_CORE_LOCK_SCOPE();
    
    bool ok = false;
    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        if (((config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND) &&
                config_data.conf[instance].clock_init.cfg.twoStepFlag) ||
        (config_data.conf[instance].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM)) {
            ok = vtss_ptp_port_wireless_delay_set(ptp_global.ptpi[instance], delay_cfg, portnum);
        }
        if (mesa_cap_misc_chip_family == MESA_CHIP_FAMILY_SERVAL && mesa_cap_ts_ptp_rs422) {
            if ((config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY) &&
                        config_data.conf[instance].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM) {
                ptp_pim_modem_delay_msg_send(uport2iport(portnum), &delay_cfg->base_delay);
                ok = true;
            } else if ((config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY) &&
                        config_data.conf[instance].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM) {
                wireless_status.local_pre = false;
                wireless_status.local_delay = delay_cfg->base_delay;
                ok = true;
            }
        }
    }
    return ok;
}

/*
 * Get the delay configuration.
 */
bool ptp_port_wireless_delay_get(vtss_ptp_delay_cfg_t *delay_cfg, int portnum, uint instance)
{
    PTP_CORE_LOCK_SCOPE();
    
    bool ok = false;
    if (PTP_READY() && (instance < PTP_CLOCK_INSTANCES)) {
        if (((config_data.conf[instance].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND) &&
                config_data.conf[instance].clock_init.cfg.twoStepFlag) ||
                (config_data.conf[instance].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_OAM)) {
            ok = vtss_ptp_port_wireless_delay_get(ptp_global.ptpi[instance], delay_cfg, portnum);
        }
    }
    return ok;
}

mesa_rc vtss_appl_ptp_system_time_sync_mode_set(const vtss_appl_ptp_system_time_sync_conf_t *const conf)
{
    PTP_CORE_LOCK_SCOPE();
    
    /*lint -save -e568 */
    if ((conf->mode >= VTSS_APPL_PTP_SYSTEM_TIME_NO_SYNC) && (conf->mode <= VTSS_APPL_PTP_SYSTEM_TIME_SYNC_SET)) {
        /*lint -restore */
        mesa_rc rc = VTSS_RC_OK;
        u32 sys_time_default_period = 10;   // default filter period in system_time_sync mode
        u32 sys_time_default_dist = 1;      // default filter dist in system_time_sync mode
        static const vtss_appl_ptp_clock_servo_config_t sys_time_servo = {false, true, true, true, 25, 1000, 500, 1, VTSS_APPL_PTP_CLOCK_FREE, 0,0,60,300};
        static const vtss_appl_ptp_clock_slave_config_t slave_cfg = {20000, 2000, 20000};
        if (PTP_READY()) {
            switch (conf->mode) {
                case VTSS_APPL_PTP_SYSTEM_TIME_SYNC_GET:
                    if (config_data.conf[0].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_NONE) {
                        /* create master only clock */
                        config_data.conf[0].clock_init.cfg.deviceType = VTSS_APPL_PTP_DEVICE_MASTER_ONLY;
                        if (PTP_READY()) {
                            vtss_ptp_clock_create(ptp_global.ptpi[0]);
                        }
                    }
                    if (config_data.conf[0].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY) {
                        config_data.conf[0].filter_params.period = sys_time_default_period;
                        config_data.conf[0].filter_params.dist = sys_time_default_dist;
                        config_data.conf[0].servo_params = sys_time_servo;
                        config_data.conf[0].slave_cfg = slave_cfg;
                        vtss_ptp_clock_slave_config_set(ptp_global.ptpi[0]->ssm.servo, &slave_cfg);
                        vtss_ptp_timer_start(&sys_time_sync_timer, SYS_TIME_SYNC_INIT_VALUE , true);
                    } else {
                        T_IG(VTSS_TRACE_GRP_SYS_TIME, "%s", ptp_error_txt(PTP_RC_CONFLICT_PTP_ENABLED));
                        rc = PTP_RC_CONFLICT_PTP_ENABLED;
                    }
                    break;
                case VTSS_APPL_PTP_SYSTEM_TIME_SYNC_SET:
#if defined(VTSS_SW_OPTION_NTP)
                    ntp_conf_t ntp_conf;
                    if ((VTSS_RC_OK == ntp_mgmt_conf_get(&ntp_conf)) && ntp_conf.mode_enabled) {
                        T_IG(VTSS_TRACE_GRP_SYS_TIME, "%s", ptp_error_txt(PTP_RC_CONFLICT_NTP_ENABLED));
                        rc = PTP_RC_CONFLICT_NTP_ENABLED;
                    }
#endif
                    if (rc == VTSS_RC_OK) {
                        vtss_ptp_timer_start(&sys_time_sync_timer, SYS_TIME_SYNC_INIT_VALUE , true);
                    }
                    break;
                default:
                    vtss_ptp_timer_stop(&sys_time_sync_timer);
                    break;
            }
        }
        if (rc == VTSS_RC_OK) {
            system_time_sync_conf = *conf;
        }
        return rc;
    }
    else return VTSS_RC_ERROR;  // One or more parameters were illegal.
}

mesa_rc vtss_appl_ptp_system_time_sync_mode_get(vtss_appl_ptp_system_time_sync_conf_t *const conf)
{
    PTP_CORE_LOCK_SCOPE();
    
    *conf = system_time_sync_conf;
    return VTSS_RC_OK;
}

mesa_rc ptp_clock_slave_statistics_enable(int instance, bool enable)
{
    PTP_CORE_LOCK_SCOPE();
    
    return vtss_ptp_clock_slave_statistics_enable(ptp_global.ptpi[instance], enable);
}

mesa_rc ptp_clock_slave_statistics_get(int instance, vtss_ptp_slave_statistics_t *statistics, bool clear)
{
    PTP_CORE_LOCK_SCOPE();
    
    return vtss_ptp_clock_slave_statistics_get(ptp_global.ptpi[instance], statistics, clear);
}

mesa_rc ptp_clock_path_trace_get(int instance, ptp_path_trace_t *trace)
{
    PTP_CORE_LOCK_SCOPE();
    if (instance < PTP_CLOCK_INSTANCES) {
        memcpy(trace, &ptp_global.ptpi[instance]->path_trace, sizeof(ptp_global.ptpi[instance]->path_trace));
        return VTSS_RC_OK;
    } else {
        return VTSS_RC_ERROR;
    }
}

mesa_rc ptp_clock_path_802_1as_status_get(int instance, vtss_ptp_clock_802_1as_bmca_t *status)
{
    PTP_CORE_LOCK_SCOPE();
    if (instance < PTP_CLOCK_INSTANCES) {
        *status = ptp_global.ptpi[instance]->bmca_802_1as;
        return VTSS_RC_OK;
    } else {
        return VTSS_RC_ERROR;
    }
}

mesa_rc ptp_port_path_802_1as_status_get(int instance, mesa_port_no_t port_no, vtss_ptp_port_802_1as_bmca_t *status)
{
    PTP_CORE_LOCK_SCOPE();
    if (instance < PTP_CLOCK_INSTANCES && port_no < mesa_port_cnt(nullptr)) {
        *status = ptp_global.ptpi[instance]->ptpPort[port_no].bmca_802_1as;
        return VTSS_RC_OK;
    } else {
        return VTSS_RC_ERROR;
    }
}

static u32 source_id_to_pin(meba_event_t source_id)
{
    u32 i;
    for (i = 0; i < mesa_cap_ts_io_cnt; i++) {
        if (ptp_io_pin[i].source_id == source_id) {
            return i;
        }
    }
    T_WG(VTSS_TRACE_GRP_1_PPS, "unknown source_id %d", source_id);
    return 0;
}

/*
 * IO pin interrupt handler
 */
static void io_pin_interrupt_handler(meba_event_t     source_id,
                                     u32                         instance_id)
{
    PTP_CORE_LOCK_SCOPE();
    
    mesa_timestamp_t                ts;
    u32                             tc;
    char str1[40];
    u32 io_pin;
    
    T_DG(VTSS_TRACE_GRP_1_PPS, "I/O pin event: source_id %d, instance_id %u", source_id, instance_id);
    io_pin = source_id_to_pin(source_id);
    T_DG(VTSS_TRACE_GRP_1_PPS, "I/O pin: %u, pin mode %d", io_pin, ptp_io_pin[io_pin].io_pin.pin);
    PTP_RC(mesa_ts_saved_timeofday_get(0, io_pin, &ts, &tc));
    if ((ptp_io_pin[io_pin].ptp_inst == 0) && (config_data.init_ext_clock_mode_rs422.mode == VTSS_PTP_RS422_MAIN_AUTO || config_data.init_ext_clock_mode_rs422.mode == VTSS_PTP_RS422_CALIB) &&
            ptp_io_pin[io_pin].io_pin.pin == VTSS_APPL_PTP_IO_MODE_ONE_PPS_SAVE) {
        u32 turnaround_latency;

        PTP_RC(mesa_ts_alt_clock_saved_get(NULL, &turnaround_latency));
        T_IG(VTSS_TRACE_GRP_1_PPS, "Saved turn around or onesec counter value: %u", turnaround_latency);
        // save calculated cable delay
        if (config_data.init_ext_clock_mode_rs422.mode == VTSS_PTP_RS422_CALIB) {
            static int calib_iteration = 0;
            static u32 turnaround_latency_accumulated = 0;
    
            if (calib_1pps_initiate) {
               calib_iteration = 0;
               turnaround_latency_accumulated = 0;
               calib_1pps_initiate = false;
            }
            calib_iteration++;
    
            turnaround_latency_accumulated += turnaround_latency;
    
            if (calib_iteration == 10) {
                config_data.init_ext_clock_mode_rs422.delay = turnaround_latency_accumulated / calib_iteration;  /* In calibration mode, store the raw measurement of the round-trip delay */
                calib_1pps_enabled = false;
            }

        } else {
            config_data.init_ext_clock_mode_rs422.delay = turnaround_latency / 2 + 19;  /* 19 ns is the additional delay in the path to the SUB module */
        }
        // wait 40 msec before sending timing message
        if (vtss_timer_start(&pps_msg_timer) != VTSS_RC_OK) {
            T_EG(VTSS_TRACE_GRP_CLOCK, "Unable to start pps_msg_timer");
        }
    } else {
        if ((ptp_io_pin[io_pin].ptp_inst >= 0) && ptp_io_pin[io_pin].io_pin.pin == VTSS_APPL_PTP_IO_MODE_ONE_PPS_SAVE) {
            T_IG(VTSS_TRACE_GRP_1_PPS, "inst: %u, devicetype %d, protocol %d", ptp_io_pin[io_pin].ptp_inst, config_data.conf[ptp_io_pin[io_pin].ptp_inst].clock_init.cfg.deviceType, config_data.conf[ptp_io_pin[io_pin].ptp_inst].clock_init.cfg.protocol);
            //if (config_data.conf[ptp_io_pin[io_pin].ptp_inst].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY &&
            //    config_data.conf[ptp_io_pin[io_pin].ptp_inst].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_ONE_PPS) {
            T_DG(VTSS_TRACE_GRP_1_PPS, "io_pin %u, time %s", io_pin, TimeStampToString (&ts, str1));
            if (config_data.conf[ptp_io_pin[io_pin].ptp_inst].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY ||
                    (config_data.conf[ptp_io_pin[io_pin].ptp_inst].clock_init.cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY &&
                     config_data.conf[ptp_io_pin[io_pin].ptp_inst].clock_init.cfg.protocol == VTSS_APPL_PTP_PROTOCOL_ONE_PPS)) {
                ptp_external_input_slave_function(&ptp_io_pin[io_pin].slave_data, ptp_io_pin[io_pin].ptp_inst, &ts, false);

            }
        }
    }
    if (ptp_io_pin[io_pin].io_pin.pin != VTSS_APPL_PTP_IO_MODE_ONE_PPS_DISABLE && ptp_io_pin[io_pin].io_pin.pin != VTSS_APPL_PTP_IO_MODE_WAVEFORM_OUTPUT) {
        PTP_RC(vtss_interrupt_source_hook_set(VTSS_MODULE_ID_PTP, io_pin_interrupt_handler, source_id, INTERRUPT_PRIORITY_NORMAL));
    }
}

static vtss_appl_ptp_ext_io_pin_cfg_t io_pin[MAX_VTSS_TS_IO_ARRAY_SIZE] = {VTSS_APPL_PTP_IO_MODE_ONE_PPS_DISABLE, VTSS_APPL_PTP_IO_MODE_ONE_PPS_DISABLE, VTSS_APPL_PTP_IO_MODE_ONE_PPS_DISABLE, VTSS_APPL_PTP_IO_MODE_ONE_PPS_DISABLE};
static u32 io_pin_domain[MAX_VTSS_TS_IO_ARRAY_SIZE] = {0,0,0,0};
// the first two pins are used by the PTP application
static ptp_io_pin_usage_t io_pin_default_usage[MAX_VTSS_TS_IO_ARRAY_SIZE] = {PTP_IO_PIN_USAGE_MAIN, PTP_IO_PIN_USAGE_MAIN, PTP_IO_PIN_USAGE_NONE, PTP_IO_PIN_USAGE_NONE};

static mesa_rc ptp_pin_init(void)
{
    u32 pin_idx;
    mesa_ts_ext_io_mode_t io_mode;
    T_I("initialize IO PIN mode");
    for (pin_idx = 0; pin_idx < mesa_cap_ts_io_cnt; pin_idx++) {
        ptp_io_pin[pin_idx].io_pin.pin = io_pin[pin_idx];
        ptp_io_pin[pin_idx].io_pin.domain = io_pin_domain[pin_idx];
        ptp_io_pin[pin_idx].io_pin.freq = 1;
        ptp_io_pin[pin_idx].ptp_inst = -1;
        ptp_io_pin[pin_idx].slave_data.one_pps_slave_port_no =  VTSS_PORT_NO_NONE;
        ptp_io_pin[pin_idx].slave_data.enable_t1 = false;
        ptp_io_pin[pin_idx].usage = io_pin_default_usage[pin_idx];
        T_I("IO PIN %d init", pin_idx);
        if (pin_idx > 1) {
            ptp_io_pin[pin_idx].source_id = init_int_source_id[pin_idx];
            PTP_RETURN(vtss_interrupt_source_hook_set(VTSS_MODULE_ID_PTP, io_pin_interrupt_handler,
                       init_int_source_id[pin_idx],
                       INTERRUPT_PRIORITY_NORMAL));
            io_mode.freq = ptp_io_pin[pin_idx].io_pin.freq;
            io_mode.domain = ptp_io_pin[pin_idx].io_pin.domain;
            io_mode.pin = vtss_appl_io_pin_mode_to_ts_io_pin_mode(ptp_io_pin[pin_idx].io_pin.pin);
            PTP_RETURN(mesa_ts_external_io_mode_set(NULL, pin_idx, &io_mode));
            T_I("source %d", init_int_source_id[pin_idx]);
        }
    }
    return VTSS_RC_OK;
}

static mesa_rc io_pin_instance_set(int instance, u32 io_pin, BOOL enable)
{
    mesa_rc rc = VTSS_RC_OK;
    if (instance < PTP_CLOCK_INSTANCES && io_pin < mesa_cap_ts_io_cnt) {
        T_I("enable %d, instance %d, io_pin %d", enable, instance, io_pin);
        if (enable) {
            ptp_io_pin[io_pin].ptp_inst = instance;
        } else if (ptp_io_pin[io_pin].ptp_inst == instance) {
            ptp_io_pin[io_pin].ptp_inst = -1;
        }
    } else {
        T_W("Invalid instance %d or io_pin %d", instance, io_pin);
        rc = VTSS_RC_ERROR;
    }
    return rc;
}

mesa_rc vtss_appl_ptp_io_pin_conf_get(u32 io_pin, vtss_appl_ptp_ext_io_mode_t *const mode)
{
    PTP_CORE_LOCK_SCOPE();
    if (io_pin < mesa_cap_ts_io_cnt) {
        if (ptp_io_pin[io_pin].usage == PTP_IO_PIN_USAGE_IO_PIN) {
            *mode = ptp_io_pin[io_pin].io_pin;
            mode->one_pps_slave_port_number = iport2uport(ptp_io_pin[io_pin].slave_data.one_pps_slave_port_no);
            T_I("slaveport %d", ptp_io_pin[io_pin].slave_data.one_pps_slave_port_no);
        } else {
            return vtss_appl_ptp_io_pin_conf_default_get(io_pin, mode);

        }
        return VTSS_RC_OK;
    } else {
        return VTSS_RC_ERROR;
    }
}

mesa_rc vtss_appl_ptp_io_pin_conf_default_get(u32 pin_idx, vtss_appl_ptp_ext_io_mode_t *const mode)
{
    if (pin_idx < mesa_cap_ts_io_cnt) {
        mode->pin = io_pin[pin_idx];
        mode->domain = io_pin_domain[pin_idx];
        mode->freq = 1;
        mode->one_pps_slave_port_number = mesa_cap_port_cnt;
        return VTSS_RC_OK;
    } else {
        return VTSS_RC_ERROR;
    }
}

mesa_rc vtss_appl_ptp_io_pin_conf_set(u32 io_pin, const vtss_appl_ptp_ext_io_mode_t *const mode)
{
    PTP_CORE_LOCK_SCOPE();
    mesa_ts_ext_io_mode_t io_mode;
    if (io_pin < mesa_cap_ts_io_cnt) {
        if (ptp_io_pin[io_pin].usage == PTP_IO_PIN_USAGE_NONE || ptp_io_pin[io_pin].usage == PTP_IO_PIN_USAGE_IO_PIN) {
            ptp_io_pin[io_pin].io_pin = *mode;
            ptp_io_pin[io_pin].slave_data.one_pps_slave_port_no = uport2iport(mode->one_pps_slave_port_number);
            if (ptp_io_pin[io_pin].slave_data.one_pps_slave_port_no == VTSS_PORT_NO_NONE && mode->pin != VTSS_APPL_PTP_IO_MODE_ONE_PPS_DISABLE) {
                ptp_io_pin[io_pin].slave_data.one_pps_slave_port_no = mesa_cap_port_cnt;
            }
            ptp_io_pin[io_pin].slave_data.enable_t1 = false;
            if ((mode->pin == VTSS_APPL_PTP_IO_MODE_ONE_PPS_SAVE || mode->pin == VTSS_APPL_PTP_IO_MODE_ONE_PPS_LOAD || mode->pin == VTSS_APPL_PTP_IO_MODE_ONE_PPS_OUTPUT) && ptp_io_pin[io_pin].slave_data.one_pps_slave_port_no < mesa_cap_port_cnt) {
                // if mode is 'SAVE', 'LOAD' or OUTPUT, we assume this pin is used as an internal port in a C-DTC
                // and therefore the port must be assigned the same domain as the pin.
                port_data[ptp_io_pin[io_pin].slave_data.one_pps_slave_port_no].port_domain = mode->domain;
                PTP_RC(ptp_ts_operation_mode_set(ptp_io_pin[io_pin].slave_data.one_pps_slave_port_no));
            }
            T_IG(VTSS_TRACE_GRP_1_PPS, "set hook: source_id %d", ptp_io_pin[io_pin].source_id);
            if (VTSS_RC_OK != vtss_interrupt_source_hook_set(VTSS_MODULE_ID_PTP, io_pin_interrupt_handler, ptp_io_pin[io_pin].source_id, INTERRUPT_PRIORITY_NORMAL)) {
                T_IG(VTSS_TRACE_GRP_1_PPS, "source already hooked");
            }
            io_mode.freq = ptp_io_pin[io_pin].io_pin.freq;
            io_mode.domain = ptp_io_pin[io_pin].io_pin.domain;
            io_mode.pin = vtss_appl_io_pin_mode_to_ts_io_pin_mode(ptp_io_pin[io_pin].io_pin.pin);
            PTP_RC(mesa_ts_external_io_mode_set(NULL, io_pin, &io_mode));
            if (ptp_io_pin[io_pin].io_pin.pin == VTSS_APPL_PTP_IO_MODE_ONE_PPS_DISABLE) {
                ptp_io_pin[io_pin].usage = PTP_IO_PIN_USAGE_NONE;
                T_IG(VTSS_TRACE_GRP_1_PPS, "IO_PIN[%d] usage = NONE", io_pin);
            } else {
                ptp_io_pin[io_pin].usage = PTP_IO_PIN_USAGE_IO_PIN;
                T_IG(VTSS_TRACE_GRP_1_PPS, "IO_PIN[%d] usage = IO_PIN", io_pin);
            }
            return VTSS_RC_OK;
        } else {
            return VTSS_RC_ERROR;
        }
    } else {
        return VTSS_RC_ERROR;
    }
}

u32 ptp_instance_2_timing_domain(int instance)
{
    if (instance < PTP_CLOCK_INSTANCES) {
        return config_data.conf[instance].clock_init.cfg.clock_domain;
    } else {
        return 0;
    }
}

extern "C" int ptp_icli_cmd_register();

/****************************************************************************/
/*  Initialization functions                                                */
/****************************************************************************/

mesa_rc ptp_init(vtss_init_data_t *data)
{
    vtss_isid_t isid = data->isid;
    int i;
    mesa_rc rc;
    switch (data->cmd) {
    case INIT_CMD_EARLY_INIT:
        /* Initialize and register trace ressources */
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);
        break;
    case INIT_CMD_INIT:
        ptp_global.ready = false;
        critd_init(&ptp_global.coremutex, "ptp_core", VTSS_MODULE_ID_PTP, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
        PTP_CORE_UNLOCK();

        // Cache various MESA capabilities for fast access.
        meba_cap_synce_dpll_mode_dual            = MESA_CAP(MEBA_CAP_SYNCE_DPLL_MODE_DUAL);
        meba_cap_synce_dpll_mode_single          = MESA_CAP(MEBA_CAP_SYNCE_DPLL_MODE_SINGLE);
        mesa_cap_misc_chip_family                = MESA_CAP(MESA_CAP_MISC_CHIP_FAMILY);
        mesa_cap_oam_used_as_ptp_protocol        = MESA_CAP(MESA_CAP_OAM_USED_AS_PTP_PROTOCOL);
        mesa_cap_packet_auto_tagging             = MESA_CAP(MESA_CAP_PACKET_AUTO_TAGGING);
        mesa_cap_phy_ts                          = MESA_CAP(MESA_CAP_PHY_TS);
        mesa_cap_port_cnt                        = MESA_CAP(MESA_CAP_PORT_CNT);
        mesa_cap_synce_ann_auto_transmit         = MESA_CAP(MESA_CAP_SYNCE_ANN_AUTO_TRANSMIT);
        mesa_cap_ts                              = MESA_CAP(MESA_CAP_TS);
        mesa_cap_ts_asymmetry_comp               = MESA_CAP(MESA_CAP_TS_ASYMMETRY_COMP);
        mesa_cap_ts_bc_ts_combo_is_special       = MESA_CAP(MESA_CAP_TS_BC_TS_COMBO_IS_SPECIAL);
        mesa_cap_ts_c_dtc_supported              = MESA_CAP(MESA_CAP_TS_C_DTC_SUPPORTED);
        mesa_cap_ts_delay_req_auto_resp          = MESA_CAP(MESA_CAP_TS_DELAY_REQ_AUTO_RESP);
        mesa_cap_ts_domain_cnt                   = MESA_CAP(MESA_CAP_TS_DOMAIN_CNT);
        mesa_cap_ts_has_alt_pin                  = MESA_CAP(MESA_CAP_TS_HAS_ALT_PIN);
        mesa_cap_ts_has_ptp_io_pin               = MESA_CAP(MESA_CAP_TS_HAS_PTP_IO_PIN);
        mesa_cap_ts_hw_fwd_e2e_1step_internal    = MESA_CAP(MESA_CAP_TS_HW_FWD_E2E_1STEP_INTERNAL);
        mesa_cap_ts_hw_fwd_p2p_1step             = MESA_CAP(MESA_CAP_TS_HW_FWD_P2P_1STEP);
        mesa_cap_ts_io_cnt                       = MESA_CAP(MESA_CAP_TS_IO_CNT);
        mesa_cap_ts_internal_mode_supported      = MESA_CAP(MESA_CAP_TS_INTERNAL_MODE_SUPPORTED);
        mesa_cap_ts_internal_ports_req_twostep   = MESA_CAP(MESA_CAP_TS_INTERNAL_PORTS_REQ_TWOSTEP);
        mesa_cap_ts_missing_tx_interrupt         = MESA_CAP(MESA_CAP_TS_MISSING_TX_INTERRUPT);
        mesa_cap_ts_org_time                     = MESA_CAP(MESA_CAP_TS_ORG_TIME);
        mesa_cap_ts_p2p_delay_comp               = MESA_CAP(MESA_CAP_TS_P2P_DELAY_COMP);
        mesa_cap_ts_pps_via_configurable_io_pins = MESA_CAP(MESA_CAP_TS_PPS_VIA_CONFIGURABLE_IO_PINS);
        mesa_cap_ts_ptp_rs422                    = MESA_CAP(MESA_CAP_TS_PTP_RS422);
        mesa_cap_ts_twostep_always_required      = MESA_CAP(MESA_CAP_TS_TWOSTEP_ALWAYS_REQUIRED);
        mesa_cap_ts_use_external_input_servo     = MESA_CAP(MESA_CAP_TS_USE_EXTERNAL_INPUT_SERVO);
        vtss_appl_cap_max_acl_rules_pr_ptp_clock = MESA_CAP(VTSS_APPL_CAP_MAX_ACL_RULES_PR_PTP_CLOCK);
        vtss_appl_cap_ptp_clock_cnt              = MESA_CAP(VTSS_APPL_CAP_PTP_CLOCK_CNT);
        vtss_appl_cap_zarlink_servo_type         = MESA_CAP(VTSS_APPL_CAP_ZARLINK_SERVO_TYPE);

        vtss_flag_init( &ptp_global_control_flags );
        acl_rules_init();
        if (mesa_cap_ts_ptp_rs422) {
            PTP_RC(ptp_1pps_serial_init());
        }

        if (mesa_cap_phy_ts) {
            PTP_RC(ptp_1pps_sync_init());
            PTP_RC(ptp_1pps_closed_loop_init());
        }

#ifdef VTSS_SW_OPTION_ICFG
        PTP_RC(ptp_icfg_init());
#endif
#ifdef VTSS_SW_OPTION_PRIVATE_MIB
        vtss_ptp_mib_init();
#endif
#ifdef VTSS_SW_OPTION_JSON_RPC
        vtss_appl_ptp_json_init();
#endif
#ifdef VTSS_SW_OPTION_JSON_RPC
        vtss_appl_ptp_json_init();
#endif
        ptp_icli_cmd_register();
        T_IG(0, "INIT_CMD_INIT PTP" );
        break;
    case INIT_CMD_START:
        T_IG(0, "INIT_CMD_START PTP");
        break;
    case INIT_CMD_CONF_DEF:
        T_IG(0, "INIT_CMD_CONF_DEF PTP" );
        if (isid == VTSS_ISID_GLOBAL)
            vtss_flag_setbits(&ptp_global_control_flags, CTLFLAG_PTP_DEFCONFIG);
        break;
    case INIT_CMD_MASTER_UP:
        if (mesa_cap_ts_ptp_rs422) {
            if ((rc = meba_ptp_rs422_conf_get(board_instance, &rs422_conf)) != VTSS_OK) {
                T_E("RS422 configuration not supported: %s", error_txt(rc));
                return rc;
            } else {
                T_I("RS422 configuration: gpio_rs422_1588_mstoen %d, gpio_rs422_1588_slvoen %d, \nptp_pin_ldst %d, ptp_pin_ppso %d, \nptp_rs422_pps_int_id %d, ptp_rs422_ldsv_int_id %d",
                    rs422_conf.gpio_rs422_1588_mstoen,
                    rs422_conf.gpio_rs422_1588_slvoen,
                    rs422_conf.ptp_pin_ldst,
                    rs422_conf.ptp_pin_ppso,
                    rs422_conf.ptp_rs422_pps_int_id,
                    rs422_conf.ptp_rs422_ldsv_int_id);
            }
        }

        // Read PTP port calibration from file on Linux filesystem
        {
            int ptp_port_calib_file = open(PTP_CALIB_FILE_NAME, O_RDONLY);
            if (ptp_port_calib_file != -1) {
                u32 dummy;

                ssize_t numread = read(ptp_port_calib_file, &ptp_port_calibration.version, sizeof(u32));
                numread += read(ptp_port_calib_file, &ptp_port_calibration.crc32, sizeof(u32));
                numread += read(ptp_port_calib_file, ptp_port_calibration.port_calibrations.data(), mesa_cap_port_cnt * sizeof(port_calibrations_s));
                numread += read(ptp_port_calib_file, &dummy, sizeof(u32));

                if (numread == 2 * sizeof(u32) + mesa_cap_port_cnt * sizeof(port_calibrations_s)) {
                    if (ptp_port_calibration.version != PTP_CURRENT_CALIB_FILE_VER) {   // File was read. Check version of file is current.
                        T_I("PTP port calibration data file has incorrect version - using default values.");
                        memset(ptp_port_calibration.port_calibrations.data(), 0, mesa_cap_port_cnt * sizeof(port_calibrations_s));
                    } else {  // File was read and version was OK. Check the CRC32.
                        u32 crc32 = vtss_crc32((const unsigned char*)&ptp_port_calibration.version, sizeof(u32));
                        crc32 = vtss_crc32_accumulate(crc32, (const unsigned char*)ptp_port_calibration.port_calibrations.data(), mesa_cap_port_cnt * sizeof(port_calibrations_s));

                        if (crc32 != ptp_port_calibration.crc32) {
                            T_W("PTP port calibration data file is corrupt (incorrect CRC32) - using default values.");
                            memset(ptp_port_calibration.port_calibrations.data(), 0, mesa_cap_port_cnt * sizeof(port_calibrations_s));
                        }
                    } 
                } else {
                    T_W("PTP port calibration data file is corrupt (incorrect length) - using default values.");
                    memset(ptp_port_calibration.port_calibrations.data(), 0, mesa_cap_port_cnt * sizeof(port_calibrations_s));
                }

                if (close(ptp_port_calib_file) == -1) {
                    T_W("Could not close PTP port calibration data file.");
                }
            } else {
                T_I("No PTP port calibration data file - using default values.");
                memset(ptp_port_calibration.port_calibrations.data(), 0, mesa_cap_port_cnt * sizeof(port_calibrations_s));
            }
        }

        ptp_conf_read(false);
        port_data_pre_initialize();

        for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
            /* Create a clock servo instance */
#ifdef SW_OPTION_BASIC_PTP_SERVO
            basic_servo[i] = vtss::create<VTSS_MODULE_ID_PTP, ptp_basic_servo>(i, &config_data.conf[i].filter_params, &config_data.conf[i].servo_params, config_data.conf[i].clock_init.numberPorts);
#endif // SW_OPTION_BASIC_PTP_SERVO
#if defined(VTSS_SW_OPTION_ZLS30387)
            advanced_servo[i] = vtss::create<VTSS_MODULE_ID_PTP, ptp_ms_servo>(i, &config_data.conf[i].servo_params, &config_data.conf[i].clock_init.cfg, config_data.conf[i].clock_init.numberPorts);
#endif

            /* Create a PTP engine */
            {
#if defined(VTSS_SW_OPTION_ZLS30387)
                ptp_servo *servo = advanced_servo[i];
#endif //  defined(VTSS_SW_OPTION_ZLS30387)

                ptp_global.ptpi[i] = vtss_ptp_clock_add(&config_data.conf[i].clock_init,
                                                        &config_data.conf[i].time_prop,
                                                        &config_data.conf[i].port_config[0],
                                                        servo,
                                                        i);
            }
            VTSS_ASSERT(ptp_global.ptpi[i] != NULL);
            if (mesa_cap_synce_ann_auto_transmit) {
                for (int j = 0; j < mesa_cap_port_cnt; j++) {
                    T_IG(0, "set afi pointer for inst %d, port %d", i,j);
                    ptp_global.ptpi[i]->ptpPort[j].msm.afi = 0;
                    ptp_global.ptpi[i]->ptpPort[j].ansm.afi = 0;
                }
            }
        }
        if (mesa_cap_ts_pps_via_configurable_io_pins) {
            PTP_RC(ptp_pin_init());
        }

        T_IG(0, "INIT_CMD_MASTER_UP");
        break;
    case INIT_CMD_SWITCH_ADD:
        T_IG(0, "INIT_CMD_SWITCH_ADD resuming thread- ISID %u", isid);
        if (isid == VTSS_ISID_START) {
            vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
                               ptp_thread,
                               0,
                               "PTP",
                               nullptr,
                               0,
                               &ptp_thread_handle,
                               &ptp_thread_block);
//            vtss_thread_resume(ptp_thread_handle);
            vtss_flag_setbits(&ptp_global_control_flags, CTLFLAG_PTP_SET_ACL);
        } else {
            T_EG(0, "INIT_CMD_SWITCH_ADD - unknown ISID %u", isid);
        }
        break;
    case INIT_CMD_MASTER_DOWN:
        T_IG(0, "INIT_CMD_MASTER_DOWN - ISID %u", isid);
        break;
    default:
        break;
    }

    return 0;
}

// Convert error code to text
// In : rc - error return code
const char *ptp_error_txt(mesa_rc rc)
{
    switch (rc)
    {
    case PTP_ERROR_INV_PARAM:                       return "Invalid parameter error returned from PTP";
    case PTP_RC_INVALID_PORT_NUMBER:                return "Invalid port number";
    case PTP_RC_INTERNAL_PORT_NOT_ALLOWED:          return "Enabling internal mode is only valid for a Transparent clock";
    case PTP_RC_MISSING_PHY_TIMESTAMP_RESOURCE:     return "No timestamp engine is available in the PHY";
    case PTP_RC_MISSING_IP_ADDRESS:                 return "cannot set up unicast ACL before my ip address is defined";
    case PTP_RC_UNSUPPORTED_ACL_FRAME_TYPE:         return "unsupported ACL frame type";
    case PTP_RC_UNSUPPORTED_PTP_ENCAPSULATION_TYPE: return "unsupported PTP ancapsulation type";
    case PTP_RC_UNSUPPORTED_1PPS_OPERATION_MODE:    return "unsupported 1PPS operation mode";
    case PTP_RC_CONFLICT_NTP_ENABLED:               return "cannot set system time if ntp is enabled";
    case PTP_RC_CONFLICT_PTP_ENABLED:               return "cannot get system time if ptp BC/Slave is enabled";
    case PTP_RC_MULTIPLE_SLAVES:                    return "cannot create multiple slave clocks within the same clock domain";
    case PTP_RC_MULTIPLE_TC:                        return "cannot create multiple transparent clocks";
    case PTP_RC_CLOCK_DOMAIN_CONFLICT:              return "Clock domain conflict: a port can only be active in one clock domain";
    case PTP_RC_MISSING_ACL_RESOURCES:              return "Cannot add ACL resource";
    case PTP_RC_ADJ_METHOD_CHANGE_NOT_ALLOWED:      return "Cannot change preferred adj method if active PTP instances are using clock domain 0";

    default:                                        return "Unknown error returned from PTP";

    }
}

const char *sync_src_type_2_txt(vtss_ptp_synce_src_type_t s)
{
    switch (s) {
        case VTSS_PTP_SYNCE_NONE: return "NONE";
        case VTSS_PTP_SYNCE_ELEC: return "ELEC";
        case VTSS_PTP_SYNCE_PAC : return "PAC";
        default: return "UNKNOWN";
    }
}

static vtss_ptp_synce_src_t current_src = {VTSS_PTP_SYNCE_NONE, 0};

mesa_rc ptp_set_selected_src(vtss_ptp_synce_src_t *src)
{
    PTP_CORE_LOCK_SCOPE();
    if (src->type != current_src.type || src->ref != current_src.ref) {
        current_src = *src;
        T_IG(VTSS_TRACE_GRP_MS_SERVO, "current_src changed to type %s, ref %d", sync_src_type_2_txt(current_src.type), current_src.ref);
    }
    return VTSS_RC_OK;
}

static mesa_rc get_selected_src(vtss_ptp_synce_src_t *src)
{
    *src = current_src;
    T_NG(VTSS_TRACE_GRP_MS_SERVO, "current_src type %s, ref %d", sync_src_type_2_txt(current_src.type), current_src.ref);
    return VTSS_RC_OK;
}

mesa_rc ptp_get_selected_src(vtss_ptp_synce_src_t *src)
{
    PTP_CORE_LOCK_SCOPE();
    return get_selected_src(src);
}

const char *sync_servo_mode_2_txt(vtss_ptp_servo_mode_t s)
{
    switch (s) {
        case VTSS_PTP_SERVO_NONE     : return "NONE";
        case VTSS_PTP_SERVO_HYBRID   : return "HYBRID";
        case VTSS_PTP_SERVO_ELEC     : return "ELEC";
        case VTSS_PTP_SERVO_PAC      : return "PACKET";
        case VTSS_PTP_SERVO_HOLDOVER : return "HOLDOVER";
        default: return "UNKNOWN";
    }
}

mesa_rc vtss_ptp_get_servo_mode_ref(int inst, vtss_ptp_servo_mode_ref_t *mode_ref)
{
    if (inst >= 0 && inst < PTP_CLOCK_INSTANCES) {
        *mode_ref = current_servo_mode_ref[inst];
        return VTSS_RC_OK;
    } else {
        return VTSS_RC_ERROR;
    }
}

static bool vtss_ptp_aci_sync_filter_type_get(int instance)
{
    u32 ft = ptp_global.ptpi[instance]->clock_init->cfg.filter_type;
    bool rv = (ft == PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_PHASE ||
                ft == PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE ||
                ft == PTP_FILTERTYPE_ACI_BASIC_PHASE ||
                ft == PTP_FILTERTYPE_ACI_BASIC_PHASE_LOW);
    T_NG(VTSS_TRACE_GRP_MS_SERVO, "instance %d sync_filter type %s", instance, rv ? "true" : "false");
    return rv;
}

static mesa_rc vtss_ptp_update_selected_src(void)
{
    mesa_rc rc = VTSS_RC_OK;
    int instance;
    vtss_ptp_synce_src_t new_src;
    vtss_ptp_servo_mode_t servo_mode;
    int active_ref;
    bool instance_deleted = false;
    CapArray<int, VTSS_APPL_CAP_PTP_CLOCK_CNT> active_domain;
    PTP_RC(get_selected_src(&new_src));
    for (instance = 0; instance < PTP_CLOCK_INSTANCES; instance++) {
        servo_mode = VTSS_PTP_SERVO_NONE;
        active_ref = -1;
        active_domain[instance] = -1;
        if (ptp_global.ptpi[instance] != 0) {
            int clock_option = vtss_ptp_adjustment_method(instance);
            if (ptp_global.ptpi[instance]->clock_init->cfg.deviceType == VTSS_APPL_PTP_DEVICE_ORD_BOUND || 
                ptp_global.ptpi[instance]->clock_init->cfg.deviceType == VTSS_APPL_PTP_DEVICE_SLAVE_ONLY ||
                ptp_global.ptpi[instance]->clock_init->cfg.deviceType == VTSS_APPL_PTP_DEVICE_MASTER_ONLY) {
                // if more instances uses the same domain, the one with lowest instance number has first priority
                if (active_domain[ptp_global.ptpi[instance]->clock_init->cfg.clock_domain] == -1 && ptp_global.ptpi[instance]->slavePort != 0) {
                    active_domain[ptp_global.ptpi[instance]->clock_init->cfg.clock_domain] = instance;
                    active_ref = active_domain[ptp_global.ptpi[instance]->clock_init->cfg.clock_domain];
                }
                if (ptp_global.ptpi[instance]->clock_init->cfg.clock_domain == 0) {
                    vtss_appl_ptp_profile_t profile = ptp_global.ptpi[instance]->clock_init->cfg.profile;
                    switch (new_src.type) {
                        case VTSS_PTP_SYNCE_NONE:
                            if (clock_option == CLOCK_OPTION_SYNCE_DPLL) {
                                // Put PTP into packet mode unless PTP is using the 8265.1 profile. In that case PTP has to be selected by SyncE and that is covered by the VTSS_PTP_SYNCE_PAC case below.
                                if (ptp_global.ptpi[instance]->clock_init->cfg.profile != VTSS_APPL_PTP_PROFILE_G_8265_1) {
                                    servo_mode = VTSS_PTP_SERVO_PAC;
                                } else {
                                    // Servo shall be put into holdover mode (or freerun mode, if holdover is not possible).
                                    if (current_servo_mode_ref[instance].mode == VTSS_PTP_SERVO_PAC || current_servo_mode_ref[instance].mode == VTSS_PTP_SERVO_HYBRID) {
                                        servo_mode = VTSS_PTP_SERVO_HOLDOVER;
                                    } else if (current_servo_mode_ref[instance].mode == VTSS_PTP_SERVO_HOLDOVER) {
                                        servo_mode = VTSS_PTP_SERVO_HOLDOVER;
                                    } else if (current_servo_mode_ref[instance].mode == VTSS_PTP_SERVO_ELEC) {
                                        servo_mode = VTSS_PTP_SERVO_ELEC;
                                    }
                                }
                            } else {
                                // Always in packet mode
                                servo_mode = VTSS_PTP_SERVO_PAC;
                            }
                            T_NG(VTSS_TRACE_GRP_MS_SERVO, "servo mode is %s, ref %d", sync_servo_mode_2_txt(servo_mode), active_ref);
                            break;
                        case VTSS_PTP_SYNCE_ELEC:
                            if (clock_option == CLOCK_OPTION_SYNCE_DPLL) {
                                bool aci_sync_mode = vtss_ptp_aci_sync_filter_type_get(instance);
                                if (profile == VTSS_APPL_PTP_PROFILE_G_8265_1 || !aci_sync_mode) {
                                    //put PTP into Electrical mode
                                    servo_mode = VTSS_PTP_SERVO_ELEC;
                                    active_ref = new_src.ref;
                                } else {
                                    //put PTP into Hybrid mode
                                    servo_mode = VTSS_PTP_SERVO_HYBRID;
                                } 
                            } else {
                                // Always in packet mode
                                servo_mode = VTSS_PTP_SERVO_PAC;
                            }
                            T_NG(VTSS_TRACE_GRP_MS_SERVO, "servo mode is %s, ref %d", sync_servo_mode_2_txt(servo_mode), active_ref);
                            break;
                        case VTSS_PTP_SYNCE_PAC:
                            if (clock_option == CLOCK_OPTION_SYNCE_DPLL) {
                                if (ptp_global.ptpi[instance]->clock_init->cfg.profile == VTSS_APPL_PTP_PROFILE_G_8265_1) {
                                    //put PTP into Packet mode
                                    // Set active ref
                                    servo_mode = VTSS_PTP_SERVO_PAC;
                                    active_ref = new_src.ref;
                                } else {
                                    //put PTP into Packet mode
                                    servo_mode = VTSS_PTP_SERVO_PAC;
                                } 
                            } else {
                                if (ptp_global.ptpi[instance]->clock_init->cfg.profile == VTSS_APPL_PTP_PROFILE_G_8265_1) {
                                    // Shall not be possible but put PTP into Packet mode
                                    servo_mode = VTSS_PTP_SERVO_PAC;
                                    active_ref = new_src.ref;
                                    T_IG(0, "Instance %d unsupported configuration", instance);
                                } else {
                                    //put PTP into Packet mode
                                    servo_mode = VTSS_PTP_SERVO_PAC;
                                } 
                            }
                            T_NG(VTSS_TRACE_GRP_MS_SERVO, "instance %d, servo mode is %s, ref %d", instance, sync_servo_mode_2_txt(servo_mode), active_ref);
                            break;
                        default:
                            T_WG(VTSS_TRACE_GRP_MS_SERVO, "Instance %d Unknown src type", instance);
                            break;
                    }
                } else {
                    // domain != 0, i.e. no synce dependency
                    servo_mode = VTSS_PTP_SERVO_PAC;
                    T_NG(VTSS_TRACE_GRP_MS_SERVO, "servo mode is %s, ref %d", sync_servo_mode_2_txt(servo_mode), active_ref);
                }
            }
            // update servo state
            if (servo_mode != current_servo_mode_ref[instance].mode) {
                T_IG(VTSS_TRACE_GRP_MS_SERVO, "instance %d, servo mode changed to %s mode, ref %d", instance, sync_servo_mode_2_txt(servo_mode), active_ref);
                switch (servo_mode) {
                    case VTSS_PTP_SERVO_HYBRID:
                        if (vtss_ptp_switch_to_hybrid_mode(instance) == VTSS_RC_OK) {
                            if (clock_option == CLOCK_OPTION_SYNCE_DPLL) {
                                mesa_ts_domain_adjtimer_set(NULL, 0, 0);  // Make sure the frequency offset of the LTC is 0 so that it does not cause the phase to drift
                            }
                            if (instance == active_ref) {
                                (void)vtss_ptp_set_active_ref(active_ref);
                            }
                            T_IG(VTSS_TRACE_GRP_MS_SERVO, "Servo mode changed to 'hybrid mode'.");
                        } else {
                            T_WG(VTSS_TRACE_GRP_MS_SERVO, "Could not change servo mode to 'hybrid mode'.");
                        }
                        break;
                    case VTSS_PTP_SERVO_ELEC:
                        vtss_ptp_set_active_electrical_ref(active_ref);
                        T_IG(VTSS_TRACE_GRP_MS_SERVO, "change servo mode to 'electrical mode'.");
                        break;
                    case VTSS_PTP_SERVO_PAC:
                        BOOL enable;
                        if (vtss_ptp_force_holdover_get(instance, &enable) == VTSS_RC_OK) {
                            if (enable == TRUE) {
                                if (vtss_ptp_force_holdover_set(instance, FALSE) == VTSS_RC_OK) {
                                    T_IG(VTSS_TRACE_GRP_MS_SERVO, "Servo mode 'holdover' cleared.");
                                } else {
                                    T_WG(VTSS_TRACE_GRP_MS_SERVO, "Could not clear servo mode 'holdover'.");
                                }
                            }
                        } else {
                            T_WG(VTSS_TRACE_GRP_MS_SERVO, "Could not get servo mode 'holdover' state.");
                        }
                        if (vtss_ptp_switch_to_packet_mode(instance) == VTSS_RC_OK) {
                            if (clock_option == CLOCK_OPTION_SYNCE_DPLL) {
                                vtss_tod_set_adjtimer(0, 0LL);  // Make sure the frequency offset of the LTC is 0 so that it does not cause the phase to drift
                            }
                            if (instance == active_ref) {
                                (void)vtss_ptp_set_active_ref(active_ref);
                            }
                            T_IG(VTSS_TRACE_GRP_MS_SERVO, "Servo mode changed to 'packet mode'.");
                        } else {
                            T_WG(VTSS_TRACE_GRP_MS_SERVO, "Could not change servo mode to 'packet mode'.");
                        }
                        break;
                    case VTSS_PTP_SERVO_HOLDOVER:
                        if (vtss_ptp_force_holdover_set(instance, TRUE) == VTSS_RC_OK) {
                            T_IG(VTSS_TRACE_GRP_MS_SERVO, "Servo mode 'holdover' set.");
                        } else {
                            T_WG(VTSS_TRACE_GRP_MS_SERVO, "Could not set servo mode 'holdover'.");
                        }
                        break;
                    default:
                        instance_deleted = true;
                        T_IG(VTSS_TRACE_GRP_MS_SERVO, "Instance %d is not active", instance);
                        break;
                }
            } else if (active_ref != current_servo_mode_ref[instance].ref) {
                //Servo mode cot changed, but reference is changed
                T_IG(VTSS_TRACE_GRP_MS_SERVO, "instance %d, servo mode not changed %s mode, but ref changed to %d", instance, sync_servo_mode_2_txt(servo_mode), active_ref);
                switch (servo_mode) {
                    case VTSS_PTP_SERVO_HYBRID:
                    case VTSS_PTP_SERVO_PAC:
                        if (instance == active_ref) {
                            (void)vtss_ptp_set_active_ref(active_ref);
                        }
                        break;
                    case VTSS_PTP_SERVO_ELEC:
                        vtss_ptp_set_active_electrical_ref(active_ref);
                        break;
                    default:
                        T_IG(VTSS_TRACE_GRP_MS_SERVO, "Instance %d is not active", instance);
                        break;
                }
                
            }
            current_servo_mode_ref[instance].mode = servo_mode;
            current_servo_mode_ref[instance].ref = active_ref;
            T_DG(VTSS_TRACE_GRP_MS_SERVO, "instance %d, servo mode %s ref %d", instance, sync_servo_mode_2_txt(servo_mode), active_ref);
        }
    }
    if (instance_deleted) {
        bool change = true;
        //If no PTP instances uses the Servo, electrical mode must be set
        for (instance = 0; instance < PTP_CLOCK_INSTANCES; instance++) {
            T_DG(VTSS_TRACE_GRP_MS_SERVO, "Instance %d servo_mode %s", instance, sync_servo_mode_2_txt(current_servo_mode_ref[instance].mode));
            if (current_servo_mode_ref[instance].mode != VTSS_PTP_SERVO_NONE) {
                change = false;
            }
        }
        if (change) {
            T_IG(VTSS_TRACE_GRP_MS_SERVO, "No Instance use the servo");
            vtss_ptp_set_active_electrical_ref(new_src.ref);
        }
    }
    return rc;
}

mesa_rc vtss_ptp_switch_to_packet_mode(int instance)
{
    vtss_rc rc = VTSS_RC_ERROR;

    T_IG(VTSS_TRACE_GRP_MS_SERVO, "Instance %d", instance);
    if (instance >= 0 && instance < PTP_CLOCK_INSTANCES) {
        if (ptp_global.ptpi[instance] != 0) {
#if defined(VTSS_SW_OPTION_SYNCE)
            if (config_data.conf[instance].clock_init.cfg.clock_domain == 0) {
                if (vtss_synce_ptp_clock_hybrid_mode_set(FALSE) != VTSS_RC_OK) {
                    T_W("vtss_synce_ptp_clock_hybrid_mode_set(FALSE) returned error.");
                }
            }
#endif
            rc = ptp_global.ptpi[instance]->ssm.servo->switch_to_packet_mode(config_data.conf[instance].clock_init.cfg.clock_domain);
        } 
    }

    return rc;
}

mesa_rc vtss_ptp_force_holdover_set(int instance, BOOL enable)
{
    if (instance >= 0 && instance < PTP_CLOCK_INSTANCES) {
        if (ptp_global.ptpi[instance] != 0) {
            mesa_rc rc = ptp_global.ptpi[instance]->ssm.servo->force_holdover_set(enable);
            return rc;
        } else {
            return VTSS_RC_ERROR;
        }
    } else {
        return VTSS_RC_ERROR;
    }
}

mesa_rc vtss_ptp_force_holdover_get(int instance, BOOL *enable)
{
    if (instance >= 0 && instance < PTP_CLOCK_INSTANCES) {
        if (ptp_global.ptpi[instance] != 0) {
            mesa_rc rc = ptp_global.ptpi[instance]->ssm.servo->force_holdover_get(enable);
            return rc;
        } else {
            return VTSS_RC_ERROR;
        }
    } else {
        return VTSS_RC_ERROR;
    }
}

mesa_rc vtss_ptp_switch_to_hybrid_mode(int instance)
{
    vtss_rc rc = VTSS_RC_ERROR;

    T_IG(VTSS_TRACE_GRP_MS_SERVO, "Instance %d", instance);
    if (instance >= 0 && instance < PTP_CLOCK_INSTANCES) {
        if (ptp_global.ptpi[instance] != 0) {
#if defined(VTSS_SW_OPTION_SYNCE)
            if (config_data.conf[instance].clock_init.cfg.clock_domain == 0) {
                if (vtss_synce_ptp_clock_hybrid_mode_set(TRUE) != VTSS_RC_OK) {
                    T_W("vtss_synce_ptp_clock_hybrid_mode_set(TRUE) returned error.");
                }
            }
#endif
            rc = ptp_global.ptpi[instance]->ssm.servo->switch_to_hybrid_mode(config_data.conf[instance].clock_init.cfg.clock_domain);
        }
    }

    return rc;
}

mesa_rc vtss_ptp_get_hybrid_mode_state(int instance, bool *hybrid_mode)
{
    if (instance >= 0 && instance < PTP_CLOCK_INSTANCES) {
        if (ptp_global.ptpi[instance] != 0) {
            *hybrid_mode = ptp_global.ptpi[instance]->ssm.servo->mode_is_hybrid_mode(config_data.conf[instance].clock_init.cfg.clock_domain);
            return VTSS_RC_OK;
        } else {
            return VTSS_RC_ERROR;
        }
    } else {
        return VTSS_RC_ERROR;
    }
}

mesa_rc vtss_ptp_set_active_ref(int stream)
{
    vtss_rc rc = VTSS_RC_ERROR;

    if (stream >= 0 && stream < vtss_appl_cap_ptp_clock_cnt) {
        T_IG(VTSS_TRACE_GRP_MS_SERVO, "Instance %d, domain %d", stream, config_data.conf[stream].clock_init.cfg.clock_domain);
        if (ptp_global.ptpi[stream] != 0) {
            rc = ptp_global.ptpi[stream]->ssm.servo->set_active_ref(config_data.conf[stream].clock_init.cfg.clock_domain, stream);
        }
    }
    return rc;
}

mesa_rc vtss_ptp_set_active_electrical_ref(int input)
{
    vtss_rc rc = VTSS_RC_ERROR;

    T_IG(VTSS_TRACE_GRP_MS_SERVO, "input %d", input);
    // electrical references are only relevant for clock domain 0
    if ((rc = zl30380_apr_set_active_elec_ref(0, input)) != VTSS_RC_OK) {
        T_WG(VTSS_TRACE_GRP_MS_SERVO, "Could not set electrical reference mode");
    } else {
        T_IG(VTSS_TRACE_GRP_MS_SERVO, "APR set electrical reference mode");
    }
#if defined(VTSS_SW_OPTION_SYNCE)
    rc = vtss_synce_ptp_clock_hybrid_mode_set(FALSE);
#endif
    return rc;
}

mesa_rc vtss_ptp_set_hybrid_transient(vtss_ptp_hybrid_transient state)
{
    int instance = 0;

    static vtss_ptp_hybrid_transient old_state = VTSS_PTP_HYBRID_TRANSIENT_NOT_ACTIVE;
    T_IG(VTSS_TRACE_GRP_MS_SERVO, "old_state %d, new state %d", old_state, state);
    PTP_CORE_LOCK_SCOPE();
    if (state != old_state) {
        old_state = state;
        if (ptp_global.ptpi[0] != 0) {
            return ptp_global.ptpi[0]->ssm.servo->set_hybrid_transient(config_data.conf[instance].clock_init.cfg.clock_domain, state);
        } else {
            return VTSS_RC_ERROR;
        }
    }
    return VTSS_RC_OK;
}

bool vtss_ptp_servo_get_holdover_status(int inst)
{
    PTP_CORE_LOCK_SCOPE();
    return ptp_global.ptpi[inst]->ssm.servo->holdover_ok;
}

#if defined(VTSS_SW_OPTION_SYNCE)
void vtss_ptp_ptsf_state_set(const int instance) {
    mesa_rc rc;
    vtss_appl_synce_ptp_ptsf_state_t ptsf_state;
    T_N("vtss_ptp_ptsf_state_set %u ", instance);
    if (ptp_global.ptpi[instance] != 0) {
        if (ptp_global.ptpi[instance]->ssm.ptsf_loss_of_announce) {
            ptsf_state = SYNCE_PTSF_LOSS_OF_ANNOUNCE;
        } else if (ptp_global.ptpi[instance]->ssm.ptsf_loss_of_sync) {
            ptsf_state = SYNCE_PTSF_LOSS_OF_SYNC;
        } else if (ptp_global.ptpi[instance]->ssm.ptsf_unusable) {
            ptsf_state = SYNCE_PTSF_UNUSABLE;
        } else {
            ptsf_state = SYNCE_PTSF_NONE;
        }
        if (ptsf_state != ptp_global.ptpi[instance]->ptsf_state) {
            // only send to SyncE if changed
            T_D("instance %d vtss_ptp_ptsf_state_set %s", instance, vtss_sync_ptsf_state_2_txt(ptsf_state));
            rc = vtss_synce_ptp_clock_ptsf_state_set(instance, ptsf_state);
            ptp_global.ptpi[instance]->ptsf_state = ptsf_state;
            if (rc != VTSS_RC_OK) T_W("vtss_synce_ptp_clock_ptsf_state_set returned error code %s", error_txt(rc));
        }
    } else {
        T_W("vtss_ptp_ptsf_state_set not ready %u ", instance);
    }
    u8 clock_class;
    if (ptp_global.ptpi[instance]->slavePort != 0)
        clock_class = ptp_global.ptpi[instance]->parentDS.grandmasterClockQuality.clockClass;
    else {
        clock_class = 255;
    }
    (void) vtss_synce_ptp_clock_ssm_ql_set(ptp_global.ptpi[instance]->localClockId, clock_class);
    ptp_global.ptpi[instance]->synce_clock_class = clock_class;
    return;
}
#endif

#if defined TIMEOFDAY_TEST
static bool first_time = true;
static void tod_test(void)
{
    mesa_timestamp_t ts;
    mesa_timestamp_t prev_ts;

    mesa_timeinterval_t diff;
    u32 tc;
    u32 prev_tc = 0;
    u32 diff_tc;
    int i;
    char str1[40];
    char str2[40];

    vtss_tod_gettimeofday(0, &ts, &tc);
    T_IG(_C,"Testing TOD: Now= %s, tc = %lu", TimeStampToString (&ts, str1),tc);
    for (i = 0; i < 250; i++) {
        vtss_tod_gettimeofday(0, &ts, &tc);
        if (!first_time) {
            first_time = false;
            subTimeInterval(&diff, &ts, &prev_ts);
            if (diff < 0) {
                T_WG(_C,"Bad time reading: Now= %s, Prev = %s, diff = " VPRI64d,
                     TimeStampToString (&ts, str1),
                     TimeStampToString (&prev_ts, str2), diff);
            }
            prev_ts = ts;
            diff_tc = tc-prev_tc;
            if (tc < prev_tc) { /* time counter has wrapped */
                diff_tc += VTSS_HW_TIME_WRAP_LIMIT;
                T_WG(_C,"counter wrapped: tc = %lu,  hw_time = %lu, diff = %lu",tc,prev_tc, diff_tc);
            }
            if (diff_tc > VTSS_HW_TIME_NSEC_PR_CNT) {
                T_WG(_C,"Bad TC reading: tc = %lu,  prev_tc = %lu, diff = %lu",tc, prev_tc, diff_tc);
            }
            prev_tc = tc;
        }
    }
    vtss_tod_gettimeofday(0, &ts, &tc);
    T_IG(_C,"End Testing TOD: Now= %s, tc = %lu", TimeStampToString (&ts, str1),tc);

}

#endif

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
