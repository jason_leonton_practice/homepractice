/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#include "ptp_icli_show_functions.h"
#include "ptp.h"

#include "vtss_tod_api.h"
#include "misc_api.h"
#include "vtss_ptp_local_clock.h"

/***************************************************************************/
/*  Internal functions                                                     */
/***************************************************************************/


/*
 * Implementation of ICLI show functions
 */

static const char *cli_bool_disp(bool b)
{
    return (b ? "True" : "False");
}

static const char *cli_adj_method(vtss_appl_ptp_preferred_adj_t m)
{
    switch (m) {
        case VTSS_APPL_PTP_PREFERRED_ADJ_LTC: return "LTC";
        case VTSS_APPL_PTP_PREFERRED_ADJ_SINGLE: return "Single";
        case VTSS_APPL_PTP_PREFERRED_ADJ_INDEPENDENT: return "Independent";
        case VTSS_APPL_PTP_PREFERRED_ADJ_COMMON: return "Common";
        case VTSS_APPL_PTP_PREFERRED_ADJ_AUTO: return "Auto";
        default: return "unknown";
    }
}

static const char *cli_one_pps_mode_disp(u8 m)
{
    switch (m) {
        case VTSS_APPL_PTP_ONE_PPS_DISABLE: return "Disable";
        case VTSS_APPL_PTP_ONE_PPS_OUTPUT: return "Output";
        case VTSS_APPL_PTP_ONE_PPS_INPUT: return "Input";
        case VTSS_APPL_PTP_ONE_PPS_OUTPUT_INPUT: return "Out/Input";
        default: return "unknown";
    }
}

static const char *cli_state_disp(u8 b)
{

    switch (b) {
        case VTSS_APPL_PTP_COMM_STATE_IDLE:
            return "IDLE";
        case VTSS_APPL_PTP_COMM_STATE_INIT:
            return "INIT";
        case VTSS_APPL_PTP_COMM_STATE_CONN:
            return "CONN";
        case VTSS_APPL_PTP_COMM_STATE_SELL:
            return "SELL";
        case VTSS_APPL_PTP_COMM_STATE_SYNC:
            return "SYNC";
        default:
            return "?";
    }
}

static const char *cli_protocol_disp(u8 p)
{
    switch (p) {
        case VTSS_APPL_PTP_PROTOCOL_ETHERNET:
            return "Ethernet";
        case VTSS_APPL_PTP_PROTOCOL_ETHERNET_MIXED:
            return "EthernetMixed";
        case VTSS_APPL_PTP_PROTOCOL_IP4MULTI:
            return "IPv4Multi";
        case VTSS_APPL_PTP_PROTOCOL_IP4MIXED:
            return "IPv4Mixed";
        case VTSS_APPL_PTP_PROTOCOL_IP4UNI:
            return "IPv4Uni";
        case VTSS_APPL_PTP_PROTOCOL_OAM:
            return "Oam";
        case VTSS_APPL_PTP_PROTOCOL_ONE_PPS:
            return "1pps";
        default:
            return "?";
    }
}

static const char *cli_srv_opt_disp(vtss_appl_ptp_srv_clock_option_t p)
{
    switch (p) {
        case VTSS_APPL_PTP_CLOCK_FREE:
            return "free";
        case VTSS_APPL_PTP_CLOCK_SYNCE:
            return "synce";
        default:
            return "?";
    }
}

static const char *cli_delaymechanism_disp(uchar d)
{
    switch (d) {
        case 1:
            return "e2e";
        case 2:
            return "p2p";
        default:
            return "?\?\?";
    }
}

static const char *cli_dest_adr_type_disp(vtss_appl_ptp_dest_adr_type_t d)
{
    switch (d) {
        case VTSS_APPL_PTP_PROTOCOL_SELECT_DEFAULT:
            return "deflt";
        case VTSS_APPL_PTP_PROTOCOL_SELECT_LINK_LOCAL:
            return "liLoc";
        default:
            return "?\?\?";
    }
}

static const char *cli_two_step_option_disp(vtss_appl_ptp_twostep_override_t d)
{
    switch (d) {
        case VTSS_APPL_PTP_TWO_STEP_OVERRIDE_NONE:
            return "Clk Def.";
        case VTSS_APPL_PTP_TWO_STEP_OVERRIDE_FALSE:
            return "False";
        case VTSS_APPL_PTP_TWO_STEP_OVERRIDE_TRUE:
            return "True";
        default:
            return "?\?\?";
    }
}

static const char *cli_profile_disp(vtss_appl_ptp_profile_t d)
{
    switch (d) {
        case VTSS_APPL_PTP_PROFILE_NO_PROFILE:
            return "No profile";
        case VTSS_APPL_PTP_PROFILE_1588:
            return "ieee1588";
        case VTSS_APPL_PTP_PROFILE_G_8265_1:
            return "g8265.1";
        case VTSS_APPL_PTP_PROFILE_G_8275_1:
            return "g8275.1";
        case VTSS_APPL_PTP_PROFILE_IEEE_802_1AS:
            return "802.1as";
        default:
            return "?\?\?";
    }
}



















void ptp_cli_table_header(const char *txt, vtss_ptp_cli_pr *pr)
{
    int i, j, len, count = 0;

    pr("%s\n", txt);
    while (*txt == ' ') {
        (void)pr(" ");
        txt++;
    }
    len = strlen(txt);
    for (i = 0; i < len; i++) {
        if (txt[i] == ' ') {
            count++;
        } else {
            for (j = 0; j < count; j++) {
                (void)pr("%c", count > 1 && (j >= (count - 2)) ? ' ' : '-');
            }
            (void)pr("-");
            count = 0;
        }
    }
    for (j = 0; j < count; j++) {
        (void)pr("%c", count > 1 && (j >= (count - 2)) ? ' ' : '-');
    }
    (void)pr("\n");
}



void ptp_show_clock_foreign_master_record_ds(int inst, mesa_port_no_t uport, bool first, vtss_ptp_cli_pr *pr)
{
    ptp_foreign_ds_t bs;
    char str2 [40];
    char str3 [40];
    i16 ix;
    if(first) {
        ptp_cli_table_header("Port  ForeignmasterIdentity         ForeignmasterClockQality     Pri1  Pri2  Lpri  Qualif  Best ", pr);
    }
    for (ix = 0; ix < DEFAULT_MAX_FOREIGN_RECORDS; ix++) {
        if (ptp_get_port_foreign_ds(&bs,uport, ix, inst)) {
            (void)pr("%-4d  %-27s%-4d  %-27s%-6d%-6d%-6d%-8s%-5s\n",
                     uport,
                     ClockIdentityToString(bs.foreignmasterIdentity.clockIdentity, str2),
                     bs.foreignmasterIdentity.portNumber,
                     ClockQualityToString(&bs.foreignmasterClockQuality, str3),
                     bs.foreignmasterPriority1,
                     bs.foreignmasterPriority2,
                     bs.foreignmasterLocalPriority,
                     cli_bool_disp(bs.qualified),
                     cli_bool_disp(bs.best));
        } else {
            continue;
        }
    }

}

void ptp_show_clock_port_ds(int inst, mesa_port_no_t uport, bool first, vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_config_port_ds_t port_cfg;
    vtss_appl_ptp_status_port_ds_t port_status;
    vtss_ifindex_t ifindex;
    char str1[24];
    char str2[24];
    char str3[24];
    char str4[24];

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    if (first) {
        ptp_cli_table_header("Port  Enabled  Stat  MDR  PeerMeanPathDel  Anv  ATo  Syv  SyvErr  Dlm  MPR  DelayAsymmetry   IngressLatency   EgressLatency    Ver  Lpri  NoSlv  McAdr  Two-step", pr);
    }
    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &port_cfg) == VTSS_RC_OK) &&
        (vtss_appl_ptp_status_clocks_port_ds_get(inst, ifindex, &port_status) == VTSS_RC_OK))
    {
        (void)pr("%-6d%-9s%-6s%-5d%-17s%-5d%-5d%-5d%-8s%-5s%-5d%-17s%-17s%-17s%-5d%-6d%-7s%-7s%-8s\n",
                 port_status.portIdentity.portNumber,
                 port_cfg.enabled == 1 ? "True" : "False",
                 PortStateToString(port_status.portState),
                 port_status.logMinDelayReqInterval,
                 vtss_tod_TimeInterval_To_String(&port_status.peerMeanPathDelay, str1,','),
                 port_cfg.logAnnounceInterval,
                 port_cfg.announceReceiptTimeout, port_cfg.logSyncInterval,
                 port_status.syncIntervalError ? "Yes" : "No",
                 cli_delaymechanism_disp(port_cfg.delayMechanism),
                 port_cfg.logMinPdelayReqInterval,
                 vtss_tod_TimeInterval_To_String(&port_cfg.delayAsymmetry, str2,','),
                 vtss_tod_TimeInterval_To_String(&port_cfg.ingressLatency, str3,','),
                 vtss_tod_TimeInterval_To_String(&port_cfg.egressLatency, str4,','),
                 port_cfg.versionNumber,
                 port_cfg.localPriority,
                 port_cfg.notSlave ? "True" : "False",
                 cli_dest_adr_type_disp(port_cfg.dest_adr_type),
                 cli_two_step_option_disp(port_cfg.twoStepOverride));
    }
}

void ptp_show_clock_port_state_ds(int inst, vtss_uport_no_t uport, bool first, vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_config_port_ds_t port_cfg;
    vtss_appl_ptp_status_port_ds_t port_status;
    vtss_ptp_port_link_state_t ds;
    vtss_ifindex_t ifindex;

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    if (first) {
        ptp_cli_table_header("Port  Enabled  PTP-State  Internal  Link  Port-Timer  Vlan-forw  Phy-timestamper  Peer-delay", pr);
    }
    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &port_cfg) == VTSS_RC_OK) &&
        (vtss_appl_ptp_status_clocks_port_ds_get(inst, ifindex, &port_status) == VTSS_RC_OK) &&
        (ptp_get_port_link_state(inst, uport, &ds) == VTSS_RC_OK))
    {
        (void) pr("%4d  %-7s  %-9s  %-8s  %-4s  %-10s  %-9s  %-15s  %-10s\n",
                  port_status.portIdentity.portNumber,
                  port_cfg.enabled ? "TRUE" : "FALSE",
                  PortStateToString(port_status.portState),
                  port_cfg.portInternal ? "TRUE" : "FALSE",
                  ds.link_state ? "Up" : "Down",
                  ds.in_sync_state ? "In Sync" : "OutOfSync",
                  ds.forw_state ? "Forward" : "Discard",
                  ds.phy_timestamper ? "TRUE" : "FALSE",
                  port_status.peer_delay_ok ? "OK" : "FAIL");
    }
}

void ptp_show_clock_port_statistics(int inst, mesa_port_no_t uport, bool first, vtss_ptp_cli_pr *pr, bool clear)
{
    vtss_appl_ptp_status_port_statistics_t port_stati;
    vtss_ifindex_t ifindex;
    vtss_rc rc;
    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    ptp_cli_table_header("Port  Parameter                                 counter     ", pr);
    if (clear) {
        rc = vtss_appl_ptp_status_clocks_port_statistics_get_clear(inst, ifindex, &port_stati);
    } else {
        rc = vtss_appl_ptp_status_clocks_port_statistics_get(inst, ifindex, &port_stati);
    }
    if (rc == VTSS_RC_OK)
    {
        (void) pr("%4d  %-40s  %10d\n", uport, "rxSyncCount", port_stati.rxSyncCount);
        (void) pr("      %-40s  %10d\n", "rxFollowUpCount", port_stati.rxFollowUpCount);
        (void) pr("      %-40s  %10d\n", "rxPdelayRequestCount", port_stati.rxPdelayRequestCount);
        (void) pr("      %-40s  %10d\n", "rxPdelayResponseCount", port_stati.rxPdelayResponseCount);
        (void) pr("      %-40s  %10d\n", "rxPdelayResponseFollowUpCount", port_stati.rxPdelayResponseFollowUpCount);
        (void) pr("      %-40s  %10d\n", "rxAnnounceCount", port_stati.rxAnnounceCount);
        (void) pr("      %-40s  %10d\n", "rxPTPPacketDiscardCount", port_stati.rxPTPPacketDiscardCount);
        (void) pr("      %-40s  %10d\n", "syncReceiptTimeoutCount", port_stati.syncReceiptTimeoutCount);
        (void) pr("      %-40s  %10d\n", "announceReceiptTimeoutCount", port_stati.announceReceiptTimeoutCount);
        (void) pr("      %-40s  %10d\n", "pdelayAllowedLostResponsesExceededCount", port_stati.pdelayAllowedLostResponsesExceededCount);
        (void) pr("      %-40s  %10d\n", "txSyncCount", port_stati.txSyncCount);
        (void) pr("      %-40s  %10d\n", "txFollowUpCount", port_stati.txFollowUpCount);
        (void) pr("      %-40s  %10d\n", "txPdelayRequestCount", port_stati.txPdelayRequestCount);
        (void) pr("      %-40s  %10d\n", "txPdelayResponseCount", port_stati.txPdelayResponseCount);
        (void) pr("      %-40s  %10d\n", "txPdelayResponseFollowUpCount", port_stati.txPdelayResponseFollowUpCount);
        (void) pr("      %-40s  %10d\n", "txAnnounceCount", port_stati.txAnnounceCount);
        if (clear) {
        (void) pr("\n counters cleared\n");
        }
    }
}


































































void ptp_show_clock_virtual_port_state_ds(int inst, u32 virtual_port, bool first, vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_status_port_ds_t port_status;
    vtss_appl_ptp_virtual_port_config_t  virtual_port_cfg;

    if (first) {
        ptp_cli_table_header("VirtualPort  Enabled  PTP-State  Io-pin", pr);
    }
    if ((ptp_clock_config_virtual_port_config_get(inst, &virtual_port_cfg) == VTSS_RC_OK) &&
            (vtss_appl_ptp_status_clocks_virtual_port_ds_get(inst, virtual_port, &port_status) == VTSS_RC_OK))
    {
        (void) pr("%11d  %-7s  %-9s  %6d\n",
                  iport2uport(virtual_port),
                  virtual_port_cfg.enable ? "TRUE" : "FALSE",
                  PortStateToString(port_status.portState),
                  virtual_port_cfg.io_pin);
    }
}

void ptp_show_clock_wireless_ds(int inst, mesa_port_no_t uport, bool first, vtss_ptp_cli_pr *pr)
{
    bool mode;
    vtss_ptp_delay_cfg_t delay_cfg;
    if (first) {
        ptp_cli_table_header("Port  Wireless Mode  Base_delay(ns)  Incr_delay(ns) ", pr);
    }
    if (ptp_port_wireless_delay_mode_get(&mode, uport,inst) &&
            ptp_port_wireless_delay_get(&delay_cfg, uport, inst)) {
        (void)pr("%4u  %-13s  %10d.%03d  %10d.%03d\n",
                 uport, mode ? "Enabled" : "Disabled",
                 VTSS_INTERVAL_NS(delay_cfg.base_delay),VTSS_INTERVAL_PS(delay_cfg.base_delay),
                 VTSS_INTERVAL_NS(delay_cfg.incr_delay),VTSS_INTERVAL_PS(delay_cfg.incr_delay));
    }
}

void ptp_show_ext_clock_mode(vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_ext_clock_mode_t mode;
    /* show the local clock  */
    (void) vtss_appl_ptp_ext_clock_out_get(&mode);
    (void)pr("PTP External One PPS mode: %s, Clock output enabled: %s, frequency : %d, Preferred adj method: %s\n",
             cli_one_pps_mode_disp(mode.one_pps_mode), cli_bool_disp(mode.clock_out_enable), mode.freq,
             cli_adj_method(mode.adj_method));
}

static const char *clock_adjustment_method_txt(int m)
{
    switch (m) {
        case 0:
            return "Internal Timer";
        case 1:
            return "PTP DPLL";
        case 2:
            return "DAC option";
        case 3:
            return "Software";
        case 4:
            return "Synce DPLL";
        default:
            return "Unknown";
    }
}
void ptp_show_local_clock(int inst, vtss_ptp_cli_pr *pr)
{
    u32 hw_time;
    mesa_timestamp_t t;
    char str [14];
    /* show the local clock  */
    vtss_local_clock_time_get(&t,inst, &hw_time);
    (void)pr("PTP Time (%d)    : %s %s\n", inst, misc_time2str(t.seconds), vtss_tod_ns2str(t.nanoseconds, str,','));
    (void)pr("Clock Adjustment method: %s\n", clock_adjustment_method_txt(vtss_ptp_adjustment_method(inst)));
}

void ptp_show_clock_default_ds(int inst, vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_clock_status_default_ds_t status;
    vtss_appl_ptp_clock_config_default_ds_t cfg;
    char str1 [40];
    char str2 [40];

    ptp_cli_table_header("ClockId  HW-Domain  DeviceType  Profile     2StepFlag  Ports  vtss_appl_clock_identity", pr);

    if ((vtss_appl_ptp_clock_config_default_ds_get(inst, &cfg) == VTSS_RC_OK) &&
        (vtss_appl_ptp_clock_status_default_ds_get(inst, &status) == VTSS_RC_OK))
    {
        if (cfg.deviceType == VTSS_APPL_PTP_DEVICE_NONE) {
            (void)pr("%-9d%-11d%-12s%-12s\n",
                     inst,
                     cfg.clock_domain,
                     DeviceTypeToString(cfg.deviceType),
                     cli_profile_disp(cfg.profile));
        } else {
            (void)pr("%-9d%-11d%-12s%-12s%-11s%-7d%-24s\n",
                     inst,
                     cfg.clock_domain,
                     DeviceTypeToString(cfg.deviceType),
                     cli_profile_disp(cfg.profile),
                     cli_bool_disp(cfg.twoStepFlag),
                     status.numberPorts,
                     ClockIdentityToString(status.clockIdentity, str1));
            (void)pr("\n");
            ptp_cli_table_header("Dom  vtss_appl_clock_quality         Pri1  Pri2  Lpri", pr);
            (void)pr("%-5d%-32s%-6d%-6d%-4d\n",
                     cfg.domainNumber, ClockQualityToString(&status.clockQuality, str2), cfg.priority1, cfg.priority2, cfg.localPriority);
            (void)pr("\n");
            ptp_cli_table_header("Protocol       One-Way    VID    PCP  DSCP  PathTraceEnable", pr);
            (void)pr("%-15s%-11s%-7d%-5d%-6d%-17s\n",
                     cli_protocol_disp(cfg.protocol), cli_bool_disp(cfg.oneWay),
                     cfg.configured_vid, cfg.configured_pcp, cfg.dscp,
                     cli_bool_disp(cfg.path_trace_enable));
#if defined(VTSS_SW_OPTION_MEP)
            if (MESA_CAP(MESA_CAP_TS_MISSING_PTP_ON_INTERNAL_PORTS)) {
                (void)pr("\n");
                ptp_cli_table_header("Mep Id    ", pr);
                (void)pr("%-10d\n", cfg.mep_instance);
            }
#endif







        }
    } else {
        (void)pr("%-11d%-8s%-12s\n", inst, "", DeviceTypeToString(VTSS_APPL_PTP_DEVICE_NONE));
    }
}


void ptp_show_clock_current_ds(int inst, vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_clock_current_ds_t bs;
    vtss_appl_ptp_clock_config_default_ds_t cfg;
    char str1[60];
    char str2[40];

    ptp_cli_table_header("stpRm  OffsetFromMaster    MeanPathDelay       ", pr);
    if ((vtss_appl_ptp_clock_config_default_ds_get(inst, &cfg) == VTSS_RC_OK) &&
        (vtss_appl_ptp_clock_status_current_ds_get(inst, &bs) == VTSS_RC_OK)) {
        (void)pr("%-7d%-20s%s\n",
                 bs.stepsRemoved, vtss_tod_TimeInterval_To_String(&bs.offsetFromMaster, str1,','),
                 vtss_tod_TimeInterval_To_String(&bs.meanPathDelay, str2,','));















    }
}

void ptp_show_clock_parent_ds(int inst, vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_clock_parent_ds_t bs;
    vtss_appl_ptp_clock_config_default_ds_t cfg;
    char str1 [40];
    char str2 [40];
    char str3 [40];
    ptp_cli_table_header("ParentPortIdentity      port  Pstat  Var  ChangeRate  ", pr);
    if ((vtss_appl_ptp_clock_config_default_ds_get(inst, &cfg) == VTSS_RC_OK) &&
        (vtss_appl_ptp_clock_status_parent_ds_get(inst, &bs) == VTSS_RC_OK)) {
        (void)pr("%-24s%-6d%-7s%-5d%-12d\n",
                 ClockIdentityToString(bs.parentPortIdentity.clockIdentity, str1), bs.parentPortIdentity.portNumber,
                 cli_bool_disp(bs.parentStats),
                 bs.observedParentOffsetScaledLogVariance,
                 bs.observedParentClockPhaseChangeRate);
        (void)pr("\n");
        ptp_cli_table_header("GrandmasterIdentity      GrandmasterClockQuality    Pri1  Pri2", pr);
        (void)pr("%-25s%-27s%-6d%-6d\n",
                 ClockIdentityToString(bs.grandmasterIdentity, str2),
                 ClockQualityToString(&bs.grandmasterClockQuality, str3),
                 bs.grandmasterPriority1,
                 bs.grandmasterPriority2);








    }
}

void ptp_show_clock_time_property_ds(int inst, vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_clock_timeproperties_ds_t bs;

    ptp_cli_table_header("UtcOffset  Valid  leap59  leap61  TimeTrac  FreqTrac  ptpTimeScale  TimeSource", pr);
    if (vtss_appl_ptp_clock_status_timeproperties_ds_get(inst, &bs) == VTSS_RC_OK) {
        (void)pr("%-11d%-7s%-8s%-8s%-10s%-10s%-14s%-10d\n",
                 bs.currentUtcOffset,
                 cli_bool_disp(bs.currentUtcOffsetValid),
                 cli_bool_disp(bs.leap59),
                 cli_bool_disp(bs.leap61),
                 cli_bool_disp(bs.timeTraceable),
                 cli_bool_disp(bs.frequencyTraceable),
                 cli_bool_disp(bs.ptpTimescale),
                 bs.timeSource);
    }
}

void ptp_show_clock_filter_ds(int inst, vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_clock_filter_config_t filter_params;

    /* show the filter parameters  */
    ptp_cli_table_header("DelayFilter  Period  Dist", pr);
    if (vtss_appl_ptp_clock_filter_parameters_get(inst, &filter_params) == VTSS_RC_OK) {
        (void)pr("%-13d%-8d%-4d\n",
                 filter_params.delay_filter,
                 filter_params.period,
                 filter_params.dist
                );
    }
}

void ptp_show_clock_servo_ds(int inst, vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_clock_servo_config_t default_servo;
    /* show the servo parameters  */
    ptp_cli_table_header("Display  P-enable  I-enable  D-enable  'P'constant  'I'constant  'D'constant  gain const ",pr);
    if (vtss_appl_ptp_clock_servo_parameters_get(inst, &default_servo) == VTSS_RC_OK) {
        (void)pr("%-9s%-10s%-10s%-10s%-13d%-13d%-13d%-13d\n",
                 cli_bool_disp(default_servo.display_stats),
                 cli_bool_disp(default_servo.p_reg),
                 cli_bool_disp(default_servo.i_reg),
                 cli_bool_disp(default_servo.d_reg),
                 default_servo.ap,
                 default_servo.ai,
                 default_servo.ad,
                 default_servo.gain);
    }
}

void ptp_show_clock_clk_ds(int inst, vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_clock_servo_config_t default_servo;
    /* show the servo parameters  */
    ptp_cli_table_header("Option  threshold  'P'constant", pr);
    if (vtss_appl_ptp_clock_servo_parameters_get(inst, &default_servo) == VTSS_RC_OK) {
        (void)pr("%-9s%-11d%-13d\n",
                cli_srv_opt_disp(default_servo.srv_option),
                default_servo.synce_threshold,
                default_servo.synce_ap);
    }
}

void ptp_show_clock_ho_ds(int inst, vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_clock_servo_config_t default_servo;
    vtss_ptp_servo_status_t status;
    /* show the holdover parameters  */
    ptp_cli_table_header("Holdover filter  Adj threshold (ppb)", pr);
    if (vtss_appl_ptp_clock_servo_parameters_get(inst, &default_servo) == VTSS_RC_OK) {
        (void)pr("%15d  %17d.%d\n",
                 default_servo.ho_filter,
                 (i32)default_servo.stable_adj_threshold/10,(i32)default_servo.stable_adj_threshold%10);
    }

    /* show the holdover status  */
    if (vtss_appl_ptp_clock_servo_status_get(inst, &status) == VTSS_RC_OK) {
        ptp_cli_table_header("Holdover Ok  Holdover offset (ppb)", pr);
        (void)pr("%-11s  %19d.%ld\n",
                 status.holdover_ok ? "TRUE" : "FALSE",
                 (i32)status.holdover_adj / 10, labs((i32)status.holdover_adj % 10));
    }
}

void ptp_show_clock_uni_ds(int inst, vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_unicast_slave_config_t uni_slave_cfg;
    vtss_appl_ptp_unicast_slave_table_t uni_slave_status;
    char buf1[20];
    uint ix;

    /* show the unicast parameters  */
    ptp_cli_table_header("index  duration  ip_address       grant  CommState", pr);
    ix = 0;
    while ((vtss_appl_ptp_clock_config_unicast_slave_config_get(inst, ix, &uni_slave_cfg) == VTSS_RC_OK) &&
           (vtss_appl_ptp_clock_status_unicast_slave_table_get(inst, ix++, &uni_slave_status) == VTSS_RC_OK))
    {
        (void)pr("%-7d%-10d%-17s%-7d%-9s\n",
                 ix-1,
                 uni_slave_cfg.duration,
                 misc_ipv4_txt(uni_slave_cfg.ip_addr, buf1),
                 uni_slave_status.log_msg_period,
                 cli_state_disp(uni_slave_status.comm_state));
    }
}

void ptp_show_clock_master_table_unicast_ds(uint inst, vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_unicast_master_table_t uni_master_table;
    uint clock_next, slave_next;
    mesa_rc rc;
    char str1[20];
    char str2[30];    

    ptp_cli_table_header("ip_addr          mac_addr           port  Ann  Sync ", pr);
    rc = vtss_appl_ptp_clock_slave_itr(&inst, &clock_next, NULL, &slave_next);
    while ((rc == VTSS_RC_OK) && (clock_next == inst)) {    
        rc = vtss_appl_ptp_clock_status_unicast_master_table_get(inst, slave_next, &uni_master_table);
        if (rc == VTSS_RC_OK) {         
            if (uni_master_table.ann) {
                if (uni_master_table.sync) {
                    (void)pr("%-17s%-19s%-6d%-4d %-4d\n",
                             misc_ipv4_txt(slave_next, str1),
                             misc_mac_txt((const uchar *) uni_master_table.mac.addr, str2),
                             uni_master_table.port,
                             uni_master_table.ann_log_msg_period,
                             uni_master_table.log_msg_period);
                } else {
                    (void)pr("%-17s%-19s%-6d%-4d %-4s\n",
                             misc_ipv4_txt(slave_next, str1),
                             misc_mac_txt((const uchar *)uni_master_table.mac.addr, str2),
                             uni_master_table.port,
                             uni_master_table.ann_log_msg_period,
                             "N.A.");
                }
            }
            rc = vtss_appl_ptp_clock_slave_itr(&inst, &clock_next, &slave_next, &slave_next);            
        }
    }
}

void ptp_show_clock_slave_ds(int inst, vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_clock_slave_ds_t ss;
    char str1 [40];

    ptp_cli_table_header("Slave port  Slave state    Holdover(ppb)  ", pr);
    if (vtss_appl_ptp_clock_status_slave_ds_get(inst, &ss) == VTSS_RC_OK) {
        (void)pr("%-10d  %-13s  %-13s\n",
                 ss.port_number,
                 vtss_ptp_slave_state_2_text(ss.slave_state),
                 vtss_ptp_ho_state_2_text(ss.holdover_stable, ss.holdover_adj, str1));
    }
}

static char * log_mode_2_text(int log_mode) {
    switch (log_mode) {
        case 1:  return "log offset";
        case 2:  return "log forward";
        case 3:  return "log reverse";
        case 4:  return "log fwd and rev";
        default: return "log not active";
    }
}

void ptp_show_log_mode(int inst, vtss_ptp_cli_pr *pr)
{
    vtss_ptp_logmode_t log_mode;

    ptp_cli_table_header("Debug mode       Active  KeepControl  Log time (sec)  Time left (sec)  ", pr);
    if (ptp_debug_mode_get(inst, &log_mode)) {
        if (log_mode.debug_mode) {
            (void)pr("%-15s  %-6s  %-11s  %14d  %15d\n",
                     log_mode_2_text(log_mode.debug_mode),
                     log_mode.file_open ? "Yes" : "No",
                     log_mode.keep_control ? "Yes" : "No",
                     log_mode.log_time,
                     log_mode.time_left);
        } else {
            (void)pr("%-15s  \n",
                     log_mode_2_text(log_mode.debug_mode));
        }
    } else {
        (void)pr("Could not get log mode\n");
    }
}

void ptp_show_slave_cfg(int inst, vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_clock_slave_config_t slave_cfg;

    ptp_cli_table_header("Stable Offset  Offset Ok      Offset Fail", pr);
    if (vtss_appl_ptp_clock_slave_config_get(inst, &slave_cfg) == VTSS_RC_OK) {
        (void)pr("%-13u  %-13u  %-13u\n", slave_cfg.stable_offset, slave_cfg.offset_ok, slave_cfg.offset_fail);
    }
}

void ptp_show_clock_slave_table_unicast_ds(int inst, vtss_ptp_cli_pr *pr)
{
    vtss_appl_ptp_unicast_slave_table_t uni_slave_table;
    char str1[20];
    char str2[30];
    char str3[30];
    int ix = 0;

    ptp_cli_table_header("Index  IP-addr      State  MAC-addr           Port  Srcport clock id         Srcport port  Grant ", pr);
    while (vtss_appl_ptp_clock_status_unicast_slave_table_get(inst, ix, &uni_slave_table) == VTSS_RC_OK) {
        if (uni_slave_table.conf_master_ip) {
            if (uni_slave_table.comm_state >= VTSS_APPL_PTP_COMM_STATE_CONN) {
                (void)pr("%-7d%-13s%-7s%-19s%-6d%-25s%-14d%-7d\n",
                         ix,
                         misc_ipv4_txt(uni_slave_table.conf_master_ip, str1),
                         cli_state_disp(uni_slave_table.comm_state),
                         misc_mac_txt((const uchar *) uni_slave_table.master.mac.addr, str2),
                         uni_slave_table.port,
                         ClockIdentityToString(uni_slave_table.sourcePortIdentity.clockIdentity, str3),
                         uni_slave_table.sourcePortIdentity.portNumber,
                         uni_slave_table.log_msg_period);
            } else {
                (void)pr("%-7d%-13s%-7s\n",
                         ix,
                         misc_ipv4_txt(uni_slave_table.conf_master_ip, str1),
                         cli_state_disp(uni_slave_table.comm_state));
            }
        }
        ix++;
    }
}

static const char *cli_rs422_mode_disp(ptp_rs422_mode_t m)
{
    switch (m) {
        case VTSS_PTP_RS422_DISABLE: return "Disable";
        case VTSS_PTP_RS422_MAIN_AUTO: return "Main_Auto";
        case VTSS_PTP_RS422_SUB: return "sub";
        case VTSS_PTP_RS422_MAIN_MAN: return "Main_Man";
        case VTSS_PTP_RS422_CALIB: return "Calib";
        default: return "unknown";
    }
}

static const char *cli_rs422_proto_disp(ptp_rs422_protocol_t m)
{
    switch (m) {
        case VTSS_PTP_RS422_PROTOCOL_SER_POLYT: return "Serial (Polyt)";
        case VTSS_PTP_RS422_PROTOCOL_SER_ZDA  : return "Serial (ZDA)";
        case VTSS_PTP_RS422_PROTOCOL_SER_RMC  : return "Serial (RMC)";
        case VTSS_PTP_RS422_PROTOCOL_PIM      : return "PIM";
        default: return "unknown";
    }
}

void ptp_show_rs422_clock_mode(vtss_ptp_cli_pr *pr)
{
    vtss_ptp_rs422_conf_t mode;
    vtss_ext_clock_rs422_conf_get(&mode);
    (void)pr("PTP RS422 clock mode: %s, delay : %d, Protocol: %s, port: %d\n",
             cli_rs422_mode_disp(mode.mode), mode.delay,
             cli_rs422_proto_disp(mode.proto), iport2uport(mode.port));
}


/****************************************************************************/
/*  End of file.                                                            */
/****************************************************************************/
