/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "icli_api.h"
#include "icli_porting_util.h"
#include "ptp.h" // For Trace
#include "ptp_api.h"
#include "ptp_icli_functions.h"
#include "ptp_icli_show_functions.h"
#include "ptp_1pps_sync.h"
#include "ptp_1pps_closed_loop.h"
#include "ptp_1pps_serial.h"

#if defined(VTSS_SW_OPTION_ZLS30387)
#include "zl_3038x_api_pdv_api.h"
#endif

#include "tod_api.h"
#include "vtss_tod_api.h"
#include "misc_api.h"
#include "vtss_ptp_local_clock.h"
#include "ptp_pim_api.h"
#include "vlan_api.h"

/***************************************************************************/
/*  Internal types                                                         */
/***************************************************************************/

/***************************************************************************/
/*  Internal functions                                                     */
/***************************************************************************/


static icli_rc_t ptp_icli_traverse_ports(i32 session_id, int clockinst,
                                    icli_stack_port_range_t *port_type_list_p,
                                    icli_rc_t (*show_function)(i32 session_id, int inst, mesa_port_no_t uport, BOOL first, vtss_ptp_cli_pr *pr))
{
    icli_rc_t rc =  ICLI_RC_OK;
    u32             range_idx, cnt_idx;
    vtss_isid_t     isid;
    mesa_port_no_t  uport;
    switch_iter_t   sit;
    port_iter_t     pit;
    BOOL            first = true;

    if (port_type_list_p) { //at least one range input
        for (range_idx = 0; range_idx < port_type_list_p->cnt; range_idx++) {
            isid = port_type_list_p->switch_range[range_idx].isid;
            for (cnt_idx = 0; cnt_idx < port_type_list_p->switch_range[range_idx].port_cnt; cnt_idx++) {
                uport = port_type_list_p->switch_range[range_idx].begin_uport + cnt_idx;

                //ignore stacking port
                if (port_isid_port_no_is_stack(isid, uport2iport(uport))) {
                    continue;
                }
                if (ICLI_RC_OK != show_function(session_id, clockinst, uport, first, icli_session_self_printf)) {
                    rc = ICLI_RC_ERROR;
                }
                first = false;
            }
        }
    } else { //show all port configuraton
        (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_USID_CFG);
        while (switch_iter_getnext(&sit)) {
            (void) port_iter_init(&pit, NULL, sit.isid, PORT_ITER_SORT_ORDER_UPORT, PORT_ITER_FLAGS_NORMAL);
            while (port_iter_getnext(&pit)) {
                if (ICLI_RC_OK != show_function(session_id, clockinst, pit.uport, first, icli_session_self_printf)) {
                    rc = ICLI_RC_ERROR;
                }
                first = false;
            }
        }
    }
    return rc;
}


static icli_rc_t ptp_icli_config_traverse_ports(i32 session_id, int clockinst,
                                           icli_stack_port_range_t *port_type_list_p, void *port_cfg,
                                           icli_rc_t (*cfg_function)(i32 session_id, int inst, mesa_port_no_t uport, void *cfg))
{
    icli_rc_t rc =  ICLI_RC_OK;
    u32             range_idx, cnt_idx;
    vtss_isid_t     isid;
    mesa_port_no_t  uport;
    switch_iter_t   sit;
    port_iter_t     pit;

    if (port_type_list_p) { //at least one range input
        for (range_idx = 0; range_idx < port_type_list_p->cnt; range_idx++) {
            isid = port_type_list_p->switch_range[range_idx].isid;
            for (cnt_idx = 0; cnt_idx < port_type_list_p->switch_range[range_idx].port_cnt; cnt_idx++) {
                uport = port_type_list_p->switch_range[range_idx].begin_uport + cnt_idx;

                //ignore stacking port
                if (port_isid_port_no_is_stack(isid, uport2iport(uport))) {
                    continue;
                }
                if (ICLI_RC_OK != cfg_function(session_id, clockinst, uport, port_cfg)) {
                    rc = ICLI_RC_ERROR;
                }
            }
        }
    } else { //show all port configuration
        (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_USID_CFG);
        while (switch_iter_getnext(&sit)) {
            (void) port_iter_init(&pit, NULL, sit.isid, PORT_ITER_SORT_ORDER_UPORT, PORT_ITER_FLAGS_NORMAL);
            while (port_iter_getnext(&pit)) {
                if (ICLI_RC_OK != cfg_function(session_id, clockinst, pit.uport, port_cfg)) {
                    rc = ICLI_RC_ERROR;
                }
            }
        }
    }
    return rc;
}

static icli_rc_t icli_show_clock_port_state_ds(i32 session_id, int inst, mesa_port_no_t uport, BOOL first, vtss_ptp_cli_pr *pr)
{
    ptp_show_clock_port_state_ds(inst, uport, first, pr);
    return ICLI_RC_OK;
}

static icli_rc_t icli_show_clock_port_statistics(i32 session_id, int inst, mesa_port_no_t uport, BOOL first, vtss_ptp_cli_pr *pr)
{
    ptp_show_clock_port_statistics(inst, uport, first, pr, false);
    return ICLI_RC_OK;
}

static icli_rc_t icli_show_clock_port_ds(i32 session_id, int inst, mesa_port_no_t uport, BOOL first, vtss_ptp_cli_pr *pr)
{
    ptp_show_clock_port_ds(inst, uport, first, pr);
    return ICLI_RC_OK;
}















static icli_rc_t icli_show_clock_wireless_ds(i32 session_id, int inst, mesa_port_no_t uport, BOOL first, vtss_ptp_cli_pr *pr)
{
    ptp_show_clock_wireless_ds(inst, uport, first, pr);
    return ICLI_RC_OK;
}

static icli_rc_t icli_show_clock_foreign_master_record_ds(i32 session_id, int inst, mesa_port_no_t uport, BOOL first, vtss_ptp_cli_pr *pr)
{
    ptp_show_clock_foreign_master_record_ds(inst, uport, first, pr);
    return ICLI_RC_OK;
}

static icli_rc_t icli_wireless_mode_enable(i32 session_id, int inst, mesa_port_no_t uport, BOOL first, vtss_ptp_cli_pr *pr)
{
    icli_rc_t rc =  ICLI_RC_OK;
    if (!ptp_port_wireless_delay_mode_set(true, uport, inst)) {
        ICLI_PRINTF("Wireless mode not available for ptp instance %d, port %u\n", inst, uport);
        ICLI_PRINTF("Wireless mode requires a two-step or Oam based BC\n");
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

static icli_rc_t icli_wireless_mode_disable(i32 session_id, int inst, mesa_port_no_t uport, BOOL first, vtss_ptp_cli_pr *pr)
{
    icli_rc_t rc =  ICLI_RC_OK;
    if (!ptp_port_wireless_delay_mode_set(false, uport, inst)) {
        ICLI_PRINTF("Wireless mode not available for ptp instance %d, port %u\n", inst, uport);
        ICLI_PRINTF("Wireless mode requires a two-step or Oam based BC\n");
    }
    return rc;
}

static icli_rc_t icli_wireless_pre_not(i32 session_id, int inst, mesa_port_no_t uport, BOOL first, vtss_ptp_cli_pr *pr)
{
    icli_rc_t rc =  ICLI_RC_OK;
    if (!ptp_port_wireless_delay_pre_notif(uport, inst)) {
        ICLI_PRINTF("Wireless mode not available for ptp instance %d, port %u\n", inst, uport);
        ICLI_PRINTF("Wireless mode requires a two-step or Oam based BC\n");
    }
    return rc;
}


/***************************************************************************/
/*  Functions called by iCLI                                               */
/***************************************************************************/

//  see ptp_icli_functions.h
icli_rc_t ptp_icli_show(i32 session_id, int clockinst, BOOL has_default, BOOL has_current, BOOL has_parent, BOOL has_time_property,
                   BOOL has_filter, BOOL has_servo, BOOL has_clk, BOOL has_ho, BOOL has_uni,
                   BOOL has_master_table_unicast, BOOL has_slave, BOOL has_port_state, BOOL has_port_statistics, BOOL has_port_ds, BOOL has_wireless,
                   BOOL has_foreign_master_record, BOOL has_interface, icli_stack_port_range_t *port_type_list_p, BOOL has_log_mode)
{
    icli_rc_t rc =  ICLI_RC_OK;
    if (has_default) {
        ptp_show_clock_default_ds(clockinst, icli_session_self_printf);
    }
    if (has_current) {
        ptp_show_clock_current_ds(clockinst, icli_session_self_printf);
    }
    if (has_parent) {
        ptp_show_clock_parent_ds(clockinst, icli_session_self_printf);
    }
    if (has_time_property) {
        ptp_show_clock_time_property_ds(clockinst, icli_session_self_printf);
    }
    if (has_filter) {
        ptp_show_clock_filter_ds(clockinst, icli_session_self_printf);
    }
    if (has_servo) {
        ptp_show_clock_servo_ds(clockinst, icli_session_self_printf);
    }
    if (has_clk) {
        ptp_show_clock_clk_ds(clockinst, icli_session_self_printf);
    }
    if (has_ho) {
        ptp_show_clock_ho_ds(clockinst, icli_session_self_printf);
    }
    if (has_uni) {
        ptp_show_clock_uni_ds(clockinst, icli_session_self_printf);
    }
    if (has_master_table_unicast) {
        ptp_show_clock_master_table_unicast_ds(clockinst, icli_session_self_printf);
    }
    if (has_slave) {
        ptp_show_clock_slave_ds(clockinst, icli_session_self_printf);
    }
    if (has_log_mode) {
        ptp_show_log_mode(clockinst, icli_session_self_printf);
    }
    if (has_port_state) {
        rc = ptp_icli_traverse_ports(session_id, clockinst, port_type_list_p, icli_show_clock_port_state_ds);
        ptp_show_clock_virtual_port_state_ds(clockinst, MESA_CAP(MESA_CAP_PORT_CNT), true, icli_session_self_printf);



    }
    if (has_port_statistics) {
        rc = ptp_icli_traverse_ports(session_id, clockinst, port_type_list_p, icli_show_clock_port_statistics);
    }
    if (has_port_ds) {
        rc = ptp_icli_traverse_ports(session_id, clockinst, port_type_list_p, icli_show_clock_port_ds);



    }
    if (has_wireless) {
        rc = ptp_icli_traverse_ports(session_id, clockinst, port_type_list_p, icli_show_clock_wireless_ds);
    }
    if (has_foreign_master_record) {
        rc = ptp_icli_traverse_ports(session_id, clockinst, port_type_list_p, icli_show_clock_foreign_master_record_ds);
    }
    return rc;
}

icli_rc_t ptp_icli_ext_clock_mode_show(i32 session_id)
{
    ptp_show_ext_clock_mode(icli_session_self_printf);
    return ICLI_RC_OK;
}

icli_rc_t ptp_icli_rs422_clock_mode_show(i32 session_id)
{
    ptp_show_rs422_clock_mode(icli_session_self_printf);
    return ICLI_RC_OK;
}

const char *ptp_1pps_baudrate_to_str(vtss_serial_baud_rate_t b)
{
    switch(b) {
        case VTSS_SERIAL_BAUD_50:     return     "50"; break;
        case VTSS_SERIAL_BAUD_75:     return     "75"; break;
        case VTSS_SERIAL_BAUD_110:    return    "110"; break;
        case VTSS_SERIAL_BAUD_134_5:  return  "134.5"; break;
        case VTSS_SERIAL_BAUD_150:    return    "150"; break;
        case VTSS_SERIAL_BAUD_200:    return    "200"; break;
        case VTSS_SERIAL_BAUD_300:    return    "300"; break;
        case VTSS_SERIAL_BAUD_600:    return    "600"; break;
        case VTSS_SERIAL_BAUD_1200:   return   "1200"; break;
        case VTSS_SERIAL_BAUD_1800:   return   "1800"; break;
        case VTSS_SERIAL_BAUD_2400:   return   "2400"; break;
        case VTSS_SERIAL_BAUD_3600:   return   "3600"; break;
        case VTSS_SERIAL_BAUD_4800:   return   "4800"; break;
        case VTSS_SERIAL_BAUD_7200:   return   "7200"; break;
        case VTSS_SERIAL_BAUD_9600:   return   "9600"; break;
        case VTSS_SERIAL_BAUD_14400:  return  "14400"; break;
        case VTSS_SERIAL_BAUD_19200:  return  "19200"; break;
        case VTSS_SERIAL_BAUD_38400:  return  "38400"; break;
        case VTSS_SERIAL_BAUD_57600:  return  "57600"; break;
        case VTSS_SERIAL_BAUD_115200: return "115200"; break;
        case VTSS_SERIAL_BAUD_230400: return "230400"; break;
        case VTSS_SERIAL_BAUD_460800: return "460800"; break;
        case VTSS_SERIAL_BAUD_921600: return "921600"; break;
        default: return "???";
    }
}

const char *ptp_1pps_stop_bits_to_str(vtss_serial_stop_bits_t s)
{
    switch(s) {
        case VTSS_SERIAL_STOP_1:   return "1";   break;
        case VTSS_SERIAL_STOP_2:   return "2";   break;
        default: return "???";
    }
}

const char *ptp_1pps_parity_to_str(vtss_serial_parity_t p)
{
    switch(p) {
        case VTSS_SERIAL_PARITY_NONE:  return "none";  break;
        case VTSS_SERIAL_PARITY_EVEN:  return "even";  break;
        case VTSS_SERIAL_PARITY_ODD:   return "odd";   break;
        case VTSS_SERIAL_PARITY_MARK:  return "mark";  break;
        case VTSS_SERIAL_PARITY_SPACE: return "space"; break;
        default: return "???";
    }
}

const char *ptp_1pps_wordlength_to_str(vtss_serial_word_length_t w)
{
    switch(w) {
        case VTSS_SERIAL_WORD_LENGTH_5: return "5"; break;
        case VTSS_SERIAL_WORD_LENGTH_6: return "6"; break;
        case VTSS_SERIAL_WORD_LENGTH_7: return "7"; break;
        case VTSS_SERIAL_WORD_LENGTH_8: return "8"; break;
        default: return "???";
    }
}

static const char *filter_type_to_str(u32 p)
{
    switch (p) {
#if defined(VTSS_SW_OPTION_ZLS30387)
        case PTP_FILTERTYPE_ACI_DEFAULT:
            return "aci-default";
        case PTP_FILTERTYPE_ACI_FREQ_XO:
            return "aci-freq-xo";
        case PTP_FILTERTYPE_ACI_PHASE_XO:
            return "aci-phase-xo";
        case PTP_FILTERTYPE_ACI_FREQ_TCXO:
            return "aci-freq-tcxo";
        case PTP_FILTERTYPE_ACI_PHASE_TCXO:
            return "aci-phase-tcxo";
        case PTP_FILTERTYPE_ACI_FREQ_OCXO_S3E:
            return "aci-freq-ocxo-s3e";
        case PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E:
            return "aci-phase-ocxo-s3e";
        case PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_FREQ:
            return "aci-bc-partial-on-path-freq";
        case PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_PHASE:
            return "aci-bc-partial-on-path-phase";
        case PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_FREQ:
            return "aci-bc-full-on-path-freq";
        case PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE:
            return "aci-bc-full-on-path-phase";
        case PTP_FILTERTYPE_ACI_FREQ_ACCURACY_FDD:
            return "aci-freq-accuracy-fdd";
        case PTP_FILTERTYPE_ACI_FREQ_ACCURACY_XDSL:
            return "aci-freq-accuracy-xdsl";
        case PTP_FILTERTYPE_ACI_ELEC_FREQ:
            return "aci-elec-freq";
        case PTP_FILTERTYPE_ACI_ELEC_PHASE:
            return "aci-elec-phase";
        case PTP_FILTERTYPE_ACI_PHASE_TCXO_Q:
            return "aci-phase-tcxo-q";
        case PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_Q:
            return "aci-bc-full-on-path-phase-q";
        case PTP_FILTERTYPE_ACI_PHASE_RELAXED_C60W:
            return "aci-phase-relaxed-c60w";
        case PTP_FILTERTYPE_ACI_PHASE_RELAXED_C150:
            return "aci-phase-relaxed-c150";
        case PTP_FILTERTYPE_ACI_PHASE_RELAXED_C180:
             return "aci-phase-relaxed-c180";
        case PTP_FILTERTYPE_ACI_PHASE_RELAXED_C240:
             return "aci-phase-relaxed-c240";
        case PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_BETA:
             return "aci-bc-full-on-path-phase-beta";
        case PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E_R4_6_1:
             return "aci-phase-ocxo-s3e-r4-6-1";
        case PTP_FILTERTYPE_ACI_BASIC_PHASE:
             return "aci-basic-phase";
        case PTP_FILTERTYPE_ACI_BASIC_PHASE_LOW:
             return "aci-basic-phase-low";
#endif
        default: return "?";
    }
}

icli_rc_t ptp_icli_rs422_clock_mode_show_baudrate(i32 session_id)
{
    static vtss_serial_info_t serial_info;

    if (ptp_1pps_get_baudrate(&serial_info) != VTSS_RC_OK) {
        return ICLI_RC_ERROR;
    }
    
    ICLI_PRINTF("Parameters of RS422 port are: baudrate = %s, parity = %s, wordlength = %s, stopbits = %s, flags = %08x\n",
                ptp_1pps_baudrate_to_str(serial_info.baud),
                ptp_1pps_parity_to_str(serial_info.parity),
                ptp_1pps_wordlength_to_str(serial_info.word_length),
                ptp_1pps_stop_bits_to_str(serial_info.stop),
                serial_info.flags);

    return ICLI_RC_OK;
}

icli_rc_t ptp_icli_cal_t_plane(i32 session_id, u32 iport, BOOL has_ext, BOOL has_int)
{
    icli_rc_t rc = ICLI_RC_OK;
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    vtss_appl_ptp_clock_config_default_ds_t init_default_ds;
    vtss_appl_ptp_clock_status_default_ds_t init_default_ds_status;

    ICLI_PRINTF("Starting calibration of t-plane on port: %d with loopback type: %s\n", iport2uport(iport), has_int ? "Internal" : "External");

    // Remove any PTP instances already present.
    ICLI_PRINTF("Deleting any existing PTP instances\n");
    for (int clockinst = 0; clockinst < PTP_CLOCK_INSTANCES; clockinst++) {
        if (vtss_appl_ptp_clock_config_default_ds_get(clockinst, &default_ds_cfg) == VTSS_RC_OK) {
            if (vtss_appl_ptp_clock_config_default_ds_del(clockinst) != VTSS_RC_OK) {
                ICLI_PRINTF("Cannot delete clock instance %d (may be that another clock instance depends on it)\n", clockinst);
                ICLI_PRINTF("Please try to delete all PTP instances before attempting a calibration.\n");
                rc = ICLI_RC_ERROR;
            }
        }
    }

    // Reset VLAN configuration so the switch in general is using VLAN 1 for all ports and all ports are configured as
    // access ports. VLAN 2 shall be used for port under calibration.
    if (rc == ICLI_RC_OK) {
        u8 access_vids[VTSS_APPL_VLAN_BITMASK_LEN_BYTES];

        ICLI_PRINTF("Resetting VLAN configuration to default\n");
        vtss_init_data_t init_data{INIT_CMD_CONF_DEF, VTSS_ISID_GLOBAL, 0, 0, 0, 0};
        if (vlan_init(&init_data) != MESA_RC_OK) {
            ICLI_PRINTF("Could not reset VLAN configuration.\n");
            rc = ICLI_RC_ERROR;
        } else {    
            ICLI_PRINTF("Adding VLAN ID 2 to allowed access VLANs\n");
            if (vtss_appl_vlan_access_vids_get(access_vids) != MESA_RC_OK) {  // Get current list of access VLANs
                ICLI_PRINTF("Unable to get current list of access VLANs.\n");
                rc = ICLI_RC_ERROR;
            } else {                                                          // Add VLAN IDs 2 and 3 to allowed access VLANs
                VTSS_BF_SET(access_vids, 2, 1);
                if (vtss_appl_vlan_access_vids_set(access_vids) != MESA_RC_OK) {
                    ICLI_PRINTF("Unable to add VLAN ID = 2  to alllowed VLANs\n");
                    rc = ICLI_RC_ERROR;
                } else {
                    vtss_appl_vlan_port_conf_t conf;
    
                    if (vlan_mgmt_port_conf_get(VTSS_ISID_START, iport, &conf, VTSS_APPL_VLAN_USER_STATIC, FALSE) != VTSS_RC_OK) {
                        ICLI_PRINTF("Unable to get port VLAN configuration for port %d\n", iport2uport(iport));
                        rc = ICLI_RC_ERROR;
                    } else {
                        conf.access_pvid = 2;
                        if (vlan_mgmt_port_conf_set(VTSS_ISID_START, iport, &conf, VTSS_APPL_VLAN_USER_STATIC) != VTSS_RC_OK) {
                            ICLI_PRINTF("Unable to set port VLAN configuration for port %d\n", iport2uport(iport));
                            rc = ICLI_RC_ERROR;
                        }
                    }
                }
            }
        }
    }

    // Setup a new PTP master and slave using the port specified by iport.
    // Note: Since only the Sync packets are needed, the PTP instances shall be configured as one-way.
    if (rc == ICLI_RC_OK) {
        mesa_rc my_rc;

        ICLI_PRINTF("Creating PTP master and slave used for calibration\n");

        ptp_get_default_clock_default_ds(&init_default_ds_status, &init_default_ds);
        ptp_apply_profile_defaults_to_default_ds(&init_default_ds, VTSS_APPL_PTP_PROFILE_NO_PROFILE);

        init_default_ds.deviceType = VTSS_APPL_PTP_DEVICE_MASTER_ONLY;
        init_default_ds.oneWay = true;

        if ((my_rc = ptp_clock_clockidentity_set(0, &init_default_ds_status.clockIdentity)) != VTSS_RC_OK) {
            ICLI_PRINTF("Cannot set clock identity of PTP master: %s\n", error_txt(my_rc));
            rc = ICLI_RC_ERROR;
        } else {
            init_default_ds.configured_vid = 2;
            if ((my_rc = vtss_appl_ptp_clock_config_default_ds_set(0, &init_default_ds)) != VTSS_RC_OK) {
                ICLI_PRINTF("Cannot create PTP master as instance 0: %s\n", error_txt(my_rc));
                rc = ICLI_RC_ERROR;
            } else {
                init_default_ds_status.clockIdentity[7] += 1;
                if ((my_rc = ptp_clock_clockidentity_set(1, &init_default_ds_status.clockIdentity)) != VTSS_RC_OK) {
                    ICLI_PRINTF("Cannot set clock identity of PTP slave: %s\n", error_txt(my_rc));
                    rc = ICLI_RC_ERROR;
                } else {
                    init_default_ds.deviceType = VTSS_APPL_PTP_DEVICE_SLAVE_ONLY;
                    if ((my_rc = vtss_appl_ptp_clock_config_default_ds_set(1, &init_default_ds)) != VTSS_RC_OK) {
                        ICLI_PRINTF("Cannot create PTP slave as instance 1: %s\n", error_txt(my_rc));
                        rc = ICLI_RC_ERROR;
                    }
                }
            }
        }

        // Enable port given by value of iport
        if (rc == ICLI_RC_OK) {
            vtss_ifindex_t ifindex;
            vtss_appl_ptp_config_port_ds_t ds_cfg;

            (void) vtss_ifindex_from_port(0, iport, &ifindex);
            if ((my_rc = vtss_appl_ptp_config_clocks_port_ds_get(0, ifindex, &ds_cfg)) == VTSS_RC_OK) {
                ds_cfg.enabled = true;
                ds_cfg.logSyncInterval = -4;
                if ((my_rc = vtss_appl_ptp_config_clocks_port_ds_set(0, ifindex, &ds_cfg)) != VTSS_RC_OK) {
                    ICLI_PRINTF("Error enabling port %d (%s) on PTP master\n", iport2uport(iport), error_txt(my_rc));
                    rc = ICLI_RC_ERROR;
                } else {
                    if ((my_rc = vtss_appl_ptp_config_clocks_port_ds_get(1, ifindex, &ds_cfg)) == VTSS_RC_OK) {
                        ds_cfg.enabled = true;
                        ds_cfg.logSyncInterval = -4;
                        if ((my_rc = vtss_appl_ptp_config_clocks_port_ds_set(1, ifindex, &ds_cfg)) != VTSS_RC_OK) {
                            ICLI_PRINTF("Error enabling port %d (%s) on PTP slave\n", iport2uport(iport), error_txt(my_rc));
                            rc = ICLI_RC_ERROR;
                        }
                    } else {
                        ICLI_PRINTF("Error getting port data for port %d on PTP slave\n", iport2uport(iport));
                        rc = ICLI_RC_ERROR;
                    }
                }
            } else {
                ICLI_PRINTF("Error getting port data for port %d on PTP master\n", iport2uport(iport));
                rc = ICLI_RC_ERROR;
            }
        }
    }

    // If has_int is true then configure an internal loopback (equipment loopback).    
    if (has_int && rc == ICLI_RC_OK) {
        mesa_port_test_conf_t conf;

        ICLI_PRINTF("Need to wait 5 seconds before setting up internal loopback\n");
        sleep(5);

        if (mesa_port_test_conf_get(NULL, iport, &conf) == MESA_RC_OK) {
            conf.loopback = MESA_PORT_LB_EQUIPMENT;  // MESA_PORT_LB_NEAR_END;
            if (mesa_port_test_conf_set(NULL, iport, &conf) != MESA_RC_OK) {
                ICLI_PRINTF("Error enabling port loopback.\n");
                rc = ICLI_RC_ERROR;
            }
        } else {
            ICLI_PRINTF("Error getting configuration for loopback port.\n");
            rc = ICLI_RC_ERROR;
        }
    }

    // If spanning tree is configured, it shall be disabled temporarily for the port that is being calibrated.
    {
    }

    // Set flag that makes vtss_ptp_slave_sync do the calibration
    if (rc == ICLI_RC_OK) {
        calib_initiate = true;
        calib_t_plane_enabled = true;
    
        // Wait for vtss_ptp_slave_sync to clear the flag and report that calibration has been performed
        // If waiting time exceeds 30 seconds (value TBD) stop calibration and report that there is a problem
        // for instance an external loopback may not have been applied correctly. 
        ICLI_PRINTF("Now waiting up tp 30 seconds for calibration to be performed.\n");
        {
            int n = 0;
            while (calib_t_plane_enabled && n < 30) {
                sleep(1);
                n++;
            }
            if (n == 30) {
                calib_t_plane_enabled = false;
                rc = ICLI_RC_ERROR;
            }
        }

        // Remove internal loopback if one was set up
        if (has_int) {
            mesa_port_test_conf_t conf;
    
            if (mesa_port_test_conf_get(NULL, iport, &conf) == MESA_RC_OK) {
                conf.loopback = MESA_PORT_LB_DISABLED;
                if (mesa_port_test_conf_set(NULL, iport, &conf) != MESA_RC_OK) {
                    ICLI_PRINTF("Error removing port loopback.\n");
                    rc = ICLI_RC_ERROR;
                }
            } else {
                ICLI_PRINTF("Error getting configuration for loopback port.\n");
                rc = ICLI_RC_ERROR;
            }
        }
    }

    // Report that the calibration has been completed
    if (rc == ICLI_RC_OK) {
        ICLI_PRINTF("Calibration completed.\n");
    } else {
        ICLI_PRINTF("Calibration aborted.\n");
    }

    return rc;
}

icli_rc_t ptp_icli_cal_p2p(i32 session_id, u32 ref_iport, u32 other_iport, i32 cable_latency)
{
    icli_rc_t rc = ICLI_RC_OK;
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    vtss_appl_ptp_clock_config_default_ds_t init_default_ds;
    vtss_appl_ptp_clock_status_default_ds_t init_default_ds_status;

    ICLI_PRINTF("Starting port to port calibration of port: %d with reference port: %d and cable latency: %d)\n", iport2uport(other_iport), iport2uport(ref_iport), cable_latency);

    // Remove any PTP instances already present.
    ICLI_PRINTF("Deleting any existing PTP instances\n");
    for (int clockinst = 0; clockinst < PTP_CLOCK_INSTANCES; clockinst++) {
        if (vtss_appl_ptp_clock_config_default_ds_get(clockinst, &default_ds_cfg) == VTSS_RC_OK) {
            if (vtss_appl_ptp_clock_config_default_ds_del(clockinst) != VTSS_RC_OK) {
                ICLI_PRINTF("Cannot delete clock instance %d (may be that another clock instance depends on it)\n", clockinst);
                ICLI_PRINTF("Please try to delete all PTP instances before attempting a calibration.\n");
                rc = ICLI_RC_ERROR;
            }
        }
    }

    // Reset VLAN configuration so the switch in general is using VLAN 1 for all ports and all ports are configured as
    // access ports. VLAN 2 shall be used for the master port and VLAN 3 shall be used for the slave port.
    if (rc == ICLI_RC_OK) {
        u8 access_vids[VTSS_APPL_VLAN_BITMASK_LEN_BYTES];

        ICLI_PRINTF("Resetting VLAN configuration to default\n");
        vtss_init_data_t init_data{INIT_CMD_CONF_DEF, VTSS_ISID_GLOBAL, 0, 0, 0, 0};
        if (vlan_init(&init_data) != MESA_RC_OK) {
            ICLI_PRINTF("Could not reset VLAN configuration.\n");
            rc = ICLI_RC_ERROR;
        } else {
            ICLI_PRINTF("Adding VLAN IDs 2 and 3 to allowed access VLANs\n");
            if (vtss_appl_vlan_access_vids_get(access_vids) != MESA_RC_OK) {  // Get current list of access VLANs
                ICLI_PRINTF("Unable to get current list of access VLANs.\n");
                rc = ICLI_RC_ERROR;
            } else {                                                          // Add VLAN IDs 2 and 3 to allowed access VLANs
                VTSS_BF_SET(access_vids, 2, 1);
                VTSS_BF_SET(access_vids, 3, 1);   
                if (vtss_appl_vlan_access_vids_set(access_vids) != MESA_RC_OK) {
                    ICLI_PRINTF("Unable to add VLAN IDs = 2 and 3 to alllowed VLANs\n");
                    rc = ICLI_RC_ERROR;
                } else {
                    vtss_appl_vlan_port_conf_t conf;
    
                    if (vlan_mgmt_port_conf_get(VTSS_ISID_START, ref_iport, &conf, VTSS_APPL_VLAN_USER_STATIC, FALSE) != VTSS_RC_OK) {
                        ICLI_PRINTF("Unable to get port VLAN configuration for port %d\n", iport2uport(ref_iport));
                        rc = ICLI_RC_ERROR;
                    } else {
                        conf.access_pvid = 2;
                        if (vlan_mgmt_port_conf_set(VTSS_ISID_START, ref_iport, &conf, VTSS_APPL_VLAN_USER_STATIC) != VTSS_RC_OK) {
                            ICLI_PRINTF("Unable to set port VLAN configuration for port %d\n", iport2uport(ref_iport));
                            rc = ICLI_RC_ERROR;
                        }
                    }
    
                    if (vlan_mgmt_port_conf_get(VTSS_ISID_START, other_iport, &conf, VTSS_APPL_VLAN_USER_STATIC, FALSE) != VTSS_RC_OK) {
                        ICLI_PRINTF("Unable to get port VLAN configuration for port %d\n", other_iport);
                        rc = ICLI_RC_ERROR;
                    } else {
                        conf.access_pvid = 3;
                        if (vlan_mgmt_port_conf_set(VTSS_ISID_START, other_iport, &conf, VTSS_APPL_VLAN_USER_STATIC) != VTSS_RC_OK) {
                            ICLI_PRINTF("Unable to set port VLAN configuration for port %d\n", other_iport);
                            rc = ICLI_RC_ERROR;
                        }
                    }
                }
            }
        }
    }

    // Setup a new PTP master and slave using the ports specified by ref_iport and other_iport.
    // While setting up the PTP master and slave use the VLAN IDs configured above.
    // Note: Since only the Sync packets are needed, the PTP instances shall be configured as one-way.
    if (rc == ICLI_RC_OK) {
        mesa_rc my_rc;

        ICLI_PRINTF("Creating PTP master and slave used for calibration\n");

        ptp_get_default_clock_default_ds(&init_default_ds_status, &init_default_ds);
        ptp_apply_profile_defaults_to_default_ds(&init_default_ds, VTSS_APPL_PTP_PROFILE_NO_PROFILE);

        init_default_ds.deviceType = VTSS_APPL_PTP_DEVICE_MASTER_ONLY;
        init_default_ds.oneWay = true;

        if ((my_rc = ptp_clock_clockidentity_set(0, &init_default_ds_status.clockIdentity)) != VTSS_RC_OK) {
            ICLI_PRINTF("Cannot set clock identity of PTP master: %s\n", error_txt(my_rc));
            rc = ICLI_RC_ERROR;
        } else {
            init_default_ds.configured_vid = 2;
            if ((my_rc = vtss_appl_ptp_clock_config_default_ds_set(0, &init_default_ds)) != VTSS_RC_OK) {
                ICLI_PRINTF("Cannot create PTP master as instance 0: %s\n", error_txt(my_rc));
                rc = ICLI_RC_ERROR;
            } else {
                init_default_ds.configured_vid = 3;
                init_default_ds_status.clockIdentity[7] += 1;
                if ((my_rc = ptp_clock_clockidentity_set(1, &init_default_ds_status.clockIdentity)) != VTSS_RC_OK) {
                    ICLI_PRINTF("Cannot set clock identity of PTP slave: %s\n", error_txt(my_rc));
                    rc = ICLI_RC_ERROR;
                } else {
                    init_default_ds.deviceType = VTSS_APPL_PTP_DEVICE_SLAVE_ONLY;
                    if ((my_rc = vtss_appl_ptp_clock_config_default_ds_set(1, &init_default_ds)) != VTSS_RC_OK) {
                        ICLI_PRINTF("Cannot create PTP slave as instance 1: %s\n", error_txt(my_rc));
                        rc = ICLI_RC_ERROR;
                    }
                }
            }
        }

        // Enable master port given by value of ref_iport and slave port given by value of slave_iport
        if (rc == ICLI_RC_OK) {
            vtss_ifindex_t ifindex;
            vtss_appl_ptp_config_port_ds_t ds_cfg;

            (void) vtss_ifindex_from_port(0, ref_iport, &ifindex);
            if ((my_rc = vtss_appl_ptp_config_clocks_port_ds_get(0, ifindex, &ds_cfg)) == VTSS_RC_OK) {
                ds_cfg.enabled = true;
                ds_cfg.logSyncInterval = -4;
                if ((my_rc = vtss_appl_ptp_config_clocks_port_ds_set(0, ifindex, &ds_cfg)) != VTSS_RC_OK) {
                    ICLI_PRINTF("Error enabling port %d (%s) on PTP master\n", iport2uport(ref_iport), error_txt(my_rc));
                    rc = ICLI_RC_ERROR;
                } else {
                    (void) vtss_ifindex_from_port(0, other_iport, &ifindex);
                    if ((my_rc = vtss_appl_ptp_config_clocks_port_ds_get(1, ifindex, &ds_cfg)) == VTSS_RC_OK) {
                        ds_cfg.enabled = true;
                        ds_cfg.logSyncInterval = -4;
                        if ((my_rc = vtss_appl_ptp_config_clocks_port_ds_set(1, ifindex, &ds_cfg)) != VTSS_RC_OK) {
                            ICLI_PRINTF("Error enabling port %d (%s) on PTP slave\n", iport2uport(other_iport), error_txt(my_rc));
                            rc = ICLI_RC_ERROR;
                        }
                    } else {
                        ICLI_PRINTF("Error getting port data for port %d on PTP slave\n", iport2uport(other_iport));
                        rc = ICLI_RC_ERROR;
                    }
                }
            } else {
                ICLI_PRINTF("Error getting port data for port %d on PTP master\n", iport2uport(ref_iport));
                rc = ICLI_RC_ERROR;
            }
        }
    }

    // If spanning tree is configured, it shall be disabled temporarily for the port that is being calibrated.
    {
    }

    // Set flag that makes vtss_ptp_slave_sync do the calibration
    if (rc == ICLI_RC_OK) {
        calib_initiate = true;
        calib_cable_latency = cable_latency;
        calib_p2p_enabled = true;

        // Wait for vtss_ptp_slave_sync to clear the flag and report that calibration has been performed
        // If waiting time exceeds 30 seconds (value TBD) stop calibration and report that there is a problem
        // for instance an external loopback may not have been applied correctly. 
        ICLI_PRINTF("Now waiting up tp 30 seconds for calibration to be performed.\n");
        {
            int n = 0;
            while (calib_p2p_enabled && n < 30) {
                sleep(1);
                n++;
            }
            if (n == 30) {
                calib_p2p_enabled = false;
                rc = ICLI_RC_ERROR;
            }
        }
    }

    // Report that the calibration has been completed
    if (rc == ICLI_RC_OK) {
        ICLI_PRINTF("Calibration completed.\n");
    } else {
        ICLI_PRINTF("Calibration aborted.\n");
    }

    return rc;
}

icli_rc_t ptp_icli_cal_port_start(i32 session_id, u32 iport, BOOL has_synce)
{
    icli_rc_t rc = ICLI_RC_OK;
    vtss_ifindex_t ifindex;
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    vtss_appl_ptp_clock_config_default_ds_t init_default_ds;
    vtss_appl_ptp_clock_status_default_ds_t init_default_ds_status;

    (void) vtss_ifindex_from_port(0, iport, &ifindex);

    ICLI_PRINTF("Starting calibration of port: %d using external reference.\n", iport2uport(iport));

    // Check that port has link status 'up'. A port without link cannot be calibrated.
    {
        vtss_appl_port_status_t port_status;

        // Determine port mode
        (void) vtss_appl_port_status_get(ifindex, &port_status);

        // Check that port has link stateus 'up'
        if (!port_status.status.link) {
            ICLI_PRINTF("Port link status is 'down' - cannot calibrate.\n");
            rc = ICLI_RC_ERROR;
        }
    }

    // Clear SyncE configuration
#if defined(VTSS_SW_OPTION_SYNCE)
    if (rc == ICLI_RC_OK) {
        ICLI_PRINTF("Resetting SyncE configuration\n");
        vtss_appl_synce_reset_configuration_to_default();
    }
#endif

    // If synce parameter is specified then configure SyncE on the port being calibrated
#if defined(VTSS_SW_OPTION_SYNCE)
    if (rc == ICLI_RC_OK) {
        if (has_synce) {
            mesa_rc my_rc;
            vtss_appl_synce_port_config_t port_config;

            ICLI_PRINTF("Enabling SyncE on port to be calibrated\n");

            // Enable SSM for port being calibrated
            if ((my_rc = vtss_appl_synce_port_config_get(ifindex, &port_config)) == VTSS_RC_OK) {
                port_config.ssm_enabled = true;
                if ((my_rc = vtss_appl_synce_port_config_set(ifindex, &port_config)) != VTSS_RC_OK) {
                    ICLI_PRINTF("Error setting SyncE port configuration for port %d\n", iport2uport(iport));
                    rc = ICLI_RC_ERROR;
                }
            } else {
                ICLI_PRINTF("Error getting SyncE port configuration for port %d\n", iport2uport(iport));
                rc = ICLI_RC_ERROR;
            }

            // Set clock source nomination
            if (rc == ICLI_RC_OK) {
                vtss_appl_synce_clock_source_nomination_config_t clock_source_nomination_config;

                if ((my_rc = vtss_appl_synce_clock_source_nomination_config_get(1, &clock_source_nomination_config)) == VTSS_RC_OK) {
                    clock_source_nomination_config.nominated = true;
                    clock_source_nomination_config.network_port = ifindex;
                    clock_source_nomination_config.clk_in_port = 0;
                    clock_source_nomination_config.priority = 0;
                    if ((my_rc = vtss_appl_synce_clock_source_nomination_config_set(1, &clock_source_nomination_config)) != VTSS_RC_OK) {
                        ICLI_PRINTF("Error setting SyncE clock source nomination configuration for source 1\n");
                        rc = ICLI_RC_ERROR;
                    }
                } else {
                    ICLI_PRINTF("Error getting SyncE clock source nomination configuration for source 1\n");
                    rc = ICLI_RC_ERROR;
                }
            }

            // Set clock selection mode
            if (rc == ICLI_RC_OK) {
                vtss_appl_synce_clock_selection_mode_config_t clock_selection_mode_config;

                if ((my_rc = vtss_appl_synce_clock_selection_mode_config_get(&clock_selection_mode_config)) == VTSS_RC_OK) {
                    clock_selection_mode_config.selection_mode = VTSS_APPL_SYNCE_SELECTOR_MODE_MANUEL;
                    clock_selection_mode_config.source = 1;
                    clock_selection_mode_config.wtr_time = 0;
                    clock_selection_mode_config.eec_option = VTSS_APPL_SYNCE_EEC_OPTION_1;
                    if ((my_rc = vtss_appl_synce_clock_selection_mode_config_set(&clock_selection_mode_config)) != VTSS_RC_OK) {
                        ICLI_PRINTF("Error setting SyncE clock selection mode configuration\n");
                        rc = ICLI_RC_ERROR;
                    }
                } else {
                    ICLI_PRINTF("Error getting SyncE clock selection mode configuration\n");
                    rc = ICLI_RC_ERROR;
                }
            }
        }
    }
#endif

    // Setup PTP adjustment method
    if (rc == ICLI_RC_OK) {
        vtss_appl_ptp_ext_clock_mode_t ext_clock_mode;

        ext_clock_mode.one_pps_mode = VTSS_APPL_PTP_ONE_PPS_OUTPUT;
        ext_clock_mode.clock_out_enable =  false;
        ext_clock_mode.adj_method = VTSS_APPL_PTP_PREFERRED_ADJ_AUTO;
        ext_clock_mode.freq = 1;

        if (vtss_appl_ptp_ext_clock_out_set(&ext_clock_mode) != VTSS_RC_OK) {
            ICLI_PRINTF("Could not set PTP adjustment method.\n");
            rc = ICLI_RC_ERROR;
        }
    }

    // Remove any PTP instances already present.
    if (rc == ICLI_RC_OK) {
        ICLI_PRINTF("Deleting any existing PTP instances\n");
        for (int clockinst = 0; clockinst < PTP_CLOCK_INSTANCES; clockinst++) {
            if (vtss_appl_ptp_clock_config_default_ds_get(clockinst, &default_ds_cfg) == VTSS_RC_OK) {
                if (vtss_appl_ptp_clock_config_default_ds_del(clockinst) != VTSS_RC_OK) {
                    ICLI_PRINTF("Cannot delete clock instance %d (may be that another clock instance depends on it)\n", clockinst);
                    ICLI_PRINTF("Please try to delete all PTP instances before attempting a calibration.\n");
                    rc = ICLI_RC_ERROR;
                }
            }
        }
    }

    // Reset VLAN configuration so the switch in general is using VLAN 1 for all ports and all ports are configured as
    // access ports. VLAN 2 shall be used for the master port and VLAN 3 shall be used for the slave port.
    if (rc == ICLI_RC_OK) {
        ICLI_PRINTF("Resetting VLAN configuration to default\n");
        vtss_init_data_t init_data{INIT_CMD_CONF_DEF, VTSS_ISID_GLOBAL, 0, 0, 0, 0};
        if (vlan_init(&init_data) != MESA_RC_OK) {
            ICLI_PRINTF("Could not reset VLAN configuration.\n");
            rc = ICLI_RC_ERROR;
        } 
    }

    // Setup a new PTP slave using the port specified by iport.
    if (rc == ICLI_RC_OK) {
        mesa_rc my_rc;

        ICLI_PRINTF("Creating PTP slave used for calibration\n");

        // Create a PTP instance
        ptp_get_default_clock_default_ds(&init_default_ds_status, &init_default_ds);
        ptp_apply_profile_defaults_to_default_ds(&init_default_ds, VTSS_APPL_PTP_PROFILE_NO_PROFILE);
        init_default_ds.deviceType = VTSS_APPL_PTP_DEVICE_SLAVE_ONLY;
        init_default_ds.filter_type = PTP_FILTERTYPE_BASIC;

        if ((my_rc = ptp_clock_clockidentity_set(0, &init_default_ds_status.clockIdentity)) != VTSS_RC_OK) {
            ICLI_PRINTF("Cannot set clock identity of PTP slave: %s\n", error_txt(my_rc));
            rc = ICLI_RC_ERROR;
        } else {
            if ((my_rc = vtss_appl_ptp_clock_config_default_ds_set(0, &init_default_ds)) != VTSS_RC_OK) {
                ICLI_PRINTF("Cannot create PTP slave as instance 0: %s\n", error_txt(my_rc));
                rc = ICLI_RC_ERROR;
            }
        }

        // Enable slave port given by value of iport
        if (rc == ICLI_RC_OK) {
            vtss_ifindex_t ifindex;
            vtss_appl_ptp_config_port_ds_t ds_cfg;

            (void) vtss_ifindex_from_port(0, iport, &ifindex);
            if ((my_rc = vtss_appl_ptp_config_clocks_port_ds_get(0, ifindex, &ds_cfg)) == VTSS_RC_OK) {
                ds_cfg.enabled = true;
                ds_cfg.logSyncInterval = -7;
                ds_cfg.logMinPdelayReqInterval = -7;
                if ((my_rc = vtss_appl_ptp_config_clocks_port_ds_set(0, ifindex, &ds_cfg)) != VTSS_RC_OK) {
                    ICLI_PRINTF("Error enabling port %d (%s) on PTP slave\n", iport2uport(iport), error_txt(my_rc));
                    rc = ICLI_RC_ERROR;
                }
            } else {
                ICLI_PRINTF("Error getting port data for port %d on PTP master\n", iport2uport(iport));
                rc = ICLI_RC_ERROR;
            }
        }

        // Change filter parameters
        if (rc == ICLI_RC_OK) {
            vtss_appl_ptp_clock_filter_config_t filter_params;

            if ((my_rc = vtss_appl_ptp_clock_filter_parameters_get(0, &filter_params)) == VTSS_RC_OK) {
                filter_params.delay_filter = 0;
                filter_params.period = 1;
                filter_params.dist = 0;

                if ((my_rc = vtss_appl_ptp_clock_filter_parameters_set(0, &filter_params)) != VTSS_RC_OK) {
                    ICLI_PRINTF("Error setting filter parameters for PTP instance 0\n");
                    rc = ICLI_RC_ERROR;
                } 
            } else {
                ICLI_PRINTF("Error getting filter parameters for PTP instance 0\n");
                rc = ICLI_RC_ERROR;
            }
        }

        // Change servo parameters
        if (rc == ICLI_RC_OK) {
            vtss_appl_ptp_clock_servo_config_t servo_params;

            if ((my_rc = vtss_appl_ptp_clock_servo_parameters_get(0, &servo_params)) == VTSS_RC_OK) {
                servo_params.p_reg = true;
                servo_params.i_reg = true;
                servo_params.d_reg = true;
                servo_params.ap = 12;
                servo_params.ai = 512;
                servo_params.ad = 7;
                servo_params.gain = 16;

                if (has_synce) {
                    servo_params.srv_option = VTSS_APPL_PTP_CLOCK_SYNCE;
                }

                if ((my_rc = vtss_appl_ptp_clock_servo_parameters_set(0, &servo_params)) != VTSS_RC_OK) {
                    ICLI_PRINTF("Error setting servo parameters for PTP instance 0\n");
                    rc = ICLI_RC_ERROR;
                } 
            } else {
                ICLI_PRINTF("Error getting servo parameters for PTP instance 0\n");
                rc = ICLI_RC_ERROR;
            }
        }
    }

    if (rc == ICLI_RC_OK) {
        ICLI_PRINTF("------------------------------------------------------------------------------------------\n");
        ICLI_PRINTF("A PTP slave was setup for calibration.\n");
        ICLI_PRINTF("Please wait for servo to lock. Then measure 1PPS difference between PTP master (reference)\n");
        ICLI_PRINTF("and PTP slave (device under calibration) the continue calibration i.e. issue command:\n");
        ICLI_PRINTF("\n");
        ICLI_PRINTF("    ptp cal port <port> offset <-100000-100000> cable-latency <-100000-100000>\n");
        ICLI_PRINTF("\n");
        ICLI_PRINTF("------------------------------------------------------------------------------------------\n");
    }

    return rc;
}

icli_rc_t ptp_icli_cal_port_offset(i32 session_id, u32 iport, i32 pps_offset, i32 cable_latency)
{
    icli_rc_t rc = ICLI_RC_OK;
    ICLI_PRINTF("Performing calibration of port: %d using external reference with measured 1PPS offset: %d and specified cable latency: %d\n", iport2uport(iport), pps_offset, cable_latency);

    // Check that the PTP setup is the same as set up by the "cal port start" command.
    // I.e. that there is only a single PTP slave instance and that it is in two-way mode.
    for (int clockinst = 0; clockinst < PTP_CLOCK_INSTANCES; clockinst++) {
       vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg; 

       if (clockinst == 0) {
           if (vtss_appl_ptp_clock_config_default_ds_get(clockinst, &default_ds_cfg) != VTSS_RC_OK) {
               ICLI_PRINTF("Cannot get defaultDS for PTP instance 0\n");
               rc = ICLI_RC_ERROR;
           } else {
               if (default_ds_cfg.deviceType != VTSS_APPL_PTP_DEVICE_SLAVE_ONLY) {
                   ICLI_PRINTF("PTP instance 0 is supposed to be a 'slave only' device.\n");
                   rc = ICLI_RC_ERROR;
               } else if (default_ds_cfg.oneWay) {
                   ICLI_PRINTF("PTP instance 0 is supposed to be configured as 'two-way'\n");
                   rc = ICLI_RC_ERROR;
               } else {
                   port_iter_t pit;
                   vtss_ifindex_t ifindex;
                   uint port_no;
                   vtss_appl_ptp_config_port_ds_t port_cfg;

                   // Check that PTP instance 0 has only a single port enabled and that the port is the one specified by iport
                   port_iter_init_local(&pit);
                   while (port_iter_getnext(&pit)) {
                       (void) vtss_ifindex_from_port(0, pit.iport, &ifindex);
                       (void) ptp_ifindex_to_port(ifindex, &port_no);
       
                       if (vtss_appl_ptp_config_clocks_port_ds_get(clockinst, ifindex, &port_cfg) == VTSS_RC_OK) {
                           if (!port_cfg.enabled & (port_no == iport)) {
                               ICLI_PRINTF("PTP instance 0 is supposed to have port %d enabled (as the only one)\n", iport2uport(iport));
                               rc = ICLI_RC_ERROR;
                           } else if (port_cfg.enabled & (port_no != iport)) {
                               ICLI_PRINTF("PTP instance 0 is not supposed to have other ports enabled than port %d. Port %d is enabled.\n", iport2uport(iport), iport2uport(port_no));
                               rc = ICLI_RC_ERROR;
                           }
                       }
                   }
               }
           }
       } else {
           if (vtss_appl_ptp_clock_config_default_ds_get(clockinst, &default_ds_cfg) == VTSS_RC_OK) {       
               ICLI_PRINTF("Extra PTP instances found. Only PTP instance 0 is supposed to exist.\n");
               rc = ICLI_RC_ERROR;
           }
       }
    }

    // Check that the servo is locked
    if (rc == ICLI_RC_OK) {
        vtss_appl_ptp_clock_slave_ds_t ss;
    
        if (vtss_appl_ptp_clock_status_slave_ds_get(0, &ss) == VTSS_RC_OK) {
            if (ss.slave_state != VTSS_APPL_PTP_SLAVE_CLOCK_STATE_PHASE_LOCKED) {
                ICLI_PRINTF("PTP slave not phase locked.\n");
                rc = ICLI_RC_ERROR;
            }
        } else {
            ICLI_PRINTF("Could not get state of PTP slave\n");
            rc = ICLI_RC_ERROR;
        }
    }

    // Check that offset from master is below 5 ns
    if (rc == ICLI_RC_OK) {
        vtss_appl_ptp_clock_current_ds_t status;

        if (vtss_appl_ptp_clock_status_current_ds_get(0, &status) == VTSS_RC_OK) {
            if (llabs(status.offsetFromMaster) > (5LL << 16)) {
                ICLI_PRINTF("Offset from master must be in the range from -5ns to 5ns to perform calibration. Calibration not performed.\n");
                rc = ICLI_RC_ERROR;
            }
        } else {
            ICLI_PRINTF("Could not get current DS of PTP slave\n");
            rc = ICLI_RC_ERROR;
        }
    }

    // Set flag that makes vtss_ptp_slave_sync do the calibration
    if (rc == ICLI_RC_OK) {
        calib_initiate = true;
        calib_cable_latency = cable_latency;
        calib_pps_offset = pps_offset;
        calib_port_enabled = true;

        // Wait for vtss_ptp_slave_sync to clear the flag and report that calibration has been performed
        // If waiting time exceeds 30 seconds (value TBD) stop calibration and report that there is a problem
        // for instance an external loopback may not have been applied correctly. 
        ICLI_PRINTF("Now waiting up tp 30 seconds for calibration to be performed.\n");
        {
            int n = 0;
            while (calib_port_enabled && n < 30) {
                sleep(1);
                n++;
            }
            if (n == 30) {
                calib_port_enabled = false;
                rc = ICLI_RC_ERROR;
            }
        }
    }

    // Report that the calibration has been completed
    if (rc == ICLI_RC_OK) {
        ICLI_PRINTF("Calibration completed.\n");
    } else {
        ICLI_PRINTF("Calibration aborted.\n");
    }

    return rc;
}

icli_rc_t ptp_icli_cal_port_reset(i32 session_id, u32 iport, BOOL has_10m_cu, BOOL has_100m_cu, BOOL has_1g_cu,
                                  BOOL has_1g, BOOL has_2g5, BOOL has_5g, BOOL has_10g, BOOL has_all)
{
    icli_rc_t rc = ICLI_RC_OK;

    auto specified_mode = [has_10m_cu, has_100m_cu, has_1g_cu, has_1g, has_2g5, has_5g, has_10g, has_all]() {
                              if (has_10m_cu) return "10M_CU";
                              else if (has_100m_cu) return "100M_CU";
                              else if (has_1g_cu) return "1G_CU";
                              else if (has_1g)  return "1G";
                              else if (has_2g5) return "2G5";
                              else if (has_5g)  return "5G";
                              else if (has_10g) return "10G";
                              else if (has_all) return "ALL";
                              else              return "?";
                          };

    if (!has_1g && !has_2g5 && !has_10g && !has_all) {
        ICLI_PRINTF("Resetting calibration of the current mode for port: %d.\n", iport2uport(iport));

        // Check that port has link status 'up'. A port without link cannot be calibrated.
        {
            vtss_appl_port_status_t port_status;
            vtss_ifindex_t ifindex;
    
            // Determine port mode
            (void) vtss_ifindex_from_port(0, iport, &ifindex);
            (void) vtss_appl_port_status_get(ifindex, &port_status);
    
            // Check that port has link stateus 'up'
            if (!port_status.status.link) {
                ICLI_PRINTF("Port link status is 'down' - cannot determine port mode and hence not reset calibration.\n");
                rc = ICLI_RC_ERROR;
            } else {
                switch (port_status.status.speed) {
                    case MESA_SPEED_10M:    has_10m_cu  = TRUE; break;
                    case MESA_SPEED_100M:   has_100m_cu = TRUE; break;
                    case MESA_SPEED_1G:     if (port_status.status.fiber) {
                                                has_1g  = TRUE;
                                                break;
                                            } else {
                                                has_1g_cu  = TRUE;
                                                break;
                                            }
                    case MESA_SPEED_2500M:  has_2g5 = TRUE; break;
                    case MESA_SPEED_5G:     has_5g = TRUE; break;
                    case MESA_SPEED_10G:    has_10g = TRUE; break;
                    default:;
                }
            }
        }
    } else {
        bool explicit_mode_supported = false;

        if (!has_all) {
            /* Check that explicitly specified port mode is actually supported */
            vtss_ifindex_t ifindex;
            vtss_appl_port_status_t port_status;

            if (vtss_ifindex_from_port(0, iport, &ifindex) != VTSS_RC_OK) {
                T_W("Could not get ifindex");
            } else {
                if (vtss_appl_port_status_get(ifindex, &port_status) != VTSS_RC_OK) {
                    T_W("Could not get port status");
                } else {
                    if ((has_10m_cu && (port_status.cap & (MEBA_PORT_CAP_10M_HDX | MEBA_PORT_CAP_10M_FDX))) ||
                        (has_100m_cu && (port_status.cap & (MEBA_PORT_CAP_100M_HDX | MEBA_PORT_CAP_100M_FDX))) ||
                        (has_1g_cu && (port_status.cap & (MEBA_PORT_CAP_1G_FDX)) && (port_status.cap & (MEBA_PORT_CAP_COPPER | MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER))) ||
                        (has_1g && ((port_status.cap & (MEBA_PORT_CAP_2_5G_FDX | MEBA_PORT_CAP_10G_FDX)) ||
                                    ((port_status.cap & MEBA_PORT_CAP_1G_FDX) && (port_status.cap & (MEBA_PORT_CAP_SFP_ONLY | MEBA_PORT_CAP_FIBER | MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER))))) ||
                        (has_2g5 && (port_status.cap & MEBA_PORT_CAP_2_5G_FDX)) ||
                        (has_5g && (port_status.cap & MEBA_PORT_CAP_5G_FDX)) ||
                        (has_10g && (port_status.cap & MEBA_PORT_CAP_10G_FDX)))
                    {
                        explicit_mode_supported = true;
                    }
                }
            }
        }

        if (has_all || explicit_mode_supported) {
            ICLI_PRINTF("Resetting calibration of %s mode(s) for port: %d.\n", specified_mode(), iport2uport(iport));
        } else {
            ICLI_PRINTF("Port %d does not support %s mode.\n", iport2uport(iport), specified_mode());
            rc = ICLI_RC_ERROR;
        }
    }

    if (rc == ICLI_RC_OK) {
        if (has_10m_cu || has_all) {
            ptp_port_calibration.port_calibrations[iport].port_latencies_10m_cu.ingress_latency = 0;
            ptp_port_calibration.port_calibrations[iport].port_latencies_10m_cu.egress_latency = 0;
        }

        if (has_100m_cu || has_all) {
            ptp_port_calibration.port_calibrations[iport].port_latencies_100m_cu.ingress_latency = 0;
            ptp_port_calibration.port_calibrations[iport].port_latencies_100m_cu.egress_latency = 0;
        }

        if (has_1g_cu || has_all) {
            ptp_port_calibration.port_calibrations[iport].port_latencies_1g_cu.ingress_latency = 0;
            ptp_port_calibration.port_calibrations[iport].port_latencies_1g_cu.egress_latency = 0;
        }

        if (has_1g || has_all) {
            ptp_port_calibration.port_calibrations[iport].port_latencies_1g.ingress_latency = 0;
            ptp_port_calibration.port_calibrations[iport].port_latencies_1g.egress_latency = 0;
        }
    
        if (has_2g5 || has_all) {
            ptp_port_calibration.port_calibrations[iport].port_latencies_2g5.ingress_latency = 0;
            ptp_port_calibration.port_calibrations[iport].port_latencies_2g5.egress_latency = 0;
        }

        if (has_5g || has_all) {
            ptp_port_calibration.port_calibrations[iport].port_latencies_5g.ingress_latency = 0;
            ptp_port_calibration.port_calibrations[iport].port_latencies_5g.egress_latency = 0;
        }
    
        if (has_10g || has_all) {
            ptp_port_calibration.port_calibrations[iport].port_latencies_10g.ingress_latency = 0;
            ptp_port_calibration.port_calibrations[iport].port_latencies_10g.egress_latency = 0;
        }
    
        // Write PTP port calibration to file on Linux filesystem
        {
            int ptp_port_calib_file = open(PTP_CALIB_FILE_NAME, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
            if (ptp_port_calib_file != -1) {
                ptp_port_calibration.version = PTP_CURRENT_CALIB_FILE_VER;
                u32 crc32 = vtss_crc32((const unsigned char*)&ptp_port_calibration.version, sizeof(u32));
                crc32 = vtss_crc32_accumulate(crc32, (const unsigned char*)ptp_port_calibration.port_calibrations.data(), MESA_CAP(MESA_CAP_PORT_CNT) * sizeof(port_calibrations_s));
                ptp_port_calibration.crc32 = crc32;
     
                ssize_t numwritten = write(ptp_port_calib_file, &ptp_port_calibration.version, sizeof(u32));
                numwritten += write(ptp_port_calib_file, &ptp_port_calibration.crc32, sizeof(u32));
                numwritten += write(ptp_port_calib_file, ptp_port_calibration.port_calibrations.data(), MESA_CAP(MESA_CAP_PORT_CNT) * sizeof(port_calibrations_s));
     
                if (numwritten != 2 * sizeof(u32) + MESA_CAP(MESA_CAP_PORT_CNT) * sizeof(port_calibrations_s)) {
                    T_W("Problem writing PTP port calibration data file.");
                    rc = ICLI_RC_ERROR;
                } else {
                    // Update ingrees and egress latencies in the hardware
                    {
                        mesa_timeinterval_t egress_latency;
    
                        vtss_1588_egress_latency_get(iport2uport(iport), &egress_latency);
                        vtss_1588_egress_latency_set(iport2uport(iport), egress_latency);
                    }
                    {
                        mesa_timeinterval_t ingress_latency;
    
                        vtss_1588_ingress_latency_get(iport2uport(iport), &ingress_latency);
                        vtss_1588_ingress_latency_set(iport2uport(iport), ingress_latency);
                    }
                }

                if (close(ptp_port_calib_file) == -1) {
                    T_W("Could not close PTP port calibration data file.");
                    rc = ICLI_RC_ERROR;
                }
            } else {
                T_W("Could not create/open PTP port calibration data file");
                rc = ICLI_RC_ERROR;
            }
        }
    }

    return rc;
}


icli_rc_t ptp_icli_cal_1pps(i32 session_id, i32 cable_latency)
{
    icli_rc_t rc = ICLI_RC_OK;

    vtss_ptp_rs422_conf_t mode;

    ICLI_PRINTF("Calibration of 1PPS input (cable_latency = %d)\n", cable_latency);

    // Turn on calibration mode for 1PPS input
    mode.mode = VTSS_PTP_RS422_CALIB;
    mode.delay = 0;
    mode.proto = VTSS_PTP_RS422_PROTOCOL_SER_POLYT;
    mode.port  = 0;
    vtss_ext_clock_rs422_conf_set(&mode);

    // Set flag that makes one_pps_external_input_interrupt_handler / io_pin_interrupt_handler do the calibration
    {
        calib_1pps_initiate = true;
        calib_1pps_enabled = true;

        // Wait for one_pps_external_input_interrupt_handler / io_pin_interrupt_handler to clear the flag and report that calibration has been performed
        // If waiting time exceeds 30 seconds (value TBD) stop calibration and report that there is a problem
        ICLI_PRINTF("Now waiting up tp 30 seconds for calibration to be performed.\n");
        {
            int n = 0;
            while (calib_1pps_enabled && n < 30) {
                sleep(1);
                n++;
            }
            if (n == 30) {
                calib_1pps_enabled = false;
                rc = ICLI_RC_ERROR;
            } else {
                vtss_ptp_rs422_conf_t mode;
                vtss_ext_clock_rs422_conf_get(&mode);

                ICLI_PRINTF("Measured 1PPS average roundtrip delay is %d ns\n", mode.delay);
            }
        }
    }

    // Disable calibration mode for 1PPS input
    mode.mode = VTSS_PTP_RS422_DISABLE;
    mode.delay = 0;
    mode.proto = VTSS_PTP_RS422_PROTOCOL_SER_POLYT;
    mode.port  = 0;
    vtss_ext_clock_rs422_conf_set(&mode);

    return rc;
}


icli_rc_t ptp_icli_ptp_cal_show(i32 session_id)
{
    ICLI_PRINTF("PTP Port Calibration\n\n");
    for (int n = 0; n < 7; n++) {
        switch (n) {
            case 0: ICLI_PRINTF("Mode: 10M\n");
                    break;
            case 1: ICLI_PRINTF("Mode: 100M\n");
                    break;
            case 2: ICLI_PRINTF("Mode: 1G (CU)\n");
                    break;                   
            case 3: ICLI_PRINTF("Mode: 1G (SFP)\n");
                    break;
            case 4: ICLI_PRINTF("Mode: 2.5G\n");
                    break;
            case 5: ICLI_PRINTF("Mode: 5G\n");
                    break;
            case 6: ICLI_PRINTF("Mode: 10G\n");
                    break;
        } 
        ICLI_PRINTF("Port        Ingress latency      Egress latency   \n");
        ICLI_PRINTF("--------------------------------------------------\n");

        for (int q = 0; q < MESA_CAP(MESA_CAP_PORT_CNT); q++) {
            vtss_ifindex_t ifindex;
            vtss_appl_port_status_t port_status;

            if (vtss_ifindex_from_port(0, q, &ifindex) != VTSS_RC_OK) {
                T_W("Could not get ifindex");
            } else {
                if (vtss_appl_port_status_get(ifindex, &port_status) != VTSS_RC_OK) {
                    T_W("Could not get port status");
                } else {
                    if ((n == 0 && (port_status.cap & (MEBA_PORT_CAP_10M_HDX | MEBA_PORT_CAP_10M_FDX))) ||
                        (n == 1 && (port_status.cap & (MEBA_PORT_CAP_100M_HDX | MEBA_PORT_CAP_100M_FDX))) ||
                        (n == 2 && (port_status.cap & (MEBA_PORT_CAP_1G_FDX)) && (port_status.cap & (MEBA_PORT_CAP_COPPER | MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER))) ||
                        (n == 3 && ((port_status.cap & (MEBA_PORT_CAP_2_5G_FDX | MEBA_PORT_CAP_10G_FDX)) ||
                                    ((port_status.cap & MEBA_PORT_CAP_1G_FDX) && (port_status.cap & (MEBA_PORT_CAP_SFP_ONLY | MEBA_PORT_CAP_FIBER | MEBA_PORT_CAP_DUAL_COPPER | MEBA_PORT_CAP_DUAL_FIBER))))) ||
                        (n == 4 && (port_status.cap & MEBA_PORT_CAP_2_5G_FDX)) ||
                        (n == 5 && (port_status.cap & MEBA_PORT_CAP_5G_FDX)) ||
                        (n == 6 && (port_status.cap & MEBA_PORT_CAP_10G_FDX)))
                    {
                        char buf[32];          
                    
                        auto mesa_timeinterval_to_str = [&buf](mesa_timeinterval_t t) {
                             snprintf(buf, sizeof(buf), "%10lld.%03lld", t >> 16, t >= 0 ? (((t & 0xffff) * 1000) >> 16) : (((-t & 0xffff) * 1000) >> 16));
                             return buf;
                        };
            
                        auto ptp_icli_ptp_cal_show_port = [session_id, mesa_timeinterval_to_str](u32 uport, port_latencies_s port_latencies) {
                            ICLI_PRINTF(" %2d      %s", iport2uport(uport), mesa_timeinterval_to_str(port_latencies.ingress_latency));                           
                            ICLI_PRINTF("      %s\n", mesa_timeinterval_to_str(port_latencies.egress_latency));
                        };
            
                        switch (n) {
                            case 0: ptp_icli_ptp_cal_show_port(q, ptp_port_calibration.port_calibrations[q].port_latencies_10m_cu);  break;
                            case 1: ptp_icli_ptp_cal_show_port(q, ptp_port_calibration.port_calibrations[q].port_latencies_100m_cu); break;
                            case 2: ptp_icli_ptp_cal_show_port(q, ptp_port_calibration.port_calibrations[q].port_latencies_1g_cu); break;
                            case 3: ptp_icli_ptp_cal_show_port(q, ptp_port_calibration.port_calibrations[q].port_latencies_1g);  break;
                            case 4: ptp_icli_ptp_cal_show_port(q, ptp_port_calibration.port_calibrations[q].port_latencies_2g5); break;
                            case 5: ptp_icli_ptp_cal_show_port(q, ptp_port_calibration.port_calibrations[q].port_latencies_5g); break;
                            case 6: ptp_icli_ptp_cal_show_port(q, ptp_port_calibration.port_calibrations[q].port_latencies_10g); break;
                        }
                    }
                }    
            }
        }
        ICLI_PRINTF("\n");
    }

    return ICLI_RC_OK;
}

icli_rc_t ptp_icli_local_clock_set(i32 session_id, int clockinst, BOOL has_update, BOOL has_ratio, i32 ratio)
{
    mesa_timestamp_t t;
    T_IG(VTSS_TRACE_GRP_PTP_ICLI, "clockinst %d has_update %d has_ratio %d ratio  %d", clockinst, has_update, has_ratio, ratio);

    if (has_update) {
        /* update the local clock to the system clock */
        t.sec_msb = 0;
        t.seconds = time(NULL);
        t.nanoseconds = 0;
        ptp_local_clock_time_set(&t, ptp_instance_2_timing_domain(clockinst));
    }
    if (has_ratio) {
        /* set the local clock master ratio */
        if (ratio == 0) {
            vtss_local_clock_ratio_clear(ptp_instance_2_timing_domain(clockinst));
        } else {
            vtss_local_clock_ratio_set((ratio<<16)/10, ptp_instance_2_timing_domain(clockinst));
        }
    }
    return ICLI_RC_OK;
}

icli_rc_t ptp_icli_show_filter_type(i32 session_id, int clockinst)
{
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    
    if (vtss_appl_ptp_clock_config_default_ds_get(clockinst, &default_ds_cfg) == VTSS_RC_OK) {
        ICLI_PRINTF("Clockinst: %d, filter type: %s\n", clockinst, filter_type_to_str(default_ds_cfg.filter_type));
    } else {
        ICLI_PRINTF("Failed reading filter type for clockinst %d\n", clockinst);
        return ICLI_RC_ERROR;
    }
    return ICLI_RC_OK;
}

icli_rc_t ptp_icli_local_clock_show(i32 session_id, int clockinst)
{
    ptp_show_local_clock(clockinst, icli_session_self_printf);
    return ICLI_RC_OK;
}

icli_rc_t ptp_icli_slave_cfg_set(i32 session_id, int clockinst, BOOL has_stable_offset, u32 stable_offset, BOOL has_offset_ok, u32 offset_ok, BOOL has_offset_fail, u32 offset_fail)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_appl_ptp_clock_slave_config_t slave_cfg;

    T_IG(VTSS_TRACE_GRP_PTP_ICLI, "clockinst %d, has_stable_offset %d, stable_offset %d, has_offset_ok %d, offset_ok %d, has_offset_fail %d, offset_fail %d", clockinst, has_stable_offset, stable_offset, has_offset_ok, offset_ok, has_offset_fail, offset_fail);
    if (vtss_appl_ptp_clock_slave_config_get(clockinst, &slave_cfg) == VTSS_RC_OK) {
        if (has_stable_offset) slave_cfg.stable_offset = stable_offset;
        if (has_offset_ok) slave_cfg.offset_ok = offset_ok;
        if (has_offset_fail) slave_cfg.offset_fail = offset_fail;
        if (vtss_appl_ptp_clock_slave_config_set(clockinst, &slave_cfg) != VTSS_RC_OK) {
            ICLI_PRINTF("Failed setting slave-cfg\n");
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Clock instance %d : does not exist\n", clockinst);
    }
    return rc;
}

icli_rc_t ptp_icli_slave_cfg_show(i32 session_id, int clockinst)
{
    ptp_show_slave_cfg(clockinst, icli_session_self_printf);
    return ICLI_RC_OK;
}

icli_rc_t ptp_icli_slave_table_unicast_show(i32 session_id, int clockinst)
{
    ptp_show_clock_slave_table_unicast_ds(clockinst, icli_session_self_printf);
    return ICLI_RC_OK;
}

icli_rc_t ptp_icli_virtual_port_show(i32 session_id, int clockinst)
{
    icli_rc_t rc = ICLI_RC_ERROR;
    if (clockinst < PTP_CLOCK_INSTANCES) {
        vtss_appl_ptp_virtual_port_config_t c;
        if (ptp_clock_config_virtual_port_config_get(clockinst, &c) == VTSS_RC_OK) {
            ICLI_PRINTF("clockinst %d, class %d, accuracy %d, variance %d, localPriority %d, priority1 %d, priority2 %d, io-pin %d, enable %s\n", clockinst, c.clockQuality.clockClass, c.clockQuality.clockAccuracy, c.clockQuality.offsetScaledLogVariance, c.localPriority, c.priority1, c.priority2, c.io_pin, c.enable ? "TRUE" : "FALSE");
            rc = ICLI_RC_OK;
        }
    } else {
        ICLI_PRINTF("Clock instance %d : does not exist\n", clockinst);
    }
    return rc;
}

//  see ptp_icli_function
icli_rc_t ptp_icli_wireless_mode_set(i32 session_id, int clockinst, BOOL enable, icli_stack_port_range_t *port_type_list_p)
{
    icli_rc_t rc =  ICLI_RC_OK;
    T_IG(VTSS_TRACE_GRP_PTP_ICLI, "Wireless mode set clockinst %d, enable %d", clockinst, enable);
    if (enable) {
        rc = ptp_icli_traverse_ports(session_id, clockinst, port_type_list_p, icli_wireless_mode_enable);
    } else {
        rc = ptp_icli_traverse_ports(session_id, clockinst, port_type_list_p, icli_wireless_mode_disable);
    }
    return rc;
}

icli_rc_t ptp_icli_wireless_pre_notification(i32 session_id, int clockinst, icli_stack_port_range_t *port_type_list_p)
{
    icli_rc_t rc =  ICLI_RC_OK;
    T_IG(VTSS_TRACE_GRP_PTP_ICLI, "Wireless mode pre clockinst %d", clockinst);
    rc = ptp_icli_traverse_ports(session_id, clockinst, port_type_list_p, icli_wireless_pre_not);
    return rc;
}

static icli_rc_t my_port_wireless_delay_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_ptp_delay_cfg_t *my_cfg = (vtss_ptp_delay_cfg_t *)cfg;

    if (!ptp_port_wireless_delay_set(my_cfg, uport, inst)) {
        ICLI_PRINTF("Wireless mode not available for ptp instance %d, port %u\n", inst, uport);
        ICLI_PRINTF("Wireless mode requires a two-step or Oam based BC\n");
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_wireless_delay(i32 session_id, int clockinst, i32 base_delay, i32 incr_delay, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_ptp_delay_cfg_t delay_cfg;
    T_IG(VTSS_TRACE_GRP_PTP_ICLI, "clockinst %d, base_delay %d, incr_delay %d", clockinst, base_delay, incr_delay);
    delay_cfg.base_delay = ((mesa_timeinterval_t)base_delay)*0x10000/1000;
    delay_cfg.incr_delay = ((mesa_timeinterval_t)incr_delay)*0x10000/1000;
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &delay_cfg, my_port_wireless_delay_set);
    return rc;
}

icli_rc_t ptp_icli_mode(i32 session_id, int clockinst,
                        BOOL has_boundary, BOOL has_e2etransparent, BOOL has_p2ptransparent, BOOL has_master, BOOL has_slave, BOOL has_bcfrontend,
                        BOOL has_onestep, BOOL has_twostep,
                        BOOL has_ethernet, BOOL has_ethernet_mixed, BOOL has_ip4multi, BOOL has_ip4mixed, BOOL has_ip4unicast, BOOL has_oam, BOOL has_onepps,
                        BOOL has_oneway, BOOL has_twoway,
                        BOOL has_id, icli_clock_id_t *v_clock_id,
                        BOOL has_vid, u32 vid, u32 prio, BOOL has_mep, u32 mep_id,
                        BOOL has_profile, BOOL has_ieee1588, BOOL has_g8265_1, BOOL has_g8275_1, BOOL has_802_1as,
                        BOOL has_clock_domain, u32 clock_domain, BOOL has_dscp, u32 dscp_id)
{
    icli_rc_t rc = ICLI_RC_ERROR;
    vtss_appl_ptp_clock_config_default_ds_t init_default_ds;
    vtss_appl_ptp_clock_status_default_ds_t init_default_ds_status;
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    vtss_appl_ptp_clock_status_default_ds_t default_ds_status;
    mesa_rc my_rc;
    vtss_appl_ptp_config_port_ds_t port_cfg;
    vtss_appl_ptp_config_port_ds_t new_port_cfg;
    vtss_ifindex_t ifindex;
    port_iter_t pit;
    u32 port_no;
    
    if (!MESA_CAP(MESA_CAP_TS_MISSING_PTP_ON_INTERNAL_PORTS)) {
        if (has_oam) {
            ICLI_PRINTF("OAM encapsulation is only possible in Serval\n");
            return rc;
        }
    }

    vtss_appl_ptp_profile_t profile = has_profile ? (has_ieee1588 ?  VTSS_APPL_PTP_PROFILE_1588 :
                                      has_g8265_1 ? VTSS_APPL_PTP_PROFILE_G_8265_1 :
                                      has_g8275_1 ? VTSS_APPL_PTP_PROFILE_G_8275_1 :
                                      has_802_1as ? VTSS_APPL_PTP_PROFILE_IEEE_802_1AS : VTSS_APPL_PTP_PROFILE_NO_PROFILE) : VTSS_APPL_PTP_PROFILE_NO_PROFILE;

    ptp_get_default_clock_default_ds(&init_default_ds_status, &init_default_ds);
    ptp_apply_profile_defaults_to_default_ds(&init_default_ds, profile);

    init_default_ds.deviceType = has_boundary ? VTSS_APPL_PTP_DEVICE_ORD_BOUND : has_e2etransparent ? VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT :
                             has_p2ptransparent ? VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT : has_master ? VTSS_APPL_PTP_DEVICE_MASTER_ONLY :
                             has_slave ? VTSS_APPL_PTP_DEVICE_SLAVE_ONLY : has_bcfrontend ? VTSS_APPL_PTP_DEVICE_BC_FRONTEND :
                             VTSS_APPL_PTP_DEVICE_NONE;

    init_default_ds.oneWay = has_oneway ? true : has_twoway ? false : init_default_ds.oneWay;
    init_default_ds.twoStepFlag = has_onestep ? false : has_twostep ? true : init_default_ds.twoStepFlag;

    init_default_ds.protocol = has_ethernet ? VTSS_APPL_PTP_PROTOCOL_ETHERNET : has_ethernet_mixed ? VTSS_APPL_PTP_PROTOCOL_ETHERNET_MIXED :
                           has_ip4multi ? VTSS_APPL_PTP_PROTOCOL_IP4MULTI : has_ip4mixed ? VTSS_APPL_PTP_PROTOCOL_IP4MIXED :
                           has_ip4unicast ? VTSS_APPL_PTP_PROTOCOL_IP4UNI : has_oam ? VTSS_APPL_PTP_PROTOCOL_OAM :
                           has_onepps ? VTSS_APPL_PTP_PROTOCOL_ONE_PPS : VTSS_APPL_PTP_PROTOCOL_ETHERNET;

    init_default_ds.configured_vid = has_vid ? vid : init_default_ds.configured_vid;
    init_default_ds.configured_pcp = has_vid ? prio : init_default_ds.configured_pcp;
    init_default_ds.mep_instance = has_mep ? mep_id : init_default_ds.mep_instance;
    init_default_ds.dscp = has_dscp ? dscp_id : init_default_ds.dscp;
    init_default_ds.clock_domain = has_clock_domain ? clock_domain : init_default_ds.clock_domain + clockinst;
    //init_ds.cfg.clock_domain = init_default_ds.clock_domain;
    init_default_ds.profile = profile;
    
    if (has_id) {
        memcpy(init_default_ds_status.clockIdentity, v_clock_id, sizeof(init_default_ds_status.clockIdentity));
    } else {
        init_default_ds_status.clockIdentity[7] += clockinst;
    }

    if ((vtss_appl_ptp_clock_config_default_ds_get(clockinst, &default_ds_cfg) == VTSS_RC_OK) &&
        (vtss_appl_ptp_clock_status_default_ds_get(clockinst, &default_ds_status) == VTSS_RC_OK))
    {
        // clock instance already exists
        if (default_ds_cfg.deviceType != init_default_ds.deviceType) {
            ICLI_PRINTF("Cannot create clock instance %d. A clock of another type (%s) already exists with that instance number.\n", clockinst, DeviceTypeToString(default_ds_cfg.deviceType));
        }
        else {
            if (memcmp(init_default_ds_status.clockIdentity, default_ds_status.clockIdentity, 8) == 0) {
                if (vtss_appl_ptp_clock_config_default_ds_set(clockinst, &init_default_ds) != VTSS_RC_OK) {
                    ICLI_PRINTF("There was a problem updating the clock.\n");
                }
                else rc = ICLI_RC_OK;
            }
            else {
                ICLI_PRINTF("Cannot modify the ID of an existing clock - please delete the clock and create it again!\n");
            }
        }
    }
    else {
        // clock instance does not exist: create a new
        //init_ds.cfg = init_default_ds.cfg;
        //memcpy(init_ds.clockIdentity, init_default_ds_status.clockIdentity, sizeof(init_ds.clockIdentity));
        if ((my_rc = ptp_clock_clockidentity_set(clockinst, &init_default_ds_status.clockIdentity)) != VTSS_RC_OK) {
            ICLI_PRINTF("Cannot set clock identity: %s\n", error_txt(my_rc));
        }
        T_IG(VTSS_TRACE_GRP_PTP_ICLI, "clockinst %d, device_type %s", clockinst, DeviceTypeToString(init_default_ds.deviceType));
        if ((my_rc = vtss_appl_ptp_clock_config_default_ds_set(clockinst, &init_default_ds)) != VTSS_RC_OK) {
        //if ((my_rc = ptp_clock_create(&init_ds, clockinst)) != VTSS_RC_OK) {
            ICLI_PRINTF("Cannot create clock: %s\n", error_txt(my_rc));
        }
        else rc = ICLI_RC_OK;
    }
    if (rc == ICLI_RC_OK) {
        /* Apply profile defaults to all ports of this clock (whether enabled or not) */
        port_iter_init_local(&pit);
        while (port_iter_getnext(&pit)) {
            (void) vtss_ifindex_from_port(0, pit.iport, &ifindex);
            (void) ptp_ifindex_to_port(ifindex, &port_no);
    
            if (vtss_appl_ptp_config_clocks_port_ds_get(clockinst, ifindex, &port_cfg) == VTSS_RC_OK) {
                new_port_cfg = port_cfg;
                ptp_apply_profile_defaults_to_port_ds(&new_port_cfg, profile);
    
                if (memcmp(&new_port_cfg, &port_cfg, sizeof(vtss_appl_ptp_config_port_ds_t)) != 0) {
                    if (vtss_appl_ptp_config_clocks_port_ds_set(clockinst, ifindex, &new_port_cfg) == VTSS_RC_ERROR) {
                        T_D("Clock instance %d : does not exist", clockinst);
                    }
                }
            }
        }
    }
    return rc;
}

//  see ptp_icli_functions.h

BOOL ptp_icli_runtime_synce_present(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    switch (ask) {
        case ICLI_ASK_PRESENT:
#if defined(VTSS_SW_OPTION_SYNCE)
            runtime->present = TRUE;
#else
            runtime->present = FALSE;
#endif
            return TRUE;
        default:
            break;
    }
    return FALSE;
}

BOOL ptp_icli_runtime_zls30380_present(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    vtss_zarlink_servo_t servo_type = (vtss_zarlink_servo_t)MESA_CAP(VTSS_APPL_CAP_ZARLINK_SERVO_TYPE);
    switch (ask) {
        case ICLI_ASK_PRESENT:
            if (servo_type == VTSS_ZARLINK_SERVO_ZLS30380) {
                runtime->present = TRUE;
            } else {
                runtime->present = FALSE;
            }
            return TRUE;
        default:
            break;
    }
    return FALSE;
}

BOOL ptp_icli_runtime_zls30380_or_zls30384_present(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    vtss_zarlink_servo_t servo_type = (vtss_zarlink_servo_t)MESA_CAP(VTSS_APPL_CAP_ZARLINK_SERVO_TYPE);
    switch (ask) {
        case ICLI_ASK_PRESENT:
            if (servo_type == VTSS_ZARLINK_SERVO_ZLS30380 || servo_type == VTSS_ZARLINK_SERVO_ZLS30384) {
                runtime->present = TRUE;
            } else {
                runtime->present = FALSE;
            }
            return TRUE;
        default:
            break;
    }
    return FALSE;
}

BOOL ptp_icli_runtime_zls30380_or_zls30384_or_zls30386_or_zls30387_present(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    switch (ask) {
        case ICLI_ASK_PRESENT:
#if defined(VTSS_SW_OPTION_ZLS30387)
            runtime->present = TRUE;
#else
            runtime->present = FALSE;
#endif // defined(VTSS_SW_OPTION_ZLS30387)
            return TRUE;
        default:
            break;
    }
    return FALSE;
}

BOOL ptp_icli_runtime_zls30380_or_zls30384_or_zls30386_present(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    vtss_zarlink_servo_t servo_type = (vtss_zarlink_servo_t)MESA_CAP(VTSS_APPL_CAP_ZARLINK_SERVO_TYPE);
    switch (ask) {
        case ICLI_ASK_PRESENT:
            if (servo_type == VTSS_ZARLINK_SERVO_ZLS30380 || servo_type == VTSS_ZARLINK_SERVO_ZLS30384) {
                runtime->present = TRUE;
            } else {
                runtime->present = FALSE;
            }
            return TRUE;
        default:
            break;
    }
    return FALSE;
}

BOOL ptp_icli_runtime_zls30380_or_zls30384_or_zls30387_present(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    switch (ask) {
        case ICLI_ASK_PRESENT:
#if defined(VTSS_SW_OPTION_ZLS30387)
            runtime->present = TRUE;
#else
            runtime->present = FALSE;
#endif // defined(VTSS_SW_OPTION_ZLS30387)
            return TRUE;
        default:
            break;
    }
    return FALSE;
}

BOOL ptp_icli_runtime_8275_1_present(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    switch (ask) {
        case ICLI_ASK_PRESENT:
            runtime->present = TRUE;
            return TRUE;
        default:
            break;
    }
    return FALSE;
}

BOOL ptp_icli_runtime_802_1as_present(u32                session_id,
                                        icli_runtime_ask_t ask,
                                        icli_runtime_t     *runtime)
{
    switch (ask) {
        case ICLI_ASK_PRESENT:



            runtime->present = FALSE;

            return TRUE;
        default:
            break;
    }
    return FALSE;
}

BOOL ptp_icli_runtime_has_phy_timestamp_capability(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    switch (ask) {
        case ICLI_ASK_PRESENT:
            if (MESA_CAP(MESA_CAP_PHY_TS)) {
                runtime->present = TRUE;
            } else {
                runtime->present = FALSE;
            }
            return TRUE;
        default:
            break;
    }
    return FALSE;
}

BOOL ptp_icli_runtime_pps_via_configrable_io_pins(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    switch (ask) {
        case ICLI_ASK_PRESENT:
            if (MESA_CAP(MESA_CAP_TS_PPS_VIA_CONFIGURABLE_IO_PINS)) {
                runtime->present = TRUE;
            } else {
                runtime->present = FALSE;
            }
            return TRUE;
        default:
            break;
    }
    return FALSE;
}

BOOL ptp_icli_runtime_has_single_dpll_mode_capability(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    switch (ask) {
        case ICLI_ASK_PRESENT:
            if (MESA_CAP(MEBA_CAP_SYNCE_DPLL_MODE_SINGLE)) {
                runtime->present = TRUE;
            } else {
                runtime->present = FALSE;
            }
            return TRUE;
        default:
            break;
    }
    return FALSE;
}

BOOL ptp_icli_runtime_has_dual_dpll_mode_capability(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    switch (ask) {
        case ICLI_ASK_PRESENT:
            if (MESA_CAP(MEBA_CAP_SYNCE_DPLL_MODE_DUAL)) {
                runtime->present = TRUE;
            } else {
                runtime->present = FALSE;
            }
            return TRUE;
        default:
            break;
    }
    return FALSE;
}

icli_rc_t ptp_icli_no_mode(i32 session_id, int clockinst, BOOL has_boundary, BOOL has_e2etransparent, BOOL has_p2ptransparent, BOOL has_master, BOOL has_slave, BOOL has_bcfrontend)
{
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_appl_ptp_device_type_t device_type = has_boundary ? VTSS_APPL_PTP_DEVICE_ORD_BOUND : has_e2etransparent ? VTSS_APPL_PTP_DEVICE_E2E_TRANSPARENT :
                                              has_p2ptransparent ? VTSS_APPL_PTP_DEVICE_P2P_TRANSPARENT : has_master ? VTSS_APPL_PTP_DEVICE_MASTER_ONLY :
                                              has_slave ? VTSS_APPL_PTP_DEVICE_SLAVE_ONLY : has_bcfrontend ? VTSS_APPL_PTP_DEVICE_BC_FRONTEND :
                                              VTSS_APPL_PTP_DEVICE_NONE;
    T_IG(VTSS_TRACE_GRP_PTP_ICLI, "clockinst %d, device_type %s", clockinst, DeviceTypeToString(device_type));
    if (vtss_appl_ptp_clock_config_default_ds_get(clockinst, &default_ds_cfg) == VTSS_RC_OK) {
        if (default_ds_cfg.deviceType == device_type) {
            if (vtss_appl_ptp_clock_config_default_ds_del(clockinst) != VTSS_RC_OK) {
                ICLI_PRINTF("Cannot delete clock instance %d (may be that another clock instance depends on it)\n", clockinst);
                rc = ICLI_RC_ERROR;
            }
        } else {
            ICLI_PRINTF("Cannot delete clock instance %d : it is not a %s clock type\n", clockinst, DeviceTypeToString(device_type));
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Clock instance %d : does not exist\n", clockinst);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_virtual_port_class_set(i32 session_id, int clockinst, u8 ptpclass)
{
    return ptp_set_virtual_port_clock_class(clockinst, ptpclass) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_virtual_port_accuracy_set(i32 session_id, int clockinst, u8 ptpaccuracy)
{
    return ptp_set_virtual_port_clock_accuracy(clockinst, ptpaccuracy) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_virtual_port_variance_set(i32 session_id, int clockinst, u16 ptpvariance)
{
    return ptp_set_virtual_port_clock_variance(clockinst, ptpvariance) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_virtual_port_local_priority_set(i32 session_id, int clockinst, u8 localPriority)
{
    return ptp_set_virtual_port_local_priority(clockinst, localPriority) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_virtual_port_priority1_set(i32 session_id, int clockinst, u8 priority1)
{
    return ptp_set_virtual_port_priority1(clockinst, priority1) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_virtual_port_priority2_set(i32 session_id, int clockinst, u8 priority2)
{
    return ptp_set_virtual_port_priority2(clockinst, priority2) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_virtual_port_io_pin_set(i32 session_id, int clockinst, u16 io_pin, BOOL enable)
{
    return ptp_set_virtual_port_io_pin(clockinst, io_pin, enable) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_virtual_port_class_clear(i32 session_id, int clockinst)
{
    return ptp_clear_virtual_port_clock_class(clockinst) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_virtual_port_accuracy_clear(i32 session_id, int clockinst)
{
    return ptp_clear_virtual_port_clock_accuracy(clockinst) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_virtual_port_variance_clear(i32 session_id, int clockinst)
{
    return ptp_clear_virtual_port_clock_variance(clockinst) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_virtual_port_local_priority_clear(i32 session_id, int clockinst)
{
    return ptp_clear_virtual_port_local_priority(clockinst) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_virtual_port_priority1_clear(i32 session_id, int clockinst)
{
    return ptp_clear_virtual_port_priority1(clockinst) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_virtual_port_priority2_clear(i32 session_id, int clockinst)
{
    return ptp_clear_virtual_port_priority2(clockinst) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_debug_class_set(i32 session_id, int clockinst, u8 ptpclass)
{
    return ptp_set_clock_class(clockinst, ptpclass) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_debug_accuracy_set(i32 session_id, int clockinst, u8 ptpaccuracy)
{
    return ptp_set_clock_accuracy(clockinst, ptpaccuracy) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_debug_variance_set(i32 session_id, int clockinst, u16 ptpvariance)
{
    return ptp_set_clock_variance(clockinst, ptpvariance) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_debug_class_clear(i32 session_id, int clockinst)
{
    return ptp_clear_clock_class(clockinst) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_debug_accuracy_clear(i32 session_id, int clockinst)
{
    return ptp_clear_clock_accuracy(clockinst) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_debug_variance_clear(i32 session_id, int clockinst)
{
    return ptp_clear_clock_variance(clockinst) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
}

//  see ptp_icli_functions.h
icli_rc_t ptp_icli_priority1_set(i32 session_id, int clockinst, u8 priority1)
{
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    icli_rc_t rc =  ICLI_RC_OK;

    T_IG(VTSS_TRACE_GRP_PTP_ICLI, "Priority1 set clockinst %d, priority1 %d", clockinst, priority1);
    if (vtss_appl_ptp_clock_config_default_ds_get(clockinst, &default_ds_cfg) == VTSS_RC_OK) {
        default_ds_cfg.priority1 = priority1;

        if (vtss_appl_ptp_clock_config_default_ds_set(clockinst, &default_ds_cfg) != VTSS_RC_OK) {
            T_IG(VTSS_TRACE_GRP_PTP_ICLI, "Clock instance %d : does not exist", clockinst);
            rc = ICLI_RC_ERROR;
        }
    }
    return rc;
}

icli_rc_t ptp_icli_priority2_set(i32 session_id, int clockinst, u8 priority2)
{
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    icli_rc_t rc =  ICLI_RC_OK;

    T_IG(VTSS_TRACE_GRP_PTP_ICLI, "Priority2 set clockinst %d, priority2 %d", clockinst, priority2);
    if (vtss_appl_ptp_clock_config_default_ds_get(clockinst, &default_ds_cfg) == VTSS_RC_OK) {
        default_ds_cfg.priority2 = priority2;

        if (vtss_appl_ptp_clock_config_default_ds_set(clockinst, &default_ds_cfg) != VTSS_RC_OK) {
            T_IG(VTSS_TRACE_GRP_PTP_ICLI, "Clock instance %d : does not exist", clockinst);
            rc = ICLI_RC_ERROR;
        }
    }
    return rc;
}

//  see ptp_icli_functions.h
icli_rc_t ptp_icli_local_priority_set(i32 session_id, int clockinst, u8 localpriority)
{
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    icli_rc_t rc =  ICLI_RC_OK;

    T_IG(VTSS_TRACE_GRP_PTP_ICLI, "Local Priority set clockinst %d, priority %d", clockinst, localpriority);
    if (vtss_appl_ptp_clock_config_default_ds_get(clockinst, &default_ds_cfg) == VTSS_RC_OK) {
        default_ds_cfg.localPriority = localpriority;

        if (vtss_appl_ptp_clock_config_default_ds_set(clockinst, &default_ds_cfg) != VTSS_RC_OK) {
            T_IG(VTSS_TRACE_GRP_PTP_ICLI, "Clock instance %d : does not exist", clockinst);
            rc = ICLI_RC_ERROR;
        }
    }
    return rc;
}

//  see ptp_icli_functions.h
icli_rc_t ptp_icli_path_trace_set(i32 session_id, int clockinst, BOOL enable)
{
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    icli_rc_t rc =  ICLI_RC_OK;

    T_IG(VTSS_TRACE_GRP_PTP_ICLI, "Path trace set clockinst %d, enable %s", clockinst, enable ? "TRUE" : "FALSE");
    if (vtss_appl_ptp_clock_config_default_ds_get(clockinst, &default_ds_cfg) == VTSS_RC_OK) {
        default_ds_cfg.path_trace_enable = enable;

        if (vtss_appl_ptp_clock_config_default_ds_set(clockinst, &default_ds_cfg) != VTSS_RC_OK) {
            T_IG(VTSS_TRACE_GRP_PTP_ICLI, "Clock instance %d : does not exist", clockinst);
            rc = ICLI_RC_ERROR;
        }
    }
    return rc;
}

icli_rc_t ptp_icli_domain_set(i32 session_id, int clockinst, u8 domain)
{
    vtss_appl_ptp_clock_config_default_ds_t default_ds_cfg;
    icli_rc_t rc =  ICLI_RC_OK;

    T_IG(VTSS_TRACE_GRP_PTP_ICLI, "Domain set clockinst %d, domain %d", clockinst, domain);
    if (vtss_appl_ptp_clock_config_default_ds_get(clockinst, &default_ds_cfg) == VTSS_RC_OK) {
        default_ds_cfg.domainNumber = domain;

        if (vtss_appl_ptp_clock_config_default_ds_set(clockinst, &default_ds_cfg) != VTSS_RC_OK) {
            T_IG(VTSS_TRACE_GRP_PTP_ICLI, "Clock instance %d : does not exist", clockinst);
            rc = ICLI_RC_ERROR;
        }
    }
    return rc;
}

icli_rc_t ptp_icli_time_property_set(i32 session_id, int clockinst, BOOL has_utc_offset, i32 utc_offset, BOOL has_valid,
                                     BOOL has_leapminus_59, BOOL has_leapminus_61, BOOL has_time_traceable,
                                     BOOL has_freq_traceable, BOOL has_ptptimescale,
                                     BOOL has_time_source, u8 time_source,
                                     BOOL has_leap_pending, char *date_string, BOOL has_leaptype_59, BOOL has_leaptype_61)
{
    vtss_appl_ptp_clock_timeproperties_ds_t prop;
    icli_rc_t rc =  ICLI_RC_OK;
    T_IG(VTSS_TRACE_GRP_PTP_ICLI, "icli timeproperty instance %d", clockinst);
    ptp_clock_default_timeproperties_ds_get(&prop);
    if (has_utc_offset)  prop.currentUtcOffset = utc_offset;
    prop.currentUtcOffsetValid = has_valid;
    prop.leap59 = has_leapminus_59;
    prop.leap61 = has_leapminus_61;
    prop.timeTraceable = has_time_traceable;
    prop.frequencyTraceable = has_freq_traceable;
    prop.ptpTimescale = has_ptptimescale;
    if (has_time_source) prop.timeSource = time_source;
    if (has_leap_pending) {
        prop.pendingLeap = true;
        (void)extract_date(date_string, &prop.leapDate);
        prop.leapType = has_leaptype_59 ? VTSS_APPL_PTP_LEAP_SECOND_59 : VTSS_APPL_PTP_LEAP_SECOND_61;
    }
    if (vtss_appl_ptp_clock_config_timeproperties_ds_set(clockinst, &prop) != VTSS_RC_OK) {
        ICLI_PRINTF("Error writing timing properties instance %d\n", clockinst);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_filter_type_set(i32 session_id, int clockinst, BOOL has_aci_default, BOOL has_aci_freq_xo,
                                   BOOL has_aci_phase_xo, BOOL has_aci_freq_tcxo, BOOL has_aci_phase_tcxo,
                                   BOOL has_aci_freq_ocxo_s3e, BOOL has_aci_phase_ocxo_s3e, BOOL has_aci_bc_partial_on_path_freq,
                                   BOOL has_aci_bc_partial_on_path_phase, BOOL has_aci_bc_partial_on_path_phase_synce,
                                   BOOL has_aci_bc_full_on_path_freq, BOOL has_aci_bc_full_on_path_phase,
                                   BOOL has_aci_bc_full_on_path_phase_synce, BOOL has_aci_freq_accuracy_fdd,
                                   BOOL has_aci_freq_accuracy_xdsl, BOOL has_aci_elec_freq, BOOL has_aci_elec_phase,
                                   BOOL has_aci_phase_tcxo_q, BOOL has_aci_bc_full_on_path_phase_q,
                                   BOOL has_aci_phase_relaxed_c60w, BOOL has_aci_phase_relaxed_c150,
                                   BOOL has_aci_phase_relaxed_c180, BOOL has_aci_phase_relaxed_c240,
                                   BOOL has_aci_bc_full_on_path_phase_beta, BOOL has_aci_phase_ocxo_s3e_r4_6_1,
                                   BOOL has_aci_basic_phase, BOOL has_aci_basic_phase_synce,
                                   BOOL has_aci_basic_phase_low, BOOL has_aci_basic_phase_low_synce)
{
    vtss_appl_ptp_clock_config_default_ds_t clock_config;
    
    if (vtss_appl_ptp_clock_config_default_ds_get(clockinst, &clock_config) == VTSS_RC_ERROR) {
        return ICLI_RC_ERROR;
    } else {
        u32 new_filter_type = PTP_FILTERTYPE_ACI_BASIC_PHASE;

        if (has_aci_default)
            new_filter_type = PTP_FILTERTYPE_ACI_DEFAULT;
        else if (has_aci_freq_xo)
            new_filter_type = PTP_FILTERTYPE_ACI_FREQ_XO;
        else if (has_aci_phase_xo)
            new_filter_type = PTP_FILTERTYPE_ACI_PHASE_XO;
        else if (has_aci_freq_tcxo)
            new_filter_type = PTP_FILTERTYPE_ACI_FREQ_TCXO;
        else if (has_aci_phase_tcxo)
            new_filter_type = PTP_FILTERTYPE_ACI_PHASE_TCXO;
        else if (has_aci_freq_ocxo_s3e)
            new_filter_type = PTP_FILTERTYPE_ACI_FREQ_OCXO_S3E;
        else if (has_aci_phase_ocxo_s3e)
            new_filter_type = PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E;
        else if (has_aci_bc_partial_on_path_freq)
            new_filter_type = PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_FREQ;
        else if (has_aci_bc_partial_on_path_phase)
            new_filter_type = PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_PHASE;
        else if (has_aci_bc_partial_on_path_phase_synce)
            new_filter_type = PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_PHASE;
        else if (has_aci_bc_full_on_path_freq)
            new_filter_type = PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_FREQ;
        else if (has_aci_bc_full_on_path_phase)
            new_filter_type = PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE;
        else if (has_aci_bc_full_on_path_phase_synce)
            new_filter_type = PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE;
        else if (has_aci_freq_accuracy_fdd)
            new_filter_type = PTP_FILTERTYPE_ACI_FREQ_ACCURACY_FDD;
        else if (has_aci_freq_accuracy_xdsl)
            new_filter_type = PTP_FILTERTYPE_ACI_FREQ_ACCURACY_XDSL;
        else if (has_aci_elec_freq)
            new_filter_type = PTP_FILTERTYPE_ACI_ELEC_FREQ;
        else if (has_aci_elec_phase)
            new_filter_type = PTP_FILTERTYPE_ACI_ELEC_PHASE;
        else if (has_aci_phase_tcxo_q)
            new_filter_type = PTP_FILTERTYPE_ACI_PHASE_TCXO_Q;
        else if (has_aci_bc_full_on_path_phase_q)
            new_filter_type = PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_Q;
        else if (has_aci_phase_relaxed_c60w)
            new_filter_type = PTP_FILTERTYPE_ACI_PHASE_RELAXED_C60W;
        else if (has_aci_phase_relaxed_c150)
            new_filter_type = PTP_FILTERTYPE_ACI_PHASE_RELAXED_C150;
        else if (has_aci_phase_relaxed_c180)
            new_filter_type = PTP_FILTERTYPE_ACI_PHASE_RELAXED_C180;
        else if (has_aci_phase_relaxed_c240)
            new_filter_type = PTP_FILTERTYPE_ACI_PHASE_RELAXED_C240;
        else if (has_aci_bc_full_on_path_phase_beta)
            new_filter_type = PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_BETA;
        else if (has_aci_phase_ocxo_s3e_r4_6_1)
            new_filter_type = PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E_R4_6_1;
        else if (has_aci_basic_phase)
            new_filter_type = PTP_FILTERTYPE_ACI_BASIC_PHASE;
        else if (has_aci_basic_phase_synce)
            new_filter_type = PTP_FILTERTYPE_ACI_BASIC_PHASE;
        else if (has_aci_basic_phase_low)
            new_filter_type = PTP_FILTERTYPE_ACI_BASIC_PHASE_LOW;
        else if (has_aci_basic_phase_low_synce)
            new_filter_type = PTP_FILTERTYPE_ACI_BASIC_PHASE_LOW;

        if (new_filter_type == clock_config.filter_type) {
            return ICLI_RC_OK;
        }
        clock_config.filter_type = new_filter_type;
        return vtss_appl_ptp_clock_config_default_ds_set(clockinst, &clock_config) == VTSS_RC_OK ? ICLI_RC_OK : ICLI_RC_ERROR;
    }
}

icli_rc_t ptp_icli_servo_clear(i32 session_id, int clockinst)
{
    vtss_appl_ptp_clock_servo_config_t default_servo;
    icli_rc_t rc =  ICLI_RC_OK;

    if (vtss_appl_ptp_clock_servo_parameters_get(clockinst, &default_servo) == VTSS_RC_OK) {
        if (vtss_appl_ptp_clock_servo_clear(clockinst) == VTSS_RC_ERROR) {
            ICLI_PRINTF("Error resetting servo, instance %d\n", clockinst);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Clock instance %d : does not exist\n", clockinst);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_servo_displaystate_set(i32 session_id, int clockinst, BOOL enable)
{
    vtss_appl_ptp_clock_servo_config_t default_servo;
    icli_rc_t rc =  ICLI_RC_OK;

    if (vtss_appl_ptp_clock_servo_parameters_get(clockinst, &default_servo) == VTSS_RC_OK) {
        default_servo.display_stats = enable;
        if (vtss_appl_ptp_clock_servo_parameters_set(clockinst, &default_servo) == VTSS_RC_ERROR) {
            ICLI_PRINTF("Error writing servo parameters, instance %d\n", clockinst);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Clock instance %d : does not exist\n", clockinst);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_clock_servo_options_set(i32 session_id, int clockinst, BOOL synce, u32 threshold, u32 ap)
{
    vtss_appl_ptp_clock_servo_config_t default_servo;
    icli_rc_t rc =  ICLI_RC_OK;

    if (vtss_appl_ptp_clock_servo_parameters_get(clockinst, &default_servo) == VTSS_RC_OK) {
        default_servo.srv_option = (synce == 1) ? VTSS_APPL_PTP_CLOCK_SYNCE : VTSS_APPL_PTP_CLOCK_FREE;
        if (synce != 0) {  // Only update synce related parameters when servo mode is set to synce (i.e. ignore values when mode is being set to free running).
            default_servo.synce_threshold = threshold;
            default_servo.synce_ap = ap;
        }    
        if (vtss_appl_ptp_clock_servo_parameters_set(clockinst, &default_servo) == VTSS_RC_ERROR) {
            ICLI_PRINTF("Error writing servo parameters, instance %d\n", clockinst);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Clock instance %d : does not exist\n", clockinst);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_clock_unicast_conf_set(i32 session_id, int clockinst, int idx, BOOL has_duration, u32 duration, u32 ip)
{
    vtss_appl_ptp_unicast_slave_config_t uni_slave;
    icli_rc_t rc =  ICLI_RC_OK;

    uni_slave.duration = has_duration ? duration : 100;
    uni_slave.ip_addr = ip;
    if (vtss_appl_ptp_clock_config_unicast_slave_config_set(clockinst, idx, &uni_slave) == VTSS_RC_ERROR) {
        ICLI_PRINTF("Clock instance %d : does not exist\n", clockinst);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_ext_clock_set(i32 session_id, BOOL has_output, BOOL has_input, BOOL has_output_input, BOOL has_ext, u32 clockfreq,
                                 BOOL has_ltc, BOOL has_single, BOOL has_independent, BOOL has_common, BOOL has_auto)
{
    vtss_appl_ptp_ext_clock_mode_t mode;
    icli_rc_t rc =  ICLI_RC_OK;
    mesa_rc my_rc = VTSS_RC_OK;

    /* update the local clock to the system clock */
    mode.one_pps_mode = has_output ? VTSS_APPL_PTP_ONE_PPS_OUTPUT : (has_input ? VTSS_APPL_PTP_ONE_PPS_INPUT : 
                        (has_output_input ? VTSS_APPL_PTP_ONE_PPS_OUTPUT_INPUT : VTSS_APPL_PTP_ONE_PPS_DISABLE));
    mode.clock_out_enable = has_ext;
    mode.adj_method = has_ltc ? VTSS_APPL_PTP_PREFERRED_ADJ_LTC :
                      has_single ? VTSS_APPL_PTP_PREFERRED_ADJ_SINGLE :
                      has_independent ? VTSS_APPL_PTP_PREFERRED_ADJ_INDEPENDENT :
                      has_common ? VTSS_APPL_PTP_PREFERRED_ADJ_COMMON :
                      VTSS_APPL_PTP_PREFERRED_ADJ_AUTO;
    mode.freq = clockfreq;
    if (!clockfreq) {
        mode.freq = 1;
    }
    if (MESA_CAP(MESA_CAP_TS_PPS_MODE_OVERRULES_CLK_OUT)) {
        if (mode.one_pps_mode != VTSS_APPL_PTP_ONE_PPS_DISABLE && mode.clock_out_enable) {
            ICLI_PRINTF("One_pps_mode overrules clock_out_enable, i.e. clock_out_enable is set to false\n");
            mode.clock_out_enable = false;
        }
    }
    if (MESA_CAP(MESA_CAP_TS_PPS_OUT_OVERRULES_CLK_OUT)) {
        if ((mode.one_pps_mode == VTSS_APPL_PTP_ONE_PPS_OUTPUT) && mode.clock_out_enable) {
            ICLI_PRINTF("One_pps_output overrules clock_out_enable, i.e. clock_out_enable is set to false\n");
            mode.clock_out_enable = false;
        }
    }
    if (VTSS_RC_OK != (my_rc = vtss_appl_ptp_ext_clock_out_set(&mode))) {
        ICLI_PRINTF("Could not set ext clock, ptp returned error: %s\n", error_txt(my_rc));
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_rs422_clock_set(i32 session_id, BOOL has_main_auto, BOOL has_main_man, BOOL has_sub, BOOL has_calib, BOOL has_pps_delay, u32 pps_delay, BOOL has_ser, BOOL has_proto, BOOL has_polyt, BOOL has_zda, BOOL has_rmc, BOOL has_pim, u32 pim_port)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_ptp_rs422_conf_t mode;

    /* update the local clock to the system clock */
    mode.mode = has_main_auto ? VTSS_PTP_RS422_MAIN_AUTO : has_main_man ? VTSS_PTP_RS422_MAIN_MAN : has_sub ? VTSS_PTP_RS422_SUB : has_calib ? VTSS_PTP_RS422_CALIB : VTSS_PTP_RS422_DISABLE;
    mode.delay = has_pps_delay ? pps_delay : 0;
    mode.proto = has_ser ? (has_proto ? (has_rmc ? VTSS_PTP_RS422_PROTOCOL_SER_RMC : (has_zda ? VTSS_PTP_RS422_PROTOCOL_SER_ZDA : VTSS_PTP_RS422_PROTOCOL_SER_POLYT)) : VTSS_PTP_RS422_PROTOCOL_SER_POLYT) : (has_pim ? VTSS_PTP_RS422_PROTOCOL_PIM : VTSS_PTP_RS422_PROTOCOL_SER_POLYT);
    mode.port  = pim_port;
    vtss_ext_clock_rs422_conf_set(&mode);
    return rc;
}

icli_rc_t ptp_icli_rs422_set_baudrate(i32 session_id, u32 baudrate, BOOL has_parity, BOOL has_none, BOOL has_even, BOOL has_odd, BOOL has_wordlength, u32 wordlength, BOOL has_stopbits, u32 stopbits, BOOL has_flowctrl, BOOL has_noflow, BOOL has_rtscts)
{
    vtss_serial_info_t serial_info;

    switch(baudrate) {
        case  9600: serial_info.baud = VTSS_SERIAL_BAUD_9600;  break;
        case 19200: serial_info.baud = VTSS_SERIAL_BAUD_19200; break;
        case 38400: serial_info.baud = VTSS_SERIAL_BAUD_38400; break;
        default   : serial_info.baud = VTSS_SERIAL_BAUD_115200;
    }
    if (has_parity && has_even) {
        serial_info.parity = VTSS_SERIAL_PARITY_EVEN;
    } else if (has_parity && has_odd)  {
        serial_info.parity = VTSS_SERIAL_PARITY_ODD;
    } else {
        serial_info.parity = VTSS_SERIAL_PARITY_NONE;
    }
    if (has_wordlength) {
        switch(wordlength) {
            case  5: serial_info.word_length = VTSS_SERIAL_WORD_LENGTH_5; break;
            case  6: serial_info.word_length = VTSS_SERIAL_WORD_LENGTH_6; break;
            case  7: serial_info.word_length = VTSS_SERIAL_WORD_LENGTH_7; break;
            default: serial_info.word_length = VTSS_SERIAL_WORD_LENGTH_8;
        }
    } else {
        serial_info.word_length = VTSS_SERIAL_WORD_LENGTH_8;
    }
    if (has_stopbits) {
       switch(stopbits) {
           case  2: serial_info.stop = VTSS_SERIAL_STOP_2;   break;
           default: serial_info.stop = VTSS_SERIAL_STOP_1;
       }
    } else {
        serial_info.stop = VTSS_SERIAL_STOP_1;
    }
    if (has_flowctrl && has_rtscts) {
        serial_info.flags = VTSS_SERIAL_FLOW_RTSCTS_RX | VTSS_SERIAL_FLOW_RTSCTS_TX;
    } else {
        serial_info.flags = 0;
    }
    if (ptp_1pps_set_baudrate(&serial_info) != VTSS_RC_OK) {
        return ICLI_RC_ERROR;
    }

    return ICLI_RC_OK;
}

icli_rc_t ptp_icli_ho_spec_set(i32 session_id, BOOL has_cat1, u32 cat1, BOOL has_cat2, u32 cat2, BOOL has_cat3, u32 cat3)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_appl_ho_spec_conf_t ho_spec;

    vtss_ho_spec_conf_get(&ho_spec);
    /* update the local clock to the system clock */
    ho_spec.cat1 = has_cat1 ? cat1 : ho_spec.cat1;
    ho_spec.cat2 = has_cat2 ? cat2 : ho_spec.cat2;
    ho_spec.cat3 = has_cat3 ? cat3 : ho_spec.cat3;
    vtss_ho_spec_conf_set(&ho_spec);
    return rc;
}

icli_rc_t ptp_icli_debug_log_mode_set(i32 session_id, int clockinst, u32 debug_mode, BOOL has_log_to_file, BOOL has_control, BOOL has_max_time, u32 max_time)
{
    icli_rc_t rc =  ICLI_RC_OK;
    u32 log_time = 10000;
    if (has_max_time) {
        log_time = max_time;
    }
    if (!ptp_debug_mode_set(debug_mode, clockinst, has_log_to_file, has_control, log_time)) {
        ICLI_PRINTF("Clock instance %d : does not exist\n", clockinst);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_log_delete(i32 session_id, int clockinst)
{
    icli_rc_t rc =  ICLI_RC_OK;
    if (!ptp_log_delete(clockinst)) {
        ICLI_PRINTF("Could not delete log file for PTP instance %d - Did one exist at all?\n", clockinst);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_afi_mode_set(i32 session_id, int clockinst, bool ann, bool enable)
{
    icli_rc_t rc =  ICLI_RC_OK;
    if (VTSS_RC_OK != ptp_afi_mode_set(clockinst, ann, enable)) {
        ICLI_PRINTF("Clock instance %d : does not exist\n", clockinst);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}
typedef struct ptp_icli_port_state_s {
    BOOL enable;
    BOOL internal;
} ptp_icli_port_state_t;

static icli_rc_t my_port_state_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    ptp_icli_port_state_t  *my_cfg = (ptp_icli_port_state_t *)cfg;
    mesa_rc my_rc;
    vtss_appl_ptp_config_port_ds_t ds_cfg;
    vtss_ifindex_t ifindex;

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);
    if ((my_rc = vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &ds_cfg)) == VTSS_RC_OK) {
        ds_cfg.enabled = my_cfg->enable;
        ds_cfg.portInternal = my_cfg->internal;
        if ((my_rc = vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &ds_cfg)) != VTSS_RC_OK) {
            ICLI_PRINTF("Error enabling or disabling inst %d port %d (%s)\n", inst, uport, error_txt(my_rc));
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Error getting port data instance %d port %d\n", inst, uport);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_port_state_set(i32 session_id, int clockinst, BOOL enable, BOOL has_internal, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    ptp_icli_port_state_t port_cfg;
    port_cfg.enable = enable;
    port_cfg.internal = has_internal;
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &port_cfg, my_port_state_set);
    return rc;
}

typedef struct ptp_icli_announce_state_s {
    BOOL has_interval;
    i8 interval;
    BOOL has_timeout;
    u8 timeout;
} ptp_icli_announce_state_t;

static icli_rc_t my_port_ann_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    ptp_icli_announce_state_t *my_cfg = (ptp_icli_announce_state_t *)cfg;
    vtss_appl_ptp_config_port_ds_t ds_cfg;
    vtss_ifindex_t ifindex;

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &ds_cfg) == VTSS_RC_OK) && (ds_cfg.enabled == 1)) {
        if (my_cfg->has_interval) ds_cfg.logAnnounceInterval = my_cfg->interval;
        if (my_cfg->has_timeout) ds_cfg.announceReceiptTimeout = my_cfg->timeout;
        if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &ds_cfg) != VTSS_RC_OK) {
            ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_port_announce_set(i32 session_id, int clockinst, BOOL has_interval, i8 interval, BOOL has_stop, BOOL has_default, BOOL has_timeout, u8 timeout, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    ptp_icli_announce_state_t ann_cfg;
    ann_cfg.has_interval = has_interval;
    ann_cfg.interval = interval;
    ann_cfg.has_timeout = has_timeout;
    ann_cfg.timeout = timeout;
    if (has_interval) {
        if (has_stop) {
            ann_cfg.interval = 127;
        } else if (has_default) {
            ann_cfg.interval = 126;
        }
    }
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &ann_cfg, my_port_ann_set);
    return rc;
}

static icli_rc_t my_port_sync_interval_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    i8 *interval = (i8 *)cfg;
    vtss_appl_ptp_config_port_ds_t ds_cfg;
    vtss_ifindex_t ifindex;

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &ds_cfg) == VTSS_RC_OK) && (ds_cfg.enabled == 1))
    {
        ds_cfg.logSyncInterval = *interval;
        if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &ds_cfg) == VTSS_RC_ERROR) {
            ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_port_sync_interval_set(i32 session_id, int clockinst, i8 interval, BOOL has_stop, BOOL has_default, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    T_IG(VTSS_TRACE_GRP_PTP_ICLI, "interval %d, has_stop %d, has_default %d", interval, has_stop, has_default);
    if (has_stop) {
        interval = 127;
    } else if (has_default) {
        interval = 126;
    }
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &interval, my_port_sync_interval_set);
    return rc;
}

BOOL ptp_icli_log_sync_interval_check(u32                session_id,
                                      icli_runtime_ask_t ask,
                                      icli_runtime_t     *runtime)
{
    switch (ask) {
        case ICLI_ASK_PRESENT:
            runtime->present = TRUE;
            return TRUE;
        case ICLI_ASK_BYWORD:
            icli_sprintf(runtime->byword, "<Interval : -7-4>");
            return TRUE;
        case ICLI_ASK_RANGE:
            runtime->range.type = ICLI_RANGE_TYPE_SIGNED;
            runtime->range.u.sr.cnt = 1;
            runtime->range.u.sr.range[0].min = -7;
            runtime->range.u.sr.range[0].max = 4;
            return TRUE;
        case ICLI_ASK_HELP:
            icli_sprintf(runtime->help, "logSyncInterval");
            return TRUE;
        default:
            break;
    }
    return FALSE;
}


static icli_rc_t my_port_delay_mechanism_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    u8 *dly = (u8 *)cfg;
    vtss_appl_ptp_config_port_ds_t ds_cfg;
    vtss_ifindex_t ifindex;

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &ds_cfg) == VTSS_RC_OK) && (ds_cfg.enabled == 1))
    {
        ds_cfg.delayMechanism = *dly;
        if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &ds_cfg) == VTSS_RC_ERROR) {
            ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_port_delay_mechanism_set(i32 session_id, int clockinst, BOOL has_e2e, BOOL has_p2p, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    u8 dly;
    dly = has_e2e ? DELAY_MECH_E2E : DELAY_MECH_P2P;
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &dly, my_port_delay_mechanism_set);
    return rc;
}

static icli_rc_t my_port_min_pdelay_interval_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    i8 *interval = (i8 *)cfg;
    vtss_appl_ptp_config_port_ds_t ds_cfg;
    vtss_ifindex_t ifindex;

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &ds_cfg) == VTSS_RC_OK) &&
        (ds_cfg.enabled == 1))
    {
        ds_cfg.logMinPdelayReqInterval = *interval;
        if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &ds_cfg) == VTSS_RC_ERROR) {
            ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_port_min_pdelay_interval_set(i32 session_id, int clockinst, i8 interval, BOOL has_stop, BOOL has_default, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    T_IG(VTSS_TRACE_GRP_PTP_ICLI, "interval %d, has_stop %d, has_default %d", interval, has_stop, has_default);
    if (has_stop) {
        interval = 127;
    } else if (has_default) {
        interval = 126;
    }
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &interval, my_port_min_pdelay_interval_set);
    return rc;
}

static icli_rc_t my_port_delay_asymmetry_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    mesa_timeinterval_t *asy = (mesa_timeinterval_t *)cfg;
    vtss_appl_ptp_config_port_ds_t ds_cfg;
    vtss_ifindex_t ifindex;

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &ds_cfg) == VTSS_RC_OK) &&
        (ds_cfg.enabled == 1))
    {
        ds_cfg.delayAsymmetry = *asy;
        if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &ds_cfg) == VTSS_RC_ERROR) {
            ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_delay_asymmetry_set(i32 session_id, int clockinst, i32 delay_asymmetry, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    mesa_timeinterval_t asy = ((mesa_timeinterval_t)delay_asymmetry)<<16;
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &asy, my_port_delay_asymmetry_set);
    return rc;
}

static icli_rc_t my_port_ingress_latency_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    mesa_timeinterval_t *igr = (mesa_timeinterval_t *)cfg;
    vtss_appl_ptp_config_port_ds_t ds_cfg;
    vtss_ifindex_t ifindex;

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &ds_cfg) == VTSS_RC_OK) &&
        (ds_cfg.enabled == 1))
    {
        ds_cfg.ingressLatency = *igr;
        if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &ds_cfg) == VTSS_RC_ERROR) {
            ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_ingress_latency_set(i32 session_id, int clockinst, i32 ingress_latency, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    mesa_timeinterval_t igr = ((mesa_timeinterval_t)ingress_latency)<<16;
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &igr, my_port_ingress_latency_set);
    return rc;
}

static icli_rc_t my_port_egress_latency_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    mesa_timeinterval_t *egr = (mesa_timeinterval_t *)cfg;
    vtss_appl_ptp_config_port_ds_t ds_cfg;
    vtss_ifindex_t ifindex;

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &ds_cfg) == VTSS_RC_OK) &&
        (ds_cfg.enabled == 1))
    {
        ds_cfg.egressLatency = *egr;
        if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &ds_cfg) == VTSS_RC_ERROR) {
            ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_egress_latency_set(i32 session_id, int clockinst, i32 egress_latency, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    mesa_timeinterval_t egr = ((mesa_timeinterval_t)egress_latency)<<16;
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &egr, my_port_egress_latency_set);
    return rc;
}

static icli_rc_t my_port_local_priority_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    u8* local_pri = (u8 *)cfg;
    vtss_appl_ptp_config_port_ds_t ds_cfg;
    vtss_ifindex_t ifindex;

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &ds_cfg) == VTSS_RC_OK) &&
            (ds_cfg.enabled == 1))
    {
        ds_cfg.localPriority = *local_pri;
        if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &ds_cfg) == VTSS_RC_ERROR) {
            ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_port_local_priority_set(i32 session_id, int clockinst, u8 localpriority, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &localpriority, my_port_local_priority_set);
    return rc;
}

static icli_rc_t my_port_not_slave_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    bool* not_slave = (bool *)cfg;
    vtss_appl_ptp_config_port_ds_t ds_cfg;
    vtss_ifindex_t ifindex;

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &ds_cfg) == VTSS_RC_OK) &&
            (ds_cfg.enabled == 1))
    {
        ds_cfg.notSlave = *not_slave;
        if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &ds_cfg) == VTSS_RC_ERROR) {
            ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_port_not_slave_set(i32 session_id, int clockinst, bool notSlave, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &notSlave, my_port_not_slave_set);
    return rc;
}

typedef struct ptp_icli_destaddr_type_s {
    bool has_default;
    bool has_link_local;
} ptp_icli_destaddr_type_t;

typedef struct ptp_icli_override_type_s {
    bool set_value;
    bool value;
} ptp_icli_override_type_t;

static icli_rc_t my_port_mcast_dest_adr_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    ptp_icli_destaddr_type_t* p = (ptp_icli_destaddr_type_t *)cfg;
    vtss_appl_ptp_config_port_ds_t ds_cfg;
    vtss_ifindex_t ifindex;

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &ds_cfg) == VTSS_RC_OK) &&
            (ds_cfg.enabled == 1))
    {
        ds_cfg.dest_adr_type = p->has_default ? VTSS_APPL_PTP_PROTOCOL_SELECT_DEFAULT :
                              (p->has_link_local ? VTSS_APPL_PTP_PROTOCOL_SELECT_LINK_LOCAL : ds_cfg.dest_adr_type);
        if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &ds_cfg) == VTSS_RC_ERROR) {
            ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

static icli_rc_t my_port_two_step_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    ptp_icli_override_type_t *p = (ptp_icli_override_type_t *)cfg;
    vtss_appl_ptp_config_port_ds_t ds_cfg;
    vtss_ifindex_t ifindex;

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &ds_cfg) == VTSS_RC_OK) &&
            (ds_cfg.enabled == 1))
    {
        if (p->set_value) {
            ds_cfg.twoStepOverride = p->value ? VTSS_APPL_PTP_TWO_STEP_OVERRIDE_TRUE : VTSS_APPL_PTP_TWO_STEP_OVERRIDE_FALSE;
        }
        else {
            ds_cfg.twoStepOverride = VTSS_APPL_PTP_TWO_STEP_OVERRIDE_NONE;
        }
        if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &ds_cfg) == VTSS_RC_ERROR) {
            ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_port_mcast_dest_adr_set(i32 session_id, int clockinst, bool has_default, bool has_link_local, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    ptp_icli_destaddr_type_t p;
    p.has_default = has_default;
    p.has_link_local = has_link_local;
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &p, my_port_mcast_dest_adr_set);
    return rc;
}

icli_rc_t ptp_icli_port_two_step_set(i32 session_id, int clockinst, bool set_value, bool value, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    ptp_icli_override_type_t p;
    p.set_value = set_value;
    p.value = value;
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &p, my_port_two_step_set);
    return rc;
}

static icli_rc_t my_port_delay_thresh_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    mesa_timeinterval_t *thr = (mesa_timeinterval_t *)cfg;
    vtss_appl_ptp_config_port_ds_t ds_cfg;
    vtss_ifindex_t ifindex;

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &ds_cfg) == VTSS_RC_OK) &&
            (ds_cfg.enabled == 1))
    {
        ds_cfg.c_802_1as.neighborPropDelayThresh = *thr;
        if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &ds_cfg) == VTSS_RC_ERROR) {
            ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Error getting port data instance %d port %d\n", inst, uport);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_port_delay_thresh_set(i32 session_id, int clockinst, BOOL set, u32 delay_thresh, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    mesa_timeinterval_t thr;
    if (set) {
        thr = ((mesa_timeinterval_t)delay_thresh)<<16;
    } else {
        thr = VTSS_MAX_TIMEINTERVAL;
    }
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &thr, my_port_delay_thresh_set);
    return rc;
}

static icli_rc_t my_port_sync_rx_to_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    u8 *to = (u8 *)cfg;
    vtss_appl_ptp_config_port_ds_t ds_cfg;
    vtss_ifindex_t ifindex;

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &ds_cfg) == VTSS_RC_OK) &&
            (ds_cfg.enabled == 1))
    {
        ds_cfg.c_802_1as.syncReceiptTimeout = *to;
        if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &ds_cfg) == VTSS_RC_ERROR) {
            ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Error getting port data instance %d port %d\n", inst, uport);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_port_sync_rx_to_set(i32 session_id, int clockinst, BOOL set, u8 sync_rx_to, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    u8 to;
    if (set) {
        to = sync_rx_to;
    } else {
        to = 3;
    }
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &to, my_port_sync_rx_to_set);
    return rc;
}

static icli_rc_t my_port_statistics_clear(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    BOOL * clear = (BOOL*)cfg;
    ptp_show_clock_port_statistics(inst, uport, false, icli_session_self_printf, *clear);
    return rc;
}

icli_rc_t ptp_icli_port_statistics_clear(i32 session_id, int clockinst, BOOL has_clear, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &has_clear, my_port_statistics_clear);
    return rc;
}

static icli_rc_t my_port_allow_lost_resp_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    u16 *resp = (u16 *)cfg;
    vtss_appl_ptp_config_port_ds_t ds_cfg;
    vtss_ifindex_t ifindex;

    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);

    if ((vtss_appl_ptp_config_clocks_port_ds_get(inst, ifindex, &ds_cfg) == VTSS_RC_OK) &&
            (ds_cfg.enabled == 1))
    {
        ds_cfg.c_802_1as.allowedLostResponses = *resp;
        if (vtss_appl_ptp_config_clocks_port_ds_set(inst, ifindex, &ds_cfg) == VTSS_RC_ERROR) {
            ICLI_PRINTF("Error setting port data instance %d port %d\n", inst, uport);
            rc = ICLI_RC_ERROR;
        }
    } else {
        ICLI_PRINTF("Error getting port data instance %d port %d\n", inst, uport);
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_port_allow_lost_resp_set(i32 session_id, int clockinst, BOOL set, u16 allow_lost_resp, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    u16 resp;
    if (set) {
        resp = allow_lost_resp;
    } else {
        resp = 3;
    }
    rc = ptp_icli_config_traverse_ports(session_id, clockinst, v_port_type_list, &resp, my_port_allow_lost_resp_set);
    return rc;
}

static icli_rc_t my_port_1pps_mode_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_1pps_sync_conf_t *pps_conf = (vtss_1pps_sync_conf_t *)cfg;
    mesa_rc my_rc;

    if (VTSS_RC_OK != (my_rc = ptp_1pps_sync_mode_set(uport2iport(uport), pps_conf))) {
        ICLI_PRINTF("Error setting 1pps sync for port %d (%s)\n", uport, error_txt(my_rc));
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_port_1pps_mode_set(i32 session_id, BOOL has_main_auto, BOOL has_main_man, BOOL has_sub, BOOL has_pps_phase, i32 pps_phase,
                                 BOOL has_cable_asy, i32 cable_asy, BOOL has_ser_man, BOOL has_ser_auto, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_1pps_sync_conf_t pps_conf;
    pps_conf.mode = has_main_auto ? VTSS_PTP_1PPS_SYNC_MAIN_AUTO : has_main_man ? VTSS_PTP_1PPS_SYNC_MAIN_MAN :
                    has_sub ? VTSS_PTP_1PPS_SYNC_SUB : VTSS_PTP_1PPS_SYNC_DISABLE;
    pps_conf.pulse_delay = has_pps_phase ? pps_phase : 0;
    pps_conf.cable_asy = has_cable_asy ? cable_asy : 0;
    pps_conf.serial_tod = has_ser_man ? VTSS_PTP_1PPS_SER_TOD_MAN : has_ser_auto ? VTSS_PTP_1PPS_SER_TOD_AUTO : VTSS_PTP_1PPS_SER_TOD_DISABLE;
    rc = ptp_icli_config_traverse_ports(session_id, 0, v_port_type_list, &pps_conf, my_port_1pps_mode_set);
    return rc;
}

static icli_rc_t my_port_1pps_delay_set(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_1pps_closed_loop_conf_t *pps_delay = (vtss_1pps_closed_loop_conf_t *)cfg;
    mesa_rc my_rc;

    if (VTSS_RC_OK != (my_rc = ptp_1pps_closed_loop_mode_set(uport2iport(uport), pps_delay))) {
        ICLI_PRINTF("Error setting 1pps delay for port %d (%s)\n", uport, error_txt(my_rc));
        rc = ICLI_RC_ERROR;
    }
    return rc;
}

icli_rc_t ptp_icli_port_1pps_delay_set(i32 session_id, BOOL has_auto, u32 master_port, BOOL has_man, u32 cable_delay, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_1pps_closed_loop_conf_t pps_delay;
    pps_delay.mode = has_auto ? VTSS_PTP_1PPS_CLOSED_LOOP_AUTO : has_man ? VTSS_PTP_1PPS_CLOSED_LOOP_MAN : VTSS_PTP_1PPS_CLOSED_LOOP_DISABLE;
    pps_delay.master_port = has_auto ? master_port : 0;
    pps_delay.cable_delay = has_man ? cable_delay : 0;
    rc = ptp_icli_config_traverse_ports(session_id, 0, v_port_type_list, &pps_delay, my_port_1pps_delay_set);
    return rc;
}

icli_rc_t ptp_icli_ms_pdv_show_apr_server_status(i32 session_id, u16 cguId, u16 serverId)
{
    icli_rc_t rc =  ICLI_RC_OK;

#if defined(VTSS_SW_OPTION_ZLS30387)
    char *s;
    if (zl_30380_pdv_apr_server_status_get(cguId, serverId, &s) != VTSS_RC_OK) {
        rc = ICLI_RC_ERROR;
    } else ICLI_PRINTF(s);
#else
    ICLI_PRINTF("External PDV filter function not defined\n");
#endif // defined(VTSS_SW_OPTION_ZLS30387)
    return rc;
}

icli_rc_t ptp_icli_ms_pdv_show_apr_config(i32 session_id, u16 cguId)
{
    icli_rc_t rc =  ICLI_RC_OK;

#if defined(VTSS_SW_OPTION_ZLS30387)
    if (zl_30380_apr_config_dump(cguId) == FALSE) {
        rc = ICLI_RC_ERROR;
    }
#else
    ICLI_PRINTF("External PDV filter function not defined\n");
#endif // defined(VTSS_SW_OPTION_ZLS30387)
    return rc;
}

static const char *tc_int_mode_disp(mesa_packet_internal_tc_mode_t m)
{
    switch (m) {
        case MESA_PACKET_INTERNAL_TC_MODE_30BIT: return "MODE_30BIT";
        case MESA_PACKET_INTERNAL_TC_MODE_32BIT: return "MODE_32BIT";
        case MESA_PACKET_INTERNAL_TC_MODE_44BIT: return "MODE_44BIT";
        case MESA_PACKET_INTERNAL_TC_MODE_48BIT: return "MODE_48BIT";
        default: return "unknown";
    }
}

icli_rc_t ptp_icli_tc_internal_set(i32 session_id, BOOL has_mode, u32 mode)
{
    icli_rc_t rc =  ICLI_RC_OK;
    mesa_packet_internal_tc_mode_t my_mode;
    T_IG(VTSS_TRACE_GRP_PTP_ICLI, "tc_internal_set, has_mode: %d, mode %d", has_mode, mode);
    if(has_mode) {
        my_mode = (mesa_packet_internal_tc_mode_t)mode;
        if(!tod_tc_mode_set(&my_mode)) {
            ICLI_PRINTF("Failed to set the TC internal mode to %d\n", my_mode);
            rc = ICLI_RC_ERROR;
        } else {
            ICLI_PRINTF("\nSuccessfully set the TC internal mode...\n");
            if (tod_ready()) {
                ICLI_PRINTF("Internal TC mode Configuration has been set, you need to reboot to activate the changed conf.\n");
            }
        }
    } else {
         if(!tod_tc_mode_get(&my_mode)) {
              ICLI_PRINTF("Failed to get the TC internal mode\n");
              rc = ICLI_RC_ERROR;
         } else {
              ICLI_PRINTF("TC internal mode %s\n", tc_int_mode_disp(my_mode));
         }
    }
    return rc;
}

static const char *sys_clock_sync_mode_txt(vtss_appl_ptp_system_time_sync_mode_t mode)
{
    switch (mode) {
        case VTSS_APPL_PTP_SYSTEM_TIME_NO_SYNC:  return "No System clock to PTP Sync";
        case VTSS_APPL_PTP_SYSTEM_TIME_SYNC_SET: return "Set System time from PTP time";
        case VTSS_APPL_PTP_SYSTEM_TIME_SYNC_GET: return "Get PTP time from System time";
    }
    return "INVALID";
}

icli_rc_t ptp_icli_system_time_sync_set(i32 session_id, BOOL has_get, BOOL has_set)
{
    vtss_appl_ptp_system_time_sync_conf_t conf;
    icli_rc_t rc =  ICLI_RC_OK;
    mesa_rc my_rc;

    if (VTSS_RC_OK != (my_rc = vtss_appl_ptp_system_time_sync_mode_get(&conf))) {
        ICLI_PRINTF("Error getting system clock synch mode (%s)\n", error_txt(my_rc));
        rc = ICLI_RC_ERROR;
    }
    conf.mode = has_get ? VTSS_APPL_PTP_SYSTEM_TIME_SYNC_GET :
                has_set ? VTSS_APPL_PTP_SYSTEM_TIME_SYNC_SET : VTSS_APPL_PTP_SYSTEM_TIME_NO_SYNC;
    if (VTSS_RC_OK != (my_rc = vtss_appl_ptp_system_time_sync_mode_set(&conf))) {
        ICLI_PRINTF("Error setting system clock synch mode (%s)\n", error_txt(my_rc));
        rc = ICLI_RC_ERROR;
    } else {
        ICLI_PRINTF("System clock synch mode (%s)\n", sys_clock_sync_mode_txt(conf.mode));
    }
    return rc;

}

icli_rc_t ptp_icli_system_time_sync_show(i32 session_id)
{
    vtss_appl_ptp_system_time_sync_conf_t conf;
    icli_rc_t rc =  ICLI_RC_OK;
    mesa_rc my_rc;

    if (VTSS_RC_OK != (my_rc = vtss_appl_ptp_system_time_sync_mode_get(&conf))) {
        ICLI_PRINTF("Error getting system clock synch mode (%s)\n", error_txt(my_rc));
        rc = ICLI_RC_ERROR;
    } else {
        ICLI_PRINTF("System clock synch mode (%s)\n", sys_clock_sync_mode_txt(conf.mode));
    }
    return rc;
}

static const char *ref_clock_freq_txt(mesa_phy_ts_clockfreq_t freq)
{
    switch (freq) {
        case MESA_PHY_TS_CLOCK_FREQ_125M:  return "125 MHz ";
        case MESA_PHY_TS_CLOCK_FREQ_15625M: return "156.25 MHz";
        case MESA_PHY_TS_CLOCK_FREQ_250M: return "250 MHz";
        default: return "INVALID";
    }
}

icli_rc_t ptp_icli_ptp_ref_clock_set(i32 session_id, BOOL has_mhz125, BOOL has_mhz156p25, BOOL has_mhz250)
{
    icli_rc_t rc =  ICLI_RC_OK;

    if (MESA_CAP(MESA_CAP_PHY_TS)) {
        mesa_phy_ts_clockfreq_t freq;

        if (!tod_ref_clock_freg_get(&freq)) {
            ICLI_PRINTF("Error getting ref clock frequency\n");
            rc = ICLI_RC_ERROR;
        }
        freq = has_mhz125 ? MESA_PHY_TS_CLOCK_FREQ_125M :
                    has_mhz156p25 ? MESA_PHY_TS_CLOCK_FREQ_15625M :
                    has_mhz250 ? MESA_PHY_TS_CLOCK_FREQ_250M : MESA_PHY_TS_CLOCK_FREQ_250M;
        if (!tod_ref_clock_freg_set(&freq)) {
            ICLI_PRINTF("Error setting ref clock frequency\n");
            rc = ICLI_RC_ERROR;
        } else {
            ICLI_PRINTF("System ref clock frequency (%s)\n", ref_clock_freq_txt(freq));
        }
    }

    return rc;
}

static icli_rc_t my_port_ptp_icli_debug_pim_statistics(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    BOOL *clear = (BOOL *)cfg;
    ptp_pim_frame_statistics_t stati;
    
    /* show the statistics  */
    ptp_pim_statistics_get(uport2iport(uport), &stati, *clear);
    if (!*clear) {
        ICLI_PRINTF("Port %d Statistics:\n", uport);
        ICLI_PRINTF("%-24s%12u\n", "Requests", stati.request);
        ICLI_PRINTF("%-24s%12u\n", "Replies", stati.reply);
        ICLI_PRINTF("%-24s%12u\n", "Events", stati.event);
        ICLI_PRINTF("%-24s%12u\n", "RX-Dropped", stati.dropped);
        ICLI_PRINTF("%-24s%12u\n", "TX-Dropped", stati.tx_dropped);
        ICLI_PRINTF("%-24s%12u\n", "Errors", stati.errors);
    }
    return rc;
}

icli_rc_t ptp_icli_debug_pim_statistics(i32 session_id, icli_stack_port_range_t *v_port_type_list, BOOL clear)
{
    icli_rc_t rc =  ICLI_RC_OK;
    rc = ptp_icli_config_traverse_ports(session_id, 0, v_port_type_list, &clear, my_port_ptp_icli_debug_pim_statistics);
    return rc;
}

icli_rc_t ptp_icli_debug_egress_latency_statistics(i32 session_id, BOOL clear)
{
    icli_rc_t rc =  ICLI_RC_OK;
    observed_egr_lat_t lat;
    char str1[20];
    char str2[20];
    char str3[20];
    if (clear) {
        /* clear the Observed Egress latency */
        ptp_clock_egress_latency_clear();
        ICLI_PRINTF("Observed Egress Latency counters cleared\n");
    } else {
        ptp_cli_table_header("min             mean            max             count ", icli_session_self_printf);
        ptp_clock_egress_latency_get(&lat);
        ICLI_PRINTF("%-16s%-16s%-16s%-5d\n",
                vtss_tod_TimeInterval_To_String(&lat.min,str1,','),
                vtss_tod_TimeInterval_To_String(&lat.mean,str2,','),
                vtss_tod_TimeInterval_To_String(&lat.max,str3,','),
                lat.cnt);
    }
    return rc;
}

icli_rc_t ptp_icli_debug_slave_statistics(i32 session_id, int clockinst, BOOL has_enable, BOOL has_disable, BOOL has_clear)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_ptp_slave_statistics_t stati;
    char str1[20];
    char str2[20];
    char str3[20];
    char str4[20];
    char str5[20];
    char str6[20];
    char str7[20];
    char str8[20];
    if (has_enable || has_disable) {
        if (VTSS_RC_OK != ptp_clock_slave_statistics_enable(clockinst, has_enable)) {
            rc = ICLI_RC_ERR_PARAMETER;
        }
    } else {
        if (VTSS_RC_OK != ptp_clock_slave_statistics_get(clockinst, &stati, has_clear)) {
            rc = ICLI_RC_ERR_PARAMETER;
        } else {
            if (stati.enable) {
                mesa_timeinterval_t master_to_slave_mean = stati.master_to_slave_mean % MAX(stati.master_to_slave_mean_count, 1);
                mesa_timeinterval_t slave_to_master_mean = stati.slave_to_master_mean % MAX(stati.slave_to_master_mean_count, 1);

                ICLI_PRINTF(
                        "master_to_slave_max     : %14s sec\n" 
                        "master_to_slave_min     : %14s sec\n" 
                        "master_to_slave_mean    : %14s sec\n"
                        "master_to_slave_cur     : %14s sec\n" 
                        "slave_to_master_max     : %14s sec\n" 
                        "slave_to_master_min     : %14s sec\n" 
                        "slave_to_master_mean    : %14s sec\n"
                        "slave_to_master_cur     : %14s sec\n" 
                        "sync_pack_rx_cnt        : %14u\n" 
                        "sync_pack_timeout_cnt   : %14u\n" 
                        "delay_req_pack_tx_cnt   : %14u\n" 
                        "sync_pack_seq_err_cnt   : %14u\n" 
                        "follow_up_pack_loss_cnt : %14u\n" 
                        "delay_resp_pack_loss_cnt: %14u\n",
                        vtss_tod_TimeInterval_To_String(&stati.master_to_slave_max,str1,','),
                        vtss_tod_TimeInterval_To_String(&stati.master_to_slave_min,str2,','),
                        vtss_tod_TimeInterval_To_String(&master_to_slave_mean,str7,','),
                        vtss_tod_TimeInterval_To_String(&stati.master_to_slave_cur,str3,','),
                        vtss_tod_TimeInterval_To_String(&stati.slave_to_master_max,str4,','),
                        vtss_tod_TimeInterval_To_String(&stati.slave_to_master_min,str5,','),
                        vtss_tod_TimeInterval_To_String(&slave_to_master_mean,str8,','),
                        vtss_tod_TimeInterval_To_String(&stati.slave_to_master_cur,str6,','),
                        stati.sync_pack_rx_cnt,
                        stati.sync_pack_timeout_cnt,
                        stati.delay_req_pack_tx_cnt,
                        stati.sync_pack_seq_err_cnt,
                        stati.follow_up_pack_loss_cnt,
                        stati.delay_resp_pack_loss_cnt);
                if (has_clear) {
                    ICLI_PRINTF("Slave statistics cleared\n");
                }
            } else {
                ICLI_PRINTF("Slave statistics not enabled\n");
            }
        }
    }
    return rc;
}

icli_rc_t ptp_icli_debug_one_pps_tod_statistics(i32 session_id, BOOL has_clear)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_ptp_one_pps_tod_statistics_t stati;
    if (VTSS_RC_OK != ptp_clock_one_pps_tod_statistics_get(&stati, has_clear)) {
        rc = ICLI_RC_ERR_PARAMETER;
    } else {
        ICLI_PRINTF(
            "one_pps_cnt            : %14u\n" 
            "missed_one_pps_cnt     : %14u\n" 
            "missed_tod_rx_cnt      : %14u\n",
            stati.one_pps_cnt,
            stati.missed_one_pps_cnt,
            stati.missed_tod_rx_cnt);
        if (has_clear) {
            ICLI_PRINTF("Slave statistics cleared\n");
        }
    }
    return rc;
}

BOOL ptp_icli_ref_clk_option(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    switch (ask) {
        case ICLI_ASK_PRESENT:
            if (MESA_CAP(MEBA_CAP_1588_REF_CLK_SEL)) {
                runtime->present = true;
            } else {
                runtime->present = false;
            }
            return true;
        default:
            break;
    }
    return false;
}

BOOL ptp_icli_rs_422_option(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    switch (ask) {
        case ICLI_ASK_PRESENT:
            if (MESA_CAP(MESA_CAP_TS_PTP_RS422)) {
                runtime->present = true;
            } else {
                runtime->present = false;
            }
            return true;
        default:
            break;
    }
    return false;
}
#define HI_BIT_U8(val) ((u8)val & 0xF0) ? ((val & 0xC0) ? ((val & 0x80) ? 8 : 7) : ((val & 0x20) ? 6 : 5)) : \
                         (((val & 0xC) ? ((val & 8) ? 4 : 3) : ((val & 2) ? 2 : ((val & 1) ? 1 : 0))))

BOOL port_icli_runtime_tc_internal_range(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    switch (ask) {
        case ICLI_ASK_PRESENT:
             if (MESA_CAP(MESA_CAP_TS_TOD_INTERNAL_TC_MODE)) {
                 runtime->present = true;
             } else {
                 runtime->present = false;
             }
             return true;
        case ICLI_ASK_HELP:
            if (((HI_BIT_U8(MESA_CAP(MESA_CAP_TS_TOD_INTERNAL_TC_MODE))) - 1) <= 2) {
                icli_sprintf(runtime->help, "0 = MODE_30BIT, 1 = MODE_32BIT, 2 = MODE_44BIT");
            } else {
                icli_sprintf(runtime->help, "0 = MODE_30BIT, 1 = MODE_32BIT, 2 = MODE_44BIT, 3 = MODE_48BIT");
            }
            return TRUE;
        case ICLI_ASK_RANGE:
            runtime->range.type = ICLI_RANGE_TYPE_UNSIGNED;
            runtime->range.u.sr.cnt = 1;
            runtime->range.u.sr.range[0].min = 0;
            runtime->range.u.sr.range[0].max = ((HI_BIT_U8(MESA_CAP(MESA_CAP_TS_TOD_INTERNAL_TC_MODE))) - 1);
            return TRUE;
        default:
            break;
    }
    return FALSE;
}

BOOL ptp_icli_sync_ann_auto_option(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    switch (ask) {
        case ICLI_ASK_PRESENT:
            if (MESA_CAP(MESA_CAP_SYNCE_ANN_AUTO_TRANSMIT)) {
                runtime->present = true;
            } else {
                runtime->present = false;
            }
            return true;
        default:
            break;
    }
    return false;
}

BOOL ptp_icli_filter_type_basic_option(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    switch (ask) {
        case ICLI_ASK_PRESENT:
#ifdef SW_OPTION_BASIC_PTP_SERVO
            runtime->present = true;
#else
            runtime->present = false;
#endif
            return true;
        default:
            break;
    }
    return false;
}

icli_rc_t ptp_icli_debug_servo_best_master(i32 session_id, int clockinst)
{
    icli_rc_t rc = ICLI_RC_OK;

//    rc = (vtss_ptp_servo_set_best_master(clockinst) == VTSS_RC_OK) ? ICLI_RC_OK : ICLI_RC_ERROR;
    ICLI_PRINTF("Servo best master is not supported\n");
    return rc;
}

icli_rc_t ptp_icli_show_servo_source(i32 session_id)
{
    vtss_ptp_synce_src_t src;
    if (ptp_get_selected_src(&src) == VTSS_RC_OK) {
        bool generic = zl_3038x_servo_dpll_config_generic();

        ICLI_PRINTF("Servo current source is type %s ref %d, DPLL_type %s\n", sync_src_type_2_txt(src.type), src.ref, generic ? "Generic" : "DPLLSpecific");
        return ICLI_RC_OK;
    } else {
        return ICLI_RC_ERROR;
    }
}

icli_rc_t ptp_icli_show_servo_mode_ref(i32 session_id)
{
    int inst = 0;
    vtss_ptp_servo_mode_ref_t mode_ref;
    while (VTSS_RC_OK == vtss_ptp_get_servo_mode_ref(inst, &mode_ref)) {
        ICLI_PRINTF("Servo [%d] mode %s ref %d\n", inst, sync_servo_mode_2_txt(mode_ref.mode), mode_ref.ref);
        inst++;
    }
    return ICLI_RC_OK;
}


icli_rc_t ptp_icli_debug_path_trace(i32 session_id, int clockinst)
{
    icli_rc_t rc;
    ptp_path_trace_t trace;
    int i;
    char str1 [40];

    rc = (ptp_clock_path_trace_get(clockinst, &trace) == VTSS_RC_OK) ? ICLI_RC_OK : ICLI_RC_ERROR;
    if (rc == ICLI_RC_OK) {
        ICLI_PRINTF("path_trace size: %d\n", trace.size);
        for (i = 0; i < trace.size && i < PTP_PATH_TRACE_MAX_SIZE; i++) {
            ICLI_PRINTF("path_trace[%d] %s\n", i, ClockIdentityToString(trace.pathSequence[i], str1));

        }
    }
    return rc;
}














































static const char * io_pin2txt (vtss_appl_ptp_ext_io_pin_cfg_t pin)
{
    switch (pin) {
        case VTSS_APPL_PTP_IO_MODE_ONE_PPS_DISABLE:    return "PPS_DISABLE";
        case VTSS_APPL_PTP_IO_MODE_ONE_PPS_OUTPUT:     return "PPS_OUTPUT";
        case VTSS_APPL_PTP_IO_MODE_WAVEFORM_OUTPUT:    return "WAVEFORM_OUTPUT";
        case VTSS_APPL_PTP_IO_MODE_ONE_PPS_LOAD:       return "PPS_LOAD";
        case VTSS_APPL_PTP_IO_MODE_ONE_PPS_SAVE:       return "PPS_SAVE";
        default:                                return "Unknown";
    }
}

icli_rc_t ptp_icli_io_pin_config(i32 session_id, u32 io_pin, BOOL has_disable, BOOL has_pps_output, BOOL has_waveform_output, BOOL has_load, 
                                 BOOL has_save, BOOL has_domain,
                                 u32 domain, BOOL has_freq, u32 freq, BOOL has_port, mesa_port_no_t port)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_appl_ptp_ext_io_mode_t  conf;
    mesa_port_no_t one_pps_slave_port_no;
    if (VTSS_RC_OK != vtss_appl_ptp_io_pin_conf_default_get(io_pin, &conf)) {
        rc = ICLI_RC_ERR_PARAMETER;
    } else {
        if (has_disable || has_pps_output || has_waveform_output || has_load || has_save) {
            // set IO-PIN
            conf.pin = has_disable ? VTSS_APPL_PTP_IO_MODE_ONE_PPS_DISABLE : (
                       has_pps_output ? VTSS_APPL_PTP_IO_MODE_ONE_PPS_OUTPUT : (
                       has_waveform_output ? VTSS_APPL_PTP_IO_MODE_WAVEFORM_OUTPUT : (
                       has_load ? VTSS_APPL_PTP_IO_MODE_ONE_PPS_LOAD : VTSS_APPL_PTP_IO_MODE_ONE_PPS_SAVE)));
            conf.domain = has_domain ? domain : conf.domain;
            conf.freq = has_freq ? freq : conf.freq;
            one_pps_slave_port_no = has_port ? port : VTSS_PORT_NO_NONE;
            conf.one_pps_slave_port_number = iport2uport(one_pps_slave_port_no);

            if (VTSS_RC_OK != vtss_appl_ptp_io_pin_conf_set(io_pin, &conf)) {
                rc = ICLI_RC_ERR_PARAMETER;
                ICLI_PRINTF("Could not set io pin configuration\n");
            }
        } else {
            // show IO-PIN
            if (VTSS_RC_OK != vtss_appl_ptp_io_pin_conf_get(io_pin, &conf)) {
                rc = ICLI_RC_ERR_PARAMETER;
            } else {
                T_W("slaveport %d", conf.one_pps_slave_port_number);
                if (conf.one_pps_slave_port_number == 0) {
                    conf.one_pps_slave_port_number = MESA_CAP(MESA_CAP_PORT_CNT);
                }

                ICLI_PRINTF("io_pin %d  mode %s   domain %d   freq %d   port %d\n",
                            io_pin,
                            io_pin2txt(conf.pin),
                            conf.domain,
                            conf.freq,
                            conf.one_pps_slave_port_number);
            }
        }
    }
    return rc;
}

#if defined(VTSS_SW_OPTION_ZLS30387)
icli_rc_t ptp_icli_debug_mspdv_loglevel(i32 session_id, u32 loglevel)
{
    return zl_30380_apr_set_log_level(loglevel) ? ICLI_RC_OK : ICLI_RC_ERROR;
}
#endif

#if defined(VTSS_SW_OPTION_ZLS30387)
icli_rc_t ptp_icli_show_psl_fcl_config(i32 session_id, u16 cguId)
{
    return zl_30380_apr_show_psl_fcl_config(cguId) ? ICLI_RC_OK : ICLI_RC_ERROR;
}

icli_rc_t ptp_icli_show_apr_statistics(i32 session_id, u16 cguId, u32 stati)
{
    return zl_30380_apr_show_statistics(cguId, stati) ? ICLI_RC_OK : ICLI_RC_ERROR;
}



#endif

icli_rc_t ptp_icli_debug_set_ptp_delta_time(i32 session_id, u32 domain, u16 sec_msb, u32 sec_lsb, u32 nanosec, BOOL has_neg)
{
    icli_rc_t rc = ICLI_RC_OK;
    mesa_timestamp_t  ts;
    ts.sec_msb = sec_msb;
    ts.seconds = sec_lsb;
    ts.nanoseconds = nanosec;
    
    vtss_local_clock_time_set_delta(&ts, domain, has_neg);
    return rc;
}

/****************************************************************************/
/*  End of file.                                                            */
/****************************************************************************/
