/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
/* ptp_local_clock.c */

#include "vtss_ptp_local_clock.h"   /* define callouts from base part */
#include "ptp_local_clock.h"        /* define platform specific part */
#include "vtss_tod_api.h"
#include "ptp.h"
#include "ptp_api.h"
#include <sys/time.h>
#include "critd_api.h"
#include "misc_api.h"

#define CLOCK_ADJ_SLEW_RATE (10000LL*ADJ_SCALE)
#if defined(VTSS_SW_OPTION_SYNCE_DPLL)
#include "synce_custom_clock_api.h"
#include "zl_3038x_api_pdv_api.h"
#endif

/**
 * \brief internal clock management
 *
 * in a HW based timer, actual time = hw_time + ptp_offset
 * in a sw based timer, actual time = hw_time + drift + ptp_offset
 *                      drift = +(hw_time - t0)*ratio
 */
static struct {
    mesa_timeinterval_t        drift; /* accumulated drift relative to the HW clock */
    mesa_timestamp_t       t0;    /* Time of latest adjustment */
    mesa_timeinterval_t    ratio; /* actual clock drift ratio in pbb */
    i32 ptp_offset;  /* offset in seconds between hw clock and PTP clock */
    int             clock_option; /* clock adjustment option */
    vtss_appl_ptp_preferred_adj_t adj_method; /* preferred clock adjust method (only relevant for clock domain 0)*/
    BOOL current_synce_enable;
    u32  set_time_count;
}   sw_clock_offset[PTP_CLOCK_INSTANCES];

static critd_t clockmutex;          /* clock internal data protection */

#define CLOCK_LOCK()        critd_enter(&clockmutex, _C, LOCK_TRACE_LEVEL, __FILE__, __LINE__)
#define CLOCK_UNLOCK()      critd_exit (&clockmutex, _C, LOCK_TRACE_LEVEL, __FILE__, __LINE__)

//#include <cyg/io/i2c_vcoreiii.h>
#define AD5667_I2C_ADDR 0x0f
#define MAX_5667_I2C_LEN 3
static const u16 init_dac = 0x9999;

static int vtss_ptp_adjustment_option(int clock_domain)
{
    int option;
    //bool grant_enable;
    option = sw_clock_offset[clock_domain].clock_option;
    //if (option == CLOCK_OPTION_SYNCE_DPLL) {
    //    PTP_RC(clock_adjtimer_enable_request(true, &grant_enable, instance));
    //    if (!grant_enable) {
    //        option = CLOCK_OPTION_INTERNAL_TIMER;
    //    }
    //    if (sw_clock_offset[instance].current_synce_enable && !grant_enable) {
    //        T_IG(_C,"SYNCE DPLL frequency adjust not allowed, falling back to LTC");
    //    }
    //    sw_clock_offset[instance].current_synce_enable = grant_enable;
    //}
    return option;
}

static bool vtss_dac_clock_init_adjtimer(bool cold)
{
    bool ret_val = false;

    if (MESA_CAP(MESA_CAP_MISC_DAC_CONTROLS_LOCAL_OSC)) {
        int i2c_dac_dev, i2c_dac_addr;
        u8 buf[MAX_5667_I2C_LEN];
    
        i2c_dac_dev = vtss_i2c_dev_open("ptp_dac", NULL, &i2c_dac_addr);
        if (i2c_dac_dev >= 0) {
            ret_val = true;
            buf[0] = 0x38;  /*command: internal reference setup, reg addr */
            buf[1] = 0x00;  /*reg high val */
            buf[2] = 0x01;  /*reg low val */
            if (vtss_i2c_dev_wr(i2c_dac_dev, &buf[0], MAX_5667_I2C_LEN) != VTSS_RC_OK)    ret_val = false;
            buf[0] = 0x17;  /*command: write to input reg, reg addr both DAC's */
            buf[1] = init_dac>>8;  /*reg high val */
            buf[2] = init_dac & 0x0ff;  /*reg low val */
            if (vtss_i2c_dev_wr(i2c_dac_dev, &buf[0], MAX_5667_I2C_LEN) != VTSS_RC_OK)    ret_val = false;
            T_DG(_C,"ret_val = %d", ret_val);
        }
        vtss_i2c_dev_close(i2c_dac_dev);
    }

    return ret_val;
}

/*
 * dac value: 65535 ~ 2,5V, nominel freq ~1,5V, +/-1V ~+/- 5ppm =>
 *            adj = 0 => dac = 1,5 V  = 39321
 *            0,1 ppb = 0,1ppb/5ppm*1V => dac = (0,1/5000)*26214
 *            dac = 39321 + 26214/50000*adj
 */
static bool vtss_dac_clock_set_adjtimer(i64 adj)
{
    bool ret_val = false;

    if (MESA_CAP(MESA_CAP_MISC_DAC_CONTROLS_LOCAL_OSC)) {
        int i2c_dac_dev, i2c_dac_addr;
        i32 clock_adj = (adj*10LL)>>16;
        u16 dac = init_dac;
        i32 temp;
        i2c_dac_dev = vtss_i2c_dev_open("ptp_dac", NULL, &i2c_dac_addr);
        if (i2c_dac_dev >= 0) {
            ret_val = true;
            if (clock_adj > 50000) clock_adj = 50000;
            if (clock_adj < -50000) clock_adj = -50000;
            temp = (26214 * clock_adj)/50000;
            dac += temp;
            u8 buf[MAX_5667_I2C_LEN];
            buf[0] = 0x17;  /*command: write to input reg, reg addr both DAC's */
            buf[1] = dac>>8;  /*reg high val */
            buf[2] = dac & 0xff;  /*reg low val */
            if (vtss_i2c_dev_wr(i2c_dac_dev, &buf[0], MAX_5667_I2C_LEN) != VTSS_RC_OK)    ret_val = false;
            T_DG(_C,"ret_val = %d, dac = %d", ret_val, dac);
        }
        vtss_i2c_dev_close(i2c_dac_dev);
    }

    return ret_val;
}

static sync_clock_feature_t features = SYNCE_CLOCK_FEATURE_NONE; /* pr default no Synce features are available */

static mesa_rc calc_clock_option(vtss_appl_ptp_preferred_adj_t adj_method, vtss_appl_ptp_profile_t profile, int *clock_option)
{
    mesa_rc rc = VTSS_RC_OK;
    vtss_appl_ptp_preferred_adj_t my_method;
#if defined(VTSS_SW_OPTION_SYNCE_DPLL)
    PTP_RC(clock_features_get(&features));
#endif
    *clock_option = CLOCK_OPTION_INTERNAL_TIMER;
    my_method = adj_method;
    if (my_method == VTSS_APPL_PTP_PREFERRED_ADJ_AUTO) {
        T_IG(_C,"Auto adjust selection");
        if (profile == VTSS_APPL_PTP_PROFILE_G_8265_1 || profile == VTSS_APPL_PTP_PROFILE_G_8275_1) {
            my_method = VTSS_APPL_PTP_PREFERRED_ADJ_SINGLE;
        } else {
            my_method = VTSS_APPL_PTP_PREFERRED_ADJ_INDEPENDENT;
        }
    }
    switch (my_method) {
        case     VTSS_APPL_PTP_PREFERRED_ADJ_SINGLE:
#if defined(VTSS_SW_OPTION_SYNCE_DPLL)
            if (MESA_CAP(MEBA_CAP_SYNCE_DPLL_MODE_SINGLE) && features != SYNCE_CLOCK_FEATURE_NONE) {
                *clock_option = CLOCK_OPTION_SYNCE_DPLL;
            } else {
                T_IG(_C,"Fallback to LTC adjustment");
            }
#else
            rc = VTSS_RC_ERROR;
            T_IG(_C,"Synce DPLL not supported");
#endif
            break;
        case     VTSS_APPL_PTP_PREFERRED_ADJ_INDEPENDENT:
            if (MESA_CAP(MEBA_CAP_SYNCE_DPLL_MODE_DUAL) && MESA_CAP(MESA_CAP_SYNCE_SEPARATE_TIMING_DOMAINS)) {
                if (features == SYNCE_CLOCK_FEATURE_DUAL || features == SYNCE_CLOCK_FEATURE_DUAL_INDEPENDENT) {
                    *clock_option = CLOCK_OPTION_PTP_DPLL;
                } else {
                    T_IG(_C,"DPLL does not support independant. Fallback to LTC adjustment");
                }
            } else {
                T_IG(_C,"Board does not support independent. Fallback to LTC adjustment");
            }
            break;
        case     VTSS_APPL_PTP_PREFERRED_ADJ_COMMON:
            if (MESA_CAP(MEBA_CAP_SYNCE_DPLL_MODE_SINGLE)) {    // At present the real common mode is not implemented. Common mode is
                *clock_option = CLOCK_OPTION_SYNCE_DPLL;  // the same as single mode but with DPLL in generic mode.
                T_IG(_C,"Synce freq or phase phase adjustment");
            } else {
                T_IG(_C,"Fallback to LTC adjustment");
            }
            break;
        default:
            break;
    }
#if defined(VTSS_SW_OPTION_SYNCE_DPLL)
    ptp_clock_source_t source;
    source = (*clock_option == CLOCK_OPTION_SYNCE_DPLL) ? PTP_CLOCK_SOURCE_SYNCE : PTP_CLOCK_SOURCE_INDEP;
    if (VTSS_RC_OK != clock_ptp_timer_source_set(source)) {
        T_IG(_C,"clock_ptp_timer_source_set %d, only supported if a DPLL is present", source);
    }
    rc = zl_3038x_servo_dpll_config(*clock_option, (*clock_option != CLOCK_OPTION_SYNCE_DPLL) || (my_method != VTSS_APPL_PTP_PREFERRED_ADJ_SINGLE));

#endif //defined(VTSS_FEATURE_CLOCK) && defined(VTSS_SW_OPTION_SYNCE)

    T_IG(_C,"adjust selection: preferred method %d, my_method %d, clock_option %d, profile %d, features %d", adj_method, my_method, *clock_option, profile, features);
    return rc;
}

mesa_rc vtss_local_clock_adj_method(vtss_appl_ptp_preferred_adj_t adj_method, vtss_appl_ptp_profile_t profile)
{
    static bool first_time = true;
    mesa_rc rc = VTSS_RC_OK;
    int instance = 0; // only instance 0 supports different adjustment methods.
    if ((sw_clock_offset[instance].adj_method != adj_method) && !first_time) {
        vtss_local_clock_ratio_clear(instance);
        T_IG(_C,"adj_method changed, i.e. clock ratio cleared");
    }
    first_time = false;
    sw_clock_offset[instance].adj_method = adj_method;
    T_IG(_C,"adj_method %d", adj_method);

    rc = calc_clock_option(adj_method, profile, &sw_clock_offset[instance].clock_option);
    if (rc != VTSS_RC_OK) {
        T_IG(_C,"Preferred adj_method not supported");
    }
    return rc;
}

void vtss_local_clock_initialize(u32 time_domain, BOOL cold, vtss_appl_ptp_preferred_adj_t adj_method)
{
    T_IG(_C,"time_domain %d", time_domain);
    if (time_domain == 0) {
        critd_init(&clockmutex, "clockmutex", VTSS_MODULE_ID_PTP, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
    } else {
        CLOCK_LOCK();
    }

    sw_clock_offset[time_domain].adj_method = adj_method;
    sw_clock_offset[time_domain].ptp_offset = 0;
    /* SW simulated clock */
    sw_clock_offset[time_domain].drift = 0;
    sw_clock_offset[time_domain].t0.seconds = 0;
    sw_clock_offset[time_domain].t0.nanoseconds = 0;
    sw_clock_offset[time_domain].ratio = 0;
    sw_clock_offset[time_domain].set_time_count = 0;
    /*
     * HW initialization
     */
    if (time_domain == 0) {    /* only time_domain 0 is a HW clock */
#ifdef VTSS_SW_OPTION_PTP_GPIO
        ulong cur_reg_val;
        // Set the GPIO12 (Cable test A) to be output.
        vtss_global_lock(__FILE__, __LINE__);
        cur_reg_val = SYSTEM_GPIO;
        cur_reg_val |=
            VTSS_BIT((SYSTEM_GPIO_F_OUTPUT_ENABLE_FPOS + PTP_CLOCK_GPIO));
        SYSTEM_GPIO = cur_reg_val;

        // Set the control register to gate GPIO.
        cur_reg_val = SYSTEM_GPIOCTRL;
        cur_reg_val |=
            VTSS_BIT((SYSTEM_GPIOCTRL_F_REGCTRL_FPOS + PTP_CLOCK_GPIO));
        SYSTEM_GPIOCTRL = cur_reg_val;
        vtss_global_unlock(__FILE__, __LINE__);
#endif
        PTP_RC(calc_clock_option(adj_method, VTSS_APPL_PTP_PROFILE_NO_PROFILE, &sw_clock_offset[time_domain].clock_option));
        if (sw_clock_offset[time_domain].clock_option == CLOCK_OPTION_DAC) {
            if (vtss_dac_clock_init_adjtimer(cold)) {
                T_DG(_C,"AD5667 DAC Clock adjustment enabled");
            }
        }

    } else if (time_domain < MESA_CAP(MESA_CAP_TS_DOMAIN_CNT)) {
        sw_clock_offset[time_domain].clock_option = CLOCK_OPTION_INTERNAL_TIMER;
    } else {
        sw_clock_offset[time_domain].clock_option = CLOCK_OPTION_SOFTWARE;
    }
    CLOCK_UNLOCK();
}

void vtss_local_clock_time_get(mesa_timestamp_t *t, int instance, u32 *hw_time)
{
    u32 time_domain = ptp_instance_2_timing_domain(instance);
    T_DG(_C,"instance %d, time_domain %d",instance, time_domain);
    if (time_domain < MESA_CAP(MESA_CAP_TS_DOMAIN_CNT)) {
        vtss_tod_gettimeofday(time_domain, t, hw_time);
    } else {
        mesa_timeinterval_t deltat;
        vtss_tod_gettimeofday(0, t, hw_time);
        CLOCK_LOCK();
        vtss_tod_sub_TimeInterval(&deltat, t, &sw_clock_offset[time_domain].t0);
        deltat = deltat/(1<<16); /* normalize to ns */
        deltat = (deltat*sw_clock_offset[time_domain].ratio);
        deltat = (deltat/VTSS_ONE_MIA)<<16;
        vtss_tod_add_TimeInterval(t, t, &deltat);
        vtss_tod_add_TimeInterval(t, t, &sw_clock_offset[time_domain].drift);
        t->seconds += sw_clock_offset[time_domain].ptp_offset;
        CLOCK_UNLOCK();
    }
}


void vtss_local_clock_time_set(const mesa_timestamp_t *t, u32 time_domain)
{
    char buf [40];
    mesa_timestamp_t thw = {0,0,0};
    mesa_timestamp_t my_time;
    i32 ptp_offset;
    u32 tc;
    T_IG(_C,"Set timeofday domain %d, time %s", time_domain, TimeStampToString(t,buf));
    if (time_domain < MESA_CAP(MESA_CAP_TS_DOMAIN_CNT)) {
        vtss_tod_settimeofday(time_domain, t);
        ptp_time_setting_start();
    } else {
        vtss_tod_gettimeofday(0, &thw, &tc);
        ptp_offset = t->seconds - thw.seconds;
        //if (t->nanoseconds > (500000000 + thw.nanoseconds)) {
        //    ++ptp_offset;
        //    T_IG(_C,"increment ptp_offset");
        //} else if (thw.nanoseconds > (500000000 + t->nanoseconds)) {
        //    --ptp_offset;
        //    T_IG(_C,"decrement ptp_offset");
        //}
        CLOCK_LOCK();
        sw_clock_offset[time_domain].t0 = *t;
        sw_clock_offset[time_domain].drift = ((mesa_timeinterval_t)(t->nanoseconds) - (mesa_timeinterval_t)(thw.nanoseconds))<<16;
        vtss_tod_add_TimeInterval(&my_time, &thw, &sw_clock_offset[time_domain].drift);
        my_time.seconds += ptp_offset;

        T_IG(_C,"drift: %s",vtss_tod_TimeInterval_To_String(&sw_clock_offset[time_domain].drift,buf,0));
        T_IG(_C,"his time:t_sec = %d,  t_nsec = %d, ptp_offset = %d",t->seconds, t->nanoseconds, ptp_offset);
        T_IG(_C,"my time :t_sec = %d,  t_nsec = %d",my_time.seconds, my_time.nanoseconds);
        sw_clock_offset[time_domain].ptp_offset = ptp_offset;
        sw_clock_offset[time_domain].set_time_count++;
        CLOCK_UNLOCK();
    }
}

#define TIMEINTERVAL_1SEC (1000000000LL<<16)

void vtss_local_clock_time_set_delta(const mesa_timestamp_t *t, u32 time_domain, BOOL negative)
{
    char buf [40];
    T_IG(_C,"Set delta timeofday domain %d, time %s",time_domain, TimeStampToString(t,buf));
    if (time_domain < MESA_CAP(MESA_CAP_TS_DOMAIN_CNT)) {
        vtss_tod_settimeofday_delta(time_domain, t, negative);
        ptp_time_setting_start();

    } else {
        CLOCK_LOCK();
        if (negative) {
            sw_clock_offset[time_domain].ptp_offset -= t->seconds;
            sw_clock_offset[time_domain].drift -= ((mesa_timeinterval_t)t->nanoseconds)<<16;
            while (sw_clock_offset[time_domain].drift < -TIMEINTERVAL_1SEC) {
                sw_clock_offset[time_domain].drift += TIMEINTERVAL_1SEC;
                sw_clock_offset[time_domain].ptp_offset--;
            }
        } else {
            sw_clock_offset[time_domain].ptp_offset += t->seconds;
            sw_clock_offset[time_domain].drift += ((mesa_timeinterval_t)t->nanoseconds)<<16;
            while (sw_clock_offset[time_domain].drift > TIMEINTERVAL_1SEC) {
                sw_clock_offset[time_domain].drift -= TIMEINTERVAL_1SEC;
                sw_clock_offset[time_domain].ptp_offset++;
            }
        }

        T_IG(_C,"drift: %s",vtss_tod_TimeInterval_To_String(&sw_clock_offset[time_domain].drift,buf,0));
        T_IG(_C,"delta time:t_sec = %d,  t_nsec = %d, negative %d",t->seconds, t->nanoseconds, negative);
        sw_clock_offset[time_domain].set_time_count++;
        CLOCK_UNLOCK();
    }
}


void vtss_local_clock_convert_to_time(u32 cur_time, mesa_timestamp_t *t, int instance)
{
    char buf1 [20];
    char buf2 [20];
    u32 time_domain = ptp_instance_2_timing_domain(instance);
    vtss_tod_ts_to_time(time_domain,  cur_time, t);

    CLOCK_LOCK();
    T_DG(_C,"instance %d, time_domain %d",instance, time_domain);
    if (time_domain < MESA_CAP(MESA_CAP_TS_DOMAIN_CNT)) {
        t->seconds += sw_clock_offset[time_domain].ptp_offset;
    } else {
        mesa_timeinterval_t deltat;
        vtss_tod_sub_TimeInterval(&deltat, t, &sw_clock_offset[time_domain].t0);
        deltat = deltat/(1<<16); /* normalize to ns */
        deltat = (deltat*sw_clock_offset[time_domain].ratio);
        deltat = (deltat/VTSS_ONE_MIA)<<16;
        vtss_tod_add_TimeInterval(t, t, &deltat);
        vtss_tod_add_TimeInterval(t, t, &sw_clock_offset[time_domain].drift);
        t->seconds += sw_clock_offset[time_domain].ptp_offset;
        T_IG(_C,"time_domain = %d,  deltat = %s,drift = %s",time_domain, vtss_tod_TimeInterval_To_String(&deltat,buf1,0),
             vtss_tod_TimeInterval_To_String(&sw_clock_offset[time_domain].drift,buf2,0));


    }

    T_DG(_C,"t_sec = %d,  t_nsec = %d, ptp_offset = %d",t->seconds, t->nanoseconds, sw_clock_offset[time_domain].ptp_offset);
    CLOCK_UNLOCK();
}

void vtss_local_clock_convert_to_hw_tc(u32 ns, u32 *cur_time)
{
    *cur_time = mesa_packet_ns_to_ts_cnt(NULL, ns, cur_time);
    T_NG(_C,"ns = %u,  cur_time = %u",ns, *cur_time);
}

static i64 actual_adj[PTP_CLOCK_INSTANCES] = {0LL, 0LL, 0LL, 0LL};

i64 vtss_local_clock_ratio_get(u32 domain)
{
    T_DG(_C,"domain %d, adj " VPRI64d ".", domain, actual_adj[domain]);
    return actual_adj[domain];
}

void vtss_local_clock_ratio_set(i64 adj, u32 time_domain)
{
    char buf1 [20];
    char buf2 [20];
    int current_option;
    T_IG(_C,"frequency adjustment. domain %d, adj " VPRI64d ".", time_domain, adj);
    //vtss_ptp__clock_mode_t my_new_mode;
    if (adj > ADJ_FREQ_MAX_LL*ADJ_SCALE)
        adj = ADJ_FREQ_MAX_LL*ADJ_SCALE;
    else if (adj < -ADJ_FREQ_MAX_LL*ADJ_SCALE)
        adj = -ADJ_FREQ_MAX_LL*ADJ_SCALE;
    CLOCK_LOCK();
    current_option = vtss_ptp_adjustment_option(time_domain);
    // if (adj != actual_adj[time_domain]) {  // FIXME: Have disabled this check as it is not reliable. Always do adjustment when asked to do so. Do not rely on actual value being known.
    if (1) {                                  // NOTE: This line has been inserted as a replacement for the line above.
        T_DG(_C,"before: adj " VPRI64d ", actual_adj " VPRI64d, adj, actual_adj[time_domain]);
        if (adj  > actual_adj[time_domain] + CLOCK_ADJ_SLEW_RATE) adj = actual_adj[time_domain] + CLOCK_ADJ_SLEW_RATE;
        if (adj  < actual_adj[time_domain] - CLOCK_ADJ_SLEW_RATE) adj = actual_adj[time_domain] - CLOCK_ADJ_SLEW_RATE;
        T_DG(_C,"after: adj " VPRI64d, adj);
        if (time_domain == 0) {
            if (current_option == CLOCK_OPTION_INTERNAL_TIMER) {
        /* PTP Module should not lock here for the adjtimer if remote_ts is enabled */



                vtss_tod_set_adjtimer(time_domain, adj);



            } else if (current_option == CLOCK_OPTION_PTP_DPLL) {
                // Do frequency adjustment in PTP reference clock.
#if defined(VTSS_SW_OPTION_SYNCE_DPLL)
                clock_output_adjtimer_set(adj);
#else
                T_WG(_C,"No support for Sync_XO in current HW");
#endif
            } else if (current_option == CLOCK_OPTION_DAC) {
                if (vtss_dac_clock_set_adjtimer(adj)) {
                    T_DG(_C,"AD5667 DAC Clock adjustment");
                }
#if defined(VTSS_SW_OPTION_SYNCE_DPLL)
            } else if (current_option == CLOCK_OPTION_SYNCE_DPLL) {
                // Do frequency adjustment in Controller (in DCO mode).
                if (clock_adjtimer_set(adj)) {
                    T_DG(_C,"Synce Clock adjustment");
                }
            } else if (current_option == CLOCK_OPTION_PTP_DPLL) {
                // Do frequency adjustment in Synthesizer (in DCO mode).
                PTP_RC(clock_output_adjtimer_set(adj));
#endif
            } else {
                T_WG(_C,"undefined clock adj method %d", current_option);
            }

        } else if (time_domain < MESA_CAP(MESA_CAP_TS_DOMAIN_CNT)) {
            vtss_tod_set_adjtimer(time_domain, adj);
        } else {
            mesa_timeinterval_t deltat;
            mesa_timestamp_t t = {0,0,0};
            u32 tc;
            vtss_tod_gettimeofday(0, &t, &tc);
            vtss_tod_sub_TimeInterval(&deltat, &t, &sw_clock_offset[time_domain].t0);
            T_NG(_C,"deltat = %s, ratio " VPRI64d,vtss_tod_TimeInterval_To_String(&deltat,buf1,0), sw_clock_offset[time_domain].ratio);
            deltat = deltat/(1<<16); /* normalize to ns */
            deltat = (deltat*sw_clock_offset[time_domain].ratio);
            T_NG(_C,"deltat = " VPRI64d,deltat);
            deltat = (deltat/VTSS_ONE_MIA)<<16;
            sw_clock_offset[time_domain].drift += deltat;

            if (sw_clock_offset[time_domain].drift > ((mesa_timeinterval_t)VTSS_ONE_MIA)<<16) {
                ++sw_clock_offset[time_domain].ptp_offset;
                sw_clock_offset[time_domain].drift -= ((mesa_timeinterval_t)VTSS_ONE_MIA)<<16;
                T_IG(_C,"drift adjusted = %s ptp_offset = %d",vtss_tod_TimeInterval_To_String(&deltat,buf1,0), sw_clock_offset[time_domain].ptp_offset);
            } else if (deltat < (-((mesa_timeinterval_t)VTSS_ONE_MIA))<<16) {
                --sw_clock_offset[time_domain].ptp_offset;
                sw_clock_offset[time_domain].drift += ((mesa_timeinterval_t)VTSS_ONE_MIA)<<16;
                T_IG(_C,"drift adjusted = %s, ptp_offset = %d",vtss_tod_TimeInterval_To_String(&deltat,buf1,0), sw_clock_offset[time_domain].ptp_offset);
            }
            sw_clock_offset[time_domain].t0 = t;
            sw_clock_offset[time_domain].ratio = adj/ADJ_SCALE;
            T_DG(_C,"adj = " VPRI64d ",  deltat = %s, drift = %s",adj,
                 vtss_tod_TimeInterval_To_String(&deltat,buf1,0), vtss_tod_TimeInterval_To_String(&sw_clock_offset[time_domain].drift,buf2,0));
        }
    }
    T_IG(_C,"frequency adjustment: adj = " VPRI64d, adj);
    actual_adj[time_domain] = adj;
    CLOCK_UNLOCK();
}

void vtss_local_clock_ratio_clear(u32 time_domain)
{
    char buf1 [20];
    char buf2 [20];
    i64 adj = 0;
    int current_option;
    T_IG(_C,"frequency adjustment clear. domain %d", time_domain);

    CLOCK_LOCK();
    current_option = vtss_ptp_adjustment_option(time_domain);
    if (time_domain == 0) {
        if (adj != actual_adj[time_domain]) {
            if (current_option == CLOCK_OPTION_INTERNAL_TIMER) {
                /* PTP Module should not lock here for the adjtimer if remote_ts is enabled */



                vtss_tod_set_adjtimer(time_domain, adj);



            } else if (current_option == CLOCK_OPTION_PTP_DPLL) {
                // Do frequency adjustment in PTP reference clock.
#if defined(VTSS_SW_OPTION_SYNCE_DPLL)
                clock_output_adjtimer_set(adj);
#else
                T_WG(_C,"No support for Sync_XO in current HW");
#endif
            } else if (current_option == CLOCK_OPTION_DAC) {
                if (vtss_dac_clock_set_adjtimer(adj)) {
                    T_DG(_C,"AD5667 DAC Clock adjustment");
                }
#if defined(VTSS_SW_OPTION_SYNCE_DPLL)
            } else if (current_option == CLOCK_OPTION_SYNCE_DPLL) {
                if (clock_adjtimer_set(adj)) {
                    T_DG(_C,"SyncE Clock adjustment");
                }
            } else if (current_option == CLOCK_OPTION_PTP_DPLL) {
                // Do frequency adjustment in Synthesizer (in DCO mode).
                PTP_RC(clock_output_adjtimer_set(adj));
#endif
            } else {
                T_WG(_C,"undefined clock adj method %d", current_option);
            }

            T_IG(_C,"frequency adjustment: adj = " VPRI64d, adj);
            actual_adj[time_domain] = adj;
        }

    } else if (time_domain < MESA_CAP(MESA_CAP_TS_DOMAIN_CNT)) {
        vtss_tod_set_adjtimer(time_domain, adj);
    } else {
        mesa_timeinterval_t deltat;
        mesa_timestamp_t t = {0,0,0};
        u32 tc;
        vtss_tod_gettimeofday(0, &t, &tc);
        vtss_tod_sub_TimeInterval(&deltat, &t, &sw_clock_offset[time_domain].t0);
        T_NG(_C,"deltat = %s, ratio " VPRI64d,vtss_tod_TimeInterval_To_String(&deltat,buf1,0), sw_clock_offset[time_domain].ratio);
        deltat = deltat/(1<<16); /* normalize to ns */
        deltat = (deltat*sw_clock_offset[time_domain].ratio);
        T_NG(_C,"deltat = " VPRI64d,deltat);
        deltat = (deltat/VTSS_ONE_MIA)<<16;
        sw_clock_offset[time_domain].drift += deltat;

        if (sw_clock_offset[time_domain].drift > ((mesa_timeinterval_t)VTSS_ONE_MIA)<<16) {
            ++sw_clock_offset[time_domain].ptp_offset;
            sw_clock_offset[time_domain].drift -= ((mesa_timeinterval_t)VTSS_ONE_MIA)<<16;
            T_IG(_C,"drift adjusted = %s ptp_offset = %d",vtss_tod_TimeInterval_To_String(&deltat,buf1,0), sw_clock_offset[time_domain].ptp_offset);
        } else if (deltat < (-((mesa_timeinterval_t)VTSS_ONE_MIA))<<16) {
            --sw_clock_offset[time_domain].ptp_offset;
            sw_clock_offset[time_domain].drift += ((mesa_timeinterval_t)VTSS_ONE_MIA)<<16;
            T_IG(_C,"drift adjusted = %s, ptp_offset = %d",vtss_tod_TimeInterval_To_String(&deltat,buf1,0), sw_clock_offset[time_domain].ptp_offset);
        }
        sw_clock_offset[time_domain].t0 = t;
        sw_clock_offset[time_domain].ratio = adj/ADJ_SCALE;
        T_DG(_C,"adj = " VPRI64d ",  deltat = %s, drift = %s",adj,
             vtss_tod_TimeInterval_To_String(&deltat,buf1,0), vtss_tod_TimeInterval_To_String(&sw_clock_offset[time_domain].drift,buf2,0));
    }
    CLOCK_UNLOCK();
}

void vtss_local_clock_fine_adj_offset(i64 offset, u32 domain)
{
#if defined(VTSS_SW_OPTION_SYNCE_DPLL)
    if (domain == 0 && features == SYNCE_CLOCK_FEATURE_DUAL) {
        if (offset <= (32767<<16) && offset >= (-32768<<16)) {
            PTP_RC(clock_adj_phase_set(offset));
        } else {
            T_WG(_C,"clock offset adj %d ns: too big", (u32)(offset>>16));
        }
    } else {
#else
    {
#endif
        i32 ns_phase = (offset>>16);
        /* limit the max phase adjustment pr adjustment */
        if (ns_phase > MESA_HW_TIME_MAX_FINE_ADJ) ns_phase = MESA_HW_TIME_MAX_FINE_ADJ;
        if (ns_phase < -MESA_HW_TIME_MAX_FINE_ADJ) ns_phase = -MESA_HW_TIME_MAX_FINE_ADJ;
        vtss_local_clock_adj_offset(ns_phase, domain);
    }
}

void vtss_local_clock_adj_offset(i32 offset, u32 time_domain)
{
    CLOCK_LOCK();
    T_IG(_C,"offset adjustment. domain %d, offset %d.", time_domain, offset);
    if (time_domain < MESA_CAP(MESA_CAP_TS_DOMAIN_CNT)) {
        if (offset > MESA_HW_TIME_MAX_FINE_ADJ || offset < -MESA_HW_TIME_MAX_FINE_ADJ) {
            ptp_time_setting_start();
        }
        if (offset != 0) {
            PTP_RC(mesa_ts_domain_timeofday_offset_set(NULL, time_domain, offset));
            T_IG(_C,"clock offset adj %d", offset);
        }

    } else {
        T_IG(_C,"offset adjustment: %d ns for time_domain %d", offset, time_domain);
        sw_clock_offset[time_domain].drift -= (i64)offset<<16;

        if (sw_clock_offset[time_domain].drift > ((mesa_timeinterval_t)VTSS_ONE_MIA)<<16) {
            ++sw_clock_offset[time_domain].ptp_offset;
            sw_clock_offset[time_domain].drift -= ((mesa_timeinterval_t)VTSS_ONE_MIA)<<16;
            T_IG(_C,"drift adjusted = %d ptp_offset = %d",offset, sw_clock_offset[time_domain].ptp_offset);
        } else if (sw_clock_offset[time_domain].drift < ((mesa_timeinterval_t)0)) {
            --sw_clock_offset[time_domain].ptp_offset;
            sw_clock_offset[time_domain].drift += ((mesa_timeinterval_t)VTSS_ONE_MIA)<<16;
            T_IG(_C,"drift adjusted = %d, ptp_offset = %d",offset, sw_clock_offset[time_domain].ptp_offset);
        }
    }
    sw_clock_offset[time_domain].set_time_count++;
    CLOCK_UNLOCK();
}

int vtss_ptp_adjustment_method(int instance)
{
    int option;
    CLOCK_LOCK();
    option = vtss_ptp_adjustment_option(ptp_instance_2_timing_domain(instance));
    CLOCK_UNLOCK();
    return option;
}

u32 vtss_local_clock_set_time_count_get(int instance)
{
    u32 count;
    CLOCK_LOCK();
    count =  sw_clock_offset[ptp_instance_2_timing_domain(instance)].set_time_count;
    CLOCK_UNLOCK();
    return count;
}

void vtss_local_clock_set_time_count_incr(u32 domain)
{
    CLOCK_LOCK();
    sw_clock_offset[domain].set_time_count++;
    CLOCK_UNLOCK();
}

