/*	$NetBSD: fetch.c,v 1.19 2009/08/11 20:48:06 joerg Exp $	*/
/*-
 * Copyright (c) 1998-2004 Dag-Erling Co�dan Sm�rgrav
 * Copyright (c) 2008 Joerg Sonnenberger <joerg@NetBSD.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer
 *    in this position and unchanged.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD: fetch.c,v 1.41 2007/12/19 00:26:36 des Exp $
 */

#include "libfetch_config.h"

#if !defined(NETBSD) && !defined(OS_ECOS) && !defined(OS_LINUX)
#include <nbcompat.h>
#endif

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fetch.h"
#include "common.h"

#if defined(OS_LINUX)
#define TLS __thread
#elif defined(OS_ECOS)
#define TLS
static int initialized, thr_index;
#endif
static TLS fetch_instance_t _default_instance = { 3, 1 };

fetch_instance_t *_fetch_instance(void)
{
#if defined(OS_ECOS)
    vtss_global_lock(__FILE__, __LINE__);
    if (!initialized) {
        thr_index = vtss_thread_new_data_index();
        vtss_thread_set_data(thr_index, (vtss_addrword_t)&_default_instance);
        initialized++;
    }
    vtss_global_unlock(__FILE__, __LINE__);
    return (fetch_instance_t *)vtss_thread_get_data(thr_index);
#else
    return &_default_instance;
#endif
}

void
fetchInitInstance(fetch_instance_t *fi)
{
    memset(fi, 0, sizeof(*fi));
    fi->_fetchConnTimeout = 3;
    fi->_fetchRestartCalls = 1;
#if defined(OS_ECOS)
    if (_fetch_instance() != fi) {  /* NB: This also triggers initialization */
        vtss_thread_set_data(thr_index, (vtss_addrword_t)fi);
    }
#endif
}

/*** Local data **************************************************************/

/*
 * Error messages for parser errors
 */
#define URL_MALFORMED		1
#define URL_BAD_SCHEME		2
#define URL_BAD_PORT		3
#define URL_BAD_USERPWD     4
static struct fetcherr url_errlist[] = {
	{ URL_MALFORMED,	FETCH_URL,	"Malformed URL" },
	{ URL_BAD_SCHEME,	FETCH_URL,	"Invalid URL scheme" },
	{ URL_BAD_PORT,		FETCH_URL,	"Invalid server port" },
	{ URL_BAD_USERPWD,  FETCH_URL,	"Invalid username or password" },
	{ -1,			FETCH_UNKNOWN,	"Unknown parser error" }
};


/*** Public API **************************************************************/

/*
 * Select the appropriate protocol for the URL scheme, and return a
 * read-only stream connected to the document referenced by the URL.
 * Also fill out the struct url_stat.
 */
fetchIO *
fetchXGet(struct url *URL, struct url_stat *us, const char *flags)
{

	if (us != NULL) {
		us->size = -1;
		us->atime = us->mtime = 0;
	}
	if (strcasecmp(URL->scheme, SCHEME_FILE) == 0)
		return (fetchXGetFile(URL, us, flags));
	else if (strcasecmp(URL->scheme, SCHEME_FTP) == 0)
		return (fetchXGetFTP(URL, us, flags));
	else if (strcasecmp(URL->scheme, SCHEME_HTTP) == 0)
		return (fetchXGetHTTP(URL, us, flags));
	else if (strcasecmp(URL->scheme, SCHEME_HTTPS) == 0)
		return (fetchXGetHTTP(URL, us, flags));
	url_seterr(URL_BAD_SCHEME);
	return (NULL);
}

/*
 * Select the appropriate protocol for the URL scheme, and return a
 * read-only stream connected to the document referenced by the URL.
 */
fetchIO *
fetchGet(struct url *URL, const char *flags)
{
	return (fetchXGet(URL, NULL, flags));
}

/*
 * Select the appropriate protocol for the URL scheme, and return a
 * write-only stream connected to the document referenced by the URL.
 */
fetchIO *
fetchPut(struct url *URL, const char *flags, const char *xmit, size_t xlen)
{

	if (strcasecmp(URL->scheme, SCHEME_FILE) == 0)
		return (fetchPutFile(URL, flags));
	else if (strcasecmp(URL->scheme, SCHEME_FTP) == 0)
		return (fetchPutFTP(URL, flags));
	else if (strcasecmp(URL->scheme, SCHEME_HTTP) == 0)
		return (fetchPutHTTP(URL, flags, xmit, xlen));
	else if (strcasecmp(URL->scheme, SCHEME_HTTPS) == 0)
		return (fetchPutHTTP(URL, flags, xmit, xlen));
	url_seterr(URL_BAD_SCHEME);
	return (NULL);
}

/*
 * Select the appropriate protocol for the URL scheme, and return the
 * size of the document referenced by the URL if it exists.
 */
int
fetchStat(struct url *URL, struct url_stat *us, const char *flags)
{

	if (us != NULL) {
		us->size = -1;
		us->atime = us->mtime = 0;
	}
	if (strcasecmp(URL->scheme, SCHEME_FILE) == 0)
		return (fetchStatFile(URL, us, flags));
	else if (strcasecmp(URL->scheme, SCHEME_FTP) == 0)
		return (fetchStatFTP(URL, us, flags));
	else if (strcasecmp(URL->scheme, SCHEME_HTTP) == 0)
		return (fetchStatHTTP(URL, us, flags));
	else if (strcasecmp(URL->scheme, SCHEME_HTTPS) == 0)
		return (fetchStatHTTP(URL, us, flags));
	url_seterr(URL_BAD_SCHEME);
	return (-1);
}

/*
 * Select the appropriate protocol for the URL scheme, and return a
 * list of files in the directory pointed to by the URL.
 */
int
fetchList(struct url_list *ue, struct url *URL, const char *pattern,
    const char *flags)
{

	if (strcasecmp(URL->scheme, SCHEME_FILE) == 0)
		return (fetchListFile(ue, URL, pattern, flags));
	else if (strcasecmp(URL->scheme, SCHEME_FTP) == 0)
		return (fetchListFTP(ue, URL, pattern, flags));
	else if (strcasecmp(URL->scheme, SCHEME_HTTP) == 0)
		return (fetchListHTTP(ue, URL, pattern, flags));
	else if (strcasecmp(URL->scheme, SCHEME_HTTPS) == 0)
		return (fetchListHTTP(ue, URL, pattern, flags));
	url_seterr(URL_BAD_SCHEME);
	return -1;
}

/*
 * Attempt to parse the given URL; if successful, call fetchXGet().
 */
fetchIO *
fetchXGetURL(const char *URL, struct url_stat *us, const char *flags)
{
	struct url *u;
	fetchIO *f;

	if ((u = fetchParseURL(URL)) == NULL)
		return (NULL);

	f = fetchXGet(u, us, flags);

	fetchFreeURL(u);
	return (f);
}

/*
 * Attempt to parse the given URL; if successful, call fetchGet().
 */
fetchIO *
fetchGetURL(const char *URL, const char *flags)
{
	return (fetchXGetURL(URL, NULL, flags));
}

/*
 * Attempt to parse the given URL; if successful, call fetchPut().
 */
fetchIO *
fetchPutURL(const char *URL, const char *flags, size_t xlen, const void *xmit)
{
	struct url *u;
	fetchIO *f;
	char	*xdata;

	if ((u = fetchParseURL(URL)) == NULL)
		return (NULL);

	xdata = NULL;
	if (xmit && (xlen > 0)) {
		if ((xdata = libfetch_callout_malloc(xlen)) != NULL) {
			memcpy(xdata, xmit, xlen);
		}
	}

	f = fetchPut(u, flags, xdata, xlen);

	fetchFreeURL(u);
	if (xdata != NULL) {
		libfetch_callout_free(xdata);
	}
	return (f);
}

/*
 * Attempt to parse the given URL; if successful, call fetchStat().
 */
int
fetchStatURL(const char *URL, struct url_stat *us, const char *flags)
{
	struct url *u;
	int s;

	if ((u = fetchParseURL(URL)) == NULL)
		return (-1);

	s = fetchStat(u, us, flags);

	fetchFreeURL(u);
	return (s);
}

/*
 * Attempt to parse the given URL; if successful, call fetchList().
 */
int
fetchListURL(struct url_list *ue, const char *URL, const char *pattern,
    const char *flags)
{
	struct url *u;
	int rv;

	if ((u = fetchParseURL(URL)) == NULL)
		return -1;

	rv = fetchList(ue, u, pattern, flags);

	fetchFreeURL(u);
	return rv;
}

/*
 * Make a URL
 */
struct url *
fetchMakeURL(const char *scheme, const char *host, int port, const char *doc,
    const char *user, const char *pwd)
{
	struct url *u;

	if (!scheme || (!host && !doc)) {
		url_seterr(URL_MALFORMED);
		return (NULL);
	}

	if (port < 0 || port > 65535) {
		url_seterr(URL_BAD_PORT);
		return (NULL);
	}

	/* allocate struct url */
	if ((u = libfetch_callout_calloc(1, sizeof(*u))) == NULL) {
		fetch_syserr();
		return (NULL);
	}

	if ((u->doc = libfetch_callout_strdup(doc ? doc : "/")) == NULL) {
		fetch_syserr();
		libfetch_callout_free(u);
		return (NULL);
	}

#define seturl(x) snprintf(u->x, sizeof(u->x), "%s", x)
	seturl(scheme);
	seturl(host);
	seturl(user);
	seturl(pwd);
#undef seturl
	u->port = port;

	return (u);
}

int
fetch_urlpath_safe(char x)
{
	if ((x >= '0' && x <= '9') || (x >= 'A' && x <= 'Z') ||
	    (x >= 'a' && x <= 'z'))
		return 1;

	switch (x) {
	case '$':
	case '-':
	case '_':
	case '.':
	case '+':
	case '!':
	case '*':
	case '\'':
	case '(':
	case ')':
	case ',':
	/* The following are allowed in segment and path components: */
	case '?':
	case ':':
	case '@':
	case '&':
	case '=':
	case '/':
	case ';':
	/* If something is already quoted... */
	case '%':
		return 1;
	default:
		return 0;
	}
}

/*
 * Copy an existing URL.
 */
struct url *
fetchCopyURL(const struct url *src)
{
	struct url *dst;
	char *doc;

	/* allocate struct url */
	if ((dst = libfetch_callout_malloc(sizeof(*dst))) == NULL) {
		fetch_syserr();
		return (NULL);
	}
	if ((doc = libfetch_callout_strdup(src->doc)) == NULL) {
		fetch_syserr();
		libfetch_callout_free(dst);
		return (NULL);
	}
	*dst = *src;
	dst->doc = doc;

	return dst;
}

/*
 * Check if there is any unsafe character in URL username/password string
 *
 * If the following special characters: space !"#$%&'()*+,/:;<=>?@[\]^`{|}~
 * need to be contained in the input url string, they should have percent-encoded.
 *
 * Return 1 when success, return 0 otherwise.
 */
static int fetch_urluserpwd_safe(const char *user_or_pwd, size_t len)
{
    const char *c;
    size_t process_idx;

    for (process_idx = 0, c = user_or_pwd; process_idx < len; process_idx++, c++) {
	    if ((*c >= '0' && *c <= '9') || (*c >= 'A' && *c <= 'Z') || (*c >= 'a' && *c <= 'z')) {
            continue;
        }

	    switch (*c) {
	    case '-':
	    case '_':
	    case '.':
            break;
	    case '%':
	        // Check if the two next chars are hexadecimal.
	        if (process_idx >= (len - 2) ||
	            !isxdigit(*(c+1)) ||
	            !isxdigit(*(c+2))) {
	            // Not match format [% hex hex]
	            return 0;
	        }
            break;
        default:
	    	return 0;
	    }
	}

    return 1;
}

static int fetch_a16toi(char ch)
{
    if (ch >= '0' && ch <= '9') {
        return ch - '0';
    }
    return toupper(ch) - 'A' + 10;
}

/*
 * If the following special characters: space !"#$%&'()*+,/:;<=>?@[\]^`{|}~
 * need to be contained in the input url string, they should have percent-encoded.
 *
 * The special characters are encoded by a character triplet consisting
 * of the character "%" followed by the two hexadecimal digits (from
 * "0123456789ABCDEF") which forming the hexadecimal value of the octet.
 * For example, '!' is encoded to '%21', '@' is encoded to '%40'.
 *
 * The reserved characters is based on RFC 3986 and here is the mapping list.
 * space  !   "   #   $   %   &   '   (   )   *   +   ,   /   :   ;   <   =   >   ?   @   [   \   ]   ^   `   {   |   }   ~
 * %20    %21 %22 %23 %24 %25 %26 %27 %28 %29 %2A %2B %2C %2F %3A %3B %3C %3D %3E %3F %40 %5B %5C %5D %5E %60 %7B %7C %7D %7E
 *
 * Return 1 when decode success, return 0 otherwise.
 */
static int fetch_url_user_pwd_decode(const char *from, char *to, unsigned int from_len, unsigned int to_len)
{
    unsigned int from_i = 0, to_i = 0;
    char ch;

    if (to_len == 0) {
        return 0;
    }

    while (from_i < from_len) {
        if (from[from_i] == '%') {
            // Check if there are chars enough left in @from to complete the conversion
            if (from_i + 2 >= from_len) {
                // There aren't.
                return 0;
            }

            // Check if the two next chars are hexadecimal.
            if (!isxdigit(from[from_i + 1])  || !isxdigit(from[from_i + 2])) {
                return 0;
            }

            ch = 16 * fetch_a16toi(from[from_i + 1])  + fetch_a16toi(from[from_i + 2]);

            from_i += 3;
        } else {
            ch = from[from_i++];
        }

        to[to_i++] = ch;
        if (to_i == to_len) {
            // Not room for trailing '\0'
            return 0;
        }
    }

    to[to_i] = '\0';
    return 1;
}

/*
 * Split an URL into components. URL syntax is:
 * [method:/][/[user[:pwd]@]host[:port]/][document]
 * This almost, but not quite, RFC1738 URL syntax.
 *
 * If the following special characters: space !"#$%&'()*+,/:;<=>?@[\]^`{|}~
 * need to be contained in the input url string, they should have percent-encoded.
 */
struct url *
fetchParseURL(const char *URL)
{
	const char *p, *q;
	struct url *u;
	size_t i, count;
	int pre_quoted;

	/* allocate struct url */
	if ((u = libfetch_callout_calloc(1, sizeof(*u))) == NULL) {
		fetch_syserr();
		return (NULL);
	}

	if (*URL == '/') {
		pre_quoted = 0;
		strcpy(u->scheme, SCHEME_FILE);
		p = URL;
		goto quote_doc;
	}
	if (strncmp(URL, "file:", 5) == 0) {
		pre_quoted = 1;
		strcpy(u->scheme, SCHEME_FILE);
		URL += 5;
		if (URL[0] != '/' || URL[1] != '/' || URL[2] != '/') {
			url_seterr(URL_MALFORMED);
			goto ouch;
		}
		p = URL + 2;
		goto quote_doc;
	}
	if (strncmp(URL, "http:", 5) == 0 ||
	    strncmp(URL, "https:", 6) == 0) {
		pre_quoted = 1;
		if (URL[4] == ':') {
			strcpy(u->scheme, SCHEME_HTTP);
			URL += 5;
		} else {
			strcpy(u->scheme, SCHEME_HTTPS);
			URL += 6;
		}

		if (URL[0] != '/' || URL[1] != '/') {
			url_seterr(URL_MALFORMED);
			goto ouch;
		}
		URL += 2;
		p = URL;
		goto find_user;
	}
	if (strncmp(URL, "ftp:", 4) == 0) {
		pre_quoted = 1;
		strcpy(u->scheme, SCHEME_FTP);
		URL += 4;
		if (URL[0] != '/' || URL[1] != '/') {
			url_seterr(URL_MALFORMED);
			goto ouch;
		}
		URL += 2;
		p = URL;
		goto find_user;			
	}

	url_seterr(URL_BAD_SCHEME);
	goto ouch;

find_user:
	p = strpbrk(URL, "/@");
	if (p != NULL && *p == '@') {
	    char encoded_user[URL_USERLEN + 1], encoded_pwd[URL_PWDLEN + 1];

		/* username */
		memset(encoded_user, 0, sizeof(encoded_user));
		for (q = URL, i = 0; (*q != ':') && (*q != '@'); q++) {
			if (i < URL_USERLEN)
				encoded_user[i++] = *q;
		}

		// Decode username string
		if (!fetch_urluserpwd_safe(encoded_user, i) ||
		    !fetch_url_user_pwd_decode(encoded_user, u->user, strlen(encoded_user), URL_USERLEN)) {
			url_seterr(URL_BAD_USERPWD);
			goto ouch;
		}

		/* password */
		memset(encoded_pwd, 0, sizeof(encoded_pwd));
		if (*q == ':') {
			for (q++, i = 0; (*q != '@'); q++)
				if (i < URL_PWDLEN)
					encoded_pwd[i++] = *q;

			// Decode password string
			if (!fetch_urluserpwd_safe(encoded_pwd, i) ||
			    !fetch_url_user_pwd_decode(encoded_pwd, u->pwd, strlen(encoded_pwd), URL_PWDLEN)) {
				url_seterr(URL_BAD_USERPWD);
				goto ouch;
			}
		}

		p++;
	} else {
		p = URL;
	}

	/* hostname */
#ifdef INET6
	if (*p == '[' && (q = strchr(p + 1, ']')) != NULL &&
	    (*++q == '\0' || *q == '/' || *q == ':')) {
		if ((i = q - p - 2) > URL_HOSTLEN)
			i = URL_HOSTLEN;
		strncpy(u->host, ++p, i);
		p = q;
	} else
#endif
		for (i = 0; *p && (*p != '/') && (*p != ':'); p++)
			if (i < URL_HOSTLEN)
				u->host[i++] = *p;

	/* port */
	if (*p == ':') {
		for (q = ++p; *q && (*q != '/'); q++)
			if (isdigit((unsigned char)*q))
				u->port = u->port * 10 + (*q - '0');
			else {
				/* invalid port */
				url_seterr(URL_BAD_PORT);
				goto ouch;
			}
		p = q;
	}

	/* document */
	if (!*p)
		p = "/";

quote_doc:
	count = 1;
	for (i = 0; p[i] != '\0'; ++i) {
		if ((!pre_quoted && p[i] == '%') ||
		    !fetch_urlpath_safe(p[i]))
			count += 3;
		else
			++count;
	}

	if ((u->doc = libfetch_callout_malloc(count)) == NULL) {
		fetch_syserr();
		goto ouch;
	}
	for (i = 0; *p != '\0'; ++p) {
		if ((!pre_quoted && *p == '%') ||
		    !fetch_urlpath_safe(*p)) {
			u->doc[i++] = '%';
			if ((unsigned char)*p < 160)
				u->doc[i++] = '0' + ((unsigned char)*p) / 16;
			else
				u->doc[i++] = 'a' - 10 + ((unsigned char)*p) / 16;
			if ((unsigned char)*p % 16 < 10)
				u->doc[i++] = '0' + ((unsigned char)*p) % 16;
			else
				u->doc[i++] = 'a' - 10 + ((unsigned char)*p) % 16;
		} else
			u->doc[i++] = *p;
	}
	u->doc[i] = '\0';

	return (u);

ouch:
	libfetch_callout_free(u);
	return (NULL);
}

/*
 * Free a URL
 */
void
fetchFreeURL(struct url *u)
{
	libfetch_callout_free(u->doc);
	libfetch_callout_free(u);
}

static char
xdigit2digit(char digit)
{
	digit = tolower((unsigned char)digit);
	if (digit >= 'a' && digit <= 'f')
		digit = digit - 'a' + 10;
	else
		digit = digit - '0';

	return digit;
}

/*
 * Unquote whole URL.
 * Skips optional parts like query or fragment identifier.
 */ 
char *
fetchUnquotePath(struct url *url)
{
	char *unquoted;
	const char *iter;
	size_t i;

	if ((unquoted = libfetch_callout_malloc(strlen(url->doc) + 1)) == NULL)
		return NULL;

	for (i = 0, iter = url->doc; *iter != '\0'; ++iter) {
		if (*iter == '#' || *iter == '?')
			break;
		if (iter[0] != '%' ||
		    !isxdigit((unsigned char)iter[1]) ||
		    !isxdigit((unsigned char)iter[2])) {
			unquoted[i++] = *iter;
			continue;
		}
		unquoted[i++] = xdigit2digit(iter[1]) * 16 +
		    xdigit2digit(iter[2]);
		iter += 2;
	}
	unquoted[i] = '\0';
	return unquoted;
}


/*
 * Extract the file name component of a URL.
 */
char *
fetchUnquoteFilename(struct url *url)
{
	char *unquoted, *filename;
	const char *last_slash;

	if ((unquoted = fetchUnquotePath(url)) == NULL)
		return NULL;

	if ((last_slash = strrchr(unquoted, '/')) == NULL)
		return unquoted;
	filename = libfetch_callout_strdup(last_slash + 1);
	libfetch_callout_free(unquoted);
	return filename;
}

char *
fetchStringifyURL(const struct url *url)
{
	size_t total;
	char *doc;

	/* scheme :// user : pwd @ host :port doc */
	total = strlen(url->scheme) + 3 + strlen(url->user) + 1 +
	    strlen(url->pwd) + 1 + strlen(url->host) + 6 + strlen(url->doc) + 1;
	if ((doc = libfetch_callout_malloc(total)) == NULL)
		return NULL;
	if (url->port != 0)
		snprintf(doc, total, "%s%s%s%s%s%s%s:%d%s",
		    url->scheme,
		    url->scheme[0] != '\0' ? "://" : "",
		    url->user,
		    url->pwd[0] != '\0' ? ":" : "",
		    url->pwd,
		    url->user[0] != '\0' || url->pwd[0] != '\0' ? "@" : "",
		    url->host,
		    (int)url->port,
		    url->doc);
	else {
		snprintf(doc, total, "%s%s%s%s%s%s%s%s",
		    url->scheme,
		    url->scheme[0] != '\0' ? "://" : "",
		    url->user,
		    url->pwd[0] != '\0' ? ":" : "",
		    url->pwd,
		    url->user[0] != '\0' || url->pwd[0] != '\0' ? "@" : "",
		    url->host,
		    url->doc);
	}
	return doc;
}
