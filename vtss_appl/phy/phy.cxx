/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "main.h"
#include "port_api.h"
#include "phy_api.h"
#include "phy.h"
#include "conf_api.h"
#include "critd_api.h"
#include "misc_api.h"
#include "phy_icfg.h"
#include <vtss/appl/macsec.hxx>
#include "mscc/ethernet/switch/api.h"
#include "vtss/basics/enum_macros.hxx" /* For VTSS_ENUM_INC() */
#include "vtss_optional_modules.hxx"

#define VTSS_MACSEC_ACTION_MAX MESA_MACSEC_MATCH_ACTION_CNT
#define VTSS_MACSEC_DIRECTION_MAX MESA_MACSEC_DIRECTION_CNT

#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>

/* Global configuration for the module */
static phy_global_t phy;
static BOOL inst_created_during_boot = FALSE;

/*lint -sem(mmd_read,   thread_protected) ... We're protected  */
/*lint -sem(mmd_write,  thread_protected) ... We're protected */
/*lint -sem(miim_read,  thread_protected) ... We're protected */
/*lint -sem(miim_write, thread_protected) ... We're protected */
/*lint -sem(mmd_read_inc, thread_protected) ... We're protected */
/*lint -sem(phy_mgmt_failover_set, thread_protected) ... We're protected */
/*lint -sem(phy_mgmt_failover_get, thread_protected) ... We're protected */
/*lint -sem(phy_mgmt_inst_activate_default, thread_protected) ... We're protected */
/*lint -sem(phy_channel_failover, thread_protected) ... We're protected */

static mesa_rc vtss_phy_ts_base_port_get(const mesa_inst_t inst,
                           const mesa_port_no_t port_no,
                           mesa_port_no_t     *const base_port_no);

/* All PHY MIIM reads/writes comes through the following functions: */
static mesa_rc mmd_read(const mesa_inst_t     inst,
                        const mesa_port_no_t  port_no,
                        const u8              mmd,
                        const u16             addr,
                        u16                   *const value)
{
    /* Must use the MMD functions from the default API instance (and update the API pointer to the default state) */
    if (mesa_init_conf_get(NULL, &phy.init_conf_default) != VTSS_RC_OK) {
        return VTSS_RC_ERROR;
    }

    if (phy.init_conf_default.mmd_read(NULL, port_no,  mmd,  addr, value) != VTSS_RC_OK) {
        T_I("MMD read Failed");
        return VTSS_RC_ERROR;
    }
    return VTSS_RC_OK;
}

static mesa_rc mmd_write(const mesa_inst_t     inst,
                         const mesa_port_no_t  port_no,
                         const u8              mmd,
                         const u16             addr,
                         const u16             value)

{
    /* Must use the MMD functions from the default API instance (and update the API pointer to the default state) */
    if (mesa_init_conf_get(NULL, &phy.init_conf_default) != VTSS_RC_OK) {
        return VTSS_RC_ERROR;
    }
    if (phy.init_conf_default.mmd_write(NULL, port_no, mmd, addr, value) != VTSS_RC_OK) {
        T_I("MMD write Failed");
        return VTSS_RC_ERROR;
    }
    return VTSS_RC_OK;
}

static mesa_rc mmd_read_inc(const mesa_inst_t     inst,
                            const mesa_port_no_t  port_no,
                            const u8              mmd,
                            const u16             addr,
                            u16                   *buf,
                            u8                    count)
{
   /* Must use the MMD functions from the default API instance (and update the API pointer to the default state) */
    if (mesa_init_conf_get(NULL, &phy.init_conf_default) != VTSS_RC_OK) {
        return VTSS_RC_ERROR;
    }

    if (phy.init_conf_default.mmd_read_inc(NULL, port_no, mmd, addr, buf, count) != VTSS_RC_OK) {
        T_I("MMD read inc Failed");
        return VTSS_RC_ERROR;
    }
    return VTSS_RC_OK;
}


static mesa_rc miim_read(const mesa_inst_t     inst,
                         const mesa_port_no_t  port_no,
                         const u8              addr,
                         u16                   *const value)
{
    /* Must use the MMD functions from the default API instance (and update the API pointer to the default state) */
    if (mesa_init_conf_get(NULL, &phy.init_conf_default) != VTSS_RC_OK) {
        return VTSS_RC_ERROR;
    }

    if (phy.init_conf_default.miim_read(NULL, port_no, addr, value) != VTSS_RC_OK) {
        T_E("MIIM read Failed");
        return VTSS_RC_ERROR;
    }
    return VTSS_RC_OK;
}


static mesa_rc miim_write(const mesa_inst_t     inst,
                          const mesa_port_no_t  port_no,
                          const u8              addr,
                          const u16             value)

{
    /* Must use the MMD functions from the default API instance (and update the API pointer to the default state) */
    if (mesa_init_conf_get(NULL, &phy.init_conf_default) != VTSS_RC_OK) {
        return VTSS_RC_ERROR;
    }
    if (phy.init_conf_default.miim_write(NULL, port_no, addr, value) != VTSS_RC_OK) {
        T_E("MIIM write Failed");
        return VTSS_RC_ERROR;
    }
    return VTSS_RC_OK;
}

static mesa_rc phy_inst_create(void)
{
    mesa_inst_create_t       create;
    mesa_init_conf_t         init_conf;
    mesa_port_no_t           port_no;
    meba_port_cap_t               cap;
    mesa_rc                  rc;

    if (phy.start_inst == PHY_INST_NONE) {
        phy.current_inst = NULL;
        return VTSS_RC_OK; /* The Phys are included in the Default API instance */
    }

    if (phy.start_inst == PHY_INST_10G_PHY && phy.mode) {
        /* Must roll through 10G Phys in reversed order because channel 0 comes last */
        for (port_no = mesa_port_cnt(nullptr)-1;;port_no--) {
            if (port_cap_get(port_no, &cap) != VTSS_OK) {
                T_E("Could not get port CAP info p:%u",port_no);
                continue;
            }
            if ((cap & MEBA_PORT_CAP_VTSS_10G_PHY)) {
                break;
            }
            if (port_no == VTSS_PORT_NO_START) {
                port_no = mesa_port_cnt(nullptr);
                break;
            }
        }
    } else if (phy.start_inst == PHY_INST_1G_PHY && phy.mode) {
        for (port_no = VTSS_PORT_NO_START; port_no < mesa_port_cnt(nullptr); port_no++) {
            if (port_cap_get(port_no, &cap) != VTSS_OK) {
                T_E("Could not get port CAP info p:%u",port_no);
                continue;
            }
            if (cap & MEBA_PORT_CAP_1G_PHY) {
                break;
            }            
        }
    } else {
        port_no = phy.start_port; /* port is manually configured to this value from cli */
    }

    T_I("Using port p:%d as a %s restart port\n",port_no, (phy.start_inst == PHY_INST_10G_PHY) ? "10G":"1G");
      
    if (mesa_capability(nullptr, MESA_CAP_PHY_10G)) {
        if (port_no == mesa_port_cnt(nullptr)) {
            if (inst_created_during_boot == TRUE) {
                T_E("Could not find a Phy to store Warm start info");
                return VTSS_RC_ERROR;
            } else {
                T_I("Phy iteration again to store Warm start info after Malibu detect");
                return VTSS_RC_INCOMPLETE; //special return value for Malibu warm-start workaround
            }
        }
    } else {
        if (port_no == mesa_port_cnt(nullptr)) {
            T_E("Could not find a Phy to store Warm start info");
            return VTSS_RC_ERROR;
        }
    }

    /* Create a 1G or 10G API instance */
    if ((rc = mesa_inst_get((phy.start_inst == PHY_INST_10G_PHY) ? MESA_TARGET_10G_PHY : MESA_TARGET_CU_PHY, &create)) != VTSS_RC_OK) {
        T_E("mesa_inst_get() Failed");
        return rc;
    }
    if ((rc = mesa_inst_create(&create, &phy.current_inst)) != VTSS_RC_OK) {
        T_E("mesa_inst_create() Failed");
        return rc;
    }
    /* Get the defaults */
    if ((rc = mesa_init_conf_get(phy.current_inst, &init_conf)) != VTSS_RC_OK) {
        T_E("mesa_init_conf_get() Failed");
        return rc;
    }

    /* Hook up our local call out functions */
    init_conf.mmd_read = mmd_read;
    init_conf.mmd_write = mmd_write;
    init_conf.mmd_read_inc = mmd_read_inc;
    init_conf.miim_read = miim_read;
    init_conf.miim_write = miim_write;

    /* Apply Warm start info */
    init_conf.warm_start_enable = 1;
    init_conf.restart_info_port = port_no;
    init_conf.restart_info_src = (phy.start_inst == PHY_INST_10G_PHY) ?  MESA_RESTART_INFO_SRC_10G_PHY : MESA_RESTART_INFO_SRC_CU_PHY;

    /* Save the phy instance */
    T_D("mesa_init_conf_set");
    if ((rc = mesa_init_conf_set(phy.current_inst, &init_conf)) != VTSS_RC_OK) {
        T_E("mesa_init_conf_set() Failed");
        return rc;
    }
    /* Unlock the VTSS_API  */
    return VTSS_RC_OK;
}

mesa_rc phy_inst_create_after_malibu_detect(void)
{
    mesa_rc                  rc = VTSS_RC_OK;
    if (inst_created_during_boot == FALSE) {
        VTSS_API_ENTER();
        rc = phy_inst_create();
        VTSS_API_EXIT();
        if (rc == VTSS_RC_OK) {
            inst_created_during_boot = TRUE;
        }
    }
    return rc;
}

BOOL is_1588gen2(mesa_port_no_t port_no)
{
    mesa_phy_type_t             id1g;
    mesa_phy_10g_id_t       phy_id_10g;

    if (mesa_capability(nullptr, MESA_CAP_PHY_10G)) {
        (void)mesa_phy_10g_id_get(phy.current_inst, port_no, &phy_id_10g);
        if ((phy_id_10g.part_number == MESA_PHY_TYPE_8490) || 
            (phy_id_10g.part_number == MESA_PHY_TYPE_8491)) {
            return 1;
        }
        if ((phy_id_10g.part_number == MESA_PHY_TYPE_8489) &&
            (phy_id_10g.device_feature_status & MESA_PHY_10G_TIMESTAMP_DISABLED) == 0) {
            return 1;
        }
    }
    (void)mesa_phy_id_get(phy.current_inst, port_no, &id1g);
    if ((id1g.part_number == 8582) ||
        (id1g.part_number == 8584) ||
        (id1g.part_number == 8575) ||
        (id1g.part_number == 8586)) {
        return 1;
    }
    return 0;
}

VTSS_ENUM_INC(mesa_phy_ts_engine_t);
typedef struct {
    BOOL data[2][4];
} phy_ts_eng_used_info_t;

static CapArray<phy_ts_eng_used_info_t, MESA_CAP_PORT_CNT> phy_ts_eng_used_info;
static mesa_rc phy_ts_conf_get(const mesa_inst_t inst, const mesa_port_no_t port_no)
{
    inst_store_ts_t             *port_ts_conf = &phy.store_ts[port_no];
    mesa_phy_ts_init_conf_t     ts_init_conf;
    mesa_phy_ts_eng_init_conf_t ts_eng_init_conf;
    mesa_phy_ts_engine_t        eng_id;

    memset(port_ts_conf, 0, sizeof(inst_store_ts_t));
    phy_ts_eng_used_info.clear();
    (void)mesa_phy_ts_init_conf_get(inst, port_no, &port_ts_conf->port_ts_init_done, &ts_init_conf);
    if (port_ts_conf->port_ts_init_done == FALSE) {
    /* TS block is not initialized */
        return VTSS_RC_OK;
    }
    port_ts_conf->clk_freq = ts_init_conf.clk_freq;
    port_ts_conf->clk_src = ts_init_conf.clk_src;
    port_ts_conf->rx_ts_pos = ts_init_conf.rx_ts_pos;
    port_ts_conf->rx_ts_len = ts_init_conf.rx_ts_len;
    port_ts_conf->tx_fifo_mode = ts_init_conf.tx_fifo_mode;
    port_ts_conf->tx_ts_len = ts_init_conf.tx_ts_len;
    port_ts_conf->one_step_txfifo = ts_init_conf.one_step_txfifo;
    if (mesa_capability(nullptr, MESA_CAP_PORT_10G)) {
        port_ts_conf->xaui_sel_8487 = ts_init_conf.xaui_sel_8487;
    }
    port_ts_conf->tc_op_mode = ts_init_conf.tc_op_mode;
    port_ts_conf->auto_clear_ls = ts_init_conf.auto_clear_ls;
    port_ts_conf->chk_ing_modified = ts_init_conf.chk_ing_modified;

    VTSS_RC(mesa_phy_ts_mode_get(inst, port_no, &port_ts_conf->port_ena));
    VTSS_RC(mesa_phy_ts_fifo_sig_get(inst, port_no, &port_ts_conf->sig_mask));
    VTSS_RC(mesa_phy_ts_ingress_latency_get(inst, port_no, &port_ts_conf->ingress_latency));
    VTSS_RC(mesa_phy_ts_egress_latency_get(inst, port_no, &port_ts_conf->egress_latency));
    VTSS_RC(mesa_phy_ts_path_delay_get(inst, port_no, &port_ts_conf->path_delay));
    VTSS_RC(mesa_phy_ts_clock_rateadj_get(inst, port_no, &port_ts_conf->rate_adj));

    if (is_1588gen2(port_no)) {
        VTSS_RC(mesa_phy_ts_pps_conf_get(inst, port_no, &port_ts_conf->phy_pps_conf));
        VTSS_RC(mesa_phy_ts_alt_clock_mode_get(inst, port_no, &port_ts_conf->phy_alt_clock_mode));
        VTSS_RC(mesa_phy_ts_sertod_get(inst, port_no, &port_ts_conf->sertod_conf));
        VTSS_RC(mesa_phy_ts_loadpulse_delay_get(inst, port_no, &port_ts_conf->delay));
    }

    eng_id = MESA_PHY_TS_PTP_ENGINE_ID_0;
    while (eng_id != MESA_PHY_TS_ENGINE_ID_INVALID) {
        (void)(mesa_phy_ts_ingress_engine_init_conf_get(inst, port_no, eng_id, &ts_eng_init_conf));
        port_ts_conf->ingress_eng_conf[eng_id].eng_used = ts_eng_init_conf.eng_used;
        if (port_ts_conf->ingress_eng_conf[eng_id].eng_used) {
            port_ts_conf->ingress_eng_conf[eng_id].encap_type = ts_eng_init_conf.encap_type;
            port_ts_conf->ingress_eng_conf[eng_id].flow_match_mode = ts_eng_init_conf.flow_match_mode;
            port_ts_conf->ingress_eng_conf[eng_id].flow_st_index = ts_eng_init_conf.flow_st_index;
            port_ts_conf->ingress_eng_conf[eng_id].flow_end_index = ts_eng_init_conf.flow_end_index;
            VTSS_RC(mesa_phy_ts_ingress_engine_conf_get(inst, port_no, eng_id, &port_ts_conf->ingress_eng_conf[eng_id].flow_conf));
            VTSS_RC(mesa_phy_ts_ingress_engine_action_get(inst, port_no, eng_id, &port_ts_conf->ingress_eng_conf[eng_id].action_conf));
        }
        eng_id++;
    }
    eng_id = MESA_PHY_TS_PTP_ENGINE_ID_0;
    while (eng_id != MESA_PHY_TS_ENGINE_ID_INVALID) {
        (void)(mesa_phy_ts_egress_engine_init_conf_get(inst, port_no, eng_id, &ts_eng_init_conf));
        port_ts_conf->egress_eng_conf[eng_id].eng_used = ts_eng_init_conf.eng_used;
        if (port_ts_conf->egress_eng_conf[eng_id].eng_used) {
            port_ts_conf->egress_eng_conf[eng_id].encap_type = ts_eng_init_conf.encap_type;
            port_ts_conf->egress_eng_conf[eng_id].flow_match_mode = ts_eng_init_conf.flow_match_mode;
            port_ts_conf->egress_eng_conf[eng_id].flow_st_index = ts_eng_init_conf.flow_st_index;
            port_ts_conf->egress_eng_conf[eng_id].flow_end_index = ts_eng_init_conf.flow_end_index;
            VTSS_RC(mesa_phy_ts_egress_engine_conf_get(inst, port_no, eng_id, &port_ts_conf->egress_eng_conf[eng_id].flow_conf));
            VTSS_RC(mesa_phy_ts_egress_engine_action_get(inst, port_no, eng_id, &port_ts_conf->egress_eng_conf[eng_id].action_conf));
        }
        eng_id++;
    }
    (void)(mesa_phy_ts_event_enable_get(inst, port_no, &port_ts_conf->event_mask));
    return VTSS_RC_OK;
}


static mesa_rc phy_ts_conf_set(const mesa_inst_t inst, const mesa_port_no_t port_no)
{
    inst_store_ts_t             *stored_ts_conf = &phy.store_ts[port_no];
    mesa_phy_ts_init_conf_t     ts_init_conf;
    mesa_phy_ts_engine_t        eng_id;
    mesa_port_no_t              base_port = 0;
    mesa_phy_type_t             id1g;

    VTSS_RC(mesa_phy_id_get(phy.current_inst, port_no, &id1g));
    VTSS_RC(vtss_phy_ts_base_port_get(inst, port_no, &base_port));
    memset(&ts_init_conf, 0, sizeof(mesa_phy_ts_init_conf_t));
    if (stored_ts_conf->port_ts_init_done == FALSE) {
    /* TS block was not initialized */
        return VTSS_RC_OK;
    }
    /*Initialize the TS block*/
    ts_init_conf.clk_freq = stored_ts_conf->clk_freq;
    ts_init_conf.clk_src = stored_ts_conf->clk_src;
    ts_init_conf.rx_ts_pos = stored_ts_conf->rx_ts_pos;
    ts_init_conf.rx_ts_len = stored_ts_conf->rx_ts_len;
    ts_init_conf.tx_fifo_mode = stored_ts_conf->tx_fifo_mode;
    ts_init_conf.tx_ts_len = stored_ts_conf->tx_ts_len;
    ts_init_conf.one_step_txfifo = stored_ts_conf->one_step_txfifo;
    if (mesa_capability(nullptr, MESA_CAP_PORT_10G)) {
        ts_init_conf.xaui_sel_8487 = stored_ts_conf->xaui_sel_8487;
    }
    ts_init_conf.tc_op_mode = stored_ts_conf->tc_op_mode;
    ts_init_conf.auto_clear_ls = stored_ts_conf->auto_clear_ls;
    ts_init_conf.chk_ing_modified = stored_ts_conf->chk_ing_modified;
#if 0 /* TODO: is not stored in state */
    ts_init_conf.remote_phy = stored_ts_conf->remote_phy;
#endif
    VTSS_RC(mesa_phy_ts_init(inst, port_no, &ts_init_conf));
    VTSS_RC(mesa_phy_ts_mode_set(inst, port_no, stored_ts_conf->port_ena));
    VTSS_RC(mesa_phy_ts_ingress_latency_set(inst, port_no, &stored_ts_conf->ingress_latency));
    VTSS_RC(mesa_phy_ts_egress_latency_set(inst, port_no, &stored_ts_conf->egress_latency));
    VTSS_RC(mesa_phy_ts_path_delay_set(inst, port_no, &stored_ts_conf->path_delay));
    VTSS_RC(mesa_phy_ts_delay_asymmetry_set(inst, port_no, &stored_ts_conf->delay_asym));

    if (is_1588gen2(port_no)) {
        VTSS_RC(mesa_phy_ts_alt_clock_mode_set(inst, port_no, &stored_ts_conf->phy_alt_clock_mode));
        VTSS_RC(mesa_phy_ts_sertod_set(inst, port_no, &stored_ts_conf->sertod_conf));
        VTSS_RC(mesa_phy_ts_pps_conf_set(inst, port_no, &stored_ts_conf->phy_pps_conf));
        VTSS_RC(mesa_phy_ts_loadpulse_delay_set(inst, port_no, &stored_ts_conf->delay));
    }

    /*Configuring the Ingress engines*/
    eng_id = MESA_PHY_TS_PTP_ENGINE_ID_0;
    while (eng_id != MESA_PHY_TS_ENGINE_ID_INVALID) {
        if (stored_ts_conf->ingress_eng_conf[eng_id].eng_used) {
            /*Ingress Engine Initialization*/
            if (phy_ts_eng_used_info[base_port].data[0][eng_id] == FALSE) {
                VTSS_RC(mesa_phy_ts_ingress_engine_init(inst,
                                port_no, eng_id,
                                stored_ts_conf->ingress_eng_conf[eng_id].encap_type,
                                stored_ts_conf->ingress_eng_conf[eng_id].flow_st_index,
                                stored_ts_conf->ingress_eng_conf[eng_id].flow_end_index,
                                stored_ts_conf->ingress_eng_conf[eng_id].flow_match_mode));
                phy_ts_eng_used_info[base_port].data[0][eng_id] = TRUE;
            }
            /*Programming the Comparators/flows of the engine*/
            VTSS_RC(mesa_phy_ts_ingress_engine_conf_set(inst, port_no, eng_id, &stored_ts_conf->ingress_eng_conf[eng_id].flow_conf));
            /*Programming the Engine Action*/
            VTSS_RC(mesa_phy_ts_ingress_engine_action_set(inst, port_no, eng_id, &stored_ts_conf->ingress_eng_conf[eng_id].action_conf));
        }
        eng_id++;
    }
    /*Configuring the egress engines*/
    eng_id = MESA_PHY_TS_PTP_ENGINE_ID_0;
    while (eng_id != MESA_PHY_TS_ENGINE_ID_INVALID) {
        if (stored_ts_conf->egress_eng_conf[eng_id].eng_used) {
            /*Ingress Engine Initialization*/
            if (phy_ts_eng_used_info[base_port].data[1][eng_id] == FALSE) {
                VTSS_RC(mesa_phy_ts_egress_engine_init(inst,
                                port_no, eng_id,
                                stored_ts_conf->egress_eng_conf[eng_id].encap_type,
                                stored_ts_conf->egress_eng_conf[eng_id].flow_st_index,
                                stored_ts_conf->egress_eng_conf[eng_id].flow_end_index,
                                stored_ts_conf->egress_eng_conf[eng_id].flow_match_mode));
                phy_ts_eng_used_info[base_port].data[1][eng_id] = TRUE;
            }
            /*Programming the Comparators/flows of the engine*/
            VTSS_RC(mesa_phy_ts_egress_engine_conf_set(inst, port_no, eng_id, &stored_ts_conf->egress_eng_conf[eng_id].flow_conf));
            /*Programming the Engine Action*/
            VTSS_RC(mesa_phy_ts_egress_engine_action_set(inst, port_no, eng_id, &stored_ts_conf->egress_eng_conf[eng_id].action_conf));
        }
        eng_id++;
    }
    VTSS_RC(mesa_phy_ts_fifo_sig_set(inst, port_no, stored_ts_conf->sig_mask));
    VTSS_RC(mesa_phy_ts_clock_rateadj_set(inst, port_no, &stored_ts_conf->rate_adj));
    VTSS_RC(mesa_phy_ts_event_enable_set(inst, port_no, TRUE, stored_ts_conf->event_mask));
    return VTSS_RC_OK;
}

// Function for determining if we should do MACSEC warmstart for a specific port
// IN - port_no - port in question
// Return - TRUE if warmstart can be done, else FALSE

























































































































































































































































































































static mesa_rc phy_inst_conf_10g_get(void) {
    mesa_port_no_t          port_no;
    u32                     port_count = port_count_max();
    u32                     i;
    u32                     gpio_pin_max;
    mesa_phy_10g_id_t       phy_id_10g;
    inst_store_10g          *p10g;
    vtss_clear(phy.store_10g);
    for (port_no = VTSS_PORT_NO_START; port_no < port_count; port_no++) {
        if (mesa_phy_10G_is_valid(phy.current_inst, port_no)) {
            (void)mesa_phy_10g_id_get(phy.current_inst, port_no, &phy_id_10g);
            T_I("Storing 10G phy, port:%u",port_no);
            p10g = &phy.store_10g[port_no];
            p10g->active = 1;
                /* Store standard 10G stuff */
            VTSS_RC(mesa_phy_10g_mode_get(phy.current_inst, port_no, &p10g->mode));
            VTSS_RC(mesa_phy_10g_loopback_get(phy.current_inst, port_no, &p10g->loopback));
            VTSS_RC(mesa_phy_10g_power_get(phy.current_inst, port_no, &p10g->power));
            VTSS_RC(mesa_phy_10g_rxckout_get(phy.current_inst, port_no, &p10g->rxckout));
            VTSS_RC(mesa_phy_10g_txckout_get(phy.current_inst, port_no, &p10g->txckout));
            VTSS_RC(mesa_phy_10g_srefclk_conf_get(phy.current_inst, port_no, &p10g->srefclk));
            if (phy_id_10g.part_number != 0x8486) {
                VTSS_RC(mesa_phy_10g_failover_get(phy.current_inst, port_no, &p10g->failover));
            }
            VTSS_RC(mesa_phy_10g_event_enable_get(phy.current_inst, port_no, &p10g->ev_mask));
            if (phy_id_10g.family == MESA_PHY_FAMILY_MALIBU) {
                gpio_pin_max = APPL_10G_PHY_GPIO_MAL_MAX;
            } else {
                gpio_pin_max = APPL_10G_PHY_GPIO_MAX;
            }
            if (phy_id_10g.family == MESA_PHY_FAMILY_MALIBU) {
                VTSS_RC(mesa_phy_10g_extended_event_enable_get(phy.current_inst, port_no, &p10g->ex_ev_mask));
                VTSS_RC(mesa_phy_10g_auto_failover_get(phy.current_inst, &p10g->failover_mode));
                VTSS_RC(mesa_phy_10g_clause_37_status_get(phy.current_inst, port_no, &p10g->clause_status));
                VTSS_RC(mesa_phy_10g_base_kr_host_conf_get(phy.current_inst, port_no, &p10g->kr_conf));
            }
            for (i=0; i<gpio_pin_max; ++i) {
                if (phy_id_10g.part_number != 0x8486)
                    VTSS_RC(mesa_phy_10g_gpio_mode_get(phy.current_inst, port_no, i, &p10g->gpio_mode[i]));
            }
            VTSS_RC(mesa_phy_10g_synce_clkout_get(phy.current_inst, port_no, &p10g->synce_clkout));
            VTSS_RC(mesa_phy_10g_xfp_clkout_get(phy.current_inst, port_no, &p10g->xfp_clkout));
            
            /* More 10g get functions here... */
            /* EWIS */
            //                VTSS_RC(mesa_ewis_static_conf_get(phy.current_inst, port_no, &p10g->ewis_mode.static_conf));
            VTSS_RC(mesa_ewis_force_conf_get(phy.current_inst, port_no, &p10g->ewis_mode.force_mode));
            VTSS_RC(mesa_ewis_tx_oh_get(phy.current_inst, port_no, &p10g->ewis_mode.tx_oh));
            VTSS_RC(mesa_ewis_tx_oh_passthru_get(phy.current_inst, port_no, &p10g->ewis_mode.tx_oh_passthru));
            VTSS_RC(mesa_ewis_mode_get(phy.current_inst, port_no, &p10g->ewis_mode.ewis_mode));
            VTSS_RC(mesa_ewis_cons_act_get(phy.current_inst, port_no, &p10g->ewis_mode.section_cons_act));
            VTSS_RC(mesa_ewis_section_txti_get(phy.current_inst, port_no, &p10g->ewis_mode.section_txti));
            VTSS_RC(mesa_ewis_path_txti_get(phy.current_inst, port_no, &p10g->ewis_mode.path_txti));
            VTSS_RC(mesa_ewis_test_mode_get(phy.current_inst, port_no, &p10g->ewis_mode.test_conf));
            if (mesa_capability(nullptr, MESA_CAP_TS)) {
                VTSS_RC(phy_ts_conf_get(phy.current_inst, port_no));
            }
        }
    }
    return VTSS_RC_OK;
}

// Function for getting vtss_state for 1G PHY for current state.
static mesa_rc phy_inst_conf_1g_get(void) {
    mesa_port_no_t          port_no;
    u32                     port_count = port_count_max();
    mesa_phy_type_t         id1g;
    inst_store_1g           *p1g;
    u32                     i;
    for (port_no = VTSS_PORT_NO_START; port_no < port_count; port_no++) {
        if ((mesa_phy_id_get(phy.current_inst, port_no, &id1g) == VTSS_RC_OK) && (id1g.part_number != 0)) {
            T_D_PORT(port_no, "Storing 1G Phy");
            p1g = &phy.store_1g[port_no];
            p1g->active = 1;        
            /* Store standard 1G stuff */    
            VTSS_RC(mesa_phy_reset_get(phy.current_inst, port_no, &p1g->reset));
            VTSS_RC(mesa_phy_conf_get(phy.current_inst, port_no, &p1g->conf));
            VTSS_RC(mesa_phy_conf_1g_get(phy.current_inst, port_no, &p1g->conf_1g));
            VTSS_RC(mesa_phy_power_conf_get(phy.current_inst, port_no, &p1g->power));
            VTSS_RC(mesa_phy_loopback_get(phy.current_inst, port_no, &p1g->loopback));
            
            VTSS_RC(mesa_phy_eee_conf_get(phy.current_inst, port_no, &p1g->eee_conf));
            VTSS_RC(mesa_phy_led_intensity_get(phy.current_inst, port_no, &p1g->led_intensity));
            VTSS_RC(mesa_phy_enhanced_led_control_init_get(phy.current_inst, port_no, &p1g->enhanced_led_control));

            if (mesa_capability(nullptr, MESA_CAP_TS)) {
                T_D_PORT(port_no, "Storing 1G Phy, phy_ts_conf_get");
                VTSS_RC(phy_ts_conf_get(phy.current_inst, port_no));
            }
            T_D_PORT(port_no, "Storing 1G Phy, mesa_phy_event_enable_get");
            VTSS_RC(mesa_phy_event_enable_get(phy.current_inst, port_no, &p1g->ev_mask));                
            VTSS_RC(mesa_phy_event_enable_get(phy.current_inst, port_no, &p1g->ev_mask));                
            
            T_D_PORT(port_no, "Storing 1G Phy, mesa_phy_clock_conf_get");
            for (i=0; i<MESA_PHY_RECOV_CLK_NUM; ++i)
                VTSS_RC(mesa_phy_clock_conf_get(phy.current_inst, port_no, i, &p1g->recov_clk[i].conf, &p1g->recov_clk[i].source));
            T_D_PORT(port_no, "1g Read done");
            /* More 1G get functions here... */
        }
    }
    return VTSS_RC_OK;
}

/* Store or Apply API CONF */
static mesa_rc phy_inst_conf(BOOL store)
{
    mesa_port_no_t     port_no;
    inst_store_1g      *p1g;
    u32                port_count = port_count_max();
    u32                i;
    inst_store_10g     *p10g;
    mesa_phy_10g_id_t  phy_id_10g;
    mesa_port_status_t dummy_status;

    T_D("Store:%d", store);
    VTSS_RC(mesa_phy_detect_base_ports(phy.current_inst));
    if (store) {
        /* Store the PHY API instance configuration locally  */
        /* ************************************************* */
        vtss_clear(phy.store_1g);
        if (mesa_capability(nullptr, MESA_CAP_TS)) {
            T_D("Storing 1G Phy, mesa_phy_ts_fifo_read_cb_get");
            VTSS_RC(mesa_phy_ts_fifo_read_cb_get(phy.current_inst, &phy.ts_fifo_cb, &phy.cb_cntxt));
        }
        T_D("Store:%d", store);
        VTSS_RC(phy_inst_conf_1g_get());
        if (mesa_capability(nullptr, MESA_CAP_PHY_10G)) {
            VTSS_RC(phy_inst_conf_10g_get());
        }



    } else {            
        /* Apply the stored PHY API instance configuration */
        /* *********************************************** */
        if (mesa_capability(nullptr, MESA_CAP_TS)) {
            VTSS_RC(mesa_phy_ts_fifo_read_install(phy.current_inst, phy.ts_fifo_cb, phy.cb_cntxt));
        }
        for (port_no = VTSS_PORT_NO_START; port_no < port_count; port_no++) {
            p1g = &phy.store_1g[port_no];
            if (p1g->active) {
                T_D("Applying 1g phy conf port:%u",port_no);
                /* Apply standard 1G stuff */    
                VTSS_RC(mesa_phy_reset(phy.current_inst, port_no, &p1g->reset));
                VTSS_RC(mesa_phy_conf_set(phy.current_inst, port_no, &p1g->conf));
                VTSS_RC(mesa_phy_conf_1g_set(phy.current_inst, port_no, &p1g->conf_1g));
                VTSS_RC(mesa_phy_power_conf_set(phy.current_inst, port_no, &p1g->power));
                VTSS_RC(mesa_phy_loopback_set(phy.current_inst, port_no, p1g->loopback));            
                VTSS_RC(mesa_phy_eee_conf_set(phy.current_inst, port_no, p1g->eee_conf));
                VTSS_RC(mesa_phy_led_intensity_set(phy.current_inst, port_no, p1g->led_intensity));
                VTSS_RC(mesa_phy_enhanced_led_control_init(phy.current_inst, port_no, p1g->enhanced_led_control));
                if (mesa_capability(nullptr, MESA_CAP_TS)) {
                    VTSS_RC(phy_ts_conf_set(phy.current_inst, port_no));
                }
                VTSS_RC(mesa_phy_event_enable_set(phy.current_inst, port_no, p1g->ev_mask, TRUE));
                for (i=0; i<MESA_PHY_RECOV_CLK_NUM; ++i)
                    if ((p1g->recov_clk[i].conf.src == MESA_PHY_CLK_DISABLED) || (p1g->recov_clk[i].source == port_no))
                        VTSS_RC(mesa_phy_clock_conf_set(phy.current_inst, port_no, i, &p1g->recov_clk[i].conf));

                VTSS_RC(mesa_phy_status_get(phy.current_inst, port_no, &dummy_status));
                /* More 1g set functions here... */
                T_D("1g write done");
            }
        }

        if (mesa_capability(nullptr, MESA_CAP_PHY_10G)) {
            /* Must initilize the 10G chip in reversed order because channel 0 must get registered first */
            /* Normally this is done by the Port module */
            for (port_no = port_count - 1; ; port_no--) {
                p10g = &phy.store_10g[port_no];
                if (p10g->active) {
                    T_I_PORT(port_no, "mesa_phy_10g_mode_set 10g p:%d",port_no);
                    VTSS_RC(mesa_phy_10g_mode_set(phy.current_inst, port_no, &p10g->mode));
                }

                if (port_no == VTSS_PORT_NO_START) { break; }
            }

            for (port_no = VTSS_PORT_NO_START; port_no < port_count; port_no++) {
                p10g = &phy.store_10g[port_no];
                (&p10g->failover_mode)->port_no = port_no;
                if (p10g->active) {
                    u32 gpio_pin_max;

                    T_D("Applying 10g phy conf port:%u",port_no);
                    /* Apply standard 10G stuff */
                    VTSS_RC(mesa_phy_10g_reset(phy.current_inst, port_no));

                    VTSS_RC(mesa_phy_10g_id_get(phy.current_inst, port_no, &phy_id_10g));

                    T_D("mesa_phy_10g_loopback_set, port:%u",port_no);
                    VTSS_RC(mesa_phy_10g_loopback_set(phy.current_inst, port_no, &p10g->loopback));
                    T_D("mesa_phy_10g_power_set, port:%u",port_no);
                    VTSS_RC(mesa_phy_10g_power_set(phy.current_inst, port_no, &p10g->power));

                    if (phy_id_10g.part_number != 0x8486) {
                        VTSS_RC(mesa_phy_10g_failover_set(phy.current_inst, port_no, &p10g->failover)); 
                    }
                    if (phy_id_10g.family == MESA_PHY_FAMILY_MALIBU) {
                        T_D("mesa_phy_10g_auto_failover_set, port:%u",port_no);
                        VTSS_RC(mesa_phy_10g_auto_failover_set(phy.current_inst, &p10g->failover_mode));
                        T_D("mesa_phy_10g_extended_event_enable_set, port:%u",port_no);
                        VTSS_RC(mesa_phy_10g_extended_event_enable_set(phy.current_inst, port_no, p10g->ex_ev_mask, TRUE));
                    }
                    T_D("mesa_phy_10g_event_enable_set, port:%u",port_no);
                    VTSS_RC(mesa_phy_10g_event_enable_set(phy.current_inst, port_no, p10g->ev_mask, TRUE));

                    if (phy_id_10g.family == MESA_PHY_FAMILY_MALIBU) {
                        gpio_pin_max = APPL_10G_PHY_GPIO_MAL_MAX;
                    } else {
                        gpio_pin_max = APPL_10G_PHY_GPIO_MAX;
                    }

                    for (i=0; i<gpio_pin_max; ++i) {
                        if (phy_id_10g.part_number != 0x8486)
                            VTSS_RC(mesa_phy_10g_gpio_mode_set(phy.current_inst, port_no, i, &p10g->gpio_mode[i]));
                    }

                    T_D("mesa_phy_10g_synce_clkout_set, port:%u",port_no);
                    VTSS_RC(mesa_phy_10g_rxckout_set(phy.current_inst, port_no, &p10g->rxckout));
                    VTSS_RC(mesa_phy_10g_txckout_set(phy.current_inst, port_no, &p10g->txckout));
                    VTSS_RC(mesa_phy_10g_srefclk_conf_set(phy.current_inst, port_no, &p10g->srefclk));
                    T_D("mesa_phy_10g_base_kr_conf_set, port:%u",port_no);
                    VTSS_RC(mesa_phy_10g_base_kr_conf_set(phy.current_inst, port_no, &p10g->kr_conf));
                    T_D("mesa_phy_10g_clause_37_control_set, port:%u",port_no);
                    VTSS_RC(mesa_phy_10g_clause_37_control_set(phy.current_inst, port_no, &p10g->clause_37));

                    /* More 10g set functions here... */
                    if (mesa_capability(nullptr, MESA_CAP_PHY_10G)) {
                        T_D("mesa_ewis_force_conf_set, port:%u",port_no);
                        VTSS_RC(mesa_ewis_force_conf_set(phy.current_inst, port_no, &p10g->ewis_mode.force_mode));
                        VTSS_RC(mesa_ewis_tx_oh_set(phy.current_inst, port_no, &p10g->ewis_mode.tx_oh));
                        VTSS_RC(mesa_ewis_mode_set(phy.current_inst, port_no, &p10g->ewis_mode.ewis_mode));
                        VTSS_RC(mesa_ewis_cons_act_set(phy.current_inst, port_no, &p10g->ewis_mode.section_cons_act));
                        VTSS_RC(mesa_ewis_section_txti_set(phy.current_inst, port_no, &p10g->ewis_mode.section_txti));
                        VTSS_RC(mesa_ewis_path_txti_set(phy.current_inst, port_no, &p10g->ewis_mode.path_txti));
                        VTSS_RC(mesa_ewis_test_mode_set(phy.current_inst, port_no, &p10g->ewis_mode.test_conf));
                    }

                    if (mesa_capability(nullptr, MESA_CAP_TS)) {
                        T_D("phy_ts_conf_set, port:%u",port_no);
                        VTSS_RC(phy_ts_conf_set(phy.current_inst, port_no));
                    }
                }
            }
        }




    }
    return VTSS_RC_OK;
}

static void phy_channel_failover(void)
{
    mesa_phy_10g_failover_mode_t current;
    
    if (phy.failover_port == VTSS_PORT_NO_NONE)
        return;
    
    if (mesa_phy_10g_failover_get(phy.current_inst, phy.failover_port, &current) != VTSS_RC_OK) {
        T_E("Could not complete mesa_phy_10g_failover_get() operation port:%u",phy.failover_port);
    }
    if (current == phy.failover)
        return;

    if (mesa_phy_10g_failover_set(phy.current_inst, phy.failover_port, &phy.failover) != VTSS_RC_OK) {
        T_E("Could not complete mesa_phy_10g_failover_set() operation port:%u",phy.failover_port);
    }
    T_I("Completed failover to port:%u (channel: %d)",phy.failover_port+1,phy.failover==MESA_PHY_10G_PMA_TO_FROM_XAUI_NORMAL?0:1);
}


/****************************************************************************/
/*  Management functions                                                                                                           */
/****************************************************************************/

// Debug function for testing phy warm start. This function does the phy initialization which is normally done in the port_init_ports function.
void port_warm_start_simulate_phy_init(void) {
    mesa_port_no_t        port_no;
    u32                   port_count = port_count_max();
    inst_store_1g         *p1g;
    T_D("enter");
    mesa_phy_reset_conf_t phy_reset;

    memset(&phy_reset,0 ,sizeof(mesa_phy_reset_conf_t));
    for (port_no = VTSS_PORT_NO_START; port_no < port_count; port_no++) {
        p1g = &phy.store_1g[port_no];
        if (p1g->active) {
            (void) mesa_phy_reset(phy.current_inst, port_no, &phy_reset);
        } 
        T_N("Port:%d", port_no);
    } /* Port loop */
    
    (void) mesa_phy_detect_base_ports(phy.current_inst);
}

mesa_inst_t phy_mgmt_inst_get(void)
{
    mesa_inst_t inst = nullptr;
    
    if (phy_warm_start_test_enabled()) {
        PHY_CRIT_ENTER();
        inst = phy.current_inst;
        PHY_CRIT_EXIT();
    }
    return inst;
}

phy_inst_start_t phy_mgmt_start_inst_get(void)
{
    phy_inst_start_t inst = PHY_INST_NONE;

    if (phy_warm_start_test_enabled()) {
        PHY_CRIT_ENTER();
        inst = phy.start_inst;
        PHY_CRIT_EXIT();
    }
    return inst;
}

mesa_rc phy_mgmt_inst_restart(mesa_inst_t inst, phy_inst_restart_t restart)
{
    mesa_rc rc=VTSS_RC_OK;

    if (phy.start_inst == PHY_INST_NONE) {
        T_W("Current start instance is 'PHY_INST_NONE'. Please create PHY inst (if PHY instance is already present then don't do 'reload default' doing so restores the start instance to default i.e. 'PHY_INST_NONE') followed by 'reload cold' and then do warm restart");
        return VTSS_RC_ERROR; /* The Phys are included in the Default API instance */
    }

    PHY_CRIT_ENTER();
    /* Store the current API Conf */
    if ((rc = phy_inst_conf(1)) != VTSS_RC_OK) {
        T_E("Problem in storing API, rc:%d", rc);
        PHY_CRIT_EXIT();
        return rc;
    }

    /* Set restart mode.  WARM/COOL and the API version is written to the restart PHY. 
       This info is later fetched and used when the API is initilized through mesa_init_conf_set() */
    T_I("Set the restart mode to %s",restart==WARM ? "WARM":"COOL");
    if ((rc = mesa_restart_conf_set(phy.current_inst, restart==WARM ? MESA_RESTART_WARM : MESA_RESTART_COOL) != VTSS_RC_OK)) {
        T_E("Could not complete mesa_restart_conf_set()");
        PHY_CRIT_EXIT();
        return rc;
    }

    /* Lock the VTSS API */
    VTSS_API_ENTER();

    /* Destroy current instance */
    T_I("Destroy the current Phy instance");
    if ((rc = mesa_inst_destroy(phy.current_inst)) != VTSS_RC_OK) {
        T_E("Could not complete mesa_inst_destroy()");
        VTSS_API_EXIT();
        PHY_CRIT_EXIT();
        return rc;
    }

    /* Create a new instance */
    T_I("Create an new Phy instance");
    if ((rc = phy_inst_create()) != VTSS_RC_OK) {
        T_E("Could not create PHY instance");
        VTSS_API_EXIT();
        PHY_CRIT_EXIT();
        return rc;

    }
    T_I("UnLock the VTSS API");
    /* UnLock the VTSS API */
    VTSS_API_EXIT();

    
    
    // Call the port module for simulating a PHY init sequence. 
    T_I("Call the port module for simulating a PHY init sequence. ");
    port_warm_start_simulate_phy_init();


    T_I("Apply the stored configuration to the new instance");
    if ((rc = phy_inst_conf(0)) != VTSS_RC_OK) {
        T_E("Problem in applying stored API conf, rc:%d", rc);
        PHY_CRIT_EXIT();
        return rc;
    }
    PHY_CRIT_EXIT();

    /* Synchronize API and chip */
    T_I("Synchronize API and Phys");
    if ((rc = mesa_restart_conf_end(phy.current_inst)) != VTSS_RC_OK) {
        T_E("Could not complete mesa_restart_conf_end()");
    }

    /* The API Phy instance restart is now completed. */

    if (mesa_capability(nullptr, MESA_CAP_PHY_10G)) {
        /* Perform a failover if requested */
        (void)phy_channel_failover();
    }

    return rc;
}

mesa_rc phy_mgmt_inst_activate_default(void)
{
    if (!mesa_capability(nullptr, MESA_CAP_PHY_10G)) {
        return VTSS_RC_ERROR;
    }

    mesa_phy_10g_mode_t      mode;
    mesa_port_no_t           port_no;
    u32                      port_count = port_count_max();

    for (port_no = port_count-1;;port_no--) {
        if (mesa_phy_10G_is_valid(phy.current_inst, port_no)) {
            VTSS_RC(mesa_phy_10g_mode_get(phy.current_inst, port_no, &mode));
            VTSS_RC(mesa_phy_10g_mode_set(NULL, port_no, &mode));                
        }
        if (port_no == VTSS_PORT_NO_START) { break; }
    }
    return VTSS_RC_OK;
}

mesa_rc phy_mgmt_failover_set(mesa_port_no_t port_no, mesa_phy_10g_failover_mode_t *failover)
{
    PHY_CRIT_ENTER();
    phy.failover = *failover;    
    phy.failover_port = port_no;    
    PHY_CRIT_EXIT();
    return VTSS_RC_OK;
}


mesa_rc phy_mgmt_failover_get(mesa_port_no_t port_no, mesa_phy_10g_failover_mode_t *failover)
{
    PHY_CRIT_ENTER();
    *failover = phy.failover;
    PHY_CRIT_EXIT();
    return VTSS_RC_OK;
}

static mesa_rc vtss_phy_ts_base_port_get(const mesa_inst_t inst,
                           const mesa_port_no_t port_no,
                           mesa_port_no_t     *const base_port_no)
{
    mesa_phy_10g_id_t phy_id_10g;
    mesa_phy_type_t   phy_id_1g;

    memset(&phy_id_1g, 0, sizeof(mesa_phy_type_t));
    memset(&phy_id_10g, 0, sizeof(mesa_phy_10g_id_t));
    if (mesa_capability(nullptr, MESA_CAP_PORT_10G) && mesa_phy_10g_id_get(inst, port_no, &phy_id_10g) == VTSS_RC_OK) {
        /* Get base port_no for 10G */
        if(phy_id_10g.channel_id <= 1)
            *base_port_no = phy_id_10g.phy_api_base_no;
        else
            *base_port_no = phy_id_10g.phy_api_base_no - 2;
    } else if (mesa_phy_id_get(inst, port_no, &phy_id_1g) == VTSS_RC_OK) {
        *base_port_no = phy_id_1g.phy_api_base_no;
    } else {
        return VTSS_RC_ERROR;
    }
    return VTSS_RC_OK;
}

mesa_rc vtss_appl_phy_inst_destroy(void)
{
    T_I("Destroying PHY instance\r\n");
    return phy_mgmt_inst_create(PHY_INST_NONE);
}

mesa_rc vtss_appl_phy_inst_create(mesa_port_no_t port)
{
    T_I("Save phy inst conf to flash\r\n");
    mesa_port_no_t base_port;

    VTSS_RC(vtss_phy_ts_base_port_get(phy.current_inst, port, &base_port));

    if (port != base_port)
        T_W("%% Note: Start port must be equal to 'API base port', "
            "so changing configured port:%u to 'API base port':%u\n", iport2uport(port), iport2uport(base_port));

    PHY_CRIT_ENTER();
    phy.start_port = base_port;
    PHY_CRIT_EXIT();

    return phy_mgmt_inst_create(PHY_INST_PORT);
}

mesa_rc phy_mgmt_inst_create(phy_inst_start_t inst_create)
{
    T_D("Enter phy_mgmt_inst_create\n");
    phy_conf_t *phy_blk;
    meba_port_cap_t cap;

    PHY_CRIT_ENTER();
    phy.mode = (inst_create == PHY_INST_PORT) ? 0 : 1; /* if mode==0 then is manually set from icli */
    phy.start_inst = inst_create;
    PHY_CRIT_EXIT();

    /* if mode = 0, then value of phy.start_inst will be set to cap. of phy.start_port */
    if (!phy.mode) {
        if (port_cap_get(phy.start_port, &cap) != VTSS_OK) {
            T_E("Could not get port CAP info iport:%u", phy.start_port);
        }
        if (cap & MEBA_PORT_CAP_1G_PHY) {
            PHY_CRIT_ENTER();
            phy.start_inst = PHY_INST_1G_PHY;
            PHY_CRIT_EXIT();
        } else if (cap & MEBA_PORT_CAP_VTSS_10G_PHY) {
            PHY_CRIT_ENTER();
            phy.start_inst = PHY_INST_10G_PHY;
            PHY_CRIT_EXIT();
        } else {
            T_E("Configure iport: %u, is neither 1G nor 10G\n", phy.start_port);
        }
    }

    /* Save to flash */
    if ((phy_blk = (phy_conf_t *)conf_sec_open(CONF_SEC_GLOBAL, CONF_BLK_PHY_CONF, NULL)) == NULL) {
        T_E("Failed to open phy conf");
    } else {    
        PHY_CRIT_ENTER();
        phy_blk->inst  = phy.start_inst;
        phy_blk->iport = phy.start_port;
        phy_blk->mode  = phy.mode;
        phy_blk->version = PHY_CONF_VERSION; 
        PHY_CRIT_EXIT();
        conf_sec_close(CONF_SEC_GLOBAL, CONF_BLK_PHY_CONF);
    }

    return VTSS_RC_OK;
}

/* Read/create and activate configuration */
static void phy_conf_read(vtss_isid_t isid_add, BOOL force_default)
{
    phy_conf_t             *phy_blk;
    ulong                  size;
    BOOL                   create; 

    T_D("enter, isid_add: %d", isid_add);
    /* Restore. Open or create configuration block */
    if ((phy_blk = (phy_conf_t *)conf_sec_open(CONF_SEC_GLOBAL, CONF_BLK_PHY_CONF, &size)) == NULL) {
        create = 1;
        T_D("Could not open conf block. Defaulting.");
    } else {
        /* If the configuration size have changed then create a block defaults */
        if (size != sizeof(*phy_blk)) {
            create = 1;
            T_D("Configuration size have changed, creating defaults");
        } else if (phy_blk->version != PHY_CONF_VERSION) {
            T_D("Version mismatch, creating defaults");
            create = 1;
        } else {
            create = (isid_add != VTSS_ISID_GLOBAL);
        }
    }
    /* Defaulting */
    if (create || force_default) {
        T_D("Defaulting configuration.");
        phy_blk = (phy_conf_t *)conf_sec_create(CONF_SEC_GLOBAL, CONF_BLK_PHY_CONF, sizeof(phy_conf_t));

        /* Write to the global structure */
        phy.start_inst = PHY_INST_NONE;
        phy.start_port = VTSS_PORT_NO_START;
        phy.mode       = 1;/* automatically find the first base port for start instance */

        /* Write to flash */
        if (phy_blk != NULL) {
            phy_blk->inst = phy.start_inst;
            phy_blk->iport  = phy.start_port;
            phy_blk->mode   = phy.mode;
            phy_blk->version = PHY_CONF_VERSION;
        } 
    } else {
        /* Use stored configuration */
        T_D("Using stored config");

        /* Move entries from flash to local structure */
        if (phy_blk != NULL) {
            phy.start_inst = phy_blk->inst;
            phy.start_port = phy_blk->iport;
            phy.mode       = phy_blk->mode;
        }
    }

    if (phy_blk == NULL) {
        T_E("Failed to open phy config table");
    } else {
        conf_sec_close(CONF_SEC_GLOBAL, CONF_BLK_PHY_CONF);
    }

    T_D("exit");
}

#ifdef VTSS_SW_OPTION_JSON_RPC
VTSS_PRE_DECLS void vtss_appl_phy_json_init(void);
#endif

extern "C" int phy_icli_cmd_register();

bool phy_warm_start_test_enabled()
{
    bool retval = false;
    // The PHY warm start test depends on the MACSC module for now




    T_N("phy_warm_start_test_enabled: %u", retval);
    return retval;
}

mesa_rc phy_init(vtss_init_data_t *data)
{
    mesa_rc rc;

    if (data->cmd == INIT_CMD_EARLY_INIT) {
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);
    }

    T_D("enter, cmd: %d, isid: %u, flags: 0x%x", data->cmd, data->isid, data->flags);

    switch (data->cmd) {
    case INIT_CMD_INIT:
        if (phy_warm_start_test_enabled()) {
            phy.current_inst = NULL;
            phy.failover_port = VTSS_PORT_NO_NONE;
            /* Create API semaphore (initially locked) */
            critd_init(&phy.crit, "phy_crit", VTSS_TRACE_MODULE_ID, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
            /* Initialize API */
            phy_conf_read(VTSS_ISID_GLOBAL,0);
        }
#if defined(VTSS_SW_OPTION_ICFG)
        rc = phy_icfg_init();
        if (rc != VTSS_OK) {
            T_E("%% Error fail to init phy icfg registration, rc = %s", error_txt(rc));
        }
#endif
        if (phy_warm_start_test_enabled()) {
            VTSS_API_ENTER();
            rc = phy_inst_create();
            VTSS_API_EXIT();
            PHY_CRIT_EXIT();
            if (rc == VTSS_RC_OK) {
                inst_created_during_boot = TRUE;
            }
        }
#if defined(VTSS_SW_OPTION_JSON_RPC)
        if (mesa_capability(nullptr, MESA_CAP_PHY_10G)) {
            vtss_appl_phy_json_init();
        }
#endif
        phy_icli_cmd_register();
        break;
    case INIT_CMD_START:
        break;
    case INIT_CMD_CONF_DEF:
        if (phy_warm_start_test_enabled()) {
            phy_conf_read(VTSS_ISID_GLOBAL,1);
        }
        break;
    case INIT_CMD_MASTER_UP:
        break;
    case INIT_CMD_MASTER_DOWN:
        break;
    case INIT_CMD_SWITCH_ADD:
        break;
    case INIT_CMD_SWITCH_DEL:
        break;
    default:
        break;
    }

    T_D("exit");
    return 0;
}

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
