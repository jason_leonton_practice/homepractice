/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

#ifndef _PHY_API_H_
#define _PHY_API_H_

#include "main_types.h"
#include "main.h"
#include "mscc/ethernet/switch/api.h"
#include "main_types.h"

#undef VTSS_CHIP_10G_PHY_SAVE_FAILOVER_IN_CFG /* define it to enable saving of failover cfg in icfg */
#define VTSS_SW_ICLI_PHY_INVISIBLE /* undef 'VTSS_SW_ICLI_PHY_INVISIBLE' to make PHY's module icli cmds visible */
/* To control visibility of PHY config & interface mode icli cmds */
#ifdef VTSS_SW_ICLI_PHY_INVISIBLE
#define ICLI_PHY_NONE_SHOW_CMDS_PROPERTY ICLI_CMD_PROP_INVISIBLE
#else
#define ICLI_PHY_NONE_SHOW_CMDS_PROPERTY ICLI_CMD_PROP_VISIBLE
#endif

/* if VTSS_SW_OPTION_PHY does not exist then PHY_INST=NULL */
#define PHY_INST phy_mgmt_inst_get()

/* Initialize module */
mesa_rc phy_init(vtss_init_data_t *data);

/* Get the instance  */
mesa_inst_t phy_mgmt_inst_get(void);

typedef enum {
    PHY_INST_NONE,
    PHY_INST_1G_PHY,
    PHY_INST_10G_PHY,
    PHY_INST_PORT,
} phy_inst_start_t;

/* get start instance */
phy_inst_start_t phy_mgmt_start_inst_get(void);

/* Create a PHY instance, takes port number as input */
mesa_rc vtss_appl_phy_inst_create(mesa_port_no_t port);

/* Destroy PHY instance */
mesa_rc vtss_appl_phy_inst_destroy(void);

/* Create a Phy instance */
mesa_rc phy_mgmt_inst_create(phy_inst_start_t inst_create);

typedef enum {
    COOL,
    WARM,
} phy_inst_restart_t;

/* Restart the instance */
mesa_rc phy_mgmt_inst_restart(mesa_inst_t inst, phy_inst_restart_t restart);
/* Activate the Default instance, i.e. make it possible to configure the PHYs through it */
mesa_rc phy_mgmt_inst_activate_default(void);

/* Set the failover mode used after next instance restart */
mesa_rc phy_mgmt_failover_set(mesa_port_no_t port_no, mesa_phy_10g_failover_mode_t *failover);
/* Get the current failover mode */
mesa_rc phy_mgmt_failover_get(mesa_port_no_t port_no, mesa_phy_10g_failover_mode_t *failover);
mesa_rc phy_inst_create_after_malibu_detect(void);
/* Find out if PHY warm start test is enabled */
bool phy_warm_start_test_enabled();
#endif /* _PHY_API_H_ */

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
