/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

#ifndef _PHY_H_
#define _PHY_H_

/* ================================================================= *
 * Trace definitions
 * ================================================================= */
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_PHY
#define VTSS_TRACE_GRP_DEFAULT 0
#define TRACE_GRP_CRIT         1
#define TRACE_GRP_ICLI         2
#define TRACE_GRP_CNT          3

#define APPL_10G_PHY_GPIO_MAL_MAX   40
#define APPL_10G_PHY_GPIO_MAX   12

#include <vtss_trace_api.h>
#include "critd_api.h"
#include <vtss/appl/macsec.hxx>


#include "mscc/ethernet/switch/api.h"

#if (VTSS_TRACE_ENABLED)
static vtss_trace_reg_t trace_reg =
{
    VTSS_TRACE_MODULE_ID,
    "phy",
    "PHY"
};

static vtss_trace_grp_t trace_grps[TRACE_GRP_CNT] =
{
    /* VTSS_TRACE_GRP_DEFAULT */ { 
        "default",
        "Default",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP,
    },
    /* TRACE_GRP_CRIT */ { 
        "crit",
        "Critical regions ",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP,
    }
};
//static critd_t   phy_crit; 
#define PHY_CRIT_ENTER() critd_enter(&phy.crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define PHY_CRIT_EXIT()  critd_exit( &phy.crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#endif /* VTSS_TRACE_ENABLED */

/* ================================================================= *
 *  Aggr local definitions
 * ================================================================= */
#define PHY_CONF_VERSION     1

/* Flash/Mem structs */
typedef struct {      
    phy_inst_start_t inst;
    mesa_port_no_t   iport;
    BOOL             mode;
    ulong            version; /* Block version     */
} phy_conf_t;


typedef struct {      
    BOOL                          active;
    mesa_phy_10g_mode_t           mode;
    BOOL                          synce_clkout;
    BOOL                          xfp_clkout;
    mesa_phy_10g_rxckout_conf_t   rxckout;
    mesa_phy_10g_txckout_conf_t   txckout;
    mesa_phy_10g_srefclk_mode_t   srefclk;
    mesa_phy_10g_loopback_t       loopback;
    mesa_phy_10g_power_t          power;
    mesa_phy_10g_failover_mode_t  failover;
    mesa_phy_10g_auto_failover_conf_t failover_mode;
    mesa_phy_10g_event_t          ev_mask;
    mesa_phy_10g_extnd_event_t    ex_ev_mask;
    mesa_gpio_10g_gpio_mode_t     gpio_mode[APPL_10G_PHY_GPIO_MAL_MAX];
    mesa_phy_10g_clause_37_control_t clause_37;
    mesa_phy_10g_clause_37_cmn_status_t clause_status;
    mesa_phy_10g_base_kr_conf_t    kr_conf;
    mesa_ewis_conf_t              ewis_mode;
} inst_store_10g;


typedef struct {
    mesa_phy_clock_conf_t  conf;
    mesa_port_no_t         source;
} clock_conf_t;

typedef struct {      
    BOOL                     active;
    mesa_phy_conf_t          conf;
    mesa_phy_reset_conf_t    reset;
    mesa_phy_conf_1g_t       conf_1g;
    mesa_phy_power_conf_t    power;
    clock_conf_t             recov_clk[MESA_PHY_RECOV_CLK_NUM];
    mesa_phy_clock_conf_t    clk_conf;
    mesa_phy_loopback_t      loopback;
    mesa_phy_eee_conf_t      eee_conf;
    mesa_phy_led_intensity   led_intensity;
    mesa_phy_enhanced_led_control_t enhanced_led_control;
    mesa_phy_event_t         ev_mask;
} inst_store_1g;


typedef struct {
    BOOL                            eng_used; /* allocated the engine to application */
    mesa_phy_ts_encap_t             encap_type; /* engine encapsulation */
    mesa_phy_ts_engine_flow_match_t flow_match_mode; /* strict/non-strict flow match */
    u8                              flow_st_index; /* start index of flow */
    u8                              flow_end_index; /* end index of flow */
    mesa_phy_ts_engine_flow_conf_t  flow_conf; /* engine flow config */
    mesa_phy_ts_engine_action_t     action_conf; /* engine action */
    u8                              action_flow_map[6]; /* action map to flow */
} vtss_phy_ts_eng_conf_t;

typedef struct {
    BOOL                            eng_used; /* allocated the engine to application */
    mesa_phy_ts_encap_t             encap_type; /* engine encapsulation */
    mesa_phy_ts_engine_flow_match_t flow_match_mode; /* strict/non-strict flow match */
    u8                              flow_st_index; /* start index of flow */
    u8                              flow_end_index; /* end index of flow */
    mesa_phy_ts_engine_flow_conf_t  flow_conf; /* engine flow config */
    mesa_phy_ts_engine_action_t     action_conf; /* engine action */
} vtss_phy_ts_eng_conf_stored_t;

typedef struct {
    BOOL                             port_ts_init_done; /* PHY TS init done */
    BOOL                             port_ena;
    mesa_phy_ts_clockfreq_t          clk_freq;  /* reference clock frequency */
    mesa_phy_ts_clock_src_t          clk_src;   /* reference clock source */
    mesa_phy_ts_rxtimestamp_pos_t    rx_ts_pos; /* Rx timestamp position */
    mesa_phy_ts_rxtimestamp_len_t    rx_ts_len; /* Rx timestamp length */
    mesa_phy_ts_fifo_mode_t          tx_fifo_mode; /* Tx TSFIFO access mode */
    mesa_phy_ts_fifo_timestamp_len_t tx_ts_len; /* timestamp size in Tx TSFIFO */
    mesa_phy_ts_8487_xaui_sel_t      xaui_sel_8487; /* 8487 XAUI Lane selection */
    mesa_phy_ts_tc_op_mode_t         tc_op_mode; /* TC operating mode */
    BOOL                             one_step_txfifo; /* used when transmitting Delay_Req in one step mode.FALSE means correctionfield update is used instead */
    BOOL                             auto_clear_ls; /* Load and Save of LTC are auto cleared */
    BOOL                             chk_ing_modified;/* True if the flag bit needs to be modified in ingress and thus in egress */
    mesa_phy_ts_fifo_sig_mask_t      sig_mask;  /* FIFO signature */
    u32                              fifo_age;  /* SW TSFIFO age in milli-sec */
    mesa_timeinterval_t              ingress_latency;
    mesa_timeinterval_t              egress_latency;
    mesa_timeinterval_t              path_delay;
    mesa_timeinterval_t              delay_asym;
    mesa_phy_ts_scaled_ppb_t         rate_adj;  /* clock rate adjustment */
    mesa_phy_ts_pps_conf_t           phy_pps_conf;
    mesa_phy_ts_alt_clock_mode_t     phy_alt_clock_mode;
    mesa_phy_ts_sertod_conf_t        sertod_conf;
    u16                              delay;
    i32                              offset;
    vtss_phy_ts_eng_conf_stored_t    ingress_eng_conf[4]; /*port ingress engine configuration including encapsulation, comparator configuration and action  */
    vtss_phy_ts_eng_conf_stored_t    egress_eng_conf[4]; /*port egress engine configuration including encapsulation, comparator configuration and action  */
    mesa_phy_ts_event_t              event_mask; /* interrupt mask config */
} inst_store_ts_t;
































































typedef struct {  
    phy_inst_start_t             start_inst;
    mesa_port_no_t               start_port;
    BOOL                         mode; /* if mode = 1(auto) then start_port is the first (base) port for that type of PHY instance
                                          if mode = 0(manual) then start_port is configured from management */
    mesa_inst_t                  current_inst;       
    mesa_init_conf_t             init_conf_default;
    phy_conf_t                   conf;
    critd_t                      crit; 
    CapArray<inst_store_1g, MESA_CAP_PORT_CNT> store_1g;
    CapArray<inst_store_10g, MESA_CAP_PORT_CNT> store_10g;



    mesa_port_no_t               failover_port;
    mesa_phy_10g_failover_mode_t failover;
    
    mesa_phy_ts_fifo_read        ts_fifo_cb;
    void                         *cb_cntxt;
    CapArray<inst_store_ts_t, MESA_CAP_PORT_CNT> store_ts;
} phy_global_t;
#endif /* _PHY_H_ */
