/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 

*/
#include "port_trace.h"
#include "main.h"
#include "icli_api.h"
#include "icli_porting_util.h"
#include "phy_icli_util.h"
#include "port_api.h"
#include "phy_api.h"

static void phy_icli_port_iter_init(port_iter_t *pit)
{
    (void)icli_port_iter_init(pit, VTSS_ISID_START, PORT_ITER_FLAGS_NORMAL);
}

static void phy_icli_phy_reg(phy_icli_req_t *req, port_iter_t *pit, u8 addr)
{
    u32    session_id = req->session_id;
    u32    addr_page = ((req->page << 5) + addr);
    ushort value;
    int    i;

    /* Ignore non-PHY ports */
    if (!is_port_phy(pit->iport))
        return;

    if (req->write) {
        /* Write */
        if (mesa_phy_write(NULL, pit->iport, addr_page, req->value) != VTSS_OK)
            req->count++;
    } else if (mesa_phy_read(NULL, pit->iport, addr_page, &value) != VTSS_OK) {
        /* Read failure */
        req->count++;
    } else {
        if (req->header) {
            req->header = 0;
            ICLI_PRINTF("Port  Addr     Value   15      8 7       0\n");
        }
        ICLI_PRINTF("%-6u0x%02x/%-4u0x%04x  ", pit->uport, addr, addr, value);
        for (i = 15; i >= 0; i--) {
            ICLI_PRINTF("%u%s", value & (1<<i) ? 1 : 0, i == 0 ? "\n" : (i % 4) ? "" : ".");
        }
    }
}

#define PHY_ICLI_PHY_ADDR_MAX 32

void phy_icli_debug_phy(phy_icli_req_t *req)
{
    u32                   session_id = req->session_id;
    icli_unsigned_range_t *list = req->addr_list;
    u8                    i, j, addr, addr_list[PHY_ICLI_PHY_ADDR_MAX];
    port_iter_t           pit;
    
    /* Build address list */
    for (addr = 0; addr < PHY_ICLI_PHY_ADDR_MAX; addr++) {
        addr_list[addr] = (list == NULL ? 1 : 0);
    }
    for (i = 0; list != NULL && i < list->cnt; i++) {
        for (j = list->range[i].min; j < PHY_ICLI_PHY_ADDR_MAX && j <= list->range[i].max; j++) {
            addr_list[j] = 1;
        }
    }

    if (req->addr_sort) {
        /* Iterate in (address, port) order */
        for (addr = 0; addr < PHY_ICLI_PHY_ADDR_MAX; addr++) {
            if (addr_list[addr]) {
                phy_icli_port_iter_init(&pit);
                while (icli_port_iter_getnext(&pit, req->port_list)) {
                    phy_icli_phy_reg(req, &pit, addr);
                }
            }
        }
    } else {
        /* Iterate in (port, address) order */
        phy_icli_port_iter_init(&pit);
        while (icli_port_iter_getnext(&pit, req->port_list)) {
            for (addr = 0; addr < PHY_ICLI_PHY_ADDR_MAX; addr++) {
                if (addr_list[addr]) {
                    phy_icli_phy_reg(req, &pit, addr);
                }
            }
        }
    }
    if (req->count) {
        ICLI_PRINTF("%u operations failed\n", req->count);
    }
}


mesa_rc phy_icli_debug_phy_mode_set(i32 session_id, icli_stack_port_range_t *plist, BOOL has_media_if, u8 media_if, BOOL has_conf_mode, u8 conf_mode, BOOL has_speed, BOOL has_1g, BOOL has_100M, BOOL has_10M)
{
    mesa_phy_reset_conf_t reset_conf;
    mesa_phy_conf_t       setup_conf;
    switch_iter_t         sit;
    port_iter_t           pit;

    // Loop through all switches in a stack
    VTSS_RC(icli_switch_iter_init(&sit));
    while (icli_switch_iter_getnext(&sit, plist)) {
        // Loop though the ports
        VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT));
        while (icli_port_iter_getnext(&pit, plist)) {
            VTSS_RC(mesa_phy_reset_get(NULL, pit.iport, &reset_conf));
            VTSS_RC(mesa_phy_conf_get(NULL, pit.iport, &setup_conf));

            if (has_media_if || has_conf_mode || has_speed ) { 
                if (has_media_if) {
                    reset_conf.media_if = (mesa_phy_media_interface_t)media_if;
                }
                VTSS_RC(mesa_phy_reset(NULL, pit.iport, &reset_conf));

                if (has_conf_mode) {
                    setup_conf.mode = (mesa_phy_mode_t)conf_mode;
                }
                if (conf_mode == MESA_PHY_MODE_FORCED && has_speed) {
                    if (has_100M) {
                        ICLI_PRINTF("Speed 100M\n");
                        setup_conf.forced.speed = MESA_SPEED_100M;                
                    } 

                    if (has_10M) {
                        ICLI_PRINTF("Speed 10M\n");
                        setup_conf.forced.speed = MESA_SPEED_10M;              
                    }

                    if (has_1g) {
                        ICLI_PRINTF("Speed 1G\n");
                        setup_conf.forced.speed = MESA_SPEED_1G;                
                    }
                } else if (!has_speed) {
                    ICLI_PRINTF("Setting default forced speed to 1G \n");
                    setup_conf.forced.speed = MESA_SPEED_1G;
                }
                ICLI_PRINTF("conf_mode:%d media_if:%d speed %u \n", conf_mode, media_if,setup_conf.forced.speed);
                VTSS_RC(mesa_phy_conf_set(NULL, pit.iport, &setup_conf));
            } else {
                ICLI_PRINTF("conf_mode:%d media_if:%d speed %u \n", setup_conf.mode, reset_conf.media_if,setup_conf.forced.speed);
            }
        }
    }
    return VTSS_RC_OK;
}

//  see port_icli_functions.h
mesa_rc phy_icli_debug_phy_pass_through_speed(i32 session_id, icli_stack_port_range_t *plist, BOOL has_1g, BOOL has_100M, BOOL has_10M)
{
    mesa_phy_reset_conf_t reset_conf;
    mesa_phy_conf_t       setup_conf;
    switch_iter_t         sit;
    port_iter_t           pit;
  
    // Loop through all switches in a stack
    VTSS_RC(icli_switch_iter_init(&sit));
    while (icli_switch_iter_getnext(&sit, plist)) {
        // Loop though the ports
        VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT));
        while (icli_port_iter_getnext(&pit, plist)) {
            VTSS_RC(mesa_phy_reset_get(NULL, pit.iport, &reset_conf));
            reset_conf.media_if = MESA_PHY_MEDIA_IF_SFP_PASSTHRU;
            VTSS_RC(mesa_phy_reset(NULL, pit.iport, &reset_conf));

            VTSS_RC(mesa_phy_conf_get(NULL, pit.iport, &setup_conf));
            setup_conf.mode = MESA_PHY_MODE_FORCED;
            if (has_100M) {
                setup_conf.forced.speed = MESA_SPEED_100M;                
            } 

            if (has_10M) {
                setup_conf.forced.speed = MESA_SPEED_10M;              
            }

            if (has_1g) {
                setup_conf.forced.speed = MESA_SPEED_1G;                
            }
            
            VTSS_RC(mesa_phy_conf_set(NULL, pit.iport, &setup_conf));
        }
    }
    return VTSS_RC_OK;
}



//  see port_icli_functions.h
mesa_rc phy_icli_debug_do_page_chk(i32 session_id, BOOL has_enable, BOOL has_disable) {
    if (has_enable) {
        VTSS_RC(mesa_phy_do_page_chk_set(NULL, TRUE));
    } else if (has_disable) {
        VTSS_RC(mesa_phy_do_page_chk_set(NULL, FALSE));
    } else {
        BOOL enabled;
        VTSS_RC(mesa_phy_do_page_chk_get(NULL, &enabled));
        ICLI_PRINTF("Do page check is %s \n", enabled ? "enabled" : "disabled");
    }
    return VTSS_RC_OK;
}

//  see phy_icli_functions.h
mesa_rc phy_icli_debug_phy_reset(i32 session_id, icli_stack_port_range_t *plist) {
    port_iter_t   pit;
    switch_iter_t sit;
    // Loop through all switches in a stack
    VTSS_RC(icli_switch_iter_init(&sit));
    while (icli_switch_iter_getnext(&sit, plist)) {
        
        // Loop though the ports
        VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT));
        while (icli_port_iter_getnext(&pit, plist)) {
            VTSS_RC(do_phy_reset(pit.iport));
        }
    }
    return VTSS_RC_OK;
}
//  see phy_icli_functions.h
mesa_rc phy_icli_debug_phy_loop(i32 session_id, icli_stack_port_range_t *plist, BOOL has_near, BOOL has_far, BOOL has_connector, BOOL has_mac_serdes_input,
                                BOOL has_mac_serdes_facility, BOOL has_mac_serdes_equipment, BOOL has_media_serdes_input, BOOL has_media_serdes_facility,
                                 BOOL has_media_serdes_equipment, BOOL no) {
    port_iter_t   pit;
    switch_iter_t sit;
    // Loop through all switches in a stack
    VTSS_RC(icli_switch_iter_init(&sit));
    while (icli_switch_iter_getnext(&sit, plist)) {
        
        // Loop though the ports
        VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT));
        while (icli_port_iter_getnext(&pit, plist)) {
            mesa_phy_loopback_t lb;
            VTSS_RC(mesa_phy_loopback_get(PHY_INST, pit.iport, &lb));
            if (has_near) {
                lb.near_end_enable = !no;
            }

            if (has_far) {
                lb.far_end_enable = !no;
            }
            if (has_connector) {
                lb.connector_enable = !no;
            }
            if (has_mac_serdes_input) {
                lb.mac_serdes_input_enable = !no;
            }
            if (has_mac_serdes_facility) {
                lb.mac_serdes_facility_enable = !no;
            }
            if (has_mac_serdes_equipment) {
                lb.mac_serdes_equipment_enable = !no;
            }
            if (has_media_serdes_input) {
                lb.media_serdes_input_enable = !no;
            }
            if (has_media_serdes_facility) {
                lb.media_serdes_facility_enable = !no;
            }
            if (has_media_serdes_equipment) {
                lb.media_serdes_equipment_enable = !no;
            }
            T_I("far_end:%d, near:%d, has_far:%d, has_near:%d", lb.far_end_enable, lb.near_end_enable, has_far, has_near);
            VTSS_RC(mesa_phy_loopback_set(PHY_INST, pit.iport, lb));
        }
    }
    return VTSS_RC_OK;
}
static const char *ring_res_type2txt(const mesa_phy_ring_resiliency_conf_t *rrng_rsln)
{
    switch(rrng_rsln->ring_res_status) {
        case MESA_PHY_TIMING_DEFAULT_SLAVE: return "DEFAULT-TIMING-SLAVE ";
        case MESA_PHY_TIMING_SLAVE_AS_MASTER: return "TIMING-SLAVE-AS-MASTER";
        case MESA_PHY_TIMING_DEFAULT_MASTER: return  "DEFAULT-TIMING-MASTER";
        case MESA_PHY_TIMING_MASTER_AS_SLAVE: return "TIMING-MASTER-AS-SLAVE";
        case MESA_PHY_TIMING_LP_NOT_RESILIENT_CAP: return "TIMING_LP_NOT_RESILIENT_CAP";
    }
    return "INVALID";
}
mesa_rc phy_icli_debug_phy_ring_resiliency_conf(i32 session_id, icli_stack_port_range_t *plist, BOOL has_enable,
                                                BOOL has_disable, BOOL has_get)
{

    port_iter_t   pit;
    switch_iter_t sit;
    // Loop through all switches in a stack
    VTSS_RC(icli_switch_iter_init(&sit));
    while (icli_switch_iter_getnext(&sit, plist)) {

        // Loop though the ports
        VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT));
        while (icli_port_iter_getnext(&pit, plist)) {
            mesa_phy_ring_resiliency_conf_t r_rrslnt;
            VTSS_RC(mesa_phy_ring_resiliency_conf_get(PHY_INST, pit.iport, &r_rrslnt));
            if(has_get) {
                ICLI_PRINTF("RING-RESILIENCY enabled :%s\n", r_rrslnt.enable_rrslnt ? "TRUE" : "FALSE");
                ICLI_PRINTF("RING-RESILIENCY node-state :%s\n", ring_res_type2txt(&r_rrslnt));
                ICLI_PRINTF("\n\n\n");
            }
            if (has_enable) {
                r_rrslnt.enable_rrslnt = TRUE;
            }
            if (has_disable) {
                r_rrslnt.enable_rrslnt = FALSE;
            }
            if(has_disable || has_enable ) {
                VTSS_RC(mesa_phy_ring_resiliency_conf_set(PHY_INST, pit.iport, &r_rrslnt));
            }
        }
    }
    return VTSS_RC_OK;

}
//  see phy_icli_functions.h
mesa_rc phy_icli_debug_phy_gpio(i32 session_id, icli_stack_port_range_t *plist, BOOL has_mode_output, BOOL has_mode_input, BOOL has_mode_alternative, BOOL has_gpio_get, BOOL has_gpio_set, BOOL value, u8 gpio_no) {
    port_iter_t   pit;
    switch_iter_t sit;
    // Loop through all switches in a stack
    VTSS_RC(icli_switch_iter_init(&sit));
    while (icli_switch_iter_getnext(&sit, plist)) {
        
        // Loop though the ports
        VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT));
        while (icli_port_iter_getnext(&pit, plist)) {
            if (has_mode_output) {
                VTSS_RC(mesa_phy_gpio_mode(NULL, pit.iport, gpio_no, MESA_PHY_GPIO_OUT));
            }

            if (has_mode_input) {
                VTSS_RC(mesa_phy_gpio_mode(NULL, pit.iport, gpio_no, MESA_PHY_GPIO_IN));
            }

            if (has_mode_alternative) {
                VTSS_RC(mesa_phy_gpio_mode(NULL, pit.iport, gpio_no, MESA_PHY_GPIO_ALT_0));
            }
            
            if (has_gpio_get) {
                VTSS_RC(mesa_phy_gpio_get(NULL, pit.iport, gpio_no, &value));
                ICLI_PRINTF("GPIO:%d is %s\n", gpio_no, value ? "high" : "low");
            }

            if (has_gpio_set) {
                VTSS_RC(mesa_phy_gpio_set(NULL, pit.iport, gpio_no, value));
            }
        }
    }
    return VTSS_RC_OK;
}

vtss_rc phy_icli_debug_phy_led_set(i32 session_id,
                                   icli_stack_port_range_t *plist,
                                   BOOL has_led_num,
                                   u8 led_num,
                                   BOOL has_led_mode,
                                   u8 led_mode)
{
    mesa_phy_led_mode_select_t led_blink_mode;
    switch_iter_t              sit;
    port_iter_t                pit;

    // Loop through all switches in a stack
    VTSS_RC(icli_switch_iter_init(&sit));
    while (icli_switch_iter_getnext(&sit, plist)) {
        // Loop though the ports
        VTSS_RC(icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_FRONT));
        while (icli_port_iter_getnext(&pit, plist)) {
            if (has_led_num || has_led_mode) {
                led_blink_mode.number = MESA_PHY_LED0;
                if (has_led_num) {
                    led_blink_mode.number = (mesa_phy_led_number_t) led_num;
                }

                if (has_led_mode) {
                    led_blink_mode.mode = (mesa_phy_led_mode_t) led_mode;
                } else {
                    ICLI_PRINTF("Select valid LED Mode\n");
                    return VTSS_RC_ERROR;
                }
                VTSS_RC(mesa_phy_led_mode_set(PHY_INST, pit.iport, led_blink_mode));
            }
        }
    }
    return VTSS_RC_OK;
}
