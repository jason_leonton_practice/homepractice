/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 

*/

#ifndef _PHY_ICLI_UTIL_H_
#define _PHY_ICLI_UTIL_H_


#ifdef __cplusplus
extern "C" {
#endif
/* ICLI request structure */
typedef struct {
    u32                     session_id;
    icli_stack_port_range_t *port_list;
    icli_unsigned_range_t   *addr_list;
    u32                     count;
    u32                     page;
    u32                     value;
    BOOL                    addr_sort;
    BOOL                    write;
    BOOL                    header;
    BOOL                    dummy;  /* Unused, for Lint */
} phy_icli_req_t;

void phy_icli_debug_phy(phy_icli_req_t *req);

// Debug function used for testing PHY for simulating cu SFP setup in forced mode.
mesa_rc phy_icli_debug_phy_pass_through_speed(i32 session_id, icli_stack_port_range_t *plist, BOOL has_1g, BOOL has_100M, BOOL has_10M);
mesa_rc phy_icli_debug_phy_mode_set(i32 session_id, icli_stack_port_range_t *plist, BOOL has_media_if, u8 media_if, BOOL has_conf_mode, u8 conf_mode, BOOL has_speed, BOOL has_1g, BOOL has_100M, BOOL has_10M);

// Debug function for enabling/disabling phy page checking. PHY page checking is a mode where the page register is checked for is we are at the right page for all phy registers accesses (This will double the amount of registers access)
mesa_rc phy_icli_debug_do_page_chk(i32 session_id, BOOL has_enable, BOOL has_disable);

/**
 * \brief Debug function for resetting a PHY 
 *
 * \param session_id [IN] The session id use by iCLI print.
 * \param plist      [IN] Port list with ports to configure (Not Stack-aware).
 * \return VTSS_RC_OK if PHY were reset correct else error code.
 **/
mesa_rc phy_icli_debug_phy_reset(i32 session_id, icli_stack_port_range_t *plist);

/**
 * \brief Debug function for accessing/configuring PHY GPIOs
 *
 * \param session_id           [IN] The session id use by iCLI print.
 * \param plist                [IN] Port list with ports to configure (Not Stack-aware).
 * \param has_mode_output      [IN] Set to TRUE in order to configure GPIO as output.
 * \param has_mode_input       [IN] Set to TRUE in order to configure GPIO as input.
 * \param has_mode_alternative [IN] Set to TRUE in order to configure GPIO to alternative mode (See datasheet).
 * \param has_gpio_get         [IN] Set to TRUE in order to print the current state for the GPIO.
 * \param has_gpio_set         [IN] Set to TRUE in order to set the current state of the GPIO.
 * \param value                [IN] Set to TRUE in order to set the current state of the GPIO to high (FALSE = set low). 
 * \param gpio_no              [IN] the gpio number to access/configure.
 * \return VTSS_RC_OK if PHY GPIO were accessed/configured correct else error code.
 **/
mesa_rc phy_icli_debug_phy_gpio(i32 session_id, icli_stack_port_range_t *plist, BOOL has_mode_output, BOOL has_mode_input, BOOL has_mode_alternative, BOOL has_gpio_get, BOOL has_gpio_set, BOOL value, u8 gpio_no);

/**
 * \brief Debug function for setting a PHY into local loopback mode 
 *
 * \param session_id                   [IN] The session id use by iCLI print.
 * \param plist                        [IN] Port list with ports to configure (Not Stack-aware).
 * \param has_near                     [IN] Set to TRUE to configure near-end loopback
 * \param has_far                      [IN] Set to TRUE to configure far-end loopback
 * \param has_connector                [IN] Set to TRUE to configure connector loopback
 * \param has_mac_serdes_input         [IN] Set to TRUE to configure SerDes MAC Input loopback
 * \param has_mac_serdes_facility      [IN] Set to TRUE to configure SerDes MAC Facility loopback
 * \param has_mac_serdes_equipment     [IN] Set to TRUE to configure SerDes MAC Equipment loopback
 * \param has_media_serdes_input       [IN] Set to TRUE to configure SerDes MEDIA Input loopback
 * \param has_media_serdes_faciliy     [IN] Set to TRUE to configure SerDes MEDIA Facility loopback
 * \param has_media_serdes_equipment   [IN] Set to TRUE to configure SerDes MEDIA Equipment loopback
 * \param no                           [IN] Set to TRUE to disable loopback. Set to FALSE to enable loopback
 * \return VTSS_RC_OK if PHY were reset correct else error code.
 **/
mesa_rc phy_icli_debug_phy_loop(i32 session_id, icli_stack_port_range_t *plist, BOOL has_near, BOOL has_far, BOOL has_connector, BOOL has_mac_serdes_input,
                                BOOL has_mac_serdes_facility, BOOL has_mac_serdes_equipment, BOOL has_media_serdes_input, BOOL has_media_serdes_facility,
                                BOOL has_media_serdes_equipment, BOOL no);
/**
 * \brief Debug function for setting a PHY into ring resiliency mode
 *
 * \param session_id  [IN] The session id use by iCLI print.
 * \param plist       [IN] Port list with ports to configure (Not Stack-aware).
 * \param has_enable  [IN] Set to TRUE to enable ring resiliency
 * \param has_disable [IN] Set to TRUE to disable ring resiliency
 * \param has_get     [IN] Set to TRUE to get ring resiliency state
 * \return VTSS_RC_OK if PHY were reset correct else error code.
 **/
mesa_rc phy_icli_debug_phy_ring_resiliency_conf(i32 session_id, icli_stack_port_range_t *plist, BOOL has_enable,
                                                BOOL has_disable, BOOL has_get);

/**
 * \brief Debug function for setting a PHY LED configuration
 *
 * \param session_id       [IN] The session id use by iCLI print.
 * \param plist            [IN] Port list with ports to configure (Not Stack-aware).
 * \param has_led_num      [IN] Set to TRUE to configure particular LED
 * \param led_num          [IN] Set mode configuration to LED
 * \param has_led_mode     [IN] Set to TRUE to configure LED mode
 * \param led_mode         [IN] Set LED mode configuration
 * \return VTSS_RC_OK if PHY were reset correct else error code.
 **/
vtss_rc phy_icli_debug_phy_led_set(i32 session_id, icli_stack_port_range_t *plist, BOOL has_led_num, u8 led_num, BOOL has_led_mode, u8 led_mode);

#ifdef __cplusplus
}
#endif

#endif /* _PHY_ICLI_UTIL_H_ */
