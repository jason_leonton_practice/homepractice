/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include <stdio.h>
#include <dlfcn.h>

#include "meba.h"
#include "conf_api.h"

meba_inst_t meba_create(const meba_board_interface_t *iface,
                        const char *lib)
{
    meba_inst_t inst = NULL;
    void *dlh = dlopen(lib, RTLD_NOW|RTLD_NODELETE);
    if (!dlh) {
        fprintf(stderr, "Unable to load %s: %s\n", lib, dlerror());
        return NULL;
    }

    // Get a copy of port_conf from CONF module for meba initialization
    lntn_global_conf_t lntn_conf = lntn_conf_get();
    meba_init_lntn_conf_t iniconf = (meba_init_lntn_conf_t) dlsym(dlh, "meba_init_lntn_conf");
    if (!iniconf) {
        fprintf(stderr, "Unable to load %s: No initialize entrypoint for iniconf\n", lib);
    }
    iniconf(lntn_conf);

    meba_initialize_t inifun = (meba_initialize_t) dlsym(dlh, "meba_initialize");
    if (!inifun) {
        fprintf(stderr, "Unable to load %s: No initialize entrypoint\n", lib);
        goto out;
    }

    inst = inifun(sizeof(*iface), iface);

out:
    dlclose(dlh);
    return inst;
}
