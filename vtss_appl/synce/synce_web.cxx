/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.
 */

#include "web_api.h"
#include "synce.h"
#include "synce_board.hxx"
#if defined(VTSS_SW_OPTION_PTP)
#include "ptp_api.h"
#endif
#ifdef VTSS_SW_OPTION_PRIV_LVL
#include "vtss_privilege_api.h"
#include "vtss_privilege_web_api.h"
#endif

/* =================
 * Trace definitions
 * -------------- */
#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_WEB
#include <vtss_trace_api.h>
/* ============== */

#define SYNCE_WEB_BUF_LEN 512

/****************************************************************************/
/*  Web Handler  functions                                                  */
/****************************************************************************/
//
// SYNCE handler
//

const static char *synce_web_error_txt(mesa_rc error)
{
    switch (error)
    {
        case SYNCE_RC_OK:                return("SYNCE_RC_OK");
        case SYNCE_RC_INVALID_PARAMETER: return("SYNCE_RC_INVALID_PARAMETER");
        case SYNCE_RC_NOM_PORT:          return("SYNCE_RC_NOM_PORT");
        case SYNCE_RC_SELECTION:         return("SYNCE_RC_SELECTION");
        case SYNCE_RC_INVALID_PORT:      return("SYNCE_RC_INVALID_PORT");
        case SYNCE_RC_NOT_SUPPORTED:     return("SYNCE_RC_NOT_SUPPORTED");
        default:                         return("SYNCE_RC_INVALID_PARAMETER");
    }
}

const static char *synce_web_lol_state_to_text(vtss_appl_synce_lol_alarm_state_t state) {
    switch(state) {
        case VTSS_APPL_SYNCE_LOL_ALARM_STATE_FALSE: return "Up";
        case VTSS_APPL_SYNCE_LOL_ALARM_STATE_TRUE:  return "Down";
        case VTSS_APPL_SYNCE_LOL_ALARM_STATE_NA:
        default:                                    return "Off";
    }
}

static i32 handler_config_synce(CYG_HTTPD_STATE* p)
{
    vtss_isid_t                       sid = web_retrieve_request_sid(p); /* Includes USID = ISID */
    mesa_port_no_t                    iport;
    mesa_rc                           rc;
    char                              var_rate[32];
    vtss_appl_synce_clock_source_nomination_config_t clock_source_nomination_config;
    vtss_appl_synce_clock_selection_mode_config_t    clock_selection_mode_config;
    vtss_appl_synce_station_clock_config_t           station_clock_config;   
    vtss_appl_synce_port_config_t                    port_config;
    vtss_appl_synce_clock_selection_mode_status_t    clock_selection_mode_status;
    vtss_appl_synce_clock_source_nomination_status_t clock_source_nomination_status;
    vtss_appl_synce_port_status_t                    port_status;
    // vtss_appl_synce_selector_state_t  selector_state;
    // synce_mgmt_alarm_state_t          alarm_state;
    // vtss_appl_synce_quality_level_t   ssm_rx;
    // vtss_appl_synce_quality_level_t   ssm_tx;
    vtss_appl_synce_frequency_t       clock_in_freq;
    vtss_appl_synce_frequency_t       clock_out_freq;
    meba_synce_clock_hw_id_t          clock_hw_id;
    uint                              clock_type;
    uint                              eec_type;
    
    uint                              i, port_cnt, source, nominated, enabled, uport, priority, mode, overwrite, holdover, freerun, wtr, aneg, hold, eec_option, clock_freq;
    int                               ct;
    static mesa_rc                    nom_error = VTSS_OK, prio_error = VTSS_OK, selection_error = VTSS_OK, station_clock_error = VTSS_OK ; // Used to select an error message to be given back to the web page -- 0 = no error
    char                              data_string[2000];
    port_iter_t                       pit;

    if(redirectUnmanagedOrInvalid(p, sid)) /* Redirect unmanaged/invalid access to handler */
        return -1;

#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_SYNCE))
        return -1;
#endif

    //
    // Setting new configuration
    //
    if(p->method == CYG_HTTPD_METHOD_POST)
    {
        for (source=0; source<synce_my_nominated_max; source++)
        {
            sprintf(var_rate, "wtrC_%d", source+1);
            if (cyg_httpd_form_varable_int(p, var_rate, (int *)&enabled))
                if (enabled != 0)    synce_mgmt_wtr_clear_set(source);

            sprintf(var_rate, "nom_%d", source+1);
            nominated = 0;
            if (cyg_httpd_form_varable_find(p, var_rate)) nominated = 1;

            size_t len;
            const char *var_string;
            sprintf(var_rate, "port_%d", source+1);
            var_string = cyg_httpd_form_varable_string(p, var_rate, &len);
            if (len > 0)
            {
                char *end_ptr;
                vtss_ifindex_t ifindex;
                u8 clk_in_port;
                
                if (!strncmp(var_string, "S-CLK&", 6) || !strcmp(var_string, "S-CLK")) {
                    ifindex = VTSS_IFINDEX_NONE;
                    clk_in_port = 0;
                }
                else if (!strncmp(var_string, "PTP-", 4)) {
                    ifindex = VTSS_IFINDEX_NONE;
                    errno = 0;
                    clk_in_port = strtol(var_string + 4, &end_ptr, 10) + 128;
                    if (((*end_ptr != 0) && (*end_ptr != '&')) || (errno != 0)) {
                        T_W("Malformed PTP clock port name (%s).", var_string);
                        continue;  // Make sure this source is not updated with incorrect data - skip the update completely.
                    }
                }
                else {
                    errno = 0;
                    uport = strtol(var_string, &end_ptr, 10);
                    if (((*end_ptr != 0) &&  (*end_ptr != '&')) || (errno != 0)) {
                        T_W("Malformed port number (%s).", var_string);
                        continue;  // Make sure this source is not updated with incorrect data - skip the update completely.
                    }
                    (void) vtss_ifindex_from_port(0, uport2iport(uport), &ifindex);
                    clk_in_port = 0;
                }
                
                sprintf(var_rate, "pri_%d", source+1);
                if (cyg_httpd_form_varable_int(p, var_rate, (int *)&priority))
                {
                    sprintf(var_rate, "ssmO_%d", source+1);
                    if (cyg_httpd_form_varable_int(p, var_rate, (int *)&overwrite))
                    {
                        sprintf(var_rate, "aneg_%d", source+1);
                        if (cyg_httpd_form_varable_int(p, var_rate, (int *)&aneg))
                        {
                            sprintf(var_rate, "hold_%d", source+1);
                            if (cyg_httpd_form_varable_int(p, var_rate, (int *)&hold))
                            {
                                
                                clock_source_nomination_config.nominated = nominated;
                                clock_source_nomination_config.network_port = ifindex;
                                clock_source_nomination_config.clk_in_port = clk_in_port;
                                clock_source_nomination_config.priority = priority;
                                clock_source_nomination_config.aneg_mode = (vtss_appl_synce_aneg_mode_t)aneg;
                                clock_source_nomination_config.ssm_overwrite = (vtss_appl_synce_quality_level_t)overwrite;
                                clock_source_nomination_config.holdoff_time = hold;
                                
                                if ((rc = vtss_appl_synce_clock_source_nomination_config_set(source + 1, &clock_source_nomination_config)) != VTSS_OK) {
                                    if (nom_error == VTSS_OK) nom_error = rc;                                    
                                }
                            }
                        }
                    }
                }
            }
        }

        sprintf(var_rate, "selM");
        if (cyg_httpd_form_varable_int(p, var_rate, (int *)&mode))
        {
            sprintf(var_rate, "selS");
            if (cyg_httpd_form_varable_int(p, var_rate, (int *)&source))
            {
                sprintf(var_rate, "wtrT");
                if (cyg_httpd_form_varable_int(p, var_rate, (int *)&wtr))
                {
                    sprintf(var_rate, "ssmHo");
                    if (cyg_httpd_form_varable_int(p, var_rate, (int *)&holdover))
                    {
                        sprintf(var_rate, "ssmFr");
                        if (cyg_httpd_form_varable_int(p, var_rate, (int *)&freerun))
                        {
                            sprintf(var_rate, "eecOpt");
                            if (cyg_httpd_form_varable_int(p, var_rate, (int *)&eec_option))
                            {
                                clock_selection_mode_config.selection_mode = (vtss_appl_synce_selection_mode_t)mode;
                                clock_selection_mode_config.source = source;
                                clock_selection_mode_config.wtr_time = wtr;
                                clock_selection_mode_config.ssm_holdover = (vtss_appl_synce_quality_level_t)holdover;
                                clock_selection_mode_config.ssm_freerun = (vtss_appl_synce_quality_level_t)freerun;
                                clock_selection_mode_config.eec_option = (vtss_appl_synce_eec_option_t)eec_option;
                                
                                if ((rc = vtss_appl_synce_clock_selection_mode_config_set(&clock_selection_mode_config)) != VTSS_OK)
                                {                                                                                      
                                    if (selection_error == VTSS_OK) selection_error = rc;
                                }
                            }
                        }
                    }
                }
            }
        }

        sprintf(var_rate, "clkIn");
        if (cyg_httpd_form_varable_int(p, var_rate, (int *)&clock_freq)) {
            station_clock_error = synce_mgmt_station_clock_in_set((vtss_appl_synce_frequency_t)clock_freq);
        }
        sprintf(var_rate, "clkOut");
        if (cyg_httpd_form_varable_int(p, var_rate, (int *)&clock_freq)) {
            station_clock_error = synce_mgmt_station_clock_out_set((vtss_appl_synce_frequency_t)clock_freq);
        }

        port_iter_init_local(&pit);
        while (port_iter_getnext(&pit)) {
            iport = pit.iport;
            sprintf(var_rate, "ssmE_%u", iport2uport(iport));
            enabled = 0;
            if (cyg_httpd_form_varable_find(p, var_rate))
              enabled = 1;
            synce_mgmt_ssm_set(iport, enabled);
        }

        redirect(p, "/synce_config.htm");
    }
    else {
        rc = vtss_appl_synce_clock_selection_mode_config_get(&clock_selection_mode_config);
        rc = vtss_appl_synce_station_clock_config_get(&station_clock_config);
        rc = vtss_appl_synce_clock_selection_mode_status_get(&clock_selection_mode_status);
        
        rc = synce_mgmt_station_clock_type_get(&clock_type);      // clock type can be 0, 1 or 2 depending on the type of board.
        rc = synce_mgmt_eec_option_type_get(&eec_type);
        rc = synce_mgmt_station_clock_in_get(&clock_in_freq);
        rc = synce_mgmt_station_clock_out_get(&clock_out_freq);
        rc = clock_hardware_id_get(&clock_hw_id);

        cyg_httpd_start_chunked("html");

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|",
                      synce_my_nominated_max);
        cyg_httpd_write_chunked(p->outbuffer, ct);

        for (source = 0; source<synce_my_nominated_max; source++)
        {
            /* Get source configuration and status */
            vtss_appl_synce_clock_source_nomination_config_get((source + 1), &clock_source_nomination_config);
            vtss_appl_synce_clock_source_nomination_status_get((source + 1), &clock_source_nomination_status);
            
            /* Count how many ports can be selected for this clk source */
            for (i = 0, port_cnt = 0; i < SYNCE_PORT_COUNT + PTP_CLOCK_INSTANCES; ++i) if (synce_get_source_port(source, i)) port_cnt++;

            data_string[0] = '\0';
            sprintf(data_string,"%u/", port_cnt);

            /* The possible selected ports for this source is written to the buffer */
            for (i = 0; i < SYNCE_PORT_COUNT + PTP_CLOCK_INSTANCES; ++i) {
                if (synce_get_source_port(source, i)) {
                    if (i == SYNCE_STATION_CLOCK_PORT) {
                        sprintf(data_string,"%sS-CLK/", data_string);
                    }
                    else if (i > SYNCE_STATION_CLOCK_PORT) {
                        sprintf(data_string,"%sPTP-%u/", data_string, i - SYNCE_STATION_CLOCK_PORT - 1);
                    }
                    else {
                        sprintf(data_string,"%s%u/", data_string, iport2uport(i));
                    }
                }
            }
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s", data_string);
            cyg_httpd_write_chunked(p->outbuffer, ct);

            /* Rest of the information for this cource is written to the buffer */
            u32 port;
            synce_network_port_clk_in_port_combo_to_port(clock_source_nomination_config.network_port, clock_source_nomination_config.clk_in_port, &port);
            if (port == SYNCE_STATION_CLOCK_PORT) {
                sprintf(data_string, "S-CLK");
            }
            else if (port > SYNCE_STATION_CLOCK_PORT) {
                sprintf(data_string,"PTP-%u", port - SYNCE_STATION_CLOCK_PORT - 1);
            }
            else {
                sprintf(data_string,"%u", iport2uport(port));
            }
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u/%s/%u/%u/%u/%u/%s/%s/%s/%s/%u|",
                          source + 1,
                          clock_source_nomination_config.nominated,
                          data_string,
                          clock_source_nomination_config.priority,
                          clock_source_nomination_config.ssm_overwrite,
                          clock_source_nomination_config.holdoff_time,
                          (uint)clock_source_nomination_config.aneg_mode,
                          clock_source_nomination_status.locs ? "Down" : "Up",
                          clock_source_nomination_status.fos ? "Down" : "Up",
                          clock_source_nomination_status.ssm ? "Down" : "Up",
                          clock_source_nomination_status.wtr ? "Down" : "Up",
                          0);
            cyg_httpd_write_chunked(p->outbuffer, ct);
        }
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%s|",
                      synce_web_error_txt((nom_error != VTSS_OK) ? nom_error : prio_error));
        cyg_httpd_write_chunked(p->outbuffer, ct);

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u/%u/%u/%u/%u/%u/%u/%u/%s/%s/%s|%s|",
                      (uint)(clock_selection_mode_config.selection_mode),
                      clock_selection_mode_config.source,
                      clock_selection_mode_config.wtr_time,
                      clock_selection_mode_config.ssm_holdover,
                      clock_selection_mode_config.ssm_freerun,
                      eec_type,
                      clock_selection_mode_config.eec_option,
                      (uint)clock_selection_mode_status.selector_state,
                      clock_selection_mode_status.clock_input+1,
                      clock_selection_mode_status.losx ? "Down" : "Up",
                      synce_web_lol_state_to_text(clock_selection_mode_status.lol),
                      clock_selection_mode_status.dhold ? "Down" : "Up",
                      synce_web_error_txt(selection_error));
        cyg_httpd_write_chunked(p->outbuffer, ct);

        /* add station clock data */
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u/%u/%u|%s|",
                      clock_type,
                      clock_in_freq,
                      clock_out_freq,
                      clock_hw_id,
                      synce_web_error_txt(station_clock_error));
        cyg_httpd_write_chunked(p->outbuffer, ct);
        
        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", port_isid_port_count(VTSS_ISID_LOCAL));
        cyg_httpd_write_chunked(p->outbuffer, ct);

        vtss_ifindex_t ifindex;    
        port_iter_init_local(&pit);
        while (port_iter_getnext(&pit)) {
            iport = pit.iport;
            if (vtss_ifindex_from_port(0, pit.iport, &ifindex) != VTSS_RC_OK) {
                T_E("Unable to convert %u to an ifindex", pit.iport);
            }
            rc = vtss_appl_synce_port_config_get(ifindex, &port_config);
            rc = vtss_appl_synce_port_status_get(ifindex, &port_status);
            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u/%u/%u/%u/%u|",
                          iport2uport(iport),
                          port_config.ssm_enabled,
                          port_status.ssm_tx,
                          port_status.ssm_rx,
                          port_status.master);
            cyg_httpd_write_chunked(p->outbuffer, ct);
        }

        ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%u|", PTP_CLOCK_INSTANCES);
        cyg_httpd_write_chunked(p->outbuffer, ct);
#if defined(VTSS_SW_OPTION_PTP)
        for (i = 0; i < PTP_CLOCK_INSTANCES; i++) {
            synce_ptp_port_state_t ptp_port_state;
            uint ssm_rx;

            (void) vtss_synce_ptp_port_state_get(i, &ptp_port_state);
            switch (ptp_port_state.ssm_rx)
            {
                case SSM_QL_PRS_INV:  ssm_rx = (clock_selection_mode_config.eec_option == VTSS_APPL_SYNCE_EEC_OPTION_2) ? VTSS_APPL_SYNCE_QL_PRS : VTSS_APPL_SYNCE_QL_INV; break;
                case SSM_QL_STU:      ssm_rx = VTSS_APPL_SYNCE_QL_STU;  break;
                case SSM_QL_PRC:      ssm_rx = VTSS_APPL_SYNCE_QL_PRC;  break;
                case SSM_QL_ST2:      ssm_rx = VTSS_APPL_SYNCE_QL_ST2;  break;
                case SSM_QL_SSUA_TNC: ssm_rx = (clock_selection_mode_config.eec_option == VTSS_APPL_SYNCE_EEC_OPTION_2) ? VTSS_APPL_SYNCE_QL_TNC : VTSS_APPL_SYNCE_QL_SSUA; break;
                case SSM_QL_SSUB:     ssm_rx = VTSS_APPL_SYNCE_QL_SSUB; break;
                case SSM_QL_ST3E:     ssm_rx = VTSS_APPL_SYNCE_QL_ST3E; break;
                case SSM_QL_EEC2:     ssm_rx = VTSS_APPL_SYNCE_QL_EEC2; break;
                case SSM_QL_EEC1:     ssm_rx = VTSS_APPL_SYNCE_QL_EEC1; break;
                case SSM_QL_SMC:      ssm_rx = VTSS_APPL_SYNCE_QL_SMC;  break;
                case SSM_QL_PROV:     ssm_rx = VTSS_APPL_SYNCE_QL_PROV; break;
                case SSM_QL_DNU_DUS:  ssm_rx = (clock_selection_mode_config.eec_option == VTSS_APPL_SYNCE_EEC_OPTION_2) ? VTSS_APPL_SYNCE_QL_DUS : VTSS_APPL_SYNCE_QL_DNU; break;
                default:              ssm_rx = VTSS_APPL_SYNCE_QL_FAIL; break;  // NOTE: This includes the case SSM_QL_FAIL
            }

            ct = snprintf(p->outbuffer, sizeof(p->outbuffer), "%d/%u/%u|",
                          i,
                          ssm_rx,
                          ptp_port_state.ptsf);
            cyg_httpd_write_chunked(p->outbuffer, ct);
        }
#endif
        nom_error = VTSS_OK;
        prio_error = VTSS_OK;
        selection_error = VTSS_OK;

        cyg_httpd_end_chunked();
    }
    return -1; // Do not further search the file system.
}

// Status
static i32 handler_status_synce(CYG_HTTPD_STATE* p)
{
#ifdef VTSS_SW_OPTION_PRIV_LVL
    if (web_process_priv_lvl(p, VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_SYNCE))
        return -1;
#endif

    if(p->method == CYG_HTTPD_METHOD_GET) {
        cyg_httpd_start_chunked("html");
        cyg_httpd_end_chunked();
    }
    return -1; // Do not further search the file system.
}

/****************************************************************************/
/*  Module JS lib config routine                                            */
/****************************************************************************/

static size_t synce_lib_config_js(char **base_ptr, char **cur_ptr, size_t *length)
{
    char buff[SYNCE_WEB_BUF_LEN];

    (void) snprintf(buff, SYNCE_WEB_BUF_LEN, 
                    "var configSyncePrioMax = %u;\n"
                    "var configSynceNormMax = %u;\n", 
                    synce_my_priority_max, 
                    synce_my_nominated_max);

    return webCommonBufferHandler(base_ptr, cur_ptr, length, buff);
}

/****************************************************************************/
/*  JS lib config table entry                                               */
/****************************************************************************/

web_lib_config_js_tab_entry(synce_lib_config_js);

/****************************************************************************/
/*  HTTPD Table Entries                                                     */
/****************************************************************************/

CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_config_synce, "/config/synceConfig", handler_config_synce);
CYG_HTTPD_HANDLER_TABLE_ENTRY(get_cb_status_synce, "/stat/synce_status", handler_status_synce);

/****************************************************************************/
/*  End of file.                                                            */
/****************************************************************************/
