/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

#include "critd_api.h"
#include "port_api.h"
#include "conf_api.h"
#include "misc_api.h"
#include "main.h"
#include "control_api.h"
#include "main_types.h"
#include "board_if.h"
#if defined(VTSS_SW_OPTION_PTP)
#include "ptp_api.h"
#include "vtss_ptp_api.h"
#include "vtss_tod_api.h"
#endif

#include "synce_custom_clock_api.h"
#include "synce_dpll_base.h"
extern synce_dpll_base *synce_dpll;

#ifdef VTSS_SW_OPTION_PACKET
#include "packet_api.h"
#endif
#ifdef VTSS_SW_OPTION_PHY
#include "phy_api.h"
#endif

#ifdef VTSS_SW_OPTION_ICFG
#include "synce_icli_functions.h" // For synce_icfg_init
#endif

#include "lock.hxx"
#include "synce_trace.h"

#include "mscc/ethernet/switch/api.h"

#include "synce_board.hxx"

#define VTSS_RC(expr) { mesa_rc __rc__ = (expr); if (__rc__ < VTSS_RC_OK) return __rc__; }

/****************************************************************************/
/*  Global variables                                                                                                                      */
/****************************************************************************/

#define SSM_OK          0x00
#define SSM_LINK_DOWN   0x01
#define SSM_FAIL        0x02
#define SSM_INVALID     0x04
#define SSM_NOT_SEEN    0x08

#define WTR_OK          0x00
#define WTR_LINK        0x01
#define WTR_SSM_FAIL    0x02
#define WTR_SSM_INV     0x04
#define WTR_FOS         0x08
#define WTR_LOCS        0x10

#define FLAG_CLOCK_SOURCE_LOCS 0x0001
#define FLAG_CLOCK_SOURCE_FOS  0x0002
#define FLAG_CLOCK_SOURCE_LOSX 0x0004
#define FLAG_CLOCK_SOURCE_LOL  0x0008
#define FLAG_PORT_CALL_BACK    0x0010
#define FLAG_FRAME_RX          0x0020
#define FLAG_ONE_SEC           0x0040
#define FLAG_SSM_EVENT         0x0080
#define FLAG_PTSF_CHANGE       0x0100

#define PREFER_MAX  2
#define PREFER_TIMEOUT 8
#define PREFER_RESET  PREFER_TIMEOUT+10

#define SWITCH_AUTO_SQUELCH true              /* Enable auto squelching in the Switch (this was earlier disabled to avoid detection of
                                                 LOCS before Link down on Serdes ports. The problem was that the WTR timer was not
                                                 activated if Locs was detected before Link Down.
                                                 This problem has now been solved in func_thread in the PORT_CALL_BACK action. */

uint synce_my_nominated_max;                  /* actual max number of nominated ports */
uint synce_my_priority_max;                   /* actual max number of priorities */
uint synce_my_prio_disabled;
static uint my_oscillator_pri = synce_my_prio_disabled;

typedef struct
{
    bool    new_ssm;
    bool    new_link;
    bool    new_fiber;
    uint    ssm_frame;
    uint    ssm_rx;
    uint    ssm_tx;
    uint    ssm_count;
    uint    ssm_state;
    mesa_port_speed_t speed;
    bool    fiber;
    bool    master;
    bool    link;
    bool    phy;
    bool    first_port_callback;
    uint    prefer_timer;
    bool    actual_link;
    bool    port_dnu;           // set if the port cannot be used as synce source (e.g. 10G ports on Serval2_lite)
#if defined(VTSS_SW_OPTION_PTP)
    vtss_appl_synce_ptp_ptsf_state_t ptsf;
    bool    new_ptsf;
#endif
} port_state_t;

typedef struct
{
    bool    new_locs;
    bool    new_fos;
    uint    wtr_state;
    uint    wtr_timer;  // Note: Representation of wtr_time values in the wtr_timer field in this structure is in units of seconds.
    bool    holdoff;
} clock_state_t;

#if defined(VTSS_SW_OPTION_PTP)
typedef struct
{
    bool    new_link;
    bool    link;
    bool    new_ptsf;
    bool    first_port_callback;
    vtss_appl_synce_ptp_ptsf_state_t ptsf;
    uint    ssm_frame;
    bool    new_ssm;
} ptsf_state_t;
#endif

typedef struct
{
    vtss_appl_synce_clock_source_nomination_config_t clock_source_nomination_config[SYNCE_NOMINATED_MAX];
    vtss_appl_synce_clock_selection_mode_config_t    clock_selection_mode_config;
    vtss_appl_synce_station_clock_config_t           station_clock_config;
    CapArray<vtss_appl_synce_port_config_t, VTSS_APPL_CAP_SYNCE_PORT_AND_STATION_AND_PTP_CNT> port_config;
} conf_blk_t;

typedef struct
{
    bool active;
    bool master;
    u32  timer;
} port_prefer_state;

#ifdef VTSS_SW_OPTION_PACKET
static u8  ssm_dmac[6] = {0x01,0x80,0xC2,0x00,0x00,0x02};
static u8  ssm_ethertype[2] = {0x88,0x09};
static u8  ssm_standard[6] = {0x0A,0x00,0x19,0xA7,0x00,0x01};
#endif

static vtss_flag_t   func_wait_flag;
static vtss_handle_t func_thread_handle;
static vtss_thread_t func_thread_block;

/*lint -esym(457, crit) */
static critd_t          crit;
static critd_t          crit_ptsf;

static vtss_appl_synce_clock_source_nomination_config_t clock_source_nomination_config[SYNCE_NOMINATED_MAX];
static vtss_appl_synce_clock_selection_mode_config_t clock_selection_mode_config;
static vtss_appl_synce_station_clock_config_t station_clock_config;
static CapArray<vtss_appl_synce_port_config_t, VTSS_APPL_CAP_SYNCE_PORT_AND_STATION_AND_PTP_CNT> port_config;
static synce_mgmt_alarm_state_t           clock_alarm_state;
static vtss_appl_synce_selector_state_t   clock_old_selector_state;

static CapArray<port_state_t, VTSS_APPL_CAP_SYNCE_PORT_AND_STATION_AND_PTP_CNT> port_state;
static clock_state_t                 clock_state[SYNCE_NOMINATED_MAX];
#if defined(VTSS_SW_OPTION_PTP)
static ptsf_state_t                  ptsf_state[PTP_CLOCK_INSTANCES];
#endif

/*
 * The priority assigned to a source based on the nominated priority, the quality of the source and the quality og the internal oscillator.
 */
static uint                          prio_assigned[SYNCE_NOMINATED_MAX];

static mesa_phy_clock_conf_t         phy_clock_config[SYNCE_NOMINATED_MAX];

// static bool cur_top_state = false;
// static bool cur_top_init = false;

#if defined(VTSS_SW_OPTION_PTP)
static int best_master_source;
static int best_master = -1;

#endif

static vtss_appl_synce_quality_level_t current_ql;
static vtss_appl_synce_selector_state_t current_selector_state;
static u32 current_selected_port;
static bool physical_port_selected = false;

#if (VTSS_TRACE_ENABLED)
static vtss_trace_reg_t trace_reg =
{
    VTSS_TRACE_MODULE_ID, "SyncE", "SyncE module."
};

static vtss_trace_grp_t trace_grps[TRACE_GRP_CNT] =
{
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CRIT */ {
        "crit",
        "Critical regions ",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_PDU_RX */ {
        "rx",
        "Rx PDU print out ",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_PDU_TX */ {
        "tx",
        "Tx PDU print out ",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_API */ {
        "api",
        "Switch API printout",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CLOCK */ {
        "clock",
        "Clock API printout ",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CLI */ {
        "cli",
        "CLI",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_DEVELOP */ {
        "develop",
        "Develop",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_BOARD */ {
        "board",
        "Board",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    }
};

#define SYNCE_CRIT_ENTER() critd_enter(&crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define SYNCE_CRIT_EXIT()  critd_exit( &crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define SYNCE_PTSF_CRIT_ENTER() critd_enter(&crit_ptsf, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define SYNCE_PTSF_CRIT_EXIT()  critd_exit( &crit_ptsf, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#else
#define SYNCE_CRIT_ENTER() critd_enter(&crit)
#define SYNCE_CRIT_EXIT()  critd_exit( &crit)
#define SYNCE_PTSF_CRIT_ENTER() critd_enter(&crit_ptsf)
#define SYNCE_PTSF_CRIT_EXIT()  critd_exit( &crit_ptsf)
#endif /* VTSS_TRACE_ENABLED */

static vtss::Lock masterLock; // Lock to keep state of master up/down

#if defined(VTSS_SW_OPTION_PRIVATE_MIB)
    #ifdef __cplusplus
        extern "C" void vtss_synce_mib_init();
    #else
        void vtss_synce_mib_init();
    #endif
#endif

#if defined(VTSS_SW_OPTION_JSON_RPC)
    #ifdef __cplusplus
        extern "C" void vtss_appl_synce_json_init();
    #else
        void vtss_appl_synce_json_init();
    #endif
#endif

/****************************************************************************/
/*  Various local functions                                                                                                          */
/****************************************************************************/
static void configure_master_slave(void);
static void set_tx_ssm(void);
static void set_clock_source(uint source);
static void set_wtr(uint source);
static bool port_is_nominated(uint port, uint *source);

mesa_rc synce_network_port_clk_in_port_combo_to_port(vtss_ifindex_t network_port, u8 clk_in_port, u32 *v)
{
    if (network_port != 0) {
        vtss_ifindex_elm_t e;
        VTSS_RC(vtss_ifindex_decompose(network_port, &e));
        if (e.iftype != VTSS_IFINDEX_TYPE_PORT) {
            T_D("Interface %u is not a port interface", VTSS_IFINDEX_PRINTF_ARG(network_port));
            return VTSS_RC_ERROR;
        }
        *v = e.ordinal;
    }
    else if (clk_in_port == 0) {
        *v = SYNCE_STATION_CLOCK_PORT;
    }
#if defined(VTSS_SW_OPTION_PTP)
    else {
        *v = SYNCE_STATION_CLOCK_PORT + 1 + (clk_in_port - 128);
    }
#else
    else {
        *v = SYNCE_STATION_CLOCK_PORT;
        T_E("clk_in_port was != 0 but support for PTP ports in SyncE is not included.");
    }
#endif

    return VTSS_RC_OK;
}

static void port_change_callback(mesa_port_no_t port_no, port_info_t *info)
{
    uint  port;

    port = port_no-VTSS_PORT_NO_START;

    if (port >= SYNCE_PORT_COUNT)  return;

    SYNCE_CRIT_ENTER();
    port_state[port].first_port_callback = true;
    if (port_state[port].link != info->link)
    {
        port_state[port].new_link = true;
        if (port_state[port].prefer_timer == 0) {
            port_state[port].link = info->link;
        } else {
            port_state[port].link = true;
        }
        T_I("new Link state %d, prefer_timer %d", info->link, port_state[port].prefer_timer);
    }
    port_state[port].actual_link = info->link;
    if (port_state[port].fiber != info->fiber)
    {
        port_state[port].new_fiber = true;
        port_state[port].fiber = info->fiber;
    }
    port_state[port].phy = info->phy;
    port_state[port].speed = info->speed;
    //if (!info->phy) {
    //    if (port_10g_phy(port_no)) {
    //        port_state[port].phy = true;
    //        if (port_state[port].fiber != true)
    //        {
    //            port_state[port].new_fiber = true;
    //            port_state[port].fiber = true;
    //        }
    //    }
    //}
    meba_port_cap_t port_cap = vtss_board_port_cap(port);
    if ((port_cap & MEBA_PORT_CAP_10G_FDX) && MESA_CAP(MESA_CAP_SYNCE_10G_DNU)) {
        port_state[port].port_dnu = true;
    } else {
        port_state[port].port_dnu = false;
    }
    T_I("Port %d port_dnu %s", port, port_state[port].port_dnu ? "True " : "False");

    T_I("port_no %u  link %u, fiber %u, phy %d, speed %d", port_no, info->link, port_state[port].fiber, port_state[port].phy, info->speed);

    vtss_flag_setbits(&func_wait_flag, FLAG_PORT_CALL_BACK);
    SYNCE_CRIT_EXIT();
}

static void system_reset(mesa_restart_t restart)
{
    if (clock_selection_mode_set(VTSS_APPL_SYNCE_SELECTOR_MODE_FORCED_HOLDOVER, 0) != VTSS_OK)    T_D("error returned");
}

static bool port_is_nominated(uint port, uint *source)
{
    for (*source=0; *source<synce_my_nominated_max; ++*source)
    {
        u32 iport;
        (void) synce_network_port_clk_in_port_combo_to_port(clock_source_nomination_config[*source].network_port, clock_source_nomination_config[*source].clk_in_port, &iport);

        if (clock_source_nomination_config[*source].nominated && (iport == port))
        /* This 'port' is a nominated port */
            return (true);
    }
    return (false);
}

void synce_set_clock_source_nomination_config_to_default(vtss_appl_synce_clock_source_nomination_config_t *config)
{
    uint   i;
    vtss_ifindex_t ifindex;

    // Convert uport index 1 (with usid = 1) to vtss_ifindex_t type
    (void) vtss_ifindex_from_usid_uport(1, 1, &ifindex);

    for (i=0; i<synce_my_nominated_max; ++i) {
        config[i].nominated = false;
        if ((i < synce_my_nominated_max-1) || !clock_zarlink()) {
            config[i].network_port = ifindex;  // vtss_ifindex_t value of port 0
        }
        else {
            config[i].network_port = VTSS_IFINDEX_NONE;        // SYNCE_STATION_CLOCK_PORT;
        }
        config[i].clk_in_port = 0;          // At present, clk_in_port is always 0 (or >= 128 for PTP sources) as only one station clock input is supported.
        config[i].priority = 0;
        config[i].aneg_mode = VTSS_APPL_SYNCE_ANEG_NONE;
        config[i].holdoff_time = 0;
        config[i].ssm_overwrite = VTSS_APPL_SYNCE_QL_NONE;
    }
}

void synce_mgmt_set_clock_source_nomination_config_to_default(vtss_appl_synce_clock_source_nomination_config_t *config)
{
    SYNCE_CRIT_ENTER();
    synce_set_clock_source_nomination_config_to_default(config);
    SYNCE_CRIT_EXIT();
}
void synce_get_clock_selection_mode_config_default(vtss_appl_synce_clock_selection_mode_config_t *config)
{
    config->selection_mode = VTSS_APPL_SYNCE_SELECTOR_MODE_AUTOMATIC_REVERTIVE;
    config->source = 1;
    config->wtr_time = 5;  /* Default 5 min.*/
    config->ssm_holdover = VTSS_APPL_SYNCE_QL_NONE;
    config->ssm_freerun = VTSS_APPL_SYNCE_QL_NONE;
    config->eec_option = VTSS_APPL_SYNCE_EEC_OPTION_1;
}

void synce_set_clock_selection_mode_config_to_default(vtss_appl_synce_clock_selection_mode_config_t *config)
{
    synce_get_clock_selection_mode_config_default(config);
}

void synce_set_station_clock_config_to_default(vtss_appl_synce_station_clock_config_t *config)
{
    config->station_clk_in  = VTSS_APPL_SYNCE_STATION_CLK_DIS;
    config->station_clk_out = VTSS_APPL_SYNCE_STATION_CLK_DIS;
}

void synce_mgmt_set_station_clock_config_to_default(vtss_appl_synce_station_clock_config_t *config)
{
    SYNCE_CRIT_ENTER();
    synce_set_station_clock_config_to_default(config);
    SYNCE_CRIT_EXIT();
}

static void synce_set_port_config_to_default(vtss_appl_synce_port_config_t *config)
{
    uint   i;

    for (i=0; i<SYNCE_PORT_COUNT; i++) {
        config[i].ssm_enabled = false;
    }
}

void synce_mgmt_set_port_config_to_default(vtss_appl_synce_port_config_t *config)
{
    SYNCE_CRIT_ENTER();
    synce_set_port_config_to_default(config);
    SYNCE_CRIT_EXIT();
}

static void apply_configuration(conf_blk_t *blk)
{
    uint  i;
    uint  rc=SYNCE_RC_OK;

    for (i = 0; i < synce_my_nominated_max; ++i)
        rc += vtss_appl_synce_clock_source_nomination_config_set((i + 1), &blk->clock_source_nomination_config[i]);

    SYNCE_CRIT_ENTER();
    clock_selection_mode_config = blk->clock_selection_mode_config;
    SYNCE_CRIT_EXIT();
    rc += vtss_appl_synce_clock_selection_mode_config_set(&clock_selection_mode_config);

    rc += synce_mgmt_station_clock_out_set(blk->station_clock_config.station_clk_out);
    rc += synce_mgmt_station_clock_in_set(blk->station_clock_config.station_clk_in);

    for (i = 0; i < SYNCE_PORT_COUNT; ++i)
        rc += synce_mgmt_ssm_set(i + VTSS_PORT_NO_START, blk->port_config[i].ssm_enabled);

    if (rc != SYNCE_RC_OK)      T_D("error returned");
}

static uint overwrite_conv(vtss_appl_synce_quality_level_t overwrite)
{
    switch (overwrite)
    {
        case VTSS_APPL_SYNCE_QL_NONE: return SSM_QL_PROV;   //This is not standard
        case VTSS_APPL_SYNCE_QL_PRC:  return SSM_QL_PRC;
        case VTSS_APPL_SYNCE_QL_SSUA: return SSM_QL_SSUA_TNC;
        case VTSS_APPL_SYNCE_QL_SSUB: return SSM_QL_SSUB;
        case VTSS_APPL_SYNCE_QL_EEC2: return SSM_QL_EEC2;
        case VTSS_APPL_SYNCE_QL_EEC1: return SSM_QL_EEC1;
        case VTSS_APPL_SYNCE_QL_DNU:  return SSM_QL_DNU_DUS;
        case VTSS_APPL_SYNCE_QL_INV:  return SSM_QL_PRS_INV;    // for test only
        case VTSS_APPL_SYNCE_QL_PRS:  return SSM_QL_PRS_INV;
        case VTSS_APPL_SYNCE_QL_STU:  return SSM_QL_STU;
        case VTSS_APPL_SYNCE_QL_ST2:  return SSM_QL_ST2;
        case VTSS_APPL_SYNCE_QL_TNC:  return SSM_QL_SSUA_TNC;
        case VTSS_APPL_SYNCE_QL_ST3E: return SSM_QL_ST3E;
        case VTSS_APPL_SYNCE_QL_SMC:  return SSM_QL_SMC;
        case VTSS_APPL_SYNCE_QL_PROV: return SSM_QL_PROV;
        case VTSS_APPL_SYNCE_QL_DUS:  return SSM_QL_DNU_DUS;
        default:  return (SSM_QL_DNU_DUS);
    }
}

static uint get_ssm_rx(uint port, uint source)
{
    if ((port_config[port].ssm_enabled) || (port >= SYNCE_PORT_COUNT))
        return (clock_source_nomination_config[source].ssm_overwrite != VTSS_APPL_SYNCE_QL_NONE) ? overwrite_conv(clock_source_nomination_config[source].ssm_overwrite) : port_state[port].ssm_rx;
    else
        return (overwrite_conv(clock_source_nomination_config[source].ssm_overwrite));
}



static void set_wtr(uint source)
{
    uint wtr_state;
    u32 port;

    if ((clock_selection_mode_config.wtr_time != 0) && clock_source_nomination_config[source].nominated)
    {
    /* WTR enabled  on a nominated source */
        wtr_state = clock_state[source].wtr_state;

        (void) synce_network_port_clk_in_port_combo_to_port(clock_source_nomination_config[source].network_port, clock_source_nomination_config[source].clk_in_port, &port);

        if (!port_state[port].first_port_callback) return;

//T_D("link %d  fos %d  ssm_state %d", port_state[port].link, clock_alarm_state.fos[source], port_state[port].ssm_state);

        if (clock_alarm_state.fos[source])
            clock_state[source].wtr_state |= WTR_FOS;
        else
            clock_state[source].wtr_state &= ~WTR_FOS;


        if (port_state[port].link)
            clock_state[source].wtr_state &= ~WTR_LINK;
        else
            clock_state[source].wtr_state |= WTR_LINK;

        if ((port_config[port].ssm_enabled || (port >= SYNCE_PORT_COUNT)) && (clock_source_nomination_config[source].ssm_overwrite == VTSS_APPL_SYNCE_QL_NONE) && (port_state[port].ssm_state & SSM_FAIL))
            clock_state[source].wtr_state |= WTR_SSM_FAIL;
        else
            clock_state[source].wtr_state &= ~WTR_SSM_FAIL;

        if ((port_config[port].ssm_enabled || (port >= SYNCE_PORT_COUNT)) && (clock_source_nomination_config[source].ssm_overwrite == VTSS_APPL_SYNCE_QL_NONE) && (port_state[port].ssm_state & SSM_INVALID))
            clock_state[source].wtr_state |= WTR_SSM_INV;
        else
            clock_state[source].wtr_state &= ~WTR_SSM_INV;

        if (clock_state[source].wtr_state != WTR_OK)
        {
            clock_alarm_state.wtr[source] = false;
            clock_state[source].wtr_timer = 0;
        }
        else
        {
            if (wtr_state != WTR_OK) {
                clock_alarm_state.wtr[source] = true;
                clock_state[source].wtr_timer = clock_selection_mode_config.wtr_time * 60;  // Representation in clock_state is in seconds
                T_I("WTR start timer source %d, value %d sec", source, clock_state[source].wtr_timer);
                T_I("WTR old wtr_state 0x%x , new wtr_state 0x%x", wtr_state, clock_state[source].wtr_state);
            }
        }
    }
    else
    {
        clock_state[source].wtr_state = WTR_OK;
        clock_state[source].wtr_timer = 0;
        clock_alarm_state.wtr[source] = false;
    }
    T_D("source %d  wtr_time %d  wtr_state %d  WTR %d", source, clock_selection_mode_config.wtr_time, clock_state[source].wtr_state, clock_alarm_state.wtr[source]);
}


static bool get_source_disable(uint port, uint source)
{
    T_N("port %d, source %d", port, source);
    if ((get_ssm_rx(port, source) == SSM_QL_DNU_DUS) ||         /* DNU is received */
        ((port_state[port].ssm_state & (SSM_FAIL | SSM_INVALID)) && (clock_source_nomination_config[source].ssm_overwrite == VTSS_APPL_SYNCE_QL_NONE)))     /* SSM is failing and there is no SSM owerwrite */
        return (true);

    if ((clock_selection_mode_config.wtr_time != 0) && (clock_alarm_state.wtr[source] || (clock_state[source].wtr_state != WTR_OK)))     /* WTR is enabled and this source is in WTR state */
        return (true);

    if ((port < SYNCE_STATION_CLOCK_PORT) && !port_state[port].phy && !port_state[port].link) /* Link is down and port is NOT a PHY meaning SERDES - need to disable in order to create LOCS */
        return true;
        
    //if (clock_alarm_state.locs[source]) return true;

    return(false);
}

/*
 * set_clock_source:
 * If a clock source is not nominated or if the priority is 'disabled', the recovered clock signal is squelched.
 * This is because the Silabs 5326 clock input cannot be disabled, i.e. if the internal oscillator quality is better then the clock souede quality,
 * and we therefore want to use the internal clock, we squelch the clock source in order to make the si5326 select freerun ot holdover state.
 */
static void set_clock_source(uint source)
{
    mesa_rc                rc;
    mesa_phy_recov_clk_t   phy_clk_port;
    mesa_synce_clock_in_t  clk_in;
    mesa_synce_clk_port_t  switch_clk_port = 0;  // Compiler was generating a warning that switch_clk_port may be used un-initialized in a call to mesa_synce_clock_in_set below.
                                                 // The waning was false since to get to the call MESA_CAP_SYNCE had to be true and in that case switch_clk_port would be set before the call
                                                 // or this function would return without calling mesa_synce_clock_in_set. We set switch_clk_port = 0 to get rid of the warning.
    u32                    port;

    if (source == STATION_CLOCK_SOURCE_NO) return;  /* station clock is neither set up in the Switch nor in the PHY */
    /* This port number is actually allso identifying the PHY - if relevant. */
    /* Requirement is that this function is only called with not nominated source if it previously has been nominated */
    (void) synce_network_port_clk_in_port_combo_to_port(clock_source_nomination_config[source].network_port, clock_source_nomination_config[source].clk_in_port, &port);

    if (port < SYNCE_PORT_COUNT) {
        phy_clk_port = synce_get_phy_recovered_clock(source, port);
    }
    else {  // PTP 8265.1 ports
        if (source == 1) {
            phy_clk_port = MESA_PHY_RECOV_CLK2;
        }
        else {
            phy_clk_port = MESA_PHY_RECOV_CLK1;
        }
    }

    if (MESA_CAP(MESA_CAP_SYNCE)) {
        if (source  < SYNCE_NOMINATED_MAX && port < SYNCE_PORT_COUNT) { 
            switch_clk_port = synce_get_switch_recovered_clock(source, port);
        }
#if defined(VTSS_SW_OPTION_PTP)
        else if ((source >= SYNCE_NOMINATED_MAX - 1) || (port >= SYNCE_PORT_COUNT + PTP_CLOCK_INSTANCES)) {  // PTP 8265.1 ports exist for all but the last source associated with the station clock
            T_WG(TRACE_GRP_API, "invalid clock source %u or port %d", source, port);
            return;
        }
        else {
            switch_clk_port = synce_get_switch_recovered_clock(source, 0);  // In case of PTP clocks, assume port is 0 (also done below). The input is going to be squelshed anyway.
        }
#else
        else {
            T_WG(TRACE_GRP_API, "invalid clock source %u or port %d", source, port);
            return;
        }
#endif
        clk_in.enable = false;
        clk_in.port_no = port;
        clk_in.squelsh = SWITCH_AUTO_SQUELCH;
        if (MESA_CAP(MESA_CAP_SYNCE_IN_TYPE)) {
            if (port != SYNCE_STATION_CLOCK_PORT) {
                clk_in.clk_in = MESA_SYNCE_CLOCK_INTERFACE;
            } else {
                clk_in.clk_in = MESA_SYNCE_CLOCK_STATION_CLK;
                clk_in.port_no = 0; // only support for one station clock input with index 0
            }
        }
        T_IG(TRACE_GRP_API, "clock source %u for port %d, clk_in %d", source, port, clk_in.clk_in);
    }

    if (port >= SYNCE_PORT_COUNT) {  // PTP 8265.1 port. Squelsh physical input to make clock selector prefer internal clock
        clk_in.port_no = 0;          // clk_in.port_no needs to be set to a port that is legal in combination with the value of phy_clk_port
        if (MESA_CAP(MESA_CAP_SYNCE)) {
            T_DG(TRACE_GRP_API, "mesa_synce_clock_in_set  clk_port %u  enable %u  port_no %u", switch_clk_port, clk_in.enable, clk_in.port_no);
            if ((rc = mesa_synce_clock_in_set(NULL,  switch_clk_port,  &clk_in)) != VTSS_OK)    T_D("error returned port %u  rc %u", port, rc);
        }
    }
    else {
        if (port_state[port].phy) {  /* Port is a PHY */
            
            /* Internal PHYs are represented in the graph by means of a "virtual" board mux. We can use this to determine if PHY is an internal PHY. */
            u32 input = synce_get_mux_selector_w_attr(source, port);
            
            /* If PHY is an internal PHY or if PHYs recovered clock output is routed trough the switch, make sure clock output of switch is enabled and divider correctly configured */
            if ((input & MESA_SYNCE_TRI_STATE_FROM_PHY) || switch_clk_port != -1) {
                mesa_synce_clock_out_t clk_out;
    
                clk_out.enable = true;     // Enable for clock output from switch
                clk_out.divider = synce_get_switch_clock_divider(source, port, port_state[port].speed);
                if (mesa_synce_clock_out_set(NULL, switch_clk_port != -1 ? switch_clk_port : phy_clk_port, &clk_out) != VTSS_OK)  T_D("mesa_synce_clock_out_set returned error");
            }

            /* set up the multiplexer in the CPLD in cases where applicable */
            synce_mux_set(source, port);

            /* calculate the clock port */
            if (!clock_source_nomination_config[source].nominated || prio_assigned[source] == synce_my_prio_disabled) {
                /* disable clock out from phy if not nominated or not prioritized */
                phy_clock_config[source].src = MESA_PHY_CLK_DISABLED;
            } else {
                /* A PHY port is norminated */
                if (MESA_CAP(MESA_CAP_SYNCE_ALWAYS_USE_COPPER_MEDIA_CLK)) {
                    // in ServalT always enable copper media recovered clock
                    phy_clock_config[source].src = MESA_PHY_COPPER_MEDIA;
                } else {
                    if (port_state[port].fiber) phy_clock_config[source].src = MESA_PHY_SERDES_MEDIA;
                    else                        phy_clock_config[source].src = MESA_PHY_COPPER_MEDIA;
                }

                if (get_source_disable(port, source))
                    phy_clock_config[source].src = MESA_PHY_CLK_DISABLED;
            }
            
            /* Get DPLL input frequency from graph and setup the DPLL */
            clock_frequency_t dpll_input_frequency = synce_get_dpll_input_frequency(source, port, port_state[port].speed);
            u32 dpll_input_frequency_khz = 125000;
            switch (dpll_input_frequency) {
                case CLOCK_FREQ_25MHZ:
                    dpll_input_frequency_khz = 25000;
                    break;
                case CLOCK_FREQ_125MHZ:
                    dpll_input_frequency_khz = 125000;
                    break;
                default:
                    dpll_input_frequency_khz = 125000;
                    T_I("Unsupported frequency %d", (int) dpll_input_frequency);
            }

            if (clock_ref_clk_in_freq_set(source, dpll_input_frequency_khz) != MESA_RC_OK) {
                // currently only supported in zls30363, ther error return can be ignored in zls30343
                T_I("Could not set dpll input frequency.");
            }

            /* Configure clock selection in PHY */
            clock_frequency_t phy_output_frequency = synce_get_phy_output_frequency(source, port, port_state[port].speed);
            switch (phy_output_frequency) {
                case CLOCK_FREQ_25MHZ:
                    phy_clock_config[source].freq = MESA_PHY_FREQ_25M;
                    break;
                case CLOCK_FREQ_125MHZ:
                    phy_clock_config[source].freq = MESA_PHY_FREQ_125M;
                    break;
                default:
                    phy_clock_config[source].freq = MESA_PHY_FREQ_125M;
                    T_I("Unsupported frequency %d", (int) phy_output_frequency);
            }

            T_W("source = %d, port = %d, src = %d, freq %d, squelch %d, phy output freq %d", source, port, phy_clock_config[port].src, phy_clock_config[port].freq, phy_clock_config[port].squelch, (int) phy_output_frequency);
            if (port_state[port].speed == MESA_SPEED_10G) {
                T_IG(TRACE_GRP_API, "10G phy port %d nominated", port);
            } else {
                if ((rc = mesa_phy_clock_conf_set(PHY_INST, port, phy_clk_port, &phy_clock_config[source])) != VTSS_OK)    T_D("error returned port %u  rc %u", port, rc);
            }

            if (MESA_CAP(MESA_CAP_SYNCE_USE_SWITCH_SELECTOR_WITH_PHY)) {
                if (phy_clock_config[source].src != MESA_PHY_CLK_DISABLED) {
                    clk_in.enable = true;
                }
                /* In servalT the switch selector is also set up when a PHY port is selected */
                T_DG(TRACE_GRP_API, "mesa_synce_clock_in_set  clk_port %u  enable %u  port_no %u", switch_clk_port, clk_in.enable, clk_in.port_no);
                if ((rc = mesa_synce_clock_in_set(NULL,  switch_clk_port,  &clk_in)) != VTSS_OK)    T_D("error returned port %u  rc %u", port, rc);
//                if ((rc = clock_priority_set(source, clk_in.enable ? prio_assigned[source] : synce_my_prio_disabled)) != VTSS_OK)    T_D("error returned source %u  rc %u", source, rc);
            }

        } else if (MESA_CAP(MESA_CAP_SYNCE)) {
            /* Port is NOT a PHY meaning it is a SERDES */

            /* Get DPLL input frequency from graph and setup the DPLL */
            clock_frequency_t dpll_input_frequency = synce_get_dpll_input_frequency(source, port, port_state[port].speed);
            T_W("source = %d, port = %d, speed = %d, dpll input frequency %d", source, port, port_state[port].speed, (int) dpll_input_frequency);

            /* Configure clock selection in PHY */
            u32 dpll_input_frequency_khz = 125000;
            switch (dpll_input_frequency) {
                case CLOCK_FREQ_25MHZ:
                    dpll_input_frequency_khz = 25000;
                    break;
                case CLOCK_FREQ_31_25MHZ:
                     dpll_input_frequency_khz = 31250;
                     break;
                case CLOCK_FREQ_32_226MHZ:
                     dpll_input_frequency_khz = 32226;
                     break;
                case CLOCK_FREQ_80_565MHZ:
                     dpll_input_frequency_khz = 80565;
                     break;
                case CLOCK_FREQ_125MHZ:
                     dpll_input_frequency_khz = 125000;
                     break;
                case CLOCK_FREQ_156_25MHZ:
                     dpll_input_frequency_khz = 156250;
                     break;
                case CLOCK_FREQ_161_13MHZ:
                     dpll_input_frequency_khz = 161130;
                     break;
                default:
                    T_E("Unsupported frequency %d", (int) dpll_input_frequency);
            }

            if (clock_ref_clk_in_freq_set(source, dpll_input_frequency_khz) != MESA_RC_OK) {
                T_W("Could not set dpll input frequency Khz %d.", dpll_input_frequency_khz);
            }
            
            /* Make sure clock output of switch is enabled and divider correctly configured */
            {
                mesa_synce_clock_out_t clk_out;
    
                clk_out.enable = true;     // Enable for clock output from switch
                clk_out.divider = synce_get_switch_clock_divider(source, port, port_state[port].speed);
                if (mesa_synce_clock_out_set(NULL, switch_clk_port, &clk_out) != VTSS_OK)  T_D("mesa_synce_clock_out_set returned error");
            }

            /* set up the multiplexer in the CPLD in cases where applicable */
            synce_mux_set(source, port);

            /* disable clock out from SERDES if not nominated or not prioritized */
            if (!clock_source_nomination_config[source].nominated  || prio_assigned[source] == synce_my_prio_disabled) {
                clk_in.enable = false;
            } else {
                clk_in.enable = get_source_disable(port, source) ? false : true;
            }

            if (port_state[port].fiber && port_state[port].speed == MESA_SPEED_100M) {
                clk_in.squelsh = false;  //squelch does not work on 100Mbit fiber ports
            } else {
                clk_in.squelsh = SWITCH_AUTO_SQUELCH;
            }
            /* Configure clock selection in SWITCH */
            T_DG(TRACE_GRP_API, "mesa_synce_clock_in_set  clk_port %u  enable %u, squelch %d, port_no %u", switch_clk_port, clk_in.enable, clk_in.squelsh, clk_in.port_no);
            if ((rc = mesa_synce_clock_in_set(NULL,  switch_clk_port,  &clk_in)) != VTSS_OK) T_D("error returned port %u  rc %u", port, rc);
        }
    }
}

// this function tekes care of the fact that the PTP Servo may periodically take HW control in hybrid mode, and in this situation we want
// to report locked state even if the DPLL toggles between Locked, FreeRun and Acquiring Lock.
// the function is called once pr. sec, and starts a timer when the state changes from Locked to Freerun, and the hybrid mode is selected.
// when the timer is active, the Locked state is reported no matter what.
#if defined(VTSS_SW_OPTION_PTP)
static uint saved_clock_input = 0;
static vtss_appl_synce_selector_state_t saved_selector_state = VTSS_APPL_SYNCE_SELECTOR_STATE_FREERUN;
static int saved_timer = 0;
static BOOL my_hybrid_mode = FALSE;
#endif
#define SAVED_TIMER_START 10  // it take up to 10 sec to get in lock after dco mode
static mesa_rc clock_selector_monitor_state(void)
{
    mesa_rc rc = VTSS_RC_OK;
#if defined(VTSS_SW_OPTION_PTP)
    uint actual_clock_input;
    vtss_appl_synce_selector_state_t actual_selector_state;
    rc = clock_selector_state_get(&actual_clock_input, &actual_selector_state);
    if (saved_timer == 0) {
        if (my_hybrid_mode && saved_selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_LOCKED && actual_selector_state != VTSS_APPL_SYNCE_SELECTOR_STATE_LOCKED) {
            saved_timer = SAVED_TIMER_START;
            T_I("Start Timer: clock_input %d, selector_state %d", actual_clock_input, actual_selector_state);
        } else {
            saved_clock_input = actual_clock_input;
            saved_selector_state = actual_selector_state;
        }
        T_D("clock_input %d, selector_state %d",actual_clock_input, actual_selector_state);
    } else {
        saved_timer--;
        if (saved_timer == 0) {
            saved_clock_input = actual_clock_input;
            saved_selector_state = actual_selector_state;
            T_I("Timeout: clock_input %d, selector_state %d",actual_clock_input, actual_selector_state);
        } else  if (actual_selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_LOCKED) {
            saved_timer = 0;
            saved_clock_input = actual_clock_input;
            saved_selector_state = actual_selector_state;
            T_I("Locked entered: clock_input %d, selector_state %d",actual_clock_input, actual_selector_state);
        }
    }
#endif //defined(VTSS_SW_OPTION_PTP)
    return rc;
}

static mesa_rc clock_selector_compensated_state_get(uint *const clock_input, vtss_appl_synce_selector_state_t *const selector_state)
{
    mesa_rc rc = VTSS_RC_OK;
#if defined(VTSS_SW_OPTION_PTP)
    *clock_input = saved_clock_input;
    *selector_state = saved_selector_state;
#else
    rc = clock_selector_state_get(clock_input, selector_state);
#endif
    return rc;
}

static void configure_master_slave(void)
{
    uint                              best_prio, clock_input, i, priority;
    u32                               port;
    mesa_phy_status_1g_t              phy_status;
    mesa_phy_conf_1g_t                phy_setup;
    bool                              found, master;
    vtss_appl_synce_selector_state_t  selector_state;

    /* get clculated selected clock source */
    clock_input = 0;
    best_prio = synce_my_priority_max;
    found = false;

    if ((clock_selection_mode_config.selection_mode == VTSS_APPL_SYNCE_SELECTOR_MODE_FORCED_HOLDOVER) || (clock_selection_mode_config.selection_mode == VTSS_APPL_SYNCE_SELECTOR_MODE_FORCED_FREE_RUN))
        return;
    else
    if (clock_selection_mode_config.selection_mode == VTSS_APPL_SYNCE_SELECTOR_MODE_MANUEL) {
        clock_input = clock_selection_mode_config.source;
        found = true;
    }
    else
    if (clock_selection_mode_config.selection_mode == VTSS_APPL_SYNCE_SELECTOR_MODE_AUTOMATIC_NONREVERTIVE) {
        if (clock_selector_compensated_state_get(&clock_input, &selector_state) != VTSS_OK) T_D("clock_selector_compensated_state_get failed");
        if (selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_LOCKED)
            found = true;
    }
    else {  /* This is automatic revertive - find the best clock to be selected */
        for (i=0; i<synce_my_nominated_max && i != STATION_CLOCK_SOURCE_NO; ++i) {
            if (clock_source_nomination_config[i].nominated) {
                (void) synce_network_port_clk_in_port_combo_to_port(clock_source_nomination_config[i].network_port, clock_source_nomination_config[i].clk_in_port, &port);    /* Port number of this clock input */
                if (clock_priority_get(i, &priority) != VTSS_OK)    T_D("Call to clock_priority_get source %u failed", i);
                if ((priority < best_prio) && !(port_state[port].ssm_state & SSM_NOT_SEEN) && (!get_source_disable(port, i) || clock_state[i].holdoff)) {
                    best_prio = priority;
                    clock_input = i;
                    found = true;
                }
            }
        }
    }
    T_D("best_prio %u clock_input = %u, found %u", best_prio, clock_input, found);

    if (found)
    {
    /* A clock source is calculated to be the selected input */
        if (clock_source_nomination_config[clock_input].aneg_mode != VTSS_APPL_SYNCE_ANEG_FORCED_SLAVE) {
            clock_source_nomination_config[clock_input].aneg_mode = VTSS_APPL_SYNCE_ANEG_PREFERED_SLAVE;  /* The selected source is preferred slave */
        }
    }

    T_D("clock_input = %u, aneg_mode %u", clock_input, clock_source_nomination_config[clock_input].aneg_mode);
    for (i=0; i<synce_my_nominated_max; ++i) { /* Check for ANEG */
        // Earlier the ANEG mode was only set if (clock_source_nomination_config[i].aneg_mode != VTSS_APPL_SYNCE_ANEG_NONE) this had the side effect that when the link came up in master mode, then the link went down and up after WTR timeout or clear, and therefore the WTR timer was reatarted, therefore this is cnanged. */
        if (clock_source_nomination_config[i].nominated) { /* Some ANEG is requested, i.e. if the port is nominated, then the preferred mode is slave */
            (void) synce_network_port_clk_in_port_combo_to_port(clock_source_nomination_config[i].network_port, clock_source_nomination_config[i].clk_in_port, &port);    /* Port number of this clock input */
            T_D("ssm_rx %u ssm_tx = %d", get_ssm_rx(port, i), port_state[port].ssm_tx);
            if ((get_ssm_rx(port, i) <= port_state[port].ssm_tx) &&
                    found == true && clock_input == i && best_prio == 0) { /* well only do the ANEG if the ssm_rx is <= ssm_tx and the port is the preferred selection, otherwise the ANEG will oscillate, is both ends of a link is nominated */
                (void) synce_network_port_clk_in_port_combo_to_port(clock_source_nomination_config[i].network_port, clock_source_nomination_config[i].clk_in_port, &port);    /* Port number of this clock input */
                if (!(port_state[port].prefer_timer) && port_state[port].phy && (port_state[port].speed == MESA_SPEED_1G)) { /* ANEG is allowed - not already active and correct port type */
                    master = (clock_source_nomination_config[i].aneg_mode == VTSS_APPL_SYNCE_ANEG_PREFERED_MASTER);
                    if (mesa_phy_status_1g_get(PHY_INST, port, &phy_status) != VTSS_OK)    T_D("mesa_phy_status_1g_get port %u failed", port);
                    T_D("get phy conf for port %u phy_setup_master %u, master %u", port, master, phy_status.master);

                    if (phy_status.master != master) {  /* This port has different aneg mode than requested */
                        phy_setup.master.cfg = true;
                        phy_setup.master.val = master;
                        if (mesa_phy_conf_1g_set(PHY_INST, port, &phy_setup) != VTSS_OK)    T_D("mesa_phy_conf_1g_set port %u failed", port);
                        port_state[port].prefer_timer = 1;  /* Activate prefer time out */
                        T_I("set phy conf for port %u master = %d", port, master);
                    }
                }
            }
            if (clock_source_nomination_config[i].aneg_mode != VTSS_APPL_SYNCE_ANEG_FORCED_SLAVE)  /* Forced slave mode cannot be cleared - unless from management */
                clock_source_nomination_config[i].aneg_mode = VTSS_APPL_SYNCE_ANEG_NONE;
        }
    }
}

static uint get_internal_clock_ql(void)
{
    uint ssm;
    vtss_appl_synce_selector_state_t  selector_state;
    uint                              clock_input;
    if (clock_selector_compensated_state_get(&clock_input, &selector_state) != VTSS_OK) T_D("error returned");

    switch (selector_state)
    {
        case VTSS_APPL_SYNCE_SELECTOR_STATE_LOCKED:
        case VTSS_APPL_SYNCE_SELECTOR_STATE_ACQUIRING:
        case VTSS_APPL_SYNCE_SELECTOR_STATE_HOLDOVER:
            ssm = overwrite_conv(clock_selection_mode_config.ssm_holdover);
            break;
        case VTSS_APPL_SYNCE_SELECTOR_STATE_FREERUN:
        case VTSS_APPL_SYNCE_SELECTOR_STATE_PTP:
        case VTSS_APPL_SYNCE_SELECTOR_STATE_REF_FAILED:
            ssm = overwrite_conv(clock_selection_mode_config.ssm_freerun);
            break;
        default:
            ssm = SSM_QL_DNU_DUS;;
    }
    return ssm;
}

/*
 * set clock priority
 * Assign priority to the nominated sources and internal oscillator:
 * The nominated source with the best QL is assigned highest priority, and so on.
 * If the quality of a nominated source is less than or equal to the internal oscillator, the source priority is 'disabled'
 * All potential source are assigned a priority. I.e. the non nominated sources are assigned priorities lower than the nominateds.
 * The result is that a source will not be selected if the quality is less than or equal to the internal quality.

 */

static void set_clock_priority(void)
{
    uint ssm, ssm_rx, prio, i, prio_count = 0;
    u32 port;
    uint internal_oscillator_ssm = get_internal_clock_ql();
    uint internal_oscillator_pri = synce_my_prio_disabled;
    for (i=0; i<synce_my_nominated_max; ++i)    prio_assigned[i] = synce_my_prio_disabled;

    /* Check all qualities */
    for (ssm = 0; (ssm <= SSM_QL_DNU_DUS) && (internal_oscillator_pri == synce_my_prio_disabled); ++ssm)
    {
        if (clock_selection_mode_config.eec_option == VTSS_APPL_SYNCE_EEC_OPTION_1) {
            if (!((ssm == SSM_QL_PRC) || (ssm == SSM_QL_SSUA_TNC) || (ssm == SSM_QL_SSUB) || (ssm == SSM_QL_EEC1) || (ssm == SSM_QL_DNU_DUS) || (ssm == SSM_QL_PROV)))
                continue;
        }
        else if (clock_selection_mode_config.eec_option == VTSS_APPL_SYNCE_EEC_OPTION_2) {
            if (!((ssm == SSM_QL_PRS_INV) || (ssm == SSM_QL_STU) || (ssm == SSM_QL_ST2) || (ssm == SSM_QL_SSUA_TNC) || (ssm == SSM_QL_ST3E) || (ssm == SSM_QL_EEC2) ||
                  (ssm == SSM_QL_SMC) || (ssm == SSM_QL_PROV) || (ssm == SSM_QL_DNU_DUS)))
                continue;
        }
        else continue;

        if (internal_oscillator_ssm == ssm) {
            if (prio_count == 0) {
                // The clock with the highest priority is the internal clock. Set best_master in offset filter/servo to -1 so that the servo will not try to control the DLL
#if defined(VTSS_SW_OPTION_PTP)
                T_D("Best master set to -1");
                best_master = -1;
#endif
                synce_mgmt_clock_adjtimer_set(0);  // Reset the DCO offset to 0
                T_I("No external clock available for Sync");

                // Update the clock selection mode
                {
                    vtss_appl_synce_selection_mode_t mode;

                    if (clock_selection_mode_get(&mode) == VTSS_RC_OK) {
                        if (mode != clock_selection_mode_config.selection_mode) {
                            (void) clock_selection_mode_set(clock_selection_mode_config.selection_mode, clock_selection_mode_config.source);
                        }
                    }
                }
            }
            internal_oscillator_pri = prio_count++;
            continue;
        }

        /* check all priorities */
        for (prio = 0; (prio < synce_my_nominated_max) && (internal_oscillator_pri == synce_my_prio_disabled); ++prio)
        {
            /* check all clock sources */
            for (i = 0; (i < synce_my_nominated_max) && (internal_oscillator_pri == synce_my_prio_disabled); ++i)
            {
                /* a nominated clock */
                if (clock_source_nomination_config[i].nominated) { // This clock source is nominated
                    (void) synce_network_port_clk_in_port_combo_to_port(clock_source_nomination_config[i].network_port, clock_source_nomination_config[i].clk_in_port, &port);  // Port number of this clock input
                    // T_D("TEST_TEST: %d %d %d %d", i, clock_source_nomination_config[i].network_port, clock_source_nomination_config[i].clk_in_port, port);
                    ssm_rx = get_ssm_rx(port, i);  // Check if 'rx_ssm' must be overwritten
                    T_IG(TRACE_GRP_CLOCK, "ssm %d, port %d, rx_ssm %d, prio %u, source %d", ssm, port, ssm_rx, prio, i);

#if defined(VTSS_SW_OPTION_PTP)
                    if (port >= SYNCE_PORT_COUNT) {  // This source is using PTP 8265.1
                        if (!get_source_disable(port, i)) {
                            if ((ssm_rx == ssm) && (clock_source_nomination_config[i].priority == prio)) {  // This clock source is receiving SSM that match and got priority match
                                if (prio_count == 0) {  // This source has the highest priority. NOTE: A PTP 8265.1 source can only be selected if it has the highest priority of all sources.
                                    uint clock_input;
                                    vtss_appl_synce_selector_state_t selector_state;
                                    if (clock_selector_compensated_state_get(&clock_input, &selector_state) != VTSS_OK) T_E("error returned");  // Check which source is currently selected and if it is locked

                                    if ((port_state[port].ptsf == SYNCE_PTSF_NONE) ||  // If source is not locked then only select it if no other source that is also nominated is already selected and locked.
                                        ((port_state[port].ptsf <= SYNCE_PTSF_UNUSABLE) &&  // Do not select a PTP source if it has "loss of sync" or "loss of announce"
                                        !(((selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_LOCKED) && (clock_input < SYNCE_NOMINATED_MAX) && (clock_source_nomination_config[clock_input].nominated) && (clock_source_nomination_config[clock_input].network_port != 0)) ||
                                         (((selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_FREERUN) || (selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_HOLDOVER)) && (best_master >= 0) && (best_master + SYNCE_PORT_COUNT != port) && (port_state[best_master + SYNCE_PORT_COUNT].ptsf == SYNCE_PTSF_NONE)))))
                                    {
                                        best_master = port - SYNCE_PORT_COUNT;
                                        
                                        T_D("Best master set to %d", port - SYNCE_PORT_COUNT);
                                        best_master_source = i;

                                        // If mode is not NCO, we need to select NCO mode but also get the current holdover value so that it can be used for setting a base offset.
                                        uint clock_input;
                                        vtss_appl_synce_selector_state_t selector_state;
                                        (void) clock_selector_compensated_state_get(&clock_input, &selector_state);
                                        T_D("Before");
                                        if (selector_state != VTSS_APPL_SYNCE_SELECTOR_STATE_PTP) {
                                        }
                                        T_D("After (selector_state was %d)", selector_state);
                                    }
                                    else continue;  // Disregard this PTP 8265.1 source
                                    internal_oscillator_pri = prio_count++;  /* Set internal_oscillator_pri != synce_my_prio_disabled to finish the assignment of priorities */
                                }
                            }
                        }
                    }
                    else {  // This source is using a normal input port. 
#else
                    {  // This source is using a normal input port. 
#endif
//                        if (!get_source_disable(port, i)) {
                        if ((ssm_rx == ssm) && (clock_source_nomination_config[i].priority == prio)) {  // This clock source is receiving SSM that match and got priority match
#if defined(VTSS_SW_OPTION_PTP)
                            //vtss_ptp_servo_set_best_master(-1);  // Set best_master in offset filter/servo to -1 so that no PTP servo will not try to control the DCO.
                            best_master = -1;
                            T_D("Best master set to -1");
#endif
                            synce_mgmt_clock_adjtimer_set(0);    // Reset the DCO offset to 0
                            T_I("Synce takes the DPLL");
                            prio_assigned[i] = prio_count++;

                            // Check that the configured clock selection mode is in sync with the DPLL driver
                            {
                                vtss_appl_synce_selection_mode_t mode;

                                if (clock_selection_mode_get(&mode) == VTSS_RC_OK) {
                                    if (mode != clock_selection_mode_config.selection_mode) {
                                        T_I("prio %d, selection_mode %d, source %d", prio, clock_selection_mode_config.selection_mode, clock_selection_mode_config.source);
                                        // Note: If actual clock selection mode is different from the configured it has to be reset to the configured value.
                                        //       This can happen if it was previously forced to "Holdover" because an 8265.1 slave was selected as the best source.
                                        (void) clock_selection_mode_set(clock_selection_mode_config.selection_mode, clock_selection_mode_config.source);
                                    }
                                }
                            }
                        }
//                        }
                    }
                }
            }
        }
    }

    T_IG(TRACE_GRP_CLOCK, "internal oscillator ssm %d, priority prio %u", internal_oscillator_ssm, internal_oscillator_pri);
    my_oscillator_pri = internal_oscillator_pri;
    bool my_physical_port_selected = false;
#if defined(VTSS_SW_OPTION_PTP)
    vtss_ptp_synce_src_t src = {VTSS_PTP_SYNCE_NONE, 0};
    src.ref = clock_selection_mode_config.source;
    int bm;
#endif //defined(VTSS_SW_OPTION_PTP)
    for (i=0; i<synce_my_nominated_max; ++i)
    {
        T_DG(TRACE_GRP_CLOCK, "clock_input_priority_set  source %u  prio %u", i, prio_assigned[i]);
        if (clock_priority_set(i, prio_assigned[i]) != VTSS_OK)    T_D("error returned source %u", i);
        (void) synce_network_port_clk_in_port_combo_to_port(clock_source_nomination_config[i].network_port, clock_source_nomination_config[i].clk_in_port, &port);  // Port number of this clock input
        if (prio_assigned[i] == 0) {
            if (port < SYNCE_PORT_COUNT) {
                my_physical_port_selected = true;
#if defined(VTSS_SW_OPTION_PTP)
                src.type = VTSS_PTP_SYNCE_ELEC;
                src.ref = synce_get_selector_ref_no(i, port);
#endif //defined(VTSS_SW_OPTION_PTP)
            }
#if defined(VTSS_SW_OPTION_PTP)
        } else if ((bm = best_master) >= 0) {
            src.type = VTSS_PTP_SYNCE_PAC;
            src.ref = bm;
#endif //defined(VTSS_SW_OPTION_PTP)
        }
        T_IG(TRACE_GRP_CLOCK, "source %u  prio %u, port %u, physical_port_selected %d", i, prio_assigned[i], port, my_physical_port_selected);
    }
#if defined(VTSS_SW_OPTION_PTP)
    SYNCE_RC(ptp_set_selected_src(&src));
#endif //defined(VTSS_SW_OPTION_PTP)
    physical_port_selected = my_physical_port_selected;
}

vtss_appl_synce_quality_level_t ssm_to_ql(uint ssm)
{
    switch (ssm)
    {
        case SSM_QL_PRS_INV:  return (VTSS_APPL_SYNCE_QL_INV);
        case SSM_QL_PRC:      return (VTSS_APPL_SYNCE_QL_PRC);
        case SSM_QL_SSUA_TNC: return (VTSS_APPL_SYNCE_QL_SSUA);
        case SSM_QL_SSUB:     return (VTSS_APPL_SYNCE_QL_SSUB);
        case SSM_QL_EEC2:     return (VTSS_APPL_SYNCE_QL_EEC2);
        case SSM_QL_EEC1:     return (VTSS_APPL_SYNCE_QL_EEC1);
        case SSM_QL_PROV:     return (VTSS_APPL_SYNCE_QL_NONE);
        case SSM_QL_DNU_DUS:  return (VTSS_APPL_SYNCE_QL_DNU);
        case SSM_QL_FAIL:     return (VTSS_APPL_SYNCE_QL_FAIL);
        default:              return (VTSS_APPL_SYNCE_QL_FAIL);
    }
}
static void update_hybrid_transient(void)
{
#if defined(VTSS_SW_OPTION_PTP)
    bool new_transient;
    new_transient = (clock_alarm_state.lol || ((current_ql != VTSS_APPL_SYNCE_QL_PRC) && (current_ql != VTSS_APPL_SYNCE_QL_INV))) &&
                                               (my_oscillator_pri != 0) && !clock_alarm_state.dhold;
    if (new_transient != clock_alarm_state.trans) {
        T_W("New transient %d, Old transient %d.", new_transient, clock_alarm_state.trans);
        T_W("lol %d, ql %s, dhold %d, oscillator_pri %u", clock_alarm_state.lol, ssm_string(current_ql), clock_alarm_state.dhold, my_oscillator_pri);
        clock_alarm_state.trans = new_transient;
        if (vtss_ptp_set_hybrid_transient(new_transient ? VTSS_PTP_HYBRID_TRANSIENT_QUICK : VTSS_PTP_HYBRID_TRANSIENT_NOT_ACTIVE) != VTSS_RC_OK) {
            T_D("Could not set hybrid transient to %s.", new_transient ? "VTSS_PTP_HYBRID_TRANSIENT_QUICK" : "VTSS_PTP_HYBRID_TRANSIENT_NOT_ACTIVE");
        }
    }
#endif //(VTSS_SW_OPTION_PTP)
}

static void set_tx_ssm(void)
{
    static uint                       old_tx_ssm = SSM_QL_EEC1;
    vtss_appl_synce_selector_state_t  selector_state;
    uint                              clock_input, new_tx_ssm, i;
    u32                               port;

    if (clock_selector_compensated_state_get(&clock_input, &selector_state) != VTSS_RC_OK) {
        T_D("error returned");                                                        // When clock_selector_compensated_state_get returns VTSS_RC_ERROR, it is because
        clock_input = 0;                                                              // no SyncE DPLL has been detected and in that case SyncE is not actually supported.
        selector_state = VTSS_APPL_SYNCE_SELECTOR_STATE_FREERUN;
    }

    clock_old_selector_state = selector_state;
    T_D("selector_state = %d", selector_state);

    new_tx_ssm = old_tx_ssm;
    port = 0;

    switch (selector_state)
    {
        case VTSS_APPL_SYNCE_SELECTOR_STATE_LOCKED:
        case VTSS_APPL_SYNCE_SELECTOR_STATE_ACQUIRING:
        {
            (void) synce_network_port_clk_in_port_combo_to_port(clock_source_nomination_config[clock_input].network_port, clock_source_nomination_config[clock_input].clk_in_port, &port);
            new_tx_ssm = get_ssm_rx(port, clock_input);

            break;
        }
        case VTSS_APPL_SYNCE_SELECTOR_STATE_HOLDOVER:
        {
#if defined(VTSS_SW_OPTION_PTP)
            int m = best_master;
            if (m >= 0) {  // SYNCE source is using PTP 8265.1
                // If PTSF for PTP source is None then signal the SSM level indicated by the source otherwise signal our own SSM level for Freerun state (as PTP is not yet locked)
                if (port_state[SYNCE_PORT_COUNT + m].ptsf == SYNCE_PTSF_NONE) {
                    new_tx_ssm = get_ssm_rx(SYNCE_PORT_COUNT + m, best_master_source);
                } else {
                    new_tx_ssm = overwrite_conv(clock_selection_mode_config.ssm_freerun);
                }
            }
            else {
#else
            {
#endif
                new_tx_ssm = overwrite_conv(clock_selection_mode_config.ssm_holdover);
            }
            break;
        }
        case VTSS_APPL_SYNCE_SELECTOR_STATE_FREERUN:
        case VTSS_APPL_SYNCE_SELECTOR_STATE_PTP:
        {
#if defined(VTSS_SW_OPTION_PTP)
            int m = best_master;
            if (m >= 0) {  // SYNCE source is using PTP 8265.1
                // If PTSF for PTP source is None then signal the SSM level indicated by the source otherwise signal our own SSM level for Freerun state (as PTP is not yet locked)
                if (port_state[SYNCE_PORT_COUNT + m].ptsf == SYNCE_PTSF_NONE) {
                    new_tx_ssm = get_ssm_rx(SYNCE_PORT_COUNT + m, best_master_source);
                } else {
                    new_tx_ssm = overwrite_conv(clock_selection_mode_config.ssm_freerun);
                }
            }
            else {
#else
            {
#endif
                new_tx_ssm = overwrite_conv(clock_selection_mode_config.ssm_freerun);
                T_I("new_tx_ssm = %d",new_tx_ssm);
            }
            break;
        }
        default: new_tx_ssm = SSM_QL_DNU_DUS;
            break;
    }

    /* transmit received ssm on all ports - ofc DNU on selected */
    for (i=0; i<SYNCE_PORT_COUNT; ++i) {
        port_state[i].ssm_tx = new_tx_ssm;
        if (port_state[i].port_dnu) {
            port_state[i].ssm_tx = SSM_QL_DNU_DUS;
        }
    }

    if ((selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_LOCKED || selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_ACQUIRING) && (port < SYNCE_PORT_COUNT))
        port_state[port].ssm_tx = SSM_QL_DNU_DUS;

    current_ql = ssm_to_ql(new_tx_ssm);
#if defined(VTSS_SW_OPTION_PACKET) && defined(VTSS_SW_OPTION_PTP)
    if (new_tx_ssm != old_tx_ssm) {
        update_hybrid_transient();
        //if (!clock_alarm_state.lol) {
        //    // If the new_tx_ssm != old_tx_ssm, it means the source has changed or we received a different SSM from the same source
        //    if (new_tx_ssm != SSM_QL_PRC && new_tx_ssm != SSM_QL_PRS_INV) {   // FIXME: Need to implement some code here to handle any glitch in relation to the MS-PDV
        //        if (vtss_ptp_set_hybrid_transient(VTSS_PTP_HYBRID_TRANSIENT_QUICK) != VTSS_RC_OK) {
        //            T_D("Could not set hybrid transient to VTSS_PTP_HYBRID_TRANSIENT_QUICK.");
        //        }
        //    } else {
        //        if (vtss_ptp_set_hybrid_transient(VTSS_PTP_HYBRID_TRANSIENT_NOT_ACTIVE) != VTSS_RC_OK) {
        //            T_D("Could not set hybrid transient to VTSS_PTP_HYBRID_TRANSIENT_NOT_ACTIVE.");
        //        }
        //    }
        //}
        vtss_flag_setbits(&func_wait_flag, FLAG_SSM_EVENT);
    }
#endif

    current_selector_state = selector_state;
    current_selected_port = port;
    T_D("current_ql %d, cur state %d, cur_port %d", current_ql, current_selector_state, current_selected_port);
    old_tx_ssm = new_tx_ssm;
}



#ifdef VTSS_SW_OPTION_PACKET
static void ssm_frame_tx(uint port, bool event_flag, uint ssm)
{
    static u8    reserved[3] = {0x00,0x00,0x00};
    static u8    tlv[3] = {0x01,0x00,0x04};
    u8           version = 0x10;
    u32          len = 64;
    u8           *buffer;
    conf_board_t conf;

    if (!port_config[port].ssm_enabled)  return;

    if (conf_mgmt_board_get(&conf) < 0)
        return;

    if ((buffer = packet_tx_alloc(len))) {
        packet_tx_props_t tx_props;

//T_D("port %d ssm %x", port, ssm);
        memset(buffer, 0xff, len);
        if (event_flag) version |= 0x08;
        ssm &= 0x0F;

        memcpy(buffer, ssm_dmac, 6);
        memcpy(buffer+6, conf.mac_address, 6);
        memcpy(buffer+6+6, ssm_ethertype, 2);
        memcpy(buffer+6+6+2, ssm_standard, 6);
        memcpy(buffer+6+6+2+6, &version, 1);
        memcpy(buffer+6+6+2+6+1, reserved, 3);
        memcpy(buffer+6+6+2+6+1+3, tlv, 3);
        memcpy(buffer+6+6+2+6+1+3+3, &ssm, 1);
        packet_tx_props_init(&tx_props);
        tx_props.packet_info.modid     = VTSS_MODULE_ID_SYNCE;
        tx_props.packet_info.frm       = buffer;
        tx_props.packet_info.len       = len;
        tx_props.tx_info.dst_port_mask = VTSS_BIT64(port);

        T_DG(TRACE_GRP_PDU_TX, "port %u  length %u  dmac %X-%X-%X-%X-%X-%X  smac %X-%X-%X-%X-%X-%X  Ethertype %X-%X", port, len, buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5], buffer[6], buffer[7], buffer[8], buffer[9], buffer[10], buffer[11], buffer[12], buffer[13]);
        T_DG(TRACE_GRP_PDU_TX, "frame[14-20] %X-%X-%X-%X-%X-%X-%X  ssm %X-%X-%X-%X", buffer[14], buffer[15], buffer[16], buffer[17], buffer[18], buffer[19], buffer[20], buffer[24], buffer[25], buffer[26], buffer[27]);
        if (packet_tx(&tx_props) != VTSS_RC_OK) {
            T_D("Packet tx failed");
        }
    }
}



static BOOL ssm_frame_rx(void *contxt, const unsigned char *const frm, const mesa_packet_rx_info_t *const rx_info)
{
    uint port;

    SYNCE_CRIT_ENTER();
    T_DG(TRACE_GRP_PDU_RX, "port %u  length %u  dmac %X-%X-%X-%X-%X-%X  smac %X-%X-%X-%X-%X-%X  ethertype %X-%X", rx_info->port_no, rx_info->length, frm[0], frm[1], frm[2], frm[3], frm[4], frm[5], frm[6], frm[7], frm[8], frm[9], frm[10], frm[11], frm[12], frm[13]);
    T_DG(TRACE_GRP_PDU_RX, "frame[14-20] %X-%X-%X-%X-%X-%X-%X  ssm %X-%X-%X-%X", frm[14], frm[15], frm[16], frm[17], frm[18], frm[19], frm[20], frm[24], frm[25], frm[26], frm[27]);
    if ((rx_info->length < 28) || (memcmp(&frm[14], ssm_standard, 6) == 0))
    {
    /* this is a 'true' ESMC (SSM) slow protocal PDU */
        port = rx_info->port_no;
        if ((port < SYNCE_PORT_COUNT) && port_config[port].ssm_enabled)
        {
            port_state[port].ssm_frame = frm[27] & 0x0F;
            port_state[port].new_ssm = true;

            vtss_flag_setbits(&func_wait_flag, FLAG_FRAME_RX);
        }
        SYNCE_CRIT_EXIT();
        return TRUE;
    }
    SYNCE_CRIT_EXIT();
    return FALSE;
}
#endif


static void change_state_action(uint source)
{
    /* do the WTR stuff */
    set_wtr(source);

    /* calculate and change priority in clock controller */
    set_clock_priority();

    /* calculate and change clock source in phy */
    set_clock_source(source);

    /* check for change in transmission of SSM */
    set_tx_ssm();

    /* Configure master/slave */
    configure_master_slave();
}

void set_eec_option(vtss_appl_synce_eec_option_t eec_option)
{
    (void)clock_eec_option_set(eec_option == VTSS_APPL_SYNCE_EEC_OPTION_1 ? CLOCK_EEC_OPTION_1 : CLOCK_EEC_OPTION_2);
}

static void func_thread(vtss_addrword_t data)
{
    uint                             i, source, new_ssm, old_ssm, old_state;
    bool                             state, holdoff_active;
    vtss_flag_value_t                flag_value;
    mesa_phy_status_1g_t             phy_status;
    mesa_phy_conf_1g_t               phy_setup;
    conf_blk_t                       save_blk;
    vtss_mtimer_t                    onesec_timer;
    vtss_appl_synce_selector_state_t new_selector_state;
    uint                             new_source;
    clock_event_type_t ev_mask;

    masterLock.wait();      // only continue when master

// FIXME: This exception need to be handled in the board API somehow.
//
//    if (vtss::synce::dpll::pcb104_synce) {
//        /* Enable Station clock input for PCB104 */
//        synce_set_source_port(0, SYNCE_STATION_CLOCK_PORT, 1);
//        synce_set_source_port(1, SYNCE_STATION_CLOCK_PORT, 1);
//    }

    synce_set_clock_source_nomination_config_to_default(save_blk.clock_source_nomination_config);
    synce_get_clock_selection_mode_config_default(&save_blk.clock_selection_mode_config);
    synce_set_station_clock_config_to_default(&save_blk.station_clock_config);
    synce_set_port_config_to_default(save_blk.port_config.data());

    /* Apply configuration */
    apply_configuration(&save_blk);

    VTSS_MTIMER_START(&onesec_timer, 950);
    holdoff_active = false;
    
    for (;;)
    {
#if defined(VTSS_SW_OPTION_PTP)
        flag_value = FLAG_PORT_CALL_BACK | FLAG_FRAME_RX | FLAG_SSM_EVENT | FLAG_PTSF_CHANGE;
#else
        flag_value = FLAG_PORT_CALL_BACK | FLAG_FRAME_RX | FLAG_SSM_EVENT;
#endif
        flag_value = vtss_flag_timed_wait(&func_wait_flag, flag_value, VTSS_FLAG_WAITMODE_OR_CLR, vtss_current_time() + VTSS_OS_MSEC2TICK(holdoff_active ? 50 : 1000));

        SYNCE_CRIT_ENTER();
        /* no return within critical zone */

        // Note: The check for changes in LOL has been moved up so this is now the very first thing done. Also it is done everytime. This is because it controls the signaling of
        //       transients of type "Quick" in the MS-PDV and these shall have higher priority than transients of type "Optional" signaled by set_tx_ssm.
        {
            vtss_appl_synce_selector_state_t  selector_state;
            uint                              clock_input;

            T_D("new lol");
            if (clock_selector_compensated_state_get(&clock_input, &selector_state) != VTSS_OK) T_D("error returned");
            if (selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_PTP) {
                T_I("new lol N/A");
                if (clock_alarm_state.lol != VTSS_APPL_SYNCE_LOL_ALARM_STATE_NA) {
                    clock_alarm_state.lol = VTSS_APPL_SYNCE_LOL_ALARM_STATE_NA;

#if defined(VTSS_SW_OPTION_PTP)
                    update_hybrid_transient();
                    //if (vtss_ptp_set_hybrid_transient(VTSS_PTP_HYBRID_TRANSIENT_NOT_ACTIVE) != VTSS_RC_OK) {
                    //    T_D("Could not clear hybrid transient.");
                    //}
#endif

                    /* check for change in transmission of SSM */
                    set_tx_ssm();
                }
            } else {
                if (clock_lol_state_get(&state) != VTSS_OK) T_D("error returned");
                T_N("new lol %d", state);

                vtss_appl_synce_lol_alarm_state_t new_lol_alarm_state = state ? VTSS_APPL_SYNCE_LOL_ALARM_STATE_TRUE : VTSS_APPL_SYNCE_LOL_ALARM_STATE_FALSE;   
                if (clock_alarm_state.lol != new_lol_alarm_state) {
                    clock_alarm_state.lol = new_lol_alarm_state;

#if defined(VTSS_SW_OPTION_PTP)
                    update_hybrid_transient();
                    //if (vtss_ptp_set_hybrid_transient(state ? VTSS_PTP_HYBRID_TRANSIENT_QUICK : VTSS_PTP_HYBRID_TRANSIENT_NOT_ACTIVE) != VTSS_RC_OK) {  // FIXME: Should set/clear ZL303XX_BHIT_QUICK type transient
                    //    T_D("Could not set hybrid transient.");
                    //}
#endif
    
                    /* check for change in transmission of SSM */
                    set_tx_ssm();
                }
            }
            if (clock_input != current_selected_port) {
                set_tx_ssm();
            }
        }

#ifdef VTSS_SW_OPTION_PACKET
        if (VTSS_MTIMER_TIMEOUT(&onesec_timer) || (flag_value & FLAG_SSM_EVENT)) {
            /* SSM is transmitted once a sec or when an event occur */
            for (i=0; i<SYNCE_PORT_COUNT; ++i)
            { /* Transmit SSM */
                if (!port_config[i].ssm_enabled)  continue;

                ssm_frame_tx(i, (flag_value & FLAG_SSM_EVENT), port_state[i].ssm_tx);
            }
        }
#endif
        if (VTSS_MTIMER_TIMEOUT(&onesec_timer)) {
#ifdef VTSS_SW_OPTION_PACKET
            static int packet_rx_delay = 0;

            if (packet_rx_delay < 21) {  // Count to 21 and stop there. Packet filter is installed at the point when counter reaches 20.
                ++packet_rx_delay;
            }
            if (packet_rx_delay == 20) {
                packet_rx_filter_t filter;
                void *filter_id;
                mesa_rc rc;

                /* hook up on SSM frame */
                packet_rx_filter_init(&filter);
                filter.modid = VTSS_MODULE_ID_SYNCE;
                filter.match = PACKET_RX_FILTER_MATCH_DMAC | PACKET_RX_FILTER_MATCH_ETYPE;
                filter.cb    = ssm_frame_rx;
                filter.prio  = PACKET_RX_FILTER_PRIO_NORMAL;
                filter.etype = 0x8809; // slow protocol ethertype
                memcpy(filter.dmac, ssm_dmac, sizeof(filter.dmac));

                if ((rc = packet_rx_filter_register(&filter, &filter_id)) != VTSS_OK) T_D("error returned rc %u", rc);
            }
#endif

            /* check if selector state has changed (this check is needed because the ZL30343 may shortly enter holdover mode before locked mode */
            if (clock_selector_compensated_state_get(&new_source, &new_selector_state) != VTSS_OK) T_D("clock_selector_compensated_state_get failed");
            if (new_selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_FREERUN || clock_old_selector_state != new_selector_state)
            {
            /* wait for selector out of free run mode during system startup */
                /* check for change in transmission of SSM */
                set_tx_ssm();

                /* check and configure master/slave mode of the nominated sources */
                configure_master_slave();
            }

            for (source=0; source<synce_my_nominated_max; ++source)
            { /* Run the WTR timer */
                if (clock_alarm_state.wtr[source])
                {
                /* WTR state - run timer */
                    if (clock_state[source].wtr_timer > 0)  clock_state[source].wtr_timer--;
                    if (clock_state[source].wtr_timer == 0)
                    {
                    /* WTR timeout */
                        T_D("WTR timeout  source %d", source);
                        clock_alarm_state.wtr[source] = false;
                        change_state_action(source);
                    }
                }
            }

            for (i=0; i<SYNCE_PORT_COUNT; ++i)
            { /* Run the prefer master/slave timer */
                if (port_state[i].prefer_timer) {   /* Prefer timer is active */
                    port_state[i].prefer_timer++;
                    if (port_state[i].prefer_timer >= PREFER_TIMEOUT) {   /* Prefer time out */
                        port_state[i].prefer_timer = 0;  /* Passive prefer timer */
                        /* don't call the phy, for the station clock 'port' */
                        if (i < SYNCE_STATION_CLOCK_PORT && port_is_nominated(i, &source) && (clock_source_nomination_config[source].aneg_mode != VTSS_APPL_SYNCE_ANEG_FORCED_SLAVE)) {
                            phy_setup.master.cfg = false;
                            if (mesa_phy_conf_1g_set(PHY_INST, i, &phy_setup) != VTSS_OK)    T_D("mesa_phy_conf_1g_set port %u failed", i);
                        }
                        T_I("Port %d,  Actual link status %d, Current link status %d", i, port_state[i].actual_link, port_state[i].link);
                        if (port_state[i].actual_link != port_state[i].link) {
                            port_state[i].link = port_state[i].actual_link;
                            port_state[i].new_link = true;
                            vtss_flag_setbits(&func_wait_flag, FLAG_PORT_CALL_BACK);
                        }
                    }
                }
            }

            for (i = 0; i < SYNCE_PORT_COUNT + PTP_CLOCK_INSTANCES; ++i)
            { /* Check for no SSM received - declare SSM failed */
                if ((port_config[i].ssm_enabled || i >= SYNCE_PORT_COUNT) && !(port_state[i].ssm_state & (SSM_LINK_DOWN | SSM_FAIL)))
                {
                /* SSM enabled and link is active - check for SSM received */
                    port_state[i].ssm_count++;
                    if (port_state[i].ssm_count >= 10)
                    {
                        /* SSM on this port is now failed */
                        port_state[i].ssm_count = 10;
                        port_state[i].ssm_state |= SSM_FAIL;
                        port_state[i].ssm_rx = SSM_QL_FAIL;
                        T_D("SSM fail  port %d  ssm_state %d", i, port_state[i].ssm_state);

                        if (port_is_nominated(i, &source))
                        {
                            clock_alarm_state.ssm[source] = (port_state[i].ssm_state != SSM_OK) ? true : false;

                            change_state_action(source);
                        }
                    }
                }
            }

            for (i=0; i<SYNCE_PORT_COUNT; ++i)
            { /* Get PHY master/slave mode */
                if (port_state[i].phy && port_state[i].speed == MESA_SPEED_1G)
                {
                    if (mesa_phy_status_1g_get(NULL, i, &phy_status) != VTSS_OK)    T_D("mesa_phy_status_1g_get port %u failed", i);
                    port_state[i].master = phy_status.master;
                }
            }

            { /* Get digital hold valid state */
#if defined(VTSS_SW_OPTION_PTP)

                if (best_master >= 0) {  // PLL is being controlled by PTP 8265.1 servo. Get the holdover value from the servo.
                    clock_alarm_state.dhold = !vtss_ptp_servo_get_holdover_status(best_master);
                }
                else {  // PLL is using normal SyncE input as source
                    if (clock_dhold_state_get(&state) != VTSS_OK)    T_D("error returned");
                    clock_alarm_state.dhold = !state;
                }
                update_hybrid_transient();
#else
                if (clock_dhold_state_get(&state) != VTSS_OK)    T_D("error returned");
                clock_alarm_state.dhold = !state;
#endif
            }
        } /* One second time out */
        // the Event Pool call is needed because this function updates some internal data
        {
            static struct timespec time_of_last_poll = {0};
            struct timespec tp;

            if (clock_gettime(CLOCK_MONOTONIC_COARSE, &tp) != 0) {
                T_E("clock_gettime returned an error");
            } else {
                i64 time_since_last = (i64)(tp.tv_sec - time_of_last_poll.tv_sec) * 1000000000 + (tp.tv_nsec - time_of_last_poll.tv_nsec);
                if (time_since_last < 25000000) {
                    T_D("Time since last poll " VPRI64d " ns (i.e. < 25 ms) - clock_event_poll skipped\n", time_since_last);
                } else {
                    clock_event_poll(false, &ev_mask);
                }
                time_of_last_poll = tp;
            }
        }
        if (holdoff_active) { /* Run the hold off timer */
            bool holdoff;
            if (clock_holdoff_run(&holdoff_active) != VTSS_OK)    T_D("error returned");
            for (source=0; source<synce_my_nominated_max; ++source) { /* Check for holdoff time out on any clock source */
                if (clock_holdoff_active_get(source, &holdoff) != VTSS_OK)    T_D("error returned");
                if (clock_state[source].holdoff && !holdoff) { /* Hold off timer runnig out */
                    clock_state[source].holdoff = false;
                    if (clock_locs_state_get(source, &state) != VTSS_OK)    T_D("error returned");
                    if (state)  change_state_action(source);
                }
            }
        }

        /* Loss Of Clock Source is checked at every run, because it controls the holdoff process */
        for (source = 0; source < synce_my_nominated_max; ++source)
        {
            u32 port;
            (void) synce_network_port_clk_in_port_combo_to_port(clock_source_nomination_config[source].network_port, clock_source_nomination_config[source].clk_in_port, &port);
#if defined(VTSS_SW_OPTION_PTP)
            if (port < SYNCE_PORT_COUNT) {
                if (clock_locs_state_get(source, &state) != VTSS_OK)    T_D("error returned");
            } else {
                state = (port_state[port].ptsf <= SYNCE_PTSF_UNUSABLE) ? false : true;
            }
#else
            if (clock_locs_state_get(source, &state) != VTSS_OK)    T_D("error returned");
#endif
            T_D("locs source %d, value %d", source, state);
            if (state != clock_alarm_state.locs[source]) {
                clock_alarm_state.locs[source] = state;

                if (clock_holdoff_event(source) != VTSS_OK)    T_D("error returned");   /* Signal Hold Off event to clock API */
                if (clock_holdoff_active_get(source, &clock_state[source].holdoff) != VTSS_OK)    T_D("error returned");    /* Check if hold of active on any clock source */
                holdoff_active = holdoff_active || clock_state[source].holdoff;

                (void) synce_network_port_clk_in_port_combo_to_port(clock_source_nomination_config[source].network_port, clock_source_nomination_config[source].clk_in_port, &port);
                if (!clock_state[source].holdoff && (state || port_state[port].link))  /* Holdoff is not active and state is active or link is up */
                    change_state_action(source);
            }
        }

#if defined(VTSS_SW_OPTION_PTP)
        if (flag_value & FLAG_PTSF_CHANGE) {  // A PTSF has changed. If it changed to SYNCE_PTSF_NONE it means a PTP 8265.1 source has now locked.
            SYNCE_PTSF_CRIT_ENTER();
            for (i = 0; i < PTP_CLOCK_INSTANCES; ++i) {
                if (ptsf_state[i].new_ptsf) {
                    port_state[SYNCE_PORT_COUNT + i].ptsf = ptsf_state[i].ptsf;
                    T_D("new ptsf to source %d value is now %d", source, port_state[SYNCE_PORT_COUNT + i].ptsf);
                    port_state[SYNCE_PORT_COUNT + i].new_ptsf = true;
                    ptsf_state[i].new_ptsf = false;
                }
            }
            SYNCE_PTSF_CRIT_EXIT();
            for (int source = 0; source < synce_my_nominated_max; ++source) {
                u32 port;
                (void) synce_network_port_clk_in_port_combo_to_port(clock_source_nomination_config[source].network_port, clock_source_nomination_config[source].clk_in_port, &port);
                if (port >= SYNCE_PORT_COUNT) {
                    if (port_state[port].new_ptsf) {
                        T_D("new ptsf to source %d value is now %d", source, port_state[port].ptsf);
                        port_state[port].new_ptsf = false;
                        if (port_state[port].ptsf == SYNCE_PTSF_NONE) change_state_action(source);
                    }
                }
            }
            set_tx_ssm();  // A change of PTSF values may affect the transmitted SSM levels, so call set_tx_ssm to update the SSMs
        }
#endif

        if (VTSS_MTIMER_TIMEOUT(&onesec_timer))
        {
            T_D("new losx");
            if (clock_losx_state_get(&state) != VTSS_OK)    T_D("error returned");
            if (clock_alarm_state.losx != state) {
                clock_alarm_state.losx = state;

                /* check for change in transmission of SSM */
                set_tx_ssm();
            }
            (void)clock_selector_monitor_state();
        }
        if (flag_value & FLAG_PORT_CALL_BACK)
        {   /* Change in port state */
#if defined(VTSS_SW_OPTION_PTP)
            SYNCE_PTSF_CRIT_ENTER();
            for (i = 0; i < PTP_CLOCK_INSTANCES; ++i) {
                port_state[SYNCE_PORT_COUNT + i].new_link = ptsf_state[i].new_link;
                port_state[SYNCE_PORT_COUNT + i].link = ptsf_state[i].link;
                ptsf_state[i].new_link = false;
                port_state[SYNCE_PORT_COUNT + i].first_port_callback = ptsf_state[i].first_port_callback;
                T_D("new-link ptp inst %d link %d", i, ptsf_state[i].link);
            }
            SYNCE_PTSF_CRIT_EXIT();
#endif
            for (i = 0; i < SYNCE_PORT_COUNT + PTP_CLOCK_INSTANCES; ++i)
            {
                T_D("new-link port %d link %d", i, port_state[i].new_link);
                if (!port_state[i].link && (port_config[i].ssm_enabled || (i >= SYNCE_PORT_COUNT))) {
                    port_state[i].ssm_state |= (SSM_LINK_DOWN | SSM_NOT_SEEN);
                    port_state[i].ssm_state |= SSM_FAIL;
                    port_state[i].ssm_rx = SSM_QL_FAIL;
                }
                else {
                    port_state[i].ssm_state &= ~SSM_LINK_DOWN;
                }
                if (port_state[i].new_link)
                {
                    T_D("new-link port %d link %d", i, port_state[i].link);
                    port_state[i].new_link = false;
                    if (port_is_nominated(i, &source))
                    {
                    /* the port is norminated */
                        clock_alarm_state.ssm[source] = (port_state[i].ssm_state != SSM_OK) ? true : false;
                        if (port_state[i].link && !clock_state[source].holdoff)   /* Link is comming back up without holdoff */
                            change_state_action(source);

                        if (!port_state[i].link) /* Link is going down: WTR state must always be updated, otherwise the link state is not registered in WTR state unless the locs is detected after link down */
                            change_state_action(source);

                        if (MESA_CAP(MESA_CAP_SYNCE)) {
                            mesa_synce_clock_in_t  clk_in;
                            if (i < (SYNCE_PORT_COUNT - 1) && !port_state[i].phy && !port_state[i].link) { /* Link is going down and port is NOT a PHY meaning SERDES - need to disable in order to create LOCS */
                                clk_in.enable = false;
                                clk_in.port_no = i;
                                clk_in.squelsh = SWITCH_AUTO_SQUELCH;
                                T_D("mesa_synce_clock_in_set source %d", source);
                                if (mesa_synce_clock_in_set(NULL, synce_get_switch_recovered_clock(source, i),  &clk_in) != VTSS_OK)    T_D("mesa_synce_clock_in_set returned error  port %u", i);
                            }
                        }
                    }
                }
                if (port_state[i].new_fiber)
                {
                    T_D("new-fiber port %d", i);
                    port_state[i].new_fiber = false;
                    if (port_is_nominated(i, &source))
                        set_clock_source(source);
                }
            }
        }
        if (flag_value & FLAG_FRAME_RX)
        {
            // copy ssm info received from PTP
#if defined(VTSS_SW_OPTION_PTP)
            SYNCE_PTSF_CRIT_ENTER();
            for (i = 0; i < PTP_CLOCK_INSTANCES; ++i)
            {
                port_state[SYNCE_PORT_COUNT + i].new_ssm = ptsf_state[i].new_ssm;
                port_state[SYNCE_PORT_COUNT + i].ssm_frame = ptsf_state[i].ssm_frame;
            }
            SYNCE_PTSF_CRIT_EXIT();
#endif
            for (i = 0; i < SYNCE_PORT_COUNT + PTP_CLOCK_INSTANCES; ++i)
            {
                if (port_state[i].new_ssm)
                {
                    port_state[i].new_ssm = false;

                    new_ssm = port_state[i].ssm_frame;
                    old_ssm = port_state[i].ssm_rx;
                    old_state = port_state[i].ssm_state;

                    port_state[i].ssm_rx = new_ssm;

                    /* has to count to 5 again to be SSM_FAIL */
                    port_state[i].ssm_count = 0;
                    port_state[i].ssm_state &= ~(SSM_FAIL | SSM_NOT_SEEN);

                    if (clock_selection_mode_config.eec_option == VTSS_APPL_SYNCE_EEC_OPTION_1) {
                        if ((new_ssm == SSM_QL_PRC) || (new_ssm == SSM_QL_SSUA_TNC) || (new_ssm == SSM_QL_SSUB) || (new_ssm == SSM_QL_EEC1) || (new_ssm == SSM_QL_DNU_DUS)) {
                            port_state[i].ssm_state &= ~SSM_INVALID;  /* received a valid SSM */
                        } else {
                            port_state[i].ssm_state |= SSM_INVALID;
                        }
                    } else if (clock_selection_mode_config.eec_option == VTSS_APPL_SYNCE_EEC_OPTION_2) {
                        if ((new_ssm == SSM_QL_PRS_INV) || (new_ssm == SSM_QL_STU) || (new_ssm == SSM_QL_ST2) || (new_ssm == SSM_QL_SSUA_TNC) || (new_ssm == SSM_QL_ST3E) ||
                            (new_ssm == SSM_QL_EEC2) || (new_ssm == SSM_QL_SMC) || (new_ssm == SSM_QL_PROV) || (new_ssm == SSM_QL_DNU_DUS))
                        {
                            port_state[i].ssm_state &= ~SSM_INVALID;  /* received a valid SSM */
                        } else {
                            port_state[i].ssm_state |= SSM_INVALID;
                        }
                    } else {
                        port_state[i].ssm_state |= SSM_INVALID;
                    }

                    if (((port_state[i].ssm_state != old_state) || (new_ssm != old_ssm)) && port_is_nominated(i, &source))
                    {
                        clock_alarm_state.ssm[source] = (port_state[i].ssm_state != SSM_OK) ? true : false;
                        T_D("frame rx  port %d  ssm %x  ssm_state %d", i, new_ssm, port_state[i].ssm_state);

                        if (port_state[i].link)    change_state_action(source);
                    }

                }
            }
        }
        if (VTSS_MTIMER_TIMEOUT(&onesec_timer)) {
            /* One second time out: restart timer */
            VTSS_MTIMER_START(&onesec_timer, 950);
            T_D("one sec restarted");
        }
        SYNCE_CRIT_EXIT();
    }
}

/****************************************************************************/
/*  MISC. Help functions                                                    */
/****************************************************************************/

const char* ssm_string(vtss_appl_synce_quality_level_t ssm)
{
    switch(ssm)
    {
        case VTSS_APPL_SYNCE_QL_NONE: return("QL_NONE");
        case VTSS_APPL_SYNCE_QL_PRC:  return("QL_PRC");
        case VTSS_APPL_SYNCE_QL_SSUA: return("QL_SSUA");
        case VTSS_APPL_SYNCE_QL_SSUB: return("QL_SSUB");
        case VTSS_APPL_SYNCE_QL_DNU:  return("QL_DNU");
        case VTSS_APPL_SYNCE_QL_EEC2: return("QL_EEC2");
        case VTSS_APPL_SYNCE_QL_EEC1: return("QL_EEC1");
        case VTSS_APPL_SYNCE_QL_INV:  return("QL_INV");
        case VTSS_APPL_SYNCE_QL_FAIL: return("QL_FAIL");
        case VTSS_APPL_SYNCE_QL_LINK: return("QL_LINK");
        case VTSS_APPL_SYNCE_QL_PRS:  return("QL_PRS");
        case VTSS_APPL_SYNCE_QL_STU:  return("QL_STU");
        case VTSS_APPL_SYNCE_QL_ST2:  return("QL_ST2");
        case VTSS_APPL_SYNCE_QL_TNC:  return("QL_TNC");
        case VTSS_APPL_SYNCE_QL_ST3E: return("QL_ST3E");
        case VTSS_APPL_SYNCE_QL_SMC:  return("QL_SMC");
        case VTSS_APPL_SYNCE_QL_PROV: return("QL_PROV");
        case VTSS_APPL_SYNCE_QL_DUS:  return("QL_DUS");

        default:                      return("QL_NONE");
    }
}

// Convert error code to text
// In : rc - error return code
const char *synce_error_txt(mesa_rc rc)
{
    switch (rc) {
        case SYNCE_RC_OK:                return("SYNCE_RC_OK");
        case SYNCE_RC_INVALID_PARAMETER: return("Invalid parameter error returned from SYNCE\n");
        case SYNCE_RC_NOM_PORT:          return("Port nominated to a clock source is already nominated\n");
        case SYNCE_RC_SELECTION:         return("NOT possible to make Manuel To Selected if not in locked mode\n");
        case SYNCE_RC_INVALID_PORT:      return("The selected port is not valid\n");
        case SYNCE_RC_NOT_SUPPORTED:     return("The selected feature is not available in current HW config.\n");
        default:                         return("Unknown error returned from SYNCE\n");
    }
}

// Function for converting from user clock source (starting from 1) to clock source index (starting from 0)
// In - uclk - user clock source
// Return - Clock source index
u8 synce_uclk2iclk(u8 uclk) {
    return uclk - 1;
}

// Function for converting from clock source index (starting from 0) to user clock source (starting from 1)
// In - iclk - Index clock source
// Return - Clock source index
u8 synce_iclk2uclk(u8 iclk) {
    return iclk + 1;
}

clock_frequency_t station_clock_2clockFreq(vtss_appl_synce_frequency_t clk_in_f)
{
    switch (clk_in_f) {
        case VTSS_APPL_SYNCE_STATION_CLK_DIS: return CLOCK_FREQ_INVALID;
        case VTSS_APPL_SYNCE_STATION_CLK_1544_KHZ: return CLOCK_FREQ_1544_KHZ;
        case VTSS_APPL_SYNCE_STATION_CLK_2048_KHZ: return CLOCK_FREQ_2048_KHZ;
        case VTSS_APPL_SYNCE_STATION_CLK_10_MHZ: return CLOCK_FREQ_10MHZ;;
        default: return CLOCK_FREQ_INVALID;
    }
}

static void update_clk_in_selector(uint source)
{
    clock_frequency_t    frq;
    uint reference;
    if (clock_source_nomination_config[source].nominated) {
        u32 port;
        (void) synce_network_port_clk_in_port_combo_to_port(clock_source_nomination_config[source].network_port, clock_source_nomination_config[source].clk_in_port, &port);

        reference = synce_get_selector_ref_no(source, port);
        frq = (clock_source_nomination_config[source].network_port == 0) ?    // data_port == 0 means that station clock input is used.
              station_clock_2clockFreq(station_clock_config.station_clk_in) : synce_get_rcvrd_clock_frequency(source, port, port_state[port].speed);
    } else {
        frq = CLOCK_FREQ_INVALID;
        reference = CLOCK_REF_INVALID;
    }
    T_D("Set freq, source %d, freq %d", source,frq);
    if (clock_frequency_set(source, frq) != VTSS_OK)    T_D("error returned");
    if (clock_selector_map_set(reference, source) != VTSS_OK) T_D("error returned");
}



/****************************************************************************/
/*  SyncE interface                                                         */
/****************************************************************************/
mesa_rc vtss_appl_synce_clock_source_nomination_config_set(const uint sourceId, const vtss_appl_synce_clock_source_nomination_config_t *const config)
{
    uint  i;
    vtss_appl_synce_selector_state_t selector_state = VTSS_APPL_SYNCE_SELECTOR_STATE_FREERUN;
    uint clock_input = 0;
    u32 port;

    SYNCE_CRIT_ENTER();

    (void) synce_network_port_clk_in_port_combo_to_port(config->network_port, config->clk_in_port, &port);
    T_D("source = %d, port_no  = %d, aneg_mode = %d, ssm_overwrite %d, enable:%d, synce_get_source_port(source, port_no):%d",
        sourceId, port, config->aneg_mode, config->ssm_overwrite, config->nominated, synce_get_source_port(sourceId - 1, port));

    if ((sourceId < 1) || (sourceId > synce_my_nominated_max)) {
        SYNCE_CRIT_EXIT();
        return (SYNCE_RC_INVALID_PARAMETER);
    }

#if defined(VTSS_SW_OPTION_PTP)
    if (config->network_port == 0) {
        if ((config->clk_in_port != 0) && ((config->clk_in_port < 128) || (config->clk_in_port >= 128 + PTP_CLOCK_INSTANCES))) {
            SYNCE_CRIT_EXIT();
            return SYNCE_RC_INVALID_PARAMETER;
        }
    }
    else {
        if (config->clk_in_port != 0) {
            SYNCE_CRIT_EXIT();
            return SYNCE_RC_INVALID_PARAMETER;
        }
        if (port >= SYNCE_PORT_COUNT - 1) {
            SYNCE_CRIT_EXIT();
            return (SYNCE_RC_INVALID_PARAMETER);
        }
    }
#else
    if (config->clk_in_port != 0) {
        SYNCE_CRIT_EXIT();
        return SYNCE_RC_INVALID_PARAMETER;
    }
#endif

    if (config->nominated && !synce_get_source_port(sourceId - 1, port)) {
        SYNCE_CRIT_EXIT();
        return (SYNCE_RC_INVALID_PORT);
    }

    if (!config->nominated) {       
        /* This is a de-nomination of the clock source */
        clock_state[sourceId - 1].wtr_state = WTR_OK;
        clock_alarm_state.wtr[sourceId - 1] = false;
        clock_alarm_state.ssm[sourceId - 1] = false;

#if defined(VTSS_SW_OPTION_PTP)
        if ((clock_source_nomination_config[sourceId - 1].network_port != 0) || (clock_source_nomination_config[sourceId - 1].clk_in_port == 0)) {  // If this is true, the source being denominated is using an ordinary input port (or station clock) as its source port.
#else
        {
#endif
            if (clock_selector_compensated_state_get(&clock_input, &selector_state) != VTSS_OK) T_D("error returned");
            if ((selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_LOCKED) && (clock_input == (sourceId - 1))) {
                T_D("Force to holdover");
                /* node is locked to the nominated source, therefore switch to holdover */
                if (clock_selection_mode_set(VTSS_APPL_SYNCE_SELECTOR_MODE_FORCED_HOLDOVER, clock_input) != VTSS_OK)    T_D("error returned");
            }
        }
#if defined(VTSS_SW_OPTION_PTP)
        else {  // Clock being denominated is using a PTP 8265.1 (before the update) instance as it source port.
            int m = best_master;
            if (m == sourceId - 1) {
                if (port_state[m + SYNCE_PORT_COUNT].ptsf == SYNCE_PTSF_NONE) {
                    if (clock_selection_mode_set(clock_selection_mode_config.selection_mode, clock_selection_mode_config.source) != VTSS_OK)    T_D("error returned");
                }
                //vtss_ptp_servo_set_best_master(-1);  // Make sure the PTP 8265.1 instance does not control the DPLL now that it is no longer nominated.
                best_master = -1;
            }
        }
#endif
        /* save this de-normination in config block */
        clock_source_nomination_config[sourceId - 1].nominated = false;
    } else {
        /* if an other port is already nominated, then remove the nomination before nominating the new port */
        if (clock_source_nomination_config[sourceId - 1].nominated && clock_source_nomination_config[sourceId - 1].network_port != config->network_port) {
            clock_source_nomination_config[sourceId - 1].nominated = false;
            set_clock_source(sourceId - 1);
        }
        clock_source_nomination_config[sourceId - 1].nominated = true;
    }

    if (clock_selection_mode_config.eec_option == VTSS_APPL_SYNCE_EEC_OPTION_2) {
        if ((config->ssm_overwrite != VTSS_APPL_SYNCE_QL_NONE) && (config->ssm_overwrite != VTSS_APPL_SYNCE_QL_PRS) && (config->ssm_overwrite != VTSS_APPL_SYNCE_QL_STU) &&
            (config->ssm_overwrite != VTSS_APPL_SYNCE_QL_ST2) && (config->ssm_overwrite != VTSS_APPL_SYNCE_QL_TNC) && (config->ssm_overwrite != VTSS_APPL_SYNCE_QL_ST3E) &&
            (config->ssm_overwrite != VTSS_APPL_SYNCE_QL_EEC2) && (config->ssm_overwrite != VTSS_APPL_SYNCE_QL_SMC) && (config->ssm_overwrite != VTSS_APPL_SYNCE_QL_PROV) &&
            (config->ssm_overwrite != VTSS_APPL_SYNCE_QL_DUS))
        {
            SYNCE_CRIT_EXIT();
            return SYNCE_RC_INVALID_PARAMETER;
        }
    }
    else {
        if ((config->ssm_overwrite != VTSS_APPL_SYNCE_QL_NONE) && (config->ssm_overwrite != VTSS_APPL_SYNCE_QL_PRC) && (config->ssm_overwrite != VTSS_APPL_SYNCE_QL_SSUA) &&
            (config->ssm_overwrite != VTSS_APPL_SYNCE_QL_SSUB) && (config->ssm_overwrite != VTSS_APPL_SYNCE_QL_EEC1) && (config->ssm_overwrite != VTSS_APPL_SYNCE_QL_DNU))
        {
            SYNCE_CRIT_EXIT();
            return SYNCE_RC_INVALID_PARAMETER;
        }
    }
    if (config->holdoff_time && (config->holdoff_time != SYNCE_HOLDOFF_TEST) && ((config->holdoff_time < SYNCE_HOLDOFF_MIN) || (config->holdoff_time > SYNCE_HOLDOFF_MAX))) {
        SYNCE_CRIT_EXIT();
        return SYNCE_RC_INVALID_PARAMETER;
    }

    /* check if other clock sources is nominated to the same port    */
    for (i = 0; (i < synce_my_nominated_max) && (i != STATION_CLOCK_SOURCE_NO); ++i) {
        if (clock_source_nomination_config[sourceId - 1].nominated && clock_source_nomination_config[i].nominated &&
            (clock_source_nomination_config[i].network_port == config->network_port) &&
#if defined(VTSS_SW_OPTION_PTP)
            (clock_source_nomination_config[i].clk_in_port == config->clk_in_port) &&
#endif
            (i != (sourceId - 1)))
        {
            SYNCE_CRIT_EXIT();
            return SYNCE_RC_NOM_PORT;
        }
    }

    /* save this normination in config block */
    clock_source_nomination_config[sourceId - 1].network_port = config->network_port;
#if defined(VTSS_SW_OPTION_PTP)
    clock_source_nomination_config[sourceId - 1].clk_in_port = config->clk_in_port;

    if (port >= SYNCE_PORT_COUNT) {
        clock_alarm_state.locs[sourceId - 1] = (port_state[port].ptsf >= SYNCE_PTSF_LOSS_OF_SYNC) ? true : false;
    }
#endif    

    clock_source_nomination_config[sourceId - 1].ssm_overwrite = config->ssm_overwrite;

    if ((config->aneg_mode == VTSS_APPL_SYNCE_ANEG_NONE) && (clock_source_nomination_config[sourceId - 1].aneg_mode == VTSS_APPL_SYNCE_ANEG_FORCED_SLAVE))
        clock_source_nomination_config[sourceId - 1].aneg_mode = VTSS_APPL_SYNCE_ANEG_PREFERED_SLAVE;
    else
        clock_source_nomination_config[sourceId - 1].aneg_mode = config->aneg_mode;

    clock_source_nomination_config[sourceId - 1].holdoff_time = config->holdoff_time;

    update_clk_in_selector(sourceId - 1);
    if (clock_holdoff_time_set(sourceId - 1, config->holdoff_time*100) != VTSS_OK)    T_D("error returned");

    /* calculate and change priority in clock controller */
    set_clock_priority();

    if (!clock_source_nomination_config[sourceId - 1].nominated)  {SYNCE_CRIT_EXIT(); return (SYNCE_RC_OK);} // Skip updating hardware, since the clock source is not nominated.

    // If here, it means a port has been nominated. If the 

    /* check for change in transmission of SSM */
    set_tx_ssm();

    /* do the WTR stuff */
    set_wtr(sourceId - 1);

    /* calculate and change clock source in phy */
    set_clock_source(sourceId - 1);

    /* check and configure master/slave mode of the nominated sources */
    configure_master_slave();

    SYNCE_CRIT_EXIT();

    // Finally set priority and return error code from this operation.
    return synce_mgmt_nominated_priority_set(sourceId - 1, config->priority);
}

mesa_rc vtss_appl_synce_clock_selection_mode_config_set(const vtss_appl_synce_clock_selection_mode_config_t *const config)
{
    vtss_appl_synce_selection_mode_t selection_mode;
    vtss_appl_synce_clock_selection_mode_status_t status;
    uint                          clock_input, i;
    uint                          clock_type;
    vtss_appl_synce_eec_option_t  my_eec_option;

    T_D("mode = %d, source = %d, wtr_time = %d", config->selection_mode, config->source, config->wtr_time);

    if ((config->source < 1) || (config->source > synce_my_nominated_max)) return(SYNCE_RC_INVALID_PARAMETER);
    if ((clock_selection_mode_config.selection_mode == VTSS_APPL_SYNCE_SELECTOR_MODE_FORCED_FREE_RUN) && (config->selection_mode == VTSS_APPL_SYNCE_SELECTOR_MODE_FORCED_HOLDOVER))    return(SYNCE_RC_INVALID_PARAMETER);
    if ((config->selection_mode == VTSS_APPL_SYNCE_SELECTOR_MODE_MANUEL_TO_SELECTED) &&
        (clock_selection_mode_config.selection_mode != VTSS_APPL_SYNCE_SELECTOR_MODE_MANUEL) &&
        (clock_selection_mode_config.selection_mode != VTSS_APPL_SYNCE_SELECTOR_MODE_AUTOMATIC_NONREVERTIVE) &&
        (clock_selection_mode_config.selection_mode != VTSS_APPL_SYNCE_SELECTOR_MODE_AUTOMATIC_REVERTIVE))    return(SYNCE_RC_INVALID_PARAMETER);
    if (config->wtr_time > 12)    return(SYNCE_RC_INVALID_PARAMETER);

    if (config->eec_option == VTSS_APPL_SYNCE_EEC_OPTION_2) {
        if ((config->ssm_holdover != VTSS_APPL_SYNCE_QL_NONE) && (config->ssm_holdover != VTSS_APPL_SYNCE_QL_PRS) && (config->ssm_holdover != VTSS_APPL_SYNCE_QL_STU) && (config->ssm_holdover != VTSS_APPL_SYNCE_QL_ST2) &&
            (config->ssm_holdover != VTSS_APPL_SYNCE_QL_TNC) && (config->ssm_holdover != VTSS_APPL_SYNCE_QL_ST3E) && (config->ssm_holdover != VTSS_APPL_SYNCE_QL_EEC2) && (config->ssm_holdover != VTSS_APPL_SYNCE_QL_SMC) &&
            (config->ssm_holdover != VTSS_APPL_SYNCE_QL_PROV) && (config->ssm_holdover != VTSS_APPL_SYNCE_QL_DUS))
            return (SYNCE_RC_INVALID_PARAMETER);
    }
    else {
        if ((config->ssm_holdover != VTSS_APPL_SYNCE_QL_NONE) && (config->ssm_holdover != VTSS_APPL_SYNCE_QL_PRC) && (config->ssm_holdover != VTSS_APPL_SYNCE_QL_SSUA) && (config->ssm_holdover != VTSS_APPL_SYNCE_QL_SSUB) &&
            (config->ssm_holdover != VTSS_APPL_SYNCE_QL_EEC1) && (config->ssm_holdover != VTSS_APPL_SYNCE_QL_DNU) && (config->ssm_holdover != VTSS_APPL_SYNCE_QL_INV))
            return (SYNCE_RC_INVALID_PARAMETER);
    }

    if (config->eec_option == VTSS_APPL_SYNCE_EEC_OPTION_2) {
        if ((config->ssm_freerun != VTSS_APPL_SYNCE_QL_NONE) && (config->ssm_freerun != VTSS_APPL_SYNCE_QL_PRS) && (config->ssm_freerun != VTSS_APPL_SYNCE_QL_STU) && (config->ssm_freerun != VTSS_APPL_SYNCE_QL_ST2) &&
            (config->ssm_freerun != VTSS_APPL_SYNCE_QL_TNC) && (config->ssm_freerun != VTSS_APPL_SYNCE_QL_ST3E) && (config->ssm_freerun != VTSS_APPL_SYNCE_QL_EEC2) && (config->ssm_freerun != VTSS_APPL_SYNCE_QL_SMC) &&
            (config->ssm_freerun != VTSS_APPL_SYNCE_QL_PROV) && (config->ssm_freerun != VTSS_APPL_SYNCE_QL_DUS))
            return (SYNCE_RC_INVALID_PARAMETER);
    }
    else {
        if ((config->ssm_freerun != VTSS_APPL_SYNCE_QL_NONE) && (config->ssm_freerun != VTSS_APPL_SYNCE_QL_PRC) && (config->ssm_freerun != VTSS_APPL_SYNCE_QL_SSUA) && (config->ssm_freerun != VTSS_APPL_SYNCE_QL_SSUB) &&
            (config->ssm_freerun != VTSS_APPL_SYNCE_QL_EEC1) && (config->ssm_freerun != VTSS_APPL_SYNCE_QL_DNU) && (config->ssm_freerun != VTSS_APPL_SYNCE_QL_INV))
            return (SYNCE_RC_INVALID_PARAMETER);
    }

    if ((config->eec_option != VTSS_APPL_SYNCE_EEC_OPTION_1) && (config->eec_option != VTSS_APPL_SYNCE_EEC_OPTION_2))
        return (SYNCE_RC_INVALID_PARAMETER);
    (void)synce_mgmt_eec_option_type_get(&clock_type);
    my_eec_option = config->eec_option;
    if ((my_eec_option != VTSS_APPL_SYNCE_EEC_OPTION_1) && (clock_type != 0)) my_eec_option = VTSS_APPL_SYNCE_EEC_OPTION_1;

    clock_input = config->source - 1;

    if (vtss_appl_synce_clock_selection_mode_status_get(&status) != VTSS_OK)    T_D("error returned");

    switch (config->selection_mode)
    {
        case VTSS_APPL_SYNCE_SELECTOR_MODE_MANUEL:
            selection_mode = VTSS_APPL_SYNCE_SELECTOR_MODE_MANUEL;
            break;
        case VTSS_APPL_SYNCE_SELECTOR_MODE_MANUEL_TO_SELECTED:
            if (status.selector_state != VTSS_APPL_SYNCE_SELECTOR_STATE_LOCKED)
                return (SYNCE_RC_SELECTION);   /* NOT possible to make Manuel To Selected if not in locked mode */

            clock_input = status.clock_input;
            selection_mode = VTSS_APPL_SYNCE_SELECTOR_MODE_MANUEL;
            break;
        case VTSS_APPL_SYNCE_SELECTOR_MODE_AUTOMATIC_NONREVERTIVE:
            selection_mode = VTSS_APPL_SYNCE_SELECTOR_MODE_AUTOMATIC_NONREVERTIVE;
            break;
        case VTSS_APPL_SYNCE_SELECTOR_MODE_AUTOMATIC_REVERTIVE:
            selection_mode = VTSS_APPL_SYNCE_SELECTOR_MODE_AUTOMATIC_REVERTIVE;
            break;
        case VTSS_APPL_SYNCE_SELECTOR_MODE_FORCED_HOLDOVER:
            if (status.selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_FREERUN) {
                selection_mode = VTSS_APPL_SYNCE_SELECTOR_MODE_FORCED_FREE_RUN;   /* NOT possible to make forced hold over in free run selector state */
            }
            else {
                selection_mode = VTSS_APPL_SYNCE_SELECTOR_MODE_FORCED_HOLDOVER;
            }
            break;
        case VTSS_APPL_SYNCE_SELECTOR_MODE_FORCED_FREE_RUN:
            selection_mode = VTSS_APPL_SYNCE_SELECTOR_MODE_FORCED_FREE_RUN;
            break;
        default:
            return(SYNCE_RC_INVALID_PARAMETER);
    }

    T_DG(TRACE_GRP_CLOCK, "clock_selection_mode_set  selection_mode %u  clock_input %u", selection_mode, clock_input);
#if defined(VTSS_SW_OPTION_PTP)
    int m = best_master;
    if (m >= 0) {  // NOTE: If one of the PTP 8265.1 sources is selected as best master then set the selection mode to "Forced Holdover" or "Forced Freerun" in the hardware but update the configuration (below) with the configured value.
        vtss_appl_synce_selection_mode_t current_clock_selection_mode;
        if (clock_selection_mode_get(&current_clock_selection_mode) == VTSS_RC_OK) {
            if ((current_clock_selection_mode != VTSS_APPL_SYNCE_SELECTOR_MODE_FORCED_FREE_RUN) && (current_clock_selection_mode != VTSS_APPL_SYNCE_SELECTOR_MODE_FORCED_HOLDOVER)) {
                if (clock_selection_mode_set(VTSS_APPL_SYNCE_SELECTOR_MODE_FORCED_HOLDOVER, clock_input) != VTSS_OK)    T_D("error returned");
            }
        }
    }
    else {
#else
    {
#endif
        if (clock_selection_mode_set(selection_mode, clock_input) != VTSS_OK)    T_D("error returned");
    }

    SYNCE_CRIT_ENTER();
    clock_selection_mode_config.selection_mode = selection_mode;
    clock_selection_mode_config.source = clock_input;
    clock_selection_mode_config.wtr_time = config->wtr_time;

    clock_selection_mode_config.ssm_holdover = config->ssm_holdover;
    clock_selection_mode_config.ssm_freerun = config->ssm_freerun;
    clock_selection_mode_config.eec_option = my_eec_option;
    set_eec_option(my_eec_option);

    for (i=0; i<synce_my_nominated_max; ++i)
    {
        set_wtr(i);                 /* do the WTR stuff */
        set_clock_source(i);    /* calculate and change clock source in phy */
    }

    /* check and configure master/slave mode of the nominated sources */
    configure_master_slave();

    /* calculate and change priority in clock controller */
    set_clock_priority();

    /* check for change in transmission of SSM */
    set_tx_ssm();
    SYNCE_CRIT_EXIT();

    return(SYNCE_RC_OK);
}

mesa_rc synce_mgmt_nominated_priority_set(const uint source, const uint priority)
{
    uint  rc=SYNCE_RC_OK;

    T_D("source = %d, priority = %d", source, priority);

    if ((source >= synce_my_nominated_max) || (priority >= synce_my_priority_max))    return (SYNCE_RC_INVALID_PARAMETER);

    SYNCE_CRIT_ENTER();
    /* save this priority in config block */
    clock_source_nomination_config[source].priority = priority;

    /* calculate and change priority in clock controller */
    set_clock_priority();

    /* check and configure master/slave mode of the nominated sources */
    configure_master_slave();

    /* check for change in transmission of SSM */
    set_tx_ssm();
    SYNCE_CRIT_EXIT();

    return(rc);
}

mesa_rc synce_mgmt_ssm_set(const uint port_no, const bool ssm_enabled)
{
    uint     source;

    T_D("port_no = %d, ssm_enabled = %d", port_no, ssm_enabled);

    if (port_no >= SYNCE_PORT_COUNT)    return (SYNCE_RC_INVALID_PARAMETER);

    SYNCE_CRIT_ENTER();
    port_config[port_no].ssm_enabled = ssm_enabled;

    if (ssm_enabled)
        port_state[port_no].ssm_state = port_state[port_no].link ? SSM_OK : (SSM_LINK_DOWN | SSM_NOT_SEEN);
    else
        port_state[port_no].ssm_state = SSM_OK;

    if (port_is_nominated(port_no, &source))
    {
        clock_alarm_state.ssm[source] = (port_state[port_no].ssm_state != SSM_OK) ? true : false;

        /* calculate and change priority in clock controller */
        set_clock_priority();

        /* check for change in transmission of SSM */
        set_tx_ssm();

        /* do the WTR stuff */
        set_wtr(source);

        /* calculate and change clock source in phy */
        set_clock_source(source);

        /* check and configure master/slave mode of the nominated sources */
        configure_master_slave();
    }

    SYNCE_CRIT_EXIT();

    return(SYNCE_RC_OK);
}

mesa_rc synce_mgmt_wtr_clear_set(const uint     source)     /* nominated source - range is 0 - synce_my_nominated_max */
{
    if (source >= synce_my_nominated_max)    return(SYNCE_RC_INVALID_PARAMETER);

    SYNCE_CRIT_ENTER();
    if (clock_state[source].wtr_timer != 0)
    {
    /* WTR timer running */
        clock_state[source].wtr_timer = 1;
    }
    SYNCE_CRIT_EXIT();

    return(SYNCE_RC_OK);
}

#if defined(VTSS_SW_OPTION_PTP)
int synce_mgmt_get_best_master()
{
    return best_master;
}
#endif

static u32 freq_khz(vtss_appl_synce_frequency_t f)
{
    switch(f)
    {
        case VTSS_APPL_SYNCE_STATION_CLK_DIS:      return 0;
        case VTSS_APPL_SYNCE_STATION_CLK_1544_KHZ: return 1544;
        case VTSS_APPL_SYNCE_STATION_CLK_2048_KHZ: return 2048;
        case VTSS_APPL_SYNCE_STATION_CLK_10_MHZ:   return 10000;
        default:                                   return 0;
    }
}

bool clock_out_range_check(const vtss_appl_synce_frequency_t freq)
{
    bool valid[4][VTSS_APPL_SYNCE_STATION_CLK_MAX] = {{true,true,true,true}, {true, false, true,true}, {true, false, false, false}, {true, false, false, true}};
    uint clock_type;

    if (clock_station_clock_type_get(&clock_type) == VTSS_RC_OK)
        return valid[clock_type][freq];
    else
        return true;
}

bool clock_in_range_check(const vtss_appl_synce_frequency_t freq)
{
    bool valid[4][VTSS_APPL_SYNCE_STATION_CLK_MAX] = {{true,true,true,true}, {true, false, false, true}, {true, false, false, false}, {true,true,true,true}};
    uint clock_type;

    if (clock_station_clock_type_get(&clock_type) == VTSS_RC_OK)
        return valid[clock_type][freq];
    else
        return true;
}

uint synce_mgmt_station_clock_out_set(const vtss_appl_synce_frequency_t freq) /* set the station clock output frequency */
{
    mesa_rc rc = SYNCE_RC_OK;
    SYNCE_CRIT_ENTER();
    if (clock_out_range_check(freq)) {
        /* save this parameter in config block */
        station_clock_config.station_clk_out = freq;
        /* apply configuration */
        rc = clock_station_clk_out_freq_set(freq_khz(freq));
        if (rc != SYNCE_RC_OK) rc = SYNCE_RC_INVALID_PARAMETER;

    } else {
        rc = SYNCE_RC_INVALID_PARAMETER;
    }
    SYNCE_CRIT_EXIT();
    return(rc);
}

uint synce_mgmt_station_clock_out_get(vtss_appl_synce_frequency_t *const freq)
{
    SYNCE_CRIT_ENTER();
    /* get this parameter from config block */
    *freq = station_clock_config.station_clk_out;
    SYNCE_CRIT_EXIT();
    return(SYNCE_RC_OK);
}

uint synce_mgmt_station_clock_in_set(const vtss_appl_synce_frequency_t freq) /* set the station clock input frequency */
{
    mesa_rc rc = SYNCE_RC_OK;
    uint source;
    SYNCE_CRIT_ENTER();
    if (clock_in_range_check(freq)) {
        /* save this parameter in config block */
        station_clock_config.station_clk_in = freq;
        /* apply configuration */
        rc = clock_station_clk_in_freq_set(freq_khz(freq));
        if (rc != SYNCE_RC_OK) rc = SYNCE_RC_INVALID_PARAMETER;
    } else {
        rc = SYNCE_RC_INVALID_PARAMETER;
    }
    for (source = 0; source < synce_my_nominated_max; source++) {
        update_clk_in_selector(source);
    }
    SYNCE_CRIT_EXIT();
    return(rc);
}

uint synce_mgmt_station_clock_in_get(vtss_appl_synce_frequency_t *const freq)
{
    SYNCE_CRIT_ENTER();
    /* get this parameter from config block */
    *freq = station_clock_config.station_clk_in;
    SYNCE_CRIT_EXIT();
    return(SYNCE_RC_OK);
}

uint synce_mgmt_station_clock_type_get(uint *const clock_type)
{
    mesa_rc rc = SYNCE_RC_OK;
    SYNCE_CRIT_ENTER();
    rc = clock_station_clock_type_get(clock_type);
    SYNCE_CRIT_EXIT();
    return (rc);
}


uint synce_mgmt_eec_option_type_get(uint *const eec_type)
{
    mesa_rc rc = SYNCE_RC_OK;
    SYNCE_CRIT_ENTER();
    rc = clock_eec_option_type_get(eec_type);
    SYNCE_CRIT_EXIT();
    return (rc);
}

void _vtss_appl_synce_clock_selection_mode_status_get(const uint clock_input, const vtss_appl_synce_selector_state_t selector_state, vtss_appl_synce_clock_selection_mode_status_t *const status)
{
    // If selector state == freerun and best_master >= 0, it means SyncE is using a PTP clock instance as its source.
    // The source is assumed to be locked unless it is reporting PTSF == Unusable, in which case, it means it is not locked.
    T_DG(TRACE_GRP_DEVELOP, "vvv --- Entering _vtss_appl_synce_clock_selection_mode_status_get --- vvv");
#if defined(VTSS_SW_OPTION_PTP)
    int m = best_master;
    T_DG(TRACE_GRP_DEVELOP, "m = %d", m);
    if (((selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_FREERUN) || (selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_HOLDOVER)) && (m >= 0)) {
        if (port_state[SYNCE_PORT_COUNT + m].ptsf == SYNCE_PTSF_NONE) {
            T_DG(TRACE_GRP_DEVELOP, "1");
            status->selector_state = VTSS_APPL_SYNCE_SELECTOR_STATE_PTP;
            status->lol = VTSS_APPL_SYNCE_LOL_ALARM_STATE_FALSE;
        }
        else if (port_state[SYNCE_PORT_COUNT + m].ptsf == SYNCE_PTSF_LOSS_OF_ANNOUNCE) {
            T_DG(TRACE_GRP_DEVELOP, "2");
            if (vtss_ptp_servo_get_holdover_status(m)) {
                T_DG(TRACE_GRP_DEVELOP, "3");
                status->selector_state = VTSS_APPL_SYNCE_SELECTOR_STATE_HOLDOVER;
            }
            else {
                T_DG(TRACE_GRP_DEVELOP, "4");
                status->selector_state = VTSS_APPL_SYNCE_SELECTOR_STATE_FREERUN;
            }
            status->lol = VTSS_APPL_SYNCE_LOL_ALARM_STATE_TRUE;
        }
        else {
            T_DG(TRACE_GRP_DEVELOP, "5");
            status->selector_state = VTSS_APPL_SYNCE_SELECTOR_STATE_ACQUIRING;
            status->lol = VTSS_APPL_SYNCE_LOL_ALARM_STATE_TRUE;
        }
        status->clock_input = best_master_source;
    }
    else {
        T_DG(TRACE_GRP_DEVELOP, "6");
        if (selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_PTP) {
            T_DG(TRACE_GRP_DEVELOP, "7");
            if (m >= 0) {
                T_DG(TRACE_GRP_DEVELOP, "8");
                status->clock_input = best_master_source;
                status->selector_state = VTSS_APPL_SYNCE_SELECTOR_STATE_PTP;
            }
            else {
                T_DG(TRACE_GRP_DEVELOP, "9");
                status->selector_state = VTSS_APPL_SYNCE_SELECTOR_STATE_HOLDOVER;
            }
        }
        else {
            T_DG(TRACE_GRP_DEVELOP, "10");
            status->clock_input = clock_input;
            status->selector_state = selector_state;
        }
        status->lol = clock_alarm_state.lol;
    }
#else
    status->clock_input = clock_input;
    status->selector_state = selector_state;
    status->lol = clock_alarm_state.lol;
#endif
    status->losx = clock_alarm_state.losx;
    status->dhold = clock_alarm_state.dhold;

    T_DG(TRACE_GRP_DEVELOP, "^^^ --- Returning from _vtss_appl_synce_clock_selection_mode_status_get --- ^^^");
}

mesa_rc vtss_appl_synce_clock_selection_mode_status_get(vtss_appl_synce_clock_selection_mode_status_t *const status)
{
    uint clock_input;
    vtss_appl_synce_selector_state_t selector_state;

    if (clock_selector_compensated_state_get(&clock_input, &selector_state) != VTSS_OK) T_D("error returned");

    SYNCE_CRIT_ENTER();
    _vtss_appl_synce_clock_selection_mode_status_get(clock_input, selector_state, status);
    SYNCE_CRIT_EXIT();

    return SYNCE_RC_OK;
}

mesa_rc vtss_appl_synce_clock_source_nomination_status_get(const uint sourceId, vtss_appl_synce_clock_source_nomination_status_t *const status)
{
    mesa_rc rc = VTSS_RC_OK;
    SYNCE_CRIT_ENTER();
    if ((sourceId >= 1) && (sourceId <= synce_my_nominated_max)) {
        status->locs = clock_alarm_state.locs[sourceId - 1];
        status->fos = clock_alarm_state.fos[sourceId - 1];
        status->ssm = clock_alarm_state.ssm[sourceId - 1];
        status->wtr = clock_alarm_state.wtr[sourceId - 1];
    }
    else rc = VTSS_RC_ERROR;
    SYNCE_CRIT_EXIT();
    return rc;
}

mesa_rc vtss_appl_synce_port_status_get(const vtss_ifindex_t portId, vtss_appl_synce_port_status_t *const status)
{
    u32 v;
    VTSS_RC(synce_network_port_clk_in_port_combo_to_port(portId, 0, &v));

    uint ssm_state;

    T_D("portId = %d", VTSS_IFINDEX_PRINTF_ARG(portId));

    /*lint -save -e685 -e568 */
    if ((v < 0) || (v >= SYNCE_PORT_COUNT)) return (SYNCE_RC_INVALID_PARAMETER);
    /*lint -restore */

    SYNCE_CRIT_ENTER();
    ssm_state = port_state[v].ssm_state;

    if (ssm_state == SSM_OK)
    {
        switch (port_state[v].ssm_rx)
        {
            case SSM_QL_PRS_INV:  status->ssm_rx = (clock_selection_mode_config.eec_option == VTSS_APPL_SYNCE_EEC_OPTION_2) ? VTSS_APPL_SYNCE_QL_PRS : VTSS_APPL_SYNCE_QL_INV; break;
            case SSM_QL_STU:      status->ssm_rx = VTSS_APPL_SYNCE_QL_STU;  break;
            case SSM_QL_PRC:      status->ssm_rx = VTSS_APPL_SYNCE_QL_PRC;  break;
            case SSM_QL_ST2:      status->ssm_rx = VTSS_APPL_SYNCE_QL_ST2;  break;
            case SSM_QL_SSUA_TNC: status->ssm_rx = (clock_selection_mode_config.eec_option == VTSS_APPL_SYNCE_EEC_OPTION_2) ? VTSS_APPL_SYNCE_QL_TNC : VTSS_APPL_SYNCE_QL_SSUA; break;
            case SSM_QL_SSUB:     status->ssm_rx = VTSS_APPL_SYNCE_QL_SSUB; break;
            case SSM_QL_ST3E:     status->ssm_rx = VTSS_APPL_SYNCE_QL_ST3E; break;
            case SSM_QL_EEC2:     status->ssm_rx = VTSS_APPL_SYNCE_QL_EEC2; break;
            case SSM_QL_EEC1:     status->ssm_rx = VTSS_APPL_SYNCE_QL_EEC1; break;
            case SSM_QL_SMC:      status->ssm_rx = VTSS_APPL_SYNCE_QL_SMC;  break;
            case SSM_QL_PROV:     status->ssm_rx = VTSS_APPL_SYNCE_QL_PROV; break;
            default:              status->ssm_rx = (clock_selection_mode_config.eec_option == VTSS_APPL_SYNCE_EEC_OPTION_2) ? VTSS_APPL_SYNCE_QL_DUS : VTSS_APPL_SYNCE_QL_DNU; break;
        }
    }
    else
    {
        if (ssm_state & SSM_LINK_DOWN)              status->ssm_rx = VTSS_APPL_SYNCE_QL_LINK;
        else
        if (ssm_state & (SSM_FAIL | SSM_NOT_SEEN))  status->ssm_rx = VTSS_APPL_SYNCE_QL_FAIL;
        else
        if (ssm_state & SSM_INVALID)                status->ssm_rx = VTSS_APPL_SYNCE_QL_INV;
    }

    switch (port_state[v].ssm_tx)
    {
        case SSM_QL_PRS_INV:  status->ssm_tx = (clock_selection_mode_config.eec_option == VTSS_APPL_SYNCE_EEC_OPTION_2) ? VTSS_APPL_SYNCE_QL_PRS : VTSS_APPL_SYNCE_QL_INV; break;
        case SSM_QL_STU:      status->ssm_tx = VTSS_APPL_SYNCE_QL_STU;  break;
        case SSM_QL_PRC:      status->ssm_tx = VTSS_APPL_SYNCE_QL_PRC;  break;        
        case SSM_QL_ST2:      status->ssm_tx = VTSS_APPL_SYNCE_QL_ST2;  break;
        case SSM_QL_SSUA_TNC: status->ssm_tx = (clock_selection_mode_config.eec_option == VTSS_APPL_SYNCE_EEC_OPTION_2) ? VTSS_APPL_SYNCE_QL_TNC : VTSS_APPL_SYNCE_QL_SSUA; break;
        case SSM_QL_SSUB:     status->ssm_tx = VTSS_APPL_SYNCE_QL_SSUB; break;
        case SSM_QL_ST3E:     status->ssm_tx = VTSS_APPL_SYNCE_QL_ST3E; break;
        case SSM_QL_EEC2:     status->ssm_tx = VTSS_APPL_SYNCE_QL_EEC2; break;
        case SSM_QL_EEC1:     status->ssm_tx = VTSS_APPL_SYNCE_QL_EEC1; break;
        case SSM_QL_SMC:      status->ssm_tx = VTSS_APPL_SYNCE_QL_SMC;  break;
        case SSM_QL_PROV:     status->ssm_tx = VTSS_APPL_SYNCE_QL_PROV; break;
        default:              status->ssm_tx = (clock_selection_mode_config.eec_option == VTSS_APPL_SYNCE_EEC_OPTION_2) ? VTSS_APPL_SYNCE_QL_DUS : VTSS_APPL_SYNCE_QL_DNU; break;
    }

    status->master = port_state[v].master;
    SYNCE_CRIT_EXIT();

    return(SYNCE_RC_OK);
}

mesa_rc vtss_appl_synce_ptp_port_status_get(const uint sourceId, vtss_appl_synce_ptp_port_status_t *const status)
{
#if defined(VTSS_SW_OPTION_PTP)
    vtss_appl_synce_quality_level_t ssm_rx;
    
    /*lint -save -e685 -e568 */
    if (sourceId >= PTP_CLOCK_INSTANCES) return SYNCE_RC_INVALID_PARAMETER;
    /*lint -restore */
    
    SYNCE_CRIT_ENTER();
    switch (port_state[SYNCE_PORT_COUNT + sourceId].ssm_rx) {
        case SSM_QL_PRS_INV:  ssm_rx = VTSS_APPL_SYNCE_QL_INV;  break;
        case SSM_QL_PRC:      ssm_rx = VTSS_APPL_SYNCE_QL_PRC;  break;
        case SSM_QL_SSUA_TNC: ssm_rx = VTSS_APPL_SYNCE_QL_SSUA; break;
        case SSM_QL_SSUB:     ssm_rx = VTSS_APPL_SYNCE_QL_SSUB; break;
        case SSM_QL_EEC2:     ssm_rx = VTSS_APPL_SYNCE_QL_EEC2; break;
        case SSM_QL_EEC1:     ssm_rx = VTSS_APPL_SYNCE_QL_EEC1; break;
        case SSM_QL_PROV:     ssm_rx = VTSS_APPL_SYNCE_QL_NONE; break;
        case SSM_QL_DNU_DUS:  ssm_rx = VTSS_APPL_SYNCE_QL_DNU;  break;
        case SSM_QL_FAIL:     ssm_rx = VTSS_APPL_SYNCE_QL_FAIL; break;
        default:              ssm_rx = VTSS_APPL_SYNCE_QL_FAIL; break;
    }
    status->ssm_rx = ssm_rx;
    status->ptsf = port_state[SYNCE_PORT_COUNT + sourceId].ptsf;
    SYNCE_CRIT_EXIT();

    return SYNCE_RC_OK;
#else
    return SYNCE_RC_NOT_SUPPORTED;
#endif
}

mesa_rc vtss_appl_synce_clock_source_nomination_config_get(uint sourceId, vtss_appl_synce_clock_source_nomination_config_t *const config)
{
    /*lint -save -e685 -e568 */
    if ((sourceId >= 1) && (sourceId <= synce_my_nominated_max)) {
        /*lint -restore */
        SYNCE_CRIT_ENTER();
        *config = clock_source_nomination_config[sourceId - 1];
        SYNCE_CRIT_EXIT();

        return VTSS_RC_OK;
    }
    else {
        return VTSS_RC_ERROR;
    }
}

mesa_rc vtss_synce_clock_source_nomination_config_all_get(vtss_appl_synce_clock_source_nomination_config_t *const config)
{
    SYNCE_CRIT_ENTER();
    memcpy(config, clock_source_nomination_config, SYNCE_NOMINATED_MAX*sizeof(vtss_appl_synce_clock_source_nomination_config_t));
    SYNCE_CRIT_EXIT();
    return VTSS_RC_OK;
}

mesa_rc vtss_appl_synce_clock_selection_mode_config_get(vtss_appl_synce_clock_selection_mode_config_t *const config)
{
    mesa_rc rc = VTSS_RC_OK;

    SYNCE_CRIT_ENTER();
    *config = clock_selection_mode_config;
    config->source++;
    SYNCE_CRIT_EXIT();

    return rc;
}

mesa_rc vtss_appl_synce_station_clock_config_set(const vtss_appl_synce_station_clock_config_t *const config)
{
    if (synce_mgmt_station_clock_in_set(config->station_clk_in) != SYNCE_RC_OK ||
        synce_mgmt_station_clock_out_set(config->station_clk_out) != SYNCE_RC_OK)
    {
        return VTSS_RC_ERROR;
    }
    else {
        return VTSS_RC_OK;
    }
}

mesa_rc vtss_appl_synce_station_clock_config_get(vtss_appl_synce_station_clock_config_t *const config)
{
    SYNCE_CRIT_ENTER();
    *config = station_clock_config;
    SYNCE_CRIT_EXIT();

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_synce_port_config_set(const vtss_ifindex_t portId, const vtss_appl_synce_port_config_t *const config)
{
    u32 v;
    VTSS_RC(synce_network_port_clk_in_port_combo_to_port(portId, 0, &v));

    /*lint -save -e685 -e568 */
    if ((v >= 0) && (v <= SYNCE_PORT_COUNT-1)) {
        /*lint -restore */
        return synce_mgmt_ssm_set(v, config->ssm_enabled);
    }
    else {
        return VTSS_RC_ERROR;
    }
}

mesa_rc vtss_appl_synce_port_config_get(const vtss_ifindex_t portId, vtss_appl_synce_port_config_t *const config)
{
    u32 v;
    VTSS_RC(synce_network_port_clk_in_port_combo_to_port(portId, 0, &v));

    /*lint -save -e685 -e568 */
    if ((v >= 0) && (v <= SYNCE_PORT_COUNT-1)) {
        /*lint -restore */
        SYNCE_CRIT_ENTER();
        *config = port_config[v];
        SYNCE_CRIT_EXIT();

        return VTSS_RC_OK;
    }
    else {
        return VTSS_RC_ERROR;
    }
}

uint synce_mgmt_register_get(const uint reg, uint *const  value)
{
    if (clock_read(reg, value) != VTSS_OK)    T_D("error returned");
    return(SYNCE_RC_OK);
}

uint synce_mgmt_register_set(const uint reg, const uint value)
{
    if (clock_write(reg, value) != VTSS_OK)    T_D("error returned");
    return(SYNCE_RC_OK);
}

#if defined(VTSS_SW_OPTION_PTP)
mesa_rc vtss_synce_ptp_port_state_get(const uint idx, synce_ptp_port_state_t *state) 
{
    mesa_rc rc = VTSS_RC_ERROR;
    
    SYNCE_PTSF_CRIT_ENTER();
    if (idx < PTP_CLOCK_INSTANCES) {
        state->ssm_rx = port_state[SYNCE_PORT_COUNT + idx].ssm_rx;
        state->ptsf = port_state[SYNCE_PORT_COUNT + idx].ptsf;
        rc = VTSS_RC_OK;
    }
    SYNCE_PTSF_CRIT_EXIT();
    return rc;
}
#endif

#if defined(VTSS_SW_OPTION_PTP)
mesa_rc vtss_synce_ptp_clock_ssm_ql_set(const uint idx, u8 clockClass) {
    uint ssm_rx;
    
    SYNCE_PTSF_CRIT_ENTER();
    switch(clockClass) {
        case 6 : ssm_rx = SSM_QL_PRC; break;
        case 80 : ssm_rx = SSM_QL_PRS_INV; break;
        case 82 : ssm_rx = SSM_QL_STU; break;
        case 84 : ssm_rx = SSM_QL_PRC; break;
        case 86 : ssm_rx = SSM_QL_ST2; break;
        case 90 : ssm_rx = SSM_QL_SSUA_TNC; break;
        case 96 : ssm_rx = SSM_QL_SSUB; break;
        case 100 : ssm_rx = SSM_QL_ST3E; break;
        case 102 : ssm_rx = SSM_QL_EEC2; break;
        case 104 : ssm_rx = SSM_QL_EEC1; break;
        case 106 : ssm_rx = SSM_QL_SMC; break;
        case 108 : ssm_rx = SSM_QL_PROV; break;
        case 110 : ssm_rx = SSM_QL_DNU_DUS; break;
        default: ssm_rx = SSM_QL_FAIL; break;
    }
    ptsf_state[idx].ssm_frame = ssm_rx;
    ptsf_state[idx].new_ssm = true;
    vtss_flag_setbits(&func_wait_flag, FLAG_FRAME_RX);
    SYNCE_PTSF_CRIT_EXIT();
    return VTSS_RC_OK;
}

const char *vtss_sync_ptsf_state_2_txt(vtss_appl_synce_ptp_ptsf_state_t s)
{
    switch (s) {
        case SYNCE_PTSF_NONE  : return "PTSF_NONE";
        case SYNCE_PTSF_UNUSABLE: return "PTSF_UNUSABLE";
        case SYNCE_PTSF_LOSS_OF_SYNC: return "PTSF_LOSS_OF_SYNC";
        case SYNCE_PTSF_LOSS_OF_ANNOUNCE: return "PTSF_LOSS_OF_ANNOUNCE";
        default: return "PTSF_UNKNOWN";
    }
}

mesa_rc vtss_synce_ptp_clock_ptsf_state_set(const uint idx, vtss_appl_synce_ptp_ptsf_state_t ptsfState) {
    SYNCE_PTSF_CRIT_ENTER();

    ptsf_state[idx].first_port_callback = true;

    if (((ptsf_state[idx].ptsf == SYNCE_PTSF_LOSS_OF_ANNOUNCE) && (ptsfState != SYNCE_PTSF_LOSS_OF_ANNOUNCE)) ||
        ((ptsf_state[idx].ptsf != SYNCE_PTSF_LOSS_OF_ANNOUNCE) && (ptsfState == SYNCE_PTSF_LOSS_OF_ANNOUNCE)))
    {
        ptsf_state[idx].new_link = true;
    }

    if (ptsf_state[idx].ptsf != ptsfState)
    {
        ptsf_state[idx].new_ptsf = true;
        vtss_flag_setbits(&func_wait_flag, FLAG_PTSF_CHANGE);
    }

    if (ptsf_state[idx].ptsf != ptsfState) {
        T_I("ptsf_state[%d].ptsf changed to %s", idx, vtss_sync_ptsf_state_2_txt(ptsfState));
    }
    
    ptsf_state[idx].ptsf = ptsfState;

    if (ptsf_state[idx].new_link) {
        ptsf_state[idx].link = (ptsfState == SYNCE_PTSF_LOSS_OF_ANNOUNCE) ? false : true;
        vtss_flag_setbits(&func_wait_flag, FLAG_PORT_CALL_BACK);
    }
    if (ptsf_state[idx].new_link) {
        T_I("ptsf_state[%d].link changed to %d", idx, ptsf_state[idx].link);
    }

    SYNCE_PTSF_CRIT_EXIT();
    return VTSS_RC_OK;
}

mesa_rc vtss_synce_ptp_clock_hybrid_mode_set(BOOL hybrid)
{
    SYNCE_PTSF_CRIT_ENTER();
    my_hybrid_mode = hybrid;
    T_I("hybrid mode changed to %s", hybrid ? "TRUE" : "FALSE");
    SYNCE_PTSF_CRIT_EXIT();
    return VTSS_RC_OK;

}

#endif

/******************************************************************************
 * Description: Set frequency adjust value
 *
 ******************************************************************************/
mesa_rc synce_mgmt_clock_adjtimer_set(i64 adj)
{
    mesa_rc rc = SYNCE_RC_OK;
    rc = clock_adjtimer_set(adj);
    if (rc == VTSS_RC_ERROR) rc = SYNCE_RC_NOT_SUPPORTED;
    return rc;
}

mesa_rc synce_mgmt_clock_ql_get(vtss_appl_synce_quality_level_t *ql)
{
    mesa_rc rc = SYNCE_RC_OK;
    *ql = current_ql;
    return rc;
}

/******************************************************************************
 * Description: Make phase adjust
 *
 ******************************************************************************/
mesa_rc synce_mgmt_clock_locked_ql_get(mesa_port_no_t port, vtss_appl_synce_quality_level_t *ql)
{
    mesa_rc rc = SYNCE_RC_OK;
    SYNCE_CRIT_ENTER();
    if (current_selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_LOCKED && port == current_selected_port) {
        *ql = current_ql;
    } else {
        *ql = VTSS_APPL_SYNCE_QL_DNU;
    }
    SYNCE_CRIT_EXIT();
    return rc;
}

extern "C" void vtss_synce_json_init();
extern "C" int synce_icli_cmd_register();

void vtss_appl_synce_reset_configuration_to_default()
{
    conf_blk_t save_blk;

    synce_set_clock_source_nomination_config_to_default(save_blk.clock_source_nomination_config);
    synce_set_clock_selection_mode_config_to_default(&save_blk.clock_selection_mode_config);
    synce_set_station_clock_config_to_default(&save_blk.station_clock_config);
    synce_set_port_config_to_default(save_blk.port_config.data());
    
    apply_configuration(&save_blk);
}

/* Initialize module */
mesa_rc synce_init(vtss_init_data_t *data)
{
    u32                i;
    mesa_rc            rc;
    vtss_isid_t        isid = data->isid;
    port_iter_t        pit;

// Note: The code installing the ssm_frame_rx packet filter callback has been moved to func_thread and delayed 20 seconds from startup since the Linux system
//       seems to be occupying the SPI IF for extended periods during startup up. This causes SyncE / Zarlink API to wait for access while holding a lock
//       that prevents the ssm_frame_rx routine from running causing further problems.
//
// #ifdef VTSS_SW_OPTION_PACKET
//     packet_rx_filter_t filter;
//     void               *filter_id;
// #endif

    /*lint --e{454,456} */

    if (data->cmd == INIT_CMD_EARLY_INIT) {
        /* Initialize and register trace ressources */
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);
    }

    T_D("enter, cmd: %d, isid: %u, flags: 0x%x", data->cmd, data->isid, data->flags);

    switch (data->cmd)
    {
        case INIT_CMD_INIT:
            T_D("INIT");
            if (MESA_CAP(MESA_CAP_CLOCK)) {
                vtss_synce_json_init();
            }

            /* initialize critd */
            critd_init(&crit, "SyncE Crit", VTSS_MODULE_ID_SYNCE, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
            critd_init(&crit_ptsf, "SyncE PTSF Crit", VTSS_MODULE_ID_SYNCE, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);

            vtss_flag_init(&func_wait_flag);

            if (synce_dpll) {
                synce_my_nominated_max = synce_dpll->clock_my_input_max;      // FIXME: Make sure that the synce_dpll clock_init function is called before this happens or the values will be undefined.
                synce_my_priority_max = synce_dpll->clock_my_input_max;       //        This means that in main.c synce_dpll needs not be initialized before synce
                synce_my_prio_disabled = synce_dpll->synce_my_prio_disabled;  //
            } else {
                synce_my_nominated_max = CLOCK_INPUT_MAX - 1;                 // FIXME: This is really a hack to prevent a seg-fault in case no DPLL is detected.
                synce_my_priority_max = CLOCK_INPUT_MAX - 1;                  //        We should handle this case in a better way since when no DPLL is present, synce is not supported at all.
                synce_my_prio_disabled = CLOCK_INPUT_MAX - 1;                 //
            }

            vtss_thread_create(VTSS_THREAD_PRIO_DEFAULT,
                              func_thread,
                              0,
                              "SYNCE Function",
                              nullptr,
                              0,
                              &func_thread_handle,
                              &func_thread_block);
            SYNCE_CRIT_EXIT();
            SYNCE_PTSF_CRIT_EXIT();

#ifdef VTSS_SW_OPTION_ICFG
        // Initialize ICLI "show running" configuration
        VTSS_RC(synce_icfg_init());
#endif
#ifdef VTSS_SW_OPTION_PRIVATE_MIB
        vtss_synce_mib_init();
#endif
#ifdef VTSS_SW_OPTION_JSON_RPC
        vtss_appl_synce_json_init();
#endif
        synce_icli_cmd_register();

            break;
        case INIT_CMD_START:
            T_D("START");
            for (i=0; i<synce_my_nominated_max; ++i)
            {
                phy_clock_config[i].squelch = MESA_PHY_CLK_SQUELCH_MAX;
                phy_clock_config[i].src = MESA_PHY_CLK_DISABLED;
                clock_state[i].holdoff = false;
                clock_state[i].new_locs = false;
                clock_state[i].new_fos = false;
                clock_state[i].wtr_state = WTR_OK;
                clock_state[i].wtr_timer = 0;
                clock_alarm_state.locs[i] = false;
                clock_alarm_state.fos[i] = false;
                clock_alarm_state.ssm[i] = false;
                clock_alarm_state.wtr[i] = false;
            }
            clock_alarm_state.losx = false;
            clock_alarm_state.lol = VTSS_APPL_SYNCE_LOL_ALARM_STATE_FALSE;

            for (i=0; i<SYNCE_PORT_COUNT + PTP_CLOCK_INSTANCES; ++i) {
                port_state[i].new_ssm = false;
                port_state[i].new_link = false;
                port_state[i].new_fiber = false;
                port_state[i].ssm_rx = SSM_QL_FAIL;
                port_state[i].ssm_tx = SSM_QL_EEC1;
                port_state[i].ssm_count = 0;
                port_state[i].ssm_state = SSM_FAIL;
                port_state[i].fiber = false;
                port_state[i].master = true;
                port_state[i].link = false;
                port_state[i].first_port_callback = false;
                port_state[i].prefer_timer = 0;
#if defined(VTSS_SW_OPTION_PTP)
                port_state[i].ptsf = SYNCE_PTSF_LOSS_OF_ANNOUNCE;  // NOTE: Only used by PTP clock instances
                port_state[i].new_ptsf = false;
#endif
            }
#if defined(VTSS_SW_OPTION_PTP)
            for (i=0; i<PTP_CLOCK_INSTANCES; ++i) {
                ptsf_state[i].new_link = false;
                ptsf_state[i].link = false;
                ptsf_state[i].new_ptsf = false;
                ptsf_state[i].first_port_callback = false;
                ptsf_state[i].ptsf = SYNCE_PTSF_LOSS_OF_ANNOUNCE;
                ptsf_state[i].ssm_frame = SSM_QL_FAIL;
                ptsf_state[i].new_ssm = false;
            }
#endif

            VTSS_RC(synce_board_init());

            clock_old_selector_state = (vtss_appl_synce_selector_state_t)0xFF;

            synce_set_clock_source_nomination_config_to_default(clock_source_nomination_config);
            synce_get_clock_selection_mode_config_default(&clock_selection_mode_config);
            synce_set_station_clock_config_to_default(&station_clock_config);
            synce_set_port_config_to_default(port_config.data());

            break;
        case INIT_CMD_CONF_DEF:
            T_D("CONF_DEF");
            if (isid == VTSS_ISID_GLOBAL) {              
                vtss_appl_synce_reset_configuration_to_default();
            }
            break;
        case INIT_CMD_MASTER_UP:
            T_D("MASTER_UP");

            port_iter_init_local(&pit);
            while (port_iter_getnext(&pit)) {
                i = pit.iport;
                T_I("port %d mux_selector[0] = %d, mux_selector[1] = %d", i, synce_get_mux_selector(0, i), synce_get_mux_selector(1, i));
            }
            T_I("port %d mux_selector[0] = %d, mux_selector[1] = %d", SYNCE_STATION_CLOCK_PORT, synce_get_mux_selector(0, SYNCE_STATION_CLOCK_PORT), synce_get_mux_selector(1, SYNCE_STATION_CLOCK_PORT));
            port_iter_init_local(&pit);
            while (port_iter_getnext(&pit)) {
                i = pit.iport;
                T_I("port %d source[0] = %d, source[1] = %d, source[2] = %d",i, synce_get_source_port(0, i), synce_get_source_port(1, i), synce_get_source_port(2, i));
            }
            port_iter_init_local(&pit);
            while (port_iter_getnext(&pit)) {
                i = pit.iport;
                T_I("port %d switch_rcvrd_clock[0] = %d, switch_rcvrd_clock[1] = %d, switch_rcvrd_clock[2] = %d",i, synce_get_switch_recovered_clock(0, i), synce_get_switch_recovered_clock(1, i), synce_get_switch_recovered_clock(2, i));
            }
            port_iter_init_local(&pit);
            while (port_iter_getnext(&pit)) {
                i = pit.iport;
                T_I("port %d phy_rcvrd_clock[0] = %d, phy_rcvrd_clock[1] = %d, phy_rcvrd_clock[2] = %d",i, synce_get_phy_recovered_clock(0, i), synce_get_phy_recovered_clock(1, i), synce_get_phy_recovered_clock(2, i));
            }
            
            port_iter_init_local(&pit);
            while (port_iter_getnext(&pit)) {
                i = pit.iport;
                T_I("port %d ref[0] = %d, ref[1] = %d, ref[2] = %d",i, synce_get_selector_ref_no(0, i), synce_get_selector_ref_no(1, i), synce_get_selector_ref_no(2, i));
                
                T_I("port %d, freq (via ref[0]) = %d, freq (via ref[0]) = %d, freq (via ref[0]) = %d", i, synce_get_rcvrd_clock_frequency(0, i, port_state[i].speed), synce_get_rcvrd_clock_frequency(1, i, port_state[i].speed), synce_get_rcvrd_clock_frequency(2, i, port_state[i].speed));

            }
            T_I("port %d ref[0] = %d, ref[1] = %d, ref[2] = %d", SYNCE_STATION_CLOCK_PORT, synce_get_selector_ref_no(0, SYNCE_STATION_CLOCK_PORT),
                                                                                           synce_get_selector_ref_no(1, SYNCE_STATION_CLOCK_PORT),
                                                                                           synce_get_selector_ref_no(2, SYNCE_STATION_CLOCK_PORT));

            for (i=0; i<SYNCE_PORT_COUNT; ++i)
                port_state[i].phy = port_phy(i);

            // This will wait until the PHYs are initialized.
            port_phy_wait_until_ready();

            /* hook up on port change */
            if ((rc = port_change_register(VTSS_MODULE_ID_SYNCE, port_change_callback)) != VTSS_OK)    T_D("error returned rc %u", rc);

// Note: The code installing the ssm_frame_rx packet filter callback has been moved to func_thread and delayed 20 seconds from startup since the Linux system
//       seems to be occupying the SPI IF for extended periods during startup up. This causes SyncE / Zarlink API to wait for access while holding a lock
//       that prevents the ssm_frame_rx routine from running causing further problems.
//
// #ifdef VTSS_SW_OPTION_PACKET
//             /* hook up on SSM frame */
//             packet_rx_filter_init(&filter);
//             filter.modid = VTSS_MODULE_ID_SYNCE;
//             filter.match = PACKET_RX_FILTER_MATCH_DMAC | PACKET_RX_FILTER_MATCH_ETYPE;
//             filter.cb    = ssm_frame_rx;
//             filter.prio  = PACKET_RX_FILTER_PRIO_NORMAL;
//             filter.etype = 0x8809; // slow protocol ethertype
//             memcpy(filter.dmac, ssm_dmac, sizeof(filter.dmac));
//
//             if ((rc = packet_rx_filter_register(&filter, &filter_id)) != VTSS_OK)    T_D("error returned rc %u", rc);
// #endif
            control_system_reset_register(system_reset);

            masterLock.lock(false); // unlock masterLock to allow thread to run
            break;
        case INIT_CMD_MASTER_DOWN:
            T_D("MASTER_DOWN");
            break;
        case INIT_CMD_SWITCH_ADD:
            T_D("SWITCH_ADD");
            break;
        case INIT_CMD_SWITCH_DEL:
            T_D("SWITCH_DEL");
            break;
        case INIT_CMD_SUSPEND_RESUME:
            T_D("SUSPEND_RESUME");
            break;
        default:
            break;
    }

    T_D("exit");
    return 0;
}

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
