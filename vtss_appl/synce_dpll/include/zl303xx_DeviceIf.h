

/*******************************************************************************
*
*  $Id: zl303xx_DeviceIf.h 13649 2016-06-22 20:06:43Z RF $
*
*  Copyright 2006-2016 Microsemi Semiconductor Limited.
*  All rights reserved.
*
*  Module Description:
*     This file contains functions related to the ...
*
*******************************************************************************/

#ifndef _ZL303XX_DEVICE_IF_H_
#define _ZL303XX_DEVICE_IF_H_

#if defined __cplusplus
extern "C" {
#endif


/*****************   INCLUDE FILES   ******************************************/
#include "zl303xx_Global.h"

/*****************   DEFINES   ************************************************/

/*****************   DATA TYPES   *********************************************/

/* Device family defines */
typedef enum
{
    UNKNOWN_DEVICETYPE = 0,
    ZL3031X_DEVICETYPE,
    ZL30330_DEVICETYPE,
    ZL3034X_DEVICETYPE,
    ZL3036X_DEVICETYPE,
    ZL3075X_DEVICETYPE,
    ZL3072X_DEVICETYPE,
    ZL3070X_DEVICETYPE,
    CUSTOM_DEVICETYPE
} zl303xx_DevTypeE;


/* DeviceId valid values */
typedef enum
{
    UNKNOWN_DEV_TYPE = 0,
    ZL30161_DEV_TYPE = 0xA1,
    ZL30162_DEV_TYPE = 0xA2,
    ZL30163_DEV_TYPE = 0xA3,
    ZL30164_DEV_TYPE = 0xA4,
    ZL30165_DEV_TYPE = 0xA5,
    ZL30166_DEV_TYPE = 0xA6,
    ZL30167_DEV_TYPE = 0xA7,
    ZL30168_DEV_TYPE = 0x14,    /* Corrected in EBinder 14522 */
    ZL30361_DEV_TYPE = 0x3D,
    ZL30362_DEV_TYPE = 0x3E,
    ZL30363_DEV_TYPE = 0x3F,
    ZL30364_DEV_TYPE = 0x40,
    ZL30365_DEV_TYPE = 0x41,
    ZL30366_DEV_TYPE = 0x42,
    ZL30367_DEV_TYPE = 0x43,

    ZL303XX_DEVICE_ID_ZL30250 = 0x00,
    ZL303XX_DEVICE_ID_ZL30251 = 0x01,
    ZL303XX_DEVICE_ID_ZL30252 = 0x02,
    ZL303XX_DEVICE_ID_ZL30253 = 0x03,
    ZL303XX_DEVICE_ID_ZL30244 = 0x04,
    ZL303XX_DEVICE_ID_ZL30151 = 0x05,
    ZL303XX_DEVICE_ID_ZL30245 = 0x06,
    ZL303XX_DEVICE_ID_ZL30169 = 0x07,
    ZL303XX_DEVICE_ID_ZL30182 = 0x08,
    ZL303XX_DEVICE_ID_ZL30621 = 0x09,
    ZL303XX_DEVICE_ID_ZL30622 = 0x0A,
    ZL303XX_DEVICE_ID_ZL30623 = 0x0B,
    ZL303XX_DEVICE_ID_ZL30255 = 0x0E, /* End of real ones! */
    /* Encoded with rev 2 so these are FAKE IDS we use within the s/w */
    ZL303XX_DEVICE_ID_ZL30721 = 0x29, /* Rev 2, ID 0x9 */
    ZL303XX_DEVICE_ID_ZL30722 = 0x2A, /* Rev 2, ID 0xA */
    ZL303XX_DEVICE_ID_ZL30723 = 0x2B,  /* Rev 2, ID 0xB */

    ZL303XX_DEVICE_ID_ZL30174 = 0x0c66,
    ZL303XX_DEVICE_ID_ZL30611 = 0x0e1b,
    ZL303XX_DEVICE_ID_ZL30612 = 0x0e1c,
    ZL303XX_DEVICE_ID_ZL30614 = 0x0e1e,
    ZL303XX_DEVICE_ID_ZL30601 = 0x0e11,
    ZL303XX_DEVICE_ID_ZL30602 = 0x0e12,
    ZL303XX_DEVICE_ID_ZL30603 = 0x0e13,
    ZL303XX_DEVICE_ID_ZL30604 = 0x0e14,
    ZL303XX_DEVICE_ID_ZL30701 = 0x0e75,
    ZL303XX_DEVICE_ID_ZL30702 = 0x0e76,
    ZL303XX_DEVICE_ID_ZL30703 = 0x0e77,
    ZL303XX_DEVICE_ID_ZL30704 = 0x0e78,
    ZL303XX_DEVICE_ID_ZL80029 = 0x1e5d

} zl303xx_DeviceIdE;


typedef enum
{
   ZL303XX_REF_AUTO = -1,
   ZL303XX_REF_ID_0 = 0,
   ZL303XX_REF_ID_1 = 1,
   ZL303XX_REF_ID_2 = 2,
   ZL303XX_REF_ID_3 = 3,
   ZL303XX_REF_ID_4 = 4,
   ZL303XX_REF_ID_5 = 5,
   ZL303XX_REF_ID_6 = 6,
   ZL303XX_REF_ID_7 = 7,
   ZL303XX_REF_ID_8 = 8,
   ZL303XX_REF_ID_9 = 9,
   ZL303XX_REF_ID_10= 10,
   ZL303XX_LAST_REF=ZL303XX_REF_ID_10
} zl303xx_RefIdE;


/* DpllId valid values */
typedef enum
{
    ZL303XX_DPLL_ID_1 = 0,
    ZL303XX_DPLL_ID_2,
    ZL303XX_DPLL_ID_3,
    ZL303XX_DPLL_ID_4
} zl303xx_DpllIdE;


/* Driver functions */
typedef enum {
    ZL303XX_DPLL_DRIVER_MSG_GET_DEVICE_INFO                               = 0,
    ZL303XX_DPLL_DRIVER_MSG_TSXHGF                                        = 1,
    ZL303XX_DPLL_DRIVER_MSG_SET_FREQ                                      = 2,
    ZL303XX_DPLL_DRIVER_MSG_TSXHHWT                                       = 3,
    ZL303XX_DPLL_DRIVER_MSG_TSXHHWR                                       = 4,
    ZL303XX_DPLL_DRIVER_MSG_SET_TIME                                      = 5,
    ZL303XX_DPLL_DRIVER_MSG_STEP_TIME                                     = 6,
    ZL303XX_DPLL_DRIVER_MSG_JUMP_TIME_CGU                                 = 7,
    ZL303XX_DPLL_DRIVER_MSG_ADJUST_IF_HITLESS_COMPENSATION_BEING_USED     = 8,
    ZL303XX_DPLL_DRIVER_MSG_GET_HW_MANUAL_FREERUN_STATUS                  = 9,
    ZL303XX_DPLL_DRIVER_MSG_GET_HW_MANUAL_HOLDOVER_STATUS                 = 10,
    ZL303XX_DPLL_DRIVER_MSG_GET_HW_SYNC_INPUT_EN_STATUS                   = 11,
    ZL303XX_DPLL_DRIVER_MSG_GET_HW_OUT_OF_RANGE_STATUS                    = 12,
    ZL303XX_DPLL_DRIVER_MSG_NCO_STEP_TIME_PHASE_SET                       = 13,
    ZL303XX_DPLL_DRIVER_MSG_DEVICE_PARAMS_INIT                            = 14,
    ZL303XX_DPLL_DRIVER_MSG_CURRENT_REF_GET                               = 15,
    ZL303XX_DPLL_DRIVER_MSG_CURRENT_REF_SET                               = 16,
    ZL303XX_DPLL_DRIVER_MSG_FREQ_DETECT_GET                               = 17,
    ZL303XX_DPLL_DRIVER_MSG_CHECK_REF_SYNC_PAIR                           = 18,
    ZL303XX_DPLL_DRIVER_MSG_SET_ACTIVE_ELEC_REF_HITLESS_ACTIONS           = 19,
    ZL303XX_DPLL_DRIVER_MSG_GET_TRANSIENT_HITLESS_COMPENSATION_PARAMS     = 20,
    ZL303XX_DPLL_DRIVER_MSG_GET_USER_HITLESS_COMPENSATION_PARAMS          = 21,
    ZL303XX_DPLL_DRIVER_MSG_SET_PHASE_SLOPE_LIMIT                         = 22,
    ZL303XX_DPLL_DRIVER_MSG_REF_SWITCH_HITLESS_COMPENSATION               = 23,
    ZL303XX_DPLL_DRIVER_MSG_NCO_STEP_TIME_PHASE_GET                       = 24,
    ZL303XX_DPLL_DRIVER_MSG_TSXHTW                                        = 25,
    ZL303XX_DPLL_DRIVER_MSG_TSXHTWSG                                      = 26,
    ZL303XX_DPLL_DRIVER_MSG_TSXHTWSTS                                     = 27,
    ZL303XX_DPLL_DRIVER_MSG_TSXHTWCS                                      = 28,
    ZL303XX_DPLL_DRIVER_MSG_TSXHPSSG                                      = 29,
    ZL303XX_DPLL_DRIVER_MSG_TSXHPSSC                                      = 30,
    ZL303XX_DPLL_DRIVER_MSG_TSXHNRSCS                                     = 31,
    ZL303XX_DPLL_DRIVER_MSG_GET_SYNTH_MASK                                = 32,
    ZL303XX_DPLL_DRIVER_MSG_REGISTER_TOD_DONE_FUNC                        = 33,
    ZL303XX_DPLL_DRIVER_MSG_CONFIRM_HW_CNTRL                              = 34,
    ZL303XX_DPLL_DRIVER_MSG_GET_HW_LOCK_STATUS                            = 35,
    ZL303XX_DPLL_DRIVER_MSG_GET_STEP_TIME_CURR_MAX_STEP_SIZE              = 36,
    ZL303XX_DPLL_DRIVER_MSG_TSXHTR                                        = 37,
    ZL303XX_DPLL_DRIVER_MSG_72X_CLEAR_HOLDOVER_FFO                        = 38,
    ZL303XX_DPLL_DRIVER_MSG_SET_AFBDIV                                    = 40,
    ZL303XX_DPLL_DRIVER_MSG_72X_DETERMINE_MAX_STEP_SIZE_PER_ADJUSTMENT    = 42
/*  ZL303XX_DPLL_DRIVER_MSG_ADJUST_TIME                                   = xx   */
} zl303xx_DpllDriverMsgTypesE;


/* Data structure passed to and received from the given driver.
   - in data  = data into the driver
   - out data = data out of the driver
*/
/* Set freq data */
typedef struct {
    Sint32T freqOffsetInPpt;
} zl303xx_SetFreqInDataS;

/* Get freq data */
typedef struct {
    Sint32T memPart;
} zl303xx_GetFreqInDataS;
typedef struct {
    Sint32T freqOffsetInPpt;
} zl303xx_GetFreqOutDataS;

/* Get HW lock status data */
typedef struct {
    Sint32T lockStatus;
} zl303xx_GetHWLockStatusOutDataS;

/* SetTime */
typedef struct {
    Uint32T pllId;
    Uint64S seconds;
    Uint32T nanoSeconds;
    zl303xx_BooleanE bBackwardAdjust;
} zl303xx_SetTimeInDataS;

/* stepTime data */
typedef struct {
    Uint32T pllId;
    Sint32T deltaTime;
    zl303xx_BooleanE inCycles;
    Uint32T clockFreq;
} zl303xx_StepTimeInDataS;

/* JumpTimeCGU */
typedef struct {
    Uint32T pllId;
    Uint64S seconds;
    Uint32T nanoSeconds;
    zl303xx_BooleanE bBackwardAdjust;
} zl303xx_JumpTimeCGUInDataS;

/* AdjustIfHitlessCompensationBeingUsed */
typedef struct {
    Uint32T hitlessType;
} zl303xx_AdjustIfHitlessCompensationBeingUsedInDataS;
typedef struct {
    zl303xx_BooleanE apply;
} zl303xx_AdjustIfHitlessCompensationBeingUsedOutDataS;

/* Get HW manual freerun status data */
typedef struct {
    Sint32T status;
} zl303xx_GetHWManualFreerunOutDataS;

/* Get HW manual holdover status data */
typedef struct {
    Sint32T status;
} zl303xx_GetHWManualHoldoverOutDataS;

/* Get HW sync output enabled status data */
typedef struct {
    Sint32T status;
} zl303xx_GetHWSyncInputEnOutDataS;

/* Get HW out-of-range status data */
typedef struct {
    Sint32T status;
} zl303xx_GetHWOutOfRangeOutDataS;

/* set NCO phase data */
typedef struct {
    Uint32T pllId;
    Sint32T phaseNs;
} zl303xx_NcoStepTimePhaseSetInDataS;

/* device initialisation data */
typedef struct {
    void *hwParams;
} zl303xx_DeviceInitInDataS;

/* Get current ref data */
typedef struct {
    Uint32T ref;
} zl303xx_GetCurrRefOutDataS;

/* Set ref data */
typedef struct {
    Uint32T ref;
} zl303xx_SetCurrRefInDataS;

/* ref freq detect data */
typedef struct {
    Sint32T ref;
} zl303xx_GetFreqDetectInDataS;
typedef struct {
    Uint32T freq;
} zl303xx_GetFreqDetectOutDataS;

/* Check Ref Sync pair data */
typedef struct {
    Sint32T ref;
} zl303xx_CheckRefSyncPairInDataS;
typedef struct {
    Uint32T syncId;
    zl303xx_BooleanE bRefSyncPair;
} zl303xx_CheckRefSyncPairOutDataS;

/* Set active elec ref actions data */
typedef struct {
    Sint32T ref;
} zl303xx_SetActiveElecRefHitlessActionsInDataS;

/* Get transient hitless compensation data */
typedef struct {
    Uint32T PhaseSlopeLimit;
    Uint32T Bandwidth;
    Uint32T CustomBandwidth;
    zl303xx_BooleanE FastLock;
    Uint32T DelayPeriod;
    Uint32T RefSyncDelayPeriod;
} zl303xx_GetTransientHitlessCompensationParamsOutDataS;

/* Get user hitless compensation params data */
typedef struct {
    Uint32T PhaseSlopeLimit;
    Uint32T Bandwidth;
    Uint32T CustomBandwidth;
    zl303xx_BooleanE FastLock;
} zl303xx_GetUserHitlessCompensationParamsOutDataS;

/* Set phase slope limit data */
typedef struct {
    Sint32T psl;
} zl303xx_SetPhaseSlopeLimitInDataS;

/* Apr's ref switch hitless compensation data */
typedef struct {
    Uint32T pllId;
    zl303xx_BooleanE apply;
} zl303xxrefSwitchHitlessCompensationInDataS;

/* get NCO phase data */
typedef struct {
    Uint32T pllId;
} zl303xx_GetNcoStepTimePhaseInDataS;
typedef struct {
    Sint32T phaseNs;
} zl303xx_GetNcoStepTimePhaseOutDataS;

/* TsxhTw data */
typedef struct {
    Uint32T tieNs;
} zl303xx_TsxhTwInDataS;

/* TsxhTwsg data */
typedef struct {
    zl303xx_BooleanE ready;
} zl303xx_TsxhTwsgOutDataS;

/* TsxhTw data */
typedef struct {
    Uint32T threshNs;
} zl303xx_TsxhTwstsInDataS;

/* TsxhTwcs data */
typedef struct {
    Uint32T tieWrClear;
} zl303xx_TsxhTwcsInDataS;

/* TsxhPssg data */
typedef struct {
    Uint32T synthId;
    Uint32T postDivBitmap;
} zl303xx_TsxhPssgInDataS;
typedef struct {
    Uint32T completeBitmap;
} zl303xx_TsxhPssgOutDataS;

/* TsxhPssc data */
typedef struct {
    Uint32T synthId;
    Uint32T postDivBitmap;
} zl303xx_TsxhPsscInDataS;

/* TsxhNrscs data */
typedef struct {
    zl303xx_BooleanE fastLock;
} zl303xx_TsxhNrscsInDataS;

/* Get synth mask data */
typedef struct {
    Uint32T pllId;
} zl303xx_GetSynthMaskInDataS;
typedef struct {
    Uint32T synthMask;
} zl303xx_GetSynthMaskOutDataS;

/* Set TOD done callback function data */
typedef Sint32T (*hwFuncPtrTODDone)(void*);
typedef struct {
   hwFuncPtrTODDone TODdoneFuncPtr;
} zl303xx_SetTODDoneFuncInDataS;

/* confirm Hw Cntrl data */
typedef struct {
    Uint32T addr;
} zl303xx_ConfirmHwCntrlInDataS;
typedef struct {
    Uint32T data;
} zl303xx_ConfirmHwCntrlOutDataS;

/* get device info */
typedef struct {
    zl303xx_DevTypeE devType;
    zl303xx_DeviceIdE devId;
    Uint32T devRev;
} zl303xx_GetDeviceInfoOutDataS;

/* get step time current max step size from 70x */
typedef struct {
    Sint32T size;
} zl303xx_GetStepTimeCurrMaxStepSizeOutDataS;

/* TsxhTr */
typedef struct {
    Uint32T timeout;
} zl303xx_TsxhTrInDataS;
typedef struct {
    Sint32T tieNs;
} zl303xx_TsxhTrOutDataS;

/* Set AFBDIV */
typedef struct {
    Sint32T df;
} zl303xx_SetAFBDIVInDataS;


/* determine the max step size that the  */
typedef struct {
    Uint32T maxStepFaster;
    Uint32T maxStepSlower;
} zl303xx_DetermineMaxStepSizePerAdjustmentOutDataS;


typedef struct {
    zl303xx_DpllDriverMsgTypesE dpllMsgType;
    union {
        zl303xx_SetFreqInDataS setFreq;
        zl303xx_GetFreqInDataS getFreq;
        zl303xx_SetTimeInDataS setTime;
        zl303xx_StepTimeInDataS stepTime;
        zl303xx_JumpTimeCGUInDataS jumpTimeCGU;
        zl303xx_AdjustIfHitlessCompensationBeingUsedInDataS aihcbu;
        zl303xx_NcoStepTimePhaseSetInDataS NcoStepTimePhaseSet;
        zl303xx_DeviceInitInDataS deviceInit;
        zl303xx_SetCurrRefInDataS setCurrRef;
        zl303xx_GetFreqDetectInDataS getFreqDetect;
		zl303xx_CheckRefSyncPairInDataS checkRefSyncPair;
        zl303xx_SetActiveElecRefHitlessActionsInDataS saerha;
        zl303xx_SetPhaseSlopeLimitInDataS setPhaseSlopeLimit;
        zl303xxrefSwitchHitlessCompensationInDataS refSwitchHitlessCompensation;
        zl303xx_GetNcoStepTimePhaseInDataS getNcoStepTimePhase;
        zl303xx_TsxhTwInDataS tsxhTw;
        zl303xx_TsxhTwstsInDataS tsxhTwsts;
        zl303xx_TsxhTwcsInDataS tsxhTwcs;
        zl303xx_TsxhPssgInDataS tsxhPssg;
        zl303xx_TsxhPsscInDataS tsxhPssc;
        zl303xx_TsxhNrscsInDataS tsxhNrscs;
        zl303xx_GetSynthMaskInDataS getSynthMask;
        zl303xx_SetTODDoneFuncInDataS setTODDoneFunc;
        zl303xx_ConfirmHwCntrlInDataS confirmHwCntrl;
        zl303xx_TsxhTrInDataS tsxhTr;
        zl303xx_SetAFBDIVInDataS setAFBDIV;
    } d;
} zl303xx_DriverMsgInDataS;

typedef struct {
    zl303xx_DpllDriverMsgTypesE dpllMsgType;
    union {
        zl303xx_GetFreqOutDataS getFreq;
        zl303xx_GetHWLockStatusOutDataS getHWLockStatus;
        zl303xx_AdjustIfHitlessCompensationBeingUsedOutDataS aihcbu;
        zl303xx_GetHWManualFreerunOutDataS getHWManualFreerun;
        zl303xx_GetHWManualHoldoverOutDataS getHWManualHoldover;
        zl303xx_GetHWSyncInputEnOutDataS getHWSyncInputEn;
        zl303xx_GetHWOutOfRangeOutDataS getHWOutOfRange;
        zl303xx_GetCurrRefOutDataS getCurrRef;
        zl303xx_GetFreqDetectOutDataS getFreqDetect;
        zl303xx_CheckRefSyncPairOutDataS checkRefSyncPair;
        zl303xx_GetTransientHitlessCompensationParamsOutDataS gthcp;
        zl303xx_GetUserHitlessCompensationParamsOutDataS guhcp;
        zl303xx_GetNcoStepTimePhaseOutDataS getNcoStepTimePhase;
        zl303xx_TsxhTwsgOutDataS tsxhTwsg;
        zl303xx_TsxhPssgOutDataS tsxhPssg;
        zl303xx_GetSynthMaskOutDataS getSynthMask;
        zl303xx_ConfirmHwCntrlOutDataS confirmHwCntrl;
        zl303xx_GetDeviceInfoOutDataS getDeviceInfo;
        zl303xx_GetStepTimeCurrMaxStepSizeOutDataS getStepTimeCurrMaxStepSize;
        zl303xx_TsxhTrOutDataS tsxhTr;
        zl303xx_DetermineMaxStepSizePerAdjustmentOutDataS determineMaxStepSizePerAdjustment;
    } d;
} zl303xx_DriverMsgOutDataS;


/* Driver callout function type. A routine of this type is provided by each
   driver and passed to APR at initialisation. APR uses this callout to access
   driver functions. */
typedef int (*hwFuncPtrDriverMsgRouter)(void*, void*, void*);

/* Shared types between APR and the drivers */
/* 1) Lock status */
typedef enum
{
   ZL303XX_LOCK_STATUS_ACQUIRING = 0,
   ZL303XX_LOCK_STATUS_LOCKED,
   ZL303XX_LOCK_STATUS_PHASE_LOCKED,
   ZL303XX_LOCK_STATUS_HOLDOVER,
   ZL303XX_LOCK_STATUS_REF_FAILED,
   ZL303XX_LOCK_NO_ACTIVE_SERVER,
   ZL303XX_LOCK_STATUS_UNKNOWN,
   ZL303XX_APRLOCKSTATE_MAX = ZL303XX_LOCK_STATUS_UNKNOWN
} zl303xx_AprLockStatusE;

#define  zl303xx_LockStatusE zl303xx_AprLockStatusE

/* 2) Holdover Quality */
typedef enum {
    HOLDOVER_QUALITY_UNKNOWN        = 0,
    HOLDOVER_QUALITY_IN_SPEC,
    HOLDOVER_QUALITY_LOCKED_TO_SYNCE,
    HOLDOVER_QUALITY_OUT_OF_SPEC,

    HOLDOVER_QUALITY_LAST = HOLDOVER_QUALITY_OUT_OF_SPEC
} zl303xx_HoldoverQualityTypesE;

/* 3) Hitless compensation types */
typedef enum
{
   ZL303XX_HITLESS_COMP_FALSE,
   ZL303XX_HITLESS_COMP_TRUE,
   ZL303XX_HITLESS_COMP_AUTO
} zl303xx_HitlessCompE;


/*****************   EXPORTED GLOBAL VARIABLE DECLARATIONS   ******************/

/* Global data used for device identification */
extern zl303xx_DevTypeE       globalDevType;

/*****************   EXTERNAL FUNCTION DECLARATIONS   *************************/

/* Device identification routines */
zl303xx_DevTypeE zl303xx_GetDefaultDeviceType(void);
unsigned int zl303xx_SetDefaultDeviceType(zl303xx_DevTypeE f);



#if defined __cplusplus
}
#endif

#endif /* MULTIPLE INCLUDE BARRIER */
