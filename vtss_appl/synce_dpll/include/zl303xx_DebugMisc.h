

/*******************************************************************************
*
*  $Id: zl303xx_DebugMisc.h 11354 2014-11-06 21:38:39Z DP $
*
*  Copyright 2006-2016 Microsemi Semiconductor Limited.
*  All rights reserved.
*
*  Module Description:
*     This is the header file for the accessing routines in zl303xx_DebugMisc.c.
*
*******************************************************************************/

#ifndef _ZL303XX_DEBUGMISC_H
#define _ZL303XX_DEBUGMISC_H

#ifdef __cplusplus
extern "C" {
#endif

#include "zl303xx_Global.h"
#include "zl303xx.h"

zlStatusE zl303xx_DebugHwRefStatus(zl303xx_ParamsS *zl303xx_Params, Uint32T refId);
zlStatusE zl303xx_DebugDpllStatus(zl303xx_ParamsS *zl303xx_Params, Uint32T dpllId);
zlStatusE zl303xx_DebugPllStatus(zl303xx_ParamsS *zl303xx_Params);
zlStatusE zl303xx_DebugHwRefCfg(zl303xx_ParamsS *zl303xx_Params, Uint32T refId);
zlStatusE zl303xx_DebugDpllConfig(zl303xx_ParamsS *zl303xx_Params, Uint32T dpllId);

void zl303xx_DebugApiBuildInfo(void);


#ifdef __cplusplus
}
#endif

#endif /* _ZL303XX_DEBUGMISC_H */
