

/*******************************************************************************
*
*  $Id: zl303xx.h 13982 2016-08-16 20:37:02Z RF $
*  Copyright 2006-2016 Microsemi Semiconductor Limited.
*  All rights reserved.
*
*  Module Description:
*     High level header file defining components of the zl303xx API.
*
*******************************************************************************/

#ifndef ZL303XX_MAIN_H
#define ZL303XX_MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/*****************   INCLUDE FILES   ******************************************/

/* This should always be the first file included */
#include "zl303xx_Global.h"

/* Now include essential files */
#include "zl303xx_Error.h"
#include "zl303xx_Trace.h"
#include "zl303xx_Macros.h"
#include "zl303xx_DeviceSpec.h"

#if defined __cplusplus
}
#endif

#endif   /* MULTIPLE INCLUDE BARRIER */

