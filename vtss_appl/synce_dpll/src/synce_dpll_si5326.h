/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.


*/

#if !defined(_SYNCE_DPLL_SI5326_H)
#define _SYNCE_DPLL_SI5326_H

#include "synce_dpll_base.h"

#define SPI_FREE_RUN_REG        0
#define SPI_CK_PRIOR_REG        1
#define SPI_BWSEL_REG           2
#define SPI_CKSEL_REG           3
#define SPI_DHOLD_REG           3
#define SPI_AUTOSEL_REG         4
#define SPI_N31_REG            43
#define SPI_N32_REG            46
#define SPI_CLK_ACTV_REG      128
#define SPI_LOS_INT_REG       129
#define SPI_LOSX_INT_REG      129
#define SPI_LOL_INT_REG       130
#define SPI_FOS_INT_REG       130
#define SPI_DIGHOLDVALID_REG  130
#define SPI_ICAL_REG          136
#define SPI_FASTLOCK_REG      137

#define CLOCK_LOS1_MASK              0x02
#define CLOCK_LOS2_MASK              0x04
#define CLOCK_FOS1_MASK              0x02
#define CLOCK_FOS2_MASK              0x04
#define CLOCK_LOSX_MASK              0x01
#define CLOCK_LOL_MASK               0x01

struct synce_dpll_si5326 : synce_dpll_base {
    virtual void control_selector(void);
    virtual mesa_rc clock_selector_state_get(uint *const clock_input, vtss_appl_synce_selector_state_t *const selector_state);
    virtual mesa_rc clock_selection_mode_set(const vtss_appl_synce_selection_mode_t mode, const uint clock_input);
    virtual mesa_rc clock_priority_set(const uint clock_input, const uint priority);
    virtual mesa_rc clock_priority_get(const uint clock_input, uint *const priority);
    virtual mesa_rc clock_locs_state_get(const uint clock_input, bool *const state);
    virtual mesa_rc clock_fos_state_get(const uint clock_input, bool *const state);
    virtual mesa_rc clock_losx_state_get(bool *const state);
    virtual mesa_rc clock_lol_state_get(bool *const state);
    virtual mesa_rc clock_dhold_state_get(bool *const state);
    virtual mesa_rc clock_event_poll(bool interrupt, clock_event_type_t *ev_mask);
    virtual mesa_rc clock_event_enable(clock_event_type_t ev_mask);
    virtual mesa_rc clock_station_clk_out_freq_set(const u32 freq_khz);
    virtual mesa_rc clock_station_clk_in_freq_set(const u32 freq_khz);
    virtual mesa_rc clock_eec_option_set(const clock_eec_option_t clock_eec_option);
    virtual mesa_rc clock_eec_option_type_get(uint *const eec_type);
    virtual mesa_rc clock_adjtimer_set(i64 adj);
    virtual mesa_rc clock_adjtimer_enable(bool enable);
    virtual mesa_rc clock_hardware_id_get(meba_synce_clock_hw_id_t *const clock_hw_id);
    virtual mesa_rc clock_selector_map_set(const uint reference, const uint clock_input);
    virtual mesa_rc clock_holdoff_time_set(const uint clock_input, const uint ho_time);
    virtual mesa_rc clock_holdoff_event(const uint clock_input);
    virtual mesa_rc clock_holdoff_run(bool *const active);
    virtual mesa_rc clock_holdoff_active_get(const uint clock_input, bool *const active);
    virtual mesa_rc clock_startup(bool cold_init, bool pcb104_synce);
    virtual mesa_rc clock_frequency_set(const uint clock_input, const clock_frequency_t frequency);
    virtual mesa_rc clock_init(bool cold_init, void **device_ptr);
    virtual mesa_rc clock_station_clock_type_get(uint *const clock_type);
    virtual mesa_rc clock_features_get(sync_clock_feature_t *features);
    virtual mesa_rc clock_adj_phase_set(i32 adj);
    virtual mesa_rc clock_output_adjtimer_set(i64 adj);
    virtual mesa_rc clock_ptp_timer_source_set(ptp_clock_source_t source);
    virtual mesa_rc clock_read(const uint reg, uint *const value);
    virtual mesa_rc clock_write(const uint reg, const uint value);
    virtual mesa_rc clock_writemasked(const uint reg, const uint value, const uint mask);
    virtual vtss_zl_30380_dpll_type_t dpll_type() { return VTSS_ZL_30380_DPLL_GENERIC; }
    virtual int clock_input2ref_id(int source);
protected:
    vtss_handle_t thread_handle_clock;
    vtss_thread_t thread_block_clock;

    static bool hold_over_on_free_run_clock;
    static bool clock_thread_stop;
    static vtss::Lock freerunLock;                                                                 // Lock to keep state of freerun / not freerun
    static const u16 init_dac;
    static void locked_mode(const uint clock_input, const clock_frequency_t frequency);
    static void clock_thread(vtss_addrword_t data);
    void free_run_mode(void);
    bool vtss_dac_clock_set_adjtimer(i64 adj);
    bool vtss_dac_clock_init_adjtimer(bool cold);
};

#endif // _SYNCE_DPLL_SI5326_H
