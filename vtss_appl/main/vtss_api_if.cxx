/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

 $Revision$

*/

#include <endian.h>
#include <asm/byteorder.h>
#include <sys/mman.h>
#include <dirent.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>  /* I2C support */
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <string.h>

#include "main.h"
#include "main_conf.hxx"
#include "port_api.h"
#include "board_if.h"
#include "vtss_api_if_api.h"
#include "critd_api.h"
#include "conf_api.h"
#include "misc_api.h"
#include "vtss_os_wrapper.h"
#include "vtss_linux_frameio.hxx"
#include "board_subjects.hxx"
#include "port_api.h"
#include "l2proto_api.h"
#if defined(VTSS_SW_OPTION_VLAN)
#include "vlan_api.h"
#endif
#if defined(VTSS_SW_OPTION_AFI)
#include "afi_api.h"
#endif
#if defined(VTSS_SW_OPTION_QOS)
#include "qos_api.h"
#endif
#if defined(VTSS_SW_OPTION_DOT1X)
#include "dot1x_api.h"
#endif
#ifdef VTSS_SW_OPTION_PSEC
#include "psec_api.h"
#endif







#ifdef VTSS_SW_OPTION_AGGR
#include "aggr_api.h"
#endif

#if defined(VTSS_SW_OPTION_LLDP)
#include "lldp_api.h"
#endif
#ifdef VTSS_SW_OPTION_LLDP_MED
#include "lldpmed_shared.h"
#endif

#ifdef VTSS_SW_OPTION_IPMC
#include "ipmc_lib_porting_defs.h"
#endif

#ifdef VTSS_SW_OPTION_PTP
#include "ptp_constants.h"
#endif

#if defined(VTSS_SW_OPTION_ZLS30387)
#include "zl_3038x_api_pdv_api.h"
#endif

#if defined(VTSS_PERSONALITY_STACKABLE)
#include "topo_api.h"
#endif
#if defined(VTSS_SW_OPTION_CE_MAX)
#include "ce_max_api.h"
#endif /* VTSS_SW_OPTION_CE_MAX */




#if defined(MSCC_BRSDK)
#include "packet_api.h" /* For packet_ufdma_trace_update() */
#endif /* defined(MSCC_BRSDK) */

#ifdef VTSS_SW_OPTION_MEP
#include "mep_api.h"
#endif

#if (__BYTE_ORDER == __BIG_ENDIAN)
#define PCIE_HOST_CVT(x) __builtin_bswap32((x))  /* PCIe is LE - we're BE, so swap */
#define CPU_HTONL(x)     (x)                     /* We're network order already */
#else
#define PCIE_HOST_CVT(x) (x)                     /* We're LE already */
#define CPU_HTONL(x)     __builtin_bswap32(x)    /* LE to network order */
#endif

#ifdef __cplusplus
VTSS_ENUM_INC(mesa_trace_group_t);
#endif /* #ifdef __cplusplus */

#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_API_AI /* Can't distinguish between AIL and CIL */

/* API semaphore */
static critd_t vtss_api_crit;

/* Mutex for get-modify-set API operations */
static critd_t vtss_appl_api_crit;

// PUBLIC (used by vtss_linux_frameio.cxx)
volatile uint32_t *base_mem;

#define STR(s) #s
#if defined(SW_OPTION_UIO_DEVICE)
 #define SWITCH_DRIVER STR(SW_OPTION_UIO_DEVICE)
#else
 #define SWITCH_DRIVER STR(mscc_switch)
#endif

/* Board information and instance data*/
static meba_board_interface_t board_info;
meba_inst_t board_instance;

#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_API_AI
#define TRACE_GRP_CNT        (MESA_TRACE_GROUP_COUNT + 1)

#if (VTSS_TRACE_ENABLED)
static vtss_trace_reg_t trace_reg_ail =
{
    VTSS_MODULE_ID_API_AI, "api_ail", "VTSS API - Application Interface Layer"
};

static vtss_trace_reg_t trace_reg_cil =
{
    VTSS_MODULE_ID_API_CI, "api_cil", "VTSS API - Chip Interface Layer"
};

/* Default trace level */
#define VTSS_API_DEFAULT_TRACE_LVL VTSS_TRACE_LVL_ERROR
static vtss_trace_grp_t trace_grps_ail[TRACE_GRP_CNT] =
{
    /* MESA_TRACE_GROUP_DEFAULT */ {
        "default",
        "Default",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* MESA_TRACE_GROUP_PORT */ {
        "port",
        "Port",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* MESA_TRACE_GROUP_PHY */ {
        "phy",
        "PHY",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* MESA_TRACE_GROUP_PACKET */ {
        "packet",
        "Packet",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* MESA_TRACE_GROUP_AFI */ {
        "afi",
        "AFI",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* MESA_TRACE_GROUP_QOS */ {
        "qos",
        "QoS",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* MESA_TRACE_GROUP_L2 */ {
        "l2",
        "Layer 2",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* MESA_TRACE_GROUP_L3 */ {
        "l3",
        "Layer 3",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* MESA_TRACE_GROUP_SECURITY */ {
        "security",
        "Security",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* MESA_TRACE_GROUP_EVC */ {
        "evc",
        "EVC",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* MESA_TRACE_GROUP_FDMA_NORMAL */ {
        "fdma",
        "FDMA when scheduler/interrupts is/are enabled",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* MESA_TRACE_GROUP_FDMA_IRQ */ {
        "fdma_irq",
        "FDMA when scheduler/interrupts is/are disabled",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_USEC|VTSS_TRACE_FLAGS_RINGBUF|VTSS_TRACE_FLAGS_IRQ
    },

    /* MESA_TRACE_GROUP_REG_CHECK */ {
        "reg_check",
        "Register Access Checks (level = error)",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_USEC|VTSS_TRACE_FLAGS_RINGBUF|VTSS_TRACE_FLAGS_IRQ
    },

    /* MESA_TRACE_GROUP_MPLS */ {
        "mpls",
        "MPLS/MPLS-TP",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },

    /* MESA_TRACE_GROUP_HW_PROT */ {
        "hwprot",
        "Hardware Protection",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },

    /* MESA_TRACE_GROUP_HQOS */ {
        "hqos",
        "Hierarchical Quality of Service",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },

    /* MESA_TRACE_GROUP_MACSEC */ {
        "macsec",
        "MacSec",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },

    /* MESA_TRACE_GROUP_VCAP */ {
        "vcap",
        "VCAP",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },

    /* MESA_TRACE_GROUP_OAM */ {
        "oam",
        "OAM",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },

    /* MESA_TRACE_GROUP_TS */ {
        "ts",
        "Timestamp",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },

    /* MESA_TRACE_GROUP_CLOCK */ {
        "clock",
        "SyncE clock api",
        VTSS_API_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },

    /* CRITD trace group, MUST be last */
    /* TRACE_GRP_CNT - 1 */ {
        "crit",
        "Critical regions ",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP,
    }
};

static vtss_trace_grp_t trace_grps_cil[TRACE_GRP_CNT];

/* Using function name is more informative than file name in this case */
#define API_CRIT_ENTER(function) critd_enter(&vtss_api_crit,      MESA_TRACE_GROUP_COUNT,   VTSS_TRACE_LVL_NOISE, function,    0)
#define API_CRIT_EXIT(function)  critd_exit( &vtss_api_crit,      MESA_TRACE_GROUP_COUNT,   VTSS_TRACE_LVL_NOISE, function,    0)
#define APPL_API_CRIT_ENTER()    critd_enter(&vtss_appl_api_crit, MESA_TRACE_GROUP_DEFAULT, VTSS_TRACE_LVL_NOISE, __FUNCTION__, __LINE__)
#define APPL_API_CRIT_EXIT()     critd_exit( &vtss_appl_api_crit, MESA_TRACE_GROUP_DEFAULT, VTSS_TRACE_LVL_NOISE, __FUNCTION__, __LINE__)
#else
#define API_CRIT_ENTER(function) critd_enter(&vtss_api_crit)
#define API_CRIT_EXIT(function)  critd_exit( &vtss_api_crit)
#define APPL_API_CRIT_ENTER()    critd_enter(&vtss_appl_api_crit)
#define APPL_API_CRIT_EXIT()     critd_exit( &vtss_appl_api_crit)
#endif /* VTSS_TRACE_ENABLED */

/* Globally exposed variable! */
CapArray<meba_port_entry_t, MESA_CAP_PORT_CNT> port_custom_table;

/* Port info inherited from port module */
struct {
    mesa_port_no_t stack_a, stack_b;
} port = {
    VTSS_PORT_NO_NONE, VTSS_PORT_NO_NONE,
};

meba_port_cap_t vtss_board_port_cap(mesa_port_no_t port_no)
{
    return port_custom_table[port_no].cap;
}

/* ================================================================= *
 *  API lock/unlock
 * ================================================================= */

void mesa_callout_lock(const mesa_api_lock_t *const lock)
{
    API_CRIT_ENTER(lock->function);
}

void mesa_callout_unlock(const mesa_api_lock_t *const lock)
{
    API_CRIT_EXIT(lock->function);
}

void vtss_appl_api_lock(void)
{
    APPL_API_CRIT_ENTER();
}

void vtss_appl_api_unlock(void)
{
    APPL_API_CRIT_EXIT();
}

/* ================================================================= *
 *  Trace
 * ================================================================= */

#if (VTSS_TRACE_ENABLED)
/* Convert API trace level to application trace level */
static int api2appl_level(mesa_trace_level_t level)
{
    int lvl;

    switch (level) {
    case MESA_TRACE_LEVEL_NONE:
        lvl = VTSS_TRACE_LVL_NONE;
        break;
    case MESA_TRACE_LEVEL_ERROR:
        lvl = VTSS_TRACE_LVL_ERROR;
        break;
    case MESA_TRACE_LEVEL_INFO:
        lvl = VTSS_TRACE_LVL_INFO;
        break;
    case MESA_TRACE_LEVEL_DEBUG:
        lvl = VTSS_TRACE_LVL_DEBUG;
        break;
    case MESA_TRACE_LEVEL_NOISE:
        lvl = VTSS_TRACE_LVL_NOISE;
        break;
    default:
        lvl = VTSS_TRACE_LVL_RACKET; /* Should never happen */
        break;
    }
    return lvl;
}

/* Convert application trace level to API trace level */
static mesa_trace_level_t appl2api_level(int lvl)
{
    mesa_trace_level_t level;

    switch (lvl) {
    case VTSS_TRACE_LVL_ERROR:
    case VTSS_TRACE_LVL_WARNING:
        level = MESA_TRACE_LEVEL_ERROR;
        break;
    case VTSS_TRACE_LVL_INFO:
        level = MESA_TRACE_LEVEL_INFO;
        break;
    case VTSS_TRACE_LVL_DEBUG:
        level = MESA_TRACE_LEVEL_DEBUG;
        break;
    case VTSS_TRACE_LVL_NOISE:
    case VTSS_TRACE_LVL_RACKET:
        level = MESA_TRACE_LEVEL_NOISE;
        break;
    case VTSS_TRACE_LVL_NONE:
        level = MESA_TRACE_LEVEL_NONE;
        break;
    default:
        level = MESA_TRACE_LEVEL_ERROR; /* Should never happen */
        break;
    }
    return level;
}

static int api2appl_module_id(const mesa_trace_layer_t layer)
{
    return (layer == MESA_TRACE_LAYER_AIL ? trace_reg_ail.module_id : trace_reg_cil.module_id);
}
#endif /* VTSS_TRACE_ENABLED */

/* Trace callout function */
void mesa_callout_trace_printf(const mesa_trace_layer_t layer,
                               const mesa_trace_group_t group,
                               const mesa_trace_level_t level,
                               const char *file,
                               const int line,
                               const char *function,
                               const char *format,
                               va_list args)
{
#if (VTSS_TRACE_ENABLED)
    int     module_id = api2appl_module_id(layer);
    int     lvl = api2appl_level(level);
    vtss_trace_vprintf(module_id, group, lvl, function, line, format, args);
#endif /* VTSS_TRACE_ENABLED */
}

/* Trace hex-dump callout function */
void mesa_callout_trace_hex_dump(const mesa_trace_layer_t layer,
                                 const mesa_trace_group_t group,
                                 const mesa_trace_level_t level,
                                 const char               *file,
                                 const int                line,
                                 const char               *function,
                                 const unsigned char      *byte_p,
                                 const int                byte_cnt)
{
#if (VTSS_TRACE_ENABLED)
    int module_id = api2appl_module_id(layer);
    int lvl = api2appl_level(level);

    /* Map API trace to WebStaX trace */
    vtss_trace_hex_dump(module_id, group, lvl, function, line, byte_p, byte_cnt);
#endif /* VTSS_TRACE_ENABLED */
}


/* Called when module trace levels have been changed */
static void vtss_api_trace_update_do(BOOL call_ufdma)
{
#if VTSS_TRACE_ENABLED
    int               grp;
    mesa_trace_conf_t conf;
    int               global_lvl = vtss_trace_global_lvl_get();

    for (grp = 0; grp < MESA_TRACE_GROUP_COUNT; grp++) {
        /* Map WebStaX trace level to API trace level */
        conf.level[MESA_TRACE_LAYER_AIL] = global_lvl > trace_grps_ail[grp].lvl ? appl2api_level(global_lvl) : appl2api_level(trace_grps_ail[grp].lvl);
        conf.level[MESA_TRACE_LAYER_CIL] = global_lvl > trace_grps_cil[grp].lvl ? appl2api_level(global_lvl) : appl2api_level(trace_grps_cil[grp].lvl);
        mesa_trace_conf_set((mesa_trace_group_t)grp, &conf);
    }

#if defined(MSCC_BRSDK)
    if (call_ufdma) {
        // The uFDMA piggy-backs on the normal trace system, but is
        // completely decoupled from the API trace, because it is
        // a standalone component. Just call it to update itself.
        packet_ufdma_trace_update();
    }
#endif /* defined(MSCC_BRSDK) */
#endif /* VTSS_TRACE_ENABLED */
}

void vtss_api_trace_update(void)
{
    vtss_api_trace_update_do(TRUE);
}

/* ================================================================= *
 *  I2C
 * ================================================================= */

#define I2C_PORT2DEV(p) (100 + p)

/**
 * \brief Function for doing i2c reads from the switch i2c controller
 *
 * \param port_no [IN] Port number
 * \param i2c_addr [IN] I2C device address
 * \param addr [IN]   Register address
 * \param data [OUT]  Pointer the register(s) data value.
 * \param cnt [IN]    Number of registers to read
 *
 * \return Return code.
 **/
mesa_rc i2c_read(const mesa_port_no_t port_no, const u8 i2c_addr, const u8 addr, u8 *const data, const u8 cnt)
{
    int file;
    mesa_rc rc = VTSS_RC_ERROR;
    if ((file = vtss_i2c_adapter_open(50, i2c_addr)) >= 0) {
        struct i2c_rdwr_ioctl_data packets;
        struct i2c_msg messages[2];

        // Write portion
        messages[0].addr  = i2c_addr;
        messages[0].flags = 0;
        messages[0].len   = 1;
        *data = addr;    // (Re-)Use the read buffer for the address write
        messages[0].buf   = data;

        // Read portion
        messages[1].addr  = i2c_addr;
        messages[1].flags = I2C_M_RD /* | I2C_M_NOSTART*/;
        messages[1].len   = cnt;
        messages[1].buf   = data;

        /* Transfer the i2c packets to the kernel and verify it worked */
        packets.msgs  = messages;
        packets.nmsgs = ARRSZ(messages);
        if(ioctl(file, I2C_RDWR, &packets) < 0) {
            T_I("I2C transfer failed: %s, port_no: %u, i2c_addr: %u, addr: %u, cnt: %u", strerror(errno), port_no, i2c_addr, addr, cnt);
        } else {
            rc = VTSS_OK;
        }
        close(file);
    }
    T_D("i2c read port %d, addr 0x%x, %d bytes - RC %d", port_no, i2c_addr, cnt, rc);
    return rc;
}

/**
 * \brief Function for doing i2c reads from the switch i2c controller
 *
 * \param port_no [IN] Port number
 * \param i2c_addr [IN] I2C device address
 * \param data [OUT]  Pointer the register(s) data value.
 * \param cnt [IN]    Number of registers to read
 *
 * \return Return code.
 **/
mesa_rc i2c_write(const mesa_port_no_t port_no, const u8 i2c_addr, u8 *const data, const u8 cnt)
{
    int file;
    mesa_rc rc = VTSS_RC_ERROR;
    if ((file = vtss_i2c_adapter_open(50, i2c_addr)) >= 0) {
        struct i2c_rdwr_ioctl_data packets;
        struct i2c_msg messages[1];

        // Write portion
        messages[0].addr  = i2c_addr;
        messages[0].flags = 0;
        messages[0].len   = cnt;
        messages[0].buf   = data;

        /* Transfer the i2c packets to the kernel and verify it worked */
        packets.msgs  = messages;
        packets.nmsgs = ARRSZ(messages);
        if(ioctl(file, I2C_RDWR, &packets) < 0) {
            T_I("I2C transfer failed!: %s", strerror(errno));
        } else {
            rc = VTSS_OK;
        }
        close(file);
    }
    T_D("i2c write port %d, addr 0x%x, %d bytes - RC %d", port_no, i2c_addr, cnt, rc);
    return rc;
}

static mesa_rc board_conf_set_u32(char *buf, size_t bufsize, size_t *buflen, uint32_t val)
{
    size_t len;

    len = snprintf(buf, bufsize, "%u", val);
    if (buflen) {
        *buflen = len;
    }

    return MESA_RC_OK;
}

static mesa_rc board_conf_get(const char *tag, char *buf, size_t bufsize, size_t *buflen)
{
    mesa_rc        rc = MESA_RC_ERROR;

    if (strcmp("port_cfg", tag) == 0) {
        // NB; Interim solution
        FILE *fp = fopen(VTSS_FS_FLASH_DIR "board.txt", "r");
        int i;

        if (fp) {
            if (fscanf(fp, "%d", &i) == 1) {
                rc = board_conf_set_u32(buf, bufsize, buflen, i);
            }

            fclose(fp);
        }
    } else {
        auto &c = vtss::appl::main::module_conf_get("meba");
        auto str = c.str_get(tag, "");
        if (str.length()) {
            strncpy(buf, str.c_str(), bufsize);
            buf[bufsize-1] = '\0';    // Ensure terminated
            if (buflen) {
                *buflen = strlen(buf);
            }

            rc = MESA_RC_OK;
        }
    }

    return rc;
}

static void board_debug(meba_trace_level_t level,
                        const char *location,
                        uint32_t line_no,
                        const char *fmt,
                        ...)
{
    int m = VTSS_MODULE_ID_MAIN, g = VTSS_TRACE_GRP_BOARD;
    if (TRACE_IS_ENABLED(m, g, level)) {
        va_list ap;
        va_start(ap, fmt);
        vtss_trace_vprintf(m, g, level, location, line_no, fmt, ap);
        va_end(ap);
    }
}

static mesa_rc vtss_appl_load_board_driver(const char *path)
{
    mesa_rc rc = MESA_RC_ERROR;
    if (access("/proc/modules", F_OK | R_OK) == 0) {
#if defined(__NR_finit_module)
        char board_type[50];
        int res = 0, fd;

        fd = open(path, O_RDONLY|O_CLOEXEC);
        if (fd < 0) {
            T_E("Unable to open driver %s: %s", path, strerror(errno));
            goto EXIT;
        }
        // finit_module(int fd, const char *uargs, int flags)
        sprintf (board_type,"board_type=%d", vtss_board_type());
        T_D("%s", board_type);
        res = syscall(__NR_finit_module, fd, board_type, 0);
        if (res == -1) {
            T_E("Unable to open driver %s: %s", path, strerror(errno));
            goto EXIT_CLOSE;
        }

        T_I("Kernel module loaded: %s", path);
        rc = MESA_RC_OK;

  EXIT_CLOSE:
        close(fd);
#endif /* (!)defined(__NR_finit_module) */
    } else {
        T_W("Kernel lacks module support, not loading %s", path);
    }

EXIT:
    return rc;
}

/* ================================================================= *
 *  Stack port info
 * ================================================================= */

/* Port number of stack port 0 or 1 - Optimized version - uses cached info */
mesa_port_no_t port_no_stack(BOOL port_1)
{
    return (port_1 ? port.stack_b : port.stack_a);
}

/* Port number is stack port? */
BOOL port_no_is_stack(mesa_port_no_t port_no)
{
    return 0;
}

BOOL vtss_stacking_enabled(void)
{
    return (port.stack_a == VTSS_PORT_NO_NONE ? 0 : 1);
}

/* ================================================================= *
 *  API register access
 * ================================================================= */
//namespace vtss {

class board;

static void do_reset(const char *dirname, const char *devname, const char *driver)
{
    char fn[PATH_MAX];
    FILE *fp;
    snprintf(fn, sizeof(fn), "%s/%s/device/softreset", dirname, devname);
    if ((fp = fopen(fn, "w"))) {
        fputs("1", fp);
        fclose(fp);
        T_I("Board reset done.");
    } else {
        if (errno != ENOENT) {
            int lasterr = errno;
            T_W("Unable to reset device - %s: %s", fn, strerror(lasterr));
        }
    }
}

static BOOL find_dev(const char *driver, char *iodev, size_t bufsz, size_t *mapsize)
{
    const char *top = "/sys/class/uio";
    DIR *dir;
    struct dirent *dent;
    char fn[PATH_MAX], devname[128];
    FILE *fp;
    BOOL found = FALSE;

    if (!(dir = opendir (top))) {
        perror(top);
        exit (1);
    }

    while((dent = readdir(dir)) != NULL) {
        if (dent->d_name[0] != '.') {
            snprintf(fn, sizeof(fn), "%s/%s/name", top, dent->d_name);
            if ((fp = fopen(fn, "r"))) {
                const char *rrd = fgets(devname, sizeof(devname), fp);
                fclose(fp);
                if (rrd && (strstr(devname, driver) || strstr(devname, "vcoreiii_switch"))) {
                    snprintf(iodev, bufsz, "/dev/%s", dent->d_name);
                    snprintf(fn, sizeof(fn), "%s/%s/maps/map0/size", top, dent->d_name);
                    if ((fp = fopen(fn, "r"))) {
                        if (fscanf(fp, "%zi", mapsize)) {
                            fclose(fp);
                            do_reset(top, dent->d_name, devname);
                            found = TRUE;
                            break;
                        }
                        fclose(fp);
                    }
                }
            }
        }
    }

    closedir(dir);

    return found;
}

class board_factory
{
  public:
    board_factory() {}
    static board *the_board();
  private:
    static board *the_board_ptr;
}; // class board_factory

board *board_factory::the_board_ptr=nullptr;

class board
{
  public:
    board() {}
    board(const char *driver)

    {
        char iodev[64];
        size_t mapsize;

        if(!find_dev(driver, iodev, sizeof(iodev), &mapsize)) {
            T_E("Unable to locate '%s' UIO device, please check 'lspci -nn' and 'lsmod'", driver);
            exit(1);
        }

        /* Open the UIO device file */
        T_I("Using UIO, found '%s' driver at %s", driver, iodev);
        dev_fd = open(iodev, O_RDWR);
        if (dev_fd < 1) {
            perror(iodev);
            exit(1);
        }

        /* mmap the UIO device */
        base_mem = static_cast<volatile u32*>(mmap(NULL, mapsize, PROT_READ|PROT_WRITE, MAP_SHARED, dev_fd, 0));
        if(base_mem != MAP_FAILED) {
            T_D("Mapped register memory @ %p", base_mem);
        } else {
            perror("mmap");
        }
    }

    mesa_rc reg_read(const mesa_chip_no_t chip_no,
                     const u32            addr,
                     u32                  *const value)
    {
        *value = PCIE_HOST_CVT(base_mem[addr]);
        return VTSS_RC_OK;
    }

    mesa_rc reg_write(const mesa_chip_no_t chip_no,
                      const u32            addr,
                      const u32            value)
    {
        base_mem[addr] = PCIE_HOST_CVT(value);
        return VTSS_RC_OK;
    }

    void init()
    {
        vtss_frame_extraction_setup();
    }

    int irq_fd() {return dev_fd;}

  private:
    int dev_fd;
}; //class chip

class board_serval_ref : public board
{
  public:
    board_serval_ref() :
            board(SWITCH_DRIVER)
    {
    }
};

board *board_factory::the_board()
{
    if (!the_board_ptr) {
        the_board_ptr=new board_serval_ref();
    }
    return the_board_ptr;
}

#ifdef VTSS_SW_OPTION_DEBUG
static u64 reg_reads[2];
static u64 reg_writes[2];
#endif


static mesa_rc reg_read(const mesa_chip_no_t chip_no,
                        const u32            addr,
                        u32                  *const value)
{
    int rc=board_factory::the_board()->reg_read(chip_no,addr,value);

#ifdef VTSS_SW_OPTION_DEBUG
    vtss_global_lock(__FILE__, __LINE__);
    reg_reads[chip_no]++;
    vtss_global_unlock(__FILE__, __LINE__);
#endif
    return rc;
}

static mesa_rc reg_write(const mesa_chip_no_t chip_no,
                         const u32            addr,
                         const u32            value)
{
    int rc=board_factory::the_board()->reg_write(chip_no,addr,value);
#ifdef VTSS_SW_OPTION_DEBUG
    vtss_global_lock(__FILE__, __LINE__);
    reg_writes[chip_no]++;
    vtss_global_unlock(__FILE__, __LINE__);
#endif
    return rc;
}

static mesa_rc clock_read_write(const u32 addr, u32 *value, BOOL read)
{
    if (read) {
        return reg_read(0, addr, value);
    } else {
        return reg_write(0, addr, *value);
    }
}
static mesa_rc clock_read(const u32 addr, u32 *const value)
{
    mesa_rc rc;
    rc = clock_read_write(addr, value, TRUE);
    return rc;
}
static mesa_rc clock_write(const u32 addr, const u32 value)
{
    u32 val = value;
    return clock_read_write(addr, &val, FALSE);
}

#ifdef VTSS_SW_OPTION_MEP
static uint32_t get_mep_loop_port()
{
    static bool initialized;
    static uint32_t mep_loop_port;

    if (!initialized) {
        char param[20];
        mep_loop_port = MESA_PORT_NO_NONE;
        if (board_info.conf_get("mep_loop_port", param, sizeof(param), 0) == MESA_RC_OK) {
            uint32_t port;
            char     *end;
            port = strtoul(param, &end, 0);
            if (*end == '\0' || port != 0 || port <= mesa_port_cnt(NULL)) {
                mep_loop_port = port;
            }
        }
        initialized = true;
    }
    return mep_loop_port;
}
#endif

/* ================================================================= *
 *  API initialization
 * ================================================================= */

static uint32_t vtss_appl_capability(const void *_inst_unused_, int cap)
{
    uint32_t c = 0;
    int32_t pwrmaxbgt = 0;

    T_D("cap = %d", cap);

    switch (cap) {
    case VTSS_APPL_CAP_ISID_CNT:
        c = VTSS_ISID_CNT;
        break;
    case VTSS_APPL_CAP_ISID_END:
        c = VTSS_ISID_END;
        break;
    case VTSS_APPL_CAP_PORT_USER_CNT:
        c = PORT_USER_CNT;
        break;
    case VTSS_APPL_CAP_L2_PORT_CNT:
        c = L2_MAX_PORTS_;
        break;
    case VTSS_APPL_CAP_L2_LLAG_CNT:
        c = L2_MAX_LLAGS_;
        break;
    case VTSS_APPL_CAP_L2_GLAG_CNT:
        c = L2_MAX_GLAGS_;
        break;
    case VTSS_APPL_CAP_L2_POAG_CNT:
        c = L2_MAX_POAGS_;
        break;
    case VTSS_APPL_CAP_MSTI_CNT:
        c = N_L2_MSTI_MAX;
        break;
    case VTSS_APPL_CAP_PACKET_RX_PORT_CNT:
        c = (mesa_port_cnt(NULL) + 1);
        break;
    case VTSS_APPL_CAP_PACKET_RX_PRIO_CNT:
        c = (MESA_PRIO_CNT + 1);
        break;
    case VTSS_APPL_CAP_PACKET_TX_PORT_CNT:
        c = (mesa_port_cnt(NULL) + 2);
        break;
    case VTSS_APPL_CAP_VLAN_VID_CNT:
        c = MESA_VIDS;
        break;
    case VTSS_APPL_CAP_VLAN_COUNTERS:
#if defined(VTSS_SW_OPTION_MEP)
        c = 0;
#else
        c = MESA_CAP(MESA_CAP_L2_VLAN_COUNTERS);
#endif
        break;
    case VTSS_APPL_CAP_VLAN_USER_INT_CNT:
#if defined(VTSS_SW_OPTION_VLAN)
        c = vlan_user_int_cnt();
#endif
        break;
#if defined(VTSS_SW_OPTION_AFI)
    case VTSS_APPL_CAP_AFI_SINGLE_CNT:
        c = AFI_SINGLE_CNT;
        break;
    case VTSS_APPL_CAP_AFI_MULTI_CNT:
        c = AFI_MULTI_CNT;
        break;
    case VTSS_APPL_CAP_AFI_MULTI_LEN_MAX:
        c = AFI_MULTI_LEN_MAX;
        break;
#endif
    case VTSS_APPL_CAP_MSTP_PORT_CONF_CNT:
        // MSTP: One config per port and one common config for all aggregations
        c = (mesa_port_cnt(NULL) + 1);
        break;
    case VTSS_APPL_CAP_MSTP_MSTI_CNT:
        c = N_L2_MSTI_MAX;
        break;
#if defined(VTSS_SW_OPTION_QOS)
    case VTSS_APPL_CAP_QOS_PORT_QUEUE_CNT:
        c = VTSS_APPL_QOS_PORT_QUEUE_CNT;
        break;
    case VTSS_APPL_CAP_QOS_WRED_DPL_CNT:
        c = (MESA_CAP(MESA_CAP_QOS_DPL_CNT) - 1);
        break;
    case VTSS_APPL_CAP_QOS_WRED_GRP_CNT:
        c = MESA_CAP(MESA_CAP_QOS_WRED_GROUP_CNT);
        break;
#endif
#if defined(VTSS_SW_OPTION_DOT1X)
    case VTSS_APPL_CAP_NAS_STATE_MACHINE_CNT:
        c = mesa_port_cnt(NULL);
#if defined(NAS_USES_PSEC) && defined(NAS_MULTI_CLIENT)
        if (c < PSEC_MAC_ADDR_ENTRY_CNT) {
            c = PSEC_MAC_ADDR_ENTRY_CNT;
        }
#endif
        break;
#endif
    case VTSS_APPL_CAP_ACL_ACE_CNT:
        switch (MESA_CAP(MESA_CAP_MISC_CHIP_FAMILY)) {
        case MESA_CHIP_FAMILY_CARACAL:
            c = 256;
            break;
        case MESA_CHIP_FAMILY_SERVAL:
            c = 512;
            break;
        case MESA_CHIP_FAMILY_OCELOT:
            c = 128;
            break;
        case MESA_CHIP_FAMILY_SERVALT:
            c = 128; // One VCAP_SUPER block
            break;
        case MESA_CHIP_FAMILY_JAGUAR2:
            c = 512; // One VCAP_SUPER block
            break;
        default:
            c = 128;
            break;
        }
        break;
    case VTSS_APPL_CAP_MAX_ACL_RULES_PR_PTP_CLOCK:
        if (MESA_CAP(MESA_CAP_TS_DELAY_REQ_AUTO_RESP)) {  // in auto resp mode we need a rule pr. interface
            c = 7 + 2 * MESA_CAP(MESA_CAP_PORT_CNT);
        } else {
            c = 7;
        }
        break;
    case VTSS_APPL_CAP_EVC_L2CP_TUNNELING:



        break;
    case VTSS_APPL_CAP_EVC_USER_CNT:



        break;

    case VTSS_APPL_CAP_LLDP_REMOTE_ENTRY_CNT:
#if defined(VTSS_SW_OPTION_LLDP)
        c = LLDP_REMOTE_ENTRIES;
#endif
        break;

    case VTSS_APPL_CAP_LLDPMED_POLICIES_CNT:
#ifdef VTSS_SW_OPTION_LLDP_MED
        c = LLDPMED_POLICIES_CNT;
#endif
        break;

    case VTSS_APPL_CAP_IP_INTERFACE_CNT:
        c = MESA_CAP(MESA_CAP_L3_RLEG_CNT);
        if (!c) {
            c = 8;
        }
        break;

    case VTSS_APPL_CAP_IP_ROUTE_CNT:
        c = MESA_CAP(VTSS_APPL_CAP_IP_INTERFACE_CNT);
        if (c < 32) {
            c = 32;
        }
        break;

    case VTSS_APPL_CAP_DHCP_HELPER_USER_CNT:
#ifdef VTSS_SW_OPTION_DHCP_HELPER
        c = DHCP_HELPER_USER_CNT;
#endif
        break;

    case VTSS_APPL_CAP_IPMC_LIB_TOTAL_SRC_LIST:
#ifdef VTSS_SW_OPTION_IPMC
        c = IPMC_LIB_TOTAL_SRC_LIST;
#endif
        break;

    case VTSS_APPL_CAP_Y1564_TEST_COMBO_CNT:



        break;

    case VTSS_APPL_CAP_AGGR_MGMT_GLAG_END:
#ifdef VTSS_SW_OPTION_AGGR
        c = AGGR_MGMT_GLAG_END_;
#endif
        break;

    case VTSS_APPL_CAP_AGGR_MGMT_GROUPS:
#ifdef VTSS_SW_OPTION_AGGR
         c = AGGR_LLAG_CNT_ + AGGR_GLAG_CNT + 1;
#endif
        break;

    case VTSS_APPL_CAP_LACP_MAX_PORTS_IN_AGGR:
#ifdef VTSS_SW_OPTION_LACP
         c = AGGR_MGMT_LAG_PORTS_MAX_;
#endif
        break;

    case VTSS_APPL_CAP_VOICE_VLAN_LLDP_TELEPHONY_MAC_ENTRIES_CNT:
#ifdef VTSS_SW_OPTION_VOICE_VLAN
         c = 4 * mesa_port_cnt(nullptr);
#endif
        break;

    case VTSS_APPL_CAP_SYNCE_PORT_AND_STATION_CNT:
#define SYNCE_STATION_CLOCK_CNT 1
        c = mesa_port_cnt(nullptr) + SYNCE_STATION_CLOCK_CNT;
        break;

    case VTSS_APPL_CAP_PTP_CLOCK_CNT:
#ifdef VTSS_SW_OPTION_PTP
        c = PTP_CLOCK_INSTANCES;
#endif
        break;

    case VTSS_APPL_CAP_SYNCE_PORT_AND_STATION_AND_PTP_CNT:
#ifdef VTSS_SW_OPTION_PTP
        c = PTP_CLOCK_INSTANCES + mesa_port_cnt(nullptr) + SYNCE_STATION_CLOCK_CNT;
#else
        c = mesa_port_cnt(nullptr) + SYNCE_STATION_CLOCK_CNT;
#endif
        break;

    case VTSS_APPL_CAP_MEP_INSTANCE_MAX:
        c = 100;

        if (MESA_CAP(MESA_CAP_OAM_V2)) {
            c = MESA_CAP(MESA_CAP_OAM_VOE_CNT) + MESA_CAP(MESA_CAP_OAM_DOWN_MIP_CNT) + MESA_CAP(MESA_CAP_OAM_UP_MIP_CNT);
        }
        break;

    case VTSS_APPL_CAP_MEP_TEST_RATE_MAX:
        switch (MESA_CAP(MESA_CAP_MISC_CHIP_FAMILY)) {
        case MESA_CHIP_FAMILY_SERVAL:
            c = 2500000;
            break;
        case MESA_CHIP_FAMILY_SERVALT:
        case MESA_CHIP_FAMILY_JAGUAR2:
            c = 10000000;
            break;
        default:
            c = 400000;
            break;
        }
        break;

    case VTSS_APPL_CAP_MEP_EVC_QOS:
        switch (MESA_CAP(MESA_CAP_MISC_CHIP_FAMILY)) {
        case MESA_CHIP_FAMILY_CARACAL: // aka. Luton26
            c = 1;
            break;
        default:
            c = 0;
            break;
        }
        break;

    case VTSS_APPL_CAP_MEP_OPER_STATE:
        if (MESA_CAP(MESA_CAP_OAM_V1)) {
            c = 0;
        }
        if (MESA_CAP(MESA_CAP_OAM_V2)) {
            c = 1;
        }
        break;

    case VTSS_APPL_CAP_MEP_PACKET_RX_MTU:
        return    MESA_CAP(MESA_CAP_PORT_FRAME_LENGTH_MAX);
//        return    PACKET_RX_MTU_DEFAULT;
        break;

    case VTSS_APPL_CAP_AFI:
        return MESA_CAP(MESA_CAP_AFI_V1) || MESA_CAP(MESA_CAP_AFI_V2);

    case VTSS_APPL_CAP_MEP_RX_ISDX_SIZE:
#ifdef VTSS_SW_OPTION_MEP
        c = (mesa_port_cnt(nullptr) > VTSS_APPL_MEP_PRIO_MAX) ? mesa_port_cnt(nullptr) : VTSS_APPL_MEP_PRIO_MAX;
#endif
        break;

    case VTSS_APPL_CAP_MEP_MCE_IDX_USED_MAX:
#ifdef VTSS_SW_OPTION_MEP
        c = mesa_port_cnt(nullptr) * VTSS_APPL_MEP_PRIO_MAX * VTSS_APPL_MEP_TEST_MAX;
#endif
        break;

    case VTSS_APPL_CAP_ZARLINK_SERVO_TYPE:
#if defined(VTSS_SW_OPTION_ZLS30387)
        c = zl_3038x_module_type();
#endif
        break;

    case VTSS_APPL_CAP_MEP_LOOP_PORT:
#ifdef VTSS_SW_OPTION_MEP
        c = get_mep_loop_port();
#else
        c = MESA_PORT_NO_NONE;
#endif
        break;

    case MEBA_CAP_POE:
        if (conf_mgmt_pwrmaxbgt_get(&pwrmaxbgt) != 0) {
            pwrmaxbgt = 0;
        }
        c =  (pwrmaxbgt == 0 ? false : true);
        T_D("MEBA_CAP_POE[%d], pwrmaxbgt[%d], c[%d]", MEBA_CAP_POE, pwrmaxbgt, c);
        break;

    case VTSS_APPL_CAP_POE_PORT_CNT:
        // TODO: wait for PoE co-version
        c = 8;
        break;

    default:
        if (board_instance) {
            c = meba_capability(board_instance, cap);
        } else {
            VTSS_ASSERT(0);
        }
        break;
    }

    return c;
}

static mesa_cap_callback_data_t vtss_appl_callback = {
    .cb = vtss_appl_capability,
    .inst = NULL,
};

static void api_init(void)
{
    mesa_rc            rc;
    mesa_inst_create_t create;
    mesa_init_conf_t   conf;
    mesa_port_no_t     port_no;
    CapArray<mesa_port_map_t, MESA_CAP_PORT_CNT> port_map;
    auto &c = vtss::appl::main::module_conf_get("meba");
    auto lib = c.str_get("lib", "/lib/meba_board.so");

    /* Initialize Board API */
    board_info.reg_read = reg_read;
    board_info.reg_write = reg_write;
    board_info.i2c_read = i2c_read;
    board_info.i2c_write = i2c_write;
    board_info.conf_get = board_conf_get;
    board_info.debug = board_debug;
    board_instance = meba_create(&board_info, lib.c_str());
    VTSS_ASSERT(board_instance != NULL);

    mesa_inst_get(board_instance->props.target, &create);
    mesa_inst_create(&create, NULL);

    // Register application capability function
    vtss_appl_callback.inst = board_instance;
    mesa_cap_callback_add(NULL, &vtss_appl_callback);

    (void) mesa_init_conf_get(NULL, &conf);
    conf.reg_read = board_info.reg_read;
    conf.reg_write = board_info.reg_write;

    if (MESA_CAP(MESA_CAP_CLOCK)) {
        conf.clock_read  = clock_read;
        conf.clock_write = clock_write;
    }





    conf.mux_mode = board_instance->props.mux_mode;

#if defined(SW_OPTION_MANUAL_EXTRACTION)
    conf.using_ufdma = FALSE;  // Using manual frame injection
#else
    conf.using_ufdma = TRUE;   // Otherwise, assume using uFDMA (outside MESA)
#endif

// If Y.1564 module is included, we need 10Gbps through VD1 on JR2 and siblings.
// Otherwise if MEP module is included, we need ~500 Mbps through VD1 for Up-MEP OAM
// Otherwise we don't need anything.










#if   defined(VTSS_SW_OPTION_MEP)
    conf.loopback_bw_mbps = 500;
#else
    conf.loopback_bw_mbps = 0;
#endif
    if (MESA_CAP(MESA_CAP_PORT_QS_CONF)) {
        mesa_qs_conf_t *qs_conf;
        ulong          size;

        if ((qs_conf = (mesa_qs_conf_t *)conf_sec_open(CONF_SEC_GLOBAL, CONF_BLK_QUEUE_CONF, &size)) != NULL) {
            conf.qs_conf = *qs_conf; // The stored queue system configuration
        }
    }

    // VLAN counter support, appl capability function called directly, since it is not registered yet
    conf.vlan_counters_disable = (vtss_appl_capability(NULL, VTSS_APPL_CAP_VLAN_COUNTERS) ? FALSE : TRUE);

    rc = mesa_init_conf_set(NULL, &conf);
    VTSS_ASSERT(rc == VTSS_RC_OK);

    /* Open up API only after initialization */
    API_CRIT_EXIT(__FUNCTION__);

    // Do a board init before the port map is established in case of any changes
    meba_reset(board_instance, MEBA_BOARD_INITIALIZE);

    /* Setup port map for board */
    if (meba_capability(board_instance, MEBA_CAP_BOARD_PORT_MAP_COUNT) > mesa_port_cnt(nullptr)) {
        T_E("Consistency issue: mesa_port_cnt(nullptr) %d, board has %d ports",
            mesa_port_cnt(nullptr), meba_capability(board_instance, MEBA_CAP_BOARD_PORT_MAP_COUNT));
    }

    for (port_no = 0; port_no < port_custom_table.size(); port_no++) {
        if (meba_port_entry_get(board_instance, port_no, &port_custom_table[port_no]) == MESA_RC_OK) {
            port_map[port_no] = port_custom_table[port_no].map;
        } else {
            memset(&port_map[port_no], 0, sizeof(port_map[0]));
            port_map[port_no].chip_port = -1;    // Unused entry
        }
    }

    mesa_port_map_set(NULL, mesa_port_cnt(NULL), port_map.data());

    // Start off any background activities on the board
    board_factory::the_board()->init();
}

static void api_start(void)
{
    // Start off any background activities on the board
    vtss::board_subjects_start(board_factory::the_board()->irq_fd());
}

u32 vtss_api_if_chip_count(void)
{
    return 1;    // Legacy
}

#if defined(VTSS_SW_OPTION_PORT_LOOP_TEST)
/* Port loopback test                                                                                 */
/* -----------------                                                                                  */
/* This is a standalone function which uses the API to perform a port loopback test.                  */
/* This function should only be used  before the main application is started.                         */
/* Each switch port is enabled and the phy is reset and set in loopback.                              */
/* The CPU transmits 1 frame to each port and verifies that it is received again to the CPU buffer.   */
/* After the test completes the MAC address and counters are cleared.                                 */
static void port_loop_test(void)
{
    mesa_port_conf_t        conf;
    mesa_phy_reset_conf_t   phy_reset;
    mesa_port_no_t          port_no;
    BOOL                    testpassed=1;
    mesa_mac_table_entry_t  entry;
    u8                      frame[100];
    mesa_packet_rx_header_t header;

    /* Initilize */
    memset(frame,0,sizeof(frame));
    memset(&phy_reset,0,sizeof(mesa_phy_reset_conf_t));
    memset(&conf, 0, sizeof(mesa_port_conf_t));
    memset(&entry, 0, sizeof(mesa_mac_table_entry_t));
    frame[5] = 0xFF;  /* Test frame DMAC: 00-00-00-00-00-00-FF */
    frame[11] = 0x1;  /* Test frame SMAC: 00-00-00-00-00-00-01 */
    conf.if_type = MESA_PORT_INTERFACE_SGMII;
    conf.speed = MESA_SPEED_1G;
    conf.fdx = 1;
    conf.max_frame_length = 1518;
    entry.vid_mac.vid = 1;
    entry.locked = TRUE;
    entry.vid_mac.mac.addr[5] = 0xff;
    entry.copy_to_cpu = 1;
    (void) mesa_mac_table_add(NULL, &entry);

    /* Board and Phy init */
    port_custom_init();
    port_custom_reset();
    for(port_no = VTSS_PORT_NO_START; port_no < VTSS_PORT_NO_END; port_no++) {
        if (port_phy(port_no)) {
            mesa_phy_post_reset(NULL, port_no);
            break;
        }
    }
    /* Reset and enable switch ports and phys. Enable phy loopback. */
    T_D("Port Loop Test Start:");
    for(port_no = VTSS_PORT_NO_START; port_no < VTSS_PORT_NO_END; port_no++) {
        if (port_phy(port_no)) {
            conf.if_type = MESA_PORT_INTERFACE_SGMII;
            (void)mesa_phy_reset_get(NULL, port_no, &phy_reset);
            phy_reset.mac_if = port_custom_table[port_no].mac_if;
            (void)mesa_phy_reset(NULL, port_no, &phy_reset);
            (void)mesa_phy_write(NULL, port_no, 0, 0x4040);
        } else {
            conf.if_type = MESA_PORT_INTERFACE_LOOPBACK;
        }
        (void)mesa_port_conf_set(NULL, port_no, &conf);
    }
    /* Wait while the Phys syncs up with the SGMII interface */
    VTSS_MSLEEP(2000);

    /* Enable frame forwarding and send one frame towards each port from the CPU.  Verify that the frame is received again. */
    for(port_no = VTSS_PORT_NO_START; port_no < VTSS_PORT_NO_END; port_no++) {
        (void)mesa_port_state_set(NULL, port_no, 1);
        (void)mesa_packet_tx_frame_port(NULL, port_no, frame, 64);
        VTSS_MSLEEP(1); /* Wait until the frame is received again */
        if (mesa_packet_rx_frame_get(NULL, 0, &header, frame, 100) != VTSS_RC_OK) {
            T_E("Port %lu failed self test \n",port_no);
            testpassed = 0;
        }
        (void)mesa_port_state_set(NULL, port_no, 0);
        if (port_phy(port_no)) {
            (void)mesa_phy_write(NULL, port_no, 0, 0x8000);
        }
        (void)mesa_port_counters_clear(NULL, port_no);
    }
    /* Clean up */
   (void)mesa_mac_table_del(NULL, &entry.vid_mac);
   (void)mesa_mac_table_flush(NULL);
    if (testpassed) {
        T_D("...Passed\n");
    }
}
#endif /* VTSS_SW_OPTION_PORT_LOOP_TEST */

mesa_rc vtss_api_if_init(vtss_init_data_t *data)
{
    int i;

    switch (data->cmd) {
    case INIT_CMD_EARLY_INIT:
        /* Copy ail groups to cil */
        for (i = 0; i < TRACE_GRP_CNT; i++)
            trace_grps_cil[i] = trace_grps_ail[i];

        /* Register AIL trace groups */
        VTSS_TRACE_REG_INIT(&trace_reg_ail, trace_grps_ail, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg_ail);

        /* Register CIL trace groups */
        VTSS_TRACE_REG_INIT(&trace_reg_cil, trace_grps_cil, TRACE_GRP_CNT - 1);
        VTSS_TRACE_REGISTER(&trace_reg_cil);

        /* Update API trace levels to initialization settings */
        vtss_api_trace_update_do(FALSE);

        break;
    case INIT_CMD_INIT:
        /* Create API semaphore (initially locked) */
        critd_init(&vtss_api_crit, "vtss_api_crit", VTSS_TRACE_MODULE_ID, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);

        /* Create application API mutex (initially locked) */
        critd_init(&vtss_appl_api_crit, "vtss_appl_api_crit", VTSS_MODULE_ID_API_AI, VTSS_MODULE_ID_API_AI, CRITD_TYPE_MUTEX);
        APPL_API_CRIT_EXIT();

        /* Initialize API */
        api_init();

        /* Load any board specific drivers to the kernel */
        vtss_appl_load_board_driver("/lib/modules/board/mscc_board.ko");

        // USB VNC2 module
        vtss_appl_load_board_driver("/lib/modules/vnc2.ko");


#if   defined(VTSS_SW_OPTION_PORT_LOOP_TEST)
        /* Perform a port loop test */
        port_loop_test();
#endif /*VTSS_SW_OPTION_PORT_LOOP_TEST*/

        break;
    case INIT_CMD_START:
        api_start();

        /* Update API trace levels to settings loaded from Flash by trace module */
        vtss_api_trace_update();

#ifdef VTSS_SW_OPTION_DEBUG
        // Enable register access checking.
        (void)mesa_debug_reg_check_set(NULL, TRUE);
#endif
        break;
    default:
        break;
    }

    return VTSS_OK;
}
