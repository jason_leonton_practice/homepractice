/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "main.h"
#include "interrupt_api.h"

#include <endian.h>
#include <asm/byteorder.h>
#include <net/if.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/uio.h>

#include "vtss_linux_frameio.hxx"

#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_API_AI

static const char *npidev = VTSS_NPI_DEVICE;

static void interface_set_up(const char *ifname, bool up)
{
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd >= 0) {
        struct ifreq ifr;
        memset(&ifr, 0, sizeof(ifr));
        strncpy(ifr.ifr_name, ifname, IFNAMSIZ);
        if (ioctl(sockfd, SIOCGIFFLAGS, &ifr) >= 0) {
            strncpy(ifr.ifr_name, ifname, IFNAMSIZ);
            if (up) {
                ifr.ifr_flags |=  (IFF_UP | IFF_RUNNING);
            } else {
                ifr.ifr_flags &= ~(IFF_UP | IFF_RUNNING);
            }
            if (ioctl(sockfd, SIOCSIFFLAGS, &ifr) >= 0) {
                T_D("%s is UP", ifname);
            } else {
                T_E("ioctl(%s, SIOCSIFFLAGS) failed: %s", ifname, strerror(errno));
            }
        } else {
            T_W("ioctl(%s, SIOCGIFFLAGS) failed: %s", ifname, strerror(errno));
        }
        close(sockfd);
    } else {
        T_E("Unable to UP %s", ifname);
    }
}

#if defined(SW_OPTION_MANUAL_EXTRACTION)

#include "board_subjects.hxx"

#include <linux/if_tun.h>
#include <linux/limits.h>
#define TUNDEV "/dev/net/tun"

static u8 npi_encap[] = {
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0xff, 0xff, 0xff, 0xff, 0xff, 0x88, 0x80, 0x00, 0x00
};

static int tap_fd;

static void write_npi(int fd, void *buf, size_t len)
{
    struct iovec iov[2];
    iov[0].iov_base = npi_encap;
    iov[0].iov_len = sizeof(npi_encap);
    iov[1].iov_base = buf;
    iov[1].iov_len = len;
    if (writev(fd, iov, 2) <= 0) {
        T_E("Unable to write frame to NPI: %s", strerror(errno));
    }
}

static int maketap(const char *ifname)
{
    struct ifreq ifr;
    int fd, err;

    if( (fd = open(TUNDEV, O_RDWR)) < 0 ) {
        T_D("%s - not opened", TUNDEV);
        return fd;
    }

    memset(&ifr, 0, sizeof(ifr));
    strncpy(ifr.ifr_name, ifname, IFNAMSIZ);
    ifr.ifr_flags = IFF_TAP | IFF_NO_PI;

    if ((err = ioctl(fd, TUNSETIFF, (void *) &ifr)) < 0 ) {
        T_E("%s - ioctl(TUNSETIFF) failed: %s", ifname, strerror(errno));
        close(fd);
        return err;
    }

    interface_set_up(ifname, true);

    return fd;
}

static mesa_rc rx_frames_register(mesa_irq_t chip_irq,
                                  meba_event_signal_t signal_notifier)
{
    u32 rx_iobuf[1024];  // 4K frame max
    u32 total = 0, ifhlen, frmlen;

    while (mesa_packet_rx_frame_get_raw(NULL, (u8*) rx_iobuf, sizeof(rx_iobuf), &ifhlen, &frmlen) == VTSS_RC_OK) {
        u32 totlen = ifhlen + frmlen;
        total++;
        T_DG(MESA_TRACE_GROUP_PACKET, "Received %d+%d = %d bytes", ifhlen, frmlen, totlen);
        T_NG_HEX(MESA_TRACE_GROUP_PACKET, (u8*) rx_iobuf, ifhlen + 16);
        if (tap_fd > 0) {
            write_npi(tap_fd, rx_iobuf, totlen);
        }
    }
    T_DG(MESA_TRACE_GROUP_PACKET, "Extracted %d frames", total);
    return MESA_RC_OK;
}

static void tx_frame()
{
    u32 tx_iobuf[1024];  // 4K frame max
    ssize_t len, frame_len;

    frame_len = read(tap_fd, ((u8*) tx_iobuf), sizeof(tx_iobuf));
    if(frame_len > 0) {
        T_DG(MESA_TRACE_GROUP_PACKET, "TAP: NPI->switch inject %zd bytes", frame_len);
        if (memcmp(tx_iobuf, npi_encap, sizeof(npi_encap)) == 0) {
            // Skip NPI Header
            u32 *frame_ptr = &tx_iobuf[sizeof(npi_encap)/sizeof(u32)];
            mesa_packet_tx_ifh_t ifh;
            T_NG_HEX(MESA_TRACE_GROUP_PACKET, (u8*) tx_iobuf, frame_len);
            frame_len -= sizeof(npi_encap);
            ifh.length = MESA_CAP(MESA_CAP_PACKET_RX_IFH_SIZE);
            memcpy(&ifh.ifh, frame_ptr, ifh.length);
            frame_ptr += ifh.length/sizeof(frame_ptr[0]);
            frame_len -= ifh.length;
            if (mesa_packet_tx_frame(NULL, &ifh, (u8*) frame_ptr, frame_len) == VTSS_RC_OK) {
                T_DG(MESA_TRACE_GROUP_PACKET, "TAP: Register injection %zd bytes", frame_len);
                T_NG_HEX(MESA_TRACE_GROUP_PACKET, (u8*) ifh.ifh, ifh.length);
                T_RG_HEX(MESA_TRACE_GROUP_PACKET, (u8*) frame_ptr, frame_len);
            } else {
                T_EG(MESA_TRACE_GROUP_PACKET, "mesa_packet_tx_frame error");
            }
        } else {
            T_NG(MESA_TRACE_GROUP_PACKET, "Non-NPI data skipped");
        }
    }
}

static void NPI_thread(vtss_addrword_t addr_word)
{
    for(;;) {
        tx_frame();
    }
}

void vtss_frame_extraction_setup()
{
    vtss_rc rc;

    tap_fd = maketap(npidev);
    if (tap_fd >= 0) {
        T_I("Allocated TAP device '%s'", npidev);
        // Create NPI rx thread
        static vtss_handle_t npi_thread_handle;

        vtss_thread_create(VTSS_THREAD_PRIO_HIGHER,
                           NPI_thread,
                           0,
                           "NPI_thread",
                           nullptr,
                           0,
                           &npi_thread_handle,
                           NULL);
    } else {
        T_E("Unable to allocate TAP device/Npi device '%s': %s", npidev, strerror(errno));
    }

    if ((rc = vtss_interrupt_handler_add(VTSS_MODULE_ID_PACKET, MESA_IRQ_XTR, rx_frames_register)) != VTSS_RC_OK) {
        T_E("vtss_interrupt_handler_add failed %s", error_txt(rc));
    }

    // NPI encapsulation EPID
    npi_encap[sizeof(npi_encap) - 1] = MESA_CAP(MESA_CAP_PACKET_IFH_EPID);
}

#else

void vtss_frame_extraction_setup()
{
    interface_set_up(npidev, true);
}

#endif
