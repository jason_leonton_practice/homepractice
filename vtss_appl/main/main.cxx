/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "primitives.hxx"
#include "main.h"
#include "control_api.h"
#include "conf_api.h"
#include "os_file_api.h"
#include "vtss_api_if_api.h"
#include "port_api.h"
#include "critd_api.h"
#include "interrupt_api.h"
#include "led_api.h"
#include "misc_api.h"
#include "msg_api.h"
#include "msg_test_api.h"
#include "topo_api.h"
#include "crashhandler.hxx"
#include <unistd.h>
#include <linux/unistd.h>
#include <net/if.h>
#include <netinet/in.h>
#include "vtss/basics/stream.hxx"
#include "vtss/basics/parser_impl.hxx"
#include "vtss/basics/string-utils.hxx"
#include "board_misc.h"
#include "flash_mgmt_api.h"
#include "lock.hxx"              /* For vtss::Lock */
#include <sys/mount.h>
#include <sys/ioctl.h>
#include <mtd/ubi-user.h>
#include <chrono.hxx> /* For vtss::uptime_milliseconds() */
#include "backtrace.hxx"
#include "vtss_alloc.h"

#ifdef VTSS_SW_OPTION_CLI
#include "cli_io_api.h"
#endif

#ifdef VTSS_SW_OPTION_ICLI
#include "icli_api.h"
#endif

#ifdef VTSS_SW_OPTION_ICFG
#include "icfg_api.h"
#endif

#ifdef VTSS_SW_OPTION_CONSOLE
#include "console_api.h"
#endif

#ifdef VTSS_SW_OPTION_SYSUTIL
#include "sysutil_api.h"
#endif

#ifdef VTSS_SW_OPTION_SYSLOG
#include "syslog_api.h"
#endif

#ifdef VTSS_SW_OPTION_FIRMWARE
#include "firmware_api.h"
#endif

#ifdef VTSS_SW_OPTION_AGGR
#include "aggr_api.h"
#endif

#ifdef VTSS_SW_OPTION_QOS
#include "qos_api.h"
#endif

#ifdef VTSS_SW_OPTION_VLAN
#include "vlan_api.h"
#endif

#ifdef VTSS_SW_OPTION_WEB
#include "web_api.h"
#endif

#ifdef VTSS_SW_OPTION_MAC
#include "mac_api.h"
#endif

#ifdef VTSS_SW_OPTION_LOOP_DETECT
#include "vtss_lb_api.h"
#endif

#ifdef VTSS_SW_OPTION_MIRROR
#include "mirror_basic_api.h"
#include "mirror_api.h"
#endif

#ifdef VTSS_SW_OPTION_ACL
#include "acl_api.h"
#endif

#ifdef VTSS_SW_OPTION_PACKET
#include "packet_api.h"
#endif

#if defined(VTSS_SW_OPTION_FRR)
#include "frr_api.hxx"
#endif

#if defined(VTSS_SW_OPTION_IP)
#include "ping_api.h"
#include "traceroute_api.h"
#endif

#if defined(VTSS_SW_OPTION_IP)
#include "ip_api.h"
#endif

#if defined(VTSS_SW_OPTION_DHCP_CLIENT)
#include "dhcp_client_api.h"
#endif

#ifdef VTSS_SW_OPTION_DHCP6_CLIENT
#include "dhcp6_client_api.h"
#endif

#ifdef VTSS_SW_OPTION_SYNCE_DPLL
#include "synce_custom_clock_api.h"
#endif

#ifdef VTSS_SW_OPTION_SYNCE
#include "synce.h"
#endif









#ifdef VTSS_SW_OPTION_EPS
#include "eps_api.h"
#endif

#ifdef VTSS_SW_OPTION_MEP
#include "mep_api.h"
#endif

#ifdef VTSS_SW_OPTION_PVLAN
#include "pvlan_api.h"
#endif

#if defined(VTSS_SW_OPTION_DOT1X)
#include "dot1x_api.h"
#endif

#ifdef VTSS_SW_OPTION_L2PROTO
#include "l2proto_api.h"
#endif

#ifdef VTSS_SW_OPTION_LACP
#include "lacp_api.h"
#endif

#ifdef VTSS_SW_OPTION_LLDP
#include "lldp_api.h"
#endif

#if defined(VTSS_SW_OPTION_SNMP)
#include "vtss_snmp_api.h"
#endif

#ifdef VTSS_SW_OPTION_RMON
#include "rmon_api.h"
#endif

#ifdef VTSS_SW_OPTION_IGMPS
#include "igmps_api.h"
#endif

#ifdef VTSS_SW_OPTION_DNS
#include "ip_dns_api.h"
#endif

#ifdef VTSS_SW_OPTION_POE
#include "poe_api.h"
#endif

#ifdef VTSS_SW_OPTION_AUTH
#include "vtss_auth_api.h"
#endif

#ifdef VTSS_SW_OPTION_UPNP
#include "vtss_upnp_api.h"
#endif

#ifdef VTSS_SW_OPTION_RADIUS
#include "vtss_radius_api.h"
#endif

#ifdef VTSS_SW_OPTION_ACCESS_MGMT
#include "access_mgmt_api.h"
#endif

#if defined(VTSS_SW_OPTION_MSTP)
#include "mstp_api.h"
#endif

#if defined(VTSS_SW_OPTION_PTP)
#include "ptp_api.h"
#endif

#ifdef VTSS_SW_OPTION_DHCP_HELPER
#include "dhcp_helper_api.h"
#endif

#ifdef VTSS_SW_OPTION_DHCP_RELAY
#include "dhcp_relay_api.h"
#endif

#ifdef VTSS_SW_OPTION_DHCP_SNOOPING
#include "dhcp_snooping_api.h"
#endif

#if defined(VTSS_SW_OPTION_NTP)
#include "vtss_ntp_api.h"
#endif

#if defined(VTSS_SW_OPTION_NTP_LINUX)
#include "vtss_ntp_linux_api.h"
#endif

#if defined(VTSS_SW_OPTION_USERS)
#include "vtss_users_api.h"
#endif

#if defined(VTSS_SW_OPTION_PRIV_LVL)
#include "vtss_privilege_api.h"
#endif

#ifdef VTSS_SW_OPTION_ARP_INSPECTION
#include "arp_inspection_api.h"
#endif

#ifdef VTSS_SW_OPTION_IP_SOURCE_GUARD
#include "ip_source_guard_api.h"
#endif

#ifdef VTSS_SW_OPTION_PSEC
#include "psec_api.h"
#endif

#ifdef VTSS_SW_OPTION_PSEC_LIMIT
#include "psec_limit_api.h"
#endif

#ifdef VTSS_SW_OPTION_IGMP_HELPER
#include "igmp_rx_helper_api.h"
#endif

#ifdef VTSS_SW_OPTION_IPMC_LIB
#include "ipmc_lib_api.h"
#endif

#ifdef VTSS_SW_OPTION_MVR
#include "mvr_api.h"
#endif

#ifdef VTSS_SW_OPTION_VOICE_VLAN
#include "voice_vlan_api.h"
#endif

#ifdef VTSS_SW_OPTION_ERPS
#include "erps_api.h"
#endif

#ifdef VTSS_SW_OPTION_EEE
#include "eee_api.h"
#endif

#ifdef VTSS_SW_OPTION_FAN
#include "fan_api.h"
#endif

#ifdef VTSS_SW_OPTION_THERMAL_PROTECT
#include "thermal_protect_api.h"
#endif

#ifdef VTSS_SW_OPTION_LED_POW_REDUC
#include "led_pow_reduc_api.h"
#endif

#ifdef VTSS_SW_OPTION_ETH_LINK_OAM
#include "eth_link_oam_api.h"
#endif

#if defined(VTSS_SW_OPTION_TOD)
#include "tod_api.h"
#endif

#ifdef VTSS_SW_OPTION_VCL
#include "vcl_api.h"
#endif

#ifdef VTSS_SW_OPTION_MLDSNP
#include "mldsnp_api.h"
#endif

#ifdef VTSS_SW_OPTION_MPLS
#include "mpls_api.h"
#endif





#ifdef VTSS_SW_OPTION_IPMC
#include "ipmc_api.h"
#endif

#ifdef VTSS_SW_OPTION_SFLOW
#include "sflow_api.h"
#endif

#ifdef VTSS_SW_OPTION_SYMREG
#include "symreg_api.h"
#endif

#ifdef VTSS_SW_OPTION_AFI
#include "afi_api.h"
#endif

#ifdef VTSS_SW_OPTION_THREAD_LOAD_MONITOR
#include "thread_load_monitor_api.hxx"
#endif

#ifdef VTSS_SW_OPTION_ALARM
#include "alarm_api.hxx"
#endif





#ifdef VTSS_SW_OPTION_VLAN_TRANSLATION
#include "vlan_translation_api.h"
#endif





#ifdef VTSS_SW_OPTION_XXRP
#include "xxrp_api.h"
#endif

#ifdef VTSS_SW_OPTION_LOOP_PROTECTION
#include "loop_protect_api.h"
#endif

#ifdef VTSS_SW_OPTION_TIMER
#include "vtss_timer_api.h"
#endif

#ifdef VTSS_SW_OPTION_DAYLIGHT_SAVING
#include "daylight_saving_api.h"
#endif

#ifdef VTSS_SW_OPTION_PHY
#include "phy_api.h"
#endif

#if defined(VTSS_SW_OPTION_ZLS30387)
#include "zl_3038x_api_pdv_api.h"
#endif

#ifdef VTSS_SW_OPTION_DHCP_SERVER
#include "dhcp_server_api.h"
#endif









#ifdef VTSS_SW_OPTION_JSON_RPC
#include "vtss_json_rpc_api.h"
#endif

#ifdef VTSS_SW_OPTION_JSON_RPC_NOTIFICATION
#include "json_rpc_notification_api.h"
#endif

#ifdef VTSS_SW_OPTION_SNMP_DEMO
#include "vtss_snmp_demo_api.h"
#endif













#if defined(VTSS_SW_OPTION_DDMI)
#include "ddmi_api.h"
#endif

#if defined(VTSS_SW_OPTION_SUBJECT)
#include "subject_api.h"
#endif

#if defined(VTSS_SW_OPTION_UDLD)
#include "udld_api.h"
#endif









#if defined(VTSS_SW_OPTION_FAST_CGI)
#include "fast_cgi_api.h"
#endif

#if defined(VTSS_SW_OPTION_JSON_IPC)
#include "json_ipc_api.h"
#endif


#if defined(VTSS_SW_OPTION_DISCOVERY)
#include "discovery_api.h"
#endif

#if defined(VTSS_SW_OPTION_MODBUS)
#include "vtss_modbus_api.h"
#endif


#if defined(VTSS_SW_OPTION_OPTIONAL_MODULES)
#include "vtss_optional_modules_api.h"
#endif

#if defined(VTSS_SW_OPTION_MPTEST)
#include "mptest_api.h"

#if defined(VTSS_SW_OPTION_EVENT_WARNING)
#include "event_warning_api.h"
#endif
#endif

#if defined(VTSS_SW_OPTION_RSTBTN)
#include "rstbtn_api.h"
#endif

#if defined(VTSS_SW_OPTION_OPTIONAL_MODULES)
#include "usb_api.h"
#endif

#if defined(CYGPKG_IO_FILEIO) && defined(CYGPKG_FS_RAM) && defined(VTSS_SW_OPTION_ICFG)
#include <unistd.h>
#include <cyg/fileio/fileio.h>
#endif

#include "vtss_os_wrapper.h"

#if defined(CYGPKG_DEVS_DISK_MMC)
#include <pkgconf/devs_disk_mmc.h>
#endif

#include "vtss/basics/trace.hxx"

// ===========================================================================
//  Trace definitions
// ---------------------------------------------------------------------------
#include <vtss_module_id.h>

#include <vtss_trace_lvl_api.h>

#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_MAIN

#define VTSS_TRACE_GRP_DEFAULT      0
#define VTSS_TRACE_GRP_INIT_MODULES 1
//#define VTSS_TRACE_GRP_BOARD        2 - See vtss_api_if_api.h
//#define MAIN_TRACE_GRP_CRIT         3 - See vtss_alloc.h
#define TRACE_GRP_CNT               4

#include <vtss_trace_api.h>
#include <vtss_trace_vtss_switch_api.h>

#if (VTSS_TRACE_ENABLED)
static vtss_trace_reg_t trace_reg =
{
    VTSS_TRACE_MODULE_ID, "main", "Main module"
};

#ifndef MAIN_DEFAULT_TRACE_LVL
#define MAIN_DEFAULT_TRACE_LVL VTSS_TRACE_LVL_ERROR
#endif

static vtss_trace_grp_t trace_grps[TRACE_GRP_CNT] =
{
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default",
        MAIN_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_INIT_MODULES */ {
        "initmods",
        "init_modules() calls",
        MAIN_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_BOARD */ {
        "board",
        "Board (meba) trace",
        MAIN_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* MAIN_TRACE_GRP_CRIT */ {
        "crit",
        "Mutex calls",
        MAIN_DEFAULT_TRACE_LVL,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
};
#endif /* VTSS_TRACE_ENABLED */
// ===========================================================================

#if VTSS_TRACE_ENABLED
#define INIT_MODULES_CRIT_ENTER() critd_enter(        &init_modules_crit, MAIN_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define INIT_MODULES_CRIT_EXIT()  critd_exit(         &init_modules_crit, MAIN_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#else
// Leave out function and line arguments
#define INIT_MODULES_CRIT_ENTER() critd_enter(        &init_modules_crit)
#define INIT_MODULES_CRIT_EXIT()  critd_exit(         &init_modules_crit)
#endif

static vtss_flag_t            control_flags;
static critd_t                init_modules_crit;
static critd_t                vtss_global_crit;
static vtss_recursive_mutex_t reset_mutex;
// Reset (shutdown) callbacks
static int reset_callbacks;
static control_system_reset_callback_t callback_list[10];

// Debugging stuff
static vtss_init_data_t dbg_latest_init_modules_data;
static ulong            dbg_latest_init_modules_init_fun_idx;

#define CTLFLAG_SYSTEM_RESET    (1 << 1)
#define CTLFLAG_CONFIG_RESTORE  (1 << 2)

#define INITFUN(x)   {x, #x},

static struct {
    mesa_rc           (*func)(vtss_init_data_t *data);
    const char        *name;
    vtss_tick_count_t max_callback_ticks;
    init_cmd_t        max_callback_cmd;
} initfun[] = {
    INITFUN(vtss_trace_init)
    INITFUN(critd_module_init)
#ifdef VTSS_SW_OPTION_TIMER
    INITFUN(vtss_timer_init)
#endif
#ifdef VTSS_SW_OPTION_THREAD_LOAD_MONITOR
    INITFUN(thread_load_monitor_init)
#endif
    INITFUN(conf_init)
    INITFUN(vtss_api_if_init)
#ifdef VTSS_SW_OPTION_ALARM
     INITFUN(alarm_init)
#endif
#ifdef VTSS_SW_OPTION_ICFG
    INITFUN(vtss_icfg_early_init)
#endif
    INITFUN(msg_init)
    INITFUN(port_init)
    INITFUN(interrupt_init)


#ifdef VTSS_SW_OPTION_CONSOLE
    INITFUN(console_start)
#endif
#if defined(CYGPKG_FS_RAM) && defined(VTSS_SW_OPTION_ICFG)
    INITFUN(os_file_init)
#endif
#ifdef VTSS_SW_OPTION_ICLI
    INITFUN(icli_init)
#endif
#ifdef VTSS_SW_OPTION_CLI
    INITFUN(cli_io_init)
#endif
#ifdef VTSS_SW_OPTION_PHY
    INITFUN(phy_init)
#endif
#if defined(VTSS_SW_OPTION_SYSLOG)
    INITFUN(syslog_init)
#endif

#ifdef VTSS_SW_OPTION_MPTEST
    INITFUN(mptest_init)
#endif


#ifdef VTSS_SW_OPTION_PSEC
    INITFUN(psec_init)
#endif
#ifdef VTSS_SW_OPTION_VLAN
    INITFUN(vlan_init)
#endif
    INITFUN(led_init)



    INITFUN(misc_init)






#ifdef VTSS_SW_OPTION_MEP
    INITFUN(mep_init) // Depends upon misc module
#endif



    INITFUN(standalone_init)
#ifdef VTSS_SW_OPTION_ACCESS_MGMT
    INITFUN(access_mgmt_init)
#endif
#ifdef VTSS_SW_OPTION_WEB
    INITFUN(web_init)
#endif
#ifdef VTSS_SW_OPTION_PACKET
    INITFUN(packet_init)
#endif
#ifdef VTSS_SW_OPTION_SYSUTIL
    INITFUN(system_init)
#endif
#ifdef VTSS_SW_OPTION_DHCP_CLIENT
    INITFUN(vtss_dhcp_client_init)
#endif
#if defined(VTSS_SW_OPTION_IP)
    INITFUN(ping_init)
    INITFUN(traceroute_init)
#endif
#ifdef VTSS_SW_OPTION_IP
    INITFUN(vtss_ip_init)
#ifndef VTSS_SW_OPTION_NTP
#endif
#endif
#ifdef VTSS_SW_OPTION_DHCP6_CLIENT
    INITFUN(dhcp6_client_init)
#endif
#ifdef VTSS_SW_OPTION_ACL
    INITFUN(acl_init)
#endif
#ifdef VTSS_SW_OPTION_MIRROR
    INITFUN(mirror_basic_init)
    INITFUN(mirror_init)
#endif
#ifdef VTSS_SW_OPTION_LOOP_DETECT
    INITFUN(vtss_lb_init)
#endif
#ifdef VTSS_SW_OPTION_MAC
    INITFUN(mac_init)
#endif
#ifdef VTSS_SW_OPTION_QOS
    INITFUN(qos_init)
#endif
#ifdef VTSS_SW_OPTION_AGGR
    INITFUN(aggr_init)
#endif
#ifdef VTSS_SW_OPTION_PVLAN
    INITFUN(pvlan_init)
#endif
#ifdef VTSS_SW_OPTION_FIRMWARE
    INITFUN(firmware_init)
#endif
#ifdef VTSS_SW_OPTION_SYNCE_DPLL
    INITFUN(synce_dpll_init)  // Depends upon misc module
#endif
#ifdef VTSS_SW_OPTION_SYNCE
    INITFUN(synce_init)  // Depends upon synce_dpll module
#endif
#if defined(VTSS_SW_OPTION_MSTP) || defined(VTSS_SW_OPTION_DOT1X) || defined(VTSS_SW_OPTION_LACP) || defined(VTSS_SW_OPTION_SNMP)
    INITFUN(l2_init)
#endif
#ifdef VTSS_SW_OPTION_EPS
    INITFUN(eps_init) // Depends upon misc module
#endif
#ifdef VTSS_SW_OPTION_MSTP
    INITFUN(mstp_init)
#endif
#ifdef VTSS_SW_OPTION_PTP
    INITFUN(ptp_init)
#endif
#ifdef VTSS_SW_OPTION_LACP
    INITFUN(lacp_init)
#endif
#ifdef VTSS_SW_OPTION_DOT1X
    INITFUN(dot1x_init)
#endif
#ifdef VTSS_SW_OPTION_LLDP
    INITFUN(lldp_init)
#endif
#ifdef VTSS_SW_OPTION_EEE
    INITFUN(eee_init)
#endif
#ifdef VTSS_SW_OPTION_FAN
    INITFUN(fan_init)
#endif
#ifdef VTSS_SW_OPTION_THERMAL_PROTECT
    INITFUN(thermal_protect_init)
#endif
#ifdef VTSS_SW_OPTION_LED_POW_REDUC
    INITFUN(led_pow_reduc_init)
#endif
#if defined(VTSS_SW_OPTION_SNMP)
    INITFUN(snmp_init)
#endif
#ifdef VTSS_SW_OPTION_RMON
    INITFUN(rmon_init)
#endif
#ifdef VTSS_SW_OPTION_DNS
    INITFUN(ip_dns_init)
#endif
#ifdef VTSS_SW_OPTION_POE
    INITFUN(poe_init)
#endif
#ifdef VTSS_SW_OPTION_AUTH
    INITFUN(vtss_auth_init)
#endif
#ifdef VTSS_SW_OPTION_UPNP
    INITFUN(upnp_init)
#endif
#ifdef VTSS_SW_OPTION_RADIUS
    INITFUN(vtss_radius_init)
#endif
#ifdef VTSS_SW_OPTION_DHCP_HELPER
    INITFUN(dhcp_helper_init)
#endif
#ifdef VTSS_SW_OPTION_DHCP_SNOOPING
    INITFUN(dhcp_snooping_init)
#endif
#ifdef VTSS_SW_OPTION_DHCP_RELAY
    INITFUN(dhcp_relay_init)
#endif
#ifdef VTSS_SW_OPTION_NTP
    INITFUN(ntp_init)
#endif
#ifdef VTSS_SW_OPTION_USERS
    INITFUN(vtss_users_init)
#endif
#ifdef VTSS_SW_OPTION_PRIV_LVL
    INITFUN(vtss_priv_init)
#endif
#ifdef VTSS_SW_OPTION_ARP_INSPECTION
    INITFUN(arp_inspection_init)
#endif
#ifdef VTSS_SW_OPTION_IP_SOURCE_GUARD
    INITFUN(ip_source_guard_init)
#endif
#ifdef VTSS_SW_OPTION_PSEC_LIMIT
    INITFUN(psec_limit_init)
#endif
#ifdef VTSS_SW_OPTION_IPMC_LIB
    INITFUN(ipmc_lib_init)
#endif
#ifdef VTSS_SW_OPTION_MVR
    INITFUN(mvr_init)
#endif
#ifdef VTSS_SW_OPTION_IPMC
    INITFUN(ipmc_init)
#endif
#ifdef VTSS_SW_OPTION_IGMP_HELPER
    INITFUN(vtss_igmp_helper_init)
#endif
#ifdef VTSS_SW_OPTION_IGMPS
    INITFUN(igmps_init)
#endif
#ifdef VTSS_SW_OPTION_MLDSNP
    INITFUN(mldsnp_init)
#endif
#ifdef VTSS_SW_OPTION_VOICE_VLAN
    INITFUN(voice_vlan_init)
#endif
#ifdef VTSS_SW_OPTION_ERPS
    INITFUN(erps_init)
#endif
#ifdef VTSS_SW_OPTION_ETH_LINK_OAM
    INITFUN(eth_link_oam_init)
#endif
#ifdef VTSS_SW_OPTION_TOD
    INITFUN(tod_init)
#endif
#ifdef VTSS_SW_OPTION_MPLS
    INITFUN(mpls_init)
#endif



#ifdef VTSS_SW_OPTION_SFLOW
    INITFUN(sflow_init)
#endif
#ifdef VTSS_SW_OPTION_VCL
    INITFUN(vcl_init)
#endif
#ifdef VTSS_SW_OPTION_SYMREG
    INITFUN(symreg_init)
#endif
#ifdef VTSS_SW_OPTION_AFI
    INITFUN(afi_init)
#endif
#ifdef VTSS_SW_OPTION_VLAN_TRANSLATION
    INITFUN(vlan_trans_init)
#endif



#ifdef VTSS_SW_OPTION_XXRP
    INITFUN(xxrp_init)
#endif
#ifdef VTSS_SW_OPTION_LOOP_PROTECTION
    INITFUN(loop_protect_init)
#endif
#ifdef VTSS_SW_OPTION_DAYLIGHT_SAVING
    INITFUN(time_dst_init)
#endif
#if defined(VTSS_SW_OPTION_ZLS30387)
    INITFUN(zl_3038x_pdv_init)
#endif



#ifdef VTSS_SW_OPTION_JSON_RPC
    INITFUN(vtss_json_rpc_init)
#endif
#ifdef VTSS_SW_OPTION_JSON_RPC_NOTIFICATION
    INITFUN(vtss_json_rpc_notification_init)
#endif
#ifdef VTSS_SW_OPTION_SNMP_DEMO
    INITFUN(vtss_snmp_demo_init)
#endif


#if defined(VTSS_SW_OPTION_DDMI)
    INITFUN(ddmi_init)
#endif

#if defined(VTSS_SW_OPTION_SUBJECT)
    INITFUN(vtss_subject_init)
#endif
#ifdef VTSS_SW_OPTION_FRR
    INITFUN(frr_init)
#endif
#ifdef VTSS_SW_OPTION_DHCP_SERVER
    INITFUN(dhcp_server_init)
#endif
#if defined(VTSS_SW_OPTION_UDLD)
    INITFUN(udld_init)
#endif






#ifdef VTSS_SW_OPTION_NTP_LINUX
    INITFUN(ntp_linux_init)
#endif
#if defined(VTSS_SW_OPTION_FAST_CGI)
    INITFUN(vtss_fast_cgi_init)
#endif
#if defined(VTSS_SW_OPTION_JSON_IPC)
    INITFUN(json_ipc_init)
#endif



#if defined(VTSS_SW_OPTION_DISCOVERY)
    INITFUN(discovery_init)
#endif

#ifdef VTSS_SW_OPTION_EVENT_WARNING
    INITFUN(event_warning_init)
#endif

#ifdef VTSS_SW_OPTION_RSTBTN
    INITFUN(rstbtn_init)
#endif

#ifdef VTSS_SW_OPTION_USB
    INITFUN(usb_init)
#endif

#if defined(VTSS_SW_OPTION_MODBUS)
    INITFUN(modbus_init)
#endif


#if defined(VTSS_SW_OPTION_OPTIONAL_MODULES)
    INITFUN(vtss_optional_modules_init)
#endif

// **** NOTE: vtss_icfg_late_init must be last in this list ****
#ifdef VTSS_SW_OPTION_ICFG
    INITFUN(vtss_icfg_late_init)
#endif

// **** NOTE: sw_push_button_init() is a special case need to process behind vtss_icfg_late_init()
// because the startup configuration will be reset to factory default when press this button and holding over 5 seconds. ****



};

/* API error text */
static const char *mesa_error_txt(mesa_rc rc)
{
    switch (rc) {
    case MESA_RC_OK:
        return "MESA_RC_OK";
    case MESA_RC_ERROR:
        return "MESA_RC_ERROR";
    case MESA_RC_INV_STATE:
        return "MESA_RC_INV_STATE";
    case MESA_RC_INCOMPLETE:
        return "MESA_RC_INCOMPLETE";
    case MESA_RC_NOT_IMPLEMENTED:
        return "MESA_RC_NOT_IMPLEMENTED";
    case MESA_RC_ERR_PARM:
        return "MESA_RC_ERR_PARM";
    case MESA_RC_ERR_NO_RES:
        return "MESA_RC_ERR_NO_RES";
    default:
        return "MESA: Unknown error code";
    }
}

/* Error code interpretation */
const char *error_txt(mesa_rc rc)
{
    const char  *txt;
    int         module_id;
    int         code;
    static char txt_default[100];

    if (rc > 0) {
        T_E("error_txt() invoked with a positive number (%d)", rc);
        return "Unknown error (rc is positive)";
    }

    module_id = VTSS_RC_GET_MODULE_ID(rc);
    code      = VTSS_RC_GET_MODULE_CODE(rc);

    /* Default text if no module-specific decoding available */
    sprintf(txt_default, "module_id=%s, code=%d", vtss_module_names[module_id], code);
    T_D("%s",txt_default);
    if (rc <= VTSS_RC_OK) {
        switch (module_id) {
        case VTSS_MODULE_ID_API_IO:
        case VTSS_MODULE_ID_API_CI:
        case VTSS_MODULE_ID_API_AI:
            txt = mesa_error_txt(rc);
            break;
#ifdef VTSS_SW_OPTION_ACL
        case VTSS_MODULE_ID_ACL:
            txt = acl_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_MAC
        case VTSS_MODULE_ID_MAC:
            txt = mac_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_VLAN
        case VTSS_MODULE_ID_VLAN:
            txt = vlan_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_PACKET
        case VTSS_MODULE_ID_PACKET:
            txt = packet_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_QOS
        case VTSS_MODULE_ID_QOS:
            txt = vtss_appl_qos_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_FRR
        case VTSS_MODULE_ID_FRR:
            txt = frr_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_IP
        case VTSS_MODULE_ID_IP:
            txt = ip_error_txt(rc);
            break;
        case VTSS_MODULE_ID_IP_CHIP:
            txt = ip_chip_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_DHCP_CLIENT
        case VTSS_MODULE_ID_DHCP_CLIENT:
            txt = dhcp_client_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_DHCP6_CLIENT
        case VTSS_MODULE_ID_DHCP6C:
            txt = dhcp6_client_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_IPMC_LIB
        case VTSS_MODULE_ID_IPMC_LIB:
            txt = ipmc_lib_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_PVLAN
        case VTSS_MODULE_ID_PVLAN:
            txt = pvlan_error_txt(rc);
            break;
#endif
#if defined(VTSS_SW_OPTION_DOT1X)
        case VTSS_MODULE_ID_DOT1X:
            txt = dot1x_error_txt(rc);
            break;
#endif
#if defined(VTSS_SW_OPTION_MIRROR)
        case VTSS_MODULE_ID_MIRROR:
            txt = mirror_error_txt(rc);
            break;
#endif
#if defined(VTSS_SW_OPTION_SYNCE)
        case VTSS_MODULE_ID_SYNCE:
            txt = synce_error_txt(rc);
            break;
#endif

#ifdef VTSS_SW_OPTION_FIRMWARE
        case VTSS_MODULE_ID_FIRMWARE:
            txt = firmware_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_AGGR
        case VTSS_MODULE_ID_AGGR:
          txt = aggr_error_txt(rc);
          break;
#endif
#ifdef VTSS_SW_OPTION_LACP
        case VTSS_MODULE_ID_LACP:
          txt = lacp_error_txt(rc);
          break;
#endif
#if defined(VTSS_SW_OPTION_SNMP)
        case VTSS_MODULE_ID_SNMP:
            txt = snmp_error_txt((snmp_error_t)rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_AUTH
        case VTSS_MODULE_ID_AUTH:
            txt = vtss_auth_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_ACCESS_SSH
        case VTSS_MODULE_ID_SSH:
            txt = ssh_error_txt(rc);
            break;
#endif
#if defined(VTSS_SW_OPTION_RADIUS)
        case VTSS_MODULE_ID_RADIUS:
            txt = vtss_radius_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_ACCESS_MGMT
        case VTSS_MODULE_ID_ACCESS_MGMT:
            txt = access_mgmt_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_UPNP
        case VTSS_MODULE_ID_UPNP:
            txt = upnp_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_DHCP_HELPER
        case VTSS_MODULE_ID_DHCP_HELPER:
            txt = dhcp_helper_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_DHCP_RELAY
        case VTSS_MODULE_ID_DHCP_RELAY:
            txt = dhcp_relay_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_DHCP_SNOOPING
        case VTSS_MODULE_ID_DHCP_SNOOPING:
            txt = dhcp_snooping_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_NTP
        case VTSS_MODULE_ID_NTP:
            txt = ntp_error_txt((ntp_error_t)rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_DAYLIGHT_SAVING
        case VTSS_MODULE_ID_DAYLIGHT_SAVING:
            txt = daylight_saving_error_txt((time_dst_error_t)rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_USERS
        case VTSS_MODULE_ID_USERS:
            txt = vtss_users_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_PRIV_LVL
        case VTSS_MODULE_ID_PRIV_LVL:
            txt = vtss_privilege_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_ARP_INSPECTION
        case VTSS_MODULE_ID_ARP_INSPECTION:
            txt = arp_inspection_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_IP_SOURCE_GUARD
        case VTSS_MODULE_ID_IP_SOURCE_GUARD:
            txt = ip_source_guard_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_PSEC
        case VTSS_MODULE_ID_PSEC:
            txt = psec_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_PSEC_LIMIT
        case VTSS_MODULE_ID_PSEC_LIMIT:
            txt = psec_limit_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_VOICE_VLAN
        case VTSS_MODULE_ID_VOICE_VLAN:
            txt = voice_vlan_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_PORT
        case VTSS_MODULE_ID_PORT:
            txt = port_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_LLDP
        case VTSS_MODULE_ID_LLDP:
            txt = lldp_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_EEE
        case VTSS_MODULE_ID_EEE:
            txt = eee_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_FAN
        case VTSS_MODULE_ID_FAN:
            txt = fan_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_LED_POW_REDUC
        case VTSS_MODULE_ID_LED_POW_REDUC:
            txt = led_pow_reduc_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_VCL
        case VTSS_MODULE_ID_VCL:
            txt = vcl_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_SYSLOG
        case VTSS_MODULE_ID_SYSLOG:
            txt = syslog_error_txt(rc);
            break;
#endif





#ifdef VTSS_SW_OPTION_VLAN_TRANSLATION
        case VTSS_MODULE_ID_VLAN_TRANSLATION:
            txt = vlan_trans_error_txt(rc);
            break;
#endif





#ifdef VTSS_SW_OPTION_XXRP
        case VTSS_MODULE_ID_XXRP:
            txt = xxrp_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_ICLI
        case VTSS_MODULE_ID_ICLI:
            txt = icli_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_SFLOW
        case VTSS_MODULE_ID_SFLOW:
            txt = sflow_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_CONSOLE
        case VTSS_MODULE_ID_CONSOLE:
            txt = console_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_DHCP_SERVER
        case VTSS_MODULE_ID_DHCP_SERVER:
            txt = dhcp_server_error_txt(rc);
            break;
#endif





#ifdef VTSS_SW_OPTION_JSON_RPC
        case VTSS_MODULE_ID_JSON_RPC:
            txt = vtss_json_rpc_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_JSON_RPC_NOTIFICATION
        case VTSS_MODULE_ID_JSON_RPC_NOTIFICATION:
            txt = vtss_json_rpc_notification_error_txt(rc);
            break;
#endif
#if defined(VTSS_SW_OPTION_PTP)
        case VTSS_MODULE_ID_PTP:
            txt = ptp_error_txt(rc);
            break;
#endif
#if defined(VTSS_SW_OPTION_MEP)
        case VTSS_MODULE_ID_MEP:
            txt = mep_error_txt(rc);
            break;
#endif
#if defined(VTSS_SW_OPTION_EPS)
        case VTSS_MODULE_ID_EPS:
            txt = eps_error_txt(rc);
            break;
#endif
#if defined(VTSS_SW_OPTION_ERPS)
        case VTSS_MODULE_ID_ERPS:
            txt = erps_error_text(rc);
            break;
#endif

























#ifdef VTSS_SW_OPTION_SYMREG
        case VTSS_MODULE_ID_SYMREG:
            txt = symreg_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_AFI
        case VTSS_MODULE_ID_AFI:
            txt = afi_error_txt(rc);
            break;
#endif
#ifdef VTSS_SW_OPTION_ALARM
        case VTSS_MODULE_ID_ALARM:
            txt = alarm_error_txt(rc);
            break;
#endif





#ifdef VTSS_SW_OPTION_THREAD_LOAD_MONITOR
        case VTSS_MODULE_ID_THREAD_LOAD_MONITOR:
            txt = thread_load_monitor_error_txt(rc);
            break;
#endif
        default:
            txt = txt_default;
            break;
        }
    } else {
        txt = "No error";
    }
    return txt;
}

const char *control_system_restart_to_str(mesa_restart_t restart)
{
    switch (restart) {
    case MESA_RESTART_COLD:
        return "cold";

    case MESA_RESTART_COOL:
        return "cool";

    case MESA_RESTART_WARM:
        return "warm";

    default:
        T_E("Unknown restart type: %d", restart);
        return "unknown";
    }
}

extern "C" int main_icli_cmd_register();

mesa_rc init_modules(vtss_init_data_t *data)
{
    int i;
    mesa_rc rc, rc_last = VTSS_RC_OK;
    vtss_tick_count_t start, total;
    INIT_MODULES_CRIT_ENTER();

    dbg_latest_init_modules_data = *data;
    for (i = 0; i < ARRSZ(initfun); i++) {
        dbg_latest_init_modules_init_fun_idx = i;
        T_IG(VTSS_TRACE_GRP_INIT_MODULES, "%s(%s, cmd: %d (%s), isid: %x, flags: 0x%x)", __FUNCTION__, initfun[i].name, data->cmd, control_init_cmd2str(data->cmd), data->isid, data->flags);

        start = vtss_current_time();
        if ((rc = initfun[i].func(data)) != VTSS_RC_OK) {
            rc_last = rc; /* Last error is returned */
        }
        total = vtss_current_time() - start;
        T_IG(VTSS_TRACE_GRP_INIT_MODULES, "%s: " VPRI64u " ms", initfun[i].name, VTSS_OS_TICK2MSEC(total));

        if (total > initfun[i].max_callback_ticks) {
            initfun[i].max_callback_ticks = total;
            initfun[i].max_callback_cmd   = data->cmd;
        }
    }

    if (data->cmd == INIT_CMD_MASTER_UP) {
        T_WG(VTSS_TRACE_GRP_INIT_MODULES, "INIT_CMD_MASTER_UP done. Time since boot = " VPRI64u "", vtss::uptime_milliseconds());
    }

    if (data->cmd == INIT_CMD_INIT) {
         // Tell message module that the INIT_CMD_INIT
         // message has been pumped through all modules.
         void msg_init_done(void); // Not published
         msg_init_done();
         main_icli_cmd_register();
    }

    INIT_MODULES_CRIT_EXIT();

    return rc_last;
}

#ifdef CYGPKG_CPULOAD
static u32           cpuload_calibrate;
static cyg_cpuload_t cpuload_obj;
static vtss_handle_t cpuload_handle;
#endif

vtss_bool_t control_sys_get_cpuload(u32 *average_point1s,
                                    u32 *average_1s,
                                    u32 *average_10s)
{
    vtss_cpuload_get(average_point1s, average_1s, average_10s);
    return true;
}

#ifdef __cplusplus
extern "C" void control_access_statistics_start(void);
#else
void control_access_statistics_start(void);
#endif

void vtss_global_lock(const char *file, unsigned int line)
{
    if (!vtss_global_crit.init_done) {
        // We do a lazy init, because this function may be called during
        // construction of objects, that is, prior to invocation of main().
        critd_init(&vtss_global_crit, "Global lock", VTSS_MODULE_ID_MAIN, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX_RECURSIVE);

        // Notice that critd_init() creates the mutex locked, so it's already in
        // the right state when initialized. However, we must exit and re-enter
        // it, because of the ways that critd() handles the initial locked
        // state (through a flag) and in order to truly support recursiveness of
        // this mutex.
        critd_exit(&vtss_global_crit, MAIN_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, file, line);
    }

    critd_enter(&vtss_global_crit, MAIN_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, file, line);
}

void vtss_global_unlock(const char *file, unsigned int line)
{
    critd_exit(&vtss_global_crit, MAIN_TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, file, line);
}

static void basic_linux_system_init() {
    int fd = -1;
    int res, line;
    struct ifreq ifr = {};
    struct sockaddr_in *sa = (struct sockaddr_in *)&ifr.ifr_addr;

    // Very basic stuff
    chdir("/");
    setsid();
    putenv((char *) "HOME=/");
    putenv((char *) "TERM=linux");
    putenv((char *) "SHELL=/bin/sh");
    putenv((char *) "USER=root");

#define DO(X) res = X; if (res == -1) { line = __LINE__; goto ERROR; }
#define CHECK(X) if (X) { line = __LINE__; goto ERROR; }
    // Mount the various file systems
    DO(mkdir("/dev/shm", 0755));
    DO(mkdir("/dev/pts", 0755));
    DO(mount("proc", "/proc", "proc", 0, 0));
    DO(mount("sysfs", "/sys", "sysfs", 0, 0));
    DO(mount("tmpfd", "/tmp", "tmpfs", 0, "size=8388608,nosuid,nodev,mode=1777"));
    DO(mount("tmpfd", "/run", "tmpfs", 0, "size=2097152,nosuid,nodev,mode=1777"));
    DO(mount("devpts", "/dev/pts", "devpts", 0, 0));
    // We do not perform a pivot_root here, as we intend to continue running on
    // the read-only squashfs filesystem that is mounted as part of initrd.
    // This may change in near future as we could boot from NAND flash instead.

    // Enable sys-requests - ignore errors as this is a nice-to-have
    if (fd == -1) close(fd);
    fd = open("/proc/sys/kernel/sysrq", O_WRONLY);
    static const char *one = "1\n";
    if (fd != -1) {
        write(fd, one, strlen(one));
        close(fd);
        fd = -1;
    }

    // Assign address 127.0.0.1 to the loop-back device (lo) and bring it up
    if (fd == -1) close(fd);
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    CHECK(fd == -1);
    strncpy(ifr.ifr_name, "lo", IFNAMSIZ);
    sa->sin_family = AF_INET;
    sa->sin_port = 0;
    sa->sin_addr.s_addr = inet_addr("127.0.0.1");
    DO(ioctl(fd, SIOCSIFADDR, &ifr));
    DO(ioctl(fd, SIOCGIFFLAGS, &ifr));
    ifr.ifr_flags |= IFF_UP | IFF_RUNNING;
    DO(ioctl(fd, SIOCSIFFLAGS, &ifr));

#undef DO
#undef CHECK
    if (fd != -1) close(fd);
    return;

ERROR:
    if (fd != -1) close(fd);
    printf("BASIC SYSTEM INIT FAILED!!!\n");
    printf("File: %s, Line: %d, code: %d\n", __FILE__, line, res);
    printf("errno: %d error: %s\n\n", errno, strerror(errno));
    fflush(stdout);
    fflush(stderr);

    ::abort();
}

static void basic_linux_system_init_urandom() {
    bool seed_ok = false;
    const size_t buf_size = 512;
    static const char *DEV = "/dev/urandom";
    static const char *SEED = VTSS_FS_FLASH_DIR "/random-seed";
    char buf[buf_size];
    char *p;
    size_t s;
    int fd = -1;

    // Read the seed from last time (if it exists)
    seed_ok = false;
    fd = open(SEED, O_RDONLY);
    if (fd != -1) {
        p = buf;
        s = buf_size;

        while (s) {
            int res = read(fd, buf, s);
            if (res == -1) {
                printf("URANDOM: Failed to read from %s: %s", SEED,
                       strerror(errno));
                break;
            } else if (res == 0) {
                // zero indicates end of file
                break;
            } else {
                p += res;
                s -= res;
            }
        }

        if (s == 0) {
            seed_ok = true;
        }

        close(fd);
        fd = -1;
    }

    // Write the seed into the urandom device
    if (seed_ok) {
        fd = open(DEV, O_WRONLY);
        if (fd != -1) {
            p = buf;
            s = buf_size;

            while (s) {
                int res = write(fd, buf, s);
                if (res == -1) {
                    printf("URANDOM: Failed to write to %s: %s", DEV,
                           strerror(errno));
                    break;
                } else if (res == 0) {
                    // zero indicates end of file
                    break;
                } else {
                    p += res;
                    s -= res;
                }
            }

            close(fd);
            fd = -1;
        }
    }

    // Read a new seed from urandom
    seed_ok = false;
    fd = open(DEV, O_RDONLY);
    if (fd != -1) {
        p = buf;
        s = buf_size;

        while (s) {
            int res = read(fd, buf, s);
            if (res == -1) {
                printf("URANDOM: Failed to read from %s: %s", DEV,
                       strerror(errno));
                break;
            } else if (res == 0) {
                // zero indicates end of file
                break;
            } else {
                p += res;
                s -= res;
            }
        }

        if (s == 0) {
            seed_ok = true;
        }

        close(fd);
        fd = -1;
    } else {
        printf("URANDOM: Failed to open %s: %s\n", DEV, strerror(errno));
    }

    // Write the seed such that it will be used on the next boot
    if (seed_ok) {
        fd = open(SEED, O_WRONLY | O_CREAT | O_TRUNC);
        if (fd != -1) {
            p = buf;
            s = buf_size;

            while (s) {
                int res = write(fd, buf, s);
                if (res == -1) {
                    printf("URANDOM: Failed to write to %s: %s", SEED,
                           strerror(errno));
                    break;
                } else if (res == 0) {
                    // zero indicates end of file
                    break;
                } else {
                    p += res;
                    s -= res;
                }
            }

            close(fd);
            fd = -1;

        } else {
            printf("URANDOM: Failed to open %s: %s\n", SEED, strerror(errno));
        }
    }
}

namespace vtss {
namespace parser {
struct MtdLine : public ParserBase {
    typedef str val;

    bool operator()(const char *&b, const char *e) {
        // Expect something like. The first line is a header which will fail
        // dev:    size   erasesize  name
        // mtd0: 00040000 00040000 "RedBoot"
        // mtd1: 00040000 00040000 "conf"
        // mtd2: 00100000 00040000 "stackconf"
        // mtd3: 00040000 00040000 "syslog"
        // mtd4: 00380000 00040000 "managed"
        // mtd5: 00a00000 00040000 "linux"
        // mtd6: 00040000 00040000 "FIS_directory"
        // mtd7: 00001000 00040000 "RedBoot_config"
        // mtd8: 00040000 00040000 "Redundant_FIS"
        // mtd9: 10000000 00020000 "rootfs_data"
        Lit sep(":");
        Lit mtd("mtd");
        OneOrMore<group::Space> sp;
        return Group(b, e, mtd, dev_, sep, sp, size_, sp, erase_size_, sp,
                     name_);
    }
    bool operator()(str s) {
        const char *b = s.begin();
        const char *e = s.end();
        return operator()(b, e);
    }

    val get() const { return name_.get(); }
    str name() const { return name_.get(); }
    uint32_t dev() const { return dev_.get(); }
    uint32_t size() const { return size_.get(); }
    uint32_t erase_size() const { return erase_size_.get(); }

    Int<uint32_t, 10, 1, 3> dev_;
    Int<uint32_t, 16, 1, 8> size_;
    Int<uint32_t, 16, 1, 8> erase_size_;
    StringRef name_;
};
}  // namespace parser

static bool mtd_device_by_name(const char *name, Buf *dev) {
    const int buf_size = 1024 * 1024;
    char buf[buf_size];

    int fd = open("/proc/mtd", O_RDONLY);
    if (fd == -1) {
        printf("Failed to open /proc/mtd: %s\n", strerror(errno));
        return false;
    }

    char *p = buf;
    int s = buf_size;

    while (s) {
        int res = read(fd, p, s);
        if (res == -1 || res == 0) {
            break;
        }

        s -= res;
        p += res;
    }
    close(fd);
    fd = -1;

    str data(buf, p);

    for (str line : LineIterator(data)) {
        vtss::parser::MtdLine mtd_line;
        if (mtd_line(line)) {
            if (mtd_line.name() == str(name)) {
                BufPtrStream o(dev);
                o << mtd_line.dev();
                o.cstring();
                return o.ok();
            }
        }
    }

    return false;
}
}  // namespace vtss

static int ubi_attach (int mtd_dev)
{
    int fd, ret;
    struct ubi_attach_req uatt;

    fd = open("/dev/ubi_ctrl", O_RDONLY);
    if (fd < 0) {
        return -1;
    }

    memset(&uatt, 0, sizeof(uatt));
    uatt.mtd_num = mtd_dev;
    uatt.ubi_num = 0;

    ret = ioctl(fd, UBI_IOCATT, &uatt);

    close(fd);

    return ret;
}

static void basic_linux_system_mount_rw() {
    int res;
    vtss::SBuf16 mtd_dev;

    if (access(VTSS_FS_FLASH_DIR "/", R_OK | W_OK | X_OK | F_OK) == 0) {
        // Flash FS already there and read/writeable (booted on eCPU).
        printf("Using existing mount point for " VTSS_FS_FLASH_DIR "\n");
        return;
    }

    if (mtd_device_by_name("rootfs_data", &mtd_dev)) {
        if (ubi_attach(atoi(mtd_dev.begin())) != 0) {
            printf("\n\n");
            printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            printf("!! FAILED TO ATTACH PERSISTENT STORAGE\n");
            printf("!! Attach(/dev/mtd%s) failed: %s\n", mtd_dev.begin(), strerror(errno));
            printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            printf("\n\n");
            goto MOUNT_TMP;
        }
        res = mount("ubi0:switch", VTSS_FS_FLASH_DIR, "ubifs", MS_SYNCHRONOUS, 0);
        if (res == -1) {
            printf("\n\n");
            printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            printf("!! FAILED TO MOUNT PERSISTENT STORAGE\n");
            printf("!! MOUNT-ERROR: %s (%d)\n", strerror(errno), errno);
            printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            printf("\n\n");

            // Mount a temp filesystem instead such that we can get the switch
            // booted
            goto MOUNT_TMP;
        }

    } else {
        printf("\n\n");
        printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
        printf("!! FAILED TO MOUNT PERSISTENT STORAGE\n");
        printf("!! ERROR: COULD NOT FIND 'rootfs_data' partition\n");
        printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
        printf("\n\n");
        goto MOUNT_TMP;
    }

    return;

MOUNT_TMP:
    res = mount("tmpfs", VTSS_FS_FLASH_DIR, "tmpfs", 0,
                "size=8388608,nosuid,nodev,mode=1777");

    if (res == -1) {
        printf("\n\n");
        printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
        printf("!! FAILED TO MOUNT RAM FILESYSTEM AS PERSISTENT STORAGE\n");
        printf("!! ERROR: %s (%d)\n", strerror(errno), errno);
        printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
        printf("\n\n");
        ::abort();
    }
}

static void main_early_init(void)
{
    vtss_init_data_t data;

    // Pump an INIT_CMD_EARLY_INIT around as the very, very first thing we do.
    // This allows every module to do something before everything else starts.
    // This is mostly used to initialize the trace system, so that we have it
    // at hand already in INIT_CMD_INIT.

    // First, initialize our own trace, because main() doesn't have an init
    // function that will get invoked by the init_modules() call.
    VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, TRACE_GRP_CNT);
    VTSS_TRACE_REGISTER(&trace_reg);

    memset(&data, 0, sizeof(data));
    data.cmd = INIT_CMD_EARLY_INIT;
    (void)init_modules(&data);
}

static mesa_restart_t __restart;

static void print_time(void)
{
    time_t     rawtime;
    struct tm *timeinfo;
    char       buffer[80];

    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(buffer, sizeof(buffer), "%X", timeinfo);
    printf("%s ", buffer);
}

int main(int argc, char *argv[]) {
    int ac;
    int opt;
    char **av;
    int force_init = 0;

    print_time();
    printf("Starting application...\n");

    // Take over various signals to allow us to print thread info if e.g. an
    // abort or segmentation fault occurs.
    vtss_crashhandler_setup();

    critd_init(&init_modules_crit, "Init Modules", VTSS_MODULE_ID_MAIN, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX_RECURSIVE);
    // It might take some time to load a worst-case configuration, so allow
    // indefinite lock of this mutex.
    init_modules_crit.max_lock_time = -1;
    INIT_MODULES_CRIT_EXIT();

    // Pump an early init event out before doing anything else.
    main_early_init();

#if defined(VTSS_SW_OPTION_ZLS30387)
    zl_3038x_api_version_check();
#endif

    ac = argc;
    av = argv;
    while ((opt = getopt(ac, av, "iht:")) != -1) {
        switch (opt) {
        case 't':
            break;

        case 'i':
            force_init = 1;
            break;

        case '?':
        case 'h': /* Help */
        default:
            printf("%s [-h] [-t <module>:<group>:<level>] [-i]\n", argv[0]);
            printf("-h: Show help\n");
            printf("-t: Show or set trace level for a specific module and group. (Multiple occurrences allowed)\n");
            printf("-i: Force system init\n");
            exit(0);
        }
    }

    // Certain parts of the init steps is only relevant if we run as PID==1
    bool system_init = ((int)getpid() == 1) || force_init;

    // Performs a couple of basic initializations steps that is not allowed to
    // fail.
    if (system_init) basic_linux_system_init();

    // Mount rw storage
    basic_linux_system_mount_rw();

    // Load the random seed (if we have such) - must run after we have a rw
    // filesystem.
    basic_linux_system_init_urandom();

    // Creating various folders needed by the application
    (void)mkdir(VTSS_FS_FILE_DIR, 0777);
    (void)mkdir(VTSS_FS_RUN_DIR, 0777);




    vtss_init_data_t data;
    memset(&data, 0, sizeof(data));
    data.cmd = INIT_CMD_INIT;
    data.isid = VTSS_ISID_LOCAL;
    data.switch_info[VTSS_ISID_LOCAL].configurable = TRUE;

    init_modules(&data);

    ac = argc;
    av = argv;
    while ((opt = getopt(ac, av, "iht:")) != -1) {
        switch (opt) {
        case 't': {
            int module, group, level;
            const char *s_module, *s_group, *s_level;
            char *saveptr;
            s_module = strtok_r(optarg, ":", &saveptr);
            s_group = strtok_r(NULL, ":", &saveptr);
            s_level = strtok_r(NULL, ":", &saveptr);

            if (s_module == NULL || s_group == NULL || s_level == NULL) {
                printf("Illegal trace arg: use <module>:<group>:<level>\n");
                continue;
            }

            if (!vtss_trace_module_to_val(s_module, &module)) {
                printf("Illegal trace module: %s\n", s_module);
                continue;
            }

            if (!vtss_trace_grp_to_val(s_group, module, &group)) {
                printf("Illegal trace group for %s: %s\n", s_module, s_group);
                continue;
            }

            if (!vtss_trace_lvl_to_val(s_level, &level)) {
                printf("Illegal trace level for: %s - using 'debug'\n", s_level);
                level = VTSS_TRACE_LVL_DEBUG;
            }

            vtss_trace_module_lvl_set(module, group, level);
            vtss_api_trace_update();

            break;
        }
        }
    }

    // Wait until INIT_CMD_INIT is complete.
    msg_wait(MSG_WAIT_UNTIL_INIT_DONE, VTSS_MODULE_ID_MAIN);

    vtss_flag_init(&control_flags);
    vtss_recursive_mutex_init(&reset_mutex);

    for (;;) {
        vtss_flag_value_t flags;
        flags = vtss_flag_wait(&control_flags, 0xFF, VTSS_FLAG_WAITMODE_OR_CLR);
        if (flags & CTLFLAG_SYSTEM_RESET) {
            control_system_reset_sync(__restart);
            /* NOTREACHED */
        }
    }

    return 0;
}

/*
 * Leaching - control API functions
 */

static void control_config_change_detect_set(BOOL enable)
{
    conf_mgmt_conf_t conf;

    if (conf_mgmt_conf_get(&conf) == VTSS_OK) {
        conf.change_detect = enable;
        (void)conf_mgmt_conf_set(&conf);
    }
}

extern "C"
void control_config_reset(vtss_usid_t usid, vtss_flag_value_t flags)
{
    vtss_init_data_t data;
    vtss_isid_t      isid;

    memset(&data, 0, sizeof(data));
    data.cmd = INIT_CMD_CONF_DEF;
    data.flags = flags;

    for (isid = VTSS_ISID_START; isid < VTSS_ISID_END; isid++) {
        // Pretend that all switches are configurable. Only the fact
        // that a switch is not configurable in INIT_CMD_CONF_DEF
        // events must be used for anything.
        data.switch_info[isid].configurable = TRUE;
    }

    // We want all the calls to init_modules()
    // below to be non-intervenable.
    // Notice that the semaphore is recursive, so that
    // the same thread may take it several times. This
    // is used in this case, because the init_modules()
    // function itself also takes it.
    INIT_MODULES_CRIT_ENTER();

    /* For improved performance, configuration change detection is disabled
       during default creation */
    control_config_change_detect_set(0);

    if(msg_switch_is_master()) {
        if(usid == VTSS_USID_ALL) {
            T_W("Resetting global configuration, flags 0x%x", flags);
            data.isid = VTSS_ISID_GLOBAL;
            init_modules(&data);
        }
        if(vtss_switch_stackable()) { /* Reset selected switches individually */
            static control_msg_t cfg_reset_message = { .msg_id = CONTROL_MSG_ID_CONFIG_RESET };
            vtss_usid_t i;
            cfg_reset_message.flags = flags;
            for(i = VTSS_USID_START; i < VTSS_USID_END; i++) {
                if(usid == VTSS_USID_ALL || i == usid) {
                    vtss_isid_t isid = topo_usid2isid(i);
                    T_W("Reset switch %d configuration", isid);
                    data.isid = isid;
                    init_modules(&data);
                    if(msg_switch_exists(isid)) {
                        T_W("Requesting local config reset, sid %d (%d)", i, isid);
                        msg_tx_adv(NULL, NULL, MSG_TX_OPT_DONT_FREE,
                                   VTSS_MODULE_ID_MAIN, isid,
                                   (const void *)&cfg_reset_message, sizeof(cfg_reset_message));
                    }
                }
            }
        } else {
            T_W("Reset switch %d configuration", VTSS_ISID_START);
            data.isid = VTSS_ISID_START;
            init_modules(&data);
        }
    }

    /* Local config reset */
    if(!vtss_switch_stackable() || !msg_switch_is_master())  {
        T_W("Resetting local configuration, flags 0x%x", flags);
        data.isid = VTSS_ISID_LOCAL;
        init_modules(&data);
    }

    /* Enable configuration change detection again */
    control_config_change_detect_set(1);

    INIT_MODULES_CRIT_EXIT();

    T_I("Reset configuration done");
}

static void control_system_reset_callback(mesa_restart_t restart)
{
    int i;
    for(i = 0; i < reset_callbacks; i++) {
        T_D("Reset callback %d enter (@ %p)", i, callback_list[i]);
        callback_list[i](restart);
        T_D("Reset callback %d return", i);
    }
}

static void cool_restart(void)
{
#if defined(MSCC_BRSDK)
    // Ask the HAL layer to perform the cool restart.
    // It takes one parameter, which is the address of a register
    // in the switch core domain that must survive the reset.
#if defined(VTSS_DEVCPU_GCB_CHIP_REGS_GENERAL_PURPOSE)
    hal_cool_restart((u32 *)&VTSS_DEVCPU_GCB_CHIP_REGS_GENERAL_PURPOSE);
#elif defined(VTSS_DEVCPU_GCB_CHIP_REGS_GPR)
    hal_cool_restart((u32 *)&VTSS_DEVCPU_GCB_CHIP_REGS_GPR);
#endif
#endif

    // Die...
    killpg(getpgrp(), SIGKILL);
    exit(0);
}

static void control_system_reset_do_sync(mesa_restart_t restart, BOOL bypass_callback)
{
    // Let those modules that are interested in this know about the upcoming restart.
    // In some situations, this is not possible and could cause a deadlock or
    // a recursive call to this function (through VTSS_ASSERT()). In such situations,
    // we bypass both callbacks and grabbing the reset lock (since that also might
    // cause a deadlock).
    if (!bypass_callback) {
        control_system_reset_callback(restart);
         /* Boost current holder of lock - if any */
        vtss_thread_prio_set(vtss_thread_self(), VTSS_THREAD_PRIO_HIGHER);
        VTSS_OS_MSLEEP(1000);
    }

    // Tell the method to the API.
    mesa_restart_conf_set(NULL, restart);

    switch (restart) {
    case MESA_RESTART_COOL: {
        cool_restart();
        break;
      }

    default:
        // If we get here, the reboot method is not supported on this platform.
        T_E("Unsupported reboot method: %s", control_system_restart_to_str(restart));
        break;
    }

    T_E("Reset(%d) trying to return", restart);
    VTSS_ASSERT(0);             /* Don't return */
}

void control_system_reset_sync(mesa_restart_t restart)
{
    control_system_reset_do_sync(restart, FALSE);
    exit(1);
}

void control_system_assert_do_reset(void)
{
    vtss_backtrace(printf, 0);



    assert(0); // Causes a SIGABRT, handled by crashhandler.cxx#vtss_crashhandler_signal() and a subsequent reboot.

    while(1); /* Don't return */
}

int control_system_reset(BOOL local, vtss_usid_t usid, mesa_restart_t restart)
{
    /* Force imediate configuration flush */
    conf_flush();

    if (control_system_flash_trylock()) {
        /* once lock, never return */
        __restart = restart;
        printf("Rebooting system...\n");
        vtss_thread_yield();
        vtss_flag_setbits(&control_flags, CTLFLAG_SYSTEM_RESET);
    } else {
        printf("Unable to reboot system because the flash is currently locked. Please try again later.\n");
        return -1;
    }

    return 0;
}

void control_system_flash_lock(void)
{
    T_D("Locking system reset/flash");
    vtss_recursive_mutex_lock(&reset_mutex);
    T_D("System reset/flash locked, level %d", reset_mutex.lock_cnt);
}

BOOL control_system_flash_trylock(void)
{
    BOOL result;
    T_D("Attempting to lock system reset/flash");
    result = vtss_recursive_mutex_trylock(&reset_mutex);
    if (result) {
      T_D("System reset/flash locked, level %d", reset_mutex.lock_cnt);
    } else {
      T_D("System reset/flash try lock failed");
    }
    return result;
}

void control_system_flash_unlock(void)
{
    vtss_recursive_mutex_unlock(&reset_mutex);
    T_D("Unlocked system reset/flash, level %d", reset_mutex.lock_cnt);
}

/* Return TRUE: mutex locked, FALSE: mutex unlocked */
BOOL control_system_flash_locked(void)
{
    if (vtss_recursive_mutex_trylock(&reset_mutex)) {
        vtss_recursive_mutex_unlock(&reset_mutex);
        return true;
    }

    return false;
}

void control_system_reset_register(control_system_reset_callback_t cb)
{
    VTSS_ASSERT(reset_callbacks < ARRSZ(callback_list));
    vtss_global_lock(__FILE__, __LINE__);
    callback_list[reset_callbacks++] = cb;
    vtss_global_unlock(__FILE__, __LINE__);
}

/****************************************************************************/
/****************************************************************************/
int control_flash_erase(vtss_flashaddr_t base, size_t len)
{
  vtss_flashaddr_t err_address;
  int err;
  control_system_flash_lock(); // Prevent system from resetting and this function from being re-entrant
  err = vtss_flash_erase(base, len, &err_address);
  control_system_flash_unlock();
  if(err != VTSS_FLASH_ERR_OK) {
      T_E("An error occurred when attempting to erase " VPRIz " bytes at %p @ %p: %s", len, (void *)base, (void *)err_address, vtss_flash_errmsg(err));
  }
  return err;
}

/****************************************************************************/
/****************************************************************************/
int control_flash_program(vtss_flashaddr_t flash_base, const void *ram_base, size_t len)
{
  vtss_flashaddr_t err_address;
  int err;
  control_system_flash_lock(); // Prevent system from resetting and this function from being re-entrant
  err = vtss_flash_program(flash_base, ram_base, len, &err_address);
  control_system_flash_unlock();
  if(err != VTSS_FLASH_ERR_OK) {
      T_E("An error occurred when attempting to program " VPRIz " bytes to %p @ %p: %s", len, (void *)flash_base, (void *)err_address, vtss_flash_errmsg(err));
  }
  return err;
}

/****************************************************************************/
/****************************************************************************/
int control_flash_read(vtss_flashaddr_t flash_base, void *dest, size_t len)
{
  vtss_flashaddr_t err_address;
  int err;
  control_system_flash_lock(); // Prevent system from resetting and this function from being re-entrant
  err = vtss_flash_read(flash_base, dest, len, &err_address);
  control_system_flash_unlock();
  if(err != VTSS_FLASH_ERR_OK) {
      T_E("An error occurred when attempting to read " VPRIz " bytes from %p @ %p: %s", len, (void *)flash_base, (void *)err_address, vtss_flash_errmsg(err));
  }
  return err;
}


/****************************************************************************/
/****************************************************************************/
void control_dbg_latest_init_modules_get(vtss_init_data_t *data, const char **init_module_func_name)
{
    *data  = dbg_latest_init_modules_data;
    *init_module_func_name = initfun[dbg_latest_init_modules_init_fun_idx].name;
}

const char *control_init_cmd2str(init_cmd_t cmd)
{
    switch (cmd) {
    case INIT_CMD_EARLY_INIT:
        return "EARLY_INIT";
    case INIT_CMD_INIT:
        return "INIT";
    case INIT_CMD_START:
        return "START";
    case INIT_CMD_CONF_DEF:
        return "CONF DEFAULT";
    case INIT_CMD_MASTER_UP:
        return "MASTER UP";
    case INIT_CMD_MASTER_DOWN:
        return "MASTER DOWN";
    case INIT_CMD_SWITCH_ADD:
        return "SWITCH ADD";
    case INIT_CMD_SWITCH_DEL:
        return "SWITCH DELETE";
    case INIT_CMD_SUSPEND_RESUME:
        return "SUSPEND/RESUME";
    case INIT_CMD_WARMSTART_QUERY:
        return "WARMSTART QUERY";
    default:
        return "UNKNOWN COMMAND";
    }
}

/****************************************************************************/
/****************************************************************************/
void control_dbg_init_modules_callback_time_max_print(msg_dbg_printf_t dbg_printf, BOOL clear)
{
    int i;
    if (!clear) {
        dbg_printf("Init Modules Callback           Max time [ms] Callback type\n");
        dbg_printf("------------------------------- ------------- ---------------\n");
    }
    for (i = 0; i < sizeof(initfun) / sizeof(initfun[i]); i++) {
        if (clear) {
            initfun[i].max_callback_ticks = 0;
            initfun[i].max_callback_cmd   = (init_cmd_t)0;
        } else {
            dbg_printf("%-31s " VPRI64Fu("13") " %-15s\n", initfun[i].name, VTSS_OS_TICK2MSEC(initfun[i].max_callback_ticks), control_init_cmd2str(initfun[i].max_callback_cmd));
        }
    }

    if (clear) {
        dbg_printf("Init Modules callback statistics cleared\n");
    }
}

/* - Assertions ----------------------------------------------------- */

vtss_common_assert_cb_t vtss_common_assert_cb=NULL;

void vtss_common_assert_cb_set(vtss_common_assert_cb_t cb)
{
    VTSS_ASSERT(vtss_common_assert_cb == NULL);
    vtss_common_assert_cb = cb;
}

void cap_array_check_dim(size_t idx, size_t max) {
    if (idx > max) {
        printf("Array out of range! %zd > %zd\n", idx, max);
        VTSS_ASSERT(0);
    }
}

vtss::ostream& operator<<(vtss::ostream& o, init_cmd_t c) {
    switch (c) {
        case INIT_CMD_EARLY_INIT:
            o << "EARLY_INIT";
            return o;

        case INIT_CMD_INIT:
            o << "INIT";
            return o;

        case INIT_CMD_START:
            o << "START";
            return o;

        case INIT_CMD_CONF_DEF:
            o << "CONF_DEF";
            return o;

        case INIT_CMD_MASTER_UP:
            o << "MASTER_UP";
            return o;

        case INIT_CMD_MASTER_DOWN:
            o << "MASTER_DOWN";
            return o;

        case INIT_CMD_SWITCH_ADD:
            o << "SWITCH_ADD";
            return o;

        case INIT_CMD_SWITCH_DEL:
            o << "SWITCH_DEL";
            return o;

        case INIT_CMD_SUSPEND_RESUME:
            o << "SUSPEND_RESUME";
            return o;

        case INIT_CMD_WARMSTART_QUERY:
            o << "WARMSTART_QUERY";
            return o;
    }

    o << "<UNKNOWN: " << (int)c << ">";
    return o;
}


/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
