/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _VTSS_TOD_PHY_ENGINE_H_
#define _VTSS_TOD_PHY_ENGINE_H_
#include "main_types.h"

#include "mscc/ethernet/switch/api.h"

#ifdef __cplusplus
extern "C" {
#endif
/**
 * \brief Initialize the PHY engine allocation table.
 * \return nothing.
 **/
void tod_phy_eng_alloc_init(void);

/**
 * \brief Return ist of allocated engines for a port.
 * \param port_no     [IN]  port number that an engine is allocated for.
 * \param engine_list [OUT] array of 4 booleans indicating if an engine i allocated.
 *
 * \return nothing.
 **/
void tod_phy_eng_alloc_get(mesa_port_no_t port_no, BOOL *engine_list);

/**
 * \brief Allocate a PHY engine for a port.
 * \param port_no    [IN]  port number that an engine is allocated for.
 * \param encap_type [IN]  The encapsulation type, that the engine is allocated for.
 *
 * \return allocated engine ID, if no engine can be allocated, MESA_PHY_TS_ENGINE_ID_INVALID is returned.
 **/
mesa_phy_ts_engine_t tod_phy_eng_alloc(mesa_port_no_t port_no, mesa_phy_ts_encap_t encap_type);

/**
 * \brief Free a PHY engine for a port.
 * \param port_no    [IN]  port number that an engine is allocated for.
 * \param eng_id     [IN]  The engine id that is freed.
 *
 * \return nothing.
 **/
void tod_phy_eng_free(mesa_port_no_t port_no, mesa_phy_ts_engine_t eng_id);

#ifdef __cplusplus
}
#endif
#endif // _VTSS_TOD_PHY_ENGINE_H_


// ***************************************************************************
//
//  End of file.
//
// ***************************************************************************
