/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "main.h"
#include "main_types.h"
#include "tod.h"
#include "tod_api.h"
#include "vtss_tod_api.h"
#include "critd_api.h"
#include "interrupt_api.h"      /* interrupt handling */
#include "misc_api.h"

#include "port_api.h"

#include "vtss_tod_mod_man.h"
#include "vtss_timer_api.h"

//#define CALC_TEST

#ifdef CALC_TEST
#include "vtss_ptp_types.h"
#endif

#if defined(VTSS_SW_OPTION_PHY)
#include "phy_api.h"
#endif
#include "main.h"
#define API_INST_DEFAULT PHY_INST 
#define PHY_PORT_NO_START 0

#include "vtss_os_wrapper.h"

/* i2c static functions */
/* unshifted address for device address 0xe0(1110 0000)*/
#define MAX_MAL_5338_I2C_LEN 8
#define MAX_I2C_LEN 3 

#if 0
static int zl30251_device_rd(u16 reg_addr, u8 *data, char len)
{
    u8 buf[MAX_I2C_LEN],addr[2];
    int i2c_zl30251_dev, i2c_zl30251_addr;
    int read_fail = 1;
    mesa_rc rc1, rc2;

    addr[0] = reg_addr>>8;
    addr[1] = reg_addr & 0xff;
    i2c_zl30251_dev = vtss_i2c_dev_open("ZL30251", NULL, &i2c_zl30251_addr);
    if (i2c_zl30251_dev >= 0) {
        while (read_fail > 0 && read_fail < 3) {
            rc1 = vtss_i2c_dev_wr_rd(i2c_zl30251_dev, i2c_zl30251_addr, addr, 2, data, 1);
            rc2 = vtss_i2c_dev_wr_rd(i2c_zl30251_dev, i2c_zl30251_addr, addr, 2, buf, 1);
            if (rc1 != VTSS_RC_OK || rc2 != VTSS_RC_OK || memcmp(data, buf, 1)) {
                read_fail++;
            } else
                read_fail = 0;
        }
        vtss_i2c_dev_close(i2c_zl30251_dev);
    }
    if (read_fail == 0) return len;
    else return 0;
}
#endif

// Do the i2c write to zl30251 on the synce module.
// The device write operation may fail, therefore read back and compare result
static void zl30251_device_wr(u16 reg_addr, u8* data, char len)
{
    u8 buf[MAX_I2C_LEN];
    int write_fail = 1;
    int i2c_zl30251_dev, i2c_zl30251_addr;
    i2c_zl30251_dev = vtss_i2c_dev_open("ZL30251", NULL, &i2c_zl30251_addr);
    if (i2c_zl30251_dev >= 0) {
        while (write_fail > 0 && write_fail < 3) {
            buf[0] = reg_addr>>8;
            buf[1] = reg_addr & 0xff;
            memcpy(&buf[2], data, 1);
            if (vtss_i2c_dev_wr(i2c_zl30251_dev, buf, MAX_I2C_LEN) != VTSS_RC_OK)   T_E("Failed write to ZL30251");
            write_fail = 0;
            vtss_i2c_dev_close(i2c_zl30251_dev);
        }
    }
}

static int mal_si5338_device_rd(u8 reg_addr, u8 * data, char len)
{
    u8 buf[MAX_MAL_5338_I2C_LEN];
    int i2c_si5338_mal_dev, i2c_si5338_mal_addr;
    int read_fail = 1;
    mesa_rc rc1, rc2;
    i2c_si5338_mal_dev = vtss_i2c_dev_open("malibu_silabs", NULL, &i2c_si5338_mal_addr);
    if (i2c_si5338_mal_dev >= 0) {
        while (read_fail > 0 && read_fail < 3) {
            rc1 = vtss_i2c_dev_wr_rd(i2c_si5338_mal_dev, i2c_si5338_mal_addr, &reg_addr, 1, data, len);
            rc2 = vtss_i2c_dev_wr_rd(i2c_si5338_mal_dev, i2c_si5338_mal_addr, &reg_addr, 1, buf, len);
            if (rc1 != VTSS_RC_OK || rc2 != VTSS_RC_OK || memcmp(data, buf, len)) {
                read_fail++;
            } else
                read_fail = 0;
        }
        vtss_i2c_dev_close(i2c_si5338_mal_dev);
    }
    if (read_fail == 0) return len;
    else return 0;
}

// Do the i2c write to si5338 on the synce module.
// The device write operation may fail, therefore read back and compare result
static void mal_si5338_device_wr(u8 reg_addr, u8* data, char len)
{
    u8 buf[MAX_MAL_5338_I2C_LEN];
    int write_fail = 1;
    int i2c_si5338_mal_dev, i2c_si5338_mal_addr;
    i2c_si5338_mal_dev = vtss_i2c_dev_open("malibu_silabs", NULL, &i2c_si5338_mal_addr);
    if (i2c_si5338_mal_dev >= 0) {
        while (write_fail > 0 && write_fail < 3) {
            buf[0] = reg_addr;
            len = len >=MAX_MAL_5338_I2C_LEN ? MAX_MAL_5338_I2C_LEN-1 : len;
            memcpy(&buf[1], data, len);
            if (vtss_i2c_dev_wr(i2c_si5338_mal_dev, &buf[0], len+1) != VTSS_RC_OK)   T_E("Failed write to Malibu SI5338");
            write_fail = 0;
            vtss_i2c_dev_close(i2c_si5338_mal_dev);
        }
    }
}


/* ================================================================= *
 *  Trace definitions
 * ================================================================= */


#if (VTSS_TRACE_ENABLED)

static vtss_trace_reg_t trace_reg = {
    VTSS_TRACE_MODULE_ID, "tod", "Time of day for PTP and OAM etc."
};

static vtss_trace_grp_t trace_grps[TRACE_GRP_CNT] = {
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default (TOD core)",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP|VTSS_TRACE_FLAGS_USEC
    },
    /* VTSS_TRACE_GRP_CLOCK */ {
        "clock",
        "TOD time functions",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_MOD_MAN */ {
        "mod_man",
        "PHY Timestamp module manager",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PHY_TS */ {
        "phy_ts",
        "PHY Timestamp feature",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_REM_PHY */ {
        "rem_phy",
        "PHY Remote Timestamp feature",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* VTSS_TRACE_GRP_PHY_ENG */ {
        "phy_eng",
        "PHY Engine allocation feature",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
};
#endif /* VTSS_TRACE_ENABLED */

static struct {
    BOOL ready;                 /* TOD Initited  */
    critd_t datamutex;          /* Global data protection */
} tod_global;


/* ================================================================= *
 *  Configuration definitions
 * ================================================================= */

typedef struct tod_config_t {
    mesa_packet_internal_tc_mode_t     int_mode;       /* internal timestamping mode */
    mesa_phy_ts_clockfreq_t            freq;           /* timestamping reference clock frequency */
    CapArray <BOOL, MESA_CAP_PORT_CNT> phy_ts_enable;  /* enable PHY timestamping mode */
} tod_config_t;

static tod_config_t config_data ;
/*
 * Propagate the PTP (module) configuration to the PTP core
 * library.
 */
static void
tod_conf_propagate(void)
{
    T_I("TOD Configuration has been reset, you need to reboot to activate the changed conf." );
}


/**
 * Read the PTP configuration.
 * \param create indicates a new default configuration block should be created.
 *
 */
static void
tod_conf_read(BOOL create)
{
    BOOL            tod_conf_changed = FALSE;

    TOD_DATA_LOCK();
    /* Check if configuration changed */
        if (config_data.int_mode != MESA_PACKET_INTERNAL_TC_MODE_30BIT) tod_conf_changed = TRUE;
    /* initialize run-time options to reasonable values */
    config_data.int_mode = MESA_PACKET_INTERNAL_TC_MODE_30BIT;
    if (MESA_CAP(MEBA_CAP_1588_REF_CLK_SEL)) {
        config_data.freq = MESA_PHY_TS_CLOCK_FREQ_250M; /* 250 MHz is default on the PCB107 board */
    }
    if (tod_conf_changed) {
        T_I("TOD Configuration has been reset, you need to reboot to activate the changed conf." );
    }

    TOD_DATA_UNLOCK();

}
/****************************************************************************
 * Configuration API
 ****************************************************************************/

BOOL tod_tc_mode_get(mesa_packet_internal_tc_mode_t *mode)
{
    BOOL ok = FALSE;
    TOD_DATA_LOCK();
    ok = TRUE;
    *mode = config_data.int_mode;
    TOD_DATA_UNLOCK();
    return ok;
}
BOOL tod_tc_mode_set(mesa_packet_internal_tc_mode_t *mode)
{
    BOOL ok = FALSE;
    TOD_DATA_LOCK();
    if (*mode < MESA_PACKET_INTERNAL_TC_MODE_MAX) {
        ok = TRUE;
        config_data.int_mode = *mode;
    }
    TOD_DATA_UNLOCK();
    return ok;
}

BOOL tod_port_phy_ts_get(BOOL *ts, mesa_port_no_t portnum)
{
    BOOL ok = FALSE;
    *ts = FALSE;

    if (MESA_CAP(MESA_CAP_PHY_TS)) {
        TOD_DATA_LOCK();
        if (portnum < port_isid_port_count(VTSS_ISID_LOCAL)) {
            *ts = config_data.phy_ts_enable[portnum];
            ok = TRUE;
        }
        TOD_DATA_UNLOCK();
    }
    return ok;
}

BOOL tod_port_phy_ts_set(BOOL *ts, mesa_port_no_t portnum)
{
    BOOL ok = FALSE;

    if (MESA_CAP(MESA_CAP_PHY_TS)) {
        TOD_DATA_LOCK();
        if (portnum < port_isid_port_count(VTSS_ISID_LOCAL)) {
            config_data.phy_ts_enable[portnum] = *ts;
            ok = TRUE;
        }
        TOD_DATA_UNLOCK();
    }
    return ok;
}

BOOL tod_ref_clock_freg_get(mesa_phy_ts_clockfreq_t *freq)
{
    BOOL ok = FALSE;
    TOD_DATA_LOCK();
    ok = TRUE;
    *freq = config_data.freq;
    TOD_DATA_UNLOCK();
    return ok;
}

BOOL tod_ref_clock_freg_set(mesa_phy_ts_clockfreq_t *freq)
{
    BOOL ok = FALSE;
    TOD_DATA_LOCK();
    if (MESA_CAP(MEBA_CAP_1588_REF_CLK_SEL)) {
        if (*freq < MESA_PHY_TS_CLOCK_FREQ_MAX) {
            ok = TRUE;
            config_data.freq = *freq;
            if (!tod_global.ready) {
                T_I("The 1588 reference clock is set");
            } else {
                T_W("The 1588 reference clock has been changed, please save configuration and reboot to apply the change");
            }
        }
    } else {
        T_W("The 1588 reference clock freq set is not supported");
    }
    TOD_DATA_UNLOCK();
    return ok;
}

BOOL tod_ready(void)
{
    return tod_global.ready;
}

/****************************************************************************
 * Callbacks
 ****************************************************************************/
#ifdef CALC_TEST

#define TC(t,c) ((t<<16) + c)
static void calcTest(void)
{
    u32 r, x, y;
    mesa_timeinterval_t t;
    char str1 [30];

/* sub */    
    x = TC(43,4321);
    y = TC(41,1234); 
    vtss_tod_ts_cnt_sub(&r, x, y); /* no wrap */
    T_W("%ld,%ld - %ld,%ld = %ld,%ld", x>>16,x & 0xffff,y>>16,y & 0xffff,r>>16,r & 0xffff );
    x = TC(43,4321);
    y = TC(41,4321); 
    vtss_tod_ts_cnt_sub(&r, x, y); /* no wrap */
    T_W("%ld,%ld - %ld,%ld = %ld,%ld", x>>16,x & 0xffff,y>>16,y & 0xffff,r>>16,r & 0xffff );
    x = TC(41,4321);
    y = TC(41,1234); 
    vtss_tod_ts_cnt_sub(&r, x, y); /* no wrap */
    T_W("%ld,%ld - %ld,%ld = %ld,%ld", x>>16,x & 0xffff,y>>16,y & 0xffff,r>>16,r & 0xffff );
    x = TC(41,4321);
    y = TC(42,1234); 
    vtss_tod_ts_cnt_sub(&r, x, y); /* tick wrap */
    T_W("%ld,%ld - %ld,%ld = %ld,%ld", x>>16,x & 0xffff,y>>16,y & 0xffff,r>>16,r & 0xffff );
    x = TC(43,4321);
    y = TC(41,4322); 
    vtss_tod_ts_cnt_sub(&r, x, y); /* clk wrap */
    T_W("%ld,%ld - %ld,%ld = %ld,%ld", x>>16,x & 0xffff,y>>16,y & 0xffff,r>>16,r & 0xffff );

/* add */    
    x = TC(41,1234);
    y = TC(43,4321); 
    vtss_tod_ts_cnt_add(&r, x, y); /* no wrap */
    T_W("%ld,%ld + %ld,%ld = %ld,%ld", x>>16,x & 0xffff,y>>16,y & 0xffff,r>>16,r & 0xffff );
    x = TC(41,1234);
    y = TC(43,8781); 
    vtss_tod_ts_cnt_add(&r, x, y); /* no wrap */
    T_W("%ld,%ld + %ld,%ld = %ld,%ld", x>>16,x & 0xffff,y>>16,y & 0xffff,r>>16,r & 0xffff );
    x = TC(41,1234);
    y = TC(43,8782); 
    vtss_tod_ts_cnt_add(&r, x, y); /* clk wrap */
    T_W("%ld,%ld + %ld,%ld = %ld,%ld", x>>16,x & 0xffff,y>>16,y & 0xffff,r>>16,r & 0xffff );
    x = TC(41,1234);
    y = TC(43,8783); 
    vtss_tod_ts_cnt_add(&r, x, y); /* clk wrap */
    T_W("%ld,%ld + %ld,%ld = %ld,%ld", x>>16,x & 0xffff,y>>16,y & 0xffff,r>>16,r & 0xffff );
    x = TC(41,1234);
    y = TC(58,4321); 
    vtss_tod_ts_cnt_add(&r, x, y); /* no wrap */
    T_W("%ld,%ld + %ld,%ld = %ld,%ld", x>>16,x & 0xffff,y>>16,y & 0xffff,r>>16,r & 0xffff );
    x = TC(48,1234);
    y = TC(52,4321); 
    vtss_tod_ts_cnt_add(&r, x, y); /* tick wrap */
    T_W("%ld,%ld + %ld,%ld = %ld,%ld", x>>16,x & 0xffff,y>>16,y & 0xffff,r>>16,r & 0xffff );


/* timeinterval to cnt */
    t = VTSS_SEC_NS_INTERVAL(0,123456);
    vtss_tod_timeinterval_to_ts_cnt(&r, t);
    T_W("TimeInterval: %s => cnt %ld,%ld", TimeIntervalToString (&t, str1, '.'),r>>16,r & 0xffff );
    t = VTSS_SEC_NS_INTERVAL(0,123456789);
    vtss_tod_timeinterval_to_ts_cnt(&r, t);
    T_W("TimeInterval: %s => cnt %ld,%ld", TimeIntervalToString (&t, str1, '.'),r>>16,r & 0xffff );
    t = VTSS_SEC_NS_INTERVAL(0,999999000);
    vtss_tod_timeinterval_to_ts_cnt(&r, t);
    T_W("TimeInterval: %s => cnt %ld,%ld", TimeIntervalToString (&t, str1, '.'),r>>16,r & 0xffff );
    t = VTSS_SEC_NS_INTERVAL(0,999999999);
    vtss_tod_timeinterval_to_ts_cnt(&r, t);
    T_W("TimeInterval: %s => cnt %ld,%ld", TimeIntervalToString (&t, str1, '.'),r>>16,r & 0xffff );
    t = VTSS_SEC_NS_INTERVAL(1,0);
    vtss_tod_timeinterval_to_ts_cnt(&r, t);
    T_W("TimeInterval: %s => cnt %ld,%ld", TimeIntervalToString (&t, str1, '.'),r>>16,r & 0xffff );

/* cnt to timeinterval */
    x = TC(48,1234);
    vtss_tod_ts_cnt_to_timeinterval(&t, x);
    T_W("cnt %ld,%ld => TimeInterval: %s",r>>16,r & 0xffff , TimeIntervalToString (&t, str1, '.'));
    
}
#endif

/****************************************************************************/
/*  Initialization functions                                                */
/****************************************************************************/

static mesa_ts_internal_fmt_t serval_internal(void)
{
    mesa_packet_internal_tc_mode_t mode;
    if (tod_tc_mode_get(&mode)) {
        T_D("Internal mode: %d", mode);
        switch (mode) {
            case MESA_PACKET_INTERNAL_TC_MODE_30BIT: return MESA_TS_INTERNAL_FMT_RESERVED_LEN_30BIT;
            case MESA_PACKET_INTERNAL_TC_MODE_32BIT: return MESA_TS_INTERNAL_FMT_RESERVED_LEN_32BIT;
            case MESA_PACKET_INTERNAL_TC_MODE_44BIT: return MESA_TS_INTERNAL_FMT_SUB_ADD_LEN_44BIT_CF62;
            case MESA_PACKET_INTERNAL_TC_MODE_48BIT: return MESA_TS_INTERNAL_FMT_RESERVED_LEN_48BIT_CF;
            default: return MESA_TS_INTERNAL_FMT_NONE;
        }
    } else {
        return MESA_TS_INTERNAL_FMT_NONE;
    }
}

static void one_pps_pulse_interrupt_handler(meba_event_t source_id, u32 instance_id);

static u32 ns_sample_max(int count)
{
    if (count >= MESA_CAP(MESA_CAP_TOD_SAMPLES_PR_SEC)) {
        return 1000000000;
    } else {
        return count * (1000000000 / MESA_CAP(MESA_CAP_TOD_SAMPLES_PR_SEC)) + (1000000000 / MESA_CAP(MESA_CAP_TOD_SAMPLES_PR_SEC) / 2);
    }
}

/* 
 * sample_count:
 * Jaguar2: The timeofday is sampled more than once pr sec, in order to be able to convert 30 bit timestamps to timeofday
 *          sample_count = 0 => one_sec function and module man are called, and the timer is started
 *          sample_count = 1 => one_sec function is called, and interrupt is enabled
 *          sample_count = 2 =>  interrupt is enabled, this step is caused by the extra interupt that Jaguar2 generates then the one-sec function is called
 */
static uint sample_count;

void tod_one_sec_timer_expired(vtss::Timer *timer)
{
    BOOL ongoing_adj;
    mesa_timestamp_t ts;
    u32              tc;
    TOD_DATA_LOCK();

    if (sample_count < MESA_CAP(MESA_CAP_TOD_SAMPLES_PR_SEC)) {
        TOD_RC(mesa_ts_adjtimer_one_sec(0, &ongoing_adj))
        T_D("ongoing_adj %u", ongoing_adj);
        if (MESA_CAP(MESA_CAP_PHY_TS)) {
            if (sample_count == 0) {
                TOD_RC(ptp_module_man_trig(ongoing_adj));
            }
        }
    }
    TOD_RC(mesa_ts_timeofday_get(NULL, &ts, &tc));
    if (MESA_CAP(MESA_CAP_TOD_SAMPLES_PR_SEC) > 1) {
        if (ts.nanoseconds > ns_sample_max(sample_count)) {
            T_I("Too slow timeofday sampling: sample_count %u, current time (ns) %u, must be less than %u", sample_count, ts.nanoseconds, ns_sample_max(sample_count));
        }
    }
    T_D("sample_count %u, current time (ns) %u", sample_count, ts.nanoseconds);
    if (++sample_count < MESA_CAP(MESA_CAP_TOD_SAMPLES_PR_SEC))
    {
        timer->set_period(vtss::microseconds(1000000 / MESA_CAP(MESA_CAP_TOD_SAMPLES_PR_SEC)));
        if (vtss_timer_start(timer) != VTSS_RC_OK) {
            T_EG(VTSS_TRACE_GRP_CLOCK, "Unable to start tod timer");
        }
    } else {
        if (sample_count > MESA_CAP(MESA_CAP_TOD_SAMPLES_PR_SEC) || MESA_CAP(MESA_CAP_TOD_SAMPLES_PR_SEC) == 1) sample_count = 0;
        TOD_RC(vtss_interrupt_source_hook_set(VTSS_MODULE_ID_TOD,
                                              one_pps_pulse_interrupt_handler,
                                              (meba_event_t)MESA_CAP(MEBA_CAP_ONE_PPS_INT_ID),
                                              INTERRUPT_PRIORITY_NORMAL));
    }
    TOD_DATA_UNLOCK();
}

/*
 * Time stamp age timer expired.
 */
void tod_time_stamp_age_timer_expired(vtss::Timer *timer)
{
    if (MESA_CAP(MESA_CAP_TS_MISSING_TX_INTERRUPT)) {
        // check if any timestamps are updated before ageing
        TOD_RC(mesa_tx_timestamp_update(NULL));
    }
    TOD_RC(mesa_timestamp_age(NULL));
}

/*
 * timer used to delay the One sec action
 */
/*lint -e{459} ... 'timer' only used in the init thread and the Interrupt thread */
static vtss::Timer timer(VTSS_THREAD_PRIO_HIGH);

static void init_one_pps_timer(void) {
    // Create tod timer

    timer.set_repeat(FALSE);
    timer.set_period(vtss::milliseconds(2));     /* 2 msec */
    timer.callback    = tod_one_sec_timer_expired;
    timer.modid       = VTSS_MODULE_ID_TOD;
    T_IG(VTSS_TRACE_GRP_CLOCK, "timer initialized");
}

static vtss::Timer age_timer;
/*
 * timer used for calling Time stamp age function
 */
static void init_time_stamp_age_timer(void) {
    age_timer.set_repeat(TRUE);
    age_timer.set_period(vtss::milliseconds(10));     /* 10 msec */
    age_timer.callback    = tod_time_stamp_age_timer_expired;
    age_timer.modid       = VTSS_MODULE_ID_TOD;
    T_IG(VTSS_TRACE_GRP_CLOCK, "Age timer initialized");
    TOD_RC(vtss_timer_start(&age_timer));
    T_IG(VTSS_TRACE_GRP_CLOCK, "Age timer started");
}

/*
 * TOD Synchronization 1 pps pulse update handler
 */
/*lint -esym(459, one_pps_pulse_interrupt_handler) */
static void one_pps_pulse_interrupt_handler(meba_event_t     source_id,
        u32                         instance_id)
{
    T_N("One sec pulse event: source_id %d, instance_id %u", source_id, instance_id);
    if (MESA_CAP(MESA_CAP_TOD_SAMPLES_PR_SEC) > 1) {
        /* trig ModuleManager */
        /* start 1 ms timer */
        TOD_DATA_LOCK();
        timer.set_period(vtss::microseconds(2));     /* 2 usec */
        if (vtss_timer_start(&timer) != VTSS_RC_OK) {
            T_EG(VTSS_TRACE_GRP_CLOCK, "Unable to start tod timer");
        }
        TOD_DATA_UNLOCK();
    } else {
        BOOL ongoing_adj;
        TOD_RC(mesa_ts_adjtimer_one_sec(0, &ongoing_adj));
        /* trig ModuleManager */
        if (MESA_CAP(MESA_CAP_PHY_TS)) {
            TOD_RC(ptp_module_man_trig(ongoing_adj));
        }
        TOD_RC(vtss_interrupt_source_hook_set(VTSS_MODULE_ID_TOD,
                                              one_pps_pulse_interrupt_handler,
                                              (meba_event_t)MESA_CAP(MEBA_CAP_ONE_PPS_INT_ID),
                                              INTERRUPT_PRIORITY_NORMAL));
    }
}

/*
 * PTP Timestamp update handler
 */
static void timestamp_interrupt_handler(meba_event_t     source_id,
        u32                         instance_id)
{

    T_D("New timestamps detected: source_id %d, instance_id %u", source_id, instance_id);
    TOD_RC(mesa_tx_timestamp_update(0));


    TOD_RC(vtss_interrupt_source_hook_set(VTSS_MODULE_ID_TOD,
                timestamp_interrupt_handler,
                MEBA_EVENT_CLK_TSTAMP,
                INTERRUPT_PRIORITY_NORMAL));
}

static void tod_ts_port_state_change_callback(vtss_isid_t isid,
                               mesa_port_no_t port_no,
                               port_info_t *info)
{
    //vtss_tod_ts_phy_topo_t phy_ts;

    if (!info->stack) {
        // the call to mesa_ts_status_change is also implemented in port.cxx, therefore it is commented out here
        //if (info->link) {
        //    T_I("port_change link up");
        //    if (MESA_CAP(MESA_CAP_PHY_TS)) {
        //        tod_ts_phy_topo_get(port_no, &phy_ts);
        //    }
        //    if (!MESA_CAP(MESA_CAP_PHY_TS) || (MESA_CAP(MESA_CAP_PHY_TS) && phy_ts.ts_feature == VTSS_PTP_TS_NONE)) {
        //        if (mesa_ts_status_change(NULL, port_no) != VTSS_RC_OK) {
        //            T_I("vtss_phy_ts_phy_status_change failed");
        //        } else {
        //            T_I("port_change link up, port %d, speed %d", port_no, info->speed);
        //        }
        //    }
        //} else {
        //    T_I("port_change link down");
        //}
        if (MESA_CAP(MESA_CAP_PHY_TS)) {
            /* refresh the timestamping mode, as 8488 in 1G mode does not support timestamping */
            TOD_RC(ptp_module_man_port_data_update(port_no));
        }
    } else {
        T_W("port_change on stack port");
    }
}

static BOOL is_zl30251_present(void) {
    mesa_phy_10g_id_t phy_id;
    mesa_rc rc;
    if (vtss_board_type() == VTSS_BOARD_JAGUAR2_REF) {
        rc = mesa_phy_10g_id_get(API_INST_DEFAULT, 24, &phy_id);
        if (rc == VTSS_RC_OK && (phy_id.family == MESA_PHY_FAMILY_VENICE)) {
            return TRUE;
        }
    }
    return FALSE;
}

static void zl30251_conf(mesa_phy_ts_clockfreq_t freq) {
    u8 reg_value = 0;
    //X , 0X0009 , 0X02 ; MCR1
    reg_value = 0X02;
    zl30251_device_wr(0X0009,&reg_value,sizeof(reg_value));
    //X , 0X0621 , 0X08 ; PLLTCR18
    reg_value = 0X08;
    zl30251_device_wr(0X0621,&reg_value,sizeof(reg_value));
    //X , 0X0631 , 0X40 ; TCCR2
    reg_value =0X40 ;
    zl30251_device_wr(0X0631,&reg_value,sizeof(reg_value));
    //X , 0X0101 , 0X22 ; APLLCR2
    reg_value = 0X22;
    zl30251_device_wr(0X0101,&reg_value,sizeof(reg_value));
    //X , 0X0102 , 0X01 ; APLLCR3
    reg_value = 0X01;
    zl30251_device_wr(0X0102,&reg_value,sizeof(reg_value));
    //X , 0X010A , 0X30 ; AFBDIV5
    reg_value = 0X30;
    zl30251_device_wr(0X010A,&reg_value,sizeof(reg_value));
    //X , 0X0114 , 0X0D ; AFBBP
    reg_value = 0X0D;
    zl30251_device_wr(0X0114,&reg_value,sizeof(reg_value));
    //X , 0X0120 , 0X06 ; DCO1
    reg_value = 0X06;
    zl30251_device_wr(0X0120,&reg_value,sizeof(reg_value));
    //X , 0X0125 , 0XC0 ; PLLTCR5
    reg_value = 0XC0;
    zl30251_device_wr(0X0125,&reg_value,sizeof(reg_value));
    //X , 0X0126 , 0X40 ; PLLTCR6
    reg_value = 0X40;
    zl30251_device_wr(0X0126,&reg_value,sizeof(reg_value));
    //X , 0X0127 , 0X7F ; PLLTCR7
    reg_value = 0X7F;
    zl30251_device_wr(0X0127,&reg_value,sizeof(reg_value));
    //X , 0X0129 , 0X04 ; PLLTCR9
    reg_value = 0X04;
    zl30251_device_wr(0X0129,&reg_value,sizeof(reg_value));
    //X , 0X012A , 0XB3 ; PLLTCR10
    reg_value = 0XB3;
    zl30251_device_wr(0X012A,&reg_value,sizeof(reg_value));
    //X , 0X012B , 0XC0 ; PLLTCR11
    reg_value = 0XC0;
    zl30251_device_wr(0X012B,&reg_value,sizeof(reg_value));
    //X , 0X012C , 0X80 ; PLLTCR12
    reg_value = 0X80;
    zl30251_device_wr(0X012C,&reg_value,sizeof(reg_value));
    //X , 0X0320 , 0X03 ; IC2CR1
    reg_value = 0X03;
    zl30251_device_wr(0X0320,&reg_value,sizeof(reg_value));
    //X , 0X0340 , 0X03 ; IC3CR1
    reg_value = 0X03;
    zl30251_device_wr(0X0340,&reg_value,sizeof(reg_value));
    //X , 0X0200 , 0X01 ; OC1CR1
    reg_value = 0X01;
    zl30251_device_wr(0X0200,&reg_value,sizeof(reg_value));
    //X , 0X0201 , 0X01 ; OC1CR2
    reg_value = 0X01;
    zl30251_device_wr(0X0201,&reg_value,sizeof(reg_value));
    //X , 0X0202 , 0X10 ; OC1CR3
    reg_value = 0X10;
    zl30251_device_wr(0X0202,&reg_value,sizeof(reg_value));
    //X , 0X0203 , 0X02 ; OC1DIV1
    reg_value = 0X02;
    zl30251_device_wr(0X0203,&reg_value,sizeof(reg_value));
    //X , 0X0210 , 0X01 ; OC2CR1
    reg_value = 0X01;
    zl30251_device_wr(0X0210,&reg_value,sizeof(reg_value));
    //X , 0X0211 , 0X01 ; OC2CR2
    reg_value = 0X01;
    zl30251_device_wr(0X0211,&reg_value,sizeof(reg_value));
    //X , 0X0212 , 0X10 ; OC2CR3
    reg_value = 0X10;
    zl30251_device_wr(0X0212,&reg_value,sizeof(reg_value));
    //X , 0X0213 , 0X02 ; OC2DIV1
    reg_value = 0X02;
    zl30251_device_wr(0X0213,&reg_value,sizeof(reg_value));
    //X , 0X0220 , 0X02 ; OC3CR1
    reg_value = 0X02;
    zl30251_device_wr(0X0220,&reg_value,sizeof(reg_value));
    //X , 0X0221 , 0X06 ; OC3CR2
    reg_value = 0X06;
    zl30251_device_wr(0X0221,&reg_value,sizeof(reg_value));
    //X , 0X0222 , 0X10 ; OC3CR3
    reg_value = 0X10;
    zl30251_device_wr(0X0222,&reg_value,sizeof(reg_value));
    //X , 0X0223 , 0X04 ; OC3DIV1
    reg_value = 0X04;
    zl30251_device_wr(0X0223,&reg_value,sizeof(reg_value));
    //X , 0X000C , 0X01 ; ICEN
    reg_value = 0X01;
    zl30251_device_wr(0X000C,&reg_value,sizeof(reg_value));
    //X , 0X000B , 0X01 ; PLLEN
    reg_value = 0X01;
    zl30251_device_wr(0X000B,&reg_value,sizeof(reg_value));
    //W , 10000
    VTSS_MSLEEP(10);
    //X , 0X000D , 0X07 ; OCEN
    reg_value = 0X07;
    zl30251_device_wr(0X000D,&reg_value,sizeof(reg_value));
#if 0
    reg_value = 0;
    zl30251_device_rd(0x000D,&reg_value,sizeof(reg_value));     
#endif
}
static BOOL is_si5338_present(void) {
    mesa_phy_10g_id_t phy_id;
    mesa_rc rc;
    if (vtss_board_type() == VTSS_BOARD_JAGUAR2_REF) {
        rc = mesa_phy_10g_id_get(API_INST_DEFAULT, 24, &phy_id);
        if (rc == VTSS_RC_OK && (phy_id.family == MESA_PHY_FAMILY_MALIBU)) {
            return TRUE;
        }
    }
    return FALSE;
}

static void si5338_conf(mesa_phy_ts_clockfreq_t freq) {
    u8 si5338_reg =0;
    // Read 0xE0 0xF7 // see what alarms happened in the past
    si5338_reg = 0;
    mal_si5338_device_rd(0xf7,&si5338_reg,sizeof(si5338_reg));
    // Write 0xE0 0xF7 0x00 // clear sticky bits
    si5338_reg = 0;
    mal_si5338_device_wr(0xf7,&si5338_reg,sizeof(si5338_reg));
    // Read 0xE0 0xF7 // confirm if any alarms exist (expected value is 0x08)
    si5338_reg = 0;
    mal_si5338_device_rd(0xf7,&si5338_reg,sizeof(si5338_reg));
    // Read 0xE0 0xE6 // expected value is 0x4
    si5338_reg = 0;
    mal_si5338_device_rd(0xe6,&si5338_reg,sizeof(si5338_reg));
    // Write 0xE0 0xE6 0x14 //per Figure 9 of 5338 Datasheet, Disable Outputs
    si5338_reg = 0x14;
    mal_si5338_device_wr(0xE6,&si5338_reg,sizeof(si5338_reg));
    // Read 0xE0 0xF1 // expected value is 0x65
    si5338_reg = 0;
    mal_si5338_device_rd(0xf1,&si5338_reg,sizeof(si5338_reg));
    // Write 0xE0 0xF1 0xE5 //per Figure 9 of 5338 Datasheet, Pause LOL
    si5338_reg = 0xE5;
    mal_si5338_device_wr(0xf1,&si5338_reg,sizeof(si5338_reg));
    // Read 0xE0 0x1C // expected value is 0x16
    si5338_reg = 0;
    mal_si5338_device_rd(0x1c,&si5338_reg,sizeof(si5338_reg));
    // Read 0xE0 0x1D // expected value is 0x90
    si5338_reg = 0;
    mal_si5338_device_rd(0x1d,&si5338_reg,sizeof(si5338_reg));
    // Write 0xE0 0x1C 0x0A  // reconfigure input for IN3 not IN1/2 xtal
    si5338_reg = 0x0A;
    mal_si5338_device_wr(0x1c,&si5338_reg,sizeof(si5338_reg));
    // Write 0xE0 0x1D 0x08 // reconfigure input for IN3 not IN1/2 xtal
    si5338_reg = 0x08;
    mal_si5338_device_wr(0x1d,&si5338_reg,sizeof(si5338_reg));
    // Read 0xE0 0xDA // expect 0x08, validate input clock status  if not 0x8 investigate input clock
    si5338_reg = 0;
    mal_si5338_device_rd(0xda,&si5338_reg,sizeof(si5338_reg));
    // Read 0xE0 0x31  // expect 0x80
    si5338_reg = 0;
    mal_si5338_device_rd(0x31,&si5338_reg,sizeof(si5338_reg));
    // Write 0xE0 0x31 0x00 // Configure PLL for locking, set FCAL_OVRD_EN
    si5338_reg = 0;
    mal_si5338_device_wr(0x31,&si5338_reg,sizeof(si5338_reg));
    // Write 0xE0 0xF6 0x02 // soft reset then wait 25ms
    si5338_reg = 0x2;
    mal_si5338_device_wr(0xf6,&si5338_reg,sizeof(si5338_reg));
    // Write 0xE0 0xF1 0x65 // return to original value, restart LOL
    si5338_reg = 0x65;
    mal_si5338_device_wr(0xf1,&si5338_reg,sizeof(si5338_reg));
    // Read 0xE0 0xDA // likely see 0x18 alarms if not, then something is not right . need to debug.
    si5338_reg = 0;
    mal_si5338_device_rd(0xda,&si5338_reg,sizeof(si5338_reg));
    // Read 0xE0 0xED // fcal setting . likely see 0x00
    si5338_reg = 0;
    mal_si5338_device_rd(0xed,&si5338_reg,sizeof(si5338_reg));
    // Write 0xE0 0x2F <value from above . only bits 1:0> // copy fcal setting over
    mal_si5338_device_wr(0x2f,&si5338_reg,sizeof(si5338_reg));
    // Read 0xE0 0xEC // fcal setting . likely see 0x01
    si5338_reg = 0;
    mal_si5338_device_rd(0xec,&si5338_reg,sizeof(si5338_reg));
    // Write 0xE0 0x2E <value from above> // copy fcal setting over
    mal_si5338_device_wr(0x2e,&si5338_reg,sizeof(si5338_reg));
    // Read 0xE0 0xEB // fcal setting . likely see 0xEC
    si5338_reg = 0;
    mal_si5338_device_rd(0xeb,&si5338_reg,sizeof(si5338_reg));
    // Write 0xE0 0x2D <value from above> // copy fcal setting over
    si5338_reg = 0xec;
    mal_si5338_device_wr(0x2d,&si5338_reg,sizeof(si5338_reg));
    // Read 0xE0 0x2F // part of a read modify write
    si5338_reg = 0;
    mal_si5338_device_rd(0x2f,&si5338_reg,sizeof(si5338_reg));
    // Write 0xE0 0x2F <value from above OR 0x14>
    si5338_reg = 0x14;
    mal_si5338_device_wr(0x2f,&si5338_reg,sizeof(si5338_reg));
    // Write 0xE0 0x31 0x80 // return to original value, FCAL_OVRD_EN
    si5338_reg = 0x80;
    mal_si5338_device_wr(0x31,&si5338_reg,sizeof(si5338_reg));
    // Write 0xE0 0xE6 0x04 // return to original value, enable outputs
    si5338_reg = 0x4;
    mal_si5338_device_wr(0xe6,&si5338_reg,sizeof(si5338_reg));
    si5338_reg = freq == MESA_PHY_TS_CLOCK_FREQ_125M ? 8 :
        freq == MESA_PHY_TS_CLOCK_FREQ_15625M ? 6 : 3;
    mal_si5338_device_wr(0x36,&si5338_reg,sizeof(si5338_reg));
}

extern "C" int tod_icli_cmd_register();

mesa_rc
tod_init(vtss_init_data_t *data)
{
    vtss_isid_t isid = data->isid;

    switch (data->cmd) {
    case INIT_CMD_EARLY_INIT:
        /* Initialize and register trace ressources */
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);
        break;
    case INIT_CMD_INIT:
        tod_global.ready = FALSE;
        critd_init(&tod_global.datamutex, "tod_data", VTSS_MODULE_ID_TOD, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
        TOD_DATA_UNLOCK();
        //critd_init(&tod_global.coremutex, "tod_core", VTSS_MODULE_ID_TOD, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
        //TOD_CORE_UNLOCK();
        if (MESA_CAP(MESA_CAP_PHY_TS)) {
            TOD_RC(ptp_module_man_init());
        }
        tod_icli_cmd_register();
        T_I("INIT_CMD_INIT TOD" );
        break;
    case INIT_CMD_START:
        T_I("INIT_CMD_START TOD");
        break;
    case INIT_CMD_CONF_DEF:
        tod_conf_read(TRUE);
        tod_conf_propagate();
        T_I("INIT_CMD_CONF_DEF TOD" );
        break;
    case INIT_CMD_MASTER_UP:
        T_I("INIT_CMD_MASTER_UP ");
        tod_conf_read(FALSE);
#ifdef CALC_TEST
        calcTest();
#endif
        break;
    case INIT_CMD_SWITCH_ADD:
        if (isid == VTSS_ISID_START) {
            if (MESA_CAP(MESA_CAP_TS_INTERNAL_MODE_SUPPORTED)) {
                mesa_ts_internal_mode_t mode;
                mode.int_fmt = serval_internal();
                T_D("Internal mode.int_fmt: %d", mode.int_fmt);
                TOD_RC(mesa_ts_internal_mode_set(NULL, &mode));
            }

            if (MESA_CAP(MESA_CAP_PHY_TS)) {
                mesa_phy_ts_clockfreq_t freq;
                if (MESA_CAP(MEBA_CAP_1588_REF_CLK_SEL)) {
                    if (tod_ref_clock_freg_get(&freq)) {
                        if (freq < MESA_PHY_TS_CLOCK_FREQ_MAX) {
                            if (MESA_CAP(MESA_CAP_PHY_MALIBU_10G_PLUGIN_MOUNTED)) {
                                if (is_zl30251_present() == TRUE) {
                                    zl30251_conf(freq);
                                } else if (is_si5338_present() == TRUE ) {
                                    si5338_conf(freq);
                                }
                            } else {
                                // update the clock divider in the Si5338 clock generator for clock 0.
                                // Fortunately the can set to 125, 156,25 or 250 MHz by only changing the CLK0_INTDIV register.
                                // For other frequencies we need to change the internal frequency, which will disturb the other clock 
                                // outputs from the clock generator, including the CPU clock. Therefore only these frequencies are supported.
                                // currently only relevant for PCB107, which is not supported in Lunux builds
                                //u8 buf[MAX_5338_I2C_LEN];
                                //buf[0] = SI5338_REG_CLK0_INTDIV_15_8;  /* reg addr */
                                //buf[1] = freq == MESA_PHY_TS_CLOCK_FREQ_125M ? 8 :
                                //    freq == MESA_PHY_TS_CLOCK_FREQ_15625M ? 6 : 3;
                                //if (VTSS_RC_OK != vtss_i2c_wr(NULL, SI5338_I2C_ADDR, &buf[0], MAX_5338_I2C_LEN, 50, VTSS_I2C_NO_MULTIPLEXER)) {
                                //    T_W("Error writing to si_5338_device");
                                //}
                                //T_I("The 1588 reference clock is set");
                            }
                        }
                    }
                } else {
                    /* tbd if configurable for Jaguar2 boards */
                    T_I("The 1588 reference clock can not be set in this architecture");
                }
            }
            (void) port_global_change_register(VTSS_MODULE_ID_TOD, tod_ts_port_state_change_callback);
            
            init_one_pps_timer();

            if (MESA_CAP(MESA_CAP_PHY_TS)) {
                TOD_RC(ptp_module_man_resume());
            }

            if (MESA_CAP(MESA_CAP_TS)) {
                TOD_RC(vtss_interrupt_source_hook_set(VTSS_MODULE_ID_TOD,
                                                      one_pps_pulse_interrupt_handler,
                                                      (meba_event_t)MESA_CAP(MEBA_CAP_ONE_PPS_INT_ID),
                                                      INTERRUPT_PRIORITY_NORMAL));
            }

            if (!MESA_CAP(MESA_CAP_TS_MISSING_TX_INTERRUPT)) {
                T_I("Enabling timestamp interrupt");
                TOD_RC(vtss_interrupt_source_hook_set(VTSS_MODULE_ID_TOD,
                            timestamp_interrupt_handler,
                            MEBA_EVENT_CLK_TSTAMP,
                            INTERRUPT_PRIORITY_NORMAL));
            }

            if (MESA_CAP(MESA_CAP_TS)) {
                init_time_stamp_age_timer();
            }

            tod_global.ready = TRUE;
        } else {
            T_E("INIT_CMD_SWITCH_ADD - unknown ISID %u", isid);
        }
        T_I("INIT_CMD_SWITCH_ADD - ISID %u", isid);
        break;
    case INIT_CMD_MASTER_DOWN:
        T_I("INIT_CMD_MASTER_DOWN - ISID %u", isid);
        break;
    default:
        break;
    }

    return 0;
}

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
