/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "mscc/ethernet/switch/api.h"
#include "icli_porting_util.h"
#include "tod_icli_functions.h"


#include "tod_api.h"
#include "vtss_tod_api.h"
#include "vtss_tod_phy_engine.h"
#include "vtss_tod_mod_man.h"
#include "misc_api.h"

/***************************************************************************/
/*  Internal types                                                         */
/***************************************************************************/
/*
 * phy_ts configuration.
 */
typedef struct {
    BOOL enable;
    BOOL disable;
} vtss_tod_phy_ts_conf_t;

/*
 * engine configuration.
 */
typedef struct {
    BOOL set;
    mesa_phy_ts_encap_t encap;
} vtss_tod_engine_conf_t;

/*
 * phy monitor configuration.
 */
typedef struct {
    BOOL enable;
    BOOL disable;
} vtss_tod_phy_monitor_conf_t;


/***************************************************************************/
/*  Internal functions                                                     */
/***************************************************************************/

static icli_rc_t tod_icli_config_traverse_ports(i32 session_id, int clockinst,
                                           icli_stack_port_range_t *port_type_list_p, void *port_cfg,
                                           icli_rc_t (*cfg_function)(i32 session_id, int inst, mesa_port_no_t uport, void *cfg))
{
    icli_rc_t rc =  ICLI_RC_OK;
    u32             range_idx, cnt_idx;
    vtss_isid_t     isid;
    mesa_port_no_t  uport;
    switch_iter_t   sit;
    port_iter_t     pit;

    if (port_type_list_p) { //at least one range input
        for (range_idx = 0; range_idx < port_type_list_p->cnt; range_idx++) {
            isid = port_type_list_p->switch_range[range_idx].isid;
            for (cnt_idx = 0; cnt_idx < port_type_list_p->switch_range[range_idx].port_cnt; cnt_idx++) {
                uport = port_type_list_p->switch_range[range_idx].begin_uport + cnt_idx;

                //ignore stacking port
                if (port_isid_port_no_is_stack(isid, uport2iport(uport))) {
                    continue;
                }
                if (ICLI_RC_OK != cfg_function(session_id, clockinst, uport, port_cfg)) {
                    rc = ICLI_RC_ERROR;
                }
            }
        }
    } else { //show all port configuration
        (void) switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_USID_CFG);
        while (switch_iter_getnext(&sit)) {
            (void) port_iter_init(&pit, NULL, sit.isid, PORT_ITER_SORT_ORDER_UPORT, PORT_ITER_FLAGS_NORMAL);
            while (port_iter_getnext(&pit)) {
                if (ICLI_RC_OK != cfg_function(session_id, clockinst, pit.uport, port_cfg)) {
                    rc = ICLI_RC_ERROR;
                }
            }
        }
    }
    return rc;
}

static const char* engine_name(mesa_phy_ts_engine_t eng_id) 
{
    switch (eng_id) {
        case    MESA_PHY_TS_PTP_ENGINE_ID_0:  return " ID_00";
        case    MESA_PHY_TS_PTP_ENGINE_ID_1:  return " ID_01";
        case    MESA_PHY_TS_OAM_ENGINE_ID_2A: return " ID_2A";
        case    MESA_PHY_TS_OAM_ENGINE_ID_2B: return " ID_2B";
        default                             : return "INVALID";
    }
}


/***************************************************************************/
/*  Functions called by iCLI                                               */
/***************************************************************************/

BOOL tod_icli_runtime_phy_timestamping_present(u32 session_id, icli_runtime_ask_t ask, icli_runtime_t *runtime)
{
    switch (ask) {
        case ICLI_ASK_PRESENT:
            if (MESA_CAP(MESA_CAP_PHY_TS)) {
                runtime->present = TRUE;
            } else {
                runtime->present = FALSE;
            }
            return TRUE;
        default:
            break;
    }
    return FALSE;
}

static icli_rc_t my_tod_icli_debug_phy_ts(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_tod_phy_ts_conf_t *conf = (vtss_tod_phy_ts_conf_t *)cfg;
    mesa_port_no_t     port_no;
    BOOL enabled;

    port_no = uport2iport(uport);
    if(conf->enable || conf->disable) {
        if(!tod_port_phy_ts_set(&conf->enable, port_no)) {
            ICLI_PRINTF("Failed to set the phy ts mode for port %d\n", uport);
        } else {
            ICLI_PRINTF("Port %d: Successfully %s the phy ts mode\n", uport, conf->enable ? "enable" : "disable");
        }
    } else {
        if(!tod_port_phy_ts_get(&enabled, port_no)) {
            ICLI_PRINTF("Failed to get the the phy ts mode for port %d\n", uport);
        } else {
            ICLI_PRINTF("Phy ts mode port %d = %s\n", uport, enabled ? "enable" : "disable");
        }
    }
    return rc;
}

icli_rc_t tod_icli_debug_phy_ts(i32 session_id, BOOL has_enable, BOOL has_disable, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_tod_phy_ts_conf_t conf;
    conf.enable = has_enable;
    conf.disable = has_disable;
    rc = tod_icli_config_traverse_ports(session_id, 0, v_port_type_list, &conf, my_tod_icli_debug_phy_ts);
    return rc;
}

static icli_rc_t my_tod_icli_debug_engine_alloc(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_tod_engine_conf_t *conf = (vtss_tod_engine_conf_t *)cfg;
    mesa_phy_ts_engine_t eng_id;
    BOOL engine_list[4];
    mesa_port_no_t     port_no;

    port_no = uport2iport(uport);
    if(conf->set) {
        eng_id = tod_phy_eng_alloc(port_no, conf->encap);
        if (eng_id <= MESA_PHY_TS_ENGINE_ID_INVALID) {
            ICLI_PRINTF("Allocated engine %s for port %d\n", engine_name(eng_id), uport);
        }
    } else {
        tod_phy_eng_alloc_get(port_no, engine_list);
        ICLI_PRINTF("Port %d Engines allocated: %s%s%s%s\n", uport, 
                engine_list[0] ? engine_name(MESA_PHY_TS_PTP_ENGINE_ID_0) : "", engine_list[1] ? engine_name(MESA_PHY_TS_PTP_ENGINE_ID_1) : "", 
                engine_list[2] ? engine_name(MESA_PHY_TS_OAM_ENGINE_ID_2A) : "", engine_list[3] ? engine_name(MESA_PHY_TS_OAM_ENGINE_ID_2B) : "" );
    }
    return rc;
}

icli_rc_t tod_icli_debug_engine_alloc(i32 session_id, BOOL has_set, u32 encap, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_tod_engine_conf_t conf;
    conf.set = has_set;
    conf.encap = (mesa_phy_ts_encap_t)encap;
    rc = tod_icli_config_traverse_ports(session_id, 0, v_port_type_list, &conf, my_tod_icli_debug_engine_alloc);
    return rc;
}

static icli_rc_t my_tod_icli_debug_engine_free(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    mesa_phy_ts_engine_t *eng_id = (mesa_phy_ts_engine_t *)cfg;
    mesa_port_no_t     port_no;

    port_no = uport2iport(uport);
    if (*eng_id <= MESA_PHY_TS_ENGINE_ID_INVALID) {
        tod_phy_eng_free(port_no, *eng_id);
        ICLI_PRINTF("Free'ed engine %s for port %d\n", engine_name(*eng_id), uport);
    }
    return rc;
}

icli_rc_t tod_icli_debug_engine_free(i32 session_id, u32 engine, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    rc = tod_icli_config_traverse_ports(session_id, 0, v_port_type_list, &engine, my_tod_icli_debug_engine_free);
    return rc;
}


static icli_rc_t my_tod_icli_debug_tod_monitor(i32 session_id, int inst, mesa_port_no_t uport, void *cfg)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_tod_phy_monitor_conf_t *conf = (vtss_tod_phy_monitor_conf_t *)cfg;
    mesa_port_no_t     port_no;

    port_no = uport2iport(uport);
    if(conf->enable || conf->disable) {
        if (VTSS_RC_OK == ptp_module_man_time_slave_timecounter_enable_disable(port_no, conf->enable)) {
            ICLI_PRINTF("%s monitoring of port %d\n", conf->enable ? "Enable" : "Disable", uport);
        } else {
            ICLI_PRINTF("Enable/disable failed\n");
        }
    }
    return rc;
}

icli_rc_t tod_icli_debug_tod_monitor(i32 session_id, BOOL has_enable, BOOL has_disable, icli_stack_port_range_t *v_port_type_list)
{
    icli_rc_t rc =  ICLI_RC_OK;
    vtss_tod_phy_monitor_conf_t conf;
    conf.enable = has_enable;
    conf.disable = has_disable;
    rc = tod_icli_config_traverse_ports(session_id, 0, v_port_type_list, &conf, my_tod_icli_debug_tod_monitor);
    return rc;
}

/****************************************************************************/
/*  End of file.                                                            */
/****************************************************************************/
