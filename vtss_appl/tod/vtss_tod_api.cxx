/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
/* vtss_tod_api.c */

#include "vtss_tod_api.h"
#include "tod.h"
#include "main.h"

#include "mscc/ethernet/switch/api.h"
#include "vtss/basics/i128.hxx"

#include "vtss_tod_mod_man.h"

// -------------------------------------------------------------------------
// vtss_tod_gettimeofday()
// Get the current time in a Timestamp
// Jaguar : get time from API layer (SW) with 4 ns resolution (only domain 0).
// Luton26: get time from API layer (HW) with 4 ns resolution (only domain 0).
// Serval1: get time from API layer (HW) with 4 ns resolution (only domain 0).
// Jaguar2: get time from API layer (HW) with 4 ns resolution (domain 0..2).
void vtss_tod_gettimeofday(u32 domain, mesa_timestamp_t *ts, u32 *tc)
{
    TOD_RC(mesa_ts_domain_timeofday_get(NULL, domain, ts, tc));
}

// -------------------------------------------------------------------------
// vtss_tod_settimeofday()
// Set the current time from a Timestamp
// Jaguar : set time in API layer (SW).
// Luton26: set time in API layer (HW).
void vtss_tod_settimeofday(const u32 domain, const mesa_timestamp_t *ts)
{
    TOD_RC(mesa_ts_domain_timeofday_set(NULL, domain, ts));
}

// -------------------------------------------------------------------------
// vtss_tod_settimeofday_delta()
// Set delta from the current time 
// Jaguar         : set time in API layer (SW).
// Luton26, Serval: set time in API layer (HW).
void vtss_tod_settimeofday_delta(const u32 domain, const mesa_timestamp_t *ts, BOOL negative)
{
    TOD_RC(mesa_ts_domain_timeofday_set_delta(NULL, domain, ts, negative));
}

// -------------------------------------------------------------------------
// vtss_tod_get_ts_cnt()
// Get the current time as a time counter
// Jaguar : get time from API layer (SW) with 4 ns resolution.
// Luton26: get time from API layer (HW) with 4 ns resolution.
u32 vtss_tod_get_ts_cnt(void)
{
    mesa_timestamp_t ts;
    u32 tc;
    TOD_RC(mesa_ts_timeofday_get(NULL, &ts, &tc));
    return tc;
}

// -------------------------------------------------------------------------
// vtss_tod_ts_to_time()
// Convert a hw time counter to a timestamp, assuming that the hw time
// counter has not wrapped more than once
void vtss_tod_ts_to_time(u32 domain, u32 hw_time, mesa_timestamp_t *ts)
{
    if (MESA_CAP(MESA_CAP_TOD_SAMPLES_PR_SEC) > 1) {
        const u32 HALF_PERIOD = (((u32)(MESA_CAP(MESA_CAP_TS_HW_TIME_WRAP_LIMIT) - 1))>>1);
        u32 tc;
        u32 diff;
        /* in Jaguar 2 the mesa_ts_timeofday_get may return an old value (it is sampled twice pr sec. */
        TOD_RC(mesa_ts_domain_timeofday_get(NULL, domain, ts, &tc));
        /* add time counter difference */
        T_DG(_C,"half_period = %u", HALF_PERIOD);
        T_DG(_C,"now: ts_sec = %d,  ts_nsec = %d, tc = %u",ts->seconds, ts->nanoseconds, tc);
        diff = tc-hw_time;
        if (((tc < HALF_PERIOD) && (hw_time > tc && hw_time < tc + HALF_PERIOD)) ||   /* tc is in the lower half of the period */
            ((tc >= HALF_PERIOD) && (hw_time > tc || hw_time < tc - HALF_PERIOD)))  { /* tc is in the upper half of the period */
            /* hw_time is newer than tc */
            diff = hw_time -tc;
            T_IG(_C,"hw_time is newer than tc: tc = %u,  hw_time = %u, diff = %u",tc, hw_time, diff);
            while (diff > MESA_CAP(MESA_CAP_TS_HW_TIME_CNT_PR_SEC)) {
                ++ts->seconds;
                diff -= MESA_CAP(MESA_CAP_TS_HW_TIME_CNT_PR_SEC);
                T_IG(_C,"big diff: tc = %u, hw_time = %u",tc, hw_time);
            }
            diff = diff * MESA_CAP(MESA_CAP_TS_HW_TIME_NSEC_PR_CNT);
            if (diff > MESA_CAP(MESA_CAP_TS_HW_TIME_CNT_PR_SEC) - ts->nanoseconds) {
                ++ts->seconds;
                ts->nanoseconds -= 1000000000L;
            }
            ts->nanoseconds += diff;
        } else { /* hw_time is older than tc */
            diff = tc-hw_time;
            T_IG(_C,"hw_time is older than tc: tc = %u,  hw_time = %u, diff = %u",tc, hw_time, diff);
            while (diff > MESA_CAP(MESA_CAP_TS_HW_TIME_CNT_PR_SEC)) {
                --ts->seconds;
                diff -= MESA_CAP(MESA_CAP_TS_HW_TIME_CNT_PR_SEC);
                T_IG(_C,"big diff: tc = %u, hw_time = %u",tc, hw_time);
            }
            diff = diff * MESA_CAP(MESA_CAP_TS_HW_TIME_NSEC_PR_CNT);
            if (diff > ts->nanoseconds) {
                --ts->seconds;
                ts->nanoseconds += 1000000000L;
            }
            ts->nanoseconds -= diff;
        }
        T_DG(_C,"then: ts_sec = %d,  ts_nsec = %d, hw_time = %u",ts->seconds, ts->nanoseconds, hw_time);
        
        return;
    } else {
        u32 tc;
        u32 diff;
        TOD_RC(mesa_ts_timeofday_get(NULL, ts, &tc));
        /* add time counter difference */
        T_DG(_C,"now: ts_sec = %d,  ts_nsec = %d, tc = %u",ts->seconds, ts->nanoseconds, tc);
        diff = tc-hw_time;
        if (tc < hw_time) { /* time counter has wrapped */
            diff += MESA_CAP(MESA_CAP_TS_HW_TIME_WRAP_LIMIT);
            T_IG(_C,"counter wrapped: tc = %u,  hw_time = %u, diff = %u",tc, hw_time, diff);
        }
        /* clock rate offset adjustment */
        //if (adjust_divisor) {
        //    T_IG(_C,"diff before ns: %u", diff);
        //    diff += (i32)diff/adjust_divisor;
        //    T_IG(_C,"diff after ns: %u", diff);
        //}
        
        while (diff > MESA_CAP(MESA_CAP_TS_HW_TIME_CNT_PR_SEC)) {
            --ts->seconds;
            diff -= MESA_CAP(MESA_CAP_TS_HW_TIME_CNT_PR_SEC);
            T_IG(_C,"big diff: tc = %u, hw_time = %u",tc, hw_time);
        }
        
        diff = diff * MESA_CAP(MESA_CAP_TS_HW_TIME_NSEC_PR_CNT);
        if (diff > ts->nanoseconds) {
            --ts->seconds;
            ts->nanoseconds += 1000000000L;
        }
        ts->nanoseconds -= diff;
            
        T_DG(_C,"then: ts_sec = %d,  ts_nsec = %d, hw_time = %u",ts->seconds, ts->nanoseconds, hw_time);
        
        return;
    }
}

static CapArray<i32, MESA_CAP_TS_IO_CNT> old_switch_adj;

#define MIN_ADJ_RATE_DELTA 5
// -------------------------------------------------------------------------
// vtss_tod_set_adjtimer()
// Adjust the time rate in the HW
// adj = clockrate adjustment in scaled PPB.
void vtss_tod_set_adjtimer(u32 domain, i64 adj)
{
    if (labs(adj) < MESA_CAP(MESA_CAP_TS_HW_TIME_MIN_ADJ_RATE)) adj = 0;
    i32 switch_adj = (adj*10LL + (1<<15))>>16; /* add (1<<15) to make the correct rounding */
    if (domain < MESA_CAP(MESA_CAP_TS_IO_CNT)) {
        if (abs(switch_adj - old_switch_adj[domain]) >= MIN_ADJ_RATE_DELTA) {
            T_IG(_C,"frequency adjustment:domain %d, adj = " VPRI64d " switch_adj = %d", domain, adj, switch_adj);
            TOD_RC(mesa_ts_domain_adjtimer_set(NULL, domain, switch_adj));
            if (MESA_CAP(MESA_CAP_PHY_TS)) {
                if (domain == 0) {
                    TOD_RC(ptp_module_man_rateadj(&adj));
                }
            }
            old_switch_adj[domain] = switch_adj;
        } else {
            T_DG(_C,"change < 0,5 ppb not applied old_switch_adj %d, switch_adj = %d", old_switch_adj[domain], switch_adj);
        }
    } else {
        T_WG(_C,"invalid domain = %d", domain);
    }
}

// -------------------------------------------------------------------------
// vtss_tod_ts_cnt_sub()
// Calculate time difference (in time counter units) r = x-y
void vtss_tod_ts_cnt_sub(u32 *r, u32 x, u32 y)
{
    *r = x-y;
    if (x < y) {*r += MESA_CAP(MESA_CAP_TS_HW_TIME_WRAP_LIMIT);} /* time counter has wrapped */
}

// -------------------------------------------------------------------------
// vtss_tod_ts_cnt_add()
// Calculate time sum (in time counter units) r = x+y
void vtss_tod_ts_cnt_add(u32 *r, u32 x, u32 y)
{
    *r = x+y;
    if (*r > MESA_CAP(MESA_CAP_TS_HW_TIME_WRAP_LIMIT)) {*r -= MESA_CAP(MESA_CAP_TS_HW_TIME_WRAP_LIMIT);} /* time counter has wrapped */
}


// -------------------------------------------------------------------------
// vtss_tod_timeinterval_to_ts_cnt()
// Convert a timeInterval to time difference (in time counter units)
// It is assumed that the timeinterval is > 0 and < counter wrap around time
void vtss_tod_timeinterval_to_ts_cnt(u32 *r, mesa_timeinterval_t x)
{
    if (x < -VTSS_SEC_NS_INTERVAL(1,0) || x >= VTSS_SEC_NS_INTERVAL(1,0)) {
        T_WG(_C,"Time interval overflow ");
        *r = 0;
        return;
    }
    if (x >= 0) {
        *r = VTSS_INTERVAL_NS(x) / MESA_CAP(MESA_CAP_TS_HW_TIME_NSEC_PR_CNT);
        //if (adjust_divisor) {
        //    *r -= *r/adjust_divisor;
        //}
    } else {
        *r = MESA_CAP(MESA_CAP_TS_HW_TIME_WRAP_LIMIT) - VTSS_INTERVAL_NS(-x) / MESA_CAP(MESA_CAP_TS_HW_TIME_NSEC_PR_CNT);
    }

    T_NG(_C,"x " VPRI64d ", r %d ", x>>16, *r);
}

// -------------------------------------------------------------------------
// vtss_tod_ts_cnt_to_timeinterval()
// Convert a time difference (in time counter units) to timeInterval
// it is assumed that time difference is < 1 sec.
void vtss_tod_ts_cnt_to_timeinterval(mesa_timeinterval_t *r, u32 x)
{
    x = x * MESA_CAP(MESA_CAP_TS_HW_TIME_NSEC_PR_CNT);
    //if (adjust_divisor) {
    //    x += x/adjust_divisor;
    //}
    *r = VTSS_SEC_NS_INTERVAL(0,x);
}

// -------------------------------------------------------------------------
// vtss_tod_ts_cnt_to_ns(ts)
// Convert time counter units to a nanosec value
u32 vtss_tod_ts_cnt_to_ns(u32 ts)
{
    return ts * MESA_CAP(MESA_CAP_TS_HW_TIME_NSEC_PR_CNT);
}


char *vtss_tod_ns2str (u32 nanoseconds, char *str, char delim)
{
    /* nsec time stamp. Format: mmm,uuu,nnn */
    int m, u, n;
    char d[2];
    d[0] = delim;
    d[1] = 0;
    m = nanoseconds / 1000000;
    u = (nanoseconds / 1000) % 1000;
    n = nanoseconds % 1000;
    sprintf(str, "%03d%s%03d%s%03d", m, d, u, d, n);
    return str;
}


char *vtss_tod_ScaledNs_To_String (const mesa_scaled_ns_t *t, char* str, char delim)
{
    vtss::I128 t2;
    char str1 [14];
    char str2 [33];
    t2 = vtss::I128(t->scaled_ns_low) + vtss::I128(1LL, 0LL) * t->scaled_ns_high;
    T_DG(_C,"high %d, low %" PRIi64, t->scaled_ns_high, t->scaled_ns_low);
    t2 = t2 / 0x10000; // convert to whole nanosec
    if (t2 < 0) {
        t2 = -t2;
        str[0] = '-';
    } else {
        str[0] = ' ';
    }
    vtss::I128 sec = t2 / VTSS_ONE_MIA;
    vtss::I128 nsec = t2 % VTSS_ONE_MIA;
    int i = sizeof(str2)-1;
    str2[i--] = 0; // 0'terminated string
    int dig = 0;
    do {
        vtss::I128 digit = sec % 10;
        str2[i--] = '0' + digit.to_char();
        if ((++dig % 3) == 0 && sec != 0) str2[i--] = delim;
        sec = sec/10;
    } while (sec > 0 && i > 0);
    if (sec != 0) {
        T_WG(_C,"buffer overflow in scaled_ns printout");
    }
    sprintf(str+1, "%s.%s", str2+i+1, vtss_tod_ns2str (nsec.to_int32(), str1, delim));
    return str;
}

char *vtss_tod_TimeInterval_To_String (const mesa_timeinterval_t *t, char* str, char delim)
{
    mesa_timeinterval_t t1;
    char str1 [14];
    if (*t < 0) {
        t1 = -*t;
        str[0] = '-';
    } else {
        t1 = *t;
        str[0] = ' ';
    }
    if (t1 == VTSS_MAX_TIMEINTERVAL) {
        strcpy(str+1, "NAN");
    } else {
        sprintf(str+1, "%d.%s", VTSS_INTERVAL_SEC(t1), vtss_tod_ns2str (VTSS_INTERVAL_NS(t1), str1, delim));
    }
    return str;
}

void vtss_tod_sub_TimeInterval(mesa_timeinterval_t *r, const mesa_timestamp_t *x, const mesa_timestamp_t *y)
{
    *r = (mesa_timeinterval_t)x->seconds + (((mesa_timeinterval_t)x->sec_msb)<<32) - 
                                 (mesa_timeinterval_t)y->seconds - (((mesa_timeinterval_t)y->sec_msb)<<32);
    *r = *r*1000000000LL;
    *r += ((mesa_timeinterval_t)x->nanoseconds - (mesa_timeinterval_t)y->nanoseconds);
    *r *= (1<<16);
}

/* This function does a "simple" subtraction of time stamps without PTP adjustment - without rotating left 16 */
void vtss_tod_sub_TimeStamps(i64 *r, const mesa_timestamp_t *x, const mesa_timestamp_t *y)
{
    *r = (i64)x->seconds + (((i64)x->sec_msb)<<32) - (i64)y->seconds - (((i64)y->sec_msb)<<32);
    *r = *r*1000000000LL;
    *r += ((i64)x->nanoseconds - (i64)y->nanoseconds);
}

static void timestamp_fix(mesa_timestamp_t *r, const mesa_timestamp_t *x)
{
    if ((i32)r->nanoseconds < 0) {
        r->seconds -= 1;
        r->nanoseconds += VTSS_ONE_MIA;
    }
    if (r->nanoseconds > VTSS_ONE_MIA) {
        r->seconds += 1;
        r->nanoseconds -= VTSS_ONE_MIA;
    }
    if (r->seconds < 70000 && x->seconds > (0xffffffffL -70000L)) ++r->sec_msb; /* sec counter has wrapped */
    if (x->seconds < 70000 && r->seconds > (0xffffffffL -70000L)) --r->sec_msb; /* sec counter has wrapped (negative) */
}

void vtss_tod_add_TimeInterval(mesa_timestamp_t *r, const mesa_timestamp_t *x, const mesa_timeinterval_t *y)
{
    r->seconds = x->seconds + VTSS_INTERVAL_SEC(*y);
    r->sec_msb = x->sec_msb;
    r->nanoseconds = x->nanoseconds + VTSS_INTERVAL_NS(*y);

    timestamp_fix(r, x);
}

void vtss_tod_sub_TimeStamp(mesa_timestamp_t *r, const mesa_timestamp_t *x, const mesa_timeinterval_t *y)
{
    mesa_timeinterval_t y_temp = -*y;
    vtss_tod_add_TimeInterval(r, x, &y_temp);
}

#if 0
void arithTest(void)
{
    mesa_timestamp_t r;
    mesa_timestamp_t x [] = {{0,123,4567},{0,123,4567},{0,123,900000123}, {1,4294967295LU,900000123}};
    mesa_timeinterval_t y [] = {4568*(1<<16),-4568*(1<<16),(mesa_timeinterval_t)100000000*(1<<16), (mesa_timeinterval_t)200000012*(1<<16) };
    mesa_timeinterval_t r1;
    mesa_timestamp_t x1 [] = {{0,123,4567},{0,123,4567},{0,123,900000123},{0,123,900000123},{0,123,900000119},{1,1,900000119},{0,4294967295LU,900000123}};
    mesa_timestamp_t y1 [] = {{0,122,4545},{0,124,4545},{0,122,900000125},{0,124,900000125},{0,123,900000123},{0,4294967295LU,900000123},{1,1,900000119}};
    mesa_timestamp_t r2;
    mesa_timestamp_t x2 [] = {{0,123,4567},{0,123,4567},{0,123,900000123}, {1,4294967295LU,900000123}};
    mesa_timeinterval_t y2 [] = {4568*(1<<16),4565*(1<<16),(mesa_timeinterval_t)100000000*(1<<16), (mesa_timeinterval_t)200000012*(1<<16) };
    uint i;
    char str1 [40];
    char str2 [40];
    char str3 [40];
    for (i=0; i < sizeof(y)/sizeof(mesa_timeinterval_t); i++) {
        vtss_tod_add_TimeInterval(&r,&x[i],&y[i]);
        T_I ("vtss_tod_add_TimeInterval: %s = %s + %s\n", TimeStampToString(&r,str1), TimeStampToString(&x[i],str2), vtss_tod_TimeInterval_To_String (&y[i],str3,','));
    }
    for (i=0; i < sizeof(y1)/sizeof(mesa_timestamp_t); i++) {
        vtss_tod_sub_TimeInterval(&r1,&x1[i],&y1[i]);
        T_I ("vtss_tod_sub_TimeInterval: %s = %s - %s\n", vtss_tod_TimeInterval_To_String(&r1,str1,','), TimeStampToString(&x1[i],str2), TimeStampToString (&y1[i],str3));
    }
    for (i=0; i < sizeof(y2)/sizeof(mesa_timeinterval_t); i++) {
        vtss_tod_sub_TimeStamp(&r2,&x2[i],&y2[i]);
        T_I ("subTimeStamp: %s = %s - %s\n", TimeStampToString(&r2,str1), TimeStampToString(&x2[i],str2), vtss_tod_TimeInterval_To_String (&y2[i],str3, ','));
    }
}
#endif
