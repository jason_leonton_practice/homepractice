/* Switch API software.

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

/**
 * \file
 * \brief vcl icli functions
 * \details This header file describes vcl icli functions
 */

#ifndef VTSS_ICLI_VCL_H
#define VTSS_ICLI_VCL_H

#include "icli_api.h"

#ifdef __cplusplus
extern "C" {
#endif
/**
 * \brief Function for showing vcl mac status
 * \param session_id  [IN] For ICLI_PRINTF
 * \param has_address [IN] TRUE to look up a specific MAC address entry. If FALSE all MAC address entries are looked up.
 * \param mac_addr    [IN] Only valid when has_address is TRUE. The MAC address to look up.

 **/
mesa_rc vcl_icli_show_mac(const i32 session_id, const BOOL has_address, const mesa_mac_t *mac_addr);

/**
 * \brief Function for showing vcl ip-subnet status
 * \param session_id  [IN] For ICLI_PRINTF
 * \param has_address [IN] TRUE to look up a specific subnet id address entry. If FALSE all subnet entries are looked up.
 * \param mac_addr    [IN] Only valid when has_id is TRUE. The subnet to look up.

 **/
mesa_rc vcl_icli_show_ipsubnet(const i32 session_id, const icli_ipv4_subnet_t *ipv4);

mesa_rc vcl_icli_show_protocol(u32 session_id, BOOL has_eth2, u16 etype, BOOL has_arp, BOOL has_ip, BOOL has_ipx,
                               BOOL has_at, BOOL has_snap, uint oui, BOOL has_rfc_1042, BOOL has_snap_8021h, u16 pid,
                               BOOL has_llc, u8 dsap, u8 ssap);

mesa_rc vcl_icli_subnet_add(const icli_ipv4_subnet_t *ipv4, mesa_vid_t vid, icli_stack_port_range_t *plist);

mesa_rc vcl_icli_subnet_del(const icli_ipv4_subnet_t *ipv4, icli_stack_port_range_t *plist);

mesa_rc vcl_icli_protocol_group_add(u32 session_id, BOOL has_eth2, u16 etype, BOOL has_arp, BOOL has_ip, BOOL has_ipx,
                                    BOOL has_at, BOOL has_snap, uint oui, BOOL has_rfc_1042, BOOL has_snap_8021h, u16 pid,
                                    BOOL has_llc, u8 dsap, u8 ssap, char *name);

mesa_rc vcl_icli_protocol_group_del(u32 session_id, BOOL has_eth2, u16 etype, BOOL has_arp, BOOL has_ip, BOOL has_ipx,
                                    BOOL has_at, BOOL has_snap, uint oui, BOOL has_rfc_1042, BOOL has_snap_8021h, u16 pid,
                                    BOOL has_llc, u8 dsap, u8 ssap);

mesa_rc vcl_icli_protocol_add(u32 session_id, char *grp_id, mesa_vid_t vid, icli_stack_port_range_t *mode_port_list);

mesa_rc vcl_icli_protocol_del(u32 session_id, char *grp_id, icli_stack_port_range_t *mode_port_list);

mesa_rc vcl_icli_mac_add(mesa_mac_t mac_addr, mesa_vid_t vid, icli_stack_port_range_t *plist);

mesa_rc vcl_icli_mac_del(u32 session_id, mesa_mac_t mac_addr, icli_stack_port_range_t *plist);

mesa_rc vcl_icli_address(u32 session_id, BOOL has_int, icli_stack_port_range_t *plist, BOOL has_set, BOOL has_dmac, BOOL has_smac);

mesa_rc vcl_icli_debug_show_mac(u32 session_id, BOOL has_address, mesa_mac_t *mac_addr);

#if defined(VTSS_SW_OPTION_IP)
mesa_rc vcl_icli_debug_show_ipsubnet(u32 session_id, icli_ipv4_subnet_t *ipv4);
#endif

mesa_rc vcl_icli_debug_show_protocol(u32 session_id, BOOL has_eth2, u16 etype, BOOL has_arp, BOOL has_ip, BOOL has_ipx,
                                     BOOL has_at, BOOL has_snap, uint oui, BOOL has_rfc_1042, BOOL has_snap_8021h, u16 pid,
                                     BOOL has_llc, u8 dsap, u8 ssap);

#ifdef __cplusplus
}
#endif
#endif /* VTSS_ICLI_VCL_H */



/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
