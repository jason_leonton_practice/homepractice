/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

/*
******************************************************************************

    Include files

******************************************************************************
*/
#include "icfg_api.h"
#include "vcl_api.h"
#ifdef VTSS_SW_OPTION_IP
#include "ip_utils.h" // For vtss_conv_prefix_to_ipv4mask
#endif
#include "misc_api.h"   //uport2iport(), iport2uport()
#include "topo_api.h"   //topo_usid2isid(), topo_isid2usid()
#include "vcl_icfg.h"
#include "vcl_trace.h"
/*
******************************************************************************

    Constant and Macro definition

******************************************************************************
*/

/*
******************************************************************************

    Data structure type definition

******************************************************************************
*/

/*
******************************************************************************

    Static Function

******************************************************************************
*/

/* ICFG callback functions */
static mesa_rc VCL_ICFG_global_conf(const vtss_icfg_query_request_t *req,
                                    vtss_icfg_query_result_t *result)
{
    mesa_rc                           rc = VTSS_OK;
    vcl_proto_mgmt_group_conf_proto_t entry = {0};
    BOOL                              first_entry = TRUE, next = FALSE;

    while (vcl_proto_mgmt_proto_get(&entry, first_entry, next) == VTSS_RC_OK) {
        if (first_entry == TRUE) {
            first_entry = FALSE;
        }
        if (entry.proto_encap_type == VTSS_APPL_VCL_PROTO_ENCAP_ETH2) {
            // ETH type
            i8  eth2_proto_string[15];

            switch (entry.proto.eth2_proto.eth_type) {
            case ETHERTYPE_ARP:
                strcpy(eth2_proto_string, "arp");
                break;
            case ETHERTYPE_IP:
                strcpy(eth2_proto_string, "ip");
                break;
            case ETHERTYPE_IPX:
                strcpy(eth2_proto_string, "ipx");
                break;
            case ETHERTYPE_AT:
                strcpy(eth2_proto_string, "at");
                break;
            default:
                sprintf(eth2_proto_string, "0x%X", entry.proto.eth2_proto.eth_type);
                break;
            }
            VTSS_RC(vtss_icfg_printf(result, "%s eth2 %s group %s\n", VCL_GLOBAL_MODE_PROTO_TEXT, eth2_proto_string, entry.name));
        } else if (entry.proto_encap_type == VTSS_APPL_VCL_PROTO_ENCAP_LLC_SNAP) {
            // SNAP
            // Convert snap oui to string
            i8  snap_oui_string[15];
            if (entry.proto.llc_snap_proto.oui[0] == 0x0 &&
                entry.proto.llc_snap_proto.oui[1] == 0x0 &&
                entry.proto.llc_snap_proto.oui[2] == 0x0) {
                strcpy(snap_oui_string, "rfc-1042");
            } else if (entry.proto.llc_snap_proto.oui[0] == 0x0 &&
                       entry.proto.llc_snap_proto.oui[1] == 0x0 &&
                       entry.proto.llc_snap_proto.oui[2] == 0xF8) {
                strcpy(snap_oui_string, "snap-8021h");
            } else {
                sprintf(snap_oui_string, "0x%X", (entry.proto.llc_snap_proto.oui[0] << 16) |
                        (entry.proto.llc_snap_proto.oui[1] << 8) |
                        (entry.proto.llc_snap_proto.oui[2]));
            }
            VTSS_RC(vtss_icfg_printf(result, "%s snap %s 0x%X group %s\n", VCL_GLOBAL_MODE_PROTO_TEXT, snap_oui_string,
                                     entry.proto.llc_snap_proto.pid, entry.name));
        } else {
            // llc
            VTSS_RC(vtss_icfg_printf(result, "%s llc 0x%x 0x%x group %s\n", VCL_GLOBAL_MODE_PROTO_TEXT,
                                     entry.proto.llc_other_proto.dsap,
                                     entry.proto.llc_other_proto.ssap, entry.name));
        }
        next = TRUE;
    }
    return rc;
}

static mesa_rc VCL_ICFG_port_conf(const vtss_icfg_query_request_t *req, vtss_icfg_query_result_t *result)
{
    vtss_isid_t    isid = topo_usid2isid(req->instance_id.port.usid);
    mesa_port_no_t iport = uport2iport(req->instance_id.port.begin_uport);
    BOOL           first, next;

    // switchport vlan protocol
    vcl_proto_mgmt_group_conf_entry_global_t proto_vce;

    next = FALSE;
    first = TRUE;
    while (vcl_proto_mgmt_conf_get(isid, &proto_vce, first, next) == VTSS_RC_OK) {
        next = TRUE;
        first = FALSE;
        if (proto_vce.ports[isid - VTSS_ISID_START][iport]) { // Only print if current port is part the entry
            VTSS_RC(vtss_icfg_printf(result, " %s %s %s %u\n", VCL_PORT_MODE_PROTO_GROUP_TEXT, proto_vce.name, "vlan", proto_vce.vid));
        }
    }

#ifdef VTSS_SW_OPTION_IP
    // switchport vlan ip-subnet
    i8 buf[100];
    i8 buf1[100];
    vcl_ip_mgmt_vce_conf_global_t ip_vce;
    mesa_ipv4_t                   mask;

    next = FALSE;
    first = TRUE;
    while (vcl_ip_mgmt_conf_get(isid, &ip_vce, first, next) == VTSS_RC_OK) {
        next = TRUE;
        first = FALSE;
        if (ip_vce.ports[isid - VTSS_ISID_START][iport]) { // Only print if current port is part of IP subnet based VLAN membership
            VTSS_RC(vtss_conv_prefix_to_ipv4mask(ip_vce.mask_len, &mask));
            VTSS_RC(vtss_icfg_printf(result, " switchport vlan ip-subnet %s/%s vlan %d\n",
                                     misc_ipv4_txt(ip_vce.ip_addr, buf),
                                     misc_ipv4_txt(mask, buf1),
                                     ip_vce.vid));
        }
    }
#endif

    // switchport vlan mac vlan
    vcl_mac_mgmt_vce_conf_global_t mac_vce;

    next = FALSE;
    first = TRUE;
    while (vcl_mac_mgmt_conf_get(isid, &mac_vce, first, next) == VTSS_RC_OK) {
        next = TRUE;
        first = FALSE;
        if (mac_vce.ports[isid - VTSS_ISID_START][iport]) { // Only print if current port is part the entry
            VTSS_RC(vtss_icfg_printf(result, " switchport vlan mac %s vlan %d\n",
                                     misc_mac2str(mac_vce.smac.addr),
                                     mac_vce.vid));
        }
    }

    return VTSS_RC_OK;
}

/*
******************************************************************************

    Public functions

******************************************************************************
*/

/* Initialization function */
mesa_rc VCL_icfg_init(void)
{
    mesa_rc rc;

    /* Register callback functions to ICFG module.
       The configuration divided into two groups for this module.
       1. Global configuration
       2. Port configuration
    */
    if ((rc = vtss_icfg_query_register(VTSS_ICFG_VCL_GLOBAL_CONF, "vlan", VCL_ICFG_global_conf)) != VTSS_OK) {
        return rc;
    }
    rc = vtss_icfg_query_register(VTSS_ICFG_VCL_PORT_CONF, "vlan", VCL_ICFG_port_conf);

    return rc;
}

