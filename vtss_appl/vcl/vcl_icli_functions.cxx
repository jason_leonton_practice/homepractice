/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/
#include "icli_api.h"
#include "icli_porting_util.h"
#include "misc_api.h" // For e.g. vtss_uport_no_t
#include "msg_api.h"
#include "mgmt_api.h" // For mgmt_prio2txt
#include "vcl_api.h"
#include "vcl_icli_functions.h"
#include <vcl_trace.h>
#include "ip_utils.h"

/***************************************************************************/
/*  Functions called by iCLI                                                */
/****************************************************************************/
//  see vcl_icli_functions.h
mesa_rc vcl_icli_show_mac(const i32 session_id, const BOOL has_address, const mesa_mac_t *mac_addr)
{
    vcl_mac_mgmt_vce_conf_global_t entry;
    BOOL                           first = TRUE, next, entry_found = FALSE, mac_found = FALSE,
                                   found_sid = FALSE, found_sid_prev = FALSE, found_start = FALSE;
    switch_iter_t                  sit, sit_ext;
    vtss_isid_t                    start_isid;
    port_iter_t                    pit;
    i8                             str_buf[ICLI_STR_MAX_LEN];

    T_DG(TRACE_GRP_ICLI, "Enter Show MAC");
    // Distinguish between show specific MAC add and show everything
    if (has_address) {
        memcpy(&entry.smac, mac_addr, sizeof(*mac_addr));
        T_DG(TRACE_GRP_ICLI, "Searching for entry with MAC address %02x:%02x:%02x:%02x:%02x:%02x",  entry.smac.addr[0],
             entry.smac.addr[1], entry.smac.addr[2], entry.smac.addr[3], entry.smac.addr[4], entry.smac.addr[5]);
    } else {
        T_DG(TRACE_GRP_ICLI, "Looping through all the entries");
    }
    VTSS_RC(switch_iter_init(&sit_ext, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_USID));
    while (switch_iter_getnext(&sit_ext)) {
        T_NG(TRACE_GRP_ICLI, "Entering switch %u - ext iteration", sit_ext.isid);
        // Initialize the mgmt() parameters between iterations
        if (has_address) {
            first = FALSE;
            next = FALSE;
            memcpy(&entry.smac, mac_addr, sizeof(*mac_addr));
        } else {
            first = TRUE;
            next = FALSE;
        }
        mac_found = FALSE;
        // Call the management function to either get the specified entry or loop through all the entries
        while ((mac_found == FALSE) && (vcl_mac_mgmt_conf_get(VTSS_ISID_GLOBAL, &entry, first, next) == VTSS_RC_OK)) {
            next = TRUE;
            first = FALSE;
            found_sid_prev = FALSE;
            found_start = FALSE;
            start_isid = 0; // for lint purposes

            // user has asked for a specific MAC address, skip all others.
            if (has_address) {
                if (memcmp(mac_addr, &entry.smac, sizeof(entry.smac)) != 0) {
                    T_IG(TRACE_GRP_ICLI, "Returned MAC address does not match to the requested one");
                    break;
                } else {
                    // Entry has been found, so stopping the mgmt loop
                    mac_found = TRUE;
                }
            }

            if (!entry_found) { // Printing header if this is the very first entry found
                icli_parm_header(session_id, "MAC Address        VID   Interfaces");
                entry_found = TRUE;
            }

            // Looping through all switches and printing only those that have active ports for that entry
            VTSS_RC(switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_USID));
            while (switch_iter_getnext(&sit)) {
                VTSS_RC(port_iter_init(&pit, NULL, sit.isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL));
                while (port_iter_getnext(&pit)) {
                    if (entry.ports[sit.isid - 1][pit.iport] == 1) {
                        found_sid = TRUE;
                        break;
                    }
                }
                if (found_sid) {
                    if (found_start == FALSE) {
                        start_isid = sit.isid;
                        found_start = TRUE;
                        T_RG(TRACE_GRP_ICLI, "Found entry start on switch %u", sit.isid);
                    }
                    if (sit_ext.isid == start_isid) {
                        if (found_sid_prev == FALSE) {
                            ICLI_PRINTF("%02x:%02x:%02x:%02x:%02x:%02x  %-4u  %s\n", entry.smac.addr[0], entry.smac.addr[1],
                                        entry.smac.addr[2], entry.smac.addr[3], entry.smac.addr[4], entry.smac.addr[5],
                                        entry.vid, icli_port_list_info_txt(sit.isid, entry.ports[sit.isid - 1], str_buf, FALSE));
                            found_sid_prev = TRUE;
                        } else {
                            ICLI_PRINTF("%23s  %s\n", "", icli_port_list_info_txt(sit.isid, entry.ports[sit.isid - 1], str_buf, FALSE));
                        }
                    } else {
                        T_RG(TRACE_GRP_ICLI, "Skipping print of entry - not the right switch");
                    }
                } else {
                    if (sit_ext.isid == sit.isid) { // the purpose of this check is to reduce the total number of iterations - entry is not needed in this case
                        T_RG(TRACE_GRP_ICLI, "Skipping entire entry");
                        break;
                    }
                }
                found_sid = FALSE;
            }
        }
        T_NG(TRACE_GRP_ICLI, "Exiting switch %u - ext iteration", sit_ext.isid);
    }

    if (!entry_found && has_address) {
        ICLI_PRINTF("Entry with MAC address %02x-%02x-%02x-%02x-%02x-%02x was not found in the switch/stack\n", mac_addr->addr[0],
                    mac_addr->addr[1], mac_addr->addr[2], mac_addr->addr[3], mac_addr->addr[4], mac_addr->addr[5]);
    }
    T_DG(TRACE_GRP_ICLI, "Exit Show MAC");
    return VTSS_RC_OK;
}

//  see vcl_icli_functions.h
mesa_rc vcl_icli_show_ipsubnet(const i32 session_id, const icli_ipv4_subnet_t *ipv4)
{
    vcl_ip_mgmt_vce_conf_global_t entry;
    BOOL                          first = TRUE, next = FALSE, entry_found = FALSE, ip_found = FALSE, found_sid = FALSE, found_sid_prev = FALSE, found_start = FALSE;
    i8                            ip_str[100];
    i8                            str_buf[ICLI_STR_MAX_LEN];
    switch_iter_t                 sit, sit_ext;
    vtss_isid_t                   start_isid;
    port_iter_t                   pit;
    u32                           mask_len = 0;
    i8                            i;
    mesa_ipv4_t                   ip_addr = 0, ip_mask;

    T_DG(TRACE_GRP_ICLI, "Enter Show Subnet");
    // Distinguish between show specific IP subnet and show everything
    if (ipv4->ip) {
        entry.ip_addr = ipv4->ip;
        VTSS_RC(vtss_conv_ipv4mask_to_prefix(ipv4->netmask, &mask_len));
        entry.mask_len = (u8) mask_len;
        T_DG(TRACE_GRP_ICLI, "Searching for a specific subnet entry");
    } else {
        T_DG(TRACE_GRP_ICLI, "Looping through all the entries");
    }
    VTSS_RC(switch_iter_init(&sit_ext, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_USID));
    while (switch_iter_getnext(&sit_ext)) {
        T_NG(TRACE_GRP_ICLI, "Entering switch %u - ext iteration", sit_ext.isid);
        // Initialize the mgmt() parameters between iterations
        if (ipv4->ip) {
            first = FALSE;
            next = FALSE;
            entry.ip_addr = ipv4->ip;
            VTSS_RC(vtss_conv_ipv4mask_to_prefix(ipv4->netmask, &mask_len));
            entry.mask_len = (u8) mask_len;
        } else {
            first = TRUE;
            next = FALSE;
        }
        ip_found = FALSE;
        // Call the management function to either get the specified entry or loop through all the entries
        while ((ip_found == FALSE) && (vcl_ip_mgmt_conf_get(VTSS_ISID_GLOBAL, &entry, first, next) == VTSS_RC_OK)) {
            next = TRUE;
            first = FALSE;
            found_sid_prev = FALSE;
            found_start = FALSE;
            start_isid = 0; // for lint purposes

            // user has asked for a specific IP subnet, skip all others.
            if (ipv4->ip) {
                ip_mask = 0;
                for (i = 31; i >= (32 - ((u8) mask_len)); i--) {
                    ip_mask |= (1 << i);
                }
                ip_addr = ipv4->ip;
                ip_addr = ip_addr & ip_mask;
                if ((ip_addr != entry.ip_addr) || (((u8) mask_len) != entry.mask_len)) {
                    T_RG(TRACE_GRP_ICLI, "Returned IP subnet does not match to the requested one");
                    break;
                } else {
                    // Entry has been found, so stopping the mgmt loop
                    ip_found = TRUE;
                }
            }

            if (!entry_found) { // Printing header if this is the very first entry found
                icli_parm_header(session_id, "IP Address       Mask Length  VID   Interfaces");
                entry_found = TRUE;
            }

            // Looping through all switches and printing only those that have active ports for that entry
            VTSS_RC(switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_USID));
            while (switch_iter_getnext(&sit)) {
                VTSS_RC(port_iter_init(&pit, NULL, sit.isid, PORT_ITER_SORT_ORDER_IPORT, PORT_ITER_FLAGS_NORMAL));
                while (port_iter_getnext(&pit)) {
                    if (entry.ports[sit.isid - 1][pit.iport] == 1) {
                        found_sid = TRUE;
                        break;
                    }
                }
                if (found_sid) {
                    if (found_start == FALSE) {
                        start_isid = sit.isid;
                        found_start = TRUE;
                        T_RG(TRACE_GRP_ICLI, "Found entry start on switch %u", sit.isid);
                    }
                    if (sit_ext.isid == start_isid) {
                        if (found_sid_prev == FALSE) {
                            ICLI_PRINTF("%-15s  %-11u  %-4u  %s\n", misc_ipv4_txt(entry.ip_addr, ip_str),
                                        entry.mask_len, entry.vid, icli_port_list_info_txt(sit.isid, entry.ports[sit.isid - 1],
                                                                                           str_buf, FALSE));
                            found_sid_prev = TRUE;
                        } else {
                            ICLI_PRINTF("%34s  %s\n", "", icli_port_list_info_txt(sit.isid, entry.ports[sit.isid - 1], str_buf, FALSE));
                        }
                    } else {
                        T_RG(TRACE_GRP_ICLI, "Skipping print of entry - not the right switch");
                    }
                } else {
                    if (sit_ext.isid == sit.isid) { // the purpose of this check is to reduce the total number of iterations - entry is not needed in this case
                        T_RG(TRACE_GRP_ICLI, "Skipping entire entry");
                        break;
                    }
                }
                found_sid = FALSE;
            }
        }
        T_NG(TRACE_GRP_ICLI, "Exiting switch %u - ext iteration", sit_ext.isid);
    }

    if (!entry_found && (ipv4->ip)) {
        ICLI_PRINTF("Entry with IP subnet was not found in the switch/stack\n");
    }
    T_DG(TRACE_GRP_ICLI, "Exit Show Subnet");
    return VTSS_RC_OK;
}

mesa_rc vcl_icli_show_protocol(u32 session_id, BOOL has_eth2, u16 etype, BOOL has_arp, BOOL has_ip, BOOL has_ipx,
                               BOOL has_at, BOOL has_snap, uint oui, BOOL has_rfc_1042, BOOL has_snap_8021h, u16 pid,
                               BOOL has_llc, u8 dsap, u8 ssap)
{
    vcl_proto_mgmt_group_conf_proto_t         group_conf = {0};
    vcl_proto_mgmt_group_conf_entry_global_t  proto_vce;
    BOOL                                      first_entry, first, next;
    switch_iter_t                             sit;
    i8                                        str_buf[ICLI_STR_MAX_LEN];
    mesa_rc                                   rc = VTSS_RC_OK;

    T_DG(TRACE_GRP_ICLI, "Enter Show Protocol");
    if (has_eth2) {
        group_conf.proto_encap_type = VTSS_APPL_VCL_PROTO_ENCAP_ETH2;
        if (has_arp) {
            etype = ETHERTYPE_ARP;
        } else if (has_ip) {
            etype = ETHERTYPE_IP;
        } else if (has_ipx) {
            etype = ETHERTYPE_IPX;
        } else if (has_at) {
            etype = ETHERTYPE_AT;
        }
        group_conf.proto.eth2_proto.eth_type = etype;
    } else if (has_snap) {
        group_conf.proto_encap_type = VTSS_APPL_VCL_PROTO_ENCAP_LLC_SNAP;
        group_conf.proto.llc_snap_proto.pid = pid;
        if (has_rfc_1042) {
            group_conf.proto.llc_snap_proto.oui[0] = 0x0;
            group_conf.proto.llc_snap_proto.oui[1] = 0x0;
            group_conf.proto.llc_snap_proto.oui[2] = 0x0;
        } else if (has_snap_8021h) {
            group_conf.proto.llc_snap_proto.oui[0] = 0x0;
            group_conf.proto.llc_snap_proto.oui[1] = 0x0;
            group_conf.proto.llc_snap_proto.oui[2] = 0xF8;
        } else { /* valid OUI */
            group_conf.proto.llc_snap_proto.oui[0] = (oui >> 16) & 0xFF;
            group_conf.proto.llc_snap_proto.oui[1] = (oui >> 8) & 0xFF;
            group_conf.proto.llc_snap_proto.oui[2] = oui & 0xFF;
        }
        if ((group_conf.proto.llc_snap_proto.oui[0] == 0) &&
            (group_conf.proto.llc_snap_proto.oui[1] == 0) &&
            (group_conf.proto.llc_snap_proto.oui[2] == 0)) {
            if (pid < 0x600) {
                return VCL_ERROR_INVALID_PID;
            }
        }
    } else if (has_llc) {
        group_conf.proto_encap_type = VTSS_APPL_VCL_PROTO_ENCAP_LLC_OTHER;
        group_conf.proto.llc_other_proto.dsap = dsap;
        group_conf.proto.llc_other_proto.ssap = ssap;
    }
    if (has_eth2 | has_snap | has_llc) {
        T_DG(TRACE_GRP_ICLI, "Searching for a specific protocol");
        if ((rc = vcl_proto_mgmt_proto_get(&group_conf, FALSE, FALSE)) == VTSS_RC_OK) {
            ICLI_PRINTF("Protocol Type  Protocol (Value)          Group ID\n");
            ICLI_PRINTF("-------------  ------------------------  --------\n");
            if (group_conf.proto_encap_type == VTSS_APPL_VCL_PROTO_ENCAP_ETH2) {
                ICLI_PRINTF("%-13s  ETYPE:0x%-4x              %s\n", vcl_proto_mgmt_encaptype2string(group_conf.proto_encap_type),
                            group_conf.proto.eth2_proto.eth_type, group_conf.name);
            } else if (group_conf.proto_encap_type == VTSS_APPL_VCL_PROTO_ENCAP_LLC_SNAP) {
                ICLI_PRINTF("%-13s  OUI-%02x:%02x:%02x; PID:0x%-4x  %s\n", vcl_proto_mgmt_encaptype2string(group_conf.proto_encap_type),
                            group_conf.proto.llc_snap_proto.oui[0], group_conf.proto.llc_snap_proto.oui[1],
                            group_conf.proto.llc_snap_proto.oui[2], group_conf.proto.llc_snap_proto.pid, group_conf.name);
            } else {
                ICLI_PRINTF("%-13s  DSAP:0x%-2x; SSAP:0x%-2x      %s\n",
                            vcl_proto_mgmt_encaptype2string(group_conf.proto_encap_type), group_conf.proto.llc_other_proto.dsap,
                            group_conf.proto.llc_other_proto.ssap, group_conf.name);
            }
        } else {
            ICLI_PRINTF("The requested protocol was not found\n");
            return rc;
        }
    } else {
        T_DG(TRACE_GRP_ICLI, "Looping through all the entries");
        T_DG(TRACE_GRP_ICLI, "Looping through all the Protocol to Group mappings");
        first_entry = TRUE;
        next = FALSE;
        while (vcl_proto_mgmt_proto_get(&group_conf, first_entry, next) == VTSS_RC_OK) {
            if (first_entry == TRUE) {
                ICLI_PRINTF("Protocol Type  Protocol (Value)          Group ID\n");
                ICLI_PRINTF("-------------  ------------------------  --------\n");
                first_entry = FALSE;
            }
            if (group_conf.proto_encap_type == VTSS_APPL_VCL_PROTO_ENCAP_ETH2) {
                ICLI_PRINTF("%-13s  ETYPE:0x%-4x              %s\n", vcl_proto_mgmt_encaptype2string(group_conf.proto_encap_type),
                            group_conf.proto.eth2_proto.eth_type, group_conf.name);
            } else if (group_conf.proto_encap_type == VTSS_APPL_VCL_PROTO_ENCAP_LLC_SNAP) {
                ICLI_PRINTF("%-13s  OUI-%02x:%02x:%02x; PID:0x%-4x  %s\n", vcl_proto_mgmt_encaptype2string(group_conf.proto_encap_type),
                            group_conf.proto.llc_snap_proto.oui[0], group_conf.proto.llc_snap_proto.oui[1],
                            group_conf.proto.llc_snap_proto.oui[2], group_conf.proto.llc_snap_proto.pid, group_conf.name);
            } else {
                ICLI_PRINTF("%-13s  DSAP:0x%-2x; SSAP:0x%-2x      %s\n",
                            vcl_proto_mgmt_encaptype2string(group_conf.proto_encap_type), group_conf.proto.llc_other_proto.dsap,
                            group_conf.proto.llc_other_proto.ssap, group_conf.name);
            }
            next = TRUE;
        }
        T_DG(TRACE_GRP_ICLI, "Looping through all the Group to VID mappings");
        (void)switch_iter_init(&sit, VTSS_ISID_GLOBAL, SWITCH_ITER_SORT_ORDER_USID_CFG);
        while (switch_iter_getnext(&sit)) {
            T_NG(TRACE_GRP_ICLI, "Entering switch %u", sit.isid);
            first_entry = TRUE;
            next = FALSE;
            first = TRUE;
            while (vcl_proto_mgmt_conf_get(sit.isid, &proto_vce, first, next) == VTSS_RC_OK) {
                if (first_entry == TRUE) {
                    ICLI_PRINTF("\nSwitch #%d\n", sit.usid);
                    ICLI_PRINTF("---------\n");
                    ICLI_PRINTF("Group ID          VID   Ports\n");
                    ICLI_PRINTF("----------------  ----  -----\n");
                    first_entry = FALSE;
                }
                ICLI_PRINTF("%-16s  %-4u  %s\n", proto_vce.name, proto_vce.vid,
                            icli_port_list_info_txt(sit.isid, proto_vce.ports[sit.isid - VTSS_ISID_START], str_buf, FALSE));
                next = TRUE;
                first = FALSE;
            }
            T_NG(TRACE_GRP_ICLI, "Exiting switch %u", sit.isid);
        }
    }
    T_DG(TRACE_GRP_ICLI, "Exit Show Protocol");
    return VTSS_RC_OK;
}

// Function for adding ip subnet-based
// IN : ipv4   - Pointer to the IP address and mask
//      vce_id - Entry id
//      plist  - List of ports to configure
mesa_rc vcl_icli_subnet_add(const icli_ipv4_subnet_t *ipv4, mesa_vid_t vid, icli_stack_port_range_t *plist)
{
    vcl_ip_mgmt_vce_conf_global_t entry;
    vcl_ip_mgmt_vce_conf_local_t  local_entry;
    switch_iter_t                 sit;
    port_iter_t                   pit;
    u32                           mask_len;
    mesa_rc                       rc = VTSS_RC_OK;

    T_DG(TRACE_GRP_ICLI, "Enter Add Subnet to VID mapping");
    memset(&entry, 0, sizeof(entry));
    entry.ip_addr = ipv4->ip;
    // Mask length will only be from 1-32 even though this function takes an u32 type as length.
    VTSS_RC(vtss_conv_ipv4mask_to_prefix(ipv4->netmask, &mask_len));
    entry.mask_len = (u8) mask_len; // Convert to u8
    if (vcl_ip_mgmt_conf_get(VTSS_ISID_GLOBAL, &entry, FALSE, FALSE) != VTSS_RC_OK) {
        memset(&entry, 0, sizeof(entry));
    } else {
        if (entry.vid != vid) {
            T_DG(TRACE_GRP_ICLI, "The provided VLAN ID does not match the one of the existing IP Subnet entry. If you wanted to update the VID, then you must delete the entry first (updating the VID is not possible!)");
            return VCL_ERROR_ENTRY_DIFF_VID;
        }
    }
    // Loop over all configurable switches in usid order...
    (void)icli_switch_iter_init(&sit);
    // ...provided they're also in the plist.
    while (icli_switch_iter_getnext(&sit, plist)) {
        memset(&local_entry, 0, sizeof(local_entry));
        local_entry.ip_addr = ipv4->ip;
        local_entry.mask_len = (u8) mask_len; // Convert to u8
        local_entry.vid = vid;
        memcpy(local_entry.ports, entry.ports[sit.isid - VTSS_ISID_START], sizeof(entry.ports[sit.isid - VTSS_ISID_START]));
        // Loop over all ports in uport order...
        (void)icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_NORMAL);
        // ... provided they're also in the plist.
        while (icli_port_iter_getnext(&pit, plist)) {
            local_entry.ports[pit.iport] = TRUE;
        }
        if (memcmp(local_entry.ports, entry.ports[sit.isid - VTSS_ISID_START], sizeof(entry.ports[sit.isid - VTSS_ISID_START]))) {
            if ((rc = vcl_ip_mgmt_conf_add(sit.isid, &local_entry)) != VTSS_RC_OK) {
                return rc;
            }
        } else {
            T_DG(TRACE_GRP_ICLI, "The provided port list matches the existing one and therefore no update is required to the mapping");
        }
    }
    T_DG(TRACE_GRP_ICLI, "Exit Add Subnet to VID mapping");
    return VTSS_RC_OK;
}

// Function for deleting ip subnet-based
// IN : ipv4   - Pointer to the IP address and mask
//      vce_id_list - List of entry ids to delete
mesa_rc vcl_icli_subnet_del(const icli_ipv4_subnet_t *ipv4, icli_stack_port_range_t *plist)
{
    vcl_ip_mgmt_vce_conf_global_t entry;
    vcl_ip_mgmt_vce_conf_local_t  local_entry;
    switch_iter_t                 sit;
    port_iter_t                   pit;
    u32                           mask_len;
    mesa_port_list_t              ports;
    mesa_rc                       rc = VTSS_RC_OK;

    T_DG(TRACE_GRP_ICLI, "Enter Delete Subnet to VID mapping");
    memset(&entry, 0, sizeof(entry));
    entry.ip_addr = ipv4->ip;
    VTSS_RC(vtss_conv_ipv4mask_to_prefix(ipv4->netmask, &mask_len));
    entry.mask_len = (u8) mask_len;
    if ((rc = vcl_ip_mgmt_conf_get(VTSS_ISID_GLOBAL, &entry, FALSE, FALSE)) != VTSS_RC_OK) {
        return rc;
    }
    // Loop over all configurable switches in usid order...
    (void)icli_switch_iter_init(&sit);
    // ...provided they're also in the plist.
    while (icli_switch_iter_getnext(&sit, plist)) {
        memset(&local_entry, 0, sizeof(local_entry));
        local_entry.ip_addr = ipv4->ip;
        local_entry.mask_len = (u8) mask_len;
        local_entry.vid = entry.vid;
        memcpy(local_entry.ports, entry.ports[sit.isid - VTSS_ISID_START], sizeof(entry.ports[sit.isid - VTSS_ISID_START]));
        // Loop over all ports in uport order...
        (void)icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_NORMAL);
        // ... provided they're also in the plist.
        while (icli_port_iter_getnext(&pit, plist)) {
            local_entry.ports[pit.iport] = FALSE;
        }
        if (memcmp(local_entry.ports, entry.ports[sit.isid - VTSS_ISID_START], sizeof(entry.ports[sit.isid - VTSS_ISID_START]))) {
            if (memcmp(ports, local_entry.ports, sizeof(local_entry.ports))) {
                if ((rc = vcl_ip_mgmt_conf_add(sit.isid, &local_entry)) != VTSS_RC_OK) {
                    return rc;
                }
            } else {
                if ((rc = vcl_ip_mgmt_conf_del(sit.isid, &local_entry)) != VTSS_RC_OK) {
                    return rc;
                }
            }
        } else {
            T_DG(TRACE_GRP_ICLI, "The provided port list matches the existing one and therefore no update is required to the mapping");
        }
    }
    T_DG(TRACE_GRP_ICLI, "Exit Delete Subnet to VID mapping");
    return VTSS_RC_OK;
}

mesa_rc vcl_icli_protocol_group_add(u32 session_id, BOOL has_eth2, u16 etype, BOOL has_arp, BOOL has_ip, BOOL has_ipx,
                                    BOOL has_at, BOOL has_snap, uint oui, BOOL has_rfc_1042, BOOL has_snap_8021h, u16 pid,
                                    BOOL has_llc, u8 dsap, u8 ssap, char *name)
{
    vcl_proto_mgmt_group_conf_proto_t group_conf;
    u8                                proto_cnt = 0;
    mesa_rc                           rc = VTSS_RC_OK;

    T_DG(TRACE_GRP_ICLI, "Enter Add Protocol to Group mapping");
    if (name == NULL) {
        return VCL_ERROR_NULL_GROUP_NAME;
    }
    if (has_eth2) {
        proto_cnt++;
    }
    if (has_snap) {
        proto_cnt++;
    }
    if (has_llc) {
        proto_cnt++;
    }
    if (proto_cnt > 1) {
        return VCL_ERROR_INVALID_PROTO_CNT;
    }
    if (has_eth2) {
        group_conf.proto_encap_type = VTSS_APPL_VCL_PROTO_ENCAP_ETH2;
        if (has_arp) {
            etype = ETHERTYPE_ARP;
        } else if (has_ip) {
            etype = ETHERTYPE_IP;
        } else if (has_ipx) {
            etype = ETHERTYPE_IPX;
        } else if (has_at) {
            etype = ETHERTYPE_AT;
        }
        group_conf.proto.eth2_proto.eth_type = etype;
    } else if (has_snap) {
        group_conf.proto_encap_type = VTSS_APPL_VCL_PROTO_ENCAP_LLC_SNAP;
        group_conf.proto.llc_snap_proto.pid = pid;
        if (has_rfc_1042) {
            group_conf.proto.llc_snap_proto.oui[0] = 0x0;
            group_conf.proto.llc_snap_proto.oui[1] = 0x0;
            group_conf.proto.llc_snap_proto.oui[2] = 0x0;
        } else if (has_snap_8021h) {
            group_conf.proto.llc_snap_proto.oui[0] = 0x0;
            group_conf.proto.llc_snap_proto.oui[1] = 0x0;
            group_conf.proto.llc_snap_proto.oui[2] = 0xF8;
        } else { /* valid OUI */
            group_conf.proto.llc_snap_proto.oui[0] = (oui >> 16) & 0xFF;
            group_conf.proto.llc_snap_proto.oui[1] = (oui >> 8) & 0xFF;
            group_conf.proto.llc_snap_proto.oui[2] = oui & 0xFF;
        }
        if ((group_conf.proto.llc_snap_proto.oui[0] == 0) &&
            (group_conf.proto.llc_snap_proto.oui[1] == 0) &&
            (group_conf.proto.llc_snap_proto.oui[2] == 0)) {
            if (pid < 0x600) {
                return VCL_ERROR_INVALID_PID;
            }
        }
    } else if (has_llc) {
        group_conf.proto_encap_type = VTSS_APPL_VCL_PROTO_ENCAP_LLC_OTHER;
        group_conf.proto.llc_other_proto.dsap = dsap;
        group_conf.proto.llc_other_proto.ssap = ssap;
    } else {
        return VCL_ERROR_NO_PROTO_SELECTED;
    }
    memcpy(group_conf.name, name, MAX_GROUP_NAME_LEN);
    group_conf.name[strlen((char *)name)] = '\0';
    if ((rc = vcl_proto_mgmt_proto_add(&group_conf)) != VTSS_RC_OK) {
        ICLI_PRINTF("Adding Protocol to Group mapping Failed\n");
        return rc;
    }
    T_DG(TRACE_GRP_ICLI, "Exit Add Protocol to Group mapping");
    return VTSS_RC_OK;
}

mesa_rc vcl_icli_protocol_group_del(u32 session_id, BOOL has_eth2, u16 etype, BOOL has_arp, BOOL has_ip, BOOL has_ipx,
                                    BOOL has_at, BOOL has_snap, uint oui, BOOL has_rfc_1042, BOOL has_snap_8021h, u16 pid,
                                    BOOL has_llc, u8 dsap, u8 ssap)
{
    vcl_proto_mgmt_group_conf_proto_t group_conf;
    u8                                proto_cnt = 0;
    mesa_rc                           rc = VTSS_RC_OK;

    T_DG(TRACE_GRP_ICLI, "Enter Delete Protocol to Group mapping");
    if (has_eth2) {
        proto_cnt++;
    }
    if (has_snap) {
        proto_cnt++;
    }
    if (has_llc) {
        proto_cnt++;
    }
    if (proto_cnt > 1) {
        return VCL_ERROR_INVALID_PROTO_CNT;
    }
    if (has_eth2) {
        group_conf.proto_encap_type = VTSS_APPL_VCL_PROTO_ENCAP_ETH2;
        if (has_arp) {
            etype = ETHERTYPE_ARP;
        } else if (has_ip) {
            etype = ETHERTYPE_IP;
        } else if (has_ipx) {
            etype = ETHERTYPE_IPX;
        } else if (has_at) {
            etype = ETHERTYPE_AT;
        }
        group_conf.proto.eth2_proto.eth_type = etype;
    } else if (has_snap) {
        group_conf.proto_encap_type = VTSS_APPL_VCL_PROTO_ENCAP_LLC_SNAP;
        group_conf.proto.llc_snap_proto.pid = pid;
        if (has_rfc_1042) {
            group_conf.proto.llc_snap_proto.oui[0] = 0x0;
            group_conf.proto.llc_snap_proto.oui[1] = 0x0;
            group_conf.proto.llc_snap_proto.oui[2] = 0x0;
        } else if (has_snap_8021h) {
            group_conf.proto.llc_snap_proto.oui[0] = 0x0;
            group_conf.proto.llc_snap_proto.oui[1] = 0x0;
            group_conf.proto.llc_snap_proto.oui[2] = 0xF8;
        } else { /* valid OUI */
            group_conf.proto.llc_snap_proto.oui[0] = (oui >> 16) & 0xFF;
            group_conf.proto.llc_snap_proto.oui[1] = (oui >> 8) & 0xFF;
            group_conf.proto.llc_snap_proto.oui[2] = oui & 0xFF;
        }
        if ((group_conf.proto.llc_snap_proto.oui[0] == 0) &&
            (group_conf.proto.llc_snap_proto.oui[1] == 0) &&
            (group_conf.proto.llc_snap_proto.oui[2] == 0)) {
            if (pid < 0x600) {
                return VCL_ERROR_INVALID_PID;
            }
        }
    } else if (has_llc) {
        group_conf.proto_encap_type = VTSS_APPL_VCL_PROTO_ENCAP_LLC_OTHER;
        group_conf.proto.llc_other_proto.dsap = dsap;
        group_conf.proto.llc_other_proto.ssap = ssap;
    } else {
        return VCL_ERROR_NO_PROTO_SELECTED;
    }
    if ((rc = vcl_proto_mgmt_proto_del(&group_conf)) != VTSS_RC_OK) {
        ICLI_PRINTF("Deleting Protocol to Group mapping Failed\n");
        return rc;
    }
    T_DG(TRACE_GRP_ICLI, "Exit Delete Protocol to Group mapping");
    return VTSS_RC_OK;
}

mesa_rc vcl_icli_protocol_add(u32 session_id, char *name, mesa_vid_t vid, icli_stack_port_range_t *plist)
{
    vcl_proto_mgmt_group_conf_entry_global_t entry;
    vcl_proto_mgmt_group_conf_entry_local_t  local_entry;
    switch_iter_t                            sit;
    port_iter_t                              pit;
    mesa_rc                                  rc = VTSS_RC_OK;

    T_DG(TRACE_GRP_ICLI, "Enter Add Group to VID mapping");
    if (name == NULL) {
        return VCL_ERROR_NULL_GROUP_NAME;
    }
    memset(&entry, 0, sizeof(entry));
    memcpy(entry.name, name, MAX_GROUP_NAME_LEN);
    entry.name[strlen((char *)name)] = '\0';
    entry.vid = vid;
    if (vcl_proto_mgmt_conf_get(VTSS_ISID_GLOBAL, &entry, FALSE, FALSE) != VTSS_RC_OK) {
        memset(&entry, 0, sizeof(entry));
    } else {
        if (entry.vid != vid) {
            T_DG(TRACE_GRP_ICLI, "The provided VLAN ID does not match the one of the existing Protocol Group entry. If you wanted to update the VID, then you must delete the entry first (updating the VID is not possible!)");
            return VCL_ERROR_ENTRY_DIFF_VID;
        }
    }
    // Loop over all configurable switches in usid order...
    (void)icli_switch_iter_init(&sit);
    // ...provided they're also in the plist.
    while (icli_switch_iter_getnext(&sit, plist)) {
        memset(&local_entry, 0, sizeof(local_entry));
        memcpy(local_entry.name, name, MAX_GROUP_NAME_LEN);
        local_entry.name[strlen((char *)name)] = '\0';
        local_entry.vid = vid;
        memcpy(local_entry.ports, entry.ports[sit.isid - VTSS_ISID_START], sizeof(entry.ports[sit.isid - VTSS_ISID_START]));
        // Loop over all ports in uport order...
        (void)icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_NORMAL);
        // ... provided they're also in the plist.
        while (icli_port_iter_getnext(&pit, plist)) {
            local_entry.ports[pit.iport] = TRUE;
        }
        if (memcmp(local_entry.ports, entry.ports[sit.isid - VTSS_ISID_START], sizeof(entry.ports[sit.isid - VTSS_ISID_START]))) {
            if ((rc = vcl_proto_mgmt_conf_add(sit.isid, &local_entry)) != VTSS_RC_OK) {
                ICLI_PRINTF("Adding Group to VLAN mapping Failed\n");
                return rc;
            }
        } else {
            T_DG(TRACE_GRP_ICLI, "The provided port list matches the existing one and therefore no update is required to the mapping");
        }
    }
    T_DG(TRACE_GRP_ICLI, "Exit Add Group to VID mapping");
    return VTSS_RC_OK;
}

mesa_rc vcl_icli_protocol_del(u32 session_id, char *name, icli_stack_port_range_t *plist)
{
    vcl_proto_mgmt_group_conf_entry_global_t entry;
    vcl_proto_mgmt_group_conf_entry_local_t  local_entry;
    switch_iter_t                            sit;
    port_iter_t                              pit;
    mesa_rc                                  rc = VTSS_RC_OK;
    mesa_port_list_t                         ports;

    T_DG(TRACE_GRP_ICLI, "Enter Delete Group to VID mapping");
    if (name == NULL) {
        return VCL_ERROR_NULL_GROUP_NAME;
    }
    memset(&entry, 0, sizeof(entry));
    memcpy(entry.name, name, MAX_GROUP_NAME_LEN);
    entry.name[strlen((char *)name)] = '\0';
    if ((rc = vcl_proto_mgmt_conf_get(VTSS_ISID_GLOBAL, &entry, FALSE, FALSE)) != VTSS_RC_OK) {
        return rc;
    }
    // Loop over all configurable switches in usid order...
    (void)icli_switch_iter_init(&sit);
    // ...provided they're also in the plist.
    while (icli_switch_iter_getnext(&sit, plist)) {
        memset(&local_entry, 0, sizeof(local_entry));
        memcpy(local_entry.name, name, MAX_GROUP_NAME_LEN);
        local_entry.name[strlen((char *)name)] = '\0';
        local_entry.vid = entry.vid;
        memcpy(local_entry.ports, entry.ports[sit.isid - VTSS_ISID_START], sizeof(entry.ports[sit.isid - VTSS_ISID_START]));
        // Loop over all ports in uport order...
        (void)icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_NORMAL);
        // ... provided they're also in the plist.
        while (icli_port_iter_getnext(&pit, plist)) {
            local_entry.ports[pit.iport] = FALSE;
        }
        if (memcmp(local_entry.ports, entry.ports[sit.isid - VTSS_ISID_START], sizeof(entry.ports[sit.isid - VTSS_ISID_START]))) {
            if (memcmp(ports, local_entry.ports, sizeof(local_entry.ports))) {
                if ((rc = vcl_proto_mgmt_conf_add(sit.isid, &local_entry)) != VTSS_RC_OK) {
                    return rc;
                }
            } else {
                if ((rc = vcl_proto_mgmt_conf_del(sit.isid, &local_entry)) != VTSS_RC_OK) {
                    ICLI_PRINTF("Deleting Group to VLAN mapping Failed\n");
                    return rc;
                }
            }
        } else {
            T_DG(TRACE_GRP_ICLI, "The provided port list matches the existing one and therefore no update is required to the mapping");
        }
    }
    T_DG(TRACE_GRP_ICLI, "Exit Delete Group to VID mapping");
    return VTSS_RC_OK;
}

// Function for adding mac-based
// IN : ipv4   - Pointer to the IP address and mask
//      vce_id - Entry id
//      plist  - List of ports to configure
mesa_rc vcl_icli_mac_add(mesa_mac_t mac_addr, mesa_vid_t vid, icli_stack_port_range_t *plist)
{
    vcl_mac_mgmt_vce_conf_global_t entry;
    vcl_mac_mgmt_vce_conf_local_t  local_entry;
    switch_iter_t                  sit;
    port_iter_t                    pit;
    mesa_rc                        rc = VTSS_RC_OK;

    T_DG(TRACE_GRP_ICLI, "Enter Add MAC to VID mapping");
    memset(&entry, 0, sizeof(entry));
    memcpy(&entry.smac, &mac_addr, sizeof(mac_addr));
    if (vcl_mac_mgmt_conf_get(VTSS_ISID_GLOBAL, &entry, FALSE, FALSE) != VTSS_RC_OK) {
        memset(&entry, 0, sizeof(entry));
    } else {
        if (entry.vid != vid) {
            T_DG(TRACE_GRP_ICLI, "The provided VLAN ID does not match the one of the existing MAC entry. If you wanted to update the VID, then you must delete the entry first (updating the VID is not possible!)");
            return VCL_ERROR_ENTRY_DIFF_VID;
        }
    }
    // Loop over all configurable switches in usid order...
    (void)icli_switch_iter_init(&sit);
    // ...provided they're also in the plist.
    while (icli_switch_iter_getnext(&sit, plist)) {
        memset(&local_entry, 0, sizeof(local_entry));
        /* Populate the mac_vlan_entry to pass it to VCL module */
        memcpy(&local_entry.smac, &mac_addr, sizeof(mac_addr));
        local_entry.vid = vid;
        memcpy(local_entry.ports, entry.ports[sit.isid - VTSS_ISID_START], sizeof(entry.ports[sit.isid - VTSS_ISID_START]));
        // Loop over all ports in uport order...
        (void)icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_NORMAL);
        // ... provided they're also in the plist.
        while (icli_port_iter_getnext(&pit, plist)) {
            local_entry.ports[pit.iport] = TRUE;
        }
        if (memcmp(local_entry.ports, entry.ports[sit.isid - VTSS_ISID_START], sizeof(entry.ports[sit.isid - VTSS_ISID_START]))) {
            if ((rc = vcl_mac_mgmt_conf_add(sit.isid, &local_entry)) != VTSS_RC_OK) {
                return rc;
            }
        } else {
            T_DG(TRACE_GRP_ICLI, "The provided port list matches the existing one and therefore no update is required to the mapping");
        }
    }
    T_DG(TRACE_GRP_ICLI, "Exit Add MAC to VID mapping");
    return VTSS_RC_OK;
}

mesa_rc vcl_icli_mac_del(u32 session_id, mesa_mac_t mac_addr, icli_stack_port_range_t *plist)
{
    vcl_mac_mgmt_vce_conf_global_t entry;
    vcl_mac_mgmt_vce_conf_local_t  local_entry;
    switch_iter_t                  sit;
    port_iter_t                    pit;
    mesa_port_list_t               ports;
    mesa_rc                        rc = VTSS_RC_OK;

    T_DG(TRACE_GRP_ICLI, "Enter Delete MAC to VID mapping");
    memset(&entry, 0, sizeof(entry));
    memcpy(&entry.smac, &mac_addr, sizeof(mac_addr));
    if ((rc = vcl_mac_mgmt_conf_get(VTSS_ISID_GLOBAL, &entry, FALSE, FALSE)) != VTSS_RC_OK) {
        return rc;
    }
    // Loop over all configurable switches in usid order...
    (void)icli_switch_iter_init(&sit);
    // ...provided they're also in the plist.
    while (icli_switch_iter_getnext(&sit, plist)) {
        memset(&local_entry, 0, sizeof(local_entry));
        /* Populate the mac_vlan_entry to pass it to VCL module */
        memcpy(&local_entry.smac, &mac_addr, sizeof(mac_addr));
        local_entry.vid = entry.vid;
        memcpy(local_entry.ports, entry.ports[sit.isid - VTSS_ISID_START], sizeof(entry.ports[sit.isid - VTSS_ISID_START]));
        // Loop over all ports in uport order...
        (void)icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_NORMAL);
        // ... provided they're also in the plist.
        while (icli_port_iter_getnext(&pit, plist)) {
            local_entry.ports[pit.iport] = FALSE;
        }
        if (memcmp(local_entry.ports, entry.ports[sit.isid - VTSS_ISID_START], sizeof(entry.ports[sit.isid - VTSS_ISID_START]))) {
            if (memcmp(ports, local_entry.ports, sizeof(local_entry.ports))) {
                if ((rc = vcl_mac_mgmt_conf_add(sit.isid, &local_entry)) != VTSS_RC_OK) {
                    return rc;
                }
            } else {
                if ((rc = vcl_mac_mgmt_conf_del(sit.isid, &mac_addr)) != VTSS_RC_OK) {
                    return rc;
                }
            }
        } else {
            T_DG(TRACE_GRP_ICLI, "The provided port list matches the existing one and therefore no update is required to the mapping");
        }
    }
    T_DG(TRACE_GRP_ICLI, "Exit Delete MAC to VID mapping");
    return VTSS_RC_OK;
}

mesa_rc vcl_icli_address(u32 session_id, BOOL has_int, icli_stack_port_range_t *plist, BOOL has_set, BOOL has_dmac, BOOL has_smac)
{
    switch_iter_t                 sit;
    port_iter_t                   pit;
    mesa_vcl_port_conf_t          conf;
    BOOL                          first_entry = TRUE;
    mesa_rc                       rc = VTSS_RC_OK;

    T_DG(TRACE_GRP_ICLI, "Enter Debug VCL Address");
    // Loop over all configurable switches in usid order...
    (void)icli_switch_iter_init(&sit);
    // ...provided they're also in the plist.
    while (icli_switch_iter_getnext(&sit, plist)) {
        // Loop over all ports in uport order...
        (void)icli_port_iter_init(&pit, sit.isid, PORT_ITER_FLAGS_NORMAL);
        // ... provided they're also in the plist.
        while (icli_port_iter_getnext(&pit, plist)) {
            memset(&conf, 0, sizeof(conf));
            if (has_set) {
                if (has_dmac) {
                    conf.dmac_dip = TRUE;
                } else {
                    if (has_smac) {
                        conf.dmac_dip = FALSE;
                    }
                }
                if ((rc = mesa_vcl_port_conf_set(NULL, pit.iport, &conf)) != VTSS_RC_OK) {
                    return rc;
                }
            } else {
                if (first_entry) {
                    first_entry = FALSE;
                    ICLI_PRINTF("Port  Address\n");
                }
                if (( rc = mesa_vcl_port_conf_get(NULL, pit.iport, &conf)) != VTSS_RC_OK) {
                    return rc;
                } else {
                    ICLI_PRINTF("%-6u%s\n", pit.uport, conf.dmac_dip ? "DMAC/DIP" : "SMAC/SIP");
                }
            }
        }
    }
    T_DG(TRACE_GRP_ICLI, "Exit Debug VCL Address");
    return VTSS_RC_OK;
}

mesa_rc vcl_icli_debug_show_mac(u32 session_id, BOOL has_address, mesa_mac_t *mac_addr)
{
    vcl_mac_mgmt_vce_conf_local_t entry;
    BOOL                          first, next, entry_found = FALSE, mac_found = FALSE;
    i8                            str_buf[ICLI_STR_MAX_LEN];

    T_DG(TRACE_GRP_ICLI, "Enter Debug Show MAC");
    memset(&entry, 0, sizeof(entry));
    // Distinguish between show specific MAC Address and show everything
    if (has_address) {
        first = FALSE;
        next = FALSE;
        memcpy(&entry.smac, mac_addr, sizeof(*mac_addr));
        T_DG(TRACE_GRP_ICLI, "Searching for entry with MAC address %02x:%02x:%02x:%02x:%02x:%02x",  entry.smac.addr[0],
             entry.smac.addr[1], entry.smac.addr[2], entry.smac.addr[3], entry.smac.addr[4], entry.smac.addr[5]);
    } else {
        first = TRUE;
        next = FALSE;
        T_DG(TRACE_GRP_ICLI, "Looping through all the entries");
    }
    while ((mac_found == FALSE) && (vcl_mac_mgmt_conf_local_get(&entry, first, next) == VTSS_RC_OK)) {
        first = FALSE;
        next = TRUE;
        if (has_address) {
            if (memcmp(mac_addr, &entry.smac, sizeof(entry.smac)) != 0) {
                T_IG(TRACE_GRP_ICLI, "Returned MAC address does not match to the requested one");
                break;
            } else {
                // Entry has been found, so stopping the mgmt loop
                mac_found = TRUE;
            }
        }
        if (!entry_found) { // Printing header if this is the very first entry found
            icli_parm_header(session_id, "MAC Address        VID   Interfaces");
            entry_found = TRUE;
        }
        ICLI_PRINTF("%02x:%02x:%02x:%02x:%02x:%02x  %-4u  %s\n", entry.smac.addr[0], entry.smac.addr[1],
                    entry.smac.addr[2], entry.smac.addr[3], entry.smac.addr[4], entry.smac.addr[5],
                    entry.vid, icli_iport_list_txt(entry.ports, str_buf));
    }
    if (!entry_found && has_address) {
        ICLI_PRINTF("Entry with MAC address %02x-%02x-%02x-%02x-%02x-%02x was not found in the switch\n", mac_addr->addr[0],
                    mac_addr->addr[1], mac_addr->addr[2], mac_addr->addr[3], mac_addr->addr[4], mac_addr->addr[5]);
    }
    T_DG(TRACE_GRP_ICLI, "Exit Debug Show MAC");
    return VTSS_RC_OK;
}

#if defined(VTSS_SW_OPTION_IP)
mesa_rc vcl_icli_debug_show_ipsubnet(u32 session_id, icli_ipv4_subnet_t *ipv4)
{
    vcl_ip_mgmt_vce_conf_local_t entry;
    BOOL                         first, next, entry_found = FALSE, ip_found = FALSE;
    i8                           str_buf[ICLI_STR_MAX_LEN];
    i8                           ip_str[100];
    u32                          mask_len = 0;
    i8                           i;
    mesa_ipv4_t                  ip_addr = 0, ip_mask;

    T_DG(TRACE_GRP_ICLI, "Enter Debug Show Subnet");
    memset(&entry, 0, sizeof(entry));
    // Distinguish between show specific Subnet and show everything
    if (ipv4->ip) {
        first = FALSE;
        next = FALSE;
        entry.ip_addr = ipv4->ip;
        VTSS_RC(vtss_conv_ipv4mask_to_prefix(ipv4->netmask, &mask_len));
        entry.mask_len = (u8) mask_len;
        T_DG(TRACE_GRP_ICLI, "Searching for a specific subnet entry");
    } else {
        first = TRUE;
        next = FALSE;
        T_DG(TRACE_GRP_ICLI, "Looping through all the entries");
    }
    while ((ip_found == FALSE) && (vcl_ip_mgmt_conf_local_get(&entry, first, next) == VTSS_RC_OK)) {
        first = FALSE;
        next = TRUE;
        if (ipv4->ip) {
            ip_mask = 0;
            for (i = 31; i >= (32 - ((u8) mask_len)); i--) {
                ip_mask |= (1 << i);
            }
            ip_addr = ipv4->ip;
            ip_addr = ip_addr & ip_mask;
            if ((ip_addr != entry.ip_addr) || (((u8) mask_len) != entry.mask_len)) {
                T_RG(TRACE_GRP_ICLI, "Returned IP subnet does not match to the requested one");
                break;
            } else {
                // Entry has been found, so stopping the mgmt loop
                ip_found = TRUE;
            }
        }

        if (!entry_found) { // Printing header if this is the very first entry found
            icli_parm_header(session_id, "IP Address       Mask Length  VID   Interfaces");
            entry_found = TRUE;
        }
        ICLI_PRINTF("%-15s  %-11u  %-4u  %s\n", misc_ipv4_txt(entry.ip_addr, ip_str),
                    entry.mask_len, entry.vid, icli_iport_list_txt(entry.ports, str_buf));
    }
    if (!entry_found && (ipv4->ip)) {
        ICLI_PRINTF("Entry with IP subnet was not found in the switch\n");
    }
    T_DG(TRACE_GRP_ICLI, "Exit Debug Show Subnet");
    return VTSS_RC_OK;
}
#endif

mesa_rc vcl_icli_debug_show_protocol(u32 session_id, BOOL has_eth2, u16 etype, BOOL has_arp, BOOL has_ip, BOOL has_ipx,
                                     BOOL has_at, BOOL has_snap, uint oui, BOOL has_rfc_1042, BOOL has_snap_8021h, u16 pid,
                                     BOOL has_llc, u8 dsap, u8 ssap)
{
    vcl_proto_mgmt_proto_conf_local_t entry;
    BOOL                              first, next, found_entry = FALSE;
    i8                                str_buf[ICLI_STR_MAX_LEN];
    mesa_rc                           rc = VTSS_RC_OK;

    T_DG(TRACE_GRP_ICLI, "Enter Debug Show Protocol");
    memset(&entry, 0, sizeof(entry));
    if (has_eth2) {
        entry.proto_encap_type = VTSS_APPL_VCL_PROTO_ENCAP_ETH2;
        if (has_arp) {
            etype = ETHERTYPE_ARP;
        } else if (has_ip) {
            etype = ETHERTYPE_IP;
        } else if (has_ipx) {
            etype = ETHERTYPE_IPX;
        } else if (has_at) {
            etype = ETHERTYPE_AT;
        }
        entry.proto.eth2_proto.eth_type = etype;
    } else if (has_snap) {
        entry.proto_encap_type = VTSS_APPL_VCL_PROTO_ENCAP_LLC_SNAP;
        entry.proto.llc_snap_proto.pid = pid;
        if (has_rfc_1042) {
            entry.proto.llc_snap_proto.oui[0] = 0x0;
            entry.proto.llc_snap_proto.oui[1] = 0x0;
            entry.proto.llc_snap_proto.oui[2] = 0x0;
        } else if (has_snap_8021h) {
            entry.proto.llc_snap_proto.oui[0] = 0x0;
            entry.proto.llc_snap_proto.oui[1] = 0x0;
            entry.proto.llc_snap_proto.oui[2] = 0xF8;
        } else { /* valid OUI */
            entry.proto.llc_snap_proto.oui[0] = (oui >> 16) & 0xFF;
            entry.proto.llc_snap_proto.oui[1] = (oui >> 8) & 0xFF;
            entry.proto.llc_snap_proto.oui[2] = oui & 0xFF;
        }
        if ((entry.proto.llc_snap_proto.oui[0] == 0) &&
            (entry.proto.llc_snap_proto.oui[1] == 0) &&
            (entry.proto.llc_snap_proto.oui[2] == 0)) {
            if (pid < 0x600) {
                return VCL_ERROR_INVALID_PID;
            }
        }
    } else if (has_llc) {
        entry.proto_encap_type = VTSS_APPL_VCL_PROTO_ENCAP_LLC_OTHER;
        entry.proto.llc_other_proto.dsap = dsap;
        entry.proto.llc_other_proto.ssap = ssap;
    }
    // Distinguish between show specific MAC Address and show everything
    if (has_eth2 | has_snap | has_llc) {
        first = FALSE;
        next = FALSE;
        T_DG(TRACE_GRP_ICLI, "Searching for a specific protocol");
        while ((rc = vcl_proto_mgmt_conf_local_proto_get(&entry, first, next)) == VTSS_RC_OK) {
            if (!found_entry) {
                found_entry = TRUE;
                ICLI_PRINTF("Protocol Type  Protocol (Value)          VID   Ports\n");
                ICLI_PRINTF("-------------  ------------------------  ----  -----\n");
            }
            if (entry.proto_encap_type == VTSS_APPL_VCL_PROTO_ENCAP_ETH2) {
                ICLI_PRINTF("%-13s  ETYPE:0x%-4x              %-4d  %s\n", vcl_proto_mgmt_encaptype2string(entry.proto_encap_type),
                            entry.proto.eth2_proto.eth_type, entry.vid, icli_iport_list_txt(entry.ports, str_buf));
            } else if (entry.proto_encap_type == VTSS_APPL_VCL_PROTO_ENCAP_LLC_SNAP) {
                ICLI_PRINTF("%-13s  OUI-%02x:%02x:%02x; PID:0x%-4x  %-4d  %s\n", vcl_proto_mgmt_encaptype2string(entry.proto_encap_type),
                            entry.proto.llc_snap_proto.oui[0], entry.proto.llc_snap_proto.oui[1],
                            entry.proto.llc_snap_proto.oui[2], entry.proto.llc_snap_proto.pid, entry.vid,
                            icli_iport_list_txt(entry.ports, str_buf));
            } else {
                ICLI_PRINTF("%-13s  DSAP:0x%-2x; SSAP:0x%-2x      %-4d  %s\n",
                            vcl_proto_mgmt_encaptype2string(entry.proto_encap_type), entry.proto.llc_other_proto.dsap,
                            entry.proto.llc_other_proto.ssap, entry.vid, icli_iport_list_txt(entry.ports, str_buf));
            }
        }
        if (!found_entry) {
            ICLI_PRINTF("The requested protocol was not found in the switch\n");
            return rc;
        }
    } else {
        first = TRUE;
        next = FALSE;
        T_DG(TRACE_GRP_ICLI, "Looping through all the entries");
        while (vcl_proto_mgmt_conf_local_proto_get(&entry, first, next) == VTSS_RC_OK) {
            if (first == TRUE) {
                ICLI_PRINTF("Protocol Type  Protocol (Value)          VID   Ports\n");
                ICLI_PRINTF("-------------  ------------------------  ----  -----\n");
                first = FALSE;
            }
            if (entry.proto_encap_type == VTSS_APPL_VCL_PROTO_ENCAP_ETH2) {
                ICLI_PRINTF("%-13s  ETYPE:0x%-4x              %-4d  %s\n", vcl_proto_mgmt_encaptype2string(entry.proto_encap_type),
                            entry.proto.eth2_proto.eth_type, entry.vid, icli_iport_list_txt(entry.ports, str_buf));
            } else if (entry.proto_encap_type == VTSS_APPL_VCL_PROTO_ENCAP_LLC_SNAP) {
                ICLI_PRINTF("%-13s  OUI-%02x:%02x:%02x; PID:0x%-4x  %-4d  %s\n", vcl_proto_mgmt_encaptype2string(entry.proto_encap_type),
                            entry.proto.llc_snap_proto.oui[0], entry.proto.llc_snap_proto.oui[1],
                            entry.proto.llc_snap_proto.oui[2], entry.proto.llc_snap_proto.pid, entry.vid,
                            icli_iport_list_txt(entry.ports, str_buf));
            } else {
                ICLI_PRINTF("%-13s  DSAP:0x%-2x; SSAP:0x%-2x      %-4d  %s\n",
                            vcl_proto_mgmt_encaptype2string(entry.proto_encap_type), entry.proto.llc_other_proto.dsap,
                            entry.proto.llc_other_proto.ssap, entry.vid, icli_iport_list_txt(entry.ports, str_buf));
            }
            next = TRUE;
        }
    }
    T_DG(TRACE_GRP_ICLI, "Exit Debug Show Protocol");
    return VTSS_RC_OK;
}


/****************************************************************************/
/*  End of file.                                                            */
/****************************************************************************/
