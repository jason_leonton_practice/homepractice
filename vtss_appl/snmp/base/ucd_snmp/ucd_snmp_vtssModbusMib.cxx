/*
 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable format
 (e.g. HEX file) and only in or with products utilizing the Microsemi switch and
 PHY products.  The source code of the software may not be disclosed, transmitted
 or distributed without the prior written permission of Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all ownership,
 copyright, trade secret and proprietary rights in the software and its source code,
 including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL WARRANTIES
 OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES ARE EXPRESS,
 IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION, WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND NON-INFRINGEMENT.
*/

//       It is based on UCD-SNMP APIs, we should not do any change unless the implement
//       is different with standard MIB definition. For example:
//       1. The specific OID is not supported.
//       2. The 'read-write' operation doesn't supported.
//       3. The specific variable range is different from standard definition.

#include <main.h>
#include "vtss_os_wrapper_snmp.h"
#include "ucd_snmp_vtssModbusMib.h"
#include "vtssModbusMib.h"
#include "ucd_snmp_callout.h"   // ucd_snmp_callout_malloc(), ucd_snmp_callout_free()
#include "snmp_custom_api.h"
#include "mibContextTable.h" // mibContextTable_register()
#include "vtss_modbus_api.h"


// #define VTSSEVENTMIB_NOT_SUPPORTED       0   /* Excpetion case 1. */
// #define VTSSEVENTMIB_ONLY_RO_SUPPORTED   1   /* Excpetion case 2. */
// #define VTSSEVENTMIB_DIFFERENT_RANGE     1   /* Excpetion case 3. */



/******************************************************************************/
//
// Local function declarations
//
/******************************************************************************/
FindVarMethod vtssModbusMib_var;
FindVarMethod vtssModbusTcp_var;
WriteMethod vtssModbusTcpMode_write;



/******************************************************************************/
//
// Local variable declarations
//
/******************************************************************************/


/*
 * variable vtssModbusMib_variables:
 *   this variable defines function callbacks and type return information
 *   for the vtssModbusMib mib section
 */

struct variable7 vtssModbusMib_variables[] = {
    /*
     * magic number, variable type, ro/rw, callback fn, L, oidsuffix
     */

#define VTSSMODBUSTCPMODE		1
    {VTSSMODBUSTCPMODE, ASN_INTEGER, RWRITE, vtssModbusTcp_var, 4, {1,1,1, 1}},
};

#define REGISTER_REDEFINE_MIB(descr, var, vartype, theoid, oidlen) {\
        if (register_mib(descr, (struct variable *) var, sizeof(struct vartype), \
                         sizeof(var)/sizeof(struct vartype),            \
                         theoid, oidlen) != MIB_REGISTERED_OK ) \
            DEBUGMSGTL(("register_mib", "%s registration failed\n", descr)); \
    }

/******************************************************************************/
// ucd_snmp_init_vtssModbusMib()
// Initializes the UCD-SNMP-part of the VTSS-MODBUS-MIB:vtssModbusMib.
/******************************************************************************/
void ucd_snmp_init_vtssModbusMib(void)
{
    DEBUGMSGTL(("vtssModbusMib", "Initializing\n"));

    // Register mib tree to UCD-SNMP core engine
#if 1
    oid *theoid = snmp_private_mib_get_private_oid_modbus();
    int oid_len = snmp_private_mib_oid_len_get() + 2;
    // Register mibContextTable
    mibContextTable_register(theoid, oid_len, "VTSS-MODBUS : vtssModbusMib");
    REGISTER_REDEFINE_MIB("vtssModbusMib", vtssModbusMib_variables, variable7, theoid, oid_len);
#else
    REGISTER_MIB("vtssModbusMib", vtssModbusMib_variables, variable7, vtssModbusMib_variables_oid);
#endif
}


/******************************************************************************/
//
// Variable scalar functions
//
/******************************************************************************/
/*
 * vtssModbusMib_var():
 *   This function is called every time the agent gets a request for
 *   a scalar variable that might be found within your mib section
 *   registered above.  It is up to you to do the right thing and
 *   return the correct value.
 *     You should also correct the value of "var_len" if necessary.
 *
 *   Please see the documentation for more information about writing
 *   module extensions, and check out the examples in the examples
 *   and mibII directories.
 */
u_char *
vtssModbusMib_var(struct variable *vp,
          oid     *name,
          size_t  *length,
          int     exact,
          size_t  *var_len,
          WriteMethod **write_method)
{

    *write_method = NULL;
    if (header_generic(vp, name, length, exact, var_len, write_method) == MATCH_FAILED) {
        return NULL;
    }

    /*
     * this is where we do the value assignments for the mib results.
     */
    switch (vp->magic) {
    default:
        DEBUGMSGTL(("snmpd", "unknown sub-id %d in vtssEventMib_var\n", vp->magic));
    }
    return NULL;
}


/******************************************************************************/
//
// Variable table functions
//
/******************************************************************************/
/*
 * vtssModbusTcp_var():
 *   Handle this table separately from the scalar value case.
 *   The workings of this are basically the same as for vtssEventMib_var above.
 */
u_char *
vtssModbusTcp_var(struct variable *vp,
       oid     *name,
       size_t  *length,
       int     exact,
       size_t  *var_len,
       WriteMethod **write_method)
{
    int mode;
    static long VAR = 0;

    *write_method = NULL;
 
// No indexes, so we can use header_generic
    if (header_generic(vp, name, length, exact, var_len, write_method)
        == MATCH_FAILED) {
        return NULL;
    }
    
    if(vtssModbusTcpMode_get(&mode)){
        return NULL;
    }

    switch (vp->magic) {
        case VTSSMODBUSTCPMODE:
            *write_method = vtssModbusTcpMode_write;
            VAR = (i32) mode;
            *var_len = sizeof(VAR);
            return (u_char *) & VAR;
        default:
        DEBUGMSGTL(("snmpd", "unknown sub-id %d in vtssModbusTcp_var\n",
                    vp->magic));
    }
    return NULL;

}



/******************************************************************************/
//
// Write table functions
//
/******************************************************************************/
int
vtssModbusTcpMode_write(int      action,
           u_char   *var_val,
           u_char   var_val_type,
           size_t   var_val_len,
           u_char   *statP,
           oid      *name,
           size_t   name_len)
{
    
    static long           buf = 1, old_buf = 1;
    size_t          max_size;
    long            intval;

    max_size = sizeof(long);
    intval = *((long *) var_val);

    switch (action) {
    case RESERVE1: {
        if (var_val_type != ASN_INTEGER) {
            (void) snmp_log(LOG_ERR,
                            "write to vtssModbusTcpMode: not ASN_INTEGER\n");
            return SNMP_ERR_WRONGTYPE;
        }
        if (var_val_len > max_size) {
            (void) snmp_log(LOG_ERR,
                            "write to vtssModbusTcpMode: bad length\n");
            return SNMP_ERR_WRONGLENGTH;
        }
        if (intval != 1 && intval != 2) {
            (void) snmp_log(LOG_ERR,
                            "write to vtssModbusTcpMode: bad value\n");
            return SNMP_ERR_WRONGVALUE;
        }
        break;
    }
    case RESERVE2: {
        /*
        * Allocate memory and similar resources
        */
        break;
    }
    case FREE: {
        /*
        * Release any resources that have been allocated
        */
        break;
    }
    case ACTION: {
        /*
        * The variable has been stored in 'value' for you to use,
        * and you have just been asked to do something with it.
        * Note that anything done here must be reversable in the UNDO case
        */
        /*
        * Save to current configuration
        */
        buf = *((long *) var_val);
        
        if(vtssModbusTcpMode_set(buf)){
            return SNMP_ERR_RESOURCEUNAVAILABLE;
        }
        break;
    }
    case UNDO: {
        /*
        * Back out any changes made in the ACTION case
        */
        /*
        * Restore current configuration form old configuration
        */
        buf = old_buf;
        break;
    }
    case COMMIT: {
        /*
        * Things are working well, so it's now safe to make the change
        * permanently.  Make sure that anything done here can't fail!
        */
        /*
        * Update old configuration
        */
        old_buf = buf;
        break;
    }
    }
    return SNMP_ERR_NOERROR;
}


