/*
 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable format
 (e.g. HEX file) and only in or with products utilizing the Microsemi switch and
 PHY products.  The source code of the software may not be disclosed, transmitted
 or distributed without the prior written permission of Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all ownership,
 copyright, trade secret and proprietary rights in the software and its source code,
 including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL WARRANTIES
 OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES ARE EXPRESS,
 IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION, WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND NON-INFRINGEMENT.
*/

// Note: This file originally auto-generated by mib2c using vtss_mib2c_ucd_snmp.conf v3.40
//       It is based on UCD-SNMP APIs, we should not do any change unless the implement
//       is different with standard MIB definition. For example:
//       1. The specific OID is not supported.
//       2. The 'read-write' operation doesn't supported.
//       3. The specific variable range is different from standard definition.

#include <main.h>
#include "vtss_os_wrapper_snmp.h"
#include "ucd_snmp_vtssEventMib.h"
#include "vtssEventMib.h"
#include "ucd_snmp_callout.h"   // ucd_snmp_callout_malloc(), ucd_snmp_callout_free()
#include "snmp_custom_api.h"
#include "mibContextTable.h" // mibContextTable_register()

// #define VTSSEVENTMIB_NOT_SUPPORTED       0   /* Excpetion case 1. */
// #define VTSSEVENTMIB_ONLY_RO_SUPPORTED   1   /* Excpetion case 2. */
// #define VTSSEVENTMIB_DIFFERENT_RANGE     1   /* Excpetion case 3. */


/******************************************************************************/
//
// Local data structure declaration
//
/******************************************************************************/
// The data structure for return value, UCD-SNMP engine needs as address point for processing get operation
typedef struct {
    long                long_ret;
} vtssEventMib_ret_t;


/******************************************************************************/
//
// Local function declarations
//
/******************************************************************************/
FindVarMethod vtssEventMib_var;
FindVarMethod vtssEventPowerTable_var;
FindVarMethod vtssEventInterfaceTable_var;
FindVarMethod vtssEventDdmi_var;
WriteMethod vtssEventPowerRelayStatus_write;
WriteMethod vtssEventInterfaceRelayStatus_write;
WriteMethod vtssEventDdmiRelayStatus_write;


/******************************************************************************/
//
// Local variable declarations
//
/******************************************************************************/
/*lint -esym(459, vtssEventMib_global_ret) */
// The variable is protected by thread
// The UCD-SNMP engine needs as address point for processing get operation
static vtssEventMib_ret_t vtssEventMib_global_ret;

/*
 * vtssEventMib_variables_oid:
 *   this is the top level oid that we want to register under.  This
 *   is essentially a prefix, with the suffix appearing in the
 *   variable below.
 */
// The registered OID is replaced by LNTN-specfic dynamic generated OID
// static oid vtssEventMib_variables_oid[] = {1,3,6,1,4,1,6603,1,5001};



/*
 * variable vtssEventMib_variables:
 *   this variable defines function callbacks and type return information
 *   for the vtssEventMib mib section
 */

struct variable7 vtssEventMib_variables[] = {
    /*
     * magic number, variable type, ro/rw, callback fn, L, oidsuffix
     */

#define VTSSEVENTPOWERRELAYSTATUS		1
    {VTSSEVENTPOWERRELAYSTATUS, ASN_INTEGER, RWRITE, vtssEventPowerTable_var, 5, {1,1,1, 1, 2}},
#define VTSSEVENTINTERFACERELAYSTATUS		2
    {VTSSEVENTINTERFACERELAYSTATUS, ASN_INTEGER, RWRITE, vtssEventInterfaceTable_var, 5, {1,2,1, 1, 2}},
#define VTSSEVENTDDMIRELAYSTATUS		2
    {VTSSEVENTDDMIRELAYSTATUS, ASN_INTEGER, RWRITE, vtssEventDdmi_var, 3, {1, 3, 1}},
};

#define REGISTER_REDEFINE_MIB(descr, var, vartype, theoid, oidlen) {\
        if (register_mib(descr, (struct variable *) var, sizeof(struct vartype), \
                         sizeof(var)/sizeof(struct vartype),            \
                         theoid, oidlen) != MIB_REGISTERED_OK ) \
            DEBUGMSGTL(("register_mib", "%s registration failed\n", descr)); \
    }

/******************************************************************************/
// ucd_snmp_init_vtssEventMib()
// Initializes the UCD-SNMP-part of the VTSS-EVENT-MIB:vtssEventMib.
/******************************************************************************/
void ucd_snmp_init_vtssEventMib(void)
{
    DEBUGMSGTL(("vtssEventMib", "Initializing\n"));

    // Register mib tree to UCD-SNMP core engine
#if 1
    oid *theoid = snmp_private_mib_get_private_oid_event();
    int oid_len = snmp_private_mib_oid_len_get() + 2;
    // Register mibContextTable
    mibContextTable_register(theoid, oid_len, "VTSS-EVENT : vtssEventMib");
    REGISTER_REDEFINE_MIB("vtssEventMib", vtssEventMib_variables, variable7, theoid, oid_len);
#else
    REGISTER_MIB("vtssEventMib", vtssEventMib_variables, variable7, vtssEventMib_variables_oid);
#endif
}


/******************************************************************************/
//
// Variable scalar functions
//
/******************************************************************************/
/*
 * vtssEventMib_var():
 *   This function is called every time the agent gets a request for
 *   a scalar variable that might be found within your mib section
 *   registered above.  It is up to you to do the right thing and
 *   return the correct value.
 *     You should also correct the value of "var_len" if necessary.
 *
 *   Please see the documentation for more information about writing
 *   module extensions, and check out the examples in the examples
 *   and mibII directories.
 */
u_char *
vtssEventMib_var(struct variable *vp,
          oid     *name,
          size_t  *length,
          int     exact,
          size_t  *var_len,
          WriteMethod **write_method)
{

    *write_method = NULL;
    if (header_generic(vp, name, length, exact, var_len, write_method) == MATCH_FAILED) {
        return NULL;
    }


    /*
     * this is where we do the value assignments for the mib results.
     */
    switch (vp->magic) {
    default:
        DEBUGMSGTL(("snmpd", "unknown sub-id %d in vtssEventMib_var\n", vp->magic));
    }
    return NULL;
}

/******************************************************************************/
// vtssEventPowerTable_parse()
// Parse the table entry key from #name.
//
// Returns:
//  -1 on error
//   0 on getnext or getexact
//   1 on getfirst
/******************************************************************************/
static int
vtssEventPowerTable_parse(oid     *name,
           size_t  *length,
           int     exact,
           vtssEventPowerTable_entry_t *table_entry)
{
#if 1
    size_t  op_pos = snmp_private_mib_oid_len_get() + 5 + 2;
#else
	size_t  op_pos = 12 + 2;
#endif
    oid     *op = (oid *)(name + op_pos);

    memset(table_entry, 0, sizeof(*table_entry));
#if 1
    if (exact && *length < (snmp_private_mib_oid_len_get() + 5 + 1 + 1)) {
#else
	if (exact && *length < (12 + 1 + 1)) {
#endif
        return -1;
    } else if (!exact && *length <= op_pos) {
        if (vtssEventPowerTableEntry_getfirst(table_entry)) {
            return -1;
        }
        return 1; /* getfirst */
    }

    if (*length > op_pos) {
        table_entry->vtssEventPowerIfIndex = (long) * op++;
        op_pos++;
    } else if (exact) {
        return -1;
    } else {
        return 0;
    }

    if (exact && *length != op_pos) {
        return -1;
    }

    return 0;
}

/******************************************************************************/
// vtssEventPowerTable_fillobj()
// Fills in #name according to the table entry key.
/******************************************************************************/
static int
vtssEventPowerTable_fillobj(oid     *name,
             size_t  *length,
             vtssEventPowerTable_entry_t *table_entry)
{
#if 1
    int     name_pos = snmp_private_mib_oid_len_get() + 5 + 2;
#else
	int     name_pos = 12 + 2;
#endif

    name[name_pos++] = (oid) table_entry->vtssEventPowerIfIndex;

    *length = name_pos;
    return 0;
}


/******************************************************************************/
//
// Variable table functions
//
/******************************************************************************/
/*
 * vtssEventPowerTable_var():
 *   Handle this table separately from the scalar value case.
 *   The workings of this are basically the same as for vtssEventMib_var above.
 */
u_char *
vtssEventPowerTable_var(struct variable *vp,
       oid     *name,
       size_t  *length,
       int     exact,
       size_t  *var_len,
       WriteMethod **write_method)
{
    int                 rc;
    oid                 newname[MAX_OID_LEN];
    size_t              newname_len;
    vtssEventPowerTable_entry_t table_entry;

    *write_method = NULL;
    memcpy((char *) newname, (char *) vp->name, (int) (vp->namelen * sizeof(oid)));
    newname_len = vp->namelen;

    if (memcmp(name, vp->name, sizeof(oid) * vp->namelen) != 0) {
        memcpy(name, vp->name, sizeof(oid) * vp->namelen);
        *length = vp->namelen;
    }

    if ((rc = vtssEventPowerTable_parse(name, length, exact, &table_entry)) < 0) {
        return NULL;
    } else if (rc > 0) { /* getfirst */
        if (vtssEventPowerTable_fillobj(newname, &newname_len, &table_entry)) {
            return NULL;
        }
    } else {
        do {
            if (vtssEventPowerTableEntry_get(&table_entry, exact ? FALSE : TRUE)) {
                return NULL;
            }
            if (vtssEventPowerTable_fillobj(newname, &newname_len, &table_entry)) {
                return NULL;
            }
            if (exact) {
                break;
            }
            rc = snmp_oid_compare(newname, newname_len, name, *length);
        } while (rc < 0);
    }

    /*
     * fill in object part of name for current entry
     */
    memcpy((char *) name, (char *) newname, (int) (newname_len * sizeof(oid)));
    *length = newname_len;

    /*
     * this is where we do the value assignments for the mib results.
     */
    switch (vp->magic) {
    case VTSSEVENTPOWERRELAYSTATUS: {
        *write_method = vtssEventPowerRelayStatus_write;
        vtssEventMib_global_ret.long_ret = table_entry.vtssEventPowerRelayStatus;
        *var_len = sizeof(vtssEventMib_global_ret.long_ret);
        return (u_char *) &vtssEventMib_global_ret.long_ret;
    }
    default:
        DEBUGMSGTL(("snmpd", "unknown sub-id %d in var_vtssEventPowerTable\n", vp->magic));
    }
    return NULL;
}
/******************************************************************************/
// vtssEventInterfaceTable_parse()
// Parse the table entry key from #name.
//
// Returns:
//  -1 on error
//   0 on getnext or getexact
//   1 on getfirst
/******************************************************************************/
static int
vtssEventInterfaceTable_parse(oid     *name,
           size_t  *length,
           int     exact,
           vtssEventInterfaceTable_entry_t *table_entry)
{
#if 1
    size_t  op_pos = snmp_private_mib_oid_len_get() + 5 + 2;
#else
	size_t  op_pos = 12 + 2;
#endif
    oid     *op = (oid *)(name + op_pos);

    memset(table_entry, 0, sizeof(*table_entry));
#if 1
    if (exact && *length < (snmp_private_mib_oid_len_get() + 5 + 1 + 1)) {
#else
    if (exact && *length < (12 + 1 + 1)) {
#endif
        return -1;
    } else if (!exact && *length <= op_pos) {
        if (vtssEventInterfaceTableEntry_getfirst(table_entry)) {
            return -1;
        }
        return 1; /* getfirst */
    }

    if (*length > op_pos) {
        table_entry->vtssEventInterfaceIfIndex = (long) * op++;
        op_pos++;
    } else if (exact) {
        return -1;
    } else {
        return 0;
    }

    if (exact && *length != op_pos) {
        return -1;
    }

    return 0;
}

/******************************************************************************/
// vtssEventInterfaceTable_fillobj()
// Fills in #name according to the table entry key.
/******************************************************************************/
static int
vtssEventInterfaceTable_fillobj(oid     *name,
             size_t  *length,
             vtssEventInterfaceTable_entry_t *table_entry)
{
#if 1
    int     name_pos = snmp_private_mib_oid_len_get() + 5 + 2;
#else
	int     name_pos = 12 + 2;
#endif

    name[name_pos++] = (oid) table_entry->vtssEventInterfaceIfIndex;

    *length = name_pos;
    return 0;
}


/******************************************************************************/
//
// Variable table functions
//
/******************************************************************************/
/*
 * vtssEventInterfaceTable_var():
 *   Handle this table separately from the scalar value case.
 *   The workings of this are basically the same as for vtssEventMib_var above.
 */
u_char *
vtssEventInterfaceTable_var(struct variable *vp,
       oid     *name,
       size_t  *length,
       int     exact,
       size_t  *var_len,
       WriteMethod **write_method)
{
    int                 rc;
    oid                 newname[MAX_OID_LEN];
    size_t              newname_len;
    vtssEventInterfaceTable_entry_t table_entry;

    *write_method = NULL;
    memcpy((char *) newname, (char *) vp->name, (int) (vp->namelen * sizeof(oid)));
    newname_len = vp->namelen;

    if (memcmp(name, vp->name, sizeof(oid) * vp->namelen) != 0) {
        memcpy(name, vp->name, sizeof(oid) * vp->namelen);
        *length = vp->namelen;
    }

    if ((rc = vtssEventInterfaceTable_parse(name, length, exact, &table_entry)) < 0) {
        return NULL;
    } else if (rc > 0) { /* getfirst */
        if (vtssEventInterfaceTable_fillobj(newname, &newname_len, &table_entry)) {
            return NULL;
        }
    } else {
        do {
            if (vtssEventInterfaceTableEntry_get(&table_entry, exact ? FALSE : TRUE)) {
                return NULL;
            }
            if (vtssEventInterfaceTable_fillobj(newname, &newname_len, &table_entry)) {
                return NULL;
            }
            if (exact) {
                break;
            }
            rc = snmp_oid_compare(newname, newname_len, name, *length);
        } while (rc < 0);
    }

    /*
     * fill in object part of name for current entry
     */
    memcpy((char *) name, (char *) newname, (int) (newname_len * sizeof(oid)));
    *length = newname_len;

    /*
     * this is where we do the value assignments for the mib results.
     */
    switch (vp->magic) {
    case VTSSEVENTINTERFACERELAYSTATUS: {
        *write_method = vtssEventInterfaceRelayStatus_write;
        vtssEventMib_global_ret.long_ret = table_entry.vtssEventInterfaceRelayStatus;
        *var_len = sizeof(vtssEventMib_global_ret.long_ret);
        return (u_char *) &vtssEventMib_global_ret.long_ret;
    }
    default:
        DEBUGMSGTL(("snmpd", "unknown sub-id %d in var_vtssEventInterfaceTable\n", vp->magic));
    }
    return NULL;
}

/******************************************************************************/
//
// Variable table functions
//
/******************************************************************************/
/*
 * vtssEventDdmi_var():
 *   Handle this table separately from the scalar value case.
 *   The workings of this are basically the same as for vtssEventMib_var above.
 */
u_char *
vtssEventDdmi_var(struct variable *vp,
       oid     *name,
       size_t  *length,
       int     exact,
       size_t  *var_len,
       WriteMethod **write_method)
{
    int conf_value;
    static long VAR = 0;

    *write_method = NULL;

// No indexes, so we can use header_generic
    if (header_generic(vp, name, length, exact, var_len, write_method)
        == MATCH_FAILED) {
        return NULL;
    }

    if(vtssEventDdmi_get(&conf_value)){
        return NULL;
    }

    switch (vp->magic) {
        case VTSSEVENTDDMIRELAYSTATUS:
            *write_method = vtssEventDdmiRelayStatus_write;
            VAR = (i32) conf_value;
            *var_len = sizeof(VAR);
            return (u_char *) & VAR;
        default:
        DEBUGMSGTL(("snmpd", "unknown sub-id %d in vtssEventDdmiRelayStatus\n",
                    vp->magic));
    }
    return NULL;

}


/******************************************************************************/
//
// Write scalar functions
//
/******************************************************************************/


/******************************************************************************/
//
// Write table functions
//
/******************************************************************************/

/******************************************************************************/
// vtssEventPowerRelayStatus_write()
/******************************************************************************/
int
vtssEventPowerRelayStatus_write(int      action,
           u_char   *var_val,
           u_char   var_val_type,
           size_t   var_val_len,
           u_char   *statP,
           oid      *name,
           size_t   name_len)
{
    long set_value = var_val ? *((long *) var_val) : 0;
    vtssEventPowerTable_entry_t table_entry;
    static vtssEventPowerTable_entry_t *old_table_entry_p = NULL;

    switch (action) {
        case RESERVE1: {
            // Check syntax, variable valid range
            if (var_val_type != ASN_INTEGER) {
                (void) snmp_log(LOG_ERR, "write to vtssEventPowerRelayStatus: not ASN_INTEGER\n");
                return SNMP_ERR_WRONGTYPE;
            }
            if (var_val_len > sizeof(long)) {
                (void) snmp_log(LOG_ERR, "write to vtssEventPowerRelayStatus: bad length\n");
                return SNMP_ERR_WRONGLENGTH;
            }
            if (set_value != 1
                && set_value != 2
            ) {
                (void) snmp_log(LOG_ERR, "write to vtssEventPowerRelayStatus: bad value\n");
                return SNMP_ERR_WRONGVALUE;
            }
            break;
        }
        case RESERVE2: {
            // Allocate dynamic memory for saving the new configuration and initialize it
            if ((old_table_entry_p = (vtssEventPowerTable_entry_t *)ucd_snmp_callout_malloc(sizeof(*old_table_entry_p))) == NULL) {
                return SNMP_ERR_RESOURCEUNAVAILABLE;
            }
            if (vtssEventPowerTable_parse(name, &name_len, TRUE, old_table_entry_p)) {
                return SNMP_ERR_RESOURCEUNAVAILABLE;
            }
            if (vtssEventPowerTableEntry_get(old_table_entry_p, FALSE)) {
                return SNMP_ERR_RESOURCEUNAVAILABLE;
            }
            break;
        }
        case FREE: {
            // Release dynamic memory
            if (old_table_entry_p) {
                ucd_snmp_callout_free(old_table_entry_p);
                old_table_entry_p = NULL;
            }
            break;
        }
        case ACTION: {
            // Set new configuration
            table_entry = *old_table_entry_p;
            table_entry.vtssEventPowerRelayStatus = set_value;
            if (vtssEventPowerTableEntry_set(&table_entry)) {
                return SNMP_ERR_GENERR;
            }
            break;
        }
        case UNDO: {
            // Restore original configuration
            int undo_rc = vtssEventPowerTableEntry_set(old_table_entry_p);
            ucd_snmp_callout_free(old_table_entry_p);
            old_table_entry_p = NULL;
            if (undo_rc) {
                return SNMP_ERR_UNDOFAILED;
            }
            break;
        }
        case COMMIT: {
            ucd_snmp_callout_free(old_table_entry_p);
            old_table_entry_p = NULL;
            break;
        }
        default:
            break;
    }
    return SNMP_ERR_NOERROR;
}

/******************************************************************************/
// vtssEventInterfaceRelayStatus_write()
/******************************************************************************/
int
vtssEventInterfaceRelayStatus_write(int      action,
           u_char   *var_val,
           u_char   var_val_type,
           size_t   var_val_len,
           u_char   *statP,
           oid      *name,
           size_t   name_len)
{
    long set_value = var_val ? *((long *) var_val) : 0;
    vtssEventInterfaceTable_entry_t table_entry;
    static vtssEventInterfaceTable_entry_t *old_table_entry_p = NULL;

    switch (action) {
        case RESERVE1: {
            // Check syntax, variable valid range
            if (var_val_type != ASN_INTEGER) {
                (void) snmp_log(LOG_ERR, "write to vtssEventInterfaceRelayStatus: not ASN_INTEGER\n");
                return SNMP_ERR_WRONGTYPE;
            }
            if (var_val_len > sizeof(long)) {
                (void) snmp_log(LOG_ERR, "write to vtssEventInterfaceRelayStatus: bad length\n");
                return SNMP_ERR_WRONGLENGTH;
            }
            if (set_value != 1
                && set_value != 2
            ) {
                (void) snmp_log(LOG_ERR, "write to vtssEventInterfaceRelayStatus: bad value\n");
                return SNMP_ERR_WRONGVALUE;
            }
            break;
        }
        case RESERVE2: {
            // Allocate dynamic memory for saving the new configuration and initialize it
            if ((old_table_entry_p = (vtssEventInterfaceTable_entry_t *)ucd_snmp_callout_malloc(sizeof(*old_table_entry_p))) == NULL) {
                return SNMP_ERR_RESOURCEUNAVAILABLE;
            }
            if (vtssEventInterfaceTable_parse(name, &name_len, TRUE, old_table_entry_p)) {
                return SNMP_ERR_RESOURCEUNAVAILABLE;
            }
            if (vtssEventInterfaceTableEntry_get(old_table_entry_p, FALSE)) {
                return SNMP_ERR_RESOURCEUNAVAILABLE;
            }
            break;
        }
        case FREE: {
            // Release dynamic memory
            if (old_table_entry_p) {
                ucd_snmp_callout_free(old_table_entry_p);
                old_table_entry_p = NULL;
            }
            break;
        }
        case ACTION: {
            // Set new configuration
            table_entry = *old_table_entry_p;
            table_entry.vtssEventInterfaceRelayStatus = set_value;
            if (vtssEventInterfaceTableEntry_set(&table_entry)) {
                return SNMP_ERR_GENERR;
            }
            break;
        }
        case UNDO: {
            // Restore original configuration
            int undo_rc = vtssEventInterfaceTableEntry_set(old_table_entry_p);
            ucd_snmp_callout_free(old_table_entry_p);
            old_table_entry_p = NULL;
            if (undo_rc) {
                return SNMP_ERR_UNDOFAILED;
            }
            break;
        }
        case COMMIT: {
            ucd_snmp_callout_free(old_table_entry_p);
            old_table_entry_p = NULL;
            break;
        }
        default:
            break;
    }
    return SNMP_ERR_NOERROR;
}

/******************************************************************************/
// vtssEventDdmiRelayStatus_write()
/******************************************************************************/
int
vtssEventDdmiRelayStatus_write(int      action,
           u_char   *var_val,
           u_char   var_val_type,
           size_t   var_val_len,
           u_char   *statP,
           oid      *name,
           size_t   name_len)
{

    static long     buf = 1, old_buf = 1;
    size_t          max_size;
    long            intval;

    max_size = sizeof(long);
    intval = *((long *) var_val);

    switch (action) {
    case RESERVE1: {
        if (var_val_type != ASN_INTEGER) {
            (void) snmp_log(LOG_ERR,
                            "write to vtssEventDdmiRelayStatus: not ASN_INTEGER\n");
            return SNMP_ERR_WRONGTYPE;
        }
        if (var_val_len > max_size) {
            (void) snmp_log(LOG_ERR,
                            "write to vtssEventDdmiRelayStatus: bad length\n");
            return SNMP_ERR_WRONGLENGTH;
        }
        if (intval != 1 && intval != 2) {
            (void) snmp_log(LOG_ERR,
                            "write to vtssEventDdmiRelayStatus: bad value\n");
            return SNMP_ERR_WRONGVALUE;
        }
        break;
    }
    case RESERVE2: {
        /*
        * Allocate memory and similar resources
        */
        break;
    }
    case FREE: {
        /*
        * Release any resources that have been allocated
        */
        break;
    }
    case ACTION: {
        /*
        * The variable has been stored in 'value' for you to use,
        * and you have just been asked to do something with it.
        * Note that anything done here must be reversable in the UNDO case
        */
        /*
        * Save to current configuration
        */
        buf = *((long *) var_val);

        if(vtssEventDdmi_set(buf)){
            return SNMP_ERR_RESOURCEUNAVAILABLE;
        }
        break;
    }
    case UNDO: {
        /*
        * Back out any changes made in the ACTION case
        */
        /*
        * Restore current configuration form old configuration
        */
        buf = old_buf;
        break;
    }
    case COMMIT: {
        /*
        * Things are working well, so it's now safe to make the change
        * permanently.  Make sure that anything done here can't fail!
        */
        /*
        * Update old configuration
        */
        old_buf = buf;
        break;
    }
    }
    return SNMP_ERR_NOERROR;
}
