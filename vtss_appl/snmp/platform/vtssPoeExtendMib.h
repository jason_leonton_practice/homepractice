/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable format
 (e.g. HEX file) and only in or with products utilizing the Microsemi switch and
 PHY products.  The source code of the software may not be disclosed, transmitted
 or distributed without the prior written permission of Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all ownership,
 copyright, trade secret and proprietary rights in the software and its source code,
 including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL WARRANTIES
 OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES ARE EXPRESS,
 IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION, WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND NON-INFRINGEMENT.

*/

// Note: This file originally auto-generated by mib2c using vtss_mib2c_ucd_snmp.conf v3.40
#ifndef _VTSSPOEEXTENDMIB_H_
#define _VTSSPOEEXTENDMIB_H_

#include "vtss_os_wrapper_snmp.h"

#define VTSSPOEEXTENDMIB_STR_LEN_MAX    63  // FIXME: Redefine a sufficient value for saving the memory
#define VTSSPOEEXTENDMIB_OID_LEN_MAX    16  // FIXME: Redefine a sufficient value for saving the memory
#define VTSSPOEEXTENDMIB_BITS_LEN_MAX   4   // FIXME: Redefine a sufficient value for saving the memory


/******************************************************************************/
//
// Data structure declarations
//
/******************************************************************************/


// The table entry data structure for vtssPoePingAliveInterfacTable
typedef struct {
    // Entry keys
    long   vtssPoePingAliveInterfaceIfIndex;

    // Entry columns
    long vtssPoePingAliveInterfaceState;
    in_addr_t vtssPoePingAliveInterfaceAddress;
    long vtssPoePingAliveInterfaceInterval;
} vtssPoePingAliveInterfacTable_entry_t;

// The table entry data structure for vtssPoeScheduleTimeTable
typedef struct {
    // Entry keys
    long   vtssPoeScheduleId;
    long   vtssPoeScheduleWeekday;

    // Entry columns
    char vtssPoeScheduleTime[VTSSPOEEXTENDMIB_STR_LEN_MAX + 1];
    size_t  vtssPoeScheduleTime_len;
} vtssPoeScheduleTimeTable_entry_t;

// The table entry data structure for vtssPoeScheduleInterfacTable
typedef struct {
    // Entry keys
    long   vtssPoeScheduleInterfaceIfIndex;

    // Entry columns
    long vtssPoeScheduleInterfaceId;
    long vtssPoeScheduleInterfaceMode;
} vtssPoeScheduleInterfacTable_entry_t;

typedef struct {
    long   id;
    long   day;
} sched_table_t;
/******************************************************************************/
//
// Initial function
//
/******************************************************************************/
/**
  * \brief Initializes the SNMP-part of the VTSS-POE-MIB:vtssPoeExtendMib.
  **/
void vtssPoeExtendMib_init(void);


/******************************************************************************/
//
// Scalar access function declarations
//
/******************************************************************************/


/******************************************************************************/
//
// Table entry access function declarations
//
/******************************************************************************/
/**
  * \brief Get first table entry of vtssPoePingAliveInterfacTableEntry.
  *
  * \param table_entry [IN_OUT]: Pointer to structure that contains the table
  *                              entry to get the first table entry.
  *
  * \return: 0 if the operation success, non-zero value otherwise.
  **/
int vtssPoePingAliveInterfacTableEntry_getfirst(vtssPoePingAliveInterfacTable_entry_t *table_entry);

/**
  * \brief Get/Getnext table entry of vtssPoePingAliveInterfacTableEntry.
  *
  * \param table_entry [IN_OUT]: Pointer to structure that contains the table
  *                              entry to get/getnext the table entry.
  *
  * \return: 0 if the operation success, non-zero value otherwise.
  **/
int vtssPoePingAliveInterfacTableEntry_get(vtssPoePingAliveInterfacTable_entry_t *table_entry, int getnext);

/**
  * \brief Set table entry of vtssPoePingAliveInterfacTableEntry.
  *
  * \param table_entry [IN_OUT]: Pointer to structure that contains the table
  *                              entry to set the table entry.
  *
  * \return: 0 if the operation success, non-zero value otherwise.
  **/
int vtssPoePingAliveInterfacTableEntry_set(vtssPoePingAliveInterfacTable_entry_t *table_entry);
/**
  * \brief Get first table entry of vtssPoeScheduleTimeTableEntry.
  *
  * \param table_entry [IN_OUT]: Pointer to structure that contains the table
  *                              entry to get the first table entry.
  *
  * \return: 0 if the operation success, non-zero value otherwise.
  **/
int vtssPoeScheduleTimeTableEntry_getfirst(vtssPoeScheduleTimeTable_entry_t *table_entry);

/**
  * \brief Get/Getnext table entry of vtssPoeScheduleTimeTableEntry.
  *
  * \param table_entry [IN_OUT]: Pointer to structure that contains the table
  *                              entry to get/getnext the table entry.
  *
  * \return: 0 if the operation success, non-zero value otherwise.
  **/
int vtssPoeScheduleTimeTableEntry_get(vtssPoeScheduleTimeTable_entry_t *table_entry, int getnext);

/**
  * \brief Set table entry of vtssPoeScheduleTimeTableEntry.
  *
  * \param table_entry [IN_OUT]: Pointer to structure that contains the table
  *                              entry to set the table entry.
  *
  * \return: 0 if the operation success, non-zero value otherwise.
  **/
int vtssPoeScheduleTimeTableEntry_set(vtssPoeScheduleTimeTable_entry_t *table_entry);
/**
  * \brief Get first table entry of vtssPoeScheduleInterfacTableEntry.
  *
  * \param table_entry [IN_OUT]: Pointer to structure that contains the table
  *                              entry to get the first table entry.
  *
  * \return: 0 if the operation success, non-zero value otherwise.
  **/
int vtssPoeScheduleInterfacTableEntry_getfirst(vtssPoeScheduleInterfacTable_entry_t *table_entry);

/**
  * \brief Get/Getnext table entry of vtssPoeScheduleInterfacTableEntry.
  *
  * \param table_entry [IN_OUT]: Pointer to structure that contains the table
  *                              entry to get/getnext the table entry.
  *
  * \return: 0 if the operation success, non-zero value otherwise.
  **/
int vtssPoeScheduleInterfacTableEntry_get(vtssPoeScheduleInterfacTable_entry_t *table_entry, int getnext);

/**
  * \brief Set table entry of vtssPoeScheduleInterfacTableEntry.
  *
  * \param table_entry [IN_OUT]: Pointer to structure that contains the table
  *                              entry to set the table entry.
  *
  * \return: 0 if the operation success, non-zero value otherwise.
  **/
int vtssPoeScheduleInterfacTableEntry_set(vtssPoeScheduleInterfacTable_entry_t *table_entry);

#endif /* _VTSSPOEEXTENDMIB_H_ */

