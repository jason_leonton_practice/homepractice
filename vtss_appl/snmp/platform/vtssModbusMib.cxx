/*
 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable format
 (e.g. HEX file) and only in or with products utilizing the Microsemi switch and
 PHY products.  The source code of the software may not be disclosed, transmitted
 or distributed without the prior written permission of Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all ownership,
 copyright, trade secret and proprietary rights in the software and its source code,
 including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL WARRANTIES
 OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES ARE EXPRESS,
 IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION, WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND NON-INFRINGEMENT.
*/

//       Revise the "FIXME" parts to make it as a completed code.
#include <main.h>
#include "vtss_os_wrapper_snmp.h"
#include "vtss_snmp_api.h"
#include "vtssModbusMib.h"
#include "ucd_snmp_vtssModbusMib.h"
#include "mibContextTable.h" // mibContextTable_register()
// FIXME: Remove the include file if your implementation don't need to redefine the standard standard MIB objects
#include "snmp_mib_redefine.h"  // snmp_mib_redefine_register()
#include <vtss_module_id.h>
#include <vtss_trace_lvl_api.h>

#include "main.h"
#include "vtss_modbus_api.h"


// Trace module ID
#define VTSS_TRACE_MODULE_ID    VTSS_MODULE_ID_MODBUS
#define VTSS_SNMP_TRUE (1)
#define VTSS_SNMP_FALSE (2)


/******************************************************************************/
//
// Initial function
//
/******************************************************************************/
/**
  * \brief Initializes the SNMP-part of the VTSS-MODBUS-MIB:vtssModbusMib.
  **/
void vtssModbusMib_init(void)
{
    T_D("enter");
    ucd_snmp_init_vtssModbusMib();

#if 0 // FIXME: Remove whole block(#if0 - #endif) if your implementation don't need to redefine the standard MIB objects
    /* Register snmpMibRedefineTable */


    // vtssModbusTcpConfigMode
    oid vtssModbusTcpConfigMode_variables_oid[] = { 1,3,6,1,4,1,6603,1,5003, 1,1,1, 1 };
    snmp_mib_redefine_register(vtssModbusTcpConfigMode_variables_oid,
                               sizeof(vtssModbusTcpConfigMode_variables_oid) / sizeof(oid),
                               "VTSS-MODBUS-MIB : vtssModbusTcpConfigMode",
                               "TruthValue",
                               SNMP_MIB_ACCESS_TYPE_RWRITE,
                               SNMP_MIB_ACCESS_TYPE_RWRITE,
                               FALSE,
                               "\
{1 true} \
{2 false} \
");

#endif

    // FIXME: Place any other initialization you need here

    T_D("exit");
}



/******************************************************************************/
//
// conig access functions
//
/******************************************************************************/

int vtssModbusTcpMode_get(int * get_value)
{
    int conf_mode;
    if(modbus_mgmt_conf_get(&conf_mode)!=VTSS_OK){
        return -1;
    }

    *get_value = conf_mode ? VTSS_SNMP_TRUE: VTSS_SNMP_FALSE;

    return 0;
}

int vtssModbusTcpMode_set(long set_value)
{
    int rc=0;
    if(set_value==VTSS_SNMP_TRUE){
        rc=modbus_mgmt_conf_set(true);      
    }else{
        rc=modbus_mgmt_conf_set(false);
    }

    if (rc != VTSS_OK) {
        return -1;
    }

    return 0;
}


