/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "lacp_serializer.hxx"
#include "vtss/appl/lacp.h"
#include "vtss_common_iterator.hxx"

mesa_rc lacp_ifaceport_idx_itr(const vtss_ifindex_t *prev_ifindex, vtss_ifindex_t *next_ifindex)
{
    return vtss_appl_iterator_ifindex_front_port_exist(prev_ifindex, next_ifindex);
}

mesa_rc lacp_port_stats_clr_set( vtss_ifindex_t ifindex, const BOOL *const clear)
{
    if (clear && *clear) {
        return vtss_lacp_port_stats_clr(ifindex);
    }
    return VTSS_RC_OK;
}

mesa_rc lacp_port_stats_clr_dummy_get(vtss_ifindex_t ifindex, BOOL *const clear)
{
    if (clear && *clear) {
        *clear = FALSE;
    }
    return VTSS_RC_OK;
}

