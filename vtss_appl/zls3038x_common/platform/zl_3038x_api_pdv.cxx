/*

 Copyright (c) 2006-2018 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#include "assert.hxx"
#include "main.h"
#include "main_types.h"
#include "mscc/ethernet/switch/api.h"
#include "zl_3038x_api_pdv.h"
#include "zl_3038x_api_pdv_api.h"
#include "critd_api.h"
#include "unistd.h" // for sleep function

// zl30380 base interface
#include "zl303xx_DeviceIf.h"
#include "zl303xx_Error.h"
#include "zl303xx_Apr.h"
#include "zl303xx_LogToMsgQ.h"
#include "zl303xx_ExampleAprGlobals.h"
#include "zl303xx_ExampleUtils.h"
#include "zl303xx_ExampleAprBinding.h"
#include "zl303xx_DebugApr.h"
#include "zl303xx_ExampleApr.h"
#include "zl303xx_AprStatistics.h"
#include "zl303xx_Apr1Hz.h"
#include "zl303xx_Trace.h"
#include "interrupt_api.h"

#undef min
#undef max

/* interface to other components */
#include "vtss_tod_api.h"
#include "vtss_ptp_local_clock.h"
#include "vtss/appl/synce.h"
#include "synce_custom_clock_api.h"
#include "vtss_ptp_slave.h"

#define PDV_NO_OF_DOMAINS 4
zl303xx_ParamsS *zl303xx_Params_dpll;        // Pointers to a zl303xx_ParamsS structure associated with the primary SyncE DPLL chip.
                                             // Usually, this pointer is initialized by the driver for the zls30343, zls30363, silabs or ServalT DPLL.

zl303xx_ParamsS *zl303xx_Params_generic[PDV_NO_OF_DOMAINS];  // Index 0-3: These are pointers to zl303xx_ParamsS structures representing timing doimains that are not associated
                                             //            with a physical DPLL (or in case of index 0 a physical DPLL like the zls30343 being controlled in
                                             //            generic mode). The domains may be using a software emulation or a capability in hardware to set an
                                             //            offset relative to the domain controlled by the SyncE DPLL.
static zl303xx_ParamsS *zl303xx_Params[PDV_NO_OF_DOMAINS];          // Index 0-3: These are pointers to zl303xx_ParamsS of the actual domains.

// these two variables are needed because the APR code uses a global variable to store the actual DPLL type, therefore we need to save the
// two alternative DPLL types that we can switch between when we change the preferred adjustment method.
// this method only partly solves the problem, i.e. it is solved for domain 0, but if domain 0 uses an other DPLL type than other domains we
// still have the problem.
// The right solution is to avoid using the global variable, but there is 396 regerences to this vatiable in the APR code, provided by TIM.
static zl303xx_DevTypeE default_dpll_type;
static zl303xx_DevTypeE alternative_dpll_type;

#define CGUID_CHECK(expr) { if (expr >= PDV_NO_OF_DOMAINS) return VTSS_RC_ERROR; }

/* ================================================================= *
 *  Trace definitions
 * ================================================================= */


#if (VTSS_TRACE_ENABLED)

static vtss_trace_reg_t trace_reg = {
    VTSS_TRACE_MODULE_ID, "zl_30380", "ZL30380 Filter algorithm Module."
};

static vtss_trace_grp_t trace_grps[TRACE_GRP_CNT] = {
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default (ZL30380 core)",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP|VTSS_TRACE_FLAGS_USEC
    },
    /* TRACE_GRP_CRIT */ {
        "crit",
        "Critical Regions",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP|VTSS_TRACE_FLAGS_USEC
    },
    /* TRACE_GRP_PTP_INTF */ {
        "ptp_intf",
        "ZL-PTP application interface functions",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP|VTSS_TRACE_FLAGS_USEC
    },
    /* TRACE_GRP_OS_PORT */ {
        "os_port",
        "ZL-PTP application OS portability layer",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP|VTSS_TRACE_FLAGS_USEC
    },
    /* TRACE_GRP_ZL_TRACE */ {
        "zl_trace",
        "ZL-PTP application zl trace",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP|VTSS_TRACE_FLAGS_USEC
    },
};
#endif /* VTSS_TRACE_ENABLED */

#include "synce_spi_if.h" // Note: Must be included after the trace definitions as it depends on these.

static struct {
    BOOL ready;                 /* ZL3034X Initited  */
    critd_t datamutex;          /* Global data protection */
} zl30380_global;

/* ================================================================= *
 *  Persistent Configuration definitions
 * ================================================================= */

typedef struct zl30380_config_t {
    i8 version;             /* configuration version, to be changed if this structure is changed */
    zl303xx_AprAlgTypeModeE algTypeMode;
    zl303xx_AprOscillatorFilterTypesE oscillatorFilter;
    zl303xx_AprFilterTypesE filter;
    zl303xx_BooleanE enable1Hz;
    zl303xx_BooleanE bHoldover;
    BOOL apr_server_notify_flag;
    u32 mode;
    u32 adj_freq_min_phase;
} zl30380_config_t;

static zl30380_config_t config_data ;

static int my_clock_option;
static bool my_generic;
static vtss_ptp_hybrid_transient curTransient = VTSS_PTP_HYBRID_TRANSIENT_NOT_ACTIVE;

/**
 * Read the PTP configuration.
 * \param create indicates a new default configuration block should be created.
 *
 */
static void zl30380_conf_read(BOOL create)
{
    ZL_3036X_DATA_LOCK();
    /* initialize run-time options to reasonable values */
    config_data.version = ZL30380_CONF_VERSION;
    config_data.algTypeMode = ZL303XX_NATIVE_PKT_FREQ;
    config_data.oscillatorFilter = ZL303XX_OCXO_S3E;
    config_data.filter = ZL303XX_BW_0_FILTER;
    config_data.enable1Hz = ZL303XX_FALSE;
    config_data.bHoldover = ZL303XX_FALSE;
    config_data.apr_server_notify_flag = FALSE;
    config_data.mode = 1;
    config_data.adj_freq_min_phase = 20; /* zl default value */
    ZL_3036X_DATA_UNLOCK();
}

/* ================================================================= *
 *  Configuration definitions
 * ================================================================= */

static zl303xx_AprAddServerS server[4];  // Support for 4 servers i.e. PTP streams.

typedef struct {
    exampleAprConfigIdentifiersE filter_mode;
} vtss_zls_server_cfg_t;
static vtss_zls_server_cfg_t server_cfg[4];  // Support for 4 servers i.e. PTP streams.

static const char *apr_alg_type_2_txt(zl303xx_AprAlgTypeModeE value)
{
    switch (value) {
        case ZL303XX_NATIVE_PKT_FREQ                   : return "NATIVE_PKT_FREQ";
        case ZL303XX_NATIVE_PKT_FREQ_UNI               : return "NATIVE_PKT_FREQ_UNI";
        case ZL303XX_NATIVE_PKT_FREQ_CES               : return "NATIVE_PKT_FREQ_CES";
        case ZL303XX_NATIVE_PKT_FREQ_ACCURACY          : return "NATIVE_PKT_FREQ_ACCURACY";
        case ZL303XX_NATIVE_PKT_FREQ_ACCURACY_UNI      : return "NATIVE_PKT_FREQ_ACCURACY_UNI";
        case ZL303XX_NATIVE_PKT_FREQ_FLEX              : return "NATIVE_PKT_FREQ_FLEX";
        case ZL303XX_BOUNDARY_CLK                      : return "BOUNDARY_CLK";
        case ZL303XX_NATIVE_PKT_FREQ_ACCURACY_FDD      : return "NATIVE_PKT_FREQ_ACCURACY_FDD";
        case ZL303XX_XDSL_FREQ_ACCURACY                : return "XDSL_FREQ_ACCURACY";
        case ZL303XX_CUSTOM_FREQ_ACCURACY_200          : return "CUSTOM_FREQ_ACCURACY_200";
        case ZL303XX_CUSTOM_FREQ_ACCURACY_15           : return "CUSTOM_FREQ_ACCURACY_15";
    }
    return "INVALID";
}

static const char *apr_osc_filter_type_2_txt(zl303xx_AprOscillatorFilterTypesE value)
{
    switch (value) {
        case ZL303XX_TCXO                   : return "TCXO";
        case ZL303XX_TCXO_FAST              : return "TCXO_FAST";
        case ZL303XX_OCXO_S3E               : return "OCXO_S3E";
        case ZL303XX_OCXO_S3E_C             : return "OCXO_S3E_C";
        case ZL303XX_OCXO_S3E_DEPRECATED    : return "OCXO_S3E_DEP";
        case ZL303XX_OCXO_S3E_C_DEPRECATED  : return "OCXO_S3E_C_DEP";
        default                             : return "INVALID";
    }
}

static const char *apr_filter_type_2_txt(zl303xx_AprFilterTypesE value)
{
    switch (value) {
        case ZL303XX_BW_0_FILTER      : return "BW_0_FILTER";
        case ZL303XX_BW_1_FILTER      : return "BW_1_FILTER";
        case ZL303XX_BW_2_FILTER      : return "BW_2_FILTER";
        case ZL303XX_BW_3_FILTER      : return "BW_3_FILTER";
        case ZL303XX_BW_4_FILTER      : return "BW_4_FILTER";
        case ZL303XX_BW_5_FILTER      : return "BW_5_FILTER";
        case ZL303XX_BW_6_FILTER      : return "BW_6_FILTER";
        case ZL303XX_BW_7_FILTER      : return "BW_7_FILTER";
        default                       : return "INVALID";
    }
}

/****************************************************************************
 * Configuration API
 ****************************************************************************/

static exampleAprConfigIdentifiersE filter_type_2_mode(u32 filter_type);

/*
 * Process timestamps received in the PTP protocol.
 */
BOOL zl_30380_process_timestamp(u32 domain, u16 serverId, vtss_zl_30380_process_timestamp_t *ts)
{
    zl303xx_AprTimestampS aprTs;
    zlStatusE status = ZL303XX_OK;
    BOOL rc = TRUE;

    memset(&aprTs, 0, sizeof(aprTs));
#if 0  // problem with Zarlink corrfield
    if (ts->corr != 0LL) {
        vtss_tod_add_TimeInterval(&ts->tx_ts, &ts->tx_ts, &ts->corr);
        ts->corr = 0LL;
    }
#endif
    T_DG(TRACE_GRP_PTP_INTF, "domain %d, serverId %d", domain, serverId);
    aprTs.serverId = serverId;
    aprTs.txTs.second.lo = ts->tx_ts.seconds;
    aprTs.txTs.second.hi = ts->tx_ts.sec_msb;
    aprTs.txTs.subSecond = ts->tx_ts.nanoseconds;
    aprTs.rxTs.second.lo = ts->rx_ts.seconds;
    aprTs.rxTs.second.hi = ts->rx_ts.sec_msb;
    aprTs.rxTs.subSecond = ts->rx_ts.nanoseconds;
    aprTs.bForwardPath = (zl303xx_BooleanE)ts->fwd_path;
    aprTs.corr.hi = ts->corr>>32;
    aprTs.corr.lo = ts->corr & 0xffffffff;
    aprTs.bPeerDelay = (zl303xx_BooleanE)ts->peer_delay;
    aprTs.peerMeanDelay.hi = 0;
    aprTs.peerMeanDelay.lo = 0;
    aprTs.offsetFromMaster.hi = 0;
    aprTs.offsetFromMaster.lo = 0;
    aprTs.offsetFromMasterValid = ZL303XX_FALSE;
    T_NG(TRACE_GRP_PTP_INTF, "aprTs txTs %u:%u, rxTs %u:%u, fwd %d, corr %u:%u (%lld ns), peer_delay %d", aprTs.txTs.second.lo, aprTs.txTs.subSecond,
         aprTs.rxTs.second.lo, aprTs.rxTs.subSecond, aprTs.bForwardPath,
         aprTs.corr.hi, aprTs.corr.lo, ts->corr>>16, aprTs.bPeerDelay);
    status = zl303xx_AprProcessTimestamp(zl303xx_Params[domain], &aprTs);
    if (status == ZL303XX_OK) {
        T_DG(TRACE_GRP_PTP_INTF, "timestamp updated");
    } else {
        T_WG(TRACE_GRP_PTP_INTF, "zl303xx_AprProcessTimestamp failed with status = %u", status);
        rc = FALSE;
    }
    return rc;
}

mesa_rc zl_30380_apr_device_status_get(zl303xx_ParamsS *zl303xx_Params)
{
    mesa_rc rc = VTSS_RC_OK;

    if (zl303xx_GetAprDeviceStatus(zl303xx_Params) != ZL303XX_OK) {
        T_D("Error during Zarlink APR device status get");
        rc = VTSS_RC_ERROR;
    }
    return(rc);
}

mesa_rc zl_30380_apr_server_config_get(zl303xx_ParamsS *zl303xx_Params, Uint16T serverId)
{
    mesa_rc rc = VTSS_RC_OK;

    if (zl303xx_GetAprServerConfigInfo(zl303xx_Params, serverId) != ZL303XX_OK) {
        T_D("Error during Zarlink APR server config get");
        rc = VTSS_RC_ERROR;
    }
    return(rc);
}

mesa_rc zl_30380_apr_server_status_get(zl303xx_ParamsS *zl303xx_Params, Uint16T serverId)
{
    mesa_rc rc = VTSS_RC_OK;

    if (zl303xx_GetAprServerStatus(zl303xx_Params, serverId) != ZL303XX_OK) {
        T_D("Error during Zarlink APR server status get");
        rc = VTSS_RC_ERROR;
    }
    return(rc);
}

mesa_rc zl_30380_apr_force_holdover_set(u32 domain, BOOL enable)
{
    mesa_rc rc = VTSS_RC_OK;
    zlStatusE aperr;
    ZL_3036X_DATA_LOCK();
    if ((aperr = zl303xx_AprSetHoldover(zl303xx_Params[domain], enable ? ZL303XX_TRUE : ZL303XX_FALSE)) != ZL303XX_OK) {
        T_D("Error %d during Zarlink APR force holdover set", aperr);
        rc = VTSS_RC_ERROR;
    }
    ZL_3036X_DATA_UNLOCK();
    return(rc);
}

mesa_rc zl_30380_apr_statistics_get(zl303xx_ParamsS *zl303xx_Params)
{
    mesa_rc rc = VTSS_RC_OK;

    if (zl303xx_DebugGetAllAprStatistics(zl303xx_Params) != ZL303XX_OK) {
        T_D("Error during Zarlink APR Statistics get");
        rc = VTSS_RC_ERROR;
    }
    return(rc);
}

mesa_rc zl_30380_apr_statistics_reset(zl303xx_ParamsS *zl303xx_Params)
{
    mesa_rc rc = VTSS_RC_OK;

//    if (zl303xx_AprResetPerfStatistics(zl303xx_Params) != ZL303XX_OK) {
        T_D("Error during Zarlink APR Statistics reset");
        rc = VTSS_RC_ERROR;
//    }
    return(rc);
}

static void apr_server_notify(zl303xx_AprServerNotifyS *msg)
{
    ZL_3036X_DATA_LOCK();
    if (config_data.apr_server_notify_flag == TRUE) {
        exampleAprServerNotify(msg);
    }
    ZL_3036X_DATA_UNLOCK();
    return;
}

mesa_rc apr_server_notify_set(BOOL enable)
{
    ZL_3036X_DATA_LOCK();
    config_data.apr_server_notify_flag = enable;
    ZL_3036X_DATA_UNLOCK();
    return(VTSS_RC_OK);
}

mesa_rc apr_server_notify_get(BOOL *enable)
{
    ZL_3036X_DATA_LOCK();
    *enable = config_data.apr_server_notify_flag;
    ZL_3036X_DATA_UNLOCK();
    return(VTSS_RC_OK);
}


mesa_rc apr_server_one_hz_set(zl303xx_ParamsS *zl303xx_Params, BOOL enable)
{
    mesa_rc rc = VTSS_RC_OK;

    ZL_3036X_DATA_LOCK();
    config_data.enable1Hz = (zl303xx_BooleanE)enable;
    T_D("Set 1 Hz mode if %p != NULL",zl303xx_Params);
    if (zl303xx_Params != NULL) {
        if (zl303xx_AprSetDevice1HzEnabled(zl303xx_Params, (zl303xx_BooleanE)enable) != ZL303XX_OK) {
            rc = VTSS_RC_ERROR;
        }
    }
    ZL_3036X_DATA_UNLOCK();
    return(rc);
}

mesa_rc apr_server_one_hz_get(BOOL *enable)
{
    ZL_3036X_DATA_LOCK();
    *enable = config_data.enable1Hz;
    ZL_3036X_DATA_UNLOCK();
    return(VTSS_RC_OK);
}

mesa_rc zl_30380_apr_config_dump(u16 cguId)
{
    mesa_rc rc = VTSS_RC_OK;
    CGUID_CHECK(cguId);

    /* Debugging Api Calls */

    printf("### zl303xx_GetAprDeviceConfigInfo\n");
    if (ZL303XX_OK != zl303xx_GetAprDeviceConfigInfo(zl303xx_Params[cguId])) rc = VTSS_RC_ERROR;
    printf("### zl303xx_GetAprServerConfigInfo\n");
    if (ZL303XX_OK != zl303xx_GetAprServerConfigInfo(zl303xx_Params[cguId], 0)) rc = VTSS_RC_ERROR;
    printf("### zl303xx_DebugAprGet1HzData FWD Path\n");
    if (ZL303XX_OK != zl303xx_DebugAprGet1HzData(zl303xx_Params[cguId], 0, 0)) rc = VTSS_RC_ERROR;
    printf("### 1Hz data FWD ####\n");
    if (ZL303XX_OK != zl303xx_AprPrint1HzData(zl303xx_Params[cguId], 0, 0)) rc = VTSS_RC_ERROR;
    printf("### zl303xx_DebugAprGet1HzData  Rev Path\n");
    if (ZL303XX_OK != zl303xx_DebugAprGet1HzData(zl303xx_Params[cguId], 1, 0)) rc = VTSS_RC_ERROR;
    printf("### 1Hz data REV ####\n");
    if (ZL303XX_OK != zl303xx_AprPrint1HzData(zl303xx_Params[cguId], 1, 0)) rc = VTSS_RC_ERROR;
    printf("### Other Params ####\n");
    if (ZL303XX_OK != zl303xx_DebugPrintAprByReferenceId(zl303xx_Params[cguId], 0)) rc = VTSS_RC_ERROR;
    return(rc);
}

mesa_rc zl_30380_apr_log_level_set(u8 level)
{
    mesa_rc rc = VTSS_RC_OK;

    if (zl303xx_SetAprLogLevel(level) != ZL303XX_OK) {
        rc = VTSS_RC_ERROR;
    }
    return(rc);
}

mesa_rc zl_30380_apr_log_level_get(u8 *level)
{
    mesa_rc rc = VTSS_RC_OK;

    *level = zl303xx_GetAprLogLevel();
    return(rc);
}

mesa_rc zl_30380_apr_ts_log_level_set(u8 level)
{
    mesa_rc rc = VTSS_RC_OK;

    if (level <= 2) {
        zl303xx_AprLogTimestampInputStart(level);
    } else {
        zl303xx_AprLogTimestampInputStop();
    }
    return(rc);
}

mesa_rc zl_30380_apr_ts_log_level_get(u8 *level)
{
    mesa_rc rc = VTSS_RC_OK;
    *level = 3; // tbd (saved persistent)
    return(rc);
}
/*
 * Process packet rate indications received in the PTP protocol.
 */

static  zl303xx_AprPktRateE rate_table [] = {
          ZL303XX_128_PPS,
          ZL303XX_64_PPS,
          ZL303XX_32_PPS,
          ZL303XX_16_PPS,
          ZL303XX_8_PPS,
          ZL303XX_4_PPS,
          ZL303XX_2_PPS,
          ZL303XX_1_PPS,
          ZL303XX_1_PP_2S,
          ZL303XX_1_PP_4S,
          ZL303XX_1_PP_8S,
          ZL303XX_1_PP_16S};

BOOL zl_30380_packet_rate_set(u32 domain, u16 serverId, i8 ptp_rate, BOOL forward)
{
    BOOL rc = TRUE;
    zl303xx_AprPktRateE fwdPktRate;
    zl303xx_AprPktRateE revPktRate;
    zl303xx_AprPktRateE new_rate = ZL303XX_1_PPS;
    zlStatusE status;

    if (ptp_rate == 0x7f) {
        if (forward) ptp_rate = -6;
        else ptp_rate = -4;
    }

    if (ptp_rate < -7 || ptp_rate > 4) {
        ptp_rate = 0;
    }

    if ((status = zl303xx_AprGetServerPktRate(zl303xx_Params[domain], serverId, &fwdPktRate, &revPktRate)) == ZL303XX_OK) {
        new_rate = rate_table[ptp_rate + 7];

        if ((forward && new_rate != fwdPktRate) || (!forward && new_rate != revPktRate)) {
            status = zl303xx_AprNotifyServerPktRateChange(zl303xx_Params[domain], serverId, (zl303xx_BooleanE)forward, new_rate);
            if (status == ZL303XX_OK) {
                T_IG(TRACE_GRP_PTP_INTF, "packet rate updated, rate = %d , forw = %d", new_rate, (zl303xx_BooleanE)forward);
            } else {
                T_WG(TRACE_GRP_PTP_INTF, "zl303xx_AprNotifyServerPktRateChange failed with status = %u", status);
                rc = FALSE;
            }
        }
    } else {
        T_WG(TRACE_GRP_PTP_INTF, "zl303xx_AprGetServerPktRate failed with status = %u", status);
    }

    return rc;
}

BOOL zl_30380_pdv_status_get(u32 domain, u16 serverId, vtss_slave_clock_state_t *pdv_clock_state, i32 *freq_offset)
{
    zl303xx_AprServerStatusFlagsS status_flags;

    zlStatusE status = zl303xx_AprGetServerStatusFlags(zl303xx_Params[domain], serverId, &status_flags);
    if (status == ZL303XX_OK) {
        switch (status_flags.state) {
            case ZL303XX_FREQ_LOCK_ACQUIRING:   *pdv_clock_state = VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKING; break;
            case ZL303XX_FREQ_LOCK_ACQUIRED:    *pdv_clock_state = VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKED; break;
            case ZL303XX_PHASE_LOCK_ACQUIRED:   *pdv_clock_state = VTSS_PTP_SLAVE_CLOCK_PHASE_LOCKED; break;
            case ZL303XX_HOLDOVER:              *pdv_clock_state = VTSS_PTP_SLAVE_CLOCK_HOLDOVER; break;
            case ZL303XX_REF_FAILED:            *pdv_clock_state = VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKING; break;  // Note: In this case and the cases below, *pdv_clock_state is set to
            case ZL303XX_UNKNOWN:               *pdv_clock_state = VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKING; break;  //       VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKING and not ZL303XX_REF_FAILED,
            case ZL303XX_MANUAL_FREERUN:        *pdv_clock_state = VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKING; break;  //       ZL303XX_UNKNOWN, ZL303XX_MANUAL_FREERUN, etc. as one would
            case ZL303XX_MANUAL_HOLDOVER:       *pdv_clock_state = VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKING; break;  //       expect. This is made so since vtss_ptp_slave may otherwise
            case ZL303XX_MANUAL_SERVO_HOLDOVER: *pdv_clock_state = VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKING; break;  //       stop generating delay_requests.
            case ZL303XX_NO_ACTIVE_SERVER:      *pdv_clock_state = VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKING; break;  //
            default:                            *pdv_clock_state = VTSS_PTP_SLAVE_CLOCK_FREQ_LOCKING; break;  //
        }

        status = zl303xx_AprGetServerFreqOffset(zl303xx_Params[domain], freq_offset);
        if (status != ZL303XX_OK) {
            T_WG(TRACE_GRP_PTP_INTF, "zl303xx_AprGetServerFreqOffset failed with status = %u", status);
        }
    } else {
        T_WG(TRACE_GRP_PTP_INTF, "zl303xx_AprGetServerStatusFlags failed with status = %u", status);
    }

    return (status == ZL303XX_OK) ? TRUE : FALSE;

}

mesa_rc  zl_30380_pdv_apr_server_status_get(u16 cguId, u16 serverId, char **s)
{
    static char buf[600];
    zl303xx_AprServerStatusFlagsS status_flags;
    zlStatusE status;
    mesa_rc  rc = VTSS_RC_OK;
    CGUID_CHECK(cguId);
    status = zl303xx_AprGetServerStatusFlags(zl303xx_Params[cguId], serverId, &status_flags);
    if (status != ZL303XX_OK) {
        T_WG(TRACE_GRP_PTP_INTF, "zl303xx_AprGetServerStatusFlags failed with status = %u", status);
        sprintf(buf, "Error reading APR server status.\n");
        rc = VTSS_RC_ERROR;
    }
    else {
        int n = 0;
        n += sprintf(&buf[n], "L1 = %s\n", (status_flags.L1 ? "True" : "False"));
        n += sprintf(&buf[n], "L2 = %s\n", (status_flags.L2 ? "True" : "False"));
        n += sprintf(&buf[n], "L3 = %s\n", (status_flags.L3 ? "True" : "False"));
        n += sprintf(&buf[n], "L = %s\n", (status_flags.L ? "True" : "False"));
        n += sprintf(&buf[n], "gstL = %s\n", (status_flags.gstL ? "True" : "False"));
        n += sprintf(&buf[n], "V = %s\n", (status_flags.V ? "True" : "False"));
        n += sprintf(&buf[n], "gstV = %s\n", (status_flags.gstV ? "True" : "False"));
        n += sprintf(&buf[n], "S = %s\n", (status_flags.S ? "True" : "False"));
        n += sprintf(&buf[n], "U = %s\n", (status_flags.U ? "True" : "False"));
        n += sprintf(&buf[n], "U1 = %s\n", (status_flags.U1 ? "True" : "False"));
        n += sprintf(&buf[n], "PE = %s\n", (status_flags.PE ? "True" : "False"));
        n += sprintf(&buf[n], "PA = %s\n", (status_flags.PA ? "True" : "False"));
        n += sprintf(&buf[n], "gstPA = %s\n", (status_flags.gstPA ? "True" : "False"));
        n += sprintf(&buf[n], "H = %s\n", (status_flags.H ? "True" : "False"));
        n += sprintf(&buf[n], "gstH = %s\n", (status_flags.gstH ? "True" : "False"));
        n += sprintf(&buf[n], "OOR = %s\n", (status_flags.OOR ? "True" : "False"));
        n += sprintf(&buf[n], "ttErrDetected = %s\n", (status_flags.ttErrDetected ? "True" : "False"));
        n += sprintf(&buf[n], "outageDetected = %s\n", (status_flags.outageDetected ? "True" : "False"));
        n += sprintf(&buf[n], "outlierDetected = %s\n", (status_flags.outlierDetected ? "True" : "False"));
        n += sprintf(&buf[n], "frrDetected = %s\n", (status_flags.frrDetected ? "True" : "False"));
        n += sprintf(&buf[n], "rrrDetected = %s\n", (status_flags.rrrDetected ? "True" : "False"));
        n += sprintf(&buf[n], "stepDetected = %s\n", (status_flags.stepDetected ? "True" : "False"));
        n += sprintf(&buf[n], "pktLossDetectedFwd = %s\n", (status_flags.pktLossDetectedFwd ? "True" : "False"));
        n += sprintf(&buf[n], "pktLossDetectedRev = %s\n\n", (status_flags.pktLossDetectedRev ? "True" : "False"));

        n += sprintf(&buf[n], "algPathFlag = ");
        switch (status_flags.algPathFlag) {
            case ZL303XX_USING_FWD_PATH: n += sprintf(&buf[n], "USING_FWD_PATH\n"); break;
            case ZL303XX_USING_REV_PATH: n += sprintf(&buf[n], "USING_REV_PATH\n"); break;
            case ZL303XX_USING_FWD_REV_COMBINE: n += sprintf(&buf[n], "USING_FWD_REV_COMBINE\n\n"); break;
        }

        n += sprintf(&buf[n], "Algorithm forward state = S%d\n", status_flags.algFwdState);
        n += sprintf(&buf[n], "Algorithm reverse state = S%d\n", status_flags.algRevState);
        n += sprintf(&buf[n], "State is = ");

        switch (status_flags.state) {
            case ZL303XX_FREQ_LOCK_ACQUIRING: n += sprintf(&buf[n], "FREQ_LOCK_ACQUIRING\n"); break;
            case ZL303XX_FREQ_LOCK_ACQUIRED: n += sprintf(&buf[n], "FREQ_LOCK_ACQUIRED\n"); break;
            case ZL303XX_PHASE_LOCK_ACQUIRED: n += sprintf(&buf[n], "PHASE_LOCK_ACQUIRED\n"); break;
            case ZL303XX_HOLDOVER: n += sprintf(&buf[n], "HOLDOVER\n"); break;
            case ZL303XX_REF_FAILED: n += sprintf(&buf[n], "REF_FAILED\n"); break;
            case ZL303XX_UNKNOWN: n += sprintf(&buf[n], "UNKNOWN\n"); break;
            case ZL303XX_MANUAL_FREERUN: n += sprintf(&buf[n], "MANUAL_FREERUN\n"); break;
            case ZL303XX_MANUAL_HOLDOVER: n += sprintf(&buf[n], "MANUAL_HOLDOVER\n"); break;
            case ZL303XX_MANUAL_SERVO_HOLDOVER: n += sprintf(&buf[n], "MANUAL_SERVO_HOLDOVER\n"); break;
            case ZL303XX_NO_ACTIVE_SERVER: n += sprintf(&buf[n], "NO_ACTIVE_SERVER\n"); break;
        }
    }
    *s = buf;
    return rc;
}

static const char *state_2_txt(zl303xx_AprStateE value)
{
    switch (value) {
        case ZL303XX_FREQ_LOCK_ACQUIRING: return "FREQ_LOCK_ACQUIRING";
        case ZL303XX_FREQ_LOCK_ACQUIRED: return "FREQ_LOCK_ACQUIRED";
        case ZL303XX_PHASE_LOCK_ACQUIRED: return "PHASE_LOCK_ACQUIRED";
        case ZL303XX_HOLDOVER : return "HOLDOVER ";
        case ZL303XX_REF_FAILED: return "REF_FAILED";
        case ZL303XX_UNKNOWN: return "UNKNOWN";
        case ZL303XX_MANUAL_FREERUN: return "MANUAL_FREERUN";
        case ZL303XX_MANUAL_HOLDOVER: return "MANUAL_HOLDOVER";
        case ZL303XX_MANUAL_SERVO_HOLDOVER: return "MANUAL_SERVO_HOLDOVER";
        case ZL303XX_NO_ACTIVE_SERVER:  return "NO_ACTIVE_SERVER";
    }
    return "INVALID";
}

static void apr_cgu_notify(zl303xx_AprCGUNotifyS *msg)
{

    if(msg->type == ZL303XX_CGU_STATE) {
        T_I("STATE flag changed to: %s", state_2_txt(msg->flags.state));
    }
    exampleAprCguNotify(msg);
}

mesa_rc zl_30380_holdover_set(u32 domain, int clock_inst, BOOL enable)
{
    mesa_rc rc = VTSS_RC_OK;

    T_I("holdover enable: %d", enable);
    ZL_3036X_DATA_LOCK();

    if (zl303xx_AprSetServerHoldover (zl303xx_Params[domain], clock_inst, enable ? ZL303XX_TRUE : ZL303XX_FALSE) != ZL303XX_OK) {
        T_D("Error during Zarlink APR force holdover set");
        rc = VTSS_RC_ERROR;
    }
    ZL_3036X_DATA_UNLOCK();
    return(rc);

}

/****************************************************************************/
/*  Initialization functions                                                */
/****************************************************************************/

/*
 * hwParams_2_domain
 *
 * Find the domain corresponding to a zl303xx_Params pointer (clkGenId).
 * If the domain is found then also check if the corresponding CGU is
 * active for the particular domain. This is needed for domain 0 since
 * that domain has two different CGUs associated (one corresponding to
 * a physical DPLL and another corresponding to a generic DPLL used when
 * adjustment method is LTC).
 *
 */
static mesa_rc hwParams_2_domain(void *clkGenId, u32 *domain, BOOL *active)
{
    mesa_rc rc = MESA_RC_ERROR;

    if (clkGenId == zl303xx_Params_dpll) {
        *domain = 0;
        rc = MESA_RC_OK;
    } else {
        for (u32 n = 0; n < PDV_NO_OF_DOMAINS; n++) {
            if (clkGenId == zl303xx_Params_generic[n]) {
                *domain = n;
                rc = MESA_RC_OK;
                break;
            }
        }
    }

    if (rc == MESA_RC_OK) {
        if (zl303xx_Params[*domain] == clkGenId) {
            *active = TRUE;
        } else {
            *active = FALSE;
        }
    } else {
        T_DG(TRACE_GRP_PTP_INTF, "Invalid CGU pointer: %p", clkGenId);
    }

    return rc;
}

static Sint32T apr_set_time_tsu(void *clkGenId, Uint64S deltaTimeSec, Uint32T deltaTimeNanoSec, zl303xx_BooleanE negative)
{
    zlStatusE status = ZL303XX_OK;
    mesa_timestamp_t t;

    t.nanoseconds = deltaTimeNanoSec;
    t.seconds = deltaTimeSec.lo;
    t.sec_msb = deltaTimeSec.hi;

    u32 domain;
    BOOL active;
    if (hwParams_2_domain(clkGenId, &domain, &active) == MESA_RC_OK) {
        if (active) {
            T_IG(TRACE_GRP_PTP_INTF, "Delta time: %u.%u:%u, negative %d, domain %d", t.sec_msb, t.seconds, t.nanoseconds, negative, domain);
            vtss_local_clock_time_set_delta(&t, domain, negative);
        }
    } else {
        T_WG(TRACE_GRP_PTP_INTF, "clkGenid %p, invalid domain %d", clkGenId, domain);
        status = ZL303XX_INVALID_POINTER;
    }

    return (Sint32T)status;
}

static Sint32T apr_step_time_tsu(void *clkGenId, Sint32T deltaTimeNs)
{
    zlStatusE status = ZL303XX_OK;

    u32 domain;
    BOOL active;
    if (hwParams_2_domain(clkGenId, &domain, &active) == MESA_RC_OK) {
        if (active) {
            T_IG(TRACE_GRP_PTP_INTF, "Delta nanosec: %d, domain %d", deltaTimeNs, domain);
            /* the vtss_local_clock_adj_offset subtracts the time from current time */
            vtss_local_clock_adj_offset(-deltaTimeNs, domain);
        }
    } else {
        T_WG(TRACE_GRP_PTP_INTF, "clkGenid %p, invalid domain %d", clkGenId, domain);
        status = ZL303XX_INVALID_POINTER;
    }

    return (Sint32T)status;
}

static Sint32T apr_adj_time_tsu(void *clkGenId, Sint32T deltaTimeNs, Uint32T maxAdjTime)
{
    zlStatusE status = ZL303XX_OK;

    u32 domain;
    BOOL active;
    i64 delta_time_scaled_ns;

    if (hwParams_2_domain(clkGenId, &domain, &active) == MESA_RC_OK) {
        if (active) {
            T_IG(TRACE_GRP_PTP_INTF, "AdjustTime: %d ns within max %u secs time, domain %d", deltaTimeNs, maxAdjTime, domain);
            if (deltaTimeNs >= -32000 && deltaTimeNs <= 32000) {
                delta_time_scaled_ns = (i64)deltaTimeNs << 16;
                if (zl303xx_AprGet1HzRealignmentType() == ZL303XX_1HZ_REALIGNMENT_PER_PACKET) {
                    delta_time_scaled_ns = delta_time_scaled_ns / 8;                   // FIXME: This is a temporary work-around to get around a bug in the zl30380 software that prevents us
                    T_IG(TRACE_GRP_PTP_INTF, "Realignment pr packet => divide by 8: scaled ns %" PRIi64 "", delta_time_scaled_ns);
                }
                vtss_local_clock_fine_adj_offset(-delta_time_scaled_ns, domain);   //        from setting the stepTimeAdjustTimeThreshold below 500000 ns
            } else {
                if (zl303xx_AprGet1HzRealignmentType() == ZL303XX_1HZ_REALIGNMENT_PER_PACKET) {
                    deltaTimeNs = deltaTimeNs / 8;
                }
                vtss_local_clock_adj_offset(-deltaTimeNs, domain);
            }
        }
    } else {
        T_WG(TRACE_GRP_PTP_INTF, "clkGenid %p, invalid domain %d", clkGenId, domain);
        status = ZL303XX_INVALID_POINTER;
    }

    return (Sint32T)status;
}

static Sint32T apr_adj_freq_tsu(void *clkGenId, Sint32T deltaFreq)
{
    zlStatusE status = ZL303XX_OK;

    u32 domain;
    BOOL active;
    if (hwParams_2_domain(clkGenId, &domain, &active) == MESA_RC_OK) {
        if (active) {
            T_IG(TRACE_GRP_PTP_INTF, "AdjustFreq: %d ppt, domain %d", deltaFreq, domain);
            vtss_local_clock_ratio_set(((i64)deltaFreq << 16) / 1000, domain);
        }
    } else {
        T_WG(TRACE_GRP_PTP_INTF, "clkGenid %p, invalid domain %d", clkGenId, domain);
        status = ZL303XX_INVALID_POINTER;
    }

    return (Sint32T)status;
}

static Sint32T apr_dco_get_freq(void *clkGenId, Sint32T *freqOffsetInPartsPerTrillion)
{
    zlStatusE status = ZL303XX_OK;

    u32 domain;
    BOOL active;
    if (hwParams_2_domain(clkGenId, &domain, &active) == MESA_RC_OK) {
        if (active) {
            *freqOffsetInPartsPerTrillion = (vtss_local_clock_ratio_get(domain) * 1000) / (1 << 16);
            T_NG(TRACE_GRP_PTP_INTF, "clkGenId %p, domain %d", clkGenId, domain);
        }
    } else {
        T_DG(TRACE_GRP_PTP_INTF, "clkGenid %p, invalid domain %d", clkGenId, domain);
        status = ZL303XX_INVALID_POINTER;
    }

    return (Sint32T)status;
}

static Sint32T apr_get_hw_manual_holdover_status(void *clkGenId, Sint32T *holdover_status)
{
    zlStatusE status = ZL303XX_OK;

    u32 domain;
    BOOL active;
    if (hwParams_2_domain(clkGenId, &domain, &active) == MESA_RC_OK) {
        if (active) {
            if (my_clock_option == CLOCK_OPTION_SYNCE_DPLL && domain == 0) {
                vtss_appl_synce_selection_mode_t mode;

                if (clock_selection_mode_get(&mode) == VTSS_RC_OK) {
                    *holdover_status = (Sint32T)(mode == VTSS_APPL_SYNCE_SELECTOR_MODE_FORCED_HOLDOVER);
                } else {
                    *holdover_status = (Sint32T)false;
                }
            } else {
                *holdover_status = (Sint32T)false;
            }
            T_DG(TRACE_GRP_PTP_INTF, "APR made a request for the hw manual holdover status. Value returned was %d", (int)*holdover_status);
        }
    } else {
        T_DG(TRACE_GRP_PTP_INTF, "clkGenid %p, invalid domain %d", clkGenId, domain);
        status = ZL303XX_INVALID_POINTER;
    }

    return (Sint32T)status;
}

static Sint32T apr_get_hw_manual_freerun_status(void *clkGenId, Sint32T *freerun_status)
{
    zlStatusE status = ZL303XX_OK;

    u32 domain;
    BOOL active;
    if (hwParams_2_domain(clkGenId, &domain, &active) == MESA_RC_OK) {
        if (active) {
            if (my_clock_option == CLOCK_OPTION_SYNCE_DPLL && domain == 0) {
                vtss_appl_synce_selection_mode_t mode;

                if (clock_selection_mode_get(&mode) == VTSS_RC_OK) {
                    *freerun_status = (Sint32T)(mode == VTSS_APPL_SYNCE_SELECTOR_MODE_FORCED_FREE_RUN);
                } else {
                    *freerun_status = (Sint32T)false;
                }
            }
            T_DG(TRACE_GRP_PTP_INTF, "APR made a request for the hw manual freerun status. Actual value was %d. Value returned was 0", (int)*freerun_status);
            *freerun_status = (Sint32T)false;
        }
    } else {
        T_DG(TRACE_GRP_PTP_INTF, "clkGenid %p, invalid domain %d", clkGenId, domain);
        status = ZL303XX_INVALID_POINTER;
    }

    return (Sint32T)status;
}

static Sint32T apr_ref_switch_to_packet_ref(void *clkGenId)
{
    zlStatusE status = ZL303XX_OK;

    T_IG(TRACE_GRP_PTP_INTF, "APR requested a switch to packet mode.");
    u32 domain;
    BOOL active;
    if (hwParams_2_domain(clkGenId, &domain, &active) == MESA_RC_OK) {
        if (active) {
            if (my_clock_option == CLOCK_OPTION_SYNCE_DPLL && domain == 0) {
                if (clock_adjtimer_enable(true) != VTSS_RC_OK) {
                    status = ZL303XX_ERROR;
                }
            }
        }
    } else {
        T_DG(TRACE_GRP_PTP_INTF, "clkGenid %p, invalid domain %d", clkGenId, domain);
        status = ZL303XX_INVALID_POINTER;
    }

    return (Sint32T)status;
}

static Sint32T apr_ref_switch_to_electrical_ref(void *clkGenId)
{
    zlStatusE status = ZL303XX_OK;

    T_IG(TRACE_GRP_PTP_INTF, "APR requested a switch to electrical mode.");
    u32 domain;
    BOOL active;
    if (hwParams_2_domain(clkGenId, &domain, &active) == MESA_RC_OK) {
        if (active) {
            if (my_clock_option == CLOCK_OPTION_SYNCE_DPLL && domain == 0) {
                if (clock_adjtimer_enable(false) != VTSS_RC_OK) {
                    status = ZL303XX_ERROR;
                }
            }
        }
    } else {
        T_DG(TRACE_GRP_PTP_INTF, "clkGenid %p, invalid domain %d", clkGenId, domain);
        status = ZL303XX_INVALID_POINTER;
    }

    return (Sint32T)status;
}

Sint32T apr_get_hw_lock_status(void *clkGenId, Sint32T *lock_status)
{
    zlStatusE status = ZL303XX_OK;

    u32 domain;
    BOOL active;
    if (hwParams_2_domain(clkGenId, &domain, &active) == MESA_RC_OK) {
        if (active) {
            if (my_clock_option == CLOCK_OPTION_SYNCE_DPLL && domain == 0) {
                vtss_appl_synce_selector_state_t selector_state;
                uint clock_input;

                if (clock_selector_state_get(&clock_input, &selector_state) == VTSS_RC_OK) {
                    *lock_status = (Sint32T)(selector_state == VTSS_APPL_SYNCE_SELECTOR_STATE_LOCKED);
                } else {
                    *lock_status = (Sint32T)false;
                }
            }
            T_DG(TRACE_GRP_PTP_INTF, "APR made a request for the lock status. Value returned was %d", (int)*lock_status);
        }
    } else {
        T_DG(TRACE_GRP_PTP_INTF, "clkGenid %p, invalid domain %d", clkGenId, domain);
        status = ZL303XX_INVALID_POINTER;
    }

    return (Sint32T)status;
}

Sint32T apr_get_hw_sync_input_en_status(void *clkGenId, Sint32T *status)
{
    *status = (Sint32T)false;
    T_DG(TRACE_GRP_PTP_INTF, "APR made a request for the sync input enable status. Not implemented yet - simply returned FALSE");

    return (Sint32T)ZL303XX_OK;
}

Sint32T apr_get_hw_out_of_range_status(void *clkGenId, Sint32T *status)
{
    *status = (Sint32T)false;
    T_DG(TRACE_GRP_PTP_INTF, "APR made a request for the hw out of range status. Not implemented yet - simply returned FALSE");

    return (Sint32T)ZL303XX_OK;
}


/* apr_stream_create */
/**
   An example of how to start a APR stream/server

*******************************************************************************/
mesa_rc apr_stream_create(u32 domain, Uint16T serverId, u32 filter_type)
{
    zlStatusE status = ZL303XX_OK;

    if ((status = zl303xx_AprAddServerStructInit(&server[serverId])) != ZL303XX_OK)
    {
        T_E("zl303xx_AprAddServerStructInit() failed with status = %u", status);
    }

    server_cfg[serverId].filter_mode = filter_type_2_mode(filter_type);
    T_I("Calling exampleAprSetConfigParameters to put APR into mode %d", (int) server_cfg[serverId].filter_mode);
    if (exampleAprSetConfigParameters(server_cfg[serverId].filter_mode) != ZL303XX_OK) {
        T_W("Error could not update APR parameters.");
    }
    /* Overwrite defaults with values set through example globals. */
    if (status == ZL303XX_OK )
    {
        server[serverId].serverId = serverId;

        server[serverId].algTypeMode = zl303xx_AprGetAlgTypeMode();
        server[serverId].oscillatorFilterType = zl303xx_AprGetOscillatorFilterType();
        server[serverId].osciHoldoverStability = zl303xx_AprGetHoldoverStability();
        server[serverId].sModeTimeout = zl303xx_AprGetSModeTimeout();
        server[serverId].sModeAgeout = zl303xx_AprGetSModeAgeOut();
        server[serverId].bXdslHpFlag = zl303xx_AprGetXdslHpFlag();
        server[serverId].filterType = zl303xx_AprGetFilterType();
        server[serverId].fwdPacketRateType = zl303xx_AprGetPktRate(ZL303XX_TRUE);
        server[serverId].revPacketRateType = zl303xx_AprGetPktRate(ZL303XX_FALSE);
        server[serverId].tsFormat = zl303xx_AprGetTsFormat();
        server[serverId].b32BitTs = zl303xx_AprGet32BitTsFlag();
        server[serverId].rtpClkRateInHz = zl303xx_AprGetRtpClkRate();
        server[serverId].bUseRevPath = zl303xx_AprGetUseReversePath();
        server[serverId].bHybridMode = zl303xx_AprGetHybridServerFlag();
//        server[serverId].packetDiscardDurationInSec = zl303xx_AprGetPacketDiscardDurationInSecFlag();   // FIXME: We need to determine if the value should be configurable or hardcoded as below:
        server[serverId].packetDiscardDurationInSec = 5;                                                  // 5 sec is selected because the PHY's needs 4 sec to become synchronized
        server[serverId].pullInRange = zl303xx_AprGetPullInRange();
        server[serverId].enterHoldoverGST = zl303xx_AprGetEnterHoldoverGST();
        server[serverId].exitVFlagGST = zl303xx_AprGetExitValidGST();
        server[serverId].exitLFlagGST = zl303xx_AprGetExitLockGST();
        server[serverId].lockFlagsMask = zl303xx_AprGetLockMasks();
        server[serverId].thresholdForFlagV = zl303xx_AprGetCustomerThresholdForFlagV();
        server[serverId].exitPAFlagGST = zl303xx_AprGetExitPhaseAlignGST();
        server[serverId].fastLockBW = zl303xx_AprGetFastLockBW();
        server[serverId].fastLockTime =   zl303xx_AprGetFastLockTotalTimeInSecs();
        server[serverId].fastLockWindow = zl303xx_AprGetFastLockPktSelWindowSize();
   }

    T_I("server algorithm %s, oscillator %s, filter %s", apr_alg_type_2_txt(server[serverId].algTypeMode), apr_osc_filter_type_2_txt(server[serverId].oscillatorFilterType), apr_filter_type_2_txt(server[serverId].filterType));
    if ((status == ZL303XX_OK) &&
        (status = zl303xx_AprAddServer(zl303xx_Params[domain], &server[serverId])) != ZL303XX_OK)
    {
        T_E("zl303xx_AprAddServer(%p, %p) failed with status = %u",
            zl303xx_Params[domain], &server[serverId], status);
    }

    T_D("Calling mesa_ts_domain_adjtimer_set to make sure LTC frequency offset is 0");
    vtss_tod_set_adjtimer(0, 0LL);  // Make sure the frequency offset of the LTC is 0 so that it does not cause the phase to drift

    if (status == ZL303XX_OK)
    {
       T_D("APR STREAM created. handle=%u", server[serverId].serverId);
    }

    /* APR-1Hz settings */
    if (status == ZL303XX_OK)
    {
        status = exampleConfigure1HzWithGlobals(zl303xx_Params[domain], server[serverId].serverId);

        if (status != ZL303XX_OK)
        {
            T_D("exampleAprStreamCreate, error config 1Hz, status %d", status);
        }
    }

    if (status == ZL303XX_OK)
    {
       T_D("APR STREAM created. handle=%u", server[serverId].serverId);
    }

    return (status == ZL303XX_OK) ? VTSS_RC_OK : VTSS_RC_ERROR;
}

mesa_rc apr_set_active_timing_server(zl303xx_ParamsS *zl303xx_Params, Uint16T serverId)
{
   zlStatusE status = ZL303XX_OK;

   if ((status = zl303xx_AprSetActiveRef(zl303xx_Params, serverId)) != ZL303XX_OK)
   {
       T_E("zl303xx_AprSetActiveRef(%p, %d) failed with status = %u", zl303xx_Params, serverId, status);
   }

   return (status == ZL303XX_OK) ? VTSS_RC_OK : VTSS_RC_ERROR;
}

/* apr_stream_remove */
/**
   Remove a APR stream/server

*******************************************************************************/
mesa_rc apr_stream_remove(u32 domain, Uint16T serverId)
{
   zlStatusE status;
   zl303xx_AprRemoveServerS aprServerInfo;

   status = zl303xx_AprRemoveServerStructInit(&aprServerInfo);

   if (status != ZL303XX_OK) {
      T_E("Call to zl303xx_AprRemoveServerStructInit() failure = %d\n", status);
   }

   if (status == ZL303XX_OK) {
      aprServerInfo.serverId = serverId;
      status = zl303xx_AprRemoveServer(zl303xx_Params[domain], &aprServerInfo);

      if(status != ZL303XX_OK) {
         T_E("Call to zl303xx_AprRemoveServer() failure = %d\n", status);
      }
   }

   /* Wait 1 second after removing stream to make sure shutdown has been completed before we continue with operations like removing the CGU etc. */
   //sleep(1);


   return (status == ZL303XX_OK) ? VTSS_RC_OK : VTSS_RC_ERROR;
}

extern Uint32T defaultAdjSize1HzPSL[ZL303XX_MAX_NUM_PSL_LIMITS];
extern Uint32T defaultPSL_1Hz[ZL303XX_MAX_NUM_PSL_LIMITS];

#ifdef __cplusplus
extern "C" {
#endif
Sint32T zl303xx_Dpll34xMsgRouter(void *hwParams, void *inData, void *outData);
Sint32T zl303xx_Dpll36xMsgRouter(void *hwParams, void *inData, void *outData);
#ifdef __cplusplus
}
#endif

Sint32T zl303xx_DpllDummyMsgRouter(void *hwParams, void *inData, void *outData)
{
    zlStatusE status = ZL303XX_OK;
    zl303xx_DriverMsgInDataS *in;
    zl303xx_DriverMsgOutDataS *out;


    if (status == ZL303XX_OK)
    {
        status = ZL303XX_CHECK_POINTER(hwParams);
    }
    if (status == ZL303XX_OK)
    {
        status = ZL303XX_CHECK_POINTER(inData);
    }
    if (status == ZL303XX_OK)
    {
        status = ZL303XX_CHECK_POINTER(outData);
    }

    if (status == ZL303XX_OK)
    {
        in  = (zl303xx_DriverMsgInDataS *)inData;
        out = (zl303xx_DriverMsgOutDataS *)outData;
    }
    if (status == ZL303XX_OK)
    {

        status = ZL303XX_UNSUPPORTED_OPERATION;

        switch( in->dpllMsgType )
        {
            case ZL303XX_DPLL_DRIVER_MSG_GET_DEVICE_INFO:

                status = ZL303XX_OK;
                out->d.getDeviceInfo.devType = CUSTOM_DEVICETYPE;
                out->d.getDeviceInfo.devId = (zl303xx_DeviceIdE)0;
                out->d.getDeviceInfo.devRev = 0;
                break;

            default:
                break;

        }
    }
    return ZL303XX_OK;
}

static zlStatusE apr_clock_create(zl303xx_ParamsS *zl303xx_Params, vtss_zl_30380_dpll_type_t dpll_type)
{
   zlStatusE status = ZL303XX_OK;
   zl303xx_AprAddDeviceS device;
   int j;

    T_I("device instance pointer %p", zl303xx_Params);

   /* Initialize addDevice struct */
   memset(&device, 0, sizeof(zl303xx_AprAddDeviceS));
   if ((status = zl303xx_AprAddDeviceStructInit(zl303xx_Params, &device)) != ZL303XX_OK)
   {
       T_E("zl303xx_AprAddDeviceStructInit() failed with status = %u", status);
   }
   /* Overwrite defaults with values set through example globals. */
   if (status == ZL303XX_OK)
   {
       (void)zl303xx_AprSetDeviceOptMode(ZL303XX_ELECTRIC_MODE);                                // start it in the default mode (Electrical mode).
       device.devMode = zl303xx_AprGetDeviceOptMode(); /* PKT, ELEC, HYB */
       device.hwDcoResolutionInPpt = zl303xx_AprGetHwDcoResolution();
       device.enterPhaseLockStatusThreshold = zl303xx_AprGetEnterPhaseLockStatusThreshold();
       device.exitPhaseLockStatusThreshold = zl303xx_AprGetExitPhaseLockStatusThreshold();
       device.bWarmStart = zl303xx_AprGetWarmStart();
       device.warmStartInitialFreqOffset = zl303xx_AprGetWsInitialOffset();
       device.clkStabDelayLimit = zl303xx_AprGetClkStabDelayIterations();

       device.cguNotify = apr_cgu_notify;  /* Hook the default notify handlers */
       device.elecNotify = exampleAprElecNotify;
       device.serverNotify = apr_server_notify;
       device.oneHzNotify = exampleAprOneHzNotify;

       device.setTime = apr_set_time_tsu;
       device.stepTime = apr_step_time_tsu;
       // FIXME: The two lines just below do not seem to have any effect in the 30380. We may have to patch the message router in the individual drivers.
       device.defaultCGU[ZL303XX_ADCGU_SET_TIME] = ZL303XX_FALSE;   // Disable built-in function for set_time (this is needed when DPLL type is 30343 or 30363)
       if (0) {
       }
#ifdef VTSS_SW_OPTION_ZLS30341
       else if (dpll_type == VTSS_ZL_30380_DPLL_ZLS3034X) {
           T_D("30343 TYPE DPLL");
           device.driverMsgRouter = zl303xx_Dpll34xMsgRouter;
           (void)zl303xx_SetDefaultDeviceType(ZL3034X_DEVICETYPE);
       }
#endif
#ifdef VTSS_SW_OPTION_ZLS30361
       else if (dpll_type == VTSS_ZL_30380_DPLL_ZLS3036X) {
           T_D("30363 TYPE DPLL");
           device.driverMsgRouter = zl303xx_Dpll36xMsgRouter;
           (void)zl303xx_SetDefaultDeviceType(ZL3036X_DEVICETYPE);
       }
#endif
       else {
           T_D("GENERIC TYPE DPLL");
           device.driverMsgRouter = zl303xx_DpllDummyMsgRouter;
           device.defaultCGU[ZL303XX_ADCGU_STEP_TIME] = ZL303XX_FALSE;  // Disable built-in function for step_time (this is needed when DPLL type is 30343 or 30363)
           (void)zl303xx_SetDefaultDeviceType(CUSTOM_DEVICETYPE);
           device.adjustTime = apr_adj_time_tsu;
           device.adjustFreq = apr_adj_freq_tsu;
           device.getAprServerFreqOffset = apr_dco_get_freq;
           device.getHwManualHoldoverStatus = apr_get_hw_manual_holdover_status;
           device.getHwManualFreerunStatus = apr_get_hw_manual_freerun_status;
           device.refSwitchToPacketRef = apr_ref_switch_to_packet_ref;
           device.refSwitchToElectricalRef = apr_ref_switch_to_electrical_ref;
           device.getHwLockStatus = apr_get_hw_lock_status;
           device.getHwSyncInputEnStatus = apr_get_hw_sync_input_en_status;
           device.getHwOutOfRangeStatus = apr_get_hw_out_of_range_status;
           ZL_30380_CHECK(zl303xx_SetUseAdjustTimeHybrid(ZL303XX_TRUE));  // Only enable adjust time in hybrid mode when running on Generic type DPLL e.g. ServalT
       }

//       device.adjModifier = exampleAdjModifier;
//       device.jumpTimeTSU = exampleJumpTimeTSU;
//       device.jumpNotification = exampleJumpNotification;
//       device.sendRedundancyData = exampleSendRedundancyData;
//       device.jumpActiveCGU = exampleJumpActiveCGU;
//       device.jumpStandbyCGU = exampleJumpStandbyCGU;
//       device.setTimePacketTreatment = zl303xx_AprGetSetTimePacketTreatment();
//       device.stepTimePacketTreatment = zl303xx_AprGetStepTimePacketTreatment();
//       device.adjustTimePacketTreatment = zl303xx_AprGetAdjustTimePacketTreatment();
//       device.legacyTreatment = zl303xx_AprGetLegacyTreatment();

       device.PFConfig.setTimeResolution = 1;
       device.PFConfig.stepTimeResolution = 1;
       device.PFConfig.stepTimeAdjustTimeThreshold = 2000;
       device.PFConfig.APRFrequencyLockedPhaseSlopeLimit = zl303xx_AprGetAPRFrequencyLockedPhaseSlopeLimit();
       device.PFConfig.APRFrequencyNotLockedPhaseSlopeLimit = zl303xx_AprGetAPRFrequencyNotLockedPhaseSlopeLimit();
       device.PFConfig.APRFrequencyFastPhaseSlopeLimit = zl303xx_AprGetAPRFrequencyFastPhaseSlopeLimit();

       for(j = 0; j < ZL303XX_MAX_NUM_PSL_LIMITS; j++)
       {
          zl303xx_AprGetPSL(j, &(device.PFConfig.adjSize1HzPSL[j]), &(device.PFConfig.PSL_1Hz[j]) );
       }

       device.PFConfig.lockInThreshold = zl303xx_AprGetPFLockInThreshold();
       device.PFConfig.lockInCount = zl303xx_AprGetPFLockInCount();
       device.PFConfig.lockOutThreshold = zl303xx_AprGetPFLockOutThreshold();
       device.PFConfig.adjustFreqMinPhase = 0;
       device.PFConfig.adjustTimeMinThreshold = 0;
       device.PFConfig.bUseAdjustTimeHybrid = zl303xx_GetUseAdjustTimeHybrid();
       device.PFConfig.bUseAdjustTimePacket = zl303xx_GetUseAdjustTimePacket();
       device.PFConfig.stepTimeDetectableThr = zl303xx_GetStepTimeDetectableThr();

//       device.PFConfig.AprTIEWriteEnabled = zl303xx_GetAprTIEWriteEnabled();
//       device.PFConfig.AprTIEWriteThr = zl303xx_GetAprTIEWriteThr();

       device.useLegacyStreamStartUp = zl303xx_AprGetUseLegacyStreamStartUp();
       device.make1HzAdjustmentsDuringHoldover = zl303xx_AprGetAllow1HzAdjustmentsInHoldover();

       device.PFConfig.stepTimeExecutionTime = 4000;
       device.PFConfig.adjustTimeExecutionTime = 1000;
   }

   /* Register a hardware device with APR. */
   if ((status == ZL303XX_OK) && (status = zl303xx_AprAddDevice(zl303xx_Params, &device)) != ZL303XX_OK)
   {
      T_E("zl303xx_AprAddDevice() failed with status = %u", status);
   }

   if (status == ZL303XX_OK)
   {
       T_D("APR Device added. cguId=%p", zl303xx_Params);
   }
   return status;
}

static zlStatusE user_delay_func(Uint32T requiredDelayMs, Uint64S startOfRun, Uint64S endofRun)
{
    zlStatusE status = ZL303XX_OK;

    if (OS_TASK_DELAY(requiredDelayMs) != OS_OK)               /* New delay ms to compensate */
        status = ZL303XX_ERROR;
    T_D("status = %u", status );
    return status;
}

/* apr_env_init */
/**
   Initializes the APR environment and the global variables used in vitesse
   PTP code.

*******************************************************************************/
static zlStatusE apr_env_init(void)
{
   zlStatusE status = ZL303XX_OK;

   static zl303xx_AprInitS aprInit;

   if ((status == ZL303XX_OK) &&
       ((status = zl303xx_AprInitStructInit(&aprInit)) != ZL303XX_OK))
   {
       T_E("zl303xx_AprInitStructInit() failed with status = %u", status );
   }

   T_D("zl303xx_AprInitStructInit() succeded with status = %u", status );
   if (status == ZL303XX_OK)
   {
      /* Change any APR init defaults here: */
      /*
       * ZL303XX_APR_MAX_NUM_DEVICES = 1                         - Maximum number of clock generation device in APR;
       * ZL303XX_APR_MAX_NUM_MASTERS = 8                         - Maximum number of packet/hybrid server clock for each clock device;
       * aprInit.logLevel = 0                                  - APR log level */

       aprInit.PFInitParams.useHardwareTimer = ZL303XX_TRUE; /* - Use PSLFCL and Sample delay binding (see aprAddDevice) */
       aprInit.PFInitParams.userDelayFunc = (swFuncPtrUserDelay)user_delay_func; /* - Hook PSLFCL delay binding to handler */
       aprInit.AprSampleInitParams.userDelayFunc = (swFuncPtrUserDelay)user_delay_func; /* - Hook Sample delay binding to handler */
       aprInit.logLevel = 0;  /* least detailed log level, can be set from CLI */
       aprInit.PFInitParams.logLevel = 0;  /* least detailed log level, can be set from CLI */
   }

   if ((status == ZL303XX_OK) &&                                     // This code has been inserted to resolve problems
       ((status = zl303xx_SetAprQIFQLength(128)) !=  ZL303XX_OK))    // with "ANMS: error sending=-1" messages on the console.
   {                                                                 //
       T_W("Failed execute zl303xx_SetAprQIFQLength(128)");          //
   }                                                                 //

   if ((status == ZL303XX_OK) &&
       (status = zl303xx_AprInit(&aprInit)) != ZL303XX_OK)
   {
       T_E("zl303xx_AprInit() failed with status = %u", status );
   }

   return status;
}

void apr_show_server_1hz_config(zl303xx_ParamsS *zl303xx_Params, Uint16T serverId)
{
    Uint16T server_id;
    zl303xx_AprConfigure1HzS fwd_1hz;
    zl303xx_AprConfigure1HzS rev_1hz;

    ZL_30380_CHECK(zl303xx_AprGetCurrent1HzConfigData(zl303xx_Params, &server_id, &fwd_1hz, &rev_1hz));
    T_N("1Hz configuration, serverId %d", server_id);
    T_N("1Hz fwd configuration, dis %d, realign %d, interval %d", fwd_1hz.disabled, fwd_1hz.realignmentType, fwd_1hz.realignmentInterval);
    T_N("1Hz rev configuration, dis %d, realign %d, interval %d", rev_1hz.disabled, rev_1hz.realignmentType, rev_1hz.realignmentInterval);
}

static exampleAprConfigIdentifiersE filter_type_2_mode(u32 filter_type)
{
    switch(filter_type) {
        case PTP_FILTERTYPE_ACI_DEFAULT:                        return ACI_DEFAULT;
        case PTP_FILTERTYPE_ACI_FREQ_XO:                        return ACI_FREQ_XO;
        case PTP_FILTERTYPE_ACI_PHASE_XO:                       return ACI_PHASE_XO;
        case PTP_FILTERTYPE_ACI_FREQ_TCXO:                      return ACI_FREQ_TCXO;
        case PTP_FILTERTYPE_ACI_PHASE_TCXO:                     return ACI_PHASE_TCXO;
        case PTP_FILTERTYPE_ACI_FREQ_OCXO_S3E:                  return ACI_FREQ_OCXO_S3E;
        case PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E:                 return ACI_PHASE_OCXO_S3E;
        case PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_FREQ:        return ACI_BC_PARTIAL_ON_PATH_FREQ;
        case PTP_FILTERTYPE_ACI_BC_PARTIAL_ON_PATH_PHASE:       return ACI_BC_PARTIAL_ON_PATH_PHASE;
        case PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_FREQ:           return ACI_BC_FULL_ON_PATH_FREQ;
        case PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE:          return ACI_BC_FULL_ON_PATH_PHASE;
        case PTP_FILTERTYPE_ACI_FREQ_ACCURACY_FDD:              return ACI_FREQ_ACCURACY_FDD;
        case PTP_FILTERTYPE_ACI_FREQ_ACCURACY_XDSL:             return ACI_FREQ_ACCURACY_XDSL;
        case PTP_FILTERTYPE_ACI_ELEC_FREQ:                      return ACI_ELEC_FREQ;
        case PTP_FILTERTYPE_ACI_ELEC_PHASE:                     return ACI_ELEC_PHASE;
        case PTP_FILTERTYPE_ACI_PHASE_TCXO_Q:                   return ACI_PHASE_TCXO_Q;
        case PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_Q:        return ACI_BC_FULL_ON_PATH_PHASE_Q;
        case PTP_FILTERTYPE_ACI_PHASE_RELAXED_C60W:             return ACI_PHASE_RELAXED_C60W;
        case PTP_FILTERTYPE_ACI_PHASE_RELAXED_C150:             return ACI_PHASE_RELAXED_C150;
        case PTP_FILTERTYPE_ACI_PHASE_RELAXED_C180:             return ACI_PHASE_RELAXED_C180;
        case PTP_FILTERTYPE_ACI_PHASE_RELAXED_C240:             return ACI_PHASE_RELAXED_C240;
        case PTP_FILTERTYPE_ACI_BC_FULL_ON_PATH_PHASE_BETA:     return ACI_BC_FULL_ON_PATH_PHASE_BETA;
        case PTP_FILTERTYPE_ACI_PHASE_OCXO_S3E_R4_6_1:          return ACI_PHASE_OCXO_S3E_R4_6_1;
        case PTP_FILTERTYPE_ACI_BASIC_PHASE:                    return ACI_BASIC_PHASE;
        case PTP_FILTERTYPE_ACI_BASIC_PHASE_LOW:                return ACI_BASIC_PHASE_LOW;
        default:                                                return ACI_DEFAULT;
    }
}

static mesa_rc zl_3038x_pdv_create(zl303xx_ParamsS *zl303xx_Params, vtss_zl_30380_dpll_type_t dpll_type)
{
    zlStatusE zle;
    /* create the PDV instance */
    T_D("create the MS-PDV instance");


    ZL_30380_CHECK(zl303xx_SetAprLogLevel(0));
    /* Create a CGU */
    ZL_30380_CHECK(apr_clock_create(zl303xx_Params, dpll_type));

    if ((zle = zl303xx_AprSetHoldover(zl303xx_Params, config_data.bHoldover)) != ZL303XX_OK) {
        T_D("Error %d during Zarlink APR force holdover set", zle);
    }

    return VTSS_RC_OK;
}

mesa_rc zl_3038x_servo_dpll_config(int clock_option, bool generic)
{
    // if generic, domain 0 uses zl303xx_Params_generic[0] otherwise is uses zl303xx_Params_dpll which is configured for the actual DPLL
    // this function shall only be called if there are no active PTP instances using domain 0.
    my_generic = generic;
    my_clock_option = clock_option;
    if (generic) {
        zl303xx_Params[0] = zl303xx_Params_generic[0];
        (void)zl303xx_SetDefaultDeviceType(alternative_dpll_type);
        T_IG(TRACE_GRP_PTP_INTF, "Generic DPLL %p selected", zl303xx_Params[0]);
    } else {
        zl303xx_Params[0] = zl303xx_Params_dpll;
        (void)zl303xx_SetDefaultDeviceType(default_dpll_type);
        T_IG(TRACE_GRP_PTP_INTF, "Zl303xx DPLL %p selected", zl303xx_Params[0]);
    }
    return VTSS_RC_OK;
}

bool zl_3038x_servo_dpll_config_generic(void)
{
    return (my_generic);
}

void zl_3038x_api_version_check()
{
    if (strcmp(zl303xx_ApiReleaseVersion, zl303xx_AprReleaseVersion) != 0) {
        T_E("Expected zl303xx_ApiReleaseVersion = %s", (const char*)zl303xx_ApiReleaseVersion );
        T_E("Actual zl303xx_AprReleaseVersion = %s", (const char*)zl303xx_AprReleaseVersion );
        VTSS_ASSERT(zl303xx_ApiReleaseVersion == zl303xx_AprReleaseVersion);
    }
}

vtss_zarlink_servo_t zl_3038x_module_type()
{
    if (strcmp(zl303xx_AprReleaseSwId, "ZLS30387") == 0) {
        return VTSS_ZARLINK_SERVO_ZLS30387;
    }
    if (strcmp(zl303xx_AprReleaseSwId, "ZLS30384") == 0) {
        return VTSS_ZARLINK_SERVO_ZLS30384;
    }
    if (strcmp(zl303xx_AprReleaseSwId, "ZLS30380") == 0) {
        return VTSS_ZARLINK_SERVO_ZLS30380;
    }
    return VTSS_ZARLINK_SERVO_NONE;
}

mesa_rc zl_3038x_pdv_init(vtss_init_data_t *data)
{
    vtss_isid_t isid = data->isid;

    switch (data->cmd) {
    case INIT_CMD_EARLY_INIT:
        /* Initialize and register trace ressources */
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);
        break;
    case INIT_CMD_INIT:
        critd_init(&zl30380_global.datamutex, "zl_30380_data", VTSS_MODULE_ID_ZL_3034X_PDV, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
        ZL_3036X_DATA_UNLOCK();
        T_D("INIT_CMD_INIT ZLS30380" );
        break;
    case INIT_CMD_START:
        T_D("INIT_CMD_START ZLS30380");
        T_D("Expected zl303xx_ApiReleaseSwId = %s", (const char*)zl303xx_ApiReleaseSwId);
        T_D("Actual zl303xx_AprReleaseSwId = %s", (const char*)zl303xx_AprReleaseSwId);
        T_D("Expected zl303xx_ApiReleaseVersion = %s", (const char*)zl303xx_ApiReleaseVersion );
        T_D("Actual zl303xx_AprReleaseVersion = %s", (const char*)zl303xx_AprReleaseVersion );
        break;
    case INIT_CMD_CONF_DEF:
        zl30380_conf_read(TRUE);
        T_D("INIT_CMD_CONF_DEF ZLS30380" );
        break;
    case INIT_CMD_MASTER_UP:
        zl30380_conf_read(FALSE);
        /* Setup logging */
        ZL_30380_CHECK(zl303xx_SetupLogToMsgQ(stdout));
        zl303xx_TraceSetLevelAll(0);
        //ZL_30380_CHECK(zl303xx_SetAprLogLevel(0));

        /* Launch APR application */
        ZL_30380_CHECK(apr_env_init());
        T_I("apr_env_init finished");
        // create a zl_3038x pdv (device) instance
        vtss_zl_30380_dpll_type_t dpll_type;
        if (clock_dpll_type_get(&dpll_type) != VTSS_RC_OK) {
            T_I("Could not get DPLL type - assuming GENERIC");
            dpll_type = VTSS_ZL_30380_DPLL_GENERIC;
        }
        zl_3038x_pdv_create(zl303xx_Params_dpll, dpll_type);
        default_dpll_type = zl303xx_GetDefaultDeviceType();
        zl303xx_Params[0] = zl303xx_Params_dpll;
        zl_3038x_pdv_create(zl303xx_Params_generic[0], VTSS_ZL_30380_DPLL_GENERIC);
        zl_3038x_pdv_create(zl303xx_Params_generic[1], VTSS_ZL_30380_DPLL_GENERIC);
        zl303xx_Params[1] = zl303xx_Params_generic[1];
        zl_3038x_pdv_create(zl303xx_Params_generic[2], VTSS_ZL_30380_DPLL_GENERIC);
        zl303xx_Params[2] = zl303xx_Params_generic[2];
        //zl_3038x_pdv_create(zl303xx_Params_generic[3], VTSS_ZL_30380_DPLL_GENERIC);
        zl303xx_Params_generic[3] = 0;
        zl303xx_Params[3] = zl303xx_Params_generic[3];
        alternative_dpll_type = zl303xx_GetDefaultDeviceType();
        //T_WG(TRACE_GRP_PTP_INTF, "zl303xx_Params_dpll = %p", zl303xx_Params_dpll);
        //int idx;
        //for (idx = 0; idx < PDV_NO_OF_DOMAINS; idx++) {
        //    T_WG(TRACE_GRP_PTP_INTF, "zl303xx_Params_generic[%d] = %p", idx, zl303xx_Params_generic[idx]);
        //}
        T_I("pdv_create, dpll_type = %d finished", dpll_type);

        T_D("INIT_CMD_MASTER_UP ");
        break;
    case INIT_CMD_SWITCH_ADD:
        T_D("INIT_CMD_SWITCH_ADD");
        break;
    case INIT_CMD_MASTER_DOWN:
        T_D("INIT_CMD_MASTER_DOWN - ISID %u", isid);
        break;
    default:
        break;
    }

    return 0;
}

mesa_rc zl_30380_apr_set_server_mode(zl303xx_ParamsS *zl303xx_Params, int mode, u32 serverId)
{
    if (mode == 1) {  // FIXME: For now only 1 == packet is defined
        if (zl303xx_AprSetServerMode(zl303xx_Params, serverId, ZL303XX_PACKET_MODE) == ZL303XX_OK) return VTSS_RC_OK;
    }
    return VTSS_RC_ERROR;
}

mesa_rc zl_30380_apr_switch_to_packet_mode(u32 domain, int clock_inst)
{
    bool changed_mode = false;
    T_IG(TRACE_GRP_PTP_INTF,"domain %u, clock_inst %d", domain, clock_inst);
    // clear transient mode before switching to packet mode
    zl_30380_apr_set_hybrid_transient(domain, clock_inst, VTSS_PTP_HYBRID_TRANSIENT_NOT_ACTIVE);


    if (server_cfg[clock_inst].filter_mode == ACI_BC_PARTIAL_ON_PATH_PHASE_SYNCE) {
        server_cfg[clock_inst].filter_mode = ACI_BC_PARTIAL_ON_PATH_PHASE;
        changed_mode = true;
    }
    if (server_cfg[clock_inst].filter_mode == ACI_BC_FULL_ON_PATH_PHASE_SYNCE) {
        server_cfg[clock_inst].filter_mode = ACI_BC_FULL_ON_PATH_PHASE;
        changed_mode = true;
    }
    if (server_cfg[clock_inst].filter_mode == ACI_BASIC_PHASE_SYNCE) {
        server_cfg[clock_inst].filter_mode = ACI_BASIC_PHASE;
        changed_mode = true;
    }
    if (server_cfg[clock_inst].filter_mode == ACI_BASIC_PHASE_LOW_SYNCE) {
        server_cfg[clock_inst].filter_mode = ACI_BASIC_PHASE_LOW;
        changed_mode = true;
    }
    if (changed_mode) {
        if (exampleAprModeSwitching(zl303xx_Params[domain], clock_inst, server_cfg[clock_inst].filter_mode) == ZL303XX_OK) {
            T_IG(TRACE_GRP_PTP_INTF,"domain %u, clock_inst %d, mode switching to %d", domain, clock_inst, server_cfg[clock_inst].filter_mode);
            if (exampleConfigure1HzWithGlobals(zl303xx_Params[domain], server[clock_inst].serverId) != ZL303XX_OK)
            {
                T_D("error config 1Hz");
            }
            return VTSS_RC_OK;
        }
    } else {
        if (zl303xx_AprSetServerMode(zl303xx_Params[domain], clock_inst, ZL303XX_PACKET_MODE) == ZL303XX_OK) {
            T_IG(TRACE_GRP_PTP_INTF,"domain %u, clock_inst %d, server mode ZL303XX_PACKET_MODE", domain, clock_inst);
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

mesa_rc zl_30380_apr_switch_to_hybrid_mode(u32 domain, int clock_inst)
{
    bool changed_mode = false;
    T_IG(TRACE_GRP_PTP_INTF,"domain %u, clock_inst %d", domain, clock_inst);

    if (server_cfg[clock_inst].filter_mode == ACI_BC_PARTIAL_ON_PATH_PHASE) {
        server_cfg[clock_inst].filter_mode = ACI_BC_PARTIAL_ON_PATH_PHASE_SYNCE;
        changed_mode = true;
    }
    if (server_cfg[clock_inst].filter_mode == ACI_BC_FULL_ON_PATH_PHASE) {
        server_cfg[clock_inst].filter_mode = ACI_BC_FULL_ON_PATH_PHASE_SYNCE;
        changed_mode = true;
    }
    if (server_cfg[clock_inst].filter_mode == ACI_BASIC_PHASE) {
        server_cfg[clock_inst].filter_mode = ACI_BASIC_PHASE_SYNCE;
        changed_mode = true;
    }
    if (server_cfg[clock_inst].filter_mode == ACI_BASIC_PHASE_LOW) {
        server_cfg[clock_inst].filter_mode = ACI_BASIC_PHASE_LOW_SYNCE;
        changed_mode = true;
    }
    if (changed_mode) {
        if (exampleAprModeSwitching(zl303xx_Params[domain], clock_inst, server_cfg[clock_inst].filter_mode) == ZL303XX_OK) {
            zl_30380_apr_set_hybrid_transient(domain, clock_inst, curTransient);
            T_IG(TRACE_GRP_PTP_INTF,"domain %u, clock_inst %d, mode switching to %d", domain, clock_inst, server_cfg[clock_inst].filter_mode);
            if (exampleConfigure1HzWithGlobals(zl303xx_Params[domain], server[clock_inst].serverId) != ZL303XX_OK)
            {
                T_D("error config 1Hz");
            }
            return VTSS_RC_OK;
        }
    } else {
        if (zl303xx_AprSetServerMode(zl303xx_Params[domain], clock_inst, ZL303XX_HYBRID_MODE) == ZL303XX_OK) {
            T_IG(TRACE_GRP_PTP_INTF,"domain %u, clock_inst %d, server mode ZL303XX_HYBRID_MODE", domain, clock_inst);
            return VTSS_RC_OK;
        }
    }
    return VTSS_RC_ERROR;
}

mesa_rc zl_30380_apr_in_hybrid_mode(u32 domain, int clock_inst, BOOL *state)
{
    zl303xx_BooleanE hybridMode;

    if (zl303xx_AprIsServerInHybridMode(zl303xx_Params[domain], clock_inst, &hybridMode) == ZL303XX_OK) {
        T_IG(TRACE_GRP_PTP_INTF,"domain %u, clock_inst %d, state %d", domain, clock_inst, hybridMode);
        if (hybridMode == ZL303XX_TRUE)
            *state = TRUE;
        else
            *state = FALSE;

        return VTSS_RC_OK;
    }
    return VTSS_RC_ERROR;
}

mesa_rc zl_30380_apr_set_active_ref(u32 domain, int stream)
{
    zlStatusE status;

    T_IG(TRACE_GRP_PTP_INTF, "Params %p, stream %d", zl303xx_Params[domain], stream);
//    if ((status = zl303xx_AprGetDeviceCurrActiveRef(zl303xx_Params, &serverId)) == ZL303XX_OK) {
//        if (serverId != stream) {
            if ((status = zl303xx_AprSetActiveRef(zl303xx_Params[domain], stream)) == ZL303XX_OK)
                return VTSS_RC_OK;
            else {
                T_WG(TRACE_GRP_PTP_INTF, "Could not set active reference. Status was %d", status);
                return VTSS_RC_ERROR;
            }
//        }
//        return VTSS_RC_OK;
//    } else {
//        T_WG(TRACE_GRP_PTP_INTF, "Could not get current active ref. Status was %d", (int) status);
//    }
//    return VTSS_RC_ERROR;
}

mesa_rc zl30380_apr_set_active_elec_ref(u32 domain, int input)
{
    zlStatusE status;

    T_IG(TRACE_GRP_PTP_INTF,"domain %u, input %d", domain, input);
//    if ((status = zl303xx_AprGetDeviceCurrActiveRef(zl303xx_Params, &serverId)) == ZL303XX_OK) {
//        if (serverId != 0xFFFF) {
            if ((status = zl303xx_AprSetActiveElecRef(zl303xx_Params[domain], input)) == ZL303XX_OK)
                return VTSS_RC_OK;
            else {
                T_WG(TRACE_GRP_PTP_INTF, "Could not set active electrical ref. mode. Status was %d", status);
                return VTSS_RC_ERROR;
            }
//        }
//        return VTSS_RC_OK;
//    } else {
//        T_WG(TRACE_GRP_PTP_INTF, "Could not get current active ref. Status was %d", (int) status);
//    }
//    return VTSS_RC_ERROR;
}

mesa_rc zl_30380_apr_set_hybrid_transient(u32 domain, int clock_inst, vtss_ptp_hybrid_transient transient)
{
    mesa_rc rc = VTSS_RC_OK;
    BOOL is_in_hybrid_mode;
    i64 adj;
    zl303xx_AprConfigure1HzS par;
   
    T_IG(TRACE_GRP_PTP_INTF, "curTransient %d, transient %d.", curTransient, transient);
    //if (curTransient != VTSS_PTP_HYBRID_TRANSIENT_NOT_ACTIVE && transient != VTSS_PTP_HYBRID_TRANSIENT_NOT_ACTIVE && transient != curTransient) {
    //    T_W("Cannot go directly from one type of transient to another.");
    //    rc = VTSS_RC_ERROR;
    //}
    if (rc == VTSS_RC_OK) {
        rc = zl_30380_apr_in_hybrid_mode(domain, clock_inst, &is_in_hybrid_mode);
        if (rc == VTSS_RC_OK) {
            if (CUSTOM_DEVICETYPE == zl303xx_GetDefaultDeviceType()) {
                if (is_in_hybrid_mode) {
                    ZL_30380_CHECK(zl303xx_AprConfigure1HzStructInit(zl303xx_Params[domain], ZL303XX_TRUE, &par));
                    /* Servo does not support transient mitigation for generic DPLL's (i.e. non zls303x3 dplls) */
                    switch(transient) {
                        case VTSS_PTP_HYBRID_TRANSIENT_QUICK: 
                            // get current holdover value from synce DPLL
                            rc = clock_ho_frequency_offset_get(&adj);
                            
                            // set offset in the PTP dpll
                            rc = clock_output_adjtimer_set(adj);
                            // switch PTP to holdover
                            rc = clock_ptp_timer_source_set(PTP_CLOCK_SOURCE_INDEP);
                            par.disabled = ZL303XX_FALSE;
                            par.realignmentType = ZL303XX_1HZ_REALIGNMENT_PERIODIC;
                            par.realignmentInterval = 1;
                            //ZL_30380_CHECK(zl303xx_AprConfigServer1Hz(zl303xx_Params[domain], clock_inst, ZL303XX_TRUE, &par));
                            //ZL_30380_CHECK(zl303xx_AprConfigServer1Hz(zl303xx_Params[domain], clock_inst, ZL303XX_FALSE, &par));
                            T_WG(TRACE_GRP_PTP_INTF, "set transient state, offset %d ppb", (i32)(adj>>16));
                            break;
                        case VTSS_PTP_HYBRID_TRANSIENT_OPTIONAL:
                            T_WG(TRACE_GRP_PTP_INTF, "HYBRID_TRANSIENT_OPTIONAL not supported");
                            rc = VTSS_RC_ERROR;
                            break;
                        case VTSS_PTP_HYBRID_TRANSIENT_NOT_ACTIVE:
                            par.disabled = ZL303XX_FALSE;
                            par.realignmentType = ZL303XX_1HZ_REALIGNMENT_PERIODIC;
                            par.realignmentInterval = 1;
                            //ZL_30380_CHECK(zl303xx_AprConfigServer1Hz(zl303xx_Params[domain], clock_inst, ZL303XX_TRUE, &par));
                            //ZL_30380_CHECK(zl303xx_AprConfigServer1Hz(zl303xx_Params[domain], clock_inst, ZL303XX_FALSE, &par));
                            // switch back to Synce
                            rc = clock_ptp_timer_source_set(PTP_CLOCK_SOURCE_SYNCE);
                            // set offset in the PTP dpll
                            rc = clock_output_adjtimer_set(0LL);
                            T_WG(TRACE_GRP_PTP_INTF, "clear transient state");
                            break;
                        default: rc = VTSS_RC_ERROR;
                    }
                } else {
                    T_WG(TRACE_GRP_PTP_INTF, "HYBRID_TRANSIENT detected but servo is not in hybrid mode");
                }
            } else {
                zlStatusE status = ZL303XX_OK;

                switch(transient) {
                    case VTSS_PTP_HYBRID_TRANSIENT_QUICK: status = zl303xx_AprSetHybridTransient(zl303xx_Params[domain], ZL303XX_BHTT_QUICK);
                                                          T_IG(TRACE_GRP_PTP_INTF, "Call to zl303xx_AprSetHybridTransient to set transient state to ZL303XX_BHTT_QUICK returned %d", status);
                                                          break;
                    case VTSS_PTP_HYBRID_TRANSIENT_OPTIONAL: status = zl303xx_AprSetHybridTransient(zl303xx_Params[domain], ZL303XX_BHTT_OPTIONAL);
                                                             T_IG(TRACE_GRP_PTP_INTF, "Call to zl303xx_AprSetHybridTransient to set transient state to ZL303XX_BHTT_OPTIONAL returned %d", status);
                                                             break;
                    case VTSS_PTP_HYBRID_TRANSIENT_NOT_ACTIVE: status = zl303xx_AprSetHybridTransient(zl303xx_Params[domain], ZL303XX_BHTT_NOT_ACTIVE);
                                                               T_IG(TRACE_GRP_PTP_INTF, "Call to zl303xx_AprSetHybridTransient to set transient state to ZL303XX_BHTT_NOT_ACTIVE returned %d", status);
                                                               break;
                    default: status = ZL303XX_ERROR;
                }

                if (status != ZL303XX_OK) {
                    rc = VTSS_RC_ERROR;
                }
            }
        }
    }
    if (rc == VTSS_RC_OK) {
        curTransient = transient;
    }
    return rc;
}

mesa_rc zl_30380_apr_set_log_level(i32 level)
{
    if (level >= 0) {
        ZL_30380_CHECK(zl303xx_SetAprLogLevel(level));
        aprLoggingEnabled = ZL303XX_TRUE;
    } else {
        aprLoggingEnabled = ZL303XX_FALSE;
        ZL_30380_CHECK(zl303xx_SetAprLogLevel(0));
    }

    return VTSS_RC_OK;
}

mesa_rc zl_30380_apr_show_psl_fcl_config(u16 cguId)
{
    CGUID_CHECK(cguId);
    ZL_30380_CHECK(zl303xx_DebugAprPrintPSLFCLData(zl303xx_Params[cguId]));

    return VTSS_RC_OK;
}

mesa_rc zl_30380_apr_show_statistics(u16 cguId, u32 stati)
{
    CGUID_CHECK(cguId);
    if (stati == 0) {
        ZL_30380_CHECK(zl303xx_DebugGetAprPathStatistics(zl303xx_Params[cguId]));
    }
    if (stati == 1) {
        ZL_30380_CHECK(zl303xx_AprGetDeviceCurrentPathDelays(zl303xx_Params[cguId]));
    }
    if (stati == 2) {
        ZL_30380_CHECK(zl303xx_DebugGetAllAprStatistics(zl303xx_Params[cguId]));
    }
    return VTSS_RC_OK;
}

/****************************************************************************/
/*                                                                          */
/*  End of file.                                                            */
/*                                                                          */
/****************************************************************************/
