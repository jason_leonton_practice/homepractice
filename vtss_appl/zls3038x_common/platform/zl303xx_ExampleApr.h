

/*******************************************************************************
*
*  $Id: zl303xx_ExampleApr.h 14468 2016-11-11 19:10:35Z FL $
*
*  Copyright 2006-2016 Microsemi Semiconductor Limited.
*  All rights reserved.
*
*  Module Description:
*     Header file for APR example files
*
*******************************************************************************/

#ifndef _ZL303XX_EXAMPLE_APR_H_
#define _ZL303XX_EXAMPLE_APR_H_

#ifdef __cplusplus
extern "C" {
#endif

/*****************   INCLUDE FILES   ******************************************/
#include "zl303xx_Global.h"
#include "zl303xx_Error.h"
#include "zl303xx_Apr.h"
#include "zl303xx_DeviceSpec.h"

/*****************   DEFINES   ************************************************/

#define ZL303XX_HYBRID_TRANSIENT_ON   (1)
#define ZL303XX_HYBRID_TRANSIENT_OFF  (0)

/*****************   DATA TYPES   *********************************************/

typedef struct
{
   zl303xx_ParamsS *cguId;
   zl303xx_AprAddServerS server;
} exampleAprStreamCreateS;

typedef struct
{
   zl303xx_ParamsS *cguId;
   zl303xx_AprAddDeviceS device;

} exampleAprClockCreateS;



/*****************   EXPORTED GLOBAL VARIABLE DECLARATIONS   ******************/

/*****************   EXTERNAL FUNCTION DECLARATIONS   *************************/

zlStatusE exampleAprEnvInit(void);
zlStatusE exampleAprEnvClose(void);

zlStatusE exampleAprClockCreateStructInit(exampleAprClockCreateS *pClock);
zlStatusE exampleAprClockCreate(exampleAprClockCreateS *pClock);
zlStatusE exampleAprClockRemove(exampleAprClockCreateS *pClock);

zlStatusE exampleAprStreamCreateStructInit(exampleAprStreamCreateS *pStream);
zlStatusE exampleAprStreamCreate(exampleAprStreamCreateS *pStream);
zlStatusE exampleAprStreamRemove(exampleAprStreamCreateS *pStream);

Sint32T exampleAprDcoSetFreq(void *clkGenId, Sint32T freqOffsetInPartsPerTrillion);
Sint32T exampleAprDcoGetFreq(void *clkGenId, Sint32T *freqOffsetInPartsPerTrillion);
Sint32T exampleAprGetHwManualFreerunStatus(void *hwParams, Sint32T *manStatus);
Sint32T exampleAprGetHwManualHoldoverStatus(void *hwParams, Sint32T *manStatus);
Sint32T exampleAprGetHwLockStatus(void *hwParams, Sint32T *manStatus);
Sint32T exampleAprDefaultgetHwSyncInputEnStatus(void *hwParams, Sint32T *manStatus);
Sint32T exampleAprDefaultgetHwOutOfRangeStatus(void *hwParams, Sint32T *manStatus);
Sint32T exampleAprDefaultSwitchToPacketRef(void *hwParams);
Sint32T exampleAprDefaultSwitchToElectricalRef(void *hwParams);


Sint32T exampleAprSetTimeTsu(void *clkGenId, Uint64S deltaTimeSec, Uint32T deltaTimeNanoSec,
                             zl303xx_BooleanE negative);
Sint32T exampleAprStepTimeTsu(void *clkGenId, Sint32T deltaTimeNs);

#if defined ZLS30341_INCLUDED
Sint32T example34xJumpTimeTsu(void *clkGenId,
                              Uint64S deltaTimeSec,
                              Uint32T deltaTimeNanoSec,
                              zl303xx_BooleanE negative);
#endif
#if defined ZLS30361_INCLUDED
Sint32T example36xJumpTimeTsu(void *clkGenId,
                              Uint64S deltaTimeSec,
                              Uint32T deltaTimeNanoSec,
                              zl303xx_BooleanE negative);
#endif
#if defined ZLS30721_INCLUDED
Sint32T example72xJumpTimeTsu(void *clkGenId,
                              Uint64S deltaTimeSec,
                              Uint32T deltaTimeNanoSec,
                              zl303xx_BooleanE negative);
#endif
#if defined ZLS30701_INCLUDED
Sint32T example70xJumpTimeTsu(void *clkGenId,
                              Uint64S deltaTimeSec,
                              Uint32T deltaTimeNanoSec,
                              zl303xx_BooleanE negative);
#endif
#if defined ZLS30751_INCLUDED
Sint32T example75xJumpTimeTsu(void *clkGenId,
                              Uint64S deltaTimeSec,
                              Uint32T deltaTimeNanoSec,
                              zl303xx_BooleanE negative);
#endif


zlStatusE exampleAprHandleHybridTransient(void *hwParams, zl303xx_BCHybridTransientType transient);
Sint32T exampleAprBCHybridActionPhaseLock(void *hwParams);
Sint32T exampleAprBCHybridActionOutOfLock(void *hwParams);
Sint32T exampleAprDriverMsgRouter(void *hwParams, void *inData, void *outData);


#ifdef __cplusplus
}
#endif

#endif /* MULTIPLE INCLUDE BARRIER */
