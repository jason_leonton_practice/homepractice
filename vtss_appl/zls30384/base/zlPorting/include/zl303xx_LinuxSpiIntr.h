

/*******************************************************************************
*
*  $Id: zl303xx_LinuxSpiIntr.h 10065 2013-09-25 15:44:04Z PC $
*
*  Copyright 2006-2016 Microsemi Semiconductor Limited.
*  All rights reserved.
*
*  Module Description:
*     Types and prototypes needed by the SPI routines
*
*******************************************************************************/

#ifndef _ZL303XX_LNX_SPI_INTERRUPTS_H_
#define _ZL303XX_LNX_SPI_INTERRUPTS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "zl303xx_Global.h"  /* This should always be the first include file */
#include "zl303xx_Error.h"    /* For zlStatusE */
#include "zl303xx.h"

#define ZL303XX_LNX_INTR_TASK_STACK_SIZE 2000
#define ZL303XX_LNX_INTR_TASK_PRIORITY   99   /* Highest priority */

#ifdef OS_LINUX
    void zl303xx_Intr0Handler(Sint32T sigNum, Sint32T unUsed);
    zlStatusE zl303xx_InitHighIntr0Task(Uint8T cpuIntNum);
    zlStatusE zl303xx_DestroyHighIntr0Task(Uint8T cpuIntNum);
    zlStatusE zl303xx_InitLowIntr0Task(Uint8T cpuIntNum);
    zlStatusE zl303xx_DestroyLowIntr0Task(Uint8T cpuIntNum);
    void zl303xx_LinuxHighIntr0Task(void);
    void zl303xx_LinuxLowIntr0Task(void) __attribute__ ((noreturn));
    osStatusT zl303xx_ResetDev(const char * devBaseName, Uint8T devCSToReset);

    #ifdef ZL_INTR_USES_SIGACTION
        zlStatusE zl303xx_Init0Intr(zl303xx_ParamsS *zl303xx_Params, Uint16T sigNum);
        zlStatusE zl303xx_Destroy0Intr(zl303xx_ParamsS *zl303xx_Params, Uint16T sigNum);
    #endif
#endif

#ifdef __cplusplus
}
#endif

#endif  /* _ZL303XX_LNX_SPI_INTERRUPTS_H_ */
