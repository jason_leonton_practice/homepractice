/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef __VTSS_ERPS_SERIALIZER_HXX__
#define __VTSS_ERPS_SERIALIZER_HXX__

#include "vtss_appl_serialize.hxx"
#include "vtss/appl/erps.h"

//----------------------------------------------------------------------------
// SNMP tagging for basic types
//----------------------------------------------------------------------------

VTSS_SNMP_TAG_SERIALIZE(vtss_appl_erps_u32_dsc, u32, a, s ) {
    a.add_leaf(vtss::AsInt(s.inner),
               vtss::tag::Name("groupIndex"),
               vtss::expose::snmp::RangeSpec<uint32_t>(0, 2147483647),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(1),
               vtss::tag::Description("ERPS group index number. Valid range is (1..max groups). The maximum group number is platform-specific and can be retrieved from the ERPS capabilities."));
}

//----------------------------------------------------------------------------
// SNMP enums; become textual conventions
//----------------------------------------------------------------------------

VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_erps_ring_type_t,
                         "ErpsRingType",
                         vtss_appl_erps_ring_type_txt,
                         "Specifies the ERPS ring type.");

VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_erps_admin_cmd_t,
                         "ErpsAdminCmd",
                         vtss_appl_erps_admin_cmd_txt,
                         "Specifies the Administrative command.");

VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_erps_version_t,
                         "ErpsVersion",
                         vtss_appl_erps_version_txt,
                         "Specifies the ERPS protocol version.");

VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_erps_rpl_mode_t,
                         "ErpsRplMode",
                         vtss_appl_erps_rpl_mode_txt,
                         "Specifies the Ring Protection Link mode. Use 'none' if port is neither RPL owner nor neighbour.");

VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_erps_port_state_t,
                         "ErpsPortState",
                         vtss_appl_erps_port_state_txt,
                         "Specifies the ring port state.");

VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_erps_protection_state_t,
                         "ErpsProtectionState",
                         vtss_appl_erps_protection_state_txt,
                         "Specifies the ring protection state.");

VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_erps_port_t,
                         "ErpsPort",
                         vtss_appl_erps_port_txt,
                         "Specifies a particular logical ring port.");

VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_erps_request_state_t,
                         "ErpsRequestState",
                         vtss_appl_erps_request_state_txt,
                         "Specifies a request/state.");

VTSS_XXXX_SERIALIZE_ENUM(vtss_appl_erps_control_cmd_t,
                         "ErpsControlCmd",
                         vtss_appl_erps_control_cmd_txt,
                         "Specifies a control command.");

//----------------------------------------------------------------------------
// Capabilities
//----------------------------------------------------------------------------

template<typename T>
void serialize(T &a, vtss_appl_erps_capabilities_t &s)
{
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_erps_capabilities_t"));

    m.add_leaf(s.max_groups,
               vtss::tag::Name("MaxGroups"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(1),
               vtss::tag::Description("Maximum number of configured ERPS groups."));
    m.add_leaf(s.max_vlans_per_group,
               vtss::tag::Name("MaxVlansPerGroup"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(2),
               vtss::tag::Description("Maximum number of protected VLANS per ERPS group."));
}

//----------------------------------------------------------------------------
// Configuration
//----------------------------------------------------------------------------

template<typename T>
void serialize(T &a, vtss_appl_erps_conf_t &s)
{
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_erps_conf_t"));

    m.add_leaf(s.ring_type,
               vtss::tag::Name("RingType"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(2),
               vtss::tag::Description("Type of ring. Can only be set once for a ring instance."));

    m.add_leaf(vtss::AsInterfaceIndex(s.port0),
               vtss::tag::Name("Port0"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(3),
               vtss::tag::Description("ifindex of ring protection port 0. Can only be set once for a ring instance."));

    m.add_leaf(vtss::AsInterfaceIndex(s.port1),
               vtss::tag::Name("Port1"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(4),
               vtss::tag::Description("ifindex of ring protection Port 1. For interconnected sub-rings this value must be zero. Can only be set once for a ring instance."));

    m.add_leaf(s.interconnect_major_ring_id,
               vtss::tag::Name("InterconnectMajorRingGroupIndex"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(5),
               vtss::tag::Description("For sub-ring: zero = not interconnected; > zero = index of major ring group. "
                        "For major ring: zero = not interconnected; > zero = is interconnected (i.e flag-like semantics). "
                        "Can only be set once for a ring instance."));

    m.add_leaf(vtss::AsBool(s.virtual_channel),
               vtss::tag::Name("VirtualChannel"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(6),
               vtss::tag::Description("Whether to use a virtual channel. Can only be set once for a ring instance."));

    m.add_leaf(s.port0_sf_mep_id,
               vtss::tag::Name("Port0SignalFailMepIndex"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(7),
               vtss::tag::Description("Index of SignalFail MEP for Port 0. Zero if not set. To clear an ERPS group's MEP association, set this to zero; it affects all MEP indices."));

    m.add_leaf(s.port0_aps_mep_id,
               vtss::tag::Name("Port0ApsMepIndex"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(8),
               vtss::tag::Description("Index of APS MEP for Port 0. Zero if not set."));

    m.add_leaf(s.port1_sf_mep_id,
               vtss::tag::Name("Port1SignalFailMepIndex"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(9),
               vtss::tag::Description("Index of SignalFail MEP for Port 1. Zero if not set. Must be zero for interconnected sub-rings."));

    m.add_leaf(s.port1_aps_mep_id,
               vtss::tag::Name("Port1ApsMepIndex"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(10),
               vtss::tag::Description("Index of APS MEP for Port 1. Zero if not set. Must be zero for interconnected sub-rings with virtual channel."));

    m.add_leaf(s.hold_off_time,
               vtss::tag::Name("HoldOffTime"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(11),
               vtss::tag::Description("Hold off time in ms. Value is rounded down to 100ms precision. Valid range is 0-10000 ms"));

    m.add_leaf(s.wtr_time,
               vtss::tag::Name("WaitToRestoreTime"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(12),
               vtss::tag::Description("Wait-to-Restore time in ms. Valid range is 60000-720000 ms (1-12 minutes)."));

    m.add_leaf(s.guard_time,
               vtss::tag::Name("GuardTime"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(13),
               vtss::tag::Description("Guard time in ms. Valid range is 10-2000 ms."));

    m.add_leaf(s.rpl_mode,
               vtss::tag::Name("RplMode"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(14),
               vtss::tag::Description("Ring Protection Link mode."));

    m.add_leaf(s.rpl_port,
               vtss::tag::Name("RplPort"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(15),
               vtss::tag::Description("RPL port. Only valid if the ERPS group is RPL owner or neighbour."));

    m.add_leaf(vtss::AsBool(s.revertive),
               vtss::tag::Name("Revertive"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(16),
               vtss::tag::Description("Revertive (true) or Non-revertive (false) mode."));

    m.add_leaf(s.version,
               vtss::tag::Name("Version"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(17),
               vtss::tag::Description("ERPS protocol version."));

    m.add_leaf(vtss::AsBool(s.topology_change),
               vtss::tag::Name("TopologyChange"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(18),
               vtss::tag::Description("Whether to enable Topology Change propagation; only valid for an interconnected node."));

    m.add_rpc_leaf(vtss::AsVlanList(s.protected_vlans, 512),
                   vtss::tag::Name("ProtectedVlans"),
                   vtss::tag::Description("VLANs which is protected by this ring instance"));

    m.add_snmp_leaf(vtss::AsVlanListQuarter(s.protected_vlans + 0 * 128, 128),
                    vtss::tag::Name("ProtectedVlans0Kto1K"),
                    vtss::expose::snmp::Status::Current,
                    vtss::expose::snmp::OidElementValue(19),
                    vtss::tag::Description("First quarter of bit-array indicating whether a VLAN is protected by this ring instance ('1') or not ('0')."));

    m.add_snmp_leaf(vtss::AsVlanListQuarter(s.protected_vlans + 1 * 128, 128),
                    vtss::tag::Name("ProtectedVlans1Kto2K"),
                    vtss::expose::snmp::Status::Current,
                    vtss::expose::snmp::OidElementValue(20),
                    vtss::tag::Description("Second quarter of bit-array indicating whether a VLAN is protected by this ring instance ('1') or not ('0')."));

    m.add_snmp_leaf(vtss::AsVlanListQuarter(s.protected_vlans + 2 * 128, 128),
                    vtss::tag::Name("ProtectedVlans2Kto3K"),
                    vtss::expose::snmp::Status::Current,
                    vtss::expose::snmp::OidElementValue(21),
                    vtss::tag::Description("Third quarter of bit-array indicating whether a VLAN is protected by this ring instance ('1') or not ('0')."));

    a.add_snmp_leaf(vtss::AsVlanListQuarter(s.protected_vlans + 3 * 128, 128),
                    vtss::tag::Name("ProtectedVlans3Kto4K"),
                    vtss::expose::snmp::Status::Current,
                    vtss::expose::snmp::OidElementValue(22),
                    vtss::tag::Description("Fourth quarter of bit-array indicating whether a VLAN is protected by this ring instance ('1') or not ('0')."));
}

//----------------------------------------------------------------------------
// Status
//----------------------------------------------------------------------------

template<typename T>
void serialize(T &a, vtss_appl_erps_status_t &s)
{
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_erps_status_t"));

    m.add_leaf(vtss::AsBool(s.active),
               vtss::tag::Name("Active"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(2),
               vtss::tag::Description("Specifies whether ERPS group is currently active or not."));

    m.add_leaf(s.state,
               vtss::tag::Name("ProtectionState"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(3),
               vtss::tag::Description("Specifies current ERPS group protection state."));

    m.add_leaf(vtss::AsBool(s.rpl_blocked),
               vtss::tag::Name("RplBlocked"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(4),
               vtss::tag::Description("Specifies whether the RPL is currently blocked or not."));

    m.add_leaf(s.wtr_remaining_time,
               vtss::tag::Name("WtrRemaining"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(5),
               vtss::tag::Description("Specifies the remaining wait-time before restoring ring."));

    m.add_leaf(s.admin_cmd,
               vtss::tag::Name("AdminCmd"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(6),
               vtss::tag::Description("Specifies the currently active administrative command."));

    m.add_leaf(vtss::AsBool(s.fop_alarm),
               vtss::tag::Name("FopAlarm"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(7),
               vtss::tag::Description("Specifies whether the FailureOfProtocol alarm is currently active or not."));

    m.add_leaf(vtss::AsBool(s.tx),
               vtss::tag::Name("TxActive"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(8),
               vtss::tag::Description("Specifies whether R-APS transmission is currently active."));

    m.add_leaf(s.tx_req,
               vtss::tag::Name("TxRequestOrState"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(9),
               vtss::tag::Description("Specifies the currently transmitted request/state. Only relevant when Tx is active."));

    m.add_leaf(vtss::AsBool(s.tx_rb),
               vtss::tag::Name("TxRplBlocked"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(10),
               vtss::tag::Description("Specifies the currently transmitted RPL Blocked flag. Only relevant when Tx is active."));

    m.add_leaf(vtss::AsBool(s.tx_dnf),
               vtss::tag::Name("TxDoNotFlush"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(11),
               vtss::tag::Description("Specifies the currently transmitted Do Not Flush flag. Only relevant when Tx is active."));

    m.add_leaf(s.tx_bpr,
               vtss::tag::Name("BlockedPortReference"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(12),
               vtss::tag::Description("Specifies the currently transmitted Blocked Port Reference. Only relevant when Tx is active."));

    // Port 0:

    m.add_leaf(vtss::AsBool(s.port0_blocked),
               vtss::tag::Name("Port0Blocked"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(13),
               vtss::tag::Description("Specifies whether ring Port 0 is blocked or not."));

    m.add_leaf(s.port0_state,
               vtss::tag::Name("Port0State"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(14),
               vtss::tag::Description("Specifies the state of ring Port 0."));

    m.add_leaf(vtss::AsBool(s.port0_rx),
               vtss::tag::Name("Port0RxActive"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(15),
               vtss::tag::Description("Specifies if ring Port0 receives R-APS."));

    m.add_leaf(s.port0_rx_req,
               vtss::tag::Name("Port0RxRequestOrState"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(16),
               vtss::tag::Description("Specifies the most recently received request/state on ring Port 0. Only relevant when Rx is active."));

    m.add_leaf(vtss::AsBool(s.port0_rx_rb),
               vtss::tag::Name("Port0RxRplBlocked"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(17),
               vtss::tag::Description("Specifies the most recently received RPL Blocked flag on ring Port 0. Only relevant when Rx is active."));

    m.add_leaf(vtss::AsBool(s.port0_rx_dnf),
               vtss::tag::Name("Port0RxDoNotFlush"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(18),
               vtss::tag::Description("Specifies the most recently received Do Not Flush flag on ring Port 0. Only relevant when Rx is active."));

    m.add_leaf(s.port0_rx_bpr,
               vtss::tag::Name("Port0RxBlockedPortReference"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(19),
               vtss::tag::Description("Specifies the most recently received Blocked Port Reference on ring Port 0. Only relevant when Rx is active."));

    m.add_leaf(vtss::AsOctetString(s.port0_rx_node_id, sizeof(s.port0_rx_node_id)),
               vtss::tag::Name("Port0RxNodeId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(20),
               vtss::tag::Description("Specifies the most recently received node ID on ring Port 0. Only relevant when Rx is active."));

    // Port 1:
    m.add_leaf(vtss::AsBool(s.port1_blocked),
               vtss::tag::Name("Port1Blocked"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(21),
               vtss::tag::Description("Specifies whether ring Port 1 is blocked or not."));

    m.add_leaf(s.port1_state,
               vtss::tag::Name("Port1State"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(22),
               vtss::tag::Description("Specifies the state of ring Port 1."));

    m.add_leaf(vtss::AsBool(s.port1_rx),
               vtss::tag::Name("Port1RxActive"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(23),
               vtss::tag::Description("Specifies if ring Port1 receives R-APS."));

    m.add_leaf(s.port1_rx_req,
               vtss::tag::Name("Port1RxRequestOrState"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(24),
               vtss::tag::Description("Specifies the most recently received request/state on ring Port 1. Only relevant when Rx is active."));

    m.add_leaf(vtss::AsBool(s.port1_rx_rb),
               vtss::tag::Name("Port1RxRplBlocked"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(25),
               vtss::tag::Description("Specifies the most recently received RPL Blocked flag on ring Port 1. Only relevant when Rx is active."));

    m.add_leaf(vtss::AsBool(s.port1_rx_dnf),
               vtss::tag::Name("Port1RxDoNotFlush"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(26),
               vtss::tag::Description("Specifies the most recently received Do Not Flush flag on ring Port 1. Only relevant when Rx is active."));

    m.add_leaf(s.port1_rx_bpr,
               vtss::tag::Name("Port1RxBlockedPortReference"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(27),
               vtss::tag::Description("Specifies the most recently received Blocked Port Reference on ring Port 1. Only relevant when Rx is active."));

    m.add_leaf(vtss::AsOctetString(s.port1_rx_node_id, sizeof(s.port1_rx_node_id)),
               vtss::tag::Name("Port1RxNodeId"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(28),
               vtss::tag::Description("Specifies the most recently received node ID on ring Port 1. Only relevant when Rx is active."));
}

//----------------------------------------------------------------------------
// Statistics
//----------------------------------------------------------------------------

template<typename T>
void serialize(T &a, vtss_appl_erps_statistics_t &s)
{
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_erps_statistics_t"));

    m.add_leaf(vtss::AsCounter(s.raps_sent),
               vtss::tag::Name("RapsTx"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(2),
               vtss::tag::Description("Number of transmitted RAPS PDUs."));

    m.add_leaf(vtss::AsCounter(s.raps_rcvd),
               vtss::tag::Name("RapsRx"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(3),
               vtss::tag::Description("Number of received RAPS PDUs."));

    m.add_leaf(vtss::AsCounter(s.raps_rx_dropped),
               vtss::tag::Name("RapsRxDrop"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(4),
               vtss::tag::Description("Number of received RAPS PDUs that were dropped."));

    m.add_leaf(vtss::AsCounter(s.local_sf),
               vtss::tag::Name("LocalSF"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(5),
               vtss::tag::Description("Number of local SignalFail events."));

    m.add_leaf(vtss::AsCounter(s.local_sf_cleared),
               vtss::tag::Name("LocalSFCleared"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(6),
               vtss::tag::Description("Number of local SignalFail clear events."));

    m.add_leaf(vtss::AsCounter(s.remote_sf),
               vtss::tag::Name("RemoteSF"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(7),
               vtss::tag::Description("Number of remote SignalFail events."));

    m.add_leaf(vtss::AsCounter(s.event_nr),
               vtss::tag::Name("NR"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(8),
               vtss::tag::Description("Number of NoRequest events."));

    m.add_leaf(vtss::AsCounter(s.remote_ms),
               vtss::tag::Name("RemoteMS"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(9),
               vtss::tag::Description("Number of remote ManualSwitch events."));

    m.add_leaf(vtss::AsCounter(s.local_ms),
               vtss::tag::Name("LocalMS"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(10),
               vtss::tag::Description("Number of local ManualSwitch events."));

    m.add_leaf(vtss::AsCounter(s.remote_fs),
               vtss::tag::Name("RemoteFS"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(11),
               vtss::tag::Description("Number of remote ForcedSwitch events."));

    m.add_leaf(vtss::AsCounter(s.local_fs),
               vtss::tag::Name("localFS"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(12),
               vtss::tag::Description("Number of local ForcedSwitch events."));

    m.add_leaf(vtss::AsCounter(s.admin_cleared),
               vtss::tag::Name("AdminClear"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(13),
               vtss::tag::Description("Number of AdminClear events."));
}

//----------------------------------------------------------------------------
// Control
//----------------------------------------------------------------------------

template<typename T>
void serialize(T &a, vtss_appl_erps_control_t &s)
{
    typename T::Map_t m =
            a.as_map(vtss::tag::Typename("vtss_appl_erps_control_t"));

    m.add_leaf(s.cmd,
               vtss::tag::Name("Command"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(2),
               vtss::tag::Description("Control command to execute. Always returns none when read."));
    m.add_leaf(s.port,
               vtss::tag::Name("Port"),
               vtss::expose::snmp::Status::Current,
               vtss::expose::snmp::OidElementValue(3),
               vtss::tag::Description("Ring port affected by control command. Only relevant for administrative commands. Always returns port0 when read."));
}


namespace vtss {
namespace appl {
namespace erps {
namespace interfaces {

struct ErpsCapabilities {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamVal<vtss_appl_erps_capabilities_t *>
    > P;

    VTSS_EXPOSE_SERIALIZE_ARG_1(vtss_appl_erps_capabilities_t &i) {
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_erps_capabilities_get);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_ERPS);
};

struct ErpsConfTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_erps_conf_t *>
    > P;

    static constexpr uint32_t snmpRowEditorOid = 100;
    static constexpr const char *table_description =
        "This is the ERPS group configuration table.";

    static constexpr const char *index_description =
        "Each entry in this table represents an ERPS group.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, vtss_appl_erps_u32_dsc(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_erps_conf_t &i) {
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_erps_conf_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_erps_conf_itr);
    VTSS_EXPOSE_SET_PTR(vtss_appl_erps_conf_set);
    VTSS_EXPOSE_ADD_PTR(vtss_appl_erps_conf_set);
    VTSS_EXPOSE_DEL_PTR(vtss_appl_erps_conf_del);
    VTSS_EXPOSE_DEF_PTR(vtss_appl_erps_conf_default);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_ERPS);
};

struct ErpsStatusTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_erps_status_t *>
    > P;

    static constexpr const char *table_description =
        "This table contains status per EPRS group.";

    static constexpr const char *index_description =
        "Status.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, vtss_appl_erps_u32_dsc(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_erps_status_t &i) {
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_erps_status_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_erps_status_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_ERPS);
};

struct ErpsStatisticsTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_erps_statistics_t *>
    > P;

    static constexpr const char *table_description =
        "This table contains statistics per EPRS group.";

    static constexpr const char *index_description =
        "Various PDU and event counters.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, vtss_appl_erps_u32_dsc(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_erps_statistics_t &i) {
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_erps_statistics_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_erps_statistics_itr);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_STAT_TYPE, VTSS_MODULE_ID_ERPS);
};

struct ErpsControlTable {
    typedef vtss::expose::ParamList<
        vtss::expose::ParamKey<u32>,
        vtss::expose::ParamVal<vtss_appl_erps_control_t *>
    > P;

    static constexpr const char *table_description =
        "This is the ERPS group control table.";

    static constexpr const char *index_description =
        "Each entry in this table represents dynamic control elements an ERPS group.";

    VTSS_EXPOSE_SERIALIZE_ARG_1(u32 &i) {
        serialize(h, vtss_appl_erps_u32_dsc(i));
    }

    VTSS_EXPOSE_SERIALIZE_ARG_2(vtss_appl_erps_control_t &i) {
        serialize(h, i);
    }

    VTSS_EXPOSE_GET_PTR(vtss_appl_erps_control_get);
    VTSS_EXPOSE_ITR_PTR(vtss_appl_erps_control_itr);
    VTSS_EXPOSE_SET_PTR(vtss_appl_erps_control_set);
    VTSS_EXPOSE_WEB_PRIV(VTSS_PRIV_LVL_CONFIG_TYPE, VTSS_MODULE_ID_ERPS);
};

}  // namespace interfaces
}  // namespace erps
}  // namespace appl
}  // namespace vtss
#endif  /* __VTSS_ERPS_SERIALIZER_HXX__ */
