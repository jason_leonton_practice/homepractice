/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

// Disable GCC 6.3 warnings due to  VTSS_ERPS_BLOCK_RING_PORT macro etc.
#if defined(__GNUC__) && __GNUC__ >= 6
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wtautological-compare"
#endif

#include "main.h"
#include "conf_api.h"
#include "critd_api.h"
#include "mscc/ethernet/switch/api.h"
#include "interrupt_api.h"
#include "erps.h"
#include "vtss_erps_api.h"
#include "vlan_api.h" 
#include "misc_api.h"   //uport2iport(), iport2uport()
#include "vtss/appl/erps.h"
#include "vtss_safe_queue.hxx"

#ifdef VTSS_SW_OPTION_ICFG
#include "icfg_api.h"
#include "msg_api.h"
#include "topo_api.h"
#include "icli_porting_util.h"
#endif

#ifdef VTSS_SW_OPTION_MEP
#include "mep_api.h"
#endif

#include "vtss_erps.h" // for ERPS_NODE_RPL_OWNER
#include "led_api.h"

#ifdef VTSS_SW_OPTION_PRIVATE_MIB
VTSS_PRE_DECLS void erps_mib_init(void);
#endif /* VTSS_SW_OPTION_PRIVATE_MIB */

#ifdef VTSS_SW_OPTION_JSON_RPC
VTSS_PRE_DECLS void vtss_appl_erps_json_init(void);
#endif

/******************************************************************************
                                       #defines
******************************************************************************/
#define VTSS_TRACE_MODULE_ID VTSS_MODULE_ID_ERPS
#define VTSS_ALLOC_MODULE_ID VTSS_MODULE_ID_ERPS

#define VTSS_TRACE_GRP_DEFAULT 0
#define TRACE_GRP_CRIT         1
#define TRACE_GRP_MGMT         2
#define TRACE_GRP_CTRL         3
#define TRACE_GRP_ERPS_RX      4
#define TRACE_GRP_ERPS_TX      5
#define TRACE_GRP_SWAPI        6
#define TRACE_GRP_STATE_CHANGE 7
#define TRACE_GRP_CNT          8


/*******************************************************************************
  Global variables                                                        
*******************************************************************************/
/*
 * erps_mep_t contains mapping of port to mep_ids, mep_ids are not exposed to
 * base thus will be converted to mep_id to/from erps base
 * 
 * port[0] contains east port and port[1] contains west port
 * ccm_mep_id[0] contains ccm east_mepid and ccm_mep_id[1] contains ccm west_mepid
 * aps_mep_id[0] contains raps east_mepid and aps_mep_id[1] contains raps west_mepid
 */
typedef struct erps_mep
{
#define ERPS_ARRAY_OFFSET_EAST 0
#define ERPS_ARRAY_OFFSET_WEST 1

    mesa_port_no_t  port[2];
    u32             ccm_mep_id[2];
    u32             raps_mep_id[2];
}erps_mep_t;

erps_mep_t   port_to_mep[ERPS_MAX_PROTECTION_GROUPS+1];

/* ERPS thread resources */
static vtss_handle_t   run_thread_handle;
static vtss_thread_t   run_thread_block;

/* queue resources */
static vtss::SafeQueue erps_queue;

/* critical section resources */
static critd_t         crit;
static critd_t         crit_p;
static critd_t         crit_fsm;
static BOOL            erps_init_done = FALSE;

#if (VTSS_TRACE_ENABLED)

static vtss_trace_reg_t trace_reg =
{
    VTSS_TRACE_MODULE_ID, "erps", "Ethernet Ring Protection"
};

static vtss_trace_grp_t trace_grps[TRACE_GRP_CNT] =
{
    /* VTSS_TRACE_GRP_DEFAULT */ {
        "default",
        "Default (ERPS core)",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_CRIT */ {
        "crit",
        "Critical regions ",
        VTSS_TRACE_LVL_ERROR,
        VTSS_TRACE_FLAGS_TIMESTAMP
    },
    /* TRACE_GRP_MGMT */ {
        "mgmt",
        "MGMT API",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
     },
    /* TRACE_GRP_CTRL */ {
        "Ctrl",
        "Ctrl API",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
     },
    /* TRACE_GRP_ERPS_RX */ {
        "ERPSRx",
        "ERPS Rx frames",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
     },
    /* TRACE_GRP_ERPS_TX */ {
        "ERPSTx",
        "ERPS Tx frames",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
     },
    /* TRACE_GRP_SWAPI */ {
        "SwAPI",
        "Switch API",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
     },
    /* TRACE_GRP_STATE_CHANGE */ {
        "ERPSFSM",
        "ERPS State Changes",
        VTSS_TRACE_LVL_WARNING,
        VTSS_TRACE_FLAGS_TIMESTAMP
     },
};

#define CRIT_ENTER(crit) critd_enter(&crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define CRIT_EXIT(crit)  critd_exit(&crit, TRACE_GRP_CRIT, VTSS_TRACE_LVL_NOISE, __FILE__, __LINE__)
#define CRIT_ASSERT_LOCKED(crit) critd_assert_locked(&crit, TRACE_GRP_CRIT, __FILE__, __LINE__)
#else

#define CRIT_ENTER(crit) critd_enter(&crit)
#define CRIT_EXIT(crit)  critd_exit(&crit)
#define CRIT_ASSERT_LOCKED(crit) critd_assert_locked(&crit)

#endif /* VTSS_TRACE_ENABLED */

void vtss_erps_crit_lock(void)
{
    CRIT_ENTER(crit);
}

void vtss_erps_crit_unlock(void)
{
    CRIT_EXIT(crit);
}

void vtss_erps_crit_assert_locked(void)
{
    CRIT_ASSERT_LOCKED(crit);
}
/*
 * mutex for ERPS FSM
 */
void vtss_erps_fsm_crit_lock(void)
{
    CRIT_ENTER(crit_fsm);
}

void vtss_erps_fsm_crit_unlock(void)
{
    CRIT_EXIT(crit_fsm);
}


/******************************************************************************
                              ERPS Control Functions 
******************************************************************************/

static void erps_restore_to_default(void)
{
    u32                   group_id;
    i32                   ret;
    vtss_erps_base_conf_t pconf;
    u8                    grp_status;

    /* All MEP-related functions are going to fail because the MEPs have already been
     * reset to default -- module 'mep' comes before module 'erps' in the initfun table.
     */

    for (group_id = 0; group_id < ERPS_MAX_PROTECTION_GROUPS; ++group_id) {
        if (vtss_erps_get_protection_group_status(group_id, &grp_status) != VTSS_RC_OK) {
            T_D("Failed getting status for internal group id %d", group_id);
            continue;
        }

        // Special case for RESERVED group: no MEP stuff is configured, so just delete it
        if (grp_status == ERPS_PROTECTION_GROUP_RESERVED) {
            if (vtss_erps_delete_protection_group (group_id) != VTSS_RC_OK) {
                T_D("Failed to delete internal group id %d (state RESERVED)", group_id);
            }
            continue;
        }

        if (grp_status != ERPS_PROTECTION_GROUP_ACTIVE) {
            continue;
        }

        T_D("Disabling active internal group id %d", group_id);

        memset(&pconf, 0, sizeof(pconf));
        pconf.group_id = group_id;
        if (vtss_erps_get_protection_group_by_id(&pconf) != VTSS_RC_OK) {
            T_D("Failed getting internal group id %d", group_id);
            continue;
        }

        /* disable R-APS forwarding for this protection group */
        ret = vtss_erps_raps_forwarding(pconf.erpg.east_port, group_id, FALSE);
        if (ret != VTSS_RC_OK) {
            T_D("vtss_erps_raps_forwarding failed for group = %u", group_id);
        }

        if (pconf.erpg.west_port || pconf.erpg.virtual_channel) {
            ret = vtss_erps_raps_forwarding(pconf.erpg.west_port, group_id, FALSE);
            if (ret != VTSS_RC_OK) {
                T_D("vtss_erps_raps_forwarding failed for group = %u", group_id);
            }
        }

        /* disable R-APS transmission for this protection group */
        ret = vtss_erps_raps_transmission(pconf.erpg.east_port, group_id, FALSE);
        if (ret != VTSS_RC_OK) {
            T_D("vtss_erps_raps_transmission failed for group = %u", group_id);
        }

        if (pconf.erpg.west_port || pconf.erpg.virtual_channel) {
            ret = vtss_erps_raps_transmission(pconf.erpg.west_port, group_id, FALSE);
            if (ret != VTSS_RC_OK) {
                T_D("vtss_erps_raps_transmission failed for group = %u", group_id);
            }
        }

        /* deregister with MEP */
        ret = erps_mep_aps_sf_register(pconf.erpg.east_port, pconf.erpg.west_port,
                                       group_id, FALSE, pconf.erpg.virtual_channel);
        if (ret != VTSS_RC_OK) {
            T_D("vtss_erps_mep_aps_sf_register failed for group = %u", group_id);
        }

        ret = vtss_erps_delete_protection_group (group_id);
        if (ret != VTSS_APPL_ERPS_RC_OK) {
            T_D("Failed to delete internal group id status %d", group_id);
            continue;
        }
        erps_platform_clear_port_to_mep(group_id);
        lntn_sysled_ring_off();
        lntn_sysled_ring_master_off();
    }
}

/******************************************************************************
       File Scope functions
******************************************************************************/
void erps_platform_set_erpg_ports (mesa_port_no_t east, mesa_port_no_t west, 
                                   u32 group_id)
{
    CRIT_ENTER(crit_p);
    port_to_mep[group_id].port[0] = east;
    port_to_mep[group_id].port[1] = west;
    CRIT_EXIT(crit_p);
}

void erps_platform_set_erpg_meps (u32 ccm_east, u32 ccm_west, u32 raps_east,
                                  u32 raps_west, u32 group_id)
{
    CRIT_ENTER(crit_p);
    port_to_mep[group_id].ccm_mep_id[0]  = ccm_east;
    port_to_mep[group_id].ccm_mep_id[1]  = ccm_west;
    port_to_mep[group_id].raps_mep_id[0] = raps_east;
    port_to_mep[group_id].raps_mep_id[1] = raps_west;
    CRIT_EXIT(crit_p);

}

void erps_platform_clear_port_to_mep (u32 group_id)
{
    CRIT_ENTER(crit_p);
    bzero(&port_to_mep[group_id], sizeof(erps_mep_t));
    CRIT_EXIT(crit_p);
}


static erps_mep_t vtss_get_erpg_to_mep (u32 group_id)
{
    erps_mep_t inst;
    CRIT_ENTER(crit_p);
    memcpy(&inst, &port_to_mep[group_id], sizeof(erps_mep_t));
    CRIT_EXIT(crit_p);
    return (inst);
}

const char *erps_error_text(mesa_rc error)
{
    switch(error) {
        case VTSS_APPL_ERPS_ERROR_VLAN_ALREADY_PARTOF_GROUP:
            return("VLAN is already a group member");
        case VTSS_APPL_ERPS_ERROR_INVALID_REMOTE_EVENT:
            return("Invalid remote event");
        case VTSS_APPL_ERPS_ERROR_INVALID_LOCAL_EVENT:
            return("Invalid local event");
        case VTSS_APPL_ERPS_ERROR_INCORRECT_ERPS_PDU_SIZE:
            return("Incorrect ERPS PDU size");
        case VTSS_APPL_ERPS_ERROR_RAPS_PARSING_FAILED:
            return("RAPS parsing error");
        case VTSS_APPL_ERPS_ERROR_FAILED_IN_SETTING_VLANMAP:
            return("Can not set VLAN map");
        case VTSS_APPL_ERPS_ERROR_MOVING_TO_NEXT_STATE:
            return("Can not change state");
        case VTSS_APPL_ERPS_ERROR_RAPS_ENABLE_FORWARDING_FAILED:
            return("Failed to enable RAPS forwarding");
        case VTSS_APPL_ERPS_ERROR_RAPS_DISABLE_FORWARDING_FAILED:
            return("Failed to disable RAPS forwarding");
        case VTSS_APPL_ERPS_ERROR_RAPS_DISABLE_RAPS_TX_FAILED:
            return("Failed to disable RAPS transmission");
        case VTSS_APPL_ERPS_ERROR_RAPS_ENABLE_RAPS_TX_FAILED:
            return("Failed to enable RAPS transmission");
        case VTSS_APPL_ERPS_ERROR_SF_DERIGISTER_FAILED:
            return("Could not de-register from SF");
        case VTSS_APPL_ERPS_ERROR_SF_RIGISTER_FAILED:
            return("Could not register for SF");
        case VTSS_APPL_ERPS_ERROR_MOVING_TO_FORWARDING:
            return("Could not enter forwarding state");
        case VTSS_APPL_ERPS_ERROR_MOVING_TO_DISCARDING:
            return("Could not enter discard state");
        case VTSS_APPL_ERPS_ERROR_CANTBE_IC_NODE:
            return("Can not be an interconnected node");
        case VTSS_APPL_ERPS_ERROR_NOT_AN_INTERCONNECTED_NODE:
            return("Not an interconnected node");
        case VTSS_APPL_ERPS_ERROR_CANNOTBE_INTERCONNECTED_SUB_NODE:
            return("Can not be an interconnected sub-node");
        case VTSS_APPL_ERPS_ERROR_PRIORITY_LOGIC_FAILED:
            return("Error in priority logic processing");
        case VTSS_APPL_ERPS_ERROR_PG_CREATION_FAILED:
            return("Protection group creation failed");
        case VTSS_APPL_ERPS_ERROR_GROUP_NOT_EXISTS:
            return("Given protection group does not exist");
        case VTSS_APPL_ERPS_ERROR_GROUP_ALREADY_EXISTS:
            return("Given protection group already created");
        case VTSS_APPL_ERPS_ERROR_INVALID_PGID:
            return("Invalid protection group ID");
        case VTSS_APPL_ERPS_ERROR_CANNOT_SET_RPL_OWNER_FOR_ACTIVE_GROUP:
            return("RPL owner can not be set when a group is active");
        case VTSS_APPL_ERPS_ERROR_NODE_ALREADY_RPL_OWNER:
            return("This node is RPL owner for given protection group");
        case VTSS_APPL_ERPS_ERROR_PG_IN_INACTIVE_STATE:
            return("Protection group not in active state");
        case VTSS_APPL_ERPS_ERROR_NODE_NON_RPL:
            return("This node is not RPL for given protection group");
        case VTSS_APPL_ERPS_ERROR_SETTING_RPL_BLOCK:
            return("Failed setting RPL block");
        case VTSS_APPL_ERPS_ERROR_VLANS_CANNOT_BE_ADDED:
            return("VLANs can not be added for this protection group");
        case VTSS_APPL_ERPS_ERROR_VLANS_CANNOT_BE_DELETED:
            return("VLANs can not be deleted for this protection group");
        case VTSS_APPL_ERPS_ERROR_CONTROL_VID_PART_OF_ANOTHER_GROUP:
            return("Control VLAN ID is part of another group");
        case VTSS_APPL_ERPS_ERROR_EAST_AND_WEST_ARE_SAME:
            return("Port 0 and port 1 are the same");
        case VTSS_APPL_ERPS_ERROR_CANNOT_ASSOCIATE_GROUP:
            return("Cannot update MEP association");
        case VTSS_APPL_ERPS_ERROR_ALREADY_NEIGHBOUR_FOR_THISGROUP:
            return("Node is configured as neighbour for given group, can not set as RPL");
        case VTSS_APPL_ERPS_ERROR_GROUP_IN_ACTIVE_MODE:
            return("Cannot configure when group is in active mode");
        case VTSS_APPL_ERPS_ERROR_GIVEN_PORT_NOT_EAST_OR_WEST:
            return("Given port is not configured either port 0 or 1 for this group");
        case VTSS_APPL_ERPS_ERROR:
            return("Generic error occurred");
        case VTSS_APPL_ERPS_ERROR_MAXIMUM_PVIDS_REACHED:
            return("Maximum number of VLANs already configured for protection group");
        default:
            return("ERPS: Unknown error code");
    }
}
/******************************************************************************
                              ERPS Callout Functions
******************************************************************************/
 /* VLAN Module Interface Functions */
mesa_rc vtss_erps_vlan_ingress_filter (mesa_port_no_t l2port, BOOL enable)
{
    mesa_port_no_t             switchport;
    vtss_isid_t                isid;
    vtss_appl_vlan_port_conf_t vlan_pconf;
    mesa_rc                    rc;

    if (l2port2port(API2L2PORT(l2port), &isid, &switchport)) {
        if (vlan_mgmt_port_conf_get(isid, switchport, &vlan_pconf, VTSS_APPL_VLAN_USER_ERPS, TRUE) != VTSS_RC_OK ) {
            return (VTSS_APPL_ERPS_ERROR);
        }

        T_NG(TRACE_GRP_CTRL, "VLAN:vlan_mgmt_port_conf_set -- l2port = %u enable = %d", l2port, enable);
        if (enable) {
            vlan_pconf.hybrid.flags = VTSS_APPL_VLAN_PORT_FLAGS_INGR_FILT;
        } else {
            vlan_pconf.hybrid.flags = 0;
        }

        vlan_pconf.hybrid.ingress_filter = enable;

        if ((rc = vlan_mgmt_port_conf_set(isid, switchport, &vlan_pconf, VTSS_APPL_VLAN_USER_ERPS)) != VTSS_RC_OK) {
            T_E("%u:%u: %s", isid, iport2uport(switchport), error_txt(rc));
        }
    } else {
        T_D("Set l2port %u VLAN filtering %d - not a port", l2port, enable);
    }
    return VTSS_RC_OK;
}

/* MEP Module Interface Functions */
/* function for sending out of R-APS Messages */
mesa_rc vtss_erps_raps_tx (mesa_port_no_t port, u32 eps_instance, u8 *aps, BOOL event)
{
    mesa_rc ret = VTSS_APPL_ERPS_ERROR;
    u32    mep_id;

    T_NG(TRACE_GRP_CTRL, "MEP:mep_tx_aps_info_set -- port = %u eps_instance = %u, event = %d", port, eps_instance, event);

    T_NG(TRACE_GRP_ERPS_TX, "MEP:mep_tx_aps_info_set -- port = %u eps_instance = %u", port, eps_instance);

    T_NG_HEX(TRACE_GRP_CTRL, aps, ERPS_PDU_SIZE);

    T_NG_HEX(TRACE_GRP_ERPS_TX, aps, ERPS_PDU_SIZE);


    CRIT_ENTER(crit_p);
    if (port_to_mep[eps_instance].port[ERPS_ARRAY_OFFSET_EAST] == port) {
        mep_id = CONV_MGMTTOERPS_INSTANCE(port_to_mep[eps_instance].raps_mep_id[ERPS_ARRAY_OFFSET_EAST]);

        if (mep_id >= VTSS_APPL_MEP_INSTANCE_MAX ) {
            CRIT_EXIT(crit_p);
            return (VTSS_RC_OK);
        }

        T_NG(TRACE_GRP_CTRL, "MEP:mep_tx_aps_info_set east mep_id = %u", mep_id);

        ret = mep_tx_aps_info_set (mep_id, eps_instance, aps, event);
        if (ret != VTSS_RC_OK) {
            T_D("failed to set tx for east raps_mep_id");
            CRIT_EXIT(crit_p);
            return (VTSS_APPL_ERPS_ERROR);
        }
    } else if (port_to_mep[eps_instance].port[ERPS_ARRAY_OFFSET_WEST] == port) {
        mep_id = CONV_MGMTTOERPS_INSTANCE(port_to_mep[eps_instance].raps_mep_id[ERPS_ARRAY_OFFSET_WEST]);
        if (mep_id >= VTSS_APPL_MEP_INSTANCE_MAX ) {
            CRIT_EXIT(crit_p);
            return (VTSS_RC_OK);
        }

        T_NG(TRACE_GRP_CTRL, "MEP:mep_tx_aps_info_set west mep_id = %u", mep_id);

        ret = mep_tx_aps_info_set (mep_id, eps_instance, aps, event);
        if (ret != VTSS_RC_OK) {
            T_D("failed to set tx for west raps_mep_id");
            CRIT_EXIT(crit_p);
            return (VTSS_APPL_ERPS_ERROR);
        }
    } else {
         T_D("unable to set aps for tx");
         CRIT_EXIT(crit_p);
         return (VTSS_APPL_ERPS_ERROR);
    }
    CRIT_EXIT(crit_p);
    return (VTSS_RC_OK);
}

/* function for enabling R-APS PDU forwarding to ERPS */
mesa_rc vtss_erps_mep_register (mesa_port_no_t port, u32 erps_instance, BOOL enable)
{
    mesa_rc ret = VTSS_APPL_ERPS_ERROR;
    u32     mep_id = 0;

    CRIT_ENTER(crit_p);
    if (port_to_mep[erps_instance].port[ERPS_ARRAY_OFFSET_EAST] == port) {            
        mep_id = CONV_MGMTTOERPS_INSTANCE(port_to_mep[erps_instance].raps_mep_id[ERPS_ARRAY_OFFSET_EAST]);
        if (mep_id >= VTSS_APPL_MEP_INSTANCE_MAX ) {
            CRIT_EXIT(crit_p);
            return VTSS_APPL_ERPS_ERROR_INVALID_PGID;
        }
        T_NG(TRACE_GRP_CTRL, "MEP:mep_eps_aps_register mep_id = %u erps_instance = %u enable = %d", mep_id, erps_instance, enable);
        ret = mep_eps_aps_register(mep_id, erps_instance, MEP_EPS_TYPE_ERPS, enable);
        T_NG(TRACE_GRP_CTRL, "MEP:mep_eps_aps_register return value = %d", ret);
    } else if (port_to_mep[erps_instance].port[ERPS_ARRAY_OFFSET_WEST] == port) {
        mep_id = CONV_MGMTTOERPS_INSTANCE(port_to_mep[erps_instance].raps_mep_id[ERPS_ARRAY_OFFSET_WEST]);
        if (mep_id >= VTSS_APPL_MEP_INSTANCE_MAX ) {
            CRIT_EXIT(crit_p);
            return VTSS_APPL_ERPS_ERROR_INVALID_PGID;
        }
        T_NG(TRACE_GRP_CTRL, "MEP:mep_eps_aps_register mep_id = %u erps_instance = %u enable = %d", mep_id, erps_instance, enable);
        ret = mep_eps_aps_register(mep_id, erps_instance, MEP_EPS_TYPE_ERPS, enable);
        T_NG(TRACE_GRP_CTRL, "MEP:mep_eps_aps_register return value = %d", ret );
    }
    CRIT_EXIT(crit_p);
    return (ret);
}

mesa_rc vtss_erps_raps_forwarding ( mesa_port_no_t port,
                                    u32 erps_instance,
                                    BOOL enable )
{
    u32 mep_id = 0;
    u32 ret; 

    CRIT_ENTER(crit_p); 
    if (port_to_mep[erps_instance].port[ERPS_ARRAY_OFFSET_EAST] == port) {
        mep_id = CONV_MGMTTOERPS_INSTANCE(port_to_mep[erps_instance].raps_mep_id[ERPS_ARRAY_OFFSET_EAST]);
    } else if (port_to_mep[erps_instance].port[ERPS_ARRAY_OFFSET_WEST] == port) {
        mep_id = CONV_MGMTTOERPS_INSTANCE(port_to_mep[erps_instance].raps_mep_id[ERPS_ARRAY_OFFSET_WEST]);
    }
    CRIT_EXIT(crit_p); 

    if (mep_id >= VTSS_APPL_MEP_INSTANCE_MAX ) {
        T_DG(TRACE_GRP_CTRL, "MEP: mep_raps_forwarding returning error VTSS_APPL_MEP_INSTANCE_MAX");
        return (VTSS_RC_OK);
    }

    ret = mep_raps_forwarding(mep_id, erps_instance, enable);

    T_NG(TRACE_GRP_CTRL, "MEP: mep_raps_forwarding mep_id = %u erps_instance = %u enable = %d => rc = %d", mep_id, erps_instance, enable, ret);

    return (ret);
}

/* function for registering for SF signals from MEP */
mesa_rc vtss_erps_mep_sf_register (mesa_port_no_t port, u32 erps_instance,
                                   BOOL enable)
{
    mesa_rc ret;

    u32 mep_id = 0;

    CRIT_ENTER(crit_p);
    if (port_to_mep[erps_instance].port[ERPS_ARRAY_OFFSET_EAST] == port) {
        mep_id   = CONV_MGMTTOERPS_INSTANCE(port_to_mep[erps_instance].ccm_mep_id[ERPS_ARRAY_OFFSET_EAST]);
    } else if (port_to_mep[erps_instance].port[ERPS_ARRAY_OFFSET_WEST] == port) {
        mep_id   = CONV_MGMTTOERPS_INSTANCE(port_to_mep[erps_instance].ccm_mep_id[ERPS_ARRAY_OFFSET_WEST]);
    }
    CRIT_EXIT(crit_p);

    if (mep_id >= VTSS_APPL_MEP_INSTANCE_MAX) {
        T_DG(TRACE_GRP_CTRL, "MEP %d > %u - failing", mep_id, VTSS_APPL_MEP_INSTANCE_MAX);
        return (VTSS_RC_OK);
    }

    ret = mep_eps_sf_register(mep_id,   erps_instance, MEP_EPS_TYPE_ERPS, enable);

    T_NG(TRACE_GRP_CTRL, "MEP: mep_eps_sf_register mep_id = %u  erps_instance = %u  enable = %d  rc = %d", mep_id, erps_instance, enable, ret);

    return (ret != VTSS_RC_OK) ? VTSS_APPL_ERPS_ERROR : VTSS_APPL_ERPS_RC_OK;
}

mesa_rc vtss_erps_raps_transmission ( mesa_port_no_t port, 
                                          u32 erps_instance, BOOL enable )
{
    u32 raps_mep = 0;
    u32 ret;

    CRIT_ENTER(crit_p);
    if (port_to_mep[erps_instance].port[ERPS_ARRAY_OFFSET_EAST] == port) {
        raps_mep = CONV_MGMTTOERPS_INSTANCE(port_to_mep[erps_instance].raps_mep_id[ERPS_ARRAY_OFFSET_EAST]);
    } else if (port_to_mep[erps_instance].port[ERPS_ARRAY_OFFSET_WEST] == port) {
        raps_mep = CONV_MGMTTOERPS_INSTANCE(port_to_mep[erps_instance].raps_mep_id[ERPS_ARRAY_OFFSET_WEST]);
    }
    CRIT_EXIT(crit_p);

    if (raps_mep >= VTSS_APPL_MEP_INSTANCE_MAX) {
        T_DG(TRACE_GRP_CTRL, "RAPS MEP %d > %u - failing", raps_mep, VTSS_APPL_MEP_INSTANCE_MAX);
        return (VTSS_RC_OK);
    }

    ret = mep_raps_transmission(raps_mep, erps_instance, enable);

    T_NG(TRACE_GRP_CTRL, "MEP: vtss_erps_mep_raps_transmission raps_mep = %u erps_instance = %u enable = %d => rc = %d", raps_mep, erps_instance, enable, ret);
    return(ret);
}

mesa_rc erps_mep_aps_sf_register (mesa_port_no_t east_port, mesa_port_no_t west_port, 
                                  u32 erps_instance, BOOL enable, BOOL raps_virt_channel)
{
    mesa_rc  rc = VTSS_RC_OK;
    i32      ret;

    /* register with mep for RAPS */
    ret = vtss_erps_mep_register(east_port, erps_instance, enable);
    if (ret != VTSS_RC_OK) {
        rc = VTSS_APPL_ERPS_ERROR;
        T_D("vtss_erps_mep_register failed for group = %u", erps_instance);
    }
    if (west_port || raps_virt_channel) {
        ret = vtss_erps_mep_register(west_port, erps_instance, enable);
        if (ret != VTSS_RC_OK) {
            rc = VTSS_APPL_ERPS_ERROR;
            T_D("vtss_erps_mep_register failed for group = %u", erps_instance);
        }
    }

    /* need to register for SF also */
    ret = vtss_erps_mep_sf_register(east_port, erps_instance, enable);
    if (ret != VTSS_RC_OK) {
        rc = VTSS_APPL_ERPS_ERROR;
        T_D("vtss_erps_mep_sf_register failed for group = %u", erps_instance);
    }
    if (west_port || raps_virt_channel) {
        ret = vtss_erps_mep_sf_register(west_port, erps_instance, enable);
        if (ret != VTSS_RC_OK) {
            rc = VTSS_APPL_ERPS_ERROR;
            T_D("vtss_erps_mep_sf_register failed for group = %u", erps_instance);
        }
    }
    return rc;
}

mesa_rc erps_sf_sd_state_set(const u32 instance, const u32 mep_instance,
                             const BOOL sf_state, const BOOL sd_state)
{
    u32 mep_id, wmep, emep;
    mesa_port_no_t e_port, w_port;
    mesa_rc rc = VTSS_APPL_ERPS_ERROR;
    mep_id = CONV_ERPSTOMGMT_INSTANCE(mep_instance);
    vtss_appl_mep_state_t state;

    if (mep_id > VTSS_APPL_MEP_INSTANCE_MAX ) {
        return (VTSS_RC_OK);
    }

    CRIT_ENTER(crit_p);
    wmep = port_to_mep[instance].ccm_mep_id[ERPS_ARRAY_OFFSET_WEST];
    emep = port_to_mep[instance].ccm_mep_id[ERPS_ARRAY_OFFSET_EAST];
    e_port = port_to_mep[instance].port[ERPS_ARRAY_OFFSET_EAST];
    w_port = port_to_mep[instance].port[ERPS_ARRAY_OFFSET_WEST];
    CRIT_EXIT(crit_p);

    // Need to lock the crit (crit_fsm) and get the newest MEP status because it might have changed by another thread,
    // i.e. we ignore the incoming states (BZ23778).
    CRIT_ENTER(crit_fsm);
    if (vtss_appl_mep_instance_status_get(mep_instance, &state) != VTSS_RC_OK) {
        T_D("Failed getting Mep state info (vtss_appl_mep_instance_status_get)");
        CRIT_EXIT(crit_fsm);
        return (VTSS_APPL_ERPS_ERROR);
    }

    T_IG(TRACE_GRP_CTRL, "MEP: erps_sf_sd_state_set wmep = %u emep = %u inst = %u sf = %d sd = %d", wmep, emep, instance, state.aTsf, state.aTsd);

    if (emep == mep_id) {
        rc  = (vtss_erps_sf_sd_state_set(instance, state.aTsf, state.aTsd, e_port) == VTSS_RC_OK) ? VTSS_APPL_ERPS_RC_OK : VTSS_APPL_ERPS_ERROR;
    } else if (wmep == mep_id) {
        rc = (vtss_erps_sf_sd_state_set(instance, state.aTsf, state.aTsd, w_port) == VTSS_RC_OK) ? VTSS_APPL_ERPS_RC_OK : VTSS_APPL_ERPS_ERROR;
    }

    CRIT_EXIT(crit_fsm);
    return (VTSS_APPL_ERPS_ERROR);
}

void erps_instance_signal_in (u32 erps_instance)
{
    u32 mep_id;
    mesa_rc ret = VTSS_APPL_ERPS_ERROR;
  
    CRIT_ENTER(crit_p);
    mep_id = CONV_MGMTTOERPS_INSTANCE(port_to_mep[erps_instance].raps_mep_id[ERPS_ARRAY_OFFSET_EAST]);
    CRIT_EXIT(crit_p);

    T_NG(TRACE_GRP_CTRL, "MEP: mep_signal_in erps_instance = %u mep_id = %u", erps_instance, mep_id);

    if (mep_id >= VTSS_APPL_MEP_INSTANCE_MAX ) {
        return;
    }
    ret = mep_signal_in(mep_id, erps_instance);

    if (ret != VTSS_RC_OK) {
        T_D("failed in registering with MEP for raps east");
    }

    CRIT_ENTER(crit_p);

    mep_id = CONV_MGMTTOERPS_INSTANCE(port_to_mep[erps_instance].raps_mep_id[ERPS_ARRAY_OFFSET_WEST]);
    CRIT_EXIT(crit_p);
    if (mep_id >= VTSS_APPL_MEP_INSTANCE_MAX ) {
        return;
    }
    ret = mep_signal_in(mep_id, erps_instance);

    if (ret != VTSS_RC_OK) {
        T_D("failed in registering with MEP for raps west");
    }

    CRIT_ENTER(crit_p);
    mep_id = CONV_MGMTTOERPS_INSTANCE(port_to_mep[erps_instance].ccm_mep_id[ERPS_ARRAY_OFFSET_EAST]);
    CRIT_EXIT(crit_p);
    if (mep_id >= VTSS_APPL_MEP_INSTANCE_MAX ) {
        return;
    }
    ret = mep_signal_in(mep_id, erps_instance);

    if (ret != VTSS_RC_OK) {
        T_D("failed in registering with MEP for ccm east");
    }

    CRIT_ENTER(crit_p);
    mep_id = CONV_MGMTTOERPS_INSTANCE(port_to_mep[erps_instance].ccm_mep_id[ERPS_ARRAY_OFFSET_WEST]);
    CRIT_EXIT(crit_p);
    if (mep_id >= VTSS_APPL_MEP_INSTANCE_MAX ) {
        return;
    }
    ret = mep_signal_in(mep_id, erps_instance);

    if (ret != VTSS_RC_OK) {
        T_D("failed in registering with MEP for ccm west");
    }
}

                            
/* Chip Platform Interface */
mesa_rc vtss_erps_ctrl_set_vlanmap (mesa_vid_t vid, mesa_erpi_t inst, BOOL member)
{
    T_NG(TRACE_GRP_SWAPI, "SWAPI: mesa_erps_vlan_member_set erpi inst = %u vid = %d ", inst, vid);

    if (mesa_erps_vlan_member_set(NULL, API2ERPS_HWINSTANCE(inst), vid, member) != VTSS_RC_OK) {
        T_D("setting vtss_erps_vlan_erpi_set failed mesa_erpi_t = %d ", inst );
        return (VTSS_APPL_ERPS_ERROR);
    }
 
    return (VTSS_RC_OK);
}

mesa_rc vtss_erps_protection_group_state_set ( mesa_erpi_t inst, 
                                               mesa_port_no_t port,
                                               mesa_erps_state_t state )
{
    mesa_rc ret = VTSS_APPL_ERPS_ERROR;

    T_NG(TRACE_GRP_SWAPI, "SWAPI: mesa_erps_port_state_set erpi inst = %u port = %u (API port:%u)  state = %s",
         inst, port, API2L2PORT(port), state == MESA_ERPS_STATE_FORWARDING ? "FWD" : "DISC");

    ret = mesa_erps_port_state_set(NULL, API2ERPS_HWINSTANCE(inst), API2L2PORT(port), state);
    mep_ring_protection_block(API2L2PORT(port),  (state == MESA_ERPS_STATE_DISCARDING));
    if ( ret != VTSS_RC_OK) {
        T_D("error in setting protection group state = %d ", inst );
        return (VTSS_APPL_ERPS_ERROR);
    }
    return (VTSS_RC_OK);
}

mesa_rc vtss_erps_put_protected_vlans_in_forwarding (mesa_erpi_t inst, mesa_port_no_t rplport)
{
    mesa_port_no_t     iport, port_count;
    mesa_rc ret = VTSS_APPL_ERPS_ERROR;
    port_count = MESA_CAP(MEBA_CAP_BOARD_PORT_COUNT);
    T_NG(TRACE_GRP_SWAPI, "SWAPI: mesa_erps_port_state_set erpi inst = %u port = %u  state = %d", inst, rplport, MESA_ERPS_STATE_FORWARDING);
    for (iport = VTSS_PORT_NO_START; iport < port_count; iport++) {
        if (iport == API2L2PORT(rplport)){
            continue;
        }
        ret = mesa_erps_port_state_set(NULL, API2ERPS_HWINSTANCE(inst), iport, MESA_ERPS_STATE_FORWARDING);
        if (ret != VTSS_RC_OK) {
            T_D("error in putting all port+vlan combination in forwarding state");
            return (VTSS_APPL_ERPS_ERROR);
        }
    }
    return (VTSS_RC_OK);
}

mesa_rc vtss_erps_flush_fdb (mesa_port_no_t port, mesa_vid_t vid)
{
    T_NG(TRACE_GRP_SWAPI, "SWAPI:mesa_mac_table_vlan_flush port:%d vid = %d", API2L2PORT(port), vid);

    return(mesa_mac_table_vlan_port_flush(NULL, API2L2PORT(port), vid));
}

mesa_rc vtss_erps_get_port_mac (u32 port, uchar mac[6])
{
    if (conf_mgmt_mac_addr_get(mac, port) < 0) {
        T_D("Error getting own MAC:");
        return (VTSS_APPL_ERPS_ERROR);
    }
    return (VTSS_RC_OK);
}

void vtss_erps_trace(const char  *const string, const u32   param)
{
    T_D("%s %u", string, param) ;
}

void vtss_erps_trace_state_change (const char *const string, const u32 parm)
{
    T_NG(TRACE_GRP_STATE_CHANGE, "%s  %u", string, parm);
}

u64 vtss_erps_current_time(void)
{
    return vtss_current_time();
}

BOOL vtss_erps_is_initialized(void)
{
    return erps_init_done;
}

// Check if *management* MEP ID is valid range (mgmt. MEP ID starts at 1)
static BOOL erps_mgmt_mepid_is_valid_range(u32 mep_id)
{
    return (mep_id > 0) && (mep_id <= VTSS_APPL_MEP_INSTANCE_MAX);
}

/****************************************************************************/
// LNTN functions
/****************************************************************************/
typedef enum {
    LNTN_RING_LED_MOD_OFF = 0,
    LNTN_RING_LED_MOD_SOLID,
    LNTN_RING_LED_MOD_BLINK,
    LNTN_RING_LED_MOD_MAX
} lntn_ring_led_mod_t;

void lntn_erps_led_notify(void)
{
    vtss_erps_base_conf_t      erpg;
    uint                       actived = 0;
    uint                       fails = 0;
    uint                       rpl_owner = 0;
    uint                       group_id = 0;
    static lntn_ring_led_mod_t mode = LNTN_RING_LED_MOD_OFF;

    memset(&erpg, 0, sizeof(vtss_erps_base_conf_t));

    while (!vtss_erps_getnext_protection_group_by_id(&erpg)) {
        T_D("group_id[%d], state[%d], rpl_owner[%d]", erpg.group_id, erpg.stats.state, erpg.erpg.rpl_owner);
        actived++;
        if (erpg.stats.state == ERPS_STATE_PROTECTED) {
            fails++;
        }

        if (erpg.erpg.rpl_owner == ERPS_NODE_RPL_OWNER) {
            rpl_owner++;
            break;
        }
        group_id = erpg.group_id + 1;
        memset(&erpg, 0, sizeof(vtss_erps_base_conf_t));
        erpg.group_id = group_id;
    }

    /* ring master LED */
    if (rpl_owner) {
        lntn_sysled_ring_master_solid_green();
    } else {
        lntn_sysled_ring_master_off();
    }

    /* ring LED */
    if (actived) {
        if (fails) {
            if (mode != LNTN_RING_LED_MOD_BLINK) {
                T_D("Change Ring LED to blink mode");
                mode = LNTN_RING_LED_MOD_BLINK;
                lntn_sysled_ring_fast_blink_green();
            }
        } else if (mode != LNTN_RING_LED_MOD_SOLID){
            T_D("Change Ring LED to solid mode");
            mode = LNTN_RING_LED_MOD_SOLID;
            lntn_sysled_ring_solid_green();
        }
    } else if (mode != LNTN_RING_LED_MOD_OFF) {
        T_D("Change Ring LED to off mode");
        mode = LNTN_RING_LED_MOD_OFF;
        lntn_sysled_ring_off();
    }
}

/******************************************************************************
                              ERPS Interface 
******************************************************************************/
/* this function gets called from MEP module soon an R-APS PDU received */
void erps_handover_raps_pdu(const u8 *pdu, u32 mep_id, u8 len, u32 erpg)
{
    raps_pdu_in_t   *req = (raps_pdu_in_t *)VTSS_MALLOC(sizeof(raps_pdu_in_t));

    if ( req != NULL ) {
        memcpy(req->pdu, pdu, len);
        req->len    = len;
        req->mep_id = mep_id;
        req->erpg = erpg;
        if (!erps_queue.vtss_safe_queue_put(req)) {
            T_D("error in writing into safe queue"); 
        }
    }
}

// Utility: Move active group to reserved state, i.e. release resources but stay (largely) configured
static mesa_rc erps_move_to_reserved(u32 group_id)
{
    mesa_rc               ret = VTSS_APPL_ERPS_RC_OK;
    vtss_erps_base_conf_t pconf;
    u8                    grp_status;

    if ((ret = vtss_erps_get_protection_group_status(group_id, &grp_status)) != VTSS_RC_OK) {
        return ret;
    }

    if (grp_status == ERPS_PROTECTION_GROUP_RESERVED) {
        return VTSS_APPL_ERPS_RC_OK;
    }

    memset(&pconf, 0, sizeof(pconf));
    pconf.group_id = group_id;
    if ((ret = vtss_erps_get_protection_group_by_id(&pconf)) != VTSS_RC_OK) {
        return ret;
    }


    if (vtss_erps_raps_forwarding(pconf.erpg.east_port, pconf.group_id, FALSE) != VTSS_RC_OK) {
        T_D("vtss_erps_raps_forwarding failed for group = %u", pconf.group_id);
        ret = VTSS_APPL_ERPS_ERROR;
    }

    if (pconf.erpg.west_port || pconf.erpg.virtual_channel) {
        if (vtss_erps_raps_forwarding(pconf.erpg.west_port, pconf.group_id, FALSE) != VTSS_RC_OK) {
            T_D("vtss_erps_raps_forwarding failed for group = %u", pconf.group_id);
            ret = VTSS_APPL_ERPS_ERROR;
        }
    }

    if (vtss_erps_raps_transmission(pconf.erpg.east_port, pconf.group_id, FALSE) != VTSS_RC_OK) {
        T_D("vtss_erps_raps_transmission failed for group = %u", pconf.group_id);
        ret = VTSS_APPL_ERPS_ERROR;
    }

    if (pconf.erpg.west_port || pconf.erpg.virtual_channel) {
        if (vtss_erps_raps_transmission(pconf.erpg.west_port, pconf.group_id, FALSE) != VTSS_RC_OK) {
            T_D("vtss_erps_raps_transmission failed for group = %u", pconf.group_id);
            ret = VTSS_APPL_ERPS_ERROR;
        }
    }

    if (erps_mep_aps_sf_register(pconf.erpg.east_port, pconf.erpg.west_port, pconf.group_id, FALSE, pconf.erpg.virtual_channel) != VTSS_RC_OK) {
        T_D("vtss_erps_mep_aps_sf_register failed for group = %u", pconf.group_id);
        ret = VTSS_APPL_ERPS_ERROR;
    }

    if (vtss_erps_move_protection_group_to_reserved(group_id) != VTSS_RC_OK) {
        T_D("vtss_erps_move_protection_group_to_reserved failed for group = %u", pconf.group_id);
        ret = VTSS_APPL_ERPS_ERROR;
    }

    return ret;
}

/******************************************************************************
                              MGMT Interface 
******************************************************************************/
mesa_rc erps_mgmt_set_protection_group_request (const vtss_erps_mgmt_conf_t *mgmt_req)
{
    mesa_rc                ret = VTSS_APPL_ERPS_RC_OK;
    vtss_erps_base_conf_t  pconf;
    u8                     grp_status;
    u32                    group_id = CONV_MGMTTOERPS_INSTANCE(mgmt_req->group_id);

    switch (mgmt_req->req_type) {
        case ERPS_CMD_PROTECTION_GROUP_ADD:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_create_protection_group group_id = %u east_port = %u west_port = %u",
                 group_id, mgmt_req->data.create.east_port, mgmt_req->data.create.west_port);
            ret = vtss_erps_create_protection_group(group_id,
                                               mgmt_req->data.create.east_port,
                                               mgmt_req->data.create.west_port,
                                               mgmt_req->data.create.ring_type,
                                               mgmt_req->data.create.interconnected,
                                               mgmt_req->data.create.major_ring_id,
                                               mgmt_req->data.create.virtual_channel);
            if (ret == VTSS_APPL_ERPS_RC_OK) {
                /* update port numbers in port_to_mep global strcture */
                erps_platform_set_erpg_ports(mgmt_req->data.create.east_port,
                                             mgmt_req->data.create.west_port,
                                             group_id);
            }
            break;
        case ERPS_CMD_PROTECTION_GROUP_DELETE:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_delete_protection_group group_id = %u", group_id);
            if ((ret = vtss_erps_get_protection_group_status(group_id, &grp_status)) == VTSS_RC_OK) {
                if (grp_status != ERPS_PROTECTION_GROUP_INACTIVE) {
                    (void) erps_move_to_reserved(group_id);
                    if ((ret = vtss_erps_delete_protection_group (group_id)) == VTSS_APPL_ERPS_RC_OK) {
                        erps_platform_clear_port_to_mep(group_id);
                    }
                }
            }
            break;
        case ERPS_CMD_HOLD_OFF_TIMER:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_set_holdoff_timeout group_id = %u hold timeout = " VPRI64u"", group_id, mgmt_req->data.timer.time);
            ret = vtss_erps_set_holdoff_timeout (group_id, mgmt_req->data.timer.time);
            break;
        case ERPS_CMD_WTR_TIMER:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_set_wtr_timeout group_id = %u wtr timeout = " VPRI64u"", group_id, mgmt_req->data.timer.time);
            ret = vtss_erps_set_wtr_timeout (group_id, mgmt_req->data.timer.time);
            break;
        case ERPS_CMD_GUARD_TIMER:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_set_guard_timeout group_id = %u guard timeout = " VPRI64u"", group_id, mgmt_req->data.timer.time);
            ret = vtss_erps_set_guard_timeout (group_id, mgmt_req->data.timer.time);
            break;
        case ERPS_CMD_ADD_VLANS:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_associate_protected_vlan group_id = %u vid = %d", group_id, mgmt_req->data.vid.p_vid);
            ret = vtss_erps_associate_protected_vlans (group_id, mgmt_req->data.vid.p_vid);
            break;
        case ERPS_CMD_DEL_VLANS:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_remove_protected_vlan group_id = %u vid = %d", group_id, mgmt_req->data.vid.p_vid);
            ret = vtss_erps_remove_protected_vlan (group_id, mgmt_req->data.vid.num_vids, mgmt_req->data.vid.p_vid);
            break;
        case ERPS_CMD_SET_RPL_BLOCK:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_set_rpl_block group_id = %u block port = %u ", group_id, mgmt_req->data.rpl_block.rpl_port);
            ret = vtss_erps_set_rpl_owner (group_id, mgmt_req->data.rpl_block.rpl_port, FALSE );
            break;
        case ERPS_CMD_UNSET_RPL_BLOCK:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_unset_rpl_owner group_id = %u ", group_id);
            ret = vtss_erps_unset_rpl_owner (group_id);
            break;
        case ERPS_CMD_ADD_MEP_ASSOCIATION:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_associate_group group_id = %u eastmep = %u westmep = %u east rapsmep = %u west rapsmep = %u",
                 group_id, mgmt_req->data.mep.east_mep_id, mgmt_req->data.mep.west_mep_id, mgmt_req->data.mep.raps_eastmep, mgmt_req->data.mep.raps_westmep);

            if ((ret = vtss_erps_get_protection_group_status(group_id, &grp_status)) == VTSS_RC_OK) {
                if (grp_status == ERPS_PROTECTION_GROUP_INACTIVE) {
                    ret = VTSS_APPL_ERPS_ERROR_GROUP_NOT_EXISTS;
                } else {
                    memset(&pconf, 0, sizeof(pconf));
                    pconf.group_id = group_id;
                    if ((ret = vtss_erps_get_protection_group_by_id(&pconf)) == VTSS_RC_OK) {
                        /* Valid combinations:
                         *   East   West   East   West   East   West
                         *   P0     P1     P0-APS P1-APS P0-SF  P1-SF  Type   Interc VirtCh Maj.Id
                         *   ------ ------ ------ ------ ------ ------ ------ ------ ------ ------
                         *   !=0    !=0    !=0    !=0    !=0    !=0    Major  T/F    n/a    n/a     Major
                         *   !=0    !=0    !=0    !=0    !=0    !=0    Sub    F      T      n/a     Sub, not interconn, w. virt. ch.
                         *   !=0    !=0    !=0    !=0    !=0    !=0    Sub    F      F      n/a     Sub, not interconn, w/o virt. ch.
                         *   !=0    0      !=0    !=0    !=0    0      Sub    T      T      !=0     Sub, interconn w. virt. ch.
                         *   !=0    0      !=0    0      !=0    0      Sub    T      F      !=0     Sub, interconn w/o virt. ch.
                         */

                        const BOOL       sub_interconn = pconf.erpg.ring_type == ERPS_RING_TYPE_SUB && pconf.erpg.inter_connected_node;
                        const BOOL       virt_chan     = pconf.erpg.virtual_channel;
                        const erps_mep_t current_meps  = vtss_get_erpg_to_mep(group_id);
                        const BOOL       meps_changed  = mgmt_req->data.mep.east_mep_id  != current_meps.ccm_mep_id[ERPS_ARRAY_OFFSET_EAST]  ||
                                                         mgmt_req->data.mep.west_mep_id  != current_meps.ccm_mep_id[ERPS_ARRAY_OFFSET_WEST]  ||
                                                         mgmt_req->data.mep.raps_eastmep != current_meps.raps_mep_id[ERPS_ARRAY_OFFSET_EAST] ||
                                                         mgmt_req->data.mep.raps_westmep != current_meps.raps_mep_id[ERPS_ARRAY_OFFSET_WEST];
   
                        BOOL meps_ok = erps_mgmt_mepid_is_valid_range(mgmt_req->data.mep.east_mep_id) &&
                                       erps_mgmt_mepid_is_valid_range(mgmt_req->data.mep.raps_eastmep);

                        if (sub_interconn) {
                            meps_ok = meps_ok && mgmt_req->data.mep.west_mep_id == 0;
                        } else {
                            meps_ok = meps_ok && erps_mgmt_mepid_is_valid_range(mgmt_req->data.mep.west_mep_id);
                        }

                        if (sub_interconn && !virt_chan) {
                            meps_ok = meps_ok && mgmt_req->data.mep.raps_westmep == 0;
                        } else {
                            meps_ok = meps_ok && erps_mgmt_mepid_is_valid_range(mgmt_req->data.mep.raps_westmep);
                        }

                        if (!meps_ok) {
                            T_D("Invalid MEP ID(s), cannot associate");
                            ret = VTSS_APPL_ERPS_ERROR_CANNOT_ASSOCIATE_GROUP;
                        } else if (meps_changed) {
                            if ((ret = erps_move_to_reserved(group_id)) != VTSS_RC_OK) {
                                T_D("erps_move_to_reserved failed for group = %u", pconf.group_id);
                            }
                            erps_platform_set_erpg_meps(mgmt_req->data.mep.east_mep_id, mgmt_req->data.mep.west_mep_id, mgmt_req->data.mep.raps_eastmep, mgmt_req->data.mep.raps_westmep, group_id);
                            if ((ret = erps_mep_aps_sf_register(pconf.erpg.east_port, pconf.erpg.west_port, pconf.group_id, TRUE, pconf.erpg.virtual_channel)) != VTSS_RC_OK) {
                                T_D("vtss_erps_mep_aps_sf_register failed for group = %u", pconf.group_id);
                            }
                            if ((ret = vtss_erps_associate_group(group_id)) == VTSS_RC_OK) {
                                /* register with MEP for incoming signal */
                                erps_instance_signal_in(group_id);
                            }
                        }
                    }
                }
            }
            break;
        case ERPS_CMD_DEL_MEP_ASSOCIATION:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_associate_group group_id = %u eastmep = %u westmep = %u east rapsmep = %u west rapsmep = %u",
                group_id, mgmt_req->data.mep.east_mep_id, mgmt_req->data.mep.west_mep_id,
                mgmt_req->data.mep.raps_eastmep, mgmt_req->data.mep.raps_westmep);
            if (vtss_erps_get_protection_group_status(group_id, &grp_status) == VTSS_RC_OK) {
                if (grp_status == ERPS_PROTECTION_GROUP_INACTIVE) {
                    ret = VTSS_APPL_ERPS_ERROR_GROUP_NOT_EXISTS;
                } else {
                    ret = erps_move_to_reserved(group_id);
                    erps_platform_set_erpg_meps(0, 0, 0, 0, group_id);
                }
            }
            break;
        case ERPS_CMD_SET_RPL_NEIGHBOUR:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_set_rpl_neighbour group_id = %u neighbour port = %u ", \
                                  group_id, mgmt_req->data.rpl_block.rpl_port);
            ret = vtss_erps_set_rpl_neighbour(group_id,
                                              mgmt_req->data.rpl_block.rpl_port);
            break;
        case ERPS_CMD_UNSET_RPL_NEIGHBOUR:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_unset_rpl_neighbour group_id = %u", group_id);
            ret = vtss_erps_unset_rpl_neighbour(group_id);
            break;
        case ERPS_CMD_REPLACE_RPL_BLOCK:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_set_rpl_block group_id = %u neighbour port = %u ", \
                                  group_id, mgmt_req->data.rpl_block.rpl_port);
            ret = vtss_erps_set_rpl_owner (group_id,
                                           mgmt_req->data.rpl_block.rpl_port, TRUE );
            break;
        case ERPS_CMD_ENABLE_INTERCONNECTED_NODE:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_set_rpl_block group_id = %u major_ring_id = %u ", \
                                  group_id, mgmt_req->data.inter_connected.major_ring_id);
            ret = vtss_erps_enable_interconnected(group_id,
                                                   CONV_MGMTTOERPS_INSTANCE(mgmt_req->data.inter_connected.major_ring_id));
            break;
        case ERPS_CMD_DISABLE_INTERCONNECTED_NODE:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_set_rpl_block group_id = %u major_ring_id = %u ", \
                                  group_id, mgmt_req->data.inter_connected.major_ring_id);
            ret = vtss_erps_disable_interconnected(group_id,
                                                   CONV_MGMTTOERPS_INSTANCE(mgmt_req->data.inter_connected.major_ring_id));
            break;
        case ERPS_CMD_ENABLE_NON_REVERTIVE:
            ret = vtss_erps_enable_non_reversion(group_id);
            break;
        case ERPS_CMD_DISABLE_NON_REVERTIVE:
            ret = vtss_erps_disable_non_reversion(group_id);
            break;
        case ERPS_CMD_FORCED_SWITCH:
            ret = vtss_erps_admin_command(group_id, ERPS_ADMIN_CMD_FORCED_SWITCH,
                                          mgmt_req->data.create.east_port);
            break;
        case ERPS_CMD_MANUAL_SWITCH:
            ret = vtss_erps_admin_command(group_id, ERPS_ADMIN_CMD_MANUAL_SWITCH,
                                          mgmt_req->data.create.east_port);
            break;
        case ERPS_CMD_CLEAR:
            ret = vtss_erps_admin_command(group_id, ERPS_ADMIN_CMD_CLEAR, 0);
            break;
        case ERPS_CMD_TOPOLOGY_CHANGE_PROPAGATE:
            ret = vtss_erps_set_topology_change_propogation(group_id, TRUE);
            break;
        case ERPS_CMD_TOPOLOGY_CHANGE_NO_PROPAGATE:
            ret = vtss_erps_set_topology_change_propogation(group_id, FALSE);
            break;
        case ERPS_CMD_SUB_RING_WITH_VIRTUAL_CHANNEL:
            ret = vtss_erps_enable_virtual_channel(group_id);
            break;
        case ERPS_CMD_SUB_RING_WITHOUT_VIRTUAL_CHANNEL:
            ret = vtss_erps_disable_virtual_channel(group_id);
            break;
        case ERPS_CMD_ENABLE_VERSION_1_COMPATIBLE:
            ret = vtss_erps_set_protocol_version(group_id, ERPS_VERSION_V1);
            break;
        case ERPS_CMD_DISABLE_VERSION_1_COMPATIBLE:
            ret = vtss_erps_set_protocol_version(group_id, ERPS_VERSION_V2);
            break;
        case ERPS_CMD_CLEAR_STATISTICS:
            T_NG(TRACE_GRP_MGMT, "vtss_erps_clear_statistics group_id = %u", group_id);
            ret = vtss_erps_clear_statistics(group_id);
            break;
        default:
            break;
    }

    lntn_erps_led_notify();
    return ret;
}

mesa_rc erps_mgmt_getnext_protection_group_request (vtss_erps_mgmt_conf_t *mgmt_req)
{
   mesa_rc ret = VTSS_APPL_ERPS_ERROR;
   vtss_erps_base_conf_t  base_conf;
   erps_mep_t  ermep;

   memset(&base_conf, 0, sizeof(base_conf));
//   ERPS_CRITD_ENTER();
   base_conf.group_id = mgmt_req->group_id;
   ret = vtss_erps_getnext_protection_group_by_id(&base_conf);
   mgmt_req->group_id = base_conf.group_id;
   memcpy(&mgmt_req->data.get.erpg, &base_conf.erpg, sizeof(vtss_erps_config_erpg_t));
   memcpy(&mgmt_req->data.get.stats, &base_conf.stats, sizeof(vtss_erps_fsm_stat_t));
   memcpy(&mgmt_req->data.get.raps_stats, &base_conf.raps_stats, sizeof(vtss_erps_statistics_t));
//   ERPS_CRITD_EXIT();

   ermep = vtss_get_erpg_to_mep(mgmt_req->group_id);

   mgmt_req->group_id = CONV_ERPSTOMGMT_INSTANCE(mgmt_req->group_id);
   mgmt_req->data.get.erpg.group_id = CONV_ERPSTOMGMT_INSTANCE (mgmt_req->data.get.erpg.group_id);

   mgmt_req->data.mep.east_mep_id = ermep.ccm_mep_id[ERPS_ARRAY_OFFSET_EAST];
   mgmt_req->data.mep.west_mep_id = ermep.ccm_mep_id[ERPS_ARRAY_OFFSET_WEST];
   mgmt_req->data.mep.raps_eastmep = ermep.raps_mep_id[ERPS_ARRAY_OFFSET_EAST];
   mgmt_req->data.mep.raps_westmep = ermep.raps_mep_id[ERPS_ARRAY_OFFSET_WEST];

   return (ret);
}

mesa_rc erps_mgmt_getexact_protection_group_by_id (vtss_erps_mgmt_conf_t *mgmt_req)
{
   mesa_rc ret = VTSS_APPL_ERPS_ERROR;
   vtss_erps_base_conf_t  base_conf;
   erps_mep_t  ermep;

   memset(&base_conf, 0, sizeof(base_conf));

//   ERPS_CRITD_ENTER();
   base_conf.group_id = mgmt_req->group_id;
   ret = vtss_erps_get_protection_group_by_id(&base_conf);
   mgmt_req->group_id = base_conf.group_id;
   memcpy(&mgmt_req->data.get.erpg, &base_conf.erpg, sizeof(vtss_erps_config_erpg_t));
   memcpy(&mgmt_req->data.get.stats, &base_conf.stats, sizeof(vtss_erps_fsm_stat_t));
   memcpy(&mgmt_req->data.get.raps_stats, &base_conf.raps_stats, sizeof(vtss_erps_statistics_t));
//   ERPS_CRITD_EXIT();

   ermep = vtss_get_erpg_to_mep(mgmt_req->group_id);

   mgmt_req->group_id = CONV_ERPSTOMGMT_INSTANCE(mgmt_req->group_id);
   mgmt_req->data.get.erpg.group_id = CONV_ERPSTOMGMT_INSTANCE (mgmt_req->data.get.erpg.group_id);

   mgmt_req->data.mep.east_mep_id = ermep.ccm_mep_id[ERPS_ARRAY_OFFSET_EAST];
   mgmt_req->data.mep.west_mep_id = ermep.ccm_mep_id[ERPS_ARRAY_OFFSET_WEST];
   mgmt_req->data.mep.raps_eastmep = ermep.raps_mep_id[ERPS_ARRAY_OFFSET_EAST];
   mgmt_req->data.mep.raps_westmep = ermep.raps_mep_id[ERPS_ARRAY_OFFSET_WEST];

   return (ret);
}


/******************************************************************************
   Misc functions
******************************************************************************/

#ifdef VTSS_SW_OPTION_ICFG

static mesa_rc erps_icfg_conf_group(const vtss_icfg_query_request_t  *req,
                                    vtss_icfg_query_result_t         *result,
                                    const vtss_erps_mgmt_conf_t      *conf)
{
    const vtss_erps_config_erpg_t *erpg = &conf->data.get.erpg;
    u32                           group = conf->group_id;
    vtss_usid_t                   usid  = topo_isid2usid(msg_master_isid());
    char                          port0[40], port1[40];
    u32                           i;
    BOOL                          sub_interconnected;

    sub_interconnected = erpg->ring_type == ERPS_RING_TYPE_SUB  &&  erpg->inter_connected_node;

    (void) icli_port_info_txt(usid, erpg->east_port, port0);
    (void) icli_port_info_txt(usid, erpg->west_port, port1);

    if (erpg->ring_type == ERPS_RING_TYPE_MAJOR) {
        VTSS_RC(vtss_icfg_printf(result, "erps %d major port0 interface %s port1 interface %s%s\n",
                                 group, port0, port1, (erpg->inter_connected_node ? " interconnect" : "")));
        
    } else {
        if (erpg->inter_connected_node) {
            VTSS_RC(vtss_icfg_printf(result, "erps %d sub port0 interface %s interconnect %d%s\n",
                                     group, port0, erpg->major_ring_id, (erpg->virtual_channel ? " virtual-channel" : "")));
        } else {
            VTSS_RC(vtss_icfg_printf(result, "erps %d sub port0 interface %s port1 interface %s%s\n",
                                     group, port0, port1, (erpg->virtual_channel ? " virtual-channel" : "")));
        }
    }

    BOOL have_meps = conf->data.mep.east_mep_id && conf->data.mep.raps_eastmep;

    if (req->all_defaults && !have_meps) {
        VTSS_RC(vtss_icfg_printf(result, "no erps %d mep\n", group));
    } else if (have_meps) {
        if (sub_interconnected) {
            if (erpg->virtual_channel) {
                // Interconnected sub-ring with virtual channel has an APS MEP "on port1"
                VTSS_RC(vtss_icfg_printf(result, "erps %d mep port0 sf %d aps %d port1 aps %d\n",
                                         group,
                                         conf->data.mep.east_mep_id, conf->data.mep.raps_eastmep, conf->data.mep.raps_westmep));
            } else {
                // Interconnected sub-ring with virtual channel does not have any MEPs on port 1
                VTSS_RC(vtss_icfg_printf(result, "erps %d mep port0 sf %d aps %d\n",
                                         group,
                                         conf->data.mep.east_mep_id, conf->data.mep.raps_eastmep));
            }
        } else {
            VTSS_RC(vtss_icfg_printf(result, "erps %d mep port0 sf %d aps %d port1 sf %d aps %d\n",
                                     group,
                                     conf->data.mep.east_mep_id, conf->data.mep.raps_eastmep,
                                     conf->data.mep.west_mep_id, conf->data.mep.raps_westmep));
        }
    }

    if (req->all_defaults || erpg->version != ERPS_VERSION_V2) {
        VTSS_RC(vtss_icfg_printf(result, "erps %d version %d\n", group,
                                 (erpg->version == ERPS_VERSION_V1 ? 1 : 2)));
    }

    if (req->all_defaults && !erpg->rpl_owner && !erpg->rpl_neighbour) {
        VTSS_RC(vtss_icfg_printf(result, "no erps %d rpl\n", group));
    } else {
        if (erpg->rpl_owner) {
            VTSS_RC(vtss_icfg_printf(result, "erps %d rpl owner %s\n", group,
                                     (erpg->rpl_owner_port == erpg->east_port ? "port0" : "port1")));
        } else if (erpg->rpl_neighbour) {
            VTSS_RC(vtss_icfg_printf(result, "erps %d rpl neighbor %s\n", group,
                                     (erpg->rpl_neighbour_port == erpg->east_port ? "port0" : "port1")));
        }
    }

    if (req->all_defaults || erpg->hold_off_time != 0) {
        VTSS_RC(vtss_icfg_printf(result, "erps %d holdoff %" PRIu64 "\n", group, erpg->hold_off_time));
    }

    if (req->all_defaults || erpg->guard_time != RAPS_GUARDTIMEOUT_DEFAULT_MILLISECONDS) {
        VTSS_RC(vtss_icfg_printf(result, "erps %d guard %" PRIu64 "\n", group, erpg->guard_time));
    }

    if (erpg->revertive) {
        if (req->all_defaults  ||  erpg->wtr_time != 1) {
            VTSS_RC(vtss_icfg_printf(result, "erps %d revertive %" PRIu64 "\n", group, erpg->wtr_time));
        }
    } else {
        VTSS_RC(vtss_icfg_printf(result, "no erps %d revertive\n", group));
    }

    if (req->all_defaults && !erpg->topology_change) {
        VTSS_RC(vtss_icfg_printf(result, "no erps %d topology-change propagate\n", group));
    } else if (erpg->topology_change) {
        VTSS_RC(vtss_icfg_printf(result, "erps %d topology-change propagate\n", group));
    }

    // VLANs

    for (i = 0; i < PROTECTED_VLANS_MAX  &&  !erpg->protected_vlans[i]; i++) {
        // loop
    }
    BOOL has_vlans = i < PROTECTED_VLANS_MAX;

    if (req->all_defaults  &&  !has_vlans) {
        VTSS_RC(vtss_icfg_printf(result, "no erps %d vlan\n", group));
    } else if (has_vlans) {
        const char *separator = "";
        VTSS_RC(vtss_icfg_printf(result, "erps %d vlan ", group));
        for (i = 0; i < PROTECTED_VLANS_MAX; i++) {
            if (erpg->protected_vlans[i]) {
                VTSS_RC(vtss_icfg_printf(result, "%s%d", separator, erpg->protected_vlans[i]));
                separator = ",";
            }
        }
        VTSS_RC(vtss_icfg_printf(result, "\n"));
    }

    return VTSS_RC_OK;
}

static mesa_rc erps_icfg_conf(const vtss_icfg_query_request_t  *req,
                              vtss_icfg_query_result_t         *result)
{
    vtss_erps_mgmt_conf_t conf_req;
    u32                   instance = 0;
    mesa_rc               rc;

    memset(&conf_req, 0, sizeof(conf_req));

    while (instance <= ERPS_MAX_PROTECTION_GROUPS  &&  erps_mgmt_getnext_protection_group_request(&conf_req) == VTSS_RC_OK) {
        rc = erps_icfg_conf_group(req, result, &conf_req);
        if (rc != VTSS_RC_OK) {
            return rc;
        }
        instance = conf_req.group_id;
        memset(&conf_req, 0, sizeof(conf_req));
        conf_req.group_id = instance;
    }

    return VTSS_RC_OK;
}
#endif /* VTSS_SW_OPTION_ICFG */

/******************************************************************************
                               OS Resources 
******************************************************************************/
/* ERPS main thread */
static void erps_run_thread(vtss_addrword_t data)
{
    mesa_rc            ret;
    mesa_port_no_t     port = 0;
    struct timeval     now;
    u32                now_ms, next_ms, t = 10;
    raps_pdu_in_t      *req;
    vtss_tick_count_t  wakeup;

    (void)gettimeofday(&now, NULL);
    now_ms = 1000 * now.tv_sec + now.tv_usec / 1000;
    next_ms = now_ms + 10;
    for (;;) {
        wakeup = vtss_current_time() + VTSS_OS_MSEC2TICK(t);
        while((req = (raps_pdu_in_t *)erps_queue.vtss_safe_queue_timed_get(wakeup))) {

            req->mep_id = CONV_ERPSTOMGMT_INSTANCE(req->mep_id);

            T_NG(TRACE_GRP_ERPS_RX, "ERPS PDU received on mep_id = %u", req->mep_id);
            T_NG_HEX(TRACE_GRP_ERPS_RX, req->pdu, ERPS_PDU_SIZE);

            if (port_to_mep[req->erpg].raps_mep_id[ERPS_ARRAY_OFFSET_EAST] == req->mep_id) {
                port = port_to_mep[req->erpg].port[ERPS_ARRAY_OFFSET_EAST];
                ret = vtss_erps_rx(port, req->len, req->pdu, req->erpg);
                if ( ret != VTSS_APPL_ERPS_RC_OK ) {
                    T_D("error in erps_rx = %d", ret);
                }
            } else if (port_to_mep[req->erpg].raps_mep_id[ERPS_ARRAY_OFFSET_WEST] == req->mep_id) {
                port = port_to_mep[req->erpg].port[ERPS_ARRAY_OFFSET_WEST];
                ret = vtss_erps_rx(port, req->len, req->pdu, req->erpg);
                if ( ret != VTSS_APPL_ERPS_RC_OK ) {
                    T_D("error in erps_rx = %d", ret);
                }
            } 
            VTSS_FREE(req);
        }
        (void)gettimeofday(&now, NULL);
        now_ms = 1000 * now.tv_sec + now.tv_usec / 1000;
        if ((now_ms - next_ms) > 1000) {
            next_ms = now_ms + 10;
            t = 10;
        } else {
            t = 10 - (now_ms - next_ms);
            if (t > 10) {
                t = 0;
            }
            next_ms += 10;
        }
        /* Upon timeout - run timer vtss_erps_timer_thread() */
        vtss_erps_timer_thread();
    }
}

/******************************************************************************
        Application public API, used by SNMP/JSON
******************************************************************************/

const vtss_enum_descriptor_t vtss_appl_erps_ring_type_txt[] = {
    { VTSS_APPL_ERPS_RING_TYPE_MAJOR, "major" },
    { VTSS_APPL_ERPS_RING_TYPE_SUB,   "sub"   },
    {}
};

const vtss_enum_descriptor_t vtss_appl_erps_admin_cmd_txt[] = {
    { VTSS_APPL_ERPS_ADMIN_CMD_MANUAL_SWITCH, "manualSwitch" },
    { VTSS_APPL_ERPS_ADMIN_CMD_FORCED_SWITCH, "forcedSwitch" },
    { VTSS_APPL_ERPS_ADMIN_CMD_CLEAR,         "clear"        },
    {}
};

const vtss_enum_descriptor_t vtss_appl_erps_version_txt[] = {
    { VTSS_APPL_ERPS_VERSION_V1, "version1" },
    { VTSS_APPL_ERPS_VERSION_V2, "version2" },
    {}
};

const vtss_enum_descriptor_t vtss_appl_erps_rpl_mode_txt[] = {
    { VTSS_APPL_ERPS_RPL_MODE_NONE,      "none"      },
    { VTSS_APPL_ERPS_RPL_MODE_OWNER,     "owner"     },
    { VTSS_APPL_ERPS_RPL_MODE_NEIGHBOUR, "neighbour" },
    {}
};

const vtss_enum_descriptor_t vtss_appl_erps_port_state_txt[] = {
    { VTSS_APPL_ERPS_PORT_STATE_OK, "ok"         },
    { VTSS_APPL_ERPS_PORT_STATE_SF, "signalFail" },
    {}
};

const vtss_enum_descriptor_t vtss_appl_erps_protection_state_txt[] = {
    { VTSS_APPL_ERPS_STATE_NONE,          "none"         },
    { VTSS_APPL_ERPS_STATE_IDLE,          "idle"         },
    { VTSS_APPL_ERPS_STATE_PROTECTED,     "protected"    },
    { VTSS_APPL_ERPS_STATE_FORCED_SWITCH, "forcedSwitch" },
    { VTSS_APPL_ERPS_STATE_MANUAL_SWITCH, "manualSwitch" },
    { VTSS_APPL_ERPS_STATE_PENDING,       "pending"      },
    {}
};

const vtss_enum_descriptor_t vtss_appl_erps_port_txt[] = {
    { VTSS_APPL_ERPS_PORT_0, "port0" },
    { VTSS_APPL_ERPS_PORT_1, "port1" },
    {}
};

const vtss_enum_descriptor_t vtss_appl_erps_request_state_txt[] = {
    { VTSS_APPL_ERPS_REQUEST_STATE_NONE,          "none" },
    { VTSS_APPL_ERPS_REQUEST_STATE_MANUAL_SWITCH, "manualSwitch" },
    { VTSS_APPL_ERPS_REQUEST_STATE_SIGNAL_FAIL,   "signalFail" },
    { VTSS_APPL_ERPS_REQUEST_STATE_FORCED_SWITCH, "forcedSwitch" },
    { VTSS_APPL_ERPS_REQUEST_STATE_EVENT,         "event" },
    {}
};

const vtss_enum_descriptor_t vtss_appl_erps_control_cmd_txt[] = {
    { VTSS_APPL_ERPS_CONTROL_CMD_NONE,                    "none" },
    { VTSS_APPL_ERPS_CONTROL_CMD_ADMIN_CMD_FORCED_SWITCH, "admCmdForcedSwitch" },
    { VTSS_APPL_ERPS_CONTROL_CMD_ADMIN_CMD_MANUAL_SWITCH, "admCmdManualSwitch" },
    { VTSS_APPL_ERPS_CONTROL_CMD_ADMIN_CMD_CLEAR,         "admCmdClear" },
    { VTSS_APPL_ERPS_CONTROL_CMD_STATISTICS_CLEAR,        "statisticsClear" },
    {}
};

#define CASE_U2E(e) case e : return VTSS_APPL_##e

static inline vtss_appl_erps_protection_state_t u16_to_vtss_appl_erps_protection_state_t(u16 s)
{
    switch (s) {
    CASE_U2E(ERPS_STATE_NONE);
    CASE_U2E(ERPS_STATE_IDLE);
    CASE_U2E(ERPS_STATE_PROTECTED);
    CASE_U2E(ERPS_STATE_FORCED_SWITCH);
    CASE_U2E(ERPS_STATE_MANUAL_SWITCH);
    CASE_U2E(ERPS_STATE_PENDING);
    default:
        T_W("Unmatched enum: %u", s);
        return VTSS_APPL_ERPS_STATE_NONE;
    }
}

static inline vtss_appl_erps_port_state_t u16_to_vtss_appl_erps_port_state_t(u16 s)
{
    switch (s) {
    CASE_U2E(ERPS_PORT_STATE_OK);
    CASE_U2E(ERPS_PORT_STATE_SF);
    default:
        T_W("Unmatched enum: %u", s);
        return VTSS_APPL_ERPS_PORT_STATE_OK;
    }
}

// C++ polymorph overloading in action
static inline vtss_appl_erps_ring_type_t to_appl_t(vtss_erps_ring_type_t t)
{
    switch (t) {
    CASE_U2E(ERPS_RING_TYPE_MAJOR);
    CASE_U2E(ERPS_RING_TYPE_SUB);
    default:
        T_W("Unmatched enum: %u", (u32)t);
        return VTSS_APPL_ERPS_RING_TYPE_MAJOR;
    }
}

static inline vtss_appl_erps_version_t to_appl_t(vtss_erps_version_t t)
{
    switch (t) {
    CASE_U2E(ERPS_VERSION_V1);
    CASE_U2E(ERPS_VERSION_V2);
    default:
        T_W("Unmatched enum: %u", (u32)t);
        return VTSS_APPL_ERPS_VERSION_V2;
    }
}

#undef CASE_U2E

static inline vtss_appl_erps_admin_cmd_t u8_to_vtss_appl_erps_admin_cmd_t(u8 a)
{
    switch (a) {
    case 0: return VTSS_APPL_ERPS_ADMIN_CMD_CLEAR;
    case 1: return VTSS_APPL_ERPS_ADMIN_CMD_MANUAL_SWITCH;
    case 2: return VTSS_APPL_ERPS_ADMIN_CMD_FORCED_SWITCH;
    default:
        T_W("Unmatched enum: %u", a);
        return VTSS_APPL_ERPS_ADMIN_CMD_CLEAR;
    }
}

static inline vtss_appl_erps_request_state_t u16_to_vtss_appl_erps_request_state_t(u16 s)
{
    switch (s) {
    case 14: return VTSS_APPL_ERPS_REQUEST_STATE_EVENT;
    case 13: return VTSS_APPL_ERPS_REQUEST_STATE_FORCED_SWITCH;
    case 11: return VTSS_APPL_ERPS_REQUEST_STATE_SIGNAL_FAIL;
    case 07: return VTSS_APPL_ERPS_REQUEST_STATE_MANUAL_SWITCH;
    case 00: return VTSS_APPL_ERPS_REQUEST_STATE_NONE;
    default:
        // Don't T_W here as 's' is a binary value from the PDU.
        return VTSS_APPL_ERPS_REQUEST_STATE_NONE;
    }
}

static vtss_ifindex_t uport_to_ifindex(mesa_port_no_t uport)
{
    vtss_ifindex_t ifindex;
    if (vtss_ifindex_from_port(VTSS_ISID_LOCAL, uport2iport(uport), &ifindex) == VTSS_RC_OK) {
        return ifindex;
    } else {
        return VTSS_IFINDEX_NONE;
    }
}

// Return FALSE if conversion is invalid; only local physical ports are allowed
static BOOL ifindex_to_uport(vtss_ifindex_t ifidx, mesa_port_no_t *uport)
{
    vtss_ifindex_elm_t elm;
    if (vtss_ifindex_decompose(ifidx, &elm) != VTSS_RC_OK || elm.iftype != VTSS_IFINDEX_TYPE_PORT || !msg_switch_is_local(elm.isid)) {
        return FALSE;
    }
    *uport = iport2uport(elm.ordinal);
    return TRUE;
}

mesa_rc vtss_appl_erps_capabilities_get(vtss_appl_erps_capabilities_t *const cap)
{
    cap->max_groups          = VTSS_APPL_ERPS_MAX_PROTECTION_GROUPS;
    cap->max_vlans_per_group = VTSS_APPL_ERPS_PROTECTED_VLANS_MAX;
    return VTSS_RC_OK;
}

// Generic group id iterator, used by the vtss_appl_erps_..._itr() funcs
static mesa_rc erps_generic_group_id_itr(const u32 *const in, u32 *const out)
{
    u32 grp = in ? *in : 0;

    vtss_erps_base_conf_t base_conf;
    mesa_rc              rc;

    memset(&base_conf, 0, sizeof(base_conf));
    base_conf.group_id = grp;

    rc = vtss_erps_getnext_protection_group_by_id(&base_conf);

    if (rc == VTSS_RC_OK) {
        *out = CONV_ERPSTOMGMT_INSTANCE(base_conf.group_id);
    }

    return rc;
}

#define CHECK_RC_OK(RC) do { mesa_rc _rc = (RC); if (_rc != VTSS_RC_OK) { T_D(#RC " failed with code %u; exiting", _rc); return VTSS_APPL_ERPS_ERROR; } } while (0)

static mesa_rc vtss_appl_erps_conf_set_internal(u32 group_id, const vtss_appl_erps_conf_t *const in, BOOL attempt_restore_on_error)
{
    vtss_appl_erps_conf_t current;
    vtss_erps_mgmt_conf_t conf_req;
    mesa_port_no_t        uport0 = 0, uport1 = 0;
    BOOL                  create = FALSE;

    // The general outline for this func is this:
    //
    //   1. Create protection group if it doesn't exit. If something goes wrong, make sure the group is destroyed again.
    //   2. Set "lower-impact" parameters, i.e. parameters which don't usually affect traffic
    //   3. Set "high-impact" parameters, such as RPL role, VLANs, MEPs
    //   4. If any of the parameter configuration goes wrong on an existing group, attempt to revert by setting all
    //      parameters back to the values prior to the update attempt.
    //
    // All of this trickery is necessary since the ERPS management core code doesn't allow "transactional updates", so we
    // sometimes end up in a situation where some parameter changes have succeeded by the time a later parameter update fails.



    // Certain objects are write-once, i.e. cannot be modified once set. This covers:
    //
    //  - Ring type
    //  - Ports
    //  - Interconnection
    //  - Virtual channel
    //
    // The remainder are adjustable.

    T_D("Entering set, group id %u. %s attempt restore on error", group_id, attempt_restore_on_error ? "Will" : "Won't");

    create = vtss_appl_erps_conf_get(group_id, &current) != VTSS_RC_OK;

    if (!create) {
        // Group exists so this is an update request. The actual update takes place further down this func; here
        // we make sure that write-once objects are indeed unchanged:
#define TST(FIELD) do { if (current.FIELD != in->FIELD) { T_D("Denied attempt to change write-once field: " #FIELD); return VTSS_APPL_ERPS_ERROR; } } while (0)
        TST(ring_type);
        TST(port0);
        TST(port1);
        TST(interconnect_major_ring_id);
        TST(virtual_channel);
#undef TST
    } else {
        // Group doesn't exist, so try to create the group
        memset(&conf_req, 0, sizeof(conf_req));
        conf_req.req_type = ERPS_CMD_PROTECTION_GROUP_ADD;
        conf_req.group_id = group_id;

        if (in->ring_type == VTSS_APPL_ERPS_RING_TYPE_MAJOR) {
            if (!ifindex_to_uport(in->port0, &uport0) || !ifindex_to_uport(in->port1, &uport1)) {
                T_D("Bad port ifindex: %u, %u", VTSS_IFINDEX_PRINTF_ARG(in->port0), VTSS_IFINDEX_PRINTF_ARG(in->port1));
                goto error_out;
            }

            conf_req.data.create.ring_type       = ERPS_RING_TYPE_MAJOR;
            conf_req.data.create.east_port       = uport0;
            conf_req.data.create.west_port       = uport1;
            conf_req.data.create.interconnected  = in->interconnect_major_ring_id > 0;
            conf_req.data.create.major_ring_id   = 0;
            conf_req.data.create.virtual_channel = FALSE;
        } else {  // Sub
            if (!ifindex_to_uport(in->port0, &uport0)) {
                T_D("Bad port0 ifindex: %u", VTSS_IFINDEX_PRINTF_ARG(in->port0));
                goto error_out;
            }
            if (in->port1 > 0 &&  !ifindex_to_uport(in->port1, &uport1)) {
                T_D("Bad port1 ifindex: %u", VTSS_IFINDEX_PRINTF_ARG(in->port1));
                goto error_out;
            }

            conf_req.data.create.ring_type       = ERPS_RING_TYPE_SUB;
            conf_req.data.create.east_port       = uport0;
            conf_req.data.create.west_port       = uport1; // may be zero
            conf_req.data.create.interconnected  = in->interconnect_major_ring_id > 0;
            conf_req.data.create.major_ring_id   = in->interconnect_major_ring_id;
            conf_req.data.create.virtual_channel = in->virtual_channel;
        }

        // Commit, then learn the defaults so we know what fields to update, if any
        T_D("Creating new protection group");
        if (erps_mgmt_set_protection_group_request(&conf_req) != VTSS_RC_OK) {
            goto error_out;
        }
        if (vtss_appl_erps_conf_get(group_id, &current) != VTSS_RC_OK) {
            T_E("Failed to get newly created ERPS group parameters");       // This really shouldn't happen!
            goto error_create_param;
        }
    }

    // Configure lower-impact parameters if they're changed

    if (current.hold_off_time != in->hold_off_time) {
        u64 holdoff_time_ms = in->hold_off_time;
        if (holdoff_time_ms % 100 != 0) {
            holdoff_time_ms = (holdoff_time_ms / 100) * 100;
        }

        memset(&conf_req, 0, sizeof(conf_req));

        conf_req.req_type        = ERPS_CMD_HOLD_OFF_TIMER;
        conf_req.group_id        = group_id;
        conf_req.data.timer.time = holdoff_time_ms;

        if (erps_mgmt_set_protection_group_request(&conf_req) != VTSS_RC_OK) {
            T_D("Failed to set hold-off time");
            goto error_create_param;
        }
    }

    if (current.wtr_time != in->wtr_time) {
        memset(&conf_req,0,sizeof(conf_req));
        conf_req.req_type         = ERPS_CMD_WTR_TIMER;
        conf_req.group_id         = group_id;
        conf_req.data.timer.time  = in->wtr_time;

        if (erps_mgmt_set_protection_group_request(&conf_req) != VTSS_RC_OK) {
            T_D("Failed to set WTR time");
            goto error_create_param;
        }
    }

    if (current.revertive != in->revertive) {
        memset(&conf_req, 0, sizeof(conf_req));
        conf_req.req_type = in->revertive ? ERPS_CMD_DISABLE_NON_REVERTIVE : ERPS_CMD_ENABLE_NON_REVERTIVE;
        conf_req.group_id = group_id;

        if (erps_mgmt_set_protection_group_request(&conf_req) != VTSS_RC_OK) {
            T_D("Failed to set revertive mode");
            goto error_create_param;
        }
    }

    if (current.guard_time != in->guard_time) {
        memset(&conf_req, 0, sizeof(conf_req));

        conf_req.req_type        = ERPS_CMD_GUARD_TIMER;
        conf_req.group_id        = group_id;
        conf_req.data.timer.time = in->guard_time;

        if (erps_mgmt_set_protection_group_request(&conf_req) != VTSS_RC_OK) {
            T_D("Failed to set guard timer");
            goto error_create_param;
        }
    }

    if (current.version != in->version) {
        memset(&conf_req, 0, sizeof(conf_req));

        conf_req.req_type = in->version == VTSS_APPL_ERPS_VERSION_V1 ? ERPS_CMD_ENABLE_VERSION_1_COMPATIBLE : ERPS_CMD_DISABLE_VERSION_1_COMPATIBLE;
        conf_req.group_id = group_id;

        if (erps_mgmt_set_protection_group_request(&conf_req) != VTSS_RC_OK) {
            T_D("Failed to set version");
            goto error_create_param;
        }
    }

    if (current.topology_change != in->topology_change) {
        memset(&conf_req, 0, sizeof(conf_req));
        conf_req.req_type = in->topology_change ? ERPS_CMD_TOPOLOGY_CHANGE_PROPAGATE : ERPS_CMD_TOPOLOGY_CHANGE_NO_PROPAGATE;
        conf_req.group_id = group_id;

        if (erps_mgmt_set_protection_group_request(&conf_req) != VTSS_RC_OK) {
            T_D("Failed to set topology change propagation");
            goto error_create_param;
        }
    }

    // Configure MEPs for the group

    if ((in->port0_sf_mep_id > 0) &&
        (
            (current.port0_sf_mep_id  != in->port0_sf_mep_id)  ||
            (current.port0_aps_mep_id != in->port0_aps_mep_id) ||
            (current.port1_sf_mep_id  != in->port1_sf_mep_id)  ||
            (current.port1_aps_mep_id != in->port1_aps_mep_id)
        )) {
        // Set/change

        T_D("About to set MEP associations");

        // First validate params

        if (in->port0_aps_mep_id == 0) {
            T_D("Port 0 SF and APS MEPs must be specified");
            goto error_create_param;
        }

        if (in->ring_type == VTSS_APPL_ERPS_RING_TYPE_MAJOR) {
            if (in->port1_sf_mep_id == 0 || in->port1_aps_mep_id == 0) {
                T_D("Major ring: Port 1 SF and APS must be specified");
                goto error_create_param;
            }
        } else {
            if (in->interconnect_major_ring_id > 0) {
                if (in->port1_sf_mep_id > 0) {
                    T_D("Interconnected sub-ring: No port 1 SF MEP is allowed");
                    goto error_create_param;
                }
                if (in->virtual_channel) {
                    if (in->port1_aps_mep_id == 0) {
                        T_D("Interconnected sub-ring with virtual channel: Must have port 1 APS MEP");
                        goto error_create_param;
                    }
                } else {
                    if (in->port1_aps_mep_id > 0) {
                        T_D("Interconnected sub-ring without virtual channel: Must not have port 1 APS MEP");
                        goto error_create_param;
                    }
                }
            }
        }

        // Then configure

        memset(&conf_req, 0, sizeof(conf_req));
        conf_req.req_type              = ERPS_CMD_ADD_MEP_ASSOCIATION;
        conf_req.group_id              = group_id;
        conf_req.data.mep.east_mep_id  = in->port0_sf_mep_id;
        conf_req.data.mep.west_mep_id  = in->port1_sf_mep_id;
        conf_req.data.mep.raps_eastmep = in->port0_aps_mep_id;
        conf_req.data.mep.raps_westmep = in->port1_aps_mep_id;

        if (erps_mgmt_set_protection_group_request(&conf_req) != VTSS_RC_OK) {
            T_D("Failed to add MEP association");
            goto error_create_param;
        }
    } else if (current.port0_sf_mep_id > 0 && in->port0_sf_mep_id == 0) {
        // Set, want to clear (delete)

        T_D("About to delete MEP associations");

        memset(&conf_req, 0, sizeof(conf_req));
        conf_req.req_type              = ERPS_CMD_DEL_MEP_ASSOCIATION;
        conf_req.group_id              = group_id;
        conf_req.data.mep.east_mep_id  = 0;
        conf_req.data.mep.west_mep_id  = 0;
        conf_req.data.mep.raps_eastmep = 0;
        conf_req.data.mep.raps_westmep = 0;

        if (erps_mgmt_set_protection_group_request(&conf_req) != VTSS_RC_OK) {
            T_D("Failed to delete MEP association");
            goto error_create_param;
        }
    }

    if (current.rpl_port != in->rpl_port || current.rpl_mode != in->rpl_mode) {
        // Changing RPL port or mode: This requires us to tear down the old RPL config and set up the new

        mesa_port_no_t port;
        BOOL           do_set;

        memset(&conf_req, 0, sizeof(conf_req));
        conf_req.group_id = group_id - 1;
        if (erps_mgmt_getexact_protection_group_by_id(&conf_req) != VTSS_RC_OK) {
            goto error_create_param;
        }
        port = in->rpl_port == VTSS_APPL_ERPS_PORT_0 ? conf_req.data.get.erpg.east_port : conf_req.data.get.erpg.west_port;

        // Unconfigure existing

        memset(&conf_req, 0, sizeof(conf_req));
        conf_req.group_id                = group_id;
        conf_req.data.rpl_block.rpl_port = port;
        do_set                           = TRUE;

        switch (current.rpl_mode) {
        case VTSS_APPL_ERPS_RPL_MODE_OWNER:
            T_D("Unconfiguring RPL owner mode");
            conf_req.req_type = ERPS_CMD_UNSET_RPL_BLOCK;
            break;
        case VTSS_APPL_ERPS_RPL_MODE_NEIGHBOUR:
            T_D("Unconfiguring RPL neighbour mode");
            conf_req.req_type = ERPS_CMD_UNSET_RPL_NEIGHBOUR;
            break;
        case VTSS_APPL_ERPS_RPL_MODE_NONE:
            T_D("RPL mode none, no unconfiguration necessary");
            // no-op; don't do a set
            do_set = FALSE;
            break;
        }

        if (do_set) {
            if (erps_mgmt_set_protection_group_request(&conf_req) != VTSS_RC_OK) {
                T_D("Failed to unconfigure RPL mode/port");
                goto error_create_param;
            }
        }

        // Configure new

        memset(&conf_req, 0, sizeof(conf_req));
        conf_req.group_id                = group_id;
        conf_req.data.rpl_block.rpl_port = port;
        do_set                           = TRUE;

        switch (in->rpl_mode) {
        case VTSS_APPL_ERPS_RPL_MODE_OWNER:
            T_D("Becoming RPL owner on uport %u", port);
            conf_req.req_type = ERPS_CMD_SET_RPL_BLOCK;
            break;
        case VTSS_APPL_ERPS_RPL_MODE_NEIGHBOUR:
            T_D("Becoming RPL neighbour on uport %u", port);
            conf_req.req_type = ERPS_CMD_SET_RPL_NEIGHBOUR;
            break;
        case VTSS_APPL_ERPS_RPL_MODE_NONE:
            T_D("RPL mode none, no configuration necessary");
            // no-op; don't do a set
            do_set = FALSE;
            break;
        }

        if (do_set) {
            if (erps_mgmt_set_protection_group_request(&conf_req) != VTSS_RC_OK) {
                T_D("Failed to set RPL mode/port");
                goto error_create_param;
            }
        }
    }

    if (memcmp(current.protected_vlans, in->protected_vlans, sizeof(current.protected_vlans)) != 0) {
        T_D("Updating VLAN list");

        memset(&conf_req, 0, sizeof(conf_req));
        conf_req.group_id          = group_id;
        conf_req.data.vid.num_vids = 1;

        // T_D_HEX(in->protected_vlans, ARRSZ(in->protected_vlans));
        // T_D_HEX(current.protected_vlans, ARRSZ(current.protected_vlans));

        for (u32 vid = 1; vid <= VTSS_APPL_VLAN_ID_MAX; vid++) {
            const u32 val = VTSS_BF_GET(in->protected_vlans, vid);
            if (VTSS_BF_GET(current.protected_vlans, vid) != val) {
                conf_req.req_type          = val ? ERPS_CMD_ADD_VLANS : ERPS_CMD_DEL_VLANS;
                conf_req.data.vid.p_vid    = vid;
                if (erps_mgmt_set_protection_group_request(&conf_req) != VTSS_RC_OK) {
                    T_D("Failed to set VID %u to %u", vid, val);
                    goto error_create_param;
                }
            }
        }
    }

    T_D("Leaving set with status OK");
    return VTSS_RC_OK;

error_create_param:
    if (create) {
        // We got here because an ERPS group was created, but one of the subsequent parameter updates failed.
        memset(&conf_req, 0, sizeof(conf_req));
        conf_req.req_type = ERPS_CMD_PROTECTION_GROUP_DELETE;
        conf_req.group_id = group_id;

        T_D("Deleting the protection group due to subsequent parameter updates failed");
        if (erps_mgmt_set_protection_group_request(&conf_req) != VTSS_RC_OK) {
            T_D("Cannot delete protection group after parameter update failure");
        }
    } else {
        if (attempt_restore_on_error) {
            T_D("Attempting to restore previous configuration");
            if (vtss_appl_erps_conf_set_internal(group_id, &current, FALSE) != VTSS_RC_OK) {
                T_I("ERPS %d: Could not restore original group configuration", group_id);
            }
        }
    }

error_out:
    T_D("Leaving set with status ERROR");
    return VTSS_APPL_ERPS_ERROR;
}

mesa_rc vtss_appl_erps_conf_set(u32 group_id, const vtss_appl_erps_conf_t *const in)
{
    return vtss_appl_erps_conf_set_internal(group_id, in, TRUE);
}

mesa_rc vtss_appl_erps_conf_get(u32 group_id, vtss_appl_erps_conf_t *const out)
{
    vtss_erps_mgmt_conf_t conf;
    mesa_rc               rc;
    u32                   i;

    memset(out, 0, sizeof(*out));

    memset(&conf, 0, sizeof(conf));
    conf.group_id = group_id - 1;

    rc = erps_mgmt_getexact_protection_group_by_id(&conf);

    if (rc == VTSS_RC_OK) {
        out->ring_type                  = to_appl_t(conf.data.get.erpg.ring_type);
        out->port0                      = uport_to_ifindex(conf.data.get.erpg.east_port);
        out->port1                      = uport_to_ifindex(conf.data.get.erpg.west_port);
        out->interconnect_major_ring_id = conf.data.get.erpg.major_ring_id;
        out->virtual_channel            = conf.data.get.erpg.virtual_channel;

        out->port0_sf_mep_id            = conf.data.mep.east_mep_id;
        out->port1_sf_mep_id            = conf.data.mep.west_mep_id;
        out->port0_aps_mep_id           = conf.data.mep.raps_eastmep;
        out->port1_aps_mep_id           = conf.data.mep.raps_westmep;

        out->hold_off_time              = conf.data.get.erpg.hold_off_time;
        out->wtr_time                   = MIN_TO_MS(conf.data.get.erpg.wtr_time);
        out->guard_time                 = conf.data.get.erpg.guard_time;
        out->revertive                  = conf.data.get.erpg.revertive;
        out->version                    = to_appl_t(conf.data.get.erpg.version);
        out->topology_change            = conf.data.get.erpg.topology_change;
        out->rpl_port                   = conf.data.get.erpg.rpl_owner_port == conf.data.get.erpg.east_port ? VTSS_APPL_ERPS_PORT_0 : VTSS_APPL_ERPS_PORT_1;

        if (conf.data.get.erpg.rpl_owner) {
            out->rpl_mode               = VTSS_APPL_ERPS_RPL_MODE_OWNER;
        } else if (conf.data.get.erpg.rpl_neighbour) {
            out->rpl_mode               = VTSS_APPL_ERPS_RPL_MODE_NEIGHBOUR;
        } else {
            out->rpl_mode               = VTSS_APPL_ERPS_RPL_MODE_NONE;
        }

        VTSS_BF_CLR(out->protected_vlans, ARRSZ(out->protected_vlans));
        for (i = 0; i < PROTECTED_VLANS_MAX; i++) {
            mesa_vid_t v = conf.data.get.erpg.protected_vlans[i];
            if (v) {
                VTSS_BF_SET(out->protected_vlans, v, 1);
            }
        }
    }

    return rc;
}

mesa_rc vtss_appl_erps_conf_default(u32 *key, vtss_appl_erps_conf_t *const out)
{
    *key = 0;
    memset(out, 0, sizeof(*out));

    out->ring_type                  = VTSS_APPL_ERPS_RING_TYPE_MAJOR;
    out->port0                      = VTSS_IFINDEX_NONE;
    out->port1                      = VTSS_IFINDEX_NONE;
    out->interconnect_major_ring_id = 0;
    out->virtual_channel            = FALSE;

    out->port0_sf_mep_id            = 0;
    out->port1_sf_mep_id            = 0;
    out->port0_aps_mep_id           = 0;
    out->port1_aps_mep_id           = 0;

    out->hold_off_time              = 0;
    out->wtr_time                   = MIN_TO_MS(1);
    out->guard_time                 = RAPS_GUARDTIMEOUT_DEFAULT_MILLISECONDS;
    out->revertive                  = TRUE;
    out->version                    = VTSS_APPL_ERPS_VERSION_V2;
    out->topology_change            = FALSE;
    out->rpl_port                   = VTSS_APPL_ERPS_PORT_0;
    out->rpl_mode                   = VTSS_APPL_ERPS_RPL_MODE_NONE;

    return VTSS_RC_OK;
}

mesa_rc vtss_appl_erps_conf_del(u32 group_id)
{
    vtss_erps_mgmt_conf_t mgmt_conf_req;

    memset(&mgmt_conf_req, 0, sizeof(mgmt_conf_req));
    mgmt_conf_req.req_type = ERPS_CMD_PROTECTION_GROUP_DELETE;
    mgmt_conf_req.group_id = group_id;

    return erps_mgmt_set_protection_group_request(&mgmt_conf_req);
}

mesa_rc vtss_appl_erps_conf_itr(const u32 *const in, u32 *const out)
{
    return erps_generic_group_id_itr(in, out);
}

mesa_rc vtss_appl_erps_status_get(u32 group_id, vtss_appl_erps_status_t *const out)
{
    vtss_erps_base_conf_t base_conf;
    mesa_rc               rc;

    memset(&base_conf, 0, sizeof(base_conf));
    base_conf.group_id = group_id - 1;

    rc = vtss_erps_get_protection_group_by_id(&base_conf);

    if (rc == VTSS_RC_OK) {
        out->active             = base_conf.stats.active;
        out->state              = u16_to_vtss_appl_erps_protection_state_t(base_conf.stats.state);
        out->rpl_blocked        = base_conf.stats.rpl_blocked == RAPS_RPL_BLOCKED;
        out->wtr_remaining_time = base_conf.stats.wtr_remaining_time;
        out->admin_cmd          = u8_to_vtss_appl_erps_admin_cmd_t(base_conf.stats.admin_cmd);
        out->fop_alarm          = base_conf.stats.fop_alarm;

        out->tx                 = base_conf.stats.tx != 0;
        out->tx_req             = u16_to_vtss_appl_erps_request_state_t(base_conf.stats.tx_req);
        out->tx_rb              = base_conf.stats.tx_rb != 0;
        out->tx_dnf             = base_conf.stats.tx_dnf != 0;
        out->tx_bpr             = base_conf.stats.tx_bpr ? VTSS_APPL_ERPS_PORT_1 : VTSS_APPL_ERPS_PORT_0;

        out->port0_blocked      = base_conf.stats.east_blocked;
        out->port0_state        = u16_to_vtss_appl_erps_port_state_t(base_conf.stats.east_port_state);
        out->port0_rx           = base_conf.stats.rx[0] != 0;
        out->port0_rx_req       = u16_to_vtss_appl_erps_request_state_t(base_conf.stats.rx_req[0]);
        out->port0_rx_rb        = base_conf.stats.rx_rb[0] != 0;
        out->port0_rx_dnf       = base_conf.stats.rx_dnf[0] != 0;
        out->port0_rx_bpr       = base_conf.stats.rx_bpr[0] ? VTSS_APPL_ERPS_PORT_1 : VTSS_APPL_ERPS_PORT_0;
        memcpy(out->port0_rx_node_id, base_conf.stats.rx_node_id[0], sizeof(out->port0_rx_node_id));

        out->port1_blocked      = base_conf.stats.west_blocked;
        out->port1_state        = u16_to_vtss_appl_erps_port_state_t(base_conf.stats.west_port_state);
        out->port1_rx           = base_conf.stats.rx[1] != 0;
        out->port1_rx_req       = u16_to_vtss_appl_erps_request_state_t(base_conf.stats.rx_req[1]);
        out->port1_rx_rb        = base_conf.stats.rx_rb[1] != 0;
        out->port1_rx_dnf       = base_conf.stats.rx_dnf[1] != 0;
        out->port1_rx_bpr       = base_conf.stats.rx_bpr[1] ? VTSS_APPL_ERPS_PORT_1 : VTSS_APPL_ERPS_PORT_0;
        memcpy(out->port1_rx_node_id, base_conf.stats.rx_node_id[1], sizeof(out->port1_rx_node_id));
    }

    return rc;
}

mesa_rc vtss_appl_erps_status_itr(const u32 *const in, u32 *const out)
{
    return erps_generic_group_id_itr(in, out);
}

mesa_rc vtss_appl_erps_statistics_get(u32 group_id, vtss_appl_erps_statistics_t *const out)
{
    vtss_erps_base_conf_t base_conf;
    mesa_rc               rc;

    memset(&base_conf, 0, sizeof(base_conf));
    base_conf.group_id = group_id - 1;

    rc = vtss_erps_get_protection_group_by_id(&base_conf);

    if (rc == VTSS_RC_OK) {
        out->raps_sent        = base_conf.raps_stats.raps_sent;
        out->raps_rcvd        = base_conf.raps_stats.raps_rcvd;
        out->raps_rx_dropped  = base_conf.raps_stats.raps_rx_dropped;
        out->local_sf         = base_conf.raps_stats.local_sf;
        out->local_sf_cleared = base_conf.raps_stats.local_sf_cleared;
        out->remote_sf        = base_conf.raps_stats.remote_sf;
        out->event_nr         = base_conf.raps_stats.event_nr;
        out->remote_ms        = base_conf.raps_stats.remote_ms;
        out->local_ms         = base_conf.raps_stats.local_ms;
        out->remote_fs        = base_conf.raps_stats.remote_fs;
        out->local_fs         = base_conf.raps_stats.local_fs;
        out->admin_cleared    = base_conf.raps_stats.admin_cleared;
    }

    return rc;
}

mesa_rc vtss_appl_erps_statistics_itr(const u32 *const in, u32 *const out)
{
    return erps_generic_group_id_itr(in, out);
}

mesa_rc vtss_appl_erps_control_set(u32 group_id, const vtss_appl_erps_control_t *const ctrl)
{
    vtss_erps_mgmt_conf_t conf_req;

    switch (ctrl->cmd) {
    case VTSS_APPL_ERPS_CONTROL_CMD_NONE:
        // No-op
        break;

    case VTSS_APPL_ERPS_CONTROL_CMD_ADMIN_CMD_FORCED_SWITCH:
    case VTSS_APPL_ERPS_CONTROL_CMD_ADMIN_CMD_MANUAL_SWITCH:
    case VTSS_APPL_ERPS_CONTROL_CMD_ADMIN_CMD_CLEAR:
        memset(&conf_req, 0, sizeof(conf_req));
        conf_req.group_id = group_id - 1;
        CHECK_RC_OK(erps_mgmt_getexact_protection_group_by_id(&conf_req));

        conf_req.data.create.east_port = (ctrl->port == VTSS_APPL_ERPS_PORT_0) ? conf_req.data.get.erpg.east_port : conf_req.data.get.erpg.west_port;

        switch (ctrl->cmd) {
        case VTSS_APPL_ERPS_CONTROL_CMD_ADMIN_CMD_FORCED_SWITCH: conf_req.req_type = ERPS_CMD_FORCED_SWITCH; break;
        case VTSS_APPL_ERPS_CONTROL_CMD_ADMIN_CMD_MANUAL_SWITCH: conf_req.req_type = ERPS_CMD_MANUAL_SWITCH; break;
        case VTSS_APPL_ERPS_CONTROL_CMD_ADMIN_CMD_CLEAR:         conf_req.req_type = ERPS_CMD_CLEAR;         break;
        default:
            // Quiet irrelevant compiler warning
            break;
        }

        CHECK_RC_OK(erps_mgmt_set_protection_group_request(&conf_req));
        break;

    case VTSS_APPL_ERPS_CONTROL_CMD_STATISTICS_CLEAR:
        memset(&conf_req, 0, sizeof(conf_req));

        conf_req.req_type = ERPS_CMD_CLEAR_STATISTICS;
        conf_req.group_id = group_id;

        CHECK_RC_OK(erps_mgmt_set_protection_group_request(&conf_req));
        break;

    default:
        return VTSS_APPL_ERPS_ERROR;
    }

    return VTSS_RC_OK;
}

#undef CHECK_RC_OK

mesa_rc vtss_appl_erps_control_get(u32 group_id, vtss_appl_erps_control_t *const ctrl)
{
    ctrl->cmd  = VTSS_APPL_ERPS_CONTROL_CMD_NONE;
    ctrl->port = VTSS_APPL_ERPS_PORT_0;
    return VTSS_RC_OK;
}

mesa_rc vtss_appl_erps_control_itr(const u32 *const in, u32 *const out)
{
    return erps_generic_group_id_itr(in, out);
}

/******************************************************************************
 Module Init Function 
******************************************************************************/

extern "C" int erps_icli_cmd_register();

mesa_rc erps_init(vtss_init_data_t *data)
{
    vtss_isid_t isid = data->isid; 

    if (data->cmd == INIT_CMD_EARLY_INIT) {
        /* Initialize and register trace ressources */
        VTSS_TRACE_REG_INIT(&trace_reg, trace_grps, TRACE_GRP_CNT);
        VTSS_TRACE_REGISTER(&trace_reg);
    }

    switch (data->cmd)
    {
        case INIT_CMD_INIT:
            T_D("INIT ERPS");
            T_D("initializing ERPS module");

            /* initialize critd */
            critd_init(&crit, "ERPS Crit", VTSS_MODULE_ID_ERPS, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
            critd_init(&crit_p, "ERPS supp Crit", VTSS_MODULE_ID_ERPS, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);
            critd_init(&crit_fsm, "ERPS FSM Crit", VTSS_MODULE_ID_ERPS, VTSS_TRACE_MODULE_ID, CRITD_TYPE_MUTEX);

            /* semaphore initialization go here */
            vtss_thread_create(VTSS_THREAD_PRIO_HIGHER,
                               erps_run_thread,
                               0,
                               "ERPS State Machine",
                               nullptr,
                               0,
                               &run_thread_handle,
                               &run_thread_block);
            
            CRIT_EXIT(crit);
            CRIT_EXIT(crit_p);
            CRIT_EXIT(crit_fsm);
            erps_init_done = FALSE;
#ifdef VTSS_SW_OPTION_ICFG
            VTSS_RC(vtss_icfg_query_register(VTSS_ICFG_ERPS_GLOBAL_CONF, "erps", erps_icfg_conf));
#endif
#ifdef VTSS_SW_OPTION_PRIVATE_MIB
            erps_mib_init();
#endif
#ifdef VTSS_SW_OPTION_JSON_RPC
            vtss_appl_erps_json_init();
#endif
            erps_icli_cmd_register();
            break;
        case INIT_CMD_CONF_DEF:
            T_D("CONF_DEF");
            if (isid == VTSS_ISID_LOCAL) {
                erps_restore_to_default();
            }
            break;
        case INIT_CMD_MASTER_UP:
            T_D("MASTER_UP");
            erps_restore_to_default();
            erps_init_done = TRUE;
            break;
        default:
            break;
    }

    T_D("exit");
    return 0;
}

#if defined(__GNUC__) && __GNUC__ >= 6
#pragma GCC diagnostic pop
#endif

