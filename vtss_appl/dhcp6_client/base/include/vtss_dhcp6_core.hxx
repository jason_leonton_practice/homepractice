/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _VTSS_DHCP6_CORE_HXX_
#define _VTSS_DHCP6_CORE_HXX_

#include <forward_list>

#include "netdb.h"
#include "vtss_dhcp6_type.hxx"
#include "vtss_dhcp6_frame.hxx"

#define VTSS_TRACE_MODULE_ID                VTSS_MODULE_ID_DHCP6C

namespace vtss
{
namespace dhcp6
{
#define DHCPV6_DUID_TYPE_LLT                1
#define DHCPV6_DUID_TYPE_EN                 2
#define DHCPV6_DUID_TYPE_LL                 3
#define DHCPV6_DUID_HARDWARE_TYPE           1   /* Ethernet */

#define DHCPV6_DO_AUTHENTICATION            0

#define DHCPV6_SOL_MAX_DELAY                1
#define DHCPV6_SOL_TIMEOUT                  1
#define DHCPV6_SOL_MAX_RT                   120

#define DHCPV6_REQ_TIMEOUT                  1
#define DHCPV6_REQ_MAX_RT                   30
#define DHCPV6_REQ_MAX_RC                   10

#define DHCPV6_CNF_MAX_DELAY                1
#define DHCPV6_CNF_TIMEOUT                  1
#define DHCPV6_CNF_MAX_RT                   4
#define DHCPV6_CNF_MAX_RD                   10

#define DHCPV6_REN_TIMEOUT                  10
#define DHCPV6_REN_MAX_RT                   600

#define DHCPV6_REB_TIMEOUT                  10
#define DHCPV6_REB_MAX_RT                   600

#define DHCPV6_INF_MAX_DELAY                1
#define DHCPV6_INF_TIMEOUT                  1
#define DHCPV6_INF_MAX_RT                   120

#define DHCPV6_REL_TIMEOUT                  1
#define DHCPV6_REL_MAX_RC                   5

#define DHCPV6_DEC_TIMEOUT                  1
#define DHCPV6_DEC_MAX_RC                   5

#define DHCPV6_REC_TIMEOUT                  2
#define DHCPV6_REC_MAX_RC                   8

#define DHCPV6_HOP_COUNT_LIMIT              32

#define DHCP6_ADRS_CMP(x, y)                memcmp((x), (y), sizeof(mesa_ipv6_t))
#define DHCP6_ADRS_CPY(x, y)                memcpy((x), (y), sizeof(mesa_ipv6_t))
#define DHCP6_ADRS_SET(x, y)                memset((x), (y), sizeof(mesa_ipv6_t))
#define DHCP6_ADRS_ZERO(x)                  !DHCP6_ADRS_CMP(x, &dhcp6_unspecified_address)
#define DHCP6_ADRS_EQUAL(x, y)              !DHCP6_ADRS_CMP(x, y)

#define DHCP6_DUID_LLT_LEN(x)               (sizeof((x)->duid_type) + sizeof((x)->type.llt))
#define DHCP6_DUID_LL_LEN(x)                (sizeof((x)->duid_type) + sizeof((x)->type.ll))
#define DHCP6_DUID_EN_LEN(x)                (sizeof((x)->duid_type) + sizeof((x)->type.en))

#define DHCP6_MSG_MSG_TYPE(x)               (ntohl((x)->msg_xid) >> 24)
#define DHCP6_MSG_TRANSACTION_ID(x)         ((ntohl((x)->msg_xid) << 8) >> 8)
#define DHCP6_MSG_OPTIONS_PTR(x)            (Dhcp6Option *)(&((x)->options))

#define DHCP6_MSG_OPT_CODE(x)               (ntohs((x)->code))
#define DHCP6_MSG_OPT_LENGTH(x)             (ntohs((x)->length))
#define DHCP6_MSG_OPT_DATA_PTR_CAST(x, y)   (DHCP6_MSG_OPT_LENGTH(x) ? (y *)(&((x)->data)) : NULL)
#define DHCP6_MSG_OPT_SHIFT_LENGTH(x)       (sizeof((x)->code) + sizeof((x)->length) + DHCP6_MSG_OPT_LENGTH((x)))
#define DHCP6_MSG_OPT_NEXT(x)               (Dhcp6Option *)((u8 *)(x) + DHCP6_MSG_OPT_SHIFT_LENGTH((x)))

#define DHCP6_OPT_CODE_SET(x, y)            (x)->common.code = htons((y))
#define DHCP6_OPT_CODE_GET(x)               DHCP6_MSG_OPT_CODE(&((x)->common))
#define DHCP6_OPT_LEN_SET(x, y)             (x)->common.length = htons((y))
#define DHCP6_OPT_DATA_LEN_GET(x)           DHCP6_MSG_OPT_LENGTH(&((x)->common))
#define DHCP6_OPT_LEN_GET(x)                DHCP6_OPT_DATA_LEN_GET((x)) + sizeof(OptCommon)
#define DHCP6_OPT_LEN_INC(x, y)             (x) = (x) + DHCP6_OPT_LEN_GET((y))
#define DHCP6_OPT_DO_COPY(a, b, c, d, e, f) do {    \
    void    *_opt = (d);                            \
    size_t  _ofst = (e), _len = (f);                \
    memcpy((a), (b), (c));                          \
    if (_opt && _len) {                             \
        memcpy((a) + _ofst, (u8 *)_opt, _len);      \
    }                                               \
    (a) += DHCP6_OPT_LEN_GET((b));                  \
} while (0)

/*
    RFC-3315: DUID-LLT  (9.2)
     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |              1                |    hardware type (16 bits)    |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                       time (32 bits)                          |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    .                                                               .
    .            link-layer address (variable length)               .
    .                                                               .
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

    RFC-3315: DUID-EN   (9.3)
     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |              2                |       enterprise-number       |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |   enterprise-number (contd)   |                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+                               |
    .                           identifier                          .
    .                       (variable length)                       .
    .                                                               .
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

    RFC-3315: DUID-LL   (9.4)
     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |              3              |     hardware type (16 bits)     |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    .                                                               .
    .            link-layer address (variable length)               .
    .                                                               .
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
struct DeviceDuid {
    u16                         duid_type;

    union {
        struct {
            u16                 hardware_type;
            u32                 time;
            mesa_mac_t          lla;
        } VTSS_DHCP6_PACK_STRUCT llt;

        struct {
            u16                 hardware_type;
            mesa_mac_t          lla;
        } VTSS_DHCP6_PACK_STRUCT ll;

        struct {
            u32                 enterprise_number;
            u32                 id;
        } VTSS_DHCP6_PACK_STRUCT en;
    } type;
} VTSS_DHCP6_PACK_STRUCT;

struct Rxmit {
    vtss_tick_count_t           rxmit_timeout;
    u16                         init_rxmit_time;
    u16                         max_rxmit_time;
    vtss_tick_count_t           max_rxmit_duration;
    u16                         max_rxmit_count;
    u16                         max_delay;
    u32                         rtprev;
};

struct OptCommon {
    u16                         code;
    u16                         length;
} VTSS_DHCP6_PACK_STRUCT;

BOOL duid_equal(BOOL nwo_a, const DeviceDuid *const a, BOOL nwo_b, const DeviceDuid *const b);
BOOL duid_assign(BOOL nwo_a, DeviceDuid *const a, BOOL nwo_b, const DeviceDuid *const b);
BOOL client_support_msg_type(const Dhcp6Message *const dhcp6_msg);
BOOL client_validate_opt_code(const Dhcp6Message *const dhcp6_msg, u32 len);

/*
    RFC-3315: Client Identifier Option (22.2)

    The Client Identifier option is used to carry a DUID (see section 9)
    identifying a client between a client and a server.
*/
struct OptClientId {
    OptCommon                   common;
    DeviceDuid                  duid;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3315: Server Identifier Option (22.3)

    The Server Identifier option is used to carry a DUID (see section 9)
    identifying a server between a client and a server.
*/
struct OptServerId {
    OptCommon                   common;
    DeviceDuid                  duid;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3315: Identity Association for Non-temporary Addresses Option (22.4)

    The Identity Association for Non-temporary Addresses option (IA_NA
    option) is used to carry an IA_NA, the parameters associated with the
    IA_NA, and the non-temporary addresses associated with the IA_NA.
*/
struct OptIaNa {
    OptCommon                   common;
    u32                         iaid;
    u32                         t1;
    u32                         t2;
    u8                          *options;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3315: Identity Association for Temporary Addresses Option (22.5)

    The Identity Association for the Temporary Addresses (IA_TA) option
    is used to carry an IA_TA, the parameters associated with the IA_TA
    and the addresses associated with the IA_TA.
*/
struct OptIaTa {
    OptCommon                   common;
    u32                         iaid;
    u8                          *options;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3315: IA Address Option (22.6)

    The IA Address option is used to specify IPv6 addresses associated
    with an IA_NA or an IA_TA.
*/
struct OptIaAddr {
    OptCommon                   common;
    mesa_ipv6_t                 address;
    u32                         preferred_lifetime;
    u32                         valid_lifetime;
    u8                          *options;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3315: Option Request Option (22.7)

    A client MAY include an Option Request option in a Solicit, Request,
    Renew, Rebind, Confirm or Information-request message to inform the
    server about options the client wants the server to send to the client.
*/
struct OptOro {
    OptCommon                   common;
    u32                         *options;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3315: Preference Option (22.8)

    The Preference option is sent by a server to a client to affect the
    selection of a server by the client.
*/
struct OptPreference {
    OptCommon                   common;
    u8                          pref_value;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3315: Elapsed Time Option (22.9)

    A client MUST include an Elapsed Time option in messages to indicate
    how long the client has been trying to complete a DHCP message exchange.
*/
struct OptElapsedTime {
    OptCommon                   common;
    u16                         elapsed_time;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3315: Authentication Option (22.11)*NotSupport

    The Authentication option carries authentication information to
    authenticate the identity and contents of DHCP messages.
*/
struct OptAuth {
    OptCommon                   common;
    u8                          protocol;
    u8                          algorithm;
    u8                          replay_detection_method;
    u64                         replay_detection;
    u8                          *authentication_information;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3315: Server Unicast Option (22.12)

    The server sends this option to a client to indicate to the client
    that it is allowed to unicast messages to the server.
*/
struct OptUnicast {
    OptCommon                   common;
    mesa_ipv6_t                 server_address;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3315: Status Code Option (22.13)

    This option returns a status indication related to the DHCP message
    or option in which it appears.
*/
struct OptStatusCode {
    OptCommon                   common;
    u16                         status_code;
    i8                          *status_message;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3315: Rapid Commit Option (22.14)

    The Rapid Commit option is used to signal the use of the two message
    exchange for address assignment.
*/
struct OptRapidCommit {
    OptCommon                   common;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3315: User Class Option (22.15)*NotSupport

    The User Class option is used by a client to identify the type or
    category of user or applications it represents.
*/
struct OptUserClass {
    OptCommon                   common;
    u16                         user_class_len;
    u8                          *opaque_data;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3315: Vendor Class Option (22.16)*NotSupport

    This option is used by a client to identify the vendor that
    manufactured the hardware on which the client is running.
*/
struct OptVendorClass {
    OptCommon                   common;
    u32                         enterprise_number;
    u16                         vendor_class_len;
    u8                          *opaque_data;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3315: Vendor-specific Information Option (22.17)*NotSupport

    This option is used by clients and servers to exchange
    vendor-specific information.
*/
struct OptVendorOpt {
    OptCommon                   common;
    u32                         enterprise_number;
    u8                          *option_data;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3315: Reconfigure Message Option (22.19)

    A server includes a Reconfigure Message option in a Reconfigure
    message to indicate to the client whether the client responds with a
    Renew message or an Information-request message.
*/
struct OptReconfMsg {
    OptCommon                   common;
    u8                          msg_type;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3315: Reconfigure Accept Option (22.20)

    A client uses the Reconfigure Accept option to announce to the server
    whether the client is willing to accept Reconfigure messages, and a
    server uses this option to tell the client whether or not to accept
    Reconfigure messages.
*/
struct OptReconfAccept {
    OptCommon                   common;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3633: IA_PD Prefix option (10)

    The IA_PD Prefix option is used to specify IPv6 address prefixes
    associated with an IA_PD. The IA_PD Prefix option must be
    encapsulated in the IA_PD-options field of an IA_PD option.
*/
struct OptIaPrefix {
    OptCommon                   common;
    u32                         preferred_lifetime;
    u32                         valid_lifetime;
    u8                          prefix_length;
    mesa_ipv6_t                 ipv6_prefix;
    u8                          *ia_prefix_options;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3633: IA_PD option (9)

    The IA_PD option is used to carry a prefix delegation identity
    association, the parameters associated with the IA_PD and the
    prefixes associated with it.
*/
struct OptIaPd {
    OptCommon                   common;
    u32                         iaid;
    u32                         t1;
    u32                         t2;
    OptIaPrefix                 *ia_pd_options;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3646: DNS Recursive Name Server option (3)

    The DNS Recursive Name Server option provides a list of one or more
    IPv6 addresses of DNS recursive name servers to which a client's DNS
    resolver MAY send DNS queries [STD 13, RFC 1035].
*/
struct OptDnsServers {
    OptCommon                   common;
    mesa_ipv6_t                 *servers;
} VTSS_DHCP6_PACK_STRUCT;

/*
    RFC-3646: Domain Search List option (4)

    The Domain Search List option specifies the domain search list the
    client is to use when resolving hostnames with DNS.
*/
struct OptDomainList {
    OptCommon                   common;
    i8                          *srchlst;
} VTSS_DHCP6_PACK_STRUCT;

namespace client
{

#define DHCP6_WALK_FWD_LIST(x, y)           for (auto (y) = (x).begin(); (y) != (x).end(); ++(y))

const OptionCode SOL_OPT_REQ[] = {
    OPT_DNS_SERVERS,
    OPT_DOMAIN_LIST,
    /*
        OPT_IA_PD,
        OPT_IAPREFIX,
        OPT_VENDOR_OPTS,
    */

    /* add new support option(s) above this line */
    OPT_RESERVED
};
#define DHCP6_SOL_OPT_REQ_CNT               ((sizeof(vtss::dhcp6::client::SOL_OPT_REQ) / sizeof(vtss::dhcp6::OptionCode)) - 1)

#define DHCP6_SOL_OPT_REQ_ENCODE(x)         do {        \
    u8  _i = 0;                                         \
    while (_i < DHCP6_SOL_OPT_REQ_CNT) {                \
        if (_i % 2) {                                   \
            *(x) = *(x) | (SOL_OPT_REQ[_i] & 0xFFFF);   \
            *(x) = htonl(*(x));                         \
            if (++_i < DHCP6_SOL_OPT_REQ_CNT) {         \
                (x)++;                                  \
            }                                           \
        } else {                                        \
            *(x) = 0;                                   \
            *(x) = SOL_OPT_REQ[_i] << 16;               \
            if (++_i == DHCP6_SOL_OPT_REQ_CNT) {        \
                *(x) = htonl(*(x));                     \
            }                                           \
        }                                               \
    }                                                   \
} while (0)

struct AddrInfo {
    IaType                      type;
    mesa_ipv6_t                 address;
    u32                         prefix_length;

    vtss_tick_count_t           refresh_ts;
    vtss_tick_count_t           t1;
    vtss_tick_count_t           t2;
    vtss_tick_count_t           preferred_lifetime;
    vtss_tick_count_t           valid_lifetime;
};

struct ServerRecord {
    mesa_ipv6_t                 addrs;
    mesa_ipv6_t                 ucast;
    BOOL                        rapid_commit;
    BOOL                        has_unicast;
    u16                         reserved;
    DeviceDuid                  duid;
    u32                         xid_prf;
    std::forward_list<mesa_ipv6_t>   name_server;
    std::forward_list<std::string>   name_list;
    std::forward_list<AddrInfo>      adrs_pool;
};

#define DHCP6_SERVER_RECORD_CNT             3
#define DHCP6_SERVER_RECORD_XIDX_GET(x)     ((x)->xid_prf >> 8)
#define DHCP6_SERVER_RECORD_PREF_GET(x)     ((x)->xid_prf & 0xFF)
#define DHCP6_SERVER_RECORD_XIDX_SET(x, y)  (x)->xid_prf = ((x)->xid_prf & 0xFF) | (((y) << 8) & 0xFFFFFF00)
#define DHCP6_SERVER_RECORD_PREF_SET(x, y)  (x)->xid_prf = ((x)->xid_prf & 0xFFFFFF00) | ((y) & 0xFF)

#define DHCP6_INTF_MEM_CALLOC_CAST(x) (x = VTSS_CREATE(vtss::dhcp6::client::Interface))
#define DHCP6_INTF_MEM_FREE(x)              do {        \
    u8  _cnt = DHCP6_SERVER_RECORD_CNT + 1;             \
    for (; _cnt; --_cnt) {                              \
        if (!(x)->srvrs[_cnt - 1].name_server.empty())  \
            (x)->srvrs[_cnt - 1].name_server.clear();   \
        if (!(x)->srvrs[_cnt - 1].name_list.empty())    \
            (x)->srvrs[_cnt - 1].name_list.clear();     \
        if (!(x)->srvrs[_cnt - 1].adrs_pool.empty())    \
            (x)->srvrs[_cnt - 1].adrs_pool.clear();     \
    }                                                   \
    vtss_destroy(x);                                    \
} while (0)

struct Counter {
    u32                         rx_advertise;
    u32                         rx_reply;
    u32                         rx_reconfigure;
    u32                         rx_error;
    u32                         rx_drop;
    u32                         rx_unknown;

    u32                         tx_solicit;
    u32                         tx_request;
    u32                         tx_confirm;
    u32                         tx_renew;
    u32                         tx_rebind;
    u32                         tx_release;
    u32                         tx_decline;
    u32                         tx_information_request;
    u32                         tx_error;
    u32                         tx_drop;
    u32                         tx_unknown;
};

struct Interface {
    DeviceDuid                  duid;
    ServiceType                 type;

    mesa_vid_t                  ifidx;
    BOOL                        rapid_commit;
    BOOL                        stateless;
    BOOL                        link;
    BOOL                        dad;
    BOOL                        m_flag;
    BOOL                        o_flag;

    AddrInfo                    addrs;
    Counter                     cntrs;

    u32                         iaid;
    u32                         xidx;

    Rxmit                       rxmit;
    MessageType                 active_msg;
    vtss_tick_count_t           init_xmt;
    vtss_tick_count_t           last_xmt;
    u32                         xmt_cntr;

    ServerRecord                *server;
    ServerRecord                srvrs[DHCP6_SERVER_RECORD_CNT + 1];
};

void initialize(u8 max_intf_cnt);
mesa_rc tick(void);

mesa_rc receive(
    u8                  *const frm,
    vtss_tick_count_t   ts,
    u32                 len,
    mesa_vid_t          ifx
);

mesa_rc transmit(
    mesa_vid_t          ifx,
    const mesa_ipv6_t   *const sip,
    const mesa_ipv6_t   *const dip,
    MessageType         msg_type,
    u32                 xid,
    u32                 opt_len,
    const u8            *const opts
);

mesa_rc do_rx_solicit(
    BOOL                commit,
    const mesa_ipv6_t   *const sip,
    mesa_vid_t          ifx,
    u32                 xidx,
    u16                 code,
    u16                 length,
    const u8            *const data
);
mesa_rc do_rx_advertise(
    BOOL                commit,
    const mesa_ipv6_t   *const sip,
    mesa_vid_t          ifx,
    u32                 xidx,
    u16                 code,
    u16                 length,
    const u8            *const data
);
mesa_rc do_rx_request(
    BOOL                commit,
    const mesa_ipv6_t   *const sip,
    mesa_vid_t          ifx,
    u32                 xidx,
    u16                 code,
    u16                 length,
    const u8            *const data
);
mesa_rc do_rx_confirm(
    BOOL                commit,
    const mesa_ipv6_t   *const sip,
    mesa_vid_t          ifx,
    u32                 xidx,
    u16                 code,
    u16                 length,
    const u8            *const data
);
mesa_rc do_rx_renew(
    BOOL                commit,
    const mesa_ipv6_t   *const sip,
    mesa_vid_t          ifx,
    u32                 xidx,
    u16                 code,
    u16                 length,
    const u8            *const data
);
mesa_rc do_rx_rebind(
    BOOL                commit,
    const mesa_ipv6_t   *const sip,
    mesa_vid_t          ifx,
    u32                 xidx,
    u16                 code,
    u16                 length,
    const u8            *const data
);
mesa_rc do_rx_reply(
    BOOL                commit,
    const mesa_ipv6_t   *const sip,
    mesa_vid_t          ifx,
    u32                 xidx,
    u16                 code,
    u16                 length,
    const u8            *const data
);
mesa_rc do_rx_release(
    BOOL                commit,
    const mesa_ipv6_t   *const sip,
    mesa_vid_t          ifx,
    u32                 xidx,
    u16                 code,
    u16                 length,
    const u8            *const data
);
mesa_rc do_rx_decline(
    BOOL                commit,
    const mesa_ipv6_t   *const sip,
    mesa_vid_t          ifx,
    u32                 xidx,
    u16                 code,
    u16                 length,
    const u8            *const data
);
mesa_rc do_rx_reconfigure(
    BOOL                commit,
    const mesa_ipv6_t   *const sip,
    mesa_vid_t          ifx,
    u32                 xidx,
    u16                 code,
    u16                 length,
    const u8            *const data
);
mesa_rc do_rx_information_request(
    BOOL                commit,
    const mesa_ipv6_t   *const sip,
    mesa_vid_t          ifx,
    u32                 xidx,
    u16                 code,
    u16                 length,
    const u8            *const data
);

mesa_rc do_tx_solicit(Interface *const intf, vtss_tick_count_t ts);
mesa_rc do_tx_advertise(Interface *const intf, vtss_tick_count_t ts);
mesa_rc do_tx_request(Interface *const intf, vtss_tick_count_t ts);
mesa_rc do_tx_confirm(Interface *const intf, vtss_tick_count_t ts);
mesa_rc do_tx_renew(Interface *const intf, vtss_tick_count_t ts);
mesa_rc do_tx_rebind(Interface *const intf, vtss_tick_count_t ts);
mesa_rc do_tx_reply(Interface *const intf, vtss_tick_count_t ts);
mesa_rc do_tx_release(Interface *const intf, vtss_tick_count_t ts);
mesa_rc do_tx_decline(Interface *const intf, vtss_tick_count_t ts);
mesa_rc do_tx_reconfigure(Interface *const intf, vtss_tick_count_t ts);
mesa_rc do_tx_information_request(Interface *const intf, vtss_tick_count_t ts);
mesa_rc do_tx_migrate_counter(Interface *const intf);

mesa_rc interface_itr(mesa_vid_t ifx, Interface *const intf);
mesa_rc interface_get(mesa_vid_t ifx, Interface *const intf);
mesa_rc interface_set(mesa_vid_t ifx, const Interface *const intf);
mesa_rc interface_del(mesa_vid_t ifx);
mesa_rc interface_rst(mesa_vid_t ifx);
mesa_rc interface_clr(mesa_vid_t ifx, BOOL cntr, BOOL addr);
mesa_rc interface_link(mesa_vid_t ifx, i8 link);
mesa_rc interface_dad(mesa_vid_t ifx);
mesa_rc interface_flag(mesa_vid_t ifx, BOOL managed, BOOL other);
mesa_rc interface_rx_cntr_inc(mesa_vid_t ifx, MessageType msg_type, BOOL err, BOOL drop);
mesa_rc interface_tx_cntr_inc(mesa_vid_t ifx, MessageType msg_type, BOOL err, BOOL drop);
} /* client */
} /* dhcp6 */
} /* vtss */

#undef VTSS_TRACE_MODULE_ID

#endif /* _VTSS_DHCP6_CORE_HXX_ */
