/*

 Copyright (c) 2006-2017 Microsemi Corporation "Microsemi". All Rights Reserved.

 Unpublished rights reserved under the copyright laws of the United States of
 America, other countries and international treaties. Permission to use, copy,
 store and modify, the software and its source code is granted but only in
 connection with products utilizing the Microsemi switch and PHY products.
 Permission is also granted for you to integrate into other products, disclose,
 transmit and distribute the software only in an absolute machine readable
 format (e.g. HEX file) and only in or with products utilizing the Microsemi
 switch and PHY products.  The source code of the software may not be
 disclosed, transmitted or distributed without the prior written permission of
 Microsemi.

 This copyright notice must appear in any copy, modification, disclosure,
 transmission or distribution of the software.  Microsemi retains all
 ownership, copyright, trade secret and proprietary rights in the software and
 its source code, including all modifications thereto.

 THIS SOFTWARE HAS BEEN PROVIDED "AS IS". MICROSEMI HEREBY DISCLAIMS ALL
 WARRANTIES OF ANY KIND WITH RESPECT TO THE SOFTWARE, WHETHER SUCH WARRANTIES
 ARE EXPRESS, IMPLIED, STATUTORY OR OTHERWISE INCLUDING, WITHOUT LIMITATION,
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE AND
 NON-INFRINGEMENT.

*/

#ifndef _VTSS_DHCP6_FRAME_HXX_
#define _VTSS_DHCP6_FRAME_HXX_

namespace vtss
{
#define IPV6_IP_HEADER_VERSION(x)           (ntohl((x)->ver_tc_fl) >> 28)
#define IPV6_IP_HEADER_TRAFFIC_CLASS(x)     ((ntohl((x)->ver_tc_fl) << 4) >> 24)
#define IPV6_IP_HEADER_FLOW_LABEL(x)        (ntohl((x)->ver_tc_fl) & 0xFFFFF)
#define IPV6_IP_HEADER_PAYLOAD_LEN(x)       (ntohs((x)->payload_len))
#define IPV6_IP_HEADER_NXTHDR(x)            ((x)->next_header)
#define IPV6_IP_HEADER_HOP_LIMIT(x)         ((x)->hop_limit)
#define IPV6_IP_HEADER_SRC_ADDR_PTR(x)      (&((x)->src))
#define IPV6_IP_HEADER_DST_ADDR_PTR(x)      (&((x)->dst))

struct Ip6Header {
    u32                     ver_tc_fl;      /* Version, Traffic Class, Flow Label */
    u16                     payload_len;    /* Payload Length */
    u8                      next_header;    /* Next Header */
    u8                      hop_limit;      /* Hop Limit */
    mesa_ipv6_t             src;            /* Source Address */
    mesa_ipv6_t             dst;            /* Destination Address */
} VTSS_IPV6_PACK_STRUCT;

#define IPV6_UPPER_LAYER_HDR_CHKSUM_INVALID 0
#define IPV6_CHECKSUM_SUM(x, y, z)          do {    \
    u16 *_ptr = (u16 *)(y);                         \
    int __len = (z);                                \
    while (__len > 1) {                             \
        (x) += *_ptr++;                             \
        __len -= 2;                                 \
    }                                               \
    if (__len == 1) { /* add up any odd byte */     \
        (x) += ((*_ptr) & htons(0xFF00));           \
    }                                               \
} while (0)

/*
    RFC-2460 Upper-Layer Checksums (8.1)
    TCP and UDP "pseudo-header" for IPv6

    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                                                               |
    +                                                               +
    |                                                               |
    +                         Source Address                        +
    |                                                               |
    +                                                               +
    |                                                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                                                               |
    +                                                               +
    |                                                               |
    +                      Destination Address                      +
    |                                                               |
    +                                                               +
    |                                                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                   Upper-Layer Packet Length                   |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                      zero                     |  Next Header  |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

    o   If the IPv6 packet contains a Routing header, the Destination
        Address used in the pseudo-header is that of the final
        destination.  At the originating node, that address will be in
        the last element of the Routing header; at the recipient(s),
        that address will be in the Destination Address field of the
        IPv6 header.
    o   The Next Header value in the pseudo-header identifies the
        upper-layer protocol (e.g., 6 for TCP, or 17 for UDP).  It will
        differ from the Next Header value in the IPv6 header if there
        are extension headers between the IPv6 header and the upper-
        layer header.
    o   The Upper-Layer Packet Length in the pseudo-header is the
        length of the upper-layer header and data (e.g., TCP header
        plus TCP data).  Some upper-layer protocols carry their own
        length information (e.g., the Length field in the UDP header);
        for such protocols, that is the length used in the pseudo-
        header.  Other protocols (such as TCP) do not carry their own
        length information, in which case the length used in the
        pseudo-header is the Payload Length from the IPv6 header, minus
        the length of any extension headers present between the IPv6
        header and the upper-layer header.
    o   Unlike IPv4, when UDP packets are originated by an IPv6 node,
        the UDP checksum is not optional.  That is, whenever
        originating a UDP packet, an IPv6 node must compute a UDP
        checksum over the packet and the pseudo-header, and, if that
        computation yields a result of zero, it must be changed to hex
        FFFF for placement in the UDP header.  IPv6 receivers must
        discard UDP packets containing a zero checksum, and should log
        the error.
*/
struct Ip6PseudoHeader {
    mesa_ipv6_t             src;            /* Source Address */
    mesa_ipv6_t             dst;            /* Destination Address */
    u32                     len;            /* Upper-Layer Packet Length */
    u8                      zero[3];        /* MBZ */
    u8                      nxt_hdr[1];     /* Next Header */
} VTSS_IPV6_PACK_STRUCT;

#define IPV6_UDP_HEADER_SRC_PORT(x)         (ntohs((x)->src_port))
#define IPV6_UDP_HEADER_DST_PORT(x)         (ntohs((x)->dst_port))
#define IPV6_UDP_HEADER_CHECKSUM(x)         (ntohs((x)->check_sum))
#define IPV6_UDP_HEADER_LENGTH(x)           (ntohs((x)->len))

struct UdpHeader {
    u16                     src_port;       /* UDP Source Port */
    u16                     dst_port;       /* UDP Destination Port */
    u16                     len;            /* UDP Length */
    u16                     check_sum;      /* UDP Checksum */
} VTSS_IPV6_PACK_STRUCT;

namespace dhcp6
{
/*
    RFC-3315

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |  msg-type   |                  transaction-id                 |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                                                               |
    .                          options                              .
    .                         (variable)                            .
    |                                                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    msg-type:       Identifies the DHCP message type
    transaction-id: The transaction ID for this message exchange.
    options:        Options carried in this message.
*/
struct Dhcp6Message {
    u32                     msg_xid;        /* DHCPv6 Message Type & Transaction ID */
    u8                      *options;       /* DHCPv6 Options */
} VTSS_DHCP6_PACK_STRUCT;

mesa_rc rx_solicit(
    ServiceRole             r,
    const mesa_ipv6_t       *const sip,
    const Dhcp6Message      *const msg,
    vtss_tick_count_t       ts,
    u32                     len,
    mesa_vid_t              ifx
);

mesa_rc rx_advertise(
    ServiceRole             r,
    const mesa_ipv6_t       *const sip,
    const Dhcp6Message      *const msg,
    vtss_tick_count_t       ts,
    u32                     len,
    mesa_vid_t              ifx
);

mesa_rc rx_request(
    ServiceRole             r,
    const mesa_ipv6_t       *const sip,
    const Dhcp6Message      *const msg,
    vtss_tick_count_t       ts,
    u32                     len,
    mesa_vid_t              ifx
);

mesa_rc rx_confirm(
    ServiceRole             r,
    const mesa_ipv6_t       *const sip,
    const Dhcp6Message      *const msg,
    vtss_tick_count_t       ts,
    u32                     len,
    mesa_vid_t              ifx
);

mesa_rc rx_renew(
    ServiceRole             r,
    const mesa_ipv6_t       *const sip,
    const Dhcp6Message      *const msg,
    vtss_tick_count_t       ts,
    u32                     len,
    mesa_vid_t              ifx
);

mesa_rc rx_rebind(
    ServiceRole             r,
    const mesa_ipv6_t       *const sip,
    const Dhcp6Message      *const msg,
    vtss_tick_count_t       ts,
    u32                     len,
    mesa_vid_t              ifx
);

mesa_rc rx_reply(
    ServiceRole             r,
    const mesa_ipv6_t       *const sip,
    const Dhcp6Message      *const msg,
    vtss_tick_count_t       ts,
    u32                     len,
    mesa_vid_t              ifx
);

mesa_rc rx_release(
    ServiceRole             r,
    const mesa_ipv6_t       *const sip,
    const Dhcp6Message      *const msg,
    vtss_tick_count_t       ts,
    u32                     len,
    mesa_vid_t              ifx
);

mesa_rc rx_decline(
    ServiceRole             r,
    const mesa_ipv6_t       *const sip,
    const Dhcp6Message      *const msg,
    vtss_tick_count_t       ts,
    u32                     len,
    mesa_vid_t              ifx
);

mesa_rc rx_reconfigure(
    ServiceRole             r,
    const mesa_ipv6_t       *const sip,
    const Dhcp6Message      *const msg,
    vtss_tick_count_t       ts,
    u32                     len,
    mesa_vid_t              ifx
);

mesa_rc rx_information_request(
    ServiceRole             r,
    const mesa_ipv6_t       *const sip,
    const Dhcp6Message      *const msg,
    vtss_tick_count_t       ts,
    u32                     len,
    mesa_vid_t              ifx
);

template<unsigned SIZE>
struct Buf_t {
    Buf_t() {}

    template<typename I>
    Buf_t(I d)
    {
        for (unsigned i = 0; i < SIZE; ++i) {
            data_[i] = *d++;
        }
    }

    unsigned size() const
    {
        return SIZE;
    }

    void copy_to(u8 *buf) const
    {
        for (unsigned i = 0; i < SIZE; ++i) {
            *buf++ = data_[i];
        }
    }

    template<typename I>
    void copy_from(I buf)
    {
        for (unsigned i = 0; i < SIZE; ++i) {
            data_[i] = *buf++;
        }
    }

    u8 data_[SIZE];
};

/*
    RFC-3315

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    | option-code |                    option-len                   |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                        option-data                            |
    |                    (option-len octets)                        |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    option-code:    An unsigned integer identifying the specific option
                    type carried in this option.
    option-len:     An unsigned integer giving the length of the option-data
                    field in this option in octets.
    option-data:    The data for the option; the format of this data depends
                    on the definition of the option.
*/
struct Dhcp6Option {
    u16                     code;           /* DHCPv6 Option Code */
    u16                     length;         /* DHCPv6 Option Length */
    u8                      *data;          /* DHCPv6 Option Data */
} VTSS_DHCP6_PACK_STRUCT;

} /* dhcp6 */
} /* vtss */

#endif /* _VTSS_DHCP6_FRAME_HXX_ */
